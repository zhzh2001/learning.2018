#include<iostream>
#include<algorithm>
using namespace std;
const int N=105;
int n;
unsigned long long a[N],ans;
void dfs(int k,unsigned long long val,int mask)
{
	if(k==n+1)
		ans=min(ans,val);
	else
	{
		unsigned long long opr=a[k];
		if(mask&(1<<(k-1)))
			opr=~opr;
		dfs(k+1,val&opr,mask);
		dfs(k+1,val|opr,mask);
		dfs(k+1,val^opr,mask);
	}
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin>>t;
	for(int T=1;T<=t;T++)
	{
		cout<<"Case #"<<T<<": ";
		cin>>n;
		for(int i=1;i<=n;i++)
			cin>>a[i];
		if(n>6)
		{
			cout<<0<<endl;
			continue;
		}
		ans=0xffffffffffffffffull;
		for(int i=0;i<1<<n;i++)
			if(i&1)
				dfs(2,~a[1],i);
			else
				dfs(2,a[1],i);
		cout<<ans<<endl;
	}
	return 0;
}
