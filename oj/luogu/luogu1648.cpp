#include<iostream>
#include<algorithm>
using namespace std;
const int N=1000005;
const long long INF=1e18;
long long p[N][4];
int main()
{
	ios::sync_with_stdio(false);
	int n,d;
	cin>>n>>d;
	for(int i=1;i<=n;i++)
		for(int j=0;j<d;j++)
			cin>>p[i][j];
	long long ans=0;
	for(int i=0;i<1<<(d-1);i++)
	{
		long long now=-INF;
		int ind=0;
		for(int j=1;j<=n;j++)
		{
			long long now2=p[j][0];
			for(int k=0;k<d-1;k++)
				if(i&(1<<k))
					now2-=p[j][k+1];
				else
					now2+=p[j][k+1];
			if(now2>now)
			{
				now=now2;
				ind=j;
			}
		}
		for(int j=1;j<=n;j++)
		{
			long long now2=0;
			for(int k=0;k<d;k++)
				now2+=abs(p[ind][k]-p[j][k]);
			ans=max(ans,now2);
		}
		now=INF;
		ind=0;
		for(int j=1;j<=n;j++)
		{
			long long now2=p[j][0];
			for(int k=0;k<d-1;k++)
				if(i&(1<<k))
					now2-=p[j][k+1];
				else
					now2+=p[j][k+1];
			if(now2<now)
			{
				now=now2;
				ind=j;
			}
		}
		for(int j=1;j<=n;j++)
		{
			long long now2=0;
			for(int k=0;k<d;k++)
				now2+=abs(p[ind][k]-p[j][k]);
			ans=max(ans,now2);
		}
	}
	cout<<ans<<endl;
	return 0;
}
