#include<iostream>
#include<cstring>
using namespace std;
const int N=55,MOD=45989;
int n,m,t,a,b;
struct matrix
{
	long long mat[N][N];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix res;
		for(int i=0;i<=n;i++)
			for(int j=0;j<=n;j++)
				for(int k=0;k<=n;k++)
					res.mat[i][j]+=mat[i][k]*rhs.mat[k][j];
		for(int i=0;i<=n;i++)
			for(int j=0;j<=n;j++)
				res.mat[i][j]%=MOD;
		return res;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
};
matrix I()
{
	matrix res;
	for(int i=1;i<=n;i++)
		res.mat[i][i]=1;
	return res;
}
matrix qpow(matrix a,int b)
{
	matrix ans=I();
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}while(b/=2);
	return ans;
}
int main()
{
	cin>>n>>m>>t>>a>>b;
	a++;b++;
	matrix trans;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		u++;v++;
		trans.mat[u][v]++;trans.mat[v][u]++;
	}
	matrix init;
	init.mat[a][0]=1;
	cout<<(qpow(trans,t)*init).mat[b][0]<<endl;
	return 0;
}
