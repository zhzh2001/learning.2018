#include<iostream>
#include<algorithm>
using namespace std;
const int N=500005;
int a[N],b[N];
long long v[N];
int main()
{
	ios::sync_with_stdio(false);
	int n,c;
	cin>>n>>c;
	long long ans=0;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		ans+=a[i];
	}
	for(int i=1;i<=n;i++)
		cin>>b[i];
	for(int i=1;i<=n;i++)
		v[i]=b[i]-a[i]+1ll*(n-i)*c;
	sort(v+1,v+n+1);
	long long now=ans;
	for(int i=1;i<=n;i++)
	{
		now+=v[i];
		now-=1ll*(i-1)*c;
		ans=min(ans,now);
	}
	cout<<ans<<endl;
	return 0;
}
