#include<iostream>
using namespace std;
const int N=2005,MOD=1e9+7;
int sum[N][N],sumk[N][N],dp[N][N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=m-2;j++)
		{
			dp[i][j]=(1ll*(j+1)*sum[i-1][j]-sumk[i-1][j]+MOD+1)%MOD;
			sum[i][j]=((j?sum[i][j-1]:0)+dp[i][j])%MOD;
			sumk[i][j]=((j?sumk[i][j-1]:0)+1ll*dp[i][j]*j)%MOD;
		}
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=m-2;j++)
			ans=(ans+1ll*(m-2-j+1)*dp[i][j]%MOD*(dp[n-i+1][j]-dp[n-i][j]+MOD))%MOD;
	cout<<ans<<endl;
	return 0;
}
