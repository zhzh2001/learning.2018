#include<iostream>
#include<algorithm>
using namespace std;
const int N=2005,INF=1e9;
struct node
{
	int a,b;
	bool operator<(const node& rhs)const
	{
		return a+b<rhs.a+rhs.b;
	}
}p[N];
int f[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin>>n;
	int sum=0;
	for(int i=1;i<=n;i++)
	{
		cin>>p[i].a>>p[i].b;
		sum+=p[i].a;
	}
	int h;
	cin>>h;
	sort(p+1,p+n+1);
	fill(f+1,f+n+1,INF);
	for(int i=1;i<=n;i++)
		for(int j=i;j;j--)
			if(sum-f[j-1]+p[i].b>=h)
				f[j]=min(f[j],f[j-1]+p[i].a);
	for(int i=n;i>=0;i--)
		if(f[i]<INF)
		{
			cout<<i<<endl;
			break;
		}
	return 0;
}
