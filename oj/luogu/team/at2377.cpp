#include<iostream>
#include<unordered_map>
#include<unordered_set>
#include<queue>
#include<algorithm>
using namespace std;
const int N=100005;
int f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
struct myhash
{
	static const size_t B=1e9+7,MOD=1e9+9;
	size_t operator()(const pair<int,int>& pr)const
	{
		return (pr.first*B+pr.second)%MOD;
	}
};
unordered_set<int> mat[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin>>n;
	unordered_map<pair<int,int>,int,myhash> occur;
	queue<pair<int,int> > Q;
	for(int i=1;i<=n*2-2;i++)
	{
		int u,v;
		cin>>u>>v;
		mat[u].insert(v);
		mat[v].insert(u);
		if(u>v)
			swap(u,v);
		if(++occur[make_pair(u,v)]==2)
			Q.push(make_pair(u,v));
	}
	for(int i=1;i<=n;i++)
		f[i]=i;
	while(!Q.empty())
	{
		int u=getf(Q.front().first),v=getf(Q.front().second);
		Q.pop();
		mat[u].erase(v);mat[v].erase(u);
		if(mat[u].size()<mat[v].size())
			swap(u,v);
		for(int x:mat[v])
		{
			if(x>v)
				occur[make_pair(v,x)]=0;
			else
				occur[make_pair(x,v)]=0;
			mat[x].erase(v);
			mat[x].insert(u);
			if(x>u)
			{
				if(++occur[make_pair(u,x)]==2)
					Q.push(make_pair(u,x));
			}
			else
			{
				if(++occur[make_pair(x,u)]==2)
					Q.push(make_pair(x,u));
			}
			mat[u].insert(x);
		}
		f[v]=u;
		mat[v].clear();
	}
	int cnt=0;
	for(int i=1;i<=n;i++)
		cnt+=getf(i)==i;
	if(cnt==1)
		cout<<"YES\n";
	else
		cout<<"NO\n";
	return 0;
}
