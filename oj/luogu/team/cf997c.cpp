#include<iostream>
using namespace std;
const int N=1000005,MOD=998244353;
int fact[N],inv[N];
long long qpow(long long a,long long b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}while(b/=2);
	return ans;
}
inline long long c(int n,int m)
{
	return 1ll*fact[n]*inv[n-m]%MOD*inv[m]%MOD;
}
int main()
{
	int n;
	cin>>n;
	fact[0]=1;
	for(int i=1;i<=n;i++)
		fact[i]=1ll*fact[i-1]*i%MOD;
	inv[n]=qpow(fact[n],MOD-2);
	for(int i=n-1;i>=0;i--)
		inv[i]=1ll*inv[i+1]*(i+1)%MOD;
	int ans1=0,sign=1;
	for(int i=1;i<=n;i++,sign=-sign)
		ans1=(ans1+c(n,i)*sign*qpow(3,1ll*n*(n-i)+i)%MOD+MOD)%MOD;
	int ans2=0;
	sign=-1;
	int p=1;
	for(int i=0;i<n;i++,sign=-sign,p=p*3ll%MOD)
		ans2=(ans2+c(n,i)*sign*(qpow(1-p+MOD,n)-qpow(-p+MOD,n)+MOD)%MOD+MOD)%MOD;
	cout<<(ans1*2+ans2*3ll)%MOD<<endl;
	return 0;
}
