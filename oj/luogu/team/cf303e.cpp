#include<iostream>
#include<algorithm>
using namespace std;
const int N=81;
int l[N],r[N],x[N],cover[N];
double fore[N],in[N],dp[N][N][N],ans[N][N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>l[i]>>r[i];
		x[i*2-1]=l[i];
		x[i*2]=r[i];
	}
	sort(x+1,x+n*2+1);
	int xn=unique(x+1,x+n*2+1)-x-1;
	for(int seg=1;seg<xn;seg++)
	{
		for(int i=1;i<=n;i++)
			if(!(r[i]<=x[seg]||l[i]>=x[seg+1]))
			{
				int cc=0,cless=0;
				for(int j=1;j<=n;j++)
					if(!(r[j]<=x[seg]||l[j]>=x[seg+1]))
					{
						in[j]=1.*(x[seg+1]-x[seg])/(r[j]-l[j]);
						cover[++cc]=j;
					}
					else
					{
						in[j]=.0;
						if(r[j]<=x[seg])
							cless++;
					}
				dp[0][0][0]=1.;
				for(int j=1;j<=cc;j++)
	 				if(cover[j]==i)//why?
						for(int p=0;p<=n;p++)
							for(int q=0;q<=n;q++)
								dp[j][p][q]=dp[j-1][p][q];
					else
						for(int p=0;p<=j;p++)
							for(int q=0;q<=j-p;q++)
							{
								dp[j][p][q]=dp[j-1][p][q]*(1.-fore[cover[j]]-in[cover[j]]);
								if(p)
									dp[j][p][q]+=dp[j-1][p-1][q]*fore[cover[j]];
								if(q)
									dp[j][p][q]+=dp[j-1][p][q-1]*in[cover[j]];
							}
				for(int p=0;p<cc;p++)
					for(int q=0;q<cc-p;q++)
					{
						ans[i][cless+p+1]+=dp[cc][p][q]*in[i]/(q+1.);//sw weird
						ans[i][cless+p+q+2]-=dp[cc][p][q]*in[i]/(q+1.);
					}
			}
		for(int i=1;i<=n;i++)
			fore[i]+=in[i];
	}
	cout.precision(15);
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
			cout<<(ans[i][j]+=ans[i][j-1])<<' ';
		cout<<endl;
	}
	return 0;
}
