#include<iostream>
using namespace std;
const int N=100005,K=25;
unsigned c[N][K];
int main()
{
	ios::sync_with_stdio(false);
	c[0][0]=1;
	for(int i=1;i<N;i++)
	{
		c[i][0]=1;
		for(int j=1;j<=i&&j<K;j++)
			c[i][j]=c[i-1][j]+c[i-1][j-1];
	}
	int t;
	cin>>t;
	while(t--)
	{
		int n,k;
		cin>>n>>k;
		unsigned ans=0;
		for(int i=1;i<=k&&i<K;i++)
			ans+=(c[n][i]*i)<<(i-1);
		cout<<(ans&((1<<23)-1))<<'\n';
	}
	return 0;
}
