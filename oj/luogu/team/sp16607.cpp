#include<iostream>
using namespace std;
const int N=15,MOD=2004;
int n,a,b,m[N],ans;
int c(int n,int m)
{
	int p2=0,p3=0,p5=0,p7=0;
	for(int i=2;i<=m;i++)
	{
		int x=i;
		for(;x%2==0;x/=2)
			p2++;
		for(;x%3==0;x/=3)
			p3++;
		for(;x%5==0;x/=5)
			p5++;
		for(;x%7==0;x/=7)
			p7++;
	}
	int ans=1;
	for(int i=n-m+1;i<=n;i++)
	{
		int x=i;
		for(;p2&&x%2==0;x/=2)
			p2--;
		for(;p3&&x%3==0;x/=3)
			p3--;
		for(;p5&&x%5==0;x/=5)
			p5--;
		for(;p7&&x%7==0;x/=7)
			p7--;
		ans=1ll*ans*x%MOD;
	}
	return ans;
}
void dfs(int k,int inv,int rem)
{
	if(k==n+1)
		if(inv&1)
			ans=(ans-c(rem+n,n)+MOD)%MOD;
		else
			ans=(ans+c(rem+n,n))%MOD;
	else
	{
		dfs(k+1,inv,rem);
		if(rem>=m[k]+1)
			dfs(k+1,inv+1,rem-m[k]-1);
	}
}
int main()
{
	cin>>n>>a>>b;
	for(int i=1;i<=n;i++)
		cin>>m[i];
	dfs(1,0,b);
	dfs(1,1,a-1);
	cout<<ans<<endl;
	return 0;
}
