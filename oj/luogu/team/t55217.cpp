#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;
const int N = 1000005;
int a[N], ans[N];
pair<long long, int> q[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	for (int i = 1; i <= m; i++)
	{
		cin >> q[i].first;
		q[i].second = i;
	}
	sort(q + 1, q + m + 1);
	priority_queue<int> Q;
	for (int i = n; i; i--)
		if (a[i] < 0)
			Q.push(a[i]);
		else
			while (!Q.empty())
				if (Q.top() + a[i] > 0)
				{
					a[i] += Q.top();
					Q.pop();
				}
				else
				{
					int now = Q.top() + a[i];
					Q.pop();
					Q.push(now);
					break;
				}
	long long pred = 0;
	for (int i = 1; i <= m; i++)
	{
		long long tmp = q[i].first;
		q[i].first -= pred;
		pred = tmp;
		while (!Q.empty())
			if (Q.top() + q[i].first >= 0)
			{
				q[i].first += Q.top();
				Q.pop();
			}
			else
			{
				int now = Q.top() + q[i].first;
				Q.pop();
				Q.push(now);
				break;
			}
		ans[q[i].second] = Q.size();
	}
	for (int i = 1; i <= m; i++)
		cout << ans[i] << '\n';
	return 0;
}