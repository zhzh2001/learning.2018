#include<iostream>
#include<algorithm>
using namespace std;
const int N=2005;
int a[N];
int gcd(int a,int b)
{
	return b?gcd(b,a%b):a;
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	int cnt=count(a+1,a+n+1,1);
	if(cnt)
		cout<<n-cnt<<endl;
	else
	{
		int ans=-1;
		for(int i=1;i<=n;i++)
		{
			int g=a[i];
			for(int j=i+1;j<=n;j++)
			{
				g=gcd(g,a[j]);
				if(g==1)
				{
					int now=j-i+n-1;
					if(ans==-1||now<ans)
						ans=now;
					break;
				}
			}
		}
		cout<<ans<<endl;
	}
	return 0;
}
