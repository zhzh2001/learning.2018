#include<iostream>
#include<set>
#include<algorithm>
using namespace std;
const int N=200005;
int a[N],t[N];
int main()
{
	ios::sync_with_stdio(false);
	int n,w,k;
	cin>>n>>w>>k;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	for(int i=1;i<=n;i++)
		cin>>t[i];
	multiset<int> full,half;
	int ans=0,nowt=0,Ans=0;
	for(int l=1,r=0;r<=n;)
	{
		nowt+=(t[++r]+1)/2;
		ans+=a[r];
		half.insert(t[r]);
		if(half.size()>w)
		{
			int mn=*half.begin();
			nowt-=(mn+1)/2;
			nowt+=mn;
			half.erase(half.begin());
			full.insert(mn);
		}
		for(;l<=r&&nowt>k;l++)
		{
			ans-=a[l];
			if(t[l]<*half.begin())
			{
				nowt-=t[l];
				full.erase(full.find(t[l]));
			}
			else
			{
				nowt-=(t[l]+1)/2;
				half.erase(half.find(t[l]));
			}
			if(half.size()<w&&full.size())
			{
				int mx=*full.rbegin();
				nowt-=mx;
				nowt+=(mx+1)/2;
				full.erase(--full.end());
				half.insert(mx);
			}
		}
		Ans=max(Ans,ans);
	}
	cout<<Ans<<endl;
	return 0;
}
