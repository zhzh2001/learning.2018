#include<iostream>
#include<stack>
#include<algorithm>
#include<vector>
#include<cstdlib>
using namespace std;
const int N=200005,L=20;
int head[N],v[N*2],nxt[N*2],e,dfn[N],low[N],t,belong[N],scc,compont[N],cc;
inline void add_edge(int u,int v)
{
	::v[e]=v;
	nxt[e]=head[u];
	head[u]=e++;
}
bool vis[N];
stack<int> S;
vector<int> mat[N];
void dfs(int u,int fat)
{
	dfn[u]=low[u]=++t;
	compont[u]=cc;
	S.push(u);
	for(int k=head[u];~k;k=nxt[k])
	{
		if(!dfn[v[k]])
		{
			dfs(v[k],k^1);
			low[u]=min(low[u],low[v[k]]);
		}
		else if(k!=fat)
			low[u]=min(low[u],dfn[v[k]]);
	}
	if(dfn[u]==low[u])
	{
		++scc;
		int tmp;
		do
		{
			tmp=S.top();S.pop();
			belong[tmp]=scc;
		}while(tmp!=u);
	}
}
int f[N][L],dep[N],delta[N][2];
void dfs2(int u,int fat)
{
	for(int v:mat[u])
		if(v!=fat)
		{
			dep[v]=dep[u]+1;
			f[v][0]=u;
			dfs2(v,u);
		}
}
int lca(int u,int v)
{
	if(dep[u]<dep[v])
		swap(u,v);
	int delta=dep[u]-dep[v];
	for(int i=0;delta;i++,delta/=2)
		if(delta&1)
			u=f[u][i];
	if(u==v)
		return u;
	for(int i=L-1;i>=0;i--)
		if(f[u][i]!=f[v][i])
		{
			u=f[u][i];
			v=f[v][i];
		}
	return f[u][0];
}
void traverse(int u,int fat)
{
	vis[u]=true;
	for(int v:mat[u])
		if(v!=fat)
		{
			traverse(v,u);
			delta[u][0]+=delta[v][0];
			delta[u][1]+=delta[v][1];
		}
	if(delta[u][0]&&delta[u][1])
	{
		cout<<"No\n";
		exit(0);
	}
}
int main()
{
	ios::sync_with_stdio(false);
	int n,m,q;
	cin>>n>>m>>q;
	fill(head+1,head+n+1,-1);
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	for(int i=1;i<=n;i++)
		if(!dfn[i])
		{
			++cc;
			dfs(i,0);
		}
	for(int u=1;u<=n;u++)
		for(int k=head[u];~k;k=nxt[k])
			if(belong[u]!=belong[v[k]])
				mat[belong[u]].push_back(belong[v[k]]);
	for(int i=1;i<=scc;i++)
		if(!f[i][0])
			dfs2(i,0);
	for(int j=1;j<L;j++)
		for(int i=1;i<=scc;i++)
			f[i][j]=f[f[i][j-1]][j-1];
	while(q--)
	{
		int u,v;
		cin>>u>>v;
		if(compont[u]!=compont[v])
		{
			cout<<"No\n";
			return 0;
		}
		u=belong[u];v=belong[v];
		int a=lca(u,v);
		delta[u][0]++;delta[a][0]--;
		delta[v][1]++;delta[a][1]--;
	}
	for(int i=1;i<=scc;i++)
		if(!vis[i])
			traverse(i,0);
	cout<<"Yes\n";
	return 0;
}
