#include<iostream>
#include<algorithm>
#include<queue>
using namespace std;
const int N=100005,M=1000005;
int h[N],head[N],v[M*2],w[M*2],nxt[M*2],e;
bool vis[N];
inline void add_edge(int u,int v,int w)
{
	::v[++e]=v;
	::w[e]=w;
	nxt[e]=head[u];
	head[u]=e;
}
struct edge
{
	int u,v,w;
	bool operator<(const edge& rhs)const
	{
		if(h[v]!=h[rhs.v])
			return h[v]>h[rhs.v];
		return w<rhs.w;
	}
}E[M];
int f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	ios::sync_with_stdio(false);
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>h[i];
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		if(h[u]>=h[v])
			add_edge(u,v,w);
		if(h[v]>=h[u])
			add_edge(v,u,w);
	}
	queue<int> Q;
	Q.push(1);
	vis[1]=true;
	m=0;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(int i=head[k];i;i=nxt[i])
		{
			E[++m].u=k;E[m].v=v[i];E[m].w=w[i];
			if(!vis[v[i]])
			{
				Q.push(v[i]);
				vis[v[i]]=true;
			}
		}
	}
	sort(E+1,E+m+1);
	for(int i=1;i<=n;i++)
		f[i]=i;
	long long sum=0;
	for(int i=1;i<=m;i++)
	{
		int ru=getf(E[i].u),rv=getf(E[i].v);
		if(ru!=rv)
		{
			f[ru]=rv;
			sum+=E[i].w;
		}
	}
	cout<<count(vis+1,vis+n+1,true)<<' '<<sum<<endl;
	return 0;
}
