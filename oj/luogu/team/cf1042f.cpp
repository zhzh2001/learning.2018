#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
const int N=1000005;
int n,dist,ans;
vector<int> mat[N];
int dfs(int k,int fat)
{
	if(!mat[k].size())
		return 0;
	vector<int> child;
	for(int v:mat[k])
		if(v!=fat)
			child.push_back(dfs(v,k)+1);
	sort(child.begin(),child.end());
	for(;child.size()>1&&child.back()+child[child.size()-2]>dist;child.pop_back())
		ans++;
	return child.size()?child.back():0;
}
int main()
{
	ios::sync_with_stdio(false);
	cin>>n>>dist;
	for(int i=1;i<n;i++)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	for(int i=1;i<=n;i++)
		if(mat[i].size()>1)
		{
			dfs(i,0);
			break;
		}
	cout<<ans+1<<endl;
	return 0;
}
