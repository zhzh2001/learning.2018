#include <iostream>
#include <algorithm>
using namespace std;
const int N = 10005, M = 1005, INF = 1e9;
int up[N], down[N], low[N], high[N], f[N][M][2];
bool flag[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m, k;
	cin >> n >> m >> k;
	for (int i = 0; i < n; i++)
	{
		cin >> up[i] >> down[i];
		low[i] = 0;
		high[i] = m + 1;
	}
	low[n] = 0;
	high[n] = m + 1;
	while (k--)
	{
		int p, l, h;
		cin >> p >> l >> h;
		low[p] = l;
		high[p] = h;
	}
	fill_n(&f[0][0][0], sizeof(f) / sizeof(int), INF);
	for (int i = 0; i <= m; i++)
		f[0][i][0] = 0;
	for (int i = 0; i < n; i++)
		for (int j = 0; j <= m; j++)
		{
			int newh = min(j + up[i], m);
			if (f[i][j][0] < INF)
			{
				if (j - down[i] > low[i + 1] && j - down[i] < high[i + 1])
					f[i + 1][j - down[i]][0] = min(f[i + 1][j - down[i]][0], f[i][j][0]);
				f[i][newh][1] = min(f[i][newh][1], f[i][j][0] + 1);
				if (newh < high[i + 1] && newh > low[i + 1])
					f[i + 1][newh][0] = min(f[i + 1][newh][0], f[i][j][0] + 1);
			}
			if (f[i][j][1] < INF)
			{
				f[i][newh][1] = min(f[i][newh][1], f[i][j][1] + 1);
				if (newh < high[i + 1] && newh > low[i + 1])
					f[i + 1][newh][0] = min(f[i + 1][newh][0], f[i][j][1] + 1);
			}
			if ((f[i][j][0] < INF || f[i][j][1] < INF) && high[i] <= m)
				flag[i] = true;
		}
	int ans = *min_element(f[n][0], f[n][m] + 1);
	if (ans < INF)
		cout << 1 << endl
			 << ans << endl;
	else
		cout << 0 << endl
			 << count(flag + 1, flag + n + 1, true) << endl;
	return 0;
}