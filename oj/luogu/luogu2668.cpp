#include <iostream>
#include <cstring>
using namespace std;
const int N = 30;
int n, ans, cnt[N], cc;
bool vis[N][5];
void singlelist(int k, int cur, int dk, int drem);
void doublelist(int k, int cur, int dk, int drem);
void trilist(int k, int cur, int dk, int drem);
void dfs(int k, int rem)
{
	bool flags[4] = {};
	++cc;
	if (!rem)
	{
		ans = k - 1;
		return;
	}
	if (k > ans)
		return;
	if (rem >= 6)
	{
		for (int i = 1; i <= 14; i++)
			if (cnt[i] == 4)
				for (int j = 0; j <= 14; j++)
					if (j != i && cnt[j])
						for (int t = 0; t <= 14; t++)
							if (t != i && (cnt[t] >= (t == j) + 1))
							{
								vis[i][1] = vis[i][2] = vis[i][3] = vis[i][4] = false;
								cnt[i] = 0;
								if (cnt[j] >= 2 && cnt[t] >= (t == j) * 2 + 2)
								{
									for (int u = 1; u <= 4; u++)
										if (vis[j][u])
											for (int v = u + 1; v <= 4; v++)
												if (vis[j][v])
												{
													vis[j][u] = vis[j][v] = false;
													cnt[j] -= 2;
													for (int w = 1; w <= 4; w++)
														if (vis[t][w])
															for (int x = w + 1; x <= 4; x++)
																if (vis[t][x])
																{
																	vis[t][w] = vis[t][x] = false;
																	cnt[t] -= 2;
																	flags[0] = flags[1] = flags[2] = flags[3] = true;
																	dfs(k + 1, rem - 8);
																	vis[t][w] = vis[t][x] = true;
																	cnt[t] += 2;
																}
													vis[j][u] = vis[j][v] = true;
													cnt[j] += 2;
												}
								}
								else
									for (int x = 1; x <= 4; x++)
										if (vis[j][x])
										{
											vis[j][x] = false;
											cnt[j]--;
											for (int y = 1; y <= 4; y++)
												if (vis[t][y])
												{
													vis[t][y] = false;
													cnt[t]--;
													flags[0] = flags[1] = flags[2] = flags[3] = true;
													dfs(k + 1, rem - 6);
													vis[t][y] = true;
													cnt[t]++;
												}
											vis[j][x] = true;
											cnt[j]++;
										}
								vis[i][1] = vis[i][2] = vis[i][3] = vis[i][4] = true;
								cnt[i] = 4;
							}
		for (int i = 3; i <= 12; i++)
			if (cnt[i] >= 2 && cnt[i + 1] >= 2 && cnt[i + 2] >= 2)
				doublelist(1, i, k + 1, rem);
		for (int i = 3; i <= 13; i++)
			if (cnt[i] >= 3 && cnt[i + 1] >= 3)
				trilist(1, i, k + 1, rem);
	}
	if (rem >= 5)
	{
		for (int i = 3; i <= 10; i++)
			if (cnt[i] && cnt[i + 1] && cnt[i + 2] && cnt[i + 3] && cnt[i + 4])
				singlelist(1, i, k + 1, rem);
		for (int i = 1; i <= 14; i++)
			if (cnt[i] >= 3)
				for (int j = 0; j <= 14; j++)
					if (j != i && cnt[j] >= 2)
						for (int u = 1; u <= 4; u++)
							if (vis[i][u])
								for (int v = u + 1; v <= 4; v++)
									if (vis[i][v])
										for (int w = v + 1; w <= 4; w++)
											if (vis[i][w])
												for (int x = 1; x <= 4; x++)
													if (vis[j][x])
														for (int y = x + 1; y <= 4; y++)
															if (vis[j][y])
															{
																vis[i][u] = vis[i][v] = vis[i][w] = vis[j][x] = vis[j][y] = false;
																cnt[i] -= 3;
																cnt[j] -= 2;
																flags[1] = flags[2] = flags[3] = true;
																dfs(k + 1, rem - 5);
																vis[i][u] = vis[i][v] = vis[i][w] = vis[j][x] = vis[j][y] = true;
																cnt[i] += 3;
																cnt[j] += 2;
															}
	}
	if (rem >= 4)
	{
		if (!flags[0])
			for (int i = 1; i <= 14; i++)
				if (cnt[i] == 4)
				{
					vis[i][1] = vis[i][2] = vis[i][3] = vis[i][4] = false;
					cnt[i] = 0;
					flags[3] = flags[1] = true;
					dfs(k + 1, rem - 4);
					vis[i][1] = vis[i][2] = vis[i][3] = vis[i][4] = true;
					cnt[i] = 4;
				}
		if (true)
			for (int i = 1; i <= 14; i++)
				if (cnt[i] >= 3)
					for (int j = 0; j <= 14; j++)
						if (j != i && cnt[j])
							for (int u = 1; u <= 4; u++)
								if (vis[i][u])
									for (int v = u + 1; v <= 4; v++)
										if (vis[i][v])
											for (int w = v + 1; w <= 4; w++)
												if (vis[i][w])
													for (int x = 1; x <= 4; x++)
														if (vis[j][x])
														{
															vis[i][u] = vis[i][v] = vis[i][w] = vis[j][x] = false;
															cnt[i] -= 3;
															cnt[j]--;
															flags[2] = flags[3] = flags[1] = true;
															dfs(k + 1, rem - 4);
															vis[i][u] = vis[i][v] = vis[i][w] = vis[j][x] = true;
															cnt[i] += 3;
															cnt[j]++;
														}
	}
	if (k + rem / 3 > ans)
		return;
	if (rem >= 3 && !flags[2])
		for (int i = 1; i <= 14; i++)
			if (cnt[i] >= 3)
				for (int u = 1; u <= 4; u++)
					if (vis[i][u])
						for (int v = u + 1; v <= 4; v++)
							if (vis[i][v])
								for (int w = v + 1; w <= 4; w++)
									if (vis[i][w])
									{
										vis[i][u] = vis[i][v] = vis[i][w] = false;
										cnt[i] -= 3;
										flags[3] = flags[1] = true;
										dfs(k + 1, rem - 3);
										vis[i][u] = vis[i][v] = vis[i][w] = true;
										cnt[i] += 3;
									}
	if (k + rem / 2 > ans)
		return;
	if (rem >= 2 && !flags[3])
		for (int i = 0; i <= 14; i++)
			if (cnt[i] >= 2)
				for (int u = 1; u <= 4; u++)
					if (vis[i][u])
						for (int v = u + 1; v <= 4; v++)
							if (vis[i][v])
							{
								vis[i][u] = vis[i][v] = false;
								cnt[i] -= 2;
								flags[1] = true;
								dfs(k + 1, rem - 2);
								vis[i][u] = vis[i][v] = true;
								cnt[i] += 2;
							}
	if (k + rem > ans)
		return;
	if (!flags[1])
		for (int i = 0; i <= 14; i++)
			if (cnt[i])
				for (int j = 1; j <= 4; j++)
					if (vis[i][j])
					{
						vis[i][j] = false;
						cnt[i]--;
						dfs(k + 1, rem - 1);
						vis[i][j] = true;
						cnt[i]++;
					}
}
void singlelist(int k, int cur, int dk, int drem)
{
	if (k > 5)
		dfs(dk, drem);
	if (cur <= 14 && cnt[cur])
		for (int i = 1; i <= 4; i++)
			if (vis[cur][i])
			{
				vis[cur][i] = false;
				cnt[cur]--;
				singlelist(k + 1, cur + 1, dk, drem - 1);
				vis[cur][i] = true;
				cnt[cur]++;
			}
}
void doublelist(int k, int cur, int dk, int drem)
{
	if (k > 3)
		dfs(dk, drem);
	if (cur <= 14 && cnt[cur] >= 2)
		for (int i = 1; i <= 4; i++)
			if (vis[cur][i])
				for (int j = i + 1; j <= 4; j++)
					if (vis[cur][j])
					{
						vis[cur][i] = vis[cur][j] = false;
						cnt[cur] -= 2;
						doublelist(k + 1, cur + 1, dk, drem - 2);
						vis[cur][i] = vis[cur][j] = true;
						cnt[cur] += 2;
					}
}
void trilist(int k, int cur, int dk, int drem)
{
	if (k > 2)
		dfs(dk, drem);
	if (cur <= 14 && cnt[cur] >= 3)
		for (int i = 1; i <= 4; i++)
			if (vis[cur][i])
				for (int j = i + 1; j <= 4; j++)
					if (vis[cur][j])
						for (int t = j + 1; t <= 4; t++)
							if (vis[cur][t])
							{
								vis[cur][i] = vis[cur][j] = vis[cur][t] = false;
								cnt[cur] -= 3;
								trilist(k + 1, cur + 1, dk, drem - 3);
								vis[cur][i] = vis[cur][j] = vis[cur][t] = true;
								cnt[cur] += 3;
							}
}
int main()
{
	int t;
	cin >> t >> n;
	while (t--)
	{
		memset(vis, 0, sizeof(vis));
		memset(cnt, 0, sizeof(cnt));
		for (int i = 1; i <= n; i++)
		{
			int a, b;
			cin >> a >> b;
			if (a == 1)
				a = 14;
			vis[a][b] = true;
			cnt[a]++;
		}
		ans = n;
		cc = 0;
		dfs(1, n);
		cout << ans << endl;
	}
	return 0;
}