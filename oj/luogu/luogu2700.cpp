#include<iostream>
#include<algorithm>
using namespace std;
const int N=100005;
bool enemy[N];
struct edge
{
	int u,v,w;
	bool operator<(const edge& rhs)const
	{
		return w>rhs.w;
	}
}e[N];
int f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	ios::sync_with_stdio(false);
	int n,k;
	cin>>n>>k;
	for(int i=1;i<=k;i++)
	{
		int x;
		cin>>x;
		enemy[x+1]=true;
	}
	long long sum=0;
	for(int i=1;i<n;i++)
	{
		cin>>e[i].u>>e[i].v>>e[i].w;
		e[i].u++;e[i].v++;
		sum+=e[i].w;
	}
	sort(e+1,e+n);
	for(int i=1;i<=n;i++)
		f[i]=i;
	for(int i=1;i<n;i++)
	{
		int ru=getf(e[i].u),rv=getf(e[i].v);
		if(ru!=rv&&(!enemy[ru]||!enemy[rv]))
		{
			f[ru]=rv;
			if(enemy[ru])
				enemy[rv]=true;
			sum-=e[i].w;
		}
	}
	cout<<sum<<endl;
	return 0;
}
