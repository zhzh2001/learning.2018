#include <fstream>
#include <random>
#include <ctime>
using namespace std;
ofstream fout("luogu1967_rope.02.in");
const int n = 10000, m = 50000;
minstd_rand gen(time(NULL));
inline int random(int l, int r)
{
	return uniform_int_distribution<>(l, r)(gen);
}
int main()
{
	fout << n << ' ' << m << endl;
	for (int i = 1; i <= m; i++)
		fout << random(1, n) << ' ' << random(1, n) << ' ' << random(0, 1e5) << endl;
	fout << 0 << endl;
	return 0;
}