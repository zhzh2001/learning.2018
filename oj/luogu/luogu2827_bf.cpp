#include <iostream>
#include <queue>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	int n, m, q, u, v, t;
	cin >> n >> m >> q >> u >> v >> t;
	double p = 1. * u / v;
	priority_queue<int> Q;
	for (int i = 1; i <= n; i++)
	{
		int x;
		cin >> x;
		Q.push(x);
	}
	for (int i = 1; i <= m; i++)
	{
		int now = Q.top() + (i - 1) * q;
		if (i % t == 0)
			cout << now << ' ';
		Q.pop();
		int n1 = int(now * p) - i * q, n2 = now - int(now * p) - i * q;
		Q.push(n1);
		Q.push(n2);
	}
	cout << endl;
	for (int i = 1; i <= n + m; i++)
	{
		if (i % t == 0)
			cout << Q.top() + m * q << ' ';
		Q.pop();
	}
	cout << endl;
	return 0;
}