#include<iostream>
#include<algorithm>
#include<queue>
using namespace std;
const int N=100005,M=200005,K=51,INF=1e9;
typedef pair<int,int> state;
int n,m,k,mod;
bool vis[N];
struct graph
{
	int head[N],v[M],w[M],nxt[M],e,d[N];
	void init()
	{
		fill(head+1,head+n+1,0);
		e=0;
	}
	void add_edge(int u,int v,int w)
	{
		this->v[++e]=v;
		this->w[e]=w;
		nxt[e]=head[u];
		head[u]=e;
	}
	void dijkstra(int src)
	{
		fill(d+1,d+n+1,INF);
		d[src]=0;
		fill(vis+1,vis+n+1,false);
		priority_queue<state,vector<state>,greater<state> > Q;
		Q.push(make_pair(0,src));
		while(!Q.empty())
		{
			state k=Q.top();Q.pop();
			if(vis[k.second])
				continue;
			vis[k.second]=true;
			for(int i=head[k.second];i;i=nxt[i])
				if(!vis[v[i]]&&d[k.second]+w[i]<d[v[i]])
					Q.push(make_pair(d[v[i]]=d[k.second]+w[i],v[i]));
		}
	}
}G,RG;
int Head[N][K],Nxt[M*K],E,ind[N][K],dp[N][K];
state V[M*K];
inline void Add_edge(int u,int uk,int v,int vk)
{
	V[++E]=make_pair(v,vk);
	Nxt[E]=Head[u][uk];
	Head[u][uk]=E;
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin>>t;
	while(t--)
	{
		cin>>n>>m>>k>>mod;
		G.init();RG.init();
		while(m--)
		{
			int u,v,w;
			cin>>u>>v>>w;
			G.add_edge(u,v,w);
			RG.add_edge(v,u,w);
		}
		G.dijkstra(1);
		RG.dijkstra(n);
		for(int i=1;i<=n;i++)
			for(int j=0;j<=k;j++)
				Head[i][j]=dp[i][j]=ind[i][j]=0;
		E=0;
		for(int i=1;i<=n;i++)
			for(int j=G.head[i];j;j=G.nxt[j])
			{
				int delta=G.d[i]+G.w[j]-G.d[G.v[j]];
				for(int t=0;G.d[i]+G.w[j]+t+RG.d[G.v[j]]<=G.d[n]+k;t++)
				{
					Add_edge(i,t,G.v[j],t+delta);
					ind[G.v[j]][t+delta]++;
				}
			}
		dp[1][0]=1;
		queue<state> Q;
		for(int i=1;i<=n;i++)
			for(int j=0;j<=k;j++)
				if(!ind[i][j])
					Q.push(make_pair(i,j));
		int cnt=0;
		while(!Q.empty())
		{
			state k=Q.front();Q.pop();
			cnt++;
			for(int i=Head[k.first][k.second];i;i=Nxt[i])
			{
				if(!--ind[V[i].first][V[i].second])
					Q.push(V[i]);
				(dp[V[i].first][V[i].second]+=dp[k.first][k.second])%=mod;
			}
		}
		if(cnt<n*(k+1))
			cout<<-1<<endl;
		else
		{
			int ans=0;
			for(int i=0;i<=k;i++)
				ans=(ans+dp[n][i])%mod;
			cout<<ans<<endl;
		}
	}
	return 0;
}
