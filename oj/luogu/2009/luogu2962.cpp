#include <iostream>
#include <map>
#include <algorithm>
using namespace std;
const int N = 35, INF = 1e9;
int n, m, ans;
typedef long long state_t;
state_t mask[N];
map<state_t, int> f;
void dfs1(int k, state_t state, int step)
{
	if (k == n / 2)
	{
		if (f.find(state) == f.end() || step < f[state])
			f[state] = step;
	}
	else
	{
		dfs1(k + 1, state, step);
		dfs1(k + 1, state ^ mask[k], step + 1);
	}
}
void dfs2(int k, state_t state, int step)
{
	if (k == n)
	{
		state_t comp = ~state & ((1ll << n) - 1);
		map<state_t, int>::iterator it = f.find(comp);
		if (it != f.end())
			ans = min(ans, it->second + step);
	}
	else
	{
		dfs2(k + 1, state, step);
		dfs2(k + 1, state ^ mask[k], step + 1);
	}
}
int main()
{
	cin >> n >> m;
	for (int i = 0; i < n; i++)
		mask[i] = 1ll << i;
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		u--;
		v--;
		mask[u] |= 1ll << v;
		mask[v] |= 1ll << u;
	}
	dfs1(0, 0ll, 0);
	ans = INF;
	dfs2(n / 2, 0ll, 0);
	cout << ans << endl;
	return 0;
}