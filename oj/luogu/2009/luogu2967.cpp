#include <iostream>
#include <algorithm>
using namespace std;
const int M = 100005, INF = 1e9;
int f[M], g[M];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
	{
		int p, gn;
		cin >> p >> gn;
		for (int j = m; j >= p; j--)
			g[j] = f[j - p];
		for (int j = p - 1; j >= 0; j--)
			g[j] = -INF;
		while (gn--)
		{
			int p, v;
			cin >> p >> v;
			for (int j = m; j >= p; j--)
				g[j] = max(g[j], g[j - p] + v);
		}
		for (int j = 0; j <= m; j++)
			f[j] = max(f[j], g[j]);
	}
	cout << f[m] << endl;
	return 0;
}