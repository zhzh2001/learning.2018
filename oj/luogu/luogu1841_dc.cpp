#include<iostream>
#include<algorithm>
using namespace std;
const int N=205,L=10,INF=1e9;
int n,m,mat[L][N][N],f[N][N];
bool ans[N];
void solve(int id,int l,int r)
{
	if(l==r)
	{
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				if(mat[id-1][i][j]>f[i][j])
					ans[l]=true;
		return;
	}
	int mid=(l+r)/2;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[id][i][j]=mat[id-1][i][j];
	for(int k=l;k<=mid;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				mat[id][i][j]=min(mat[id][i][j],mat[id][i][k]+mat[id][k][j]);
	solve(id+1,mid+1,r);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[id][i][j]=mat[id-1][i][j];
	for(int k=mid+1;k<=r;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				mat[id][i][j]=min(mat[id][i][j],mat[id][i][k]+mat[id][k][j]);
	solve(id+1,l,mid);
}
int main()
{
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[0][i][j]=i==j?0:INF;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[0][u][v]=mat[0][v][u]=w;
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			f[i][j]=mat[0][i][j];
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
	solve(1,1,n);
	bool found=false;
	for(int i=1;i<=n;i++)
		if(ans[i])
		{
			cout<<i<<' ';
			found=true;
		}
	if(found)
		cout<<endl;
	else
		cout<<"No important cities.\n";
	return 0;
}
