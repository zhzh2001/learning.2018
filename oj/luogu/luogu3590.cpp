#include<iostream>
#include<string>
using namespace std;
const int N=1000005,K=3;
int cnt[N][3];
inline bool check(int l,int r)
{
	int a=cnt[r][0]-cnt[l-1][0];
	int b=cnt[r][1]-cnt[l-1][1];
	int c=cnt[r][2]-cnt[l-1][2];
	return a==r-l+1||b==r-l+1||c==r-l+1||(a!=b&&a!=c&&b!=c);
}
int main()
{
	int n;
	cin>>n;
	string s;
	cin>>s;
	s=' '+s;
	for(int i=1;i<=n;i++)
	{
		cnt[i][0]=cnt[i-1][0]+(s[i]=='B');
		cnt[i][1]=cnt[i-1][1]+(s[i]=='C');
		cnt[i][2]=cnt[i-1][2]+(s[i]=='S');
	}
	int ans=0;
	for(int i=1;i<=n&&i<=K;i++)
		for(int j=i;j<=n;j++)
			if(check(i,j))
				ans=max(ans,j-i+1);
	for(int i=n;i&&n-i+1<=K;i--)
		for(int j=1;j<=i;j++)
			if(check(j,i))
				ans=max(ans,i-j+1);
	cout<<ans<<endl;
	return 0;
}
