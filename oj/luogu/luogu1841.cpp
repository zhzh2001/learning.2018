#include<iostream>
using namespace std;
const int N=205,INF=1e9;
int mat[N][N],cont[N][N];
bool ans[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=i==j?0:INF;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u][v]=mat[v][u]=w;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
			{
				if(i==j||i==k||j==k)
					continue;
				if(mat[i][k]+mat[k][j]<mat[i][j])
				{
					mat[i][j]=mat[i][k]+mat[k][j];
					cont[i][j]=k;
				}
				else if(mat[i][k]+mat[k][j]==mat[i][j])
					cont[i][j]=0;
			}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			ans[cont[i][j]]=true;
	bool found=false;
	for(int i=1;i<=n;i++)
		if(ans[i])
		{
			cout<<i<<' ';
			found=true;
		}
	if(found)
		cout<<endl;
	else
		cout<<"No important cities.\n";
	return 0;
}
