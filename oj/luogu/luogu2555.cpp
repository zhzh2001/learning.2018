#include<iostream>
#include<string>
#include<cassert>
using namespace std;
#define fin cin
#define fout cout
const string unit="SBQWSBQYSBQW";
string s;
pair<bool,string> simplify(int l,int r)
{
	bool neg=false;
	int i=l;
	for(;s[i]=='+'||s[i]=='-';i++)
		if(s[i]=='-')
			neg=!neg;
	string ans;
	for(;i<=r&&s[i]=='0';i++);
	if(i>r)
		return make_pair(neg,ans);
	for(;i<=r&&s[i]!='.';i++)
		ans+=s[i];
	if(i>r)
		return make_pair(neg,ans);
	assert(s[i]=='.');
//	for(;s[r]=='0';r--);
	if(s[r]=='.')
		r--;
	for(;i<=r;i++)
		ans+=s[i];
	return make_pair(neg,ans);
}
void read(string num)
{
	if(num[0]=='.')
		fout.put('0');
	int len=num.find('.');
	if(len==string::npos)
		len=num.length();
	bool con0=false;
	for(int i=0;i<len;i++)
		if(num[i]=='0')
		{
			if(!con0)
			{
				bool flag=true;
				int j=i;
				for(;j<len;j++)
				{
					if(num[j]!='0')
						flag=false;
					if(unit[len-j-2]=='W'||unit[len-j-2]=='Y')
						break;
				}
				if(!flag)
					fout.put('0');
				else if(j<len)
				{
					if(unit[len-i-1]!='W'&&unit[len-i-1]!='Y')
						fout.put(unit[len-j-2]);
					con0=false;
					i=j;
					continue;
				}
			}
			con0=true;
		}
		else
		{
			fout.put(num[i]);
			if(i+1<len)
				fout.put(unit[len-i-2]);
			con0=false;
		}
	if(len==num.length())
		return;
	assert(num[len]='.');
	fout.put('D');
	for(int i=len+1;i<num.length();i++)
		fout.put(num[i]);
}
int main()
{
	while(fin>>s)
	{
		if(s.find('/')==string::npos)
			s+="/1";
		pair<bool,string> a=simplify(0,s.find('/')-1);
		pair<bool,string> b=simplify(s.find('/')+1,s.length()-1);
		bool neg=a.first^b.first;
		if(a.second=="0"||a.second=="")
		{
			fout.put('0');
			fout<<endl;
			continue;
		}
		if(neg)
			fout.put('F');
		if(b.second!="1")
		{
			read(b.second);
			fout<<"fz";
		}
		read(a.second);
		fout<<endl;
	}
	return 0;
}
