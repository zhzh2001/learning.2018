#include <cstdio>
#include <cctype>
using namespace std;
const int MOD = 19260817;
int read()
{
	char c = getchar();
	for (; isspace(c); c = getchar())
		;
	int x = 0;
	for (; isdigit(c); c = getchar())
		x = (x * 10 + c - '0') % MOD;
	return x;
}
int qpow(long long a, int b)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % MOD;
		a = a * a % MOD;
	} while (b /= 2);
	return ans;
}
int main()
{
	int a = read(), b = read();
	if (b)
		printf("%d\n", 1ll * a * qpow(b, MOD - 2) % MOD);
	else
		puts("Angry!");
	return 0;
}