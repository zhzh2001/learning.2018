#include <iostream>
#include <algorithm>
using namespace std;
const int N = 200005;
int a[N], id[N], t, ans;
bool ins[N];
void dfs(int k)
{
	id[k] = ++t;
	ins[k] = true;
	if (!id[a[k]])
		dfs(a[k]);
	else if (ins[a[k]])
		ans = min(ans, t + 1 - id[a[k]]);
	ins[k] = false;
}
int main()
{
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	ans = n;
	for (int i = 1; i <= n; i++)
		if (!id[i])
			dfs(i);
	cout << ans << endl;
	return 0;
}