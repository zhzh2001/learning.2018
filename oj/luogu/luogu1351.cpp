#include <iostream>
#include <algorithm>
using namespace std;
const int N = 200005, MOD = 10007;
int w[N], head[N], nxt[N * 2], v[N * 2], e, maxu, sum;
inline void add_edge(int u, int v)
{
	::v[++e] = v;
	nxt[e] = head[u];
	head[u] = e;
}
void dfs(int k, int fat, int fat2)
{
	maxu = max(maxu, w[k] * w[fat2]);
	sum = (sum + w[k] * w[fat2] * 2) % MOD;
	int now = 0, now2 = 0, max1 = 0, max2 = 0;
	for (int i = head[k]; i; i = nxt[i])
		if (v[i] != fat)
		{
			now = (now + w[v[i]]) % MOD;
			now2 = (now2 + w[v[i]] * w[v[i]]) % MOD;
			if (w[v[i]] >= max1)
			{
				max2 = max1;
				max1 = w[v[i]];
			}
			else
				max2 = max(max2, w[v[i]]);
			dfs(v[i], k, fat);
		}
	if (max2)
	{
		sum = (sum + now * now - now2 + MOD) % MOD;
		maxu = max(maxu, max1 * max2);
	}
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i < n; i++)
	{
		int u, v;
		cin >> u >> v;
		add_edge(u, v);
		add_edge(v, u);
	}
	for (int i = 1; i <= n; i++)
		cin >> w[i];
	dfs(1, 0, 0);
	cout << maxu << ' ' << sum << endl;
	return 0;
}