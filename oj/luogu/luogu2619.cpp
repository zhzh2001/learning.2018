#include<iostream>
#include<algorithm>
using namespace std;
const int N=50005,M=100005;
int n,m,k,f[N],sum;
struct edge
{
	int u,v,w,c;
	bool operator<(const edge& rhs)const
	{
		if(w==rhs.w)
			return c<rhs.c;
		return w<rhs.w;
	}
}e[M];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int check(int mid)
{
	for(int i=1;i<=m;i++)
		if(!e[i].c)
			e[i].w+=mid;
	sort(e+1,e+m+1);
	for(int i=1;i<=n;i++)
		f[i]=i;
	int cnt=0,white=0;
	sum=0;
	for(int i=1;i<=m;i++)
		if(getf(e[i].u)!=getf(e[i].v))
		{
			f[getf(e[i].u)]=getf(e[i].v);
			if(!e[i].c)
				white++;
			sum+=e[i].w;
			if(++cnt==n-1)
				break;
		}
	for(int i=1;i<=m;i++)
		if(!e[i].c)
			e[i].w-=mid;
	return white;
}
int main()
{
	ios::sync_with_stdio(false);
	cin>>n>>m>>k;
	for(int i=1;i<=m;i++)
	{
		cin>>e[i].u>>e[i].v>>e[i].w>>e[i].c;
		e[i].u++;e[i].v++;
	}
	int l=-100,r=100,ans;
	while(l<=r)
	{
		int mid=(l+r)/2;
		if(check(mid)>=k)
		{
			ans=mid;
			l=mid+1;
		}
		else
			r=mid-1;
	}
	check(ans);
	cout<<sum-k*ans<<endl;
	return 0;
}
