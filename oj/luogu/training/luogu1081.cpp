#include<iostream>
#include<algorithm>
using namespace std;
const int N=100005,L=18,INF=1e9;
int h[N],rank[N],l[N],r[N],nxt1[N],nxt2[N],nxt[N][L];
long long dis1[N][L],dis2[N][L];
pair<int,int> hi[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>h[i];
		hi[i]=make_pair(h[i],i);
	}
	sort(hi+1,hi+n+1);
	for(int i=1;i<=n;i++)
		rank[hi[i].second]=i;
	for(int i=1;i<=n;i++)
	{
		l[i]=i-1;
		r[i]=i+1;
	}
	for(int i=1;i<=n;i++)
	{
		int idx=rank[i];
		if(l[idx]||r[idx]<=n)
			if(r[idx]==n+1||(l[idx]&&hi[idx].first-hi[l[idx]].first<=hi[r[idx]].first-hi[idx].first))
			{
				nxt1[i]=hi[l[idx]].second;
				if(l[l[idx]]||r[idx]<=n)
					if(r[idx]==n+1||(l[l[idx]]&&hi[idx].first-hi[l[l[idx]]].first<=hi[r[idx]].first-hi[idx].first))
						nxt2[i]=hi[l[l[idx]]].second;
					else
						nxt2[i]=hi[r[idx]].second;
			}
			else
			{
				nxt1[i]=hi[r[idx]].second;
				if(r[r[idx]]<=n||l[idx])
					if(!l[idx]||(r[r[idx]]<=n&&hi[r[r[idx]]].first-hi[idx].first<hi[idx].first-hi[l[idx]].first))
						nxt2[i]=hi[r[r[idx]]].second;
					else
						nxt2[i]=hi[l[idx]].second;
			}
		if(r[idx]<=n)
			l[r[idx]]=l[idx];
		if(l[idx])
			r[l[idx]]=r[idx];
	}
	nxt1[n+1]=nxt2[n+1]=nxt[n+1][0]=n+1;
	for(int i=1;i<=n;i++)
	{
		if(!nxt1[i])
			nxt1[i]=n+1;
		if(!nxt2[i])
			nxt2[i]=n+1;
	}
	for(int i=1;i<=n;i++)
	{
		nxt[i][0]=nxt1[nxt2[i]];
		dis1[i][0]=abs(h[i]-h[nxt2[i]]);
		dis2[i][0]=abs(h[nxt2[i]]-h[nxt1[nxt2[i]]]);
	}
	for(int j=1;j<L;j++)
		for(int i=1;i<=n+1;i++)
		{
			nxt[i][j]=nxt[nxt[i][j-1]][j-1];
			dis1[i][j]=dis1[i][j-1]+dis1[nxt[i][j-1]][j-1];
			dis2[i][j]=dis2[i][j-1]+dis2[nxt[i][j-1]][j-1];
		}
	int x0;
	cin>>x0;
	int s0=0,x1=INF,x2=0;
	for(int i=1;i<=n;i++)
	{
		int now=i,nx1=0,nx2=0;
		for(int j=L-1;j>=0;j--)
			if(nxt[now][j]<=n&&nx1+nx2+dis1[now][j]+dis2[now][j]<=x0)
			{
				nx1+=dis1[now][j];
				nx2+=dis2[now][j];
				now=nxt[now][j];
			}
		if(nxt2[now]<=n&&nx1+nx2+abs(h[now]-h[nxt2[now]])<=x0)
			nx1+=abs(h[now]-h[nxt2[now]]);
		if(!nx1)
			continue;
		if(1ll*x1*nx2>1ll*x2*nx1||(1ll*x1*nx2==1ll*x2*nx1&&h[i]>h[s0]))
		{
			s0=i;
			x1=nx1;x2=nx2;
		}
	}
	cout<<s0<<endl;
	int m;
	cin>>m;
	while(m--)
	{
		int s,x;
		cin>>s>>x;
		int ans1=0,ans2=0;
		for(int i=L-1;i>=0;i--)
			if(nxt[s][i]<=n&&ans1+ans2+dis1[s][i]+dis2[s][i]<=x)
			{
				ans1+=dis1[s][i];
				ans2+=dis2[s][i];
				s=nxt[s][i];
			}
		if(nxt2[s]<=n&&ans1+ans2+abs(h[s]-h[nxt2[s]])<=x)
			ans1+=abs(h[s]-h[nxt2[s]]);
		cout<<ans1<<' '<<ans2<<endl;
	}
	return 0;
}
