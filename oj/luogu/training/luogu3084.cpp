#include<iostream>
#include<algorithm>
using namespace std;
const int N=200005,M=100005,INF=1e9;
int n,m,l[N],r[N],dp[N],tree[N*4];
void build(int id,int l,int r)
{
	tree[id]=-INF;
	if(l<r)
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
	}
}
void modify(int id,int l,int r,int x)
{
	if(l==r)
		tree[id]=dp[x];
	else
	{
		int mid=(l+r)/2;
		if(x<=mid)
			modify(id*2,l,mid,x);
		else
			modify(id*2+1,mid+1,r,x);
		tree[id]=max(tree[id*2],tree[id*2+1]);
	}
}
int query(int id,int l,int r,int L,int R)
{
	if(L<=l&&R>=r)
		return tree[id];
	int mid=(l+r)/2;
	if(R<=mid)
		return query(id*2,l,mid,L,R);
	if(L>mid)
		return query(id*2+1,mid+1,r,L,R);
	return max(query(id*2,l,mid,L,R),query(id*2+1,mid+1,r,L,R));
}
int main()
{
	ios::sync_with_stdio(false);
	cin>>n>>m;
	fill(r,r+n,INF);
	for(int i=1;i<=m;i++)
	{
		int x,y;
		cin>>x>>y;
		l[x]=max(l[x],y);
		r[x-1]=min(r[x-1],y);
	}
	for(int i=1;i<=n;i++)
		l[i]=max(l[i-1],l[i]);
	r[n]=n+1;
	for(int i=n-1;i>=0;i--)
		r[i]=min(r[i+1],r[i]);
	build(1,1,n+1);
	modify(1,1,n+1,n+1);
	for(int i=n;i>=0;i--)
	{
		if(l[i]<r[i])
			dp[i]=query(1,1,n+1,l[i]+1,r[i])+1;
		else
			dp[i]=-INF;
		modify(1,1,n+1,i);
	}
	if(dp[0]>0)
		cout<<dp[0]-1<<endl;
	else
		cout<<-1<<endl;
	return 0;
}
