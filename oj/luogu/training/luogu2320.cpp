#include<iostream>
#include<algorithm>
using namespace std;
const int N=40;
int a[N];
int main()
{
	int n;
	cin>>n;
	if(n==5)
	{
		cout<<"3\n1 1 3\n";
		return 0;
	}
	int cc=0;
	for(int i=1;i<=n;i*=2)
	{
		a[++cc]=i;
		n-=i;
	}
	if(n)
	{
		if(n>1&&__builtin_popcount(n)==1)
		{
			a[++cc]=1;
			a[++cc]=n-1;
		}
		else
			a[++cc]=n;
	}
	sort(a+1,a+cc+1);
	cout<<cc<<endl;
	for(int i=1;i<=cc;i++)
		cout<<a[i]<<' ';
	cout<<endl;
	return 0;
}
