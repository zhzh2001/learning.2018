#include<iostream>
#include<algorithm>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	int l,n;
	cin>>l>>n;
	int ans1=0,ans2=0;
	while(n--)
	{
		int x;
		cin>>x;
		ans1=max(ans1,min(x,l+1-x));
		ans2=max(ans2,max(x,l+1-x));
	}
	cout<<ans1<<' '<<ans2<<endl;
	return 0;
}
