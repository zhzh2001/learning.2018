#include<iostream>
#include<algorithm>
using namespace std;
const int N=55;
int a[N][N],f[N][N][N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			cin>>a[i][j];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			for(int k=1;k<=n;k++)
			{
				if(i+j-k<=0||i+j-k>m)
					continue;
				f[i][j][k]=max(max(f[i-1][j][k-1],f[i-1][j][k]),max(f[i][j-1][k-1],f[i][j-1][k]))+a[i][j]+a[k][i+j-k];
				if(i==k&&j==i+j-k)
					f[i][j][k]-=a[i][j];
			}
	cout<<f[n][m][n]<<endl;
	return 0;
}
