#include<iostream>
#include<string>
using namespace std;
const int N=505;
int n,dp[N],dp2[N];
string s;
int cmp(int l1,int r1,int l2,int r2)
{
	for(;l1<=r1&&s[l1]=='0';l1++);
	for(;l2<=r2&&s[l2]=='0';l2++);
	int len1=r1-l1+1,len2=r2-l2+1;
	if(len1<len2)
		return -1;
	if(len1>len2)
		return 1;
	for(int i=l1,j=l2;i<=r1;i++,j++)
	{
		if(s[i]<s[j])
			return -1;
		if(s[i]>s[j])
			return 1;
	}
	return 0;
}
int main()
{
	cin>>s;
	n=s.length();
	s=' '+s;
	for(int i=1;i<=n;i++)
	{
		dp[i]=1;
		for(int j=i-1;j;j--)
			if(cmp(dp[j],j,j+1,i)<0)
			{
				dp[i]=j+1;
				break;
			}
	}
	dp2[dp[n]]=n;
	int i=dp[n]-1;
	for(;s[i]=='0';i--)
		dp2[i]=n;
	for(;i;i--)
	{
		dp2[i]=i;
		for(int j=dp[n];j>i;j--)
			if(cmp(i,j-1,j,dp2[j])<0)
			{
				dp2[i]=j-1;
				break;
			}
	}
	for(int i=1;i<=n;i=dp2[i],i++)
		cout<<s.substr(i,dp2[i]-i+1)<<(dp2[i]==n?"":",");
	cout<<endl;
	return 0;
}
