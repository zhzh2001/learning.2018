#include<iostream>
#include<algorithm>
using namespace std;
const int N=200005,M=100005;
pair<int,int> photo[M],now[M];
int r[N];
int main()
{
	ios::sync_with_stdio(false);
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=m;i++)
		cin>>photo[i].first>>photo[i].second;
	sort(photo+1,photo+m+1);
	int cc=0;
	for(int i=1;i<=m;i++)
		if(photo[i].second>now[cc].second)
			now[++cc]=photo[i];
	for(int i=1;i<=cc;i++)
		r[now[i].first]=max(now[i].first,now[i].second);
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		if(!r[i])
			continue;
		ans++;
		i=r[i];
	}
	cout<<ans<<endl;
	return 0;
}
