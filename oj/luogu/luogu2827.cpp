#include <iostream>
#include <algorithm>
#include <functional>
using namespace std;
const int N = 100005, M = 7000005;
int a[N];
pair<int, int> q1[M], q2[M];
int main()
{
	ios::sync_with_stdio(false);
	int n, m, q, u, v, t;
	cin >> n >> m >> q >> u >> v >> t;
	double p = 1. * u / v;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	sort(a + 1, a + n + 1, greater<int>());
	int q1l = 1, q1r = 0, q2l = 1, q2r = 0, l = 1;
	for (int i = 1; i <= m; i++)
	{
		int now0 = l <= n ? a[l] + (i - 1) * q : -1;
		int now1 = q1l <= q1r ? q1[q1l].first + (i - q1[q1l].second - 1) * q : -1;
		int now2 = q2l <= q2r ? q2[q2l].first + (i - q2[q2l].second - 1) * q : -1;
		int now = max({now0, now1, now2});
		int n1 = int(now * p), n2 = now - n1;
		q1[++q1r] = make_pair(n1, i);
		q2[++q2r] = make_pair(n2, i);
		if (now == now0)
			l++;
		else if (now == now1)
			q1l++;
		else
			q2l++;
		if (i % t == 0)
			cout << now << ' ';
	}
	cout << endl;
	for (int i = 1; i <= n + m; i++)
	{
		int now0 = l <= n ? a[l] + m * q : -1;
		int now1 = q1l <= q1r ? q1[q1l].first + (m - q1[q1l].second) * q : -1;
		int now2 = q2l <= q2r ? q2[q2l].first + (m - q2[q2l].second) * q : -1;
		int now = max({now0, now1, now2});
		if (now == now0)
			l++;
		else if (now == now1)
			q1l++;
		else
			q2l++;
		if (i % t == 0)
			cout << now << ' ';
	}
	cout << endl;
	return 0;
}