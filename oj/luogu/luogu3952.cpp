#include<iostream>
#include<string>
#include<algorithm>
#include<cctype>
using namespace std;
const int L=105,A=26;
string code[L];
bool var[A];
int s[L];
bool compile(int loc)
{
	int open=0;
	fill(var,var+A,false);
	for(int i=1;i<=loc;i++)
		if(code[i][0]=='F')
		{
			s[++open]=code[i][2]-'a';
			if(var[code[i][2]-'a'])
				return true;
			var[code[i][2]-'a']=true;
		}
		else
		{
			var[s[open]]=false;
			if(--open<0)
				return true;
		}
	return open;
}
bool check(int loc,string cpl)
{
	int mx=0,sp=0,now=0,con=0;
	for(int i=1;i<=loc;i++)
		if(code[i][0]=='F')
		{
			int l=0,r=0,nxt;
			if(code[i][4]=='n')
			{
				l=1000;
				nxt=6;
			}
			else
			{
				for(nxt=4;isdigit(code[i][nxt]);nxt++)
					l=l*10+code[i][nxt]-'0';
				nxt++;
			}
			if(code[i][nxt]=='n')
				r=1000;
			else
				for(;nxt<code[i].length()&&isdigit(code[i][nxt]);nxt++)
					r=r*10+code[i][nxt]-'0';
			if(r-l>500)
			{
				s[++sp]=1;
				now++;
			}
			else if(l<=r)
				s[++sp]=0;
			else
			{
				s[++sp]=-1;
				con++;
			}
			if(!con)
				mx=max(mx,now);
		}
		else
		{
			if(s[sp]>=0)
				now-=s[sp];
			else
				con--;
			sp--;
		}
	if(cpl=="O(1)"&&mx==0)
		return true;
	int out=0;
	for(int i=4;isdigit(cpl[i]);i++)
		out=out*10+cpl[i]-'0';
	return mx==out;
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin>>t;
	while(t--)
	{
		int loc;
		string cpl;
		cin>>loc>>cpl;
		for(int i=0;i<=loc;i++)
			getline(cin,code[i]);
		if(compile(loc))
			cout<<"ERR\n";
		else if(check(loc,cpl))
			cout<<"Yes\n";
		else
			cout<<"No\n";
	}
	return 0;
}
