#include <iostream>
#include <algorithm>
using namespace std;
const int N = 10005, M = 50005, NODES = 5e6;
struct edge
{
	int u, v, w;
	bool operator<(const edge &rhs) const
	{
		return w > rhs.w;
	}
} e[M];
typedef int ptr;
struct node
{
	int ls, rs;
} tree[NODES];
ptr root[M];
int n, m, cnt, sz[N];
void build(ptr &id, int l, int r)
{
	id = ++cnt;
	if (l == r)
		tree[id].ls = l;
	else
	{
		int mid = (l + r) / 2;
		build(tree[id].ls, l, mid);
		build(tree[id].rs, mid + 1, r);
	}
}
void modify(ptr &id, ptr rt, int l, int r, int x, int val)
{
	id = ++cnt;
	if (l == r)
		tree[id].ls = val;
	else
	{
		int mid = (l + r) / 2;
		if (x <= mid)
		{
			modify(tree[id].ls, tree[rt].ls, l, mid, x, val);
			tree[id].rs = tree[rt].rs;
		}
		else
		{
			modify(tree[id].rs, tree[rt].rs, mid + 1, r, x, val);
			tree[id].ls = tree[rt].ls;
		}
	}
}
int query(ptr id, int l, int r, int x)
{
	if (l == r)
		return tree[id].ls;
	int mid = (l + r) / 2;
	if (x <= mid)
		return query(tree[id].ls, l, mid, x);
	return query(tree[id].rs, mid + 1, r, x);
}
int getf(ptr &id, int x)
{
	int fat = query(id, 1, n, x);
	if (fat == x)
		return x;
	int rt = getf(id, fat);
	modify(id, id, 1, n, x, rt);
	return rt;
}
int getf_readonly(ptr id, int x)
{
	int fat = query(id, 1, n, x);
	if (fat == x)
		return x;
	return getf_readonly(id, fat);
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> m;
	for (int i = 1; i <= m; i++)
		cin >> e[i].u >> e[i].v >> e[i].w;
	sort(e + 1, e + m + 1);
	build(root[0], 1, n);
	fill(sz + 1, sz + n + 1, 1);
	for (int i = 1; i <= m; i++)
	{
		root[i] = root[i - 1];
		int ru = getf_readonly(root[i], e[i].u), rv = getf_readonly(root[i], e[i].v);
		if (ru != rv)
		{
			if (sz[ru] > sz[rv])
			{
				modify(root[i], root[i], 1, n, rv, ru);
				sz[ru] += sz[rv];
			}
			else
			{
				modify(root[i], root[i], 1, n, ru, rv);
				sz[rv] += sz[ru];
			}
		}
	}
	int q;
	cin >> q;
	while (q--)
	{
		int u, v;
		cin >> u >> v;
		int l = 1, r = m;
		while (l < r)
		{
			int mid = (l + r) / 2;
			if (getf_readonly(root[mid], u) == getf_readonly(root[mid], v))
				r = mid;
			else
				l = mid + 1;
		}
		if (getf_readonly(root[l], u) != getf_readonly(root[l], v))
			cout << -1 << endl;
		else
			cout << e[l].w << endl;
	}
	return 0;
}