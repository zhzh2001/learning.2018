#include<iostream>
#include<algorithm>
using namespace std;
const int N=55,L=35;
bool f[N][N][N];
int d[N][N];
int main()
{
	ios::sync_with_stdio(false);
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			d[i][j]=i==j?0:L;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		f[0][u][v]=true;
		d[u][v]=1;
	}
	for(int i=1;i<L;i++)
		for(int j=1;j<=n;j++)
			for(int k=1;k<=n;k++)
			{
				for(int t=1;t<=n;t++)
					f[i][j][k]|=f[i-1][j][t]&&f[i-1][t][k];
				if(f[i][j][k]&&j!=k)
					d[j][k]=1;
			}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				d[i][j]=min(d[i][j],d[i][k]+d[k][j]);
	cout<<d[1][n]<<endl;
	return 0;
}
