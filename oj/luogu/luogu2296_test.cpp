#include <iostream>
#include <queue>
using namespace std;
const int N = 10005, M = 200005;
int head[N], v[M], nxt[M], e, rhead[N], rv[M], rnxt[M], re, indeg[N];
inline void add_edge(int u, int v)
{
	::v[++e] = v;
	nxt[e] = head[u];
	head[u] = e;
}
inline void radd_edge(int u, int v)
{
	::rv[++re] = v;
	rnxt[re] = rhead[u];
	rhead[u] = re;
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		add_edge(u, v);
		radd_edge(v, u);
		indeg[u]++;
	}
	int s, t;
	cin >> s >> t;
	queue<int> Q;

	return 0;
}