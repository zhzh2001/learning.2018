#include<iostream>
#include<cstring>
using namespace std;
const int m=8,MOD=1000;
struct matrix
{
	int mat[m][m];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int i=0;i<m;i++)
			for(int j=0;j<m;j++)
				for(int k=0;k<m;k++)
					ans.mat[i][j]=(ans.mat[i][j]+mat[i][k]*rhs.mat[k][j])%MOD;
		return ans;
	}
	matrix operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
};
matrix I()
{
	matrix ans;
	for(int i=0;i<m;i++)
		ans.mat[i][i]=1;
	return ans;
}
matrix qpow(matrix a,int b)
{
	matrix ans=I();
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}while(b/=2);
	return ans;
}
int main()
{
	int n;
	cin>>n;
	matrix trans;
	for(int i=0;i<m;i++)
	{
		int pred=(i-1+m)%m,nxt=(i+1)%m;
		if(pred!=4)
			trans.mat[i][pred]=1;
		if(nxt!=4)
			trans.mat[i][nxt]=1;
	}
	matrix init;
	init.mat[0][0]=1;
	cout<<(qpow(trans,n)*init).mat[4][0]<<endl;
	return 0;
}
