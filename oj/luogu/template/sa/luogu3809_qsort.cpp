#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
const int N = 1000005;
struct node
{
	int x, y, id;
	bool operator<(const node &rhs) const
	{
		if (x == rhs.x)
			return y < rhs.y;
		return x < rhs.x;
	}
} a[N];
int rk[N], sa[N];
int main()
{
	ios::sync_with_stdio(false);
	string s;
	cin >> s;
	int n = s.length();
	s = ' ' + s;
	for (int i = 1; i <= n; i++)
		rk[i] = s[i];
	for (int i = 1; i <= n; i *= 2)
	{
		for (int j = 1; j <= n; j++)
		{
			a[j].id = j;
			a[j].x = rk[j];
			a[j].y = j + i <= n ? rk[j + i] : 0;
		}
		sort(a + 1, a + n + 1);
		int cnt = 0;
		for (int j = 1; j <= n; j++)
		{
			cnt += a[j].x != a[j - 1].x || a[j].y != a[j - 1].y;
			rk[a[j].id] = cnt;
		}
	}
	for (int i = 1; i <= n; i++)
		sa[rk[i]] = i;
	for (int i = 1; i <= n; i++)
		cout << sa[i] << ' ';
	cout << endl;
	return 0;
}