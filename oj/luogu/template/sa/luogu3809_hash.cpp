#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
const int N = 1000005, B = 233;
typedef unsigned hash_t;
hash_t h[N], p[N];
int sa[N];
int main()
{
	ios::sync_with_stdio(false);
	string s;
	cin >> s;
	int n = s.length();
	s = ' ' + s;
	p[0] = 1;
	for (int i = 1; i <= n; i++)
	{
		h[i] = h[i - 1] * B + s[i];
		p[i] = p[i - 1] * B;
		sa[i] = i;
	}
	sort(sa + 1, sa + n + 1, [=](int lhs, int rhs) {
		int l = 0, r = max(n - lhs + 1, n - rhs + 1), ans = 0;
		while (l <= r)
		{
			int mid = (l + r) / 2;
			if (h[lhs + mid - 1] - h[lhs - 1] * p[mid] == h[rhs + mid - 1] - h[rhs - 1] * p[mid])
			{
				ans = mid;
				l = mid + 1;
			}
			else
				r = mid - 1;
		}
		return s[lhs + ans] < s[rhs + ans];
	});
	for (int i = 1; i <= n; i++)
		cout << sa[i] << ' ';
	cout << endl;
	return 0;
}