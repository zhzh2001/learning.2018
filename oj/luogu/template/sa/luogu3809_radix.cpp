#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
const int N = 1000005;
int rk[N], sa[N], tp[N], n, m = 127, bucket[N];
void radixsort()
{
	fill(bucket, bucket + m + 1, 0);
	for (int i = 1; i <= n; i++)
		bucket[rk[i]]++;
	for (int i = 1; i <= m; i++)
		bucket[i] += bucket[i - 1];
	for (int i = n; i; i--)
		sa[bucket[rk[tp[i]]]--] = tp[i];
}
int main()
{
	ios::sync_with_stdio(false);
	string s;
	cin >> s;
	n = s.length();
	s = ' ' + s;
	for (int i = 1; i <= n; i++)
		rk[i] = s[i], tp[i] = i;
	radixsort();
	for (int i = 1, cc = 0; cc < n; m = cc, i *= 2)
	{
		cc = 0;
		for (int j = 1; j <= i; j++)
			tp[++cc] = n - i + j;
		for (int j = 1; j <= n; j++)
			if (sa[j] > i)
				tp[++cc] = sa[j] - i;
		radixsort();
		swap(rk, tp);
		cc = 0;
		for (int j = 1; j <= n; j++)
		{
			cc += tp[sa[j]] != tp[sa[j - 1]] || tp[sa[j] + i] != tp[sa[j - 1] + i];
			rk[sa[j]] = cc;
		}
	}
	for (int i = 1; i <= n; i++)
		cout << sa[i] << ' ';
	cout << endl;
	return 0;
}