#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("hayfeast.in");
ofstream fout("hayfeast.out");
const int N = 100005;
int f[N], s[N];
int main()
{
	int n;
	long long m;
	fin >> n >> m;
	for (int i = 1; i <= n; i++)
		fin >> f[i] >> s[i];
	s[++n] = 1e9 + 7;
	int l = 1, r = *max_element(s + 1, s + n + 1);
	while (l < r)
	{
		int mid = (l + r) / 2;
		long long now = 0, maxv = 0;
		for (int i = 1; i <= n; i++)
			if (s[i] <= mid)
				now += f[i];
			else
			{
				maxv = max(maxv, now);
				now = 0;
			}
		if (maxv >= m)
			r = mid;
		else
			l = mid + 1;
	}
	fout << r << endl;
	return 0;
}