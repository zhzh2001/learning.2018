#include <fstream>
#include <set>
#include <queue>
#include <utility>
using namespace std;
ifstream fin("piepie.in");
ofstream fout("piepie.out");
const int N = 100005;
bool vis[N * 2];
int ans[N];
pair<int, int> pie[N * 2];
int main()
{
	int n, d;
	fin >> n >> d;
	set<pair<int, int>> bes, els;
	for (int i = 1; i <= n * 2; i++)
	{
		fin >> pie[i].first >> pie[i].second;
		if (i <= n)
			bes.insert(make_pair(pie[i].second, i));
		else
			els.insert(make_pair(pie[i].first, i));
	}
	queue<pair<int, int>> Q;
	for (int i = 1; i <= n; i++)
		if (!pie[i].second)
		{
			Q.push(make_pair(i, 1));
			vis[i] = true;
		}
	for (int i = n + 1; i <= n * 2; i++)
		if (!pie[i].first)
		{
			Q.push(make_pair(i, 1));
			vis[i] = true;
		}
	while (!Q.empty())
	{
		pair<int, int> k = Q.front();
		Q.pop();
		if (k.first <= n)
		{
			ans[k.first] = k.second;
			for (auto it = els.upper_bound(make_pair(pie[k.first].first + 1, 0)); it == els.end() || pie[k.first].first - it->first <= d; --it)
			{
				if (it != els.end() && pie[k.first].first >= it->first && !vis[it->second])
				{
					Q.push(make_pair(it->second, k.second + 1));
					vis[it->second] = true;
				}
				if (it == els.begin())
					break;
			}
		}
		else
		{
			for (auto it = bes.upper_bound(make_pair(pie[k.first].second + 1, 0)); it == bes.end() || pie[k.first].second - it->first <= d; --it)
			{
				if (it != bes.end() && pie[k.first].second >= it->first && !vis[it->second])
				{
					Q.push(make_pair(it->second, k.second + 1));
					vis[it->second] = true;
				}
				if (it == bes.begin())
					break;
			}
		}
	}
	for (int i = 1; i <= n; i++)
		fout << (ans[i] ? ans[i] : -1) << endl;
	return 0;
}