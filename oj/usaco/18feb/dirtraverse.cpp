#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;
ifstream fin("dirtraverse.in");
ofstream fout("dirtraverse.out");
const int N = 100005;
vector<int> mat[N];
int a[N], leaf, into[N], outo[N], len[N];
long long ans;
struct node
{
	long long sum;
	int lazy;
} tree[N * 4];
void build(int id, int l, int r)
{
	if (l == r)
		tree[id].sum = len[l];
	else
	{
		int mid = (l + r) / 2;
		build(id * 2, l, mid);
		build(id * 2 + 1, mid + 1, r);
		tree[id].sum = tree[id * 2].sum + tree[id * 2 + 1].sum;
	}
}
inline void pushdown(int id, int l, int r)
{
	if (tree[id].lazy)
	{
		int mid = (l + r) / 2;
		tree[id * 2].sum += 1ll * (mid - l + 1) * tree[id].lazy;
		tree[id * 2].lazy += tree[id].lazy;
		tree[id * 2 + 1].sum += 1ll * (r - mid) * tree[id].lazy;
		tree[id * 2 + 1].lazy += tree[id].lazy;
		tree[id].lazy = 0;
	}
}
void modify(int id, int l, int r, int L, int R, int val)
{
	if (L <= l && R >= r)
	{
		tree[id].sum += 1ll * (r - l + 1) * val;
		tree[id].lazy += val;
	}
	else
	{
		pushdown(id, l, r);
		int mid = (l + r) / 2;
		if (L <= mid)
			modify(id * 2, l, mid, L, R, val);
		if (R > mid)
			modify(id * 2 + 1, mid + 1, r, L, R, val);
		tree[id].sum = tree[id * 2].sum + tree[id * 2 + 1].sum;
	}
}
void dfs(int k, int now)
{
	if (!mat[k].size())
		len[++leaf] = now;
	else
	{
		into[k] = leaf + 1;
		for (int i = 0; i < mat[k].size(); i++)
			dfs(mat[k][i], now + a[mat[k][i]] + (bool)mat[mat[k][i]].size());
		outo[k] = leaf;
	}
}
void dfs(int k)
{
	ans = min(ans, tree[1].sum);
	for (int i = 0; i < mat[k].size(); i++)
		if (mat[mat[k][i]].size())
		{
			if (into[mat[k][i]] > 1)
				modify(1, 1, leaf, 1, into[mat[k][i]] - 1, 3);
			modify(1, 1, leaf, into[mat[k][i]], outo[mat[k][i]], -a[mat[k][i]] - 1);
			if (outo[mat[k][i]] < leaf)
				modify(1, 1, leaf, outo[mat[k][i]] + 1, leaf, 3);
			dfs(mat[k][i]);
			if (into[mat[k][i]] > 1)
				modify(1, 1, leaf, 1, into[mat[k][i]] - 1, -3);
			modify(1, 1, leaf, into[mat[k][i]], outo[mat[k][i]], a[mat[k][i]] + 1);
			if (outo[mat[k][i]] < leaf)
				modify(1, 1, leaf, outo[mat[k][i]] + 1, leaf, -3);
		}
}
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
	{
		string name;
		fin >> name;
		a[i] = name.length();
		int k;
		fin >> k;
		while (k--)
		{
			int v;
			fin >> v;
			mat[i].push_back(v);
		}
	}
	dfs(1, 0);
	build(1, 1, leaf);
	ans = tree[1].sum;
	dfs(1);
	fout << ans << endl;
	return 0;
}