#include <fstream>
#include <algorithm>
#include <set>
using namespace std;
ifstream fin("snowboots.in");
ofstream fout("snowboots.out");
const int N = 100005;
pair<int, int> snow[N];
struct boot_t
{
	int dep, step, id;
	bool operator<(const boot_t &rhs) const
	{
		return dep < rhs.dep;
	}
} boot[N];
bool ans[N];
int main()
{
	int n, m;
	fin >> n >> m;
	for (int i = 1; i <= n; i++)
	{
		fin >> snow[i].first;
		snow[i].second = i;
	}
	sort(snow + 2, snow + n);
	for (int i = 1; i <= m; i++)
	{
		fin >> boot[i].dep >> boot[i].step;
		boot[i].id = i;
	}
	sort(boot + 1, boot + m + 1);
	multiset<int> mn;
	mn.insert(n - 1);
	set<int> reach;
	reach.insert(1);
	reach.insert(n);
	for (int i = 1, j = 2; i <= m; i++)
	{
		for (; j < n && snow[j].first <= boot[i].dep; j++)
		{
			set<int>::iterator it1 = reach.insert(snow[j].second).first, it2 = it1;
			mn.erase(mn.find(*++it2 - *--it1));
			mn.insert(snow[j].second - *it1);
			mn.insert(*it2 - snow[j].second);
		}
		ans[boot[i].id] = boot[i].step >= *mn.rbegin();
	}
	for (int i = 1; i <= m; i++)
		fout << ans[i] << endl;
	return 0;
}