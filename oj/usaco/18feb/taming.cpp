#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("taming.in");
ofstream fout("taming.out");
const int N = 105, INF = 1e9;
int a[N], f[N][N][N];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	fill_n(&f[0][0][0], sizeof(f) / sizeof(int), INF);
	f[1][1][0] = (bool)a[1];
	for (int i = 1; i < n; i++)
		for (int j = 1; j <= i; j++)
			for (int k = 0; k < i; k++)
				if (f[i][j][k] < INF)
				{
					f[i + 1][j][k + 1] = min(f[i + 1][j][k + 1], f[i][j][k] + (a[i + 1] != k + 1));
					f[i + 1][j + 1][0] = min(f[i + 1][j + 1][0], f[i][j][k] + (bool)a[i + 1]);
				}
	for (int i = 1; i <= n; i++)
		fout << *min_element(f[n][i], f[n][i] + n) << endl;
	return 0;
}