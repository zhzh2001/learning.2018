#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
pair<int, int> a[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		cin >> a[i].first;
		a[i].second = i;
	}
	sort(a + 1, a + n + 1);
	int ans = 0;
	for (int i = 1; i <= n; i++)
		ans = max(ans, a[i].second - i + 1);
	cout << ans << endl;
	return 0;
}