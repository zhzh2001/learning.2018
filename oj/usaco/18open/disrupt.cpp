#include <iostream>
#include <algorithm>
using namespace std;
const int N = 50005, INF = 0x3f3f3f3f;
int n, m, head[N], v[N * 2], nxt[N * 2], e;
inline void addedge(int u, int v)
{
	::v[++e] = v;
	nxt[e] = head[u];
	head[u] = e;
}
int sz[N], f[N], dep[N], son[N], top[N], id[N], rid[N], tim;
void dfs(int k)
{
	sz[k] = 1;
	for (int i = head[k]; i; i = nxt[i])
		if (v[i] != f[k])
		{
			dep[v[i]] = dep[k] + 1;
			f[v[i]] = k;
			rid[(i + 1) / 2] = v[i];
			dfs(v[i]);
			if (sz[v[i]] > sz[son[k]])
				son[k] = v[i];
			sz[k] += sz[v[i]];
		}
}
void dfs(int k, int anc)
{
	id[k] = ++tim;
	top[k] = anc;
	if (son[k])
		dfs(son[k], anc);
	for (int i = head[k]; i; i = nxt[i])
		if (v[i] != f[k] && v[i] != son[k])
			dfs(v[i], v[i]);
}
int tree[N * 4];
void build(int id, int l, int r)
{
	tree[id] = INF;
	if (l < r)
	{
		int mid = (l + r) / 2;
		build(id * 2, l, mid);
		build(id * 2 + 1, mid + 1, r);
	}
}
inline void pushdown(int id)
{
	tree[id * 2] = min(tree[id * 2], tree[id]);
	tree[id * 2 + 1] = min(tree[id * 2 + 1], tree[id]);
}
void modify(int id, int l, int r, int L, int R, int val)
{
	if (L <= l && R >= r)
		tree[id] = min(tree[id], val);
	else
	{
		pushdown(id);
		int mid = (l + r) / 2;
		if (L <= mid)
			modify(id * 2, l, mid, L, R, val);
		if (R > mid)
			modify(id * 2 + 1, mid + 1, r, L, R, val);
	}
}
void modify(int u, int v, int w)
{
	while (top[u] != top[v])
	{
		if (dep[top[u]] < dep[top[v]])
			swap(u, v);
		modify(1, 1, n, id[top[u]], id[u], w);
		u = f[top[u]];
	}
	u = id[u];
	v = id[v];
	if (u > v)
		swap(u, v);
	if (u < v)
		modify(1, 1, n, u + 1, v, w);
}
int query(int id, int l, int r, int x)
{
	if (l == r)
		return tree[id];
	pushdown(id);
	int mid = (l + r) / 2;
	if (x <= mid)
		return query(id * 2, l, mid, x);
	return query(id * 2 + 1, mid + 1, r, x);
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> m;
	for (int i = 1; i < n; i++)
	{
		int u, v;
		cin >> u >> v;
		addedge(u, v);
		addedge(v, u);
	}
	dfs(1);
	dfs(1, 1);
	build(1, 1, n);
	while (m--)
	{
		int u, v, w;
		cin >> u >> v >> w;
		modify(u, v, w);
	}
	for (int i = 1; i < n; i++)
	{
		int ans = query(1, 1, n, id[rid[i]]);
		if (ans == INF)
			ans = -1;
		cout << ans << endl;
	}
	return 0;
}