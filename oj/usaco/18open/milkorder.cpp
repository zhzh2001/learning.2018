#include <iostream>
#include <vector>
#include <queue>
#include <functional>
#include <algorithm>
using namespace std;
const int N = 100005;
vector<int> a[N], mat[N];
int ind[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= m; i++)
	{
		int k;
		cin >> k;
		while (k--)
		{
			int x;
			cin >> x;
			a[i].push_back(x);
		}
	}
	int l = 0, r = m, ans = 0;
	while (l <= r)
	{
		int mid = (l + r) / 2;
		for (int i = 1; i <= n; i++)
		{
			mat[i].clear();
			ind[i] = 0;
		}
		for (int i = 1; i <= mid; i++)
			for (int j = 0; j < a[i].size() - 1; j++)
			{
				mat[a[i][j]].push_back(a[i][j + 1]);
				ind[a[i][j + 1]]++;
			}
		queue<int> Q;
		for (int i = 1; i <= n; i++)
			if (!ind[i])
				Q.push(i);
		int cc = 0;
		while (!Q.empty())
		{
			int k = Q.front();
			Q.pop();
			++cc;
			for (int v : mat[k])
				if (!--ind[v])
					Q.push(v);
		}
		if (cc == n)
		{
			ans = mid;
			l = mid + 1;
		}
		else
			r = mid - 1;
	}
	for (int i = 1; i <= n; i++)
	{
		mat[i].clear();
		ind[i] = 0;
	}
	for (int i = 1; i <= ans; i++)
		for (int j = 0; j < a[i].size() - 1; j++)
		{
			mat[a[i][j]].push_back(a[i][j + 1]);
			ind[a[i][j + 1]]++;
		}
	priority_queue<int, vector<int>, greater<int>> Q;
	for (int i = 1; i <= n; i++)
		if (!ind[i])
			Q.push(i);
	while (!Q.empty())
	{
		int k = Q.top();
		Q.pop();
		cout << k << ' ';
		for (int v : mat[k])
			if (!--ind[v])
				Q.push(v);
	}
	cout << endl;
	return 0;
}