#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
using namespace std;
const int N = 255, M = 1e6 + 5, dx[] = {-1, 1, 0, 0}, dy[] = {0, 0, -1, 1};
int n, a[N][N], belong[N][N], cc, vis[M], vis2[N * N + 5], tim, ans;
vector<pair<int, int>> compont[N * N + 5];
vector<int> mat[N * N + 5];
set<pair<int, int>> S;
void dfs(int x, int y)
{
	belong[x][y] = cc;
	compont[cc].push_back(make_pair(x, y));
	for (int i = 0; i < 4; i++)
	{
		int nx = x + dx[i], ny = y + dy[i];
		if (nx && nx <= n && ny && ny <= n && a[nx][ny] == a[x][y] && !belong[nx][ny])
			dfs(nx, ny);
	}
}
void dfs(int k, int cow1, int cow2)
{
	ans += compont[k].size();
	vis2[k] = tim;
	for (int v : mat[k])
		if ((a[compont[v][0].first][compont[v][0].second] == cow1 || a[compont[v][0].first][compont[v][0].second] == cow2) && vis2[v] < tim && S.find(make_pair(k, v)) == S.end())
		{
			S.insert(make_pair(k, v));
			S.insert(make_pair(v, k));
			dfs(v, cow1, cow2);
		}
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			cin >> a[i][j];
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			if (!belong[i][j])
			{
				cc++;
				dfs(i, j);
			}
	int ans = 0;
	for (int i = 1; i <= cc; i++)
		ans = max(ans, (int)compont[i].size());
	cout << ans << endl;
	for (int i = 1; i <= cc; i++)
	{
		vis[i] = i;
		for (auto p : compont[i])
			for (int j = 0; j < 4; j++)
			{
				int nx = p.first + dx[j], ny = p.second + dy[j];
				if (nx && nx <= n && ny && ny <= n && vis[belong[nx][ny]] < i)
				{
					mat[i].push_back(belong[nx][ny]);
					vis[belong[nx][ny]] = i;
				}
			}
	}
	ans = 0;
	fill(vis + 1, vis + cc + 1, 0);
	for (int i = 1; i <= cc; i++)
	{
		vis[a[compont[i][0].first][compont[i][0].second]] = i;
		for (int v : mat[i])
			if (vis[a[compont[v][0].first][compont[v][0].second]] < i)
			{
				++tim;
				::ans = 0;
				dfs(i, a[compont[i][0].first][compont[i][0].second], a[compont[v][0].first][compont[v][0].second]);
				ans = max(ans, ::ans);
				vis[a[compont[v][0].first][compont[v][0].second]] = i;
			}
	}
	cout << ans << endl;
	return 0;
}