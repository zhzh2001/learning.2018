#include <fstream>
#include <algorithm>
#include <set>
using namespace std;
ifstream fin("lifeguards.in");
ofstream fout("lifeguards.out");
const int N = 100005;
pair<int, int> event[N * 2];
int alone[N];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
	{
		int l, r;
		fin >> l >> r;
		event[i * 2 - 1] = make_pair(l, i);
		event[i * 2] = make_pair(r, -i);
	}
	sort(event + 1, event + n * 2 + 1);
	set<int> S;
	int cover = 0;
	for (int i = 1; i <= n * 2; i++)
	{
		if (S.size() == 1)
			alone[*S.begin()] += event[i].first - event[i - 1].first;
		if (!S.empty())
			cover += event[i].first - event[i - 1].first;
		if (event[i].second > 0)
			S.insert(event[i].second);
		else
			S.erase(-event[i].second);
	}
	fout << cover - *min_element(alone + 1, alone + n + 1) << endl;
	return 0;
}