#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("mootube.in");
ofstream fout("mootube.out");
const int N = 100005;
struct node
{
	int u, v, w;
	bool operator<(const node &rhs) const
	{
		return w > rhs.w;
	}
} e[N], q[N];
int f[N], sz[N], ans[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	int n, m;
	fin >> n >> m;
	for (int i = 1; i < n; i++)
		fin >> e[i].u >> e[i].v >> e[i].w;
	sort(e + 1, e + n);
	for (int i = 1; i <= m; i++)
	{
		fin >> q[i].w >> q[i].u;
		q[i].v = i;
	}
	sort(q + 1, q + m + 1);
	for (int i = 1; i <= n; i++)
	{
		f[i] = i;
		sz[i] = 1;
	}
	for (int i = 1, j = 1; i <= m; i++)
	{
		for (; j < n && e[j].w >= q[i].w; j++)
		{
			int ru = getf(e[j].u), rv = getf(e[j].v);
			if (ru != rv)
			{
				f[ru] = rv;
				sz[rv] += sz[ru];
			}
		}
		ans[q[i].v] = sz[getf(q[i].u)] - 1;
	}
	for (int i = 1; i <= m; i++)
		fout << ans[i] << endl;
	return 0;
}