#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("lifeguards.in");
ofstream fout("lifeguards.out");
const int N = 100005;
int l[N], r[N], x[N * 2];
struct node
{
	int val, sum;
} tree[N * 8];
inline void pullup(int id, int l, int r)
{
	if (tree[id].val)
		tree[id].sum = x[r + 1] - x[l];
	else if (l == r)
		tree[id].sum = 0;
	else
		tree[id].sum = tree[id * 2].sum + tree[id * 2 + 1].sum;
}
void modify(int id, int l, int r, int L, int R, int val)
{
	if (L <= l && R >= r)
	{
		tree[id].val += val;
		pullup(id, l, r);
	}
	else
	{
		int mid = (l + r) / 2;
		if (L <= mid)
			modify(id * 2, l, mid, L, R, val);
		if (R > mid)
			modify(id * 2 + 1, mid + 1, r, L, R, val);
		pullup(id, l, r);
	}
}
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
	{
		fin >> l[i] >> r[i];
		x[i * 2 - 1] = l[i];
		x[i * 2] = r[i];
	}
	sort(x + 1, x + n * 2 + 1);
	for (int i = 1; i <= n; i++)
	{
		l[i] = lower_bound(x + 1, x + n * 2 + 1, l[i]) - x;
		r[i] = lower_bound(x + 1, x + n * 2 + 1, r[i]) - x;
		modify(1, 1, n * 2 - 1, l[i], r[i] - 1, 1);
	}
	int ans = 0;
	for (int i = 1; i <= n; i++)
	{
		modify(1, 1, n * 2 - 1, l[i], r[i] - 1, -1);
		ans = max(ans, tree[1].sum);
		modify(1, 1, n * 2 - 1, l[i], r[i] - 1, 1);
	}
	fout << ans << endl;
	return 0;
}