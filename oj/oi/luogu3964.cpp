#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int x[N], y[N];
long long ans[N];
pair<int, int> tmp[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	long long sumx = 0, sumy = 0;
	for (int i = 1; i <= n; i++)
	{
		int x, y;
		cin >> x >> y;
		::x[i] = x + y;
		sumx += ::x[i];
		::y[i] = x - y;
		sumy += ::y[i];
	}
	for (int i = 1; i <= n; i++)
		tmp[i] = make_pair(x[i], i);
	sort(tmp + 1, tmp + n + 1);
	long long sum = 0;
	for (int i = 1; i <= n; i++)
	{
		sum += tmp[i].first;
		ans[tmp[i].second] += 1ll * i * tmp[i].first - sum + sumx - sum - 1ll * (n - i) * tmp[i].first;
	}
	for (int i = 1; i <= n; i++)
		tmp[i] = make_pair(y[i], i);
	sort(tmp + 1, tmp + n + 1);
	sum = 0;
	for (int i = 1; i <= n; i++)
	{
		sum += tmp[i].first;
		ans[tmp[i].second] += 1ll * i * tmp[i].first - sum + sumy - sum - 1ll * (n - i) * tmp[i].first;
	}
	cout << *min_element(ans + 1, ans + n + 1) / 2 << endl;
	return 0;
}