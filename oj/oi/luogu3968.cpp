#include <iostream>
#include <set>
#include <map>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace std;
struct segment
{
	int l, r;
	bool operator<(const segment &rhs) const
	{
		if (r - l == rhs.r - rhs.l)
			return l > rhs.l;
		return r - l > rhs.r - rhs.l;
	}
};
int main()
{
	ios::sync_with_stdio(false);
	int n, q;
	cin >> n >> q;
	set<segment> S = {{1, n}};
	__gnu_pbds::tree<int, __gnu_pbds::null_type, less<int>, __gnu_pbds::rb_tree_tag, __gnu_pbds::tree_order_statistics_node_update> T;
	T.insert(0);
	T.insert(n + 1);
	map<int, int> pos;
	while (q--)
	{
		int opt;
		cin >> opt;
		if (opt)
		{
			if (pos.find(opt) != pos.end())
			{
				int l = *--T.lower_bound(pos[opt]), r = *T.upper_bound(pos[opt]);
				T.erase(pos[opt]);
				S.erase({l + 1, pos[opt] - 1});
				S.erase({pos[opt] + 1, r - 1});
				S.insert({l + 1, r - 1});
				pos.erase(opt);
			}
			else
			{
				int l = S.begin()->l, r = S.begin()->r, mid = (l + r + 1) / 2;
				T.insert(mid);
				if (l < mid)
					S.insert({l, mid - 1});
				if (mid < r)
					S.insert({mid + 1, r});
				S.erase(S.begin());
				pos[opt] = mid;
			}
		}
		else
		{
			int l, r;
			cin >> l >> r;
			cout << T.order_of_key(r + 1) - T.order_of_key(l) << endl;
		}
	}
	return 0;
}