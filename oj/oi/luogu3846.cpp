#include<iostream>
#include<map>
#include<cmath>
using namespace std;
long long qpow(long long a,int b,int p)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%p;
		a=a*a%p;
	}while(b/=2);
	return ans;
}
int main()
{
	int p,b,n;
	cin>>p>>b>>n;
	int m=sqrt(p)+1;
	map<int,int> loga;
	int now=1;
	for(int i=0;i<m;i++,now=1ll*now*b%p)
		loga[now]=i;
	for(int i=0;i<=m;i++)
	{
		map<int,int>::iterator it=loga.find(1ll*n*qpow(qpow(b,i*m,p),p-2,p)%p);
		if(it!=loga.end())
		{
			cout<<i*m+it->second<<endl;
			return 0;
		}
	}
	cout<<"no solution\n";
	return 0;
}
