#include <iostream>
#include <string>
using namespace std;
const int N = 100005, B = 233, MOD = 1e9 + 7;
int n, m, h[N], hp[N], b[N];
string s, pat;
int lcp(int x, int y)
{
	int l = 0, r = m - y + 1, ans = 0;
	while (l <= r)
	{
		int mid = (l + r) / 2;
		if ((h[x + mid - 1] - 1ll * h[x - 1] * b[mid] % MOD + MOD) % MOD == (hp[y + mid - 1] - 1ll * hp[y - 1] * b[mid] % MOD + MOD) % MOD)
		{
			ans = mid;
			l = mid + 1;
		}
		else
			r = mid - 1;
	}
	return ans;
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	b[0] = 1;
	for (int i = 1; i < N; i++)
		b[i] = 1ll * b[i - 1] * B % MOD;
	while (t--)
	{
		cin >> s >> pat;
		n = s.length();
		m = pat.length();
		s = ' ' + s;
		pat = ' ' + pat;
		for (int i = 1; i <= n; i++)
			h[i] = (1ll * h[i - 1] * B + s[i]) % MOD;
		for (int i = 1; i <= m; i++)
			hp[i] = (1ll * hp[i - 1] * B + pat[i]) % MOD;
		int ans = 0;
		for (int i = 1; i + m - 1 <= n; i++)
		{
			int len1 = lcp(i, 1);
			if (len1 == m)
			{
				ans++;
				continue;
			}
			int len2 = lcp(i + len1 + 1, len1 + 2);
			if (len1 + len2 + 1 == m)
			{
				ans++;
				continue;
			}
			int len3 = lcp(i + len1 + 1 + len2 + 1, len1 + len2 + 3);
			if (len1 + len2 + len3 + 2 == m)
			{
				ans++;
				continue;
			}
			int len4 = lcp(i + len1 + 1 + len2 + 1 + len3 + 1, len1 + len2 + len3 + 4);
			if (len1 + len2 + len3 + len4 + 3 == m)
				ans++;
		}
		cout << ans << endl;
	}
	return 0;
}