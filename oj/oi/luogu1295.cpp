#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int a[N], f[N];
struct node
{
	int val, min, lazy;
} tree[N * 4];
inline void pushdown(int id)
{
	if (tree[id].lazy)
	{
		tree[id * 2].lazy = max(tree[id * 2].lazy, tree[id].lazy);
		tree[id * 2].val = tree[id * 2].min + tree[id * 2].lazy;
		tree[id * 2 + 1].lazy = max(tree[id * 2 + 1].lazy, tree[id].lazy);
		tree[id * 2 + 1].val = tree[id * 2 + 1].min + tree[id * 2 + 1].lazy;
	}
}
void modify(int id, int l, int r, int x, int val)
{
	if (l == r)
		tree[id].val = tree[id].min = val;
	else
	{
		pushdown(id);
		int mid = (l + r) / 2;
		if (x <= mid)
			modify(id * 2, l, mid, x, val);
		else
			modify(id * 2 + 1, mid + 1, r, x, val);
		if (tree[id].lazy)
			tree[id].val = tree[id].min + tree[id].lazy;
		else
			tree[id].val = min(tree[id * 2].val, tree[id * 2 + 1].val);
	}
}
void modify(int id, int l, int r, int L, int R, int val)
{
	if (L <= l && R >= r)
	{
		tree[id].lazy = max(tree[id].lazy, val);
		tree[id].val = tree[id].min + tree[id].lazy;
	}
	else
	{
		pushdown(id);
		int mid = (l + r) / 2;
		if (L <= mid)
			modify(id * 2, l, mid, L, R, val);
		if (R > mid)
			modify(id * 2 + 1, mid + 1, r, L, R, val);
		tree[id].min = min(tree[id * 2].min, tree[id * 2 + 1].min);
		if (tree[id].lazy)
			tree[id].val = tree[id].min + tree[id].lazy;
		else
			tree[id].val = min(tree[id * 2].val, tree[id * 2 + 1].val);
	}
}
int query(int id, int l, int r, int L, int R)
{
	if (L <= l && R >= r)
		return tree[id].val;
	pushdown(id);
	int mid = (l + r) / 2;
	if (R <= mid)
		return query(id * 2, l, mid, L, R);
	if (L > mid)
		return query(id * 2 + 1, mid + 1, r, L, R);
	return min(query(id * 2, l, mid, L, R), query(id * 2 + 1, mid + 1, r, L, R));
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
	{
		cin >> a[i];
		a[i] += a[i - 1];
	}
	for (int i = 1, j = 0; i <= n; i++)
	{
		for (; j < i && a[i] - a[j] > m; j++)
			;
		modify(1, 0, n, j, i - 1, a[i] - a[i - 1]);
		f[i] = query(1, 0, n, j, i - 1);
		modify(1, 0, n, i, f[i]);
	}
	cout << f[n] << endl;
	return 0;
}