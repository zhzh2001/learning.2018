#include <iostream>
using namespace std;
const int N = 100005;
int n, mod, tree[N * 4];
pair<int, int> q[N];
void build(int id, int l, int r)
{
	tree[id] = 1;
	if (l < r)
	{
		int mid = (l + r) / 2;
		build(id * 2, l, mid);
		build(id * 2 + 1, mid + 1, r);
	}
}
void modify(int id, int l, int r, int x, int val)
{
	if (l == r)
		tree[id] = val;
	else
	{
		int mid = (l + r) / 2;
		if (x <= mid)
			modify(id * 2, l, mid, x, val);
		else
			modify(id * 2 + 1, mid + 1, r, x, val);
		tree[id] = 1ll * tree[id * 2] * tree[id * 2 + 1] % mod;
	}
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		cin >> n >> mod;
		for (int i = 1; i <= n; i++)
			cin >> q[i].first >> q[i].second;
		build(1, 1, n);
		for (int i = 1; i <= n; i++)
		{
			if (q[i].first == 1)
				modify(1, 1, n, i, q[i].second);
			else
				modify(1, 1, n, q[i].second, 1);
			cout << tree[1] << endl;
		}
	}
	return 0;
}