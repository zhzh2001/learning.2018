#include <iostream>
#include <string>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	typedef pair<int, int> pii;
	__gnu_pbds::tree<pii, __gnu_pbds::null_type, less<pii>, __gnu_pbds::rb_tree_tag, __gnu_pbds::tree_order_statistics_node_update> T;
	int cc = 0;
	while (n--)
	{
		int x;
		cin >> x;
		T.insert(make_pair(x, ++cc));
	}
	int m;
	cin >> m;
	while (m--)
	{
		string opt;
		cin >> opt;
		if (opt == "mid")
			cout << T.find_by_order((T.size() - 1) / 2)->first << '\n';
		else
		{
			int x;
			cin >> x;
			T.insert(make_pair(x, ++cc));
		}
	}
	return 0;
}