#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
using namespace std;
const int N = 100005;
int c[N];
long long ans[N];
vector<int> mat[N];
unordered_map<int, int> *cnt[N];
map<int, long long> *rcnt[N];
void dfs(int k, int fat)
{
	int mx = 0, heavy = 0;
	for (int v : mat[k])
		if (v != fat)
		{
			dfs(v, k);
			if (cnt[v]->size() > mx)
			{
				mx = cnt[v]->size();
				heavy = v;
			}
		}
	if (heavy)
	{
		cnt[k] = cnt[heavy];
		rcnt[k] = rcnt[heavy];
	}
	else
	{
		cnt[k] = new unordered_map<int, int>();
		rcnt[k] = new map<int, long long>();
		(*rcnt[k])[0] = c[k];
	}
	if (!((*rcnt[k])[(*cnt[k])[c[k]]] -= c[k]))
		rcnt[k]->erase((*cnt[k])[c[k]]);
	(*cnt[k])[c[k]]++;
	(*rcnt[k])[(*cnt[k])[c[k]]] += c[k];
	for (int v : mat[k])
		if (v != fat && v != heavy)
			for (auto it : *cnt[v])
			{
				if (!((*rcnt[k])[(*cnt[k])[it.first]] -= it.first))
					rcnt[k]->erase((*cnt[k])[it.first]);
				(*cnt[k])[it.first] += it.second;
				(*rcnt[k])[(*cnt[k])[it.first]] += it.first;
			}
	ans[k] = rcnt[k]->rbegin()->second;
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> c[i];
	for (int i = 1; i < n; i++)
	{
		int u, v;
		cin >> u >> v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	dfs(1, 0);
	for (int i = 1; i <= n; i++)
		cout << ans[i] << ' ';
	cout << endl;
	return 0;
}