#include<bits/stdc++.h>
#define N 200005
using namespace std;
int f[N],dep[N],vis[N];
int n,Q,ans[N];
struct Que{
	int x,t,id;
}q[N];
bool cmp(Que a,Que b){
	if (a.t+dep[a.x]!=b.t+dep[b.x])
		return a.t+dep[a.x]<b.t+dep[b.x];
	return a.x<b.x;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d%d",&n,&Q);
	for (int i=1;i<=n;i++){
		scanf("%d",&f[i]);
		dep[i]=dep[f[i]]+1;
	}
	for (int i=1;i<=Q;i++){
		scanf("%d%d",&q[i].x,&q[i].t);
		q[i].id=i;
	}
	sort(q+1,q+Q+1,cmp);
	for (int i=1;i<=Q;i++){
		int x=q[i].x,t=q[i].t;
		for (;x&&vis[x]<=t;x=f[x],t++);
		int tmp=ans[q[i].id]=2*t-q[i].t;
		for (int y=q[i].x;dep[y]>dep[x];y=f[y])
			vis[y]=tmp--;
	}
	for (int i=1;i<=Q;i++)
		printf("%d ",ans[i]);
}
/*
6 3
0 1 2 3 2 5
4 6
6 9
5 11

3 2
0 1 1
2 1
3 1

8 3
0 1 1 2 3 3 4 5
6 1
8 2
4 5
*/
