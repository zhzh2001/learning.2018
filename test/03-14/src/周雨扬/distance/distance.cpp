#include<bits/stdc++.h>
#define N 233333
using namespace std;
struct P{
	double x,y;
}a[N],p1,p2;
int n,A;
double ans,p,c;
double sqr(double x){
	return x*x;
}
double calc(double x){
	double y=p*x+c;
	double D1=sqrt(sqr(x)+sqr(y+A));
	double D2=sqrt(sqr(x)+sqr(y-A));
	return fabs(D1-D2);
}
int rnd(){
	int x=0;
	for (int i=1;i<=9;i++)
		x=x*10+rand()%10;
	return x;
}
int main(){
	freopen("distance.in","r",stdin);
	freopen("distance.out","w",stdout);
	scanf("%d%d",&n,&A);
	for (int i=1;i<=n;i++)
		scanf("%lf%lf",&a[i].y,&a[i].x);
	for (int i=2;i<=n;i++){
		int y=rnd()%(i-1)+1;
		swap(a[i],a[y]);
	}
	double ans=2*A;
	int times=0;
	for (int i=1;i<=n;i++){
		double tmp1=sqrt(sqr(a[i].x)+sqr(a[i].y-A));
		double tmp2=sqrt(sqr(a[i].x)+sqr(a[i].y+A));
		if (fabs(tmp1-tmp2)>ans) continue;
		times++;
		for (int j=i+1;j<=n;j++){
			if (a[i].x==a[j].x) continue;
			if (a[i].y==a[j].y){
				ans=min(ans,2*fabs(a[i].y));
				continue;
			}
			p=(a[i].y-a[j].y)/(a[i].x-a[j].x);
			c=a[i].y-a[i].x*p;
			if (2*fabs(c)>ans) continue;
			double l=c/p,r=1e6;
			while (r>1e-7){
				double vnow=calc(l);
				double vnxt=calc(l+r);
				while ((vnxt-vnow)/r>5e-7)
					vnow=vnxt,l+=r,vnxt=calc(l+r);
				vnxt=calc(l-r);
				while ((vnxt-vnow)/r>5e-7&&l-r>c/p)
					vnow=vnxt,l-=r,vnxt=calc(l-r);
				if (vnow>ans) break;
				//if (ans<30) printf("%.10Lf %.10Lf %.10Lf %.10Lf\n",vnow,vnxt,l,r);
				r*=0.1;
			}
			if (r>1e-7) continue;
			double tmp=calc((2*l+r)/2);
			l=c/p,r=-1e6;
			while (r<-1e-7){ 
				double vnow=calc(l);
				double vnxt=calc(l+r);
				while ((vnxt-vnow)/(-r)>5e-7)
					vnow=vnxt,l+=r,vnxt=calc(l+r);
				vnxt=calc(l-r);
				while ((vnxt-vnow)/(-r)>5e-7&&l-r<c/p)
					vnow=vnxt,l-=r,vnxt=calc(l-r);
				if (vnow>ans) break;
				//if (ans<4.63) printf("%.10Lf %.10Lf %.10Lf %.10Lf\n",vnow,vnxt,l,r);
				r*=0.1;
			}
			if (r>1e-7) continue;
			ans=min(ans,max(tmp,calc((2*l+r)/2)));
			//printf("%d %d %.10Lf %.10Lf %.10Lf\n",i,j,l,calc((2*l+r)/2),tmp);
			//printf("%.10Lf %.10Lf %.10Lf %.10Lf %.10Lf %.10Lf\n",a[i].x,a[i].y,a[j].x,a[j].y,c,ans);
		}
	}
	printf("%.10lf\n",ans,times);
}
