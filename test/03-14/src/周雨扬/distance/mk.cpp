#include<bits/stdc++.h>
#define pa pair<int,int>
using namespace std;
int rnd(){
	int x=0;
	for (int i=1;i<=4;i++)
		x=x*10+rand()%10;
	x++;
	return rand()&1?x:-x;
}
map<pa,int> mp;
int main(){
	srand(time(NULL));
	freopen("distance.in","w",stdout);
	printf("10000 2333\n");
	for (int i=1;i<=10000;i++){
		int x=rnd(),y=rnd();
		while (mp[pa(x,y)])
			x=rnd(),y=rnd();
		printf("%d %d\n",x,y);
		mp[pa(x,y)]=1;
	}
}
