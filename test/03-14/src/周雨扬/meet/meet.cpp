#include<bits/stdc++.h>
#define N 100005
using namespace std;
struct edge{
	int to,next;
}e[N*2];
int dep[N],sz[N],dfn[N],la[N];
int head[N],top[N],fa[N];
int n,Q,tot,T,L[N];
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
}
struct Que{
	int t,v,x,y;
}q[N];
bool cmp(Que a,Que b){
	return a.v>b.v;
}
void dfs1(int x){
	sz[x]=1; dep[x]=dep[fa[x]]+1;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa[x]){
			fa[e[i].to]=x;
			dfs1(e[i].to);
			sz[x]+=sz[e[i].to];
		}
}
void dfs2(int x,int tp){
	dfn[x]=++T;
	top[x]=tp;
	int k=0;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa[x]&&sz[e[i].to]>sz[k])
			k=e[i].to;
	if (k) dfs2(k,tp);
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa[x]&&e[i].to!=k)
			dfs2(e[i].to,e[i].to);
	la[x]=T;
}
bool father(int x,int y){
	return dfn[x]<=dfn[y]&&la[x]>=la[y];
}
int LCA(int x,int y){
	for (;top[x]!=top[y];x=fa[top[x]])
		if (dep[top[x]]<dep[top[y]]) swap(x,y);
	return dep[x]<dep[y]?x:y;
}
int dis(int x,int y){
	return dep[x]+dep[y]-2*dep[LCA(x,y)];
}
int findclose(int x,int y,int z){
	return dis(x,y)<dis(x,z)?y:z;
}
int main(){
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	scanf("%d%d",&n,&Q);
	for (int i=1;i<n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y); add(y,x);
	}
	dfs1(1);
	dfs2(1,1);
	for (int i=1;i<=Q;i++)
		scanf("%d%d%d%d",&q[i].t,&q[i].v,&q[i].x,&q[i].y);
	sort(q+1,q+Q+1,cmp);
	double ans=1e9;
	for (int i=1;i<=Q;i++)
		L[i]=LCA(q[i].x,q[i].y);
	for (int i=1;i<=Q;i++){
		if (q[i].t>ans) continue;
		for (int j=i+1;j<=Q;j++){
			if (q[j].t>ans) continue;
			int L1=L[i];
			int L2=L[j];
			if (!father(L1,L2)) swap(L1,L2);
			if (!father(L1,L2)) continue;
			int l1=LCA(q[i].x,q[j].x);
			int l2=LCA(q[i].y,q[j].x);
			int l3=LCA(q[i].x,q[j].y);
			int l4=LCA(q[i].y,q[j].y);
			if (dep[l2]>dep[l1]) swap(l1,l2);
			if (dep[l3]>dep[l1]) swap(l1,l3);
			if (dep[l4]>dep[l1]) swap(l1,l4);
			if (dep[l3]>dep[l2]) swap(l2,l3);
			if (dep[l4]>dep[l2]) swap(l2,l4);
			if (dep[l4]>dep[l3]) swap(l3,l4);
			int v1,v2;
			int p1=findclose(q[i].x,l1,l2);
			int p2=findclose(q[j].x,l1,l2);
			int d=dis(l1,l2);
			double t1=q[i].t+1.0*dis(q[i].x,p1)/q[i].v;
			double t2=q[j].t+1.0*dis(q[j].x,p2)/q[j].v;
			double e1=t1+1.0*d/q[i].v;
			double e2=t2+1.0*d/q[j].v;
			if (t1-1e-5>e2||t2-1e-5>e1) continue;
			if (t1-1e-5>ans||t2-1e-5>ans) continue;
			if (p1==p2&&q[i].v!=q[j].v&&t1+(t1-t2)/(1.0*q[i].v/q[j].v-1)<e2+1e-5)
				ans=min(ans,t1+(t1-t2)/(1.0*q[i].v/q[j].v-1));
			if (p1!=p2) ans=min(ans,t1+1.0*(d+(t2-t1)*q[j].v)/(q[i].v+q[j].v));
		}
	}
	if (ans>1e8) puts("-1");
	else printf("%.10lf\n",ans);
}
/*
6 4
2 5
6 5
3 6
4 6
4 1
27 6 1 3
9 5 1 6
27 4 3 4
11 29 2 6
*/
