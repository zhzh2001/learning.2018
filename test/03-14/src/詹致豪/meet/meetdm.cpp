#include <cstdio>
#include <ctime>
#include <algorithm>

using namespace std;

int main()
{
	freopen("meet.in","w",stdout);
	srand((int) time(0));
	int n=10,m=4;
	printf("%d %d\n",n,m);
	for (int i=1;i<n;i++)
		printf("%d %d\n",rand()%i+1,i+1);
	for (int i=1;i<=m;i++)
		printf("%d %d %d %d\n",rand()%5,rand()%3+1,(rand()<<15|rand())%n+1,(rand()<<15|rand())%n+1);
	return 0;
}
