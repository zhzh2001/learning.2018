#include <cstdio>
#include <algorithm>

using namespace std;

const double eps=1e-12;

int edge[300000],next[300000],first[120000];
int d[120000],f[120000][20],u[10],v[120000],w[120000],x[120000],y[120000],z[120000];
int i,j,k,m,n,p,q,sum_edge;
double tpi,tqi,tpj,tqj,tvi,tvj,dpq,s;

inline void addedge(int x,int y)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],first[x]=sum_edge;
	return;
}

inline void dfs(int x,int y,int z)
{
	d[x]=z,f[x][0]=y;
	for (int i=1;(1<<i)<=z;i++)
		f[x][i]=f[f[x][i-1]][i-1];
	for (int i=first[x];i!=0;i=next[i])
		if (edge[i]!=y)
			dfs(edge[i],x,z+1);
	return;
}

inline int LCA(int x,int y)
{
	int k;
	if (d[x]>d[y])
		swap(x,y);
	for (k=0;(1<<k)<=d[y]-d[x];k++);
	for (k--;k>=0;k--)
		if ((1<<k)<=d[y]-d[x])
			y=f[y][k];
	if (x==y)
		return x;
	for (k=0;f[x][k]!=f[y][k];k++);
	for (k--;k>=0;k--)
		if (f[x][k]!=f[y][k])
			x=f[x][k],y=f[y][k];
	return f[x][0];
}

inline int getdis(int x,int y)
{
	int z=LCA(x,y);
	return d[x]+d[y]-2*d[z];
}

inline bool cmp(int x,int y)
{
	return d[x]>d[y];
}

int main()
{
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<n;i++)
		scanf("%d%d",&p,&q),addedge(p,q),addedge(q,p);
	dfs(1,0,0);
	for (i=1;i<=m;i++)
		scanf("%d%d%d%d",&z[i],&v[i],&x[i],&y[i]),w[i]=LCA(x[i],y[i]);
	s=1e6;
	for (i=1;i<m;i++)
		for (j=i+1;j<=m;j++)
		{
			u[1]=LCA(x[i],x[j]);
			u[2]=LCA(x[i],y[j]);
			u[3]=LCA(y[i],x[j]);
			u[4]=LCA(y[i],y[j]);
			sort(u+1,u+5,cmp);
			p=u[1],q=u[2];
			if (min(d[p],d[q])<max(d[w[i]],d[w[j]]))
				continue;
			tpi=1.0*getdis(p,x[i])/v[i]+z[i];
			tqi=1.0*getdis(q,x[i])/v[i]+z[i];
			tpj=1.0*getdis(p,x[j])/v[j]+z[j];
			tqj=1.0*getdis(q,x[j])/v[j]+z[j];
			tvi=v[i];
			tvj=v[j];
			if ((tpi<tqi+eps)^(tpj<tqj+eps))
			{
				if (tpi>tqi) swap(tpi,tqi);
				if (tpj>tqj) swap(tpj,tqj);
				if (max(tpi,tpj)<min(tqi,tqj)+eps)
				{
					dpq=getdis(p,q);
					if (tpi<tpj) dpq=dpq-(tpj-tpi)*tvi,tpi=tpj;
					if (tpj<tpi) dpq=dpq-(tpi-tpj)*tvj,tpj=tpi;
					s=min(s,tpi+dpq/(tvi+tvj));
				}
			}
			else
			{
				if (tpi>tqi) swap(tpi,tqi);
				if (tpj>tqj) swap(tpj,tqj);
				if (tvi>tvj) swap(tpi,tpj),swap(tqi,tqj),swap(tvi,tvj);
				if ((tpj+eps>tpi) && (tqj<tqi+eps))
				{
					dpq=(tpj-tpi)*tvi;
					s=min(s,tpj+dpq/(tvj-tvi));
				}
			}
		}
	if (s==1e6)
		puts("-1");
	else
		printf("%.10lf",s);
	return 0;
}
