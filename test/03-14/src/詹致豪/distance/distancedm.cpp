#include <cstdio>
#include <ctime>
#include <algorithm>
#include <map>

using namespace std;

map < pair <int,int> , int > M;

int main()
{
	freopen("distance.in","w",stdout);
	srand((int) time(0));
	int n=100000,m=rand()%10000+1;
	printf("%d %d\n",n,m);
	for (int i=1;i<=n;i++)
	{
		int x=rand()%20000-10000,y=rand()%20000-10000; if (x>=0) x++; if (y>=0) y++;
		while (M[make_pair(x,y)])
			{ x=rand()%20000-10000,y=rand()%20000-10000; if (x>=0) x++; if (y>=0) y++; }
		printf("%d %d\n",x,y);
		M[make_pair(x,y)]=1;
	}
	return 0;
}
