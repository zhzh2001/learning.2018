#include <cstdio>
#include <cmath>
#include <algorithm>

using namespace std;

struct point
{
	int x,y;
};

point p[120000];
point u,v;

int i,j,m,n;
double s;

inline bool cmp(point x,point y)
{
	if (abs(x.x)!=abs(y.x))
		return abs(x.x)<abs(y.x);
	else
		return abs(x.y)>abs(y.y);
}

inline double calc(point x,point y)
{
	double wx=v.x,wy=v.y;
	if (x.x==y.x) wx=2*x.x-wx; else
	{
		double k=1.0*(y.y-x.y)/(y.x-x.x),b=x.y-k*x.x,kk=-1.0/k;
		double d=2*abs((v.x-x.x)*(v.y-y.y)-(v.y-x.y)*(v.x-y.x))/sqrt((x.x-y.x)*(x.x-y.x)+(x.y-y.y)*(x.y-y.y));
		double tx=d/sqrt(kk*kk+1),ty=tx*kk;
		if ((k*wx+b>wy)^(k*(wx+tx)+b>(wy+ty))) wx=wx+tx,wy=wy+ty; else wx=wx-tx,wy=wy-ty;
	}
	return sqrt((u.x-wx)*(u.x-wx)+(u.y-wy)*(u.y-wy));
}

int main()
{
	freopen("distance.in","r",stdin);
	freopen("distance.out","w",stdout);
	scanf("%d%d",&n,&m);
	u=(point) {-m,0},v=(point) {m,0};
	for (i=1;i<=n;i++)
		scanf("%d%d",&p[i].x,&p[i].y);
	sort(p+1,p+n+1,cmp);
	s=2*m;
	for (i=1;i<=min(n,6000);i++)
		for (j=i+1;j<=min(n,6000);j++)
			s=min(s,calc(p[i],p[j]));
	printf("%.10lf",s);
	return 0;
}
