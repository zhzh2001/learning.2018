#include<cstdio>
#include<set>
#include<algorithm>
using namespace std;
typedef long double lb;
const int N=100100,M=2002000;
const lb eps=1e-10,Eps=1e-9;
int i,j,k,n,m,En,ch,x,y,nm,t,c,fx,fy,fg,Tn,nx,ny;
int h[N];
struct number {
	lb x;
	void operator = (const int &n){
		x=n;
	}
	void operator = (const double &n){
		x=n;
	}
	bool operator < (const number &n) const {
		return x+eps<n.x;
	}
	number operator + (const number &n) const {
		number a;
		a.x=x+n.x;
		return a;
	}
	number operator - (const number &n) const {
		number a;
		a.x=x-n.x;
		return a;
	}
	number operator * (const number &n) const {
		number a;
		a.x=x*n.x;
		return a;
	}
	number operator / (const number &n) const {
		number a;
		a.x=x/n.x;
		return a;
	}
};
number Time,ans,X1,X2,Y1,Y2,tx1,tx2;
struct cc {
	number k,b,r;
	bool operator < (const cc &n) const {
		if (!(k*Time+b<n.k*Time+n.b) && !(n.k*Time+n.b<k*Time+b)) {
			if (!(k<n.k) && !(n.k<k)) {
				if (!(b<n.b) && !(n.b<b)) return r<n.r;
				return b<n.b;
			}
			return k<n.k;
		}
		return k*Time+b<n.k*Time+n.b;
	}
} tmp;
multiset<cc> S;
multiset<cc>::iterator it,itt;
struct dd {
	int fg;number x,k,b,r;
	bool operator < (const dd &n) const {
		if (!(x<n.x) && !(n.x<x)) return fg>n.fg;
		return x<n.x;
	}
} A[M];
struct ee { int l,r;} X[N],Y[N];
struct edge { int s,n;} E[N<<1];
struct point { int fa,dp,sz,sn,fv,tp;} P[N];
void R(int &x) {
	x=0;ch=getchar();
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
}
void E_add(int x,int y) {
	E[++En].s=y;E[En].n=h[x];h[x]=En;
	E[++En].s=x;E[En].n=h[y];h[y]=En;
}
void dfs1(int x,int F) {
	P[x].fa=F;P[x].dp=P[F].dp+1;P[x].sz=1;
	for (int k=h[x];k;k=E[k].n) if (E[k].s!=F) {
		dfs1(E[k].s,x);
		P[x].sz+=P[E[k].s].sz;
		if (P[E[k].s].sz>P[P[x].sn].sz) P[x].sn=E[k].s;
	}
}
void dfs2(int x,int F,int top) {
	P[x].fv=++Tn;P[x].tp=top;
	if (P[x].sn) dfs2(P[x].sn,x,top);
	for (int k=h[x];k;k=E[k].n) if (E[k].s!=F && E[k].s!=P[x].sn) dfs2(E[k].s,x,E[k].s);
}
void check(cc a,cc b) {
	if (!(a.k<b.k) && !(b.k<a.k)) {
		if (!(a.b<b.b) && !(b.b<a.b)) {
			if (Time<ans) fg=1,ans=Time;
		}
		return;
	}
	number t=(b.b-a.b)/(a.k-b.k);
	if (t<Time) return;
	if (a.r<t) return;
	if (b.r<t) return;
	if (t<ans) fg=1,ans=t;
}
void work(int fg1,int fg2) {
	A[++nm].fg=1;
	A[nm].k=(Y2-Y1)/(X2-X1);
	A[nm].b=Y1-X1*A[nm].k;
	if (fg1) X1.x+=Eps;
	if (fg2) X2.x-=Eps;
	A[nm].x=X1;
	A[nm].r=X2;
	A[++nm].fg=2;
	A[nm].x=X2;
	A[nm].k=A[nm-1].k;
	A[nm].b=A[nm-1].b;
	A[nm].r=X2;
	tx1=tx2;
}
int main() {
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	R(n);R(m);
	for (i=1;i<n;i++) {
		R(x);R(y);
		E_add(x,y);
	}
	dfs1(1,0);
	dfs2(1,0,1);
	double tttt=1e-9;
	for (i=1;i<=m;i++) {
		R(t);R(c);R(x);R(y);
		if (x==y) {
			A[++nm].fg=1;
			A[nm].x=1.0*t-tttt;
			A[nm].k=0;
			A[nm].b=P[x].fv;
			A[nm].r=t;
			A[++nm].fg=2;
			A[nm].x=1.0*t+tttt;
			A[nm].k=0;
			A[nm].b=P[x].fv;
			A[nm].r=t;
		}
		else {
			nx=ny=0;
			fx=P[x].tp;fy=P[y].tp;
			while (fx!=fy) {
				if (P[fx].dp>P[fy].dp) {
					X[++nx].l=P[fx].fv-1;
					X[nx].r=P[x].fv;
					x=P[fx].fa;
					fx=P[x].tp;
				}
				else {
					Y[++ny].l=P[fy].fv-1;
					Y[ny].r=P[y].fv;
					y=P[fy].fa;
					fy=P[y].tp;
				}
			}
			tx1=t;
			for (j=1;j<=nx;j++) {
				X1=tx1;
				X2=1.0*(X[j].r-X[j].l)/(1.0*c);
				X2=X1+X2;
				tx2=X2;
				Y1=X[j].r;
				Y2=X[j].l;
				//X2.x-=Eps;
				//Y2.x-=Eps;
				work(0,1);
			}
			if (x==y) {
				A[++nm].fg=1;
				A[nm].x.x=tx1.x-Eps;
				A[nm].k=0;
				A[nm].b=P[x].fv;
				A[nm].r=tx1;
				A[++nm].fg=2;
				A[nm].x.x=tx1.x+Eps;
				A[nm].k=0;
				A[nm].b=P[x].fv;
				A[nm].r=tx1;
			}
			else {
				X1=tx1;
				if (P[x].dp>P[y].dp) X2=1.0*(P[x].fv-P[y].fv)/(1.0*c);
				else X2=1.0*(P[y].fv-P[x].fv)/(1.0*c);
				X2=X1+X2;
				tx2=X2;
				Y1=P[x].fv;
				Y2=P[y].fv;
				work(0,0);
			}
			for (j=ny;j;j--) {
				X1=tx1;
				X2=1.0*(Y[j].r-Y[j].l)/(1.0*c);
				X2=X1+X2;
				tx2=X2;
				Y1=Y[j].l;
				Y2=Y[j].r;
				//X1.x+=Eps;
				//Y1.x+=Eps;
				work(1,0);
			}
		}
	}
	sort(A+1,A+nm+1);
	fg=0;
	ans=1e20;
	number Last=A[1].x;
	for (i=1;i<=nm;i++) {
		if (fg && !(A[i].x<ans)) break;
		//tmp.k=3;tmp.b=-5;tmp.r=3;
		//if (S.find(tmp)==S.end()) printf("QAQ %d\n",i);
		/*printf("%Lf\n",A[i].x.x);
		for (multiset<cc>::iterator it=S.begin();it!=S.end();it++) {
			
			//if ((*it).k.x==tmp.k.x && (*it).b.x==tmp.b.x && (*it).r.x==tmp.r.x) printf("QAQ %d\n",i);
			printf("%Lf %Lf %Lf\n",(*it).k.x,(*it).b.x,(*it).r.x);	
		}
		puts("===========================");*/
		
		Time=A[i].x;
		tmp.k=A[i].k;tmp.b=A[i].b;tmp.r=A[i].r;
		if (A[i].fg&1) {
			it=S.insert(tmp);
			if (it!=S.begin()) {
				itt=it;
				itt--;
				check(tmp,*itt);
			}
			itt=S.end();
			itt--;
			if (it!=itt) {
				itt=it;
				itt++;
				check(tmp,*itt);
			}
		}
		else {
			//Time=Last;
			it=S.find(tmp);
			if (it==S.end())
			if (it!=S.begin()) {
				itt=it;
				itt--;
				//Time=A[i].x;
				check(tmp,*itt);
				//Time=Last;
			}
			itt=S.end();
			itt--;
			if (it!=itt) {
				itt=it;
				itt++;
				//Time=A[i].x;
				check(tmp,*itt);
				//Time=Last;
			}
			S.erase(it);
		}
		if (Last<A[i].x) Last=A[i].x;
	}
	if (!fg) puts("-1");
	else printf("%.9lf\n",(double) ans.x);
}
