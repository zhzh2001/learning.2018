#include<cstdio>
#include<algorithm>
using namespace std;
const int N=200200,D=2;
int i,j,k,n,m,nm,Tn,ch,En,x,y,fx,s;
int h[N],p[N],ans[N];
struct tree { int s,ma,pos;} T[N<<2];
struct cc { int x,t,q;} A[N];
struct dd { int l,r;} B[N];
struct point { int fa,dp,sz,sn,fv,tp;} P[N];
struct edge { int s,n;} E[N];
void R(int &x) {
	x=0;ch=getchar();
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
}
void W(int x) {
	if (x>=10) W(x/10);
	putchar(x%10+'0');
}
void E_add(int x,int y) {
	E[++En].s=y;E[En].n=h[x];h[x]=En;
}
void dfs1(int x) {
	P[x].sz=1;
	for (int k=h[x];k;k=E[k].n) {
		P[E[k].s].dp=P[x].dp+1;
		dfs1(E[k].s);
		P[x].sz+=P[E[k].s].sz;
		if (P[E[k].s].sz>P[P[x].sn].sz) P[x].sn=E[k].s;
	}
}
void dfs2(int x,int top) {
	P[x].fv=++Tn;p[Tn]=x;P[x].tp=top;
	if (P[x].sn) dfs2(P[x].sn,top);
	for (int k=h[x];k;k=E[k].n) if (E[k].s!=P[x].sn) dfs2(E[k].s,E[k].s);
}
bool cmp(const cc &a,const cc &b) {
	if (P[a.x].dp+a.t==P[b.x].dp+b.t) return a.x<b.x;
	return P[a.x].dp+a.t<P[b.x].dp+b.t;
}
int max(const int &x,const int &y) {
	if (x<y) return y;
	return y;
}
void up(int k) {
	if (T[k<<1].ma>T[k<<1|1].ma) T[k].ma=T[k<<1].ma,T[k].pos=T[k<<1].pos;
	else T[k].ma=T[k<<1|1].ma,T[k].pos=T[k<<1|1].pos;
}
void down(int k,int L,int R) {
	if (T[k].s) {
		int mid=(L+R)>>1;
		T[k<<1].s=T[k].s;
		T[k<<1].ma=T[k].s+(mid-L)*D;
		T[k<<1].pos=mid;
		T[k<<1|1].s=T[k<<1].ma+D;
		T[k<<1|1].ma=T[k].s+(R-L)*D;
		T[k<<1|1].pos=R;
		T[k].s=0;
	}
}
int T_find(int L,int R,int l,int r,int x,int k) {
	if (L==R) {
		if (T[k].ma>x) return R;
		return -1;
	}
	down(k,L,R);
	int mid=(L+R)>>1;
	if (L==l && R==r) {
		if (T[k<<1|1].ma>x) return T_find(mid+1,R,mid+1,r,x,k<<1|1);
		if (T[k<<1].ma>x) return T_find(L,mid,l,mid,x,k<<1);
		return -1;
	}
	if (r<=mid) return T_find(L,mid,l,r,x,k<<1);
	if (l>mid) return T_find(mid+1,R,l,r,x,k<<1|1);
	int t=T_find(mid+1,R,mid+1,r,x,k<<1|1);
	if (t!=-1) return t;
	return T_find(L,mid,l,mid,x,k<<1);
}
void T_add(int L,int R,int l,int r,int s,int k) {
	if (L==l && R==r) {
		T[k].s=s;
		T[k].ma=s+(R-L)*D;
		T[k].pos=R;
		return;
	}
	down(k,L,R);
	int mid=(L+R)>>1;
	if (r<=mid) T_add(L,mid,l,r,s,k<<1);
	else {
		if (l>mid) T_add(mid+1,R,l,r,s,k<<1|1);
		else T_add(L,mid,l,mid,s,k<<1),T_add(mid+1,R,mid+1,r,s+(mid-l+1)*D,k<<1|1);
	}
	up(k);
}
int main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	R(n);n++;R(m);
	for (i=2;i<=n;i++) {
		R(P[i].fa);P[i].fa++;
		E_add(P[i].fa,i);
	}
	dfs1(1);
	dfs2(1,1);
	for (i=1;i<=m;i++) {
		R(A[i].x);R(A[i].t);
		A[i].q=i;A[i].x++;
	}
	sort(A+1,A+m+1,cmp);
	for (i=1;i<=m;i++) {
		x=A[i].x;fx=P[x].tp;nm=0;
		while (x) {
			y=T_find(1,Tn,P[fx].fv,P[x].fv,A[i].t+P[A[i].x].dp,1);
			if (y==-1) {
				B[++nm].l=P[fx].fv;
				B[nm].r=P[x].fv;
			}
			else {
				B[++nm].l=y;
				B[nm].r=P[x].fv;
				break;
			}
			x=P[fx].fa;
			fx=P[x].tp;
		}
		ans[A[i].q]=A[i].t+2*(P[A[i].x].dp-P[p[y]].dp);
		if (y==-1) {
			s=A[i].t+P[A[i].x].dp;
			while (nm) {
				T_add(1,Tn,B[nm].l,B[nm].r,s,1);
				s+=(B[nm].r-B[nm].l+1)*D;
				nm--;
			}
		}
		else {
			s=A[i].t+P[A[i].x].dp+D;
			if (B[nm].l==B[nm].r) nm--;
			else B[nm].l++;
			while (nm) {
				T_add(1,Tn,B[nm].l,B[nm].r,s,1);
				s+=(B[nm].r-B[nm].l+1)*D;
				nm--;
			}
		}
	}
	for (i=1;i<=m;i++) W(ans[i]),putchar(' ');
	puts("");
}
