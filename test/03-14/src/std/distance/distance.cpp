#include<cstdio>
#include<cmath>
#include<algorithm>
#include<set>
using namespace std;
typedef double db;
const db eps=1e-10,pi=acos(-1.0);
const int N=100100;
int i,j,k,n,m;
db a,l,r,mid;
struct cc { db x,y,r;} A[N];
struct dd {
	db l,r;
	bool operator < (const dd &n) const {
		if (l==n.l) return r>n.r;
		return l<n.l;
	}
} B[N<<1];
db sqr(db x) {
	return x*x;
}
db ab(db x) {
	if (x<0) return -x;
	return x;
}
set<db> S;
set<db>::iterator it;
bool check(double x) {
	int i,nm=0;
	db u,t,dis,ll,rr;
	for (i=1;i<=n;i++) {
		dis=sqrt(sqr(A[i].x-a)+sqr(A[i].y));
		if (dis>A[i].r+x || A[i].r>dis+x || x>dis+A[i].r) continue;
		u=atan2(A[i].y,A[i].x-a);
		t=acos((sqr(dis)+sqr(x)-sqr(A[i].r))/(2.0*dis*x));
		ll=u-t;rr=u+t;
		if (ll<-pi) ll+=2*pi;
		if (ll>pi) ll-=2*pi;
		if (rr<-pi) rr+=2*pi;
		if (rr>pi) rr-=2*pi;
		if (ll>rr) {
			t=ll;ll=rr;rr=t;
		}
		nm++;
		B[nm].l=ll;B[nm].r=rr;
	}
	sort(B+1,B+nm+1);
	//puts("=================================");
	//printf("%.10lf\n",x);
	//for (i=1;i<=nm;i++) printf("%.10lf %.10lf\n",B[i].l,B[i].r);
	S.clear();
	for (i=1;i<=nm;i++) {
		it=S.upper_bound(B[i].l);
		if (it!=S.end() && *it<B[i].r) return 1;
		S.insert(B[i].r);
	}
	return 0;
}
int main() {
	freopen("distance.in","r",stdin);
	freopen("distance.out","w",stdout); 
	scanf("%d%lf",&n,&a);
	for (i=1;i<=n;i++) {
		scanf("%lf%lf",&A[i].x,&A[i].y);
		A[i].r=sqrt(sqr(A[i].x+a)+sqr(A[i].y));
	}
	l=0.0;r=2.0*a;
	for (i=1;i<=50;i++) {
		mid=0.5*(l+r);
		if (check(mid)) r=mid;
		else l=mid;
	}
	printf("%.10lf\n",l);
}
