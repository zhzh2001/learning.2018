#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
#define pii pair<int,int>
#define mk(x,y) make_pair(x,y)

const int N = 200005,M = 200005;
set<pii > up,down;		//first: point ; second: question
set<pii > ::iterator it,dit;
struct Que{
	int x,t,ans;
}q[M];int top;
stack<int> path[M];
int now[M];
int n,m,t;
bool wait[N];
int fa[N];

int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n = read(); m = read(); wait[0] = true; top = 1;
	for(int i = 1;i <= n;++i) fa[i] = read();
	for(int i = 1;i <= m;++i){ q[i].x = read(); q[i].t = read(); }
	while(top <= m || up.begin() != up.end() || down.begin() != down.end()){
		for(it = down.begin();it != down.end();){
			int to = path[it->second].top(); path[it->second].pop();
			wait[to] = false;
			// printf("%d %d %d %d\n", to,it->first,it->second,t);
			if(to == (it->first)){
				dit = it;
				q[it->second].ans = t; it++;
				down.erase(dit);
			}else it++;
		}
		for(it = up.begin();it != up.end();){
			int to = fa[now[it->second]];
			path[it->second].push(now[it->second]);
			// printf("@ %d %d %d %d\n",it->second,to,now[it->second],t);
			if(wait[now[it->second]]){
				puts("1");
				path[it->second].pop(); if(!path[it->second].empty()) path[it->second].pop();
				if(path[it->second].empty()) q[it->second].ans = t;
				else down.insert(mk(it->first,it->second));
				dit = it;
				it++; up.erase(dit);
			}else if(wait[to]){
				wait[now[it->second]] = true; 
				dit = it;
				down.insert(mk(it->first,it->second));
				it++; up.erase(dit);
			}else{
				wait[now[it->second]] = true;
				now[it->second] = to;
				it++;
			}
		}
		if(up.begin() == up.end() && down.begin() == down.end()) t = q[top].t;
		while(top <= m && q[top].t == t){
			if(wait[q[top].x]) q[top].ans = t;
			else{
				now[top] = q[top].x;
				up.insert(mk(q[top].x,top));
			}++top;
		}
		++t;
	}
	for(int i = 1;i <= m;++i)
		printf("%d ",q[i].ans);
	puts("");
	return 0;
}