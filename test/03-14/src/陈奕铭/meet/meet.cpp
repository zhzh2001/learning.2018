#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005,M = 100005;
int n,m;
double ans = 1e9;
int head[N],nxt[N*2],to[N*2],cnt;
struct people{
	int t,c,v,u,len;
	int lca;
}a[M];
int f[18][N],dep[N];

inline void insert(int x,int y){to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;}

void dfs(int x,int fa){
	dep[x] = dep[fa]+1; f[0][x] = fa;
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa)
			dfs(to[i],x);
}

inline void parpre(){
	for(int j = 1;j < 17;++j)
		for(int i = 1;i <= n;++i)
			f[j][i] = f[j-1][f[j-1][i]];
}

inline int getf(int x,int y){
	for(int j = 0;j < 17;++j)
		if(y&(1<<j))
			x = f[j][x];
	return x;
}

inline int getlca(int x,int y){
	if(dep[x] < dep[y]) swap(x,y);
	int num = dep[x]-dep[y];
	for(int j = 0;j < 17;++j)
		if(num&(1<<j))
			x = f[j][x];
	if(x == y) return x;
	for(int j = 16;j >= 0;--j)
		if(f[j][x] != f[j][y])
			x = f[j][x],y = f[j][y];
	return f[0][x];
}

inline void solve(people a,people b){
	if(a.t > b.t) swap(a,b);
	int len = (b.t-a.t)*a.c;
	if(len > a.len) return;
	if(len > dep[a.u]-dep[a.lca]){
		a.lca = a.u = getf(a.v,a.len-len);
		a.len -= len;
	}else{
		a.lca = a.u = getf(a.u,len);
		a.len -= len;
	}
	int Lca = getlca(a.u,b.u);
	len = dep[a.u]+dep[b.u]-2*dep[Lca];
	double t = 1.0*len/(a.c+b.c);
	double len1 = t*a.c,len2 = t*b.c;
	int as,at,bs,bt;
	if(len1 > a.len || len2 > b.len) return;
	if((double)((int)(len1)) != len1){
		if(len1 > dep[a.u]-dep[a.lca]){
			as = getf(a.v,a.len-(int)(len1));
			at = getf(a.v,a.len-(int)(len1)-1);
		}else{
			as = getf(a.u,(int)(len1));
			at = getf(a.u,(int)(len1)+1);
		}
	}else{
		if(len1 > dep[a.u]-dep[a.lca]){
			as = at = getf(a.v,a.len-(int)(len1));
		}else{
			as = at = getf(a.u,(int)(len1));
		}
	}
	if((double)((int)(len1)) != len1){
		if(len1 > dep[b.u]-dep[b.lca]){
			bs = getf(b.v,b.len-(int)(len1));
			bt = getf(b.v,b.len-(int)(len1)-1);
		}else{
			bs = getf(b.u,(int)(len1));
			bt = getf(b.u,(int)(len1)+1);
		}
	}else{
		if(len1 > dep[b.u]-dep[b.lca]){
			bs = bt = getf(b.v,b.len-(int)(len1));
		}else{
			bs = bt = getf(b.u,(int)(len1));
		}
	}
	// printf("@ %d %d %d %d\n",as,at,bs,bt);
	if(bs == bt && (as == bs || at == bs)){
		ans = min(ans,b.t+t);
		return;
	}
	if(as == at && (as == bs || as == bt)){
		ans = min(ans,b.t+t);
		return;
	}
	if(as == bt && at == bs){
		ans = min(ans,b.t+t);
		return;
	}
}

int main(){
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	n = read(); m = read();
	if(m > 30000){
		puts("-1");
		return 0;
	}
	for(int i = 1;i < n;++i){
		int x = read(),y = read();
		insert(x,y); insert(y,x);
	}
	dfs(1,1);
	parpre();
	for(int i = 1;i <= m;++i){
		a[i].t = read(); a[i].c = read();
		a[i].u = read(); a[i].v = read();
		a[i].lca = getlca(a[i].u,a[i].v);
		a[i].len = dep[a[i].u]+dep[a[i].v]-2*dep[a[i].lca];
	}
	for(int i = 1;i <= m;++i)
		for(int j = i+1;j <= m;++j){
			// printf("@ %d %d\n", i,j);
			solve(a[i],a[j]);
		}
	if(ans < 1e8) printf("%.7lf\n",ans);
	else puts("-1");
	return 0;
}