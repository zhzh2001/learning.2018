#include <bits/stdc++.h>
#define N 1000020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
struct FFT {
  typedef std::complex<double> cp;
  int rev[N], n, l;
  cp wn[N][2], w[N];
  void init(int m) {
    for (n = 1; n <= m; n <<= 1, ++ l);
    for (int i = 0; i < n; i++)
      rev[i] = rev[i >> 1] >> 1 | ((i & 1) << l - 1);
    double t = acos(-1) / n;
    for (int i = 0; i < n; i += 2) {
      wn[i][0] = cp(cos(t * i), sin( t * i));
      wn[i][1] = cp(cos(t * i), sin(-t * i));
    }
  }
  void dft(cp *x, int f) {
    for (int i = 0; i < n; i++)
      if (i < rev[i]) swap(x[i], x[rev[i]]);
    w[0] = cp(1, 0);
    for (int d = 1; d < n; d <<= 1) {
      for (int i = 1; i <= d; i++)
        w[i] = wn[n / d * i][f];
      for (int i = 0; i < n; i += d <<= 1) {
        for (int k = 0, l = i, r = i + d; k < d; ++ k, ++ l, ++ r) {
          cp tmp = x[r] * w[k];
          x[r] = x[l] - tmp;
          x[l] = x[l] + tmp;
        }
      }
    }
    if (f)
      for (int i = 0; i < n; i++)
        x[i] /= n;
  }
}fft;
int main(int argc, char const *argv[]) {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);
  puts("QAQ");
  return 0;
}