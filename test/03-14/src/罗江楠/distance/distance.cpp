#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int x[N], y[N], n, a;
double sqr(double x) {
  return x * x;
}
double k, b;
double calc(double x, double y) {
  return fabs(
    sqrt(sqr(x - a) + sqr(y)) -
    sqrt(sqr(x + a) + sqr(y)));
}
double sanfen(double l, double r) {
  while (r - l > 1e-6) {
    double m1 = l + (r - l) / 3, m2 = r - (r - l) / 3;
    double ans1 = calc(m1, k*m1+b), ans2 = calc(m2, k*m2+b);
    // printf("%.10lf %.10lf\n", ans1, ans2);
    if (ans1 > ans2) r = m2;
    else             l = m1;
  }
  double res = calc(l, k*l+b);
  return res;
}
double work(int i, int j) {
  if (x[i] == x[j]) return fabs(fabs(a - x[i]) - fabs(a + x[i]));
  k = 1. * (y[i] - y[j]) / (x[i] - x[j]);
  b = y[i] - x[i] * k;
  return fmax(sanfen(-1e8, 0), sanfen(0, 1e8));
}
int main(int argc, char const *argv[]) {
  freopen("distance.in", "r", stdin);
  freopen("distance.out", "w", stdout);
  n = read(); a = read();
  // for (int i = -1000; i <= 1000; i++)
  //   printf("%d -> %.8lf\n", i, calc(1, -1, i));
  for (int i = 1; i <= n; i++)
    x[i] = read(), y[i] = read();
  double ans = 1e60;
  for (int i = 1; i <= n; i++)
    for (int j = i + 1; j <= n; j++)
      ans = min(ans, work(i, j));
  printf("%.10lf\n", ans);
  return 0;
}