#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int head[N], to[N<<1], nxt[N<<1], cnt = 1;
void insert(int x, int y) {
  to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}
int fa[N], ed[N];
void dfs(int x, int f, int e) {
  fa[x] = f; ed[x] = e;
  for (int i = head[x]; i; i = nxt[i])
    if (to[i] != f) dfs(to[i], x, i);
}
std::vector<pair<double, double> > ass[N<<1];
int t[N], v[N], u[N], c[N];
int main(int argc, char const *argv[]) {
  freopen("meet.in", "r", stdin);
  freopen("meet.out", "w", stdout);
  int n = read(), m = read();
  for (int i = 1; i < n; i++)
    insert(read(), read());
  for (int i = 1; i <= m; i++) {
    t[i] = read(); c[i] = read();
    v[i] = read(); u[i] = read();
    dfs(u[i], 0, 0);
    int x = v[i];
    double ts = t[i];
    double as = 1. / c[i];
    while (x != u[i]) {
      ass[ed[x]].push_back(make_pair(ts, ts + as));
      ts += as;
      x = fa[x];
    }
  }
  double ans = 1e60;
  for (int i = 2; i <= cnt; i += 2) {
    int sza = ass[i].size();
    int szb = ass[i + 1].size();
    for (int a = 0; a < sza; a++) {
      for (int b = a + 1; b < sza; b++) {
        double t1 = ass[i][a].first;
        double t4 = ass[i][a].second;
        double t2 = ass[i][b].first;
        double t3 = ass[i][b].second;
        double v1 = 1. / (t3 - t2);
        double v2 = 1. / (t4 - t1);
        // all compose v1 > v2
        if (v1 < v2) {
          swap(t1, t2);
          swap(t3, t4);
          swap(v1, v2);
        }
        if (v1 == v2) continue;
        if (t1 < t2) {
          double tx = (v1 * t2 - v2 * t1) / (v1 - v2);
          if (tx > fmin(t3, t4) || tx * v2 > 1 || tx * v1 > 1)
            continue;
          ans = fmin(ans, tx);
        }
      }
    }
    for (int a = 0; a < szb; a++) {
      for (int b = a + 1; b < szb; b++) {
        double t1 = ass[i + 1][a].first;
        double t4 = ass[i + 1][a].second;
        double t2 = ass[i + 1][b].first;
        double t3 = ass[i + 1][b].second;
        double v1 = 1. / (t3 - t2);
        double v2 = 1. / (t4 - t1);
        // all compose v1 > v2
        if (v1 < v2) {
          swap(t1, t2);
          swap(t3, t4);
          swap(v1, v2);
        }
        if (v1 == v2) continue;
        if (t1 <= t2) {
          double tx = (v1 * t2 - v2 * t1) / (v1 - v2);
          if (tx > fmin(t3, t4) || tx * v2 > 1 || tx * v1 > 1)
            continue;
          ans = fmin(ans, tx);
        }
      }
    }
    for (int a = 0; a < sza; a++) {
      for (int b = 0; b < szb; b++) {
        double t1 = ass[i][a].first;
        double t2 = ass[i][a].second;
        double t3 = ass[i + 1][b].first;
        double t4 = ass[i + 1][b].second;
        if (fmax(t1, t3) < fmin(t2, t4)) { // 有交
          double v1 = 1. / (t2 - t1);
          double v2 = 1. / (t4 - t3);
          double tx = (v1*t1+v2*t3+1)/(v1+v2);
          ans = fmin(ans, tx);
        }
      }
    }
  }
  if (ans == 1e60) puts("-1");
  else printf("%.10lf\n", ans);
  return 0;
}