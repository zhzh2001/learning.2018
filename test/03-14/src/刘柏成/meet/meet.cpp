#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
const int MAXSIZE=10000020;
const db eps=1e-8;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=100002;
const int maxm=200002;
db dcmp(db x){
	if (fabs(x)<eps)
		return 0;
	return x>0?1:-1;
}
struct seg{
	db l,r;
	seg():l(0),r(1e9){}
	seg(db x):l(x),r(x){}
	seg(db l,db r):l(l),r(r){}
	seg operator &(const seg& rhs)const{
		return seg(max(l,rhs.l),min(r,rhs.r));
	}
};
struct graph{
	int n,m;
	struct edge{
		int to,next;
	}e[maxm];
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	int sz[maxn],fa[maxn],dep[maxn],top[maxn],son[maxn];
	void dfs(int u){
		sz[u]=1;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (sz[v])
				continue;
			fa[v]=u;
			dep[v]=dep[u]+1;
			dfs(v);
			sz[u]+=sz[v];
			if (sz[v]>sz[son[u]])
				son[u]=v;
		}
	}
	void dfs2(int u,int tp){
		top[u]=tp;
		if (son[u])
			dfs2(son[u],tp);
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (!top[v])
				dfs2(v,v);
		}
	}
	int lca(int u,int v){
		// fprintf(stderr,"lca %d %d\n",u,v);
		for(;top[u]!=top[v];u=fa[top[u]])
			if (dep[top[u]]<dep[top[v]])
				swap(u,v);
		return dep[u]<dep[v]?u:v;
	}
	void prepare(){
		dfs(1);
		dfs2(1,1);
	}
	seg get(int u,db x,int su,bool f1,int v,db y,int sv,bool f2){ //true : root to u,false:u to root
		if (f1){
			swap(u,v);
			swap(su,sv);
			swap(x,y);
			swap(f1,f2);
		}
		int lc=lca(u,v);
		x-=y;
		// printf("get %d %.3f %d %d %d %.3f %d %d\n",u,x,su,f1,v,y,sv,f2);
		if (f1){
			if (su==sv)
				return dcmp(x)?seg(1,0):seg(y,(db)dep[lc]/su+y);
			db ans=(x*su)/(su-sv);
			if (ans<0 || ans<x || (ans-x)*su>dep[lc])
				return seg(1,0);
			return ans+y;
		}
		if (f2){
			db ans=(dep[u]+x*su)/(su+sv);
			if (ans<0 || ans<x || ans*sv>dep[lc])
				return seg(1,0);
			return ans+y;
		}
		if (su==sv)
			return dcmp(dep[u]-dep[v]+x*su)?seg(1,0):seg(db(dep[v]-dep[lc])/sv+y,(db)dep[v]/sv+y);
		db ans=(dep[u]-dep[v]+x*su)/(su-sv);
		// printf("ans=%.2f\n",ans);
		if (ans<0 || ans<x || dep[u]-(ans-x)*su>dep[lc])
			return seg(1,0);
		return ans+y;
	}
	struct query{
		int id,u,v;
		db t;
		int c;
		bool operator <(const query& rhs)const{
			return c<rhs.c;
		}
	}q[maxm];
	int cur,now;
	void add(int u,int v,int t,int c){
		int lc=lca(u,v);
		now++;
		q[++cur]=(query){now,u,lc,(db)t,c};
		q[++cur]=(query){now,lc,v,t+(db)(dep[u]-dep[lc])/c,c};
	}
	seg s[maxm];
	db work(){
		db res=1e9;
		sort(q+1,q+cur+1);
		for(int i=1;i<=cur;i++)
			s[i]=seg(q[i].t,fabs(dep[q[i].u]-dep[q[i].v])/q[i].c+q[i].t);
		for(int i=1;i<=cur;i++){
			int u=dep[q[i].u]>dep[q[i].v]?q[i].u:q[i].v;
			db x=dep[q[i].u]>dep[q[i].v]?q[i].t:q[i].t-(db)dep[q[i].u]/q[i].c;
			// printf("%d x=%.5f q[i].t=%.5f dep[u]=%d dep[v]=%d\n",i,x,q[i].t,dep[q[i].u],dep[q[i].v]);
			for(int j=i-1;j>=1;j--){
				if (cur>10000 && q[i].c!=q[j].c)
					break;
				if (q[i].id==q[j].id)
					continue;
				int v=dep[q[j].u]>dep[q[j].v]?q[j].u:q[j].v;
				db y=dep[q[j].u]>dep[q[j].v]?q[j].t:q[j].t-(db)dep[q[j].u]/q[j].c;
				seg ans=get(u,x,q[i].c,dep[q[i].u]<=dep[q[i].v],v,y,q[j].c,dep[q[j].u]<=dep[q[j].v]);
				// printf("%.5f %.5f\n",ans.l,ans.r);
				ans=ans&s[i];
				ans=ans&s[j];
				if (dcmp(ans.r-ans.l)>=0)
					res=min(res,ans.l);
				// if (!dcmp(ans.l-9))
					// fprintf(stderr,"%d %d\n",i,j);
			}
		}
		return res;
	}
}g;
int main(){
	init();
	int n=readint(),m=readint();
	for(int i=1;i<n;i++){
		int u=readint(),v=readint();
		g.addedge(u,v);
		g.addedge(v,u);
	}
	g.prepare();
	// fprintf(stderr,"WTF");
	while(m--){
		int t=readint(),c=readint(),u=readint(),v=readint();
		g.add(u,v,t,c);
	}
	// fprintf(stderr,"WTF");
	db ans=g.work();
	if (ans>=1e8)
		puts("-1");
	else printf("%.7f\n",ans);
}