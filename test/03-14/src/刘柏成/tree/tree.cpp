#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=200002;
int fa[maxn];
struct request{
	int fr,to;
	bool operator <(const request& rhs)const{
		return fr<rhs.fr;
	}
}req[maxn],nreq[maxn];
int cr;
int fr[maxn];
int ans[maxn],nans[maxn],ca;
int res[maxn],cnt;
int now;
void nxt(){
	now++;
	int ncr=0,nca=0;
	sort(ans+1,ans+ca+1);
	for(int i=1;i<=ca;i++){
		int u=ans[i];
		// assert(wait[u]);
		// fprintf(stderr,"on %d\n",u);
		if (fr[u]<0){
			// fprintf(stderr,"%d %d %d\n",u,fr[u],now);
			res[-fr[u]]=now;
			fr[u]=0;
			cnt--;
			continue;
		}
		// if (u && !fr[u])
			// fprintf(stderr,"%d %d\n",u,fr[u]);
		nans[++nca]=fr[u];
		fr[u]=0;
	}
	// fprintf(stderr,"%d\n",cr);
	sort(req+1,req+cr+1);
	for(int i=1;i<=cr;i++){
		int u=req[i].fr,v=req[i].to;
		// if (!fr[u])
			// continue;
		// fprintf(stderr,"request %d %d\n",u,v);
		if (fr[v] || !v){
			// if (!u)
				// fprintf(stderr,"0 to %d\n",v);
			nans[++nca]=u;
			continue;
		}
		fr[v]=u;
		nreq[++ncr]=(request){v,fa[v]};
	}
	ca=nca,cr=ncr;
	for(int i=1;i<=nca;i++)
		ans[i]=nans[i];
	for(int i=1;i<=ncr;i++)
		req[i]=nreq[i];
}
int main(){
	init();
	int n=readint(),m=readint();
	for(int i=1;i<=n;i++)
		fa[i]=readint();
	cnt=m;
	for(int i=1;i<=m;i++){
		int x=readint(),t=readint();
		while(now!=t-1){
			nxt();
			if (!ca && !cr){
				now=t-1;
				break;
			}
		}
		fr[x]=-i;
		req[++cr]=(request){x,fa[x]};
		nxt();
	}
	while(cnt){
		// assert(cr+ca);
		nxt();
	}
	for(int i=1;i<=m;i++)
		printf("%d ",res[i]+1);
}