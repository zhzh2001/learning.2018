#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
inline void write_p(int x){write(x);putchar(' ');}
const int N=200055;
bool vis[N];
int dep[N],cnt,nxt[N],head[N],poi[N],Fa[N][20],ans[N],n,m;

inline void add(int x,int y)
{
	poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;
	poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;
}
inline void Dfs(int x,int fa)
{
	Fa[x][0]=fa;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		dep[poi[i]]=dep[x]+1;
		Dfs(poi[i],x);
	}
}
inline void Pre()
{
	For(j,1,18)	
		For(i,1,n)	Fa[i][j]=Fa[Fa[i][j-1]][j-1];
}
inline int lca(int x,int y)
{
	if(dep[x]<dep[y])	swap(x,y);
	int del=int(dep[x]-dep[y]+0.5);
	For(i,0,18)	if(del>>i&1)	x=Fa[x][i];
	if(x==y)	return x;
	Dow(i,0,18)	if(Fa[x][i]!=Fa[y][i])	x=Fa[x][i],y=Fa[y][i];
	return Fa[x][0];
}
inline int up(int x,int del){int t=x;For(i,0,18)	if(del>>i&1)	t=Fa[t][i];return t;}
struct que{int x,t,num;} q[N];
inline bool cmp(que x,que y){return x.t<y.t;}
struct node{int x,zt,num,ok;node(int _x=0,int _zt=0,int _num=0,int _ok=0){x=_x;zt=_zt;num=_num;ok=_ok;}}Q[N];
inline bool cmp1(node x,node y){return x.ok==y.ok?x.zt==y.zt?x.x<y.x:x.zt<y.zt:x.ok>y.ok;}
int main()
{
	freopen("tree.in","r",stdin);freopen("tree.out","w",stdout);
	n=read()+1;m=read();
	For(i,1,n-1)	add(i+1,read()+1);
	Dfs(1,1);
	Pre();
	For(i,1,m)	q[i].x=read()+1,q[i].t=read(),q[i].num=i;
	sort(q+1,q+m+1,cmp);
	int now=q[1].t,tep=1,l=1,r=0;
	while(1)
	{
		sort(Q+l,Q+r+1,cmp1);
		while(l<=r&&Q[l].ok)	l++;
		For(i,l,r)
		{
			if(Q[i].zt==1)
			{	
				Q[i].x=Fa[Q[i].x][0];
				if(vis[Q[i].x]||Q[i].x==1)	Q[i].zt=-1;
				vis[Q[i].x]=1;
			}
			else
			{
				vis[Q[i].x]=0;
				Q[i].x=up(q[Q[i].num].x,dep[q[Q[i].num].x]-dep[Q[i].x]-1);
				if(Q[i].x==q[Q[i].num].x)	vis[Q[i].x]=0,ans[Q[i].num]=now,Q[i].ok=1;
			}
		}
		while(tep<=m&&q[tep].t<=now)	
		{
			if(vis[q[tep].x])	ans[q[tep].num]=q[tep].t,tep++;else
			Q[++r]=node(q[tep].x,1,q[tep].num,0),vis[q[tep].x]=1,tep++;
		}
		if(l>r)	if(tep==m+1)	break;else now=q[tep+1].t;
		if(l<=r)	now++;
	}
	For(i,1,m)	write_p(ans[i]);
}
/*
6 3
0 1 2 3 2 5
4 6
6 9
5 11

3 2
0 1 1
2 1
3 1

8 3
0 1 1 2 3 3 4 5
6 1
8 2
4 5
*/