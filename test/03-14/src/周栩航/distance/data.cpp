#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
inline void write_p(int x){write(x);putchar(' ');}
int n,a,x,y;
map<int,int> vis[2001];
int main()
{
	freopen("distance.in","w",stdout);
	srand(time(0));
	n=5000;a=rand()%500;
	cout<<n<<' '<<a<<endl;
	For(i,1,n)
	{
		x=rand()%1000-500,y=rand()%1000-500;
		while(vis[x+501][y])	x=rand()%1000-500,y=rand()%1000-500;
		vis[x+501][y]=1;
		printf("%d %d\n",x,y);		
	}
}