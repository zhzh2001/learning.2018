#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
inline void write_p(int x){write(x);putchar(' ');}
double ans=1000000000;
double x[110001],y[110001],a,k,b;
int n,t[110001];
inline double dis(double x1,double y1,double x2,double y2){return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));}
inline double Getx(double X)
{
	double Y=k*X+b;
	return abs(dis(X,Y,a,0)-dis(X,Y,-a,0));
}
inline void Solve_x(int p1,int p2)
{
	k=(y[p1]-y[p2])/(x[p1]-x[p2]),b=y[p1]-k*x[p1];
	double l=-10000,r=10000;
	For(T,1,60)
	{
		double mid1=(l+r)/2,mid2=(mid1+r)/2;
		if(Getx(mid1)>=ans)	return;
		if(Getx(mid1)>Getx(mid2))	r=mid2;else	l=mid1;
	}
	ans=min(ans,Getx(l));
}
inline double Gety(double Y,int p1)
{
	double X=x[p1];
	return abs(dis(X,Y,a,0)-dis(X,Y,-a,0));
}
inline void Solve_y(int p1,int p2)
{
	double l=-10000,r=10000;
	For(T,1,60)
	{
		double mid1=(l+r)/2,mid2=(mid1+r)/2;
		if(Gety(mid1,p1)>=ans)	return;	
		if(Gety(mid1,p1)>Gety(mid2,p2))	r=mid2;else	l=mid1;
	}
	ans=min(ans,Gety(l,p1));
}
inline void Solve(int p1,int p2)
{
	if(x[p1]!=x[p2])	Solve_x(p1,p2);else	Solve_y(p1,p2);
}
int main()
{
	freopen("distance.in","r",stdin);freopen("distance.out","w",stdout);
	n=read();a=read();
	For(i,1,n)	x[i]=read(),y[i]=read();
	For(i,1,n)	t[i]=i;random_shuffle(t+1,t+n+1);
	int tep=min(n,5000);
	For(i,1,tep)
		For(j,max(i+1,n-5000),n)	Solve(t[i],t[j]);
	printf("%.20lf",ans);
}
