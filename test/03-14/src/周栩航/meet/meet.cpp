#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
#define eps 1e-8
#define y1 orzwzp
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
inline void write_p(int x){write(x);putchar(' ');}
const int N=200005;
int poi[N],nxt[N],head[N],Fa[N][20],cnt,in[N],out[N],m,n,tim;
struct node{int t,s,e;double v;}	p[N];
inline void add(int x,int y)
{
	poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;
	poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;
}
double dep[100001],ans=1000000000;
inline void Dfs(int x,int fa)
{
	in[x]=++tim;
	Fa[x][0]=fa;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		dep[poi[i]]=dep[x]+1;
		Dfs(poi[i],x);
	}
	out[x]=++tim;
}
inline void Pre()
{
	For(j,1,18)	
		For(i,1,n)	Fa[i][j]=Fa[Fa[i][j-1]][j-1];
}
inline int lca(int x,int y)
{
	if(dep[x]<dep[y])	swap(x,y);
	int del=int(dep[x]-dep[y]+0.5);
	For(i,0,18)	if(del>>i&1)	x=Fa[x][i];
	if(x==y)	return x;
	Dow(i,0,18)	if(Fa[x][i]!=Fa[y][i])	x=Fa[x][i],y=Fa[y][i];
	return Fa[x][0];
}
inline int up(int x,int del){int t=x;For(i,0,18)	if(del>>i&1)	t=Fa[t][i];return t;}
inline bool inc(int x,int y){return in[x]<=in[y]&&out[y]<=out[x];}//x包含y
inline bool cmp(node x,node y){return x.t<y.t;}
inline void Solve(int p1,int p2)
{
	//jump
	int d_time=p[p2].t-p[p1].t,t_dis=d_time*int(p[p1].v+0.5);
	int x1=p[p1].s,y1=p[p1].e,x2=p[p2].s,y2=p[p2].e;double v1=p[p1].v,v2=p[p2].v;
	int m1=lca(x1,y1),m2=lca(x2,y2);
	if(dep[x1]-dep[m1]<=t_dis)	
	{
		t_dis-=(dep[x1]-dep[m1]),x1=m1;
		if(dep[y1]-dep[m1]<=t_dis)	return;else	x1=up(y1,(dep[y1]-dep[m1])-t_dis);
	}else	x1=up(x1,t_dis);
	m1=lca(x1,y1);
	if(!inc(m1,m2))	if(!inc(m2,m1))	return;
	double TIM=p[p2].t;
	//up with up
	int t_p=lca(x1,x2);
	double t1=0,t2=0;
	if(inc(m1,t_p)&&inc(m2,t_p))
	{
		t1=(dep[x1]-dep[t_p])/v1;t2=(dep[x2]-dep[t_p])/v2;
		if(t1==t2)	{ans=min(ans,TIM+t1);return;}
		if(t1>t2)	swap(x1,x2),swap(y1,y2),swap(v1,v2),swap(t1,t2);
		x1=t_p;TIM+=t1;
	}
	if(inc(x1,x2))
	{
		if(v1<v2)
		{
			double tt1=(dep[x1]-dep[m1])/v1,tt2=(dep[x2]-dep[m2])/v2-t1;
			double dis=dep[x2]-dep[x1]-t1*v2;
			if(dis/(v2-v1)<=min(tt1,tt2))	{ans=min(ans,TIM+dis/(v2-v1));return;}
		}
	}
	//up with down
	t1=(dep[x1]-dep[m1])/v1,t2=(dep[x2]-dep[m2])/v2;
	if(t1>t2)	swap(x1,x2),swap(y1,y2),swap(v1,v2),swap(t1,t2);
	x1=m1;TIM+=t1;//x2点已走t1
	if(inc(x1,x2))
	{
		int tp=lca(x2,y1);
		if(inc(m2,tp))
		{
			double can_TIM=min((dep[tp]-dep[x1])/v1,(dep[x2]-dep[m2])/v2-t1);
			if((dep[x2]-dep[x1]-t1*v2)/(v1+v2)<=can_TIM-eps)
				{ans=min(ans,TIM+(dep[x2]-dep[x1]-t1*v2)/(v1+v2));return;}
		}
	}
	//down with down
	x2=m2;TIM+=t2-t1;
	t1=t2-t1;//x1点已走t1
	if(inc(x2,x1))
	{
		int tt_p=lca(y1,y2);
		if(v2>v1)
		{
			double can_TIM=min((dep[tt_p]-dep[x1])/v1-t1,(dep[tt_p]-dep[x2])/v2);
			if((dep[x1]-dep[x2]+t1*v1)/(v2-v1)<=can_TIM-eps)	
				{ans=min(ans,TIM+(dep[x1]-dep[x2]+t1*v1)/(v2-v1));return;}
		}
	}
}
int main()
{
	freopen("meet.in","r",stdin);freopen("meet.out","w",stdout);
	n=read();m=read();
	For(i,1,n-1)	add(read(),read());
	dep[1]=1;
	Dfs(1,1);
	Pre();
	For(i,1,m)	p[i].t=read(),p[i].v=read(),p[i].s=read(),p[i].e=read();
	sort(p+1,p+m+1,cmp);
	For(i,1,m)	{if(p[i].t>ans)	break;For(j,i+1,m)	Solve(i,j);}
	if(ans>99999999)	puts("-1");else	printf("%.20lf",ans);
}