#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;


inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

double X;
struct node{
	double x,y;
}a[10008];
double eps=1e-8;
double min(double x,double y){
	if (x<y) return x; return y;
}
double res=100000000;
double doit(double x,double k,double b){
	double y=x*k+b;
	return abs(sqrt((X-x)*(X-x)+y*y)-sqrt((-X-x)*(-X-x)+y*y));
}
void Check(node x,node y){
	double l=-100000,r=-l,ans;
	double k=(y.y-x.y)/(y.x-x.x);
	double b=x.y-x.x*k;
	while (l+eps<r){
		double mid1=(l+r)/2;
		double mid2=(r+mid1)/2;
		if (doit(mid1,k,b)+eps>doit(mid2,k,b)) r=mid2,ans=mid1;
		else l=mid1,ans=mid2;
	}
	double ret=doit(ans,k,b);
	res=min(res,ret);
}
int n;

int main(){

	// say hello


	freopen("distance.in","r",stdin);
	freopen("distance.out","w",stdout);
	
	scanf("%d%lf",&n,&X);
	For(i,1,n) scanf("%lf%lf",&a[i].x,&a[i].y);
	double res1=res;
	For(i,1,n) For(j,i+1,n) Check(a[i],a[j]);
	if (res==res1) printf("-1");
	else printf("%.8lf",res);


	// say goodbye

}

