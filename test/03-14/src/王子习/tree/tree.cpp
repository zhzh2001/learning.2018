#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;


inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

#include<queue>

struct node{
	int now,flag,u,last,idx,lastvis;
};
struct cmp{
	bool operator ()(node x,node y){
		if (x.flag!=y.flag) return x.flag>y.flag;
		else return x.u>y.u;
	}
};
priority_queue <node,vector<node>,cmp > q;
const int N=10007;
int n,m,fa[N],vis[N],pre[N],res[N];
struct Ask_{
	int x,t,id;
}a[N];
bool operator <(Ask_ x,Ask_ y){
	if (y.t!=x.t) return x.t<y.t;
	else return x.x<y.x; 
}

int main(){

	// say hello


	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);

	n=read()+1,m=read();
	For(i,2,n) fa[i]=read()+1;
	For(i,1,m) a[i].x=read()+1,a[i].t=read(),a[i].id=i;
	sort(a+1,a+1+m);
	int now=0,pri=1;
//	For(i,1,m) printf("%d %d\n",a[i].x,a[i].t);
	vis[1]=1;
	while (!q.empty()||pri<=m){
		now++;
		priority_queue <node,vector<node>,cmp > q1;
		while (!q.empty()){
			node x=q.top();
			q.pop();
			if (x.lastvis) x.lastvis=0,swap(x.now,x.last);
			else {
				x.last=x.now;
				if (x.flag) x.now=pre[x.now];
				else x.now=fa[x.now];			
			}
			
	//		printf("%d  vis=%d\n",x.now,vis[x.now]);
			if (vis[x.now]){
				if (!x.flag) x.flag=1,x.lastvis=1;
				else if (x.now!=1) vis[x.now]=0;
			}
			else {
				pre[x.now]=x.last;
	//			printf("%d last=%d\n",x.now,x.last);
				vis[x.now]=1;
			}
	//		printf("TIM=%d   NOW=%d LAST=%d FLAG=%d TOP=%d\n",now,x.now,x.last,x.flag,x.u);
			if (x.now==x.u) res[x.idx]=now;
			else q1.push(x);
		}
		q=q1;
		if (q.empty()) now=a[pri].t;
		while (a[pri].t<=now&&pri<=m){
			node x;
			x.now=a[pri].x;
			x.flag=0;
			x.u=a[pri].x;
			x.idx=a[pri].id;
			x.lastvis=0;
			x.last=x.now;
			pri++;
			if (!vis[x.now]) q.push(x);
			else res[x.idx]=now;
			vis[x.now]=1;
		}
	}
	For(i,1,m) printf("%d ",res[i]);

	// say goodbye

}

