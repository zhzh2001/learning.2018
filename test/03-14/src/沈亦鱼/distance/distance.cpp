#include<cmath>
#include<cstdio>
int n,mm,a[210000],b[210000];
double m,ans,x,y,a1,b1,a2,b2,a3,b3;
/*
void sor(int l,int r){
	int x=a[(l+r)>>1],i=l,j=r;
	while(i<j){
		while(a[i]<x)i++;
		while(x<a[j])j--;
		if(i<=j){
			swap(a[i],a[j]);
			swap(b[i],b[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}
*/
int main(){
	freopen("distance.in","r",stdin);
	freopen("distance.out","w",stdout);
	scanf("%d%d",&n,&mm);
	m=abs(mm);
	for(int i=1;i<=n;i++)
		scanf("%d%d",&a[i],&b[i]);
/*
	sor(1,n);
	for(int i=1;i<=n;i++){
		if(a[i]>-m&&!loc1)loc1=i;
		if(a[i]>m&&!loc2)loc2=i;
	}
*/
	ans=1e9;
	for(int i=1;i<n;i++)
		for(int j=i+1;j<=n;j++){
			if(b[i]==b[j]){
				if(2*m<ans)ans=2*m;
				continue;
			}
			if(a[i]==a[j]){
				if(fabs(fabs(m-a[i])-fabs(m+a[i]))<ans)ans=fabs(fabs(m-a[i])-fabs(m+a[i]));
				continue;
			}
			x=a[i]-a[j];
			y=b[i]-b[j];
			a1=y/x;
			b1=b[i]-a1*a[i];
			x=-b1/a1;
			if(x<-m&&x>m){
				if(2*m<ans)ans=2*m;
				continue;
			}
			a2=-1/a1;
			b2=-m*a2;
			x=(b2-b1)/(a1-a2);
			y=m-x;
			x-=y;
			y=x*a2+b2;
			a3=y/(x+m);
			b3=m*a3;
			x=(b3-b1)/(a1-a3);
			y=x*a1+b1;
			if(fabs(sqrt((x+m)*(x+m)+y*y)-sqrt((x-m)*(x-m)+y*y))<ans)
				ans=fabs(sqrt((x+m)*(x+m)+y*y)-sqrt((x-m)*(x-m)+y*y));
		}
	printf("%0.10f",ans);
	return 0;
}
/*
������1���롿
2 5
1 0
2 1
������1�����
7.2111025509
*/
