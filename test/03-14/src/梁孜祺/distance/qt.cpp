#include<bits/stdc++.h>
#define ll long long
#define eps 1e-8
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 100005
using namespace std;
int p[5][2];
struct data{
	double x,y;
	int id;
}a[N];
int n,m;
inline double sqr(double x){return x*x;}
inline double dis(data a,data b){
	return sqrt(sqr(a.x-b.x)+sqr(a.y-b.y));
}
inline bool operator >(data a,data b){
	if(a.y>=0) return (1.0*a.x/dis(a,(data){0,0})>1.0*b.x/dis(b,(data){0,0}));
	else return (1.0*a.x/dis(a,(data){0,0})<1.0*b.x/dis(b,(data){0,0}));
}
inline bool operator <(data a,data b){
	return (p[a.x>=0][a.y>=0]==p[b.x>=0][b.y>=0])?(a>b):(p[a.x>=0][a.y>=0]<p[b.x>=0][b.y>=0]);
}
double ans;
int L,R;
inline void solve(data P,data Q){
	if(P.y==Q.y){
		ans=min(ans,1.0*abs(P.y));
		return;
	}else if(P.x==Q.x){
		if(P.x<L||P.x>R) ans=min(ans,1.0*min(abs(P.x-L),abs(P.x-R)));
		else ans=0.0;
		return;
	}else{
		double k=1.0*(Q.y-P.y)/(Q.x-P.x),b=1.0*P.y-P.x*k;//y=kx+b
	//	cout<<k<<" "<<b<<endl;
		double lx=(1.0*(Q.x-P.x)*R-1.0*(Q.y-P.y)*b)/(1.0*Q.x-P.x+k*(Q.y-P.y)),ly=k*lx+b;
		double rx=(1.0*(Q.x-P.x)*L-1.0*(Q.y-P.y)*b)/(1.0*Q.x-P.x+k*(Q.y-P.y)),ry=k*rx+b;
		
		while(rx-lx>eps){
			double mid1x=lx+(rx-lx)/3,mid2x=rx-(rx-lx)/3;
			double mid1y=ly+(ry-ly)/3,mid2y=ry-(ry-ly)/3;
			if(dis((data){mid1x,mid1y},(data){1.0*L,0})+dis((data){mid1x,mid1y},(data){1.0*R,0})<
			   dis((data){mid2x,mid2y},(data){1.0*L,0})+dis((data){mid2x,mid2y},(data){1.0*R,0})){
				rx=mid2x;ry=mid2y;
			}else{
				lx=mid1x;ly=mid1y;
			}
		}
		cout<<lx<<" "<<rx<<endl;
		ans=min(ans,dis((data){lx,ly},(data){1.0*L,0})+dis((data){lx,ly},(data){1.0*R,0}));
	}
}
int main(){
	scanf("%d%d",&n,&m);ans=1000000000.0;
	For(i,1,n) scanf("%lf%lf",&a[i].x,&a[i].y),a[i].id=i;
	p[1][1]=1;p[0][1]=2;p[0][0]=3;p[1][0]=4;
	For(i,1,n) a[i].x-=m;int tot=n;
	For(i,1,n) a[++tot]=(data){-a[i].x,-a[i].y,-i};
	sort(a+1,a+1+n);L=-m*2,R=0;
	For(i,1,n){
		double X,Y;
		if(a[i].id>0) X=a[i].x,Y=a[i].y;
		else X=-a[i].x,Y=-a[i].y;
		int l=(i==1)?n:i-1,r=(i==n)?1:i+1;
		while(a[l].id<0){
			l--;if(l<1) l=n;
		}
		while(a[r].id<0){
			r++;if(r>n) r=1;
		}
		solve(a[l],(data){X,Y});
		solve(a[r],(data){X,Y});
	}tot=0;
	For(i,1,n*2) if(a[i].id>0) a[++tot]=a[i];
	For(i,1,n) a[i].x+=2*m;
	For(i,1,n) a[++tot]=(data){-a[i].x,-a[i].y,-i};
	sort(a+1,a+1+n);L=0,R=m*2;
	For(i,1,n){
		double X,Y;
		if(a[i].id>0) X=a[i].x,Y=a[i].y;
		else X=-a[i].x,Y=-a[i].y;
		int l=(i==1)?n:i-1,r=(i==n)?1:i+1;
		while(a[l].id<0){
			l--;if(l<1) l=n;
		}
		while(a[r].id<0){
			r++;if(r>n) r=1;
		}
		solve(a[l],(data){X,Y});
		solve(a[r],(data){X,Y});
	}
	printf("%.10lf\n",ans);
	return 0;
}
