#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 100005
using namespace std;
int n,m,head[N],cnt;
struct edge{int nxt,to;}e[N<<1];
inline void insert(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
	e[++cnt]=(edge){head[v],u};head[v]=cnt;
}
struct que{
	int t,c,u,v;
	bool operator <(const que &a)const{
		return t<a.t;
	}
}q[N];
int fa[N][30],bin[30],dep[N];
inline void dfs(int x,int f){
	For(i,1,20){
		if(dep[x]<bin[i]) break;
		fa[x][i]=fa[fa[x][i-1]][i-1];
	}
	for(int i=head[x];i;i=e[i].nxt)
		if(e[i].to!=f){
			dep[e[i].to]=dep[x]+1;fa[e[i].to][0]=x;
			dfs(e[i].to,x);
		}
}
inline int lca(int x,int y){
	if(dep[x]<dep[y]) swap(x,y);
	int del=dep[x]-dep[y];
	For(i,0,20) if(bin[i]&del) x=fa[x][i];
	Rep(i,20,0) if(fa[x][i]!=fa[y][i]) x=fa[x][i],y=fa[y][i];
	if(x==y) return x;
	return fa[x][0];
}
int main(){
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	puts("-1");
	return 0;
}
