#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define inf 1e9
#define N 100005
using namespace std;
int n,m,cnt,head[N];
struct edge{int nxt,to;}e[N<<1];
inline void insert(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
	e[++cnt]=(edge){head[v],u};head[v]=cnt;
}
struct que{
	int t,c,u,v;
	bool operator <(const que &a)const{
		return t<a.t;
	}
}q[N];
int fa[N][30],bin[30],dep[N];
inline void dfs(int x,int f){
	For(i,1,20){
		if(dep[x]<bin[i]) break;
		fa[x][i]=fa[fa[x][i-1]][i-1];
	}
	for(int i=head[x];i;i=e[i].nxt)
		if(e[i].to!=f){
			dep[e[i].to]=dep[x]+1;fa[e[i].to][0]=x;
			dfs(e[i].to,x);
		}
}
inline int lca(int x,int y){
	if(dep[x]<dep[y]) swap(x,y);
	int del=dep[x]-dep[y];
	For(i,0,20) if(bin[i]&del) x=fa[x][i];
	Rep(i,20,0) if(fa[x][i]!=fa[y][i]) x=fa[x][i],y=fa[y][i];
	if(x==y) return x;
	return fa[x][0];
}
struct data{
	double t;int c,u,v,lca;
	bool operator <(const data &a)const{
		return t<a.t;
	}
};
double ans=1000000000.0;
vector <data> a[N];
inline void sw(int x,int f){
	for(int i=head[x];i;i=e[i].nxt)
		if(e[i].to!=f){
			int y=e[i].to;dfs(y,x);
			for(int j=0;j<a[y].size();j++) a[y][j].t+=1.0/a[y][j].c;
			for(int j=1;j<a[y].size();j++) if(a[y][j].t<=a[y][j-1].t){
				double t1=a[y][j].t-1.0/a[y][j].c,t2=a[y][j-1].t-1.0/a[y][j-1].c;
				ans=min(ans,)
				a[y][j-1].t=1e9;
			}
		}
	if(!a[x].size()) return;
	for(int i=0;i<a[x].size();i++) if(a[x][i].lca==x) a[x][i].t=1e9;
	sort(a[x].begin(),a[x].end());
	for(int i=a[x].size()-1;i>=0;i--) if(a[x][i].t==inf) a[x].pop_back();
}
int main(){
	bin[0]=1;For(i,1,20) bin[i]=bin[i-1]<<1;
	scanf("%d%d",&n,&m);
	For(i,2,n){
		int x,y;scanf("%d%d",&x,&y);
		insert(x,y);
	}
	dfs(1,0);
	For(i,1,m) scanf("%d%d%d%d",&q[i].t,&q[i].c,&q[i].u,&q[i].v);
	sort(q+1,q+1+m);
	For(i,1,m){
		int Lca=lca(q[i].u,q[i].v);
		a[i].push_back((data){q[i].t,q[i].c,q[i].u,q[i].v,Lca});
	}
	sw(1,0);
	return 0;
}
