#include<bits/stdc++.h>
using namespace std;
const int N=200005;
struct data
{
	int x,t,y,z,pos;
	bool operator<(const data &a)const
	{
		return y==a.y?y>a.y:x<a.x;
	}
}a[N],tmp;
vector<int> v[2];
vector<data> f[N][2];
int n,m,fa[N],ans[N],opt,t,k,from[N];
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
		putchar('0');
	int a[15];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar(' ');
}
inline void work(int x)
{
	// cout<<t<<' '<<x<<'\n';
	sort(f[x][opt].begin(),f[x][opt].end());
	for (unsigned i=0;i<f[x][opt].size();++i)
	{
		if (f[x][opt][i].y)
		{
			if (f[x][opt][i].x==x)
				ans[f[x][opt][i].pos]=t;
			else
			{
				f[from[x]][!opt].push_back(f[x][opt][i]);
				v[!opt].push_back(from[x]);
				from[x]=0;
			}
		}
		else
		{
			if (from[x]||x==1)
			{
				tmp=f[x][opt][i];
				tmp.y=1;
				if (tmp.x==x)
					ans[tmp.pos]=t;
				else
				{
					f[tmp.z][!opt].push_back(tmp);
					v[!opt].push_back(tmp.z);
				}
			}
			else
			{
				tmp=f[x][opt][i];
				from[x]=tmp.z;
				tmp.z=x;
				f[fa[x]][!opt].push_back(tmp);
				v[!opt].push_back(fa[x]);
			}
		}
	}
	f[x][opt].clear();
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(n);
	read(m);
	++n;
	for (int i=2;i<=n;++i)
	{
		read(fa[i]);
		++fa[i];
	}
	for (int i=1;i<=m;++i)
	{
		read(a[i].x);
		read(a[i].t);
		++a[i].x;
		a[i].pos=i;
	}
	t=0;
	k=1;
	opt=0;
	while (v[opt].size()||k<=m)
	{
		if (!v[opt].size())
			t=a[k].t;
		for (;t==a[k].t&&k<=m;++k)
		{
			f[a[k].x][opt].push_back(a[k]);
			v[opt].push_back(a[k].x);
		}
		for (unsigned i=0;i<v[opt].size();++i)
			work(v[opt][i]);
		v[opt].clear();
		opt=!opt;
		++t;
	}
	for (int i=1;i<=m;++i)
		write(ans[i]);
	return 0;
}