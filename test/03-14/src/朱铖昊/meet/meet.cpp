#include<bits/stdc++.h>
using namespace std;
const int N=100005;
const double eps=1e-8,inf=1e9+7;
int la[N],to[N*2],pr[N*2],fa[N][20],cnt,sd[N],n,m,x,y;
double ans;
struct Ask
{
	int x,y,type,v;
	double t1,t2;
}a[N][2];
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline int read()
{
	char c=getchar();
	int x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
void dfs(int x,int y)
{
	for (int i=1;i<=17;++i)
		fa[x][i]=fa[fa[x][i-1]][i-1];
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=y)
		{
			fa[to[i]][0]=x;
			sd[to[i]]=sd[x]+1;
			dfs(to[i],x);
		}
}
inline int lca(int x,int y)
{
	if (sd[x]<sd[y])
		swap(x,y);
	int k=sd[x]-sd[y];
	for (int i=0;i<=17;++i)
		if ((1<<i)&k)
			x=fa[x][i];
	for (int i=17;i>=0;--i)
		if (fa[x][i]!=fa[y][i])
		{
			x=fa[x][i];
			y=fa[y][i];
		}
	return x==y?x:fa[x][0];
}
inline int cross(double x1,double x2,double y1,double y2)
{
	return (x2<y1)||(y2<x1);
}
inline void work(Ask x,Ask y)
{
	if (cross(x.t1,x.t2,y.t1,y.t2))
		return;
	int p=lca(x.x,y.x),q;
	if (sd[p]<sd[x.y]||sd[p]<sd[y.y])
		return;
	if (sd[x.y]<sd[y.y])
		q=y.y;
	else
		q=x.y;
	double Tx1,Tx2,Ty1,Ty2;
	if (x.type)
	{
		Tx1=x.t1+(double)(sd[q]-sd[x.y])/(double)x.v;
		Tx2=x.t1+(double)(sd[p]-sd[x.y])/(double)x.v;
	}
	else
	{
		Tx1=x.t1-(double)(sd[p]-sd[x.x])/(double)x.v;
		Tx2=x.t1-(double)(sd[q]-sd[x.x])/(double)x.v;
	}
	if (y.type)
	{
		Ty1=y.t1+(double)(sd[q]-sd[y.y])/(double)y.v;
		Ty2=y.t1+(double)(sd[p]-sd[y.y])/(double)y.v;
	}
	else
	{
		Ty1=y.t1-(double)(sd[p]-sd[y.x])/(double)y.v;
		Ty2=y.t1-(double)(sd[q]-sd[y.x])/(double)y.v;
	}
	if (cross(Tx1,Tx2,Ty1,Ty2))
		return;
	int dist=sd[p]-sd[q],detv;
	double dets,T;
	if (x.type==y.type)
	{
		if (fabs(Tx1-Ty1)<eps)
		{
			ans=min(ans,Tx1);
			return;
		}
		if (Tx1<Ty1)
		{
			dets=(Ty1-Tx1)*x.v;
			detv=y.v-x.v;
			if (detv<=0)
				return;
			T=dets/(double)detv;
			if (T*y.v>(double)dist)
				return;
			ans=min(ans,T+Ty1);
		}
		else
		{
			dets=(Tx1-Ty1)*y.v;
			detv=x.v-y.v;
			if (detv<=0)
				return;
			T=dets/(double)detv;
			if (T*x.v>(double)dist)
				return;
			ans=min(ans,T+Tx1);
		}
	}
	else
	{
		if (Tx1<Ty1)
			ans=min(ans,((double)dist-(double)x.v*(Ty1-Tx1))/(double)(x.v+y.v)+Ty1);
		else
			ans=min(ans,((double)dist-(double)y.v*(Tx1-Ty1))/(double)(x.v+y.v)+Tx1);
	}
}
int main()
{
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		add(x,y);
		add(y,x);
	}
	sd[1]=1;
	dfs(1,0);
	ans=inf;
	for (int i=1;i<=m;++i)
	{
		a[i][0].t1=read();
		a[i][0].v=a[i][1].v=read();
		a[i][0].x=read();
		a[i][1].x=read();
		a[i][0].y=a[i][1].y=lca(a[i][0].x,a[i][1].x);
		a[i][0].t2=a[i][1].t1=a[i][0].t1+(double)(sd[a[i][0].x]-sd[a[i][0].y])/(double)a[i][0].v;
		a[i][1].t2=a[i][1].t1+(double)(sd[a[i][1].x]-sd[a[i][1].y])/(double)a[i][1].v;
		a[i][0].type=0;
		a[i][1].type=1;
	}
	random_shuffle(a+1,a+1+m);
	for (int i=2;i<=m;++i)
	{
		if (ans<a[i][0].t1)
			continue;
		for (int j=1;j<i;++j)
		{
			work(a[i][0],a[j][0]);
			work(a[i][1],a[j][1]);
			work(a[i][0],a[j][1]);
			work(a[i][1],a[j][0]);
		}
	}
	if (fabs(ans-inf)<eps)
		puts("-1");
	else
		printf("%.10lf",ans);
	return 0;
}