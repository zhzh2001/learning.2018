#include<bits/stdc++.h>
using namespace std;
const double inf=1e9+7,eps=1e-8;
const int N=100005;
double ans,cr;
int n,k,sum,m;
inline double sqr(double x)
{
	return x*x;
}
inline void read(int &x)
{
	char c=getchar();
	x=0;
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
struct Point
{
	int x,y;
	double z;
	bool operator<(const Point &a)const
	{
		return fabs(z-a.z)<eps?z>a.z:abs(x)<(a.x);
	}
}a[N];
struct Data
{
	double x,y;
}q;
struct Line
{
	double k,b,x;
	void make_line(const Point &f,const Point &g)
	{
		if (f.x==g.x)
		{
			k=inf;
			b=0;
			x=f.x;
			return;
		}
		k=((double)f.y-g.y)/((double)f.x-g.x);
		b=(double)f.y-k*f.x;
		x=-b/k;
	}
}tmp,p;
inline signed sjs()
{
	return (rand())+(rand()<<16);
}
int main()
{
	srand(time(0));
	freopen("distance.in","r",stdin);
	freopen("distance.out","w",stdout);
	read(n);
	read(k);
	for (int i=1;i<=n;++i)
	{
		read(a[i].x);
		read(a[i].y);
		if (a[i].x==0)
		{
			++sum;
			a[i].z=inf;
		}
		else
			a[i].z=fabs(((double)a[i].y)/(double)a[i].x)+23333.0/fabs(a[i].x);
	}
	if (sum>=2)
	{
		puts("0.00000000000000000000");
		return 0;
	}
	sort(a+1,a+1+n);
	ans=inf;
	m=min(n,5000);
	for (int i=2;i<=m;++i)
		for (int j=1;j<i;++j)
		{
			tmp.make_line(a[i],a[j]);
			if (tmp.x>(double)k||tmp.x<(double)-k)
				ans=min(ans,2.0*k);
			else
			{
				if (fabs(tmp.k-inf)<eps)
					ans=min(ans,2.0*fabs(tmp.x));
				else
				{
					if (fabs(tmp.k)<eps)
						ans=min(ans,2.0*sqrt(sqr(tmp.b)+sqr(k)));
					else
					{
						p.k=-1.0/tmp.k;
						p.b=-p.k*k;
						cr=(p.b-tmp.b)/(tmp.k-p.k);
						q.x=(double)k-2.0*(k-cr);
						q.y=p.b+p.k*q.x;
						ans=min(ans,sqrt(sqr(q.x+k)+sqr(q.y)));
					}
				}	
			}
			ans=ans;
		}
	if (n>m+1)
	{
		for (int i=1;i<=1000;++i)
		{
			int kkk=sjs()%(n-m)+m+1,ltt=sjs()%(n-m)+m+1;
			while (kkk==ltt)
				ltt=sjs()%(n-m)+m+1;
			tmp.make_line(a[kkk],a[ltt]);
			if (tmp.x>(double)k||tmp.x<(double)-k)
				ans=min(ans,2.0*k);
			else
			{
				if (fabs(tmp.k-inf)<eps)
					ans=min(ans,2.0*fabs(tmp.x));
				else
				{
					if (fabs(tmp.k)<eps)
						ans=min(ans,2.0*sqrt(sqr(tmp.b)+sqr(k)));
					else
					{
						p.k=-1.0/tmp.k;
						p.b=-p.k*k;
						cr=(p.b-tmp.b)/(tmp.k-p.k);
						q.x=(double)k-2.0*(k-cr);
						q.y=p.b+p.k*q.x;
						ans=min(ans,sqrt(sqr(q.x+k)+sqr(q.y)));
					}
				}	
			}
		}
	}
	printf("%.10lf",ans);
	return 0;
}