#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define gc getchar
#define pc putchar
#define RG register
#define rd read
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Down(i,a,b) for(RG int i=(a);i>=(b);--i)
#define fin(x) freopen(#x".in","r",stdin)
#define mp make_pair
#define pb push_back
#define mem(x,v) memset(x,v,sizeof(x))
#define hash ______hash
inline ll read(){
	ll T = 0;int f = 0;char c = gc();
	for(;c<'0'||c>'9';c=gc()) f|=c=='-';
	for(;c>='0'&&c<='9';c=gc()) T=T*10+c-'0';
	return f?-T:T;
}
const int maxn = 200005;
int fa[maxn],deep[maxn],x[maxn],t[maxn];
int cnt,pre[5000005],nxt[5000005],last[5000005],v[5000005];
int ans[maxn],top,s[5000005];
bool wait[maxn],flag[maxn],wtt[maxn];
int n,m,T;
vector<int> V;
vector<int> E;
int main(){
	Down(i,5000000,1) s[++top] = i;
//	fin(tree);
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
//	freopen("tree.out","w",stdout);
	n = rd(),m = rd();
	deep[0] = 0;
	Rep(i,1,n) fa[i] = rd(),deep[i]=deep[fa[i]]+1;
	mem(wait,false);
	Rep(i,1,m) x[i]=rd(),t[i]=rd();
	wait[0] = true;
	int i = 0;
	T = 0;
	V.clear();
	cnt = 0;
	while(1){
		++T;
		if(V.size()==0 || T==t[i+1]){
			++i;
			if(i==m+1){
				Rep(i,1,m){
					printf("%d ",ans[i]);
				}
				return 0;
			}
			T=t[i];
			V.pb(i);
			
			cnt=s[top--];pre[cnt]=nxt[cnt]=0;
			pre[cnt] = last[i];
			nxt[last[i]] = cnt;
			last[i] = cnt;
			v[cnt] = x[i];
			
			if(wait[x[i]])flag[i] = 1; else flag[i]=0;
			while(t[i+1]==T){
				++i;
				V.pb(i);
				
				cnt=s[top--];pre[cnt]=nxt[cnt]=0;
				pre[cnt] = last[i];
				nxt[last[i]] = cnt;
				last[i] = cnt;
				v[cnt] = x[i];
				
				if(wait[x[i]])flag[i] = 1; else flag[i]=0;
			}
		}
		vector<int>::iterator j;//puts("");
		for(j=V.begin();j!=V.end();j++){
			int w = *j;
			if(flag[w]==0)continue;
			if(pre[last[w]] == 0){//回到当前点 
				ans[w] = T;
				auto _j = j;_j--;
				V.erase(j);
				j=_j;
				continue;
			}
			wait[v[last[w]]] = false;
			s[++top] = last[w];
			last[w] = pre[last[w]];
		}
		for(j=V.begin();j!=V.end();j++){
			int w = *j;
			if(flag[w]==1)continue;
			if(wait[v[last[w]]]){
				flag[w] = 1;
				s[++top] = last[w];
				last[w] = pre[last[w]];
			} else{
				wait[v[last[w]]] = true;
				int x = fa[v[last[w]]];
				cnt=s[top--];pre[cnt]=nxt[cnt]=0;
				pre[cnt] = last[w];
				nxt[last[w]] = cnt;
				last[w] = cnt;
				v[cnt] = x;
			}
		}

	}
	return 0;
}
