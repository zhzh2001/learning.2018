#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define gc getchar
#define pc putchar
#define RG register
#define rd read
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Down(i,a,b) for(RG int i=(a);i>=(b);--i)
#define fin(x) freopen(#x".in","r",stdin)
inline ll read(){
	ll T = 0;int f = 0;char c = gc();
	for(;c<'0'||c>'9';c=gc()) f|=c=='-';
	for(;c>='0'&&c<='9';c=gc()) T=T*10+c-'0';
	return f?-T:T;
}
const double eps = 1e-7;
const int maxn = 1e5+233;
const double pi = acos(-1.0);
struct Vector{
	int x,y;
	double ang;
	bool operator < (const Vector &w) const{
		return ang < w.ang;
	}
}P,Q,p[maxn*2];
int n,a;
double ans;
int Cross(Vector a,Vector b,Vector c){
	return (a.x-c.x)*(b.y-c.y)-(a.y-c.y)*(b.x-c.x);
}
double dist(Vector a,Vector b){
	return sqrt(sqr(a.x-b.x)+sqr(a.y-b.y));
}
void solve(Vector a,Vector b){
	if(a.x==b.x&&a.y==b.y)return;
	int x = Cross(P,a,b),y = Cross(Q,a,b);
	if(x<0&&y>0 || x>0&&y<0) return;//无法更新？？？
	double z = dist(a,b);
	double xx = x / z,yy = y / z;
	ans = min(ans,2*P.x/abs(xx-yy)*min(xx,yy));
}
bool check(double r){
	printf("check(%lf)\n",r);
	Vector tmp;
	tmp.x=tmp.y=0;
	r/=2;
	double ang1 = acos(r/a);
	double ang2 = pi - ang1;
	tmp.ang = ang1;
	int x = lower_bound(p+1,p+1+n,tmp) - p;
	if(p[x].ang > ang2) return false;
	tmp.ang = ang1-pi;
	x = lower_bound(p+1,p+1+n,tmp) - p;
	if(p[x].ang > ang2-pi) return false;
	return true;
}
int main(){
	fin(distance);
	n = rd(),a = rd();
	P.x=a;P.y=0;P.ang=atan2(P.y,P.x);
	Q.x=a;Q.y=0;Q.ang=atan2(Q.y,Q.x);
	int flag = 0;
	Rep(i,1,n){
		p[i].x = rd(),p[i].y = rd();
		p[i].ang = atan2(p[i].y,p[i].x);
		p[i+n] = p[i];p[i+n].ang += 2*pi;
		if(p[i].y==0)flag++;
	}
	if(flag>=2){
		puts("0.00000000000");
		return 0;
	}
	sort(p+1,p+1+n*2);
	ans = 2*a  ;
	P.ang += eps;
	Q.ang += eps;
	for(int i=1;i<=n;i++){
		if(p[i].y==0) continue; 
		if(p[i].ang < 0){
			int x = upper_bound(p+1,p+1+2*n,P) - p - 1;
			if(x<=2*n)solve(p[i],p[x]);
		} else{
			int x = upper_bound(p+1,p+1+2*n,Q) - p - 1;
			if(x<=2*n)solve(p[i],p[x]);
		}
	}
	double L = 0,R = ans;
	while(R-L > eps){
		double mid = (L+R) / 2;
		if(check(mid)){
			R = mid;
			ans = mid;
		} else{
			L = mid;
		}
	}
	printf("%.7lf\n",L);
	return 0;
}
/*
问题转化为找出一条直线，使得Q关于直线对称后距离P最近。 
*/
