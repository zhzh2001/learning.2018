#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define gc getchar
#define pc putchar
#define RG register
#define rd read
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Down(i,a,b) for(RG int i=(a);i>=(b);--i)
#define fin(x) freopen(#x".in","r",stdin)
inline ll read(){
	ll T = 0;int f = 0;char c = gc();
	for(;c<'0'||c>'9';c=gc()) f|=c=='-';
	for(;c>='0'&&c<='9';c=gc()) T=T*10+c-'0';
	return f?-T:T;
}
const double eps = 1e-7;
const int maxn = 1e5+233;
const double pi = acos(-1.0);
struct Vector{
	int x,y;
//	double ang;
//	bool operator < (const Vector &w) const{
//		return ang < w.ang;
//	}
}P,Q,p[maxn*2];
int n,a;
double ans;
int Cross(Vector a,Vector b,Vector c){
	return (a.x-c.x)*(b.y-c.y)-(a.y-c.y)*(b.x-c.x);
}
int Dot(Vector a,Vector b,Vector c){
	return (a.x-c.x)*(b.x-c.x)+(a.y-c.y)*(b.y-c.y);
}
double dist(Vector a,Vector b){
	return sqrt(sqr(a.x-b.x)+sqr(a.y-b.y));
}
void solve(Vector a,Vector b){
	if(a.x==b.x&&a.y==b.y)return;
	int x = Cross(P,a,b),y = Cross(Q,a,b);double z = fabs(dist(a,b)); 
	if(a.y==b.y){
		ans = 0;
		return;
	}
	if(x<0&&y>0 || x>0&&y<0){
		if(a.x==b.x){
			ans = max(P.x-a.x,a.x-Q.x) - min(P.x-a.x,a.x-Q.x);
			return ;
		}
		double len = fabs(Dot(P,b,a)/z);
		double _x,_y,__x,__y;
		_x = a.x + (b.x-a.x)/z*len;
		_y = a.y + (b.y-a.y)/z*len;
		__x = _x*2-P.x;
		__y = _y*2-P.y;
		ans=min(ans,sqrt(sqr(__x-Q.x)+sqr(__y-Q.y)));
	} else
	if(x==0||y==0){
		ans = 0;
	}else
	{
		double xx = fabs(x / z),yy = fabs(y / z);
		ans= min(ans,2*P.x*min(xx,yy)/abs(xx-yy));
	}
}
int main(){
	freopen("distance.in","r",stdin);
	//fin(distance);
	freopen("distance.out","w",stdout);
	n = rd(),a = rd();
	if(n>=30000) puts("nan");
	P.x=a;P.y=0;//P.ang=atan2(P.y,P.x);
	Q.x=-a;Q.y=0;//P.ang=atan2(Q.y,Q.x);
	ans = 2*a;
	Rep(i,1,n)
		p[i].x = rd(),p[i].y = rd();
	Rep(i,1,n)Rep(j,i+1,n)solve(p[i],p[j]);
	printf("%.10lf\n",ans);
	return 0;
}
/*
问题转化为找出一条直线，使得Q关于直线对称后距离P最近。 
*/
