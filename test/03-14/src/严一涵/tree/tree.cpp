#include <iostream>
#include <algorithm>
#include <cstdio>
#include <queue>
using namespace std;
#define dd c=getchar()
inline int read(){int a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
struct st{
	int x,t;bool v,o;
	st(){
		x=t=v=0;
	}
	st(int a,int b,bool c){
		x=a;t=b;v=c;o=0;
	}
}r,q;
bool operator<(st a,st b){
	if(a.t!=b.t)return a.t<b.t;
	if(a.v!=b.v)return a.v;
	return a.x<b.x;
}
bool operator>(st a,st b){
	return b<a;
}
priority_queue<st,vector<st>,greater<st> >p;
int n,m,f[200003],v[200003],x,y,z,ta[200003],ans[200003];
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++)f[i]=read();
	for(int i=1;i<=m;i++){
		r.x=read();
		r.t=read();
		r.v=0;
		r.o=1;
		ta[r.x]=i;
		p.push(r);
	}
	while(!p.empty()){
		q=p.top();
		p.pop();
		if(q.v){
			if(v[q.x]==0){
				ans[ta[q.x]]=q.t;
			}else{
				p.push(st(v[q.x],q.t+1,1));
				v[q.x]=0;
			}
		}else{
			x=f[q.x];
			if(q.o&&v[q.x]){
				ans[ta[q.x]]=q.t;
			}else if(x==0||v[x]>0){
				p.push(st(q.x,q.t+2,1));
			}else{
				v[x]=q.x;
				p.push(st(x,q.t+1,0));
			}
		}
	}
	for(int i=1;i<=m;i++)printf("%d ",ans[i]);
	return 0;
}
