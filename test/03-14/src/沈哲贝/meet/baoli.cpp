#include<bits/stdc++.h>
using namespace std;
#define ll int
#define llu unsigned long long
#define ld double
#define llu unsigned long long
#define rep(i,x,y)  for(ll i=x;i<y;++i)
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define pi acos(-1)
#define mk make_pair<ll,ll>
#define pa pair<ll,ll>
#define lf else if
#define max(x,y)    ((x)<(y)?(y):(x))
#define min(x,y)    ((x)<(y)?(x):(y))
#define sqr(x)      ((x)*(x))
#define abs(x)      ((x)>0?(x):-(x))
#define Mul(x,y)    ((x)=1LL*(x)*(y)%mod)
#define Add(x,y)    ((x)=((x)+(y))%mod)
#define Max(x,y)    ((x)=((x)<(y)?(y):(x)))
#define Min(x,y)    ((x)=((x)>(y)?(y):(x)))
#define E(x)        return writeln(x),0
#define p(x)        printf("~%d~\n",x)
#define pp(x,y)     printf("~~%d %d~~\n",x,y)
#define ppp(x,y,z)  printf("~~~%d %d %d~~~\n",x,y,z)
#define pppp(a,b,c,d)	printf("~~~%d %d %d %d\n",a,b,c,d)
#define f_in(x)     freopen(x".in","r",stdin)
#define f_out(x)    freopen(x".out","w",stdout)
#define open(x)     f_in(x),f_out(x)
#define fi first
#define se second
typedef complex<double> E;
namespace SHENZHEBEI{
#define NEG 1
    const int L=2333333;
    inline char gc(){	return getchar();	}
#if NEG
    inline ll read(){    ll x=0,f=1; char ch=gc();   for (;!isdigit(ch);ch=gc()) if (ch=='-') f=-1;  for (;isdigit(ch);ch=gc())  x=x*10-48+ch;   return x*f; }
    inline void write(ll x){    if (x<0) putchar('-'),x=-x;  if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#else
    inline ll read(){    ll x=0; char ch=gc();   for (;!isdigit(ch);ch=gc());    for (;isdigit(ch);ch=gc())  x=x*10-48+ch;   return x;   }
    inline void write(ll x){    if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#endif
    inline char readchar(){ char ch=gc();   for(;isspace(ch);ch=gc());  return ch;  }
    inline ll readstr(char *s){ char ch=gc();   int cur=0;  for(;isspace(ch);ch=gc());      for(;!isspace(ch);ch=gc())  s[cur++]=ch;    s[cur]='\0';    return cur; }
    inline void writeln(ll x){  write(x);   puts("");   }
}using namespace SHENZHEBEI;
const ll N=200010;
const ld eps=1e-8;
ll son[N],siz[N],u[N],v[N],head[N],nxt[N],vet[N],fa[N],top[N],dep[N],n,tot,m,dt,clc,LCA[5010][5010];
ld ans=1e9,Tim[N],Val[N],First_Down,First_Up,Second_Down,Second_Up;
void insert(ll x,ll y){nxt[++tot]=head[x];head[x]=tot;vet[tot]=y;}
void dfs(ll x){
	siz[x]=1;
	for(ll i=head[x];i;i=nxt[i])if (vet[i]^fa[x]){
		fa[vet[i]]=x;
		dep[vet[i]]=dep[x]+1;
		dfs(vet[i]);
		if (siz[vet[i]]>siz[son[x]])son[x]=vet[i];
	}
}
void dfs1(ll x){
	top[x]=son[fa[x]]==x?top[fa[x]]:x;
	for(ll i=head[x];i;i=nxt[i])if (vet[i]^fa[x])dfs1(vet[i]);
}
ll lca(ll x,ll y){
	for(;top[x]^top[y];dep[top[x]]<dep[top[y]]?y=fa[top[y]]:x=fa[top[x]]);
	return dep[x]<dep[y]?x:y;
}
int main(){
	freopen("meet.in","r",stdin);
	n=read(),m=read();
	For(i,2,n){
		ll x=read(),y=read();
		insert(x,y),insert(y,x);
	}dfs(1);dfs1(1);
	For(i,1,m){
		ll Time=read(),val=read(),x=read(),y=read(),Lca=lca(x,y);
		Tim[++dt]=Time,Val[dt]=1.0/val,u[dt]=x,v[dt]=Lca;
		Tim[++dt]=Time+1.0/val*(dep[x]+dep[y]-2*dep[Lca]),Val[dt]=-1.0/val,u[dt]=y,v[dt]=Lca;
	}
	if (n<=5000){For(i,1,n)For(j,i,n)LCA[i][j]=LCA[j][i]=lca(i,j);}
	For(i,1,n){
		For(j,1,n)printf("%d ",LCA[i][j]);
		puts("");
	}
	For(i,1,dt)For(j,((i&1)?(i+2):(i+1)),dt){
		ll Down=n<=5000?LCA[u[i]][u[j]]:lca(u[i],u[j]),Up=dep[v[i]]<dep[v[j]]?v[j]:v[i],
		dist=dep[Down]-dep[Up];
		if (dep[Down]<dep[Up])continue;
		First_Down=Tim[i]+Val[i]*(dep[u[i]]-dep[Down]),
		Second_Down=Tim[j]+Val[j]*(dep[u[j]]-dep[Down]);
		if (Val[i]==Val[j]){
			First_Up=First_Down+Val[i]*dist,Second_Up=Second_Down+Val[j]*dist;
			if (abs(First_Down-Second_Down)<=eps)Min(ans,First_Down);
			if (abs(First_Up-Second_Up)<=eps)Min(ans,First_Up);
		}else{
			ld Add=Val[j]-Val[i],ned=(First_Down-Second_Down)/Add;
			if (-eps<=ned&&ned<=eps+dep[Down]-dep[Up])Min(ans,First_Down+ned*Val[i]);
		}
	}if (ans>5e8)puts("-1");else printf("%lf\n",ans);
}
/*
���������2�� 
*/
