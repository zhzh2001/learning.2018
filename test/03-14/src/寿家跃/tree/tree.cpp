#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>
#include<map>
#include<set>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;
typedef pair<int,int> PII;

const int N=200005;

int n,m,fa[N],Dep[N],Bol[N],Gra[N];

struct node{
	int x,t,pat;
}P[N];
inline bool Cmp(node A,node B){
	return A.t<B.t;
}
int Ans[N];

set<PII>Ups,Dws;
vector<int>V[N];

int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),m=read();
	Rep(i,1,n) fa[i]=read(),Dep[i]=Dep[fa[i]]+1;
	Bol[0]=true;
	Rep(i,1,m) P[i].x=read(),P[i].t=read(),P[i].pat=i;
	sort(P+1,P+m+1,Cmp);
	for (int i=1,pos=1;;i++){
//		printf("i=%d\n",i);
		if (Ups.size()==0){
			if (pos>m) break;
			if (P[pos].t>i){
				i=P[pos].t-1;continue;
			}
		}
		for (set<PII>::iterator it=Ups.begin();it!=Ups.end();it++){
			int pos=it->second;
			Gra[pos]=fa[Gra[pos]];
		}
		while (pos<=m && P[pos].t<=i){
			Ups.insert(make_pair(P[pos].x,pos));
			Gra[pos]=P[pos].x;pos++;
		}
		vector<set<PII>::iterator>Ew;
		for (set<PII>::iterator it=Dws.begin();it!=Dws.end();it++){
			int pos=it->second;
			if (V[pos].size()==0){
				Ew.push_back(it);
			}
			else{
				Bol[V[pos][V[pos].size()-1]]=false;
				V[pos].pop_back();
			}
		}
		Rep(i,0,Ew.size()-1) Dws.erase(Ew[i]);
		
		vector<set<PII>::iterator>Ep;
		for (set<PII>::iterator it=Ups.begin();it!=Ups.end();it++){
			int pos=it->second;
			if (Bol[Gra[pos]]){
				Ans[P[pos].pat]=P[pos].t+2*(Dep[P[pos].x]-Dep[Gra[pos]]);
				Ep.push_back(it);
				Dws.insert(*it);
			}
			else{
				Bol[Gra[pos]]=true;
				V[pos].push_back(Gra[pos]);
			}
		}
		Rep(i,0,Ep.size()-1) Ups.erase(Ep[i]);
	}
	Rep(i,1,m) printf("%d ",Ans[i]);puts("");
}
/*
6 3
0 1 2 3 2 5
4 6
6 9
5 11

8 3
0 1 1 2 3 3 4 5
6 1
8 2
4 5
*/
