#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=100005;

int n,m;
int lines,front[N];
struct Edge{
	int next,to;
}E[N*2];
inline void Addline(int u,int v){
	E[++lines]=(Edge){front[u],v};front[u]=lines;
}
int size[N],deep[N],fa[N],heavy[N],L[N],R[N],cnt;

void Dfs(int u,int d,int f){
	L[u]=++cnt;
	size[u]=1;
	fa[u]=f;
	deep[u]=d;
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=f){
			Dfs(v,d+1,u);
			size[u]+=size[v];
			if (size[v]>size[heavy[u]]) heavy[u]=v;
		}
	}
	R[u]=cnt;
}
int top[N],dep[N];
void Par(int u,int d,int t){
	top[u]=t;
	dep[u]=d;
	int son=heavy[u];
	if (son) Par(son,d,t);
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=fa[u] && v!=son){
			Par(v,d+1,v);
		}
	}
}
inline int Lca(int x,int y){
	while (top[x]!=top[y]){
		if (dep[x]<dep[y]) swap(x,y);
		x=fa[top[x]];
	}
	return deep[x]<deep[y]?x:y;
}
inline int Dis(int x,int y){
	return deep[x]+deep[y]-2*deep[Lca(x,y)];
}
inline int Subside(int x,int y){
	if (L[x]<=L[y] && L[y]<=R[x]) return true;
		else return false;
}
inline int Cross(int p,int x,int y){
	if (deep[x]>deep[y]) swap(x,y);
	if (Subside(x,p) && Subside(p,y)) return true;
	return false;
}
int t[N],c[N],u[N],v[N];

inline pair<int,int> Link(int a,int b){
	pair<int,int> res=make_pair(-1,-1);
	int lca=Lca(u[a],v[a]);
	int lcb=Lca(u[b],v[b]);
	if (lca==lcb){
		res.first=res.second=lca;
		int val;
		if ((val=Lca(u[a],u[b]))!=lca) res.first=val;else
		if ((val=Lca(u[a],v[b]))!=lca) res.first=val;
		if ((val=Lca(v[a],u[b]))!=lca) res.second=val;else
		if ((val=Lca(v[a],v[b]))!=lca) res.second=val;
		return res;
	}
	if (Cross(lca,lcb,u[b])) return make_pair(Lca(u[a],u[b]),Lca(v[a],u[b]));
	if (Cross(lca,lcb,v[b])) return make_pair(Lca(u[a],v[b]),Lca(v[a],v[b]));
	if (Cross(lcb,lca,u[a])) return make_pair(Lca(u[b],u[a]),Lca(v[b],u[a]));
	if (Cross(lcb,lca,v[a])) return make_pair(Lca(u[b],v[a]),Lca(v[b],v[a]));
	return res;
}
int main(){
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	n=read(),m=read();
	if (m>5000){
		puts("-1");return 0;
	}
	Rep(i,1,n-1){
		int u=read(),v=read();
		Addline(u,v),Addline(v,u);
	}
	Dfs(1,0,0);
	Par(1,0,1);
	Rep(i,1,m){
		t[i]=read(),c[i]=read(),u[i]=read(),v[i]=read();
	}
	double Ans=1e18;
	Rep(i,1,m) Rep(j,i+1,m){
		pair<int,int> res=Link(i,j);
		if (res.first==-1) continue;
//		printf("u=%d v=%d\n",res.first,res.second);
		double si=0,ei=0,opti=0,sj=0,ej=0,optj=0;
		si=t[i]+1.0*Dis(u[i],res.first)/c[i];
		ei=t[i]+1.0*Dis(u[i],res.second)/c[i];
		sj=t[j]+1.0*Dis(u[j],res.first)/c[j];
		ej=t[j]+1.0*Dis(u[j],res.second)/c[i];
		opti=si<ei?0:1;
		optj=sj<ej?0:1;
		if (opti!=optj){
			if (si>ei) swap(si,ei);
			if (sj>ej) swap(sj,ej);
//			printf("i=%d j=%d si=%lf sj=%lf ei=%lf ej=%lf\n",i,j,si,sj,ei,ej);
			if (max(si,sj)-(1e-9)<min(ei,ej)){
				double D=Dis(res.first,res.second);
				if (si<sj) D-=(sj-si)*c[i];
				if (si>sj) D-=(si-sj)*c[j];
				if (D<0) continue;
				Ans=min(Ans,max(si,sj)+D/(c[i]+c[j]));
			}
		}
		else{
			if (si>ei) swap(si,ei);
			if (sj>ej) swap(sj,ej);
			if (si<sj){
				if (c[i]>=c[j]) continue;
				double t=sj+(sj-si)*c[i]/(c[j]-c[i]);
				if (t-(1e-9)>min(ei,ej)) continue;
				Ans=min(Ans,t);
			}
			if (sj<si){
				if (c[j]>=c[i]) continue;
				double t=si+(si-sj)*c[j]/(c[i]-c[j]);
				if (t-(1e-9)>min(ei,ej)) continue;
				Ans=min(Ans,t);
			}
		}
	}
	if (Ans>1e17){
		puts("-1");return 0;
	}
	printf("%lf\n",Ans);
}
/*
6 4
2 5
6 5
3 6
4 6
4 1
27 6 1 3
9 5 1 5
27 4 3 4
11 29 2 6

6 4
3 1
4 5
6 4
6 1
2 6
16 4 4 5
13 20 6 2
3 16 4 5
28 5 3 5
*/
