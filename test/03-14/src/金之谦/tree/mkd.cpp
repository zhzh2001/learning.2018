#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int main()
{
	srand(time(0));
	freopen("tree.in","w",stdout);
	int n=rand()%300,m=rand()%100;
	cout<<n<<' '<<m<<endl;
	for(int i=1;i<=n;i++){
		int x=rand()%i;write(x);putchar(' ');
	}puts("");
	int t=0;
	for(int i=1;i<=m;i++){
		int x=rand()%n+1;write(x);putchar(' ');
		t=t+rand()%30;writeln(t);
	}
	return 0;
}
