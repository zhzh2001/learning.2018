#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
struct ppap{int x,t;}a[200010];
struct qqaq{int p,x,op,fa;}Q[200010];
int fa[200010],b[200010],ans[200010],n,q;
int main()
{
	freopen("tree.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();q=read();
	for(int i=1;i<=n;i++)fa[i]=read();
	for(int i=1;i<=q;i++)a[i].x=read(),a[i].t=read();
	int T=1,l=1;
	for(;;T++){
		bool flag=0;
		for(int i=1;i<=n;i++)if(Q[i].op==2){
			flag=1;
			b[Q[i].x]=0;
			if(Q[i].x==i){
				ans[Q[i].p]=T;Q[i].op=0;
			}else{
				Q[i].x=Q[i].fa;Q[i].fa=b[Q[i].fa];
			}
		}
		for(int i=1;i<=n;i++)if(Q[i].op==1){
			flag=1;
			if(!b[Q[i].x]&&Q[i].x){
				b[Q[i].x]=Q[i].fa;
				Q[i].fa=Q[i].x;
				Q[i].x=fa[Q[i].x];
			}else{
				Q[i].op=2;
				Q[i].x=Q[i].fa;Q[i].fa=b[Q[i].fa];
			}
		}
		if(!flag){
			if(l==q+1)break;
			T=a[l].t;
		}
		for(;a[l].t==T;l++){
			int x=a[l].x;
			if(b[x]){ans[l]=T;continue;}
			b[x]=1e9;
			Q[x]=(qqaq){l,fa[x],1,x};
		}
	}
	for(int i=1;i<=q;i++)write(ans[i]),putchar(' ');
	return 0;
}
/*
8 3
0 1 1 2 3 3 4 5
6 1
8 2
4 5
*/
