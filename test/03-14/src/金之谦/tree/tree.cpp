#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
struct ppap{int x,t;}a[200010];
struct qqaq{int p,s,x,fa;}Q1[200010],Q2[200010];
inline bool cmp(qqaq a,qqaq b){return a.s<b.s;}
int fa[200010],b[200010],ans[200010],n,q;
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();q=read();
	for(int i=1;i<=n;i++)fa[i]=read();
	for(int i=1;i<=q;i++)a[i].x=read(),a[i].t=read();
	int T=1,l=1,top1=0,top2=0;
	for(;;T++){
		int sh1=0,sh2=0;bool flag=0;
		for(int i=1;i<=top2;i++){
			flag=1;b[Q2[i].x]=0;
			if(Q2[i].x==Q2[i].s){
				ans[Q2[i].p]=T;Q2[i].s=1e9;sh2++;
			}else{
				Q2[i].x=Q2[i].fa;Q2[i].fa=b[Q2[i].fa];
			}
		}
		for(int i=1;i<=top1;i++){
			flag=1;
			if(!b[Q1[i].x]&&Q1[i].x){
				b[Q1[i].x]=Q1[i].fa;
				Q1[i].fa=Q1[i].x;
				Q1[i].x=fa[Q1[i].x];
			}else{
				Q1[i].x=Q1[i].fa;Q1[i].fa=b[Q1[i].fa];
				Q2[++top2]=Q1[i];Q1[i].s=1e9;sh1++;
			}
		}
		sort(Q2+1,Q2+top2+1,cmp);top2-=sh2;
		if(!flag){
			if(l==q+1)break;
			T=a[l].t;
		}
		for(;a[l].t==T;l++){
			int x=a[l].x;
			if(b[x]){ans[l]=T;continue;}
			b[x]=1e9;
			Q1[++top1]=(qqaq){l,x,fa[x],x};
		}
		sort(Q1+1,Q1+top1+1,cmp);top1-=sh1;
	}
	for(int i=1;i<=q;i++)write(ans[i]),putchar(' ');
	return 0;
}
