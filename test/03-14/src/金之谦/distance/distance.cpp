#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
typedef double D;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,A;D p,q;
struct ppap{int x,y;}a[100010];
inline D sqr(D x){return x*x;}
inline D cross(int x,int y){return (D)(a[x].y-a[y].y)/(a[x].x-a[y].x);}
inline D cal(D x,D y){
	return fabs(sqrt(sqr(x-A)+sqr(y))-sqrt(sqr(x+A)+sqr(y)));
}
inline D sanfen(D l,D r){
	while(r-l>1e-7){
		D m1=l+(r-l)/3,m2=m1+(r-l)/3;
		if(cal(m1,p*m1+q)>cal(m2,p*m2+q))r=m2;
		else l=m1;
	}
	return cal(l,p*l+q);
}
int main()
{
	freopen("distance.in","r",stdin);
	freopen("distance.out","w",stdout);
	n=read();A=read();
	for(int i=1;i<=n;i++)a[i].x=read(),a[i].y=read();
	D ans=1e15;
	for(int i=1;i<=n;i++){
		if(cal(a[i].x,a[i].y)>ans)continue;
		for(int j=i+1;j<=n;j++){
			if(cal(a[j].x,a[j].y)>ans)continue;
			if(a[i].x==a[j].x){
				D l=0,r=1e5;
				while(r-l>1e-7){
					D m1=l+(r-l)/3,m2=m1+(r-l)/3;
					if(cal(a[i].x,m1)>cal(a[i].x,m2))r=m2;
					else l=m1;
				}
				D rp=cal(a[i].x,l);ans=min(ans,rp);
				continue;
			}
			p=cross(i,j);q=a[i].y-a[i].x*p;
			D rp=max(sanfen(-1e5,0),sanfen(0,1e5));
			ans=min(ans,rp);
		}
	}
	printf("%.7lf",ans);
	return 0;
}
