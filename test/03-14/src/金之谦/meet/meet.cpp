#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
typedef double D;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5;
int nedge=0,p[2*N],nex[2*N],head[2*N],n,m,cnt=0;
int deep[N],fa[N],s[N],son[N],top[N],sx[N];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(int x,int Fa){
	deep[x]=deep[Fa]+1;fa[x]=Fa;
	for(int k=head[x];k;k=nex[k])if(p[k]!=Fa){
		dfs(p[k],x);s[x]+=s[p[k]];
		if(s[son[x]]<s[p[k]])son[x]=p[k];
	}
}
inline void dfss(int x,int t){
	top[x]=t;sx[x]=++cnt;
	if(son[x])dfss(son[x],t);
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa[x]&&p[k]!=son[x])dfss(p[k],p[k]);
}
struct ppap{
	int t,v,x,y;
	inline void Read(){
		t=read();v=read();x=read();y=read();
	}
}a[100010];
int main()
{
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	n=read();m=read();
	if(m>1e4)return puts("-1")&0;
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addedge(x,y);addedge(y,x);
	}
	dfs(1,0);dfss(1,1);
	for(int i=1;i<=m;i++){
		a[i].Read();
		for(int j=1;j<i;j++){
			
		}
	}
	puts("-1");
	return 0;
}
