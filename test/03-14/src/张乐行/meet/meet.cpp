#include<cstdio>
#include<algorithm>
using namespace std;
const int N=1e5+5,M=2*N;
const double inf=1e9;
int n,m,i,j;double ans;
int et,he[N],fa[N],d[N];
struct edge{int l,to;}e[M];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void dfs(int x){
	d[x]=d[fa[x]]+1;
	for (int i=he[x];i;i=e[i].l){
		int y=e[i].to;if (y==fa[x]) continue;
		fa[y]=x;dfs(y);
	}
}
int lca(int x,int y){
	if (d[x]<d[y]) swap(x,y);
	for (;d[x]>d[y];x=fa[x]);
	for (;x!=y;x=fa[x],y=fa[y]);
	return x;
}
int na;
struct arr{int x,v;double l,r;}a[2*N];
bool chk(double x,double l,double r){
	if (l>r) swap(l,r);return l<=x&&x<=r;
}
void solve(arr A,arr B){
	double t=(d[A.x]-d[B.x]+A.l*A.v-B.l*B.v)/(A.v-B.v);
	if (chk(t,A.l,A.r)&&chk(t,B.l,B.r)) ans=min(ans,t);
}
int main(){
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	n=read();m=read();
	if (n>5000){puts("-1");return 0;}
	for (i=1;i<n;i++){
		int x=read(),y=read();
		e[++et].l=he[x];he[x]=et;e[et].to=y;
		e[++et].l=he[y];he[y]=et;e[et].to=x;
	}
	dfs(1);
	for (i=1;i<=m;i++){
		int t=read(),v=read(),x=read(),y=read();
		int p=lca(x,y);
		double tp=t+(double)(d[x]-d[p])/v;
		double td=tp+(double)(d[y]-d[p])/v;
		a[++na]=(arr){x,v,t,tp};
		a[++na]=(arr){y,-v,td,tp};
	}
	ans=inf;
	for (i=1;i<=na;i++) for (j=1;j<i;j++)
		if ((i&1)||j!=i-1) solve(a[i],a[j]);
	if (ans<inf) printf("%.10lf",ans);
	else puts("-1");
}
