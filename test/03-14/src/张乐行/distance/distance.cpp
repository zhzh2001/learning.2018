#include<cstdio>
#include<cmath>
#define ll long long
const int N=1e5+5;
int n,ra,i,j;double ans;
struct Pt{int x,y;}a[N];
int read(){
	char c=getchar();int k=0,p=0;
	for (;c<48||c>57;c=getchar()) if (c=='-') p=1;
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;
	return p?-k:k;
}
void solve(Pt A,Pt B){
	int a1=B.y-A.y,b1=A.x-B.x;
	int c1=(A.y-B.y)*(ra-2*A.x)+2*(A.x-B.x)*A.y;
	int a2=A.x-B.x,b2=A.y-B.y,c2=(A.x-B.x)*ra;
	double x=(double)((ll)c1*b2-(ll)c2*b1)/(a1*b2-a2*b1);
	double y=b1?(c1-a1*x)/b1:(c2-a2*x)/b2;
	double dis=sqrt((x+ra)*(x+ra)+y*y);
	if (dis<ans) ans=dis;
}
int main(){
	freopen("distance.in","r",stdin);
	freopen("distance.out","w",stdout);
	n=read();ra=read();ans=2*ra;
	for (i=1;i<=n;i++) a[i]=(Pt){read(),read()};
	for (i=2;i<=n;i++) for (j=1;j<i;j++)
		solve(a[i],a[j]);
	printf("%.10lf",ans);
}
