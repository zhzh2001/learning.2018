#include<cstdio>
#include<algorithm>
using namespace std;
const int N=2e5+5;
int n,m,i,j,fa[N],d[N],top[N],ans[N];
struct arr{int x,t,id;}a[N];
bool operator < (arr A,arr B){
	int vx=A.t+d[A.x],vy=B.t+d[B.x];
	return vx<vy||vx==vy&&A.x<B.x;
}
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int lca(int x,int y){
	for (;x!=y;x=fa[x]) if (x<y) swap(x,y);
	return x;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();m=read();
	for (i=1;i<=n;i++) fa[i]=read();
	for (d[1]=1,i=2;i<=n;i++)
		d[i]=d[fa[i]]+1;
	for (i=1;i<=m;i++)
		a[i]=(arr){read(),read(),i};
	sort(a+1,a+m+1);
	for (i=1;i<=m;i++){
		int x=a[i].x,p=0;
		for (j=1;j<i;j++){
			int y=a[j].x,px=lca(x,y);
			if (d[px]<=d[p]) continue;
			int r=a[j].t+d[y]+d[px]-2*d[top[j]];
			int k=a[i].t+d[x]-d[px];
			if (k<r) p=px;
		}
		top[i]=p;
		ans[a[i].id]=a[i].t+(d[x]-d[p])*2;
	}
	for (i=1;i<=m;i++) write(ans[i]),putchar(' ');
}
