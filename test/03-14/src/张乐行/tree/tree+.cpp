#include<cstdio>
#include<algorithm>
#define ls p<<1,l,m
#define rs p<<1|1,m+1,r
using namespace std;
const int N=2e5+5,P=4*N,LG=20,inf=1e8;
int n,m,i,j,x,y,top[N],ans[N];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
struct arr{int x,t,id;}a[N];
bool operator < (arr A,arr B){
	int vx=A.t+d[A.x],vy=B.t+d[B.x];
	return vx<vy||vx==vy&&A.x<B.x;
}
int lt[N],he[N],fa[N],d[N];
int cnt,dfn[N];

int pos[N],mx[P];
void change(int x,int k){
	int p=pos[x];
	for (;p;p>>=1) mx[p]=max(mx[p],k);
}

void build(int p,int l,int r){
	mx[p]=-inf;
	if (l==r){pos[l]=p;return;}
	int m=l+r>>1;build(ls);build(rs);
}

int get(int p){
	x=dfn[p];y=x+size[p]-1;
}
void dfs(int x,int dep){
	d[x]=dep;dfn[x]=++cnt;
	for (int i=he[x],j;i;i=e[i].l){
		int y=e[i].to;if (y==fa[x]) continue;
		for (ft[y][0]=x,j=1;j<=lg[dep];j++)
			ft[y][j]=ft[ft[y][j-1]][j-1];
		dfs(y,dep+1);
	}
}
int solve(int x,int sx){
	int k=sx+d[x];
	if (get(x)+2*d[x]>k) return x;
	for (int i=lg[d[x]];~i;i--){
		int y=ft[x][i];if (!y) continue;
		if (get(y)+2*d[y]<=k) x=y;
	}
	return fa[x];
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();m=read();
	for (i=1;i<=n;i++){
		int x=read();fa[i]=x;
		lt[i]=he[x];he[x]=i;
	}
	for (lg[0]=-1,i=1;i<=n;i++) lg[i]=lg[i>>1]+1;
	dfs(1,1);build(1,1,n);
	for (i=1;i<=m;i++)
		a[i]=(arr){read(),read(),i};
	sort(a+1,a+m+1);
	for (i=1;i<=m;i++){
		top[i]=solve(a[i].x,a[i].t);
		change(dfn[a[i].x],a[i].t+d[a[i].x]-2*d[top[i]]);
		ans[a[i].id]=a[i].t+2*(d[a[i].x]-d[top[i]]);
	}
	for (i=1;i<=m;i++) write(ans[i]),putchar(' ');
}
