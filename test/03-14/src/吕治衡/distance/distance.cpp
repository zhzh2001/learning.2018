#include<bits/stdc++.h>
#define ll long long
#define ld long double 
#define eps 1e-10
#define sqr(x) ((x)*(x))
#define doit(x,y) fabs(sqrt(sqr(x-d)+sqr(y))-sqrt(sqr(x+d)+sqr(y)))
inline ll read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	ll k=0,kk=1;if (c=='-')c=getchar(),kk=-1;
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return kk*k;
}using namespace std;
void write(ll x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
struct lsg{int x,y;ld z;}a[1000010];
int n,d;
ld ans,xxx,yyy,sum1,sum2;
bool pd(lsg x,lsg y){return x.z<y.z;}
inline ld calc(int x,int y,int xx,int yy){
	if (x==xx)return abs(abs(x+d)-abs(x-d));
	if (y==yy)return d*2;
	ld k=1.0*(y-yy)/(x-xx),b=y-k*x;
	ld l=-1e5,r=1e5;
	while (l+eps*max((ld)1,l)<r){
		ld mid=(r-l)/3,m1=l+mid,m2=r-mid;
		xxx=m1;yyy=k*m1+b;sum1=doit(xxx,yyy);
		xxx=m2;yyy=k*m2+b;sum2=doit(xxx,yyy);
		if (sum1<sum2)l=m1;else r=m2;
	}xxx=l;yyy=k*l+b;return doit(xxx,yyy);
}
signed main(){
	freopen("distance.in","r",stdin);freopen("distance.out","w",stdout);
	n=read();d=read();for (int i=1;i<=n;i++)
		a[i].x=read(),a[i].y=read(),a[i].z=doit(a[i].x,a[i].y);
	sort(a+1,a+1+n,pd);n=min(n,850);
	ans=1e10;for (int i=1;i<=n;i++)
		for (int j=i+1;j<=n;j++)
			ans=min(ans,calc(a[i].x,a[i].y,a[j].x,a[j].y));
	printf("%.20Lf\n",ans);
}/*
2 5
1 0
2 1
*/
