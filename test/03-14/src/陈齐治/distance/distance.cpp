#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define db double
using namespace std;
const int N=500100;
const db eps=1e-8;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
int n;db a,ans;
struct cqz{db x,y;}p[N],P,Q,pp,d;
inline cqz operator-(cqz i,cqz j){return (cqz){i.x-j.x,i.y-j.y};}
inline db operator*(cqz i,cqz j){return i.x*j.y-j.x*i.y;}
inline void dis(cqz x,cqz y){
	db res=(y.x-x.x)*(y.x-x.x)+(y.y-x.y)*(y.y-x.y);
	if(res<ans)
	ans=res;
}
inline void cross(cqz&a,cqz&b,cqz&c,cqz&d,cqz&re){
	db s1,s2;
	s1=(b-a)*(d-a);
	s2=(c-a)*(b-a);
	s1=s1/(s1+s2);
	re.x=d.x+s1*(c.x-d.x);
	re.y=d.y+s1*(c.y-d.y);
	
}
inline void solve(cqz x,cqz y){
	db res=(y.y-x.y)/(y.x-x.x);
	res=x.y-x.x*res;
	if(res<-a+eps||res>a-eps)return;
	pp.x=P.x+y.y-x.y;
	pp.y=P.y+x.x-y.x;
	cross(x,y,P,pp,d);
	pp.x=d.x*2-P.x;
	pp.y=d.y*2-P.y;
	dis(Q,pp);
}
bool fff;
int main(){
	freopen("distance.in","r",stdin);
	freopen("distance.out","w",stdout);
	register int i,j,x,y;
	n=read();a=read();ans=2*a;ans=ans*ans;
	P.x=a;Q.x=-a;
	for(i=1;i<=n;i++){
		p[i].x=read(),p[i].y=read();
		if(p[i].x==0){
			if(fff){
				puts("0.0000000000000");
				return 0;
			}else fff=1;
		}
		if(n<15000)for(j=1;j<i;j++)solve(p[j],p[i]);
	}
	if(n>15000){
		srand(19260818);
		for(i=1e7+5e6;i;i--){
			x=rand()%n+1;
			y=rand()%n+1;
			if(y==x)y=y%n+1;
			solve(p[x],p[y]);
		}
	}
	printf("%.10lf\n",sqrt(ans));
	return 0;
}
