#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define db double
using namespace std;
const int N=400100;
inline int read(){
    int x=0,c=getchar(),f=0;
    for(;c>'9'||c<'0';f=c=='-',c=getchar());
    for(;c>='0'&&c<='9';c=getchar())
    x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
const db eps=1e-6;
double ans;
int n,m,h[N],L,ne[N],to[N],lv[N],ti,dfn[N],st[21][N],lg[N];
inline void addl(int x,int y){
	ne[++L]=h[x];h[x]=L;to[L]=y;
	ne[++L]=h[y];h[y]=L;to[L]=x;
}
inline void dfs(int x,int fa){
	lv[x]=lv[fa]+1;
	dfn[x]=++ti;st[0][ti]=x;
	for(int k=h[x];k;k=ne[k])
	if(to[k]!=fa)
	dfs(to[k],x),st[0][++ti]=x;
}
inline int Mn(int x,int y){
	return lv[x]<lv[y]?x:y;
}
inline void build(int n){
	register int k,i;
	lg[0]=-1;
	for(i=1;i<=n;i++)lg[i]=lg[i/2]+1;
	for(k=0;k<lg[n];k++)for(i=1;i<=n;i++)
	st[k+1][i]=Mn(st[k][i],st[k][i+(1<<k)]);
}
inline int lca(int x,int y){
	x=dfn[x];y=dfn[y];
	if(x>y)swap(x,y);
	int k=lg[y-x+1];
	y=y-(1<<k)+1;
	return Mn(st[k][x],st[k][y]);
}
struct cqz{int t,x,y,z;db v;}q[N];
inline void ask1(int ax,int ay,db ta,db va,int bx,int by,db tb,db vb){
	int c=lca(ax,bx),d;
	d=lv[ay]>lv[by]?ay:by;
	if(lv[c]<lv[d])return;
	db tac,tbc,tad,tbd,res;
	tac=ta+(lv[ax]-lv[c])/va;
	tad=ta+(lv[ax]-lv[d])/va;
	tbc=tb+(lv[bx]-lv[c])/vb;
	tbd=tb+(lv[bx]-lv[d])/vb;
	if(tac<tbc+eps&&tbd<tad+eps){
		res=(tbc-tac)*va;
		res=tbc+res/(vb-va);
		if(res<ans)ans=res;
	}
	if(tbc<tac+eps&&tad<tbd+eps){
		res=(tac-tbc)*vb;
		res=tac+res/(va-vb);
		if(res<ans)ans=res;
	}
}
inline void ask3(int ax,int ay,db ta,db va,int bx,int by,db tb,db vb){
	int c,d=lca(ay,by);
	c=lv[ax]>lv[bx]?ax:bx;
	if(lv[c]>lv[d])return;
	db tac,tbc,tad,tbd,res;
	tac=ta+(lv[c]-lv[ax])/va;
	tad=ta+(lv[d]-lv[ax])/va;
	tbc=tb+(lv[c]-lv[bx])/vb;
	tbd=tb+(lv[d]-lv[bx])/vb;
	if(tac<tbc+eps&&tbd<tad+eps){
		res=(tbc-tac)*va;
		res=tbc+res/(vb-va);
		if(res<ans)ans=res;
	}
	if(tbc<tac+eps&&tad<tbd+eps){
		res=(tac-tbc)*vb;
		res=tac+res/(va-vb);
		if(res<ans)ans=res;
	}
}
inline void ask2(int ax,int ay,db ta,db va,int bx,int by,db tb,db vb){
	int c,d=lca(ax,by);
	c=lv[ay]>lv[bx]?ay:bx;
	if(lv[c]>lv[d])return;
	db tac,tbc,tad,tbd,res;
	tac=ta+(lv[ax]-lv[c])/va;
	tad=ta+(lv[ax]-lv[d])/va;
	tbc=tb+(lv[c]-lv[bx])/vb;
	tbd=tb+(lv[d]-lv[bx])/vb;
	if(tad+eps>tbc&&tad<tbd+eps){
		res=lv[d]-lv[c]-(tad-tbc)*vb;
		res=tad+res/(va+vb);
		if(res<ans)ans=res;
	}
	if(tbc+eps>tad&&tbc<tac+eps){
		res=lv[d]-lv[c]-(tbc-tad)*va;
		res=tbc+res/(va+vb);
		if(res<ans)ans=res;
	}
}
inline void solve(cqz&a,cqz&b){
	double ta,tb;
	ta=a.t+(lv[a.x]+lv[a.y]-2*lv[a.z])/a.v;
	tb=b.t+(lv[b.x]+lv[b.y]-2*lv[b.z])/b.v;
	if(a.t>tb+eps)return;
	if(b.t>ta+eps)return;
	ta=a.t+(lv[a.x]-lv[a.z])/a.v;
	tb=b.t+(lv[b.x]-lv[b.z])/b.v;
	ask1(a.x,a.z,a.t,a.v,b.x,b.z,b.t,b.v);
	ask3(a.z,a.y,ta,a.v,b.z,b.y,tb,b.v);
	ask2(a.x,a.z,a.t,a.v,b.z,b.y,tb,b.v);
	ask2(b.x,b.z,b.t,b.v,a.z,a.y,ta,a.v);
}
int main(){
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	register int i,j;
	n=read();m=read();ans=2e9;
	if(m>15000){puts("-1");return 0;}
	for(i=1;i<n;i++)addl(read(),read());
	dfs(1,0);build(ti);
	for(i=1;i<=m;i++){
		q[i].t=read();q[i].v=read();
		q[i].x=read();q[i].y=read();q[i].z=lca(q[i].x,q[i].y);
		for(j=1;j<i;j++)solve(q[i],q[j]);
	}if(ans<1e9)printf("%.10lf\n",ans);
	else puts("-1");
	return 0;
}
