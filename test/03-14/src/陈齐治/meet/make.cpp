#include<ctime>
#include<map>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=500100,mod=1e9;
inline int read(){
    int x=0,c=getchar(),f=0;
    for(;c>'9'||c<'0';f=c=='-',c=getchar());
    for(;c>='0'&&c<='9';c=getchar())
    x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
void write(ll x){
    if(x>9)write(x/10);
    putchar(x%10+'0');
}
char s[N];
int n,m,o,w,T,P=97,x,y,a,b,x1,t,xx,yy,i,j,k;
ll rll(){
	return (ll)(rand()<<15)|rand();
}
map<int,bool>f[N];
int main(){
	freopen("meet.in","w",stdout);
	srand(unsigned(time(0)));
	n=100000;m=15000;T=100000;
	printf("%d %d\n",n,m);
	for(i=1;i<n;i++)printf("%d %d\n",rll()%i+1,i+1);
	for(i=1;i<=m;i++){
		x=rll()%n+1;
		y=rll()%n+1;
		printf("%d %d %d %d\n",rll()%T+1,rll()%n+1,x,y);
	}
	return 0;
}
