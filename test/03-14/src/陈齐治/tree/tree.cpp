#include<stdio.h>
#include<cstring>
#include<algorithm>
#define ll long long
#define md ((l+r)>>1)
using namespace std;
const int N=200100,oo=2e9;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,fa[N],L,h[N],ne[N],to[N];
void addl(int x,int y){
	ne[++L]=h[x];h[x]=L;to[L]=y;
}
int lv[N],sz[N],son[N],top[N],len[N],ti,dfn[N],id[N];
void dfs(int x){
	lv[x]=lv[fa[x]]+1;sz[x]=1;
	for(int y,k=h[x];k;k=ne[k]){
		dfs(y=to[k]);sz[x]+=sz[y];
		if(sz[y]>sz[son[x]])son[x]=y;
	}
}
void dfs(int x,int tp){
	dfn[x]=++ti;id[ti]=x;
	top[x]=tp;len[tp]++;
	if(son[x]<=n)dfs(son[x],tp);
	for(int k=h[x];k;k=ne[k])
	if(to[k]!=son[x])dfs(to[k],to[k]);
}
struct cqz{int x,t,i;}q[N];
inline bool operator<(cqz i,cqz j){
	if(i.t+lv[i.x]==j.t+lv[j.x])return i.x<j.x;
	else return i.t+lv[i.x]<j.t+lv[j.x];
}
struct seg{
	int tu,ps;
}tr[N*4];
int ls[N*4],rs[N*4],cv[N*4],sm,rt[N],ans[N];
void build(int&i,int l,int r){
	i=++sm;tr[i].ps=r;tr[i].tu=-1;cv[i]=-oo;
	if(l==r)return;build(ls[i],l,md);build(rs[i],md+1,r);
}
void down(int i,int l,int r);
seg max(seg x,seg y){
	if(x.tu-(y.ps-x.ps)>y.tu)return x;
	else return y;
}
void cov(int i,int l,int r,int x,int y,int k){
	if(l>=x&&r<=y){
		cv[i]=k;
		tr[i].ps=r;
		tr[i].tu=k+r;
		return;
	}down(i,l,r);
	if(x<=md)cov(ls[i],l,md,x,y,k);
	if(y>md)cov(rs[i],md+1,r,x,y,k);
	tr[i]=max(tr[ls[i]],tr[rs[i]]);
}
void down(int i,int l,int r){
	if(cv[i]==-oo)return;
	cov(ls[i],l,md,l,md,cv[i]);
	cov(rs[i],md+1,r,md+1,r,cv[i]);
	cv[i]=-oo;
}
bool can(seg&w,int x,int t){
	t+=x-w.ps;
	return w.tu>t;
}
int get(int i,int l,int r,int x,int t){
	if(l>x)return 0;
	if(!can(tr[i],x,t))return 0;
	if(l==r)return l;
	down(i,l,r);
	int re=get(rs[i],md+1,r,x,t);
	if(re)return re;
	return get(ls[i],l,md,x,t);
}
void solve(int X,int T,int I){
	int x,y,t,p,r;
	for(x=X,t=T;x>=0;x=fa[y]){
		y=top[x];
		p=lv[x]-lv[y]+1;
		r=get(rt[y],1,len[y],p,t);
		if(r){r=id[dfn[y]+r-1];break;}
		t+=p;
	}
	t=T+lv[X]+lv[X]-lv[r]-lv[r];
	ans[I]=t;
	for(x=X;top[x]!=top[r];x=fa[y]){
		y=top[x];
		p=lv[x]-lv[y]+1;
		t-=p;
		cov(rt[y],1,len[y],1,p,t);
	}
	y=top[x];
	r=lv[r]-lv[y]+1;
	p=lv[x]-lv[y]+1;
	t-=p;
	if(r<p)cov(rt[y],1,len[y],r+1,p,t);
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	register int i;
	n=read();m=read();
	fa[0]=-1;
	for(i=1;i<=n;i++)fa[i]=read(),addl(fa[i],i);
	for(i=0;i<=n;i++)son[i]=n+1;
	dfs(0);dfs(0,0);
	for(i=0;i<=n;i++)if(top[i]==i)build(rt[i],1,len[i]);
	cov(rt[0],1,len[0],1,1,oo);
	for(i=1;i<=m;i++)q[i].x=read(),q[i].t=read(),q[i].i=i;
	sort(q+1,q+m+1);
	for(i=1;i<=m;i++)solve(q[i].x,q[i].t,q[i].i);
	for(i=1;i<m;i++)printf("%d ",ans[i]);printf("%d\n",ans[m]);
	return 0;
}
