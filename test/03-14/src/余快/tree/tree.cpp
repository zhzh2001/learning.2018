#include<bits/stdc++.h>
#define ll long long
#define N 5005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m;
int Next[N*2],head[N],to[N*2],nedge;
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;}
#define V to[i]
struct node{
	int x,t;//
}z[N],p;
struct xx{
	int x,y;//当前节点，目标节点; 
}f[3][N],g[3][N];//f表示请求，g表示回答 
int vis[N],fa[N],yk[N][N],lon[N],fnum1,gnum1,fnum2,gnum2,tot,ans[N];
bool cmp(xx a,xx b){
	return a.x<b.x;
}
signed main(){
	freopen("tree.in","r",stdin);freopen("tree.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n;i++){fa[i]=read();add(fa[i],i);}
	for (int i=1;i<=m;i++){z[i].x=read();z[i].t=read();}
	tot=1;
	for (int t=z[1].t,num=0;num<=n;){
		for (int j=1;j<=fnum2;j++) f[1][j]=f[2][j];
		for (int j=1;j<=gnum2;j++) g[1][j]=g[2][j];
		fnum1=fnum2;gnum1=gnum2;fnum2=0;gnum2=0;
		while (z[tot].t==t&&tot<=n){
			f[1][++fnum1].x=z[tot].x;f[1][fnum1].y=tot;
			int x=z[tot].x;
			yk[tot][++lon[tot]]=x;//注意 
			tot++;
		}
		if (fnum1==0&&fnum2==0&&gnum1==0&&gnum2==0){
			if (tot>=n) break;
			else{t=z[++tot].t;continue;}
		}
		for (int j=1;j<=gnum1;j++){
			vis[g[1][j].x]=0;
			if (g[1][j].x==z[g[1][j].y].x) {ans[g[1][j].y]=t;continue;}
			gnum2++;g[2][gnum2].y=g[1][j].y;
			g[2][gnum2].x=yk[g[1][j].y][--lon[g[1][j].y]];//注意 
		}
		sort(f[1]+1,f[1]+fnum1+1,cmp);//等会可以优化一下用桶排 
		for (int j=1;j<=fnum1;j++){
			int xpp=f[1][j].x,xpp2=f[1][j].y;
			if (vis[xpp]||xpp==0){
				if (xpp==z[xpp2].x) {ans[xpp2]=z[xpp2].t;continue;}
				gnum2++;g[2][gnum2].x=yk[xpp2][--lon[xpp2]];g[2][gnum2].y=xpp2;
			}
			else{
				vis[xpp]=1;yk[xpp2][++lon[xpp2]]=fa[xpp];f[2][++fnum2].x=fa[xpp];f[2][fnum2].y=xpp2;
			}
		}
		t++;
	}
	for (int i=1;i<=m;i++) wri(ans[i]);
	return 0;
}
/*
8 3
0 1 1 2 3 3 4 5
6 1
8 2
4 5
*/
