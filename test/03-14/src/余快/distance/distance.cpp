#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define db double
#define eps (1e-8)
#define re register
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,c;db ans;
struct p{db x,y;}z[N];
inline db get_dis(p a,p b){return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));}
inline db check(p a){return get_dis(a,z[0])-get_dis(a,z[n+1]);}
inline db get_ans(p a,p b){
	re db k=(b.y-a.y)/(b.x-a.x);
	re db b1=a.y-a.x*k;
	re p f1,f2;
	re db l=0,r=1e5,mid1,mid2;	
	while (r-l>eps){
		mid1=l+(r-l)/3;f1.x=mid1;f1.y=mid1*k+b1;
		mid2=r-(r-l)/3;f2.x=mid2;f2.y=mid2*k+b1;
//		printf("%.10lf %.10lf\n",mid1,mid2);		
		if (check(f1)>check(f2)) l=mid1;
		else r=mid2;
	}
	f1.x=l;f1.y=l*k+b1;
	re db xpp=fabs(check(f1));
	l=-1e5;r=0;
	while (r-l>eps){
		mid1=l+(r-l)/3;f1.x=mid1;f1.y=mid1*k+b1;
		mid2=r-(r-l)/3;f2.x=mid2;f2.y=mid2*k+b1;
//		printf("%.10lf %.10lf\n",r-l,eps);		
		if (check(f1)<check(f2)) l=mid1;
		else r=mid2;
	}
	f1.x=l;f1.y=l*k+b1;
	xpp=max(xpp,fabs(check(f1)));
	return xpp;
}
void solve(p a,p b){
	if (a.x==b.x){ans=min(ans,fabs(a.x*2));return;}
	if (a.y==b.y){a.x=c;ans=min(ans,fabs(check(a)));return;}
	re db k=(b.y-a.y)/(b.x-a.x);
	re db b1=a.y-a.x*k;
	re db yk=-b1/k;
	if (fabs(yk)+eps>c) return;
	p xpp;xpp.x=c;xpp.y=b1+k*c;
	db wzp=fabs(check(xpp));
	if (wzp<ans) ans=min(ans,get_ans(a,b));
}
signed main(){
	freopen("distance.in","r",stdin);freopen("distance.out","w",stdout);
	n=read();c=read();z[0].x=c;z[0].y=0;z[n+1].x=-c;z[n+1].y=0;
	for (int i=1;i<=n;i++){z[i].x=read();z[i].y=read();}
	ans=c*2;
	for (int i=1;i<n;i++){
		for (int j=i+1;j<=n;j++){
			solve(z[i],z[j]);
		}
	}
	printf("%.10lf",ans);
	return 0;
}
/*
2 5
1 1
2 2
*/
