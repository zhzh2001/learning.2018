#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,c,x,y,vis[100000005];
int get(){
	int a=rand()%500+1;
	int b=rand()%2;
	if (b) return a;
	return -a;
}
signed main(){
	srand(time(NULL));
	freopen("distance.in","w",stdout);
	n=30000;c=rand()%500+1;
	wri(n);wrn(c);
	for (int i=1;i<=n;i++){
		x=get();y=get();
		while (vis[x+500+(y+500)*500]) x=get(),y=get();
		vis[x+500+(y+500)*500]=1;
		wri(x);wrn(y);
	}
	return 0;
}
