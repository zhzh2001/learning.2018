#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=200005,inf=2e9;
int fa[N],x[N],t[N],bj[N],ans[N];
pair<int,int> f[N],g[N];
int n,m,deep[N];
int main(){
	freopen("tree.in","r",stdin); freopen("tree.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=n;i++){
		deep[i]=deep[fa[i]=read()]+1;
	}
	for(int i=1;i<=m;i++){
		x[i]=read(); ans[i]=t[i]=read();
		for(int j=t[i-1];j<t[i]&&f[0].first;j++){
			sort(&f[1],&f[f[0].first+1]);
			for(int k=1;k<=f[0].first;k++){
				int t=f[k].first,id=f[k].second;
				if(bj[t]>j||!t){
					ans[id]+=(deep[x[id]]-deep[t])*2;
					for(int k=x[id];k!=t;k=fa[k])bj[k]=j+deep[k]-deep[t];
				}else{
					bj[t]=inf; g[++g[0].first]=(mp(fa[t],id));
				}
			}
			f[0]=g[0]; for(int i=1;i<=f[0].first;i++)f[i]=g[i]; g[0].first=0;
		}
		f[++f[0].first]=mp(x[i],i);
	}
	for(int j=t[m];f[0].first;j++){
		sort(&f[1],&f[f[0].first+1]);
		//cout<<bj[4]<<endl;
		for(int k=1;k<=f[0].first;k++){
			int t=f[k].first,id=f[k].second;
			if(bj[t]>j||!t){
				ans[id]+=(deep[x[id]]-deep[t])*2;
				for(int k=x[id];k!=t;k=fa[k])bj[k]=j+deep[k]-deep[t];
			}else{
				bj[t]=inf; g[++g[0].first]=(mp(fa[t],id));
			}
		}
		f[0]=g[0]; for(int i=1;i<=f[0].first;i++)f[i]=g[i]; g[0].first=0;
	}
	for(int i=1;i<=m;i++)write(ans[i]),putchar(' ');
}
/*
6 3
0 1 2 3 2 5
4 6
6 9
5 11
3 2
0 1 1
2 1
3 1
8 3
0 1 1 2 3 3 4 5
6 1
8 2
4 5
6 3
0 1 2 3 2 5
4 6
6 9
5 12
*/
