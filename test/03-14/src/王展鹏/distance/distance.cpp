#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const double oo=1e100,eps=1e-10,pi=2*asin(1);
struct point{
	double x,y;
	point(double _x=0,double _y=0):x(_x),y(_y){}
	point operator +(const point b){return point(x+b.x,y+b.y);}
	point operator -(const point b){return point(x-b.x,y-b.y);}
};
inline double dist(point x,point y){return sqrt((x.x-y.x)*(x.x-y.x)+(x.y-y.y)*(x.y-y.y));}
inline point rotate(point x,double angle){
	return point(x.x*cos(angle)-x.y*sin(angle),x.y*cos(angle)+x.x*sin(angle));
}
const int N=300005;
int n,A,tong[N];
double ans=1e18;
point a[N];
inline bool cmp(point x,point y){
	return x.x<y.x;
}
set<pair<int,int> > s;
set<pair<int,int> >::iterator it;
int main(){
	freopen("distance.in","r",stdin); freopen("distance.out","w",stdout);
	n=read(); A=read(); int ttt=7000000/n;
	for(int i=1;i<=n;i++){
		int x=read(),y=read();
		a[i]=point(x,y);
	}
	sort(&a[1],&a[n+1],cmp);
	for(int i=2;i<=n;i++){
		double t=1e18; int pos=0;
		/*for(it=s.begin();it!=s.end();it++){
			int j=(*it).second;
			if(a[i].x==a[j].x){
				if(fabs(2*a[i].x)<t){t=fabs(2*a[i].x); pos=j;}
			}else{
				double k=(a[i].y-a[j].y)/(a[i].x-a[j].x),b=(a[i].y-k*a[i].x);
				point zs=point(-b/k,0);
				point zz=rotate(point(-A,0)-zs,-2*atan(k))+zs;
				//cout<<zz.x<<" "<<zz.y<<" "<<(point(0,-A)-zs).x<<" "<<(point(0,-A)-zs).y<<endl;
				if(dist(point(A,0),zz)<t){
					t=dist(point(A,0),zz); pos=j;
				}
			}
		}*/
		for(int j=max(i-ttt,1);j<i;j++){
			if(a[i].x==a[j].x){
				if(fabs(2*a[i].x)<t){t=fabs(2*a[i].x); pos=j;}
			}else{
				double k=(a[i].y-a[j].y)/(a[i].x-a[j].x),b=(a[i].y-k*a[i].x);
				point zs=point(-b/k,0);
				point zz=rotate(point(-A,0)-zs,-2*atan(k))+zs;
				//cout<<zz.x<<" "<<zz.y<<" "<<(point(0,-A)-zs).x<<" "<<(point(0,-A)-zs).y<<endl;
				if(dist(point(A,0),zz)<t){
					t=dist(point(A,0),zz); pos=j;
				}
			}
		}
		ans=min(ans,t);
		/*if(s.count(mp(tong[pos],pos))){
			s.erase(mp(tong[pos],pos)); s.insert(mp(++tong[pos],pos));
		}else{
			if(s.empty())s.insert(mp(++tong[pos],pos)); else
			if(++tong[pos]>=(*s.begin()).first){
				s.erase(s.begin());
				s.insert(mp(tong[pos],pos));
			}
		}*/
	}
	//for(int i=1;i<=n;i++)cout<<tong[i]<<" "; puts("");
	printf("%.8lf\n",ans);
}
