#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
int n,m;
int main(){
	srand(time(0));
	freopen("meet.in","w",stdout);
	n=100000; m=1000;
	cout<<n<<" "<<m<<endl;
	for(int i=1;i<n;i++){
		cout<<rand()%i+1<<" "<<i+1<<endl;
	}
	for(int i=1;i<=m;i++){
		cout<<rand()%100<<" "<<rand()%10<<" "<<rand()%n+1<<" "<<rand()%n+1<<endl;
	}
}

