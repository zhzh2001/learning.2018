#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=100005,K=20;
int n,m,dp[N][K],deep[N],in[N],out[N],ti,u[N],v[N],c[N];
double t[N],ans=4e18;
vector<int> e[N];
#define fa(a,b) (in[a]<=in[b]&&out[a]>=out[b])
inline int LCA(int a,int b){
	if(fa(a,b))return a; else if(fa(b,a))return b;
	for(int i=K-1;i>=0;i--)if(!fa(dp[a][i],b))a=dp[a][i];
	return dp[a][0];
}
inline int get(int a,int b,double high){
	if(deep[b]>deep[a]){
		swap(a,b);
	}
	int t=deep[a]-high;
	for(int i=K-1;i>=0;i--)if(t>>i&1)a=dp[a][i];
	return a;
}
void dfs(int p,int fa){
	in[p]=++ti; deep[p]=deep[fa]+1; dp[p][0]=fa;
	for(unsigned i=0;i<e[p].size();i++)if(e[p][i]!=fa){
		dfs(e[p][i],p);
	}
	out[p]=ti;
}
int main(){
	freopen("meet.in","r",stdin); freopen("meet.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<n;i++){
		int s=read(),t=read();
		e[s].push_back(t); e[t].push_back(s);
	}
	dfs(1,0);
	for(int j=1;j<K;j++)for(int i=1;i<=n;i++)dp[i][j]=dp[dp[i][j-1]][j-1];
	for(int i=1;i<=m;i++){
		int tt=read(),cc=read(),uu=read(),vv=read();
		int zs=LCA(uu,vv);
		u[i*2-1]=uu; v[i*2-1]=zs; t[i*2-1]=tt; c[i*2-1]=-cc;
		u[i*2]=zs; v[i*2]=vv; t[i*2]=tt+(double)(deep[uu]-deep[zs])/cc; c[i*2]=cc;
		//cout<<t[i*2-1]<<" "<<t[i*2]<<endl;
	}
	for(int i=1;i<=2*m;i++){
		for(int j=(i&1)?i+2:i+1;j<=2*m;j++)if(max(t[i],t[j])<ans){
			double high=(-deep[u[i]]*c[j]+t[i]*c[i]*c[j]-t[j]*c[i]*c[j]+deep[u[j]]*c[i])/(c[i]-c[j]);
			int t1=get(u[i],v[i],high),t2=get(u[j],v[j],high);
			//if(i==2&&j==7)cout<<(high-deep[u[i]])/c[i]+t[i]<<" "<<high-deep[u[i]]<<endl;
			int u1=deep[u[i]]>deep[v[i]]?u[i]:v[i],v1=u[i]^v[i]^u1,u2=deep[u[j]]>deep[v[j]]?u[j]:v[j],v2=u[j]^v[j]^u2;
			if(t1&&fa(t1,u1)&&fa(v1,t1)&&fa(t2,u2)&&fa(v2,t2)){
				//if(t1==t2&&(high-deep[u[i]])/c[i]>=0&&high>=0)cout<<i<<" "<<j<<" "<<high<<" "<<t1<<" "<<(high-deep[u[i]])/c[i]+t[i]<<endl;
				if(t1==t2&&(high-deep[u[i]])/c[i]>=0&&high>=0)ans=min(ans,(high-deep[u[i]])/c[i]+t[i]);
			}
		}
	}
	if(ans>1e18)puts("-1"); else cout<<ans<<endl;
}
//-deep[u[i]]*c[j]+t[i]*c[i]*c[j]-t[j]*c[i]*c[j]+deep[u[j]]*c[i]=high
//(high-deep[u[i]])/c[i]+t[i]
/*
6 4
2 5
6 5
3 6
4 6
4 1
27 6 1 3
9 5 1 6
27 4 3 4
11 29 2 6

6 4
3 1
4 5
6 4
6 1
2 6
16 4 4 5
13 20 6 2
3 16 4 5
28 5 3 5
*/
