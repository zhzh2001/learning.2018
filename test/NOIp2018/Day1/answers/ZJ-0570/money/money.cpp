#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define rt register int
#define r read()
#define l putchar('\n')
using namespace std;
ll read(){
	ll x=0;char ch=getchar(),zf=1;
	while(!isdigit(ch)&&ch!='-')ch=getchar();
	if(ch=='-')ch=getchar(),zf=-1;
	while(isdigit(ch))x=x*10+ch-'0',ch=getchar();
	return x*zf;
}
void write(ll x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);l;}
int i,j,k,m,n,x,y,z,cnt;
bool b[25010];int a[105];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(rt T=r;T;T--){
		n=r;
		memset(b,0,sizeof(b));b[0]=1;
		int mmax=0;
		for(rt i=1;i<=n;i++)
		a[i]=r,mmax=max(mmax,a[i]);
		sort(a+1,a+n+1);int ans=0;
		for(rt i=1;i<=n;i++)if(!b[a[i]]){
			ans++;
			for(rt j=a[i];j<=mmax;j++)
			b[j]|=b[j-a[i]];
		}
		writeln(ans);
	}
	return 0;
}

