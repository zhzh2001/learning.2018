#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<vector>
#define M 100010
#define ll long long
#define rt register int
#define r read()
#define l putchar('\n')
using namespace std;
ll read(){
	ll x=0;char ch=getchar(),zf=1;
	while(!isdigit(ch)&&ch!='-')ch=getchar();
	if(ch=='-')ch=getchar(),zf=-1;
	while(isdigit(ch))x=x*10+ch-'0',ch=getchar();
	return x*zf;
}
void write(ll x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);l;}
int i,j,k,m,n,x,y,z,cnt;
int F[M],L[M],N[M],a[M],c[M];
void add(int x,int y,int z){
	a[++k]=y;c[k]=z;
	if(!F[x])F[x]=k;
	else N[L[x]]=k;
	L[x]=k;
}
struct num{
	int sl,up;
};
vector<int>son[50010];
int v[50010];
int check(int n,int ans,int gg){
	int ret=0;
	for(rt i=n,j=1;i>=1;i--)if(i!=gg){
		while((v[i]+v[j]<ans||j==gg)&&j<i)j++;
		if(j>=i)return ret;
		j++;ret++;
	}
	return ret;
}
num dfs(int x,int pre,int ans){
	int ret=0;
	for(rt i=F[x];i;i=N[i])if(a[i]!=pre){
		const num now=dfs(a[i],x,ans);
		ret+=now.sl;
		if(now.up+c[i]>=ans)ret++;
		else son[x].push_back(now.up+c[i]);
	}
	int sz=son[x].size();
	if(!sz)return (num){ret,0};
	for(rt i=1;i<=sz;i++)v[i]=son[x][i-1];
	sort(v+1,v+sz+1);int sum=check(sz,ans,0);
	if(sum*2==sz)return (num){ret+sum,0};
	int L=1,R=sz,wz=1;
	while(L<=R){
		const int mid=L+R>>1;
		if(check(sz,ans,mid)==sum)wz=mid,L=mid+1;
		else R=mid-1;
	}return (num){ret+sum,v[wz]};
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=r;m=r;ll v=0;
	for(rt i=1;i<n;i++){
		x=r;y=r;z=r;
		add(x,y,z);
		add(y,x,z);
		v+=z;
	}
	int L=1,R=v/m,ans=1;
	while(L<=R){
		for(rt i=1;i<=n;i++)son[i].clear();
		const int mid=L+R>>1;
		if(dfs(1,1,mid).sl>=m)ans=mid,L=mid+1;
		else R=mid-1;
	}
	writeln(ans);
	return 0;
}

