#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define rt register int
#define r read()
#define l putchar('\n')
using namespace std;
ll read(){
	ll x=0;char ch=getchar(),zf=1;
	while(!isdigit(ch)&&ch!='-')ch=getchar();
	if(ch=='-')ch=getchar(),zf=-1;
	while(isdigit(ch))x=x*10+ch-'0',ch=getchar();
	return x*zf;
}
void write(ll x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);l;}
int i,j,k,m,n,x,y,z,cnt;
int a[200010],Min[20][200010],LG[200010];
int RMQ(int L,int R){
	int len=LG[R-L+1];
	x=Min[len][L],y=Min[len][R-(1<<len)+1];
	if(a[x]<a[y])return x;else return y;
}
ll calc(int L,int R,int tag){
	if(L>R)return 0;
	if(L==R)return a[L]-tag;
	int x=RMQ(L,R);
	return a[x]-tag+calc(L,x-1,a[x])+calc(x+1,R,a[x]);
}
int main(){
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	n=r;
	for(rt i=1;i<=n;i++)a[i]=r,Min[0][i]=i;
	for(rt i=1;i<=18;i++)
	for(rt j=1;j<=n;j++){
		x=Min[i-1][j],y=Min[i-1][min(j+(1<<i-1),n)];
		if(a[x]<a[y])Min[i][j]=x;else Min[i][j]=y;
	}
	for(rt i=2;i<=n;i++)LG[i]=(LG[i>>1]+1);
	writeln(calc(1,n,0));
	return 0;
}

