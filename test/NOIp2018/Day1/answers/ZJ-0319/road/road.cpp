#include<bits/stdc++.h>
using namespace std;
int n;
long long mmm;
long long a[200000];
int f[200000][30];

int read()
{
	char ch=getchar();
	int x=0,f=1;
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		x=x*10-48+ch;
		ch=getchar();
	}
	return x*f;
}
int l2(int x)
{
	int y=1,z=1;
	while(x>=y)
		y=y*2,z++;
	return z-1;
}
int cxx(int l,int r)
{
	int k=l2(r-l+1);
	if(a[f[l][k]]<a[f[r-(1<<(k-1))+1][k]])
		return f[l][k];
	else
		return f[r-(1<<(k-1))+1][k];
}
void solv(int l,int r,long long v)
{
	if(l>r) return;
	long long v1=cxx(l,r);
	mmm+=a[v1]-v;
	solv(l,v1-1,a[v1]);
	solv(v1+1,r,a[v1]);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read(),f[i][1]=i;
	a[0]=2147483647;
	int k=l2(n);
	for(int i=2;i<=k;i++)
		for(int j=1;j<=n;j++)
			f[j][i]=a[f[j][i-1]]<a[f[j+(1<<(i-2))][i-1]]?f[j][i-1]:f[j+(1<<(i-2))][i-1];
	solv(1,n,0);
	printf("%lld\n",mmm);
	return 0;
}
