#include<bits/stdc++.h>
using namespace std;
int a[10000];
int f[300000];
int T_T;
int n;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T_T);
	while(T_T--)
	{
		memset(f,0,sizeof(f));
		scanf("%d",&n);
		for(int i=1;i<=n;i++) scanf("%d",&a[i]);
		if(n==1)
		{
			printf("%d\n",n);
			continue;
		}
		sort(a+1,a+n+1);
		f[0]=1;
		for(int i=a[1];i<=a[n];++i)
			f[i]=f[i]||f[i-a[1]];
		int s=1,qq=n;
		while(qq>1&&f[a[qq]]) qq--;
		for(int i=2;i<=n;++i)
			if(f[a[i]]==0)
			{
				++s;
				for(int j=a[i];j<=a[qq];++j)
					f[j]=f[j]||f[j-a[i]];
				while(qq>i&&f[a[qq]]) qq--;
				if(qq<=i) break;
			}
		printf("%d\n",s);
	}
	return 0;
}
