#include<bits/stdc++.h>
using namespace std;
inline int read() {
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int f[25005], n, a[105];
inline void solve() {
  n = read();
  for (int i = 1; i <= n; i++) a[i] = read();
  sort(a+1,a+1+n);
  memset(f, 0, sizeof f);
  int ans = 0, mx = a[n];
  f[0] = 1;
  for (int i = 1; i <= n; i++) {
    int x = a[i];
    if (!f[x]) {
      ans++;
      for (int j = 0; j <= mx; j++)
        f[j+x] |= f[j];
    }
  }
  printf("%d\n", ans);
}
int main() {
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  int T = read();
  while (T--) solve();
  return 0;
}
