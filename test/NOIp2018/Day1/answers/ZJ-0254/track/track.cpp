#include<bits/stdc++.h>
using namespace std;
inline int read() {
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
const int N = 50005, oo = 1e9;
int n, m, cnt, head[N];
struct edge {int nxt, to, w;}e[N*2];
inline void insert(int u, int v, int w) {
  e[++cnt] = (edge) {head[u], v, w}; head[u] = cnt;
  e[++cnt] = (edge) {head[v], u, w}; head[v] = cnt;
}
namespace zhijing {
  int f[N], ans;
  inline void dfs(int x, int fa) {
    for (int y, i = head[x]; i; i = e[i].nxt)
      if ((y = e[i].to) != fa) {
        f[y] = f[x]+e[i].w;
        dfs(y, x);
      }
  }
  inline void dfs2(int x, int fa) {
    for (int y, i = head[x]; i; i = e[i].nxt) 
      if ((y = e[i].to) != fa) {
        f[y] = f[x]+e[i].w;
        dfs(y, x);
      }
  }
}
namespace lian{
  int a[N];
  inline bool check(int mid) {
    int now = 0, sum = 0;
    for (int i = 1; i < n; i++) {
      now += a[i];
      if (now > mid) sum++, now = 0;
    }
    return (sum >= m);
  }
  inline void solve() {
    int l = 0, r = oo, ans = 0;
    while (l <= r) {
      int mid = l+r>>1;
      if (check(mid)) ans = mid, l = mid+1;
      else r = mid-1;
    }
    printf("%d\n", ans);
  }
}
namespace juhua{
  int tot, a[N];
  inline int check(int mid) {
    multiset <int> s;
    int sum = 0;
    multiset <int>::iterator it;
    for (int i = 1; i <= tot; i++){
      if (a[i] < mid) s.insert(a[i]);
      else sum++;
    }
    s.insert(oo);
    for (int i = 1; i <= tot; i++)
      if (s.find(a[i]) != s.end() &&a[i] < mid) {
        it = s.find(a[i]);
        s.erase(it);
        it = s.lower_bound(mid-a[i]);
        if ((*it) != oo) {
          s.erase(it);
          sum++;
        }
      }
    return (sum >= m);
  }
  inline void solve() {
    sort(a+1,a+1+tot);
    int l = 0, r = oo, ans = 0;
    while (l <= r) {
      int mid = l+r>>1;
      if (check(mid)) ans = mid, l = mid+1;
      else r = mid-1;
    }
    printf("%d\n", ans);
  }
}
int main() {
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  n = read(); m = read();
  int flag1 = 1, flag2 = 1;
  for (int i = 1; i < n; i++) {
    int x = read(), y = read(), w = read();
    insert(x, y, w);
    if (x != 1) flag1 = 0;
    if (y != x+1) flag2 = 0;
  }
  if (m == 1) {
    using namespace zhijing;
    dfs(1, 0);
    int mx = 0;
    for (int i = 1; i <= n; i++)
      if (f[mx] < f[i]) mx = i;
    memset(f, 0, sizeof f);
    dfs2(mx, 0);
    mx = 0;
    for (int i = 1; i <= n; i++)
      mx = max(mx, f[i]);
    printf("%d\n", mx);
  }else if (flag2) {
    using namespace lian;
    for (int i = 1; i < cnt; i += 2) {
      int x = e[i].to;
      a[x-1] = e[i].w;
    }
    solve();
  }else if (flag1) {
    using namespace juhua;
    tot = 0;
    for (int i = 1; i < cnt; i += 2) a[++tot] = e[i].w;
    solve();
  }
  return 0;
}
