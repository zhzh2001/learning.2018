#include<bits/stdc++.h>
using namespace std;
inline int read() {
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
const int N = 100005;
int n, mx, b[N];
vector <int> a[10005];
set <pair<int, int> > s;
set <pair<int, int> >::iterator it;
int main() {
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
	n = read();
	for (int i = 1; i <= n; i++) {
		b[i] = read();
		a[b[i]].push_back(i);
		mx = max(mx, b[i]);
	}
	int now = n;
	int m = a[0].size();
	int flag = 0;
	for (int i = m-1; i >= 0; i--) {
	  flag = 1;
		if (now != a[0][i]) s.insert(make_pair(a[0][i]+1, now));
		now = a[0][i]-1;
	}
	s.insert(make_pair(n+1,n+1));
	if (!flag) s.insert(make_pair(1, n));
	else {
	  if (now) s.insert(make_pair(1, now));
  }
	long long ans = 0;
	for (int i = 1; i <= mx; i++) {
	  m = s.size();
		ans += m-1;
		m = a[i].size();
		for (int j = 0; j < m; j++) {
			int x = a[i][j];
			it = s.lower_bound(make_pair(x, 0));
			pair <int, int> p;
			if ((*it).first > x) p = *--it; 
			else p = *it;
			if (p.first == p.second) s.erase(p);
			else {
				pair <int, int> A, B;
				A = make_pair(p.first, x-1);
				B = make_pair(x+1,p.second);
				if (p.first < x) s.insert(make_pair(p.first, x-1));
        if (x < p.second) s.insert(make_pair(x+1, p.second));
				s.erase(p);
			}
		}
	}
	printf("%lld\n", ans);
	return 0;
}
/*
6
4 3 2 5 3 5
*/
