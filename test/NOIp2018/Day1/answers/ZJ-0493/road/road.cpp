#include<bits/stdc++.h>
#define ll long long
using namespace std;

char getch()
{
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}

void read(ll &x)
{
	x=0;
	char c=getch();
	while(c<'0'||c>'9') c=getch();
	while(c>='0'&&c<='9')
	{
		x=(x<<1)+(x<<3)+(c-'0');
		c=getch();
	}
}

const ll INF=2147483647; 
ll n,ans;
ll a[100001];

struct Segment_tree
{
	ll bit;
	ll tree[500003],tag[500003];
	void push_up(ll p)
	{
		tree[p]=tree[p<<1]<tree[p<<1|1]?tree[p<<1]:tree[p<<1|1];
		tag[p]=tree[p<<1]<tree[p<<1|1]?tag[p<<1]:tag[p<<1|1];
	}
	void build(ll n)
	{
		for(bit=1;bit<=n+1;bit<<=1);
		for(ll i=bit+1;i<=bit+n;++i) tree[i]=a[i-bit],tag[i]=i-bit;
		for(ll i=bit+n+1;i<(bit<<1);++i) tree[i]=INF,tag[i]=i-bit;
		for(ll i=bit-1;i;--i) push_up(i);
	}
	ll query(ll s,ll t)
	{
		ll ans=INF,ansn;
		for(s=s+bit-1,t=t+bit+1;s^t^1;s>>=1,t>>=1)
		{
			if((~s&1)&&(ans>tree[s^1]))
			{
				ans=tree[s^1];
				ansn=tag[s^1];
			}
			if(( t&1)&&(ans>tree[t^1]))
			{
				ans=tree[t^1];
				ansn=tag[t^1];
			} 
		}
		return ansn;
	}
}t;

void dfs(ll l,ll r)
{
	if(l>r) return;
	if(l==r)
	{
		ans+=a[l];
		return;
	}
	ll minn=t.query(l,r),minm=a[minn];
	ans+=minm;
	for(ll i=l;i<=r;++i)
	{
		a[i]-=minm;
		if(a[i]==0)
		{
			dfs(l,i-1);
			l=i+1;
		}
	}
	if(l==r) ans+=a[l];
	else dfs(l,r);
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for(ll i=1;i<=n;++i) read(a[i]);
	t.build(n);
	dfs(1,n);
	printf("%d",ans);
	return 0;
}
