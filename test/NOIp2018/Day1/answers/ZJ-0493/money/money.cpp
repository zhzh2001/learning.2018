#include<bits/stdc++.h>
using namespace std;

char getch()
{
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}

void read(int &x)
{
	x=0;
	char c=getch();
	while(c<'0'||c>'9') c=getch();
	while(c>='0'&&c<='9')
	{
		x=(x<<1)+(x<<3)+(c-'0');
		c=getch();
	}
}

int t,n,cnt,a;
int vis[25001],prime[25001],p[25000];

void get_prime()
{
	for(int i=2;i<=25000;++i)
	{
		if(!vis[i]) prime[++cnt]=i;
		for(int j=1;j<=cnt&&i*prime[j]<=25000;++j)
		{
			vis[i*prime[j]]=1;
			if(i%prime[j]==0) break;
		}
	}
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	get_prime();
	read(t);
	while(t--)
	{
		memset(p,0,sizeof(p));
		int ans=0;
		read(n);
		for(int i=1;i<=n;++i)
		{
			read(a); if(a==1) p[1]++;
			for(int j=1;j<=cnt;++j)
			{
				if(a%prime[j]==0) p[prime[j]]++;
				while(a%prime[j]==0) a/=prime[j];
			}
		}
		for(int i=1;i<=25000;++i)
			if(p[i]) ans++;
		printf("%d\n",ans);
	}
	return 0;
}
/*
�Ż���sqrt��25000�� 
*/
