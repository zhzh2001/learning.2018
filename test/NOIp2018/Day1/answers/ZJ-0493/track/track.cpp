#include<bits/stdc++.h>
#define ll long long
using namespace std;

char getch()
{
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}

void read(ll &x)
{
	x=0;
	char c=getch();
	while(c<'0'||c>'9') c=getch();
	while(c>='0'&&c<='9')
	{
		x=(x<<1)+(x<<3)+(c-'0');
		c=getch();
	}
}

ll n,m,cnt,ans,sum,flag1,flag2;
ll head[50003];

struct node
{
	ll nxt,v,w;
}e[100001];

struct edge
{
	ll x,y,z;
}ed[100001];

void add(ll u,ll v,ll w)
{
	e[++cnt].v=v;
	e[cnt].w=w;
	e[cnt].nxt=head[u];
	head[u]=cnt;
}

ll a[50003];
ll cmp(edge a,edge b){return a.z>b.z;}
void work1()
{
	ll minn=2147483647,last=min(n-1,m<<1);
	sort(ed+1,ed+n,cmp);
	for(ll i=1;i<=m;++i) a[i]=ed[i].z;
	for(ll i=m+1;i<=last;++i)
		a[(m<<1)-i+1]+=ed[i].z;
	for(ll i=1;i<=m;++i) minn=min(a[i],minn);
	printf("%d",minn);
}

ll check(ll x)
{
	ll num=0,now=0;
	for(ll i=1;i<n;++i)
	{
		now+=ed[i].z;
		if(now>=x)
		{
			num++;
			now=0;
		}
	}
	return num>=m;
}
void work2()
{
	ll l=1,r=sum,minn;
	while(l<=r)
	{
		ll mid=(l+r)>>1;
		if(check(mid))
		{
			minn=mid;
			l=mid+1;
		}
		else r=mid-1;
	}
	printf("%d",minn);
}

ll val[50003],son[50003];
void dfs1(ll u,ll f,ll w)
{
	val[u]=w;
	for(ll i=head[u];i;i=e[i].nxt)
	{
		ll v=e[i].v;
		if(v==f) continue;
		dfs1(v,u,e[i].w);
		val[u]+=val[v];
		if(val[v]>val[son[u]]) son[u]=v;
	}
}
ll dfs2(ll u)
{
	if(!son[u]) return u;
	return dfs2(son[u]);
}
void work3()
{
	dfs1(1,0,0);
	ll l=dfs2(1);
	memset(val,0,sizeof(val));
	memset(son,0,sizeof(son));
	dfs1(l,0,0);
	printf("%d",val[l]);
	cerr<<val[l];
}

void work()
{
	printf("%d",ed[n-1].z);
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);flag1=flag2=1;
	for(ll i=1,x,y,z;i<n;++i)
	{
		read(ed[i].x);read(ed[i].y);read(ed[i].z);
		add(ed[i].x,ed[i].y,ed[i].z);
		add(ed[i].y,ed[i].x,ed[i].z);
		sum+=ed[i].z;
		if(ed[i].x!=1) flag1=0;
		if(ed[i].y!=ed[i].x+1) flag2=0;
	}
	if(flag1==1) work1();
	else if(flag2==1) work2();
	else if(m==1) work3();
	else work();
	return 0;
}
