#include<cstdio>
#include<cstring>
#include<algorithm>
#define cl(x,y) memset(x,y,sizeof(x))
using namespace std;

const int maxn=105,maxv=25005;
int tst,n,m,V,a[maxn];
bool f[maxv];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&tst);
	while (tst--){
		scanf("%d",&n);m=n;V=0;
		for (int i=1;i<=n;i++)
		 scanf("%d",&a[i]),V=max(V,a[i]);
		sort(a+1,a+1+n);
		cl(f,0); f[0]=1;
		for (int i=1;i<=n;i++){
			if (f[a[i]]) {m--;continue;}
			for (int j=a[i];j<=V;j++)
			 f[j]|=f[j-a[i]];
		}
		printf("%d\n",m);
	}
	return 0;
}
