#include<cstdio>
#include<set>
#include<algorithm>
using namespace std;
inline char nc(){
	static char buf[100000],*l=buf,*r=buf;
	return l==r&&(r=(l=buf)+fread(buf,1,100000,stdin),l==r)?EOF:*l++;
}
inline int red(){
	int res=0,f=1;char ch=nc();
	while (ch<'0'||'9'<ch) {if (ch=='-') f=-f;ch=nc();}
	while ('0'<=ch&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}

const int maxn=50005,maxe=100005,INF=0x3f3f3f3f;
int n,m;
int tot,son[maxe],nxt[maxe],lnk[maxn],w[maxe];
inline void add(int x,int y,int z){
	son[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;w[tot]=z;
}
multiset<int> S[maxn];
int MID,cnt,f[maxn];
void dfs(int x,int fa){
	S[x].clear();f[x]=0;
	for (int j=lnk[x];j;j=nxt[j])
	 if (son[j]!=fa){
	 	dfs(son[j],x);
		S[x].insert(f[son[j]]+w[j]);
	 }
	while (S[x].size()&&*(--S[x].end())>=MID) cnt++,S[x].erase(--S[x].end());
	S[x].insert(INF);
	while (S[x].size()>2){
		int a=*S[x].begin(); S[x].erase(S[x].find(a));
		int b=*S[x].lower_bound(MID-a);
		if (b==INF) {f[x]=max(f[x],a);continue;}
		S[x].erase(S[x].find(b));cnt++;
	}
	S[x].erase(INF);
	if (S[x].size()) f[x]=max(f[x],*(--S[x].end()));
}
bool check(){
	cnt=0;dfs(1,1);
	return cnt>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=red(),m=red();
	for (int i=1,x,y,z;i<n;i++) x=red(),y=red(),z=red(),add(x,y,z),add(y,x,z);
	int l=1,r=5e8,ans=-1;
	while (l<=r){
		MID=l+r>>1;
		if (check()) ans=MID,l=MID+1;else r=MID-1;
	}
	printf("%d",ans);
	return 0;
}
