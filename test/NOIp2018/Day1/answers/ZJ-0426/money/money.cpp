#include <cstdio>
#include <algorithm>
using namespace std;const int N=105;
int T,n,a[N],b[N],tot,f[25005],ma;
int main()
{
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	int i,j;scanf("%d",&T);while(T--)
	{
		ma=0;scanf("%d",&n);for(i=1;i<=n;i++)scanf("%d",&a[i]),ma=max(ma,a[i]);sort(a+1,a+1+n);b[1]=a[1];tot=1;
		for(i=2;i<=n;i++)if(a[i]!=a[i-1])b[++tot]=a[i];if(b[1]==1){printf("%d\n",1);continue;}f[0]=1;
		for(i=1;i<=ma;i++){f[i]=0;for(j=1;j<=tot&&b[j]<=i;j++)f[i]+=f[i-b[j]];}
		int re=0;for(i=1;i<=tot;i++)if(f[b[i]]>1)re++;printf("%d\n",tot-re);
	}return 0;
}
