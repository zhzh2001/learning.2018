#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;const int N=50005,inf=1<<29;
int n,m,l[N],a[N],nxt[N<<1],to[N<<1],H[N],tot,W[N<<1],f[N],ans;
void Add(int u,int v,int w)
{
	nxt[++tot]=H[u],to[tot]=v,W[tot]=w,H[u]=tot;
	nxt[++tot]=H[v],to[tot]=u,W[tot]=w,H[v]=tot;
}
void dp(int x,int fa)
{
	for (int i=H[x];i;i=nxt[i])
	if (to[i]!=fa)
	{
		dp(to[i],x);
		f[x]=max(f[x],f[to[i]]+W[i]);ans=max(ans,f[x]);
	}
	for (int i=H[x];i;i=nxt[i])
	for (int j=nxt[i];j;j=nxt[j])
	{
		ans=max(f[to[i]]+f[to[j]]+W[i]+W[j],ans);
	}
}
void work1(){dp(1,0);printf("%d",ans);}
void workt1()
{
	sort(l+1,l+1+m);ans=inf;
	for(int i=n-m+1,j=n-m;i<n;i++,j--)
	{
		if (j>=1) ans=min(ans,l[i]+l[j]);
		else ans=min(ans,l[i]);
	}
	printf("%d\n",ans);
}
void workt2()
{
	int L=1,R=500000000,mid,tot,sum,mi;
	while(L<R)
	{
		mid=L+R>>1,tot=sum=0,mi=inf;
		for(int i=H[1];i;i=nxt[i])
		{
			sum+=W[i];
			if (sum>mid) sum=0,tot++,mi=min(mi,sum);
		}
		if (sum>mid) tot++,mi=min(sum,mi);
		if (tot>=m) ans=max(ans,mi),L=mid+1;
		else R=mid-1;
	}
	printf("%d",ans);
}
int Tott,tmp[N];
vector<int> V[N];int L,R,mid;
void dfs(int x,int fa)
{
	int t=0;V[x].clear();
	for(int i=H[x];i;i=nxt[i])
	if (to[i]!=fa)
	{
		t++;dfs(to[i],x);
		V[x].push_back(f[to[i]]+W[i]);
	}
	sort(V[x].begin(),V[x].end());
	int l,r,mx=0,s=0,tt=0;while (t&&V[x][t-1]>=mid){Tott++;t--;}tmp[0]=0;
	for (int i=0;i<t;i++) tmp[i+1]=V[x][i];
	for (int i=1;i<=t;i++)
	{
		tt=0;
		for (l=1+(i==1),r=t-(i==t);l<r;)
		{	
			if (tmp[l]+tmp[r]>=mid) 
			{
				tt++;r--;
				if (r==i) r--;
			}
			l++;if (l==i) l++;
		}
		if (tt>mx)
		{
			mx=tt;
			s=tmp[i];
		}else if (tt==mx) s=max(s,tmp[i]);
	}
	Tott+=mx,f[x]=s;
}
bool pd()
{
	Tott=0;dfs(1,0);
	return Tott+(f[1]>=mid)>=m;
}
int main()
{
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	int x,y,w;int t1=1,t2=1;
	int i,j;scanf("%d%d",&n,&m);for(i=1;i<n;i++)scanf("%d%d%d",&x,&y,&w),l[i]=w,a[i]=x,Add(x,y,w),t1&=(x==1?1:0),t2&=(y==x+1?1:0);
	if(m==1)work1();
	else if (t1) workt1();
	else if (t2) workt2();
	else
	{
		L=1,R=500000000,mid;int ans=0;
		while (L<=R)
		{
			mid=L+R>>1;
			if (pd()) ans=mid,L=mid+1;
			else R=mid-1;
		}printf("%d",ans);
	}return 0;
}
