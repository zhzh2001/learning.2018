#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int n,a[N],f[N];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(int T=read();T;T--){
		memset(f,0,sizeof f);
		n=read();int ma=0;
		for(int i=1;i<=n;i++){
			a[i]=read();
			ma=max(ma,a[i]);
		}
		sort(a+1,a+n+1);
		int ans=n;f[0]=1;
		for(int i=1;i<=n;i++){
			if(f[a[i]])ans--;
			else{
				for(int j=a[i];j<=ma;j++)if(f[j-a[i]])f[j]=1;
			}
		}
		writeln(ans);
	}
	return 0;
}
