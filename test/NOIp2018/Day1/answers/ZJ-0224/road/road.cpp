#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
const ll N=1e5+10;
ll a[N],n,ans=0;
ll t[4*N];
inline void build(ll l,ll r,ll nod){
	if(l==r){t[nod]=l;return;}
	ll mid=l+r>>1;
	build(l,mid,nod<<1);build(mid+1,r,nod<<1|1);
	if(a[t[nod<<1]]<=a[t[nod<<1|1]])t[nod]=t[nod<<1];
	else t[nod]=t[nod<<1|1];
}
inline ll smax(ll l,ll r,ll i,ll j,ll nod){
	if(l>=i&&r<=j)return t[nod];
	ll mid=l+r>>1;
	if(j<=mid)return smax(l,mid,i,j,nod<<1);
	else if(i>mid)return smax(mid+1,r,i,j,nod<<1|1);
	ll pa=smax(l,mid,i,j,nod<<1),pb=smax(mid+1,r,i,j,nod<<1|1);
	if(a[pa]<=a[pb])return pa;return pb;
}
inline void solve(ll l,ll r,ll v){
	if(l>r)return;
	ll pos=smax(1,n,l,r,1);
	ans+=a[pos]-v;
	solve(l,pos-1,a[pos]);solve(pos+1,r,a[pos]);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(ll i=1;i<=n;i++)a[i]=read();
	build(1,n,1);
	solve(1,n,0);
	writeln(ans);
	return 0;
}
