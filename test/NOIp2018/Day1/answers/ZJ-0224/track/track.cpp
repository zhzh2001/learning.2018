#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int n,m,sum=0;
int nedge=0,p[2*N],nex[2*N],head[2*N],c[2*N];
inline void addedge(int a,int b,int v){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
	c[nedge]=v;
}
int mf[N],ans=0;
inline void mdfs(int x,int fa){
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		mdfs(p[k],x);
		ans=max(ans,mf[x]+mf[p[k]]+c[k]);
		mf[x]=max(mf[x],mf[p[k]]+c[k]);
	}
}
inline void MAIN(){
	mdfs(1,0);writeln(ans);
	exit(0);
}
inline void Main(){
	static int a[N];int cnt=0;
	for(int k=head[1];k;k=nex[k])a[++cnt]=c[k];
	sort(a+1,a+cnt+1);
	int l=0,r=sum;
	while(l<=r){
		int mid=l+r>>1;
		int L=1,R=cnt,p=m;
		while(p&&a[R]>=mid)R--,p--;
		while(p&&L<R){
			while(L<R&&a[L]+a[R]<mid)L++;
			if(a[L]+a[R]>=mid)p--,L++,R--;
		}
		if(!p)ans=mid,l=mid+1;
		else r=mid-1;
	}
	writeln(ans);
	exit(0);
}
inline void MAin(){
	int l=0,r=sum;
	while(l<=r){
		int mid=l+r>>1;
		int ss=0,sss=0,x=1;
		while(x!=n){
			for(int k=head[x];k;k=nex[k])if(p[k]==x+1){
				sss+=c[k];
				if(sss>=mid)sss=0,ss++;
				x=p[k];
			}
		}
		if(ss>=m)ans=mid,l=mid+1;
		else r=mid-1;
	}
	writeln(ans);
	exit(0);
}
int f[N][2],lim;
inline void dfs(int x,int fa){
	multiset<int>s;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		dfs(p[k],x);f[x][0]+=f[p[k]][0];
		if(f[p[k]][1]+c[k]>=lim)f[x][0]++;
		else s.insert(f[p[k]][1]+c[k]);
	}
	multiset<int>::iterator it=s.begin(),ti;
	while(it!=s.end()){
		ti=s.lower_bound(lim-(*it));
		if(ti!=s.end()){
			if(ti==it){
				ti++;
				if(ti==s.end())break;
			}
			f[x][0]++;s.erase(ti);ti=it;it++;s.erase(ti);
		}else it++;
	}
	if(s.size()){
		it=s.end();it--;
		f[x][1]=*it;
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	bool flag1=1,flag2=1;
	for(int i=1;i<n;i++){
		int x=read(),y=read(),v=read();
		sum+=v;
		if(x!=1)flag1=0;
		if(y!=x+1)flag2=0;
		addedge(x,y,v);addedge(y,x,v);
	}
	if(m==1)MAIN();
	if(flag1)Main();
	if(flag2)MAin();
	int l=0,r=sum;
	while(l<=r){
		int mid=l+r>>1;
		memset(f,0,sizeof f);
		lim=mid;dfs(1,0);
		if(f[1][0]>=m)ans=mid,l=mid+1;
		else r=mid-1;
	}
	writeln(ans);
	return 0;
}
