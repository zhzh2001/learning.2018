#include<bits/stdc++.h>
#define inf 1000000007
using namespace std;
struct node{
	int nxt,to,w;
}edge[100100];
int n,m,point[100100],head[100100],cnt,w[100100],c[100100];
int start[100100],end[100100],ans;
int f[1010][1010];
int dfs(int x,int fa){
	point[x]=0;
	for(int i=head[x];i;i=edge[i].nxt){
		int upup=edge[i].to;
		if(upup==fa) continue;
		point[x]=max(point[x],dfs(upup,x)+edge[i].w);
	}
	return point[x];
}
void addedge(int x,int y,int w){
	edge[++cnt].to=y;
	edge[cnt].w=w;
	edge[cnt].nxt=head[x];
	head[x]=cnt;
}
void doit1(){
	int ans1=0,ans2=0,Ans=0;
	ans=0;
	for(int i=1;i<=n;i++){
		ans1=ans2=0;
		for(int j=head[i];j;j=edge[j].nxt){
			int upup=edge[j].to;
			ans=dfs(upup,i)+edge[j].w;
			if(ans>ans1) ans2=ans1,ans1=ans;
			else if(ans>ans2) ans2=ans; 
		}
		Ans=max(Ans,ans1+ans2);
	}
	printf("%d\n",Ans);
}
void doit2(){
	ans=inf;
	sort(w+1,w+n);
	if(m<=n/2){
		for(int i=1;i<=m;i++)
			ans=min(w[n-i]+w[n-2*m+i-1],ans);
		printf("%d\n",ans);
		return;
	}
	int x=n-1-m;
	for(int i=1;i<=x;i++)
		w[2*x-i+1]+=w[i];
	for(int i=x+1;i<=n-1;i++)
		ans=min(ans,w[i]);
	printf("%d\n",ans);
	return;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool a_1=true,a_2=true;
	for(int i=1;i<=n-1;i++){
		int a,b;
		scanf("%d%d%d",&a,&b,&w[i]);
		addedge(a,b,w[i]);
		addedge(b,a,w[i]);
		if(a!=1) a_1=false;
		if(b!=a+1) a_2=false;
	}
	if(m==1){
		doit1();
		return 0;
	}
	if(a_1){
		doit2();
		return 0;
	}
	if(a_2){
		ans=0;
		c[0]=0;
		for(int i=1;i<n;i++)
			c[i]=c[i-1]+w[i];
		memset(f,0,sizeof(f));
		f[1][0]=w[1];
		for(int i=2;i<=n-1;i++)
			for(int j=0;j<=min(m-1,i-1);j++){
				if(j==0){
					f[i][j]=c[i];
					continue;
				}
				int x=0;
				for(int k=1;k<=min(n-1,i-1);k++)
					x=max(x,min(f[k][j-1],c[i]-c[k]));
				f[i][j]=x;
			}
		printf("%d",f[n-1][m-1]);
		return 0;
	}
}
