#include<bits/stdc++.h>
using namespace std;
int t,n,ans,a[200],f[30010],p[30010],Max_a;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d",&n);
		ans=Max_a=0;
		memset(a,0,sizeof(a));
		for(int i=1;i<=n;i++){
			scanf("%d",&a[i]);
			Max_a=max(Max_a,a[i]);			
		}
		sort(a+1,a+n+1);
		memset(f,0,sizeof(f));
		f[0]=1;
		for(int i=1;i<=n;i++){
			if(f[a[i]]) continue;
			memset(p,0,sizeof(p));
			for(int j=Max_a;j>=0;j--)
				if(f[j]) {
					p[j]=1;
					for(int k=j+a[i];k<=Max_a;k+=a[i]){
						if(p[k]) break;
						f[k]=true;
					}
				}
			ans++;
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
