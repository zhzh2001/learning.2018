#include<bits/stdc++.h>
using namespace std;
int n,a,lst;
long long ans;
inline void readl(int &x){
	char c=getchar();
	int f=1;
	x=0;
	while((c>'9' || c<'0')&& c!='-')
		c=getchar();
	if(c=='-')
		f=-1,c=getchar();
	while(c>='0' && c<='9')
		x=x*10+c-48,c=getchar();
	x*=f;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	readl(n);
	for(register int i=1;i<=n;++i){
		readl(a);
		if(a>lst)
			ans+=(a-lst);
		lst=a;
	}
	printf("%lld\n",ans);
	return 0;
}
