#include<bits/stdc++.h>
#include<vector>
#include<queue>
using namespace std;
int n,m,depth[500005],cnt,_hash[500005],a[500005],f[500005],ans,mn,val[500005];
long long tot;
struct node{
	int d,id;
}d[500005];
vector<pair<int,int> >s[500005];
queue<int>q;
inline void readl(int &x){
	char c=getchar();
	int f=1;
	x=0;
	while((c>'9' || c<'0')&& c!='-')
		c=getchar();
	if(c=='-')
		f=-1,c=getchar();
	while(c>='0' && c<='9')
		x=x*10+c-48,c=getchar();
	x*=f;
}
bool com(node a,node b){
	return (a.d<b.d)||(a.d==b.d && a.id<b.id);
}
bool cmp(int a,int b){
	return a>b;
}
inline int lca(int ll,int rr){
	while(depth[ll]<depth[rr])
		rr=f[rr];
	while(depth[ll]>depth[rr])
		ll=f[ll];
	while(ll!=rr)
		ll=f[ll],rr=f[rr];
	return ll;
}
inline void dfss(int place,int sum,int total){
	if(sum>m){
		ans=max(mn,ans);
		return;
	}
	if(place>n)
		return;
	int minn=mn;
	if(total>=tot){
		mn=min(mn,total);
		dfss(place+1,sum+1,val[place]);
		mn=minn;
		return;
	}
	mn=min(total+val[place],mn);
	dfss(place+1,sum,total+val[place]);
	mn=minn;
	mn=min(total,mn);
	dfss(place+1,sum+1,val[place]);
	mn=minn;
}
inline void dfs(int sum){
	if(sum>m){
		ans=max(ans,mn);
		return;
	}
	int minn=mn;
	for(int i=1;i<=cnt;++i){
		if(!_hash[d[i].id])
			for(int j=1;j<i;++j){
				if(!_hash[d[j].id]){
					int kk=lca(d[i].id,d[j].id);
					mn=min(mn,a[d[i].id]-a[kk]+a[d[j].id]-a[kk]);
					_hash[d[i].id]=1;
					_hash[d[j].id]=1;
					dfs(sum+1);
					_hash[d[i].id]=0;
					_hash[d[j].id]=0;
					mn=minn;
				}
			}
	}
}
int main(){ 
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	readl(n);
	readl(m);
	if(m==n-1){
		int ll,rr,w,mn=1e9;
		for(int i=1;i<=n;++i)
			readl(ll),readl(rr),readl(w),mn=min(w,mn);
		printf("%d\n",mn);
		return 0;
	}
	for(register int i=1;i<=n;++i)
		d[i].id=i;
	/*if(n<=1000){
		int ll,rr,w;
		for(register int i=1;i<n;++i){
			readl(ll);
			readl(rr);
			readl(w);
			++d[ll].d;
			++d[rr].d;
			s[ll].push_back(make_pair(rr,w));
			s[rr].push_back(make_pair(ll,w));
		}
		sort(d+1,d+1+n,com);
		for(int i=2;i<=n;++i)
			if(d[i].d!=1){
				cnt=i-1;
				break;
			}
		memset(a,127/3,sizeof(a));
		int root=d[1].id;
		q.push(root);
		_hash[root]=1;
		a[root]=0;
		while(!q.empty()){
			int head=q.front();
			q.pop();
			_hash[head]=0;
			for(register int i=0;i<s[head].size();++i){
				int kk=s[head][i].first;
				w=s[head][i].second;
				if(a[kk]>a[head]+w){
					a[kk]=a[head]+w;
					if(_hash[kk])
						continue;
					q.push(kk);
					_hash[kk]=1;
				}
			}
		}
		depth[root]=1;
		f[root]=0;
		q.push(root);
		while(!q.empty()){
			int head=q.front();
			q.pop();
			for(int i=0;i<s[head].size();++i){
				int kk=s[head][i].first;
				if(!depth[kk]){
					depth[kk]=depth[head]+1;
					f[kk]=head;
					q.push(kk);
				}
			}
		}
		mn=1e9;
		dfs(1);
		printf("%d\n",ans);
		return 0;
	}*/
	int flag1=0,flag2=0,ll,rr;
	for(register int i=1;i<n;++i){
		readl(ll);
		readl(rr);
		if(ll!=1)
			flag1=1;
		if(rr!=ll+1)
			flag2=1;
		readl(val[i]);
		tot+=val[i];
		++d[ll].d;
		++d[rr].d;
		s[ll].push_back(make_pair(rr,val[i]));
		s[rr].push_back(make_pair(ll,val[i]));
	}
	if(flag1==0){
		sort(val+1,val+n,cmp);
		for(int i=1;i<=m;++i)
			a[i]=val[i];
		for(int i=m+1;i<=min(2*m,n);++i)
			a[2*m+1-i]+=val[i];
		ans=1e9;
		for(int i=1;i<=m;++i)
			ans=min(a[i],ans);
		printf("%d\n",ans);
		return 0;
	}
	if(flag2==0){
		tot/=m;
		mn=1e9;
		ans=0;
		dfss(1,1,0);
		printf("%d\n",ans);
		return 0;
	}
	//
	int w;
	sort(d+1,d+1+n,com);
		for(int i=2;i<=n;++i)
			if(d[i].d!=1){
				cnt=i-1;
				break;
			}
		memset(a,127/3,sizeof(a));
		int root=d[1].id;
		q.push(root);
		_hash[root]=1;
		a[root]=0;
		while(!q.empty()){
			int head=q.front();
			q.pop();
			_hash[head]=0;
			for(register int i=0;i<s[head].size();++i){
				int kk=s[head][i].first;
				w=s[head][i].second;
				if(a[kk]>a[head]+w){
					a[kk]=a[head]+w;
					if(_hash[kk])
						continue;
					q.push(kk);
					_hash[kk]=1;
				}
			}
		}
		depth[root]=1;
		f[root]=0;
		q.push(root);
		while(!q.empty()){
			int head=q.front();
			q.pop();
			for(int i=0;i<s[head].size();++i){
				int kk=s[head][i].first;
				if(!depth[kk]){
					depth[kk]=depth[head]+1;
					f[kk]=head;
					q.push(kk);
				}
			}
		}
		mn=1e9;
		dfs(1);
		printf("%d\n",ans);
	return 0;
}
