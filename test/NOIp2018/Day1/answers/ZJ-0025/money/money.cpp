#include<bits/stdc++.h>
#include<queue>
#include<vector>
using namespace std;
int T,n,m,a,ans,_hash[500005];
priority_queue<int,vector<int>,greater<int> >q;
inline void readl(int &x){
	char c=getchar();
	int f=1;
	x=0;
	while((c>'9' || c<'0')&& c!='-')
		c=getchar();
	if(c=='-')
		f=-1,c=getchar();
	while(c>='0' && c<='9')
		x=x*10+c-48,c=getchar();
	x*=f;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	readl(T);
	while(T--){
		memset(_hash,0,sizeof(_hash));
		readl(n);
		m=0;
		ans=0;
		for(register int i=1;i<=n;++i)
			readl(a),q.push(a),m=max(m,a);
		while(!q.empty()){
			int head=q.top();
			q.pop();
			if(_hash[head])
				continue;
			++ans;
			_hash[head]=1;
			for(register int i=1;i<=m;++i)
				if(_hash[i])
					_hash[i+head]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
