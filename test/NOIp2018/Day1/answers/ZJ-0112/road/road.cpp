#include <cstdio>
int n = 0;
long long Max = 0, ans = 0;
void link(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
}
int main(){
	link();
	scanf("%d", &n);
	for(int i = 1; i <= n; ++i){
		long long d;
		scanf("%lld", &d);
		if(d > Max)
			ans += d - Max, Max = d;
		else
			Max = d;
	}
	printf("%lld\n", ans);
	return 0;
}
