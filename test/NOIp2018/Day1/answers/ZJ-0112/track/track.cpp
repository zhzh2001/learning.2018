#include <cstdio>
#include <cstring>
#include <algorithm>
using std :: sort;
//我就喜欢黑白图
int n, m, got, Need;
int first[50010], to[100010], nxt[100010], val[100010], cnt = 0;
inline void addE(int u, int v, int w){
	to[++cnt] = v;
	val[cnt] = w;
	nxt[cnt] = first[u];
	first[u] = cnt;
}
int Maxleft[50010], son[50010], num = 0, taken[50010];

void dfs(int u, int fa){
	for(int p = first[u]; p; p = nxt[p]){
		int v = to[p];
		if(v == fa)
			continue;
		//else
		dfs(v, u);
	}
	//son[]做完就删掉，干净
	num = 0;
	for(int p = first[u]; p; p = nxt[p]){//均摊的复杂度 
		int v = to[p];
		if(v == fa)
			continue;
		Maxleft[v] += val[p];
		son[++num] = Maxleft[v];
	}
	sort(son+1, son+num+1);
	//实际上有多少组？指针移动，O(n)
	int i = 1, j = num, Max = 0;
	while(son[j] >= Need and j >= i)
		++Max, --j;
	while(i < j){
		while(son[i] + son[j] < Need and i < j)
			++i;
		if(i < j)
			++Max, ++i, --j;
	}//搭配 i and j 
	//这是无脑使用版
	//最大的，也就搭配一组嘛
	got += Max;//注意时间，为可能有误的准备  

	//震惊，又是二分！
	int l = 0, r = num;
	while(l < r){
		int mid = (l + r + 1) >> 1, Maxgot = 0;
		taken[mid] = 1;
		i = 1, j = num;
		while(son[j] >= Need and j >= i){
			if(!taken[j])
				++Maxgot;
			--j;
		}
		while(i < j){
			if(taken[j]){
				--j;
				continue;
			}
			if(taken[i]){
				++i;
				continue;//打成了一坨***，自己都看不懂 
			}
			while(son[i] + son[j] < Need and i < j)
				++i;
			if(taken[i])
				++i;
			if(i < j)
				++Maxgot, ++i, --j;
		}
		taken[mid] = 0;
		if(Maxgot == Max)//mid is ok and taken could be larger
			l = mid;
		else
			r = mid-1;
	}//what if all used?
	son[0] = 0;
	Maxleft[u] = son[l];
}

inline bool check(int key){
	//whole!
	got = 0; Need = key;
	memset(Maxleft, 0, sizeof(Maxleft));
	dfs(1, 0);
	if(got >= m)
		return true;
	return false;
}
void link(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
}
int main(){
	link();
	
	memset(first, 0, sizeof(first));
	memset(nxt, 0, sizeof(nxt));
	
	scanf("%d %d", &n, &m);//m strs needed
	int sum = 0;
	for(int i = 1; i <= n-1; ++i){
		int u, v, w;
		scanf("%d %d %d", &u, &v, &w);
		sum += w;
		addE(u, v, w);
		addE(v, u, w);
	}
	//if it's right...
	int l = 1, r = sum;
	while(l < r){
		int mid = (l + r + 1) >> 1;
		if(check(mid))// mid is ok and Maxans may be larger
			l = mid;
		else
			r = mid - 1;//mid is ill and Maxans should be smaller
	}
	printf("%d\n", l);
	return 0;
}
