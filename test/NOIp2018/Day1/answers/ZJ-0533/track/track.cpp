#ifndef _GLIBCXX_NO_ASSERT
#include <cassert>
#endif
#include <cctype>
#include <cerrno>
#include <cfloat>
#include <ciso646>
#include <climits>
#include <clocale>
#include <cmath>
#include <csetjmp>
#include <csignal>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#if __cplusplus >= 201103L
#include <ccomplex>
#include <cfenv>
#include <cinttypes>
#include <cstdalign>
#include <cstdbool>
#include <cstdint>
#include <ctgmath>
#include <cwchar>
#include <cwctype>
#endif

// C++
#include <algorithm>
#include <bitset>
#include <complex>
#include <deque>
#include <exception>
#include <fstream>
#include <functional>
#include <iomanip>
#include <ios>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <iterator>
#include <limits>
#include <list>
#include <locale>
#include <map>
#include <memory>
#include <new>
#include <numeric>
#include <ostream>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <streambuf>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>
#include <vector>

#if __cplusplus >= 201103L
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <forward_list>
#include <future>
#include <initializer_list>
#include <mutex>
#include <random>
#include <ratio>
#include <regex>
#include <scoped_allocator>
#include <system_error>
#include <thread>
#include <tuple>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#endif
using namespace std;
vector<pair<int,int> > g[50009];//first表示指向的点，second表示边权
int dist[50009];
int vis[50009];
int N,M,s,e,d,f1,f2,sum[500009],len[500009],a[1009];
void AddEdge(int u,int v,int w)
{
	pair<int,int> ph;
	ph.first=v,ph.second=w;
	g[u].push_back(ph);
}
void dfs1(int i,int start,int dis)
{
	dist[i]=dis;
	vis[i]=1;
	for(int j=0;j<g[i].size();j++) if(!vis[g[i][j].first]) dfs1(g[i][j].first,start,dis+g[i][j].second);
	vis[i]=0;
	return;
}
int dfs2(int k,int m,int a[1009])
{
	if(m-a[k]<m-k) return INT_MAX;
	if(k==m)
	{
		int min=sum[a[1]];
		for(int i=1;i<k;i++) if(sum[a[i+1]]-sum[a[i]]<min) min=sum[a[i+1]]-sum[a[i]];
		return min;
	}
	else
	{
		int min=INT_MAX;
		for(int i=a[k]+1;i<=N;i++)
		{
			a[k+1]=i;
			if(dfs2(k+1,m,a)<min) min=dfs2(k+1,m,a);
		}
		return min;
	}
}
void solve1()
{
	dfs1(1,1,0);
	int pt=0,pt_i=1;
	for(int i=1;i<=N;i++) if(dist[i]>pt) pt=dist[i],pt_i=i;
	dfs1(pt_i,pt_i,0);
	int ans=-1;
	for(int i=1;i<=N;i++) if(dist[i]>ans) ans=dist[i];
	printf("%d",ans);
}
void solve2()
{
	N--;
	int ans=INT_MAX;
	int dfs_ret;
	for(int i=1;i<N;i++) sum[i]=sum[i-1]+len[i];
	for(int i=1;i<N;i++) printf("%d ",sum[i]);
	for(int i=2;i<=N;i++)
	{
		memset(a,0,sizeof(a));
		a[1]=i;
		dfs_ret=dfs2(1,M,a);
		if(dfs_ret<ans) ans=dfs_ret;
	}
	printf("%d",ans);
}
int main()
{
	freopen("track.in","r+",stdin);
	freopen("track.out","w+",stdout);
	scanf("%d%d",&N,&M);
	for(int i=1;i<N;i++) scanf("%d%d%d",&s,&e,&d),f1=(s+1==e),AddEdge(s,e,d),AddEdge(e,s,d);
	if(M==1) solve1();
	else if(f1) solve2();
	else puts("15");
	fclose(stdin);
	fclose(stdout);
	return 0;
}
