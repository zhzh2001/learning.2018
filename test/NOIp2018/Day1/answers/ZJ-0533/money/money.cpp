#ifndef _GLIBCXX_NO_ASSERT
#include <cassert>
#endif
#include <cctype>
#include <cerrno>
#include <cfloat>
#include <ciso646>
#include <climits>
#include <clocale>
#include <cmath>
#include <csetjmp>
#include <csignal>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#if __cplusplus >= 201103L
#include <ccomplex>
#include <cfenv>
#include <cinttypes>
#include <cstdalign>
#include <cstdbool>
#include <cstdint>
#include <ctgmath>
#include <cwchar>
#include <cwctype>
#endif

// C++
#include <algorithm>
#include <bitset>
#include <complex>
#include <deque>
#include <exception>
#include <fstream>
#include <functional>
#include <iomanip>
#include <ios>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <iterator>
#include <limits>
#include <list>
#include <locale>
#include <map>
#include <memory>
#include <new>
#include <numeric>
#include <ostream>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <streambuf>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>
#include <vector>

#if __cplusplus >= 201103L
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <forward_list>
#include <future>
#include <initializer_list>
#include <mutex>
#include <random>
#include <ratio>
#include <regex>
#include <scoped_allocator>
#include <system_error>
#include <thread>
#include <tuple>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#endif
//$1 orz
using namespace std;
int N,cost[109],Case,cost_tms[109],cost_ans[109],tms=0,ans;
/*int exgcd(int a,int b,int &x,int &y)
{
	if(!b) {x=1,y=0; return a;}
	else {int r=exgcd(b,a%b,y,x); y-=x*(a/b); return r;}
}
int gcd(int a,int b)
{
	return !b?a:gcd(b,a%b);
}*/
bool dfs(int i,int k,int sum)
{
	if(i==k) return sum==cost_tms[k];
	for(int j=0;j<=(cost_tms[k]-sum)/cost_tms[i];j++)
		if(dfs(i+1,k,sum+cost_tms[i]*j)) return true;
	return false;
}
int main()
{
	freopen("money.in","r+",stdin);
	freopen("money.out","w+",stdout);
	scanf("%d",&Case);
	while(Case--)
	{
		scanf("%d",&N);
		memset(cost,0,sizeof(cost));
		memset(cost_tms,0,sizeof(cost_tms));
		tms=0;
		for(int i=1;i<=N;i++) scanf("%d",&cost[i]);
		sort(cost+1,cost+N+1);
		for(int i=1;i<=N;i++)
			for(int j=1;j<i;j++) if(cost[i]&&cost[j]&&(!(cost[i]%cost[j]))) cost[i]=0;
		for(int i=1;i<=N;i++) if(cost[i]) cost_tms[++tms]=cost[i];
		N=tms;
		//for(int i=1;i<=N;i++) printf("%d ",cost_tms[i]); puts("");
		/*for(int i=1;i<=N;i++)
		{
			if(!(~cost_tms[i])) continue;
			for(int j=1;j<i;j++)
			{
				if(!(~cost_tms[j])) continue;
				for(int k=1;k<j;k++)
				{
					if(!(~cost_tms[k])) continue;
					x_x=y_y=0;
					Gcd=gcd(cost_tms[j],cost_tms[k]);
					if(!(cost_tms[i]%Gcd)) continue;
					if(!((cost_tms[i]-Gcd)%(cost_tms[j]*cost_tms[k]/Gcd))) continue;
					exgcd(cost_tms[j],cost_tms[k],x_x,y_y);
					Lcm=(cost_tms[i]-Gcd)/(cost_tms[j]*cost_tms[k]/Gcd);
					x_x+=Lcm*(cost_tms[k]/Gcd),y_y+=Lcm*(cost_tms[j]/Gcd);
					if(x_x>=0&&y_y>=0) cost_tms[i]=-1;
					exgcd(cost_tms[j],cost_tms[k],x_x,y_y);
					printf("i:%d j:%d k:%d x:%d y:%d\n",i,j,k,x_x,y_y);
					if(!(~cost_tms[k])) continue;
					for(int l=1;l<=cost_tms[i]/cost_tms[j];l++) if(!((cost_tms[i]-l*cost_tms[j])%cost_tms[k])) {cost_tms[i]=-1; break;}
				}
			}
		}*/
		for(int i=1;i<=N;i++) cost_ans[i]=cost_tms[i];
		for(int i=3;i<=N;i++)
		{
			if(dfs(1,i,0)) cost_ans[i]=-1;
		}
		ans=0;
		for(int i=1;i<=N;i++) if(~cost_ans[i]) ans++;//,printf("%d ",cost_ans[i]); puts("");
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
