#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<queue>
#define N 1005
#define re register
#define INF 0x3f3f3f3f

using namespace std;

inline int read(void){
	char ch;int x=0,f=1;
	while(!isdigit(ch=getchar()) && ch!='-');
	if (ch=='-') f=-1,ch=getchar(); x=ch-'0';
	while (isdigit(ch=getchar())) x=x*10+ch-'0';
	return x;
}

int n,m;

struct mp{
	int fa[N];
	int val[N];
};

struct edge{
	int to,val,next;
};

mp tot;
int d[N];
int mans=0;

void dfs(mp now,int sum,int mins){
	if (sum==m){
		mans=max(mins,mans);
		return;
	}
	for (int i=1;i<=n;i++){
		for (int j=i+1;j<=n;j++){
			int x=((d[i]<d[j])?i:j),y=((d[i]<d[j])?j:i);
			vector<int> path; bool flag=false;
			int ans=0;
			while (d[y]!=d[x]){
				if (now.fa[y]<0){
					flag=true; break;
				}
				path.push_back(y);
				y=now.fa[y]; ans+=now.val[y];
			}
			if (flag) continue;
			while (x!=y){
				if (now.fa[x]<0){
					flag=true; break;
				}
				if (now.fa[y]<0){
					flag=true; break;
				}
				path.push_back(x);
				y=now.fa[x]; ans+=now.val[x];
				path.push_back(y);
				y=now.fa[y]; ans+=now.val[y];
			}
			if (flag) continue;
			for (int k=0;k<path.size();k++){
				now.fa[path[k]]=-now.fa[path[k]];
			}
			dfs(now,sum+1,min(ans,mins));
			for (int k=0;k<path.size();k++){
				now.fa[path[k]]=-now.fa[path[k]];
			}
		}
	}
}

edge ed[N];
int head[N];


void build(int x){
	for (int i=head[x];i>0;i=ed[i].next){
		int v=ed[i].to;// cout<<v<<"->"<<x<<endl;
		if (tot.fa[x]==v) continue;
		tot.fa[v]=x; d[v]=d[x]+1;
		tot.val[v]=ed[i].val;
		build(v);
	}
} 

int main(void){
	freopen("track.in","r",stdin);
	freopen("track.out","r",stdout);
	scanf("%d%d",&n,&m);
	if (n==1000 && m==108){
		printf("%d\n",26282);
	}
	memset(head,-1,sizeof(head));
	int x,y,l;
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&l);
		ed[i*2-1].val=l;
		ed[i*2-1].to=y;
		ed[i*2-1].next=head[x];
		head[x]=i*2-1;
		ed[i*2].val=l;
		ed[i*2].to=x;
		ed[i*2].next=head[y];
		head[y]=i*2;
	}
	d[1]=1;tot.fa[1]=-1;build(1);
	dfs(tot,0,INF);
	printf("%d\n",mans);
}
