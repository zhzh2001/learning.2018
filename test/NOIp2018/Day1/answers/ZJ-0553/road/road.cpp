#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<cmath>
#define re register
#define N 100005
#define INF 0x3f3f3f3f
#define L(x) (x<<1)
#define R(x) ((x<<1)+1)

using namespace std;

inline int read(void){
	char ch;int x=0,f=1;
	while(!isdigit(ch=getchar()) && ch!='-');
	if (ch=='-') f=-1,ch=getchar(); x=ch-'0';
	while (isdigit(ch=getchar())) x=x*10+ch-'0';
	return x;
}

int n;
int d[N];

struct pos{
	int id,val;
};

pos max(pos x,pos y){
	return ((x.val>y.val)?(x):(y));
}

struct node{
	int l,r;
	pos val;
};

struct Segtree{
	node a[N<<2];
	void updata(int x){
		a[x].val=max(a[L(x)].val,a[R(x)].val);
	}
	void build(int x,int l,int r){
		a[x].l=l; a[x].r=r;
		if (l==r){
			a[x].val=(pos){l,d[l]};
			return;
		}
		int mid=(l+r)>>1;
		build(L(x),l,mid);
		build(R(x),mid+1,r);
		updata(x);
	}
	pos find(int x,int l,int r){
		if (l<=a[x].l && a[x].r<=r){
			return a[x].val;
		}
		pos ans=(pos){0,-INF};
		if (l<=a[L(x)].r) ans=max(ans,find(L(x),l,r));
		if (r>=a[R(x)].l) ans=max(ans,find(R(x),l,r));
		return ans;
	}
} seg;

int f(int l,int r,int val){
	if (l>r) return 0;
	/*int maxn=-INF,id;
	for (int i=l;i<=r;i++){
		if (d[i]>maxn){
			maxn=d[i],id=i;
		}
	}*/
	pos ans=seg.find(1,l,r);
	int id=ans.id,maxn=ans.val;
	//printf("$%d,%d:(%d)\n",l,r,id);
	return (val-maxn)+f(l,id-1,maxn)+f(id+1,r,maxn);
}

int main(void){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) d[i]=-read();
	seg.build(1,1,n);
	printf("%d\n",f(1,n,0));
}
