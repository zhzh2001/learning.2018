#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<cmath>
#define re register
#define N 100005
#define INF 0x3f3f3f3f

using namespace std;

inline int read(void){
	char ch;int x=0,f=1;
	while(!isdigit(ch=getchar()) && ch!='-');
	if (ch=='-') f=-1,ch=getchar(); x=ch-'0';
	while (isdigit(ch=getchar())) x=x*10+ch-'0';
	return x;
}

int n;
int d[N];

int f(int l,int r,int val){
	if (l>r) return 0;
	int maxn=-INF,id;
	for (int i=l;i<=r;i++){
		if (d[i]>maxn){
			maxn=d[i],id=i;
		}
	}
	//printf("$%d,%d:(%d)\n",l,r,id);
	return (val-maxn)+f(l,id-1,maxn)+f(id+1,r,maxn);
}

int main(void){
	n=read();
	for (int i=1;i<=n;i++) d[i]=-read();
	printf("%d",f(1,n,0));
}
