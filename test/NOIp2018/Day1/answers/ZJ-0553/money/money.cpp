#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<cmath>
#define re register
#define N 105
#define MAXA 25005

using namespace std;

inline int read(void){
	char ch;int x=0,f=1;
	while(!isdigit(ch=getchar()) && ch!='-');
	if (ch=='-') f=-1,ch=getchar(); x=ch-'0';
	while (isdigit(ch=getchar())) x=x*10+ch-'0';
	return x;
}

int T,n,A;
int a[N];
bool f[MAXA];

int main(void){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		n=read();
		for (re int i=1;i<=n;i++)
			a[i]=read();
		memset(f,0,sizeof(f)); f[0]=true;
		sort(a+1,a+n+1); A=a[n];
		re int ans=0;
		for (re int i=1;i<=n;i++){
			if (!f[a[i]]) ans++;
			for (re int j=a[i];j<=A;j++){
				f[j]=(f[j] || f[j-a[i]]);
			}
		}
		printf("%d\n",ans);
	}
}
