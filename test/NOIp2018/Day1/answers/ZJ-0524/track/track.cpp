#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=5e4+1000;
struct Edge{
	int v,nxt,c;
}edge[maxn<<1];
int head[maxn],tot;
int n,m,ans=0;
inline void read(int &x){
	x=0;int fl=1;char tmp=getchar();
	while(tmp<'0'||tmp>'9'){if(tmp=='-')fl=-fl;tmp=getchar();}
	while(tmp>='0'&&tmp<='9') x=(x<<1)+(x<<3)+tmp-'0',tmp=getchar();
	x=x*fl;
}
inline void add_edge(int x,int y,int c){
	edge[tot]=(Edge){y,head[x],c},head[x]=tot++;
}
int f[maxn];
void tdp(int u,int fa){
	int res1=0,res2=0;
	for(int i=head[u];i!=-1;i=edge[i].nxt){
		int v=edge[i].v;
		if(v==fa) continue;
		tdp(v,u);
		int tmp=f[v]+edge[i].c;
		f[u]=max(f[u],tmp);
		if(!res1||res1<tmp) res2=res1,res1=tmp;
		else if(res2<tmp) res2=tmp;
	}
	ans=max(ans,res1+res2);
}
bool check1(){
	for(int i=1;i<=n;i++)
		for(int j=head[i];j!=-1;j=edge[j].nxt){
			int v=edge[j].v;
			if(v!=i-1&&v!=i+1) return 0;
		}
	return 1;
}
bool check2(){
	for(int i=2;i<=n;i++){
		for(int j=head[i];j!=-1;j=edge[j].nxt){
			if(edge[j].v==1) continue;
			return 0;
		}
	}
	return 1;
}
int sum[maxn];
inline bool ok1(int x){
	int cnt=0;
	for(int i=1;i<=n;i++){
		int l=i,r=i;
		while(sum[r]-sum[l]<x&&r<=n) r++;
		if(sum[r]-sum[l]>=x) cnt++;
		i=r-1;
	}
	return cnt>=m;
}
void solve1(){
	sum[1]=0;
	for(int i=2;i<=n;i++){
		for(int j=head[i];j!=-1;j=edge[j].nxt){
			if(edge[j].v>i) continue;
			sum[i]=sum[edge[j].v]+edge[j].c;
		}
	}
	int l=0,r=sum[n],mid;
	ans=-1;
	while(l<=r){
		mid=l+r>>1;
		if(ok1(mid)) ans=mid,l=mid+1;
		else r=mid-1;
	}
}
void solve2(){
	n--;
	for(int i=1;i<=n;i++)
		sum[i]=edge[i-1<<1].c;
	sort(sum+1,sum+n+1);
	ans=1<<30;
	if(2*m<n){
		for(int i=1;i<=m;i++)
			ans=min(ans,sum[n-i+1]+sum[n-2*m+i]);
	}
	else if(2*m>=n){
		for(int i=1;i<=n-m;i++)
			ans=min(ans,sum[i]+sum[(n-m)*2-i+1]);
		if(2*m>n)ans=min(ans,sum[(n-m)*2+1]);
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(head,-1,sizeof(head));
	cin>>n>>m;
	for(int i=1;i<n;i++){
		int x,y,c;read(x),read(y),read(c);
		add_edge(x,y,c);
		add_edge(y,x,c);
	}
	if(m==1){
		tdp(1,-1);
		printf("%d\n",ans);
	}
	else if(check1()){
		solve1();
		printf("%d\n",ans);
	}
	else if(check2()){
		solve2();
		printf("%d\n",ans);
	}
	else if(m==n-1){
		ans=1<<30;
		for(int i=0;i<tot;i++)
			ans=min(ans,edge[i].c);
		printf("%d\n",ans);
	}
	return 0;
}
