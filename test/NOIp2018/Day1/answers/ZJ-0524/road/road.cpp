#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=1e5+1000;
long long n;
long long ans=0;
long long a[maxn];
inline void read(long long &x){
	x=0;long long fl=1;char tmp=getchar();
	while(tmp<'0'||tmp>'9'){if(tmp=='-') fl=-fl;tmp=getchar();}
	while(tmp>='0'&&tmp<='9') x=(x<<1)+(x<<3)+tmp-'0',tmp=getchar();
	x=x*fl;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	cin>>n;
	for(int i=1;i<=n;i++)
		read(a[i]);
	for(int i=1;i<=n;i++)
		if(a[i]>a[i-1]) ans=ans+a[i]-a[i-1];
	cout<<ans<<endl;
	return 0;
}
