#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=110;
const int N=25000+1000;
int n;
int a[maxn];
bool f[N];
inline void read(int &x){
	x=0;int fl=1;char tmp=getchar();
	while(tmp<'0'||tmp>'9'){if(tmp=='-')fl=-fl;tmp=getchar();}
	while(tmp>='0'&&tmp<='9') x=(x<<1)+(x<<3)+tmp-'0',tmp=getchar();
	x=x*fl;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;cin>>T;
	while(T--){
		cin>>n;
		for(int i=1;i<=n;i++) read(a[i]);
		sort(a+1,a+n+1);
		memset(f,0,sizeof(f));
		f[0]=1;int ans=0,num;
		for(int i=1;i<=n;i++){
			if(f[a[i]]) continue;
			else ans++,num=a[n]-a[i];
			for(int j=0;j<=num;j++){
				f[j+a[i]]=f[j+a[i]]|f[j];
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
