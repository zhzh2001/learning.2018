#include<cstdio>
#include<algorithm>
using namespace std;
int a[500005],mp[500005];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int n,ans=0;
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
		{
			scanf("%d",a+i);
		}
		mp[0]=1;
		for(int i=1;i<=25000;i++)
		{
			mp[i]=0;
		}
		sort(a+1,a+1+n);
		for(int i=1;i<=n;i++)
		{
			if(!mp[a[i]])
			{
				ans++;
				for(int j=0;j<=25000;j++)
				{
					if(mp[j]) mp[j+a[i]]=1;
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
