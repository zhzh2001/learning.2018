#include<cstdio>
#include<queue>
using namespace std;
struct cd
{
	int x,v;
};
struct ab
{
	int f,t,v;
} t[5000005];
int hh[5000005],h,mp[5000005],tt[5000005];
void make(int x,int y,int z)
{
	t[++h].f=hh[x];
	t[h].t=y;
	t[h].v=z;
	hh[x]=h;
}
cd dfs(int x,int y,int v)
{
	priority_queue<int,vector<int>,greater<int> >q;
	int ans=0,n=0,anss=0;
	for(int i=hh[x];i;i=t[i].f)
	{
		int j=t[i].t;
		if(j==y) continue;
		cd z=dfs(j,x,v);
		ans+=z.v;
		q.push(z.x+t[i].v);
	}
	while(!q.empty())
	{
		tt[++n]=q.top();
		q.pop();
	}
	for(int i=1;i<=n;i++)
	{
		mp[i]=0;
	}
	for(int i=0;i<=n;i++)
	{
		if(mp[i]) continue;
		if(tt[i]>=v)
		{
			ans++;
			continue;
		}
		int flag1=0;
		for(int j=i+1;j<=n;j++)
		{
			if(mp[j]) continue;
			if(tt[i]+tt[j]>=v)
			{
				ans++;
				mp[j]=1;
				flag1=1;
				break;
			}
		}
		if(!flag1)
		{
			anss=tt[i];
		}
	}
	cd z;
	z.v=ans;
	z.x=anss;
	return z;
}
int work(int x,int m)
{
	cd z=dfs(1,0,x);
	if(z.v>=m) return 1;
	return 0;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n,m,x,y,z,flag=0,tot=0;
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		make(x,y,z);
		make(y,x,z);
		tot+=z;
	}
	tot/=m;
	int l=0,r=tot+1;
	while(l<r)
	{
		int mid=(l+r)>>1;
		if(work(mid,m)) l=mid+1;
		else r=mid;
	}
	printf("%d\n",l-1);
	return 0;
}
