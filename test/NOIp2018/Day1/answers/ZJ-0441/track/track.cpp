#include <cstdio>
#include <cstring>
#include <algorithm>
#define N 100006
#define mid (l+r>>1)
using namespace std;
struct A{int ne,to,v,w;}e[N];
int n,m,l,r,x,y,z,cnt,f[N],gs[N],a[N],head[N],bj[N];bool flag[N];
void add(int x,int y,int z)
{
	cnt++;e[cnt].to=y;e[cnt].ne=head[x];e[cnt].v=z;head[x]=cnt;
	cnt++;e[cnt].to=x;e[cnt].ne=head[y];e[cnt].v=z;head[y]=cnt;
}
void check(int x,int fa,int lim)
{
	f[x]=0;gs[x]=0;
	for(int i=head[x];i;i=e[i].ne){
		int son=e[i].to;
		if(e[i].to!=fa)
			check(son,x,lim),gs[x]+=gs[son],e[i].w=f[son]+e[i].v;
	}
	int t=0;
	for(int i=head[x];i;i=e[i].ne)
		if(e[i].to!=fa)a[++t]=e[i].w,flag[t]=1;
	sort(a+1,a+t+1);
	while(a[t]>=lim)t--,gs[x]++;
	for(int i=1,j=t;i<t;i++){
		while(a[i]+a[j]>=lim&&j>i)j--;
		bj[i]=j+1;
	}
	for(int i=1;i<t;i++)if(flag[i]){
		int j=bj[i];
		while(j<=t&&!flag[j])j++;
		if(j<=t)flag[i]=flag[j]=0,gs[x]++;
	}
	for(int i=t;i>=1;i--)if(flag[i]){f[x]=a[i];break;}
	/*if(t>2&&a[t-1]+a[1]>=lim&&t%2==1)t--,f[x]=a[t];
	for(int i=t,j=1;i>j;i--){
		while(a[i]+a[j]<lim&&i>j)j++;
		if(i==j)break;
		else gs[x]++,flag[i]=flag[j]=0,j++;
	}
	if(!f[x])for(int i=t;i>=1;i--)if(flag[i]){f[x]=a[i];break;}
	printf("%d %d %d %d\n",x,f[x],gs[x],lim);*/
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++)
		scanf("%d%d%d",&x,&y,&z),add(x,y,z),r+=z;
	l=1;r/=m;
	while(l<=r){
		check(1,0,mid);
		if(gs[1]>=m)l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",r);
}
