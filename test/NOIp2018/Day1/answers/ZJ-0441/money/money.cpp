#include <cstdio>
#include <cstring>
#include <algorithm>
#define Max(a,b) a>b?a:b
#define mid (l+r>>1)
#define N 25006
using namespace std;
int T,n,ans,ma,a[N],b[N];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);b[0]=1;
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			scanf("%d",&a[i]),ma=a[i]>ma?a[i]:ma;
		for(int i=1;i<=ma;i++)b[i]=0;ans=0;
		sort(a+1,a+n+1);
		for(int i=1;i<=n;i++)
			if(a[i]!=a[i-1]&&!b[a[i]]){
				ans++;
				for(int j=0;j<ma;j++)if(b[j])
					for(int k=1;j+k*a[i]<=ma;k++)b[j+k*a[i]]=1;
			}
		printf("%d\n",ans);
	}
}
