#include <cstdio>
#include <cstring>
#include <algorithm>
#define Max(a,b) a>b?a:b
#define mid (l+r>>1)
#define N 100006
using namespace std;
struct A{int key,L,R,i;}a[N];
int n,ans,s[10*N];
int find(int i,int l,int r,int x)
{
	if(l==r)return s[i];int t=0;
	if(x<=mid)t=find(i<<1,l,mid,x);
	else t=find(i<<1|1,mid+1,r,x);
	return Max(s[i],t);
}
void change(int i,int l,int r,int x,int y,int p)
{
	if(x<=l&&r<=y){s[i]=p;return;}
	if(y<=mid)change(i<<1,l,mid,x,y,p);
	else if(x>mid)change(i<<1|1,mid+1,r,x,y,p);
	else change(i<<1,l,mid,x,y,p),change(i<<1|1,mid+1,r,x,y,p);
}
bool cmp(A x,A y)
{
	return x.key<y.key;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);a[0].key=a[n+1].key=-1;
	for(int i=1;i<=n;i++)scanf("%d",&a[i].key),a[i].L=a[i].R=a[i].i=i;
	for(int i=1;i<=n;i++)
		while(a[a[i].L-1].key>=a[i].key)a[i].L=a[a[i].L-1].L;
	for(int i=n;i>=1;i--)
		while(a[a[i].R+1].key>=a[i].key)a[i].R=a[a[i].R+1].R;
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++){
		int tmp=find(1,1,n,a[i].i);
		if(tmp<a[i].key)
			ans+=a[i].key-tmp;change(1,1,n,a[i].L,a[i].R,a[i].key);
	}
	printf("%d\n",ans);
}
