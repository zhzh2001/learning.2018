#include<cstdio>
using namespace std;
int n,mx,ans,a[110],f[26000];
int read(){
	int x=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9'){
		if(c=='-')f=-1;
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		x=x*10+c-48;
		c=getchar();
	}
	return x*f;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--){
		n=read();
		mx=0;
		for(int i=1;i<=n;i++){
			a[i]=read();
			if(a[i]>mx)mx=a[i];
		}
		for(int i=1;i<=mx;i++)
			f[i]=0;
		f[0]=1;
		for(int i=1;i<=n;i++)
			for(int j=0,sz=mx-a[i];j<=sz;j++)
				f[j+a[i]]+=f[j];
		ans=n;
		for(int i=n;i>0;i--)
			if(f[a[i]]>1){
				ans--;
				for(int j=mx;j>=a[i];j--)
					f[j]-=f[j-a[i]];
			}
		printf("%d\n",ans);
	}
	return 0;
}
