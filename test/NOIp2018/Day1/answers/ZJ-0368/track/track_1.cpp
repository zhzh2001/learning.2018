#include <cstdio>
#include <algorithm>
#include <vector>
#define maxn 50010
const int TANG_Yx = 20040826;
inline int max(int a, int b) {return a > b ? a : b;}
int head[maxn], cnt;
struct Edge {
	int to, nxt, w;
} e[maxn << 1];
inline void add(int a, int b, int c) {
	e[++cnt] = (Edge) {b, head[a], c}; head[a] = cnt;
}

int n, m, sum, ans;
int k, f[maxn];
inline bool debug(int k) {return true;}
int pre[maxn], nxt[maxn];

int dfs(int u, int fa = 0) {
//	printf("u: %d\n", u);
	std::vector<int> V; V.clear();
	f[u] = 0;
	for (int i = head[u]; i; i = e[i].nxt) {
		int v = e[i].to;
		if (v != fa) {
			int tmp = dfs(v, u) + e[i].w;
//			if (debug(k)) printf("%d: %d\n", v, tmp);
			f[u] += f[v];
			V.push_back(tmp);
		}
	}
	std::sort(V.begin(), V.end());
	int sz = V.size();
//	if (debug(k)) {for (int i = 0; i < sz; i++) printf("%d ", V[i]); puts("");}
	while (sz && V[sz - 1] >= k) f[u]++, sz--;
	int l = 0, r = 1, rem = 0;
	if (sz > 0) {
		#define End sz
		#define Begin sz + 1
		for (int i = 0; i < sz; i++) {
			pre[i] = i - 1;
			nxt[i] = i + 1;
		}
		nxt[Begin] = 0;
		pre[0] = Begin;
		nxt[sz - 1] = End;
		pre[End] = sz - 1;
		while (r < End && l < r) {
			while (nxt[r] < End && V[l] + V[r] < k) r = nxt[r];
			while (pre[r] > l && pre[r] != Begin && V[l] + V[pre[r]] >= k) r = pre[r];
			if (V[l] + V[r] >= k) {
				f[u]++;
				nxt[pre[l]] = nxt[l];
				pre[nxt[l]] = pre[l];
				nxt[pre[r]] = nxt[r];
				pre[nxt[r]] = pre[r];
				if (pre[r] != Begin) r = pre[r];
				else r = nxt[r];
			}
//			printf("%d:: %d %d: %d\n", u, l, r, f[u]);
			l = nxt[l];
		}
		if (0 <= pre[End] && pre[End] < sz) rem = V[pre[End]];
		else rem = 0;
//		if (debug(k)) printf("%d %d; u: %d; f[u]: %d\n", l, r, u, f[u]);
		#undef End
		#undef Begin
	}
	return rem;
}
bool check(int mid) {
	k = mid;
	dfs(1);
	return f[1] >= m;
}

namespace Work1 {
	int MAX, ans;
	void dfs(int u, int fa = 0, int dep = 0) {
		if (dep > MAX) {
			MAX = dep;
			ans = u;
		}
		for (int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if (v != fa) {
				dfs(v, u, dep + e[i].w);
			}
		}
	}
	int main() {
		MAX = 0;
		dfs(1);
		int x = ans;
		MAX = 0;
		dfs(x);
		printf("%d\n", MAX);
		return 0;
	}
}

namespace Work2 {
	int pre[maxn];
	void dfs(int u, int fa = 0) {
		for (int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if (v != fa) {
				pre[v] = pre[u] + e[i].w;
				dfs(v, u);
			}
		}
	}
	bool check(int mid) {
		int last = 0, res = 0;
		for (int i = 1; i <= n; i++) {
			if (pre[i] - last >= mid) {
				last = pre[i];
				res++;
			}
		}
		return res >= m;
	}
	int main() {
		dfs(1);
		int l = 1, r = sum / m, ans = 0;
		while (l <= r) {
			int mid = l + r >> 1;
			if (check(mid)) {
				l = mid + 1;
				ans = mid;
			} else r = mid - 1;
		}
		printf("%d\n", ans);
		return 0;
	}
}

bool flag = true;
int main() {
	freopen("track.in", "r", stdin);
//	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1, a, b, c; i < n; i++) {
		scanf("%d%d%d", &a, &b, &c);
		add(a, b, c);
		add(b, a, c);
		if (a - b != 1 && b - a != 1) flag = false;
		sum += c;
	}
	if (m == 1) {
		return Work1::main();
	}
	if (flag) {
		return Work2::main();
	}
	int l = 1, r = sum / m;
	while (l <= r) {
		int mid = l + r >> 1;
//		printf("%d\n", mid);
		if (check(mid)) {
//			puts("succeed");
			l = mid + 1;
			ans = mid;
		} else r = mid - 1;// puts("failed");
	}
	printf("%d\n", ans);
	return 0;
}
