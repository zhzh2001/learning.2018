#include <cstdio>
#define maxn 100010
int n;
int a[maxn], s[maxn];
long long ans;
inline int max(int a, int b) {return a > b ? a : b;}
int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++) {
		scanf("%d", a + i);
		s[i] = a[i] - a[i - 1];
	}
	for (int i = 1; i <= n; i++) {
		ans += max(s[i], 0);
	}
	printf("%lld\n", ans);
	return 0;
}
