#include <cstdio>
#include <cstring>
#include <algorithm>
#define maxn 111
int Tim, n, ans, MAX;
int s[maxn];
bool v[26000];
inline int max(int a, int b) {return a > b ? a : b;}
int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &Tim);
	while (Tim --> 0) {
		scanf("%d", &n); MAX = 0;
		for (register int i = 1; i <= n; i++) scanf("%d", s + i), MAX = max(MAX, s[i]);
		std::sort(s + 1, s + n + 1);
		memset(v, false, sizeof v);
		v[0] = true;
		ans = 0;
		for (register int i = 1; i <= n; i++) if (!v[s[i]]) {
			ans++;
			for (register int j = s[i]; j <= MAX; j++) if (v[j - s[i]]) v[j] = true;
		}
		printf("%d\n", ans);
	}
	return 0;
}
