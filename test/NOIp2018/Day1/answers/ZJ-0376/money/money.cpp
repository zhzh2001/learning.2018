#include<bits/stdc++.h>
#define LL long long
const int N=110;
const int M=25010;
const int Maxn=25000;
using namespace std;

int a[N],can[M],n,T,maxn;

void SOLVE(){
	int res=n;
	can[0]=1;
	sort(a+1,a+n+1);
	maxn=a[n];
	for(int i=1;i<=n;i++){
		if(can[a[i]])res--;
		for(int num=0;num<=maxn;num++){
			if(num+a[i]>maxn)break;
			if(!can[num])continue;
			can[num+a[i]]=1;
		}
	}
	printf("%d\n",res);
}

void INIT(){
	for(int i=0;i<=Maxn;i++)can[i]=0;
	maxn=0;
}

void OvO(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}

int main(){
	OvO();
	scanf("%d",&T);
	while(T--){
		INIT();
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",&a[i]);
		SOLVE();
	}
	return 0;
}
