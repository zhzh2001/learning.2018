#include<bits/stdc++.h>
#define LL long long
const int N=50010;
const int M=100010;
using namespace std;

int edge[M],lst[N],nxt[M],val[M],t=0;
int n,m,flag,flnk,dis[N],b[N];

void ADD(int x,int y,int z){
	edge[++t]=y;nxt[t]=lst[x];lst[x]=t;val[t]=z;
}

void READ(){
	int u,v,w;
	flag=1;flnk=1;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n-1;i++){
		scanf("%d%d%d",&u,&v,&w);
		ADD(u,v,w);ADD(v,u,w);
		b[i]=w;
		if(u!=1&&v!=1)flag=0;
		if((u!=v+1)&&(v!=u+1))flnk=0;
		if(v==u+1)dis[u]=w;
		if(u==v+1)dis[v]=w;
	}
}

void SEARCH(int x,int fa,int &pos,int num,int &mx){
	if(num>mx)mx=num,pos=x;
	for(int r=lst[x];r;r=nxt[r]){
		if(edge[r]==fa)continue;
		SEARCH(edge[r],x,pos,num+val[r],mx);
	}
}

void SOLVEA(){
	int S,T;
	S=T=1;
	int mx=-1;
	SEARCH(1,0,S,0,mx);
	mx=-1;
	SEARCH(S,S,T,0,mx);
	printf("%d\n",mx);
	return;
}

void SOLVEB(){
	int len=n-1,res=-1;
	sort(b+1,b+len+1);
	if(m*2<=len){
		int temp=len-m*2+1;
		res=b[temp]+b[len];
		for(int i=len-1;i>=len-m+1;i--){
			temp++;
			//printf("b[%d]=%d b[%d]=%d\n",temp,b[temp],i,b[i]);
			res=min(res,b[temp]+b[i]);
		}
		printf("%d\n",res);
		return;
	}
	int Two=len-m;
	//printf("Two=%d\n",Two);
	//for(int i=1;i<=len;i++)printf("B[%d]=%d\n",i,b[i]);
	res=b[1]+b[2*Two];
	for(int i=2;i<=Two;i++){
		res=min(res,b[i]+b[Two*2-i+1]);
	}
	for(int i=Two*2+1;i<=len;i++)res=min(res,b[i]);
	printf("%d\n",res);
	return;
}

int CHK(int x){
	int now=0,cnt=0;
	for(int i=1;i<=n-1;i++){
		if(now>=x){
			cnt++;
			now=0;
			if(cnt==m)return 1;
		}
		now+=dis[i];
	}
	if(cnt==m-1&&now>=x)return 1;
	return 0;
}

void SOLVEC(){
	int L=0,R=0,res=0;
	for(int i=1;i<=n-1;i++)R+=dis[i];
	while(L<=R){
		int mid=(L+R)/2;
		if(CHK(mid))res=max(res,mid),L=mid+1;
		  else R=mid-1;
	}
	printf("%d\n",res);
}

void OvO(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
}

int main(){
	OvO();
	READ();
	if(m==1){SOLVEA();return 0;}
	if(flag==1){SOLVEB();return 0;}
	if(flnk==1){SOLVEC();return 0;}
	if(m==n-1){
		int res=b[1];
		for(int i=1;i<=n-1;i++)res=min(res,b[i]);
		printf("%d\n",res);
		return 0;
	}
	int len=n-1;
	sort(b+1,b+len+1);
	int mn=b[1]+b[len];
	for(int i=1;i<=len/2;i++){
		mn=max(mn,b[i]+b[len-i+1]);
	}
	printf("%d\n",mn);
	return 0;
}
