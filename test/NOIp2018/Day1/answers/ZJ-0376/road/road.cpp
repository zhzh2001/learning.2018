#include<bits/stdc++.h>
#define LL long long
const int N=100010;
using namespace std;

int n,a[N];
LL ans=0;

void OvO(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
}

int main(){
	OvO();
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	int las=0,mx=0;
	a[0]=0;
	int flag=0;
	for(int i=1;i<=n;i++){
		if(a[i]>a[i-1]&&!flag){
			ans+=mx-las;
			las=a[i-1];
			mx=las;
			flag=1;
		}
		mx=max(mx,a[i]);
		if(a[i]<a[i-1])flag=0;
	}
	ans+=mx-las;
	printf("%lld\n",ans);
	return 0;
}
