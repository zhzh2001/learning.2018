#include<bits/stdc++.h>
using namespace std;
int n,m,d[50004],maxx,k,p,s,mid,l,r,ss;
vector<int > v[50004],w[50004];
bool t1=1,t2=1;
struct node{
	int x,y,z;
} a[50004];
bool cmp(node a,node b)
{
	return a.z<b.z;
}
void dfs1(int x,int y)
{
	if (d[x]>maxx) maxx=d[x],k=x;
	for (int i=0;i<v[x].size();i++)
	if (v[x][i]!=y) d[v[x][i]]=d[x]+w[x][i],dfs1(v[x][i],x);
}
void dfs2(int x,int y)
{
	if (s>=mid) s=0,p++;
	for (int i=0;i<v[x].size();i++)
	if (v[x][i]!=y) s+=w[x][i],dfs2(v[x][i],x);
}
bool q()
{
	p=s=0;
	dfs2(k,0);
	if (p>=m) return 1;
	else return 0;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		if (x!=1&&y!=1) t1=0;
		if (y-x!=1&&x-y!=1) t2=0;
		v[x].push_back(y),w[x].push_back(z);
		v[y].push_back(x),w[y].push_back(z);
		a[i].x=x,a[i].y=y,a[i].z=z;
		ss+=z;
	}
	if (m==1)
	{
		for (int i=1;i<=n;i++) d[i]=0;
		maxx=0;
		dfs1(1,0);
		for (int i=1;i<=n;i++) d[i]=0;
		maxx=0;
		dfs1(k,0);
		printf("%d\n",maxx);
	}
	else if (t1)
	{
		sort(a+1,a+n,cmp);
		maxx=1e9;
		for (int i=1;i<=m;i++)
		if (n-2*m+i-1>0) maxx=min(maxx,a[n-i].z+a[n-2*m+i-1].z);
		else maxx=min(maxx,a[n-i].z);
		printf("%d\n",maxx);
	}
	else if (t2)
	{
		for (int i=1;i<n;i++)
		if (v[i].size()==1) k=i;
		l=0,r=ss/m+1;
		while (l<r)
		{
			mid=(l+r+1)/2;
			if (q()) l=mid;
			else r=mid-1;
		}
		printf("%d\n",l);
	}
	else printf("%d\n",ss/m-1);
	return 0;
}
