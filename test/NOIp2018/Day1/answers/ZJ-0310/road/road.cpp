#include<cstdio>
#include<iostream>
using namespace std;
#define maxn 100010
#define LL long long

int n,a[maxn];
LL ans;

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	scanf("%d",&n);
	for(int i=1;i<=n;i++) scanf("%d",a+i);
	ans=a[1];
	for(int i=2;i<=n;i++)
		if(a[i]>a[i-1]) ans+=a[i]-a[i-1];
	printf("%lld\n",ans);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
