#include<cstdio>
#include<iostream>
#include<algorithm>
#include<vector>
#include<cstring>
using namespace std;
#define maxn 50010
#define LL long long
#define pii pair<int,int>

int n,m;
vector<pii > G[maxn];
int u,v,w;
int f[maxn],dis[maxn],son[maxn];
LL dp[maxn];
int num,tot;

void dfs(int u){
	int v;
	for(unsigned int i=0;i<G[u].size();i++){
		v=G[u][i].first;
		if(v==f[u]) continue;
		son[u]++;
		dis[v]=G[u][i].second;
		f[v]=u;
		dfs(v);
	}
}

inline bool cmp(const int a,const int b){
	return a>b;
}

void sea(int u){
	int v;dp[u]=dis[u];
	
	int q[son[u]+1],cnt=0;
	for(unsigned int i=0;i<G[u].size();i++){
		v=G[u][i].first;
		if(v==f[u]) continue;
		sea(v);
		q[++cnt]=dp[v];
	}
	if(son[u]==0){
		if(dp[u]>=tot){
			num++;
			dp[u]=0;
		}return;
	}else if(son[u]==1){
		dp[u]+=q[1];
		if(dp[u]>=tot){
			num++;
			dp[u]=0;
		}return;
	}
	sort(q+1,q+son[u]+1,cmp);
	if(q[1]+q[2]<tot){
		dp[u]+=q[1];
		if(dp[u]>=tot){
			num++;
			dp[u]=0;
		}
		return;
	}
	
	int r=son[u];
	while(q[r]+q[1]<tot) r--;
	bool vis[son[u]+1];memset(vis,false,sizeof(vis));
	for(int i=r;i>=1;i--)if(!vis[i]){
		for(int j=i-1;j>=1;j--)
			if(!vis[j]&&q[j]+q[i]>=tot){
				vis[i]=vis[j]=true;
				num++;
				break;
			}
	}
	for(int i=1;i<=son[u];i++)
		if(!vis[i]){
			dp[u]+=q[i];
			if(dp[u]>=tot){
				num++;
				dp[u]=0;
			}return;
		}
	if(dp[u]>=tot){
		num++;
		dp[u]=0;
	}return;
}

inline bool check(int x){
	num=0;tot=x;
	memset(dp,0,sizeof(dp));
	sea(1);
	return num>=m;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&u,&v,&w);
		G[u].push_back(make_pair(v,w));
		G[v].push_back(make_pair(u,w));
	}
	dfs(1);
	
	LL l=1,r=1e9,mid;
	while(l<r){
		mid=(l+r+1)>>1;
		if(check(mid)) l=mid;
		else r=mid-1;
	}
	printf("%lld\n",l);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
