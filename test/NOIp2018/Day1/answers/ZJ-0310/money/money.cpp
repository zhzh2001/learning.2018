#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
#define maxn 25000

int T;
int n,a[110];
bool vis[25010];
int ans;

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	scanf("%d",&T);
	while(T--){
		memset(vis,false,sizeof(vis));
		ans=0;vis[0]=true;
		scanf("%d",&n);
		for(int i=1;i<=n;i++) scanf("%d",a+i);
		sort(a+1,a+n+1);
		for(int i=1;i<=n;i++){
			if(vis[a[i]]) continue;
			ans++;
			for(int j=a[i];j<=maxn;j++)
				if(vis[j-a[i]]) vis[j]=true;
		}
		printf("%d\n",ans);
	}

	
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
