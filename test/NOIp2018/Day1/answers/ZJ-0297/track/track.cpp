#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,tot,head[N],f[N],g[N],flag1,flag2,b[N],c[N],sum;
struct edge{ ll to,nxt,w; }e[N<<1];
void add(ll u,ll v,ll w) { e[++tot]=(edge){v,head[u],w}; head[u]=tot; }
void dfs(ll u,ll last){
	for (ll i=head[u],v=e[i].to;i;i=e[i].nxt,v=e[i].to){
		if (v==last) continue; dfs(v,u);
		if (f[v]+e[i].w>f[u]) g[u]=f[u],f[u]=f[v]+e[i].w;
		else g[u]=max(g[u],f[v]+e[i].w);
	}
}
ll judge(ll x){
	ll l=1,r=n-1,cnt=0;
	while (l<=r){
		if (b[r]>=x) ++cnt,--r;
		else{
			while (b[l]+b[r]<x&&l+1!=r) ++l;
			if (b[l]+b[r]>=x&&l!=r) ++cnt,--r;
			else return 0;
		}
		if (cnt>=m) return 1;
	}
	return 0;
}
ll check(ll x){
	ll now=0,cnt=0;
	rep(i,1,n){
		now+=c[i];
		if (now>=x) ++cnt,now=0;
	}
	return cnt>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(); m=read();
	rep(i,2,n){
		ll u=read(),v=read(),w=read();
		add(u,v,w); add(v,u,w); b[i-1]=w; c[u]=w; sum+=w;
		if (u!=1) flag1=1; if (v!=u+1) flag2=1;
	}
	if (m==1){
		dfs(1,-1); ll ans=0;
		rep(i,1,n) ans=max(ans,f[i]+g[i]);
		printf("%d",ans);
		return 0;
	}else if (!flag1){
		ll l=0,r=sum,ans=0;
		sort(b+1,b+n);
		while (l<=r){
			ll mid=(l+r)>>1;
			if (judge(mid)) ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d",ans);
	}else if (!flag2){
		ll l=0,r=sum,ans=0;
		while (l<=r){
			ll mid=(l+r)>>1;
			if (check(mid)) ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d",ans);
	}else{
		printf("%d",sum/m);
	}
}
/*

7 3
1 2 1
2 3 4
3 4 8
4 5 5
5 6 3
6 7 2

*/
