#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 25005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,ans,a[N],f[N];
void work(){
	n=ans=read(); memset(f,0,sizeof f); f[0]=1;
	rep(i,1,n) a[i]=read(); sort(a+1,a+1+n);
	rep(i,1,n){
		if (f[a[i]]) { --ans; continue; }
		rep(j,0,a[n]-a[i]) f[j+a[i]]|=f[j];
	}
	printf("%d\n",ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	ll T=read(); while (T--) work();
}
/*
2
4
3 19 10 6
5
11 29 13 19 17

*/
