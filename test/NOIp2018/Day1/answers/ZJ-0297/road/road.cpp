#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,a[N],ans=0,q[N],top;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read(); rep(i,1,n) a[i]=read();
	rep(i,1,n){
		if (top&&q[top]>=a[i]){
			ans+=q[top]-a[i];
			while (top&&q[top]>=a[i]) --top;
		}
		q[++top]=a[i];
	}
	if (top) ans+=q[top];
	printf("%lld",ans);
}
