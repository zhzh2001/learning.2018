#include<bits/stdc++.h>
#define x first
#define y second
#define FOR(a,b,c) for(int a=(b),a##_end=(c);a<=a##_end;++a)
#define ROF(a,b,c) for(int a=(b),a##_end=(c);a>=a##_end;--a)
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int INF=0x3f3f3f3f,N=110,M=28000;
template<class T>inline bool chkmin(T &A,const T B){return B<A?A=B,1:0;}
template<class T>inline bool chkmax(T &A,const T B){return A<B?A=B,1:0;}
int n,a[N],dp[M];
void Solve(){
	int ans=0,mx=0;
	memset(dp,0,sizeof(dp));
	dp[0]=1;
	FOR(i,1,n)chkmax(mx,a[i]);
	FOR(i,1,n)if(!dp[a[i]]){
		++ans;
		FOR(j,a[i],mx)if(dp[j-a[i]])
			dp[j]=1;
	}
	printf("%d\n",ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while(cas--){
		scanf("%d",&n);
		FOR(i,1,n)scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		n=unique(a+1,a+1+n)-a-1;
		Solve();
	}
	return 0;
}
