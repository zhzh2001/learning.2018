#include<bits/stdc++.h>
#define x first
#define y second
#define FOR(a,b,c) for(int a=(b),a##_end=(c);a<=a##_end;++a)
#define ROF(a,b,c) for(int a=(b),a##_end=(c);a>=a##_end;--a)
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int INF=0x3f3f3f3f,N=50010;
template<class T>inline bool chkmin(T &A,const T B){return B<A?A=B,1:0;}
template<class T>inline bool chkmax(T &A,const T B){return A<B?A=B,1:0;}
template<const int Maxn,const int Maxm>struct Link_List{
	int tot,Head[Maxn],Next[Maxm],W[Maxm];
	inline int operator [] (int x){return W[x];}
	inline void Add(int x,int y){Next[++tot]=Head[x];W[Head[x]=tot]=y;}
	#define LFOR(a,b,c) for(int a=(b).Head[c];a;a=(b).Next[a])
};Link_List<N,N<<1>E,W;
int n,m,lim;
int dp[N],f[N],a[N];
int calc(int n,int t){
	int p=1,ans=0;
	ROF(i,n,1)if(i!=t){
		while(p==t||(p<i&&a[p]+a[i]<lim))p++;
		if(p<i){
			++ans;
			++p;
		}
	}
	return ans;
}
namespace P1{
	bool check(){return n<=1000;}
	void dfs(int x,int F){
		int y,tot=0;
		LFOR(i,E,x)if((y=E[i])!=F){
			dfs(y,x);
			dp[x]+=dp[y];
		}
		LFOR(i,E,x)if((y=E[i])!=F){
			if(f[y]+W[i]>=lim)dp[x]++;
			else a[++tot]=f[y]+W[i];
		}
		sort(a+1,a+1+tot);
		int tmp=calc(tot,0),res=0;
		FOR(i,1,tot){
			if(calc(tot,i)==tmp)
				res=i;
		}
		dp[x]+=tmp;
		f[x]=a[res];
	}
	void Main(){
		int l=1,r=500000000,res=1;
		while(l<=r){
			lim=(l+r)>>1;
			memset(dp,0,sizeof(int)*(n+1));
			memset(f,0,sizeof(int)*(n+1));
			dfs(1,0);
			if(dp[1]>=m){
				res=lim;
				l=lim+1;
			}else r=lim-1;
		}
		printf("%d\n",res);
	}
}
void dfs(int x,int F){
	int y,tot=0;
	LFOR(i,E,x)if((y=E[i])!=F){
		dfs(y,x);
		dp[x]+=dp[y];
	}
	LFOR(i,E,x)if((y=E[i])!=F){
		if(f[y]+W[i]>=lim)++dp[x];
		else a[++tot]=f[y]+W[i];
	}
	sort(a+1,a+1+tot);
	int tmp=calc(tot,0),l=1,r=tot,res=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if(calc(tot,mid)==tmp){
			l=mid+1;
			res=mid;
		}else r=mid-1;
	}
	dp[x]+=tmp;
	f[x]=a[res];
}
void Solve(){
	int l=1,r=500000000,res=1;
	while(l<=r){
		lim=(l+r)>>1;
		memset(f,0,sizeof(f));
		memset(dp,0,sizeof(dp));
		dfs(1,0);
		if(dp[1]>=m){
			res=lim;
			l=lim+1;
		}else r=lim-1;
	}
	printf("%d\n",res);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int x,y,z;
	scanf("%d%d",&n,&m);
	FOR(i,1,n-1){
		scanf("%d%d%d",&x,&y,&z);
		E.Add(x,y);E.Add(y,x);
		W.Add(x,z);W.Add(y,z);
	}
	Solve();
	return 0;
}
