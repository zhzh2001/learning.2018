#include<bits/stdc++.h>
#define x first
#define y second
#define FOR(a,b,c) for(int a=(b),a##_end=(c);a<=a##_end;++a)
#define ROF(a,b,c) for(int a=(b),a##_end=(c);a>=a##_end;--a)
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int INF=0x3f3f3f3f,N=1e5+10;
template<class T>inline bool chkmin(T &A,const T B){return B<A?A=B,1:0;}
template<class T>inline bool chkmax(T &A,const T B){return A<B?A=B,1:0;}
int n,d[N];
namespace P1{
	bool check(){return n<=10;}
	int Solve(int l,int r){
		if(l>r)return 0;
		int mn=INF,p=0;
		FOR(i,l,r)
			if(chkmin(mn,d[i]))
				p=i;
		FOR(i,l,r)d[i]-=mn;
		return mn+Solve(l,p-1)+Solve(p+1,r);
	}
	void Main(){
		int ans=Solve(1,n);
		printf("%d\n",ans);
	}
}
struct Sparse_Table{
	int Log2[N],st[N][20];
	int cmp(int a,int b){return d[a]<d[b]?a:b;}
	void Build(){
		FOR(i,2,N-10)Log2[i]=Log2[i>>1]+1;
		ROF(i,n,1){
			st[i][0]=i;
			for(int j=1;i+(1<<j)<=n+1;++j)
				st[i][j]=cmp(st[i][j-1],st[i+(1<<(j-1))][j-1]);
		}
	}
	int Query(int l,int r){
		int k=Log2[r-l+1];
		return cmp(st[l][k],st[r-(1<<k)+1][k]);
	}
}ST;
int ans;
void calc(int l,int r,int Mn){
	if(l>r)return;
	int t=ST.Query(l,r);
	ans+=d[t]-Mn;
	calc(l,t-1,d[t]);
	calc(t+1,r,d[t]);
}
void Solve(){
	ST.Build();
	calc(1,n,0);
	printf("%d\n",ans);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	FOR(i,1,n)scanf("%d",&d[i]);
	Solve();
	return 0;
}
