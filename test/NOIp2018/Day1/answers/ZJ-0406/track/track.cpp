#include<bits/stdc++.h>
#define N 50003
using namespace std;
typedef long long LL;

bool h[N];
LL n, m, len, ans, cnt, t[N], fst[N];
struct node{ LL v, nx; }a[N];
struct edge{ LL x, v, nx; }e[N << 1];

LL dfs(LL x, LL fa)
{
	LL mx = 0, tot = 0, f = 0;
	for(LL i = fst[x]; i; i = e[i].nx)
	{
		if(e[i].x == fa) continue;
		LL tmp = dfs(e[i].x, x) + e[i].v;
		a[++ cnt].v = tmp;
		a[cnt].nx = f;
		f = cnt;
		if(a[cnt].v >= len)
		{
			a[cnt].v = 0;
			ans ++;
		}
	}
	for(LL i = f; i; i = a[i].nx)
		t[++ tot] = a[i].v;
	sort(t + 1, t + tot + 1);
	for(int i = 1; i <= tot; i ++) h[i] = 0;
	for(LL i = 1; i <= tot; i ++)
		if(!h[i]) for(LL j = i + 1; j <= tot; j ++)
			if(!h[j] && t[i] + t[j] >= len)
			{
				ans ++;
				h[i] = h[j] = 1;
				break;
			}
	for(LL i = 1; i <= tot; i ++)
		if(!h[i]) mx = max(mx, t[i]);
	return mx;
}

bool judge()
{
	ans = cnt = 0;
	dfs(1, 0);
	return ans >= m;
}

int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%lld%lld", &n, &m);
	for(LL i = 1, x, y, v, tot = 0; i < n; i ++)
	{
		scanf("%lld%lld%lld", &x, &y, &v);
		e[++ tot].x = y, e[tot].v = v, e[tot].nx = fst[x], fst[x] = tot;
		e[++ tot].x = x, e[tot].v = v, e[tot].nx = fst[y], fst[y] = tot;
	}
	LL l = 1, r = 500000000; len = l + r >> 1;
	while(l < r)
	{
		if(judge()) l = len;
		else r = len - 1;
		len = l + r + 1 >> 1;
	}
	printf("%lld", len);
	return 0;
}
