#include<bits/stdc++.h>
#define N 103
#define M 25003
using namespace std;

int T, n, mx, ans, a[N], dp[N];

int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	for(int ii = 1; ii <= T; ii ++)
	{
		memset(dp, 0, sizeof(dp));
		dp[0] = 1;
		mx = ans = 0;
		scanf("%d", &n);
		for(int i = 1; i <= n; i ++)
			scanf("%d", &a[i]), mx = max(mx, a[i]);
		for(int i = 1; i <= n; i ++)
			for(int j = a[i]; j <= mx; j ++)
				dp[j] += dp[j - a[i]];
		for(int i = 1; i <= n; i ++)
			if(dp[a[i]] > 1) ans ++;
		printf("%d\n", n - ans);
	}
	return 0;
}
