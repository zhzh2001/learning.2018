#include<bits/stdc++.h>
#define N 100003
using namespace std;

int n, hd = 1, tl, ans, a[N], q[N], l[N], r[N];

int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for(int i = 1; i <= n; i ++)
	{
		scanf("%d", &a[i]);
		while(hd <= tl && a[i] < a[q[tl]])
		{
			r[q[tl]] = i;
			tl --;
		}
		q[++ tl] = i;
		l[i] = q[tl - 1];
	}
	while(hd <= tl)
		r[q[tl --]] = n + 1;
	for(int i = 1; i <= n; i ++)
		ans += a[i] - max(a[l[i]], a[r[i]]);
	printf("%d", ans);
	return 0;
}
