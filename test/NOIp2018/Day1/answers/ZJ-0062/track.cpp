#include<bits/stdc++.h>
using namespace std;

#define FOR(i,s,t) for(int i=(s),_t=(t); i<=_t; ++i)
#define DOR(i,s,t) for(int i=(s),_t=(t); i>=_t; --i)
#define EOR(i,x) for(int i=Head[x]; ~i; i=Nxt[i])

const int M=(int)5e4+50;

int Head[M],E[M<<1],Val[M<<1],Nxt[M<<1],tol;
void Edge(int x,int y,int v) {
	E[tol]=y;Val[tol]=v;Nxt[tol]=Head[x];Head[x]=tol++;
}
int n,m;
int Len[M];
int deg[M];
namespace P_1{
	bool check(int mid) {
		int t=0,x=0;
		FOR(i,2,n) {
			x+=Len[i];
			if(x>=mid) {
				++t,x=0;
			}
		}
		return t>=m;
	}
	void solve() {
		int L=1,R=0,res=0;
		FOR(i,2,n) R+=Len[i];
		while(L<=R) {
			int mid=(L+R)>>1;
			if(check(mid)) res=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",res);
	}
}
namespace P_2{
	int lim;
	struct node{
		int x,y;
	};
	node f[M];
	int g[M];
	int cnt;
	int sum[M];
	bool del[M];
	void upd(int x) {
		for(; x<=cnt; x+=x&-x) --sum[x];
	}
	int qry(int x) {
		if(x>cnt) return 0;
		int s=0;
		for(--x; x; x^=x&-x) s+=sum[x];
		int t=0;
		DOR(i,18,0) if((t|(1<<i))<=cnt) {
			if(sum[t|(1<<i)]<=s) {
				s-=sum[t|(1<<i)];
				t|=1<<i;
			}
		}
		++t;
		return t>cnt?0:t;
	}
	void dfs(int x,int fr) {
		EOR(i,x) if(E[i]!=fr) dfs(E[i],x);
		f[x]=(node){0,0};
		cnt=0;
		EOR(i,x) if(E[i]!=fr) {
			int y=E[i];
			f[x].x+=f[y].x;
			if(f[y].y+Val[i]>=lim) {
				++f[x].x;
			} else {
				g[++cnt]=f[y].y+Val[i];
			}
		}
		sort(g+1,g+cnt+1);
		int p=cnt+1;
		FOR(i,1,cnt) sum[i]=i&-i;
		FOR(i,1,cnt) del[i]=false;
		FOR(i,1,cnt) {
			while(p>1 && g[i]+g[p-1]>=lim) --p;
			if(!del[i]) {
				int t=qry(max(p,i+1));
				if(t) {
					++f[x].x;
					upd(i);
					upd(t);
					del[i]=del[t]=true; 
				} else {
					f[x].y=g[i];
				}
			}
		}
	}
	int root;
	bool check(int mid) {
		lim=mid;
		dfs(root,0);
		return f[root].x>=m;
	}
	void solve() {
		root=1;
		FOR(i,1,n) {
			if(deg[i]==1) {
				root=i;
			}
		}
		int L=1,R=0,res=0;
		FOR(i,2,n) R+=Len[i];
		while(L<=R) {
			int mid=(L+R)>>1;
			if(check(mid)) res=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",res);
	}
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	memset(Head,-1,sizeof(Head));
	scanf("%d%d",&n,&m);
	bool fff=true;
	FOR(i,2,n) {
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		if(y!=x+1) {
			fff=false;
		}
		Edge(x,y,v);
		Edge(y,x,v);
		deg[x]++;
		deg[y]++;
		Len[y]=v;
	}
	if(fff) 
		P_1::solve();
	else 
		P_2::solve();
	return 0;
}
