#include<bits/stdc++.h>
using namespace std;

#define FOR(i,s,t) for(int i=(s),_t=(t); i<=_t; ++i)
#define DOR(i,s,t) for(int i=(s),_t=(t); i>=_t; --i)

const int M=(int)25050;

int val[M];
bool f[M];
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	int Cas;
	scanf("%d",&Cas);
	FOR(ttt,1,Cas) {
		int n;
		scanf("%d",&n);
		FOR(i,1,n) {
			scanf("%d",&val[i]);
		}
		sort(val+1,val+n+1);
		int res=0,mx=val[n];
		memset(f,0,sizeof(f));
		f[0]=true;
		FOR(i,1,n) if(!f[val[i]]) {
			++res;
			FOR(j,0,mx-val[i]) {
				f[j+val[i]]|=f[j];
			}
		}
		printf("%d\n",res);
	}
	return 0;
}
