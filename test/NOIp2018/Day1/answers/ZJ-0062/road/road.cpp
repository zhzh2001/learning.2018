#include<bits/stdc++.h>
using namespace std;

#define FOR(i,s,t) for(int i=(s),_t=(t); i<=_t; ++i)
#define DOR(i,s,t) for(int i=(s),_t=(t); i>=_t; --i)

int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	int res=0,n,x,p=0;
	scanf("%d",&n);
	FOR(i,1,n) {
		scanf("%d",&x);
		res=res+x-min(x,p);
		p=x;
	}
	printf("%d\n",res);
	return 0;
}
