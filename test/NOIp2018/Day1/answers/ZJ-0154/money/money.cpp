#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define M 20010
#define INF 0x3f3f3f3f
using namespace std;
int n;
int a[M];
bool f[50010];
int read()
{
	int x=0,tmp=1;
	char c=getchar();
	while (c<'0'||c>'9')
	{
		if (c=='-')
			tmp=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=(x<<3)+(x<<1)+c-48;
		c=getchar();
	}
	return x*tmp;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while (T--)
	{
		int maxn=-1,ans=0;
		n=read();
		if (n==1)
		{
			a[1]=read();
			printf("1\n");
			continue;
		}
		for (int i=1; i <= n; i++)
			a[i]=read();
		sort(a+1,a+n+1);
		if (a[1]==1)
		{
			printf("1\n");
			continue;
		}
		memset(f,0,sizeof(f));
		f[0]=1; 
		for (int j=0; j+a[1]<=a[n]; j++)
			{
				if (f[j])
					f[j+a[1]]=1;
			}
		ans++;
		for (int i=2; i <= n; i++)
			if (f[a[i]])
				continue;
			else
			{
				for (int j=0; j+a[i]<=a[n]; j++)
					if (f[j])
						f[j+a[i]]=1;
				ans++;
			}
		printf("%d\n",ans);
	}
	return 0;
}
