#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define M 200010
using namespace std;
long long int ans;
int n;
int a[M];
int read()
{
	int x=0,tmp=1;
	char c=getchar();
	while (c<'0'||c>'9')
	{
		if (c=='-')
			tmp=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=(x<<3)+(x<<1)+c-48;
		c=getchar();
	}
	return x*tmp;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (int i=1; i <= n; i++)
		a[i]=read();
	ans+=a[1];
	for (int i=2; i <= n; i++)
	{
		if (a[i]>a[i-1])
			ans+=a[i]-a[i-1];
	}
	printf("%lld\n",ans);
	return 0;
}
