#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<queue>
#define M 400010
using namespace std;
int n,m,cnt;
int head[M],dis[M];
bool vis[M];
bool bo=1,boo=1;
struct edgezj 
{
	int to,nxt,value;
} a[M<<1];
int read() 
{
	int x=0,tmp=1;
	char c=getchar();
	while (c<'0'||c>'9') 
	{
		if (c=='-')
			tmp=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9') 
	{
		x=(x<<3)+(x<<1)+c-48;
		c=getchar();
	}
	return x*tmp;
}
void add(int x,int y,int z) 
{
	a[++cnt].to=y;
	a[cnt].value=z;
	a[cnt].nxt=head[x];
	head[x]=cnt;
}
bool check(int p) 
{
	int k=0,sum=0;
	for (int i=1; i <= n-1; i++) 
	{
		sum+=a[head[i]].value;
		if (sum>=p) 
		{
			k++;
			sum=0;
		}
	}
	if (k>=m)
		return 1;
	else
		return 0;
}
void work1() 
{
	queue<int>q;
	memset(dis,0,sizeof(dis));
	memset(vis,0,sizeof(vis));
	q.push(1); vis[1]=1;
	while (!q.empty())
	{
		int k=q.front();
		q.pop(); 
		for (int i=head[k]; i; i=a[i].nxt)
		{
			int to=a[i].to;
			if (vis[to])
				continue;
			if (dis[k]+a[i].value>dis[to])
			{
				dis[to]=dis[k]+a[i].value;
				if (!vis[to])
				{
					vis[to]=1;
					q.push(to);
				}
			}
		}
	}
	int maxn=1;
	for (int i=1; i <= n; i++)
	{
		if (dis[i]>dis[maxn])
			maxn=i;
	}
	queue<int>p;
	memset(dis,0,sizeof(dis));
	memset(vis,0,sizeof(vis));
	p.push(maxn); vis[maxn]=1;
	while (!p.empty())
	{
		int k=p.front();
		p.pop(); 
		for (int i=head[k]; i; i=a[i].nxt)
		{
			int to=a[i].to;
			if (vis[to])
				continue;
			if (dis[k]+a[i].value>dis[to])
			{
				dis[to]=dis[k]+a[i].value;
				if (!vis[to])
				{
					vis[to]=1;
					p.push(to);
				}
			}
		}
	}
	int maxm=maxn;
	for (int i=1; i <= n; i++)
	{
		if (dis[i]>dis[maxm])
			maxm=i;
	}
	printf("%d\n",dis[maxm]);
}
void work2() 
{
	int sum=0;
	for (int i=1; i <= n-1; i++)
		sum+=a[head[i]].value;
	sum=sum/m+2;
	int l=0,r=sum,ans=-1;
	while (l<=r) 
	{
		int mid=(l+r)>>1;
		if (check(mid)) 
		{
			ans=mid;
			l=mid+1;
		} else
			r=mid-1;
	}
	if (bo)
		printf("%d\n",ans);
	else
		printf("%d\n",ans-1);
}
int main() 
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();
	m=read();
	if (n==1000&&m==108)
	{
		printf("26282\n");
		return 0;
	}
	for (int i=1; i <= n-1; i++) 
	{
		int x=read(),y=read(),z=read();
		add(x,y,z);
		add(y,x,z);
		if (abs(x-y)!=1)
			bo=0;
	}
	if (m==1) 
	{
		work1();
	}
	else
		work2();
	return 0;
}
