#include<bits/stdc++.h>
using namespace std;
#define M 50005
#define oo 1000000000
#define P 1000000007
bool p1,p2,p3;
int n,m,C[M],deg[M];
struct node{
	int to,v;
	bool operator <(const node &tmp)const{
		return v<tmp.v;
	}
}Q[M];
vector<node>vec[M];
struct bao{//树的直径
	int mx,st; 
	void dfs(int now,int f,int x){
		if(x>mx){
			mx=x;
			st=now;
		}
		for(int i=0;i<vec[now].size();i++){
			node tmp=vec[now][i];
			if(tmp.to==f)continue;
			dfs(tmp.to,now,x+tmp.v);
		}
	}
	void solve(){
		mx=0;
		st=1;
		dfs(1,0,0);
		mx=0;
		dfs(st,0,0);
		printf("%d\n",mx);
	}
}P1;
/*
struct da{//瞎写 
	int A[M];
	void solve(){
		for(int i=0;i<vec[1].size();i++){
			A[i+1]=vec[1][i].v;
		}
		sort(A+1,A+n);
		printf("%d\n",A[n-m]);
	}
}P2;*/
struct qing{//二分+贪心 
	bool check(int a){
		int hav=0,sum=0;
		for(int i=1;i<n;i++){
			sum+=C[i];
			if(sum>=a){
				sum=0;
				hav++;
			}
		}
		if(hav>=m)return 1;
		return 0;
	}
	void solve(){
		//puts("!");
		int L=1,R=(n-1)*10000,res=0;
		while(L<=R){
			int mid=L+R>>1;
			if(check(mid)){
				L=mid+1;
				res=mid;
			}else {
				R=mid-1;
			}
		}
		printf("%d\n",res);
	}
}P3;
struct daqing{//树上+二分+垃圾贪心 
	int mx[M],hav,son[M][4],val[M][4],D[5];
	void dfs(int now,int f,int ned){
		int t=0;
		//printf("now=%d %d\n",now,ned);
		for(int i=0;i<vec[now].size();i++){
			node tmp=vec[now][i];
			if(tmp.to==f)continue;
			dfs(tmp.to,now,ned);
			son[now][++t]=tmp.to;
			val[now][t]=tmp.v;
		}
		//printf("a=%d %d\n",now,son[1]);
		if(t==3){
			D[1]=val[now][1]+mx[son[now][1]];
			D[2]=val[now][2]+mx[son[now][2]];
			D[3]=val[now][3]+mx[son[now][3]];
			sort(D+1,D+4);
			if(D[3]+D[2]>=ned)hav++;
		}
		if(t==2){
			//printf("now=%d\n",now);
			int a=mx[son[now][1]]+val[now][1];
			int b=mx[son[now][2]]+val[now][2];
			//if(now==1){
			//	printf("n%d\n",b);
			//}
			if(a>b)swap(a,b);
			if(a>=ned){
				hav++;
				mx[now]=b;
			}else if(b>=ned){
				hav++;
				mx[now]=a;
			}else if(a+b>=ned){
				hav++;
				mx[now]=0;
			}else {
				mx[now]=max(a,b);
			}
		}
		else if(t==1){
			int a=mx[son[now][1]]+val[now][1];
			if(a>=ned){
				hav++;
				mx[now]=0;
			}else mx[now]=a;
		}else mx[now]=0;
	//	printf("now=%d %d\n",now,mx[now]);
	}
	bool check(int x){
		memset(mx,0,sizeof(mx));
		memset(son,0,sizeof(son));
		hav=0;
		dfs(1,0,x); 
		if(hav>=m)return 1;
		return 0;
	}
	void solve(){
		int L=1,R=(n-1)*10000,res=0;
		while(L<=R){
			int mid=L+R>>1;
			if(check(mid)){
				res=mid;
				L=mid+1;
			}else R=mid-1;
		}
		printf("%d\n",res);
	}
}P4;
struct baodaqing{
	int mx[M],hav;
	int gan(int len,int bi,int ned){
		int L=1,cnt=0;
		//printf("aoe=%d %d\n",Q[1].v,Q[2].v);
		for(int i=len;i>=L&&i>=1;i--){
			if(i==bi)continue;
			if(Q[i].v>=ned){
				cnt++;
				continue;
			}
			while(Q[i].v+Q[L].v<ned||L==bi){
				L++;
				if(L>=i)break;
			}
			if(L<i){
			//	printf("bb=%d %d\n",L,i);
				cnt++;
				L++;
			}
		}
		return cnt;
	}
	void dfs(int now,int f,int ned){
		//printf("a=%d %d %d\n",now,f,ned);
		for(int i=0;i<vec[now].size();i++){
			node tmp=vec[now][i];
			if(tmp.to==f)continue;
			dfs(tmp.to,now,ned);
		}
		int t=0;
		for(int i=0;i<vec[now].size();i++){
			node tmp=vec[now][i];
			if(tmp.to==f)continue;
			Q[++t]=(node){tmp.to,tmp.v+mx[tmp.to]};
		}
		if(t==0)return;
		sort(Q+1,Q+t+1);
		//if(now==1)printf("%d %d\n",Q[1]);
		int cnt=gan(t,0,ned);
		//if(now==1)printf("a=%d %d\n",cnt,ned);
		int l=1,r=t,res=0;
		while(l<=r){
			int mid=l+r>>1;
			if(gan(t,mid,ned)==cnt){
				res=mid;
				l=mid+1;
			}else r=mid-1;
		}
		//if(now==1)printf("res=%d\n",res);
		Q[0].v=0;
		mx[now]=Q[res].v;
		hav+=cnt;
		//if(now==2)printf("h=%d\n",hav);
	}
	bool check(int x){
		hav=0;
		memset(mx,0,sizeof(mx));
		dfs(1,0,x);
		//printf("%d\n",x);
		if(hav>=m)return 1;
		return 0;
	}
	void solve(){
		int L=1,R=(n-1)*10000,res=0;
		
		while(L<=R){
			int mid=L+R>>1;
			if(check(mid)){
				res=mid;
				L=mid+1;
			}else R=mid-1;
		}
		printf("%d\n",res);
		//puts("!");
	}
}P5;
int main(){
	int i,j,a,b,c;
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	p1=1;p2=1;p3=1;
	for(i=1;i<n;i++){
		scanf("%d %d %d",&a,&b,&c);
		deg[a]++;deg[b]++;
		if(deg[a]>3||deg[b]>3)p3=0;
		C[i]=c;
		vec[a].push_back((node){b,c});
		vec[b].push_back((node){a,c});
		if(a!=1)p1=0;
		if(a!=i||b!=i+1)p2=0;
	}
	if(m==1)P1.solve();
	//else if(p1)P2.solve();
	else if(p2)P3.solve();
	//else if(p3)P4.solve();
	else P5.solve();
	return 0;
}
/*
7 1
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7

7 3
1 4 2
1 2 7
1 3 9
1 5 2
1 6 3
1 7 10

5 3
1 2 6
2 3 3
3 4 5
4 5 10

11 3
1 2 3
1 3 4
1 4 6
2 5 7
2 6 13
6 7 7
3 8 2
3 9 9
8 10 8
8 11 3
*/
