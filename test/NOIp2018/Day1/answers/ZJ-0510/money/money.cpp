#include<bits/stdc++.h>
using namespace std;
#define M 25005
#define oo 1000000000
#define P 1000000007
int n,m,A[M],t;
struct bao{
	bool mark[M];
	int ans;
	void solve(){
		memset(mark,0,sizeof(mark));
		mark[0]=1;
		sort(A+1,A+n+1);
		ans=0;
		for(int i=1;i<=n;i++){
			if(mark[A[i]])continue;
			ans++;
			for(int j=A[i];j<=25000;j++){
				if(mark[j-A[i]])mark[j]=1;
			}
		}
		printf("%d\n",ans);
	}
}P1;
int main(){
	int T,i;
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	cin>>T;
	while(T--){
		cin>>n;
		for(i=1;i<=n;i++){
			scanf("%d",&A[i]);
		}
		P1.solve();
	}
	return 0;
}
/*
1
3
1 2 3 
*/
