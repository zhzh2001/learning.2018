#include <bits/stdc++.h>
using namespace std;
#define P(x) putchar(x + 48)
#define fi first
#define se second
#define pb push_back
#define mp make_pair

const int maxn = 5e4 + 5;

void read(int &a) {
	a = 0;
	int f = 1;
	char ch = getchar();
	while(ch > '9' || ch < '0') {
		if(ch == '-')f = -1;
		ch = getchar();
	}
	while(ch <= '9' && ch >= '0') {
		a = (a << 3) + (a << 1) + ch - '0';
		ch = getchar();
	}
	a *= f;
}

void write(int a) {
	if(a < 0)P(-3),write(-a);
	if(a > 9)write(a / 10);
	P(a % 10);
}

int n,m,u,v,w,ans;

pair<int,int>f[maxn];

vector<pair<int,int> >edge[maxn];

void dfs(int u,int fa,int len) {
	const int s = edge[u].size() + 2;
	int tot = 0,l,r,z[s],tmp;
	for(int i = 0; i < edge[u].size(); i++) {
		if(edge[u][i].fi == fa)continue;
		dfs(edge[u][i].fi,u,len);
		f[u].fi += f[edge[u][i].fi].fi;
		if(edge[u][i].se + f[edge[u][i].fi].se >= len)f[u].fi++;
		else z[++tot] = edge[u][i].se + f[edge[u][i].fi].se;
	}
	sort(z + 1,z + tot + 1);
	l = 1,r = tot,tmp = 0;
	while(l < r) {
		if(z[r] + z[l] >= len)f[u].fi++,r--,l++;
		else tmp = z[l++];
	}
	if(l == r)tmp = z[l];
	f[u].se = tmp;
}

int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);
	read(m);
	for(int i = 1; i < n; i++) {
		read(u);
		read(v);
		read(w);
		edge[u].pb(mp(v,w));
		edge[v].pb(mp(u,w));
	}
	int l = 1,r = 1e9;
	while(l <= r) {
		int mid = (l + r) / 2;
		for(int i = 1; i <= n; i++)f[i].fi = f[i].se = 0;
		dfs(1,0,mid);
		if(f[1].fi >= m) {
			ans = mid;
			l = mid + 1;
		} else r = mid  - 1;
	}
	write(ans);
}

/*
裸二分

考虑如何check

无法贪心（maybe?)

考虑树形DP

f[i]表示i的子树内最多有多少条满足条件的路径,i所连接的多条路径中不满足的路劲长度最大是多少

卡卡常...差不多了吧...

9 3
1 2 6
2 3 3
3 4 5
4 5 10
6 2 4
7 2 9
8 4 7
9 4 4

*/
