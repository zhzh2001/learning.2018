#include <bits/stdc++.h>
using namespace std;
#define P(x) putchar(x + 48)

const int maxn = 1e2 + 5;

void read(int &a) {
	a = 0;
	int f = 1;
	char ch = getchar();
	while(ch > '9' || ch < '0') {
		if(ch == '-')f = (int)(-1);
		ch = getchar();
	}
	while(ch <= '9' && ch >= '0') {
		a = (a << 3) + (a << 1) + ch - '0';
		ch = getchar();
	}
	a *= f;
}

void write(int a) {
	if(a < 0)P(-3),write(-a);
	if(a > 9)write(a / 10);
	P(a % 10);
}

int T,n,a[maxn],ans;

bool flag;

int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	while(T--) {
		read(n);
		for(int i = 1; i <= n; i++) read(a[i]);
		ans = n;
		for(int i = 1; i <= n; i++) {
			flag = 0;
			for(int j = 1; j <= n; j++) {
				for(int k = 1; k <= n; k++) {
					if(j == k || j == i || k == i)continue;
					if((a[j] - 1) * (a[k] - 1) <= a[i]) {
						flag = 1;
						break;
					}
				}
				if(flag) {
					ans--;
					break;
				}
			}
		}
		write(ans);
		puts("");
	}
	return 0;
}

/*

个人感觉这次的day1难度排序书1,3,2...

子问题
给定x,求x能否被(n,a)表示出
怎么搞啊...
有点像小凯的疑惑啊...
是不是可以直接搞啊...
试一试...



不大对啊...
写完感觉这题才是最水的...
所以我一开始就直接去钢T2,T3了?
woc... 
*/
