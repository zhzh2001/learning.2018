#include <bits/stdc++.h>
using namespace std;
#define P(x) putchar(x + 48)

const int maxn = 1e5 + 5 ;
const int inf = 1e9 ;

void read(int &a) {
	a = 0;
	int f = 1;
	char ch = getchar();
	while(ch > '9' || ch < '0') {
		if(ch == '-')f = (int)(-1);
		ch = getchar();
	}
	while(ch <= '9' && ch >= '0') {
		a = (a << 3) + (a << 1) + ch - '0';
		ch = getchar();
	}
	a *= f;
}

void write(int a) {
	if(a < 0)P(-3),write(-a);
	if(a > 9)write(a / 10);
	P(a % 10);
}

int n,a[maxn],ans;

void work(int l,int r) {
	if(l > r)return;
	int minn = inf,x = 0;
	for(int i = l;i <= r;i++) if(minn > a[i])minn = a[i],x = i;
	ans += minn;
	for(int i = l;i <= r;i++) a[i] -= minn;
	work(l,x - 1);
	work(x + 1,r);
}

int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for(int i = 1;i <= n;i++) read(a[i]);
	work(1,n);
	write(ans);
	return 0;
} 

/*
6
4 3 2 5 3 5
3 2 1 4 2 4
2 1 0 3 1 3


*/

