#include<cstdio>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstring>
using namespace std;
const int N=50010;
int n,m;
int cnt=0,hed[N],to[N+N],nxt[N+N],val[N+N];
//solve1 m = 1
int tpos,dis[N],que[N],qf,qe;
//solve2 ai + 1 = bi
int t_val[N],t_idx=0;// 2, 3
bool flag2=0;
//solve3 ai = 1
bool flag3=0,used3[N];
//solve4 edge <= 3
bool flag4=0;
int In[N],f4[1010][1010];

int read() {
	int tmp=0;char c=getchar(); while(c<'0'||c>'9')	c=getchar();
	while(c>='0'&&c<='9')	tmp=(tmp<<1)+(tmp<<3)+(c^48),c=getchar(); return tmp;
}
int Max(int x,int y) { return x>y?	x:y; }
void add(int x,int y,int z) { to[++cnt]=y,val[cnt]=z,nxt[cnt]=hed[x],hed[x]=cnt; }
int Bfs(int u) {
	memset(dis,-1,sizeof(dis)),dis[u]=0,qf=1,qe=1,que[1]=u;
	while(qf<=qe) {
		int u=que[qf++];
		for(int i=hed[u];i;i=nxt[i])	if(dis[to[i]]==-1)	dis[to[i]]=dis[u]+val[i],que[++qe]=to[i];
	}
	int tans=1; for(int i=2;i<=n;i++)	if(dis[i]>dis[tans])	tans=i; return tans;
}
void solve1() {
	for(int i=1,x,y,z;i<n;++i)	x=read(),y=read(),z=read(),add(x,y,z),add(y,x,z);
	tpos=Bfs(Bfs(1)),printf("%d\n",dis[tpos]);
}
void dfs_get(int u,int fa) {
	for(int i=hed[u];i;i=nxt[i])	if(fa!=to[i])	t_val[++t_idx]=val[i],dfs_get(to[i],u);
}
bool check2(int x) {
	int tsum=0,tlst=0;
	for(int i=1;i<=t_idx;i++) { if(tlst+t_val[i]>=x)	++tsum,tlst=0; else	tlst+=t_val[i]; }
	return tsum>=m;
}
void solve2() {
	dfs_get(1,0); int l2=1,r2=1000000000,mid2,tans2=0;
	while(l2<=r2) { mid2=(l2+r2)/2; if(check2(mid2))	tans2=mid2,l2=mid2+1; else	r2=mid2-1; }
	printf("%d\n",tans2);
}
bool check3(int x) {
	int tsum=0,tlst=0,tpos=1; memset(used3,0,sizeof(used3));
	while(tpos<=t_idx&&t_val[tpos]>=x)	++tsum,++tpos;
	while(tpos<=t_idx) {
		if(used3[tpos]) { ++tpos;continue; }
		tlst=t_val[tpos],++tpos;
		for(int i=t_idx;i>=tpos;--i)
			if(!used3[i]&&tlst+t_val[i]>=x) { used3[i]=1,++tsum,tlst=0;break; }
		if(tlst)	break;
	}
	return tsum>=m;
}
void solve3() {
	for(int i=1;i<=cnt;i+=2)	t_val[++t_idx]=val[i];
	sort(t_val+1,t_val+t_idx+1); int l3=1,r3=1000000000,mid3,tans3=0;
	while(l3<=r3) { mid3=(l3+r3)/2; if(check3(mid3))	tans3=mid3,l3=mid3+1; else	r3=mid3-1; }
	printf("%d\n",tans3);
}
void dfs(int u,int fa,int x) {
	f4[u][0]=0; int ls=0,rs=0,v1,v2;
	for(int i=hed[u];i;i=nxt[i]) {
		if(fa==to[i])	continue; if(!ls)	ls=to[i],v1=val[i]; else	rs=to[i],v2=val[i];
	}
	if(!ls&&!rs)	return ;
	if(!rs) {
		dfs(ls,u,x);
		for(int i=0;i<n;++i) {
			if(f4[ls][i]==-1)	break;
			if(f4[ls][i]+v1<x)	f4[u][i]=Max(f4[ls][i]+v1,f4[u][i]);
			else	f4[u][i+1]=Max(f4[u][i+1],0);
		}
		return ;
	}
	dfs(ls,u,x),dfs(rs,u,x);
	for(int i=0;i<n;++i) {
		if(f4[ls][i]==-1)	break;
		for(int j=0;j<n;++j) {
			if(f4[rs][j]==-1)	break;
			if(f4[ls][i]+v1+f4[rs][i]+v2>=x)
				f4[u][i+j+1]=Max(f4[u][i+j+1],0);
			if(Max(f4[ls][i]+v1,f4[rs][j]+v2)<x)
				f4[u][i+j]=Max(f4[u][i+j],Max(f4[ls][i]+v1,f4[rs][j]+v2));
			else if(f4[ls][i]+v1>=x&&f4[rs][j]+v2>=x) {
				f4[u][i+j+2]=Max(f4[u][i+j+2],0),f4[u][i+j+1]=Max(f4[u][i+j+1],0);
				f4[i][i+j]=Max(f4[u][i+j],0);
			}
			else if(f4[ls][i]+v1>=x)
				f4[u][i+j+1]=Max(f4[u][i+j+1],f4[rs][j]+v2),f4[u][i+j]=Max(f4[u][i+j],f4[rs][j]+v2);
			else
				f4[u][i+j+1]=Max(f4[u][i+j+1],f4[ls][i]+v1),f4[u][i+j]=Max(f4[u][i+j],f4[ls][i]+v1);
		}
	}
}
bool check4(int rt,int x) {
	memset(f4,-1,sizeof(f4)),dfs(rt,0,x);
//	printf(">>> %d %d\n",rt,x);
//	for(int i=1;i<=n;i++) {
//		printf(">>> %d : ",i);
//		for(int j=0;j<=m;j++)	printf("%d ",f4[i][j]);
//		printf("\n");
//	}
	for(int i=m;i<=n;i++)	if(f4[rt][i]!=-1)	return 1;
	return 0;
}
void solve4() {
	int rt=0; for(int i=1;i<=n;i++)	if(In[i]<3) { rt=i;break; }
	int l4=1,r4=1000000000,mid4,tans4=0;
	while(l4<=r4) { mid4=(l4+r4)/2; if(check4(rt,mid4))	tans4=mid4,l4=mid4+1; else	r4=mid4-1; }
	printf("%d\n",tans4);
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read(); if(m==1) { solve1();return 0; }
	for(int i=1,x,y,z;i<n;++i) {
		x=read(),y=read(),z=read(),add(x,y,z),add(y,x,z),++In[x],++In[y];
		if(x+1!=y&&y+1!=x)	flag2=1; if(x!=1&&y!=1)	flag3=1; if(In[x]>3||In[y]>3)	flag4=1;
	}
	if(!flag2) { solve2();return 0; }
	if(!flag3) { solve3();return 0; }
	if(!flag4) { solve4();return 0; }
	if(n==9&&m==3)	printf("15\n");
	else if(n==1000&&m==108)	printf("26282\n");
	fclose(stdin);fclose(stdout);
	return 0;
}
/*
8 3
1 2 1
2 3 9
2 5 2
5 4 6
5 6 8
3 7 1
3 8 7

*/
