#include<cstdio>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstring>
using namespace std;
int T,n,a[110],ans;
bool num[25010];

int read() {
	int tmp=0;char c=getchar();
	while(c<'0'||c>'9')	c=getchar();
	while(c>='0'&&c<='9')	tmp=(tmp<<1)+(tmp<<3)+(c^48),c=getchar();
	return tmp;
}
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	for(;T;--T) {
		n=read(); for(int i=1;i<=n;++i)	a[i]=read();
		sort(a+1,a+n+1);
		memset(num,0,sizeof(num)),ans=0,num[0]=1;
		for(int i=1;i<=n;++i)
			if(!num[a[i]]) {
				++ans;
				for(int j=0;j<=25000-a[i];++j)
					if(num[j]&&!num[j+a[i]])	num[j+a[i]]=1;
			}
		printf("%d\n",ans);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
