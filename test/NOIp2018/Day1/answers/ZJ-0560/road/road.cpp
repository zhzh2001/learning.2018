#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
const int N=100010;
int n,a[N],ans=0;

int read() {
	int tmp=0;char c=getchar();
	while(c<'0'||c>'9')	c=getchar();
	while(c>='0'&&c<='9')	tmp=(tmp<<1)+(tmp<<3)+(c^48),c=getchar();
	return tmp;
}
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read(); for(int i=1;i<=n;++i)	a[i]=read();
	for(int i=1;i<=n;++i)	if(a[i]>a[i-1])	ans=ans+a[i]-a[i-1];
	printf("%d\n",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}
