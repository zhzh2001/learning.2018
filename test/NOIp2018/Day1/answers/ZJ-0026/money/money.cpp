#include<bits\stdc++.h>
using namespace std;
const int N=103,A=25003;
int a[N],n,t,ans;
char f[A]={1};
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(scanf("%d",&t);t;t--)
	{
		ans=0;
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
		scanf("%d",a+i);
		sort(a+1,a+n+1);
		for(int i=1;i<=a[n];i++)
		f[i]=0;
		for(int k=1;k<=n;k++)
		{
			if(f[a[k]]==1)continue;
			ans++;
			for(int i=0;i<=a[n]-a[k];i++)
			f[i+a[k]]|=f[i];
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
