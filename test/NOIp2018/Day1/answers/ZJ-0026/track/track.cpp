#include<bits\stdc++.h>
using namespace std;
const int N=50003,L=10000;
struct node{
	int to,next,dis;
}a[N*2];
int b[N],n,m,ai=1,d,p,test,g[N],g0,tg[N],z,xiao=INT_MAX,he;
bool e[N];
void dfs(int i,int dis){
	if(dis>d)d=dis,p=i;
	for(int k=b[i];k;k=a[k].next)
	if(!e[k>>1])
	{
		e[k>>1]=1;
		dfs(a[k].to,dis+a[k].dis);
		e[k>>1]=0;
	}
}
void dfs2(int i,int dis,int times){
	if(dis>=d)
	{
		dis=0;times++;
		if(times==m)
		{
			test=1;
			return;
		}
	}
	for(int k=b[i];k;k=a[k].next)
	if(!e[k>>1])
	{
		e[k>>1]=1;
		dfs2(a[k].to,dis+a[k].dis,times);
		e[k>>1]=0;
	}
}
void dfs3(int i,int dis,int times){
	if(dis>=z)
	{
		times++;
		tg[times]=g0;
		if(times==m)
		{
			test=1;
			return;
		}
		else// if(g0>tg[times-1])
		{
			d=0;
			dfs(g[g0-1],0);
			d=0;
			dfs3(p,0,times);
		}
	}
	for(int k=b[i];k;k=a[k].next)
	if(!e[k>>1])
	{
		e[k>>1]=1;
		if(a[a[b[i]].next].next)g[g0++]=i;
		dfs3(a[k].to,dis+a[k].dis,times);
		if(a[a[b[i]].next].next)g0--;
		e[k>>1]=0;
		if(test==1)return;
	}
}
int main(){
	freopen("track.in","r",stdin);
//	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int x,y,i=1;i<n;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		if(abs(x-y)!=1)p=1;
		if(z<xiao)xiao=z;
		he+=z;
		a[++ai]=(node){y,b[x],z};b[x]=ai;
		a[++ai]=(node){x,b[y],z};b[y]=ai;
	}
	if(m==1)
	{
		dfs(1,0);
		d=0;
		dfs(p,0);
		printf("%d",d);
	}
	else if(m==n-1)
	{
		d=INT_MAX;
		for(int i=1;i<n;i++)
		if(a[i<<1].dis<d)d=a[i<<1].dis;
		printf("%d",d);
	}
	else if(p==0)
	{
		int l=xiao,r=he;
		while(l<=r)
		{
			d=l+r>>1;
			test=0;
			dfs2(1,0,0);
			if(test)l=d+1;
			else r=d-1;
		}
		printf("%d",r);
	}
	else
	{
		int l=xiao,r=he;
		while(l<=r)
		{
			z=l+r>>1;
			test=0;
			dfs3(1,0,0);
			if(test)l=z+1;
			else r=z-1;
		}
		printf("%d",r);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
