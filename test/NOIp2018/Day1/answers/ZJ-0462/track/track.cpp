#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
using namespace std;
char ch;
inline void read(int &x)
{
	ch=getchar();x=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
}
int ne[100010],pr[100010],fin[50010],val[100010],nm,n,m;
ll l,r;
ll f[50010];int fn;
struct Node
{
	int tot;ll sum;
};
void add(int x,int y,int d)
{
	ne[++nm]=y;pr[nm]=fin[x];fin[x]=nm;val[nm]=d;
}
Node check(int fa,int x,ll lim)
{
	Node t;int rec,fi=fn+1;
	rec=0;
	for (int i=fin[x];i>0;i=pr[i])
		if (ne[i]!=fa)
		{
			t=check(x,ne[i],lim);
			rec+=t.tot;f[++fn]=t.sum+val[i];
		}
	t.tot=rec;rec=0;t.sum=0;
	if (fn<fi) return t;
	sort(f+fi,f+fn+1);
	while (f[fn]>=lim&&fn>=fi) t.tot++,fn--;
	if (fi>fn) return t;
	if (fn==fi) {t.sum=f[fn];fn--;return t;}
	if (f[fn]+f[fn-1]<lim) {t.sum=f[fn];fn=fi-1;return t;}
	int i,j;
	for (i=fi;i<fn;i++) if ((f[i+1]<<1)>=lim) break;
	j=i+1;
	while (i>=fi&&j<=fn)
	{
		if (f[i]+f[j]>=lim) t.tot++,i--,j++;
		else if (t.sum!=0) t.tot++,t.sum=0,j++;
		else t.sum=f[j],j++;
	}
	if (j<=fn)
	{
		if (t.sum!=0) t.tot++,t.sum=0,j++;
		t.tot+=(fn-j+1)/2;
		if ((fn-j+1)%2==1) t.sum=f[fn];
	}
	if (i>=fi&&t.sum==0) t.sum=f[i];
	fn=fi-1;
	return t;
}
int x,y,d;Node t;ll mid;
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);
	l=0;r=0;nm=0;
	for (int i=1;i<n;i++) {read(x);read(y);read(d);add(x,y,d);add(y,x,d);r+=d;}
	while (l<r)
	{
		mid=((l+r)>>1)+1;fn=0;
		t=check(0,1,mid);
		if (t.tot>=m) l=mid;else r=mid-1;
	}
	printf("%lld\n",l);
	return 0;
}
