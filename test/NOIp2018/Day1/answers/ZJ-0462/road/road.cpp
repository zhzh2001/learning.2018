#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
char ch;
inline void read(int &x)
{
	ch=getchar();x=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
}
struct Node
{
	int mi,tag,ne;
}rmq[100010][20];
int a[100010],n;
void build()
{
	for (int i=1;i<=n;i++)
	{
		rmq[i][0].mi=a[i];
		rmq[i][0].tag=i;
		rmq[i][0].ne=i+1;
	}
	Node t;
	for (int i=1;i<=18;i++)
		for (int j=1;j<=n;j++)
		{
			t=rmq[rmq[j][i-1].ne][i-1];
			if (t.ne>0)
			{
				if (rmq[j][i-1].mi<=t.mi) rmq[j][i]=rmq[j][i-1];
					else rmq[j][i]=t;
				rmq[j][i].ne=t.ne;
			}
		}
}
Node charge(int l,int r)
{
	Node t=rmq[l][0];
	for (int i=19;i>=0;i--)
	{
		if (rmq[l][i].ne>0&&rmq[l][i].ne<=r+1)
		{
			if (rmq[l][i].mi<t.mi) t=rmq[l][i];
			l=rmq[l][i].ne;
		}
	}
	return t;
}
int cal(int x,int y,int no)
{
	if (x>y) return 0;
	Node t=charge(x,y);
	return t.mi-no+cal(x,t.tag-1,t.mi)+cal(t.tag+1,y,t.mi);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for (int i=1;i<=n;i++) read(a[i]);
	build();
	printf("%d\n",cal(1,n,0));
	return 0;
}
