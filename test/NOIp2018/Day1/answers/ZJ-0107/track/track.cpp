#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=50005;
int head[N],nxt[2*N],tail[2*N],e[2*N],cnt;
inline void addto(int x,int y,int z)
{
	nxt[++cnt]=head[x];
	head[x]=cnt;
	tail[cnt]=y;
	e[cnt]=z;
}
int f[N];
int dfs(int k,int fa,int t)
{
	f[k]=0;
	multiset<int>a;
//	a.clear();
	for(register int i=head[k];i;i=nxt[i]){
		int v=tail[i];
		if(v==fa)continue;
		int tmp=dfs(v,k,t)+e[i];
		if(tmp>=t)f[k]++;
		else a.insert(tmp);
		f[k]+=f[v];
	}
	int ret=0;
//	cout<<k<<'!';
//	multiset<int>::iterator it2;
//	for(it2=a.begin();it2!=a.end();it2++)
//		cout<<(*it2)<<' ';
//	cout<<endl;
	while(a.size()>0){
		int now=*a.begin();
		a.erase(a.begin());
		if(now>=t){
			f[k]+=a.size();
			a.clear();
			return ret;
		}
		multiset<int>::iterator it=a.lower_bound(t-now);
		if(it!=a.end()){
			f[k]++;
			a.erase(it);
//			xx=a.size();
		}else ret=now;
	}
//	cout<<f[k]<<endl;
	return ret;
}
int sum,n,m;
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	for(register int i=1;i<n;i++){
		int x,y,z;
		x=read();y=read();z=read();
		addto(x,y,z);
		addto(y,x,z);
		sum+=z;
	}
	int l=1,r=sum/m+1,ans=1;
	while(l<=r){
		int mid=(l+r)/2;
//	for(int mid=l;mid<=r;mid++){
//		cout<<'?'<<mid<<endl;
		dfs(1,1,mid);
//		cout<<mid<<' '<<f[1]<<endl;
		if(f[1]>=m)ans=mid,l=mid+1;
		else r=mid-1;
	}
//	dfs(1,1,26282);
	writeln(ans);
}
/*
9 5
1 2 6
2 3 3
3 4 5
4 5 10
6 2 4
7 2 9
8 4 7
9 4 4

8
*/

