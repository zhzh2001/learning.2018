#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=105;
const int M=25005;
int a[N];
int f[N];
int n;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--){
		memset(f,0,sizeof(f));
		n=read();
		for(int i=1;i<=n;i++)
			a[i]=read();
		sort(a+1,a+n+1);
		int m=a[n];
		int ans=n;
		for(int i=1;i<=n;i++){
			if(f[a[i]]){
				ans--;
				continue;
			}
//			cout<<a[i]<<endl;
			f[a[i]]=1;
			for(int j=a[i]+1;j<=m;j++)
				if(f[j-a[i]])f[j]=1;
//		for(int i=1;i<=m;i++)
//			cout<<f[i]<<' ';
//		cout<<endl;
		}
		writeln(ans);
	}
}
