#include <bits/stdc++.h>
using namespace std;
#define M 50005
#define ll long long
ll S;
int n,m,In[M];
struct edge{
	int h[M],nx[M<<1],to[M<<1],dis[M<<1],tot;
	edge(){memset(h,-1,sizeof(h));}
	void link(int a,int b,int c){
		to[++tot]=b;
		dis[tot]=c;
		nx[tot]=h[a];
		h[a]=tot;
	}
}E;
struct P20{
	ll mx;
	int mxer;
	void dfs(int x,int f,ll di){
		if(di>mx){
			mxer=x;
			mx=di;
		}
		for(int i=E.h[x];~i;i=E.nx[i]){
			int v=E.to[i],d=E.dis[i];
			if(v==f)continue;
			dfs(v,x,di+d);
		}
	}
	void solve(){
		mx=0;
		dfs(1,0,0);
		mx=0;
		dfs(mxer,0,0);
		printf("%lld\n",mx);
	}
}A20;
struct P40{
	ll sum[M];
	int tot,val[M];
	void dfs(int x,int f,int di){
		val[x]=di;
		for(int i=E.h[x];~i;i=E.nx[i]){
			int v=E.to[i],d=E.dis[i];
			if(v==f)continue;
			dfs(v,x,d);
		}
	}
	bool check(ll x){
		int cnt=0,l=1;
		for(int i=2;i<=n;i++)
			if(sum[i]-sum[l]>=x)cnt++,l=i;
		return cnt>=m;
	}
	void solve(){
		dfs(1,0,0);
		for(int i=1;i<=n;i++)sum[i]=sum[i-1]+val[i];
		ll l=0,r=sum[n],ans;
		while(l<=r){
			ll mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%lld\n",ans);
	}
}A40;
struct Px{
	int dp[M];
	ll mx[5][M],key;
	void dfs(int x,int f){
		for(int i=E.h[x];~i;i=E.nx[i]){
			int v=E.to[i],d=E.dis[i];
			if(v==f)continue;
			dfs(v,x);
			dp[x]+=dp[v];
			if(mx[0][v]+d>=key)dp[x]++;
			else if(mx[0][x]+mx[0][v]+d>=key){
				int cnt=0;
				for(int j=1;j<=2;j++)if(mx[j][x]+mx[0][v]+d>=key)cnt++;
				for(int j=cnt;j<2;j++)mx[j][x]=mx[j+1][x];
				mx[2][x]=0;
				dp[x]++;
			}
			else {
				for(int j=0;j<=2;j++){
					if(mx[j][x]<mx[0][v]+d){
						for(int k=2;k>j;k--)mx[k][x]=mx[k-1][x];
						mx[j][x]=mx[0][v]+d;
						break;
					}
				}
			}
		}
	}
	bool check(ll x){
		key=x;
		memset(dp,0,sizeof(dp));
		memset(mx,0,sizeof(mx));
		dfs(1,0);
		if(dp[1]>=m)return true;
		return false;
	}
	void solve(){
		ll l=0,r=S,ans;
		while(l<r){
			ll mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%lld\n",ans);
	}
}Ax;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d %d",&n,&m);
	bool flag=1;
	for(int i=1;i<n;i++){
		int a,b,c;
		scanf("%d %d %d",&a,&b,&c);
		S+=c;
		if(a>b)swap(a,b);
		if(a!=b-1)flag=0;
		E.link(a,b,c);
		E.link(b,a,c);
		In[a]++,In[b]++;
	}
	if(m==1)A20.solve();
	else if(flag)A40.solve();
	else Ax.solve();
	return 0;
}
