#include <bits/stdc++.h>
using namespace std;
#define M 25005
int n,A[105];
struct P100{
	bool mark[M];
	void solve(){
		memset(mark,0,sizeof(mark));
		int ans=0;
		sort(A+1,A+1+n);
		mark[0]=1;
		for(int i=1;i<=n;i++){
			if(!mark[A[i]]){
				ans++;
				for(int j=0;j<=M-5-A[i];j++)
					if(mark[j])mark[j+A[i]]=1;
			}
		}
		printf("%d\n",ans);
	};	
}A100;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",&A[i]);		
		A100.solve();
	}
	return 0;
}
