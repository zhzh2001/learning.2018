#include <bits/stdc++.h>
using namespace std;
#define M 100005
int n,A[M];
struct P70 {
	int ans;
	void solve() {
		A[n+1]=0;
		bool flag=1;
		while(flag) {
			flag=0;
			int l=1,mn=-1;
			for(int i=1; i<=n+1; i++) {
				if(A[i]==0) {
					if(mn!=-1){
						for(int j=l; j<=i-1; j++)A[j]-=mn;
						ans+=mn;
					}
					mn=-1;
					l=i+1;
				} else {
					flag=1;
					if(mn==-1)mn=A[i];
					else mn=min(A[i],mn);
				}
			}
		}
		printf("%d\n",ans);
	}
} A70;
struct P100{
	int L[M],R[M],ans;
	bool mark[M];
	struct node{
		int id,val;
		bool operator < (const node &A)const{
			return val<A.val;
		}
	}H[M];
	void solve(){
		for(int i=1;i<=n;i++){
			H[i].id=i;
			H[i].val=A[i];
		}
		sort(H+1,H+1+n);
		for(int i=1;i<=n;i++){L[i]=i-1;R[i]=i+1;}
		L[0]=0,R[n+1]=n+1;
		mark[0]=mark[n+1]=1;
		int l=0,cnt=1;
		for(int i=1;i<=n;i++){
			int x=H[i].id;
			ans+=(H[i].val-l)*cnt;;
			if(mark[L[x]]&&mark[R[x]])cnt--;
			else if((!mark[L[x]])&&(!mark[R[x]]))cnt++;
			l=H[i].val;
			mark[x]=1;
		}
		printf("%d\n",ans);
	}
}A100;
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1; i<=n; i++)scanf("%d",&A[i]);
	if(n<=1000)A70.solve();
	else A100.solve();
	return 0;
}
