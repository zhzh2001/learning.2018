#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <vector>
#include <queue>
using namespace std;

priority_queue<pair<int, int> > Q;
int n;
int a[100005], dp1[100005], dp2[100005], s[100005];

int dfs1(int i){
	if(i == 1) return dp1[1] = 1;
	if(dp1[i] != -1) return dp1[i];
	int j = i;
	while(j != 1 && a[j - 1] >= a[i]) j = dfs1(j - 1);
	return dp1[i] = j;
}

int dfs2(int i){
	if(i == n) return dp2[n] = n;
	if(dp2[i] != -1) return dp2[i];
	int j = i;
	while(j != n && a[j + 1] >= a[i]) j = dfs2(j + 1);
	return dp2[i] = j;
}

int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	int ans = 0;
	for(int i = 1; i <= n; i++){
		scanf("%d", &a[i]);
		ans += a[i];
		Q.push(make_pair(-a[i], i));
	}
	memset(dp1, -1, sizeof(dp1));
	memset(dp2, -1, sizeof(dp2)); 
	for(int i = 1; i <= n; i++){
		if(dp1[i] == -1) dfs1(i);
		if(dp2[i] == -1) dfs2(i);
	}
	while(!Q.empty()){
		int i = Q.top().second;
		Q.pop();
		s[i] = max(s[dp1[i]], s[dp2[i]]);
		ans -= (dp2[i] - dp1[i]) * (a[i] - s[i]);
		s[i - 1] = a[i];
		s[i + 1] = a[i];
		s[dp1[i]] = a[i];
		s[dp2[i]] = a[i];
	}
	printf("%d\n", ans);
	return 0;
}
