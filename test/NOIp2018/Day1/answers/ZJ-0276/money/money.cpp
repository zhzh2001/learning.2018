#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <vector>
using namespace std;

int n, V;
int a[105], dp[25005];

void CompelitPack(int c){
	for(int i = c; i <= V; i++)
		dp[i] = dp[i] || dp[i - c];
}

int judge(){
	int res = 0;
	for(int i = 1; i <= n; i++)
		if(!dp[a[i]]) res++;
	return res;
}

int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int t;
	scanf("%d", &t);
	while(t--){
		V = 0;
		memset(dp, 0, sizeof(dp));
		dp[0] = 1;
		scanf("%d", &n);
		for(int i = 1; i <= n; i++){
			scanf("%d", &a[i]);
			V = max(V, a[i]);
		}
		int ans = 1 << 30, used = 0;
		sort(a + 1, a + n + 1);
		for(int i = 1; i <= n; i++){
			if(!dp[a[i]]){
				used++;
				CompelitPack(a[i]);
			}
			ans = min(ans, judge() + used);
		}
		printf("%d\n", ans);
	}
	return 0;
}
