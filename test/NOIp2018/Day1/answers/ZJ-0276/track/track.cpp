#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <vector>
using namespace std;

vector<pair<int, int> > ve[50005];
int n, m;
int dis[50005], a[50005];

void addedge(int u, int v, int val){
	ve[u].push_back(make_pair(v, val));
}

void dfs(int u, int fa){
	for(int i = 0; i < ve[u].size(); i++){
		int v = ve[u][i].first, val = ve[u][i].second;
		if(v == fa) continue;
		dfs(v, u);
		dis[u] = max(dis[u], dis[v] + val);
	}
}

bool check(int mid){
	int sum = 0, res = 0;
	for(int i = 1; i <= n; i++){
		sum += a[i];
		if(sum >= mid){
			res++;
			sum = 0;
		}
	}
	return res >= m;
}

int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	int flag = 1, sum = 0;
	for(int i = 1; i < n; i++){
		int x, y, z;
		scanf("%d%d%d", &x, &y, &z);
		if(abs(x - y) != 1) flag = 0;
		sum += z;
		addedge(x, y, z);
		addedge(y, x, z);
	}
	if(m == 1){
		dfs(1, 0);
		int res1 = 0, res2 = 0;
		for(int i = 0; i < ve[1].size(); i++){
			int v = ve[1][i].first, val = ve[1][i].second;
			if(dis[v] + val > res1){
				res2 = res1;
				res1 = dis[v] + val;
			}
			else res2 = max(res2, dis[v] + val);
		}
		printf("%d\n", res1 + res2);
		return 0;
	}
	if(ve[1].size() == n - 1){
		dfs(1, 0);
		int ans = 1 << 30;
		for(int i = 0; i < ve[1].size(); i++){
			int v = ve[1][i].first, val = ve[1][i].second;
			a[i + 1] = dis[v] + val;
		}
		sort(a + 1, a + max(n - 1, 2 * m) + 1);
		for(int i = 1; i <= m; i++){
			int res = a[i] + a[2 * m - i + 1];
			ans = min(ans, res);
		}
		printf("%d\n", ans);
		return 0;
	}
	if(flag){
		dfs(1, 0);
		for(int i = 1; i < n; i++) a[i] = dis[i] - dis[i + 1];
		int l = 1, r = sum, ans;
		while(l <= r){
			int mid = (l + r) >> 1;
			if(check(mid)){
				ans = mid;
				l = mid + 1;
			}
			else r = mid - 1;
		}
		printf("%d\n", ans);
		return 0;
	}
	return 0;
}
