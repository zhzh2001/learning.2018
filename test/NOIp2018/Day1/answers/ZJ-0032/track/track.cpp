#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=50005,maxe=100005;
int n,m,tot,x,ans;
int lnk[maxn],nxt[maxe],to[maxe],w[maxe];
int dis[maxn];/*m==1*/
int b[maxn];/*ai==1||bi=ai+1*/
struct Edge
{
	int x,y,l;
	bool operator <(const Edge b)const{return x<b.x;}
}a[maxn];
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-f;ch=getchar();}
	while(ch>=48&&ch<=57){ret=ret*10+ch-48;ch=getchar();}
	return ret*f;
}
void adde(int x,int y,int z){to[++tot]=y;w[tot]=z;nxt[tot]=lnk[x];lnk[x]=tot;}
void dfs(int x,int f){for(int j=lnk[x];j;j=nxt[j]) if(to[j]!=f) dis[to[j]]=dis[x]+w[j],dfs(to[j],x);}
bool check(int x)
{
	int r=n-1,l=1,cnt=0;
	while(b[r]>=x) r--,cnt++;
	while(l<r)
	{
		while(b[l]+b[r]<x&&l<r) l++;
		if(l<r) cnt++;
		l++;r--;
	}
	return cnt>=m;
}
bool checkline()
{
	for(int i=1;i<n;i++) if(a[i].y!=a[i].x+1) return 0;
	return 1;
}
bool check2(int x)
{
	int now=0,cnt=0;
	for(int i=1;i<n;i++)
	{
		now+=b[i];
		if(now>=x) cnt++,now=0;
	}
	return cnt>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<n;i++) a[i]=(Edge){read(),read(),read()};
	if(m==1)
	{
		for(int i=1;i<n;i++) adde(a[i].x,a[i].y,a[i].l),adde(a[i].y,a[i].x,a[i].l);
		dfs(1,0);
		for(int i=1;i<=n;i++) if(dis[i]>dis[x]) x=i;
		memset(dis,0,sizeof dis);
		dfs(x,0);
		for(int i=1;i<=n;i++) ans=max(ans,dis[i]);
		printf("%d\n",ans);
	}
	else
	{
		sort(a+1,a+n);
		for(int i=1;i<n;i++) b[i]=a[i].l;
		if(a[n-1].x==1)
		{
			sort(b+1,b+n);
			int l=0,r=1e9,mid;
			while(l<=r)
			{
				mid=(l+r)>>1;
				if(check(mid)) l=mid+1;
				else r=mid-1;
			}
			printf("%d\n",r);
		}
		else if(checkline())
		{
			int l=0,r=1e9,mid;
			while(l<=r)
			{
				mid=(l+r)>>1;
				if(check2(mid)) l=mid+1;
				else r=mid-1;
			}
			printf("%d\n",r);
		}
	}
	return 0;
}
