#include<cstdio>
#include<cmath>
#include<cstring>
#include<windows.h>
using namespace std;
const int maxn=100005,maxs=20;
int n,len;
int a[maxn],f[maxn][maxs],pre[maxn],lst[maxn],lg[maxn];
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-f;ch=getchar();}
	while(ch>=48&&ch<=57){ret=ret*10+ch-48;ch=getchar();}
	return ret*f;
}
int get(int l,int r)
{
	len=lg[r-l+1];
	if(a[f[l][len]]<a[f[r-(1<<len)+1][len]]) return f[l][len];
	else return f[r-(1<<len)+1][len];
}
int work(int l,int r,int d)
{
	if(l>r) return 0;
	int x=get(l,r);
	int now=a[x]-d+work(x+1,r,a[x]);
	for(;lst[x]>=l-1;x=lst[x]) now+=work(lst[x]+1,x-1,a[x]);
	now+=work(l,x-1,a[x]);
	return now;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	memset(pre,-1,sizeof pre);
	n=read();
	for(int i=1;i<=n;i++) lg[i]=log2(i);
	for(int i=1;i<=n;i++) a[i]=read(),f[i][0]=i;
	for(int i=1;i<=n;i++) lst[i]=pre[a[i]],pre[a[i]]=i;
	for(int j=1;j<=log2(n);j++)
	{
		for(int i=1;i+(1<<j)-1<=n;i++)
		{
			if(a[f[i][j-1]]<a[f[i+(1<<j-1)][j-1]]) f[i][j]=f[i][j-1];
			else f[i][j]=f[i+(1<<j-1)][j-1];
		}
	}
	printf("%d\n",work(1,n,0));
	return 0;
}

