#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,ans,T;
int f[25005],a[105];
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-f;ch=getchar();}
	while(ch>=48&&ch<=57){ret=ret*10+ch-48;ch=getchar();}
	return ret*f;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--)
	{
		memset(f,0,sizeof f);f[0]=1;
		n=ans=read();
		for(int i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+1+n);
		for(int i=1;i<=n;i++)
		{
			if(f[a[i]]){ans--;continue;}
			for(int j=a[i];j<=a[n];j++) if(f[j-a[i]]) f[j]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
