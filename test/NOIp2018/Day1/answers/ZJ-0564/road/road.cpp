#include<bits/stdc++.h>
using namespace std;

const int maxn=100005;
int a[maxn];

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n;
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%d",&a[i]);
	}
	a[0]=0;
	int ans=0;
	for (int i=1;i<=n;i++){
		ans+=max(0,a[i]-a[i-1]);
	}
	cout<<ans;
	return 0;
}
