#include<bits/stdc++.h>
using namespace std;

const int maxn=50005;

struct Edge{
	int next,to,w;
}edge[maxn*2];

int nedge=0,n,l,num,dis;
int head[maxn];
vector <int> a[maxn];

void addedge(int a,int b,int c){
	edge[nedge].w=c;
	edge[nedge].to=b;
	edge[nedge].next=head[a];
	head[a]=nedge++;
}


int dfs(int v,int len,int fa){
	a[v].clear();
	for (int i=head[v];i!=-1;i=edge[i].next){
		int u=edge[i].to;
		if (u==fa) continue;
		int d=dfs(u,edge[i].w,v);
		if (d>=dis) num++;
		else a[v].push_back(d);
	}
	if (a[v].size()==0) return len;
	sort(a[v].begin(),a[v].end());
	int tmp,maxx=0;
	for (int i=0;i<a[v].size();i++){
		int k=i+1;
		if (a[v][i]==0) continue;
		for (int j=k;j<a[v].size();j++){
			if (a[v][j]==0) continue;
			if (a[v][i]+a[v][j]>=dis){
				num++;
				if (a[v][i]==a[v][i+1]) k=j+1;
				else k=i+1;
				a[v][i]=0;
				a[v][j]=0;
				break;
			}
		}
	}
	for (int i=0;i<a[v].size();i++){
		maxx=max(maxx,a[v][i]);
	}
	return maxx+len;
}

int check(int mid){
	num=0;
	dis=mid;
	int tmp=dfs(1,0,0);
	return num>=l;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(head,-1,sizeof(head));
	int sum=0;
	scanf("%d%d",&n,&l);
	for (int i=1;i<n;i++){
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		addedge(a,b,c);
		addedge(b,a,c);
		sum+=c;
	}
	int ll=0,r=sum/l*2;
	while(ll<r){
		int mid=(ll+r+1)/2;
		if (check(mid)) ll=mid;
		else r=mid-1;
	}
	cout<<ll;
	return 0;
}

/*9 3
1 2 6
2 3 3
3 4 5
4 5 10
6 2 4
7 2 9
8 4 7
9 4 4


9 1
1 2 1
2 4 1
2 5 2
2 6 3
5 8 4
5 9 4
1 3 2
3 7 2

7 1
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7
*/
