#include<bits/stdc++.h>
using namespace std;

const int maxn=25005;

int a[maxn],f[maxn],flag[maxn];

int main(){
	int cas;
	scanf("%d",&cas);
	while(cas--){
		freopen("money.in","r",stdin);
		freopen("money.out","w",stdout);
		memset(flag,0,sizeof(flag));
		int n;
		scanf("%d",&n);
		for (int i=1;i<=n;i++){
			scanf("%d",&a[i]);
		}
		for (int ii=1;ii<=n;ii++){
			memset(f,0,sizeof(f));
			if (flag[ii]) continue;
			int k=a[ii];
			for (int i=1;i<=n;i++){
				if (flag[i]||i==ii) continue;
				for (int j=0;j<=k;j++){
					if (j>=a[i]) f[j]=max(f[j],f[j-a[i]]+a[i]);
				}
			}
			if (f[k]==k) flag[ii]=1;
		}
		int ans=0;
		for (int i=1;i<=n;i++){
			if (!flag[i]) ans++;
		}
		cout<<ans<<endl;
	}
	return 0;
}
