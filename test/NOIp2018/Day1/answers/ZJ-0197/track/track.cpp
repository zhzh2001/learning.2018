#include<bits/stdc++.h>
#define M 50005
using namespace std;
int n,m,s,sz,V[M],dis[M];
struct hzl {
	int a,b;
};
vector<hzl>E[M];
vector<int>son[M];
void dfs(int x,int f) {
	bool flag=false;
	for(int i=0; i<E[x].size(); i++) {
		int v=E[x][i].a;
		if(v==f)continue;
		flag=true;
		dfs(v,x);
		dis[v]+=E[x][i].b;
		if(dis[v]>=sz)s++;
		else son[x].push_back(dis[v]);
	}
	if(!flag){
		dis[x]=0;
		return ;
	} 
	int st=0;
	for(int i=0;i<son[x].size();i++)if(son[x][i]>=0)V[++st]=son[x][i];
	sort(V+1,V+st+1);
	int l=1,r=st,mx=0,cnt=0;
	while(l<=r) {
		if(V[l]+V[r]>=sz&&l!=r) {
			l++;
			r--;
			cnt++;
		} else {
			mx=max(mx,V[l]);
			l++;
		}
	}
	r=st-cnt;
	l=r-1;
	int mxn=0,cnt2=0;
	while(r<=st&&l>0) {
		if(V[r]+V[l]>=sz) {
			r++;
			l--;
			cnt2++;
		} else {
			mxn=max(mxn,V[r]);
			r++;
		}
	}
	if(r==st&&l==0)mxn=max(mxn,V[r]);
	if(cnt==cnt2)mx=max(mx,mxn);
	s+=cnt;
	dis[x]=mx;
}
bool check(int a) {
	s=0;
	sz=a;
	for(int i=1; i<=n; i++)son[i].clear();
	dfs(1,0);
	if(s>=m)return true;
	return false;
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1; i<n; i++) {
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		E[a].push_back((hzl) {
			b,c
		});
		E[b].push_back((hzl) {
			a,c
		});
	}
	int l=1,r=1e9,ans=0;
	while(l<=r) {
		int mid=(l+r)>>1;
		if(check(mid)) {
			ans=mid;
			l=mid+1;
		} else r=mid-1;
	}
	printf("%d",ans);
	return 0;
}
