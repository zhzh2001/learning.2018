#include<bits/stdc++.h>
using namespace std;
int now,A[105],dp[25005];
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--) {
		now++;
		int n,mx=0,ans=0;
		scanf("%d",&n);
		for(int i=1; i<=n; i++) {
			scanf("%d",&A[i]);
			mx=max(A[i],mx);
		}
		sort(A+1,A+n+1);
		for(int i=1;i<=n;i++){
			if(dp[A[i]]!=now){
				dp[A[i]]=now;
				ans++;
				for(int j=1;j+A[i]<=mx;j++)if(dp[j]==now)dp[j+A[i]]=now;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
