#include<algorithm>
#include<iostream>
#include<cstdio>
#include<cstring>
#include<vector>
#define int64 long long
using namespace std;
const int maxn=50010;
struct node
{
	int y,z;
};
vector<node> a[maxn];
int n,m,i,j,x,y,z,state,tot;
int64 l,r,ans,d[maxn],sum;
void add(int x,int y,int z)
{
	a[x].push_back((node){y,z});
	a[y].push_back((node){x,z});
}
void dp(int x,int v)
{
	int i,y;
	int64 z;
	for(i=0;i<a[x].size();i++)
	{
		y=a[x][i].y;
		z=a[x][i].z;
		if (y==v)
			continue;
		dp(y,x);
		ans=max(ans,d[x]+d[y]+z);
		d[x]=max(d[x],d[y]+z);
	}
}

int check(int mid)
{
	int i,j,v[maxn],t=0;
	memset(v,0,sizeof(v));
	for(i=n;i>=1;i--)
	{
		if (d[i]>=mid)
		{
			v[i]=1;
			t++;
			continue;
		}
		
		v[i]=1;
		j=lower_bound(d+1,d+i,mid-d[i])-d;
		while(j<=i-1&&v[j])
			j++;
		if (j==i)
			return t;
		v[j]=1;
		t++;
	}
	return t;
}
void dfs1(int x,int v)
{
	int i;
	for(i=0;i<a[x].size();i++)
	{
		int y=a[x][i].y;
		if (y==v)
			continue;
		d[++tot]=a[x][i].z;
		dfs1(y,x);
	}
}
int check2(int64 mid)
{
	int i,t=0;
	int64 s=0;
	for(i=1;i<=n;i++)
		if (s+(int64)d[i]>=mid)
		{
			t++;
			s=0;
		}
		else
			s+=(int64)d[i];
	return t;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	state=1;
	for(i=1;i<=n-1;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		if (y-x!=1)
			state=0;
		add(x,y,z);
		sum+=(int64)z;
	}
	if (m==1)
	{
		ans=0;
		dp(1,0);
		cout<<ans;
		fclose(stdin);fclose(stdout);
		return 0;
	}
	if (a[1].size()==n-1)
	{
		for(i=0;i<a[1].size();i++)
			d[i+1]=a[1][i].z;
		n--;
		sort(d+1,d+n+1);
		l=1;r=d[n]+d[n-1];
		while(l<r)
		{
			int mid=(l+r)>>1;
			if (check(mid)>=m)
				l=mid+1;
			else
				r=mid;
		}
		l--;
		cout<<l;
		fclose(stdin);fclose(stdout);
		return 0;
	}
	if (state)
	{
		tot=0;
		dfs1(1,0);
		n--;
		l=1,r=0;
		for(i=1;i<=n;i++)
			r+=(int64)d[i];
		while(l<r)
		{
			int64 mid=(l+r)>>1;
			if (check2(mid)>=m)
				l=mid+1;
			else
				r=mid;
		}
		l--;
		cout<<l;
		fclose(stdin);fclose(stdout);
		return 0;
	}
	cout<<sum/m;
	fclose(stdin);fclose(stdout);
	return 0;
}
