#include<algorithm>
#include<iostream>
#include<cstdio>
#include<cstring>
#define int64 long long
using namespace std;
const int maxn=110;
const int maxm=25010;
int i,j,n,ans,T;
int a[maxn],b[maxm];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);
		for(i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		memset(b,0,sizeof(b));
		b[0]=1;
		ans=n;
		for(i=1;i<=n;i++)
		{
			if (b[a[i]])
			{
				ans--;
				continue;
			}
			for(j=a[i];j<=a[n];j++)
				if (b[j-a[i]])
					b[j]=1;
		}
		cout<<ans<<endl;
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
