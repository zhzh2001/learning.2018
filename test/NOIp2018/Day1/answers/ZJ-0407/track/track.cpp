#include<stdio.h>
#include<bits/stdc++.h>
using namespace std;

struct edge{
	int t,nt,len;
}e[100004];
int cnt=0,head[50004];
int n,m,f[50004],d[50004];

void add(int x,int y,int len){
	e[++cnt].t=y;
	e[cnt].nt=head[x];
	e[cnt].len=len;
	head[x]=cnt;
}

int dfs(int u,int fa,int x){
	int v,sum=0;
	f[u]=0;
	for(int i=head[u];i;i=e[i].nt){
		v=e[i].t;
		if(v!=fa) sum+=dfs(v,u,x);
	}
	int tp=0;
	for(int i=head[u];i;i=e[i].nt){
		v=e[i].t;
		if(v!=fa) d[++tp]=f[v]+e[i].len;
	}
	sort(d+1,d+tp+1);
	d[0]=0;
	for(int i=1;i<=tp;i++){
		if(d[i]==0) continue;
		for(int j=0;j<i;j++){
			if(d[j]==0&&j!=0) continue;
			if(d[i]+d[j]>=x){
				sum++;
				d[i]=d[j]=0;
			}
		}
	}
	for(int i=tp;i>=1;i--){
		if(d[i]!=0){
			f[u]=d[i];
			break;
		}
	}
	return sum;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	
	int x,y,len,mxlen=0;
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&len);
		add(x,y,len),add(y,x,len);
		mxlen=max(mxlen,len);
	}
	int mid,l=0,r=mxlen/m*n+n,ans;
	while(l<=r){
		mid=(l+r)>>1;
		if(dfs(1,-1,mid)>=m) l=mid+1,ans=mid;
		else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}

