#include<bits/stdc++.h>
#define N 100005
#define K 10000
using namespace std;
bool cur1;
int n;
int d[N];
struct P70{
	int ans;
	void solve(){
		ans=0;
		bool flag=1;
		while(flag){
			flag=0;
			int last=0;
			for(int i=1;i<=n;i++){
				if(d[i]==0){
					if(last+1!=i)ans++;
					last=i;
				}
				else d[i]--,flag=1;
			}
			if(last!=n)ans++;
		}
		printf("%d\n",ans);
	}
}p70;
struct P100{
	int ans;
	int cnt[K+5];
	void solve(){
		ans=0;
		memset(cnt,0,sizeof(cnt));
		cnt[0]=1;
		for(int i=1;i<=n;i++){
			int o=0;
			if(d[i-1]>=d[i])o++;
			if(d[i+1]>d[i])o++;
			if(o==2)cnt[d[i]]++;
			else if(o==0)cnt[d[i]]--;
		}
		for(int i=1;i<=K;i++)cnt[i]+=cnt[i-1];
		int last=0;
		for(int i=1;i<=K;i++)ans+=cnt[i-1];
		printf("%d\n",ans);
	}
}p100;
bool cur2;
int main(){
//	printf("%lf MB\n",(&cur2-&cur1)/1024.0/1024);
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&d[i]);
	p100.solve();
	return 0;
}
