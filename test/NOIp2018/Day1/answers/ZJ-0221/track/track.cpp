#include<bits/stdc++.h>
#define N 50005
using namespace std;
bool cur1;
int n,m;
int head[N],id;
struct edge{
	int to,len,nxt;
}E[N<<1];
void add_edge(int a,int b,int c){
	E[++id]=(edge){b,c,head[a]};
	head[a]=id;
}
struct P100{
	int cnt,now;
	int mx[N];
	int que[N],top;
	int stk[N],top1;
	void dfs(int x,int f){
		for(int i=head[x];i;i=E[i].nxt){
			int v=E[i].to,c=E[i].len;
			if(v==f)continue;
			dfs(v,x);
			mx[v]+=c;
		}
		int MX=0;top=0;top1=0;
		for(int i=head[x];i;i=E[i].nxt){
			int v=E[i].to;
			if(v==f)continue;
			if(mx[v]>=now)cnt++;
			else que[++top]=mx[v];
		}
		sort(que+1,que+top+1);
		if(top==1)MX=que[1];
		for(int i=1,r=top;r>i;i++){
			while(r>i+1&&que[r-1]+que[i]>=now){
				stk[++top1]=r;
				r--;
			}
			if(r>i){
				if(que[i]+que[r]>=now)cnt++,r--;
				else if(top1!=0)top1--,cnt++;
				else MX=max(MX,que[i]);
			}
			if(i+1==r)stk[++top1]=r;
		}
		cnt+=top1/2;
		if(top1&1)MX=que[stk[1]];
		mx[x]=MX;
	}
	bool check(int x){
		memset(mx,0,sizeof(mx));
		memset(que,0,sizeof(que));
		cnt=0;now=x;
		dfs(1,-1);
		if(cnt>=m)return true;
		return false;
	}
	void solve(){
		int l=1,r=500000000,ans=1;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}p100;
bool cur2;
int main(){
//	printf("%lf MB\n",(&cur2-&cur1)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1,a,b,c;i<n;i++){
		scanf("%d%d%d",&a,&b,&c);
		add_edge(a,b,c);
		add_edge(b,a,c);
	}
	p100.solve();
	return 0;
}
