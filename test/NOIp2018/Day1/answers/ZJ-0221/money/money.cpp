#include<bits/stdc++.h>
#define N 105
#define K 25005
using namespace std;
bool cur1;
int n;
int a[N];
bool mark[K];
struct P100{
	int ans;
	void init(){
		ans=n;
	}
	void solve(){
		init();
		int l=1;
		for(int i=1;i<=a[n];i++){
			bool flag=0;
			for(int j=1;j<=n;j++){
				if(i<a[j])continue;
				if(mark[i-a[j]]){
					mark[i]=1;
					if(i!=a[j]){
						flag=1;
						break;
					}
				}
			}
			while(i==a[l]){
				if(flag)ans--;
				l++;
				flag=1;
			}
		}
		printf("%d\n",ans);
	}
}p100;
bool cur2;
int main(){
//	printf("%lf MB\n",(&cur2-&cur1)/1024.0/1024);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		memset(mark,0,sizeof(mark));mark[0]=1;
		for(int i=1;i<=n;i++)scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		p100.solve();
	}
	return 0;
}
