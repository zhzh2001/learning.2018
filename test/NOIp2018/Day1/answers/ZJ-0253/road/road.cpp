#include<cstdio>
#include<algorithm>
using namespace std;
int n,ans,a[100010];
int read(){
	int ret=0,c=getchar();
	while(c<48||57<c) c=getchar();
	while(47<c&&c<58) ret=ret*10+c-48,c=getchar();
	return ret;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	for(int i=1;i<=n;i++) ans+=max(a[i]-a[i-1],0);
	printf("%d",ans);
	return 0;
}
