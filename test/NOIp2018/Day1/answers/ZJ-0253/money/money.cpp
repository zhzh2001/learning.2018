#include<cstdio>
#include<algorithm>
using namespace std;
int T,n,m,ans,a[110];
bool f[25010];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	f[0]=1;
	for(scanf("%d",&T);T;T--){
		scanf("%d",&n),ans=n;
		for(int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1),m=a[n];
		for(int i=1;i<=m;i++) f[i]=0;
		for(int i=1;i<=n;i++){
			if(f[a[i]]){ans--;continue;}
			for(int j=0,_=m-a[i];j<=_;j++) if(f[j]) f[j+a[i]]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
