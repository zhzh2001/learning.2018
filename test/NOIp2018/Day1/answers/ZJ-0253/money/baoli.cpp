#include<cstdio>
#include<algorithm>
using namespace std;
int T,n,m,ans,a[110],f[110][25010];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.ans","w",stdout);
	for(int i=0;i<=100;i++) f[i][0]=1;
	for(scanf("%d",&T);T;T--){
		scanf("%d",&n),ans=0;
		for(int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1),m=a[n];
		for(int i=1;i<=n;i++){
			if(f[i-1][a[i]]) ans++;
			for(int j=1;j<=m;j++){
				f[i][j]=0;
				for(int k=0,_=j/a[i];k<=_;k++)
					if(f[i-1][j-a[i]*k]){f[i][j]=1;break;}
			}
		}
		printf("%d\n",n-ans);
	}
	return 0;
}
