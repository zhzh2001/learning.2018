#include<cstdio>
#include<set>
#include<algorithm>
#define N 50010
using namespace std;
typedef multiset<int>::iterator iter;
int n,m,k,tot,ans,fir[N],nex[N<<1],nod[N<<1],len[N<<1],f[N][2];
multiset<int> ss[N],s;
int read(){
	int ret=0,c=getchar();
	while(c<48||57<c) c=getchar();
	while(47<c&&c<58) ret=ret*10+c-48,c=getchar();
	return ret;
}
void add(int x,int y,int z){
	nod[++k]=y,len[k]=z,nex[k]=fir[x],fir[x]=k;
}
void DFS(int u,int fa,int l){
	for(int i=fir[u],v;i;i=nex[i]) if((v=nod[i])!=fa){
		DFS(v,u,l),f[u][0]+=f[v][0],f[v][1]+=len[i];
		if(f[v][1]>=l) f[u][0]++;
		else ss[u].insert(f[v][1]);
	}
	s=ss[u],ss[u].clear();
	int x=0,y=0;
	for(iter i;!s.empty();){
		i=s.begin(),x=*i,s.erase(i),i=s.lower_bound(l-x);
		if(i==s.end()){y=max(y,x);continue;}
		f[u][0]++,s.erase(i);
	}
	f[u][1]=y;
}
bool judg(int l){
	for(int i=1;i<=n;i++) f[i][0]=f[i][1]=0;
	DFS(1,0,l);
	return f[1][0]>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(int i=1,x,y,z;i<n;i++)
		x=read(),y=read(),z=read(),add(x,y,z),add(y,x,z),tot+=z;
	for(int l=1,r=tot,m;l<=r;) judg(m=l+r>>1)?(ans=m,l=m+1):r=m-1;
	printf("%d",ans);
	return 0;
}
