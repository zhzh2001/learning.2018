#include<cstdio>
#include<algorithm>
#define N 100
using namespace std;
int n,m,k,ans,fir[N],nex[N<<1],nod[N<<1],len[N<<1];
int fat[N],dep[N],fro[N],a[N],b[N],cho[N],vis[N];
void init(int u,int f){
	fat[u]=f,dep[u]=dep[f]+1;
	for(int i=fir[u],v;i;i=nex[i])
		if((v=nod[i])!=f) init(v,u),fro[v]=len[i];
}
void add(int x,int y,int z){
	nod[++k]=y,len[k]=z,nex[k]=fir[x],fir[x]=k;
}
int chec(){
	int ret=100000000;
	for(int i=1;i<=n;i++) vis[i]=0;
	//for(int i=1;i<=m;i++) printf("%d %d\n",a[i],b[i]);
	for(int i=1,x,y,z;i<=m;i++){
		x=a[i],y=b[i];
		if(dep[x]<dep[y]) z=x,x=y,y=z;
		z=0;
		while(dep[x]>dep[y]){
			if(vis[x]) return 0;
			z+=fro[x],vis[x]=1,x=fat[x];
		}
		while(x!=y){
			if(vis[x]||vis[y]) return 0;
			z+=fro[x]+fro[y];
			vis[x]=vis[y]=1,x=fat[x],y=fat[y];
		}
		ret=min(ret,z);
		//printf("%d\n",z);
	}
	return ret;
}
void DFS(int x,int y){
	if(x>m){ans=max(ans,chec());return;}
	for(int i=y;i<n;i++)
		for(int j=i+1;j<=n;j++) a[x]=i,b[x]=j,DFS(x+1,i);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.ans","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1,x,y,z;i<n;i++)
		scanf("%d%d%d",&x,&y,&z),add(x,y,z),add(y,x,z);
	init(1,0),DFS(1,1),printf("%d",ans);
	return 0;
}
