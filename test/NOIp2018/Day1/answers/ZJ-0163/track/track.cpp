#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;
int n,m,g=1,h=1;
struct edge{
	int a;
	int l;
};
vector<edge> v[50001];
struct res{
	int a;
	int l;
};
res dfs(int a,int b){
	res result;
	result.a=a;
	result.l=0;
	for(int i=0;i<v[a].size();i++){
		if(v[a][i].a==b)continue;
		res h=dfs(v[a][i].a,a);
		if(h.l+v[a][i].l>result.l){
			result.l=h.l+v[a][i].l;
			result.a=h.a;
		}
	}
	return result;
}
int checkh(int p,int m){
	int jd=1;
	for(int i=1;i<=m;i++){
		int s=0;
		while(s<p){
			int k;
			for(k=0;k<v[jd].size();k++){
				if(v[jd][k].a==jd+1){
					break;
				}
			}
			if(k==v[jd].size())return 0;
			s+=v[jd][k].l;
			jd++;
		}
	}
	return 1;
}
int findz(){
	res t=dfs(1,1);
	t=dfs(t.a,t.a);
	t=dfs(t.a,t.a);
	return t.l;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int suml=0;
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		int a,b,l;edge c;edge d;
		scanf("%d%d%d",&a,&b,&l);
		if(a!=1)g=0;
		if(b!=a+1)h=0;
		c.a=b;d.a=a;c.l=l;d.l=l;suml+=l;
		v[a].push_back(c);
		v[b].push_back(d);
	}
	if(m==1){
		printf("%d\n",findz());
	}else if(h){
		int l=0,r=(suml/m)+3;
		while(l+1<r){
			int mid=l+((r-l)/2);
			if(checkh(mid,m)){
				l=mid;
			}else{
				r=mid;
			}
		}
		printf("%d\n",l);
	}else{
		printf("%d\n",suml/m);
	}/*else{
		int l=0,r=(suml/m)+3;
		while(l+1<r){
			int mid=l+((r-l)/2);
			if(check(mid,m)){
				l=mid;
			}else{
				r=mid;
			}
		}
		printf("%d\n",l);
	}*/
}
