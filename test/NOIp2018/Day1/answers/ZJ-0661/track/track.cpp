#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int getint(){
	char ch=gc(); int res=0,ff=1;
	while(!isdigit(ch)) ch=='-'?ff=-1:0, ch=gc();
	while(isdigit(ch)) res=(res<<3)+(res<<1)+ch-'0', ch=gc();
	return res*ff;
}
const int maxn=50005,maxe=maxn*2;
int fir[maxn],nxt[maxe],w[maxe],son[maxe],tot;
void add(int x,int y,int z){
	son[++tot]=y; w[tot]=z; nxt[tot]=fir[x]; fir[x]=tot;
}
int S,T,res,d[maxn];
void dfs2(int x,int pre,int now){
	if(now>res) T=x, res=now;
	for(int j=fir[x];j;j=nxt[j])
		if(son[j]!=pre) dfs2(son[j],x,now+w[j]);
}
int n,m,a[maxn],sum[maxn],INF;
void dfs3(int x,int pre){
	for(int j=fir[x];j;j=nxt[j])
		if(son[j]!=pre){
			a[++a[0]]=w[j];
			dfs3(son[j],x);
		} 
}
bool Check1(int mid){
	int cnt=0,p=0; for(int i=1;i<=a[0];i++) if(a[i]<mid) p=i;
	cnt+=a[0]-p; if(cnt>=m) return true;
	int p2=1;
	for(int i=p;i>=1&&i>p2;i--){
		while(p2<i&&a[p2]+a[i]<mid) p2++;
		if(p2<i) cnt++, p2++;
	}
	return cnt>=m;
}
int f[maxn];
bool Check2(int mid){
	for(int i=0;i<=a[0]+1;i++) f[i]=0, fir[i]=0; tot=0;
	for(int i=1,p=1;i<=a[0];i++){
		while(p<a[0]&&sum[p]-sum[i-1]<mid) p++;
		if(sum[p]-sum[i-1]>=mid) add(p+1,i,0);
	}
	for(int i=1;i<=a[0]+1;i++){
		f[i]=f[i-1];
		for(int j=fir[i];j;j=nxt[j])
			f[i]=max(f[i],f[son[j]]+1);
	}
	return f[a[0]+1]>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=getint(); m=getint();
	bool pd1=true, pd2=true;
	for(int i=1;i<=n-1;i++){
		int x=getint(),y=getint(),z=getint(); a[++a[0]]=z; d[x]++; d[y]++;
		if(x!=1) pd1=false; if(y!=x+1) pd2=false;
		add(x,y,z); add(y,x,z);
	}
	if(m==1){
		res=-1e9; dfs2(1,1,0);
		S=T; res=-1e9; dfs2(S,S,0);	
		printf("%d\n",res);
		return 0;
	}
	if(pd1){
		int res=0; int L=1,R=1e9;
		sort(a+1,a+1+a[0]);
		while(L<=R){
			int mid=(L+R)>>1;
			if(Check1(mid)) res=mid, L=mid+1; else R=mid-1;
		}
		printf("%d\n",res);
		return 0;
	}
	if(pd2){
		int s=0; for(int i=1;i<=n;i++) if(d[i]==1) s=i;
		a[0]=0; dfs3(s,s);
		for(int i=1;i<=a[0];i++) sum[i]=sum[i-1]+a[i];
		int res=0; int L=1,R=1e9;
		while(L<=R){
			int mid=(L+R)>>1;
			if(Check2(mid)) res=mid, L=mid+1; else R=mid-1;
		}
		printf("%d\n",res);
		return 0;
	}
	return 0;
}
