#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int getint(){
	char ch=gc(); int res=0,ff=1;
	while(!isdigit(ch)) ch=='-'?ff=-1:0, ch=gc();
	while(isdigit(ch)) res=(res<<3)+(res<<1)+ch-'0', ch=gc();
	return res*ff;
}
const int maxn=105,maxm=25005;
int _test,n,a[maxn],f[maxm];
int max(int x,int y){ return x>y?x:y; }
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	_test=getint();
	while(_test--){
		n=getint(); int _max=0,ans=0;
		for(int i=1;i<=n;i++) a[i]=getint(), _max=max(_max,a[i]);
		sort(a+1,a+1+n); 
		for(int i=0;i<=_max;i++) f[i]=0; f[0]=1;
		for(int i=1;i<=n;i++)
			if(!f[a[i]]){
				ans++;
				for(int j=a[i];j<=_max;j++) f[j]|=f[j-a[i]];
			}
		printf("%d\n",ans);
	}
	return 0;
}
