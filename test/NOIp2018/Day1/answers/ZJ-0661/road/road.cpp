#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int getint(){
	char ch=gc(); int res=0,ff=1;
	while(!isdigit(ch)) ch=='-'?ff=-1:0, ch=gc();
	while(isdigit(ch)) res=(res<<3)+(res<<1)+ch-'0', ch=gc();
	return res*ff;
}
const int maxn=100005;
int n,a[maxn],f[maxn];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=getint();
	for(int i=1;i<=n;i++) a[i]=getint();
	for(int i=1;i<=n;i++){
		if(a[i]<=a[i-1]) f[i]=f[i-1];
		else f[i]=f[i-1]+a[i]-a[i-1];
	}
	printf("%d\n",f[n]);
	return 0;
}
