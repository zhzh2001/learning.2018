#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read()
{
	char ch=getchar(); int ans=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch<='9'&&ch>='0') ans=ans*10+ch-48,ch=getchar();
	return ans;
}
const int N=500005,M=1000005,MX=100000;
int num,vet[M],val[M],nex[M],head[N];
int n,m,a[N],b[N],dis[N],s,c[MX+5],f[MX+5],tnum[MX+5];
inline void chkmax(int &x,int y){x=(y>x)?y:x;}
bool cmp(int x,int y){return x>y;}
inline void add(int u,int v,int l)
{
	num++;
	vet[num]=v; val[num]=l; nex[num]=head[u]; head[u]=num;
}
void dfs(int u,int pre)
{
	for (int i=head[u]; i; i=nex[i])
	{
		int v=vet[i];
		if (v!=pre) dis[v]=dis[u]+val[i],dfs(v,u);
	}
}
inline int lowbit(int x){return x&(-x);}
inline void Add(int x,int y)
{
	for (; x<=MX; x+=lowbit(x)) tnum[x]+=y;
}
inline int Query(int x)
{
	int ans=0;
	for (; x; x-=lowbit(x)) ans=ans+tnum[x];
	return ans;
}
inline int Findk(int k)
{
	int s=0,ans=0;
	for (int i=16; i>=0; i--)
	{
		ans=ans+(1<<i);
		(ans>MX||s+tnum[ans]>=k)?(ans-=(1<<i)):(s+=tnum[ans]);
	}
	return ans+1;
}
inline bool check1(int x)
{
	int sum=0;
	memset(tnum,0,sizeof(tnum));
	for (int i=a[s]; i<=min(x-1,a[1]); i++) 
		if (c[i]) f[i]=c[i],sum=sum+c[i],Add(i,c[i]);
	for (int i=a[s]; i<=min(x-1,a[1]); i++)
	{
		if (f[i]&&x-i-1>a[1]) return 0;
		for (int j=1; j<=f[i]; j++)
		{
			int t=Query(x-i-1);
			if (sum==t) return 0;
			int p=Findk(t+1);
			--f[p]; sum-=2; Add(p,-1); Add(i,-1);
		}
		f[i]=0;
	}
	return 1;
}
inline bool check2(int x)
{
	int s=0,ans=0;
	for (int i=1; i<n; i++)
	{
		s=s+b[i];
		if (s>=x) ++ans,s=0;
	}
	return (ans>=m);
}
int main()
{
	freopen("track.in","r",stdin); freopen("track.out","w",stdout);
	n=read(),m=read(); 
	int mxl=0,sub1=1,sub2=1;
	for (int i=1; i<n; i++)
	{
		int u=read(),v=read(),l=read();
		add(u,v,l); add(v,u,l);
		a[i]=l,b[u]=l; mxl=mxl+l;
		if (u!=1) sub1=0;
		if (u+1!=v) sub2=0;
	}
	if (m==1)
	{
		dis[1]=0; dfs(1,0);
		int u=0,mx=0;
		for (int i=1; i<=n; i++) 
			if (dis[i]>mx) mx=dis[i],u=i;
		dis[u]=0; dfs(u,0);
		int ans=0;
		for (int i=1; i<=n; i++) chkmax(ans,dis[i]);
		printf("%d\n",ans);
		return 0;
	}
	if (sub1)
	{
		sort(a+1,a+n,cmp);
		s=min(m<<1,n-1);
		for (int i=1; i<=s; i++) c[a[i]]++;
		int l=1,r=min(mxl,a[1]<<2);
		while (l<r)
		{
			int mid=(l+r+1)>>1;
			if (check1(mid)) l=mid;
			else r=mid-1; 
		}
		printf("%d\n",l);
		return 0;
	}
	sub2=2;
	if (sub2)
	{
		int l=1,r=mxl;
		while (l<r)
		{
			int mid=(l+r+1)>>1;
			if (check2(mid)) l=mid;
			else r=mid-1;
		}
		printf("%d\n",l+sub2-1);
		return 0;
	}
	return 0;
}
/*
20 5
1 2 20
1 3 18
1 4 17
1 5 15
1 6 15
1 7 10
1 8 9
1 9 8
1 10 5
1 11 5
1 12 3
1 13 3
1 14 3
1 15 3
1 16 3
1 17 3
1 18 1
1 19 1
1 20 1

13 5
1 2 12
2 3 2
3 4 7
4 5 3
5 6 1
6 7 20
7 8 4
8 9 5
9 10 9
10 11 6
11 12 8
12 13 10
*/
