#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read()
{
	char ch=getchar(); int ans=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch<='9'&&ch>='0') ans=ans*10+ch-48,ch=getchar();
	return ans;
}
const int N=100005;
int n,ans,a[N];
int main()
{
	freopen("road.in","r",stdin); freopen("road.out","w",stdout);
	n=read();
	for (int i=1; i<=n; i++) a[i]=read();
	ans=0;
	for (int i=1; i<=n; i++) ans=ans+max(0,a[i]-a[i-1]);
	printf("%d\n",ans);
	return 0;
}