#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read()
{
	char ch=getchar(); int ans=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch<='9'&&ch>='0') ans=ans*10+ch-48,ch=getchar();
	return ans;
}
const int N=105,M=250005;
int n,m,mx,s,ans,a[N],b[N],c[M],f[M];
inline void chkmax(int &x,int y){x=(y>x)?y:x;}
inline void solve(int x,int y)
{
	int t1=mx/x;
	for (int i=0; i<=t1; i++)
	{
		int t2=(mx-x*i)/y;
		for (int j=1; j<=t2; j++)
		{
			int a=x*i+y*j;
			//printf("%d %d %d\n",x,y,a);
			if (!f[a]) f[a]=1,c[++s]=a;
		}
	}
}
int main()
{
	freopen("money.in","r",stdin); freopen("money.out","w",stdout);
	int cas=read();
	while (cas--)
	{
		s=0; m=0;
		for (int i=1; i<=mx; i++) f[i]=0;
		n=read(); mx=0;
		for (int i=1; i<=n; i++) chkmax(mx,a[i]=read());
		sort(a+1,a+1+n);
		b[++m]=a[1];
		for (int i=2; i<=n; i++)
		{
			int flag=0;
			for (int j=1; j<i; j++)
				if (a[i]%a[j]==0){flag=1; break;}
			if (!flag) b[++m]=a[i];
		}
		if (m==1){puts("1"); continue;}
		for (int i=1; i<=mx/b[1]; i++) f[i*b[1]]=1,c[++s]=i*b[1];
		ans=m; solve(b[1],b[2]);
		for (int i=3; i<=m; i++)
		{
			//printf("%d %d\n",b[i],f[b[i]]);
			if (f[b[i]]) --ans;
			else
				for (int j=1; j<=s; j++) solve(c[j],b[i]);
		}
		printf("%d\n",ans);
	}
	return 0;
}
/*
1
8
13 11 19 16 3 15 20 17
*/
