#include<bits/stdc++.h>
using namespace std;

const int maxn = 5e4 + 10;
typedef long long ll;

int n,m;
int Link[maxn],tot;
struct edge{
	int y,nxt,v;
}e[maxn << 1];
int tag[maxn << 1];

ll f[maxn];

ll ans;
int chn[maxn],num;

inline void read(int &x)
{
	x = 0;int f = 1;char s = getchar();
	for(;s < '0' || s > '9';s = getchar()) if(s == '-') f = - 1;
	for(;s >= '0' && s <= '9';s = getchar()) x = (x << 3) + (x << 1) + s - 48;
	x *= f;
}

inline int Max(int a,int b) {return a > b ? a : b;}
inline void Insert(int x,int y,int v) {e[++ tot].y = y;e[tot].v = v;e[tot].nxt = Link[x];Link[x] = tot;}
inline bool _cmp(edge a,edge b) {return a.v < b.v;}

void Dis(int x,int pre)
{
	for(int i = Link[x];i;i = e[i].nxt)
		if(e[i].y ^ pre){
			int y = e[i].y;
			Dis(y,x);
			ans = Max(ans,f[y] + f[x] + e[i].v);
			if(f[y] + e[i].v > f[x]) f[x] = f[y] + e[i].v;			
		}	
}

void Get_Chn(int x,int pre)
{
	for(int i = Link[x];i;i = e[i].nxt) 
		if(e[i].y ^ pre) chn[++ num] = e[i].v,Get_Chn(e[i].y,x);
}

bool Check_ex(ll st)
{
	int l = 1,r = 1;
	ll now = 0;
	int cnt = 0;
	for(;;)
	{
		if(l > n - 1) break;
		ll now = chn[l];
		for(;now < st && r <= n - 1;) ++ r,now += chn[r];
		if(now >= st) ++ cnt;
		else break;
		if(cnt >= m) return 1; 
		if(r > n - 1) break;
		l = r + 1;++ r;
	}
	return 0;
}

bool Check_jh(ll st)
{
	int l = 1,r = n - 1;
	int cnt = 0;
	for(;;)
	{
		if(l > r) break;
		if(chn[r] >= st) ++ cnt;
		else{
			for(;l < r && chn[r] + chn[l] < st;) ++ l;
			if(l != r && chn[l] + chn[r] >= st) ++ cnt,++ l;
			else break; 
		}
		-- r;
		if(cnt >= m) return 1;
	}
	return 0;
}

void Work_dis()
{
	Dis(1,0);
	printf("%lld",ans);
}

void Work_ex()
{
	ll l = 0,r = 0;
	for(int i = 1;i <= tot;++ i) r += e[i].v;
	Get_Chn(1,0);
	
	r >>= 1;
	while(l + 1 < r) Check_ex((l + r) >> 1) ? l = (l + r) >> 1 : r = (l + r) >> 1;
	printf("%lld",Check_ex(r) ? r : l);
}

void Work_jh()
{
	sort(e + 1,e + tot + 1,_cmp);
	for(int i = 1;i <= tot;i += 2) chn[++ num] = e[i].v;
	ll l = 0,r = 0;
	for(int i = 1;i <= num;++ i) r += chn[i];

	while(l + 1 < r) Check_jh((l + r) >> 1) ? l = (l + r) >> 1 : r = (l + r) >> 1;
	printf("%lld",Check_jh(r) ? r : l);	
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	read(n);read(m);
	bool flag = 1,fl = 1;
	int x,y,v;
	for(int i = 1;i < n;++ i) 
	{
		read(x);read(y);read(v);
		Insert(x,y,v);Insert(y,x,v);
		if(y != x + 1) flag = 0;
		if(x != 1) fl = 0;
	}
	if(m == 1) Work_dis();//20pts
	else if(fl) Work_jh();//15pts
	else if(flag) Work_ex();//20pts
	
	fclose(stdin);fclose(stdout);
	return 0;
}
