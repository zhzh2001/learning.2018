#include<bits/stdc++.h>
using namespace std;

const int maxn = 100000 + 10;
typedef long long ll;

int n;
int d[maxn];
int f[20][maxn],K[maxn];
//f:Min

ll ans;

inline void read(int &x)
{
	x = 0;int f = 1;char s = getchar();
	for(;s < '0' || s > '9';s = getchar()) if(s == '-') f = - 1;
	for(;s >= '0' && s <= '9';s = getchar()) x = (x << 3) + (x << 1) + s - 48;
	x *= f;
}

inline int Min_d(int a,int b) {return d[a] < d[b] ? a : b;}

inline int Query(int x,int y)
{
	int k = K[y - x + 1];
	return Min_d(f[k][x],f[k][y - (1 << k) + 1]);
}

void Solve(int l,int r,int now)
{
	if(l > r) return;
	if(l == r){
		ans += d[l] - now;return;
	}
	int Mid = Query(l,r);
	ans += d[Mid] - now;
	Solve(l,Mid - 1,d[Mid]);Solve(Mid + 1,r,d[Mid]);
	return;
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	read(n);
	for(int i = 1;i <= n;++ i) read(d[i]);
	for(int i = 1;i <= n;++ i) f[0][i] = i,K[i] = log2(i);
	for(int i = 1;(1 << i) <= n;++ i)
		for(int j = 1;j + (1 << i) - 1 <= n;++ j) f[i][j] = Min_d(f[i - 1][j],f[i - 1][j + (1 << i - 1)]);
	Solve(1,n,0);
	printf("%lld",ans);
	
	fclose(stdin);fclose(stdout);
	return 0;
}
