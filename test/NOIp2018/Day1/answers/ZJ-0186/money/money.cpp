#include<bits/stdc++.h>
using namespace std;

const int maxn = 110;
typedef long long ll;

int t,n;
int a[maxn];
bool ext[maxn];
int xxj[maxn],cnt;
int fdd[maxn],cnt_2;

inline void read(int &x)
{
	x = 0;int f = 1;char s = getchar();
	for(;s < '0' || s > '9';s = getchar()) if(s == '-') f = - 1;
	for(;s >= '0' && s <= '9';s = getchar()) x = (x << 3) + (x << 1) + s - 48;
	x *= f;
}

inline int Min(int a,int b) {return a < b ? a : b;}

bool dfs(int now,int s,int st)
{
	if(now > cnt) return (s == st);
	for(int i = (st - s) / a[ xxj[now] ];i >= 0;-- i) 
		if(dfs(now + 1,s + i * a[ xxj[now] ],st)) return 1;
	return 0;
}

void _QAQ()
{
	int ans = n;
	for(int s = 1;s < (1 << n);++ s)
	{
		cnt = 0;cnt_2 = 0;
		for(int i = 1;i <= n;++ i) ext[i] = (s >> i - 1) & 1,ext[i] ? xxj[++ cnt] = i : fdd[++ cnt_2] = i;
		bool flag = 1;
		for(int i = 1;i <= cnt_2;++ i)
		{
			if(! dfs(1,0,a[ fdd[i] ])){
				flag = 0;break;
			}
		}	
		if(flag) ans = Min(ans,cnt);
	}
	printf("%d\n",ans);
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	read(t);
	for(;t;-- t)
	{
		read(n);
		for(int i = 1;i <= n;++ i) read(a[i]);
		_QAQ();
	}
	
	fclose(stdin);fclose(stdout);
	return 0;
}
