#include<bits/stdc++.h>
#define ll long long
#define res register int
using namespace std;
const int M=25510;
int n,m,i,j,k,t,p,a[M],b[M],c[M],f[M],ans;
bool d[M];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	cin>>p;
	for (int q=1;q<=p;q++)
	{
		scanf("%d",&n);
		for ( i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		for ( i=1;i<=n;i++) d[i]=1;
		for ( i=2;i<=n;i++)
		{
			for ( j=1;j<=i-1;j++)
			{
				if (a[i]%a[j]==0) 
				{
					d[i]=0; break;
				}
			}
		}
		t=0;
		for ( i=1;i<=n;i++) if (d[i]) c[++t]=a[i];
		n=t; m=c[n];
		for ( i=1;i<=m;i++) d[i]=1;
		for ( i=1;i<=m;i++) f[i]=0;
		f[0]=1;
		for ( i=1;i<=n;i++)
		{
			if (d[c[i]])
			{
				for ( j=c[i];j<=m;j++)
				{
					if (f[j]) continue;
					if (f[j-c[i]]) 
					{
						f[j]=1;
					    if (j>c[i]) d[j]=0;
					}
				}
			}
		}
		ans=0;
		for ( i=1;i<=n;i++) if (d[c[i]]) ans++;
		printf("%d\n",ans);
	}
	return 0;
}
