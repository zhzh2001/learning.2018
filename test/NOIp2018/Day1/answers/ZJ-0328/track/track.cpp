#include<bits/stdc++.h>
using namespace std;
#define rep(x,y,z) for(int x=y; x<=z; x++)
#define downrep(x,y,z) for(int x=y; x>=z; x--)
#define LL long long
#define repedge(x,y) for(int x=hed[y]; ~x; x=edge[x].nex)
#define ms(x,y,z) memset(x,y,sizeof(z))
const int N=50005;
const LL inf=1e9;
int n,m,fa[N],hed[N],nedge,f[N],nw,num;
LL g[N],len,s[N];
struct Edge{ int to,nex; LL cst; }edge[N<<1];
void addedge(int a,int b,LL c){
	edge[nedge].to=b; edge[nedge].nex=hed[a];
	edge[nedge].cst=c; hed[a]=nedge++;
}
int Check(int tmp){
	int r=num; int res=0;
	rep(i,1,num){
		if (i==tmp) continue;
		if (r==tmp) r--;
		if ((i<r)&&(s[i]+s[r]>=len)){ r--; res++; }
	}
	return (res==nw);
}
LL Solve(){
	if (!Check(1)) return 0;
	int l=1; int r=num;
	while (l<r){
		int mid=((l+r)>>1);
		if (l==mid){ if (Check(r)) l=r; else r=l; break; }
		if (Check(mid)) l=mid;
		else r=mid-1;
	}
	return s[l];
}
void dfs(int k){
	repedge(i,k){
		int v=edge[i].to; if (v==fa[k]) continue;
		fa[v]=k; dfs(v); f[k]+=f[v];
	}
	num=0;
	repedge(i,k){
		int v=edge[i].to; if (v==fa[k]) continue;
		LL tmp=g[v]+edge[i].cst; if (tmp>=len) f[k]++; else s[++num]=tmp;
	}
	if (!num) return;
	sort(s+1,s+num+1); nw=0; int r=num;
	rep(i,1,num) if ((i<r)&&(s[i]+s[r]>=len)){ r--; nw++; } f[k]+=nw;
	g[k]=Solve(); 
}
int check(LL x){
    len=x; rep(i,1,n) f[i]=g[i]=0;
	dfs(1); return (f[1]>=m);
}
LL solve(){
	LL l=0; LL r=inf;
	while (l<r){
		LL mid=((l+r)>>1);
		if (l==mid){ if (check(r)) l=r; else r=l; break; }
		if (check(mid)) l=mid;
		else r=mid-1;
	}
	return l; 
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m); nedge=0; ms(hed,-1,hed);
	rep(i,1,n-1){ int a,b,c; scanf("%d%d%d",&a,&b,&c);
	   addedge(a,b,c); addedge(b,a,c); }
	printf("%lld\n",solve());
	return 0;
}
