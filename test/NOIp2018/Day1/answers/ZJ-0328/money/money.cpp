#include<bits/stdc++.h>
using namespace std;
#define rep(x,y,z) for(int x=y; x<=z; x++)
#define downrep(x,y,z) for(int x=y; x>=z; x--)
#define LL long long
#define repedge(x,y) for(int x=hed[y]; ~x; x=edge[x].nex)
#define ms(x,y,z) memset(x,y,sizeof(z))
const int N=105;
const int M=25005;
int n,a[N],f[M];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int cas; scanf("%d",&cas);
	rep(cl,1,cas){
		scanf("%d",&n);
		rep(i,1,n) scanf("%d",&a[i]); 
		sort(a+1,a+n+1); 
		rep(i,0,a[n]) f[i]=0; f[0]=1;
		int ans=n;
		rep(i,1,n){
			if (f[a[i]]) ans--;
			rep(j,a[i],a[n]) f[j]|=f[j-a[i]];
		}   
		printf("%d\n",ans);
	}
	return 0;
}
