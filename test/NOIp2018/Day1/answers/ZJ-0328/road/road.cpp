#include<bits/stdc++.h>
using namespace std;
#define rep(x,y,z) for(int x=y; x<=z; x++)
#define downrep(x,y,z) for(int x=y; x>=z; x--)
#define LL long long
#define repedge(x,y) for(int x=hed[y]; ~x; x=edge[x].nex)
#define ms(x,y,z) memset(x,y,sizeof(z))
const int N=100005;
int n;
LL d[N];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	rep(i,1,n) scanf("%lld",&d[i]);
	LL ans=0;
	rep(i,1,n) if (d[i]>d[i-1]) ans+=d[i]-d[i-1];
	printf("%lld\n",ans);
	return 0;
}
