#include<bits/stdc++.h>
using namespace std;
int n,A[105];
bool dp[25005];
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--) {
		scanf("%d",&n);
		int Mx=0;
		for(int i=1; i<=n; i++) {
			scanf("%d",&A[i]);
			Mx=max(Mx,A[i]);
		}
		sort(A+1,A+1+n);
		int ans=0;
		for(int i=1;i<=Mx;i++)dp[i]=0;
		dp[0]=1;
		for(int i=1; i<=n; i++) {
			if(dp[A[i]])continue;
			ans++;
			for(int j=A[i];j<=Mx;j++){
				dp[j]|=dp[j-A[i]];
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
