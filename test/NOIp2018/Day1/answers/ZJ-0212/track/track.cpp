#include<bits/stdc++.h>
using namespace std;
const int M=50005;
int n,m;
int head[M],asdf;
struct edge {
	int to,nxt,cost;
} G[M*2];
void add_edge(int a,int b,int c) {
	G[++asdf].to=b;
	G[asdf].nxt=head[a];
	G[asdf].cost=c;
	head[a]=asdf;
}
int lim,A[M],len;
int par[M];
int get_fa(int x) {
	if(par[x]==x)return x;
	return par[x]=get_fa(par[x]);
}
struct node {
	int cnt,dis;
} dp[M];
int get(int L,int R,int a) {
	int res=R+1;
	while(L<=R) {
		int mid=(L+R)>>1;
		if(A[mid]>=a) {
			res=mid;
			R=mid-1;
		} else {
			L=mid+1;
		}
	}
	return res;
}
void dfs(int x,int f) {
	dp[x].cnt=0,dp[x].dis=0;
	for(int i=head[x]; i; i=G[i].nxt) {
		int y=G[i].to;
		if(y==f)continue;
		dfs(y,x);
		dp[x].cnt+=dp[y].cnt;
	}
	len=0;
	for(int i=head[x]; i; i=G[i].nxt) {
		int y=G[i].to;
		if(y==f)continue;
		if(dp[y].dis+G[i].cost>=lim)dp[x].cnt++;
		else A[++len]=dp[y].dis+G[i].cost;
	}
	sort(A+1,A+1+len);
	for(int i=1; i<=len+1; i++)par[i]=i;
	for(int i=1; i<len; i++) {
		if(par[i]!=i)continue;
		int y=get(i+1,len,lim-A[i]);
		y=get_fa(y);
		if(y>len)continue;
		dp[x].cnt++;
		par[i]=len+1;
		par[y]=y+1;
	}
	int Mx=0;
	for(int i=len; i>=1; i--) {
		if(par[i]!=i)continue;
		Mx=A[i];
		break;
	}
	dp[x].dis=Mx;
}
bool check(int mid) {
	lim=mid;
	dfs(1,0);
	return dp[1].cnt>=m;
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int a,b,c;
	int L=1e9,R=0;
	scanf("%d %d",&n,&m);
	for(int i=1; i<n; i++) {
		scanf("%d %d %d",&a,&b,&c);
		add_edge(a,b,c);
		add_edge(b,a,c);
		L=min(L,c);
		R=R+c;
	}
	R/=m;
	int ans=L;
	while(L<=R) {
		int mid=(L+R)>>1;
		if(check(mid)) {
			ans=mid;
			L=mid+1;
		} else {
			R=mid-1;
		}
	}
	printf("%d\n",ans);
	return 0;
}
