#include<bits/stdc++.h>
using namespace std;
const int M=100005;
int n,A[M];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%d",&A[i]);
	}
	int ans=0;
	A[0]=0;
	for(int i=1;i<=n;i++){
		if(A[i-1]<=A[i]){
			ans+=A[i]-A[i-1];
		}
	}
	printf("%d\n",ans);
	return 0;
}
