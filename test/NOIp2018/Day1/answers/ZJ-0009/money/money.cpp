#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cmath>
#include<cstring>
#define N 105
#define M 25005
int a[N],b[N],f[M],T,n,m;
using namespace std;
int read(){
	int T=0;
	char c=getchar();
	while (c<'0'||c>'9') c=getchar();
	while (c>='0'&&c<='9'){
		T=T*10+c-48;
		c=getchar();
	}
	return T;
}
bool ok(int x){
	for (int i=1;i<=x;i++) f[i]=0;
	f[0]=1;
	for (int i=1;i<=m;i++)
		for (int j=b[i];j<=x;j++){
			if (j>=b[i]) f[j]|=f[j-b[i]];
			if (f[x]) return 1;
		}
	return 0;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		n=read();
		memset(f,0,sizeof(f));
		f[0]=1;
		for (int i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+1+n);
		m=0;
		for (int i=1;i<=n;i++)
			if (f[a[i]]==0){
				for (int j=a[i];j<=a[n];j++) f[j]|=f[j-a[i]];
				m++;
			}
		printf("%d\n",m);
	}
	return 0;
}
