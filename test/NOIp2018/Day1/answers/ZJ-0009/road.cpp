#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#define N 100005
#define M 10000005
using namespace std;
int n,a[N],b[N],l[M],r[M],t,w,ans;
int read(){
	int T=0;
	char c=getchar();
	while (c<'0'||c>'9') c=getchar();
	while (c>='0'&&c<='9'){
		T=T*10+c-48;
		c=getchar();
	}
	return T;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) a[i]=read();
	w=1; l[1]=1; r[1]=n;
	while (t<w){
		t++;
		if (l[t]==r[t]){
			ans+=a[l[t]];
			a[l[t]]=0;
			continue;
		}
		int m=1e9;
		for (int i=l[t];i<=r[t];i++) m=min(m,a[i]);
		ans+=m;
		for (int i=l[t];i<=r[t];i++) a[i]-=m;
		int len=l[t];
		while (a[len]==0) len++;
		b[1]=len-1;
		len=1;
		int last=r[t];
		while (a[last]==0) last--;
		for (int i=b[1]+1;i<last;i++)
			if (a[i]==0) b[++len]=i;
		b[++len]=last+1;
		for (int i=1;i<len;i++)
			if (b[i]+1<=b[i+1]-1){
				w++;
				l[w]=b[i]+1;
				r[w]=b[i+1]-1;
			} 
	}
	printf("%d\n",ans);
	return 0;
}
