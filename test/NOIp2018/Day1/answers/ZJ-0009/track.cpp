#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#define N 50005
using namespace std;
int tot,n,m,ans,root,Next[N<<1],Head[N],len[N<<1],To[N<<1],x,y,z,p[N],a[N],su[N],cnt,f[1005][1005];
void add(int x,int y,int z){
	tot++;
	Next[tot]=Head[x];
	To[tot]=y;
	len[tot]=z;
	Head[x]=tot;
}
int read(){
	int T=0;
	char c=getchar();
	while (c<'0'||c>'9') c=getchar();
	while (c>='0'&&c<='9'){
		T=T*10+c-48;
		c=getchar();
	}
	return T;
}
void dfs1(int x,int fa,int s){
	if (s>ans){
		root=x;
		ans=s;
	}
	for (int i=Head[x];i;i=Next[i]){
		int y=To[i];
		if (y!=fa) dfs1(y,x,s+len[i]);
	}
}
void dfs2(int x,int fa){
	root=x;
	for (int i=Head[x];i;i=Next[i]){
		int y=To[i];
		if (y!=fa){
			a[++cnt]=len[i]; 
			dfs2(y,x);
		}
	}
}
void dfs(int k,int s,int last){
	if (k>m){
		ans=max(ans,s);
		return;
	}
	for (int i=last+1;i<=n;i++) dfs(k+1,min(s,su[i]-su[last]),i);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int p1=1;
	int p2=1;
	for (int i=1;i<n;i++){
		x=read(); y=read(); z=read();
		add(x,y,z);
		add(y,x,z);
		a[i]=z;
		if (x!=1) p1=0;
		if (y!=x+1) p2=0;
	}
	if (m==1){
		ans=0;
		dfs1(1,0,0);
		ans=0;
		dfs1(root,0,0);
		printf("%d\n",ans);
		return 0;
	}
	if (p1){
		for (int time=1;time<=n-1-m;time++){
			int zx=1e9,k1=0,k2=0;
			for (int i=1;i<n;i++)
				if (a[i]<zx&&p[i]==0){
					zx=a[i];
					k1=i;
				}
			zx=1e9;
			for (int i=1;i<n;i++)
				if (a[i]<zx&&i!=k1&&p[i]==0){
					zx=a[i];
					k2=i;
				}
			p[k1]=1; p[k2]=1;
			a[k1]=a[k1]+a[k2];
			a[k2]=a[k1]+a[k2];
		}
		ans=1e9;
		for (int i=1;i<n;i++) ans=min(ans,a[i]);
		printf("%d\n",ans);
		return 0;
	}
	if (p2){
		root=0;
		dfs2(1,0);
		for (int i=1;i<n;i++) su[i]=su[i-1]+a[i];
		for (int i=1;i<n;i++) f[i][1]=su[i];
		for (int j=2;j<=m;j++)
			for (int i=j;i<n;i++)
				for (int k=j-1;k<=i-1;k++) f[i][j]=max(f[i][j],min(f[k][j-1],su[i]-su[k]));
		printf("%d\n",f[n-1][m]);
		return 0;
	}
	return 0;
}
