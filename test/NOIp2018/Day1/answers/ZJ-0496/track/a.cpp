#include<cstring>
#include<cstdio>
#include<cmath>
#include<cassert>
#include<algorithm>
#include<iostream>

using namespace std;

#define Oops() cout<<"!!!!!!!"<<endl
#define Divhim() cout<<">>>>>>"<<endl
#define Divher() cout<<"<<<<<<"<<endl
#define Whats(x) cout<<#x<<" is "<<(x)<<endl

template<typename T> bool GetMax(T &a,T b)
{
	return (a<b)?(a=b,true):false;
}

template<typename T> bool GetMin(T &a,T b)
{
	return (a>b)?(a=b,true):false;
}

/*
	 -<Unlimied Blade Works>-
 */

const int N=50000+10;

struct Edge
{
	int v,w;
}edges[N<<1];

int n,m;
int fir[N],nxt[N<<1],tote;

void Adde(int u,int v,int w)
{
	edges[++tote]=(Edge){v,w};
	nxt[tote]=fir[u];
	fir[u]=tote;
	edges[++tote]=(Edge){u,w};
	nxt[tote]=fir[v];
	fir[v]=tote;
}

namespace Solver_me1
{

	int dep[N];

	void Dfs(int u,int fa=0)
	{
		if(!fa)
			dep[u]=0;
		for(int i=fir[u];i;i=nxt[i])
		{
			Edge &e=edges[i];
			if(e.v==fa)
				continue;
			dep[e.v]=dep[u]+e.w;
			Dfs(e.v,u);
		}
	}

	void Solve()
	{
		Dfs(1);
		int a=1;
		for(int i=2;i<=n;++i)if(dep[i]>dep[a])
			a=i;
		Dfs(a);
		a=0;
		for(int i=1;i<=n;++i)
			GetMax(a,dep[i]);
		printf("%d\n",a);
	}

}

namespace Solver_ju
{

	int lst[N],ed;

	bool Check(int x)
	{
		int cnt=0,r=ed;
		for(int i=1;i<=r;++i)
		{
			if(lst[i]>=x)
				++cnt;
			else
			{
				for(;r>=i&&lst[r]+lst[i]<x;--r);
				if(r<i)
					break;
				--r;
				++cnt;
			}
		}
		return cnt>=m;
	}

	void Solve()
	{
		for(int i=fir[1];i;i=nxt[i])
			lst[++ed]=edges[i].w;
		sort(lst+1,lst+ed+1);
		int left=0,mid,right=500000000,ans=0;
		while(left<=right)
		{
			if(Check(mid=(left+right)>>1))
				left=(ans=mid)+1;
			else
				right=mid-1;
		}
		printf("%d",ans);
	}

}

namespace Solver_li
{

	int s[N],tot;

	bool Check(int x)
	{
		int l=0,cnt=0;
		for(int i=1;i<=tot&&cnt<m;++i)
		{
			if(s[i]-s[l]>=x)
			{
				l=i;
				++cnt;
			}
		}
		return cnt>=m;
	}

	void Dfs(int u,int fa=0)
	{
		for(int i=fir[u];i;i=nxt[i])
		{
			Edge &e=edges[i];
			if(e.v==fa)
				continue;
			s[++tot]=e.w;
			Dfs(e.v,u);
		}
	}

	void Solve()
	{
		Dfs(1);
		for(int i=1;i<=tot;++i)
			s[i]+=s[i-1];
		int left=0,mid,right=500000000,ans=0;
		while(left<=right)
		{
			if(Check(mid=(left+right)>>1))
				left=(ans=mid)+1;
			else
				right=mid-1;
		}
		printf("%d",ans);
	}

}

namespace Solver_bf
{

#define TN 1010
#define B 12

	int fa[TN],st[TN][B],dep[TN],len[TN],dfn[TN],mx[TN],tm;

	void Dfs(int u)
	{
		mx[u]=dfn[u]=++tm;
		dep[u]=dep[st[u][0]=fa[u]]+1;
		for(int i=1;i<B;++i)
			st[u][i]=st[st[u][i-1]][i-1];
		for(int i=fir[u];i;i=nxt[i])
		{
			Edge &e=edges[i];
			if(e.v==fa[u])
				continue;
			fa[e.v]=u;
			len[e.v]=len[u]+e.w;
			Dfs(e.v);
			GetMax(mx[u],mx[e.v]);
		}
	}

	int Lca(int u,int v)
	{
		if(dep[u]<dep[v])
			swap(u,v);
		for(int i=B-1;i>=0;--i)if(dep[st[u][i]]>=dep[v])
			u=st[u][i];
		if(u==v)
			return u;
		for(int i=B-1;i>=0;--i)if(st[u][i]!=st[v][i])
		{
			u=st[u][i];
			v=st[v][i];
		}
		return fa[u];
	}

	struct Data
	{

		int lca,u,v,p,q,w;

		void Song(int a,int b)
		{
			lca=Lca(a,b);
			u=a;
			v=b;
			w=len[a]+len[b]-2*len[lca];
			if(a==lca)
				p=-1;
			else
			{
				for(int i=B-1;i>=0;--i)if(dep[st[a][i]]>dep[lca])
					a=st[a][i];
				p=a;
			}
			if(b==lca)
				q=-1;
			else
			{
				for(int i=B-1;i>=0;--i)if(dep[st[b][i]]>dep[lca])
					b=st[b][i];
				q=b;
			}
		}

	}data[TN*TN];

	int tot;

	bool Cmp(const Data &a,const Data &b)
	{
		return (dep[a.lca]!=dep[b.lca])?(dep[a.lca]>dep[b.lca]):((a.p==-1)+(a.q==-1)<(b.p==-1)+(b.q==-1));
		//return (dep[a.lca]!=dep[b.lca])?(dep[a.lca]>dep[b.lca]):(a.w<b.w);
		//return dep[a.lca]>dep[b.lca];
	}

	int lst[TN*TN],ed;
	
	int C[TN];

	void Add(int x,int k)
	{
		for(;x<=tm;x+=(x&(-x)))
			C[x]+=k;
	}

	int Query(int x)
	{
		int res=0;
		for(;x;x&=x-1)
			res+=C[x];
		return res;
	}

	bool Valid(const Data &a)
	{
		return !(Query(dfn[a.u])+Query(dfn[a.v])-2*Query(dfn[a.lca]));
	}

	void Update(const Data &a)
	{
		if(~a.p)
		{
			Add(dfn[a.p],1);
			Add(mx[a.p]+1,-1);
		}
		if(~a.q)
		{
			Add(dfn[a.q],1);
			Add(mx[a.q]+1,-1);
		}
	}

	bool Check(int x)
	{
		for(int i=1;i<=tm;++i)
			C[i]=0;
		int cnt=0;
		for(int i=1;i<=tot&&cnt<m;++i)
		{
			if(data[i].w>=x&&Valid(data[i]))
			{
				//cout<<data[i].u<<","<<data[i].v<<endl;
				++cnt;
				Update(data[i]);
			}
		}
		return cnt>=m;
	}

	void Solve()
	{
		Dfs(1);
		for(int i=1;i<=n;++i)
			for(int j=i+1;j<=n;++j)
			{
				data[++tot].Song(i,j);
				lst[++ed]=data[tot].w;
			}
		sort(lst+1,lst+ed+1);
		ed=unique(lst+1,lst+ed+1)-lst-1;
		sort(data+1,data+tot,Cmp);
		int left=1,mid,right=ed,ans=1;
		while(left<=right)
		{
			if(Check(lst[mid=(left+right)>>1]))
				left=(ans=mid)+1;
			else
				right=mid-1;
		}
		printf("%d\n",lst[ans]);
	}

#undef TN
#undef B

}

int main()
{
	scanf("%d%d",&n,&m);
	bool fl=true,fl2=true;
	for(int i=1,u,v,w;i<n;++i)
	{
		scanf("%d%d%d",&u,&v,&w);
		Adde(u,v,w);
		fl&=(u==1);
		fl2&=(u+1==v);
	}
	if(m==1)
		Solver_me1::Solve();
	else if(fl)
		Solver_ju::Solve();
	else if(fl2)
		Solver_li::Solve();
	else
		Solver_bf::Solve();
	return 0;
}
