#include<cstring>
#include<cstdio>
#include<cmath>
#include<cassert>
#include<algorithm>
#include<iostream>

using namespace std;

#define Oops() cout<<"!!!!!!!"<<endl
#define Divhim() cout<<">>>>>>"<<endl
#define Divher() cout<<"<<<<<<"<<endl
#define Whats(x) cout<<#x<<" is "<<(x)<<endl

template<typename T> bool GetMax(T &a,T b)
{
	return (a<b)?(a=b,true):false;
}

template<typename T> bool GetMin(T &a,T b)
{
	return (a>b)?(a=b,true):false;
}

/*
	 -<Unlimied Blade Works>-
 */

#define int long long

int n,ans;

signed main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%lld",&n);
	for(int i=1,a,la=0;i<=n;++i)
	{
		scanf("%lld",&a);
		ans+=max(0ll,a-la);
		la=a;
	}
	printf("%lld\n",ans);
	return 0;
}
