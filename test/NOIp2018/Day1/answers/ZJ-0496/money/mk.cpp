#include<cstring>
#include<cstdio>
#include<cmath>
#include<cassert>
#include<algorithm>
#include<iostream>

using namespace std;

#define Oops() cout<<"!!!!!!!"<<endl
#define Divhim() cout<<">>>>>>"<<endl
#define Divher() cout<<"<<<<<<"<<endl
#define Whats(x) cout<<#x<<" is "<<(x)<<endl

template<typename T> bool GetMax(T &a,T b)
{
	return (a<b)?(a=b,true):false;
}

template<typename T> bool GetMin(T &a,T b)
{
	return (a>b)?(a=b,true):false;
}

/*
	 -<Unlimied Blade Works>-
 */

int Rdw()
{
	return rand()%25000+1;
}

int n;

void Work()
{
	n=100;
	cout<<n<<endl;
	for(int i=1;i<=n;++i)
		cout<<Rdw()<<" ";
	cout<<endl;
}

int main()
{
	int T;
	T=20;
	cout<<T<<endl;
	for(;T--;Work());
	return 0;
}
