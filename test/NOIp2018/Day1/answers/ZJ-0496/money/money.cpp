#include<cstring>
#include<cstdio>
#include<cmath>
#include<cassert>
#include<algorithm>
#include<iostream>
#include<bitset>

using namespace std;

#define Oops() cout<<"!!!!!!!"<<endl
#define Divhim() cout<<">>>>>>"<<endl
#define Divher() cout<<"<<<<<<"<<endl
#define Whats(x) cout<<#x<<" is "<<(x)<<endl

template<typename T> bool GetMax(T &a,T b)
{
	return (a<b)?(a=b,true):false;
}

template<typename T> bool GetMin(T &a,T b)
{
	return (a>b)?(a=b,true):false;
}

/*
	 -<Unlimied Blade Works>-
 */

const int N=233;
const int MX=25005;

int n,mx,a[N];
bool dp[MX];

void Work()
{
	scanf("%d",&n);
	mx=0;
	for(int i=1;i<=n;++i)
	{
		scanf("%d",a+i);
		GetMax(mx,a[i]);
	}
	sort(a+1,a+n+1);
	dp[0]=1;
	for(int i=1;i<=mx;++i)
		dp[i]=0;
	int ans=0;
	for(int i=1;i<=n;++i)
	{
		ans+=(!dp[a[i]]);
		for(int j=a[i];j<=mx;++j)
			dp[j]|=dp[j-a[i]];
	}
	printf("%d\n",ans);
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	for(scanf("%d",&T);T--;Work());
	return 0;
}
