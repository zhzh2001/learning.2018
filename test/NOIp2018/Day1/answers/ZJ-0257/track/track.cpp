#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
const int N=50002;
int n,m,head[N],nex[N<<1],to[N<<1],w[N<<1],a[1002],b[1002],c[1002],cnt;
inline void read(int &x){
	char c=getchar();x=0;
	for(;!isdigit(c);c=getchar());
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+c-'0';
	return;
}
inline void add(int u,int v,int wi){
	nex[++cnt]=head[u],head[u]=cnt,to[cnt]=v,w[cnt]=wi;
}
int mx1[N],mx2[N],top,mxans,f[1002];
inline void dfs(int x,int fa){
	for(int i=head[x];i;i=nex[i])if(to[i]!=fa){
		dfs(to[i],x);int dis=mx1[to[i]]+w[i];
		if(dis>=mx1[x])mx2[x]=mx1[x],mx1[x]=dis;
		else if(dis>mx2[x])mx2[x]=dis;
	}
	return;
}
inline void solve1(){
	dfs(1,0);
	int ans=0;
	for(int i=1;i<=n;i++)ans=max(ans,mx1[i]+mx2[i]);
	printf("%d\n",ans);
	return;
}
inline void solve2(){
	int ans=1e9;
	for(int i=head[1];i;i=nex[i])mx1[++top]=w[i];
	sort(mx1+1,mx1+top+1);
	if((m<<1)<=top){
		int l=top-(m<<1)+1,r=top;
		for(;l<r;++l,--r)ans=min(ans,mx1[l]+mx1[r]);
	}else{
		int t=top-m;
		for(int i=1;i<=t;++i)ans=min(ans,mx1[i]+mx1[(t<<1)+1-i]);
		for(int i=(t<<1)+1;i<=top;++i)ans=min(ans,mx1[i]);
	}
	printf("%d\n",ans);
	return;
}
inline bool check(int x){
	int sum=0,tot=0;
	for(int i=1;i<=top;i++)if(sum+mx1[i]>=x)sum=0,++tot;else sum+=mx1[i];
	return tot>=m;
}
inline void solve3(){
	int l=1,r=1e8,mid,ans=0;
	for(int i=1;i<n;i++)for(int j=head[i];j;j=nex[j])if(to[j]==i+1)mx1[++top]=w[j];
	while(l<=r){
		mid=(l+r)>>1;
		if(check(mid))ans=max(ans,mid),l=mid+1;else r=mid-1;
	}
	printf("%d\n",ans);
	return;
}
inline void dfs4(int x,int ne){
	if(ne==m){
		int minn=f[1];
		for(int i=2;i<=m;i++)minn=min(minn,f[i]);
		mxans=max(mxans,minn);
	}
	if(x>top)return;
	int tl,tr;
	for(int i=1;i<=ne;i++){
		if(mx1[i]==a[x])tl=mx1[i],mx1[i]=b[x],f[i]+=c[x],dfs4(x+1,ne),mx1[i]=tl,f[i]-=c[x];
		if(mx1[i]==b[x])tl=mx1[i],mx1[i]=a[x],f[i]+=c[x],dfs4(x+1,ne),mx1[i]=tl,f[i]-=c[x];
		if(mx2[i]==a[x])tr=mx2[i],mx2[i]=b[x],f[i]+=c[x],dfs4(x+1,ne),mx2[i]=tr,f[i]-=c[x];
		if(mx2[i]==b[x])tr=mx2[i],mx2[i]=a[x],f[i]+=c[x],dfs4(x+1,ne),mx2[i]=tr,f[i]-=c[x];
	}
	if(ne<m)mx1[ne+1]=a[x],mx2[ne+1]=b[x],f[ne+1]=c[x],dfs4(x+1,ne+1),mx1[ne+1]=0,mx2[ne+1]=0,f[ne+1]=0;
	dfs4(x+1,ne);
	return;
}
inline void getedge(int x,int fa){
	for(int i=head[x];i;i=nex[i])if(to[i]!=fa)a[++top]=x,b[top]=to[i],c[top]=w[i],getedge(to[i],x);
}
inline void solve4(){
	top=0,getedge(1,0);
	dfs4(1,0);
	printf("%d\n",mxans);
	return;
}
int main(){
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	bool fl1,fl2;read(n),read(m),fl1=fl2=0;
	for(int u,v,wi,i=1;i<n;i++)read(u),read(v),read(wi),add(u,v,wi),add(v,u,wi),fl1|=(u!=1),fl2|=(v!=u+1);
	if(m==1)solve1();
	else if(!fl1)solve2();
	else if(!fl2)solve3();
	else solve4();
	return 0;
}
