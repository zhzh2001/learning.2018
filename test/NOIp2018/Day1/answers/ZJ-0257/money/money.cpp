#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;
const int M=25005;
bool vis[M];
int T,n,ans,a[105];
inline void read(int &x){
	char c=getchar();x=0;
	for(;!isdigit(c);c=getchar());
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+c-'0';
	return;
}
int main(){
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	for(read(T);T;--T){
		read(n),ans=0;
		memset(vis,0,sizeof vis);
		for(int i=1;i<=n;i++)read(a[i]);
		sort(a+1,a+n+1),vis[0]=1;
		for(int i=1;i<=n;i++)if(!vis[a[i]]){
			ans++;
			for(int j=a[i];j<=25000;++j)
				vis[j]=vis[j]|vis[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
