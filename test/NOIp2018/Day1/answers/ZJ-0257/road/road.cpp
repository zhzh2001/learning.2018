#include<cstdio>
#include<cctype>
using namespace std;
inline void read(int &x){
	char c=getchar();x=0;
	for(;!isdigit(c);c=getchar());
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+c-'0';
	return;
}
const int N=100005;
typedef long long ll;
ll ans;
int n,top,q[N];
int main(){
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	read(n),ans=0,top=0;
	for(int x,i=1;i<=n;i++){
		read(x);
		if((top>0)&&(q[top]>=x))ans+=1ll*(q[top]-x);
		while((top>0)&&(q[top]>=x))--top;
		q[++top]=x;
	}
	if(top>0)ans+=1ll*q[top];
	printf("%lld\n",ans);
	return 0;
}
