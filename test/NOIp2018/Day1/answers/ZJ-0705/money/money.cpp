#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if (x<0)	{x=-x;putchar('-');}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=205;
int a[N],n;
bool yb[25005];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--)
	{
		n=read();
		For(i,1,n)	a[i]=read();
		For(i,0,25000)	yb[i]=0;
		yb[0]=1;
		sort(a+1,a+n+1);
		int ans=0;
		For(i,1,n)
		{
			if(yb[a[i]])	continue;
			ans++;
			For(j,a[i],25000)	yb[j]|=yb[j-a[i]];
		}	
		writeln(ans);
	}
	return 0;
}
