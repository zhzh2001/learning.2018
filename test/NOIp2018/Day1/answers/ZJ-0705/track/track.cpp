#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back
#define IT set<int> :: iterator
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if (x<0)	{x=-x;putchar('-');}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
const int N=5e4+5;
vector<int> vec[N];
int poi[N*2],nxt[N*2],v[N*2],head[N],cnt,n,m,nd,tot;
inline void add(int x,int y,int z)	{poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;v[cnt]=z;}
bool vis[N];
int Fa[N];
inline int Get(int x){return x==Fa[x]?x:Fa[x]=Get(Fa[x]);}
inline void Merge(int x,int y)
{
	x=Get(x);y=Get(y);
	Fa[x]=y;
}
inline int Dfs(int x,int fa)
{
	vec[x].clear();
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		int tmp=Dfs(poi[i],x)+v[i];
		if(tmp>=nd)	tot++;else	vec[x].pb(tmp);
	}
	if(!vec[x].size())	return 0;
	sort(vec[x].begin(),vec[x].end());
	int r=(int)vec[x].size()-1,l=0;
	For(i,0,r+1)	Fa[i]=i;
	int tmp=0;
	For(i,0,r)	
	{
		if(Fa[i]!=i)	continue;
		Merge(i,i+1);//i->i+1
		int it=r+1,l1=i+1,r1=r;// diyige >=nd-vec[x][i]
		while(l1<=r1)
		{
			int mid=l1+r1>>1;
			if(vec[x][mid]>=nd-vec[x][i])	it=mid,r1=mid-1;else	l1=mid+1;
		}
		it=Get(it);
		if(it==r+1)	tmp=max(tmp,vec[x][i]);
		else	
		{
			Merge(it,it+1);
			tot++;
		}
	}
	return tmp;
}
int rt;
inline bool check(int ned)
{
	tot=0;
	nd=ned;
	Dfs(1,1);
	if(tot>=m)	return 1;
	else	return 0;
}
int t[N];
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	For(i,1,n-1)
	{
		int x=read(),y=read(),z=read();
		add(x,y,z);add(y,x,z);
		t[i]=z;
	}
	int l=0,r=5e8,ans=0;
	while(l<=r)
	{
		int mid=l+r>>1;
		if(check(mid))	ans=mid,l=mid+1;else	r=mid-1;
	}
	writeln(ans);
	return 0;
}
/*
10 4
1 2 2 
2 3 3 
3 4 5
4 5 10
5 6 3
6 7 10
7 8 13
8 9 123
9 10 132
*/
