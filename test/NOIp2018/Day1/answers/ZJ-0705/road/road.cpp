#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if (x<0)	{x=-x;putchar('-');}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
const int N=200005;
bool vis[N];
int n;
struct node{int x,v;}	a[N];
inline bool cmp(node x,node y){return x.v>y.v;}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	For(i,1,n)	a[i].v=read(),a[i].x=i;
	sort(a+1,a+n+1,cmp);
	ll num=0;
	ll ans=0;
	For(i,1,n)
	{
		num++;
		vis[a[i].x]=1;
		if(vis[a[i].x-1])	num--;if(vis[a[i].x+1])	num--;
		ans=ans+1LL*num*(a[i].v-a[i+1].v);
	}
	writeln(ans);
	return 0;
}
