#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <algorithm>
#include <queue>
#define rit register int
//#define rit int
using std::sort;
using std::priority_queue;
using std::pair;
long int Min(long int a,long int b)
{
	return (a<b)?a:b;
}
long int Max(long int a,long int b)
{
	return (a>b)?a:b;
}
struct node
{
	int a,b,l;
	bool friend operator <(const node temp1,const node temp2)
	{
		return temp1.l<temp2.l;
	}
	bool friend operator >(const node temp1,const node temp2)
	{
		return temp1.l>temp2.l;
	}
}Edge[50001];
int n,m;
void solve2()
{
	int Lenth[50001];
	long long totl=0;
	for(register int i=0;i<n-1;++i)
	{
		totl+=Edge[i].l;
		Lenth[Edge[i].a]=Edge[i].l;
	}
	long long maxl=totl/m;
	long int dp[50001][2];
	memset(dp,0,sizeof(dp));
	for(register int i=0;i<n-1;++i)
	{
		for(register int j=Min(i,m);j;--j)
		{
			if(dp[j][1]>maxl) break;
			if(dp[j][0]<Min(dp[j-1][0],dp[j-1][1]))
			{
				dp[j][0]=Min(dp[j-1][0],dp[j-1][1]);
				dp[j][1]=0;
			}
			dp[j][1]+=Lenth[i];
		}
		if(dp[0][1]<=maxl)
		dp[0][1]+=Lenth[i];
		dp[0][0]+=Lenth[i];
	}
	dp[m][0]=Max(Min(dp[m-1][1],dp[m-1][0]),dp[m][0]);
	printf("%ld\n",dp[m][0]);
}
void solve1()
{
	int Lenth[50001];
	for(register int i=0;i<n-1;++i)
	{
		Lenth[Edge[i].b-1]=Edge[i].l;
	}
	sort(Lenth,Lenth+n-1);
	int ans=0x7ffffff;
	for(register int i=n-2,j=n-1-2*m;i>j;--i,++j)
	{
		ans=Min(Lenth[i]+Lenth[j],ans);
	}
	printf("%d\n",ans);
}
pair<long int ,long int> dfs(int x,int Frm)
{
	long int ans0=0,ans11=0,ans1=0;
	for(rit i=0;i<n-1;i++)
	{
		if(Edge[i].a!=x&&Edge[i].b!=x) continue;
		if(Edge[i].a==Frm||Edge[i].b==Frm) continue;
		if(Edge[i].a==x)
		{
			pair<long int,long int>temp=dfs(Edge[i].b,x);
			ans0=Max(ans0,temp.first);
			if(ans1<temp.second+Edge[i].l)
			{
				ans11=ans1;
				ans1=temp.second+Edge[i].l;
			}
			else if(ans11<temp.second+Edge[i].l)
			{
				ans11=temp.second+Edge[i].l;
			}
		}
		else
		{
			pair<long int,long int>temp=dfs(Edge[i].a,x);
			ans0=Max(ans0,temp.first);
			if(ans1<temp.second+Edge[i].l)
			{
				ans11=ans1;
				ans1=temp.second+Edge[i].l;
			}
			else if(ans11<temp.second+Edge[i].l)
			{
				ans11=temp.second+Edge[i].l;
			}
		}
	}
	pair<long int,long int>ans(Max(ans0,ans1+ans11),ans1);
	return ans;
}
void solve3()
{
	pair<long int,long int>ans=dfs(0,-1);
	printf("%ld\n",Max(ans.first,ans.second));
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool istyp1=true;
	bool istyp2=true;//b=a+1
	int minl=0x7ffffff,minl2=0x7ffffff;
	for(register int i=0;i<n-1;i++)
	{
		scanf("%d%d%d",&Edge[i].a,&Edge[i].b,&Edge[i].l);
		--Edge[i].a,--Edge[i].b;
		if(minl>=Edge[i].l) minl2=minl,minl=Edge[i].l;
		else if(minl2>Edge[i].l) minl2=Edge[i].l;
		istyp2=(Edge[i].b!=Edge[i].a+1)?false:istyp2;
		istyp1=(Edge[i].a)?false:istyp1;
	}
	if(m==n-1) printf("%d\n",minl);
	else if(istyp2) solve2();
	else if(istyp1) solve1();
	else if(m==n-2) printf("%d\n",minl2);
	else solve3();
	return 0;
}
