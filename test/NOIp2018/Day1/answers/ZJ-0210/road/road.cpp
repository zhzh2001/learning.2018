#include <stdio.h>
int n,d[100001];
long long tot=0;
void DFS(int st,int ed)
{
	if(st==ed) return;
	if(st==ed+1){
		tot+=d[st];
		return;
	}
	int mini=st;
	for(register int i=st;i<ed;i++)
	{
		mini=(d[i]<d[mini])?i:mini;
	}
	int mind=d[mini];
	for(register int i=st;i<ed;i++)
	{
		d[i]-=mind;
	}
	tot+=mind;
	DFS(st,mini);
	DFS(mini+1,ed);
	return;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(register int i=0;i<n;++i)
	{
		scanf("%d",&d[i]);
	}
	DFS(0,n);
	printf("%lld\n",tot);
	return 0;
}
