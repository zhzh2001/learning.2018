#include <bits/stdc++.h>
#define N 500010
using namespace std;

inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
struct edge{int v,w,next;}vs[N<<1];
int n,m,ee,st[N],L=0,R=0;
int F[N],Ans=0,sts,Vs[N],si[N];

struct segment_tree
{
	int tree[N<<2];
	inline void add(int x,int y,int l,int r,int rt)
	{
		if(l==r) {tree[rt]+=y; return ;}
		int mid=(l+r)>>1;
		if(mid>=x) add(x,y,l,mid,rt<<1);
		if(mid<x) add(x,y,mid+1,r,rt<<1|1);
		tree[rt]=tree[rt<<1]+tree[rt<<1|1];
	}
	inline int find(int x,int l,int r,int rt)
	{
		if(!tree[rt]) return -1;
		if(l==r) return l;
		int mid=(l+r)>>1,res=-1;
		if(mid>=x&&tree[rt<<1])
			res=find(x,l,mid,rt<<1);
		if(tree[rt<<1|1]&&res==-1)
			res=find(x,mid+1,r,rt<<1|1);
		return res;
	}
}G;

inline void addedge(int u,int v,int w)
{
	vs[++ee].v=v;vs[ee].next=st[u];
	vs[ee].w=w;st[u]=ee;
}
inline int find(int rt,int pr)
{
	int mx1=0,mx2=0;
	for(int i=st[rt];i;i=vs[i].next)
	{
		if(vs[i].v==pr) continue;
		int gs=vs[i].w+find(vs[i].v,rt);
		if(gs>=mx1) mx2=mx1,mx1=gs;
		else if(gs>mx2) mx2=gs;
	}
	R=max(R,mx1+mx2); return mx1;
}
inline int dfs(int rt,int pr,int las)
{
	int stt=las+1; F[rt]=0;
	Vs[++las]=0; si[las]=0;
	for(int i=st[rt];i;i=vs[i].next)
	{
		if(vs[i].v==pr) continue;
		int gs=vs[i].w+dfs(vs[i].v,rt,las);
		F[rt]+=F[vs[i].v];
		if(gs<sts) Vs[++las]=gs,si[las]=1;
		else F[rt]++;
	}
	sort(Vs+stt,Vs+las+1); int tot=las-stt+1;
	for(int i=stt;i<=las;i++) G.add(i,1,1,tot,1);
	for(int i=stt;i<=las;i++) if(si[i])
	{
		int ty=sts-Vs[i]; G.add(i,-1,1,tot,1);
		int tg=upper_bound(Vs+stt,Vs+las+1,ty-1)-Vs-stt;
		int sg=tg>tot? -1:G.find(tg,1,tot,1);
		if(sg==-1) G.add(i,1,1,tot,1);
		else G.add(sg,-1,1,tot,1),si[i]=si[sg]=0,F[rt]++;
	}
	for(int i=stt;i<=las;i++) if(si[i]) G.add(i,-1,1,tot,1);
	int lef=0; for(int i=stt;i<=las;i++) 
		if(si[i]) lef=max(lef,Vs[i]); 
	return lef;
}
inline bool check(int X)
{
	sts=X; dfs(1,0,0);
	return F[1]>=m;
}
int main()
{
	//freopen("read.in","r",stdin);
	n=read(); m=read();
	for(int i=1;i<n;i++)
	{
		int u=read(), v=read(), l=read();
		addedge(u,v,l); addedge(v,u,l);
	}
	find(1,0);
	while(L<=R)
	{
		int mid=(L+R)>>1;
		if(check(mid)) Ans=mid,L=mid+1;
		else R=mid-1;
	}
	cout << Ans << endl;
	return 0;
}
