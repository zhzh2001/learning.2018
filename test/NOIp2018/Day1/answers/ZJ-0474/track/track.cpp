#include <bits/stdc++.h>
#define N 500010
using namespace std;

inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
struct edge{int v,w,next;}vs[N<<1];
int n,m,ee,st[N],L=0,R=0;
int F[N],Ans=0,sts,Vs[N];
inline void addedge(int u,int v,int w)
{
	vs[++ee].v=v;vs[ee].next=st[u];
	vs[ee].w=w;st[u]=ee;
}
inline int find(int rt,int pr)
{
	int mx1=0,mx2=0;
	for(int i=st[rt];i;i=vs[i].next)
	{
		if(vs[i].v==pr) continue;
		int gs=vs[i].w+find(vs[i].v,rt);
		if(gs>=mx1) mx2=mx1,mx1=gs;
		else if(gs>mx2) mx2=gs;
	}
	R=max(R,mx1+mx2); return mx1;
}
inline int dfs(int rt,int pr,int las)
{
	int stt=las+1; F[rt]=0;
	for(int i=st[rt];i;i=vs[i].next)
	{
		if(vs[i].v==pr) continue;
		int gs=vs[i].w+dfs(vs[i].v,rt,las);
		F[rt]+=F[vs[i].v];
		if(gs<sts) Vs[++las]=gs;
		else F[rt]++;
	}
	sort(Vs+stt,Vs+las+1);	
	for(int i=stt;i<=las;i++) if(Vs[i])
	{
		for(int j=i+1;j<=las;j++)
			if(Vs[i]+Vs[j]>=sts)
				F[rt]++,Vs[i]=Vs[j]=0;
	}
	int lef=0; for(int i=stt;i<=las;i++) 
		lef=max(lef,Vs[i]); 
	return lef;
}
inline bool check(int X)
{
	sts=X; dfs(1,0,0);
	return F[1]>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<n;i++)
	{
		int u=read(), v=read(), l=read();
		addedge(u,v,l); addedge(v,u,l);
	}
	find(1,0);
	if(m==1){cout << R << endl; return 0;}
	while(L<=R)
	{
		int mid=(L+R)>>1;
		if(check(mid)) Ans=mid,L=mid+1;
		else R=mid-1;
	}
	cout << Ans << endl;
	return 0;
}
