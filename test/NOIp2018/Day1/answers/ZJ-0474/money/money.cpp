#include <bits/stdc++.h>
#define MAX_V 25000
using namespace std;

inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
int t,n,a[110],F[MAX_V+10],Ans;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read(); while(t--)
	{
		n=read(); Ans=0;
		memset(F,0,sizeof F);
		for(int i=1;i<=n;i++)
			a[i]=read();
		sort(a+1,a+1+n);
		for(int i=1;i<=n;i++)
		{
			if(!F[a[i]]) Ans++;
			F[a[i]]=1;
			for(int j=1;j<=MAX_V-a[i];j++)
				F[j+a[i]]|=F[j];
		}
		cout << Ans << endl;
	}
	return 0;
}
	
