#include <bits/stdc++.h>
#define N 100010
using namespace std;

inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
int n,a[N],sum;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	for(int i=n+1;i>=1;i--) a[i]-=a[i-1];
	for(int i=1;i<=n+1;i++)
		sum+=a[i]>0? a[i]:0;
	cout << sum << endl;
	return 0;
}
