#include<bits/stdc++.h>
using namespace std;
const int MAXN=200005;
typedef long long ll;
int a[MAXN];
ll ans;
inline int read()
{
	int f=1,x=0; char c=getchar();
	while (c<'0'||c>'9') { if (c=='-') f=-1; c=getchar(); }
	while (c>='0'&&c<='9') { x=(x<<3)+(x<<1)+c-'0'; c=getchar(); }
	return x*f;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n=read();
	for (int i=1;i<=n;i++) a[i]=read();
	a[0]=0;
	for (int i=1;i<=n;i++)
		if (a[i]>a[i-1]) ans+=(ll)a[i]-a[i-1];
	printf("%lld\n",ans);
	return 0;
}
