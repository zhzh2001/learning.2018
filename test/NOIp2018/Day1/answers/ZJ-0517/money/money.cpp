#include<bits/stdc++.h>
using namespace std;
const int MAXN=50005;
int a[MAXN];
bool f[MAXN];
inline int read()
{
	int f=1,x=0; char c=getchar();
	while (c<'0'||c>'9') { if (c=='-') f=-1; c=getchar(); }
	while (c>='0'&&c<='9') { x=(x<<3)+(x<<1)+c-'0'; c=getchar(); }
	return x*f;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int Case=read();
	while (Case--)
	{
    	int n=read(),ans=0;
    	for (int i=1;i<=n;i++) a[i]=read();
    	sort(a+1,a+n+1);
    	memset(f,0,sizeof f);
    	f[0]=1;
		for (int i=1;i<=n;i++)
		if (f[a[i]]==0) 
		{
		    ans++;
    	    for (int j=a[i];j<=30000;j++)
			    if (f[j-a[i]]&&!f[j]) f[j]=1;
			//for (int j=1;j<=30;j++) cout<<f[j];cout<<endl;
		}
		printf("%d\n",ans);
	}
}
