#include<bits/stdc++.h>
using namespace std;
const int MAXN=100005;
typedef long long ll;
struct enode{ll to,c;};
vector<enode> e[MAXN];
ll ans[MAXN],maxl[MAXN],s[MAXN],mid;
inline int read()
{
	int f=1,x=0; char c=getchar();
	while (c<'0'||c>'9') { if (c=='-') f=-1; c=getchar(); }
	while (c>='0'&&c<='9') { x=(x<<3)+(x<<1)+c-'0'; c=getchar(); }
	return x*f;
}
void tree_dp(int x,int father)
{
	for (int i=0;i<e[x].size();i++) 
	    if (e[x][i].to!=father) tree_dp(e[x][i].to,x);
	maxl[x]=0,ans[x]=0;
	ll p=0,ANS=0;
	for (int i=0;i<e[x].size();i++)
	    if (e[x][i].to!=father) ans[x]+=ans[e[x][i].to],s[++p]=maxl[e[x][i].to]+e[x][i].c;
	sort(s+1,s+p+1);
	while (s[p]>=mid&&p>0) ANS++,p--;
	
	if (p>=1)
	{
    	ll l=1,r=p;
    	while (l<r)
    	{
    		if (s[l]+s[r]>=mid) ans[x]++,l++,r--;
    		else maxl[x]=max(maxl[x],s[l]),l++;
    	}
    	if (l==r) maxl[x]=max(maxl[x],s[l]);
    }
    ll P=1,Q=p;
    while (P<=Q)
    {
    	//cout<<mid<<" "<<x<<" "<<P<<" "<<Q<<" "<<s[1]<<" "<<s[2]<<" "<<s[3]<<" "<<ans[x]<<endl;
    	ll MID=(P+Q)>>1,Ans=0;
    	ll l=1,r=p;
    	while (l<r)
    	{
    		if (l==MID) l++;
    		if (r==MID) r--;
    		if (l>=r) break;
    		//cout<<l<<" "<<r<<" "<<s[l]+s[r]<<endl;
    		if (s[l]+s[r]>=mid) Ans++,l++,r--;
    		else l++;
    	}
        if (Ans==ans[x]) maxl[x]=max(maxl[x],s[MID]),P=MID+1;
        else Q=MID-1;
	}
    ans[x]+=ANS;
    //cout<<mid<<" "<<x<<" "<<ans[x]<<" "<<maxl[x]<<endl;
}
int main()
{
    freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n=read(),m=read();
	ll l=0,r=0;
	bool flag1=1,flag2=1;
	for (int i=1;i<n;i++)
	{
		int u=read(),v=read(),c=read();
		if (v!=u+1) flag1=0;
		if (u!=1) flag2=0;
		e[u].push_back((enode){v,c});
		e[v].push_back((enode){u,c});
		r+=(ll)c;
	}
	if (flag1)
	{
    	while (l<r)
    	{
	    	ll mid=(l+r+1)>>1,ans=0,p=0;
	    	for (int i=1;i<=n;i++)
    		{
	    	    for (int j=0;j<e[i].size();j++)
    		    if (e[i][j].to==i+1) p+=e[i][j].c;
    		    if (p>=mid) ans++,p=0;
    		}
    	    if (ans>=m) l=mid;
    	    else r=mid-1;
    	}
    	printf("%lld\n",l);
    	return 0;
	}
	if (flag2)
	{
    	while (l<r)
    	{
	    	ll mid=(l+r+1)>>1,ans=0,p=0;
	    	for (int j=0;j<e[1].size();j++) s[++p]=e[1][j].c;
    	    ll L=1,R=p;
			while (L<R)
			{
    		    if (s[L]+s[R]>=mid) ans++,L++,R--;
    	    	else L++;
			}
			if (ans>=m) l=mid;
    	    else r=mid-1;
    	}
    	printf("%lld\n",l);
    	return 0;
	}
	while (l<r)
	{
		mid=(l+r+1)>>1;
		memset(ans,0,sizeof ans);
		memset(maxl,0,sizeof maxl);
	    tree_dp(1,0);
	    if (ans[1]>=m) l=mid;
	    else r=mid-1;
	}
	printf("%lld\n",l);
	return 0;
}
