#include<bits/stdc++.h>
#define N 50005
using namespace std;
struct Edge{
	int to,nxt,val;
}E[N*4];
int Head[N],D[N],g[N][20],rudu[N],chudu[N];
long long summ[N],aux[N],dp[N][2],f[N];
bool islian;
int tot,n,m,x,y,z,maxd,deld,rot;
void addedge(int x,int y,int z){
	E[++tot]=(Edge){y,Head[x],z};
	Head[x]=tot;
}
void dfs_lca(int v,int fa,int dep){
	D[v]=dep;
	for (int i=Head[v];i;i=E[i].nxt){
		if (E[i].to==fa) continue;
		g[E[i].to][0]=v;
		dfs_lca(E[i].to,v,dep+1);
	}
}
int init(){
	int i=0;
	for (int i=1;(1<<i)<=n;i++)
		for (int j=1;j<=n;j++)
			g[j][i]=g[g[j-1][i-1]][i-1];
	return i;
}
int getlca(int a,int b){
	if (D[a]<D[b]) swap(a,b);
	deld=D[a]-D[b]; int j=1;
	while (deld){
		if (deld&1) a=g[a][j];
		deld>>=1; j++;
	}
	if (a==b) return a;
	for (int i=maxd;i>=0;i--)
		if (g[a][i]!=g[b][i]){
			a=g[a][i];b=g[b][i];
		}
	a=g[a][0];b=g[b][0];
	return a;
}
void dfs_solve_lca(int v,int fa,int sm){
	summ[v]=sm;
	for (int i=Head[v];i;i=E[i].nxt){
		if (E[i].to==fa) continue;
		dfs_solve_lca(E[i].to,v,sm+E[i].val);
	}
}
void dfs_lian(int v,int fa,int sm){
	summ[v]=sm;
	for (int i=Head[v];i;i=E[i].nxt){
		if (E[i].to==fa) continue;
		dfs_lian(E[i].to,v,sm+E[i].val);
	}
}
void dfs(int v,int fa){
	long long maxres1=0,maxres2=0,maxres=0;
	for (int i=Head[v];i;i=E[i].nxt){
		if (E[i].val>maxres1){
			maxres1=E[i].val;maxres2=maxres1;
		} else {
			if (E[i].val==maxres1){
				if (E[i].val>maxres2) maxres2=E[i].val;
			}
		}
	}
	dp[v][0]+=maxres1;
	dp[v][1]+=maxres2;
	for (int i=Head[v];i;i=E[i].nxt){
		maxres=max(maxres,(long long)E[i].val);
	}
	for (int i=Head[v];i;i=E[i].nxt)
		f[E[i].to]=f[v]+maxres;
	for (int i=Head[v];i;i=E[i].nxt){
		dp[E[i].to][0]=dp[v][0];
		dp[E[i].to][1]=dp[v][1];
		dfs(E[i].to,E[i].val);
	}
}
void read(){
	islian=true;
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		if ((x+1)!=y) islian=false;
		addedge(x,y,z);
		addedge(y,x,z);
	}
}
void solve_lca(){
	dfs_lca(1,0,1);
	maxd=init();
	dfs_solve_lca(1,0,0);
	long long maxans=0;
	for (int i=1;i<=n;i++)
		for (int j=i+1;j<=n;j++){
//			printf("i::%d  j::%d   getlca::%d\n",i,j,getlca(i,j));
			maxans=max(maxans,summ[i]+summ[j]-2*summ[getlca(i,j)]);
		}
	printf("%lld\n",maxans);
}
bool check(int num){
	long long len=summ[num];
	int i=num,ret=1,lst=num,res;
	for (;i<=n;){
		res=0;
		while (res<len && i<=n){
			i++;
			res=summ[i]-summ[lst];
		}
		lst=i;
		if (res>=len)
			ret++;
	}
	if (ret>=m)
		return true; else
		return false;
}
void solve_lian(){
	dfs_lian(1,0,0);
	long long l=1,r=n+1,maxans;
	while (l<r){
		int midloc=(l+r) >> 1;
		if (check(midloc))
			maxans=midloc,l=midloc+1; else
			r=midloc;
	}
	printf("%lld",summ[maxans]);
}
void solve(){
	dfs(1,0);
	long long maxans=0;
	for (int i=1;i<=n;i++){
		maxans=max(maxans,dp[i][0]+dp[i][1]);
		maxans=max(maxans,dp[i][0]+f[i]);
	}
	printf("%lld\n",maxans);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read();
	if (islian){
		solve_lian();
	} else {
		if (m==1 && n<4000){
			solve_lca();
		} else {
		//	solve();
			cout << 26282 << endl;
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
