#include<bits/stdc++.h>
#define N 205
using namespace std;
int T,a[N],n,res,now,tot,aux[N],maxn;
bool use[25005];
void read(){
	memset(a,0,sizeof(a));
	tot=0;maxn=0;
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%d",&aux[i]),
		maxn=max(maxn,aux[i]+1);
	for (int i=0;i<=maxn+2;i++)	use[i]=false;
	for (int i=1;i<=n;i++){
		a[++tot]=aux[i];
		if (a[i]==1){
			printf("1\n");
			return;
		}
		if (use[a[tot]]){
			tot--;
			continue;
		} 
		for (int j=1;j<=maxn;j++){
			if (j*a[tot]>maxn) break;
			use[j*a[tot]]=true;
		}
	}
}
void solve(){
	sort(a+1,a+tot+1);
//	for (int i=1;i<=tot;i++)
//		cout << a[i] << " ";
//	cout <<  endl;
	now=1;res=0;
	for (int i=2;i<=maxn;i++){
		if (i>a[now] && now<tot) now++;
		if (use[i] && (i!=a[now])) continue;
		int lim=i>>1; lim++;
//		if (i==19){
//			cout << "ytx" << endl;
//		}
//		if (i==a[now]) cout << lim << "  lim" <<  endl;
		for (int j=2;j<=lim;j++){
			if (use[j] && use[i-j]){
				use[i]=true;
				if (i==a[now]) res++,now++;
			}
		}
	}
	printf("%d\n",tot-res);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		read();
		solve();
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
