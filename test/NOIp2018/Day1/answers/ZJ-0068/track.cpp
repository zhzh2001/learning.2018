#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
using namespace std;
int read(){
	int ans=0;
	char ch=getchar();
	while(ch<'0'||ch>'9')
		ch=getchar();
	while(ch<='9'&&ch>='0'){
		ans=ans*10+ch-'0';
		ch=getchar();
	}
	return ans;
}
int n,m,en,vet[100005],Next[100005],head[50005],val[100005],Max,now,L,R,MID;
int l,r,mid,f[50005],g[50005],a[50005],cnt,last;
void addedge(int u,int v,int VAL){
	Next[++en]=head[u];
	head[u]=en;
	vet[en]=v;
	val[en]=VAL;
}
void dfs(int u,int pre){
	f[u]=0;
	g[u]=0;
	for(int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if(v!=pre)
			dfs(v,u);
	}
	cnt=0;
	for(int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if(v!=pre){
			f[u]+=f[v];
			if(g[v]+val[i]<mid)
				a[++cnt]=g[v]+val[i];
			else
				f[u]++;
		}
	}
	sort(a+1,a+1+cnt);
	last=cnt;
	Max=0;
	for(int i=1;i<=cnt;i++)
		if(i<last){
			if(a[i]+a[last]>=mid){
				Max++;
				last--;
			}
		}
		else
			break;
	f[u]+=Max;
	L=0;
	R=cnt;
	while(L<R){
		MID=L+R+1>>1;
		now=0;
		last=cnt;
		if(last==MID)
			last--;
		for(int i=1;i<=cnt;i++)
			if(i<last){
				if(i!=MID&&a[i]+a[last]>=mid){
					now++;
					last--;
					if(last==MID)
						last--;
				}
			}
			else
				break;
		if(now==Max)
			L=MID;
		else
			R=MID-1;
	}
	g[u]=a[L];
}
bool check(){
	dfs(1,0);
	return f[1]>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read(),z=read();
		r+=z;
		addedge(x,y,z);
		addedge(y,x,z);
	}
	l=0;
	while(l<r){
		mid=l+r+1>>1;
		if(check())
			l=mid;
		else
			r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
