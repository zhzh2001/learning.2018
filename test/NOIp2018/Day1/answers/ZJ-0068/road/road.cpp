#include<cstdio>
#include<cmath>
#include<cstdlib>
#include<cstring>
#include<algorithm>
using namespace std;
int read(){
	int ans=0;
	char ch=getchar();
	while(ch<'0'||ch>'9')
		ch=getchar();
	while(ch<='9'&&ch>='0'){
		ans=ans*10+ch-'0';
		ch=getchar();
	}
	return ans;
}
int n,a[100005],ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		if(a[i]>a[i-1])
			ans+=a[i]-a[i-1];
	}
	printf("%d\n",ans);
	return 0;
}
