#include<cstdio>
#include<cmath>
#include<cstdlib>
#include<cstring>
#include<algorithm>
using namespace std;
int T,n,a[105],ans;
bool f[25005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	f[0]=true;
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		for(int i=1;i<=n;i++)
			if(!f[a[i]]){
				ans++;
				for(int j=a[i];j<=a[n];j++)
					f[j]|=f[j-a[i]];
			}
		printf("%d\n",ans);
		ans=0;
		for(int i=1;i<=a[n];i++)
			f[i]=false;
	}
	return 0;
}
