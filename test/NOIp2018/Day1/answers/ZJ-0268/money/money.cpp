#include<bits/stdc++.h>
#define M 105
using namespace std;
bool mmm1;
void Read(int &x){
	char ch=getchar();
	x=0;
	bool f=0;
	while(ch<'0'||ch>'9'){if(ch=='-')f=1;ch=getchar();}
	while('0'<=ch&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if(f)x=-x;
}
int A[M];
struct p100{
	bool dp[25005];
	void solve(int n){
		sort(A+1,A+n+1);
		memset(dp,0,sizeof(dp));
		dp[0]=1;
		int ans=n,mx=0;
		for(int i=1;i<=n;i++)mx=max(A[i],mx);
		for(int i=1;i<=n;i++){
			if(dp[A[i]]){
				ans--;
				continue;
			}
			for(int j=0;j<=mx-A[i];j++){
				if(dp[j]){
					dp[j+A[i]]=1;
					//printf("Add:%d\n",j+A[i]);
				}
			}
		}
		printf("%d\n",ans);
	}
}P100;
bool mmm2;
int main(){
	//printf("%lf\n",(&mmm2-&mmm1)/1024.0/1024.0);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	Read(T);
	while(T--){
		int n;
		Read(n);
		for(int i=1;i<=n;i++)Read(A[i]);
		P100.solve(n);
	}
	return 0;
}
