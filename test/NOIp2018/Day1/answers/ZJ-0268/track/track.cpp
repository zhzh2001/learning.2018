#include<bits/stdc++.h>
#define M 50005
using namespace std;
bool mmm1;
void Read(int &x){
	char ch=getchar();
	x=0;
	bool f=0;
	while(ch<'0'||ch>'9'){if(ch=='-')f=1;ch=getchar();}
	while('0'<=ch&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if(f)x=-x;
}
struct E{
	int to,nx,d;
}edge[M<<1];
int tot,head[M];
void Addedge(int a,int b,int d){
	edge[++tot].to=b;
	edge[tot].nx=head[a];
	edge[tot].d=d;
	head[a]=tot;
}
struct p20{
	int fa[M],ans,Mx[M][2];
	void dfs(int now){
		Mx[now][0]=Mx[now][1]=0;
		for(int i=head[now];i;i=edge[i].nx){
			int nxt=edge[i].to;
			if(nxt==fa[now])continue;
			fa[nxt]=now;
			dfs(nxt);
			int nxtd=Mx[nxt][0]+edge[i].d;
			if(nxtd>Mx[now][0])Mx[now][1]=Mx[now][0],Mx[now][0]=nxtd;
			else if(nxtd>Mx[now][1])Mx[now][1]=nxtd;
		}
		ans=max(ans,Mx[now][0]+Mx[now][1]);
	}
	void solve(){
		ans=0,fa[1]=0;
		dfs(1);
		printf("%d\n",ans);
	}
}P20;
struct Lin{
	int a,b,d;
	bool operator <(const Lin &x)const{
		return d>x.d;
	}
}Line[M];
struct p15{
	int n,m;
	int dis[M];
	bool check(int n){
		memset(dis,0,sizeof(dis));
		for(int i=1;i<n;i++){
			if(Line[i].b!=Line[i].a+1)return false;
			dis[Line[i].a]=Line[i].d;
		}
		return true;
	}
	bool is_ok(int x){
		int cnt=0,res=0;
		for(int i=1;i<n;i++){
			res+=dis[i];
			if(res>=x)cnt++,res=0;
		}
		return cnt>=m;
	}
	void solve(int n,int m){
		this->m=m,this->n=n;
		int L=0,R=1e9,ans=0;
		while(L<=R){
			int mid=(0ll+L+R)>>1;
			if(is_ok(mid)){
				L=mid+1;
				ans=mid;
			}else R=mid-1;
		}
		printf("%d\n",ans);
	}
}P15;
struct p5{
	bool check(int n){
		for(int i=1;i<n;i++)if(Line[i].a!=1)return false;
		return true;
	}
	void solve(int n,int m){
		sort(Line+1,Line+n);
		printf("%d\n",Line[m].d);
	}
}P5;
struct p00{
	int fa[M],ID[M],T,P[M];
	void dfs(int now){
		ID[now]=++T;
		P[T]=now;
		for(int i=head[now];i;i=edge[i].nx){
			int nxt=edge[i].to;
			if(nxt==fa[now])continue;
			fa[nxt]=now;
			dfs(nxt);
		}
	}
	int Mx[M][3],n,m;
	bool is_ok(int x){
		int cnt=0;
		memset(Mx,0,sizeof(Mx));
		for(int i=n;i>=1;i--){
			int now=P[i];
			Mx[now][0]=Mx[now][1]=Mx[now][2]=0;
			for(int i=head[now];i;i=edge[i].nx){
				int nxt=edge[i].to;
				if(nxt==fa[now])continue;
				int nxtd=Mx[nxt][0]+edge[i].d;
				if(nxtd>Mx[now][0])Mx[now][2]=Mx[now][1],Mx[now][1]=Mx[now][0],Mx[now][0]=nxtd;
				else if(nxtd>Mx[now][1])Mx[now][2]=Mx[now][1],Mx[now][1]=nxtd;
				else if(nxtd>Mx[now][2])Mx[now][2]=nxtd;
			}
			if(Mx[now][2]+Mx[now][1]>=x)cnt++;
			else if(Mx[now][0]+Mx[now][2]>=x)cnt++,Mx[now][0]=Mx[now][1];
			else if(Mx[now][0]+Mx[now][1]>=x)cnt++,Mx[now][0]=Mx[now][2];
		}
		return cnt>=m;
	}
	void solve(int n,int m){
		T=0,fa[1]=0;
		dfs(1);
		this->m=m,this->n=n;
		int L=0,R=1e9,ans=0;
		while(L<=R){
			int mid=(0ll+L+R)>>1;
			if(is_ok(mid)){
				L=mid+1;
				ans=mid;
			}else R=mid-1;
		}
		printf("%d\n",ans);
	}
}P00;
bool mmm2;
int main(){
	//printf("%lf\n",(&mmm2-&mmm1)/1024.0/1024.0);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n,m;
	Read(n),Read(m);
	for(int i=1;i<n;i++){
		int a,b,d;
		Read(a),Read(b),Read(d);
		Line[i].a=a,Line[i].b=b,Line[i].d=d;
		Addedge(a,b,d);
		Addedge(b,a,d);
	}
	if(P5.check(n)||m==n-1)P5.solve(n,m);
	else if(P15.check(n))P15.solve(n,m);
	else if(m==1)P20.solve();
	else P00.solve(n,m);
	return 0;
}
