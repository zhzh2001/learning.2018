#include<bits/stdc++.h>
#define M 100005
using namespace std;
bool mmm1;
int A[M];
struct p70{
	void solve(int n){
		int ans=0,mx=0;
		for(int i=1;i<=n;i++)mx=max(mx,A[i]);
		for(int i=1;i<=mx;i++){
			int cnt=0;
			bool f=0;
			for(int j=1;j<=n;j++){
				if(A[j]<i){
					if(f)cnt++;
					f=0;
				}
				else f=1;
			}
			if(f)cnt++;
			ans+=cnt;
		}
		printf("%d\n",ans);
	}
}P70;
struct p100{
	int stk[M],L[M],R[M];
	void solve(int n){
		int top=0;
		stk[0]=0;
		A[0]=0;
		for(int i=1;i<=n;i++){
			while(top&&A[stk[top]]>=A[i])top--;
			L[i]=stk[top];
			stk[++top]=i;
		}
		top=0;
		for(int i=n;i>=1;i--){
			while(top&&A[stk[top]]>A[i])top--;
			R[i]=stk[top];
			stk[++top]=i;
		}
		//for(int i=1;i<=n;i++)printf("L[%d]=%d R[%d]=%d\n",i,L[i],i,R[i]);
		long long ans=0;
		for(int i=1;i<=n;i++){
			ans+=min(A[i]-A[L[i]],A[i]-A[R[i]]);
		}
		printf("%lld\n",ans);
	}
}P100;
bool mmm2;
int main(){
	//printf("%lf\n",(&mmm2-&mmm1)/1024.0/1024.0);
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n;
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&A[i]);
	//P100.solve(n);
	//return 0;
	if(n<=1000)P70.solve(n);
	else P100.solve(n);
	return 0;
}
