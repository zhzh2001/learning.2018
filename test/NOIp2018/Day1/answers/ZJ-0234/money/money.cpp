#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int i,j,k,n,m,t,s1,s2,ma,flag,ans,fin,tot;
int org[100],a[100],f[25005],b[100],c[100],vl[100];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		tot=0;
		scanf("%d",&n);
		for (i=1;i<=n;++i)
			scanf("%d",&org[i]);
		for (i=1;i<=n;++i)
			vl[i]=1;
		for (i=1;i<=n;++i)
			for (j=1;j<=n;++j)
				if (i!=j&&(org[j]%org[i])==0)
					vl[j]=0;
		for (i=1;i<=n;++i)
			if (vl[i])
				a[++tot]=org[i];
		n=tot;
		fin=n;
		for (i=0;i<1<<n;++i)
		{
			s1=s2=0;
			ma=ans=0;
			for (j=0;j<n;++j)
				if (i&(1<<j))
				{
					b[++s1]=a[j+1];
					++ans;
				}
				else
				{
					c[++s2]=a[j+1];
					ma=max(ma,a[j+1]);
				}
			if (ans>=fin)
				continue;
			memset(f,0,sizeof(f));
			f[0]=1;
			for (j=1;j<=s1;++j)
				for (k=b[j];k<=ma;++k)
					f[k]|=f[k-b[j]];
			flag=1;
			for (j=1;j<=s2;++j)
				flag&=f[c[j]];
			if (flag)
				fin=min(ans,fin);
		}
		printf("%d\n",fin);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
