#include<cstdio>
#include<algorithm>
using namespace std;
const int N=100005;
int i,j,k,n,now,ans,ma,tot,q;
int a[N],h[N],l[N];
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;++i)
	{
		scanf("%d",&q);
		ma=max(ma,q);
		if (q!=a[tot])
			a[++tot]=q;
	}
	n=tot;
	for (i=1;i<=n;++i)
	{
		if (a[i]>a[i-1]&&a[i]>a[i+1])
			++h[a[i]];
		if (a[i]<a[i-1]&&a[i]<a[i+1])
			++l[a[i]];
		if (a[i]&&!a[i-1])
			++now;
	}
	for (i=1;i<=ma;++i)
	{
		ans+=now;
		now+=l[i];
		now-=h[i];
	}
	printf("%d",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
