#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=50005;
struct nod
{
	int u,v,w;
}a[N];
int i,j,k,n,m,l,r,mid,u,v,w,ma1,ma2,tot,sum;
int pre[N<<1],las[N],des[N<<1],wei[N<<1],dis[N];
bool cmp(nod x,nod y)
{
	return x.u<y.u;
}
void link(int x,int y,int z)
{
	pre[++tot]=las[x],las[x]=tot;
	des[tot]=y,wei[tot]=z;
	pre[++tot]=las[y],las[y]=tot;
	des[tot]=x,wei[tot]=z;
}
void dfs(int x)
{
	for (int i=las[x];i;i=pre[i])
		if (!dis[des[i]])
		{
			dis[des[i]]=dis[x]+wei[i];
			dfs(des[i]);
		}
}
int can(int x)
{
	int now=0,cnt=0;
	for (int i=1;i<n;++i)
	{
		now+=a[i].w;
		if (now>=x)
			now=0,++cnt;
	}
	return cnt>=m;
}
void work1()
{
	for (i=1;i<n;++i)
	{
		scanf("%d%d%d",&u,&v,&w);
		link(u,v,w);
	}
	memset(dis,0,sizeof(dis));
	dis[1]=1;
	dfs(1);
	ma1=0;
	for (i=1;i<=n;++i)
		if (dis[i]>ma1)
		{
			ma1=dis[i];
			ma2=i;
		}
	memset(dis,0,sizeof(dis));
	dis[ma2]=1;
	dfs(ma2);
	ma1=0;
	for (i=1;i<=n;++i)
		ma1=max(ma1,dis[i]);
	printf("%d",ma1-1);
}
void work2()
{
	for (i=1;i<n;++i)
	{
		scanf("%d%d%d",&a[i].u,&a[i].v,&a[i].w);
		sum+=a[i].w;
	}
	sort(a+1,a+n,cmp);
	l=0,r=sum+1;
	while (l<r)
	{
		mid=(l+r>>1)+1;
		if (can(mid))
			l=mid;
		else
			r=mid-1;
	}
	printf("%d",l);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m==1)
		work1();
	else
		work2();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
