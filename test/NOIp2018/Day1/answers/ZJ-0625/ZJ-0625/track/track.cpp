#include<bits/stdc++.h>
using namespace std;
//�ڴ�  �ļ���  
#define M 50005
#define ll long long
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),!isdigit(c));
	do res=(res<<3)+(res<<1)+(c^48);
	while(c=getchar(),isdigit(c));	
}
void Max(int &a,int b){
	if(a<b)a=b;	
}
int n,m;
int head[M],a[M],b[M],c[M],cnt[M],tot;
struct node{
	int to,len,nxt;	
}edge[M<<1];
inline void Add(int a,int b,int c){
	edge[tot].to=b;edge[tot].len=c;edge[tot].nxt=head[a];head[a]=tot++;	
}
struct P20{
	int Root,ans;
	void dfs(int x,int pre,int len){
		if(len>ans){ans=len;Root=x;}
		for(int i=head[x];~i;i=edge[i].nxt){
			int y=edge[i].to;
			if(y==pre)continue;
			dfs(y,x,len+edge[i].len);	
		}
	}
	void rdfs(int x,int pre,int len){
		if(len>ans)ans=len;
		for(int i=head[x];~i;i=edge[i].nxt){
			int y=edge[i].to;
			if(y==pre)continue;
			rdfs(y,x,len+edge[i].len);	
		}
	}
	void solve(){
		ans=0;
		dfs(1,-1,0);
		rdfs(Root,-1,0);
		printf("%d\n",ans);
	}
}p20;
struct P35{
	bool check(int len){
		int Use=0,res=0;
		for(int i=n;i>=1;i--){
			int j=lower_bound(c+1,c+n+1,len-c[i])-c;
			if(j<=Use)j=Use+1;Use=j;
			if(i<=j)break;
			res++;
		}
		return res>=m;
	}
	void solve(){
		sort(c+1,c+n+1);c[0]=0;c[n+1]=1e9;
		int sum=0;
		for(int i=1;i<=n;i++)sum+=c[i];
		int l=1,r=sum,res=-1;
		while(l<=r){
			int mid=l+r>>1;
			if(check(mid))l=mid+1,res=mid;
			else r=mid-1;
		}
		printf("%d\n",res);
	}
}p35;
struct P55{
	int len[M];
	bool check(int x){
		int sum=0,res=0;
		for(int i=1;i<n;i++){
			sum+=len[i];
			if(sum>=x)sum=0,res++;
		}
		return res>=m;
	}
	void solve(){
		for(int i=1;i<n;i++)len[a[i]]=c[i];
		int sum=0;
		for(int i=1;i<=n;i++)sum+=c[i];
		int l=1,r=sum,res=-1;
		while(l<=r){
			int mid=l+r>>1;
			if(check(mid))l=mid+1,res=mid;
			else r=mid-1;
		}
		printf("%d\n",res);
	}
}p55;
struct P80{
	#define S 1005
	int Root,dp[S],fa[S],dis[S],falen[S],Lson[S],Llen[S],Rson[S],Rlen[S],Bro[S],mark[S],mp[S],l[S],r[S],TT;
	void dfs(int x,int pre,int d){
		dis[x]=d;l[x]=++TT;mp[TT]=x;
		for(int i=head[x];~i;i=edge[i].nxt){
			int y=edge[i].to;
			if(y==pre)continue;
			if(!Lson[x]){
				Lson[x]=y;fa[y]=x;
				Llen[x]=falen[y]=edge[i].len;
			}
			else {
				Rson[x]=y;fa[y]=x;
				Rlen[x]=falen[y]=edge[i].len;
			}
			dfs(y,x,d+edge[i].len);
		}
		r[x]=TT;
		if(Rson[x]){Bro[Lson[x]]=Rson[x];Bro[Rson[x]]=Lson[x];}
	}
	int Bianli1(int x,int len,int Need,int Now){
		if(!Lson[x])return 0;
		if(Bro[x]){rdfs(Bro[x],Need);Now+=dp[Bro[x]];}
		if(len>=Need){
			rdfs(x,Need);
			Now+=dp[x]+1;
			return Now;
		}
		int res=Bianli1(Lson[x],len+Llen[x],Need,Now);
		Max(res,Bianli1(Rson[x],len+Rlen[x],Need,Now));
	}
	void rdfs(int x,int Need){
		if(mark[x]==1)return;
		mark[x]=1;
		dp[x]=Bianli1(x,falen[x],Need,0);
		if(Rson[x]){
			for(int i=l[Lson[x]];i<=r[Lson[x]];i++){
				int p=mp[i],len=0,res=0;
				rdfs(p,Need);res+=dp[p];
				while(fa[p]!=x){
					len+=falen[p];
					rdfs(Bro[p],Need);res+=dp[Bro[p]];
					p=fa[p];
				}
				len+=falen[p];int tmp1=len,tmp2=res;
				for(int j=l[Rson[x]];j<=r[Rson[x]];j++){
					p=mp[j];
					rdfs(p,Need);res+=dp[p];
					while(fa[p]!=x){
						len+=falen[p];
						rdfs(Bro[p],Need);res+=dp[Bro[p]];
						p=fa[p];
					}
					len+=falen[p];
					if(len>=Need)res++;
					Max(dp[x],res);
					len=tmp1;res=tmp2;
				}
			}
		}
	}
	bool check(int Need){
		rdfs(Root,Need);
		return dp[Root]>=m;
	}
	void solve(){
		TT=0;
		for(int i=1;i<=n;i++)if(cnt[i]<3){Root=i;break;}
		dfs(Root,-1,0);fa[Root]=0;falen[Root]=0;
//		printf("%d\n",Root);printf("%d\n",dis[3]);
		dis[0]=0;Lson[0]=Root;Llen[0]=0;Bro[Root]=0;
		int sum=0;
		for(int i=1;i<=n;i++)sum+=c[i];
		int l=1,r=sum,res=-1;
		while(l<=r){
			memset(dp,0,sizeof(dp));
			memset(mark,0,sizeof(mark));
			int mid=l+r>>1;
			if(check(mid))l=mid+1,res=mid;
			else r=mid-1;
		}
		printf("%d\n",res);
	}
}p80;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d %d",&n,&m);
	int flag1=1,flag2=1,flag3=1;
	memset(head,-1,sizeof(head));
	for(int i=1;i<n;i++){
		scanf("%d %d %d",&a[i],&b[i],&c[i]);
		Add(a[i],b[i],c[i]);Add(b[i],a[i],c[i]);
		cnt[a[i]]++;cnt[b[i]]++;
		flag1&=(a[i]==1);flag2&=(b[i]==a[i]+1);
	}
	for(int i=1;i<=n;i++)flag3&=(cnt[i]<=3);
	if(m==1)p20.solve();
	else if(flag1)p35.solve();
	else if(flag2)p55.solve();
	else if(flag3)p80.solve();
	return 0;
}
