var
  n,i,j,ans,min,s:int64;
  k:longint;
  a:array[0..100005] of int64;
begin
  assign(input,'road.in');
  reset(input);
  assign(output,'road.out');
  rewrite(output);
  readln(n);
  for k:=1 to n do
   begin
    read(a[k]);
    s:=s+a[k];
   end;
  while s>0 do
   begin
    i:=1;
    while (a[i]=0) and (i<=n) do inc(i);
    if i>n then break;
    j:=i;
    min:=maxlongint;
    while (a[j]>0) and (j<=n) do
     begin
      if a[j]<min then min:=a[j];
      inc(j);
     end;
    for k:=i to j-1 do
     begin
      a[k]:=a[k]-min;
      s:=s-min;
     end;
    inc(ans,min);
   end;
  writeln(ans);
  close(input);
  close(output);
end.
