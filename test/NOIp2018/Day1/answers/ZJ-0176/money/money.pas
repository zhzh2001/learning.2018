var
  t,n,i,j,ans:longint;
  a:array[0..105] of int64;
  flag:array[0..105] of boolean;
procedure qsort(l,r:longint);
 var p,q,t,mid:longint;
 begin
   if l>=r then exit;
   p:=l;q:=r;mid:=a[(l+r) div 2];
   while p<=q do
    begin
     while (p<r) and (a[p]<mid) do inc(p);
     while (q>l) and (a[q]>mid) do dec(q);
     if p<=q then
      begin
       t:=a[p];a[p]:=a[q];a[q]:=t;
       inc(p);dec(q);
      end;
    end;
   qsort(l,q);qsort(p,r);
 end;

procedure try(k,t,p:longint);
 var q:longint;
 begin
   if t>a[p] then exit;
   if t=a[p] then begin flag[p]:=true;exit;end;
   if k=p then exit;
   q:=0;
   while t+a[k]*q<=a[p] do
    begin
     try(k+1,t+a[k]*q,p);
     inc(q);
    end;
 end;

begin
  assign(input,'money.in');
  reset(input);
  assign(output,'money.out');
  rewrite(output);
  readln(t);
  for i:=1 to t do
   begin
    readln(n);
    fillchar(a,sizeof(a),0);
    for j:=1 to n do read(a[j]);
    qsort(1,n);
    fillchar(flag,sizeof(flag),false);
    for j:=2 to n do try(1,0,j);
    ans:=0;
    for j:=1 to n do
     if not flag[j] then inc(ans);
    writeln(ans);
   end;
  close(input);
  close(output);
end.
