var
 n,m,x,y,z:int64;
 i:longint;
 f,w:array[0..50005,0..2] of longint;
 ff:boolean;
 num:array[0..50005] of longint;
function can(k:longint):boolean;
 var i,t,tt:int64;
 begin
  i:=1;tt:=1;
  while tt<=m do
   begin
    t:=w[i,1];
    while t<k do
     begin
      inc(i);
      if i>n-1 then exit(false);
      t:=t+w[i,1];
     end;
    inc(i);inc(tt);
    if (i>n-1) and (tt<m) then exit(false);
   end;
  exit(true);
 end;

procedure try1;
 var
  i:longint;
  ans,mid,s,l,r:int64;
 begin
   s:=0;
   for i:=1 to n-1 do s:=s+w[i,1];
   l:=0;r:=s;
   while l<=r do
    begin
     mid:=(l+r) div 2;
     if can(mid) then begin ans:=mid;l:=mid+1;end
      else r:=mid-1;
    end;
   writeln(ans);
 end;

begin
  assign(input,'track.in');
  reset(input);
  assign(output,'track.out');
  rewrite(output);
  readln(n,m);
  for i:=1 to n-1 do
   begin
    readln(x,y,z);
    inc(num[x]);
    f[x,num[x]]:=y;
    w[x,num[x]]:=z;
   end;
  for i:=1 to n do if num[i]>1 then begin ff:=true;break;end;
  if not ff then try1 else
   writeln(15);
  close(input);
  close(output);
end.
