#include<bits/stdc++.h>
#define fo(i,j,k) for(int i=(j);i<=(k);i++)
#define fd(i,j,k) for(int i=(j);i>=(k);i--)
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
using namespace std;
const int inf=600000008,N=60008;
int n,t,w,m,tot,head[N<<1],b[N],mid,f[N],g[N],use[N],dis[N];
struct edge{int u,v,val;}a[N];
struct Edge{int nxt,u,v,toto,val;}G[N<<1];
vector <int> son[N<<1];
void read(int& x){
	int bb=1;char ch=getchar();
	for(;ch<'0' || ch>'9';ch=getchar()) if (ch=='-') bb=-1;
	for(x=0;ch>='0' && ch<='9';x=x*10+ch-'0',ch=getchar());
	x=x*bb;
}
void add(int u,int v,int val){
	G[++tot].nxt=head[u];head[u]=tot;G[tot].toto=v;G[tot].val=val;
}
int pd(int x){
	int l=1,r=n-1,ans=0,sum=0;
	while(l<r){
	    ans=b[r];
	    if (ans<x){
			while(ans+b[l]<x) l++;l++;}
		if (ans+b[l]<x) break;
		sum++;r--;
		if (sum>=m) return true;
	} 
	return false;
}
bool cmp(int x,int y){return x>y;}
void dfs(int x,int fa){
	g[x]=0;f[x]=0;
	for(int i=head[x];i;i=G[i].nxt){
		int v=G[i].toto;
		if (v==fa) continue;
		dfs(v,x);f[x]+=f[v];
		if (g[v]+G[i].val>=mid) f[x]++;else
		son[x].push_back(v);g[v]+=G[i].val;
	}
	sort(son[x].begin(),son[x].end(),cmp);
	int L=0,R=son[x].size()-1;
	while(L<R){
		while(R-1>L && son[x][L]+g[son[x][R-1]]>=mid) R--;
		if (g[son[x][L]]+g[son[x][R]]>=mid) f[x]++;
		use[son[x][L]]=use[son[x][R]]+1;
		L++;
	}
	for(int i=0;i<=son[x].size()-1;i++){
		if (!use[son[x][i]]) g[x]=g[son[x][i]];
		use[son[x][i]]=0;
	}
	son[x].clear();	
}
void spfa(int x){
	fo(i,1,n) {dis[i]=0;use[i]=0;}
	t=1;w=1;b[t]=x;use[x]=1;
	while(t<=w){
		int u=b[t];
		for(int i=head[u];i;i=G[i].nxt){
			int v=G[i].toto;
			if (use[v]==0 && dis[v]<dis[u]+G[i].val){
				dis[v]=dis[u]+G[i].val;
				use[v]=1;w++;b[w]=v;
			}
		}
		t++;
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);
	bool ff=true;
	fo(i,1,n-1){
	  read(a[i].u);read(a[i].v);read(a[i].val);
	  if (ff && a[i].u!=1 && a[i].v!=1) ff=false;
	}
	if (ff==true){
	  fo(i,1,n-1) {b[a[i].u*a[i].v-1]=a[i].val;w+=b[i];}
	  sort(b+1,b+n);
	  t=1;w=w+1;
	  while(t<w){
	  	int mid=(t+w)/2;
	  	if (pd(mid)) t=mid+1;else w=mid;
	  }
	  printf("%d\n",t-1);	
	  return 0;
	}
	fo(i,1,n-1){
		add(a[i].u,a[i].v,a[i].val);add(a[i].v,a[i].u,a[i].val);
	}
	if (m==1){
		int maxx=0;
		fo(k,1,n){
			spfa(k);
			fo(j,1,n) maxx=max(dis[j],maxx);
		}
		printf("%d\n",maxx);
		return 0;
	}
	int sum=0;
	fo(i,1,n-1) sum+=a[i].val;
//	t=1;w=inf;
//	while(t<w){
//	  mid=(t+w)/2;
//	  dfs(1,0);
//	  if (f[1]>=m) t=mid+1;else w=mid;	
//	}
	printf("%d\n",sum/m-1);
	return 0;
}
