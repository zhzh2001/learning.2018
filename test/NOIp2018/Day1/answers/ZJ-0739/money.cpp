#include<bits/stdc++.h>
#define fo(i,j,k) for(int i=(j);i<=(k);i++)
#define fd(i,j,k) for(int i=(j);i>=(k);i--)
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
using namespace std;
const int NN=25008,N=1008;
int f[NN],a[N],T,n,ans;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		fo(i,1,n) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		memset(f,false,sizeof(f));
		f[0]=true;ans=n;
		fo(i,1,n){
			if (f[a[i]]) ans--;
			fo(j,0,25000-a[i])
			 if (f[j]) f[j+a[i]]=true;
		}
		printf("%d\n",ans);
	}
	return 0;	
}
