#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define dep(i,r,l) for(int i=r;i>=l;i--)
#define fy 20021218
#define N 100010
using namespace std;
int n,m,i,j,k,l,r,T,ans;
int a[N],boo[N];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	rep(t,1,T)
	{
		scanf("%d",&n); m=0; ans=0;
		rep(i,1,n) 
		{
			scanf("%d",&a[i]);
			if(a[i]>m) m=a[i];
		}
		sort(a+1,a+n+1);
		rep(i,1,m) boo[i]=0; boo[0]=1;
		rep(i,1,n)
		{
			if(!boo[a[i]])
			{
				ans++;
				rep(j,0,m-a[i])
				{
					if(boo[j]) boo[j+a[i]]=1;
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
