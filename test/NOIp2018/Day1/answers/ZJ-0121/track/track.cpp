#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define dep(i,r,l) for(int i=r;i>=l;i--)
#define fy 20021218
#define N 100010
using namespace std;
int n,m,i,j,k,l,r,num,x,y,mid;
int a[N],b[N],c[N],v[N],sum[N],ans[N],Q[N];
void add(int l,int r,int k)
{
	a[++num]=r; b[num]=c[l]; c[l]=num; v[num]=k;
	a[++num]=l; b[num]=c[r]; c[r]=num; v[num]=k;
}
void dfs(int k,int fa)
{
	LL n=0;
	for(int j=c[k];j;j=b[j])
	{
		if(a[j]!=fa)
		{
			dfs(a[j],k);
			ans[k]+=ans[a[j]];
		}
	}
	for(int j=c[k];j;j=b[j])
		if(a[j]!=fa) Q[++n]=sum[a[j]]+v[j];
	sort(Q+1,Q+n+1);
	int l=1,r=n,cnt=ans[k];
	while(l<=r)
	{
		while(Q[r]>=mid) ans[k]++,r--;
		while(l<=r&&Q[l]+Q[r]<mid) l++;
		if(l>=r) break;
		ans[k]++;
		l++; r--;
	}
	l=1; r=n;
	while(l<=r)
	{
		int middle=(l+r)/2,s=cnt;
		int i=1,j=n;
		while(i<=j)
		{
			while(Q[j]>=mid) 
			{
				if(j!=middle) s++;
				j--;
			}
			if(i==middle) i++;
			if(j==middle) j--;
			while(i<=j&&Q[i]+Q[j]<mid||i==middle) i++;
			if(i>=j) break;
			s++; i++; j--;
		}
		if(s==ans[k]) l=middle+1;
		else r=middle-1;
	}
	sum[k]=Q[l-1];
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m); num=0; l=fy; r=0;
	rep(i,1,n-1)
	{
		scanf("%d%d%d",&x,&y,&k);
		add(x,y,k);
		if(l>k) l=k; r+=k;
	}
	while(l<=r)
	{
		mid=(l+r)/2;
		rep(i,1,n) sum[i]=0,ans[i]=0;
		dfs(1,0);
		if(ans[1]>=m) l=mid+1;
		else r=mid-1;
	}
	printf("%d",l-1);
	return 0;
}
