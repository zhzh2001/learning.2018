#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define dep(i,r,l) for(int i=r;i>=l;i--)
#define fy 20021218
#define N 100010
using namespace std;
int n,m,i,j,k,l,r,num,ans;
int a[N],Q[N];
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	rep(i,1,n) scanf("%d",&a[i]);
	Q[1]=0; num=1; ans=0;
	rep(i,1,n+1)
	{
		if(a[i]>Q[num]) Q[++num]=a[i];
		else
		{
			ans+=Q[num]-a[i];
			while(a[i]>Q[num]) num--;
			Q[++num]=a[i];
		}
	}
	printf("%d",ans);
	return 0;
}
