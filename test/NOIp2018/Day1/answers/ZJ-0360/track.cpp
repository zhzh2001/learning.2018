#include<bits/stdc++.h>
using namespace std;
const int N=500000;
struct node{
	int to,next,d;
}e[N];
int head[N],fa[N],dep[N],depest[N],num[N],a[N],n,m,s,tot,cnt;
bool b[N];
void add(int x,int y,int z)
{
	e[++tot].to=y;
	e[tot].next=head[x];
	e[tot].d=z;
	head[x]=tot;
}
int gg(int x,int n,int bz)
{
	int l=1,r=n,mid;
	while (l<r)
	{
		mid=(l+r)/2;
		if (a[mid]+x>=bz)
			r=mid;
		else
			l=mid+1;
	}
	return l;
}
void dfs(int t,int bz)
{
	for (int i=head[t];i;i=e[i].next)
		if (e[i].to!=fa[t])
		{
			fa[e[i].to]=t;
			dep[e[i].to]=dep[t]+e[i].d;
			depest[e[i].to]=dep[e[i].to];
			dfs(e[i].to,bz);
			if (depest[e[i].to]-dep[t]>=bz)
			{
				depest[e[i].to]=dep[t];
				s++;
				num[t]--;
			}
			num[t]++;
			depest[t]=max(depest[t],depest[e[i].to]);
		}
	if (num[t]>1)
	{
		cnt=0;
		for (int i=head[t];i;i=e[i].next)
			if (e[i].to!=fa[t]&&depest[e[i].to]-dep[t]!=0)
				a[++cnt]=depest[e[i].to]-dep[t];
		sort(a+1,a+cnt+1);
		for (int i=1;i<=cnt;i++)
			b[i]=1;
		int k;
		for (int i=1;i<=cnt;i++)
		if (b[i])
		{
			k=gg(a[i],cnt,bz);
			if (a[k]+a[i]<bz)
				continue;
			while (!b[k]||k==i)
				k++;
			if (k<=cnt)
			{
				b[i]=0;
				b[k]=0;
				s++;
			}
		}
		depest[t]=dep[t];
		for (int i=cnt;i>=1;i--)
			if (b[i])
			{
				depest[t]=a[i]+dep[t];
				break;
			}
	}
}
bool pdd(int bz)
{
	memset(fa,0,sizeof(fa));
	memset(dep,0,sizeof(dep));
	memset(depest,0,sizeof(depest));
	memset(num,0,sizeof(num));
	s=0;
	dfs(1,bz);
	if (s>=m)
		return 1;
	return 0;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int l=0,r=0;
	for (int i=1;i<n;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		r+=z;
		add(x,y,z);
		add(y,x,z);
	}
	r=r/m;
	int mid;
	while (l<r)
	{
		mid=(l+r+1)/2;
		if (pdd(mid))
			l=mid;
		else
			r=mid-1;
	}
	printf("%d",l);
	return 0;
}
