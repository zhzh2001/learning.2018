#include<bits/stdc++.h>
using namespace std;
bool f[100005];
int n,a[100005];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		for (int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		memset(f,0,sizeof(f));
		f[0]=1;
		int ans=0;
		for (int i=1;i<=n;i++)
			if (!f[a[i]])
			{
				ans++;
				for (int j=0;j<=25000-a[i];j++)
					if (f[j])
						f[j+a[i]]=1;
			}
		printf("%d\n",ans);
	}
	return 0;
}
