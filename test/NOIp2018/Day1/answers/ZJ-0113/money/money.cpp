#include <cstdio>
#include <algorithm>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

const int INF = 1000000000;
const int _n = 100 + 10, _x = 25000 + 10;
int T;
int n;
int maxx;
int x[_n];
int f[_x];
int ans;

int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	T = read();
	while (T--)
	{
		n = read();
		maxx = -INF;
		for (int i = 1; i <= n; i++)
		{
			x[i] = read();
			maxx = std::max(maxx, x[i]);
		}
		std::sort(x + 1, x + n + 1);
		for (int i = 0; i <= maxx; i++)
		{
			f[i] = 0;
		}
		f[0] = 1;
		ans = 0;
		for (int i = 1; i <= n; i++)
		{
			if (! f[x[i]])
			{
				ans++;
				for (int j = x[i]; j <= maxx; j++)
				{
					f[j] = f[j] | f[j - x[i]];
				}
			}
		}
		printf("%d\n", ans);
	}
	return 0;
}

