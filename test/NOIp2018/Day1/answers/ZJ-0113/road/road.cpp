#include <cstdio>
#include <algorithm>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

struct node
{
	int x, pos;
};

const int _n = 100000 + 10;
int n;
node x[_n];
int flag[_n];
int cnt, ans;

bool comp(node x, node y)
{
	return x.x > y.x;
}

int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	n = read();
	for (int i = 1; i <= n; i++)
	{
		x[i].x = read();
		x[i].pos = i;
	}
	std::sort(x + 1, x + n + 1, comp);
	int now = 1;
	cnt = 0;
	for (int i = 10000; i >= 1; i--)
	{
		while (now <= n && x[now].x == i)
		{
			int pos = x[now].pos;
			cnt++;
			flag[pos] = 1;
			cnt -= flag[pos - 1];
			cnt -= flag[pos + 1];
			now++;
		}
		ans += cnt;
	}
	printf("%d\n", ans);
	return 0;
}
