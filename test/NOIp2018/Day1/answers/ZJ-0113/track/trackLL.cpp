#include <cstdio>
#include <algorithm>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

const int _n = 50000 + 10, _l = 200000 + 10, maxl = 200000;
int n, m;
int edgenum;
int vet[2 * _n], val[2 * _n], nextx[2 * _n], head[_n];
int f[_n];
int K;
int cnt;
int last;
int vv[_n], ff[_n];
int top;
int stack[_n];

void add(int u, int v, int cost)
{
	edgenum++;
	vet[edgenum] = v;
	val[edgenum] = cost;
	nextx[edgenum] = head[u];
	head[u] = edgenum;
}

void dfs(int u, int father)
{
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (v != father)
		{
			dfs(v, u);
		}
	}
	last = 0;
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		int cost = val[i];
		if (v != father)
		{
			if (f[v] + cost >= K)
			{
				cnt++;
			}
			else
			{
				last++;
				vv[last] = f[v] + cost;
			}
		}
	}
	std::sort(vv + 1, vv + last + 1);
	int nowl = 1, nowr = last;
	while (nowl < nowr + 1)
	{

}

bool check(int mid)
{
	K = mid;
	cnt = 0;
	dfs(1, 0);
//	printf("G%d %d\n", mid, cnt);
	return cnt >= m;
}

int main()
{
	n = read();
	m = read();
	int sum = 0;
	for (int i = 1; i < n; i++)
	{
		int u, v, cost;
		u = read();
		v = read();
		cost = read();
		add(u, v, cost);
		add(v, u, cost);
		sum += cost;
	}
	int l = 0, r = sum;
	while (l < r)
	{
//		printf("%d %d\n", l, r);
		int mid = (l + r + 1) >> 1;
		if (check(mid))
		{
			l = mid;
		}
		else
		{
			r = mid - 1;
		}
	}
	printf("%d\n", l);
//	check(27226);
	return 0;
}

