#include <cstdio>
#include <algorithm>

const int _n = 50000 + 10, _l = 200000 + 10, maxl = 200000;
int n, m;
int edgenum;
int vet[2 * _n], val[2 * _n], nextx[2 * _n], head[_n];
int f[_n];
int ans;

void add(int u, int v, int cost)
{
	edgenum++;
	vet[edgenum] = v;
	val[edgenum] = cost;
	nextx[edgenum] = head[u];
	head[u] = edgenum;
}

void dfs(int u, int father)
{
	int maxx = 0, maxy = 0;
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		int cost = val[i];
		if (v != father)
		{
			dfs(v, u);
			if (f[v] + cost > maxx)
			{
				maxy = maxx;
				maxx = f[v] + cost;
			}
			else
			{
				if (f[v] + cost > maxy)
				{
					maxy = f[v] + cost;
				}
			}
		}
	}
	ans = std::max(ans, maxx + maxy);
	f[u] = maxx;
}

int main()
{
	scanf("%d %d", &n, &m);
	for (int i = 1; i < n; i++)
	{
		int u, v, cost;
		scanf("%d%d%d", &u, &v, &cost);
		add(u, v, cost);
		add(v, u, cost);
	}
	ans = 0;
	dfs(1, 0);
	printf("%d\n", ans);
	return 0;
}
