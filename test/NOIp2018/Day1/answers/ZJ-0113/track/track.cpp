#include <cstdio>
#include <algorithm>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

const int _n = 50000 + 10;
int n, m;
int edgenum;
int vet[2 * _n], val[2 * _n], nextx[2 * _n], head[_n];
int f[_n];
int K;
int cnt;
int last;
int vv[_n], ff[_n];
int top;
int stack[_n];

void add(int u, int v, int cost)
{
	edgenum++;
	vet[edgenum] = v;
	val[edgenum] = cost;
	nextx[edgenum] = head[u];
	head[u] = edgenum;
}

void dfs(int u, int father)
{
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (v != father)
		{
			dfs(v, u);
		}
	}
	last = 0;
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		int cost = val[i];
		if (v != father)
		{
			if (f[v] + cost >= K)
			{
				cnt++;
			}
			else
			{
				last++;
				vv[last] = f[v] + cost;
			}
		}
	}
	std::sort(vv + 1, vv + last + 1);
	int mm = last + 1;
	for (int i = 1; i <= last; i++)
	{
		ff[i] = 1;
	}
	for (int i = 1; i <= last; i++)
	{
		if (vv[i] >= (K + 1) / 2)
		{
			mm = i;
			break;
		}
	}
	int tmp = 0;
	int nowl = mm - 1;
	int nowr = mm;
	top = 0;
	while (nowr <= last)
	{
		while (nowl > 0 && vv[nowl] + vv[nowr] >= K)
		{
			top++;
			stack[top] = nowl;
			nowl--;
		}
		if (top > 0)
		{
			cnt++;
			tmp++;
			ff[nowr] = 0;
			ff[stack[top]] = 0;
			top--;
		}
		nowr++;
	}
	int zz = 0;
	for (int i = mm; i <= last; i++)
	{
		if (ff[i])
		{
			zz++;
		}
	}
	int tt = tmp;
	cnt += zz / 2;
	tmp += zz / 2;
	zz = (zz / 2) * 2;
	for (int i = mm; i <= last; i++)
	{
		if (ff[i] && zz)
		{
			ff[i] = 0;
			zz--;
		}
	}
	f[u] = 0;
	for (int i = last; i >= 1; i--)
	{
		if (ff[i])
		{
			f[u] = vv[i];
			break;
		}
	}
}

bool check(int mid)
{
	K = mid;
	cnt = 0;
	dfs(1, 0);
	return cnt >= m;
}

int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	n = read();
	m = read();
	int sum = 0;
	for (int i = 1; i < n; i++)
	{
		int u, v, cost;
		u = read();
		v = read();
		cost = read();
		add(u, v, cost);
		add(v, u, cost);
		sum += cost;
	}
	int l = 0, r = sum;
	while (l < r)
	{
		int mid = (l + r + 1) >> 1;
		if (check(mid))
		{
			l = mid;
		}
		else
		{
			r = mid - 1;
		}
	}
	printf("%d\n", l);
	return 0;
}

