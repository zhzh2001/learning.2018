#include <cstdio>
#include <algorithm>

const int _n = 50000 + 10, _l = 200000 + 10, maxl = 200000;
int n, m;
int uuu[_n], vvv[_n], ccc[_n];
int f[_n];
int last[1000];
int vv[1000][1000], cas[1000][1000];
int flag[_n];
int ans;

bool check(int cc, int x, int cx, int y, int cy)
{
	return (cx == 0 ? vvv[x] : uuu[x]) == (cy == 0 ? uuu[y] : vvv[y]);
}

void calc()
{
	int min = 1000000000;
	if (last[1] == 4)
	{
	/*	for (int i = 1; i <= 4; i++)
		{
			printf("%d ", vv[1][i]);
		}
		puts("");*/
	}
/*	if (last[1] == 4 && vv[1][1] == 3 && vv[1][2] == 1 && vv[1][3] == 2 && vv[1][4] == 6)
	{
		puts("XXX");
		for (int i = 1; i <= 4; i++)
		{
			if (cas[1][i])
			{
				printf("%d %d\n", uuu[vv[1][i]], vvv[vv[1][i]]);
			}
			else
			{
				printf("%d %d\n", vvv[vv[1][i]], uuu[vv[1][i]]);
			}
		}
	}*/
	for (int i = 1; i <= m; i++)
	{
		bool flag = true;
		int s = 0;
		if (last[i] == 0)
		{
			s = 0;
		}
		else
		{
			s = ccc[vv[i][1]];
			for (int j = 2; j <= last[i]; j++)
			{
				if (! check(last[1] == 4 && vv[1][1] == 3 && vv[1][2] == 1 && vv[1][3] == 2 && vv[1][4] == 6, vv[i][j], cas[i][j], vv[i][j - 1], cas[i][j - 1]))
				{
					return;
				}
				s += ccc[vv[i][j]];
			}
		}
		min = std::min(min, s);
	}
	if (min == 27)
	{
		for (int i = 1; i <= m; i++)
		{
//			printf("#%d\n", i);
//			for (int j = 1; j <= last[i]; j++)
//			{
//				printf("%d ", vv[i][j]);
//			}
//			puts("");
		}
	}
	ans = std::max(ans, min);
}

void dfs(int k, int now)
{
	if (k == n || now > m)
	{
		calc();
		return;
	}
	dfs(k, now + 1);
	for (int i = 1; i < n; i++)
	{
		if (! flag[i])
		{
			flag[i] = 1;
			last[now]++;
			vv[now][last[now]] = i;
			cas[now][last[now]] = 0;
			dfs(k + 1, now);
			cas[now][last[now]] = 1;
			dfs(k + 1, now);
			flag[i] = 0;
			last[now]--;
		}
	}
}

int main()
{
	scanf("%d %d", &n, &m);
	for (int i = 1; i < n; i++)
	{
		scanf("%d %d %d", &uuu[i], &vvv[i], &ccc[i]);
	}
	ans = 0;
	dfs(1, 1);
	printf("%d\n", ans);
	return 0;
}
