#include <cstdio>
#include <algorithm>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

const int _n = 50000 + 10, _l = 200000 + 10, maxl = 200000;
int n, m;
int edgenum;
int vet[2 * _n], val[2 * _n], nextx[2 * _n], head[_n];
int bit[_l];
int f[_n];
int K;
int cnt;
int last;
int vv[_n];
int cc[_l];

void add(int u, int v, int cost)
{
	edgenum++;
	vet[edgenum] = v;
	val[edgenum] = cost;
	nextx[edgenum] = head[u];
	head[u] = edgenum;
}

inline int lowbit(int x)
{
	return x & -x;
}

void change(int x, int delta)
{
	for (int i = x; i <= maxl; i += lowbit(i))
	{
		bit[i] += delta;
	}
}

int query(int x)
{
	int ans = 0;
	for (int i = x; i; i -= lowbit(i))
	{
		ans += bit[i];
	}
	return ans;
}

int find(int k)
{
	int now = 0;
	int cnt = 0;
	for (int i = 17; i >= 0; i--)
	{
		if (cnt + bit[now + (1 << i)] <= k)
		{
			now += (1 << i);
			cnt += bit[now];
		}
	}
	return now;
}

void dfs(int u, int father)
{
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (v != father)
		{
			dfs(v, u);
		}
	}
	last = 0;
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		int cost = val[i];
		if (v != father)
		{
			if (f[u] + cost >= K)
			{
				cnt++;
			}
			else
			{
				last++;
				vv[last] = f[u] + cost;
			}
		}
	}
	std::sort(vv + 1, vv + last + 1);
	for (int i = 1; i <= last; i++)
	{
		change(vv[i], 1);
		cc[vv[i]]++;
	}
	for (int i = 1; i <= last; i++)
	{
		if (cc[vv[i]])
		{
			if (query(maxl) - query(K - vv[i]) > 0)
			{
				int t = query(vv[i]);
				int k = find(t) + 1;
				cc[vv[i]]--;
				cc[k]--;
				change(vv[i], -1);
				change(k, -1);
				cnt++;
			}
		}
	}
	f[u] = 0;
	for (int i = last; i >= 1; i--)
	{
		if (cc[vv[i]])
		{
			f[u] = std::max(f[u], vv[i]);
			change(vv[i], -1);
			cc[vv[i]]--;
		}
	}
	printf("O%d %d\n", u, f[u]);
}

bool check(int mid)
{
	K = mid;
	cnt = 0;
	dfs(1, 0);
	return cnt >= m;
}

int main()
{
	n = read();
	m = read();
	int sum = 0;
	for (int i = 1; i < n; i++)
	{
		int u, v, cost;
		u = read();
		v = read();
		cost = read();
		add(u, v, cost);
		add(v, u, cost);
		sum += cost;
	}
	int l = 0, r = sum;
	while (l < r)
	{
		printf("%d %d\n", l, r);
		int mid = (l + r + 1) >> 1;
		if (check(mid))
		{
			l = mid;
		}
		else
		{
			r = mid - 1;
		}
	}
	printf("%d\n", l);
	return 0;
}
