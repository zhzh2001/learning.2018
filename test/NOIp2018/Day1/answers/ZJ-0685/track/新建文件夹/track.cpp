#include <bits/stdc++.h>
using namespace std;
const int N=1e5+7;

#define C getchar()-48
int read()
{
  int s=0,t=1,k=C;
  for (;k<0||9<k;k=C) if (k==-3) t=-1;
  for (;0<=k&&k<=9;k=C) s=(s<<1)+(s<<3)+k;
  return s*t;
}

  struct hh
    {
      int n,r,v;
    }e[N];
  int h[N];
  int t;
void ins()
{
  int x=read(),y=read(),z=read();
  e[++t]=(hh){h[x],y,z};h[x]=t;
  e[++t]=(hh){h[y],x,z};h[y]=t;
}

  int n,m;
void in()
{
  cin>>n>>m;
  for (int i=n;--i;)
    ins();
}

  int d[N],f[N],s[N],b[N],r[N];
#define P(i) e[i].r
void fir(int k,int F)
{
  f[k]=F;
  d[k]=d[F]+1;
  for (int i=h[k];i;i=e[i].n)
    if (P(i)^F)
      {
        r[P(i)]=r[k]+e[i].v;
        fir(P(i),k);
        if (s[b[k]]<s[P(i)])
          b[k]=P(i);
      }
}

  int u[N];
void sec(int k,int T)
{
  u[k]=T;
  if (b[k]) sec(b[k],T);
  for (int i=h[k];i;i=e[i].n)
    if (P(i)^f[k]&&P(i)^b[k])
      sec(P(i),P(i));
}

int lca(int x,int y)
{
  while (u[x]^u[y])
    {
      if (d[u[x]]<d[u[y]])
        swap(x,y);
      x=f[u[x]];
    }
  if (d[x]<d[y]) swap(x,y);
  return y;
}

  multiset<int> S;
void ud(int x,int y,int z)
{
  int k=r[x]+r[y]-(r[z]<<1);
  S.insert(k);
  if (S.size()>m)
    S.erase(S.begin());
}

  int A;
void wor()
{
  fir(1,0);
  sec(1,1);
  for (int i=0;++i<=n;)
    for (int j=i;++j<=n;)
      ud(i,j,lca(i,j));
  A=*S.begin();
}

void out()
{
  cout<<A;
}

int main()
{
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  in();
  wor();
  out();
  fclose(stdin);
  fclose(stdout);
  exit(0);
} 
