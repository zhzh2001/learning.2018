#include <bits/stdc++.h>
using namespace std;
const int N=1e5+7;

#define C getchar()-48
int read()
{
  int s=0,t=1,k=C;
  for (;k<0||9<k;k=C) if (k==-3) t=-1;
  for (;0<=k&&k<=9;k=C) s=(s<<1)+(s<<3)+k;
  return s*t;
}

  struct hh
    {
      int n,r,v;
    }e[N];
  int h[N];
  int t;
void ins()
{
  int x=read(),y=read(),z=read();
  e[++t]=(hh){h[x],y,z};h[x]=t;
  e[++t]=(hh){h[y],x,z};h[y]=t;
}

  int n,m;
void in()
{
  cin>>n>>m;
  for (int i=n;--i;)
    ins();
}

  int l[N],b[N][4];
  int T;
#define P(i) e[i].r
void dfs(int k,int F,int A)
{
  l[k]=0;
  b[k][1]=b[k][2]=b[k][3]=0;
  for (int i=h[k];i;i=e[i].n)
    if (P(i)^F)
      {
        dfs(P(i),k,A);
        l[P(i)]+=e[i].v;
        if (l[b[k][3]]<=l[P(i)])
          b[k][3]=P(i);
        for (int j=3;--j;)
          if (l[b[k][j+1]]>l[b[k][j]])
            swap(b[k][j+1],b[k][j]);
      }
  if (l[b[k][2]]+l[b[k][3]]>=A)
    ++T,l[k]=l[b[k][1]];
  else
  if (l[b[k][1]]+l[b[k][3]]>=A)
    ++T,l[k]=l[b[k][2]];
  else
  if (l[b[k][1]]+l[b[k][2]]>=A)
    ++T,l[k]=l[b[k][3]];
  else
    l[k]=l[b[k][1]];
}

int che(int A)
{
  for (int i=4;--i;)
    {  
      T=0;
      dfs(rand()%n+1,0,A);
      if (T>=m) return 1;
    }
  return 0;
}

  int A=0;
void wor()
{
  srand((unsigned)time(NULL));
  for (int i=30;~--i;)
    if (che(A+(1<<i)))
      A+=1<<i;
}

void out()
{
  cout<<A;
}

int main()
{
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  in();
  wor();
  out();
  fclose(stdin);
  fclose(stdout);
  exit(0);
} 
