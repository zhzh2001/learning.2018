#include <bits/stdc++.h>
using namespace std;
const int N=1e5+7;

#define C getchar()-48
int read()
{
  int s=0,t=1,k=C;
  for (;k<0||9<k;k=C) if (k==-3) t=-1;
  for (;0<=k&&k<=9;k=C) s=(s<<1)+(s<<3)+k;
  return s*t;
}

  struct hh
    {
      int n,r,v;
    }e[N];
  int h[N];
  int t;
void ins()
{
  int x=read(),y=read(),z=read();
  e[++t]=(hh){h[x],y,z};h[x]=t;
  e[++t]=(hh){h[y],x,z};h[y]=t;
}

  int n,m;
void in()
{
  cin>>n>>m;
  for (int i=n;--i;)
    ins();
}

  int l[N],z[N],v[N];
  int T,Z;
#define P(i) e[i].r
void dfs(int k,int F,int A)
{
  Z=0;
  l[k]=0;
  for (int i=h[k];i;i=e[i].n)
    if (P(i)^F)
      {
        dfs(P(i),k,A);
        l[P(i)]+=e[i].v;
        z[++Z]=l[P(i)];
      }
  sort(z+1,z+Z+1);
  for (int i=0;++i<=Z;) v[i]=0;
  while (z[Z]>=A&&Z) --Z,++T;

  for (int R=0,L=Z;L;--L)
    {
      while ((z[L]+z[R]<A||v[R])&&R<Z) ++R;
      if (!v[L]&&!v[R]&&z[L]+z[R]>=A&&R>L)
        ++T,v[L]=1,v[R]=1;
    }

  for (int L=0;++L<=Z;)
    if (!v[L]) l[k]=z[L];
}

int che(int A)
{
  for (int i=4;--i;)
    {  
      T=0;
      dfs(1,0,A);
      if (T>=m) return 1;
    }
  return 0;
}

  int A=0;
void wor()
{
  srand((unsigned)time(NULL));
  for (int i=30;~--i;)
    if (che(A+(1<<i)))
      A+=1<<i;
}

void out()
{
  cout<<A;
}

int main()
{
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  in();
  wor();
  out();
  fclose(stdin);
  fclose(stdout);
  exit(0);
} 
