#include <bits/stdc++.h>
using namespace std;
const int N=233,M=25007;

#define C getchar()-48
int read()
{
  int s=0,t=1,k=C;
  for (;k<0||9<k;k=C) if (k==-3) t=-1;
  for (;0<=k&&k<=9;k=C) s=(s<<1)+(s<<3)+k;
  return s*t;
}

  int f[M];
  int A,m;
void fir()
{
  m=A=0;
  memset(f,0,sizeof f);
}

  int n;
  int a[N];
void in()
{
  cin>>n;
  for (int i=0;++i<=n;)
    {
      a[i]=read();
      if (a[i]>m) m=a[i];
    }
}

void wor()
{
  sort(a+1,a+n+1);
  f[0]=1;
  for (int i=0;++i<=n;)
    {
      if (!f[a[i]]) ++A;
      else continue;
      for (int j=a[i];j<=m;++j)
        f[j]|=f[j-a[i]];
    }
}

void out()
{
  cout<<A<<endl;
}

void sol()
{
  fir();
  in();
  wor();
  out();
}

int main()
{
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  for (int T=read();T;--T) sol();
  fclose(stdin);
  fclose(stdout);
  exit(0);
} 
