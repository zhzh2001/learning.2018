#include<bits/stdc++.h>
using namespace std;
const int N=100010;
int A[N],stk[N],lson[N],rson[N],Ans;
void dfs(int x,int Las){
	Ans+=A[x]-Las;
	if (lson[x]>0){
		dfs(lson[x],A[x]);
	}
	if (rson[x]>0){
		dfs(rson[x],A[x]);
	}
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n,i;
	scanf("%d",&n);
	int top=0;
	for (i=1;i<=n;i++){
		scanf("%d",&A[i]);
		int Las=0;
		while (top>0 && A[stk[top]]>=A[i]){
			Las=stk[top--];
		}
		if (Las!=0){
			lson[i]=Las;
		}
		if (top>0){
			rson[stk[top]]=i;
		}
		stk[++top]=i;
	}
	dfs(stk[1],0);
	printf("%d\n",Ans);
	return 0;
}
