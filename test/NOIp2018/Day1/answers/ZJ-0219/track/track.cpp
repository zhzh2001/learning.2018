#include<bits/stdc++.h>
using namespace std;
//bool mem1;
const int N=50010,INF=0x3f3f3f3f,MINF=0xc0c0c0c0;
struct edge{
	int nxt,t,s;
}e[N<<1];
int head[N],edge_cnt;
void add_edge(int x,int y,int z){
	e[edge_cnt]=(edge){head[x],y,z};
	head[x]=edge_cnt++;
}
int n,m,sMn,sMx;
struct P1{
	int Tar;
	int sum[N],Mx[N],B[N],C[N];
	bool mark[N];
	void Deal(int h,int x){
		sort(B+1,B+1+h);
		int i,j,k,res=-1,res1=0;
		for (k=h;k>=0;k--){
			int g=0;
			for (i=1;i<=h;i++){
				if (i==k){
					continue;
				}
				C[++g]=B[i];
			}
			int cnt=0;
			j=1;
			for (i=g;i>=j;i--){
				while (j<i && C[j]+C[i]<Tar){
					j++;
				}
				if (j>=i){
					break;
				}
				j++;
				cnt++;
			}
			if (cnt>res){
				res=cnt;
				res1=B[k];
			}
		}
		sum[x]+=res;
		Mx[x]=res1;
	}
	void Deal1(int h,int x){
		sort(B+1,B+1+h);
		
		int i,j,k,res=0,res1=0;
		j=1;
		for (i=h;i>=j;i--){
			while (j<i && B[j]+B[i]<Tar){
				j++;
			}
			if (j>=i){
				break;
			}
			mark[i]=mark[j]=1;
			j++;
			res++;
		}
		for (i=1;i<=h;i++){
			if (!mark[i]){
				res1=B[i];
			}else{
				mark[i]=0;
			}
		}
		for (k=h;k>=max(1,h-15);k--){
			int g=0;
			for (i=1;i<=h;i++){
				if (i==k){
					continue;
				}
				C[++g]=B[i];
			}
			int cnt=0;
			j=1;
			for (i=g;i>=j;i--){
				while (j<i && C[j]+C[i]<Tar){
					j++;
				}
				if (j>=i){
					break;
				}
				j++;
				cnt++;
			}
			if (cnt>res || (cnt==res && B[k]>res1)){
				res=cnt;
				res1=B[k];
			}
		}
		
		sum[x]+=res;
		Mx[x]=res1;
	}
	void dfs(int x,int f,int P){
		sum[x]=0;
		Mx[x]=0;
		int i,h=0;
		for (i=head[x];~i;i=e[i].nxt){
			int to=e[i].t,val=e[i].s;
			if (to==f){
				continue;
			}
			dfs(to,x,val);
		}
		for (i=head[x];~i;i=e[i].nxt){
			int to=e[i].t;
			if (to==f){
				continue;
			}
			sum[x]+=sum[to];
			B[++h]=Mx[to];
		}
		
		if (h!=0){
			if (n<=1000 || h<=40){
				Deal(h,x);
			}else{
				Deal1(h,x);
			}
		}
		
		Mx[x]+=P;
		if (Mx[x]>=Tar){
			sum[x]++;
			Mx[x]=0;
		}
	}
	bool check(int x){
		Tar=x;
		dfs(1,0,0);
		return sum[1]>=m;
	}
	void solve(){
		int L=sMn,R=sMx,res=sMn;
		while (L<=R){
			int mid=(L+R)>>1;
			if (check(mid)){
				L=mid+1;
				res=mid;
			}else{
				R=mid-1;
			}
		}
		printf("%d\n",res);
	}
}P100;
//bool mem2;
int main(){
//	cerr<<(&mem2-&mem1)/1024.0/1024<<endl;
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(head+1,-1,sizeof(int)*1*n);
	sMn=INF;
	int i;
	for (i=1;i<n;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		add_edge(x,y,z);
		add_edge(y,x,z);
		sMx+=z;
		sMn=min(sMn,z);
	}
	P100.solve();
	return 0;
}
//struct P1{
//	static const int M=1010;
//	int Tar;
//	int dp[M][M][2],siz[M];
//	void dfs(int x,int f,int P){
//		siz[x]=0;
//		int i,j,k,son=0;
//		dp[x][0][0]=0;
//		for (i=head[x];~i;i=e[i].nxt){
//			int to=e[i].t,val=e[i].s;
//			if (to==f){
//				continue;
//			}
//			son++;
//			dfs(to,x,val);
////			cerr<<to<<endl;
//			for (j=siz[x];j>=0;j--){
//				int t0=dp[x][j][0],t1=dp[x][j][1];
//				for (k=siz[to];k>=0;k--){
//					int g0=dp[to][k][0],g1=dp[to][k][1];
////					if (x==1 && to==2 && j==0 && k==0){
////						cerr<<t1<<" "<<g1<<endl;
////					}
//					if (t1+g1>=Tar){
//						dp[x][j+k+1][0]=max(dp[x][j+k+1][0],0);
//					}
//					if (t0!=MINF){
//						dp[x][j+k][1]=max(dp[x][j+k][1],t0+g1);
//					}
//					if (g0!=MINF){
//						dp[x][j+k][1]=max(dp[x][j+k][1],t1+g0);
//					}
//				}
//			}
//			siz[x]+=siz[to];
//		}
////		cerr<<x<<" "<<i<<endl;
//		if (x!=1){
//			for (i=0;i<=siz[x];i++){
//				int t0=dp[x][i][0],t1=dp[x][i][1];
//				if (t0!=MINF){
//					dp[x][i][1]=max(dp[x][i][1],t0+P);
//				}
//				if (t0!=MINF){
//					dp[x][i][1]=max(dp[x][i][1],t1+P);
//				}
//			}
//		}else{
//			for (i=0;i<=siz[x];i++){
//				if (dp[x][i][1]>=Tar){
//					dp[x][i+1][0]=max(dp[x][i+1][0],0);
//				}
//			}
//		}
//		if (son==0){
//			siz[x]=1;
//		}
//			cerr<<x<<"******"<<endl;
//			for (j=0;j<=siz[x];j++){
//				cerr<<j<<" "<<dp[x][j][0]<<" "<<dp[x][j][1]<<endl;
//			}
//	}
//	bool check(int x){
//		Tar=x;
//		int i;
//		for (i=1;i<=n;i++){
//			memset(dp[i],192,sizeof(int)*2*(n+1));
//		}
//		dfs(1,0,0);
////		cerr<<dp[4][0][0]<<endl;
//		return dp[1][m][0]!=MINF;
//	}
//	void solve(){
//		cerr<<check(15)<<endl;
////		int L=Mn,R=Mx,res=Mn;
////		while (L<=R){
////			int mid=(L+R)>>1;
////			if (check(mid)){
////				L=mid+1;
////				res=mid;
////			}else{
////				R=mid-1;
////			}
////		}
////		printf("%d\n",res);
//	}
//}P50;

		
//		if (h>1){
//			j=1;
//			for (i=h-1;i>=j;i--){
//				while (j<i && B[j]+B[i]<Tar){
//					j++;
//				}
//				if (j>=i){
//					break;
//				}
//				j++;
//				rsum2++;
//			}
//			rMx2=B[h];
//			
//			swap(B[h],B[h-1]);
//			int t1=0,t2=0;
//			j=1;
//			for (i=h-1;i>=j;i--){
//				while (j<i && B[j]+B[i]<Tar){
//					j++;
//				}
//				if (j>=i){
//					break;
//				}
//				j++;
//				t1++;
//			}
//			t2=B[h];
//			
//			if (t1>rsum2 || (t1==rsum2 && t2>rMx2)){
//				rsum2=t1;
//				rMx2=t2;
//			}
//		}
//		
//		if (rsum1>rsum2 || (rsum1==rsum2 && rMx1>rMx2)){
//			sum[x]+=rsum1;
//			Mx[x]=rMx1;
//		}else{
//			sum[x]+=rsum2;
//			Mx[x]=rMx2;
//		}
