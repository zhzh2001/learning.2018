#include<bits/stdc++.h>
using namespace std;
const int N=110,M=25010;
bool dp[M],mark[M];
int A[N];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int Case;
	scanf("%d",&Case);
	dp[0]=mark[0]=1;
	while (Case--){
		int n,Mx=0,Ans=0,i,j;
		scanf("%d",&n);
		for (i=1;i<=n;i++){
			scanf("%d",&A[i]);
			Mx=max(Mx,A[i]);
		}
		for (i=1;i<=n;i++){
			for (j=0;j+A[i]<=Mx;j++){
				dp[j+A[i]]|=dp[j];
			}
		}
		for (i=1;i<=Mx;i++){
			if (dp[i] && !mark[i]){
				Ans++;
				for (j=0;j+i<=Mx;j++){
					mark[j+i]|=mark[j];
				}
			}
		}
		memset(dp+1,0,sizeof(bool)*1*Mx);
		memset(mark+1,0,sizeof(bool)*1*Mx);
		printf("%d\n",Ans);
	}
	return 0;
}
