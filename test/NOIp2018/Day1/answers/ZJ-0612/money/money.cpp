#include<cstdio>
#include<algorithm>
using namespace std;
int n,t,dp[50001],ans,a[111],x;
bool cmp(int aa,int bb){return(aa<bb);}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	for(int ii=1;ii<=t;ii++)
	{
		for(int i=1;i<=25000;i++)dp[i]=0;
		dp[0]=1;
		ans=0;
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",&a[i]);
		sort(a+1,a+1+n,cmp);
		for(int i=1;i<=n;i++)
		{
			x=a[i];
			if(dp[x]>0)continue;
			ans++;
			for(int i=0;i<=25000;i++)if(dp[i]>0)
			{
				if((i+x)<=25000)dp[i+x]=1;
			}
		}
		printf("%d\n",ans);
	}
}
