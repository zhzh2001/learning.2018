#include<cstdio>
#include<algorithm>
using namespace std;
struct node{int z,ss;};
node a[50001];
int b[50011],fa[50001],fir[50001],tot,ans,n,m,ui,vi,dp[50001],wi,nex[100001],sto[100001],v[100001],l,r,mid,cntt,cnt,li[200001],ri[200001],sz[200001];
bool cmp(int aa,int bb){return(aa<bb);}
void addbian(int aa,int bb,int cc)
{
	tot++;
	nex[tot]=fir[aa];
	fir[aa]=tot;
	sto[tot]=bb;
	v[tot]=cc;
}
void built(int num,int x,int y)
{
	li[num]=x;
	ri[num]=y;
	if(x==y)
	{
		sz[num]=a[x].ss;
		return;
	}
	int mid=(x+y)/2;
	built(num*2,x,mid);
	built(num*2+1,mid+1,y);
	sz[num]=sz[num*2]+sz[num*2+1];
}
void gai(int num,int x)
{
	sz[num]--;
	if(li[num]==ri[num])return;
	int mid=(li[num]+ri[num])/2;
	if(x<=mid)gai(num*2,x);
	else gai(num*2+1,x);
}
int ask(int num,int x)
{
	if(sz[num]==0)return(-1);
	if((li[num]==ri[num]))return(li[num]);
	if(x<a[li[num]].z)x=a[li[num]].z;
	int mid=(li[num]+ri[num])/2;
	if(x<=a[mid].z)
	{
		int answ=ask(num*2,x);
		if(answ>0)return(answ);
		else return(ask(num*2+1,a[mid+1].z));
	}
	else return(ask(num*2+1,x));
}
void dfs(int x)
{
	int aa=fir[x];
	dp[x]=0;
	while(aa!=0)
	{
		if(sto[aa]!=fa[x])
		{
			fa[sto[aa]]=x;
			dfs(sto[aa]);
		}
		aa=nex[aa];
	}
	aa=fir[x];
	cntt=0;
	cnt=0;
	while(aa!=0)
	{
		if(sto[aa]!=fa[x])
		{
			if((dp[sto[aa]]+v[aa])>=mid)ans++;
			else
			{
				cntt++;
				b[cntt]=dp[sto[aa]]+v[aa];
			}
		}
		aa=nex[aa];
	}
	if(cntt>0)
	{
		sort(b+1,b+1+cntt,cmp);
		cnt=1;
		a[1].z=b[1];
		a[1].ss=1;
		for(int i=2;i<=cntt;i++)
		{
			if(b[i]!=b[i-1])
			{
				cnt++;
				a[cnt].z=b[i];
				a[cnt].ss=0;
			}
			a[cnt].ss++;
		}
		built(1,1,cnt);
		while(sz[1]>0)
		{
			int bb,cc=-1;
			bb=ask(1,a[1].z);
			gai(1,bb);
			if((a[cnt].z+a[bb].z)>=mid)cc=ask(1,mid-a[bb].z);
			if(cc>0)
			{
				ans++;
				gai(1,cc);
			}
			else
			{
				if(a[bb].z>dp[x])dp[x]=a[bb].z;
			}
		}
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=2;i<=n;i++)
	{
		scanf("%d%d%d",&ui,&vi,&wi);
		addbian(ui,vi,wi);
		addbian(vi,ui,wi);
	}
	fa[1]=1;
	sz[0]=0;
	l=1;
	r=499990000;
	while(l<r)
	{
		mid=(l+r+1)/2;
		ans=0;
		dfs(1);
		if(ans>=m)l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
}
