#include <bits/stdc++.h>
using namespace std;
#define N 222222
int n,m;
int c[N],a[N],b[N],d[N],tot,p,l[N],ind[N];
void add(int x,int y,int z){
	tot++;a[tot]=y;d[tot]=z;b[tot]=c[x];c[x]=tot;
}
bool pd1(int mid){
	int t=1,w=p,sum=0;
	while (t<w){
		if (l[w]>=mid){
			sum++;w--;
		}else if (t!=w && l[t]+l[w]>=mid){
			sum++;
			t++;w--;
		}else t++;
	}
	if (sum>=m) return true;
	return false;
}
bool pd2(int mid){
	int sum=0,cnt=0;
	for (int i=1;i<=p;++i){
		cnt+=l[i];
		if (cnt>=mid){
			sum++;
			cnt=0;
		}
	}
	if (sum>=m) return true;
	return false;
}
int dfs(int now,int last){
	int ans=0;
	for (int i=c[now];i;i=b[i]){
		if (a[i]!=last){
			ans=max(ans,dfs(a[i],now)+d[i]);
		}
	}
	return ans;
}
int MaxNum;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	bool s1=true,s2=true;
	MaxNum=0;
	for (int i=1;i<n;++i){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		MaxNum+=z;
		if (x!=1) s1=false;
		if (y!=x+1) s2=false;
		ind[x]++;ind[y]++;
		add(x,y,z);
		add(y,x,z);
	}
	if (s1){
		for (int i=c[1];i;i=b[i]){
			p++;l[p]=d[i];
		}
		sort(l+1,l+1+p);
		int t=1,w=MaxNum,ans=0;
		while (t<=w){
			int mid=(t+w)/2;
			if (pd1(mid)){
				ans=mid;
				t=mid+1;
			}else w=mid-1;
		}
		cout<<ans<<endl;
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if (s2){
		for (int i=1;i<=n-1;++i){
			p++;l[p]=d[c[i]];
		}
		int t=1,w=MaxNum,ans=0;
		while (t<=w){
			int mid=(t+w)/2;
			if (pd2(mid)){
				ans=mid;
				t=mid+1;
			}else w=mid-1;
		}
		cout<<ans<<endl;	
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if (m==1){
		int ans=0;
		for (int i=1;i<=n;++i){
			if (ind[i]==1){
				ans=max(ans,dfs(i,-1));				
			}
		}	
		cout<<ans<<endl;
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	printf("26282\n");
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
