#include <bits/stdc++.h>
using namespace std;
#define INF 1000
#define N 111
int ans;
int MaxNum,n;
int f[N],a[N],fc[N];
void sc(int t,int y){
	if (y>=ans){
		return;
	}
	if (t>n){
		memset(f,0,sizeof(f));
		f[0]=true;
		for (int i=1;i<=n;++i){
			if (!fc[i]) continue;
			for (int j=a[i];j<=MaxNum;++j){
				if (f[j-a[i]]) f[j]=true;
			}
		}
		bool flag=true;
		for (int i=1;i<=n;++i){
			if (!f[a[i]]){
				flag=false;break;
			}
		}
		if (flag){
			ans=y;
		}
		return;
	}
	fc[t]=true;
	sc(t+1,y+1);
	fc[t]=false;
	sc(t+1,y);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	int T;
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		ans=INF;
		memset(fc,0,sizeof(fc));
		for (int i=1;i<=n;++i){
			scanf("%d",&a[i]);
			if (a[i]>MaxNum) MaxNum=a[i];
		}
		sc(1,0);
		cout<<ans<<endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
