#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
#define rep(x,y,z) for (int x=y;x<=z;x++)
#define per(x,y,z) for (int x=y;x>=z;x--)
int read(int &x){char ch=getchar();x=0;while(ch<='9'&&ch>='0'){x=x*10+ch-'0';ch=getchar();}}
int read(int &x,int &y){read(x);read(y);}
int read(int &x,int &y,int &z){read(x);read(y);read(z);}
const int maxn=55555;
int a,b[maxn],l[maxn],n,m;
struct GRAPH
{
	int to[maxn*2],nxt[maxn*2],first[maxn],cnt,dist[maxn],value[maxn];
	void addedge(int u,int v,int w)
	{
		to[++cnt]=v;
		value[cnt]=w;
		nxt[cnt]=first[u];
		first[u]=cnt;
	}
	void addedge2(int u,int v,int w){addedge(u,v,w);addedge(v,u,w);}
	void dfs(int x,int fa)
	{
		for (int q=first[x];q!=0;q=nxt[q])
		{
			if (to[q]==fa) continue;
			dist[to[q]]=dist[x]+value[q];
			dfs(to[q],x);
		}
	}
	int PHI()
	{
		rep(i,1,n) dist[i]=0;
		dfs(1,0);
		int maxx=0,choice=1;
		rep(i,1,n) if (dist[i]>maxx) maxx=dist[i],choice=i;
		rep(i,1,n) dist[i]=0;
		dfs(choice,0);
		maxx=0;
		choice=0;
		rep(i,1,n) if (dist[i]>maxx) maxx=dist[i],choice=i;
		return maxx;
	}
}graph;
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n,m);
	bool flag=true,flag2=true;
	rep(i,1,n-1) {read(a,b[i],l[i]);if (b[i]!=a+1) flag2=false;if (a!=1) flag=false;graph.addedge2(a,b[i],l[i]);}
	if (m==1){printf("%d\n",graph.PHI());}
	else if (flag)
	{
		
	}
	return 0;	
}
