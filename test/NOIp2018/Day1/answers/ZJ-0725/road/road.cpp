#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;
#define rep(x,y,z) for (int x=y;x<=z;x++)
#define per(x,y,z) for (int x=y;x>=z;x--)
int read(int &x){char ch=getchar();x=0;while(ch<='9'&&ch>='0'){x=x*10+ch-'0';ch=getchar();}}
int read(int &x,int &y){read(x);read(y);}
const int maxn=111111;
int a[maxn],l[maxn],r[maxn];
int n,m,ans,minn;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	//scanf("%d",&n);
	minn=0x3f3f3f3f;
	rep(i,1,n) 
	{
		read(a[i]);
		//scanf("%d",a+i);
		minn=min(a[i],minn);
	}
	if (minn!=0) rep(i,1,n) a[i]-=minn;
	ans+=minn;
	int temp=2;
	int nn=n;
	rep(i,2,n)
	{
		if (a[i]!=a[i-1]) {a[temp]=a[i];temp++;}
		else nn--;
	}
	a[0]=0;a[nn+1]=0;
	per(i,nn-1,1)
	{
		if (a[i]==0) continue;
		r[i]=nn+1;
		for (int j=i+1;j<=nn;j=r[j])
		{
			if (a[j]<a[i]) {r[i]=j;break;}
			j=r[j]-1;
		}
	}
	rep(i,1,nn)
	{
		if (a[i]==0) continue;
		l[i]=0;
		bool flag=false;
		for (int j=i-1;j>=1;j=l[j])
		{
			if (a[j]<a[i]) {l[i]=j;break;} 
			if (!(a[j]>a[i])) {flag=true;l[i]=l[j];break;}
		}
		if (!flag) ans+=a[i]-max(a[l[i]],a[r[i]]);
	}
	printf("%d\n",ans);
	return 0;	
}
