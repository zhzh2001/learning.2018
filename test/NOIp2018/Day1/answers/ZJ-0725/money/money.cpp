#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <set>
#include <cmath>
using namespace std;
#define rep(x,y,z) for (int x=y;x<=z;x++)
#define per(x,y,z) for (int x=y;x>=z;x--)
int read(int &x){char ch=getchar();x=0;while(ch<='9'&&ch>='0'){x=x*10+ch-'0';ch=getchar();}}
int read(int &x,int &y){read(x);read(y);}
const int maxn=111;
int n,m,T,a[maxn];
bool bo[25555];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	bo[0]=true;
	while (T--)
	{
		rep(i,1,25000) bo[i]=false;
		read(n);
		rep(i,1,n) read(a[i]);
		sort(a+1,a+n+1);
		int ans=n;
		rep(i,1,n) 
		{
			if (!bo[a[i]]) {rep(j,0,25000) if (bo[j]) bo[j+a[i]]=true;}
			else ans--;
		}
		printf("%d\n",ans);
	}
	return 0;	
}
