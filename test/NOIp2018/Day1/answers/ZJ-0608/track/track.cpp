#include <bits/stdc++.h>
using namespace std;
void judge(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
}
const int N=51000;
int n,m,sum[N];
struct info{
	int to,nxt,w;
}e[N<<1];
struct atom{
	int x,y,w;
}a[N];
bool cmp(atom a,atom b){
	return a.w>b.w;
}
int head[N],opt;
void add(int x,int y,int z){
	e[++opt]=(info){y,head[x],z};
	head[x]=opt;
}
int dep[N],fa[20][N];
void dfs(int u){
	dep[u]=dep[fa[0][u]]+1;
	for (int i=head[u];i;i=e[i].nxt){
		int k=e[i].to,w=e[i].w;
		if (k==fa[0][u]) continue;
		fa[0][k]=u; sum[k]=sum[u]+w; dfs(k);
	}
}
int lca(int x,int y){
	if (dep[x]<dep[y]) swap(x,y);
	for (int i=18;i>=0;i--){
		if (dep[fa[i][x]]>=dep[y]) x=fa[i][x];
	}
	if (x==y) return x;
	for (int i=18;i>=0;i--){
		if (fa[i][x]!=fa[i][y]) x=fa[i][x],y=fa[i][y];
	}
	return fa[0][x];
}
int main(){
	judge();
	scanf("%d%d",&n,&m);
	bool flag1=1,flag2=1;
	for (int i=1;i<n;i++){
		int x,y,z; scanf("%d%d%d",&x,&y,&z);
		if (x!=1) flag1=0;
		if (x!=y-1) flag2=0;
		add(x,y,z),add(y,x,z);
		a[i]=(atom){x,y,z};
	}
	if (flag1){
		sort(a+1,a+n,cmp);
		if (m!=n-1) printf("%d\n",a[1].w+a[2].w);
		else printf("%d\n",a[1].w);
		return 0;
	}
	if (flag2){
		int len=n-m,wzp=0;
		for (int i=2;i<=n;i++) sum[i]=sum[i-1]+a[i-1].w;
		for (int i=1;i<=n;i++){
			int j=i+len;
			int dis=sum[j]-sum[i];
			wzp=max(wzp,dis);
		}
		printf("%d\n",wzp);
		return 0;
	}
	if (m==1){
		int wzp=0;
		dfs(1);
		for (int i=1;i<=18;i++){
			for (int j=1;j<=n;j++) fa[i][j]=fa[i-1][fa[i-1][j]];
		}
		//for (int i=1;i<=n;i++) printf("%d\n",fa[0][i]);
		for (int i=1;i<=n;i++){
			for (int j=1;j<=n;j++){
				if (i==j) continue;
				int t=lca(i,j);
				//printf("%d %d %d\n",i,j,t);
				int dis=sum[i]+sum[j]-2*sum[t];
				wzp=max(dis,wzp);
			}
		}
		printf("%d\n",wzp);
		return 0;
	}
	return 0;
}
