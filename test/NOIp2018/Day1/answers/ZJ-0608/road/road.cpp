#include <bits/stdc++.h>
using namespace std;
void judge(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
}
inline void read(int &x){
	x=0; char ch=getchar();
	while (!isdigit(ch)) ch=getchar();
	while (isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
}
const int N=110000,inf=1<<30;
int n,alb[N];
long long ans;
long long wzp(int l,int r){
	if (l==r) return alb[l];
	if (l>r) return 0;
	int Min=inf,pre=l;
	long long sum=0;
	for (int i=l;i<=r;i++) Min=min(Min,alb[i]);
	sum+=Min;
	for (int i=l;i<=r;i++){
		alb[i]-=Min;
		if (!alb[i]) sum+=wzp(pre,i-1),pre=i+1;
	}
	sum+=wzp(pre,r);
	return sum;
}
int main(){
	judge();
	read(n);
	for (int i=1;i<=n;i++) read(alb[i]);
	ans=wzp(1,n);
	printf("%lld\n",ans);
	return 0;
}
