#include <bits/stdc++.h>
using namespace std;
void judge(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}
const int N=110;
int n;
namespace subtask1{
	int a,b;
	void Main(){
		scanf("%d%d",&a,&b);
		if (a%b==0||b%a==0) printf("1\n");
		else printf("2\n");
	}
}
namespace subtask2{
	int a[4];
	void Main(){
		int ans=3;
		for (int i=1;i<=3;i++) scanf("%d",&a[i]);
		for (int i=1;i<=3;i++){
			bool flag=0;
			for (int j=0;j<=1000;j++){
				if (flag) break;
				for (int k=0;k<=1000;k++){
					int t;
					if (i==1) t=a[2]*j+a[3]*k;
					if (i==2) t=a[1]*j+a[3]*k;
					if (i==3) t=a[1]*j+a[2]*k;
					if (t==a[i]){flag=1; break;}
					if (t>a[i]) break;
				}
			}
			if (flag) ans--;
		}
		printf("%d\n",max(1,ans));
	}
}
namespace subtask3_4{
	int alb[10];
	bool dp(int x){
		bool f[1100];
		memset(f,0,sizeof(f));
		f[0]=1;
		for (int i=1;i<=n;i++){
			if (i==x) continue;
			f[alb[i]]=1;
		}
		for (int i=1;i<=alb[x];i++){
			for (int j=1;j<=n;j++){
				if (j==x) continue;
				for (int k=1;k<=1000;k++){
					if (alb[j]*k>i) break;
					f[i]|=f[i-alb[j]*k];
				}
			}
		}
		return f[alb[x]];
	}
	void Main(){
		int t=n;
		for (int i=1;i<=n;i++) scanf("%d",&alb[i]);
		for (int i=1;i<=n;i++){
			if (dp(i)) t--;
		}
		printf("%d\n",max(t,1));
	}
}
namespace subtask5_6{
	int alb[110];
	bool dp(int x){
		bool f[1100];
		memset(f,0,sizeof(f));
		f[0]=1;
		for (int i=1;i<=n;i++){
			if (i==x) continue;
			f[alb[i]]=1;
		}
		for (int i=1;i<=alb[x];i++){
			for (int j=1;j<=n;j++){
				if (j==x) continue;
				for (int k=1;k<=25000;k++){
					if (alb[j]*k>i) break;
					f[i]|=f[i-alb[j]*k];
				}
			}
		}
		return f[alb[x]];
	}
	void Main(){
		int t=n;
		for (int i=1;i<=n;i++) scanf("%d",&alb[i]);
		for (int i=1;i<=n;i++){
			if (dp(i)) t--;
		}
		printf("%d\n",max(t,1));
	}
}
int main(){
	judge();
	int t; scanf("%d",&t);
	while (t--){
		scanf("%d",&n);
		int wzp;
		if (n==1) scanf("%d",&wzp),printf("1\n");
		if (n==2) subtask1::Main();
		if (n==3) subtask2::Main();
		if (n==4||n==5) subtask3_4::Main();
		if (n>5) subtask5_6::Main();
	}
	return 0;
}