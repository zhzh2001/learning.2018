//lj&guo_away bless me to AK day1
#include<bits/stdc++.h>
using namespace std;
const int N=25000+7;
int n,ans,f[N];
bool flag[N];
inline int read()
{
	int num=0; char g=getchar();
	while(g<48||57<g) g=getchar();
	while(47<g&&g<58) num=(num<<1)+(num<<3)+g-48,g=getchar();
	return num;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t=read();
	while(t--)
	{
		memset(flag,0,sizeof(flag));
		n=read(),flag[0]=true,ans=0;
		for(int i=1;i<=n;i++) f[i]=read();
		sort(f+1,f+n+1);
		for(int i=1;i<=n;i++)
		{
			if(flag[f[i]]) continue; ans++;
			for(int x=0;x<=25000-f[i];x++) if(flag[x]) flag[x+f[i]]=true;
		}
		cout<<ans<<endl;
	}
	return 0;
}
