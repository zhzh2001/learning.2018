//lj&guo_away bless me to AK day1 
#include<bits/stdc++.h>
using namespace std;
const int N=1e5+7;
bool used[N],flag[N];
struct heaps{int a,b;}hep[N<<1],tmp;
int n,m,l,r,d,lft,rit,mmid,ans,siz,lntot,sum,hed,tot=1,p[N],f[N],fa[N],res[N],pos[N],rres[N];
struct edge{int a,b,c;}edg[N<<1];
inline void addedge(int a,int b,int c) {tot++,edg[tot].a=b,edg[tot].b=c,edg[tot].c=f[a],f[a]=tot;}
inline int read()
{
	int num=0; char g=getchar();
	while(g<48||57<g) g=getchar();
	while(47<g&&g<58) num=(num<<1)+(num<<3)+g-48,g=getchar();
	return num;
}
inline int Max(int a,int b) {if(a>b) return a; return b;}
inline void swap_heap(int a,int b) {tmp=hep[a],hep[a]=hep[b],hep[b]=tmp,pos[hep[a].b]=a,pos[hep[b].b]=b;}
bool cmp(int a,int b) {return a>b;}
inline void update(int u)
{
	while(u>1)
	{
		if(hep[u>>1].a<hep[u].a) swap_heap(u,u>>1),u=u>>1;
		else return;
	}
}
inline void pop(int u)
{
	while((u<<1)<=lntot)
	{
		if((u<<1)==lntot) swap_heap(u,u<<1),u=u<<1;
		else if(hep[u<<1].a>=hep[u<<1|1].a) swap_heap(u,u<<1),u=u<<1;
		else swap_heap(u,u<<1|1),u=u<<1|1;
	}
	hep[u].a=0;
}
inline void find(int u)
{
	used[u]=true;
	for(int i=f[u];i>0;i=edg[i].c) if(!used[edg[i].a]) fa[edg[i].a]=u,find(edg[i].a);
	sum=0,used[u]=false;
	for(int i=f[u];i>0;i=edg[i].c)
	{
		int x=edg[i].a;
		if(fa[u]==x) continue;
		if(edg[i].b+res[x]>=d) siz++;
		else p[++sum]=edg[i].b+res[x];
	}
	sort(p+1,p+sum+1,cmp);
	for(int i=1;i<=sum;i++)
	{
		lft=i+1,rit=sum,rres[i]=0;
		while(lft<=rit)
		{
			mmid=(lft+rit)>>1;
			if(p[i]+p[mmid]>=d) rres[i]=mmid,lft=mmid+1;
			else rit=mmid-1;
		}
	}
	hed=sum+1,lntot=0;
	for(int i=sum;i>0;i--)
	{
		if(!rres[i]) continue;
		for(int x=rres[i+1]+1;x<=rres[i];x++) if(!flag[x]) lntot++,pos[x]=lntot,hep[lntot].a=x,hep[lntot].b=x,update(lntot);
		if(hep[1].a<=i)  continue;
		else siz++,flag[i]=true,flag[hep[1].a]=true,pop(1),pop(pos[i]);
	}
	for(int i=1;i<=lntot;i++) hep[i].a=0,pos[i]=0;
	for(int i=sum;i>0;i--) if(!flag[i]) res[u]=p[i]; else flag[i]=false;
}
bool checkans()
{
	siz=0,memset(res,0,sizeof(res)); find(1);
	if(siz>=m) return true; return false;
}
int main()
{
	int a,b,c; l=0,r=5e8;
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<n;i++) a=read(),b=read(),c=read(),addedge(a,b,c),addedge(b,a,c); 
	while(l<=r)
	{
		d=(l+r)>>1;
		if(checkans()) ans=d,l=d+1;
		else r=d-1;
	}
	cout<<ans<<endl;
	return 0;
}
