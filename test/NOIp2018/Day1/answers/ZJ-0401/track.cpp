#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<vector>

using namespace std;
int f[200005],n,m,l,mid,r,sum;
int edgenum,vet[200005],nex[200005],val[200005],head[200005];

int read()
{
	char ch=getchar();
	int f=1,s=0;
	while (ch<'0' || ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0' && ch<='9') {s=s*10+ch-48;ch=getchar();}
	return f*s;
}
void addedge(int u,int v,int w)
{
	edgenum++;
	vet[edgenum]=v;
	nex[edgenum]=head[u];
	val[edgenum]=w;
	head[u]=edgenum;
}
void dfs(int u,int pre)
{
	multiset<int>S;
	for (int i=head[u];i!=0;i=nex[i])
	{
		int v=vet[i];
		if (v==pre) continue;
		dfs(v,u);
		if (f[v]+val[i]>=mid) sum++;
		else S.insert(f[v]+val[i]);
	}
	multiset<int>::iterator it;
	for (it=S.begin();it!=S.end();it=S.begin())
	{
		int x=*it;S.erase(it);
		multiset<int>::iterator it2=S.lower_bound(mid-x);
		if (it2==S.end()) {f[u]=max(f[u],x);continue;}
		if (x+*it2>=mid) sum++,S.erase(it2);
		else f[u]=max(x,f[u]);
	}
	return;
}
bool check()
{
	sum=0;
	for (int i=1;i<=n;i++)
		f[i]=0;
	dfs(1,0);
	return (sum>=m);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++)
	{
		int x=read(),y=read(),z=read();
		addedge(x,y,z);
		addedge(y,x,z);
		r+=z;
	}
	l=0;
	while (l<r)
	{
		mid=(l+r+1)>>1;
		if (check()) l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
