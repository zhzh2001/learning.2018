#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
int n,ans,a[100005];
bool f[100005];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while (cas--)
	{
		memset(f,false,sizeof(f));
		scanf("%d",&n);
		ans=0;f[0]=true;
		for (int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		for (int i=1;i<=n;i++)
		{
			if (f[a[i]]==0)
			{
				ans++;
				for (int j=a[i];j<=25000;j++)
					f[j]|=f[j-a[i]];
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
