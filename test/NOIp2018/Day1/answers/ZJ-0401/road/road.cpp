#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<queue>
using namespace std;
long long ans;
int n,m,a[200005],b[200005],lef[200005],rig[200005];
struct node
{
	int w,id;
}q[200005];
bool cmp(node t1,node t2)
{
	return t1.w>t2.w;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	a[0]=-1;
	for (int i=1;i<=n;i++)
		if (a[i]!=a[i-1]) m++,b[m]=a[i];
	n=m;
	for (int i=1;i<=n;i++)
	{
		a[i]=b[i];
		lef[i]=i-1;
		rig[i]=i+1;
		q[i].w=a[i];
		q[i].id=i;
	}
	sort(q+1,q+n+1,cmp);
	for (int i=1;i<n;i++)
	{
		int u=q[i].id;
		if (lef[u]==0)
		{
			ans+=a[u]-a[rig[u]];
			lef[rig[u]]=lef[u];
		}
		else if (rig[u]==n+1)
		{
			ans+=a[u]-a[lef[u]];
			rig[lef[u]]=rig[u];
		}
		else if (a[lef[u]]>a[rig[u]])
		{
			ans+=a[u]-a[lef[u]];
			rig[lef[u]]=rig[u];
			lef[rig[u]]=lef[u];
		}
		else
		{
			ans+=a[u]-a[rig[u]];
			rig[lef[u]]=rig[u];
			lef[rig[u]]=lef[u];
		}
	}
	ans+=a[q[n].id];
	printf("%lld\n",ans);
	return 0;
}
