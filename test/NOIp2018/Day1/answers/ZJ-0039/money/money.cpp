#include<bits/stdc++.h>
#define FOR(i,x,y) for(int i=(x),i##END=(y);i<=i##END;++i)
#define DOR(i,x,y) for(int i=(x),i##END=(y);i>=i##END;--i)
typedef long long LL;
using namespace std;
const int N=105;
const int M=25005;
int A[N],n,m;
bool dp[M];

int main()//file LL memory
{
//	printf("%.2lf\n",(&mmr-&mmr1)/1024.0/1024.0);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);
		FOR(i,1,n)scanf("%d",&A[i]);
		sort(A+1,A+1+n);
		memset(dp,0,sizeof(dp));
		dp[0]=1;
		m=0;
		FOR(i,1,n)if(!dp[A[i]])
		{
			FOR(j,A[i],M-3)
				dp[j]|=dp[j-A[i]];
			m++;
		}
		printf("%d\n",m);
	}
	return 0;
}
