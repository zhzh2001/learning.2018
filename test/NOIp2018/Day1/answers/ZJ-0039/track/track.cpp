#include<bits/stdc++.h>
#define FOR(i,x,y) for(int i=(x),i##END=(y);i<=i##END;++i)
#define DOR(i,x,y) for(int i=(x),i##END=(y);i>=i##END;--i)
typedef long long LL;
using namespace std;
const int N=5e4+5;
template<const int maxn,const int maxm>struct Linked_list
{
	int head[maxn],to[maxm],cost[maxm],nxt[maxm],tot;
	Linked_list(){clear();}
	void clear(){memset(head,-1,sizeof(head));tot=0;}
	void add(int u,int v,int w){to[++tot]=v,cost[tot]=w,nxt[tot]=head[u],head[u]=tot;}
	#define EOR(i,G,u) for(int i=G.head[u];~i;i=G.nxt[i])
};
Linked_list<N,N<<1>G;
int n,m,K,cnt,ans,W[N];

void Read(int &x)
{
	x=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar())x=(x<<3)+(x<<1)+ch-'0';
}
int dfs(int u,int f)
{
	multiset<int>S;
	multiset<int>::iterator it,it2;
	EOR(i,G,u)
	{
		int v=G.to[i],w=G.cost[i];
		if(v==f)continue;
		int tmp=dfs(v,u)+w;
		if(tmp>=K)cnt++;
		else S.insert(tmp);
	}
	for(it=S.begin();it!=S.end();)
	{
		it2=S.lower_bound(K-*it);
		if(it2!=S.end()&&it2!=it)
		{
			cnt++;
			S.erase(it2);
			it2=it;it++;
			S.erase(it2);
		}
		else it++;
	}
	if(S.empty())return 0;
	else return *--S.end();
}
bool check(int mid)
{
	cnt=0;
	K=mid;
	dfs(1,0);
	return cnt>=m;
}
int DFS(int u,int f)
{
	int mx1=0,mx2=0;
	EOR(i,G,u)
	{
		int v=G.to[i],w=G.cost[i];
		if(v==f)continue;
		int tmp=DFS(v,u)+w;
		if(tmp>mx1)mx2=mx1,mx1=tmp;
		else if(tmp>mx2)mx2=tmp;
	}
	ans=max(ans,mx1+mx2);
	return mx1;
}
bool Check(int K)
{
	FOR(u,1,n-1)EOR(i,G,u)
	{
		int v=G.to[i],w=G.cost[i];
		if(v==u+1)
		{
			W[u]=w;
			break;
		}
	}
	int sum=0,cnt=0;
	FOR(i,1,n-1)
	{
		sum+=W[i];
		if(sum>=K)
		{
			sum=0;
			cnt++;
		}
	}
	return cnt>=m;
}
bool Cmp(int x,int y){return x>y;}

int main()//file LL memory
{
//	printf("%.2lf\n",(&mmr2-&mmr1)/1024.0/1024.0);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	bool f=1,g=1;
	Read(n),Read(m);
	FOR(i,1,n-1)
	{
		int u,v,w;
		Read(u),Read(v),Read(w);
		if(u>v)swap(u,v);
		G.add(u,v,w),G.add(v,u,w);
		if(v!=u+1)f=0;
		if(u!=1)g=0;
	}
	if(m==1)
	{
		ans=0;
		DFS(1,0);
		printf("%d\n",ans);
		return 0;
	}
	else if(f)
	{
		int l=0,r=5e8+10;
		while(l<r)
		{
			int mid=(l+r+1)>>1;
			if(Check(mid))
				l=mid;
			else r=mid-1;
		}
		printf("%d\n",l);
		return 0;
	}
	else if(g)
	{
		int tot=0,t=m,miner=2e9;
		EOR(i,G,1)W[++tot]=G.cost[i];
		sort(W+1,W+1+tot,Cmp);
		FOR(i,m+1,tot)
		{
			if(t==0)break;
			W[t]+=W[i];
			t--;
		}
		FOR(i,1,m)miner=min(miner,W[i]);
		printf("%d\n",miner);
		return 0;
	}
	int l=1,r=5e8+10;
	while(l<r)
	{
		int mid=(l+r+1)>>1;
		if(check(mid))
			l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
