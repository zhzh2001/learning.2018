#include<cstdio>
using namespace std;
const int maxn=100005;
int a[maxn],n,ans;
int _read(){
	int num=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') num=num*10+ch-48,ch=getchar();
	return num;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=_read();for (int i=1;i<=n;i++) a[i]=_read();
	int lst=0;
	for (int i=1;i<=n;i++)
	if (a[i]<=lst) lst=a[i];else ans+=a[i]-lst,lst=a[i];
	printf("%d\n",ans);
	return 0;
}
