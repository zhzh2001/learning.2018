#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=105,maxm=25005;
int n,a[maxn],f[maxm],T,m,ans;
void work(){
	scanf("%d",&n);m=0;ans=n;
	for (int i=1;i<=n;i++) scanf("%d",&a[i]),m=max(m,a[i]);
	memset(f,0,sizeof(f));f[0]=1;
	sort(a+1,a+1+n);
	for (int i=1;i<=n;i++){
		if (f[a[i]]){ans--;continue;}
		for (int j=a[i];j<=m;j++) f[j]|=f[j-a[i]];
	}
	printf("%d\n",ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);while(T--) work();
	return 0;
}
