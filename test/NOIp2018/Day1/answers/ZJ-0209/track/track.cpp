#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=50005;
int tot,w[maxn*2],nxt[maxn*2],son[maxn*2],lnk[maxn];
int n,m,f[maxn],a[maxn],c[maxn],ans,top,sb;
struct jz{
	int x,y;
}b[maxn];
int _read(){
	int num=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') num=num*10+ch-48,ch=getchar();
	return num;
}
void add(int x,int y,int z){nxt[++tot]=lnk[x];lnk[x]=tot;son[tot]=y;w[tot]=z;}
void DFS(int x,int fa,int sx){
	for (int j=lnk[x];j;j=nxt[j]) if (son[j]!=fa) DFS(son[j],x,sx);
	top=0;for (int j=lnk[x];j;j=nxt[j]) if (son[j]!=fa) a[++top]=f[son[j]]+w[j],c[top]=1;
	if (top) sort(a+1,a+1+top);int l=1;sb=0;
	for (int i=top;i>=1;i--)
	if (a[i]>=sx) c[i]=0,ans++;else{
		while(l<i){
			if (a[l]+a[i]>=sx){
				c[l]=0;c[i]=0;b[++sb].x=l;b[sb].y=i;
				ans++;l++;break;
			}
			l++;
		}
	}
	f[x]=0;for (int i=1;i<=top;i++) if (c[i]) f[x]=a[i];
	for (int i=sb;i>=1;i--) if (f[x]+a[b[i].x]>=sx) f[x]=a[b[i].y];
}
bool check(int x){ans=0;DFS(1,0,x);return ans>=m;}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=_read();m=_read();
	for (int i=1;i<n;i++){
		int x=_read(),y=_read(),z=_read();
		add(x,y,z);add(y,x,z);
	}
	int L=1,R=5e8;
	while(L<=R){
		int mid=L+(R-L>>1);
		if (check(mid)) L=mid+1;else R=mid-1;
	}
	printf("%d\n",R);
	return 0;
}
