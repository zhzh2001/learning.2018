#include <bits/stdc++.h>
using namespace std;
const int N = 5e5;
int n, ai[N], st[N], top, res;
int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; ++ i)
		scanf("%d", &ai[i]);
	for (int i = 1; i <= n + 1; ++ i)
	{
		while (ai[i] < st[top])
			res += st[top] - max(ai[i], st[top - 1]), 
			top --;
		st[++ top] = ai[i];
	}
	printf("%d\n", res);
}
