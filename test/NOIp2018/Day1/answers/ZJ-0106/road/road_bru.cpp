#include <bits/stdc++.h>
using namespace std;
const int N = 5e5;
int n, ai[N];
long long res;
int mn[20][N];
int mnn(int l, int r)
{
	int g = log2(r - l + 1);
	if (ai[mn[g][l]] < ai[mn[g][r - (1 << g) + 1]])
		return mn[g][l];
	else return mn[g][r - (1 << g) + 1];
}
int solve(int l, int r)
{
	int id = mnn(l, r);
	if (l < id) res += solve(l, id - 1) - ai[id];
	if (id < r) res += solve(id + 1, r) - ai[id];
	return ai[id];
}
int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.bru", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; ++ i)
		scanf("%d", &ai[i]);
	for (int j = 1; j <= n; ++ j)
		mn[0][j] = j;
	for (int i = 1; i < 20; ++ i)
		for (int j = 1; j <= n; ++ j)
			if (ai[mn[i - 1][j]] < ai[mn[i - 1][j + (1 << (i - 1))]])
				mn[i][j] = mn[i - 1][j];
			else mn[i][j] = mn[i - 1][j + (1 << (i - 1))];
	res += solve(1, n);
	printf("%lld\n", res);
}
