#include <bits/stdc++.h>
using namespace std;
const int N = 200;
const int M = 26000;
int T;
int n, ai[N], ok[M], res;
int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	while (T --)
	{
		scanf("%d", &n); res = 0;
		for (int i = 1; i <= n; ++ i)
			scanf("%d", &ai[i]);
		sort(ai + 1, ai + n + 1);
		for (int i = 0; i <= ai[n]; ++ i) ok[i] = 0;
		ok[0] = 1;
		for (int i = 1; i <= n; ++ i)
			if (!ok[ai[i]])
			{
				res ++;
				for (int j = ai[i]; j <= ai[n]; ++ j)
					if (ok[j - ai[i]]) ok[j] = 1;
			}
		printf("%d\n", res);
	}
}
