#include <bits/stdc++.h>
using namespace std;

const int N = 1e5;
int n, m;
int su, bd, ct;
int F[N], eg[N];
vector <int> bi[N], ci[N];

void dfs(int t, int f)
{
	F[t] = 0;
	for (int i = 0; i < bi[t].size(); ++ i)
		if (bi[t][i] != f)
			dfs(bi[t][i], t);
	int c = 0, nw;
	for (int i = 0; i < bi[t].size(); ++ i)
		if (bi[t][i] != f)
		{
			int v = ci[t][i] + F[bi[t][i]];
			if (v >= bd) ct ++;
			else eg[++ c] = v;
		}
	sort(eg + 1, eg + c + 1);
	{
		int l = 0, r = c / 2;
		while (l < r)
		{
			int i = (l + r + 1) / 2;
			int f = 1;
			for (int j = 1; j <= i; ++ j)
				if (eg[c - j + 1] + eg[c - i * 2 + j] < bd)
					f = 0;
			if (f) l = i;
			else r = i - 1;
		}
		nw = l;
		ct += l;
	}
	{
		int l = 0, r = c;
		while (l < r)
		{	
			int i = (l + r + 1) / 2;
			int f = 1;
			for (int j = 1; j <= nw; ++ j)
			{
				int p = c - j + 1, q = c - nw * 2 + j;
				if (p <= i) p --; if (q <= i) q --;
				if (eg[p] + eg[q] < bd) f = 0;
			}
			if (f) l = i;
			else r = i - 1;
		}
		F[t] = eg[l];
		
	}
}
int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i < n; ++ i)
	{
		int a, b, l;
		scanf("%d%d%d", &a, &b, &l);
		bi[a].push_back(b); ci[a].push_back(l);
		bi[b].push_back(a); ci[b].push_back(l);
		su += l;
	}
	int l = 1, r = su / m;
	while (l < r)
	{
		bd = (l + r + 1) / 2;
		ct = 0;
		dfs(1, 0);
		// cerr << bd << " " << ct << endl;
		if (ct >= m) l = bd;
		else r = bd - 1;
	}
	printf("%d\n", l);
}
