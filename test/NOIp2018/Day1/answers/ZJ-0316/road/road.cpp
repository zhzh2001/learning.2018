#include <bits/stdc++.h>
using namespace std;

inline char nextchar() {
  static char buf[100000], *p1 = buf, *p2 = buf;
  return (p1 == p2) &&
    (p2 = (p1 = buf) + fread(buf, 1, 100000, stdin), p1 == p2) ? EOF : *p1++;
}

inline void read(int &x) {
  x = 0; bool sign = false; char ch = 0;
  while (!isdigit(ch)) { sign |= (ch == '-'); ch = nextchar(); }
  while (isdigit(ch)) { x = x * 10 + (ch ^ 48); ch = nextchar(); }
  x = sign ? -x : x;
}

inline void write(int x) {
  if (x == 0) { putchar('0'); return; }
  int stk[100], top = 0;
  if (x < 0) { x = -x; putchar('-'); }
  while (x) { stk[++top] = x % 10; x /= 10; }
  while (top) putchar(stk[top--] + '0');
}

#define N 100010
#define INF 0x7f7f7f7f
int n, d[N], notZero, ans;

inline int solve(int l, int r) {
  if (l > r) return 0;
  if (l == r) return d[l];
  if (!notZero) return 0;
  int idx = 0;
  for (int i = l; i <= r; ++i)
    if (d[i] == 0) { idx = i; break; }
  int ret = 0;
  if (idx == 0) {
    int minn = INF;
    for (int i = l; i <= r; ++i) minn = min(minn, d[i]);
    for (int i = l; i <= r; ++i) {
      if (d[i] == minn) {
        d[i] = 0;
        notZero--;
      } else d[i] -= minn;
    }
    ret += minn;
    ret += solve(l, r);
  } else {
    ret += solve(l, idx - 1);
    ret += solve(idx + 1, r);
  }
  return ret;
}

int main() {
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
  read(n);
  for (int i = 1; i <= n; ++i) read(d[i]);
  for (int i = 1; i <= n; ++i) if (d[i] != 0) notZero++;
  if (notZero == 0) { write(0); return 0; }
  write(solve(1, n));
  return 0;
}
