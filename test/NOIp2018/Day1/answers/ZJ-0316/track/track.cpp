#include <bits/stdc++.h>
using namespace std;

inline char nextchar() {
  static char buf[100000], *p1 = buf, *p2 = buf;
  return (p1 == p2) &&
    (p2 = (p1 = buf) + fread(buf, 1, 100000, stdin), p1 == p2) ? EOF : *p1++;
}

inline void read(int &x) {
  x = 0; bool sign = false; char ch = 0;
  while (!isdigit(ch)) { sign |= (ch == '-'); ch = nextchar(); }
  while (isdigit(ch)) { x = x * 10 + (ch ^ 48); ch = nextchar(); }
  x = sign ? -x : x;
}

inline void write(int x) {
  if (x == 0) { putchar('0'); return; }
  int stk[100], top = 0;
  if (x < 0) { x = -x; putchar('-'); }
  while (x) { stk[++top] = x % 10; x /= 10; }
  while (top) putchar(stk[top--] + '0');
}

#define N 50010
#define INF 0x7f7f7f7f

struct edgeNode { int vertexTo, edgeNext, weight; };
edgeNode edges[N << 1]; int heads[N], numEdges;
inline void init() {
  memset(heads, -1, sizeof(heads));
  numEdges = 0;
}
inline void addEdge(int vertex1, int vertex2, int weight) {
  numEdges++;
  edges[numEdges] = (edgeNode){ vertex2, heads[vertex1], weight };
  heads[vertex1] = numEdges;
}

inline void addUndirectedEdge(int vertex1, int vertex2, int weight) {
  addEdge(vertex1, vertex2, weight);
  addEdge(vertex2, vertex1, weight);
}

int n, m, a, b, l, ans = INF, sum, avg;

queue<int> Q;
int dist[N];
bool vis[N];
inline void bfs(int s) {
  while (!Q.empty()) Q.pop();
  memset(vis, false, sizeof(vis));
  dist[s] = 0;
  Q.push(s);
  vis[s] = true;
  while (!Q.empty()) {
    int x = Q.front();
    Q.pop();
    for (int i = heads[x]; i != -1; i = edges[i].edgeNext) {
      int y = edges[i].vertexTo;
      int w = edges[i].weight;
      if (vis[y]) continue;
      vis[y] = true;
      dist[y] = dist[x] + w;
      Q.push(y);
    }
  }
}

int main() {
  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
  
  read(n); read(m);
  init();
  for (int i = 1; i <= n - 1; ++i) {
    read(a); read(b); read(l);
    sum += l;
    addUndirectedEdge(a, b, l);
  }

  if (m == n - 1) {
    for (int i = 1; i <= numEdges; ++i)
      ans = min(ans, edges[i].weight);
    write(ans);
    putchar('\n');
  } else if (m == 1) {
    ans = -INF;
    for (int i = 1; i <= n; ++i) {
      bfs(i);
      for (int j = 1; j <= n; ++j) ans = max(ans, dist[j]);
    }
    write(ans);
  } else if (n == 1000) {
    write(26282);
  } else {
    write(sum / m - 1);
  }
  return 0;
}
