#include <bits/stdc++.h>
using namespace std;

inline char nextchar() {
  static char buf[100000], *p1 = buf, *p2 = buf;
  return (p1 == p2) &&
    (p2 = (p1 = buf) + fread(buf, 1, 100000, stdin), p1 == p2) ? EOF : *p1++;
}

inline void read(int &x) {
  x = 0; bool sign = false; char ch = 0;
  while (!isdigit(ch)) { sign |= (ch == '-'); ch = nextchar(); }
  while (isdigit(ch)) { x = x * 10 + (ch ^ 48); ch = nextchar(); }
  x = sign ? -x : x;
}

inline void write(int x) {
  if (x == 0) { putchar('0'); return; }
  int stk[100], top = 0;
  if (x < 0) { x = -x; putchar('-'); }
  while (x) { stk[++top] = x % 10; x /= 10; }
  while (top) putchar(stk[top--] + '0');
}

#define N 110
#define A 25010
int t, n, a[N], ans;
bool has[A], sum[A];

int main() {
  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
  read(t);
  while (t--) {
    memset(has, false, sizeof(has));
    memset(sum, false, sizeof(sum));
    read(n);
    for (int i = 1; i <= n; ++i) read(a[i]);
    if (n <= 1) { write(n); putchar('\n'); continue; }
    sort(a + 1, a + n + 1);
    for (int i = 1; i <= n; ++i) has[a[i]] = true;
    for (int i = 1; i <= n; ++i) sum[a[i]] = true;
    for (int i = 1; i <= n; ++i)
      for (int j = 2; j * a[i] <= a[n]; ++j)
        sum[j * a[i]] = true;
    for (int i = 1; i <= a[n]; ++i) {
      if (!sum[i]) continue;
      for (int j = i + 1; j <= a[n] - i; ++j) {
        if (!sum[j]) continue;
        sum[i + j] = true;
        has[i + j] = false;
      }
    }
    for (int i = 1; i <= n; ++i)
      for (int j = 2; j * a[i] <= a[n]; ++j)
        has[j * a[i]] = false;
    ans = 0;
    for (int i = 1; i <= a[n]; ++i) if (has[i]) ans++;
    write(ans);
    putchar('\n');
  }
  return 0;
}
