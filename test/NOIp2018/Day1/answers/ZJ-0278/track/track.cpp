#include<iostream>
#include<cstdio>
#include<vector>
#include<queue>
#include<memory.h>
using namespace std;
struct node
{
	int to,dis;
	node(){};
	node(int x,int y):to(x),dis(y){};
};
int n,m,f[50005],b[50005],c[50005];
vector<node> a[50005];
void bfs(int x)
{
	queue<int> p;
	while (!p.empty()) p.pop();
	memset(f,0,sizeof(f));
	memset(b,0,sizeof(b));
	b[x]=1;
	p.push(x);
	while (!p.empty())
	{
		int v=p.front();
		p.pop();
		for (int i=0;i<a[v].size();i++)
		{
			if (b[a[v][i].to]==1) continue;
			b[a[v][i].to]=1;
			f[a[v][i].to]=f[v]+a[v][i].dis;
			p.push(a[v][i].to);
		}
	}
}
void fun_zhijing()
{
	int x,y,z;
	for (int i=1;i<n;i++)
	{
		scanf("%d %d %d",&x,&y,&z);
		a[x].push_back(node(y,z));
		a[y].push_back(node(x,z));
	}
	bfs(1);
	x=0,y=1;
	for (int i=1;i<=n;i++)
	{
		if (f[i]>x) 
		{
			x=f[i];
			y=i;
		}
	}
	bfs(y);
	x=0;
	for (int i=1;i<=n;i++)
	{
		x=max(x,f[i]);
	}
	cout<<x;
}
bool judge(int x)
{
	int y=0,z=0;
	for (int i=1;i<n;i++)
	{
		y+=c[i];
		if (y>=x) 
		{
			y=0;
			z++;
		}
	}
	return z>=m;
}
int erfen(int l,int r)
{
	while (l<=r)
	{
		int m=(l+r)/2;
		if (judge(m)) l=m+1;
		else r=m-1;
	}
	return r;
}
void fun_lian()
{
	int x,y,z,sum=0;
	for (int i=1;i<n;i++) 
	{
		scanf("%d %d %d",&x,&y,&z);
		c[x]=z;
		sum+=z;
	}
	cout<<erfen(0,sum/m+1);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	if (m==1) fun_zhijing();
	else fun_lian();
}
