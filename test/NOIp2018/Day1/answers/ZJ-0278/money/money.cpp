#include<iostream>
#include<cstdio>
#include<algorithm>
#include<memory.h>
using namespace std;
int n,a[105],f[25005];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	cin>>T;
	while (T--)
	{
		cin>>n;
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		int ans=n;
		memset(f,0,sizeof(f));
		f[0]=1;
		for (int i=1;i<=n;i++)
		{
			if (f[a[i]]==1) 
			{
				ans--;
				continue;
			}
			for (int j=a[i];j<=a[n];j++)
			{
				f[j]=f[j]|f[j-a[i]];
			}
		}
		cout<<ans<<'\n';
	}
}
