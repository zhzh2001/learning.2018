#include <cstdio>
#include <cstring>

int t, n, i, j, k, l, r, mi, a[105], xx, yy, ans = 0, v[25005];

inline void swap(int &a, int &b){
	int t = a; a = b; b = t;
}

int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	for (scanf("%d", &t); t > 0; --t){
		scanf("%d", &n);
		if (n == 1){
			scanf("%*d");
			printf("1\n");
		}else if (n == 2){
			scanf("%d%d", &xx, &yy);
			if ((xx % yy == 0)||(yy % xx == 0)) printf("1\n");
			else printf("2\n");
		}else{
			for (i = 0; i < n; ++i) scanf("%d", &a[i]);
			for (i = 0; i < n; ++i){
				mi = i;
				for (j = i + 1; j < n; ++j) if (a[j] < a[mi]) mi = j;
				swap(a[mi], a[i]);
			}for (i = 1; i < n; ++i) if (a[i] % a[1] != 0) i = 1000;
			if (i < 110) printf("1\n");
			else{
				memset(v, 0, sizeof(v));
				for (ans = i = 0; i < n; ++i){
					if (!v[a[i]]){
						v[a[i]] = 1;
						for (j = 1; j <= a[n - 1] - a[i]; ++j) if (v[j]) v[j + a[i]] = 1;
						++ans;
					}
				}printf("%d\n", ans);
			}
		}
	}fclose(stdin);
	fclose(stdout);
	return 0;
}
