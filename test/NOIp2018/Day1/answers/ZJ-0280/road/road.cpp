#include <cstdio>

int n, i, a[100005], ans = 0;

int solve(int l, int r){
	if (l == r) return a[l];
	if (l > r) return 0;
	int i, mi = a[r], p = r;
	for (i = l; i <= r; ++i)
		if (a[i] < mi){
			mi = a[i];
			p = i;
		}
	for (i = l; i <= r; ++i) a[i] -= mi;
	return solve(l, p - 1) + mi + solve(p + 1, r);
}

int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (i = 0; i < n; ++i) scanf("%d", &a[i]);
	ans = solve(0, n - 1);
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
