#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

using namespace std;

int map[101][101], m, n, i, j, k, a, b, l, bs = 0, d[50005], dd[50005], ans;

struct Edge{
	int fr, to, di;
}ed[50005];
void added(int fr, int to, int di){
	ed[bs].fr = fr;
	ed[bs].to = to;
	ed[bs].di = di;
	++bs;
};


int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m); ans = 0;
	if ((n <= 100)&&(m == 1)){
		for (i = 1; i <= n; ++i)
			for (j = 1; j <= n; ++j) map[i][j] = 1 << 28;
		for (i = 1; i <= n; ++i) map[i][i] = 0;
		for (i = 1; i < n; ++i){
			scanf("%d%d%d", &a, &b, &l);
			map[a][b] = map[b][a] = l;
		}for (k = 1; k <= n; ++k)
			for (i = 1; i <= n; ++i)
				for (j = 1; j <= n; ++j)
					if (map[i][j] > map[i][k] + map[k][j]) map[i][j] = map[i][k] + map[k][j];
		for (i = 1; i <= n; ++i)
			for (j = 1; j <= n; ++j)
				if (ans < map[i][j]) ans = map[i][j];
		printf("%d\n", ans);
	}else{
		for (i = 1; i < n; ++i){
			scanf("%d%d%d", &a, &b, &l);
			added(a, b, l);
		}added(0, 0, 0); --bs;
		for (i = 0; i < bs; ++i) if (ed[i].fr != 1) i = 100000;
		if (i > 60000){
			for (int i = 0; i < bs; ++i) d[i] = -ed[i].di;
			sort(d, d + bs);
			printf("%d", -d[m - 3]);
		}else{
			for (i = 0; i < bs; ++i) d[i] = -ed[i].di;
			sort(d, d + bs);
			if (m == 1) printf("%d\n", - d[0] - d[1]);
			else{
				if (m < n / 2){
					ans = d[0] + d[m + m - 1];
					for (i = 1; i < m; ++i)
						if (ans < d[i] + d[m + m - 1 - i]) ans = d[i] + d[m + m - 1 - i];
					printf("%d", -ans);
				}else{
					ans = d[m - 1];
					for (i = n - m - 1; i >= 0; --i){
						if (d[n - 2 - i - i] < ans) ans = d[n - 2 - i - i];
						for (j = 0; j < i; ++j)
							if (ans < d[n - 1 - i - i + j] + d[n - 1 - j])
								ans = d[n - 1 - i - i + j] + d[n - 1 - j];
					}printf("%d", -ans);
				}
			}
		}
	}fclose(stdin);
	fclose(stdout);
	return 0;
}
