#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int N = 5e4 + 10;
const int M = 1e5 + 10;
int fi[N], di[M], ne[M], da[M], ru[N], ma, ma_id, L = 1, m, n;
bool v[M];
inline int re()
{
	register int x = 0;
	register char c = getchar();
	register bool p = 0;
	for (; c < '0' || c > '9'; c = getchar())
		p |= c == '-';
	for (; c >= '0' && c <= '9'; c = getchar())
		x = x * 10 + (c - '0');
	return p ? -x : x;
}
inline void add(int x, int y, int z)
{
	di[++L] = y;
	da[L] = z;
	ne[L] = fi[x];
	fi[x] = L;
}
void dfs_1(int x, int fa, int d)
{
	int i, y;
	if (d > ma)
	{
		ma = d;
		ma_id = x;
	}
	for (i = fi[x]; i; i = ne[i])
		if ((y = di[i]) ^ fa)
			dfs_1(y, x, d + da[i]);
}
bool dfs_2(int x, int fa, int mid, int d, int k)
{
	if (d >= mid)
	{
		d = 0;
		k++;
	}
	if (k == m)
		return true;
	int i, y;
	for (i = fi[x]; i; i = ne[i])
		if ((y = di[i]) ^ fa)
			if (dfs_2(y, x, mid, d + da[i], k))
				return true;
	return false;
}
bool dfs(int x, int fa, int mid, int d)
{
	if (d >= mid)
		return true;
	for (int i = fi[x]; i; i = ne[i])
		if (!v[i])
		{
			v[i] = v[i ^ 1] = 1;
			if (dfs(di[i], x, mid, d + da[i]))
				return true;
			v[i] = v[i ^ 1] = 0;
		}
	return false;
}
bool judge(int mid)
{
	memset(v, 0, sizeof(v));
	int s = 0;
	for (int i = 1; i <= n; i++)
		if (dfs(i, 0, mid, 0))
			s++;
	return s >= m;
}
int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	int i, x, y, z, s = 0;
	bool p = 0;
	n = re();
	m = re();
	for (i = 1; i < n; i++)
	{
		x = re();
		y = re();
		z = re();
		if (x + 1 != y)
			p = 1;
		add(x, y, z);
		add(y, x, z);
		ru[x]++;
		ru[y]++;
		s += z;
	}
	if (m == 1)
	{
		dfs_1(1, 0, 0);
		ma = 0;
		dfs_1(ma_id, 0, 0);
		printf("%d", ma);
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if (!p)
	{
		for (i = 1; i <= n; i++)
			if (ru[i] == 1)
				break;
		int l, r, mid, an;
		l = an = 0;
		r = s / m;
		while (l <= r)
		{
			mid = (l + r) >> 1;
			if (dfs_2(i, 0, mid, 0, 0))
			{
				an = mid;
				l = mid + 1;
			}
			else
				r = mid - 1;
		}
		printf("%d", an);
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	int l, r, mid, an;
	l = an = 0;
	r = s / m;
	while (l <= r)
	{
		mid = (l + r) >> 1;
		if (judge(mid))
		{
			an = mid;
			l = mid + 1;
		}
		else
			r = mid - 1;
	}
	printf("%d", an);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
