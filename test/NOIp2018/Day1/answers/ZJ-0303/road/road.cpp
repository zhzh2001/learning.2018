#include<cstdio>
using namespace std;
typedef long long ll;
const int N = 1e5 + 10;
int a[N];
inline int re()
{
	register int x = 0;
	register char c = getchar();
	register bool p = 0;
	for (; c < '0' || c > '9'; c = getchar())
		p |= c == '-';
	for (; c >= '0' && c <= '9'; c = getchar())
		x = x * 10 + (c - '0');
	return p ? -x : x;
}
inline int maxn(register int x, register int y){ return x > y ? x : y; }
ll dfs(register int m, int o[])
{
	if (!m)
		return 0;
	a[m + 1] = 0;
	register int ma = 0, x = 0, i, k = 0, L = 0;
	register ll s = 0;
	for (i = 1; i <= m; i++)
	{
		ma = maxn(ma, a[i]);
		if (a[i - 1] > a[i] && a[i] < a[i + 1])
		{
			s += ma - maxn(k, a[i]);
			ma = 0;
			k = a[i];
			x = maxn(x, a[i]);
			a[++L] = a[i];
			if (a[L] == a[L - 1])
				L--;
		}
	}
	s += ma - k;
	return s + dfs(L, a);
}
int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	register int i, n, m = 0;
	n = re();
	for (i = 1; i <= n; i++)
	{
		a[++m] = re();
		if (a[m] == a[m - 1])
		{
			m--;
			continue;
		}
	}
	printf("%lld", dfs(m, a));
	fclose(stdin);
	fclose(stdout);
	return 0;
}
