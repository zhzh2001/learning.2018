#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int N = 110;
const int M = 2.5e4 + 10;
int a[N], m;
bool v[M];
inline int re()
{
	register int x = 0;
	register char c = getchar();
	register bool p = 0;
	for (; c < '0' || c > '9'; c = getchar())
		p |= c == '-';
	for (; c >= '0' && c <= '9'; c = getchar())
		x = x * 10 + (c - '0');
	return p ? -x : x;
}
bool judge(register int x, register int mid, register int s, register int o)
{
	v[s] = 1;
	if (s == o || v[o - s])
		return true;
	if (x > mid)
		return false;
	for (register int i = 0; i * a[x] + s <= o; i++)
		if (judge(x + 1, mid, s + i * a[x], o))
			return true;
	return false;
}
int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	register int i, j, n, t, an;
	t = re();
	while (t--)
	{
		n = re();
		for (i = 1; i <= n; i++)
			a[i] = re();
		m = 0;
		sort(a + 1, a + n + 1);
		for (i = 1; i < n; i++)
			if (a[i])
				for (j = i + 1; j <= n; j++)
					if (a[j] && !(a[j] % a[i]))
						a[j] = 0;
		for (i = 1; i <= n; i++)
			if (a[i])
				a[++m] = a[i];
		memset(v, 0, sizeof(v));
		an = m;
		for (i = 2; i <= m; i++)
			if (judge(1, i - 1, 0, a[i]))
			{
				v[a[i]] = 1;
				an--;
			}
		printf("%d\n", an);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
