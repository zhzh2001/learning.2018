#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline int cmp1(const int&a,const int&b){return(a<b);}
int n,m,a[50050],b[50050],c[50050],power[50050],lnum,head[100010];
ll dp1[50050][2],lsum;
bool vis[50050];
struct line{
	int fro,to,next,po;
};  line lx[100010];
inline void addl(int x,int y,int p){
	lnum++;
	lx[lnum].to=y; lx[lnum].fro=x; lx[lnum].po=p;
	lx[lnum].next=head[x]; head[x]=lnum;
}
inline void dfs1(int k){
	vis[k]=true;
	int t=head[k]; dp1[k][0]=dp1[k][1]=0;
	ll max0=0,max1=0,max2=0;
	while(t!=0){
		int b=lx[t].to;
		if(!vis[b]){
			dfs1(b); max0=max(max0,dp1[b][0]);
			if(dp1[b][1]+lx[t].po>max1){
				max2=max1; max1=dp1[b][1]+lx[t].po;
			}else{
				if(dp1[b][1]+lx[t].po>max2)max2=dp1[b][1]+lx[t].po;
			}
		}
		t=lx[t].next;
	}
	dp1[k][0]=max(max0,max1+max2); dp1[k][1]=max1;
}
inline bool check(ll k){
	ll shosum=0; int temnum=0;
	for(int i=1;i<=n-1;++i){
		shosum+=c[i];
		if(shosum>=k){
			temnum++;
			shosum=0;
		}
	}
	if(temnum>=m)return true;
	return false;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	ll inf=(1<<28); inf=inf*inf; //cout<<inf;
	scanf("%d%d",&n,&m);
	ll suma=0,dta=0;
	for(int i=1;i<=n-1;++i){
		scanf("%d%d%d",&a[i],&b[i],&power[i]);
		suma+=min(a[i],b[i]);
		dta+=abs(a[i]-b[i]);
	}
	if(suma==n-1){
		sort(power+1,power+n,cmp1);
		ll ans=inf;
		while(m*2>n-1){
			ans=min(ans,(ll)power[n-1]);
			n--; m--;
		}
		for(int i=n-1;i>=n-m;--i){
			//cout<<i<<" "<<n-2*m+(n-1)-i<<endl;
			ans=min(ans,(ll)power[i]+power[n-2*m+(n-1)-i]);
		}
		cout<<ans<<endl;
		return 0;
	}
	if(dta==n-1){
		lsum=0;
		for(int i=1;i<=n-1;++i){
			c[min(a[i],b[i])]=power[i];
			lsum+=power[i];
		}
		lsum/=m;
		ll l=1,r=lsum,ans=1;
		while(l<=r){
			ll mid=(l+r)/2;
			if(check(mid)){
				ans=mid; l=mid+1;
			}else r=mid-1;
		}
		cout<<ans<<endl;
		return 0;
	}
	if(m==1){
		lnum=0;
		for(int i=1;i<=n;++i)vis[i]=false;
		for(int i=1;i<=n-1;++i){
			addl(a[i],b[i],power[i]);
			addl(b[i],a[i],power[i]);
		}
		dfs1(1); cout<<dp1[1][0]<<endl;
		return 0;
	}
	
  return 0;
}
