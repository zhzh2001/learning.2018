#include<cstdio>
#define rep(i,l,r) for(int i=l;i<=r;++i)
using namespace std;
int dis[1005][1005];
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	rep(i,1,m)
	{
		int a,b,l;
		scanf("%d%d%d",&a,&b,&l);
		dis[a][b]=l;
	}
	rep(i,1,n)
		rep(j,1,n)
			rep(k,1,n)
				dis[j][k]=dis[j][i]+dis[i][k]<dis[j][k]?dis[j][i]+dis[i][k]:dis[j][k];
	int ans=0;
	rep(i,1,n)
		rep(j,1,n)
			ans=dis[i][j]>ans?dis[i][j]:ans;
	printf("%d",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
