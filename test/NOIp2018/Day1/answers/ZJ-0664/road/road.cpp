#include<cstdio>
#include<algorithm>
#define rep(i,l,r) for(int i=l;i<=r;++i)
using namespace std;
int d[100005];
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n;
	scanf("%d",&n);
	rep(i,1,n)
	{
		scanf("%d",&d[i]);
	}
	int ans=0;
	rep(i,1,n)
	{
		int min=20000,num=0;
		rep(j,1,n)
			if((d[j]<min) && (d[j]!=0))
			{
				min=d[j];
				num=j;
			}
		if(!num)break;
		ans+=min;
		int t=num-1;
		while((d[t]!=0)&&(t>0))
		{
			d[t]-=d[num];
			--t;
		}
		t=num+1;
		while((d[t]!=0)&&(t<=n))
		{
			d[t]-=d[num];
			++t;
		}
		d[num]=0;
	}
	printf("%d",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
