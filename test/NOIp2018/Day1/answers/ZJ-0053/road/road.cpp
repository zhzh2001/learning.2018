#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
using namespace std;
int n;
long long a[100010];
long long q[100010];
long long ans;
int top,hed;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	  scanf("%d",&a[i]);
	top=1;
	q[1]=a[1];
	for(int i=2;i<=n;i++)
	{
		while(a[i]<q[top] && top>1)
		{
			ans+=q[top]-max(q[top-1],a[i]);
			top--;
		}
		if(top==1 && a[i]<q[1])
		{
			ans+=q[1]-a[i];
			q[1]=a[i];
		}
		else
		{
			top++;
			q[top]=a[i];
		}
	}
	for(int i=top;i>1;i--)
	{
		ans+=q[i]-q[i-1];
	}
	ans+=q[1];
	printf("%lld\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
