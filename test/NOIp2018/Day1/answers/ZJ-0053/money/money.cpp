#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <queue>
#include <vector>
using namespace std;
int t,n,flag=0,top,Max;
int a[110];
bool f[30010];
bool cmp(int x,int y)
{
	return x<y;
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d",&n);
		flag=0;
		for(int i=1;i<=n;i++)
		{
		    scanf("%d",&a[i]);
		    if(a[i]==1)
		      flag=1;
		}
		if(flag)
		{
			printf("1\n");
			continue;
		}
		sort(a+1,a+1+n,cmp);
		Max=a[n];
        memset(f,0,sizeof(f));
        top=1;
        for(int i=a[1];i<=Max;i+=a[1])
          f[i]=1;
        for(int i=2;i<=n;i++)
        {
        	if(!f[a[i]])
        	{
        		top++;
        		f[a[i]]=1;
        		for(int j=a[1];j<=Max-a[i];j++)
        		{
        			
					if(f[j]==true)
        			  f[j+a[i]]=1;
        		}
			}
		}
		printf("%d\n",min(top,n));
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
