#include <bits/stdc++.h>

typedef int INT;

const int N = 100 + 10;
const int M = 2500000 + 10;

int T, n;

int a[N], b[N], sum;

int k[N];

int dp[M];

int ans;

int read()
{
	int sum = 0;
	char c = getchar(), f = c;
	while(c < '0' || c > '9') f = c, c = getchar();
	while(c >= '0' && c <= '9') sum = sum * 10 + c - '0', c = getchar();
	if(f == '-') sum *= -1;
	return sum;
}

void Msort(int l, int r)
{
	if(l >= r) return ;
	int mid = (l + r) / 2;
	Msort(l, mid);
	Msort(mid + 1, r);
	int i = l, j = mid + 1, k = l;
	while(k <= r)
	{
		if(i <= mid && (j > r || a[i] <= a[j])) b[k++] = a[i++];
		if(j <= r && (i > mid || a[i] > a[j])) b[k++] = a[j++];
	}
	for(i = l; i <= r; i++) a[i] = b[i];
}

bool DFS(int u)
{
	if(dp[u] != -1) return dp[u];
	dp[u] = 0;
	for(int i = 1; i <= ans; i++)
	{
		if(u < k[i]) return dp[u];
		dp[u] |= DFS(u - k[i]);
	}
	return dp[u];
}

INT main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	T = read();
	while(T--)
	{
		sum = ans = 0;
		memset(a, 0, sizeof(a));
		memset(dp, -1, sizeof(dp));
		n = read();
		for(int i = 1; i <= n; i++) a[i] = read(), sum += a[i];
		Msort(1, n);
		dp[0] = 1;
		for(int i = 1; i <= n; i++)
			if(DFS(a[i]) == 0) k[++ans] = a[i], dp[a[i]] = 1;
		printf("%d\n", ans);
	}
	return 0;
}
