#include <bits/stdc++.h>

typedef int INT;

const int N = 100000 + 10;

int n;

int a[N];

int ans;

int read()
{
	int sum = 0;
	char c = getchar(), f = c;
	while(c < '0' || c > '9') f = c, c = getchar();
	while(c >= '0' && c <= '9') sum = sum * 10 + c - '0', c = getchar();
	if(f == '-') sum *= -1;
	return sum;
}

INT main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	n = read();
	a[0] = 0;
	for(int i = 1; i <= n; i++)
	{
		a[i] = read();
		if(a[i] <= a[i - 1]) continue;
		ans += a[i] - a[i - 1];
	}
	printf("%d\n", ans);
	return 0;
}
