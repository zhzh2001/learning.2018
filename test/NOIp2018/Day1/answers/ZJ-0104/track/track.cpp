#include <bits/stdc++.h>

typedef int INT;

const int N = 50000 + 10;
const int INF = 100000000;
#define max(x, y) ((x) > (y) ? (x) : (y))
#define min(x, y) ((x) < (y) ? (x) : (y))

int n, m;

int mxx, mxv, mnv = INF, S;

bool mrk = 1, flg = 1;

int val[N], b[N], Mx, U;

int head[N], cnt;

struct Node { int x, y, v; } q[N];

struct node { int to, va, nxt; } e[N << 1];

int ans;

int read()
{
	int sum = 0;
	char c = getchar(), f = c;
	while(c < '0' || c > '9') f = c, c = getchar();
	while(c >= '0' && c <= '9') sum = sum * 10 + c - '0', c = getchar();
	if(f == '-') sum *= -1;
	return sum;
}

void Swap(int &x, int &y) { int t = x; x = y; y = t; }

void Add(int x, int y, int v) { e[++cnt] = (node) { y, v, head[x] }; head[x] = cnt; }

void Msort(int l, int r)
{
	if(l >= r) return ;
	int mid = (l + r) / 2;
	Msort(l, mid);
	Msort(mid + 1, r);
	int i = l, j = mid + 1, k = l;
	while(k <= r)
	{
		if(i <= mid && (j > r || val[i] <= val[j])) b[k++] = val[i++];
		if(j <= r && (i > mid || val[i] > val[j])) b[k++] = val[j++];
	}
	for(i = l; i <= r; i++) val[i] = b[i];
}

void DFS(int u, int f, int d)
{
	for(int i = head[u]; i; i = e[i].nxt)
	{
		int v = e[i].to;
		int w = e[i].va;
		if(v == f) continue;
		if(d + w > Mx) Mx = d + w, U = v;
		DFS(v, u, d + w);
	}
}

void Dfs(int u, int stp, int sum, int mn)
{
	if(mn <= ans) return ;
	if(u == n && stp == m) { ans = max(ans, min(mn, sum)); return ; }
	if(u == n || stp > m) return ;
	if((sum + q[u].v) * m >= S)
	Dfs(u + 1, stp + 1, q[u].v, min(mn, sum));
	Dfs(u + 1, stp, sum + q[u].v, mn);
}

void Solve0()
{
	for(int i = 1; i < n; i++)
		Add(q[i].x, q[i].y, q[i].v),
		Add(q[i].y, q[i].x, q[i].v);
	Mx = 0; DFS(1, 0, 0);
	Mx = 0; DFS(U, 0, 0);
	printf("%d\n", Mx);
}

void Solve1()
{
	for(int i = 1; i < n; i++) val[i] = q[i].v;
	Msort(1, n - 1);
	if(m * 2 >= n)
	{
		int k = n - 1 - m;
		ans = val[k * 2 + 1];
		for(int l = 1, r = k * 2; l < r; l++, r--)
			ans = min(ans, val[l] + val[r]);
	}
	else
	{
		ans = INF;
		for(int l = n - m * 2, r = n - 1; l < r; l++, r--)
			ans = min(ans, val[l] + val[r]);
	}
	printf("%d\n", ans);
}

void Solve2()
{
	Dfs(1, 1, 0, INF);
	printf("%d\n", ans);
}

INT main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	n = read(), m = read();
	for(int i = 1; i < n; i++)
	{
		int x = q[i].x = read();
		int y = q[i].y = read();
		int v = q[i].v = read();
		if(x > y) Swap(x, y), Swap(q[i].x, q[i].y);
		mxx = max(mxx, x);
		mxv = max(mxv, v);
		mnv = min(mnv, v);
		S += v;
		mrk &= (y == x + 1);
	}
	if(m == 1) { Solve0(); return 0; }
	if(m == n - 1) { printf("%d\n", mnv); return 0; }
	if(mxx == 1) { Solve1(); return 0; }
	if(mrk) { Solve2(); return 0; }
	printf("%d\n", mxv);
	return 0;
}
