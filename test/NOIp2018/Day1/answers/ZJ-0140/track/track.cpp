#include<bits/stdc++.h>
using namespace std;
const int Maxn = 50002;
int n, m, head[Maxn], g[Maxn], gv[Maxn], nxt[Maxn], cnt, f[Maxn], d[Maxn], deep[Maxn];
int L[1100][1120], Max, u, dis[Maxn];
bool used[1100][1100];
struct node
{
	int x, y;
	int v;
}a[Maxn];
bool cmp3(node x, node y){
	return x.v > y.v;
}
struct node2{
	int x, y;
	int v;
	int lca, d;	
}r[1000010], b[1000010];
bool bfs(int sx)
{
	Max = 0;
	queue<int> q;
	memset(dis, -1, sizeof(dis));
	dis[sx] = 0;
	q.push(sx);
	while(!q.empty()){
		int x = q.front();
		q.pop();
		for(int i = head[x]; i; i = nxt[i]){
			if(dis[g[i]] == -1){
				dis[g[i]] = dis[x] + gv[i];
				if(dis[g[i]] > Max) Max = dis[g[i]], u = g[i];
				q.push(g[i]);
			}
		}
	}
}
bool cmp1(node2 x, node2 y){
	return x.v < y.v;
}
bool cmp2(node2 x, node2 y){
	return x.d > y.d;
}
void addedge(int x, int y, int z)
{
	cnt ++;
	g[cnt] = y;
	gv[cnt] = z;
	nxt[cnt] = head[x];
	head[x] = cnt;
}
void build(int x, int fa)
{
	f[x] = fa;
	d[x] = d[fa] + 1;
	Max = max(Max, d[x]);
	for(int i = head[x]; i; i = nxt[i]){
		if(g[i] != fa){
			deep[g[i]] = deep[x] + gv[i];
			build(g[i], x);
		}
	}
}
int get_lca(int x, int y){
	if(L[x][y] != -1) return L[x][y];
	if(d[x] >= d[y]){
		if(x == y){
			L[x][y] = x;
			return x;
		}
		L[x][y] = get_lca(f[x], y);
		return L[x][y];
	}
	if(d[y] > d[x]){
		L[x][y] = get_lca(x, f[y]);
		return L[x][y];
	}
}
bool cando(int x, int to){
	if(x == to) return 1;
	if(used[x][f[x]]) return 0;
	return cando(f[x], to);
}
int erfens(int v)
{
	int L = 1, R = cnt+1;
	while(L < R){
		int Mid = (L + R) >> 1;
		if(v > r[Mid].v) R = Mid;
		else L = Mid + 1;
	}	
	return L;
}
bool check(int Mid)
{
	int res = 0;
	memset(used, 0, sizeof(used));
	int tbian = 0;
//	int it = erfens(Mid); printf("%d ", it);
	for(int i = 1; i <= cnt; i++){
		if(r[i].v >= Mid)
		b[++tbian] = r[i];
	}
	sort(b + 1, b + 1 + tbian, cmp2);
	
	for(int i = 1; i <= tbian; i++){
		if(cando(b[i].x, b[i].lca) && cando(b[i].y, b[i].lca)){
			res++;
			int x = b[i].x;
			if(res >= m) return 1;
			while(x != b[i].lca){
				used[x][f[x]] = 1;
				x = f[x];
			}
			x = b[i].y;
			while(x != b[i].lca){
				used[x][f[x]] = 1;
				x = f[x];
			}
		}
	}
	if(res >= m) return 1;
	return 0;
}
int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	memset(L, -1, sizeof(L));
	scanf("%d %d", &n, &m);
	for(int i = 1; i < n; i++){
		scanf("%d %d %d", &a[i].x, &a[i].y, &a[i].v);
		addedge(a[i].x, a[i].y, a[i].v);
		addedge(a[i].y, a[i].x, a[i].v);
	}
	build(1, 0);
	if(m == 1){
		bfs(1);
		bfs(u);
		printf("%d", Max);
		return 0;
	}
	for(int i = 1; i <= n; i++){
		for(int j = i + 1; j <= n; j++){
				int lca = get_lca(i, j);
				r[++cnt].x = i, r[cnt].y = j, r[cnt].v = deep[i] - deep[lca] + deep[j] - deep[lca];
				r[cnt].lca = lca, r[cnt].d = d[lca];
	//			printf("%d %d %d %d %d\n", r[cnt].x, r[cnt].y, r[cnt].v, r[cnt].lca, r[cnt].d);			
		}
	}
	sort(r + 1, r + 1 + cnt, cmp1);
	int ans = 0;
	int L = 1, R = 1e9;
	while(L <= R)
	{
	//	printf("%d %d\n", L, R);
		int Mid = (L + R) >> 1;
		if(check(Mid)){
			ans = max(ans, Mid);
			L = Mid + 1;
		}
		else R = Mid - 1;
	}
	printf("%d", ans);
	return 0;
}
