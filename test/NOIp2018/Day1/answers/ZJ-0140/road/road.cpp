#include<bits/stdc++.h>
using namespace std;
int n;
long long a[100003];
int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for(int i = 1; i <= n; i++){
		scanf("%lld", &a[i]);
	}
	long long ans =0;
	long long Min = 0;
	for(int i = 1; i <= n; i++){
		if(a[i] > Min) ans += a[i] - Min;
		Min = a[i];
	}
	printf("%lld", ans);
	return 0;
}
