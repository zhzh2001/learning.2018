#include<bits/stdc++.h>
using namespace std;
int n, a[102];
bool dp[2500002];
int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int t;
	scanf("%d", &t);
	while(t--)
	{
		memset(dp, 0, sizeof(dp));
		scanf("%d", &n);
		int ans = 0;
		int sum = 0;
		for(int i = 1; i <= n; i++){
			scanf("%d", &a[i]);
			sum += a[i];		
		}
		dp[0] = 1;
		sort(a+1, a+1+n);
		for(int i = 1; i <= n; i++){
			if(!dp[a[i]]){
				ans++;
				for(int j = 0; j <= sum; j++){
					if(dp[j] && j + a[i] <= sum) dp[j + a[i]] = 1;	
				}
			}
		}
		printf("%d\n", ans);
	}
	return 0;
}
