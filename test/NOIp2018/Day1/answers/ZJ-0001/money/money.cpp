#include <bits/stdc++.h>
using namespace std;
int T,n,ma,i,j,ans,f[25010],a[105];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		ma=0;
		for (i=1;i<=n;i++)
		  scanf("%d",&a[i]),ma=a[i]>ma?a[i]:ma;
		for (i=1;i<=ma;i++)
		  f[i]=0;
		f[0]=1;
		for (i=1;i<=n;i++)
		  for (j=0;j<=ma;j++)
		    f[j+a[i]]+=f[j];
		ans=0;
		for (i=1;i<=n;i++)
		  if (f[a[i]]==1)
		    ans++;
	    printf("%d\n",ans);
	}
	return 0;
}
