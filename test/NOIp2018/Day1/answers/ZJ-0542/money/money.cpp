#include<iostream>
#include<cstdio>
using namespace std;
int a[200];
int flag[30000];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		int n;
		scanf("%d",&n);
		for (int i=0;i<=25000;++i) flag[i]=0;
		int ma=0;
		for (int i=1;i<=n;++i){
			int x;
			scanf("%d",&x);
			ma=max(ma,x);
			flag[x]=2;
		}
		int ans=0;
		for (int i=1;i<=ma;++i)
		if (flag[i]>0){
			if (flag[i]==2){
				++ans;
				a[ans]=i;
			}
			for (int j=1;j<=ans;++j){
				if (i+a[j]>ma) break;
				flag[i+a[j]]=1;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
