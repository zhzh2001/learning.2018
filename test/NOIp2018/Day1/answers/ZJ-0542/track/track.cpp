#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
#define M 50100
int vet[M<<1],val[M<<1],Next[M<<1];
int hed[M],f[M],cost[M],ha[M],pi[M];
bool flag[M];
int num,n,T;
void add(int u,int v,int z){
	++num;
	vet[num]=v;
	val[num]=z;
	Next[num]=hed[u];
	hed[u]=num;
}
void dfs(int x,int fa,int lim){
	int st=T;
	for (int i=hed[x];i!=-1;i=Next[i]){
		int v=vet[i];
		if (v==fa) continue;
		dfs(v,x,lim);
		++T;
		ha[T]=cost[v]+val[i];
		//cout<<T<<" "<<ha[T]<<" "<<cost[v]<<" "<<val[i]<<endl;
		f[x]+=f[v];
	}
	for (int i=st+1;i<=T;++i) flag[i]=false,pi[i]=0;
	sort(ha+st+1,ha+T+1);
	/*cout<<x<<"::::";
	for (int i=st+1;i<=T;++i){
		cout<<i<<" "<<ha[i]<<endl;
	}
	cout<<endl;*/
	int l=st+1,r=T;
	while (l<=r&&ha[r]>=lim){
		flag[r]=true;
		pi[r]=r;
		++f[x];
		--r;
	}
	while (l<r){
		//cout<<l<<" "<<r<<" "<<ha[l]<<" "<<ha[r]<<endl;
		while (l<r&&ha[l]+ha[r]<lim) ++l;
	  if (l>=r) break;
	  flag[l]=true;
	  flag[r]=true;
	  pi[l]=r;
	  pi[r]=l;
		++l;
	  --r;
	  f[x]++;
	}
	l=st+1;
	for (int i=T;i>=st+1;--i)
	if (flag[i]==false){
		while (l<i&&ha[l]+ha[i]<lim) ++l;
		if (ha[l]+ha[i]>=lim){
			if (pi[l]>i) cost[x]=ha[pi[l]];
			else cost[x]=ha[i];
		}else cost[x]=ha[i];
		break;
	}
	T=st;
	//cout<<x<<" "<<f[x]<<" "<<cost[x]<<endl;
}
int check(int lim){
	for (int i=1;i<=n;++i){
		f[i]=0;
	  cost[i]=0;
	}
	T=0;
	dfs(1,-1,lim);
	return f[1];
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int m;
	scanf("%d%d",&n,&m);
	num=0;
	int r=0;
	for (int i=1;i<=n;++i) hed[i]=-1;
	for (int i=1;i<n;++i){
		int u,v,z;
		scanf("%d%d%d",&u,&v,&z);
		add(u,v,z);
		add(v,u,z);
		r+=z;
	}
	int l=0,ans=0;
	while (l<=r){
		int mid=(l+r)>>1;
		if (check(mid)>=m){
			ans=mid;
			l=mid+1;
		}else r=mid-1;
	}
	//cout<<check(15)<<endl;
	printf("%d\n",ans);
	return 0;
}
