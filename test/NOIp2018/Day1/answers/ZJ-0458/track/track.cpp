#include<iostream>
#include<cstdio>
#include<algorithm>
#include<vector>
using namespace std;
const int N=50010;
struct node{
	int from,to,sum;
}edge[2*N];
int ans,n,m,i,j,c[N],k,first[N],nxt[2*N],u,v,s,l,r,mid,b[N],belong[N],cnt[N];
bool have[N];
vector<int> a[N];
void addedge(int u,int v,int s){
	edge[i].from=edge[i+n-1].to=u;edge[i].to=edge[i+n-1].from=v;edge[i].sum=edge[i+n-1].sum=s;nxt[i]=first[u];first[u]=i;nxt[i+n-1]=first[v];first[v]=i+n-1;
}
void clear(int u){
	for (int i=1;i<=cnt[u];i++) belong[i]=0,have[i]=false;for (int i=1;i<=cnt[u];i++) a[u].pop_back();cnt[u]=0;
}
int doit(int u,int x){int l=1,r=cnt[u],sum=0;if (r==0){b[u]=0;clear(u);return sum;}
	while (l<r){while (a[u][l]+a[u][r]<x&&l<r) l++;if (l<r) belong[r]=l,have[l]=have[r]=true,sum++,l++,r--;}int j=0;
	for (int i=1;i<=cnt[u];i++) if (!have[i]) j=i;if (j==0){b[u]=0;clear(u);return sum;}if (j==cnt[u]){b[u]=a[u][cnt[u]];clear(u);return sum;}
	for (int i=r+1;i<=cnt[u];i++){
		if (i!=r+1) if (a[u][belong[i]]+a[u][i-1]<x){b[u]=a[u][i-1];clear(u);return sum;}
		if (i==r+1) if (a[u][belong[i]]+a[u][j]<x){b[u]=a[u][j];clear(u);return sum;}
	}b[u]=a[u][cnt[u]];clear(u);return sum;
}
bool dfs(int u,int father,int x){if (ans>=m) return true;int k=first[u],v;
	while (k){v=edge[k].to;if (v==father){k=nxt[k];continue;}
	if (dfs(v,u,x)) return true; else ++cnt[u],a[u].push_back(edge[k].sum+b[v]);k=nxt[k];}
	for (int i=1;i<=cnt[u];i++) c[i]=a[u][i];sort(c+1,c+cnt[u]+1);for (int i=1;i<=cnt[u];i++) a[u][i]=c[i];
	while (a[u][cnt[u]]>=x&&cnt[u]) cnt[u]--,a[u].pop_back(),ans++;ans+=doit(u,x);if (ans>=m) return true; else return false;
}
bool check(int x){for (i=1;i<=n;i++) clear(i),b[i]=0;
	ans=0;if (dfs(1,0,x)) return true; else return false;
}
int main(){freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);for (i=1;i<=n-1;i++) scanf("%d%d%d",&u,&v,&s),addedge(u,v,s);for (i=1;i<=n;i++) a[i].push_back(0);
	l=1;r=500000000;while (l<r){mid=(l+r)/2+1;if (check(mid)) l=mid; else r=mid-1;}printf("%d\n",l);return 0;
}
