#include <cstdio>
#include <algorithm>
#include <cstring>
#define N 200
#define A 30000

using namespace std;
int a[N], dp[A];

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int t;
	scanf("%d", &t);
	while(t--) {
		memset(dp, 0, sizeof dp);
		int n, maxa = 0, ans = 0;
		scanf("%d", &n);
		for (int i = 1; i <= n; ++i) {
			scanf("%d", &a[i]);
			if (a[i] > maxa) maxa = a[i];
		}
		sort(a + 1, a + n + 1);
		for (int i = 1; i <= maxa; ++i) {
			for (int j = 1; j <= n; ++j) {
				if (a[j] < i) {
					if (dp[i - a[j]]) {
						dp[i] = 1;
						break;
					}
					continue;
				}
				if (a[j] == i) {
					dp[i] = 1;
					ans++;
				}
				break;
			}
		}
		printf("%d\n", ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
