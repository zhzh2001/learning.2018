#include <cstdio>
#include <algorithm>
#define N 100010

using namespace std;
int ans, n, m, a[N];

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; ++i) {
		scanf("%d", &a[i]);
	}
	m = unique(a + 1, a + n + 1) - (a + 1);
	for (int i = 1; i <= m; ++i) {
		if (a[i] > a[i - 1] && a[i] > a[i + 1]) ans += a[i];
		if (a[i] < a[i - 1] && a[i] < a[i + 1]) ans -= a[i];
	}
	printf("%d\n", ans);
	fclose(stdin); fclose(stdout);
	return 0;
}
