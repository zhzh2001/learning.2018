#include <cstdio>
#include <vector>
#include <algorithm>
#define N 50005

using namespace std;
typedef long long LL;
struct Edge{
	int to, nxt;
	LL len;
}e[N << 1];
vector <LL> p[N];
int cnt, lst[N], lk;

inline void add(int u, int v, LL w) {
	e[++cnt].to = v;
	e[cnt].nxt = lst[u];
	e[cnt].len = w;
	lst[u] = cnt;
}

inline LL dfs(int x, int fa, LL l) {
	p[x].clear();
	for (int i = lst[x]; i; i = e[i].nxt) {
		int son = e[i].to;
		if (fa == son) continue;
		p[x].push_back(dfs(son, x, l) + e[i].len);
	}
	sort(p[x].begin(), p[x].end());
	vector <LL> :: iterator it, lt = p[x].end(), id;
	LL maxlen = 0;
	for (it = p[x].begin(); it != p[x].end(); it++) {
		if ((*it) >= l) break;
		if ((*it) == 0) continue;
		id = lower_bound(it + 1, lt, l - (*it));
		if (id != lt) {
			lk++;
			*id = 0;
			lt = id;
		}
		else {
			maxlen = max(maxlen, *it);
		}
	}
	for (it = p[x].begin(); it != p[x].end(); it++) {
		if ((*it) >= l) lk++;
	}
	return maxlen;
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	int n, m, u, v;
	LL all = 0, w;
	scanf("%d%d", &n, &m);
	for (int i = 1; i < n; ++i) {
		scanf("%d%d%lld", &u, &v, &w);
		add(u, v, w);
		add(v, u, w);
		all += w;
	}
	LL l = 0, r = all / m + 1;
	while (l < r) {
		LL mid = (l + r + 1) >> 1;
		lk = 0;
		LL chk = dfs(1, 1, mid);
		if (lk >= m) l = mid;
		else r = mid - 1;
	}
	printf("%lld\n", l);
	fclose(stdin); fclose(stdout);
	return 0;
}
