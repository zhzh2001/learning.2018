#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<string.h>
#include<string>
#include<math.h>
#include<queue>
#include<vector>
#include<set>
#include<map>
#include<ctype.h>
#include<time.h>
#include<stdlib.h>
#define M 50005
using namespace std;
void Rd(int &res){
	char c;
	res=0;
	while(c=getchar(),!isdigit(c));
	do{
		res=(res<<3)+(res<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
}
struct node{
	int to,v;
};
struct nodt{
	int a,b,v;
}E[M];
vector<node>edge[M];
int n,m;
int jump[20][M],S=0,dis[M],dep[M];
void sdfs(int now,int las,int deep,int d){
	jump[0][now]=las,dis[now]=d,dep[now]=deep;
	for(int i=0;i<edge[now].size();i++){
		node pp=edge[now][i];
		if(pp.to==las)continue;
		sdfs(pp.to,now,deep+1,d+pp.v);
	}
}
void makedfs(){
	int x=1;
	while(x<=n)x<<=1,S++;
	S--;
	for(int i=0;i<S;i++){
		for(int k=1;k<=n;k++){
			if(jump[i][k]==-1)jump[i+1][k]=-1;
			else jump[i+1][k]=jump[i][jump[i][k]];
		}
	}
}
int LCA(int x,int y){
	if(dep[x]>dep[y])swap(x,y);
	int d=dep[y]-dep[x];
	for(int i=0;i<=S;i++){
		if(d&1)y=jump[i][y];
		d>>=1;
	}
	if(x==y)return x;
	for(int i=S;i>=0;i--){
		if(jump[i][x]!=jump[i][y]){
			x=jump[i][x];
			y=jump[i][y];
		}
	}
	return jump[0][x];
}
struct P20{
	int ma[M],ans;
	int dfs(int now,int las){
		int ma1=0,ma2=0;
		for(int i=0;i<edge[now].size();i++){
			node pp=edge[now][i];
			if(pp.to==las)continue;
			dfs(pp.to,now);
			ma[now]=max(ma[now],ma[pp.to]+pp.v);
			if(ma[pp.to]+pp.v>ma1)ma2=ma1,ma1=ma[pp.to]+pp.v;
			else if(ma[pp.to]+pp.v>ma2)ma2=ma[pp.to]+pp.v;
		}
//		if(now==2)printf("%d %d\n",ma1,ma2);
		ans=max(ans,ma1+ma2);
	}
	void solve(){
		ans=0;
		memset(ma,0,sizeof(ma));
		dfs(1,-1);
		printf("%d\n",ans);
	}
}p20;
struct P15{
	int a[M];
	int sz;
	bool check(int x){
		int cnt=0,l=1,r=sz;
		while(1){
			bool flag=0;
			if(l<=r){
				if(a[r]>=x)r--,cnt++,flag=1;
				else {
					while(l<r&&a[l]+a[r]<x)l++;
					if(l<r&&a[l]+a[r]>=x)l++,r--,cnt++,flag=1;
				}
			}
			if(cnt>=m)return 1;
			if(!flag)return 0;
		}
		return 0;
	}
	void solve(){
		int L=1,R=0,ans=0;
		sz=n-1;
		for(int i=1;i<n;i++)a[i]=E[i].v,R+=a[i];
		sort(a+1,a+sz+1);
		while(L<=R){
			int mid=L+R>>1;
			if(check(mid))L=mid+1,ans=max(ans,mid);
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}p15;
struct P20P{
	int a[M];
	int sz;
	bool check(int x){
		int cnt=0,sum=0;
		for(int i=1;i<=sz;i++){
			sum+=a[i];
			if(sum>=x)sum=0,cnt++;
		}
		return cnt>=m;
	}
	void solve(){
		int L=1,R=0,ans=0;
		sz=n-1;
		for(int i=1;i<n;i++)a[i]=E[i].v,R+=a[i];
		while(L<=R){
			int mid=L+R>>1;
			if(check(mid))L=mid+1,ans=max(ans,mid);
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}p20p;
struct P100{
	int tmp[M],a[M],cnt;
	bool mark[M];
	int dfs(int now,int las,int x){
		for(int i=0;i<edge[now].size();i++){
			node pp=edge[now][i];
			if(pp.to==las)continue;
			a[pp.to]=dfs(pp.to,now,x)+pp.v;
		}
		int l=1,r=0;
		for(int i=0;i<edge[now].size();i++){
			node pp=edge[now][i];
			if(pp.to==las)continue;
			tmp[++r]=a[pp.to];
			mark[r]=0;
		}
		if(l>r)return 0;
		sort(tmp+1,tmp+1+r);
//		if(x==15){
//			printf("->(%d %d)\n",now,las);
//			for(int i=l;i<=r;i++)printf("%d ",tmp[i]);
//			puts("");
//			puts("--------");
//		}
		//将所有能形成答案的路径全部用掉，剩下最大的返回 
		int ttt1=0,re1=0;
		int l1=l,r1=r;
		while(1){
			bool flag=0;
			if(l<=r){
				if(tmp[r]>=x)mark[r]=1,r--,ttt1++,flag=1;
				else {
					while(l<r&&tmp[l]+tmp[r]<x)l++;
					if(l<r&&tmp[l]+tmp[r]>=x){
						mark[l]=1,mark[r]=1;
						l++,r--,ttt1++,flag=1;
					}
				}
			}
			if(!flag)break;
		}
		for(int i=r1;i>=l1;i--){
			if(!mark[i]){
				re1=tmp[i];
				break;
			}
		}
		
		l=1,r=0;
		for(int i=0;i<edge[now].size();i++){
			node pp=edge[now][i];
			if(pp.to==las)continue;
			tmp[++r]=a[pp.to];
			mark[r]=0;
		}
		if(l>r)return 0;
		sort(tmp+1,tmp+1+r);
		int ttt2=0,re2=tmp[r];
		r--;
		while(1){
			bool flag=0;
			if(l<=r){
				if(tmp[r]>=x)mark[r]=1,r--,ttt2++,flag=1;
				else {
					while(l<r&&tmp[l]+tmp[r]<x)l++;
					if(l<r&&tmp[l]+tmp[r]>=x){
						mark[l]=1,mark[r]=1;
						l++,r--,ttt2++,flag=1;
					}
				}
			}
			if(!flag)break;
		}
//		if(x==21&&ttt1==ttt2)printf("(%d %d)\n",re1,re2);
		if(ttt1>ttt2){
			cnt+=ttt1;
			return re1;
		}
		else if(ttt1==ttt2&&re1<re2){
			cnt+=ttt1;
			return re2;
		}
		return re1;
//		return re1;
	}
	bool check(int x){
		cnt=0;
		dfs(1,0,x);
		return cnt>=m;
	}
	void solve(){
		memset(mark,0,sizeof(mark));
		int L=1,R=0,ans=0;
		for(int i=1;i<n;i++)R+=E[i].v;
		while(L<=R){
			int mid=L+R>>1;
			if(check(mid))L=mid+1,ans=max(ans,mid);
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}p100;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	Rd(n),Rd(m);
	bool shulian=1;
	bool ai1=1;
	for(int i=1;i<n;i++){
		int x,y,v;
		Rd(x),Rd(y),Rd(v);
		E[i]=(nodt){x,y,v};
		edge[x].push_back((node){y,v});
		edge[y].push_back((node){x,v});
		if(x!=1)ai1=0;
		if(y!=x+1)shulian=0;
	}
	sdfs(1,-1,0,0);
	makedfs();
//	printf("%d\n",shulian);
//	p15.solve();
//	p20p.solve();
//	p100.solve();
	if(m==1)p20.solve();
	else if(ai1)p15.solve();
	else if(shulian)p20p.solve();
	else p100.solve();
	return 0;
}
