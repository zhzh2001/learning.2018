#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<string.h>
#include<string>
#include<math.h>
#include<queue>
#include<vector>
#include<set>
#include<map>
#include<ctype.h>
#include<time.h>
#include<stdlib.h>
#define M 105
#define N 25005
using namespace std;
int a[M],T,n;
int mark[N];
struct P80{
	void solve(int t){
		mark[0]=t;
		sort(a+1,a+1+n);
		int ans=0;
		for(int i=1;i<=n;i++){
			if(mark[a[i]]!=t){
				ans++;
				for(int k=0;k+a[i]<N;k++){
					if(mark[k]==t)mark[k+a[i]]=t;
				}
			}
		}
		printf("%d\n",ans);
	}
}p80;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	memset(mark,0,sizeof(mark));
	for(int t=1;t<=T;t++){
		int ma=0;
		scanf("%d",&n);
		for(int i=1;i<=n;i++){
			scanf("%d",&a[i]);
			ma=max(ma,a[i]);
		}
		p80.solve(t);
	}
	return 0;
}
