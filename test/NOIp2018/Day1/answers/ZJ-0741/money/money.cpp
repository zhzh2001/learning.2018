#include<bits/stdc++.h>
using namespace std;
const int N=105;

int T,n,a[N],ans;
bool f[25005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",&a[i]);
		memset(f,0,sizeof f);
		f[0]=1,ans=n;
		sort(a+1,a+1+n);
		for(int i=1;i<=n;i++){
			if(f[a[i]])ans--;
			for(int j=0;j<=25000-a[i];j++){
				if(f[j])f[j+a[i]]=true;
			}
		}
		printf("%d\n",ans);
	}
}
