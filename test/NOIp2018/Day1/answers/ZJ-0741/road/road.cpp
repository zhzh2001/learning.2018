#include<bits/stdc++.h>
#define LL long long
using namespace std;
const int N=1e5+5;

LL n,d[N],ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%lld",&n);
	scanf("%lld",&d[1]);
	for(int i=2;i<=n;i++){
		scanf("%lld",&d[i]);
		if(d[i]<d[i-1])ans+=d[i-1]-d[i];
	}
	ans+=d[n];
	printf("%lld",ans);
}
