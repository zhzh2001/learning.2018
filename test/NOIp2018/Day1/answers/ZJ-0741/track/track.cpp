#include<bits/stdc++.h>
#define IT multiset<int>::iterator
using namespace std;
const int M=1e5+5,N=5e4+5;

struct edge{
	int v,w,nxt;
}e[M];
int n,m,tot,head[N],l,r,mid,ans,f[N],g[N];
multiset<int>s[N];
void addedge(int u,int v,int w){
	e[++tot]=(edge){v,w,head[u]};
	head[u]=tot;
	e[++tot]=(edge){u,w,head[v]};
	head[v]=tot;
}
void dfs(int u,int fa){
	f[u]=0,g[u]=0;
	for(int i=head[u];i;i=e[i].nxt){
		if(e[i].v==fa)continue;
		dfs(e[i].v,u);
		f[u]+=f[e[i].v];
		if(g[e[i].v]+e[i].w>=mid)f[u]++;
		else{
			g[e[i].v]+=e[i].w;
			s[u].insert(g[e[i].v]);
		}
	}
	IT it;
	int x;
	while(!s[u].empty()){
		it=s[u].begin();
		x=*(s[u].begin());
		s[u].erase(it);
		it=s[u].lower_bound(mid-x);
		if(it!=s[u].end()){
			f[u]++;
			s[u].erase(it);
		}
		else g[u]=max(g[u],x);
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		addedge(u,v,w);
	}
	l=1,r=500000000;
	while(l<=r){
		mid=l+r>>1;
		dfs(1,0);
		if(f[1]>=m)ans=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",ans);
}
