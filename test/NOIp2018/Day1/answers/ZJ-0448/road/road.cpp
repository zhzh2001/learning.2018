// road
#include <cstdio>

#define rg register
#define gch getchar

const int MAXN = 1e5 + 10;

int N, top, S[MAXN];

namespace FastIO {
	template <typename T>
		void read(T & x) {
			rg char ch = gch();
			for (; ch < '0' || ch > '9'; ch = gch());
			for (x = 0; ch >= '0' && ch <= '9'; x = (x << 3) + (x << 1) + (ch ^ '0'), ch = gch());
		}
}

int main() {
#ifndef LOCAL
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
#endif
	using FastIO::read;

	read(N);
	long long ans = 0;
	top = 1;
	for (int i = 0, Ai; i < N; i++) {
		read(Ai);
		if (!top) ans += Ai;
		else if (S[top - 1] < Ai) ans += Ai - S[top - 1];
		while (top && S[top - 1] >= Ai) --top;
		S[top++] = Ai;
	}

	printf("%lld\n", ans);
#ifndef LOCAL
	fclose(stdin); fclose(stdout);
#endif
	return 0;
}
