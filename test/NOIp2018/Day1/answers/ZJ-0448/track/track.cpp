// track
#include <cstdio>
#include <algorithm>
#include <vector>

#define rg register
#define gch getchar
#define pb push_back
#define forto(_) for (rg int v, e = last[_]; e; e = E[e].next)

const int MAXN = 5e4 + 10;

struct Edge {
	int to, cost, next;
} E[MAXN << 1];

int N, M, tote, totlen, minlen, cntrd, last[MAXN];

namespace FastIO {
	template <typename T>
		void read(T & x) {
			rg char ch = gch();
			for (; ch < '0' || ch > '9'; ch = gch());
			for (x = 0; ch >= '0' && ch <= '9'; x = (x << 3) + (x << 1) + (ch ^ '0'), ch = gch());
		}
}
int dfs(int u, int fa);
inline void add_edge(int u, int v, int c) {
	E[++tote] = (Edge){v, c, last[u]}, last[u] = tote;
	E[++tote] = (Edge){u, c, last[v]}, last[v] = tote;
}
inline bool check(int mnl) {
	minlen = mnl, cntrd = 0;
	dfs(1, 0);
	return cntrd >= M;
}

int main() {
#ifndef LOCAL
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
#endif
	using FastIO::read;

	read(N), read(M);
	for (int i = 1, x, y, z; i < N; i++) {
		read(x), read(y), read(z);
		add_edge(x, y, z);
		totlen += z;
	}

	int lb = 0, ub = totlen + 1, mid;
	for (; ub - lb > 1; ) {
		mid = lb + ub >> 1;
		if (check(mid)) lb = mid;
		else ub = mid;
	}

	printf("%d\n", lb);
#ifndef LOCAL
	fclose(stdin); fclose(stdout);
#endif
	return 0;
}

int dfs(int u, int fa) {
	std::vector<int> A;
	forto(u) {
		if ((v = E[e].to) == fa) continue;
		int tmp = dfs(v, u) + E[e].cost;
		if (tmp >= minlen) ++cntrd;
		else A.pb(tmp);
	}
	std::sort(A.begin(), A.end());
	std::vector<int> f(A.size(), 0);
	int ret = 0;
	for (int i = 0, sz = A.size(); i < sz; i++) {
		if (f[i]) continue;
		int j = std::max(i + 1, std::lower_bound(A.begin(), A.end(), minlen - A[i]) - A.begin());
		for (; j < sz && f[j]; j++);
		if (j == sz) ret = std::max(ret, A[i]);
		else f[j] = 1, ++cntrd;
	}
	return ret;
}
