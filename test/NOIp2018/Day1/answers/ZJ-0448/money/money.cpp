// money
#include <cstdio>
#include <cstring>
#include <algorithm>

const int MAXN = 105;
const int MAXA = 25010;

int N, A[MAXN], DP[MAXA];

void solve();

int main() {
#ifndef LOCAL
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
#endif
	int t;
	for (scanf("%d", &t); t--; ) solve();

#ifndef LOCAL
	fclose(stdin); fclose(stdout);
#endif
	return 0;
}

void solve() {
	scanf("%d", &N);
	int mxAi = 0;
	for (int i = 0; i < N; i++) {
		scanf("%d", A + i);
		mxAi = std::max(mxAi, A[i]);
	}
	
	std::sort(A, A + N);
	memset(DP, 0, sizeof DP);
	DP[0] = 1;
	int ans = 0;
	for (int i = 0; i < N; i++) {
		if (DP[A[i]]) continue;
		++ans;
		for (int j = A[i]; j <= mxAi; j++) DP[j] |= DP[j - A[i]];
	}

	printf("%d\n", ans);
}
