#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
using namespace std;

inline void getint(int &num){
	register int ch, neg = 0;
	while(!isdigit(ch = getchar())) if(ch == '-') neg = 1;
	num = ch & 15;
	while(isdigit(ch = getchar())) num = num * 10 + (ch & 15);
	if(neg) num = -num;
}

int n, m, tot = 0, tope = 0;
struct Edge {int np, val; Edge *nxt;} E[100005], *V[50005];

inline void addedge(const int &u, const int &v, const int &w){
	E[++tope].np = v, E[tope].val = w;
	E[tope].nxt = V[u], V[u] = E + tope;
}

namespace Subtask1{
	int dis[50005], q[50005], fr, re;
	
	inline int bfs(int S){
		memset(dis, -1, sizeof(dis)), dis[S] = 0, q[fr = 0] = S, re = 1;
		while(fr != re){
			int u = q[fr++];
			for(register Edge *ne = V[u]; ne; ne = ne->nxt)
				if(dis[ne->np] == -1){
					dis[ne->np] = dis[u] + ne->val;
					q[re++] = ne->np;
				}
		}
		int id, dist = 0;
		for(register int i = 1; i <= n; i++)
			if(dis[i] > dist) id = i, dist = dis[i];
		return id; 
	}
}

namespace Subtask2{
	int flag = 1, d[50005], topd = 0;
	
	inline bool check(int k){
		const int pos = lower_bound(d + 1, d + n, k) - d;
		int res = n - pos; if(res >= m) return 1;
		for(register int r = pos - 1, l = 1; l < r; l++, r--){
			while(l < r && d[r] + d[l] < k) l++;
			if(l == r) break;
			if(++res == m) return 1;
		}
		return 0;
	}
}

namespace Subtask3{
	int flag = 1, d[50005];
	
	inline bool check(int k){
		int res = 0, cur = 0;
		for(register int i = 1; i < n; i++)
			if((cur += d[i]) >= k){
				if(++res == m) return 1;
				cur = 0;
			}
		return 0;
	}
}

int main(){
	freopen("track.in", "r", stdin), freopen("track.out", "w", stdout);
	getint(n), getint(m);
	for(register int i = 1; i < n; i++){
		int u, v, w; getint(u), getint(v), getint(w);
		addedge(u, v, w), addedge(v, u, w), tot += w;
		if(u != 1) Subtask2::flag = 0;
		if(v != u + 1) Subtask3::flag = 0;
	}
	if(m == 1){
		using namespace Subtask1;
		int s = bfs(1), t = bfs(s);
		return printf("%d\n", dis[t]), 0;
	}
	if(Subtask2::flag){
		using namespace Subtask2;
		for(register Edge *ne = V[1]; ne; ne = ne->nxt) d[++topd] = ne->val;
		sort(d + 1, d + topd + 1); int l = 0, r = 20000;
		while(l < r){
			const int mid = l + r + 1 >> 1;
			if(check(mid)) l = mid;
			else r = mid - 1;
		}
		return printf("%d\n", l), 0;
	}
	if(Subtask3::flag){
		using namespace Subtask3;
		for(register int i = 1; i < n; i++)
			for(register Edge *ne = V[i]; ne; ne = ne->nxt)
				if(ne->np == i + 1) d[i] = ne->val;
		int l = 0, r = tot;
		while(l < r){
			const int mid = l + r + 1 >> 1;
			if(check(mid)) l = mid;
			else r = mid - 1;
		}
		return printf("%d\n", l), 0;
	}
	return printf("%u\n", rand() * (unsigned)rand() % tot + 1), 0;
}
