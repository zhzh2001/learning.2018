#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<queue>
using namespace std;
typedef long long ll;
typedef struct {int id, dep;} Pair;

inline void getint(int &num){
	register int ch, neg = 0;
	while(!isdigit(ch = getchar())) if(ch == '-') neg = 1;
	num = ch & 15;
	while(isdigit(ch = getchar())) num = num * 10 + (ch & 15);
	if(neg) num = -num;
}

inline bool operator < (const Pair &p1, const Pair &p2){
	return p1.dep < p2.dep;
}

int n, d[100005], pre[100005], suc[100005];
priority_queue<Pair> q; ll ans = 0;

inline void del(int u){
	suc[pre[u]] = suc[u], pre[suc[u]] = pre[u];
	suc[u] = pre[u] = -1;
}

int main(){
	freopen("road.in", "r", stdin), freopen("road.out", "w", stdout);
	getint(n);
	for(register int i = 1; i <= n; i++)
		getint(d[i]), suc[i] = i + 1, pre[i] = i - 1;
	for(register int i = 1; i <= n; i++)
		if(d[i] == d[i + 1]) del(i); else q.push((Pair){i, d[i]});
	while(!q.empty()){
		int u = q.top().id; q.pop();
		if(pre[u] != -1 && suc[u] != -1){
			const int p = pre[u], s = suc[u];
			ans += d[u] - max(d[p], d[s]), del(u);
			if(d[p] == d[s]) del(p);
		}
	}
	printf("%lld\n", ans);
	return 0;
}
