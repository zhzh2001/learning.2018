#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
using namespace std;

inline void getint(int &num){
	register int ch, neg = 0;
	while(!isdigit(ch = getchar())) if(ch == '-') neg = 1;
	num = ch & 15;
	while(isdigit(ch = getchar())) num = num * 10 + (ch & 15);
	if(neg) num = -num;
}

int T, n, a[102], f[25002], ans, maxa;

int main(){
	freopen("money.in", "r", stdin), freopen("money.out", "w", stdout);
	getint(T);
	while(T--){
		getint(n), memset(f, 0, sizeof(f)), f[0] = 1, ans = maxa = 0;
		for(register int i = 1; i <= n; i++) getint(a[i]), maxa = max(maxa, a[i]);
		sort(a + 1, a + n + 1);
		for(register int i = 1; i <= n; i++)
			if(f[a[i]] == 0){
				ans++;
				for(register int j = a[i]; j <= maxa; j++)
					if(f[j - a[i]] == 1) f[j] = 1;
			}
		printf("%d\n", ans);
	}
	return 0;
}
