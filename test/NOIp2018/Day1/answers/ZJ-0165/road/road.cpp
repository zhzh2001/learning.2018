#include<iostream>
#include<cstdio>
#include<cstring>
#define MAXN 100005
using namespace std;
int nA[MAXN];
int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	int nN, i;
	long long nAns;
	scanf("%d", &nN);
	nAns=0;
	for(i=1;i<=nN;i++)
		scanf("%d", &nA[i]);
	nAns=nA[1];
	for(i=2;i<=nN;i++)
	{
		if(nA[i]<=nA[i-1])
			continue;
		else
			nAns+=(nA[i]-nA[i-1]);
	}
	printf("%lld\n", nAns);
	return 0;
}
