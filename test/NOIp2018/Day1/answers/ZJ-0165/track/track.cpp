#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define MAXN 50001
#define INF 0x3fffffff
#define MAXI 5
using namespace std;
int nN, nM, nF[MAXN<<1], nFEdge[MAXN], nFirst[MAXN], nDth[MAXN], nNext[MAXN<<1], nTo[MAXN<<1], nW[MAXN<<1], nMax1[MAXN], nMax2[MAXN], nAns, nFather[MAXN];
int nFrom[MAXN], nT[MAXN];
bool bVis[MAXN], bUse[MAXN];
void vAdd(int nA, int nB, int i, int nC);
void vDfs(int nRoot);
void vDfs2(int nRoot, int i);
void vBuild(int nRoot);
void vDfsU(int nDth);
int nCheck();
int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	int i, nA, nB, nC;
	scanf("%d %d", &nN, &nM);
	memset(nFirst, -1, sizeof(nFirst));
	for(i=1;i<nN;i++)
	{
		scanf("%d %d %d", &nA, &nB, &nC);
		vAdd(nA, nB, (i<<1)-1, nC);
		vAdd(nB, nA, (i<<1), nC);
	}
	if(nM==1)
	{
		memset(bVis, false, sizeof(bVis));
		nAns=0;
		vDfs(1);
		memset(bVis, false, sizeof(bVis));
		vDfs2(1, 0);
		printf("%d\n", nAns);
	}
	else
	{
		vBuild(1);
		vDfsU(1);
		printf("%d\n", nAns);
	}
	return 0;
}
void vAdd(int nA, int nB, int i, int nC)
{
	nF[i]=nA;
	nNext[i]=nFirst[nA];
	nFirst[nA]=i;
	nTo[i]=nB;
	nW[i]=nC;
}
void vDfsU(int nDth)
{
	int i, j;
	if(nDth==nM+1)
	{
		nAns=max(nAns, nCheck());
		return;
	}
	for(i=1;i<=nN;i++)
	{
		if(bVis[i])
			continue;
		bVis[i]=true;
		for(j=1;j<=nN;j++)
		{
			if(bVis[j])
				continue;
			bVis[j]=true;
			nFrom[nDth]=i;
			nT[nDth]=j;
			vDfsU(nDth+1);
			bVis[j]=false;
		}
		bVis[i]=false;
	}
}
void vDfs(int nRoot)
{
	int nAi;
	bVis[nRoot]=true;
	for(nAi=nFirst[nRoot];nAi!=-1;nAi=nNext[nAi])
	{
		if(!bVis[nTo[nAi]])
		{
			nFather[nTo[nAi]]=nRoot;
			vDfs(nTo[nAi]);
			if(nMax1[nTo[nAi]]+nW[nAi]>nMax1[nRoot])
			{
				nMax2[nRoot]=nMax1[nRoot];
				nMax1[nRoot]=nMax1[nTo[nAi]]+nW[nAi];
			}
			else
			{
				if(nMax1[nTo[nAi]]+nW[nAi]>nMax2[nRoot])
					nMax2[nRoot]=nMax1[nTo[nAi]]+nW[nAi];
			}
		}
	}
}
void vDfs2(int nRoot, int i)
{
	int nAi, t;
	nAns=max(nAns, nMax1[nRoot]);
	bVis[nRoot]=true;
	if(nRoot!=1)
	{
		if(nMax1[nFather[nRoot]]==nMax1[nRoot]+nW[i])
			t=nMax2[nFather[nRoot]]+nW[i];
		else
			t=nMax1[nFather[nRoot]]+nW[i];
		nAns=max(nAns, t);
		if(t>nMax1[nRoot])
		{
			nMax1[nRoot]=t;
			nMax2[nRoot]=nMax1[nRoot];
		}
		else
			nMax2[nRoot]=max(nMax2[nRoot], t);
	}
	for(nAi=nFirst[nRoot];nAi!=-1;nAi=nNext[nAi])
	{
		if(!bVis[nTo[nAi]])
			vDfs2(nTo[nAi], nAi);
	}
}
void vBuild(int nRoot)
{
	nDth[nRoot]=nFather[nRoot]+1;
	for(int i=nFirst[nRoot];i!=-1;i=nNext[i])
	{
		if(nTo[i]!=nFather[nRoot])
		{
			nFather[nTo[i]]=nRoot;
			if(nTo[i+1]==nRoot&&nF[i+1]==nTo[i])
				nFEdge[nTo[i]]=i+1;
			else
				nFEdge[nTo[i]]=i-1;
			vBuild(nTo[i]);
		}
	}
}
int nCheck()
{
	int nRet=INF, i, nA, nB, nSum;
	memset(bUse, false, sizeof(bUse));
	for(i=1;i<=nM;i++)
	{
		nA=nFrom[i];
		nB=nT[i];
		nSum=0;
		if(nDth[nA]<nDth[nB])
		{
			nA+=nB;
			nB=nA-nB;
			nA-=nB;
		}
		while(nDth[nA]!=nDth[nB])
		{
			nSum+=nW[nFEdge[nA]];
			if(bUse[nFEdge[nA]+1>>1])
				return -1;
			bUse[nFEdge[nA]+1>>1]=true;
			nA=nTo[nFEdge[nA]];
		}
		while(nA!=nB)
		{
			nSum+=nW[nFEdge[nA]];
			if(bUse[nFEdge[nA]+1>>1])
				return -1;
			bUse[nFEdge[nA]+1>>1]=true;
			nA=nTo[nFEdge[nA]];
			if(bUse[nFEdge[nB]+1>>1])
				return -1;
			nSum+=nW[nFEdge[nB]];
			bUse[nFEdge[nB]+1>>1]=true;
			nB=nTo[nFEdge[nB]];
		}
		nRet=min(nRet, nSum);
	}
	return nRet;
}
