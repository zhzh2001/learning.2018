#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define MAXM 101
#define MAXN 25001
using namespace std;
int nA[MAXM], nUse[MAXM], nLen;
bool bDp[MAXN];
bool bCheck(int nN);
int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int nN, i, nT, a;
	scanf("%d", &nT);
	while(nT--)
	{
		scanf("%d", &nN);
		for(i=1;i<=nN;i++)
			scanf("%d", &nA[i]);
		sort(nA+1, nA+1+nN);
		nLen=0;
		for(i=1;i<=nN;i++)
		{
			if(!bCheck(nA[i]))
			{
				nLen++;
				nUse[nLen]=nA[i];
			}
		}
		printf("%d\n", nLen);
	}
	return 0;
}
bool bCheck(int nN)
{
	int i, j;
	memset(bDp, false, sizeof(bDp));
	bDp[0]=true;
	for(i=1;i<=nLen;i++)
	{
		for(j=nUse[i];j<=nN;j++)
			bDp[j]=bDp[j]|bDp[j-nUse[i]];
	}
	return bDp[nN];
}
