#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdio>
#define N 1005
using namespace std;

int n,a[N],ans;
bool t[50005];

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		ans=n;
		memset(t,false,sizeof(t));
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		for (int i=1;i<=n;i++){
			if (t[a[i]]) ans--;
			t[a[i]]=true;
			for (int j=1;j<=a[n];j++)
				if (t[j]) t[j+a[i]]=true;
		}
		printf("%d\n",ans);
	}
	return 0;
}

