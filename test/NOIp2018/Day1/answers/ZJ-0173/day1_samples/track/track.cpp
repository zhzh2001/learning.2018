#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdio>
#define INF 0x3f3f3f3f
#define N 50005
using namespace std;

int father[N],deep[N],son[N],size[N],top[N];
int n,m,head[N],in[N],tot=0,ID,used[N],root;
long long dis[N],ans=0;
bool visit[N];

struct Edge{
	int to,next,value;
}edge[N<<1];

void add(int u,int v,int w){
	in[v]++;
	edge[++tot].to=v;
	edge[tot].value=w;
	edge[tot].next=head[u];
	head[u]=tot;
}

void dfs1(int u){
	size[u]=1;
	deep[u]=deep[father[u]]+1;
	for (int i=head[u];i;i=edge[i].next){
		int v=edge[i].to;
		if (v!=father[u]){
			father[v]=u;
			dis[v]=dis[u]+edge[i].value;
			dfs1(v);
			size[u]+=size[v];
			if (size[v]>size[son[u]]) son[u]=v;
		}
	}
}

void dfs2(int u,int Top){
	top[u]=Top;
	if (son[u]) dfs2(son[u],Top);
	for (int i=head[u];i;i=edge[i].next){
		int v=edge[i].to;
		if (v!=father[u]&&v!=son[u]) dfs2(v,v);
	}
}

int LCA(int x,int y){
	while (top[x]!=top[y])
		(deep[top[x]]>deep[top[y]])?x=father[top[x]]:y=father[top[y]];
	return (deep[x]<deep[y])?x:y;
}

void dfs(int u,long long s){
	visit[u]=true;
	if (s>ans) ID=u,ans=s;
	for (int i=head[u];i;i=edge[i].next){
		int v=edge[i].to;
		if (!visit[v]) dfs(v,s+edge[i].value);
	}
}

void Find(int k,long long s){
	if (k>m){ ans=max(ans,s); return; }
	for (int i=1;i<=n;i++){
		if (in[i]!=1||used[i]) continue;
		used[i]++;
		for (int j=i+1;j<=n;j++){
 			if (in[j]!=1||used[j]) continue;
 			if (father[i]!=father[j]&&(used[father[j]]||used[father[i]])) continue;
			int lca=LCA(i,j);
			if (father[i]!=father[j]) used[father[j]]++,used[father[i]]++;
			used[j]++;
			long long DIS=dis[i]+dis[j]-2*dis[lca];
			Find(k+1,min(s,DIS));
			used[j]--;
			if (father[i]!=father[j]) used[father[j]]--,used[father[i]]--;
		}
		used[i]--;
	}
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int x,y,z;
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z); add(y,x,z);
	}
	root=1;
	while (in[root]==1) root++;
	father[root]=root;
	dfs1(root);
	dfs2(root,root);
	if (m==1){
		dfs(1,0); 
		memset(visit,0,sizeof(visit));
		dfs(ID,0);
		printf("%lld\n",ans);
		return 0;
	}
	father[root]=0;
	Find(1,INF);
	printf("%lld\n",ans);
	return 0;
}

