#include<iostream>
#include<cstring>
#include<cstdio>
#define N 100005
using namespace std;

int n,a[N];
long long ans=0;

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%d",&a[i]);
		if (a[i]>a[i-1]) ans+=a[i]-a[i-1];
	}
	printf("%lld\n",ans);
	return 0;
}

