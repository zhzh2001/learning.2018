#include <bits/stdc++.h>

using namespace std;

const int N = 1E5 + 10;

int a[N], d[N], n, ans, tot;
int main() {
	freopen("road.in", "r", stdin), freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", a + i), d[i] = a[i] - a[i - 1];
	for (int i = 1; i <= n; i++)
		if (d[i] < 0)
			ans -= d[i], tot = max(0, tot + d[i]);
		else tot += d[i];
	return printf("%d\n", ans + tot), 0;
}
