#include <bits/stdc++.h>

using namespace std;

const int N = 105, V = 25005;

int a[N], T, n, ans;
bool f[V];

int main() {
	freopen("money.in", "r", stdin), freopen("money.out", "w", stdout);
	for (scanf("%d", &T); T--;) {
		scanf("%d", &n);
		for (int i = 1; i <= n; i++)
			scanf("%d", a + i);
		sort(a + 1, a + n + 1);
		memset(f, ans = 0, sizeof f), f[0] = 1;
		for (int i = 1; i <= n; i++) {
			if (f[a[i]]) continue;
					else ans++;
			for (int j = 0; j <= a[n] - a[i]; j++)
				f[j + a[i]] |= f[j];
		}
		printf("%d\n", ans);
	}
	return 0;
}
