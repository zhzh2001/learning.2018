#include <stdio.h>

struct node{
	int d;
	int h;
	struct node * fthr;
};

void srt(int *,int *,int);
struct node * lca(struct node *,struct node *,int *);
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
    int n,m;
    scanf("%d %d",&n,&m);
    int i;
    int a[3][n];
    int b[n];
    struct node s[n+1];
    int sign=1;
    for(i=1;i<n;i++){
    	scanf("%d %d %d",a[0]+i,a[1]+i,a[2]+i);
    	if(a[0][i]>a[1][i]){
    		int tmp=a[0][i];
    		a[0][i]=a[1][i];
    		a[1][i]=tmp;
    		sign=0;
		}else if(a[1][i]-a[0][i]>1){
			sign=0;
		}
		b[i]=i;
	}
	s[1].fthr=s+1;
	s[1].h=0;
	s[1].d=0;
	srt(b+1,a[0]+1,n-1);
	for(i=1;i<n;i++){
		int tmp=b[i];
		s[a[1][tmp]].fthr=s+a[0][tmp];
		s[a[1][tmp]].d=a[2][tmp];
		s[a[1][tmp]].h=s[a[0][tmp]].h+1;
	}
	if(m==1){
		int ss[2][n+1];
		for(i=1;i<=n;i++){
			ss[0][i]=0;
			ss[1][i]=0;
		}
		for(i=n;i>1;i--){
			int tmpp=s[i].fthr-s;
			if(ss[0][tmpp]<ss[0][i]+s[i].d){
				ss[1][tmpp]=ss[0][tmpp];
			    ss[0][tmpp]=ss[0][i]+s[i].d;
			}else if(ss[1][tmpp]<ss[0][i]+s[i].d){
				ss[1][tmpp]=ss[0][i]+s[i].d;
			}
		}
		int ans=0;
		for(i=1;i<=n;i++){
			if(ss[0][i]+ss[1][i]>ans){
				ans=ss[0][i]+ss[1][i];
			}
		}
		printf("%d",ans);
	}else if(a[0][b[n-1]]==1){
		srt(b+1,a[2]+1,n-1);
		if(m+m<=n){
			int ans=1000000000;
			for(i=0;i<m;i++){
				if(a[2][n-m-m+i]+a[2][n-1-i]<ans) ans=a[2][n-m-m+i]+a[2][n-1-i];
			}
			printf("%d",ans);
		}else{
			int ans=1000000000;
			for(i=0;i<n-m;i++){
				if(a[2][i]+a[2][2*(n-m)-1-i]<ans) ans=a[2][i]+a[2][2*(n-m)-1-i];
			}
			for(i=n-m;i<n;i++){
				if(a[2][i]<ans) ans=a[2][i];
			}
			printf("%d",ans);
		}
	}else{
		int tm;
		lca(s+1,s+n,&tm);
		printf("%d",tm);
	}
	return 0;
}

struct node * lca(struct node * a,struct node * b,int * s){
	while(a->h<b->h){
		*s+=b->d;
   	    b=b->fthr;
    }
	while(a->h>b->h){
		*s+=a->d;
	    a=a->fthr;
    }
	while(a!=b){
		*s+=a->d;
		*s+=b->d;
		a=a->fthr;
		b=b->fthr;
	}
	return a;
}

void srt(int * a,int * v,int l){
	if(l<=1) return;
	int lft=l>>1;
	int rgt=l-lft;
	int i=0;
	int j=lft;
	int b[l];
	int k=0;
	srt(a,v,lft);
	srt(a+lft,v,rgt);
	while((i<lft)&&(j<l)){
		if(v[a[i]]<=v[a[j]]){
			b[k]=a[i];
			k++;
			i++;
		}else{
			b[k]=a[j];
			k++;
			j++;
		}
	}
	while(i<lft){
		b[k]=a[i];
		k++;
		i++;
	}
	while(j<l){
		b[k]=a[j];
		k++;
		j++;
	}
	for(i=0;i<l;i++){
		a[i]=b[i];
	}
}
