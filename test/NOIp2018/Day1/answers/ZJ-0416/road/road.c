#include <stdio.h>
int sdf(int *,int,int);
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n;
	scanf("%d",&n);
	int a[n];
	int xa[n];
	
	int i;
	
	for(i=0;i<n;i++){
		scanf("%d",a+i);
		xa[i]=i;
	}
	printf("%d",sdf(a,n,0));
	return 0;
}

int sdf(int * a,int l,int d){
	if(l<=0) return 0;
	int i;
	int mn=1000000;
	int mni;
	for(i=0;i<l;i++){
		if(a[i]<mn){
			mn=a[i];
			mni=i;
		}
	}
	return sdf(a,mni,mn)+sdf(a+mni+1,l-mni-1,mn)+a[mni]-d;
}
