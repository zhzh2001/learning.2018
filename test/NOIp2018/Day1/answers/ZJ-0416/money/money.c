#include <stdio.h>
void srt(int *,int);
int bld(int *,int,int);
int gcd(int,int);
int gcds(int *,int);
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T,n;
	scanf("%d",&T);
	int jjc;
	for(jjc=0;jjc<T;jjc++){
		scanf("%d",&n);
		int a[n];
		int b[n];
		int index=0;
		int i;
		for(i=0;i<n;i++){
			scanf("%d",a+i);
		}
		srt(a,n);
		for(i=0;i<n;i++){
			if(!bld(b,index,a[i])){
				b[index]=a[i];
				index++;
			}
		}
		printf("%d\n",index);
	}
	return 0;
}

int gcds(int *a,int l){
	int x=a[0];
	int i;
	for(i=1;i<l;i++){
		x=gcd(x,a[i]);
	}
	return x;
}

int gcd(int a,int b){
	if(a<b) return gcd(b,a);
	if(!b) return a;
	return gcd(b,a%b);
}

int bld(int *a,int l,int td){///
	if(l==0) return 0;
	if(l==1){
		if(td%a[0]) return 0;
		return 1;
	}
	int ttt=gcds(a,l);
	if(td%ttt!=0) return 0;
	int i=td;
	if(td%a[l-1]==0) return 1;
	while(i>0){
		if(bld(a,l-1,i)==1) return 1;
		i-=a[l-1];
	}
	return 0;
}

void srt(int * a,int l){
	if(l<=1) return;
	int lft=l>>1;
	int rgt=l-lft;
	int i=0;
	int j=lft;
	int b[l];
	int k=0;
	srt(a,lft);
	srt(a+lft,rgt);
	while((i<lft)&&(j<l)){
		if(a[i]<=a[j]){
			b[k]=a[i];
			k++;
			i++;
		}else{
			b[k]=a[j];
			k++;
			j++;
		}
	}
	while(i<lft){
		b[k]=a[i];
		k++;
		i++;
	}
	while(j<l){
		b[k]=a[j];
		k++;
		j++;
	}
	for(i=0;i<l;i++){
		a[i]=b[i];
	}
}
