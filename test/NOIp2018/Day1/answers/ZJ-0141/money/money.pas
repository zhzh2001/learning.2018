uses math;
var n,t,i,j,k,m,ans,cc:longint;
    a,c:array[0..100]of longint;
    pos:array[0..25000]of longint;
    b:array[0..100]of boolean;
    flag:array[0..25000]of boolean;
procedure sort(l,r: longint);
      var i,j,x,y: longint;
      begin
         i:=l;j:=r;x:=a[(l+r) div 2];
         repeat
           while a[i]<x do inc(i);
           while x<a[j] do dec(j);
           if not(i>j) then begin
                y:=a[i];a[i]:=a[j];a[j]:=y;
                inc(i);j:=j-1;
           end;
         until i>j;
         if l<j then sort(l,j);
         if i<r then sort(i,r);
      end;
begin
  assign(input,'money.in');reset(input);
  assign(output,'money.out');rewrite(output);
  readln(t);
  for t:=t downto 1 do begin
        readln(n);cc:=0;
        for i:=1 to n do
                read(a[i]);
        readln;
        sort(1,n);m:=0;ans:=0;
        fillchar(b,sizeof(b),false);
        fillchar(pos,sizeof(pos),0);
        fillchar(flag,sizeof(flag),false);
        for i:=1 to n-1 do
                for j:=i+1 to n do
                        if a[j] mod a[i]= 0 then
                                b[j]:=true;
        for i:=1 to n do
                if not b[i] then begin
                        inc(m);
                        c[m]:=a[i];
                        cc:=max(cc,c[m]);
                end;
        for i:=1 to m do begin
                if flag[c[i]] then continue;
                inc(ans);
                flag[c[i]]:=true;
                for j:=1 to cc-c[i] do begin
                        for k:=1 to (cc-j)div c[i] do
                                if flag[j] then
                                        flag[j+c[i]*k]:=true;
                end;
        end;
        writeln(ans);
  end;
  close(input);close(output);
end.