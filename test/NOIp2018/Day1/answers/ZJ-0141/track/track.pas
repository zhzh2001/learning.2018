uses math;
var ans,n,i,j,k,m,zz,e,v,tot,count,l,r,mid,sum,x,y,z:longint;
    head,next,go,w:array[0..100005]of longint;
    p,q,len:array[1..50000]of longint;
    flag:array[0..100005]of boolean;
    a1,ab:boolean;
procedure add(x,y,z:longint);
  begin inc(tot);w[tot]:=z;next[tot]:=head[x];head[x]:=tot;go[tot]:=y;end;
procedure bfs(u:longint);
  var st,ed,e,v,i,j,l,r:longint;
  begin
    fillchar(p,sizeof(p),0);
    fillchar(q,sizeof(q),0);
    fillchar(flag,sizeof(flag),false);
    ans:=0;k:=0;
    st:=1;ed:=1;p[1]:=u;q[1]:=0;
    while st<=ed do begin
        i:=p[st];
        e:=head[i];
        flag[i]:=true;
        while e<>0 do begin
          v:=go[e];
          if flag[v] then begin e:=next[e];continue;end;
          inc(ed);
          p[ed]:=v;
          q[ed]:=q[st]+w[e];
          e:=next[e];
        end;
        if ans<q[st] then begin
                ans:=q[st];
                k:=p[st];
        end;
        inc(st);
    end;
  end;
procedure sort(l,r: longint);
      var i,j,x,y: longint;
      begin
         i:=l;j:=r;x:=len[(l+r) div 2];
         repeat
           while len[i]<x do inc(i);
           while x<len[j] do dec(j);
           if not(i>j) then begin y:=len[i];len[i]:=len[j];len[j]:=y;inc(i);j:=j-1;end;
         until i>j;
         if l<j then sort(l,j);
         if i<r then sort(i,r);
      end;
begin
  assign(input,'track.in');reset(input);
  assign(output,'track.out');rewrite(output);
  readln(n,m);zz:=maxlongint;
  for i:=1 to n-1 do begin
        readln(x,y,z);
        if x<>1 then a1:=true;
        if x<>y-1 then ab:=true;
        add(x,y,z);
        add(y,x,z);
        sum:=sum+z;
        zz:=min(zz,z);
  end;
  if not a1 then begin
        for i:=1 to n-1  do
                len[i]:=w[i*2-1];
        sort(1,n-1);
        writeln(len[n-m]);
        close(input);close(output);
        halt;
  end;
  if not ab then begin
        l:=zz;r:=sum;
        for i:=1 to n-1 do
                len[i]:=w[head[i]];
        while l<=r do begin
                mid:=(l+r)div 2;
                sum:=0;count:=0;
                for i:=1 to n-1do begin
                        sum:=sum+len[i];
                        if sum>=mid then begin
                                sum:=0;
                                inc(count);
                        end;
                end;
                if count>=m then begin
                        ans:=max(ans,mid);
                        l:=mid+1;
                end else r:=mid-1;
        end;
        writeln(ans);
        close(input);close(output);
        halt;
  end;
  if m=1 then begin
        bfs(1);
        bfs(k);
        writeln(ans);
        close(input);close(output);
        halt;
  end;
  close(input);close(output);
end.

