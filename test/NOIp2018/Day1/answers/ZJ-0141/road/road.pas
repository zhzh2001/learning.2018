uses math;
var  n,i,j,k,m,ans,sum,dim:longint;
     d:array[0..100005]of longint;

begin
  assign(input,'road.in');reset(input);
  assign(output,'road.out');rewrite(output);
  readln(n);
  for i:=1 to n do
        read(d[i]);
  sum:=maxlongint;
  inc(ans,d[1]);
  for i:=1 to n do begin
        if sum<d[i] then begin
                inc(ans,d[i]-sum);
                sum:=max(sum,d[i]);
        end else sum:=min(sum,d[i]);
  end;
  writeln(ans);
  close(input);close(output);
end.


