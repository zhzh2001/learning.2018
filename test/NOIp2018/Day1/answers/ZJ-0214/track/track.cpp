#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<cmath>
using namespace std;
typedef long long ll;
const ll size=5e4+10,maxn=1e3+10;
ll n,m,l,r,tot,flag,sum[size],g[size],d[size],f[maxn][maxn],nxdp[size],dp[size],w[size];
ll head[size],ver[size*2],nxt[size*2],edge[size*2];
ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
void add(ll x,ll y,ll z){
	ver[++tot]=y;nxt[tot]=head[x];head[x]=tot;edge[tot]=z;
}
void dfs(ll x,ll f){
	for(ll i=head[x];i;i=nxt[i]){
		ll y=ver[i],z=edge[i];
		if(y==f) continue;
		dfs(y,x);
		ll t=dp[y]+z;
		if(t>dp[x]) nxdp[x]=dp[x],dp[x]=t;
		else if(t>nxdp[x]) nxdp[x]=t;
	}
}
bool judge(ll val){
	ll cnt=0;
	
	return g[1]>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	flag=1;l=1e18,r=0;
	for(ll i=1;i<n;i++){
		ll x=read(),y=read(),z=read();
		add(x,y,z);add(y,x,z);
		if(y!=x+1) flag=0;
		l=min(l,z);r+=z;
		w[x]=z;
	}
	dfs(1,0);
	if(m==1){
		printf("%lld\n",dp[1]+nxdp[1]);
	}
	else if(flag){
		for(ll i=1;i<n;i++){
			sum[i]=w[i]+sum[i-1];
		}
		for(ll i=1;i<=m;i++){
			ll val=f[i-1][0];
			for(ll j=1;j<n;j++){
				f[i][j]=max(f[i][j],sum[j]+val);
				val=max(val,f[i-1][j]-sum[j]);
			}
		}
		printf("%lld\n",f[m][n-1]);
	}
	else{
		while(l<r){
			ll mid=(l+r+1)>>1;
			if(judge(mid)) l=mid;
			else r=mid-1;
		}
		printf("%lld\n",l);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
