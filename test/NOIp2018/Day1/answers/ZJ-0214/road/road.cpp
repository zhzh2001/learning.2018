#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<cmath>
using namespace std;
const int size=1e5+10;
int n,ans,tot,sum,a[size],v[size];
int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	while(1){
		int minx=1e5+10,last_pos=1,f=0;
		for(int i=1;i<=n;i++){
			if(a[i]) f=1,minx=min(minx,a[i]);
			if(a[i]==0){
				last_pos=i+1;continue;
			}
			if(a[i+1]==0){
				for(int j=last_pos;j<=i;j++){
					a[j]-=minx;
				}
				ans+=minx;minx=1e5+10;last_pos=i+2;i++;
			}
		}
		if(!f) break;
	}
	printf("%d",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}
