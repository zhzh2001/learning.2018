#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<cmath>
using namespace std;
typedef long long ll;
const ll size=1e2+10,maxn=1e5+10;
ll t,n,minx,p[size],a[size],v[size],f[maxn],vis[size];
ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
ll gcd(ll x,ll y){
	return y?gcd(y,x%y):x;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read();
	while(t--){
		memset(f,0,sizeof(f));
		memset(v,0,sizeof(v));
		memset(vis,0,sizeof(vis));
		n=read();minx=1e18;
		for(ll i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+1+n);
		for(ll i=n;i>1;i--){
			for(ll j=i-1;j>=1;j--){
				if(a[i]%a[j]==0){
					v[i]=1;break;
				}
			}
		}
		for(ll i=1;i<n;i++){
			if(v[i]) continue;
			for(ll j=i+1;j<=n;j++){
				if(v[j]) continue;
				if(gcd(a[i],a[j])==1)
				minx=min(minx,a[i]*a[j]-a[i]-a[j]+1);
			}
		}
		ll sum=0,m,maxn=0;
		for(ll i=1;i<=n;i++){
			if(!v[i]&&a[i]<=minx+1){
				p[++sum]=a[i];maxn=max(maxn,a[i]);
			}
		}
		m=sum;sum=m;
		f[0]=1;
		for(ll i=1;i<=m;i++){
			vis[i]=1;
			for(ll j=1;j<=maxn;j++) f[j]=0;
			for(ll j=1;j<=m;j++){
				if(vis[j]) continue;
				for(ll w=p[j];w<=maxn;w++){
					f[w]|=f[w-p[j]];
				}
			}
			if(f[p[i]]){
				sum--;vis[i]=1;continue;
			}
			vis[i]=0;
		}
		printf("%lld\n",sum);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

