#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read(){
	int Res=0,f=1;char ch=getchar();
	while (ch>'9'||ch<'0') f=(ch=='-'?-f:f),ch=getchar();
	while (ch>='0'&&ch<='9') Res=Res*10+ch-'0',ch=getchar();
	return Res*f;
}
int T,N,Ans,Max,A[105];
bool vis[25005];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (T=read();T;T--){
		N=read();Ans=N;Max=0;
		memset(vis,0,sizeof vis);
		for (int i=1;i<=N;i++) A[i]=read(),Max=max(A[i],Max);
		sort(A+1,A+1+N);vis[0]=1;
		for (int i=1;i<=N;i++){
			if (vis[A[i]]){Ans--;continue;}
			for (int j=0;j<=Max-A[i];j++) vis[j+A[i]]|=vis[j];
		}
		printf("%d\n",Ans);
	}
	return 0;
}
