#include<queue>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read(){
	int Res=0,f=1;char ch=getchar();
	while (ch>'9'||ch<'0') f=(ch=='-'?-f:f),ch=getchar();
	while (ch>='0'&&ch<='9') Res=Res*10+ch-'0',ch=getchar();
	return Res*f;
}
int N,M,Mx=2e9;
int tot,dis[50005],lnk[50005],nxt[100005],son[100005],w[100005];
inline void add(int x,int y,int z){son[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;w[tot]=z;}
namespace Case1{ //m=1
	int Max,id;
	void dfs(int x,int fa){
		for (int i=lnk[x];i;i=nxt[i]){
			if (son[i]==fa) continue;
			dis[son[i]]=dis[x]+w[i];dfs(son[i],x);
		}
	}
	void Solve(){
		dfs(1,0);
		for (int i=1;i<=N;i++) if (dis[i]>Max) Max=dis[i],id=i;
		memset(dis,0,sizeof dis);
		dfs(id,0);Max=0;
		for (int i=1;i<=N;i++) Max=max(Max,dis[i]);
		printf("%d\n",Max);
	}
}
namespace Case2{ //ai=1
	bool vis=1;
	int L=2e9,R=0,mid,Ans=0;
	bool check(int x){
		int Sum=0,Now=0,Co=0;
		for (int i=1;i<N;i++){
			if (Sum<x&&Co==2) return 0;
			if (Sum>=x) Sum=0,Now++,Co=0;
			Sum+=dis[i];Co++;
		}
		if (Sum>=x) Now++;
		return Now>=M;
	}
	void Solve(){
		for (int i=1;i<=tot;i+=2) dis[(i+1)>>1]=w[i],R+=w[i],L=min(L,w[i]);
		sort(dis+1,dis+N);
		while (L<=R){
			mid=(L+R)>>1;
			if (check(mid)) Ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",Ans);
	}
}
namespace Case3{ //bi=ai+1
	bool vis=1;
	int L=2e9,R=0,mid,Ans=0;
	bool check(int x){
		int Sum=0,Now=0;
		for (int i=1;i<N;i++){if (Sum>=x) Sum=0,Now++;Sum+=dis[i];}
		if (Sum>=x) Now++;
		return Now>=M;
	}
	void Solve(){
		for (int i=1;i<=tot;i+=2) dis[(i+1)>>1]=w[i],R+=w[i],L=min(L,w[i]);
		sort(dis+1,dis+N);
		while (L<=R){
			mid=(L+R)>>1;
			if (check(mid)) Ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",Ans);
	}
}
//how to write deg<=3
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	N=read();M=read();
	for (int i=1;i<N;i++){
		int x=read(),y=read(),z=read();
		Case2::vis&=(x==1);Case3::vis&=(y==x+1);
		add(x,y,z);add(y,x,z);Mx=min(Mx,z);
	}
	if (M==1) Case1::Solve();
	else if (Case2::vis) Case2::Solve();
	else if (Case3::vis) Case3::Solve();
	else printf("%d\n",Mx);
	return 0;
}
