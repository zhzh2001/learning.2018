#include<cstdio>
#include<algorithm>
#define par pair<int,int>
#define mk(x,y) make_pair(x,y)
using namespace std;
inline int read(){
	int Res=0,f=1;char ch=getchar();
	while (ch>'9'||ch<'0') f=(ch=='-'?-f:f),ch=getchar();
	while (ch>='0'&&ch<='9') Res=Res*10+ch-'0',ch=getchar();
	return Res*f;
}
int N,M,Ans;
par T[400005];
par getMin(int l,int r){
	par Min=mk(2e9,0);
	for (l=l+M-1,r=r+M+1;l^r^1;l>>=1,r>>=1){
		if (~l&1) Min=min(Min,T[l^1]);
		if (r&1) Min=min(Min,T[r^1]);
	}
	return Min;
}
void Solve(int l,int r,int d){
	if (l>r) return ;
	par Now=getMin(l,r);
	Ans+=Now.first-d;
	Solve(l,Now.second-1,Now.first);Solve(Now.second+1,r,Now.first);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	N=read();
	for (M=1;M<N;M<<=1);
	for (int i=M+1;i<=M+N;i++) T[i].first=read(),T[i].second=i-M;
	for (int i=M-1;i;i--) T[i]=min(T[i<<1],T[i<<1|1]);
	Solve(1,N,0);
	printf("%d\n",Ans);
	return 0;
}
