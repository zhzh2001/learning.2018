#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
typedef pair<int,int> pii;
const int MAXN=50000;
struct Edge
{
	int to,next,w;
}e[2*MAXN+10];
int head[MAXN+10];
void addEdge(int a,int b,int w)
{
	static int c=0;
	e[++c]=(Edge){b,head[a],w};
	head[a]=c;
	e[++c]=(Edge){a,head[b],w};
	head[b]=c;
}
int n,m;
vector<int> a[MAXN+10];
int len;
int calc(int n,const vector<int>& x,int p)
{
	int ans=0;
	int pl=1,pr=n;
	if (pr==p) --pr;
	if (pl==p) ++pl;
	while(pl<pr) {
		while(pl<pr && (x[pl]+x[pr]<len || pl==p)) {
			++pl;
		}
		if (pl<pr) {
			++ans;
			--pr;
			if (pr==p) --pr;
			++pl; 
			if (pl==p) ++pl;
		}
	}
	return ans;
}
pii dfs(int now,int fa)
{
	int nowans=0;
	int cnt=0;
	for(int i=head[now]; i!=0; i=e[i].next) {
		int v=e[i].to;
		if (v!=fa) {
			pii res=dfs(v,now);
			res.second+=e[i].w;
			if (res.second>=len) {
				nowans+=res.first+1;
				continue;
			}
			nowans+=res.first;
			a[now][++cnt]=res.second;
		}
	}
	sort(a[now].begin()+1,a[now].begin()+1+cnt);
	int num=calc(cnt,a[now],0);
	nowans+=num;
	if (num*2==cnt) return pii(nowans,0);
	int l=1,r=cnt,ans=1;
	while(l<=r) {
		int mid=(l+r)/2;
		if (calc(cnt,a[now],mid)==num) {
			ans=mid;
			l=mid+1;
		} else {
			r=mid-1;
		}
	}
	return pii(nowans,a[now][ans]);
}
bool check(int mid)
{
	len=mid;
	pii res=dfs(1,0);
	return res.first>=m;
}
void dfs0(int now,int fa)
{
	a[now].push_back(0);
	for(int i=head[now]; i!=0; i=e[i].next) {
		int v=e[i].to;
		if (v!=fa) {
			dfs0(v,now);
			a[now].push_back(0);
		}
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	LL sum=0;
	for(int i=1; i<n; ++i) {
		int a,b,w;
		scanf("%d%d%d",&a,&b,&w);
		addEdge(a,b,w);
		sum+=w;
	}
	dfs0(1,0);
	LL l=0,r=sum/m;
	LL ans=0;
	while(l<=r) {
		LL mid=(l+r)/2;
		if (check(mid)) {
			ans=mid;
			l=mid+1;
		} else {
			r=mid-1;
		}
	}
	printf("%lld\n",ans);
	return 0;
}

