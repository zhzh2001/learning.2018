#include <bits/stdc++.h>
using namespace std;
const int MAXN=100;
const int MAXM=25000;
int a[MAXN+10];
bool b[MAXM+10];
int c[MAXM+10];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--) {
		memset(b,false,sizeof(b));
		memset(c,0,sizeof(c));
		int n;
		scanf("%d",&n);
		int m=0;
		for(int i=1; i<=n; ++i) {
			scanf("%d",&a[i]);
			m=max(m,a[i]);
		}
		sort(a+1,a+1+n);
		for(int i=1; i<=n; ++i) {
			c[a[i]]=i;
		}
		b[0]=true;
		int ans=n;
		for(int i=1; i<=n; ++i) {
			if (a[i]==0) continue;
			c[a[i]]=0;
			for(int j=0; j+a[i]<=m; ++j) {
				if (b[j]) {
					int now=j+a[i];
					b[now]=true;
					if (c[now]>0) {
						a[c[now]]=0;
						c[now]=0;
						--ans;
					}
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}

