#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const int MAXN=100000;
int a[MAXN+10];
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n;
	scanf("%d",&n);
	for(int i=1; i<=n; ++i) {
		scanf("%d",&a[i]);
	}
	int now=a[1];
	LL ans=a[1];
	for(int i=2; i<=n; ++i) {
		if (a[i]>now) {
			ans+=(a[i]-now);
		}
		now=a[i];
	}
	printf("%lld\n",ans);
	return 0;
}

