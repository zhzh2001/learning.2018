#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
typedef long long LL;
int n;
LL ans=0,a[1000001];
int main() {
    freopen("road.in","r",stdin);
    freopen("road.out","w",stdout);
    scanf("%d",&n);
    for (int i=1;i<=n;i++) scanf("%lld",&a[i]);
    a[0]=0;
    for (int i=1;i<=n;i++)
        if (a[i]>a[i-1]) ans+=a[i]-a[i-1];
    printf("%lld\n",ans);
    return 0;
}
