#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <bitset>
#include <algorithm>
using namespace std;
const int V=25000;
const int N=1000;
int t,n,a[N+10];
bool vis[V+10];
int main() {
    freopen("money.in","r",stdin);
    freopen("money.out","w",stdout);
    scanf("%d",&t);
    while (t--) {
        for (int i=0;i<=V;i++) vis[i]=0;
        vis[0]=1;
        scanf("%d",&n);
        for (int i=1;i<=n;i++) scanf("%d",&a[i]);
        sort(a+1,a+n+1);
        int ans=0;
        for (int i=1;i<=n;i++) {
            if (vis[a[i]]) continue;
            ans++;
            for (int j=a[i];j<=V;j++)
                vis[j]|=vis[j-a[i]];
        }
        printf("%d\n",ans);
    }
    return 0;
}
