#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
int n,m,a[1000001];
bool check(int x) {
    int k=1;
    int cnt=0;
    int px=n-1;
    while (px>=1&&a[px]>=x) {
        px--;
        cnt++;
    }
    for (int i=px;i>k;i--) {
        while (k<i&&a[k]+a[i]<x) k++;
        if (k>=i) break;
        cnt++;
        k++;
    }
    if (cnt<m) return 0;
    return 1;
}
int Solve() {
    int l=1,r=1e9;
    while (l<r) {
        int mid=(l+r+1)/2;
        if (check(mid)) l=mid;
        else r=mid-1;
    }
    return l;
}
int main() {
    scanf("%d%d",&n,&m);
    for (int i=1;i<n;i++) {
        int u,v;
        scanf("%d%d%d",&u,&v,&a[i]);
    }
    sort(a+1,a+n);
    int ans=Solve();
    printf("%d\n",ans);
    return 0;
}
