#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
int n,m,a[1000001];
bool check(int x) {
    int cnt=0,sum=0;
    for (int i=1;i<n;i++) {
        sum+=a[i];
        if (sum>=x) {
            cnt++;
            sum=0;
        }
    }
    if (cnt>=m) return 1;
    return 0;
}
int Solve() {
    int l=1,r=1e9;
    while (l<r) {
        int mid=(l+r+1)/2;
        if (check(mid)) l=mid;
        else r=mid-1;
    }
    return l;
}
int main() {
    scanf("%d%d",&n,&m);
    for (int i=1;i<n;i++) {
        int u,v;
        scanf("%d%d%d",&u,&v,&a[i]);
    }
    int ans=Solve();
    printf("%d\n",ans);
    return 0;
}
