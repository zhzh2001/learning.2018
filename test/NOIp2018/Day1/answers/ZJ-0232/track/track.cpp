#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
const int M=2e5;
int n,m,tot=0;
int head[M+10];
int vec[M+10];
pair <int,int> f[M+10];
struct data {
    int next,num,w;
}edge[M+10];
void Add(int u,int v,int w) {
    edge[++tot].next=head[u];
    edge[tot].num=v;
    edge[tot].w=w;
    head[u]=tot;
}
void Dfs(int x,int fat,int wx,int len) {
    int ans=0;
    for (int i=head[x];i!=-1;i=edge[i].next) {
        int kx=edge[i].num;
        if (kx==fat) continue;
        Dfs(kx,x,edge[i].w,len);
        pair <int,int> pii=f[kx];
        ans+=pii.first;
    }
    int sz=0;
    for (int i=head[x];i!=-1;i=edge[i].next) {
        int kx=edge[i].num;
        if (kx==fat) continue;
        vec[sz++]=f[kx].second;
    }
    sort(vec,vec+sz);
    int cnt=0;
    int k=0;
    for (int i=sz-1;i>k;i--) {
        while (i>k&&vec[i]+vec[k]<len) k++;
        if (i<=k) break;
        cnt++;
        k++;
    }
    ans+=cnt;
    int lx=-1,rx=sz-1;
    while (lx<rx) {
        int mid=(lx+rx+1)/2;
        int nowc=0;
        int k=0;
        if (k==mid) k++;
        for (int i=sz-1;i>k;i--) {
            if (i==mid) continue;
            while (i>k&&vec[i]+vec[k]<len) {
                k++;
                if (k==mid) k++;
            }
            if (i<=k) break;
            nowc++;
            k++;
            if (k==mid) k++;
        }
        if (nowc<cnt) rx=mid-1;
        else lx=mid;
    }
    if (lx==-1) {
        if (wx>=len) f[x]=make_pair(ans+1,0);
        else f[x]=make_pair(ans,wx);
        return;
    }
    if (vec[lx]+wx>=len) f[x]=make_pair(ans+1,0);
    else f[x]=make_pair(ans,vec[lx]+wx);    
}
int Work(int x) {
    Dfs(1,-1,0,x);
    return f[1].first;
}
int Solve() {
    int l=1,r=1e9;
    while (l<r) {
        int mid=(l+r+1)/2;
        if (Work(mid)>=m) l=mid;
        else r=mid-1;
    }
    return l;
}
int main() {
    freopen("track.in","r",stdin);
    freopen("track.out","w",stdout);
    memset(head,-1,sizeof(head));
    scanf("%d%d",&n,&m);
    for (int i=1;i<n;i++) {
        int u,v,w;
        scanf("%d%d%d",&u,&v,&w);
        Add(u,v,w);
        Add(v,u,w);
    }
    int ans=Solve();
    printf("%d\n",ans);
    return 0;
}
