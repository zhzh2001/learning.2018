#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
const int N=1e6;
int n,m,head[N+10],maxd,rt,tot=0;
struct data {
    int next,num,w;
}edge[N+10];
void Add(int u,int v,int w) {
    edge[++tot].next=head[u];
    edge[tot].num=v;
    edge[tot].w=w;
    head[u]=tot;
}
void Dfs(int x,int fat,int dx) {
    if (dx>maxd) {
        maxd=dx;
        rt=x;
    }
    for (int i=head[x];i!=-1;i=edge[i].next) {
        int kx=edge[i].num;
        if (kx==fat) continue;
        Dfs(kx,x,dx+edge[i].w);
    }
}
int main() {
    memset(head,-1,sizeof(head));
    scanf("%d%d",&n,&m);
    for (int i=1;i<n;i++) {
        int u,v,w;
        scanf("%d%d%d",&u,&v,&w);
        Add(u,v,w);
        Add(v,u,w);
    }
    maxd=0;
    Dfs(1,-1,0);
    maxd=0;
    Dfs(rt,-1,0);
    printf("%d\n",maxd);
    return 0;
}
