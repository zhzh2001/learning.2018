#include <bits/stdc++.h>
using namespace std;
#define rep(i,l,r) for(int i=l;i<=r;++i)
#define per(i,r,l) for(int i=r;i>=l;--i)
inline int rd(){
	char c=getchar();int x=0,f=1;
	for(;c<'0'||c>'9';c=getchar())
		if(c=='-') f=-1;
	for(;c>='0'&&c<='9';c=getchar())
		x=x*10+c-'0';
	return x*f;
}
const int N=111,V=25000;
bool h[V+9];
int a[N];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=rd();
	for(;T;--T){
		int n=rd();
		rep(i,1,n) a[i]=rd();
		sort(a+1,a+1+n);
		
		int sum=0,v=a[n];
		rep(i,0,v) h[i]=0;
		h[0]=1;
		rep(i,1,n){
			if(h[a[i]]) continue;
			sum++;
			rep(j,1,v) if(a[i]<=j)
				h[j]=h[j]|h[j-a[i]];
		}
		printf("%d\n",sum);
	}
	return 0;
}
