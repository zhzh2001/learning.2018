#include <bits/stdc++.h>
using namespace std;
#define rep(i,l,r) for(int i=l;i<=r;++i)
#define per(i,r,l) for(int i=r;i>=l;--i)
inline int rd(){
	char c=getchar();int x=0,f=1;
	for(;c<'0'||c>'9';c=getchar())
		if(c=='-') f=-1;
	for(;c>='0'&&c<='9';c=getchar())
		x=x*10+c-'0';
	return x*f;
}
int n,sum;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=rd();
	int a=0,b;
	rep(i,1,n){
		b=rd();
		if(b>a) sum+=b-a;
		a=b;
	}
	printf("%d",sum);
	return 0;
}
