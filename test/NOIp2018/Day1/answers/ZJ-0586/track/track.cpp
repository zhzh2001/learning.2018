#include <bits/stdc++.h>
using namespace std;
#define rep(i,l,r) for(int i=l;i<=r;++i)
#define per(i,r,l) for(int i=r;i>=l;--i)
inline int rd(){
	char c=getchar();int x=0,f=1;
	for(;c<'0'||c>'9';c=getchar())
		if(c=='-') f=-1;
	for(;c>='0'&&c<='9';c=getchar())
		x=x*10+c-'0';
	return x*f;
}
typedef long long ll;
const int N=50009;
int n,m;
vector<int>s[N],w[N];

int d[N],cnt;
multiset<int>st;
multiset<int>::iterator it;
void dfs(int fa,int anc,int v){
	d[fa]=0;
	per(i,s[fa].size()-1,0){
		int son=s[fa][i];
		if(son!=anc) dfs(son,fa,v);
	}
	
	per(i,s[fa].size()-1,0){
		int son=s[fa][i],ww=w[fa][i];
		if(son!=anc){
			ww+=d[son];
			if(ww>=v) cnt++;
			else st.insert(ww);
		}
	}
	
	for(;st.size();){
		int vvv=(*st.begin());
		st.erase(st.begin());
		it=st.lower_bound(v-vvv);
		if(it==st.end()) d[fa]=vvv;
		else{
			cnt++;
			st.erase(it);
		}
	}
	
}
int calc(int v){
	cnt=0;
	dfs(1,0,v);
	return cnt;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=rd();m=rd();
	rep(i,1,n-1){
		int u=rd(),v=rd(),W=rd();
		s[u].push_back(v);w[u].push_back(W);
		s[v].push_back(u);w[v].push_back(W);
	}
	int L=0,R=5e8;
	for(;L<R;){
		int mid=(L+R)>>1;
		if(calc(mid+1)>=m) L=mid+1;
		else R=mid; 
	}
	printf("%d",L);
	return 0;
}
