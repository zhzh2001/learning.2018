#include<cstdio>
#include<algorithm>
using namespace std;
struct edge{int to,nxt,val;}e[100005];
int n,m,tot,fir[50005],f[100005];
int cmp(const int &x,const int &y) {return x>y;}
void addedge(int u,int v,int w)
{
	e[++tot].to=v;e[tot].nxt=fir[u];
	e[tot].val=w;fir[u]=tot;
}
void dfs1(int x,int pre,int &ret)
{
	int mx1=0,mx2=0;
	for(int i=fir[x];i;i=e[i].nxt)
	if(e[i].to!=pre)
	{
		dfs1(e[i].to,x,ret);
		int s=f[e[i].to]+e[i].val;
		if(s>mx1) mx2=mx1,mx1=s;
		else if(s>mx2) mx2=s;
	}
	f[x]=mx1;
	if(mx1+mx2>ret) ret=mx1+mx2;
}
void pt1() {int ret=0;dfs1(1,0,ret);printf("%d\n",ret);}
void pt2()
{
	for(int i=1;i<n;i++) f[i]=e[2*i].val;
	sort(f+1,f+n,cmp);int ret=0x3f3f3f3f;
	for(int i=1;i<=m;i++)
	ret=min(ret,f[i]+f[2*m+1-i]);
	printf("%d\n",ret);
}
int pd3(int x)
{
	int s=0,cnt=0,p=1;
	while(p<n)
	{
		while(p<n&&s<x) 
		{
			int i=fir[p];
			while(e[i].to<p) i=e[i].nxt;
			s+=e[i].val,p++;	
		}
		if(s>=x) cnt++,s=0;
	}
	return cnt>=m;
}
void pt3()
{
	int l=1,r=5e8,ret;
	while(l<=r)
	{
		int mid=(l+r)>>1;
		if(pd3(mid)) ret=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",ret);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int fg1=1,fg2=1;
	for(int i=1;i<n;i++)
	{
		int x,y,z;scanf("%d%d%d",&x,&y,&z);
		addedge(x,y,z);addedge(y,x,z);
		if(x!=1) fg1=0;if(y!=x+1) fg2=0;
	}
	if(m==1) {pt1();return 0;}
	if(fg1) {pt2();return 0;}
	if(fg2) {pt3();return 0;}
	return 0;
}
