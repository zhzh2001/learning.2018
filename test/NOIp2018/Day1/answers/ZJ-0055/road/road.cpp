#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
int d[100005],a[100005],s[400005],ls[400005],rs[400005];
void ch(int k,int l,int r,int p)
{
//	if(p<l||p>r) return;
	if(l==r) {s[k]=ls[k]=rs[k]=1;return;}
	int mid=(l+r)>>1;
	if(p<=mid) ch(2*k,l,mid,p);else ch(2*k+1,mid+1,r,p);
	s[k]=s[2*k]+s[2*k+1]-(ls[2*k+1]&&rs[2*k]?1:0);
	ls[k]=ls[2*k];rs[k]=rs[2*k+1];
}
int cmp(const int &x,const int &y) {return d[x]>d[y];}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n,mx=-1;scanf("%d",&n);
	for(int i=1;i<=n;i++) scanf("%d",&d[i]),a[i]=i,mx=max(mx,d[i]);
	ll ans=0;
	sort(a+1,a+n+1,cmp);int p=1;
	for(int i=mx;i;i--)
	{
		while(d[a[p]]>=i) 
		{
			ch(1,1,n,a[p]);p++;
		}
		ans+=s[1];
	}
	printf("%lld\n",ans);
	return 0;
}
