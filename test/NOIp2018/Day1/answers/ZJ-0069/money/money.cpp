#include <bits/stdc++.h>
using namespace std;
#define LL long long
#define M 105
bool MM1;
int n,A[M];
struct P80{
	int dp[25005];
	void solve(){
		memset(dp,0,sizeof(dp));
		int Mx=A[n],Ans=0;dp[0]=1;
		for(int i=1;i<=n;i++){
			if(dp[A[i]])continue;
			Ans++;
			dp[A[i]]=1;
			for(int j=A[i]+1;j<=Mx;j++)if(!dp[j])
				for(int k=0;k<=j;k+=A[i])
					dp[j]|=dp[k]&&dp[j-k];
		}
		printf("%d\n",Ans);
	}
}p;
bool MM2;
int main(){
//	 printf("%lf\n",(&MM2-&MM1)/1024.0/1024.0);
	//文件名 内存 LL
	// 题目 极限数据 数组
	//Mod(1e9+7)
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout); 
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			scanf("%d",&A[i]);
		sort(A+1,A+1+n);
		p.solve();
	}
	return 0;
}
