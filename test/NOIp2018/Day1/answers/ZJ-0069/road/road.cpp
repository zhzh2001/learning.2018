#include <bits/stdc++.h>
using namespace std;
#define LL long long
#define M 100005
bool MM1;
int n,A[M];
struct P70{
	void solve(){
		int Ans=0;
		while(1){
			int add=0,p=1,Mi=1e9,L=0;
			while(p<=n){
				if(A[p]==0&&Mi!=1e9){
					for(int i=L+1;i<p;i++)
						if(A[i]>0)A[i]-=Mi;
					add+=Mi,Mi=1e9;
					L=p;
				}
				else if(A[p]!=0)Mi=min(Mi,A[p]);
				p++;
			}
			if(Mi!=1e9){
				add+=Mi;
				for(int i=L+1;i<=n;i++)
					if(A[i]>0)A[i]-=Mi;
			}
			if(!add)break;
			Ans+=add;
		}
		printf("%d\n",Ans);
	}
}p70;
struct P{
	int mi[M][18];
	int Min(int a,int b){
		if(A[a]<A[b])return a;
		return b;
	}
	struct TREE{
		struct Node{
			int l,r,add;
		}tree[M<<2];
		#define fa tree[p]
		void build(int L,int R,int p){
			fa.l=L,fa.r=R,fa.add=0;
			if(L==R)return;
			int mid=L+R>>1;
			build(L,mid,p<<1);
			build(mid+1,R,p<<1|1);
		}
		void Down(int p){
			tree[p<<1|1].add+=fa.add;
			tree[p<<1].add+=fa.add;
			fa.add=0;
		}
		int Query(int x,int p){
			if(fa.l==fa.r)return fa.add;
			int mid=fa.l+fa.r>>1;
			Down(p);
			if(x>mid)return Query(x,p<<1|1);
			else return Query(x,p<<1);
		}
		void Update(int L,int R,int x,int p){
			if(fa.l==L&&fa.r==R){
				fa.add+=x;
				return;
			}
			int mid=fa.l+fa.r>>1;
			if(R<=mid)Update(L,R,x,p<<1);
			else if(L>mid)Update(L,R,x,p<<1|1);
			else {
				Update(L,mid,x,p<<1);
				Update(mid+1,R,x,p<<1|1);
			}
		}
	}T;
	struct Node{
		int l,r;
	}Q[M<<2];
	int Query(int L,int R){
		int k=Log[R-L+1];
		return Min(mi[L][k],mi[R-(1<<k)+1][k]);
	}
	int Log[M];
	void solve(){
		for(int i=1;i<=n;i++){
			mi[i][0]=i;
			if(i>1)Log[i]=Log[i>>1]+1;
		}
		for(int i=1;(1<<i)<=n;i++)
			for(int j=1;j+(1<<i)-1<=n;j++)
				mi[j][i]=Min(mi[j][i-1],mi[j+(1<<i-1)][i-1]);
		int L=0,R=-1;
		Q[++R]=(Node){1,n};
		T.build(1,n,1);
		LL Ans=0;
		while(L<=R){
			int l=Q[L].l,r=Q[L++].r;
			int x=Query(l,r);
			int Res=A[x]+T.Query(x,1);
			Ans+=Res;
			T.Update(l,r,-Res,1);
			if(l<=x-1)Q[++R]=(Node){l,x-1};
			if(x+1<=r)Q[++R]=(Node){x+1,r};
		}
		printf("%lld\n",Ans);
	}
}p;
bool MM2;
int main(){
//	 printf("%lf\n",(&MM2-&MM1)/1024.0/1024.0);
	//文件名 内存 LL
	// 题目 极限数据 数组
	//Mod(1e9+7)
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout); 
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%d",&A[i]);
	if(n<=1000)p70.solve();
	else p.solve();
	return 0;
}
