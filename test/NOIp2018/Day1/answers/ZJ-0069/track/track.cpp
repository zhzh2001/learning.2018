#include <bits/stdc++.h>
using namespace std;
#define LL long long
#define M 100005
bool MM1;
int n,m;
struct Edge{
	int to,nx,w;
}edge[M<<1];
int h[M],tt;
void link(int a,int b,int w){
	edge[++tt].to=b;
	edge[tt].nx=h[a];
	edge[tt].w=w;
	h[a]=tt;
}
#define DEG(i,st) for(int i=h[st];i;i=edge[i].nx)
struct P1{	
	LL mx,p;
	void dfs(int fa,int now,LL d){
		if(d>mx)p=now,mx=d;
		DEG(i,now){
			int to=edge[i].to;
			if(to==fa)continue;
			dfs(now,to,d+edge[i].w);
		}
	}
	void solve(){mx=-1;
		dfs(-1,1,0);
		mx=-1;int ed=p;
		dfs(-1,ed,0);
		printf("%lld\n",mx);
	}
}p1;
LL Val[M];
struct P2{
	bool check(LL x){
		int last=1,cnt=0;
		for(int i=2;i<=n;i++)
			if(Val[i]-Val[last]>=x)
				cnt++,last=i;
		return cnt>=m;
	}
	void solve(){
		for(int i=2;i<=n;i++)Val[i]+=Val[i-1];
		LL L=0,R=1e18,Res=-1;
		while(L<=R){
			LL mid=L+R>>1;
			if(check(mid))Res=mid,L=mid+1;
			else R=mid-1;	
		}
		printf("%lld\n",Res);
	}
}p2;
struct P3{
	bool check(LL x){
		int cnt=0,ed=n;
		for(int i=n;i>=1&&Val[i]>=x;i--)
			cnt++,ed=i-1;
		int p=1;
		for(int i=ed;i>p;i--){
			while(p<i&&Val[p]+Val[i]<x)p++;
			if(i>p)cnt++;
		}
		return cnt>=m;
	}
	void solve(){
		for(int i=1;i<n;i++)Val[i]=Val[i+1];
		sort(Val+1,Val+n);
		LL L=0,R=1e18,Res=-1;
		while(L<=R){
			LL mid=L+R>>1;
			if(check(mid))Res=mid,L=mid+1;
			else R=mid-1;	
		}
		printf("%lld\n",Res);
	}
}p3;
bool MM2;
int main(){
//	 printf("%lf\n",(&MM2-&MM1)/1024.0/1024.0);
	//文件名 内存 LL
	// 题目 极限数据 数组
	//Mod(1e9+7)
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout); 
	scanf("%d%d",&n,&m);
	bool fl1=1,fl2=1;
	for(int a,b,v,i=1;i<n;i++){
		scanf("%d%d%d",&a,&b,&v);
		link(a,b,v),link(b,a,v);
		if(a!=1)fl2=0;
		if(b!=a+1)fl1=0;
		Val[b]=v;
	}
	if(m==1)p1.solve();
	else if(fl1==1)p2.solve();
	else p3.solve();
	return 0;
}
