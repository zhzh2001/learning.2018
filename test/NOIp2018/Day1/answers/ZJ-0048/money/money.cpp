#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
#define LL long long
#define pb push_back
#define mp make_pair
#define fi first
#define se second
typedef pair<int,int> pa;
bool f[3000010];
int T,n,mx,a[500010],ans;
void doit(){
	cin>>n;
	mx=0;
	FOR(i,1,n) scanf("%d",&a[i]),mx=max(mx,a[i]);
	sort(a+1,a+n+1);
	FOR(i,1,mx) f[i]=0;
	f[0]=1;
	ans=0;
	FOR(i,1,n){
		if (f[a[i]]) continue;
		++ans;
		FOR(j,a[i],mx) f[j]|=f[j-a[i]];
	}
	printf("%d\n",ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	cin>>T;
	while (T--) doit();
	return 0;
}
