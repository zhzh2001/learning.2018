#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
#define LL long long
#define pb push_back
#define mp make_pair
#define fi first
#define se second
typedef pair<int,int> pa;
int n,m;
int main(){
	srand(time(0));
	freopen("track.in","w",stdout);
	n=50000;
	m=1;
	cout<<n<<' '<<m<<endl;
	FOR(i,2,n){
		int x=i;
		int y=(i-1)/2+1;
		if (rand()&1) swap(x,y);
		printf("%d %d %d\n",x,y,rand()%10000+1);
	}
	return 0;
}
