#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
#define LL long long
#define pb push_back
#define mp make_pair
#define fi first
#define se second
typedef pair<int,int> pa;
pa f[100010];
bool gg[500010];
int val[500010],too[500010],nxt[500010],nedge,hed[500010];
int n,m,l,r,o,x,y,w,sum,tmp[500010],ts;
void ae(int x,int y,int w){
	nxt[++nedge]=hed[x];
	hed[x]=nedge;
	too[nedge]=y;
	val[nedge]=w;
}
int cal(){
	int R=ts,L=1;
	int mx=0;
	while (L<=R){
		if (gg[R]){--R;continue;}
		if (tmp[R]>=o) ++mx,--R;
		else{
			while (L<R && tmp[L]+tmp[R]<o) ++L;
			if (gg[L]) ++L;
			if (L>=R) break;
			++mx;
			++L,--R;
		}
	}
	return mx;
}
void dfs(int x,int l){
	for (int i=hed[x];i;i=nxt[i]){
		int y=too[i];
		if (y==l) continue;
		dfs(y,x);
	}
	ts=0;
	f[x].fi=0;
	for (int i=hed[x];i;i=nxt[i]){
		int y=too[i];
		if (y==l) continue;
		tmp[++ts]=f[y].se+val[i];
		f[x].fi+=f[y].fi;
	}
	if (!ts){
		f[x]=mp(0,0);
		return;
	}
	
//	if (f[x].fi>=m) return;
	
	sort(tmp+1,tmp+ts+1);
	int mx=cal();
	int le=0,ri=ts;
	while (le<ri){
		int mo=((le+ri)>>1)+1;
		gg[mo]=1;
		int t=cal();
		gg[mo]=0;
		if (t>=mx) le=mo;
		else ri=mo-1;
	}
	f[x].fi+=mx;
	f[x].se=tmp[ri];
}
bool ck(){
	if (o<1) return 1;
	dfs(1,0);
	return (f[1].fi>=m);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	FOR(i,2,n){
		scanf("%d%d%d",&x,&y,&w);
		ae(x,y,w),ae(y,x,w);
		sum+=w;
	}
	l=0,r=sum/m;
	while (l<r){
		o=(l+r>>1)+1;
		if (ck()) l=o;
		else r=o-1;
	}
	cout<<r<<endl;
	return 0;
}
