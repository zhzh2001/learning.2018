#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
#define LL long long
#define pb push_back
#define mp make_pair
#define fi first
#define se second
typedef pair<int,int> pa;
int n,m,x,y,w,f[500010],ans;
int hed[500010],nedge,too[500010], nxt[500010],val[500010];
void ae(int x,int y,int w){
	nxt[++nedge]=hed[x];
	hed[x]=nedge;
	too[nedge]=y;
	val[nedge]=w;
}
void dfs(int x,int l){
	int mx1=0,mx2=0;
	for (int i=hed[x];i;i=nxt[i]){
		int y=too[i];
		if (y==l) continue;
		dfs(y,x);
		if (f[y]+val[i]>mx1){
			mx2=mx1;
			mx1=f[y]+val[i];
		}
		else if (f[y]+val[i]>mx2) mx2=f[y]+val[i];
	}
	f[x]=mx1;
	ans=max(ans,mx1+mx2);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("bf.out","w",stdout);
	cin>>n>>m;
	if (m!=1) return puts("gg"),0;
	FOR(i,2,n){
		scanf("%d%d%d",&x,&y,&w);
		ae(x,y,w),ae(y,x,w);
	}
	dfs(1,0);
	cout<<ans<<endl;
	return 0;
}
