#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 100010
using namespace std;

int n,a[N],acht;

void solve(int l,int r,int d) {
	int mn=1e9;
	if(l>r) return;
	if(l==r) {
		acht+=a[l]-d;return;
	}
	for(int i=l;i<=r;i++) {
		if(a[i]<mn) mn=a[i];
	}
	acht+=mn-d;
	int p=l-1;
	for(int i=l;i<=r;i++) {
		if(a[i]==mn) {
			solve(p+1,i-1,mn);
			p=i;
		}
	}
	if(p<r) solve(p+1,r,mn);
}

int main() {
	freopen("road.in","r",stdin);
	freopen("road2.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++) {
		scanf("%d",&a[i]);
	}
	acht=0;
	solve(1,n,0);
	printf("%d\n",acht);
	return 0;
}
