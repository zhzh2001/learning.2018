#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#define N 100010
using namespace std;

int n,a[N],acht,tot;
bool f[N];
vector<int> g[20010];

int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	memset(f,0,sizeof f);tot=1;
	a[0]=0;acht=0;f[0]=1;f[n+1]=1;
	for(int i=1;i<=n;i++) {
		scanf("%d",&a[i]);
		g[a[i]].push_back(i);
		if(a[i]==0) {
			f[i]=1;
			if(!f[i-1] && !f[i+1]) {
				tot++;
			}
			if(f[i-1] && f[i+1]) {
				tot--;
			}
		}
	}
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++) if(a[i]!=a[i-1]) {
		acht+=(a[i]-a[i-1])*tot;
		for(unsigned int j=0;j<g[a[i]].size();j++) {
			int p=g[a[i]][j];
			f[p]=true;
			if(!f[p-1] && !f[p+1]) {
				tot++;
			}
			if(f[p-1] && f[p+1]) {
				tot--;
			}
		}
	}
	printf("%d\n",acht);
	return 0;
}
