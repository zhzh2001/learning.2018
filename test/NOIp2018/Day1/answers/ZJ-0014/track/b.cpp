#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 50010
using namespace std;

void read(int &x) {
	x=0;char ch=getchar();bool flag;
	while(ch!='-' && !(ch>='0' && ch<='9')) ch=getchar();
	if(ch=='-') ch=getchar(),flag=true; else flag=false;
	for(;ch>='0' && ch<='9';ch=getchar()) x=x*10+ch-48;
	if(flag) x=-x;
}

struct Edge {
	int to,Dist,ne;
} e[N<<1];

int n,m,g[N],cnt,sum,p[N],mxs,NN;
bool vis[N];

bool check1(int x) {
	int ct=0,dis=0;
	for(int u=1;u<n;u++) {
		if(g[u]!=-1) {
			dis+=e[g[u]].Dist;
			if(dis>=x) {
				ct++;
				dis=0;
			}
		}
	}
	return ct>=m;
}

void solve1() {
	int l=0,r=sum+1,mid,ans=0;
	while(l<r) {
		mid=(l+r)>>1;
		if(check1(mid)) {
			l=mid+1;
			if(mid>ans) ans=mid;
		} else {
			r=mid;
		}
	}
	printf("%d\n",ans);
}

bool check2(int x) {
	int ct=0;register int j;
	for(int i=n-1;i>=1;i--) {
		if(p[i]>=x) {
			ct++;
			if(ct>=m) return true;
			continue;
		}
		j=lower_bound(p+1,p+n,x-p[i])-p;
		while(j<i && vis[j]) j++;
		if(j>=i) return false;
		ct++;
		if(ct>=m) return true;
	}
	return ct>=m;
}

void solve2() {
	int l=0,r=sum+1,mid,ans=0;
	sort(p+1,p+n);
	while(l<r) {
		mid=(l+r)>>1;
		if(check2(mid)) {
			l=mid+1;
			if(mid>ans) ans=mid;
		} else {
			r=mid;
		}
	}
	printf("%d\n",ans);
}

void dfs(int u,int fa,int dis) {
	if(dis>mxs) {
		mxs=dis;
		NN=u;
	}
	for(int i=g[u];i!=-1;i=e[i].ne) {
		if(e[i].to==fa) continue;
		dfs(e[i].to,u,dis+e[i].Dist);
	}
}

void solve3() {
	mxs=0;
	dfs(1,0,0);
	mxs=0;
	dfs(NN,0,0);
	printf("%d\n",mxs);
}

int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);
	int x,y,z;bool fl=true;cnt=0;
	bool ff=true;
	sum=0;
	memset(g,-1,sizeof g);
	memset(p,0,sizeof p);
	for(int i=1;i<n;i++) {
		read(x);read(y);read(z);
		p[i]=z;
		sum+=z;
		e[cnt]=(Edge){y,z,g[x]};
		g[x]=cnt++;
		e[cnt]=(Edge){x,z,g[y]};
		g[y]=cnt++;
		if(y!=x+1) fl=false;
		if(x!=1) ff=false;
	}
	if(fl) {
		solve1();
		return 0;
	}
	if(m==1) {
		solve3();
		return 0;
	}
	if(ff) {
		solve2();
		return 0;
	}
	printf("%d\n",sum/m-1);
	return 0;
}
