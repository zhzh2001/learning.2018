#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 110
using namespace std;

int n,ans,a[N];
bool f[25010];

int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T_T;
	scanf("%d",&T_T);
	while(T_T--) {
		scanf("%d",&n);
		for(int i=1;i<=n;i++) {
			scanf("%d",&a[i]);
		}
		sort(a+1,a+n+1);
		memset(f,0,sizeof f);
		if(a[1]==1) {
			printf("1\n");
			continue;
		}
		ans=0;
		for(int k=1;k<=n;k++) if(!f[a[k]]) {
			ans++;
			f[a[k]]=1;
			for(int i=a[k]+1;i<=a[n];i++) {
				f[i]|=f[i-a[k]];
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
