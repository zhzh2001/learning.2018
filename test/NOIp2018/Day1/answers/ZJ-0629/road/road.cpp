//by Judge
#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int M=1e5+5;
#define getchar() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<21,stdin),p1==p2)?EOF:*p1++)
char buf[1<<21],*p1,*p2;
inline ll read(){ ll x=0;
	char c=getchar(); while(!isdigit(c)) c=getchar();
	for(;isdigit(c);c=getchar()) x=x*10+c-'0'; return x;
} ll n,top,x,stk[M],ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;++i){
		x=read();
		if(top&&x<stk[top])
			ans+=stk[top]-x;
		while(top&&x>=stk[top])
			--top;
		stk[++top]=x;
	}
	ans+=stk[top];
	printf("%lld\n",ans);
	return 0;
}
