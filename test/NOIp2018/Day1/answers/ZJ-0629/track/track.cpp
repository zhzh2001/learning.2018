//by Judge
#include<bits/stdc++.h>
using namespace std;
const int M=1e5+5;
#define getchar() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<21,stdin),p1==p2)?EOF:*p1++)
char buf[1<<21],*p1,*p2;
inline int read(){ int x=0,f=1; char c=getchar();
	for(;!isdigit(c);c=getchar()) if(c=='-') f=-1;
	for(;isdigit(c);c=getchar()) x=x*10+c-'0'; return x*f;
} int n,m,num,pat,head[M],du[M],to[M],val[M],d[M],in[M],lef[M];
int L=1,R,mid; vector<int> vec[M];
struct Edge{ int to,val,next;
	Edge(int v,int c,int x):to(v),val(c),next(x){} Edge(){}
}e[M<<1];
inline void add(int u,int v,int c){
	++du[u],++du[v];
	e[++pat]=Edge(v,c,head[u]),head[u]=pat;
	e[++pat]=Edge(u,c,head[v]),head[v]=pat;
}
inline void cmax(int& a,int b){ if(a<b) a=b; }
#define v e[i].to
queue<int> q;
inline void topo(){
	for(int i=1;i<=n;++i)
		d[i]=du[i];
	for(int i=1;i<=n;++i)
		if(d[i]==1) q.push(i);
	while(!q.empty()){
		int u=q.front(); in[u]=1,q.pop();
		for(int i=head[u];i;i=e[i].next){
			if(in[v]) continue;
			to[u]=v,val[u]=e[i].val;
			if(--d[v]==1) q.push(v);
		}
	}
}
#undef v
inline bool check(){
	num=0;
	for(int i=1;i<=n;++i){
		lef[i]=in[i]=0,
		vec[i].clear(),d[i]=du[i];
	}
	for(int i=1;i<=n;++i)
		if(d[i]==1) q.push(i);
	int st,ed,top,MX;
	while(!q.empty()){
		int u=q.front(); in[u]=1,q.pop();
		if(vec[u].size()){
			st=0,ed=vec[u].size()-1,top=0,MX=0;
			sort(vec[u].begin(),vec[u].end());
			while(ed>=0&&vec[u][ed]>=mid)
				++num,--ed;
			for(;st<ed;++st,--ed,++num){
				while(st<ed&&vec[u][st]+vec[u][ed]<mid){
					if(top) --top,++num;
					else lef[u]=vec[u][st];
					++st;
				}
				while(st<ed-1&&vec[u][st]+vec[u][ed-1]>=mid)
					cmax(MX,vec[u][ed]),++top,--ed;
				if(st==ed) break;
			}
			if(st==ed&&top)
				--top,++st,++num;
			if(top){
				num+=top/2;
				if(top&1^1) MX=0;
			}
			if(st==ed) cmax(lef[u],vec[u][ed]);
			cmax(lef[u],MX);
		}
		vec[to[u]].push_back(lef[u]+val[u]);
		if(--d[to[u]]==1) q.push(to[u]);
	}
	return num>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(int i=1,a,b,c;i<n;++i){
		a=read(),b=read(),c=read();
		add(a,b,c),R+=c;
	}
	topo();
	while(L<=R){
		mid=L+R>>1;
		if(check()) L=mid+1;
		else R=mid-1;
	}
	printf("%d\n",R);
	return 0;
}
