//by Judge
#include<bits/stdc++.h>
using namespace std;
const int M=1e5+5;
#define getchar() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<21,stdin),p1==p2)?EOF:*p1++)
char buf[1<<21],*p1,*p2;
inline int read(){ int x=0,f=1; char c=getchar();
	for(;!isdigit(c);c=getchar()) if(c=='-') f=-1;
	for(;isdigit(c);c=getchar()) x=x*10+c-'0'; return x*f;
} int T,n,mx,ans,a[205],f[M];
inline void cmax(int& a,int b){
	if(a<b) a=b;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		n=read(),mx=0,ans=n;
		for(int i=1;i<=n;++i){
			a[i]=read();
			cmax(mx,a[i]);
		}
		for(int i=1;i<=mx;++i)
			f[i]=0;
		sort(a+1,a+1+n),f[0]=1;
		for(int i=1;i<=n;++i){
			if(f[a[i]]){
				--ans; continue;
			}
			for(int j=a[i];j<=mx;++j)
				f[j]|=f[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
