#include<bits/stdc++.h>
#define For(i,a,b) for(int i=(a),i##_=(b);i<=i##_;++i)
#define Ror(i,a,b) for(int i=(a),i##_=(b);i>=i##_;--i)
#define Dor(i,x) for(int i=head[x];i;i=nxt[i])
#define LL long long
#define pb push_back
#define M 50005
using namespace std;
bool cur1;
int n,m;
int mk[M],tim;
int head[M],nxt[M<<1],to[M<<1],v[M<<1],tot;
struct node{
	int cnt,len;
}dp[M];
vector<int>H[M];
template<class T>inline void Min(T &x,T y){
	if(y<x)x=y; 
} 
template<class T>inline void Max(T &x,T y){
	if(y>x)x=y;
}
template<class T>inline void read(T &res){
	res=0;char p;
	while(p=getchar(),p<48);
	do res=(res<<1)+(res<<3)+(p^48);
	while(p=getchar(),p>47);
}
inline void Add(int x,int y,int z){
	nxt[++tot]=head[x];
	head[x]=tot;
	to[tot]=y,v[tot]=z;
}
void dfs(int x,int f,int mx){
	dp[x].cnt=dp[x].len=0;H[x].clear();
	Dor(i,x){
		int y=to[i];
		if(y!=f){
			dfs(y,x,mx);
			H[x].pb((dp[y].len+v[i])%mx);	
			dp[x].cnt+=dp[y].cnt+(dp[y].len+v[i])/mx;
		}
	}
	tim++;
	sort(H[x].begin(),H[x].end());
	int sz=H[x].size(),las=sz-1;
	For(i,0,sz-1)if(mk[i]!=tim){
		int pos=-1;
		For(j,i+1,sz-1)if(mk[j]!=tim&&H[x][j]+H[x][i]>=mx){pos=j;break;}
		if(~pos)dp[x].cnt++,mk[i]=mk[pos]=tim;
	}
	Ror(i,sz-1,0)if(mk[i]!=tim){
		dp[x].len=H[x][i];
		break;
	}
}
bool Chk(int mid){
	dfs(1,0,mid);
	return dp[1].cnt>=m;
}
bool cur2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
//	freopen("track.in","r",stdin);
//	freopen("check.out","w",stdout);
	read(n),read(m);
	int x,y,z,sum=0,mi=1e9;
	For(i,1,n-1){
		read(x),read(y),read(z);
		Add(x,y,z),Add(y,x,z);
		sum+=z;
		Min(mi,z);
	}
	int L=mi,R=sum/m,res=mi;
	if(!L)L++;
	while(L<=R){
		int mid=(L+R)>>1;
		if(Chk(mid))L=mid+1,res=mid;
		else R=mid-1;
	}
	printf("%d\n",res);
	return 0;
}
