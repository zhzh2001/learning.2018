#include<bits/stdc++.h>
#define For(i,a,b) for(int i=(a),i##_=(b);i<=i##_;++i)
#define Ror(i,a,b) for(int i=(a),i##_=(b);i>=i##_;--i)
#define M 100005
using namespace std;
bool cur1;
int n,L[M],R[M];
struct node{
	int x,id;
	bool operator<(const node &a)const{
		if(x!=a.x)return x<a.x;
		return id<a.id; 
	}
}A[M];
template<class T>inline void Min(T &x,T y){
	if(y<x)x=y; 
} 
template<class T>inline void Max(T &x,T y){
	if(y>x)x=y;
}
template<class T>inline void read(T &res){
	res=0;char p;
	while(p=getchar(),p<48);
	do res=(res<<1)+(res<<3)+(p^48);
	while(p=getchar(),p>47);
}
struct SegmentTree{
	#define ls p<<1
	#define rs p<<1|1
	struct node{
		int L,R,x;
	}tree[M<<2];
	void Build(int L,int R,int p){
		tree[p].L=L,tree[p].R=R,tree[p].x=0;
		if(L==R){
			tree[p].x=A[L].x;
			return;
		}
		int mid=(L+R)>>1;
		Build(L,mid,ls),Build(mid+1,R,rs);
	}
	void Update(int L,int R,int a,int p){
		if(tree[p].L==L&&tree[p].R==R){
			tree[p].x+=a;
			return;
		}
		int mid=(tree[p].L+tree[p].R)>>1;
		if(R<=mid)Update(L,R,a,ls);
		else if(L>mid)Update(L,R,a,rs);
		else Update(L,mid,a,ls),Update(mid+1,R,a,rs);
	}
	int Query(int x,int p){
		if(tree[p].L==tree[p].R)return tree[p].x;
		int mid=(tree[p].L+tree[p].R)>>1;
		if(x<=mid)return tree[p].x+Query(x,ls);
		return tree[p].x+Query(x,rs);
	}
}T;
bool cur2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	For(i,1,n){
		read(A[i].x),A[i].id=i; 
		int las=i-1;
		while(las&&A[las].x>A[i].x)las=L[las]-1; 
		L[i]=las+1;
	}
	Ror(i,n,1){
		int las=i+1;
		while(las<=n&&A[las].x>=A[i].x)las=R[las]+1;
		R[i]=las-1; 
	}
	T.Build(1,n,1);
	sort(A+1,A+n+1);
	int ans=0;
	For(i,1,n){
		int pos=A[i].id;
		int tmp=T.Query(pos,1);
		ans+=tmp;
		T.Update(L[pos],R[pos],-tmp,1);
	} 
	printf("%d\n",ans);
	return 0;
}
