#include<bits/stdc++.h>
#define For(i,a,b) for(int i=(a),i##_=(b);i<=i##_;++i)
#define Ror(i,a,b) for(int i=(a),i##_=(b);i>=i##_;--i)
#define LL long long
#define M 105
#define N 25005
using namespace std;
bool cur1;
int A[M];
bool dp[N];
template<class T>inline void Min(T &x,T y){
	if(y<x)x=y; 
}
template<class T>inline void Max(T &x,T y){
	if(y>x)x=y;
}
template<class T>inline void read(T &res){
	res=0;char p;
	while(p=getchar(),p<48);
	do res=(res<<1)+(res<<3)+(p^48);
	while(p=getchar(),p>47);
}
void Merge(int x,int mx){
	For(i,x,mx)dp[i]|=dp[i-x];
}
bool cur2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t,n;
	read(t);
	while(t--){
		read(n);
		For(i,1,n)read(A[i]);
		sort(A+1,A+1+n);
		int mx=A[n],ans=0;
		For(i,1,mx)dp[i]=0;
		dp[0]=1;
		For(i,1,n)if(!dp[A[i]]){
			ans++;
			Merge(A[i],mx);
		}
		printf("%d\n",ans);
	}
	return 0;
}
