#include <bits/stdc++.h>
#define ll long long
#define lop(i, x, y) for (register int i = (x), _E = (y); i <= _E; ++i)
#define pol(i, x, y) for (register int i = (x), _E = (y); i >= _E; --i)
using namespace std;

inline int read () {
	int x = 0, f = 1; char ch = getchar();
	for ( ; ch < '0' || ch > '9'; ch = getchar())
		if (ch == '-') f = -1;
	for ( ; ch >= '0' && ch <= '9'; ch = getchar())
		x = x * 10 + ch - 48;
	return x * f;
}

const int N = 5e4 + 5, L = 16;
int n, m, tot, T;
int lnk[N], nxt[N << 1], son[N << 1], w[N << 1];
int dfn[N], dep[N], fa[N][L + 2], we[N][L + 2];
void add (int x, int y, int z) {
	nxt[++tot] = lnk[x], lnk[x] = tot, son[tot] = y, w[tot] = z;
}
void dfs (int x, int p) {
	dfn[x] = ++T, fa[x][0] = p, dep[x] = dep[p] + 1;
	for (int j = lnk[x]; j; j = nxt[j]) if (son[j] ^ p)
		dfs(son[j], x), we[son[j]][0] = w[j];
}
int lca (int x, int y) {
	if (dep[x] < dep[y]) swap(x, y);
	int dif = dep[x] - dep[y];
	pol (j, L, 0) if (dif >> j & 1) x = fa[x][j];
	if (x == y) return x;
	pol (j, L, 0) if (fa[x][j] != fa[y][j])
		x = fa[x][j], y = fa[y][j];
	return fa[x][0];
}
int jump (int x, int v) {
	if (v <= 0) return x;
	pol (j, L, 0) if (fa[x][j] && we[x][j] < v)
		x = fa[x][j], v -= we[x][j];
	return fa[x][0];
}
int getW (int x, int y) {
	if (dep[x] < dep[y]) swap(x, y);
	int dif = dep[x] - dep[y], ret = 0;
	pol (j, L, 0) if (dif >> j & 1)
		ret += we[x][j], x = fa[x][j];
	return ret;
}

typedef pair <int, int> pii;
int LE, cnt, mx, wup[N];
set <pii> s;
set <pii> :: iterator it;
set <pii> :: iterator ot;
pii tmp, tmp0;

void going (int x) {
	wup[x] = 0;
	for (int j = lnk[x]; j; j = nxt[j]) {
		if (son[j] == fa[x][0]) continue;
		going(son[j]), wup[son[j]] += w[j];
	}
	cnt = mx = 0;
	for (int j = lnk[x]; j; j = nxt[j]) {
		if (son[j] == fa[x][0]) continue;
		s.insert(pii(wup[son[j]], son[j]));
	}
	for ( ; !s.empty(); ) {
		it = s.begin(), tmp = *it;
		if (tmp.first >= LE) {++tot, s.erase(it); continue;}
		if (s.lower_bound(pii(LE - tmp.first, 0)) != s.end()) {
			tmp0 = *(lower_bound(s.begin(), s.end(), pii(LE - tmp.first, 0)));
			if (tmp != tmp0 && tmp0.first >= LE - tmp.first) {
				++cnt, s.erase(tmp), s.erase(tmp0);
			} else mx = tmp.first, s.erase(tmp);
		} else mx = tmp.first, s.erase(tmp);
	}
	wup[x] = mx;
}
bool exam (int len) {
	LE = len, cnt = 0, s.clear();
	going(1);
	return cnt >= m;
}
void prework () {
	dfs(1, 0);
	lop (j, 1, L)
	lop (i, 1, n) {
		fa[i][j] = fa[fa[i][j - 1]][j - 1];
		we[i][j] = we[i][j - 1] + we[fa[i][j - 1]][j - 1];
	}
}
int solve () {
	int l = 1, r = 5e8, mid, ret = 0;
	for ( ; l <= r; ) {
		mid = l + r >> 1;
		if (exam(mid)) {
			ret = mid, l = mid + 1;
		} else r = mid - 1;
	}
	return ret;
}
int main () {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	n = read(), m = read();
	lop (i, 1, n - 1) {
		int x = read(), y = read(), z = read();
		add(x, y, z), add(y, x, z);
	}
	prework();
	cout << solve() << endl;
	return 0;
}
