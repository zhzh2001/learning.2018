#include <bits/stdc++.h>
#define ll long long
#define lop(i, x, y) for (register int i = (x), _E = (y); i <= _E; ++i)
#define pol(i, x, y) for (register int i = (x), _E = (y); i >= _E; --i)
using namespace std;

inline int read () {
	int x = 0, f = 1; char ch = getchar();
	for ( ; ch < '0' || ch > '9'; ch = getchar())
		if (ch == '-') f = -1;
	for ( ; ch >= '0' && ch <= '9'; ch = getchar())
		x = x * 10 + ch - 48;
	return x * f;
}

const int N = 105, M = 25000;
int T, n, a[N]; bool f[M + 5];

int solve () {
	int ret = 0;
	lop (j, 0, M) f[j] = 0;
	f[0] = 1;
	lop (i, 1, n) {
		if (f[a[i]]) ++ret;
		lop (j, 0, M) if (j >= a[i])
			f[j] = f[j] || f[j - a[i]];
	}
	return n - ret;
}
int main () {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	T = read();
	lop (qwq, 1, T) {
		n = read();
		lop (i, 1, n) a[i] = read();
		sort(a + 1, a + 1 + n);
		printf("%d\n", solve());
	}
	return 0;
}
