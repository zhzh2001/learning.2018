#include <bits/stdc++.h>
#define ll long long
#define lop(i, x, y) for (register int i = (x), _E = (y); i <= _E; ++i)
#define pol(i, x, y) for (register int i = (x), _E = (y); i >= _E; --i)
using namespace std;

inline int read () {
	int x = 0, f = 1; char ch = getchar();
	for ( ; ch < '0' || ch > '9'; ch = getchar())
		if (ch == '-') f = -1;
	for ( ; ch >= '0' && ch <= '9'; ch = getchar())
		x = x * 10 + ch - 48;
	return x * f;
}

const int N = 1e5 + 5;
int n, ans, top, st[N];

void ins (int x) {
	for ( ; top; --top) {
		if (st[top] <= x) break;
		ans += st[top] - max(st[top - 1], x);
	}
	st[++top] = x;
}

int x;
int main () {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	n = read();
	lop (i, 1, n) x = read(), ins(x);
	ins(0);
	cout << ans << endl;
	return 0;
}
