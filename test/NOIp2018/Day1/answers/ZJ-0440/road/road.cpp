#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
typedef long long ll;
const int N=500005;
ll a[N],dp[N];
int n;
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n); 
	for (int i=1; i<=n; i++) {
		scanf("%lld",&a[i]);
		if (a[i]>=a[i-1]) dp[i]=dp[i-1]+a[i]-a[i-1];
		else dp[i]=dp[i-1];
	}
	printf("%lld\n",dp[n]);
	return 0;
}

