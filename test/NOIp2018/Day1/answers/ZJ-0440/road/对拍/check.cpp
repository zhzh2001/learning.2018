#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;
typedef long long ll;
const int N=500005;
ll a[N];
int n;
int main() {
	freopen("road.in","r",stdin); freopen("check.out","w",stdout);
	scanf("%d",&n);
	for (int i=1; i<=n; i++) scanf("%lld",&a[i]);
	long long tot=0;
	while (true) {
		bool flag=false;
		for (int i=1; i<=n; i++) if (a[i]!=0) flag=true;
		if (!flag) break;
		tot++;
		int tmp=0; 
		for (int i=1; i<=n; i++) 
			if (a[i]!=0) { tmp=i; break; }
		int tmp2=0;
		for (int i=tmp; i<=n; i++) {
			if (a[i]!=0)  tmp2=i;
			else break;
		}
		for (int i=tmp; i<=tmp2; i++) a[i]--;
	}
	printf("%lld\n",tot);
	return 0;
}
		
