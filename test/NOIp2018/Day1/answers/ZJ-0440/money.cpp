#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;
const int N=500005;
int n,Max,a[N],dp[N];
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--) {
		scanf("%d",&n);
		Max=-1;
		for (int i=1; i<=n; i++) {
			scanf("%d",&a[i]);
			Max=max(Max,a[i]);
		}
		sort(a+1,a+n+1);
		int tot=0;
		for (int j=0; j<=Max+1; j++) dp[j]=false;
		dp[0]=true;
		for (int i=1; i<=n; i++)
			if (!dp[a[i]]) {
				tot++;
				for (int j=a[i]; j<=Max; j++) dp[j]|=dp[j-a[i]];
			}
		printf("%d\n",tot);
	}
	return 0;
}
