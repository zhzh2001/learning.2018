#include <cstdio>
#include <iostream>
using namespace std;
const int Inf=2e9;
const int N=500005;
bool yod[N],aux[N];
int ans,n,a[N],sta[N],tot,Max;
void dfs(int x) {
	if (x==n+1) {
		for (int i=0; i<=10000; i++) yod[i]=false;
		yod[0]=true;
		for (int i=1; i<=tot; i++) {
			int x=sta[i];
			for (int j=x; j<=10000; j++)
				yod[j]|=yod[j-x];
		}
		for (int j=1; j<=10000; j++) if (yod[j]!=aux[j]) return;
		ans=min(ans,tot);
		return;
	}
	sta[++tot]=a[x];
	dfs(x+1);
	tot--;
	dfs(x+1);
}
int main() {
	freopen("money.in","r",stdin);
	freopen("check.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--) {
		scanf("%d",&n); Max=-1;
		for (int i=1; i<=n; i++) {
			scanf("%d",&a[i]);
			Max=max(Max,a[i]);
		} ans=Inf;
		for (int i=0; i<=10000; i++) aux[i]=false;
		aux[0]=true;
		for (int i=1; i<=n; i++)
			for (int j=a[i]; j<=10000; j++)
				aux[j]|=aux[j-a[i]];
		dfs(1);
		printf("%d\n",ans);
	}
	return 0;
}
