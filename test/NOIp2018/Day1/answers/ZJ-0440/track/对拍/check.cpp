#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;
const int N=300005;
const int Inf=2e9;
int g[N],f[N],sta[N],ok[N],head[N],vet[N],nxt[N],edgenum,val[N],t,n,m,a[N];

bool check(int mid) {
	int num=0,tot=0;
	for (int i=1; i<n; i++) {
		num+=a[i]; 
		if (num>=mid) tot++,num=0;
	}
//	printf("
	return tot>=m;
}

int main() {
	freopen("track.in","r",stdin);
	freopen("check.out","w",stdout);
	scanf("%d%d",&n,&m);
	int Max=0;
	for (int i=1; i<n; i++) {
		int u,v,val; 
		scanf("%d%d%d",&u,&v,&val);
		a[i]=val; 
		Max+=val;
	}
	int l=1,r=Max,res=-1; 
	while (l<=r) {
		int mid=l+(r-l)/2;
		if (check(mid)) {
			l=mid+1; res=mid;
		} else r=mid-1;
	}
	printf("%d\n",res);
	return 0;
}
/*
7 4
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7
*/
		
