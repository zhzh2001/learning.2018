#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;
const int N=300005;
const int Inf=2e9;
int g[N],f[N],sta[N],ok[N],head[N],vet[N],nxt[N],edgenum,val[N],t,n,m;
void Add(int u,int v,int valu) {
	vet[++edgenum]=v; nxt[edgenum]=head[u]; head[u]=edgenum; val[edgenum]=valu;
}
void dfs(int x,int fa) {
//	printf("!!!!!!!!%d\n",x);
	int num=0;
	for (int e=head[x]; e!=0; e=nxt[e]) {
		int v=vet[e]; if (v==fa) continue;
		dfs(v,x); 
	}
	for (int e=head[x]; e!=0; e=nxt[e]) {
		int v=vet[e]; if (v==fa) continue;
		f[x]+=f[v]; if (g[v]+val[e]>=t) f[x]++; else sta[++num]=g[v]+val[e];
	}
	sort(sta+1,sta+num+1);
	int l=1,r=num;
	for (int i=1; i<=num; i++) ok[i]=false;
//	printf("%d ***",x);for (int i=1; i<=num; i++) printf("%d ",sta[i]); printf("\n");
	while (l<r) {
		if (sta[l]+sta[r]>=t) {
			ok[l]=ok[r]=true; //printf("Oh Yes %d %d\n",sta[l],sta[r]);
			l++,r--,f[x]++;
		} else l++;
	}
	g[x]=0;
	for (int i=1; i<=num; i++) if (!ok[i]) g[x]=max(g[x],sta[i]);
}

bool check(int mid) {
	t=mid;
	for (int i=1; i<=n; i++) f[i]=0,g[i]=0,ok[i]=false;
	dfs(1,0);
//	printf("!! %d\n",t);
//	for (int i=1; i<=n; i++) printf("%d ",f[i]); printf("\n");
//	for (int i=1; i<=n; i++) printf("%d ",g[i]); printf("\n");
	if (f[1]>=m) return true;
	return false;
}

int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int Max=0;
	for (int i=1; i<n; i++) {
		int u,v,val; 
		scanf("%d%d%d",&u,&v,&val);
		Add(u,v,val),Add(v,u,val);
		Max+=val;
	}//	printf("%d %d %d %d\n",check(26175),check(26176),check(26199),check(1000000));
	int l=1,r=Max,res=-1; 
//	printf("%d\n",r);
	while (l<=r) {
		int mid=l+(r-l)/2;
		if (check(mid)) {
			l=mid+1; res=mid;
		} else r=mid-1;
	}
	printf("%d\n",res);
	return 0;
}
/*
10 1
1 2 10
1 3 9
1 4 8
1 5 7
2 6 6
2 7 5
4 8 4
4 9 3
5 10 2

9 1
1 2 1
2 4 1
2 5 1
1 3 1
3 6 1
3 7 1
4 9 1
7 8 1
*/
		
