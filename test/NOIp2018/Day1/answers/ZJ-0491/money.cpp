#include<bits/stdc++.h>
using namespace std;
int a[202],n,ans,maxn,T,N,an[202];
bool f[30000];
bool flag;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		maxn=0;
		flag=false;
		scanf("%d",&n);
		for(int i=1;i<=n;++i){
			scanf("%d",&a[i]);
			maxn=max(maxn,a[i]);
			if(a[i]==1)flag=true;
			
		}
		if(flag){
			printf("1\n");
			continue;
		}
		sort(a+1,a+1+n);
		N=0;
		maxn=0;
		
		for(int i=1;i<=n;++i){
			bool tn=false;
			for(int j=1;j<=i-1;++j){
				if(a[i]%a[j]==0)tn=true;
			}
			if(!tn){
				++N;
				an[N]=a[i];
				maxn=max(maxn,an[N]);
			}
		}
		ans=N;
		for(int i=1;i<=maxn;++i){
			f[i]=false;
		}
		f[0]=true;
		for(int i=1;i<=N;++i){
			if(f[an[i]]==true)ans-=1;
			else{
				f[an[i]]=true;
				for(int j=0;j<=maxn;++j){
					if(!f[j])continue;
					int k=j+an[i];
					while(k<=maxn){
						f[k]=true;
						k+=an[i];
					}
				}
			}
			
		}
		printf("%d\n",ans);
	}
	return 0;
}
