#include<bits/stdc++.h>
using namespace std;
int a,b,c,n,m,head[100100],cnt,nxt[100100],to[100100],maxn[50050],len[50050],ans;
void addedge(int u,int v,int w){
	++cnt;
	nxt[cnt]=head[u];
	head[u]=cnt;
	to[cnt]=v;
	len[cnt]=w;
}

void dfs(int x,int from){
	int m1=0,m2=0;
	for(int i=head[x];i;i=nxt[i]){
		int y=to[i];
		if(y==from)continue;
		dfs(y,x);
		m2=max(m2,maxn[y]+len[i]);
		if(m1<m2){
			int t0=m1;
			m1=m2;m2=t0;
		}
	}
	maxn[x]=m1;
	ans=max(ans,m1+m2);
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int minn=10001;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n-1;++i){
		scanf("%d%d%d",&a,&b,&c);
		addedge(a,b,c);
		addedge(b,a,c);
		minn=min(c,minn);
	}
	if(m==n-1){
		printf("%d",minn);
		return 0;
	}
	dfs(1,0);
	printf("%d",ans);
	return 0;
}
