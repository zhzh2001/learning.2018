# include <bits/stdc++.h>
using namespace std;
const int MAXN=1e5+10;
int a[MAXN],n,ans;
int getL()
{
	for (int i=1;i<=n;i++)
	 if (a[i]>0) return i;
	return -1; 
}
int getR(int pos)
{
	for (int i=pos+1;i<=n;i++)
	 if (a[i]==0) return i-1;
	return -1; 
}
int update(int L,int R)
{
	int Min=0x7f7f7f7f;
	for (int i=L;i<=R;i++)
	 Min=min(Min,a[i]);
	ans+=Min; 
	for (int i=L;i<=R;i++)
	 a[i]-=Min; 
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) 
	 scanf("%d",&a[i]);
	n++; a[n]=0;
	while (true) {
		int L=getL();
		if (L==-1) break;
		int R=getR(L);
		update(L,R);
	}
	printf("%d\n",ans);
	return 0;
}