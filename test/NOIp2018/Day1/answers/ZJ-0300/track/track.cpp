# include <bits/stdc++.h>
# define int long long
using namespace std;
const int MAXN=5e4+10;
int head[MAXN],tot=0,n,m,d[MAXN];
int du[MAXN];
struct rec{ int pre,to,w;}a[MAXN*2];
void adde(int u,int v,int w)
{
	a[++tot].pre=head[u];
	a[tot].to=v;
	a[tot].w=w;
	head[u]=tot;
}
void dfs1(int u,int fa)
{
	for (int i=head[u];i;i=a[i].pre){
		int v=a[i].to; if (v==fa) continue;
		d[v]=d[u]+a[i].w;
		dfs1(v,u);
	}
}
void work1()
{
	memset(d,0,sizeof(d));
	dfs1(1,-1);
	int pos=1;;
	for (int i=2;i<=n;i++)
	 if (d[i]>d[pos]) pos=i;
	memset(d,0,sizeof(d));
	dfs1(pos,-1);
	pos=1;
	for (int i=2;i<=n;i++)
	 if (d[i]>d[pos]) pos=i;
	printf("%lld\n",d[pos]); 
	exit(0);
}
int tt,tmp[MAXN];
bool cmp(int x,int y){return x>y;}
void work2()
{
	int tt=0;
 	for (int i=head[1];i;i=a[i].pre) {
		 tmp[++tt]=a[i].w; 
	 }
 	sort(tmp+1,tmp+1+tt,cmp);
	printf("%lld\n",tmp[m]);
	exit(0); 
}
void dfs2(int u,int fa)
{
	for (int i=head[u];i;i=a[i].pre){
		int v=a[i].to; if (v==fa) continue;
		d[++d[0]]=a[i].w;
		dfs2(v,u);
	}
}
bool check(int MID)
{
	int cnt=0,sum=0;
	for (int i=1;i<=n-1;i++) {
		sum=sum+d[i];
		if (sum>=MID){ sum=0;cnt++;}
	}
	if (cnt>=m) return true;
	else return false;
}
void work3()
{
	int pos=0;
	for (int i=1;i<=n;i++)
	 if (du[i]==1) { pos=i; break; }
	memset(d,0,sizeof(d));
	dfs2(pos,-1);
	int l=1,r=0,ans;
	for (int i=1;i<=n-1;i++) r+=d[i];
	while (l<=r) {
		int mid=(l+r)/2;
		if (check(mid)) l=mid+1,ans=mid;
		else r=mid-1;
	} 
	printf("%lld\n",ans);
}
signed main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	int u,v,w;
	bool flag1=true;
	bool lian1=true;
	for (int i=1;i<n;i++){
		scanf("%lld%lld%lld",&u,&v,&w);
		adde(u,v,w); adde(v,u,w);
		du[u]++; du[v]++;
		if (u!=1) flag1=false;
		if (v!=u+1) lian1=false;
	}
	if (m==1) work1();
	if (flag1) work2();
	if (lian1) work3();
	return 0;
}
