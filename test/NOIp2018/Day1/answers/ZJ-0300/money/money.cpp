# include <bits/stdc++.h>
using namespace std;
const int MAXN=105;
int n,a[MAXN],cnt,t[MAXN],base[MAXN],ans;
bool flag;
void get(int x,int now)
{
	if (now>x) return;
	if (now==x) { flag=true; return;}
	for (int i=1;i<=cnt;i++) 
	 get(x,now+base[i]);
}
bool pick(int x)
{
	flag=false; get(x,0);
	return flag;
}
void check()
{
	cnt=0;
	memset(base,0,sizeof(base));
	for (int i=1;i<=n;i++)
	 if (t[i]==1) base[++cnt]=a[i];
	if (cnt==n||cnt==0) return; 
	for (int i=1;i<=n;i++)
	 if (t[i]==0&&!pick(a[i])) return;
	ans=min(ans,cnt);  
}
void dfs1(int dep)
{
	if (dep==n+1) {
		check(); return;
	}
	t[dep]=1; dfs1(dep+1);
	t[dep]=0; dfs1(dep+1);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--) {
		scanf("%d",&n); ans=n;
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		dfs1(1);
		printf("%d\n",ans);
	}
	return 0;
}
