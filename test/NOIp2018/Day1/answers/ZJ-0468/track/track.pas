var
    next,vet,dist:array[0..200000]of longint;
    head,dp,f,a,f1,f2:array[0..100000]of longint;
    n,m,i,mid,l,r,tot,ans,x,y,z:longint;
    flag:boolean;
procedure add(x,y,z:longint);
begin
    inc(tot);
    next[tot]:=head[x];
    vet[tot]:=y;
    head[x]:=tot;
    dist[tot]:=z;
end;
procedure dfs(u,father:longint);
var
    v,i,first,second,third:longint;
begin
    first:=0; second:=0; third:=0;
    i:=head[u];
    while i<>0 do
    begin
        v:=vet[i];
        if v<>father then
        begin
            dfs(v,u);
            dp[u]:=dp[u]+dp[v];
            f[v]:=f[v]+dist[i];
            if f[v]>first then
            begin
                third:=second; second:=first; first:=f[v];
            end else
                if f[v]>second then
                begin
                    third:=second; second:=f[v];
                end else
                    if f[v]>third then third:=f[v];
        end;
        i:=next[i];
    end;
    if second+third>=mid then
    begin
        inc(dp[u]); f[u]:=first;
    end else
        if first+third>=mid then
        begin
            inc(dp[u]); f[u]:=second;
        end else
            if first+second>=mid then
            begin
                inc(dp[u]); f[u]:=third;
            end else f[u]:=first;
end;
procedure sort(l,r:longint);
var
    i,j,x,y:longint;
begin
    i:=l; j:=r;
    x:=a[(l+r) div 2];
    repeat
        while a[i]>x do inc(i);
        while x>a[j] do dec(j);
        if not(i>j) then
        begin
            y:=a[i]; a[i]:=a[j]; a[j]:=y;
            inc(i); j:=j-1;
        end;
    until i>j;
    if l<j then sort(l,j);
    if i<r then sort(i,r);
end;
procedure ershi;
var
    i,j,v,sum:longint;
begin
    i:=head[1];
    tot:=0;
    while i<>0 do
    begin
        inc(tot);
        a[tot]:=dist[i];
        i:=next[i];
    end;
    sort(1,tot);
    j:=tot;
    l:=1; r:=500000000; ans:=0;
    while l<=r do
    begin
        mid:=(l+r)>>1;
        j:=tot; sum:=0;
        for i:=1 to tot do
        begin
            while (a[i]+a[j]<mid)and(i<j) do dec(j);
            if i>=j then break;
            inc(sum);
        end;
        if sum>=m then
        begin
            ans:=mid;
            l:=mid+1;
        end else r:=mid-1;
    end;
    writeln(ans);
    close(input);
    close(output);
    halt;
end;
procedure dffs(u,father:longint);
var
    i,v:longint;
begin
    f1[u]:=0; f2[u]:=0;
    i:=head[u];
    while i<>0 do
    begin
        v:=vet[i];
        if v<>father then
        begin
            dffs(v,u);
            f1[v]:=f1[v]+dist[i];
            f2[v]:=f2[v]+dist[i];
            if f1[v]>f1[u] then
            begin
                f2[u]:=f1[u]; f1[u]:=f1[v];
            end else
                if f1[v]>f2[u] then f2[u]:=f1[v];
            f1[v]:=f1[v]-dist[i];
            f2[v]:=f2[v]-dist[i];
        end;
        i:=next[i];
    end;
end;
procedure shi;
var
    i:longint;
begin
    dffs(1,0);
    for i:=1 to n do
        if f1[i]+f2[i]>ans then ans:=f1[i]+f2[i];
    writeln(ans);
    close(input);
    close(output);
    halt;
end;
begin
    assign(input,'track.in');reset(input);
    assign(output,'track.out');rewrite(output);
    read(n,m);
    flag:=true;
    for i:=1 to n-1 do
    begin
        read(x,y,z);
        add(x,y,z); add(y,x,z);
        if (x<>1)and(y<>1) then flag:=false;
    end;
    if m=1 then shi;
    if flag then ershi;
    l:=1; r:=500000000;
    while l<=r do
    begin
        mid:=(l+r)>>1;
        fillchar(f,sizeof(f),0);
        fillchar(dp,sizeof(dp),0);
        dfs(1,0);
        if dp[1]>=m then
        begin
            ans:=mid;
            l:=mid+1;
        end else r:=mid-1;
    end;
    writeln(ans);
    close(input);
    close(output);
end.
