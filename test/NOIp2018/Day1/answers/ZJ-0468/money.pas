var
    a:array[1..200]of longint;
    flag:array[0..30000]of boolean;
    t,n,ans,i,j:longint;
procedure sort(l,r:longint);
var
    i,j,x,y:longint;
begin
    i:=l; j:=r;
    x:=a[(l+r) div 2];
    repeat
        while a[i]<x do inc(i);
        while x<a[j] do dec(j);
        if not(i>j) then
        begin
            y:=a[i]; a[i]:=a[j]; a[j]:=y;
            inc(i); j:=j-1;
        end;
    until i>j;
    if l<j then sort(l,j);
    if i<r then sort(i,r);
end;
begin
    assign(input,'money.in');reset(input);
    assign(output,'money.out');rewrite(output);
    read(t);
    while t>0 do
    begin
        fillchar(flag,sizeof(flag),false);
        flag[0]:=true;
        read(n); ans:=n;
        for i:=1 to n do
            read(a[i]);
        sort(1,n);
        for i:=1 to n-1 do
        begin
            for j:=a[i] to 25000 do
                flag[j]:=flag[j] or flag[j-a[i]];
            if flag[a[i+1]] then dec(ans);
        end;
        writeln(ans);
        dec(t);
    end;
    close(input);
    close(output);
end.
