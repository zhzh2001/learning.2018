var
    now,x,n,i,ans:longint;
begin
    assign(input,'road.in');reset(input);
    assign(output,'road.out');rewrite(output);
    read(n);
    now:=0;
    for i:=1 to n do
    begin
        read(x);
        if now>=x then now:=x else
        begin
            ans:=ans+x-now;
            now:=x;
        end;
    end;
    writeln(ans);
    close(input);
    close(output);
end.
