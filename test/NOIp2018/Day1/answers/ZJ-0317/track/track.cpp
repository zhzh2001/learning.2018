#include<iostream>
#include<algorithm>
#include<string.h>
#include<set>
#include<bits/stdc++.h>
using namespace std;
inline long long read(){
	long long x=0;char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-48,ch=getchar();
	return x;
}
inline void write(long long x){
	if(x>9)write(x/10);
	putchar(x%10+48);
}
inline void writeln(long long x){
	write(x),puts("");
}
long long n,m,u,v,w;
int to[110003],nxt[110003],head[51003],tot;
long long val[110003];
inline void add(long long a,long long b,long long c){
	to[++tot]=b,val[tot]=c,nxt[tot]=head[a],head[a]=tot;
}
long long sum,pdd,cnt;
long long wtf[50003];
multiset<long long> st[50003];
multiset<long long>::iterator it1,it2;
void dfs(int x,int fa){
	st[x].clear();
	for(register int i=head[x];i;i=nxt[i]){
		if(to[i]^fa){
			dfs(to[i],x);
			st[x].insert(wtf[to[i]]+val[i]);
		}
	}
	while(!st[x].empty()){
		it1=st[x].end();
		--it1;
		if((*it1)>=pdd) st[x].erase(it1),++cnt;
		else break;
	}
	while(!st[x].empty()){
		it1=st[x].begin();
		it2=st[x].lower_bound(pdd-(*it1));
		if((*it1)==(*it2)) ++it2;
		if(it2!=st[x].end()) st[x].erase(it1),st[x].erase(it2),++cnt;
		else wtf[x]=(*it1),st[x].erase(it1);
	}
}
inline bool pd(long long x){
	for(register int i=1;i<=n;++i)wtf[i]=0;
	cnt=0;
	pdd=x;
	dfs(1,0);
	return cnt>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(register int i=1;i<n;++i){
		u=read(),v=read(),w=read();
		sum+=w;
		add(u,v,w),add(v,u,w);
	}
	register long long l=0,r=sum/m,mid;
	while(r*m<sum) ++r;
	while(l<r-1){
		mid=(l+r)>>1;
		if(pd(mid)) l=mid;
		else r=mid-1;
	}
	if(pd(r+1)) writeln(r+1);
	else if(pd(r)) writeln(r);
	else writeln(l);
	return 0;
}
/*

in:
7 1
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7

out:
31

in:
9 3
1 2 6
2 3 3
3 4 5
4 5 10
6 2 4
7 2 9
8 4 7
9 4 4

out:
15

*/
