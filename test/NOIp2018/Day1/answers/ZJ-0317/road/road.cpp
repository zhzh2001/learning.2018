#include<iostream>
#include<algorithm>
#include<stdio.h>
#include<bits/stdc++.h>
using namespace std;
inline long long read(){
	long long x=0;char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-48,ch=getchar();
	return x;
}
inline void write(long long x){
	if(x>9)write(x/10);
	putchar(x%10+48);
}
inline void writeln(long long x){
	write(x),puts("");
}
long long n,d[200003],num[200003],x,y;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	x=0,y=0;
	for(register int i=1;i<=n;++i){
		d[i]=read();
	}
	for(register int i=1;i<=n;++i){
		num[i]=d[i]-d[i-1];
		if(num[i]>0){
			x+=num[i];
		}else{
			y-=num[i];
		}
	}
	writeln(max(x,y));
	return 0;
}
/*

in:
6
4 3 2 5 3 5

out:
9

*/
