#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<string.h>
#include<bits/stdc++.h>
using namespace std;
inline long long read(){
	long long x=0;char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-48,ch=getchar();
	return x;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+48);
}
inline void writeln(long long x){
	write(x),puts("");
}
int T,n,num[203];
int f[500003];
int ans,mx;
inline void solve(){
	for(register int i=1;i<=mx;++i){
		f[i]=0;
	}
	f[0]=1;
	for(register int i=1;i<=n;++i){
		if(f[num[i]]){
			--ans;
			continue;
		}
		for(register int j=num[i];j<=mx;++j){
			f[j]|=f[j-num[i]];
		}
	}
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		n=read();
		ans=n;
		for(register int i=1;i<=n;++i){
			num[i]=read();
		}
		sort(num+1,num+n+1);
		mx=num[n];
		solve();
		writeln(ans);
	}
	return 0;
}
/*

in:
2
4
3 19 10 6
5
11 29 13 19 17

out:
2
5

*/
