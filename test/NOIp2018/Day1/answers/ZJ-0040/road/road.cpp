#include<bits/stdc++.h>
using namespace std;
#define Rep(i,a,b) for(register int i=(a);i<=(b);++i)
#define rep(i,a,b) for(register int i=(a);i<(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=(b);--i)
typedef long long ll;
#define gc getchar
#define pc putchar
inline ll read(){
	ll x = 0;int f = 1;char c = gc();
	for(;!isdigit(c);c=gc()) if(c=='-') f = -1;
	for(;isdigit(c);c=gc()) x = (x<<1) + (x<<3) + (c^'0');
	return x * f;
}
inline void write(ll x){
	if(x<0){pc('-');x=-x;}if(x>=10) write(x / 10);pc(x%10+'0');
}
inline void writeln(ll x){write(x),pc('\n');}
inline void wri(ll x){write(x),pc(' ');}
const int maxn = 1e5+233;
#define lson (o<<1)
#define rson (o<<1|1)
#define mid ((l+r)>>1)
int T[maxn << 2];
int a[maxn],n;
int ans = 0;
inline void build(int o,int l,int r){
	if(l==r){
		T[o] = l;
		return ;
	}
	build(lson,l,mid);
	build(rson,mid+1,r);
	T[o] = (a[T[lson]] < a[T[rson]]) ? T[lson] : T[rson];
}
inline int query(int o,int l,int r,int x,int y){
	if(l==x&&r==y){
		return T[o];
	}
	if(y<=mid) return query(lson,l,mid,x,y); else
	if(mid+1<=x) return query(rson,mid+1,r,x,y); else{
		int p1 = query(lson,l,mid,x,mid);
		int p2 = query(rson,mid+1,r,mid+1,y);
		return (a[p1] < a[p2]) ? p1 : p2;
	}
}
inline void solve(int l,int r,int add){
	//printf("%d %d\n",l,r);
	if(l>r) return ;
	int v = query(1,1,n,l,r);
	ans += a[v] - add;
	solve(l,v-1,a[v]);
	solve(v+1,r,a[v]);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n = read();
	Rep(i,1,n) a[i] = read();
	build(1,1,n);
	ans = 0;
	solve(1,n,0);
	writeln(ans);
	return 0;
}

