#include<bits/stdc++.h>
using namespace std;
#define Rep(i,a,b) for(register int i=(a);i<=(b);++i)
#define rep(i,a,b) for(register int i=(a);i<(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=(b);--i)
typedef long long ll;
#define gc getchar
#define pc putchar
inline ll read(){
	ll x = 0;int f = 1;char c = gc();
	for(;!isdigit(c);c=gc()) if(c=='-') f = -1;
	for(;isdigit(c);c=gc()) x = (x<<1) + (x<<3) + (c^'0');
	return x * f;
}
inline void write(ll x){
	if(x<0){pc('-');x=-x;}if(x>=10) write(x / 10);pc(x%10+'0');
}
inline void writeln(ll x){write(x),pc('\n');}
inline void wri(ll x){write(x),pc(' ');}
const int inf = 0x3f3f3f3f;
const int maxn = 105;
const int maxm = 25055;
int dis[maxm],b[maxm];
bool flag,vis[maxm];
int a[maxn],n,m;
inline void solve(int x,int x1,int x2){
	b[0] = 0;
	int i = x;
	while(true){
		b[++b[0]] = i;
		vis[i] = true;
		i += x1;
		if(i>=m) i-=m;
		if(i == x) break;
	}
	int pos = -1;
	for(int i=1;i<=b[0];++i){
		if(pos == -1 || (dis[b[i]] < dis[b[pos]])){
			pos = i;
		}
	}
//	printf("pos = %d\n",pos);
//	printf("pos=%d[%d %d %d]\n",pos,b[pos],dis[b[pos]],dis[b[1]]);
	for(register int i=pos;i<=b[0];++i){
		int S=b[i],T;
		if(i==b[0]) T=b[1]; else T=b[i+1];
		if(dis[T] > dis[S]+x2){
			dis[T] = dis[S]+x2;
			flag = true;
		}
	}
	for(register int i=1,j;i<=pos-1;++i){
		int S=b[i],T=b[i+1];
		if(dis[T] > dis[S]+x2){
			dis[T] = dis[S]+x2;
			flag = true;
		}
	}
}
inline bool insert(int x){ 
	int x1 = x % m;
	rep(i,0,m) vis[i] = false;
	flag = false;
	rep(i,0,m)
		if(!vis[i])
			solve(i,x1,x);
	return flag;
}
void Main(){
	n = read(),m = 25000;
	Rep(i,1,n) a[i] =read(); 
	sort(a+1,a+1+n);
	n = unique(a+1,a+1+n) - a - 1; 
	m = a[1];
	dis[0] = 1;
	rep(i,1,m) dis[i] = inf;
	int ans = 1;//第一个一定是 
	Rep(i,2,n){
		if(insert(a[i])){
			ans++;
		}//如果可以插入 
	}
	writeln(ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T = read();
	while(T--) Main();
	return 0;
}
