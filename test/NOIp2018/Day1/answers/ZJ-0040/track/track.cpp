#include<bits/stdc++.h>
using namespace std;
#define Rep(i,a,b) for(register int i=(a);i<=(b);++i)
#define rep(i,a,b) for(register int i=(a);i<(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=(b);--i)
typedef long long ll;
#define gc getchar
#define pc putchar
inline ll read(){
	ll x = 0;int f = 1;char c = gc();
	for(;!isdigit(c);c=gc()) if(c=='-') f = -1;
	for(;isdigit(c);c=gc()) x = (x<<1) + (x<<3) + (c^'0');
	return x * f;
}
inline void write(ll x){
	if(x<0){pc('-');x=-x;}if(x>=10) write(x / 10);pc(x%10+'0');
}
inline void writeln(ll x){write(x),pc('\n');}
inline void wri(ll x){write(x),pc(' ');}
const int maxn = 5e4+233;
struct Edge{
	int to,nxt,dist;
	Edge(){}
	Edge(int to,int nxt,int dist):to(to),nxt(nxt),dist(dist){}
}edge[maxn * 2];
int first[maxn],nume;
int n,m;
int fa[maxn],last[maxn];
int q[maxn];
void Addedge(int a,int b,int c){
	edge[nume] = Edge(b,first[a],c);
	first[a] = nume++;
}
void pre(int u){
	q[++q[0]] = u;
	for(int e=first[u];~e;e=edge[e].nxt){
		int v=edge[e].to;
		if(v==fa[u]) continue;
		fa[v] = u;
		last[v] = edge[e].dist;
		pre(v);
	}
}
int dp[maxn],a[maxn];
int s[maxn],top=0;
bool check(int limit){
	int ans = 0;
	Dep(wzp,q[0],1){
		int u = q[wzp];
		dp[u] = 0;
		a[0] = 0;
		for(int e=first[u];~e;e=edge[e].nxt){
			int v = edge[e].to;
			if(v == fa[u]) continue;
			a[++a[0]] = dp[v];
			//s.insert(dp[v]);
		}
		if(a[0]){
			sort(a+1,a+1+a[0]);
			int i=1,j=a[0];//upper_bound(a+1,a+1+a[0],a[1]) - a - 1;
			top = 0;
			while(1){
				while(i<=j && a[i]+a[j] >= limit){
					s[++top] = a[j];
					j--;
				}
				if(i > j) break;
				if(top){
//					printf("{%d %d}\n",a[i],s[top]);
//					assert(a[i]+s[top]>=limit);
					ans++;top--;
				} else dp[u] = max(dp[u],a[i]);
				i++;
			}
			while(top>=2){
//				printf("{%d %d}\n",s[top],s[top-1]);
				top--;top--;
				ans++;
			}
			if(top) dp[u] = max(dp[u],s[top]);
		}
		if(u!=1) dp[u] += last[u];
		if(dp[u] >= limit){
			dp[u] = 0;
			ans++;
		}
//		printf("dp[%d] = %d[%d]\n",u,dp[u],ans);
	}
//	printf("%d %d\n",limit,ans);
	return ans >= m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n = read(),m = read();
	int l = 500000000,r = 0;
	memset(first,-1,sizeof(first));nume = 0;
	rep(i,1,n){
		int a = read(),b = read(),c = read();
		Addedge(a,b,c);
		Addedge(b,a,c);
		l = min(l,c);
		r = r + c;
	}
	fa[1] = 0;
	q[0] = 0;
	pre(1);
//	Rep(i,1,n){
//		printf("%d %d\n",fa[i],last[i]);
//	}
	int ans = -1;
	while(l <= r){
		int mid = (l + r) >> 1;
		if(check(mid)){
			ans = mid;
			l = mid + 1;
		} else{
			r = mid - 1;
		}
	}
//	check(15);
	writeln(ans);
}
