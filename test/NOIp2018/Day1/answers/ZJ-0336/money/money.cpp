#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=105,M=25005;
template<class T>void read(T &x){
	int f=0;x=0;char ch=getchar();
	while(ch<'0'||ch>'9')f=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+(ch^48),ch=getchar();
	x=f? -x:x;
}
int n,t,a[N],f[M],ans,maxx;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(t);int i,j;
	while(t--){
		ans=0;maxx=0;
		memset(f,0,sizeof(f));
		read(n);
		for(i=1;i<=n;i++){
			read(a[i]);
			maxx=max(maxx,a[i]);
		}
		sort(a+1,a+1+n);
		for(i=1;i<=n;i++)
			if(!f[a[i]]){
				ans++;f[a[i]]=1;
				for(j=1;j+a[i]<=maxx;j++)
					if(f[j])f[j+a[i]]=1;
			}
		printf("%d\n",ans);
	}
	return 0;
}
