#include<cstdio>
using namespace std;
const int N=100005;
template<class T>void read(T &x){
	int f=0;x=0;char ch=getchar();
	while(ch<'0'||ch>'9')f=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+(ch^48),ch=getchar();
	x=f? -x:x;
}
int n,ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);int i,x,now=0;
	for(i=1;i<=n;i++){
		read(x);
		if(now<x)ans+=(x-now);
		now=x;
	}
	printf("%d\n",ans);
	return 0;
}
