#include<cstdio>
using namespace std;
const int N=50005;
template<class T>void read(T &x){
	int f=0;x=0;char ch=getchar();
	while(ch<'0'||ch>'9')f=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+(ch^48),ch=getchar();
	x=f? -x:x;
}
struct E{int nxt,to,dis;}e[N<<1];
int n,ednum,head[N],m,st[N<<2],id[N],top[N],siz[N],fa[N],dep[N],son[N],w[N],cnt,ans;
bool chainf=1;
int totlen=0,val[N],f[N][2],minnlen=9999999;//���ַ� 
inline void adde(int from,int to,int d){
	e[++ednum].nxt=head[from];e[ednum].to=to;
	e[ednum].dis=d;head[from]=ednum;
}
inline int min(int x,int y){return x<y? x:y;}
inline int max(int x,int y){return x>y? x:y;}
inline void swap(int &x,int &y){int t=x;x=y;y=t;}
inline void up(int p){st[p]=st[p<<1]+st[p<<1|1];}
bool poi(int x){
	int now=0,totduan=0;
	for(int i=2;i<=n;i++){
		now+=val[i];
		if(now>=x){
			totduan++;
			now=0;
		}
	}
	return totduan>=m;
}
void build(int p,int l,int r){
	if(l==r){st[p]=val[l];return;}
	int mid=(l+r)>>1;
	build(p<<1,l,mid);build(p<<1|1,mid+1,r);
	up(p);
}
int query(int p,int l,int r,int x,int y){
	if(x<=l&&r<=y)return st[p];
	int mid=(l+r)>>1,ret=0;
	if(x<=mid)ret+=query(p<<1,l,mid,x,y);
	if(mid<y)ret+=query(p<<1|1,mid+1,r,x,y);
	return ret;
}
void dfs1(int x,int f,int deep){
	fa[x]=f;dep[x]=deep;siz[x]=1;
	int maxson=-1;
	for(int i=head[x];i;i=e[i].nxt){
		int y=e[i].to;
		if(y==f)continue;val[y]=e[i].dis;
		dfs1(y,x,deep+1);siz[x]+=siz[y];
		if(siz[y]>maxson)maxson=siz[y],son[x]=y;
	}
}
void dfs2(int x,int topf){
	top[x]=topf;w[id[x]=++cnt]=val[x];
	if(!son[x])return;
	dfs2(son[x],topf);
	for(int i=head[x];i;i=e[i].nxt){
		int y=e[i].to;
		if(y==fa[x]||y==son[x])continue;
		dfs2(y,y);
	}
}
void dp(int x){
	int maxson=0;
	for(int i=head[x];i;i=e[i].nxt){
		int y=e[i].to;
		if(y==fa[x])continue;
		dp(y);
		f[x][1]=max(f[x][1],f[y][1]);
		f[x][0]=max(f[x][0],f[y][0]);
		f[x][0]=max(f[x][0],f[maxson][1]+f[y][1]);
		if(f[y][1]>f[maxson][1])maxson=y;
	}
	f[x][1]+=val[x];
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);int i,x,y,d;
	for(i=1;i<n;i++){
		read(x);read(y);read(d);
		if(y!=x+1)chainf=0;
		adde(x,y,d);adde(y,x,d);
		totlen+=d;minnlen=min(minnlen,d);
	}
	if(m==n-1){printf("%d\n",minnlen);return 0;}
	dfs1(1,0,1);dfs2(1,1);build(1,1,n);
	if(m==1){
		dp(1);
		printf("%d\n",max(f[1][0],f[1][1]));
		return 0;					
	}
	if(chainf){
		int l=minnlen,r=totlen;
		while(r-l>1){
			int mid=(l+r)>>1;
			if(poi(mid))l=mid;
			else r=mid;
		}
		printf("%d\n",poi(r)? r:l);
		return 0;
	}
	printf("233\n");
	return 0;
}
