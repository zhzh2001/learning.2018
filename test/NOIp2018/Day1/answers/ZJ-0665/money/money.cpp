#include<cstdio>
#include<iostream>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;

inline void kai(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}

inline int read(){
	int ans=0,f=1;char chr=getchar();
	while(!isdigit(chr)){if(chr=='-') f=-1;chr=getchar();}
	while(isdigit(chr)) {ans=(ans<<3)+(ans<<1)+chr-48;chr=getchar();}
	return ans*f;
}
int n,T,a[10005],f[2500005];
int p[10005];
int flag;
inline bool check(int m){
	memset(f,0,sizeof(f));
	for(int i=1;i<=m;i++) f[p[i]]=1;
	f[0]=1;
	for(int i=1;i<=m;i++)
		for(int j=p[i];j<=a[n];j++)
			f[j]|=f[j-p[i]];
	for(int i=1;i<=n;i++)
		if(f[a[i]]==0) return 0;
	return 1;
}

void dfs(int step,int m,int cnt){
	if(flag) return;
	if(cnt==m){
		if(check(cnt)) flag=m;
		return;
	}
	if(step==n+1) return;
	p[++cnt]=a[step];
	dfs(step+1,m,cnt);//ѡ 
	--cnt;
	dfs(step+1,m,cnt);//��ѡ 
}

int main(){
	kai();
	T=read();
	while(T--){
		n=read();
		for(int i=1;i<=n;i++)	a[i]=read();
		sort(a+1,a+n+1);
		int l=1,r=n,mid,ans;
		flag=0;
		p[1]=a[1];
		while(l<=r){
			mid=(l+r)>>1;
			flag=0;
			dfs(1,mid,1);
			if(flag) r=mid-1,ans=mid;
			else l=mid+1;
		}
		printf("%d",ans);puts("");
	}
	return 0;
}
