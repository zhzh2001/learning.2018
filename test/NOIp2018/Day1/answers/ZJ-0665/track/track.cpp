#include<cstdio>
#include<iostream>
#include<cctype>
#include<cstring>
#include<vector>
#include<map>
#include<ctime>
#include<cstdlib>
#define ll long long
using namespace std;

inline void kai(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
}

inline int read(){
	int ans=0,f=1;char chr=getchar();
	while(!isdigit(chr)){if(chr=='-') f=-1;chr=getchar();}
	while(isdigit(chr)) {ans=(ans<<3)+(ans<<1)+chr-48;chr=getchar();}
	return ans*f;
}
int n,m;
vector<int> v;
const int M=50005<<1;
int head[M],nxt[M],ver[M],tot,du[M];
ll val[M],ans;
inline void add(int x,int y,ll z){
	ver[++tot]=y;val[tot]=z;nxt[tot]=head[x];head[x]=tot;
}
inline void input(){
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		ll z;
		scanf("%lld",&z);
		du[x]++,du[y]++;
		add(x,y,z),add(y,x,z);
	}
}
inline bool juhua(){
	if(du[1]==n-1) return 1;
	return 0;
}
inline bool lian(){
	if(du[1]!=1||du[n]!=1) return 0;
	for(int i=2;i<=n-1;i++)
		if(du[i]!=2) return 0;
	return 1;
}
bool vis[M];
void dfs(int x,ll now){
	ans=max(now,ans);
	for(int i=head[x];i;i=nxt[i]){
		int y=ver[i];
		if(!vis[y]){
			vis[y]=1;
			dfs(y,now+val[i]);
			vis[y]=0;
		}
	}
}

inline void TP1(){//找 一 条树上最长距离 
	input();
	if(juhua()){
		int maxn=0,pos;
		int ans=0;
		for(int i=1;i<=tot;i+=2)
			if(maxn<val[i]){
				pos=i;
				maxn=val[i];
			}
		ans+=maxn;
		maxn=0;
		for(int i=1;i<=tot;i+=2)
			if(maxn<val[i]&&pos!=i){
				maxn=val[i];
			}
		ans+=maxn;
		cout<<ans;
		return;
	}
	for(int i=1;i<=n;i++)
		if(du[i]==1) v.push_back(i);
	ans=0;
	for(int i=0;i<v.size();i++){
		memset(vis,0,sizeof(vis));
		vis[v[i]]=1;
		dfs(v[i],0);
	}
	cout<<ans;
}
inline void TP2(){//m==n-1
	int ans=0x3f3f3f3f;
	for(int i=1;i<=n-1;i++){
		int x=read(),y=read(),z=read();
		ans=min(z,ans);
	}
	cout<<ans;
}
ll path[M];
void Path(int x,int fa,int cnt){
	path[cnt]=val[head[x]];
	for(int i=head[x];i;i=nxt[i]){
		int y=ver[i];
		if(!vis[y]){
			vis[y]=1;
			Path(y,x,++cnt);
			return;
		}
	}
}

inline bool check(ll x){
	int tot=0;
	ll p=0;
	for(int i=1;i<=n-1;i++){
		p+=path[i];
		if(p>x){
			tot++;
			p=path[i];
		}
	}
	if(p==x) tot++;
	if(tot>=m) return 1;
	return 0;
}

inline void TP3(){//链形 
	ll l=0,r=1000000000;
	Path(1,0,1);
	while(l<=r){
		ll mid=(l+r)>>1;
		if(check(mid)) l=mid+1,ans=mid;
		else r=mid-1;
	}
	cout<<ans;
	return;
}
map<int,ll> mn[50005];
inline void TP4(){ll minn=0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			mn[j][i]=mn[i][j]=val[head[i]]+val[head[j]],minn=min(mn[j][i],minn);
	cout<<minn*2;
	return;
}
int random(ll x){
	return rand()%x+1;
}
int main(){
	kai();
	srand(unsigned(time(0)));
	n=read(),m=read();
	if(m==1) {TP1();return 0;}//找一种就够了
	if(m==n-1) {TP2();return 0;}//找所有路径里的最小值 
	input();
	if(lian()){TP3();return 0;}//链形 
	if(juhua()) {TP4();return 0;}//菊花图 
	ll p=0;
	for(int i=1;i<=tot;i+=2) p+=val[i];
	cout<<random(p)<<endl;
	return 0;
}
