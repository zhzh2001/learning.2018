#include<cstdio>
#include<iostream>
#include<cctype>
#include<cstring>
#include<map>
#include<algorithm>
#define ll long long
using namespace std;

inline void kai(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
}

inline int read(){
	int ans=0,f=1;char chr=getchar();
	while(!isdigit(chr)){if(chr=='-') f=-1;chr=getchar();}
	while(isdigit(chr)) {ans=(ans<<3)+(ans<<1)+chr-48;chr=getchar();}
	return ans*f;
}
int n,a[1000005];
ll ans=0;
ll calc(int l,int r){
	ll t=0;
	if(l>r) return 0;
	if(l==r) return a[l];
	if(l<1||r>n) return 0;
	int minn=a[l];
	for(int i=l;i<=r;i++) minn=min(a[i],minn);
	for(int i=l;i<=r;i++) a[i]-=minn;//SEG�Ż� 
	t+=minn*1ll;
	int pos=l;
	while(a[pos]==0) pos++;
	int k=pos;
	for(int i=k+1;i<=r;i++){
		if(a[i]==0) {
			t+=calc(pos,i-1);
			pos=i;
			while(a[pos]==0) pos++;
		}
	}
	t+=calc(pos,r);
	return t;
}

int main(){
	kai();
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	ans=calc(1,n);
	cout<<ans;
	return 0;
}
