#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
int a[50005];
int cnt,mid,tot;
int n,m,nn;
inline int read() {
	char ch=getchar();int sum=0;
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') sum=sum*10+ch-'0',ch=getchar();
	return sum;
}
inline bool check() {
	cnt=0;
	int sum=0;
	for (int i=1;i<n;i++) {
		sum+=a[i];
		if (sum>=mid) sum=0,cnt++;
	}
	if (cnt>=m) return true;
	else return false;
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.ans","w",stdout);
	n=read();m=read();
	for (int i=1;i<n;i++) {
		int x=read(),y=read();
		a[i]=read();
		tot+=a[i];
	}
	int l=1,r=tot+1;
	while (l<r) {
		mid=(l+r)>>1;
		if (check()) l=mid+1;
		else r=mid;
	}
	printf("%d\n",r-1);
	return 0;
}
