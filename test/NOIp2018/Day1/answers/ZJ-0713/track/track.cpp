#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
int head[50005],vet[100005],w[100005],nxt[100005],num;
int a[50005],Nxt[50005],dp[50005];
bool used[50005];
int cnt,mid,tot;
int n,m,nn;
inline int read() {
	char ch=getchar();int sum=0;
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') sum=sum*10+ch-'0',ch=getchar();
	return sum;
}
inline void addedge(int x,int y,int c) {
	num++;vet[num]=y;w[num]=c;nxt[num]=head[x];head[x]=num;
	num++;vet[num]=x;w[num]=c;nxt[num]=head[y];head[y]=num;
}
inline int find(int x) {
	if (used[x]) Nxt[x]=find(Nxt[x]);
	return Nxt[x];
}
void dfs(int x,int fa) {
	dp[x]=0;
	for (int e=head[x];e;e=nxt[e]) {
		int v=vet[e];
		if (v==fa) continue;
		dfs(v,x);
	}
	nn=0;
	for (int e=head[x];e;e=nxt[e]) {
		int v=vet[e];
		if (v==fa) continue;
		a[++nn]=dp[v]+w[e];
	}
	if (nn==0) return;
	sort(a+1,a+nn+1);
	
	/*cout<<x<<":\n";
	for (int i=1;i<=nn;i++) cout<<a[i]<<' ';
	cout<<endl;*/
	
	while (nn>0&&a[nn]>=mid) nn--,cnt++;
	//
	//cout<<"nn"<<nn<<" cnt"<<cnt<<endl;
	
	if (nn==0) return;
	for (int i=1;i<=nn+1;i++) Nxt[i]=i,used[i]=0;
	for (int i=1;i<=nn;i++) {
		if (used[i]) continue;
		int po=lower_bound(a+1,a+nn+1,mid-a[i])-a;
		if (po>nn) continue;
		po=find(po);
		if (po==i) po++;
		if (po>nn) continue;
		po=find(po);
		if (po>nn) continue;
		//cout<<i<<"with"<<po<<endl;
		used[po]=used[i]=1;cnt++;
		Nxt[po]=find(Nxt[po]+1);
		Nxt[i]=find(Nxt[i]+1);
	}
	for (int i=nn;i>0;i--) {
		if (!used[i]) {
			dp[x]=a[i];
			break;
		}
	}
	//cout<<"dp"<<dp[x]<<" cnt"<<cnt<<endl;
}
inline bool check() {
	cnt=0;
	dfs(1,0);
	if (cnt>=m) return true;
	else return false;
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<n;i++) {
		int x=read(),y=read(),c=read();
		addedge(x,y,c);
		tot+=c;
	}
	/*
	for (int i=1;i<=tot+1;i++) {
		mid=i;
		cout<<i<<' '<<check()<<endl;
	}*/
	//
	//mid=8;check();
	//return 0;
	
	int l=1,r=tot+1;
	while (l<r) {
		mid=(l+r)>>1;
		if (check()) l=mid+1;
		else r=mid;
	}
	printf("%d\n",r-1);
	return 0;
}
