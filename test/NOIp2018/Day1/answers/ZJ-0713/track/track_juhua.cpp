#include<iostream>
#include<cstdio>
#include<algorithm>
#include<set>
using namespace std;
int a[50005];
int cnt,mid,tot;
int n,m,nn;
multiset<int> S;
inline int read() {
	char ch=getchar();int sum=0;
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') sum=sum*10+ch-'0',ch=getchar();
	return sum;
}
inline bool check() {
	//cout<<"check"<<mid<<endl;
	cnt=0;
	S.clear();
	for (int i=1;i<n;i++) {
		if (a[i]>=mid) cnt++;
		else S.insert(a[i]);
	}
	for (int i=1;i<n;i++) {
		multiset<int>::iterator it1=S.lower_bound(a[i]);
		if (it1==S.end() || (*it1)!=a[i]) continue;
		multiset<int>::iterator it2=S.lower_bound(mid-a[i]);
		if (it2==it1) it2++;
		if (it2==S.end()) continue;
		//cout<<(*it1)<<' '<<(*it2)<<endl;
		if ((*it1)+(*it2)<mid) puts("ERROR");
		S.erase(it1);S.erase(it2);
		cnt++;
		
	}
	if (cnt>=m) return true;
	else return false;
}
inline void solve() {
	//cout<<"solve"<<mid<<endl;
	sort(a+1,a+n);
for (int j=n-1;j>0;j--) {
	cnt=0;
	S.clear();
	for (int i=1;i<n;i++) {
		if (i==j) continue;
		if (a[i]>=mid) cnt++;
		else S.insert(a[i]);
	}
	for (int i=1;i<n;i++) {
		if (i==j) continue;
		multiset<int>::iterator it1=S.lower_bound(a[i]);
		if (it1==S.end() || (*it1)!=a[i]) continue;
		multiset<int>::iterator it2=S.lower_bound(mid-a[i]);
		if (it2==it1) it2++;
		if (it2==S.end()) continue;
		//cout<<(*it1)<<' '<<(*it2)<<endl;
		if ((*it1)+(*it2)<mid) puts("ERROR");
		S.erase(it1);S.erase(it2);
		cnt++;
	}
	if (cnt>=m) {
		while (a[j]>=mid && j>0) j--;
		
		printf("%d",a[j]);
		return;
	}
}
printf("0");
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.ans","w",stdout);
	n=read();m=read();
	for (int i=1;i<n;i++) {
		int x=read(),y=read();
		a[i]=read();
		tot+=a[i];
	}
	int l=1,r=tot+1;
	while (l<r) {
		mid=(l+r)>>1;
		if (check()) l=mid+1;
		else r=mid;
	}
	printf("%d\n",r-1);
	mid=r-1;
	solve();
	return 0;
}
