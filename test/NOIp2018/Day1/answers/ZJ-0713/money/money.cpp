#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
bool dp[25005];
int a[105],mx;
int n,T,ans;
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		scanf("%d",&n);
		ans=0;
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		mx=a[n];
		memset(dp,0,sizeof(dp));dp[0]=1;
		for (int i=1;i<=n;i++) {
			if (!dp[a[i]]) {
				ans++;
				for (int j=a[i];j<=mx;j++) {
					dp[j]|=dp[j-a[i]];
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
