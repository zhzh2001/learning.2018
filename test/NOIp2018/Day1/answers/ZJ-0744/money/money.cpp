#include <bits/stdc++.h>
using namespace std;
bool f[26000];
int a[110];
int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T, n, m, mx;
	scanf("%d", &T);
	for (; T--; )
	{
		memset(f, 0, sizeof f); f[0] = 1;
		scanf("%d", &n);
		m = n;
		for (int i = 1; i <= n; i++) scanf("%d", a + i);
		sort(a + 1, a + n + 1); mx = a[n];
		for (int i = 1; i <= n; i++)
		{
			if (f[a[i]])
			{
				m--;
				continue;
			}
			for (int j = 0; j <= mx; j++)
				if (j + a[i] <= mx && f[j]) f[j + a[i]] = 1;
		}
		printf("%d\n", m);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
