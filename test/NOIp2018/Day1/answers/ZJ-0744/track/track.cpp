#include <bits/stdc++.h>
#define pb push_back
using namespace std;
const int N = 50000 + 10;
int fst[N], nxt[N << 1], to[N << 1], wei[N << 1], E;
inline void add(int u, int v, int w) { nxt[++E] = fst[u], fst[u] = E, to[E] = v, wei[E] = w; }

int cnt[N], dep[N], nex[N], len, n, m, pre[N];
vector<int> tmp[N];

void dfs(int rt, int fa)
{
	cnt[rt] = dep[rt] = 0;
	for (int i = fst[rt]; i != -1; i = nxt[i]) if (to[i] != fa)
	{
		dfs(to[i], rt);
		cnt[rt] += cnt[to[i]];
		if (dep[to[i]] + wei[i] >= len) cnt[rt]++;
		else tmp[rt].pb(dep[to[i]] + wei[i]);
	}
	int al = tmp[rt].size() - 1;
	if (tmp[rt].empty()) return;
	if (al != 0) sort(tmp[rt].begin(), tmp[rt].end());
	int mat = al;
	for (int i = 0; i < al; i++) nex[i] = i + 1;
	nex[al] = -1;
	for (int i = 1; i <= al; i++) pre[i] = i - 1;
	pre[0] = N - 5;
	for (int i = 0; i < al; i++)
	{
		if (tmp[rt][i] == -1) continue;
		mat = max(mat, nex[i]);
		while (mat > i && tmp[rt][i] + tmp[rt][mat] >= len) mat--;
		if (mat == i) mat++;
		if (tmp[rt][i] + tmp[rt][mat] < len)
		{
			if (nex[mat] != -1 && tmp[rt][i] + tmp[rt][nex[mat]] >= len)
			{
				cnt[rt]++;
				tmp[rt][nex[mat]] = tmp[rt][i] = -1;
				nex[mat] = nex[nex[mat]], pre[nex[mat]] = pre[mat];
				nex[pre[i]] = nex[i], pre[nex[i]] = pre[i];
			}
		}
		else
		{
			cnt[rt]++;
			tmp[rt][mat] = tmp[rt][i] = -1;
			nex[pre[mat]] = nex[mat], pre[nex[mat]] = pre[mat];
			nex[pre[i]] = nex[i], pre[nex[i]] = pre[i];
		}
	}
	for (int i = al; i >= 0; i--) if (tmp[rt][i] != -1)
	{
		dep[rt] = tmp[rt][i];
		break;
	}
}

inline bool check(int x)
{
	for (int i = 1; i <= n; i++) tmp[i].clear();
	len = x;
	dfs(1, -1);
	return cnt[1] >= m;
}

int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	memset(fst, -1, sizeof fst);
	int u, v, w, ans = 0, sum = 0;
	scanf("%d%d", &n, &m);
	for (int i = 1; i < n; i++)
		scanf("%d%d%d", &u, &v, &w), add(u, v, w), add(v, u, w), sum += w;
	sum /= m;
	for (int l = 1, r = sum, mid = l + r >> 1; l <= r; mid = l + r >> 1)
	{
		if (check(mid)) ans = mid, l = mid + 1;
		else r = mid - 1;
	}
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
