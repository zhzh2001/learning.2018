#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const int N = 100100;
int a[N];

int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	int n;
	LL tot = 0;
	scanf("%d", &n);
	a[0] = 0;
	for (int i = 1; i <= n; i++)
	{
		scanf("%d", a + i);
		tot += abs(a[i] - a[i - 1]);
	}
	tot += abs(a[n]);
	printf("%lld\n", tot / 2);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
