#include<bits/stdc++.h>
using namespace std;
int n,m,cnt,ver[100001],head[100001],nxt[100001],edge[100001],poin,maxn,vis[100001],tp[100001];
inline void add(int x,int y,int z){
	ver[++cnt]=y;
	edge[cnt]=z;
	nxt[cnt]=head[x];
	head[x]=cnt;
	ver[++cnt]=x;
	edge[cnt]=z;
	nxt[cnt]=head[y];
	head[y]=cnt;
	return;
}
void dfs1(int x,int dis){
	vis[x]=1;
	if(dis>maxn){
		maxn=dis;
		poin=x;
	}
	for(int i=head[x];i;i=nxt[i]){
		if(!vis[ver[i]]){
			dfs1(ver[i],dis+edge[i]);
		}
	}
	return;
}
inline int min(int x,int y){
	return x<y?x:y;
}
inline int max(int x,int y){
	return x>y?x:y;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m==1){
		for(int i=1;i<n;++i){
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			add(u,v,w);
		}
		maxn=0;
		dfs1(1,0);
		memset(vis,0,sizeof(vis));
		dfs1(poin,0);
		printf("%d",maxn);
		return 0;
	}
	bool flag=1;
	for(int i=1;i<n;++i){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		add(u,v,w);
		tp[i]=w;
		if(u!=1&&v!=1)flag=0;
	}
	if(flag){
	sort(tp+1,tp+n);
	int minn=998244353;
    for(int i=1;i<=m;++i){
    	minn=min(minn,tp[n-i]+tp[n-1-2*m+i]);
	}
	printf("%d",minn);
	return 0;
	}
	sort(tp+1,tp+n);
	printf("%d",tp[n-m]);
	return 0;
}
