#include<bits/stdc++.h>
using namespace std;
int t,n,a[101],vis[25001];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d",&n);
		for(int i=1;i<=n;++i){
			scanf("%d",&a[i]);
		}
		sort(a+1,a+1+n);
		int ans=0;
		memset(vis,0,sizeof(vis));
		for(int i=1;i<=n;++i){
			if(!vis[a[i]]){
				ans++;
				vis[a[i]]=1;
				for(int j=1;j<=a[n];++j){
					if(vis[j]&&j+a[i]<=a[n])vis[j+a[i]]=1;
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
