#include<bits/stdc++.h>
#define rep(i, x, y) for(register int i = x; i <= y; ++i)
#define repd(i, x, y) for(register int i = x; i >= y; --i)
#define repl(i, x) for(register int i = fi[x]; i; i = l[i].next)
#define p_b push_back
#define m_p make_pair
#define X first
#define Y second
using namespace std;
typedef long long ll;
typedef pair<int, int> Pair;
const int maxn = 50010;
struct line{
	int y, w, next;
}l[maxn<<1];
int n, m, flag_2, flag_3, sum;
int lcnt, fi[maxn], deg[maxn];
int len, ma[maxn][2];
int pcnt, p[maxn];
int root, cnt[maxn], f[maxn], tmp[maxn];
vector<int> vec[maxn];
int read(){
	int num = 0, flag = 1; char c = getchar();
	for(; c < '0' || c >'9'; c = getchar())
		if(c == '-') flag = 0;
	for(; c >= '0' && c <= '9'; c = getchar())
		num = (num<<3)+(num<<1)+c-48;
	return flag? num: -num;
}
void make_line(int x, int y, int w){
	l[++lcnt].y = y; l[lcnt].next = fi[x];
	fi[x] = lcnt; l[lcnt].w = w;
}

void init(){
	n = read(); m = read();
	flag_2 = flag_3 = 1;
	rep(i, 1, n-1){
		int x = read(), y = read(), w = read();
		if(x != 1) flag_2 = 0;
		if(y != x+1) flag_3 = 0;
		make_line(x, y, w); make_line(y, x, w);
		++deg[x]; ++deg[y]; sum += w;
	}
}

void get_ma(int x, int f){
	repl(i, x){
		int v = l[i].y;
		if(v == f) continue;
		get_ma(v, x);
		if(ma[v][0]+l[i].w >= ma[x][0]){
			ma[x][1] = ma[x][0];
			ma[x][0] = ma[v][0]+l[i].w;
		} else ma[x][1] = max(ma[x][1], ma[v][0]+l[i].w);
	}
	len = max(len, ma[x][0]+ma[x][1]);
}

void work_1(){
	get_ma(1, 0);
	printf("%d\n", len);
	exit(0);
}

bool check_2(int x){
	int res = 0;
	int l = 1, r = pcnt;
	while(r && p[r] >= x) ++res, --r;
	while(l < r){
		if(p[l]+p[r] >= x){
			++res; ++l; --r;
		} else ++l;
	}
	return res >= m;
}

void work_2(){
	repl(i, 1) p[++pcnt] = l[i].w;
	sort(p+1, p+pcnt+1);
	int l = p[1], r = p[pcnt-1]+p[pcnt];
	while(l+1 < r){
		int mid = (l+r)>>1;
		if(check_2(mid)) l = mid;
		else r = mid;
	}
	printf("%d\n", check_2(r)? r: l);
	exit(0);
}

bool check_3(int x){
	int res = 0, sum = 0;
	rep(i, 1, pcnt){
		sum += p[i];
		if(sum >= x) ++res, sum = 0;
	}
	return res >= m;
}

void work_3(){
	rep(x, 1, n) repl(i, x)
		if(l[i].y == x+1) p[++pcnt] = l[i].w;
	int l = 0, r = sum/m;
	while(l+1 < r){
		int mid = (l+r)>>1;
		if(check_3(mid)) l = mid;
		else r = mid;
	}
	printf("%d\n", check_3(r)? r: l);
	exit(0);
}

bool check_f(){
	rep(i, 1, n) if(deg[i] > 3) return false;
	return true;
}

void get_cnt(int x, int fa, int val){
	tmp[x] = f[x] = cnt[x] = 0;
	repl(i, x){
		int v = l[i].y;
		if(v == fa) continue;
		get_cnt(v, x, val);
		cnt[x] += cnt[v];
		if(f[v]+l[i].w >= val){
			++cnt[x]; continue;
		}
		if(!tmp[x]) tmp[x] = f[v]+l[i].w;
		else {
			if(tmp[x]+f[v]+l[i].w >= val){
				++cnt[x]; tmp[x] = 0;
			} else tmp[x] = max(tmp[x], f[v]+l[i].w);
		}
	}
	f[x] = max(f[x], tmp[x]);
}

bool check_4(int x){
	get_cnt(root, 0, x);
	return cnt[root] >= m;
}

void work_4(){
	rep(i, 1, n) if(deg[i] <= 2){
		root = i; break;
	}
	int l = 0, r = sum/m;
	while(l+1 < r){
		int mid = (l+r)>>1;
		if(check_4(mid)) l = mid;
		else r = mid;
	}
	printf("%d\n", check_4(r)? r: l);
	exit(0);
}

bool check_ma(int x, int pos, int cnt, int val){
	int l = 0, r = (int)vec[x].size()-1;
	int res = 0;
	while(r >= 0 && vec[x][r] >= val){
		if(r != pos) ++res;
		--r;
	}
	if(l == pos) ++l;
	if(r == pos) --r;
	while(l < r){
		if(vec[x][l]+vec[x][r] >= val){
			++res; ++l; --r;
		} else ++l;
		if(l == pos) ++l;
		if(r == pos) --r;
	}
	return res >= cnt;
}

int get_ma(int x, int cnt, int val){
	int l = 0, r = (int)vec[x].size()-1;
	if(!cnt) return vec[x][r];
	while(l+1 < r){
		int mid = (l+r)>>1;
		if(check_ma(x, mid, cnt, val)) l = mid;
		else r = mid;
	}
	if(check_ma(x, r, cnt, val)) return vec[x][r];
	if(check_ma(x, l, cnt, val)) return vec[x][l];
	return 0;
}

Pair deal(int x, int val){
	int l = 0, r = (int)vec[x].size()-1;
	if(r == -1) return m_p(0, 0);
	sort(vec[x].begin(), vec[x].end());
	int res = 0;
	while(r >= 0 && vec[x][r] >= val) ++res, --r;
	while(l < r){
		if(vec[x][l]+vec[x][r] >= val){
			++res; ++l; --r;
		} else ++l;
	}
	return m_p(res, get_ma(x, res, val));
}

void get_f(int x, int fa, int val){
	cnt[x] = f[x] = 0; vec[x].clear();
	repl(i, x){
		int v = l[i].y;
		if(v == fa) continue;
		get_f(v, x, val);
		cnt[x] += cnt[v];
		vec[x].p_b(f[v]+l[i].w);
	}
	Pair now = deal(x, val);
	cnt[x] += now.X; f[x] = now.Y;
}

bool check(int x){
	get_f(1, 0, x);
	return cnt[1] >= m;
}

void work(){
	int l = 0, r = sum/m;
	while(l+1 < r){
		int mid = (l+r)>>1;
		if(check(mid)) l = mid;
		else r = mid;
	}
	printf("%d\n", check(r)? r: l);
}

int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	init();
	if(m == 1) work_1();
	if(flag_2) work_2();
	if(flag_3) work_3();
	if(check_f()) work_4();
	work();
	return 0;
}
