#include<bits/stdc++.h>
#define rep(i, x, y) for(register int i = x; i <= y; ++i)
#define repd(i, x, y) for(register int i = x; i >= y; --i)
#define repl(i, x) for(register int i = fi[x]; i; i = l[i].next)
using namespace std;
typedef long long ll;
const int maxn = 100010;
int n, d[maxn];
int read(){
	int num = 0, flag = 1; char c = getchar();
	for(; c < '0' || c >'9'; c = getchar())
		if(c == '-') flag = 0;
	for(; c >= '0' && c <= '9'; c = getchar())
		num = (num<<3)+(num<<1)+c-48;
	return flag? num: -num;
}

void init(){
	n = read();
	rep(i, 1, n) d[i] = read();
}

void work(){
	int now = 0; ll ans = 0;
	rep(i, 1, n){
		if(d[i] >= now) now = d[i];
		else ans += (ll)now-d[i], now = d[i];
	}
	printf("%lld\n", ans+now);
}

int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	init();
	work();
	return 0;
}
