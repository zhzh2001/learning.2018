#include<bits/stdc++.h>
#define rep(i, x, y) for(register int i = x; i <= y; ++i)
#define repd(i, x, y) for(register int i = x; i >= y; --i)
#define repl(i, x) for(register int i = fi[x]; i; i = l[i].next)
using namespace std;
typedef long long ll;
const int maxn = 110, maxm = 25010;
int n, a[maxn];
int f[maxm];
int read(){
	int num = 0, flag = 1; char c = getchar();
	for(; c < '0' || c >'9'; c = getchar())
		if(c == '-') flag = 0;
	for(; c >= '0' && c <= '9'; c = getchar())
		num = (num<<3)+(num<<1)+c-48;
	return flag? num: -num;
}

void init(){
	n = read();
	rep(i, 1, n) a[i] = read();
	sort(a+1, a+n+1);
}

void work(){
	int ans = n;
	memset(f, 0, sizeof(f));
	f[0] = 1;
	rep(i, 1, n){
		if(f[a[i]]){
			--ans; continue;
		}
		rep(j, a[i], a[n])
			f[j] |= f[j-a[i]];
	}
	printf("%d\n", ans);
}

int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T = read();
	while(T--){
		init();
		work();
	}
	return 0;
}
