#include<bits/stdc++.h>
using namespace std;
#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define rep(i,a,b) for (int i=a;i<=b;i++)
#define per(i,a,b) for (int i=a;i>=b;i--)
#define Rep(i,a,b) for (int i=a;i<b;i++)
#define Per(i,a,b) for (int i=a;i>b;i--)

void read(int&x){
	x=0;char c=getchar();
	while (!isdigit(c))c=getchar();
	while (isdigit(c))x=x*10+c-'0',c=getchar();
}

#define maxn 200005
int n,a[maxn];

int main(){
	freopen("road.in","r",stdin);
	freopen("bf.out","w",stdout);
	read(n);rep(i,1,n)read(a[i]);
	int res=0;
	while (true){
		int ind=1;
		while (a[ind]==0&&ind<=n)ind++;
		if (ind>n)break;
		int mn=a[ind];
		for (int i=ind;i<=n&&a[i]!=0;i++)mn=min(a[i],mn);
		res+=mn;
		for (int i=ind;i<=n&&a[i]!=0;i++)a[i]-=mn;
	}
	printf("%d\n",res);return 0;
}
