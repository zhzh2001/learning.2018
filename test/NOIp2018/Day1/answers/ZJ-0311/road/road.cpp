#include<bits/stdc++.h>
using namespace std;
#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define rep(i,a,b) for (int i=a;i<=b;i++)
#define per(i,a,b) for (int i=a;i>=b;i--)
#define Rep(i,a,b) for (int i=a;i<b;i++)
#define Per(i,a,b) for (int i=a;i>b;i--)

void read(int&x){
	x=0;char c=getchar();
	while (!isdigit(c))c=getchar();
	while (isdigit(c))x=x*10+c-'0',c=getchar();
}

#define maxn 200005
int n,a[maxn];
void solve(int l,int r){
	int res=0;res+=a[l];
	rep(i,l+1,r)if (a[i]>a[i-1])res+=a[i]-a[i-1];
	printf("%d\n",res);
}
int main(){
	// int x;read(x);cout<<x<<endl;	
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	rep(i,1,n)read(a[i]),a[i+n]=a[i];
	int mn=1;
	// rep(i,2,n)if (a[i]<a[mn])mn=i;
	solve(mn,mn+n-1);
	return 0;
}
