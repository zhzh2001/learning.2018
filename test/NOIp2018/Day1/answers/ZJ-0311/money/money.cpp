#include<bits/stdc++.h>
using namespace std;
#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define rep(i,a,b) for (int i=a;i<=b;i++)
#define per(i,a,b) for (int i=a;i>=b;i--)
#define Rep(i,a,b) for (int i=a;i<b;i++)
#define Per(i,a,b) for (int i=a;i>b;i--)
	
void read(int&x){
	x=0;char c=getchar();
	while (!isdigit(c))c=getchar();
	while (isdigit(c))x=x*10+c-'0',c=getchar();
}

#define maxn 105
#define maxv 25005
int T,n,a[maxn];bool vis[maxv];
void work(){
	read(n);rep(i,1,n)read(a[i]);
	sort(a+1,a+n+1);int res=0;
	memset(vis,0,sizeof(vis));
	// rep(i,1,n)printf("%d ",a[i]);puts("");
	vis[0]=true;
	rep(i,1,n)if (!vis[a[i]]){
		res++;
		rep(j,a[i],a[n])
			vis[j]|=vis[j-a[i]];
	}
	printf("%d\n",res);
}
int main(){
	// int x;read(x);cout<<x<<endl;	
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);while (T--)work();return 0;
}
