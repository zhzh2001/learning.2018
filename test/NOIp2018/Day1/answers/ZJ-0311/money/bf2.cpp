#include<bits/stdc++.h>
using namespace std;
#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define rep(i,a,b) for (int i=a;i<=b;i++)
#define per(i,a,b) for (int i=a;i>=b;i--)
#define Rep(i,a,b) for (int i=a;i<b;i++)
#define Per(i,a,b) for (int i=a;i>b;i--)

void read(int&x){
	x=0;char c=getchar();
	while (!isdigit(c))c=getchar();
	while (isdigit(c))x=x*10+c-'0',c=getchar();
}

#define maxn 105
#define maxv 25005
int T,n,a[maxn];bool vis[maxv],vis0[maxv];
int c[maxn],mv;
void solve(bool*v){
	rep(i,0,mv)v[i]=false;v[0]=true;
	rep(i,1,c[0]){
		rep(j,c[i],mv)v[j]|=v[j-c[i]];
	}
}
int lowbit(int x){return x&-x;}
int bitcnt(int c){
	int res=0;while (c)c-=lowbit(c),res++;return res;
}
bool check(){
	rep(i,0,mv)if (vis[i]!=vis0[i])return false;return true;
}
void work(){
	mv=0;read(n);rep(i,1,n)read(a[i]),mv=max(mv,a[i]);
	c[0]=n;rep(i,1,n)c[i]=a[i];
	solve(vis);
	// rep(i,1,mv)printf("%d ",vis[i]);puts("");
	int res=n;
	Rep(i,1,1<<n){
		int x=bitcnt(i);
		if (x<res){
			c[0]=0;
			for (int k=i,j=1;k;k>>=1,j++)
				if (k&1)c[++c[0]]=a[j];
			solve(vis0);
			if (check())res=x;
		}
	}
	printf("%d\n",res);return;
}
int main(){
	// int x;read(x);cout<<x<<endl;	
	freopen("money.in","r",stdin);
	freopen("bf.out","w",stdout);
	read(T);while (T--)work();return 0;
}
