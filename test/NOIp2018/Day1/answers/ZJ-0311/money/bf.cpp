#include<bits/stdc++.h>
using namespace std;
#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define rep(i,a,b) for (int i=a;i<=b;i++)
#define per(i,a,b) for (int i=a;i>=b;i--)
#define Rep(i,a,b) for (int i=a;i<b;i++)
#define Per(i,a,b) for (int i=a;i>b;i--)

void read(int&x){
	x=0;char c=getchar();
	while (!isdigit(c))c=getchar();
	while (isdigit(c))x=x*10+c-'0',c=getchar();
}

#define maxn 105
#define maxv 25005
int T,n,a[maxn];bool vis[maxv],vis0[maxv];
int c[maxn],mv;
void solve(bool*v){
	rep(i,0,mv)v[i]=false;v[0]=true;
	rep(i,1,n){
		rep(j,a[i],mv)v[j]|=v[j-a[i]];
	}
}
void work(){
	mv=0;read(n);rep(i,1,n)read(a[i]),mv=max(mv,a[i]);
	solve(vis);int res=0;
	rep(i,0,mv)vis0[i]=0;vis0[0]=true;
	rep(i,1,mv){
		if (vis[i]&&!vis0[i]){
			res++;
			rep(j,i,mv)vis0[j]|=vis0[j-i];
		}
	}
	rep(i,0,mv)if (vis[i]^vis0[i])printf("%d\n",-1);
	printf("%d\n",res);return;
}
int main(){
	// int x;read(x);cout<<x<<endl;	
	freopen("money.in","r",stdin);
	freopen("bf.out","w",stdout);
	read(T);while (T--)work();return 0;
}
