#include<bits/stdc++.h>
using namespace std;
typedef pair<int,int> pi;
#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define rep(i,a,b) for (int i=a;i<=b;i++)
#define per(i,a,b) for (int i=a;i>=b;i--)
#define Rep(i,a,b) for (int i=a;i<b;i++)
#define Per(i,a,b) for (int i=a;i>b;i--)

void read(int&x){
	x=0;char c=getchar();
	while (!isdigit(c))c=getchar();
	while (isdigit(c))x=x*10+c-'0',c=getchar();
}

#define maxn 50005
int n,k;
vector<pi >E[maxn];
int mid;
multiset<int>w[maxn];
pair<int,int> check(int x,int f){
	w[x].clear();int res=0;
	Rep(i,0,E[x].size())if (E[x][i].fi!=f){
		pair<int,int> v=check(E[x][i].fi,x);res+=v.fi;
		if (v.se+E[x][i].se>=mid)res++;else w[x].insert(v.se+E[x][i].se);
	}
	if (w[x].size()==0)return mp(res,0);
	// sort(w[x].begin(),w[x].end());
	int return_num=0;
	while (w[x].size()>=2){
		multiset<int>::iterator it=w[x].begin();
		multiset<int>::iterator el=w[x].lower_bound(mid-*it);
		// multiset<int>::iterator el=lower_bound(w[x].begin(),w[x].end(),mid-*it);
		if (el==it)el++;
		if (el!=w[x].end())w[x].erase(it),w[x].erase(el),res++;
		else return_num=*it,w[x].erase(it);
	}
	if (w[x].size()==1)return_num=*w[x].begin();
	return mp(res,return_num);
}
int main(){
	// int x;read(x);cout<<x<<endl;	
	freopen("track.in","r",stdin);
	freopen("bf.out","w",stdout);
	read(n);read(k);
	Rep(i,1,n){
		int x,y,w;read(x);read(y);read(w);
		E[x].pb(mp(y,w));E[y].pb(mp(x,w));
	}
	
	// mid=26282;pi ans=check(1,0);cout<<ans.fi<<" "<<ans.se<<endl;return 0;
	int l=0,r=500000000,res=0;
	while (l<=r){
		mid=(l+r)>>1;
		if (check(1,0).fi>=k)res=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",res);return 0;
}
