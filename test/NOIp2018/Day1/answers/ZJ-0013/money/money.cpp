#include<bits/stdc++.h>
using namespace std;
const int N=25002;
int val[N];
int gcd(int a,int b){
	return b?gcd(b,a%b):a;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T; scanf("%d",&T);
	while(T--){
		int n; scanf("%d",&n);
		for(int i=1;i<=n;i++) scanf("%d",&val[i]);
		if(n==2){
			int res=gcd(val[1],val[2]);
			if(res==1) puts("2"); else puts("1");
			continue;
		}
		sort(val+1,val+n+1);
		if(n==3){
			bool flag=0;
			if(gcd(val[1],gcd(val[2],val[3]))!=1){
				puts("1"); continue;
			}
			if(gcd(val[2],val[3])!=1||gcd(val[1],val[3])!=1){
				puts("2"); continue;
			}
			for(int i=0;i<=1000;i+=val[1]){
				if(i>val[3]) break;
				if((val[3]-i)%val[2]==0){
					flag=1;
					if(gcd(val[1],val[2])==1) puts("2");
					else puts("1");
					break;
				}
			}
			if(!flag) puts("3");
			continue;
		}
        if(n==4) if(gcd(gcd(val[1],val[2]),gcd(val[3],val[4]))!=1) puts("1"); else puts("4");
        if(n==5) if(gcd(gcd(val[1],val[2]),gcd(val[3],val[4]))!=1) puts("1"); else puts("4");
	}
return 0;
}
