#include<bits/stdc++.h>
using namespace std;
const int N=5e4+2;
struct graph{
	int v,nxt,w;
}e[N];
int head[N],tot,sum,ans,edg[N],vis[N];
void add(int u,int v,int w){
	e[++tot].v=v; e[tot].w=w;
	e[tot].nxt=head[u]; head[u]=tot;
}
bool cmp(int a,int b){return a>b;}
void dfs(int u,int all){
	ans=max(ans,all);
	for(int i=head[u];i!=-1;i=e[i].nxt){
		int v=e[i].v,w=e[i].w;
		if(!vis[v]){
			vis[v]=1; dfs(v,all+w); vis[v]=0;
		}
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n,m; scanf("%d%d",&n,&m);
	bool flower=1; memset(head,-1,sizeof(head));
	for(int i=1;i<n;i++){
		int u,v,w; scanf("%d%d%d",&u,&v,&w);
		edg[i]=w; if(u!=1) flower=0;
		add(u,v,w); add(v,u,w);
	}
	if(flower){
		int Min=2e9; sort(edg+1,edg+n,cmp);
		if(m*2<=n-1){
			int l=1,r=m*2;
			while(l<=r){
				Min=min(Min,edg[l]+edg[r]); l++; r--;
			}
			printf("%d",Min); return 0;
		}
		if(m==n-1){printf("%d",edg[n-1]); return 0;}
		if((n-1)&1){
			int Min=edg[(m-n/2)*2+1],l=(m-n/2)*2+2,r=n-1;
			while(l<=r){
				Min=min(Min,edg[l]+edg[r]); l++; r--;
			}
			printf("%d",Min); return 0;
		}
		else{
			int Min=edg[(m-n/2)*2],l=(m-n/2)*2+1,r=n-1;
			while(l<=r){
				Min=min(Min,edg[l]+edg[r]); l++; r--;
			}
			printf("%d",Min); return 0;
		}
	}
	if(m==1){
		for(int i=1;i<=n;i++) vis[i]=1,dfs(i,0),vis[i]=0;
		printf("%d",ans); return 0;
	}
return 0;
}
