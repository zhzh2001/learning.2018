#include <cstdio>
#include <vector>
#include <algorithm>
#include <cassert>
int h[50001], to[100001], val[100001], nxt[100001], cnt;
inline void add_edge(const int& u, const int& v, const int& w) {
	to[++cnt] = v; nxt[cnt] = h[u]; val[cnt] = w; h[u] = cnt;
}
int n, m, u, v, w, l = 0x7fffffff, r = 0;
bool vis[50001];
int d[50001], dp[50001];
std::vector<int> vec[50001];
void dfs(const int& u, const int& fa, const int& x) {
	std::vector<int>&v = vec[u];
	for (register int i = h[u]; i; i = nxt[i]) if (to[i] != fa) {
		dfs(to[i], u, x);
		dp[u] += dp[to[i]];
		if (d[to[i]] + val[i] >= x) ++dp[u];
		else v.push_back(d[to[i]] + val[i]);
	}
	std::sort(v.begin(), v.end());
	int i = 0, j = v.size() - 1;
	for (; i < j; ++i)
		if (v[i] + v[j] >= x) {
			++dp[u];
			--j;
		}
		else d[u] = v[i];
	if (i <= j) d[u] = v[j];
}
bool check(const int& x) {
	for (register int i = 1; i <= n; ++i) {
		vis[i] = d[i] = dp[i] = 0;
		vec[i].clear();
	}
	dfs(1000, 0, x);
//	printf("check(%d) = %d\n", x, dp[1] >= m);
	return dp[1000] >= m;
}
int main() {
	scanf("%d%d", &n, &m);
	for (register int i = 1; i < n; ++i) {
		scanf("%d%d%d", &u, &v, &w);
		add_edge(u, v, w); add_edge(v, u, w);
		if (l > w) l = w;
		r += w;
	}
	int mid, ans = -1;
	while (l <= r) {
		mid = (l + r) >> 1;
//		printf("l = %d, r = %d, mid = %d\n", l, r, mid);
		if (check(mid)) ans = mid, l = mid + 1;
		else r = mid - 1;
	}
	assert(ans != -1);
	printf("%d\n", ans);
}
