#include <cstdio>
#include <set>
#include <algorithm>
#include <cassert>
#include <ctime>
int h[50001], to[100001], val[100001], nxt[100001], cnt;
inline void add_edge(const int& u, const int& v, const int& w) {
	to[++cnt] = v; nxt[cnt] = h[u]; val[cnt] = w; h[u] = cnt;
}
int n, m, u, v, w, l = 0x7fffffff, r = 0;
int d[50001], dp[50001];
std::multiset<int> ss[50001];
std::multiset<int>::iterator it, it1;
bool dfs(const int& u, const int& fa, const int& x) {
	std::multiset<int>&s = ss[u];
	for (register int i = h[u]; i; i = nxt[i]) if (to[i] != fa) {
		if (dfs(to[i], u, x)) return 1;
		if ((dp[u] += dp[to[i]]) >= m) return 1;
		if (d[to[i]] + val[i] >= x) ++dp[u];
		else s.insert(d[to[i]] + val[i]);
	}
	int temp;
	while (!s.empty()) {
		it = s.begin();
		temp = *it;
		s.erase(it);
		it1 = std::lower_bound(s.begin(), s.end(), x - temp);
		if (it1 != s.end()) {
			if ((++dp[u]) >= m) return 1;
			s.erase(it1);
		}
		else d[u] = temp;
	}
	return 0;
//	printf("d[%d] = %d\n", u, d[u]);
}
int root;
bool check(const int& x) {
//	printf("call::check(%d)\n", x);
	for (register int i = 1; i <= n; ++i) {
		d[i] = dp[i] = 0;
		ss[i].clear();
	}
//	printf("dp[%d] = %d\n", root, dp[root]);
//	printf("check(%d) = %d\n", x, dp[root] >= m);
//	assert(dp[root] < m);
	return dfs(1, 0, x);
}
int main() {
	clock_t c1 = clock();
	scanf("%d%d", &n, &m);
	for (register int i = 1; i < n; ++i) {
		scanf("%d%d%d", &u, &v, &w);
		add_edge(u, v, w); add_edge(v, u, w);
		if (l > w) l = w;
		r += w;
	}
//	for (root = 1; root <= n; ++root) check(26282);
	int mid, ans = -1;
	r /= m;
	while (l <= r) {
		mid = (l + r) >> 1;
//		printf("l = %d, r = %d, mid = %d\n", l, r, mid);
		if (check(mid)) ans = mid, l = mid + 1;
		else r = mid - 1;
	}
	assert(ans != -1);
	printf("%d\n", ans);
	printf("%.6lfms\n", (double)(clock() - c1) / CLOCKS_PER_SEC);
}
