#include <cstdio>
#include <cctype>
#include <set>
#include <algorithm>
const int ri_top = 1e7;
char ri[ri_top + 1], *rich = ri, *rr = ri;
inline void rd() {*(rr = ri + fread(rich = ri, 1, ri_top, stdin)) = 0;}
inline char nch() {if (++rich >= rr) rd(); return *rich;}
inline void read_int(int& x) {
	while (!isdigit(*rich)) nch();
	for (x = *rich - '0'; isdigit(nch()); x = x * 10 + *rich - '0');
}
int h[50001], to[100001], val[100001], nxt[100001], cnt;
inline void add_edge(const int& u, const int& v, const int& w) {
	to[++cnt] = v; nxt[cnt] = h[u]; val[cnt] = w; h[u] = cnt;
}
int n, m, u, v, w, l = 0x7fffffff, r = 0;
int d[50001], dp[50001], dfn[50001], clk, fa[50001];
std::multiset<int>::iterator it, it1;
void dfs(const int& u) {
	dfn[++clk] = u;
	for (register int i = h[u]; i; i = nxt[i]) if (to[i] != fa[u]) {
		fa[to[i]] = u;
		dfs(to[i]);
	}
}
bool check(const int& x) {
	std::multiset<int>s;
	for (register int i = n + 1; (u = dfn[--i]); ) {
		d[u] = dp[u] = 0;
		for (register int i = h[u]; i; i = nxt[i]) if (to[i] != fa[u]) {
			if ((dp[u] += dp[to[i]]) >= m) return 1;
			if (d[to[i]] + val[i] >= x) {
				if (++dp[u] >= m) return 1;
			}
			else s.insert(d[to[i]] + val[i]);
		}
		int temp;
		while (!s.empty()) {
			it = s.begin();
			temp = *it;
			s.erase(it);
			it1 = std::lower_bound(s.begin(), s.end(), x - temp);
			if (it1 != s.end()) {
				if ((++dp[u]) >= m) return 1;
				s.erase(it1);
			}
			else d[u] = temp;
		}
	}
	return 0;
}
int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	read_int(n); read_int(m);
	for (register int i = 1; i < n; ++i) {
		read_int(u); read_int(v); read_int(w);
		add_edge(u, v, w); add_edge(v, u, w);
		if (l > w) l = w;
		r += w;
	}
	dfs(1);
	int mid, ans = -1;
	r /= m;
	l *= (n / m);
	while (l <= r) {
		mid = (l + r) >> 1;
		if (check(mid)) ans = mid, l = mid + 1;
		else r = mid - 1;
	}
	printf("%d\n", ans);
	return fclose(stdin), fclose(stdout), 0;
}
