#include <cstdio>
#include <algorithm>
int n, x, l, s;
int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (register int i = 0; i < n; ++i) {
		scanf("%d", &x);
		s += std::max(x - l, 0);
		l = x;
	}
	printf("%d\n", s);
	return fclose(stdin), fclose(stdout), 0;
}
