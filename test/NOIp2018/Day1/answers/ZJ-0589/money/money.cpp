#include <cstdio>
#include <bitset>
#include <algorithm>
std::bitset<25001>b;
int T, n, a[101], ans, limit, x, y;
int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	for (scanf("%d", &T); T --> 0; ) {
		b = 1; x = y = 0;
		scanf("%d", &n); ans = n;
		for (register int i = 0; i < n; ++i)
			scanf("%d", &a[i]);
		std::sort(a, a + n);
		if (a[0] == 1) {
			puts("1");
			continue;
		}
		limit = 25000;
		for (register int i = 0; i < n; ++i) {
			if (a[i] > limit || b.test(a[i])) --ans;
			else {
				if (!x) x = a[i];
				else if (!y) y = a[i], limit = std::min(x * y - x - y, 25000);
				for (register int j = limit / a[i]; j; --j)
					if (!b.test(j * a[i])) b |= b << (j *  a[i]);
			}
		}
		printf("%d\n", ans);
	}
	return fclose(stdin), fclose(stdout), 0;
}
