#include <cstdio>
#include <bitset>
#include <algorithm>
#include <ctime>
std::bitset<25001>b;
int T, n, a[101], ans;
int main() {
	for (scanf("%d", &T); T --> 0; ) {
		b = 1;
		scanf("%d", &n); ans = n;
		for (register int i = 0; i < n; ++i)
			scanf("%d", &a[i]);
		std::sort(a, a + n);
		for (register int i = 0; i < n; ++i)
			if (b.test(a[i])) --ans;
			else for (register int j = 25000 / a[i]; j; --j)
				if (!b.test(j * a[i])) b |= b << (j *  a[i]);
		printf("%d\n", ans);
	}
}
