#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;

const int N=1e6+5;
int n,m,uu[N],vv[N],kk[N];

namespace P1
{
	int cnt,to[N],nxt[N],he[N],w[N];
	int d1[N],d2[N];
	
	inline void add(int u,int v,int k)
	{
		to[++cnt]=v,nxt[cnt]=he[u],
		he[u]=cnt,w[cnt]=k;
	}
	
	void dfs(int fa,int u)
	{
		for(int e=he[u];e;e=nxt[e])
		{
			int v=to[e];
			if(v!=fa)
			{
				dfs(u,v);
				if(d1[u]<d1[v]+w[e]) 
					d2[u]=d1[u],d1[u]=d1[v]+w[e];
				else d2[u]=max(d2[u],d1[v]+w[e]);
			}
		}
	}
	
	void main()
	{
		for(int i=1;i<n;i++) 
		{
			int u,v,k;
			scanf("%d%d%d",&u,&v,&k);
			add(u,v,k),add(v,u,k);
		}
		dfs(0,1);
		printf("%d\n",d1[1]+d2[1]);
	}
}

namespace P2
{
	int mx,l,r,mid,ans;
	inline bool check(int x)
	{
		int res=0,num=0;
		for(int i=1;i<n;i++) 
			if(res+kk[i]>=x) 
			{
				res=0,num++;
				if(num>=m) return 1;
			} else res+=kk[i];
		return 0;
	}
	
	void main()
	{
		mx=0;
		for(int i=1;i<n;i++) mx+=kk[i];
		l=1,r=mx,ans=0;
		while(l<=r)
		{
			mid=l+r>>1;
			if(check(mid)) ans=mid,l=mid+1;
				else r=mid-1;	
		}
		printf("%d\n",ans);
	}
}

namespace P3
{
	int cnt,t[N],nxt[N],to[N],he[N],w[N];
	int l,r,ans,mid;
	
	struct A{
		int x,res;
	};
	
	inline void add(int u,int v,int k)
	{
		to[++cnt]=v,nxt[cnt]=he[u],
		he[u]=cnt,w[cnt]=k;
	}
	bool cmp(int x,int y)
	{
		return x>y;
	}
	A dfs(int fa,int u,int lim)
	{
		int ret=0,num=0,a[4];
		a[1]=a[2]=a[3]=0;
		for(int e=he[u];e;e=nxt[e])
		{
			int v=to[e];
			if(v!=fa) 
			{
				A x=dfs(u,v,lim);
				ret+=x.x;
				a[++num]=x.res+w[e];
			}
		}
		if(num==1) 
		{
			if(a[1]>=lim) return (A){ret+1,0};
				else return (A){ret,a[1]};
		}else 
		if(num==2)
		{
			if(a[1]+a[2]>=lim) 
			{
				if(a[1]>=lim&&a[2]>=lim) return (A){ret+2,0};
					else if(max(a[1],a[2])>=lim) return (A){ret+1,min(a[1],a[2])}; 
						else return (A){ret+1,0};
			} else return (A){ret,max(a[1],a[2])};
		}
		if(num==3) 
		{
			sort(a+1,a+3+1,cmp);
			if(a[1]>=lim&&a[2]>=lim&&a[3]>=lim) return (A){ret+3,0};
			if(a[1]>=lim&&a[2]>=lim) return (A){ret+2,a[3]};
			if(a[1]>=lim)
			{
				if(a[2]+a[3]>=lim) return (A){ret+2,0};
				return (A){ret+1,a[2]};
			}
			if(a[1]+a[2]>=lim) return (A){ret+1,a[3]};
			return (A){ret,a[1]};
		}
		return (A){0,0};
	}
	void main()
	{
		l=r=ans=0;
		for(int i=1;i<n;i++)
			add(uu[i],vv[i],kk[i]),add(vv[i],uu[i],kk[i]),r+=kk[i];
		while(l<=r)
		{
			mid=l+r>>1;
			if(dfs(0,1,mid).x>=m) ans=mid,l=mid+1;
				else r=mid-1;
		}
		printf("%d\n",ans);
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m==1) 
	{
		P1::main();
		fclose(stdin); fclose(stdout);
		return 0;
	}
	bool IS_LIAN=1;
	for(int i=1;i<n;i++)
	{
		scanf("%d%d%d",&uu[i],&vv[i],&kk[i]);
		if(vv[i]!=uu[i]+1) IS_LIAN=0;
	}
	if(IS_LIAN) 
	{
		P2::main();
		fclose(stdin); fclose(stdout);
		return 0;
	}
	P3::main();
	fclose(stdin); fclose(stdout); 
	return 0;
}
