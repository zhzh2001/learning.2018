#include<cstdio>
#include<algorithm>
using namespace std;

const int N=105,M=1e5+5;
int n,a[N],f[M],mx,ans;

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);
		for(int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		mx=a[n]; ans=0;
		for(int i=1;i<=n;i++)
			if(!f[a[i]])
			{
				f[a[i]]=1;
				for(int j=a[i]+1;j<=mx;j++) 
					if(f[j-a[i]]) f[j]=1;
				ans++;
			}
		printf("%d\n",ans);
		for(int i=1;i<=mx;i++) f[i]=0;
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
