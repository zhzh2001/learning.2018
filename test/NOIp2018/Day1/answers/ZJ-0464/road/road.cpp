#include<cstdio>
using namespace std;

const int N=1e6+5;
int n,lst,ans,x;

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	lst=ans=0;
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&x);
		if(x>lst) ans+=x-lst;
		lst=x;
	}
	printf("%d\n",ans);
	fclose(stdin); fclose(stdout);
	return 0;
}
