#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
int n,a[100008],f[10008],inf,ans;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--)
	{
		memset(a,0,sizeof(a));
		memset(f,false,sizeof(f));
		inf=0; ans=0;
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			scanf("%d",&a[i]),inf=max(inf,a[i]);
		sort(a+1,a+n+1);
		f[0]=true;
		for(int i=1;i<=n;i++)
		{
			if(f[a[i]])continue;
			for(int j=a[i];j<=inf;j++)
				if(f[j-a[i]])f[j]=true;
			ans++;
		}
		printf("%d\n",ans);
	}
	return 0;
}
