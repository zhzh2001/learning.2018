#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
const int N=500008;
struct Edge
{
	int nxt,to,len;
}edge[N];
struct Node
{
	int num,len;
}g[N];
int cnt,head[N],dep[N],dis[N],n,m,len[N],ans,r[N],son[N][3],f[N];
void add(int x,int y,int z)
{
	edge[++cnt]=(Edge){head[x],y,z};
	head[x]=cnt;
	r[x]++;
}
bool cmp(Edge a,Edge b){return a.len>b.len;}
void dfs1(int x,int fa)
{
	for(int p=head[x];p;p=edge[p].nxt)
	{
		int u=edge[p].to;
		if(u==fa)continue;
		dfs1(u,x);
		if(dep[u]+edge[p].len>dep[x])dis[x]=dep[x],dep[x]=dep[u]+edge[p].len;
		else if(dep[u]+edge[p].len>dis[x])dis[x]=dep[u]+edge[p].len;
		len[x]=max(len[x],len[u]);
	}
	len[x]=max(len[x],dis[x]+dep[x]);
}
bool judge(int x)
{
	int sum=0,now=0;
	for(int i=1;i<n;i++)
	{
		now=now+len[i];
		if(now>=x)now=0,sum++;
	}
	return sum>=m;
}
int check(int x,int y,int need)
{
	int tot=0;
	if(x>=need)tot++; if(y>=need)tot++;
	if(tot)return tot;
	if(x+y>=need)return 1;else return 0;
}
void dfs2(int x,int fa,int need)
{
	int tot=0;
	for(int p=head[x];p;p=edge[p].nxt)
	{
		int u=edge[p].to;
		if(u==fa)continue;
		dfs2(u,x,need);
		son[x][++tot]=u;
		len[son[x][tot]]=edge[p].len;
	}
	if(tot==0)return;
	f[x]=f[son[x][1]]+f[son[x][2]]+check(len[son[x][1]],len[son[x][2]],need);
	int tmp=len[son[x][1]]+len[son[x][2]];
	int have[3];
	for(int i=1;i<=2;i++)
		if(g[son[x][i]].len+len[son[x][i]]>=need)have[i]=g[son[x][1]].num+1; else have[i]=f[son[x][i]];
	f[x]=max(have[1]+have[2],f[x]);
	for(int i=1;i<=2;i++)
		if(g[son[x][i]].len+tmp>=need)f[x]=max(f[x],g[son[x][i]].num+1+f[son[x][i%2+1]]);
	if(g[son[x][1]].len+tmp+g[son[x][2]].len>=need)f[x]=max(f[x],g[son[x][1]].num+g[son[x][2]].num+1);
	g[x]=(Node){f[son[x][1]]+f[son[x][2]],max(len[son[x][1]],len[son[x][2]])};
	for(int i=1;i<=2;i++)
		if(g[son[x][i]].len+len[son[x][i]]<need)
			if(g[son[x][i]].num+have[i%2+1]>g[x].num)
				g[x]=(Node){g[son[x][i]].num+have[i%2+1],g[son[x][i]].len+len[son[x][i]]};
			else 
				if(g[son[x][i]].num+have[i%2+1]==g[x].num)
					g[x].len=max(g[x].len,g[son[x][i]].len+len[son[x][i]]);
	return;
}
bool ask(int x)
{
	memset(f,0,sizeof(f));
	memset(g,0,sizeof(g));
	for(int i=1;i<=n;i++)
		if(r[i]<6){dfs2(i,0,x);return f[i]>=m;}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	bool flag1=true,flag2=true,flag3=true;
	int tot=0;
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		tot+=z;
		add(x,y,z); add(y,x,z);
		if(x!=1)flag1=false;
		if(y!=x+1)flag2=false;
		if(r[x]>6||r[y]>6)flag3=false;
	}
	if(m==1)
	{
		dfs1(1,0);
		printf("%d",len[1]);
		return 0;
	}
	if(flag1)
	{
		sort(edge+1,edge+cnt+1,cmp);
		printf("%d",edge[2*m].len);
		return 0;
	}
	if(flag2)
	{
		for(int i=1;i<n;i++)
			for(int p=head[i];p;p=edge[p].nxt)
				if(edge[p].to==i+1){len[i]=edge[p].len;break;}
		int l=0,r=tot/2;
		while(l<=r)
		{
			int mid=l+r>>1;
			if(judge(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d",ans);
		return 0;
	}
	if(flag3)
	{
		int l=0,r=tot/2;
		while(l<=r)
		{
			int mid=l+r>>1;
			if(ask(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d",ans);
		return 0;
	}
	return 0;
}
/*
7 2
1 2 2
1 3 3
2 4 1
2 5 10
5 7 1
3 6 2
*/
