const
    maxn = 200005;
var
    edge : array[0..maxn] of record
        t, len, next : int64;
    end;
    a, dp, head : array[0..maxn] of int64;
    r : array[0..maxn] of record
        a, b, l : int64;
    end;
    ans, num, n, m, tot, stdmid, total : int64;
    i : longint;
    ai1, biai1 : boolean;

function max(x, y : int64) : int64;

begin
    if x > y then exit(x) else exit(y);
end;

procedure add(x, y, z : longint);

begin
    inc(num);
    edge[num].t := y;
    edge[num].len := z;
    edge[num].next := head[x];
    head[x] := num;
end;

procedure swap(var x, y : int64);
var
    tmp : int64;

begin
    tmp := x; x := y; y := tmp;
end;

procedure sort(l, r : longint);
var
    i, j, mid : longint;

begin
    i := l; j := r; mid := a[(l + r) >> 1];
    repeat
        while a[i] < mid do inc(i);
        while a[j] > mid do dec(j);
        if i <= j then
        begin
            swap(a[i], a[j]);
            inc(i); dec(j);
        end;
    until i > j;
    if i < r then sort(i, r);
    if l < j then sort(l, j);
end;

function check(mid : int64) : boolean;
var
    cnt, i : longint;
    sum : int64;

begin
    sum := 0; cnt := 0;
    for i := 1 to tot do
    begin
        inc(sum, a[i]);
        if sum >= mid then
        begin
            sum := 0; inc(cnt);
        end;
    end;
    if cnt >= m then exit(true) else exit(false);
end;

function check2(mid : int64) : boolean;
var
    i, j, k : longint;
    cnt : int64;

begin
    cnt := 0;
    i := n - 1;
    while (a[i] >= mid) and (i >= 1) do
    begin
        inc(cnt); dec(i);
    end;
    if cnt >= m then exit(true);
    k := i;
    for i := 1 to k - 1 do
        if a[i] + a[k] >= mid then break;
    j := i;
    while (j < k) and (a[j] + a[k] >= mid) do
    begin
        inc(cnt);
        dec(k);
        inc(j);
        while (j < k - 1) and (a[j] + a[k] < mid) do inc(j);
    end;
    if cnt >= m then exit(true) else exit(false);
end;

function dfs(u, pre : longint) : int64;
var
    i : longint;
    s, maxf, maxs, v : int64;

begin
    if dp[u] > 0 then exit(dp[u]);
    i := head[u];
    dp[u] := 0;
    maxf := -maxlongint; maxs := maxf;
    while i <> 0 do
    begin
        v := edge[i].t;
        if v <> pre then
        begin
            s := edge[i].len + dfs(v, u);
            dp[u] := max(dp[u], s);
            if s > maxf then
            begin
                maxs := maxf; maxf := s;
            end else
            if s > maxs then maxs := s;
        end;
        i := edge[i].next;
    end;
    ans := max(ans, maxf + maxs);
    exit(dp[u]);
end;

procedure do1;
var
    i : longint;
    x : int64;

begin
    ans := 0;
    x := dfs(1, 0);
    writeln(ans);
end;

procedure do2;
var
    l, rr, ans, mid : int64;
    i : longint;

begin
    tot := n - 1; rr := 0;
    for i := 1 to tot do
    begin
        a[i] := r[i].l;
        inc(rr, a[i]);
    end;
    sort(1, tot);
    l := 0;
    ans := 0;
    while l <= rr do
    begin
        mid := (l + rr) >> 1;
        if check2(mid) then
        begin
            l := mid + 1; ans := mid;
        end else rr := mid - 1;
    end;
    writeln(ans);
end;

procedure do3;
var
    u, l, rr, mid, ans, i : int64;

begin
    u := 1; tot := 0; rr := 0;
    while tot < n - 1 do
    begin
        i := head[u];
        while edge[i].t <> u + 1 do i := edge[i].next;
        inc(tot); a[tot] := edge[i].len; inc(rr, edge[i].len);
        u := edge[i].t;
    end;
    l := 0;
    ans := 0;
    while l <= rr do
    begin
        mid := (l + rr) >> 1;
        if check(mid) then
        begin
            l := mid + 1;
            ans := mid;
        end else rr := mid - 1;
    end;
    writeln(ans);
end;

function dfs1(u, pre : longint) : int64;
var
    maxf, maxs, s, v, i : int64;

begin
    if dp[u] > 0 then exit(dp[u]);
    maxf := -maxlongint; maxs := -maxlongint;
    i := head[u];
    while i <> 0 do
    begin
        v := edge[i].t;
        if v <> pre then
        begin
            s := dfs1(v, u) + edge[i].len;
            if s >= stdmid then
            begin
                inc(total);
            end else
            begin
                if s > maxf then
                begin
                    maxs := maxf; maxf := s;
                end else
                if s > maxs then
                begin
                    maxs := s;
                end;
            end;
        end;
        i := edge[i].next;
    end;
    if maxf + maxs >= stdmid then
    begin
        inc(total); dp[u] := 0;
    end else dp[u] := max(0, maxf);
    exit(dp[u]);
end;

function check3(mid : int64) : boolean;
var
    i : longint;
    x : int64;

begin
    total := 0; stdmid := mid;
    fillchar(dp, sizeof(dp), 0);
    x := dfs1(1, 0);
    if total >= m then exit(true) else exit(false);
end;

procedure do4;
var
    l, rr, mid : int64;
    i : longint;

begin
    l := 0; rr := 1;
    for i := 1 to n - 1 do inc(rr, r[i].l);
    ans := 0;
    while l <= rr do
    begin
        mid := (l + rr) >> 1;
        if check3(mid) then
        begin
            l := mid + 1; ans := mid;
        end else rr := mid - 1;
    end;
    writeln(ans);
end;

begin
    assign(input,'track.in'); reset(input);
    assign(output,'track.out'); rewrite(output);
    readln(n, m);
    for i := 1 to n - 1 do
    begin
        readln(r[i].a, r[i].b, r[i].l);
        if r[i].a <> 1 then ai1 := true;
        if r[i].b <> r[i].a + 1 then biai1 := true;
        add(r[i].a, r[i].b, r[i].l);
        add(r[i].b, r[i].a, r[i].l);
    end;
    if m = 1 then do1 else
    if not ai1 then do2 else
    if not biai1 then do3 else do4;
    close(input); close(output);
end.