var
    dp : array[-25000..2500000] of boolean;
    a : array[0..10000] of longint;
    qnum, q, sum, i, ans, j, k, n, maxa : longint;

function max(x, y : longint) : longint;

begin
    if x > y then exit(x) else exit(y);
end;

procedure swap(var x, y : longint);
var
    tmp : longint;

begin
    tmp := x; x := y; y := tmp;
end;

procedure sort(l, r : longint);
var
    i, j, mid : longint;

begin
    i := l; j := r; mid := a[(l + r) >> 1];
    repeat
        while a[i] < mid do inc(i);
        while a[j] > mid do dec(j);
        if i <= j then
        begin
            swap(a[i], a[j]);
            inc(i); dec(j);
        end;
    until i > j;
    if i < r then sort(i, r);
    if l < j then sort(l, j);
end;

begin
    assign(input,'money.in'); reset(input);
    assign(output,'money.out'); rewrite(output);
    readln(qnum);
    for q := 1 to qnum do
    begin
        readln(n);
        sum := 0;
        maxa := 0;
        for i := 1 to n do
        begin
            read(a[i]);
            maxa := max(maxa, a[i]);
        end;
        sort(1, n);
        ans := 0;
        fillchar(dp, sizeof(dp), 0);
        dp[0] := true;
        for i := 1 to n do
        begin
            if not dp[a[i]] then inc(ans);
            for j := a[i] to maxa do
                dp[j] := dp[j] or dp[j - a[i]];
        end;
        writeln(ans);
    end;
    close(input); close(output);
end.


