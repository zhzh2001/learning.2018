var
    i : longint;
    n, x, ans, last : int64;

begin
    assign(input,'road.in'); reset(input);
    assign(output,'road.out'); rewrite(output);
    readln(n);
    read(ans);
    last := ans;
    for i := 2 to n do
    begin
        read(x);
        if x > last then inc(ans, x - last);
        last := x;
    end;
    writeln(ans);
    close(input); close(output);
end.