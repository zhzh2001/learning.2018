#include<bits/stdc++.h>
#define R register
using namespace std;
const int N = 5e4 + 10;
const int inf = 0x3f3f3f3f;

int n, m, E, lim, mid;
int fir[N], nex[N << 1], arr[N << 1], len[N << 1];
int a[N], f[N], g[N];
int tot, tr[N * 60], ch[N * 60][2], mxnum[N * 60];

void read(int &x) {
   R char c = getchar();
   while (c < '0' || c > '9') c = getchar();
   x = 0;
   while (c >= '0' && c <= '9')
      x = (x << 3) + (x << 1) + c - '0', c = getchar();
}

inline void Add_Edge(int x, int y, int l) {
   nex[++E] = fir[x];
   fir[x] = E; arr[E] = y; len[E] = l;
}

int Nw() {
   ++tot;
   tr[tot] = ch[tot][0] = ch[tot][1] = 0;
   mxnum[tot] = -inf;
   return tot;
}

void Updata(int &x, int l, int r, int pos) {
   if (!x) x = Nw();
   ++tr[x];
   mxnum[x] = max(mxnum[x], pos);
   if (l == r) return;
   int mid = (l + r) >> 1;
   if (mid >= pos) {
      Updata(ch[x][0], l, mid, pos);
   }
   else {
      Updata(ch[x][1], mid + 1, r, pos);
   }
}

int Query(int x, int l, int r, int val) {
   if (!tr[x]) return -1;
   if (l == r) {
      if (!--tr[x]) mxnum[x] = -inf;
      return l;
   }
   int mid = (l + r) >> 1;
   if (!tr[ch[x][0]] || mxnum[ch[x][0]] < val) {
      int ans = Query(ch[x][1], mid + 1, r, val);
      tr[x] = tr[ch[x][0]] + tr[ch[x][1]];
      mxnum[x] = max(mxnum[ch[x][0]], mxnum[ch[x][1]]);
      return ans;
   }
   else {
      int ans = Query(ch[x][0], l, mid, val);
      tr[x] = tr[ch[x][0]] + tr[ch[x][1]];
      mxnum[x] = max(mxnum[ch[x][0]], mxnum[ch[x][1]]);
      return ans;
   }
}

void dfs(int x, int fa, int pre) {
   f[x] = g[x] = 0;
   for (int i = fir[x]; i; i = nex[i]) {
      if (arr[i] != fa) {
	 dfs(arr[i], x, len[i]);
	 f[x] += f[arr[i]];
      }
   }
   tot = 0;
   int rt = 0, mx = 0;
   for (int i = fir[x]; i; i = nex[i]) {
      if (arr[i] != fa && g[arr[i]]) {
	 Updata(rt, 1, lim, g[arr[i]]);
      }
   }
   while (tr[rt]) {
      int t = Query(rt, 1, lim, 0);
      int r = Query(rt, 1, lim, mid - t);
      if (r != -1) {
	 ++f[x];
      }
      else {
	 mx = max(mx, t);
      }
   }
   g[x] = mx + pre;
   if (g[x] >= mid) {
      g[x] = 0;
      ++f[x];
   }
}

int check() {
   dfs(1, 0, 0);
   return f[1] >= m;
}

int main() {
   freopen("track.in", "r", stdin);
   freopen("track.out", "w", stdout);
   read(n); read(m);
   for (int i = 1; i < n; ++i) {
      int u, v, l;
      read(u); read(v); read(l);
      Add_Edge(u, v, l);
      Add_Edge(v, u, l);
      lim += l;
   }
   int l = 1, r = lim / m, ans = 0;
   while (l <= r) {
      mid = (l + r) >> 1;
      if (check()) {
	 l = mid + 1;
	 ans = mid;
      }
      else {
	 r = mid - 1;
      }
   }
   cout << ans << endl;
   return 0;
}
