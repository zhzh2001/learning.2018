#include<bits/stdc++.h>
using namespace std;
const int N = 1e5 + 10;

int n, m;
int a[N], tr[N << 2];
long long res;

void read(int &x) {
   char c = getchar();
   while (c < '0' || c > '9') c = getchar();
   for (x = 0; c >= '0' && c <= '9'; c = getchar()) {
      x = (x << 3) + (x << 1) + c - '0';
   }
}

void Build(int x, int l, int r) {
   if (l == r) {
      tr[x] = l;
      return;
   }
   int mid = (l + r) >> 1;
   Build(x << 1, l, mid);
   Build(x << 1 | 1, mid + 1, r);
   if (a[tr[x << 1]] < a[tr[x << 1 | 1]]) tr[x] = tr[x << 1];
   else tr[x] = tr[x << 1 | 1];
}

int Query(int x, int l, int r, int ql, int qr) {
   if (ql <= l && r <= qr) return tr[x];
   int mid = (l + r) >> 1, ans = 0;
   if (mid >= ql) {
      int tmp = Query(x << 1, l, mid, ql, qr);
      if (a[ans] > a[tmp]) ans = tmp;
   }
   if (mid < qr) {
      int tmp = Query(x << 1 | 1, mid + 1, r, ql, qr);
      if (a[ans] > a[tmp]) ans = tmp;
   }
   return ans;
}

void Solve(int l, int r, int pre)  {
   if (l > r) return ;
   int pos = Query(1, 1, n, l , r);
   res += a[pos] - pre;
   Solve(l, pos - 1, a[pos]);
   Solve(pos + 1, r, a[pos]);
}

int main() {
   freopen("road.in", "r", stdin);
   freopen("road.out", "w", stdout);
   read(n);
   a[0] = 1e9;
   for (int i = 1; i <= n; ++i)
      read(a[i]);
   Build(1, 1, n);
   Solve(1, n, 0);
   cout << res << endl;
   return 0;
}
