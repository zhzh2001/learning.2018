#include<bits/stdc++.h>
#define R register
using namespace std;
const int N = 105;
const int M = 25005;

int T;
int n, a[N], b[N], use[M], dis[M];

void init() {
   scanf("%d", &n);
   for (int i = 1; i <= n; ++i)
      scanf("%d", &a[i]);
   sort(a + 1, a + n + 1);
   int m = 0;
   for (int i = 1; i <= n; ++i) {
      if (!use[a[i] % a[1]]) {
	 use[a[i] % a[1]] = 1;
	 b[++m] = a[i];
      }
   }
   for (int i = 1; i <= m; ++i)
      a[i] = b[i];
   n = m;
   for (int i = 1; i <= n; ++i)
      use[a[i] % a[1]] = 0;
}

inline int mmin(R int x, R int y) {
   return (x < y) ? x : y;
}

void Solve() {
   init();
   memset(dis, 0, sizeof dis);
   dis[0] = 1;
   int ans = n;
   for (int i = 1; i <= n; ++i) {
      if (dis[a[i]]) {
	 --ans;
	 continue;
      }
      R int t = a[n] - a[i], x = a[i];
      for (R int j = 0; j <= t; ++j) {
	 dis[j + x] |= dis[j];
      }
   }
   cout << ans << endl;
}

int main() {
   freopen("money.in", "r", stdin);
   freopen("money.out", "w", stdout);
   cin >> T;
   while (T--) Solve();
   return 0;
}
