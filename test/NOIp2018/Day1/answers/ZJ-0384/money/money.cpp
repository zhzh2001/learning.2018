#include <cstdio>
#include <cstring>
#include <algorithm>
int n,a[1003];
bool f[200003];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		std::sort(a+1,a+n+1);
		memset(f,false,sizeof(f));
		int ans=0;f[0]=true;
		for (int i=1;i<=n;i++){
			if (!f[a[i]]){
				ans++;
				for (int j=0;j<=25000;j++) if (f[j]) f[j+a[i]]=true;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
