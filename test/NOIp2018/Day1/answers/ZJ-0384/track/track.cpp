#include <cstdio>
int n,M;
struct asd{
	int to,next,val;
}e[500003];
int head[500003],cnt=0;
namespace do1{
	int mx=0,mid=0;
	void dfs(int x,int fa,int dis){
		if (dis>mx){
			mx=dis;mid=x;
		}
		for (int i=head[x];i;i=e[i].next) if (e[i].to!=fa){
			dfs(e[i].to,x,dis+e[i].val);
		}
	}
	inline void doo(){
		mx=0;mid=0;dfs(1,0,0);
		dfs(mid,0,0);
		printf("%d\n",mx);
	}
}
namespace do2{
	int v[500003];
	inline bool pd(int m){
		int tsum=0,tot=0;
		for (int i=1;i<n;i++){
			tsum+=v[i];
			if (tsum>=m){
				tot++;tsum=0;
			}
		}
		if (tot>=M) return true;else return false;
	}
	inline void doo(){
		for (int i=1;i<n;i++){
			for (int j=head[i];j;j=e[j].next) if (e[j].to==i+1) v[i]=e[j].val;
		}
		
		int l=0,r=10000*n,ans=0;
		while (l<=r){
			int m=(l+r)>>1;
			if (pd(m)){
				ans=m;
				l=m+1;
			}else{
				r=m-1;
			}
		}
		printf("%d\n",ans);
	}
}
namespace do3{
	int f[500003],sum[500003];
	int dt=0,dm=0;
	void dfs3(int x,int fa,int dis,int val){
		if (dis>=dm){
			if (dt<val+f[x]+1) dt=val+f[x]+1;
			//return;
		}
		for (int i=head[x];i;i=e[i].next) if (e[i].to!=fa){
			dfs3(e[i].to,x,dis+e[i].val,val+sum[x]-f[e[i].to]);
		}
	}
	void dfs2(int x1,int fa1,int x2,int fa2,int dis,int val){
		if (dis>=dm){
			if (dt<val+f[x1]+f[x2]+1) dt=val+f[x1]+f[x2]+1;
			//return;
		}
		for (int i=head[x1];i;i=e[i].next) if (e[i].to!=fa1){
			for (int j=head[x2];j;j=e[j].next) if (e[j].to!=fa2){
				dfs2(e[i].to,x1,e[j].to,x2,dis+e[i].val+e[j].val,val+sum[x1]-f[e[i].to]+sum[x2]-f[e[j].to]);
			}
		}
		for (int i=head[x1];i;i=e[i].next) if (e[i].to!=fa1){
			dfs2(e[i].to,x1,x2,fa2,dis+e[i].val,val+sum[x1]-f[e[i].to]);
		}
		for (int j=head[x2];j;j=e[j].next) if (e[j].to!=fa2){
			dfs2(x1,fa1,e[j].to,x2,dis+e[j].val,val+sum[x2]-f[e[j].to]);
		}
	}
	void dfs(int x,int fa){
		for (int i=head[x];i;i=e[i].next) if (e[i].to!=fa){
			dfs(e[i].to,x);
			sum[x]+=f[e[i].to];
		}
		f[x]=sum[x];
		dt=0;dfs3(x,fa,0,0);
		for (int i=head[x];i;i=e[i].next) if (e[i].to!=fa){
			for (int j=head[x];j;j=e[j].next) if (e[j].to!=fa && e[i].to!=e[j].to){
				dfs2(e[i].to,x,e[j].to,x,e[i].val+e[j].val,sum[x]-f[e[i].to]-f[e[j].to]);
			}
		}
		if (dt>f[x]) f[x]=dt;
	}
	inline bool pd(int m){
		for (int i=1;i<=n;i++) f[i]=0,sum[i]=0;
		dm=m;
		dfs(1,0);
		if (f[1]>=M) return true;else return false;
	}
	inline void doo(){
		int l=0,r=10000*n,ans=0;
		while (l<=r){
			int m=(l+r)>>1;
			if (pd(m)){
				ans=m;
				l=m+1;
			}else{
				r=m-1;
			}
		}
		printf("%d\n",ans);
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&M);
	int x,y,z;bool pd2=true;
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);if (y!=x+1) pd2=false;
		e[++cnt].to=y;e[cnt].val=z;e[cnt].next=head[x];head[x]=cnt;
		e[++cnt].to=x;e[cnt].val=z;e[cnt].next=head[y];head[y]=cnt;
	}
	if (M==1){
		do1::doo();
	}else if (pd2){
		do2::doo();
	}else if (M==n-1){
		int ans=23333333;
		for (int i=1;i<=cnt;i++) if (e[i].val<ans) ans=e[i].val;
		printf("%d\n",ans);
	}else{
		do3::doo();
	}
	return 0;
}
