#include <stdio.h>
#include <string.h>
#include <functional>
#include <algorithm>
using namespace std;

int n, m;
const int N = 50001;
int a[N];
int b[N];
int c[N];
int d[N];
int e[N];
int f[1001][1001];

static void juhua() {
	partial_sort(c + 1, c + 1 + m * 2, c + n, greater<int>());
	printf("%d\n", c[m * 2 - 1] + c[m * 2]);
}

static bool check(const int len) {
	int mm = 0;
	int sm = 0;
	for (int i = 1; i <= n - 1; ++i) {
		sm += c[i];
		if (sm >= len) {
			++mm;
			if (mm >= m)
				return true;
			sm = 0;
		}
	}
	return false;
}

static bool inline cmp(int l, int r) {
	return a[l] < a[r];
}

static void lian() {
	for (int i = 1; i <= n - 1; ++i) {
		d[i] = i;
	}
	sort(d + 1, d + n, cmp);
	sort(a + 1, a + n);
	sort(b + 1, b + n);
	for (int i = 1; i <= n - 1; ++i) {
		e[i] = c[d[i]];
	}
	memcpy(c, e, sizeof(c));
	int sum = 0;
	int mx = 10001;
	for (int i = 1; i <= n - 1; ++i) {
		sum += c[i];
		if (c[i] < mx) {
			mx = c[i];
		}
	}
	int l = mx, r = sum;
	while (r - l > 1) {
		const int mid = (l + r) / 2;
		if (check(mid))
			l = mid;
		else
			r = mid - 1;
	}
	printf("%d\n", check(r) ? r : l);
}

static void flyd() {
	memset(f, 0x3f, sizeof(f));
	for (int i = 1; i <= n - 1; ++i) {
		f[a[i]][b[i]] = f[b[i]][a[i]] = c[i];
	}
	for (int k = 1; k <= n; ++k)
		for (int i = 1; i <= n; ++i)
			for (int j = 1; j <= n; ++j)
				if (i == j) continue;
				else {
					const int dis = f[i][k] + f[k][j];
					if (dis < f[i][j])
						f[i][j] = dis;
				}
	int ans = 0;
	for (int i = 1; i <= n; ++i)
		for (int j = 1; j <= n; ++j)
			if (f[i][j] < 0x3f3f3f3f && f[i][j] > ans)
				ans = f[i][j];
	printf("%d\n", ans);
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n - 1; ++i) {
		scanf("%d%d%d", a + i, b + i, c + i);
	}
	bool ai1 = true;
	for (int i = 1; i <= n - 1; ++i) {
		if (a[i] != 1) {
			ai1 = false;
			break;
		}
	}
	if (ai1) {
		juhua();
		return 0;
	}
	if (m == 1 && n <= 1000) {
		flyd();
		return 0;
	}
	if (m == n - 1) {
		printf("%d\n", *max_element(c + 1, c + n));
		return 0;
	}
	bool bai1 = true;
	for (int i = 1; i <= n - 1; ++i) {
		if (b[i] != a[i] + 1) {
			bai1 = false;
			break;
		}
	}
	if (bai1) {
		lian();
		return 0;
	}
	return 0;
}
