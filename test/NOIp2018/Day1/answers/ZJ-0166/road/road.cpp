#include <stdio.h>
#include <stack>
#include <algorithm>
using namespace std;

const int N = 100001;
int n, ans = 0;
int a[N];

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; ++i) {
		scanf("%d", a + i);
	}
	stack<int> stk;
	stk.push(a[1]);
	for (int i = 2; i <= n; ++i) {
		const int cur = a[i];
		while (!stk.empty()) {
			const int prev = stk.top();
			if (prev > cur) {
				stk.pop();
				const int pp = !stk.empty() ? stk.top() : -0x3f3f3f3f;
				ans += prev - max(pp, cur);
			} else if (prev < cur) {
				break;
			} else {
				stk.pop();
			}
		}
		stk.push(cur);
	}
	if (!stk.empty()) {
		ans += stk.top();
	}
	printf("%d\n", ans);
	return 0;
}
