#include <stdio.h>
#include <assert.h>
#include <algorithm>
#include <set>
using namespace std;

const int N = 101;
#define mx (*a.rbegin())
#define It set<int>::const_iterator

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T;
	scanf("%d", &T);
	set<int> a;
	for (int k = 0; k < T; ++k) {
		a.clear();
		int n;
		scanf("%d", &n);
		int ba[N];
		for (int i = 1; i <= n; ++i) {
			scanf("%d", ba + i);
			a.insert(ba[i]);
		}
		for (It it(a.begin()); it != a.end(); ++it) {
			int v = *it;
			int t = v * 2;
			while (t <= mx) {
				a.erase(t);
				t += v;
			}
		}
		for (It it(a.begin()); it != a.end(); ++it) {
			for (It jt(it); jt != a.end(); ++jt) {
				if (it == jt) continue;
				const int iv = *it;
				const int jv = *jt;
				for (int ii = 1; iv * ii < mx; ++ii) {
					for (int jj = 1; iv * ii + jv * jj <= mx; ++jj) {
						const int v = iv * ii + jv * jj;
						a.erase(v);
					}
				}
			}
		}
		printf("%d\n", a.size());
	}
	return 0;
}
