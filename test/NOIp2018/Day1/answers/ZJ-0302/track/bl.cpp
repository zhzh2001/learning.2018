#include<bits/stdc++.h>
using namespace std;

ifstream fin("track.in");
ofstream fout("track.ans");

const int N=100100;
int a[N],b[N],dp[N],fa[N],f[N],sum[N],G[N],ne[N],to[N],da[N],xb,n,m;

void dfs(int x){
	for(int i=G[x];~i;i=ne[i]){
		int u=to[i];
		if(u!=fa[x]){
			dp[u]=dp[x]+da[i];fa[u]=x;
			dfs(u);
		}
	}
}

int get_ans(int*a,int n,int m){
	int j=n,ans=0;
	for(int i=1;i<=n;i++){
		if(a[i]+a[j]<m)continue;
		if(i>=j)break;
		ans++;j--;
	}
	return ans;
}

void dfs2(int x,int mid){
	for(int i=G[x];~i;i=ne[i])
		if(to[i]!=fa[x])dfs2(to[i],mid);
	int cnt=0;sum[x]=0;f[x]=dp[x]-dp[fa[x]];
	for(int i=G[x];~i;i=ne[i])
		if(to[i]!=fa[x]){
			sum[x]+=sum[to[i]];
			a[++cnt]=f[to[i]];
		}
	a[0]=0;
	sort(a+1,a+cnt+1);
	int ret=get_ans(a,cnt,mid);
	int l=0,r=cnt;
	while(l<r){
		int Mid=(l+r+1)/2;
		int cnt2=0;
		for(int i=1;i<=cnt;i++)if(i!=Mid)
			b[++cnt2]=a[i];
		if(ret!=get_ans(b,cnt2,mid))r=Mid-1;
		else l=Mid;
	}
	sum[x]+=ret;f[x]+=a[l];
	if(f[x]>=mid)sum[x]++,f[x]=0;
}


int ck(int mid){
	dfs2(1,mid);
	return sum[1]>=m;
}


void add(int x,int y,int z){
	ne[xb]=G[x];to[xb]=y;da[xb]=z;G[x]=xb++;
}

int main(){
	fin>>n>>m;memset(G,-1,sizeof G);
	for(int i=1;i<n;i++){
		int x,y,z;fin>>x>>y>>z;
		add(x,y,z);add(y,x,z);
	}
	dfs(1);
	int l=0,r=10000*n;
	while(l<r){
		int mid=(l+r+1)/2;
		if(ck(mid))l=mid;
		else r=mid-1;		
	}
	fout<<l<<endl;
	return 0;
}

