#include<bits/stdc++.h>
using namespace std;

const int N=25001;
int f[N],a[N],n,T;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.ans","w",stdout);
	for(scanf("%d",&T);T--;){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",a+i);
		memset(f,0x3f,sizeof f);
		sort(a+1,a+n+1);
		f[0]=0;
		int mod=a[1];
		int ans=1;
		for(int i=2;i<=n;i++)
			if(f[a[i]%mod]>a[i]){
				ans++;
				for(int j=0;j<mod;j++)
					for(int k=0;k<mod;k++)
						f[(j+a[i]*k)%mod]=min(f[(j+a[i]*k)%mod],f[j]+a[i]*k);
			}
		printf("%d\n",ans);
	}
	return 0;
}

