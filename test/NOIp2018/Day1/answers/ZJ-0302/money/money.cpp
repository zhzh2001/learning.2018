#include<bits/stdc++.h>
using namespace std;

ifstream fin("money.in");
ofstream fout("money.out");

int gcd(int n,int m){return m?gcd(m,n%m):n;}

const int N=50010;
int M[N],f[N],a[N],n,T;
int main(){
	for(fin>>T;T--;){
		fin>>n;
		for(int i=1;i<=n;i++)fin>>a[i];
		sort(a+1,a+n+1);
		int m=1;
		memset(f,0x3f,sizeof f);f[0]=0;
		for(int i=0;i<2*a[1];i++)M[i]=i%a[1];
		for(int i=2;i<=n;i++)
			if(f[a[i]%a[1]]>a[i]){
				m++;
				int d=gcd(a[i],a[1]);
				int tmp1=a[i]%a[1];
				for(int j=0;j<d;j++){
					register int l=j;
					for(register int k=M[j+tmp1];k!=j;k=M[k+tmp1])f[l]>f[k]?l=k:0;
					int D=l;
					for(register int k=M[l+tmp1];k!=D;l=k,k=M[k+tmp1])f[k]>f[l]+a[i]?f[k]=f[l]+a[i]:0;
				}
			}
		fout<<m<<'\n';
	}
	return 0;
}

