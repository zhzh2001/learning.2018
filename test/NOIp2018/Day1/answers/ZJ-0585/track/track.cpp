#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<=b;i++)
#define per(i,a,b) for(int i=a;i>=b;i--)
typedef long long ll;
using namespace std;
inline void read(int &x){
	x=0;char c=getchar();int f=1;
	while(!isdigit(c)){if(c=='-')f=-1;c=getchar();}
	while(isdigit(c)){x=10*x+c-'0';c=getchar();}x*=f;
}
const int N = 202000;
int n,m,head[N],to[N],nxt[N],dis[N],edgenum;
void add(int u, int v, int d){
	dis[++edgenum]=d;to[edgenum]=v;
	nxt[edgenum]=head[u];head[u]=edgenum;
}
int w[N],fa[N],f[N],res,s[N],d,a[N];
void init(int u, int Fa){
	fa[u]=Fa;
	for(int i=head[u];i;i=nxt[i])if(to[i]!=Fa){
		init(to[i],u);w[to[i]]=dis[i];
	}
}
inline int yzr(int p, int len){//s[1..len] cut p
	int n=0;rep(i,1,len)if(i!=p)a[++n]=s[i];//a[1..n]
	int res=0;
	for(int i=n,j=1;j<i;i--){
		while(j<i&&a[i]+a[j]<d)j++;
		if(j>=i)break;res++;j++;
	}
	return res;
}
inline void dfs(int u){
	for(int i=head[u];i;i=nxt[i])if(to[i]!=fa[u])dfs(to[i]);
	int len=0;
	for(int i=head[u];i;i=nxt[i])if(to[i]!=fa[u]){
		if(f[to[i]]>=d)res++;else s[++len]=f[to[i]];
	}
	if(!len){f[u]=w[u];return;}
	s[0]=0;sort(s+1,s+len+1);int q=yzr(0,len);
	int pos=0,l=1,r=len;
	while(l<=r){
		int mid=(l+r)>>1;
		if(yzr(mid,len)==q)pos=mid,l=mid+1;else r=mid-1;
	}
	res+=q;f[u]=w[u]+s[pos];
}
inline int solve(int D){
	memset(f,0,4*(n+2));
	res=0;d=D;dfs(1);return res;
}
int main(){
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	read(n);read(m);int sum=0;
	rep(i,1,n-1){int u,v,l;read(u);read(v);read(l);add(u,v,l);add(v,u,l);sum+=l;}
	init(1,0);int l=1,r=sum/m+1,res=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if(solve(mid)>=m)res=mid,l=mid+1;else r=mid-1;
	}
	cout<<res;
	return 0;
}
