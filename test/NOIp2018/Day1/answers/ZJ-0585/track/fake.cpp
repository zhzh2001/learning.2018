#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<=b;i++)
#define per(i,a,b) for(int i=a;i>=b;i--)
typedef long long ll;
using namespace std;
inline void read(int &x){
	x=0;char c=getchar();int f=1;
	while(!isdigit(c)){if(c=='-')f=-1;c=getchar();}
	while(isdigit(c)){x=10*x+c-'0';c=getchar();}x*=f;
}
const int N = 202000;
int n,m,head[N],to[N],nxt[N],dis[N],edgenum;
void add(int u, int v, int d){
	dis[++edgenum]=d;to[edgenum]=v;
	nxt[edgenum]=head[u];head[u]=edgenum;
}
int w[N],fa[N],f[N],s[N],len;
void init(int u, int Fa){
	fa[u]=Fa;s[++len]=u;
	for(int i=head[u];i;i=nxt[i])if(to[i]!=Fa){
		init(to[i],u);w[to[i]]=dis[i];
	}
}
inline int solve(int d){
	int res=0;rep(i,0,n)f[i]=0;
	per(t,n,1){
		int u=s[t];
		if(f[u]>=d)f[u]=0,res++;
		f[u]+=w[u];if(f[u]>=d)f[u]=0,res++;
		if(f[fa[u]]+f[u]>=d)f[fa[u]]=d+1;else f[fa[u]]=max(f[fa[u]],f[u]);
	}
	return res;
}
int main(){
	freopen("track2.in","r",stdin);freopen("track.out","w",stdout);
	read(n);read(m);
	rep(i,1,n-1){int u,v,l;read(u);read(v);read(l);add(u,v,l);add(v,u,l);}
	init(1,0);int l=1,r=n*10000,res=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if(solve(mid)>=m)res=mid,l=mid+1;else r=mid-1;
	}
	cout<<res;
	return 0;
}
