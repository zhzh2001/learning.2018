#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<=b;i++)
#define per(i,a,b) for(int i=a;i>=b;i--)
typedef long long ll;
using namespace std;
inline void read(int &x){
	x=0;char c=getchar();int f=1;
	while(!isdigit(c)){if(c=='-')f=-1;c=getchar();}
	while(isdigit(c)){x=10*x+c-'0';c=getchar();}x*=f;
}
const int N = 666;
int n,a[N];
bool f[N];
int solve(int S){
	memset(f,0,sizeof(f));f[0]=1;
	rep(i,1,n)if(S>>i-1&1)
		rep(j,a[i],N-1)f[j]|=f[j-a[i]];
	rep(i,1,n)if(!f[a[i]])return 1e9;
	int res=0;while(S)res++,S-=S&-S;return res;
}
int main(){
	freopen("money.in","r",stdin);freopen("std.out","w",stdout);
	int T;read(T);
	while(T--){
		read(n);rep(i,1,n)read(a[i]);sort(a+1,a+n+1);
		int res=1e9;
		rep(i,0,(1<<n)-1)res=min(res,solve(i));
		printf("%d\n",res);
	}
	return 0;
}
