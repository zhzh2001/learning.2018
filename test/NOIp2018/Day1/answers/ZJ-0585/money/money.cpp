#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<=b;i++)
#define per(i,a,b) for(int i=a;i>=b;i--)
typedef long long ll;
using namespace std;
inline void read(int &x){
	x=0;char c=getchar();int f=1;
	while(!isdigit(c)){if(c=='-')f=-1;c=getchar();}
	while(isdigit(c)){x=10*x+c-'0';c=getchar();}x*=f;
}
const int N = 25555;
int n,a[N];
bool f[N];
int main(){
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	int T;read(T);
	while(T--){
		read(n);rep(i,1,n)read(a[i]);sort(a+1,a+n+1);
		memset(f,0,sizeof(f));f[0]=1;int res=0;
		rep(i,1,n)if(f[a[i]]==0){
			res++;rep(j,a[i],N-1)f[j]|=f[j-a[i]];
		}
		printf("%d\n",res);
	}
	return 0;
}
