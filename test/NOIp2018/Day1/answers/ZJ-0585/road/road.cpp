#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<=b;i++)
#define per(i,a,b) for(int i=a;i>=b;i--)
typedef long long ll;
using namespace std;
inline void read(int &x){
	x=0;char c=getchar();int f=1;
	while(!isdigit(c)){if(c=='-')f=-1;c=getchar();}
	while(isdigit(c)){x=10*x+c-'0';c=getchar();}x*=f;
}
const int N = 555555;
int n,a[N],fa[N],sta[N],top;ll res;

int main(){
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	read(n);rep(i,1,n)read(a[i]);
	rep(i,1,n){
		int x=0;while(top&&a[sta[top]]>a[i])x=sta[top--];
		if(x)fa[x]=i;if(top)fa[i]=sta[top];sta[++top]=i;
	}
	rep(i,1,n)res+=a[i]-a[fa[i]];cout<<res;
	return 0;
}
