#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<=b;i++)
#define per(i,a,b) for(int i=a;i>=b;i--)
typedef long long ll;
using namespace std;
inline void read(int &x){
	x=0;char c=getchar();int f=1;
	while(!isdigit(c)){if(c=='-')f=-1;c=getchar();}
	while(isdigit(c)){x=10*x+c-'0';c=getchar();}x*=f;
}
const int N = 555555;
int n,a[N],fa[N],sta[N],top;ll res;
void solve(int l, int r, int val){
	int p=l;rep(i,l,r)if(a[i]<a[p])p=i;
	res+=a[p]-val;if(l<=p-1)solve(l,p-1,a[p]);if(p+1<=r)solve(p+1,r,a[p]);
}
int main(){
	freopen("road.in","r",stdin);freopen("std.out","w",stdout);
	read(n);rep(i,1,n)read(a[i]);solve(1,n,0);
	cout<<res;
	return 0;
}
