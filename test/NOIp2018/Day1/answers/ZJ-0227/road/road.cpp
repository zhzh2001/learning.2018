#include <bits/stdc++.h>
using namespace std;
#define res register int
#define rep(a,b,c) for((a)=(b);(a)<=(c);++(a))
inline void read_(int &x){
	static int f;static char ch;
	for(f=x=0,ch=getchar();!isdigit(ch);f|=ch=='-',ch=getchar());
	for(;isdigit(ch);x=(x<<1)+(x<<3)+ch-48,ch=getchar());
	x=f?-x:x;
	return;
}
inline void print_(res x){
	if(!x){
		putchar(48);
		return;
	}
	if(x<0){
		putchar('-');
		x=-x;
	}
	static int Top=-1,Stack[20];
	for(;x;Stack[++Top]=x%10,x/=10);
	for(;~Top;putchar(Stack[Top--]+48));
	return;
}

int n,now,d,ans;

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read_(n);
	now=0;
	res i;
	rep(i,1,n){
		read_(d);
		if(d>now)ans+=d-now;
		now=d;
	}
	print_(ans);
	putchar('\n');
	fclose(stdin);
	fclose(stdout);
	return 0;
}
