#include <bits/stdc++.h>
using namespace std;
#define maxn 105
#define maxa 25005
#define res register int
#define rep(a,b,c) for((a)=(b);(a)<=(c);++(a))
inline void read_(int &x){
	static int f;static char ch;
	for(f=x=0,ch=getchar();!isdigit(ch);f|=ch=='-',ch=getchar());
	for(;isdigit(ch);x=(x<<1)+(x<<3)+ch-48,ch=getchar());
	x=f?-x:x;
	return;
}
inline void print_(int x){
	if(!x){
		putchar(48);
		return;
	}
	if(x<0){
		putchar('-');
		x=-x;
	}
	static int Top=-1,Stack[20];
	for(;x;Stack[++Top]=x%10,x/=10);
	for(;~Top;putchar(Stack[Top--]+48));
	return;
}

int T,n,a[maxn],ans;
bool f[maxa];

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read_(T);
	res i,j;
	for(;T;--T){
		read_(n);
		rep(i,1,n)read_(a[i]);
		sort(a+1,a+n+1);
		rep(i,1,a[n])f[i]=false;
		f[0]=true;
		ans=n;
		rep(i,1,n){
			if(f[a[i]]){
				--ans;
				continue;
			}
			rep(j,a[i],a[n])if(f[j-a[i]])f[j]=true;
		}
		print_(ans);
		putchar('\n');
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
