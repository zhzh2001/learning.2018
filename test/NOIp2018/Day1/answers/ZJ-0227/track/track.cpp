#include <bits/stdc++.h>
using namespace std;
#define maxn 50005
#define res register int
#define rep(a,b,c) for((a)=(b);(a)<=(c);++(a))
inline void read_(int &x){
	static int f;static char ch;
	for(f=x=0,ch=getchar();!isdigit(ch);f|=ch=='-',ch=getchar());
	for(;isdigit(ch);x=(x<<1)+(x<<3)+ch-48,ch=getchar());
	x=f?-x:x;
	return;
}
inline void print_(res x){
	if(!x){
		putchar(48);
		return;
	}
	if(x<0){
		putchar('-');
		x=-x;
	}
	static int Top=-1,Stack[20];
	for(;x;Stack[++Top]=x%10,x/=10);
	for(;~Top;putchar(Stack[Top--]+48));
	return;
}

int n,m;
int u[maxn],v[maxn],w[maxn];

namespace solve1{	//m=1
	struct Edge{
		int nxt,to,w;
	}e[maxn<<1];
	int cnt,head[maxn];
	inline void addedge(res u,res v,res w){
		e[++cnt].nxt=head[u];
		e[cnt].to=v;
		e[cnt].w=w;
		head[u]=cnt;
		return;
	}
	
	int mx,mxdep;
	int fa[maxn];
	void dfs(res u,res dep){
		if(dep>mxdep){
			mxdep=dep;
			mx=u;
		}
		res i,v;
		for(v=e[i=head[u]].to;i;v=e[i=e[i].nxt].to)
			if(v!=fa[u]){
				fa[v]=u;
				dfs(v,dep+e[i].w);
			}
		return;
	}
	
	void solve(){
		res i;
		rep(i,1,n-1){
			addedge(u[i],v[i],w[i]);
			addedge(v[i],u[i],w[i]);
		}
		mxdep=0;
		rep(i,0,n)fa[i]=0;
		dfs(1,0);
		mxdep=0;
		rep(i,0,n)fa[i]=0;
		dfs(mx,0);
		print_(mxdep);
		putchar('\n');
		return;
	}
}

namespace solve2{	//link
	int len[maxn];
	inline bool check(res lim){
		res i=1,now=0,tot=0;
		rep(i,1,n-1){
			now+=len[i];
			if(now>=lim){
				++tot;
				now=0;
			}
		}
		return tot>=m;
	}
	void solve(){
		res i;
		res l=0,r=0,mid;
		rep(i,1,n-1){
			len[u[i]]=w[i];
			r+=w[i];
		}
		for(;l<r;){
			mid=(l+r+1)>>1;
			if(check(mid))l=mid;
			else r=mid-1;
		}
		print_(l);
		putchar('\n');
		return;
	}
}

namespace solve3{	//flower
	inline bool check(res lim){
		res tot=0;
		for(;tot<=m&&w[n-tot-1]>=lim;++tot);
		res head=1;
		for(;tot<=m&&head<n-tot-1;++head)if(w[n-tot-1]+w[head]>=lim)++tot;
		return tot>=m;
	}
	void solve(){
		res i;
		res l=0,r=0,mid;
		rep(i,1,n)r+=w[i];
		for(;l<r;){
			mid=(l+r+1)>>1;
			if(check(mid))l=mid;
			else r=mid-1;
		}
		sort(w+1,w+(n-1)+1);
		print_(l);
		putchar('\n');
		return;
	}
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read_(n);
	read_(m);
	res i;
	register bool flag1=true,flag2=true;
	rep(i,1,n-1){
		read_(u[i]);
		read_(v[i]);
		read_(w[i]);
		if(v[i]!=u[i]+1)flag1=false;
		if(u[i]!=1)flag2=false;
	}
	if(m==1){
		solve1::solve();
		return 0;
	}
	if(flag1){
		solve2::solve();
		return 0;
	}
	if(flag2){
		solve3::solve();
		return 0;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
