var n,t,i,j,ans,maxx:longint;
    a:array[0..1001] of longint;
    vis:array[0..1000001] of boolean;
function max(x,y:longint):longint;
begin
  if x>y then
    exit(x);
  exit(y);
end;
procedure sort(l,r:longint);
var i,j,x,y:longint;
begin
  i:=l;j:=r;x:=a[(l+r) div 2];
  repeat
    while a[i]<x do
      inc(i);
    while x<a[j] do
      dec(j);
    if not(i>j) then
      begin
        y:=a[i];a[i]:=a[j];a[j]:=y;
        inc(i);j:=j-1;
      end;
  until i>j;
  if l<j then
    sort(l,j);
  if i<r then
    sort(i,r);
end;
begin
assign(input,'money.in');reset(input);
assign(output,'money.out');rewrite(output);
  read(t);
  for t:=1 to t do
    begin
      read(n);
      maxx:=-maxlongint;
      fillchar(a,sizeof(a),0);
      fillchar(vis,sizeof(vis),false);
      for i:=1 to n do
        begin
          read(a[i]);
          maxx:=max(maxx,a[i]);
        end;
      sort(1,n);
      ans:=0;
      for i:=1 to n do
        begin
          if vis[a[i]] then
            continue;
          inc(ans);
          vis[a[i]]:=true;
          for j:=0 to maxx-a[i] do
           if vis[j] then
             vis[j+a[i]]:=true;
        end;
      writeln(ans);
    end;
close(input);
close(output);
end.