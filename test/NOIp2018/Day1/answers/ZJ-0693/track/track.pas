var n,m,i,j,t,k,ans,q1,q2,qq1,qq2,lst,max1,max2,maxx,tot,ts,s,root1,root2,root:longint;
    flag,p1,p2:boolean;
    alpha:double;
    head,x,y,z,_in:array[0..200001] of longint;
    vis:array[0..200001] of boolean;
    ed:Array[0..200001] of record
                             y,w,nxt:longint;
                           end;
function max(x,y:longint):longint;
begin
  if x>y then
    exit(x);
  exit(y);
end;
function min(x,y:longint):longint;
begin
  if x<y then
    exit(x);
  exit(y);
end;
procedure add(x,y,z:longint);
begin
  inc(tot);
  ed[tot].y:=y;
  ed[tot].w:=z;
  ed[tot].nxt:=head[x];
  head[x]:=tot;
end;
procedure dfs(x,fa,k:longint);
var i,v:longint;
begin
  if k=m then
    exit;
  i:=head[x];
  while i>0 do
    begin
      v:=ed[i].y;
      if v<>fa then
        begin
          ts:=ts-ed[i].w;
          dfs(v,x,k+1);
        end;
      i:=ed[i].nxt;
    end;
end;
procedure sort(l,r:longint);
var i,j,xx,yy:longint;
begin
  i:=l;j:=r;xx:=z[(l+r) div 2];
  repeat
    while z[i]>xx do
      inc(i);
    while xx>z[j] do
      dec(j);
    if not(i>j) then
      begin
        yy:=x[i];x[i]:=x[j];x[j]:=yy;
        yy:=y[i];y[i]:=y[j];y[j]:=yy;
        yy:=z[i];z[i]:=z[j];z[j]:=yy;
        inc(i);j:=j-1;
      end;
  until i>j;
  if l<j then
    sort(l,j);
  if i<r then
    sort(i,r);
end;
function dfs1(x,fa:longint):longint;
var i,t,v:longint;
begin
  i:=head[x];
  t:=0;
  while i>0 do
    begin
      v:=ed[i].y;
      if v<>fa then
        t:=max(t,dfs1(v,x)+ed[i].w);
      i:=ed[i].nxt;
    end;
  exit(t);
end;
begin
assign(input,'track.in');reset(input);
assign(output,'track.out');rewrite(output);
  read(n,m);
  for i:=1 to n-1 do
    begin
      read(x[i],y[i],z[i]);
      if x[i]<>1 then
        p1:=true;
      if y[i]<>x[i]+1 then
        p2:=true;
      inc(_in[x[i]]);inc(_in[y[i]]);
      add(x[i],y[i],z[i]);
      add(y[i],x[i],z[i]);
      s:=s+z[i];
      maxx:=max(maxx,z[i]);
    end;
  if m=1 then
    begin
      for i:=1 to n-1 do
        if _in[i]=1 then
          ans:=max(ans,dfs1(i,0));
      writeln(ans);
      close(input);
      close(output);
      halt;
    end;
  if not p2 then                                        //202
    begin
      root1:=0;root2:=0;
      for i:=1 to n do
        if _in[i]=1 then
          if root1=0 then
            root1:=i
          else
            begin
              root2:=i;
              break;
            end;
      writeln(s div m-1);
      close(input);
      close(output);
      halt;
    end;
  sort(1,n-1);
  alpha:=(maxx/(m-1));
  while alpha>=1 do
    alpha:=alpha/10;
  if not p1 then                                        //?
    begin
      for i:=1 to n do
        if _in[i]=n-1 then
          root:=i;
      if n<100 then
        begin
          lst:=maxlongint;
          for k:=1 to m do
            begin
              ans:=0;
              for i:=1 to n-1 do
                for j:=i+1 to n do
                  if (head[i]<>head[j]) and (i<>root) and (j<>root) then
                  if (ed[head[i]].w+ed[head[j]].w<=lst) and not vis[i] and not vis[j] then
                    if ed[head[i]].w+ed[head[j]].w>s*alpha*2 then
                      begin
                        ans:=ed[head[i]].w+ed[head[j]].w;
                        q1:=i;q2:=j;
                      end;
              vis[q1]:=true;vis[q2]:=true;
              lst:=ans;
            end;
          writeln(ans);
        end
      else
        begin
          k:=1;flag:=false;
          for i:=1 to n-1 do
            begin
              j:=i+1;
              while z[i]*alpha>z[j] do
                begin
                  inc(k);
                  inc(j);
                  if k=m then
                    begin
                      flag:=true;
                      break;
                    end;
                end;
              if flag then
                break;
            end;
          writeln(z[i]+z[j]);
        end;
      close(input);
      close(output);
      halt;
    end;
  writeln(trunc(s*alpha)-z[m]+2);
close(input);
close(output);
end.
