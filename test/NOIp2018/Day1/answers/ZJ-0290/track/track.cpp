// Sooke bless me.
// LJC00118 bless me.
#include<bits/stdc++.h>

#define ll long long
#define INF 2147483647

int Inp(){
	char c = getchar();
	int Neg = 1;
	while(c < '0' || c > '9'){
		c = getchar();
		if(c == '-')
			 Neg = -1;
	}
	int Sum = 0;
	while(c >= '0' && c <= '9'){
		Sum = Sum * 10 + c - '0';
		c = getchar();
	}
	return Sum * Neg;
}

int Head[50010];
int End[100010];
int Value[100010];
int Next[100010];
int Cou = 0;
int Deepth[50010];

void Link(int a, int b, int v){
	Next[++Cou] = Head[a];
	Head[a] = Cou;
	End[Cou] = b;
	Value[Cou]= v;
}

void Dfs(int Cur, int Last){
	for(int x = Head[Cur]; x != -1; x = Next[x]){
		if(End[x] == Last)
			continue;
		Deepth[End[x]] = Deepth[Cur] + Value[x];
		Dfs(End[x], Cur);
	}
}
int Dis_Next[50010];
int n, m;

bool Check(int Lim){ // 真·树链剖分 
	int Sum = 0;
	int Cnt = 0;
	for(int i = 1; i <= n; i++){
		Sum += Dis_Next[i];
		if(Sum >= Lim){
			Cnt++;
			Sum = 0;
		}
	}
	return Cnt >= m;
}

int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	memset(Head, -1, sizeof(Head));
	n = Inp();
	m = Inp();
	for(int i = 1; i < n; i++){
		int x = Inp();
		int y = Inp();
		int w = Inp();
		if(y == x + 1)
			Dis_Next[x] = w;
		Link(x, y, w);
		Link(y, x, w);
	}
	if(m == 1){ // m = 1 -> 直径 
		Dfs(1, -1);
		int Max = 0;
		int Maxk;
		for(int i = 1; i <= n; i++){
			if(Deepth[i] > Max){
				Max = Deepth[i];
				Maxk = i;
			}
		}
		Deepth[Maxk] = 0;
		Dfs(Maxk, -1);
		int Ans = 0;
		for(int i = 1; i <= n; i++)
			Ans = std::max(Ans, Deepth[i]);
		printf("%d", Ans);
	} else if(m == n - 1){ // m = n - 1 -> 最短的边 
		int Min = INF;
		for(int i = 1; i <= Cou; i++)
			Min = std::min(Min, Value[i]);
		printf("%d", Min);
	} else { // 链 -> 二分 + 贪心 
		int l = 1;
		int r = 1e9;
		while(l < r){
			int Mid = (l + r + 1) >> 1;
			if(Check(Mid))
				l = Mid;
			else
				r = Mid - 1;
		}
		 printf("%d", l);
	}
}
