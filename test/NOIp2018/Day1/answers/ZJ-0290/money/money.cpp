// Sooke bless me.
// LJC00118 bless me.
#include<bits/stdc++.h>

#define ll long long
#define INF 2147483647

int Inp(){
	char c = getchar();
	int Neg = 1;
	while(c < '0' || c > '9'){
		c = getchar();
		if(c == '-')
			 Neg = -1;
	}
	int Sum = 0;
	while(c >= '0' && c <= '9'){
		Sum = Sum * 10 + c - '0';
		c = getchar();
	}
	return Sum * Neg;
}

int a[110];
int f[100000];

int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	for(int t = Inp(); t >= 1; t--){
		memset(f, 0, sizeof(f));
		int n = Inp();
		for(int i = 1; i <= n; i++)
			a[i] = Inp();
		f[0] = 1;
		for(int i = 0; i <= 30000; i++){
			if(f[i] == 0)
				continue;
			for(int j = 1; j <= n; j++)
				f[i + a[j]] += f[i];
		}
		int Ans = n;
		for(int i = 1; i <= n; i++){
			if(f[a[i]] > 1)
				Ans--;
		}
		printf("%d\n", Ans);
	}
}
