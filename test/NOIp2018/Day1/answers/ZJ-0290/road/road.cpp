// Sooke bless me.
// LJC00118 bless me.
#include<bits/stdc++.h>

#define ll long long
#define INF 2147483647

int Inp(){
	char c = getchar();
	int Neg = 1;
	while(c < '0' || c > '9'){
		c = getchar();
		if(c == '-')
			 Neg = -1;
	}
	int Sum = 0;
	while(c >= '0' && c <= '9'){
		Sum = Sum * 10 + c - '0';
		c = getchar();
	}
	return Sum * Neg;
}

int d[100010];
int a[100010];

int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	int n = Inp();
	d[0] = 0;
	for(int i = 1; i <= n; i++){
		d[i] = Inp();
		a[i] = d[i] - d[i - 1];
	}
	ll Ans = 0;
	for(int i = 1; i <= n; i++)
		Ans += abs(a[i]);
	Ans += d[n];
	printf("%lld", Ans / 2);
}

