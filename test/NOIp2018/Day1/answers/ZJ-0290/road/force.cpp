#include<bits/stdc++.h>

#define ll long long
#define INF 2147483647

int Inp(){
	char c = getchar();
	int Neg = 1;
	while(c < '0' || c > '9'){
		c = getchar();
		if(c == '-')
			 Neg = -1;
	}
	int Sum = 0;
	while(c >= '0' && c <= '9'){
		Sum = Sum * 10 + c - '0';
		c = getchar();
	}
	return Sum * Neg;
}

int d[100010];

int main(){
	freopen("road.in", "r", stdin);
	freopen("force.out", "w", stdout);
	int n = Inp();
	for(int i = 1; i <= n; i++)
		d[i] = Inp();
	int Ans = 0;
	while(1){
		int Cur = 1;
		while(Cur <= n && d[Cur] == 0)
			Cur++;
		if(Cur > n)
			break;
		int Last = Cur;
		int Min = INF;
		while(Last <= n && d[Last] != 0)
			Min = std::min(Min, d[Last++]);
		
		for(int i = Cur; i < Last; i++){
			d[i] -= Min;
		}
		
		Ans += Min;
	}
	printf("%d", Ans);
}

