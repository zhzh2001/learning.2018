#include<bits/stdc++.h>

#define ll long long
#define INF 2147483647

int Inp(){
	char c = getchar();
	int Neg = 1;
	while(c < '0' || c > '9'){
		c = getchar();
		if(c == '-')
			 Neg = -1;
	}
	int Sum = 0;
	while(c >= '0' && c <= '9'){
		Sum = Sum * 10 + c - '0';
		c = getchar();
	}
	return Sum * Neg;
}

int main(){
	srand((unsigned)time(NULL));
	freopen("road.in", "w", stdout);
	int n = 10000;
	printf("%d\n", n);
	for(int i = 1; i <= n; i++){
		printf("%d ", rand() % 10000);
	}
}

