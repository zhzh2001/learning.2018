#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int T,n,tot;
int a[5000];
bool b[2000000];
bool bb[2000000];

bool pd(int x,int y,int z){
	int u=a[x];
	int v=a[y];
	int w=a[z];
	for (int i=0;i<=w/u;i++)
		for (int j=0;j<=w/v;j++)
			if (i*u+j*v==w) return 1;
	return false;	
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		memset(b,0,sizeof(b));
		memset(a,0,sizeof(a));
		scanf("%d",&n);
		for (int i=1;i<=n;i++)scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		int maxx=a[n];
		for (int i=1;i<n;i++)
			for (int j=i+1;j<=n;j++)
				if (a[j]%a[i]==0) b[j]=1;
		for (int i=1;i<=n-2;i++)
			for (int j=i+1;j<n;j++)
				for (int k=j+1;k<=n;k++)
					if (pd(i,j,k)){ //判断a[i] a[j]  是否可以表示出a[k] 
							b[k]=1;
						}
		tot=0;
		for (int i=1;i<=n;i++)
			if (!b[i]) {
				tot++;
			}
		printf("%d\n",tot);
	}
	return 0;
}
