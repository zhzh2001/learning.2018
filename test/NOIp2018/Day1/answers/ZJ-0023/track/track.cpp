#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
using namespace std;
#define N 200000

bool ch[N],vi[N];

int n,m,tot;
int save[N];
int u,v,w,maxx;
vector<int>son[N];
int anc[N][100];
struct Edge{
	int to,next,w;
}edge[N];
int head[N],fa[N],max1,max2,val[N],depth[N];
bool vis[N];

void add(int u,int v,int w){
	tot++;
	edge[tot].next=head[u];edge[tot].to=v;
	edge[tot].w=w;head[u]=tot;	
}

void dfs(int x){
	for (int i=head[x];i;i=edge[i].next){
		int p=edge[i].to;
		if (!vis[p]){
			vis[p]=1; fa[p]=x; son[x].push_back(p);
			depth[p]=depth[x]+1;
			val[p]=edge[i].w;
			dfs(p);
		}
	}	
}

int lca(int x,int y){
	if (depth[x]<depth[y]) swap(x,y);
	while (depth[x]>depth[y]) x=fa[x];
	if (x==y) return y;
	while (x!=y){x=fa[x];y=fa[y];}
	return x;	
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	tot=0;
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&u,&v,&w);
		add(u,v,w);add(v,u,w);
	}
	vis[1]=1;depth[1]=1;
	dfs(1);
	if (m==1){
		maxx=0;
		int qq=0;
		for (int i=1;i<=n;i++)
			if (!(son[i].size())){qq++;save[qq]=i;}
		for (int i=1;i<qq;i++)
			for (int j=i+1;j<=qq;j++)
				{
					int l=lca(save[i],save[j]);
					int vv=0;
					int pp=save[i];
					while (pp!=l) {vv+=val[pp];pp=fa[pp];}
					pp=save[j];
					while (pp!=l) {vv+=val[pp];pp=fa[pp];}
					maxx=max(vv,maxx);
				}
		printf("%d",maxx);
		return 0;
	}
	return 0;
}
