#include<bits/stdc++.h>
using namespace std;
int f[25009];
int n;
int t;
int num[109];
int ans;
int read()
{
	int tot=0,fs=1;
	char ch;
	while((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if(ch=='-') ch=getchar(),fs=-1;
	while(ch>='0'&&ch<='9') tot=tot*10+ch-'0',ch=getchar();
	return tot*fs;	
}
void write(int x)
{
	if(x<0) x*=-1,putchar('-');
	if(x>9) write(x/10);
	putchar('0'+x%10);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read();
	while(t--)
	{
		memset(f,0,sizeof(f));
		n=read();
		for(int i=1;i<=n;i++)
		{
			num[i]=read();
			f[num[i]]=2;
		}
		sort(num+1,num+n+1);
		for(int i=1;i<n;i++)
		{
			for(int j=1;j<=num[n];j++)
			{
				if(j+num[i]>num[n]) break;
				if(f[j])
				{
					if(f[j+num[i]]==0) f[j+num[i]]=1;
					else if(f[j+num[i]]==2) f[j+num[i]]=3;
				}
			}
		}
		ans=n;
		for(int i=1;i<=n;i++)
		{
			if(f[num[i]]==3) ans--;
		}
		printf("%d\n",ans);
	}
}
