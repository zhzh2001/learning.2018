#include<bits/stdc++.h>
using namespace std;
long long n,m;
long long head[50009];
long long to[100009],nt[100009],val[100009],bh;
long long sum;
long long ans;
long long yq;
long long f[50009];
long long num[50009],lennum;
long long mark[50009],lenmark;
long long jsq;
long long read()
{
	long long tot=0,fs=1;
	char ch;
	while((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if(ch=='-') ch=getchar(),fs=-1;
	while(ch>='0'&&ch<='9') tot=tot*10+ch-'0',ch=getchar();
	return tot*fs;	
}
void write(long long x)
{
	if(x<0) x*=-1,putchar('-');
	if(x>9) write(x/10);
	putchar('0'+x%10);
}
void ad(long long u,long long v,long long w)
{
	bh++;
	nt[bh]=head[u];
	head[u]=bh;
	to[bh]=v;
	val[bh]=w;
}
long long query(long long k)
{
	long long l=1,r=lennum,mid,fin=lennum+1;
	while(l<=r)
	{
		mid=(l+r)>>1;
		if(num[mid]+k>=yq) fin=mid,r=mid-1;
		else l=mid+1;
	}
	return fin;
}
void dfs(long long now,long long fa)
{
	bool kx=0;
	for(long long i=head[now];i;i=nt[i])
	{
		if(to[i]!=fa) kx=1,dfs(to[i],now);
	}
	if(kx)
	{
		long long mx=0;
		lennum=0;
		lenmark++;
		for(long long i=head[now];i;i=nt[i])
		{
			if(to[i]!=fa)
			{
				if(f[to[i]]+val[i]>=yq) jsq++;
				else num[++lennum]=f[to[i]]+val[i];
			}
		}
		sort(num+1,num+lennum+1);
		for(long long i=1;i<=lennum;i++)
		{
			if(mark[i]!=lenmark)
			{
				bool flg=0;
				long long ls=query(num[i]);
				for(long long j=ls;j<=lennum;j++)
				{
					if(mark[j]!=lenmark&&j!=i)
					{
						flg=1;
						mark[j]=lenmark;
						mark[i]=lenmark;
						jsq++;
						break;
					}
				}
				if(!flg) mx=max(mx,num[i]);
			}
		}
		f[now]=mx;
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(long long i=1;i<n;i++)
	{
		long long u=read(),v=read(),w=read();
		ad(u,v,w);
		ad(v,u,w);
		sum+=w;
	}
	long long l=1,r=sum/m;
	while(l<=r)
	{
		//cout<<l<<" "<<r<<endl;
		yq=(l+r)/2;
		jsq=0;
		memset(f,0,sizeof(f));
		dfs(1,0);
		if(jsq>=m) l=yq+1,ans=yq;
		else r=yq-1;
	}
	printf("%lld\n",ans);
	return 0;
}
/*
7 1
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7
*/
/*
9 3
1 2 6
2 3 3
3 4 5
4 5 10
6 2 4
7 2 9
8 4 7
9 4 4
*/
