#include<bits/stdc++.h>
using namespace std;
vector<int> vec[10009];
int mn[200000][20];
int nb[20];
int lg[200000];
int ql[10000000],qr[10000000],qk[10000000],head,tail;
int n;
long long ans;
int read()
{
	int tot=0,fs=1;
	char ch;
	while((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if(ch=='-') ch=getchar(),fs=-1;
	while(ch>='0'&&ch<='9') tot=tot*10+ch-'0',ch=getchar();
	return tot*fs;	
}
void write(int x)
{
	if(x<0) x*=-1,putchar('-');
	if(x>9) write(x/10);
	putchar('0'+x%10);
}
int queryl(int x,int k)
{
	int l=0,r=vec[x].size()-1,mid,fin;
	while(l<=r)
	{
		mid=(l+r)>>1;
		if(vec[x][mid]>=k) fin=mid,r=mid-1;
		else l=mid+1;
	}
	return fin;
}
int queryr(int x,int k)
{
	int l=0,r=vec[x].size()-1,mid,fin;
	while(l<=r)
	{
		mid=(l+r)>>1;
		if(vec[x][mid]<=k) fin=mid,l=mid+1;
		else r=mid-1;
	}
	return fin;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	nb[0]=1;
	for(int i=1;i<=17;i++)
	{
		nb[i]=nb[i-1]*2;
	}
	for(int i=2;i<=199999;i++)
	{
		lg[i]=lg[i-1];
		if(nb[lg[i]+1]==i) ++lg[i];
	}
	n=read();
	for(int i=1;i<=n;i++)
	{
		mn[i][0]=read();
		vec[mn[i][0]].push_back(i);
	}
	
	for(int j=1;j<=lg[n];j++)
	{
		for(int i=1;i<=n+1-nb[j];i++)
		{
			mn[i][j]=min(mn[i][j-1],mn[i+nb[j-1]][j-1]);
		}
	}
	for(int i=1;i<=n;i++)
	{
		while(mn[i][0]==0&&i<=n) i++;
		if(i>n) break;
		tail++;
		ql[tail]=i,qr[tail]=i-1;
		while(mn[i][0]!=0&&i<=n) 
		{
			qr[tail]++;
			i++;	
		}	
	}
	while(head<tail)
	{
		head++;
		int ls=min(mn[ql[head]][lg[qr[head]-ql[head]+1]],mn[qr[head]-nb[lg[qr[head]-ql[head]+1]]+1][lg[qr[head]-ql[head]+1]]);
		ans+=ls-qk[head];
		int l=queryl(ls,ql[head]),r=queryr(ls,qr[head]);
		//cout<<ql[head]<<" "<<qr[head]<<" "<<qk[head]<<" "<<l<<" "<<r<<endl;
		if(ql[head]<vec[ls][l])
		{
			tail++;
			ql[tail]=ql[head];
			qr[tail]=vec[ls][l]-1;
			qk[tail]=ls;
		}
		while(l<r)
		{
			if(vec[ls][l]<vec[ls][l+1])
			{
				tail++;
				ql[tail]=vec[ls][l]+1;
				qr[tail]=vec[ls][l+1]-1;
				qk[tail]=ls;	
			}
			l++;
		}
		if(vec[ls][r]<qr[head])
		{
			tail++;
			ql[tail]=vec[ls][r]+1;
			qr[tail]=qr[head];
			qk[tail]=ls;
		}
	}
	printf("%lld",ans);
}
/*
6 
4 3 2 5 3 5
*/
