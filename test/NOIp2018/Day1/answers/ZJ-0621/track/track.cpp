#include<bits/stdc++.h>
using namespace std;
int read()
{
	int x=0,w=0;
	char ch=0;
	while(!isdigit(ch)){w|=ch=='-';ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return w? -x:x;
}
struct edge{
	int to,next,w;
}e[200010];
struct ss
{
	int from,to,w;
}s[50010];
int linkk[200010],len=1;
void insert(int u,int v,int w)
{
	e[++len].to=v,e[len].next=linkk[u],linkk[u]=len,e[len].w=w;
	e[++len].to=u,e[len].next=linkk[v],linkk[v]=len,e[len].w=w;
}
int n,m;
int dis[50010],answer=0;
int d[50010];
bool vis[50010];
void dfs(int x,int fa)
{
	vis[x]=1;
	for(int i=linkk[x];i;i=e[i].next)
	{
		int to=e[i].to;
		if(vis[to]) continue;
		if(to==fa) continue;
		dfs(to,x);
		answer=max(answer,d[x]+d[to]+e[i].w);
		d[x]=max(d[x],d[to]+e[i].w);
	}
}
int bel[20],c[20];
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<n;++i)
	{
		s[i].from=read(),s[i].to=read(),s[i].w=read();
		insert(s[i].from,s[i].to,s[i].w);
	}
	if(m==1)
	{
		dfs(1,0);
		cout<<answer<<endl;
		return 0;
	}
	else
	{
		srand(time(NULL));
		int minn=2012030;
		for(int i=1;i<n;++i) minn=min(minn,s[i].w);
		cout<<minn<<endl; 
	}
	return 0;	
}
