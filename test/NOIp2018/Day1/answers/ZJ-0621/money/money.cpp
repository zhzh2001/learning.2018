#include<bits/stdc++.h>
using namespace std;
int read()
{
	int x=0,w=0;
	char ch=0;
	while(!isdigit(ch)){w|=ch=='-';ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return w? -x:x;
}
int n,t;
int a[110];
bool f[25010],vis[110];
bool cmp(int x,int y)
{
	return x>y;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read();
	while(t--)
	{
		memset(vis,1,sizeof(vis));
		memset(f,0,sizeof(f));
		n=read(); int maxn=0;
		for(int i=1;i<=n;++i) a[i]=read(),maxn=max(maxn,a[i]);
		sort(a+1,a+1+n,cmp);
		for(int i=1;i<=n;++i)
		{
			for(int j=1;j<=n;++j)
				if(i!=j) if(a[j]%a[i]==0) vis[j]=0;
		}
		for(int sel=1;sel<=n;++sel)
		{
			if(!vis[sel]) continue;
			memset(f,0,sizeof(f));
			f[0]=1;
			for(int i=1;i<=n;++i)
				if(i!=sel)
				{
					for(int j=a[i];j<=maxn;++j)
						f[j]|=f[j-a[i]];
				}
			if(f[a[sel]]) vis[sel]=0,sel=0;
		}
		int cnt=0;
		for(int i=1;i<=n;++i)
			if(vis[i]) ++cnt;
		printf("%d\n",cnt);
	}
	return 0;
}
