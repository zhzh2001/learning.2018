#include<cstdio>
#include<algorithm>
using namespace std;

int n, a[101], t, flag, f[101];

void dfs(int x, int y, int z)
{
	int w = 0;
	while (1)
	{
		if (flag) return;
		if (a[y] >= x) return;
		if (w * a[y] > x) return;
		if ((z + w * a[y]) == x)
		{
			flag = 1;
			return;
		}
		if (z + w * a[y] > x) return;
		dfs (x, y + 1, z + w * a[y]);
		w++;
	}
}

int main()
{
	freopen ("money.in", "r", stdin);
	freopen ("money.out", "w", stdout);
	
	scanf ("%d", &t);
	while (t--)
	{
		scanf ("%d", &n);
		int tmp = n;
		for (int i = 1; i <= n; i++) scanf ("%d", &a[i]);
		sort (a + 1, a + n + 1);
		for (int i = 2; i <= n; i++)
		{
			flag = 0;
			dfs (a[i], 1, 0);
			if (flag) tmp--;
		}
		printf ("%d\n", tmp);
	}
	
	return 0;
}
