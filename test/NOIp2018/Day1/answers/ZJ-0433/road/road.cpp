#include<cstdio>
using namespace std;

int n, d[100001], cnt, ans;

int main()
{
	freopen ("road.in", "r", stdin);
	freopen ("road.out", "w", stdout);
	
	scanf ("%d", &n);
	for (int i = 1; i <= n; i++) scanf ("%d", &d[i]);
	int flag1 = 1;
	while (cnt != n)
	{
		int minn = 10001, flag = n;
		for (int j = flag1; j <= n; j++)
		{
			if (!d[j] && minn == 10001)
			{
				flag1 = j + 1;
				continue;
			}
			if (!d[j] && minn != 10001)
			{
				flag = j - 1;
				break;
			}
			if (d[j] < minn)
			{
				minn = d[j];
			}
		}
		ans += minn;
		for (int j = flag1; j <= flag; j++)
		{
			d[j] -= minn;
			if (!d[j]) cnt++;
		}
	}
	
	printf ("%d", ans);
	
	return 0;
}
