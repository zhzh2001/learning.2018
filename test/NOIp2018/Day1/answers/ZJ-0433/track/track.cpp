#include<cstdio>
using namespace std;

int n, m, map[1001][1001], maxn;
bool bmap[1001][1001];

void dfs (int x, int sum)
{
	if (sum > maxn) maxn = sum;
	for (int i = 1; i <= n; i++)
	{
		if (bmap[x][i]) continue;
		if (map[x][i])
		{
			bmap[x][i] = true;
			bmap[i][x] = true;
			dfs (i, sum + map[x][i]);
			bmap[x][i] = false;
			bmap[i][x] = false;
		}
	}
}

int main()
{
	freopen ("track.in", "r", stdin);
	freopen ("track.out", "w", stdout);
	
	scanf ("%d%d", &n, &m);
	for (int i = 1; i <= n - 1; i++)
	{
		int a, b, c;
		scanf ("%d%d%d", &a, &b, &c);
		map[a][b] = c;
		map[b][a] = c;
	}
	
	for (int i = 1; i <= n; i++) dfs (i, 0);
	
	printf ("%d", maxn);
	
	return 0;
}
