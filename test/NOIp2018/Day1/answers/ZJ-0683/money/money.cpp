#include<bits/stdc++.h>
using namespace std;
int dp[25010],n,ans;
int a[110];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--){
		scanf("%d",&n);
		ans=n;
		memset(dp,0,sizeof dp);
		dp[0]=1;
		for(int i=1;i<=n;i++)scanf("%d",a+i);
		sort(a+1,a+1+n);
		for(int i=1;i<=n;i++){
			int now=a[i];
			if(dp[now])ans--;
			else{
				for(int i=now;i<=25000;i++)dp[i]|=dp[i-now];
			}
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
