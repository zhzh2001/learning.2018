#include<bits/stdc++.h>
using namespace std;
int dp1[50010],dp2[50010];
int n,m;
int l,r,ans;
struct edge{
	int v,w,next;
}e[100010];
int head[50010],tot,fa[50010],w[50010];
struct node{
	int size,lson,rson,key;
}t[50010];
int root[50010];
inline void up(int x){
	t[x].size=t[t[x].lson].size+t[t[x].rson].size+1;
}
int merge(int x,int y){
	if(!x||!y)return x+y;
	else if(rand()%(t[x].size+t[y].size)<t[x].size)return t[x].rson=merge(t[x].rson,y),up(x),x;
	return t[y].lson=merge(x,t[y].lson),up(y),y;
}
void split(int now,int k,int &x,int &y){
	if(now==0)x=y=0;
	else if(t[now].key<=k)x=now,split(t[now].rson,k,t[x].rson,y),up(x);
	else y=now,split(t[now].lson,k,x,t[y].lson),up(y);
}
void split_k(int now,int k,int &x,int &y){
	if(now==0)x=y=0;
	else if(t[t[now].lson].size>=k)y=now,split_k(t[now].lson,k,x,t[y].lson),up(y);
	else x=now,split_k(t[now].rson,k-t[t[now].lson].size-1,t[x].rson,y),up(x);
}
void addedge(int u,int v,int w){
	e[++tot].v=v;
	e[tot].w=w;
	e[tot].next=head[u];
	head[u]=tot;
}
void init(int x,int f){
	fa[x]=f;
	for(int i=head[x];i;i=e[i].next){
		if(e[i].v!=f)w[e[i].v]=e[i].w,init(e[i].v,x);
	}
}
void dfs(int x,int k){
	root[x]=0;
	int a,b,c,d;
	dp1[x]=0;
	dp2[x]=0;
	for(int i=head[x];i;i=e[i].next){
		if(e[i].v==fa[x])continue;
		dfs(e[i].v,k);
		split(root[x],dp1[e[i].v],a,b);
		root[x]=merge(merge(a,e[i].v),b);
		dp2[x]+=dp2[e[i].v];
	}
	while(root[x]){
		split_k(root[x],1,a,root[x]);
		a=t[a].key;
		split(root[x],k-a-1,b,c);
		if(c){
			split_k(c,1,d,c);
			dp2[x]++;
		}else{
			dp1[x]=a;
		}
		root[x]=merge(b,c);
	}
	dp1[x]+=w[x];
	if(dp1[x]>=k)dp1[x]=0,dp2[x]++;
	t[x].key=dp1[x];
	t[x].lson=t[x].rson=0;
	t[x].size=1;
}
inline int check(int k){
	dfs(1,k);
	if(dp2[1]>=m)return 1;
	else return 0;
}
int main(){
	srand(20050218);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		addedge(u,v,w);
		addedge(v,u,w);
	}
	r=500000000;
	init(1,0);
	while(l<=r){
		int mid=(l+r)>>1;
		if(check(mid))l=mid+1,ans=mid;
		else r=mid-1;
	}
	printf("%d",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
