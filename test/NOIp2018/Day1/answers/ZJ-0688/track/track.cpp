#include <bits/stdc++.h>
using namespace std;
#define N 100100

struct edge {int nex,to,dis;}e[N<<1];
int n,m,len,cnt,sum,l,r,ans;
int head[N],c[N],vis[N],a[N],p[N],fa[N];
vector<int> G[N];

void add(int x,int y,int z) {
	e[++cnt].to=y;
	e[cnt].dis=z;
	e[cnt].nex=head[x];
	head[x]=cnt;
}

int find(int x) {
	if (x==fa[x]) return x;
	return fa[x]=find(fa[x]);
}

int dfs(int x,int father) {
	for (int i=head[x];i;i=e[i].nex) {
		int v=e[i].to;
		if (v==father) continue;
		int tmp=dfs(v,x)+e[i].dis;
		G[x].push_back(tmp);
	}
	int tot=G[x].size(),now=tot;
	for (int i=0;i<tot;i++) c[i+1]=G[x][i];
	sort(c+1,c+tot+1);
	for (;c[now]>=len;now--) cnt++;
	
	for (int i=1;i<=now+1;i++) vis[i]=0,fa[i]=i;
	for (int i=1;i<=now;i++) if (!vis[i]) {
		int t=lower_bound(c+1,c+now+1,len-c[i])-c;
		if (t>now) continue;
		if (t==i) t=find(t+1); else t=find(t);
		if (t<=now && t!=i) vis[i]=vis[t]=1,fa[i]=i+1,fa[t]=t+1,cnt++;
	}
	for (int i=now;i;i--) if (!vis[i]) return c[i];
	return 0;

}

bool check(int num) {
	memset(vis,0,sizeof(vis));
	memset(p,0,sizeof(p));
	for (int i=1;i<=n;i++) G[i].clear();
	cnt=0; len=num;
	dfs(1,0);
	if (cnt>=m) return 1;
	else return 0;
}

int x,y,z;
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++) {
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z); add(y,x,z);
		sum+=z;
	}
	l=1; r=sum;
	while (l<=r) {
		int mid=(l+r)>>1;
		if (check(mid)) ans=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
