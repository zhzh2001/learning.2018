#include <bits/stdc++.h>
using namespace std;
#define N 26000

int T,n,mx,ans;
int a[N],vis[N];

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		memset(vis,0,sizeof(vis)); vis[0]=1;
		mx=ans=0;
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]),mx=max(mx,a[i]);
		sort(a+1,a+n+1);
		for (int i=1;i<=n;i++) {
			if (vis[a[i]]) continue;
			ans++;
			for (int j=0;j<=mx-a[i];j++)
				if (vis[j]) vis[j+a[i]]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
