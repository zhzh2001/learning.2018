#include <bits/stdc++.h>
using namespace std;
#define N 101000

int n;
int a[N];
long long ans;

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<=n;i++) ans+=max(a[i]-a[i-1],0);
	printf("%lld\n",ans);
	return 0;
}
