#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <algorithm>
using namespace std;
const int MAXN = 105;
int n, a[MAXN];
bool f[25005];
void doit() {
	scanf("%d", &n);
	for (int i = 1; i <= n; ++i) scanf("%d", &a[i]);
	sort(a + 1, a + n + 1);
	memset(f, false, sizeof(f));
	int ans = 0;
	f[0] = true;
	for (int i = 1; i <= n; ++i) {
		if (f[a[i]]) continue;
		++ans;
		for (int j = a[i]; j <= 25000; ++j)
			f[j] = f[j - a[i]] || f[j];
	}
	printf("%d\n", ans);
}
int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int t;
	scanf("%d", &t);
	while (t--) doit();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
