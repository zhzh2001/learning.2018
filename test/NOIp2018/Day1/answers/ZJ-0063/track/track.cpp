#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int MAXN = 50005;
struct Edge {
	int to, nxt, w;
} edge[MAXN << 1];
int fir[MAXN], ecnt, n, m, f[MAXN], g[MAXN];
bool flag;
void addedge(int u, int v, int w) {
	edge[++ecnt].to = v;
	edge[ecnt].nxt = fir[u];
	edge[ecnt].w = w;
	fir[u] = ecnt;
}
void dfs(int u, int fa, int mid) {
	if (flag) return;
	vector <int> nod;
	for (int e = fir[u]; e && !flag; e = edge[e].nxt) {
		int v = edge[e].to;
		if (v == fa) continue;
		dfs(v, u, mid);
		f[u] += f[v];
		if (g[v] + edge[e].w >= mid) ++f[u];
		else nod.push_back(g[v] + edge[e].w);
	}
	sort(nod.begin(), nod.end());
	int siz = nod.size(), s = 0, i = 0, j = siz - 1;
	while (i < j) {
		if (nod[i] + nod[j] >= mid) {
			++s;
			++i, --j;
		} else
			++i;
	}
	f[u] += s;
	int left = 0, right = siz - 1, ret = -1;
	while (left <= right) {
		int mi = (left + right) >> 1;
		int ns = 0, i = 0, j = siz - 1;
		while (true) {
			if (i == mi) ++i;
			if (j == mi) --j;
			if (i >= j) break;
			if (nod[i] + nod[j] >= mid) {
				++ns;
				++i, --j;
			} else
				++i;			
		}
		if (ns == s) {
			ret = mi;
			left = mi + 1;
		} else
			right = mi - 1;
	}
	if (ret != -1) g[u] = nod[ret];
	if (f[u] >= m) flag = true;
}
bool okay(int mid) {
	memset(f, 0, sizeof(f));
	memset(g, 0, sizeof(g));
	flag = false;
	dfs(1, 0, mid);
	return flag;
}
int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i < n; ++i) {
		int u, v, w;
		scanf("%d%d%d", &u, &v, &w);
		addedge(u, v, w);
		addedge(v, u, w);
	}
	int left = 0, right = 500000000, ret = 0;
	while (left <= right) {
		int mid = (left + right) >> 1;
		if (okay(mid)) {
			ret = mid;
			left = mid + 1;
		} else
			right = mid - 1;
	}
	printf("%d\n", ret);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
