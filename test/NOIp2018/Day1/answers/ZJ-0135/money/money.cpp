#include<cstdio>
#include<algorithm>
using namespace std;
int n,Q,ans;
int a[105],f[25005];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x*f;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	Q=read();
	while (Q--)
	{
		n=read();
		for (int i=1; i<=n; i++) a[i]=read();
		sort(a+1,a+n+1);
		for (int i=1; i<=a[n]; i++) f[i]=0;
		f[0]=1;
		ans=0;
		for (int i=1; i<=n; i++)
		{
			if (f[a[i]]) continue;
			ans++;
			for (int j=a[i]; j<=a[n]; j++)
				f[j]|=f[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
