#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;
const int N=5e4+5;
int n,m,l,r,mid,ans,mx,u,v,w,num;
int head[N],point[N<<1],len[N<<1],Next[N<<1];
int mxl[N],f[N],a[N],b[N],p[N];
vector<int> G[N];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x*f;
}
void adde(int u,int v,int l)
{
	point[++num]=v;
	len[num]=l;
	Next[num]=head[u];
	head[u]=num;
}
void dfs(int now,int pre,int x)
{
	int tot=0;
	for (int i=head[now]; i; i=Next[i])
	{
		int v=point[i];
		if (v==pre) continue;
		dfs(v,now,x);
		f[now]+=f[v];
		G[now].push_back(mxl[v]+len[i]);
	}
	tot=G[now].size();
	for (int i=1; i<=tot; i++)
		a[i]=G[now][i-1],p[i]=0;
	sort(a+1,a+tot+1);
	int tb=0;
	for (int i=1; i<=tot; i++)
		if (a[i]>=x) f[now]++,p[i]=1;
		else
		{
			if (tb==0 || b[tb]+a[i]<x) b[++tb]=a[i];
			else
			{
				int loc=lower_bound(b+1,b+tb+1,x-a[i])-b;
				for (int j=loc; j<=tb; j++) b[j]=b[j+1];
				tb--;
				f[now]++;
			}
		}
	mxl[now]=b[tb];
}
int check(int x)
{
	for (int i=1; i<=n; i++) mxl[i]=f[i]=0,G[i].clear();
	dfs(1,0,x);
	if (f[1]>=m) return 1;
	else return 0;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(); m=read();
	for (int i=1; i<=n-1; i++)
	{
		u=read(); v=read(); w=read();
		adde(u,v,w); adde(v,u,w);
		mx+=w;
	}
	l=1; r=mx;
	while (l<=r)
	{
		mid=(l+r)>>1;
		if (check(mid)) 
		{
			ans=mid;
			l=mid+1;
		}
		else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
