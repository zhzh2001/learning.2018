#include<cstdio>
int n,ans;
int d[100005];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x*f;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (int i=1; i<=n; i++) d[i]=read();
	ans=d[1];
	for (int i=2; i<=n; i++)
	{
		if (d[i-1]>=d[i]) continue;
		else ans+=d[i]-d[i-1];
	}
	printf("%d\n",ans);
	return 0;
}
