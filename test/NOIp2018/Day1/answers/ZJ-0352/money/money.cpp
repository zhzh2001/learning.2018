#include<bits/stdc++.h>
using namespace std;
const int M=205,N=50005;
int n,A[M];

struct P80 {
	bool dp[N];
	int B[M],ans;
	bool Unfish(int x) {
		for(int i=1; i<=x; i++)dp[i]=false;
		dp[0]=true;
		for(int i=1; i<=x; i++)
			for(int j=1; j<=ans; j++)
				if(B[j] <= i && dp[i-B[j]])dp[i]=true;
		return dp[x];
	}
	void Solve() {
		ans=0;
		sort(A+1,A+n+1);
		for(int i=1; i<=n; i++)
			if(!Unfish(A[i]))
				B[++ans]=A[i];
		printf("%d\n",ans);
	}
} p_80;

struct P100 {
	bool Appr[N];
	int Vec[N],sz;
	void Insert(int x) {
//		printf("Insert %d\n",x);
		if(x>25000 || Appr[x])return;
		Vec[++sz]=x;
		Appr[x]=true;
	}
	void Choose(int x) {
//		printf("Choose %d\n",x);
		for(int i=1; i<=sz; i++)
			Insert(Vec[i]+x);
	}
	void Solve() {
		memset(Appr,0,sizeof(Appr));
		sz=0;
		Insert(0);
		int ans=0;
		sort(A+1,A+n+1);
		for(int i=1; i<=n; i++)
			if(!Appr[A[i]])
				ans++,Choose(A[i]);
		printf("%d\n",ans);
	}
} p_100;

int main() {

//	printf("Th mecordciop'a %.5lf MB\n",(&mem2-&mem1)/1024.0/1024);

	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);

	int T;
	scanf("%d",&T);
	while(T--) {
		scanf("%d",&n);
		for(int i=1; i<=n; i++)scanf("%d",&A[i]);
		if(n<=25)p_80.Solve();
		else p_100.Solve();
	}
	return 0;
}
