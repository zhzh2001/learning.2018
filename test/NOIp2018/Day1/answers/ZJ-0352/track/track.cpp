#include<bits/stdc++.h>
using namespace std;
const int M=1e5+5;

void qRead(int &r) {
	r=0;
	char c=0;
	for(; c<'0'||c>'9'; c=getchar());
	for(; c>='0'&&c<='9'; c=getchar())r=(r<<1)+(r<<3)+(c^48);
}

int n,m;
struct Edge {
	int v,c,to;
} E[M<<1];
int Head[M],e_tot;
void Link(int u,int v,int c) {
	E[++e_tot]=(Edge) {
		v,c,Head[u]
	};
	Head[u]=e_tot;
}

struct P_meq1 {
	int Max,Dis[M];
	void dfs(int u,int f) {
		if(Dis[u] > Dis[Max])Max=u;
		for(int i=Head[u]; i!=0; i=E[i].to) {
			int v=E[i].v,c=E[i].c;
			if(v == f)continue;
			Dis[v]=Dis[u]+c;
			dfs(v,u);
		}
	}
	int Get_qoy_D() {
		dfs(1,-1);
		Dis[Max]=0,dfs(Max,-1);
		return Dis[Max];
	}
	void Solve() {
		printf("%d\n",Get_qoy_D());
	}
} p_20;

struct P_chain {
	int D[M];
	void Get_D(int u) {
		for(int i=Head[u]; i!=0; i=E[i].to) {
			int v=E[i].v,c=E[i].c;
			if(v == u-1)continue;
			D[u]=c;
			Get_D(u+1);
		}
	}
	bool Unfish(int d) {
		int ans=0,l=0;
		for(int i=1; i<n; i++) {
			l+=D[i];
			if(l>=d)l=0,ans++;
		}
		return ans>=m;
	}
	void Solve() {
		Get_D(1);
		int l=1,r=500000000,ans=-1;
		while(l<=r) {
			int mid=(l+r)>>1;
			if(Unfish(mid))l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
} p_40;

struct qnode {
	int id,val;
	bool operator <(const qnode &b)const {
		if(val != b.val)return val<b.val;
		return id<b.id;
	}
};
#define Make_qnode(x,y) (qnode){x,y}
multiset<qnode>S;

struct P100 {
	struct node {
		int cnt,dep;
	};
#define Make_node(x,y) (node){x,y}
	int Rul[M],siz;
	bool Used[M];
	node Solve_in_Rul(int d) {
		int ans=0;
		sort(Rul+1,Rul+siz+1);
		for(int i=1; i<=siz; i++)Used[i]=false;
		S.clear();
		for(int i=1; i<=siz; i++)
			if(Rul[i] >= d)ans++,Used[i]=true;
			else S.insert(Make_qnode(i,Rul[i]));
		S.insert(Make_qnode(0,0x3f3f3f3f));
		for(int i=1; i<=siz; i++) {
			if(Used[i])continue;
//			printf("search for %d\n",Rul[i]);
			S.erase(S.find(Make_qnode(i,Rul[i])));
			multiset<qnode>::iterator it=S.upper_bound(Make_qnode(0,d-Rul[i]));
			qnode Fd=*it;
//			printf("Find %d\n",Fd.val);
			if(Fd.val == 0x3f3f3f3f)continue;
			Used[i]=true;
			Used[Fd.id]=true;
			S.erase(it);
			ans++;
		}
		int dep=0;
		for(int i=1; i<=siz; i++)if(!Used[i])dep=Rul[i];
		return Make_node(ans,dep);
	}
	node Dres[M];
	void dfs(int u,int f,int d) {
		for(int i=Head[u]; i!=0; i=E[i].to) {
			int v=E[i].v;
			if(v == f)continue;
			dfs(v,u,d);
		}
		siz=0;
		int ans=0;
		for(int i=Head[u]; i!=0; i=E[i].to) {
			int v=E[i].v,c=E[i].c;
			if(v == f)continue;
			Rul[++siz]=Dres[v].dep + c;
			ans+=Dres[v].cnt;
		}
		Dres[u]=Solve_in_Rul(d);
		Dres[u].cnt+=ans;
	}
	bool Unfish(int d) {
		dfs(1,-1,d);
		return Dres[1].cnt >= m;
	}
	void Solve() {
		int l=1,r=p_20.Get_qoy_D(),ans=-1;
		while(l<=r) {
			int mid=(l+r)>>1;
			if(Unfish(mid))l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
} p_100;

int main() {

//	printf("Th mecordciop'a %.5lf MB\n",(&mem2-&mem1 + M*10)/1024.0/1024);

	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);

	scanf("%d%d",&n,&m);
	bool Chain=true;
	for(int i=1; i<n; i++) {
		int u,v,c;
		qRead(u),qRead(v),qRead(c);
		if(u!=v+1 && v!=u+1)Chain=false;
		Link(u,v,c);
		Link(v,u,c);
	}
	if(m==1)p_20.Solve();
	else if(Chain)p_40.Solve();
	else p_100.Solve();
	return 0;
}
