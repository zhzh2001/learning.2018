#include <cstdio>
#include <algorithm>
#include <cstring>
#include <ctype.h>
using namespace std;
const int maxn=105,maxv=25005;
int T,n,ans,a[maxn],Max,f[maxv];
inline int read(){
	int ret=0; char ch=getchar(); bool fl=0;
	while (!isdigit(ch))
		fl^=!(ch^'-'),ch=getchar();
	while (isdigit(ch))
		ret=((ret+(ret<<2))<<1)+ch-'0',ch=getchar();
	return fl?-ret:ret;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (T=read();T--;){
		n=read(),Max=0;
		for (int i=1;i<=n;i++)
			a[i]=read(),Max=max(a[i],Max);
		memset(f,255,sizeof(f));
		f[0]=0;
		for (int i=1;i<=n;i++)
			for (int j=a[i];j<=Max;j++)
				if (f[j-a[i]]!=-1)
					f[j]=max(f[j],j-a[i]);
		ans=n;
		for (int i=1;i<=n;i++)
			if (f[a[i]]>0)
				ans--;
		printf("%d\n",ans);
	}
	return 0;
}
