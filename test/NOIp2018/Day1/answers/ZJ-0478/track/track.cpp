#include <cstdio>
#include <algorithm>
#include <cstring>
#include <ctype.h>
using namespace std;
const int maxn=50005;
int n,m,a[maxn],fa[maxn],f[maxn],Max[maxn],ent[maxn],ans;
int son[maxn<<1],lnk[maxn],nxt[maxn<<1],w[maxn<<1],tot;
int que[maxn],head,tail;
int tmp[maxn],len,L[maxn],R[maxn];
inline int read(){
	int ret=0; char ch=getchar(); bool fl=0;
	while (!isdigit(ch))
		fl^=!(ch^'-'),ch=getchar();
	while (isdigit(ch))
		ret=((ret+(ret<<2))<<1)+ch-'0',ch=getchar();
	return fl?-ret:ret;
}
void add(int x,int y,int z){
	son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot,w[tot]=z;
}
void DFS(int x,int last){
	for (int j=lnk[x];j;j=nxt[j])
		if (son[j]!=last){
			fa[son[j]]=x,ent[x]++,a[son[j]]=w[j];
			DFS(son[j],x);
		}
}
inline void Build_que(){
	head=tail=0;
	for (int i=1;i<=n;i++)
		if (!ent[i])
			que[++tail]=i;
	while (head!=tail){
		head++;
		if ((--ent[fa[que[head]]])==0)
			que[++tail]=fa[que[head]];
	}
}
inline bool check(int mid){
	memset(f,0,sizeof(f));
	memset(Max,0,sizeof(Max));
	for (int pos=1;pos<=n;pos++){
		len=0;
		for (int j=lnk[que[pos]];j;j=nxt[j])
			if (son[j]!=fa[que[pos]]){
				f[que[pos]]+=f[son[j]];
				if (Max[son[j]]>=mid)
					f[que[pos]]++;
				else
					tmp[++len]=Max[son[j]];
			}
		sort(tmp+1,tmp+len+1);
		for (int i=1;i<=len;i++)
			L[i]=i-1,R[i]=i+1;
		R[len]=len+1,L[len+1]=len;
		int Max_temp=0;
		for (int i=1,j=len;i<=len;i=R[i]){
			if (i==j)
				j=R[j];
			while (L[j]!=i&&tmp[i]+tmp[L[j]]>=mid)
				j=L[j];
			while (j<=len&&tmp[i]+tmp[j]<mid)
				j=R[j];
			if (j>len)
				Max_temp=tmp[i];
			else{
				f[que[pos]]++;
				R[L[j]]=R[j],L[R[j]]=L[j],j=R[j];
			}
		}
		Max[que[pos]]=Max_temp+a[que[pos]];
	}
	if (f[1]>=m)
		return 1;
	else
		return 0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<n;i++){
		int x=read(),y=read(),z=read();
		add(x,y,z),add(y,x,z);
	}
	DFS(1,-1);
	Build_que();
	int l=1,r=500000000,mid;
	while (l<=r){
		mid=(l+r)>>1;
		if (check(mid))
			ans=mid,l=mid+1;
		else
			r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
