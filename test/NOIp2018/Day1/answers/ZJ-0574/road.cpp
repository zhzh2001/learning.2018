#include<bits/stdc++.h>
using namespace std;

#define Uni all right
#define REP(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;++i)
#define DREP(i,a,b) for(int i=(a),i##_end_=(b);i>i##_end_;--i)
#define LREP(i,a) for(int i=Head[a];i;i=Next[i])
#define LL long long

static const int M=100004;
LL Ans;
int n,Stk[M],top;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	scanf("%d",&n);
	
	REP(i,0,n){
		int x;
		scanf("%d",&x);
		while(top && Stk[top]>=x)Ans+=Stk[top]-max(x,Stk[top-1]),top--;
		Stk[++top]=x;
	}
	while(top)Ans+=Stk[top]-Stk[top-1],top--;
	printf("%lld\n",Ans);
	
	return 0;
}
