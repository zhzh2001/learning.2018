#include<bits/stdc++.h>
using namespace std;

#define Uni all right
#define REP(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;++i)
#define DREP(i,a,b) for(int i=(a),i##_end_=(b);i>i##_end_;--i)
#define LREP(i,a) for(int i=Head[a];i;i=Next[i])
#define LL long long

static const int M=50004;
int Head[M],Next[M<<1],V[M<<1],W[M<<1],tot;
void Add_Edge(int u,int v,int w){
	Next[++tot]=Head[u],V[Head[u]=tot]=v,W[tot]=w;
}
int n,m;
int F[M],E[M],Res,Limit;
int Mark[M];
void DFS(int x,int f){
	LREP(i,x)if(V[i]!=f)
		DFS(V[i],x);
	int d=0,j=0,Tmp=0;
	LREP(i,x)if(V[i]!=f)
		E[d++]=F[V[i]]+W[i];
	sort(E,E+d);
	REP(i,0,d)Mark[i]=-1;
	DREP(i,d-1,-1){
		while(j<i && E[j]+E[i]<Limit)++j;
		if(E[i]>=Limit)++Res,Mark[i]=i;
		else if(j<i)++Res,Mark[i]=j,Mark[j]=i,++j;
		else break;
	}
	DREP(i,d-1,-1)if(Mark[i]==-1){
		Tmp=E[i];break;
	}
	DREP(i,d-1,-1)if(Tmp<E[i] && Mark[i]!=-1 && Mark[i]<i && E[Mark[i]]+Tmp>=Limit){
		Tmp=E[i];break;
	}
	F[x]=Tmp;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	REP(i,1,n){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		Add_Edge(u,v,w);
		Add_Edge(v,u,w);
	}
	int l=1,r=500000000,Ans=-1;
	while(l<=r){
		Limit=l+r>>1;
		Res=0;
		DFS(1,0);
		if(Res>=m)Ans=Limit,l=Limit+1;
		else r=Limit-1;
	}
	printf("%d\n",Ans);
	
	return 0;
}
