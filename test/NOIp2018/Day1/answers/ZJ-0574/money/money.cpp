#include<bits/stdc++.h>
using namespace std;

#define Uni all right
#define REP(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;++i)
#define DREP(i,a,b) for(int i=(a),i##_end_=(b);i>i##_end_;--i)
#define LREP(i,a) for(int i=Head[a];i;i=Next[i])
#define ULL unsigned long long

static const int N=104,M=25044;
bool F[M];
int T,n,A[N];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		REP(i,0,n)scanf("%d",&A[i]);
		sort(A,A+n);
		int Ans=0;
		memset(F,0,sizeof(F));
		F[0]=1;
		REP(i,0,n)if(!F[A[i]]){
			++Ans;
			for(int j=A[i],k=0;j<=25000;j+=8,k+=8){
				F[j+0]|=F[k+0];
				F[j+1]|=F[k+1];
				F[j+2]|=F[k+2];
				F[j+3]|=F[k+3];
				F[j+4]|=F[k+4];
				F[j+5]|=F[k+5];
				F[j+6]|=F[k+6];
				F[j+7]|=F[k+7];
			}
		}
		printf("%d\n",Ans);
	}
	return 0;
}
