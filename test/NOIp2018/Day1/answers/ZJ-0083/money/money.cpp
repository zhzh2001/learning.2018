#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#define LL long long
using namespace std;

const LL Maxn = 110, MaxAlpha = 25010;
LL n, A[ Maxn ], Ans;
bool Appeared[ MaxAlpha ];

void Work() {
	memset( Appeared, false, sizeof( Appeared ) );
	scanf( "%lld", &n ); Ans = n;
	for( LL i = 1; i <= n; ++i ) scanf( "%lld", &A[ i ] );
	sort( A + 1, A + n + 1 );
	Appeared[ 0 ] = true;
	for( LL i = 1; i <= n; ++i ) {
		if( Appeared[ A[ i ] ] ) {
			--Ans;
			continue;
		}
		for( LL j = A[ i ]; j <= A[ n ]; ++j ) 
			if( Appeared[ j - A[ i ] ] ) Appeared[ j ] = true;
	}
	printf( "%lld\n", Ans );
	return;
}

int main() {
	freopen( "money.in", "r", stdin );
	freopen( "money.out", "w", stdout );
	LL t; scanf( "%lld", &t );
	for( ; t; --t ) Work();
	return 0;
}
