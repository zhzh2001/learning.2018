#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#define LL long long
using namespace std;

const LL Maxn = 100010;
LL n, d[ Maxn ], Stack[ Maxn ], Size, Ans = 0;

int main() {
	freopen( "road.in", "r", stdin );
	freopen( "road.out", "w", stdout );
	scanf( "%lld", &n );
	for( LL i = 1; i <= n; ++i ) scanf( "%lld", &d[ i ] );	
	Size = 0;
	for( LL i = 1; i <= n; ++i ) {
		if( d[ i ] > Stack[ Size ] ) {
			Ans += d[ i ] - Stack[ Size ];
			Stack[ ++Size ] = d[ i ];
		} else {
			while( Size && Stack[ Size ] >= d[ i ] ) --Size;
			Stack[ ++Size ] = d[ i ];
		}
	}
	printf( "%lld\n", Ans );
	return 0;
}
