#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#define LL long long
using namespace std;

const LL Maxn = 50010;
struct edge {
	LL To, Length, Next;
};
edge Edge[ Maxn << 1 ];
LL Start[ Maxn ], UsedSpace;
LL n, m;

LL Num, Vc[ Maxn ];

LL L, R, Queue[ Maxn ];
bool Vis[ Maxn ];

LL Left, Right, Mid;
LL Count, ReturnVal[ Maxn ];
bool Rec[ Maxn ];

bool Check( LL k ) {
	Count = 0; memset( Vis, false, sizeof( Vis ) );
	memset( ReturnVal, 0, sizeof( ReturnVal ) );
	for( LL i = R; i >= 1; --i ) {
		LL u = Queue[ i ];
		Vis[ u ] = true;
		Num = 0;
		for( LL t = Start[ u ]; t; t = Edge[ t ].Next ) {
			LL v = Edge[ t ].To;
			if( !Vis[ v ] ) continue;
			Vc[ ++Num ] = ReturnVal[ v ] + Edge[ t ].Length;
		}
		sort( Vc + 1, Vc + Num + 1 );
		LL l = 1, r = Num;
		for( ; r >= 1 && Vc[ r ] >= k; --r ) ++Count;
		Num = r;
		for( LL j = 1; j <= Num; ++j ) Rec[ j ] = false; //GetVal
		while( l < r ) {
			if( Vc[ l ] + Vc[ r ] >= k ) {
				++Count; Rec[ l ] = true;
				--r;
			}
			++l;
		}
		LL T = 0;
		for( LL j = 1; j <= Num; ++j ) if( Rec[ j ] ) T = j;
		l = T + 1;
		for( LL j = T; j >= 1; --j ) {
			if( !Rec[ j ] ) continue;
			while( Vc[ l ] + Vc[ j ] < k ) ++l;
			Rec[ l ] = true;
			++l;
		}
		for( LL j = Num; j >= 1; --j ) 
			if( !Rec[ j ] ) {
				ReturnVal[ u ] = Vc[ j ];
				break;
			}
	}
	return Count >= m;
}

void AddEdge( LL x, LL y, LL z ) {
	Edge[ ++UsedSpace ] = ( edge ) { y, z, Start[ x ] };   
	Start[ x ] = UsedSpace;
	return;
}

void Init() {
	Left = Right = 0;
	scanf( "%lld%lld", &n, &m );
	for( LL i = 1; i < n; ++i ) {
		LL a, b, l; scanf( "%lld%lld%lld", &a, &b, &l );
		AddEdge( a, b, l ); AddEdge( b, a, l );
		Right += l;
	}
	return;
}

void Bfs() {
	memset( Vis, false, sizeof( Vis ) );
	L = R = 1;
	Queue[ R++ ] = 1;
	Vis[ 1 ] = true;
	while( L < R ) {
		LL u = Queue[ L++ ];
		for( LL t = Start[ u ]; t; t = Edge[ t ].Next ) {
			LL v = Edge[ t ].To;
			if( Vis[ v ] ) continue;
			Queue[ R++ ] = v; Vis[ v ] = true;
		}
	}
	--R;
	return;
}

int main() {
	freopen( "track.in", "r", stdin );
	freopen( "track.out", "w", stdout );
	Init();
	Bfs();
	while( Left < Right ) {
		Mid = ( Left + Right + 1 ) >> 1;
		if( Check( Mid ) ) Left = Mid; else Right = Mid - 1;
	}
	printf( "%lld\n", Left );
	return 0;
}
