#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
const int N=50005;
using namespace std;
int n,m,i,j,g,k,x,y,z,q,p,t,o,sum,mid,h[N],v[N],l[N];
struct Node{
	int pre,nxt,num;
}tree[N*2],f[N],fa[N];
void read(int &a)
{
	a=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') a=a*10+ch-48,ch=getchar();
}
void add(int x,int y,int z)
{tree[++g].pre=h[x];tree[g].nxt=y;tree[g].num=z;h[x]=g;}
int dfs(int x)
{	v[x]=1;
	for(int i=h[x];i;i=tree[i].pre)
	if (!v[tree[i].nxt])
	{	int t=dfs(tree[i].nxt)+tree[i].num;
		if (!f[x].pre) f[x].pre=t; else
		if (t>f[x].pre)
		{	f[x].nxt=f[x].pre;
			f[x].pre=t;
		}	else
		if (t>f[x].nxt) f[x].nxt=t;
	}	return f[x].pre;
}
void search(int x,int y)
{	v[x]=1;
	for(int i=h[x];i;i=tree[i].pre)
	if (!v[tree[i].nxt])
	{	fa[tree[i].nxt].pre=x;
		fa[tree[i].nxt].nxt=y+1;
		fa[tree[i].nxt].num=tree[i].num;
		search(tree[i].nxt,y+1);
	}
}
int change(int z)
{	if(z>=m) return 1000000007;
	int tt=0;
	for(int i=1;i<=n;i++)	if(!v[i])
	for(int j=1;j<=n;j++)	if(!v[j]&&i!=j)
	{	int x=i,y=j,t=0,flag=0;
		if (fa[x].nxt<fa[y].nxt) swap(x,y);
		while(fa[x].nxt>fa[y].nxt)
		{	if (v[x]){flag=1;break;}
			t+=fa[x].num,v[x]=1,x=fa[x].pre;
		}
		if (!flag)
		while(x!=y)
		{	if (v[x]||v[y]) {flag=1;break;}
			t+=fa[x].num+fa[y].num,v[x]=1,v[y]=1,x=fa[x].pre,y=fa[y].pre;
		}
		if (flag)
		{	int xx=i,yy=j;
			if (fa[xx].nxt<fa[yy].nxt) swap(xx,yy);
			while(x!=xx) v[xx]=0,xx=fa[xx].pre;
			while(y!=yy) v[yy]=0,yy=fa[yy].pre;
			continue;
		}
		tt=max(min(change(z+1),t),tt);
	}
	return tt;
}
inline bool cmp(Node a,Node b){return a.num>b.num;}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);q=1;p=1;
	for(i=1;i<n;i++)
	{	read(x);read(y);read(z);
		if (x!=1&&y!=1) q=0;
		if (abs(x-y)!=1)p=0;else l[x]=z;
		add(x,y,z);add(y,x,z);
	}
	if (m==1)
	{	memset(f,0,sizeof(f));
		memset(v,0,sizeof(v));
		k=dfs(1);
		for(i=1;i<=n;i++)
		sum=max(f[i].pre+f[i].nxt,sum);
		printf("%d\n",sum);
	}	else
	if (q)
	{	sort(tree+1,tree+g+1,cmp);
		sum=1000000007;
		for(i=1;i<=m;i++)
		sum=min(sum,tree[i*2].num+tree[(m*2-i+1)*2].num);
		printf("%d\n",sum);
	}	else
	if (p)
	{	t=0;
		for(i=1;i<n;i++) t+=l[i];
		p=1;q=t/m;
		while(p<=q)
		{	mid=(p+q)/2;
			t=0;o=0;
			for(i=1;i<=n;i++)
			{	t+=l[i];
				if (t>=mid){t=0;o++;}
				if (o>=m) break;
			}
			if (o<m) q=mid-1;else p=mid+1,sum=mid;
		}
		printf("%d\n",sum);
	}
	if (n<=15)
	{	memset(v,0,sizeof(v));
		search(1,1);
		sum=0;
		for(i=1;i<=n;i++)
		for(j=1;j<=n;j++)
		if (i!=j)
		{	memset(v,0,sizeof(v));
			x=i;y=j;t=0;
			if (fa[x].nxt<fa[y].nxt) swap(x,y);
			while(fa[x].nxt>fa[y].nxt) t+=fa[x].num,v[x]=1,x=fa[x].pre;
			while(x!=y) t+=fa[x].num+fa[y].num,v[x]=1,v[y]=1,x=fa[x].pre,y=fa[y].pre; 
			sum=max(sum,min(t,change(1)));
		}
		printf("%d\n",sum+2);
	}
	return 0;
}
