#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
const int N=25005;
int T,n,i,j,k,g,a[N],f[N],h[N];
using namespace std;
void read(int &a)
{
	a=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') a=a*10+ch-48,ch=getchar();
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	while(T--)
	{
		read(n);
		for(i=1;i<=n;i++) read(a[i]);
		sort(a+1,a+n+1);
		memset(f,0,sizeof(f));g=0;
		for(i=1;i<=n;i++)
		if (f[a[i]]==0)
		{
			h[++g]=a[i];f[a[i]]=1;
			for(j=1;j<=a[n];j++)
			if (f[j])
			for(k=1;k<=g;k++)
			if (j+h[k]<=a[n])
			f[j+h[k]]=1;
		}
		printf("%d\n",g);
	}
	return 0;
}
