#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
const int Inf=1000000007;
const int N=100005;
using namespace std;
int n,i,sum,a[N];
void read(int &a)
{
	a=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') a=a*10+ch-48,ch=getchar();
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	a[0]=Inf;
	for(i=1;i<=n;i++)
	{
		read(a[i]);
		a[0]=min(a[0],a[i]);
	}
	for(i=1;i<=n;i++)
	if (a[i]>a[i-1])
	sum+=a[i]-a[i-1];
	printf("%d\n",sum+a[0]);
	return 0;
}
