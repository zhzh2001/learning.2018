#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<queue>
#include<algorithm>
using namespace std;
#define maxn (100000+10)
#define mod 1
#define ll long long
#define isnum(c) (c>='0' && c<='9')
ll read(){
	ll x=0,f=1; char c=getchar();
	for(;!isnum(c);c=getchar())
		if(c=='-')f=-1;
	for(; isnum(c);c=getchar())
		x=x*10+c-48;
	return x*f;
}
int ans=0,n;
int a[maxn];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=0;i<n;i++)a[i]=read();
	for(int i=0;i<n;i++){
		if(i==n-1){
			ans+=a[i];
			break;
		}
		if(a[i]>a[i+1]){
			ans+=a[i]-a[i+1];
		}
	}
	printf("%d\n",ans);
	return 0;
}
