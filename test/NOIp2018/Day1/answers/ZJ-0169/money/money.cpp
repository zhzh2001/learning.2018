#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<queue>
#include<algorithm>
using namespace std;
#define maxn (100+10)
#define ll long long
#define isnum(c) (c>='0' && c<='9')
ll read(){
	ll x=0,f=1; char c=getchar();
	for(;!isnum(c);c=getchar())
		if(c=='-')f=-1;
	for(; isnum(c);c=getchar())
		x=x*10+c-48;
	return x*f;
}
int T,n;
int a[maxn];
int gcd(int a,int b){return b?gcd(b,a%b):a;}
int lcm(int a,int b){return a/gcd(a,b)*b;}

bool dfs(int x,int rem){
	if(rem%a[x]==0)return 1;
	if(x==0)return 0;
	while(1){
		if(rem<0)return 0;
		if(dfs(x-1,rem))return 1;
		rem-=a[x];
	}
}

bool chazhao(int x){
	if(x>=2 && a[x]>(a[0]-1)*(a[1]-1)-1)
		return 1;
	return dfs(x-1,a[x]);
}

void partjob(){
	n=read();
	for(int i=0;i<n;i++){
		a[i]=read();
	}
	sort(a,a+n);
	
	for(int i=1;i<n;i++){
		if(chazhao(i)){
			for(int k=i;k<n;k++){
				a[k]=a[k+1];
			}
			n--;
			i--;
		}
	}
	printf("%d\n",n);
	return;
	//for(int i=0;i<n;i++)
		//printf("%d ",a[i]);
	//printf("\n");
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	for(int ii=0;ii<T;ii++)
		partjob();
	return 0;
}
