#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#include<queue>
#include<algorithm>
#include<conio.h>
//#include<map>
using namespace std;
#define maxn (50000+10)
#define mod 1
#define ll long long
#define isnum(c) (c>='0' && c<='9')
ll read(){
	ll x=0,f=1; char c=getchar();
	for(;!isnum(c);c=getchar())
		if(c=='-')f=-1;
	for(; isnum(c);c=getchar())
		x=x*10+c-48;
	return x*f;
}

struct node{int to,len;};
int u,v,len,n,m,guiding,sumlen=0;
vector<node> a[maxn];
int num;//已建成的赛道数
int dis[maxn];//当前最长赛道
int deeep[maxn];//深度
vector<int> length;

#define dele(a,p) {\
	for(int ddi=p;ddi<int(a.size()-1);ddi++)\
	a[ddi]=a[ddi+1];\
	a.pop_back();\
}

bool dfs(int x,int d){
	if(deeep[x])return 0;
	deeep[x]=d; dis[x]=0;
	
	for(int i=0;i<int(a[x].size());i++)
	if(dfs(a[x][i].to,d+1))//递归 
		return 1;
		
	length.clear();
	
	for(int i=0;i<int(a[x].size());i++)
	if(deeep[a[x][i].to]==deeep[x]+1){
		int p=a[x][i].to;
		
		length.push_back(a[x][i].len+dis[p]);
	}
	sort(length.begin(),length.end());
		//cout<<"entry "<<x<<endl;
		//cout<<"length=";
		//for(int i=0;i<length.size();i++)cout<<length[i]<<' ';
		//cout<<endl;
	while(!length.empty() && length.back()>=guiding){
		num++;
		length.pop_back();
	}
	if(num>=m)return 1;
	for(int i=1;i<int(length.size());i++){
		int j;
		for(j=0;j<i;j++){
			if(length[j]+length[i]>=guiding)
				break;
		}
		if(j<i){
			num++;
			dele(length,i);
			dele(length,j);
			i--;
		}
	}
	if(num>=m)return 1;
	if(!length.empty())
		dis[x]=length.back();
	return 0;
}

bool okk(int x){
	//cout<<"guiding="<<x<<endl;
	guiding=x;
	for(int i=0;i<=n;i++)deeep[i]=0; num=0;
	int returnx=dfs(1,1);
	//cout<<"while guiding="<<x<<" return "<<returnx<<endl;
	return returnx;
}

int erfen_in_main(int l,int r){
	int mid;
	while(l<=r){
		mid=(l+r)/2;
		if(okk(mid))l=mid+1;
		else r=mid-1;
	}
	return l;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(); m=read();
	for(int i=0;i<n-1;i++){
		u=read(); v=read(); len=read(); sumlen+=len;
		//if(u>v)swap(u,v);
		a[u].push_back((node){v,len});
		a[v].push_back((node){u,len});
	}
	int ans=erfen_in_main(0,sumlen)-1;
	//printf("sumlen=%d\n",sumlen);
	printf("%d\n",ans);
	return 0;
}
