#include<cstdio>
#include<queue>
#include<algorithm>
using namespace std;
const int N=50010;
int n,m,head[N],cnt,A[N],dep[N],dis[N],siz[N],fa[N],lim,bel[N],pos[N],s[N],f[N][17],tmpx,tmpy;
struct E{int to,nxt,w;}e[N<<1];
struct lian{int l,r,w,g,L,R;}a[1000010];
struct node{int l,r,sum,lazy;}t[N<<3];
queue<int>q;
bool vis[N],B;
void Judge(){freopen("track.in","r",stdin);freopen("track.out","w",stdout);}
int read(){int d=0,f=1; char c=getchar(); while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();} while (c>='0'&&c<='9') d=d*10+c-48,c=getchar(); return d*f;}
void add(int x,int y,int z){e[++cnt]=(E){y,head[x],z}; head[x]=cnt;}
void dfs(int u)
{
	for (int i=head[u];i;i=e[i].nxt)
	{
		int v=e[i].to;
		if (v==fa[u]) continue;
		dep[v]=dep[u]+e[i].w; fa[v]=u; dfs(v); 
	}
}
void dfs4(int u)
{
	siz[u]=1;
	for (int i=head[u];i;i=e[i].nxt)
	{
		int v=e[i].to;
		if (v==fa[u]) continue;
		dep[v]=dep[u]+1; fa[v]=u; dfs4(v); siz[u]+=siz[v];
		if (siz[v]>siz[s[u]]) s[u]=v;
	}
}
void bfs(int S)
{
	for (int i=1;i<=n;i++) dis[i]=1E9,vis[i]=0;
	q.push(S); vis[S]=1; dis[S]=0;
	while (!q.empty())
	{
		int u=q.front(); q.pop();
		for (int i=head[u];i;i=e[i].nxt)
		{
			int v=e[i].to;
			if (!vis[v]) dis[v]=dis[u]+e[i].w,vis[v]=1,q.push(v);
		}
	}
}
bool check(int lim)
{
	int tot=0,sum=0;
	for (int i=1;i<n;i++)
	{
		sum+=A[i];
		if (sum>=lim) tot++,sum=0;
	}
	return tot>=m;
}
bool cmp(lian a,lian b){return a.w>b.w;}
void build(int i,int l,int r)
{
	t[i].l=l; t[i].r=r;
	if (l==r) return;
	int mid=(l+r)>>1;
	build(i<<1,l,mid); build(i<<1|1,mid+1,r);
}
void pushup(int i){t[i].sum=t[i<<1].sum+t[i<<1|1].sum;}
void pushdown(int i)
{
	if (t[i].lazy==0) return;
	int i1=i<<1,i2=i1|1;
	t[i1].lazy+=t[i].lazy; t[i2].lazy+=t[i].lazy;
	t[i1].sum+=t[i].lazy*(t[i1].r-t[i1].l+1);
	t[i2].sum+=t[i].lazy*(t[i2].r-t[i2].l+1);
	t[i].lazy=0;
}
void change(int i,int l,int r,int val)
{
	pushdown(i);
	if (l<=t[i].l&&t[i].r<=r)
	{
		t[i].lazy=val; t[i].sum+=val*(t[i].r-t[i].l+1);
		return;
	}
	int mid=(t[i].l+t[i].r)>>1;
	if (r<=mid) change(i<<1,l,r,val);
	else if (l>mid) change(i<<1|1,l,r,val);
	else change(i<<1,l,mid,val),change(i<<1|1,mid+1,r,val);
	pushup(i);
}
int Query(int i,int l,int r)
{
	pushdown(i);
	if (l<=t[i].l&&t[i].r<=r) return t[i].sum;
	int mid=(t[i].l+t[i].r)>>1;
	if (r<=mid) return Query(i<<1,l,r);
	else if (l>mid) return Query(i<<1|1,l,r);
	else return Query(i<<1,l,mid)+Query(i<<1|1,mid+1,r);
}
void update(int x,int y,int xx,int yy,int val,int g)
{
	while (bel[x]!=bel[xx])
	{
		change(1,pos[bel[x]],pos[x],val);
		x=fa[bel[x]];
	}
	if (x!=g) change(1,pos[xx],pos[x],val);
	while (bel[y]!=bel[yy])
	{
		change(1,pos[bel[y]],pos[y],val);
		y=fa[bel[y]];
	}
	if (y!=g) change(1,pos[yy],pos[y],val);
}
int query(int x,int y,int xx,int yy,int g)
{
	int res=0;
	while (bel[x]!=bel[xx])
	{
		res+=Query(1,pos[bel[x]],pos[x]);
		x=fa[bel[x]];
	}
	if (x!=g) res+=Query(1,pos[xx],pos[x]);
	while (bel[y]!=bel[yy])
	{
		res+=Query(1,pos[bel[y]],pos[y]);
		y=fa[bel[y]];
	}
	if (y!=g) res+=Query(1,pos[yy],pos[y]);
	return res;
}
void dfs2(int k,int lst)
{
	if (k>m) {B=1; return;}
	for (int i=lst+1;!B&&i<=cnt&&a[i].w>=lim;i++)
	{
		int x=a[i].l,y=a[i].r,xx=a[i].L,yy=a[i].R;
		if (query(x,y,xx,yy,a[i].g)!=0) continue;
		update(x,y,xx,yy,1,a[i].g);
		dfs2(k+1,i);
		update(x,y,xx,yy,-1,a[i].g);
	}
}
bool check(){B=0; dfs2(1,0); return B;}
void dfs3(int u,int Bel)
{
	pos[u]=++cnt; bel[u]=Bel;
	if (s[u]) dfs3(s[u],Bel);
	for (int i=head[u];i;i=e[i].nxt)
	{
		int v=e[i].to;
		if (v==fa[u]||v==s[u]) continue;
		dfs3(v,v);
	}
}
int Jump(int x,int y){for (int i=16;i>=0;i--) if (y&(1<<i)) x=f[x][i]; return x;}
int lca(int x,int y)
{
	if (dep[x]<dep[y]) swap(x,y),B=1;
	tmpx=x;
	for (int i=16;i>=0;i--)
		if (dep[f[x][i]]>=dep[y]) x=f[x][i];
	if (x==y)
	{
		tmpx=Jump(tmpx,dep[tmpx]-dep[x]-1); tmpy=y;
		return x;
	}
	for (int i=16;i>=0;i--)
		if (f[x][i]!=f[y][i]) x=f[x][i],y=f[y][i];
	tmpx=x; tmpy=y;
	return f[x][0];
}
int main()
{
	Judge();
	n=read(); m=read();
	bool bo=1;
	for (int i=1;i<n;i++)
	{
		int u=read(),v=read(),w=read();
		if (v!=u+1) bo=0; else A[u]=w;
		add(u,v,w); add(v,u,w);
	}
	if (m==1)
	{
		dfs(1);
		int mx=0; for (int i=1;i<=n;i++) if (dep[i]>dep[mx]) mx=i;
		bfs(mx);
		int ans=0; for (int i=1;i<=n;i++) if (dis[i]>ans) ans=dis[i];
		printf("%d",ans);
	}
	else if (bo)
	{
		int l=1,r=1E9,ans=0;
		while (l<=r)
		{
			int mid=(l+r)>>1;
			if (check(mid)) ans=mid,l=mid+1; else r=mid-1;
		}
		printf("%d",ans);
	}
	else
	{
		dep[1]=1; dfs4(1);
		for (int i=1;i<=n;i++) f[i][0]=fa[i];
		for (int j=1;j<=16;j++)
			for (int i=1;i<=n;i++)
				f[i][j]=f[f[i][j-1]][j-1];
		cnt=0; dfs3(1,1);
		build(1,1,n);
		cnt=0;
		for (int i=1;i<=n;i++)
		{
			bfs(i);
			for (int j=i+1;j<=n;j++)
			{
				a[++cnt]=(lian){i,j,dis[j]};
				B=0; a[cnt].g=lca(i,j);
				a[cnt].L=tmpx; a[cnt].R=tmpy;
				if (B) swap(a[cnt].L,a[cnt].R);
			}
		}
		sort(a+1,a+1+cnt,cmp);
		int l=1,r=5E8,ans=0;
		while (l<=r)
		{
			lim=(l+r)>>1;
			if (check()) ans=lim,l=lim+1; else r=lim-1;
		}
		printf("%d",ans);
	}
	return 0;
}
