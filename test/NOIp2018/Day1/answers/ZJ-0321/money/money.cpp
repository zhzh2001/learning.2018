#include<cstdio>
#include<algorithm>
using namespace std;
const int N=111,M=25010;
int T,n,a[N];
bool f[N][M];
void Judge(){freopen("money.in","r",stdin);freopen("money.out","w",stdout);}
int read(){int d=0,f=1; char c=getchar(); while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();} while (c>='0'&&c<='9') d=d*10+c-48,c=getchar(); return d*f;}
int main()
{
	Judge();
	T=read();
	while (T--)
	{
		int n=read();
		for (int i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+1+n);
		for (int i=1;i<=n;i++)
			for (int j=1;j<=25000;j++)
				f[i][j]=0;
		f[0][0]=1;
		for (int i=1;i<=n;i++)
		{
			for (int j=0;j<=25000;j++) f[i][j]=f[i-1][j];
			for (int j=0;j<=25000-a[i];j++)
				if (f[i][j])
					f[i][j+a[i]]=1;
		}
		int ans=n;
		for (int i=n;i>=1;i--)
			if (f[i-1][a[i]]) ans--;
		printf("%d\n",ans);
	}
	return 0;
}
