#include<cstdio>
#include<cmath>
const int N=100010;
int n,a[N],f[N][17],g[N][17],ans;
void Judge(){freopen("road.in","r",stdin);freopen("road.out","w",stdout);}
int read(){int d=0,f=1; char c=getchar(); while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();} while (c>='0'&&c<='9') d=d*10+c-48,c=getchar(); return d*f;}
void Solve(int l,int r,int now)
{
	if (r<l) return;
	int k=log2(r-l+1);
	int id=g[l][k];
	if (f[l][k]>f[r-(1<<k)+1][k]) id=g[r-(1<<k)+1][k];
	ans+=a[id]-now;
	Solve(l,id-1,a[id]); Solve(id+1,r,a[id]);
}
int main()
{
	Judge();
	n=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<=n;i++) f[i][0]=a[i],g[i][0]=i;
	for (int j=1;j<=16;j++)
		for (int i=1;i<=n-(1<<j)+1;i++)
			if (f[i][j-1]<f[i+(1<<(j-1))][j-1]) f[i][j]=f[i][j-1],g[i][j]=g[i][j-1];
			else f[i][j]=f[i+(1<<(j-1))][j-1],g[i][j]=g[i+(1<<(j-1))][j-1];
	Solve(1,n,0);
	printf("%d",ans);
	return 0;
}
