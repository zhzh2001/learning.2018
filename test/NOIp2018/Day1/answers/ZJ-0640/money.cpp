//Maggie
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>

using namespace std;

int n,a[105],b[105];
int maxn;
bool flag[25005];

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		int res=0;
		scanf("%d",&n);
		memset(a,0,sizeof(a));
		memset(flag,0,sizeof(flag));
		memset(b,0,sizeof(b));
		int K=0;
		maxn=0;
		bool ff=false;
		for (int i=1;i<=n;i++){
			scanf("%d",&b[i]);
			if (b[i] == 1){
				ff=true;
				break;
			}
			maxn=max(b[i],maxn);
		}
		if (ff) {
			printf("1\n");
			continue;
		}
		for (int i=1;i<=n;i++){
			a[++K]=b[i];
			if (flag[a[K]] == true){
				K--;
				continue;
			}
			for (int j=1;j<=maxn;j++){
				if (j*a[K]>maxn) break;
				flag[j*a[K]] = true;
			}
		}
		sort(a+1,a+K+1);
		int k=1;
		for (int i=2;i<=maxn;i++){
			if (i > a[k] && k < K) k++;
			if (flag[i] == true && i != a[k]) continue;
			int x=(i>>1) + 1;
			for (int j=1;j<=x;j++){
				if (flag[j] && flag[i-j]){
					flag[i]=true;
					if (i == a[k]) res++,k++;
				}
			}
		}
		printf("%d\n",K-res);
	}
	return 0;
}
