//Maggie
#include <cstdio>

using namespace std;

int n,a[100005];

int absx(int a){
	if (a < 0) return -a;
	else return a;
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) 	scanf("%d",&a[i]);
	int ans = a[1];
	int Last = a[1];
	for (int i=1;i<=n;i++){
		if (a[i] < a[i+1])
			ans += absx(Last-a[i+1]);
		Last = a[i+1];
		//printf("%d\n",ans);
	}
	printf("%d",ans);
	return 0;
}
