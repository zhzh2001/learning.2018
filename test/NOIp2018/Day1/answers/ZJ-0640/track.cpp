//Maggie
#include <cstdio>
#include <algorithm>

using namespace std;

const int N = 500005;
int n,m,ans,tot;
int fa[N];

struct Maggie{
	int u,v,w;
}e[N];

bool cmp(Maggie a,Maggie b){
	return a.w>b.w;
}

void add(int u,int v,int w){
	e[++tot].u=u;
	e[tot].v=v;
	e[tot].w=w;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	long long sum=0;
	bool flag1=false;
	for (int i=1;i<n;i++){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		add(u,v,w);
		sum+=w;
		if (u != 1) flag1=true;
	}
	if (flag1 == false){
		sort(e+1,e+1+tot,cmp);
		if (m == 1)
			printf("%d",e[1].w+e[2].w);
		return 0;
	}
	printf("%lld",sum/m);
	return 0;
}

