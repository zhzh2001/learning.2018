#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=105,Mx=25007;
int n,m,T;
int a[N],f[Mx];
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		memset(f,0,sizeof f);
		scanf("%d",&n);m=n;
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		f[0]=1;
		for (int i=1;i<=n;i++) {
			if (f[a[i]]) {m--;continue;}
			for (int j=a[i];j<=Mx;j++) f[j]|=f[j-a[i]];
		}
		printf("%d\n",m);
	}
}
