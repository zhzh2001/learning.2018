#include <bits/stdc++.h>
using namespace std;
const int N=50005;
struct edge {
	int t,nxt,w;
}e[N<<1];
int n,m,cnt,now,res,ans;
int head[N],siz[N],Bs[N];
void add(int u,int t,int w) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;e[cnt].w=w;
}
vector<int>s[N];
int dfs(int u,int fa) {
	s[u].clear();
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==fa) continue; 
		int v=dfs(t,u)+e[i].w;
		if (res>=m) return 0;
		if (v>=now) res++;
		else s[u].push_back(v);
	}
	if (res>=m) return 0;
	if (s[u].empty()) return 0;
//	printf("%d:",u);
	sort(s[u].begin(),s[u].end());
//	for (int i=0;i<s[u].size();i++) printf("%d ",s[u][i]);
	int l=0,r=s[u].size()-1,Mx=0;
	while (l<=r) {
		while (s[u][l]+s[u][r]<now && l<r) Mx=s[u][l],l++;
		if (l==r) {Mx=s[u][r];break;}
		l++;r--;res++;
	}
//	printf("%d\n",Mx);
//	puts("");
	return Mx;
}
bool check(int mid) {
	now=mid;
	res=0;
	dfs(1,0);//不同的点当根不同?
	if (res>=m) return 1;
	return 0;
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	srand((unsigned)time(NULL));
	scanf("%d%d",&n,&m);
	for (int i=1,u,v,w;i<n;i++) {
		scanf("%d%d%d",&u,&v,&w);
		add(u,v,w);add(v,u,w);
	}
	int l=0,r=1e9;
	while (l<=r) {
		int mid=(l+r)>>1;
		if (check(mid)) {ans=mid;l=mid+1;}
		else r=mid-1;
	}
	printf("%d\n",ans);
}
