#include <cstdio>
#include <algorithm>
using namespace std;
const int N=1e5+7;
int n,mn=1e9;
int a[N],L[N],R[N],id[N];
long long ans;
bool cmp(int x,int y) {return a[x]>a[y];}
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) {
		scanf("%d",&a[i]);
		mn=min(mn,a[i]);
	}
	for (int i=1;i<=n;i++) id[i]=i;
	sort(id+1,id+n+1,cmp);
	for (int i=1;i<=n;i++) L[i]=i-1,R[i]=i+1;
	for (int i=1;i<=n;i++) {
		int x=id[i];
		if (a[L[x]]!=a[x] && a[R[x]]!=a[x]) {
			ans+=a[x]-max(a[L[x]],a[R[x]]);
		}
		R[L[x]]=R[x];L[R[x]]=L[x];
	}
	printf("%lld\n",ans);
}
