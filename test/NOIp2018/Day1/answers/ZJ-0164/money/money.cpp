#include<bits/stdc++.h>
  using namespace std;

#define Abigail inline void
typedef long long LL;

const int N=25000;

void inc(int &a,int b){
  a+=b;
  if (a>2) a=2;
}

int n,a[N+9],m;
int cnt[N+9],mx;
int f[N+9];

Abigail into(){
  scanf("%d",&n);
  int x;
  mx=0;
  for (int i=1;i<=n;++i){
    scanf("%d",&x);
    ++cnt[x];
    mx=max(mx,x);
  }
}

Abigail work(){
  n=0;m=0;
  for (int i=1;i<=mx;++i)
    if (cnt[i]) a[++n]=i;
  for (int i=1;i<=n;++i){
    inc(f[a[i]],1);
    for (int j=a[i];j<=mx;++j)
      inc(f[j],f[j-a[i]]);
  }
  for (int i=1;i<=mx;++i)
    if (cnt[i]&&f[i]==1) ++m;
  for (int i=1;i<=mx;++i)
    cnt[i]=f[i]=0;
}

Abigail outo(){
  printf("%d\n",m);
}

int main(){
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  int T;
  scanf("%d",&T);
  while (T--){
    into();
    work();
    outo();
  }
  return 0;
}
