#include<bits/stdc++.h>
  using namespace std;

#define Abigail inline void
typedef long long LL;

const int N=50000,INF=1<<30;

int ans;
struct side{
  int y,next,v;
}e[N*2+9];
int lin[N+9],top,n,m;

void ins(int x,int y,int v){
  e[++top].y=y;e[top].v=v;
  e[top].next=lin[x];
  lin[x]=top;
} 

int a[N+9],b[N+9],V[N+9],Gu[N+9];

Abigail into(){
  scanf("%d%d",&n,&m);
  int v;
  for (int i=1;i<n;++i){
    scanf("%d%d%d",&a[i],&b[i],&v);
    ins(a[i],b[i],v);ins(b[i],a[i],v);
    V[b[i]]=v;Gu[i]=v;
  }
}

int dis[N+9],use[N+9];
queue<int>q;

int bfs(int x){
  for (int i=1;i<=n;++i) use[i]=0;
  int gg=x;
  dis[x]=0;use[x]=1;q.push(x);
  while (!q.empty()){
  	int t=q.front();q.pop();
	for (int i=lin[t];i;i=e[i].next)
	  if (!use[e[i].y]){
	  	use[e[i].y]=1;
	  	dis[e[i].y]=dis[t]+e[i].v;
	  	q.push(e[i].y);
	  	if (dis[gg]<dis[e[i].y]) gg=e[i].y;
	  } 
  }
  return gg;
}

Abigail work1(){
  ans=dis[bfs(bfs(1))];
}      //m=1

bool check(){
  for (int i=1;i<n;++i)
    if (a[i]+1!=b[i]) return false;
  return true;
}      //�ж�ͼ�Ƿ�Ϊ�� 

bool check1(int mid){
  int sum=0,now=0;
  for (int i=2;i<=n;++i){
    now+=V[i];
    if (now>=mid) ++sum,now=0;
  }
  return sum>=m;
}

Abigail work2(){
  ans=0;
  for (int i=29;i>=0;--i)
    if (check1(ans+(1<<i))) ans+=1<<i;
}      //��

bool check2(int mid){
  int l=1,r=n-1,sum=0;
  while (l<=r)
  	if (Gu[r]>=mid) ++sum,--r;
  	else if (l==r) ++l;
  	  else if (Gu[l]+Gu[r]>=mid){
  	    ++l,--r,++sum;
	  }else ++l;
  return sum>=m;
}

Abigail work3(){
  sort(Gu+1,Gu+n);
  ans=0;
  for (int i=29;i>=0;--i)
    if (check2(ans+(1<<i))) ans+=1<<i;
}      //�ջ�ͼ 

Abigail outo(){
  printf("%d\n",ans);
}

int main(){
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  into();
  if (m==1) work1();
  else if (check()) work2();
    else work3();
  outo();
  return 0;
}
