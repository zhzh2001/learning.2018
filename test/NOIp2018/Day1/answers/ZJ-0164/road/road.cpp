#include<bits/stdc++.h>
  using namespace std;

#define Abigail inline void
typedef long long LL;

const int N=100000;

int n;
LL a[N+9],ans;

Abigail into(){
  scanf("%d",&n);
  for (int i=1;i<=n;++i)
    scanf("%lld",&a[i]);
}

Abigail work(){
  for (int i=1;i<=n;++i)
    ans+=a[i]<a[i-1]?0:a[i]-a[i-1];
}

Abigail outo(){
  printf("%lld\n",ans);
}

int main(){
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
  into();
  work();
  outo();
  return 0;
}
