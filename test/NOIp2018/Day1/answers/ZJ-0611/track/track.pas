var
n,m,i,s:longint;
a,b,l:array[1..50000]of longint;
c1,c2:boolean;

procedure qsort(var a:array of longint);
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
    begin
       sort(1,n-1);
    end;

begin
assign(input,'track.in');
assign(output,'track.out');
reset(input);
rewrite(output);
readln(n,m);
c1:=true;c2:=true;
for i:=1 to n-1 do
begin
readln(a[i],b[i],l[i]);
if a[i]<>1 then c1:=false;
if b[i]<>a[i]+1 then c2:=false;
end;
s:=0;
if c1 then
begin
qsort(l);
for i:=n-1 downto n-m-1 do s:=s+l[i];
end;
writeln(s);
close(input);
close(output);
end.