#include<bits/stdc++.h>
#define ll long long
using namespace std;

inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-') f=-1;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

ll n,Now,Pre,ans;

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
	{
		Now=read();
		if(Now>Pre) ans+=Now-Pre;
		Pre=Now;
	}
	
	printf("%lld\n",ans);
	return 0;
}
