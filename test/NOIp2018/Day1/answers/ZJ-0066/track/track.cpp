#include<bits/stdc++.h>
#define ll long long
using namespace std;
const ll N=5e4+20;

inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-') f=-1;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

ll n,m,tot,flag_1,a[N],b[N],l[N];
ll ver[N*2],edge[N*2],nex[N*2],head[N*2];
inline void add(ll x,ll y,ll z)
{
	ver[++tot]=y;edge[tot]=z;nex[tot]=head[x];head[x]=tot;
	ver[++tot]=x;edge[tot]=z;nex[tot]=head[y];head[y]=tot;
}

struct solve1
{
	ll p,q,dist[N];
	inline void dfs(ll x,ll fa)
	{
		for(ll i=head[x];i;i=nex[i])
		if(ver[i]!=fa)
		{
			dist[ver[i]]=dist[x]+edge[i];
			dfs(ver[i],x);
		}
	}
	
	inline void work()
	{
		for(ll i=1;i<n;i++)
			add(a[i],b[i],l[i]);
		dfs(1,0);
		for(ll i=1;i<=n;i++)
		if(dist[i]>dist[p]) p=i;
		for(ll i=1;i<=n;i++) dist[i]=0;
		dfs(p,0);
		for(ll i=1;i<=n;i++)
		if(dist[i]>dist[q]) q=i;
		printf("%lld\n",dist[q]);
	}
}T;

inline void solve2()
{
	ll len=0,edg[N]={};
	std::sort(l+1,l+n);
	for(ll i=n-m*2;i<n;i++) l[++len]=l[i];
	for(ll i=1;i<=len>>1;i++)
		edg[i]=l[i]+l[len-i+1];
	len>>=1;std::sort(edg+1,edg+len+1);
	printf("%lld\n",edg[1]);
	return ;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();flag_1=1;
	for(ll i=1;i<n;i++)
	{
		a[i]=read(),b[i]=read(),l[i]=read();
		if(b[i]<a[i]) swap(a[i],b[i]);
		if(a[i]!=1) flag_1=0;
	}
	
	if(m==1) {T.work();return 0;}
	if(flag_1) {solve2();return 0;}
	for(ll i=2;i<n;i++) l[1]+=l[i];
	l[1]/=m;printf("%lld\n",l[1]);
	return 0;
}
