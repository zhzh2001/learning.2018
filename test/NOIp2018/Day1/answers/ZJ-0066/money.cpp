#include<bits/stdc++.h>
using namespace std;
const int N=120;

inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-') f=-1;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int n,m,pos,V,flag,a[N],b[N];

inline void dfs(int dep,int tmp)
{
	if(tmp==a[pos]) {flag=1;return ;}
	if(tmp>a[pos]) return ; 
	if(dep>V) return ;
	for(int i=1;i<=m;i++)
		dfs(dep+1,tmp+b[i]);
}

inline void solve1()
{
	if(a[2]%a[1]) printf("2\n");
	else printf("1\n");
	return ;
}

inline void work()
{
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	sort(a+1,a+n+1);
	
	if(a[1]==1 || n==1)
		{printf("1\n");return ;}
	if(n==2) solve1();
	b[m=1]=a[1];
	for(pos=2;pos<=n;pos++)
	{
		int tmp=a[pos];flag=0;
		for(int i=m;i;i--)
		{
			tmp%=b[i];
			if(!tmp){flag=1;break;}
		}
		if(flag) continue;
		V=ceil(1.0*a[pos]/b[1]);
		dfs(1,0);
		if(!flag) b[++m]=a[pos];
	}
	
	printf("%d\n",m);
	return ;
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--) work();
	return 0;
}
