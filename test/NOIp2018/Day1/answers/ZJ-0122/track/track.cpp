#include <iostream>
#include <algorithm>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <vector>
using namespace std;

#define LL long long
struct edge{int Next,to;LL w;};
edge G[100100];
int head[50010];
LL Dis[50010],Value[50010];
int N,M,cnt=2;
int MaxDisNode=0;
LL MaxDis=0LL,MinDis=1LL<<62LL,SumDis=0;
vector<int> PianFen2Data,PD2;
bool flagPianFen2=true;
bool flagPianFen3=true;

inline void Add_Edge(int u,int v,LL w){
	G[cnt].w=w;
	G[cnt].to=v;
	G[cnt].Next=head[u];
	head[u]=cnt++;
	return;
}

void DFSA(int u,int fa){
	if(Dis[u]>MaxDis){MaxDis=Dis[u];MaxDisNode=u;}
	for(int i=head[u];i;i=G[i].Next){
		int v=G[i].to;
		if(v==fa) continue;
		Dis[v]=Dis[u]+G[i].w;
		DFSA(v,u);
	}
	return;
}

void PianFen1(){
	DFSA(1,0);
	memset(Dis,0,sizeof(Dis));
	DFSA(MaxDisNode,0);
	printf("%lld\n",Dis[MaxDisNode]);
	return;
}

void PianFen2(){
	sort(PianFen2Data.begin(),PianFen2Data.end());
	int SIZE=(int)PianFen2Data.size()-1;
	for(int i=SIZE;i>=SIZE-M+1;--i){
		int pos=SIZE-2*M+1+SIZE-i;
		if(pos>0) PD2.push_back(PianFen2Data[i]+PianFen2Data[pos]);
		else PD2.push_back(PianFen2Data[i]);
	}
	sort(PD2.begin(),PD2.end());
	LL Res=PD2.front();
	printf("%lld\n",Res);
	return;
}

bool Judge3(LL x){
	LL Sum=0LL;
	int Count=0;
	for(int i=1;i<=N-1;++i){
		if(Sum>=x) {Sum=0LL;++Count;}
		Sum+=Value[i];
	}
	if(Sum>=x) ++Count;
	if(Count>=M) return true;
	return false;
}

void PianFen3(){
	LL Res=0LL;
	LL L=0LL,R=SumDis;
	while(L<=R){
		LL mid=(L+R)>>1LL;
		if(Judge3(mid)) {Res=max(Res,mid);L=mid+1LL;}
		else R=mid-1LL;
	}
	printf("%lld\n",Res);
	return;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	PianFen2Data.push_back(-1);
	scanf("%d%d",&N,&M);
	for(int i=1;i<=N-1;++i){
		int u,v;LL w;
		scanf("%d%d%lld",&u,&v,&w);
		Add_Edge(u,v,w);
		Add_Edge(v,u,w);
		if(u!=1 && v!=1) flagPianFen2=false;
		if(v!=u+1) flagPianFen3=false;
		PianFen2Data.push_back(w);
		Value[u]=w;
		MinDis=min(MinDis,w);
		SumDis+=w;
	}
	if(M==N-1){printf("%lld\n",MinDis);return 0;}
	if(M==1){PianFen1();return 0;}
	if(flagPianFen2){PianFen2();return 0;}
	if(flagPianFen3){PianFen3();return 0;}
	
	return 0;	
}

