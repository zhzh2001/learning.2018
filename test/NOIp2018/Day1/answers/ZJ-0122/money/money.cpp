#include <iostream>
#include <algorithm>
#include <cstring>
#include <cstdio>
#include <cstdlib>
using namespace std;

bool Exist[30000];
int Data[200];
int N,MaxNum,Ans=0;

void Solve(){
	for(int i=0;i<=MaxNum;++i)
		Exist[i]=false;
	Exist[0]=true;
	for(int i=1;i<=N;++i){
		int num=Data[i];
		if(Exist[num]) continue;
		++Ans;
		for(int j=0;j<=MaxNum-num;++j)
			if(Exist[j]) Exist[num+j]=true;
	}
	return;
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		MaxNum=-1;Ans=0;
		scanf("%d",&N);
		for(int i=1;i<=N;++i){
			scanf("%d",&Data[i]);
			MaxNum=max(MaxNum,Data[i]);
		}
		sort(Data+1,Data+N+1);
		Solve();
		printf("%d\n",Ans);
	}
	
	return 0;
}

