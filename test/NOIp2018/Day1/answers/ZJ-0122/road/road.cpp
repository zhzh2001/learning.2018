#include <iostream>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <cstdio>
using namespace std;

#define LL long long
LL Height[100100];
int N;

LL Solve(int L,int R,LL Add){
	if(L>R) return 0LL;
	if(L==R) return Height[L]-Add;
	bool flag=true;
	for(int i=L;i<=R;++i)
		if(Height[i]-Add != Height[L]-Add){flag=false;break;}
	if(flag) return Height[L]-Add;
	int PosL=L,PosR;
	LL Res=0LL;
	while(PosL<=R){
		if(Height[PosL]-Add==0){++PosL;continue;}
		PosR=PosL;
		LL Min=1LL<<62LL;
		while(PosR<=R){
			if(Height[PosR]-Add == 0) break;
			Min=min(Min,Height[PosR]-Add);
			++PosR;
		}
		--PosR;
		Res+=Min+Solve(PosL,PosR,Min+Add);
		PosL=PosR+1;
	}
	return Res;
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&N);
	for(int i=1;i<=N;++i)
		scanf("%lld",&Height[i]);
	printf("%lld\n",Solve(1,N,0LL));

	return 0;
}
