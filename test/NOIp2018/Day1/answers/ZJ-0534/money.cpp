#include<bits/stdc++.h>
using namespace std;
int T,n,hd,tl,ans,a[105],b[105],q[25005];
bool vis[25005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		ans=0;
		hd=tl=0;
		memset(vis,0,sizeof(vis));
		scanf("%d",&n);
		for (int i=1; i<=n; i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		for (int i=1; i<=n; i++){
			if (vis[a[i]]) continue;
			b[++ans]=a[i];
			q[tl++]=a[i];
			vis[a[i]]=1;
			while (hd<tl){
				int u=q[hd++];
				for (int j=1; j<=ans; j++)
					if (u+b[j]<=25000 && !vis[u+b[j]]){
						vis[u+b[j]]=1;
						q[tl++]=u+b[j];
					}
			}
		}
		printf("%d\n",ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
