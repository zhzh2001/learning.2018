#include<bits/stdc++.h>
using namespace std;
int n,m,l,r,tot;
int id,lt,top,f[50005],q[50005];
int pre[50005],nxt[50005];
int cnt,vet[100005],Next[100005],head[100005],value[100005];
bool vis[50005];
void add(int x,int y,int z){
	vet[++cnt]=y;
	value[cnt]=z;
	Next[cnt]=head[x];
	head[x]=cnt;
}
void dfs(int x,int fa,int lim){
	f[x]=0;
	for (int i=head[x]; i; i=Next[i]){
		int v=vet[i];
		if (v==fa) continue;
		dfs(v,x,lim);
	}
	top=0;
	for (int i=head[x]; i; i=Next[i]){
		int v=vet[i];
		if (v==fa) continue;
		q[++top]=f[v]+value[i];
	}
	sort(q+1,q+top+1);
	while (q[top]>=lim) tot++,top--;
	for (int i=1; i<=top; i++) vis[i]=0;
	for (int i=1; i<=top; i++) pre[i]=i-1,nxt[i]=i+1;
	id=top;
	for (int i=1; i<=top; i++)
		if (!vis[i]){
			while ((i>=id || q[i]+q[id]<lim) && nxt[id]!=top+1) id=nxt[id];
			while (pre[id]>i && q[i]+q[pre[id]]>=lim) id=pre[id];
			if (i<id && q[i]+q[id]>=lim){
				tot++;
				vis[i]=1;
				vis[id]=1;
				nxt[pre[id]]=nxt[id];
				pre[nxt[id]]=pre[id];
				nxt[pre[i]]=nxt[i];
				pre[nxt[i]]=pre[i];
				id=pre[id];
			}
		}
	for (int i=top; i>=1; i--)
		if (!vis[i]){
			f[x]=q[i];
			break;
		}
}
bool check(int x){
	tot=0;
	dfs(1,0,x);
	return tot>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<n; i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z);
		add(y,x,z);
		r=r+z;
	}
	l=1,r/=m;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (check(mid)) l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
	fclose(stdin); fclose(stdout);
	return 0;
}
