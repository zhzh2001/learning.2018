#include<bits/stdc++.h>
using namespace std;
int n,ans,a[100005],lg[100005],fm[100005][25],gm[100005][25];
void solve(int l,int r,int x){
	if (l>r) return;
	int k=lg[r-l+1];
	int t=0;
	if (fm[l][k]<fm[r-(1<<k)+1][k]) t=gm[l][k];
	else t=gm[r-(1<<k)+1][k];
	ans=ans+a[t]-x;
	solve(l,t-1,a[t]);
	solve(t+1,r,a[t]);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1; i<=n; i++) scanf("%d",&a[i]);
	for (int i=2; i<=n; i++) lg[i]=lg[i>>1]+1;
	for (int i=1; i<=n; i++) fm[i][0]=a[i],gm[i][0]=i;
	for (int j=1; (1<<j)<=n; j++)
		for (int i=1; i+(1<<j)-1<=n; i++)
			if (fm[i][j-1]<fm[i+(1<<(j-1))][j-1]){
				fm[i][j]=fm[i][j-1];
				gm[i][j]=gm[i][j-1];
			}
			else{
				fm[i][j]=fm[i+(1<<(j-1))][j-1];
				gm[i][j]=gm[i+(1<<(j-1))][j-1];
			}
	solve(1,n,0);
	printf("%d\n",ans);
	fclose(stdin); fclose(stdout);
	return 0;
}
