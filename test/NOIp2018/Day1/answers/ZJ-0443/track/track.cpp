#include <cstdio>
#include <cstring>

template <typename T>
inline void _read(T &x) {
	x = 0; T fu = 1; char c = getchar();
	while(c < '0' || c > '9') { if(c == '-') fu = -1; c = getchar(); }
	while(c >= '0' && c <= '9') { x = (x << 3) + (x << 1) + (c & 15); c = getchar(); }
	x *= fu;
}

inline int min(int x, int y) { return x < y ? x : y; }
inline int max(int x, int y) { return x > y ? x : y; }

const int N = 50005, INF = 0x7fffffff;

struct Node {
	int _size, value;
	Node *left, *right;
	Node () {}
	Node (int a, int b, Node *c, Node *d) : _size(a), value(b), left(c), right(d) {}
}*rt[N], *st[N << 1], t[N << 1], *null;

int cnt;

#define update(u) if(u -> left -> _size) u -> _size = u -> left -> _size + u -> right -> _size, u -> value = u -> right -> value
#define new_Node(a, b, c, d) (&(*st[cnt++] = Node(a, b, c, d)))
#define merge(a, b) new_Node(a -> _size + b -> _size, b -> value, a, b)

void maintain(Node *u) {
	if(u -> left -> _size > u -> right -> _size * 4) u -> right = merge(u -> left -> right, u -> right), st[--cnt] = u -> left, u -> left = u -> left -> left;
	else if(u -> right -> _size > u -> left -> _size * 4) u -> left = merge(u -> left, u -> right -> left), st[--cnt] = u -> right, u -> right = u -> right -> right;
}

void ins(Node *u, int x) {
	if(u -> _size == 1) u -> left = new_Node(1, min(u -> value, x), null, null), u -> right = new_Node(1, max(u -> value, x), null, null);
	else ins(x > u -> left -> value ? u -> right : u -> left, x);
	update(u); maintain(u);
}

void earse(Node *u, int x) {
	if(u -> left -> _size == 1 && u -> left -> value == x) st[--cnt] = u -> left, st[--cnt] = u -> right, *u = *u -> right;
	else if(u -> right -> _size == 1 && u -> right -> value == x) st[--cnt] = u -> left, st[--cnt] = u -> right, *u = *u -> left;
	else earse(x > u -> left -> value ? u -> right : u -> left, x);
	update(u); maintain(u);
}

int find(Node *u, int x) {
	if(u -> _size == 1) return u -> value;
	return x > u -> left -> _size ? find(u -> right, x - u -> left -> _size) : find(u -> left, x);
}

int Rank(Node *u, int x) {
	if(u -> _size == 1) return 1;
	return x > u -> left -> value ? Rank(u -> right, x) + u -> left -> _size : Rank(u -> left, x);
}

int lb(Node *u, int x) {
	if(u -> _size == 1) return u -> value;
	if(u -> left -> value >= x) return lb(u -> left, x);
	else return lb(u -> right, x);
}

struct Edge {
	int u, v, nxt, val;
	Edge () {}
	Edge (int a, int b, int c, int d) : u(a), v(b), nxt(c), val(d) {}
}G[N << 1];

//std::multiset <int> t[N];
//std::multiset <int> :: iterator it, itt;
int hed[N], f[N][2];
int n, m, tot, sum, x;

inline void addedge(int u, int v, int val) {
	G[++tot] = (Edge) {u, v, hed[u], val}, hed[u] = tot;
	G[++tot] = (Edge) {v, u, hed[v], val}, hed[v] = tot;
}

void dfs(int u, int fa) {
	for(register int i = hed[u]; i; i = G[i].nxt) {
		int v = G[i].v;
		if(v == fa) continue;
		dfs(v, u);
		f[u][0] += f[v][0];
		if(f[v][1] + G[i].val >= x) { f[u][0]++; continue; }
//		t[u].insert(f[v][1] + G[i].val);
		ins(rt[u], f[v][1] + G[i].val);
	}
	while(rt[u] -> _size > 1) {
		int tmp = find(rt[u], 1);
		earse(rt[u], tmp);
		int qwq = lb(rt[u], x - tmp);
		if(qwq == INF) {
			f[u][1] = tmp;
			continue;
		}
		earse(rt[u], qwq); f[u][0]++;
	}
//	std :: cout << 1 << std :: endl;
	/*for(it = t[u].begin(); it != t[u].end(); ) {
		int tmp = *it;
		itt = t[u].lower_bound(x - tmp);
		if(it == itt) itt++;
		if(itt == t[u].end()) {
			f[u][1] = tmp; it++;
			continue;
		}
		f[u][0]++; t[u].erase(itt);
		itt = it; it++; t[u].erase(itt);
	}
	t[u].clear(); */
//	std :: cout << 2 << std :: endl;
}

bool check(int mid) {
	memset(f, 0, sizeof(f)); 
	x = mid; dfs(1, 0);
	if(f[1][0] >= m) return 1;
	return 0;
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	_read(n); _read(m);
	null = new Node(0, 0, 0, 0);
	for(register int i = 1; i <= n; i++) rt[i] = new Node(1, INF, null, null);
	for(register int i = 0; i < (N << 1); i++) st[i] = &t[i];
	for(register int i = 1; i < n; i++) {
		int a, b, c;
		_read(a); _read(b); _read(c);
		addedge(a, b, c); sum += c;
	}
	int l = 0, r = sum / m + 1, ans = -1;
	while(l <= r) {
		int mid = (l + r) >> 1;
		if(check(mid)) ans = mid, l = mid + 1;
		else r = mid - 1;
	}
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
