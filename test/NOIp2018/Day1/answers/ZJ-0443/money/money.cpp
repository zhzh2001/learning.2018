#include <algorithm>
#include <cstring>
#include <cstdio>

template <typename T>
inline void _read(T &x) {
	x = 0; T fu = 1; char c = getchar();
	while(c < '0' || c > '9') { if(c == '-') fu = -1; c = getchar(); }
	while(c >= '0' && c <= '9') { x = (x << 3) + (x << 1) + (c & 15); c = getchar(); }
	x *= fu;
}

const int N = 105, M = 25005;

bool f[M];
int a[N];
int T, n, ans, maxn;

inline int max(int x, int y) { return x > y ? x : y; }

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	_read(T);
	while(T--) {
		memset(f, 0, sizeof(f)); ans = maxn = 0;
		_read(n); f[0] = 1;
		for(register int i = 1; i <= n; i++) _read(a[i]), maxn = max(maxn, a[i]);
		std::sort(a + 1, a + n + 1);
		for(register int i = 1; i <= n; i++) {
			if(!f[a[i]]) {
				ans++;
				for(register int j = a[i]; j <= maxn; j++) if(f[j - a[i]]) f[j] = 1;
			}
		}
		printf("%d\n", ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
