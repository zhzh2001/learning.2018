#include <cstdio>

template <typename T>
inline void _read(T &x) {
	x = 0; T fu = 1; char c = getchar();
	while(c < '0' || c > '9') { if(c == '-') fu = -1; c = getchar(); }
	while(c >= '0' && c <= '9') { x = (x << 3) + (x << 1) + (c & 15); c = getchar(); }
	x *= fu;
}

const int N = 1e5 + 5;

int a[N];
int n, ans;

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	_read(n);
	for(register int i = 1; i <= n; i++) _read(a[i]);
	for(register int i = 1; i < n; i++) {
		if(a[i] <= a[i + 1]) continue;
		ans += (a[i] - a[i + 1]);
	}
	ans += a[n]; printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
