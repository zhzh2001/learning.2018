#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int MAX_N=10+1e5;
struct Edge{
	int to,nxt,key;
}edge[MAX_N*2];
int head[MAX_N],top_edge=-1;
void add_edge(int x,int y,int key){
	edge[++top_edge]=(Edge){y,head[x],key};
	head[x]=top_edge;
}
int f[MAX_N],g[MAX_N];
int a[MAX_N],b[MAX_N],top;
void dfs(int x,int pre,int std,int dis){
	f[x]=0;
	for(int j=head[x];j!=-1;j=edge[j].nxt){
		int y=edge[j].to;
		if(y!=pre){
			dfs(y,x,std,edge[j].key);
			f[x]+=f[y];
		}
	}
	top=0;
	for(int j=head[x];j!=-1;j=edge[j].nxt){
		int y=edge[j].to;
		if(y!=pre) a[++top]=g[y];
	}
	sort(a+1,a+top+1);
	while(a[top]>=std) --top,++f[x];
	for(int i=1;i<=top;++i) b[i]=a[i];
	int poi=0;
	for(int i=1;i<=top;++i)
		if(a[i]>=(std+1>>1)){
			poi=i-1;
			break;
		}
	int poi1=poi+1;
	for(;poi>0;--poi){
		while(poi1<=top&&a[poi]+a[poi1]<std) ++poi1;
		if(poi1<=top) a[poi]=-1,a[poi1]=-1,++f[x];
		else break;
	}
	int last=0;
	for(int i=1;i<=top;++i)
		if(a[i]>=(std+1>>1)){
			if(last==0) last=i;
			else{
				a[last]=-1;
				a[i]=-1;
				++f[x];
				last=0;
			}
		}
	poi1=top;
	if(last==0&&poi>0){
		while(b[poi]<(std+1>>1)){ 
			if(b[poi]+b[poi1]<std) break;
			++poi,--poi1;
		}
		g[x]=b[poi];
	}else{
		g[x]=0;
		for(int i=1;i<=top;++i)
			g[x]=max(g[x],a[i]);
	}
	g[x]+=dis;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n,m; scanf("%d%d",&n,&m);
	memset(head,-1,sizeof(head));
	for(int i=1;i<n;++i){
		int u,v,key; scanf("%d%d%d",&u,&v,&key);
		add_edge(u,v,key),add_edge(v,u,key);
	}
	int left=0,right=(int)5e8+1;
	while(left<right){
		int mid=left+right+1>>1;
		dfs(1,0,mid,0);
		if(f[1]>=m) left=mid;
		else right=mid-1;
	}
	printf("%d\n",left);
	return 0;
}
