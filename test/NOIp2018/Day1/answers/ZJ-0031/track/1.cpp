#include<cstdio>
#include<cstring>
#include<algorithm>
#include<set>
using namespace std;
const int MAX_N=5+1e5;
struct Edge{
	int to,nxt,key;
}edge[MAX_N*2];
int head[MAX_N],top_edge=-1;
void add_edge(int x,int y,int key){
	edge[++top_edge]=(Edge){y,head[x],key};
	head[x]=top_edge;
}
int f[MAX_N],g[MAX_N];
int a[MAX_N],nxt[MAX_N],pr[MAX_N];
multiset<int> s;
int dfs(int x,int pre,int std,int dis){
	f[x]=0;
	for(int j=head[x];j!=-1;j=edge[j].nxt){
		int y=edge[j].to;
		if(y!=pre){
			dfs(y,x,std,edge[j].key);
			f[x]+=f[y];
		}
	}
	s.clear();	
	for(int j=head[x];j!=-1;j=edge[j].nxt){
		int y=edge[j].to;
		if(y!=pre){
			if(g[y]>=std) ++f[x];
			else s.insert(g[y]);
		}
	}
	g[x]=0;
	while(s.size()>1){
		int key=*s.begin();
		if(s.lower_bound(std-key)==s.end()){
			g[x]=max(g[x],key);
			s.erase(s.begin()); 
		}else{
			if(s.lower_bound(std-key)==s.begin()){
				multiset<int>::iterator iter=s.begin();
				iter++;
			 	s.erase(iter);
			}
			else
				s.erase(s.lower_bound(std-key));
			s.erase(s.begin());
			++f[x];
		}
	}
	if(!s.empty()) g[x]=max((*s.begin()),g[x]);
	g[x]+=dis;
	return f[x];
}
int main(){
	freopen("track.in","r",stdin);
	freopen("1.out","w",stdout);
	int n,m; scanf("%d%d",&n,&m);
	memset(head,-1,sizeof(head));
	for(int i=1;i<n;++i){
		int u,v,key; scanf("%d%d%d",&u,&v,&key);
		add_edge(u,v,key),add_edge(v,u,key);
	}
//	dfs(1,0,15,0);
//	for(int i=1;i<=n;++i) printf("(%d %d)",f[i],g[i]);
	int left=0,right=(int)1e9+1,ans=0;
	while(left<=right){
		int mid=left+right>>1;
		if(dfs(1,0,mid,0)>=m){
			ans=max(ans,mid);
			left=mid+1;
		}
		else right=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
