#include<cstdio>
#include<algorithm>
using namespace std;
const int MAX_N=5+1e5;
typedef long long ll;
ll a[MAX_N];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n; scanf("%d",&n);
	for(int i=1;i<=n;++i) scanf("%lld",&a[i]);
	ll ans=0,last=0;
	for(int i=1;i<=n;++i){
		if(a[i]>last) ans+=a[i]-last;
		last=a[i];
	}
	printf("%lld",ans);
	return 0;
}

