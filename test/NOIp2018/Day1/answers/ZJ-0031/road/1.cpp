#include<cstdio>
#include<algorithm>
using namespace std;
const int MAX_N=5+1e5;
typedef long long ll;
class STTable{
	pair<int,int> st[25][MAX_N];
	int p[MAX_N];
public:
	void build(int* a,int n){
		for(int i=1;i<=n;++i) st[0][i]=make_pair(a[i],i);
		for(int l=1;l<=20;++l)
			for(int i=1;i<=n-(1<<l)+1;++i)
				st[l][i]=min(st[l-1][i],st[l-1][i+(1<<l-1)]);
		p[1]=0;
		for(int i=2;i<=n;++i) p[i]=p[i>>1]+1;
	}
	int query(int l,int r){
		int level=p[r-l+1];
		return min(st[level][l],st[level][r-(1<<level)+1]).second;
	}
}st;
int a[MAX_N];
ll solve(int l,int r,int std){
	if(l>r) return 0;
	int mid=st.query(l,r);
	return solve(l,mid-1,a[mid])+solve(mid+1,r,a[mid])+a[mid]-std;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("1.out","w",stdout);
	int n; scanf("%d",&n);
	for(int i=1;i<=n;++i) scanf("%d",&a[i]);
	st.build(a,n);
	printf("%lld",solve(1,n,0));
	return 0;
}
