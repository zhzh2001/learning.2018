#include<cstdio>
#include<algorithm>
using namespace std;
const int MAX_N=300005;
const int maxa=3000;
bool f[MAX_N],g[MAX_N];
int a[MAX_N],n;
void solve(bool* f,int sta,int key){
	if(f[key]||key>maxa) return;
	f[key]=true;
	for(int i=1;i<=n;++i) 
		if((sta&(1<<i-1))>0) 
			solve(f,sta,key+a[i]);
}
int count(int x){
	int i=0;
	while(x>0) x-=x&-x,++i;
	return i;
}
bool judge(){
	for(int i=1;i<=maxa;++i)
		if(f[i]!=g[i]) return false;
	return true;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("1.out","w",stdout);
	int T; scanf("%d",&T);
	while(T--){
		int ans=MAX_N; scanf("%d",&n);
		for(int i=1;i<=n;++i) scanf("%d",&a[i]);
		for(int i=0;i<=maxa;++i) f[i]=false;
		solve(f,(1<<n)-1,0);
		for(int sta=0;sta<(1<<n);++sta){
			for(int i=0;i<=maxa;++i) g[i]=false;
			solve(g,sta,0);
			if(judge()) ans=min(ans,count(sta));
		}
		printf("%d\n",ans);
	}
	return 0;
}
