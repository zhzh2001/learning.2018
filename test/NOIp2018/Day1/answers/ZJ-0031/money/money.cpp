#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int MAX_N=25005;
bool f[MAX_N];
int a[MAX_N],maxa;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T; scanf("%d",&T);
	while(T--){
		int n,m=0; scanf("%d",&n);
		for(int i=1;i<=n;++i){
			scanf("%d",&a[i]);
			maxa=max(maxa,a[i]);
		}
		sort(a+1,a+n+1);
		for(int i=1;i<=maxa;++i) f[i]=false;
		f[0]=true;
		for(int i=1;i<=n;++i){
			if(!f[a[i]]){
				++m;
				for(int j=0;j<=maxa;++j)
					if(j+a[i]<=maxa&&f[j]) f[j+a[i]]=true;
			}
		}
		printf("%d\n",m);
	}
	return 0;
}

