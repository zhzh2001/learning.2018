#include<bits/stdc++.h>
using namespace std;
int n;
int d[100005];
int f[100005][19];
int p[100005];
int ans = 0;
int query(int l , int r)
{
	int k = p[r - l + 1];
	return min(f[l][k] , f[r - (1<<k) + 1][k]);
}
int next(int pos,int v,int r)
{
	if(query(pos , r) > v) return r + 1;
	int l = pos;
	while(l < r){
		int mid = (l + r >> 1);
		if(query(pos , mid) == v) r = mid;
		else l = mid + 1;
	}
	return l;
}
void solve(int l,int r,int mis)
{
	//printf("Solve %d %d , %d\n",l,r,mis);
	if(r < l) return;
	if(r == l){
		ans += d[l] - mis;return;
	}
	int mn = query(l , r);ans += (mn - mis);
	int st = l;
	while(st <= r){
		int pt = next(st , mn , r);
		solve(st , pt - 1 , mn);
		st = pt + 1;
	}
	return;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i = 1;i <= n;i++) scanf("%d",&d[i]);
	for(int i = 1;i <= n;i++) f[i][0] = d[i];
	for(int i = 1;i <= 18;i++){
		for(int j = 1;j <= n;j++){
			if(j + (1<<i-1) > n) f[j][i] = f[j][i - 1];
			else f[j][i] = min(f[j][i - 1] , f[j + (1<<i-1)][i - 1]);
		}
	}
	p[1] = 0;
	for(int i = 2;i <= n;i++){
		if((1<<p[i - 1])*2 >= i) p[i] = p[i - 1];
		else p[i] = p[i - 1] + 1;
	}
	solve(1 , n , 0);
	cout<<ans<<endl;
	return 0;
}
