#include<bits/stdc++.h>
using namespace std;
int t;
int n;
int a[101];
int gd[101][101];
bool can[25005];
int exgcd(int a,int b,int &x,int &y)
{
	if(b == 0){
		x = 1;y = 0;return a;
	}
	int d = exgcd(b,a%b,x,y);
	int t = y;y = (d - y*a)/b;x = t;
	return d;
}
vector<int> b;
int gcd(int a,int b)
{
	return a % b == 0 ? b : gcd(b , a % b);
}
int lcm(int a,int b)
{
	return a*b / gcd(a,b);
}
int up(int a,int b)
{
	if(a % b == 0) return a / b;
	double p = (double)a / b;
	if(p >= 0) return (int)p + 1;
	else return (int)p;
}
int down(int a,int b)
{
	if(a % b == 0) return a / b;
	double p = (double)a / b;
	if(p >= 0)return (int)p;
	else return (int)p - 1;
}
int mx;
int getb(int p)
{
	int x,y,l,r;
	for(int i = 0;i < b.size();i++){
		if(p % b[i] == 0) return 1;
		for(int j = i + 1;j < b.size();j++){
			int g = exgcd(b[i] , b[j] , x , y);
			if(p % g) continue;
			l = up(p * (-x) , b[j]) ; r = down(y * p , b[i]);
			if(l <= r) return 1;
		}
	}
	return 0;
}
void goes()
{
	int ans = n;
	for(int i = 1;i <= n;i++){
		b.clear();
		for(int j = 1;j <= n;j++){
			if(i != j) b.push_back(a[j]);
		}
		if(getb(a[i])) ans--;
	}
	printf("%d\n",ans);return;
}
void solve()
{
	scanf("%d",&n);mx = 0;
	for(int i = 1;i <= n;i++) scanf("%d",&a[i]);
	if(n == 2){
		if(max(a[1] , a[2]) % min(a[1] , a[2]) == 0) puts("1");
		else puts("2");
		return;
	}
	for(int i = 1;i <= n;i++) mx = max(mx , a[i]);
	goes();return;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	cin>>t;
	while(t--) solve();
}
