#include<bits/stdc++.h>
using namespace std;
int n , m;
struct edge
{
	int t , c;
};
vector<edge> E[50005];
int d[50005];
int bfs(int s)
{
	for(int i = 1;i <= n;i++) d[i] = 1e9;
	d[s] = 0;
	queue<int> q;q.push(s);
	while(q.size()){
		int u = q.front();q.pop();
		for(int i = 0;i < E[u].size();i++){
			if(d[E[u][i].t] > d[u] + E[u][i].c){
				d[E[u][i].t] = d[u] + E[u][i].c;
				q.push(E[u][i].t);
			}
		}
	}
	int mx = 1;
	for(int i = 2;i <= n;i++){
		if(d[i] > d[mx]) mx=i;
	}
	return mx;
}
void goes1()
{
	int t = bfs(1);
	int s = bfs(t);
	printf("%d\n",d[s]);
	return;
}
vector<int> sp;
bool pd[50005];
int L[50005] , R[50005];
void split(int x)
{
	L[R[x]] = L[x];
	R[L[x]] = R[x];
	return;
}
bool check(int q)
{
	memset(pd , 0 , sizeof(pd));
	int ned = sp.size() - m;
	if(ned == 0){
		return (sp[0] >= q);
	}
	int np = 0 , r = sp.size() - 1;
	if(sp[0] + sp[r] < q) return 0;
	for(int i = 1;i < n;i++) L[i] = i - 1 , R[i] = i + 1;
	while(ned){
		if(pd[np]) {np++;continue;}
		if(sp[np] >= q) return 1;
		while(r - 1 > np && (sp[r - 1] + sp[np]) >= q) r--;
		if(R[r + 1] == n && pd[r]) return 0;
		if(pd[r]) r = R[r + 1] - 1;
		//printf("PP %d %d\n",np,r);
		pd[np] = 1;pd[r] = 1;split(np+1);split(r+1);ned--;np++;
	}
	//cout<<endl;
	if((sp.size() - m) *2== sp.size()) return 1;
	while(pd[np]) np++;
	if(sp[np] >= q) return 1;
	return 0;
}
void goes2()
{
	int l = 1 , r = 5e8;
	sort(sp.begin() , sp.end());
	while((sp.size() - m) * 2 > sp.size()) sp.erase(sp.begin());
	while(l < r){
		int mid = (l + r >> 1) + 1;
		if(check(mid)) l = mid;
		else r = mid - 1;
	}
	///puts("Q");
	//cout<<check(10)<<endl;
	cout<<l<<endl;return;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool f = 1;
	for(int i = 1;i < n;i++){
		int u , v ,c;scanf("%d%d%d",&u,&v,&c);
		if(u != 1) f= 0;
		sp.push_back(c);
		edge p;p.t = v;p.c = c;
		E[u].push_back(p);p.t = u;E[v].push_back(p);
	}
	if(m == 1){
		goes1();return 0;
	}
	if(f){
		goes2();return 0;
	}
}
