#include <bits/stdc++.h>
#define N 200001
using namespace std;
int E,n,m,to[N*2],nex[N*2],fir[N],wei[N*2];
int fa[N],w[N],tem[N],ret[N],le[N];
void add(int p,int q,int o)
{
	to[++E]=q;nex[E]=fir[p];fir[p]=E;wei[E]=o;
}
void build(int now,int fat)
{
	fa[now]=fat;
	for(int i=fir[now];i;i=nex[i])
	if(to[i]!=fat)
	{
		w[to[i]]=wei[i];
		build(to[i],now);
	}
}
int quchu(int t,int len,int tar)
{
	int mem=tem[t];
	for(int i=t;i<len;i++)
		tem[i]=tem[i+1];
	int ret=0;
	for(int i=len-1,j=1;j<i;i--)
	{
		while(j<i && tem[i]+tem[j]<tar) j++;
		if(j<i) ret++,j++;
	}
	for(int i=len;i>t;i--)
		tem[i]=tem[i-1];
	tem[t]=mem;
	return ret;
}
void check(int now,int x)
{
	ret[now]=0;
	for(int i=fir[now];i;i=nex[i])
	if(to[i]!=fa[now])
		check(to[i],x),ret[now]+=ret[to[i]];
	int len=1;tem[1]=0;
	for(int i=fir[now];i;i=nex[i])
	if(to[i]!=fa[now])
	{
		if(le[to[i]]+w[to[i]]>=x) ret[now]++;
		else
		tem[++len]=le[to[i]]+w[to[i]];
	}
	if(len>1)
	{
		sort(tem+1,tem+len+1);
		int bes=quchu(1,len,x);ret[now]+=bes;
		int l=1,r=len;
		for(int mid=l+r>>1;l<r;mid=l+r>>1)
		if(quchu(mid+1,len,x)==bes) l=mid+1;
		else r=mid;
		le[now]=tem[l];
	}
	else
		le[now]=0;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++)
	{
		int p,q,o;
		scanf("%d%d%d",&p,&q,&o);
		add(p,q,o),add(q,p,o);
	}
	build(1,0);
	int l=1,r=n*10000;
	for(int mid=l+r>>1;l<r;mid=l+r>>1)
	{
		check(1,mid+1);
		if(ret[1]>=m) l=mid+1;
		else r=mid;
	}
	printf("%d\n",l);
	return 0;
}
