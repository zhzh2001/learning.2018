#include <bits/stdc++.h>
using namespace std;
int T,n,a[201],vis[30001];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(scanf("%d",&T);T;T--)
	{
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		for(int i=1;i<=25000;i++)
			vis[i]=0;
		vis[0]=1;
		int ret=0;
		for(int i=1;i<=n;i++)
		if(!vis[a[i]])
		{
			ret++;
			for(int j=a[i];j<=25000;j++)
				vis[j]|=vis[j-a[i]];
		}
		printf("%d\n",ret);
	}
	return 0;
}
