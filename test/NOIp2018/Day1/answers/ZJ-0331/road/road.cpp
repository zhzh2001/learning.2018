#include<cstdio>
#include<cstring>

int n,d[100001],ans;

int getabs(int x) {return (x<0)?(-x):x;} 

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n); d[0]=0;
	for (int i=1; i<=n; i++) scanf("%d",&d[i]),ans+=getabs(d[i]-d[i-1]);
	ans+=d[n]; printf("%d\n",(ans>>1));
	return 0;
}
