#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

int l,r,mid,ans,fa[50011],f[50010],g[50010],t[50010],ri[50010],n,hd[50010],tn,cnt,m,L,R,Mid,Ans,x,y,z,sum,v[50010];

struct node
{
	int to,next,val;
}e[100101];

void addedge(int x,int y,int z)
{
	e[++cnt].next=hd[x];
	hd[x]=cnt;
	e[cnt].to=y;
	e[cnt].val=z;
}

int ask(int x) {return (ri[x]==x)?(x):(ri[x]=ask(ri[x]));}

void dfs(int x)
{
	f[x]=0;
	for (int i=hd[x]; i; i=e[i].next)
		if (e[i].to!=fa[x]) fa[e[i].to]=x,v[e[i].to]=e[i].val,dfs(e[i].to),f[x]+=f[e[i].to];
	tn=0;
	for (int i=hd[x]; i; i=e[i].next) 
		if (e[i].to!=fa[x]) t[++tn]=g[e[i].to],ri[tn]=tn;
	ri[tn+1]=tn+1;
	sort(t+1,t+1+tn);
	while (tn&&t[tn]>=Mid) tn--,f[x]++;
	for (int i=1; i<=tn; i++)
		if (ask(i)==i)
		{
			l=i+1; r=tn; ans=tn+1;
			while (l<=r)
			{
				mid=(l+r)>>1;
				if (t[mid]+t[i]>=Mid) ans=mid,r=mid-1; else l=mid+1;
			}
			ans=ask(ans);
			if (ans<=tn) ri[i]=i+1,ri[ans]=ans+1,f[x]++;
		}
	g[x]=v[x];
	for (int i=1; i<=tn; i++)
		if (ask(i)==i) g[x]=t[i]+v[x];
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m); cnt=0;
	memset(hd,0,sizeof(hd)); sum=0;
	for (int i=1; i<n; i++) scanf("%d%d%d",&x,&y,&z),addedge(x,y,z),addedge(y,x,z),sum+=z;
	L=1; R=sum; Ans=0; fa[1]=v[1]=0;
	while (L<=R)
	{
		Mid=(L+R)>>1; dfs(1);
		if (f[1]>=m) L=Mid+1,Ans=Mid; else R=Mid-1;
	}
	printf("%d\n",Ans);
	return 0;
}
