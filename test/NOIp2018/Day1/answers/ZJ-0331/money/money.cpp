#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

const int inf=1000000000;
int n,a[1001],T,f[25001],nw,p,ans,g,mi,mii,st;

int gcd(int a,int b) {return (!b)?a:gcd(b,a%b);}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		for (int i=1; i<=n; i++) scanf("%d",&a[i]);
		sort(a+1,a+1+n); f[0]=0; ans=1;
		for (int i=1; i<a[1]; i++) f[i]=inf;
		for (int i=2; i<=n; i++)
		{
			p=a[i]%a[1]; nw=a[i];
			if (f[p]<=nw) continue;
			ans++; g=gcd(a[1],p);
			for (int k=0; k<g; k++)
			{
				mi=f[k]; mii=k; st=((k+p>=a[1])?(k+p-a[1]):(k+p));
				for (int j=st; j!=k; j=((j+p>=a[1])?(j+p-a[1]):(j+p)))
					if (f[j]<mi) mi=f[j],mii=j;
				if (mi==inf) continue;
				nw=mi+a[i]; st=((mii+p>=a[1])?(mii+p-a[1]):(mii+p));
				for (int j=st; j!=mii; j=((j+p>=a[1])?(j+p-a[1]):(j+p)))
				{
					if (nw>f[j]) nw=f[j]; else f[j]=nw;
					nw+=a[i];
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
