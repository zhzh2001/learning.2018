#include <bits/stdc++.h>
#define MAXN 100005
#define INF 0X3F3F3F3F
#define rep(i,x,y) for (int i=x;i<=y;i++)
#define per(i,x,y) for (int i=x;i>=y;i--)
using namespace std;

typedef long long ll;
ll n,x,pre,cnt,sum,res,a[MAXN],dep[10005];

int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%lld",&n);
	scanf("%lld",&pre);
	a[++cnt]=pre;
	rep(i,2,n) {
		scanf("%lld",&x);
		if (x!=pre) a[++cnt]=x;
		pre=x;
	}
	n=cnt;
	rep(i,1,n) {
		if (a[i]>a[i-1]&&a[i]>a[i+1])
			dep[a[i]]--;
		if (a[i]<a[i-1]&&a[i]<a[i+1])
			dep[a[i]]++;
	}
	sum=1;
	rep(i,0,10000) {
		sum+=dep[i];
		res+=sum;
	}
	printf("%lld\n",res);
	return 0;
}
