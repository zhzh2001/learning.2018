#include <bits/stdc++.h>
#define MAXN 105
#define INF 0X3F3F3F3F
#define rep(i,x,y) for (int i=x;i<=y;i++)
#define per(i,x,y) for (int i=x;i>=y;i--)
using namespace std;

bool f[25005];
int T,n,cnt,a[MAXN];

int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		cnt=0;
		memset(f,false,sizeof(f));
		f[0]=true;
		scanf("%d",&n);
		rep(i,1,n) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		rep(i,1,n) {
			if (f[a[i]]) cnt++;
			else
				rep(j,a[i],25000)
					if (f[j-a[i]]) f[j]=true;
		}
		printf("%d\n",n-cnt);
	}
	return 0;
}

