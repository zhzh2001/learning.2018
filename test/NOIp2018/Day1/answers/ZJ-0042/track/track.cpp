#include <bits/stdc++.h>
#define MAXN 50005
#define INF 0X3F3F3F3F
#define rep(i,x,y) for (int i=x;i<=y;i++)
#define per(i,x,y) for (int i=x;i>=y;i--)
using namespace std;

struct Edge {
	int to,val,nxt;
} edge[MAXN<<1];

bool vis[MAXN],flag1,flag2;
int n,m,x,y,l,r,mid,xx,st,ed,val,Max,eNum,b[MAXN],dis[MAXN],deg[MAXN],son[MAXN],head[MAXN];

void add(int x,int y,int val) {
	edge[++eNum].to=y;
	edge[eNum].val=val;
	edge[eNum].nxt=head[x];
	head[x]=eNum;
}

void bfs1(int x) {
	int t,w;
	memset(vis,false,sizeof(vis));
	t=w=1;
	b[1]=x;
	vis[x]=true;
	while (t<=w) {
		for (int i=head[b[t]];~i;i=edge[i].nxt)
			if (!vis[edge[i].to]) {
				w++;
				b[w]=edge[i].to;
				vis[edge[i].to]=true;
				son[b[t]]=edge[i].to;
				dis[edge[i].to]=dis[b[t]]+edge[i].val;
			}
		t++;
	}
	ed=b[w];
}

void bfs(int x) {
	int t,w;
	memset(vis,false,sizeof(vis));
	t=w=1;
	b[1]=x;
	dis[x]=0;
	vis[x]=true;
	while (t<=w) {
		for (int i=head[b[t]];~i;i=edge[i].nxt) {
			if (!vis[edge[i].to]) {
				w++;
				b[w]=edge[i].to;
				vis[edge[i].to]=true;
				dis[edge[i].to]=dis[b[t]]+edge[i].val;
				if (dis[edge[i].to]>Max) {
					Max=dis[edge[i].to];
					st=edge[i].to;
				}
			}
		}
		t++;
	}
}

bool solve(int len) {
	int x=st,pre=st,cnt=0;
	while (x!=ed) {
		while (x!=ed&&dis[x]-dis[pre]<len) x=son[x];
		if (x==ed) {
			if (dis[x]-dis[pre]>=len) cnt++;
		} else {
			cnt++;
			pre=x;
		}
		if (cnt>=m) return true;
	}
	if (cnt>=m) return true;
	return false;
}

int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(head,-1,sizeof(head));
	scanf("%d%d",&n,&m);
	flag1=flag2=true;
	rep(i,1,n-1) {
		scanf("%d%d%d",&x,&y,&val);
		xx=max(xx,val);
		add(x,y,val),add(y,x,val);
		if (y!=x+1) flag1=false;
		if (x!=1) flag2=false;
		deg[x]++,deg[y]++;
	}
	if (m==1) {
		bfs(1);
		bfs(st);
		printf("%d\n",Max);
	} else if (flag1) {
		rep(i,1,n)
			if (deg[i]==1) {
				st=i;
				break;
			}
		bfs1(st);
		l=0,r=dis[ed];
		while (l<r) {
			mid=((l+r)>>1)+1;
			if (solve(mid)) l=mid;
			else r=mid-1;
		}
		if (solve(l+1)) printf("%d\n",l+1);
		else printf("%d\n",l);
	} else {
		printf("%d\n",xx);
	}
	return 0;
}

