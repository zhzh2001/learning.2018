#include <bits/stdc++.h>
#define il inline
#define rg register
#define ll long long
#define getc getchar
#define putc putchar
#define rep(i,l,r) for (register int i = l; i <= r; ++i)
namespace ringo {

template < class T >
il void read(T &x) {
	x = 0; rg char c = getc(); rg bool f = 0;
	while (!isdigit(c)) f ^= c == '-', c = getc();
	while (isdigit(c)) x = x * 10 + c - '0', c = getc();
	if (f) x = -x;
}

template < class T >
il void print(T x) {
	if (x < 0) putc('-'), x = -x;
	if (x > 9) print(x / 10);
	putc(x % 10 + '0');
}

const int maxn = 110, M = 25010;
int T, n, ans;
int a[maxn];
bool f[M];

void main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	read(T);
	while (T--) {
		ans = 0;
		memset(f, 0, sizeof(f));
		read(n); rep(i, 1, n) read(a[i]);
		std::sort(a + 1, a + n + 1);
//		printf("=== %d ===\n", n);
		f[0] = 1;
		for (int i = 1; i <= n; i++) {
			if (f[a[i]]) ans++;
			for (int j = 0; j < M; j++)
				if (f[j] && j + a[i] < M)
					f[j + a[i]] = 1;
		}
		ans = n - ans;
		print(ans), putc('\n');
	}
}
	
} int main() { return ringo::main(), 0; }
