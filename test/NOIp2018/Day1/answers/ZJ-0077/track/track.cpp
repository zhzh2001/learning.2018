#include<cstdio>
#include<algorithm>
#define maxn 50005
using namespace std;
int read(){
	char c=getchar();
	int x=0;
	bool sgn=0;
	while((c<'0'||c>'9')&&c!='-')c=getchar();
	if(c=='-')sgn=1,c=getchar();
	while(c>='0'&&c<='9'){
		x=(x<<3)+(x<<1)+c-'0';
		c=getchar();
	}
	return x;
}
void write(int x){
	if(x<0)putchar('-'),write(-x);
	else{
		if(x>=10)write(x/10);
		putchar(x%10+'0');
	}
}
struct edge{
	int to,next,w;
};
edge e[maxn<<1];
int head[maxn],cnte;
void add(int u,int v,int w){
	e[++cnte].to=v;
	e[cnte].w=w;
	e[cnte].next=head[u];
	head[u]=cnte;
}
int n,m;
namespace GETD{
	int ansi,ans,now;
	void dfs(int u,int last){
		if(now>ans){
			ans=now;
			ansi=u;
		}
		for(int i=head[u];i;i=e[i].next){
			int v=e[i].to;
			if(v==last)continue;
			now+=e[i].w;
			dfs(v,u);
			now-=e[i].w;
		}
	}
	int getD(){
		ans=-1;
		dfs(1,0);
		ans=-1;
		dfs(ansi,0);
		return ans;
	}
}
namespace WWW{
	int W[maxn];
	int getWWW(){
		int cntw=0,ans=1000000000;
		for(int i=1;i<=cnte;i+=2){
			W[++cntw]=e[i].w;
		}
		sort(W+1,W+cntw+1);
		if(cntw&1){
			cntw--;
			ans=W[cntw];
		}
		for(int i=1;i<=(cntw>>1);i++){
			ans=min(ans,W[i]+W[n-i+1]);
		}
		return ans;
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	bool aieq1=1;
	for(int i=1;i<n;i++){
		int u=read(),v=read(),w=read();
		if(u!=1&&v!=1)aieq1=0;
		add(u,v,w);
		add(v,u,w);
	}
	if(m==1){
		write(GETD::getD());
		return 0;
	}
	if(m==n-1){
		int ans=1000000000;
		for(int i=1;i<=cnte;i+=2)if(e[i].w<ans)ans=e[i].w;
		write(ans);
		return 0;
	}
	if(aieq1){
		write(WWW::getWWW());
		return 0;
	}
	return 0;
}
