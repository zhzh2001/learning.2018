#include<cstdio>
#define maxn 100010
using namespace std;
int abs(int x){return x<0?-x:x;}
int read(){
	char c=getchar();
	int x=0;
	bool sgn=0;
	while((c<'0'||c>'9')&&c!='-')c=getchar();
	if(c=='-')sgn=1,c=getchar();
	while(c>='0'&&c<='9'){
		x=(x<<3)+(x<<1)+c-'0';
		c=getchar();
	}
	return x;
}
void write(int x){
	if(x<0)putchar('-'),write(-x);
	else{
		if(x>=10)write(x/10);
		putchar(x%10+'0');
	}
}
int a[maxn],n;
long long ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	a[0]=0;
	for(int i=1;i<=n;i++){
		a[i]=read();
		ans+=abs(a[i]-a[i-1]);
	}
	ans+=abs(a[n]);
	write(ans/2);
	putchar('\n');
	return 0;
}
