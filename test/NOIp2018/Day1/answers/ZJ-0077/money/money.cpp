#include<cstdio>
#include<algorithm>
#define maxn 110
#define P pair<int,bool>
using namespace std;
int read(){
	char c=getchar();
	int x=0;
	bool sgn=0;
	while((c<'0'||c>'9')&&c!='-')c=getchar();
	if(c=='-')sgn=1,c=getchar();
	while(c>='0'&&c<='9'){
		x=(x<<3)+(x<<1)+c-'0';
		c=getchar();
	}
	return x;
}
void write(int x){
	if(x<0)putchar('-'),write(-x);
	else{
		if(x>=10)write(x/10);
		putchar(x%10+'0');
	}
}
int a[maxn],n;
bool b[50010],dp[50010];
P q[1000010];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--){
		n=read();
		int* Start=a+1;
		int* End=a+n+1;
		for(int* i=Start;i!=End;i++)*i=read();
		sort(a+1,a+n+1);
		int base=a[1],ans=n,qtail=0;
		bool* Start2=b;
		bool* End2=b+base;
		for(bool* i=Start2;i!=End2;i++)*i=0;
		Start2=dp;End2=dp+25001;
		for(bool* i=Start2;i!=End2;i++)*i=0;
		b[0]=1;
		dp[0]=1;
		Start=a+2;End=a+n+1;
		for(int* i=Start;i!=End;i++){
			q[++qtail]=P(*i,1);
			dp[*i]=1;
//printf("in %d\n",q[qtail]);
			int ub=min(25000,base*(*i)-1);
			for(int j=*i+1;j<=ub;j++){
				if(dp[j-*i]&&!dp[j]){
					dp[j]=1;
					q[++qtail]=P(j,0);
				}
//printf("in %d\n",q[qtail]);
			}
		}
		sort(q+1,q+qtail+1);
		qtail=unique(q+1,q+qtail+1)-(q+1);
//for(int i=1;i<=qtail;i++)printf("%d,%d ",q[i].first,q[i].second);puts("");
		P* Start1=q+1;
		P* End1=q+qtail+1;
		for(P* i=Start1;i!=End1;i++){
			if(b[i->first%base]&&i->second){
				ans--;
//printf("%d already\n",q[i].first);
			}
			else{
				b[i->first%base]=1;
//printf("%d ok\n",q[i].first);
			}
		}
		write(ans);
		putchar('\n');
	}
	return 0;
}
