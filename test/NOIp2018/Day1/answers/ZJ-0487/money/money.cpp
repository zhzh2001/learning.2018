#include <bits/stdc++.h>
using namespace std;
const int maxn = 100/.9;
const int maxw = 25000/.9;
const int W = 25000;
unsigned short dp[maxw];
int a[maxn];
int n;
int T;
int main() {
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  cin.sync_with_stdio(false);
  cin.tie(0);
  cin>>T;
  for(int _ = 1; _<=T;++_) {
    memset(a,0,sizeof a);
    memset(dp,0,sizeof dp);
    cin>>n;
    for(int i=1;i<=n;++i) {
      cin>>a[i];
    }
    sort(a+1,a+n+1);
    dp[0]=1;
    for(int i=0;i<W;++i) {
      if(dp[i]>2) dp[i]=2;
      if(dp[i]==0) continue;
      for(int j=1;j<=n && i+a[j]<=W;++j) {
        dp[i+a[j]]+=dp[i];
      }
    }
    int m=0;
    for(int i=1;i<=n;++i) {
      if(dp[a[i]]==1) ++m;
    }
    cout<<m<<endl;
  }
  return 0;
}
