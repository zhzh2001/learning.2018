#include <bits/stdc++.h>
using namespace std;
int n,w;
int main(int argc, char **argv) {
  cin.sync_with_stdio(false);
  {
    FILE*RND=fopen("/dev/urandom","rb");
    int z=time(0);
    z^=fgetc(RND);
    z*=fgetc(RND);
    z+=fgetc(RND);
    srand(z);
  }
  cin.tie(0);
  n=atoi(argv[1]);
  w=atoi(argv[2]);
  cout<<n<<' '<<rand()%int(sqrt(n))+1<<endl;
  for(int i=2;i<=n;++i) {
    int fa=i/(2000/4);
    if(fa<1) fa=1;
    if(fa>=i) fa=i-1;
    cout<<i<<' '<<fa<<' '<<rand()%w+1<<'\n';
  }
  cout<<flush;
  return 0;
}
