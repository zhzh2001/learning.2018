#include<bits/stdc++.h>
using namespace std;
const int maxn = 50000/.95;
int n,m;
const int inf=0x3f3f3f3f;
struct graph {
  struct edge {
    int u,v,w;
    bool operator<(const edge &a) const {
      return u<a.u || (u==a.u && v<a.v);
    }
  };
  edge es[maxn*2];
  int beg[maxn], end[maxn];
  int e;
  void aa(int x,int y,int z) {
    es[++e]=(edge){x,y,z};
  }
  void ae(int x,int y,int z) {
    aa(x,y,z); aa(y,x,z);
  }
  void init() {
    sort(es+1,es+e+1);
    for(int i=1;i<=e;++i) {
      if(beg[es[i].u]==0) beg[es[i].u]=i;
      end[es[i].u]=i;
    }
  }
}g;
int fa[maxn];
void dfs1(int x) {
  for(int i=g.beg[x];i<=g.end[x];++i) {
    int y=g.es[i].v;
    if(y==fa[x]) continue;
    fa[y]=x;
    dfs1(y);
  }
}
int ans=0;
int sb[maxn];
int gz[maxn], gx;
int usd[maxn];
void Sort();
int fz[maxn];
const int mask = 2047;
int pa[maxn];
int bd;
void work(int x) {
  for(int i=g.beg[x];i<=g.end[x];++i) {
    int y=g.es[i].v;
    if(y==fa[x]) continue;
    work(y);
  }
  gx=0;
  for(int i=g.beg[x];i<=g.end[x];++i) {
    int y=g.es[i].v, w=g.es[i].w;
    if(y==fa[x]) continue;
    gz[++gx]=w+pa[y];
  }
  Sort();
  while(gz[gx]>=bd) --gx,++ans;
  memset(usd,0,sizeof(*usd)*(gx+3));
  // solve ans
  int lans=0;
  int MA=0;
  for(int i=1,j=gx; j>=i; ++i) {
    if(j!=i && gz[i]+gz[j]>=bd) {
      usd[j]=i; usd[i]=j;
      --j;
      ++lans;
    } else {
      if(gz[i]>gz[MA]) MA=i;
    }
  }
  ans+=lans;
  // solve pa
  int ma=gz[MA];
  for(int i=gx;i>0;--i) {
    if(gz[i]<ma) break;
    if(usd[i]) {
      // i: alter to un-used; usd[i]: alter to find a new peer
      // check if alter-able
      // usd[i] and MA form a new pair
      if(gz[usd[i]]+gz[MA]>=bd) {
        // succeed to alter
        ma=gz[i];
        usd[MA]=usd[i];
        usd[usd[i]]=MA;
        usd[i]=0;
        break;
      }
    }
  }
  pa[x]=ma;
}
int main() {
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  cin.sync_with_stdio(false); cin.tie(0);
  cin>>n>>m;
  int sw=0;
  for(int i=1;i<n;++i) {
    int u,v,w; cin>>u>>v>>w; g.ae(u,v,w);
    sw+=w;
  }
  g.init();
  int root=1;
  dfs1(root);
  int L=0,R=sw+1;
  while(R-L>1) {
    int dis=R-L, mid=L+dis/2;
    bd=mid;
    ans=0;
    work(root);
    if(ans>=m) L=mid;
    else R=mid;
  }
  cout<<L<<endl;
  return 0;
}

void Sort() {
  if(gx<mask/4) {
    sort(gz+1,gz+gx+1);
    return;
  }
  static int c[mask+1];
  memset(c,0,sizeof c);
  for(int i=1;i<=gx;++i) {
    c[gz[i]&mask]+=1;
  }
  for(int j=0;j<mask;++j) {
    c[j+1]+=c[j];
  }
  for(int i=gx;i>0;--i) {
    fz[c[gz[i]&mask]--]=gz[i];
  }
  memcpy(gz,fz, sizeof(*gz)*(gx+1));
  memset(c,0,sizeof c);
  for(int i=1;i<=gx;++i) {
    c[(gz[i]>>11)&mask]+=1;
  }
  for(int j=0;j<mask;++j) {
    c[j+1]+=c[j];
  }
  for(int i=gx;i>0;--i) {
    fz[c[(gz[i]>>11)&mask]--]=gz[i];
  }
  memcpy(gz,fz, sizeof(*gz)*(gx+1));
  memset(c,0,sizeof c);
  for(int i=1;i<=gx;++i) {
    c[(gz[i]>>22)&mask]+=1;
  }
  for(int j=0;j<mask;++j) {
    c[j+1]+=c[j];
  }
  for(int i=gx;i>0;--i) {
    fz[c[(gz[i]>>22)&mask]--]=gz[i];
  }
  memcpy(gz,fz, sizeof(*gz)*(gx+1));
}


