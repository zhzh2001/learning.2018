#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int maxn = 100000/.9;
int n;
int d[maxn];
/*
int L[maxn], R[maxn];
int fa[maxn];

int stk[maxn];
void build() {
  int top=0;
  for(int i=1;i<=n+1;++i) {
    while(top && d[stk[top]] >= d[i]) {
      R[stk[top]]=L[i];
      L[i]=stk[top];
      top--;
    }
    stk[++top]=i;
  }
}
*/

int main() {
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
  cin.sync_with_stdio(false); cin.tie(0);
  cin>>n;
  for(int i=1;i<=n;++i) {
    cin>>d[i];
  }
  d[n+1]=0;
  
  ll ans=0;
  for(int i=1;i<=n+1;++i) {
    ans+=abs(d[i]-d[i-1]);
  }
  cout<<ans/2<<endl;
  return 0;
}
