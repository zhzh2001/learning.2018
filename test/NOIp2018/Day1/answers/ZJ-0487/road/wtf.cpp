#include<bits/stdc++.h>
using namespace std;
const int maxn = 100000/.9;
typedef long long ll;
int n;
int d[maxn];
int main() {
  cin.sync_with_stdio(false);
  cin.tie(0);
  cin>>n;
  ll ans=0;
  for(int i=1;i<=n;++i) {
    cin>>d[i];
  }
  for(int i=1;i<=n+1;++i) {
    ans+=abs(d[i]-d[i-1]);
  }
  cout<<ans/2<<endl;
  return 0;
}
