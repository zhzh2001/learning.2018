#include<bits/stdc++.h>
using namespace std;
const int maxn = 100000/.9;
typedef long long ll;
int n;
int d[maxn];
ll work(int L,int R) {
  if(L>R) return 0;
  int *mp=min_element(d+L,d+R+1);
  int mx=mp-d;
  int mi=*mp;
  for(int i=L;i<=R;++i) {
    d[i]-=mi;
  }
  return mi + work(L,mx-1)+work(mx+1,R);
}
int main() {
  cin.sync_with_stdio(false);
  cin.tie(0);
  cin>>n;
  for(int i=1;i<=n;++i) {
    cin>>d[i];
  }
  cout<<work(1,n)<<endl;
  return 0;
}
