#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int maxn = 100000/.9;
int n;
int d[maxn];
int L[maxn], R[maxn];
int fa[maxn];

int stk[maxn];
void build() {
  int top=0;
  for(int i=1;i<=n+1;++i) {
    while(top && d[stk[top]] >= d[i]) {
      R[stk[top]]=L[i];
      L[i]=stk[top];
      top--;
    }
    stk[++top]=i;
  }
}

int main() {
  cin.sync_with_stdio(false); cin.tie(0);
  cin>>n;
  for(int i=1;i<=n;++i) {
    cin>>d[i];
  }
  d[n+1]=0;
  build();
  for(int i=1;i<=n+1;++i) {
    if(L[i]) fa[L[i]]=i;
    if(R[i]) fa[R[i]]=i;
  }
  ll ans=0;
  for(int i=1;i<=n;++i) {
    ans+=ll(d[i])-d[fa[i]];
  }
  cout<<ans<<endl;
  return 0;
}
