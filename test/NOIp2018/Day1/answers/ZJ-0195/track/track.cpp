#include<cstdio>
#include<algorithm>
using namespace std;

int dp[50010][2],rt,Head[50010],Next[100010],to[100010],val[100010],n,m,x,y,z,num,s,X[100010],st,Mdis,Min,de[50010];
bool f1,f2;

int read() {
	char ch=getchar(),l='+';int t=0;
	while (ch<'0'||ch>'9')	l=ch,ch=getchar();
	while (ch>='0'&&ch<='9')t=(t<<1)+(t<<3)+ch-48,ch=getchar();
	return l=='-'?-t:t;
}

void Add_Edge(int x,int y,int z) {
	Next[++num]=Head[x];
	Head[x]=num;
	to[num]=y;
	val[num]=z;
}

bool Check(int s) {
	int cnt=0,S=0;
	for (int i=1;i<n;i++)
	{
		for (;i<n&&S<s;i++)
			S+=X[i];
		if (S>=s)
			cnt++,S=X[i];
		if (i!=n&&S>=s)
			cnt++;
		if (cnt>=m)
			return 1;
	}
	return cnt>=m;
}

void dfs(int u) {
	for (int e=Head[u];e;e=Next[e])
		if (to[e]==u+1)
			X[u]=val[e],dfs(to[e]);
}

void dfs1(int u,int pre,int dis) {
	if (dis>Mdis)
		Mdis=dis,st=u;
	for (int e=Head[u];e;e=Next[e]) 
	{
		if (to[e]==pre)	continue;
		dfs1(to[e],u,dis+val[e]);
	}
	return;
}

void dfs2(int u,int pre) {
	for (int e=Head[u];e;e=Next[e]) 
	{
		if (to[e]==pre)
			continue;
		Min=min(Min,val[e]);
		dfs2(to[e],u);
	}
	return;
}

void dfs3(int u,int pre,int Mid) {
	int l=0,r=0,lval=0,rval=0;
	for (int e=Head[u];e;e=Next[e])
	{
		if (to[e]==pre)	continue;
		dfs3(to[e],u,Mid);
		l?(r=to[e],rval=val[e]):(l=to[e],lval=val[e]);
	}
	if (dp[l][0]+dp[r][0]+lval+rval>=Mid)
	{
		dp[u][0]=0,dp[u][1]=dp[l][1]+dp[r][1]+1;
	}else
	{
		dp[u][0]=max(dp[l][0]+lval,dp[r][0]+rval);
		dp[u][1]=dp[l][1]+dp[r][1];
	}
	if (dp[u][0]>=Mid)
	{
		dp[u][0]=0;
		dp[u][1]++;
	}
	return;
}

int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	f1=f2=1;
	for (int i=1;i<n;i++)
	{
		x=read(),y=read(),z=read();
		de[x]++,de[y]++;
		Add_Edge(y,x,z);
		Add_Edge(x,y,z);
		if (x+1!=y)	f1=0;
		if (x!=1)	f2=0;
		s+=z;
	}
	if (f1) 
	{
		dfs(1);
		int l=1,r=s/m,mid,ans=l;
		while (l<=r)
		{
			mid=l+((r-l)>>1);
			if (Check(mid))
				l=mid+1,ans=mid;else
				r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	if (f2) 
	{
		for (int e=Head[1];e;e=Next[e])
			X[++X[0]]=val[e];
		sort(X+1,X+n);
		int l=1,r=X[n-1]+X[n-2],mid,ans=1;
		while (l<=r)
		{
			mid=l+((r-l)>>1);
			int L=1,R=n-1,cnt=0;
			for (int i=1;i<=m&&L<R;i++)
			{
				while (L<R&&X[L]+X[R]<mid)
					L++;
				if (L<R&&X[L]+X[R]>=mid)	
					cnt++;
				R--,L++;
				if (cnt>=m)
					break;
			}
			if (cnt>=m)
				ans=mid,l=mid+1;else
				r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	if (m==1)
	{
		dfs1(1,0,0);
		dfs1(st,0,0);
		printf("%d\n",Mdis);
		return 0;
	}
	if (m==n-1)
	{
		dfs2(1,0);
		printf("%d\n",Min);
		return 0;
	}
	for (int i=1;i<=n;i++)
	{
		if (de[i]<3)
		{
			rt=i;
			break;
		}
	}
	int l=1,r=s,MID,ans=l;
	while (l<=r)
	{
		MID=l+((r-l)>>1);
		for (int i=0;i<=n;i++)
			dp[i][0]=dp[i][1]=0;
		dfs3(rt,0,MID);
		if (dp[rt][1]>=m)
			ans=MID,l=MID+1;else
			r=MID-1;
	}
	printf("%d\n",ans);
	return 0;
}
