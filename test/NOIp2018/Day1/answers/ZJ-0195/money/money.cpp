#include<cstdio>
#include<algorithm>
using namespace std;

int n,Cnt,a[110],T;
bool f[25010];

int read() {
	char ch=getchar(),l='+';int t=0;
	while (ch<'0'||ch>'9')	l=ch,ch=getchar();
	while (ch>='0'&&ch<='9')t=(t<<1)+(t<<3)+ch-48,ch=getchar();
	return l=='-'?-t:t;
}

int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	f[0]=1;
	scanf("%d",&T);
	for (;T;T--)
	{
		scanf("%d",&n);
		Cnt=0;
		for (int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		for (int i=1;i<=a[n];i++)
			f[i]=0;
		for (int i=1;i<=n;i++)
		{
			if (f[a[i]])	continue;
			Cnt++;
			for (int j=a[i];j<=a[n];j++)
				if (f[j-a[i]])
					f[j]=1;
		}
		printf("%d\n",Cnt);
	}
	return 0;
}
