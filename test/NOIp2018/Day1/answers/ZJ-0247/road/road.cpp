#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
#define N 100005
#define ll long long
using namespace std;
int n, a[N], minn = 1000000, t[N];
ll ans = 0;
#define gc ch = getchar()
inline int re(){
	char ch = getchar(); int x = 0, f = 1;
	for (; !isdigit(ch); gc) if (ch == '-') f = -1;
	for (; isdigit(ch); gc) x = x * 10 + ch - '0';
	return x * f;
}
#undef gc
void search(int l, int r){
	if (l == r){ans += a[l];return;}
	int l2, r2;
	for (int i = l; i <= r;){
		l2 = i; bool fl = 0;
		while (a[i] > 0 && i <= n) --a[i++], fl = 1;
		r2 = i - 1;  if (fl) ++ans;
		search(l2, r2);
		while (!a[i] && i <= r) ++i;
	}
}
int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	n = re();
	for (int i = 1; i <= n; i++) a[i] = re(), minn = min(minn, a[i]);
	ans += (minn - 1);
	for (int i = 1; i <= n; i++) a[i] -= (minn - 1);
	search(1, n);
	printf("%lld\n", ans);
	return 0;
}
