#include <cstdio>
#include <cstring>
#include <cctype>
#include <algorithm>
#define N 105
#define M 25005
using namespace std;
int t, n, a[N], cnt, a2[N];
bool bo[M];
#define gc ch = getchar()
inline int re(){
	char ch = getchar(); int x = 0, f = 1;
	for (; !isdigit(ch); gc) if (ch == '-') f = -1;
	for (; isdigit(ch); gc) x = x * 10 + ch - '0';
	return x * f;
}
#undef gc
bool dfs(int x){
	if (!x) return 1;
	if (bo[x]) return 1;
	for (int i = 2; i <= x; i++){
		bool fl = 0;
		if (bo[i]) fl = dfs(x - i);
		if (fl == 1) return fl;
	}
	return 0;
}
int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	t = re();
	while (t--){
		n = re();
		for (int i = 1; i <= n; i++) a[i] = re();
		sort(a + 1, a + 1 + n);
		if (a[1] == 1){puts("1"); continue;}
		cnt = 0;
		memset(bo, 0, sizeof bo);
		for (int i = 1; i <= n; i++){
			if (cnt) if (dfs(a[i])) continue;
			a2[++cnt] = a[i];
			bo[a[i]] = 1;
		}
		printf("%d\n", cnt);
	}
	return 0;
}
