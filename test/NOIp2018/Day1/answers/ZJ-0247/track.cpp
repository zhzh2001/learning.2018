#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
#define N 50005
#define ll long long
using namespace std;
struct node{
	ll nxt, to, v;
}e[N << 1];
ll n, m, w[N], cnt = 0, head[N], len[N], maxx = 0, sum = 0;
bool fl = 1, f2 = 1;
#define gc ch = getchar()
inline ll re(){
	char ch = getchar(); ll x = 0, f = 1;
	for (; !isdigit(ch); gc) if (ch == '-') f = -1;
	for (; isdigit(ch); gc) x = x * 10 + ch - '0';
	return x * f;
}
#undef gc
inline void add(int x, int y, int z){
	e[++cnt].to = y;
	e[cnt].nxt = head[x];
	e[cnt].v = z;
	head[x] = cnt;
}
void dfs(ll now, ll val, ll fa){	
	for (ll i = head[now]; i; i = e[i].nxt){
		ll v = e[i].to, w = e[i].v;
		if (v == fa) continue;
		dfs(v, val + w, now);
	}
	maxx = max(maxx, val);
}
void work(){
	ll ans = len[1];
	for (ll i = 2; i <= m; i++) ans = min(ans, len[i]);
	maxx = max(ans, maxx);
}
void solve(ll now, ll x, ll fa){
	if (x > m){work(); return;}
	for (ll i = head[now]; i; i = e[i].nxt){
		ll v = e[i].to;
		if (v == fa) continue;
		len[x] += e[i].v; 
		solve(v, x, now);
		len[x] -= e[i].v;
		len[x + 1] += e[i].v; 
		solve(v, x + 1, now);
		len[x + 1] -= e[i].v; 
	}
}
bool cmp(int x, int y){return x > y;}
int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	n = re(); m = re();
	for (ll i = 1; i < n; i++){
		ll u, v;
		u = re(); v = re(); w[i] = re();
		add(u, v, w[i]); add(v, u, w[i]); maxx = max(maxx, w[i]);
		if (v != u + 1) fl = 0; sum += w[i];
		if (u != 1) f2 = 0;
	}
	sort(w + 1, w + 1 + n, cmp);
	if (m == n - 1){printf("%lld\n", maxx); return 0;}
	else if (m == 1){
		maxx = 0;
		for (ll i = 1; i <= n; i++) dfs(i, 0, 0);
		printf("%lld\n", maxx);
	}
	else if (!fl){
		if (f2){printf("%lld\n", w[1] + w[2]); return 0;}
		maxx = 0; 
		solve(1, 1, 0);
		printf("%lld\n", maxx);
	}
	else{printf("%lld\n", sum / m);}
	return 0;
}
