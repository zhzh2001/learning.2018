#include<bits/stdc++.h>
#define N 1010
using namespace std;
int n,m,x,y,z,i,f[N*N],e[N],p[N];
int a[N],b[N],c[N],d[N],cnt,k;
int read()
{int f=1,x=0;
char c=getchar();
while (c<'0'||c>'9')
  {
   if (c=='-') f=-1;
   c=getchar();
   }
while (c>='0'&&c<='9')
  {
  x=x*10+(int)c-48;
  c=getchar();
  }
return x*f;
}
int cmp(int x,int y)
{
	return x>y;
}
void add(int x,int y,int z)
{
	cnt++;b[cnt]=a[x];a[x]=cnt;c[cnt]=y;d[cnt]=z;
}
void spfa(int x)
{ int t,w;
	t=1;w=1;e[1]=x;
	for (int i=1;i<=n;i++)
	  f[i]=INT_MAX/2;f[x]=0;
	while (t<=w)
	  {
	  	for (int i=a[e[t]];i!=0;i=b[i])
	  	  if (f[c[i]]>f[e[t]]+d[i])
	  	    {
	  	    	f[c[i]]=f[e[t]]+d[i];
	  	    	w++;e[w]=c[i];
			  }
	  	t++;
	  }
	for (int i=1;i<=n;i++)
	  if (i!=x&&i>x)
	    {
	    	k++;p[k]=f[i];
		}
}
int main()
{
freopen("track.in","r",stdin);
freopen("track.out","w",stdout);
n=read();m=read();
for (int i=1;i<=n-1;i++)
  {
  	x=read();y=read();z=read();
  	add(x,y,z);add(y,x,z);
  }
k=0;
for (int i=1;i<=n;i++)
  spfa(i);
sort(p+1,p+k+1,cmp);
printf("%d\n",p[m]);
return 0;
}
