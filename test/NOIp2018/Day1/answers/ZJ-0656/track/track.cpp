#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
int pbg[50008],ppre[50008],ndmx[50008][2],li[50008],lu[50008],fz[50008];
int n,m,ltot,rott,lup,fo4,fa4;
bool bs2,bs3,bs4;
struct EDGE
{
	int st,ed,next,le;
}
e[100008];

void edgein()
{
	bs2=true; bs3=true; bs4=true;
	for (int i=1;i<=n-1;i++)
	{
		int a,b,l;
		scanf("%d%d%d",&a,&b,&l);
		fz[a]++; fz[b]++;
		lup++; lu[lup]=l;
		if (b!=a+1) bs2=false;
		if (a!=1) bs3=false;
		li[a]=l; ltot+=l;
		if (pbg[a]==0) pbg[a]=2*i-1;
		else e[ppre[a]].next=2*i-1;
		e[2*i-1].st=a; e[2*i-1].ed=b; e[2*i-1].le=l; ppre[a]=2*i-1; 
		if (pbg[b]==0) pbg[b]=2*i;
		else e[ppre[b]].next=2*i;
		e[2*i].st=b; e[2*i].ed=a; e[2*i].le=l; ppre[b]=2*i; 
	}
}

int treedom(int nd,int fa)
{
	int ee=pbg[nd],fst=0,scd=0,fad=0;
	while (ee!=0)
	{
		if (e[ee].ed!=fa) 
		{
			int tmp1=treedom(e[ee].ed,nd);
			if (tmp1>fst){scd=fst; fst=tmp1;}
			else if (tmp1>scd) scd=tmp1;
		}
		else fad=e[ee].le;
		ee=e[ee].next;
	}
	ndmx[nd][0]=fst; ndmx[nd][1]=scd;
	return fst+fad;
}

void sol1()    //complete!! +20
{
	int mx=0;
	for (int i=1;i<=n;i++)
	{
		if (mx<ndmx[i][0]+ndmx[i][1]) mx=ndmx[i][0]+ndmx[i][1];
	}
	printf("%d\n",mx);
}

bool judge2(int wtf)
{
	int lele=0,ansis=0;
	for (int i=1;i<=n-1;i++)
	{
		lele+=li[i];
		if (lele>=wtf) {ansis++; lele=0;}
	}
	if (ansis>=m) return true;
	else return false;
}

void sol2()    //complete!! +15
{
	int maxim=ltot/m,minim=0,mayans;
	while (maxim>minim)
	{
		mayans=(maxim+minim)/2+1;
		if (judge2(mayans)) minim=mayans;
		else maxim=mayans-1;
	}
	printf("%d\n",minim);
}

void sol3()    //complete!! +20
{
	int ansix=11464989;
	sort(lu+1,lu+n);
	for (int i=1;i<=m;i++)
	{
		if (ansix>lu[n-i]+lu[n-2*m+i-1]) ansix=lu[n-i]+lu[n-2*m+i-1];
	}
	printf("%d\n",ansix);
}

int treedom2(int nd,int fa)
{
	int ee=pbg[nd],tmp2=0;
	while (ee!=0)
	{
		if (e[ee].ed!=fa) 
		{
			tmp2+=treedom2(e[ee].ed,nd);
		}
		ee=e[ee].next;
	}
	if (tmp2+ndmx[nd][0]+ndmx[nd][1]>=fa4) 
	{
		fo4++;
		tmp2-=ndmx[nd][0];
	}
	return tmp2;
}

bool judge4()
{
	fo4=0;
	treedom2(rott,0);
	if (fo4>=m) return true;
	else return false;
}

void sol4()
{
	int maxim=ltot/m,minim=0,mayans;
	while (maxim>minim)
	{
		mayans=(maxim+minim)/2+1;
		fa4=mayans;
		if (judge4()) minim=mayans;
		else maxim=mayans-1;
	}
	printf("%d\n",minim);
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	edgein();
	for (int i=1;i<=n;i++) 
	{
		if (fz[i]<=2&&rott==0) rott=i;
		if (fz[i]>3) bs4=false;
	}
	treedom(rott,0);
	if (m==1) sol1();
	else if (bs2) sol2();
	else if (bs3) sol3();
	else sol4();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
