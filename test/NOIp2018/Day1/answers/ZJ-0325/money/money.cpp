#include<bits/stdc++.h>
using namespace std;
int NN;
bool tg,f[5000000];
int n,a[10000],xb,numb[100000];
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	NN=30000;
	int T;
	scanf("%d",&T);
	while(T--){
		tg=false;
		xb=0;
		scanf("%d",&n);
		for (int i=1; i<=n; i++) {
			scanf("%d",&a[i]);
			if (a[i]==1) tg=true;
		}
		sort(a+1,a+n+1);
		numb[++xb]=a[1];
		if (tg) {
			printf("1\n");
			continue;
		}
		for (int i1=2; i1<=n; i1++) {
			int xx=a[i1];
			for (int i=1; i<=xx; i++) f[i]=false;
			f[0]=true;
			for (int i=1; i<=xb; i++)
				for (int j=numb[i]; j<=xx; j++)
					f[j]|=f[j-numb[i]];
			if (!f[xx]) numb[++xb]=a[i1];
		}
		printf("%d\n",xb);
	}
	return 0;
}
