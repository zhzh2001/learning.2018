#include<bits/stdc++.h>
#define N 555555
using namespace std;
int dp[N],nxt[N*2],vet[N*2],head[N*2],val[N*2],num,ans1,ans2,anss;
int n,m;
bool tg,tg2;
int px;
struct node {
	int x,y,z;
} a[N];
bool cmp(node x1,node x2) {
	return x1.z>x2.z;
}
bool cmp2(node x1,node x2) {
	return x1.x<x2.x;
}
void add_edge(int u,int v,int t) {
	nxt[++num]=head[u];
	head[u]=num;
	vet[num]=v;
	val[num]=t;
}
void dfs1(int u,int f) {
	for (int e=head[u]; e; e=nxt[e]) {
		int v=vet[e],w=val[e];
		if (v==f) continue;
		dfs1(v,u);
		dp[u]=max(dp[u],dp[v]+w);
		if (u==1) {
			if (dp[v]+w>ans1) {
				ans2=ans1;
				ans1=dp[v]+w;
			} else if (dp[v]+w==ans1) {
				ans2=max(ans2,dp[v]+w);
			} else if (dp[v]+w<ans1) {
				ans2=max(ans2,dp[v]+w);
			}
		}
	}
}
void solve1() {
	dfs1(1,1);
	printf("%d\n",ans1+ans2);
}
bool check(int x) {
	int h=1,w=n;
	for (int i=n; i>=1; i--) if (a[1].z+a[i].z>=x) {
			w=i;
			break;
		}
	for (int i=1; i<=m; i++) {
		if (h>=w) return false;
		int p1=a[h].z;
		if (p1>=x) {
			h++;
			continue;
		}
		if (p1+a[w].z>=x) {
			h++;
			w--;
			continue;
		}
		while((p1+a[w].z<x)&&(h<w)) {
			w--;
		}
		if (h>=w) return false;
		h++;
		w--;
	}
	return true;
}
void solve2() {
	sort(a+1,a+n,cmp);
	int l=0,r=px;
	while(l<r) {
		int mid=(l+r+1)>>1;
		if (check(mid)) l=mid;
		else
			r=mid-1;
	}
	printf("%d\n",l);
}
bool check2(int x) {
	int ss=0,numb=0;
	for (int i=1; i<n; i++) {
		ss+=a[i].z;
		if (ss>=x) {
			ss=0;
			numb++;
			continue;
		}
	}
	if (numb>=m) return true;
	else return false;
}
void solve3() {
	sort(a+1,a+n,cmp2);
	int l=0,r=px;
	while(l<r) {
		int mid=(l+r+1)>>1;
		if (check2(mid)) l=mid;
		else
			r=mid-1;
	}
	printf("%d\n",l);
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	anss=1000000000;
	for (int i=1; i<n; i++) {
		scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
		px+=a[i].z;
		add_edge(a[i].x,a[i].y,a[i].z);
		add_edge(a[i].y,a[i].x,a[i].z);
		if (a[i].x!=1)tg=true;
		if (a[i].y!=(a[i].x+1)) tg2=true;
		if (m==n-1) anss=min(anss,a[i].z);
	}
	if (m==n-1) {printf("%d\n",anss);return 0;}
	if ((m==1)&&(n<=1000)) {
		solve1();    
		return 0;
	}
	if (!tg) {
		solve2();    
		return 0;
	}
	if (!tg2) {
		solve3();    
		return 0;
	}
	return 0;
}
