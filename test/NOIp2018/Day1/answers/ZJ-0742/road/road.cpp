#include <cstdio>
#include <iostream>
#define maxx(a,b) a>b?a:b
using namespace std;
const int MAXN = 1e5 + 10;
const int MAXD = 1e4 + 10;
long long n, den[MAXN], ans;
int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%lld", &n);
	for(int i = 1; i <= n; i++)
		scanf("%lld", &den[i]), ans += maxx(den[i] - den[i-1], 0);
	printf("%lld\n", ans);
	return 0;
}
