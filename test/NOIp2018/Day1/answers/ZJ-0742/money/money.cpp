#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#define readit(x) scanf("%d", &x)
#define maxx(a,b) a>b?a:b
using namespace std;
const int MAXN = 110;
const int MAXA = 5e4;
int t, n, a[MAXN], maxa, val, stackk[MAXA], top;
bool used[MAXA];
int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	readit(t);
	while(t--){
		memset(used, 0, sizeof(used));
		top = 0;
		val = 0;
		readit(n);
		for(int i = 1; i <= n; i++)
			readit(a[i]);
		sort(a+1, a+1+n);
		maxa = a[n];
		for(int i = 1; i <= n; i++){
			if(used[a[i]]) continue;
			val ++;
			stackk[++top] = a[i];
			used[a[i]] = true;
			for(int j = 1; j <= top; j++)
				if(stackk[j] + a[i] <= maxa && !used[stackk[j] + a[i]])
					stackk[++top] = a[i] + stackk[j], used[a[i] + stackk[j]] = true;
		}
		printf("%d\n", val);
	}
	return 0;
}
