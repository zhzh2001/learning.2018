#include <cstdio>
#include <iostream>
#include <cstring>
#define min(a,b) a<b?a:b
#define max(a,b) a>b?a:b
using namespace std;
const int maxn = 3e3;
int n, m, dis[maxn][maxn], nume, head[maxn], ans, visited[maxn];
struct Edge{
	int nxt, to, w;
}edge[2*maxn];
void dfs(int x){
	if(visited[x]) return;
	visited[x] = true;
	for(int p = head[x]; p; p = edge[p].nxt){
		if(visited[edge[p].to]) continue;
		for(int i = 1; i <= n; i++){
			if(visited[i]){
				dis[edge[p].to][i] = dis[i][edge[p].to] = min(dis[i][x] + edge[p].w, dis[i][edge[p].to]);
			//	cerr << x << "---" << i << ":" << dis[x][i] <<endl;
				ans = max(dis[edge[p].to][i], ans);
			}
		}
		dfs(edge[p].to);
	}
}
int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	memset(dis, 0x3f, sizeof(dis));
	for(int i = 1; i <= n; i++)
		dis[i][i] = 0;
	for(int i = 1; i <= n - 1; i++){
		int u, v;
		scanf("%d%d%d", &u, &v, &edge[++nume].w);
		edge[nume].nxt = head[u];
		edge[nume].to = v; 
		head[u] = nume;
		edge[++nume].to = u;
		edge[nume].nxt = head[v];
		edge[nume].w = edge[nume-1].w;
		head[v] = nume;
	}
	dfs(1);
//	for(int i = 1; i <= n; i++){
//		for(int j = 1; j <= n; j++)
//			cerr<<dis[i][j]<<" ";
//		cerr<<endl;
//	}
	printf("%d\n", ans);
	return 0;
}
