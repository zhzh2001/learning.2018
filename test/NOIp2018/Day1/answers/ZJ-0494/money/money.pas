var
  n,i,j,k,t,x,ans:longint;
  a,b:array[0..100] of longint;
  f:array[0..25000] of boolean;
  procedure qsort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           qsort(l,j);
         if i<r then
           qsort(i,r);
      end;
function dfs(x,y:longint):boolean;
  var i,j:longint;
  begin
    if f[x]
      then exit(true);
    if (y=1)and(x mod b[1]=0)
      then
        begin
          f[x]:=true;
          exit(true);
        end
      else
        if y=1
          then exit(false);
    for i:=x div b[y] downto 0 do
      if dfs(x-i*b[y],y-1)
        then
          begin
            f[x]:=true;
            exit(true);
          end;
    exit(false);
  end;
begin
  assign(input,'money.in');
  reset(input);
  assign(output,'money.out');
  rewrite(output);
  readln(t);
  for x:=1 to t do
    begin
      readln(n);
      for i:=1 to 25000 do
        f[i]:=false;
      for i:=1 to n do
        read(a[i]);
      readln;
      qsort(1,n);
      ans:=1;
      b[1]:=a[1];
      for i:=2 to n do
        if not dfs(a[i],ans)
          then
            begin
              ans:=ans+1;
              b[ans]:=a[i];
            end;
      writeln(ans);
    end;
  close(input);
  close(output);
end.