var
  n,m,i,j,k,t,q,x,y,z,ans,l,r,mid:longint;
  p:array[1..50000] of longint;
  first,next,last,a,b,ip,vis,dis,f:array[0..100000] of longint;
function max(x,y:longint):longint;
  begin
    if x>y
      then exit(x)
      else exit(y);
  end;
function min(x,y:longint):longint;
  begin
    if x>y
      then exit(y)
      else exit(x);
  end;
function dfs(x:longint):longint;
  var i,j,q,k:longint;
  begin
    dfs:=0;
    vis[x]:=1;
    k:=first[x];
    while k>0 do
      begin
        q:=a[k];
        if vis[q]=0
          then dfs:=max(dfs,dfs(q)+b[k]);
        k:=next[k];
      end;
  end;
   procedure qsort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=p[(l+r) div 2];
         repeat
           while p[i]<x do
            inc(i);
           while x<p[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=p[i];
                p[i]:=p[j];
                p[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           qsort(l,j);
         if i<r then
           qsort(i,r);
      end;
function check(x:longint):boolean;
  var num,k,i:longint;
  begin
    k:=0;
    num:=0;
    i:=0;
    while k<n-1 do
      begin
        while (num<x)and(k<n-1) do
          begin
            inc(k);
            inc(num,p[k]);
          end;
        if num<x
          then dec(i);
        num:=0;
        inc(i);
        if i>=m
          then exit(true);
      end;
    exit(false);
  end;
begin
  assign(input,'track.in');
  reset(input);
  assign(output,'track.out');
  rewrite(output);
  readln(n,m);
  if m=1
    then
      begin
        for i:=1 to n-1 do
          begin
            readln(x,y,z);
            inc(ip[x]);
            inc(ip[y]);
            inc(k);
            a[k]:=y;
            b[k]:=z;
            if first[x]=0
              then first[x]:=k
              else next[last[x]]:=k;
            last[x]:=k;
            inc(k);
            a[k]:=x;
            b[k]:=z;
            if first[y]=0
              then first[y]:=k
              else next[last[y]]:=k;
            last[y]:=k;
          end;
        ans:=0;
        for i:=1 to n do
          if ip[i]=1
            then
              begin
                for j:=1 to n do
                  vis[j]:=0;
                ans:=max(ans,dfs(i));
              end;
        writeln(ans);
      end
    else
      begin
        for i:=1 to n-1 do
          begin
            readln(x,y,p[i]);
            if x>1
              then t:=2
              else t:=1;
          end;
        if t=1
          then
            begin
              qsort(1,n-1);
              ans:=maxlongint;
              if n-m*2>=1
                then
                  for i:=n-m*2 to n-m-1 do
                    ans:=min(ans,p[i]+p[n-i+n-m*2-1])
                else
                  begin
                    for i:=2*m-n+1 to n-1 do
                      ans:=min(ans,p[i]);
                    for i:=1 to (2*m-n) div 2 do
                      ans:=min(ans,p[i]+p[2*m-n+1-i]);
                  end;
              writeln(ans);
            end
          else
            begin
              l:=1;
              for i:=1 to n do
                r:=r+p[i];
              while l<r do
                begin
                  mid:=(l+r+1) div 2;
                  if check(mid)
                    then l:=mid
                    else r:=mid-1;
                end;
              writeln(l);
            end;
      end;
  close(input);
  close(output);
end.

