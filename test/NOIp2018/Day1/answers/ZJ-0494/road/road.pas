var
  n,i,j,k,l,r,x:longint;
  ans:int64;
  f,num:array[0..100000] of longint;
begin
  assign(input,'road.in');
  reset(input);
  assign(output,'road.out');
  rewrite(output);
  readln(n);
  l:=1;
  r:=0;
  for i:=1 to n do
    begin
      read(x);
      while (l<=r)and(f[r]>=x) do
        begin
          if (r-1=0)or(f[r-1]<x)
            then ans:=ans+(f[r]-x)
            else ans:=ans+(f[r]-f[r-1]);
          dec(r);
        end;
      inc(r);
      f[r]:=x;
    end;
  for i:=r downto 1 do
    ans:=ans+f[i]-f[i-1];
  writeln(ans);
  close(input);
  close(output);
end.
