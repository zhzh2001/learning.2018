#include<bits/stdc++.h>
using namespace std;
void fre()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
}
inline int read()
{
	int num = 0, flag = 1;char c = getchar();
	for(;c<'0'||c>'9';c = getchar())
	  if (c == '-') flag = -1;
	for(;c<='9'&&c>='0';c = getchar())
	  num = (num<<1)+(num<<3)+c-48;
	return num * flag;
}
bool flag = 0;
int n, m, k, ans = 0, tote = 0, maxx, q;
struct edge
{
	int y, next, v;
}e[100010];
int linke[50010] = {}, tmp[50010] = {}, dis[50010] = {};
void insert(int x,int y,int v)
{
	e[++tote] = (edge){y,linke[x],v};linke[x] = tote;
}
void dfs(int x,int fa)
{
	int cnt = 0;
	for(int i = linke[x];i;i = e[i].next) 
	  {
	  	int y = e[i].y;
	  	if (y == fa) continue;
	  	dfs(y,x);
	    if (flag) return ;
	  }
	for(int i = linke[x];i;i = e[i].next)
	  {
	  	int y = e[i].y;
	  	if (y == fa) continue;
	  	tmp[++cnt] = dis[y]+e[i].v;
	  }
	if (ans >= m) flag = 1;
	if (!cnt) return ;
	sort(tmp+1,tmp+cnt+1);
	while (tmp[cnt] >= k) ++ans, --cnt;
	int L, R, res = 0;
	for(L = 1, R = cnt;L < R;)
	  if (tmp[L]+tmp[R] >= k) ++ans, ++L, --R;
	  else res = max(res,tmp[L]), ++L;
	if (L == R) res = max(res,tmp[L]);
	dis[x] = res;
	if (ans >= m) flag = 1;
}
bool check()
{
	for(int i = 1;i <= n;++i) dis[i] = 0;
	ans = 0;
	flag = 0;
	dfs(1,0);
	return flag;
}
void dfs1(int x,int fa)
{
	if (dis[x]>maxx) maxx = dis[x], q = x;
	for(int i = linke[x];i;i = e[i].next)
	  {
	  	int y = e[i].y;
	  	if (y == fa) continue;
	  	dis[y] = dis[x]+e[i].v;
		dfs1(y,x);
	  }
}
void work()
{
	maxx = 0;
	dis[1] = 0;
	dfs1(1,0);
	maxx = 0;
	dis[q] = 0;
	dfs1(q,0);
	printf("%d\n",maxx); 
}
int main()
{
	fre();
	n = read(), m = read();
	int l = 1e9, r = 0;
	for(int i = 1;i <= n-1;++i)
	  {
	  	int x = read(), y = read(), v = read();
	  	insert(x,y,v);insert(y,x,v);
	  	l = min(l,v);r += v;
	  }
	if (m == 1) {work();return 0;}
	while (l < r)
	  {
	  	k = (l+r+1)>>1;
	  	if (check()) l = k;
	  	else r = k-1;
	  }
	printf("%d",l);
	return 0;
}
