#include<bits/stdc++.h>
using namespace std;
void fre()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}
inline int read()
{
	int num = 0, flag = 1;char c = getchar();
	for(;c<'0'||c>'9';c = getchar())
	  if (c == '-') flag = -1;
	for(;c<='9'&&c>='0';c = getchar())
	  num = (num<<1)+(num<<3)+c-48;
	return num * flag;
}
int n;
int a[110] = {};
bool f[25010] = {};
int main()
{
	fre();
	int T = read();
	while (T--)
	  {
	  	n = read();
	  	for(int i = 1;i <= n;++i)
	  	  a[i] = read();
	  	sort(a+1,a+n+1);
	  	for(int i = 1;i <= a[n];++i)
	  	  f[i] = 0;
	  	f[0] = 1;
	  	int ans = 0;
	  	for(int i = 1;i <= n;++i)
	  	  {
	  	  	if (!f[a[i]]) 
	  	  	  {
	  	  	    for(int j = a[i];j <= a[n];++j)
	  	  	      f[j] |= f[j-a[i]];
	  	  	    ++ans;
			  }
		  }
		printf("%d\n",ans);
	  }
	return 0;
}
