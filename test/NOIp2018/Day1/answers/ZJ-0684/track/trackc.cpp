#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
#define ll long long
const ll inf = 600000000;
vector <ll> lt[100005];
vector <ll> lw[100005];

ll n,m;
ll dem;
ll ans;
ll ansc(ll ct , ll bn , ll co[]){
	int qa = 0;
	if (bn == 0){
		ll j = 1 , k = ct;
		while (j<k){
			while ((j < k-1) && (co[j]+co[k] < dem)) j = j + 1;
			if ((j>=k)||(co[j]+co[k] < dem)) break;
			qa = qa + 1;
			k = k-1;
			j = j+1;
		}
		return qa;
	}
	else{
		ll j = 1 , k = ct;
		while (j<k){
			while ((j < k-1) && (co[j]+co[k] < dem)) j = j + 1;
			if (j == bn) j++;
			else if (k == bn) k++;
			if ((j>=k)||(co[j]+co[k] < dem)) break;
			qa = qa + 1;
			k = k-1;
			j = j+1;
		}
		return qa;
	}
}
ll dfs(ll o , ll ft){
	vector <int> co;
	co.push_back(0);
	ll cnt = 0;
	for (ll i = 0; i < lt[o].size(); i++)
	if (lt[o][i] != ft){
		ll v = lt[o][i];
		ll w = lw[o][i];
		ll dd = dfs(v,o);
		if (dd+w > 0){
			cnt++;
			co.push_back(dd+w);
		}
	}
	if (cnt == 0) return 0;
	sort(co+1,co+cnt+1);
	while (co[cnt] >= dem){
		cnt--;
		ans++;
	}
	if (cnt == 0) return 0;
	int qans = ansc(cnt,0,co);
	if ((cnt%2 == 0)&&(qans*2 == cnt)){
		ans += qans;
		return 0;
	}
	ll lf = 1 , ri = cnt;
	while (lf <= ri){
		ll mi = (lf+ri)/2;
		if (ansc(cnt,mi,co) < qans) ri = mi-1;
		else lf = mi+1;
	}
//	cout<<ri<<" "<<co[ri]<<endl;
	ans += qans;
	return co[ri];
}
bool jdge(ll o){
	dem = o;
	ans = 0;
	ll p = dfs(1,0);
	if (ans >= m) return true;
	return false;
}
int main(){
	freopen("track.in","r",stdin);
//	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	ll lf = 1 , ri = 0;
	ll mxx = 0;
	for (ll i = 1; i < n; i++){
		ll a,b,l;
		scanf("%lld%lld%lld",&a,&b,&l);
		ri = ri + l;
		lt[a].push_back(b);
		lw[a].push_back(l);
		lt[b].push_back(a);
		lw[b].push_back(l);
		mxx = max(mxx,(ll)lw[a].size());
		mxx = max(mxx,(ll)lw[b].size());
	}
	while (lf <= ri){
		ll mi = (lf+ri)/2;
		if (jdge(mi) == 0) ri = mi-1;
		else lf = mi+1;
	}
	printf("%lld\n",ri);
//	ll rr = 1;
//	while (jdge(rr)) rr = rr+1;
//	rr = rr-1;
//	printf("%lld\n",rr);
}

