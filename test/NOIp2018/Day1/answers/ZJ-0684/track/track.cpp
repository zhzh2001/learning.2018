#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
#define ll long long
//const ll inf = 600000000;
vector <ll> lt[100005];
vector <ll> lw[100005];
ll n,m;
ll k;
ll ans;
ll dfs(ll o , ll ft){
	ll m1=0,m2=0;
	for (ll i = 0; i < lt[o].size(); i++)
	if (lt[o][i] != ft){
		ll v = lt[o][i];
		ll w = lw[o][i];
		ll dd = dfs(v,o);
		if (dd+w > m1){
			m2 = m1;
			m1 = dd+w;
		}
		else if (dd+w > m2){
			m2 = dd+w;
		}
	}
	if (m1+m2 >= k){
		ans = ans + 1;
		return 0;
	}
	else return (m1);
}
bool jdge(ll o){
	k = o;
	ans = 0;
	ll p = dfs(1,0);
	if (ans >= m) return true;
	return false;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	ll lf = 1 , ri = 0;
	for (ll i = 1; i < n; i++){
		ll a,b,l;
		scanf("%lld%lld%lld",&a,&b,&l);
		ri = ri + l;
		lt[a].push_back(b);
		lw[a].push_back(l);
		lt[b].push_back(a);
		lw[b].push_back(l);
	}
	while (lf <= ri){
		ll mi = (lf+ri)/2;
		if (jdge(mi) == 0) ri = mi-1;
		else lf = mi+1;
	}
	printf("%lld\n",ri);
//	ll rr = 1;
//	while (jdge(rr)) rr = rr+1;
//	rr = rr-1;
//	printf("%lld\n",rr);
}

