#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
#define ll long long
ll n;
ll a[205];
int vd[50005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	ll T;
	scanf("%lld",&T);
	while (T--){
		scanf("%lld",&n);
		for (int i = 1; i <= n; i++) scanf("%lld",&a[i]);
		sort(a+1,a+n+1);
		ll mx = a[n];
		ll cnt = 0;
		for (int i = 0; i <= mx; i++) vd[i] = 0;
		vd[0] = 1;
		for (int i = 1; i <= n; i++)
		if (!vd[a[i]]){
			cnt++;
			for (int j = a[i]; j <= mx; j++)
			if (vd[j-a[i]]) vd[j] = 1;
		}
		printf("%lld\n",cnt);
	}
}
