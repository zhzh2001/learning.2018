bool Memory_test_begin;
#include<bits/stdc++.h>
using namespace std;
#define FOR(a,b,c) for(int a=(b),a##_end__=(c);a<a##_end__;++a)
#define DOR(a,b,c) for(int a=(b)-1,a##_end__=(c);a##_end__<=a;--a)
#define INF 0x3f3f3f3f
template<class T>inline bool chkmin(T&a,T const&b){return b<a?a=b,true:false;}
template<class T>inline bool chkmax(T&a,T const&b){return a<b?a=b,true:false;}
const int M=50005;
struct edge{
	int to,cost;
	edge*next;
}*G[M],Pool[M<<1],*allc=Pool;
int deg[M];
int n,m;
void add_edge(int u,int v,int c){
	*allc=(edge){v,c,G[u]};
	G[u]=allc++;
}
namespace P_1{
	const int M=1005;
	int dp[M][M],Sz[M];
	int root;
	void dfs(int u,int fa,int x){
		memset(dp[u],-1,sizeof(dp[u]));
		dp[u][0]=0;
		Sz[u]=1;
		int E[2],W[2],m=0;
		for(edge*e=G[u];e;e=e->next){
			if(e->to==fa) continue;
			dfs(e->to,u,x);
			E[m]=e->to;
			W[m]=e->cost;
			++m;
			Sz[u]+=Sz[e->to];
		}
		if(m==1){
			int v=E[0];
			FOR(i,0,Sz[v]+1){
				if(dp[v][i]<0) continue;
				int w=dp[v][i]+W[0];
				chkmax(dp[u][i],w);
				if(w>=x) chkmax(dp[u][i+1],0);
			}
		}
		if(m==2){
			int a=E[0],b=E[1];
			FOR(i,0,Sz[a]+1) FOR(j,0,Sz[b]+1){
				if(dp[a][i]<0 or dp[b][j]<0) continue;
				int w=dp[a][i]+W[0];
				int z=dp[b][j]+W[1];
				if(w+z>=x) chkmax(dp[u][i+j+1],0);
				if(w>=x) chkmax(dp[u][i+j+1],z);
				if(z>=x) chkmax(dp[u][i+j+1],w);
				if(w>=x and z>=x) chkmax(dp[u][i+j+2],0);
				chkmax(dp[u][i+j],max(w,z));
			}
		}
	}
	bool check(int x){
		dfs(root,-1,x);
		FOR(i,m,M) if(dp[root][i]>=0) return true;
		return false;
	}
	int main(){
		FOR(i,1,n+1) if(deg[i]==1) root=i;
		int l=0,r=n*10000,res=-1;
		while(l<=r){
			int mid=l+r>>1;
			if(check(mid)) res=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",res);
		return 0;
	}
}
namespace P_2{
	int dp[M];
	int ans;
	void dfs(int u,int fa){
		for(edge*e=G[u];e;e=e->next){
			if(e->to==fa) continue;
			dfs(e->to,u);
			chkmax(ans,dp[u]+dp[e->to]+e->cost);
			chkmax(dp[u],dp[e->to]+e->cost);
		}
	}
	int main(){
		dfs(1,-1);
		printf("%d\n",ans);
		return 0;
	}
}
namespace P_3{
	int A[M];
	bool check(int x){
		int l=0,r=n-2,res=0;
		while(0<=r and x<=A[r]) ++res,--r;
		while(l<r){
			while(l<r and A[l]+A[r]<x) ++l;
			if(l==r) break;
			++res;
			++l,--r;
		}
		return m<=res;
	}
	int main(){
		int w=0,ans=0;
		for(edge*e=G[1];e;e=e->next) A[w++]=e->cost;
		sort(A,A+w);
		int l=0,r=n*10000,res=-1;
		while(l<=r){
			int mid=l+r>>1;
			if(check(mid)) res=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",res);
		return 0;
	}
}
bool Memory_test_end;
int main(){
	//printf("%lf\n",1.0*((&Memory_test_end)-(&Memory_test_begin))/1024/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool flag0=true;
	FOR(i,1,n){
		int u,v,c;
		scanf("%d%d%d",&u,&v,&c);
		add_edge(u,v,c);
		add_edge(v,u,c);
		++deg[u],++deg[v];
		if(u!=1) flag0=false;
	}
	if(m==1) P_2::main();
	else if(flag0) P_3::main();
	else P_1::main();
	return 0;
}
