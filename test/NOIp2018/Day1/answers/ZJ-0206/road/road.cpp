bool Memory_test_begin;
#include<bits/stdc++.h>
using namespace std;
#define FOR(a,b,c) for(int a=(b),a##_end__=(c);a<a##_end__;++a)
template<class T>inline bool chkmin(T&a,T const&b){return b<a?a=b,true:false;}
template<class T>inline bool chkmax(T&a,T const&b){return a<b?a=b,true:false;}
const int M=100005;
int A[M];
int n;
namespace P_1{
	int Fa[M],L[M],R[M],p[M];
	bool vis[M];
	int Find(int x){return x==Fa[x]?x:Fa[x]=Find(Fa[x]);}
	bool cmp(int i,int j){
		return A[i]>A[j];
	}
	void unite(int x,int y){
		x=Find(x),y=Find(y);
		if(A[x]<A[y]) swap(x,y);
		chkmin(L[y],L[x]);
		chkmax(R[y],R[x]);
		Fa[x]=y;
	}
	int main(){
		FOR(i,1,n+1) p[i]=L[i]=R[i]=Fa[i]=i;
		sort(p+1,p+n+1,cmp);
		int ans=0;
		FOR(i,1,n){
			int j=p[i];
			if(j!=Find(j)) continue;
			int l=Find(L[j]-1);
			int r=Find(R[j]+1);
			int Mx=0;
			if(l!=0) chkmax(Mx,A[l]);
			if(r!=0) chkmax(Mx,A[r]);
			ans+=A[j]-Mx;
			if(l!=0 and Mx==A[l]) unite(j,l);
			if(r!=0 and Mx==A[r]) unite(j,r);
		}
		ans+=A[Find(1)];
		printf("%d\n",ans);
		return 0;
	}
}
bool Memory_test_end;
int main(){
	//printf("%lf\n",1.0*((&Memory_test_end)-(&Memory_test_begin))/1024/1024);
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	FOR(i,1,n+1) scanf("%d",A+i);
	P_1::main();
	return 0;
}
