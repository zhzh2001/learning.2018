#include<cstdio>
#include<vector>
#include<cstring>
#include<algorithm>

using namespace std;

int p=0;
int g[50001],to[100001],l[100001],nx[100001];

int n,m,ans;
long long tot;
long long lx,rx,mid;

void add(int u, int v, int w)
{
	++p;
	to[p]=v;
	nx[p]=g[u];
	l[p]=w;
	g[u]=p;
	++p;
	to[p]=u;
	nx[p]=g[v];
	l[p]=w;
	g[v]=p;
}

long long dfs(int u, int f)
{
	vector<long long> d;
	vector<bool> e;
	d.clear();
	e.clear();
	long long t;
	for(int i=g[u]; i; i=nx[i])
	{
		if(to[i]!=f)
		{
			t=dfs(to[i],u)+l[i];
			if(t>=mid) ++ans;
			else d.push_back(t),e.push_back(true);
		}
	}
	if(d.size()) sort(d.begin(),d.end());
	int c=d.size();
	for(int i=0; i<c; ++i)
	{
		if(e[i])
		{
			for(int j=i+1; j<c; ++j)
			{
				if((e[j])&&(d[i]+d[j]>=mid))
				{
					++ans;
					e[i]=e[j]=false;
					break;
				}
			}
		}
	}
	for(int i=c-1; i>=0; --i)
	{
		if(e[i]) return d[i];
	}
	return 0;
	/*int c=-1, a=0, b=d.size()-1;
	while(a<b)
	{
		if(d[a]+d[b]>=mid)
		{
			if(b-a>1)
			{
				if(d[a]+d[b-1]>=mid)
				{
					c=d[b];
					++ans;
					++a;
					b-=2;
				}
				else
				{
					++ans;
					++a;
					--b;
				}
			}
			else
			{
				++ans;
				++a;
				--b;
			}
		}
		else
		{
			if(d[a]>c) c=d[a]; 
			++a;
		}
	}
	if(a==b)
	{
		if(c>d[b]) return c;
		else return d[b];
	}
	else
	{
		if(c==-1) return 0;
		else return c;
	}*/
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int a,b,c;
	for(int i=1; i<n; ++i)
	{
		scanf("%d%d%d",&a,&b,&c);
		add(a,b,c);
		tot+=c;
	}
	lx=1,rx=tot;
	while(lx<rx)
	{
		mid=(lx+rx+1)>>1;
		ans=0;
		dfs(1,0);
		if(ans>=m) lx=mid;
		else rx=mid-1;
	}
	printf("%lld",rx);
	return 0;
}
