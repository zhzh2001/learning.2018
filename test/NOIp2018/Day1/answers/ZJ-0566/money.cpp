#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

int t;
int n,ans;
int a[207];
bool f[50007];

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d",&n);
		for(int i=0; i<n; ++i) scanf("%d",&a[i]);
		sort(a,a+n);
		ans=n;
		memset(f,false,sizeof(f));
		f[0]=true;
		for(int i=0; i<n; ++i)
		{
			if(f[a[i]]) --ans;
			for(int j=1; j<=a[n-1]; ++j)
				if(j-a[i]>=0) f[j]=f[j]||f[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
