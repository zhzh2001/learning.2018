#include<bits/stdc++.h>
using namespace std;
int q,n,ans,a[105];bool dp[25005];
inline int rea()
{
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	int x=ch-'0';ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	q=rea();
	for (int p=0;p<q;p++){
		n=rea(),ans=0;
		for (int i=1;i<=n;i++) a[i]=rea();
		memset(dp,false,sizeof(dp));
		sort(a+1,a+n+1),dp[0]=true;
		for (int i=1;i<=n;i++){
			if (dp[a[i]]==false) ans++;
			for (int j=a[i];j<=a[n];j++) dp[j]=dp[j]|dp[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
