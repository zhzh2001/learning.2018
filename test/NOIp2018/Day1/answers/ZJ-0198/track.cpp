#include<bits/stdc++.h>
using namespace std;
#define MAXN 50005
int n,m,x,y,z,nedge,ans,nxt[MAXN*2],head[MAXN],too[MAXN*2],value[MAXN*2],a[MAXN],f[MAXN];bool flag;
inline int rea()
{
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	int x=ch-'0';ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
inline bool cmp(int x,int y)
{
	return x>y;
}
inline void addedge(int x,int y,int z)
{
	nxt[++nedge]=head[x],head[x]=nedge,too[nedge]=y,value[nedge]=z;
	nxt[++nedge]=head[y],head[y]=nedge,too[nedge]=x,value[nedge]=z;
}
inline void dfs(int x,int fa,int sum)
{
	int k=head[x];ans=max(ans,sum);
	while (k>0){
		int v=too[k];
		if (v!=fa) dfs(v,x,sum+value[k]);
		k=nxt[k];
	}
}
inline bool check(int mid)
{
	int x=1,sum=0,tot=0;
	while (x<=n){
		int k=head[x];
		while (k>0){
			int v=too[k];
			if (v==x+1){sum+=value[k];break;} 
			k=nxt[k];
		}
		if (sum>=mid) tot++,sum=0;
		if (tot>=m) return true;
		x=x+1;
	}
	return false;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=rea(),m=rea();
	for (int i=1;i<n;i++){
		x=rea(),y=rea(),z=rea(),addedge(x,y,z);
		if (y!=x+1) flag=true;a[i]=z;
	}
	if (flag==false){
		int l=1,r=1e9,mid;
		while (l<=r){
			mid=(l+r)/2;
			if (check(mid)==true) l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%d\n",ans);return 0;
	}
	if (m==1){
		for (int i=1;i<=n;i++) dfs(i,0,0);
		printf("%d\n",ans);return 0;
	}
	sort(a+1,a+n,cmp),ans=1e9;
	for (int i=1;i<n;i++){
		int k=1;
		for (int j=2;j<=m;j++)
			if (f[j]<f[k]) k=j;
		f[k]+=a[i];
	}
	for (int i=1;i<=m;i++)
		if (f[i]<ans) ans=f[i];
	printf("%d\n",ans);
	return 0;
}
