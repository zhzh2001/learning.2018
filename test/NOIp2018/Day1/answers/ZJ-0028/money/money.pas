var
vis:array[0..2500000] of boolean;
a:array[0..500] of longint;
t,i,n,ans:longint;
procedure qsort(l,r:longint);
var i,j,t,m:longint;
begin
  i:=l;j:=r;m:=a[(l+r) div 2];
  repeat
    while a[i]<m do inc(i);
    while a[j]>m do dec(j);
    if not (i>j) then
      begin
        t:=a[i];a[i]:=a[j];a[j]:=t;
        inc(i);dec(j);
      end;
  until i>j;
  if i<r then qsort(i,r);
  if l<j then qsort(l,j);
end;
procedure dfs(x,y,z:longint);
var i:longint;
begin
  if z>1 then vis[y]:=true;
  if x=0 then x:=1;
  for i:=x to n do
    begin
      if a[i]+y>a[n] then break;
      dfs(i,a[i]+y,z+1);
    end;
end;
begin
assign(input,'money.in');reset(input);
assign(output,'money.out');rewrite(output);
  readln(t);
  while t>0 do
    begin
      dec(t);
      readln(n);
      for i:=1 to n do
        read(a[i]);
      fillchar(vis,sizeof(vis),false);
      qsort(1,n);
      dfs(0,0,0);
      ans:=0;
      vis[a[1]]:=false;
      for i:=1 to n do
        if not vis[a[i]] then inc(ans);
      writeln(ans);
    end;
close(input);close(output);
end.

