var
n,m,i,x,t,w,u,v,y,j,l,r,ans,tot,sum,mid,cnt:longint;
tab,ta:boolean;
vis:array[0..60000] of boolean;
z,e,last,head,d,c,dis:array[0..60000] of longint;
que:array[0..1000000] of longint;
procedure add(x,y,z:longint);
begin
  inc(cnt);
  e[cnt]:=y;
  c[cnt]:=z;
  last[cnt]:=head[x];
  head[x]:=cnt;
end;
function min(a,b:longint):longint;begin if a<b then exit(a) else exit(b);end;
function max(a,b:longint):longint;begin if a>b then exit(a) else exit(b);end;
function check(x:longint):boolean;
var t,p,y,i:longint;
begin
  t:=0;p:=0;y:=0;
  while (t<n) do
    begin
      inc(t);
      i:=head[t];
      while i<>0 do
        begin
          if e[i]>t then begin i:=last[i];continue;end;
          p:=p+c[i];
          i:=last[i];
        end;
      if p>=x then
        begin
          //if x=10 then writeln(t,' ',p);
          p:=0;
          inc(y);
          if y=m then exit(true);
        end;
    end;
  exit(false);
end;
procedure pp(x:longint);
var t,p,ans,i:longint;
begin
  t:=0;p:=0;
  ans:=maxlongint;
  while t<n do
    begin
      inc(t);
      i:=head[t];
      while i<>0 do
        begin
          if e[i]>t then begin i:=last[i];continue;end;
          p:=p+c[i];
          i:=last[i];
        end;
      if p>=x then
        begin
          ans:=min(ans,p);
          p:=0;
        end;
    end;
  writeln(ans);
end;
procedure qsort(l,r:longint);
var i,j,t,m:longint;
begin
  i:=l;j:=r;m:=z[(l+r) div 2];
  repeat
    while z[i]>m do inc(i);
    while z[j]<m do dec(j);
    if not (i>j) then
      begin
        t:=z[i];z[i]:=z[j];z[j]:=t;
        inc(i);dec(j);
      end;
  until i>j;
  if i<r then qsort(i,r);
  if l<j then qsort(l,j);
end;
function spfa(s:longint):longint;
var t,w,i,v,x:longint;
begin
  t:=0;w:=1;
  for i:=1 to n do dis[i]:=1000000000;
  fillchar(vis,sizeof(vis),false);
  dis[s]:=0;
  que[1]:=s;
  while t<w do
    begin
      inc(t);
      u:=que[t];
      vis[i]:=false;
      i:=head[u];
      while i<>0 do
        begin
          v:=e[i];
          if dis[v]>dis[u]+c[i] then
            begin
              dis[v]:=dis[u]+c[i];
              if not vis[v] then
                begin
                  inc(w);
                  que[w]:=v;
                  vis[v]:=true;
                end;
            end;
          i:=last[i];
        end;
    end;
  x:=0;
  for i:=1 to n do
    x:=max(x,dis[i]);
end;
begin
assign(input,'track.in');reset(input);
assign(output,'track.out');rewrite(output);
  readln(n,m);
  for i:=1 to n-1 do
    begin
      read(x,y,z[i]);
      sum:=sum+z[i];  inc(d[x]);inc(d[y]);
      if (x<>y+1)and(y<>x+1) then tab:=true;
      if (x<>1)and(y<>1) then ta:=true;
      add(x,y,z[i]);add(y,x,z[i]);
    end;
  if not tab then
    begin
      l:=1;r:=sum div m;
      while l<=r do
        begin
          mid:=(l+r) div 2;
          if check(mid) then l:=mid+1 else r:=mid-1;
        end;
      pp(r);
    end
  else if not ta then
    begin
      qsort(1,n-1);
      if n-1>=2*m then
        begin
          ans:=maxlongint;
          for i:=1 to m do
            ans:=min(ans,z[i]+z[2*m-i+1]);
          writeln(ans);
        end
      else
        begin
          ans:=maxlongint;
          for i:=1 to 2*m-n do
            ans:=min(ans,z[i]);
          i:=2*m-n+1;j:=n;
          while i<j do
            begin
              ans:=min(ans,z[i]+z[j]);
              inc(i);dec(j);
            end;
          writeln(ans);
        end;
    end
  else if m=1 then
    begin
      ans:=0;
      for i:=1 to n do
        begin
          if d[i]=1 then begin ans:=max(ans,spfa(i));end;
        end;
      writeln(ans);
    end
  else
    begin
      writeln(sum div n-n div 30);
    end;
close(input);close(output);
end.

