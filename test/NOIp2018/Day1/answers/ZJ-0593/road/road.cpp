#include <bits/stdc++.h>
using namespace std;
#define rint register int
#define IL inline
#define rep(i,h,t) for (int i=h;i<=t;i++)
#define dep(i,t,h) for (int i=t;i>=h;i--)
#define me(x) memset(x,0,sizeof(x))
#define mid ((h+t)>>1)
#define mid2 ((h+t+1)>>1)
#define ll long long
const int INF=1e9;
char ss[1<<24],*A=ss,*B=ss;
IL char gc()
{
  return A==B&&(B=(A=ss)+fread(ss,1,1<<24,stdin),A==B)?EOF:*A++;
}
template<class T>void read(T &x)
{
  rint f=1,c; while (c=gc(),c<48||c>57) if (c=='-') f=-1; x=(c^48);
  while (c=gc(),c>47&&c<58) x=(x<<3)+(x<<1)+(c^48); x*=f;
}
char sr[1<<24],z[20]; int Z,C=-1;
template<class T>void wer(T x)
{
  if (x<0) sr[++C]='-',x=-x;
  while (z[++Z]=x%10+48,x/=10);
  while (sr[++C]=z[Z],--Z);
}
IL void wer1()
{
  sr[++C]=' ';
}
IL void wer2()
{
  sr[++C]='\n';
}
template<class T>IL void mina(T &x,T y)
{
  if (x>y) x=y;
}
template<class T>IL void maxa(T &x,T y)
{
  if (x<y) x=y;
}
template<class T>IL T MAX(T x,T y)
{
   return x>y?x:y;
}
template<class T>IL T MIN(T x,T y)
{
  return x<y?x:y;
}
const int N=1e6;
int a[N];
int main()
{
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
  int n;
  read(n);
  rep(i,1,n) read(a[i]);
  ll ans=0;
  rep(i,1,n)
    if (a[i]>a[i-1]) ans+=a[i]-a[i-1];
  wer(ans);
  fwrite(sr,1,C+1,stdout);
  return 0;
}
