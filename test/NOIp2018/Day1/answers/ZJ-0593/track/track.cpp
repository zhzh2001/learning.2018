#include <bits/stdc++.h>
using namespace std;
#define rint register int
#define IL inline
#define rep(i,h,t) for (int i=h;i<=t;i++)
#define dep(i,t,h) for (int i=t;i>=h;i--)
#define me(x) memset(x,0,sizeof(x))
#define mid ((h+t)>>1)
#define mid2 ((h+t+1)>>1)
#define ll long long
const int INF=1e9;
char ss[1<<24],*A=ss,*B=ss;
IL char gc()
{
  return A==B&&(B=(A=ss)+fread(ss,1,1<<24,stdin),A==B)?EOF:*A++;
}
template<class T>void read(T &x)
{
  rint f=1,c; while (c=gc(),c<48||c>57) if (c=='-') f=-1; x=(c^48);
  while (c=gc(),c>47&&c<58) x=(x<<3)+(x<<1)+(c^48); x*=f;
}
char sr[1<<24],z[20]; int Z,C=-1;
template<class T>void wer(T x)
{
  if (x<0) sr[++C]='-',x=-x;
  while (z[++Z]=x%10+48,x/=10);
  while (sr[++C]=z[Z],--Z);
}
IL void wer1()
{
  sr[++C]=' ';
}
IL void wer2()
{
  sr[++C]='\n';
}
template<class T>IL void mina(T &x,T y)
{
  if (x>y) x=y;
}
template<class T>IL void maxa(T &x,T y)
{
  if (x<y) x=y;
}
template<class T>IL T MAX(T x,T y)
{
   return x>y?x:y;
}
template<class T>IL T MIN(T x,T y)
{
  return x<y?x:y;
}
const int N=1e5+10;
int head[N],l,f[N],g[N],n,m;
int ve[N],ve2[N];
struct re{
  int a,b,c;
}e[N*2];
IL void arr(int x,int y,int z)
{
  e[++l].a=head[x];
  e[l].b=y;
  e[l].c=z;
  head[x]=l;
}
void dfs(int x,int y,int k)
{
  for (rint u=head[x];u;u=e[u].a)
  {
    int v=e[u].b;
    if (v!=y)
    {
      dfs(v,x,k);
    }
  }
  int ans=0,cnt=0;
  for (rint u=head[x];u;u=e[u].a)
  {
    int v=e[u].b;
    if (v!=y)
    {
      ans+=f[v];
      ve[++cnt]=g[v]+e[u].c;
    }
  }
  sort(ve+1,ve+cnt+1);
  rep(i,1,cnt) ve2[i]=ve[i];
  int h=0,t=cnt;
  int ans2=0;
  while (h<t)
  {
    while (h<t&&ve[h]+ve[t]<k) h++;
    if (h>=t) break;
    ans2++;
    if (h!=0) h++;
    t--;
  }
  int lsth1=0,lstt1=0;
  int h1=1,t1=cnt,ans4=0;
  while (h1<=t1)
  {
    if (h1==lsth1&&t1==lstt1) break;
    lsth1=h1,lstt1=t1;
    int cnt2=0;
    int midd=(h1+t1+1)/2;
    rep(i,1,cnt)
    { 
      if (i!=midd) 
        ve[++cnt2]=ve2[i];
    }
    int h=0,t=cnt2;
    int ans3=0;
    while (h<t)
    {
      while (h<t&&ve[h]+ve[t]<k) h++;
      if (h>=t) break;
      ans3++;
      if (h!=0) h++;
      t--;
    }
    if (ans2==ans3) h1=midd,maxa(ans4,ve2[h1]); else t1=midd-1;
  }
  f[x]=ans+ans2; g[x]=ans4;
}
IL bool check(int x)
{
  me(g); me(f);
  dfs(1,0,x);
  int ans=f[1];
  if (ans>=m) return(1); else return(0);
}
int main()
{
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  read(n); read(m);
  rep(i,1,n-1)
  {
    int x,y,z;
    read(x); read(y); read(z);
    arr(x,y,z); arr(y,x,z);
  }
  int h=0,t=INF;
  while (h<t)
  {
    if (check(mid2)) h=mid2;
    else t=mid2-1;
  }
  wer(h);
  fwrite(sr,1,C+1,stdout);
  return 0;
}
