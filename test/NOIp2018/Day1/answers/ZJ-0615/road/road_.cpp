#include <cstdio>
#include <algorithm>

using namespace std;

int g[120000];
int i,n;

inline int merge(int l,int r)
{
	if (l>r)
		return 0;
	int t=100000,v=l;
	for (int i=l;i<=r;i++)
		t=min(t,g[i]);
	int s=t;
	for (int i=l;i<=r;i++)
	{
		g[i]=g[i]-t;
		if (! g[i])
			s=s+merge(v,i-1),v=i+1;
	}
	s=s+merge(v,r);
	return s;
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road_.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;i++)
		scanf("%d",&g[i]);
	printf("%d",merge(1,n));
	return 0;
}
