#include <cstdio>
#include <ctime>
#include <algorithm>

using namespace std;

inline int Rand()
{
	return rand()<<15|rand();
}

int main()
{
	freopen("road.in","w",stdout);
	srand((int) time(0));
	int n=100000;
	printf("%d\n",n);
	for (int i=1;i<=n;i++)
		printf("%d ",Rand()%10001);
	return 0;
}
