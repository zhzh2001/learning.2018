#include <cstdio>
#include <algorithm>

using namespace std;

int g[120000],l[120000],r[120000];
int i,n,s;

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;i++)
		scanf("%d",&g[i]);
	g[n+1]=-1;
	for (i=1;i<=n;i++)
		for (l[i]=i-1;g[i]<g[l[i]];l[i]=l[l[i]]);
	for (i=n;i>0;i--)
		for (r[i]=i+1;g[i]<=g[r[i]];r[i]=r[r[i]]);
	for (i=1;i<=n;i++)
		s=s+g[i]-max(g[l[i]],g[r[i]]);
	printf("%d",s);
	return 0;
}
