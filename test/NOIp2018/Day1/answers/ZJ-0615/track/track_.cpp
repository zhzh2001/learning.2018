#include <cstdio>
#include <set>

using namespace std;

int edge[120000],next[120000],dist[120000],first[60000];
int i,m,n,l,r,x,y,z,mid,sum_edge;

inline void addedge(int x,int y,int z)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],dist[sum_edge]=z,first[x]=sum_edge;
	return;
}

inline pair <int,int> dfs(int x,int y)
{
	multiset <int> S;
	multiset <int> :: iterator it,it_;
	S.clear();
	pair <int,int> tmp,sum;
	sum.first=sum.second=0;
	for (int i=first[x];i!=0;i=next[i])
		if (edge[i]!=y)
		{
			tmp=dfs(edge[i],x);
			tmp.second=tmp.second+dist[i];
			if (tmp.second>=mid)
				tmp.first++,tmp.second=0;
			sum.first=sum.first+tmp.first;
			if (tmp.second)
				S.insert(tmp.second);
		}
	for (it=S.begin();it!=S.end();)
	{
		it_=S.lower_bound(mid-(*it));
		if (it==it_)
			it_++;
		if (it_!=S.end())
			sum.first++,S.erase(it_),it_=it,it++,S.erase(it_);
		else
			sum.second=max(sum.second,*it),it++;
	}
	return sum;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track_.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<n;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		addedge(x,y,z),addedge(y,x,z);
	}
	l=0,r=500000000;
	while (l<r)
	{
		mid=(l+r)/2+1;
		if (dfs(1,0).first>=m)
			l=mid;
		else
			r=mid-1;
	}
	printf("%d",l);
	return 0;
}
