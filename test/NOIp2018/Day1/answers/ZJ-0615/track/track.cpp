#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

struct treenode
{
	int left,right,num;
};

treenode tree[25000000];
int root[60000];
int sum_node;

int edge[120000],next[120000],dist[120000],first[60000];
int i,m,n,l,r,x,y,z,mid,sum_edge;

inline void addedge(int x,int y,int z)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],dist[sum_edge]=z,first[x]=sum_edge;
	return;
}

inline void clear()
{
	memset(root,0,sizeof(root));
	sum_node=0;
	return;
}

inline void insert(int l,int r,int x,int y,int &node)
{
	if (! node)
	{
		sum_node++,node=sum_node;
		tree[node].left=tree[node].right=tree[node].num=0;
	}
	tree[node].num=tree[node].num+y;
	if (l!=r)
	{
		int mid=(l+r)>>1;
		if (x<=mid)
			insert(l,mid,x,y,tree[node].left);
		else
			insert(mid+1,r,x,y,tree[node].right);
	}
	return;
}

inline int leftmost(int l,int r,int x,int node)
{
	if (! tree[node].num)
		return 0;
	if (l==r)
		return l;
	int mid=(l+r)>>1;
	if (x>mid)
		return leftmost(mid+1,r,x,tree[node].right);
	int tmp=leftmost(l,mid,x,tree[node].left);
	if (tmp)
		return tmp;
	else
		return leftmost(mid+1,r,mid+1,tree[node].right);
}

inline pair <int,int> dfs(int x,int y)
{
	pair <int,int> tmp,sum;
	sum.first=sum.second=0;
	for (int i=first[x];i!=0;i=next[i])
		if (edge[i]!=y)
		{
			tmp=dfs(edge[i],x);
			tmp.second=tmp.second+dist[i];
			if (tmp.second>=mid)
				tmp.first++,tmp.second=0;
			sum.first=sum.first+tmp.first;
			if (tmp.second)
				insert(1,mid,tmp.second,1,root[x]);
			if (sum.first>=m)
				return sum;
		}
	while (tree[root[x]].num)
	{
		int left=leftmost(1,mid,1,root[x]);
		insert(1,mid,left,-1,root[x]);
		int opp=leftmost(1,mid,mid-left,root[x]);
		if (opp)
			sum.first++,insert(1,mid,opp,-1,root[x]);
		else
			sum.second=max(sum.second,left);
		if (sum.first>=m)
			return sum;
	}
	return sum;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<n;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		addedge(x,y,z),addedge(y,x,z);
		r=r+z;
	}
	l=0,r=r/m;
	while (l<r)
	{
		clear();
		mid=(l+r)/2+1;
		if (dfs(1,0).first>=m)
			l=mid;
		else
			r=mid-1;
	}
	printf("%d",l);
	return 0;
}
