#include <cstdio>
#include <ctime>
#include <algorithm>

using namespace std;

inline int Rand()
{
	return rand()<<15|rand();
}

int main()
{
	freopen("track.in","w",stdout);
	srand((int) time(0));
	int n=8,m=Rand()%n+1;
	printf("%d %d\n",n,m);
	for (int i=1;i<n;i++)
		printf("%d %d %d\n",Rand()%i+1,i+1,Rand()%10000+1);
	return 0;
}
