#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

int b[12],x[12],y[12],z[12];
int i,m,n,s;

inline void dfs(int o)
{
	if (o==n)
	{
		int d[12],r=1000000000;
		for (int i=1;i<=m;i++)
		{
			memset(d,0,sizeof(d));
			for (int j=1;j<n;j++)
				if (b[j]==i)
					d[x[j]]++,d[y[j]]++;
			int t=0;
			for (int j=1;j<=n;j++)
			{
				if (d[j]>2)
					return;
				if (d[j]==1)
					t++;
			}
			if (t!=2)
				return;
			t=0;
			for (int j=1;j<n;j++)
				if (b[j]==i)
					t=t+z[j];
			r=min(r,t);
		}
		s=max(s,r);
	}
	else
	{
		for (int i=0;i<=m;i++)
			b[o]=i,dfs(o+1);
	}
	return;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track__.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<n;i++)
		scanf("%d%d%d",&x[i],&y[i],&z[i]);
	dfs(1);
	printf("%d",s);
	return 0;
}
