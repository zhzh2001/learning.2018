#include <cstdio>
#include <cstring>

const int M=1000000;

bool b[M+5],f[M+5],h[M+5];
int i,j,k,n,r,s,t;

int main()
{
	freopen("money.in","r",stdin);
	freopen("money_.out","w",stdout);
	scanf("%d",&r);
	for (k=1;k<=r;k++)
	{
		memset(f,0,sizeof(f));
		f[0]=1;
		scanf("%d",&n);
		for (i=1;i<=n;i++)
		{
			scanf("%d",&t);
			for (j=0;j<=M-t;j++)
				f[j+t]=f[j+t]|f[j];
		}
		memset(h,1,sizeof(h));
		for (i=1;i<=M;i++)
			for (j=i;j<=M;j=j+i)
				if (! f[j])
				{
					h[i]=0;
					break;
				}
		s=0;
		memset(b,0,sizeof(b));
		b[0]=1;
		for (i=1;i<=M;i++)
			if ((h[i]) && (b[i]^f[i]))
			{
				s++;
				for (j=0;j<=M-i;j++)
					b[j+i]=b[j+i]|b[j];
			}
		printf("%d\n",s);
	}
	return 0;
}
