#include <cstdio>
#include <cstring>

bool b[1000005],f[1000005],h[1000005];
int i,j,k,m,n,r,s,t;

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&r);
	for (k=1;k<=r;k++)
	{
		memset(f,0,sizeof(f));
		f[0]=1;
		scanf("%d",&n);
		if (n<=5)
			m=1000000;
		else
			m=200000;
		for (i=1;i<=n;i++)
		{
			scanf("%d",&t);
			for (j=0;j<=m-t;j++)
				f[j+t]=f[j+t]|f[j];
		}
		memset(h,1,sizeof(h));
		for (i=1;i<=m;i++)
			for (j=i;j<=m;j=j+i)
				if (! f[j])
				{
					h[i]=0;
					break;
				}
		s=0;
		memset(b,0,sizeof(b));
		b[0]=1;
		for (i=1;i<=m;i++)
			if ((h[i]) && (b[i]^f[i]))
			{
				s++;
				for (j=0;j<=m-i;j++)
					b[j+i]=b[j+i]|b[j];
			}
		printf("%d\n",s);
	}
	return 0;
}
