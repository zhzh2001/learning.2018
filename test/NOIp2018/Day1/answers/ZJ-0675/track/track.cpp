#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <queue>
#include <algorithm>
#define maxn 50005
#define INF 2e9

inline char nc() {
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2) {
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}
inline int read() {
	int x=0,f=0;
	char ch=0;
	while (f^=(ch=='-'),!isdigit(ch=nc()));
	while (x=x*10+ch-'0',isdigit(ch=nc()));
	if (f) return -x;return x;
}
int N,M,Ans,aa;
int fa[maxn];
int lnk[maxn],nxt[maxn*2],son[maxn*2],id[maxn*2],tot;
struct Ad {
	int hd[2],w,id;
}A[maxn];
struct hp_Ad {
	int w,id;
	bool operator <(const hp_Ad &y) const{return y.w<w;}
}now;
std::priority_queue<hp_Ad> hp;
inline void Add_e(int x,int y,int z) {son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot,id[tot]=z;}
int getfa(int x) {return fa[x]==x?x:fa[x]=getfa(fa[x]);};
inline void merge_(int x,int y) {
	x=getfa(x),y=getfa(y);
	fa[y]=x;
}
void Dfs(int x,int f,int w) {
	if (w>Ans) Ans=w;
	for (int i=lnk[x];i;i=nxt[i])
		if (son[i]^f) Dfs(son[i],x,w+A[id[i]].w);
}
void Work1() {
	for (int i=1;i<=N;i++)
		Dfs(i,-1,0);
	printf("%d\n",Ans);
	exit(0);
}
inline bool cmp(Ad x,Ad y) {return x.w<y.w;}
void Work2() {
	std::sort(A+1,A+N,cmp);
	Ans=INF;
	for (int i=1;i<=N;i++) Ans=std::min(Ans,A[i].w+A[N-i].w);
	printf("%d\n",Ans);
	exit(0); 	
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	N=read(),M=read();
	for (int i=1;i<=N;i++) fa[i]=i;
	for (int i=1,x,y,w;i<N;i++) {
		A[i]=(Ad){x=read(),y=read(),w=read(),i};
		if (x!=1) aa=2;
		Add_e(x,y,i),Add_e(y,x,i);
		hp.push((hp_Ad){w,i});
	}
	if (M==1) Work1();
	if (aa==0) Work2();
	A[0].hd[0]=-1;
	for (int t=N-1,k,b,min_,q,v;t>M;t--) {
		now.id=0;
		while (A[now.id].hd[0]==-1) now=hp.top(),hp.pop();
		k=-1,b=-1,q=-1,min_=-INF;
		for (int i=0;i<2;i++)
			for (int j=lnk[A[now.id].hd[i]];j;j=nxt[j]) {
					v=getfa(id[j]);
					if (v==now.id||A[v].hd[0]==-1) continue;
					for (int z=0;z<2;z++)
					if (A[v].hd[z]==A[now.id].hd[i]&&A[v].w>min_)
						min_=A[v].w,b=i,k=v,q=z;
				}
		A[now.id].hd[b]=A[k].hd[!q];
		A[k].hd[0]=A[k].hd[1]=-1;
		A[now.id].w+=A[k].w;
		merge_(now.id,k);
		hp.push((hp_Ad){A[now.id].w,now.id});
	}
	now.id=0;
	while (A[now.id].hd[0]==-1) now=hp.top(),hp.pop();
	printf("%d\n",now.w);
	return 0;
}
