#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
#define maxn 105
#define maxv 25005

inline char nc() {
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2) {
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}
inline int read() {
	int x=0,f=0;
	char ch=0;
	while (f^=(ch=='-'),!isdigit(ch=nc()));
	while (x=x*10+ch-'0',isdigit(ch=nc()));
	if (f) return -x;return x;
}

int T,N,A[maxn],Ans,MaxA;
bool f[maxv];
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while (T--) {
		memset(f,0,sizeof(f));
		Ans=N=read(),MaxA=0;
		for (int i=1;i<=N;i++) {
			A[i]=read();
			if (A[i]>MaxA) MaxA=A[i];
		}
		std::sort(A+1,A+1+N);
		f[0]=1;
		for (int i=1;i<=N;i++) if (!f[A[i]]) {
			for (int j=A[i],ed=MaxA;j<=ed;j++)
				f[j]|=f[j-A[i]];
		} else Ans--;
		printf("%d\n",Ans);
	}
	return 0;
}
