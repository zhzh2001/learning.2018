#include <cstdio>
#include <cctype>
#include <algorithm>
#define maxn 100005

inline char nc() {
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2) {
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}
inline int read() {
	int x=0,f=0;
	char ch=0;
	while (f^=(ch=='-'),!isdigit(ch=nc()));
	while (x=x*10+ch-'0',isdigit(ch=nc()));
	if (f) return -x;return x;
}

int N,Cnt;
struct Ad {
	int w,id;
}A[maxn];
bool vis[maxn];
long long Ans;
inline bool cmpid(Ad x,Ad y) {return x.id<y.id;}
inline bool cmpw(Ad x,Ad y) {return x.w<y.w;}
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	N=read(),Cnt=1;
	for (int i=1;i<=N;i++)
		A[i]=(Ad){read(),i};
	std::sort(A+1,A+1+N,cmpw);
	vis[0]=vis[N+1]=1;
	for (int i=1,j;i<=N;) {
		Ans+=1ll*Cnt*(A[i].w-A[i-1].w);
		j=i;
		while (A[j].w==A[i].w) {
			if (vis[A[j].id-1]&&vis[A[j].id+1]) Cnt--;
			if (!vis[A[j].id-1]&&!vis[A[j].id+1]) Cnt++;
			vis[A[j].id]=1;
			j++;
		}
		i=j;
	}
	printf("%lld\n",Ans);
	return 0;
}
