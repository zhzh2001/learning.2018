#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin))?EOF:*p1++)
#define Out() (fwrite(St,1,Top,stdout))
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
using namespace std;
int Top,fn;
static char St[1000000],buf[1000000],nb[100],*p1=buf,*p2=buf;
int N,Ans,A[105];bool F[25005];
inline int read(){
	int ret=0;bool f=0;char ch=gt();
	while(ch<'0'||ch>'9') f^=(ch=='-'),ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return f?-ret:ret;
}
inline void write(int x){
	fn=0,x<0?pt('-'),x=-x:x;
	do nb[++fn]=x%10+'0',x/=10;while(x);
	while(fn>0) pt(nb[fn--]);
}
inline int gcd(int x,int y){return !y?x:gcd(y,x%y);}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(int T=read();T--;){
		N=read(),Ans=0;
		for(int i=1;i<=N;i++) A[i]=read();
		sort(A+1,A+1+N);
		for(int i=1;i<=N;i++){
			memset(F,0,sizeof F);
			F[0]=1;
			for(int j=1;j<i;j++)
			for(int k=0;k<=A[i];k++) F[k]|=F[k-A[j]];
			if(F[A[i]]) Ans++;
		}
		write(N-Ans),pt('\n');
	}
	return Out(),0;
}
