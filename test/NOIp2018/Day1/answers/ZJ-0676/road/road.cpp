#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin))?EOF:*p1++)
#define Out() (fwrite(St,1,Top,stdout))
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
using namespace std;
int Top,fn;
static char St[1000000],buf[1000000],nb[100],*p1=buf,*p2=buf;
int N,cnt,Ans,A[100005];
inline int read(){
	int ret=0;bool f=0;char ch=gt();
	while(ch<'0'||ch>'9') f^=(ch=='-'),ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return f?-ret:ret;
}
inline void write(int x){
	fn=0,x<0?pt('-'),x=-x:x;
	do nb[++fn]=x%10+'0',x/=10;while(x);
	while(fn>0) pt(nb[fn--]);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	N=read();
	for(int i=1;i<=N;i++) 
	  A[i]=read(),Ans=A[i]<Ans?A[i]:Ans;
	for(int i=1;i<=N;i++) A[i]-=Ans;
	for(int i=1;i<=N;i++){
		if(!A[i]) continue;
		if(A[i-1]) Ans-=A[i-1];
		while(A[i+1]>=A[i]&&A[i+1]>0) i++;
		Ans+=A[i];
		while(A[i+1]<=A[i]&&A[i+1]>0) i++;
	}
	write(Ans);
	return Out(),0;
}
