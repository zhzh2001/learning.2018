#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin))?EOF:*p1++)
#define Out() (fwrite(St,1,Top,stdout))
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
using namespace std;
const int maxn=100005;
int Top,fn;
static char St[1000000],buf[1000000],nb[100],*p1=buf,*p2=buf;
int N,M,E,One,Ans,Nxt;
int g[maxn],Q[maxn],e[maxn],W[maxn];
int son[maxn],w[maxn],nxt[maxn],lnk[maxn];
inline int read(){
	int ret=0;bool f=0;char ch=gt();
	while(ch<'0'||ch>'9') f^=(ch=='-'),ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return f?-ret:ret;
}
inline void Add(int x,int y,int z){son[++E]=y,w[E]=z,nxt[E]=lnk[x],lnk[x]=E;}
inline void write(int x){
	fn=0,x<0?pt('-'),x=-x:x;
	do nb[++fn]=x%10+'0',x/=10;while(x);
	while(fn>0) pt(nb[fn--]);
}
inline void Spfa(int x){
	int hed=0,til=1;
	Q[1]=x,g[x]=0;
	while(hed^til){
		for(int j=lnk[Q[++hed]];j;j=nxt[j]){
			g[son[j]]=g[Q[hed]]+w[j];
			Q[++til]=son[j],Ans=g[son[j]]>Ans?g[son[j]]:Ans;
		}
	}
}
inline bool check(int x){
	int lst=1,hed=0,til=0;Ans=1<<30;
	for(int i=1;i<=N;i++){
		while(hed<=til&&W[Q[til]]>=W[i]) til--;
		Q[++til]=i;
		while(hed<=til&&Q[hed]<lst) hed++;
		lst=Q[hed],Ans=min(Ans,W[i]-W[Q[hed]]);
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	N=read(),M=read();
	for(int i=1;i<N;i++){
		int x=read(),y=read(),z=read();
		Add(x,y,z),Add(y,x,z),One+=(x==1),e[++e[0]]=z,Nxt+=(y==x+1);
	}
	if(M==1&&One!=N-1){
		for(int i=1;i<=N;i++) Spfa(i);
		return write(Ans),Out(),0;
	}
	if(One==N-1){
		sort(e+1,e+1+e[0]),Ans=1<<30;
		if(M*2>e[0]) return write(e[e[0]-M+1]),Out(),0;
		else{
			for(int i=e[0];i>=e[0]-M+1;i--) Ans=min(Ans,e[i]+e[e[0]+M-i]);
			return write(Ans),Out(),0;
		}
	}
	if(Nxt==N-1){
		int hed=0,til=1;
		Q[1]=1;
		while(hed^til){
			for(int j=lnk[Q[++hed]];j;j=nxt[j])
			  W[son[j]]=W[Q[hed]]+w[j],Q[++til]=son[j];
		}
		int L=1,R=1<<30,mid;
		while(L<=R){
			mid=L+((R-L)>>1);
			if(check(mid)) L=mid+1;else R=mid-1;
		}
		return write(R),Out(),0;
	}
	return Out(),0;
}
