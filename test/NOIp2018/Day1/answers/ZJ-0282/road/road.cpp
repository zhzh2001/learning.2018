#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define N (100010)
#define LL long long 
using namespace std;
struct xds{
	int l,r,pos;
}t[N<<3];
int n,a[N];
LL ans;
int min_(int x,int y){
	if(a[x]<a[y])return x;else return y;
}
void build(int l,int r,int x){
	t[x].l=l,t[x].r=r;
	if(l==r){
		t[x].pos=l;
		return;
	}
	int mid=(l+r)>>1;
	build(l,mid,x*2),build(mid+1,r,x*2+1);
	t[x].pos=min_(t[x*2].pos,t[x*2+1].pos);
}
int query(int l,int r,int x){
	if(t[x].l==l&&t[x].r==r)return t[x].pos;
	int mid=(t[x].l+t[x].r)>>1;
	if(r<=mid)return query(l,r,x*2);
	else if(l>mid)return query(l,r,x*2+1);
	else return min_(query(l,mid,x*2),query(mid+1,r,x*2+1));
}
void work(int l,int r,int v){
	if(l>r)return;
	if(l==r){
		ans+=((LL)a[l]-(LL)v);
		return;
	}
	int mi=query(l,r,1),val=a[mi];
	ans+=((LL)val-(LL)v);
	work(l,mi-1,val),work(mi+1,r,val);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	scanf("%d",&a[i]);
	build(1,n,1);
	work(1,n,0);
	printf("%lld\n",ans);
}
