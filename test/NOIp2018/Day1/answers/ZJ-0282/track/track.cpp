#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<set>
#define N (50010)
#define LL long long 
using namespace std;
int n,m,fi[N],ne[N<<1],b[N<<1],c[N<<1],E;
int dis[N],f[N],len[N],a[N],ru[N];
bool used[N];
multiset<int> S;
set<int>::iterator it,it2;
void add(int x,int y,int z){
	ne[++E]=fi[x],fi[x]=E,b[E]=y,c[E]=z,ru[y]++;
}
void dfs(int u,int pre){
	for(int i=fi[u];i;i=ne[i]){
		int v=b[i];
		if(v==pre)continue;
		dis[v]=dis[u]+c[i];
		dfs(v,u);
	}
}
void dp(int u,int pre,int lim){
	int cnt=0;
	for(int i=fi[u];i;i=ne[i]){
		int v=b[i];
		if(v==pre)continue;
		dp(v,u,lim),f[u]+=f[v];
	}
	for(int i=fi[u];i;i=ne[i]){
		int v=b[i];
		if(v==pre)continue;
		if(len[v]+c[i]>=lim)f[u]++;
		else a[++cnt]=len[v]+c[i];
	}
	if(!cnt)return;
	if(cnt==1){
		len[u]=a[1];
		return;
	}
	sort(a+1,a+cnt+1);
	for(int i=1;i<=cnt;i++)used[i]=0;
	S.clear();
	for(int i=1;i<=cnt;i++)
	S.insert(a[i]);
	for(int i=1;i<=cnt;i++){
		it2=S.find(a[i]);
		if(it2==S.end())continue;
		S.erase(it2);
		it=S.lower_bound(lim-a[i]);
		if(it!=S.end()){
			f[u]++;
			S.erase(it);
		}
		else len[u]=a[i];
	}
}
bool check(int lim){
	memset(f,0,sizeof(f));
	memset(len,0,sizeof(len));
	dp(1,0,lim);
	return f[1]>=m;
}
void dp1(int u,int pre,int lim){
	int cnt=0;
	for(int i=fi[u];i;i=ne[i]){
		int v=b[i];
		if(v==pre)continue;
		dp(v,u,lim),f[u]+=f[v];
	}
	for(int i=fi[u];i;i=ne[i]){
		int v=b[i];
		if(v==pre)continue;
		if(len[v]+c[i]>=lim)f[u]++;
		else a[++cnt]=len[v]+c[i];
	}
	if(!cnt)return;
	if(cnt==1){
		len[u]=a[1];
		return;
	}
	sort(a+1,a+cnt+1);
	for(int i=1;i<=cnt;i++)used[i]=0;
	for(int i=1;i<=cnt;i++){
		if(used[i])continue;
		bool flag=0;
		for(int j=1;j<=cnt;j++){
			if(used[j]||i==j)continue;
			if(a[i]+a[j]>=lim){
				used[i]=used[j]=1,f[u]++,flag=1;
				break;
			}
		}
		if(!flag)len[u]=a[i];
	}
}
bool check1(int lim){
	memset(f,0,sizeof(f));
	memset(len,0,sizeof(len));
	dp1(1,0,lim);
	return f[1]>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int L=0,R=0; bool flag=1;
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z),add(y,x,z),R+=z;
	}
	for(int i=1;i<=n;i++)if(ru[i]>5)flag=0;
	if(n<=3000)flag=1;
	dfs(1,0);
	if(m==1){
		int ans=0,ll=1;
		for(int i=2;i<=n;i++)
		if(dis[i]>dis[ll])ll=i;
		dis[ll]=0,dfs(ll,0);
		for(int i=1;i<=n;i++)
		if(dis[i]>ans)ans=dis[i];
		printf("%d\n",ans);
		return 0;
	}
	else if(flag){
		int ans=0;
		while(L<=R){
			int mid=(L+R)>>1;
			if(check1(mid))L=mid+1,ans=mid;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
	else {
		int ans=0;
		while(L<=R){
			int mid=(L+R)>>1;
			if(check(mid))L=mid+1,ans=mid;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}
