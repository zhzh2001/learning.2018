#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define N (110)
#define M (25010)
#define LL long long 
using namespace std;
int n,a[N],m,ans,T;
bool vis[M];
void check(int x){
	for(int i=x;i<=m;i++)
	vis[i]|=vis[i-x];
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n),m=0;
		for(int i=1;i<=n;i++)
		scanf("%d",&a[i]),m=max(m,a[i]);
		sort(a+1,a+n+1),ans=0;
		memset(vis,0,sizeof(vis)),vis[0]=1;
		for(int i=1;i<=n;i++)
		if(!vis[a[i]])ans++,check(a[i]);
		printf("%d\n",ans);
	}
}
