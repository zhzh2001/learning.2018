#include<cstdio>
#include<cstring>
#include<cctype>
#include<algorithm>
using namespace std;
inline int rad(){
	int ret=0,flg=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-')flg=-1;ch=getchar();}
	while (isdigit(ch))ret=(ret<<3)+(ret<<1)+ch-'0',ch=getchar();
	return ret*flg;
}
const int maxn=50005;
int n,m,lnk[maxn],nxt[maxn*2],son[maxn*2],w[maxn*2],f[maxn][2],tot,dis[maxn],ans;
inline void add(int x,int y,int z){
	son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot,w[tot]=z;
}
void dp(int x,int fa){
	for (int i=lnk[x];i;i=nxt[i]){
		int u=son[i];
		if (u==fa)continue;
		dp(u,x);
		if (f[u][0]+w[i]>f[x][0])f[x][1]=f[x][0],f[x][0]=f[u][0]+w[i];
		else if (f[u][0]+w[i]>f[x][1])f[x][1]=f[u][0]+w[i];
	}
}
int main(){
	freopen("track.in","r",stdin);
	n=rad(),m=rad();
	for (int i=1;i<n;i++){
		int x=rad(),y=rad(),z=rad();
		add(x,y,z),add(y,x,z);
	}
	dp(1,0);
	for (int i=1;i<=n;i++)ans=max(ans,f[i][0]+f[i][1]);
	printf("%d\n",ans);
	return 0;
}
