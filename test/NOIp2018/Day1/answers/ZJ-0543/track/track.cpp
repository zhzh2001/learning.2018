#include<cstdio>
#include<cstring>
#include<cctype>
#include<algorithm>
#define q_mid(L,R) (L+((R-L)>>1))
using namespace std;
inline int rad(){
	int ret=0,flg=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-')flg=-1;ch=getchar();}
	while (isdigit(ch))ret=(ret<<3)+(ret<<1)+ch-'0',ch=getchar();
	return ret*flg;
}
const int maxn=50005;
int n,m,lnk[maxn],nxt[maxn*2],son[maxn*2],w[maxn*2],in[maxn],ans,cnt,fmx,smx,lef[maxn],tl,f[maxn][2],lin[maxn],tln,dis[maxn],tot,mx;
inline void add(int x,int y,int z){
	son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot,w[tot]=z;
}
void get_hmr(int x,int fa,int len,int wcl){
	for (int i=lnk[x];i;i=nxt[i]){
		int u=son[i];
		if (u==fa)continue;
		if (len+w[i]>=wcl)cnt++,get_hmr(u,x,0,wcl);
		else get_hmr(u,x,len+w[i],wcl);
	}
}
void dp(int x,int fa){
	for (int i=lnk[x];i;i=nxt[i]){
		int u=son[i];
		if (u==fa)continue;
		dp(u,x);
		if (f[u][0]+w[i]>f[x][0])f[x][1]=f[x][0],f[x][0]=f[u][0]+w[i];
		else if (f[u][0]+w[i]>f[x][1])f[x][1]=f[u][0]+w[i];
	}
}
inline int get_ans(){
	int ret=0;
	for (int i=1;i<=n;i++)ret=max(ret,f[i][0]+f[i][1]);
	return ret;
}
inline void work_1(){
	dp(1,0);
	printf("%d\n",get_ans());
}
inline void work_2(){
	int L=0,R=mx;
	while (L<=R){
		int mid=q_mid(L,R),vis=0;
		for (int i=1;i<=tl;i++){
			cnt=0,get_hmr(lef[i],0,0,mid);
			if (cnt>=m){vis=1;break;}
		}
		if (cnt)ans=mid,L=mid+1;else R=mid-1;
	}
	printf("%d\n",ans);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=rad(),m=rad();
	for (int i=1;i<n;i++){
		int x=rad(),y=rad(),z=rad();
		mx+=z,add(x,y,z),add(y,x,z),in[y]++;
	}
	for (int i=1;i<=n;i++)if (!in[i])lef[++tl]=i;
	if (m==1)work_1();else work_2();
	return 0;
}
