#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
#define mem(x) memset(x,0,sizeof(x))
using namespace std;
inline int rad(){
	int ret=0,flg=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-')flg=-1;ch=getchar();}
	while (isdigit(ch))ret=(ret<<3)+(ret<<1)+ch-'0',ch=getchar();
	return ret*flg;
}
const int maxn=1e2+6,maxv=25006;
int T,f[25005],a[maxn],vis[maxv],n,b[maxn],ans;
inline int check(int x){
	mem(f),f[0]=1;
	for (int i=1;i<=ans;i++)
	for (int j=b[i];j<=x;j++){
		if (f[j-b[i]])f[j]=1;
		if (f[x])return 1;
	}
	return f[x];
}
inline int check_if_div(int x,int y){
	return !(y%x);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (T=rad();T;T--){
		mem(vis),n=rad();
		for (int i=1;i<=n;i++)a[i]=rad();
		sort(a+1,a+1+n),ans=0;
//		for (int i=1;i<=n;i++)printf("%d ",a[i]);printf("\n");
		for (int i=1;i<n;i++)
		for (int j=i+1;j<=n;j++)
		if (check_if_div(a[i],a[j])&&!vis[a[i]]){
			if (check(a[i])){vis[a[i]];continue;}
			vis[b[++ans]=a[i]]=1;
			break;
		}
		for (int i=1;i<=n;i++){
			if (vis[a[i]])continue;
			if (!check(a[i]))b[++ans]=a[i],vis[a[i]]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
