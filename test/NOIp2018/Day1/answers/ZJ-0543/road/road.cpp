#include<cstdio>
#include<cctype>
#include<algorithm>
#define q_mid(L,R) (L+((R-L)>>1))
using namespace std;
inline int rad(){
	int ret=0,flg=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-')flg=-1;ch=getchar();}
	while (isdigit(ch))ret=(ret<<3)+(ret<<1)+ch-'0',ch=getchar();
	return ret*flg;
}
const int maxn=100006,INF=0x7fffffff;
struct ypc{
	int val,laz,id;
}st[5*maxn];
int n,a[maxn],ret,ret_id;
typedef long long LL;
LL ans;
void build(int rt,int L,int R){
	if (L==R){st[rt].val=a[L],st[rt].id=R;return;}
	int mid=q_mid(L,R);
	build(rt*2,L,mid),build(rt*2+1,mid+1,R);
	if (st[rt*2].val<st[rt*2+1].val)st[rt].val=st[rt*2].val,st[rt].id=st[rt*2].id;
	else st[rt].val=st[rt*2+1].val,st[rt].id=st[rt*2+1].id; 
}
inline void pushdown(int rt){
	st[rt*2].val+=st[rt].laz,st[rt*2+1].val+=st[rt].laz;
	st[rt*2].laz+=st[rt].laz,st[rt*2+1].laz+=st[rt].laz;
	st[rt].laz=0;
}
void query(int rt,int L,int R,int qL,int qR){
	if (L>qR||R<qL)return;
	if (L>=qL&&R<=qR){
		if (st[rt].val<ret)ret=st[rt].val,ret_id=st[rt].id;
		return;
	}
	int mid=q_mid(L,R);
	if (st[rt].laz<0)pushdown(rt);
	query(rt*2,L,mid,qL,qR),query(rt*2+1,mid+1,R,qL,qR);
}
void update(int rt,int L,int R,int qL,int qR,int delta){
	if (L>qR||R<qL)return;
	if (L>=qL&&R<=qR){
		st[rt].val+=delta,st[rt].laz+=delta;
		return;
	}
	int mid=q_mid(L,R);
	if (st[rt].laz<0)pushdown(rt);
	update(rt*2,L,mid,qL,qR,delta),update(rt*2+1,mid+1,R,qL,qR,delta);
	if (st[rt*2].val<st[rt*2+1].val)st[rt].val=st[rt*2].val,st[rt].id=st[rt*2].id;
	else st[rt].val=st[rt*2+1].val,st[rt].id=st[rt*2+1].id; 
}
void divide(int L,int R){
	if (L>R)return;
	ret=INF,ret_id=0,query(1,1,n,L,R);
	int bak_ret=ret,bak_ret_id=ret_id;
	ans+=bak_ret,update(1,1,n,L,R,-bak_ret);
	divide(L,bak_ret_id-1),divide(bak_ret_id+1,R);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=rad();
	for (int i=1;i<=n;i++)a[i]=rad();
	build(1,1,n),divide(1,n);
	printf("%lld\n",ans);
	return 0;
}
