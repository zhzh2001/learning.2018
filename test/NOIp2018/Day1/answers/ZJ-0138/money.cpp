#include<bits/stdc++.h>
#define N 105
#define M 25005
using namespace std;
void read(int&n){
	n=0;int k=1;char ch=getchar();
	while(!('0'<=ch&&ch<='9')){if(ch=='-')k=-1;ch=getchar();}
	while('0'<=ch&&ch<='9')n=n*10+ch-48,ch=getchar();
	n*=k;
}
int T,n,a[N],ans;bool f[M];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	while(T--){
		read(n);
		for(int i=1;i<=n;i++)read(a[i]);
		memset(f,0,sizeof(f));f[0]=1;
		sort(a+1,a+1+n);ans=0;
		for(int i=1;i<=n;i++){
			if(f[a[i]]){ans++;continue;}
			for(int j=a[i];j<=25000;j++)
				if(f[j-a[i]])f[j]=1;
		}
		printf("%d\n",n-ans);
	}
	return 0;
}
