#include<bits/stdc++.h>
#define N 50005
using namespace std;
void read(int&n){
	n=0;int k=1;char ch=getchar();
	while(!('0'<=ch&&ch<='9')){if(ch=='-')k=-1;ch=getchar();}
	while('0'<=ch&&ch<='9')n=n*10+ch-48,ch=getchar();
	n*=k;
}
int nxt[N<<1],head[N],to[N<<1],val[N<<1],tot;
void add(int x,int y,int z){
	nxt[++tot]=head[x],to[tot]=y,head[x]=tot,val[tot]=z;
	nxt[++tot]=head[y],to[tot]=x,head[y]=tot,val[tot]=z;
}
int f[N],a[N];
void dfs(int k,int x){
	f[k]=0,a[k]=k;
	for(int i=head[k];i;i=nxt[i])
		if(to[i]!=x){
			dfs(to[i],k);
			if(f[to[i]]+val[i]>f[k])
			f[k]=f[to[i]]+val[i],a[k]=a[to[i]];
		}
}
int l,r,sum,p[N],c[N];
int t,w,mid;bool b[N];
void Dfs(int k,int x){
	f[k]=0;
	for(int i=head[k];i;i=nxt[i])
		if(to[i]!=x)Dfs(to[i],k);
	tot=0;
	for(int i=head[k];i;i=nxt[i])
		if(to[i]!=x)p[++tot]=f[to[i]]+val[i],b[tot]=0;
	sort(p+1,p+1+tot);
	for(int i=tot;i;i--)
		if(p[i]>=mid)sum++,tot--;else break;
	l=1;r=tot;
	while(l<r){
		if(p[l]+p[r]>=mid)
		b[l]=1,b[r]=1,c[r]=p[l],l++,r--,sum++;
		else l++;
	}
	for(int i=tot;i;i--)
		if(!b[i]){
			l=i;
			for(int j=i+1;j<=tot;j++)
				if(c[j]+p[l]>=mid)l=j;
			f[k]=p[l];return;
		}
}
int n,m,x,y,z;bool bo1,bo2;
bool pd(){
	sum=0;
	Dfs(1,0);
	if(sum>=m)return 1;else return 0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);bo1=bo2=1;
	for(int i=1;i<n;i++){
		read(x),read(y),read(z);
		add(x,y,z);
		if(x!=1)bo1=0;
		if(y!=x+1)bo2=0;
	}
	if(m==1){
		dfs(1,0);
		dfs(x=a[1],0);
		printf("%d\n",f[x]);
		return 0;
	}
	t=1;w=1000000000;
	while(t<w){
		mid=(t+w)>>1;
		if(pd())t=mid+1;else w=mid;
	}
	printf("%d\n",t-1);
	return 0;
}
