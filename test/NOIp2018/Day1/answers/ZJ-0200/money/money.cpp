#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
using namespace std;
int T,n,a[1000],x;
long long ans;
bool b[30000];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]),x=max(x,a[i]);
		ans=n;
		sort(a+1,a+1+n);
		for (int k=n;k>=2;k--){
			memset(b,0,sizeof(b));
			b[0]=1;
			for (int i=1;i<=x;i++){
				for (int j=1;j<k;j++)
					if (i>=a[j]) b[i]=b[i]||b[i-a[j]];
			}
			if (b[a[k]]) ans--; 
		}
		printf("%lld\n",ans);
	}
	return 0;
}
