#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<queue>
#include<iostream>
#define N 50100
#define ll long long
using namespace std;
int n,m,x,y,z,tot,last[N],cnt;
ll dis[N],son[N],ans,l,r;
bool use[N];
struct A{
	int to,pre,val;
}q[N*4];
void add_edge(int x,int y,int z){
	q[++tot].to=y;
	q[tot].val=z;
	q[tot].pre=last[x];
	last[x]=tot;
}
ll search(int x,int fa){
	ll son,dis1=0,dis2=0;
	for (int i=last[x];i;i=q[i].pre){
		int to=q[i].to;
		if (to!=fa){
			son=search(to,x)+q[i].val;
			if (son>dis1) dis2=dis1,dis1=son;
			else if (son>dis2) dis2=son;
		}
	}
	if (dis1+dis2>r) r=dis1+dis2;
	return dis1;
}
void search2(int u,int fa,ll up){
	priority_queue<ll> p;
	for (int i=last[u];i;i=q[i].pre){
		int v=q[i].to;
		if (v!=fa){
			search2(v,u,up);
			dis[v]+=q[i].val;
			if (dis[v]<up) p.push(dis[v]);
			else cnt++;
		}
	}
	int tot=0;
	memset(use,0,sizeof(use));
	while (!p.empty()){
		son[++tot]=p.top();
		p.pop();
	}
	for (int i=1;i<=tot/2;i++) swap(son[i],son[tot-i+1]);
	for (int i=1;i<tot;i++){
		if (use[i]) continue;
		for (int j=i+1;j<=tot;j++){
			if (!use[j]&&son[i]+son[j]>=up) {use[i]=1,use[j]=1,cnt++;break;}
		}
	}
	while (use[tot]) tot--;
	dis[u]=son[tot];
}
bool check(ll x){
    cnt=0;
    memset(dis,0,sizeof(dis));
	search2(1,0,x);
	return (cnt>=m);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		add_edge(x,y,z);
		add_edge(y,x,z);
	}
	search(1,0);
	l=0;
	while (l<r){
		int mid=(l+r)>>1;
		if (check(mid)) ans=mid,l=mid+1;
		else r=mid;
	}
	if (check(ans+1)) ans++;
	printf("%lld\n",ans);
	return 0;
}
