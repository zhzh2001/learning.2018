#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cctype>
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
#define per(i,b,a) for(int i=(b);i>=(a);--i)
#define mp make_pair
#define pb push_back
#define fi first
#define se second

using namespace std;

inline int read(void){
	int x=0,f=1;char c=getchar();
	while(!isdigit(c)){f=c=='-'?-1:1;c=getchar();}
	while(isdigit(c)){x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x * f;
}
inline int quickpow(int m,int n,int p){int b=1;while(n){if(n&1)b=b*m%p;n>>=1;m=m*m%p;}return b;}
inline int getinv(int x,int p){return quickpow(x,p-2,p);}

const int MAXN = 1e5 + 100;

long long ans = 0;

int a[MAXN],n;

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n = read();
	for (int i = 1; i <= n; ++i) a[i] = read();
	a[0] = 0;
	for (int i = 1; i <= n; ++i) ans += (a[i] > a[i-1]) ? (a[i] - a[i-1]) : 0;
	printf("%lld\n",ans);
	return 0; 
}

