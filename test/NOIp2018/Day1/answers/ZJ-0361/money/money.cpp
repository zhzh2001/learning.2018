#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<iostream>
#include<map>
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
#define per(i,b,a) for(int i=(b);i>=(a);--i)
#define mp make_pair
#define pb push_back
#define fi first
#define se second

using namespace std;

inline int read(void){
	int x=0,f=1;char c=getchar();
	while(!isdigit(c)){f=c=='-'?-1:1;c=getchar();}
	while(isdigit(c)){x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x * f;
}
inline int quickpow(int m,int n,int p){int b=1;while(n){if(n&1)b=b*m%p;n>>=1;m=m*m%p;}return b;}
inline int getinv(int x,int p){return quickpow(x,p-2,p);}

map<int,int>mp;
int n,a[110],mx;
int f[25010];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (int T = read(); T; --T) {
		int ans = 0;
		memset(f,0,sizeof(f));
		n = read();
		mx = -1;
		rep(i,1,n) a[i] = read(),mx = max(a[i],mx);
		sort(a+1,a+n+1);
		f[0] = 1;
		rep(i,1,n) {
			if (f[a[i]]) continue;
			++ans;
			rep(j,0,mx) 
			if (j-a[i]>=0) f[j] = f[j] | f[j-a[i]];
		}
		//rep(i,1,n) if (!f[a[i]]) ++ans;
//		rep(i,2,n) {
//			f.reset();
//			f[0] = 1;
//			rep(j,1,i-1) {
//				if (!vis[j])
//				rep(k,a[j],a[i]) f[k] = f[k-a[j]] | f[k];
//				if (f[a[i]]) break;
//			}
//			vis[i] = f[a[i]];
//		}
		//rep(i,1,n) ans += (!vis[i]);//cout<<vis[i]<<endl;
		printf("%d\n",ans);
	}
	return 0;
}

