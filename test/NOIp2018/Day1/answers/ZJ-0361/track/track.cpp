#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<vector>
#include<iostream>
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
#define per(i,b,a) for(int i=(b);i>=(a);--i)
#define mp make_pair
#define pb push_back
#define fi first
#define se second

#define int long long
using namespace std;

inline int read(void){
	int x=0,f=1;char c=getchar();
	while(!isdigit(c)){f=c=='-'?-1:1;c=getchar();}
	while(isdigit(c)){x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x * f;
}
inline int quickpow(int m,int n,int p){int b=1;while(n){if(n&1)b=b*m%p;n>>=1;m=m*m%p;}return b;}
inline int getinv(int x,int p){return quickpow(x,p-2,p);}

const int MAXN = 5e4 + 100;

vector<pair<int,int> >lt[MAXN];
inline void addedge(int u,int v,int w){lt[u].pb(mp(w,v));}
int n,m;
pair<int,int> t[MAXN];
vector<int>vec[MAXN];
inline void dfs2(int u,int ff,int x){
	t[u] = mp(0,0);
	for (int i = 0; i < (int)lt[u].size(); ++i) {
		int v = lt[u][i].se;
		if (v == ff) continue;
		dfs2(v,u,x);
		t[u].fi += t[v].fi;
		vec[u].pb(lt[u][i].fi + t[v].se);
	}
	int ret = 0,lst = 0;
	sort(vec[u].begin(),vec[u].end());          
	int ll = 0,rr = (int)vec[u].size()-1;
	while(ll < rr) {
		if (vec[u][rr] >= x) {
			++ret;--rr;
			continue;
		}
		else if (vec[u][rr] + vec[u][ll] >= x) {
			++ret;--rr;++ll;
		}
		else lst = max(lst,vec[u][ll]),++ll;
	}
	if (ll == rr) {
		if (vec[u][rr] >= x) ++ret;
		else lst = max(vec[u][rr],lst);
	}
	t[u] = mp(t[u].fi + ret,lst);
 }

inline bool isok(int x){
	rep(i,1,n) vec[i].clear();
	dfs2(1,0,x);
	return (t[1].fi >= m);
}

signed main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int sum = 0;
	n = read(),m = read();
	rep(i,1,n-1) {
		int u = read(),v = read(),w = read();
		sum += w;
		addedge(u,v,w);
		addedge(v,u,w);
	}
	int l = 1,r = sum,res = -1;
	while(l <= r) {
		int mid = (l + r) >> 1;
		if (isok(mid)) {
			l = mid + 1;
			res = mid;
		}
		else r = mid - 1;
	}
	printf("%lld\n",res);
	return 0;
}

