#include<cstdio>
#define N 100005
inline int read(){
    int x=0;char ch=getchar();
    for(;ch<48||ch>57;ch=getchar());
    for(;ch>47&&ch<58;ch=getchar())
    x=(x<<1)+(x<<3)+(ch^48);return x;
}
using namespace std;
int a[N];
int main(){
    freopen("road.in","r",stdin);
    freopen("road.out","w",stdout);
    int n=read(),ans=0;
    for(int i=1;i<=n;++i)
        a[i]=read();
    for(int i=1;i<=n;++i)
        if(a[i]>a[i-1])
            ans+=a[i]-a[i-1];
    printf("%d\n",ans);
    return 0;
}
