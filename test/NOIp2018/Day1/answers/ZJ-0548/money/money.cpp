#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 105
#define M 10005
inline int read(){
    int x=0;char ch=getchar();
    for(;ch<48||ch>57;ch=getchar());
    for(;ch>47&&ch<58;ch=getchar())
    x=(x<<1)+(x<<3)+(ch^48);return x;
}
using namespace std;
int vis[M],a[N];
int main(){
    freopen("money.in","r",stdin);
    freopen("money.out","w",stdout);
    for(int T=read();T--;){
        memset(vis,0,sizeof vis);
        int n=read(),m=0;
        for(int i=0;i<n;++i)a[i]=read();
        sort(a,a+n);vis[0]=1;
        for(int i=0;i<n;++i){
            if(vis[a[i]])continue;
            for(int j=0;j+a[i]<M;++j)
                if(vis[j])
                    vis[j+a[i]]=1;
            ++m;
        }
        printf("%d\n",m);
    }
    return 0;
}
