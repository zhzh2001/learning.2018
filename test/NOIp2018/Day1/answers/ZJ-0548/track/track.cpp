#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 50005
inline int read(){
    int x=0;char ch=getchar();
    for(;ch<48||ch>57;ch=getchar());
    for(;ch>47&&ch<58;ch=getchar())
    x=(x<<1)+(x<<3)+(ch^48);return x;
}
using namespace std;
int arr[N<<1],nxt[N<<1],cot[N<<1],fir[N],d[N],e;
void ADD(int u,int v,int w){
    arr[++e]=v;nxt[e]=fir[u];fir[u]=e;cot[e]=w;++d[v];
}
int rt,mxn,val[N],f[N],g[N];
void Dfs(int u,int fa,int mx){
    for(int i=fir[u],v;i;i=nxt[i])
        if((v=arr[i])!=fa)
            Dfs(v,u,mx+cot[i]);
    if(mx>mxn)mxn=mx,rt=u;
}
int check(int n,int x){
    int cnt=0,res=0;
    for(int i=1;i<n;++i){
        cnt+=cot[fir[i]];
        if(cnt>=x)cnt=0,++res;
    }
    return res;
}
void dfs(int u,int fa,int x){
    int cnt=0;
    for(int i=fir[u],v;i;i=nxt[i])
        if((v=arr[i])^fa)
            dfs(v,u,x),f[u]+=f[v];
    for(int i=fir[u],v;i;i=nxt[i])
        if((v=arr[i])^fa)
            val[++cnt]=g[v]+cot[i];
    sort(val+1,val+cnt+1);int l=1;
    //printf("%d: %d\n",u,cnt);
    //for(int i=1;i<=cnt;++i)
        //printf("%d ",val[i]);puts("");
    for(int i=cnt;i;--i)
        if(val[i]>=x)++f[u];else{
            for(;l<i&&val[l]+val[i]<x;++l)
                g[u]=val[l];
            if(l==i){g[u]=val[i];break;}++f[u];
        }
    if(cnt==1)g[u]=val[1];
}
int main(){
    freopen("track.in","r",stdin);
    freopen("track.out","w",stdout);
    int n=read(),m=read(),o=0,ans=1e9;
    for(int i=1,u,v,w;i<n;++i)
        u=read(),v=read(),w=read(),
        ADD(u,v,w),ADD(v,u,w);
    if(m==1){//test1:
        Dfs(1,0,0);mxn=0;Dfs(rt,0,0);
        return printf("%d\n",mxn),0;
    }
    if(d[1]+1==n){//tes2:
        int cnt=0;
        for(int i=fir[1];i;i=nxt[i])
            val[++cnt]=cot[i];
        sort(val+1,val+n);
        cnt=min(n-m-1,(n-1)/2);
        for(int i=1;i<=cnt;++i)
            ans=min(ans,val[i]+val[2*cnt-i+1]);
        for(int i=cnt*2+1;i<n;++i)
            ans=min(ans,val[i]);
        return printf("%d\n",ans),0;
    }
    for(int i=1;i<=n;++i)o+=d[i]>2;
    if(!o){//test3:
        int l=0,r=5e8+10;
        for(;l<=r;){
            int mid=(l+r)>>1;
            check(n,mid)<m?r=mid-1:l=mid+1;
        }
        return printf("%d\n",r),0;
    }o=0;
    for(int i=1;i<=n;++i)o+=d[i]>3;
    if(!o){//test4:
        //none.
        return 0;
    }
    //dfs(1,0,15);printf("%d\n",f[1]);
    ///*
    //ans???
    int l=0,r=1e9;
    for(;l<=r;){
        memset(g,0,sizeof g);
        memset(f,0,sizeof f);
        int mid=(l+r)>>1;dfs(1,0,mid);
        f[1]<m?r=mid-1:l=mid+1;
    }
    return printf("%d\n",r),0;
    //*/
}
