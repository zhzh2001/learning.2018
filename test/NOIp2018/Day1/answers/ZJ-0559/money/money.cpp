#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
inline int getint() {
	register char ch;
	while(!isdigit(ch=getchar()));
	register int x=ch^'0';
	while(isdigit(ch=getchar())) x=(((x<<2)+x)<<1)+(ch^'0');
	return x;
}
const int N=101,M=25001;
int a[N];
bool f[M];
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(register int T=getint();T;T--) {
		const int n=getint();
		int m=0;
		for(register int i=1;i<=n;i++) {
			a[i]=getint();
			m=std::max(m,a[i]);
		}
		std::sort(&a[1],&a[n]+1);
		memset(f,0,sizeof f);
		f[0]=true;
		int ans=0;
		for(register int i=1;i<=n;i++) {
			if(f[a[i]]) continue;
			ans++;
			for(register int j=a[i];j<=m;j++) {
				f[j]|=f[j-a[i]];
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
