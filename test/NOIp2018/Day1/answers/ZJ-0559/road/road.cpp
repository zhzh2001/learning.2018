#include<cstdio>
#include<cctype>
inline int getint() {
	register char ch;
	while(!isdigit(ch=getchar()));
	register int x=ch^'0';
	while(isdigit(ch=getchar())) x=(((x<<2)+x)<<1)+(ch^'0');
	return x;
}
const int N=1e5+1;
int d[N];
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	const int n=getint();
	for(register int i=1;i<=n;i++) {
		d[i]=getint();
	}
	for(register int i=n;i>=1;i--) {
		d[i]-=d[i-1];
	}
	int ans=0;
	for(register int i=1;i<=n;i++) {
		if(d[i]>0) ans+=d[i];
	}
	printf("%d\n",ans);
	return 0;
}
