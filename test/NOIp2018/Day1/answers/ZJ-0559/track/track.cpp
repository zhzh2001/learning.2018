#include<set>
#include<cstdio>
#include<cctype>
#include<vector>
#include<climits>
#include<algorithm>
inline int getint() {
	register char ch;
	while(!isdigit(ch=getchar()));
	register int x=ch^'0';
	while(isdigit(ch=getchar())) x=(((x<<2)+x)<<1)+(ch^'0');
	return x;
}
const int N=5e4+1;
struct Edge {
	int to,w;
};
std::vector<Edge> e[N];
inline void add_edge(const int &u,const int &v,const int &w) {
	e[u].push_back((Edge){v,w});
	e[v].push_back((Edge){u,w});
}
std::multiset<int> t;
int f[N],g[N],len;
void dfs(const int &x,const int &par) {
	f[x]=0;
	for(unsigned i=0;i<e[x].size();i++) {
		const int &y=e[x][i].to;
		if(y==par) continue;
		dfs(y,x);
		f[x]+=f[y];
	}
	for(unsigned i=0;i<e[x].size();i++) {
		const int &y=e[x][i].to;
		if(y==par) continue;
		t.insert(g[y]+e[x][i].w);
	}
	while(!t.empty()) {
		const int u=*t.rbegin();
		if(u>=len) {
			f[x]++;
			t.erase(t.find(u));
		} else {
			break;
		}
	}
	g[x]=0;
	while(!t.empty()) {
		const int u=*t.begin();
		t.erase(t.find(u));
		const std::multiset<int>::iterator p=t.lower_bound(len-u);
		if(p==t.end()) {
			g[x]=u;
		} else {
			t.erase(p);
			f[x]++;
		}
	}
	t.clear();
}
inline int calc(const int &k) {
	len=k;
	dfs(1,0);
	return f[1];
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	const int n=getint(),m=getint();
	int l=INT_MAX,r=0;
	for(register int i=1;i<n;i++) {
		const int u=getint(),v=getint(),w=getint();
		add_edge(u,v,w);
		l=std::min(l,w);
		r+=w;
	}
	while(l<=r) {
		const int mid=(l+r)>>1;
		if(calc(mid)>=m) {
			l=mid+1;
		} else {
			r=mid-1;
		}
	}
	printf("%d\n",l-1);
	return 0;
}
