#include<ctime>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
const int N=5e4+1;
int p[N];
int main() {
	srand(time(NULL));
	const int n=5e4,m=rand()%(n-1)+1;
	printf("%d %d\n",n,m);
	for(register int i=1;i<=n;i++) p[i]=i;
	std::random_shuffle(&p[1],&p[n]+1);
	for(register int i=2;i<=n;i++) {
		printf("%d %d %d\n",p[i],p[rand()%(i-1)+1],rand()%10000+1);
	}
	return 0;
}
