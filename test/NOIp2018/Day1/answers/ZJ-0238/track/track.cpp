#include<bits/stdc++.h>
using namespace std;
int head[50005],to[100001],nxt[100001],lon[100001],top=0;
int n,m;
int u,v,w;
int maxx,id,bo=0;
int de[50005];
int sum=0;
int ma=10001;
void dfs(int so,int fa)
{
	for(int i=head[so];i;i=nxt[i])
	if(to[i]!=fa)
	{
		de[to[i]]=de[so]+lon[i];
		if(maxx<de[to[i]]) 
		maxx=de[to[i]],id=to[i];
		dfs(to[i],so);
	}
}
void dfs2(int so,int fa)
{
	for(int i=head[so];i;i=nxt[i])
	if(to[i]!=fa)
	{
		de[to[i]]=lon[i];
		dfs2(to[i],so);
	}
}
int work(int minn)
{
	int zuo=1,zhi;
	for(int i=1;i<=m;i++)
	{
		if(zuo>n) return 0;
 		zhi=0;
 		while(zhi<minn)
		{
			zuo++;
			if(zuo>n) return 0;
			zhi+=de[zuo];
		}
	}
	return 1;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++)
	{
		scanf("%d%d%d",&u,&v,&w);
		if(u!=v+1&&v!=u+1) bo=1;
		sum+=w; ma=min(ma,w);
		nxt[++top]=head[u];
		head[u]=top;
		to[top]=v;
		lon[top]=w;
		
		nxt[++top]=head[v];
		head[v]=top;
		to[top]=u;
		lon[top]=w;
	}
	if(m==1)
	{
		maxx=-1;
		dfs(1,0);
		memset(de,0,sizeof(de));
		maxx=-1;
		dfs(id,0);
		cout<<maxx<<endl;
		return 0;
	}
	if(!bo)
	{
		dfs2(1,0);
		int l=1,r=sum,mid;
		while(l!=r)
		{
			mid=(l+r+1)/2;
			if(work(mid)) l=mid;
			else          r=mid-1;
		}
		cout<<l<<endl;
		return 0;
	}
	if(m==n-1)
	{
		cout<<ma<<endl;
		return 0;
	}
	return 0;
}
