#include<bits/stdc++.h>
using namespace std;
int t,n,m;
int vis[25001],f[25001];
int ans;
int maxx;
int g[25001],top=0;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		memset(vis,0,sizeof(vis));
		memset(f,0,sizeof(f));
		top=0;
		ans=0; maxx=-1;
		scanf("%d",&n);
		for(int i=1;i<=n;i++) 
		scanf("%d",&m),vis[m]=1,maxx=max(maxx,m);
		for(int i=1;i<=maxx;i++)
		{
			for(int j=1;j<=top;j++)
			{
				f[i]=f[g[j]]&f[i-g[j]];
				if(f[i]) 
				{
					g[++top]=i;
					break;
				}
				if(g[j]>(i/2+1))
				break;
			}
			if(vis[i]&&!f[i])
			ans++,f[i]=1,g[++top]=i;
		}
		cout<<ans<<endl;
	}
	return 0;
}
