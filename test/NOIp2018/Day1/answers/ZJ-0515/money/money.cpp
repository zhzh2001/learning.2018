#include <fstream>
#include <algorithm>
typedef long long ll;
const int N = 102;
int arr_a[N], arr_b[N];

int t; // t is a timestamp too
const int AI = 25004;
int can_express[AI];

bool express(const int m, const int x) // can x = \sigma{b[i] * k[i]} ?
{
	if (m >= 2 && ll(arr_b[0] - 1) * (arr_b[1] - 1) <= x)
		return true;
	return can_express[x] == t;
}
int main()
{
	std::ifstream cin("money.in");
	std::ofstream cout ("money.out");
	cin >> t;
	++t;
	while (--t)
	{
		int n;
		cin >> n;
		for (int i = 0; i < n; ++i)
			cin >> arr_a[i];
		std::sort(arr_a, arr_a + n);
		int m = 0;
		can_express[0] = t;
		for (int i = 0; i < n; ++i)
			if (!express(m, arr_a[i]))
			{
				const int cur = arr_a[i];
				arr_b[m++] = cur;
				for (int i = cur; i < AI; ++i)
					if (can_express[i - cur] == t)
						can_express[i] = t;
			}
		cout << m << std::endl;
	}
	return 0;
}
/×

在下高三狗一枚。
禁赛十年又何妨，君子敢做要敢当。
€€₤，再见。

×/
