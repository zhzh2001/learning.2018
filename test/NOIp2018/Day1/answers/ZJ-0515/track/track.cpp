#include <fstream>
#include <algorithm>
#include <cstring>

//typedef long long ll;

const int N = 50004;
struct edge
{
	int v, l, nxt;
	edge() {}
	edge(int _v, int _l, int _nxt) : v(_v), l(_l), nxt(_nxt) {}
} e[N * 2];
int cnt_e, head[N];
void add_edge(const int u, const int v, const int l)
{
	e[++cnt_e] = edge(v, l, head[u]);
	head[u] = cnt_e;
	e[++cnt_e] = edge(u, l, head[v]);
	head[v] = cnt_e;
}

int n, m;

namespace SUB_M1
{
	bool check()
	{
		return m == 1;
	}
	int dep[N];
	void dfs(const int u, const int d, const int fr)
	{
		dep[u] = d;
		for (int it = head[u]; it != 0; it = e[it].nxt)
			if (e[it].v != fr)
				dfs(e[it].v, d + e[it].l, u);
	}
	int solve()
	{
		dfs(1, 0, 0);
		int max = 0, num = 1;
		for (int i = 2; i <= n; ++i)
			if (max < dep[i])
			{
				max = dep[i];
				num = i;
			}
		memset(dep, 0, sizeof dep);
		dfs(num, 0, 0);
		max = 0;
		for (int i = 1; i <= n; ++i)
			if (max < dep[i])
				max = dep[i];
		return max;
	}
}

namespace M_N1
{
	bool check()
	{
		return m == n - 1;
	}
	int solve()
	{
		int ans = 0x7fffffff;
		for (int i = 1; i <= cnt_e; i += 2)
			if (ans > e[i].l)
				ans = e[i].l;
		return ans;
	}
}

namespace SUB_AI
{
	int nn;
	int l[N];
	bool check()
	{
		for (int it = head[1]; it != 0; it = e[it].nxt)
			l[nn++] = e[it].l;
		return nn == n - 1;
	}
	int solve()
	{
		std::sort(l, l + nn);
		int ans_beg = 0, ans_end = 20004;
		while (ans_end - ans_beg > 1)
		{
			const int ans = (ans_beg + ans_end) >> 1;
			int cnt = 0;
			int ptr_l = -1;
			for (int i = nn - 1; cnt < m; --i)
			{
				do
					++ptr_l;
				while (ptr_l < i && l[ptr_l] + l[i] < ans);
				if (ptr_l == i)
					break;
				else
					++cnt;
			}
			if (cnt == m)
				ans_end = ans;
			else
				ans_beg = ans;
		}
		return ans_end;
	}
}

namespace SUB_AB
{
	int l[N];
	bool check()
	{
		for (int i = 1; i <= n; ++i)
			for (int it = head[i]; it != 0; it = e[it].nxt)
			{
				if (e[it].v == i + 1)
					l[i] = e[it].l;
				else if (std::abs(e[it].v - i) != 1)
					return false;
			}
		return true;
	}
	int solve()
	{
		int ans_beg = 0, ans_end = 500000008;
		while (ans_end - ans_beg > 1)
		{
			const int ans = (ans_beg + ans_end) >> 1;
			int cnt = 0;
			int sum = 0;
			for (int i = 0; i < n; ++i)
			{
				sum += l[i];
				if (sum > ans)
				{
					sum = 0;
					++cnt;
				}
			}
			if (cnt >= m)
				ans_end = ans;
			else
				ans_beg = ans;
		}
		return ans_end;
	}
}

int main()
{
	using std::endl;

	std::ifstream cin("track.in");
	std::ofstream cout("track.out");
	cin >> n >> m;
	for (int i = n - 2; i >= 0; --i)
	{
		int a, b, l;
		cin >> a >> b >> l;
		add_edge(a, b, l);
	}
#define try_solve(SUB)\
	if (SUB::check())\
	{\
		cout << SUB::solve() << endl;\
		return 0;\
	}
	try_solve(SUB_M1);
	try_solve(M_N1);
	try_solve(SUB_AI);
	try_solve(SUB_AB);
	cout << "I vote. Good game." << endl;
	return -1;
}
/×

在下高三狗一枚。
禁赛十年又何妨，君子敢做要敢当。
€€₤，再见。

×/
