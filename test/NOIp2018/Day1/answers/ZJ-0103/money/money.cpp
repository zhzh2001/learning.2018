#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int tst,n,allv,a[105],m,b[105];
bool f[25005];
inline void readi(int &x){
	x=0; char ch=getchar();
	while ('0'>ch||ch>'9') ch=getchar();
	while ('0'<=ch&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
}
int _work(){
	readi(n); for (int i=1;i<=n;i++) readi(a[i]);
	sort(a+1,a+n+1); if (a[1]==1) return 1;
	allv=a[n]; m=0; for (int i=1;i<=allv;i++) f[i]=0; f[0]=1;
	for (int i=1;i<=n;i++)
		if (!f[a[i]]){
			m++;
			for (int j=a[i];j<=allv;j++) f[j]|=f[j-a[i]];
		}
	return m;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	readi(tst);
	while (tst--) printf("%d\n",_work());
	return 0;
}
