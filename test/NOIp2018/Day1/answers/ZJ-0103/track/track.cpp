#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=50005,maxe=100005;
int n,m,ea,eb,tot,son[maxe],nxt[maxe],w[maxe],lnk[maxn],que[maxn],mx,id,a[maxn],b[maxn];
inline char nc(){
	static char buf[100000],*pa=buf,*pb=buf;
	return pa==pb&&(pb=(pa=buf)+fread(buf,1,100000,stdin),pa==pb)?EOF:*pa++;
}
inline void readi(int &x){
	x=0; char ch=nc();
	while ('0'>ch||ch>'9') ch=nc();
	while ('0'<=ch&&ch<='9'){x=x*10+ch-'0'; ch=nc();}
}
void _add(int x,int y,int z){son[++tot]=y; w[tot]=z; nxt[tot]=lnk[x]; lnk[x]=tot;}
void _dfs(int x,int dad,int dep){
	if (dep>mx){mx=dep; id=x;}
	for (int j=lnk[x];j;j=nxt[j])
		if (son[j]!=dad) _dfs(son[j],x,dep+w[j]);
}
void _worka(){
	mx=id=0; _dfs(1,0,0);
	int k=id; _dfs(k,0,0);
	printf("%d",mx);
}
void _workb(){
	sort(a+1,a+n); int ans=0,te=min(2*m,n-1);
	for (int i=1;i<=te;i++) ans-=a[i];
	printf("%d",ans);
}
bool _checkc(int x){
	int tem=0,num=0;
	for (int i=1;i<n;i++){
		tem+=b[i];
		if (tem>=x){tem=0; num++;}
	}
	return num>=m;
}
void _workc(){
	int L=1,R=5e8,mid,ans;
	while (L<=R){
		mid=L+(R-L>>1);
		if (_checkc(mid)){ans=mid; L=mid+1;}
		else R=mid-1;
	}
	printf("%d",ans);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	readi(n); readi(m); tot=ea=eb=0;
	for (int i=1,x,y,z;i<n;i++){
		readi(x); readi(y); readi(z);
		_add(x,y,z); _add(y,x,z);
		if (x==1||y==1){ea++; a[i]=-z;}
		if (y==x+1){eb++; b[x]=z;}
	}
	if (m==1) _worka();
	if (ea+1==n) _workb();
	if (eb+1==n) _workc();
	return 0;
}
