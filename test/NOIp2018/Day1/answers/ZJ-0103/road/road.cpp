#include<cstdio>
using namespace std;
typedef long long LL;
int n,a[100005];
LL ans;
inline void readi(int &x){
	x=0; char ch=getchar();
	while ('0'>ch||ch>'9') ch=getchar();
	while ('0'<=ch&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
}
LL _dfs(int L,int R){
	if (L==R) return a[L];
	if (L>R) return 0;
	while (a[L]==0&&L<=R) L++;
	while (a[R]==0&&L<=R) R--;
	if (L>R) return 0;
	int mn=1e6,lst=L-1;
	for (int i=L;i<=R;i++) mn=(mn>a[i])?a[i]:mn;
	LL sum=mn;
	for (int i=L;i<=R;i++){
		a[i]-=mn;
		if (!a[i]){sum+=_dfs(lst+1,i-1); lst=i;}
	}
	sum+=_dfs(lst+1,R);
	return sum;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	readi(n); for (int i=1;i<=n;i++) readi(a[i]);
	ans=_dfs(1,n); printf("%lld",ans);
	return 0;
}
