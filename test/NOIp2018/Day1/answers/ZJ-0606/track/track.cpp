#include <bits/stdc++.h>

const int Extra=5,Max_N=5e4;

int Read(){
	int x=0,f=0,ch=getchar();
	while (!isdigit(ch)) f^=!(ch^'-'),ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (f) x=-x; return x;
}

void Check_Judge(){
	freopen("track.in","r",stdin),freopen("track.out","w",stdout);
}

struct Edge{
	int w,to,next;
};

Edge E[2*Max_N+Extra];
int n,m,ans,p_len,t[Max_N+Extra],p[Max_N+Extra],f[Max_N+Extra],sum[Max_N+Extra],vis[Max_N+Extra],first[Max_N+Extra];

void Add(int i,int x,int y,int w){
	E[i]=(Edge){w,y,first[x]},first[x]=i;
}

void DFS(int now,int now_fa,int mid){
	f[now]=sum[now]=0;
	for (int i=first[now]; i; i=E[i].next)
		if (E[i].to!=now_fa)
			DFS(E[i].to,now,mid);
	p_len=0;
	for (int i=first[now]; i; i=E[i].next)
		if (E[i].to!=now_fa){
			f[now]+=f[E[i].to];
			if (sum[E[i].to]+E[i].w>=mid)
				++f[now];
			else
				++p_len,vis[p_len]=1,p[p_len]=sum[E[i].to]+E[i].w;
		}
	std::sort(p+1,p+1+p_len);
	int l=1,r=p_len;
	while (l<r)
		if (p[l]+p[r]>=mid)
			++f[now],vis[l]=vis[r]=0,++l,--r;
		else
			++l;
	for (int i=p_len; i>=1; --i)
		if (vis[i]){
			sum[now]=p[i];
			break;
		}
}

int Check(int mid,int k){
	DFS(1,0,mid);
	if (f[1]>=m) return 1;
	for (int i=1; i<=10; ++i){
		int now=rand()%(n-2)+2;
		DFS(now,0,mid);
		if (f[now]>=m) return 1;
	}
	return 0;
}

int check(){
	if (m==1) return 1;
	if (f[1]==n-1) return 1;
	for (int i=1; i<=n; ++i) if (t[i]>3) return 0;
	return 1;
}

int main(){
	Check_Judge();
	srand(19260817);
	n=Read(),m=Read();
	int l=10000,r=0;
	for (int i=2; i<=n; ++i){
		int x=Read(),y=Read(),w=Read();
		++t[x],++t[y];
		l=std::min(l,w),r+=w;
		Add(2*i-1,x,y,w),Add(2*i,y,x,w);
	}
	while (l<=r){
		int mid=l+r>>1;
		if (Check(mid,2))
			ans=mid,l=mid+1;
		else
			r=mid-1;
	}
	if (check()){
		printf("%d",ans);
		return 0;
	}
	l=ans,r=ans+10000;
	while (l<=r){
		int mid=l+r>>1;
		if (Check(mid,20))
			ans=mid,l=mid+1;
		else
			r=mid-1;
	}
	printf("%d",ans);
	return 0;
}
