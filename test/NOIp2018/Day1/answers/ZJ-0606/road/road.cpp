#include <bits/stdc++.h>

const int Extra=5,Max_N=1e5;

int Read(){
	int x=0,f=0,ch=getchar();
	while (!isdigit(ch)) f^=!(ch^'-'),ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (f) x=-x; return x;
}

void Check_Judge(){
	freopen("road.in","r",stdin),freopen("road.out","w",stdout);
}

int n,ans,p_len,a[Max_N+Extra],p[Max_N+Extra];

int main(){
	Check_Judge();
	n=Read();
	for (int i=1; i<=n; ++i){
		a[i]=Read();
		if (a[i]>p[p_len]||p_len==0)
			ans+=a[i]-p[p_len],++p_len,p[p_len]=a[i];
		else{
			while (a[i]>=p[p_len]&&p_len!=0) --p_len;
			++p_len,p[p_len]=a[i];
		}
	}
	printf("%d",ans);
	return 0;
}
