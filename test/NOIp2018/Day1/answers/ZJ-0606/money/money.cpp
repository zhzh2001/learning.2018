#include <bits/stdc++.h>

const int Extra=5,Max_N=1e2,Max_A=25000;

int Read(){
	int x=0,f=0,ch=getchar();
	while (!isdigit(ch)) f^=!(ch^'-'),ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (f) x=-x; return x;
}

void Check_Judge(){
	freopen("money.in","r",stdin),freopen("money.out","w",stdout);
}

int n,ans,a[Max_N+Extra],f[Max_A+Extra];

int main(){
	Check_Judge();
	for (int T=Read(); T>0; --T){
		n=ans=Read();
		for (int i=1; i<=n; ++i) a[i]=Read();
		std::sort(a+1,a+1+n);
		memset(f,0,sizeof f);
		f[0]=1;
		for (int i=1; i<=n; ++i)
			if (f[a[i]])
				--ans;
			else
				for (int j=a[i]; j<=Max_A; ++j)
					if (f[j-a[i]])
						f[j]=1;
		printf("%d\n",ans);
	}
	return 0;
}
