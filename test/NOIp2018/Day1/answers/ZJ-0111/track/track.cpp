#include<bits/stdc++.h>
#define LL long long
using namespace std;
const int M=50005,INF=0x3f3f3f3f;
bool cur1;
int n,m;
struct edge{
	int to,val;
};
vector<edge>E[M];
struct P20_m1{
	int pos;
	LL dis[M],ans;
	void dfs(int x,int f){
		if(dis[x]>ans)ans=dis[x],pos=x;
		for(int i=0;i<(int)E[x].size();i++){
			int y=E[x][i].to;
			if(y==f)continue;
			dis[y]=dis[x]+E[x][i].val;
			dfs(y,x);
		}
	}
	void solve(){
		ans=0;
		dfs(1,0);
		memset(dis,0,sizeof dis);
		dfs(pos,pos);
		printf("%lld\n",ans);
	}
}p20_m1;
struct P{
	multiset<int>S[M];
	int a[M],cnt,dp[M],mid;
	void dfs(int x,int f){
		S[x].clear();
		dp[x]=0;
		for(int i=0;i<(int)E[x].size();i++){
			int y=E[x][i].to;
			if(y==f)continue;
			dfs(y,x);
			S[x].insert(dp[y]+E[x][i].val);
		}
		while(S[x].size()&&*(--S[x].end())>=mid)cnt++,S[x].erase(--S[x].end());
		S[x].insert(INF);
		while(S[x].size()>2){
			int a=*S[x].begin();
			S[x].erase(S[x].find(a));
			int b=*S[x].lower_bound(mid-a);
			if(b==INF){
				dp[x]=max(dp[x],a);
				continue;
			}
			S[x].erase(S[x].find(b));
			cnt++;
		}
		S[x].erase(INF);
		if(S[x].size())dp[x]=max(dp[x],*(--S[x].end()));
	}
	bool check(){
		cnt=0;
		dfs(1,0);
		return cnt>=m;
	}
	void solve(){
		int l=1,r=2e9,ans=0;
		while(l<=r){
			mid=(l+r)>>1;
			if(check())ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}p;
bool cur2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		E[x].push_back((edge){y,z});
		E[y].push_back((edge){x,z});
	}
	if(m==1)
		p20_m1.solve();
	else
		p.solve();
	return 0;
}
