#include<bits/stdc++.h>
using namespace std;
const int M=105;
bool cur1;
int T,n,A[M],dp[25005];
bool cur2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		int V=0;
		for(int i=1;i<=n;i++){
			scanf("%d",&A[i]);
			V=max(V,A[i]);
		}
		sort(A+1,A+1+n);
		memset(dp,0,sizeof dp);
		dp[0]=1;
		int ans=n;
		for(int i=1;i<=n;i++){
			if(dp[A[i]]){
				ans--;
				continue;
			}
			for(int j=A[i];j<=V;j++)
				dp[j]|=dp[j-A[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
