#include<bits/stdc++.h>
using namespace std;
const int N=100005;
long long read()
{
	long long x=0,f=1;
	char ch=getchar();
	while(('0'>ch)||(ch>'9'))
	{
		if(ch=='-')
		f=-1;
		ch=getchar();
	}
	while(('0'<=ch)&&(ch<='9'))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
int n;
int d[N];
long long ans;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
	{
		d[i]=read();
		if(d[i]>d[i-1])
		{
			ans+=d[i]-d[i-1];
		}
	}
	printf("%lld",ans);
	return 0;
}
