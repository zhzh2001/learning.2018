#include<bits/stdc++.h>
using namespace std;
const int N=50005;
long long read()
{
	long long x=0,f=1;
	char ch=getchar();
	while(('0'>ch)||(ch>'9'))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(('0'<=ch)&&(ch<='9'))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
int n,m;
int head[N],ver[N*2],nxt[N*2],edge[N*2],tot;
bool flag[3],fl;
int p;
long long ans;
long long dis[N];
bool v[N*2];
int lef;
long long sedge,se;
queue<int>q;
void add(int x,int y,int z)
{
	ver[++tot]=y;
	edge[tot]=z;
	nxt[tot]=head[x];
	head[x]=tot;
}
void bfs(int rt)
{
	memset(dis,0,sizeof(dis));
	ans=0;
	q.push(rt);
	dis[rt]=0;
	while(q.size())
	{
		int x=q.front();
		q.pop();
		for(int i=head[x];i;i=nxt[i])
		{
			int y=ver[i],z=edge[i];
			if(!dis[y]&&y!=rt)
			{
				dis[y]=dis[x]+z;
				q.push(y);
				if(dis[y]>ans)
				{
					ans=dis[y];
					p=y;
				}
			}
		}
	}
}
bool cmp(long long p,long long q)
{
	return p>q;
}
bool fcheck(long long len)
{
	int h=1,t=n-1;
	int sum=0;
	for(;h<=t;h++)
	if(dis[h]>=len)
	{
		sum++;
	}
	else
	{
		break;
	}
	for(;h<=t;h++)
	{
		for(;t>h;t--)
		if(dis[h]+dis[t]>=len)
		{
			t--;
			sum++;
			break;
		}
	}
	if(sum>=m)
	return 1;
	else
	return 0;
}
bool scheck(long long len)
{
	long long now=0;
	int sum=0;
	for(int i=1;i<n;i++)
	{
		now+=dis[i];
		if(now>=len)
		{
			now=0;
			sum++;
		}
	}
	if(sum>=m)
	return 1;
	else
	return 0;
}
void ndfs(int rt,int x,int now,long long len);
void dfs(int rt,int now,long long len);
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();
	m=read();
	int x,y,z;
	flag[1]=1;
	flag[2]=1;
	tot=1;
	for(int i=1; i<n; i++)
	{
		x=read();
		y=read();
		z=read();
		sedge+=z;
		add(x,y,z);
		add(y,x,z);
		if(x!=1)
		{
			flag[1]=0;
		}
		if(y!=x+1)
		{
			flag[2]=0;
		}
	}
	if(m==1)
	{
		bfs(1);
		bfs(p);
		printf("%lld",ans);	
	}
	else
	if(flag[1])
	{
		int sum=0;
		for(int i=head[1];i;i=nxt[i])
		{
			dis[++sum]=edge[i];
		}
		sort(dis+1,dis+sum+1,cmp);
		long long L=0,R=500000000;
		while(L<=R)
		{
			long long mid=(L+R)/2;
			if(fcheck(mid))
			{
				L=mid+1;
				ans=mid;
			}
			else
			{
				R=mid-1;
			}
		}
		printf("%lld",ans);
	}
	else
	if(flag[2])
	{
		for(x=1;x<n;x++)
		for(int i=head[x];i;i=nxt[i])
		if(ver[i]==x+1)
		{
			dis[x]=edge[i];
		}
		long long L=0,R=500000000;
		while(L<=R)
		{
			long long mid=(L+R)/2;
			if(scheck(mid))
			{
				L=mid+1;
				ans=mid;
			}
			else
			{
				R=mid-1;
			}
		}
		printf("%lld",ans);
	}
	else
	{
		long long L=0,R=500000000;
		while(L<=R)
		{
			long long mid=(L+R)/2;
			fl=0;
			lef=n-1;
			se=sedge;
			memset(v,0,sizeof(v));
			dfs(1,0,mid);
			if(fl)
			{
				L=mid+1;
				ans=mid;
			}
			else
			{
				R=mid-1;
			}
		}
		printf("%lld",ans);
	}
	return 0;
}
void ndfs(int rt,int x,int now,long long len)
{
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i],z=edge[i];
		if(!v[i])
		{
			v[i]=1;
			v[i^1]=1;
			lef--;
			se-=z;
			dis[y]=dis[x]+z;
			if(dis[y]>=len)
			{
				dfs(rt,now+1,len);
				if(fl)
				return;
				dfs(rt+1,now+1,len);
				if(fl)
				return;
			}
			else
			{
				ndfs(rt,y,now,len);
				if(fl)
				return;
			}
			v[i]=0;
			v[i^1]=0;
			lef++;
			se+=z;
		}
	}
}
void dfs(int rt,int now,long long len)
{
	//cout<<rt<<' '<<now<<' '<<len<<endl;
	if(now>=m)
	{
		fl=1;
		return;
	}
	if(se/(m-now)<len)
	{
		return;
	}
	if(lef<m-now)
	return;
	dis[rt]=0;
	ndfs(rt,rt,now,len);
	if(fl)
	return;
	if(rt+1<=n)
	dfs(rt+1,now,len);
	if(fl)
	return;
}
