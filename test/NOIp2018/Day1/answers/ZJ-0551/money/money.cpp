#include<bits/stdc++.h>
using namespace std;
const int N=105;
const int K=25005;
long long read()
{
	long long x=0,f=1;
	char ch=getchar();
	while(('0'>ch)||(ch>'9'))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(('0'<=ch)&&(ch<='9'))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
int T,n;
int a[N];
bool dp[K];
int ans;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();
		ans=n;
		for(int i=1; i<=n; i++)
		{
			a[i]=read();
		}
		sort(a+1,a+n+1);
		memset(dp,0,sizeof(dp));
		dp[0]=1;
		for(int i=1; i<n; i++)
		{
			for(int j=a[i]; j<=a[n]; j++)
			if(!dp[j])
			{
				dp[j]=dp[j-a[i]];
			}
			if(dp[a[i+1]])
			{
				ans--;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
