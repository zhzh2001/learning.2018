#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define maxn 100005
using namespace std;
int n;
int d[maxn];
int a[maxn],tot=0;
ll ans=0;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;++i){
		scanf("%d",d+i);
	}
	d[0]=0;
	d[n+1]=0;
	for(int i=1;i<=n+1;++i){
		int p=d[i]-d[i-1];
		if(p>0){
			++tot;
			a[tot]=p;
		}
		else if(p<0){
			int k=-p;
			while(k>=a[tot] && tot>0){
				ans+=(ll)a[tot];
				k-=a[tot];
				--tot;
			}
			ans+=(ll)k;
			a[tot]-=k;
		}
	}
	printf("%lld\n",ans);
	return 0;
}
