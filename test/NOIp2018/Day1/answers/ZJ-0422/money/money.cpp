#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define maxn 105
#define maxl 25006
using namespace std;
int t;
int n,tot,maxx,ans;
int a[maxn];
int book[maxn];
int check[maxl];
int gcd(int a,int b){
	if(b==0){
		return a;
	}
	return gcd(b,a%b);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	for(int cas=1;cas<=t;++cas){
		scanf("%d",&n);
		for(int i=1;i<=n;++i){
			scanf("%d",a+i);
		}
		sort(a+1,a+n+1);
		tot=n;
		memset(book,0,sizeof(book));
		for(int i=1;i<=tot;++i){
			if(!book[i]){
				for(int j=i+1;j<=tot;++j){
					if(a[j]%a[i]==0){
						book[j]=1;
					}
				}
			}
		}
		for(int i=tot;i;--i){
			if(book[i]){
				for(int j=i+1;j<=tot;++j){
					a[j-1]=a[j];
				}
				--tot;
			}
		}
		//for(int i=1;i<=tot;++i){
		//	cout<<a[i]<<' ';
		//}cout<<endl;
		for(int i=1;i<=tot;++i){
			for(int j=i+1;j<=tot;++j){
				if(gcd(a[i],a[j])==1){
					int k=a[i]*a[j]-a[i]-a[j];
					while(k<a[tot] && tot>j) --tot;
				}
			}
		}
		if(tot<=2){
			printf("%d\n",tot);
			continue;
		}
		memset(check,0,sizeof(check));
		maxx=a[tot];
		ans=0;
		check[0]=1;
		for(int i=1;i<=tot;++i){
			if(check[a[i]]==0){
				++ans;
				for(int j=maxx;j>=0;--j){
					if(check[j]){
						for(int k=1;k*a[i]+j<=maxx;++k){
							int p=k*a[i]+j;
							if(check[p]) break;
							check[p]=1;
						}
					}
				}
				while(check[maxx]){
					--tot;
					maxx=a[tot];
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
