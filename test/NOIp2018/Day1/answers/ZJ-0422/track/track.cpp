#include<iostream>
#include<cstdio>
#include<vector>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define maxn 50005
using namespace std;
int n,m;
int head[maxn],tot=0;
struct Edge{
	int t,nxt,w;
}edge[maxn<<1];
int book[maxn];
int sum=0;
int cnt;
int ans;
int flag;
void add(int st,int to,int we){
	edge[tot].t=to;
	edge[tot].nxt=head[st];
	head[st]=tot;
	edge[tot].w=we;
	++tot;
	return;
}
int dfs(int np){
	vector<int> x,y;
	int num=0;
	book[np]=1;
	for(int i=head[np];i!=-1;i=edge[i].nxt){
		int nt=edge[i].t;
		if(!book[nt]){
			int k=dfs(nt)+edge[i].w;
			if(k>=flag){
				++cnt;
			}
			else{
				x.push_back(k);
				y.push_back(0);
				++num;
			}
		}
	}
	sort(x.begin(),x.begin()+num);
	for(int i=0;i<num;++i){
		if(!y[i]){
			for(int j=i+1;j<num;++j){
				if(!y[j] && x[i]+x[j]>=flag){
					++cnt;
					y[i]=1;
					y[j]=1;
					break;
				}
			}
		}	
	}
	int rest=0;
	for(int i=num-1;i>=0;--i){
		if(!y[i]){
			rest=x[i];
			break;
		}
	}
	return rest;
}
int check(int keyy){
	cnt=0;
	flag=keyy;
	memset(book,0,sizeof(book));
	dfs(0);
	if(cnt>=m){
		return 1;
	}
	return 0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d %d",&n,&m);
	memset(head,-1,sizeof(head));
	for(int i=1;i<n;++i){
		int a,b,c;
		scanf("%d %d %d",&a,&b,&c);
		sum+=c;
		add(a,b,c);
		add(b,a,c);
	}
	add(0,1,0);
	add(1,0,0);
	int l=1,r=sum;
	while(l<=r){
		int m=(l+r)>>1;
		if(check(m)){
			l=m+1;
			ans=m;
		}
		else{
			r=m-1;
		}
	}
	printf("%d\n",ans);
	return 0;
}
