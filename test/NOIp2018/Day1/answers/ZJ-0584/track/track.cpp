#include<cstdio>
#include<cstring>
struct line
{
	int u;
	int v;
	int w;
	int unext;
	int vnext;
}li[50010];
int inf=999999999;
int first[50010],cut=0;
int n,m;
bool used[50010];
int yq[50010];
void ad(int x,int y,int z)
{
	cut++;
	used[cut]=false;
	li[cut].u=x;
	li[cut].v=y;
	li[cut].w=z;
	li[cut].unext=first[x];
	first[x]=cut;
	li[cut].vnext=first[y];
	first[y]=cut;
	return;
}
int dfs(int u)
{
	int k,ma=0,t,zp;
	if(yq[u]!=0)
		return yq[u];
	for(k=first[u];k!=-1;)
	{
		if(li[k].u==u)
			t=li[k].v;
		else
			t=li[k].u;
		if(used[k]==false)
		{
			used[k]=true;
			zp=dfs(t)+li[k].w;
			used[k]=false;
			if(zp>ma)
				ma=zp;
		}
		if(li[k].u==u)
			k=li[k].unext;
		else
			k=li[k].vnext;
	}
	yq[u]=ma;
	return ma;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int i,j,x,y,z,zc,ma=0;
	int maa=0;
	memset(first,-1,sizeof(first));
	memset(yq,0,sizeof(yq));
	scanf("%d%d",&n,&m);
	for(i=1;i<=n-1;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		ad(x,y,z);
	}
	if(m==1)
	{
		for(i=1;i<=n-1;i++)
		{
			for(j=1;j<=n-1;j++)
				yq[j]=0;
			used[i]=true;
			zc=dfs(li[i].u)+dfs(li[i].v)+li[i].w;
			used[i]=false;
			if(zc>ma)
				ma=zc;
		}
		printf("%d",ma);
	}
	return 0;
}
