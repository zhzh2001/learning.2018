#include<bits/stdc++.h>
using namespace std;
int t,n,ans,a[101];
bool f[100001];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		if (a[1]==1){
			printf("1\n");
			continue;
		}
		memset(f,0,sizeof(f));
		f[0]=1;
		ans=0;
		for(int i=1;i<=n;i++)
			if (!f[a[i]]){
				ans++;
				for(int j=a[i];j<=a[n];j++)f[j]|=f[j-a[i]];
			}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
