#include<bits/stdc++.h>
using namespace std;
struct ji{
	int nex,to,len;
}edge[100001];
int E,n,m,x,y,z,ans,l,r,mid,s[50001],head[50001],f[50001],g[50001];
void add(int x,int y,int z){
	edge[E].nex=head[x];
	edge[E].to=y;
	edge[E].len=z;
	head[x]=E++;
}
int calc(int fa){
	int ans=0;
	for(int i=1;i<=fa;i++){
		if (!s[i])i++;
		if (!s[fa])fa--;
		if ((i<fa)&&(s[i]+s[fa]>=z)){
			ans++;
			fa--;
		}
	}
	return ans;
}
void dp(int k,int fa){
	f[k]=g[k]=0;
	for(int i=head[k];i!=-1;i=edge[i].nex)
		if (edge[i].to!=fa){
			dp(edge[i].to,k);
			f[k]+=f[edge[i].to];
		}
	s[0]=0;
	for(int i=head[k];i!=-1;i=edge[i].nex)
		if (edge[i].to!=fa)s[++s[0]]=g[edge[i].to]+edge[i].len;
	sort(s+1,s+s[0]+1);
	for(fa=s[0];(fa)&&(s[fa]>=z);fa--);
	f[k]+=s[0]-fa;
	s[0]=0;
	f[k]+=(ans=calc(fa));
	l=0;
	r=fa;
	while (l<r){
		mid=((l+r+1)>>1);
		swap(s[0],s[mid]);
		if (calc(fa)==ans)l=mid;
		else r=mid-1;
		swap(s[0],s[mid]);
	}
	g[k]=s[l];
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(head,-1,sizeof(head));
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z);
		add(y,x,z);
	}
	x=0;
	y=500000000;
	while (x<y){
		z=((x+y+1)>>1);
		dp(1,0);
		if (f[1]>=m)x=z;
		else y=z-1;
	}
	printf("%d",x);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
