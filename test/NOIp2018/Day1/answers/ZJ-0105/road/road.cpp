#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
const int maxn=100005;
int n,ans,a[maxn];
inline int read(){
	int x=0;bool f=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-')f=0;ch=getchar();}
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-48,ch=getchar();
	if (f) return x;return -x;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (int i=1;i<=n;++i) a[i]=read();a[n+1]=0;
	for (int i=2;i<=n+1;++i) ans+=max(0,a[i-1]-a[i]);
	printf("%d\n",ans);
	return 0;
}
