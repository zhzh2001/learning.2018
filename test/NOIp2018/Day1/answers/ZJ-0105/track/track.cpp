#include<cstdio>
#include<cctype>
#include<vector>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=50005,maxe=100005,maxm=1005;
int n,m,tot,ans,an,mid,mi=2e9,c[maxn],lk[maxn],son[maxe],nt[maxe],w[maxe];
bool fg=1,f[maxn];
inline int read(){
	int x=0;bool f=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-')f=0;ch=getchar();}
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-48,ch=getchar();
	if (f) return x;return -x;
}
void add(int x,int y,int z){nt[++tot]=lk[x];son[lk[x]=tot]=y;w[tot]=z;}
int dfs(int x,int y){
	int b[maxm];bool vis[maxm];
	memset(b,0,sizeof(b));
	memset(vis,0,sizeof(vis));
	for (int j=lk[x];j;j=nt[j])
	if (son[j]!=y) b[++b[0]]=(dfs(son[j],x)+w[j]);
	sort(b+1,b+1+b[0]);
	for (int i=1;i<=b[0];++i)if(!vis[i]){
		if (b[i]>=mid){vis[i]=1;++an;continue;}
		for (int j=i+1;j<=b[0];++j)if (!vis[j]&&b[i]+b[j]>=mid&&b[j]<mid){vis[i]=vis[j]=1;++an;break;}
	}
	for (int i=b[0];i;--i) if (!vis[i])return b[i];
	return 0;
}
bool check(int x){
	an=0;if (fg){
		memset(f,0,sizeof(f));int t=0;
		for (int i=1;i<n;++i)if(!f[i]){
			if (c[i]>=mid){f[i]=1;++an;continue;}
			for (int j=i+1;j<n;++j)if(!f[j]&&c[i]+c[j]>=mid&&c[j]<mid){f[i]=f[j]=1;++an;break;}
		}
		for (int i=n-1;i;--i)if(!f[i]){t=c[i];break;}
		return (t>=mid)+an>=m;
	}
	return (dfs(1,0)>=mid)+an>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<n;++i){
		int x=read(),y=read(),z=read();
		add(x,y,z);add(y,x,z);mi=min(mi,z);
		if (x>1) fg=0;
	}
	if (m==n-1) {printf("%d\n",mi);return 0;}
	if (fg){for(int i=1;i<n;++i)c[i]=w[lk[i+1]];sort(c+1,c+n);}
	int l=0,r=2e9;while (l<=r)
	if (check(mid=(l+r)>>1))l=mid+1;else r=mid-1;
	printf("%d\n",l-1);return 0;
}
