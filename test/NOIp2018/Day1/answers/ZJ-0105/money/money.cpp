#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=105,maxm=25005;
int T,n,ans,a[maxn],f[maxm];
inline int read(){
	int x=0;bool f=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-')f=0;ch=getchar();}
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-48,ch=getchar();
	if (f) return x;return -x;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (T=read();T--;){
		memset(f,0,sizeof(f));
		n=read();f[ans=0]=1;
		for (int i=1;i<=n;++i) a[i]=read();
		sort(a+1,a+1+n);
		for (int i=1;i<=n;++i)if(!f[a[i]]){
			++ans;
			for (int j=0,r=a[n]-a[i];j<=r;++j)if(f[j])f[j+a[i]]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
