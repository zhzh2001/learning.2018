#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
int n,m;
int a,b,c;
int ans=0;
const int qw=100010;
int nxt[qw],adj[qw],go[qw],fee[qw];
int ecnt=1;//^
bool bo1,bo2;
void add(int u,int v,int w){
	nxt[++ecnt]=adj[u];adj[u]=ecnt;go[ecnt]=v;fee[ecnt]=w;
	nxt[++ecnt]=adj[v];adj[v]=ecnt;go[ecnt]=u;fee[ecnt]=w;
}
int s;//source
int dep[qw];
bool vst[qw];
int mx_dep,mx_num;

void dfs1(int u){
	vst[u]=1;
	int v;
	for(int e=adj[u];e;e=nxt[e]){
		v=go[e];
		if(vst[v]==1)continue;
		dep[v]=dep[u]+fee[e];
		if(dep[v]>mx_dep){
			mx_dep=dep[v];mx_num=v;
		}
		dfs1(v);
	}
}

int eg[qw];
int jh[qw];
int sum=0;
bool cmp(int x,int y){
	return x>y;
}

int dis[qw],dis_n[qw];// len_bian
int mn=10000005;
int l_ans,r_ans;
void init_2(){
	int u,v;
	mn=10000005;
	for(int i=1;i<n;i++){
		u=i;
		for(int e=adj[u];e;e=nxt[e]){
			v=go[e];
			if(v==u+1){
				dis_n[i]=fee[e];
				mn=min(dis_n[i],mn);
			}
		}
	}
	dis[0]=0;
	for(int i=1;i<n;i++){
		dis[i]=dis[i-1]+dis_n[i];
	}
}
bool pd(int targ){
	int num=0;
	int nw=0;
	for(int i=1;i<n;i++){
		nw=nw+dis_n[i];
		if(nw>=targ){
			num++;
			nw=0;
		}
	}
	if(num>=m)return true;
	return false;
}

int mx=0;
int ans_num=0;
int fat[qw],l_fat[qw];
int ls[qw],rs[qw];

void dfs(int u,int targ){
	int v;
	for(int e=adj[u];e;e=nxt[e]){
		v=go[e];
		if(v==fat[u])continue;
		fat[v]=u;l_fat[v]=fee[e];
		if(ls[u]==0){
			ls[u]=v;
		}
		else if(ls[u]!=0&&rs[u]==0){
			rs[u]=v;
		}
		dfs(v,targ);
	}
	if(ls[u]==0&&rs[u]==0)return;
	if(ls[u]!=0&&l_fat[ls[u]]>=targ){
		ans_num++;ls[u]=0;
	}
	if(rs[u]!=0&&l_fat[rs[u]]>=targ){
		ans_num++;rs[u]=0;
	}
	if(ls[u]!=0&&rs[u]!=0&&l_fat[ls[u]]+l_fat[rs[u]]>=targ){
		ans_num++;ls[u]=0;rs[u]=0;
	}
	if((ls[u]!=0||rs[u]!=0)&&(l_fat[ls[u]]+l_fat[rs[u]]<targ)){
		l_fat[u]+=max(l_fat[ls[u]],l_fat[rs[u]]);
	}
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bo1=true;bo2=true;
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&a,&b,&c);
		add(a,b,c);
		if(a!=1)bo1=false;
		if(b!=a+1&&a!=b+1)bo2=false;
		eg[i]=c;
		mn=min(mn,c);
		mx+=c;
	}
	if(m==1){
		s=1;mx_dep=0;mx_num=1;
		for(int i=1;i<=n;i++){dep[i]=0;vst[i]=false;}
		dfs1(s);
		s=mx_num;
		for(int i=1;i<=n;i++){dep[i]=0;vst[i]=false;}
		mx_dep=0;
		dfs1(s);
		ans=mx_dep;
	}
	else if(bo1){
		sort(eg+1,eg+n,cmp);// da -> xiao
		for(int i=1;i<=m;i++){
			jh[i]=eg[i];
		}
		sum=m;
		for(int i=m;i>=1;i--){
			sum++;
			if(sum>n-1)break;
			jh[i]+=eg[sum];
		}
		ans=jh[1];
		for(int i=2;i<=m;i++){
			ans=min(jh[i],ans);
		}
	}
	else if(bo2){//divide xian deal with dis
		init_2();
		l_ans=mn;r_ans=dis[n-1];
		int mid;
		while(l_ans<r_ans){
			mid=(l_ans+r_ans+1)>>1;
			if(pd(mid)){
				l_ans=mid;
			}
			else r_ans=mid-1;
		}
		ans=l_ans;
	}
	else if(m==n-1){
		ans=mn;
	}
	else{
		l_ans=mn;r_ans=mx;
		int mid;
		while(l_ans<r_ans){
			mid=(l_ans+r_ans+1)>>1;
			ans_num=0;
			dfs(1,mid);
			if(ans_num>=m)l_ans=mid;else r_ans=mid-1;
		}
		ans=l_ans;
	}
	printf("%d",ans);
	return 0;
}
