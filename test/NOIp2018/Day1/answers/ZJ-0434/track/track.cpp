#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdio>
#include<cmath>
using namespace std;
int q,w,e,r,t,y,u,i,o,p,j,k,ans,a[5001][5001],c[5001][5001],b[50001],s1[50001],s2[50001],s3[50001];
bool f[5001][5001],ff3,ff2=true;
void dp33(int x,int y,int z)
{
	int i;
	if(x==w-1)
	{
		ans=max(ans,min(z,s3[q-1]-s3[y]));
	} 
	for(i=y+1;i<=q-2;i++) 
	{
		dp33(x+1,i,min(z,(s3[i]-s3[y])));
		//cout<<min(z,(s3[i]-s3[y]))<<" "<<z<<" "<<s3[i]-s3[y]<<endl;
	}
	
}

void ddffss(int p,int y)
{
		int i;
		ans=ans>y?ans:y;
		for(i=1;i<=b[p];i++)
		if(f[p][c[p][i]])
		{
			f[p][c[p][i]]=false;
			f[c[p][i]][p]=false;
			//cout<<y+a[p][c[p][i]]<<endl;	
			ddffss(c[p][i],y+a[p][c[p][i]]);		
			f[p][c[p][i]]=true;	
			f[c[p][i]][p]=true;	
		}
		
}

void dfs(int x,int y,int z,int p)
{
	if(x==w)
	{
		//cout<<ans<<endl;
		ans=ans>z?ans:z;
	} 	
	else if (y<z||x==0) 
	{
		int i;
		for(i=0;i<=b[p];i++)
		if(f[p][c[p][i]])
		{
			f[p][c[p][i]]=false;
			f[c[p][i]][p]=false;	
			//cout<<x<<" "<<p<<" "<<y+a[p][c[p][i]]<<" "<<z<<" "<<c[p][i]<<" "<<endl;
			dfs(x,y+a[p][c[p][i]],z,c[p][i]);	
				
			if (x+1<w)	
			{
				int j;		
				for(j=1;j<=q;j++)
				dfs(x+1,0,y+a[p][c[p][i]],j);	
			}
			else dfs(x+1,0,y+a[p][c[p][i]],1);
			f[p][c[p][i]]=true;	
			f[c[p][i]][p]=true;	
		}
		

	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>q>>w;
	ff3=ff2=true;
	if(q<5000)
	for(i=1;i<=q-1;i++)
	{
		cin>>e>>r>>t;
		s1[e]=t;
		if(e!=1) ff3=false;
		s2[i]=t;
		a[e][r]=t;
		a[r][e]=t;
		c[e][++b[e]]=r;
		c[r][++b[r]]=e;
		f[e][r]=true;
		f[r][e]=true;
	}
	else
	for(i=1;i<=q-1;i++)
		{
		cin>>e>>r>>t;
		s1[e]=t;
		if(e!=1) ff3=false;
		s2[i]=t;
		b[e]++;
		b[r]++;
	}
	if (ff3) 
	{
		sort(s2+1,s2+q);
		cout<<s2[q-w]<<endl;
		return 0;
	}
	ans=0;
	for(i=1;i<=q-1;i++) s3[i]=s3[i-1]+s2[i];
	for(i=1;i<=q-1;i++) if(b[i]>2) ff2=false;
	if(ff2) dp33(0,0,s3[q-1]);
	//cout<<ff2<<endl;
	if(ans>0)
	{
		cout<<ans;
		return 0;
	}
	if (w>1)
	for(j=1;j<=q;j++)
	dfs(0,0,0,j);
	else
	for(j=1;j<=q;j++) 
	ddffss(j,0);	
	cout<<ans<<endl;
	return 0;
}
