#include <cstdio>
#include <cstring>
#include <algorithm>

void read(int &t) {
  char c = getchar();
  for (; c < '0' || c > '9'; c = getchar())
    ;
  for (t = 0; c >= '0' && c <= '9'; t = t * 10 + c - '0', c = getchar())
    ;
}

int const N = 105, A = 25005;

int a[N], f[A];

int main() {
#ifndef DEBUG
  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
#endif
  int T;
  read(T);
  for (; T--;) {
    int n, ans = 0;
    read(n);
    for (int i = 0; i < n; ++i)
      read(a[i]);
    std::sort(a, a + n);
    memset(f, 0, sizeof f), f[0] = 1;
    for (int i = 0; i < n; ++i) {
      if (f[a[i]])
        continue;
      for (int j = a[i]; j < A; ++j)
        if (f[j - a[i]])
          f[j] = 1;
      ++ans;
    }
    printf("%d\n", ans);
  }
  return 0;
}

