#include <cstdio>
#include <cstring>
#include <algorithm>

void read(int &t) {
  char c = getchar();
  for (; c < '0' || c > '9'; c = getchar())
    ;
  for (t = 0; c >= '0' && c <= '9'; t = t * 10 + c - '0', c = getchar())
    ;
}

int const N = 100005;

int d[N];

int main() {
#ifndef DEBUG
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
#endif
  int n, ans = 0;
  read(n);
  for (int i = 1; i <= n; ++i)
    read(d[i]);
  for (int i = 0; i <= n; ++i)
    if (d[i + 1] > d[i])
      ans += d[i + 1] - d[i];
  printf("%d\n", ans);
  return 0;
}

