#include <cstdio>
#include <cstring>
#include <algorithm>

void read(int &t) {
  char c = getchar();
  for (; c < '0' || c > '9'; c = getchar())
    ;
  for (t = 0; c >= '0' && c <= '9'; t = t * 10 + c - '0', c = getchar())
    ;
}

int const N = 50005;

struct Edge {
  int v, w, nxt;
} e[N << 1];

int n, m, ne, h[N];

void add_edge(int u, int v, int w) {
  e[++ne] = (Edge){v, w, h[u]}, h[u] = ne;
  e[++ne] = (Edge){u, w, h[v]}, h[v] = ne;
}

int cnt, top, sta[N], used[N], pre[N], nxt[N];

void remove(int t) {
  pre[nxt[t]] = pre[t], nxt[pre[t]] = nxt[t];
}

void solve(int *a, int *b, int n, int l) {
  for (int i = 0; i <= n + 1; ++i)
    pre[i] = i - 1, nxt[i] = i + 1;
  pre[0] = 0, nxt[n + 1] = n + 1;
  int j = n + 1;
  for (int i = 1; i <= n; i = nxt[i], j = nxt[j]) {
    if (b[i])
      continue;
    for (; pre[j] > i && a[i] + a[pre[j]] >= l; j = pre[j])
      ;
    if (j <= n)
      ++cnt, b[i] = b[j] = 1, remove(i), remove(j);
  }
}

int dfs(int t, int fa, int l) {
  int rec = top;
  for (int i = h[t]; i; i = e[i].nxt)
    if (e[i].v != fa)
      sta[top] = dfs(e[i].v, t, l) + e[i].w, used[top++] = 0;
  std::sort(sta + rec, sta + top);
  int p = rec, q = top - 1;
  for (; q >= p && sta[q] >= l;)
    ++cnt, --q;
  solve(sta + p - 1, used + p - 1, q - p + 1, l);
  top = rec;
  for (; q >= rec; --q)
    if (!used[q])
      return sta[q];
  return 0;
}

int check(int l) {
  cnt = 0, dfs(1, 0, l);
  return cnt >= m;
}

int main() {
#ifndef DEBUG
  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
#endif
  read(n), read(m);
  for (int i = 1; i < n; ++i) {
    int u, v, w;
    read(u), read(v), read(w);
    add_edge(u, v, w);
  }
  int L = 1, R = 500000001;
  for (; L < R;) {
    int MID = L + R >> 1;
    if (check(MID))
      L = MID + 1;
    else
      R = MID;
  }
  printf("%d\n", L - 1);
  return 0;
}

