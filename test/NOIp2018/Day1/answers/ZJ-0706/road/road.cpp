#include<bits/stdc++.h>
#define M 100005
using namespace std;
bool cur1;
int a[M],n;
struct p70 {
	int ans;
	void solve() {
		ans=0;
		bool ok;
		while(!ok) {
			ok=1;
			for(int i=1; i<=n; i++) {
				if(a[i])ok=0;
				if(a[i]&&!a[i-1])ans++;
			}
			for(int i=1; i<=n; i++)if(a[i])a[i]--;
		}
		printf("%d\n",ans);
	}
} p70;
bool cmp(int x,int y) {
	return a[x]<a[y];
}
struct p100 {
	int p[M],L[M],R[M],ans;
	void solve() {
		ans=0;
		for(int i=1; i<=n; i++) {
			p[i]=i;
			L[i]=i-1;
			R[i]=i+1;
		}
		sort(p+1,p+n+1,cmp);
		int res=1;
		for(int i=0,j=0; i<=10000; i++) {
			while(j<n&&a[p[j+1]]<=i) {
				j++;
				int s=(L[p[j]]==p[j]-1&&p[j]-1!=0),t=(R[p[j]]==p[j]+1&&p[j]+1!=n+1);
				if(s+t==2)res++;
				if(s+t==0)res--;
				R[L[p[j]]]=R[p[j]];
				L[R[p[j]]]=L[p[j]];
			}
			ans+=res;
		}
		printf("%d\n",ans);
	}
}p100;
bool cur2;
int main() {
//	printf("%lfM\n",(&cur2-&cur1)/1024.0/1024.0);
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1; i<=n; i++)scanf("%d",&a[i]);
	if(n<=1000)p70.solve();
	else p100.solve();
	return 0;
}
