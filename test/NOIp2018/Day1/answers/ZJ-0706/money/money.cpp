#include<bits/stdc++.h>
#define M 105
using namespace std;
bool cur1;
int n,a[M];
struct p10 {
	bool s[1000005],t[1000005];
	void solve() {
		memset(s,0,sizeof(s));
		s[0]=1;
		for(int j=1; j<=n; j++) {
			for(int k=a[j]; k<=1000000; k++)if(s[k-a[j]])s[k]=1;
		}
		int ans=1e9;
		for(int i=1; i<(1<<n); i++) {
			int cnt=0;
			memset(t,0,sizeof(t));
			t[0]=1;
			for(int j=1; j<=n; j++) {
				if(!(i&(1<<(j-1))))continue;
				cnt++;
				for(int k=a[j]; k<=1000000; k++)if(t[k-a[j]])t[k]=1;
			}
			if(memcmp(s,t,sizeof(s))==0)ans=min(ans,cnt);
		}
		printf("%d\n",ans);
	}
} p10;
struct p80 {
	bool s[25005];
	void solve() {
		int ans=0;
		memset(s,0,sizeof(s));
		int mx=a[n];
		s[0]=1;
		for(int i=1; i<=n; i++) {
			if(!s[a[i]]) {
				ans++;
				for(int j=a[i]; j<=mx; j++)if(s[j-a[i]])s[j]=1;
			}
		}
		printf("%d\n",ans);
	}
}p80;
bool cur2;
int main() {
//	printf("%lfM\n",(&cur2-&cur1)/1024.0/1024.0);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--) {
		scanf("%d",&n);
		for(int i=1; i<=n; i++)scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		if(n==2)p10.solve();
		else p80.solve();
	}
	return 0;
}
