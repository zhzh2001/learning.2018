#include<bits/stdc++.h>
#define M 50005
using namespace std;
bool cur1;
int head[M],to[2*M],nxt[2*M],val[2*M];
int cnt,n,m;
void Add(int a,int b,int c) {
	to[++cnt]=b;
	val[cnt]=c;
	nxt[cnt]=head[a];
	head[a]=cnt;
}
struct p20 {
	int dis[M],f[M],mx;
	void dfs(int u) {
		if(dis[u]>dis[mx])mx=u;
		for(int i=head[u]; ~i; i=nxt[i]) {
			int v=to[i];
			if(f[u]==v)continue;
			f[v]=u;
			dis[v]=dis[u]+val[i];
			dfs(v);
		}
	}
	void solve() {
		mx=1,dis[1]=0;
		dfs(1);
		f[mx]=-1,dis[mx]=0;
		dfs(mx);
		printf("%d\n",dis[mx]);
	}
} p20;
struct pl {
	int d[M],g[M];
	bool check(int x) {
		memset(g,0,sizeof(g));
		for(int i=1,j=1; i<=n; i++) {
			while(d[i]-d[j+1]>=x)j++;
			if(d[i]-d[j]>=x)g[i]=g[j]+1;
		}
		return g[n]>=m;
	}
	void solve() {
		int tot=1;
		for(int i=1; i<=n; i++)
			for(int j=head[i]; ~j; j=nxt[j])if(j&1)d[++tot]=val[j];
		for(int i=1; i<=n; i++)d[i]+=d[i-1];
		int l=0,r=d[n];
		while(l<=r) {
			int mid=(l+r)>>1;
			if(check(mid))l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",l-1);
	}
} pl;
struct pj {
	int d[M],tot;
	bool check(int x) {
		int res=0;
		for(int i=tot,j=1; i>=1; i--) {
			if(d[i]>=x)res++;
			else {
				bool ok=0;
				while(j<i) {
					if(d[i]+d[j]>=x) {
						j++;
						ok=1;
						res++;
						break;
					}
					j++;
				}
				if(!ok)break;
			}
		}
		return res>=m;
	}
	void solve() {
		tot=0;
		int sum=0;
		for(int i=head[1]; ~i; i=nxt[i])d[++tot]=val[i],sum+=val[i];
		sort(d+1,d+tot+1);
		int l=1,r=sum;
		while(l<=r) {
			int mid=(l+r)>>1;
			if(check(mid))l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",l-1);
	}
}pj;
bool cur2;
int main() {
//	printf("%lfM\n",(&cur2-&cur1)/1024.0/1024.0);
	memset(head,-1,sizeof(head));
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool ok=1;
	for(int i=1; i<n; i++) {
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		Add(a,b,c);
		Add(b,a,c);
		if(a+1!=b)ok=0;
	}
	if(m==1)p20.solve();
	else if(ok)pl.solve();
	else pj.solve();
	return 0;
}
