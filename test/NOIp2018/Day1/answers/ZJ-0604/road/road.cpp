#include <cstdio>
#include <algorithm>

using namespace std;

const int N = 100005;

int n, ans;
int a[N];

int main() {
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) {
    scanf("%d", &a[i]);
    if (a[i] > a[i - 1]) {
      ans += a[i] - a[i - 1];
    }
  }
  printf("%d\n", ans);
  return 0;
}
