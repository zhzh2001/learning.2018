#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

const int N = 50005;
const int INF = (int)1e9 + 7;

int n, m, T;
int ans, lim;

int yu, la[N], to[N << 1], pr[N << 1], wi[N << 1];
inline void Ade(int a, int b, int c) {
  to[++yu] = b, wi[yu] = c, pr[yu] = la[a], la[a] = yu;
}

int Dfs(int x, int fat) {
  vector<int> so;
  for (int i = la[x]; i; i = pr[i]) {
    if (to[i] != fat) {
      so.push_back(wi[i] + Dfs(to[i], x));
    }
  }
  sort(so.begin(), so.end());
  while (!so.empty() && so.back() >= lim) {
    ++ans;
    so.pop_back();
  }
  if (so.empty()) {
    return 0;
  }
  static bool fl[N];
  fill(fl, fl + so.size(), 0);
  int i = 0, j = so.size() - 1;
  while (true) {
    while (i < j && so[i] + so[j] < lim) ++i;
    if (i != j) {
      ++ans;
      fl[i] = fl[j] = 1;
      ++i, --j;
      if (i > j) break;
    } else {
      break;
    }
  }
  int mx = 0;
  for (int i = 0; i < (int)so.size(); ++i)
    if (!fl[i]) mx = so[i];
  return mx;
}

bool Check(int md) {
  ans = 0, lim = md;
  Dfs(1, 0);
  if (ans >= m) return 1;
  for (int i = 1; i < T; ++i) {
    ans = 0, lim = md;
    int rt = rand() % n + 1;
    Dfs(rt, 0);
    if (ans >= m) return 1;
  }
  return 0;
}

int main() {
  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
  srand(517);
  scanf("%d%d", &n, &m);
  int sub0 = 1, sub1 = 1, sub2 = 1;
  sub0 &= (m == 1);
  for (int i = 1, x, y, z; i < n; ++i) {
    scanf("%d%d%d", &x, &y, &z);
    Ade(x, y, z), Ade(y, x, z);
    sub1 &= (x == 1);
    sub2 &= (y == x + 1);
  }
  if (sub0 | sub1 | sub2) {
    T = 5000000 / n / 450 + 1;
  } else {
    T = 27000000 / n / 450;
  }
  //cerr << "tim : " << T << endl;
  int lo = 0, hi = INF;
  for (int md; lo < hi; ) {
    md = (lo + hi + 1) >> 1;
    if (Check(md)) {
      lo = md;
    } else {
      hi = md - 1;
    }
  }
  printf("%d\n", lo);
  return 0;
}
