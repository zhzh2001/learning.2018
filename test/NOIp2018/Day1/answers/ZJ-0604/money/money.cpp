#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int N = 105;
const int M = 25005;

int tc, n;
int a[N];
bool f[M];

int main() {
  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
  scanf("%d", &tc);
  for (; tc--; ) {
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i) {
      scanf("%d", &a[i]);
    }
    sort(a + 1, a + 1 + n);
    memset(f, 0, sizeof f);
    f[0] = 1;
    int sub = 0;
    for (int i = 1; i <= n; ++i) {
      if (f[a[i]]) {
        ++sub;
        continue;
      }
      for (int j = 0; j + a[i] < M; ++j) {
        if (f[j]) f[j + a[i]] = 1;
      }
    }
    printf("%d\n", n - sub);
  }
  return 0;
}
