#include <cstdio>
#include <algorithm>
using namespace std;
const int N=105,M=25005;
int t,n,a[N],cnt,f[N],v[M],flag;
int main(){
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  scanf("%d",&t);
  while (t--){
  	scanf("%d",&n);
  	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
  	sort(a+1,a+n+1);
  	cnt=0;
  	for (int i=1;i<=n;i++)
  	   if (i==1) f[++cnt]=a[i];
  	   else
  	     {
  	      v[0]=1;flag=0;
  	      for (int j=1;j<=a[i];j++) v[j]=0;
  	      for (int j=1;j<=cnt;j++){
  	        if (flag) break;
			for (int k=f[j];k<=a[i];k++)
  	          if (v[k-f[j]]){
  	            v[k]=1;
  	            if (k==a[i]){
  	               flag=1;
  	               break;
				  }
  	          }
		   }
		   if (!flag) f[++cnt]=a[i];
         }
     printf("%d\n",cnt);
   }
  return 0;
}
