#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=50005;
int n,m,x[N],y[N],z[N],head[N],cnt,tot,ans,flag,flag2,c[N],temp,sum[N];
int q[N],dep[N],dis[N],f[N][17],w,b[N],fl,ff[N],ffff;
struct edge{
  int to,nex,val,f;
}e[N*2];
void add(int x,int y,int z){
  e[cnt].to=y;e[cnt].nex=head[x];e[cnt].val=z;head[x]=cnt++;
  e[cnt].to=x;e[cnt].nex=head[y];e[cnt].val=z;head[y]=cnt++;
}
void dfs(int x){
  if (x==m){
    int tt=tot;
  	for (int i=1;i<m;i++) 
	  tt=min(tt,sum[q[i]]-sum[q[i-1]]);
	tt=min(tt,sum[n-1]-sum[q[m-1]]);
	if (tt>ans) ans=tt;
	return; 
  }
  for (int i=q[x-1]+1;i<n-1;i++)
    {
     q[x]=i;
     dfs(x+1);
	}
}
void Dfs(int x,int fa){
  dep[x]=dep[fa]+1;f[x][0]=fa;
  for (int i=head[x],v;~i;i=e[i].nex)
    if ((v=e[i].to)!=fa)
      ff[v]=i^1,dis[v]=dis[x]+e[i].val,Dfs(v,x);
}
int lca(int x,int y){
  if (dep[x]<dep[y]) swap(x,y);
  for (int i=15;i>=0;i--)
    if (dep[f[x][0]]>=dep[y])
      x=f[x][0];
  if (x==y) return x;
  for (int i=15;i>=0;i--)
    if (f[x][i]!=f[y][i])
      x=f[x][i],y=f[y][i];
  return f[x][0];
}
void ddfs(int x,int mi){
  if (x>m){
    if (mi>ans) ans=mi;
    return;
  }
  for (int i=1;i<=n;i++)
    if (!b[i]){
      b[i]=1;
      ddfs(x+1,min(mi,z[i]));
	  b[i]=0;
	}
  for (int i=1;i<n;i++)
    for (int j=i+1;j<=n;j++)
      if (!b[i]&&!b[j]){
      	b[i]=b[j]=1;
      	ddfs(x+1,min(mi,z[i]+z[j]));
	    b[i]=b[j]=0;
	  }
}
void df(int x,int mi){
  if (x>m){
  	ans=mi;
  	return;
  }
  for (int i=1;i<n;i++)
    for (int j=i+1;j<=n;j++)
      {
       w=lca(i,j);
       int ttt=i,rr=0,fff=0,xc=0,qq[35];
       while (ttt!=w)
	     {
		   if(e[ff[ttt]].f)	fff=1;
		   e[ff[ttt]].f=e[ff[ttt]^1].f=1;
		   rr+=e[ff[ttt]].val;qq[++xc]=ff[ttt];ttt=f[ttt][0];
	     }
	   ttt=j;
	   while (ttt!=w)
	     {
	      if(e[ff[ttt]].f) fff=1;
		 e[ff[ttt]].f=e[ff[ttt]^1].f=1; 
		 rr+=e[ff[ttt]].val;qq[++xc]=ff[ttt];ttt=f[ttt][0];
	     }
	   if (!fff&&mi>ans) df(x+1,min(mi,rr));
	   for (int i=1;i<=xc;i++)
	     e[qq[i]].f=e[qq[i]^1].f=0;
	  }
}
int main(){
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  scanf("%d%d",&n,&m);
  memset(head,-1,sizeof(head));
  for (int i=1;i<n;i++) 
    {
	scanf("%d%d%d",&x[i],&y[i],&z[i]),add(x[i],y[i],z[i]);
	tot+=z[i];
    if (x[i]!=1) flag=1;
    if (y[i]!=x[i]+1) flag2=1;
	}
  if (!flag2){
    for (int i=1;i<n;i++) c[x[i]]=z[i];
    for (int i=1;i<n;i++) sum[i]=sum[i-1]+c[i];
	dfs(1);
	printf("%d\n",ans);
	return 0;
  }
  if (m==1){
    Dfs(1,0);
    for (int i=1;i<=15;i++)
      for (int j=1;j<=n;j++)
        f[j][i]=f[f[j][i-1]][i-1];
    for (int i=1;i<n;i++)
      for (int j=i+1;j<=n;j++)
        {
         w=lca(i,j);
         if (dis[i]+dis[j]-2*dis[w]>ans)
           ans=dis[i]+dis[j]-2*dis[w];
		}
    printf("%d\n",ans);
    return 0;
  }
  if (!flag){
  	ddfs(1,tot/m);
  	printf("%d\n",ans);
  	return 0;
  }
  Dfs(1,0);
    for (int i=1;i<=15;i++)
      for (int j=1;j<=n;j++)
        f[j][i]=f[f[j][i-1]][i-1];
  df(1,tot/m);
  printf("%d\n",ans);
  return 0;
}
