#include <cstdio>
#include <algorithm>
using namespace std;
typedef long long ll;
const int N=100005;
int n,a[N],b[N];
ll ans;
int main(){
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
  scanf("%d",&n);
  for (int i=1;i<=n;i++) scanf("%d",&a[i]);
  for (int i=1;i<=n;i++) b[i]=a[i]-a[i-1],ans+=max(b[i],0);
  printf("%lld\n",ans);
  return 0;
}
