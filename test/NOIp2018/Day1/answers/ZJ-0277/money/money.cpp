#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int T,n,ans,a[105];
bool f[25005];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		for (int i=1;i<=n;++i)	scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		ans=0;
		memset(f,0,sizeof(f));
		f[0]=1;
		for (int i=1;i<=n;++i)
		{
			if (!f[a[i]]) ++ans;
			else continue;
			for (int j=a[i];j<=a[n];++j)
				f[j]|=f[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
