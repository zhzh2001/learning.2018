#include <iostream>
#include <cstdio>
using namespace std;
const int N=50005;
struct node
{
	int x,y,z;
}edge[N];
int n,m;
namespace subtask1
{
	int last[N],to[N<<1],nxt[N<<1],val[N<<1],dp[N][2];
	int ans=0,l=0;
	void add(int x,int y,int z)
	{
		nxt[++l]=last[x];
		last[x]=l;
		to[l]=y;
		val[l]=z;
	}
	void modif(int s,int u)
	{
		if (s>dp[u][0])	dp[u][1]=dp[u][0],dp[u][0]=s;
		else if (s>dp[u][1])	dp[u][1]=s;
	}
	void dfs(int u,int f)
	{
		for (int x=last[u];x;x=nxt[x])
		{
			int v=to[x];
			if (v==f)	continue;
			dfs(v,u);
			modif(dp[v][0]+val[x],u);
		}
		ans=max(dp[u][0]+dp[u][1],ans);
	}
	void solve()
	{
		for (int i=1;i<=n;++i)
		{
			add(edge[i].x,edge[i].y,edge[i].z);
			add(edge[i].y,edge[i].x,edge[i].z);
		}
		dfs(1,0);
		printf("%d\n",ans);
	}
}
namespace subtask2
{
	int low,mid,high,c[N];
	bool check()
	{
		for (int i=1;i<n;++i)
			if (edge[i].y!=edge[i].x+1)	return 0;
		return 1;
	}
	bool TRY(int mid)
	{
		int sum=0,k=0;
		for (int i=1;i<n;++i)
		{
			sum+=c[i];
			if (sum>=mid)
			{
				sum=0;
				++k;
				if (k==m)	return 1;
			}
		}
		return 0;
	}
	void solve()
	{
		for (int i=1;i<n;++i)
			c[edge[i].x]=edge[i].z;
		low=1;high=500000000;
		while (low<high)
		{
			mid=low+high+1>>1;
			if (TRY(mid))	low=mid;
			else high=mid-1;
		}
		printf("%d\n",low);
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for  (int i=1;i<n;++i)
		scanf("%d%d%d",&edge[i].x,&edge[i].y,&edge[i].z);
	if (m==1)	subtask1::solve();
	else if (subtask2::check())	subtask2::solve();
	return 0;
}
