#include<bits/stdc++.h>
#define res register int
#define ll long long
#define inf 0x3f3f3f3f
#define N 50050
using namespace std;
int head[N*2],n,m,tot,dis[N],cnt,vis[N],tot1,minn;
bool allow;
bool allow1;
struct data{
	int to,nxt,val;
}edge[N*2];

inline void add(int x,int y,int z)
{
	edge[++tot].to=y;
	edge[tot].nxt=head[x];
	head[x]=tot;
	edge[tot].val=z;
}

void dfs(int root,int fa)
{
	for(res i=head[root];i;i=edge[i].nxt)
	{
		int y=edge[i].to;
		if(y==fa) continue;
		dis[y]=dis[root]+edge[i].val;
		dfs(y,root); 
	}
}

bool check(int root,int fa,int val,int k)
{
	for(res i=head[root];i;i=edge[i].nxt)
    {
    	bool ans;
    	int y=edge[i].to;
    	if(!vis[y])
    	{
    	  if(val+edge[i].val<k)
    	{
		  vis[y]=true;
    	  ans=check(y,root,val+edge[i].val,k);
	    }
    	else 
		{
          vis[y]=true;
		  cnt++;
		  ans=check(y,root,0,k);
        }
       }
	}
	if(root==n)
	{
		if(val==0)
		return true;
		else 
		return false;
	}
}

inline bool cmp(data x,data y)
{
	return x.val<y.val;
}

int main()
{
    freopen("track.in","r",stdin);
    freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	allow=true;
	allow1=true;
	minn=1e7+1;
    for(res i=1;i<=n-1;i++)
    {
    	int x,y,z;
    	scanf("%d%d%d",&x,&y,&z);
    	tot1+=z;
    	minn=min(minn,z);
    	if(y!=x+1) allow=false;
    	if(x!=1) allow1=false;
    	add(x,y,z);
    	add(y,x,z);
	}
	
	if(m==1)
    {
    	dfs(1,0);
    	int tmp=0,o=0;
    	for(res i=1;i<=n;i++)
    	if(dis[i]>o)
    	{
    		tmp=i;
    		o=dis[i];
		}
		memset(dis,0,sizeof(dis));
		dfs(tmp,tmp);
		int ans=0;
		for(res i=1;i<=n;i++)
		ans=max(ans,dis[i]);
		printf("%d\n",ans);
	}	
	
	else 
	if(allow)
	{
		int l=1;
		int r=tot1;
		int ans;
		while(l<=r)
		{
			memset(vis,0,sizeof(vis));
			int mid=(l+r)/2;
			cnt=0;
			if(check(1,0,0,mid)&&cnt>=m) 
			{
				ans=mid;
				l=mid+1;
			}
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
	
	else if(m==n-1)
	{
		printf("%d\n",minn);
		return 0;
	}
	return 0;
}
