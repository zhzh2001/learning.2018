#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cmath>
#include<cstring>
const int MAXN=1000010;
struct node{
  int ans;
  int pos;
  int lazy;
};
node tre[MAXN<<2];
int a[MAXN];
int n;
int queryans,querypos;

inline void pushup(int rt){
  if(tre[rt<<1].ans<tre[rt<<1|1].ans){
    tre[rt].ans=tre[rt<<1].ans;
    tre[rt].pos=tre[rt<<1].pos;
  }
  else{
    tre[rt].ans=tre[rt<<1|1].ans;
    tre[rt].pos=tre[rt<<1|1].pos;
  }
}

inline void build(int l,int r,int rt){
  if(l==r){
    tre[rt].ans=a[l];
    tre[rt].pos=l;
    tre[rt].lazy=0;
    return;
  }
  int mid=(l+r)>>1;
  build(l,mid,rt<<1);
  build(mid+1,r,rt<<1|1);
  pushup(rt);
}

inline void query(int L,int R,int l,int r,int rt){
  if(L<=l&&r<=R){
    if(tre[rt].ans<queryans){
      queryans=tre[rt].ans;
      querypos=tre[rt].pos;
    }
    return;
  }
  int mid=(l+r)>>1;
  if(L<=mid){
    query(L,R,l,mid,rt<<1);
  }
  if(R>mid){
    query(L,R,mid+1,r,rt<<1|1);
  }
  return;
}

int solve(int l,int r){
  int ret=0;
  queryans=99999999,querypos=99999999;
  query(l,r,1,n,1);
  int ccy=queryans;
  ret+=ccy;
  if(querypos-1>=l){
    ret+=solve(l,querypos-1);
    ret-=ccy;
  }
  if(querypos+1<=r){
    ret+=solve(querypos+1,r);
    ret-=ccy;
  }
  return ret;
}

int main(){
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
  scanf("%d",&n);
  for(int i=1;i<=n;i++){
    scanf("%d",&a[i]);
  }
  build(1,n,1);
  printf("%d\n",solve(1,n));
}
