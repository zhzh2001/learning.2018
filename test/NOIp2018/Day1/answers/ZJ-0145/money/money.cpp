#include<iostream>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<cstdio>
using namespace std;
const int MAXN=110;
const int MAXA=25010;
int t;
int n;
int ans,zuida;
int a[MAXN];
int biaoshi[MAXA];

void dfs(int now,int step){
  if(biaoshi[now]==1||now>zuida){
    return;
  }
  if(step>1){
    biaoshi[now]=1;
  }
  for(int i=1;i<=n;i++){
    dfs(a[i]+now,step+1);
  }
}

int main(){
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  scanf("%d",&t);
  while(t--){
    ans=0;
    zuida=-1;
    scanf("%d",&n);
    for(int i=1;i<=n;i++){
      scanf("%d",&a[i]);
      zuida=max(zuida,a[i]);
    }
    for(int i=1;i<=zuida;i++){
      biaoshi[i]=0;
    } 
    dfs(0,0);
    for(int i=1;i<=n;i++){
      if(biaoshi[a[i]]==0){
	ans++;
      }
    }
    printf("%d\n",ans);
  }
  return 0;
}
