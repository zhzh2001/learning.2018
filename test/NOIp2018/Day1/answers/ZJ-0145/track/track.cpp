#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<queue>
using namespace std;
const int MAXN=50010;
struct edge{
  int u,v,w,nex;
};
edge e[MAXN<<1];
int head[MAXN],cnt;
int n,m,ans=-1;
bool vis[MAXN];
int mp[210][210];
bool viss[210][210];
struct node{
  int id;
  int num;
};
node dis[MAXN];

bool cmp(edge x,edge y){
  return x.w>y.w;
}

inline void add(int u,int v,int w){
  e[++cnt].u=u;
  e[cnt].v=v;
  e[cnt].w=w;
  e[cnt].nex=head[u];
  head[u]=cnt;
}

void dfs(int now,int lenth){
  ans=max(ans,lenth);
  for(int i=head[now];i;i=e[i].nex){
    if(!vis[e[i].v]){
      vis[e[i].v]=true;
      dfs(e[i].v,lenth+e[i].w);
      vis[e[i].v]=false;
    }
  }
  return;
}

void dfs1(int now,int lenth,int which,int zuixiao){
  if(which==m){
    ans=max(ans,which);
  }
  if(which>m){
    return;
  }
  for(int i=1;i<=n;i++){
    if(i!=now&&mp[now][i]!=-1&&viss[now][i]==false){
      viss[now][i]=true;
      viss[i][now]=true;
      dfs1(i,lenth+mp[now][i],which,zuixiao);
      viss[now][i]=false;
      viss[i][now]=false;
    }
    if(lenth>0){
      if(!vis[i]){
	vis[i]=true;
	dfs1(i,0,which+1,min(zuixiao,lenth));
	vis[i]=false;
      }
    }
  }
  return;
}

void solve0(){
  for(int i=1;i<=n;i++){
    for(int j=1;j<=n;j++){
      vis[j]=false;
    }
    vis[i]=true;
    dfs(i,0);
  }
}

void solve1(){
  for(int i=1;i<=n;i++){
    memset(vis,0,sizeof(vis));
    memset(viss,0,sizeof(viss));
    vis[i]=true;
    dfs1(i,0,1,99999999);
  }
  return;
}

void solve2(){
  for(int i=1;i<=2*(n-1);i++){
    if(rand()%2){
      ans=e[i].w;
    }
  }
  return;
}

int main(){
  srand(666623333);
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  cnt=0;
  scanf("%d %d",&n,&m);
  if(n<=200){
    memset(mp,-1,sizeof(mp));
    for(int i=1;i<n;i++){
      int x,y,z;
      scanf("%d %d %d",&x,&y,&z);
      mp[x][y]=z;
      mp[y][x]=z;
    }
  }
  bool pd=true;
  bool pd2=true;
  for(int i=1;i<n;i++){
    int x,y,z;
    scanf("%d %d %d",&x,&y,&z);
    if(x!=1){
      pd=false;
    }
    if(y!=x+1){
      pd2=false;
    }
    add(x,y,z);
    add(y,x,z);
  }
  if(m==1&&pd==true){
    sort(e+1,e+1+cnt,cmp);
    ans=e[1].w+e[3].w;
  }
  else if(m==1){
    solve0();
  }
  else if(n<=200){
    solve1();
  }
  else if(pd==true){
    solve2();
  }
  else if(pd2==true){
    solve2();
  }
  else{
    solve2();
  }
  printf("%d\n",ans);
  return 0;
}
