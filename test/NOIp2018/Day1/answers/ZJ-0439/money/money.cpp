#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
int ans,T,n,a[105];
bool flag[25005];
inline void Preparation()
{
	ans=0;flag[0]=1;
	for (int i=1;i<=25000;i++) flag[i]=0;
}
inline void solve()
{
	Preparation();
	scanf("%d",&n);ans=n;
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	sort(a+1,a+n+1);
	for (int i=1;i<=n;i++)
	{
		if (flag[a[i]]){ans--;continue;}
		for (int j=a[i];j<=a[n];j++)
			if (flag[j-a[i]]) flag[j]=1;
	}
	printf("%d\n",ans);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--) solve();
	return 0;	
}
