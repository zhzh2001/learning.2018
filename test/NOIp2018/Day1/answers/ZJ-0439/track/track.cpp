#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
inline int read()
{
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
int n,m,tot,dis[50005],sum,B;
int Head[50005],ret[100005],Next[100005],len[100005];
int a[50005];
inline void ins(int x,int y,int z)
{
	ret[++tot]=y;len[tot]=z;
	Next[tot]=Head[x];Head[x]=tot;
}
void dfs_m1_1(int now,int f)
{
	for (int i=Head[now];i;i=Next[i])
	{
		if (ret[i]==f) continue;
		dis[ret[i]]=dis[now]+len[i];
		dfs_m1_1(ret[i],now);
	}
}
void dfs_m1_2(int now,int f)
{
	for (int i=Head[now];i;i=Next[i])
	{
		if (ret[i]==f) continue;
		dis[ret[i]]=dis[now]+len[i];
		dfs_m1_2(ret[i],now);
	}
}
void m1()
{
	dfs_m1_1(1,0);
	int now=0;
	for (int i=1;i<=n;i++) if (dis[i]>dis[now]) now=i;
	dis[now]=0;
	dfs_m1_2(now,0); 
	for (int i=1;i<=n;i++) if (dis[i]>dis[now]) now=i;
	printf("%d",dis[now]);
}
bool judge_A1(int mid)
{
	int pos=1,num=0;
	for (int i=n-1;i>=1;i--)
	{
		if (a[i]>=mid){num++;continue;}
		else
		{
			while (a[pos]+a[i]<mid&&pos<i) pos++;
			if (pos>=i) break;
			pos++;num++;
		}
	}
	return (num>=m);
}
void A1()
{
	int cnt=0;
	for (int i=Head[1];i;i=Next[i]) a[++cnt]=len[i];
	sort(a+1,a+cnt+1);
	int l=0,r=2*a[cnt];
	while (l!=r)
	{
		int mid=(l+r+1)>>1;
		if (judge_A1(mid)) l=mid;
		else r=mid-1;
	}
	printf("%d",l);
}
void dfs_Bi(int now,int f)
{
	for (int i=Head[now];i;i=Next[i])
	{
		if (ret[i]==f) continue;
		a[++B]=len[i];sum+=len[i];
		dfs_Bi(ret[i],now);
	}
}
bool judge_Bi(int mid)
{
	int L=0,num=0;
	for (int i=1;i<=B;i++)
	{
		L+=a[i];if (L>=mid) num++,L=0;
	}
	return (num>=m);
}
void Bi()
{
	dfs_Bi(1,0);
	int l=0,r=sum;
	while (l!=r)
	{
		int mid=(l+r+1)>>1;
		if (judge_Bi(mid)) l=mid;
		else r=mid-1;
	}
	printf("%d",l);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();bool a1=1,bi=1;
	for (int i=1;i<n;i++)
	{
		int x=read(),y=read(),z=read();
		ins(x,y,z);ins(y,x,z);
		if (x+1!=y) bi=0;
		if (x!=1) a1=0;
	}
	if (m==1) m1();
	else if (a1) A1();
	else if (bi) Bi();
	return 0;
}
