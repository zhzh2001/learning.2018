var
  n,i,x,pre:longint;
  ans:int64;
begin
  assign(input,'road.in');reset(input);
  assign(output,'road.out');rewrite(output);
  readln(n);
  pre:=0;
  for i:=1 to n do
  begin
    read(x);
    if x>pre then inc(ans,x-pre);
    pre:=x;
  end;
  writeln(ans);
  close(input);
  close(output);
end.