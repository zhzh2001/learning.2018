var
  n,m,i,a,b,c,l,r,mid,cnt,cnt1:longint;
  v,w,nxt,head,val,d,head1,w1,nxt1,e:array[0..100005] of longint;
  p:array[0..100005] of boolean;
procedure add(a,b,c:longint);
begin
  inc(cnt);
  v[cnt]:=b;
  w[cnt]:=c;
  nxt[cnt]:=head[a];
  head[a]:=cnt;
end;
procedure qsort(l,r:longint);
var
  i,j,mid,x:longint;
begin
  i:=l;j:=r;mid:=d[(l+r)>>1];
  repeat
    while d[i]>mid do inc(i);
    while d[j]<mid do dec(j);
    if i<=j then
    begin
      x:=d[i];d[i]:=d[j];d[j]:=x;
      inc(i);dec(j);
    end;
  until i>j;
  if i<r then qsort(i,r);
  if j>l then qsort(l,j);
end;
procedure add1(u,k:longint);
begin
  inc(cnt1);
  w1[cnt1]:=k;
  nxt1[cnt1]:=head1[u];
  head1[u]:=cnt1;
end;
function dfs(u,k:longint):longint;
var
  i,num,sum,s,l,r,id,ll,rr,mid,kl,kr,ans,ans1:longint;
begin
  p[u]:=true;
  i:=head[u];
  num:=0;
  sum:=0;
  val[u]:=0;
  while i<>0 do
  begin
    if not p[v[i]] then
    begin
      s:=dfs(v[i],k);
      inc(sum,s);
      inc(num);
      add1(u,val[v[i]]+w[i]);
    end;
    i:=nxt[i];
  end;
  i:=head1[u];
  id:=1;
  while i<>0 do
  begin
    d[id]:=w1[i];
    i:=nxt1[i];
    inc(id);
  end;
  if num=0 then exit(sum);
  qsort(1,num);
  l:=1;r:=num;
  while (l<=num) and (d[l]>=k) do
  begin
    inc(sum);
    inc(l);
  end;
  if l>r then exit(sum);
  ll:=l;rr:=num;
  kl:=l;kr:=r;
  ans:=0;
  while l<r do
  begin
    while (l<r) and (d[l]+d[r]<k) do
    begin
      dec(r);
    end;
    if l<r then inc(ans);
    inc(l);dec(r);
  end;
  if (ans<<1<kr-kl+1) then
  begin
    while (ll<rr) do
    begin
      mid:=(ll+rr)>>1;
      for i:=1 to mid-1 do
      e[i]:=d[i];
      for i:=mid to num-1 do
      e[i]:=d[i+1];
      l:=kl;r:=kr-1;
      ans1:=0;
      while l<r do
      begin
        while (l<r) and (e[l]+e[r]<k) do
        begin
          dec(r);
        end;
        if l<r then inc(ans1);
        inc(l);dec(r);
      end;
      if ans=ans1 then rr:=mid
      else ll:=mid+1;
    end;
    val[u]:=d[ll];
  end;
  inc(sum,ans);
  exit(sum);
end;
begin
  assign(input,'track.in');reset(input);
  assign(output,'track.out');rewrite(output);
  read(n,m);
  l:=0;r:=0;
  for i:=1 to n-1 do
  begin
    read(a,b,c);
    add(a,b,c);
    add(b,a,c);
    inc(r,c);
  end;
  while l<r do
  begin
    for i:=1 to n do
    p[i]:=false;
    mid:=(l+r+1)>>1;
    cnt1:=0;
    for i:=1 to n do
    head1[i]:=0;
    if dfs(1,mid)>=m then l:=mid
    else r:=mid-1;
  end;
  writeln(l);
  close(input);
  close(output);
end.
