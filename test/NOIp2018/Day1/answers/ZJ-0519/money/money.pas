var
  t,l,n,i,j,ans,max:longint;
  a:array[0..105] of longint;
  f:array[0..25005] of boolean;
procedure qsort(l,r:longint);
var
  i,j,mid,x:longint;
begin
  i:=l;j:=r;mid:=a[(l+r)>>1];
  repeat
    while a[i]<mid do inc(i);
    while a[j]>mid do dec(j);
    if i<=j then
    begin
      x:=a[i];a[i]:=a[j];a[j]:=x;
      inc(i);dec(j);
    end;
  until i>j;
  if i<r then qsort(i,r);
  if j>l then qsort(l,j);
end;
begin
  assign(input,'money.in');reset(input);
  assign(output,'money.out');rewrite(output);
  readln(t);
  for l:=1 to t do
  begin
    readln(n);
    max:=0;
    for i:=1 to n do
    begin
      read(a[i]);
      if a[i]>max then max:=a[i];
    end;
    ans:=0;
    for i:=1 to max do
    f[i]:=false;
    f[0]:=true;
    qsort(1,n);
    for i:=1 to n do
    if not f[a[i]] then
    begin
      inc(ans);
      for j:=a[i] to max do
      f[j]:=f[j] or f[j-a[i]];
    end;
    writeln(ans);
  end;
  close(input);
  close(output);
end.