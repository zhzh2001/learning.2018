#include <cstdio>
#include <cstring>
#include <algorithm>

int a[109];
int can[25009];

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T--) {
		int n;
		scanf("%d", &n);
		for (int i = 0; i < n; i++) {
			scanf("%d", &a[i]);
		}
		std::sort(a, a + n);
		int ans = 0;
		memset(can, 0, sizeof(can));
		can[0] = true;
		for (int i = 0; i < n; i++) {
			if (!can[a[i]]) ans++;
			for (int j = a[i]; j <= a[n - 1]; j++) {
				if (can[j - a[i]]) can[j] = true;
			}
		}
		printf("%d\n", ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
