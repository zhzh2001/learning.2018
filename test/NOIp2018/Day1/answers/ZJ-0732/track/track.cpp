#include <cstdio>
#include <algorithm>

int left[50009], right[50009];
int lengl[50009], lengr[50009];
int arr[50009];
int suml[50009];
int dp[1009][1009];

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	int n, m;
	scanf("%d %d", &n, &m);
	for (int i = 0; i < n - 1; i++) {
		int a, b, l;
		scanf("%d %d %d", &a, &b, &l);
		if (!left[a]) {
			left[a] = b;
			lengl[a] = l;
		}
		else {
			right[a] = b;
			lengr[a] = l;
		}
		if (!left[b]) {
			left[b] = a;
			lengl[b] = l;
		}
		else {
			right[b] = a;
			lengr[b] = l;
		}
	}
	int s = -1, e;
	for (int i = 1; i <= n; i++) {
		if (!right[i] && s != -1) s = i;
		else e = i;
	}
	int last = 0;
	for (int i = s, j = 0; i != e; i = right[i], j++) {
		if (left[i] != last) {
			std::swap(left[i], right[i]);
			std::swap(lengl[i], lengr[i]);
		}
		last = i;
		arr[j] = lengr[i];
	}
	for (int i = 1; i <= n - 1; i++) {
		suml[i] = suml[i - 1] + arr[i];
	}
	for (int i = 0; i <= n - 1; i++) {
		dp[i][1] = suml[i];
	}
	for (int i = 1; i <= n - 1; i++) {
		for (int j = 1; j <= i; j++) {
			for (int k = 0; k <= i; k++) {
				dp[i][j] = std::max(dp[i][j], std::min(dp[k][j - 1], suml[i] - suml[k]));
			}
		}
	}
	printf("%d\n", dp[n - 1][m]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
