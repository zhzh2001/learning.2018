#include <cstdio>

int a[100009];
int stack[100009];
int top = -1;
int vis[100009];

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	int n;
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", &a[i]);
	}
	int ans = 0;
	int min = 0x7fffffff;
	int sum = 0;
	while (sum < n) {
		for (int i = 0; i < n; i++) {
			if (a[i] == 0) {
				if (!vis[i]) {
					sum++;
					vis[i] = true;
				}
				if (min != 0x7fffffff) ans += min;
				while (top > -1) {
					a[stack[top--]] -= min;
				}
				min = 0x7fffffff;
			}
			else {
				if (a[i] < min) min = a[i];
				stack[++top] = i;
			}
		}
		if (min != 0x7fffffff) ans += min;
		while (top > -1) {
			a[stack[top--]] -= min;
		}
		min = 0x7fffffff;
	}
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
