#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
bool m[30000];
int n[300];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t;
	cin >> t;
	for (int k=1;k<=t;k++)
	{
		memset(m,false,sizeof(m));
		m[0]=true;
		int a;
		cin >> a;
		int ans=0;
		int sum=0;
		for (int i=1;i<=a;i++)
		{
			scanf("%d",&n[i]);
			if (n[i]>sum) sum=n[i];
		}
		sort(n+1,n+a+1);
		for (int i=1;i<=a;i++)
		{
			int v=n[i];
			if (m[v]==true) continue;
			ans++;
			for (int j=0;j<=sum-v;j++)
			if (m[j]==true) m[j+v]=true;
		}
		cout << ans << endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
