#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
int a,b;
int ans=0,sum=0,xx;
bool temp=true,vis[50010];
int head[50010],cnt=0,w[50010],p[50010][4][3];
struct Edge
{
	int to;
	int nxt;
	int cost;
} edge[100010];
void add(int x,int y,int z)
{
	edge[++cnt]=(Edge){y,head[x],z};
	head[x]=cnt;
}
void dfs1(int x,int y)
{
	vis[x]=false;
	if (y>sum) 
	{
		sum=y;
		xx=x;
	}
	for (int i=head[x];i;i=edge[i].nxt)
	{                                                                      
		int v=edge[i].to;
		if (vis[v]==true) dfs1(v,y+edge[i].cost);
	}
}
void dfs2(int x,int y)
{
	vis[x]=false;
	if (y>ans) ans=y;
	for (int i=head[x];i;i=edge[i].nxt)
	{
		int v=edge[i].to;
		if (vis[v]==true) dfs2(v,y+edge[i].cost);
	}
}
bool pd(int x)
{
	sum=0;
	ans=0;
	for (int i=1;i<=a-1;i++)
	{
		sum+=w[i];
		if (sum>x) 
		{
			ans++;
			sum=0;
		}
	}
	if (ans>=b) return true; else return false;
}
void find(int x)
{
	vis[x]=false;
	int r=0;
	for (int i=head[x];i;i=edge[i].nxt)
	{
		int v=edge[i].to;
		if (vis[v]==true)
		{
			r++;
			p[x][r][1]=v;
			p[x][r][2]=edge[i].cost;
			find(v);
		}
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin >> a >> b;
	for (int i=1;i<=a-1;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		w[x]=z;
		if (y!=x+1) temp=false;
		add(x,y,z);
		add(y,x,z);
	}
	if (b==1)
	{
		memset(vis,true,sizeof(vis));
	    dfs1(1,0);
	    memset(vis,true,sizeof(vis));
	    dfs2(xx,0);
	    cout << ans;
	    fclose(stdin);
		fclose(stdout);
	    return 0;
	}
	if (temp==true)
	{
		int r=999999999,l=0;
		while (r>l+1)
		{
			int mid=(l+r)/2;
			if (pd(mid)==true) l=mid; else r=mid;
		}
		cout << r;
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
    cout << 2;
    fclose(stdin);
    fclose(stdout);
	return 0;
}
