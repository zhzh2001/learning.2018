#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#define Nax 25000
#define ms
#define LL long long
using namespace std;
int a[105],f[Nax+50];
int T,n,ans;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T)
	{
		--T;
		scanf("%d",&n);
		for (int i=1;i<=n;++i)
		  scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		
		ans=0;
		for (int i=1;i<=Nax;++i) 
		  f[i]=0;
		  
		for (int i=1;i<=n;++i)
		  if (!f[a[i]])
		  {
		  	++ans; f[a[i]]=1;
		  	for (int j=a[i]+1;j<=Nax;++j)
		  	  if (f[j-a[i]]) f[j]=1;
		  }
		printf("%d\n",ans);
	}
	return 0;
}
