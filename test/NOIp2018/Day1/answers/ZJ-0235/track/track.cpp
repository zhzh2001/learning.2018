#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#define N 50050
#define ms
#define LL long long
using namespace std;
struct node{
	int u,v,w;
} b[3*N];
int dy[N],f[N],ljb[51][51];
int n,m,ans,dq;

int cmp(node x,node y){
	if (x.u!=y.u) return x.u<y.u;
	return x.v<y.v;
}
int cop(node x,node y){
	return x.w>y.w;
}

int src(int,int);
int bg(int ci,int bs,int k,int s)
{
	if (s>ans)
	  if (s<dq) 
	  {
		int p=dq; dq=s;
		src(ci+1,bs); dq=p;
	  }
	   else src(ci+1,bs);
	if (bs<=m-ci) return 0;
	for (int i=dy[k];i<dy[k+1];++i)
	  if (!ljb[k][b[i].v]) 
	  {
	  	ljb[k][b[i].v]=ci; ljb[b[i].v][k]=ci;
	  	bg(ci,bs-1,b[i].v,s+b[i].w);
	  	ljb[k][b[i].v]=0; ljb[b[i].v][k]=0;
	  }
}
int src(int k,int xz)
{
	if (k>m) {
		ans=dq; return 0;
	}
	for (int i=1;i<=n;++i)
	  bg(k,xz,i,0);
}

int pd(int k)
{
	int l,r=n;
	for (int i=1;i<=m;i++)
	if (b[i].w<k)
	{
		l=i+1; if (l>=r) return 0;
		if (b[i].w+b[i+1].w<k) return 0;
		while (l+1<r)
		{
			int mid=(l+r)>>1;
			if (b[mid].w+b[l].w>=k) l=mid;
			 else r=mid;
		}
		if (b[i].w+b[l].w>=k) r=l;
		 else return 0;
	}
	return 1;
}

int cl(int k)
{
	int l=1,yl=0;
	for (int i=1;i<n;i++)
	{
		yl+=b[i].w;
		while (yl-b[l].w>=k) yl-=b[l++].w;
		if (yl>=k) f[i]=f[l-1]+1;
		 else f[i]=f[i-1];
		if (f[i]>=m) return 1;
	}
	return 0;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d %d",&n,&m);
	int jht=1,lian=1;
	for (int i=1;i<n;++i)
	{
		int x,y,z;
		scanf("%d %d %d",&x,&y,&z);
		if (x!=1) jht=0;
		if (x+1!=y) lian=0;
		b[2*i-1].u=b[2*i].v=x;
		b[2*i-1].v=b[2*i].u=y;
		b[2*i-1].w=b[2*i].w=z;
	}
	sort(b+1,b+2*n-1,cmp);
	for (int i=1;i<=2*n-2;++i)
	  if (!dy[b[i].u]) dy[b[i].u]=i;
	dy[n+1]=2*n-1;
	
	if (jht)
	{
		sort(b+1,b+n,cop); b[n].w=0;
		int l=b[n-1].w; 
		int r=b[1].w+b[2].w;
		while (l+1<r)
		{
			int mid=(l+r)>>1;
			if (pd(mid)) l=mid;
			 else r=mid;
		}
		if (pd(r)) printf("%d",r);
		 else printf("%d",l);
		return 0;
	}
	
	if (lian)
	{
		int l=1000000000; int r=0;
		for (int i=1;i<n;i++) 
		{
			b[i]=b[2*i-1];
			if (b[i].w<l) l=b[i].w;
			r+=b[i].w;
		}
		b[n].w=0;
		if (m==1) 
		{
			printf("%d",r);
			return 0;
		}
		while (l+1<r)
		{
			int mid=(l+r)>>1;
			if (cl(mid)) l=mid;
			 else r=mid;
		}
		printf("%d",l);
		return 0;
	}
	
	dq=1000000007;
	src(1,n-1);
	printf("%d",ans);
}
