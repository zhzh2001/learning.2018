#include<bits/stdc++.h>
using namespace std;
const int N=1e5+100;
int n,a[N];
long long ans;
int read()
{
	int x;
	scanf("%d",&x);
	return x;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;++i)
		a[i]=read();
	ans=a[1];
	for(int i=2;i<=n;++i)
		if(a[i]>a[i-1]) ans+=a[i]-a[i-1];
	printf("%lld",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
