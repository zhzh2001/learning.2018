#include<bits/stdc++.h>
using namespace std;
const int N=150,M=25100;
int n,m,cnt,a[N],dis[M],diss[M],ans;
bool vis[M];
vector<int> head,nx,e,w;
struct node
{
	int num,x;
	const bool operator<(const node &b)const {return x<b.x;}	
};
int read()
{
	int x;
	scanf("%d",&x);
	return x;
}
void adde(int i,int j,int len)
{
	e[++cnt]=j;
	w[cnt]=len;
	nx[cnt]=head[i];
	head[i]=cnt;
}
void dijkstra(int r)
{
	int cot=0;
	memset(vis,0,sizeof(vis));
	priority_queue<node> q;
	node t;
	t.num=r;
	t.x=0;
	q.push(t);
	while(!q.empty())
	{
		t=q.top();
		q.pop();
		while(vis[t.num])
		{
			t=q.top();
			if(q.empty()) break;
			q.pop();
		}
		if(cot==m) break;
		int u=t.num;
		vis[u]=true;
		++cot;
		for(int i=head[u];i;i=nx[i])
		{
			int y=e[i],c=w[i];
			if(dis[y]>dis[u]+c)
			{
				dis[y]=dis[u]+c;
				t.num=y;
				t.x=dis[y];
				q.push(t);
			}
		}
	}
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--)
	{
		m=0x3fffffff;
		cnt=0;
		memset(dis,0x3f,sizeof(dis));
		memset(diss,0x3f,sizeof(diss));
		n=read();
		head.resize(n*25010);
		nx.resize(n*25010);
		e.resize(n*25010);
		w.resize(n*25010);
		head.clear();
		nx.clear();
		w.clear();
		e.clear();
		for(int i=1;i<=n;++i)
			m=min(m,a[i]=read());
		dis[0]=0;
		diss[0]=0;
		for(int i=1;i<=n;++i)
		{
			int t=a[i]%m;
			for(int j=0;j<m;++j)
				adde(j,(j+t)%m,a[i]);
		}
		dijkstra(0);
		for(int i=1;i<=n;++i)
		{
			int u=0x3f3f3f3f;
			for(int j=0;j<m;++j)
				if(dis[j]!=diss[j]&&dis[j]<u) u=dis[j];
			if(u==0x3f3f3f3f) 
			{
				ans=i;
				break;
			}
			for(int j=0;j<m;++j)
				for(int k=0;k<m;++k)
					if(diss[(k+u)%m]>diss[k]+u)
						diss[(k+u)%m]=diss[k]+u;
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;	
}

