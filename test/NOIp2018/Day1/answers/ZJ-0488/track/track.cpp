#include<bits/stdc++.h>
using namespace std;
const int N=5e4+100;
int n,m,cnt,cot,head[N],nx[N],e[N],w[N],te[N];
bool f1,f2;
int read()
{
	int x;
	scanf("%d",&x);
	return x;
}
void adde()
{
	int i,j,len;
	scanf("%d%d%d",&i,&j,&len);
	if(i!=1) f1=false;
	if(i+1!=j) f2=false;
	te[++cot]=len;
	e[++cnt]=j;
	w[cnt]=len;
	nx[cnt]=head[i];
	head[i]=cnt;
	e[++cnt]=i;
	w[cnt]=len;
	nx[cnt]=head[j];
	head[j]=cnt;
}
void dfs(int x,int fa,int dis[])
{
	for(int i=head[x];i;i=nx[i])
		{
			int y=e[i],c=w[i];
			if(y==fa) continue;
			dis[y]=dis[x]+c;
			dfs(y,x,dis);
		}
}
int calcd()
{
	int diss[N],u=1,len=0;
	memset(diss,0x3f,sizeof(diss));
	dfs(1,0,diss);
	diss[1]=0;
	for(int i=1;i<=n;++i)
		if(diss[u]<diss[i]) u=i;
	diss[u]=0;
	dfs(u,0,diss);
	for(int i=1;i<=n;++i)
		len=max(len,diss[i]);
	return len;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	f1=true;f2=true;
	n=read();m=read();
	for(int i=1;i<n;++i)
		adde();
	if(m==1)
	{
		printf("%d",calcd());
		return 0;
	}
	if(f1)
	{
		sort(te+1,te+n);
		int l=1,r=1e9,mid;
		while(l<=r)
		{
			mid=(l+r)>>1;
			int h=1,t=n-1,ta=0;
			while(h<t)
			{
				while(te[h]+te[t]<mid) ++h;
				if(h<t)
				{
					ta++;
					h++;
					t--;
				}
			}
			if(ta>=m)
				l=mid+1;
			else
			 	r=mid-1;
		}
		printf("%d",r);
	}
	if(f2)
	{
		int l=1,r=1e9,mid;
		while(l<=r)
		{
			mid=(l+r)>>1;
			int ta=0,sum=0;
			for(int i=1;i<=n;++i)
			{
				sum+=te[i];
				if(sum>=mid)
				{
					++ta;
					sum=0;
				}
			}
			if(ta>=m)
				l=mid+1;
			else
			 	r=mid-1;
		}
		printf("%d",r);
	}
	fclose(stdin);
	fclose(stdout);
}
