#include <bits/stdc++.h>
using namespace std;
const int maxn = 100010;
int n;
int a[maxn],b[maxn];
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i = 1; i <= n; ++i) scanf("%d",&a[i]);
	int cur = 0;
	int ans = 0;
	if (n >= 2000)
	{
		for (int i = n; i >= 1; --i)
		{
			int tmp = -(cur+a[i]);
			if (tmp * cur < 0)
			{
				if (abs(tmp) > abs(cur)) ans += abs(tmp) - abs(cur);
				cur += tmp; 
			}
			else ans += abs(tmp), cur += tmp;
		}
		printf("%d\n",ans);		
	}
	else
	{
		int Ans = 0;
			int ans = 0;
			for (int i = n; i >= 1; --i)
			{
				int tmp = -(cur+a[i]);
				if (tmp * cur < 0)
				{
					if (abs(tmp) > abs(cur)) ans += abs(tmp) - abs(cur);
					cur += tmp; 
				}
				else ans += abs(tmp), cur += tmp;
			}		
			Ans = max(Ans,ans);
		printf("%d\n",ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
