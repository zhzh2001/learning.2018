#include <bits/stdc++.h>
using namespace std;
const int maxn = 105;
const int maxm = 25010;
int T,n;
int a[maxn];
bool v[maxm],w[maxm];
inline int read()
{
	int x = 0, f =1 ;
	char ch = 0;
	for (;!isdigit(ch); ch = getchar()) if (ch == '-') f = -1;
	for (;isdigit(ch); ch = getchar()) x = x * 10 + ch - 48;
	return x * f;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T = read();
	while (T--)
	{
		n = read();
		for (int i = 1; i <= n; ++i) a[i] = read();
		memset(v,0,sizeof(v)); v[0] = 1;
		memset(w,0,sizeof(w));
		for (int i = 1; i <= 25000; ++i)
		{
			for (int j = 1; j <= n; ++j)
			if (i - a[j] >= 0)
			{
				if (v[i - a[j]])
				{
					v[i] = 1;
					if (i - a[j] > 0) w[i] = 1;
				}
			}
		}
		int ans = n;
		for (int i = 1; i <= n; ++i) if (w[a[i]]) ans--;
		printf("%d\n",ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
