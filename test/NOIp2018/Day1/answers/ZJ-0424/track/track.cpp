#include <bits/stdc++.h>
#define PB push_back
using namespace std;
inline int read()
{
	int x = 0, f =1 ;
	char ch = 0;
	for (;!isdigit(ch); ch = getchar()) if (ch == '-') f = -1;
	for (;isdigit(ch); ch = getchar()) x = x * 10 + ch - 48;
	return x * f;
}
const int maxn = 50010;
struct node
{
	int ne,to,dis;
}e[maxn << 1];

int he[maxn],nume;
int n,m,need;
int dp[maxn],lgt[maxn];
bool vi[maxn];
inline void addside(int u,int v,int w)
{
	e[++nume].ne = he[u];
	e[nume].to = v;
	e[nume].dis = w;
	he[u] = nume;
}
inline void dfs(int u,int pre)
{
	int resd = 0, resl = 0,cnt = 0;
	vector<int> dq; dq.clear(); dq.PB(-1);
	for (int i = he[u];i; i = e[i].ne)
	{
		int v = e[i].to,w = e[i].dis;
		if (v == pre) continue;
		dfs(v,u);
		resd += dp[v];
		dq.PB(w + lgt[v]),cnt++;
	}
	
	sort(dq.begin(), dq.end());

	while (dq[cnt] >= need&&cnt >=1) resd++, cnt--;
	int PP = cnt,i;
	bool pd = 1;
	for (int i = 1; i <= cnt; ++i) vi[i] = 0;
	for (i = 1; i < PP; ++i)
	{
		if (vi[i]) continue;
		int j = i+1;
		while ((vi[j])||(dq[j] + dq[i] < need&& j<=PP)) j++;
		if (j <= PP) vi[i] = vi[j] = 1,resd++;
	}
	for (int i = cnt; i >= 1; --i) if (!vi[i]) {resl = dq[i];break;}
	dp[u] = resd; lgt[u] = resl;
}
int W[maxn<<1];

inline void kill()
{
	for (int i = 1; i <= nume; i += 2) W[i / 2+1] = e[i].dis;
	sort(W + 1, W + nume /2 +1);
	int ll = 0, rr = 500000000;
	while (ll <= rr)
	{
		int mid = ll + rr >>1;
		int L = 1, R = nume /2 ;
		int resd = 0;
		while (L <= R)
		{
			while (W[L] + W[R] < mid&&L<R) L++;
			if (L>=R) break;
			L++; R--; resd++;
		}		
		if (resd >= m) ll = mid + 1; else rr = mid - 1;
	}
	printf("%d\n",rr);

}
inline bool work(int mid)
{
	need = mid;
	dfs(1,0);
	if (dp[1] >= m) return 1; else return 0;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n = read(); m = read();
	bool all = 1;
	for (int i = 1; i < n; ++i)
	{
		int x = read(), y = read(), z = read();
		if (x!=1&&y!=1) all = 0;
		addside(x,y,z); addside(y,x,z);
	}
	if (all&&n>=1000) {kill();return 0;}
	int L = 1, R = 500000000;
	while (L <= R)
	{
		int mid = L + R >> 1;
		if (work(mid)) L = mid + 1; else R = mid - 1;
	}
	printf("%d\n",R);
	fclose(stdin);
	fclose(stdout);
//	cerr<<work(31)<<endl;
	return 0;
}
