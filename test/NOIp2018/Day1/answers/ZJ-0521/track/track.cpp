#include<set>
#include<cstdio>
#include<vector>
#include<cstring>
#include<utility>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x) for(int y,i=head[x];i;i=E[i].nxt)

using namespace std;

const int N=50035;

set<pair<int,int> >S[N];
set<pair<int,int> >::iterator it;

vector<int>V[N],ok[N];
int cnt,head[N],L,R,n,m,deg[N],f[N],len[N],key,Ans;

struct Edge {
	int to,nxt,val;
}E[N<<1];

void Link(int u,int v,int w) {
	E[++cnt]=(Edge){v,head[u],w},head[u]=cnt;
	E[++cnt]=(Edge){u,head[v],w},head[v]=cnt;
}

void dfs(int x,int fa) {
	/*
	if (x==4 && key==15) {
		puts("2");
	}
	*/
	Rep (i,x) if ((y=E[i].to)!=fa) {
		dfs(y,x);
		f[x]+=f[y];
		if (len[y]+E[i].val>=key) f[x]++;
		else {
			V[x].push_back(len[y]+E[i].val);
			ok[x].push_back(1);
		}
	}
	
	int l,r,mx;
	
	if (V[x].size()>1) {
		l=0,r=V[x].size()-2;
		
		sort(V[x].begin(),V[x].end());
		mx=V[x][r+1];
		/*
		for (int i=0;i<V[x].size();i++)
			S[x].insert(make_pair(V[x][i],i));
		*/
		while (l<r) {
			while (l<r && V[x][l]+V[x][r]<key) l++;
			if (l<r) {
				f[x]++;
				ok[x][l]=0;
				ok[x][r]=0;
				l++,r--;
			}
		}
		for (int i=0;i<V[x].size()-1;i++)
			if (ok[x][i] && mx+V[x][i]>=key) {
				f[x]++;
				ok[x][i]=0;
				ok[x][V[x].size()-1]=0;
				break;
			}
	}
	
	len[x]=0;
	for (int i=0;i<V[x].size();i++)
		if (ok[x][i]) len[x]=V[x][i];
	
	return;
}

int main() {
	
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	scanf("%d%d",&n,&m),L=1e6;
	For (i,1,n-1) {
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		Link(x,y,z),deg[x]++,deg[y]++,R+=z,L=min(L,z);
		
//		if (deg[x]>3 || deg[y]>3) puts("F@Q");
		
	}
	/*
	key=16;
	dfs(1,-1);
	*/
	while (L<=R) {
		key=(L+R)>>1;
		
		memset(f,0,sizeof f);
		memset(len,0,sizeof len);
		For (i,1,n) {
			V[i].clear();
			ok[i].clear();
//			S[i].clear();
		}
		
		dfs(1,-1);
	
		if (f[1]>=m)
			 Ans=key,L=key+1;
		else R=key-1;
	}
	
	printf("%d\n",Ans);
	
	return 0;
}
/*
10 3
1 2 6
2 3 3
3 4 5
4 5 10
6 2 4
7 2 9
8 4 7
9 4 4
6 10 3

16
*/
