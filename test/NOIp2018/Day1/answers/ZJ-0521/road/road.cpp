#include<cstdio>
#include<cstring>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)
#define Dep(i,x,y) for(int i=x;i>=y;i--)

using namespace std;

typedef long long LL;

const int N=1e5+35;

int LOG[N],d[N],a[N][25],n,ans;

int qry(int l,int r) {
	int len=r-l+1,lg=LOG[len];
	int L=a[l][lg],R=a[r-(1<<lg)+1][lg];
	if (d[L]<=d[R]) return L; else return R;
}

void solve(int l,int r,int x) {
	if (l>r) return;
	int pos=qry(l,r);
	ans+=d[pos]-x;
	solve(l,pos-1,d[pos]);
	solve(pos+1,r,d[pos]); return;
}

int main() {
	
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	scanf("%d",&n),LOG[0]=-1;
	For (i,1,n) scanf("%d",&d[i]),a[i][0]=i,LOG[i]=LOG[i>>1]+1;
	
	For (i,1,LOG[n]) For (j,1,n-(1<<i)+1) {
		if (d[ a[j][i-1] ] > d[ a[j+(1<<(i-1))][i-1] ])
			 a[j][i]=a[j+(1<<(i-1))][i-1];
		else a[j][i]=a[j][i-1];
	}
	
	solve(1,n,0);
	
	printf("%d\n",ans);
	
	return 0;
}
