#include<cstdio>
#include<cstring>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)
#define Dep(i,x,y) for(int i=x;i>=y;i--)

using namespace std;

int n,T,ans,f[25025],ok[25025],vis[25025],a[125],mx;

int main() {
	
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	scanf("%d",&T);
	while (T--) {
		scanf("%d",&n);
		memset(a,0,sizeof a);
		memset(f,0,sizeof f);
		memset(ok,0,sizeof ok);
		memset(vis,0,sizeof vis);
	
		f[0]=1,ans=n;
		For (i,1,n) scanf("%d",&a[i]),ok[a[i]]=1;
		sort(a+1,a+n+1),mx=a[n];
		
		For (i,1,n) {
			if (vis[a[i]]) continue;
			For (j,a[i],mx) f[j]|=f[j-a[i]];
			For (j,i+1,n) if (!vis[a[j]] && f[a[j]]) vis[a[j]]=1,ans--;
		}
		
		printf("%d\n",ans);
	}
	
	return 0;
}
/*
2
4
3 19 10 6
5
11 29 13 19 17

*/
