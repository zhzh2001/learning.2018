#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

typedef long long LL;
const int MAXN = 50010;
int head[MAXN], nxt[MAXN << 1], to[MAXN << 1], val[MAXN << 1], tot;
int n, m, t1, t2, t3;
inline void addedge(int b, int e, int v) {
	nxt[++tot] = head[b]; head[b] = tot; to[tot] = e; val[tot] = v;
	nxt[++tot] = head[e]; head[e] = tot; to[tot] = b; val[tot] = v;
}
const LL BIG = 1000000000;
LL mid;
std::vector<LL> vec[MAXN];
// 希望能卡过
LL dp(int u, int fa) {
	LL ret = 0;
	// LL m1 = 0, m2 = 0;
	for (int i = head[u]; i; i = nxt[i]) if (to[i] != fa) {
		LL res = dp(to[i], u) + val[i], t = res % BIG;
		if (t >= mid) res = res + BIG - t;
		t = res % BIG;
		if (t) vec[u].push_back(t);
		// if (t >= m1) m2 = m1, m1 = t; else if (t > m2) m2 = t;
		ret += res - t;
	}
	std::vector<LL> & vec = ::vec[u];
	if (vec.size()) {
		std::sort(vec.begin(), vec.end());
		LL maxv = vec[0];
		for (int i = 1; i != vec.size() - 1; ++i) {
			const LL nc = vec[i];
		}
//		int lcur = 0, rcur = vec.size() - 1;
//		while (lcur < rcur) {
//			const int tr = vec[rcur];
//			while (lcur < rcur && vec[lcur] + tr < mid) ++lcur;
//			if (lcur < rcur) {
//				vec[rcur] = 0;
//				vec[lcur] = 0;
//				// std::cerr << "ADD " << vec[lcur] << " " << vec[rcur] << " " << mid << " " << lcur << " " << rcur <<  std::endl;
//				++lcur; --rcur;
//				ret += BIG;
//			}
//		}
//		for (int i = vec.size() - 1; ~i; --i) if (vec[i]) {
//			ret += vec[i]; break;
//		}
		vec.clear();
	}
	// if (m1 + m2 >= mid) ret += BIG;
	// else ret += m1;
	// std::cerr << "DP " << u << " RETURNED WITH " << ret / BIG << " " << ret % BIG << std::endl;
	return ret;
}
int main() {
//	freopen("track.in", "r", stdin);
//	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	LL add = 0;
	for (int i = 1; i != n; ++i) {
		scanf("%d%d%d", &t1, &t2, &t3);
		addedge(t1, t2, t3);
		add += t3;
	}
//	mid = 15;
//	std::cerr << dp(1, 0) << std::endl;
	LL l = 1, r = add, ans = 0;
	while (l <= r) {
		mid = l + r >> 1;
		// std::cerr << "SOLVE " << l << " " << r << " " << mid << " " << std::endl;
		if (dp(1, 0) / BIG >= m) l = mid + 1, ans = mid;
		else r = mid - 1;
	}
	printf("%lld\n", ans);
	return 0;
}
