#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int n, m, t1, t2, t3;
const int MAXN = 500010;
int head[MAXN], nxt[MAXN << 1], to[MAXN << 1], val[MAXN << 1], tot = 1;
inline void addedge(int b, int e, int v) {
	nxt[++tot] = head[b]; head[b] = tot; to[tot] = e; val[tot] = v;
	nxt[++tot] = head[e]; head[e] = tot; to[tot] = b; val[tot] = v;
}
// int f[1010][1010];
// bool can[1010][1010];
// inline void getmax(int & x, const int y) { if (y > x) x = y; }
// void dp(int u, int fa, int mid) {
// 	can[u][0] = true;
// 	for (int i = head[u]; i; i = nxt[i]) if (to[i] != fa) {
// 		dp(to[i], u, mid);
// 		memcpy(f[0], f[u], sizeof f[0]);
// 		memcpy(can[0], can[u], sizeof can[0]);
// 		for (int j = n; ~j; --j) if (can[to[i]][j]) {
// 			int tv = f[to[i]][j] + val[i];
// 			if (tv >= mid) {
// 				for (int k = n; ~k; --k) if (can[u][k]) {
// 					getmax(f[0][k + 1], f[u][j]);
// 					can[0][k + 1] = true;
// 				}
// 			} else 
// 			for (int k = n; ~k; --k) if (can[u][k]) {
// 				int tt = f[u][k] + f[to[i]][j] + val[i];
// 				if (tt >= mid) {
// 					getmax(f[0][k + j + 1], 0);
// 					can[0][k + j + 1] = true;
// 				} else {
// 					getmax(f[0][k + j], std::max(f[u][k], f[to[i]][j] + val[i]));
// 					can[0][k + j] = true;
// 				}
// 			}
// 		}
// 		memcpy(f[u], f[0], sizeof f[0]);
// 		memcpy(can[u], can[0], sizeof can[0]);
// 	}
// }
int bel[MAXN], xs[MAXN], ys[MAXN], vs[MAXN], deg[MAXN], cnt[MAXN];
int ans = 0;
void dfs(int st) {
	if (st >= n) {
		// std::cerr << "JUDGE : ";
		// for (int i = 1; i <= n - 1; ++i) std::cerr << bel[i] << " "; std::cerr << endl;
		bool can = true;
		for (int i = 1; i <= m; ++i) {
			cnt[i] = 0;
			for (int j = 1; j <= n; ++j) deg[j] = 0;
			for (int j = 1; j <= n - 1; ++j)
				if (bel[j] == i) ++deg[xs[j]], ++deg[ys[j]], cnt[i] += vs[j];
			int c1 = 0, co = 0;
			for (int j = 1; j <= n; ++j) c1 += deg[j] == 1, co += deg[j] > 2;
			can &= c1 == 2 && co == 0;
			if (!can) break;
		}
		if (can) {
			int t = 0x3f3f3f3f;
			for (int i = 1; i <= m; ++i) t = std::min(t, cnt[i]);
			ans = std::max(ans, t);
		}
		return ;
	}
	for (int i = 0; i <= m; ++i) {
		bel[st] = i;
		dfs(st + 1);
	}
}
int main() {
	freopen("track.in", "r", stdin);
	freopen("track.usr", "w", stdout);
	scanf("%d%d", &n, &m);
	int l = 0, r = 0;
	for (int i = 1; i != n; ++i) {
		scanf("%d%d%d", &xs[i], &ys[i], &vs[i]);
	}
	dfs(1);
	cout << ans << endl;
//	dp(1, 0, 15);
//		for (int i = 1; i <= n; ++i, std::cerr << std::endl) {
//			std::cerr << "DP " << i << " : ";
//			for (int j = 0; j <= n; ++j)
//				std::cerr << (can[i][j] ? f[i][j] : -1) << " ";
//		}
//	int ans = 0;
//	while (l <= r) {
//		int mid = l + r >> 1;
//		memset(f, 0, sizeof f);
//		memset(can, 0, sizeof can);
//		dp(1, 0, mid);
//		std::cerr << "SOVLE " << l << " " << r << " " << mid << std::endl;
//		for (int i = 1; i <= n; ++i, std::cerr << std::endl) {
//			for (int j = 1; j <= n; ++j)
//				std::cerr << f[i][j] << " ";
//		}
//		if (can[1][m]) l = mid + 1, ans = mid;
//		else r = mid - 1;
//	}
//	printf("%d\n", ans);
	return 0;
}
