#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

typedef long long LL;
const int MAXN = 50010;
int head[MAXN], nxt[MAXN << 1], to[MAXN << 1], val[MAXN << 1], tot;
int n, m, t1, t2, t3;
inline void addedge(int b, int e, int v) {
	nxt[++tot] = head[b]; head[b] = tot; to[tot] = e; val[tot] = v;
	nxt[++tot] = head[e]; head[e] = tot; to[tot] = b; val[tot] = v;
}
const LL BIG = 1000000000;
LL mid;
std::vector<LL> vec[MAXN];
// 希望能卡过
LL dp(int u, int fa) {
	LL ret = 0;
	// LL m1 = 0, m2 = 0;
	for (int i = head[u]; i; i = nxt[i]) if (to[i] != fa) {
		LL res = dp(to[i], u) + val[i], t = res % BIG;
		if (t >= mid) res = res + BIG - t;
		t = res % BIG;
		if (t) vec[u].push_back(t);
		ret += res - t;
	}
	std::vector<LL> & vec = ::vec[u];
	const int SZ = vec.size();
	if (SZ) {
		std::sort(vec.begin(), vec.end());
		int lcur = 0, rcur = SZ - 1, ll = -1;
		while (lcur < rcur) {
			const int tr = vec[rcur];
			while (lcur < rcur && vec[lcur] + tr < mid) ++lcur;
			if (lcur < rcur) {
				ll = std::max(ll, lcur);
				++lcur; --rcur;
			}
		}
		rcur = ll + 1;
		for (int i = ll; ~i && rcur < SZ; --i) {
			const int tr = vec[i];
			while (rcur < SZ && vec[rcur] + tr < mid) ++rcur;
			if (rcur < SZ) {
				ret += BIG;
				vec[i] = vec[rcur] = 0;
				++rcur;
			}
		}
		for (int i = SZ - 1; ~i; --i) if (vec[i]) {
			ret += vec[i]; break;
		}
		vec.clear();
	}
	return ret;
}
int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	LL add = 0;
	for (int i = 1; i != n; ++i) {
		scanf("%d%d%d", &t1, &t2, &t3);
		addedge(t1, t2, t3);
		add += t3;
	}
	// mid = 15;
	// std::cerr << dp(1, 0) << std::endl;
 	LL l = 1, r = add, ans = 0;
 	while (l <= r) {
 		mid = l + r >> 1;
 		// std::cerr << "SOLVE " << l << " " << r << " " << mid << " " << std::endl;
 		if (dp(1, 0) / BIG >= m) l = mid + 1, ans = mid;
 		else r = mid - 1;
 	}
	printf("%lld\n", ans);
	return 0;
}
