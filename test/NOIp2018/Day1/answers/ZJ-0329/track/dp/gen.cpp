#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
	freopen("track.in", "w", stdout);
	srand(time(0));
	int n = rand() % 9 + 2, m = rand() % (n - 1) + 1;
	cout << n << " " << m << endl;
	int typ = rand() % 3;
	for (int i = 1; i != n; ++i) {
		cout << i + 1 << " " << (typ ? (typ == 1 ? rand() % i + 1 : i) : 1)  << " " << rand() % 10000 + 1 << endl; 
	}

	return 0;
}
