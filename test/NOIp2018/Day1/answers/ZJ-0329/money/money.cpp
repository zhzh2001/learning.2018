#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>

const int MAXA = 25010;
const int UP = 25000;
const int MAXN = 110;
int f1[MAXA], f2[MAXA], n, a[MAXN];
int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T --> 0) {
		memset(f1, 0, sizeof f1);
		memset(f2, 0, sizeof f2);
		scanf("%d", &n);
		f1[0] = 1;
		for (int i = 1; i <= n; ++i) {
			scanf("%d", &a[i]);
			for (int j = 0; j + a[i] <= UP; ++j)
				if (f1[j])
					f1[j + a[i]] = true;
		}
		f2[0] = 1;
		int ans = 0;
		for (int i = 1; i <= UP; ++i) if (f2[i] != f1[i]) {
			++ans; 
			for (int j = 0; j + i <= UP; ++j)
				if (f2[j])
					f2[j + i] = true;
		}
		printf("%d\n", ans);
	}
	return 0;
}
