#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <bitset>
using namespace std;
const int MAXN = 100010;
int a[MAXN];
bool f1[MAXN], f2[MAXN];
int main() {
	freopen("money.in", "r", stdin);
	freopen("money.usr", "w", stdout);
	int T; cin >> T;
	while (T --> 0) {
		memset(f1, 0, sizeof f1);
		int n; cin >> n;
		for (int i = 1; i <= n; ++i) cin >> a[i];
		f1[0] = true;
		for (int i = 1; i <= n; ++i)
			for (int j = 0; j + a[i] <= 100000; ++j)
				if (f1[j]) f1[j + a[i]] = true;
		const int U = 1 << n;
		int ans = n;
		for (int i = 1; i != U; ++i) {
			memset(f2, 0, sizeof f2);
			f2[0] = true;
			for (int j = 1; j <= n; ++j)
				if (i >> j - 1 & 1)
					for (int k = 0; k + a[j] <= 100000; ++k)
						if (f2[k]) f2[k + a[j]] = true;
			bool flag = true;
			for (int j = 0; j <= 100000; ++j) {
				flag &= f1[j] == f2[j];
				if (!flag) break;
			}
			if (flag) ans = std::min(ans, __builtin_popcount(i));
		}
		cout << ans << endl;
	}
	return 0;
}
