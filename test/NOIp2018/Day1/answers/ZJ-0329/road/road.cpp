#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>

const int MAXN = 100010;
int a[MAXN], n;
int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	int ans = 0;
	scanf("%d", &n);
	for (int i = 1; i <= n; ++i) scanf("%d", &a[i]);
	for (int i = n; i ; --i) ans += std::max(a[i] - a[i - 1], 0);
	printf("%d\n", ans);
	return 0;
}
