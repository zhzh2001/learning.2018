#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int a[100010];
int main() {
	freopen("road.in", "r", stdin);
	freopen("road.usr", "w", stdout);
	int n; cin >> n;
	for (int i = 1; i <= n; ++i) cin >> a[i];
	long long ans = 0;
	while (true) {
		bool can = false;
		for (int i = 1; i <= n; ++i) can |= a[i] > 0;
		if (!can) break;
		for (int i = 1; i <= n; ++i) if (a[i]) {
			int cur = i;
			while (a[cur]) --a[cur++];
			++ans;
			break;
		}
	}
	cout << ans << endl;
	return 0;
}

