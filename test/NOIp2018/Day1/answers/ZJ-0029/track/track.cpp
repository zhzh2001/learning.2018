#include<bits/stdc++.h>
#define N 500005
using namespace std;
multiset<int>S;
multiset<int>::iterator it;
int n,m,x,y,z,kk,g[N],p[N],dp[N],head[N];
struct Tree{int nxt,to,step;}e[N];
inline void link(int x,int y,int z){e[++kk].nxt=head[x];e[kk].to=y;e[kk].step=z;head[x]=kk;}
void dfs1(int u,int fa,int lim){
	dp[u]=g[u]=0;
	bool lev=1;
	for (int i=head[u];i;i=e[i].nxt){
		int v=e[i].to;
		if (v==fa) continue;
		lev=0;
		dfs1(v,u,lim);
	}
	if (lev) return;
	int cnt=0;S.clear();
	for (int i=head[u];i;i=e[i].nxt){
		int v=e[i].to;
		if (v==fa) continue;
		dp[u]+=dp[v];
		p[++cnt]=g[v]+e[i].step;
	}
	sort(p+1,p+cnt+1);
	int tmp=0;
	for (int i=1;i<=cnt;i++){
		if (p[i]<lim) S.insert(p[i]);
		else tmp++;
	}
	/*for (int i=cnt;i;i--){
		if (p[i]>=lim) continue;
		if (S.find(p[i])==S.end()) continue;
		S.erase(S.find(p[i]));
		int res=lim-p[i];
		it=S.lower_bound(res);
		if (it==S.end()){S.insert(p[i]);continue;}
		tmp++;S.erase(it);
	}*/
	for (int i=1;i<=cnt;i++){
		if (p[i]>=lim) continue;
		if (S.find(p[i])==S.end()) continue;
		S.erase(S.find(p[i]));
		int res=lim-p[i];
		it=S.lower_bound(res);
		if (it==S.end()){S.insert(p[i]);continue;}
		tmp++;S.erase(it);
	}
	dp[u]+=tmp;
	if (!S.size()){g[u]=0;return;}
	else if (S.size()==1){g[u]=*S.begin();return;}
	else {
		it=S.end();it--;
		g[u]=*it;return;
	}
}
inline bool check(int x){
	dfs1(1,-1,x);
	return dp[1]>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		link(x,y,z);link(y,x,z);
	}
	//check(26282);printf("%d %d\n",dp[1],g[1]);
	int l=1;int r=1e9;
	while (l+1<r){
		int mid=(l+r)>>1;
		if (check(mid)) l=mid;
		else r=mid;
	}
	if (check(r)) printf("%d\n",r);
	else printf("%d\n",l);
	return 0;
}
