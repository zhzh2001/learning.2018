#include<bits/stdc++.h>
#define N 200005
#define inf 1000000000
#define mk make_pair
#define pi pair<int,int>
using namespace std;
int T,n,kk,ans,a[N],b[N],f[N],dp[N];
struct node{int x,y;}p[N];
inline bool cmp(node aa,node bb){return aa.y<bb.y;}
inline void solve(){
	memset(b,0,sizeof(b));int tot=0;
	for (int i=0;i<a[1];i++) dp[i]=inf;
	for (int i=1;i<=n;i++)
		dp[a[i]%a[1]]=min(dp[a[i]%a[1]],a[i]);
	int cnt=0;
	for (int i=1;i<=n;i++){
		int tmp=a[i]%a[1];
		if (b[tmp]) continue;
		b[tmp]=1;tot++;
		p[++cnt].x=a[i]%a[1];
		p[cnt].y=dp[a[i]%a[1]];
	}
	sort(p+1,p+cnt+1,cmp);
	memset(f,127,sizeof(f));f[0]=a[1];
	for (int i=2;i<=cnt;i++){
		if (f[p[i].x]<=p[i].y){tot--;continue;}
		f[p[i].x]=p[i].y;
		for (int j=0;j<a[1];j++)
			if (f[j]<inf) f[(j+p[i].x)%a[1]]=min(f[(j+p[i].x)%a[1]],f[j]+p[i].y);
		for (int j=0;j<a[1];j++)
			if (f[j]<inf) f[(j+p[i].x)%a[1]]=min(f[(j+p[i].x)%a[1]],f[j]+p[i].y);
	}
	ans=min(ans,tot);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);ans=1e9;
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		solve();
		printf("%d\n",ans);
	}
	return 0;
}
