#include<bits/stdc++.h>
#define N 50005
using namespace std;
int sum,n,m,aa,bb;
int to[N<<1],nxt[N<<1],len[N<<1],head[N],cnt,x[N];
namespace Brute{//m=1
int dis[N]={0},tmp=0,ans=0;
	void dfs(int now,int f){
		for(int i=head[now];i;i=nxt[i])
			if(to[i]!=f){
				dis[to[i]]=dis[now]+len[i];
				dfs(to[i],now);
			}
	}
	inline void work(){
		dfs(1,0);
		for(int i=1;i<=n;i++)
			if(dis[i]>ans){
				ans=dis[i];
				tmp=i;
			}
		memset(dis,0,sizeof(dis));
		dfs(tmp,0);
		for(int i=1;i<=n;i++) ans=max(ans,dis[i]);
		printf("%d",ans);
	}
}
namespace Brute1{//juhua
	inline bool check(int mid){
		int vis[N]={0},pos=0,j,i=0;
		for(j=1;j<n;j++){
			if(vis[j]) continue;
			vis[j]=1;
			if(x[j]>=mid){i++;if(i>=m) return true;}
			pos=lower_bound(x+1,x+n,mid-x[j])-x;
			while(pos<n&&vis[pos]) pos++;
			if(pos>=n) return false;
			vis[pos]=1;i++;
			if(i>=m) return true;
		}
		return false;
	}
	inline void work(){
		for(int i=1;i<=cnt;i+=2) x[i/2+1]=len[i];
		sort(x+1,x+n);
		if(2*m<=n-1) printf("%d",x[n-m]+x[n-m-1]);
		else if(m==n-1) printf("%d",x[1]);
		else {
			int l=1,r=x[n-1]+x[n-2];
			while(l<=r){
				int mid=(l+r)>>1;
				if(check(mid)) l=mid+1;
				else r=mid-1;
			}
			printf("%d",r);
		}
	}
}
namespace Brute2{//line
	inline bool check(int mid){
		int tmp=0,c=0;
		for(int i=1;i<n;i++){
			tmp+=x[i];
			if(tmp>=mid) tmp=0,c++;
			if(c>=m) return true;
		}
		return false;
	}
	inline void work(){
		for(int i=1;i<=cnt;i+=2) x[i/2+1]=len[i];
		int l=1,r=sum;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid)) l=mid+1;
			else r=mid-1;
		}
		printf("%d",r);
	}
}
inline void add_edge(int a,int b,int c){
	nxt[++cnt]=head[a];
	to[cnt]=b;
	len[cnt]=c;
	head[a]=cnt;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1,a,b,c;i<n;i++){
		scanf("%d%d%d",&a,&b,&c);
		if(a==1) aa++;
		if(b==a+1) bb++;
		sum+=c;
		add_edge(a,b,c);
		add_edge(b,a,c);
	}
	if(m==1) Brute::work();
	else if(aa==n-1) Brute1::work();
	else if(bb==n-1) Brute2::work();
	else Brute::work();
}
