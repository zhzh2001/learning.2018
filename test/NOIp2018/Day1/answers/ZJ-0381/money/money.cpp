#include<bits/stdc++.h>
#define N 105
using namespace std;
int T,n,a[N],b[N],sc,c[N],cc,flg,f[100000];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		sc=0;cc=0;
		memset(b,0,sizeof(b));
		memset(c,0,sizeof(c));
		memset(f,0,sizeof(f));
		scanf("%d",&n);
		for(int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		for(int i=1;i<=n;i++){
			flg=0;
			for(int j=1;j<i;j++)
				if(a[i]%a[j]==0){flg=1;break;}
			if(!flg) b[++sc]=a[i];
		}
		f[0]=1;
		for(int i=1;i<=sc;i++)
			if(!f[b[i]]){
				c[++cc]=b[i];
				for(int j=b[i];j<=b[sc];j++)
					if(f[j-b[i]]) f[j]=1;
			}
		printf("%d\n",cc);
	}
}
