#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
template<class T>inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(; isdigit(ch);ch=getchar())x=x*10+(ch-48);
	x*=f;
}
inline void Setfile(string Name="") {
	if(Name=="")
		return;
	freopen((Name+".in").c_str(),"r",stdin);
	freopen((Name+".out").c_str(),"w",stdout);
}
int T,n,val[105],vis[105];
inline int gcd(int x,int y) {
	return y?gcd(y,x%y):x;
}
bitset<25001>show;
int main() {
	Setfile("money");
	read(T);
	while(T--) {
		read(n);
		int mx=0;
		for(int i=1;i<=n;++i)
			read(val[i]),mx=max(mx,val[i]);
		sort(val+1,val+n+1);
		int ans=0;
		show.reset(),show[0]=1;
		for(int i=1;i<=n;++i)
			if(!show[val[i]]) {
				++ans;
				for(int j=val[i];j<=mx;j+=val[i])
					if(!show[j])
						show|=show<<j;
			}
		printf("%d\n",ans);
	}
	return 0;
}
