#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
template<class T>inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(; isdigit(ch);ch=getchar())x=x*10+(ch-48);
	x*=f;
}
inline void Setfile(string Name="") {
	if(Name=="")
		return;
	freopen((Name+".in").c_str(),"r",stdin);
	freopen((Name+".out").c_str(),"w",stdout);
}
typedef pair<int,int> pii;
#define fi first
#define se second
const int maxn=100005;
int n,vis[maxn],lst,ans,cnt=1;
pii vec[maxn];
inline void add(int pos) {
	cnt++;
	if(vis[pos-1])
		cnt--;
	if(vis[pos+1])
		cnt--;
	vis[pos]=1;
}
int main() {
	Setfile("road");
	read(n);
	for(int i=1;i<=n;++i)
		read(vec[i].fi),vec[i].se=i;
	vis[0]=vis[n+1]=1;
	sort(vec+1,vec+n+1);
	for(int i=1,j;i<=n;++i) {
		ans+=cnt*(vec[i].fi-lst);
		for(j=i;j<=n&&vec[j].fi==vec[i].fi;++j)
			add(vec[j].se);
		i=j-1,lst=vec[i].fi;
	}
	printf("%d\n",ans);
	return 0;
}
