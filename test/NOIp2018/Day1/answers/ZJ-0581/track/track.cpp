#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
template<class T>inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(; isdigit(ch);ch=getchar())x=x*10+(ch-48);
	x*=f;
}
inline void Setfile(string Name="") {
	if(Name=="")
		return;
	freopen((Name+".in").c_str(),"r",stdin);
	freopen((Name+".out").c_str(),"w",stdout);
}
const int maxn=50005;
int n,m,cnt,
lim;
map<int,int>st[maxn];
#define fi first
#define se second
struct Edge {
	int u,v,w;
	Edge(int u=0,int v=0,int w=0):u(u),v(v),w(w){}
};
vector<Edge>edges;
vector<int>G[maxn];
inline void add(int u,int v,int w) {
	edges.push_back(Edge(u,v,w));
	G[u].push_back(edges.size()-1);
	edges.push_back(Edge(v,u,w));
	G[v].push_back(edges.size()-1);
}
inline void dfs(int u=1,int p=0) {
	for(int i=0;i<(int)G[u].size();++i) {
		Edge &e=edges[G[u][i]];
		int v=e.v,w=e.w;
		if(v==p)
			continue;
		dfs(v,u);
		if(cnt>=m)
			break;
		int tmp=(*st[v].rbegin()).fi+w;
		st[v].clear();
		if(tmp>=lim) {
			cnt++;
			continue;
		}
		++st[u][tmp];
	}
	for(map<int,int>::iterator it=st[u].begin(),ti;it!=st[u].end();++it) {
		if(cnt>=m)
			break;
		while(1) {
			int tmp=lim-(*it).fi;
			ti=st[u].lower_bound(tmp);
			if(it==ti&&(*it).se<2)
				++ti;
			if(ti==st[u].end())
				 break;
			++cnt;
			--(*ti).se;
			if(!(*ti).se)
				st[u].erase(ti);
			--(*it).se;
			if(!(*it).se) {
				st[u].erase(it);
				break;
			}
			if(cnt>=m)
				break;
		}
		if(cnt>=m)
			break;
	}
	++st[u][0];
}
inline int check(int x) {
	cnt=0,lim=x;
	//cerr<<"!!!"<<lim<<endl;
	for(int i=1;i<=n;++i)
		st[i].clear();
	dfs();
	return cnt>=m;
}
int main() {
	Setfile("track");
	read(n),read(m);
	for(int i=1,x,y,z;i<n;++i)
		read(x),read(y),read(z),add(x,y,z);
	int l=1,r=6e8,ans=0;
	while(l<=r) {
		int mid=(l+r)>>1;
		if(check(mid))
			l=mid+1,ans=mid;
		else
			r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
