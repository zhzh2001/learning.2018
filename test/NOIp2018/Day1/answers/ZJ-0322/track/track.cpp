#include<bits/stdc++.h>
#define N 50005
using namespace std;
int dep[N],head[N],w[N],pd[N],Ans,s,n,m,cnt;
struct troye{ int to,nxt,vl;}E[N*2];
void add(int u,int v,int w){ E[++cnt].to=v,E[cnt].nxt=head[u],head[u]=cnt,E[cnt].vl=w;}
void DFS(int x,int fa){
	if (dep[x]>Ans){
		s=x;
		Ans=dep[x];
	}
	for (int i=head[x];i;i=E[i].nxt){
		int v=E[i].to;
		if (v==fa) continue;
		dep[v]=dep[x]+E[i].vl;
		DFS(v,x);
	}
}
void casea(){
	Ans=s=0;
	for (int i=0;i<=n;i++) dep[i]=0;
	DFS(1,0);
	Ans=0;
	for (int i=0;i<=n;i++) dep[i]=0;
	DFS(s,0);
	printf("%d\n",Ans);
}
bool Try(int x){
	int T=0;
	for (int i=1;i<n;i++) pd[i]=0;
	int i=n-1,j=1;
	while (w[i]>=x){
		T++,i--;
		if (T>=m) return true;
	}
	while (i>j){
		while ((w[j]+w[i]<x) && j<i) j++;
		if (i>j) T++,i--,j++;
		if (T>=m) return true;
	}
	if (T>=m) return true;
	return false;
}
void caseb(){
	sort(w+1,w+n);
	int np=w[n-1];
	int L=1,R=np*2;
	while (L<R){
		int M=(L+R+1)/2;
		if (Try(M)) L=M;else R=M-1;
	}
	printf("%d\n",L);
}
int main(){
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool F=true;
	for (int i=1;i<n;i++){
		int u,v;
		scanf("%d%d%d",&u,&v,&w[i]);
		add(u,v,w[i]),add(v,u,w[i]);
		if (u!=1) F=false;
	}
	if (m==1) casea();
	else if (F) caseb();
}
/*
8 3
1 2 2
1 3 8
1 4 1
1 5 9
1 6 6
1 7 3
1 8 2
*/
