#include<bits/stdc++.h>
#define N 1005
using namespace std;
bool a[N],b[N],pd[N];
int T,n,np,Ans,c[N],d[N];
bool Try(){
	for (int i=0;i<=np;i++) b[i]=0;
	b[0]=1;
	for (int i=1;i<=np;i++)
		for (int j=1;j<=n;j++)
			if (pd[j]){
				if (i-c[j]>=0 && b[i-c[j]]){
					b[i]=1;
					if (!a[i]) return false;
				}
			}
	for (int i=1;i<=np;i++)
		if (b[i]!=a[i]) return false;
	return true;
}
void DFS(int t,int m){
	if (t>n){
		if (Try()) Ans=min(Ans,m);
		return;
	}
	for (int i=0;i<=1;i++){
		if (i==1){
			pd[t]=1,DFS(t+1,m+1),pd[t]=0;
		}else DFS(t+1,m);
	}
}
void pre(){
	for (int i=0;i<=np;i++) a[i]=0;
	a[0]=1;
	for (int i=1;i<=np;i++)
		for (int j=1;j<=n;j++)
			if (i-c[j]>=0 && a[i-c[j]]) a[i]=1;
}
void casea(){
	Ans=1000000000;
	pre();
	DFS(1,0);
	printf("%d\n",Ans);
}
void caseb(){
	sort(c+1,c+n+1);
	int m=0;
	for (int i=0;i<=np;i++) b[i]=0;
	for (int i=1;i<=n;i++){
		bool F=false;
		for (int j=1;j<=c[i];j++)
			if (c[i]%j==0 && b[j]) { F=true;break;}
		if (!F) d[++m]=c[i];
		b[c[i]]=1;
	}
	np=0;
	for (int i=1;i<=m;i++) c[i]=d[i],np=max(np,d[i]);
	if (c[1]>=21){ printf("%d\n",m);return;}
	n=m;
	casea();
}
int main(){
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		np=0;
		for (int i=1;i<=n;i++) scanf("%d",&c[i]),np=max(np,c[i]),pd[i]=0;
		if (n<=13) casea();
		else if (n<=25 && np<=40) caseb();
	}
}
