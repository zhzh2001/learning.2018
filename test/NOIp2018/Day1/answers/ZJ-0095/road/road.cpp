#include <bits/stdc++.h>
#define rep(i, n) for (rint i = 1; i <= (n); i ++)
#define re0(i, n) for (rint i = 0; i < (int) n; i ++)
#define travel(i, u) for (rint i = head[u]; i; i = e[i].nxt)
#define rint register int
using namespace std;

template <class T> inline void read(T &x) {
  x = 0; char c = getchar(); int f = 0;
  for (; c < '0' || c > '9'; f |= c == '-', c = getchar());
  for (; c >= '0' && c <= '9'; x = x * 10 + c - '0', c = getchar());
  if (f) x = -x;
}

#define int long long
const int N = 3e5 + 233;
int n, a[N];

signed main(void) {
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
  read(n);
  rep (i, n) read(a[i]);
  int ans = a[1];
  for (int i = 2; i <= n; i ++)
    if (a[i] > a[i - 1])
      ans += a[i] - a[i - 1];
  cout << ans << "\n";
}
