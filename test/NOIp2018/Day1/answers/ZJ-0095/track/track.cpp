#include <bits/stdc++.h>
#define rep(i, n) for (rint i = 1; i <= (n); i ++)
#define re0(i, n) for (rint i = 0; i < (int) n; i ++)
#define travel(i, u) for (rint i = head[u]; i; i = e[i].nxt)
#define rint register int
using namespace std;

template <class T> inline void read(T &x) {
  x = 0; char c = getchar(); int f = 0;
  for (; c < '0' || c > '9'; f |= c == '-', c = getchar());
  for (; c >= '0' && c <= '9'; x = x * 10 + c - '0', c = getchar());
  if (f) x = -x;
}
const int inf = 50000 * 10000 + 233;
const int N = 3e5 + 233;
struct E {
  int nxt, to, w;
}e[N << 2];
int head[N], e_cnt = 0;
int n, m, mi;
int g[N], f[N];

inline void add(int x, int y, int w) {
  e[++ e_cnt] = (E) {head[x], y, w}; head[x] = e_cnt;
}

inline void U(int &x, int y) {
  if (x < y) x = y;
}

pair <int, int> gao(vector <int> son) {
  int n = son.size();
  if (!n) return make_pair(0, 0);
  sort(son.begin(), son.end());
  pair <int, int> ans(0, 0);
  {
    int ban = -1;
    rint na = 0, nm = 0;
    vector <int> ts;
    re0 (k, n) if (k != ban) ts.push_back(son[k]);
    rint r = ts.size() - 1, l = 0;
    while (l < r) {
      while (l < r && ts[l] + ts[r] < mi) ++ l;
      if (l == r) break;
      else ++ na, -- r, ++ l;
    }
    ans = max(ans, make_pair(na, nm));
  }
  
  for (rint ban = n - 1; ban >= 0; -- ban) {
    rint na = 0, nm = ban == -1 ? 0 : son[ban];
    vector <int> ts;
    re0 (k, n) if (k != ban) ts.push_back(son[k]);
    rint r = ts.size() - 1, l = 0;
    while (l < r) {
      while (l < r && ts[l] + ts[r] < mi) ++ l;
      if (l == r) break;
      else ++ na, -- r, ++ l;
    }
    if (na == ans.first) {
      ans.second = nm;
      break;
    }
  }
  return ans;
}

inline void dfs(int u, int fat) {
  g[u] = 0; f[u] = 0;
  vector <int> son; int tot = 0;
  travel (i, u) {
    int v = e[i].to;
    if (v != fat) {
      dfs(v, u);
      if (f[v] + e[i].w >= mi) ++ tot;
      else son.push_back(f[v] + e[i].w);
      tot += g[v];
    } 
  }
  pair <int, int> tmp = gao(son);
  // cout << u << " " << tot << " " << tmp.first << " " << tmp.second << "\n";
  f[u] = tmp.second; g[u] = tot + tmp.first;
}

inline int cal(int _mi) {
  mi = _mi; dfs(1, 0);
  return g[1];
}

int ans = 0;
inline int dd(int u, int fat) {
  int mx = 0, se = 0;
  travel (i, u) {
    int v = e[i].to;
    if (v != fat) {
      int p = dd(v, u) + e[i].w;
      if (p >= mx) se = mx, mx = p;
      else if (p >= se) se = p;
    }
  }
  ans = max(ans, mx + se);
  return mx;
}

int main(void) {
  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
  read(n); read(m);
  rep (i, n - 1) {
    int x, y, w; read(x); read(y); read(w);
    add(x, y, w); add(y, x, w);
  }
  if (m == 1) {
    dd(1, 0);
    cout << ans << "\n";
    return 0;
  }
  int l = 0, r = inf;
  while (l < r) {
    int mid = l + (r - l + 1) / 2;
    if (cal(mid) >= m) l = mid;
    else r = mid - 1;
  }
  cout << l << "\n";
}


/*
  6 5
  1 2 1
  2 3 3
  2 4 1
  3 6 2
  3 5 2
  4 1
  1 2 10
  1 3 7
  1 4 4
*/
