#include <bits/stdc++.h>
#define rep(i, n) for (rint i = 1; i <= (n); i ++)
#define re0(i, n) for (rint i = 0; i < (int) n; i ++)
#define travel(i, u) for (rint i = head[u]; i; i = e[i].nxt)
#define rint register int
using namespace std;

template <class T> inline void read(T &x) {
  x = 0; char c = getchar(); int f = 0;
  for (; c < '0' || c > '9'; f |= c == '-', c = getchar());
  for (; c >= '0' && c <= '9'; x = x * 10 + c - '0', c = getchar());
  if (f) x = -x;
}
namespace one {
  const int N = 3e5 + 233;
  int n, a[N];
  bool f[N];

  inline void init(void) {
    memset(f, 0, sizeof f);
  }

  void main(void) {
    init();
    read(n);
    rep (i, n) read(a[i]);
    sort(a + 1, a + n + 1);
    rint mx = *max_element(a + 1, a + n + 1);
    rint ans = 0;
    f[0] = 1;
    rep (i, n) {
      if (!f[a[i]]) {
	++ ans;
	for (rint p = 0; p + a[i] <= mx; p ++) 
	  f[p + a[i]] |= f[p];
      }
    }
    cout << ans << "\n";
  }

}

int main(void) {
  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
  int T; for (read(T); T --; one::main());
}
