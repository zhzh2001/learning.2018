#include <bits/stdc++.h>
#define range(i,c,o) for(register int i=(c);i<(o);++i)
using namespace std;

// QIO BEGIN
inline unsigned getu()
{
	char c; unsigned r=0;
	for(;!isdigit(c)&&c!=EOF;c=getchar());
	for(;isdigit(c);c=getchar()) r=(r<<3)+(r<<1)+c-'0';
	return r;
}
// QIO END

static int n,N;
int seg[270000],lef[270000],rig[270000],idx[100005];

inline bool cmp(const int&x,const int&y)
{
	return seg[N+x]==seg[N+y]?x<y:seg[N+x]<seg[N+y];
}

void decrease(int rt,int l,int r,int lb,int rb,int x)
{
	if(l>=lb&&r<=rb) return void(seg[rt]-=x);
	int mid=l+r>>1;
	if(lb<=mid) decrease(rt<<1  , l ,mid,lb,rb,x);
	if(rb> mid) decrease(rt<<1|1,mid+1,r,lb,rb,x);
}

inline int query(const int&x)
{
	int ret=0;
	for(int i=N+x;i;i>>=1) ret+=seg[i];
	return ret;
}

void modify_lef(int rt,int l,int r,int lb,int rb,int x)
{
	if(l>=lb&&r<=rb) return void(lef[rt]=max(lef[rt],x));
	int mid=l+r>>1;
	if(lb<=mid) modify_lef(rt<<1  , l ,mid,lb,rb,x);
	if(rb> mid) modify_lef(rt<<1|1,mid+1,r,lb,rb,x);
}

inline int query_lef(const int&x)
{
	int ret=0;
	for(int i=N+x;i;i>>=1) ret=max(ret,lef[i]);
	return ret;
}

void modify_rig(int rt,int l,int r,int lb,int rb,int x)
{
	if(l>=lb&&r<=rb) return void(rig[rt]=min(rig[rt],x));
	int mid=l+r>>1;
	if(lb<=mid) modify_rig(rt<<1  , l ,mid,lb,rb,x);
	if(rb> mid) modify_rig(rt<<1|1,mid+1,r,lb,rb,x);
}

inline int query_rig(const int&x)
{
	int ret=n-1;
	for(int i=N+x;i;i>>=1) ret=min(ret,rig[i]);
	return ret;
}

int main()
{
	freopen("road.in" ,"r",stdin );
	freopen("road.out","w",stdout);
	for(n=getu(),N=1;N<n;N<<=1);
	range(i,0,n) seg[N+i]=getu(),idx[i]=i;
	range(i,0,N+n) lef[i]=0,rig[i]=n-1;
	sort(idx,idx+n,cmp);
	int ans=0;
	range(i,0,n)
	{
		int cur=query(idx[i]),l=query_lef(idx[i]),r=query_rig(idx[i]);
		if(cur>0) ans+=cur,decrease(1,0,N-1,l,r,cur);
		if(idx[i]<r) modify_lef(1,0,N-1,idx[i]+1,r,idx[i]+1);
		if(l<idx[i]) modify_rig(1,0,N-1,l,idx[i]-1,idx[i]-1);
	}
	return printf("%d\n",ans),0;
}
