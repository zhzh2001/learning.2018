#include <bits/stdc++.h>
#define range(i,c,o) for(register int i=(c);i<(o);++i)
using namespace std;

static int T,n;
int a[105],b[105];
bool f[25005];

int main()
{
	freopen("money.in" ,"r",stdin );
	freopen("money.out","w",stdout);
	for(scanf("%d",&T);T--;)
	{
		scanf("%d",&n);
		range(i,0,n) scanf("%d",a+i); sort(a,a+n);
		int N=0;
		range(i,0,n)
		{
			bool flag=1;
			range(j,0,N) if(a[i]%b[j]==0) {flag=0; break;}
			if(flag) b[N++]=a[i];
		}
		memset(f,0,sizeof f),f[0]=1; int ans=0;
		range(i,0,N) if(!f[b[i]])
		{
			++ans;
			range(j,b[i],b[N-1]+1) f[j]|=f[j-b[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
