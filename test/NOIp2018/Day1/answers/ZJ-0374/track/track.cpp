#include <bits/stdc++.h>
#define range(i,c,o) for(register int i=(c);i<(o);++i)
using namespace std;

// QIO BEGIN
inline unsigned getu()
{
	char c; unsigned r=0;
	for(;!isdigit(c)&&c!=EOF;c=getchar());
	for(;isdigit(c);c=getchar()) r=(r<<3)+(r<<1)+c-'0';
	return r;
}
// QIO END

static int n,m;
int a[50005],b[50005],l[50005];

namespace phi
{
	int f[50005];
	vector< pair<int,int> > edg[50005];
	queue<int> que;
	int BFS(const int&S)
	{
		for(memset(f,0,sizeof f);!que.empty();que.pop());
		for(que.push(S);!que.empty();que.pop())
		{
			int x=que.front();
			range(i,0,edg[x].size())
			{
				int y=edg[x][i].first,det=edg[x][i].second;
				if(!f[y]&&y!=S) f[y]=f[x]+det,que.push(y);
			}
		}
		int ret=0;
		range(i,1,n+1) if(f[i]>f[ret]) ret=i;
		return ret;
	}
	
	inline bool judge() {return m==1;}
	int solve()
	{
		memset(edg,0,sizeof edg);
		range(i,1,n)
		{
			edg[a[i]].push_back(make_pair(b[i],l[i]));
			edg[b[i]].push_back(make_pair(a[i],l[i]));
		}
		return printf("%d\n",f[BFS(BFS(1))]),0;
	}
}

namespace flower
{
	inline bool check(const int&x)
	{
		int i=1,j=n-1,cnt=0;
		for(;j&&l[j]>=x;--j) ++cnt;
		for(;i<j;--j)
		{
			for(;i<j&&l[i]+l[j]<x;++i);
			cnt+=i++<j;
		}
		return cnt>=m;
	}
	
	inline bool judge()
	{
		range(i,1,n) if(a[i]!=1) return 0;
		return 1;
	}
	int solve()
	{
		sort(l+1,l+n);
		int low=l[1],hig=0; range(i,1,n) hig+=l[i];
		while(low<hig)
		{
			int mid=low+hig+1>>1;
			check(mid)?low=mid:hig=mid-1;
		}
		return printf("%d\n",low),0;
	}
}

namespace chain
{
	int idx[50005];
	inline bool cmp(const int&x,const int&y) {return a[x]<a[y];}
	inline bool check(const int&x)
	{
		int s=0,cnt=0;
		range(i,1,n) if((s+=l[idx[i]])>=x) s=0,++cnt;
		return cnt>=m;
	}
	
	inline bool judge()
	{
		range(i,1,n) if(a[i]+1!=b[i]) return 0;
		return 1;
	}
	int solve()
	{
		range(i,1,n) idx[i]=i; sort(idx+1,idx+n,cmp);
		int low=l[1],hig=0;
		range(i,1,n) low=min(low,l[i]),hig+=l[i];
		while(low<hig)
		{
			int mid=low+hig+1>>1;
			check(mid)?low=mid:hig=mid-1;
		}
		return printf("%d\n",low),0;
	}
}

int main()
{
	freopen("track.in" ,"r",stdin );
	freopen("track.out","w",stdout);
	n=getu(),m=getu();
	range(i,1,n) a[i]=getu(),b[i]=getu(),l[i]=getu();
	if(phi   ::judge()) return phi   ::solve();
	if(flower::judge()) return flower::solve();
	if(chain ::judge()) return chain ::solve();
	return 0;
}
