#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdio>
#include<cmath>
#define ll long long
#define rep(i,j,k) for(int i=j;i<=k;++i)
#define drp(i,j,k) for(int i=j;i>=k;--i)
#define cross(i,x) for(int i=h[x];i;i=e[i].n)
using namespace std;
char buf[25];
const int N=110;
const int M=25010;
int n,MAX,ans;
int a[N];
bool b[M];
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+(ch^48);ch=getchar();}
	return x*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}
	if(x==0){putchar('0');return;}
	int cnt=0;
	while(x){buf[++cnt]='0'+x%10;x/=10;}
	drp(i,cnt,1) putchar(buf[i]);
}
inline void solve(){
	n=read();MAX=0;ans=0;
	rep(i,1,n) a[i]=read(),MAX=max(MAX,a[i]);
	sort(a+1,a+1+n);
	memset(b,false,sizeof(b));b[0]=true;
	rep(i,1,n){
		int x=a[i];
		if(b[x]==false){
			++ans;
			rep(j,0,MAX-x)
				if(b[j]) b[j+x]=true;
		}
	}
	write(ans);putchar('\n');
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t=read();
	rep(tt,1,t) solve();
	return 0;
}
