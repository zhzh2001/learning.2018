#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<queue>
#define ll long long
#define rep(i,j,k) for(int i=j;i<=k;++i)
#define drp(i,j,k) for(int i=j;i>=k;--i)
#define cross(i,x) for(int i=h[x];i;i=e[i].n)
using namespace std;
char buf[25];
const int N=50010;
struct edge{
	int n,t,v;
} e[N*2];
int h[N],f[N],dis[N],d[N];
int n,m,cnt,MAX,ans;
queue<int>tq;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+(ch^48);ch=getchar();}
	return x*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}
	if(x==0){putchar('0');return;}
	int cnt=0;
	while(x){buf[++cnt]='0'+x%10;x/=10;}
	drp(i,cnt,1) putchar(buf[i]);
}
inline void addedge(int x,int y,int z){
	e[++cnt]=(edge){h[x],y,z};h[x]=cnt;
}
int dfs(int x,int v){
	int cnt=0;int q[4];
	cross(i,x){
		int y=e[i].t,z=e[i].v;
		if(y!=f[x]){
			f[y]=x;
			dis[y]=z;
			int tv=dfs(y,v);
			if(tv>=v) ++ans;
				else q[++cnt]=tv;
		}
	}
	if(cnt==0) return dis[x];
	if(cnt==1) return dis[x]+q[1];
	if(cnt==2)
		if(q[1]+q[2]>=v){
			++ans;
			return dis[x];
		}
		else return max(q[1],q[2])+dis[x];
	if(cnt==3){
		if(q[1]+q[2]>=v||q[2]+q[3]>=v||q[1]+q[3]>=v) ++ans;
		return 0;
	}
}
inline bool pd(int x){
	ans=0;
	ll tmp=dfs(1,x);
	if(ans>=m) return true;
		else return false;
}
inline int serch(){
	int l=0,r=MAX;
	while(l<r){
		int mid=(l+r+1)/2;
		if(pd(mid)) l=mid;
			else r=mid-1;
	}
	return l;
}
inline void bfs(int x){
	rep(i,1,n) d[i]=0;
	tq.push(x);
	while(!tq.empty()){
		int tx=tq.front();tq.pop();
		cross(i,tx){
			int y=e[i].t,z=e[i].v;
			if(y!=x&&d[y]==0){
				d[y]=d[tx]+z;
				tq.push(y);
			}
		}
	}
}
inline int find(){
	bfs(1);
	int k=1;
	rep(i,1,n) if(d[i]>d[k]) k=i;
	bfs(k);
	rep(i,1,n) if(d[i]>d[k]) k=i;
	return d[k];
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	rep(i,1,n-1){
		int x=read(),y=read(),z=read();
		addedge(x,y,z);addedge(y,x,z);
		MAX=MAX+z;
	}
	if(m==1) write(find());
		else write(serch());
	return 0;
}
