#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdio>
#include<cmath>
#define ll long long
#define rep(i,j,k) for(int i=j;i<=k;++i)
#define drp(i,j,k) for(int i=j;i>=k;--i)
#define cross(i,x) for(int i=h[x];i;i=e[i].n)
using namespace std;
char buf[25];
const int N=100010;
int n,tail;
int a[N],q[N],p[N];
ll sum;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+(ch^48);ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if(x<0){putchar('-');x=-x;}
	if(x==0){putchar('0');return;}
	int cnt=0;
	while(x){buf[++cnt]='0'+x%10;x/=10;}
	drp(i,cnt,1) putchar(buf[i]);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=read(),p[i]=0;
	q[0]=0;tail=0;
	rep(i,1,n){
		while(tail>0&&a[i]<q[tail]) --tail;
		p[i]=q[tail];
		q[++tail]=a[i];
	}
	q[0]=0;tail=0;
	drp(i,n,1){
		while(tail>0&&a[i]<=q[tail]) --tail;
		p[i]=max(p[i],q[tail]);
		q[++tail]=a[i];
	}
	/*rep(i,1,n){
			write(a[i]),putchar(' '),write(p[i]),putchar('\n');
		}*/
	rep(i,1,n) sum=sum+a[i]-p[i];
	write(sum);putchar('\n');
	return 0;
}
