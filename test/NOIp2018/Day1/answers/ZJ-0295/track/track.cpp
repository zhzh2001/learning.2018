#include<cstdio>
#include<algorithm>
using namespace std;
struct edge
{
	int eto,nxt,ew;
};
edge e[100000];
int n,m,fst[50001];
void addedge(int x,int y,int z,int i)
{   
    e[i].eto=y;
    e[i].ew=z;
    e[i].nxt=fst[x];
	fst[x]=i;
}
void solve_pt1()
{   int a[50001];
	for (int i=0;i<=n-2;i++)
	  a[i]=e[2*i].ew;
	sort(a,a+n-1);
    int l=a[0],r=a[n-2]*(n-1);
    while (l<=r)
    {
    	int pmid=(l+r)/2,cur=0,i=0,j=n-2;
    	while (cur<m && i<=j)
    	{
    		if (a[j]>=pmid) {j--; cur++;}
    		else
    		{   int k=a[j];
    			j--;
    			while (k<pmid && i<=j)
    			{   k+=a[i];
    				i++;
				}
				if (k>=pmid) cur++;
			}
		}
		if (cur==m) l=pmid+1; else r=pmid-1;
	}
	printf("%d\n",r);
}
void solve_pt2()
{   int a[50001];
	for (int i=0;i<=n-2;i++)
	  a[i]=e[2*i].ew;
	int l=a[0],r=a[0];
	for (int i=1;i<=n-2;i++)
	{
		if (a[i]<l) l=a[i];
		if (a[i]>r) r=a[i];
	}
	r*=n-1;
    while (l<=r)
    {
    	int pmid=(l+r)/2,cur=0,i=0;
    	while (cur<m && i<=n-2)
    	{  
    	    int k=a[i];
    		i++;
            while (k<pmid && i<=n-2)
    			{   k+=a[i];
    				i++;
				}
			if (k>=pmid) cur++;
		}
		if (cur==m) l=pmid+1; else r=pmid-1;
	}
	printf("%d\n",r);
}
int main()
{   freopen("track.in","r",stdin);
    freopen("track.out","w",stdout);
    scanf("%d%d",&n,&m);
	bool is_pt1=true,is_pt2=true;
	for (int i=0;i<=n-2;i++)
	{ int x,y,z;
	  scanf("%d%d%d",&x,&y,&z);	
	  addedge(x,y,z,2*i);
	  addedge(y,x,z,2*i+1);
	  if (x!=1) is_pt1=false;
	  if (y!=x+1) is_pt2=false;
    }
    if (is_pt1) solve_pt1();
    else if (is_pt2) solve_pt2();
    fclose(stdin);
    fclose(stdout);
    return 0;
}
