#include<bits/stdc++.h>
#define maxn 500005
#define ll long long
//#define kcz
using namespace std;
inline int read()
{
	int lin = 0,f = 1;
	char x = getchar();
	while(x < '0' || x > '9')
	{
		if(x == '-') f = -1;
		x = getchar();
	}
	while(x >= '0' && x <= '9')
	{
		lin = (lin << 1) + (lin << 3) + x - '0';
		x = getchar();
	}
	return lin * f;
}
#define PII pair <int,int>
#define fir first
#define sec second
#define ma(a,b) make_pair(a,b)
int n,h[maxn];
ll ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n = read();
	for(int i = 1; i <= n; i++)
		h[i] = read();
	for(int i = 1; i <= n; i++)
	{
		if(h[i] > h[i - 1])
			ans += h[i] - h[i - 1];
	}
	cout << ans;
	return 0;
}

