#include<bits/stdc++.h>
#define maxn 500005
//#define kcz
using namespace std;
inline int read()
{
	int lin = 0,f = 1;
	char x = getchar();
	while(x < '0' || x > '9')
	{
		if(x == '-') f = -1;
		x = getchar();
	}
	while(x >= '0' && x <= '9')
	{
		lin = (lin << 1) + (lin << 3) + x - '0';
		x = getchar();
	}
	return lin * f;
}
#define PII pair <int,int>
#define fir first
#define sec second
#define ma(a,b) make_pair(a,b)
#define inf 123123123
int head[maxn],tot,u,v,val,n,m,deg[maxn];
int len[maxn];
int f[maxn][2],g[maxn],fa2 = 1,fl1 = 1;
struct st{
	int v,nex,val;
}s[maxn];
void add(int u,int v,int val)
{
	++deg[u];
	s[++tot] = (st) { v,head[u],val };
	head[u] = tot;
}
void dfs_er(int now,int fa,int val)
{
	int l = 0,r = 0,val_l = 0,val_r = 0;
	for(int i = head[now]; i; i = s[i].nex)
		if(s[i].v != fa)
		{
			if(!l) { l = s[i].v; val_l = s[i].val; }
			else { r = s[i].v; val_r = s[i].val; }
			dfs_er(s[i].v,now,val);
		}
	if(!l)
	{
		f[now][0] = 0;
		f[now][1] = 0;
		g[now] = 0;
		return;
	}
	if(!r)
	{
		f[now][0] = f[l][0] + (val_l >= val);//没有向上伸展 
		f[now][1] = f[l][0] + (val_l >= val);
		g[now] = (val_l >= val ? 0 : (val_l + g[l]) >= val ? 0 : (val_l + g[l]));
		
		
		if(g[l] + val_l >= val)
		{
			f[now][0] = max(f[now][0],f[l][1] + 1);
		}
		else
		{
			if(f[now][1] < f[l][1])
			{
				f[now][1] = f[l][1];
				g[now] = g[l] + val_l;
			}
		}
	}
	/*if(l && r)
	{
		//先解决没有向上伸展的f[now][0] 
		f[now][0] = f[l][0] + (val_l >= val) + f[r][0] + (val_r >= val);
		if(g[l] + val_l >= val)
		{
			f[now][0] = max(f[now][0],f[l][1] + 1 + f[r][0]);
		}
		if(g[r] + val_r >= val)
		{
			f[now][0] = max(f[now][0],f[l][0] + f[r][1] + 1);
		}
		if(g[l] + val_l >= val && g[r] + val_r >= val)
		{
			f[now][0] = max(f[now][0],f[l][1] + f[r][1] + 2);
		}
		if(g[l] + g[r] + val_l + val_r >= val)
		{
			f[now][0] = max(f[now][0],f[l][1] + f[r][1] + 1);
		}
		
		f[now][1] = f[l][0] + f[r][0];
		g[now] = max(val_l,val_r);
		if(val_l >= val)
		{
			++f[now][1];
			g[now] = val_r;
		}
		if(val_r >= val)
		{
			++f[now][1];
			g[now] = val_l;
		}
		if(val_l >= val && val_r >= val) g[now] = 0;
		
		if(g[l] + val_l >= val)
		{
			if(f[now][1] < f[l][1])
		}
		
	}*/
}
int check(int val)
{
	memset(f,0,sizeof(f));
	memset(g,0,sizeof(g));
	dfs_er(1,1,val);
	if(f[1][0] >= m) return 1;
	return 0;
}

int main(){
	n = read(); m = read();
	for(int i = 1; i < n; i++)
	{
		u = read(); v = read(); val = read();
		add(u,v,val); add(v,u,val);
		if(u + 1 != v) fl1 = 0;
	}
	if(fa2)//二叉树 
	{
		int l = 0,r = 10000 * n,ans;
		while(l <= r)
		{
			int mid = l + r >> 1;
			if(check(mid))
			{
				ans = mid;
				l = mid + 1;
			}
			else r = mid - 1;
		}
		printf("%d\n",ans);
	}
}
/*
10 3
1 2 5
2 3 4
3 4 8
4 5 7
5 6 1
6 7 4
7 8 3
8 9 6
9 10 3
*/
