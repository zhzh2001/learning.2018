#include<bits/stdc++.h>
#define maxn 500005
//#define kcz
using namespace std;
inline int read()
{
	int lin = 0,f = 1;
	char x = getchar();
	while(x < '0' || x > '9')
	{
		if(x == '-') f = -1;
		x = getchar();
	}
	while(x >= '0' && x <= '9')
	{
		lin = (lin << 1) + (lin << 3) + x - '0';
		x = getchar();
	}
	return lin * f;
}
#define PII pair <int,int>
#define fir first
#define sec second
#define ma(a,b) make_pair(a,b)
int f[maxn];
int T,n,m,cnt,b[maxn],nee[maxn];
int dfs(int val,int k)
{
	if(val < 0) return 0;
	if(~f[val]) return f[val];
	f[val] = 0;
	int fl = 0;
	for(int i = k; i <= cnt; i++)
	{
		if(dfs(val - nee[i],i))
		{
			fl = 1;
			break;
		}
	}
	f[val] = fl;
	return f[val];
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T = read();
	while(T--)
	{
		cnt = 0;
		memset(f,-1,sizeof(f));
		f[0] = 1;
		n = read();
		for(int i = 1; i <= n; i++)
			b[i] = read();
		sort(b + 1,b + 1 + n);
		for(int i = 1; i <= n; i++)
		{
			int k = dfs(b[i],1);
			if(!k)
			{
				nee[++cnt] = b[i];
				f[b[i]] = 1;
			}
		}
		printf("%d\n",cnt);
	}
	return 0;
}

