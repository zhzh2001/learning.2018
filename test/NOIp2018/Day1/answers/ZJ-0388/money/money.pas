var
  n,k,m,l,ans,t,max:int64;
  i,j,p:longint;
  a:array[0..5000005] of int64;
  b:array[0..5000005] of boolean;
  f:boolean;
 procedure sort(l,r:int64);
      var
         i,j,x,y:int64;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
  assign(input,'money.in'); reset(input);
  assign(output,'money.out'); rewrite(output);
  readln(t);
  for p:=1 to t do
  begin
    readln(n);
    max:=0;
    ans:=1;
    for i:=1 to 25000 do b[i]:=false;
    for i:=1 to n do begin read(a[i]); if a[i]>max then max:=a[i]; end;
    sort(1,n);
    for i:=1 to n do
    begin
      if i=1 then
      begin
        for j:=1 to max div a[i] do
          b[a[i]*j]:=true;
      end else
      begin
        f:=false;
        for j:=1 to max div a[i] do
          if not b[a[i]*j] then
          begin
            f:=true;
            break;
          end;
        if f then
        begin
          inc(ans);
          b[a[i]]:=true;
          for j:=1 to max do
            if b[j] then b[j+a[i]]:=true;
        end;
      end;
    end;
    writeln(ans);
  end;
  close(input);
  close(output);
end.

