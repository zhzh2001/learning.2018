var
  n,i,j,k,m,l:longint;
  a:array[-10..300005] of longint;
  ans:int64;
begin
  assign(input,'road.in'); reset(input);
  assign(output,'road.out'); rewrite(output);
  readln(n);
  for i:=1 to n do
  read(a[i]);
  a[0]:=0;
  for i:=n downto 1 do
  begin
    if a[i]-a[i-1]>0 then ans:=ans+a[i]-a[i-1];
  end;
  writeln(ans);
  close(input);
  close(output);
end.
