//Leo
#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
#define N 100100
using namespace std;
int T,n,a[N],f[N];

int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0' || ch>'9'){if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0' && ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x*f;
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		n=read();
		int maxx=0,ans=0;
		for(int i=1;i<=n;i++) a[i]=read(),maxx=max(maxx,a[i]);
		for(int i=1;i<=maxx;i++) f[i]=0;
		f[0]=1;
		for(int i=1;i<=n;i++)
			for(int j=a[i];j<=maxx;j++) f[j]+=f[j-a[i]];
		for(int i=1;i<=n;i++) if(f[a[i]]==1) ans++;
		printf("%d\n",ans);
	}
	return 0;
}
