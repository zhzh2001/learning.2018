//Leo
#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
#define N 200200
using namespace std;
int n,num,pre,last,a[N];
long long ans;

struct leo{
	int w,id;
}q[N];

int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0' || ch>'9'){if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0' && ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x*f;
}

bool cmp(leo a,leo b){return a.w<b.w;}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) a[i]=read(),q[i].w=a[i],q[i].id=i;
	sort(q+1,q+1+n,cmp);
	pre=1;num=1;last=0;
	for(int i=2;i<=n;i++)
		if(q[i].w!=q[i-1].w){
			ans+=(long long)num*(q[i-1].w-last);
			for(int j=pre;j<i;j++){
				int opt=0;
				if(q[j].id==1 || a[q[j].id-1]==-1) opt++;
				if(q[j].id==n || a[q[j].id+1]==-1) opt++;
				if(!opt) num++;
				if(opt==2) num--;
				a[q[j].id]=-1;
			}
			last=q[i-1].w;pre=i;
		}
	ans+=(long long)num*(q[n].w-last);
	printf("%lld\n",ans);
	return 0;
}

/*
6
4 3 2 5 3 5
*/
