//Leo
#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
#define N 100100
using namespace std;
int n,m,cnt,ans,num,q[N],vet[N+N],Next[N+N],len[N+N],head[N],dp[N][2],W[N],flag[N];
bool flag1,flag2;

int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0' || ch>'9'){if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0' && ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x*f;
}

inline void add_edge(int u,int v,int w)
{
	vet[++cnt]=v;Next[cnt]=head[u];len[cnt]=w;head[u]=cnt;
}

void dfs1(int u,int fa)
{
	for(int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if(v==fa) continue;
		dfs1(v,u);
		if(dp[v][0]+len[i]>dp[u][0]){
			dp[u][1]=dp[u][0];
			dp[u][0]=dp[v][0]+len[i];
		}else if(dp[v][0]+len[i]>dp[u][1]) dp[u][1]=dp[v][0]+len[i];
	}
}

void solve1()
{
	dfs1(1,0);
	ans=0;
	for(int i=1;i<=n;i++) ans=max(ans,dp[i][0]+dp[i][1]);
	printf("%d\n",ans);
}

bool cmp(int a,int b){return a>b;}

bool check1(int mid)
{
	int tail=n-1;
	for(int i=1;i<=n;i++) flag[i]=0;
	for(int i=1;i<=m;i++){
		while(tail>i && (W[i]+W[tail]<mid || flag[tail])) tail--;
		if(tail==i) return false;
		if(flag[i] || flag[tail]) return false;
		flag[i]=flag[tail]=1;tail--;
	}
	return true;
}

void solve2()
{
	sort(W+1,W+1+n-1,cmp);
	int l=0,r=W[1]+W[2];
	while(l<r){
		int mid=(l+r+1)>>1;
		if(check1(mid)) l=mid;
		else r=mid-1;
	}
	printf("%d\n",r);
}

void dfs2(int u,int fa)
{
	for(int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if(v==fa) continue;
		q[++num]=len[i];
		dfs2(v,u);
	}
}

bool check2(int mid)
{
	int ct=0,sum=0;
	for(int i=1;i<=num;i++){
		sum+=q[i];
		if(sum>=mid){sum=0;ct++;}
	}
	if(ct>=m) return true;
	return false;
}

void solve3()
{
	num=0;
	dfs2(1,0);
	int l=0,r=0;
	for(int i=1;i<=num;i++) r+=q[i];
	while(l<r){
		int mid=(l+r+1)>>1;
		if(check2(mid)) l=mid;
		else r=mid-1;
	}
	printf("%d\n",r);
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	flag1=true;flag2=true;
	for(int i=1;i<n;i++){
		int u=read(),v=read(),w=read();
		add_edge(u,v,w);add_edge(v,u,w);
		if(u!=1) flag1=false;
		if(v-u!=1) flag2=false;
		W[i]=w;
	}
	if(m==1){solve1();return 0;}
	if(flag1){solve2();return 0;}
	if(flag2){solve3();return 0;}
	return 0;
}
