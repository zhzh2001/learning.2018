#include<cstdio>
#include<iostream>
#include<algorithm>

using namespace std;

int T,n;
int a[103];
bool not_prime[1003];
bool is_prime=true;

void INIT_prime()
{
	for(int i=2;i<=1000;i++){
		for(int j=2;i*j<=1000;j++){
			not_prime[i*j]=true;
		}
	}
}

long long gcd(long long a,long long b)//最大公约数 
{
	return b?gcd(b,a%b):a;
}

long long lcm(long long a,long long b)//最小公倍数 
{
	return a*b/gcd(a,b);
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	INIT_prime();
	ios::sync_with_stdio(false);
	cin>>T;
	for(int mdzz=1;mdzz<=T;mdzz++)
	{
		cin>>n;
		is_prime=true;
		for(int i=1;i<=n;i++){
			cin>>a[i];
			if(not_prime[a[i]]) is_prime=false;
		}
		if(is_prime){cout<<n<<endl;continue;}
		sort(a+1,a+1+n);
		for(int i=n;i>=1;i--){
			for(int j=1;j<i;j++){
				if(a[i]%a[j]==0 && a[i]!=a[j]){
					a[i]=0;
					break;
				}
			}
		}
		long long res=0;
		for(int i=1;i<=n;i++){
			if(a[i]) res++;
		}
		cout<<res<<endl;
	}
	return 0;
}

/*
1. 1<=m<=n
2. 如果全部都是素数的话,那么表示的方法一定是原来的样子 
3. 如果货币系统里面包含1,那么这个系统就是完善的,所以输入数据保证了没有1,不需要在处理prime时候写1 
4. Give Up,错的 
*/ 
