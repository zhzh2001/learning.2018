#include<cstdio>
#include<iostream>
#include<algorithm>

using namespace std;

const int INF=0x3f3f3f3f;

int n,MinI,Minn=INF;
int d[100003];
long long ans;

void jian(int x,int place)
{
	for(int i=place;i<=n;i++){
		if(d[i]==0){
			break;
		}else{
			d[i]-=x;
		}
	}
	for(int i=place-1;i>=1;i--){
		if(d[i]==0){
			break;
		}else{
			d[i]-=x;
		}
	}
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%d",&d[i]);
		if(d[i]<=Minn && d[i]!=0){
			Minn=d[i];
			MinI=i;
		}
	}
	while(Minn!=INF){
		ans+=Minn;
		jian(Minn,MinI);
		Minn=INF;
		for(int i=1;i<=n;i++){
			if(d[i]==0) continue;
			if(d[i]<=Minn){
				Minn=d[i];
				MinI=i;
			}
		}
	}
	cout<<ans<<endl;
	return 0;
}
