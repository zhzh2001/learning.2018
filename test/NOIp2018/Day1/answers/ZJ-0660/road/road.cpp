#include<cstdio>
#include<algorithm>

#define ll long long
using namespace std;
const int N=1e5+5;
int d[N],n,f[N][20],g[N][20];
ll ans;
inline void read(int &x)
{
	x=0; char c=getchar();
	for (; c<'0' || c>'9'; c=getchar());
	for (; c>='0' && c<='9'; x=(x<<3)+(x<<1)+c-'0',c=getchar());
}
/*
inline int read()
{
	int x=0; char c=getchar();
	for (; c<'0' || c>'9'; c=getchar());
	for (; c>='0' && c<='9'; x=(x<<3)+(x<<1)+c-'0',c=getchar());
	return x;
}
*/
inline void doing(int l,int r,int h)
{
	if (l==r) {
		ans+=d[l]-h;
		return;
	}
	register int l1=0,rr=20,m,w,pos;
	while (l1<=rr)
	{
		m=(l1+rr)>>1;
		if (l+(1<<m)-1>r) rr=m-1; else w=m,l1=m+1;
	}
	if (f[l][w]>f[r-(1<<w)+1][w])
	  pos=g[r-(1<<w)+1][w]; 
	else pos=g[l][w]; 
	w=min(f[l][w],f[r-(1<<w)+1][w])-h;
	ans+=w;
	if (l<pos) doing(l,pos-1,h+w); 
	if (r>pos) doing(pos+1,r,h+w);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	register int i,j;
	for (i=1; i<=n; ++i)  
	{
	  read(d[i]);
	  f[i][0]=d[i],g[i][0]=i;
	}
	for (i=1; (1<<i)<=n; ++i)
	  for (j=1; j+(1<<i)-1<=n; ++j)
	  {
	  	f[j][i]=min(f[j][i-1],f[j+(1<<i-1)][i-1]);
	  	if (f[j][i-1]>f[j+(1<<i-1)][i-1])
	  	  g[j][i]=g[j+(1<<i-1)][i-1];
	  	else g[j][i]=g[j][i-1];
	  }
	ans=0;
	doing(1,n,0);
	printf("%lld\n",ans);
	return 0;
}
