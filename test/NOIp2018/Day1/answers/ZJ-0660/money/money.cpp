#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int M=25005;
int t,n,a[105];
bool vis[M],jg[M],bj[105];
inline void read(int &x)
{
	x=0; char c=getchar();
	for (; c<'0' || c>'9'; c=getchar());
	for (; c>='0' && c<='9'; x=(x<<3)+(x<<1)+c-'0',c=getchar());
}
inline void doing(int x)
{
	for (int i=1; i<=x/2; ++i)
	{
		if (vis[i] && vis[x-i]) vis[x]=1;
		if (!vis[i] && !jg[i]) doing(i);
		if (!vis[i]) continue;
		if (!vis[x-i] && !jg[x-i]) doing(x-i);
		if (!vis[x-i]) continue;
		vis[x]=1; return;
	}
	jg[x]=1; return;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(t);
	while (t--)
	{
		read(n); memset(vis,0,sizeof(vis));
		memset(jg,0,sizeof(jg));
		memset(bj,0,sizeof(bj));
		for (int i=1; i<=n; ++i)
		  read(a[i]); 
		sort(a+1,a+1+n);
		for (int i=1; i<=n; ++i)
		{
		  if (vis[a[i]]) continue;
		  for (int j=2*a[i]; j<=a[n]; j+=a[i])
		    vis[j]=1;
		}
		for (int i=1; i<=n; ++i)
		{
			if (vis[a[i]]) {
				bj[i]=1; continue;
			}
			doing(a[i]);
			if (vis[a[i]]) bj[i]=1;
			vis[a[i]]=1;
		}
		int ans=0;
		for (int i=1; i<=n; ++i)
		  if (!bj[i]) ans++; 
		printf("%d\n",ans);
	}
	return 0;
}
