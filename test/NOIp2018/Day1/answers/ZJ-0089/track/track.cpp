#include <cstdio>
#include <cctype>
#include <queue>
#include <algorithm>
#include <ctime>
#include <cmath>
#define rg register
using namespace std;
typedef long long LL;
const int INF = 0x3f3f3f3f;
const int maxn = 100005;
inline void redi(int & a) {
	char ch = getchar (); a = 0;
	while (!isdigit (ch)) ch = getchar ();
	while (isdigit (ch) ) a = a * 10 +ch - 48,ch = getchar ();
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	srand(time(NULL));
	printf("%d",abs(rand()%19260817));
	return 0;
}
