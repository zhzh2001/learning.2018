#include <cstdio>
#include <vector>
#include <cctype>
#include <queue>
#include <cstring>
#define rg register
using namespace std;
typedef long long LL;
const int INF = 0x3f3f3f3f;
const int maxn = 100005;
inline void redi (int & a) {
	char ch = getchar (); a = 0;
	while (!isdigit (ch)) ch = getchar ();
	while (isdigit (ch) ) a = a * 10 +ch - 48,ch = getchar ();
}
int b[maxn],a[maxn];
int t,n,m,cnt;
inline void swapp(int &a,int &b) {
	a^=b^=a^=b;
}
int test (int a,int b,int x) {
	if(a<b) swapp(a,b);
	while (x>0) {
		if(!x%b) return 1;
		x-=a;
	}
	return 0;
}
inline void s1() {
	for (rg int i=1;i<=n;++i) {
		for (rg int j=1;j<=n;++j) {
			if(!b[i]) continue;
			if(i==j) continue;
			if(a[i]%a[j]==0) {
				b[i]=0;
				++cnt;
			}
		}
	}
}
inline void s2() {
	int minn=INF;
	for (rg int i=1;i<=n;++i) {
		if(b[i]&&minn>a[i]) minn=a[i];
	}
	int min2=INF;
	for (rg int i=1;i<=n;++i) {
		if(b[i]&&min2>a[i]&&a[i]!=minn) min2=a[i];
	}
	if(min2==INF) return;
	int k=minn*min2-minn-min2;
	for (rg int i=1;i<=n;++i) {
		if(b[i]&&a[i]>k) {
			b[i]=0;
			++cnt;
		}
	}
}
inline void s3() {
	for(rg int i=1;i<=n;++i) {
		if(!b[i]) continue;
		for(rg int j=1;j<=n;++j) {
			if(!b[j]) continue;
			for (rg int k = 1;k<=n;++k) {
				if(!b[k]) continue;
				if(test(a[i],a[j],a[k])) {
					b[k]=0;
					++cnt;
				}
			}
		}
	}
}
void work () {
	s1();
	s2();
	s3();
}
int main () {
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	redi(t);
	do {
		redi (n);
		memset (b,0,sizeof (b));
		memset (a,0,sizeof (a));
		for (rg int i=1;i<=n;++i) redi (a[i]),b[i]=1;
		cnt = 0;
		work ();
		printf("%d\n",n-cnt);
	}while (--t);
	return 0;
}
