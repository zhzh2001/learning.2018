#include <cstdio>
#include <vector>
#include <cctype>
#include <queue>
#include <cstring>
#include <set>
#include <algorithm>
#define rg register
using namespace std;
typedef long long LL;
const int INF = 0x3f3f3f3f;
const int maxn = 100005;
inline void redi (int & a) {
	char ch = getchar (); a = 0;
	while (!isdigit (ch)) ch = getchar ();
	while (isdigit (ch) ) a = a * 10 +ch - 48,ch = getchar ();
}
int cnt;
int chong;
int g[maxn];
int a[maxn];
bool search (int x,int s) {
	if (x<0) return false;
	for (rg int i=1;i<=s;++i) {
		if(a[i] ==x) continue;
		if(x-a[i]<0) continue;
		if(g[x-a[i]]==1) return true;
		if(search (x-a[i],s)){
			g[x-a[i]]=1;
			return true;
		}
	}
	return false;
}
int t,n;
int main() {
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	redi(t);
	do {
		redi(n);
		cnt=0;chong =0;
		memset (g,0,sizeof (g));
		g[0]=1;
		for(rg int i=1;i<=n;++i) {
			redi(a[i]);
			if(!g[a[i]]) g[a[i]]=1;
			else ++chong;
		}
		int siz=n-chong;
		sort(a+1,a+1+siz);
		for(rg int i=1;i<= siz;++i) {
			if(!search (a[i],siz)) ++cnt;
		}
		printf("%d\n",cnt);
	}while(--t);
	return 0;
}
