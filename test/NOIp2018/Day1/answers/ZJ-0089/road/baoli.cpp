#include <cstdio>
#include <cctype>
#include <queue>
#include <algorithm>
#define rg register
using namespace std;
typedef long long LL;
const int INF = 0x3f3f3f3f;
const int maxn = 100005;
int n,d[maxn],cnt,zl;
inline int minn (int a,int b) {
	return a < b ? a : b;
}
inline void redi(int & a) {
	char ch = getchar (); a = 0;
	while (!isdigit (ch)) ch = getchar ();
	while (isdigit (ch) ) a = a * 10 +ch - 48,ch = getchar ();
}
int query_min(int l,int r) {
	if(l==r) return d[l];
	else return minn(query_min(l,(l+r)/2),query_min((l+r)/2+1,r));
}


void dfs (int l,int r) {
	int minn = INF,pos;
	//
	for (int i = l;i <= r;++ i) {
		if (minn > d[i]) {
			minn = d[i];
			pos = i;
		}
	}
	//
	if ((minn == 0) || (minn == INF)) return;
	cnt += minn;
	//
	for (rg int i = l;i <= r;++i) {
		d[i] -= minn;
	}
	//
	dfs (l,pos - 1);
	dfs (pos + 1,r);
} 
int main () {
	freopen ("road.in","r",stdin);freopen ("road.out","w",stdout);
	redi(n);
	for (rg int i = 1;i <= n;++ i) {
		redi (d[i]);
	}
	dfs (1,n);
	printf ("%d",cnt);
	return 0;
}
