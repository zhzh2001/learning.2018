#include <cstdio>
#include <cctype>
#include <queue>
#include <algorithm>
#define rg register
using namespace std;
typedef long long LL;
const int INF = 0x3f3f3f3f;
const int maxn = 100005;
inline void redi(int & a) {
	char ch = getchar (); a = 0;
	while (!isdigit (ch)) ch = getchar ();
	while (isdigit (ch) ) a = a * 10 +ch - 48,ch = getchar ();
}
int n,d[maxn];
LL cnt;
void dfs (int l,int r) {
	int minn = INF,pos;
	for (int i = l;i <= r;++ i) {
		if (minn > d[i]) {
			minn = d[i];
			pos = i;
		}
	}
	if ((minn == 0) || (minn == INF)) return;
	cnt += minn;
	for (rg int i = l;i <= r;++i) {
		d[i] -= minn;
	}
	dfs (l,pos - 1);
	dfs (pos + 1,r);
} 
int main () {
	freopen ("road.in","r",stdin);freopen ("road.out","w",stdout);
	redi(n);
	for (rg int i = 1;i <= n;++ i) {
		redi (d[i]);
	}
	dfs (1,n);
	printf ("%lld",cnt);
	return 0;
}
