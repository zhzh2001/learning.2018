#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;
int n,m,fa[50009][20],d[50009],dis[50009];
struct edge{
	int v,d;
	edge(){}
	edge(const int&v,const int&d):v(v),d(d){}
};
vector<edge>g[50009];
/*void dfs(int u,int fat,int dd){
	d[u]=d[fat]+1,dis[u]=dis[fat]+dd;
	fa[u][0]=fat;
	for(int i=1;i<=20;i++)
		fa[u][i]=fa[fa[u][i-1]][i-1];
	for(int i=0;i<g[u].size();i++)
		if(g[u][i].v!=fat)
			dfs(g[u][i].v,u,g[u][i].d);
}*/
int dfs2(int u,int fat,int dd){
	int res=u;dis[u]=dis[fat]+dd;
	for(int i=0;i<g[u].size();i++)
		if(g[u][i].v!=fat){
			int dd=dfs2(g[u][i].v,u,g[u][i].d);
			if(dis[dd]>dis[res])
				res=dd;
		}
	return res;
}
/*int lca(int u,int v){
	if(d[u]<d[v])swap(u,v);
	for(int i=0;i<=20;i++)
		if(d[fa[u][i]]>=d[v])
			u=fa[u][i];
	if(u==v)return u;
	for(int i=0;i<=20;i++)
		if(fa[u][i]!=fa[v][i])
			u=fa[u][i],v=fa[v][i];
	return fa[u][0];
}*/
bool check(int x){
	int tmp=0,res=0;
	for(int i=2;i<=n;)
		if(tmp>=x)
			res++,tmp=0;
		else
			tmp+=dis[i]-dis[i-1],i++;
	if(tmp>=x)res++;
	return res>=m;
}
void chkmin(int&a,const int&b){if(a>b)a=b;}
int main(){
	freopen("track.in","r",stdin),freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool flag=1;
	for(int i=1;i<=n-1;i++){
		int u,v,d;
		scanf("%d%d%d",&u,&v,&d);
		if(v!=u+1)flag=0;
		g[u].push_back(edge(v,d)),g[v].push_back(edge(u,d));
	}
	if(m==1){
		memset(dis,0,sizeof(dis));
		int u=dfs2(1,0,0);
		memset(dis,0,sizeof(dis));
		int v=dfs2(u,0,0);
		printf("%d\n",dis[v]);
		return 0;
	}
	if(flag){
		int l=0x3f3f3f3f,r=0;
		for(int i=2;i<=n;i++){
			int d=(g[i][0].v==i-1?g[i][0].d:g[i][1].d);
			dis[i]=dis[i-1]+d;
			chkmin(l,d),r+=d;
		}
		while(l<=r){
			int mid=l+r>>1;
			check(mid)?l=mid+1:r=mid-1;
		}
		printf("%d\n",r);
		return 0;
	}
	//dfs(1,0,0);
	return 0;
}

