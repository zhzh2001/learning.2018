#include<cstdio>
#include<cctype>
#include<vector>
using namespace std;
inline char gc(){
	static char buf[1<<17],*p1,*p2;
	return (p1==p2)&&(p1=(p2=buf)+fread(buf,1,1<<17,stdin),p1==p2)?EOF:*p2++;
}
inline int gi(){
	int res=0;char ch=gc();
	for(;!isdigit(ch)&&ch!=EOF;ch=gc());
	if(ch==EOF)return res;
	for(;isdigit(ch);ch=gc())(res*=10)+=ch&15;
	return res;
}
int n,a[100009],ans;
vector<int>b;
int main(){
	freopen("road.in","r",stdin),freopen("road.out","w",stdout);
	n=gi();
	for(int i=1;i<=n;i++)
		a[i]=gi();
	for(int i=1;i<=n;i++){
		if(b.size()&&b[b.size()-1]>a[i])
			ans+=b[b.size()-1]-a[i];
		while(b.size()&&b[b.size()-1]>a[i])
			b.pop_back();
		b.push_back(a[i]);
	}
	if(b.size())
		ans+=b[b.size()-1];
	printf("%d\n",ans);
	return 0;
}

