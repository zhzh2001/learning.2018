#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
bool b[1000009],c[1000009];
int t,n,a[109];
int gcd(int a,int b){return b?gcd(b,a%b):a;}
void chkmin(int&a,const int&b){if(a>b)a=b;}
int main(){
	freopen("money.in","r",stdin),freopen("money.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		int maxn=0x3f3f3f3f,m=0;
		memset(a,0,sizeof(a));
		memset(b,0,sizeof(b));
		memset(c,0,sizeof(c));
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		int g=a[1];
		for(int i=2;i<=n;i++)
			g=gcd(g,a[i]);
		if(g!=1)
			for(int i=1;i<=n;i++)
				a[i]/=g;
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++)
				if(gcd(a[i],a[j])==1)
					chkmin(maxn,a[i]*a[j]-a[i]-a[j]+2);
		b[0]=1;
		for(int i=1;i<=n;i++){
			for(int j=a[i];j<=maxn;j+=a[i])
				b[j]=1;
			for(int j=1;j<=maxn;j++)
				if(b[j]&&j%a[i])
					for(int k=j+a[i];k<=maxn;k+=a[i])
						b[k]=1;
		}
		c[0]=1;
		for(int i=1;i<=maxn;i++)
			if(b[i]&&!c[i]){
				//printf("%d ",i);
				m++;
				for(int j=i;j<=maxn;j+=i)
					c[j]=1;
				for(int j=1;j<=maxn;j++)
					if(c[j]&&j%i)
						for(int k=j+i;k<=maxn;k+=i)
							c[k]=1;
			}
		/*for(int i=1;i<=maxn;i++)
			printf("%d ",b[i]);puts("");
		for(int i=1;i<=maxn;i++)
			printf("%d ",b[i]);puts("");*/
		printf("%d\n",m);
	}
	return 0;
}

