#include<bits/stdc++.h>
using namespace std;
const int N=105;
const int M=25005;
int a[N],n,ans,vis[M];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	int T;scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		memset(vis,0,sizeof(vis));
		vis[0]=1;ans=0;
		for (int i=1;i<=n;i++)
		{
			if (vis[a[i]]) continue;
			for (int j=0;j<=25000-a[i];j++) vis[j+a[i]]|=vis[j];
			ans++;
		}
		printf("%d\n",ans);
	}
	return 0;
}
