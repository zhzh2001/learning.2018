#include<bits/stdc++.h>
#define P pair<int,int>
#define fir first
#define sec second
using namespace std;
const int N=50005;
int cnt,head[N],n,m,u,v,w,top,q[N],k,g[N],l,r,vis[N];
P f[N];
int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while ('0'<=ch&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*f;
}
struct node{int to,next,w;}num[N<<1];
void add(int x,int y,int w)
{num[++cnt].to=y;num[cnt].next=head[x];num[cnt].w=w;head[x]=cnt;}
void dfs(int x,int fa)
{
	for (int i=head[x];i;i=num[i].next)
	  if (num[i].to!=fa) dfs(num[i].to,x);
	int sum=0,top=0;
	for (int i=head[x];i;i=num[i].next)
	  if (num[i].to!=fa)
	  {
	  	sum+=f[num[i].to].fir;
		if (f[num[i].to].sec+num[i].w>=k) sum++;
		else q[++top]=f[num[i].to].sec+num[i].w,vis[top]=0;
	  }
	sort(q+1,q+top+1);
	int hz=1,pos=0;
	for (int i=top;i>=1;i--)
	{
		while (hz<i&&q[i]+q[hz]<k) hz++;
		if (hz>=i) {pos=i;break;}
		g[i]=hz;vis[i]=vis[hz]=1;
		hz++;sum++;
	}
	int Max=0;
	for (int i=top;i>=1;i--)
	  if (!vis[i]) {Max=q[i];break;}
	for (int i=pos;i<top;i++) 
	  if (q[i]+q[pos-(i-pos)-1]>=k){
	  	if (i==top-1) Max=q[top];
		else if (g[i+2]<pos-(i-pos)-1) Max=q[i+1];
	  }else break;
	f[x]=P(sum,Max);
}
int check(int x)
{
	k=x;
	for (int i=1;i<=n;i++) f[i]=P(0,0);
	dfs(1,-1);
	if (f[1].fir>=m) return 1;else return 0;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	n=read();m=read();l=0;
	for (int i=1;i<n;i++){
		u=read();v=read();w=read();
		add(u,v,w),add(v,u,w),r+=w;
	}
	int ans=0;
	while (l<=r)
	{
		int mid=((l+r)>>1);
		if (check(mid)) ans=mid,l=mid+1;else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
