#include<bits/stdc++.h>
#define ll long long
using namespace std;
void fff(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
}
ll read(){
	ll u=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') f=-1;else f=1;c=getchar();}
	while(c>='0'&&c<='9') u=(u<<1)+(u<<3)+c-'0',c=getchar();
	return u*f;
}
void write(ll x){
	int l=0,p[30];
	if(x<0) x=-x,putchar('-');
	if(x==0) putchar('0');
	while(x) p[++l]=x%10,x/=10;
	while(l) putchar('0'+p[l--]);
}
ll n,p[100010],maxn,sum;
bool flag=true;

int main(){
	fff();
	n=read();
	for(int i=1;i<=n;i++) p[i]=read();
	p[0]=0;p[n+1]=0;
	for(int i=1;i<=n+1;i++){
		if(p[i]>=p[i-1]){
			if(!flag) sum-=p[i-1];
			maxn=p[i];
			flag=true;
		}
		if(p[i]<p[i-1]){
			if(flag) sum+=maxn,flag=false;
			maxn=p[i];
		}
	}
	write(sum);
	return 0;
}
