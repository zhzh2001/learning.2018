#include<bits/stdc++.h>
#define ll long long
using namespace std;
void fff(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
}
inline ll read(){
	ll u=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') f=-1;else f=1;c=getchar();}
	while(c>='0'&&c<='9') u=(u<<1)+(u<<3)+c-'0',c=getchar();
	return u*f;
}
void write(ll x){
	int l=0,p[30];
	if(x<0) x=-x,putchar('-');
	if(x==0) putchar('0');
	while(x) p[++l]=x%10,x/=10;
	while(l) putchar('0'+p[l--]);
}
ll n,m,cnt,maxl,maxid;
ll hed[50010],nex[100010],vt[100010],vl[100010];

inline void add(){
	ll x=read(),y=read(),w=read();
	vt[++cnt]=y,nex[cnt]=hed[x],hed[x]=cnt,vl[cnt]=w;
	vt[++cnt]=x,nex[cnt]=hed[y],hed[y]=cnt,vl[cnt]=w;
}

void dfs(int u,int fa,ll le){
	if(maxl<le) maxl=le,maxid=u;
	for(int i=hed[u];i;i=nex[i])
		if(vt[i]!=fa)
			dfs(vt[i],u,le+vl[i]);
}
ll sum,dp[50010];
vector<ll> res[50010];
vector<bool> able[50010]; 

void dfss(int u,int fa,ll le){
	res[u].clear(),able[u].clear();
	for(int i=hed[u];i;i=nex[i])
		if(vt[i]!=fa)
			dfss(vt[i],u,le),res[u].push_back(dp[vt[i]]+vl[i]),able[u].push_back(true);
	dp[u]=0;
	if(res[u].size()==0) return;
	sort(res[u].begin(),res[u].end());
	int lll=res[u].size()-1;
	while(lll>=0&&res[u][lll]>=le) sum++,able[u][lll]=false,lll--;
	int lt=0,rt=lll,ans=0,tmp=0;
	while(lt<=rt){
		int mid=(lt+rt)>>1;
		if(res[u][mid]*2<le) lt=(ans=mid)+1;
		else rt=mid-1;
	}
	lt=rt=ans;
	rt++;
	while(lt>=0&&rt<=lll){
		while(rt<=lll&&res[u][lt]+res[u][rt]<le) rt++;
		if(rt<=lll) able[u][lt]=able[u][rt]=false,lt--,rt++,tmp++;
	}
	int re=((lll-ans)-tmp);
	if(re%2){
		for(int i=lll;i>=0;i--) if(able[u][i]){
			dp[u]=res[u][i];break;
		}
	}
	else{
		for(int i=ans;i>=0;i--) if(able[u][i]){
			dp[u]=res[u][i];break;
		}
	}
	sum+=tmp+re/2;
}

inline bool check(ll le){
	sum=0;
	dfss(1,0,le);
	return sum>=m;
}

int main(){
	fff();
	n=read(),m=read();
	for(int i=1;i<n;i++) add();
	dfs(1,0,0),dfs(maxid,0,0);
	ll l=0,r=maxl,ans=0;
	while(l<=r){
		ll mid=(l+r)>>1;
		if(check(mid)) l=(ans=mid)+1;
		else r=mid-1;
	}
	write(ans);
	return 0;
}
