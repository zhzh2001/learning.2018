#include<bits/stdc++.h>
#define ll long long
using namespace std;
void fff(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}
ll read(){
	ll u=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') f=-1;else f=1;c=getchar();}
	while(c>='0'&&c<='9') u=(u<<1)+(u<<3)+c-'0',c=getchar();
	return u*f;
}
void write(ll x){
	int l=0,p[30];
	if(x<0) x=-x,putchar('-');
	if(x==0) putchar('0');
	while(x) p[++l]=x%10,x/=10;
	while(l) putchar('0'+p[l--]);
}
ll T,n,p[110],sum;
bool able[25010];

int main(){
	fff();
	T=read();
	while(T--){
		memset(p,0,sizeof(p)),memset(able,0,sizeof(able));
		n=read();sum=n;
		for(int i=1;i<=n;i++) p[i]=read();
		sort(p+1,p+n+1);
		able[0]=1;
		for(int i=1;i<=n;i++){
			if(able[p[i]]){
				sum--;
				continue;
			}
			for(int j=0;j+p[i]<=p[n];j++){
				able[j+p[i]]|=able[j];
			}
		}
		write(sum);putchar('\n');
	}
	return 0;
}
