#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <queue>
#include <vector>
using namespace std;
#define N 100005
int f[N],a[N];
int n,mx,m;
void init()
{
	mx=0;
}
void solve()
{
	init();
	scanf("%d",&n);m=n;
	for (int i=1;i<=n;++i) scanf("%d",&a[i]),mx=max(mx,a[i]);
	for (int i=0;i<=mx;++i) f[i]=0;
	f[0]=1;
	sort(a+1,a+1+n);
	for (int i=1;i<=n;++i)
	{
		if (f[a[i]]) --m;
		for (int j=a[i];j<=mx;++j)
			f[j]|=f[j-a[i]];
	}
	printf("%d\n",m);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	cin>>T;
	while (T--) solve();
	fclose(stdin);fclose(stdout);
	return 0;
}
