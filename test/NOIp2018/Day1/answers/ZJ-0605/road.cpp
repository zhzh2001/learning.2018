#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <queue>
#include <vector>
using namespace std;
#define N 1000005
int a[N];
int n,ans;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	cin>>n;
	for (int i=1;i<=n;++i) scanf("%d",&a[i]);
	ans=0;
	for (int i=1;i<=n;++i)
	if (a[i]>a[i-1]) ans+=a[i]-a[i-1];
	cout<<ans<<endl;
	fclose(stdin);fclose(stdout);
	return 0;
}
