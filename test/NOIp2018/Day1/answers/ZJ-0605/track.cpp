#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <queue>
#include <vector>
using namespace std;
#define inf 1000000000
#define N 233333
int num,to[N],Next[N],head[N],len[N];
int a[N],dis[N];
int n,m;
inline void add(int x,int y,int t)
{
	++num;
	to[num]=y;
	Next[num]=head[x];
	head[x]=num;
	len[num]=t;
}
void deal(int x,int pre)
{
	for (int i=head[x];i;i=Next[i])
	if (to[i]!=pre)
	{
		dis[to[i]]=dis[x]+len[i];
		deal(to[i],x);
	}
}
int solve1()
{
	int x=1,mx=0;
	dis[x]=0;deal(x,-1);
	for (int i=1;i<=n;++i) if (dis[i]>dis[x]) x=i;
	dis[x]=0;deal(x,-1);
	for (int i=1;i<=n;++i) if (dis[i]>mx) mx=dis[i];
	return mx;
}
int solve2()
{
	int x=1,K=0,l,r,mn=inf;
	for (int i=head[x];i;i=Next[i]) a[++K]=len[i];
	sort(a+1,a+1+K);
	r=K;l=r-m-m+1;
	while (l<=0) mn=min(mn,a[r]),++l,--r,--m;
	while (m)
	{
		mn=min(mn,a[l]+a[r]);
		++l,--r;
		--m;
	}
	return mn;
}
int check(int mx)
{
	int t=0,tot=0;
	for (int i=1;i<n;++i)
	{
		t+=a[i];
		if (t>=mx) t=0,++tot;
	}
	return (tot>=m);
}
int solve3()
{
	int K=0;
	for (int i=1;i<n;++i)
	for (int j=head[i];j;j=Next[j])
	if (to[j]>i) a[++K]=len[j];
	int l=0,r=inf,mid;
	while (l<r)
	{
		mid=(l+r+1)>>1;
		if (check(mid)) l=mid;
		else r=mid-1;
	}
	return l;
}
//prioriry_queue <int,vector<int>,greater<int> > heap;
int ftot[N],fmx[N];
void dfs(int x,int pre,int limit)
{
	for (int i=head[x];i;i=Next[i])
	if (to[i]!=pre) dfs(to[i],x,limit);
	int K=0;
	ftot[x]=fmx[x]=0;
	for (int i=head[x];i;i=Next[i])
	if (to[i]!=pre)
	{
		a[++K]=len[i]+fmx[to[i]];
		ftot[x]+=ftot[to[i]];
		if (a[K]>=limit) a[K]=0,++ftot[x],--K;
	}
	sort(a+1,a+1+K);
	//cout<<"x==="<<x<<endl;
	//cout<<"K="<<K<<endl;
	//for (int i=1;i<=K;++i) cout<<a[i]<<' ';cout<<endl;
	int l=1,r=K,ll,rr,tot=0;
	//for (int i=1;i<=K;++i) flag[i]=1;
	while (((a[l]+a[r])<limit) && l<r) fmx[x]=max(fmx[x],a[l]),++l;
	//cout<<"lr="<<l<<' '<<r<<endl;
	ll=l,rr=r;
	while (ll<rr)
	{
		if ((a[ll]+a[rr])>=limit) ++ll,--rr,++tot,++ftot[x];
		else
		fmx[x]=max(fmx[x],a[ll]),++ll;
	}
	if (ll==rr) fmx[x]=max(fmx[x],a[ll]);
	ll=rr-1;rr=rr;
	//cout<<"llrr="<<ll<<' '<<rr<<endl;
	int mmx=0;
	while (tot && ll>=l && rr<=r)
	{
		if ((a[ll]+a[rr])>=limit) --ll,++rr,--tot;else mmx=max(mmx,a[rr]),++rr;
	}
	if ((rr<=r) && (tot==0)) fmx[x]=max(fmx[x],a[rr]);
	if (tot<=0) fmx[x]=max(fmx[x],mmx);
	//cout<<ftot[x]<<' '<<fmx[x]<<endl;
	//for (int i=l;i<=K;++i) fmx[x]=max(fmx[x],a[i]);
	//for (int i=l;i<=K;++i) fmx[x]=max(fmx[x],a[i]);
}
int che(int limit)
{
	int x=1;
	dfs(x,-1,limit);
	return (ftot[x]>=m);
}
int solve()
{
	//cout<<che(8)<<endl;
	//system("pause");
	int l=0,r=inf,mid;
	while (l<r)
	{
		mid=(l+r+1)>>1;
		if (che(mid)) l=mid;else r=mid-1;
	}
	return l;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int x,y,t,f2=1,f3=1;
	cin>>n>>m;
	for (int i=1;i<n;++i)
	{
		scanf("%d%d%d",&x,&y,&t);
		if (x>y) swap(x,y);
		add(x,y,t);add(y,x,t);
		f2&=(x==1);
		f3&=((x+1)==y);
	}
	int ans=0,tt=solve();
	if (m==1) ans=solve1();else
	if (f2) ans=solve2();else
	if (f3) ans=solve3();else
	ans=solve();
	//if (ans!=tt) ;
	cout<<ans<<endl;
	fclose(stdin);fclose(stdout);
	return 0;
}
