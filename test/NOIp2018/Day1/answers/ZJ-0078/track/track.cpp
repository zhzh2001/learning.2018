#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=1e5+5;
struct edge{int to,nxt,w;}e[N<<1];
int head[N],d[N];
int n,m,cnt;
int read()
{
	char ch=getchar(); int x=0;
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	return x;
}
void add(int x,int y,int z){e[++cnt].nxt=head[x]; head[x]=cnt; e[cnt].to=y; e[cnt].w=z;}
void dfs(int u,int f,int dep)
{
	d[u]=dep;
	for(int i=head[u];i;i=e[i].nxt)if(e[i].to!=f)dfs(e[i].to,u,e[i].w+dep);
}
void case1()
{
	dfs(1,0,0);
	int mx=1;
	for(int i=1;i<=n;i++)if(d[i]>d[mx])mx=i;
	memset(d,0,sizeof(d));
	dfs(mx,0,0);
	mx=0;
	for(int i=1;i<=n;i++)if(d[i]>mx)mx=d[i];
	printf("%d\n",mx);
	return;
}
bool judge(int mid)
{
	int s=1,l=1,r=n-1;
	while(s<=m)
	{
		if(d[l]>=mid){l++; if(++s>m)return 1; continue;}
		while(d[l]+d[r]<mid&&l<r)r--;
		if(l>=r)return 0;
		l++; r--;
		if(++s>m)return 1;
	}
	return 0;
}
bool cmp(int x,int y){return x>y;}
void case2()
{
	int l=e[1].w,r=0,mid,ans=0;
	for(int i=head[1];i;i=e[i].nxt)
	{
	    r+=e[i].w;
		l=min(l,e[i].w);
		d[e[i].to]=e[i].w;
	}
	sort(d+1,d+1+n,cmp);
	r/=m;
	while(l<=r)
	{
		mid=(l+r)>>1;
		if(judge(mid)){ans=mid; l=mid+1;}else r=mid-1;
	}
	printf("%d\n",ans);
	return;
}
bool check(int mid)
{
	int s=1,l=1,tmp=0;
	while(l<n)
	{
		tmp+=d[l]; l++;
		if(tmp<mid)continue;
		tmp=0; if(++s>m)return 1;
	}
	return 0;
}
void case3()
{
	int l=e[1].w,r=0,mid,ans=0;
	for(int u=1;u<n;u++)
	    for(int i=head[u];i;i=e[i].nxt)
	    if(e[i].to==u+1)
		{
			r+=e[i].w;
		    l=min(l,e[i].w);
		    d[u]=e[i].w;
		}
	r/=m;
	while(l<=r)
	{
		mid=(l+r)>>1;
		if(check(mid)){ans=mid; l=mid+1;}else r=mid-1;
	}
	printf("%d\n",ans);
	return;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(); m=read(); bool flag=1,ff=1;
	int ss=0;
	for(int i=1,x,y,z;i<n;i++)
	{
	    x=read(); y=read(); z=read();ss+=z;
		add(x,y,z); add(y,x,z);
		if(x!=1&&y!=1)flag=0;
		if(x!=y+1&&y!=x+1)ff=0;
	}
	if(m==1){case1(); return 0;}
	if(flag){case2(); return 0;}
	if(ff){case3(); return 0;}
	printf("%d\n",ss/m);
	return 0;
}
