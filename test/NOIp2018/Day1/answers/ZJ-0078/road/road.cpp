#include<cstdio>
#include<algorithm>
using namespace std;
const int N=2e5+5;
int a[N];
int n,ans;
int read()
{
	char ch=getchar(); int x=0;
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	return x;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	ans=0;
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++)if(a[i]>a[i-1])ans+=a[i]-a[i-1];
	printf("%d\n",ans);
	return 0;
}
