#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=2e5+5;
int n,m,ans;
int a[N],f[N];
int read()
{
	char ch=getchar(); int x=0;
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	return x;
}
void solve()
{
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	sort(a+1,a+1+n);
	f[0]=1; ans=0; m=a[n];
	for(int i=1;i<=n;i++)
	if(!f[a[i]])
	{
		for(int j=a[i];j<=m;j++)if(f[j-a[i]])f[j]=1;
		ans++;
	}
	for(int j=0;j<=m;j++)f[j]=0;
	printf("%d\n",ans);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--)solve();
	return 0;
}
