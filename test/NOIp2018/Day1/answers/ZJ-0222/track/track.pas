var
  h,nx,d1,d2,g,aa,op:array[0..400005]of longint;
  n,m,i,j,x,y,z,nm,a,l,r,md,mk,ss:longint;
  ff,f1:boolean;
procedure d(x,y,z:longint);
begin
  inc(nm);
  d1[nm]:=y;
  d2[nm]:=z;
  nx[nm]:=h[x];
  h[x]:=nm;
end;
procedure fs(u,fa:longint);
var
  v,c,ll:longint;
begin
  ll:=h[u];
  while ll<>0 do
  begin
    v:=d1[ll];
    c:=d2[ll];
    if v=fa then
    begin
      ll:=nx[ll];
      continue;
    end;
    fs(v,u);
    if g[u]+g[v]+c>a then
      a:=g[u]+g[v]+c;
    if g[v]+c>g[u] then
      g[u]:=g[v]+c;
    ll:=nx[ll];
  end;
end;
procedure ssoorrtt(l,r:longint);
      var
         i,j,x,y:longint;
      begin
         i:=l;
         j:=r;
         x:=op[(l+r) div 2];
         repeat
           while op[i]<x do
            inc(i);
           while x<op[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=op[i];
                op[i]:=op[j];
                op[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           ssoorrtt(l,j);
         if i<r then
           ssoorrtt(i,r);
      end;
begin
  assign(input,'track.in'); reset(input);
  assign(output,'track.out'); rewrite(output);
  readln(n,m);
  if m=1 then
  begin
    for i:=1 to n-1 do
    begin
      readln(x,y,z);
      d(x,y,z);
      d(y,x,z);
    end;
    fs(1,0);
    writeln(a);
  end else
  begin
    r:=0;
    ff:=true;
    f1:=true;
    for i:=1 to n-1 do
    begin
      readln(x,y,z);
      r:=r+z;
      aa[x]:=z;
      op[i]:=z;
      if x<>y-1 then
        ff:=false;
      if x<>1 then
        f1:=false;
      d(x,y,z);
      d(y,x,z);
    end;
    if ff then
    begin
      l:=0;
      while l<=r do
      begin
        md:=(l+r) div 2;
        ss:=0;
        mk:=0;
        for i:=1 to n-1 do
        begin
          ss:=ss+aa[i];
          if ss>=md then
          begin
            inc(mk);
            ss:=0;
          end;
        end;
        if ss>=md then
          inc(mk);
        if mk>=m then
        begin
          a:=md;
          l:=md+1;
        end else
          r:=md-1;
      end;
      writeln(a);
    end else
    if f1 then
    begin
      ssoorrtt(1,n-1);
      l:=0;
      while l<=r do
      begin
        md:=(l+r) div 2;
        i:=1;
        j:=n-1;
        mk:=0;
        while (i<=j) and (op[j]>=md) do
        begin
          inc(mk);
          dec(j);
        end;
        while i<=j do
        begin
          while (i<j) and (op[i]+op[j]<md) do
            inc(i);
          if i>=j then
            break;
          inc(i);
          dec(j);
          inc(mk);
        end;
        if mk>=m then
        begin
          a:=md;
          l:=md+1;
        end else
          r:=md-1;
      end;
      writeln(a);
    end;
  end;
  close(input);close(output);
end.