var
  t,i,n,j,ans:longint;
  a:array[0..25005]of longint;
  f:array[0..25005]of boolean;
procedure qsort(l,r:longint);
var
  i,j,mid,p:longint;
begin
  i:=l;j:=r;mid:=a[random(r-l+1)+l];
  repeat
    while a[i]<mid do inc(i);
    while a[j]>mid do dec(j);
    if i<=j then
    begin
      p:=a[i];a[i]:=a[j];a[j]:=p;
      inc(i);dec(j);
    end;
  until i>j;
  if i<r then qsort(i,r);
  if l<j then qsort(l,j);
end;
begin
  assign(input,'money.in');reset(input);
  assign(output,'money.out');rewrite(output);
  readln(t);
  randomize;
  while t>0 do
  begin
    dec(t);
    readln(n);
    for i:=1 to n do read(a[i]);
    qsort(1,n);
    fillchar(f,sizeof(f),false);
    f[0]:=true;
    ans:=0;
    for i:=1 to n do
      if not f[a[i]] then
      begin
        f[a[i]]:=true;
        inc(ans);
        for j:=a[i] to a[n] do
          f[j]:=f[j] or f[j-a[i]];
      end;
    writeln(ans);
  end;
  close(input);
  close(output);
end.