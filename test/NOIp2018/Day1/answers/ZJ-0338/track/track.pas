type
  rec=record
    x,y:longint;
  end;
var
  a,du,up,head,next,val,vet,p:array[0..100005]of longint;
  mid,i,l,r,n,m,edgenum,u,v,w:longint;
procedure qsort(l,r:longint);
var
  i,j,mid,p:longint;
begin
  i:=l;j:=r;mid:=a[random(r-l+1)+l];
  repeat
    while a[i]>mid do inc(i);
    while a[j]<mid do dec(j);
    if i<=j then
    begin
      p:=a[i];a[i]:=a[j];a[j]:=p;
      inc(i);dec(j);
    end;
  until i>j;
  if i<r then qsort(i,r);
  if l<j then qsort(l,j);
end;
function chuli(u,num:longint):rec;
var
  t,i,e,v:longint;
begin
  e:=head[u];
  while e>0 do
  begin
    v:=vet[e];
    if v<>p[u] then
    begin
      inc(num);
      a[num]:=up[v]+val[e];
    end;
    e:=next[e];
  end;
  qsort(1,num);
  for t:=1 to num do if a[t]<mid then break;
  if a[num]>=mid then
  begin
    chuli.x:=num;
    chuli.y:=0;
    exit(chuli);
  end;
  {if mid=15 then
  begin
    writeln(u,' ',num);
    for i:=1 to num do write(a[i],' ');
    writeln;
  end;}
  for i:=num downto 1 do
  begin
    if i<=t then break;
    if a[t]+a[i]>=mid then inc(t);
    {if mid=15 then
    begin
      writeln(i,' ',t,' ',a[t]+a[i]);
    end;}
  end;
  chuli.x:=t-1;
  chuli.y:=a[t];
end;
function dfs(u:longint):longint;
var
  e,v,q,num,i:longint;
  t:rec;
begin
  if (u<>1)and(du[u]=1) then
  begin
    up[u]:=0;
    exit(0);
  end;
  q:=0;
  e:=head[u];
  num:=0;
  while e>0 do
  begin
    v:=vet[e];
    if v<>p[u] then
    begin
      p[v]:=u;
      q:=q+dfs(v);
    end;
    e:=next[e];
  end;
  t:=chuli(u,num);
  up[u]:=t.y;
  q:=q+t.x;
  exit(q);
end;
procedure addedge(u,v,w:longint);
begin
  inc(edgenum);
  next[edgenum]:=head[u];
  head[u]:=edgenum;
  val[edgenum]:=w;
  vet[edgenum]:=v;
end;
begin
  assign(input,'track.in');reset(input);
  assign(output,'track.out');rewrite(output);
  readln(n,m);
  randomize;
  //for i:=1 to n do head[i]:=0;
  for i:=1 to n-1 do
  begin
    readln(u,v,w);
    inc(du[u]);
    inc(du[v]);
    addedge(u,v,w);
    addedge(v,u,w);
  end;
  l:=1;r:=500000001;
  while l<r do
  begin
    mid:=(l+r+1)shr 1;
    w:=dfs(1);
    //writeln(w,' ',mid);
    if w>=m then l:=mid else r:=mid-1;
  end;
  writeln(l);
  close(input);
  close(output);
end.
