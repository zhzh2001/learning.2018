#include<bits/stdc++.h>
using namespace std;

#define MAX 201
#define MAXN 30001

int T;
int n;
int a[MAX];
int f[MAXN];

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		memset(f,0,sizeof(f));
		int cnt,now_max=0;
		scanf("%d",&n);
		for(int i=1;i<=n;++i){
			scanf("%d",&a[i]);
			now_max=max(now_max,a[i]);
		}
		f[0]=1;
		sort(a+1,a+n+1);
		for(int i=1;i<=n;++i){
			for(int j=0;j+a[i]<=now_max;j++){
				if(f[j])
					f[j+a[i]]++;
			}
		}
		/*
		for(int i=1;i<MAXN;i++){
			if(!f[i])
			printf("%d ",i);
			 
		}*/
		cnt=n;
		for(int i=1;i<=n;++i){
			if(f[a[i]]>1)
				--cnt;
		}
		printf("%d\n",cnt);
	}
}
