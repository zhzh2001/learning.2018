#include<bits/stdc++.h>
using namespace std;

#define MAX 50001

int n,m;

struct node{
	int u,v,len;
}s[MAX];

int cmp(node a,node b){
	return a.len<b.len;
}

namespace _edge{
	int cnt,fst[MAX];
	struct edge{
		int to,nxt;
		int len;
	}e[MAX];
	void link(int u,int v,int len){
		e[++cnt].nxt=fst[u];
		fst[u]=cnt;
		e[cnt].to=v,e[cnt].len=len;
	}
}using namespace _edge;


/////////////////////////////////// m==1
int dis[2][MAX];

void dfs(int u,int lst,int p){
	for(int i=fst[u];i;i=e[i].nxt){
		int v=e[i].to,w=e[i].len;
		if(v!=lst){
			dis[p][v]=dis[p][u]+w;
			dfs(v,u,p);
		}
	}
}
////////////////////////////// lian
int sum[MAX];

int check(int x){
	int lst=0,now=0;
	for(int i=1;i<n;++i){
		if(sum[i]-sum[lst]<x)
			continue;
		lst=i;
		++now;
	}
	if(now<m)
		return 0;
	return 1;
}

int erfen(int l,int r){
	if(l>=r){
		printf("%d",r-1);
		return 0;
	}
	int mid=(l+r)>>1;
	int flag=check(mid);
	if(flag)
		erfen(mid+1,r);
	else
		erfen(l,mid);
}

//////////////////////////

priority_queue<int,vector<int>,greater<int> >q;

int flag_lian=1,flag_juhua=1;
int main(){		//55'
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;++i){
		scanf("%d%d%d",&s[i].u,&s[i].v,&s[i].len);
		if(s[i].u+1!=s[i].v)
			flag_lian=0;
		if(s[i].u!=1)
			flag_juhua=0;
	}
	/*
	sort(s+1,s+n,cmp);*/
	if(m==1){
		for(int i=1;i<n;++i){
			link(s[i].u,s[i].v,s[i].len);
			link(s[i].v,s[i].u,s[i].len);
		}
		dfs(1,0,0);
		int s=0,t=0;
		for(int i=1;i<=n;++i){
			s=dis[0][s]<dis[0][i]?i:s;
		}
		dfs(s,0,1);
		for(int i=1;i<=n;++i){
			t=dis[1][t]<dis[1][i]?i:t;
		}
		printf("%d",dis[1][t]);
	}
	else if(flag_lian){
		for(int i=1;i<n;++i)
			sum[i]=sum[i-1]+s[i].len;
		erfen(sum[1],sum[n-1]);
	}
	else if(flag_juhua){
		int ans=0;
		sort(s+1,s+n,cmp);
		int cnt=n-1;
		for(int i=1;i<n;++i){
			if(cnt>m){
				q.push(s[i].len+s[n-i].len);
				//printf("%d ",s[i].len+s[n-i].len);
				cnt--;
				
			}
			else{
				if(i>m)
					break;
				q.push(s[i].len);
				//printf("%d ",s[i].len);
			}
		}
		printf("%d",q.top());
	}
}
