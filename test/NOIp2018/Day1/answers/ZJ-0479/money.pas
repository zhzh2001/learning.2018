var
        t,h,i,j,n,ans,flag:longint;
        f,a:array[-5..25005]of longint;
procedure qsort(l,r:longint);
var
        i,j,mid,b:longint;
begin
        i:=l; j:=r;
        mid:=a[(l+r)div 2];
        while i<=j do
        begin
                while a[i]<mid do inc(i);
                while a[j]>mid do dec(j);
                if i<=j then
                begin
                        b:=a[i]; a[i]:=a[j]; a[j]:=b;
                        inc(i); dec(j);
                end;
        end;
        if l<j then qsort(l,j);
        if i<r then qsort(i,r);
end;
function dfs(t:longint):longint;
var
        i,flag:longint;
begin
        if f[t]=1 then exit(1);
        flag:=0;
        for i:=t div 2 downto 1 do
        begin
                if f[i]=1 then
                begin
                        flag:=dfs(t-i);
                        if flag=1 then
                        begin
                                f[t]:=1;
                                exit(f[t]);
                        end;
                end;
        end;
        exit(0);
end;
begin
        assign(input,'money.in'); reset(input);
        assign(output,'money.out'); rewrite(output);
        readln(t);
        for h:=1 to t do
        begin
                readln(n);
                for i:=1 to n do read(a[i]);
                for i:=1 to 25001 do f[i]:=0;
                qsort(1,n);
                f[0]:=1;
                ans:=0;
                for i:=1 to n do
                begin
                        flag:=dfs(a[i]);
                        if flag=0 then
                        begin
                                f[a[i]]:=1;
                                ans:=ans+1;
                        end;
                        for j:=1 to a[n] div a[i] do f[a[i]*j]:=1;
                end;
                writeln(ans);
        end;
        close(input); close(output);
end.