var
        n,i:longint;
        ans,t,k:int64;
begin
        assign(input,'road.in'); reset(input);
        assign(output,'road.out'); rewrite(output);
        readln(n);
        read(t);
        ans:=t;
        k:=t;
        for i:=2 to n do
        begin
                read(t);
                if t>k then ans:=ans+(t-k);
                k:=t;
        end;
        writeln(ans);
        close(input); close(output);
end.