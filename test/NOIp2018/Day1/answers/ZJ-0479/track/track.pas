var
        n,m,i,x,y,z,num,sum,ans,flag,l,r,mid:longint;
        g,h,first,now,next,a,b,c:array[-5..100005]of longint;
function solve(t,b:longint):longint;
var
        root,sum,max1,max2:longint;
begin
        root:=first[t];
        max1:=0;
        max2:=0;
        while root<>0 do
        begin
                if g[root]<>b then
                begin
                        sum:=solve(g[root],t)+h[root];
                        if sum>max1 then begin max2:=max1; max1:=sum; end
                        else if sum>max2 then max2:=sum;
                end;
                root:=next[root];
        end;
        if max1+max2>ans then ans:=max1+max2;
        exit(max1);
end;
function check(t:longint):longint;
var
        sum,i,j,min:longint;
begin
        sum:=m;
        for i:=1 to n-1 do
        begin
                if b[i]=0 then
                begin
                        b[i]:=1;
                        if a[i]>=t then sum:=sum-1
                        else
                        begin
                                min:=0;
                                for j:=1 to n-1 do
                                begin
                                        if (b[j]=0) and (a[i]+a[j]>=t) and (a[j]<a[min]) then min:=j;
                                end;
                                if min<>0 then
                                begin
                                        b[min]:=1;
                                        sum:=sum-1;
                                end;
                        end;
                end;
        end;
        for i:=1 to n do b[i]:=0;
        if sum<=0 then exit(1)
        else exit(0);
end;
function mild(t:longint):longint;
var
        sum,he,i:longint;
begin
        sum:=m;
        he:=0;
        for i:=2 to n do
        begin
                he:=he+c[i];
                if he>=t then
                begin
                        sum:=sum-1;
                        he:=0;
                end;
        end;
        if sum<=0 then exit(1)
        else exit(0);
end;
begin
        assign(input,'track.in'); reset(input);
        assign(output,'track.out'); rewrite(output);
        readln(n,m);
        flag:=1;
        a[0]:=100000000;
        for i:=1 to n-1 do
        begin
                readln(x,y,z);
                if x<>1 then flag:=0;
                a[i]:=z;
                r:=r+z;
                c[y]:=z;
                inc(num); g[num]:=y; h[num]:=z;
                if first[x]=0 then
                begin
                        first[x]:=num;
                        now[x]:=num;
                end
                else
                begin
                        next[now[x]]:=num;
                        now[x]:=num;
                end;
                inc(num); g[num]:=x; h[num]:=z;
                if first[y]=0 then
                begin
                        first[y]:=num;
                        now[y]:=num;
                end
                else
                begin
                        next[now[y]]:=num;
                        now[y]:=num;
                end;
        end;
        if m=1 then
        begin
                sum:=solve(1,0);
                if sum>ans then ans:=sum;
                writeln(ans);
        end
        else
        if flag=1 then
        begin
                l:=0;
                while l<r do
                begin
                        mid:=(l+r+1) div 2;
                        if check(mid)=1 then l:=mid
                        else r:=mid-1;
                end;
                writeln(l);
        end
        else
        begin
                l:=0;
                while l<r do
                begin
                        mid:=(l+r+1) div 2;
                        if mild(mid)=1 then l:=mid
                        else r:=mid-1;
                end;
                writeln(l);
        end;
        close(input); close(output);
end.