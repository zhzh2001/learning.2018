#include<cstdlib>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<set>
#include<iterator>
using namespace std;
const int maxn=50100;
int head[maxn],nxt[maxn<<1],ver[maxn<<1],val[maxn<<1],tot;
inline void addedge(int a,int b,int c)
{
	nxt[++tot]=head[a];ver[tot]=b;val[tot]=c;head[a]=tot;
	nxt[++tot]=head[b];ver[tot]=a;val[tot]=c;head[b]=tot;
}
int n,m;
namespace solve1
{
	int f[maxn],ans=0;
	inline void treedp(int x,int fa)
	{
		int mx=0,sx=0;
		for(int i=head[x];i;i=nxt[i])
		{
			int y=ver[i];if(y==fa) continue;
			treedp(y,x);int ff=f[y]+val[i];
			f[x]=max(ff,f[x]);
			if(ff>mx) sx=mx,mx=ff;
			else if(ff>sx) sx=ff;
		}
		ans=max(ans,mx+sx);
	}
	inline void solve()
	{
		treedp(1,0);
		printf("%d\n",ans);
	}
}//m=1
namespace solve2
{
	multiset<int> s;
	inline bool check(int x)
	{
		s.clear();
		for(int i=head[1];i;i=nxt[i]) s.insert(val[i]);
		int cnt=0;
		while(s.size())
		{
			multiset<int>::iterator it=--s.end();
			int v=*it;s.erase(it);
			if(v>=x){cnt++;continue;}
			it=s.lower_bound(x-v);
			if(it==s.end()) break;
			s.erase(it);cnt++;
		}
		return cnt>=m;
	}
	inline void solve()
	{
		int l=1,r=500000000,ans=0;
		while(l<=r)
		{
			int mid=(l+r)>>1;
			if(check(mid)) ans=max(ans,mid),l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}//flower
namespace solve3
{
	int v[maxn];
	inline bool check(int x)
	{
		int cnt=0,vv=0;
		for(int i=1;i<n;i++)
		{
			vv+=v[i];
			if(vv>=x){cnt++;vv=0;}
		}
		return cnt>=m;
	}
	inline void solve()
	{
		for(int i=1;i<n;i++) for(int j=head[i];j;j=nxt[j]) if(ver[j]==i+1) v[i]=val[j];
		int l=1,r=500000000,ans=0;
		while(l<=r)
		{
			int mid=(l+r)>>1;
			if(check(mid)) ans=max(ans,mid),l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}//chain
namespace bf
{
	int fa[maxn],dep[maxn],lval[maxn];
	inline void getfa(int x,int fat)
	{
		fa[x]=fat;dep[x]=dep[fat]+1;
		for(int i=head[x];i;i=nxt[i])
		{
			int y=ver[i];if(y==fat) continue;
			getfa(y,x);lval[y]=val[i];
		}
	}
	int ans=0;bool cov[maxn];
	inline int cover(int x,int y,int flag)
	{
		if(dep[x]<dep[y]) swap(x,y);int ans=0;
		while(dep[x]>dep[y]) cov[x]=flag,ans+=lval[x],x=fa[x];
		while(x!=y)
		{
			cov[x]=cov[y]=flag;
			ans+=lval[x]+lval[y];
			x=fa[x];y=fa[y];
		}
		return ans;
	}
	inline bool hascover(int x,int y)
	{
		if(dep[x]<dep[y]) swap(x,y);
		while(dep[x]>dep[y])
		{
			if(cov[x]) return 1;
			x=fa[x];
		}
		while(x!=y)
		{
			if(cov[x]||cov[y]) return 1;
			x=fa[x];y=fa[y];
		}
		return 0;
	}
	int nowmin=0;
	int p[maxn];
	inline void dfs(int used,int nowmin,int lst)
	{
		if(nowmin<=ans) return;
		if(used>m){ans=max(ans,nowmin);return;}
		for(int i=lst;i<=n;i++) for(int j=i+1;j<=n;j++)
		{
			int ti=p[i],tj=p[j];
			if(hascover(ti,tj)) continue;
			int nxt=min(nowmin,cover(ti,tj,1));
			dfs(used+1,nxt,i);
			cover(ti,tj,0);
		}
	}
	inline void solve()
	{
		getfa(1,0);
		for(int i=1;i<=n;i++) p[i]=i;
		random_shuffle(p+1,p+n+1);
		dfs(1,1000000000,1);
		printf("%d\n",ans);
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool Flag2=1,Flag3=1;
	for(int i=1,a,b,c;i<n;i++)
	{
		scanf("%d%d%d",&a,&b,&c),addedge(a,b,c);
		if(b!=a+1) Flag3=0;
		if(a!=1) Flag2=0;
	}
	if(m==1)
	{
		solve1::solve();
		return 0;
	}
	if(Flag2)
	{
		solve2::solve();
		return 0;
	}
	if(Flag3)
	{
		solve3::solve();
		return 0;
	}
	bf::solve();
	return 0;
}
