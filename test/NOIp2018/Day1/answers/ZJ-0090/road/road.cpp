#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=300100;
int a[maxn];int st[maxn][20],p[maxn][20];
int LLog2[maxn];int n;
inline int query(int l,int r)
{
	int t=LLog2[r-l+1];
	return min(st[l][t],st[r-(1<<t)+1][t]);
}
inline int query_from(int l,int r)
{
	int t=LLog2[r-l+1];
	if(st[l][t]<=st[r-(1<<t)+1][t]) return p[l][t];
	return p[r-(1<<t)+1][t];
}
long long ans=0;
inline void solve(int l,int r,int nowdown)
{
	if(l>r) return;
	int Mn=query(l,r),Mf=query_from(l,r);
	ans+=Mn-nowdown;
	solve(l,Mf-1,Mn);
	solve(Mf+1,r,Mn);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++) scanf("%d",a+i);
	LLog2[1]=0;
	for(int i=2;i<=n;i++) LLog2[i]=LLog2[i>>1]+1,st[i][0]=a[i],p[i][0]=i;
	st[1][0]=a[1],p[1][0]=1;
	for(int j=1;j<=LLog2[n];j++) for(int i=1;i<=n;i++)
	{
		st[i][j]=min(st[i][j-1],st[i+(1<<(j-1))][j-1]);
		if(st[i][j]==st[i][j-1]) p[i][j]=p[i][j-1];
		else p[i][j]=p[i+(1<<(j-1))][j-1];
	}
	solve(1,n,0);
	printf("%lld",ans);
	return 0;
}
/*
6
4 3 2 5 3 5
*/
