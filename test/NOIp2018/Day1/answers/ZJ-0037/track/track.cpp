#include <bits/stdc++.h>
#define N 52000
#define FOR(a,b,c) for(int a=(b);a<=(c);a++)
using namespace std;
bool mmm1;
void read(int &x){
	int ss=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0'&&c<='9')ss=ss*10+c-'0',c=getchar();
	x=ss*f;
}
int n,m,head[N],ver[2*N],Next[2*N],edge[2*N],tot;
long long sum;
void add(int x,int y,int w){
	ver[tot]=y,edge[tot]=w;
	Next[tot]=head[x];
	head[x]=tot++;
}
struct Pzhi{
	long long d[N];
	int b1,b2;
	void dfs(int x,int f){
		for(int e=head[x];~e;e=Next[e]){
			int v=ver[e];if(v==f)continue;
			d[v]=d[x]+edge[e];
			dfs(v,x);
		}
	}
	void solve(){
		d[1]=0,dfs(1,-1);
		FOR(i,1,n)if(d[i]>d[b1])b1=i;
		d[b1]=0,dfs(b1,-1);
		FOR(i,1,n)if(d[i]>d[b2])b2=i;
		printf("%lld\n",d[b2]);
	}
}pzhi;
struct Pju{
	int used[N],cnt,num[12000];
	struct node{
		int v,id;
	}ddi[N],b[N];
	void Soo(node *a,int len){
		memset(num,0,sizeof(num));
		FOR(i,1,len)num[a[i].v]++;
		FOR(i,1,10000)num[i]+=num[i-1];
		for(int i=len;i>=1;i--)b[num[a[i].v]--]=a[i];
		memcpy(a,b,sizeof(b));
	}
	bool pd(long long x){
		memset(used,0,sizeof(used));
		int ans=0;cnt=0;
		for(int e=head[1];~e;e=Next[e]){
			if(edge[e]>=x){ans++;continue;}
			ddi[++cnt]=(node){edge[e],ver[e]};
		}
		Soo(ddi,cnt);
		int wei=1;
		for(int i=cnt;i>=1;i--){
			if(used[ddi[i].id])continue;
			while(wei<cnt&&(used[ddi[wei].id]||ddi[wei].v+ddi[i].v<x||wei==i))
				wei++;
			if(ddi[wei].v+ddi[i].v>=x&&!used[ddi[wei].id]){
				used[ddi[wei].id]=used[ddi[i].id]=1;
				ans++;
			}
			else break;
		}
		return ans>=m;
	}
	void solve(){
		long long l=1,r=sum;
		while(l<r){
			long long mid=(l+r+1)>>1;
			if(pd(mid))l=mid;
			else r=mid-1;
		}
		printf("%lld\n",l);
	}
}pju;
struct Plian{
	int ans;
	void dfs(int x,int f,long long nowh,long long bz){
		for(int e=head[x];~e;e=Next[e]){
			int v=ver[e];if(v==f)continue;
			if(nowh+edge[e]>=bz)ans++,dfs(v,x,0,bz);
			else dfs(v,x,nowh+edge[e],bz);
		}
	}
	bool pd(long long x){
		ans=0,dfs(1,-1,0,x);
		return ans>=m;
	}
	void solve(){
		long long l=1,r=sum;
		while(l<r){
			long long mid=(l+r+1)>>1;
			if(pd(mid))l=mid;
			else r=mid-1;
		}
		printf("%lld\n",l);
	}
}plian;
struct Pshui{
	void solve(){
		long long mx=0;
		for(int i=1;i<=n;i++)
			for(int e=head[i];~e;e=Next[e])
				mx=max(mx,1ll*edge[e]);
		printf("%lld\n",mx);
	}
}pshui;
struct Pqian{
	void solve(){
		cout<<sum/m<<endl;
	}
}pqian;
bool mmm2;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
//	cout<<(&mmm2-&mmm1)/1024.0/1024<<endl;
	memset(head,-1,sizeof(head));
	read(n),read(m);
	int fl1=1,fl2=1;
	FOR(i,1,n-1){
		int x,y,w;read(x),read(y),read(w);
		add(x,y,w),add(y,x,w);sum+=w;
		if(x!=1&&y!=1)fl1=0;
		if(y!=x+1&&x!=y+1)fl2=0;
	}
	if(m==1)pzhi.solve();
	else if(fl1)pju.solve();
	else if(fl2)plian.solve();
	else if(m==n-1)pshui.solve();
	else pqian.solve();
	return 0;
}
