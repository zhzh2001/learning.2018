#include <bits/stdc++.h>
#define N 120000
#define FOR(a,b,c) for(int a=(b);a<=(c);a++)
void read(int &x){
	int ss=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0'&&c<='9')ss=ss*10+c-'0',c=getchar();
	x=ss*f;
}
using namespace std;
int n,a[N];

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	FOR(i,1,n)read(a[i]);
	long long ans=0;
	FOR(i,1,n)if(a[i]>a[i-1])ans+=a[i]-a[i-1];
	cout<<ans<<endl;
	return 0;
}
