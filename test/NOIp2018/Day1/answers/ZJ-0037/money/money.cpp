#include <bits/stdc++.h>
#define N 120
#define M 26000
#define FOR(a,b,c) for(int a=(b);a<=(c);a++)
void read(int &x){
	int ss=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0'&&c<='9')ss=ss*10+c-'0',c=getchar();
	x=ss*f;
}
using namespace std;
int n,a[N],used[M];

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t;scanf("%d",&t);
	while(t--){
		read(n);int mx=0;
		FOR(i,1,n)read(a[i]),mx=max(mx,a[i]);
		sort(a+1,a+n+1);
		memset(used,0,sizeof(used));
		used[0]=1;int ans=n;
		for(int i=1;i<=n;i++){
			if(used[a[i]]){ans--;continue;}
			for(int j=a[i];j<=mx;j++)
				if(used[j-a[i]])used[j]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
