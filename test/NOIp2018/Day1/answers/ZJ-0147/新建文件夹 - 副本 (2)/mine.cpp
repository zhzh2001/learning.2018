#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
using namespace std;
const int N=5e4+10;
int m,n,t,cnt,mi,ans;
struct edge{
	int to,nxt,dis;
	bool operator<(const edge b)const
	{
		return to>b.to;
	}
}e[N<<1];int head[N],a[N];
int flag1,flag2;
struct node{
	int id,len;
	node(int id=0,int len=0):id(id),len(len){}
	bool operator<(const node b)const
	{
		return len>b.len;
	}
};priority_queue<node>q;
int d[N];

template<typename T>
inline void re(T &N)
{
	char c; int f=1;
	while((c=getchar())< '0'||c> '9')if(c=='-')f=-1;N=c-'0';
	while((c=getchar())>='0'&&c<='9')N=N*10+c-'0';
}
inline void add(int x,int y,int dis)
{
	e[++t].to=y;e[t].nxt=head[x];head[x]=t;e[t].dis=dis;
	e[++t].to=x;e[t].nxt=head[y];head[y]=t;e[t].dis=dis;
}
inline int dij()
{
	q.push(node(1,0));
	memset(d,37,sizeof(d));
	d[1]=0;
	while(!q.empty())
	{
		node k=q.top();q.pop();
		if(d[k.id]<k.len)continue;
		int h=k.id;
		for(int i=head[h];i;i=e[i].nxt)
		{
			int to=e[i].to;
			if(d[to]>d[h]+e[i].dis)
			{
				d[to]=d[h]+e[i].dis;
				q.push(node(to,d[to]));
			}
		}
	}
	int ma=1;
	for(int i=1;i<=n;i++)if(d[ma]<d[i])ma=i;
	q.push(node(ma,0));
	memset(d,37,sizeof(d));
	d[ma]=0;
	while(!q.empty())
	{
		node k=q.top();q.pop();
		if(d[k.id]<k.len)continue;
		int h=k.id;
		for(int i=head[h];i;i=e[i].nxt)
		{
			int to=e[i].to;
			if(d[to]>d[h]+e[i].dis)
			{
				d[to]=d[h]+e[i].dis;
				q.push(node(to,d[to]));
			}
		}
	}
	for(int i=1;i<=n;i++)if(d[ma]<d[i])ma=i;
	return d[ma];
}
inline int judge(int mid)
{
	int sum=0,tmp=0;
	for(int i=1;i<=cnt;i++)
	{
		tmp+=a[i];
		if(tmp>=mid)sum++,tmp=0;
	}
	return sum>=m;
}
void dfs(int x,int p)
{
	for(int i=head[x];i;i=e[i].nxt)
	{
		int to=e[i].to;
		if(to==p)continue;
		a[++cnt]=e[i].dis;
		dfs(to,x);
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
//	freopen("data.txt","r",stdin);
	re(n),re(m);flag1=flag2=1;
	for(int i=1,x,y,dis;i< n;i++)
	{
		re(x),re(y),re(dis);
		add(x,y,dis);
		flag1&=(x==1);
		flag2&=(y-x==1);
		a[++cnt]=dis;
	}
	if(m==1)printf("%d\n",dij());
	else if(flag1)
	{
		sort(a+1,a+cnt+1);
		int l=cnt-(m<<1)+1,r=cnt;
		mi=1e9;
		while(l<=r)
		{
			mi=min(a[r]+a[l],mi);
			l++;r--;
		}
		printf("%d\n",mi);
	}
	else if(flag2)
	{
		cnt=0;
		dfs(1,0);
		int l=0,r=1e9+7;
		while(l<=r)
		{
			int mid=(l+r)>>1;
			if(judge(mid))l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
	return 0;
}

