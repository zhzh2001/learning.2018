#include<cstdio>
#include<algorithm>
#include<cstring>
#define ls(x) x<<1
#define rs(x) x<<1|1
using namespace std;
const int N=1e6+10,maxn=1e9+7;
int m,n,t,mi;long long sum,k;
int a[N];
struct node{
	int ans,id;
	node(int ans=0,int id=0):ans(ans),id(id){}
	bool operator<(const node b)const
	{
		return ans<b.ans||(ans==b.ans&&id<b.id);
	}
}ans[N];int tag[N];
struct edge{
	int nxt,pre,data;
}e[N];

template<typename T>
inline void re(T &N)
{
	char c; int f=1;
	while((c=getchar())< '0'||c> '9')if(c=='-')f=-1;N=c-'0';
	while((c=getchar())>='0'&&c<='9')N=N*10+c-'0';N*=f;
}
inline node min(node a,node b){return a<b?a:b;}
inline void push_up(int p)
{
	ans[p]=min(ans[ls(p)],ans[rs(p)]);
}
inline void build(int p,int l,int r)
{
	if(l==r){ans[p].ans=a[l];ans[p].id=l;return;}
	int mid=(l+r)>>1;
	build(ls(p),l,mid);
	build(rs(p),mid+1,r);
	push_up(p);
}
inline void f(int p,int l,int r,int k)
{
	ans[p].ans+=k;
	tag[p]+=k;
}
inline void push_down(int p,int l,int r)
{
	if(tag[p])
	{
		int mid=(l+r)>>1;
		f(ls(p),l,mid,tag[p]);
		f(rs(p),mid+1,r,tag[p]);
		tag[p]=0;
	}
}
void update(int p,int l,int r,int x,int y,int k)
{
	if(x<=l&&r<=y){ans[p].ans+=k;tag[p]+=k;return;}
	push_down(p,l,r);
	int mid=(l+r)>>1;
	if(x<=mid)update(ls(p),l,mid,x,y,k);
	if(mid< y)update(rs(p),mid+1,r,x,y,k);
	push_up(p);
}
node query(int p,int l,int r,int x,int y)
{
	node s=node(maxn,0);
	if(x<=l&&r<=y)return ans[p];
	push_down(p,l,r);
	int mid=(l+r)>>1;
	if(x<=mid)s=min(s,query(ls(p),l,mid,x,y));
	if(mid< y)s=min(s,query(rs(p),mid+1,r,x,y));
	return s;
}
inline void init()
{
	for(int i=0;i<=n;i++)e[i].nxt=i+1;
	for(int i=1;i<=n;i++)e[i].pre=i-1;
	for(int i=1;i<=n;i++)e[i].data=i;
}
inline void del(int p)
{
	e[e[p].pre].nxt=e[p].nxt;
	e[e[p].nxt].pre=e[p].pre;
}
int main()
{
//	freopen("road.in","r",stdin);
//	freopen("road.out","w",stdout);
	freopen("data.txt","r",stdin);
	re(n);
	for(int i=1;i<=n;i++)re(a[i]);
	build(1,1,n);node s;
	init();
	for(int i=1;i<=n;i++)
	{
		int j=e[0].nxt;
		while(e[e[j].nxt].data-e[j].data==1)j=e[j].nxt;
		s=query(1,1,n,e[e[0].nxt].data,e[j].data);
		sum+=s.ans;
		if(s.ans)update(1,1,n,e[e[0].nxt].data,e[j].data,-s.ans);
		del(s.id);
	}
	printf("%lld\n",sum);
	return 0;
}


