#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
#define R register 
using namespace std;
const int N=1e4+10;
int m,n,t,T,ma=0,ans=0;
int a[N],v[N],vis[N*25];
struct queue{
	int a[N*25],t;
	void push(int k)
	{
		a[++t]=k;int now=t;
		while(now>1)
		{
			if(a[now]>=a[now>>1])break;
			swap(a[now],a[now>>1]);
			now>>=1;
		}
	}
	void maintain(int root)
	{
		int l=root<<1,r=root<<1|1;
		int mi=root;
		if(l<=t&&a[mi]>a[l])mi=l;
		if(r<=t&&a[mi]>a[r])mi=r;
		if(mi!=root)
		{
			swap(a[mi],a[root]);
			maintain(mi);
		}
	}
	int pop()
	{
		int tmp=a[1];
		a[1]=a[t--];
		maintain(1);
		return tmp;
	}
}q;

template<typename T>
inline void re(T &N)
{
	char c; while((c=getchar())< '0'||c> '9');
	N=c-'0';while((c=getchar())>='0'&&c<='9')N=N*10+c-'0';
}
inline void judge(int k)
{
	int l=1,r=n;
	while(l<=r)
	{
		int mid=(l+r)>>1;
		if(a[mid]==k)
		{
			v[mid]=1;
			return;
		}
		else if(a[mid]<k)l=mid+1;
		else r=mid-1;
	}
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
//	freopen("data.txt","r",stdin);
	re(T);
	while(T--)
	{
		re(n);ans=ma=0;
		memset(v,0,sizeof(v));
		memset(vis,0,sizeof(vis));
		for(int i=1;i<=n;i++)re(a[i]),ma=max(ma,a[i]);
		sort(a+1,a+n+1);
		for(R int i=1;i<=n;i++)q.push(a[i]);
		while(q.t)
		{
			int h=q.pop();
			for(R int i=1;i<=n;i++)
			{
				if(h+a[i]> ma)break;
				if(vis[h+a[i]]||v[i])continue;vis[h+a[i]]=1;
				judge(h+a[i]);
				q.push(h+a[i]);
			}
		}
		for(int i=1;i<=n;i++)if(!v[i])ans++;
		printf("%d\n",ans);
	}
	return 0;
}

