#include<bits/stdc++.h>
#define N 50005
#define LL long long
using namespace std;
bool emm1;
template<class T>inline void chkmin(T &x,T y){if(x>y)x=y;}
template<class T>inline void chkmax(T &x,T y){if(x<y)x=y;}
template<class T>inline void Rd(T &x){
	x=0;char c=getchar();bool f=0;
	while(c<'0'||c>'9'){if(c=='-')f^=1;c=getchar();}
	while(c>='0'&&c<='9'){x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	if(f)x=-x;
}
int n,m,hd[N],to[N<<1],nt[N<<1],len[N<<1],ct,E[N];
void Add(int x,int y,int z){nt[++ct]=hd[x];hd[x]=ct;to[ct]=y;len[ct]=z;}

struct Pm_1{
	int dis[N],Mx;
	void dfs(int x,int f){
		if(Mx==0||dis[x]>dis[Mx])Mx=x;
		for(int i=hd[x];i;i=nt[i]){
			int v=to[i],w=len[i];if(v==f)continue;
			dis[v]=dis[x]+w;dfs(v,x);
		}
	}
	void solve(){
		dis[1]=0;Mx=0;
		dfs(1,-1);
		dis[Mx]=0;int s=Mx;Mx=0;
		dfs(s,-1);
		printf("%d\n",dis[Mx]);
	}
}pm1;

struct Pa_1{
	void solve(){
		sort(E+1,E+n);
		int ans=0x3f3f3f3f;
		if(2*m<=n-1){
			int l=n-2*m,r=n-1;
			while(l<r&&m>0){
				chkmin(ans,E[l]+E[r]);
				l++;r--;m--;
			}
		}
		else {
			int l=1,r=n-1;
			while((r-l+1)<m*2&&m>0){
				chkmin(ans,E[r]);
				r--;m--;
			}
			l=r-2*m+1;
			while(l<r&&m>0){
				chkmin(ans,E[l]+E[r]);
				l++,r--;m--;
			}
		}
		printf("%d\n",ans);
	}
}pa1;


struct Plian{
	bool chk(LL x){
		int cnt=0,sum=0;
		for(int i=1;i<n;i++){
			sum+=E[i];
			if(sum>=x){
				cnt++;
				if(cnt>=m)return 1;
				sum=0;
			}
		}
		return 0;
	}
	void solve(){
		LL l=1,r=500000003,ans;
		while(l<=r){
			LL mid=(l+r)>>1;
			if(chk(mid))l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%lld\n",ans);
	}
}pl;
bool op1,op2;
//	 ��  a=1
bool emm2;
int main(){//file LL �ڴ� Խ�� 
//	cout<<"The memory is "<<(&emm2-&emm1)/1024.0/1024.0<<endl;

	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	Rd(n),Rd(m);
	op1=op2=1;
	for(int i=1,a,b;i<n;i++){
		Rd(a),Rd(b),Rd(E[i]);
		Add(a,b,E[i]),Add(b,a,E[i]);
		if(b!=a+1)op1=0;
		if(a!=1)op2=0;
	}
	if(op1)pl.solve();
	else if(m==1)pm1.solve();
	else if(op2)pa1.solve();
	else pm1.solve();
	return 0;
}
