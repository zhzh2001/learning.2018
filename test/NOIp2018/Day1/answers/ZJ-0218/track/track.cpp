#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for(int i=(a),i_##END_=(b);i<=i_##END_;++i)
#define REP(i,a,b) for(int i=(a),i_##END_=(b);i>=i_##END_;--i)

#define M 50005
#define N
#define debug() cerr<<"Why So Serious?"<<endl
#define fi first
#define se second
typedef long long ll;
typedef unsigned long long ull;
typedef double db;
typedef pair<int,int> Pa;
void Rd(int &x){
	x=0;char c;
	while(c=getchar(),c<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),c>47);
}
struct edge{
	int to,nxt,cost;
	edge(int _to=0,int _nxt=0,int _cost=0):to(_to),nxt(_nxt),cost(_cost){}
}G[M<<1];
int head[M];
int tol;
void addedge(int a,int b,int c){
	G[tol]=edge(b,head[a],c);
	head[a]=tol++;
}
int n,m;
namespace P1{
	int ans;
	int dfs(int x,int f){
		int mx1=0,mx2=0;
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(y==f)continue;
			int mx=dfs(y,x)+G[i].cost;
			if(mx>mx1){
				mx2=mx1;
				mx1=mx;
			}else if(mx>mx2){
				mx2=mx;
			}
		}
		ans=max(ans,mx1+mx2);
		return mx1;
	}
	void Solve(){
		dfs(1,0);
		printf("%d\n",ans);
	}
}
namespace P2{
	int res;
	int A[M],B[M];
	int now;
	#define ls p<<1
	#define rs p<<1|1
	#define lvis L,mid,ls
	#define rvis mid+1,R,rs
	int Mx[M<<2];
	void Build(int L,int R,int p){
		Mx[p]=B[R];
		if(L==R)return;
		int mid=(L+R)>>1;
		Build(lvis);
		Build(rvis);
	}
	int Query(int L,int R,int p,int k){
		if(L==R)return L;
		int mid=(L+R)>>1;
		if(Mx[ls]>=k)return Query(lvis,k);
		else return Query(rvis,k);
	}
	void Modify(int L,int R,int p,int x){
		if(L==R){
			Mx[p]=0;
			return;
		}
		int mid=(L+R)>>1;
		if(x<=mid)Modify(lvis,x);
		else Modify(rvis,x);
		Mx[p]=max(Mx[ls],Mx[rs]);
	}
	int Mark[M];
	int dfs(int x,int f){
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(y==f)continue;
			A[y]=dfs(y,x)+G[i].cost;
		}
		int cnt=0;
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(y==f)continue;
			if(A[y]>=now)res++;
			else B[++cnt]=A[y];
		}
		if(cnt){
			sort(B+1,B+cnt+1);
			Build(1,cnt,1);
			FOR(i,1,cnt){
				Modify(1,cnt,1,i);
				if(Mx[1]>=now-B[i]&&Mark[i]!=x){
					res++;
					int pos=Query(1,cnt,1,now-B[i]);
					Modify(1,cnt,1,pos);
					Mark[pos]=Mark[i]=x;
				}
			}
			REP(i,cnt,1)if(Mark[i]!=x)return B[i];
		}
		return 0;
	}
	bool Judge(int x){
		res=0;
		now=x;
		FOR(i,1,n)Mark[i]=0;
		dfs(1,0);
		return res>=m;
	}
	void Solve(){
		int L=1,R=5e8,ans=1;
		while(L<=R){
			int mid=(L+R)>>1;
			if(Judge(mid))ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	Rd(n);Rd(m);
	FOR(i,1,n)head[i]=-1;
	int a,b,c;
	FOR(i,1,n-1){
		Rd(a);Rd(b);Rd(c);
		addedge(a,b,c);
		addedge(b,a,c);
	}
	if(m==1)P1::Solve();
	else P2::Solve();
	return 0;
}
