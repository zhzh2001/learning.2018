#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for(int i=(a),i_##END_=(b);i<=i_##END_;++i)
#define REP(i,a,b) for(int i=(a),i_##END_=(b);i>=i_##END_;--i)

#define M 100005
#define N
#define debug() cerr<<"Why So Serious?"<<endl
#define fi first
#define se second
typedef long long ll;
typedef unsigned long long ull;
typedef double db;
typedef pair<int,int> Pa;
void Rd(int &x){
	x=0;char c;
	while(c=getchar(),c<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),c>47);
}
int n;
int A[M];
int Lt[M],Rt[M];
int Stk[M],top;
void Init(){
	FOR(i,1,n){
		while(top&&A[Stk[top]]>A[i])top--;
		if(top)Lt[i]=Stk[top];
		else Lt[i]=0;
		Stk[++top]=i;
	}
	top=0;
	REP(i,n,1){
		while(top&&A[Stk[top]]>=A[i])top--;
		if(top)Rt[i]=Stk[top];
		else Rt[i]=n+1;
		Stk[++top]=i;
	}
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	Rd(n);
	FOR(i,1,n)Rd(A[i]);
	Init();
	ll ans=0;
	FOR(i,1,n){
		int mx=max(A[Lt[i]],A[Rt[i]]);
		ans+=A[i]-mx;
	}
	printf("%lld\n",ans);
	return 0;
}
