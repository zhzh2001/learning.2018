#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for(int i=(a),i_##END_=(b);i<=i_##END_;++i)
#define REP(i,a,b) for(int i=(a),i_##END_=(b);i>=i_##END_;--i)

#define M 105
#define N 25005
#define debug() cerr<<"Why So Serious?"<<endl
#define fi first
#define se second
typedef long long ll;
typedef unsigned long long ull;
typedef double db;
typedef pair<int,int> Pa;
void Rd(int &x){
	x=0;char c;
	while(c=getchar(),c<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),c>47);
}
int A[M];
int n;
namespace P1{
	bool Can[N];
	int mx;
	void Solve(){
		Can[0]=1;
		sort(A+1,A+n+1);
		mx=A[n];
		FOR(i,1,mx)Can[i]=0;
		int ans=0;
		FOR(i,1,n)if(!Can[A[i]]){
			int x=A[i];
			ans++;
			FOR(j,x,mx)Can[j]|=Can[j-x];
		}
		printf("%d\n",ans);
	}
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int _;
	for(Rd(_);_;_--){
		Rd(n);
		FOR(i,1,n)Rd(A[i]);
		P1::Solve();
	}
	return 0;
}
