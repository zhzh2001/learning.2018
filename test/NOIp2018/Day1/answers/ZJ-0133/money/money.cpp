#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=105,M=25005;
bool dp[M];
int n,a[N],mx,ans;
inline void init(){
	n=read(); mx=0;
	for (int i=1;i<=n;i++){
		a[i]=read(); mx=max(mx,a[i]);
	}
}
inline void solve(){
	ans=0;
	memset(dp,0,sizeof dp); dp[0]=1;
	sort(a+1,a+1+n);
	for (int i=1;i<=n;i++){
		if (dp[a[i]]) continue;
		ans++; 
		for (int j=0;j+a[i]<=mx;j++){
			dp[j+a[i]]|=dp[j];
		}
	}
	writeln(ans);
}
int main(){
	freopen("money.in","r",stdin); freopen("money.out","w",stdout);
	int T=read();
	while (T--){
		init(); solve();
	}
	return 0;
}
