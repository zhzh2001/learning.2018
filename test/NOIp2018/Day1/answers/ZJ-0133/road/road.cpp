#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=1e5+5;
struct node{
	int mn,de;
}a[N<<2];
int n,num[N],ans;
inline void init(){
	n=read();
	for (int i=1;i<=n;i++) num[i]=read();
}
inline void pushup(int k){
	a[k].mn=min(a[k<<1].mn,a[k<<1|1].mn);
}
void build(int k,int l,int r){
	if (l==r){
		a[k].mn=num[l];
		return;
	}
	int mid=(l+r)>>1; build(k<<1,l,mid); build(k<<1|1,mid+1,r); pushup(k);
}
inline void De(int k,int v){
	a[k].mn-=v; a[k].de+=v;
}
inline void pushdown(int k){
	if (a[k].de!=0){
		De(k<<1,a[k].de); De(k<<1|1,a[k].de);
		a[k].de=0;
	}
}
int query(int k,int l,int r,int x,int y){
	if (l==x&&r==y){
		return a[k].mn;
	}
	int mid=(l+r)>>1; pushdown(k);
	if (mid>=y) return query(k<<1,l,mid,x,y);
		else if (mid<x) return query(k<<1|1,mid+1,r,x,y);
			else return min(query(k<<1,l,mid,x,mid),query(k<<1|1,mid+1,r,mid+1,y));
}
void update(int k,int l,int r,int x,int y,int v){
	if (l==x&&r==y){
		De(k,v);
		return;
	}
	int mid=(l+r)>>1; pushdown(k);
	if (mid>=y) update(k<<1,l,mid,x,y,v);
		else if (mid<x) update(k<<1|1,mid+1,r,x,y,v);
			else update(k<<1,l,mid,x,mid,v),update(k<<1|1,mid+1,r,mid+1,y,v);
	pushup(k);
}
int Query(int k,int l,int r,int x,int y){
	if (a[k].mn) return 0;
	if (l==r){
		return l;
	}
	int mid=(l+r)>>1,ans; pushdown(k);
	if (mid>=y){
		return Query(k<<1,l,mid,x,y);
	}else{
		if (mid<x) return Query(k<<1|1,mid+1,r,x,y);
			else{
				ans=Query(k<<1,l,mid,x,mid);
				if (!ans) return Query(k<<1|1,mid+1,r,mid+1,y); 
					else return ans;
			}
	}
}
void sol(int l,int r){
	if (l>r) return;
	int x=query(1,1,n,l,r); ans+=x;
	update(1,1,n,l,r,x); int tmp=l;
	while (tmp<=r){
		int p=Query(1,1,n,tmp,r);
		if (p) {
			sol(tmp,p-1);
		}else{
			break;
		}
		tmp=p+1;
	}
	if (tmp<=r) sol(tmp,r);
} 
inline void solve(){
	build(1,1,n); int j=1;
	for (int i=1;i<=n;i=j+1){
		j=i;
		if (!num[i]) {
			continue;
		}
		while (num[j+1]>0) j++;
		sol(i,j);
	}
	writeln(ans);
}
int main(){
	freopen("road.in","r",stdin); freopen("road.out","w",stdout);
	init(); solve();
	return 0;
}
