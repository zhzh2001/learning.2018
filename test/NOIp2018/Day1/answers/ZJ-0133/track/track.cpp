#include<cstdio>
#include<cstring> 
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=5e4+5,INF=1e9;
struct edge{
	int link,next,val;
}e[N<<1];
int pp,tot,head[N],n,m;
inline void add_edge(int u,int v,int w){
	e[++tot]=(edge){v,head[u],w}; head[u]=tot;
}
inline void insert(int u,int v,int w){
	add_edge(u,v,w); add_edge(v,u,w);
}
inline void init(){
	n=read(); m=read();
	for (int i=1;i<n;i++){
		int u=read(),v=read(),w=read();
		insert(u,v,w);
	}
}
int vis[N],lim,q[N],sum,f[N];
inline bool cmp(int x,int y){
	return x>y;
}
int dfs(int u,int fa){
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			f[v]=dfs(v,u)+e[i].val;
		}
	}
	tot=0; 
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			int tmp=f[v];
			if (tmp>=lim) sum++;
				else {
					q[++tot]=tmp;
				}
		}
	}
	sort(q+1,q+1+tot,cmp); int l=1,r=tot;
	for (int i=1;i<=tot;i++) vis[i]=0;
	while (l<r){
		while (l<r&&q[l]+q[r]<lim) r--;
		if (l==r) {
			break;
		}else{
			vis[l]=vis[r]=1;
			sum++; l++; r--;
		}
	}
	int mx=0;
	for (int i=1;i<=tot;i++) 
		if (!vis[i]) {
			mx=q[i]; break;
		}
	return mx;
}
inline bool check(int mid){
	int mx=0;lim=mid;
	for (int i=1;i<=min(n,pp);i++){
		sum=0; dfs(i,0); mx=max(mx,sum);
	}
	return mx>=m;
}
inline void solve(){
	int l=1,r=INF/m; pp=max(2,20000000/n/29/17);
	while (l<r){
		int mid=(l+r+1)>>1;
		if (check(mid)) l=mid;
			else r=mid-1;
	}
	writeln(l);
}
int main(){
	freopen("track.in","r",stdin); freopen("track.out","w",stdout);
	init(); solve();
	return 0;
}
