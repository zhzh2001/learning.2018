#include<bits/stdc++.h>
using namespace std;
#define N 100010
#define ll long long
ll n,k,l,r,sum,ans,m,u,v,w,head[N],to[N<<1],nxt[N<<1],va[N<<1],cnt;
ll dp[N],maxl[N],t[N],f[N];
void add(ll u,ll v,ll w)
{
	nxt[++cnt]=head[u];
	head[u]=cnt;
	to[cnt]=v;
	va[cnt]=w;
}
void dfs(ll x,ll fa)
{
	for (ll i=head[x];i;i=nxt[i])
		if (to[i]!=fa)
		{
			dfs(to[i],x);
			maxl[to[i]]+=va[i];
			dp[x]+=dp[to[i]];
		}
	ll tot=0;
	for (ll i=head[x];i;i=nxt[i])
		if (to[i]!=fa)
		{
			if (maxl[to[i]]>=m) dp[x]++;
			else 
			{
				tot++;
				t[tot]=maxl[to[i]];
				f[tot]=0;
			}
		}	
	sort(t+1,t+tot+1);
	if (t[tot]<(m+1)/2) 
	{
		maxl[x]=t[tot];
		return;
	}
	ll p=lower_bound(t+1,t+tot+1,(m+1)/2)-t;
	for (ll i=p;i<=tot;i++)
	{
		ll tt=lower_bound(t+1,t+tot+1,m-t[i])-t;
		while (f[tt] && tt<p) tt++;
		if (tt>=p) continue;
		f[tt]=1;f[i]=1;
		dp[x]++;
	}
	ll c=0;
	for (ll i=1;i<=tot;i++)
		if (!f[i] && t[i]*2>=m) c++;
	dp[x]+=c/2;
	if (c % 2==1)
	{
		for (ll i=tot;i>=1;i--)
			if (!f[i] && t[i]*2>=m)
			{
				maxl[x]=t[i];
				return;
			}
	}
	else
	{
		for (ll i=p-1;i>=1;i--)
			if (!f[i])
			{
				maxl[x]=t[i];
				return;
			}
		maxl[x]=0;
		return;
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%lld%lld",&n,&k);
	for (ll i=1;i<n;i++) 
	{
		scanf("%lld%lld%lld",&u,&v,&w),sum+=w;
		add(u,v,w);
		add(v,u,w);
	}
	memset(t,0,sizeof(t));
	l=1,r=sum/k+1;
	while (l<=r)
	{
		m=(l+r)>>1;
		memset(dp,0,sizeof(dp));
		memset(maxl,0,sizeof(maxl));
		dfs(1,0);
		if (dp[1]>=k)
		{
			ans=m;
			l=m+1;
		}
		else r=m-1;
	}
	printf("%lld\n",ans);
}
