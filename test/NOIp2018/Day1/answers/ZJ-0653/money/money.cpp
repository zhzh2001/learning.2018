#include<bits/stdc++.h>
using namespace std;
int T,n,a[10000],dp[30000],ans;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		ans=n;
		memset(dp,0,sizeof(dp));
		dp[0]=1;
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		for (int i=1;i<=n;i++)
		{
			if (dp[a[i]]) ans--;
			else
			{
				for (int j=0;j<=25001-a[i];j++) dp[j+a[i]]|=dp[j];
			}
		}
		printf("%d\n",ans);
	}
}
