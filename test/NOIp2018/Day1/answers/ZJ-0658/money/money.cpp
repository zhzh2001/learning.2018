#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=100,maxa=25000;

int te,n,a[maxn+5],f[maxa+5],ans;

#define Eoln(x) ((x)==10||(x)==13||(x)==EOF)
inline char readc(){
	static char buf[100000],*l=buf,*r=buf;
	return (l==r&&(r=(l=buf)+fread(buf,1,100000,stdin),l==r))?EOF:*l++;
}
template<typename T> inline int readi(T &x){
	T tot=0;char ch=readc(),lst='+';
	while (!isdigit(ch)) {if (ch==EOF) return EOF;lst=ch;ch=readc();}
	while (isdigit(ch)) tot=(tot<<3)+(tot<<1)+(ch^48),ch=readc();
	return (lst=='-'?x=-tot:x=tot),Eoln(ch);
}
int main(){
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	for (readi(te);te;te--){
		int MAX=0;readi(n);for (int i=1;i<=n;i++) readi(a[i]),MAX=max(MAX,a[i]);
		sort(a+1,a+1+n);n=unique(a+1,a+1+n)-a-1;memset(f,0,sizeof(f));f[0]=1;
		for (int i=1;i<=n;i++)
			for (int j=a[i];j<=MAX;j++)
				if (f[j-a[i]]) if (f[j-a[i]]<2) f[j]?f[j]=2:f[j]=1; else f[j]=2;
		ans=0;for (int i=1;i<=n;i++) if (f[a[i]]<2) ans++;printf("%d\n",ans);
	}return 0;
}
