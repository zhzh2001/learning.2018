#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
const int maxn=50000;

int n,m,num,f[maxn+5],a[maxn+5],fat[maxn+5];
int E,lnk[maxn+5],son[(maxn<<1)+5],nxt[(maxn<<1)+5],w[(maxn<<1)+5];

#define Eoln(x) ((x)==10||(x)==13||(x)==EOF)
inline char readc(){
	static char buf[100000],*l=buf,*r=buf;
	return (l==r&&(r=(l=buf)+fread(buf,1,100000,stdin),l==r))?EOF:*l++;
}
template<typename T> inline int readi(T &x){
	T tot=0;char ch=readc(),lst='+';
	while (!isdigit(ch)) {if (ch==EOF) return EOF;lst=ch;ch=readc();}
	while (isdigit(ch)) tot=(tot<<3)+(tot<<1)+(ch^48),ch=readc();
	return (lst=='-'?x=-tot:x=tot),Eoln(ch);
}
#define Add(x,y,z) (son[++E]=(y),w[E]=(z),nxt[E]=lnk[x],lnk[x]=E)
inline int Find(int x,int lim,int L,int R){
	for (int mid=L+(R-L>>1);L<=R;mid=L+(R-L>>1))
		x+a[mid]>=lim?R=mid-1:L=mid+1;return L;
}
int getfa(int x) {return x==fat[x]?x:fat[x]=getfa(fat[x]);}
void Dfs(int x,int lim,int pre=0,int len=0){
	for (int j=lnk[x];j;j=nxt[j]) if (son[j]!=pre) Dfs(son[j],lim,x,w[j]);
	a[0]=0;for (int j=lnk[x];j;j=nxt[j]) if (son[j]!=pre) a[++a[0]]=f[son[j]];
	sort(a+1,a+1+a[0]);while (a[0]&&a[a[0]]>=lim) num++,a[0]--;
    for (int i=1;i<=a[0]+1;i++) fat[i]=i;
	for (int i=1;i<a[0];i++)
		if (i==getfa(i)){
			int j=getfa(Find(a[i],lim,i+1,a[0]));
			if (j<=a[0]) num++,fat[i]=getfa(i+1),fat[j]=getfa(j+1);
		}
	f[x]=0;for (int i=a[0];i;i--) if (i==getfa(i)) {f[x]=a[i];break;}f[x]+=len;
}
inline bool check(int lim) {num=0;Dfs(1,lim);return num+(f[1]>=lim)>=m;}
int main(){
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	readi(n);readi(m);int R=0;for (int i=1,x,y,z;i<n;i++) readi(x),readi(y),readi(z),Add(x,y,z),Add(y,x,z),R+=z;
	int L=0;for (int mid=L+(R-L>>1);L<=R;mid=L+(R-L>>1)) check(mid)?L=mid+1:R=mid-1;printf("%d\n",R);return 0;
}
