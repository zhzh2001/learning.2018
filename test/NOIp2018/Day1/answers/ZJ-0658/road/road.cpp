#include<cstdio>
#include<cctype>
using namespace std;
const int maxn=100000;

int n,a[maxn+5],ans;

#define Eoln(x) ((x)==10||(x)==13||(x)==EOF)
inline char readc(){
	static char buf[100000],*l=buf,*r=buf;
	return (l==r&&(r=(l=buf)+fread(buf,1,100000,stdin),l==r))?EOF:*l++;
}
template<typename T> inline int readi(T &x){
	T tot=0;char ch=readc(),lst='+';
	while (!isdigit(ch)) {if (ch==EOF) return EOF;lst=ch;ch=readc();}
	while (isdigit(ch)) tot=(tot<<3)+(tot<<1)+(ch^48),ch=readc();
	return (lst=='-'?x=-tot:x=tot),Eoln(ch);
}
int main(){
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	readi(n);for (int i=1;i<=n;i++) readi(a[i]);
	for (int i=1;i<=n;i++) if (a[i]>a[i-1]) ans+=a[i]-a[i-1];
	printf("%d\n",ans);return 0;
}
