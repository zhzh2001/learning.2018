#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=1e5+500;
int n;
int a[N];
ll ans=0;

void Bo() {
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
}

void Solve(int l,int r) {
  int nw=0;
  ll ret=0;
  for(int i=l;i<=r;i++) {
    if(nw<a[i]) ret+=a[i]-nw;
    nw=a[i];
  }
  ans+=ret;
}

int main() {
  Bo();
  scanf("%d",&n);
  for(int i=1;i<=n;i++) scanf("%d",&a[i]);
  for(int i=1;i<=n;) {
    int j;
    for(j=i;j<=n&&a[j];j++);
    Solve(i,j-1);i=j+1;
  }
  printf("%lld\n",ans);
  return 0;
}
