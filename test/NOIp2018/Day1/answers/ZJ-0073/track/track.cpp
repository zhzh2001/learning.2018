#include<bits/stdc++.h>
using namespace std;
const int N=5e4+500;
typedef long long ll;
struct edge {
  int to,nxt;
  ll w;
}E[N<<2];
int n,m,tot,cnt,flag;
ll l,r,nlen;
int head[N];
ll f[N],W[N];
multiset<ll>S[N];
typedef multiset<ll>::iterator Sit;
void Addedge(int u,int v,ll w) {
  E[++tot].to=v;E[tot].nxt=head[u];head[u]=tot;E[tot].w=w;
  E[++tot].to=u;E[tot].nxt=head[v];head[v]=tot;E[tot].w=w;
}

void Bo() {
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
}

void Dfs(int o,int fa) {
  f[o]=0;S[o].clear();
  for(int i=head[o];~i;i=E[i].nxt) {
    int to=E[i].to;
    if(to==fa) continue;
    Dfs(to,o);
    S[o].insert(f[to]+E[i].w);
  }
  if(S[o].empty()) return ;
  Sit t=S[o].lower_bound(nlen);
  for(Sit it=t;it!=S[o].end();) {
    Sit er=it;
    ++it;
    S[o].erase(er);
    cnt++;
  }
  if(S[o].empty()) return ;
  for(Sit it=S[o].begin();it!=S[o].end();) {
    Sit tar=S[o].lower_bound(nlen-(*it));
    if(tar==S[o].end()) {++it;continue;}
    if(tar==it) {++it;continue;}
    S[o].erase(tar);
    Sit er=it;
    ++it;
    S[o].erase(er);
    cnt++;
  }
  if(!S[o].empty()) {
    Sit it=S[o].end();it--;
    f[o]=*it;
  }
  return ;
} 

int Check(ll len) {
  nlen=len;cnt=0;
  Dfs(1,1);
  return cnt>=m;
}

namespace Solver {
  multiset<ll>T,TT;
  int Check(ll len) {
    TT=T;
    int cnt=0;
    Sit ta=TT.lower_bound(len);
    for(Sit it=ta;it!=TT.end();) {
      Sit er=it;
      ++it;
      TT.erase(er);
      cnt++;
    }
    for(Sit it=TT.begin();it!=TT.end();) {
      ll tar=len-(*it);
      Sit t=TT.lower_bound(tar);
      if(t==TT.end()||t==it) {++it;continue;}
      TT.erase(t);
      Sit er=it;
      ++it;
      TT.erase(er);
      cnt++;
    }
    return cnt>=m;
  }
  
  void main() {
    for(int i=1;i<n;i++) {
      T.insert(W[i]);
    }
    ll ans=-1;
    while(l<=r) {
      ll mid=(l+r)>>1;
      if(Check(mid)) l=mid+1,ans=mid;
      else r=mid-1;
    }
    printf("%lld\n",ans);
  }
}

int main() {
  Bo();
  memset(head,-1,sizeof head);
  scanf("%d%d",&n,&m);
  flag=1;
  l=r=0;
  for(int i=1,u,v;i<n;i++) {
    ll w;
    scanf("%d%d%lld",&u,&v,&w);
    flag&=(u==1);
    Addedge(u,v,w);r+=w;
    W[i]=w;
  }
  //cerr<<flag<<endl;
  
  if(flag) {
    Solver::main();
    return 0;
  }
  
  ll ans=-1;
  while(l<=r) {
    ll mid=(l+r)>>1;
    if(Check(mid)) l=mid+1,ans=mid;
    else r=mid-1;
  }
  printf("%lld\n",ans);
  return 0;
}
