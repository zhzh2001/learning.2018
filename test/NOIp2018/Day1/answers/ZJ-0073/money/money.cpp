#include<bits/stdc++.h>
using namespace std;
const int N=105;
const int Mx=1e5+500;
int n,T;
int a[N],vis[N],f[Mx];

void Bo() {
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
}

int main() {
  Bo();
  scanf("%d",&T);
  while(T--) {
    memset(f,0,sizeof f);
    memset(vis,0,sizeof vis);
    scanf("%d",&n);
    for(int i=1;i<=n;i++) {
      scanf("%d",&a[i]);
    }
    sort(a+1,a+1+n);
    f[0]=1;
    for(int i=1;i<=n;i++) {
      if(vis[i]) continue;
      for(int j=0;j<=30000;j++) {
	if(!f[j]||j+a[i]>=30000) continue;
	f[j+a[i]]=1;
      }
      for(int j=i+1;j<=n;j++) {
	if(f[a[j]]) vis[j]=1;
      }
    }
    int cnt=0;
    for(int i=1;i<=n;i++) {
      if(!vis[i]) cnt++;
    }
    printf("%d\n",cnt);
  }
  return 0;
}
