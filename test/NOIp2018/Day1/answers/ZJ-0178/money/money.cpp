#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N = 110;
const int T = 30010;
int t, n, a[N];
int b[T], ans;

void Update(int x)
{
	for (int i = 0, ub = T-x; i < ub; ++ i)
		if (b[i])
			b[i+x] = 1;
}

int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &t);
	while (t--){
		scanf("%d", &n);
		for (int i = 1; i <= n; ++ i)
			scanf("%d", a+i);
		sort(a+1, a+n+1);
		memset(b, 0, sizeof(b)); b[0] = 1;
		ans = 0;
		for (int i = 1; i <= n; ++ i)
			if (!b[a[i]]){
				++ ans;
				Update(a[i]);
			}
		printf("%d\n", ans);
	}
	return 0;
}
