#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int inf = 1e9+7;
const int N = 5e4+10;
int n, m, point[N], dgr[N];
struct EDGE{
	int nxt, v, w;
}edge[N<<1];
int len;

void add_edge(int u, int v, int w, int e)
{
	edge[e] = (EDGE){point[u], v, w};
	point[u] = e;
}

int Solve(int p[], int pn, int ban)
{
	int le = 1, ri = pn, maxcnt = 0;
	while (p[ri] >= len && ri >= le){
		if (ri == ban){
			-- ri;
			continue;
		}
		++ maxcnt;
		-- ri;
	}
	while (le < ri){
		if (le == ban){
			++ le;
		}
		else if (ri == ban){
			-- ri;
		}
		else if (p[le]+p[ri] >= len){
			++ maxcnt;
			++ le;
			-- ri;
		}
		else{
			++ le;
		}
	}
	return maxcnt;
}

pair<int, int> Dfs(int u, int fa)
{
	int ret = 0;
	int p[dgr[u]+1], pn = 0;
	p[0] = 0;
	for (int i = point[u]; i != -1; i = edge[i].nxt){
		int v = edge[i].v;
		int w = edge[i].w;
		if (v == fa) continue;
		pair<int, int> son = Dfs(v, u);
		ret += son.first;
		p[++ pn] = son.second+w;
	}
	if (pn > 0) sort(p+1, p+pn+1);
	int maxcnt = Solve(p, pn, 0);
	int l = 0, r = pn, mid, ret2 = 0;
	while (l <= r){
		mid = (l+r)>>1;
		int tmp = Solve(p, pn, mid);
		if (tmp == maxcnt)
			ret2 = mid, l = mid+1;
		else
			r = mid-1;
	}
	return make_pair(ret+maxcnt, p[ret2]);
}

void DoAll()
{
	int l = 0, r = inf, ans;
	while (l <= r){
		len = (l+r)>>1;
		pair<int, int> tmp = Dfs(1, 0);
		if (tmp.first >= m)
			ans = len, l = len+1;
		else
			r = len-1;
	}
	printf("%d\n", ans);
}

int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	memset(point, -1, sizeof(point));
	memset(dgr, 0, sizeof(dgr));
	for (int i = 0, ub = n-1; i < ub; ++ i){
		int x, y, z;
		scanf("%d%d%d", &x, &y, &z);
		++ dgr[x];
		++ dgr[y];
		add_edge(x, y, z, i<<1);
		add_edge(y, x, z, i<<1^1);
	}
	DoAll();
	return 0;
}
