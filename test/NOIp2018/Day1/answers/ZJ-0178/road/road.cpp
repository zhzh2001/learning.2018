#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const int N = 1e5+10;
int n, a[N];
int del, ans;

int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; ++ i)
		scanf("%d", a+i);
	del = ans = 0;
	for (int i = 1; i <= n; ++ i){
		if (a[i] > del)
			ans += a[i]-del;
		del = a[i];
	}
	printf("%d\n", ans);
	return 0;
}
