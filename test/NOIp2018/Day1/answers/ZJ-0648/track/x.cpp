#include<bits/stdc++.h>
using namespace std;

int n, aim, K, hea[101000], cnr, las;
struct R{
  int to, val, nex;
}r[101000];
int a[101000], tot, st[101000], tp, f[101000];

void dfs(int x,int fff){
  if (las<=0) return;
  for (int i=hea[x];i;i=r[i].nex){
    int y=r[i].to; if (y==fff) continue;
    dfs(y,x);
  }
  if (las<=0) return;
  tot=tp=0;
  for (int i=hea[x];i;i=r[i].nex){
    int y=r[i].to; if (y==fff) continue;
    int t=f[y]+r[i].val;
    if (t>=K) --las;
    else a[++tot]=t;
  }
  sort(a+1,a+tot+1);
  int p=tot; f[x]=0;
  for (int i=1;i<=p;++i){
    for (;p>i&&a[p]+a[i]>=K;--p) st[++tp]=a[p];
    if (tp) --las, --tp;
    else f[x]=max(f[x],a[i]);
  }
  if (tp>0){
    if (tp&1) f[x]=max(f[x],st[1]);
    las-=tp/2;
  }
}

bool chk(int mid){
  K=mid; las=aim;
  dfs(1,0);
  return las<=0;
}

int main(){
  cin>>n>>aim;
  for (int i=1,u,v,w;i<n;++i){
    scanf("%d%d%d",&u,&v,&w);
    r[++cnr]=(R){v,w,hea[u]}; hea[u]=cnr;
    r[++cnr]=(R){u,w,hea[v]}; hea[v]=cnr;
  }
  int l=1, r=5e8, ans=0;
  for (;l<=r;){
    int mid=l+r>>1;
    if (chk(mid)) ans=mid, l=mid+1;
    else r=mid-1;
  }
  cout<<ans<<endl;
}
