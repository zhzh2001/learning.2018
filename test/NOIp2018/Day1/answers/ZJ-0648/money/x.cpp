#include<bits/stdc++.h>
using namespace std;

int T, n, m=25000, a[111];
bool b[25500];

int main(){
  for (cin>>T;T--;){
    cin>>n;
    for (int i=1;i<=n;++i) cin>>a[i];
    sort(a+1,a+n+1);
    int ans=0; memset(b,0,sizeof b); b[0]=1;
    for (int i=1;i<=n;++i){
      if (b[a[i]]) continue;
      for (register int j=a[i];j<=m;++j)
	b[j]|=b[j-a[i]];
      ++ans;
    }
    cout<<ans<<endl;
  }
}
