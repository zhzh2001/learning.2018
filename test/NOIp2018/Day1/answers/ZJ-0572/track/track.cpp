#include<bits/stdc++.h>
#define MAXN 50010
using namespace std;

struct edge{
	int to, Next, val;
}Right[MAXN<<1];
int Begin[MAXN], dist[MAXN], z[MAXN], n, m, Maxpath, tot;

void add_edge(int x, int y, int z){
	Right[++tot].to=y;
	Right[tot].Next=Begin[x];
	Begin[x]=tot;
	Right[tot].val=z;
}

namespace P20{
	void dfs(int u, int fa){
		for (int i=Begin[u]; i; i=Right[i].Next){
			int v=Right[i].to;
			if (v==fa) continue;
			dist[v]=max(dist[v], dist[u]+Right[i].val);
			dfs(v, u);
		}
	}
	void main(){
		memset(dist, 0, sizeof dist);
		dfs(1, 0);
		int Max=0, maxnum=0;
		for (int i=1; i<=n; i++)
			if (dist[i]>Max) {
				Max=dist[i];
				maxnum=i;
			}
		memset(dist, 0, sizeof dist);
		dfs(maxnum, 0);
		for (int i=1; i<=n; i++)
			if (dist[i]>Max) Max=dist[i];
		printf("%d\n", Max);
	}
}
namespace P15{
	bool check(int mid){
		int sum=0, cnt=0;
		for (int i=1; i<n; i++){
			sum+=z[i];
			if (sum>=mid) ++cnt, sum=0;
		}
		return cnt>=m;
	}
	void main(){
		int l=0, r=Maxpath<<1;
		while (l<r){
			int mid=((long long)l+r+1)>>1;
			if (check(mid)) l=mid;
				else r=mid-1;
		}
		printf("%d\n", l);
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d", &n, &m);
	bool flag=1;
	for (int i=1; i<n; i++) {
		int x, y;
		scanf("%d%d%d", &x, &y, &z[i]);
		if (y!=x+1) flag=0; 
		add_edge(x, y, z[i]);
		add_edge(y, x, z[i]);
		Maxpath+=z[i];
	}
	if (m==1) P20::main();
		else if (flag) P15::main();
	return 0;
}

/*
7 3
1 2 3
2 3 4
3 4 3
4 5 1
5 6 5
6 7 4

*/
