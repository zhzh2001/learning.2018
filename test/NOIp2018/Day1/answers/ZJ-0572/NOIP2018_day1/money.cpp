#include<bits/stdc++.h>
#define MAXN 105
using namespace std;

int a[MAXN], num[MAXN], cnt, Sum, Ans, P[MAXN], n;
bool Flag[5010], FLAG[5010], flag[MAXN];

void check(int s){
	for (int i=0; i<=Sum; i++) FLAG[i]=0;
	FLAG[0]=1;
	for (int i=1; i<=s; i++) 
		for (int j=0; j<=Sum; j++)
			if (j-P[i]>=0 && !FLAG[j]) FLAG[j]=FLAG[j-P[i]];
	bool vis=1;
	for (int i=0; i<=Sum; i++) 
		if (FLAG[i]!=Flag[i]) {
			vis=0;
			break;
		}
	if (vis) Ans=min(Ans, s);
}

void dfs(int k, int s){
	if (s>Ans) return;
	if (k>cnt) {
		check(s);
		return;
	}
	dfs(k+1, s);
	P[s+1]=num[k];
	dfs(k+1, s+1);
	P[s+1]=0;
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d", &T);
	while (T--){
		memset(flag, 0, sizeof flag);
		scanf("%d", &n);
		for (int i=1; i<=n; i++) scanf("%d", &a[i]);
		sort(a+1, a+1+n);
		for (int i=1; i<=n; i++) 
			for (int j=i+1; j<=n; j++) 
				if (a[j]%a[i]==0) flag[j]=1;
		cnt=0, Sum=0;
		for (int i=1; i<=n; i++)
			if (!flag[i]) num[++cnt]=a[i], Sum+=a[i];
		memset(Flag, 0, sizeof Flag);
		Flag[0]=1;
		for (int i=1; i<=cnt; i++)
			for (int j=0; j<=Sum; j++) 
				if (j-num[i]>=0 && !Flag[j]) {
					Flag[j]=Flag[j-num[i]];
				}
		Ans=1000000000;
		dfs(1, 0);
		printf("%d\n", Ans);
	}
	return 0;
}

/*
1 
4
3 19 10 6

*/
