#include<bits/stdc++.h>
#define MAXN 5010
#define Inf 1000000000
using namespace std;

int tree[100010], tag[100010], a[100010], cnt, work[MAXN][MAXN];
vector <int> num0[100010];
int n, ans;
void pushup(int rt){
	tree[rt]=min(tree[rt<<1], tree[rt<<1|1]);
}
void pushdown(int rt){
	if (tag[rt]!=0){
		tree[rt<<1]+=tag[rt];
		tree[rt<<1|1]+=tag[rt];
		tag[rt<<1]+=tag[rt];
		tag[rt<<1|1]+=tag[rt];
		tag[rt]=0;
	}
}
void build(int rt, int l, int r){
	if (l==r){
		tree[rt]=a[l];
		return;
	}
	int mid=(l+r)>>1;
	build(rt<<1, l, mid);
	build(rt<<1|1, mid+1, r);
	pushup(rt);
}
void update(int rt, int l, int r, int x, int y, int sum){
	if (y<l || x>r) return;
	if (l>=x && r<=y) {
		tree[rt]-=sum;
		tag[rt]-=sum;
		return;
	}
	pushdown(rt);
	int mid=(l+r)>>1;
	update(rt<<1, l, mid, x, y, sum);
	update(rt<<1|1, mid+1, r, x, y, sum);
	pushup(rt);
}

int query(int rt, int l, int r, int x, int y){
	if (y<l || x>r) return Inf;
	if (l>=x && r<=y) return tree[rt];
	pushdown(rt);
	int mid=(l+r)>>1;
	return min(query(rt<<1, l, mid, x, y), query(rt<<1|1, mid+1, r, x, y));
}

void dfs(int l, int r){
	int now=query(1, 1, n, l, r);
	update(1, 1, n, l, r, now);
	if (now!=Inf){
		ans+=now;
		num0[++cnt].push_back(l-1);
		work[l][r]=cnt;
		for (int i=l; i<=r; i++)
			if (query(1, 1, n, i, i)==0) num0[work[l][r]].push_back(i);
		num0[cnt].push_back(r+1);
		if (num0[work[l][r]].size()>1) for (int i=1; i<=num0[cnt].size(); i++) dfs(num0[work[l][r]][i-1]+1, num0[work[l][r]][i]-1);
	}
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d", &n);
	for (int i=1; i<=n; i++) scanf("%d", &a[i]);
	build(1, 1, n);
	dfs(1, n);
	printf("%d\n", ans);
	return 0;
}
