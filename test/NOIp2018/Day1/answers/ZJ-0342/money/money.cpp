#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
#include <map> 
#include <stack>
#include <queue>
#define rep(i,l,r) for(int i=(l);i<=(r);++i)
#define per(i,r,l) for(int i=(r);i>=(l);--i)
using namespace std;
const int maxn=110,maxv=25010;
int T,n,a[maxn],r,cnt;
bool f[maxv];
int main(){
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		memset(f,false,sizeof(f));f[0]=true;
		rep(i,1,n) scanf("%d",&a[i]);
		sort(a+1,a+n+1);cnt=0;
		int r=a[n];
		rep(i,1,n){
			if(f[a[i]]) continue;
			++cnt;
			rep(j,a[i],r) f[j]|=f[j-a[i]];
		}
		printf("%d\n",cnt);
	}
	return 0;
}
