#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <queue>
#define rep(i,l,r) for(int i=(l);i<=(r);++i)
#define per(i,r,l) for(int i=(r);i>=(l);--i)
using namespace std;
const int maxn=100010;
int n,a[maxn],ans,tp,s[maxn],ch[maxn][2],rt;
void ins(int i){
	while(tp&&a[s[tp]]>a[i]) --tp;
	int x=s[tp];
	ch[i][0]=ch[x][1];ch[x][1]=i;
	if(tp==0) rt=i;
	s[++tp]=i;
}
void dfs(int cur,int d){
	if(!cur) return;
	ans+=a[cur]-d;
	dfs(ch[cur][0],a[cur]);dfs(ch[cur][1],a[cur]);
}
int main(){
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	scanf("%d",&n);
	rep(i,1,n) scanf("%d",&a[i]);
	rep(i,1,n) ins(i);
	dfs(rt,0);
	printf("%d\n",ans);
	return 0;
}
