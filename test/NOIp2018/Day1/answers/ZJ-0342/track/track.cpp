#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <queue>
#define rep(i,l,r) for(int i=(l);i<=(r);++i)
#define per(i,r,l) for(int i=(r);i>=(l);--i)
#define For(i,u) for(int i=hed[u],v=e[i].to;i;i=e[i].nex,v=e[i].to)
using namespace std;
const int maxn=50010;
int n,m,hed[maxn],ed,u[maxn],v[maxn],w[maxn],D[maxn];
struct edge{
	int to,nex,val;
	edge(int t=0,int n=0,int v=0):to(t),nex(n),val(v){}
}e[maxn<<1];
inline void add_edge(int u,int v,int w){e[++ed]=edge(v,hed[u],w);hed[u]=ed;++D[u];}
namespace solve0{
	int far,len,d[maxn];
	void find(int u,int f){
		For(i,u) if(v!=f) d[v]=d[u]+e[i].val,find(v,u);
		if(d[u]>len) far=u,len=d[u];
	}
	void solve(){
		d[1]=len=0;find(1,0);
		d[far]=len=0;find(far,0);
		printf("%d\n",len);
	}
}
namespace solve1{
	int d[maxn];
	bool check(int x){
		int l=1,r=n,cnt=0;
		while(r&&d[r]>=x) --r,++cnt;
		while(l<r){
			while(l<r&&d[l]+d[r]<x) ++l;
			if(l<r) ++cnt;
			--r;++l;
		}
		return cnt>=m;
	}
	void solve(int l,int r){
		int ans=0;
		rep(i,1,n-1){
			if(u[i]==1) d[v[i]-1]=w[i];
			if(v[i]==1) d[u[i]-1]=w[i];
		}
		sort(d+1,d+n+1);
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid)) l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}
namespace solve2{
	int d[maxn];
	bool check(int x){
		int sum=0,cnt=0;
		rep(i,1,n-1){
			sum+=d[i];
			if(sum>=x) sum=0,++cnt;
		}
		return cnt>=m;
	}
	void solve(int l,int r){
		rep(i,1,n-1){
			if(u[i]<v[i]) d[u[i]]=w[i];
			else d[v[i]]=w[i];
		}
		int ans=0;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid)) l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}
namespace solve3{
	int len,f[maxn],dp[maxn],cnt[2],rt;
	struct node{
		int d,v;
		node(int a=0,int b=0):d(a),v(b){}
	}a[2][maxn];
	bool operator <(node a,node b){
		if(a.d==b.d) return a.v>b.v;
		return a.d<b.d;
	}
	void find(int u,int id,int rt,int d,int fa){
		a[id][++cnt[id]]=node(d,f[rt]-f[u]+dp[u]);
		For(i,u) if(v!=fa) find(v,id,rt,d+e[i].val,u);
	}
	void dfs(int u,int fa){
		for(int i=hed[u],v=e[i].to;i;i=e[i].nex,v=e[i].to) 
			if(v!=fa){
				dfs(v,u);
				f[u]+=dp[v];
			}	
		dp[u]=f[u];int tot=-1;
		For(i,u) 
			if(v!=fa) 
				cnt[++tot]=0,find(v,tot,v,e[i].val,u);
		rep(i,0,tot) sort(a[i]+1,a[i]+cnt[i]+1);
		int x=0;
		rep(i,0,tot){
			int mx=0;
			per(j,cnt[i],1){
				if(a[i][j].d<len) break;
				mx=max(mx,a[i][j].v);
			}
			x+=mx;
		}
		if(x>dp[u]) dp[u]=x;
		if(tot==1){
			int l=1,r=cnt[1];
			while(l<=cnt[0]&&r){
				while(a[0][l].d+a[1][r].d>=len) dp[u]=max(dp[u],a[0][l].v+a[1][r].v+1),--r;
				++l;
			}
		}
	}
	bool check(int x){
		len=x;
		memset(f,0,sizeof(f));memset(dp,0,sizeof(dp));
		dfs(rt,0);
		return dp[rt]>=m;
	}
	void solve(int l,int r){
		rep(i,1,n) if(D[i]<3){rt=i;break;}
		int ans=0;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid)) l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}
int main(){
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int l=0,r=0;
	rep(i,1,n-1) scanf("%d%d%d",&u[i],&v[i],&w[i]),add_edge(u[i],v[i],w[i]),add_edge(v[i],u[i],w[i]),r+=w[i];
	if(m==1){
		solve0::solve();
		return 0;
	}
	if(D[1]==n-1){
		solve1::solve(l,r);
		return 0;
	}
	int flag=true;
	rep(i,1,n) if(abs(u[i]-v[i])>1){
		flag=false;
		break;
	}
	if(flag){
		solve2::solve(l,r);
		return 0;
	}
	flag=true;
	rep(i,1,n) if(D[i]>3){
		flag=false;
		break;
	}
	if(flag){
		solve3::solve(l,r);
		return 0;
	}
	return 0;
}
