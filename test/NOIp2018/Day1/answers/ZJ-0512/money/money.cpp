#include<cstdio>
#include<algorithm>
#define maxn 25007
template<typename T> void in(T &x){
	char ch=getchar();bool flag=0;x=0;
	while(ch<'0'||ch>'9') flag|=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if(flag) x=-x;
	return ;
}
using namespace std;
int n,cur;
int vis[maxn];
int a[205];

void input(){
	in(n);
	for(int i=1;i<=n;i++)
		in(a[i]);
	sort(a+1,a+1+n);
	return ;
}

void work(){
	int ans=n;
	vis[0]=cur;
	for(int i=1;i<=n;i++){
		if(vis[a[i]]==cur) ans--;
		else {
			for(int j=a[i];j<=a[n];j++)
				if(vis[j-a[i]]==cur) vis[j]=cur;
		}
	}
	printf("%d\n",ans);
	return ;
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	in(T);
	while(T--){
		cur++;
		input();
		work();
	}		
	return 0;
}







