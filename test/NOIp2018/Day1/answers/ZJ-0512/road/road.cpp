#include<cstdio>
template<typename T> void in(T &x){
	char ch=getchar();bool flag=0;x=0;
	while(ch<'0'||ch>'9') flag|=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if(flag) x=-x;
	return ;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n;
	long long ans=0,last=0,now;
	in(n);
	for(int i=1;i<=n;i++){
		in(now);
		if(now>last) ans+=(now-last);
		last=now;
	}
	printf("%lld",ans);
	return 0;
}
