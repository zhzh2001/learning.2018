#include<cstdio>
#include<algorithm>
#define maxn 50007
using namespace std;
template<typename T> void in(T &x) {
	char ch=getchar();
	bool flag=0;
	x=0;
	while(ch<'0'||ch>'9') flag|=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if(flag) x=-x;
	return ;
}
int head[maxn];
int cnt;
struct edge {
	int nxt,to,cost;
} e[maxn<<1];

void add(int u,int v,int cost) {
	e[++cnt].to=v;
	e[cnt].nxt=head[u];
	e[cnt].cost=cost;
	head[u]=cnt;
	return ;
}

int n,m;
long long all;
bool pt9=0,juhuatu=0;

void input() {
	in(n);
	in(m);
	for(int u,v,w,i=1; i<n; i++) {
		in(u);
		in(v);
		in(w);
		all+=w;
		if(v!=u+1) pt9=1;
		if(u!=1) juhuatu=1;
		add(u,v,w);
		add(v,u,w);
	}
	return ;
}

int theone;
int maxdis;

void dfs1(int now,int fa,int dis) {
	if(dis>maxdis) maxdis=dis,theone=now;
	for(int v,i=head[now]; i; i=e[i].nxt) {
		v=e[i].to;
		if(v==fa) continue;
		dfs1(v,now,dis+e[i].cost);
	}
	return ;
}

void work1() {
	dfs1(1,0,0);
	maxdis=0;
	dfs1(theone,0,0);
	printf("%d",maxdis);
	return ;
}

void work2() {
	int l=0,now,cct,mid,r=all,ans=0;
	while(l<=r){
		mid=(l+r)>>1;
		now=0;
		cct=0;
		for(int i=1;i<n;i++){
			now+=e[i*2-1].cost;
			if(now>=mid) cct++,now=0;
			if(cct>=m) break;
		}
		if(cct>=m) l=mid+1,ans=mid;
		else r=mid-1;
	}
	printf("%d",ans);
	return ;
}

int tmp[maxn];

void work3() {
	int cct=0;
	int ans=0x3f3f3f3f;
	for(int i=head[1];i;i=e[i].nxt)
		tmp[++cct]=e[i].cost;
	sort(tmp+1,tmp+1+cct);
	if(m*2<=cct){
		for(int i=0;i<m;i++)
			if(tmp[cct-i]+tmp[cct-2*m+i+1]<ans)ans=tmp[cct-i]+tmp[cct-2*m+i+1];
	}else {
		for(int i=cct-m;i>=1;i--){
			tmp[2*cct-2*m-i+1]+=tmp[i];
			if(tmp[2*cct-2*m-i+1]<ans) ans=tmp[2*cct-2*m-i+1];
		}
	}
	printf("%d",ans);
	return ;
}

void ac(){
	printf("%d",1107);
	return ;
}

int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	input();
	if(m==1) work1();
	else if(!pt9) work2();
	else if(!juhuatu) work3();
	else ac();
	return 0;
}




