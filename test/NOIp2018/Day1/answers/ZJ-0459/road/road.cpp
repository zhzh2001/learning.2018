#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
typedef long long LL;

const int MAXN = 100000 + 10;

int n;
LL ans;
LL a[MAXN];

int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i ++) scanf("%lld", &a[i]);
	LL pre = 0;
	for (int i = 1; i <= n; i ++)
	{
		if (a[i] <= pre)
		{
			ans += 0;
		}
		else if (a[i] > pre) 
		{
			ans += a[i] - pre;
		}
		pre = a[i];
	}
	printf("%lld", ans);
	return 0;
}
/*
19

0 5 5 5 3 3 7 7 6 9 8 11 6 6 0 0 2 2 0
*/
