#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
typedef long long LL;

const int MAXN = 100 + 10, MAXM = 25000;

int T, n, ans;
int a[MAXN];
bool ori[MAXM];
//int valid[MAXN][MAXM];
LL f[MAXM];

void init()
{
	n = ans = 0;
	memset(ori, false, sizeof ori);
//	memset(valid, 0, sizeof valid);
	memset(f, 0, sizeof f);
}

int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	while (T --)
	{
		init();
		scanf("%d", &n);
		for (int i = 1; i <= n; i ++) 
		{
			scanf("%d", &a[i]);
			ori[a[i]] = true;
		}
//		valid[0][0] = 1;
		f[0] = 1;
		for (int i = 1; i <= n; i ++)
		{
			for (int j = 0; j <= 25000; j ++)
			{
//				valid[i][j] += valid[i - 1][j];
				if (j - a[i] >= 0)
//					valid[i][j + a[i]] += valid[i - 1][j];
					f[j] += f[j - a[i]];
			}
		}
		for (int i = 0; i <= 25000; i ++)
		{
//			printf("valid[%d][%d]=%d\n", n, i, valid[n][i]);
//			printf("f[%d]=%d\n", i, f[i]);
			if (f[i] == 1 && ori[i])
//			if (valid[n][i] == 1 && ori[i])
				ans ++/*, printf("chose %d\n", i)*/;
		}
		printf("%d\n", ans);	
	}
	
	return 0;
}
/*
1
30
32 9 37 39 54 36 30 14 62 47 49 55 61 20 42 59 29 38 58 33 25 18 45 63 44 64 50 43 60 65
*/
