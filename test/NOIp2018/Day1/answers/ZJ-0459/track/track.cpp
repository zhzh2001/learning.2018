#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
typedef long long LL;
const int MAXN = 50000 + 10;

int n, m;
LL ans;

int head[MAXN], nume;
struct Adj {int nex, to; LL w;} adj[MAXN << 1];
void addedge(int from, int to, LL w)
{
	adj[++nume] = (Adj) {head[from], to, w};
	head[from] = nume;
}

LL D;
LL f[MAXN]; // maxdis in subtree
void DFS(int u, int fa)
{
	LL fi = 0, se = 0;
	for (int i = head[u]; i; i = adj[i].nex)
	{
		int v = adj[i].to;
		if (v == fa) continue;
//		printf("%d --> %d\n", u, v);
		DFS(v, u);
		if (f[v] + adj[i].w > se) se = f[v] + adj[i].w;
		if (se > fi) swap(se, fi);
	}
	f[u] = fi;
//	printf("%d %lld %lld\n", u, fi, se);
	D = max(D, fi + se);
}
void solve1()
{
	DFS(1, 0);
	printf("%lld\n", D);
}

int dgr[MAXN];
bool islian()
{
	int cnt1 = 0, cnt2 = 0;
	for (int i = 1; i <= n; i ++)
	{
		if (dgr[i] == 1) cnt1 ++;
		else if (dgr[i] == 2) cnt2 ++;
	}
	return cnt1 == 2 && cnt2 == n - 2;
}
LL edge[MAXN];
bool check1(LL x)
{
	LL sum = 0, cnt = 0;
	for (int i = 1; i <= n; i ++)
	{
		if (sum < x) 
		{
			sum += edge[i];
			if (sum >= x) cnt ++, sum = 0;
		}
	}
	return cnt >= m;
}
void dolian()
{
	int st = 0;
	for (int i = 1; i <= n; i ++) if (dgr[i] == 1) {st = i; break;}
	int pre = 0, now = st;
	for (int i = 1; i <= n - 1; i ++)
	{
		for (int j = head[now]; j; j = adj[j].nex)
		{
			int v = adj[j].to;
			if (v == pre) continue;
			else 
			{
				edge[i] = adj[j].w, pre = now, now = v;
				break;
			}
		}
	}
//	for (int i = 1; i <= n - 1; i ++) printf("%lld ", edge[i]);
	LL l = 0, r = (LL)5e8, mid = 0;
	while (l <= r)
	{
		mid = (l + r) >> 1;
		if (check1(mid)) ans = mid, l = mid + 1;
		else r = mid - 1;
	}
	printf("%lld\n", ans);
}
bool isjuhua()
{
	for (int i = 2; i <= n; i ++) 
		if (dgr[i] != 1) return false;
	return dgr[1] == n - 1;
}
LL rec[MAXN];
void dojuhua()
{
	for (int i = 1; i <= nume; i += 2)
		edge[i / 2 + 1] = adj[i].w;
	sort(edge + 1, edge + n - 1 + 1);
//	for (int i = 1; i <= n - 1; i ++) printf("%lld ", edge[i]);
	int cnt = 0;
	for (int i = n - 1; i >= n - 1 - m + 1; i --) rec[++cnt] = edge[i];
//	for (int i = 1; i <= m; i ++) printf("%lld ", rec[i]);
	for (int i = n - 1 - m; i >= max(1, n - 1 - 2 * m + 1); i --) 
		rec[cnt] += edge[i], cnt --;
//	for (int i = 1; i <= m; i ++) printf("%lld ", rec[i]);
	ans = 1e18;
	for (int i = 1; i <= m; i ++) ans = min(ans, rec[i]);
	printf("%lld\n", ans);
}

int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n - 1; i ++)
	{
		int u, v; scanf("%d%d", &u, &v);
		LL w; scanf("%lld", &w);
		dgr[u] ++, dgr[v] ++;
		addedge(u, v, w);
		addedge(v, u, w);
	}
	if (islian())
	{
		dolian();
		return 0;
	}
	if (isjuhua())
	{
		dojuhua();
		return 0;
	}
	if (m == 1)
	{
		solve1();
		return 0;
	}
	return 0;
}
/*
13 1
1 2 2 
1 3 4
2 4 9
2 5 11
2 6 9
3 7 3
6 8 4
6 9 9
7 10 1
7 11 2
8 12 2
8 13 12

11 3
1 2 2
2 3 3
3 4 3
4 5 6
5 6 7
6 7 5
7 8 2
8 9 3
9 10 4
10 11 9

9 5
1 2 2
1 9 7
1 8 1
1 7 5
1 6 6
1 5 2
1 4 4
1 3 3
*/
