#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
typedef long long ll;
const ll N=4e5+5;
ll a[N];
ll re(){
	char c=getchar();ll aint=0,pd=1;
	for(;c>'9'||c<'0';c=getchar()) if(c=='-') pd=-1;
	while(c>='0'&&c<='9') aint=aint*10+c-'0',c=getchar();return aint*pd;
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	ll n=re();
	for(ll i=1;i<=n;i++) a[i]=re();
	ll last = 1;ll all = 0;
	ll ans = 0;
	while(last <= n){
		ll d = n;
		for(ll i=last;i<=n;i++)
			if(a[i] < a[i+1]) {d = i;break;}
		ans += a[last] - all;
		last = d+1;
		all = a[d];
	}printf("%lld\n",ans);
	return 0;
}
