#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const int N=25005;
int n,maxn;
int f[N],b[N],a[N];
int re(){
	char c=getchar();int all=0,pd=1;
	for(;c>'9'||c<'0';c=getchar()) if(c=='-') pd=-1;
	while(c>='0'&&c<='9') all=all*10+c-'0',c=getchar();return all*pd;
}

int dfs(){
	int flag = 0;
	memset(f,-10,sizeof(f));
	f[0] = 0;
	for(int i=1;i<=n;i++){
		if(b[i]) continue;
		for(int j=a[i];j<=maxn;j++)
			f[j] = max(f[j],f[j-a[i]] + 1);
	}
	for(int i=1;i<=n;i++)
		if(f[a[i]] >= 2 && !b[i]) b[i] = 1,flag = 1;
	return flag;
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T = re();
	while(T--){
		memset(b,0,sizeof(b));
		n =re();
		for(int i=1;i<=n;i++) a[i]=re(),maxn = max(maxn,a[i]);
		while(dfs());
		int ans = 0;
		for(int i=1;i<=n;i++) ans += b[i];
		printf("%d\n",n-ans);
	}
	return 0;
}
