#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
#include<algorithm>
using namespace std;
priority_queue<int,vector<int>,less<int> > f;
const int N=2e5+5;
struct node{
	int to,next,dis;
}a[N];
int l,n,m;
int mid;
int head[N],dis[N],b[N],que[N],fa[N];

void add(int x,int y,int z){
	a[++l].to=y;a[l].next=head[x];a[l].dis=z;head[x]=l;
}

int re(){
	char c=getchar();int all=0,pd=1;
	for(;c>'9'||c<'0';c=getchar()) if(c=='-') pd=-1;
	while(c>='0'&&c<='9') all=all*10+c-'0',c=getchar();return all*pd;
}

void ddddfs(int x,int fa){
	for(int i=head[x];i;i=a[i].next){
		int to = a[i].to;
		if(to == fa) continue;
		if(dis[to] > 0) continue;
		dis[to] = dis[x] + a[i].dis;
		ddddfs(to,x);
	}
}
void SPFA(int S){
	memset(dis,-10,sizeof(dis));
	dis[S] = 0;
	ddddfs(S,S);
	int d = 0;
	for(int i=1;i<=n;i++) if(dis[i] > dis[d]) d = i;
	memset(dis,-10,sizeof(dis));
	dis[d] = 0;ddddfs(d,d);
	int maxn = 0;
	for(int i=1;i<=n;i++) maxn = max(dis[i],maxn);
	if(f.size() < m){
		f.push(maxn);
		return;
	}
	int x = f.top();
	if(x>f.top()){
		f.pop();
		f.push(maxn);
	}
	
}
int work(){
	for(int i=1;i<=n;i++)
		SPFA(i);
	return f.top();
}

int dfs(int x,int m,int fa,int tot){
	if(m == 0) return 1;
	int flag = 0;
	for(int i=head[x];i;i=a[i].next){
		int to = a[i].to;
		if(to == fa) continue;
		if(tot + a[i].dis >= mid){
			return dfs(x,m-1,fa,0);
		}
	}
	for(int i=head[x];i;i=a[i].next){
		int to = a[i].to;
		if(to == fa) continue;
		if(tot > mid) flag = max(flag,dfs(to,m-1,x,0));
		else flag = max(flag,dfs(to,m,x,tot + a[i].dis));
		if(flag) return flag;
	}return flag;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout); 
	n=re(),m=re();
	for(int i=1;i<n;i++){
		int x=re(),y=re(),z=re();
		add(x,y,z);add(y,x,z);
	}
	if(m==1){
		printf("%d",work());
		return 0;
	}
	int ans = 0;
	int res = 0;
	int l=1,r = 1e9;
	while(l<=r){
		mid = l+r>>1;
		if(dfs(1,m,0,0)) l = mid+1,res = mid;
		else r = mid-1;
	}
	ans = max(ans,res);
	printf("%d",ans);
	return 0;
}
