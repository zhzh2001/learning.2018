#include <cstdio>
#include <algorithm>
#include <cstring>
#include <vector>
#define M 60100
using namespace std;
vector <int> g[M],c[M];
int n,m,i,u,m1,m2,y,j,mn,t,mid,w,qwq;
bool fl,vis[M],fu;
int a[M],b[M],v[M],d[M],dep[M<<1];
void dfs(int t,int sum){
	vis[t]=1;
	for (int i=0;i<g[t].size();i++)
	    if (!vis[g[t][i]]){
	    	dep[++u]=sum+c[t][i];
	    	dfs(g[t][i],sum+c[t][i]);
		}
}
bool ok(int t){
	int p=0,tot=0;
	for (i=1;i<=n;i++){
		p+=d[i];
		if (p>=t){
			p=0;tot++;
		}
	}
	if (tot>=m) return 1;else return 0;
}
int read(){
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9')
	    ch=getchar();
	while (ch>='0'&&ch<='9')
	    x=x*10+ch-48,
	    ch=getchar();
	return x;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();fu=fl=1;
	for (i=1;i<n;i++){
		a[i]=read();b[i]=read();v[i]=read();
		mn=mn==0?v[i]:min(v[i],mn);
		if (a[i]>b[i]){
			y=a[i];a[i]=b[i];b[i]=y;
		}
		fl&=(a[i]==1);
		fu&=(b[i]==(a[i]+1));
	}
	if (fu){
		for (i=1;i<=n;i++){
			d[a[i]]=v[i];
			w+=v[i];
		}
		w/=m;t=1;
		while (t<=w){
			mid=(t+w)>>1;
			if (ok(mid)) {
				qwq=mid;
				t=mid+1;
			}
			else w=mid-1;
		}
		printf("%d\n",qwq);
	}
	else if (m==n-1) printf("%d\n",mn);
	else if (fl&&m==1){
		m1=m2=0;
	    for (i=1;i<=n;i++)
	    if (v[i]>m1){
	    	m2=m1;m1=v[i];
		}
		else if (v[i]>m2) m2=v[i];
		printf("%d\n",m1+m2);
	}
	else 
	if (m==1){
		for (i=1;i<=n;i++){
			g[a[i]].push_back(b[i]);
			c[a[i]].push_back(v[i]);
			g[b[i]].push_back(a[i]);
			c[b[i]].push_back(v[i]);
		}
		u=0;
		for (i=1;i<=n;i++){
			memset(vis,0,sizeof(vis));
			dfs(i,0);
		}
		sort(dep+1,dep+u+1);		
		printf("%d\n",dep[u]);
	}
	return 0;
}
