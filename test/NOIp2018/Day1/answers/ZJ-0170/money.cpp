#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
int i,n,p,j;
int a[30000];
bool f[30000];
int read(){
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9')
	    ch=getchar();
	while (ch>='0'&&ch<='9')
	    x=x*10+ch-48,
	    ch=getchar();
	return x;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while (T--){
		p=n=read();
		memset(f,0,sizeof(f));
		f[0]=1;
		for (i=1;i<=n;i++)a[i]=read();
		sort(a+1,a+n+1);
		for (i=1;i<=n;i++)
		    if (f[a[i]]) p--;
		    else for (j=a[i];j<=a[n];j++)f[j]|=f[j-a[i]];
		printf("%d\n",p);			
	}
}
