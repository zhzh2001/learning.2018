#include <cstdio>
#define M 300100
using namespace std;
int n,t,i,s,p;
int a[M],b[M];
int read(){
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9')
	    ch=getchar();
	while (ch>='0'&&ch<='9')
	    x=x*10+ch-48,
	    ch=getchar();
	return x;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();t=1;
	for (i=1;i<=n;i++)
	    a[i]=read();
	t=0;
	for (i=1;i<=n;i++){
		if (t==0) b[++t]=a[i];
		else{
			if (a[i]>b[t]){
				b[++t]=a[i];
			}
			else {	
				s+=b[t]-a[i];			
				while (a[i]<=b[t]&&t>=1)t--;
				b[++t]=a[i];
			}
		} 
	}
	printf("%d\n",s+b[t]);
} 
