#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(;isdigit(ch);ch=getchar())x=(x<<1)+(x<<3)+ch-48;	
	return x*f;
}const int N=110000,inf=0x3f3f3f3f;
int n,d[N],ans;
void dfs(int l,int r){
	int mn=inf,pre,nxt;
	for(int i=l;i<=r;i++)mn=min(mn,d[i]);
	for(int i=l;i<=r;i++)d[i]-=mn;
	ans+=mn;pre=l-1;nxt=l;
	while(pre<=r&&nxt<=r){
		for(int i=pre+1;i<=r;i++)if(!d[i]){
			pre=i;
		}else break;
		pre++;
		for(int i=pre;i<=r;i++)
			if(d[i])nxt=i;
			else break;
		if(nxt<pre)break;
		dfs(pre,nxt);
		pre=nxt;
	}
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		d[i]=read();
	dfs(1,n);
	cout<<ans<<'\n';
	return 0;
}
