#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(;isdigit(ch);ch=getchar())x=(x<<1)+(x<<3)+ch-48;	
	return x*f;
}const int N=200,M=30000,inf=0x3f3f3f3f;
int T,n,a[N],ans,f[M],maxn,lim;
inline void work1(){
	for(int i=1;i<=n;i++){
		if(f[a[i]]){ans--;continue;}
		for(int j=0;j<=maxn;j++)if(f[j]){
			for(int k=1;k*a[i]+j<=maxn;k++){
				f[j+k*a[i]]=1;
			}
		}
	}
}int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		n=read();ans=n;
		memset(f,0,sizeof f);f[0]=1;
		for(int i=1;i<=n;i++)a[i]=read();
		sort(a+1,a+n+1);maxn=a[n];
		work1();
		printf("%d\n",ans);
	}
	return 0;
}
