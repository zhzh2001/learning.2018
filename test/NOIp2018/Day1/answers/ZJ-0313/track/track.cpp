#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(;isdigit(ch);ch=getchar())x=(x<<1)+(x<<3)+ch-48;	
	return x*f;
}const int N=110000,inf=0x3f3f3f3f;
int n,head[N],m,x,y,z,a[N],cnt,nxt[N],to[N],w[N],ans,st,maxn;
int d[N],LMT,now,pp,used[N];multiset<int>S;
inline void add(int x,int y,int z){
	nxt[++cnt]=head[x];head[x]=cnt;to[cnt]=y;w[cnt]=z;
}
void dfs(int x,int fa,int sum){
	if(sum>maxn){
		maxn=sum;st=x;
	}
	for(int i=head[x];i;i=nxt[i])if(to[i]!=fa){
		dfs(to[i],x,sum+w[i]);
	}
}
void dfs2(int x,int fa,int sum){
	if(sum>maxn){
		maxn=sum;
	}
	for(int i=head[x];i;i=nxt[i])if(to[i]!=fa){
		dfs2(to[i],x,sum+w[i]);
	}
}
void solve1(){
	maxn=0;
	dfs(1,0,0);
	maxn=0;
	dfs2(st,0,0);
	cout<<maxn<<"\n";
}bool check(int lim){
	int num=0,pre=1,sum=0;
	for(int i=1;i<n;i++){
		sum+=a[i];
		if(sum>=lim){
			pre=i+1;
			sum=0;num++;
		}
	}return num>=m;
}
void solve3(){
	int l=0,r=0,mid;
	for(int i=1;i<n;i++)r+=a[i];
	r/=m;r+=100;
	while(l<=r){
		mid=l+r>>1;
		if(check(mid)){
			ans=mid;l=mid+1;
		}else{
			r=mid-1;
		}
	}cout<<ans<<"\n";
}bool check2(int lim){
	int num=0,pos=0;S.clear();
	for(int i=n-1;i;i--)
		if(d[i]>=lim)num++;
		else{
			pos=i;break;
		}
	for(int i=1;i<=pos;i++)S.insert(d[i]);
	multiset<int>::iterator it;
	for(int i=pos;i;i--)if(!S.empty()){
		it=S.find(d[i]);
		if(it!=S.end())S.erase(it);
		it=S.lower_bound(lim-d[i]);
		if(it!=S.end()){
			num++;S.erase(it);
		}else break;
	}else break;
	return num>=m;
}
void solve2(){
	sort(d+1,d+n);int l,r,mid;
	for(int i=1;i<n;i++)r+=d[i];
	r/=m;r+=100;
	while(l<=r){
		mid=l+r>>1;
		if(check2(mid)){
			ans=mid;l=mid+1;
		}else{
			r=mid-1;
		}
	}cout<<ans<<"\n";
}
void pfs(int x,int fa,int sum){
	if(sum>LMT){
		if(sum<now){
			now=sum;pp=x;
		}
		return;
	}
	for(int i=head[x];i;i=nxt[i])if(!used[i]&&to[i]!=fa){
		pfs(to[i],x,sum+w[i]);
	}
}bool flg=0;
void cover(int x,int ed){
	if(x==ed){
		flg=1;return;
	}if(flg)return;
	for(int i=head[x];i;i=nxt[i])if(!used[i]){
		used[i]=1;
		if(i&1)used[i+1]=1;else used[i-1]=1;
		cover(to[i],ed);
		if(flg)return;
		used[i]=0;
		if(i&1)used[i+1]=0;else used[i-1]=0;
	}
}
bool check3(int lim){
	memset(used,0,sizeof used);
	LMT=lim;now=inf;int num=0;flg=0;
	for(int i=1;i<=n;i++){
		pp=-1;
		pfs(i,0,0);
		if(pp!=-1)num++;
//		else break;
		cover(i,pp);
	}return num>=m; 
}void solve4(){
	int l,r,mid;
	for(int i=1;i<=n*2-2;i++)r+=w[i];
	r/=m*2;r+=100;
	if(n>10000){
		cout<<r-100<<'\n';
		return;
	}
	while(l<=r){
		mid=l+r>>1;
		if(check3(mid)){
			ans=mid;l=mid+1;
		}else{
			r=mid-1;
		}
	}cout<<ans<<"\n";
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();bool flag=1,fft=1;
	for(int i=1;i<n;i++){
		x=read();y=read();z=read();
		a[x]=z;if(y!=x+1)flag=0;
		if(x!=1)fft=0;d[i]=z;
		add(x,y,z);add(y,x,z);
	}if(m==1){
		solve1();return 0;
	}if(flag){
		solve3();return 0;
	}if(fft){
		solve2();return 0;
	}else solve4();
	return 0;
}
