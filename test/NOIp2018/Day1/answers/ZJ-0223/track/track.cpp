#include<bits/stdc++.h>
#define ll long long
#define N 50005
using namespace std;
struct node
{
	int w,to,next;
}edge[N<<1];
int q[N],flag[N],dis[N],n,num=0,head[N],m,k,maxx,ff,fff,hh[N];
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
void add_edge(int x,int y,int z)
{
	edge[++num].to=y;
	edge[num].w=z;
	edge[num].next=head[x];
	head[x]=num;
}
void bfs(int s)
{
	memset(q,0,sizeof(q));
	memset(dis,0,sizeof(dis));
	for (int i=1;i<=n;i++)
	if (i!=s) flag[i]=1;
	int h=0,t=1;
	q[t]=s;
	while (h<=t)
	{
		h++;
		int u=q[h];
		for (int i=head[u];i;i=edge[i].next)
		if (flag[edge[i].to])
		{
			dis[edge[i].to]=dis[u]+edge[i].w;
			flag[edge[i].to]=0;
			q[++t]=edge[i].to;
		}
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	ff=1;
	fff=1;
	for (int i=1;i<n;i++)
	{
		int x=read(),y=read(),z=read();
		hh[y]=hh[x]+z;
		add_edge(x,y,z);
		add_edge(y,x,z);
		if (x!=1) ff=0;
		if (y!=x+1) fff=0;
	}
	if (m==1||ff&&m<n-1)
	{
//		printf("@\n");
		bfs(1);
//		for (int i=1;i<=n;i++) printf("%d ",q[i]);
		maxx=0;
		k=1;
//	    for (int i=1;i<=n;i++) printf("%d ",dis[i]);
		for (int i=1;i<=n;i++)
		if (dis[i]>maxx)
		{
			k=i;
			maxx=dis[i];
		}
		bfs(k);
		maxx=0;
		for (int i=1;i<=n;i++) maxx=max(maxx,dis[i]);
//		for (int i=1;i<=n;i++) printf("%d ",q[i]);
		printf("%d\n",maxx);
		return 0;
	}
	if (m==n-1)
	{
		maxx=0;
		for (int i=1;i<=num;i++) maxx=max(maxx,edge[i].w);
		printf("%d\n",maxx);
		return 0;
	}
	if (fff)
	{
		for (int i=2;i<=n;i++) 
		maxx=max(maxx,hh[i]-hh[max(1,i-(n-m))]);
		printf("%d\n",maxx);
		return 0;
	}
	return 0;
}
