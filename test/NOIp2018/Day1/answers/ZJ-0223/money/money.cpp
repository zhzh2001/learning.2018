#include<bits/stdc++.h>
#define ll long long
#define N 5000005
using namespace std;
int T,ans,n,flag[N],flag1[N],ff[10],f[10],fff[10],a[10];
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
void dfs(int k)
{
	if (k>n)
	{
		int p=0;
		for (int i=1;i<=n;i++) p+=a[i]*f[i];
		if (p<=5000000)	flag[p]=1;
		return;
	}
	for (int i=0;i<=10;i++)
	{
		f[k]=i;
		dfs(k+1);
	}
}
void check(int k)
{
	if (k>n)
	{
		int p=0;
		for (int i=1;i<=n;i++) p+=a[i]*fff[i];
		if (p<=5000000)	flag1[p]=1;
		return;
	}
	if (ff[k]==0) fff[k]=0,dfs(k+1);
	for (int i=0;i<=10;i++) 
	{
		fff[k]=i;
		dfs(k+1);
	}
}
void dfss(int k,int x)
{
	if (k>n)
	{
		memset(flag1,0,sizeof(flag1));
		check(1);
		for (int i=1;i<=5000000;i++)
		if (flag[i]!=flag1[i]) return;
		ans=min(ans,x);
		return;
	}
	ff[k]=1;
	dfss(k+1,x+1);
	ff[k]=0;
	dfss(k+1,x);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while (T--)
	{
		memset(flag,0,sizeof(flag));
		n=read();
		ans=n;
		for (int i=1;i<=n;i++) a[i]=read();
		dfs(1);
		dfss(1,0);
		printf("%d\n",ans);
	}
	return 0;
}
