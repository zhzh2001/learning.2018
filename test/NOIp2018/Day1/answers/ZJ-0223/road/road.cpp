#include<bits/stdc++.h>
#define ll long long
using namespace std;
int ans,a[100005],minn,maxx,n;
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	minn=1008208820;
	maxx=0;
	for (int i=1;i<=n;i++)
	{
	    a[i]=read();
		minn=min(a[i],minn);
		maxx=max(maxx,a[i]);
	}
	ans=minn;
	if (minn==maxx)
	{
		printf("%d\n",minn);
		putchar('\n');
		return 0;
	}
	for (int i=1;i<=n;i++) a[i]-=ans;
	while (1)
	{
		int i=1,flag=0;
		while (i<=n)
		{
			minn=1008208820;
			int k=i;
			if (a[i]==0)
			{
			    i++;
			    continue;
			}
			while (a[i]!=0) 
			{
				minn=min(minn,a[i]);
				i++;
			}
			for (int j=k;j<i;j++) 
			{
			    a[j]-=minn;
			    if (a[j]!=0) flag=1;
			}
			ans+=minn;
	    }
	    if (!flag) break;
	}
	printf("%d\n",ans);
	return 0;
}
