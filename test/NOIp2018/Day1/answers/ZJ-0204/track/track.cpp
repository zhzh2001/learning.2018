#include<cstdio>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<set>
using namespace std;
#define G getchar()
#define rep(i,j,k) for(i=j;i<=k;++i)
#define per(i,j,k) for(i=j;i>=k;--i)
#define pii pair<int,int>
#define X first
#define Y second
#define LL long long
#define N 50005
#define inf 500000001
int n,m;
int he[N],ne[N<<1],to[N<<1],W[N<<1],tot;
int d[N],rt,q[N];
int w[N],ans;
int read(){
	int x=0;bool flg=0;char ch=G;
	while((ch<48||ch>57)&&ch!=45)ch=G;
	if(ch==45)flg=1,ch=G;
	for(;ch>47&&ch<58;ch=G)x=x*10+ch-48;
	return flg?-x:x;
}
void add(int x,int y,int z){
	to[++tot]=y;W[tot]=z;ne[tot]=he[x];he[x]=tot;
}
void BFS(){
	int Ft=0,Rr=1,x,i,y;
	memset(d,-1,sizeof d);d[q[0]=rt]=0;
	while(Ft<Rr){
		x=q[Ft++];
		for(i=he[x];i;i=ne[i])if(d[y=to[i]]==-1){
			d[q[Rr++]=y]=d[x]+W[i];
		}
	}
	rep(i,1,n)if(d[i]>d[rt])rt=i;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int i,x,y,z;bool flg1=1,flg2=1;
	n=read();m=read();tot=1;
	rep(i,2,n){
		x=read();y=read();z=read();
		add(x,y,z);add(y,x,z);w[i]=z;
		if(x!=1)flg1=0;if(x!=y-1)flg2=0;
	}
	if(m==1){
		rt=1;BFS();memset(d,0,sizeof d);BFS();printf("%d\n",d[rt]);
		return 0;
	}
	if(flg1){
		sort(w+2,w+n+1);ans=inf;
		if((m<<1)<n-1){
			per(i,n,n-m+1)ans=min(ans,w[i]+w[(n-m<<1)+1-i]);
		}
		else{
			rep(i,2,n-m+1)
				ans=min(ans,w[i]+w[(n-m<<1)+1-i]);
			rep(i,(n-m<<1)+1,n)ans=min(ans,n);
		}
		printf("%d\n",ans);return 0;
	}
	printf("%d\n",w[n-m+1]);
	return 0;
}
