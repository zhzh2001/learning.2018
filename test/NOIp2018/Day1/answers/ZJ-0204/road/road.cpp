#include<cstdio>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<cmath>
using namespace std;
#define G getchar()
#define rep(i,j,k) for(i=j;i<=k;++i)
#define per(i,j,k) for(i=j;i>=k;--i)
#define pii pair<int,int>
#define X first
#define Y second
#define LL long long
#define N 100005
LL n,d[N],ans;
LL read(){
	LL x=0;bool flg=0;char ch=G;
	while((ch<48||ch>57)&&ch!=45)ch=G;
	if(ch==45)flg=1,ch=G;
	for(;ch>47&&ch<58;ch=G)x=x*10+ch-48;
	return flg?-x:x;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int i;
	n=read();rep(i,1,n){
		d[i]=read();
		if(d[i]>d[i-1])ans+=d[i]-d[i-1];
	}
	printf("%lld\n",ans);
	return 0;
}
