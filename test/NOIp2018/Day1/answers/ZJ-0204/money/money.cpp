#include<cstdio>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include<algorithm>
using namespace std;
#define G getchar()
#define rep(i,j,k) for(i=j;i<=k;++i)
#define per(i,j,k) for(i=j;i>=k;--i)
#define pii pair<int,int>
#define X first
#define Y second
#define LL long long
#define N 105
#define M 25000
int n,aa[N],m,mx;int bo[25001];
int read(){
	int x=0;bool flg=0;char ch=G;
	while((ch<48||ch>57)&&ch!=45)ch=G;
	if(ch==45)flg=1,ch=G;
	for(;ch>47&&ch<58;ch=G)x=x*10+ch-48;
	return flg?-x:x;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int cas=read(),i,j;
	while(cas--){
		memset(bo,0,sizeof bo);
		n=read();rep(i,1,n)bo[aa[i]=read()]=1;
		mx=aa[1];rep(i,2,n)mx=max(mx,aa[i]);
		rep(i,1,mx)if(bo[i]){
			rep(j,1,n)if(aa[j]+i<=mx)bo[aa[j]+i]=2;
		}
		m=0;rep(i,1,n)if(bo[aa[i]]==1)++m;
		printf("%d\n",m);
	}
}
