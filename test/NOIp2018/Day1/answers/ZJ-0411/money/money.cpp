#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;
int n,i,j,T,l,ans,maxa,r,a[1005];
bool f[25005];
inline int read()
{
	int nod=0,p;char ch;
	ch=getchar();
	while (ch!='-'&&(ch<'0'||ch>'9')) ch=getchar();
	if (ch=='-') p=-1,ch=getchar(); else p=1;
	while (ch>='0'&&ch<='9') {nod=nod*10+ch-'0';ch=getchar();}
	return nod*p;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	for (l=1;l<=T;l++)
	{
		n=read();maxa=0;ans=0;
		for (i=1;i<=n;i++)
		{
			a[i]=read();
			if (a[i]>maxa) maxa=a[i];
		}
		sort(a+1,a+n+1);
		for (i=1;i<=maxa;i++) f[i]=true;f[0]=false;
		for (i=1;i<=n;i++)
		if (f[a[i]])
		{
			ans++;
			for (j=a[i];j<=maxa;j++) f[j]=f[j-a[i]]&f[j];
		}
		printf("%d\n",ans);
	}
	return 0;
}
