#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;
int n,i,ans,hi,a[100005];
inline int read()
{
	int nod=0,p;char ch;
	ch=getchar();
	while (ch!='-'&&(ch<'0'||ch>'9')) ch=getchar();
	if (ch=='-') p=-1,ch=getchar(); else p=1;
	while (ch>='0'&&ch<='9') {nod=nod*10+ch-'0';ch=getchar();}
	return nod*p;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (i=1;i<=n;i++) a[i]=read();hi=0;
	for (i=1;i<=n;i++)
	{
		if (a[i]>=a[i-1]&&a[i]>=a[i+1]&&(!(a[i]==a[i-1]))) ans=ans+a[i]-hi;
		if (a[i]<=a[i-1]&&a[i]<=a[i+1]) hi=a[i];
	}
	printf("%d\n",ans);
	return 0;
}
