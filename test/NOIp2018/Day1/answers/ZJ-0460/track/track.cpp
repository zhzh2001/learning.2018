#include<bits/stdc++.h>
using namespace std;

const int N=5e4+50;
const int inf=0x3f3f3f3f;
int n,m,hd[N],cnt(0);
struct Edge{ int to,nxt,cst;
	void add(int u,int v,int w){
		to=v;nxt=hd[u];cst=w;hd[u]=cnt++;
	}
} edge[N<<1];
int rt,ans,wei[N];

inline int read(){
	char ch=getchar();int x=0,f=1;
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-48;
	return x*f;
}

void dfs1(int x,int fa,int now){
	if(now>ans) {ans=now;rt=x;}
	for(int i=hd[x];~i;i=edge[i].nxt)
		if(edge[i].to!=fa) dfs1(edge[i].to,x,now+edge[i].cst);
}

void solve_sp1(){// m==1
	ans=0;dfs1(1,0,0);ans=0;dfs1(rt,0,0);printf("%d\n",ans);
}

void solve_sp2(){// m==n-1
	ans=inf;
	for(int x=1;x<=n;x++)
		for(int i=hd[x];~i;i=edge[i].nxt)
			ans=min(ans,edge[i].cst);
	printf("%d\n",ans);
}

void prepare(){
	for(int x=1;x<n;x++)
		for(int i=hd[x];~i;i=edge[i].nxt)
			if(edge[i].to==x+1) {wei[x]=edge[i].cst;break;}
	for(int i=2;i<n;i++) wei[i]+=wei[i-1];
}

bool check_sp3(int mi){
	int tot=0,now=1,pos;
	while(tot<m){
		pos=upper_bound(wei+1,wei+n,mi+wei[now-1]-1)-wei;
		if(pos==n) return false;
		now=pos+1;++tot;
	}
	return true;
}

void solve_sp3(){// b[i]==a[i]+1 link
	prepare();
	int l=1,r=wei[n-1]/m,mid;
	while(l<=r){
		mid=(l+r)>>1;
		if(check_sp3(mid)) ans=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",ans);
}

bool check_sp4(int mi){
	int l=1,r=n-1,tot=0;
	while(wei[r]>=mi) ++tot,--r;
	if(tot>=m) return true;
	while(tot<m&&l<r){
		while(wei[l]+wei[r]<mi&&l<r) ++l;
		if(l==r) return false;
		++l;--r;++tot;
	}
	return (tot>=m);
}

void solve_sp4(){// a[1]==1 flower
	int tot=0,l,r,mid;
	for(int i=hd[1];~i;i=edge[i].nxt)
		wei[++tot]=edge[i].cst;
	sort(wei+1,wei+tot+1);
	l=wei[1];r=wei[tot]+wei[tot-1];
	while(l<=r){
		mid=(l+r)>>1;
		if(check_sp4(mid)) ans=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",ans);
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	memset(hd,-1,sizeof(hd));
	bool flag_sp3=true,flag_sp4=true;
	for(int i=1,x,y,z;i<n;i++){
		x=read();y=read();z=read();
		edge[cnt].add(x,y,z);
		edge[cnt].add(y,x,z);
		if(y!=x+1) flag_sp3=false;
		if(x!=1) flag_sp4=false;
	}
	if(m==1) {solve_sp1();return 0;}
	if(m==n-1) {solve_sp2();return 0;}
	if(flag_sp3) {solve_sp3();return 0;}
	if(flag_sp4) {solve_sp4();return 0;}
	srand((unsigned)time(NULL));
	ans=max(2437,rand()*rand());
	printf("%d\n",ans);
	return 0;
}
