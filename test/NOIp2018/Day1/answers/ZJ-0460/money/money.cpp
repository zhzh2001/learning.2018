#include<bits/stdc++.h>
using namespace std;

const int N=100+50;
const int NUM=25000+50;
int T,n,a[N],m,f[NUM];

inline int read(){
	char ch=getchar();int x=0,f=1;
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-48;
	return x*f;
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		n=read();
		for(int i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+n+1);
		if(a[1]==1) {puts("1");continue;}
		memset(f,0x00,sizeof(f));
		m=0;f[0]=1;
		for(int i=1;i<=n;i++){
			if(!f[a[i]]) ++m;
			else continue;
			for(int j=a[i];j<=a[n];j++)
				f[j]|=f[j-a[i]];
		}
		printf("%d\n",m);
	}
	return 0;
}
