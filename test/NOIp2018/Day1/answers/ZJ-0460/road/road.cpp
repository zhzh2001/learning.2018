#include<bits/stdc++.h>
using namespace std;

const int N=100000+50;
int n,d[N];
int q[N<<1],h=1,t=0;
int ans=0;

inline int read(){
	char ch=getchar();int x=0,f=1;
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-48;
	return x*f;
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		d[i]=read();
	for(int i=1,dep;i<=n;i++){
		dep=-1;
		if(h<=t&&d[i]<=d[q[t]]) dep=d[q[t]];
		while(h<=t&&d[i]<=d[q[t]]) --t;
		if(~dep) ans+=(dep-d[i]);
		q[++t]=i;
	}
	ans+=d[q[t]];
	printf("%d\n",ans);
	return 0;
}
