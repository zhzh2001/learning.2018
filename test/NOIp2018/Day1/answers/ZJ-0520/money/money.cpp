#define sto_hl666_orz
#include<bits/stdc++.h>
using namespace std;
int T,n,ans,a[110],mx,hs[25010],dp[25010];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		mx=0;
		memset(dp,0,sizeof(dp));
		memset(hs,0,sizeof(hs));
		scanf("%d",&n);
		for(int i=1;i<=n;i++){
			scanf("%d",&a[i]);
			if(hs[a[i]]){
				n--;
				i--;
				continue;
			}
			hs[a[i]]=1;
			mx=max(mx,a[i]);
			dp[a[i]]=1;
		}
		sort(a+1,a+n+1);
		ans=n;
		for(int i=1;i<=mx;i++){
			if(!dp[i])continue;
			for(int j=1;j<=n;j++){
				if(i+a[j]>mx)break;
				dp[i+a[j]]=2;
			}
		}
		for(int i=1;i<=n;i++){
			if(dp[a[i]]==2)ans--;
		}
		printf("%d\n",ans);
	}
	return 0;
}
