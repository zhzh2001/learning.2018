#define sto_hl666_orz
#include<bits/stdc++.h>
using namespace std;
struct edge{
	int t,v;
};
int n,m,a,b,v,tag1=1,tag2=1;
int d,mx,vis[50010];//m=1
int len[50010],ans;//a=1
int sz,l,r;//a=1 and b=a+1
int lk[50010],mid;//b=a+1
int mn;//QWQ
vector<edge>e[50010];
void Findf(int x,int dis){
	if(dis>mx){
		mx=dis;
		d=x;
	}
	vis[x]=1;
	for(int i=0;i<e[x].size();i++){
		if(!vis[e[x][i].t]){
			Findf(e[x][i].t,dis+e[x][i].v);
		}
	}
	vis[x]=0;
}
void Findlk(int x){
	for(int i=0;i<e[x].size();i++){
		if(e[x][i].t==x+1){
			lk[++sz]=e[x][i].v;
			Findlk(e[x][i].t);
		}
	}
}
bool check(int x){
	int sum=0,tot=0;
	for(int i=1;i<=sz;i++){
		sum+=lk[i];
		if(sum>=x){
			sum=0;
			tot++;
		}
	}
	return tot>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&a,&b,&v);
		e[a].push_back((edge){b,v});
		e[b].push_back((edge){a,v});
		if(a!=1&&b!=1)tag1=0;
		if(abs(a-b)>1)tag2=0;
	}
	if(m==1){
		mx=0;
		Findf(1,0);
		mx=0;
		Findf(d,0);
		printf("%d",mx);
	}else if(tag1){
		for(int i=0;i<e[1].size();i++){
			len[++sz]=e[1][i].v;
		}
		sort(len+1,len+sz+1);
		if(n/2>=m){
			l=1;
			r=sz;
			ans=1e9;
			for(int i=1;i<=m;i++,l++,r--){
				ans=min(ans,len[l]+len[r]);
			}
		}else{
			l=1;
			r=sz;
			ans=1e9;
			while((r-l+1)/2<m){
				ans=min(ans,len[r--]);
				m--;
			}
			for(int i=1;i<=m;i++,l++,r--){
				ans=min(ans,len[l]+len[r]);
			}
		}
		printf("%d",ans);
	}else if(tag2){
		Findlk(1);
		l=1;
		r=1e9;
		while(l<r){
			mid=(l+r+1)/2;
			if(check(mid)){
				l=mid;
			}else{
				r=mid-1;
			}
		}
		printf("%d",l);
	}else{
		mn=1e9;
		for(int i=1;i<=n;i++){
			for(int j=0;j<e[i].size();j++){
				mn=min(mn,e[i][j].v);
			}
		}
		printf("%d",mn);
	}
	return 0;
}
