#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);puts("");
}
ll n,dep,ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();dep=0;
	For(i,1,n){
		ll x=read();
		if (x>dep)	ans+=x-dep,dep=x;
		else		dep=x;
	}writeln(ans);
//	printf("%lf\n",1.0*clock()/CLOCKS_PER_SEC);
}
