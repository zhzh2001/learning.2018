#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);puts("");
}
const ll N=2100,Max_val=400;
ll f[N],a[N],g[N],n,ans;
void Dp(ll n){
	For(i,0,Max_val)f[i]=0;
	f[0]=1;For(i,0,Max_val)if (f[i])For(j,1,n)f[i+a[j]]=1;
}
bool Check0(){
//	For(i,0,Max_val)write(f[i]),putchar(' ');puts("");
//	For(i,0,Max_val)write(g[i]),putchar(' ');puts("");
	For(i,0,Max_val)if (!g[i]&&f[i])return 1;
	return 0;
}
bool Check1(){
	For(i,0,Max_val)if (g[i]^f[i])return 0;
	return 1;
}
void dfs(ll x,ll val){
	Dp(x-1);
	if (x>ans||Check0())return;
	if (Check1()){Min(ans,x-1);return;}
	For(i,val,50){
		a[x]=i;
		dfs(x+1,i+1);
		a[x]=0;
	}
}
int main(){
	freopen("money.in","r",stdin);
	freopen("baoli.out","w",stdout);
	ll T=read();
	for(;T--;){
		n=read();For(i,1,n)a[i]=read();
		Dp(n);ans=n;
//		For(i,0,50)write(f[i]),putchar(' ');puts("");
		memcpy(g,f,sizeof f);
//		For(i,0,Max_val)write(g[i]),putchar(' ');puts("");
		dfs(1,1);
		writeln(ans);
	}
}
