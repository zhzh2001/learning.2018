#include<bits/stdc++.h>
using namespace std;
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);puts("");
}
const ll N=100010;
ll n,ans,a[N],f[N];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(ll T=read();T--;){
		n=read();	ans=0;
		For(i,1,n)a[i]=read();
		sort(a+1,a+n+1);	n=unique(a+1,a+n+1)-a-1;
		For(i,0,25000)f[i]=0;
		f[0]=1;
		For(i,0,25000)if (f[i]){
			Min(f[i],2);
			For(j,1,n)	f[i+a[j]]+=f[i];
		}
		For(i,1,n)if (f[a[i]]>1)++ans;
		writeln(n-ans);
	}
}
