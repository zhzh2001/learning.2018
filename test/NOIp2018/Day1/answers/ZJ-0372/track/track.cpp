#include<bits/stdc++.h>
using namespace std;
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);puts("");
}
const ll N=200010;
ll head[N],nxt[N],vet[N],val[N],cant[N],q[N],f[N],g[N];
ll n,m,tot,total,top,Res;
void insert(ll x,ll y,ll w){
	nxt[++tot]=head[x];head[x]=tot;vet[tot]=y;val[tot]=w;
}
ll Ask(){
	ll l=1,r=top,res=0;
	for(;;){
		if (cant[r])--r;
		else if (q[r]>=Res)--r,++res;
		else	break;
	}
	for(;l<r;){
		if (cant[l])++l;
		else if (cant[r])--r;
		else if(q[l]+q[r]>=Res)++l,--r,++res;
		else	++l;
	}return res;
}
/*void sort(ll l,ll r){
	if (l==r)return;
	ll i=l,j=r,mid=q[(l+r)/2];
	do{
		for(;q[i]<mid;++i);
		for(;q[j]>mid;--j);
		if (i<=j)swap(q[i++],q[j--]);
	}while(i<=j);
	if (i<r)sort(i,r);
	if (l<j)sort(l,j);
}*/
void dfs(ll x,ll fa){
	f[x]=g[x]=0;
	for(ll i=head[x];i;i=nxt[i])if (vet[i]!=fa){
		dfs(vet[i],x);
		f[x]+=f[vet[i]];
	}top=0;
	for(ll i=head[x];i;i=nxt[i])if (vet[i]!=fa)q[++top]=g[vet[i]]+val[i];
	sort(q+1,q+top+1);
	ll l=0,r=top,res;
	f[x]+=(res=Ask());
	for(;l<=r;){
		ll mid=(l+r)>>1;
		cant[mid]=1;
		if (Ask()==res)l=mid+1,g[x]=q[mid];
		else	r=mid-1;
		cant[mid]=0;
	}
//	cout<<x<<' '<<f[x]<<' '<<g[x]<<endl;
}
bool check(ll x){
	Res=x;
	dfs(1,0);
	return f[1]>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();	m=read();
	For(i,2,n){
		ll x=read(),y=read(),w=read();
		insert(x,y,w);
		insert(y,x,w);
		total+=w;
	}
	ll l=1,r=total/m,ans=0;
	for(;l<=r;){
		ll mid=(l+r)>>1;
		if (check(mid))l=mid+1,ans=mid;
		else	r=mid-1;
	}writeln(ans);
//	printf("%lf\n",1.0*clock()/CLOCKS_PER_SEC);
}
/*
7 1
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7
*/
