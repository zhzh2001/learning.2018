#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);puts("");
}
const ll N=200010;
ll head[N],nxt[N],vet[N],val[N],f[N],g[N],q[N],lsg[N];
ll Ans1,Ans2,n,m,top,tot,Res,total;
void insert(ll x,ll y,ll w){
	nxt[++tot]=head[x];head[x]=tot;vet[tot]=y;val[tot]=w;
}
void dfs(ll n,ll choice,ll mx){
	if (!n){
		if (choice>Ans1)Ans1=choice,Ans2=mx;
		else	if (choice==Ans1&&mx>Ans2)	Ans2=mx;
		return;
	}
	if (q[n]>=Res){
		dfs(n-1,choice+1,mx);
	}
	else{
		if (!mx)lsg[n]=1,dfs(n-1,choice,q[n]),lsg[n]=0;
		dfs(n-1,choice,mx);
		For(i,n+1,top)if (!lsg[i]&&q[n]+q[i]>=Res){
			lsg[n]=lsg[i]=1;
			dfs(n-1,choice+1,mx);
			lsg[n]=lsg[i]=0;
		}
	}
}
void dfs(ll x,ll fa){
	f[x]=g[x]=0;
	for(ll i=head[x];i;i=nxt[i])if (vet[i]!=fa)
		dfs(vet[i],x);
	top=0;
	for(ll i=head[x];i;i=nxt[i])if (vet[i]!=fa){
		f[x]+=f[vet[i]];
		q[++top]=g[vet[i]]+val[i];
	}sort(q+1,q+top+1);
	Ans1=Ans2=0;
	dfs(top,0,0);
	f[x]+=Ans1;	g[x]+=Ans2;
//	cout<<x<<' '<<f[x]<<' '<<g[x]<<endl;
}
bool check(ll res){
	Res=res;
	dfs(1,0);
	return f[1]>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	m=read();
	For(i,2,n){
		ll x=read(),y=read(),w=read();total+=w;
		insert(x,y,w);
		insert(y,x,w);
	}
	ll l=0,r=total/m,ans=0;
	for(;l<=r;){
		ll mid=(l+r)>>1;
		if (check(mid))l=mid+1,ans=mid;
		else	r=mid-1;
	}writeln(ans);
}
