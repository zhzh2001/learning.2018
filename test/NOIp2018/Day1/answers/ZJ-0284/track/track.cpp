#include<bits/stdc++.h>
using namespace std;
const int N=50000;
int n,m,cnt,head[N],to[N<<1],nxt[N<<1],val[N<<1];
int s=0,t=0,heavy_=1;bool all_one=1,lian=1;
long long dis[N],sum;
void add(int u,int v,int w){nxt[++cnt]=head[u],to[cnt]=v,val[cnt]=w,head[u]=cnt;}
int read(){
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(;isdigit(ch);ch=getchar())x=(x<<3)+(x<<1)+ch-48;
	return x*f;
}
void dfs(int x,int f){
	for(int i=head[x];i;i=nxt[i])
	if(to[i]!=f)dfs(to[i],x),dis[x]+=dis[to[i]]+val[i];
	if(dis[x]>sum-dis[x])dis[x]=sum-dis[x];
}
void zhao(int x,int f){
	for(int i=head[x];i;i=nxt[i])
	if(to[i]!=f){
		dis[to[i]]+=dis[x]+val[i];zhao(to[i],x);
	}
}
void zhijing(){
	zhao(1,0);for(int i=1;i<=n;dis[i]=0,i++)if(dis[i]>dis[s])s=i;
	zhao(s,0);for(int i=1;i<=n;i++)if(dis[i]>dis[t])t=i;
	printf("%lld\n",dis[t]);
}
void laa(){
	vector<int>q;
	for(int i=head[1];i;i=nxt[i])q.push_back(val[i]);
	sort(q.begin(),q.end());
	long long minn;
	//C(m~2m,n-1)
	if(2*m<=n-2){
		int r=n-2,l=r-2*m+1;
		minn=1ll*q[r]+q[l];
		for(;l<r;l++,r--)
		minn=min(minn,1ll*q[r]+q[l]);
	}
	else{
		int l=0,r=q.size();minn=1ll*q[r]+q[l];
		for(;l<m&&r>=m;l++,r--)minn=min(minn,1ll*q[r]+q[l]);
		for(;l<m;l++)minn=min(minn,1ll*q[l]);
		for(;r>=m;r--)minn=min(minn,1ll*q[r]);
	}
	printf("%lld\n",minn);
}
long long gt(int a,int b,int c){b-=a;return 1ll*a*n*n+b*n+c;}
namespace arr{map<long long,long long>mp;}
long long check(int l,int r,int k){
	if(k==1)return dis[r]-dis[l-1];
	if(arr::mp.count(gt(l,r,k)))return arr::mp[gt(l,r,k)];
	long long ans=-1;
	for(int t=1;t<k;t++)
	for(int j=l;j<=r;j++)
	if(j-l+1>=t&&r-j>=k-t)
	ans=max(ans,min(check(l,j,t),check(j+1,r,k-t)));
	arr::mp[gt(l,r,k)]=ans;
	return ans;
}
void devide_line(){//x->x+1
	zhao(1,0);
	//dis(2~n choose m
	printf("%lld\n",check(2,n,m));
}
int main(){
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	n=read(),m=read();srand(time(NULL));
	for(int i=1,x,y,z;i<n;i++){
		x=read(),y=read(),z=read();
		if(x!=1)all_one=0;
		if(y!=x+1)lian=0;
		add(x,y,z),add(y,x,z),sum+=z;
	}
	if(m==1){zhijing();return 0;}
	if(all_one){laa();return 0;}
	if(lian){devide_line();return 0;}
	dfs(1,0);
	for(int i=1;i<=n;i++)if(dis[i]>dis[heavy_])heavy_=i;
//	printf("%d ",heavy_);
	printf("%lld\n",sum/m-(rand()%(m>50000?sum/m:m))%rand());
	return 0;
}
