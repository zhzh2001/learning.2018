#include<bits/stdc++.h>
using namespace std;
const int N=105,M=25505;
int T,n,a[N],ans=0;
bool is[M];int cnt=0,b[N];
int read(){
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(;isdigit(ch);ch=getchar())x=(x<<3)+(x<<1)+ch-48;
	return x*f;
}
int main(){
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	T=read();
	while(T--){
		n=read();ans=0;cnt=0;
		for(int i=1;i<=n;i++)a[i]=read();
		sort(a+1,a+n+1);
		memset(is,0,sizeof(is));
		int x=0,y=0,maxn=N;
		for(int i=1;i<=n&&a[i]<=maxn;i++){
			if(!is[a[i]]){
				ans++;b[++cnt]=a[i];is[a[i]]=1;
				for(int j=1;j<cnt;j++)
				maxn=min(maxn,a[i]*b[j]-a[i]-b[j]);
			}
			for(int k=1;k*a[i]<=maxn;k++)
			for(int j=1;k*a[i]+j<=maxn;j++)
			if(is[j])is[k*a[i]+j]=1;
		}
		printf("%d\n",min(ans,n));
	}
	return 0;
}
