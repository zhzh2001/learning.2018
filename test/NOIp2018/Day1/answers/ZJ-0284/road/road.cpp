#include<bits/stdc++.h>
using namespace std;
const int N=1e6+5;
int n,a[N];
int read(){
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(;isdigit(ch);ch=getchar())x=(x<<3)+(x<<1)+ch-48;
	return x*f;
}
int solve(int l,int r){
	if(l>r-2)return max(a[l],a[r]);
	int pos=l;
	for(int i=l;i<=r;i++)if(a[i]<a[pos])pos=i;
	if(pos==l)return solve(l+1,r);
	if(pos==r)return solve(l,r-1);
	return solve(l,pos-1)+solve(pos+1,r)-a[pos];
}
int main(){
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	printf("%d\n",solve(1,n));
	return 0;
}
