#include<stdio.h>
#include<stdlib.h>
#define maxn 100010
#include<algorithm>
#define ll long long
using namespace std;
int log_2[maxn];
ll ans;
struct data{
	int pos;
	ll val;
	bool operator <(data x) const{
		return val<x.val;
	}
} a[maxn],st[maxn][20];
data query(int l,int r){
	if(l==r) return st[l][0];
	int k=log_2[r-l>>1]+1;
	return min(st[l][k],st[r-(1<<k)+1][k]);
}
ll solve(int l,int r,int last){
	ll sum=0;
	if(l>r) return 0;
	data tmp=query(l,r);
	sum+=tmp.val-last;
	sum+=solve(l,tmp.pos-1,tmp.val)+solve(tmp.pos+1,r,tmp.val);
	return sum;
}
int main(){
	int i,j,n,m;
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++){
		scanf("%lld",&a[i].val);
		a[i].pos=i;
	}
	log_2[1]=0;
	for(i=2;i<=n;i++){
		log_2[i]=log_2[i>>1]+1;
	}
	for(i=1;i<=n;i++)
		st[i][0]=a[i];
	for(i=1;(1<<i)<=n;i++){
		for(j=1;j+(1<<i)<=n+1;j++){
			st[j][i]=min(st[j][i-1],st[j+(1<<i-1)][i-1]);
		}
	}
	ans=solve(1,n,0);
	printf("%lld",ans);
	return 0;
}
