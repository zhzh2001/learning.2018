#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
using namespace std;
int a[101],dp[25001];
int main(){
	int i,j,n,m,t,k;
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		int ans=0;
		scanf("%d",&n);
		for(i=1;i<=n;i++){
			scanf("%d",&a[i]);
		}
		sort(a+1,a+n+1);
		for(i=1;i<=a[n];i++){
			dp[i]=0;
		}
		dp[0]=1;
		for(i=1;i<=n;i++){
			if(!dp[a[i]]) ans++;
			for(j=a[i];j<=a[n];j++){
				dp[j]|=dp[j-a[i]];
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
