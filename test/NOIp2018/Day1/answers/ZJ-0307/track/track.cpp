#include<stdio.h>
#include<stdlib.h>
#define maxn 50010
#include<algorithm>
#define inf 0x3f3f3f3f
using namespace std;
struct edge{
	int u,v,w;
} e[maxn<<1];
int first[maxn],next[maxn<<1],cnt=0;
int a[maxn],b[maxn],tot=0,n,m;
void add(int x,int y,int z){
	e[++cnt].u=x;
	e[cnt].v=y;
	e[cnt].w=z;
	next[cnt]=first[x];
	first[x]=cnt;
}
bool cmp(int x,int y){
	return x>y;
}
bool mmp(edge x,edge y){
	return x.u<y.u;
}
bool check(int key){
	int i,sum=0,seg=0;
	for(i=1;i<=tot;i++){
		sum+=a[i];
		if(sum>=key){
			seg++;
			sum=0;
		}
	}
	if(seg>=m) return true;
	else return false;
}
int main(){
	int i,j,flag1=1,flag2=1,x,y,z;
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		if(x!=1&&y!=1) flag1=0;
		if(x-y!=1&&y-x!=1) flag2=0;
		add(x,y,z);
		add(y,x,z);
		a[i]=z;
	}
	if(flag1){
		sort(a+1,a+n,cmp);
		for(i=1;i<=m;i++){
			b[i]=a[i];
		}
		j=m+1;
		while(j<n){
			b[--i]+=a[j];
			j++;
		}
		int ans=inf;
		for(i=1;i<=m;i++)
			ans=min(ans,b[i]);
		printf("%d",ans);
	}
	else if(flag2){
		sort(e+1,e+cnt+1,mmp);
		for(i=1;i<=cnt;i++){
			if(e[i].v==e[i].u+1) a[++tot]=e[i].w;
		}
		int l=0,r=inf,ans=0;
		while(l<=r){
			int mid=l+r>>1;
			if(check(mid)){
				ans=max(ans,mid);
				l=mid+1;
			}
			else r=mid-1;
		}
		printf("%d",ans);
	}
	else{
		printf("TAT");
	}
	return 0;
}
