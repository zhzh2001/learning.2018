#include <bits/stdc++.h>

using namespace std;

const int N = 50005;

int n, m;
int tot = -1, head[N];
struct Edge {
    int p, nxt, w;
    Edge(int p = 0, int nxt = 0, int w = 0) : p(p), nxt(nxt), w(w) {}
} edge[N * 2];
inline void Add(int u, int v, int w) {
    edge[++tot] = Edge(v, head[u], w);
    head[u] = tot;
    return;
}

int M;
int f[N], g[N];

int s[N], t[N];
int pre[N], suc[N];

void DFS(int u, int fa) {
    f[u] = g[u] = 0;
    for (int i = head[u]; ~i; i = edge[i].nxt) {
        int v = edge[i].p;
        if (v == fa) continue;
        DFS(v, u);
        f[u] += f[v];
    }
    *s = *t = 0;
    for (int i = head[u]; ~i; i = edge[i].nxt) {
        int v = edge[i].p, w = edge[i].w;
        if (v == fa) continue;
        if (g[v] + w >= M) ++f[u];
        else if (g[v] + w <= (M - 1) / 2) s[++*s] = g[v] + w;
        else t[++*t] = g[v] + w;
    }
    sort(s + 1, s + *s + 1);
    sort(t + 1, t + *t + 1);
    suc[0] = 1;
    pre[0] = *t;
    for (int i = 1; i <= *t; ++i) {
        pre[i] = i - 1;
        suc[i] = i + 1;
    }
    suc[*t] = 0;
    for (int i = 1, j = *t; i <= *s; ++i) {
        if (!j || s[i] + t[j] < M) {
            g[u] = max(g[u], s[i]);
            continue;
        }
        for (; pre[j] && s[i] + t[pre[j]] >= M; j = pre[j]);
        ++f[u];
        suc[pre[j]] = suc[j];
        pre[suc[j]] = pre[j];
        j = suc[j] ? suc[j] : pre[j];
        --*t;
    }
    f[u] += *t / 2;
    if (*t & 1) g[u] = max(g[u], t[pre[0]]);
    return;
}

int main() {
    freopen("track.in", "r", stdin);
    freopen("track.out", "w", stdout);
    scanf("%d%d", &n, &m);
    memset(head, -1, sizeof(head));
    int L = 1, R = 0;
    for (int i = 1; i < n; ++i) {
        int u, v, w;
        scanf("%d%d%d", &u, &v, &w);
        R += w;
        Add(u, v, w);
        Add(v, u, w);
    }
    while (L < R) {
        M = (L + R + 1) / 2;
        DFS(1, 0);
        if (f[1] >= m) L = M;
        else R = M - 1;
    }
    printf("%d\n", L);
    return 0;
}
