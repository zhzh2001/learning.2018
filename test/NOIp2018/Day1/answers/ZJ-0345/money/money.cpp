#include <bits/stdc++.h>

using namespace std;

const int N = 105, M = 25005;

int n;
int a[N];
bool v[M];

void Solve() {
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i)
        scanf("%d", &a[i]);
    sort(a + 1, a + n + 1);
    int res = 0;
    memset(v, 0, sizeof(v));
    v[0] = 1;
    for (int i = 1; i <= n; ++i)
        if (!v[a[i]]) {
            ++res;
            for (int j = a[i]; j < M; ++j)
                v[j] |= v[j - a[i]];
        }
    printf("%d\n", res);
    return;
}

int main() {
    freopen("money.in", "r", stdin);
    freopen("money.out", "w", stdout);
    int t;
    scanf("%d", &t);
    while (t--) Solve();
    return 0;
}
