#include <bits/stdc++.h>

using namespace std;

const int N = 100010;

int n;
int a[N];

int main() {
    freopen("road.in", "r", stdin);
    freopen("road.out", "w", stdout);
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i)
        scanf("%d", &a[i]);
    int res = a[1];
    for (int i = 2; i <= n; ++i)
        res += max(a[i] - a[i - 1], 0);
    printf("%d\n", res);
    return 0;
}
