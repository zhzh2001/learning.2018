#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
int a[100000],vis[500000];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--){
		int n=read();
		for(int i=1;i<=n;i++)a[i]=read();
		if(n==2){
			int mx=max(a[1],a[2]);
			int mn=min(a[1],a[2]);
			if(mx%mn==0)puts("1");
			else puts("2");
		}
		else if(n==3){
			memset(vis,0,sizeof vis);
			sort(a+1,a+1+n);
			if((a[3]%a[1]==0)&&(a[2]%a[1]==0)) puts("1");
			else {
				for(int i=0;i*a[1]<=a[3]+100;i++)
				    for(int j=0;j*a[2]<=a[3]+100;j++)
				        vis[a[1]*i+a[2]*j]=1;
//				for(int i=1;i<=50;i++)if(!vis[i])cout<<i<<" ";puts("");
				if(vis[a[3]]) puts("2");
				else puts("3");
			}
		}
		else cout<<n<<endl;
		/*else {
			int mn=1000000000,mnn=1000000000;
			for(int i=1;i<=n;i++){
				if(a[i]<mn){
					mnn=mn; mn=a[i];
				}
				else if(a[i]<mnn)mnn=a[i];
			}
//			cout<<mn<<" "<<mnn<<endl;
			for(int i=0;i<=mnn;i++){
				for(int j=0;j<=mn;j++){
					cout<<i*mn+j*mnn<<endl;
					vis[i*mn+j*mnn]=1;
				}
			}
//			for(int i=1;i<=100;i++)cout<<vis[i]<<" ";puts("");
		}*/
	}
	return 0;
}
