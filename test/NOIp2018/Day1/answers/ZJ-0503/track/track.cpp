#include <algorithm>
#include <iostream>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
const int N=220005;
int ans,pos,nxt[N],v[N],w[N],head[N],mx[N],mxx[N],tmp[N];
void add(int u,int y,int ww){
	nxt[++pos]=head[u];
	v[pos]=y; w[pos]=ww;
	head[u]=pos;
}
void dfs(int rt,int fa){
	mx[rt]=0,mxx[rt]=0;
	for(int i=head[rt];i;i=nxt[i]){
		int j=v[i];
		if(j==fa)continue;
		dfs(j,rt);
		if(mx[j]+w[i]>mx[rt]){
			mxx[rt]=mx[rt];
			mx[rt]=mx[j]+w[i];
		}
		else if(mx[j]+w[i]>mxx[rt]) mxx[rt]=mx[j]+w[i];
		ans=max(mx[rt]+mxx[rt],ans);
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n=read(),m=read(),flag=1,ff=1;
	for(int i=1;i<n;i++){
		int u=read(),y=read(),ww=read();
		if(u!=1)flag=0;tmp[i]=ww;
		if(y!=u+1)ff=0;
		add(u,y,ww);add(y,u,ww);
	}
	if(m==1){
//		cout<<"aaa";
	    dfs(1,0);
	    cout<<ans<<endl;
	    return 0;
	}
	else if(flag){
		sort(tmp+1,tmp+n);
		cout<<tmp[n-m]<<endl;
		return 0;
	}
	else puts("15");
	return 0;
}
/*
7 3
1 2 11
1 3 56
1 4 22
1 5 12
1 6 1
1 7 6
*/
/*
9 1
1 2 3
1 3 20
1 4 5
2 5 4
2 9 5
3 6 7
3 7 11
6 8 20
*/
