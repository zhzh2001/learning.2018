#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef pair<int, int> pii;

const int maxn = 1000;
int n, m;
vector<pii> g[maxn];
int dep[maxn], depw[maxn], fa[maxn];
bool use[maxn];

void DfsI(int u) {
  for (int i = 0; i < g[u].size(); ++i) {
    int v = g[u][i].first, w = g[u][i].second;
    if (v == fa[u]) continue;
    dep[v] = dep[u] + 1;
    fa[v] = u;
    depw[v] = depw[u] + w;
    DfsI(v);
  }
}

void Init(void) {
  fa[0] = -1;
  DfsI(0);
}

int Can(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  int len = 0;
#define JUMP(u) { if (use[u]) return -1; len += depw[u] - depw[fa[u]]; u = fa[u]; }
  while (dep[u] > dep[v]) {
    JUMP(u);
  }
  while (u != v) {
    JUMP(u);
    JUMP(v);
  }
#undef JUMP
  return len;
}

void Set(int u, int v, int x) {
  if (dep[u] < dep[v]) swap(u, v);
#define JUMP(u) { use[u] = x; u = fa[u]; }
  while (dep[u] > dep[v]) {
    JUMP(u);
  }
  while (u != v) {
    JUMP(u);
    JUMP(v);
  }
#undef JUMP
}

int best;

void Dfs(int i, int cur) {
  if (i == m) {
    best = max(best, cur);
    return;
  }
  for (int u = 0; u < n; ++u) {
    for (int v = u; v < n; ++v) {
      int len = Can(u, v);
      if (len != -1) {
        Set(u, v, 1);
        Dfs(i + 1, min(cur, len));
        Set(u, v, 0);
      }
    }
  }
}

int main(void) {
  scanf("%d%d", &n, &m);
  for (int i = 0; i < n - 1; ++i) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    --u; --v;
    g[u].push_back(make_pair(v, w));
    g[v].push_back(make_pair(u, w));
  }
  Init();
  Dfs(0, 1e9);
  printf("%d\n", best);
  return 0;
}
