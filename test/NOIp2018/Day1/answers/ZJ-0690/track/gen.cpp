#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int main(void) {
  int n = 50000, m = n / 3;
  printf("%d %d\n", n, m);
  for (int i = 1; i < n; ++i) {
    printf("%d %d %d\n", 1, i + 1, rand() % 10000 + 1);
  }
  return 0;
}
