#include <cstdio>
#include <cstring>
#include <algorithm>
#include <ctime>
using namespace std;

void Gen(FILE *f) {
  int n = rand() % 5 + 2;
  int m = rand() % (n - 1) + 1;
  fprintf(f, "%d %d\n", n, m);
  for (int i = 1; i < n; ++i) {
    fprintf(f, "%d %d %d\n", i + 1, rand() % i + 1, rand() % 10000 + 1);
  }
  fclose(f);
}

int main(void) {
  int cnt = 0, tot = 0;
  while (true) {
    FILE *f = fopen("in", "w");
    Gen(f);
    system("./std < in > out && ./brute < in > ans");
    if (system("diff -b ans out")) {
      exit(0);
    } else if (time(0) != cnt) {
      cnt = time(0);
      printf("Ac %d\n", tot);
      fflush(stdout);
    }
    ++tot;
  }
  return 0;
}
