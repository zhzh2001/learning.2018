#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef pair<int, int> pii;
const int inf = 1e9;
const int maxn = 5e4;

int n, m, lim;
vector<pii> g[maxn];
int dp[maxn], cnt[maxn];
int seq[maxn << 1], len;

int Calc(int ban) {
  int ans = 0;
  for (int i = len - 1, j = 0; i > j; --i) {
    if (i == ban) continue;
    while (j < i && (j == ban || seq[j] + seq[i] < lim)) ++j;
    if (i == j) break;
    ++ans;
    ++j;
  }
  // printf("ret %d\n", ans);
  return ans;
}

void Dfs(int u, int p) {
  dp[u] = cnt[u] = 0;
  for (int i = 0; i < g[u].size(); ++i) {
    int v = g[u][i].first;
    if (v == p) continue;
    Dfs(v, u);
    cnt[u] += cnt[v];
  }
  len = 0;
  for (int i = 0; i < g[u].size(); ++i) {
    int v = g[u][i].first, w = g[u][i].second;
    if (v == p) continue;
    seq[len++] = dp[v] + w;
    seq[len++] = 0;
  }
  sort(seq, seq + len);
  // for (int i = 0; i < len; ++i) {
  //   printf("%d%c", seq[i], " \n"[i + 1 == len]);
  // }

  int best = Calc(-1);
  int low = -1, high = len;
  while (high - low > 1) {
    int mid = low + high >> 1;
    if (Calc(mid) == best) {
      low = mid;
    } else {
      high = mid;
    }
  }
  dp[u] = low == -1 ? 0 : seq[low];
  cnt[u] += best;
  // printf("u = %d, cnt = %d, dp = %d\n", u, cnt[u], dp[u]);
}

bool Check(int t_lim) {
  lim = t_lim;
  Dfs(0, -1);
  return cnt[0] >= m;
}

int main(void) {
  scanf("%d%d", &n, &m);
  for (int i = 1; i < n; ++i) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    --u; --v;
    g[u].push_back(make_pair(v, w));
    g[v].push_back(make_pair(u, w));
  }

  int low = 0, high = inf;
  while (high - low > 1) {
    int mid = low + high >> 1;
    if (Check(mid)) {
      low = mid;
    } else {
      high = mid;
    }
  }
  printf("%d\n", low);
  return 0;
}
