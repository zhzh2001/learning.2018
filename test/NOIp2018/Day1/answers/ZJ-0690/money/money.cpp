#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int maxn = 100, maxa = 25000;
int n, a[maxn];
bool can[maxa + 1];

void Push(int x) {
  for (int i = x; i <= maxa; ++i) {
    can[i] |= can[i - x];
  }
}

int main(void) {
  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
  int T;
  scanf("%d", &T);
  while (T--) {
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
      scanf("%d", a + i);
    }
    sort(a, a + n);
    memset(can, 0, sizeof can);
    can[0] = true;
    int m = 0;
    for (int i = 0; i < n; ++i) {
      if (!can[a[i]]) {
        Push(a[i]);
        ++m;
      }
    }
    printf("%d\n", m);
  }
  return 0;
}
