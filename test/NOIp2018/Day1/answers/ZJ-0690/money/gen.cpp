#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int main(void) {
  int T = 200;
  printf("%d\n", T);
  while (T--) {
    int n = 100;
    // int a = 25000;
    printf("%d ", n);
    for (int i = 0; i < n; ++i) {
      // printf(" %d", rand() % 500 + 1);
      printf(" %d", n + i);
    }
    printf("\n");
  }
  return 0;
}
