#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int maxn = 1e5;
int n, a[maxn + 1];

int main(void) {
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
  scanf("%d", &n);
  a[0] = 0;
  for (int i = 0; i < n; ++i) {
    scanf("%d", a + i + 1);
  }
  int ans = 0;
  for (int i = 1; i <= n; ++i) {
    if (a[i] > a[i - 1]) {
      ans += a[i] - a[i - 1];
    }
  }
  printf("%d\n", ans);
  return 0;
}
