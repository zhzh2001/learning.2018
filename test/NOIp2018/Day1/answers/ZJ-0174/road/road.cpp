#include<iostream>
#include<cstring>
#include<cstdio>
using namespace std;
const int N = 1e6 + 10, INF = 1e9;
int a[N], n, b[N];
int sta[N], tp, nxt[N], pre[N];
int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for(int i = 1; i <= n; i++){
		scanf("%d", &a[i]);
//		if(a[i] == a[i - 1]) continue;
		while(a[sta[tp]] >= a[i] && tp) nxt[sta[tp]] = i, tp--;
		sta[++tp] = i;
	}
	while(tp) nxt[sta[tp]] = n + 1, tp--;
	for(int i = n; i >= 1; i--){
//		if(a[i] == a[i - 1]) continue;
		while(a[sta[tp]] > a[i] && tp) pre[sta[tp]] = i, tp--;
		sta[++tp] = i;
	}
//	for(int i = 1; i <= n; i++)
//	  printf("%d %d\n", nxt[i], pre[i]);
	
	for(int i = 1; i <= n; i++)
//	  if(a[i] != a[i - 1])
	    b[i] = a[i] - max(a[nxt[i]], a[pre[i]]);
//	for(int i = 1; i <= n; i++)
//	  printf("%d ", b[i]);
//	puts("");
	int ans = 0;
	for(int i = 1; i <= n; i++)
	  ans += b[i];
	printf("%d\n", ans);
	return 0;
}
