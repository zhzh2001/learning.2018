#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdio>
using namespace std;
const int N = 1e5 + 10;
int head[N], now;
struct edges{ int to, next, w; }edge[N << 1];
void add(int u, int v, int w){ edge[++now] = (edges){v, head[u], w}; head[u] = now;}
int n, m, dp[N], ans, fa[N], dep[N], pre[N];
void dfs(int x){
	for(int i = head[x]; i; i = edge[i].next){
		int v = edge[i].to;
		if(v == fa[x]) continue;
		dep[v] = dep[x] + 1;  fa[v] = x; pre[v] = edge[i].w;
		dfs(v);
	}
}
int dist(int x, int y){
	int res = 0;
	while(x != y){
		if(dep[x] < dep[y]) swap(x, y);
		res += pre[x], x = fa[x];
	}
	return res;
}
void solve1(){
	dfs(1);
	for(int i = 1; i <= n; i++)
	  for(int j = i + 1;  j <= n; j++)
		 ans = max(ans, dist(i, j));
	printf("%d\n", ans);
}
int sum[N];
bool che(int x){
	for(int i = 1; i <= n; i++)  sum[i] = sum[i - 1] + pre[i];
	int l = 1, r = 0, cnt = 0;
	for(int i = 1; i <= n && l <= n; i++){
		while(sum[r + 1] - sum[l] < x && r < n) r++;
		if(r == n) break;
		cnt++, l = r + 1;
	}
	return cnt >= m;
}
void solve2(){
	dfs(1);
	int l = 1, r = 0;
	for(int i = 1; i <= n; i++) r += pre[i];
	while(l <= r){
		int mid = l + r >> 1;
		if(che(mid)) ans = mid, l = mid + 1;
		else r = mid - 1;
	}
	printf("%d\n", ans);
}
bool cmp(int x, int y){ return x > y;}
void solve3(){
	dfs(1);
	sort(pre + 1, pre + n + 1, cmp);
	ans = 1e9; int i = 1;
	int tmp = m;
	if(m * 2 >= n - 1){
		for(i = 1; i <= n && tmp * 2 >= n - 1; i++)
		  tmp--, ans = min(ans, pre[i]);
	}
	int ed = i + 2 * tmp - 1;
	for(int j = 1; tmp >= 0; j++)
	  ans = min(ans, pre[i + j - 1] + pre[ed - j + 1]), tmp--;
	printf("%d\n", ans);
}
int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	bool flg1 = 0, flg2 = 0;
	scanf("%d%d", &n, &m);  int x, y, z;
	for(int i = 1; i < n; i++){
		scanf("%d%d%d", &x, &y, &z);
		add(x, y, z);  add(y, x, z);
		flg1 |= (x != 1), flg2 |= (y != x + 1);
	}
	if(m == 1) solve1();
	else if(!flg2) solve2();
	else if(!flg1) solve3();
	return 0;
}
