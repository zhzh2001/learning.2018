#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdio>
using namespace std;
const int N = 1010;
int n, a[N];
int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T; scanf("%d", &T);
	while(T--){
		scanf("%d", &n);
		for(int i = 1; i <= n; i++) scanf("%d", &a[i]);
		sort(a + 1, a + n + 1);
		if(n == 2){
			if(a[2] % a[1] == 0) puts("1");
			else puts("2");
			continue;
		}
		if(n == 3){
			int t = 0;
			if(a[2] % a[1] == 0) a[2] = a[1], t++;
			if(a[3] % a[1] == 0) a[3] = a[2], a[2] = a[1], t++;
			if(a[2] % a[1] == 0 && a[3] % a[1] == 0){  puts("1"); continue;}
			bool flg = 0;
			for(int i = 0; a[i] * i <= a[3]; i++)
			  for(int j = 0; a[i] * i + a[j] * j <= a[3]; j++)
				if(a[3] % (i * a[1] + j * a[2]) == 0){  flg = 1; break;}
			if(flg == 1) printf("%d\n", 2 - t);
			else printf("%d\n", 3 - t);
		}
	}
	return 0;
}
