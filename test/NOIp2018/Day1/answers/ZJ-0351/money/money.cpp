#include<bits/stdc++.h>
#define ll long long
#define pb push_back
#define db double
using namespace std;
const int N=110;
const int M=25010;
int T;
int dp[M],g[M],n,a[N];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		memset(dp,0,sizeof(dp));
		scanf("%d",&n);
		int tmp=0,Mx=0;
		for(int i=1;i<=n;i++){
			int x;
			scanf("%d",&x);
			Mx=max(Mx,x);
			if(!dp[x]) a[++tmp]=x;
			dp[x]=1;
		}
		memset(dp,0,sizeof(dp));
		memset(g,0,sizeof(g));
		n=tmp;
		for(int i=1;i<=n;i++) g[a[i]]=1;
		
		for(int i=1;i<=n;i++){
			for(int j=1;j<=M-10;j++){
				if(j+a[i]<=Mx){
					dp[j+a[i]]|=(dp[j]|g[j]);
				}
			}
		}
		
		int m=n;
		for(int i=1;i<=n;i++){
			if(dp[a[i]]) m--;
		}
		printf("%d\n",m);
	}
	return 0;	
}
