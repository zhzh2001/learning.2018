#include<bits/stdc++.h>
#define ll long long
#define pb push_back
#define db double
using namespace std;
int n,m;
struct node{
	int v,w,c;
	bool operator <(const node &a)const{
		return w<a.w;	
	}
};
struct P1{
	vector<node>E[20];
	int mn,stk[20],vis[20],top,ans,Ok,tmp;
	void Max(int &x,int y){
		if(x==-1||x<y) x=y;	
	}
	void Min(int &x,int y){
		if(x==-1||x>y) x=y;	
	}
	void addedge(int a,int b,int c,int i){
		E[a].pb((node){b,c,i});
		E[b].pb((node){a,c,i});	
	}
	void Check(int x,int f,int u){
		
		if(x==u){
			for(int i=1;i<=top;i++) if(vis[stk[i]]) Ok=1;
			return ;	
		}
		for(int i=0;i<E[x].size();i++){
			int V=E[x][i].v,W=E[x][i].c;
			if(V==f) continue;
			stk[++top]=W;
			Check(V,x,u);
			top--;
		}
	}
	void Mark(int x,int f,int u){
		
		if(x==u){
			for(int i=1;i<=top;i++) vis[stk[i]]=1;
			Min(mn,tmp);
			return ;	
		}
		for(int i=0;i<E[x].size();i++){
			int V=E[x][i].v,W=E[x][i].c;
			if(V==f) continue;
			stk[++top]=W;
			tmp+=E[x][i].w;
			Mark(V,x,u);
			tmp-=E[x][i].w;
			top--;
		}
	}
	void DFS(int x){
		if(x==m+1){
			Max(ans,mn);
			return ;
		}
		int ori[20],Mn=mn;
		for(int i=1;i<=n-1;i++) ori[i]=vis[i];
		
		for(int i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				Ok=0;
				Check(i,0,j);
				if(Ok) continue;
//				printf("%d %d\n",i,j);
				Mark(i,0,j);
				DFS(x+1);
				mn=Mn;
				for(int t=1;t<=n-1;t++) vis[t]=ori[t];
			}
		}
	}
	void Do(){
		ans=-1;
		for(int i=1;i<=n-1;i++){
			int a,b,c;
			scanf("%d %d %d",&a,&b,&c);
			addedge(a,b,c,i);
		}
		mn=-1;
		DFS(1);
		printf("%d\n",ans);
	}
}Solve1;
const int N=50100;
struct P2{
	
	int e,mx,s,t;
	vector<node>E[N];
	void addedge(int a,int b,int c,int i){
		E[a].pb((node){b,c,i});
		E[b].pb((node){a,c,i});	
	}
	void dfs(int x,int f,int dis){
		if(mx<dis){
			mx=dis;
			e=x;	
		}
		for(int i=0;i<E[x].size();i++){
			int V=E[x][i].v;
			if(V==f) continue;
			dfs(V,x,dis+E[x][i].w);
		}
	}
	void Do(){
		for(int i=2;i<=n;i++){
			int a,b,c;
			scanf("%d %d %d",&a,&b,&c);
			addedge(a,b,c,i);
		}
		e=0,mx=0;
		dfs(1,0,0);
		s=e;
		e=0,mx=0;
		dfs(s,0,0);
		t=e;
		printf("%d\n",mx);
	}
}Solve2;
vector<node>G[N];
void Addedge(int u,int v,int w,int i){
	G[u].pb((node){v,w,i});
	G[v].pb((node){u,w,i});	
}
struct P3{
	
	bool check(int x){
		int cnt=0,r=G[1].size()-1;
		while(r>=0&&G[1][r].w>=x){
			r--;
			cnt++;
			if(cnt>=m) return 1;
		}
		for(int i=0;i<G[1].size();i++){
			if(i<r){
				if(G[1][i].w+G[1][r].w>=x){
					cnt++;
					r--;
				}
			}
			if(cnt>=m) return 1;
		}
		return 0;
	}
	void Do(){
		sort(G[1].begin(),G[1].end());
		int L=0,R=5e8,ret=-1;
		while(L<=R){
			int mid=(L+R)>>1;
			if(check(mid)){
				L=mid+1;
				ret=mid;
			}
			else R=mid-1;
		}
		printf("%d\n",ret);
	}
}Solve3;
struct P4{
	int f[N],mx[N],k;
	void dfs(int x,int fa){
		
		int fir=0,sec=0;
		f[x]=0;
		mx[x]=0;
		for(int i=0;i<G[x].size();i++){
			int V=G[x][i].v,W=G[x][i].w;
			if(V==fa) continue;
			dfs(V,x);
			if(fir<=mx[V]+W){
				sec=fir;
				fir=mx[V]+W;	
			}
			else if(sec<=mx[V]+W){
				sec=mx[V]+W;
			}
			f[x]+=f[V];
		}
		if(fir>=k&&sec>=k){
			mx[x]=0;
			f[x]+=2;	
		}
		else if(sec>=k){
			mx[x]=fir;
			f[x]++;
//			cout<<"!"<<endl;
		}
		else if(fir>=k){
			mx[x]=sec;
			f[x]++;	
		}
		else if(fir+sec>=k){
			mx[x]=0;
			f[x]++;
		}
		else mx[x]=fir;
//		printf("fir=%d sec=%d x=%d f=%d mx=%d\n",fir,sec,x,f[x],mx[x]);
	}
	void Do(){
		int Rt;
		for(int i=1;i<=n;i++) if(G[i].size()==1) Rt=i;
		int L=0,R=5e8,ret=-1;
		while(L<=R){
			k=(L+R)>>1;
			dfs(Rt,0);
//			printf("%d\n",Rt);
			if(f[Rt]>=m){
				L=k+1;
				ret=k;	
			}
			else R=k-1;
		}
		printf("%d\n",ret);
	}
}Solve4;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d %d",&n,&m);
	if(n<=5) Solve1.Do();
	else if(m==1) Solve2.Do();
	else{
		int ck=1;
		for(int i=1;i<n;i++){
			int a,b,c;
			scanf("%d %d %d",&a,&b,&c);
			Addedge(a,b,c,i);
			if(a!=1){
				ck=0;	
			}
		}
		if(ck) Solve3.Do();
		else Solve4.Do();
	}
	return 0;
}
/*
8 4
1 2 5
1 3 5
2 4 7
2 5 7
3 6 5
3 7 5
4 8 10
*/
