#include<bits/stdc++.h>
using namespace std;

const int MAXN = 51000;
int n,m;
int link[MAXN],o;
struct node{
	int to,next,val;
}a[MAXN * 2];
bool flag_1 = 1,flag_2 = 1;
int an = 0x3f3f3f3f;
int hjy[MAXN];

inline void in(int x,int y,int z){
	a[++o].val = z;
	a[o].to = y;
	a[o].next = link[x];
	link[x] = o;
}

void init(){
	cin>>n>>m;
	int x,y,z;
	for(int i = 1;i < n;i++){
		scanf("%d%d%d",&x,&y,&z);
		if(x != 1 && y != 1) flag_1 = 0;
		if(x + 1 != y && x - 1 != y) flag_2 = 0; 
		in(x,y,z);
		in(y,x,z);
	}
}

int q[MAXN],ggo;

bool mycmp(int x,int y){
	return x > y;
}

void work_1(){
	for(int i = 1;i <= o;i += 2) q[++ggo] = a[i].val;
	if(m * 2 <= n - 1){
		sort(q + 1,q + 1 + ggo,mycmp);
	    for(int i = 1;i <= m;i++){
		    an = min(an,q[i] + q[2 * m - i + 1]);
	    }
	}
	else{
		sort(q + 1,q + 1 + ggo);
		int w;
		for(int i = 1;i <= m;i += 2){
			an = min(an,q[i * 2] + q[i * 2 - 1]);
			if(m - i == n - 1 - i * 2){
				w = i * 2 + 1;
				break;
			}
		}
		for(int i = w;i <= ggo;i++) an = min(an,q[i]);
	}
	cout<<an;
}

int d[MAXN],gg[MAXN];

void dfs(int x,int fa){
	for(int i = link[x];i;i = a[i].next){
		if(a[i].to == fa) continue;
		dfs(a[i].to,x);
		gg[x] = max(gg[x],d[a[i].to] + d[x] + a[i].val);
		d[x] = max(d[a[i].to] + a[i].val,d[x]);
	}
}

void work_2(){
	//memset(d,0x3f,sizeof d);
	dfs(1,0);
	an = 0;
	for(int i = 1;i <= n;i++){
		an = max(an,gg[i]);
		//printf("%d ",gg[i]);
	}
	//printf("%d ",d[3]);
	cout<<an;
}

inline bool check(int x){
	int qqq = 0,last = 1;
	for(int i = 1;i <= n;i++){
		if(hjy[i] - hjy[last - 1] >= x){
			qqq++;
			last = i + 1;
			if(qqq == m) return 1;
		}
	}
	return 0;
}

void work_3(){
	for(int i = 1;i <= n;i++){
		for(int k = link[i];k;k = a[k].next){
			if(a[k].to == i - 1) hjy[i] = hjy[i - 1] + a[k].val;
		}
	}
	int l = 0,r = hjy[n];
	while(l + 1 < r){
		int mid = (l + r) >> 1;
		if(check(mid)) l = mid;else
		r = mid - 1;
	}
	printf("%d",l);
}

void work(){
	if(flag_1) work_1();else
	if(m == 1) work_2();else
	if(flag_2) work_3();
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	init();
	work();
	return 0;
}
