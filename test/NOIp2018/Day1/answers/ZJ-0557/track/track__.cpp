#include <map>
#include <set>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define fi first
#define se second
#define mp make_pair
#define pii pair<int,int>
typedef long long ll;
const int N=50010;
int fi[N*2],ne[N*2],la[N*2],a[N*2],c[N*2],b[N];
int i,j,k,n,m,x,y,t,ans,mx,f[N],s[N],p[N],tem;
struct edge{int x,y,t;}w[N];
void add(int x,int y,int t){
	k++;a[k]=y;c[k]=t;
	if (fi[x]==0)fi[x]=k;else ne[la[x]]=k;
	la[x]=k;
}
void dfs(int rt,int fa,int dis){
	if (dis>mx){mx=dis,x=rt;}
	for (int i=fi[rt];i;i=ne[i])
		if (a[i]!=fa){
			dfs(a[i],rt,dis+c[i]);
		}
}
bool check1(){
	for (int i=1;i<n;i++)if (w[i].x!=1)return 0;
	return 1;
}
bool check2(){
	for (int i=1;i<n;i++)if (w[i].x+1!=w[i].y)return 0;
	return 1;
}
multiset<int>S;
void DP(int x,int fa,int lim){
	for (int i=fi[x];i;i=ne[i]){
		if (a[i]!=fa)
		DP(a[i],x,lim);
	}
	p[0]=0;
	for (int i=fi[x];i;i=ne[i])
		if (a[i]!=fa)p[++p[0]]=f[a[i]]+c[i];
	S.clear();
//	for (int i=1;i<=p[0];i++)S.insert(p[i]);
	sort(p+1,p+1+p[0]);
	f[x]=0;
	for (int i=1;i<=p[0];i++){
			if (p[i]>=lim){
				tem++;
			}
			else{
				int res=lim-p[i];
				multiset<int>::iterator it=S.lower_bound(res);
				if (it==S.end()){S.insert(p[i]);}
				else{
					tem++;S.erase(S.find(*it));
				}
			}
		}
	if (S.size()==0)return;
	multiset<int>::iterator it=S.end();it--;
	f[x]=*it;
}
bool check(int ans){
	tem=0;
	DP(1,-1,ans);
	return tem>=m;
}
int main(){
//	freopen("track.in","r",stdin);
//	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&t);
		add(x,y,t);add(y,x,t);
		w[i].x=x;w[i].y=y;w[i].t=t;
	}
	if (m==1){
		dfs(1,-1,0);
		dfs(x,-1,0);
		printf("%d\n",mx);
		return 0;
	}
	if (check1()){
		for (i=1;i<n;i++)a[i]=w[i].t;
		sort(a+1,a+n);
		int l=0,r=500000000;
		while (l<=r){
			int mid=l+r>>1;int tem=0;S.clear();
			for (i=1;i<n;i++)S.insert(a[i]);
//			printf("%d\n",mid);
			for (i=n-1;i>=1;i--)
				if (S.find(a[i])==S.end())continue;
				else{
					if (a[i]>=mid){tem++;S.erase(S.find(a[i]));continue;}
					int res=mid-a[i];S.erase(S.find(a[i]));
					multiset<int>::iterator it=S.lower_bound(res);
					if (it==S.end())continue;
//					printf("!!!%d %d\n",a[i],*it);
					tem++;
					S.erase(it);
				}
			if (tem>=m)ans=mid,l=mid+1;else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	if (check2()){
		for (i=1;i<n;i++)a[w[i].x]=w[i].t;
		for (i=2;i<=n;i++)s[i]=s[i-1]+a[i-1];
		int l=0,r=500000000;
		while (l<=r){
			int mid=l+r>>1;
			memset(f,0,sizeof f);
			for (i=2;i<=n;i++){
				int L=1,R=i-1,ANS=-1;
				while (L<=R){
					int mmid=L+R>>1;
					if (s[i]-s[mmid]>=mid)ANS=mmid,L=mmid+1;else R=mmid-1;
				}
				if (ANS!=-1)f[i]=max(f[i-1],f[ANS]+1);
			}
			if (f[n]>=m)ans=mid,l=mid+1;else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	int l=0,r=500000000;
	while (l<=r){
		int mid=l+r>>1;
		if (check(mid))ans=mid,l=mid+1;else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
