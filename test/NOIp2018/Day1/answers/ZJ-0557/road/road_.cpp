#include <bits/stdc++.h>
using namespace std;
int i,j,k,n,m,x,y,t,a[100010];
typedef long long ll;
int solve(int l,int r){
	if (l>r)return 0;
	int mn=100010;
	for (int i=l;i<=r;i++)mn=min(mn,a[i]);
	int la=l-1;
	int ans=mn;
	for (int i=l;i<=r;i++)a[i]-=mn;
	for (int i=l;i<=r;i++)
		if (a[i]==0)ans+=solve(la+1,i-1),la=i;
	ans+=solve(la+1,r);
	return ans;
}
int main(){
	scanf("%d",&n);
	for (i=1;i<=n;i++)scanf("%d",&a[i]);
	printf("%d\n",solve(1,n));
	return 0;
}
