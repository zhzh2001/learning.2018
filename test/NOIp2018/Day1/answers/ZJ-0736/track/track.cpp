#include <fstream>
#include <vector>
#include <functional>
#include <algorithm>
#include <iterator>
std::ifstream Fin("track.in");
const int max = 50000;
int idx[max | 1];
struct edgeT
{
	int to,W,next;
}edge[max - 1 << 1 | 1];
inline void link(int a,int b,int l)
{
	static int cnt;
	edge[++ cnt] = (edgeT){b,l,idx[a]},idx[a] = cnt;
}
int ans;
typedef std::pair <int,int> PII;
typedef std::vector <int> VI;
VI T[max | 1];
int count(const VI & T)
{
	int res = 0;
	for (VI::const_iterator R = T.end() - 1,L = T.begin();L < R;++ L)
	{
		while (L < R && * L + * R < ans)
			-- R;
		if (L == R)
			break;
		++ res;
	}
	return res;
}
inline int count(VI & T,int pos)
{
	int val = T[pos];
	T.erase(T.begin() + pos);
	int ret = count(T);
	T.insert(T.begin() + pos,val);
	return ret;
}
PII DFS(int cur,int fa)
{
	int res = 0;
	VI & T = ::T[cur];
	T.clear();
	for (int i = idx[cur];i;i = edge[i].next)
		if (edge[i].to != fa)
		{
			PII sub(DFS(edge[i].to,cur));
			res += sub.first;
			T.push_back(edge[i].W + sub.second);
		}
	if (T.size())
	{
		std::sort(T.begin(),T.end(),std::greater <int>());
		int max = count(T);
		res += max;
		int L = 0,R = T.size() - 1;
		while (L < R)
		{
			int mid = L + R >> 1;
			if (count(T,mid) < max)
				L = mid + 1;
			else
				R = mid;
		}
		int rem = count(T,L) == max ? T[L] : 0;
		if (rem >= ans)
			++ res,rem = 0;
		return (PII){res,rem};
	}
	return (PII){0,0};
}
int main()
{
	std::ios_base::sync_with_stdio(0),Fin.tie(0);
	int n,m;
	Fin >> n >> m;
	while (-- n)
	{
		int ai,bi,li;
		Fin >> ai >> bi >> li;
		link(ai,bi,li),link(bi,ai,li);
	}
	int L = 1,R = (max - 1) * 10000;
	while (L < R)
	{
		ans = L + R + 1 >> 1;
		if (DFS(1,0).first >= m)
			L = ans;
		else
			R = ans - 1;
	}
	std::ofstream("track.out") << L << '\n';
}
