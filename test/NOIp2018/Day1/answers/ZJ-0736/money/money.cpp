#include <fstream>
#include <algorithm>
#include <cstring>
std::ifstream Fin("money.in");
int a[100];
const int max = 25000;
bool exist[max | 1];
std::ofstream Fout("money.out");
void solve()
{
	int n;
	Fin >> n;
	for (int i = 0;i < n;++ i)
		Fin >> a[i];
	std::sort(a,a + n);
	std::memset(exist + 1,0,max);
	int m = n;
	for (int i = 0;i < n;++ i)
	{
		int ai = a[i];
		if (! exist[ai])
			for (int j = ai;j <= max;++ j)
				exist[j] |= exist[j - ai];
		else
			-- m;
	}
	Fout << m << '\n';
}
int main()
{
	std::ios_base::sync_with_stdio(0),Fin.tie(0);
	int T;
	Fin >> T;
	for (* exist = 1;T --;solve());
}
