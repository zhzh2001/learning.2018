#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
int T,n,ans,max0,a[3500];
bool f[35000];
bool cmp(int x,int y)
{
	return x<y;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T)
	{
		T--;
		max0=0;
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]),max0=max(max0,a[i]);
		sort(a+1,a+n+1,cmp);
		memset(f,false,sizeof(f));
		f[0]=true;
		ans=n;
		for (int i=1;i<=n;i++)
		{
			if (f[a[i]]) ans--;
			for (int j=a[i];j<=max0;j++) if (f[j-a[i]]) f[j]=true;
		}
		printf("%d\n",ans);
	}
	return 0;
}
