#include<cstdio>
#include<cstring>
#include<cmath>
#include<queue>
#include<algorithm>
using namespace std;
int n,m,x,y,z,min0,cnt,max0,max1,head[103500],nxt[103500],to[103500],val[103500],dep[103500];
int l,r,tot,mid,ans,cnt0,p[103500];
bool flag0,flag1,f1[103500];
bool cmp(int x,int y)
{
	return x>y;
}
void add(int u,int v,int w)
{
	cnt++;nxt[cnt]=head[u];to[cnt]=v;val[cnt]=w;head[u]=cnt;
}
void dfs(int u,int fa)
{
	for (int i=head[u];i;i=nxt[i])
	{
		int v=to[i];
		if (v==fa) continue;
		dep[v]=max(dep[v],dep[u]+val[i]);
		if (dep[v]>max0) {max0=dep[v];max1=v;}
		dfs(v,u);
	}
}
bool check1(int x)
{
	int l1,r1,mid1,ans1;
	memset(f1,true,sizeof(f1));
	for (int i=1;i<=m;i++)
	{
		if (p[i]>=x) {f1[i]=false;continue;}
		l1=i;r1=n;
		ans1=-1;
		while (l1<r1)
		{
			mid1=(l1+r1)>>1;
			if (p[i]+p[mid1]>=x) {if (f1[mid1]) ans1=mid1;l1=mid1+1;}
			else {r1=mid1;}
		}
		if (ans1==-1) return false;
		f1[i]=false;
		f1[ans1]=false;
	}
	return true;
}
bool check11(int x)
{
	bool flag=false;
	cnt0=0;
	memset(f1,true,sizeof(f1));
	for (int i=1;i<=m;i++)
	{
		if (cnt0>=m) {flag=true;break;}
		if (p[i]>=x) {cnt0++;continue;}
		for (int j=n-1;j>=i+1;j--)
			if (p[j]+p[i]>=x && f1[j]) {f1[j]=false;cnt0++;break;}
	}
	if (cnt0>=m) {flag=true;}
	return flag;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	flag0=true;
	flag1=true;
	min0=1e9+7;
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n-1;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z);add(y,x,z);
		min0=min(min0,z);
		if (x!=1) flag0=false;
		if (y!=x+1) flag1=false;
		r=r+z;
	}
	if (m==n-1) {printf("%d\n",min0);return 0;}
	if (m==1)
	{
		dfs(1,0);
		max0=0;
		memset(dep,0,sizeof(dep));
		dfs(max1,0);
		printf("%d\n",max0);
		return 0;
	}
	l=1;
	if (flag0)
	{
		tot=0;
		for (int i=head[1];i;i=nxt[i]) if(to[i]!=1) p[++tot]=val[i];
		sort(p+1,p+tot+1,cmp);
		while (l<r)
		{
			mid=(l+r)>>1;
			if (check11(mid)) {l=mid+1;ans=mid;}
			else r=mid;
		}
		while (l<r)
		{
			mid=(l+r)>>1;
			if (check1(mid)) {l=mid+1;ans=mid;}
			else r=mid;
		}
	}
	printf("%d\n",ans);
	return 0;
}
