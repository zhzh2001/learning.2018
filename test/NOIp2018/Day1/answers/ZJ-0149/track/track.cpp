#include<bits/stdc++.h>
using namespace std;
#define M 50005
bool cur1;
bool f;
int n,m,i,a,b,l,v[M<<1],r[M<<1],h[M<<1],p[M<<1],tot;
void add(int x,int y,int t) {
	v[++tot]=t,r[tot]=y,p[tot]=h[x],h[x]=tot;
}
struct P1 {
	int mx,id;
	void dfs(int x,int f,int d) {
		if(mx<d)mx=d,id=x;
		for(int i=h[x]; i!=-1; i=p[i])
			if(r[i]!=f)dfs(r[i],x,d+v[i]);
	}
	void solve() {
		dfs(1,0,0),mx=0,dfs(id,0,0),printf("%d\n",mx);
	}
} p1;
struct P2 {
	int i,j,l,R,ans,mid,t[M];
	bool check(int x) {
		int res=0,cnt=0;
		for(i=1; i<n&&cnt<m; i++) {
			res+=t[i];
			if(res>=x)res=0,cnt++;
		}
		return cnt>=m;
	}
	void solve() {
		for(i=1; i<n; i++)
			for(j=h[i]; j!=-1; j=p[j])if(r[j]==i+1)t[i]=v[j];
		l=1,R=5e8;
		while(l<=R) {
			mid=l+R>>1;
			if(check(mid))l=mid+1,ans=mid;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
} p2;
struct P3 {
	int L,R,mid,ans,i,cnt,s[M];
	vector<int>a[M];
	bool mark[M];
	int lowe(int x,int y) {
		int l=0,r=a[x].size()-1,mid,ans=r+1;
		while(l<=r) {
			mid=l+r>>1;
			if(a[x][mid]>=y)ans=mid,r=mid-1;
			else l=mid+1;
		}
		return ans;
	}
	void dfs(int x,int f) {
		if(cnt>=m)return;
		for(int i=h[x]; i!=-1; i=p[i])
			if(r[i]!=f) {
				dfs(r[i],x);
				if(s[r[i]]+v[i]>=mid)cnt++;
				else a[x].push_back(s[r[i]]+v[i]);
			}
		sort(a[x].begin(),a[x].end());
		int mx=0,y;
		for(i=0; i<a[x].size(); i++)
			if(!mark[i]) {
				y=lowe(x,mid-a[x][i]);
				if(y<=i)y=i+1;
				while(mark[y])y++;
				if(y==a[x].size())mx=a[x][i];
				else mark[y]=1,cnt++;
			} else mark[i]=0;
		a[x].clear(),s[x]=mx;
	}
	bool check() {
		cnt=0,dfs(1,0);
		return cnt>=m;
	}
	void solve() {
		L=1,R=5e8;
		while(L<=R) {
			mid=L+R>>1;
			if(check())L=mid+1,ans=mid;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
} p3;
struct P4 {
	int L,R,mid,ans,i,cnt,s[M];
	vector<int>a[M];
	int find(int x) {
		int l=0,r=a[x].size()-2,ans=r+1,Mid;
		while(l<=r) {
			Mid=l+r>>1;
			if(a[x][Mid]+a[x][Mid+1]>=mid)ans=Mid,r=Mid-1;
			else l=Mid+1;
		}
		return ans;
	}
	void dfs(int x,int f) {
		if(cnt>=m)return;
		for(int i=h[x]; i!=-1; i=p[i])
			if(r[i]!=f) {
				dfs(r[i],x);
				if(s[r[i]]+v[i]>=mid)cnt++;
				else a[x].push_back(s[r[i]]+v[i]);
			}
		if(!a[x].size()) {
			s[x]=0;
			return;
		} else if(a[x].size()==1) {
			s[x]=a[x][0];
			return;
		}
		sort(a[x].begin(),a[x].end());
		int si=a[x].size(),l=find(x),r=l+1,mx=0;
		if(r==si) {
			s[x]=a[x][si-1],a[x].clear();
			return;
		}
		while(l>0&&a[x][l-1]+a[x][r]>=mid)mx=a[x][l],l--;
		while(r<si&&l>=0) {
			while(r<si&&a[x][l]+a[x][r]<mid)mx=a[x][r],r++;
			if(r<si)r++,l--,cnt++;
		}
		if(r<si)mx=a[x][si-1];
		if(!mx&&l>=0)mx=a[x][l];
		a[x].clear(),s[x]=mx;
	}
	bool check() {
		cnt=0,dfs(1,0);
		return cnt>=m;
	}
	void solve() {
		L=1,R=5e8;
		while(L<=R) {
			mid=L+R>>1;
			if(check())L=mid+1,ans=mid;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
} p4;
bool cur2;
int main() {
//	freopen("data.txt","r",stdin);
//	printf("%.lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m),memset(h,-1,sizeof(h));
	for(i=1; i<n; i++)scanf("%d%d%d",&a,&b,&l),add(a,b,l),add(b,a,l),f=f?1:(b!=a+1);
	if(m==1)p1.solve();
	else if(!f)p2.solve();
	else p3.solve();
//	p4.solve();
	return 0;
}
