#include<bits/stdc++.h>
using namespace std;
bool cur1;
int n,i,d[100005];
struct P1 {
	int ans;
	bool f;
	void solve() {
		while(!f) {
			f=1;
			for(i=n+1; i>0; i--)
				if(d[i]==0&&d[i-1]!=0)f=0,ans++;
				else if(d[i]!=0)d[i]--;
		}
		printf("%d\n",ans);
	}
} p1;
struct P2 {
	int l[100005],r[100005];
	long long ans;
	struct node {
		int v,id;
		bool operator<(const node&_)const {
			return _.v<v;
		}
	} b[100005];
	void solve() {
		for(i=1; i<=n; i++)l[i]=i-1,r[i]=i+1,b[i].v=d[i],b[i].id=i;
		sort(b+1,b+n+1);
		for(i=1; i<=n; i++)l[r[b[i].id]]=l[b[i].id],r[l[b[i].id]]=r[b[i].id];
		for(i=1; i<=n; i++)ans+=d[i]-max(d[l[i]],d[r[i]]);
		printf("%lld\n",ans);
	}
} p2;
bool cur2;
int main() {
//	printf("%.lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(i=1; i<=n; i++)scanf("%d",&d[i]);
	p2.solve();
	return 0;
}
