#include<cstdio>
#include<algorithm>
using namespace std;
const int N=105,M=25005,inf=1e9;
int n,m,i,j,ans;
int a[N];
bool f[M];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (int TEST=read();TEST--;){
		for (n=read(),i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+n+1);m=a[n];ans=0;
		for (f[0]=1,i=1;i<=m;i++) f[i]=0;
		for (i=1;i<=n;i++){
			if (f[a[i]]) continue;
			for (ans++,j=a[i];j<=m;j++)
				f[j]|=f[j-a[i]];
		}
		printf("%d\n",ans);
	}
}
