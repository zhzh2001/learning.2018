#include<cstdio>
const int N=1e5+5;
int n,m,i,ans;
int a[N],d[N];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	for (n=read(),i=1;i<=n;i++) a[i]=read();
	for (d[0]=-1,i=1;i<=n;i++){
		if (a[i]<d[m]) ans+=d[m]-a[i];
		for (;a[i]<=d[m];m--);d[++m]=a[i];
	}
	ans+=d[m];
	printf("%d",ans);
}
