#include<cstdio>
#include<algorithm>
using namespace std;
const int N=5e4+5,M=2*N;
int n,m,i,sum,l,r;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
int et,he[N],v[N],dn,d[N];
struct edge{int l,to,v;}e[M];
void add_edge(){
	int x=read(),y=read(),v=read();
	l=min(l,v);sum+=v;
	e[++et].l=he[x];he[x]=et;e[et].to=y;e[et].v=v;
	e[++et].l=he[y];he[y]=et;e[et].to=x;e[et].v=v;
}
int st,hs[N];
struct son{int l,to;}s[N];
void dfs(int x){
	for (int i=he[x];i;i=e[i].l){
		int y=e[i].to;if (v[y]) continue;
		s[++st].l=hs[x];hs[x]=st;s[st].to=y;
		v[y]=e[i].v;dfs(y);
	}
	d[++dn]=x;
}
int f[N],qn,q[N];
bool check(int val){
	int i,j,cnt=0;
	for (int now=1;now<=n;now++){
		int x=d[now];f[x]=0;qn=0;
		for (i=hs[x];i;i=s[i].l) q[++qn]=f[s[i].to];
		sort(q+1,q+qn+1);
		for (;q[qn]>=val;qn--){
			cnt++;if (cnt==m) return 1;
		}
		int cx=qn,cy=qn-1,ct=0;
		for (i=1;i<=cy;i++){
			if (cy>i&&q[i]+q[cy]>=val){
				cnt++;if (cnt==m) return 1;
				cy--;
			}
			else if (q[i]+q[cx]>=val){
					cnt++;if (cnt==m) return 1;
					cx=cy;cy--;
				 }
				 else ct=i;
		}
		if (cx<i) cx=0;
		f[x]=max(q[cx],q[ct])+v[x];
	}
	return 0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();l=1e9;
	for (i=1;i<n;i++) add_edge();
	v[1]=1;dfs(1);
	for (r=sum/m;l<r;){
		int mid=l+r+1>>1;
		if (check(mid)) l=mid; else r=mid-1;
	}
	printf("%d",l);
}
