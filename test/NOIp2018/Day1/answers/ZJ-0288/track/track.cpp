#include <cstdio>
#include <algorithm>
using namespace std;
#define N 100001
int ans;
int tot;
int a[N];
int max1,max2,dep[N];
int fir[N],nex[N],got[N],cos[N];
inline int read()
{
	int x=0;
	char ch=getchar();
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
inline int max(int x,int y)
{
	return x>y?x:y;
}
inline int cmp(int x,int y)
{
	return x>y;
}
inline void add_edge(int x,int y,int z)
{
	nex[++tot]=fir[x];fir[x]=tot;got[tot]=y;cos[tot]=z;
}
inline void dfs1(int x,int fa)
{
	if (dep[x]>max1) max1=dep[x],max2=x;
	for (int i=fir[x];i;i=nex[i])
	{
		int y=got[i];
		int z=cos[i];
		if (y==fa) continue;
		dep[y]=dep[x]+z;
		dfs1(y,x);
	}
}
inline int dfs2(int x,int fa)
{
	int ans=0;
	for (int i=fir[x];i;i=nex[i])
	{
		int y=got[i];
		int z=cos[i];
		if (y==fa) continue;
		ans=max(ans,z+dfs2(y,x));
	}
	return ans;
}
inline bool pd(int x,int k,int n)
{
	int len=0,num=0;
	for (int i=1;i<=n-1;i++)
	{
		len+=a[i];
		if (len>=x) len=0,++num;
		if (num>=k) return true;
	}
	return false;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n=read();
	int m=read();
	bool f1,f2,f3;
	f1=f2=f3=true;
	if (m!=1) f1=false;
	for (int i=1;i<=n-1;i++)
	{
		int x,y,z;
		x=read();y=read();z=read();
		add_edge(x,y,z);add_edge(y,x,z);
		if (x!=1) f2=false;
		if (y!=x+1) f3=false;
	}
	if (f1)
	{
		dfs1(1,0);
		printf("%d\n",dfs2(max2,0));
		return 0;
	}
	if (f2)
	{
		int cnt=0;
		for (int i=1;i<=tot;i++)
			if (i&1) a[++cnt]=cos[i];
		sort(a+1,a+cnt+1,cmp);
		if (m==1) printf("%d",a[1]+a[2]);
		else printf("%d\n",a[m]);
		return 0;
	}
	if (f3)
	{
		int t=1,w=500000000;
		for (int i=1;i<=n-1;i++)
			for (int j=fir[i];j;j=nex[j])
				if (got[j]==i+1) a[i]=cos[j];
		int ans=0;
		while (t<=w)
		{
			int mid=t+w>>1;
			if (pd(mid,m,n)) ans=mid,t=mid+1;
			else w=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	printf("%d\n",ans);
	return 0;
}
