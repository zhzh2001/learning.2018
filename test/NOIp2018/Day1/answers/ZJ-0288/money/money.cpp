#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int a[101];
bool f[25001];
inline int read()
{
	int x=0;
	char ch=getchar();
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while (T--)
	{
		int n=read();
		for (int i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+n+1);
		int ans=0;;
		memset(f,false,sizeof(f));
		f[0]=true;
		for (int i=1;i<=n;i++)
		{
			if (f[a[i]]) continue;
			for (int j=0;j<=a[n]-a[i];j++)
				if (f[j] && !f[j+a[i]]) f[j+a[i]]=true;
			++ans;
		}
		printf("%d\n",ans);
	}
	return 0;
}