#include <cstdio>
#include <iostream>
#define min(a,b) ((a)<(b)?(a):(b))
using namespace std;
#define N 100001
int n;
int a[N];
long long sc(int l,int r,int k)
{
	if (l>r) return 0;
	if (l==r) return a[l]-k;
	long long minn=INT_MAX;
	for (int i=l;i<=r;i++) 
		if (a[i]-k<minn) minn=a[i]-k;
	long long ans=0,l1=l;
	for (int i=l;i<=r;i++)
		if (a[i]-k==minn) ans+=sc(l1,i-1,k+minn),l1=i+1;
	return ans+minn+sc(l1,r,minn+k);
}
inline int read()
{
	int x=0;
	char ch=getchar();
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) a[i]=read();
	printf("%lld\n",sc(1,n,0));
	return 0;
}
