#include<bits/stdc++.h>
using namespace std;
const int N=105,MAXN=3e5+7;
int n,T,a[N];
bool vis[MAXN];
int main() {
    freopen("money.in","r",stdin);
    freopen("money.out","w",stdout);
    scanf("%d",&T);
    while (T--) {
        scanf("%d",&n);
        for (int i=1;i<=n;i++) scanf("%d",&a[i]);
        sort(a+1,a+1+n);

        memset(vis,false,sizeof(vis));
        vis[0]=true;

        int ans=0;
        for (int i=1;i<=n;i++) {
            if (!vis[a[i]]) ans++;
            for (int j=0;j<=25007-a[i];j++) {
                if (vis[j]) {
                    vis[j+a[i]]=true;
                }
            }
        }
        printf("%d\n",ans);
    }
    return 0;
}
