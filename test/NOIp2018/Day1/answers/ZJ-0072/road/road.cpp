#include<bits/stdc++.h>
using namespace std;
const int N=1e6+7;
int n,a[N];
int main() {
    freopen("road.in","r",stdin);
    freopen("road.out","w",stdout);
    scanf("%d",&n);
    for (int i=1;i<=n;i++) {
        scanf("%d",&a[i]);
    }
    int ans=0,now=0;
    for (int i=1;i<=n;i++) {
        if (a[i]>now) {
            ans+=a[i]-now;
        }
        now=a[i];
    }
    printf("%d\n",ans);
    return 0;
}
