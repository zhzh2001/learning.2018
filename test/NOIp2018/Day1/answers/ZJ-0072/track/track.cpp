#include<bits/stdc++.h>
using namespace std;
const int N=5e4+7;
int n,m,t;
int a[N],sum[N],dis[N];
bool vis[N];
struct Edge {int to,val;};
queue<int> q;
vector<Edge> edge[N];
inline void spfa(int s) {
    memset(dis,127,sizeof(dis));
    memset(vis,false,sizeof(vis));
    vis[s]=true; dis[s]=0; q.push(s);
    while (!q.empty()) {
        int x=q.front(); q.pop(); vis[x]=false;
        for (int i=0;i<edge[x].size();i++) {
            int y=edge[x][i].to,w=edge[x][i].val;
            if (dis[y]>dis[x]+w) {
                dis[y]=dis[x]+w;
                if (!vis[y]) {
                    vis[y]=true;
                    q.push(y);
                }
            }
        }
    }
    t=1;
    for (int i=1;i<=n;i++) {
        if (dis[i]>dis[t]) t=i;
    }
}
inline bool judge(int x) {
    int sum=0,num=0;
    for (int i=1;i<n;i++) {
        sum+=a[i];
        if (sum>=x) {
            sum=0; num++;
        }
    }
    if (num>=m) return true;
    else return false;
}
int main() {
    freopen("track.in","r",stdin);
    freopen("track.out","w",stdout);
    scanf("%d%d",&n,&m);
    bool flag1=true,flag2=true;
    for (int i=1;i<n;i++) {
        int x,y,z;
        scanf("%d%d%d",&x,&y,&z);
        if (x!=1) flag1=false;
        if (y!=x+1) flag2=false;
        edge[x].push_back((Edge){y,z});
        edge[y].push_back((Edge){x,z});
        a[i]=z;
    }
    if (flag1 || flag2) {
        int l=0,r=1e9;
        while (l<r) {
            int mid=(l+r+1)/2;
            if (judge(mid)) l=mid;
            else r=mid-1;
        }
        printf("%d\n",l);
        return 0;
    }
    spfa(1); spfa(t);
    printf("%d\n",dis[t]);
    return 0;
}
