#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline void ju() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
}
const int N = 101000;
int n, a[N];
ll ans, t;
int main() {
	ju();
	scanf("%d", &n);
	for(int i = 1; i <= n; ++i) scanf("%d", &a[i]);
	for(int i = 1; i < n; ++i)
		if(a[i]-t>0) {
			if(a[i]>a[i+1]) {
				if(a[i+1]-t>0) ans += a[i]-a[i+1];
				else ans += a[i]-t;
			}
			else {
				ans += a[i]-t;
				t += a[i]-t;
			}
		}
		else t += a[i]-t;
	if(a[n]-t>0) ans += a[n]-t;
	printf("%lld\n", ans);
	return 0;
}
