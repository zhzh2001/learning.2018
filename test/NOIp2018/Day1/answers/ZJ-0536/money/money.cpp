#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline void ju() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
}
const int N = 110, M = 30000;
int a[N], T, n, p;
bool b[M];
int main() {
	ju();
	scanf("%d", &T);
	while(T--) {
		int ans = 0;
		memset(b, 0, sizeof b); b[0] = 1;
		scanf("%d", &n);
		for(int i = 1; i <= n; ++i) scanf("%d", &a[i]);
		sort(a+1, a+n+1); p = n;
		for(int i = 1; i <= n; ++i) {
			if(!b[a[i]]) {
				++ans;
				for(int j = a[i]; j <= a[p]; ++j)
					if(b[j-a[i]])
						b[j] = 1;
				while(p>=2 && b[a[p]]) --p;
			}
		}
		printf("%d\n", ans);
	}
	return 0;
}
