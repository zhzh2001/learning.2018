#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline void ju() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
}
const int N = 50500;
int n, m, cnt, ans, l, r, sum, tot;
int fst[N], nxt[N], to[N], len[N];
int f[N], g[N], a[N];
bool vis[N], sub1;
inline void add(int x, int y, int z) {
	nxt[++cnt] = fst[x]; fst[x] = cnt; to[cnt] = y; len[cnt] = z;
	nxt[++cnt] = fst[y]; fst[y] = cnt; to[cnt] = x; len[cnt] = z;
}
inline void dfs(int u) {
	vis[u] = 1; int v, w, tot = 0;
	for(int i = fst[u]; i; i = nxt[i]) {
		v = to[i]; w = len[i];
		if(!vis[v]) {
			dfs(v);
			f[u] += f[v];
			if(g[v]+w>=ans) ++f[u];
			else if(g[u]+g[v]+w>=ans) ++f[u], g[u] = 0;
			else if(g[v]+w>g[u]) g[u] = g[v]+w;
		}
	}
}
int main() {
	ju();
	scanf("%d%d", &n, &m); sub1 = 1;
	for(int i = 1; i < n; ++i) {
		int x, y, z; scanf("%d%d%d", &x, &y, &z);
		add(x, y, z); sum += z;
		if(x != 1) sub1 = 0;
	}
	l = 1, r = sum/m;
	if(sub1) {
		tot = 0;
		for(int i = fst[1]; i; i = nxt[i])
			a[++tot] = len[i];
		sort(a+1, a+tot+1);
	}
	while(l < r) {
		ans = (l+r+1)/2;
		if(sub1) {
			tot = n-1; int s = 0;
			while(tot>0&&a[tot]>=ans) ++s, --tot;
			int i = 1, j = tot;
			while(i<j) {
				while(i<j&&a[i]+a[j]<ans) ++i;
				if(i<j) ++s, ++i, --j;
			}
			if(s>=m) l = ans;
			else r = ans-1;
			continue;
		}
		memset(f, 0, sizeof f);
		memset(g, 0, sizeof g);
		memset(vis, 0, sizeof vis);
		dfs(1);
		if(f[1]>=m) l = ans;
		else r = ans-1;
	}
	printf("%d\n", l);
	return 0;
}
