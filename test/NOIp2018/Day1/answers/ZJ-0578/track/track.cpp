#include<bits/stdc++.h>
#define FOR(i,x,y) for(int i=(x);i<=(y);i++)
#define DOR(i,x,y) for(int i=(x);i>=(y);i--)
#define LL long long 
#define NM 50005
using namespace std;
int ver[NM<<1],head[NM],eg[NM<<1],nex[NM<<1],tot,n,m,Q1,Q2,Q3,s2,l,r,p[NM<<1],MAX,d1,d2;
struct node{
	int x,y,z;
}a[NM<<1];
void add(int x,int y,int z){
	ver[++tot]=y; eg[tot]=z; nex[tot]=head[x]; head[x]=tot;
}
bool cmp(node a,node b){
	return a.z<b.z;
}
bool check1(int mid){
	int L=1,R=n-1,ans=0;
	while(1){
		while(L<R&&a[L].z+a[R].z<mid) L++;
		if(L>=R) break;
		ans++; R--; L++;
	}
	return ans>=m;
}
void sol1(){
	int ans;
	sort(a+1,a+n,cmp);
	l=1; r=500000000;
	while(l<=r){
		int mid=(l+r)>>1;
		if(check1(mid)) ans=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",ans);
	exit(0);
}
void dfs2(int x,int fa){
	for(int i=head[x];i;i=nex[i]){
		int y=ver[i],z=eg[i];
		if(y==fa) continue;
		p[++s2]=z;
		dfs2(y,x);
	}
}
bool check2(int mid){
	int sum=0,ans=0;
	FOR(i,1,n-1){
		sum+=p[i];
		if(sum>=mid) sum=0,ans++;
	}
	return ans>=m;
}
void sol2(){
	int ans;
	dfs2(1,0);
	l=1; r=500000000;
	while(l<=r){
		int mid=(l+r)>>1;
		if(check2(mid)) ans=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",ans);
	exit(0);
}
void dfs3(int x,int fa,int sum){
	for(int i=head[x];i;i=nex[i]){
		int y=ver[i],z=eg[i];
		if(y==fa) continue;
		if(sum+z>MAX) MAX=sum+z,d2=y;
		dfs3(y,x,sum+z); 
	}
}
void sol3(){
	MAX=0;
	dfs3(1,0,0); d1=d2; MAX=0; dfs3(d1,0,0);
	printf("%d\n",MAX);
	exit(0);
}
int main(){  // long long   mod   neicun
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	Q1=Q2=1;
	if(m==1) Q3=1;
	FOR(i,1,n-1){
		scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
		if(a[i].x!=1&&a[i].y!=1) Q1=0;
		if(a[i].y!=a[i].x+1&&a[i].x!=a[i].y+1) Q2=0;
		add(a[i].x,a[i].y,a[i].z); add(a[i].y,a[i].x,a[i].z);
	}
	if(Q3) sol3();
	else if(Q1) sol1();
	else sol2();
	fclose(stdin); fclose(stdout);
	return 0;
}
