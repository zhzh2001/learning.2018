#include<bits/stdc++.h>
#define FOR(i,x,y) for(int i=(x);i<=(y);i++)
#define DOR(i,x,y) for(int i=(x);i>=(y);i--)
#define LL long long 
#define NM 25005
#define M 105
#define oo 25002
using namespace std;
vector<int> vec;
int T,bo[NM],a[M],n;
int main(){  // long long   mod   neicun   多组数据要清空 
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		vec.clear();
		memset(bo,0,sizeof(bo));
		int ans=0;
		scanf("%d",&n);
		FOR(i,1,n){
			scanf("%d",&a[i]);
		}
		sort(a+1,a+1+n);
		FOR(i,1,n){
			if(bo[a[i]]){
				ans++; continue;
			}
			vec.push_back(a[i]);
			bo[a[i]]=1;
			int k=(int)vec.size()-1;
			FOR(j,0,k){
				FOR(z,1,oo){
					int tmp=a[i]*z+vec[j];
					if(tmp>oo) break;
					if(!bo[tmp]){
						bo[tmp]=1;
						vec.push_back(tmp);
					}
				}
			}
			
		}
		printf("%d\n",n-ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
