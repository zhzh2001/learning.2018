#include<bits/stdc++.h>
#define FOR(i,x,y) for(int i=(x);i<=(y);i++)
#define DOR(i,x,y) for(int i=(x);i>=(y);i--)
#define LL long long 
#define NM 100005
using namespace std;
int n,ans,LOG[NM];
struct node{
	int va,id;
}f[NM][20],a[NM];
void pre_ST(){
	LOG[0]=-1;
	FOR(i,1,n) LOG[i]=LOG[i>>1]+1;
	FOR(i,1,n) f[i][0]=a[i];
	int t=LOG[n];
	FOR(j,1,t) FOR(i,1,n-(1<<j)+1){
		if(f[i][j-1].va<f[i+(1<<(j-1))][j-1].va) f[i][j]=f[i][j-1];
		else f[i][j]=f[i+(1<<(j-1))][j-1];
	}
}
node ask(int l,int r){
	int k=LOG[r-l+1];
	if(f[l][k].va<f[r-(1<<k)+1][k].va) return f[l][k];
	return f[r-(1<<k)+1][k];
}
void dfs(int l,int r,int sum){
	if(l>r) return;
	node now=ask(l,r);
	ans+=now.va-sum;
	dfs(l,now.id-1,now.va);
	dfs(now.id+1,r,now.va);
}
int main(){  // long long   mod   neicun
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	FOR(i,1,n){
		scanf("%d",&a[i].va);
		a[i].id=i;
	}
	pre_ST();
	dfs(1,n,0);
	printf("%d\n",ans);
	fclose(stdin); fclose(stdout);
	return 0;
}
