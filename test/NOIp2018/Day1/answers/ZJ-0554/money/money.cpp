#include <bits/stdc++.h>
using namespace std;

#define rep(i, l, r) for(int i = l; i <= r; ++i)
#define per(i, l, r) for(int i = r; i >=l; --i)
#define repo(i, l, r) for(int i = l; i < r; ++i)
#define szof(x) (int)(x).size()
#define debug(x) cerr << #x << " : " << x << endl

bool MMmark1;

const int MAXN = 105, MAXV = 25005;

int T;
int n, A[MAXN];

struct Part100 {
	bool dp[MAXV];
	int mey[MAXV], tot;
	void clear() {
		memset(dp, 0, sizeof(dp));
		dp[0] = true;
		tot = 0;
	}
	void main() {
		clear();//!!!!!!!!!!
		int mx = 0;
		rep (i, 1, n) mx = max(mx, A[i]);
		rep (i, 1, n) {
			int x = A[i];
			rep (i, x, mx) dp[i] |= dp[i - x];
		}
		rep (i, 1, mx) if (dp[i]) mey[++tot] = i;
		
		// 下面的写法有没有问题? 
		//在 mey 递增下应该没问题 
		memset(dp, 0, sizeof(dp));
		dp[0] = true;
		int res = 0;
		rep (i, 1, tot) {
			int x = mey[i];
			if (dp[x]) continue;
			res++;;// res <= n
			rep (i, x, mx) dp[i] |= dp[i - x];
		}
		printf("%d\n", res);
	}
} part100;

bool MMmark2;

void Read() {
	scanf("%d", &n);
	rep (i, 1, n) scanf("%d", &A[i]);
}
void Solve() {
	part100.main();
}

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	
	scanf("%d", &T);
	rep (i, 1, T) {
		Read();
		Solve();
	}
	
//	cerr << ((&MMmark2 - &MMmark1) >> 20) << " MB" << endl;
	return 0;
}
