#include <bits/stdc++.h>
using namespace std;

#define rep(i, l, r) for(int i = l; i <= r; ++i)
#define per(i, l, r) for(int i = r; i >=l; --i)
#define repo(i, l, r) for(int i = l; i < r; ++i)
#define szof(x) (int)(x).size()
#define debug(x) cerr << #x << " : " << x << endl

bool MMmark1;

const int MAXN = 100005;

int n;
int A[MAXN];

void Read() {
	scanf("%d", &n);
	rep (i, 1, n) scanf("%d", &A[i]);
}

struct Part100 {
	int d[MAXN];
	void main() {
		rep (i, 1, n + 1) d[i] = A[i] - A[i - 1];
		long long res = 0;
		rep (i, 1, n  + 1) {
			if (d[i] < 0) res += -d[i];
		}
		cout << res << endl;
	}
} part100;

void Solve() {
	part100.main();
}

bool MMmark2;

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	
	Read();
	Solve();
	
//	cerr << ((&MMmark2 - &MMmark1) >> 20) << " MB" << endl;
	return 0;
}
