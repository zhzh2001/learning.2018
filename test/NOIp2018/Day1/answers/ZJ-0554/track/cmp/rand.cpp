#include <bits/stdc++.h>
using namespace std;

#define rep(i, l, r) for(int i = l; i <= r; ++i)
#define per(i, l, r) for(int i = r; i >=l; --i)
#define repo(i, l, r) for(int i = l; i < r; ++i)
#define szof(x) (int)(x).size()
#define debug(x) cerr << #x << " : " << x << endl

int rnd(int l, int r) {
	return rand() % (r - l + 1) + l;
}

int n, tot;
int A[100], B[100], l[100], e;
void dfs(int x) {
	int s = rnd(0, 2);
	if (x == 1) s = 2;
	for (int i = 1; i <= s; ++i) {
		int t = ++tot;
		if (tot > n) break;
		++e;
		A[e] = x, B[e] = t, l[e] = rnd(1, 5);
		dfs(t);
	}
}

int main() {
	srand(time(NULL));
	
	n = rnd(40000, 50000);
	int m = rnd(100, n - 1);
	
	cout << n << " " << m << endl;
	rep (i, 1, n - 1) {
		cout << 1 << " " << i + 1 << " " << rnd(1, 10000) << endl;
	}
	
	return 0;
}
