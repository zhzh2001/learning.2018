#include <bits/stdc++.h>
using namespace std;

#define rep(i, l, r) for(int i = l; i <= r; ++i)
#define per(i, l, r) for(int i = r; i >=l; --i)
#define repo(i, l, r) for(int i = l; i < r; ++i)
#define szof(x) (int)(x).size()
#define debug(x) cerr << #x << " : " << x << endl

bool MMmark1;

const int MAXN = 50005, MAXM = MAXN, MAXL = 10005;

int n, m;
int A[MAXN], B[MAXN], l[MAXN];

struct PartErCha {
	struct node {
		int t, v;
	};
	vector <node> g[MAXN];
	int rt;
	
	int giv[MAXN];
	int dfs (int x, int fa, int val) {
		int mx = 0, mi = 0;
		int cnt = 0;
		repo (i, 0, szof(g[x])) {
			int t = g[x][i].t, v = g[x][i].v;
			if (t == fa) continue;
			cnt += dfs(t, x, val);
			giv[t] += v;
			if (giv[t] > mi) mi = giv[t];
			if (giv[t] > mx) mi = mx, mx = giv[t];
		}
		
		if (mx >= val && mi >= val) {
			cnt += 2;
			giv[x] = 0;
		}
		else if (mx >= val && mi < val) {
			cnt ++;
			giv[x] = mi;
		}
		else if (mx < val && mi < val && mx + mi >= val) {
			cnt ++;
			giv[x] = 0;
		}
		else giv[x] = mx;
		return cnt;
	}
	bool chk(int val) {
		return dfs(rt, rt, val) >= m;
	}
	
	void main() {
		int resmx = 0;
		rep (i, 1, n - 1) {
			int x = A[i], y = B[i], v = l[i];
			g[x].push_back((node){y, v});
			g[y].push_back((node){x, v});
			resmx += v;
		}
		rep (i, 1, n) if (szof(g[i]) == 1) rt = i;
		
		resmx /= m;
		int l = 1, r = resmx, res = 0;
		while (l <= r) {
			int mid = (l + r) >> 1;
			if (chk(mid)) res = mid, l = mid + 1;
			else r = mid - 1;
		}
		printf("%d\n", res);
	}
} partercha;

struct PartAiEq1 {
	bool chk(int val) {
		int cnt = 0, tot = n - 1;
		while (l[tot] >= val) cnt ++, tot --;
		int lp = 1, rp = tot;
		while (lp < rp) {
			while (lp < rp && l[lp] + l[rp] < val) lp ++;
			if (lp < rp) cnt++, lp++, rp--;
		}
		return cnt >= m;
	}
	void main() {
		sort(l + 1, l + n);
		int resmx = 0;
		rep (i, 1, n - 1) resmx += l[i];
		resmx /= m;
		
		int l = 0, r = resmx, res = 0;
		while (l <= r) {
			int mid = (l + r) >> 1;
			if (chk(mid)) res = mid, l = mid + 1;
			else r = mid - 1;
		}
		
		printf("%d\n", res);
	}
} partaieq1;

/*
6 4
1 2 1
1 3 2
1 4 3
1 5 4
1 6 5

*/

bool MMmark2;

void Read() {
	scanf("%d %d", &n, &m);
	rep (i, 1, n - 1) {
		scanf("%d %d %d", &A[i], &B[i], &l[i]);
	}
}
bool judgeErCha() {
	static int deg[MAXN];
	memset(deg, 0, sizeof(deg));
	rep (i, 1, n - 1) {
		deg[A[i]]++, deg[B[i]]++;
	}
	rep (i, 1, n) if (deg[i] > 3) return false;
	return true;
}
bool judgeAieq1() {
	rep (i, 1, n - 1) if (A[i] != 1) return false;
	return true;
}
void Solve() {
	if (0);
//	else if (judgeErCha()) {
//		partercha.main();
//	}
	else if (judgeAieq1()) {
		partaieq1.main();
	}
//	else {
//		part100.main();
//	}
}

int main() {
//	freopen("track.in", "r", stdin);
//	freopen("track.out", "w", stdout);
	
	Read();
	Solve();
	
	cerr << ((&MMmark2 - &MMmark1) >> 20) << " MB" << endl;
	return 0;
}
