#include <bits/stdc++.h>
using namespace std;
const int maxn=100000+10;
int n,ans;

inline int read(){
	register int x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return (f==1)?x:-x;
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int now=0,x;
	n=read();
	for(int i=1;i<=n;i++){
		x=read();
		if(now>x) ans+=now-x;
		now=x;
	}
	printf("%d\n",ans+now);
	return 0;
}
