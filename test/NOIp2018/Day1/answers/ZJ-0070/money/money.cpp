#include <bits/stdc++.h>
using namespace std;
const int maxn=50000+10;
int n,a[maxn],dp[maxn],vis[maxn],sum,ans;

inline int read(){
	register int x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return (f==1)?x:-x;
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read(),i,j;
	while(T--){
		for(i=1;i<maxn;i++)
			dp[i]=vis[i]=0;
		dp[0]=1;ans=sum=0;
		n=read();
		for(i=1;i<=n;i++){
			a[i]=read();
			vis[a[i]]=1;
		}
		sort(a+1,a+n+1);
		for(i=1;i<=n;i++){
			if(!dp[a[i]]){
				for(j=a[i];j<maxn;j++)
					if(dp[j-a[i]]==1){
						if(vis[j]&&dp[j]==0) sum++;
						dp[j]=1;
					}
				ans++;
				if(sum==n) break;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
