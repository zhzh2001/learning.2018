#include <bits/stdc++.h>
#define res register int
using namespace std;
const int maxn=50000+10;
int n,m,head[maxn],tot,ans,sum;

struct node{
	int to,next,val;
}e[maxn<<1];

vector<int> s[maxn];
vector<int>::iterator it;

inline int read(){
	res x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return (f==1)?x:-x;
}
inline int max(int a,int b){return a>b?a:b;}

inline void add(res x,res y,res w){
	e[++tot].to=y;
	e[tot].val=w;
	e[tot].next=head[x];
	head[x]=tot;
}

int dfs(res x,res fa,res k){
	s[x].clear();
	res val;
	for(res i=head[x],y;i;i=e[i].next){
		y=e[i].to;
		if(y==fa) continue;
		val=dfs(y,x,k)+e[i].val;
		if(val>=k) ans++;
		else {
			s[x].insert(lower_bound(s[x].begin(),s[x].end(),val),val);
		}
	}
	res Max=0;
	while(!s[x].empty()){
		if(s[x].size()==1){
			return max(Max,*s[x].begin());
		}
		it=lower_bound(s[x].begin(),s[x].end(),k-*s[x].begin());
		if(it==s[x].begin()) it++;
		if(it==s[x].end()){
			Max=max(Max,*s[x].begin());
			s[x].erase(s[x].begin());
		}
		else {
			ans++;
			s[x].erase(it);s[x].erase(s[x].begin());
		}
	}
	return Max;
}

int check(res k){
	ans=0;
	dfs(1,0,k);
	if(ans>=m) return 1;
	return 0;
}

namespace subtask1{
	int a[maxn];
	void dfs(int x,int fa){
		for(int i=head[x],y;i;i=e[i].next){
			y=e[i].to;
			if(y==fa) continue;
			dfs(y,x);
			a[x]=e[i].val;
		}
	}
	int check(int k){
		int t=0,now=0;
		for(int i=1;i<n;i++){
			if(now+a[i]>=k){
				now=0;
				t++;
			}
			else now+=a[i];
		}
		return t>=m;
	}
	void solve(){
		dfs(1,0);
		int l=1,r=sum,mid;
		while(l<r){
			mid=l+r+1>>1;
			if(check(mid)) l=mid;
			else r=mid-1;
		}
		printf("%d\n",l);
		return ;
	}
}

namespace subtask2{
	int dfs(int x,int fa){
		int sum1=0,sum2=0;
		for(int i=head[x],y;i;i=e[i].next){
			y=e[i].to;
			if(y==fa) continue;
			sum2=max(sum2,dfs(y,x)+e[i].val);
			if(sum2>sum1) swap(sum1,sum2); 
		}
		ans=max(ans,sum1+sum2);
		return sum1;
	}
	void solve(){
		dfs(1,0);
		printf("%d\n",ans);
		return ;
	}
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	res x,y,w,flag=1;
	for(res i=1;i<n;i++){
		x=read(),y=read(),w=read();
		add(x,y,w);add(y,x,w);
		sum+=w;
		if(abs(x-y)!=1) flag=0;
	}
	if(flag) subtask1::solve();
	else if(m==1) subtask2::solve();
	else {
		res l=1,r=sum,mid;
		while(l<r){
			mid=l+r+1>>1;
			if(check(mid)) l=mid;
			else r=mid-1;
		}
		printf("%d\n",l);
	}
	return 0;
}
