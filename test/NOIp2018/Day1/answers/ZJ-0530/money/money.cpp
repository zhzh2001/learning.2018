#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn_=105,maxa_=25005;
bool v[maxa_];
int n,m,a[maxn_];
int rad()
{
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (int T=rad();T>0;--T)
	{
		n=rad();
		for (int i=1;i<=n;++i) a[i]=rad();
		sort(a+1,a+n+1);
		int mx_=a[n];
		for (int i=1;i<=mx_;++i) v[i]=0;
		m=0;
		for (int i=1;i<=n;++i)
		{
			if (v[a[i]]) continue;
			++m;
			int now_=a[i];
			v[now_]=1;
			for (int j=now_;j<=mx_;++j)
			  if (v[j-now_]) v[j]=1;
		}
		printf("%d\n",m);
	}
	return 0;
}
