#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn_=50005;
int n,m,ans_,now_,tot_,lnk[maxn_],son_[maxn_*2],nxt_[maxn_*2],w[maxn_*2],le[maxn_],ri[maxn_];
long long f[maxn_],a[maxn_],g[maxn_];
int rad()
{
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
void add_(int x,int y,int z)
{
	son_[++tot_]=y;nxt_[tot_]=lnk[x];lnk[x]=tot_;w[tot_]=z;
	son_[++tot_]=x;nxt_[tot_]=lnk[y];lnk[y]=tot_;w[tot_]=z;
}
void dfs(int x,int fa)
{
	f[x]=g[x]=0;
	int L=1,R=0;
	for (int i=lnk[x];i!=0;i=nxt_[i]) if (son_[i]!=fa) dfs(son_[i],x);
	for (int i=lnk[x];i!=0;i=nxt_[i]) if (son_[i]!=fa)
	{
		g[x]+=g[son_[i]];
		++R;
		le[R]=R-1;ri[R]=R+1;
		a[R]=f[son_[i]]+w[i];
	}
	sort(a+1,a+R+1);
	int lr=R;
	while (L<=R&&a[R]>=now_) {++g[x];a[R]=-1;--R;}
	int j=L+1;
	while (L<R)
	{
		while (j>R) j=le[j];
		while (j<=L) j=ri[j];
		if (j>R) break;
		while (a[L]+a[j]<now_&&ri[j]<=R) j=ri[j];
		while (a[L]+a[le[j]]>=now_&&le[j]>L) j=le[j];
		if (j>L&&j<=R&&a[L]+a[j]>=now_)
		{
			++g[x];
			a[j]=a[L]=-1;
			le[ri[j]]=le[j];
			ri[le[j]]=ri[j];
			j=le[j];
		}
		++L;
	}
	for (int i=lr;i>=1;--i)
	  if (a[i]>0){f[x]=a[i];break;}
}
bool c(int x)
{
	now_=x;
	dfs(1,0);
	return (g[1]>=(long long)(m));
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=rad();m=rad();tot_=0;
	for (int i=1,x,y,z;i<n;++i)
	{
		x=rad();y=rad();z=rad();
		add_(x,y,z);
	}
	int L=1,R=500000000,mid_;
	while (L<=R)
	{
		mid_=(L+R)/2;
		if (c(mid_)) {L=mid_+1;ans_=mid_;}
		else R=mid_-1;
	}
	printf("%d\n",ans_);
	return 0;
}
