#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn_=100005;
int n,d[maxn_],ans;
int rad()
{
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=rad();ans=0;
	for (int i=1;i<=n;++i) d[i]=rad();
	d[n+1]=0;
	for (int i=1,lst=0;i<=n+1;++i)
	{
		if (lst>d[i]) ans+=lst-d[i];
		lst=d[i];
	}
	printf("%d",ans);
	return 0;
}
