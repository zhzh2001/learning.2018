#include<cstdio>
#include<iostream>
#include<cstring>
#include<queue>
#include<vector>
#include<algorithm>
const int N = 50001;
using namespace std;
vector<int> g[N];
vector<int> d[N];
int n, m, dis[N], vis[N], check_sp1 = 1, check_sp2 = 1, val[N];
pair<int, int> l;
void dijkstra(int s)
{
	memset(dis, 0x3f, sizeof(dis));
	memset(vis, 0, sizeof(vis));
	priority_queue<pair<int, int>, vector<pair<int, int> >, less<pair<int, int> > > q;
	dis[s] = 0;
	q.push(make_pair(0, s));
	while(!q.empty())
	{
		pair<int, int> tmp = q.top();
		q.pop();
		if(tmp.first != dis[tmp.second] || vis[tmp.second])
			continue;
		vis[tmp.second] = 1;
		for(int i = 0; i < g[tmp.second].size(); i ++)
		if(dis[g[tmp.second][i]] > dis[tmp.second] + d[tmp.second][i])
		{	
			dis[g[tmp.second][i]] = dis[tmp.second] + d[tmp.second][i];
			
			q.push(make_pair(dis[g[tmp.second][i]], g[tmp.second][i]));
		}
	}
	
}
bool check(int x, int sum)
{
	int t = 0;
	for(int i = 1; i < n; i ++)
	{
		t = 0;
		while(t < x && i < n)	t += val[i], i++;
		if(t >= x)
		sum--;
		i --;
	}
	if(sum <= 0)	return 1;
	else	 return 0;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d", &n, &m);
	for(int i = 1; i < n; i ++)
	{
		int tmp, tmmp;
		scanf("%d%d%d", &tmp, &tmmp, &val[i]);
		if(tmp != 1)	check_sp1 = 0;
		if(tmmp != tmp + 1)	check_sp2 = 0;
		g[tmp].push_back(tmmp);
		g[tmmp].push_back(tmp);
		d[tmp].push_back(val[i]);
		d[tmmp].push_back(val[i]);
	}
	
	if(m == 1)
	{
		dijkstra(1);
		int x, maxx = 0;
		for(int i = 1; i <= n; i ++)
		{
			if(dis[i] > maxx)	maxx = dis[i], x = i;
		}
		dijkstra(x);
		maxx = 0;
		for(int i = 1; i <= n; i ++)
		{
			if(dis[i] > maxx)	maxx = dis[i];
		}
		printf("%d\n", maxx);
	}
	else if(check_sp1)
	{
		sort(val+1, val+n);
		int ans = 0;
		int r = 1e9+1, l = 0;
		while(l < r)
		{
			int mid = (l + r + 1) >> 1;
			if(check(mid, m))	l = mid;
			else	r = mid - 1;
		}
		printf("%d\n", l);
	}
	else if(check_sp2)
	{
		int r = 1e9+1, l = 0;
		while(l < r)
		{
			int mid = (l + r + 1) >> 1;
			if(check(mid, m))	l = mid;
			else	r = mid - 1;
		}
		printf("%d\n", l);
	}
	else if(m == n - 1)
	{
		int ans = 0x3f3f3f3f;
		for(int i = 1; i <= n; i ++)
		ans = min(ans, val[i]);
		printf("%d\n", ans);
	}
	else
	{
		printf("10000\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
