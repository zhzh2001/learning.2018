#include <bits/stdc++.h>
#define ll long long
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define gc getchar()
#define mp make_pair
#define pa pair<int,int>
#define N 300005
#define fi first
#define se second
using namespace std;
inline int read(){
	char c;int x=0,f=1;
	for (c=gc;c<'0'||c>'9';c=gc) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc) x=x*10+c-'0';
	return (f==1)?x:-x;
}
int g[N],n,r,ans,d[N];
pa dui[N];
struct xx{
	int l,r,min;
}t[N*4];
int g_m(int a,int b){
	if (d[a]<d[b]) return a;
	else return b;
}
void upd(int x){
	t[x].min=g_m(t[x*2].min,t[x*2+1].min);
}
void build(int x,int l,int r){
	t[x].l=l;t[x].r=r;
	if (l==r){
		t[x].min=l;return;
	}
	int mid=(l+r)>>1;
	build(x*2,l,mid);build(x*2+1,mid+1,r);
	upd(x);
}
int query(int x,int l,int r){
	if (t[x].l==l&&t[x].r==r) return t[x].min;
	int mid=(t[x].l+t[x].r)>>1;
	if (r<=mid) return query(x*2,l,r);
	else if (l>mid) return query(x*2+1,l,r);
	else return g_m(query(x*2,l,mid),query(x*2+1,mid+1,r));
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	F(i,1,n) d[i]=read();
	build(1,1,n);
	dui[++r]=mp(1,n);
	F(l,1,r){
		pa t=dui[l];
		if (t.fi>t.se) continue;
		if (t.fi==t.se){
			ans+=d[t.fi]-g[l];continue;
		}
		int p=query(1,t.fi,t.se);
		ans+=d[p]-g[l];
		g[l]=d[p];
		dui[++r]=mp(t.fi,p-1);g[r]=g[l];
		dui[++r]=mp(p+1,t.se);g[r]=g[l];
	}
	printf("%d\n",ans);
	return 0;
}
