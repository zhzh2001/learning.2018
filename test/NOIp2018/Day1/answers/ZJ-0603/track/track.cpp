#include <bits/stdc++.h>
#define ll long long
#define re register
#define F(i,a,b) for (re int i=(a);i<=(b);i++)
#define D(i,a,b) for (re int i=(a);i>=(b);i--)
#define go(i,x) for (re int i=head[x];i;i=Next[i])
#define gc getchar()
#define mp make_pair
#define pa pair<int,int>
#define fi first
#define se second
#define pb push_back
#define be begin()
#define en end()
#define N 200005
using namespace std;
inline int read(){
	char c;int x=0,f=1;
	for (c=gc;c<'0'||c>'9';c=gc) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc) x=x*10+c-'0';
	return (f==1)?x:-x;
}
int fa[N],g[N],dis[N],yk,a[N],num,n,m,ti,x,y,v,l,r,z[N],vis[N];
int Next[N*2],head[N],to[N*2],nedge,lon[N*2];
#define V to[i]
inline void add(int a,int b,int c){
	Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;lon[nedge]=c;
}
inline bool cmp(int a,int b){return (a>b);}
inline void dfs(int x){
	if (num>=m) return;
	dis[x]=0;
	int tot=0;
	go(i,x){
		if (V==fa[x]) continue;
		dfs(V);a[g[x]+tot]=dis[V]+lon[i];tot++;
	}
	if (tot==0) return;
	sort(a+g[x],a+g[x]+tot,cmp);
	F(i,g[x],g[x]+tot-1) vis[i]=0;
	int cnt=0,t=g[x];
	D(i,g[x]+tot-1,g[x]){
		if (a[i]>=yk){num+=i-g[x]+1;break;}
		for (;t<g[x]+tot-1&&a[i]+a[t+1]>=yk;t++){
			z[++cnt]=t+1;
		}
		while (vis[z[cnt]]&&cnt) cnt--;
		if (cnt){
			if (z[cnt]>i){
				vis[z[cnt]]=1;
				cnt--;num++;vis[i]=1;
			}
		}
	}
	F(i,g[x],g[x]+tot-1){
		dis[x]=(vis[i]==0&&a[i]<yk)?max(dis[x],a[i]):dis[x];
	}
	/*multiset <int> q;
	multiset <int>::iterator it;
	D(i,g[x]+tot-1,g[x]){
		if (num>=m) return;
		if (a[i]>=yk){
			num++;continue;
		}
		it=q.lower_bound(yk-a[i]);
		if (it==q.en) q.insert(a[i]);
		else{
			q.erase(it);
			num++;
		}
	} 
	if (q.empty()) dis[x]=0;
	else dis[x]=*(q.rbegin());*/
}
int check(int x){
	num=0;yk=x;dfs(1);
	return (num>=m);
}
void pre(int x){
	int tot=0;
	go(i,x){
		if (V==fa[x]) continue;
		fa[V]=x;pre(V);tot++;
	}
	g[x]=ti+1;
	ti+=tot;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	F(i,1,n-1){
		x=read();y=read();v=read();
		add(x,y,v);add(y,x,v);r+=v;
	}
	pre(1);
	l=1;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (check(mid)) l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
