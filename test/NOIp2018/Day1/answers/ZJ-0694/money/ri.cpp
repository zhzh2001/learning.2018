#include<bits/stdc++.h>
const int N=105,U=25005,inf=1<<30;
int d[U],dc[U];
int T,n,a[N],mi,b[N],c[N],i,j,ans;
int he[N],ta[N],q[N][U];
int main(){
//	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		mi=inf;
		memset(ta+1,0,n<<2);
		for(i=1;i<=n;++i)he[i]=1;
		for(i=1;i<=n;++i)scanf("%d",a+i),mi=std::min(mi,a[i]);
		for(i=0;i<mi;++i)d[i]=inf,dc[i]=0;d[0]=0;dc[0]=1;
		for(i=1;i<=n;++i)b[i]=a[i]/mi,c[i]=a[i]-b[i]*mi;
		for(i=1;i<=n;++i)if(a[i]==mi)q[i][++ta[i]]=0;
		for(;;){
			int u=0;
			for(i=1;i<=n;++i)if(he[i]<=ta[i] && (!u || d[q[i][he[i]]]<d[q[u][he[u]]]))u=i;
			if(!u)break;
			u=q[u][he[u]++];
			for(i=1;i<=n;++i){
				int x=u+c[i],dd=d[u]+b[i];
				if(x>=mi)x-=mi,++dd;
				if(dd<d[x])d[x]=dd,q[i][++ta[i]]=x,dc[x]=1;
					else if(dd==d[x])dc[x]=2;
			}
		}
		ans=1;
		for(i=1;i<=n;++i)if(d[c[i]]==b[i] && dc[c[i]]==1)++ans;
		printf("%d\n",ans);
	}
	return 0;
}
