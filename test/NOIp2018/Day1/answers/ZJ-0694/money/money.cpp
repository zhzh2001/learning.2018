#include<bits/stdc++.h>
const int N=105,U=25005;
int T,n,a[N],f[U],i;
int main(){
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(i=1;i<=n;++i)scanf("%d",a+i);
		f[0]=1;
		for(i=1;i<U;++i){
			f[i]=0;
			for(int j=1;j<=n;++j)if(i>=a[j])f[i]+=f[i-a[j]];
			if(f[i]>2)f[i]=2;
		}
		int ans=0;
		for(i=1;i<=n;++i)if(f[a[i]]==1)++ans;
		printf("%d\n",ans);
	}
	return 0;
}
