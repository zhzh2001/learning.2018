#include<bits/stdc++.h>
using namespace std;
const int N=50005;
int n,m,i,a,b,l,r,mid;
vector<pair<int,int> >e[N];
int mx[N],dfn[N],xb;
void dfs(int x,int fa){
	int i;dfn[++xb]=x;
	if(fa){for(i=0;e[x][i].first!=fa;++i);e[x].erase(e[x].begin()+i);}
	for(i=0;i<e[x].size();++i)dfs(e[x][i].first,x);
}
int main(){
//	freopen("track2.in","r",stdin);
	ios::sync_with_stdio(0);cin.tie(0);
	cin>>n>>m;
	for(i=1;i<n;++i){
		cin>>a>>b>>l;
		e[a].push_back(make_pair(b,l));
		e[b].push_back(make_pair(a,l));
	}
	dfs(1,0);
	for(l=0,r=600000000;l<r;){
		mid=(l+r+1)>>1;
		if(mid<=15){
++i,--i;
}
		int sum=0;
		for(i=n;i;--i){
			int x=dfn[i],j,k,z=e[x].size();
			static multiset<int>s;s.clear();
			multiset<int>::iterator it1,it2;
			for(j=0;j<z;++j)s.insert(mx[e[x][j].first]+e[x][j].second);
			for(;!s.empty() && *s.rbegin()>=mid;)++sum,s.erase(--s.end());
			for(it1=s.begin();it1!=s.end();){
				it2=s.lower_bound(mid-*it1);
				if(it2!=s.end() && it2==it1)++it2;
				if(it2!=s.end())s.erase(it2),s.erase(it1++),++sum;else ++it1;
			}
			mx[x]=s.empty()?0:*s.rbegin();
		}
		if(sum>=m)l=mid;else r=mid-1;
	}
	cout<<l<<'\n';
	return 0;
}
