#include<bits/stdc++.h>
using namespace std;
inline int max(int a,int b){return a<b?b:a;}
inline void upa(int&a,const int&b){a<b?a=b:0;}
const int N=1005;
int f[N][N];
struct edge{
	int to,next,w;
}e[N<<1];
int n,m,i,j,h[N],a,b,l,r,mid,xb;
int sz[N];
void dfs(int x,int fa){
	memset(f[x],240,sizeof f[x]);sz[x]=1;
	f[x][0]=0;
	for(int i=h[x];i;i=e[i].next)if(e[i].to!=fa){
		dfs(e[i].to,x);
		int j,k;
		static int g[N];memcpy(g,f[x],sizeof g);
		for(j=0;j<=sz[x];++j)
			for(k=0;k<=sz[e[i].to];++k)if(g[j]>=0 && f[e[i].to][k]>=0){
				upa(f[x][j+k],max(g[j],f[e[i].to][k]+e[i].w));
				if(g[j]+f[e[i].to][k]+e[i].w>=mid)
					upa(f[x][j+k+1],0);
			}
		sz[x]+=sz[e[i].to];
	}
}
int main(){
//	freopen("track1.in","r",stdin);
	scanf("%d%d",&n,&m);
	for(i=1;i<n;++i){
		scanf("%d%d%d",&a,&b,&l);
		e[++xb]=(edge){b,h[a],l};h[a]=xb;
		e[++xb]=(edge){a,h[b],l};h[b]=xb;
	}
	for(l=0,r=600000000;l<r;){
		mid=(l+r+1)>>1;
		dfs(1,0);
		for(i=n;i>=m;--i)if(f[1][i]>=0)break;
		if(i>=m)l=mid;else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}

