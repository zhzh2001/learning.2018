#include<bits/stdc++.h>
using namespace std;
inline int max(int a,int b){return a<b?b:a;}
inline void upa(int&a,const int&b){a<b?a=b:0;}
const int N=1005;
struct edge{
	int to,next,w;
}e[N<<1];
int n,m,i,j,h[N],a,b,l,r,mid,xb;
int sz[N];
int s[N],xxb;
void dfs(int x,int fa,int d,int ss){
	if(d>=mid)s[++xxb]=ss;
	for(int i=h[x];i;i=e[i].next)if(e[i].to!=fa)dfs(e[i].to,x,d+e[i].w,ss|(1<<(i/2-1)));
}
int f[(1<<25)+5];
int main(){
//	freopen("track1.in","r",stdin);
	scanf("%d%d",&n,&m);
	xb=1;
	for(i=1;i<n;++i){
		scanf("%d%d%d",&a,&b,&l);
		e[++xb]=(edge){b,h[a],l};h[a]=xb;
		e[++xb]=(edge){a,h[b],l};h[b]=xb;
	}
	for(l=0,r=600000000;l<r;){
		mid=(l+r+1)>>1;
		xxb=0;
		for(i=1;i<=n;++i)dfs(i,0,0,0);
		int S=(1<<(n-1));f[0]=0;
		for(i=1;i<S;++i){
			f[i]=0;
			for(int j=1;j<=xxb;++j)if((i&s[j])==s[j])upa(f[i],f[i^s[j]]+1);
			if(f[i]>=m)break;
		}
		if(i<S)l=mid;else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}

