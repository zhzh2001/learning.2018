#include<bits/stdc++.h>
using namespace std;
const int N=50005;
int n,m,i,a,b,l,r,mid;
vector<pair<int,int> >e[N];
int mx[N],dfn[N],xb;
void dfs(int x,int fa){
	int i;dfn[++xb]=x;
	if(fa){for(i=0;e[x][i].first!=fa;++i);e[x].erase(e[x].begin()+i);}
	for(i=0;i<e[x].size();++i)dfs(e[x][i].first,x);
}
int main(){
	freopen("track1.in","r",stdin);
	ios::sync_with_stdio(0);cin.tie(0);
	cin>>n>>m;
	for(i=1;i<n;++i){
		cin>>a>>b>>l;
		e[a].push_back(make_pair(b,l));
		e[b].push_back(make_pair(a,l));
	}
	dfs(1,0);
	for(l=0,r=600000000;l<r;){
		mid=(l+r+1)>>1;
		int sum=0;
		for(i=n;i;--i){
			int x=dfn[i],j,k,z=e[x].size();
			static int d[N];
			static bool b[N];
			for(j=0;j<z;++j)d[j]=mx[e[x][j].first]+e[x][j].second;
			memset(b,0,z);sort(d,d+z);
			for(j=0,k=z-1;j<k;++j)if(d[j]+d[k]>=mid){
				for(;k>j && d[j]+d[k]>=mid;--k);++k;
				if(k>j && && d[j]+d[k]>=mid && !b[k])b[j]=b[k]=1,++sum;
			}
			mx[x]=0;
			for(j=z-1;j>=0 && b[j];--j);if(j>=0)mx[x]=d[j];
		}
		if(sum>=m)l=mid;else r=mid-1;
	}
	cout<<l<<'\n';
	return 0;
}
