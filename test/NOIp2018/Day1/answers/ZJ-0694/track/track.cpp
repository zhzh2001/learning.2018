#include<bits/stdc++.h>
using namespace std;
const int N=50005;
int n,m,i,a,b,l,r,mid;
vector<pair<int,int> >e[N];
int mx[N],dfn[N],xb;
int md[N];
void dfs(int x,int fa){
	int i;dfn[++xb]=x;
	if(fa){for(i=0;e[x][i].first!=fa;++i);e[x].erase(e[x].begin()+i);}
	int md1=0,md2=0;
	for(i=0;i<e[x].size();++i){
		dfs(e[x][i].first,x);
		int d=md[e[x][i].first]+e[x][i].second;
		if(d>=md1)md2=md1,md1=d;
			else if(d>md2)md2=d;
	}
	md[x]=md1;if(md1+md2>r)r=md1+md2;
}
int main(){
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	ios::sync_with_stdio(0);cin.tie(0);
	cin>>n>>m;
	for(i=1;i<n;++i){
		cin>>a>>b>>l;
		e[a].push_back(make_pair(b,l));
		e[b].push_back(make_pair(a,l));
	}
	dfs(1,0);
	for(l=0;l<r;){
		mid=(l+r+1)>>1;
		int sum=0;
		for(i=n;i;--i){
			int x=dfn[i],j,k,z=e[x].size();
			static int L[N],R[N],d[N];
			for(j=0;j<z;++j)d[j+1]=mx[e[x][j].first]+e[x][j].second;
			sort(d+1,d+z+1);
			for(;z && d[z]>=mid;--z,++sum);
			for(j=0;j<=z;++j)R[j]=j+1;for(j=1;j<=z+1;++j)L[j]=j-1;
			for(j=1,k=z+1;j<=z;){
				for(;L[k] && d[L[k]]+d[j]>=mid;k=L[k]);
				if(k==j)k=R[k];
				if(k<=z)++sum,L[R[k]]=L[k],R[L[k]]=R[k],L[R[j]]=L[j],R[L[j]]=R[j],k=R[k];j=R[j];
			}
			mx[x]=L[z+1]?d[L[z+1]]:0;
		}
		if(sum>=m)l=mid;else r=mid-1;
	}
	cout<<l<<'\n';
	return 0;
}
