#include<cstdio>
#include<algorithm>
using namespace std;
const int p=1000000;
int T,n,m,ans,a[110],b[1000010],c[25010];
void bfs(int k)
  {
   int l=0;int r=n;
   while (l!=r)
     {
      l=l%p+1;
      for (int i=1;i<=n;i++)
        {
         int t=b[l]+a[i];
         if (t>m) break;
         if (c[t]!=k)
		   {
		    c[t]=k;r=r%p+1;b[r]=t;
	       }
		}
	 }
  }
int main()
  {
   freopen("money.in","r",stdin);
   freopen("money.out","w",stdout);
   
   scanf("%d",&T);
   for (int k=1;k<=T;k++)
     {
      scanf("%d",&n);
      m=-100;
      for (int i=1;i<=n;i++)
        {
         scanf("%d",&a[i]);
         b[i]=a[i];
		 if (a[i]>m) m=a[i];
		}
	  sort(a+1,a+1+n);
	  bfs(k);
	  ans=0;
	  for (int i=1;i<=n;i++)
	    if (c[a[i]]!=k) ans=ans+1;
	  printf("%d\n",ans);
	 } 
   return 0;
  }
