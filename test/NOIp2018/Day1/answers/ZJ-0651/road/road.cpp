#include<cstdio>
#include<cmath>
using namespace std;
int n,a[100010],b[100010][21],ans;
int min(int l,int r)
  {
   if (a[l]<a[r]) return l;else return r;
  }
void f(int l,int r,int k)
  {
   if (l>r) return;
   int sum=log2(r-l+1);
   int t=min(b[l][sum],b[r-(1<<sum)+1][sum]);
   ans=ans+a[t]-k;
   f(l,t-1,a[t]);f(t+1,r,a[t]);
  }
int main()
  {
   freopen("road.in","r",stdin);
   freopen("road.out","w",stdout);
   
   scanf("%d",&n);
   for (int i=1;i<=n;i++)
     {
      scanf("%d",&a[i]);
      b[i][0]=i;
	 }
   for (int i=1;i<=20;i++)
     for (int j=1;j+(1<<i)-1<=n;j++)
        b[j][i]=min(b[j][i-1],b[j+(1<<(i-1))][i-1]);
   ans=0;
   f(1,n,0);
   printf("%d",ans);
   return 0;
  }
