#include <cstdio>
#include <cmath>
#include <cstring>
#include <queue>
#include <algorithm>
using namespace std;

const int MAXN = 50010, MAXP = 16, INF = 1e9;
int n, m, sum;
int last[MAXN], in[MAXN];
struct Edge {
	int link, value, to;
} edge[MAXN << 1];

inline void read(int &x) {
	char ch = getchar(); x = 0;
	for (; ch < 48 || ch > 57; ch = getchar());
	for (; ch >= 48 && ch <= 57; ch = getchar()) x = (x << 3) + (x << 1) + (ch ^ 48);
	return;
}

inline void addedge(int u, int v, int w) {
	edge[++sum] = (Edge) { v, w, last[u] }, last[u] = sum;
	edge[++sum] = (Edge) { u, w, last[v] }, last[v] = sum;
}

namespace dia {
	int Max, num;
	int dis[MAXN];
	
	queue <int> Q;
	inline void bfs(int S) {
		for (register int i = 0; i <= n; ++i) dis[i] = INF;
		dis[S] = 0;
		while (!Q.empty()) Q.pop(); Q.push(S);
		while (!Q.empty()) {
			int x = Q.front(); Q.pop();
			for (int i = last[x]; i; i = edge[i].to) {
				int y = edge[i].link, z = edge[i].value;
				if (dis[y] == INF) {
					dis[y] = dis[x] + z; Q.push(y);
					if (dis[y] > Max) Max = dis[y], num = y;
				}
			}
		}
		return;
	}
	
	inline void doit() {
		Max = 0, bfs(1);
		Max = 0, bfs(num);
		printf("%d\n", Max);
		return;
	}
};

namespace simple {
	int Min = INF;
	
	inline void doit() {
		for (register int i = 1; i <= sum; ++i) Min = min(Min, edge[i].value);
		printf("%d\n", Min);
		return;
	}
};

namespace flower {
	int a[MAXN];
	
	inline int cmp(int a, int b) { return a > b; }
	
	inline int check(int std) {
		int p = 1, q = n - 1, cnt = 0;
		while (p <= q) {
			if (a[p] >= std) ++p, ++cnt;
			else {
				while (p <= q && a[p] + a[q] < std) --q;
				if (a[p] + a[q] >= std) ++p, --q, ++cnt;
			}
			if (cnt == m) return 1;
		}
		return 0;
	}
	
	inline void doit() {
		for (register int i = 1; i < n; ++i) a[edge[(i << 1) - 1].link - 1] = edge[i << 1].value;
		sort(a + 1, a + n + 1, cmp);
		int L = 0, R = INF;
		while (L <= R) {
			int mid = L + R >> 1;
			if (check(mid)) L = mid + 1;
			else R = mid - 1;
		}
		printf("%d\n", L - 1);
		return;
	}
};

namespace link {
	int a[MAXN];
	
	inline int check(int std) {
		int cnt = 0, add = 0;
		for (register int i = 1; i < n; ++i) {
			add += a[i];
			if (add >= std) add = 0, ++cnt;
			if (cnt == m) return 1;
		}
		return 0;
	}
	
	inline void doit() {
		for (register int i = 1; i < n; ++i) a[edge[i << 1].link] = edge[i << 1].value;
		int L = 0, R = INF;
		while (L <= R) {
			int mid = L + R >> 1;
			if (check(mid)) L = mid + 1;
			else R = mid - 1;
		}
		printf("%d\n", L - 1);
		return;
	}
};

int main() {
	freopen("track.in", "r", stdin); freopen("track.out", "w", stdout);
	read(n), read(m);
	int f1 = 1, f2 = 1, f3 = 1;
	for (register int i = 1; i < n; ++i) {
		int u, v, w; read(u), read(v), read(w);
		addedge(u, v, w);
		if (u != 1) f1 = 0;
		if (u + 1 != v) f2 = 0;
		++in[u], ++in[v];
		if (in[u] > 3 || in[v] > 3) f3 = 0;
	}
	if (m == 1) dia::doit();
	else if (m == n - 1) simple::doit();
	else if (f2) link::doit();
	else if (f1) flower::doit();
	fclose(stdin); fclose(stdout);
	return 0;
}
