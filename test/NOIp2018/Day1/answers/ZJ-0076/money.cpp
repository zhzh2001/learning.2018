#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;

typedef long long ll;
const int MAXN = 110, MAXV = 1e7;
const ll INF = 1e9;
ll n, Min;
int dp[MAXV], DP[MAXV];
ll a[MAXN];

ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}

inline void read(ll &x) {
	char ch = getchar(); x = 0;
	for (; ch < 48 || ch > 57; ch = getchar());
	for (; ch >= 48 && ch <= 57; ch = getchar()) x = (x << 3) + (x << 1) + (ch ^ 48);
	return;
}

inline void init() {
	for (register int i = 0; i <= Min; ++i) dp[i] = DP[i] = 0;
	dp[0] = DP[0] = 1;
	return;
}

int main() {
	freopen("money.in", "r", stdin); freopen("money.out", "w", stdout);
	ll T; read(T);
	while (T--) {
		read(n);
		for (register int i = 1; i <= n; ++i) read(a[i]);
		if (n == 1) { puts("1"); continue; }
		sort(a + 1, a + n + 1);
		if (n == 2) { if (!(a[2] % a[1])) puts("1"); else puts("2"); continue; }
		if (a[1] == 1) { puts("1"); continue; }
		if (a[1] == 2) {
			int flag = 1;
			for (register int i = 2; i <= n; ++i)
				if (a[i] & 1) flag = 0;
			if (flag) puts("1");
			else puts("2");
			continue;
		}
		int G = a[1]; Min = INF;
		for (register int i = 2; i <= n; ++i) G = gcd(G, a[i]);
		for (register int i = 1; i <= n; ++i) a[i] /= G;
		for (register int i = 1; i < n; ++i)
			for (register int j = i + 1; j <= n; ++j)
				if (gcd(a[i], a[j]) == 1) Min = min(Min, a[i] * a[j] - a[i] - a[j] + 1);
		Min = max(Min, a[n]);
		init();
		for (register int i = 1; i <= n; ++i)
			for (register int j = a[i]; j <= Min; ++j)
				dp[j] |= dp[j - a[i]];
		int ans = 0;
		for (register int i = 1; i <= Min; ++i)
			if (dp[i] && !DP[i]) {
				++ans;
				for (register int j = i; j <= Min; ++j) DP[j] |= DP[j - i];
			}
		printf("%d\n", ans);
	}
}
