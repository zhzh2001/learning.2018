#include <cstdio>
#include <cmath>
#include <algorithm>

int n, x, last, ans;

int main() {
	freopen("road.in", "r", stdin); freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (register int i = 1; i <= n; ++i) {
		int x; scanf("%d", &x);
		ans += abs(x - last);
		last = x;
	}
	(ans += last) >>= 1;
	printf("%d\n", ans);
	fclose(stdin); fclose(stdout);
	return 0;
}
