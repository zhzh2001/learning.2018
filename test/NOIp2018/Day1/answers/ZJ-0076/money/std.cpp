#include <cstdio>
#include <cmath>
#include <cstring>
#include <algorithm>
using namespace std;

typedef long long ll;
const int MAXN = 110, MAXV = 100000;
int n, i, j, k;
ll A, B, C, x, y;
ll need[MAXN], dp[MAXV];
ll a[MAXN];

inline void init() {
	for (register int i = 1; i <= n; ++i) need[i] = 1;
}

ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}

inline int check(ll A, ll B, ll C) {
	int T1 = gcd(A, B), T2 = gcd(T1, C);
	if (T1 > T2) return 0;
	A /= T1, B /= T1, C /= T1;
	if (A * B - A - B < C) return 1;
	for (register ll i = 1; i <= A * B; ++i) dp[i] = 0;
	dp[0] = 1;
	for (register ll i = 1; i <= B; ++i) dp[i * A] = 1;
	for (register ll i = B; i <= A * B; ++i) dp[i] |= dp[i - B];
	return dp[C];
}

int main() {
	freopen("money.in", "r", stdin); freopen("money.ans", "w", stdout);
	int T; scanf("%d", &T);
	while (T--) {
		scanf("%d", &n);
		init();
		for (register int i = 1; i <= n; ++i) scanf("%lld", &a[i]);
		sort(a + 1, a + n + 1);
		for (register int i = 1; i <= n; ++i)
			if (need[i]) {
				int flag = 1;
				for (register int j = 1; j < i; ++j)
					if (need[j]) {
						for (register int k = j; k < i; ++k)
							if (need[k])
								if (check(a[j], a[k], a[i])) { flag = 0; break; }
						if (!flag) break;
					}
				if (!flag) need[i] = 0;
			}
		int ans = 0;
		for (register int i = 1; i <= n; ++i) 
			if (need[i]) ++ans;
		printf("%d\n", ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
