#include <bits/stdc++.h>
using namespace std;

int main() {
	srand(time(0));
	freopen("money.in", "w", stdout);
	int T = rand() % 5 + 1; printf("%d\n", T);
	while (T--) {
		int n = rand() % 20 + 1; printf("%d\n", n);
		for (register int i = 1; i <= n; ++i) printf("%d ", rand() % 100 + 1);
		puts("");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
