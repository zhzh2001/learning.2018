#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define sqz main
#define ll long long
#define rep(i, a, b) for (int i = (a); i <= (b); i++)
#define per(i, a, b) for (int i = (a); i >= (b); i--)
#define Rep(i, a, b) for (int i = (a); i < (b); i++)
#define travel(i, u) for (int i = head[u]; ~i; i = edge[i].next)

const int INF = 1e9, N = 100, M = 25000;
const double eps = 1e-6, pi = acos(-1.0);
ll read()
{
	ll x = 0; int zf = 1; char ch;
	while (ch != '-' && (ch < '0' || ch > '9')) ch = getchar();
	if (ch == '-') zf = -1, ch = getchar();
	while (ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x * zf;
}
void write(ll y)
{
	if (y < 0) putchar('-'), y = -y;
	if (y > 9) write(y / 10);
	putchar(y % 10 + '0');
}

int F[M + 5], X[N + 5];
int sqz()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int H_H = read();
	while (H_H--)
	{
		rep(i, 1, M) F[i] = 0; F[0] = 1;
		int n = read(), ans = 0;
		rep(i, 1, n) X[i] = read();
		sort(X + 1, X + n + 1);
		rep(i, 1, n)
		{
			if (!F[X[i]])
			{
				ans++;
				rep(j, 0, M) if (F[j] && j + X[i] <= M) F[j + X[i]] = 1;
			}
		}
		printf("%d\n", ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
