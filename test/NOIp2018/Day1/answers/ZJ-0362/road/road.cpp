#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define sqz main
#define ll long long
#define rep(i, a, b) for (int i = (a); i <= (b); i++)
#define per(i, a, b) for (int i = (a); i >= (b); i--)
#define Rep(i, a, b) for (int i = (a); i < (b); i++)
#define travel(i, u) for (int i = head[u]; ~i; i = edge[i].next)

const int INF = 1e9;
const double eps = 1e-6, pi = acos(-1.0);
ll read()
{
	ll x = 0; int zf = 1; char ch;
	while (ch != '-' && (ch < '0' || ch > '9')) ch = getchar();
	if (ch == '-') zf = -1, ch = getchar();
	while (ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x * zf;
}
void write(ll y)
{
	if (y < 0) putchar('-'), y = -y;
	if (y > 9) write(y / 10);
	putchar(y % 10 + '0');
}

int ans = 0, last;
int sqz()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	int n = read();
	rep(i, 1, n)
	{
		int x = read();
		if (x > last) ans += x - last;
		last = x;
	}
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
