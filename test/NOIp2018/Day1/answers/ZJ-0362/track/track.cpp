#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<set>
using namespace std;
#define sqz main
#define ll long long
#define rep(i, a, b) for (int i = (a); i <= (b); i++)
#define per(i, a, b) for (int i = (a); i >= (b); i--)
#define Rep(i, a, b) for (int i = (a); i < (b); i++)
#define travel(i, u) for (int i = head[u]; ~i; i = edge[i].next)

const int INF = 1e9, N = 50000;
const double eps = 1e-6, pi = acos(-1.0);
ll read()
{
	ll x = 0; int zf = 1; char ch;
	while (ch != '-' && (ch < '0' || ch > '9')) ch = getchar();
	if (ch == '-') zf = -1, ch = getchar();
	while (ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x * zf;
}
void write(ll y)
{
	if (y < 0) putchar('-'), y = -y;
	if (y > 9) write(y / 10);
	putchar(y % 10 + '0');
}

int Deep[N + 5], head[N + 5], Flag[N + 5], F[1005][1005], Son[N + 5];
int n, m;
int edgenum = 0, ans = 0;
set<pair<int, int> > Set;
struct node
{
	int vet, next, val;
}edge[2 * N + 5];
void addedge(int u, int v, int w)
{
	edge[edgenum].vet = v;
	edge[edgenum].val = w;
	edge[edgenum].next = head[u];
	head[u] = edgenum++;
}

int Check1(int t)
{
	int num = 0;
	rep(i, 1, n) Flag[i] = 0;
	Set.clear();
	rep(i, 2, n) if (Deep[i] < t) Set.insert(make_pair(Deep[i], i));
	else Flag[i] = 1, num++;
	rep(i, 2, n)
	{
		if (Flag[i]) continue;
		Set.erase(make_pair(Deep[i], i)); Flag[i] = 1;
		set<pair<int, int> >::iterator it = Set.lower_bound(make_pair(t - Deep[i], 0));
		if (it == Set.end()) continue;
		Flag[(*it).second] = 1;
		Set.erase(it);
		num++;
	}
	return num >= m;
}
int Find1(int l, int r)
{
	if (l == r) return l;
	int mid = (l + r + 1) >> 1;
	if (Check1(mid)) return Find1(mid, r);
	else return Find1(l, mid - 1);
}

int Check2(int t)
{
	int last = 1, num = 0;
	rep(i, 2, n)
	{
		if (Deep[i] - Deep[last] >= t)
		{
			num++;
			last = i;
		}
	}
	return num >= m;
}
int Find2(int l, int r)
{
	if (l == r) return l;
	int mid = (l + r + 1) >> 1;
	if (Check2(mid)) return Find2(mid, r);
	else return Find2(l, mid - 1);
}

void dfs3(int u, int fa, int t)
{
	if (!Son[u])
	{
		F[u][0] = 0;
		return;
	}
	if (Son[u] == 1)
	{
		int son, v;
		travel(i, u) if (edge[i].vet != fa) son = edge[i].vet, v = edge[i].val;
		dfs3(son, u, t);
		rep(j, 0, m)
		{
			if (F[son][j] == -1) continue;
			if (F[son][j] + v >= t) F[u][j + 1] = 0;
			F[u][j] = F[son][j] + v;
		}
	}
	else
	{
		int son1 = 0, son2 = 0, v1, v2;
		travel(i, u)
		{
			int v = edge[i].vet;
			if (v == fa) continue;
			if (son1) son2 = v, v2 = edge[i].val;
			else son1 = v, v1 = edge[i].val;
		}
		dfs3(son1, u, t);
		dfs3(son2, u, t);
		rep(j, 0, m)
		{
			if (F[son1][j] == -1) continue;
			rep(k, 0, m)
			{
				if (F[son2][k] == -1) continue;
				if (F[son1][j] + v1 >= t && F[son2][k] + v2 >= t) F[u][j + k + 2] = max(F[u][j + k + 2], 0);
				else if (F[son1][j] + v1 >= t) F[u][k + j + 1] = max(F[son2][k] + v2, F[u][j + k + 1]);
				else if (F[son2][k] + v2 >= t) F[u][k + j + 1] = max(F[son1][j] + v1, F[u][j + k + 1]);
				else if (F[son1][j] + F[son2][k] + v1 + v2 >= t) F[u][j + k + 1] = max(F[u][j + k + 1], 0);
				else F[u][j + k] = max(F[u][j + k], max(F[son1][j] + v1, F[son2][k] + v2));
			}
		}
	}
}
int Check3(int t)
{
	rep(i, 1, n)
		rep(j, 1, m) F[i][j] = -1;
	dfs3(1, 0, t);
	return (F[1][m] != -1);
}
int Find3(int l, int r)
{
	if (l == r) return l;
	int mid = (l + r + 1) >> 1;
	if (Check3(mid)) return Find3(mid, r);
	else return Find3(l, mid - 1);
}

void dfs(int u, int fa)
{
	travel(i, u)
	{
		int v = edge[i].vet;
		if (v == fa) continue;
		Deep[v] = Deep[u] + edge[i].val;
		dfs(v, u);
	}
}
void dfs2(int u, int fa)
{
	travel(i, u)
	{
		int v = edge[i].vet;
		if (v == fa) continue;
		Deep[v] = Deep[u] + edge[i].val;
		Son[u]++; ans += edge[i].val;
		dfs2(v, u);
	}
}

int sqz()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	n = read(), m = read();
	rep(i, 1, n) head[i] = -1;
	int flag1 = 1, flag2 = 1;
	Rep(i, 1, n)
	{
		int u = read(), v = read(), w = read();
		addedge(u, v, w), addedge(v, u, w);
		if (u != 1) flag1 = 0;
		if (v != u + 1) flag2 = 0;
	}
	if (m == 1)
	{
		rep(i, 1, n) Deep[i] = 0;
		dfs(1, 0);
		int S = 1;
		rep(i, 1, n) if (Deep[i] > Deep[S]) S = i;
		rep(i, 1, n) Deep[i] = 0;
		dfs(S, 0);
		int T = 1;
		rep(i, 1, n) if (Deep[i] > Deep[T]) T = i;
		printf("%d\n", Deep[T]);
		fclose(stdin); fclose(stdout);
		return 0;
	}
	if (flag1)
	{
		int x = 0;
		travel(i, 1) Deep[edge[i].vet] = edge[i].val, x += Deep[edge[i].vet];
		sort(Deep + 1, Deep + n + 1);
		printf("%d\n", Find1(0, x / m));
		fclose(stdin); fclose(stdout);
		return 0;
	}
	if (flag2)
	{
		dfs(1, 0);
		printf("%d\n", Find2(0, Deep[n] / m));
		fclose(stdin); fclose(stdout);
		return 0;
	}
	dfs2(1, 0);
	printf("%d\n", Find3(0, ans / m));
	fclose(stdin); fclose(stdout);
	return 0;
}

