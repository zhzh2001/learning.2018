var
head,next,a,b,c,w,p,dis,q:array[0..200005]of longint;
k,x,y,z,e,n,m,i,j,max,maxx,mi,mm:longint;
vis:array[0..200005]of boolean;
bj:boolean;
procedure add(x,y,z:longint);
begin
inc(e);p[e]:=y;next[e]:=head[x];head[x]:=e;w[e]:=z;
end;
procedure spfa(tt:longint);
var v,u,h,t:longint;
 begin
 h:=0;t:=1;
 for i:=1 to n do dis[i]:=-maxlongint;
 q[1]:=tt;
 dis[1]:=0;
 while h<t do
  begin
   inc(h);
    u:=q[h];
    i:=head[u];
    while i>0 do
     begin
      v:=p[i];
      if dis[v]<dis[u]+w[i] then
       begin
        dis[v]:=dis[u]+w[i];
        if not vis[v] then
         begin
           inc(t);
           q[t]:=v;
         end;
       end;
       i:=next[i];
     end;
  end;
end;
begin
assign(input,'track.in');reset(input);
assign(output,'track.out');rewrite(output);
readln(n,m);
for i:=1 to n do
begin
  readln(x,y,z);
  a[i]:=x;b[i]:=y;c[i]:=z;
  if mm<z then begin mm:=z; mi:=x; end;
  if a[i]<>1 then bj:=true;
  add(x,y,z);
  add(y,x,z);
end;
if not bj then
 begin
  if m=1 then
  begin
    for i:=1 to n do
      begin
        if max<c[i] then begin max:=c[i]; k:=i; end;
      end;
    for i:=1 to n do
     begin
       if i<>k then
        if maxx<c[i] then maxx:=c[i];
     end;
     writeln(maxx+max);
     exit;
  end
   else
    begin
     spfa(mi);
     for i:=1 to n do if max<dis[i] then max:=dis[i];
     writeln(max);
    end;
 end;
close(input);
close(output);
end.