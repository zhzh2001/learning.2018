#include<bits/stdc++.h>
#define For(i,a,b) for(int i=(a),_en=(b);i<=_en;i++)
#define M 100005
using namespace std;
bool mmm1;
int n,a[M];
void rd(int &a){
	a=0;char w;
	while(w=getchar(),w< '0');
	do a=(a<<3)+(a<<1)+(w^48);
	while(w=getchar(),w>='0');
}
struct P70{
	int get(int L,int R,int nw){
		if(L>R)return 0;
		int mi=L;
		For(i,L+1,R)if(a[mi]>a[i])mi=i;
		return get(L,mi-1,a[mi])+get(mi+1,R,a[mi])+a[mi]-nw;
	}
	void solve(){printf("%d\n",get(1,n,0));}
}p70;
int Min(int x,int y){return a[x]<a[y]?x:y;}
struct P__{
	#define ls p<<1
	#define rs p<<1|1
	#define lson l,mid,ls
	#define rson mid+1,r,rs
	int mi[M<<2];
	void build(int l,int r,int p){
		if(l==r){mi[p]=l;return;}
		int mid=(l+r)>>1;
		build(lson);build(rson);
		mi[p]=Min(mi[ls],mi[rs]);
	}
	int query(int l,int r,int p,int L,int R){
		if(l==L&&r==R)return mi[p];
		int mid=(l+r)>>1;
		if(R<=mid)return query(lson,L,R);
		if(L> mid)return query(rson,L,R);
		return Min(query(lson,L,mid),query(rson,mid+1,R));
	}
	int get(int l,int r,int nw){
		if(l>r)return 0;
		int orz=query(1,n,1,l,r);
		return a[orz]-nw+get(l,orz-1,a[orz])+get(orz+1,r,a[orz]);
	}
	void solve(){
		build(1,n,1);
		printf("%d\n",get(1,n,0));
	}
}p__;
bool mmm2;
int main(){
//	printf("%lf\n",(&mmm2-&mmm1)/1024.0/1024);
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	rd(n);
	For(i,1,n)rd(a[i]);
	if(n<=1000)p70.solve();
	else p__.solve();
	return 0;
}
