#include<bits/stdc++.h>
#define For(i,a,b) for(int i=(a),_en=(b);i<=_en;i++)
#define Tor(i,x) for(int i=head[x];i;i=to[i].nx)
#define M 500005
using namespace std;
bool mmm1;
int head[M],tot,n,m;
struct name{int id,nx,v;}to[M<<1];
void add(int x,int y,int v){to[++tot]=(name){y,head[x],v};head[x]=tot;}
void rd(int &a){
	a=0;char w;
	while(w=getchar(),w< '0');
	do a=(a<<3)+(a<<1)+(w^48);
	while(w=getchar(),w>='0');
}
struct P20{
	int ID,mx;
	void dfs(int x,int f,int d){
		if(d>mx)mx=d,ID=x;
		Tor(i,x){
			int y=to[i].id;
			if(y==f)continue;
			dfs(y,x,d+to[i].v);
		}
	}
	void solve(){
		ID=mx=0;dfs(1,0,0);
		mx=0;dfs(ID,0,0);
		printf("%d\n",mx);
	}
}p20;
struct p40{
	int v[M];
	void dfs(int x,int f){
		Tor(i,x){
			int y=to[i].id;
			if(y==f)continue;
			v[x]=to[i].v;dfs(y,x);
		}
	}
	bool ck(int x){
		int cnt=0,now=0;
		For(i,1,n-1){
			now+=v[i];
			if(now>=x)now=0,cnt++;
		}return cnt>=m;
	}
	void solve(){
		dfs(1,0);
		int l=1,r=1e9;
		while(l<=r){
			int mid=(l+r)>>1;
			if(ck(mid))l=mid+1;
			else r=mid-1;
		}printf("%d\n",r);
	}
}p40;
struct P55{
	int v[M],cnt,V[M],orz,ans;
	void solve(){
		cnt=orz=0;ans=1e9;
		Tor(i,1)v[++cnt]=to[i].v;
		sort(v+1,v+cnt+1);
		For(i,cnt-m+1,cnt)
			if(cnt-m-(i-(cnt-m+1))>0)V[++orz]=v[i]+v[cnt-m-(i-(cnt-m+1))];
			else V[++orz]=v[i];
		For(i,1,orz)if(V[i]<ans)ans=V[i];
		printf("%d\n",ans);
	}
}p55;
struct P__{
	int ans,cnt,orz,v[M],V[M];
	void dfs(int x,int f){
		Tor(i,x){
			int y=to[i].id;
			if(y==f)continue;
			dfs(y,x);v[++cnt]=to[i].v;
		}
	}
	void solve(){
		ans=1e9;cnt=orz=0;dfs(1,0);
		sort(v+1,v+cnt+1);
		For(i,cnt-m+1,cnt)
			if(cnt-m-(i-(cnt-m+1))>0)V[++orz]=v[i]+v[cnt-m-(i-(cnt-m+1))];
			else V[++orz]=v[i];
		For(i,1,orz)if(V[i]<ans)ans=V[i];
		printf("%d\n",ans);
	}
}p__;
bool mmm2;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	rd(n),rd(m);int x,y,v,f1=1,f2=1;
	For(i,2,n){
		rd(x),rd(y),rd(v),add(x,y,v),add(y,x,v);
		if(x!=1)f1=0;
		if(x!=y+1&&y!=x+1)f2=0;
	}
	if(m==1)p20.solve();
	else if(f2)p40.solve();
	else if(f1)p55.solve();
	else p__.solve();
	return 0;
}
