#include <bits/stdc++.h>
#define rep(x,y,z) for(int x=(y);x<=(z);x++)
using namespace std;

int money[110];

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--)
	{
		int n;
		scanf("%d",&n);
		rep(i,1,n) scanf("%d",&money[i]);
		if(n==2)
		{
			if(money[1]>money[2]) swap(money[1],money[2]);
			if(money[2]%money[1]) puts("2");
				else puts("1");
		}
	}
	return 0;
}
