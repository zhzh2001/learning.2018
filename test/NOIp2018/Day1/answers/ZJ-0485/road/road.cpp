#include <bits/stdc++.h>
#define rep(x,y,z) for(int x=(y);x<=(z);x++)
using namespace std;

int road[100100];
bool flag=1;
int ans=0;

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n;
	scanf("%d",&n);
	rep(i,1,n) scanf("%d",&road[i]);
	int minn=1e9+7;
	while(flag)
	{
		int k=1;
		flag=0;
		rep(i,1,n)
		{
			if(road[i]) flag=1;
			if(road[i]==0&&minn==1e9+7) k=i+1;
			if(road[i]==0&&minn!=1e9+7)
			{
				rep(j,k,i-1) road[j]-=minn;
				ans+=minn;
				minn=1e9+7;
				k=i+1;
				continue;
			}
			if(road[i]<=minn&&road[i]!=0) minn=road[i];
			if(i==n&&minn!=1e9+7)
			{
				rep(j,k,n) road[j]-=minn;
				ans+=minn;
				minn=1e9+7;
			}
		}
	}
	printf("%d\n",ans);
	return 0;
}

/*
6
4 3 2 5 3 5

9
*/
