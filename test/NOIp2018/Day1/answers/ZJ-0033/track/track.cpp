#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int read(){
	int x=0;char c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
	return x;
}
inline void swap(int &a,int &b){a^=b;b^=a;a^=b;}
inline int min(int a,int b){return a<b?a:b;}
inline int max(int a,int b){return a>b?a:b;}
const int N=5e4+3;
const int M=1e4+3;
int n,m,f[N],cnt,head[N],s,a[N],b[N],l[N],d[N];
bool bt[N];
struct Tr{
	int v,w,l;
}edge[N<<1|1];
inline void add(int u,int v,int w){
	edge[cnt].v=v;
	edge[cnt].w=w;
	edge[cnt].l=head[u];
	head[u]=cnt++;
}
void dfs_pf1(int u){
	bt[u]=1;
	int fi=0,se=0;
	for(int i=head[u];~i;i=edge[i].l){
		int v=edge[i].v,w=edge[i].w;
		if(bt[v])continue;
		dfs_pf1(v);
		f[u]=max(f[u],f[v]+w);
		se=max(f[v]+w,se);
		if(fi<se)swap(fi,se);
	}
	//printf("%d %d %d\n",u,fi,se);
	s=max(s,fi+se);
}
bool cmp(int a,int b){return a<b;}
bool cmp1(int a,int b){return a>b;}
bool judge_pfb(int o){
	int dd=0;
	for(int t=0,i=1;i<=n;i++){
		t+=d[i];
		if(t>=o)dd++,t=0;
	}
	if(dd>=m)return 1;
	return 0;
}
bool judge_pfa(int o){
	s=0;int b=1,e=n;
	while(b<=e){
		if(l[b]>=o)s++,b++;
		else if(b==e)break;
		else{
			while(l[b]+l[e]<o&&b<e)e--;
			if(b!=e)s++,b++,e--;
			else break;
		}
	}
	if(s>=m)return 1;
	return 0;
}
void dfs_pfl(int u,int o){
	bt[u]=1;
	int fi=0,se=0;
	for(int i=head[u];~i;i=edge[i].l){
		int v=edge[i].v,w=edge[i].w;
		if(bt[v])continue;
		dfs_pfl(v,o);
		se=max(se,f[v]+w);
		if(fi<se)swap(fi,se);
	}
	if(se>=o){s+=2;return;}
	if(fi>=o){s+=1;f[u]=se;return;}
	if(fi+se>=o){s+=1;return;}
	f[u]=fi;return;
}
bool judge_pfl(int o){
	memset(bt,0,sizeof(bt));
	memset(f,0,sizeof(f));s=0;
	dfs_pfl(1,o);
	if(s<m)return 0;
	return 1;
}
void pfl(){
	int l=0,r=500000000;
	while(l<=r){
		int mid=(l+r)>>1;
		if(judge_pfl(mid))l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",r);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(head,-1,sizeof(head));
	n=read();m=read();
	if(m==n-1){
		int mi=M;
		for(int i=1;i<n;i++){
			int u=read(),v=read(),w=read();
			mi=min(mi,w);
		}
		printf("%d\n",mi);
		return 0;
	}
	if(m==1){
		for(int i=1;i<n;i++){
			int u=read(),v=read(),w=read();
			add(u,v,w),add(v,u,w);
		}
		dfs_pf1(1);
		printf("%d\n",s);
		return 0;
	}
	bool pa=1,pb=1;
	for(int i=1;i<n;i++){
		a[i]=read();b[i]=read();l[i]=read();
		if(a[i]>b[i])swap(a[i],b[i]);
		if(a[i]!=1)pa=0;
		if(b[i]!=a[i]+1)pb=0;
	}
	if(pb){
		n--;
		for(int i=1;i<=n;i++)d[a[i]]=l[i];
		int l=0,r=500000000;
		while(l<=r){
			int mid=(l+r)>>1;
			if(judge_pfb(mid))l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",r);
		return 0;
	}
	if(pa){
		n--;
		sort(l+1,l+n+1,cmp1);
		int l=0,r=500000000;
		while(l<=r){
			int mid=(l+r)>>1;
			if(judge_pfa(mid))l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",r);
		return 0;
	}
	for(int i=1;i<n;i++)
		add(a[i],b[i],l[i]),add(b[i],a[i],l[i]);
	pfl();
	return 0;
}
