#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int read(){
	int x=0;char c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
	return x;
}
const int N=103;
const int M=25000;
int T,n,a[N],s;
bool b[M+3],o[M+3];
bool cmp(int a,int b){return a<b;}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		n=read();s=0;
		memset(b,0,sizeof(b));
		memset(o,0,sizeof(o));
		for(int i=1;i<=n;i++){
			a[i]=read();
			b[a[i]]=1;
		}
		sort(a+1,a+n+1,cmp);
		o[0]=1;
		for(int i=1;i<=M;i++){
			for(int j=1;j<=n;j++){
				if(i>=a[j]&&o[i-a[j]]){
					o[i]=1;
					if(b[i]&&a[j]!=i)s++;
					break;
				}
			}
		}
		printf("%d\n",n-s);
	}
	return 0;
}
