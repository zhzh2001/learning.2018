#include<cstdio>
using namespace std;
int read(){
	int x=0;char c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
	return x;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n,s=0,t=0,o;
	n=read();
	for(int i=1;i<=n;i++){
		o=read();
		if(o<t)s+=t-o;
		t=o;
	}
	printf("%d\n",s+t);
	return 0;
}
