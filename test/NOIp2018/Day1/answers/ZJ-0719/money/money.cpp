#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {
		if(ch=='-') f=-1;ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<1)+(x<<3)+ch-'0'; ch=getchar();
	}
	return x*f;
}
int a[110];
bool f[25010];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	T=read();
	while(T--){
		memset(f,0,sizeof(f));
		f[0]=1;
		int n=read();
		for(int i=1;i<=n;++i){
			a[i]=read();
		}
		sort(a+1,a+1+n);
		int maxn=a[n],ans=0;
		for(int i=1;i<=n;++i){
			if(!f[a[i]]){
				++ans;
				for(int j=a[i];j<=maxn;++j){
					f[j]|=f[j-a[i]];
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
