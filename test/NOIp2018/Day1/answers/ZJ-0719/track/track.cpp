#include<bits/stdc++.h>
#define N 50010
using namespace std;
typedef long long ll;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {
		if(ch=='-') f=-1;ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<1)+(x<<3)+ch-'0'; ch=getchar();
	}
	return x*f;
}
int head[N],nex[N<<1],ver[N<<1],edge[N<<1],tot;
inline void add(int x,int y,int z){
	ver[++tot]=y;nex[tot]=head[x];edge[tot]=z;head[x]=tot;
}
bool v[N];
int d[N],fa[N];
int n,m;
queue<int>q;
inline void bfs(int xx){
	memset(d,0,sizeof(d));
	memset(v,0,sizeof(v));
	while(!q.empty()) q.pop();v[xx]=1;q.push(xx);
	while(!q.empty()){
		int x=q.front();q.pop();
		for(int i=head[x];i;i=nex[i]){
			int y=ver[i],z=edge[i];
			if(v[y]) continue;
			d[y]=d[x]+z;v[y]=1;q.push(y);
		}
	}
}
int pose,poss,cnt,mid,has;
int f[N][3];
multiset<int> s[N];
multiset<int>::iterator it1,it2;
inline void dfs(int x){
	s[x].clear();
	for(int i=head[x];i;i=nex[i]){
		int y=ver[i],z=edge[i];
		if(v[y]) continue;
		v[y]=1;dfs(y);v[y]=0;
		s[x].insert(f[y][1]+z);
	}
//	cout<<x<<" "<<has<<endl;
	if(s[x].empty()) return;
	while(!s[x].empty()){
	it1=s[x].begin();
	if((*it1)>=mid){
		++has,s[x].erase((*it1));continue;
	}
	it2=lower_bound(s[x].begin(),s[x].end(),mid-(*it1));
//	cout<<x<<" "<<(*it1)<<" "<<(*it2)<<endl;
	if(it1==it2) ++it2;
	if(it2==s[x].end()||(*it1)+(*it2)<mid){
		if(f[x][2]<(*it1)){
			if(f[x][1]<(*it1)) f[x][2]=f[x][1],f[x][1]=(*it1);
			else f[x][2]=(*it1);
		}
		s[x].erase(*it1);
	}
	else s[x].erase((*it1)),s[x].erase((*it2)),++has;
	}
}
inline bool check()
{
//	cout<<mid<<endl;
	memset(f,0,sizeof(f));
	int now=pose;
	cnt=0,has=0;
	while(now!=poss){
		dfs(now);
//		cout<<now<<" "<<f[now][1]<<" "<<f[now][2]<<endl;
		if(f[now][2]>=mid-cnt) ++has,cnt=f[now][1];
		else if(f[now][1]>=mid-cnt)++has,cnt=f[now][2];
		else cnt=max(cnt,f[now][1]);
		if(cnt>=mid) cnt=0,++has;
		cnt+=d[now]-d[fa[now]];
		if(cnt>=mid) cnt=0,++has;
//		if(now==poss) break;
		now=fa[now];
//		cout<<now<<" "<<cnt<<" "<<has<<endl;
		if(has>=m) return 1;
	}
	return 0;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<n;++i){
		int x=read(),y=read(),z=read();add(x,y,z);add(y,x,z);
	}
	bfs(1);
	int maxn=0;
	for(int i=1;i<=n;++i){
		if(maxn<d[i]) maxn=d[i],poss=i;
	}
	bfs(poss);
	maxn=0;
	for(int i=1;i<=n;++i){
		if(maxn<d[i]) maxn=d[i],pose=i;
	}
	memset(v,0,sizeof(v));
	int now=pose;v[now]=1;
	while(now!=poss){
		for(int i=head[now];i;i=nex[i]){
			int y=ver[i],z=edge[i];{
				if(d[now]==d[y]+z) {
					v[y]=1;fa[now]=y;now=y;continue;
				}
			}
		}
	}
//	mid=14;
//	cout<<check();
	int l=0,r=1e9+7,ans=0;
	while(l<=r){
		mid=l+r>>1;
		if(check()) ans=max(ans,mid),l=mid+1;
		else r=mid-1;
	}
	printf("%d",ans);
	return 0;
}
