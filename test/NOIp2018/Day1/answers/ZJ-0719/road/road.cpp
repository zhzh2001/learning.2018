#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {
		if(ch=='-') f=-1;ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<1)+(x<<3)+ch-'0'; ch=getchar();
	}
	return x*f;
}
int q[100010],head=0,tail=1;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n=read();
	ll ans=0;
	for(int i=1;i<=n;++i){
		int x=read();
		if(head<tail||q[head]<=x) {
			q[++head]=x;continue;
		}
		if(head>=tail&&q[head]>x){
			ans+=q[head]-x;
		}
		while(head>=tail&&q[head]>x){
			--head;
		}
		q[++head]=x;
	}
	ans+=q[head];
	printf("%lld",ans);
	return 0;
}
