#include<bits/stdc++.h>
using namespace std;
int read()
{
	char ch=getchar();int x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(int aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
int T,n,ans,now;
int f[25005],a[105];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();ans=0;
		memset(f,0,sizeof(f));f[0]=1;
		for(int i=1;i<=n;++i) a[i]=read();
		sort(a+1,a+n+1);
		for(int i=1;i<=n;++i)
		{
			if(f[a[i]]) continue;
			ans++;
			for(int j=0;j<=a[n]-a[i];++j)
			if(f[j]) f[j+a[i]]=1;
		}
		write(ans);puts("");
	}
	return 0;
}
