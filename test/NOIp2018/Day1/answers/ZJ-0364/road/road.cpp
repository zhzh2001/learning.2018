#include<bits/stdc++.h>
using namespace std;
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
long long n,ans,top;
long long zhan[100005];
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(long long i=1;i<=n;++i)
	{
		long long x=read();
		while(top&&zhan[top]>=x) ans+=min(zhan[top]-zhan[top-1],zhan[top]-x),top--;
		zhan[++top]=x;
	}
	while(top) ans+=zhan[top]-zhan[top-1],top--;
	write(ans);
	return 0;
}
