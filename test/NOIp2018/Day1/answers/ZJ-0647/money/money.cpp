#include<bits/stdc++.h>
using namespace std;
int t,n,m,maxa,a[205],b[25005];
void open()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}
void close()
{
	fclose(stdin);
	fclose(stdout);
}
int main()
{
	open();
	scanf("%d",&t);
	while (t--)
	{
		memset(b,0,sizeof(b));
		b[0]=1;
		maxa=0;
		m=0;
		scanf("%d",&n);
		for (int i=1;i<=n;i++)
		{
			scanf("%d",&a[i]);
			if (a[i]>maxa) maxa=a[i];
		}
		sort(a+1,a+n+1);
		for (int i=1;i<=n;i++)
		{
			if (b[a[i]]==1) continue;
			m++;
			for (int j=0;j<=maxa-a[i];j++)
			if (b[j]) b[j+a[i]]=1;
		}
		printf("%d\n",m);
	}
	close();
	return 0;
}
