#include<bits/stdc++.h>
using namespace std;
const int inf=2000000000;
int n,m,minl=inf,maxl,len[50005],x1,vis[50005],d[50005],ll[50005],lb[50005];
bool flaga=true,flagb=true;
vector < pair<int,int> > e[50005];
void dfs(int u,int fa)
{
	if (len[u]>len[x1]) x1=u;
	for (int i=0;i<e[u].size();i++)
	{
		int v=e[u][i].first,w=e[u][i].second;
		if (v==fa) continue;
		len[v]=len[u]+w;
		dfs(v,u);
	}
}
bool checka(int x)
{
	memset(vis,0,sizeof(vis));
	int k=n-1;
	for (int i=1;i<=m;i++)
	{
		vis[k]=1;
		int kx=x-ll[k];
		k--;
		if (kx<=0) continue;
		int kk=lower_bound(ll+1,ll+n,kx)-ll;
		while (vis[kk]==1)
		{
			kk++;
			if (kk>k) return false;
		}
	}
	return true;
}
bool checkb(int x)
{
	int k=0,now=0;
	for (int i=1;i<=n-1;i++)
	{
		now+=lb[i];
		if (now>=x)
		{
			now=0;
			k++;
		}
	}
	return k>=m;
}
void open()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
}
void close()
{
	fclose(stdin);
	fclose(stdout);
}
int main()
{
	open();
	scanf("%d%d",&n,&m);
	for (int i=1,a,b,l;i<=n-1;i++)
	{
		scanf("%d%d%d",&a,&b,&l);
		if (a!=1) flaga=false;
		if (b!=a+1) flagb=false;
		lb[a]=l;
		ll[i]=l;
		e[a].push_back(make_pair(b,l));
		e[b].push_back(make_pair(a,l));
		minl=min(minl,l);
	}
	len[1]=0;
	dfs(1,0);
	len[x1]=0;
	dfs(x1,0);
	if (m==1)
	{
		printf("%d",len[x1]);
		return 0;
	}
	if (m==n-1)
	{
		printf("%d",minl);
		return 0;
	}
	if (flaga)
	{
		sort(ll+1,ll+n);
		int l=minl,r=len[x1],mid,ans=minl;
		while (l<=r)
		{
			mid=(l+r)>>1;
			if (checkb(mid))
			{
				l=mid+1;
				ans=mid;
			}
			else
			{
				r=mid-1;
			}
		}
		printf("%d",ans);
		return 0;
	}
	if (flagb)
	{
		int l=minl,r=len[x1],mid,ans=minl;
		while (l<=r)
		{
			mid=(l+r)>>1;
			if (checkb(mid))
			{
				l=mid+1;
				ans=mid;
			}
			else
			{
				r=mid-1;
			}
		}
		printf("%d",ans);
		return 0;
	}
	printf("%d",len[x1]/m+1);
	close();
	return 0;
}
