//minamoto
#include<bits/stdc++.h>
using namespace std;
const int N=50005;
struct node{
	int u,dis;
	node(){}
	node(int u,int dis):u(u),dis(dis){}
	inline bool operator <(const node &b)const
	{return dis>b.dis;}
};priority_queue<node>q;
int a[N],n,m,p[N],tot,dis[N],vis[N];
bool check(int x){
	while(!q.empty())q.pop();
	memset(dis,0x3f,sizeof(dis));
	memset(vis,0,sizeof(vis));
	q.push(node(0,0)),dis[0]=0;
	while(!q.empty()){
		int u=q.top().u;q.pop();if(vis[u])continue;vis[u]=1;
		for(int i=2;i<=m;++i){
			int v=(u+p[i])%p[1];if(dis[u]+p[i]>x)continue;
			if(dis[v]>dis[u]+p[i])
			dis[v]=dis[u]+p[i],q.push(node(v,dis[v]));
		}
	}
	return dis[x%p[1]]<=x;
}
void solve(){
	scanf("%d",&n),m=0;
	for(int i=1;i<=n;++i)scanf("%d",&a[i]);
	sort(a+1,a+1+n),n=unique(a+1,a+1+n)-a-1;
	if(a[n]<a[1]*2)return (void)(printf("%d\n",n));
	p[++m]=a[1];
	for(int i=2;i<=n;++i)if(!check(a[i]))p[++m]=a[i];
	printf("%d\n",m);
}
int main(){
//	freopen("testdata.in","r",stdin);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--)solve();
	return 0;
}
