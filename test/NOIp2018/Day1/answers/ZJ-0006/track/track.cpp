//minamoto
#include<bits/stdc++.h>
#define IT multiset<int>::iterator
using namespace std;
const int N=2e5+5;
int head[N],Next[N],ver[N],edge[N],tot;
inline void add(int u,int v,int e){ver[++tot]=v,Next[tot]=head[u],head[u]=tot,edge[tot]=e;}
int n,m,l=1,r,flag=1,mid,ans;
namespace solve1{
	int sum[N];
	bool check(int len){
		int res=0;
		for(int l=1,r=1;l<=n;l=r){
			while(r<=n&&sum[r]-sum[l]<len)++r;
			if(r>n)break;if(++res>=m)return true;
		}
		return false;
	}
	void MAIN(){
		for(int u=1;u<n;++u)for(int i=head[u];i;i=Next[i])
		if(ver[i]==u+1)sum[ver[i]]=sum[u]+edge[i];
		while(l<=r){
			mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}
namespace solve2{
	multiset<int>s[N];IT it,itl;int res;
	void dfs(int u,int fa,int len,int e){
		for(int i=head[u];i;i=Next[i])
		if(ver[i]!=fa)dfs(ver[i],u,len,edge[i]);
		while(!s[u].empty()&&(*(--s[u].end()))>=len)++res,s[u].erase(--s[u].end());
		int mx=0;
		while(!s[u].empty()){
			itl=s[u].begin();int val=*itl;s[u].erase(itl);
			if((it=s[u].lower_bound(len-val))!=s[u].end())++res,s[u].erase(it);
			else mx=max(mx,val);
		}
		if(fa)s[fa].insert(mx+e);
//		multiset<int>().swap(s[u]);
		s[u].clear();
	}
	bool check(int len){
		res=0;
		dfs(1,0,len,0);
		return res>=m;
	}
	void MAIN(){
		while(l<=r){
			mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}
int main(){
//	freopen("testdata.in","r",stdin);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1,u,v,e;i<n;++i){
		scanf("%d%d%d",&u,&v,&e),add(u,v,e),add(v,u,e),r+=e;
		if(u!=v+1&&v!=u+1)flag=0;
	}
	flag?solve1::MAIN():solve2::MAIN();
	return 0;
}
