#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline GG read() {
	GG x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}
inline void write(GG x) {
	if(x<0) putchar('-'), x = -x;
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(GG x) {
	write(x); puts("");
}

const int N = 111; 
int n, ans, T, Ti, boom; 
int a[N], f[2000011], tmp[N], tot, old[2000011], b[N], vis[N], wzt[N], waa;  
LL alpha; 

inline void work_1() {
	if(a[1] > a[2]) swap(a[1], a[2]); 
	if(a[2] % a[1]==0) 
		puts("1"); 
	else 
		puts("2"); 
}

inline int check() {
	tot = 0; 
	For(i, 1, n) if(b[i]) tmp[++tot] = a[i]; 
	For(i, 0, alpha) f[i] = 0;
	f[0] = 1; 
	For(i, 1, tot)	
	  For(j, 0, alpha) 
	  	if(j-tmp[i]>=0) f[j] |= f[j-tmp[i]]; 
	For(i, 1, alpha) 
		if(f[i] != old[i]) return 0; 
	return 1; 
} 

void dfs(int x, int sum) {
	if(boom) return; 
	if(sum >= ans) return;  
	if(x>n) {
		if(check()) {
			++Ti; 
			if(Ti*T>=7e7){
				boom = 1; 
				return; 
			}
			if(sum < ans) ans = sum;  
		}
		return; 
	}
	b[x] = rand()%2; dfs(x+1, sum+b[x]); 
	b[x] = b[x]^1;   dfs(x+1, sum+b[x]); 
}

int gcd(int x, int y) {
	if(x%y==0) return y; 
	return gcd(y, x%y); 
}

inline void work_3() { 
	For(i, 1, n) vis[i] = 0; waa = 0; 
	For(i, 1, n) 
		For(j, 1, n) if(i!=j && a[i]%a[j]==0) vis[i] = 1; 
	For(i, 1, n) if(!vis[i]) wzt[++waa] = a[i]; 
	n = waa; 
	For(i, 1, n) a[i] = wzt[i]; 
	if(n==2) { work_1(); return; } 
	
	sort(a+1, a+n+1); 
	if(n>13 || a[n]>16) {
		alpha = a[n-1]*a[n];
	}
	else {
		alpha = a[n-1]*a[n]; 
	}
	if(n>=3 && alpha>=1000000) {
		For(i, 1, n-1) 
			For(j, i+1, n) if(a[i]*a[j]-a[i]-a[j]<alpha && gcd(a[i], a[j])==1) alpha = a[i]*a[j]-a[i]-a[j]; 
	}
	if(alpha>=1000000) alpha = 1000000; 
	++alpha; 
	
	tot = 0; 
	For(i, 1, n) tmp[++tot] = a[i]; 
	For(i, 0, alpha) old[i] = 0;
	old[0] = 1; 
	For(i, 1, tot)	
	  For(j, 0, alpha) 
	  	if(j-tmp[i]>=0) old[j] |= old[j-tmp[i]]; 
	ans = n; 
	boom = 0; 
	dfs(1, 0); 	
	writeln(ans); 
}

int main() {
	freopen("money.in", "r", stdin); 
	freopen("money.out", "w", stdout); 
	T = read(); 
	srand(time(0)); 
	while(T--) {
		Ti = 0; 
		n = read(); bool flag = 0; 
		For(i, 1, n) {
			a[i] = read(); 
			if(a[i]==1) flag = 1; 
		}
		if(flag) { puts("1"); continue; }
		if(n==2) {
			work_1(); 
			continue; 
		}
		
		work_3(); 
	}
}


/*

2
4
3 19 10 6
5
11 29 13 19 17



*/



