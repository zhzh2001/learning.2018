#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline GG read() {
	GG x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}
inline void write(GG x) {
	if(x<0) putchar('-'), x = -x;
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(GG x) {
	write(x); puts("");
}

const int N = 100011; 
int n; 
int stk[N], top, ans, Mn; 

int main() {
	freopen("road.in", "r", stdin); 
	freopen("road.out", "w", stdout); 
	n = read(); 
	Mn = 0; 
	For(i, 1, n) {
		int x = read(); 
		if(x > stk[top]) {
			ans += stk[1]-Mn; 
			Mn = stk[top]; 
			top = 0; 
		}
		stk[++top] = x; 
	}
	ans += stk[1]-Mn; 
	writeln(ans); 
}


