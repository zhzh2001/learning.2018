#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline GG read() {
	GG x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}
inline void write(GG x) {
	if(x<0) putchar('-'), x = -x;
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(GG x) {
	write(x); puts("");
}

const int N = 50011; 
struct edge{
	int to, pre, val; 
}e[N*2];
int n, m, cnt; 
int head[N], dep[N], val[N], a[N]; 

inline void add(int x, int y, int v) {
	e[++cnt].to = y; 
	e[cnt].pre = head[x]; 
	e[cnt].val = v; 
	head[x] = cnt; 
}

void dfs(int u, int fa) {
	for(int i=head[u]; i; i=e[i].pre) {
		int v = e[i].to; 
		if(v != fa) {
			dep[v] = dep[u] + e[i].val; 
			dfs(v, u); 
		}
	}
}

inline void work_1() {
	int root = 1; dep[root] = 0; dep[0] = 0; 
	dfs(root, root); 
	int S = 0; 
	For(i, 1, n) if(dep[i] > dep[S]) S = i; 
	dep[S] = 0; 
	dfs(S, S); 
	int T = 0; 
	For(i, 1, n) if(dep[i] > dep[T]) T = i; 
	writeln(dep[T]); 
	exit(0); 
}

bool cmp_1(int a, int b) {
	return a > b; 
}

inline void work_2() {
	sort(val+1, val+n, cmp_1); 
	int sum = 0; 
	if(m==n-1) 
		writeln(val[n-1]); 
	else 
		writeln(val[m]+val[m+1]); 
	exit(0); 
}

bool check(int mid) {
	int sum = 0, num = 0; 
	For(i, 1, n-1) {
		sum += a[i]; 
		if(sum >= mid) {
			++num; 
			sum = 0; 
		}
	}
	return num >= m; 
}

inline void work_3() {
	int l = 0, r = 0; 
	For(i, 1, n-1) r += val[i]; 
	while(l < r) {						
		int mid = (l+r+1) /2; 
		if(check(mid)) 
			l = mid; 
		else 
			r = mid-1; 
	}
	writeln(l); 
	exit(0); 
}

int main() {
	freopen("track.in", "r", stdin); 
	freopen("track.out", "w", stdout); 
	n = read(); m = read(); 
	bool flag = 0; bool wzt = 0; 
	For(i, 1, n-1) {
		int x = read(), y = read(), v = read(); 
		if(x > y) swap(x, y); 
		if(x!=1 && y!=1) flag = 1; 
		if(y-x!=1) wzt = 1; 
		val[i] = v; a[x] = v; 
		add(x, y, v); add(y, x, v); 
	}
	if(m==1) work_1(); 
	if(flag==0) work_2(); 
	if(!wzt) work_3(); 
}





