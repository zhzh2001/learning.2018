#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int MAXN=50005;
const int MAXM=100010;
int n,m,x,y,z,top,topr;
int nxt[MAXM],fir[MAXM],get[MAXM],value[MAXM],val[MAXM];
int q[MAXM],res[MAXM],now,ans,mid;
int dist[MAXM],h,t,st,l,r,vis[MAXM];
bool flag;
inline int read(){
	int x=0; bool flag=0; char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') flag=1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	if (!flag) return x; else return -x;
}
inline void add(int x,int y,int z){
	get[++top]=y; value[top]=z;
	nxt[top]=fir[x]; fir[x]=top; 
}
inline bool judge(int x){
	int tot=0,sum=0,kkk;
	for (int i=1;i<n;++i){
		kkk=fir[i];
		if (get[kkk]!=i+1) kkk=nxt[kkk];
		sum+=value[kkk];
		if (sum>=x) sum=0,++tot;
	}
	if (tot>=m) return 1; else return 0;
}
void spfa(int x){
	h=0; t=0;
	q[++t]=x; memset(dist,63,sizeof(dist)); dist[x]=0; vis[x]=1;
	while (h<t){
		int u=q[++h];
		vis[u]=0;
		for (int i=fir[u];i;i=nxt[i]){
			int v=get[i];
			if (dist[u]+value[i]<dist[v]){
				dist[v]=dist[u]+value[i];
				if (!vis[v]){
					vis[v]=1; q[++t]=v;
				}
			}
		}
	}
}
inline int get_ans(){
	memset(q,0,sizeof(q)); memset(vis,0,sizeof(vis)); st=1;
	spfa(st);
	for (int i=1;i<=n;++i) if (dist[i]>dist[st]) st=i;
	memset(q,0,sizeof(q)); memset(vis,0,sizeof(vis));
	spfa(st);
	for (int i=1;i<=n;++i) if (dist[i]>dist[st]) st=i;
	return dist[st];
}
bool comp(const int &x,const int&y){return x>y;}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(); m=read(); flag=0;
	for (int i=1;i<n;++i){
		x=read();y=read();z=read(); val[i]=z;
		if (x!=1) flag=1;
		add(x,y,z); add(y,x,z);
	}
	if (m==1){
		printf("%d",get_ans());
		return 0;
	} else 
	if (!flag){
		sort(val+1,val+1+n,comp); top=0;
		for (int i=1;i<=m;++i) q[++top]=val[i];
		if (m==n-1){
			printf("%d",val[n-1]); return 0;
		} topr=0; memset(res,0,sizeof(res));
		for (int i=m+1;i<=n-1;++i){
			res[++topr]=val[i]; if (topr==m) break;
		}  ans=2147483647;
		for (int i=1;i<=m;++i){
		     now=q[i]+res[m-i+1]; if (now<ans) ans=now;
		}
		printf("%d",ans);
		return 0;
	} else { 
		l=1; r=2e8;
		while (l<=r){
			mid=(l+r)/2;
			if (judge(mid)){
				l=mid+1; ans=mid;
			} else r=mid-1;
		}
		printf("%d",ans);
		return 0;
	}
	return 0;
}
