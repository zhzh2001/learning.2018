#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int MAXN=105;
const int MAXD=30005;
int t,n,fir,ans,ansnow,top,len,maxnum;
int a[MAXN],num[MAXD],q[MAXD];
bool vis[MAXD];
bool c[MAXD];
inline int read(){
	int x=0; bool flag=0; char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') flag=1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	if (!flag) return x; else return -x;
}
inline void insert(int x){
	c[x]=1;
	for (int i=x+1;i<=maxnum;++i){
		c[i]|=c[i-x]; 
	} 
}
bool comp(const int &x,const int &y){return x<y;}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read();
	while (t--){
		n=read(); ans=n; top=0; maxnum=0;
		memset(vis,0,sizeof(vis));
		for (int i=1;i<=n;++i) a[i]=read();
		sort(a+1,a+1+n,comp);
		fir=a[1]; vis[0]=1; q[++top]=fir;if (q[top]>maxnum) maxnum=q[top];
		for (int i=2;i<=n;++i){
			if (vis[a[i]%fir]){
				--ans; continue;
			}
			vis[a[i]%fir]=1;q[++top]=a[i]; if (q[top]>maxnum) maxnum=q[top];
		}
		if (top<=2){
			printf("%d\n",top); continue;
		} ans=top; len=0; memset(c,0,sizeof(c)); c[0]=1;
		insert(q[1]); insert(q[2]);
		for (int i=3;i<=top;++i){
			if (c[q[i]]){
				--ans; continue;
			}
			insert(q[i]);
		}
		printf("%d\n",ans);
	}
	return 0;
}
