#include<cstdio>
using namespace std;
int n,x,last;
long long ans;
inline int read(){
	int x=0; bool flag=0; char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') flag=1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	if (!flag) return x; else return -x;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read(); last=0;
	for (int i=1;i<=n;++i){
		x=read();
		if (x>last){
			ans=ans+1ll*(x-last);
		}
		last=x;
	}
	printf("%lld",ans);
	return 0;
}
