var
    n,i,j,s:longint;
    a:array[0..200000]of longint;
begin
    assign(input,'road.in');
    reset(input);
    assign(output,'road.out');
    rewrite(output);
    read(n);
    for i:=1 to n do read(a[i]);
    s:=a[1];
    for i:=1 to n-1 do begin
        if a[i]<a[i+1] then s:=s+a[i+1]-a[i];
    end;
    writeln(s);
    close(input);
    close(output);
end.
