#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <iostream>
using namespace std;
#define gc() getchar()
template <typename tp>
inline void read(tp& x) {
  x = 0; char tmp; bool key = 0;
  for (tmp = gc() ; !isdigit(tmp) ; tmp = gc())
    key = (tmp == '-');
  for ( ; isdigit(tmp) ; tmp = gc())
    x = (x << 3) + (x << 1) + tmp - '0';
  if (key) x = -x;
}
const int N = 100010, MP = 20;
int mn[N][MP], ln[N], n, a[N];
long long ans;
int query(int l,int r) {
  int len = ln[r - l + 1];
  return a[mn[r][len]] < a[mn[l + (1 << len) - 1][len]] ?
    mn[r][len] : mn[l + (1 << len) - 1][len];
}
void solve(int l,int r,int cur) {
  if (l > r) return;
  int p = query(l,r);
  ans += a[p] - cur;
  solve(l,p-1,a[p]);
  solve(p+1,r,a[p]);
}
int main() {
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
  read(n);
  for (int i = 1 ; i <= n ; ++ i)
    read(a[i]);
  for (int i = 1 ; i <= n ; ++ i) {
    mn[i][0] = i;
    for (int j = 1 ; (1 << j) <= i ; ++ j)
      mn[i][j] = a[mn[i][j-1]] < a[mn[i - (1 << j >> 1)][j-1]] ?
				 mn[i][j-1] : mn[i - (1 << j >> 1)][j-1];
  }
  for (int i = 2 ; i <= n ; i <<= 1)
    ln[i] ++;
  for (int i = 2 ; i <= n ; ++ i)
    ln[i] += ln[i-1];
  solve(1,n,0);
  printf("%lld\n",ans);
  return 0;
}
