#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <iostream>
using namespace std;
#define gc() getchar()
template <typename tp>
inline void read(tp& x) {
  x = 0; char tmp; bool key = 0;
  for (tmp = gc() ; !isdigit(tmp) ; tmp = gc())
    key = (tmp == '-');
  for ( ; isdigit(tmp) ; tmp = gc())
    x = (x << 3) + (x << 1) + tmp - '0';
  if (key) x = -x;
}
const int N = 110, MAX = 25010;
int n,a[N],vis[MAX];
long long cur[MAX];
inline void ckmn(long long &x,long long y) {
  x = x > y ? y : x;
}
void doit(int st,int v) {
  int key = 0;
  for (int i = st ; i != st || (!key) ; i = (i + v) % a[1]) {
    vis[i] = 1;
    ckmn(cur[(i + v) % a[1]], cur[i] + v);
    if (i == st) key = 1;
  }
}
void solve() {
  int ans;
  read(n);
  for (int i = 1 ; i <= n ; ++ i)
    read(a[i]);
  sort(a+1,a+n+1);
  memset(cur,0x3f,sizeof cur);
  cur[0] = 0;
  ans = 1;
  for (int i = 2 ; i <= n ; ++ i) {
    if (cur[a[i] % a[1]] <= a[i]) continue;
    ++ ans;
    memset(vis,0,sizeof vis);
    for (int j = 0 ; j < a[1] ; ++ j) {
      if (!vis[j]) {
	doit(j,a[i]);
	doit(j,a[i]);
      }
    }
  }
  printf("%d\n",ans);
}
int main() {
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  int T;
  read(T);
  while (T --)
    solve();
  return 0;
}
