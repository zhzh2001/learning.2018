#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <vector>
#include <set>
using namespace std;
#define gc() getchar()
template <typename tp>
inline void read(tp& x) {
  x = 0; char tmp; bool key = 0;
  for (tmp = gc() ; !isdigit(tmp) ; tmp = gc())
    key = (tmp == '-');
  for ( ; isdigit(tmp) ; tmp = gc())
    x = (x << 3) + (x << 1) + tmp - '0';
  if (key) x = -x;
}
const int N = 50010;
int n,m;
struct edge {
  int la,b,v;
} con[N << 1];
int tot,fir[N];
void add(int from,int to,int val) {
  con[++tot] = (edge) {fir[from],to,val};
  fir[from] = tot;
}
int cur,num,res[N],cnt,vec[N],ano[N],acnt;
void dfs(int pos,int fa) {
  res[pos] = 0;
  for (int i = fir[pos] ; i ; i = con[i].la) {
    if (con[i].b == fa) continue;
    dfs(con[i].b,pos);
  }
  cnt = acnt = 0;
  for (int i = fir[pos] ; i ; i = con[i].la) {
    if (con[i].b == fa) continue;
    if (res[con[i].b] + con[i].v >= cur)
      ++ num;
    else vec[++cnt] = res[con[i].b] + con[i].v;
  }
  sort(vec+1,vec+cnt+1);
  int p = cnt;
  for (int i = 1 ; i <= p ; ++ i) {
    if (vec[p] + vec[i] < cur || i >= p) {
      if (acnt) -- acnt, ++ num;
      else res[pos] = max(res[pos],vec[i]);
    } else {
      while (p - 1 > i && vec[p - 1] + vec[i] >= cur) {
	ano[++acnt] = vec[p];
	-- p;
      }
      ++ num;
      -- p;
    }
  }
  num += acnt / 2;
  if (acnt&1) res[pos] = max(res[pos], ano[1]);
}
bool check(int d) {
  cur = d;
  num = 0;
  dfs(1,0);
  // cout << res[1] << endl;
  return num >= m;
}
int main() {
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  int x,y,z,l=0,r=0;
  read(n), read(m);
  for (int i = 1 ; i < n ; ++ i) {
    read(x), read(y), read(z);
    add(x,y,z);
    add(y,x,z);
    r += z;
  }
  int mid, ans = 0;
  while (l <= r) {
    mid = (l + r) >> 1;
    if (check(mid))
      l = mid + 1, ans = mid;
    else r = mid - 1;
  }
  printf("%d\n",ans);
  return 0;
}
