#include<bits/stdc++.h>
using namespace std;
const int N=2e5+5;
int Head[N],Next[N],vet[N],val[N];
int n,m,edgenum,u,v,w,ans,len;
void addedge(int u, int v, int w){
	edgenum++;
	vet[edgenum]=v;
	val[edgenum]=w;
	Next[edgenum]=Head[u];
	Head[u]=edgenum;
}
namespace S1{
	int dp[N];
	void dfs(int u, int f){
		int Max_ist=0,Max_sec=0;
		for (int e=Head[u]; e; e=Next[e]){
			int v=vet[e];
			if (v==f) continue;
			dfs(v,u);
			int k=dp[v]+val[e];
			if (k>Max_ist){
				Max_sec=Max_ist;
				Max_ist=k;
			} else
			if (k>Max_sec) Max_sec=k;
		}
		ans=max(ans,Max_ist+Max_sec);
		dp[u]=Max_ist;
	}
	void solve1(){
		dfs(1,0);
		printf("%d\n",ans);
	}
}
namespace S2{
	bool check(int u, int f, int len, int now, int sz){
		int x=now,y=sz;
		if (x>len) y++,x=0;
		if (y>=m) return 1;
		for (int e=Head[u]; e; e=Next[e]){
			int v=vet[e];
			if (v==f) continue;
			if (check(v,u,len,x+val[e],y)) return 1;
		}
		return 0;
	}
	void solve2(){
		int l=0,r=len;
		while (l<r){
			int mid=(l+r)>>1;
			if (!check(1,0,mid,0,0)) r=mid;
			else l=mid+1;
		}
		printf("%d\n",l);
	}	
}
namespace S4{
	bool vis[N];
	int cost[N];
	void dfs(int u, int f){
		for (int e=Head[u]; e; e=Next[e]){
			int v=vet[e];
			if (v==f) continue;
			cost[v]=val[e];
			dfs(v,u);
		}
	}
	bool check(int len){
		memset(vis,0,sizeof(vis));
		int cnt=0;
		for (int i=2; i<=n; i++){
			int delta=1e9,loc=0;
			for (int j=1; j<i; j++){
				if (vis[j]) continue;
				if (cost[i]+cost[j]-len>=0 && cost[i]+cost[j]-len<delta){
					delta=cost[i]+cost[j]-len;
					loc=j;
				}
			}
			if (loc) vis[i]=vis[loc]=1,cnt++;
			if (cnt>=m) return 1;
		}
		return 0;
	}
	void solve4(){
		dfs(1,0);
		int l=0,r=len;
		while (l<r){
			int mid=(l+r)>>1;
			if (!check(mid)) r=mid;
			else l=mid+1;
		}
		printf("%d\n",l);	
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	bool flag=0,flag2=0;
	scanf("%d%d",&n,&m);
	for (int i=1; i<n; i++){
		scanf("%d%d%d",&u,&v,&w); len+=w;
		if (u+1!=v) flag=1;
		if (u!=1) flag2=1;
		addedge(u,v,w); addedge(v,u,w);
	}	
	if (m==1) S1::solve1();
	else if (!flag) S2::solve2();	
	else if (!flag2) S4::solve4();
	return 0;
}
/*
7 1
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7

8 3
1 2 7
2 3 6
3 4 8
4 5 3
5 6 5
6 7 4
7 8 6

15 6
1 2 7
1 3 8
1 4 6
3 5 2
3 6 5
4 7 9
7 8 7
7 9 3
8 10 4
8 11 5
8 12 8
12 13 7
12 14 9
12 15 6

15 4
1 2 5
1 3 7
1 4 6
1 5 4
1 6 9
1 7 8
1 8 5
1 9 2
1 10 7
1 11 6
1 12 8
1 13 7
1 14 5
1 15 9
*/
