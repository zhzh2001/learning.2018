#include<bits/stdc++.h>
using namespace std;
const int N=2e5+5;
int a[N];
int T,n,tot,Max;
bool dp[N];
void init(){
	tot=Max=0;
	memset(a,0,sizeof(a));
	memset(dp,0,sizeof(dp));
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		init();
		scanf("%d",&n);
		for (int i=1; i<=n; i++) scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		Max=a[n]; dp[0]=1;
		for (int i=1; i<=n; i++)
			if (!dp[a[i]]){
				tot++;
				for (int j=a[i]; j<=Max; j++)
					dp[j]|=dp[j-a[i]];
			}
		printf("%d\n",tot);
	}
	return 0;
}
/*
2
4
3 19 10 6
5
11 29 13 19 17
*/
