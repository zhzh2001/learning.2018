#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdio>

using namespace std;

int ff[1001][101],to[2001],first[2001],next[2001],w[2001];
int n,m,x[1001],y[1001],cnt[1001],e1=0,deep[1001];
int dep[1001],ans=0;

void insert(int o,int p,int q)
{
	to[e1]=p;w[e1]=q;next[e1]=first[o];first[o]=e1++;
}

void dfs1(int o,int p)
{
	ff[o][0]=p;deep[o]=deep[p]+1;
	for(int i=first[o];i!=-1;i=next[i])
		if(to[i]!=p)
		{
			dep[to[i]]=dep[o]+w[i];
			dfs1(to[i],o);
		}
}

void dfs2(int o,int p)
{
	for(int i=first[o];i!=-1;i=next[i])
		if(to[i]!=p) 
		{
			dfs2(to[i],o);
			cnt[o]+=cnt[to[i]];
		}
}

int LCA(int o,int p)
{
	if(deep[o]<deep[p]) swap(o,p);
	int yu=deep[o]-deep[p];
	for(int i=10;i>=0;i--) if((yu>>i)&1) o=ff[o][i];
	for(int i=10;i>=0;i--)
		if(ff[o][i]!=ff[p][i]) o=ff[o][i],p=ff[p][i];
	return (o==p)?o:ff[o][0];
}

void calc()
{
	memset(cnt,0,sizeof(cnt));
	for(int i=1;i<=m;i++)
	{
		cnt[x[i]]++;cnt[y[i]]++;
		cnt[LCA(x[i],y[i])]-=2;
	}
	dfs2(1,0);
	for(int i=1;i<=n;i++) if(cnt[i]>1) return;
	int yu=1000000000;
	for(int i=1;i<=m;i++)
		yu=min(yu,dep[x[i]]+dep[y[i]]-2*dep[LCA(x[i],y[i])]);
	ans=max(ans,yu);
}

void dfs(int o)
{
	if(o>m) {calc();return;}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			x[o]=i;y[o]=j;
			dfs(o+1);
		}
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track1.out","w",stdout);
	memset(first,-1,sizeof(first));
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++)
	{
		int x,y,z;scanf("%d%d%d",&x,&y,&z);
		insert(x,y,z);insert(y,x,z);
	}
	dfs1(1,0);
	for(int i=1;i<=10;i++)
		for(int j=1;j<=n;j++) ff[i][j]=ff[ff[i][j-1]][j-1];
	dfs(1);
	cout<<ans<<endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
}
