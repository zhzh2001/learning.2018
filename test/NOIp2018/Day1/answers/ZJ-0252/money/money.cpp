#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdio>

using namespace std;

int n,ans,a[100001];
bool f[100001];

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;cin>>T;
	while(T--)
	{
		scanf("%d",&n);
		for(int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		for(int i=0;i<=25000;i++) f[i]=0;
		f[0]=1;ans=n;
		for(int i=1;i<=n;i++)
		{
			if(f[a[i]]) ans--;
			for(int j=a[i];j<=25000;j++) f[j]|=f[j-a[i]];
		}
		cout<<ans<<endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
