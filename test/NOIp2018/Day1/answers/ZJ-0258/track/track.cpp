#include<bits/stdc++.h>
using namespace std;
#define M 50005
struct hh{
	int p,w;
};
vector<hh>d[M];
int mx1[M],mx2[M],cnt,n,m;
void bs(int x,int f){
	for(int i=0;i<(int)d[x].size();++i){
		int v=d[x][i].p;
		if(v!=f){
			bs(v,x);
			if(mx1[v]+d[x][i].w>mx1[x])mx2[x]=mx1[x],mx1[x]=mx1[v]+d[x][i].w;
			else if(mx1[v]+d[x][i].w>mx2[x])mx2[x]=mx1[v]+d[x][i].w;
		}
	}
}
vector<int>q[M];
int mk[M],s=0,sk[M],dp[1005][1005],ls[1005],mx[1005][1005],a[1005];
bool cmp(int a,int b){
	return a>b;
}
void dfs(int x,int f,int w){
	dp[x][0]=0;
	for(int i=0;i<(int)d[x].size();++i){
		int v=d[x][i].p;
		if(v!=f){
			dfs(v,x,w);
			for(int j=0;j<=n-1;++j)ls[j]=dp[x][j],a[j]=mx[x][j];
			for(int j=0;j<=n-1;++j){
				if(dp[x][j]==-1)break;
				for(int k=0;k<=n-1;++k){
					if(dp[v][k]==-1)break;
					if(dp[v][k]+d[x][i].w+mx[x][j]>=w)ls[j+k+1]=max(ls[j+k+1],dp[x][j]);
					else if(dp[v][k]+d[x][i].w+dp[x][j]>=w)ls[j+k+1]=max(ls[j+k+1],mx[x][j]);
					int he=dp[v][k]+d[x][i].w;
					if(he>=ls[j+k])a[j+k]=ls[j+k],ls[j+k]=he;
					else a[j+k]=max(a[j+k],he);
				}
			}
			for(int j=0;j<=n-1;++j)dp[x][j]=ls[j],mx[x][j]=a[j];
		}
	}
}
bool check(int x){
	memset(dp,-1,sizeof(dp));
	memset(mx,0,sizeof(mx));
	dfs(1,0,x);
	return dp[1][m]!=-1;
}
bool cmp1(hh a,hh b){
	return a.w>b.w;
}
void s40(){
	sort(d[1].begin(),d[1].end(),cmp1);
	int r=m-1;
	for(int i=m;i<(int)d[1].size();++i){
		int v=d[1][i].w;
		d[1][r--].w+=v;
		if(r<0)break;
	}
	sort(d[1].begin(),d[1].begin()+m,cmp1);
	printf("%d",d[1][m-1].w);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	bool f=1;
	scanf("%d %d",&n,&m);
	for(int i=1,a,b,c;i<n;++i){
		scanf("%d %d %d",&a,&b,&c);
		d[a].push_back((hh){b,c});
		d[b].push_back((hh){a,c});
		if(a!=1&&b!=1)f=0;
	}
	bs(1,0);
	if(m==1){
		printf("%d\n",mx1[1]+mx2[1]);
		return 0;
	}
	else if(f)s40();
	else{
		int l=1,r=mx1[1]+mx2[1],ans=0;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}
