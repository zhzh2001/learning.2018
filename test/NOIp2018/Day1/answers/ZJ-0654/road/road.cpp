#include<bits/stdc++.h>
using namespace std;
#define LL long long
const LL MAXN=static_cast<int>(1e5)+100;
const LL INF=0x3f3f3f3f3f3f3f3f; 
#define F(x,y,z) for(int x=y;x<=z;x++)
#define D(x,y,z) for(int x=y;x>=z;x--) 
inline LL read(){
	LL c=getchar(),x=0,y=1;
	while(c>'9'||c<'0'){
		if(c=='-'){
			y=-1;
		}
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*y;
}
/*xhk AK NOIp*/
LL road[MAXN];
inline bool check(LL n){
	F(i,1,n){
		if(road[i]!=0){
			return false;
		}		
	}
	return true;
}

signed main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	LL n=read();
	F(i,1,n){
		road[i]=read();
	}
	LL ans=0,mnn=INF,lft=1,rght=1;
	while(!check(n)){
		mnn=INF;
		F(i,1,n){
			if(road[i]!=0){
				rght++;
				mnn=min(mnn,road[i]);
			}else{
				if(mnn!=INF){
					ans+=mnn;
					F(i,lft,rght-1){
						road[i]-=mnn;
					}
				}
				lft=i+1;
				rght=i+1;
				mnn=INF;
			}
		}
		if(mnn!=INF){
			ans+=mnn;
			F(i,lft,rght-1){
				road[i]-=mnn;
			}
		}
		lft=1,rght=1;
	}
	printf("%lld",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
