#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define int long long
const LL MAXN=static_cast<int>(1e5)+100;
const LL INF=0x3f3f3f3f3f3f3f3f;
#define ini(x,y) memset(x,y,sizeof(x)) 
#define F(x,y,z) for(int x=y;x<=z;x++)
#define D(x,y,z) for(int x=y;x>=z;x--) 
inline LL read(){
	LL c=getchar(),x=0,y=1;
	while(c>'9'||c<'0'){
		if(c=='-'){
			y=-1;
		}
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*y;
}
/*xhk AK NOIp*/
int n,m;
struct ROAD{
	int to,nxt,dis;
}edge[MAXN<<1];
int head[MAXN],top=0;
inline void addroad(int from,int to,int dis){
	edge[++top].nxt=head[from];
	edge[top].dis=dis;
	edge[top].to=to;
	head[from]=top;
}
namespace duliuCCF_pianfen1{
	int long1[30001],long2[30001],ans=0;
	inline void dfs(int x,int father){
		//cout<<x<<endl;
		if(long1[x]!=0){
			return;
		}
		for(int ptr=head[x];ptr!=0;ptr=edge[ptr].nxt){
			int v=edge[ptr].to,dis=edge[ptr].dis;
			if(v==father){
				continue;	
			}
			dfs(v,x);
			if(long1[v]+dis>long1[x]){
				long2[x]=long1[x];
				long1[x]=long1[v]+dis;
			}else{
				long2[x]=max(long2[x],long1[v]+dis);
			}
		}
		ans=max(ans,long1[x]+long2[x]);
	}
	inline void duliuCCF_pianfen1(){
		ini(long1,0);
		ini(long2,0);
		F(i,1,n-1){
			int from=read(),to=read(),dis=read();
			addroad(from,to,dis);
			addroad(to,from,dis);
		}
		dfs(1,0);
		printf("%lld",ans);
	}
} 
namespace sha_bi_CCF_jin_sai_yu_yao_xuan_shou_pianfen2{
	inline void sha_bi_CCF_jin_sai_yu_yao_xuan_shou_pianfen2(){
		vector<int> lu;
		F(i,1,top){
			if(edge[i].to!=1){
				lu.push_back(edge[i].dis);
			}
		}
		sort(lu.begin(),lu.end(),greater<int>());
		int cnt=m-1;
		for(int i=m;i<lu.size();i++){
			if(cnt<0){
				break;
			}
			lu[cnt]+=lu[i];
			cnt--;
		}
		sort(lu.begin(),lu.end(),greater<int>());
		printf("%lld",lu[m-1]);
	}
}
namespace sha_bi_CCF_shandong_xuanshou_jiayou_pianfen3{
	int dis[MAXN];
	int check(int len){
		int now=0,cnt=0;
		F(i,1,n-1){
			now+=dis[i];
			if(now>=len){
				now=0;
				cnt++;
			}
		}
		if(now>=len){
			now=0;
			cnt++;
		}
		if(cnt>=m){
			return true;
		}
		return false;
	}
	inline void dfs(int x,int father){
		for(int ptr=head[x];ptr!=0;ptr=edge[ptr].nxt){
			if(edge[ptr].to!=father){
				dis[x]=edge[ptr].dis;
				dfs(edge[ptr].to,x);
			}
		}
	}
	inline void sha_bi_CCF_shandong_xuanshou_jiayou_pianfen3(){
		dfs(1,0);
		int left=0,right=INF,mid; 
		while(left!=right){
			mid=(left+right)>>1;
			if(check(mid)){
				left=mid;
			}else{
				right=mid-1;
			}
		}
		printf("%lld",left);
	}
}
signed main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	bool pianfen_ai_1=true,pianfen_aibi1=true;
	if(m==1){
		duliuCCF_pianfen1::duliuCCF_pianfen1();
	}else{
		F(i,1,n-1){
			int from=read(),to=read(),dis=read();
			if(from!=1&&to!=1){
				pianfen_ai_1=false;
			}
			if(abs(from-to)!=1){
				pianfen_aibi1=false;
			}
			addroad(from,to,dis);
			addroad(to,from,dis);
		}
		if(pianfen_ai_1){
			sha_bi_CCF_jin_sai_yu_yao_xuan_shou_pianfen2::sha_bi_CCF_jin_sai_yu_yao_xuan_shou_pianfen2();
		}else{
			if(pianfen_aibi1){
				sha_bi_CCF_shandong_xuanshou_jiayou_pianfen3::sha_bi_CCF_shandong_xuanshou_jiayou_pianfen3();
			}else{
				
			}
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
