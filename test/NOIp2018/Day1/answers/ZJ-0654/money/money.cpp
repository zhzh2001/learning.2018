#include<bits/stdc++.h>
using namespace std;
#define int long long
#define LL long long 
const LL MAXN=static_cast<int>(1e2)+100;
const LL INF=0x3f3f3f3f3f3f3f3f; 
#define F(x,y,z) for(int x=y;x<=z;x++)
#define D(x,y,z) for(int x=y;x>=z;x--) 
inline LL read(){
	LL c=getchar(),x=0,y=1;
	while(c>'9'||c<'0'){
		if(c=='-'){
			y=-1;
		}
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*y;
}
/*xhk AK NOIp*/
int mny[MAXN];
bool flag=false;
inline void check(int flr,int nowvalue,const int& num){
	if(flr>=num){
		return;
	}
	int time=0;
	while(nowvalue+time*mny[flr]<=mny[num]){
		if(flag){
			return;
		}
		if(nowvalue+time*mny[flr]==mny[num]){
			flag=true;
			return;
		}else{
			check(flr+1,nowvalue+time*mny[flr],num);
		}
		time++;
	}
}
signed main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--){
		int n=read();
		F(i,1,n){
			mny[i]=read();
		}
		sort(mny+1,mny+1+n);
		int ans=n;
		F(i,1,n){
			flag=false;
			check(1,0,i);
			if(flag){
				ans--;
			}
		}
		printf("%lld\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
