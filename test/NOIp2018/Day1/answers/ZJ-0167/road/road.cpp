#if 1
#include<iostream>
#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<map>
#include<set>
#include<queue>
#include<vector>
#include<stdlib.h>
#else
#include<bits/stdc++.h>
#endif

#if 0
#include<cmath>
#endif

#define REP(i, l, r) for (int i = (l); i <= (r); ++i)
#define RREP(i, r, l) for (int i = (r); i >= (l); --i)
#define rep(i, l, r) for (int i = (l); i < (r); ++i)
#define rrep(i, r, l) for (int i = (r); i > (l); --i)
#define foredge(i, u) for (int i = la[u]; i; i = ne[i])
#define mem(a) memset(a, 0, sizeof(a))
#define memid(a) memset(a, 0x3f, sizeof(a))
#define memax(a) memset(a, 0x7f, sizeof(a))
#define dbg(x) cout << #x << " = " << x << endl
#define tpn typename
#define fr(a) freopen(a, "r", stdin)
#define fw(a) freopen(a, "w", stdout)

using namespace std;

typedef long long ll;

template<tpn A> inline A Max(const A &x,const A &y){
	return x>y?x:y;
}
template<tpn A> inline A Min(const A &x,const A &y){
	return x<y?x:y;
}
template<tpn A> inline void Swap(A &x,A &y){
	x ^= y, y ^= x, x ^= y;
}
template<tpn A> inline A Abs(const A &x){
	return x<0?-x:x;
}
template<tpn A> inline void read(A &x){
	A neg=1;
	char c;
	do c=getchar();
	while ((c<'0'||c>'9')&&c!='-');
	if (c=='-') neg=-1,c=getchar();
	x=0;
	do{
		x=x*10+c-48;
		c=getchar();
	}while (c>='0'&&c<='9');
	x*=neg;
}
const int N = 100005;
int n, a[N];
int temp[N], top;
int ans;
int main(){
	fr("road.in");
	fw("road.out");
	read(n);
	REP(i, 1, n){
		read(a[i]);
		if (temp[top] > a[i]) ans += temp[top] - a[i];
		while (top && a[i] <= temp[top]) --top;
		temp[++top] = a[i];
	}
	ans += temp[top];
	cout << ans << endl;
	return 0;
}
