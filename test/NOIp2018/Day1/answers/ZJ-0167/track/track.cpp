#if 1
#include<iostream>
#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<map>
#include<set>
#include<queue>
#include<vector>
#include<stdlib.h>
#else
#include<bits/stdc++.h>
#endif

#if 0
#include<cmath>
#endif

#define REP(i, l, r) for (int i = (l); i <= (r); ++i)
#define RREP(i, r, l) for (int i = (r); i >= (l); --i)
#define rep(i, l, r) for (int i = (l); i < (r); ++i)
#define rrep(i, r, l) for (int i = (r); i > (l); --i)
#define foredge(i, u) for (int i = la[u]; i; i = ne[i])
#define mem(a) memset(a, 0, sizeof(a))
#define memid(a) memset(a, 0x3f, sizeof(a))
#define memax(a) memset(a, 0x7f, sizeof(a))
#define dbg(x) cout << #x << " = " << x << endl
#define tpn typename
#define fr(a) freopen(a, "r", stdin)
#define fw(a) freopen(a, "w", stdout)

using namespace std;

typedef long long ll;

template<tpn A> inline A Max(const A &x,const A &y){
	return x>y?x:y;
}
template<tpn A> inline A Min(const A &x,const A &y){
	return x<y?x:y;
}
template<tpn A> inline void Swap(A &x,A &y){
	x ^= y, y ^= x, x ^= y;
}
template<tpn A> inline A Abs(const A &x){
	return x<0?-x:x;
}
template<tpn A> inline void read(A &x){
	A neg=1;
	char c;
	do c=getchar();
	while ((c<'0'||c>'9')&&c!='-');
	if (c=='-') neg=-1,c=getchar();
	x=0;
	do{
		x=x*10+c-48;
		c=getchar();
	}while (c>='0'&&c<='9');
	x*=neg;
}
template<tpn A> inline void read(A &x,A &y){
	read(x),read(y);
}
template<tpn A> inline void read(A &x,A &y,A &z){
	read(x),read(y),read(z);
}
const int N = 50005;
int la[N], ne[N << 1], en[N << 1], w[N << 1], top;
inline void add(int x,int y,int z){
	ne[++top] = la[x];
	en[top] = y;
	w[top] = z;
	la[x] = top;
}
int n, m, x, y, z;
int l, r, mid, ans;
namespace task{
	int len, tot;
	int f[N];
	int temp[N], cnt, used[N];
	int touse[N], num;
	void dfs(int u,int p){
		foredge(i, u)
		if (en[i] != p){
			dfs(en[i], u);
		}
		cnt = num = 0;
		foredge(i, u)
		if (en[i] != p){
			temp[++cnt] = f[en[i]] + w[i];
			used[cnt] = 0;
		}
		sort(temp + 1, temp + cnt + 1);
		int st, en;
		st = en = cnt;
		REP(i, 1, cnt){
			if (temp[i] >= len){
				st = en = i - 1;
				break;
			}
		}
		tot += cnt - st;
		REP(i, 1, st){
			if (used[i]) continue;
			while (en && temp[i] + temp[en] >= len){
				if (!used[en]) touse[++num] = en;
				--en;
			}
			while (num && touse[num] <= i) --num;
			if (num){
				used[i] = used[touse[num]] = 1;
				--num;
				++tot;
			}
		}
		RREP(i, st, 1){
			if (!used[i]){
				f[u] = temp[i];
				break;
			}
		}
	}
	inline bool judge(int x){
		tot = 0;
		len = x;
		mem(f);
		dfs(1, 0);
		if (tot >= m) return 1;
		return 0;
	}
}
int main(){
	fr("track.in");
	fw("track.out");
	read(n, m);
	rep(i, 1, n){
		read(x, y, z);
		add(x, y, z);
		add(y, x, z);
	}
	l = 1, r = 500000000;
	//if (task::judge(31)){
	//	return 0;
	//}
	while (l <= r){
		mid = l + r >> 1;
		if (task::judge(mid)){
			ans = mid;
			l = mid + 1;
		}
		else r = mid - 1;
	}
	cout << ans << endl;
	return 0;
}
