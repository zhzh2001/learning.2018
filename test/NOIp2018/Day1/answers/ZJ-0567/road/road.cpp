#include<bits/stdc++.h>
using namespace std;

const int MAXN=1e5+2333;

int n;
int d[MAXN];

int read(){
	int x=0,f=1; char ch=getchar();
	while (!isdigit(ch)) { if (ch=='-') f=-1; ch=getchar(); }
	while (isdigit(ch)) x=x*10+ch-'0',ch=getchar(); return x*f;
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++)
		d[i]=read();
	int ans=0;
	for (int i=1;i<=n;i++) 
		if (d[i]>d[i-1]) 
			ans+=d[i]-d[i-1];
/*	for (int i=1;i<=10000;i++)
		for (int j=1,k;j<=n;j=k+1) {
			k=j;
			if (d[k]<i) continue;
			while (k<=n&&d[k]>=i) k++;
			ans++;
		}*/
	printf("%d\n",ans);
	return 0;
}
