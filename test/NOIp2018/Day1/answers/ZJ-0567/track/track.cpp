#include<bits/stdc++.h>
using namespace std;

#define max(a,b) ((a)>(b) ? (a):(b))

const int MAXN=5e4+2333;

int n,m,ans,cnt,maxd;
int head[MAXN],dis[MAXN],d[MAXN];

struct Edge {
	int v,w,nxt;
}e[MAXN];

int read() {
	int x=0,f=1; char ch=getchar();
	while (!isdigit(ch)) { if (ch=='-') f=-1; ch=getchar(); }
	while (isdigit(ch)) x=x*10+ch-'0',ch=getchar(); return x*f;
}

void add_edge(int u,int v,int w) {
	e[++cnt].nxt=head[u];
	head[u]=cnt;
	e[cnt].v=v;
	e[cnt].w=w;
}

void DFS(int x,int p) {
	for (int i=head[x];i;i=e[i].nxt) 
		if (e[i].v!=p) {
			dis[e[i].v]=dis[x]+e[i].w;
			DFS(e[i].v,x);
		}
}

void solveOne() { // m = 1
	DFS(1,0);
	int st=1,mxt=0;
	for (int i=1;i<=n;i++)
		if (dis[i]>mxt) 
			st=i,mxt=dis[i];
	memset(dis,0,sizeof(dis));
	DFS(st,0);
	mxt=0;
	for (int i=1;i<=n;i++)	
		mxt=max(mxt,dis[i]);
	ans=mxt;
}

int cnt_ln,val[MAXN];

void Find_L(int x,int p) {
	for (int i=head[x];i;i=e[i].nxt){
		if (e[i].v!=p) {
			val[++cnt_ln]=e[i].w;
			Find_L(e[i].v,x);
		}
	}
}

bool check(int x) {
	int tmp=0;
	for (int i=1,j,now;i<n;i=j+1) {
		now=val[i],j=i;
		while (j+1<n&&now<x) now+=val[++j];
		if (now>=x) tmp++;
		if (tmp>=m) return true;
	}
	return false;
}

void solveTwo() { // b[i] = a[i] + 1
	int st=0;
	for (int i=1;i<=n;i++)
		if (d[i]==1) {
			st=i;
			break;
		}
	Find_L(st,0);
	int l=1,r=0x3f3f3f3f,mid,res;
	while (l<=r) {
		mid=(l+r)>>1;
		check(mid) ? res=mid,l=mid+1:r=mid-1;
	}
	ans=res;
}

int evl[MAXN],ich[MAXN];

void solveThree() { // a[i] = 1
	sort(evl+1,evl+n);
	int mid=2*n-2*m-1;
	for (int i=n-m;i<n;i++)
		ich[i-(n-m)+1]=evl[i]+evl[max(0,mid-i)];
	sort(ich+1,ich+m+1);
	ans=ich[1];
}

typedef pair<int,int> P;

P f[1233][1233];

void FSD(int x,int p) {	// QAQ
	f[x][0]=P(0x3f3f3f3f,0);
	for (int i=head[x],v;i;i=e[i].nxt) {
		v=e[i].v;
		if (v!=p) {
			FSD(v,x);
			f[x][0]=P(0x3f3f3f3f,f[v][0].second+e[i].v);
			for (int j=1;j<=m;j++) {
				for (int o=0;o<=j;o++) 
					f[x][j]=P(max(f[x][j].first,min(f[x][o].first,f[v][j-o].first)),max(f[x][j].second,max(f[x][o].second,f[v][j-o].second+e[i].v)));
				if (min(f[x][j-1].first,f[x][j].second)>f[x][j].first)
					f[x][j]=P(min(f[x][j-1].first,f[x][j-1].second),0);
			}
		}
	}
}

void solveFour() {
	FSD(1,0);
	ans=f[1][m].first;
}

int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	bool TP=true;
	for (int i=1,u,v,w;i<n;i++) {
		u=read(),v=read(),w=read();
		d[u]++,d[v]++;
		evl[i]=w;
		TP&=((u==1)|(v==1));
		maxd=max(d[u],maxd);
		maxd=max(d[v],maxd);
		add_edge(u,v,w);
		add_edge(v,u,w);
	}
	if (m==1) solveOne();
	else if (maxd<=2) solveTwo();
	else if (TP) solveThree();
	else solveFour();
	printf("%d\n",ans);
	return 0;
}

/*
7 1
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7
*/

/*
6 1
1 2 1
2 3 2
3 4 3
4 5 4
5 6 5
*/

