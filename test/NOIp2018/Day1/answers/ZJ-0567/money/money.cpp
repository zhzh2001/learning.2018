#include<bits/stdc++.h>
using namespace std;

const int MAXN=1e3+233;
const int maxA=3e4+233;

int T,n,ans;
int a[MAXN];
int f[maxA];

int read(){
	int x=0,f=1; char ch=getchar();
	while (!isdigit(ch)) { if (ch=='-') f=-1; ch=getchar(); }
	while (isdigit(ch)) x=x*10+ch-'0',ch=getchar(); return x*f;
}
/*
void TRY1() {
	f[0]=1;
	for (int i=1;i<=n;i++)
		for (int j=a[i];j<=a[1]*a[2]-a[1]-a[2];j++)
			f[j]|=f[j-a[i]];
	for (int i=1;i<=a[1]*a[2]-a[1]-a[2];i++)
		if (!f[i])
			cout << i << ' ';			
}

void TRY2() {
	ans=0;
	memset(vis,0,sizeof(vis));
	for (int i=1;i<=n;i++)
		for (int j=i+1;j<=n;j++)
			if (a[j]%a[i]==0)
				vis[j]=1;
	int tmp1=0,tmp2=0;
	for (int i=1;i<=n;i++)
		if (!vis[i]){
			if (!tmp1)
				tmp1=a[i];
			else if (!tmp2)
				tmp2=a[i];
			else 
				break;
		}
	for (int i=1;i<=n;i++)
		if (a[i]>tmp1*tmp2-tmp1-tmp2)
			vis[i]=1;
	for (int i=1;i<=n;i++)
		if (!vis[i]) {
			ans++;
		}
}

void TRY3() {
	memset(f,0,sizeof(f));
	f[0]=1;
	for (int i=1;i<=n;i++)
		if (!f[a[i]]) {
			ans++;
			for (int j=a[i];j<=25000;j++)
				f[j]|=f[j-a[i]];
		}
}

*/

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while (T--) {
		n=read();
		ans=0;
		for (int i=1;i<=n;i++)
			a[i]=read();
		sort(a+1,a+n+1);
		memset(f,0,sizeof(f));
		f[0]=1;
		for (int i=1;i<=n;i++)
		if (!f[a[i]]) {
			ans++;
			for (int j=a[i];j<=25000;j++)
				f[j]|=f[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}

/*
2 4
3 19 10 6
5
11 29 13 19 17
*/
