#include<vector>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 50005
using namespace std;
vector<int>p[N];
struct P{
	int x,id;
	bool operator < (const P &c)const{return x<c.x;}
}a[N];
int tot,nxt[N*2],lnk[N],son[N*2],w[N*2],n,m,M,f[N],g[N],fa[N],val[N],as,oo;
bool sys;
void add(int x,int y,int z){
	nxt[++tot]=lnk[x];lnk[x]=tot;son[tot]=y;w[tot]=z;
}
int get(int x){
	int res=0,L=1;
	for (int R=as;R>=1;R--){
		if (R==x)continue;
		while ((a[L].x+a[R].x<M||L==x)&&L<R)L++;
		if (L>=R)return res;
		if (sys){
		printf("%d   ",p[a[L].id].size()+p[a[R].id].size()+1);
		for (int i=0;i<p[a[R].id].size();i++)printf("%d ",p[a[R].id][i]);
		printf("%d ",oo);
		for (int i=p[a[L].id].size()-1;i>=0;i--)printf("%d ",p[a[L].id][i]);
		puts("");
		}
		res++; L++;
	} return res;
}
void dfs(int x){
	f[x]=0;
	for (int i=lnk[x];i;i=nxt[i])if (fa[x]!=son[i]){
		fa[son[i]]=x; val[son[i]]=w[i]; dfs(son[i]); f[x]+=f[son[i]];
	} as=0; oo=x;
	for (int i=lnk[x];i;i=nxt[i])if (fa[x]!=son[i])
		a[++as].x=g[son[i]],a[as].id=son[i];
	sort(a+1,a+as+1);
	int o=get(0);
	int L=1,R=as,aa=0;
	while (L<=R){
		int MM=(L+R)>>1;
		if (get(MM)==o){
			if (aa<MM)aa=MM;
			L=MM+1;
		}else R=MM-1;
	} sys=1; get(aa); sys=0;
	f[x]+=o; g[x]=a[aa].x+val[x];
	p[x]=p[a[aa].id]; p[x].push_back(x);
	if (g[x]>=M){
		f[x]++,g[x]=0;
		printf("%d   ",p[x].size()+1);
		for (int i=0;i<p[x].size();i++)printf("%d ",p[x][i]);
		printf("%d  ",fa[x]);
		puts(""); p[x].clear();
	}
}
bool check(){
	dfs(1);
	return (f[1]>=m);
}
int main(){
	freopen("track3.in","r",stdin);
	freopen("trackp.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++){
		int x,y,z; scanf("%d%d%d",&x,&y,&z);
		add(x,y,z); add(y,x,z);
	}
	M=26282; check();
}
