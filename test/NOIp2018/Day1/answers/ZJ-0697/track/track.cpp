#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 50005
using namespace std;
int tot,nxt[N*2],lnk[N],son[N*2],w[N*2],n,m,M,f[N],g[N],fa[N],val[N],as,a[N];
void add(int x,int y,int z){
	nxt[++tot]=lnk[x];lnk[x]=tot;son[tot]=y;w[tot]=z;
}
int get(int x){
	int res=0,L=1;
	for (int R=as;R>=1;R--){
		if (R==x)continue;
		while ((a[L]+a[R]<M||L==x)&&L<R)L++;
		if (L>=R)return res;
		res++; L++;
	} return res;
}
void dfs(int x){
	f[x]=0;
	for (int i=lnk[x];i;i=nxt[i])if (fa[x]!=son[i]){
		fa[son[i]]=x; val[son[i]]=w[i]; dfs(son[i]); f[x]+=f[son[i]];
	} as=0;
	for (int i=lnk[x];i;i=nxt[i])if (fa[x]!=son[i])
		a[++as]=g[son[i]];
	sort(a+1,a+as+1);
	int o=get(0);
	int L=1,R=as,aa=0;
	while (L<=R){
		int MM=(L+R)>>1;
		if (get(MM)==o){
			if (aa<MM)aa=MM;
			L=MM+1;
		}else R=MM-1;
	}
	f[x]+=o; g[x]=a[aa]+val[x];
	if (g[x]>=M)f[x]++,g[x]=0;
}
bool check(){
	dfs(1);
	return (f[1]>=m);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++){
		int x,y,z; scanf("%d%d%d",&x,&y,&z);
		add(x,y,z); add(y,x,z);
	}
	int L=0,R=500000000,ans=0;
	while (L<=R){
		M=(L+R)>>1;
		if (check()){
			if (ans<M)ans=M;
			L=M+1;
		}else R=M-1;
	}
	printf("%d\n",ans);
}
