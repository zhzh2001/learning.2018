#include<vector>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
vector<int>a[10005];
bool vis[100005]; int n;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		int x; scanf("%d",&x);
		a[x].push_back(i);
	}
	int now=1,lst=0; long long ans=0; vis[0]=vis[n+1]=1;
	for (int i=0;i<=10000;i++)if (a[i].size()){
		ans+=(long long)(i-lst)*now;
		for (int j=0;j<a[i].size();j++){
			int k=a[i][j];
			if (vis[k-1]&&vis[k+1])now--;
			else if (!(vis[k-1]||vis[k+1]))now++;
			vis[k]=1;
		}
		lst=i;
	}
	printf("%lld\n",ans);
}
