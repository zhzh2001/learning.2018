#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
bool vis[25005];
int n,a[105];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		for (int i=1;i<=n;i++)scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		memset(vis,0,sizeof(vis));
		int ans=0; vis[0]=1;
		for (int i=1;i<=n;i++){
			if (vis[a[i]])continue;
			for (int j=a[i];j<=a[n];j++)
				vis[j]|=vis[j-a[i]];
			ans++;
		}
		printf("%d\n",ans);
	}
}
