#include <bits/stdc++.h>
using namespace std;
int maxa[30005],Nmaxa[30005],Next[70005],head[70005],to[70005],val[70005],edge,sum;
inline void add(int u,int v,int value)
{
	Next[++edge]=head[u];
	head[u]=edge;
	to[edge]=v;
	val[edge]=value;
}
int dfs(int be,int nowi)
{
	maxa[nowi]=0;
	Nmaxa[nowi]=0;
	for (int e=head[nowi];e;e=Next[e])
	if (to[e]!=be)
	{
		dfs(nowi,to[e]);
		if (maxa[to[e]]+val[e]>=maxa[nowi])
		{
			Nmaxa[nowi]=maxa[nowi];
			maxa[nowi]=maxa[to[e]]+val[e];
		}
		else
		{
			if (maxa[to[e]]+val[e]>=Nmaxa[nowi])
				Nmaxa[nowi]=maxa[to[e]]+val[e];
		}
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	if (m==1)
	{
		int x,y,z;
		for (int i=1;i<=n-1;++i)
		{
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
			add(y,x,z);
		}
		dfs(0,1);
		int maxq=-1;
		for (int i=1;i<=n;++i)
			if (maxa[i]+Nmaxa[i]>maxq)
				maxq=maxa[i]+Nmaxa[i];
		printf("%d\n",maxq);
	}
	else
	{
		int x,y,z;
		for (int i=1;i<=n-1;++i)
			scanf("%d%d%d",&x,&y,&z),sum+=z;
		printf("%d\n",(sum-y)/m);	
	}
	return 0;
}
