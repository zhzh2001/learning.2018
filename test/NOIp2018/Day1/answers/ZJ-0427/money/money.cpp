#include <bits/stdc++.h>
using namespace std;
int a[101],b[101],tot;
int pd(int p,int num)
{
	if (p==0)
		return 1;
	if (b[num]>p)
		return 0;
	if (num<=tot)
	{
		int ans=0;
		for (int i=0;i<=p/b[num];++i)
		{
			ans=ans|pd(p-(i*b[num]),num+1);
			if (ans==1)
				return 1;
		}
		return 0;
	}
	return 0;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T,n;
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		for (int i=1;i<=n;++i)
			scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		b[1]=a[1];
		tot=1;
		for (int i=2;i<=n;++i)
			if (!pd(a[i],1))
				b[++tot]=a[i];
		printf("%d\n",tot);
	}
	return 0;
}
