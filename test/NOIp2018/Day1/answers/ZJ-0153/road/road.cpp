#include<bits/stdc++.h>
#define R(i,s,t) for(int i=s;i<=t;i++)
using namespace std;
const int maxn=1e5+5;
int d[maxn],n,ans;
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	cin>>n;
	R(i,1,n) cin>>d[i];
	int s=1;
	for(;;) {
		while(!d[s]&&s<=n) s++;
		if(s==n+1) break;
		ans++;
		for(int i=s;i<=n&&d[i];i++)
			d[i]--;
	}
	cout<<ans<<endl;
	return 0;
}
