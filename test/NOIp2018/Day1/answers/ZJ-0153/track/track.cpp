#include<bits/stdc++.h>
#define R(i,s,t) for(int i=s;i<=t;i++)
#define GX(x,y) x=max(x,y)
using namespace std;
const int maxn=5e4+5;
int n,m;
struct Edge {
	int nxt,to,len;
}e[maxn<<1];
int hd[maxn],e_cnt,deg[maxn];
void add(int u,int v,int d) {
	e[++e_cnt].to=v;
	e[e_cnt].nxt=hd[u];
	hd[u]=e_cnt;
	e[e_cnt].len=d;
}
struct M_IS_ONE {
	int ans;
	void dfs(int u,int r,int dep) {
		GX(ans,dep);
		for(int i=hd[u];i;i=e[i].nxt)
			if(e[i].to!=r) dfs(e[i].to,u,dep+e[i].len);
	}
	void work() {
		R(i,1,n) dfs(i,0,0);
		cout<<ans<<endl;
		exit(0);
	}
}w1;
struct IS_A_CHAIN {
	int ans;
	void dfs(int u,int r,int d,int v) {
		if(d>=v) d=0,ans++;
		for(int i=hd[u];i;i=e[i].nxt)
			if(e[i].to!=r) dfs(e[i].to,u,d+e[i].len,v);
	}
	void work() {
		int l=1,r=5e8;
		while(l<=r) {
			int mid=l+r>>1;
			ans=0; dfs(1,0,0,mid);
			if(ans>=m) l=mid+1;
			else r=mid-1;
		}
		cout<<r<<endl;
		exit(0);
	}
}w2;
struct IS_A_FLOWER {
	int dis[maxn];
	bool used[maxn];
	bool chk(int l) {
		memset(used,0,sizeof used);
		int ans=0;
		for(int i=n-1;i>0;i--) {
			while(used[i]) i--;
			used[i]=true;
			if(dis[i]>=l) {ans++; continue;}
			int j;
			for(j=1;j<i;j++) if(!used[j]&&dis[j]+dis[i]>=l) break;
			if(j==i) break;
			ans++; used[j]=true;
		}
		return ans>=m;
	}
	void work() {
		int cnt=0;
		for(int i=hd[1];i;i=e[i].nxt)
			dis[++cnt]=e[i].len;
		sort(dis+1,dis+n);
		int l=1,r=5e8;
		while(l<=r) {
			int mid=l+r>>1;
			if(chk(mid)) l=mid+1;
			else r=mid-1;
		}
		cout<<r<<endl;
		exit(0);
	}
}w3;
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	R(i,1,n-1) {
		int a,b,l; cin>>a>>b>>l;
		add(a,b,l); add(b,a,l);
		deg[a]++; deg[b]++;
	}
	if(m==1) w1.work();
	bool flg=true;
	R(i,1,n) if(deg[i]>2) flg=false;
	if(flg) w2.work();
	w3.work();
	return 0;
}
