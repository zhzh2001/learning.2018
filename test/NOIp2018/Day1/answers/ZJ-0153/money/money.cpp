#include<bits/stdc++.h>
#define R(i,s,t) for(int i=s;i<=t;i++)
using namespace std;
int ans,a[105],n;
bool vis[25005];
void work(int w) {
	if(vis[a[w]]) {ans++; return;}
	R(i,0,a[n]-a[w]) if(vis[i]) for(int j=i+a[w];j<=a[n];j+=a[w]) vis[j]=true;
}
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T; cin>>T;
	while(T--) {
		memset(vis,false,sizeof vis);
		cin>>n; ans=0;
		R(i,1,n) cin>>a[i];
		sort(a+1,a+1+n);
		vis[0]=true;
		R(i,1,n) work(i);
		cout<<n-ans<<'\n';
	}
	return 0;
}
