#include<bits/stdc++.h>
using namespace std;
#define REP(i,f,t) for(int i=(f),i##_end_=(t);i<=i##_end_;++i)
#define SREP(i,f,t) for(int i=(f),i##_end_=(t);i<i##_end_;++i)
#define DREP(i,f,t) for(int i=(f),i##_end_=(t);i>=i##_end_;--i)
#define ll long long
template<class T>inline bool chkmin(T&x,T y){return x>y?x=y,1:0;}
template<class T>inline bool chkmax(T&x,T y){return x<y?x=y,1:0;}
template<class T>inline void rd(T&x){
	x=0;char c;int f=1;
	while((c=getchar())<48)if(c=='-')f=-1;
	do x=(x<<1)+(x<<3)+(c^48);
	while((c=getchar())>47);
	x*=f;
}

const int N=1e5+2;

int n;
int A[N];
int B[N];
vector<int>G[N];
bool mark[N];

struct pw{
	void solve(){
		REP(i,1,n) B[i]=A[i];
		
		sort(B+1,B+1+n);
		int len=unique(B+1,B+1+n)-B-1;
		
		REP(i,1,n) {
			A[i]=lower_bound(B+1,B+1+len,A[i])-B;
			G[A[i]].push_back(i);
//			printf("%d%c",A[i]," \n"[i==n]);
		}
		
		mark[0]=mark[n+1]=1;
		ll ans=0;
		int tot=1;
		REP(i,1,len){
			SREP(j,0,G[i-1].size()){
				int x=G[i-1][j];
				if(!mark[x-1] and !mark[x+1]) tot++;
				else if(mark[x-1] and mark[x+1]) tot--;
				mark[x]=1;
			}
			ans+=1ll*(B[i]-B[i-1])*tot; 
		}
		printf("%lld\n",ans);
	}
}p1;


int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	rd(n);
	REP(i,1,n) rd(A[i]);
	
	p1.solve();
	
	return 0;
}
