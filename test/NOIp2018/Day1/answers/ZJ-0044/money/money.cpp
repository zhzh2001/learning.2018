#include<bits/stdc++.h>
using namespace std;
#define REP(i,f,t) for(int i=(f),i##_end_=(t);i<=i##_end_;++i)
#define SREP(i,f,t) for(int i=(f),i##_end_=(t);i<i##_end_;++i)
#define DREP(i,f,t) for(int i=(f),i##_end_=(t);i>=i##_end_;--i)
#define ll long long
template<class T>inline bool chkmin(T&x,T y){return x>y?x=y,1:0;}
template<class T>inline bool chkmax(T&x,T y){return x<y?x=y,1:0;}
template<class T>inline void rd(T&x){
	x=0;char c;int f=1;
	while((c=getchar())<48)if(c=='-')f=-1;
	do x=(x<<1)+(x<<3)+(c^48);
	while((c=getchar())>47);
	x*=f;
}

const int N=102;

int n;
int A[N];

struct p65{
	int tmp[N];
	int now;
	int tot;
	bool dfs(int cnt,int sum){
		if(cnt==tot+1)return sum==now;
		for(int i=0;sum+i<=now;i+=tmp[cnt]) if(dfs(cnt+1,sum+i)) return 1;
		return 0;
	}
	
	void solve(){
		tot=0;
		tmp[++tot]=A[1];
		REP(i,2,n){
			now=A[i];
			if(!dfs(1,0)) tmp[++tot]=A[i];
		}
		printf("%d\n",tot);
	}
}p2;

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int cas;cin>>cas;
	while(cas--){
		rd(n);
		REP(i,1,n) rd(A[i]);
		sort(A+1,A+1+n);
		n=unique(A+1,A+1+n)-A-1;
		
		p2.solve();
	}
	return 0;
}
