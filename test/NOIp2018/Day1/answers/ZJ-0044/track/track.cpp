#include<bits/stdc++.h>
using namespace std;
#define REP(i,f,t) for(int i=(f),i##_end_=(t);i<=i##_end_;++i)
#define SREP(i,f,t) for(int i=(f),i##_end_=(t);i<i##_end_;++i)
#define DREP(i,f,t) for(int i=(f),i##_end_=(t);i>=i##_end_;--i)
#define ll long long
template<class T>inline bool chkmin(T&x,T y){return x>y?x=y,1:0;}
template<class T>inline bool chkmax(T&x,T y){return x<y?x=y,1:0;}
template<class T>inline void rd(T&x){
	x=0;char c;int f=1;
	while((c=getchar())<48)if(c=='-')f=-1;
	do x=(x<<1)+(x<<3)+(c^48);
	while((c=getchar())>47);
	x*=f;
}

const int N=5e4+2;

int n,m;

int qwq,head[N];
struct edge{
	int to,nxt,w;
}E[N<<1];
void addedge(int x,int y,int z){E[qwq]=(edge){y,head[x],z};head[x]=qwq++;}
#define EREP(x) for(int i=head[x];~i;i=E[i].nxt)

struct p20{
	
	int mx1[N],mx2[N];
	
	void dfs(int x,int f){
		EREP(x){
			int y=E[i].to;
			if(y==f)continue;
			dfs(y,x);
			if(mx1[y]+E[i].w>mx1[x]){
				mx2[x]=mx1[x];
				mx1[x]=mx1[y]+E[i].w;
			}
			else chkmax(mx2[x],mx1[y]+E[i].w); 
		}
//		printf("x=%d mx1=%d mx2=%d\n",x,mx1[x],mx2[x]);
	}
	
	void solve(){
		dfs(1,0);
		printf("%d\n",mx1[1]+mx2[1]);
	}
}p2;

int deg[N];
struct pline{
	int s;
	int id[N],W[N],tim; 
	bool check(){
		DREP(i,n,1){
			if(deg[i]>2) return 0;
			if(deg[i]==1)s=i;
		}
		return 1;
	}
	
	void dfs(int x,int f){
		id[++tim]=x;
		EREP(x) if(E[i].to!=f) W[tim]=E[i].w,dfs(E[i].to,x);
	}
	
	bool Check(int mn){
		int sum=0,cnt=0;
		SREP(i,1,n){
			sum+=W[i];
			if(sum>=mn){
				cnt++;
				sum=0;
			} 
		}
		return cnt>=m;
	}
	
	void solve(){
		dfs(s,0);
		
		int L=1,R=2e9,ans;
		while(L<=R){
			int mid=(L+R)>>1;
			if(Check(mid)) ans=mid,L=mid+1;
			else R=mid-1;
		}
		
		printf("%d\n",ans);
	}
}p3;

struct flower{
	int rt;
	bool check(){
		int cnt=0;
		REP(i,1,n) if(deg[i]>1)cnt++,rt=i;
		return cnt==1;
	}

	int W[N],tot;
	
	bool Check(int x){
		int cnt=0;
		for(int i=tot;i>=1;--i){
			if(W[i]>=x) {
				cnt++;
			}
			else if(W[i]+W[i-1]>=x) {
				i--;
				cnt++;
			}
		}
		
		return cnt>=m;
	}
	
	void solve(){
		EREP(rt) W[++tot]=E[i].w;
		sort(W+1,W+1+tot);
		
		int L=1,R=2e9,ans;
		while(L<=R){
			int mid=(L+R)>>1;
			if(Check(mid))ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}p4;

//struct baoli{
//	int Val[N];
//	
//	int fa[N],sz[N],son[N],dep[N],top[N]; 
//	void dfs1(int x,int f){
//		fa[x]=f;
//		dep[x]=dep[f]+1;
//		sz[x]=1;
//		EREP(x){
//			int y=E[i].to;
//			if(y==f)continue;
////			dis[y]=dis[x]+E[i].w;
//			Val[y]=E[i].w;
//			dfs1(y,x);
//			if(sz[son[x]]<sz[y])son[x]=y;
//		}
//	}
//	
//	void dfs2(int x,int tp){
//		top[x]=tp;
//		if(son[x]) dfs2(son[x],tp);
//		EREP(x){
//			int y=E[i].to;
//			if(y==fa[x] or y==son[x])continue;
//			dfs2(y,y);
//		}
//	}
//	
//	int Lca(int x,int y){
//		while(top[x]!=top[y]){
//			if(dep[top[x]]<dep[top[y]])y=fa[top[y]];
//			else x=fa[top[x]];
//		}
//		return dep[x]<dep[y]?x:y;
//	}
//	bool mark[N];
//	int ans;
//	void Dfs(int cnt,int Min){
//		if(cnt>m)return;
//		if(cnt and Min<=ans)return;
//		if(cnt==m)chkmax(ans,Min);
//		REP(i,1,n){
//			REP(j,i+1,n){
//				int a=i,b=j;
//				int lca=Lca(a,b),f=1;
//				for(int x=a;x!=lca;x=fa[x]){
//					if(mark[x]){
//						f=0;break;
//					}
//				}
//				for(int x=b;x!=lca;x=fa[x]){
//					if(mark[x]){
//						f=0;break;
//					}
//				}
//				if(!f)continue;
//				int sum=0;
//				for(int x=a;x!=lca;x=fa[x]) mark[x]=1,sum+=Val[x];
//				for(int x=b;x!=lca;x=fa[x]) mark[x]=1,sum+=Val[x];
//				Dfs(cnt+1,min(Min,sum));
//			}
//		}
//	}
//	
//	void solve(){
//		dfs1(1,0),dfs2(1,1);
//		ans=2e9;
//		Dfs(0,2e9);
//		printf("%d\n",ans);
//	}
//}p1;

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	rd(n),rd(m);
	memset(head,-1,sizeof head);
	SREP(i,1,n){
		int a,b,c;
		rd(a),rd(b),rd(c);
		addedge(a,b,c);
		addedge(b,a,c);
		deg[a]++,deg[b]++;
	}
	
	if(m==1) p2.solve();
	else if(p3.check())p3.solve();
	else if(p4.check())p4.solve();
//	else p1.solve();
	return 0;
}
