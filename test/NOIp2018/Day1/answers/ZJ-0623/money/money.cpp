#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

int n,lim,m;
int a[111];
bool vis[25555];

ll read()
{
   ll x=0,f=1;char c=getchar();
   while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
   while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
   return x*f;
}

void write(ll x)
{
	 if (x<0) putchar('-'),x=-x;
	 if (x>=10) write(x/10);
	 putchar(x%10+'0');
}

void writeln(ll x){write(x);puts("");}
void writern(ll x){write(x);putchar(' ');}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while (T--)
	{
		memset(vis,0,sizeof vis);
		n=read();
		for (int i=1;i<=n;i++)
			a[i]=read();
		sort(a+1,a+n+1);
		lim=a[n];m=1;
		for (int i=0;i<=lim;i+=a[1])
			vis[i]=1;
		for (int i=2;i<=n;i++)
			if (!vis[a[i]])
			{
				m++;
				for (int j=0;j+a[i]<=lim;j++)
					vis[j+a[i]]=vis[j+a[i]]||vis[j];
			}
		writeln(m);
	}
	return 0;
}

