#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

int n;
int x,y;
ll ans;

ll read()
{
   ll x=0,f=1;char c=getchar();
   while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
   while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
   return x*f;
}

void write(ll x)
{
	 if (x<0) putchar('-'),x=-x;
	 if (x>=10) write(x/10);
	 putchar(x%10+'0');
}

void writeln(ll x){write(x);puts("");}
void writern(ll x){write(x);putchar(' ');}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	x=read();
	ans+=x;
	for (int i=2;i<=n;i++)
	{
		y=read();
		if (y>x) ans+=(y-x);
		x=y;
	}
	writeln(ans);
	return 0;
}

