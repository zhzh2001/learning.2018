#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

int n,m;
int head[50011],nxt[100011],to[100011],w[100011],tot;
int d[50011];

ll read()
{
   ll x=0,f=1;char c=getchar();
   while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
   while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
   return x*f;
}

void write(ll x)
{
	 if (x<0) putchar('-'),x=-x;
	 if (x>=10) write(x/10);
	 putchar(x%10+'0');
}

void writeln(ll x){write(x);puts("");}
void writern(ll x){write(x);putchar(' ');}

void add(int fr,int tt,int tw)
{
	to[++tot]=tt;w[tot]=tw;nxt[tot]=head[fr];head[fr]=tot;
	to[++tot]=fr;w[tot]=tw;nxt[tot]=head[tt];head[tt]=tot;
}

void dfs1(int x,int fa)
{
	for (int i=head[x];i;i=nxt[i])
		if (to[i]!=fa)
		{
			d[to[i]]=d[x]+w[i];
			dfs1(to[i],x);
		}
}

bool check1(int rainy)
{
	int now=0,cnt=0;
	for (int i=1;i<n;i++)
	{
		int vv,ww;
		for (int j=head[i];j;j=nxt[j])
			if (to[j]!=i-1)
			{vv=to[j],ww=w[j];}
		now+=ww;
		if (now>=rainy)
		{
			cnt++;now=0;
		}
	}
	if (cnt>=m) return 1;
	return 0;
}

bool check2(int rainy)
{
	int s=1,cnt=0,t;
	int k=lower_bound(d+1,d+n,rainy)-d;
	if (k>n) t=n-1;
	else t=k-1,cnt+=n-1-t;
	for (int i=t;i>s;i--)
	{
		if (cnt>=m) return 1;
		if (d[i]*2<rainy) return 0;
		int kk=lower_bound(d+s,d+i,rainy-d[i])-d;
		if (kk<i&&d[kk]+d[i]>=rainy)
		{
			cnt++;s=kk+1;
		}
	}
	if (cnt>=m) return 1;
	return 0;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	bool b1=1,b2=1;
	n=read(),m=read();
	for (int i=1;i<n;i++)
	{
		int xx=read(),yy=read(),ww=read();
		add(xx,yy,ww);
		if (yy!=xx+1) b1=0;
		if (xx!=1) b2=0;
	}
	if (m==1)
	{
		dfs1(1,0);
		int maxn=0,s;
		for (int i=1;i<=n;i++)
		{
			if (d[i]>maxn)
				{
					maxn=d[i];s=i;
				}
			d[i]=0;
		}
		dfs1(s,0);
		maxn=0;
		for (int i=1;i<=n;i++)
			maxn=max(maxn,d[i]);
		writeln(maxn);
		return 0;
	}
	if (m==n-1)
	{
		int minn=123456789;
		for (int i=1;i<=tot;i++) minn=min(minn,w[i]);
		writeln(minn);
		return 0;
	}
	if (b1)
	{
		int ans,l=1,r=500000000;
		while (l<=r)
		{
			int mid=l+r>>1;
			if (check1(mid))
				ans=mid,l=mid+1;
			else r=mid-1;
		}
		writeln(ans);
		return 0;
	}
	if (b2)
	{
		int cctt=0;
		for (int i=head[1];i;i=nxt[i])
			d[++cctt]=w[i];
		sort(d+1,d+n);
		int ans,l=1,r=500000000;
		while (l<=r)
		{
			int mid=l+r>>1;
			if (check2(mid))
				ans=mid,l=mid+1;
			else r=mid-1;
		}
		writeln(ans);
		return 0;
	}
	return 0;
}

