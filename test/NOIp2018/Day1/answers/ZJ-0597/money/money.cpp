#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define N 111
#define MAXA 25000
using namespace std;
int T,n,a[N << 2],mx = 0;
int ans[N << 2];
char f1[MAXA << 2];
char f2[MAXA << 2];
inline void go(int x,char *ff){
	for(int i = x;i <= mx;i++)
		if(ff[i - x])
			ff[i] = 1;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	cin>>T;
	while(T--){
		scanf("%d",&n);
		for(int i = 1;i <= n;i++)
			scanf("%d",&a[i]),mx = max(mx,a[i]);
		if(n == 2){
			if(a[1] == a[2])puts("1");
			else puts("2");
			continue;
		}
		/*
		if(n == 3){
			if(a[1]==a[2]&&a[2]==a[3]){
				puts("1");
				continue;
			}
			if(a[1]==a[2]&&a[2]!=a[3]){
				if(a[3] % a[1] == 0)puts("1");
				else puts("2");
				continue;
			}
			if(a[1]!=a[2]&&a[2]==a[3]){
				if(a[1] % a[2] == 0)puts("1");
				else puts("2");
				continue;
			}
			if(a[1]!=a[2]&&a[1]==a[3]){
				if(a[2] % a[1] == 0)puts("1");
				else puts("2");
				continue;
			}
			int mn = 0x7fffffff;
			int ass[3],len = 0;
			for(int i = 1;i <= n;i++)
				if(a[i] != mn && a[i] % mn != 0)
					ass[++len] = a[i];
			if(len == 1){
				puts("2");
				continue;
			}
			else if(len == 0){
				puts("1");
				continue;
			}
			bool flg = 0;
			for(int i = ass[1];i >= 0;i -= mn){
				for(int j = ass[2];j <= 0;j -= mn){
					if(i == j){
						flg = 1;
						puts("2");
						break;
					}
				}
			}
			if(!flg){
				puts("3");
			}
			continue;
		}
		*/
		
		
		memset(f1,0,sizeof f2);
		memset(f2,0,sizeof f2);
		f1[0] = f2[0] = 1;
		sort(a + 1,a + 1 + n);
		go(a[1],f1);
		go(a[1],f2);
		int pos = 2;
		int cnt = 1;
		ans[cnt] = a[1];
		for(int i = a[1] + 1;i <= mx;i++){
			if(a[pos] == i){
				go(a[pos],f2);
				pos++;
			}
			if(!f1[i]){
				if(f2[i]){
					go(i,f1);
					ans[++cnt] = i;
				}
			}
		}
//		cout<<cnt<<":";
		printf("%d\n",cnt);
		/*
		for(int i = 1;i <= cnt;i++){
			printf("%d,",ans[i]);
		}
		puts("");
		*/
	}
	return 0;
}
