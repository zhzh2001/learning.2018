#include <cstdio>
#include <vector>
#include <cstring>
#include <iostream>
#include <algorithm>
#define N 51000
using namespace std;
int n,m;
int xx[N],yy[N],vv[N];
int buf[N];
int f[N];
int fa[N][22];

bool judge(int val){
	int sum = 0;
	int cnt = 0;
	for(int i = 1;i < n;i++){
		sum += buf[i];
		if(sum >= val){
			cnt++;
			sum = 0;
		}
	}
	return cnt >= m;
}
void lia(){
	int sum = 0;
	for(int i = 1;i < n;i++)
		sum += buf[i];
	int L = 1,R = sum,res,mid;
	while(L <= R){
		mid = (L + R) >> 1;
		if(judge(mid)){
			L = mid + 1;
			res = mid;
		}
		else{
			R = mid - 1;
		}
	}
	printf("%d\n",res);
}

int fst[N],nxt[N<<1],to[N<<1],val[N],cnt = 0;

inline void addedge(int x,int y,int v){
	cnt++;
	nxt[cnt] = fst[x];
	fst[x] = cnt;
	to[cnt] = y;
	val[cnt] = v;
}

void yii(){
	sort(vv + 1,vv + n);
	int pos;
	pos = (n - 1 - m);
	int mn = vv[pos * 2 + 1] + 1;
	for(int i = 1;i <= pos;i++){
		mn = min(mn,vv[i] + vv[pos * 2 - i + 1]);
	}
	printf("%d\n",mn);
}

int dep[N];
int dis[N][22];

void dfs(int u,int pa,int d){
	fa[u][0] = pa;
	dep[u] = d;
	for(int i = fst[u];i != -1;i = nxt[i]){
		int v = to[i];
		if(v == pa)continue;
		dis[v][0] = val[i];
		dfs(v,u,d + 1);
	}
}

void bb(){
	for(int i = 1;i <= 22;i++){
		for(int j = 1;j <= n;j++){
			fa[j][i] = fa[fa[j][i - 1]][i - 1];
			dis[j][i] = dis[j][i - 1] + dis[fa[j][i - 1]][i - 1];
		}
	}
}

int dd(int x,int y){
	if(x == y)return 0;
	if(dep[x] < dep[y]){
		swap(x,y);
	}
	int res = 0;
	for(int i = 21;i >= 0;i--){
		if(dep[x] - (1 << i) >= dep[y])
			res += dis[x][i],x = fa[x][i];
	}
	if(x == y)return res;
	for(int i = 21;i >= 0;i--){
		if(fa[x][i] != fa[y][i]){
			res += dis[x][i] + dis[y][i];
			x = fa[x][i];
			y = fa[y][i];
		}
	}
	return res + dis[x][0] + dis[y][0];
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(fst,-1,sizeof fst);
	cin>>n>>m;
	bool lian = 1,yi = 1;
	for(int i = 1;i < n;i++){
		scanf("%d %d %d",&xx[i],&yy[i],&vv[i]);
		if(yy[i] != xx[i] + 1)lian = 0;
		if(xx[i] != 1)yi = 0;
		buf[xx[i]] = vv[i];
		
		addedge(xx[i],yy[i],vv[i]);
		addedge(yy[i],xx[i],vv[i]);
	}
	if(lian){
		lia();
		return 0;
	}
	
	if(yi){
		yii();
		return 0;
	}
	if(m == 1){
		dfs(1,0,1);
		bb();
		int mx = 0;
		for(int i = 1;i <= n;i++)
			for(int j = 1;j <= n;j++)
				mx = max(mx,dd(i,j));
		printf("%d\n",mx);
	}
	return 0;
}
