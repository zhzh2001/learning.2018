#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define N 110000
using namespace std;
typedef long long ll;
int n;
ll mx = 0ll;
struct node{
	 ll h;
	 int id;
}a[N];
int b[N];
char used[N];
bool cmp(node x,node y){
	if(x.h != y.h)return x.h > y.h;
	return x.id < y.id;
}
/*
bool fd(int pos,int val){
	int L = 1,R = pos,res = 0,mid;
	while(L <= R){
		mid = (L + R) >> 1;
		if(a[mid].h <= val){
			res = mid;
			R = mid - 1;
		} else L = mid + 1;
	}
	return res;
}
*/
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	memset(used,0,sizeof used);
	cin>>n;
	for(int i = 1;i <= n;i++){
		scanf("%lld",&a[i].h);
		mx = max(mx,a[i].h);
		a[i].id = i;
	}
	sort(a + 1,a + 1 + n,cmp);
	b[0] = 0;
	for(int i = 1;i <= n;i++){
		b[i] = b[i - 1] + 1;
		if(used[a[i].id - 1])
			b[i]--;
		if(used[a[i].id + 1])
			b[i]--;
		used[a[i].id] = 1;
	}
	ll ans = 0;
	int pos = 1;
	for(int i = mx;i >= 1;i--){
		while(a[pos].h >= i && pos <= n)pos++;
		pos--;
		ans += (ll)b[pos];
	}
	cout<<ans<<endl;
	return 0;
}
