#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,a[1000005],vis[1000005],b[1000005];
int solve(int lim){
	int ans=0;
	for (int i=1; i<=n; i++) vis[i]=0;
	int N=0;
	for (int i=1; i<=n; i++)
		if (a[i]>=lim) ans++;
		else b[++N]=a[i];
	for (int i=N; i>=1; i--)
		if (!vis[i]){
			int last=-1;
			for (int j=i-1; j>=1; j--) if (!vis[j] && b[i]+b[j]>=lim) last=j;
			if (last!=-1) vis[last]=vis[i]=1,ans++;
		}
	return ans>=m;
}
int main(){
	freopen("ceshi.in","r",stdin);
	freopen("ceshi1.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<n; i++) scanf("%d",&a[i]);
	sort(a+1,a+n+1);
	int l=1,r=1000000;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (solve(mid)) l=mid;
		else r=mid-1;	
	}
	printf("%d\n",l);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
