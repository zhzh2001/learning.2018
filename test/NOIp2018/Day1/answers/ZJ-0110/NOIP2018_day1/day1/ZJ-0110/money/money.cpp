#include<cstdio>
#include<algorithm>
using namespace std;
int n,F[1000005],a[1000005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		for (int i=1; i<=n; i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		n=unique(a+1,a+n+1)-a-1;
		int ans=0,m=a[n];
		for (int i=0; i<=m; i++) F[i]=0;
		F[0]=1;
		for (int i=1; i<=n; i++){
			if (!F[a[i]]) ans++;
			for (int j=0; j<=m-a[i]; j++) F[j+a[i]]|=F[j];
		}
		for (int i=0; i<=m; i++) F[i]=0;
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
