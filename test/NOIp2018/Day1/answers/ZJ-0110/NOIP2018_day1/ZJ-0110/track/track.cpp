#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,cnt,cas1,cas2,Lim,last[1000005],F[1000005],G[1000005],a[1000005],vis[1000005],val[1000005],in[1000005],tree[1000005],pre[10000005];
struct node{
	int to,next,val;
}e[1000005];
void add(int a,int b,int c){
	e[++cnt].to=b;
	e[cnt].val=c;
	e[cnt].next=last[a];
	last[a]=cnt;
}
void dfs1(int x,int fa){
	F[x]=G[x]=0;
	for (int i=last[x]; i; i=e[i].next){
		int V=e[i].to;
		if (V==fa) continue;
		dfs1(V,x);
		F[x]+=F[V];
	}
	int N=0;
	for (int i=last[x]; i; i=e[i].next){
		int V=e[i].to;
		if (V==fa) continue;
		if (G[V]+e[i].val>=Lim) F[x]++;
		else a[++N]=G[V]+e[i].val;
	}
	sort(a+1,a+N+1);
	for (int i=1; i<=N; i++) vis[i]=0;
	for (int i=1; i<=N; i++)
		for (int j=1; j<i; j++)
			if (!vis[j] && a[j]+a[i]>=Lim){
				vis[i]=vis[j]=1;
				F[x]++;
				break;
			}
	for (int i=N; i>=1; i--)
		if (!vis[i]){
			G[x]=a[i];
			break;
		}
}
void update(int t){
	tree[t]=min(tree[t<<1],tree[t<<1|1]);
}
void build(int t,int l,int r){
	if (l==r){
		tree[t]=l;
		return;
	}
	int mid=(l+r)>>1;
	build(t<<1,l,mid);
	build(t<<1|1,mid+1,r);
	update(t);
}
int query(int t,int l,int r,int x,int y){
	if (r<x || l>y) return 1e9;
	if (l>=x && r<=y) return tree[t];
	int mid=(l+r)>>1;
	return min(query(t<<1,l,mid,x,y),query(t<<1|1,mid+1,r,x,y));
}
void insert(int t,int l,int r,int x){
	if (l==r){
		tree[t]=1e9;
		return;
	}
	int mid=(l+r)>>1;
	if (x<=mid) insert(t<<1,l,mid,x);
	else insert(t<<1|1,mid+1,r,x);
	update(t);
}
void dfs2(int x,int fa){
	F[x]=G[x]=0;
	for (int i=last[x]; i; i=e[i].next){
		int V=e[i].to;
		if (V==fa) continue;
		dfs2(V,x);
		F[x]+=F[V];
	}
	int N=0;
	for (int i=last[x]; i; i=e[i].next){
		int V=e[i].to;
		if (V==fa) continue;
		if (G[V]+e[i].val>=Lim) F[x]++;
		else a[++N]=G[V]+e[i].val;
	}
	if (!N) return;
	sort(a+1,a+N+1);
	int last=1;
	for (int i=N; i>=1; i--){
		while (last<i && a[last]+a[i]<Lim) last++;
		pre[i]=last;
	}
	build(1,1,N);
	for (int i=1; i<=N; i++)
		if (pre[i]<i){
			int c=query(1,1,N,pre[i],i-1);
			if (c!=1e9) {
				insert(1,1,N,c);
				insert(1,1,N,i);
				F[x]++;
			}
		}
	for (int i=N; i>=1; i--)
		if (query(1,1,N,i,i)!=1e9){
			G[x]=a[i];
			break;
		}
}
int check(int lim){
	Lim=lim;
	dfs1(1,0);
	return F[1]>=m;
}
int check1(int lim){
	Lim=lim;
	dfs2(1,0);
	return F[1]>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	cas1=cas2=1;
	int l=1,r=0;
	for (int i=1; i<n; i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		r+=z;
		val[i]=z;
		add(x,y,z);
		add(y,x,z);
		if (x!=1) cas1=0;
		in[x]++,in[y]++;
	}
	for (int i=1; i<=n; i++) if (in[i]>3) cas2=0;
	if (n<=1000 || cas2){
		while (l<r){
			int mid=(l+r+1)>>1;
			if (check(mid)) l=mid;
			else r=mid-1;
		}
		printf("%d\n",l);
	}
	else{
		while (l<r){
			int mid=(l+r+1)>>1;
			if (check1(mid)) l=mid;
			else r=mid-1;
		}
		printf("%d\n",l);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
