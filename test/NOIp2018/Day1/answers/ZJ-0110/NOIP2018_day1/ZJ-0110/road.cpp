#include<cstdio>
#include<algorithm>
using namespace std;
int n,sum,F[1000005],vis[1000005],To[1000005],Pre[1000005];
struct node{
	int val,id;
}a[1000005];
bool cmp(node a,node b){
	return a.val>b.val;
}
int query(int x){
	if (F[x]!=x) F[x]=query(F[x]);
	return F[x];
}
void merge(int x,int y){
	int X=query(x),Y=query(y);
	if (X!=Y) F[X]=Y;
}
void solve(int x){
	vis[x]=1;
	int to=To[x],pre=Pre[x];
	if (!vis[to] && !vis[pre]) sum++;
	else if (!vis[to] && vis[pre]) merge(pre,x);
	else if (vis[to] && !vis[pre]) merge(to,x);
	else{
		int X=query(pre),Y=query(to);
		if (X!=Y) sum--;
		merge(pre,x);
		merge(to,x);
	}
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1; i<=n; i++) scanf("%d",&a[i].val);
	for (int i=1; i<=n; i++) F[i]=i;
	for (int i=1; i<=n; i++) a[i].id=i;
	sort(a+1,a+n+1,cmp);
	for (int i=1; i<=n; i++) To[i]=i+1;
	for (int i=1; i<=n; i++) Pre[i]=i-1;
	sum=0;
	int head=1,ans=0;
	for (int h=10000; h>=1; h--){
		while (a[head].val>=h && head<=n){
			solve(a[head].id);
			head++;
		}
		ans+=sum;
	}
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
