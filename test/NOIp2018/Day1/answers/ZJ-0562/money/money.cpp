#include<bits/stdc++.h>
using namespace std;
int n,T;
int a[110];
bool vis[110];
bool f[26100];
inline int read()
{
	int s = 0,w = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') {if(ch == '-') w = -1;ch = getchar();}
	while(ch>='0'&&ch<='9') {s = (s << 1) + (s << 3) + ch - '0';ch = getchar();}
	return s * w;
}
inline void Check()
{
	memset(f,0,sizeof(f));
	f[0] = 1;
	for(int i = 1;i<=n;i++)
	{
		if(f[a[i]])
		{
			vis[i] = 1;
			continue;
		}
		for(int j = 0;j<=a[n]-a[i];j++) if(f[j]) f[j+a[i]] = 1;
	}
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T = read();
	while(T--)
	{
		n = read();
		memset(vis,0,sizeof(vis));
		for(int i = 1;i<=n;i++) a[i] = read();
		sort(a+1,a+1+n);
		Check();
		int ans = 0;
		for(int i = 1;i<=n;i++) ans += (vis[i] == 0);
		printf("%d\n",ans);
	}
	return 0;
}
