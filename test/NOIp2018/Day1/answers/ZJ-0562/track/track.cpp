#include<bits/stdc++.h>
#define MAXN 51000
using namespace std;
int n,m;
int hed[MAXN],nxt[MAXN*2],ed[MAXN*2],w[MAXN*2],l = 0,S;
int Maxdis = 0,dis[MAXN],WW[MAXN];
int sum = 0; 
inline void Line(int u,int v,int vlu){nxt[++l] = hed[u],hed[u] = l,ed[l] = v,w[l] = vlu;} 
inline int read()
{
	int s = 0,w = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9'){if(ch == '-') w = -1;ch = getchar();}
	while(ch>='0'&&ch<='9'){s = (s << 1) + (s << 3) + ch - '0';ch = getchar();}
	return s * w;
}
inline int D(int u,int fa)
{
	bool flag = 0;
	for(int i = hed[u];i;i=nxt[i])
	{
		if(ed[i] == fa) continue;
		flag = 1;
		dis[ed[i]] = dis[u] + w[i];
		D(ed[i],u);
	}
	if(!flag && dis[u] > Maxdis) Maxdis = dis[u],S = u;
}
inline bool CheckLine(int u)
{
	int tot = 0,opt = 0; 
	for(int i = 1;i<n;i++)
	{
		for(int j = hed[i];j;j=nxt[j]) if(ed[j] == i+1) opt+=w[j];
		if(opt >= u) tot++,opt = 0;
	}
	if(tot >= m) return 1;
	return 0;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n = read(),m = read();
	bool IsFlower = 1;
	for(int i = 1;i<n;i++)
	{
		int u = read(),v = read(),vlu = read();
		WW[i] = vlu;
		sum+=vlu;
		if(u!= 1 && v != 1) IsFlower = 0;
		Line(u,v,vlu),Line(v,u,vlu);
	}
	if(m == 1) // ֱ�� 
	{
		D(1,0);
		memset(dis,0,sizeof(dis));
		Maxdis = 0; 
		D(S,0);
		printf("%d\n",Maxdis);
	}
	else if(IsFlower) //�ջ�ͼ 
	{
		sort(WW+1,WW+n);
		int ans = 1147483647;
		if(m*2 <= n-1) for(int i = 1;i<=m;i++) ans = min(ans,WW[n-i]+WW[n-1-m*2+i]);
		else
		{
			int tmp = m*2-n+1;
			tmp = n - tmp;
			ans = WW[tmp];
			for(int i = tmp-1;i>=1;i--) ans = min(ans,WW[tmp - i]+WW[i]);
		}
		printf("%d\n",ans);
	}
	else // �� 
	{
		int l = 0,r = sum,mid;
		while(l < r - 3)
		{
			mid = (l + r) >> 1;
			if(CheckLine(mid)) l = mid;
			else r = mid - 1;
		}
		for(int i = r;i>=l;i--)
			if(CheckLine(i))
			{
				printf("%d\n",i);
				return 0;
			}
	}
	return 0;
}
