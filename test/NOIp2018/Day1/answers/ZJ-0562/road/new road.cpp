#include<bits/stdc++.h>
using namespace std;
int n,sl = 0;
int a[110000];
int ans = 0;
struct Sqare
{
	int maxn,minn,l,r;
}s[110000];
inline int read()
{
	int s = 0,w = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9'){if(ch == '-') w = -1;ch = getchar();}
	while(ch>='0'&&ch<='9'){s = (s << 1) + (s << 3) + ch - '0';ch = getchar();}
	return s * w;
}
int main()
{
	freopen("road.in","r",stdin);
//	freopen("road.out","w",stdout);
	n = read();
	for(int i = 1;i<=n;i++) a[i] = read();
	int opt = 1,maxn = a[1],minn = a[1];
	s[1].l = 1;
	for(int i = 2;i<=n;i++)
	{
		if(opt == 1 && a[i-1] <= a[i]) maxn = max(maxn,a[i]);
		else if(a[i-1] >= a[i]) minn = min(minn,a[i]),opt = 0;
		else
		{
			s[++sl].r = i-1;
			s[sl+1].l = i;
			s[sl].maxn = maxn;
			s[sl].minn = minn;
			maxn = minn = a[i];
			opt = 1;
		}
	}
	s[++sl].r = n;
	s[sl].maxn = maxn;
	s[sl].minn = minn;
	for(int i = 1;i<=sl;i++)
	{
		ans+=s[i].maxn - max(s[i-1].minn,s[i+1].minn);
	}
	printf("%d\n",ans);
	return 0;
}
