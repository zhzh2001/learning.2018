#include<bits/stdc++.h>
using namespace std;
int n;
int d[110000];
int ans = 0;
inline void D(int l,int r)
{
	int minn = 1147483647,id;
	for(int i = l;i<=r;i++) 
		if(minn > d[i]) minn = d[i],id = i;
	for(int i = l;i<=r;i++) d[i]-=minn;
	ans+=minn;
	if(l <= id-1) D(l,id-1);
	if(id+1 <= r) D(id+1,r);
}
inline int read()
{
	int s = 0,w = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9'){if(ch == '-') w = -1;ch = getchar();}
	while(ch>='0'&&ch<='9'){s = (s << 1) + (s << 3) + ch - '0';ch = getchar();}
	return s * w;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n = read();
	for(int i = 1;i<=n;i++) d[i] = read();
	D(1,n);
	printf("%d\n",ans);
	return 0;
}
