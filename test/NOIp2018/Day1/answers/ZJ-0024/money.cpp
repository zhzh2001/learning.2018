#include <iostream>
#include <cstdio>
#include <cmath>
#include <queue>
#include <cstring>
#include <algorithm>
#define ll long long
using namespace std;
int read(){
	int x=0; bool f=0; char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48; ch=getchar();}
	return (f)?-x:x;
}
const int N=25007;
int cas,n,ans,maxn,f[107][N],a[107];
bool cmp(int a,int b){return a<b;}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	cas=read();
	while (cas--){
		n=read();
		for (int i=1;i<=n;++i) a[i]=read();
		sort(a+1,a+n+1,cmp);
		for (int i=1;i<n;++i){
			if (a[i]==0) continue;
			for (int j=i+1;j<=n;++j)
				if (a[j]%a[i]==0) a[j]=0;
		}
		int tmp=0;
		for (int i=1;i<=n;++i)
			if (a[i]>0) a[++tmp]=a[i];
		n=tmp;
		maxn=a[n];
		f[0][0]=1;
		for (int i=1;i<=n;++i)
		for (int j=0;j<=maxn;++j){
			f[i][j]=f[i-1][j];
			for (int k=1;k*a[i]<=j;++k){
				f[i][j]+=f[i-1][j-a[i]*k];
				if (f[i][j]>1){f[i][j]=2; break;}
			}
		}
		ans=n;
		for (int i=1;i<=n;++i)
			if (f[n][a[i]]>1) --ans;
		printf("%d\n",ans);
	}
	return 0;
}
