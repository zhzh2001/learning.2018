#include <iostream>
#include <cstdio>
#include <cmath>
#include <queue>
#include <cstring>
#include <algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0; bool f=0; char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48; ch=getchar();}
	return (f)?-x:x;
}
inline int ABS(int x){return (x>=0)?x:-x;}
bool cmp(int a,int b){return a<b;}
const int N=50007;
struct JOKER{int b,nxt,w;}e[N<<1];
int n,m,ans,tot,head[N],dis[N];
int len,a[N],minlen;
ll sum;
bool vis[N];
inline void add(int x,int y,int z){
	e[++tot].b=y;
	e[tot].w=z;
	e[tot].nxt=head[x];
	head[x]=tot;
}
void dfs(int u){
	vis[u]=1;
	for (int i=head[u];i;i=e[i].nxt){
		int v=e[i].b;
		if (vis[v]) continue;
		dis[v]=dis[u]+e[i].w;
		dfs(v);
	}
}

inline bool check1(){
	for (int u=1;u<=n;++u)
	for (int i=head[u];i;i=e[i].nxt)
		if (ABS(u-e[i].b)!=1) return false;
	return true;
}
void dfs1(int u,int fa){
	for (int i=head[u];i;i=e[i].nxt){
		if (e[i].b==fa) continue;
		a[++len]=e[i].w;
		dfs1(e[i].b,u);
	}
}
inline bool check1_mid(int x){
	int cnt=0,s=0;
	for (int i=1;i<=n;++i){
		if (s>=x){++cnt; s=0;}
		s+=a[i];
	}
	return (cnt>=m);
}

inline bool check2(){
	int cnt=0;
	for (int i=head[1];i;i=e[i].nxt) ++cnt;
	return (cnt==n-1);
}
inline bool check2_mid(int x){
	int cnt=0;
	int j=1;
	for (int i=len;i>0;--i){
		if (a[i]>=x){++cnt; continue;}
		while (j<i && a[j]+a[i]<x) ++j;
		if (j<i){++cnt; ++j;}
		if (j>=i) break;
	}
	return (cnt>=m);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	n=read(); m=read();
	minlen=0x3f3f3f3f;
	for (int i=1;i<n;++i){
		int x=read(),y=read(),z=read();
		add(x,y,z); add(y,x,z);
		sum=sum+z;
		minlen=min(minlen,z);
	}
	if (m==1){
		for (int i=1;i<=n;++i) dis[i]=vis[i]=0;
		dfs(1); 
		int maxdis=0,x=1;
		for (int i=1;i<=n;++i)
			if (dis[i]>maxdis){maxdis=dis[i]; x=i;}
		for (int i=1;i<=n;++i) dis[i]=vis[i]=0;
		dfs(x); ans=0;
		for (int i=1;i<=n;++i) ans=max(ans,dis[i]);
		printf("%d\n",ans);
		return 0;
	}
	if (check1()){
		len=0; dfs1(1,0);
		int l=1,ans=0,r=0;
		for (int i=1;i<=len;++i) r+=a[i];
		while (l<=r){
			int mid=(l+r)>>1;
			if (check1_mid(mid)) ans=mid,l=mid+1;
				else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	if (check2()){
		len=0; dfs1(1,0);
		int l=1,ans=0,r=0;
		for (int i=1;i<=len;++i) r+=a[i];
		sort(a+1,a+len+1,cmp);
		while (l<=r){
			int mid=(l+r)>>1;
			if (check2_mid(mid)) ans=mid,l=mid+1;
				else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	if (m==n-1){
		printf("%d\n",minlen);
		return 0;
	}
	printf("%lld\n",sum/m-1);
	return 0;
}
