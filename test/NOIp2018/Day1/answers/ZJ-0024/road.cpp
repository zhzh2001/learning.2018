#include <iostream>
#include <cstdio>
#include <cmath>
#include <queue>
#include <cstring>
#include <algorithm>
#define ll long long
using namespace std;
int read(){
	int x=0; bool f=0; char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48; ch=getchar();}
	return (f)?-x:x;
}
const int N=100007;
int n,m,tmp,ans,a[N],b[N],c[N];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	n=read();
	for (int i=1;i<=n;++i) a[i]=read();
	for (int i=1;i<=n;++i){
		if (a[i]>a[i-1]){
			b[++m]=a[i-1];
			c[m]=a[i];
		}
	}
	for (int i=1;i<=m;++i) ans+=c[i]-b[i];	
	printf("%d\n",ans);
	return 0;
}
