#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>

using namespace std;

int n,m,flag1,flag2,sum,a[50010],b[50010],c[50010],d[50010],cnt,
	f[50010],dp[50010][2],nex[100010],head[50010],vet[100010],
	w[100010],tot,flag[50010];

void add(int u,int v,int val){
	nex[++tot]=head[u];
	head[u]=tot;
	vet[tot]=v;
	w[tot]=val;
}

int test1(int x){
	int p=1,ret=0,cnt=0;
	for (int i=1;i<n;i++)
		if (c[i]<x) d[++cnt]=c[i];
		else ret++;
	sort(d+1,d+cnt+1);
	for (int i=cnt;i;i--){
		while (p<i&&d[p]+d[i]<x) p++;
		if (p<i) ret++;
		else break;
	}
	if (ret>=m) return 1;
	else return 0;
}

int find1(int l,int r){
	int ret;
	while (l<=r){
		int mid=(l+r)>>1;
		if (test1(mid)) l=mid+1,ret=mid;
		else r=mid-1;
	}
	return ret;
}

int test2(int x){
	int sum=0,ret=0;
	for (int i=1;i<n;i++){
		sum+=d[i];
		if (sum>=x) ret++,sum=0;
	}
	if (ret>=m) return 1;
	else return 0;
}

int find2(int l,int r){
	int ret;
	while (l<=r){
		int mid=(l+r)>>1;
		if (test2(mid)) l=mid+1,ret=mid;
		else r=mid-1;
	}
	return ret;
}

int dfs(int u,int x){
	int cnt=0;
	for (int i=head[u];i;i=nex[i]){
		int v=vet[i];
		if (v!=f[u]){
			f[v]=u;
			dfs(v,x);
		}
	}
	for (int i=head[u];i;i=nex[i]){
		int v=vet[i];
		if (v!=f[u]){
			dp[u][0]+=dp[v][0];
			if (dp[v][1]+w[i]<x) c[++cnt]=dp[v][1]+w[i];
			else dp[u][0]++;
		}
	}
	sort(c+1,c+cnt+1);
	for (int i=1;i<=cnt;i++)
		flag[i]=0;
	int p=1;
	for (int i=cnt;i;i--){
		while (p<i&&c[p]+c[i]<x) p++;
		if (p<i){
			dp[u][0]++;
			flag[i]=1;
			flag[p]=1;
			p++;
		}
		else break;
	}
	for (int i=1;i<=cnt;i++)
		if (!flag[i]) dp[u][1]=max(dp[u][1],c[i]);
}

int test(int x){
	f[1]=0;
	memset(dp,0,sizeof(dp));
	dfs(1,x);
	if (dp[1][0]>=m) return 1;
	else return 0;
}

int find(int l,int r){
	int ret;
	while (l<=r){
		int mid=(l+r)>>1;
		if (test(mid)) l=mid+1,ret=mid;
		else r=mid-1;
	}
	return ret;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	flag1=flag2=1;
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&a[i],&b[i],&c[i]);
		if (a[i]!=1) flag1=0;
		if (b[i]!=a[i]+1) flag2=0;
		sum+=c[i];
	}
	if (flag1){
		printf("%d",find1(0,sum));
		return 0;
	}
	if (flag2){
		for (int i=1;i<n;i++)
			d[a[i]]=c[i];
		printf("%d",find2(0,sum));
		return 0;
	}
	for (int i=1;i<n;i++){
		add(a[i],b[i],c[i]);
		add(b[i],a[i],c[i]);
	}
	printf("%d",find(0,sum));
	return 0;
}
