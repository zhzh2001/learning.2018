#include<iostream>
#include<cstdio>
#include<cstring>

using namespace std;

int n,a[110],maxa,f[30010],ans,T;

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		for (int i=1;i<=n;i++){
			scanf("%d",&a[i]);
			maxa=max(maxa,a[i]);
		}
		memset(f,0,sizeof(f));
		f[0]=1;
		for (int i=0;i<=maxa;i++)
			for (int j=1;j<=n;j++)
				if (i+a[j]<=maxa&&f[i+a[j]]<2) f[i+a[j]]+=f[i];
		ans=0;
		for (int i=1;i<=n;i++)
			if (f[a[i]]<=1) ans++;
		printf("%d\n",ans);
	}
	return 0;
}
