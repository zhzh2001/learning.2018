#include<bits/stdc++.h>
using namespace std;
const int N=1e5+5;
int n,a[N],b[N],c[N];
long long ans;
int read()
{
	int x=0,f=1;char s=getchar();
	while(s>'9'||s<'0'){if(s=='-')f=-1;s=getchar();}
	while(s<='9'&&s>='0'){x=x*10+s-'0';s=getchar();}
	return x*f;
}
void find(int l,int r,int del)
{
	if(l==r)
	{
		ans+=a[l]-del;
		return;
	}
	int mn=1e9;
	for(int i=l;i<=r;i++)
		if(a[i]-del<mn)mn=a[i]-del;
	ans+=mn;
	int x=l;
	for(int i=l;i<=r;i++)
		if(a[i]-del==mn)
		{
			if(x<i)find(x,i-1,del+mn);
			x=i+1;
		}
	if(x<=r)find(x,r,del+mn);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	find(1,n,0);
	printf("%lld\n",ans);
	return 0;
}
