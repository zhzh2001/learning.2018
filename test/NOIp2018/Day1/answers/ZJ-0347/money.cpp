#include<bits/stdc++.h>
using namespace std;
const int N=1e5+5;
int n,a[N],vis[N];
int read()
{
	int x=0,f=1;char s=getchar();
	while(s>'9'||s<'0'){if(s=='-')f=-1;s=getchar();}
	while(s<='9'&&s>='0'){x=x*10+s-'0';s=getchar();}
	return x*f;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--)
	{
		n=read();
		for(int i=1;i<=25000;i++)vis[i]=0;
		for(int i=1;i<=n;i++)
		{
			a[i]=read();
			if(!vis[a[i]])vis[a[i]]=1;
			for(int j=a[i]+1;j<=25000;j++)
				if(vis[j-a[i]])vis[j]=2;
		}
		int ans=0;
		for(int i=1;i<=n;i++)
			if(vis[a[i]]!=2)ans++;
		printf("%d\n",ans);
	}
	return 0;
}
