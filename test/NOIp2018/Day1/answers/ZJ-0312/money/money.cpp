#include<bits/stdc++.h>
using namespace std;

const int N=105,MAXA=25005;
int T,n,maxa,ans;
int a[N],f[MAXA],vis[MAXA];

int read(){
	int s=0,w=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') w=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') s=(s<<3)+(s<<1)+ch-'0',ch=getchar();
	return s*w;
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	T=read();
	for (int k=1;k<=T;++k){
		n=read();maxa=0;ans=n;
		for (int i=1;i<=n;++i){
			a[i]=read();
			if (a[i]>maxa) maxa=a[i];
		}
		memset(vis,0,sizeof(vis));
		memset(f,0,sizeof(f));f[0]=1;
		for (int i=1;i<=n;++i){
			f[a[i]]=1;
			for (int j=a[i]+1;j<=maxa;++j){
				f[j]=f[j]|f[j-a[i]];
				if (f[j-a[i]]) vis[j]=1;
			}
		}
		for (int i=1;i<=n;++i) if (vis[a[i]]) --ans;
		printf("%d\n",ans);
	}
	return 0;
}
