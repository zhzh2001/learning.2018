#include<bits/stdc++.h>
using namespace std;

const int N=100005;
int n,ans;
int a[N];

int read(){
	int s=0,w=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') w=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') s=(s<<3)+(s<<1)+ch-'0',ch=getchar();
	return s*w;
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	n=read();
	for (int i=1;i<=n;++i){
		a[i]=read();
		if (a[i]<=a[i-1]) continue;
		ans+=a[i]-a[i-1];
	}
	printf("%d",ans);
	return 0;
}
