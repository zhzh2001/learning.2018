#include<bits/stdc++.h>
using namespace std;

const int N=50005;
int n,m,l,r,mid,ans;
struct myt{
	int a,b,l;
	bool operator < (const myt &o) const {
		return l>o.l;
	}
}seg[N];

int read(){
	int s=0,w=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') w=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') s=(s<<3)+(s<<1)+ch-'0',ch=getchar();
	return s*w;
}

bool check(){
	for (int i=1;i<n;++i) if (seg[i].a!=1) return 0;
	return 1;
}

bool pd(int mid){
	int rst=0,now=0;
	for (int i=1;i<n;++i){
		rst+=seg[i].l;
		if (rst>=mid) rst=0,++now;
		if (now==m) return 1;
	}
	return 0;
}

bool cmp1(myt a,myt b) {return a.l>b.l;}
bool cmp2(myt a,myt b) {return a.a<b.a;}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	n=read();m=read();
	for (int i=1;i<n;++i) seg[i].a=read(),seg[i].b=read(),seg[i].l=read(),r+=seg[i].l;
	if (check()){
		sort(seg+1,seg+1+n-1,cmp1);
		printf("%d",seg[m].l);
	}else{
		sort(seg+1,seg+1+n-1,cmp2);l=1;
		while (l<=r){
			mid=(l+r)>>1;
			if (pd(mid)) l=mid+1,ans=mid;else r=mid-1;
		}
		printf("%d",ans);
	}
	return 0;
}
