#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<string>
#define N 110
using namespace std;
int T,n,a[N];
bool bo[25010];
bool pd(int k,int nn)
{
	if(k<1) return 0;
	if(bo[k]==1) return 1;
	for(int i=1;i<=nn;i++) if(pd(k-a[i],nn)==1&&(k-a[i])>=1) 
	{
		bo[k]=1;
		break;
	}
	return bo[k];
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	for(int _i=1;_i<=T;_i++)
	{
		scanf("%d",&n);
		for(int i=1;i<=n;i++) scanf("%d",&a[i]);
		int ans=n;
		sort(a+1,a+n+1);
		memset(bo,0,sizeof(bo));
		for(int i=2;i<=n;i++) 
		{
			bo[a[i-1]]=1;
			if(pd(a[i],i-1)==1) ans--;
		}
		printf("%d\n",ans);
	}
	return 0;
}
