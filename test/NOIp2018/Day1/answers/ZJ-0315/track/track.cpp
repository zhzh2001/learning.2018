#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<string>
#define N 5010
#define M 50010
using namespace std;
int dis[N][N],T[N][N],n,m,a[M];
bool bo[N];
inline bool cmp(int k1,int k2)
{
	return k1>k2;
}
inline void mrz_add(int k1,int k2,int k3)
{
	T[k1][0]++;
	T[k2][0]++;
	T[k1][T[k1][0]]=k2;
	T[k2][T[k2][0]]=k1;
	if(k3<dis[k1][k2]) 
	{
		dis[k1][k2]=k3;
		dis[k2][k1]=k3;
	}
	return;
}
void Deal_First(int kc,int kd,int kk)
{
	dis[kc][kd]=kk;
	for(int i=1;i<=T[kd][0];i++) if(bo[T[kd][i]]==0)
	{
		bo[T[kd][i]]=1;
		Deal_First(kc,T[kd][i],kk+dis[kd][T[kd][i]]);
	}
	return;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(dis,127,sizeof(dis));
	memset(T,0,sizeof(T));
	scanf("%d%d",&n,&m);
	int u,v,l;
	bool b_a=1,b_b=1;
	for(int i=1;i<=n-1;i++)
	{
		scanf("%d%d%d",&u,&v,&l);
		a[i]=l;
		if(u!=1&&v!=1) b_a=0;
		if(v!=u+1) b_b=0;
		mrz_add(u,v,l);
	}
	if(b_a==0&&b_b==0)
	{
		for(int i=1;i<=n;i++) 
		{
			memset(bo,0,sizeof(bo));
			bo[i]=1;
			Deal_First(i,i,0);
		}
		int t=0;
		for(int i=1;i<=n;i++)
		{
			for(int j=1;j<=n;j++)
			{
				t++;
				a[t]=dis[i][j];
			}
		}
		sort(a+1,a+t+1,cmp);
		printf("%d\n",a[m]);
	} else if(b_a==1)
	{
		sort(a+1,a+n,cmp);
		printf("%d\n",a[m]);
	}
	return 0;
}
