#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<string>
#define N 100010
using namespace std;
int t=0,a[N],d[N],n;
long long ans=0;
long long f1(int l,int r)
{
	if(l>r) return 0;
	long long ret=0;
	bool bb=0;
	for(int i=1;i<=t;i++) if(a[i]>=l&&a[i]<=r)
	{
		bb=1;
		ret+=f1(l,a[i]-1)+f1(a[i]+1,r);
	}
	if(bb==0)
	{
		int minn=100000;
		for(int i=l;i<=r;i++) if(d[i]<minn) minn=d[i];
		for(int i=l;i<=r;i++)
		{
			d[i]-=minn;
			if(d[i]==0) 
			{
				t++;
				a[t]=i;
			}
		}
		return minn;
	}
	return ret;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&d[i]);
		if(d[i]==0)
		{
			t++;
			a[t]=i;
		}
	}
	while(t<n) ans+=f1(1,n);
	printf("%lld\n",ans);
	return 0;
}
