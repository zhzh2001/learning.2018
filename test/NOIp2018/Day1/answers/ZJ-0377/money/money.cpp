#include<bits/stdc++.h>
#define Fr(i,a,b) for(int i=(a);i<=(int)(b);++i)
#define Dr(i,a,b) for(int i=(a);i>=(int)(b);--i)
const int inf=0x3f3f3f3f;
typedef long long ll;
using namespace std;
#define debug(a,b) cout<<#a<<"="<<a<<" "<<#b<<"="<<b<<endl
const int M=105;
int n,a[M],len,T,b[M],ct;
bool mark[25005],dp[25005];
int main(){//money memory mod long-lonf *
//	printf("%.2lf\n",(&mmm2-&mmm1)/1024.0/1024);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	cin>>T;
	while(T--){
		n=0,ct=0,len=0;
		memset(dp,0,sizeof(dp));
		memset(mark,0,sizeof(mark));
		scanf("%d",&n);
		Fr(i,1,n)scanf("%d",&a[i]);
		sort(a+1,a+1+n),len=unique(a+1,a+1+n)-a-1;//重复的明显可以不要 
		int ma=a[n];
		Fr(i,1,len){
			if(mark[a[i]])continue;//倘若这个数字的因子出现过那也显然不要 
			else b[++ct]=a[i];
			for(int j=a[i]+a[i];j<=ma;j+=a[i])mark[j]=1;
		}
		dp[0]=1;
		int all=ct;
		Fr(i,1,ct){
			all-=dp[b[i]];//假若比这个数字小的数字能够组成他那么也显然不要 
			if(dp[b[i]])continue;
			Fr(j,b[i],ma)dp[j]|=dp[j-b[i]];
		}
		printf("%d\n",all);
	}
	return 0;
}
