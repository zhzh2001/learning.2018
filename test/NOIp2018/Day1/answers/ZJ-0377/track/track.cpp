#include<bits/stdc++.h>
#define Fr(i,a,b) for(int i=(a);i<=(int)(b);++i)
#define Dr(i,a,b) for(int i=(a);i>=(int)(b);--i)
const int inf=0x3f3f3f3f;
typedef long long ll;
using namespace std;
#define debug(a,b) cout<<#a<<"="<<a<<" "<<#b<<"="<<b<<endl
const int M=50005;
bool mmm1;
struct Graph{
	struct node{int fr,to,co;}e[M<<1];
	int tot,head[M];
	void clear(){memset(head,-1,sizeof(head)),tot=0;}
	void add(int a,int b,int c){e[++tot].to=b,e[tot].fr=head[a],head[a]=tot,e[tot].co=c;}
	#define Ed(i,u) for(int i=G.head[u];~i;i=G.e[i].fr)
}G;
int n,m,u,v,co;
struct P20{//m=1 求直径 
	int dp[M][2],maxd;
	void dfs(int u,int fa){
		dp[u][0]=dp[u][1]=0;
		Ed(i,u){
			int v=G.e[i].to,co=G.e[i].co;
			if(v==fa)continue;
			dfs(v,u);
			if(dp[v][0]+co>dp[u][0])dp[u][1]=dp[u][0],dp[u][0]=dp[v][0]+co;
			else if(dp[v][0]+co>dp[u][1])dp[u][1]=dp[v][0]+co;
		}
		maxd=max(maxd,dp[u][0]+dp[u][1]);
	}
	void sol(){
		maxd=-inf,dfs(1,0);
		printf("%d\n",maxd);
	}
}p_20;
struct P0{//m==n-1 求最小的边的边长 
	void sol(){
		int ma=inf;
		Fr(i,1,G.tot)ma=min(ma,G.e[i].co);
		printf("%d\n",ma);
	}
}p_0;
int deg[M],All;
struct PLIAN{//链 贪心求 二分即可 
	int dis[M];
	bool checkin(){
		int ct=0;
		Fr(i,1,n)if(deg[i]==1)ct++;
		return ct==2;
	}
	void dfs(int u,int fa){
		Ed(i,u){
			int v=G.e[i].to,co=G.e[i].co;
			if(v==fa)continue;
			dis[v]=dis[u]+co;
			dfs(v,u);
		}
	}
	bool check(int t){
		int lst=1,cnt=0;//贪心的取分割点 
		Fr(i,2,n){
			if(dis[i]-dis[lst]>=t)cnt++,lst=i;
			else continue;
		}
		return cnt>=m;
	}
	void sol(){
		dis[1]=0,dfs(1,0);
		int l=1,r=All,res=-1;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))l=mid+1,res=mid;
			else r=mid-1;
		}
		printf("%d\n",res);
	}
}p_lian;
bool cmp(int a,int b){return a>b;}
struct PJUHUA{//关于1 的菊花图 取前2*m大的边即可 
	int a[M];
	bool checkin(){
		Fr(i,2,n)if(deg[i]!=1)return false;
		return true;
	}
	void sol(){
		int ct=0;
		for(int i=1;i<=G.tot;i+=2){
			a[++ct]=G.e[i].co;
		}
		sort(a+1,a+1+ct,cmp);
		int all=m,ans=inf;
		for(int i=1;i<=ct&&all>0;i+=2){
			ans=min(ans,a[i]+a[i+1]);
			all--;
		}
		printf("%d\n",ans);
	}
}p_juhua;
struct PSHUI{
	int dp[M][2],cnt;
	void dfs(int u,int fa,int t){
		dp[u][0]=dp[u][1]=0;
		Ed(i,u){
			int v=G.e[i].to,co=G.e[i].co;
			if(v==fa)continue;
			dfs(v,u,t);
			if(dp[v][0]+co>dp[u][0])dp[u][1]=dp[u][0],dp[u][0]=dp[v][0]+co;
			else if(dp[v][0]+co>dp[u][1])dp[u][1]=dp[v][0]+co;
		}
		if(dp[u][0]+dp[u][1]>=t){
			cnt++;
			int cnt0=0,cnt1=0,re0=dp[u][0],re1=dp[u][1];
			dp[u][0]=0,dp[u][1]=0;
			Ed(i,u){
				int v=G.e[i].to,co=G.e[i].co;
				if(v==fa)continue;
				if(dp[v][0]+co==re0&&!cnt0){cnt0++;continue;}
				else if(dp[v][0]+co==re1&&!cnt1){cnt1++;continue;}
				if(dp[v][0]+co>dp[u][0])dp[u][1]=dp[u][0],dp[u][0]=dp[v][0]+co;
				else if(dp[v][0]+co>dp[u][1])dp[u][1]=dp[v][0]+co;
			}
		}
	}
	bool check(int t){
		cnt=0,dfs(1,0,t);
		return cnt>=m;
	}
	void sol(){
		int l=1,r=All,res=-1;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))l=mid+1,res=mid;
			else r=mid-1;
		}
		printf("%d\n",res);
	}
}p_shui;
struct P100{
	int dp[M],cnt;
	vector<int>vec[M];
	void dfs(int u,int fa,int t){
		dp[u]=0;vec[u].clear();
		Ed(i,u){
			int v=G.e[i].to,co=G.e[i].co;
			if(v==fa)continue;
			dfs(v,u,t);
			vec[u].push_back(dp[v]+co);
		}
		sort(vec[u].begin(),vec[u].end(),cmp);
		int siz=vec[u].size();
		int ia=0,ib=siz-1;
		for(;ia<=ib;ia++){
			if(vec[u][ia]>=t){
				cnt++;
				continue;
			}
			while(ib>=ia&&vec[u][ia]+vec[u][ib]<t)dp[u]=max(dp[u],vec[u][ib]),ib--;
			if(ia==ib){
				if(vec[u][ia]>=t)cnt++;
				else dp[u]=max(dp[u],vec[u][ia]);
			}
			else if(ib<ia){
				if(vec[u][ia]>=t)cnt++;
				else dp[u]=max(vec[u][ia],dp[u]);
			}
			else cnt++,ib--;
		}
	}
	bool check(int t){
		cnt=0,dfs(1,0,t);
		return cnt>=m;
	}
	void sol(){
		int l=1,r=All,res=-1;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))l=mid+1,res=mid;
			else r=mid-1;
		}
		printf("%d\n",res);
	}
}p_100;
bool mmm2;
int main(){//track memory mod long-lonf *
//	printf("%.2lf\n",(&mmm2-&mmm1)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m,G.clear();
	Fr(i,1,n-1){
		scanf("%d%d%d",&u,&v,&co);
		G.add(u,v,co),G.add(v,u,co),deg[v]++,deg[u]++;
		All+=co;
	}
	bool check3=1;
	Fr(i,1,n)if(deg[i]>3)check3=0;
	if(m==1)p_20.sol();
	else if(m==n-1)p_0.sol();
	else if(p_lian.checkin())p_lian.sol();
	else if(p_juhua.checkin())p_juhua.sol();
	else if(check3)p_shui.sol();
	else p_100.sol();
	return 0;
}
