#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=50005,maxm=100005;
int n,m,now,L,R,tot,c[maxn],b[maxn],MAX[maxn],lnk[maxn],w[maxm],son[maxm],nxt[maxm];
inline char nc()
{
	static char buf[100000],*l,*r;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF; return *l++;
}
inline int Read()
{
	int res=0,f=1; char ch=nc(),cc=ch;
	while (ch<'0'||ch>'9') cc=ch,ch=nc();
	if (cc=='-') f=-1;
	while (ch>='0'&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}
void Dfs(int x,int Fa,int p)
{
	int num=0; MAX[x]=0;
	for (int j=lnk[x]; j; j=nxt[j])
	 if (son[j]!=Fa) Dfs(son[j],x,p);
	for (int j=lnk[x]; j; j=nxt[j])
	 if (son[j]!=Fa) c[++num]=MAX[son[j]]+w[j];
	sort(c+1,c+1+num);
	int i=1,j=num,MM=0,t=0;
	while (j>0&&c[j]>=p) now++,j--;
	while (i<j) if (c[i]+c[j]>=p) now++,b[++t]=i,i++,j--; else MM=c[i],i++;
	if (i==j) MM=c[j];
	if (j!=b[t])
	 {
	 	for (int k=j+1; k<=j+t; k++)
	  	 if (c[k-1]+c[b[t]]>=p) MM=c[k],t--;
	      else break;
	 }
	MAX[x]=MM;
}
bool Check(int x)
{ 
	now=0; Dfs(1,0,x);
	return now>=m;
}
void Add(int x,int y,int z) {w[++tot]=z; son[tot]=y; nxt[tot]=lnk[x]; lnk[x]=tot;}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=Read(); m=Read(); L=R=tot=0;
	for (int i=1,x,y,z; i<n; i++) x=Read(),y=Read(),z=Read(),Add(x,y,z),Add(y,x,z),R+=z;
	R/=m;
	while (L<=R)
	{
		int mid=(R-L>>1)+L;
		if (Check(mid)) L=mid+1; else R=mid-1;
	}
	printf("%d",R);
	return 0;
}
