#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=105,maxv=25005;
int T,n,m,ans,a[maxn],f[maxv];
inline char nc()
{
	static char buf[100000],*l,*r;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF; return *l++;
}
inline int Read()
{
	int res=0,f=1; char ch=nc(),cc=ch;
	while (ch<'0'||ch>'9') cc=ch,ch=nc();
	if (cc=='-') f=-1;
	while (ch>='0'&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}
void Work()
{
	n=Read(); m=ans=0;
	for (int i=1; i<=n; i++) a[i]=Read(),m=max(a[i],m);
	sort(a+1,a+1+n);
	for (int i=1; i<=m; i++) f[i]=0; f[0]=1;
	for (int i=1; i<=n; i++)
	{
		if (f[a[i]]) continue;
		ans++;
		for (int j=0,k=m-a[i]; j<=k; j++)
		 if (f[j]) f[j+a[i]]=1;
	}
	printf("%d\n",ans);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=Read();
	while (T--) Work();
	return 0;
}
