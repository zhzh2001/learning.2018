#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=100005;
int n,m,ans;
bool vis[maxn];
struct wjd
{
	int x,id;
	bool operator < (const wjd &a) const{
		return x<a.x;
	}
}a[maxn];
inline char nc()
{
	static char buf[100000],*l,*r;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF; return *l++;
}
inline int Read()
{
	int res=0,f=1; char ch=nc(),cc=ch;
	while (ch<'0'||ch>'9') cc=ch,ch=nc();
	if (cc=='-') f=-1;
	while (ch>='0'&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=Read(); m=0;
	for (int i=1; i<=n; i++) a[i]=(wjd){Read(),i},m=max(m,a[i].x);
	sort(a+1,a+1+n);
	memset(vis,1,sizeof(vis)); vis[0]=vis[n+1]=0;
	for (int i=0,now=1,j=1,v; i<=m; i++)
	{
		if (i>0) ans+=now;
		while (j<=n&&a[j].x==i) v=a[j].id,now+=vis[v-1]+vis[v+1]-1,vis[v]=0,j++;
	}
	printf("%d",ans);
	return 0;
}
