#include<bits/stdc++.h>
using namespace std;

#define lg long long 
#define db double
#define pb push_back
#define mp make_pair
#define ft first
#define sd second
#define pii pair<int,int>

template <class T>
void read(T& d){
	int f=1;char ch=getchar();
	for(d=0;ch>'9'||ch<'0';ch=getchar())if(ch=='-')f*=-1;
	for(;ch<='9'&&ch>='0';ch=getchar())d=d*10+ch-'0';d*=f;
}
/********************************************************/
#define MN 100005

int n;
int a[MN];
pii p[MN];
int vsd[MN];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	lg res=0;
	for(int i=1;i<=n;++i){
		read(a[i]);
		p[i].sd=i,p[i].ft=a[i];
		res+=a[i];
	}
	sort(p+1,p+n+1);
	reverse(p+1,p+n+1);
	for(int i=1;i<=n;++i){
		vsd[p[i].sd]=1;
		if(vsd[p[i].sd-1])res-=p[i].ft;
		if(vsd[p[i].sd+1])res-=p[i].ft;
	}
	printf("%lld",res);
	return 0;
}
