#include<bits/stdc++.h>
using namespace std;

#define lg long long 
#define db double
#define pb push_back
#define mp make_pair
#define ft first
#define sd second
#define pii pair<int,int>

template <class T>
void read(T& d){
	int f=1;char ch=getchar();
	for(d=0;ch>'9'||ch<'0';ch=getchar())if(ch=='-')f*=-1;
	for(;ch<='9'&&ch>='0';ch=getchar())d=d*10+ch-'0';d*=f;
}
/********************************************************/
#define MN 25005
int a[MN];
int dp[MN];
int n;

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int _T;
	read(_T);
	while(_T--){
		read(n);
		memset(dp,0,sizeof dp);
		for(int i=1;i<=n;++i)read(a[i]);
		sort(a+1,a+n+1);
		dp[0]=1;
		int res=0;
		for(int i=1;i<=n;++i){
			if(dp[a[i]]){++res;continue;}
			for(register int j=0;j+a[i]<MN;++j){
				if(dp[j]&&!dp[j+a[i]])dp[j+a[i]]=1;
			}
		}
		printf("%d\n",n-res);
	}
	return 0;
}
