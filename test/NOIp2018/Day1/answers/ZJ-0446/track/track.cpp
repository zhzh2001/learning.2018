#include<bits/stdc++.h>
using namespace std;

#define lg long long 
#define db double
#define pb push_back
#define mp make_pair
#define ft first
#define sd second
#define pii pair<int,int>

template <class T>
void read(T& d){
	int f=1;char ch=getchar();
	for(d=0;ch>'9'||ch<'0';ch=getchar())if(ch=='-')f*=-1;
	for(;ch<='9'&&ch>='0';ch=getchar())d=d*10+ch-'0';d*=f;
}
/********************************************************/
#define MN 100005
int dp[MN];
int n,m;
int ls[MN];

int LEN;

int s[MN];
int ud[MN];
int ts;

int fr[MN],nex[MN],vi[MN],tot=0,wi[MN];
void add(int x,int y,int z){
	nex[++tot]=fr[x];fr[x]=tot;vi[tot]=y;wi[tot]=z;
}

void dfs(int x,int fa){
	for(int i=fr[x];i;i=nex[i])if(vi[i]!=fa){
		dfs(vi[i],x);
		dp[x]+=dp[vi[i]];
	}
	ts=0;
	for(int i=fr[x];i;i=nex[i])if(vi[i]!=fa){
		s[++ts]=ls[vi[i]]+wi[i];
		ud[ts]=0;
	}
	sort(s+1,s+ts+1);
	while(ts&&s[ts]>=LEN)++dp[x],--ts;
	for(register int i=ts,j=1;i>j;--i){
		if(ud[i])continue;
		while(j<i&&(s[j]+s[i]<LEN||ud[j]))++j;
		if(j>=i)break;
		ud[i]=j;ud[j]=i;++dp[x];
	}
	ls[x]=0;
	for(register int i=ts;i;--i){
		if(!ud[i]){ls[x]=s[i];break;}
	}
	for(register int i=ts;i;--i){
		if(s[i]>ls[x]&&s[ud[i]]+ls[x]>=LEN){ls[x]=s[i];break;}
	}
}

bool Jg(lg len){
	LEN=len;
	memset(dp,0,sizeof (int)*(n+1));
	memset(ls,0,sizeof (int)*(n+1));
	dfs(1,1);
	if(dp[1]>=m)return 1;
	else return 0;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);
	for(int i=1,x,y,z;i<n;++i){
		read(x);read(y);read(z);
		add(x,y,z);add(y,x,z);
	}
	int L=1,R=1000000000;
	while(L<R){
		int mid=L+R+1>>1;
		if(Jg(mid))L=mid;else R=mid-1;
	}
	cout<<L;
	return 0;
}
