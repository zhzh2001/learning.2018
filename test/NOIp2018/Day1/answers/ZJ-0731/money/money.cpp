#include<iostream>
#include<algorithm>
#include<cstring>
#include<set>
#include<cstdio>
#include<queue>
using namespace std;
int a[505];bool vis[505000],tw[505];
set<int> s;
set<int>::iterator iter;
queue<int> q;
int gcd(int a,int b){
	if(b==0) return a;
	return gcd(b,a%b);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int i,j,k,m,n,T,mi,li,ct=0;
	scanf("%d",&T);
	while(T--){
		bool flag=0;
		scanf("%d",&n);
		int ans=n;
		for(i=1;i<=n;i++){
			scanf("%d",&a[i]);
		}
		memset(vis,0,sizeof(vis));
		memset(tw,0,sizeof(tw));
		s.clear();
		sort(a+1,a+n+1);
		int mmr=25000;
		for(i=1;i<=n;i++){
			for(j=1;j<i;j++){
				if(a[i]%a[j]==0){
					ans--;
					tw[i]=1;
					break;
				}
			}
		}
		for(i=1;i<=n&&!flag;i++){
			if(!tw[i]){
				for(j=1;j<i&&!flag;j++){
					if(!tw[j]){
						if(gcd(a[i],a[j])==1){
							mmr=a[i]*a[j];
							flag=1;
						}
					}
				}
			}
		}
		s.insert(0);
		for(i=1;i<=n;i++){
			if(tw[i]) continue;
			if(s.count(a[i])||a[i]>mmr){
				ans--;
				continue;
			}
			for(j=1;j*a[i]<=mmr;j++){
				for(iter=s.begin();iter!=s.end()&&(*iter+a[i]*j<=mmr);iter++){
					q.push(*iter+a[i]*j);
				}
			}
			while(!q.empty()){
				s.insert(q.front());
				q.pop();
			}
		}
		printf("%d\n",ans);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
