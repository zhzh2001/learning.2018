#include<iostream>
#include<algorithm>
#include<cstdio>
#include<queue>
using namespace std;
struct TREE{
	int to,nxt,val;
}t[100050];
int a[10000050],lt;
int last[50500],cnt;
void build(int u,int v,int w){
	t[++cnt].to=v;
	t[cnt].val=w;
	t[cnt].nxt=last[u];
	last[u]=cnt;
}
int dep[50500],f[50500][22],dis[50500],lb[50500],lg[100050];
void dfs(int fa,int x,int vw){
	dep[x]=dep[fa]+1;
	f[x][0]=fa;
	dis[x]=dis[fa]+vw;
	for(int i=1;i<=20;i++){
		f[x][i]=f[f[x][i-1]][i-1];
	}
	int e=last[x];
	while(e){
		int v=t[e].to;
		if(v!=fa){
			dfs(x,v,t[e].val);
		}
		e=t[e].nxt;
	}
}
int lca(int x,int y){
	if(dep[x]<dep[y]){
		int tmp=x;
		x=y;
		y=tmp;
	}
	while(dep[x]!=dep[y]){
		x=f[x][lg[dep[x]-dep[y]]];
	}
	if(x==y) return x;
	for(int i=20;i>=0;i--){
		if(f[x][i]!=f[y][i]){
			x=f[x][i];
			y=f[y][i];
		}
	}
	return f[x][0];
}
bool cmp(const int a,const int b){
	return a>b;
}
int main(){
	int i,j,k,m,n,u,v;
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	for(i=2;i<=100000;i++) lg[i]=lg[i>>1]+1;
	scanf("%d%d",&n,&m);
	for(i=1;i<=n-1;i++){
		scanf("%d%d",&u,&v,&lb[i]);
		build(u,v,lb[i]);build(v,u,lb[i]);
	}
	dfs(0,1,0);
	for(i=1;i<=n;i++){
		for(j=1;j<i;j++){
			int ca=lca(i,j);
			a[++lt]=dis[i]+dis[j]-2*dis[ca];
			if(lt==7001217) break;
		}
	}
	sort(lb+1,lb+lt+1,cmp);
	printf("%d",lb[1]);
	fclose(stdin);fclose(stdout);
	return 0;
}
