#include <cstdio>
#include <vector>
#include <algorithm>
const int N = 50005;
int n, m, head[N], tot;
struct edge { int to, nxt, cost; } e[N << 1];
void addedge(int x, int y, int z) {
	e[++tot] = (edge) { y, head[x], z }; head[x] = tot;
	e[++tot] = (edge) { x, head[y], z }; head[y] = tot;
}
namespace p1 {
	int max = 0, p = 0;
	void dfs(int x, int l = 0, int f = 0) {
		if (l > max) max = l, p = x;
		for (int i = head[x]; i; i = e[i].nxt) if (e[i].to != f) {
			dfs(e[i].to, l + e[i].cost, x);
		}
	}
	void start() {
		int x, y, z;
		for (int i = 1; i < n; i++) std::scanf("%d%d%d", &x, &y, &z), addedge(x, y, z);
		max = 0; dfs(1);
		max = 0; dfs(p);
		std::printf("%d\n", max);
	}
}
namespace p2 {
	int nowk = 0, root;
	int max[N], sum[N], deg[N];
	void dfs(int x, int f = 0) {
		sum[x] = max[x] = 0;
		std::vector<int> v;
		for (int i = head[x]; i; i = e[i].nxt) if (e[i].to != f) {
			dfs(e[i].to, x);
			sum[x] += sum[e[i].to];
			v.push_back(max[e[i].to] + e[i].cost);
		}
		std::sort(v.begin(), v.end());
		int l = 0, r = v.size();
		while (r > 0 && v[r - 1] >= nowk) r--, sum[x]++;
		while (l < r - 1) {
			if (v[l] + v[r - 1] >= nowk) l++, r--, sum[x]++;
			else max[x] = v[l++];
		}
		if (l < r) max[x] = v[r - 1];
	}
	bool check(int mid) {
		nowk = mid; dfs(1);
		return sum[1] >= m;
	}
	void start() {
		int x, y, z;
		for (int i = 1; i < n; i++) std::scanf("%d%d%d", &x, &y, &z), addedge(x, y, z), ++deg[x], ++deg[y];
		int l = 1, r = 5E8, ans;
		while (l <= r) {
			int mid = l + r >> 1;
			if (check(mid)) l = mid + 1, ans = mid;
			else r = mid - 1;
		}
		std::printf("%d\n", ans);
	}
}
int main() {
	std::freopen("track.in", "r", stdin);
	std::freopen("track.out", "w", stdout);
	std::scanf("%d%d", &n, &m);
	if (m == 1) p1::start();
	else p2::start();
	std::fclose(stdin); std::fclose(stdout);
	return 0;
}
