#include <cstdio>
#include <algorithm>
const int N = 50005;
int n, m, head[N], tot;
struct edge { int to, nxt, cost; } e[N << 1];
void addedge(int x, int y, int z) {
	e[++tot] = (edge) { y, head[x], z }; head[x] = tot;
	e[++tot] = (edge) { x, head[y], z }; head[y] = tot;
}
namespace p1 {
	int max = 0, p = 0;
	void dfs(int x, int l = 0, int f = 0) {
		if (l > max) max = l, p = x;
		for (int i = head[x]; i; i = e[i].nxt) if (e[i].to != f) {
			dfs(e[i].to, l + e[i].cost, x);
		}
	}
	void start() {
		int x, y, z;
		for (int i = 1; i < n; i++) std::scanf("%d%d%d", &x, &y, &z), addedge(x, y, z);
		max = 0; dfs(1);
		max = 0; dfs(p);
		std::printf("%d\n", max);
	}
}
namespace p3 {
	int val[N], tot = 0;
	void init() {
		for (int i = head[1]; i; i = e[i].nxt)
			val[++tot] = e[i].cost;
		std::sort(val + 1, val + n);
	}
	bool check(int ans) {
		int l = 1, r = n - 1, sum = 0;
		while (val[r] >= ans) sum++, r--;
		while (l < r) {
			if (val[l] + val[r] >= ans) l++, r--, sum++;
			else l++;
		}
		return sum >= m;
	}
}
namespace p2 {
	int nowk = 0, root;
	int max[N], sum[N], deg[N], flag = 1;
	void dfs(int x, int f = 0) {
		int max = 0, smax = 0;
		sum[x] = 0;
		for (int i = head[x]; i; i = e[i].nxt) if (e[i].to != f) {
			dfs(e[i].to, x);
			sum[x] += sum[e[i].to];
			int tmp = e[i].cost + p2::max[e[i].to];
			if (tmp > max) smax = max, max = tmp;
			else if (tmp > smax) smax = tmp;
		}
		if (max >= nowk && smax >= nowk) sum[x] += 2, p2::max[x] = 0;
		else if (max >= nowk) sum[x]++, p2::max[x] = smax;
		else if (max + smax >= nowk) sum[x]++, p2::max[x] = 0;
		else p2::max[x] = max;
	}
	bool check(int mid) {
		if (flag) return p3::check(mid);
		nowk = mid;
		dfs(root);
		return sum[root] >= m;
	}
	void start() {
		int x, y, z;
		for (int i = 1; i < n; i++) std::scanf("%d%d%d", &x, &y, &z), addedge(x, y, z), ++deg[x], ++deg[y], flag &= x == 1;
		if (flag) p3::init();
		int l = 1, r = 5E8, ans;
		for (int i = 1; i <= n; i++) if (deg[i] != 3) {
			root = i; break;
		}
		while (l <= r) {
			int mid = l + r >> 1;
			if (check(mid)) l = mid + 1, ans = mid;
			else r = mid - 1;
		}
		std::printf("%d\n", ans);
	}
}
int main() {
	//std::freopen("track.in", "r", stdin);
	//std::freopen("track.out", "w", stdout);
	std::scanf("%d%d", &n, &m);
	if (m == 1) p1::start();
	else p2::start();
	std::fclose(stdin); std::fclose(stdout);
	return 0;
}
