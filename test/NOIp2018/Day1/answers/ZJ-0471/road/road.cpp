#include <cstdio>
const int N = 100005;
int n, a[N], ans;
int main() {
	std::freopen("road.in", "r", stdin);
	std::freopen("road.out", "w", stdout);
	std::scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		std::scanf("%d", &a[i]);
	for (int i = 1; i <= n; i++)
		if (a[i] > a[i - 1]) ans += a[i] - a[i - 1];
	std::printf("%d\n", ans);
	std::fclose(stdin); std::fclose(stdout);
	return 0;
}
