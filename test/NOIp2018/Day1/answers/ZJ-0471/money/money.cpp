#include <cstdio>
#include <cstring>
#include <algorithm>
const int N = 105;
const int R = 25005;
int tc, a[N], n, ans;
bool knap[R];
void solve() {
	std::scanf("%d", &n); ans = 0;
	for (int i = 1; i <= n; i++) std::scanf("%d", &a[i]);
	std::sort(a + 1, a + n + 1);
	std::memset(knap, 0, sizeof knap);
	knap[0] = 1;
	for (int i = 1; i <= n; i++) {
		if (knap[a[i]]) continue;
		ans++;
		for (int j = a[i]; j < R; j++)
			knap[j] |= knap[j - a[i]];
	}
	std::printf("%d\n", ans);
}
int main() {
	std::freopen("money.in", "r", stdin);
	std::freopen("money.out", "w", stdout);
	std::scanf("%d", &tc);
	while (tc--) solve();
	std::fclose(stdin); std::fclose(stdout);
	return 0;
}
