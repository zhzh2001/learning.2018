#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#define rint register int
using namespace std;
const int maxn=50010;
typedef long long ll;
struct Edge{
	int to,nex;
	ll w;
}edges[maxn<<1];
int head[maxn],cnt=0,n,m;
inline void addEdge(int u,int v,ll w){
	edges[++cnt].to=v;
	edges[cnt].nex=head[u];
	edges[cnt].w=w;
	head[u]=cnt;
}
ll f[maxn],g[maxn],p[maxn][18],dis[maxn];
void dfs1(int u,int fa){
	int v;
	ll mx1=0,mx2=0;
	for(rint i=head[u];i!=-1;i=edges[i].nex){
		v=edges[i].to;
		if(v==fa) continue;
		p[v][0]=u;
		dis[v]=dis[u]+edges[i].w;
		dfs1(v,u);
		f[u]=max(f[u],f[v]+edges[i].w);
		if(f[v]+edges[i].w>mx1){
			mx2=mx1;
			mx1=f[v]+edges[i].w;
		}else if(f[v]+edges[i].w>mx2){
			mx2=f[v]+edges[i].w;
		}
	}
	g[u]=max(mx1+mx2,f[u]);
}

void sp1(){
	dfs1(1,0);
	ll ans=-1;
	for(rint i=1;i<=n;i++) ans=max(ans,g[i]);
	printf("%lld",ans);
	exit(0);
}

int read(){
	int x=0;bool w=false;char c=0;
	while(c>'9'||c<'0') {w|=c=='-';c=getchar();}
	while(c>='0'&&c<='9') {x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	return w?-x:x;
}

ll lread(){
	ll x=0;bool w=false;char c=0;
	while(c>'9'||c<'0') {w|=c=='-';c=getchar();}
	while(c>='0'&&c<='9') {x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	return w?-x:x;
}
ll sumw[maxn],minn;

inline bool cmp(const ll &a,const ll &b){
	return a>b;
}

void sp3(){
	dfs1(1,0);
	sort(dis+1,dis+n+1,cmp);
	int pt=1;
	int upper=2*m;
	vector<ll> vec;
	for(rint i=1;i<=m;i++){
		vec.push_back(dis[pt]+dis[upper-pt+1]);
		pt++;
	}
	sort(vec.begin(),vec.end());
	printf("%lld",vec[0]);
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(head,-1,sizeof(head));
	n=read(),m=read();
	int u,v; ll w;
	minn=100000000ll;
	bool Sp1=true;
	ll tot=0;
	for(rint i=1;i<n;i++){
		u=read(),v=read(),w=lread();
		tot+=w;
		if(u!=1) Sp1=false;
		minn=min(minn,w);
		sumw[v]=sumw[u]+w;  //��������Ч 
		addEdge(u,v,w);addEdge(v,u,w);
	}
	if(m==1){
		sp1();
		return 0;
	}else if(m==n-1){
		printf("%lld",minn);
		return 0;
	}
	if(Sp1){
		sp3();
		return 0;
	}
	return 0;
}
