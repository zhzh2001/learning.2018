#include<cstdio>
#include<cstring>
#include<algorithm>
#define rint register int
using namespace std;
const int maxn=210;
typedef long long ll;
int T,n;
int a[maxn];

int read(){
	int x=0;bool w=false;char c=0;
	while(c>'9'||c<'0') {w|=c=='-';c=getchar();}
	while(c>='0'&&c<='9') {x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	return w?-x:x;
}

ll lread(){
	ll x=0;bool w=false;char c=0;
	while(c>'9'||c<'0') {w|=c=='-';c=getchar();}
	while(c>='0'&&c<='9') {x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	return w?-x:x;
}
int que[maxn];
bool vis[maxn];
bool dfs(int dep,int lim,int fr,int mx){
	if(dep==lim+1){
		memset(vis,false,sizeof(vis));
		vis[0]=true;
		for(rint i=1;i<=lim;i++){
//			printf("has %d\n",que[i]);
			for(rint j=que[i];j<=mx;j++){
				if(vis[j-que[i]]){
					vis[j]=true;
//					printf("%d ok\n",j);
				}
			}
		}
		bool flag=true;
		for(rint i=1;i<=n;i++){
			if(!vis[a[i]]){
//				printf("connot make %d\n",a[i]);
				flag=false;
				break;
			}
		}
		return flag;
	}
	for(int i=fr+1;i<=n;i++){
		que[dep]=a[i];
		if(dfs(dep+1,lim,i,mx)) return true;
	}
	return false;
}

void solve(){
	n=read();
	bool sp=false;
	int mx=0;
	for(rint i=1;i<=n;i++){
		a[i]=read();
		mx=max(mx,a[i]);
		sp|=(a[i]==1);
	}
	if(sp){
		printf("1\n");
		return;
	}
	int l=1,r=n,ans;
	while(l<=r){
		int mid=(l+r)>>1;
//		printf("trying %d\n",mid);
		if(dfs(1,mid,0,mx)){
			ans=mid;
			r=mid-1;
		}else l=mid+1;
	}
	printf("%d\n",ans);
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		solve();
	}
	return 0;
}
