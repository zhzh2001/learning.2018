#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#define rint register int
using namespace std;
const int maxn=100010;
typedef long long ll;
int read(){
	int x=0;bool w=false;char c=0;
	while(c>'9'||c<'0') {w|=c=='-';c=getchar();}
	while(c>='0'&&c<='9') {x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	return w?-x:x;
}
int n,d[maxn];
int stmin[maxn][20],LG[maxn];
vector<int> place[10010];
void init(){
	for(int i=1,j=0;i<=n;i<<=1,j++) LG[i]=j;
	for(int i=2;i<=n;i++) if(!LG[i]) LG[i]=LG[i-1];
	for(int i=1;i<20;i++){
		for(int j=1;j+(1<<i)-1<=n;j++){
			stmin[j][i]=min(stmin[j][i-1],stmin[j+(1<<(i-1))][i-1]);
		}
	}
}

int findstart(int val,int L){
	int l=0,r=place[val].size()-1,ret;
	while(l<=r){
		int mid=(l+r)>>1;
//		printf("place[%d][%d]=%d\n",val,mid,place[val][mid]);
		if(place[val][mid]>=L) ret=mid,r=mid-1;
		else l=mid+1;
	}
	return ret;
}

int query(int l,int r){
	int k=LG[r-l+1];
	return min(stmin[l][k],stmin[r-(1<<k)+1][k]);
}

ll work(int l,int r,int level){
	ll ret=0;
	if(l>r) return 0;
	else if(l==r){
		return d[l]-level;
	}
	int val=query(l,r);
	int add=val-level;
	ret+=add;
	int st=findstart(val,l);   //该区间最小值第一个大于等于l的位置
	int siz=place[val].size();
	vector<int> vec;
	vec.push_back(l-1);
	while(st<siz&&place[val][st]<=r){
		vec.push_back(place[val][st]);
		st++;
	}
	vec.push_back(r+1);
	int SS=vec.size();
	for(int i=0;i<SS-1;i++){
//		printf("now work [%d,%d] level %d\n",vec[i]+1,vec[i+1]-1,add+level);
		ret+=work(vec[i]+1,vec[i+1]-1,add+level);
	}
	return ret;
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(rint i=1;i<=n;i++){
		d[i]=read();
		stmin[i][0]=d[i];
		place[d[i]].push_back(i);
	}
	init();
	ll ret=work(1,n,0);
	printf("%lld\n",ret);
	return 0;
}
