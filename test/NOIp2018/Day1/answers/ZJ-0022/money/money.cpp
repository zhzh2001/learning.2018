#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cassert>

using namespace std;

#define fi first
#define se second
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
#define bug(x) cerr<<#x<<" = "<<(x)<<" "
#define debug(x) cerr<<#x<<" = "<<(x)<<"\n"

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

//template<class T>void rd(T &x){
//	static int f;static char c;
//	x=0;f=1;
//	while(c=getchar(),c<48)if(c=='-')f=-1;
//	do x=x*10+(c&15);
//	while(c=getchar(),c>47);
//	x*=f;
//}
//template<class T>void prin(T x){
//	if(x<0)x=-x,putchar('-');
//	else if(!x){putchar('0');return ;}
//	static int stk[100],tp;
//	while(x)stk[tp++]=x%10,x/=10;
//	while(tp)putchar(stk[--tp]^48);
//}
//
//template<class T>void ptk(T x){prin(x),putchar(' ');}
//template<class T>void ptn(T x){prin(x),putchar('\n');}
//template<class T>void Min(T &a,T b){if(b<a)a=b;}
//template<class T>void Max(T &a,T b){if(a<b)a=b;}

int gcd(int a,int b){
	if(!b)return a;
	return gcd(b,a%b);
}
int lcm(int a,int b){
	return a/gcd(a,b)*b;
}
const int N = 105,M = 25005;
int n,a[N],mark[M];
void solve(){
	scanf("%d",&n);
	rep(i,1,n+1)scanf("%d",&a[i]);
	sort(a+1,a+1+n);
	memset(mark,0,sizeof mark);
	mark[0]=1;
	int tot=0;
	rep(i,1,n+1){
		if(!mark[a[i]]){
			++tot;
			rep(j,0,a[n]-a[i]+1)if(mark[j])mark[j+a[i]]=1;
		}
	}
	n=tot;
	cout<<n<<endl;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--)solve();
	return 0;
}
