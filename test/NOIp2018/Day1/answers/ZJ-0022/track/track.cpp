#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cassert>

using namespace std;

#define fi first
#define se second
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
#define bug(x) cerr<<#x<<" = "<<(x)<<" "
#define debug(x) cerr<<#x<<" = "<<(x)<<"\n"

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

template<class T>void rd(T &x){
	static int f;static char c;
	x=0;f=1;
	while(c=getchar(),c<48)if(c=='-')f=-1;
	do x=x*10+(c&15);
	while(c=getchar(),c>47);
	x*=f;
}
template<class T>void prin(T x){
	if(x<0)x=-x,putchar('-');
	else if(!x){putchar('0');return ;}
	static int stk[100],tp;
	while(x)stk[tp++]=x%10,x/=10;
	while(tp)putchar(stk[--tp]^48);
}

template<class T>void ptk(T x){prin(x),putchar(' ');}
template<class T>void ptn(T x){prin(x),putchar('\n');}
template<class T>void Min(T &a,T b){if(b<a)a=b;}
template<class T>void Max(T &a,T b){if(a<b)a=b;}

const int N = (int)1e5+5;

int n,m,head[N],deg[N];
struct node{
	int to,val,nxt;
}G[N<<1];
struct P_0{
	int ans;
	int dfs(int x,int par){
		int fir=0,sec=0;
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(y==par)continue;
			int lg=dfs(y,x)+G[i].val;
			Max(ans,fir+lg);
			if(lg>fir)sec=fir,fir=lg;
			else Max(sec,lg);
		}
		Max(ans,fir+sec);
		return fir;
	}
	void work(){
		dfs(1,0);
		cout<<ans<<endl;
	}
}P0;
struct P_1{
	void work(){
		int ans=(int)1e9;
		rep(x,1,n+1)
			for(int i=head[x];~i;i=G[i].nxt)
				Min(ans,G[i].val);
		cout<<ans<<endl;
	}
}P1;
struct P_2{
	int a[N],rt;
	bool check(){
		rep(i,1,n+1)if(deg[i]==n-1){
			rt=i;
			return true;
		}
		return false;
	}
	void work(){
		int tot=0;
		for(int i=head[rt];~i;i=G[i].nxt)a[++tot]=G[i].val;
		sort(a+1,a+tot+1);
		if(m==tot)cout<<a[1]<<endl;
		else {
			if(m==1)cout<<a[tot]+a[tot-1]<<endl;
			else cout<<min(a[tot-m+1]+a[tot-m],a[tot-m+2])<<endl;
		}
	}
}P2;
struct P_3{
	int rt,sum[N];
	bool check(){
		int tot=0;
		rep(i,1,n+1)if(deg[i]!=2){
			if(deg[i]==1)++tot,rt=i;
			else return false;
		}
		return tot<=2;
	}
	int Nxt(int L,int lim){
		int R=n,ans=n;
		while(L<=R){
			int mid=(L+R)>>1;
			if(sum[mid]>=lim)ans=mid,R=mid-1;
			else L=mid+1;
		}
		return ans;
	}
	bool judge(int lim){
		int pos=1;
		rep(i,0,m){
			if(sum[n]-sum[pos]<lim)return false;
			pos=Nxt(pos,sum[pos]+lim);
		}
	}
	void work(){
		memset(sum,-1,sizeof sum);
		int S=0;
		while(sum[rt]==-1){
			sum[rt]=S;
			for(int i=head[rt];~i;i=G[i].nxt){
				int y=G[i].to;
				if(sum[y]==-1){
					S+=G[i].val;
					rt=y;
					break;
				}
			}
		}
		sort(sum+1,sum+1+n);
		int L = 1 , R = sum[n],ans=1;
		while(L<=R){
			int mid=(L+R)>>1;
			if(judge(mid))L=mid+1,ans=mid;
			else R=mid-1;
		}
		cout<<ans<<endl;
	}
}P3;
struct P_4{
	int dfn[N],post[N],Mp[N],dfn_clock,par[N],len[N],dis[N];
	void pre_dfs(int x){
		Mp[dfn[x]=++dfn_clock]=x;
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(y==par[x])continue;
			par[y]=x;dis[y]=dis[x]+G[i].val,len[y]=G[i].val;
			pre_dfs(y);
		}
		post[x]=dfn_clock;
	}
	int dp[N],sum[N];
	bool judge(int lim){
		memset(dp,0,(n+1)<<2);
		memset(sum,0,(n+1)<<2);
		per(t,1,n+1){
			static int x,y;
			x=Mp[t];//现在是处理谁 自底向上处理
			static int tmp[N];
			tmp[x]=0;
			static int que[N],que_cnt;
			que_cnt=0;
			int mx=0,dmx=0,P=0;
			rep(i,t+1,post[x]+1){//处理子树所有点 
				y=Mp[i];
				if(par[y]==x){
					P=post[y];
					dp[x]+=mx;
					if(dmx<lim)que[++que_cnt]=dmx;
					else ++dp[x];
					mx=dp[y];
					dmx=len[y];
				}else {
					tmp[y]=tmp[par[y]]-dp[y]+sum[y];
					if(mx<tmp[y])mx=tmp[y],dmx=dis[y]-dis[x];
					else if(mx==tmp[y])Max(dmx,dis[y]-dis[x]);
				}
				if(dmx>=lim)i=P;
			}
			dp[x]+=mx;
			if(dmx<lim)que[++que_cnt]=dmx;
			else ++dp[x];
			if(que_cnt>1){
				static int l,r;
				sort(que+1,que+que_cnt+1);
				for(l=1,r=que_cnt;l<r;--r){
					while(l<r&&que[l]+que[r]<lim)++l;
					if(l<r){
						++dp[x];
						++l;
					}
				}
			}
			Max(dp[x],sum[x]);
			sum[par[x]]+=dp[x];
		}
		return dp[1]>=m;
	}
	void work(){
		pre_dfs(1);
		int L = 1,R=(int)5e8+5,ans=1;
		while(L<=R){
			int mid=(L+R)>>1;
			if(judge(mid))L=mid+1,ans=mid;
			else R=mid-1;
		}
		cout<<ans<<endl;
	}
}P4;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	rd(n),rd(m);
	memset(head,-1,sizeof head);
	for(int i=1,x,y,v,tot_edge=0;i<n;++i){
		rd(x),rd(y),rd(v);
		++deg[x],++deg[y];
		G[tot_edge]=(node){y,v,head[x]},head[x]=tot_edge++;
		G[tot_edge]=(node){x,v,head[y]},head[y]=tot_edge++;
	}
	if(0);
	else if(m==1)P0.work();
	else if(m==n-1)P1.work();
	else if(P2.check())P2.work();
	else if(P3.check())P3.work();
	else P4.work();
	return 0;
}
