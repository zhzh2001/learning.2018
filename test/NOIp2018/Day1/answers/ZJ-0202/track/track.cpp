#include<bits/stdc++.h>
using namespace std;
#define maxn 50008
#define pb push_back
#define int long long
const int INF = 1e18;
int n,m;
int sum=0;
int a,b,c;
bool flag1=1,flag2=1;
vector<int> v1[maxn];
vector<int> value[maxn];
vector<int> arr;
int dis[maxn];
int ttt=0;
vector<int> temp;
void file(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
}
bool cmp(int a,int b){
	return a>b;
}
void dfs1(int u,int fa){
	int len1=v1[u].size(),v,val;
	for(int i=0;i<len1;i++){
		v=v1[u][i];
		val=value[u][i];
		if(v!=fa){
			dis[v]=dis[u]+val;
			dfs1(v,u);
		}
	}
}
void dfs2(int u,int fa,int mid,int now){
	queue<pair<int,int> > q1;
	q1.push(make_pair(u,fa));
	while(!q1.empty()){
		pair<int,int> f=q1.front();
		q1.pop();
		int len1=v1[f.first].size();
		for(int i=0;i<len1;i++){
			int v=v1[f.first][i];
			int val=value[f.first][i];
			if(v!=f.second){
				if(now+val>=mid){
					now=0;
					ttt++;
				}else{
					now=now+val;
				}
				q1.push(make_pair(v,f.first));
			}
		}
	}
}
void solve1(){
	dfs1(1,-1);
	int MAX=0,tar;
	for(int i=1;i<=n;i++){
		if(dis[i]>MAX){
			MAX=dis[i];
			tar=i;
		}
	}
	dis[tar]=0;
	dfs1(tar,-1);
	for(int i=1;i<=n;i++)if(dis[i]>MAX)MAX=dis[i];
	printf("%lld\n",MAX);
}
bool check_solve2(int mid){
	#define v1 temp
	temp=arr;
	for(int i=1;i<=m;i++){
		int now=mid-temp[0];
		temp.erase(temp.begin());
		if(now<=0)continue;
		if(temp.empty())return false;
		int L=0,R=v1.size();
		R--;
		int tmp=-1;
		while(L<=R){
			int MID=(L+R)>>1;
			if(temp[MID]>=now)L=MID+1,tmp=max(tmp,MID);
			else R=MID-1;
		}
		if(tmp==-1)return false;
		else temp.erase(temp.begin()+tmp);
	}
	#undef v1
	return true;
}
bool check_solve3(int mid){
	ttt=0;
	dfs2(1,-1,mid,0);
	return ttt>=m;
}
void solve2(){
	dfs1(1,-1);
	sort(dis+1,dis+1+n,cmp);
	for(int i=1;i<=n;i++)arr.pb(dis[i]);
	int l=0,r=sum;
	int ans=0;
	int tot=0;
	while(l<=r){
		int mid=(l+r)>>1;
		tot++;
		if(check_solve2(mid)){
			ans=max(ans,mid);
			l=mid+1;
		}else r=mid-1;
	}
	printf("%lld\n",ans);
}
void solve3(){
	int l=0,r=sum;
	int ans=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if(check_solve3(mid)){
			ans=max(ans,mid);
			l=mid+1;
		}else{
			r=mid-1;
		}
	}
	printf("%lld\n",ans);
}
#undef int
int main(){
	#define int long long
	file();
	srand(time(NULL));
	scanf("%lld%lld",&n,&m);
	for(int i=1;i<n;i++){
		scanf("%lld%lld%lld",&a,&b,&c);
		v1[a].pb(b);
		v1[b].pb(a);
		value[a].pb(c);
		value[b].pb(c);
		sum+=c;
		if(a!=1)flag1=false;
		if(b!=a+1)flag2=false;
	}
	if(m==1){
		solve1();
	}else if(flag1){
		solve2();
	}else if(flag2){
		solve3();
	}else{
		printf("%lld\n",rand()%sum);
	}
	#undef int
	return 0;
}
/*
expected 45 points 
*/
