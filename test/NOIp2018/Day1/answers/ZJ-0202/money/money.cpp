#include<bits/stdc++.h>
using namespace std;
#define maxn 108
int t,n;
int arr[maxn];
bool vis[25008];
bool ok[25008];
bool flag=false;
int tot=0,a;
void init(){
	memset(vis,0,sizeof(vis));
	memset(ok,0,sizeof(ok));
	memset(arr,0,sizeof(arr));
	tot=0;
}
void file(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}
int main(){
	file();
	scanf("%d",&t);
	while(t--){
		init();
		int ans=0;
		scanf("%d",&n);
		for(int i=1;i<=n;i++){
			flag=false;
			scanf("%d",&a);
			for(int j=1;j<=sqrt(a);j++){
				if(a%j==0){
					if(vis[j]){
						flag=1;
						break;
					}
					if(a/j!=j&&vis[a/j]){
						flag=1;
						break;
					}
				}
			}
			if(!flag)ok[a]=1,vis[a]=1,ans++,arr[++tot]=a;
		}
		sort(arr+1,arr+1+tot);
		bool F;
		for(int i=1;i<=25000;i++){
			F=false;
			if(ok[i])F=1;
			for(int j=1;j<=tot;j++){
				if(i-arr[j]>0&&!F&&ok[i-arr[j]]){
					ok[i]=1;break;
				}else if(F&&i-arr[j]>0){
					if(ok[i-arr[j]]){ans--;break;}
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
