#include<bits/stdc++.h>
using namespace std;
const int N=110;
int T,n,m,a[N];
bool f[25010];
inline int read()
{
	int N=0,C=0;char tf=getchar();
	for(;!isdigit(tf);tf=getchar())C|=tf=='-';
	for(;isdigit(tf);tf=getchar())N=(N<<1)+(N<<3)+(tf^48);
	return C?-N:N;
}
inline void work()
{
	n=m=read();
	for(int i=1;i<=n;++i)a[i]=read();
	for(int k=1;k<=n;++k)
	{
		memset(f,0,sizeof(f));
		f[0]=1;
		for(int i=1;i<=n;++i)
			if(i!=k)
			{
				for(int j=a[i];j<=a[k];++j)
					f[j]|=f[j-a[i]];
				if(f[a[k]]){--m;break;}
			}
	}
	printf("%d\n",m);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read()+1;
	while(--T)work();
	return 0;
}
