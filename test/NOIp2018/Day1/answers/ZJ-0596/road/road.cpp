#include<bits/stdc++.h>
using namespace std;
const int N=100010;
int n,mnn,idd;
int mn[20][N],id[20][N],lg[N];
long long ans;
inline int read()
{
	int N=0,C=0;char tf=getchar();
	for(;!isdigit(tf);tf=getchar())C|=tf=='-';
	for(;isdigit(tf);tf=getchar())N=(N<<1)+(N<<3)+(tf^48);
	return C?-N:N;
}
inline void pre()
{
	lg[0]=-1;
	for(int i=1;i<=n;++i)lg[i]=lg[i>>1]+1;
	for(int i=1;i<=lg[n];++i)
		for(int j=1;j<=n-(1<<i)+1;++j)
			if(mn[i-1][j]<mn[i-1][j+(1<<i-1)])mn[i][j]=mn[i-1][j],id[i][j]=id[i-1][j];
			else mn[i][j]=mn[i-1][j+(1<<i-1)],id[i][j]=id[i-1][j+(1<<i-1)];
}
inline void ask(int l,int r)
{
	int t=lg[r-l+1];
	if(mn[t][l]<mn[t][r-(1<<t)+1])mnn=mn[t][l],idd=id[t][l];
	else mnn=mn[t][r-(1<<t)+1],idd=id[t][r-(1<<t)+1];
}
void work(int l,int r,int d)
{
	if(l>r)return;
	ask(l,r);
	int mnnn=mnn,iddd=idd;
	ans+=mnnn-d;
	work(l,iddd-1,mnnn),work(iddd+1,r,mnnn);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;++i)mn[0][i]=read(),id[0][i]=i;
	pre();
	work(1,n,0);
	printf("%lld\n",ans);
	return 0;
}
