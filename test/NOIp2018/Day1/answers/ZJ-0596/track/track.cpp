#include<bits/stdc++.h>
using namespace std;
const int N=50010;
int n,m,x,y,z,mx,id;
int len=1,lin[N],d[N],a[N];
struct nc{int y,nxt,v;}e[N<<1];
long long l=1,r,mid;
priority_queue<int,vector<int>,greater<int> >q;
inline int read()
{
	int N=0,C=0;char tf=getchar();
	for(;!isdigit(tf);tf=getchar())C|=tf=='-';
	for(;isdigit(tf);tf=getchar())N=(N<<1)+(N<<3)+(tf^48);
	return C?-N:N;
}
inline void ins(int x,int y,int v)
{
	e[++len].nxt=lin[x];
	lin[x]=len;
	e[len].y=y;
	e[len].v=v;
}
void dfs(int x,int f)
{
	for(int i=lin[x];i;i=e[i].nxt)
	{
		int y=e[i].y,w=e[i].v;
		if(y!=f)
		{
			d[y]=d[x]+w;
			if(d[y]>mx)mx=d[y],id=y;
			dfs(y,x);
		}
	}
}
inline void work1()
{
	for(int i=1;i<n;++i)
	{
		x=read(),y=read(),z=read();
		ins(x,y,z),ins(y,x,z);
	}
	dfs(1,0);
	memset(d,0,sizeof(d)),mx=0;
	dfs(id,0);
	printf("%d\n",mx);
}
inline bool ojbk2(int k)
{
	for(int i=1;i<n;++i)q.push(a[i]);
	int cnt=0,num=0;long long now=0;
	while(num<n-1)
	{
		while(now<k&&!q.empty()&&num<n-1)
			now+=q.top(),q.pop(),++num;
		++cnt,now=0;
	}
	return cnt>=m;
}
inline void work3()//ai=1
{
	while(l<r)
	{
		mid=l+r+1>>1;
		if(ojbk2(mid))l=mid;
		else r=mid-1;
	}
	printf("%lld\n",l);
}
inline bool ojbk(int k)
{
	int cnt=0;long long now=0;
	for(int i=1;i<n;++i)
	{
		now+=a[i];
		if(now>=k)++cnt,now=0;
	}
	if(now)++cnt;
	return cnt>=m;
}
inline void work2()//bi=ai+1
{
	bool flag=1;
	for(int i=1;i<n;++i)
	{
		x=read(),y=read(),z=read();
		r+=a[i]=z;if(x!=1)flag=0;
	}
	if(flag){work3();return;}
	while(l<r)
	{
		mid=l+r+1>>1;
		if(ojbk(mid))l=mid;
		else r=mid-1;
	}
	printf("%lld\n",l);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	if(m==1)work1();
	else work2();
	return 0;
}
