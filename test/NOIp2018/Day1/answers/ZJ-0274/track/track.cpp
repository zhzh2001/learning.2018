#include<bits/stdc++.h>
const int maxn = 50035;
const int maxm = 100035;

struct Edge
{
	int y,val;
	bool vis;
	Edge(int a=0, int b=0, bool c=0):y(a),val(b),vis(c) {}
}edges[maxm];
int n,m,deg[maxn];
int edgeTot,head[maxn],nxt[maxm];
bool chainChk,speChk;

int read()
{
	char ch = getchar();
	int num = 0, fl = 1;
	for (; !isdigit(ch); ch=getchar())
		if (ch=='-') fl = -1;
	for (; isdigit(ch); ch=getchar())
		num = (num<<1)+(num<<3)+ch-48;
	return num*fl;
}
void addedge(int u, int v, int c)
{
	deg[u]++, deg[v]++;
	edges[edgeTot] = Edge(v, c), nxt[edgeTot] = head[u], head[u] = edgeTot++;
	edges[edgeTot] = Edge(u, c), nxt[edgeTot] = head[v], head[v] = edgeTot++;
}
namespace subtask1			//VastPath
{
	int ans,dis[maxn],S;
	void dfs(int x, int fa, int c)
	{
		dis[x] = c;
		for (int i=head[x]; i!=-1; i=nxt[i])
		{
			int v = edges[i].y, w = edges[i].val;
			if (v!=fa) dfs(v, x, c+w);
		}
	}
	void solve()
	{
		dis[0] = ans = S = 0, dfs(1, 1, 0);
		for (int i=1; i<=n; i++) if (dis[i] > dis[S]) S = i;
		dfs(S, S, 0);
		for (int i=1; i<=n; i++) if (dis[i] > ans) ans = dis[i];
		printf("%d\n",ans);
	}
};
namespace subtask2
{
	int ans,tot,c[maxn];
	std::multiset<int> s;
	std::multiset<int>::iterator pt;
	bool check(int w)
	{
		int ret = 0;
		s.clear();
		for (int i=1; i<n; i++) s.insert(c[i]);
		for (int i=n-1; i; i--)
		{
			if (s.count(c[i])){
				if (c[i] >= w){
					ret++, s.erase(s.find(c[i]));
					continue;
				}
				s.erase(s.find(c[i]));
				pt = s.lower_bound(w-c[i]);
				if (pt!=s.end()){
					s.erase(pt), ret++;
				}
			}
		}
		return ret >= m;
	}
	void solve()
	{
		for (int i=0; i<edgeTot; i+=2)
			tot += edges[i].val, c[(i>>1)+1] = edges[i].val;
		std::sort(c+1, c+n);
		int l = 0, r = tot/m;
		for (int mid=(l+r)>>1; l<=r; mid=(l+r)>>1)
			if (check(mid)) ans = mid, l = mid+1;
			else r = mid-1;
		printf("%d\n",ans);
	}
};
namespace subtask3
{
	int c[maxn],cnt,tot,ans;			// ATTENTION  CNT  INSTEAD OF N!!!
	void dfs(int x, int fa)
	{
		for (int i=head[x]; i!=-1; i=nxt[i])
		{
			int v = edges[i].y;
			if (v!=fa) dfs(v, x), c[++cnt] = edges[i].val;
		}
	}
	bool check(int w)
	{
		int ret = 0, tmp = 0;
		for (int i=1; i<=cnt; i++)
		{
			tmp += c[i];
			if (tmp >= w){
				tmp = 0, ret++;
			}
		}
		return ret >= m;
	}
	void solve()
	{
		tot = cnt = 0;
		for (int i=0; i<edgeTot; i+=2)
			tot += edges[i].val, c[++cnt] = edges[i].val;
		for (int i=1; i<=cnt; i++) tot += c[i];//, printf("%d ",c[i]);puts("");
		int l = 0, r = tot;
		for (int mid=(l+r)>>1; l<=r; mid=(l+r)>>1)
			if (check(mid)) ans = mid, l = mid+1;
			else r = mid-1;
		printf("%d\n",ans);
	}
};
namespace subtask4
{
bool pthLeg;
int ans,cnt,t[maxn];

bool fndPath(int T, int x, int fa)
{
	if (x==T) return 1;
	for (int i=head[x]; i!=-1; i=nxt[i])
	{
		int v = edges[i].y;
		if ((v!=fa)&&(fndPath(T, v, x))){
			if (edges[i].vis) pthLeg = 0;
			if (edges[i^1].vis) pthLeg = 0;
			return 1;
		}
	}
	return 0;
}
bool color(int T, int x, int fa, bool c)
{
	if (x==T) return 1;
	for (int i=head[x]; i!=-1; i=nxt[i])
	{
		int v = edges[i].y;
		if (v!=fa&&color(T, v, x, c)){
			edges[i].vis = c;
			edges[i^1].vis = c;
			cnt += edges[i].val;
			return 1;
		}
	}
	return 0;
}
void search(int x)
{
	if (x==m+1){
		ans = std::max(ans, *std::min_element(t+1, t+m+1));
		return;
	}
	for (int i=1; i<=n; i++)
	for (int j=i+1; j<=n; j++)
	{
		pthLeg = 1, fndPath(j, i, i);
		if (pthLeg){
			cnt = 0;
			color(j, i, i, 1);
			t[x] = cnt;
			search(x+1);
			color(j, i, i, 0);
		}
	}
}
void solve()
{
	search(1);
	printf("%d\n",ans);
}
};
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(head, -1, sizeof head);
	n = read(), m = read();
	chainChk = speChk = 1;
	for (int i=1; i<n; i++)
	{
		int u = read(), v = read(), c = read();
		if (v!=u+1) chainChk = 0;
		if (u!=1) speChk = 0;
		addedge(u, v, c);
	}
	if (m==1){
		subtask1::solve();
		return 0;
	}
	if (speChk){
		subtask2::solve();
		return 0;
	}
	if (chainChk){
		subtask3::solve();
		return 0;
	}
	subtask4::solve();
	return 0;
}
