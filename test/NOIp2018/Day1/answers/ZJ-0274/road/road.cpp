#include<bits/stdc++.h>
const int maxn = 100035;

int n,a[maxn],ans;

int read()
{
	char ch = getchar();
	int num = 0, fl = 1;
	for (; !isdigit(ch); ch=getchar())
		if (ch=='-') fl = -1;
	for (; isdigit(ch); ch=getchar())
		num = (num<<1)+(num<<3)+ch-48;
	return num*fl;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n = read();
	for (int i=1; i<=n; i++) a[i] = read();
	for (int i=1; i<=n; i++)
		if (a[i] > a[i-1]) ans += a[i]-a[i-1];
	printf("%d\n",ans);
	return 0;
}
