#include<bits/stdc++.h>
const int maxn = 103;
const int maxNum = 50035;
const int Top = 25000;

int T,n,mx,ans,a[maxn],t[maxNum];

int read()
{
	char ch = getchar();
	int num = 0, fl = 1;
	for (; !isdigit(ch); ch=getchar())
		if (ch=='-') fl = -1;
	for (; isdigit(ch); ch=getchar())
		num = (num<<1)+(num<<3)+ch-48;
	return num*fl;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (T = read(); T; T--)
	{
		ans = n = read(), mx = 0;
		for (register int i=1; i<=n; i++) a[i] = read(), mx = mx>a[i]?mx:a[i];
		memset(t, 0, (mx+4)<<2);
		t[0] = 1;
		for (register int i=1; i<=n; i++)
			for (register int j=0; j<=mx-a[i]; j++)
				t[j+a[i]] += t[j];
		for (register int i=1; i<=n; i++)
			if (t[a[i]] > 1) ans--;
		printf("%d\n",ans);
	}
	return 0;
}
