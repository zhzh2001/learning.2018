#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn = 5e4 + 5;

int n, m, edgenum, ans;
int a[maxn], b[maxn], l[maxn], head[maxn], MAX[maxn], c[maxn];
bool check1, check2;
bool used[maxn];

struct node {
	int val, loc;
} MAX1[maxn], MAX2[maxn];

struct EDGE {
	int Next, vet, val;
} edge[maxn << 1];

inline int read() {
	int ans = 0;
	char ch = getchar(), last = ' ';
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	while (ch >= '0' && ch <= '9') ans = (ans << 1) + (ans << 3) + ch - '0', ch = getchar();
	return last == '-' ? -ans : ans;
}

void dfs(int now, int pre) {
	MAX[now] = 0;
	MAX1[now].val = MAX1[now].loc = 0;
	MAX2[now].val = MAX2[now].loc = 0;
	for (int e = head[now]; e != -1; e = edge[e].Next) {
		int nxt = edge[e].vet, len = edge[e].val;
		if (nxt == pre) continue;
		dfs(nxt, now);
		if (MAX1[nxt].val + len > MAX1[now].val) MAX2[now] = MAX1[now], MAX1[now].val = MAX1[nxt].val + len, MAX1[now].loc = nxt;
		else if (MAX1[nxt].val + len > MAX2[now].val) MAX2[now].val = MAX1[nxt].val + len, MAX2[now].loc = nxt;
	}
	if (MAX1[now].loc == MAX2[now].loc) MAX[now] = MAX1[now].val;
	else MAX[now] = MAX1[now].val + MAX2[now].val;
}

inline void add_edge(int u, int v, int w) {
	edge[++edgenum] = (EDGE) {head[u], v, w};
	head[u] = edgenum;
}

inline void solve1() {
	edgenum = 0;
	memset(head, -1, sizeof(head));
	for (int i = 1; i < n; ++i) add_edge(a[i], b[i], l[i]), add_edge(b[i], a[i], l[i]);
	dfs(1, 1);
	ans = 0;
	for (int i = 1; i <= n; ++i) ans = max(ans, MAX[i]);
	printf("%d\n", ans); 
}

inline bool checkk(int x) {
	int now, res;
	now = res = 0;
	for (int i = 1; i < n; ++i) {
		now += c[i];
		if (now >= x) now = 0, ++res;
	}
	return (res >= m);
}

inline void solve2() {
	int sum = 0, L, R, MID;
	edgenum = 0;
	memset(head, -1, sizeof(head));
	for (int i = 1; i <= n; ++i) sum += l[i], add_edge(a[i], b[i], l[i]), add_edge(b[i], a[i], l[i]);
	for (int i = 1; i < n; ++i) c[i] = l[i];
	L = 0, R = sum;
	while (L < R) {
		MID = (L + R + 1) >> 1;
		if (checkk(MID)) L = MID;
		else R = MID - 1;
	}
	printf("%d\n", L);
}

inline bool check(int x) {
	memset(used, false, sizeof(used));
	int res = 0, loc;
	for (int i = 1; i <= n; ++i)
		if (!used[i]) {
			used[i] = true;
			if (l[i] >= x) ++res;
			else {
				loc = lower_bound(l + 1, l + 1 + n, x - l[i]) - l;
				if (loc <= n && !used[loc]) ++res, used[loc] = true;
			}
		}
	return (res >= m);
}

inline void solve3() {
	sort(l + 1, l + 1 + n);
	int sum, L, MID, R;
	sum = 0;
	for (int i = 1; i <= n; ++i) sum += l[i];
	L = 0, R = sum;
	while (L < R) {
		MID = (L + R + 1) >> 1;
		if (check(MID)) L = MID;
		else R = MID - 1;
	}
	printf("%d\n", L);
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	n = read(), m = read();
	check1 = check2 = true;
	for (int i = 1; i < n; ++i) {
		a[i] = read(), b[i] = read(), l[i] = read();
		if (a[i] + 1 != b[i]) check1 = false;
		if (a[i] != 1) check2 = false;
	}
	if (m == 1) solve1();
	else if (check1) solve2();
	else if (check2) solve3();
	return 0;
}
