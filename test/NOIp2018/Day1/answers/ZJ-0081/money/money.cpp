#include <stdio.h>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxa = 3e4;
const int maxn = 105;

int T, n, mx, ans;
int a[maxn];
bool flag[maxa];

inline int read() {
	int ans = 0;
	char ch = getchar(), last = ' ';
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	while (ch >= '0' && ch <= '9') ans = (ans << 1) + (ans << 3) + ch - '0', ch = getchar();
	return last == '-' ? -ans : ans;
}

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	T = read();
	while (T--) {
		n = read();
		mx = 0;
		for (register int i = 1; i <= n; ++i) {
			a[i] = read();
			mx = max(mx, a[i]);
		}
		sort(a + 1, a + 1 + n);
		ans = 0;
		memset(flag, false, sizeof(flag));
		flag[0] = true;
		for (register int i = 1; i <= n; ++i)
			if (!flag[a[i]]) {
				++ans;
				for (register int j = a[i]; j <= mx; ++j) flag[j] |= flag[j - a[i]];
			}
		printf("%d\n", ans);
	}
	return 0;
}
