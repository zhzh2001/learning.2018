#include <cstdio>
#include <algorithm>

using namespace std;

const int maxn = 1e5 + 5;

int n;
int d[maxn], Log2[maxn];
long long ans;

struct node {
	int val, loc;
} MIN[maxn][20];

inline node query(int l, int r) {
	int k = Log2[r - l + 1];
	if (MIN[l][k].val < MIN[r - (1 << k) + 1][k].val) return MIN[l][k];
	return MIN[r - (1 << k) + 1][k];
}

void solve(int l, int r, int x) {
	node Q = query(l, r);
	ans += (Q.val - x);
	if (l <= Q.loc - 1) solve(l, Q.loc - 1, Q.val);
	if (Q.loc + 1 <= r) solve(Q.loc + 1, r, Q.val);
}

inline int read() {
	int ans = 0;
	char ch = getchar(), last = ' ';
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	while (ch >= '0' && ch <= '9') ans = (ans << 1) + (ans << 3) + ch - '0', ch = getchar();
	return last == '-' ? -ans : ans;
}

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	n = read();
	for (register int i = 1; i <= n; ++i) d[i] = read();
	Log2[1] = 0;
	for (register int i = 2; i <= n; ++i) Log2[i] = Log2[i >> 1] + 1;
	for (register int i = 1; i <= n; ++i) MIN[i][0].val = d[i], MIN[i][0].loc = i;
	for (register int j = 1; j <= Log2[n]; ++j)
		for (register int i = 1; i + (1 << j) - 1 <= n; ++i) {
			if (MIN[i][j - 1].val < MIN[i + (1 << (j - 1))][j - 1].val) MIN[i][j] = MIN[i][j - 1];
			else MIN[i][j] = MIN[i + (1 << (j - 1))][j - 1];
		}
	ans = 0;
	solve(1, n, 0);
	printf("%lld\n", ans);
	return 0;
}
