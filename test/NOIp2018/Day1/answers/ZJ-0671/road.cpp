#include<cstdio>
#include<iostream>
using namespace std;
int n,last=0,d;
long long ans=0;
inline void read(int &x){
	char ch;bool fu=false;
	while((ch=getchar())<'0'||ch>'9')if(ch=='-')fu=true;
	x=ch-'0';
	while((ch=getchar())>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0';
	if(fu)x=-x;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for(int i=1;i<=n;i++){
		read(d);
		if(d-last>0)ans+=d-last;
		last=d;
	}
	printf("%lld\n",ans);
	return 0;
}
