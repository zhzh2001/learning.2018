#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int N=5e4+5;
typedef long long ll;
int minn=100000000,n,m,head[N],AA[N],tot,num,root;
bool vis[N];
ll max_dep,sum;
struct Edge{
	int to,nxt,val;
}e[N*2];
void add(int x,int y,int z){
	e[++tot].to=y;e[tot].nxt=head[x];e[tot].val=z;head[x]=tot;
}
bool cmp(Edge a,Edge b){return a.val>b.val;}
inline void read(int &x){
	char ch;bool fu=false;
	while((ch=getchar())<'0'||ch>'9')if(ch=='-')fu=true;
	x=ch-'0';
	while((ch=getchar())>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0';
	if(fu)x=-x;
}
void dfs1(int x,int fa,ll dep){
	vis[x]=1;
	if(dep>max_dep){
		max_dep=dep;root=x;
	}
	for(int i=head[x];i;i=e[i].nxt){
		if(vis[e[i].to]||e[i].to==fa)continue;
		dfs1(e[i].to,x,dep+e[i].val);
	}
}
void dfs2(int x,int fa,ll dep){
	vis[x]=1;
	if(dep>max_dep){
		max_dep=dep;
	}
	for(int i=head[x];i;i=e[i].nxt){
		if(vis[e[i].to]||e[i].to==fa)continue;
		dfs2(e[i].to,x,dep+e[i].val);
	}
}
void dfs3(int x,int fa){
	for(int i=head[x];i;i=e[i].nxt){
		if(e[i].to==fa)continue;
		AA[++num]=e[i].val;
		dfs3(e[i].to,x);
	}
}
bool cherk3(int mid){
	int now=0,last=0;
	for(int i=1;i<=num;i++)
	{
		last+=AA[i];
		if(last>=mid){
			last=0;now++;
			if(now>=m)return true;
		}
	}
	return false;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);
	bool fai=true,fbi=true;
	for(int i=1;i<n;i++){
		int a,b,c;read(a);read(b);read(c);
		add(a,b,c);add(b,a,c);
		if(a!=1&&b!=1)fai=false;
		if((b!=a+1)&&(a!=b+1))fbi=false;
		if(c<minn)minn=c;
		sum+=c;
	}
	if(m==n-1){
		printf("%d",minn);return 0;
	}
	if(m==1){
		dfs1(1,0,0);
		memset(vis,0,sizeof(vis));
		dfs2(root,0,0);
		printf("%lld",max_dep);return 0;
	}
	if(fai){
		sort(e+1,e+tot+1,cmp);
		printf("%d",e[m*2].val);
		return 0;
	}
	if(fbi){
		dfs3(1,0);
		ll l=0,r=sum;
		while(l<r){
			ll mid=(l+r+1)>>1;
			if(cherk3(mid))l=mid;
			else r=mid-1;
		}
		printf("%lld",l);
		return 0;
	}
	return 0;
}
