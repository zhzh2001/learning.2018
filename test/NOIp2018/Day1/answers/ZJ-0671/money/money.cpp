#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int n,maxn=25000,ans,T,a[25005],num;
bool p[25005],p2[25005];
inline void read(int &x){
	char ch;bool fu=false;
	while((ch=getchar())<'0'||ch>'9')if(ch=='-')fu=true;
	x=ch-'0';
	while((ch=getchar())>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0';
	if(fu)x=-x;
}
int gc(int x,int y){
	if(y==0)return x;
	return gc(y,x%y);
}
bool cmp(int x,int y){return x<y;}
void dfs(){
	for(int i=1;i<=num;i++){
	  bool df=false;
	  for(int j=a[i];j<=maxn&&!df;j++)
	    if(p[j-a[i]]){
	    	if(p[j]!=p2[j])
	    	 df=true;
		}
	  if(!df)continue;
	   ans++;
	   for(int j=a[i];j<=maxn;j++)
	     p2[j]|=p2[j-a[i]]&1;
    }
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	while(T--){
		read(n);
		memset(p,0,sizeof(p));p[0]=1;
		memset(p2,0,sizeof(p2));p2[0]=1;
		bool f1=false;num=0;maxn=25000;ans=0;
		for(int i=1;i<=n;i++){
			scanf("%d",&a[i]);
			if(a[i]==1)f1=true;
		}
		if(f1){printf("1\n");continue;}
		if(n==1){printf("1\n");continue;}
		if(n==2){
			if(a[1]>a[2])swap(a[1],a[2]);
			if(a[2]%a[1]==0)printf("1\n");
			else printf("2\n");
			continue;
		}	
		sort(a+1,a+1+n,cmp);
		if(n==3){
			if(gc(a[1],a[2])==1&&gc(a[1],a[3])==1&&gc(a[2],a[3])==1){
				if(a[1]*a[2]-a[1]-a[2]<=a[3])printf("2\n");
				else printf("3\n");continue;
			}
			if(a[3]%a[1]==0&&a[2]%a[1]==0)printf("1\n");continue;
		}
		for(int i=1;i<=n;i++)
		  for(int j=1;j<i;j++)
			 if(a[j]!=0&&a[i]%a[j]==0)a[i]=0;
		for(int i=1;i<=n;i++)
		 if(a[i])a[++num]=a[i];
		if(num==1){
			printf("%d\n",num);continue;
		}
//		for(int i=1;i<num;i++)
//		  for(int j=i+1;j<=num;j++)
//		     if(gc(a[i],a[j])==1)
//		    	if(1LL*a[i]*a[j]-a[i]-a[j]<maxn)maxn=1LL*a[i]*a[j]-a[i]-a[j];
//		while(a[num]>maxn)num--;
//		
		for(int i=1;i<=num;i++)
		  for(int j=a[i];j<=maxn;j++)
		  p[j]|=p[j-a[i]]&1;
		dfs();
		printf("%d\n",ans);
	}
	return 0;
}
