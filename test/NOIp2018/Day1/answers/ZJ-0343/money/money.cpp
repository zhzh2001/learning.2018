#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define nc() getchar()
inline int read(){
	int x=0;char ch=nc();for(;ch<'0'||ch>'9';ch=nc());
	for(;ch<='9'&&ch>='0';x=x*10+ch-48,ch=nc());return x;
}
#define N 100030
int i,j,k,m,n,x,y,T,f[N],mx,a[220],ans;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(T=read();T--;){
		memset(f,0,sizeof f),mx=0;
		for(n=ans=read(),i=1;i<=n;++i){	
			a[i]=read();
			if(f[a[i]])--ans;
			f[a[i]]=1;
			if(a[i]>mx)mx=a[i];
		}
		for(i=0;i<=mx;++i)
			for(j=1;j<=n;++j)f[i+a[j]]+=f[i];
		for(i=1;i<=n;++i)if(f[a[i]]>1)--ans;
		printf("%d\n",ans);
	}
}

