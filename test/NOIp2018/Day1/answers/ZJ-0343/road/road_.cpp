#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define nc() getchar()
inline int read(){
	int x=0;char ch=nc();for(;ch<'0'||ch>'9';ch=nc());
	for(;ch<='9'&&ch>='0';x=x*10+ch-48,ch=nc());return x;
}
#define N 100010
int i,j,k,m,n,x,y,a[N],ans,lc,mn;
#define min(a,b) ((a)<(b)?(a):(b))
void dfs(int l,int r,int d){
	if(l>r)return;
	if(l==r){ans+=a[l]-d;return;}
	int L=l,mn=0x7f7f7f7f;
	for(int i=l;i<=r;++i)if(a[i]-d<mn)mn=a[i]-d;
	ans+=mn;
	for(int i=l;i<=r;++i)if(a[i]-mn-d==0){
		int o=i;
		while(a[i]-mn-d==0)++i;
		dfs(L,o-1,d+mn),L=i;
		--i;
	}
	dfs(L,r,d+mn);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road_.out","w",stdout);
	for(n=read(),i=1;i<=n;++i)a[i]=read();
//	mn=0x7f7f7f7f;
//	for(i=1;i<=n;++i)if(a[i]<mn)mn=a[i];
	dfs(1,n,0);printf("%d",ans);
}
