#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define nc() getchar()
inline int read(){
	int x=0;char ch=nc();for(;ch<'0'||ch>'9';ch=nc());
	for(;ch<='9'&&ch>='0';x=x*10+ch-48,ch=nc());return x;
}
#define N 100010
int i,j,k,m,n,x,y,a[N],ans,lc,mn,q[N],q1,q2;
struct node{
	int mn,dw;
}T[N<<2];
#define min(a,b) ((a)<(b)?(a):(b))
inline void up(int k){
	T[k].mn=min(T[k<<1].mn,T[k<<1|1].mn);
}
void build(int k,int l,int r){
	if(l==r){T[k].mn=a[l];return;}
	int m=l+r>>1;
	build(k<<1,l,m),build(k<<1|1,m+1,r),up(k);
}
inline void down(int k){
	T[k<<1].mn-=T[k].dw,T[k<<1|1].mn-=T[k].dw;
	T[k<<1].dw+=T[k].dw,T[k<<1|1].dw+=T[k].dw;
	T[k].dw=0;
}
int ask(int k,int l,int r,int L,int R){
	if(l>=L&&r<=R){return T[k].mn;}
	int m=l+r>>1;down(k);
	int ll=0x7f7f7f7f,rr=0x7f7f7f7f;
	if(L<=m)ll=ask(k<<1,l,m,L,R);
	if(R>m)rr=ask(k<<1|1,m+1,r,L,R);up(k);
	return min(ll,rr);
}
void askq(int k,int l,int r,int L,int R){
	if(l==r&&r<=R&&l>=L){q[++q2]=l;return;}
	int m=l+r>>1;down(k);
	if(T[k<<1].mn==0&&L<=m)askq(k<<1,l,m,L,R);
	if(T[k<<1|1].mn==0&&R>m)askq(k<<1|1,m+1,r,L,R);up(k);
}
void add(int k,int l,int r,int L,int R,int v){
	if(l>=L&&r<=R){T[k].mn-=v,T[k].dw+=v;return;}
	int m=l+r>>1;down(k);
	if(L<=m)add(k<<1,l,m,L,R,v);
	if(R>m)add(k<<1|1,m+1,r,L,R,v);up(k);
}
void dfs(int l,int r){
	if(l>r)return;
	if(l==r){ans+=ask(1,1,n,l,r);return;}
	int o=ask(1,1,n,l,r);ans+=o,add(1,1,n,l,r,o);
	int qq1=q2+1;askq(1,1,n,l,r);int qq2=q2;
	int L=l;
	for(int i=qq1;i<=qq2;++i)dfs(L,q[i]-1),L=q[i]+1;
	dfs(L,r);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	for(n=read(),i=1;i<=n;++i)a[i]=read();
	build(1,1,n),q1=1,q2=0;
	dfs(1,n);
	printf("%d",ans);
}
