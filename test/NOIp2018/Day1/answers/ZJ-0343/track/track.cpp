#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define nc() getchar()
inline int read(){
	int x=0;char ch=nc();for(;ch<'0'||ch>'9';ch=nc());
	for(;ch<='9'&&ch>='0';x=x*10+ch-48,ch=nc());return x;
}
#define N 100050
int i,j,k,m,n,x,y,cnt,flag1,flag2,vis[N],q[N],ans,last[N],top[N],sz[N],dep[N],rk[N],id[N],son[N],now,w[N],fa[N],T[N],dis[N];
struct edge{
	int to,next;
}e[N<<1];
inline void add(int u,int v){
	e[++cnt]=(edge){v,last[u]},last[u]=cnt;
	e[++cnt]=(edge){u,last[v]},last[v]=cnt;
}
void dfs1(int x,int d){
	sz[x]=1,dep[x]=d;
	for(int i=last[x],y;i;i=e[i].next)if((y=e[i].to)!=fa[x]){
		fa[y]=x,dfs1(y,d+1),sz[x]+=sz[y];
		if(sz[y]>sz[son[x]])son[x]=y;
	}
}
void dfs2(int x,int d){
	top[x]=d,rk[id[x]=++now]=x;if(son[x])dfs2(son[x],d);
	for(int i=last[x],y;i;i=e[i].next)if((y=e[i].to)!=fa[x]&&y!=son[x])dfs2(y,y);
}
inline int askl(int l,int r){
	int ans=0;
	for(;top[l]!=top[r];){
		if(dep[top[l]]<dep[top[r]])swap(l,r);
		ans+=T[id[l]]-T[id[top[l]]-1];
		l=fa[top[l]];
	}
	if(dep[l]>dep[r])swap(l,r);
	return ans+T[id[r]]-T[id[l]-1];
}
inline void bfs(int x){
	int r=1,l=1;q[r]=x;
	dis[x]=0,vis[x]=1;
	while(l<=r){
		int o=q[l++];
		for(int i=last[o];i;i=e[i].next)if(!vis[e[i].to])
			q[++r]=e[i].to,vis[e[i].to]=1,dis[e[i].to]=dis[o]+w[e[i].to];
	}
}
inline bool check(int x){
	int o=m;
	for(int i=n+1;i<=now;++i)if(w[i]>=x)--o,vis[i]=1;
	int l=n+1,r=now-(m-o);
	while(l<r){
		while(w[r]+w[l]<x&&l<r)++l;
		if(l>=r)break;
		--r,++l,--o;
	}
	return o<=0;
}
inline bool Check(int x){
	int o=m;
	int l=1,r=1;
	while(r<=now){
		while(r<=now&&askl(rk[l],rk[r])<x)++r;
		if(r>now)break;
		--o,l=r+1,++r;
	}
	return o<=0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	for(n=read(),m=read(),flag1=flag2=i=1;i<n;++i){
		x=read(),y=read(),w[i+n]=read();
		if(x!=1)flag1=0;
		if(y-x!=1)flag2=0;
		add(x,i+n),add(i+n,y);
	}
	dfs1(1,0),dfs2(1,1);
	for(i=1;i<=now;++i)T[i]+=T[i-1]+w[rk[i]];
	if(m==1){
		bfs(1);int mx=0,loc=0;
		for(i=1;i<=n;++i)if(dis[i]>mx)mx=dis[i],loc=i;
		memset(vis,0,sizeof vis);
		memset(dis,0,sizeof dis);
		bfs(loc);
		mx=0;
		for(i=1;i<=n;++i)if(dis[i]>mx)mx=dis[i];
		printf("%d",mx);
	}
	if(flag1){
		sort(w+n+1,w+now+1);
		int l=0,r=500001000;
		while(l<=r){
			int M=l+r>>1;
			if(check(M))l=M+1;
			else r=M-1;
		}
		printf("%d",l-1);
	}
	if(flag2){
		int l=0,r=500001000;
		while(l<=r){
			int M=l+r>>1;
			if(Check(M))l=M+1;
			else r=M-1;
		}
		printf("%d",l-1);
	}
	printf("%d\n",rand());
}
