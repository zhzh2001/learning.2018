#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define nc() getchar()
inline int read(){
	int x=0;char ch=nc();for(;ch<'0'||ch>'9';ch=nc());
	for(;ch<='9'&&ch>='0';x=x*10+ch-48,ch=nc());return x;
}
#define N 100050
int i,j,k,m,n,x,y,cnt,ans,last[N],top[N],sz[N],dep[N],rk[N],id[N],son[N],now,w[N],fa[N],T[N];
struct edge{
	int to,next;
}e[N<<1];
inline void add(int u,int v){
	e[++cnt]=(edge){v,last[u]},last[u]=cnt;
	e[++cnt]=(edge){u,last[v]},last[v]=cnt;
}
void dfs1(int x,int d){
	sz[x]=1,dep[x]=d;
	for(int i=last[x],y;i;i=e[i].next)if((y=e[i].to)!=fa[x]){
		fa[y]=x,dfs1(y,d+1),sz[x]+=sz[y];
		if(sz[y]>sz[son[x]])son[x]=y;
	}
}
void dfs2(int x,int d){
	top[x]=d,rk[id[x]=++now]=x;if(son[x])dfs2(son[x],d);
	for(int i=last[x],y;i;i=e[i].next)if((y=e[i].to)!=fa[x]&&y!=son[x])dfs2(y,y);
}
inline int askl(int l,int r){
	int ans=0;
	for(;top[l]!=top[r];){
		if(dep[top[l]]<dep[top[r]])swap(l,r);
		ans+=T[id[l]]-T[id[top[l]]-1];
		l=fa[top[l]];
	}
	if(dep[l]>dep[r])swap(l,r);
	return ans+T[id[r]]-T[id[l]-1];
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track_.out","w",stdout);
	for(n=read(),m=read(),i=1;i<n;++i){
		x=read(),y=read(),w[i+n]=read();
		add(x,i+n),add(i+n,y);
	}
	dfs1(1,0),dfs2(1,1);
	for(i=1;i<=now;++i)T[i]+=T[i-1]+w[rk[i]];
	if(m==1){
		ans=0;
		for(i=1;i<=n;++i)for(j=1;j<=n;++j){
			int o=askl(i,j);
			if(o>ans)ans=o;
		}
		printf("%d",ans);
	}
}
