#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
#define F first
#define S second
typedef long long ll;
template<typename _T>
inline void read(_T &x) {
  x=0; bool fl=0; char ch=getchar();
  for(;(ch<'0' || ch>'9') && ch!='-';ch=getchar());
  if(ch=='-') {fl=1; ch=getchar();}
  for(;ch>='0' && ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+(ch^48);
  if(fl) x=-x;
}
//===============================================================
const int N=110,R=25010;
int n,a[N];
bool can[R];
int main() {
#ifdef LOCAL
#else
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
#endif
  int T; read(T);
  for(;T--;) {
    read(n); int Ma=0,ans=0;
    for(int i=1;i<=n;i++) {
      read(a[i]),Ma=max(Ma,a[i]);
    }
    sort(a+1,a+n+1);
    for(int i=1;i<=Ma;i++) can[i]=0; can[0]=1;
    for(int i=1;i<=n;i++) {
      if(can[a[i]]) continue;
      ++ans; int v=a[i];
      for(register int j=v;j<=Ma;j++)
	can[j]|=can[j-v];
    }
    printf("%d\n",ans);
    //fprintf(stderr,"%d\n",ans);
  }
  return 0;
}
/*
2
4
3 19 10 6
5
11 29 13 19 17

*/
