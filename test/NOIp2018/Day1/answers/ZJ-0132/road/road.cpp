#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
#define F first
#define S second
typedef long long ll;
template<typename _T>
inline void read(_T &x) {
  x=0; bool fl=0; char ch=getchar();
  for(;(ch<'0' || ch>'9') && ch!='-';ch=getchar());
  if(ch=='-') {fl=1; ch=getchar();}
  for(;ch>='0' && ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+(ch^48);
  if(fl) x=-x;
}
//===============================================================
const int N=100010;
ll a[N],n;
ll ans=0;
int main() {
#ifdef LOCAL
#else
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
#endif
  read(n);
  for(int i=1;i<=n;i++) read(a[i]);
  ll cur=0;
  for(int i=1;i<=n;i++) {
    ans+=max(0ll,a[i]-cur);
    cur=a[i];
  }
  printf("%lld\n",ans);
  return 0;
}
