#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
#define F first
#define S second
typedef long long ll;
typedef pair<int,ll> pil;
template<typename _T>
inline void read(_T &x) {
  x=0; bool fl=0; char ch=getchar();
  for(;(ch<'0' || ch>'9') && ch!='-';ch=getchar());
  if(ch=='-') {fl=1; ch=getchar();}
  for(;ch>='0' && ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+(ch^48);
  if(fl) x=-x;
}
//===============================================================
const int N=50010;
int n,m; ll all;
int head[N],nxt[N<<1],to[N<<1],co[N<<1],lst=0;
inline void adde(int x,int y,int c) {
  nxt[++lst]=head[x]; to[lst]=y; co[lst]=c; head[x]=lst;
}
int fa[N],va[N],dep[N];
void Build(int u) {
  for(int i=head[u];i;i=nxt[i])
    if(to[i]!=fa[u]) {
      int v=to[i];
      fa[v]=u; dep[v]=dep[u]+1;
      va[v]=co[i];
      Build(v);
    }
}
namespace JuHua {
  ll seq[N];int ind=0;
  ll bd;
  int DP() {
    ind=0; int ret=0;
    for(int i=2;i<=n;i++) {
      if(va[i]>=bd) ++ret;
      else seq[++ind]=va[i];
    }
    sort(seq+1,seq+ind+1);
    int p=ind;
    for(int i=1;i<p;i++) {
      if(seq[i]+seq[p]<bd) continue;
      --p; ++ret;
    }
    return ret;
  }
  void main() {
    ll L=1,R=all/m;
    ll ret=0;
    while(L<=R) {
      ll mid=(L+R)>>1;
      bd=mid;
      if(DP()>=m) {L=mid+1; ret=mid;}
      else R=mid-1;
    }
    printf("%lld\n",ret);
    return; 
  }
}
namespace Solver {
  pil f[N];
  ll bd;
  ll seq[N],ind=0;
  int vis[N],col=0;
  void DP(int u) {
    f[u]=mp(0,0ll);
    for(int i=head[u];i;i=nxt[i])
      if(to[i]!=fa[u]) {
	int v=to[i];
	DP(v);
	f[u].F+=f[v].F;
      }
    ind=0;
    for(int i=head[u];i;i=nxt[i])
      if(to[i]!=fa[u]) {
	int v=to[i];
	if(f[v].S<bd)
	  seq[++ind]=f[v].S;
      }
    sort(seq+1,seq+ind+1);
    //printf("u=%d\n",u);
    //printf("ind=%lld:",ind); for(int i=1;i<=ind;i++) printf("%lld ",seq[i]); puts("");
    int p=ind;
    int &ret=f[u].F;
    ++col;
    for(int i=1;i<=ind && i<p;i++) {
      if(seq[i]+seq[p]<bd) continue;
      vis[i]=vis[p]=col;
      ++ret; --p;
    }
      
    f[u].S=va[u];
    for(int i=ind;i>=1;i--)
      if(vis[i]!=col) {
	f[u].S=va[u]+seq[i];
	break;
      }
    if(f[u].S>=bd) ++f[u].F;
  }
  void Print(int u,int dep) {
    for(int i=1;i<=dep;i++) printf("  ");
    fprintf(stdout,"%d:%d\n",u,va[u]);
    for(int i=head[u];i;i=nxt[i])
      if(to[i]!=fa[u]) Print(to[i],dep+1);
  }
  ll Long(int u) {
    ll Ma=0;
    for(int i=head[u];i;i=nxt[i])
      if(to[i]!=fa[u]) {
	Ma=max(Ma,Long(to[i]));
      }
    return Ma+va[u];
  }
  void main() {
    /*
    bd=26282; DP(1);
    for(int i=1;i<=n;i++) if(!f[i].F) {if(Long(i)!=f[i].S) throw;}
    for(int i=1;i<=n;i++)
      if(fa[i]==728 && f[i].F) {
	fprintf(stderr,"%d:{%d,%lld}\n",i,f[i].F,f[i].S);
	Print(i,0);
      }
    return;
    //*/
    //Print(1,0);
    ll L=1,R=all/m;
    ll ret=0;
    while(L<=R) {
      ll mid=(L+R)>>1;
      bd=mid; DP(1);
      if(f[1].F>=m) {L=mid+1; ret=mid;}
      else R=mid-1;
    }
    printf("%lld\n",ret);
    return;
  }
}
signed main() {
#ifdef LOCAL
#else
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
#endif
  read(n); read(m);
  bool juhua=1;
  for(int i=1;i<n;i++) {
    int x,y,c; read(x); read(y); read(c);
    juhua&=(x==1);
    adde(x,y,c); adde(y,x,c); all+=c;
  }
  Build(1);
  /*
  for(int i=1;i<=n;i++) printf("%d ",fa[i]); puts("");
  for(int i=1;i<=n;i++) printf("%d ",va[i]); puts("");
  */
  if(juhua) JuHua::main();
  else Solver::main();
  return 0;
}
/*
8 4
1 2 3
1 3 1
1 4 4
2 5 5
2 6 2
4 7 3
4 8 2

6 4
1 2 3
2 3 4
3 4 5
4 5 4
5 6 3

7 4
1 2 2
1 3 3
2 4 2
2 5 3
3 6 2
3 7 3

9 3
1 2 6
2 3 3
3 4 5
4 5 10
6 2 4
7 2 9
8 4 7
9 4 4
*/
