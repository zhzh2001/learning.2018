#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int T;
int n,ans;
int arr[105];
int flag[25005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		ans=0;
		memset(flag,0,sizeof flag);
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			scanf("%d",&arr[i]);
		sort(arr+1,arr+n+1);
		for(int i=1;i<=n;i++)
			if(!flag[arr[i]]){
				ans++;
				flag[arr[i]]=true;
				for(int j=1;j<=25000-arr[i];j++)
					flag[j+arr[i]]|=flag[j];
			}
		printf("%d\n",ans);
	}
	return 0;
}

