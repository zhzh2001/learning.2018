#include<cstdio>
using namespace std;
int n,m;
int vet[100005],nxt[100005],head[50005],val[100005],vis[50005];
int edgenum;
int maxdis,maxdisid;
int datastaflag1,datastaflag2,datastaflag3;
int a[50005],b[50005],l[50005];
int lens[50005];
int ans,it,tt;
int minn,mini,minj;
int flag[50005];
double sum;
double min(double x,double y){
	if(x>y)
		return y;
	return x;
}
double abs(double x){
	if(x>0)
		return x;
	return -x;
}
void add(int u,int v,int value){
	vet[++edgenum]=v;
	nxt[edgenum]=head[u];
	val[edgenum]=value;
	head[u]=edgenum;
}
void dfs(int u,int dis){
	if(dis>maxdis){
		maxdis=dis;
		maxdisid=u;
	}
	vis[u]=1;
	for(int e=head[u];e;e=nxt[e]){
		int v=vet[e];
		if(!vis[v])
			dfs(v,dis+val[e]);
	}
	vis[u]=0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m==1)
		datastaflag1=true;
	datastaflag2=true;
	datastaflag3=true;
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&a[i],&b[i],&l[i]);
		if(a[i]+1!=b[i])
			datastaflag2=false;
		if(a[i]!=1)
			datastaflag3=false;
		lens[a[i]]=l[i];
		sum+=l[i];
		add(a[i],b[i],l[i]);
		add(b[i],a[i],l[i]);
	}
	if(datastaflag1){
		maxdis=0;
		maxdisid=1;
		dfs(1,0);
//		puts("---------");
		maxdis=0;
		dfs(maxdisid,0);
		printf("%d\n",maxdis);
		return 0;
	}
	//if m==1 then the ans is the D of the tree
	if(datastaflag2){
		sum=sum/m;
//		printf("%d\n",sum);
		ans=1e9+7;
		it=1;
		while(m--){
			tt=0;
//			printf("%d:\n",m+1);
			while(abs(sum-tt-lens[it])<abs(sum-tt)){
				tt+=lens[it];
				it++;
//				printf("%d %d\n",tt+lens[it]-sum,tt-sum);
			}
			ans=min(ans,tt);
		}
		printf("%d\n",ans);
		return 0;
	}
	//if b[i] == a[i] + 1  the tree is lower to the link
	if(datastaflag3){
		sum=sum/m;
		ans=1e9+7;
		while(m--){
			minn=1e9+7;
			for(int i=1;i<n;i++)
				for(int j=i+1;j<n;j++)
					if(!flag[i]&&!flag[j])
						if(abs(sum-l[i]+l[j])<minn){
							minn=l[i]+l[j];
							mini=i;
							minj=j;
						}
			ans=min(ans,l[mini]+l[minj]);
			flag[mini]=flag[minj]=1;
		}
		printf("%d\n",ans);
		return 0;
	}
	return 0;
}
