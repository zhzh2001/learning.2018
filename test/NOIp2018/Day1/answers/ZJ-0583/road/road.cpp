#include<bits/stdc++.h>
#define rep(q,a,b) for(int q(a),q##_end_(b);q<=q##_end_;++q)
#define dep(q,a,b) for(int q(a),q##_end_(b);q>=q##_end_;--q)
#define mem(a,b) memset(a,b,sizeof a )
using namespace std;
void in(int &r) {
	char c;
	r=0;
	while(c=getchar(),!isdigit(c));
	do r=(r<<1)+(r<<3)+(c^48);
	while(c=getchar(),isdigit(c));
}
const int mn=100005;
//bool cur1;
int H[mn],n;
long long ans;
namespace p70 {
	void solve(int l,int r,int v) {
		if(l>r)return;
		if(l==r) {
			ans+=H[l]-v;
			return;
		}
		int Min=1e9;
		rep(q,l,r)Min=min(Min,H[q]-v);
		ans+=Min;
		int last=l;
		rep(q,l,r) {
			if(H[q]==Min+v) {
				solve(last,q-1,v+Min);
				last=q+1;
			}
		}
		solve(last,r,Min+v);
	}
	int main() {
		solve(1,n,0);
		printf("%lld\n",ans);
		return 0;
	}
}
namespace p100 {
	struct segment_tree {
		int at[mn<<2],minv[mn<<2],At,Min,y1,y2;
		void replace(int o) {
			if(minv[o<<1]>minv[o<<1|1])at[o]=at[o<<1|1],minv[o]=minv[o<<1|1];
			else at[o]=at[o<<1],minv[o]=minv[o<<1];
		}
		void build(int o,int l,int r) {
			if(l==r)minv[o]=H[l],at[o]=l;
			else {
				int mid=l+r>>1;
				build(o<<1,l,mid);
				build(o<<1|1,mid+1,r);
				replace(o);
			}
		}
		void ask(int o,int l,int r) {
			if(y1<=l&&r<=y2) {
				if(minv[o]<Min)Min=minv[o],At=at[o];
			} else {
				int mid=l+r>>1;
				if(y1<=mid)ask(o<<1,l,mid);
				if(y2>mid)ask(o<<1|1,mid+1,r);
			}
		}
		int ask(int l,int r) {
			if(l>r)return 0;
			this->y1=l;
			this->y2=r;
			Min=1e9,At=-1;
			ask(1,1,n);
			return Min;
		}
	} an;
	void solve(int l,int r,int v) {
		if(l>r)return;
		int Min=an.ask(l,r),now=an.At;
		if(Min==v)solve(l,now-1,v),solve(now+1,r,v);
		else ans+=Min-v,solve(l,r,Min);
	}
	int main() {
		an.build(1,1,n);
		solve(1,n,0);
		printf("%lld\n",ans);
		return 0;
	}
}
//bool cur2;
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	//    printf("%lf\n",(&cur2-&cur1)/1024.0/1024.0);
	in(n);
	rep(q,1,n)in(H[q]);
	if(n<=1000)p70::main();
	else p100::main();
	return 0;
}
