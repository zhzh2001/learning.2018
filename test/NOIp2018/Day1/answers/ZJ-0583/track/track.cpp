#include<bits/stdc++.h>
#define rep(q,a,b) for(int q(a),q##_end_(b);q<=q##_end_;++q)
#define dep(q,a,b) for(int q(a),q##_end_(b);q>=q##_end_;--q)
#define mem(a,b) memset(a,b,sizeof a )
using namespace std;
void in(int &r) {
	char c;
	r=0;
	while(c=getchar(),!isdigit(c));
	do r=(r<<1)+(r<<3)+(c^48);
	while(c=getchar(),isdigit(c));
}
const int mn=100005;
#define link(a,b,c) link_edge(a,b,c),link_edge(b,a,c)
#define link_edge(a,b,c) to[++cnt1]=b,ne[cnt1]=head[a],head[a]=cnt1,cost[cnt1]=c
#define travel(x) for(int q(head[x]);q;q=ne[q])
int head[mn],ne[mn<<1],to[mn<<1],cnt1,cost[mn<<1],n,m;
namespace p0 {
	int Max_val,At,dis[mn];
	void fi_dfs(int f,int x) {
		if(dis[x]>=Max_val)At=x,Max_val=dis[x];
		travel(x)if(to[q]!=f) {
			dis[to[q]]=dis[x]+cost[q];
			fi_dfs(x,to[q]);
		}
	}
	int main() {
		Max_val=0,At=-1;
		fi_dfs(0,1);
		int at1=At;
		Max_val=0,dis[at1]=0;
		fi_dfs(0,at1);
		printf("%d\n",Max_val);
		return 0;
	}
}
struct node {
	int u,v,cost;
	bool operator <(const node &A)const {
		return cost>A.cost;
	}
} edge[mn];
namespace p1 {
	int main() {
		sort(edge+1,edge+n);
		if(2*m<=n-1) {
			int Min=1e9;
			rep(q,1,m)Min=min(edge[q].cost+edge[2*m+1-q].cost,Min);
			printf("%d\n",Min);
		} else {
			int Min=edge[2*m-(n-1)].cost;
			int At=2*m-(n-1)+1;
			m=n-1-m;
			rep(q,1,m)Min=min(edge[At+q].cost+edge[At+2*m+1-q].cost,Min);
			printf("%d\n",Min);
		}
		return 0;
	}
}

namespace p2 {
	int val[mn];
	bool check(int v) {
		int now=0,cnt=0;
		rep(q,1,n-1) {
			now+=val[q];
			if(now>=v) {
				++cnt,now=0;
				if(cnt>=m)return 1;
			}
		}
		return 0;
	}
	int main() {
		rep(q,1,n-1)val[edge[q].u]=edge[q].cost;
		int l=0,r=5e8,ans=-1;
		while(l<=r) {
			int mid=l+r>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
}
namespace p55 {
//	set<int>::iterator it;
//	set<int> son[mn];
//	int sum,vl,adv[mn];
//    void meg(int a,int b){
//	    if(son[a].size()>son[b].size())swap(son[a],son[b]),swap(adv[a],adv[b]);
//	    for(set<int>::iterator now=son[a].begin();now!=son[a].end();++now){
//		     it=son[b].lower_bound(vl-(*now)-adv[a]-adv[b]);
//           set<int>::iterator last=++now;
//		     if(it!=son[b].end())son[a].erase(now);
//		}
//	}
//	void dfs(int f,int x){
//	    son[x].insert(0);
//	    travel(x)if(to[q]!=f)dfs(x,to[q]),adv[to[q]]+=cost[q],meg(to[q],x);
//	}
//	bool check(int v){
//	    vl=v,sum=0;
//	    rep(q,1,n)son[q].clear();
//	    dfs(0,1);
//	    return sum>=m;
//	}
//	int main() {
//	    int l=0,r=5e8,ans=-1;
//	    while(l<=r){
//		   int mid=l+r>>1;
//		   if(check(mid))l=mid+1,ans=mid;
//		   else r=mid-1;
//		}
//		printf("%d\n",ans);
//		return 0;
//	}
    int val[mn];
	bool check(int v) {
		int now=0,cnt=0;
		rep(q,1,n-1) {
			now+=val[q];
			if(now>=v) {
				++cnt,now=0;
				if(cnt>=m)return 1;
			}
		}
		return 0;
	}
	int main() {
		rep(q,1,n-1)val[q]=edge[q].cost;
		int l=0,r=5e8,ans=-1;
		while(l<=r) {
			int mid=l+r>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
}
int main() {
    freopen("track.in","r",stdin);
    freopen("track.out","w",stdout);
	in(n),in(m);
	int a,b,c;
	bool is_jy=1,is_list=1;
	rep(q,2,n) {
		in(a),in(b),in(c),link(a,b,c);
		edge[q-1]= {a,b,c};
		if(a!=1)is_jy=0;
		if(b!=a+1)is_list=0;
	}
	if(m==1)p0::main();
	else if(is_jy)p1::main();
	else if(is_list)p2::main();
	else p55::main();
	return 0;
}
