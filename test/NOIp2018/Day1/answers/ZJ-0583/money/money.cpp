#include<bits/stdc++.h>
#define rep(q,a,b) for(int q(a),q##_end_(b);q<=q##_end_;++q)
#define dep(q,a,b) for(int q(a),q##_end_(b);q>=q##_end_;--q)
#define mem(a,b) memset(a,b,sizeof a )
using namespace std;
void in(int &r) {
	char c;
	r=0;
	while(c=getchar(),!isdigit(c));
	do r=(r<<1)+(r<<3)+(c^48);
	while(c=getchar(),isdigit(c));
}
const int mn=105;
int  n,qw[mn];
namespace p80 {
	int Max,que[1005];
	bool mark[1005];
	int main() {
		mem(mark,0);
		Max=0;
		rep(q,1,n)Max=max(Max,qw[q]);
		sort(qw+1,qw+n+1);
		int len=0,ans=0;
		que[++len]=0;
		rep(q,1,n)if(!mark[qw[q]]) {
			++ans;
			int last=len;
			rep(w,1,last) {
				rep(r,0,Max) {
					if(que[w]+r*qw[q]>Max)break;
					if(!mark[que[w]+r*qw[q]]) {
						mark[que[w]+r*qw[q]]=1;
						que[++len]=que[w]+r*qw[q];
					}
				}
			}
		}
		printf("%d\n",ans);
		return 0;
	}
}
namespace p100 {
	int Max;
	bool mark[25005];
	int main() {
		mem(mark,0);
		Max=0;
		rep(q,1,n)Max=max(Max,qw[q]);
		sort(qw+1,qw+n+1);
		int ans=0;
		mark[0]=1;
		rep(q,1,n)if(!mark[qw[q]]) {
			++ans;
			rep(w,qw[q],Max)if(!mark[w]&&mark[w-qw[q]])mark[w]=1;
		}
		printf("%d\n",ans);
		return 0;
	}
}
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
//    printf("%lf",(&cur2-&cur1)/1024.0/1024.0);
	int T;
	in(T);
	while(T--) {
		in(n);
		rep(q,1,n)in(qw[q]);
	    if(n<=25)p80::main();
		else p100::main();
	}
	return 0;
}
