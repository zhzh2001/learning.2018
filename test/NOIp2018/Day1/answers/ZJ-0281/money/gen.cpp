#include <cstdio>
#include <algorithm>
#include <cstring>
#include <ctime>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
#define per(i, a, b) for(int i = (a); i >= (b); i--)
#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define ll long long
#define MAXA 25000

using namespace std;

int main()
{
	freopen("money.in", "w", stdout);
	srand(time(NULL));
	int T = 20, n = 100;
	printf("%d\n", T);
	rep(i, 1, T)
	{
		printf("%d\n", n);
		rep(i, 1, n)
			printf("%d ", rand() % 25000 + 1);
		printf("\n");
	}
	return 0;
}
