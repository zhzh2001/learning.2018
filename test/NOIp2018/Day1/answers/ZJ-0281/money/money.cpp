#include <cstdio>
#include <algorithm>
#include <cstring>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
#define per(i, a, b) for(int i = (a); i >= (b); i--)
#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define ll long long
#define N 105
#define M 25005

using namespace std;

int T, n, m;
ll amax;
int a[N];
bool vis[M];

void init()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
}

int main()
{
	init();
	scanf("%d", &T);
	while(T--)
	{
		m = 0;
		memset(a, 0, sizeof(a));
		memset(vis, 0, sizeof(vis));
		scanf("%d", &n);
		rep(i, 1, n) scanf("%d", a + i);
		sort(a + 1, a + 1 + n);
		amax = a[n];
		vis[0] = true;
		rep(i, 1, n) if(!vis[a[i]])
		{
			m++;
			rep(j, a[i], amax)
			{
				if(vis[j - a[i]] == true)
					vis[j] = true;
			}
		}
		printf("%d\n", m);
	}
	return 0;
}
