#include <cstdio>
#include <algorithm>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
#define per(i, a, b) for(int i = (a); i >= (b); i--)
#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define ll long long
#define N 100005

using namespace std;

ll n, cnt, ans, dmin;
int d[N], id[N];
bool visl[N];

bool cmp(const int& a, const int& b)
{
	if(d[a] > d[b]) return true;
	else return false;
}

void init()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
}

int main()
{
	init();
	scanf("%lld", &n);
	rep(i, 1, n) scanf("%d", d + i);
	rep(i, 1, n) id[i] = i;
	sort(id + 1, id + n + 1, cmp);
	dmin = d[id[n]];
	cnt = 0; ans = 0;
	d[0] = -1;
	rep(i, 1, n)
	{
		int pos = id[i];
		if(d[pos] != d[id[i - 1]])
			ans += cnt;
		if((!visl[pos - 1]) && (!visl[pos + 1]))
			cnt++;
		else if(visl[pos - 1] && visl[pos + 1])
			cnt--;
		visl[pos] = true;
	}
	ans += cnt * dmin;
	printf("%lld\n", ans);
	return 0;
}
