#include <cstdio>
#include <algorithm>
#include <cstring>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
#define per(i, a, b) for(int i = (a); i >= (b); i--)
#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define ll long long
#define M 100050

using namespace std;

int n, m, u, v, c, cp;
int d[100][100];

int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.ans", "w", stdout);
	scanf("%d%d", &n, &m);
	if(m == 1)
	{
		memset(d, 0x7f, sizeof(d));
		rep(i, 1, n - 1)
		{
			scanf("%d%d%d", &u, &v, &c);
			d[u][v] = d[v][u] = c;
		}
		rep(k, 1, n)
		{
			rep(i, 1, n)
			{
				rep(j, 1, n) if(i != j && j != k && i != k)
				{
					if(1ll * d[i][k] + 1ll * d[k][j] < 1ll * d[i][j])
					{
						d[i][j] = d[j][i] = d[i][k] + d[k][j];
					}
				}
			}
		}
		int ans = 0;
		rep(i, 1, n) rep(j, 1, n) if(i != j && d[i][j] > ans) ans = d[i][j];
		printf("%d\n", ans);
	}
	else printf("%d\n", (n - 1) / m);
	return 0;
}
