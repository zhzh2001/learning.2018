#include <cstdio>
#include <algorithm>
#include <cstring>
#include <ctime>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
#define per(i, a, b) for(int i = (a); i >= (b); i--)
#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define ll long long

using namespace std;

int main()
{
	freopen("track.in", "w", stdout);
	srand(time(NULL));
	int n = rand() % 50000 + 100, m = rand() % (n - 1) + 1;
	printf("%d %d\n", n, m);
	rep(i, 2, n)
		printf("%d %d %d\n", i - 1, i, 1);
//	rep(i, 2, n)
//	{
//		int u = rand() % (i - 1) + 1;
//		int w = rand() % 1000 + 1;
//		printf("%d %d %d\n", u, i, w);
//	}
	return 0;
}
