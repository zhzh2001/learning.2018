#include <cstdio>
#include <algorithm>
#include <cstring>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
#define per(i, a, b) for(int i = (a); i >= (b); i--)
#define min(a, b) ((a) > (b) ? (b) : (a))
#define ll long long
#define M 100050
#define INF (1 << 30)

using namespace std;

int n, m, u, v, c, cp;
int nxt[M], head[M], to[M];
ll w[M], d[M];
bool flag2, flag3;

void add(int u, int v, int c)
{
	to[++cp] = v;
	w[cp] = c;
	nxt[cp] = head[u];
	head[u] = cp;
}

namespace subtask_one
{
	void dfs(int x, int fa)
	{
		for(int i = head[x]; i != -1; i = nxt[i])
		if(to[i] != fa) {
			d[to[i]] = d[x] + w[i];
			dfs(to[i], x);
		}
	}
	ll solve()
	{
		memset(d, 0, sizeof(d));
		dfs(1, -1);
		int idmax = 0;
		d[0] = -1;
		rep(i, 1, n) if(d[idmax] < d[i])
			idmax = i;
		memset(d, 0, sizeof(d));
		dfs(idmax, -1);
		idmax = 0; d[0] = -1;
		rep(i, 1, n) if(d[idmax] < d[i])
			idmax = i;
		return d[idmax];
	}
}

namespace subtask_two
{
	ll solve()
	{
		sort(d + 1, d + n);
		if(m * 2 <= n - 1)
		{
			int delta = n - 1 - 2 * m;
			ll mn = INF;
			rep(i, delta + 1, n - 1)
				mn = min(mn, d[i] + d[n - i + delta]);
			return mn;
		}
		else if(m * 2 > n - 1)
		{
			int delta = n - 1 - m;
			ll mn = INF;
			rep(i, 1, delta)
				mn = min(mn, d[i] + d[n - i]);
			mn = min(mn, d[delta + 1]);
			return mn;
		}
	}
}

namespace subtask_three
{
	bool check(ll mn)
	{
		int pos = 1, cnt = 0;
		ll cur = d[pos];
		while(pos < n - 1)
		{
			while(cur < mn && pos < n - 1)
				cur += d[++pos];
			cnt++;
			cur = 0;
		}
		if(cnt >= m) return true;
		else return false;
	}
	ll binary_search(ll l, ll r)
	{
		ll lp = l, rp = r, mid;
		while(lp < rp)
		{
			mid = ((lp + rp + 1) >> 1);
			if(!check(mid)) rp = mid - 1;
			else lp = mid;
		}
		return lp;
	}
	ll solve()
	{
		ll dsum = 0;
		rep(i, 1, n - 1)
			dsum += d[i];
		return binary_search(0, (long long)(dsum / m) + 1);
	}
}

void init()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
}

int main()
{
	init();
	cp = 1;
	memset(head, -1, sizeof(head));
	scanf("%d%d", &n, &m);
	flag2 = true;
	flag3 = true;
	rep(i, 1, n - 1)
	{
		scanf("%d%d%d", &u, &v, &c);
		if(v != u + 1) flag3 = false;
		if(u != 1) flag2 = false;
		add(u, v, c);
		add(v, u, c);
		d[i] = c;
	}
	if(m == 1) printf("%lld\n", subtask_one::solve());
	else if(flag3 == true) printf("%lld\n", subtask_three::solve());
	else if(flag2 == true) printf("%lld\n", subtask_two::solve());
	else printf("19260817\n");
	return 0;
}
