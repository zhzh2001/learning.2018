#include <bits/stdc++.h>
#define res register int
#define ll long long
#define pb push_back
#define mp make_pair
#define fr first
#define sc second
#define gc getchar()
#define INF 1000000007
#define MAXN 50005
using namespace std;

inline int read()
{
	int ch=gc,f=0;
	int x=0;
	while(ch<'0'||ch>'9')
	{
		f|=ch=='0';
		ch=gc;
	}
	while(ch>='0'&&ch<='9')
	{
		x=x*10+ch-'0';
		ch=gc;
	}
	return f?-x:x;
}
struct eric
{
	int to,next,v;
}edge[MAXN<<1];
int head[MAXN],size=1;
int n,m;
int lim;
int top,d[MAXN],dis[MAXN],f[MAXN],used[MAXN],pre[MAXN],nxt[MAXN];
inline void add(int x,int y,int z)
{
	edge[++size].to=y;
	edge[size].v=z;
	edge[size].next=head[x];
	head[x]=size;
}
inline int find(int x)
{
	if(x>top)
	return x;
	return !used[x]?x:(nxt[x]=find(nxt[x]));
}
inline void dfs(int pos,int fa)
{
	for(int i=head[pos];i;i=edge[i].next)
	{
		int v=edge[i].to;
		if(v==fa)
		continue;
		dfs(v,pos);
		f[pos]+=f[v];
	}
	nxt[top=0]=1;
	for(int i=head[pos];i;i=edge[i].next)
	{
		int v=edge[i].to;
		if(v==fa)
		continue;
		d[++top]=dis[v]+edge[i].v;
		if(d[top]>=lim)
		++f[pos],--top;
		used[top]=0;
		pre[top]=top-1;
		nxt[top]=top+1;
	}
	sort(d+1,d+top+1);
	pre[top+1]=top;
	for(int i=1;i<=top;i++)
	{
		if(used[i])
		continue;
		int to=lower_bound(d+1,d+top+1,lim-d[i])-d;
		used[i]=1;
		to=find(to);
		used[i]=0;
//		while((used[to]||to==i)&&to<=top)
//		to=nxt[to];
		if(to>top)
		continue;
		used[i]=1;
		pre[nxt[i]]=pre[i];
		nxt[pre[i]]=nxt[i];
		++f[pos];
		used[to]=1;
		pre[nxt[to]]=pre[to];
		nxt[pre[to]]=nxt[to];
	}
	dis[pos]=d[pre[top+1]];
}
inline bool check(int lim)
{
	memset(dis,0,sizeof dis);
	memset(f,0,sizeof f);
	dfs(1,0);
	return f[1]>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	int x,y,z;
	for(int i=1;i<n;i++)
	{
		x=read(),y=read(),z=read();
		add(x,y,z);
		add(y,x,z);
	}
	int l=1,r=INF;
	while(l<r)
	{
		int mid=l+r>>1;
		if(check(lim=mid))
		l=mid+1;
		else
		r=mid;
	}
	printf("%d\n",l-1);
	return 0;
}
