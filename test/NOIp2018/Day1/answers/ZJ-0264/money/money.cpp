#include <bits/stdc++.h>
#define res register int
#define ll long long
#define mp make_pair
#define fr first
#define sc second
#define pb push_back
#define gc getchar()
#define INF 1000000007
#define MAXN 205
using namespace std;

inline int read()
{
	int ch=gc,f=0;
	int x=0;
	while(ch<'0'||ch>'9')
	{
		f|=ch=='0';
		ch=gc;
	}
	while(ch>='0'&&ch<='9')
	{
		x=x*10+ch-'0';
		ch=gc;
	}
	return f?-x:x;
}
int n,a[MAXN],f[250005],ans;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--)
	{
		ans=n=read();
		memset(f,0,sizeof f);
		f[0]=1;
		for(int i=1;i<=n;i++)
		a[i]=read();
		sort(a+1,a+n+1);
		for(int i=1;i<=n;i++)
		{
			if(f[a[i]])
			{
				ans--;
				continue;
			}
			for(int j=a[i];j<=a[n];j++)
			f[j]|=f[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
