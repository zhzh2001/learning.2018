#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
const int N=50010;
int k,n,m,x,y,z,l,r;
int h[N],nx[N<<1],t[N<<1],w[N<<1],num;
int a[N],A[N];
int f[N],len[N];
int mx,id;
bool c1[N],c2[N];
void Add(int x,int y,int z) {
	t[++num]=y;w[num]=z;nx[num]=h[x];h[x]=num;
}
void Dfs(int x,int y,int z) {
	if(z>mx) mx=z,id=x;
	for(int i=h[x];i;i=nx[i])
		if(t[i]!=y) Dfs(t[i],x,z+w[i]);
}
int Get() {
	mx=id=0;
	Dfs(1,0,0);
	mx=0;
	Dfs(id,0,0);
	return mx;
}
void DP(int x,int y) {
	for(int i=h[x];i;i=nx[i])
		if(t[i]!=y) DP(t[i],x);
	int cnt=0,Cnt=0;
	for(int i=h[x];i;i=nx[i])
		if(t[i]!=y) {
			f[x]+=f[t[i]];
			if(len[t[i]]+w[i]>=mx) f[x]++;else a[++cnt]=len[t[i]]+w[i];
		}
	if(cnt) {
		sort(a+1,a+cnt+1);
		int Res=0,j=cnt;
		for(int i=1;i<j;i++)
			if(a[i]+a[j]>=mx) j--,Res++;
		if(!Res) len[x]=a[cnt];
		else {
			f[x]+=Res;
			if(Res*2<cnt) {
				for(int i=cnt-(Res*2);i<=cnt;i++) A[++Cnt]=a[i];
				for(int i=1;i<=Res;i++) if((i>1&&!c1[i-1])||A[i]+A[Cnt-i+1]<mx) c1[i]=0;else c1[i]=1;
				for(int i=Res;i;i--) if((i<Res&&!c2[i+1])||A[i]+A[Cnt-i]<mx) c2[i]=0;else c2[i]=1;
				for(int i=Cnt;i>Res;i--)
					if((i==Cnt||c1[Cnt-i])&&(i==Res+1||c2[Cnt-i+1])) {
						len[x]=A[i];return;
					}
				for(int i=1;i<=Res;i++) if(A[i]+A[Cnt-i+1]<mx) {
					len[x]=A[i];return;
				}
			}
		}
	}
}
bool Check(int tmp) {
	memset(f,0,sizeof(f));
	memset(len,0,sizeof(len));
	mx=tmp;
	DP(1,0);
	if(f[1]>=m) return 1;return 0;
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++) {
		scanf("%d%d%d",&x,&y,&z);
		Add(x,y,z);Add(y,x,z);
	}
	l=1;r=Get();
	while(l<=r) {
		int Mid=(l+r)>>1;
		if(Check(Mid)) l=Mid+1;else r=Mid-1;
	}
	cout<<r<<endl;
	return 0;
}
