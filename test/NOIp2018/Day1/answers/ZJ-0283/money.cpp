#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
const int N=110;
const int M=25010;
int k,n,m,T;
int Ans,mx;
int a[N],id[M];
bool b[N],f[M];
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--) {
		memset(b,0,sizeof(b));memset(id,0,sizeof(id));memset(f,0,sizeof(f));
		scanf("%d",&n);Ans=n;mx=0;
		for(int i=1;i<=n;i++) scanf("%d",&a[i]);sort(a+1,a+n+1);
		for(int i=1;i<=n;i++) {
			if(id[a[i]]) {b[i]=1;continue;}
			id[a[i]]=i,mx=max(mx,a[i]);
		}
		for(int i=1;i<=n;i++)
			if(!b[i]) {
				f[a[i]]=1;
				for(int j=1;j+a[i]<=mx;j++)
					if(f[j]) {
						f[j+a[i]]=1;if(id[j+a[i]]) b[id[j+a[i]]]=1;
					}
			}
		for(int i=1;i<=n;i++) if(b[i]) Ans--;
		printf("%d\n",Ans);
	}
	return 0;
}
