#include<cstdio>
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
const int N=100010;
vector<int>g[10010];
int k,n,m;
int a[N],l[N],r[N];
int mn[N<<2];
int c[N];
int Ans;
void Init(int x,int l,int r) {
	if(l==r) {
		mn[x]=a[l];return;
	}
	int Mid=(l+r)>>1;
	Init(x<<1,l,Mid);Init(x<<1|1,Mid+1,r);
	mn[x]=min(mn[x<<1],mn[x<<1|1]);
}
int Find1(int x,int l,int r,int R,int y) {
	if(l>R||mn[x]>=y) return 0;
	if(l==r) return l;
	int Mid=(l+r)>>1;
	if(r<=R) {
		if(mn[x<<1|1]>=y) return Find1(x<<1,l,Mid,R,y);
		return Find1(x<<1|1,Mid+1,r,R,y);
	}
	int t=Find1(x<<1|1,Mid+1,r,R,y);
	if(t) return t;return Find1(x<<1,l,Mid,R,y);
}
int Find2(int x,int l,int r,int L,int y) {
	if(r<L||mn[x]>=y) return n+1;
	if(l==r) return l;
	int Mid=(l+r)>>1;
	if(l>=L) {
		if(mn[x<<1]>=y) return Find2(x<<1|1,Mid+1,r,L,y);
		return Find2(x<<1,l,Mid,L,y);
	}
	int t=Find2(x<<1,l,Mid,L,y);
	if(t<=n) return t;return Find2(x<<1|1,Mid+1,r,L,y);
}
void Update(int x,int y) {
	for(;x<=n;x+=x&-x) c[x]+=y;
}
int Query(int x) {
	int Ans=0;
	for(;x;x-=x&-x) Ans+=c[x];
	return Ans;
}
void Up(int l,int r,int x) {
	Update(l,x);Update(r+1,-x);
}
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++) scanf("%d",&a[i]),Up(i,i,a[i]),g[a[i]].push_back(i);
	Init(1,1,n);
	l[1]=0;for(int i=2;i<=n;i++) l[i]=Find1(1,1,n,i-1,a[i]);
	r[n]=n+1;for(int i=1;i<n;i++) r[i]=Find2(1,1,n,i+1,a[i]);
	for(int i=0;i<=10000;i++) {
		int Mj=(int)g[i].size();
		for(int j=0;j<Mj;j++){
			int id=g[i][j];
			int x=Query(id);
			if(!x) continue;
			Up(l[id]+1,r[id]-1,-x);Ans+=x;
		}
	}
	cout<<Ans<<endl;
	return 0;
}
