#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int t,n,maxx,a[105];
bool vis[105];
int judge(int a,int b,int c)
{
	for(int i=1;i<=maxx/a;i++)
		for(int j=1;j<=maxx/b;j++)
			if(a*i+b*j==c)
				return 1;
	return 0;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		int ans=0;
		scanf("%d",&n);
		memset(a,0,sizeof(a));
		memset(vis,true,sizeof(vis));
		for(int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		maxx=a[n];
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++)
				if(a[j]%a[i]==0)
					vis[j]=false;
		for(int i=1;i<=n;i++)
		{
			for(int j=i+1;j<=n;j++)
			{
				for(int k=j+1;k<=n;k++)
					if(vis[k]&&judge(a[i],a[j],a[k]))
						vis[k]=false;
			}
		}
		for(int i=1;i<=n;i++) if(vis[i]) ans++;
		printf("%d\n",ans);
	}
	return 0;
}
