#include<bits/stdc++.h>
using namespace std;
const int N=2e5+7;
int a[N];
bool dp[N];
int read(){
	int n;
	scanf("%d",&n);
	return n;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while (T--)
	{
		int ans=0;
		int n=read();
		for (int i=1;i<=n;i++)
			a[i]=read();
		sort(a+1,a+1+n);
		dp[0]=1;
		for (int i=1;i<=n;i++)
		if (dp[a[i]]==0)
		{
			ans++;
			for (int j=a[i];j<=25000;j++)
				dp[j]|=dp[j-a[i]];
		}
		for (int i=0;i<=25000;i++)
			dp[i]=0;
		cout<<ans<<endl;
	}
}

