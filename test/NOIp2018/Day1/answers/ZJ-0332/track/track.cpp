#include<bits/stdc++.h>
using namespace std;
const int N=2e5+7;
vector<int> v[N],si[N],son[N];
int tmp2[N],tmp1[N],fa[N],vfa[N];
int now[N];
bool boo[N];
int n,m;
int zhan[N],deep[N];
void dfs(int x){
	for (int i=0;i<si[x].size();i++)
	{
		int p=si[x][i];
		if (fa[p]==0)
		{
			vfa[p]=v[x][i];
			deep[p]=deep[x]+1;
			fa[p]=x;
			dfs(p);
			son[x].push_back(p);
		}
	}
}
bool mmp(int x,int y){
	return x>y;}
bool check1(int x,int n,int ne,int vv){
	int l=1,r=n;
	while (l<r)
	{
		if (l==x) l++;
		if (r==x) r--;
		while(((tmp1[l]+tmp1[r])<vv)&&(r>l)) r--;
		if (r==x) r--;
		if ((r>l)&&((tmp1[l]+tmp1[r])>=vv))
		{
			l++;
			r--;
			ne--;
		}
	}
	if (ne) return 0; return 1;
	
}
bool check(int vv)
{
	int ans=0;
	for (int i=1;i<=n;i++) now[i]=0;
	for (int i=1;i<=n;i++)
	{
		if (ans>=m) return 1;
		int num1=0,num2=0;
		int x=zhan[i];
		int zong=son[x].size();
		if (zong)
		{
		
			for (int j=0;j<zong;j++)
				tmp1[j+1]=now[son[x][j]];
			sort(tmp1+1,tmp1+zong+1,mmp);
			int l=1,r=zong;
			if ((zong>1)&&((tmp1[1]+tmp1[2])>=vv))
			{
			while (l<r)
			{
				while(((tmp1[l]+tmp1[r])<vv)&&(r>l)) r--;
				if ((r>l)&&((tmp1[l]+tmp1[r])>=vv))
				{
					l++;
					r--;
					num1++;
				}
			}
			}
			for (int j=1;j<=zong;j++)
			{
				if (vfa[x]>=tmp1[j]) 
				{
					tmp2[j-1]=vfa[x];
					for (int k=j+1;k<=zong;k++)
						tmp2[k]=tmp1[k];
					break;
				}
				tmp2[j-1]=tmp1[j];
				if (j==zong) tmp2[zong]=vfa[x];
			}
			if ((zong)&&((tmp2[1]+tmp2[0])>=vv))
			{			
			l=0,r=zong;
			while (l<r)
			{
				while(((tmp2[l]+tmp2[r])<vv)&&(r>l)) r--;
				if ((r>l)&&((tmp2[l]+tmp2[r])>=vv))
				{
					l++;
					r--;
					num2++;
				}
			}	
			}
			if (num2>num1) {ans+=num2;now[x]=0;continue;}
			else 
			{
				ans+=num1;
				if ((num1*2)==zong) now[x]=0;
				else
				{
					if (num1==0) 
						now[x]=tmp1[1]; 
					else
					{
						int l=1,r=zong;
						while (l<r)
						{
							int mid=(l+r)>>1;
							if (check1(mid,zong,num1,vv)) r=mid;else l=mid+1;
						}
						now[x]=tmp1[l];
					}
				}
			}
		}
		now[x]+=vfa[x];	
		if (now[x]>=vv) {now[x]=0;ans++;}
	}
	return (ans>=m);
}
bool mp(int x,int y){
	return deep[x]>deep[y];}
int read(){
	char ch=getchar(),f=1;;
	while ((ch<'0')||(ch>'9'))
	{
		if (ch=='-') f=-1;
		ch=getchar();
	}
	int o=0;
	while ((ch>='0')&&(ch<='9'))
	{
		o=(o<<3)+(o<<1)+ch-'0';
		ch=getchar();
	}
	return o*f;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	int zong=0;
	for (int i=1;i<n;i++)
	{
		int l=read(),r=read(),vv=read();
		zong+=vv;
		si[l].push_back(r);
		v[l].push_back(vv);
		si[r].push_back(l);
		v[r].push_back(vv);
	}
	fa[1]=1;
	dfs(1);
	for (int i=1;i<=n;i++)
		zhan[i]=i;
	sort(zhan+1,zhan+1+n,mp);
	int l=1;
	int r=zong/m;
	while (l<r)
	{
		int mid=(l+r+1)>>1;
		if (check(mid)) l=mid;
		else r=mid-1;
	}
	cout<<l<<endl;
}
	
