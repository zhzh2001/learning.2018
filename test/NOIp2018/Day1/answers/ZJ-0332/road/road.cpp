#include<bits/stdc++.h>
using namespace std;
int read(){
	int n;
	scanf("%d",&n);
	return n;
}
const int N=2e5+7;
int b[N],ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n=read();
	for (int i=1;i<=n;i++)
		b[i]=read();
	for (int i=1;i<=n;i++)
		ans+=max(0,b[i]-b[i-1]);
	cout<<ans<<endl;
}
