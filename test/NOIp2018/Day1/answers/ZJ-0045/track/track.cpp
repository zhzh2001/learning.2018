#include <bits/stdc++.h>
#define rep(i,a,b) for(int i(a),i##_END_(b);i<=i##_END_;i++)
#define drep(i,a,b) for(int i(a),i##_END_(b);i>=i##_END_;i--)
#define erep(k,G,o) for(int k=G.HEAD[o];k;k=G.NXT[k])
#define File(_) freopen(#_".in","r",stdin),freopen(#_".out","w",stdout)
template<class T> inline bool tomax(T &a,T b){return a<b?a=b,1:0;}
template<class T> inline bool tomin(T &a,T b){return a>b?a=b,1:0;}
template<int N,int M,class T> struct Link{
	int HEAD[N],NXT[M],tot;T W[M];
	void add(int x,T w){NXT[++tot]=HEAD[x];W[HEAD[x]=tot]=w;}
	T& operator [] (int x){return W[x];}
};
bool cur1;
const int MN=50005;
struct Edge{int to,w;};
Link<MN,MN<<1,Edge> G;
int n,m,sum=0;
namespace PLink{
	int dis[MN];
	bool check(int mid){
		int p=1,ned=m,s=0;
		while(p<=n){
			while(p<=n&&s<mid){
				s+=dis[p];
				p++;
			}
			if(s>=mid){
				ned--;
				s=0;
			}
		}
		return ned<=0;
	}
	int main_(){
		rep(i,1,n)
			erep(k,G,i){
				if(G[k].to==i+1){
					dis[i]=G[k].w;
					break;
				}
			}
		int L=1,R=sum,mid,ans;
		while(L<=R){
			mid=L+R>>1;
			if(check(mid))
				L=mid+1,ans=mid;
			else
				R=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
}
namespace PMEql1{
	void dfs(int o,int f,int dis,int &p,int &mx){
		if(tomax(mx,dis))p=o;
		erep(k,G,o){
			int v=G[k].to,w=G[k].w;
			if(v==f)continue;
			dfs(v,o,dis+w,p,mx);
		}
	}
	int main_(){
		int p=1,mx=0;
		rep(i,1,2)
			dfs(p,0,0,p,mx);
		printf("%d\n",mx);
		return 0;
	}
}
namespace P100{
	int ned,now;
	int dfs(int o,int f){
		std::multiset<int> s;
		std::multiset<int>::iterator it;
		erep(k,G,o){
			int v=G[k].to;
			if(v==f)continue;
			int p=G[k].w+dfs(v,o);
			if(p>=now)ned--;
			else s.insert(p);
		}
		int ans=0;
		while(!s.empty()){
			int x=(*s.begin());
			s.erase(s.begin());
			if((it=s.lower_bound(now-x))!=s.end()){
				ned--;s.erase(it);
				continue;
			}
			tomax(ans,x);
		}
		return ans;
	}
	bool check(int mid){
		now=mid;ned=m;
		dfs(1,0);
		return ned<=0;
	}
	int main_(){
		int L=1,R=sum,mid,ans;
		while(L<=R){
			mid=L+R>>1;
			if(check(mid))
				L=mid+1,ans=mid;
			else
				R=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
}
bool cur2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	File(track);
	scanf("%d%d",&n,&m);
	bool flag_Link=true;
	rep(i,2,n){
		int a,b,l;
		scanf("%d%d%d",&a,&b,&l);
		if(b!=a+1)flag_Link=false;
		G.add(a,(Edge){b,l});
		G.add(b,(Edge){a,l});
		sum+=l;
	}
	if(m==1)return PMEql1::main_();
	if(flag_Link)return PLink::main_();
	return P100::main_();
}
