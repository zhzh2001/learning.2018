#include <bits/stdc++.h>
#define rep(i,a,b) for(int i(a),i##_END_(b);i<=i##_END_;i++)
#define drep(i,a,b) for(int i(a),i##_END_(b);i>=i##_END_;i--)
#define File(_) freopen(#_".in","r",stdin),freopen(#_".out","w",stdout)
template<class T> inline bool tomax(T &a,T b){return a<b?a=b,1:0;}
template<class T> inline bool tomin(T &a,T b){return a>b?a=b,1:0;}
const int MN=100005;
bool cur1;
int a[MN];
bool cur2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	File(road);
	int n;
	scanf("%d",&n);
	rep(i,1,n)scanf("%d",a+i);
	int now=0,ans=0;
	rep(i,1,n){
		if(a[i]>now)
			ans+=a[i]-now;
		now=a[i];
	}
	printf("%d\n",ans);
	return 0;
}
