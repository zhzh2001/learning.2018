#include <bits/stdc++.h>
#define rep(i,a,b) for(int i(a),i##_END_(b);i<=i##_END_;i++)
#define drep(i,a,b) for(int i(a),i##_END_(b);i>=i##_END_;i--)
#define File(_) freopen(#_".in","r",stdin),freopen(#_".out","w",stdout)
template<class T> inline bool tomax(T &a,T b){return a<b?a=b,1:0;}
template<class T> inline bool tomin(T &a,T b){return a>b?a=b,1:0;}
typedef long long ll;
bool cur1;
int a[105],cnt[25005];
bool cur2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	File(money);
	int T,n;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		rep(i,1,n)scanf("%d",a+i);
		std::sort(a+1,a+1+n);
		n=std::unique(a+1,a+1+n)-a-1;
		memset(cnt,0,sizeof cnt);
		cnt[0]=1;
		rep(k,1,25000)
			rep(i,1,n)
				if(a[i]<=k){
					cnt[k]+=cnt[k-a[i]];
					if(cnt[k]>1)cnt[k]=2;
				}
		int ans=n;
		rep(i,1,n)
			ans-=(cnt[a[i]]>1);
		printf("%d\n",ans);
	}
	return 0;
}
