#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define ll long long
const int maxn=50010;
struct node{int to,len,next;}edge[maxn];
int n,m,tot=0,head[maxn];
ll maxs;
int mnum;
int sum[1010],f[1010][1010],e[maxn];
bool flag=1,flas=1;
void add_edge(int x,int y,int l)
{
	edge[++tot]=(node){y,l,head[x]};
	head[x]=tot;
}
void get_ins()
{
	scanf("%d%d",&n,&m);
	for (int i=1,x,y,l;i<n;i++){
		scanf("%d%d%d",&x,&y,&l);
		if (x-y!=1&&x-y!=-1) flag=0;
		if (x!=1&&y!=1) flas=0;
		add_edge(x,y,l);
		add_edge(y,x,l);
		e[i]=l;
	}
}
ll dfs(int x,int fa,ll dis)
{
	for (int i=head[x];i!=0;i=edge[i].next){
		int to=edge[i].to,len=edge[i].len;
		if (to!=fa) dfs(to,x,dis+len);
	}
	if (dis>maxs) {maxs=dis;mnum=x;}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	get_ins();
	
	if (m==1)
	{
	maxs=0;mnum=0;
	dfs(1,0,0);	
	maxs=0;
	dfs(mnum,0,0);
	printf("%lld",maxs);
	return 0;
	}
	
	if (flag)
	{

	sum[0]=0;
	for (int k=1;k<n;k++)
		for (int i=head[k];i!=0;i=edge[i].next)
			if (edge[i].to==k+1) sum[k]=sum[k-1]+edge[i].len;
	for (int i=1;i<n;i++) f[i][1]=sum[i];
	for (int i=2;i<n;i++)
		for (int j=2;j<=min(i,m);j++)
			for (int k=j-1;k<i;k++)
				f[i][j]=max(f[i][j],min(f[k][j-1],sum[i]-sum[k]));
	printf("%d",f[n-1][m]);
	return 0;
	}
	
	if (flas)
	{
	int ans;
	sort(e+1,e+n);
	if (m*2<=n-1){
		ans=INT_MAX;
		for (int i=n-1-2*m+1,j=n-1;i<j;i++,j--)
			ans=min(e[i]+e[j],ans);
	}
	else{
		ans=e[2*n-2*m-1];
		for (int i=1,j=2*n-2*m-2;i<j;i++,j--)
			ans=min(e[i]+e[j],ans);
	}
	printf("%d",ans);
	return 0;
	}
}
