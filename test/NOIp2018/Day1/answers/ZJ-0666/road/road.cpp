#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	long long ans=0;
	int n;
	scanf("%d",&n);
	for (long long x=0,y;n>0;n--){
		scanf("%lld",&y);
		if (y>x) ans+=y-x;
		x=y;
	}
	printf("%lld",ans);
	return 0;
}
