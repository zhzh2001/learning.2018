#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int T,n,ans,a[110],b[30000];
bool f[30000];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	memset(f,0,sizeof(f));
	scanf("%d",&T);
	while (T--)
	{
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	sort(a+1,a+1+n);
	b[0]=1;b[1]=0;
	f[0]=1;
	ans=0;
	for (int i=1;i<=n;i++){
		if (f[a[i]]) continue;
		ans++;
		for (int j=1;j<=b[0];j++)
			if (b[j]+a[i]<=a[n]&&!f[b[j]+a[i]]){
				f[b[j]+a[i]]=1;
				b[++b[0]]=b[j]+a[i];
			}
	}
	for (int i=1;i<=b[0];i++) f[b[i]]=0;
	printf("%d\n",ans);
	}
}
