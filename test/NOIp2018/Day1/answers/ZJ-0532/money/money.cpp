#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
int T,n,ans,a[110];
bool vis[25010];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		memset(vis,0,sizeof(vis));
		vis[0]=1;
		int ans=0;
		for (int i=1;i<=n;i++)
		{
			if (vis[a[i]]) continue;
			ans++;
			for (int j=a[i];j<=25000;j++)
				if (vis[j-a[i]]) vis[j]=true;
		}
		printf("%d\n",ans);
	}
	return 0;
}
