#include <cstdio>
#include <algorithm>
#include <vector>
using namespace std;
const long long INF=1e9;
struct node
{
	long long Max,Maxlen;
}dp[50010][2];
long long DP[50010][3][2];
int val[100010],cnt,Next[100010],head[100010],dis[100010],n,m,lim;
bool Use[100010];
vector<long long> w[50010];
void add(int a,int b,int c)
{
	val[++cnt]=b;
	Next[cnt]=head[a];
	head[a]=cnt;
	dis[cnt]=c;
}
void dfs(int u,int fa,int mid)
{
	long long sum=0;
	bool flag=false;
	for (int i=head[u];i;i=Next[i])
	{
		int v=val[i],c=dis[i];
		if (v==fa) continue;
		dfs(v,u,mid);
		flag=true;
		if (dp[v][1].Maxlen+c>=mid)
		{
			dp[v][1].Max++;dp[v][1].Maxlen=0;
		}
		else dp[v][1].Maxlen+=c;
		if (c>=mid) dp[v][0].Max++;
		sum+=max(dp[v][0].Max,dp[v][1].Max);
		if (dp[v][0].Max>dp[v][1].Max) w[u].push_back((c>=mid)?0:c);
		else w[u].push_back(dp[v][1].Maxlen);
	}
	if (!flag) return;
	sort(w[u].begin(),w[u].end());
	int tl=w[u].size()-1,tot=0;
	for (int i=0;i<(int)w[u].size();i++) Use[i]=false;
	for (int i=0;i<(int)w[u].size();i++)
	{
		if (i>=tl) break;
		if (w[u][i]+w[u][tl]>=mid) Use[i]=Use[tl]=true,tl--,tot++;
	}
	int V=tot;
	dp[u][0].Max=sum+tot;
	dp[u][1].Max=sum+tot;
	for (int i=(int)w[u].size()-1;i>=0;i--)
	{
		for (int j=0;j<(int)w[u].size();j++) Use[j]=false;
		Use[i]=true;
		int tl=((i==(int)w[u].size()-1)?(w[u].size()-2):(w[u].size()-1));
		tot=0;
		for (int j=0;j<(int)w[u].size();j++)
		{
			if (j>=tl) break;
			if (w[u][j]+w[u][tl]>=mid)
			{
				Use[j]=Use[tl]=true;
				while (Use[tl]) tl--;
				tot++;
			}
		}
		if (tot==V)
		{
			dp[u][1].Maxlen=w[u][i];
			break;
		}
	}
}
bool check(int mid)
{
	for (int i=1;i<=n;i++)
	{
		w[i].clear();
		for (int j=0;j<=1;j++) dp[i][j].Max=dp[i][j].Maxlen=0;
	}
	dfs(1,0,mid);
	return (max(dp[1][0].Max,dp[1][1].Max)>=m);
}
void DFS(int u,int fa)
{
	for (int i=head[u];i;i=Next[i])
	{
		int v=val[i],c=dis[i];
		if (v==fa) continue;
		DFS(v,u);
		if (DP[v][0][0]+c>DP[u][0][0])
		{
			DP[u][1][0]=DP[u][0][0];
			DP[u][1][1]=DP[u][0][1];
			DP[u][0][0]=DP[v][0][0]+c;
			DP[u][0][1]=v;
		}
		else if (DP[v][0][0]+c>DP[u][1][0])
		{
			DP[u][1][0]=DP[v][0][0]+c;
			DP[u][1][1]=v;
		}
	}
}
void DFS1(int u,int fa,int c)
{
	if (u!=1)
	{
		if (u==DP[fa][0][1]) DP[u][2][0]=max(DP[fa][1][0],DP[fa][2][0])+c;
		else DP[u][2][0]=max(DP[fa][2][0],DP[fa][0][0])+c;
	}
	for (int i=head[u];i;i=Next[i])
	{
		int v=val[i];
		if (v==fa) continue;
		DFS1(v,u,dis[i]);
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++)
	{
		int u,v,c;
		scanf("%d%d%d",&u,&v,&c);
		add(u,v,c);add(v,u,c);
		lim+=c;
	}
	if (m==1)
	{
		DFS(1,0);
		DFS1(1,0,0);
		long long ans=0;
		for (int i=1;i<=n;i++)
			ans=max(ans,DP[i][0][0]+DP[i][2][0]);
		printf("%lld\n",ans);
		return 0;
	}
	int l=0,r=lim;
	while (l<r)
	{
		int mid=(l+r+1)>>1;
		if (check(mid)) l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
