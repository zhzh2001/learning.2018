#include<bits/stdc++.h>
using namespace std;
#define fo(i,a,b) for(i=(a);i<=(b);++i)
#define fd(i,a,b) for(i=(a);i>=(b);--i)
#define out(i,v,now) for(LL i=v.be[now],to=v.v[i];~i;i=v.ne[i],to=v.v[i])
#define FP freopen
#define PF printf
#define SF scanf
#define cmax(a,b) (a=(a)<(b)?(b):(a))
#define cmin(a,b) (a=(a)<(b)?(a):(b))
typedef long long LL;
int main(){
	FP("road.in","r",stdin);
	FP("road.out","w",stdout);
	LL n,a,ans=0,last,i;
	SF("%lld",&n);
	SF("%lld",&last);
	fo(i,2,n){
		SF("%lld",&a);
		if (last>a) ans+=last-a;
		last=a;
	}
	ans+=last;
	PF("%lld",ans);
	return 0;
}
