#include<bits/stdc++.h>
using namespace std;
#define fo(i,a,b) for(i=(a);i<=(b);++i)
#define fd(i,a,b) for(i=(a);i>=(b);--i)
#define out(i,v,now) for(LL i=v.be[now],to=v.v[i];~i;i=v.ne[i],to=v.v[i])
#define FP freopen
#define PF printf
#define SF scanf
#define cmax(a,b) (a=(a)<(b)?(b):(a))
#define cmin(a,b) (a=(a)<(b)?(a):(b))
typedef long long LL;
const LL maxn=110,maxai=25100;
LL a[maxn],n;
bool f[maxai];
int main(){
	FP("money.in","r",stdin);
	FP("money.out","w",stdout);
	LL T,i,j,ma,ans;
	SF("%lld",&T);
	while (T--){
		memset(a,0,sizeof(a)); memset(f,0,sizeof(f));
		f[0]=1; ma=0;
		SF("%lld",&n);
		fo(i,1,n){
			SF("%lld",&a[i]);
			cmax(ma,a[i]);
		}
		sort(a+1,a+n+1);
		ans=n;
		fo(i,1,n){
			if (f[a[i]]) --ans;
			else fo(j,a[i],ma) f[j]|=f[j-a[i]];
		}
		PF("%lld\n",ans);
	}
	return 0;
}
