#include<bits/stdc++.h>
using namespace std;
#define fo(i,a,b) for(i=(a);i<=(b);++i)
#define fd(i,a,b) for(i=(a);i>=(b);--i)
#define out(i,v,now) for(LL i=v.be[now],to=v.v[i];~i;i=v.ne[i],to=v.v[i])
#define FP freopen
#define PF printf
#define SF scanf
#define cmax(a,b) (a=(a)<(b)?(b):(a))
#define cmin(a,b) (a=(a)<(b)?(a):(b))
#define mid ((l+r)>>1)
typedef long long LL;
typedef pair<LL,LL> Pair;
const LL maxn=50100;
LL n,f[maxn],g[maxn],len,m;
vector<LL> sta[maxn];
vector<Pair> cho[maxn];
struct adj_list{
	LL be[maxn],v[maxn<<1],ne[maxn<<1],w[maxn<<1],cnt;
	adj_list(){
		memset(be,-1,sizeof(be)); cnt=0;
	}
	void add(LL x,LL y,LL z){
		ne[cnt]=be[x];
		be[x]=cnt;
		v[cnt]=y;
		w[cnt]=z;
		++cnt;
	}
}v;
void rd(LL& num){
	num=0;
	char c=getchar();
	while (c<'0' || c>'9') c=getchar();
	while (c>='0' && c<='9'){
		num=num*10+c-'0';
		c=getchar();
	}
}
void dfs(LL now,LL fa,LL dp){
	cho[dp].clear();
	sta[dp].clear();
	out(i,v,now)
	if (to!=fa){
		dfs(to,now,dp+1);
		sta[dp].push_back(g[to]+v.w[i]);
		f[now]+=f[to];
	}
	sort(sta[dp].begin(),sta[dp].end());
	LL i=0ll,j=(LL)sta[dp].size()-1ll;
	while (~j && sta[dp][j]>=len){
		sta[dp][j]=0; --j; ++f[now];
	}
	fd(j,j,0){
		if (i>=j) break;
		while (i<j-1 && sta[dp][i]+sta[dp][j]<len){
			++i;
		}
		if (sta[dp][i]+sta[dp][j]>=len && i<j){
			cho[dp].push_back(Pair(i,j));
			++f[now]; ++i;
		}
	}
	LL csz=(LL)cho[dp].size();
	fd(i,csz-1,0)
	while (sta[dp][cho[dp][i].first]+sta[dp][cho[dp][i].second-1]>=len && cho[dp][i].second-1>cho[dp][csz-1].first && (i==0 || cho[dp][i].second-1>cho[dp][i-1].second))
	--cho[dp][i].second;
	fo(i,0,(LL)csz-1) sta[dp][cho[dp][i].first]=sta[dp][cho[dp][i].second]=0;
	fo(i,0,(LL)sta[dp].size()-1){
		cmax(g[now],sta[dp][i]);
	}
}
int main(){
	FP("track.in","r",stdin);
	FP("track.out","w",stdout);
	rd(n); rd(m);
	LL i,x,y,z,l=0,r=1;
	fo(i,2,n){
		rd(x); rd(y); rd(z);
		v.add(x,y,z); v.add(y,x,z);
		r+=z;
	}
	while (l+1<r){
		len=mid;
		memset(f,0,sizeof(f)); memset(g,0,sizeof(g));
		dfs(1,-1,1);
		if (f[1]>=m) l=mid; else r=mid;
	}
	PF("%lld",l);
	return 0;
}
