#include<bits/stdc++.h>
using namespace std;
bool mmm1;
const int M=50005;
int head[M],to[M<<1],nxt[M<<1],val[M<<1],tot;
void Add(int x,int y,int z){
	to[++tot]=y;
	val[tot]=z;
	nxt[tot]=head[x];
	head[x]=tot;
}
int n,m;
struct P20{
	int a[M],tot,b[M],top;
	void solve(){
		tot=0,top=m;
		for(int i=head[1];i;i=nxt[i]){
			int w=val[i];
			a[++tot]=w;
		}
		sort(a+1,a+tot+1);
		for(int i=1;i<=m;i++)b[i]=a[tot-i+1];
		for(int i=1;i<=m;i++){
			if(tot-m<i)break;
			b[m-i+1]+=a[tot-m-i+1];
		}
		int minn=2e9;
		for(int i=1;i<=m;i++)minn=min(minn,b[i]);
		printf("%d\n",minn);
	}
}p20;
struct Plink{
	int dp[1005][1005];
	int sum[1005];
	void dfs(int x,int f){
		sum[x]=0;
		for(int i=head[x];i;i=nxt[i]){
			int y=to[i],w=val[i];
			if(y==f)continue;
			dfs(y,x);
			sum[x]=sum[y]+w;
		}
	}
	void solve(){
		dfs(1,0);
		for(int i=1;i<=n;i++)dp[i][1]=sum[i];
		for(int i=2;i<=m;i++)
			for(int j=n;j;j--)
				for(int k=j+1;k<=n;k++){
					if(dp[k][i-1]<=dp[j][i]||sum[j]-sum[k]<=dp[j][i])continue;
					dp[j][i]=min(dp[k][i-1],sum[j]-sum[k]);
				}
		printf("%d\n",dp[1][m]);
	}
}plink;
struct P_20{
	int rt,maxn;
	void dfs(int x,int f,int d){
		if(d>maxn)maxn=d,rt=x;
		for(int i=head[x];i;i=nxt[i]){
			int y=to[i],w=val[i];
			if(y==f)continue;
			dfs(y,x,d+w);
		}
	}
	void solve(){
		maxn=-1;dfs(1,0,0);
		maxn=-1;dfs(rt,0,0);
		printf("%d\n",maxn);
	}
}p_20;
struct P{
	int rt,maxn;
	void dfs(int x,int f,int d){
		if(d>maxn)maxn=d,rt=x;
		for(int i=head[x];i;i=nxt[i]){
			int y=to[i],w=val[i];
			if(y==f)continue;
			dfs(y,x,d+w);
		}
	}
	void solve(){
		maxn=-1;dfs(1,0,0);
		maxn=-1;dfs(rt,0,0);
		printf("%d\n",maxn/m+maxn%m);
	}
}p;
bool mmm2;
int main(){//long long   memory  file  the most  freopen
//	printf("%lf  MB \n",(&mmm2-&mmm1)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool flag1=0,flag2=0;
	for(int a,b,c,i=1;i<n;i++){
		scanf("%d%d%d",&a,&b,&c);
		Add(a,b,c),Add(b,a,c);
		if(a!=1)flag1=1;
		if(b!=a+1)flag2=1;
	}
	if(m==1)p_20.solve();
	else if(!flag1)p20.solve();
	else if(!flag2&&n<=1000)plink.solve();
	else p.solve();
	return 0;
}
