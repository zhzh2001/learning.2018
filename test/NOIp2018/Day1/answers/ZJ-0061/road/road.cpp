#include <map>
#include <cstdio>
#include <vector>
#include <algorithm>
using std::map;
using std::vector;
using std::lower_bound;
using std::upper_bound;
typedef vector<int> vec;

const int kMaxN = 100000 + 5;

int n;
int d[kMaxN];
map<int, vec> pos;

inline int min(const int lhs, const int rhs) {
  return lhs < rhs ? lhs : rhs;
}

struct Seg {
  struct Node {
    int num;
    Node *l, *r;
    Node() {
      l = r = NULL;
    }
    void Maintain() {
      num = l->num;
      if (r)
        num = min(num, r->num);
    }
  } *root;
  void Insert(Node *&now, const int left, const int right, const int pos) {
    if (!now)
      now = new Node;
    if (left == right) {
      now->num = d[pos];
    } else {
      const int m = (left + right) >> 1;
      if (pos <= m)
        Insert(now->l, left, m, pos);
      else
        Insert(now->r, m + 1, right, pos);
      now->Maintain();
    }    
  }
  int Query(Node *now, const int l, const int r, const int left, const int right) {
    if (left <= l && r <= right)
      return now->num;
    int ret = 0x7fffffff;
    const int m = (l + r) >> 1;
    if (left <= m)
      ret = min(ret, Query(now->l, l, m, left, right));
    if (m < right)
      ret = min(ret, Query(now->r, m + 1, r, left, right));
    return ret;
  }
} seg;

int ans;
void DFS(const int l, const int r, const int num) {
  if (l > r)
    return;
  int x = seg.Query(seg.root, 1, n, l, r);
  ans += x - num;
  int last = l - 1;
  vec::iterator a = lower_bound(pos[x].begin(), pos[x].end(), l);
  vec::iterator b = lower_bound(pos[x].begin(), pos[x].end(), r + 1);
  for (vec::iterator it = a; it != b; it++) {
    DFS(last + 1, *it - 1, x);
    last = *it;
  }
  DFS(last + 1, r, x);
}

int main() {
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
  scanf("%d", &n);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &d[i]);
    pos[d[i]].push_back(i);
    seg.Insert(seg.root, 1, n, i);
  }
  DFS(1, n, 0);
  printf("%d\n", ans);
  fclose(stdin);
  fclose(stdout);
  return 0;
}
