#include <cstdio>
#include <cstring>
#include <algorithm>
using std::sort;

const int kMaxN = 50000 + 5;
const int kMaxM = 100000 + 5;

template<typename T>
inline T min(const T lhs, const T rhs) {
  return lhs < rhs ? lhs : rhs;
}
template<typename T>
inline T max(const T lhs, const T rhs) {
  return lhs > rhs ? lhs : rhs;
}

int n, m;
int a[kMaxN], b[kMaxN], l[kMaxN];

int G[kMaxN], dst[kMaxM], val[kMaxM], nxt[kMaxM], G_tot;
inline void AddEdge(const int u, const int v, const int w) {
  dst[++G_tot] = v; val[G_tot] = w; nxt[G_tot] = G[u]; G[u] = G_tot;
  dst[++G_tot] = u; val[G_tot] = w; nxt[G_tot] = G[v]; G[v] = G_tot;  
}

namespace M1 {
  int dis[30000 + 5];
  void DFS(const int u, const int fa) {
    for (int i = G[u]; i; i = nxt[i]) {
      if (dst[i] == fa)
        continue;
      dis[dst[i]] = dis[u] + val[i];
      DFS(dst[i], u);
    }
  }
  void Solve() {
    for (int i = 1; i < n; i++)
      AddEdge(a[i], b[i], l[i]);
    memset(dis, 0x3f, sizeof dis);
    dis[1] = 0;
    DFS(1, 0);
    int ans = 0;
    int pos = 1;
    for (int i = 1; i <= n; i++) {
      if (dis[i] > ans) {
        pos = i;
        ans = dis[i];
      }
    }
    memset(dis, 0x3f, sizeof dis);
    dis[pos] = 0;
    DFS(pos, 0);
    ans = 0;
    for (int i = 1; i <= n; i++)
      ans = max(ans, dis[i]);
    printf("%d\n", ans);
  }
}

namespace A1 {
  bool cmp(const int lhs, const int rhs) {
    return lhs > rhs;
  }
  void Solve() {
    sort(l + 1, l + n, cmp);
    int ans = 0x7fffffff;
    if ((m << 1) < n) {
      for (int i = 1, j = m << 1; i < j; i++, j--)
        ans = min(ans, l[i] + l[j]);
    } else  {
      n--;
      if (!(n & 1) && !(m & 1))
        n--;
      const int s = (m << 1) - n;
      ans = l[s];
      for (int i = s + 1, j = n; i < j; i++, j--)
        ans = min(ans, l[i] + l[j]);      
    }
    printf("%d\n", ans);
  }
}

namespace BA1 {
  int w[10 + 5], sum[10 + 5];
  int f[10 + 5][10 + 5];
  void Solve() {
    n--;
    for (int i = 1; i <= n; i++)
      w[a[i]] = l[i];
    for (int i = 1; i <= n; i++)
      sum[i] = sum[i - 1] + w[i];
    memset(f, -1, sizeof f);
    for (int i = 0; i <= n; i++)
      f[i][0] = 0x7fffffff;
    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= m; j++) {
        for (int k = 0; k < i; k++) {
          if (f[k][j - 1] == -1)
            continue;
          f[i][j] = max(f[i][j], min(f[k][j - 1], sum[i] - sum[k]));
        }
      }
    }
    int ans = 0;
    for (int i = 1; i <= n; i++)
      ans = max(ans, f[i][m]);
    printf("%d\n", ans);
  }
}

namespace Other {
  void Solve() {
    n--;
    int sum = 0;
    for (int i = 1; i <= n; i++)
      sum += l[i];
    int ans = sum / m - 1;
    printf("%d\n", ans);
  }
}

int main() {
  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
  scanf("%d%d", &n, &m);
  bool A1 = 1, BA1 = 1;
  for (int i = 1; i < n; i++) {
    scanf("%d%d%d", &a[i], &b[i], &l[i]);
    if (a[i] != 1) A1 = 0;
    if (b[i] != a[i] + 1) BA1 = 0;
  }
  if (m == 1)
    M1::Solve();
  else if (A1)
    A1::Solve();
  else if (BA1)
    BA1::Solve();
  else
    Other::Solve();
  fclose(stdin);
  fclose(stdout);
  return 0;
}
