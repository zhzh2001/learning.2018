#include <cstdio>
#include <cstring>
#include <algorithm>
using std::sort;

const int kMaxN = 100 + 5;
const int kMaxA = 25000 + 5;

int T, n, m;
int a[kMaxN];
bool check[kMaxA];

int main() {
  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
  scanf("%d", &T);
  while (T--) {
    scanf("%d", &n);
    for (int i = 1; i <= n; i++)
      scanf("%d", &a[i]);
    m = n;
    memset(check, 0, sizeof check);
    check[0] = 1;
    sort(a + 1, a + n + 1);
    for (int i = 1; i <= n; i++) {
      if (check[a[i]]) {
        m--;
        continue;
      }
      for (int j = 0; j <= a[n] - a[i]; j++) {
        if (!check[j])
          continue;
        check[j + a[i]] = 1;
      }
    }
    printf("%d\n", m);
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}
