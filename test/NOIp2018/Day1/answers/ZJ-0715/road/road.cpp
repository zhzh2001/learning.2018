#include <bits/stdc++.h>
#define LL long long 
using namespace std;

  LL lg2[200001],res1,res2,mini[200001][21],minpo[200001][21];
  int n;

  void getmin(LL l,LL r){
  	LL ran=r-l+1,le=lg2[ran];
  	res1=min(mini[l][le],mini[r-(1<<le)+1][le]);
  	if (mini[l][le]<mini[r-(1<<le)+1][le])
  	  res2=minpo[l][le];else
  	  res2=minpo[r-(1<<le)+1][le];
  }

  LL solve(LL l,LL r,LL pre){
  	getmin(l,r);
  	LL nres1=res1,nres2=res2;
  	LL ret=nres1-pre;
  	if (nres2!=l) ret+=solve(l,nres2-1,nres1);
  	if (nres2!=r) ret+=solve(nres2+1,r,nres1);
  	return(ret);
  } 

  int main(){
  	freopen("road.in","r",stdin);
  	freopen("road.out","w",stdout);
  	
  	scanf("%d",&n);
    LL pre=1;lg2[1]=0;
    for (int i=2;i<=n;i++){
      lg2[i]=lg2[i-1];
      if (i==pre*2){
      	lg2[i]++;
      	pre*=2;
	  }
	}
  	
  	for (int i=1;i<=n;i++) for (int j=0;j<=20;j++)
  	  mini[i][j]=1e9;
  	for (int i=1;i<=n;i++) scanf("%lld",&mini[i][0]),minpo[i][0]=i;
  	for (int i=1;i<=20;i++)
  	  for (int j=1;j<=n-(1<<(i-1));j++){
  	    mini[j][i]=min(mini[j][i-1],mini[j+(1<<(i-1))][i-1]);
		if (mini[j][i-1]<mini[j+(1<<(i-1))][i-1])
		  minpo[j][i]=minpo[j][i-1];else
		  minpo[j][i]=minpo[j+(1<<(i-1))][i-1];
	  }
  	  
    
    printf("%lld\n",solve(1,n,0));
  }
