#include <bits/stdc++.h>
#define LL long long 
using namespace std;

  vector <pair <int,LL> > lis[500001];
  int nxt[500001],cnt,des[500001],nd[500001],a[500001],ans,n,mark[500001],m;
  int sta[500001];
  LL len[500001],tmp[500001],rem[500001];

  void addedge(int x,int y,LL z){
  	nxt[++cnt]=nd[x];des[cnt]=y;len[cnt]=z;nd[x]=cnt;
  }
  
  void dfs(int po,int fa){
  	for (int p=nd[po];p!=-1;p=nxt[p])
  	  if (des[p]!=fa){
  	  	lis[po].push_back(make_pair(des[p],len[p]));
  	    dfs(des[p],po);	
	  }
  	a[++cnt]=po;
  }

  void solve(LL mid){
  	ans=0;
  	for (int i=1;i<=n;i++){
  	  int x=a[i],cnt=0,stacnt=0;
	  for (int j=0;j<lis[x].size();j++)
	    tmp[++cnt]=rem[lis[x][j].first]+lis[x][j].second,mark[cnt]=0;
	  sort(tmp+1,tmp+cnt+1);
	  for (int j=1;j<=cnt;j++)
	    if (tmp[j]>=mid) ans++,mark[j]=1;
	  int po=cnt;
	  for (int j=1;j<=cnt;j++) 
	    if (!mark[j]){
	      while (po>=1&&((tmp[po]+tmp[j]>=mid))){
	      	if (!mark[po]) sta[++stacnt]=po;
	      	po--;
		  }
		  if (stacnt&&sta[stacnt]==j) stacnt--;
		  if (stacnt){
		  	ans++;
		  	mark[j]=1;mark[sta[stacnt]]=1;
		  	stacnt--;
		  }
		}
	  rem[x]=0;
	  for (int j=1;j<=cnt;j++)
	    if (!mark[j])
	      rem[x]=max(rem[x],tmp[j]);
	}
  }

  int main(){
  	freopen("track.in","r",stdin);
  	freopen("track.out","w",stdout);
  	
  	scanf("%d%d",&n,&m);
  	for (int i=1;i<=n;i++) nd[i]=-1;
  	for (int i=1;i<n;i++){
  	  int t1,t2;LL t3;
  	  scanf("%d%d%lld",&t1,&t2,&t3);	
  	  addedge(t1,t2,t3);
  	  addedge(t2,t1,t3);
	}
	
	cnt=0;
	dfs(1,0);
	
	LL l=0,r=(LL)500000000;
	while (l<r){
	  LL mid=(l+r+1)>>1;
	  solve(mid);
	  if (ans>=m) l=mid;else r=mid-1;	
	}
	printf("%lld\n",l);
  }
