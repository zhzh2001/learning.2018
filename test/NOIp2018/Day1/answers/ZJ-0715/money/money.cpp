#include <bits/stdc++.h>
using namespace std;

  int T,dp[25001],n,a[233];

  int main(){
  	freopen("money.in","r",stdin);
  	freopen("money.out","w",stdout);
  	
  	scanf("%d",&T);
  	while (T--){
  	  memset(dp,0,sizeof(dp));dp[0]=1;
  	  scanf("%d",&n);
	  for (int i=1;i<=n;i++)
	    scanf("%d",&a[i]);
	  sort(a+1,a+n+1);
	  int ans=n;
	  for (int i=1;i<=n;i++){
	  	if (dp[a[i]]) ans--;
	  	for (int j=a[i];j<=25000;j++)
	  	  dp[j]|=dp[j-a[i]];
	  }
	  printf("%d\n",ans);
	}
  }
