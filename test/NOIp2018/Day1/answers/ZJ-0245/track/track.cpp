#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
int n,m;
struct edge
{
	int v,w,next;
}e[1000005];
int tot;
int head[1000005];
void add(int u,int v,int w)
{
	e[++tot].v=v;
	e[tot].w=w;
	e[tot].next=head[u];
	head[u]=tot;
}
bool vis[500005];
int we[500005];
int maxn;
void dfs(int k,int s)
{
	vis[k]=1;
	for(int i=head[k];i;i=e[i].next)
	{
		int v=e[i].v;
		if(!vis[v])
		{
			vis[v]=1;
			dfs(v,s+e[i].w);
			vis[v]=0;
		}
	}
	maxn=max(maxn,s);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d", &n,&m);
	if(m==n-1)
	{
		int a,b,c;
		int ans=20000;
		for(int i=1;i<n;i++)
		{
			scanf("%d%d%d", &a,&b,&c);
			ans=min(ans,c);
		}
		printf("%d",ans);
	}
	else if(m==1)
	{
		int u,v,w;
		int ma=0,k1,k2;
		for(int i=1;i<n;i++)
		{
			scanf("%d%d%d", &u,&v,&w);
			add(u,v,w);
			add(v,u,w);
		}
		for(int i=1;i<=n;i++)
		{
			if(e[head[i]].next==0)
			{
			dfs(i,0);
			memset(vis,0,sizeof(vis));
			}
		}
		printf("%d",maxn);
	}
	else
	{
		bool f1=0,f2=0;
		for(int i=1;i<n;i++)
		{
			int u,v,w;
			scanf("%d%d%d", &u,&v,&w);
			add(u,v,w);
			add(v,u,w);
			we[i]=2;
			if(u!=1) f1=1;
			if(v!=u+1) f2=1;
		}
		if(!f1)
		{
			int t=n-1;
			sort(we+1,we+n);
			for(int i=1;i<=n/2;i++)
			{
			if(t==m) break;
			we[i]+=we[n-i];
			t--;
			
			}
			if(n%2==0&&t+1==n/2) we[n/2]/=2;
			sort(we+1,we+n);
			printf("%d", we[n-m]);
		}
	}
}
