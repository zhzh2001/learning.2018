#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int t,n,a[105];
bool v[105];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d", &t);
	while(t--)
	{
		scanf("%d", &n);
		for(int i=0;i<n;i++) scanf("%d", &a[i]);
		sort(a,a+n);
		int p=0;
		for(int i=0;i<n-1;i++)
		for(int j=i+1;j<n;j++)
		{
			if(v[i]||v[j]) continue;
			if(a[j]%a[i]==0)
			{
				a[j]=30000;
				v[j]=1;
				p++;
			}
		}
		n-=p;
		sort(a,a+n);
		memset(v,0,sizeof(v));
		p=0;
		for(int i=0;i<n;i++)
		for(int j=i+1;j<n;j++)
		for(int k=j+1;j<n;j++)
		{
			if(v[i]||v[j]||v[k]) continue;
			if(a[k]>a[i]*a[j]-a[i]-a[j])
			{
				a[k]=30000;
				v[k]=1;
				p++;
			}
			else if(a[k]<a[i]+a[j])
				continue;
			else
			{
				for(int a2=1;;a2++)
				{
				for(int a1=1;;a1++)
				{
					if(a1*a[i]+a2*a[j]>a[k]) break;
					if(a1*a[i]<a2*a[j]==a[k])
					{
						a[k]=30000;
						v[k]=1;
						p++;
					}
				}
				if(a2*a[j]>a[k]) break;
				}
			}
		}
		printf("%d\n", n-p);
		memset(v,0,sizeof(v));
		memset(a,0,sizeof(a));
	}
	return 0;
}
