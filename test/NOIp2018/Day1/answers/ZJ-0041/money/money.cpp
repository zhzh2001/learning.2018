#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
int n,a[N],mx,f[N],ans;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(int i,k,T=read();T--;){
		n=read();mx=0;
		for(i=1;i<=n;i++){
			a[i]=read();
			if(a[i]>mx)mx=a[i];
		}
		f[0]=1;
		sort(a+1,a+n+1);
		ans=0;
		for(i=1;i<=n;i++){
			if(f[a[i]])continue;
			ans++;
			for(k=a[i];k<=mx;k++)
			f[k]|=f[k-a[i]];
		}
		for(i=0;i<=mx;i++)f[i]=0;
		printf("%d\n",ans);
	}
	return 0;
}
