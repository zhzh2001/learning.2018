#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <string>
#include <map>
#include <set>
#include <vector>
#define M 500010

using namespace std;

int Head[M], Next[M], Go[M], Val[M], F[M], G[M], Cnt = 0, n, k;

void addedge(int x, int y, int z) {
	Go[++Cnt] = y;
	Next[Cnt] = Head[x];
	Head[x] = Cnt;
	Val[Cnt] = z;
}

int VV[M];

bool BB[M];

int calc(int lf, int ri, int v) {
	int l = lf, r = ri, ans = 0;
	for(int i = r; i > l; i--) {
		if(BB[i]) continue;
		while(l < i && (VV[l] + VV[i] < v || BB[l])) l++;
		if(l < i && VV[l] + VV[i] >= v) {
			ans++;
			l++;
		}
	}
	return ans;
}

void solve(int lf, int ri, int v, int &ans1, int &ans2) {
	int ans = -1;
	ans1 = calc(lf, ri, v);
	for(int l = lf, r = ri; l <= r; ) {
		int md = (l + r) / 2;
		for(int i = lf; i <= ri; i++) BB[i] = 0;
		BB[md] = 1;
		int tmp = calc(lf, ri, v);
		if(tmp == ans1) {
			ans = md;
			l = md + 1;
		} else r = md - 1;
		BB[md] = 0;
	}
	if(ans == -1) ans2 = 0; else ans2 = VV[ans];
}

void DFS(int x, int f, int v) {
	for(int T = Head[x]; T; T = Next[T]) if(Go[T] != f) DFS(Go[T], x, v);
	//VV.clear();
	int ans = 0;
	int vcnt = 0;
	for(int T = Head[x]; T; T = Next[T]) if(Go[T] != f) {
		ans += F[Go[T]];
		//VV.push_back(Val[T] + G[Go[T]]);
		VV[vcnt++] = Val[T] + G[Go[T]];
	}
	//sort(VV.begin(), VV.end());
	sort(VV, VV + vcnt);
	int t = vcnt;
	VV[t] = 0;
	for(int i = 0; i < t; i++) BB[i] = 0;
	int l = 0, r = t - 1;
	while(r >= 0 && VV[r] >= v) {
		ans++;
		BB[r] = 1;
		r--;
	}
	int ans1 = 0, ans2 = 0;
	if(r >= 0) solve(0, r, v, ans1, ans2);
	F[x] = ans + ans1;
	G[x] = ans2;
}

bool check(int v) {
	memset(F, 0, sizeof F);
	memset(G, 0, sizeof G);
	DFS(1, 0, v);
	return F[1] >= k;
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &k);
	for(int i = 1; i < n; i++) {
		int a, b, c;
		scanf("%d%d%d", &a, &b, &c);
		addedge(a, b, c);
		addedge(b, a, c);
	}
	int ans = 0;
	for(int l = 0, r = 500000000; l <= r; ) {
		int md = (l + r) / 2;
		if(check(md)) {
			ans = md;
			l = md + 1;
		} else {
			r = md - 1;
		}
	}
	printf("%d\n", ans);
	return 0;
}
