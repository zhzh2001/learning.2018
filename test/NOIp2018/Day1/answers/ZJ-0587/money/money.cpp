#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <string>
#include <map>
#include <set>
#include <vector>

using namespace std;

typedef long long ll;

int A[222], n, F[55555];

int Solve() {
	memset(A, 0, sizeof A);
	memset(F, 0, sizeof F);
	scanf("%d", &n);
	for(int i = 1; i <= n; i++) scanf("%d", &A[i]);
	sort(A + 1, A + n + 1);
	n = unique(A + 1, A + n + 1) - A - 1;
	A[n + 1] = 0;
	int ans = 0;
	F[0] = 1;
	for(int i = 1; i <= n; i++) if(!F[A[i]]) {
		ans++;
		for(int j = 0; A[i] + j <= 25000; j++) if(F[j]) F[j + A[i]] = 1;
	}
	return ans;
}

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while(T--) printf("%d\n", Solve());
	return 0;
}
