#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <string>
#include <map>
#include <set>
#include <vector>

using namespace std;

int n, A[100010];

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for(int i = 1; i <= n; i++) scanf("%d", &A[i]);
	int ans = 0;
	A[0] = 0;
	A[n + 1] = 0;
	for(int i = 1; i <= n; i++)
		if(A[i] > A[i - 1]) ans += A[i] - A[i - 1];
	printf("%d\n", ans);
	return 0;
}
