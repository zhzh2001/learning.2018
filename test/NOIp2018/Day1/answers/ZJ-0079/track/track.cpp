#include<bits/stdc++.h>
#define N 50000
#define add(x,y,v) (e[++ee].nxt=lnk[x],e[lnk[x]=ee].to=y,e[ee].val=v)
using namespace std;
inline void Gmax(int &x,int y) {x<y&&(x=y);}
inline void Gmin(int &x,int y) {x>y&&(x=y);}
inline bool cmp(int x,int y) {return x>y;}
int n,m,sum,ee,lnk[N+5];
struct edge
{
	int to,nxt,val,used;
}e[(N<<1)+5];
class Class_BruteForceSolver
{
	private:
		inline bool dfs(int x,int val,int cnt,int tot)
		{
			register int i;
			if(tot>=val)
			{
				if(++cnt==m) return true;
				for(i=1;i<=n;++i) if(dfs(i,val,cnt,0)) return true;
				return false;
			}
			for(i=lnk[x];i;i=e[i].nxt)
			{
				if(e[i].used) continue;
				e[i].used=e[((i-1)^1)+1].used=1;
				if(dfs(e[i].to,val,cnt,tot+e[i].val)) return e[i].used=e[((i-1)^1)+1].used=0,true;
				e[i].used=e[((i-1)^1)+1].used=0;
			}
			return false;
		}
		inline bool check(int val)
		{
			for(register int i=1;i<=n;++i) if(dfs(i,val,0,0)) return true;
			return false;
		}
	public:
		inline void Solve()
		{
			register int l=0,r=sum/m,mid;
			while(l<r) check(mid=(l+r+1)>>1)?l=mid:r=mid-1;
			printf("%d",l);
		}
}BF;
class Class_TreeDiameterSolver
{
	private:
		int ans,Max[N+5],Max_[N+5],MaxSon[N+5];
		inline void dfs1(int x,int lst)
		{
			register int i;
			for(Max[x]=Max_[x]=MaxSon[x]=0,i=lnk[x];i;i=e[i].nxt)
			{
				if(!(e[i].to^lst)) continue;
				dfs1(e[i].to,x);
				if(Max[e[i].to]+e[i].val>Max[x]) Max_[x]=Max[x],Max[x]=Max[MaxSon[x]=e[i].to]+e[i].val;
				else if(Max[e[i].to]+e[i].val>Max_[x]) Max_[x]=Max[e[i].to]+e[i].val;
			}
			Gmax(ans,Max[x]+Max_[x]);
		}
		inline void dfs2(int x,int lst,int val)
		{
			register int i;
			for(i=lnk[x];i;i=e[i].nxt)
			{
				if(!(e[i].to^lst)) continue;
				dfs2(e[i].to,x,max(val,e[i].to^MaxSon[x]?Max[x]:Max_[x])+e[i].val);
			}
			Gmax(ans,Max[x]+val);
		}
	public:
		inline void Solve() {dfs1(1,0),dfs2(1,0,0),printf("%d",ans);}
}TD;
class Class_JuFlowerSolver
{
	private:
		int cur,data[N+5];
	public:
		inline void Solve()
		{
			register int i,t,ans;
			for(i=1;i<=ee;i+=2) data[++cur]=e[i].val;
			sort(data+1,data+cur+1,cmp);
			if((m<<1)<=cur) for(ans=1e9,i=1;i<=m;++i) Gmin(ans,data[i]+data[(m<<1)-i+1]);
			else for(i=(t=(m<<1)-cur)+1,ans=data[(m<<1)-cur];i<=cur;++i) Gmin(ans,data[i]+data[cur-(i-t)+1]);
			printf("%d",ans);
		}
}JF;
class Class_LineSolver
{
	private:
		int cur,data[N+5];
		inline bool check(int val)
		{
			register int i,cnt=0,tot=0;
			for(i=1;i<=cur;++i) 
			{
				if((tot+=data[i])>=val)
				{
					if(++cnt==m) return true;
					tot=0;
				}
			}
			return false;
		}
	public:
		inline void Solve()
		{
			register int i,l=0,r=sum/m,mid;
			for(i=1;i<=ee;i+=2) data[++cur]=e[i].val;
			while(l<r) check(mid=(l+r+1)>>1)?l=mid:r=mid-1;
			printf("%d",l);
		}
}LS;
int main()
{
	freopen("track.in","r",stdin),freopen("track.out","w",stdout);
	register int i,x,y,v,flag1=1,flag2=1;
	for(scanf("%d%d",&n,&m),i=1;i<n;++i) scanf("%d%d%d",&x,&y,&v),add(x,y,v),add(y,x,v),flag1&=(x==1),flag2&=(y==x+1),sum+=v;
	if(m==1) TD.Solve();
	else if(flag1) JF.Solve();
	else if(flag2) LS.Solve();
	else BF.Solve();
	return 0;
}
