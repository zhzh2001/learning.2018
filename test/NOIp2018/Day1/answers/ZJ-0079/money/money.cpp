#include<bits/stdc++.h>
#define N 100
#define P 25000
using namespace std;
inline void Gmax(int &x,int y) {x<y&&(x=y);}
int n,a[N+5],vis[P+5],lst[P+5];
int main()
{
	freopen("money.in","r",stdin),freopen("money.out","w",stdout);
	register int T,i,j,k,ans,Max;
	for(scanf("%d",&T);T;--T)
	{
		for(scanf("%d",&n),ans=n,Max=0,i=1;i<=n;++i) scanf("%d",&a[i]),Gmax(Max,a[i]);
		for(vis[0]=T,sort(a+1,a+n+1),i=1;i<=n;++i)
		{
			if(vis[a[i]]^T)
			{
				for(j=Max;~j;--j) 
				{
					if(vis[j]^T) continue;
					for(k=1;1LL*a[i]*k+j<=Max;++k) 
					{
						if(vis[a[i]*k+j]^T||lst[a[i]*k+j]^a[i]) vis[a[i]*k+j]=T,lst[a[i]*k+j]=a[i];
						else break;
					}
				}
			}
			else --ans;
		}
		printf("%d\n",ans);
	}
}
