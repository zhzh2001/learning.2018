#include<bits/stdc++.h>
using namespace std;
long long ans;
int cnt,que[2000000],a[200000],n;
void solve (int l,int r,int v,int cg)
{
	if (l==r) 
	{
		ans+=a[l]-v;
		return;
	}
	long long minn=10000000000;
	for(int i=l;i<=r;i++)
	{
		if (a[i]<minn) 
		{
			cnt=cg;
			que[cnt]=i;
			minn=a[i];
		}
		else if (a[i]==minn)
		{
			cnt++;
			que[cnt]=i;
		}
	}
	int gg=cnt;
	for(int i=cg;i<=gg;i++)
	{
		if (i==cg&&que[i]-1>=l) solve(l,que[i]-1,minn,cnt+1);
	    if (i==gg&&que[i]+1<=r) solve(que[i]+1,r,minn,cnt+1);
		if (i!=cg) solve(que[i-1]+1,que[i]-1,minn,cnt+1);
	}
	ans+=minn-v;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	cin>>n;
	for(int i=1;i<=n;i++)
	scanf("%d",&a[i]);
	solve(1,n,0,1);
	cout<<ans;
	fclose(stdin);
	fclose(stdout);
}
