#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;

inline void Rd(int &res) {
	char c; res = 0;
	while (c = getchar(), c < '0');
	do {
		res = (res << 1) + (res << 3) + (c ^ 48);
	} while (c = getchar(), c >= '0');
}

const int M = 50005;

int Head[M], tot, n, m, deg[M];
struct Node {int to, w, nxt;} Edge[M << 1];

inline void Addedge(int a, int b, int c) {
	Edge[tot] = (Node) {b, c, Head[a]}; Head[a] = tot++;
	Edge[tot] = (Node) {a, c, Head[b]}; Head[b] = tot++;
	deg[a]++, deg[b]++;
}

namespace Subtask1 {
	
	int mx, id;
	
	void dfs(int x, int f, int dis) {
		if (dis > mx) mx = dis, id = x;
		for (int i = Head[x]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == f) continue;
			dfs(to, x, dis + Edge[i].w);
		}
	}
	
	void solve() {
		mx = -1;
		dfs(1, 0, 0);
		mx = -1;
		dfs(id, 0, 0);
		printf("%d\n", mx);
	}
	
}

namespace Subtask2 {
	
	int val[M], sz;
	
	void dfs(int x, int f) {
		for (int i = Head[x]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == f) continue;
			val[++sz] = Edge[i].w;
			dfs(to, x);
		}
	}
	
	bool check(int limit) {
		int pre = 0, t = 0;
		for (int i = 1; i <= sz; i++) {
			pre += val[i];
			if (pre >= limit) pre = 0, t++;
		}
		return t >= m;
	}
	
	void solve(int mx) {
		dfs(1, 0);
		int l = 0, r = mx, ans;
		while (l <= r) {
			int mid = (l + r) >> 1;
			if (check(mid)) {
				l = mid + 1;
				ans = mid;
			} else r = mid - 1;
		}
		printf("%d\n", ans);
	}
	
}

namespace Subtask3 {
	
	int A[M], sz;
	
	bool check(int limit) {
		int t = 0;
		int l = 1, r = sz;
		while (r >= 1 && A[r] >= limit) r--, t++;
		while (l < r) {
			if (A[l] + A[r] < limit) l++;
			else l++, r--, t++;
		}
		return t >= m;
	}
	
	void solve(int mx) {
		for (int i = Head[1]; ~i; i = Edge[i].nxt) A[++sz] = Edge[i].w;
		sort(A + 1, A + sz + 1);
		int l = 0, r = mx, ans;
		while (l <= r) {
			int mid = (l + r) >> 1;
			if (check(mid)) {
				l = mid + 1;
				ans = mid;
			} else r = mid - 1;
		}
		printf("%d\n", ans);
	}
	
}

namespace Subtask4 {
	
	int rt, limit, faedge[M], /*L[M], R[M], id[M], dfscnt,*/ dp[2][M], fa[M], dis[M];
	
	void predfs(int x, int f) {
		fa[x] = f;
//		L[x] = ++dfscnt; id[dfscnt] = x;
		for (int i = Head[x]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == f) continue;
			faedge[to] = Edge[i].w;
			dis[to] = dis[x] + Edge[i].w;
			predfs(to, x);
		}
//		R[x] = dfscnt;
	}
	
	void trans1(int x, int now, int dps) {
		int sum = 0;
		for (int i = Head[now]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == fa[now]) continue;
			sum += dp[1][to];
		}
//		if (x == 2 && now == 3) {
//			puts("HERE!");
//			printf("%d\n", dis[now] - dis[x] + faedge[x]);
//			printf("dps = %d\n", dps);
//		}
		if (dis[now] - dis[x] + faedge[x] >= limit) dp[1][x] = max(dp[1][x], max(dp[0][now], sum) + dps + 1);
		for (int i = Head[now]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == fa[now]) continue;
			trans1(x, to, dps + sum - dp[1][to]);
		}
	}
	
	void ttrans2(int x, int now, int already, int dps) {
		int sum = 0;
		for (int i = Head[now]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == fa[now]) continue;
			sum += dp[1][to];
		}
		if (already + dis[now] - dis[x] >= limit) dp[0][x] = max(dp[0][x], dps + max(dp[0][now], sum) + 1);
		for (int i = Head[now]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == fa[now]) continue;
			ttrans2(x, to, already, dps + sum - dp[1][to]);
		}
	}
	
	void trans2(int x, int now, int rson, int dps) {
		int sum = 0;
		for (int i = Head[now]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == fa[now]) continue;
			sum += dp[1][to];
		}
		ttrans2(x, rson, dis[now] - dis[x], dps + max(dp[0][now], sum));
		for (int i = Head[now]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == fa[now]) continue;
			trans2(x, to, rson, dps + sum - dp[1][to]);
		}
	}
	
	void dfs(int x) {
		for (int i = Head[x]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == fa[x]) continue;
			dfs(to);
		}
		if (deg[x] == 1) {
			dp[0][x] = 0;
			dp[1][x] = (faedge[x] >= limit);
		}
		if (deg[x] == 2) {
			int to;
			for (int i = Head[x]; ~i; i = Edge[i].nxt) if (Edge[i].to != fa[x]) to = Edge[i].to;
			dp[0][x] = dp[1][to];
			dp[1][x] = (faedge[x] >= limit) + dp[1][to];
			trans1(x, to, 0);
		}
		if (deg[x] == 3) {
			int lson = -1, rson;
			for (int i = Head[x]; ~i; i = Edge[i].nxt) {
				int to = Edge[i].to;
				if (to == fa[x]) continue;
				if (lson == -1) lson = to;
				else rson = to;
			}
			dp[0][x] = dp[1][lson] + dp[1][rson];
			trans2(x, lson, rson, 0);
//			if (x == 2) printf("lson = %d rson = %d\n", lson, rson);
			dp[1][x] = (faedge[x] >= limit) + dp[1][lson] + dp[1][rson];
			trans1(x, lson, dp[1][rson]), trans1(x, rson, dp[1][lson]);
		}
	}
	
	bool check(int x) {
		limit = x;
		int pos = Edge[Head[rt]].to;
		for (int i = 1; i <= n; i++) dp[0][i] = dp[1][i] = 0;
		dfs(pos);
//		printf("limit = %d rt = %d\n", limit, rt);
//		for (int i = 1; i <= n; i++) printf("i = %d dp0 = %d dp1 = %d\n", i, dp[0][i], dp[1][i]);
		return dp[1][pos] >= m;
	}
	
	void solve(int mx) {
		for (int i = 1; i <= n; i++) if (deg[i] == 1) rt = i;
		predfs(rt, 0);
		int l = 0, r = mx, ans;
		while (l <= r) {
			int mid = (l + r) >> 1;
			if (check(mid)) {
				l = mid + 1;
				ans = mid;
			} else r = mid - 1;
		}
		printf("%d\n", ans);
	}
	
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	Rd(n), Rd(m);
	memset(Head, -1, sizeof(Head));
	int total = 0;
	bool check_line = true, check_flower = true;
	for (int i = 1; i < n; i++) {
		int a, b, c;
		Rd(a), Rd(b), Rd(c);
		check_line &= (b == (a + 1));
		check_flower &= (a == 1);
		Addedge(a, b, c);
		total += c;
	}
//	int mxdeg = 0;
//	for (int i = 1; i <= n; i++) mxdeg = max(mxdeg, deg[i]);
	if (m == 1) Subtask1::solve();
	else if (check_line) Subtask2::solve(total);
	else if (check_flower) Subtask3::solve(total);
	else Subtask4::solve(total);
	return 0;
}

/*
10 4
1 2 7
4 5 5
3 4 6
2 3 5
6 7 7
7 8 2
8 9 7
5 6 6
9 10 4

10 3
1 2 3
1 5 2
1 8 1
1 7 8
1 9 6
1 10 4
1 4 2
1 3 3
1 6 8

7 2
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7
*/
