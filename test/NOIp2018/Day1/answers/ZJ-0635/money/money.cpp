#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long

using namespace std;

inline void Rd(int &res) {
	char c; res = 0;
	while (c = getchar(), c < '0');
	do {
		res = (res << 1) + (res << 3) + (c ^ 48);
	} while (c = getchar(), c >= '0');
}

const int N = 105;

int n, A[N];
//
//namespace Subtask1 {
//	
//	const int M = 1e6 + 5; // 100W
//	
//	int mx;
//	bool f[M], g[M];
//	
//	void solve() {
//		mx = A[n - 1] * A[n];
//		for (int i = 0; i <= mx; i++) f[i] = g[i] = false;
//		f[0] = g[0] = true;
//		for (int i = 1; i <= n; i++)
//			for (int j = 0; j + A[i] <= mx; j++)
//				if (f[j]) f[j + A[i]] = true;
//		int ans = 0;
//		for (int i = 1; i <= mx; i++) {
//			if (f[i] && !g[i]) {
//				for (int j = 0; j + i <= mx; j++)
//					if (g[j]) g[i + j] = true;
//				ans++;
//			}
//		}
//		printf("%d\n", ans);
//	}
//	
//}

namespace Subtask2 {
	
	const int M = 25005;
	
	int mx;
	bool f[M];
	
	void solve() {
		mx = A[n];
		for (int i = 0; i <= mx; i++) f[i] = false;
		int ans = 0;
		f[0] = true;
		for (int i = 1; i <= n; i++) {
			if (!f[A[i]]) {
				ans++;
				for (int j = 0; j + A[i] <= mx; j++)
					if (f[j]) f[j + A[i]] = true;
			}
		}
		printf("%d\n", ans);
	}
	
}

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T; Rd(T);
	while (T--) {
		Rd(n);
		for (int i = 1; i <= n; i++) Rd(A[i]);
		sort(A + 1, A + n + 1);
//		/*if (n <= 5) */Subtask1::solve();
		Subtask2::solve();
	}
	return 0;
}
