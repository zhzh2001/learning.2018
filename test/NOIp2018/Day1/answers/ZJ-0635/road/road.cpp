#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long

using namespace std;

inline void Rd(int &res) {
	char c; res = 0;
	while (c = getchar(), c < '0');
	do {
		res = (res << 1) + (res << 3) + (c ^ 48);
	} while (c = getchar(), c >= '0');
}

const int M = (int) 1e5 + 5;

ll ans;
int n, x;

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	Rd(n);
	int lst = 0;
	for (int i = 1; i <= n; i++) {
		Rd(x);
		if (x > lst) ans += (x - lst);
		lst = x;
	}
	cout << ans << endl;
	return 0;
}
