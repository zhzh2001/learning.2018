#include <bits/stdc++.h>
#define del(x) (R[L[x]] = R[x], L[R[x]] = L[x], x = R[x])

const int N = 51000, N2 = N * 2;

struct edge {
	int u, v, w;
	edge (int u0 = 0, int v0 = 0, int w0 = 0) : u(u0), v(v0), w(w0) {}
} e[N2];

int n, m, E;
int first[N], next[N2], p[N];
int cnt = 0, o[N], id[N];
int f[N], val[N], L[N], R[N];

inline void up(int &x, const int y) {x < y ? x = y : 0;}
inline void down(int &x, const int y) {x > y ? x = y : 0;}

inline void addedge(int u, int v, int w) {
	e[++E] = edge(u, v, w); next[E] = first[u]; first[u] = E;
	e[++E] = edge(v, u, w); next[E] = first[v]; first[v] = E;
}

void dfs(int x) {
	int i, y;
	o[++cnt] = x; id[x] = cnt;
	for (i = first[x]; i; i = next[i])
		if ((y = e[i].v) != p[x])
			p[y] = x, dfs(y);
}

void fy() {
	int i, j;
	for (i = 1; i <= n; ++i) {
		if (e[first[i]].v == p[i]) {first[i] = next[first[i]]; continue;}
		for (j = first[i]; next[j]; j = next[j])
			if (e[next[j]].v == p[i]) {next[j] = next[next[j]]; break;}
	}
}

bool check(int M) {
	int _, i, j, x, w, tot, ret = 0; *val = 0;
	for (_ = n; _; --_) {
		x = o[_]; tot = 0;
		for (i = first[x]; i; i = next[i])
			(w = f[e[i].v] + e[i].w) >= M ? ++ret : val[++tot] = w;
		if (!tot) {f[x] = 0; continue;}
		std::sort(val + 1, val + (tot + 1));
		for (i = 0; i <= tot; ++i) R[i] = i + 1, L[i + 1] = i;
		for (i = 1, j = tot; i <= tot; ) {
			if (i == j) j = R[j];
			for (down(j, L[tot + 1]); i < j && val[i] + val[j] >= M; j = L[j]); j = R[j];
			j > tot ? i = R[i] : (del(j), del(i), ++ret);
		}
		f[x] = val[L[tot + 1]];
	}
#ifdef FY
	fprintf(stderr, "check(%d) => (%d >= %d), time = %ld\n", M, ret, m, clock());
#endif
	return ret >= m;
}

int main() {
	int i, u, v, w, L = 0, R = 0, M;
#ifndef FY
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
#endif
	scanf("%d%d", &n, &m);
	for (i = 1; i < n; ++i) scanf("%d%d%d", &u, &v, &w), addedge(u, v, w), R += w;
	dfs(1); fy();
#ifdef FY
	fprintf(stderr, "middle time = %ld, L = %d, R = %d\n", clock(), L, R);
#endif
	for (; L < R; )
		check(M = (L + R + 1) / 2) ? L = M : (R = M - 1);
	printf("%d\n", L);
	return 0;
}
