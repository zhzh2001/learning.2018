#include <bits/stdc++.h>
#define N 25010

typedef std::bitset <N> bit;

int n;
int a[N];
bit K;

inline void up(int &x, const int y) {x < y ? x = y : 0;}

void work() {
	int i, j, ans = 0, max = 0;
	scanf("%d", &n);
	for (i = 0; i < n; ++i) scanf("%d", a + i), up(max, a[i]);
	std::sort(a, a + n); K.reset(); K.set(0);
	for (i = 0; i < n; ++i)
		if (!K.test(a[i]))
			for (++ans, j = 0; (a[i] << j) <= max; ++j)
				K |= K << (a[i] << j);
	printf("%d\n", ans);
}

int main() {
	int T;
#ifndef FY
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
#endif
	for (scanf("%d", &T); T; --T) work();
	return 0;
}
