#include <bits/stdc++.h>
#define N 100010

typedef long long ll;

int n;
int d[N];

int main() {
	int i; ll ans = 0;
#ifndef FY
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
#endif
	scanf("%d", &n); *d = 0;
	for (i = 1; i <= n; ++i) scanf("%d", d + i);
	for (i = n; i; --i) d[i] -= d[i - 1];
	for (i = 1; i <= n; ++i) ans += std::max(d[i], 0);
	printf("%lld\n", ans);
	return 0;
}
