#include<bits/stdc++.h>
#define N 50005
using namespace std;
int n, m;
struct edge {
	int l, r, s;
}a[N];
vector<int> son[N][3];
int size[N], sum[N][17];
bool cmp(edge x, edge y) {return x.s < y.s;}
bool cmp2(edge x, edge y) {return x.l < y.l;}
void find(int k) {
	size[k] = 0;
	for (int i = 0; i < son[k][0].size(); i ++) {
		int s = son[k][0][i], l = son[k][1][i];
		find(s);
		size[k] = max(size[k], size[s] + l);
	}
}
void build(int r) {
	int tmp = 1;
	for (int i = 1; i < r; i ++) {
		sum[i][0] = a[i].s;
	}
	for (int j = 1; j <= 16; j <<= 1) {
		for (int i = 1; i < r; i ++) {
			if (i + tmp < r) sum[i][j] = sum[i][j - 1] + sum[i + tmp][j - 1];
		}
		tmp *= 2;
	}
}
int calc(int l, int r) {
	int ret = 0;
	int tmp = l, tt = 1, rr = 0;
	while (r - tmp > 0) {
		while (tmp + tt - 1 <= r) tt *= 2, rr ++;
		tt /= 2; rr --;
		tmp += tt; ret += sum[l][rr];
	}
	return ret;
}
int mem[N];
int sea(int k, int tt) {
	return 0;
	int ret = 0;
	for (int i = k + 1; i < n; i ++) {
		return sea(i, tt - 1);
	}
}
int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d %d", &n, &m);
	bool bo = 1, boo = 1;
	for (int i = 1; i < n; i ++) {
		scanf("%d %d %d", &a[i].l, &a[i].r, &a[i].s);
		if (a[i].l > a[i].r) swap(a[i].l, a[i].r);
		if (a[i].l != 1 && a[i].l != 1) bo = 0;
		if (a[i].r - a[i].l != 1 && a[i].l - a[i].r != 1) boo = 0;
	}
	if (bo) {//菊花图
		sort(a + 1, a + n, cmp);
		int ans = 2147483647;
		if (m <= (n - 1) / 2)
			for (int i = 1; i <= m; i ++) {
				ans = min(ans, a[i + n - 2 * m - 1].s + a[n - i].s);
			}
		else {
			int x = n - m - 1;
			for (int i = 1; i <= x; i ++) {
				ans = min(ans, a[i].s + a[2 * x - i + 1].s);
			}
			for (int i = 2 * x + 1; i < n; i ++) {
				ans = min(ans, a[i].s);
			}
		}
		cout << ans << endl;
		return 0;
	}
	if (m == 1) {
		for (int i = 1; i < n; i ++) {
			son[a[i].l][0].push_back(a[i].r);
			son[a[i].l][1].push_back(a[i].s);
		}
		find(1);
		int max1 = 0, max2 = 0;
		for (int i = 1; i <= n; i ++) {
			for (int j = 0; j < son[i][0].size(); j ++) {
				int s = son[i][0][j], l = son[i][1][j];
				if (max1 < size[s] + l) {
					max2 = max1;
					max1 = size[s] + l;
				} else {
					if (max2 < size[s] + l) max2 = size[s] + l;
				}
			}
		}
		if (boo) cout << max1 << endl;
		else cout << max1 + max2 << endl;
		return 0;
	}
	if (boo) {//链:树状数组
		sort(a + 1, a + n, cmp2);
		build(n);
		cout << sea(1, m);
		return 0;
	}
	return 0;
}
/*
5 1
1 2 10
2 3 5
3 4 7
4 5 9
*/