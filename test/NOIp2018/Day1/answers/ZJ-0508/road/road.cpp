#include<bits/stdc++.h>
#define N 100005
#define inf 10004
using namespace std;
int n, a[N], minm[N];
int find(int l, int r, int k) {
	//cout << l << " " << r << " " << k << endl;
	int ret = 0;
	if (l == r) return a[l] - k;
	minm[l - 1] = inf;
	for (int i = l; i <= r; i ++) {
		if (a[i] < minm[i - 1]) minm[i] = a[i];
		else minm[i] = minm[i - 1];
	}
	for (int i = l; i <= r; i ++) {
		if (a[i] == minm[r]) {
			ret += a[i] - k;
			ret += find(l, i - 1, a[i]) + find(i + 1, r, a[i]);
			return ret;
		}
	}

	return ret;
}
int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i ++) scanf("%d", &a[i]);
	cout << find(1, n, 0);
	return 0;
}
