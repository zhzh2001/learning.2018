//找到最小的n，使n个整数能表示所有b个数
//pi{a[i]} * n * n * n * t
#include<bits/stdc++.h>
#define N 102
#define K 25005
using namespace std;
int T, n, a[N], tmp;
int b[N];
bool can(int t, int k) {
	int tp = t;
	//cout << t << " " << k << " " << tmp << endl;
	if (t == 0) return 1;
	for (int i = k; i <= tmp; i ++) {
		tp = t;
		while (tp >= b[i]) {
			tp -= b[i];
			if (can(tp, i + 1)) return 1;
		}
	}
	return 0;
}
int find(int k) {
	//cout << k << endl;
	int ret = 0;
	for (int i = k + 1; i <= n; i ++) {
		if (!can(a[i], 1)) {
			b[++ tmp] = a[i];
			// cout << a[i] << endl;
			if (i == n) return 1;
			else return find(i) + 1;
		}
	}
	return 0;
}
int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	while (T --) {
		memset(b, 0, sizeof(b));
		scanf("%d", &n);
		for (int i = 1; i <= n; i ++) scanf("%d", &a[i]);
		sort(a + 1, a + 1 + n);
		tmp = 0;
		b[++ tmp] = a[1];
		cout << find(1) + 1 << endl;
	}
	return 0;
}