#include<bits/stdc++.h>
#define ll long long

using namespace std;

ll read()
{
	ll x=0;
	char c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
	return x;
}

int n;
ll d[100010],ans;

void sol(int l,int r,ll bot)
{
	ll mn=10001;
	for(int i=l; i<=r; i++)mn=min(mn,d[i]);
	ans+=mn-bot;
	int s=l;
	for(int i=l; i<=r; i++)
	{
		if(d[i]==mn)
		{
			if(i==s)
			{
				s++;
				continue;
			}
			sol(s,i-1,mn);
			s=i+1;
		}
	}
	if(d[r]>mn)sol(s,r,mn);
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1; i<=n; i++)d[i]=read();
	sol(1,n,0);
	printf("%lld\n",ans);
	return 0;
}
