#include<bits/stdc++.h>
#define ll long long

using namespace std;

int read()
{
	int x=0;
	char c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
	return x;
}

int n,m,fir[50010],nxt[100010],to[100010],val[100010],cnt,id[100010];
int A[50010],B[50010],o[50010],S;
ll sum,s[50010],dis[50010],res;

void add(int u,int v,int w,int x)
{
	to[++cnt]=v;
	nxt[cnt]=fir[u];
	val[cnt]=w;
	id[cnt]=x;
	fir[u]=cnt;
}

void sol1()
{
	sort(A+1,A+n);
	if(m*2>=n)
	{
		int k=m-(n-1)/2;
		for(int i=1; i<=k; i++)B[i]=A[n-i];
		for(int i=k+1; i<=m; i++)B[i]=A[n-i]+A[i-k];
	}
	else
	{
		int k=n-1-m*2;
		for(int i=1; i<=m; i++)B[i]=A[n-i]+A[k+i];
	}
	sort(B+1,B+m+1);
	printf("%d",B[1]);
}

bool chk(ll x)
{
	int lt=0,C=0;
	for(int i=1; i<n; i++)
	{
		if(s[i]-s[lt]>=x)
		{
			lt=i;
			C++;
		}
	}
	return C>=m;
}

void sol2()
{
	for(int i=1; i<n; i++)s[i]=s[i-1]+A[o[i]];
	ll k=sum/m;
	ll l=0,r=k,ans=0;
	while(l<=r)
	{
		ll mid=(l+r)/2;
		if(chk(mid))l=mid+1,ans=mid;
		else r=mid-1;
	}
	printf("%lld",ans);
}

void dfs(int x,int fa)
{
	for(int i=fir[x]; i; i=nxt[i])
	{
		if(to[i]==fa)continue;
		dis[to[i]]=dis[x]+val[i];
		if(dis[to[i]]>res)
		{
			res=dis[to[i]];
			S=to[i];
		}
		dfs(to[i],x);
	}
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	bool tp1=1,tp2=1;
	for(int i=1; i<n; i++)
	{
		int a=read(),b=read();
		A[i]=read();
		sum+=A[i];
		o[a]=i;
		add(a,b,A[i],i);
		add(b,a,A[i],i);
		if(a!=1)tp1=0;
		if(a+1!=b)tp2=0;
	}
	if(tp1)
	{
		sol1();
		return 0;
	}
	if(tp2)
	{
		sol2();
		return 0;
	}
	if(m==1)
	{
		memset(dis,0,sizeof(dis));
		dfs(1,0);
		memset(dis,0,sizeof(dis));
		res=0;
		dfs(S,0);
		printf("%lld\n",res);
		return 0;
	}
	return 0;
}
