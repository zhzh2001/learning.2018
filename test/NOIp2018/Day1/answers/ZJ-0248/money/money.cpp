#include<bits/stdc++.h>

using namespace std;

inline int read()
{
	int x=0;
	char c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
	return x;
}

int n,a[110];
map<int,bool>m;
map<int,bool>::iterator it;

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--)
	{
		n=read();
		int mx=0;
		for(int i=1; i<=n; i++)
		{
			a[i]=read();
			mx=max(mx,a[i]);
		}
		sort(a+1,a+n+1);
		if(a[1]==1)
		{
			printf("1\n");
			continue;
		}
		int ans=n;
		m.clear();
		for(int i=1; i<=n; i++)
		{
			if(m.count(a[i]))
			{
				ans--;
				continue;
			}
			m[a[i]]=1;
			for(it=m.begin(); it!=m.end(); it++)
			{
				int k=it->first;
				if(k+a[i]>mx)break;
				m[k+a[i]]=1;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
