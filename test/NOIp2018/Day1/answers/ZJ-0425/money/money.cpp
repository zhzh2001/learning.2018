#include<cstdio>
#include<algorithm>
using namespace std;
int T,n,cnt,ans,a[1100],b[1100],f[310000];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		b[1]=a[1]; cnt=1;
		for (int i=2;i<=n;i++)
			if (a[i]!=a[i-1]) b[++cnt]=a[i];
		f[0]=1;
		for (int i=1;i<=25000;i++){
			f[i]=0;
			for (int j=1;j<=cnt&&b[j]<=i;j++) f[i]+=f[i-b[j]];
		}
		ans=cnt;
		for (int i=1;i<=cnt;i++)
			if (f[b[i]]>1) ans--;
		printf("%d\n",ans);
	}
	return 0;
}

