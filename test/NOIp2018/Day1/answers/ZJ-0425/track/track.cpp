#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;
vector<int> vec[110000];
int n,m,mid,a[110000],b[110000],l[110000],tot,ans,dp[110000],tmp[110000];
int edgenum,vet[110000],val[110000],Next[110000],Head[110000];
namespace task1{
	int edgenum,vet[110000],val[110000],Next[110000],Head[110000],dp[110000];
	int ans;
	void addedge(int u,int v,int cost){
		vet[++edgenum]=v; val[edgenum]=cost;
		Next[edgenum]=Head[u]; Head[u]=edgenum;
	}
	void dfs(int u,int fa){
		int mx1=0,mx2=0; dp[u]=0;
		for (int e=Head[u];e;e=Next[e])
			if (vet[e]!=fa){
				dfs(vet[e],u);
				if (dp[vet[e]]+val[e]>=mx1){
					mx2=mx1;
					mx1=dp[vet[e]]+val[e];
				} else if (dp[vet[e]]+val[e]>=mx2) mx2=dp[vet[e]]+val[e];
			}
		dp[u]=mx1; ans=max(mx1+mx2,ans);
	}
	int work(){
		ans=0;
		for (int i=1;i<n;i++){
			addedge(a[i],b[i],l[i]);
			addedge(b[i],a[i],l[i]);
		}
		dfs(1,0); return ans;
	}
}
namespace task2{
	int ans;
	int work(){
		ans=0x3f3f3f3f;
		sort(l+1,l+n);
		for (int i=n-m,j=n-m-1;i<n;i++,j--)
			if (j>=1) ans=min(ans,l[i]+l[j]);
			else ans=min(ans,l[i]);
		return ans;
	}
}
namespace task3{
	int ans,v[110000];
	int work(){
		for (int i=1;i<n;i++) v[a[i]]=l[i];
		int l=1,r=500000000,mid,tot,sum;
		while (l<=r){
			mid=(l+r)>>1; tot=0; sum=0;
			for (int i=1;i<n;i++){
				sum+=v[i]; 
				if (sum>=mid){
					tot++;
					sum=0;
				}
			}
			if (tot>=m){
				ans=mid;
				l=mid+1;
			} else r=mid-1;
		}
		return ans;
	}
}
void addedge(int u,int v,int cost){
	vet[++edgenum]=v; val[edgenum]=cost;
	Next[edgenum]=Head[u]; Head[u]=edgenum;
}
void dfs(int u,int fa){
	int cnt=0; vec[u].clear();
	for (int e=Head[u];e;e=Next[e])
		if (vet[e]!=fa){
			cnt++; dfs(vet[e],u);
			vec[u].push_back(dp[vet[e]]+val[e]);
		}
	sort(vec[u].begin(),vec[u].end());
	while (cnt&&vec[u][cnt-1]>=mid){
		tot++;
		cnt--;
	}
	tmp[0]=0;
	for (int i=0;i<cnt;i++) tmp[i+1]=vec[u][i]; 
	int mx=0,s=0,l,r,tot1;
	for (int i=0;i<=cnt;i++){
		tot1=0;
		for (l=1+(i==1),r=cnt-(i==cnt);l<r;){
			if (tmp[l]+tmp[r]>=mid){
				tot1++;
				r--; if (r==i) r--;
			}
			l++;
			if (l==i) l++;
		}
		if (tot1>mx){
			mx=tot1;
			s=tmp[i];
		} else if (tot1==mx) s=max(s,tmp[i]);
	}
	tot+=mx; dp[u]=s;
}
bool check(){
	tot=0; dfs(1,0);
	return tot+(dp[1]>=mid)>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool istask1=(m==1),istask2=true,istask3=true;
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&a[i],&b[i],&l[i]);
		addedge(a[i],b[i],l[i]); addedge(b[i],a[i],l[i]);
		istask2=istask2&&(a[i]==1); istask3=istask3&&(b[i]==(a[i]+1));
	}
	if (istask1) printf("%d\n",task1::work());
	else if (istask2) printf("%d\n",task2::work());
	else if (istask3) printf("%d\n",task3::work());
	else {
		int l,r;
		l=1; r=500000000;
		while (l<=r){
			mid=(l+r)>>1;
			if (check()){
				ans=mid;
				l=mid+1;
			} else r=mid-1;
		}
		printf("%d\n",ans);
	}
	return 0;
}

