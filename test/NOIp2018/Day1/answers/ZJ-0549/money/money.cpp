#include<bits/stdc++.h>
#define ll long long
#define C getchar()
#define Violet inline
#define M 25050
#define N 105
using namespace std;
Violet int read()
{
	int x=0,l=1;char c=C;
	for (;c<'0'||c>'9';c=C) if (c=='-') l=-1;
	for (;c>='0'&&c<='9';c=C) x=x*10+c-48;
	return x*l;
}

int T,n,ma,ans;
int a[N];
int s[N];
int v[M];

Violet void work()
{
	for (int i=1;i<=ma;i++)
		v[i]=0;
	v[0]=1;
	for (int i=1;i<=n;i++)
		if (v[a[i]])
			s[i]=0;
		else
			for (int j=a[i];j<=ma;j++)
				v[j]|=v[j-a[i]];
}

int main()
{
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	T=read();
	while (T--)
		{
			ans=n=read();
			for (int i=1;i<=n;i++)
				{
					a[i]=read();
					s[i]=1;
				}
			sort(a+1,a+n+1);
			ma=a[n];
			work();
			for (int i=1;i<=n;i++)
				if (!s[i])
					ans--;
			printf("%d\n",ans);
		}
	return 0;
}
