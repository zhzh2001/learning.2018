#include<bits/stdc++.h>
#define ll long long
#define C getchar()
#define Violet inline
#define N 50050
using namespace std;
Violet int read()
{
	int x=0,l=1;char c=C;
	for (;c<'0'||c>'9';c=C) if (c=='-') l=-1;
	for (;c>='0'&&c<='9';c=C) x=x*10+c-48;
	return x*l;
}

struct Vio {int next,y,v;}e[N<<1];
struct P {int l,sum;};

int n,m,ans,Lp=-1,mx;
int lin[N],L;
int p[N];

Violet void fnp(int x,int y,int v) { e[++L]=(Vio){lin[x],y,v},lin[x]=L; }

Violet void dfsp(int x)
{
	for (int i=lin[x],y;i,y=e[i].y;i=e[i].next)
		if (y!=p[x])
			{
				p[y]=x;
				dfsp(y);
			}
}

Violet void dfso(int x,int par,int sum)
{
	if (sum>Lp) mx=x,Lp=sum;
	for (int i=lin[x],y;i,y=e[i].y;i=e[i].next)
		if (y!=par)
			dfso(y,x,sum+e[i].v);
}

Violet P dfs(int x,int v,int k)
{
	int M[5]={},res=0;
	for (int i=lin[x],y;i,y=e[i].y;i=e[i].next)
		if (y!=p[x])
			{
				P opp=dfs(y,e[i].v,k);
				res+=opp.sum;
				if (opp.l>M[1])
					M[3]=M[2],M[2]=M[1],M[1]=opp.l;
				else if (opp.l>M[2])
					M[3]=M[2],M[2]=opp.l;
				else if (opp.l>M[3])
					M[3]=opp.l;
				else if (opp.l>=k)
					res++;
			}
	if (M[2]+M[3]>=k) return (P){M[1]+v,res+1};
	if (M[1]+M[3]>=k) return (P){M[2]+v,res+1};	
	if (M[1]+M[2]>=k) return (P){M[3]+v,res+1};
	if (M[1]+v>=k) return (P){0,res+1};
	return (P){M[1]+v,res};
}

int main()
{
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<n;i++)
		{
			int x=read(),y=read(),v=read();
			fnp(x,y,v);
			fnp(y,x,v);
		}
	dfso(1,0,0),Lp=-1,dfso(mx,0,0);
	if (m==1) { printf("%d\n",Lp);return 0; }
	dfsp(1);
	for (int j=29;j>=0;j--)
		if (dfs(1,0,ans+(1<<j)).sum>=m)
			ans+=1<<j;
	printf("%d\n",ans);
	return 0;
}
