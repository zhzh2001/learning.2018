#include<bits/stdc++.h>
#define ll long long
#define C getchar()
#define Violet inline
#define N 100500
using namespace std;
Violet int read()
{
	int x=0,l=1;char c=C;
	for (;c<'0'||c>'9';c=C) if (c=='-') l=-1;
	for (;c>='0'&&c<='9';c=C) x=x*10+c-48;
	return x*l;
}

int n;
int a[N];
int lg[N];
int f[N][25];

Violet void prepare()
{
	for (int j=1;j<=20;j++)
		for (int i=1;i+(1<<j)-1<=n;i++)
			if (a[f[i][j-1]]<a[f[i+(1<<j-1)][j-1]])
				f[i][j]=f[i][j-1];
			else
				f[i][j]=f[i+(1<<j-1)][j-1];
}

Violet int dfs(int l,int r,int pol)
{
	if (l>r) return 0;
	int len=r-l+1,p;
	if (a[f[l][lg[len]]]<a[f[r-(1<<lg[len])+1][lg[len]]])
		p=f[l][lg[len]];
	else
		p=f[r-(1<<lg[len])+1][lg[len]];
	return (dfs(l,p-1,a[p])+dfs(p+1,r,a[p])+a[p]-pol);
}

int main()
{
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++)
		{
			a[i]=read();
			f[i][0]=i;
			lg[i]=log2(i);
		}
	prepare();
	printf("%d\n",dfs(1,n,0));
	return 0;
}
