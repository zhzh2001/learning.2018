#include<bits/stdc++.h>
using namespace std;
#define inf 0x3f3f3f3f
int read ()
{
	int x=0,f=1;
	char c=getchar();
	while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();}
	while (c>='0'&&c<='9') {x=x*10+c-'0'; c=getchar();}
	return x*f;
}
int t,n;
int a[110];
int f[25010];
int i,j,k;
int maxa;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read();
	while (t--)
	{
		n=read();
		for (i=1; i<=n; i++) a[i]=read();
		sort(a+1,a+1+n);
		maxa=a[n];
		int ans=n;
		memset(f,0,sizeof(f));
		f[0]=1;
		for (i=0; i<=maxa; i++)
		if (f[i])
		{
			for (j=1; j<=n; j++)
			{
				if (i+a[j]>maxa) break;
				if (f[i+a[j]]>=2) continue;
				f[i+a[j]]++;
			}
		}
		for (i=1; i<=n; i++) if (f[a[i]]>1) ans--;
		cout<<ans<<endl;
	}
	return 0;
}
