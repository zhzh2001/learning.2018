#include<bits/stdc++.h>
using namespace std;
#define inf 0x3f3f3f3f
int read ()
{
	int x=0,f=1;
	char c=getchar();
	while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();}
	while (c>='0'&&c<='9') {x=x*10+c-'0'; c=getchar();}
	return x*f;
}
int n,m;
struct edge{
	int a,b,l;
}l[50010];
int in[50010];
int i,j,k;
int head[50010],nxt[100010],to[100010],w[100010];
int tot=0;
int len,v;
int sum=0,ave;
int ans;
void add(int x,int y,int z)
{
	tot++;
	nxt[tot]=head[x];
	to[tot]=y;
	w[tot]=z;
	head[x]=tot;
}
void dfs(int u,int fa,int l)
{
	int i;
	if (l>len)
	{
		len=l;
		v=u;
	}
	for (i=head[u]; i; i=nxt[i])
	{
		int t=to[i];
		if (t==fa) continue;
		dfs(t,u,l+w[i]);
	}
}
void dfs1(int u,int fa,int l)
{
	int i;
//	cout<<1<<endl;
	for (i=head[u]; i; i=nxt[i])
	{
		int v=to[i];
		if (v==fa) continue;
		if (l<=ave&&((l+w[i])>=ave))
		{
			if ((ave-l)<=(l+w[i]-ave))
			{
				ans=min(ans,l);
//				cout<<1<<endl;
				dfs1(v,u,w[i]);
			}
			else
			{
				ans=min(ans,l+w[i]);
//				cout<<1<<endl;
				dfs1(v,u,0);
			}
		}
		else dfs1(v,u,l+w[i]);
	}
}
bool cmp1(edge x,edge y)
{
	return x.l>y.l;
}
bool cmp2(edge x,edge y)
{
	return x.l<y.l;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();
	m=read();
	if (m==1)
	{
		for (i=1; i<n; i++)
		{
			l[i].a=read();
			l[i].b=read();
			l[i].l=read();
			add(l[i].a,l[i].b,l[i].l);
			add(l[i].b,l[i].a,l[i].l);
		}
		len=0;
		dfs(1,-1,0);
		len=0;
		dfs(v,-1,0);
		cout<<len<<endl;
		return 0;
	}
	else if (m==n-1)
	{
		int minl=inf;
		for (i=1; i<n; i++)
		{
			l[i].a=read();
			l[i].b=read();
			l[i].l=read();
			minl=min(minl,l[i].l);
			//add(a,b,l);
			//add(b,a,l);
		}
		cout<<minl<<endl;
		return 0;
	}
	bool check=true;
	bool flag=true;
	for (i=1; i<n; i++)
	{
		l[i].a=read();
		l[i].b=read();
		if (l[i].b!=l[i].a+1) check=false;
		if (l[i].a!=1) flag=false;
		l[i].l=read();
		sum+=l[i].l;
		add(l[i].a,l[i].b,l[i].l);
		in[l[i].a]++;
		add(l[i].b,l[i].a,l[i].l);
		in[l[i].b]++;
	}
	ave=sum/m;
	if (check)
	{
		ans=inf;
		int pos;
		for (i=1; i<=n; i++) if (in[i]==1) pos=i;
//		cout<<pos<<endl;
//		cout<<ave<<endl;
		dfs1(pos,-1,0);
		cout<<ans<<endl;
		return 0;
	}
	if (flag)
	{
		n=n-1;
		if (m*2<=n)
		{
			sort(l+1,l+n+1,cmp1);
			ans=inf;
			for (i=1; i<=m; i++) ans=min(ans,l[i].l+l[2*m-i+1].l);
			cout<<ans<<endl;
			return 0;
		}
		if (m*2>n)
		{
			sort(l+1,l+n+1,cmp2);
			ans=inf;
			for (i=1; i<=n-m; i++) ans=min(ans,l[i].l+l[2*(n-m)-i+1].l);
			for (i=2*(n-m)+1; i<=n; i++) ans=min(ans,l[i].l);
			cout<<ans<<endl;
		}
	}
	return 0;
}
/*
6 3
1 2 5
2 3 4
3 4 7
4 5 8
5 6 2

7 4
1 2 1
1 3 2
1 4 3
1 5 4
1 6 5
1 7 6
*/
