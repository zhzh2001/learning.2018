#include<cstdio>
#include<cstring>
#include<algorithm>
bool f[30000];
const int MAXN = 106;
int a[MAXN];
using namespace std;
int main() {
  int t;
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  scanf("%d",&t);
  while (t--) {
    int n;
    scanf("%d",&n);
    for (int i=1;i<=n;++i)
      scanf("%d",&a[i]);
    sort(a+1,a+n+1);
    memset(f,false,sizeof(f));
    f[0]=1;
    int ans=n;
    for (int i=1;i<=n;++i)
      if (f[a[i]]) --ans;
      else 
        for (int j=0;j<=25000;++j)
        if (j>=a[i]) f[j]=f[j]|f[j-a[i]];
    printf("%d\n",ans);
  }
 return 0;
}
