#include<cstdio>
#include<algorithm>
using namespace std;
const int MAXN = 1e5+6;
int a[MAXN];
int main() {
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
  int n,ans=0;
  scanf("%d",&n);
  for (int i=1;i<=n;++i)
    scanf("%d",&a[i]),ans+=a[i];
  for (int i=2;i<=n;++i)
    ans-=min(a[i],a[i-1]);
  printf("%d\n",ans);
  return 0;
}