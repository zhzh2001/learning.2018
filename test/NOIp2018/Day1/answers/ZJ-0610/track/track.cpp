#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int MAXN = 50006;
struct node {
  int to,cost,nxt;
} vet[MAXN<<1];
int h[MAXN],num=1,n,m;
inline void add(int u,int v,int w) { vet[++num]=(node){v,w,h[u]}; h[u]=num; }
namespace subtask1 {
  int mx,mx1,mx2;
  void dfs(int u,int last,int s) {
  	if (s>mx) mx=s;
    for (int e=h[u];e;e=vet[e].nxt) {
      int v=vet[e].to;
      if (v==last) continue;
      dfs(v,u,s+vet[e].cost);
    }
  }
  inline void solve() {
    int ans=0;
    for (int i=1;i<=n;++i) {
      mx1=0,mx2=0;
      for (int e=h[i];e;e=vet[e].nxt) {
	    int v=vet[e].to;
	    mx=0;
	    dfs(v,i,vet[e].cost);
	    if (mx>mx1) {mx2=mx1,mx1=mx; }
	    else if (mx>mx2) mx2=mx;
      }
      ans=max(ans,mx1+mx2);
    }
    printf("%d\n",ans);
  }
}
namespace subtask2 {
  int need;
  bool use[MAXN],flag;
  int a[MAXN];
  void dfs(int u,int last,int t,int s,int mx) {
    if (s>=mx) {
      for (int i=1;i<t;++i)	
        use[a[i]]=1,use[a[i]^1]=1;
      --need;
      flag=1;
      return;
    }
	for (int e=h[u];e;e=vet[e].nxt) {
	  int v=vet[e].to;
	  if (v==last || use[e]) continue;
	  a[t]=e;
	  dfs(v,u,t+1,s+vet[e].cost,mx);
	  if (flag) return;
    }
  }
  bool check(int x) {
  	need=m;
  	memset(use,false,sizeof(use));
  	for (int i=1;i<=n;++i)
    flag=0,dfs(i,0,1,0,x);  	
    return need<=0 ? 1 : 0;
  }
  inline void solve() {
    int l=1,r=1e9,ans;
    while (l<=r) {
      int mid=l+(r-l)/2;	
      if (check(mid)) {
        ans=mid;
		l=mid+1;	
      }
      else r=mid-1;
    }
    printf("%d\n",ans);
  }
}
int main() {
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  scanf("%d%d",&n,&m);
  for (int i=1;i<n;++i) {
  	int u,v,w;
    scanf("%d%d%d",&u,&v,&w);
    add(u,v,w); add(v,u,w);
  }
  if (m==1) subtask1 :: solve();
  else subtask2 :: solve();
  return 0;
}
