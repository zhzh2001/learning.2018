#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<algorithm>
using namespace std;
struct nob{
	int to,jump,val,mark;
}a[100005];
int jump[50005],n,m,tot=1,dp[50005],fa[50005],lin=1,point[25],flower=1;
void add(int x,int y,int z){
	tot++;
	a[tot].to=y;
	a[tot].jump=jump[x];
	a[tot].val=z;
	jump[x]=tot;
}
bool mmp(nob a,nob b){
	return a.val<b.val;
}
int ans;
void Build(int pos){
	int maxn1=0,maxn2=0;
	for (int i=jump[pos]; i; i=a[i].jump){
		if (a[i].to==fa[pos]) continue ;
		fa[a[i].to]=pos;
		Build(a[i].to);
		if (dp[a[i].to]+a[i].val>maxn1){
			maxn2=maxn1;
			maxn1=dp[a[i].to]+a[i].val;
		}
		else if (dp[a[i].to]+a[i].val>maxn2){
			maxn2=dp[a[i].to]+a[i].val;
		}
		if (maxn1+maxn2>ans) ans=maxn1+maxn2;
		dp[pos]=maxn1;
	}
}
int line[20],cal=1e9+7;
void linec(int pos){
	if (pos==n+1){
		cal=1e9+7;
		int rem,ram=2;
		if (line[n]!=m) return ;
		while (ram<=n){
			rem=0;
			while (!line[ram]) ram++;
			rem=point[ram];
			while (line[ram+1]==line[ram]){
				rem+=line[ram+1];
				ram++;
			}
			cal=min(cal,rem);
		}
		ans=max(ans,cal);
		return ;
	}
	line[pos]=0;
	linec(pos+1);
	for (int i=1; i<=m; i++){
		if (!line[pos-1]){
			line[pos]=1;
			linec(pos+1);
		}
		else if (line[pos-1]==m){
			line[pos]=m;
			linec(pos+1);
		}
		else if (line[pos-1]==i){
			line[pos]=i;
			linec(pos+1);
			line[pos]=i+1;
			linec(pos+1);
		}
	}
}
void luangao(){
	if (lin){
		for (int i=2; i<=(n-1)*2; i+=2)
			point[a[i].to]=a[i].val;
		linec(2);
		printf("%d",ans);
		return ;
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int mina=1e9+7,mmin=1e9+7;
	scanf("%d%d",&n,&m);
	for (int i=1,x,y,z; i<n; i++){
		scanf("%d%d%d",&x,&y,&z);
		if (abs(x-y)!=1) lin=0;
		if (x!=1) flower=0;
		if (mina>z){
			mmin=mina;
			mina=z;
		}
		else if (mmin>z) mmin=z;
		add(x,y,z);
		add(y,x,z);
	}
	if (m==1){
		Build(1);
		printf("%d",ans);
		return 0;
	}
	if (m==n-1){
		printf("%d",mina);
		return 0;
	}
	if (n<=10){
		luangao();
		return 0;
	}
	if (flower){
		sort(a+2,a+n*2+1,mmp);
		if (2*m>n-1){
			int rem=1e9+7;
			int pos=2;
			while (pos<=(n-1-2*m)*4){
				rem=min(rem,a[pos].val+a[pos+2].val);
				pos+=4;
			}
			while (pos<=(n-1)*2){
				rem=min(rem,a[pos].val);
				pos+=2;
			}
			printf("%d",rem);
			return 0;
		}
		else{
			int le=(n-2*m+1)*2;
			int ri=n*2;
			int rem=1e9+7,ram=1e9+7;
			while (le<ri){
				rem=min(rem,a[le].val+a[ri].val);
				le+=2;
				ri+=2;
			}
			le=(n-2*m+1)*2;
			while (le<n*2){
				ram=min(ram,a[le].val+a[le+2].val);
				le+=4;
			}
			printf("%d",max(rem,ram));
			return 0;
		}
	}
	return 0;
}
