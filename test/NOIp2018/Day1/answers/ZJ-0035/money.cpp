#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<algorithm>
using namespace std;
int n,mp[25005],T,ans,a[25005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		memset(mp,0,sizeof(mp));
		mp[0]=1;
		ans=0;
		for (int i=1; i<=n; i++)
			scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		for (int i=1; i<=n; i++)
			if (!mp[a[i]]){
				ans++;
				for (int l=0; l+a[i]<=25000; l++)
					if (mp[l]) mp[l+a[i]]=1;
			}
		printf("%d\n",ans);
	}
	return 0;
}
