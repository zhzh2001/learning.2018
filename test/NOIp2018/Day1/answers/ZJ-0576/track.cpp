#include <stdio.h>
#include <queue>

static const int MAXN = 100005;
static int Head[MAXN], vet[MAXN], len[MAXN], Next[MAXN];
static int dis[MAXN], n, x, y, z, m, tot, flag[MAXN], low[MAXN];
void add(int a, int b, int c) {
	Next[++tot] = Head[a];
	vet[tot] = b;
	len[tot] = c;
	Head[a] = tot;
}
struct s{
	int u, v;
	int operator <(const s &a) const {
		return a.v > v;
	}
	s(const int &u,const int &v) :u(u), v(v){}
};
std::priority_queue<s> Q;
std::priority_queue<int> QQ;
int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d %d", &n, &m);
	for (int i=1; i<n; ++i ) {
		scanf("%d %d %d", &x, &y, &z);
		add(x, y, z);
		add(y, x, z);
		low[x] ++;
		low[y] ++;
	}
	//for (int i=1; i<=n; ++i ){
	//	printf("%d ", low[i]);
	//}
	//puts("");
	for (int j=1;  j<=n; ++j ) {
		if (low[j] != 1) continue;
		for (int i=1; i<=n; ++i ) dis[i] = -1e9;
		for (int i=1; i<=n; ++i ) flag[i] = 0;
		dis[j] = 0;
		Q.push(s(j, 0));
		while (!Q.empty()) {
			s now = Q.top();
			Q.pop();
			int x = now.u;
			if (flag[x]) continue;
			if (now.v != dis[x]) continue;
			flag[x] = 1;
			for (int i=Head[x]; i; i=Next[i]) {
				int v = vet[i], z = len[i];
				if (dis[x] + z > dis[v] && flag[v] == 0) {
					dis[v] = dis[x] + z;
					Q.push(s(v, dis[v]));
				}
			}
		}
		//for (int i=1; i<=n; ++i ) 
		//	printf("%d ", dis[i]);
		//puts("");
		for (int i=1; i<=n; ++i ) {
			if (low[i] != 1) continue;
			QQ.push(dis[i]);
		}
	}
	for (int i=1; i<m; ++i ){
		QQ.pop();
	}
	printf("%d", QQ.top());
	return 0;
}
