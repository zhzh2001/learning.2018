#include <string.h>
#include <stdio.h>

static int T, n;
static int a[105];
static int f[1000005];
int gcd(int a, int b) {
	if (b ==0 ) return a;else 
		return gcd(b, a%b);
}
int abs(int x) {
	return x * x;
}
int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	while (T--) {
		memset (f, 0, sizeof(f));
		scanf("%d", &n);
		int ans = n;
		for (int i=1; i<=n;++i ) {
			scanf("%d", &a[i]);
			f[a[i]] = 1;
		}
		for (int i=1; i<n; ++i ){
			for (int j=i + 1; j<=n; ++j ){
				if (a[j] % a[i] == 0){
					int k = a[j] / a[i];
					int low = 1;
					while (low < k){
						if (f[low * a[i]] == 0) break;
						low <<= 1;
					}
					if (low >= k) ans --;
				}else{
					if (a[i] % a[j] == 0){
						int k = a[i] / a[j];
						int low = 1;
						while (low < k){
							if (f[low * a[j]] == 0) break;
							low <<= 1;
						}
						if (low >= k) ans --;
					}else{
				//if (f[abs(a[i] - a[j])] == 1 ) ans--;
					//if (a[j] > a[i]) continue;
					int low = abs(a[i] - a[j]);
					//printf("%d\n", low);
					for (int k=2; k<=low; ++k ) {
						if (low % k == 0 ) {
							int kk = low / k;
							int num = 1;
							while (num < kk){
								if (f[num * k] == 0) break;
								num <<= 1;
							}
							if (num >= k) {
								ans --;
								break;
							}
						}
					}
				}
				}
			}
		}
		printf("%d\n", ans);
	}
	return 0;
}
