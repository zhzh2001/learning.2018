#include <stdio.h>

typedef long long LL;
static LL ans = 0;
static LL n, a[100005];
int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%lld", &n);
	for (int i=1; i<=n; ++i ) scanf("%lld", &a[i]);
	ans = a[n];
	for (int i=n-1; i>=1; --i) {
		if (a[i] > a[i + 1]) ans += (a[i] - a[i + 1]);
	}
	printf("%lld", ans);
	return 0;
}
