#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
int n,m,tot,x,y,z,id,ans;
int p[50005];
int head[50005],Next[100005],to[100005],len[100005],edge[100005],Max[50005];
void add(int x,int y,int z)
{
	tot++;
	Next[tot]=head[x];
	to[tot]=y;
	len[tot]=z;
	edge[tot]=id;
	head[x]=tot;
}	
void syx(int k,int pre)
{
	int Max1=0,Max2=0;
	for(int i=head[k];i!=-1;i=Next[i]) 
	if(to[i]!=pre) 
	{
		syx(to[i],k);
		if(Max[to[i]]+len[i]>Max1) Max2=Max1,Max1=Max[to[i]]+len[i];
		else 
		if(Max[to[i]]+len[i]>Max2) Max2=Max[to[i]]+len[i];
		Max[k]=max(Max[k],Max[to[i]]+len[i]);
	}
	ans=max(ans,Max1+Max2);
}
void ty(int k,int road,int left)
{
	if(road<=ans) return;
	if(left<m-k+1) return;
	if(k>m) 
	{
		ans=max(ans,road);
		return;
	}
	int s,t,w,x,ok,used=0;
	int a[20],b[20],c[20],d[20];
	a[0]=0;
	for(int i=1;i<=n;i++) 
	for(int j=i+1;j<=n;j++) 
	{
		t=0;w=1;ok=0;
		a[1]=i;b[1]=0;c[1]=0;
		while(t<w) 
		{
			t++;
			x=a[t];
			if(x==j)
			{
				ok=1;
				break;
			}
			for(int y=head[x];y!=-1;y=Next[y]) 
			if(to[y]!=a[b[t]]&&p[edge[y]]==0) 
			{
				w++;
				a[w]=to[y];b[w]=t;c[w]=edge[y];d[w]=len[y];
			}
		}
		if(ok==1) 
		{
		    s=0;used=0;
	 	    x=t;
		    while(a[x]!=i) 
		    {
			    p[c[x]]++;
			    s=s+d[x];
			    x=b[x];
			    used++;
		    }
		    ty(k+1,min(s,road),left-used);
		    x=t;
		    while(a[x]!=i) 
		    {
			    p[c[x]]--;
			    x=b[x];
		    }
		}
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<=n;i++) head[i]=-1;
	for(int i=1;i<n;i++) 
	{
		scanf("%d%d%d",&x,&y,&z);
		id++;
		add(x,y,z);
		add(y,x,z);
	}
	if(m==1) syx(1,0); else ty(1,1e9,n-1);
	cout<<ans;
	fclose(stdin);
	fclose(stdout);
	return 0;
}
