#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cmath>
#define ll long long
#define p1 id<<1
#define p2 id<<1^1
using namespace std;
int a[100005],b[100005],c[100005],d[100005];
int tree[400005],num[400005];
ll ans;
int n,t,w,x;
void build(int id,int l,int r)
{
	if(l==r) 
	{
		tree[id]=d[l];
		num[id]=l;
		return;
	}
	int mid=(l+r)/2;
	build(p1,l,mid);
	build(p2,mid+1,r);
	if(tree[p1]<tree[p2]) tree[id]=tree[p1],num[id]=num[p1];else tree[id]=tree[p2],num[id]=num[p2];
}
int query(int id,int l,int r,int x,int y)
{
	if(x<=l&&r<=y) return num[id];
	int mid=(l+r)/2;
	if(y<=mid) return query(p1,l,mid,x,y);
	else
	if(x>mid) return query(p2,mid+1,r,x,y);
	else 
	{
		int u=query(p1,l,mid,x,mid);
		int v=query(p2,mid+1,r,mid+1,y);
		if(d[u]<d[v]) return u; else return v;
	}
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	cin>>n;
	for(int i=1;i<=n;i++) scanf("%d",&d[i]);
	build(1,1,n);
	w=1;
	a[1]=1;b[1]=n;c[1]=0;
	while(t<w) 
	{
		t++;
		x=query(1,1,n,a[t],b[t]);
		ans=ans+d[x]-c[t];
		if(x-1>=a[t]) 
		{
			w++;
			a[w]=a[t];
			b[w]=x-1;
			c[w]=d[x];
		}
		if(x+1<=b[t]) 
		{
			w++;
			a[w]=x+1;
			b[w]=b[t];
			c[w]=d[x];
		}
	}
	cout<<ans;	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
