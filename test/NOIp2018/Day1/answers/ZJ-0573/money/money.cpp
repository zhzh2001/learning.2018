#include<bits/stdc++.h>
using namespace std;
int T,n,ans,maxn;
int a[105];
bool f[25005],v[105];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int i,j;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1),ans=n,maxn=a[n];
		memset(f,0,sizeof(f)),memset(v,0,sizeof(v));
		f[0]=1;
		for(i=2;i<=n;i++){
			if(!v[i-1]) for(j=a[i-1];j<=maxn;j++) if(!f[j]&&f[j-a[i-1]]) f[j]=1;
			if(f[a[i]]) ans--,v[i]=1;
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*
2
4
3 19 10 6
5
11 29 13 19 17

*/
