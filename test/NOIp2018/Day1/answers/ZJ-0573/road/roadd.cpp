#include<bits/stdc++.h>
#define ll long long
using namespace std;
struct S{
	int x,num;
	bool operator < (const S & w) const{
		return x>w.x;
	} 
}b[100005];
int n,maxn;
int a[100005],l[100005],r[100005];
bool v[100005];
ll ans;
int main(){
	//freopen("road.in","r",stdin);
	//freopen("road.out","w",stdout);
	int i,j,x;
	scanf("%d",&n);
	for(i=1;i<=n;i++) scanf("%d",&a[i]),b[i].x=a[i],b[i].num=i;
	l[1]=1,r[n]=n,a[0]=a[n+1]=0;
	for(i=2;i<=n;i++) if(a[i]==a[i-1]) l[i]=l[i-1];
		else l[i]=i;
	for(i=n-1;i>=2;i--) if(a[i]==a[i+1]) r[i]=r[i+1];
		else r[i]=i;
	sort(b+1,b+n+1);
	memset(v,0,sizeof(v)),ans=0;
	for(i=1;i<=n;i++){
		x=b[i].num;
		printf("%d %d %d\n",x,r[x],l[x]);
		if(v[x]) continue;
		if(x==1){
			ans+=a[x]-a[r[x]+1];
			for(j=x;j<=r[x];j++) a[j]=a[r[x]+1],r[j]=r[r[x]+1];
			continue;
		}
		if(x==n){
			ans+=a[x]-a[l[x]-1];
			for(j=x;j>=l[x];j--) a[j]=a[l[x]-1],l[j]=l[l[x]-1];
			continue;
		}
		maxn=max(a[r[x]+1],a[l[x]-1]);
		ans+=a[x]-maxn;
		for(j=l[x];j<=r[x];j++){
			a[j]=maxn;
			if(a[l[x]-1]==maxn) l[j]=l[l[x]-1];
			if(a[r[x]+1]==maxn) r[j]=r[r[x]+1];
		}
	}
	printf("%lld",ans);
	//fclose(stdin);
	//fclose(stdout);
	return 0;
}
/*
6
4 3 2 5 3 5

*/
