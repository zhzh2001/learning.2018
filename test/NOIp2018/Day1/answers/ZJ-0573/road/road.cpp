#include<bits/stdc++.h>
#define ll long long
using namespace std;
int n,lgn,lg[100005];
int a[100005];
int f[100005][20],g[100005][20];
ll ans;
int getpos(int l,int r){
	int y=lg[r-l+1];
	if(f[l][y]>f[r-(1<<y)+1][y]) return g[r-(1<<y)+1][y];
	return g[l][y];
}
ll work(int l,int r,int sum){
	if(l>r) return 0;
	if(l==r) return a[l]-sum;
	int maxn,t;
	t=getpos(l,r);
	maxn=a[t]-sum;
	return work(l,t-1,sum+maxn)+work(t+1,r,sum+maxn)+maxn;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int i,j;
	scanf("%d",&n);
	lgn=(int)(log(n)/log(2))+1;
	for(i=1;i<=n;i++) scanf("%d",&a[i]),f[i][0]=a[i],g[i][0]=i;
	lg[0]=-1;
	for(i=1;i<=n;i++) lg[i]=lg[i>>1]+1;
	for(j=1;j<=lgn;j++) for(i=1;i+(1<<j)-1<=n;i++){
		if(f[i][j-1]>f[i+(1<<(j-1))][j-1]) f[i][j]=f[i+(1<<(j-1))][j-1],g[i][j]=g[i+(1<<(j-1))][j-1];
		else f[i][j]=f[i][j-1],g[i][j]=g[i][j-1];
	}
	ans=work(1,n,0);
	printf("%lld",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*
6
4 3 2 5 3 5

*/
