#include<bits/stdc++.h>
using namespace std;
struct S{
	int u,v,w,num;
	bool operator < (const S & k) const{
		return w>k.w;
	}
}b[50005],p;
struct ed{
	int nxt,to,dis;
}e[100005];
int h[50005],cnt;
int n,m,sum;
int a[50005],d[50005];
bool v[50005],vis[100005],flag;
queue<int> q;
priority_queue<S> hp;
void add(int from,int to,int dis){
	e[++cnt].nxt=h[from],e[cnt].to=to,e[cnt].dis=dis,h[from]=cnt;
}
bool calc(int x){
	int i,t=a[1],tot=0;
	for(i=2;i<=n;i++){
		if(t>=x) tot++,t=a[i];
		else t+=a[i]; 
	}
	if(t>=x) tot++;
	if(tot>=m) return true;
	return false;
}
int bfs(int s){
	int i,x,y,z,maxn=-1,t;
	while(!q.empty()) q.pop();
	memset(v,0,sizeof(v));
	d[s]=0,v[s]=1,q.push(s);
	while(!q.empty()){
		x=q.front(),q.pop();
		for(i=h[x];i;i=e[i].nxt){
			y=e[i].to,z=e[i].dis;
			if(v[y]) continue;
			v[y]=1,d[y]=d[x]+z;
			q.push(y);
		}
	}
	for(i=1;i<=n;i++) if(d[i]>maxn) maxn=d[i],t=i;
	return t;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int i,x,y,z,s,t,tot,maxn;
	scanf("%d %d",&n,&m);
	memset(h,0,sizeof(h)),cnt=1,sum=0,flag=1;
	for(i=1;i<n;i++){
		scanf("%d %d %d",&x,&y,&z);
		if(x>y) swap(x,y);
		add(x,y,z),add(y,x,z),sum+=z;
		a[x]=b[i].w=z,b[i].u=x,b[i].v=y,b[i].num=2*i;
		if((y-x)!=1) flag=0;
	}
	if(flag){
		int l=1,r=sum/m,mid,ans=0;
		while(l<=r){
			mid=(l+r)>>1;
			if(calc(mid)) ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d",ans);
		return 0;
	}
	if(m==1){
		s=bfs(1);
		t=bfs(s);
		printf("%d",d[t]);
		return 0;
	}
	sort(b+1,b+n);
	memset(vis,0,sizeof(vis));
	for(i=1;i<=m;i++){
		vis[b[i].num]=vis[b[i].num^1]=1;
		hp.push(b[i]);
	}
	tot=m;
	while(tot<(n-1)&&!hp.empty()){
		p=hp.top(),hp.pop();
		int mx=-1,tx,my=-1,ty;
		x=p.u;
		for(i=h[x];i;i=e[i].nxt){
			if(vis[i]) continue;
			if(e[i].dis>mx) mx=e[i].dis,tx=i;
		}
		y=p.v;
		for(i=h[y];i;i=e[i].nxt){
			if(vis[i]) continue;
			if(e[i].dis>my) my=e[i].dis,ty=i;
		}
		if(mx==-1&&my==-1){
			hp.push(p);
			break;
		}
		tot++;
		if(mx>my) p.u=e[tx].to,p.w+=mx,vis[tx]=vis[tx^1]=1;
		else p.v=e[ty].to,p.w+=my,vis[ty]=vis[ty^1]=1;
		hp.push(p);
	}
	p=hp.top();
	printf("%d",p.w);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*
7 1
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7

6 2
1 2 4
2 3 6
3 4 2
4 5 3
5 6 6

7 2
1 2 1
3 7 1
2 4 1
2 5 1
1 3 1
3 6 1

*/
