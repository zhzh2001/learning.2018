#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <string>
#include <iostream>
using namespace std;
int dp[26000],a[200],T,n;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		memset(dp,0,sizeof(dp));
		dp[0]=1;
		for (int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		int ans=n;
		for (int i=1;i<=n;i++){
			if (dp[a[i]]){
				ans--;
				continue;
			}
			for (int j=a[i];j<=a[n];j++)
				if (dp[j-a[i]]) dp[j]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}

