#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int c[N],q1[N*2],head1[N*2],to1[N*2],cnt1,f[N],opt,t,w,mid,b[N],Min,a[N],ans,cnt,n,m,x,y,z,q[N*2],to[N*2],val[N*2],head[N],dp[N*2][2];
bool l,f1;
void add(int x,int y,int z)
{
	q[++cnt]=head[x];to[cnt]=y;val[cnt]=z;head[x]=cnt;
}
bool check(int mid)
{
	int sum=0,u=0;
	for (int i=1;i<=n-1;i++)
	{
	  sum=sum+b[i];
	  if (sum>=mid)
	  {
	  	u++;
	  	sum=0;
	  }
    }
    if (u>=m) return true;
    return false;
}
void dfs(int t,int s)
{
	int opt=0;
	for (int i=head[t];i!=0;i=q[i])
	  if (to[i]!=s)
	  {
	  	opt++;
	  	dfs(to[i],t);
	  	if (opt==1)
	  	  dp[t][1]=dp[to[i]][1]+val[i];
	  	else
	  	{
	  		if (dp[to[i]][1]+val[i]>dp[t][1])
	  		{
	  			dp[t][2]=dp[t][1];
	  			dp[t][1]=dp[to[i]][1]+val[i];
			}
			else
			  if (dp[to[i]][1]+val[i]>dp[t][2])
			    dp[t][2]=dp[to[i]][1]+val[i];
		}
	  }
	ans=max(ans,dp[t][1]+dp[t][2]);
	if (opt==0) 
	  dp[t][1]=0,dp[t][2]=0;
}
void add1(int x,int y)
{
	q1[++cnt1]=head1[x];to1[cnt1]=y;head1[x]=cnt1;
}
void work(int t,int s,int midk)
{	
	if (opt>=m) return;
	int u=0;
	for (int i=head[t];i!=0;i=q[i])
	  if (to[i]!=s)
	  {
	  	work(to[i],t,midk);
	  	f[to[i]]=f[to[i]]+val[i];
	  	add1(t,f[to[i]]);
	  }
	for (int i=head1[t];i!=0;i=q1[i])
	{
		u++;
		c[u]=to1[i];
	}	
	sort(c+1,c+u+1);
	int tk,wk,p=0;
	tk=1;wk=u;
	while (tk<=wk)
	{
		if (c[wk]>=midk)
		{
			wk--;
			opt++;
		}
		else
		if (c[tk]+c[wk]>=midk && (tk!=wk))
		{
			tk++;
			wk--;
			opt++;
		}
		else
			p=max(p,c[tk]),tk++;
	}
	f[t]=p;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	l=false;
	f1=false;
	Min=2000000000;
	for (int i=1;i<n;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z);
		add(y,x,z);
		if (y!=x+1)
		  l=true;
		b[x]=z;
		a[i]=z;
		if (x!=1)
		  f1=true;
	}
	ans=0;
	if (m==1)
	{
		dfs(1,0);
		printf("%d\n",ans);
	}
	else
	if (f1==false && (m==n-1))
	{		
		sort(a+1,a+n);
		for (int i=1;i<=m-1;i++)
		    Min=min(Min,a[n-i]);
		printf("%d\n",min(Min,a[n-m]+a[n-m-1]));
	}
	else
	if (l==false)
	{
		t=0;w=500000000;
		while (t<=w)
		{
			mid=(t+w)/2;
			if (check(mid))
			{
				ans=mid;
				t=mid+1;
			}
			else w=mid-1;
		}
		printf("%d\n",ans);
	}
	else
	{
		t=0;w=500000000;
		while (t<=w)
		{
			cnt1=0;
			memset(head1,0,sizeof(head1));
			memset(f,0,sizeof(f));
			mid=(t+w)/2;
			opt=0;
			work(1,0,mid);
			if (opt>=m)
			{
				ans=mid;
				t=mid+1;
			}
			else w=mid-1;
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
