#include<bits/stdc++.h>
using namespace std;
int T,n,a[105],ans;
bool f1,f[25005];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		f1=false;
		ans=0;
		memset(f,false,sizeof(f));
		for (int i=1;i<=n;i++)
		{
			scanf("%d",&a[i]);
			if (a[i]==1)
			  f1=true;
		}
		if (f1){printf("1\n");continue;}
		sort(a+1,a+n+1);
		f[0]=true;
		for (int i=1;i<=n;i++)
		  if (f[a[i]]==false)
		  {
		    for (int j=a[i];j<=a[n];j++)
		      f[j]=f[j] | f[j-a[i]];
		    ans++;
		  }
	    printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
