#include <cstdio>
#include <cstring>
#include <algorithm>

#define mp make_pair
#define pb push_back
#define ri register int
#define px first
#define py second

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int INF = 0x3f3f3f3f;

const int MAXN = 1e5 + 5;

inline int read() {
	int ch = getchar(), x = 0;
	while (ch > '9' || ch < '0') ch = getchar();
	while ('0' <= ch && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x;
}

int a[MAXN];

int N;

int main() {

	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	
	scanf("%d", &N);

	ll ans = 0;

	for (int i = 1; i <= N; ++i) a[i] = read();
		
	for (int i = 2; i <= N + 1; ++i) 
		if (a[i - 1] > a[i]) ans += a[i - 1] - a[i];
	
	printf("%lld\n", ans);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
