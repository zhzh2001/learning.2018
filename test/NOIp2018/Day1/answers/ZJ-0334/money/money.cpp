#include <cstdio>
#include <cstring>
#include <algorithm>

#define mp make_pair
#define pb push_back
#define ri register int
#define px first
#define py second

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int INF = 0x3f3f3f3f;

const int MAXN = 105;

int N;

int flag = 0, ans;

int a[MAXN];

int dp[MAXN][25001];


inline void solve() {
	memset(dp, 0, sizeof(dp));
	for (int u = 0; u <= N; ++u) dp[u][0] = 1;
	for (int u = 1; u <= N; ++u) 
		for (int s = 1; s <= a[N]; ++s) 
			for (int k = 0; k <= s / a[u]; ++k) 
				if (dp[u - 1][s - k * a[u]] == 1) dp[u][s] = 1;
				
	for (int i = 2; i <= N; ++i) 
		if (dp[i - 1][a[i]] == 1) --ans;
	
}

int main() {

	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	
	int T;
	scanf("%d", &T);
	while (T--) {
		scanf("%d", &N);
		for (int i = 1; i <= N; ++i) scanf("%d", &a[i]);
		sort(a + 1, a + 1 + N);
		ans = N;
		solve();
		printf("%d\n", ans);
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
