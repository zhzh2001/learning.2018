#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

#define mp make_pair
#define pb push_back
#define ri register int
#define px first
#define py second

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int INF = 0x3f3f3f3f;

const int MAXN = 5e4 + 5;

int N, M;

vector<pii> edge[MAXN];

int e[MAXN];

namespace sub1 { // m = 1;
	int mx = 0;
	inline void dfs(int u, int s, int f) {
		if (s > mx) mx = s;
		for (int i = 0; i < edge[u].size(); ++i) {
			int v = edge[u][i].px;
			if (v != f) {
				dfs(v, s + edge[u][i].py, u);
			}
		}
	}
	
	void solve() {
		for (int i = 1; i <= N; ++i) {
			dfs(i, 0, -1);
			if ((i * N) > 5e7) break;
		}
		printf("%d\n", mx);
	}
}

namespace sub2 {
	int s[MAXN];
	
	inline int check(int aim) {
		int l = 1, r = 1, p = 0, flag = 1;
		while (p < M) {
			while (s[r] - s[l - 1] < aim && r < N) ++r;
			if (r >= N && p < M) {
				flag = 0;
				break;
			}
			++p;
			++r;
			l = r;
		}		
		return flag;
	}
	
	void solve() {
		int ans = 0;
		s[0] = 0;
		for (int i = 1; i < N; ++i) s[i] = s[i - 1] + e[i];
		int l = 1, r = s[N - 1];
		while (l <= r) {
			int mid = (l + r) >> 1;
			if (check(mid)) {
				if (mid > ans) ans = mid;
				l = mid + 1;
			} else r = mid - 1;
		}
		printf("%d\n", ans);
	}
}


int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);

	scanf("%d %d", &N, &M);
	
	int issub2 = 1;
	int issub3 = 1;
	
	for (int i = 1; i < N; ++i) {
		int u, v, w;
		scanf("%d %d %d", &u, &v, &w);
		edge[u].pb(mp(v, w));
		edge[v].pb(mp(u, w));		
		e[i] = w;
		if (v != (u + 1) ) issub2 = 0;
		if (u != 1) issub3 = 0 ;
	}
	
	if (issub2) {
		sub2::solve();
		return 0;
	}
	if (M == 1) {
		sub1::solve();
		return 0;
	}
	return 0;
}
