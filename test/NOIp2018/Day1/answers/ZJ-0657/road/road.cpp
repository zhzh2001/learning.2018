#include<bits/stdc++.h>
#define M 100005
#define LL long long
using namespace std;
bool mm1;
int n,A[M];
struct P100{
	#define fa tree[p]
	#define lson tree[p<<1]
	#define rson tree[p<<1|1]
	struct YD_tree{
		struct YD{
			int l,r,mi;
		}tree[M<<2];
		int MI(int a,int b){
			if(A[a]<A[b])return a;
			return b;	
		}
		void up(int p){
			if(A[lson.mi]<A[rson.mi])fa.mi=lson.mi;
			else fa.mi=rson.mi;	
		}
		void build(int l,int r,int p){
			fa.l=l;fa.r=r;fa.mi=0;
			if(l==r){fa.mi=l;return;}
			int mid=(l+r)>>1;
			build(l,mid,p<<1);
			build(mid+1,r,p<<1|1);
			up(p);	
		}
		int query(int l,int r,int p){
			if(fa.l==l&&fa.r==r)return fa.mi;
			int mid=(fa.l+fa.r)>>1;
			if(r<=mid)return query(l,r,p<<1);
			else if(l>mid)return query(l,r,p<<1|1);
			return MI(query(l,mid,p<<1),query(mid+1,r,p<<1|1));
		}
	}T;
	LL calc(int l,int r,LL add){
		if(l>r)return 0;
		if(l==r)return A[l]-add;
		int v=T.query(l,r,1);
		return calc(l,v-1,A[v])+A[v]-add+calc(v+1,r,A[v]);
	}
	void solve(){
		T.build(1,n,1);	
		printf("%lld\n",calc(1,n,0));
	}
}p100;
bool mm2;
int main(){//file LL data mod memory
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
//	printf("%lf",(&mm2-&mm1)/1024.0/1024);
	cin>>n;
	for(int i=1;i<=n;i++)scanf("%d",&A[i]);
	p100.solve();
	return 0;	
}
