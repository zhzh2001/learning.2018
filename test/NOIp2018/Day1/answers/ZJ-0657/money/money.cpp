#include<bits/stdc++.h>
#define M 105
using namespace std;
bool mm1;
int n,A[M],T;
int Q[M],qcnt=0;
bool dp[M][25005];
bool mm2;
int main(){//file LL data mod memory
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
//	printf("%lf\n",(&mm2-&mm1)/1024.0/1024);
	scanf("%d",&T);
	while(T--){
		memset(dp,0,sizeof(dp));
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",&A[i]);
		sort(A+1,A+n+1);
		qcnt=0;Q[++qcnt]=A[1];
		dp[1][0]=1;
		for(int j=1;j*A[1]<=25000;j++)dp[1][j*A[1]]=1;
		for(int i=2;i<=n;i++){
			if(!dp[qcnt][A[i]]){
				Q[++qcnt]=A[i];
				for(int j=0;j<=25000;j++){
					dp[qcnt][j]|=dp[qcnt-1][j];
					if(dp[qcnt][j]==1){
						if(j+A[i]<=25000)dp[qcnt][j+A[i]]=1;
					}
				}
			}
		}
		printf("%d\n",qcnt);
	}
	return 0;	
}
