#include<bits/stdc++.h>
#define M 50005
#define LL long long
using namespace std;
bool mm1;
int n,m;
int h[M],tot,in[M],rt;
struct edge{
	int nxt,to,co;	
}G[M<<1];
void Add(int a,int b,int c){
	G[++tot]=(edge){h[a],b,c};
	h[a]=tot;	
}
struct P0{//直径 
	int mxdis,ed;
	void dfs(int x,int f,int w){
		if(w>mxdis)mxdis=w,ed=x;
		for(int i=h[x];i;i=G[i].nxt){
			int u=G[i].to,v=G[i].co;
			if(u==f)continue;
			dfs(u,x,w+v);
		}
	}
	void solve(){
		mxdis=-1;dfs(1,0,0);
		mxdis=-1;dfs(ed,0,0);
		printf("%d",mxdis);
	}
}p0;
struct P1{//菊花图 
	int Q[M],qcnt;
	bool check(LL x){
		int ct=0,r=qcnt;
		while(Q[r]>=x)ct++,r--;
		int l=1;
		while(l<r){
			if(Q[l]+Q[r]>=x)ct++;
			l++;r--;
		}
		return (ct>=m);
	}
	void solve(){
		qcnt=0;
		for(int i=h[1];i;i=G[i].nxt){
			Q[++qcnt]=G[i].co;	
		}
		sort(Q+1,Q+qcnt+1);
		LL l=1,r=5e8,ans=-1;
		while(l<=r){
			LL mid=(l+r)>>1;
			if(check(mid)){
				ans=mid;
				l=mid+1;	
			}
			else r=mid-1;
		}
		printf("%lld\n",ans);
	}	
}p1;
struct P2{//链
	int sum[M],scnt;
	void dfs(int x,int f,int w){
		sum[++scnt]=w;
		for(int i=h[x];i;i=G[i].nxt){
			int u=G[i].to,v=G[i].co;
			if(u==f)continue;
			dfs(u,x,w+v);
		}
	}
	bool check(LL x){
		int las=0;
		int cnt=0;
		for(int i=1;i<=n;i++){
			if(sum[i]-sum[las]>=x){
				cnt++;
				las=i;
			}	
		}
		return (cnt>=m);
	}
	void solve(){
		dfs(rt,0,0);
		LL l=1,r=5e8,ans=-1;
		while(l<=r){
			LL mid=(l+r)>>1;
			if(check(mid)){
				ans=mid;
				l=mid+1;	
			}
			else r=mid-1;
		}
		printf("%lld\n",ans);
	}
}p2;
struct P3{//二叉树并且n比较小（就算写n^3也有不错的成绩） 
	int dp[M];//每棵子树下最多能有多少条>=dis的路径 
	int dis[M],L[M],R[M],tt,ln[M],qcnt[2]; 
	int son[M][2]; 
	struct node{
		int ds,add;	
	}Q[2][M];
	void dfs(int x,int f,int ds){
		L[x]=++tt;ln[tt]=x;dis[x]=ds;son[x][0]=son[x][1]=0;
		int c=0;
		for(int i=h[x];i;i=G[i].nxt){
			int u=G[i].to,v=G[i].co;
			if(u==f)continue;
			son[x][c++]=u;
			dfs(u,x,ds+v);	
		}
		R[x]=tt;
	}
	void dfs_calc(int x,int add,int o,bool f){
		Q[f][++qcnt[f]]=(node){dis[x],add+dp[x]};
		for(int i=0;i<2;i++){
			int toadd=add;
			if(i==0)toadd+=dp[son[x][1]];
			else toadd+=dp[son[x][0]];
			if(son[x][i])dfs_calc(son[x][i],toadd,o,f);
		}
	}
	void dfs_solve(int x,int f,int lim){
		for(int i=0;i<2;i++){
			if(son[x][i]!=0)
				dfs_solve(son[x][i],x,lim);	
		}
		if(son[x][0]==0){dp[x]=0;return;}
		if(son[x][1]==0){
			qcnt[0]=0;
			dfs_calc(son[x][0],0,x,0);
			int mx=0;
			for(int i=1;i<=qcnt[0];i++){
				if(Q[0][i].ds-dis[x]>=lim){
					mx=max(mx,Q[0][i].add+1);
				}
			}
			dp[x]=mx;
		}
		else {
			qcnt[0]=qcnt[1]=0;
			dfs_calc(son[x][0],0,x,0);
			dfs_calc(son[x][1],0,x,1);
			int mx=0;
			for(int i=1;i<=qcnt[0];i++){
				if(Q[0][i].ds-dis[x]>=lim){
					mx=max(mx,Q[0][i].add+1+dp[son[x][1]]);
				}
			}
			for(int i=1;i<=qcnt[1];i++){
				if(Q[1][i].ds-dis[x]>=lim){
					mx=max(mx,Q[1][i].add+1+dp[son[x][0]]);
				}
			}
			for(int i=1;i<=qcnt[0];i++){
				for(int j=1;j<=qcnt[1];j++){
					if(Q[0][i].ds+Q[1][i].ds-2*dis[x]>=lim){
						mx=max(mx,Q[0][i].add+Q[1][i].add+1);	
					}
				}
			}
			dp[x]=mx;
		}
	}
	bool check(LL x){
		memset(dp,0,sizeof(dp));
		dfs_solve(rt,0,x);
		return (dp[rt]>=m);
	}
	void solve(){
		dfs(rt,0,0);
		LL l=1,r=5e8,ans=-1;
		while(l<=r){
			LL mid=(l+r)>>1;
			if(check(mid)){
				ans=mid;
				l=mid+1;	
			}
			else r=mid-1;
		}
		printf("%lld\n",ans);
	}
}p3; 
bool mm2;
int main(){//file LL data mod memory
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
//	printf("%lf",(&mm2-&mm1)/1024.0/1024);
	cin>>n>>m;
	bool fl1=1;
	for(int i=1,a,b,c;i<n;i++){
		scanf("%d%d%d",&a,&b,&c);
		if(a!=1)fl1=0;
		Add(a,b,c);Add(b,a,c);
		in[a]++;in[b]++;	
	}
	bool fl=1,fl2=1;
	int cc=0;
	for(int i=n;i>=1;i--){
		if(in[i]>3)fl2=0;
		if(in[i]==1)rt=i,cc++;
		if(in[i]!=1&&in[i]!=2)fl=0;
	}
	if(cc!=2)fl=0; 
	if(!fl&&fl2){
		for(int i=1;i<=n;i++)if(in[i]==1){rt=i;break;}
	}
	if(m==1)p0.solve();
	else if(fl)p2.solve(); 
	else if(fl2)p3.solve();
	else if(fl1)p1.solve();
//	if(fl)p2.solve();
	return 0;	
}
