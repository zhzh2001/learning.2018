#include <bits/stdc++.h>
using namespace std;

#define FO(i,a,b) for (int i=a;i<=b;++i)
#define FD(i,a,b) for (int i=a;i>=b;--i)
inline int read(){ static int x; scanf("%d",&x); return x; }

int T,n,ans,f[25005],mx,a[105];

int main(void){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while (T--){
		n=read();ans=0;
		f[0]=1;mx=0;
		FO(i,1,n){
			a[i]=read();
			mx=max(a[i],mx);
		}
		FO(i,1,mx) f[i]=0;
		FO(i,1,n){
			FO(j,0,mx-a[i]){
				if (!f[j]) continue;
				if (f[j+a[i]]) f[j+a[i]]=2;
				else f[j+a[i]]=1;
			}
		}
		FO(i,1,n) if (f[a[i]]==1) ans++;
		printf("%d\n",ans);
	}
	return 0;
}

