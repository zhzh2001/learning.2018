#include <bits/stdc++.h>
using namespace std;

inline int read(){ static int x; scanf("%d",&x); return x; }

#define oo 0x3f3f3f3f
#define N 400005
#define FO(i,a,b) for (int i=a;i<=b;++i)
#define FD(i,a,b) for (int i=a;i>=b;--i)
#define lowbit(x) (x&(-x))

int n,a[N],id[N],f[N],det[N],ans;
inline bool cmp(int x,int y){ return a[x]<a[y]; }
inline void mod(int f[N],int x,int det){ while (x<=n) f[x]+=det,x+=lowbit(x); }
inline int que(int f[N],int x){ int _=0;while (x) _+=f[x],x-=lowbit(x); return _; }

int main(void){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	FO(i,1,n) a[i]=read(),id[i]=i;
	sort(id+1,id+1+n,cmp);
//	FO(i,1,n){
//		int l,r;l=r=id[i];
//		while (a[l]>0&&l>1) l--;if (a[l]<=0) l++;
//		while (a[r]>0&&r<n) r++;if (a[r]<=0) r--;
//		if (a[id[i]]==0) continue;
//		ans+=a[id[i]];int xx=a[id[i]];
//		FO(j,l,r) a[j]-=xx;
//	}
	FO(i,1,n){
		int p=id[i];
		int tm=que(det,p)+a[p];
		if (tm==0){ mod(f,p,1);continue; }
		int stl,str;stl=str=p;
		FD(j,17,0){
			if (stl-(1<<j)<1) continue;
			if (que(f,p)-que(f,stl-(1<<j)-1)==0) stl-=(1<<j);
		}
		FD(j,17,0){
			if (str+(1<<j)>n) continue;
			if (que(f,str+(1<<j))-que(f,p-1)==0) str+=(1<<j);
		}
		mod(det,stl,-tm);mod(det,str+1,tm);mod(f,p,1);ans+=tm;
	}
	printf("%d\n",ans);
	return 0;
}

