#include <bits/stdc++.h>
using namespace std;

inline int read(){ static int x; scanf("%d",&x); return x; }

#define oo 0x3f3f3f3f
#define N 200005
#define FO(i,a,b) for (int i=a;i<=b;++i)
#define FD(i,a,b) for (int i=a;i>=b;--i)

int n,m,head[N],nxt[N],fall[N],w[N],x[N],y[N],z[N],cnt;

void ae(int x,int y,int z){ ++cnt;nxt[cnt]=head[x];fall[cnt]=y;w[cnt]=z;head[x]=cnt; }

namespace dm{
	queue<int> Q;
	int mxx,mx,f[N];
	void bfs(){
		while (!Q.empty()){
			int x=Q.front();Q.pop();
			for (int i=head[x];i;i=nxt[i]){
				int to=fall[i],wei=w[i];
				if (!f[to]){
					f[to]=f[x]+wei;Q.push(to);
					if (f[to]>mx) mx=f[to],mxx=to;
				}
			}
		}
	}
	void solve(){
		Q.push(1);mxx=-1;
		bfs();memset(f,0,sizeof(f));
		while (!Q.empty()) Q.pop();
		Q.push(mxx);mx=0;mxx=-1;bfs();
		printf("%d\n",mx);
	}
}

namespace juhua{
	int nn,used[N];
	bool ck(int x){
		int ans=0,l=1;
		FO(i,1,nn) used[i]=0;
		FD(i,nn,1){
			if (used[i]) continue;
			if (z[i]>=x) { ans++;continue; } 
			while (l<i&&(z[l]+z[i]<x||used[l])) ++l;
			if (l>=i) break;ans++;used[l]=1;
		}
		return ans>=m;
	}
	void solve(){
		nn=n-1;
		sort(z+1,z+1+nn);
		int l,r;l=z[1];r=z[nn]*2;
		while (l<r){
			int mid=(l+r)>>1;
			if (ck(mid)) l=mid+1;
			else r=mid;
		}
		while (!ck(l)) l--;
		while (ck(l+1)) l++;
		printf("%d\n",l);
	}
}

namespace lst{
	int nn;
	bool ck(int x){
		int cnt=0,ans=0;
		FO(i,1,nn){
			cnt+=z[i];
			if (cnt>=x) ans++,cnt=0;
		}
		return ans>=m;
	}
	void solve(){
		nn=n-1;
		int l,r;l=0;FO(i,1,nn) r+=z[i];
		while (l<r){
			int mid=(l+r)>>1;
			if (ck(mid)) l=mid+1;
			else r=mid;
		}
		while (!ck(l)) l--;
		while (ck(l+1)) l++;
		printf("%d\n",l);
	}
}
namespace ec{
	int f[205][205],GLB,dao[205];
	void dfs(int x,int fa){
		f[x][0]=dao[x];
		FO(i,1,m+1) f[x][i]=-oo;
		if (dao[x]>=GLB) f[x][1]=0;
		int ec1,ec2=ec1=ec2=-1;
		for (int i=head[x];i;i=nxt[i]){
			int to=fall[i];
			if (to==fa) continue;
			dao[to]=w[i];dfs(to,x);
			if (ec1==-1) ec1=to;
			else ec2=to;
		}
		if (ec1!=-1&&ec2!=-1){
			FO(i,0,m){
				if (f[ec1][i]<0) continue;
				FO(j,0,m){
					if (f[ec2][j]<0) continue;
					if (f[ec1][i]+f[ec2][j]>=GLB) f[x][i+j+1]=max(f[x][i+j+1],dao[x]);
					f[x][i+j]=max(f[x][i+j],dao[x]+max(f[ec1][i],f[ec2][j]));
				}
			}
		}else if (ec1!=-1){
			FO(i,0,m){
				if (f[ec1][i]+dao[x]>=GLB) f[x][i+1]=max(f[x][i+1],0);
				f[x][i]=max(f[x][i],f[ec1][i]+dao[x]);
			}
		}
	}
	bool ck(int x){
		GLB=x;dfs(1,0);
		if (f[1][m]>=0||f[1][m+1]>=0) return 1;
		return 0;
	}
	void solve(){
		int l,r;l=0;FO(i,1,n-1) r+=z[i];
		while (l<r){
			int mid=(l+r)>>1;
			if (ck(mid)) l=mid+1;
			else r=mid;
		}
		while (!ck(l)) l--;
		while (ck(l+1)) l++;
		printf("%d\n",l);
	}
}

int main(void){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();int flag1,flag2;flag1=1;flag2=1;
	FO(i,1,n-1){
		x[i]=read();y[i]=read();z[i]=read();
		flag1=flag1&&y[i]==x[i]+1;flag2=flag2&&x[i]==1;
		ae(x[i],y[i],z[i]);ae(y[i],x[i],z[i]);
	}
	if (m==1) { dm::solve(); return 0; }
	if (flag1) { lst::solve(); return 0; }
	if (flag2) { juhua::solve(); return 0; }
	ec::solve();
	return 0;
}

