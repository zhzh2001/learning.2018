#include<iostream>
#include<cstdio>
#include<algorithm>
#include<vector>
#include<queue>
#include<cstring>
using namespace std;
vector<int> to[50001];
vector<int> num[50001];
int len,maxx=-0x7fffffff,n,m,a,b,c,r[50001];
bool vis[50001];
void dfs(int x,int t){
	vis[x]=1;
	if(maxx<t){
		maxx=t;
	}
	int len=to[x].size();
	for(int i=0;i<len;i++){
		if(!vis[to[x][i]]) dfs(to[x][i],t+num[x][i]);
	}
	return;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&a,&b,&c);
		to[a].push_back(b);
		to[b].push_back(a);
		num[a].push_back(c);
		num[b].push_back(c);
		r[a]++;
		r[b]++;
	}
	if(m==1){
		for(int i=1;i<=n;i++){
			if(r[i]==1){
				dfs(i,0);
				printf("%d\n",maxx);
				fclose(stdin);
				fclose(stdout);
				return 0;
			}
		}
	}
	return 0;
}
