#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,a[101],maxx=-0x7fffffff,ans;
bool f[60001],p[60001],b[60001];
inline int read(){
	int a=0;
	char c=getchar();
	for(;c<'0'||c>'9';) c=getchar();
	for(;c>='0'&&c<='9';) a=(a<<1)+(a<<3)+int(c)-48,c=getchar();
	return a;
}
int main(){
	int t;
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read();
	for(int l=1;l<=t;l++){
		n=read();
		for(int i=1;i<=n;i++) a[i]=read(),maxx=max(maxx,a[i]);
		sort(a+1,a+n+1);
		for(int i=1;i<=n;i++) b[a[i]]=p[a[i]]=1;
		memset(f,0,sizeof(f));
		f[0]=1;
		for(int i=1;i<=n;i++){
			for(int j=0;j<=2*maxx;j++){
				for(int k=1;j+a[i]*k<=2*maxx;k++){
			  		f[j+a[i]*k]=(f[j+a[i]*k]|f[j]);
			  		if(f[j+a[i]*k]&&(k!=1||j!=0)) p[j+a[i]*k]=0;
			  	}
			}
		}
		ans=0;
		for(int i=1;i<=n;i++) ans+=p[a[i]];
		printf("%d\n",ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
