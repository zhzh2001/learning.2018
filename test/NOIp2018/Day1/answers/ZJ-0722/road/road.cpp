#include<iostream>
#include<cstdio>
using namespace std;
int n,ans,a,lat;
inline int read(){
	int a=0;
	char c=getchar();
	for(;c<'0'||c>'9';) c=getchar();
	for(;c>='0'&&c<='9';) a=(a<<1)+(a<<3)+int(c)-48,c=getchar();
	return a;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	ans=read();
	lat=ans;
	for(int i=2;i<=n;i++){
		a=read();
		if(a>lat) ans+=(a-lat);
		lat=a;
	}
	printf("%d\n",ans);
	fclose(stdin); fclose(stdout);
	return 0;
}
