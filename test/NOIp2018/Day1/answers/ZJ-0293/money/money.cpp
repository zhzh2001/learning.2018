#include<bits/stdc++.h>
using namespace std;
const int N=25050;
bool f[N];
int a[105],n,ans;


void solve()
{
	ans=0;
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	sort(a+1,a+1+n);
	f[0]=true;
	for (int i=1;i<=a[n];i++) f[i]=false;
	for (int i=1;i<=n;i++)
	{
		if (!f[a[i]])
		{
			ans++;
			for (int j=0;j<=a[n]-a[i];j++)
				if (f[j]) f[j+a[i]]=true;
		}
	}
	printf("%d\n",ans);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--) solve();
	return 0;
}
