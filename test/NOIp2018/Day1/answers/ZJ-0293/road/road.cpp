#include<bits/stdc++.h>
using namespace std;
const int N=100050;
int a[N],stk[N],n,ans,top;

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	ans=a[1];
	stk[++top]=a[1];
	for (int i=2;i<=n;i++)
	{
		if (a[i]<stk[top])
		{
			stk[++top]=a[i];
			continue;
		}
		ans=ans+a[i]-stk[top];
		while (top>0 && stk[top]<=a[i])
		{
			top--;
		}
		stk[++top]=a[i];
	}
	printf("%d\n",ans);
	return 0;
}
