#include<bits/stdc++.h>
using namespace std;
const int N=50050;
multiset<int>S;
int Head[N],f[N],g[N],n,m,tot,X;

struct Edge{
	int v,next,w;
}edge[N*2];
void addedge(int x,int y,int z)
{
	edge[++tot]=(Edge){y,Head[x],z};
	Head[x]=tot;
}
void dfs(int u,int fa)
{
	for (int i=Head[u];i;i=edge[i].next)
	{
		int v=edge[i].v;
		if (v==fa) continue;
		dfs(v,u);
		g[u]+=g[v];
	}
	for (int i=Head[u];i;i=edge[i].next)
	{
		int v=edge[i].v,w=edge[i].w;
		if (v==fa) continue;
		if (f[v]+w>=X) g[u]++;
		else S.insert(f[v]+w);
	}
	while (S.begin()!=S.end())
	{
		int x=*S.begin();
		S.erase(S.begin());
		multiset<int>::iterator it=S.lower_bound(X-x);
		if (it==S.end())
		{
			f[u]=max(f[u],x);
			continue;
		}
		g[u]++;
		S.erase(it);
	}
}
bool check(int mid)
{
	X=mid;
	for (int i=1;i<=n;i++) f[i]=g[i]=0;
	dfs(1,0);
	return g[1]>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int L=10005,R=0;
	for (int i=1;i<n;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		L=min(L,z); R=R+z;
		addedge(x,y,z); addedge(y,x,z);
	}
	while (L<R)
	{
		int mid=(L+R+1)>>1;
		if (check(mid)) L=mid;
		else R=mid-1;
	}
	printf("%d\n",L);	
	return 0;
}
