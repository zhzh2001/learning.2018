#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<queue>
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;else f=1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=100007;

struct Line{
	int u,v,w,next;
}e[N<<1];
int cnt,head[N];

void Ins(int u,int v,int w){
	e[++cnt]=(Line){u,v,w,head[u]};
	head[u]=cnt;
}
void Insert(int u,int v,int w){
	Ins(u,v,w);
	Ins(v,u,w);
}

int f[N][2];
int n,m;
int a[N];

void Dfs(int x,int last){
	for (int i=head[x];i;i=e[i].next){
		int y=e[i].v;
		if (y==last) continue;
		Dfs(y,x);
		f[x][0]=max(f[x][0],max(f[y][0],f[x][1]+f[y][1]+e[i].w));
		f[x][1]=max(f[x][1],f[y][1]+e[i].w);
	}
//	printf("The root is %d =  %d %d\n",x,f[x][0],f[x][1]);
}

int b[N];

int main(){

	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	n=read(),m=read();
	int flag1=1,flag2=1;
	For(i,1,n-1){
		int u=read(),v=read(),w=read();
		Insert(u,v,w);
		if (u!=1) flag1=0;
		if (v!=u+1) flag2=0;
	}	
	if (m==1){
		Dfs(1,0);
		printf("%d\n",max(f[1][0],f[1][1]));
		return 0;
	}
	if (flag1){
		int r=0;
		for (int i=1;i<=cnt;i+=2) a[++r]=e[i].w;
		sort(a+1,a+1+r);
		int l=1;
		while (m*2<(r-l+1)) l++;
		int ans=1<<30;
		while (m*2>(r-l+1)){
			ans=min(ans,a[r]);
			m--,r--;
		}
		For(i,1,m) ans=min(ans,a[l+i-1]+a[r-i+1]);
		printf("%d\n",ans);
		return 0;
	}
	if (flag2){
		n--;
		For(x,1,n) for (int i=head[x];i;i=e[i].next) if (e[i].v==x+1) {
			b[x]=e[i].w;
		}
		while (n>m){
			int res=1<<30,x=0;
			For(i,1,n) if (b[i]<res) res=b[i],x=i;
			if (x==1){
				b[1]+=b[2];
				For(i,2,n-1) b[i]=b[i+1];
				n--;
			}
			else if (x==n){
				n--;
				b[n]+=b[n+1];
			}
			else {
				if (b[x-1]<b[x+1]){
					b[x-1]+=b[x];
					For(i,x,n-1) b[i]=b[i+1];
					n--;
				}
				else {
					b[x]+=b[x+1];
					For(i,x+1,n-1) b[i]=b[i+1];
					n--;
				}
			}
		}
		int res=1<<30;
		For(i,1,n) res=min(res,b[i]);
		printf("%d\n",res);
		return 0;
	}
}

