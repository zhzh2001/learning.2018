#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;else f=1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=100007;

struct Seg_Tree{
	int num,id;
}Tr[N<<2];
int a[N];
int flag,tm,ans,n;


#define ls (pos<<1)
#define rs (pos<<1|1)

const int INF=1<<30;

void Build(int pos,int l,int r){
	if (l==r){
		Tr[pos].num=a[l];
		Tr[pos].id=l;
		return;
	}
	int mid=(l+r)>>1;
	Build(ls,l,mid); Build(rs,mid+1,r);
	if (Tr[ls].num<Tr[rs].num) Tr[pos].id=Tr[ls].id;
	else Tr[pos].id=Tr[rs].id;
	Tr[pos].num=min(Tr[ls].num,Tr[rs].num);
}

int Query(int pos,int l,int r,int x,int y){
	if (l==x&&r==y) {
		if (Tr[pos].num<tm) tm=Tr[pos].num,flag=Tr[pos].id;
		return Tr[pos].num;
	}
	int mid=(l+r)>>1;
	if (y<=mid) return Query(ls,l,mid,x,y);
	else if (x>mid) return Query(rs,mid+1,r,x,y);
	else return min(Query(ls,l,mid,x,mid),Query(rs,mid+1,r,mid+1,y));
}

void Solve(int l,int r,int x){
	tm=INF,flag=0;
	int k=Query(1,1,n,l,r);
	ans+=k-x;
	if (l<flag) Solve(l,flag-1,k);
	if (r>flag) Solve(flag+1,r,k);
}

int main(){
	
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	n=read();
	For(i,1,n) a[i]=read();
	Build(1,1,n);
	ans=0;
	Solve(1,n,0);
	printf("%d",ans);
	
	
	return 0;
}
