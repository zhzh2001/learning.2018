#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;else f=1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

int T,n;
const int N=10007;
int a[N],b[N];
int ans;

int main(){

	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	T=read();
	For(p,1,T){
		n=read();
		For(i,1,n) a[i]=read();
		sort(a+1,a+1+n);
		memset(b,0,sizeof(b));
		ans=0;
		b[0]=1;
		For(i,1,n){
			int x=a[i];
			if (b[x]) continue;
			ans++;
			For(j,0,a[n]-x) if (b[j]==1) b[j+x]=b[j];
	//		For(j,1,a[n]) printf("%d ",b[j]);
	//		printf("\n");
		}
		printf("%d\n",ans);
	}
	return 0;

}

