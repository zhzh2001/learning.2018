#include <cstdio>
#include <cstring>
#include <algorithm>

const int N=5e5+5,M=1e6+5;
int n,m,tot,lnk[N],nxt[M],ter[M],val[M];

void add(int u,int v,int w) {
	ter[++tot]=v;
	nxt[tot]=lnk[u];
	val[tot]=w;
	lnk[u]=tot;
}
namespace subtask1 {
	long long dis[N];
	void dfs(int u,int fa) {
		for(int i=lnk[u];i;i=nxt[i]) {
			int v=ter[i];
			if(v==fa) continue;
			dis[v]=dis[u]+val[i];
			dfs(v,u);
		}
	}
	void main() {
		memset(dis,0,sizeof(dis));
		dfs(1,0);
		int pos=0;
		for(int i=1;i<=n;++i) if(dis[i]>dis[pos]) pos=i;
		memset(dis,0,sizeof(dis));
		dfs(pos,0);
		pos=0;
		for(int i=1;i<=n;++i) if(dis[i]>dis[pos]) pos=i;
		printf("%lld\n",dis[pos]);
	}
}
namespace subtask2 {
	bool check(long long x) {
		int cnt=0,st=n;
		while(st>=1&&val[st]>=x) --st,++cnt;
		if(cnt>=m) return 1;
		int l=1;
		for(int r=st;r>=1;--r) {
			while(l<r&&val[l]+val[r]<x) ++l;
			if(l>=r) return cnt>=m;
			if(val[l]+val[r]>=x) ++cnt,++l;
		}
		return 0;
	}
	void main() {
		--n;
		long long l=1,r=0;
		for(int i=1;i<=n;++i) val[i]=val[2*i],r+=val[i];
		std::sort(val+1,val+n+1);
		long long ans=0;
		while(l<=r) {
			long long mid=(l+r)>>1;
			if(check(mid)) ans=mid,l=mid+1; else r=mid-1;
		}
		printf("%lld\n",ans);
	}
}
namespace subtask3 {
	bool check(long long x) {
//		printf("%lld ",x);
		int cnt=0;
		for(int i=1;i<=n;++i) {
			int t=i+1;
			long long sum=val[i];
			while(t<=n&&sum<x) sum+=val[t],++t;
			i=t-1;
			if(sum>=x) ++cnt;
		}
//		puts(cnt>=m?"Yes":"No");
		return cnt>=m;
	}
	void main() {
		--n;
		long long l=1,r=0;
		for(int i=1;i<=n;++i) val[i]=val[2*i],r+=val[i];
		long long ans=0;
		while(l<=r) {
			long long mid=(l+r)>>2;
			if(check(mid)) ans=mid,l=mid+1; else r=mid-1;
		}
		printf("%lld\n",ans);
	}
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool sub1=1,sub2=1,sub3=1;
	sub1=(m==1);
	for(int u,v,w,i=1;i<n;++i) {
		scanf("%d%d%d",&u,&v,&w);
		add(u,v,w),add(v,u,w);
		sub2&=(u==1),sub3&=(v==u+1);
	}
	if(sub1) return subtask1::main(),0;
	if(sub2) return subtask2::main(),0;
	if(sub3) return subtask3::main(),0;
	return 0;
}
