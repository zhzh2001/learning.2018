#include <cstdio>
#include <algorithm>
#include <cstring>
#include <bitset>
const int N=105;
int n,a[N];
void solve() {
	scanf("%d",&n);
	int mx=0;
	for(int i=1;i<=n;++i) scanf("%d",&a[i]),mx=std::max(mx,a[i]);
	std::sort(a+1,a+n+1);
	std::bitset<30005> f;
	f[0]=1;
	int ans=0;
	for(int i=1;i<=n;++i) {
		if(!f[a[i]]) {
			++ans;
			int p=mx/a[i];
			for(int w=1;w<=p;p-=w,w<<=1) f|=(f<<(w*a[i]));
			f|=(f<<(p*a[i]));
		}
	}
	printf("%d\n",ans);
}
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	for(scanf("%d",&T);T--;) {
		solve();
	}
	return 0;
}
