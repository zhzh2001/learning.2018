#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

inline char nc() {
	static char buf[1000000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++;
}
inline void read(int &x) {
	x=0; char c=nc(); for(;c<'0'||c>'9';c=nc());
	for(;c>='0'&&c<='9';x=(x*10)+(c^48),c=nc());
}
const int N=1e5+5;
int n,a[N],st[N][20],Log[N];
std::vector<int> b[10005];
long long ans;

namespace subtask1 {
	void main() {
		int mx=0;
		for(int i=1;i<=n;++i) mx=std::max(a[i],mx);
		for(int i=1;i<=mx;++i) {
			bool flg=0;
			for(int j=1;j<=n;++j) {
				if(a[j]==0) ans+=flg,flg=0;
				else flg=1;
			}
			ans+=flg;
			for(int j=1;j<=n;++j) if(a[j]) --a[j];
		}
		printf("%lld\n",ans);
	}
}
namespace subtask2 {
	void buildST() {
		for(int i=0;(1<<i)<=n;++i) Log[1<<i]=i;
		for(int i=1;i<=n;++i) Log[i]=std::max(Log[i-1],Log[i]);
		memset(st,0x3f,sizeof(st));
		for(int i=1;i<=n;++i) st[i][0]=a[i];
		for(int j=1;(1<<j)<=n;++j) {
			for(int i=1;i+(1<<j)-1<=n;++i) {
				st[i][j]=std::min(st[i][j-1],st[i+(1<<(j-1))][j-1]);
			}
		}
	}
	int query(int l,int r) {
		int k=Log[r-l+1];
		return std::min(st[l][k],st[r-(1<<k)+1][k]); 
	}
	void solve(int l,int r,int val) {
		if(l>r) return;
		int mn=query(l,r);
		ans+=mn-val;
		int lst=l-1;
		int st=std::lower_bound(b[mn].begin(),b[mn].end(),l)-b[mn].begin();
		int ed=std::upper_bound(b[mn].begin(),b[mn].end(),r)-b[mn].begin();
		for(int i=st;i<ed;++i) solve(lst+1,b[mn][i]-1,mn),lst=b[mn][i];
		solve(lst+1,r,mn);
	}
	void main() {
		for(int i=1;i<=n;++i) b[a[i]].push_back(i);
		buildST();
		solve(1,n,0);
		printf("%lld\n",ans);
	}
}
namespace subtask3{
	void solve(int l,int r) {
		if(l>r) return;
		int mn=1<<30;
		for(int i=l;i<=r;++i) mn=std::min(mn,a[i]);
		ans+=mn;
		for(int i=l;i<=r;++i) a[i]-=mn;
		int lst=l-1;
		for(int i=l;i<=r;++i) if(!a[i]) solve(lst+1,i-1),lst=i;
		solve(lst+1,r);
	}
	void main() {
		solve(1,n);
		printf("%lld\n",ans);
	}
}
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for(int i=1;i<=n;++i) read(a[i]);
//	subtask2::main();
	if(n<=1000) subtask1::main();
	else subtask2::main();
	return 0;
}
