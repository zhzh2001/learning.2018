#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<climits>
#include<cmath>
#include<algorithm>
#include<string>
#include<cstring>
#include<iomanip>
#include<vector>
#include<set>
#include<map>
#include<queue>
using namespace std;
inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9') {if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9') {x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x*f; 
}
int T;
int n;
int a[105];
bool mp[25005];
int cnt;
int maxd;
bool dfs(int x,int id,int dep)
{
	if(x<a[1]) return 0;
	if(mp[x]) return 1;
	if(dep>maxd) return 0;
	for(register int i=1;i<id;i++)
	{
		if(dfs(x-a[i],id,dep+1))
		{
			mp[x-a[i]]=1;
			return 1;
		}
	}
	return 0;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();
		memset(mp,0,sizeof(mp));
		for(register int i=1;i<=n;i++)
		a[i]=read();
		sort(a+1,a+n+1);
		mp[a[1]]=1;
		cnt=1;
		for(register int i=2;i<=n;i++)
		{
			maxd=1;
			while(!dfs(a[i],i,0))
			{
				maxd++;
				if(maxd>(a[i]/a[1]+1))
				{
					cnt++;
					mp[a[i]]=1;		
					break;			
				}
			}
		}
		printf("%d\n",cnt);
	}
	return 0;
}
