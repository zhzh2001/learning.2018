#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<climits>
#include<cmath>
#include<algorithm>
#include<string>
#include<cstring>
#include<iomanip>
#include<vector>
#include<set>
#include<map>
#include<queue>
using namespace std;
inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9') {if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9') {x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x*f; 
}
int n; 
int d[100005];
int ans;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(register int i=1;i<=n;i++)
	{
		d[i]=read();
		d[i]-=d[i-1];
		ans+=d[i-1];
	}
	printf("%d\n",ans);
	return 0;
}
