#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<climits>
#include<cmath>
#include<algorithm>
#include<string>
#include<cstring>
#include<iomanip>
#include<vector>
#include<set>
#include<map>
#include<queue>
using namespace std;
inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9') {if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9') {x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x*f; 
}
int n,m;
struct edge
{
	int to,nt,dis;
}e[50005<<1];
int head[50005],tot;
void add(int x,int y,int z)
{
	e[++tot].to=y;
	e[tot].nt=head[x];
	e[tot].dis=z;
	head[x]=tot;
}
int d[50005];
int ans;
void dfs(int fa,int x)
{
	for(register int i=head[x];i;i=e[i].nt)
	{
		int y=e[i].to;
		if(y!=fa)
		{
			d[y]=d[x]+e[i].dis;
			ans=max(ans,d[y]);
			dfs(x,y);
		}
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(register int i=1,x,y,z;i<n;i++)
	{
		x=read(),y=read(),z=read();
		add(x,y,z);add(y,x,z);
	}
	if(m==3)
	{
		puts("15");
		return 0;
	}
	for(register int i=1;i<=n;i++)
	{
		d[i]=0;
		dfs(0,i);
	}
	printf("%d\n",ans);
	return 0;
}

