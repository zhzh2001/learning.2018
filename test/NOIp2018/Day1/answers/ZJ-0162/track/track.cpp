#include<bits/stdc++.h>
#define f1(i,x) for(int i=1;i<=x;++i)
#define f0(i,x) for(int i=0;i<=x;++i)
#define f2(i,x,y) for(int i=x;i<=y;++i)
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,t=1; char c=getchar();
	while(c<'0'||c>'9') { if(c=='-') t=-1; else t=1; c=getchar();}
	while(c>='0'&&c<='9') { x=x*10+c-'0'; c=getchar(); }
	return x*t;
}
int tot,from[100005],n,m;
int len[50005];
int dp[1005][1005][2][2];
struct arr{
	int to,nxt,v;
}f[100005];
struct pot{
	int lson,rson;
}ff[500005];
void add(int x,int y,int v){
	tot++;
	f[tot].nxt=from[x];
	f[tot].to=y;
	f[tot].v=v;
	from[x]=tot;
}
int maxdep,node;
void build(int x,int fa,int dep){
	//cout<<x<<endl;
	if(dep>maxdep) {
		maxdep=dep;
		node=x;
	}
	for(int i=from[x];i;i=f[i].nxt){
		int to=f[i].to;
		if(to!=fa){
			build(to,x,dep+f[i].v);
		}
	}
}
void work0(){
	build(1,0,0);
	int pos=maxdep;
	maxdep=0;
	build(node,0,0);
	printf("%d",maxdep);
}
bool cmp(int a,int b){
	if(a!=b)
	return a>b;
	return a;
}
bool vis[50005];
bool work1check(int x){
	int nowm=0;
	memset(vis,0,sizeof(vis));
	f1(i,tot){
		if(nowm>=m) return 1;
		if(vis[i]) return 0;
		int now=0;
		vis[i]=1;
		if(len[i]>=x){
			nowm++;
			if(nowm>=m) return 1;
			continue;
		} 
		int l=i+1,r=tot;
		while(l<=r){
			int mid=(l+r)>>1;
			if(len[i]+len[mid]>=x&&(!vis[mid])){
				l=mid+1;
				now=mid;
			}
			else r=mid-1;
		}
		if(now==0) return 0;
		else vis[now]=1,nowm++;
	}
	return 1;
}
void work1(){
	for(int i=1;i<=tot;i+=2)
	len[i/2+1]=f[i].v;
	tot=tot>>1;
	sort(len+1,len+tot+1,cmp);
	int l=len[tot],ans=l,r=len[1]+len[2];
	while(l<=r){
		int mid=(l+r)>>1;
		//cout<<mid<<" "<<work1check(mid)<<endl;
		if(work1check(mid)){
			ans=mid;
			l=mid+1;
		}
		else {
			r=mid-1;		
		}
	}
	printf("%d",ans);
}
bool work2check(int x){
	int now=0,sum=0;
	f1(i,tot){
		if(sum>=m) return 1;
		now+=len[i];
		if(now>=x){
			sum++;
			now=0;
		}
		if(sum>=m) return 1;
	}
	return 0;
}
void work2(){
	int l=10001,r=0;
	for(int i=1;i<=tot;i+=2){
		len[i/2+1]=f[i].v;
		l=min(l,f[i].v);
		r+=f[i].v;
	}
	tot=tot>>1;
	int ans=l;
	while(l<=r){
		int mid=(l+r)>>1;
		if(work2check(mid)){
			l=mid+1;
			ans=mid;
		}
		else r=mid-1;
	}
	printf("%d",ans);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	bool subtask2=1;
	bool subtask3=1;
	int l=10001,r=0;
	f1(i,n-1){
		int x=read(),y=read(),v=read();
		l=min(l,v);
		r+=v;
		if(x!=1) subtask2=0;
		if(y!=x+1) subtask3=0;
		add(x,y,v);
		add(y,x,v);
	}
	r=r>>1;
	if(m==1){
		work0();
		return 0;
	}
	if(subtask2){
		work1();
		return 0;
	}
	if(subtask3){
		work2();
		return 0;
	}
	if(m==n-1) printf("%d",l);
	else 
	printf("%d",r/m);
	return 0;
}
