#include<bits/stdc++.h>
#define f1(i,x) for(int i=1;i<=x;++i)
#define f0(i,x) for(int i=0;i<=x;++i)
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,t=1; char c=getchar();
	while(c<'0'||c>'9') { if(c=='-') t=-1; else t=1; c=getchar();}
	while(c>='0'&&c<='9') { x=x*10+c-'0'; c=getchar(); }
	return x*t;
}
struct tree{
	ll mi;
	int node;
}f[1600005];
struct arr{
	ll v;
	int node;
};
ll a[100005],n;
ll ans;
void build(int x,int l,int r){
	if(l==r){
		f[x].mi=a[l];
		f[x].node=l;
		//cout<<x<<" "<<l<<" "<<a[l]<<endl;
		return ;
	} 
	int mid=(l+r)>>1;
	build(x<<1,l,mid);
	build(x<<1|1,mid+1,r);
	if(f[x<<1].mi<f[x<<1|1].mi){
		f[x].mi=f[x<<1].mi;
		f[x].node=f[x<<1].node;
	}
	else {
		f[x].mi=f[x<<1|1].mi;
		f[x].node=f[x<<1|1].node;
	}
}
arr query(int x,int l,int r,int L,int R){
	//cout<<l<<" "<<r<<endl;
	if(r<L||l>R) return (arr){2147364000,-1};
	if(l==r) return (arr){f[x].mi,f[x].node};
	if(l>=L&&r<=R) return (arr){f[x].mi,f[x].node};
	int mid=(l+r)>>1;
	arr lpos=query(x<<1,l,mid,L,R);
	arr rpos=query(x<<1|1,mid+1,r,L,R);
	if(lpos.v<rpos.v){
		return lpos;
	}
	else return rpos;
}
void work(int l,int r,int v){
	if(l>r) return ; 
	int nod=query(1,1,n,l,r).node;
	if(nod==-1) return ; //cout<<nod<<" "<<l<<" "<<r<<endl;;
	int val=a[nod]-v;
	ans+=val;
	work(l,nod-1,v+val);
	work(nod+1,r,v+val);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	f1(i,n) a[i]=read();
	build(1,1,n);
	//cout<<query(1,1,n,4,6).v;
	work(1,n,0);
	cout<<ans;
	return 0;
}
