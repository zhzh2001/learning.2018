#include<bits/stdc++.h>
#define f1(i,x) for(int i=1;i<=x;++i)
#define f0(i,x) for(int i=0;i<=x;++i)
#define f2(i,x,y) for(int i=x;i<=y;++i)
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,t=1; char c=getchar();
	while(c<'0'||c>'9') { if(c=='-') t=-1; else t=1; c=getchar();}
	while(c>='0'&&c<='9') { x=x*10+c-'0'; c=getchar(); }
	return x*t;
}
int n,a[205],tot,ans,now[205];
bool v[205];
bool check(int x,int loc){
	if(!(x%now[loc])) return 0;
	if(loc==tot) return 1;
	while(x>0){
		if(!check(x,loc+1)) return 0;
		x-=now[loc];
	} 
	return 1;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t=read();
	while(t--){
		int n=read();
		memset(a,0,sizeof(a));
		memset(v,0,sizeof(v));
		memset(now,0,sizeof(now));
		tot=0,ans=1;
		f1(i,n) a[i]=read();
		sort(a+1,a+n+1);
		now[++tot]=a[1];
		f2(i,2,n){
			//cout<<a[i]<<" "<<check(a[i],1)<<endl;
			if(check(a[i],1)){
				now[++tot]=a[i];
				ans++;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
