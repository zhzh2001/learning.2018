#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=50005;
int n,m,a[N],b[N],l[N],dp[N],dp_[N],deep[N],ans;
int v[N+N],Next[N+N],head[N],cost[N+N],e;
bool cmp(int a,int b){
	return a>b;
}
void add(int a,int b,int c){
	v[++e]=b; Next[e]=head[a]; head[a]=e; cost[e]=c;
}
void dfs_1(int u,int f){
	for (int i=head[u];i;i=Next[i])
	if (v[i]!=f){
		dfs_1(v[i],u); dp[u]=max(dp[u],dp[v[i]]+cost[i]);
	}
}
void dfs_2(int u,int f){
	ans=max(ans,dp_[u]+dp[u]);
	int Max=dp_[u],Maxi=0,Maxx=0;
	for (int i=head[u];i;i=Next[i])
		if (v[i]!=f) {
			if (dp[v[i]]+cost[i]>Max) Maxx=Max,Max=dp[v[i]]+cost[i],Maxi=v[i];
			else Maxx=max(Maxx,dp[v[i]]+cost[i]);
		}
	if (u==1) ans=max(ans,Max+Maxx);
	for (int i=head[u];i;i=Next[i])
		if (v[i]!=f){
			dfs_2(v[i],u);
			if (v[i]!=Maxi) dp_[v[i]]=Max+cost[i];
			else dp_[v[i]]=Maxx+cost[i];
	}
}
void solve1(){
	for (int i=1;i<n;i++) add(a[i],b[i],l[i]),add(b[i],a[i],l[i]);
	dfs_1(1,0); dfs_2(1,0);
	printf("%d\n",ans);
}
int check(int x){
	int now=1,j=n,k=0;
	while (now<=j){
		if (l[now]>=x) k++,now++;
		else {
			if (now<j && l[now]+l[j]>=x) k++,now++,j--;
			else j--;
		}
	}
	if (k>=m) return 1;
	else return 0;
}
void solve2(){
	sort(l+1,l+n,cmp);
	int l=1,r=20000;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (check(mid)) l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
}
int check_(int x){
	int now=0,k=0;
	for (int i=1;i<=n;i++){
		now+=l[i];
		if (now>=x) k++,now=0;
	}
	if (k>=m) return 1;
	else return 0;
}
void solve3(){
	int l=1,r=500000000;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (check_(mid)) l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);  int fl1=1,fl2=1;
	for (int i=1;i<n;i++) {
		scanf("%d%d%d",&a[i],&b[i],&l[i]);
		if (a[i]!=1) fl1=0;
		if (a[i]+1!=b[i]) fl2=0;
	}
	if (m==1) {
		solve1(); return 0;
	}
	if (fl1) {
		solve2(); return 0;
	}
	if (fl2) {
		solve3(); return 0;
	}
	return 0;
}
