#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
int n,a[100005],Q[100005];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	int h=0,ans=0; Q[++h]=a[1];
	for (int i=2;i<=n;i++) {
		if (h>0 && Q[h]>=a[i]) {
			ans+=Q[h]-a[i];
			while (h>0 && Q[h]>=a[i]) h--;
		}
		Q[++h]=a[i];
	}
	printf("%d\n",ans+Q[h]);
	return 0;
}
