#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=25000;
int fl[N+5],f[N+5];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T,n,ans,x; scanf("%d",&T);
	while (T--){
		memset(fl,0,sizeof(fl));
		memset(f,0,sizeof(f));
		scanf("%d",&n); f[0]=1; ans=0;
		for (int i=1;i<=n;i++) {
			scanf("%d",&x);
			fl[x]=1;
		}
		for (int i=1;i<=N;i++) 
			if (fl[i] && f[i]==0){ 
				for (int j=i;j<=N;j++)
					f[j]|=f[j-i];
				ans++;
			}
		printf("%d\n",ans);
	}
	return 0;
}
