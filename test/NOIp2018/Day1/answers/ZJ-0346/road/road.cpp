#include<cstdio>
#include<cctype>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<string>
const int maxn = 200200;
int a[maxn],n;
int b[maxn];
namespace my{
	inline int abs(int a){
		return a>0?a:-a;
	}
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;++i)
		scanf("%d",a+i);
	long long ans=0;
	for(int i=0;i<=n;++i)
		ans+=my::abs(b[i] = a[i] - a[i+1]);
	printf("%lld",ans/2);
}
