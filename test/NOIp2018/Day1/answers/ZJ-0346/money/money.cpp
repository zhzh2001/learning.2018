#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cstring>
#include<string>
#include<cctype>
const int maxn = 100100;
int x,ch;
inline int read(){
	while(isspace(ch=getchar()));x=ch&15;
	while(isdigit(ch=getchar()))x=x*10+(ch&15);
	return x;
}
int t;
int a[maxn];
int c[maxn];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read();
	while(t--){
		int n=read();
		int ans=0;
		memset(c,0,sizeof c);
		c[0]=1;
		for(int i=1;i<=n;++i)a[i]=read();
		std::sort(a+1,a+n+1);
		for(int i=1;i<=n;++i){
			int x=a[i];
			if(c[x])continue;
			else{
				++ans;
				for(int i=0;i<=25000;++i)
					if(c[i])c[i+x]=1;
			}
		}
		printf("%d\n",ans);
	}
}
