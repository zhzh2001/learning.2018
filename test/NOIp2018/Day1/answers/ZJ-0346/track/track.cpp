#include<cstdio>
#include<algorithm>
#include<iostream>
#include<vector>
#include<list>
#include<set>
#include<cctype>
#include<cstring>
#include<string>
const int maxn = 200200;
struct T{
	int to,nxt,v;
}way[maxn<<1];
int h[maxn],num;
inline void adde(int x,int y,int v){
	way[++num]=(T){y,h[x],v},h[x]=num;
	way[++num]=(T){x,h[y],v},h[y]=num;
}
int n,m;
int vis[maxn];
inline void up(int&a,int b) {if(a<b)a=b;}
int cnt[maxn],c[maxn];
std::multiset<int>sets[maxn];
int tot,len[maxn];
inline void dfs(int a,int v){
	vis[a]=1;
	std::multiset<int>&s=sets[a];
	len[a]=0;
	int cnt=0;
	for(int i=h[a];i;i=way[i].nxt)
		cnt+=!vis[way[i].to];
	for(int i=h[a];i;i=way[i].nxt)
		if(!vis[way[i].to]){
			dfs(way[i].to,v);
			len[way[i].to]+=way[i].v;
			if(tot >= m){
				vis[a]=0;
				return ;
			}	
			if(len[way[i].to] >= v){
				++tot;
				continue;
			}
			
			if(cnt>1)s.insert(len[way[i].to]);
			else {
				up(len[a],len[way[i].to]);
			}
		}
	while(s.size()){
		int val=*s.begin();
		s.erase(s.begin()); 
		std::multiset<int>::iterator it = s.lower_bound(v-val);
		if(it!=s.end()){
			++tot;
			if(tot >= m){
				s.clear();
				vis[a]=0;
				return ;
			}
			s.erase(it);
		}else up(len[a],val);
	}
	vis[a]=0;
}
inline int calc(int x){
	tot=0;
	return dfs(1,x),tot;
}
char buf[(int)2e7],*vin=buf;
int x,ch;
#define getchar() (*vin++)
inline int read(){
	while(isspace(ch=getchar()));x=ch&15;
	while(isdigit(ch=getchar()))x=x*10+(ch&15);
	return x;
}
inline std::pair<int,int> find(int x){
	vis[x]=1;
	std::pair<int,int>y;y.second=x;
	for(int i=h[x];i;i=way[i].nxt)
		if(!vis[way[i].to]){
			std::pair<int,int> a = find(way[i].to);
			a.first+=way[i].v;
			if(a>y)y=a;
		}
	vis[x]=0;
	return y;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	fread(buf,1,sizeof buf,stdin);
	n=read(),m=read();
	int sum=0;
	for(int i=1,x,y,v;i<n;++i){
		x=read(),y=read(),v=read(),adde(x,y,v),sum+=v;
	}
	int l=0;
	int r=std::min(sum/m,find(find(1).second).first),ans=0;

	while(l!=r){
		int mid=l+r>>1;
		if(calc(mid)>=m)l=mid+1,up(ans,mid);
		else r=mid;
	}
	if(l > ans && calc(l)>=m)
		ans=l;
	printf("%d\n",ans);
}
