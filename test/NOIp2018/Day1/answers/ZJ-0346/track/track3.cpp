#include<cstdio>
#include<algorithm>
#include<iostream>
#include<vector>
#include<list>
#include<set>
#include<cctype>
#include<cstring>
#include<string>
#include<ctime>
const int maxn = 200200;
struct T{
	int to,nxt,v;
}way[maxn<<1];
int h[maxn],num;
inline void adde(int x,int y,int v){
	way[++num]=(T){y,h[x],v},h[x]=num;
	way[++num]=(T){x,h[y],v},h[y]=num;
}
int n,m;
int vis[maxn];
int used[maxn],tm;
inline void up(int&a,int b) {if(a<b)a=b;}
int nxt[maxn],pre[maxn];
std::pair<int,int> f[maxn];
int cnt[maxn],c[maxn];
inline void pt(std::vector<int>&v,int b){
	int id=0;
	for(std::vector<int>::iterator it=v.begin();it!=v.end();++it)++cnt[(c[++id]=*it)>>b&255];
	for(int i=1;i<=255;++i)cnt[i]+=cnt[i-1];
	for(int i=1;i<=id;++i)v[--cnt[c[i]>>b&255]]=c[i];
	for(int i=0;i<=255;++i)cnt[i]=0;
}
inline void my_sort(std::vector<int>&v){
	if(v.size() < 320){
		std::sort(v.begin(),v.end());
		return ;
	}
	pt(v,0),pt(v,8);
	pt(v,16),pt(v,24);
}
std::multiset<int>sets[maxn];
inline void dfs(int a,int v){
	vis[a]=1;
	std::pair<int,int>&y=f[a];
	std::multiset<int>&s=sets[a];
//	s.clear();
//	std::vector<int>vec;
	y.first=y.second=0;
	for(int i=h[a];i;i=way[i].nxt)
		if(!vis[way[i].to]){
			dfs(way[i].to,v);
			std::pair<int,int> x=f[way[i].to];
			y.first+=x.first;
			x.second+=way[i].v;
			if(x.second >= v){
				++y.first;
				continue;
			}
//			vec.push_back(x.second) ;
s.insert(x.second);
		}
//		std::sort(vec.begin(),vec.end());
//	my_sort(vec);
	while(s.size()){
		std::multiset<int>::iterator it=s.lower_bound(v-*s.begin());
		if(it == s.begin())++it;
		if(it!=s.end()){
			++y.first;
			s.erase(it),s.erase(s.begin());
		}else up(y.second,*s.begin()),s.erase(s.begin());
	}
	vis[a]=0;
}
inline int calc(int x){
	return dfs(1,x),f[1].first;
}
char buf[(int)2e7],*vin=buf;
int x,ch;
#define getchar() (*vin++)
inline int read(){
	while(isspace(ch=getchar()));x=ch&15;
	while(isdigit(ch=getchar()))x=x*10+(ch&15);
	return x;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track2.out","w",stdout);
	fread(buf,1,sizeof buf,stdin);
	n=read(),m=read();
	int sum=0;
	for(int i=1,x,y,v;i<n;++i)
		x=read(),y=read(),v=read(),adde(x,y,v),sum+=v;
	int l=0,r=sum/m,ans=0;
	while(l!=r){
		int mid=l+r>>1;
		if(calc(mid)>=m)l=mid+1,up(ans,mid);
		else r=mid;
	}
	if(l > ans && calc(l)>=m)
		ans=l;
	printf("%d\n",ans);
}
