#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const int N = 111;
const int M = 101010;

int t, n, m, ans;
int a[N], f[M];

int main(void) {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &t);
	while (t--) {
		scanf("%d", &n);
		m = 0;
		for (int i = 1; i <= n; i++) {
			scanf("%d", &a[i]);
			m = max(m, a[i]);
		}
		f[0] = 1;
		for (int i = 1; i <= m; i++) f[i] = 0;
		sort(a + 1, a + n + 1);
		ans = n;
		for (int i = 1; i <= n; i++) {
			if (f[a[i]]) --ans;
			for (int j = 0; j + a[i] <= m; j++)
				f[j + a[i]] |= f[j];
		}
		printf("%d\n", ans);
	}
	return 0;
}
