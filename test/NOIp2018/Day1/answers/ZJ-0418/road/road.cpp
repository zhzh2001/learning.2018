#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const int N = 202020;
const int INF = 1 << 30;
typedef pair<int, int> pairs;

int mn[N << 2], ad[N << 2], mnp[N << 2], ps[N << 2];
int n, ans;
int d[N];

inline void pushDown(int o) {
	if (ad[o] != 0) {
		ad[o << 1] += ad[o]; ad[o << 1 | 1] += ad[o];
		mn[o << 1] += ad[o]; mn[o << 1 | 1] += ad[o];
		ad[o] = 0;
	}
}
inline void pushUp(int o) {
	if (mn[o << 1] < mn[o << 1 | 1]) {
		mn[o] = mn[o << 1];
		mnp[o] = mnp[o << 1];
	} else {
		mn[o] = mn[o << 1 | 1];
		mnp[o] = mnp[o << 1 | 1];
	}
}
inline void add(int o, int l, int r, int L, int R, int x) {
	pushDown(o);
	if (l >= L && r <= R) {
		ad[o] += x; mn[o] += x;
		return;
	}
	int mid = (l + r) >> 1;
	if (L <= mid) add(o << 1, l, mid, L, R, x);
	if (R > mid) add(o << 1 | 1, mid + 1, r, L, R, x);
	pushUp(o);
}
inline pairs query(int o, int l, int r, int L, int R) {
	pushDown(o);
	if (l >= L && r <= R) return pairs(mn[o], mnp[o]);
	int mid = (l + r) >> 1;
	pairs res = pairs(INF, -1);
	if (L <= mid) {
		pairs tmp = query(o << 1, l, mid, L, R);
		if (tmp < res) res = tmp;
	}
	if (R > mid) {
		pairs tmp = query(o << 1 | 1, mid + 1, r, L, R);
		if (tmp < res) res = tmp;
	}
	return res;
}
inline void build(int o, int l, int r) {
	if (l == r) {
		ad[o] = 0; mn[o] = d[l];
		mnp[o] = o; ps[o] = l;
		return;
	}
	int mid = (l + r) >> 1;
	build(o << 1, l, mid);
	build(o << 1 | 1, mid + 1, r);
	pushUp(o);
}
inline void solve(int l, int r) {
	if (l > r) return;
	pairs dt = query(1, 1, n, l, r);
	add(1, 1, n, l, r, -dt.first);
	ans += dt.first;
	int pos = ps[dt.second];
	solve(l, pos - 1);
	solve(pos + 1, r);
}

int main(void) {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", &d[i]);
	build(1, 1, n);
	solve(1, n);
	cout << ans << endl;
	return 0;
}
