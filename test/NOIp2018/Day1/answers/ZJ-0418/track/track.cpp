#include <set>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

const int N = 101010;
typedef pair<int, int> pairs;

inline char get(void) {
	static char buf[100000], *s = buf, *t = buf;
	if (s == t) {
		t = (s = buf) + fread(buf, 1, 100000, stdin);
		if (s == t) return EOF;
	}
	return *s++;
}
inline void read(int &x) {
	static char c;
	for (c = get(); c < '0' || c > '9'; c = get());
	for (x = 0; c >= '0' && c <= '9'; c = get())
		x = x * 10 + c - '0';
}

struct edge {
	int to, next, key;
	edge(int t = 0, int n = 0, int k = 0): to(t), next(n), key(k) {}
} G[N << 1];
int head[N];
int n, m, x, y, z, gcnt;

inline void addEdge(int from, int to, int key) {
	G[++gcnt] = edge(to, head[from], key); head[from] = gcnt;
	G[++gcnt] = edge(from, head[to], key); head[to] = gcnt;
}

namespace work1 {
	int rt, mx;
	inline void dfs(int u, int f, int sm) {
		int to;
		if (sm > mx) {
			mx = sm; rt = u;
		}
		for (int i = head[u]; i; i = G[i].next) {
			to = G[i].to; if (to == f) continue;
			dfs(to, u, sm + G[i].key);
		}
	}
	void solve(void) {
		mx = 0; dfs(1, 0, 0);
		mx = 0; dfs(rt, 0, 0);
		printf("%d\n", mx);
	}
}
namespace work2 {
	int w[N];
	inline bool check(int lim) {
		int cnt = 0, j = 1;
		for (int i = n - 1; i >= 1; i--) {
			if (w[i] >= lim) {
				++cnt;
			} else {
				while (w[i] + w[j] < lim) ++j;
				if (j >= i) break;
				++cnt; ++j;
			}
		}
		if (cnt >= m) return true;
		else return false;
	}
	void solve(void) {
		int L = 0, R = 0, Mid, Ans;
		for (int i = 1; i <= gcnt; i += 2)
			R += (w[(i + 1) / 2] = G[i].key);
		sort(w + 1, w + n);
		while (L <= R) {
			Mid = (L + R) >> 1;
			if (check(Mid)) L = (Ans = Mid) + 1;
			else R = Mid - 1;
		}
		printf("%d\n", Ans);
	}
}
namespace work3 {
	int w[N];
	inline bool check(int lim) {
		int sm = 0, cnt = 0;
		for (int i = 1; i < n; i++) {
			sm += w[i];
			if (sm >= lim) {
				++cnt; sm = 0;
			}
		}
		if (cnt >= m) return true;
		else return false;
	}
	void solve(void) {
		int L = 0, R = 0, Mid, Ans;
		for (int i = 1; i <= gcnt; i += 2)
			R += (w[(i + 1) / 2] = G[i].key);
		while (L <= R) {
			Mid = (L + R) >> 1;
			if (check(Mid)) L = (Ans = Mid) + 1;
			else R = Mid - 1;
		}
		printf("%d\n", Ans);
	}
}
namespace work4 {
	int cnt;
	int b[N];
	inline int dfs(int u, int f, int lim) {
		vector<int> w; w.clear();
		int to, rst = 0;
		for (int i = head[u]; i; i = G[i].next) {
			to = G[i].to; if (to == f) continue;
			w.push_back(dfs(to, u, lim) + G[i].key);
		}
		for (int i = 0; i < w.size(); i++) b[i] = 1;
		sort(w.begin(), w.end());
		int j = w.size() - 1;
		while (j >= 0 && w[j] >= lim) {
			b[j] = 0; --j; ++cnt;
		}
		for (int i = 0; i < w.size(); i++)
			if (b[i]) {
				for (int k = i + 1; k < w.size(); k++)
					if (b[k] && w[i] + w[k] >= lim) {
						++cnt; b[k] = 0; b[i] = 0; break;
					}
			}
		for (int i = w.size() - 1; i >= 0; i--)
			if (b[i]) {
				rst = w[i]; break;
			}
		return rst;
	}
	inline bool check(int lim) {
		cnt = 0;
		cnt += (dfs(1, 0, lim) >= lim);
		if (cnt >= m) return true;
		else return false;
	}
	void solve(void) {
		int L = 0, R = 0, Mid, Ans;
		for (int i = 1; i <= gcnt; i += 2)
			R += G[i].key;
		while (L <= R) {
			Mid = (L + R) >> 1;
			if (check(Mid)) L = (Ans = Mid) + 1;
			else R = Mid - 1;
		}
		printf("%d\n", Ans);
	}
}

int main(void) {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	read(n); read(m);
	int tp1 = 1, tp2 = 1;
	for (int i = 1; i < n; i++) {
		read(x); read(y); read(z);
		addEdge(x, y, z);
		if (x != 1) tp1 = 0;
		if (y != x + 1) tp2 = 0;
	}
	if (m == 1) work1::solve();
	else if (n <= 1000) work4::solve();
	else if (tp1) work2::solve();
	else if (tp2) work3::solve();
	else work4::solve();
	return 0;
}
