#include <bits/stdc++.h>
using namespace std;
const int maxn = 50000;
int n, m, fa[maxn + 10], a[maxn + 10], dcnt;
int ans, f[maxn + 10], b[maxn + 10], bcnt;
bool ban[maxn + 10];
struct edge {
	int to, len, nxt;
}eg[maxn * 2 + 10];
int h[maxn + 10], egcnt;

void add(int &h, int to, int len) {
	eg[++egcnt] = (edge){to, len, h};
	h = egcnt;
}

void dfs(int p) {
	a[++dcnt] = p;
	for (int i = h[p]; i; i = eg[i].nxt)
		if (eg[i].to != fa[p]) {
			fa[eg[i].to] = p;
			dfs(eg[i].to);
		}
}

int work(int v) {
	int p = bcnt, ans = 0;
	while (p >= 1 && b[p] >= v) {
		ans += !ban[p]; --p;
	}
	for (int i = p, t = 1; i >= 1; --i)
		if (!ban[i]) {
			while (t < i && (ban[t] || b[t] + b[i] < v)) ++t;
			if (t >= i) break;
			++ans; ++t;
		}
	return ans;
}

int calc(int v) {
	int ans = 0;
	for (int i = n; i >= 1; --i) {
		int p = a[i];
		f[p] = bcnt = 0;
		for (int j = h[p]; j; j = eg[j].nxt)
			if (eg[j].to != fa[p]) b[++bcnt] = f[eg[j].to] + eg[j].len;
		sort(b + 1, b + bcnt + 1);
		for (int j = 1; j <= bcnt; ++j) ban[j] = 0;
		int res = work(v); ans += res;
		for (int l = 1, r = bcnt; l <= r; ) {
			int mid = (l + r) >> 1;
			ban[mid] = 1;
			if (work(v) == res) {
				l = mid + 1; f[p] = b[mid];
			} else r = mid - 1;
			ban[mid] = 0;
		}
	}
	return ans;
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i < n; ++i) {
		int l, r, w; scanf("%d%d%d", &l, &r, &w);
		add(h[l], r, w); add(h[r], l, w);
	}
	dfs(1);
	for (int l = 0, r = 1e9; l <= r; ) {
		int mid = (l + r) >> 1;
		if (calc(mid) >= m) {
			l = mid + 1; ans = mid;
		} else r = mid - 1;
	}
	printf("%d", ans);
	fclose(stdin);
	fclose(stdout);
}
