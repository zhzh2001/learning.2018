#include <bits/stdc++.h>
using namespace std;
const int maxn = 100000;
int n, ans, a[maxn + 10];
int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; ++i) {
		scanf("%d", &a[i]); ans += a[i];
	}
	for (int i = 1; i < n; ++i) ans -= min(a[i], a[i + 1]);
	printf("%d", ans);
	fclose(stdin);
	fclose(stdout);
}
