#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int n,a[100010],top,S[100010];
long long ans;
int read()
{
	int ans=0;
	char ch=getchar();
	while (ch<'0' || ch>'9') ch=getchar();
	while (ch>='0' && ch<='9')
	{
		ans=ans*10+ch-'0';
		ch=getchar();
	}
	return ans;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<=n+1;i++)
	{
		while (top && S[top]>=a[i])
		{
			ans+=S[top]-max(S[top-1],a[i]);
			top--;
		}
		S[++top]=a[i];
		
	}
	printf("%lld\n",ans);
	return 0;
}
