#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int cnt,nxt[200010],len[200010],vet[200010],head[100010],disL[100010];
int n,m,p,disR[100010];
long long ans;
bool flag1,flag2;
int read()
{
	int ans=0;
	char ch=getchar();
	while (ch<'0' || ch>'9') ch=getchar();
	while (ch>='0' && ch<='9')
	{
		ans=ans*10+ch-'0';
		ch=getchar();
	}
	return ans;
}
void addedge(int u,int v,int l)
{
	nxt[++cnt]=head[u];vet[cnt]=v;len[cnt]=l;head[u]=cnt;
}
void dfs(int u,int f,long long dis)
{
	if (dis>ans)
	{
		ans=dis;
		p=u;
	}
	for (int i=head[u];i;i=nxt[i])
	{
		int v=vet[i];
		if (v!=f)
		{
			dfs(v,u,dis+len[i]);
		}
	}
}
bool check(long long D)
{
	int t=0,x=1;
	long long now=0;
	while (x!=n)
	{
		now+=disL[x];
		if (now>=D)
		{
			now=0;
			t++;
		}
		x++;
	}
	return (t>=m);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	long long L=0,R=0;
	flag1=flag2=1;
	n=read();m=read();
	for (int i=1;i<n;i++)
	{
		int u=read(),v=read(),l=read();
		disL[u]=l;disR[v]=l;
		addedge(u,v,l);
		addedge(v,u,l);
		R=R+l;
		if (v!=u+1) flag2=0;
		if (u!=1) flag1=0;
	}
	if (m==1)
	{
		dfs(1,0,0);
		dfs(p,0,0);
		printf("%lld\n",ans);
		return 0;
	}
	if (flag2)
	{
		while (L<=R)
		{
			long long mid=(L+R)>>1;
			if (check(mid)) L=mid+1,ans=mid;
			else R=mid-1;
		}
		printf("%lld\n",ans);
		return 0;
	}
	if (flag1)
	{
		sort(disR+1,disR+n+1);
		ans=disR[n]+disR[n-1];
		for (int i=n-m+1;i<=n;i++)
		{
			if (2*n-2*m+1-i>1) ans=min(ans,0LL+disR[i]+disR[2*n-2*m+1-i]);
			else ans=min(ans,0LL+disR[i]);
		}
		printf("%lld\n",ans);
		return 0;
	}
	return 0;
}
