#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int T,n,a[110],dp[100010];
int read()
{
	int ans=0;
	char ch=getchar();
	while (ch<'0' || ch>'9') ch=getchar();
	while (ch>='0' && ch<='9')
	{
		ans=ans*10+ch-'0';
		ch=getchar();
	}
	return ans;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while (T--)
	{
		int m=0;
		n=read();
		for (int i=1;i<=n;i++) a[i]=read(),m=max(m,a[i]);
		memset(dp,0,sizeof(dp));
		for (int i=1;i<=n;i++)
		{
			for (int j=a[i];j<=m;j++)
				if (j-a[i]==0 || dp[j-a[i]])
					dp[j]=max(dp[j],dp[j-a[i]]+1);
		}
		int ans=n;
		for (int i=1;i<=n;i++)
			if (dp[a[i]]>1) ans--;
		printf("%d\n",ans);
	}
	return 0;
}
