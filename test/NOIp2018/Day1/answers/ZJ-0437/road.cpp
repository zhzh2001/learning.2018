#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define N 100005
int a[N];
inline int get() {
	int num = 0; char ch = getchar();
	for ( ; ch < '0' || ch > '9'; ch = getchar()) ;
	for ( ; ch >= '0' && ch <= '9'; ch = getchar())
		num = (num << 3) + (num << 1) + (ch ^ '0');
	return num;
}
inline long long solve(int l, int r, int d) {
	if (l > r) return 0;
	long long ans = 0;
	int next_d = *std::min_element(a + l, a + r + 1);
	ans = next_d - d;
	for (int i = l; i <= r; i++)
		if (a[i] == next_d) {
			if (l <= i - 1)
				ans += solve(l, i - 1, next_d);
			l = i + 1;
		}
	ans += solve(l, r, next_d);
	return ans;
} 
int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	int n;
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	printf("%lld\n", solve(1, n, 0));
	return 0;
}
