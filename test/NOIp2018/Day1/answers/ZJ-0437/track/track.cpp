#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define N 50005
inline int get() {
	int num = 0; char ch = getchar();
	for ( ; ch < '0' || ch > '9'; ch = getchar()) ;
	for ( ; ch >= '0' && ch <= '9'; ch = getchar())
		num = (num << 3) + (num << 1) + (ch ^ '0');
	return num;
}
struct edgeType {
	int to, nxt, dist;
} edge[N << 1];
int head[N];
inline void addEdge(int from, int to, int dist) {
	static int cnt = 0;
	edge[++cnt] = (edgeType){to, head[from], dist};
	head[from] = cnt;
}
int n, m, sum, ans, cnt, idx;
int u[N], v[N], w[N];
struct algo1 { // m == 1
	int f[N][0];
	algo1() {
		memset(f, 0, sizeof f);
		ans = 0;
	}
	inline void dfs(int u, int p) {
		int max1 = 0, max2 = 0;
		for (int i = head[u]; i; i = edge[i].nxt) {
			int v = edge[i].to;
			if (v == p) continue;
			dfs(v, u);
			int cur = f[v][1] + edge[i].dist;
			if (cur > max1) { max2 = max1; max1 = cur; }
			else if (cur > max2) max2 = cur;
		}
		f[u][1] = max1;
		f[u][0] = max1 + max2;
		if (f[u][0] > ans) ans = f[u][0];
		if (f[u][1] > ans) ans = f[u][1];
	}
	inline void solve() {
		dfs(1, 0);
		printf("%d\n", ans);
	}
};
struct algo2 { // a_i == 1
	algo2() {}
	inline bool judge(int bound) {
		int l = 1, r = n - 1, cnt = 0;
		while (l < r) {
			while (l < r && w[l] + w[r] < bound) l++;
			if (l < r) if (++cnt == m) return true;
			l++, r--; 
		}
		return false;
	}
	inline void solve() {
		std::sort(w + 1, w + n);
		int l = 0, r = sum;
		while (l <= r) {
			int mid = l + ((r - l) >> 1);
			if (judge(mid)) {
				ans = mid;
				l = mid + 1;
			}
			else r = mid - 1;
		}
		printf("%d\n", ans);
	} 
};
struct algo3 { // b_i == a_i + 1
	int chain[N];
	algo3() {
		memset(chain, 0, sizeof chain);
	}
	inline void expand(int u, int p) {
		for (int i = head[u]; i; i = edge[i].nxt) {
			int v = edge[i].to;
			if (v == p) continue;
			chain[++idx] = edge[i].dist;
			expand(v, u);
		}
	}
	inline bool judge(int bound) {
		int cur = 0, cnt = 0;
		for (int i = 1; i < n; i++) {
			cur += chain[i];
			if (cur >= bound) {
				cur = 0;
				if (++cnt >= m) return true;
			}
		}
		return false;
	}
	inline void solve() {
		expand(1, 0);		
		int l = 0, r = sum;
		while (l <= r) {
			int mid = l + ((r - l) >> 1);
			if (judge(mid)) {
				l = mid + 1;
				ans = mid;
			}
			else r = mid - 1;
		}
		printf("%d\n", ans);
	}
};
struct algo4 {
	algo4() {}
	inline void solve() {
		printf("%d\n", sum / m);
	}
};
int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	n = get(), m = get(), sum = 0;
	bool flag1 = true, flag2 = true;
	for (int i = 1; i < n; i++) {
		u[i] = get(), v[i] = get(), w[i] = get();
		if (u[i] ^ 1) flag1 = false;
		if (v[i] ^ (u[i] + 1)) flag2 = false;
		addEdge(u[i], v[i], w[i]); addEdge(v[i], u[i], w[i]);
		sum += w[i];
	}
	if (m == 1) { algo1 *solution = new algo1(); solution->solve(); }
	else if (flag1) { algo2 *solution = new algo2(); solution->solve(); }
	else if (flag2) { algo3 *solution = new algo3(); solution->solve(); }
	else { algo4 *solution = new algo4(); solution->solve(); }
	return 0;
}
