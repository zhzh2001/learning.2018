#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
inline int get() {
	int num = 0; char ch = getchar();
	for ( ; ch < '0' || ch > '9'; ch = getchar()) ;
	for ( ; ch >= '0' && ch <= '9'; ch = getchar())
		num = (num << 3) + (num << 1) + (ch ^ '0');
	return num;
}
int a[105], f[25005];
int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T = get();
	while (T--) {
		int n = get();
		for (int i = 1; i <= n; i++)
			a[i] = get();
		std::sort(a + 1, a + n + 1);
		int ans = n;
		memset(f, false, sizeof f);	f[0] = 1;
		for (int i = 1; i <= n; i++) {
			if (f[a[i]]) { ans--; continue;	}
			for (int j = a[i]; j <= 25000; j++)
				f[j] |= f[j - a[i]];
		}
		printf("%d\n", ans);
	}
	return 0;
}
