#include<bits/stdc++.h>
#define M 100005
using namespace std;
int n;
int A[M];
struct P70{
//	int Log2[1005],St[1005][11];
	int Log2[M],St[M][17];
	struct Node{
		int l,r,ad;
	};
	queue<Node>Q;
	int Query(int l,int r){
		int k=Log2[r-l+1];
		return Min(St[l][k],St[r-(1<<k)+1][k]);	
	}
	int Min(int x,int y){
		if(A[x]<A[y])return x;	
		return y;
	}
	void Solve(){
		memset(St,0,sizeof(St));
		for(int i=2;i<=n;i++)Log2[i]=Log2[i>>1]+1;
		for(int i=1;i<=n;i++)St[i][0]=i;
		for(int j=1;(1<<j)<=n;j++)
			for(int i=1;i+(1<<j)-1<=n;i++)
				St[i][j]=Min(St[i][j-1],St[i+(1<<j-1)][j-1]);
		while(!Q.empty())Q.pop();
		Q.push(Node{1,n,0});
		int ans=0;
		while(!Q.empty()){
			Node x=Q.front();Q.pop();
			int tmp=Query(x.l,x.r);
			ans+=A[tmp]-x.ad;
			if(x.l<=tmp-1)Q.push((Node){x.l,tmp-1,A[tmp]});
			if(tmp+1<=x.r)Q.push((Node){tmp+1,x.r,A[tmp]});
		}
		printf("%d\n",ans);
	}
}p70;
int main(){
//	printf("%.4f\n",1.0*(sizeof())/1024/1024);
	freopen("road.in","r",stdin);
//	freopen("road2.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&A[i]);
	p70.Solve();
	return 0;
}
