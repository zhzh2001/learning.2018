#include<bits/stdc++.h>
#define M 50005
using namespace std;
int n,m,etot,sum;
int Head[M],Deg[M];
struct Edge{
	int to,nxt,val;
}E[M<<1];
void Add_edge(int u,int v,int x){
	E[++etot]=(Edge){v,Head[u],x};
	Head[u]=etot;	
}
struct P20{
	int mx,mxd;
	void Dfs(int x,int f,int d){
		if(d>mx)mx=d,mxd=x;
		for(int i=Head[x];i;i=E[i].nxt){
			int y=E[i].to,z=E[i].val;
			if(y==f)continue;
			Dfs(y,x,d+z);	
		}
	}
	void Solve(){
		mx=0,mxd=0;
		Dfs(1,0,0);
		mx=0;
		Dfs(mxd,0,0);
		printf("%d\n",mx);	
	}
}p20;
struct Pchain{
	int A[M];
	bool Check(int x){
		int res=0;
		for(int i=1;i<n;i++){
			if(A[i]<x)A[i+1]+=x;
			else ++res;
		}
		return res>=m;
	}
	void Solve(){
		for(int i=1;i<n;i++){
			for(int y=Head[i];y;y=E[y].nxt){
				int to=E[y].to;
				if(to==i+1){
					A[i]=E[y].val;
					break;
				}
			}
		}
		int l=1,r=sum,ans;
		while(l<=r){
			int mid=l+r>>1;
			if(Check(mid))ans=mid,l=mid+1;
			else r=mid-1;	
		}
		printf("%d\n",ans);
	}
}pchain;
struct P2s{
	int cnt,lim;
	int Dp[1005][2];
	void Dfs(int x,int f){
		Dp[x][0]=Dp[x][1]=0;
		for(int i=Head[x];i;i=E[i].nxt){
			int y=E[i].to,z=E[i].val;
			if(y==f)continue;
			Dfs(y,x);
			if(Dp[y][0]+z>Dp[x][0])Dp[x][1]=Dp[x][0],Dp[x][0]=Dp[y][0]+z;
			else if(Dp[y][0]+z>Dp[x][1])Dp[x][1]=Dp[y][0]+z;
		}
		if(Dp[x][1]>=lim)cnt+=2,Dp[x][0]=0;
		else if(Dp[x][0]>=lim)++cnt,Dp[x][0]=Dp[x][1];
		else if(Dp[x][0]+Dp[x][1]>=lim)++cnt,Dp[x][0]=0;
//		printf("%d %d %d %d?\n",x,cnt,Dp[x][0],Dp[x][1]);
	}
	bool Check(int x){
		cnt=0;lim=x;
		Dfs(1,0);
		return cnt>=m;	
	}
	void Solve(){
		int l=1,r=sum,ans;
//		printf("%d?\n",Check(3));
		while(l<=r){
			int mid=l+r>>1;
			if(Check(mid))l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}p2s;
struct Pshui{
	int lim,cnt;
	int Dp[M];
	multiset<int>S[M],G[M];
	multiset<int>::iterator it,it1;
	void Dfs(int x,int f){
		S[x].clear();Dp[x]=0;G[x].clear();
		for(int i=Head[x];i;i=E[i].nxt){
			int y=E[i].to,z=E[i].val;
			if(y==f)continue;
			Dfs(y,x);
			if(Dp[y]+z>=lim){
				++cnt;
				continue;	
			}
			S[x].insert(Dp[y]+z);
		}
		if(S[x].size()==0)return;
		it=S[x].end();--it;
		it1=S[x].begin();
		while(S[x].size()>1){
			while(S[x].size()>1){
				if((*it1)+(*it)<lim){
					G[x].insert(*it1);
					S[x].erase(it1);
					it1=S[x].begin();
				}else {
					++cnt;
					break;	
				}
			}
			if(S[x].size()>1){
				S[x].erase(it1);S[x].erase(it);
				if(S[x].size()>1){
					it1=S[x].begin();
					it=S[x].end();--it;
				}
			}
		}
		if(S[x].size()>0){
			it=S[x].end();--it;
			Dp[x]=max(Dp[x],(*it));
		}
		if(G[x].size()>0){
			it=G[x].end();--it;
			Dp[x]=max(Dp[x],(*it));	
		}
//		printf("%d %d?\n",x,Dp[x]);
	}
	bool Check(int x){
		cnt=0;lim=x;
		Dfs(1,0);
		return cnt>=m;	
	}
	void Solve(){
//		printf("%d\n",Check(15));
		int l=1,r=sum,ans=0;
		while(l<=r){
			int mid=l+r>>1;
			if(Check(mid))l=mid+1,ans=mid;
			else r=mid-1;	
		}
		printf("%d\n",ans);
	}
}pshui;
int main(){
//	printf("%.4f\n",1.0*(sizeof())/1024/1024);
	freopen("track.in","r",stdin);
//	freopen("track3.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int mxdg=0;
	for(int i=1;i<n;i++){
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		Add_edge(a,b,c);	
		Add_edge(b,a,c);
		sum+=c;
		++Deg[a],++Deg[b];	
		mxdg=max(mxdg,max(Deg[a],Deg[b]));
	}
	if(m==1)p20.Solve();
	else if(mxdg<=2)pchain.Solve();
	else if(mxdg<=3)p2s.Solve();
	else pshui.Solve();
//	p2s.Solve();
//	pshui.Solve();
	return 0;
}
