#include<cstdio>
#define maxn 100005
using namespace std;

int n,ans;
int a[maxn];

int readln()
{
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while ('0'<=ch&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
} 

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=readln();
	a[0]=0;ans=0;
	for (int i=1;i<=n;i++)
	{
		a[i]=readln();
		if (a[i]>a[i-1]) ans+=a[i]-a[i-1];
	}
	printf("%d",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}
