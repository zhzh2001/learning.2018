#include<cstdio>
#include<algorithm>
#include<cstring>
#define maxn 50005
using namespace std;

int n,m,cnt,tot,ans;
int lnk[maxn],g[maxn],q[maxn];
struct edge
{
	int nxt,y,v;
} e[maxn<<1];

int readln()
{
	int x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while ('0'<=ch&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}

void add(int x,int y,int v)
{
	e[++tot].nxt=lnk[x];lnk[x]=tot;e[tot].y=y;e[tot].v=v;
	e[++tot].nxt=lnk[y];lnk[y]=tot;e[tot].y=x;e[tot].v=v;
}

bool cmp(int x,int y)
{
	return x<y;
}

int max(int x,int y)
{
	return x>y?x:y;
}

void dfs(int x,int fa,int k,int v)
{
	for (int i=lnk[x];i;i=e[i].nxt)
	{
		int y=e[i].y,val=e[i].v;
		if (y==fa) continue;
		dfs(y,x,k,val);
	}
	int ptot=0;
	for (int i=lnk[x];i;i=e[i].nxt)
	{
		int y=e[i].y;
		if (y==fa) continue;
		q[++ptot]=g[y];
	}
	sort(q+1,q+ptot+1,cmp);
	for (int i=ptot;i;i--) if (q[i]>=k) cnt++,ptot--;
	int ql=ptot,qr=ptot+1;
	for (int i=2;i<=ptot;i++) if (q[i]+q[1]>=k) {ql=i;qr=i+1;break;}
	for (int i=1;i<=ql;i++)
	{
		if (i<ql&&q[i]+q[ql]>=k) cnt++,ql--;
		else if (i<ql&&q[i]+q[ql]<k&&qr<=ptot) cnt++,qr++;
		else if (i==ql&&qr<=ptot) qr++,cnt++;
		else g[x]=max(g[x],q[i]);
	}
	cnt+=(ptot-qr+1)/2;
	if ((ptot-qr+1)%2==1) g[x]=q[ptot];
	g[x]+=v;
}

bool check(int x)
{
	cnt=0;
	memset(g,0,sizeof(g));
	dfs(1,0,x,0);
	return cnt>=m;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=readln();m=readln();
	for (int i=1;i<n;i++)
	{
		int x=readln(),y=readln(),z=readln();
		add(x,y,z);
	}
	int l=1,r=500000000,mid;
	while (l<=r)
	{
		mid=(l+r)>>1;
		if (check(mid)) ans=mid,l=mid+1; else r=mid-1;
	}
	printf("%d",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}
