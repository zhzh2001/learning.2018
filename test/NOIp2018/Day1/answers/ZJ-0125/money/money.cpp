#include<cstdio>
#include<algorithm>
#include<cstring>
#define maxn 105
#define maxm 25005
using namespace std;

int t,n,cnt;
int a[maxn];
bool vis[maxm];

int readln()
{
	int x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while ('0'<=ch&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}

bool cmp(int x,int y)
{
	return x<y;
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=readln();
	while (t--)
	{
		n=readln();
		for (int i=1;i<=n;i++) a[i]=readln();
		sort(a+1,a+n+1,cmp);
		cnt=0;
		memset(vis,false,sizeof(vis));
		vis[0]=true;
		for (int i=1;i<=n;i++)
		{
			if (!vis[a[i]])
			{
				cnt++;
				for (int j=0;j<=a[n]-a[i];j++)
				{
					if (vis[j]) vis[j+a[i]]=true;
				}
			}
		}
		printf("%d\n",cnt);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
