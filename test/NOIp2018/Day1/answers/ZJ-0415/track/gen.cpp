#include<cstdio>
#include<algorithm>
#include<math.h>
#include<vector>
#include<ctype.h>
#include<string.h>
#include<time.h>

#define ll long long
using namespace std;

#define rand() ((rand()<<15)^rand())
int main(){
	srand(time(0));
	freopen("track.in", "w", stdout);
	int n=50000, m=rand()%10000+1;
	printf("%d %d\n", n, m);
	for(int i=1; i<n; ++i) printf("%d %d %d\n", i-rand()%min(i, 10), i+1, rand()%10000+1);
	return 0;
}
