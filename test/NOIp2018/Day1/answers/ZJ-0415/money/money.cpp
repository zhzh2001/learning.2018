#include<cstdio>
#include<algorithm>
#include<math.h>
#include<vector>
#include<ctype.h>
#include<string.h>

#define ll long long
using namespace std;

const int N = 105, M = 25005;
int T, n, ans, a[N];
bool f[M];
int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	while(T--){
		scanf("%d", &n);
		for(int i=1; i<=n; ++i) scanf("%d", a+i);
		sort(a+1, a+n+1);
		ans=0, memset(f, 0, sizeof f);
		f[0]=1;
		for(int i=1; i<=n; ++i) if(!f[a[i]]){
			++ans;
			for(int j=a[i]; j<=a[n]; ++j) f[j]|=f[j-a[i]];
		}
		printf("%d\n", ans);
	}
	return 0;
}
