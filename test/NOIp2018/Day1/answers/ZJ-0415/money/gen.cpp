#include<cstdio>
#include<algorithm>
#include<math.h>
#include<vector>
#include<ctype.h>
#include<string.h>
#include<time.h>

#define ll long long
using namespace std;

#define rand() ((rand()<<15)^rand())
bool vis[25005];
int main(){
	srand(time(0));
	freopen("money.in", "w", stdout);
	int T=20, n=100, m=25000;
	printf("%d\n", T);
	while(T--){
		memset(vis, 0, sizeof vis);
		printf("%d\n", n);
		for(int i=1; i<=n; ++i){
			int x=rand()%m+1;
			while(vis[x]) x=rand()%m+1;
			printf("%d ", x);
		}
		puts("");
	}
	return 0;
}
