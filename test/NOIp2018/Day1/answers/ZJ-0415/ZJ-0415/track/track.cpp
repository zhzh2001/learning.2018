#include<cstdio>
#include<algorithm>
#include<math.h>
#include<vector>
#include<ctype.h>
#include<string.h>

#define ll long long
using namespace std;

const int N = 50005;
int n, m, num, mid, h[N], q[N], a[N], stk[N], fa[N], f[N], g[N], e[N<<1], pre[N<<1], w[N<<1];
bool vis[N];
inline void add(int x, int y, int z){ e[++num]=y, w[num]=z, pre[num]=h[x], h[x]=num;}
inline void bfs(){
	int l=0, r=1;
	q[1]=1;
	while(l<r){
		int u=q[++l];
//		printf("bfs:%d\n", u);
		for(int i=h[u]; i; i=pre[i]) if(e[i]!=fa[u]) fa[e[i]]=u, q[++r]=e[i];
	}
}
inline int solve(){
	for(int i=n; i; --i){
		int u=q[i], cnt=0;
		f[u]=g[u]=0;
		for(int j=h[u]; j; j=pre[j]) if(e[j]!=fa[u]) a[++cnt]=g[e[j]]+w[j], f[u]+=f[e[j]];
		sort(a+1, a+cnt+1);
		while(cnt && a[cnt]>=mid) ++f[u], --cnt;
		
		int p=cnt, top=0;
		memset(vis+1, 0, cnt);
		for(int j=1; j<=cnt; ++j) if(!vis[j]){
			while(p && a[p]+a[j]>=mid){
				if(!vis[p]) stk[++top]=p;
				--p;
			}
			while(top && (stk[top]<=j || a[stk[top]]+a[j]<mid)) --top;
			if(top) vis[j]=vis[stk[top]]=1, ++f[u], --top;
			else g[u]=a[j];
		}
//		printf("solve:%d %d %d\n", u, f[u], g[u]);
//		for(int i=1; i<=cnt; ++i) printf("[%d]", a[i]); puts("");
	}
	return f[1];
}
int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i=1; i<n; ++i){
		static int x, y, z;
		scanf("%d%d%d", &x, &y, &z);
		add(x, y, z), add(y, x, z);
	}
	bfs();
//	mid=15, printf(">>%d\n", solve());
//	return 0;
	int l=1, r=500000000, ans=1;
	while(l<=r){
		mid=(l+r)>>1;
		if(solve()>=m) ans=mid, l=mid+1; else r=mid-1;
	}
	return printf("%d", ans), 0;
}
