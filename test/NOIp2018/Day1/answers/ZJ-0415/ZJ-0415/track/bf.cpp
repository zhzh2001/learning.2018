#include<cstdio>
#include<algorithm>
#include<math.h>
#include<vector>
#include<ctype.h>
#include<string.h>
#include<set>

#define ll long long
using namespace std;

const int N = 50005;
int n, m, num, mid, h[N], q[N], a[N], stk[N], fa[N], f[N], g[N], e[N<<1], pre[N<<1], w[N<<1];
bool vis[N];
inline void add(int x, int y, int z){ e[++num]=y, w[num]=z, pre[num]=h[x], h[x]=num;}
inline void bfs(){
	int l=0, r=1;
	q[1]=1;
	while(l<r){
		int u=q[++l];
//		printf("bfs:%d\n", u);
		for(int i=h[u]; i; i=pre[i]) if(e[i]!=fa[u]) fa[e[i]]=u, q[++r]=e[i];
	}
}
inline int calc(int x, int cnt){
	set<pair<int,int> > s;
	s.clear();
	int ans=0;
	for(int i=1; i<=cnt; ++i) if(i!=x) s.insert(make_pair(a[i], i));
	for(int i=1; i<=cnt; ++i) if(i!=x && s.count(make_pair(a[i], i))){
		set<pair<int,int> >::iterator it=s.lower_bound(make_pair(mid-a[i], 0));
		if(it!=s.end()){
			if(it->second==i) ++it;
			if(it!=s.end()) ++ans, s.erase(it), s.erase(make_pair(a[i], i));
		}
	}
	return ans;
}
inline int solve(){
	for(int i=n; i; --i){
		int u=q[i], cnt=0;
		f[u]=g[u]=0;
		for(int j=h[u]; j; j=pre[j]) if(e[j]!=fa[u]) a[++cnt]=g[e[j]]+w[j], f[u]+=f[e[j]];
		sort(a+1, a+cnt+1);
		while(cnt && a[cnt]>=mid) ++f[u], --cnt;
		
		pair<int,int> tmp=make_pair(calc(0, cnt), 0);
		for(int i=1; i<=cnt; ++i) tmp=max(tmp, make_pair(calc(i, cnt), a[i]));
		f[u]+=tmp.first, g[u]=tmp.second;
	}
	return f[1];
}
int main(){
	freopen("track.in", "r", stdin);
	freopen("bf.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i=1; i<n; ++i){
		static int x, y, z;
		scanf("%d%d%d", &x, &y, &z);
		add(x, y, z), add(y, x, z);
	}
	bfs();
//	mid=31, printf(">>%d\n", solve());
//	return 0;
	int l=1, r=500000000, ans=1;
	while(l<=r){
		mid=(l+r)>>1;
		if(solve()>=m) ans=mid, l=mid+1; else r=mid-1;
	}
	return printf("%d", ans), 0;
}
