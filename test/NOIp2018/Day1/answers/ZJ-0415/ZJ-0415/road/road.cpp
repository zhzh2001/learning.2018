#include<cstdio>
#include<algorithm>
#include<math.h>
#include<vector>
#include<ctype.h>
#include<string.h>

#define ll long long
using namespace std;

const int N = 100005;
int n, ans, a[N];
int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for(int i=1; i<=n; ++i) scanf("%d", a+i), ans+=(a[i]-a[i-1]>=0?a[i]-a[i-1]:a[i-1]-a[i]);
	return printf("%d", (ans+a[n])/2), 0;
}
