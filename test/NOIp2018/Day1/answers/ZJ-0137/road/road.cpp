#include <bits/stdc++.h>
using namespace std;
inline int read(){int s=0; char c; while(!isdigit(c))c=getchar(); while(isdigit(c)){s=s*10+c-'0';c=getchar();} return s;}
const int N=100005;
int n,a[N];
typedef pair<int,int>P;
struct segtree
{
	int l,r,mi,miid,laz;
	inline int mid(){return (l+r)>>1;}
}Tree[N<<2];
#define c1 (x<<1)
#define c2 (x<<1|1)
inline void Up(int x)
{
	Tree[x].miid=(Tree[c1].mi<=Tree[c2].mi)?Tree[c1].miid:Tree[c2].miid;
	Tree[x].mi=min(Tree[c1].mi,Tree[c2].mi);
}
inline void Ad(int x,int v){Tree[x].mi+=v; Tree[x].laz+=v;}
inline void Down(int x){Ad(c1,Tree[x].laz); Ad(c2,Tree[x].laz); Tree[x].laz=0;}
inline void build(int l,int r,int x)
{
	Tree[x].l=l; Tree[x].r=r;
	if(l==r){Tree[x].mi=a[l];Tree[x].miid=l;return;}
	int mid=(l+r)>>1; build(l,mid,c1); build(mid+1,r,c2); Up(x);
}
inline int que_mi(int l,int r,int x)
{
	if(Tree[x].l==l&&Tree[x].r==r){return Tree[x].mi;}
	if(Tree[x].laz)Down(x); int mid=Tree[x].mid();
	if(r<=mid)return que_mi(l,r,c1); else if(l>mid)return que_mi(l,r,c2);
	else return min(que_mi(l,mid,c1),que_mi(mid+1,r,c2)); Up(x);
}
inline pair<int,int> que_po(int l,int r,int x)
{
	if(Tree[x].l==l&&Tree[x].r==r){return P(Tree[x].mi,Tree[x].miid);} if(Tree[x].laz)Down(x); int mid=Tree[x].mid();
	if(r<=mid)return que_po(l,r,c1);else if(l>mid)return que_po(l,r,c2);
	else {P p1=que_po(l,mid,c1),p2=que_po(mid+1,r,c2); if(p1.first<=p2.first)return p1;else return p2;} Up(x);
}
inline void ins(int l,int r,int x,int v)
{
	if(Tree[x].l==Tree[x].r){Ad(x,v);return;} if(Tree[x].laz)Down(x); int mid=Tree[x].mid();
	if(r<=mid)ins(l,r,c1,v);else if(l>mid)ins(l,r,c2,v);else ins(l,mid,c1,v),ins(mid+1,r,c2,v); Up(x);
}
inline int que(int po,int x)
{
	if(Tree[x].l==Tree[x].r){return Tree[x].mi;} if(Tree[x].laz)Down(x); int mid=Tree[x].mid();
	if(po<=mid)return que(po,c1);else return que(po,c2); Up(x);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int i,l,mi,po;long long re=0LL; n=read();
	for(i=1;i<=n;i++)
	{
		a[i]=read();
	}a[n+1]=0; build(1,n+1,1); l=1;
	while(true)
	{
		if(l>n)break;
		po=que_po(l,n+1,1).second;
		mi=que_mi(l,po-1,1);
		ins(l,po-1,1,-mi);
		re+=1LL*mi;
		while(que(l,1)==0&&l<=n+1)l++;
	}printf("%lld\n",re);
}
