#include <bits/stdc++.h>
using namespace std;
inline int read()
{
	int f=0,s=0; char c;
	while(!isdigit(c)){f|=(c=='-');c=getchar();}
	while(isdigit(c)){s=s*10+c-'0';c=getchar();}
	return (f)?(-s):(s);
}
const int N=105,M=250005;;
int T,n,a[N],inf=0x3f3f3f3f,num[N], Ma=0;
bool flg[M];
inline int gcd(int x,int y){return (!y)?(x):(gcd(y,x%y));}
inline bool jud(int x)
{
	int i,j,k,pp;
	for(i=1;i<=*num;i++)if(num[i]<=x)
	{
		for(j=x;j>=num[i];j--)
		{
			for(k=1;k*num[i]<=j;k++)
			{
				flg[j]|=flg[j-k*num[i]];
			}
		}
	}return flg[x];
}
inline int solve(int po1,int po2)
{
	int i,pp,re=n,up; for(i=0;i<=Ma;i++)flg[i]=0;
	up=a[po1]*a[po2]-a[po1]-a[po2];
	*num=0; num[++*num]=a[po1]; num[++*num]=a[po2];
	flg[0]=flg[num[1]]=flg[num[2]]=1;
	for(i=n;i>=1;i--){if(a[i]>up){if(i!=po1&&i!=po2)re--;}else break;}
	for(i=1;i<=n&&a[i]<=up;i++)if((i!=po1)&&(i!=po2))
	{
		if(!jud(a[i])){num[++*num]=a[i];flg[num[*num]]=1;}else re--;
	}return re;
}
inline bool check(){int i; for(i=2;i<=n;i++)if(a[i]%a[1])return 0; return 1;}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int i,j,re,bo; T=read();
	while(T--)
	{
		n=read(); re=n; Ma=0; bo=0;
		for(i=1;i<=n;i++)
		{
			a[i]=read(); Ma=max(Ma,a[i]);
		}sort(a+1,a+n+1); *num=0;
		if(n==1){if(a[1])puts("1");else puts("0");continue;}
		if(n==2){if(gcd(a[1],a[2])==1)puts("2");else puts("1");continue;}
		if(check()){puts("1");continue;}
		if(n<=25)
		{
			for(i=1;i<n;i++)
			{
				for(j=i+1;j<=n;j++)if(gcd(a[i],a[j])==1)
				{
					re=min(re,solve(i,j)); bo=1;
				}
			}if(!bo)re=min(re,solve(1,2)); printf("%d\n",re);
		}
		else
		{
			int lai=0,laj=0,up=inf;
			for(i=1;i<n;i++)
			{
				for(j=i+1;j<=n;j++)if(gcd(a[i],a[j])==1&&(up>a[i]*a[j]-a[i]-a[j]))
				{
					lai=i; laj=j; up=a[i]*a[j]-a[i]-a[j];
				}
			}
			if(!lai)
			{
				if(check()){puts("1");continue;}else {re=solve(1,2);}
			}else re=solve(lai,laj); printf("%d\n",re);
		}
	}
}
