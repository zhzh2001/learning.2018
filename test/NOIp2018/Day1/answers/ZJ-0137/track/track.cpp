#include <bits/stdc++.h>
using namespace std;
const int N=50005,M=50005;
int n,m,tot=0,Next[M],to[M],val[M],head[M],f[N],max_lj,jj[M],dis[M];
inline void add(int x,int y,int z){Next[++tot]=head[x]; to[tot]=y; val[tot]=z; head[x]=tot;}
//0:��
//1:·��
inline void dfs(int x,int la)
{
	int i,ma=0; f[x]=0;
	for(i=head[x];i;i=Next[i])if(to[i]!=la)
	{
		dfs(to[i],x); max_lj=max(max_lj,ma+f[to[i]]+val[i]); ma=max(ma,f[to[i]]+val[i]);
	}f[x]=ma; return;
}
inline void solve1()
{
	max_lj=0; dfs(1,0); printf("%d\n",max_lj); exit(0);
}
inline void solve2()
{
	int i; for(i=2;i<=tot;i+=2)jj[i/2]=val[i]; tot/=2; sort(jj+1,jj+tot+1); reverse(jj+1,jj+tot+1); printf("%d\n",jj[m]); exit(0);
}
inline bool jud(int mid)
{
	int i,pp=0,la=0;
	for(i=1;i<=tot;i++)
	{
		if(la+jj[i]>=mid){la=0;pp++;}else la+=jj[i];
	}return (pp>=m)?1:0;
}
inline void solve3()
{
	int i,l=0,r=1e9,mid,tmp; for(i=2;i<=tot;i+=2)jj[i/2]=val[i]; tot/=2;
	while(l<=r)
	{
		mid=(l+r)>>1; if(jud(mid)){tmp=mid;l=mid+1;}else r=mid-1;
	}printf("%d\n",tmp); exit(0);
}
inline void solve4()
{
	srand(time(NULL)); int i,l=0,r=1e9,mid,tmp; for(i=2;i<=tot;i+=2)jj[i/2]=val[i]; tot/=2; random_shuffle(jj+1,jj+tot+1);
	while(l<=r)
	{
		mid=(l+r)>>1; if(jud(mid)){tmp=mid;l=mid+1;}else r=mid-1;
	}printf("%d\n",tmp); exit(0);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int i,x,y,z,jht=1,lian=1; scanf("%d%d",&n,&m);
	for(i=1;i<n;i++)
	{
		scanf("%d%d%d",&x,&y,&z); add(x,y,z); add(y,x,z); if(x!=1)jht=0; if(y!=x+1)lian=0;
	}if(m==1)solve1(); if(jht)solve2(); if(lian)solve3(); solve4();
}
