#include <fstream>
#include <algorithm>
#include <cstring>
#include <iostream>

using namespace std;

const int N = 101;
const int M = 25001;

int n, ans;
int a[N];
int dp[M];

int main() {
   ifstream fin("money.in");
   ofstream fout("money.out");
   fin.sync_with_stdio(false);
   fout.sync_with_stdio(false);
   int T;
   fin >> T;
   while (T--) {
      fin >> n;
      ans = 1;
      memset(dp, 0, sizeof(dp));
      for (int i = 0; i < n; i++) fin >> a[i];
      sort(a, a + n);
      for (int i = 0; i < n - 1; i++) {
         for (int j = 0; j < M; j++) {
            if (j - a[i] >= 0) dp[j] = max(dp[j], dp[j - a[i]] + a[i]);
         }
         if (dp[a[i + 1]] != a[i + 1]) ans++; //, cout << a[i + 1] << endl;
      }
      fout << ans << endl;
   }
}
