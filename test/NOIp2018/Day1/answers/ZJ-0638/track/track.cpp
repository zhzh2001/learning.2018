#include <fstream>
#include <vector>
#include <set>

using namespace std;

const int N = 50000;

struct uv {
   int u, v;
   uv() {}
   uv(int a, int b) { u = a, v = b; }
   bool operator<(const uv &x) {
      return v < x.v;
   }
};

int n, m;
vector<uv> g[N];

namespace case1 {
   bool check(int k) {
      int ans = 0;
      multiset<int> s;
      for (int _ = 0; _ < g[0].size(); _++) {
         uv &i = g[0][_];
         if (i.v >= k) ans++;
         else s.insert(i.v);
      }
      while (!s.empty()) {
         multiset<int>::iterator iter = s.lower_bound(k - *s.begin());
         if (iter == s.end()) {
            s.erase(s.begin());
            continue;
         }
         if (iter == s.begin()) break;
         ans++;
         s.erase(iter);
         s.erase(s.begin());
      }
      return ans >= m;
   }
   int solve() {
      int l = 0, r = 1e9;
      while (l + 1 != r) {
         int m = (l + r) >> 1;
         if (check(m)) l = m;
         else r = m;
      }
      return l;
   }
}

namespace case2 {
   int d[N], f[N];
   void dfs(int p) {
      for (int _ = 0; _ < g[p].size(); _++) {
         uv &i = g[p][_];
         if (f[p] == i.u) continue;
         d[i.u] = d[p] + i.v;
         f[i.u] = p;
         dfs(i.u);
      }
   }
   int solve() {
      dfs(0);
      int mx = 0, mp;
      for (int i = 0; i < n; i++) if (mx < d[i]) mp = i, mx = d[i];
      f[mp] = mp;
      d[mp] = 0;
      dfs(mp);
   }
}

int main() {
   ifstream fin("track.in");
   ofstream fout("track.out");
   fin.sync_with_stdio(false);
   fout.sync_with_stdio(false);
   fin >> n >> m;
   bool flag = true;
   for (int i = 1, a, b, c; i < n; i++) {
      fin >> a >> b >> c;
      if (a != 1 && b != 1) flag = false;
      a--, b--;
      g[a].push_back(uv(b, c));
      g[b].push_back(uv(a, c));
   }
   if (flag) fout << case1::solve() << endl;
   else fout << case2::solve() << endl;
}
