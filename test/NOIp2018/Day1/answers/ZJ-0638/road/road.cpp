#include <fstream>
#include <queue>
#include <functional>
#include <vector>
#include <iostream>

using namespace std;

const int N = 100001;
const int M = 1 << 17;

template<typename T, typename U>
T &checkmin(T &a, const U &b) {
   if (b < a) a = b;
   return a;
}

struct uv {
   int u, v;
   uv() { u = -1, v = 1 << 30; }
   uv(int a, int b) { u = a, v = b; }
   bool operator<(const uv &x) const {
      return v < x.v;
   }
};

int a[N];
uv seg[2 * M];

void set(int p, int v) {
   p += M;
   seg[p] = uv(p - M, v);
   for (p >>= 1; p; p >>= 1) seg[p] = min(seg[p << 1], seg[p << 1 | 1]);
}

int getpos(int l, int r) {
   uv ans;
   for (l += M, r += M; l ^ r ^ 1; l >>= 1, r >>= 1) {
      if (~l & 1) checkmin(ans, seg[l ^ 1]);
      if (r & 1) checkmin(ans, seg[r ^ 1]);
   }
   //cout << ans.u << " " << ans.v << endl;
   return ans.u;
}

int solve(int l, int r, int h) {
   if (l > r) return 0;
   int p = getpos(l - 1, r + 1);
   return solve(l, p - 1, a[p]) + solve(p + 1, r, a[p]) + a[p] - h;
}

int main() {
   ifstream fin("road.in");
   ofstream fout("road.out");
   fin.sync_with_stdio(false);
   fout.sync_with_stdio(false);
   // for (int i = 0; i < 2 * M; i++) seg[i] = uv();
   int n;
   fin >> n;
   for (int i = 1, x; i <= n; i++) fin >> a[i], set(i, a[i]);
   // for (int i = 1; i <= n; i++) getpos(i - 1, i + 1);
   fout << solve(1, n, 0) << endl;
}
