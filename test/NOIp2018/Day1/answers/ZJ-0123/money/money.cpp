#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define fi first
#define se second
#define pb push_back
typedef long long ll;
typedef unsigned long long ull;
int n;
int a[110];
bool ok[25010];
int an;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int i,j,T;
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);an=n;
		for(i=1;i<=n;++i)
			scanf("%d",a+i);
		sort(a+1,a+n+1);
		memset(ok,0,sizeof(ok));
		ok[0]=1;
		for(i=1;i<=n;++i)
		{
			if(ok[a[i]])	{--an;continue;}
			for(j=a[i];j<=25000;++j)
				ok[j]|=ok[j-a[i]];
		}
		printf("%d\n",an);
	}
	return 0;
}
