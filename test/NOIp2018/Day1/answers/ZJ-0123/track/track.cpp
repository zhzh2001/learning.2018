#include<cstdio>
#include<algorithm>
#include<cstring>
#include<set>
using namespace std;
#define fi first
#define se second
#define pb push_back
typedef long long ll;
typedef unsigned long long ull;
/*
struct pii
{
	ll fi,se;
};
bool operator<(const pii &a,const pii &b)
{
	return a.fi<b.fi||(a.fi==b.fi&&a.se<b.se);
}
*/
struct E
{
	ll to,nxt,d;
}e[100010];
ll f1[50010],ne;
struct E1
{
	ll a,b,c;
}e1[50010];
ll n,m;
namespace sol1
{
	ll an1[50010],ans;
	void dfs(ll u,ll fa)
	{
		ll k,v;
		ll t1=0,t2=0;
		for(k=f1[u];k;k=e[k].nxt)
		{
			v=e[k].to;
			if(v!=fa)
			{
				dfs(v,u);
				if(an1[v]+e[k].d>=t1)
				{
					t2=t1;
					t1=an1[v]+e[k].d;
				}
				else if(an1[v]+e[k].d>=t2)
					t2=an1[v]+e[k].d;
			}
		}
		an1[u]=t1;
		ans=max(ans,t1+t2);
	}
	void main()
	{
		dfs(1,0);
		printf("%lld\n",ans);
	}
}
namespace sol2
{
	ll d1[50100];
	//\\d1[i]表示(1,i)的长度
	multiset<ll> s;
	bool judge(ll x)//所有路径长度>=x
	{
		ll i,t1;
		multiset<ll>::iterator i1;
		s.clear();
		for(i=2;i<=n;++i)
			s.insert(d1[i]);
		ll num=0;
		while(!s.empty())
		{
			i1=s.end();--i1;
			t1=*i1;s.erase(i1);
			if(t1>=x)	{++num;continue;}
			i1=s.lower_bound(x-t1);
			if(i1==s.end())	break;
			s.erase(i1);
			++num;
		}
		return num>=m;
	}
	void main()
	{
		ll l=1,r=500000000,mid;
		while(l!=r)
		{
			mid=l+((r-l)>>1);
			if(judge(mid+1))	l=mid+1;
			else	r=mid;
		}
		printf("%lld\n",l);
	}
}
namespace sol3
{
	ll d1[50100];//d1[i]表示(i,i+1)长度
	bool judge(ll x)
	{
		ll num=0,i,j,t;
		for(i=1;i<=n-1;i=j+1)
		{
			t=0;
			for(j=i;j<=n-1;++j)
			{
				t+=d1[j];
				if(t>=x)	{++num;break;}
			}
		}
		return num>=m;
	}
	void main()
	{
		ll l=1,r=500000000,mid;
		while(l!=r)
		{
			mid=l+((r-l)>>1);
			if(judge(mid+1))	l=mid+1;
			else	r=mid;
		}
		printf("%lld\n",l);
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	bool tag2=1,tag3=1;
	ll i,x,y,z;
	scanf("%lld%lld",&n,&m);
	for(i=1;i<=n-1;++i)
	{
		scanf("%lld%lld%lld",&x,&y,&z);
		e[++ne].to=y;e[ne].nxt=f1[x];f1[x]=ne;e[ne].d=z;
		e[++ne].to=x;e[ne].nxt=f1[y];f1[y]=ne;e[ne].d=z;
		e1[i].a=x;e1[i].b=y;e1[i].c=z;
		if(x!=1)	tag2=0;
		sol2::d1[y]=z;
		if(y!=x+1)	tag3=0;
		sol3::d1[x]=z;
	}
	if(m==1)	{sol1::main();return 0;}
	if(tag2)	{sol2::main();return 0;}
	if(tag3)	{sol3::main();return 0;}
	return 0;
}
