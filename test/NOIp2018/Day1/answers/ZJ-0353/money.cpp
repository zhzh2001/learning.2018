#include<bits/stdc++.h>
using namespace std;
int T,maxn,mi1,mi2,a[105],bo[25005],ans,n;
int main()
{
freopen("money.in","r",stdin);
freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		maxn=mi1=mi2=ans=0;
		scanf("%d",&n);
		for (int i=1;i<=n;i++) {scanf("%d",&a[i]);maxn=max(maxn,a[i]);}
		sort(a+1,a+n+1);
		for (int i=1;i<=maxn;i++) bo[i]=0; bo[0]=1;
		for (int i=1;i<=n;i++)
		{
			if (bo[a[i]]==0)
			{
				if (mi1==0) mi1=a[i];
				else if (mi2==0) {mi2=a[i];maxn=min(maxn,mi1*mi2-mi1-mi2);}
				else if (a[i]>maxn) continue;
				for (int j=0;j<=maxn-a[i];j++) if (bo[j]==1) bo[j+a[i]]=1;
				ans++;				
			}			
		}
		printf("%d\n",ans);		
	}	
	return 0;
}
