#include<bits/stdc++.h>
using namespace std;
const int inf=0x3f3f3f3f;
int n,nn,flag1,flag2,x[50005],y[50005],z[50005],l,r,mid,ans,a[50005],t;
int w,xx[100005],yy[100005],zz[100005],b[50005],bo[50005],d[50005],tt[50005],ne[100005];
int mi1,mi2,mi3,mini1,mini2,de[50005];
int f[1005][1005],fa[1005],ls[1005],rs[1005],lss[1005],rss[1005],flag3;
void bfs()
{
	int t=1,w=1; b[1]=1; bo[1]=1; d[1]=0;
	while (t<=w)
	{
		int kk=tt[b[t]];
		while (kk!=0)
		{
			if (bo[yy[kk]]==0)
			{
				bo[yy[kk]]=1; d[yy[kk]]=d[b[t]]+zz[kk]; 
				w++; b[w]=yy[kk];			
			}			
			kk=ne[kk];
		}
		t++;
	}
	int maxi=1;
	for (int i=2;i<=n;i++) if (d[maxi]<d[i]) maxi=i;
	bo[maxi]=0; d[maxi]=0; t=w=1; b[1]=maxi;
	while (t<=w)
	{
		int kk=tt[b[t]];
		while (kk!=0)
		{
			if (bo[yy[kk]]==1)
			{
				bo[yy[kk]]=0; d[yy[kk]]=d[b[t]]+zz[kk]; 
				w++; b[w]=yy[kk];			
			}			
			kk=ne[kk];
		}
		t++;
	}
	for (int i=1;i<=n;i++) ans=max(ans,d[i]);	
}
bool pd1(int k)
{
	int w=n-1,t=1,g=0;
	while (g<nn)
	{
		if (t>w) return false;
		if (z[w]>=k) {g++; w--;}
		else 
		{
			while (z[w]+z[t]<k&&t<w) t++;
			if (t>=w) return false;
			w--; t++; g++;
		}		
	}
	return true;	
}
bool pd2(int k)
{
	int su=0,g=0;
	for (int i=1;i<n;i++)
	{
		su+=a[i];
		if (su>=k) {su=0; g++;}	
	}
	return g>=nn;
}
void dfs(int k)
{
	int kk=tt[k];
	while (kk!=0)
	{
		if (yy[kk]!=fa[k])
		{
			if (ls[k]==0) {ls[k]=yy[kk];lss[k]=zz[kk];}
			 else {rs[k]=yy[kk];rss[k]=zz[kk];}
			fa[yy[kk]]=k; dfs(yy[kk]);		
		}
		kk=ne[kk];
	}	
}
void dfss(int k,int v)
{
	if (ls[k]==0) {f[k][0]=0; return;}
	if (rs[k]==0)
	{
		dfss(ls[k],v);
		for (int i=1;i<=nn;i++)
			if (f[ls[k]][i-1]>=0&&f[ls[k]][i-1]+lss[k]>=v) f[k][i]=max(f[k][i],0);
		for (int i=0;i<=nn;i++) 
			if (f[ls[k]][i]>=0)
				f[k][i]=max(f[k][i],f[ls[k]][i]+lss[k]);		
		return;
	}
	dfss(ls[k],v);
	dfss(rs[k],v);
	for (int i=1;i<=nn;i++)
		for (int j=0;j<i;j++)
		if (f[ls[k]][i-j-1]>=0&&f[rs[k]][j]>=0&&f[ls[k]][i-j-1]+lss[k]>=v) f[k][i]=max(f[k][i],f[rs[k]][j]+rss[k]);
	for (int i=1;i<=nn;i++)
		for (int j=0;j<i;j++)
		if (f[rs[k]][i-j-1]>=0&&f[ls[k]][j]>=0&&f[rs[k]][i-j-1]+rss[k]>=v) f[k][i]=max(f[k][i],f[ls[k]][j]+lss[k]);
	for (int i=0;i<=nn;i++) 
		for (int j=0;j<=i;j++)
		if (f[ls[k]][i]>=0&&f[rs[k]][j]>=0)
			f[k][i]=max(f[k][i],max(f[ls[k]][i-j]+lss[k],f[rs[k]][j]+rss[k]));		
	for (int i=1;i<=nn;i++)
		for (int j=0;j<i;j++)
			if (f[ls[k]][i-j-1]>=0&&f[rs[k]][j]>=0&&f[ls[k]][i-j-1]+lss[k]+f[rs[k]][j]+rss[k]>=v) f[k][i]=max(f[k][i],0);
}
bool pd3(int k)
{
	for (int i=1;i<=n;i++)
		for (int j=0;j<=nn;j++)
			f[i][j]=-1;
	dfss(1,k);
	if (f[1][nn]>=0) return true;
	return false;	
}
int main()
{
freopen("track.in","r",stdin);
freopen("track.out","w",stdout);
	flag1=flag2=flag3=1;
	scanf("%d%d",&n,&nn);
	for (int i=1;i<n;i++)
	{
		scanf("%d%d%d",&x[i],&y[i],&z[i]);
		de[x[i]]++; de[y[i]]++;
		if (de[x[i]]>3) flag3=0;
		if (de[y[i]]>3) flag3=0;
		if (x[i]>y[i]) {t=x[i]; x[i]=y[i]; y[i]=t;}
		if (x[i]!=1) flag1=0;
		if (y[i]!=x[i]+1) flag2=0;
		r+=z[i];
	}
	if (nn==1)
	{
		for (int i=1;i<n;i++)
		{
			w++; xx[w]=x[i]; yy[w]=y[i]; zz[w]=z[i];
			w++; xx[w]=y[i]; yy[w]=x[i]; zz[w]=z[i];
		}
		for (int i=1;i<=w;i++) {ne[i]=tt[xx[i]]; tt[xx[i]]=i;}
		bfs();		
		printf("%d\n",ans);
		return 0;
	}
	if (flag1==1)
	{
		sort(z+1,z+n);
		l=1;
		while (l<=r)
		{
			mid=(l+r)/2;
			if (pd1(mid)) {ans=mid;l=mid+1;}
			else r=mid-1;			
		}		
		printf("%d\n",ans);
		return 0;
	}
	if (flag2==1)
	{		
		for (int i=1;i<n;i++) a[x[i]]=z[i];
		l=1;
		while (l<=r)
		{
			mid=(l+r)/2;
			if (pd2(mid)) {ans=mid;l=mid+1;}
			else r=mid-1;			
		}		
		printf("%d\n",ans);
		return 0;		
	}	
	if (nn==n-1)
	{
		ans=inf;
		for (int i=1;i<n;i++) ans=min(ans,z[i]);
		printf("%d\n",ans);
		return 0;
	}
	if (nn==n-2)
	{
		mi1=mi2=mi3=inf;
		for (int i=1;i<n;i++)
		{
			if (z[i]<=mi1) {mi3=mi2; mi2=mi1; mini2=mini1; mi1=z[i]; mini1=i;}
			else if (z[i]<=mi2) {mi3=mi2; mi2=z[i]; mini2=i;}
			else mi3=min(mi3,z[i]);		
		}
		if (x[mini1]==x[mini2]||y[mini1]==x[mini2]||x[mini1]==y[mini2]||y[mini1]==y[mini2])
		printf("%d\n",min(mi3,mi2+mi1));
		else printf("%d\n",mi2);
		return 0;
	}
	if (flag3==1)
	{
		for (int i=1;i<n;i++)
		{
			w++; xx[w]=x[i]; yy[w]=y[i]; zz[w]=z[i];
			w++; xx[w]=y[i]; yy[w]=x[i]; zz[w]=z[i];
		}
		for (int i=1;i<=w;i++) {ne[i]=tt[xx[i]]; tt[xx[i]]=i;}
		dfs(1);
		l=1; 
		while (l<=r)
		{
			mid=(l+r)/2;
			if (pd3(mid)) {l=mid+1; ans=mid;}
			else r=mid-1;			
		}		
		printf("%d\n",ans);
		return 0;
	}
	printf("%d\n",r/nn);
	return 0;
}
