#include<bits/stdc++.h>
using namespace std;

int read() {
	int x=0,f=1;char ch=getchar();
	for(;ch<'0' || ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0' && ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}

const int N=100+7,M=25000+7;
int n,A[N],Ans[N],cnt;
int F[M];

int DP(int x) {
	memset(F,0,sizeof(F));F[0]=1;
	for(int i=1;i<=cnt;++i){
		for(int j=Ans[i];j<=x;++j)F[j]|=F[j-Ans[i]];
	}
	return F[x];
}

int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(int T=read();T--;){
		n=read();
		for(int i=1;i<=n;++i)A[i]=read();
		sort(A+1,A+n+1);
		cnt=0;
		for(int i=1;i<=n;++i)if(!DP(A[i]))Ans[++cnt]=A[i];
		printf("%d\n",cnt);
	}
	return 0;
}
