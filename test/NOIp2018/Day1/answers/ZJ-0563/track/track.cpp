#include<bits/stdc++.h>
using namespace std;

int read() {
	int x=0,f=1;char ch=getchar();
	for(;ch<'0' || ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0' && ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}

const int N=5e4+7;
int n,m,Sum=0;
struct Edge{
	int x,y,w;
}E[N];
bool flag1=1,flag2=1;

int to[N<<1],ne[N<<1],head[N],cnt=0,len[N<<1];
void AddEdge(int x,int y,int w) {
	to[++cnt]=y;ne[cnt]=head[x];head[x]=cnt;len[cnt]=w;
}

namespace Subtask1 {
	int A[N];
	bool check(int x) {
		int qwq=lower_bound(A+1,A+n,x)-A,L=1,R=qwq-1;
		qwq=n-qwq;
		for(;L<R;--R) {
			while(L<R && A[L]+A[R]<x)++L;
			if(L>=R)break;
			++L;++qwq;
		}
		return (qwq>=m);
	}
	void Solve() {
		for(int i=1;i<n;++i)A[i]=E[i].w;
		sort(A+1,A+n);
		int L=1,R=Sum,ans=1;
		while(L<=R) {
			int mid=(L+R)>>1;
			if(check(mid))ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}
namespace Subtask2 {
	int sum[N];
	bool check(int x) {
		int qwq=-1;
		for(int i=0;i<n;i=lower_bound(sum+1,sum+n,sum[i]+x)-sum)++qwq;
		return (qwq>=m);
	}
	void Solve() {
		for(int i=1;i<n;++i)sum[i]=sum[i-1]+E[i].w;
		int L=1,R=Sum,ans=1;
		while(L<=R) {
			int mid=(L+R)>>1;
			if(check(mid))ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}
namespace Subtask3 {
	int ans=0;
	int DFS(int x,int fa) {
		int Fir=0,Sec=0;
		for(int i=head[x];i;i=ne[i])if(to[i]!=fa) {
			int v=to[i],w=len[i];
			int nxt=DFS(v,x)+w;
			if(nxt>Fir)Sec=Fir,Fir=nxt;
			else if(nxt>Sec)Sec=nxt;
		}
		ans=max(ans,Fir+Sec);
		return Fir;
	}
	void Solve() {
		for(int i=1;i<n;++i){
			AddEdge(E[i].x,E[i].y,E[i].w);
			AddEdge(E[i].y,E[i].x,E[i].w);
		}
		DFS(1,0);
		printf("%d\n",ans);
	}
}
namespace Subtask4 {
	const int maxn=1e3+7;
	int deg[maxn],fw[maxn],root;
	int F[maxn][maxn],siz[maxn];
	bool CheckFlag() {
		if(n>1000)return 0;
		for(int i=1;i<n;++i)++deg[E[i].x],++deg[E[i].y];
		for(int i=1;i<=n;++i)if(deg[i]>3)return 0;
		return 1;
	}
	void PreDFS(int x,int f) {
		for(int i=head[x];i;i=ne[i])if(to[i]!=f) {
			int v=to[i],w=len[i];
			fw[v]=w;PreDFS(v,x);
		}
	}
	void DFS(int x,int f,int mid) {
		siz[x]=0;F[x][0]=fw[x];
		if(deg[x]==1)return;
		if(deg[x]==2) {
			int v;
			for(int i=head[x];i;i=ne[i])if(to[i]!=f)v=to[i],DFS(to[i],x,mid);
			siz[x]=siz[v];
			for(int i=1;i<=siz[v];++i)F[x][i]=F[v][i]+fw[x];
			if(F[v][siz[v]]>=mid)siz[x]=siz[v]+1,F[x][siz[x]]=fw[x];
		} else {
			int v1=0,v2=0;
			for(int i=head[x];i;i=ne[i])if(to[i]!=f){
				if(!v1)v1=to[i];else v2=to[i];
				DFS(to[i],x,mid);
			}
			siz[x]=siz[v1]+siz[v2];
			for(int i=0;i<=siz[x];++i)F[x][i]=fw[x];
			for(int i=0;i<=siz[v1];++i)F[x][i+siz[v2]]=max(F[x][i+siz[v2]],F[v1][i]);
			for(int i=0;i<=siz[v2];++i)F[x][i+siz[v1]]=max(F[x][i+siz[v1]],F[v2][i]);
			for(int i=siz[x]-1;i>=0;--i)F[x][i]=max(F[x][i],F[x][i+1]);
			if(F[v1][siz[v1]]+F[v2][siz[v2]]>=mid){
				++siz[x];F[x][siz[x]]=fw[x];
			}
		}
	}
	bool check(int x) {
		memset(F,0,sizeof(F));
		if(deg[root]==3) {
			for(int i=head[root];i;i=ne[i])DFS(to[i],root,x);
			siz[root]=0;
			int v[3],tot=0;
			for(int i=head[root];i;i=ne[i])v[tot++]=to[i],siz[root]+=siz[to[i]];
			bool flag=0;
			for(int i=0;i<3;++i){
				for(int j=i+1;j<3 && i+j<=3;++j)
				if(F[v[i]][siz[v[i]]]+F[v[j]][siz[v[j]]]>=x){
					++siz[root];flag=1;break;
				}
				if(flag)break;
			}
		} else DFS(root,0,x);
		printf("%d\n",siz[root]);
		return (siz[root]>=m);
	}
	void Solve() {
		root=0;
		for(int i=1;i<=n;++i)if(deg[i]>deg[root])root=i;
		for(int i=1;i<n;++i){
			AddEdge(E[i].x,E[i].y,E[i].w);
			AddEdge(E[i].y,E[i].x,E[i].w);
		}
		PreDFS(root,0);
		int L=1,R=Sum,ans=1;
		while(L<=R) {
			int mid=(L+R)>>1;
			if(check(mid))ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}

int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<n;++i) {
		E[i].x=read(),E[i].y=read(),E[i].w=read();
		Sum+=E[i].w;
		if(E[i].x!=1)flag1=0;
		if(E[i].y!=E[i].x+1)flag2=0;
	}
	if(flag1)Subtask1::Solve();
	else if(flag2)Subtask2::Solve();
	else if(m==1)Subtask3::Solve();
	else if(Subtask4::CheckFlag())Subtask4::Solve();
	return 0;
}
