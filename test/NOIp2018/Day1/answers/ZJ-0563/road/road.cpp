#include<bits/stdc++.h>
using namespace std;

int read() {
	int x=0,f=1;char ch=getchar();
	for(;ch<'0' || ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0' && ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}

typedef long long LL;
const int N=1e5+7;
int n,stac[N],top=0;
LL ans=0;

int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;++i) {
		int x=read();
		if(x<stac[top]) {
			ans+=stac[top]-x;
			while(x<stac[top])--top;
		}
		if(x>stac[top])stac[++top]=x;
	}
	if(stac[top])ans+=stac[top];
	printf("%lld\n",ans);
	return 0;
}
