#include<bits/stdc++.h>
using namespace std;
#define INF 0x7fffffff
#define ll long long
#define MEM(a,b) memset(a,b,sizeof(a))
#define fui(i,a,b,c) for(int i=(a);i<=(b);i+=(c))
#define fdi(i,a,b,c) for(int i=(a);i>=(b);i-=(c))
#define fel(i,u) for(int i=hd[u];i;i=dg[i].nxt)
#define maxn 50010
int n,m;
struct Edge{
	int to,nxt;ll wgh;Edge(){}
	Edge(int x,int y,ll z){to=x,nxt=y,wgh=z;}
}dg[maxn<<1];
int hd[maxn],inb;
ll val[maxn];
ll t3sum[50010];
template<class T>
inline T read(T &n){
	n=0;int t=1;double x=10;char ch;
	for(ch=getchar();!isdigit(ch)&&ch!='-';ch=getchar());
	(ch=='-')?t=-1:n=ch-'0';
	for(ch=getchar();isdigit(ch);ch=getchar())n=n*10+ch-'0';
	if(ch=='.')for(ch=getchar();isdigit(ch);ch=getchar(),x*=10)n+=(ch-'0')/x;
	return n*=t;
}
template<class T>
T write(T n){if(n<0)putchar('-'),n=-n;if(n>=10)write(n/10);putchar(n%10+'0');return n;}
template<class T>T writeln(T n){write(n);puts("");return n;}
void add(int x,int y,ll z){dg[++inb]=(Edge){y,hd[x],z};hd[x]=inb;}
void init(){
	read(n),read(m);
	for(int i=1,x,y,z;i<n;++i){read(x),read(y),read(z);add(x,y,z);add(y,x,z);}
}
int dfs(int u,int f,ll &ds){
	int aa=u,aaa;ll ma=0,maa=0;
	fel(i,u)if(dg[i].to!=f){
		aaa=dfs(dg[i].to,u,maa);
		maa+=dg[i].wgh;
		if(maa>ma)aa=aaa,ma=maa;
	}
	ds=ma;return aa;
}
int TP1(){int aa;ll ma;aa=dfs(1,1,ma);aa=dfs(aa,aa,ma);writeln(ma);return 0;}
int TP233(){ll aa=INF;fui(i,1,2*n-2,1)aa=min(aa,dg[i].wgh);writeln(aa);return 0;}
char CHK2(){int dd=0;fel(i,1)dd++;return (dd==n-1);}
int TP2(){
	ll p=0;
	fel(i,1)val[++p]=dg[i].wgh;
	sort(val+1,val+n);n--;
	p=INF;
	int p1=n-2*m+1;
	fui(i,1,m,1)p=min(p,val[n-i+1]+val[p1+i-1]);
	writeln(p);
	return 0;
}
char CHK3(){fui(i,1,n,1)fel(j,i)if(dg[j].to!=i-1&&dg[j].to!=i+1)return 0;return 1;}
char OK3(ll x){
	int p=1,q=1,nm=0;
	while(q<=n&&nm<m){
		while(q<n&&t3sum[q]-t3sum[p-1]<x)q++;
		if(t3sum[q]-t3sum[p-1]>=x)nm++;p=q=q+1;
	}
	return nm>=m;
}
int TP3(){
	int p=0;
	fui(i,1,n,1)fel(j,i)if(dg[j].to==i+1)val[++p]=dg[j].wgh;
	fui(i,1,n,1)t3sum[i]=t3sum[i-1]+val[i];
	ll l=1,r=t3sum[n],mid,ans=0;
	while(l<=r){
		mid=l+r>>1;
		if(OK3(mid))ans=mid,l=mid+1;
		else r=mid-1;
	}
	writeln(ans);
	return 0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	init();
	if(m==1)return TP1();
	if(m==n-1)return TP233();
	if(CHK2())return TP2();
	if(CHK3())return TP3();
	puts("YXAKIOI");
	return 0;
}
