#include<bits/stdc++.h>
using namespace std;
#define INF 0x7fffffff
#define ll long long
#define MEM(a,b) memset(a,b,sizeof(a))
#define fui(i,a,b,c) for(int i=(a);i<=(b);i+=(c))
#define fdi(i,a,b,c) for(int i=(a);i>=(b);i-=(c))
#define fel(i,u) for(int i=hd[u];i;i=dg[i].nxt)
#define maxn 100010
ll n;
ll d[maxn];
ll ans;
template<class T>
inline T read(T &n){
	n=0;int t=1;double x=10;char ch;
	for(ch=getchar();!isdigit(ch)&&ch!='-';ch=getchar());
	(ch=='-')?t=-1:n=ch-'0';
	for(ch=getchar();isdigit(ch);ch=getchar())n=n*10+ch-'0';
	if(ch=='.')for(ch=getchar();isdigit(ch);ch=getchar(),x*=10)n+=(ch-'0')/x;
	return n*=t;
}
template<class T>
T write(T n){if(n<0)putchar('-'),n=-n;if(n>=10)write(n/10);putchar(n%10+'0');return n;}
template<class T>T writeln(T n){write(n);puts("");return n;}
void init(){
	read(n);fui(i,1,n,1)read(d[i]);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	init();
	fui(i,1,n,1)if(d[i]>d[i-1])ans+=d[i]-d[i-1];
	writeln(ans);
	return 0;
}
