#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=105,maxw=25000;
int n,a[maxn],ans;
bool F[maxw+5];
int read(){
	int ret=0;bool f=0;char ch=getchar();
	while(ch>'9'||ch<'0') f^=ch=='-',ch=getchar();
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();
	return f?-ret:ret;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (int T=read();T--;){
		memset(F,0,sizeof F);F[0]=1;
		n=read();ans=0;
		for (int i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+1+n);
		for (int i=1;i<=n;i++) if(!F[a[i]]){
			ans++;
			for (int j=a[i];j<=maxw;j++) F[j]|=F[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
