#include<cstdio>
using namespace std;
const int maxn=100005;
int n,a[maxn],ans;
int read(){
	int ret=0;bool f=0;char ch=getchar();
	while(ch>'9'||ch<'0') f^=ch=='-',ch=getchar();
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();
	return f?-ret:ret;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++){
		a[i]=read();
		if(a[i]>a[i-1]) ans+=a[i]-a[i-1];
	}
	printf("%d\n",ans);
	return 0;
}
