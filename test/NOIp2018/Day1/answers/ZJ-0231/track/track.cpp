#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=50005,maxe=maxn<<1;
int n,m,V,cnt,a[maxn],id[maxn],f[maxn];
int e,son[maxe],w[maxe],nxt[maxe],lnk[maxn];
int read(){
	int ret=0;bool f=0;char ch=getchar();
	while(ch>'9'||ch<'0') f^=ch=='-',ch=getchar();
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();
	return f?-ret:ret;
}
void add_e(int x,int y,int z){son[++e]=y;w[e]=z;nxt[e]=lnk[x];lnk[x]=e;}
void DFS(int x,int fa){
	for (int i=lnk[x];i;i=nxt[i]) if(son[i]^fa) DFS(son[i],x);
	a[0]=f[x]=0;
	for (int i=lnk[x];i;i=nxt[i]) if(son[i]^fa) a[++a[0]]=f[son[i]]+w[i];
	sort(a+1,a+1+a[0]);
	while(a[a[0]]>=V&&a[0]) a[0]--,cnt++;
	for (int i=1;i<=a[0];i++) id[i]=0;
	for (int i=a[0],j=1;j<i;){
		while(a[j]+a[i]<V&&j<i) j++;
		if(j>=i) break;
		id[i]=j,id[j]=i;
		i--,j++,cnt++;
	}
	for (int i=a[0];i;i--) if(!id[i]){
		f[x]=a[i];
		for (i++;i<=a[0];i++)
		if(f[x]+a[id[i]]>=V) f[x]=a[i];
		break;
	}
}
bool check(int x){
	V=x,cnt=0;
	DFS(1,-1);
	return cnt>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for (int i=1,x,y,z;i<n;i++){
		x=read(),y=read(),z=read();
		add_e(x,y,z),add_e(y,x,z);
	}
	int L=1,R=500000000,mid;
	while(L<=R){
		mid=L+R>>1;
		if(check(mid)) L=mid+1;else R=mid-1;
	}
	printf("%d\n",R);
	return 0;
}
