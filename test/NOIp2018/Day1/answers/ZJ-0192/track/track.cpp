#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
#define RI register int
#define Ms(f,x) memset(f,x,sizeof(f))
#define add(x,y,z) e[++cnt]=(edge){y,head[x],z},head[x]=cnt
using namespace std;
const int N=50005;
struct edge
{
	int to,nxt,v;
}e[N<<1]; int head[N],n,m,cnt,x,y,z,sum; bool sign_link,sign_flower;
class FileInputOutput
{
	private:
		#define S 1<<21
		#define tc() (A==B&&(B=(A=Fin)+fread(Fin,1,S,stdin),A==B)?EOF:*A++)
		char Fin[S],*A,*B;
	public:
		FileInputOutput() { A=B=Fin; }
		inline void read(int &x)
		{
			x=0; char ch; while (!isdigit(ch=tc()));
			while (x=(x<<3)+(x<<1)+(ch&15),isdigit(ch=tc()));
		}
		#undef S
		#undef tc
}F;
#define to e[i].to
class Brute_Force_Solver
{
	private:
		#define BN 1005
		struct data
		{
			int l,r,val;
			data(int L=0,int R=0,int Val=0) { l=L; r=R; val=Val; }
			inline friend bool operator <(data a,data b)
			{
				return a.val<b.val;
			}
		}a[BN*BN]; int q[BN],tot,dis[BN],pre[BN],ans; bool exist[BN][BN],vis[BN];
		inline void BFS(int st)
		{
			RI H=0,T=1; Ms(vis,0); q[vis[st]=1]=st; dis[st]=0;
			while (H<T)
			{
				int now=q[++H]; a[++tot]=data(st,now,dis[now]);
				for (RI i=head[now];i;i=e[i].nxt)
				if (!vis[to]) vis[to]=1,dis[to]=dis[now]+e[i].v,q[++T]=to;
			}
		}
		inline bool paint(int st,int tar)
		{
			RI H=0,T=1; Ms(vis,0); q[vis[st]=1]=st; pre[st]=0;
			while (H<T)
			{
				int now=q[++H]; for (RI i=head[now];i;i=e[i].nxt)
				if (!vis[to]&&!exist[now][to]) vis[to]=1,pre[to]=now,q[++T]=to;
			}
			if (!vis[tar]) return 0; int now=tar;
			while (pre[now]) exist[pre[now]][now]=exist[now][pre[now]]=1,now=pre[now];
			return 1;
		}
		inline bool check(int x)
		{
			int cur=0; Ms(exist,0); for (RI i=1;i<=tot&&cur<m;++i)
			if (a[i].val>=x&&paint(a[i].l,a[i].r)) ++cur;
			return cur==m;
		}
	public:
		inline void solve(void)
		{
			RI i; for (i=1;i<=n;++i) BFS(i); sort(a+1,a+tot+1);
			int l=0,r=sum,mid; while (l<=r)
			if (check(mid=(l+r)>>1)) ans=mid,l=mid+1; else r=mid-1;
			printf("%d",ans);
		}
}Case1;
class Tree_Divistion_Solver
{
	private:
		int q[N],dis[N],pos,mx; bool vis[N];
		inline void BFS(int st)
		{
			RI H=0,T=1; Ms(vis,0); q[vis[st]=1]=st; dis[st]=0; mx=-1;
			while (H<T)
			{
				int now=q[++H]; if (dis[now]>mx) mx=dis[now],pos=now;
				for (RI i=head[now];i;i=e[i].nxt)
				if (!vis[to]) vis[to]=1,dis[to]=dis[now]+e[i].v,q[++T]=to;
			}
		}
	public:
		inline void solve(void)
		{
			BFS(1); BFS(pos); printf("%d",mx);
		}
}Case2;
class Link_Solver
{
	private:
		int ans,prefix[N];
		inline void DFS(int now,int fa)
		{
			for (RI i=head[now];i;i=e[i].nxt) if (to!=fa)
			prefix[to]=prefix[now]+e[i].v,DFS(to,now);
		}
		inline int find(int st,int x)
		{
			int l=st,r=n,res=n,mid; while (l<=r)
			if (prefix[mid=(l+r)>>1]-prefix[st]>=x) res=mid,r=mid-1; else l=mid+1;
			return res;
		}
		inline bool check(int x)
		{
			int cur=0; for (RI i=1;i<=n&&cur<m;)
			{
				int pos=find(i,x); if (prefix[pos]-prefix[i]>=x)
				++cur,i=pos+1; else return 0;
			}
			return cur==m;
		}
	public:
		inline void solve(void)
		{
			DFS(1,0); int l=0,r=sum,mid; while (l<=r)
			if (check(mid=(l+r)>>1)) ans=mid,l=mid+1; else r=mid-1;
			printf("%d",ans);
		}
}Case3;
class Flower_Solver
{
	private:
		int n,a[N],tot,ans,pos;
		inline bool check(int x)
		{
			int cur=0; pos=1; for (RI i=tot;i>=pos&&cur<m;--i)
			if (a[i]>=x) ++cur; else
			{
				while (pos<=tot&&a[pos]+a[i]<x) ++pos;
				if (pos>=i) break; else ++pos,++cur;
			}
			return cur==m;
		}
	public:
		inline void solve(void)
		{
			for (RI i=head[1];i;i=e[i].nxt) a[++tot]=e[i].v;
			sort(a+1,a+tot+1); int l=0,r=sum,mid; while (l<=r)
			if (check(mid=(l+r)>>1)) ans=mid,l=mid+1; else r=mid-1;
			printf("%d",ans);
		}
}Case4;
#undef to
int main()
{
	freopen("track.in","r",stdin); freopen("track.out","w",stdout);
	RI i; for (F.read(n),F.read(m),i=sign_link=sign_flower=1;i<n;++i)
	{
		F.read(x); F.read(y); F.read(z); add(x,y,z); add(y,x,z);
		sign_link&=(x+1==y); sign_flower&=(x==1); sum+=z;
	}
	if (m==1) return Case2.solve(),0; if (sign_link) return Case3.solve(),0;
	if (sign_flower) return Case4.solve(),0; return Case1.solve(),0;
}
