#include<cstdio>
#include<cctype>
#include<algorithm>
#define RI register int
using namespace std;
const int N=105,MAX_S=25005;
int t,n,a[N],ans,mx;
class FileInputOutput
{
	private:
		#define S 1<<21
		#define tc() (A==B&&(B=(A=Fin)+fread(Fin,1,S,stdin),A==B)?EOF:*A++)
		char Fin[S],*A,*B;
	public:
		FileInputOutput() { A=B=Fin; }
		inline void read(int &x)
		{
			x=0; char ch; while (!isdigit(ch=tc()));
			while (x=(x<<3)+(x<<1)+(ch&15),isdigit(ch=tc()));
		}
		#undef S
		#undef tc
}F;
class DP_Solver
{
	private:
		bool f[MAX_S];
		inline void expand(int x)
		{
			for (RI i=x;i<=mx;++i) f[i]|=f[i-x];
		}
	public:
		inline void solve(void)
		{
			sort(a+1,a+n+1); for (RI i=f[0]=1;i<=n;++i)
			if (!f[a[i]]) ++ans,expand(a[i]);
		}
		inline void clear(void)
		{
			for (RI i=1;i<=mx;++i) f[i]=0;
		}
}D;
int main()
{
	freopen("money.in","r",stdin); freopen("money.out","w",stdout);
	for (F.read(t);t;--t)
	{
		RI i; for (F.read(n),mx=0,i=1;i<=n;++i) F.read(a[i]),mx=a[i]>mx?a[i]:mx;
		ans=0; D.solve(); printf("%d\n",ans); D.clear();
	}
	return 0;
}
