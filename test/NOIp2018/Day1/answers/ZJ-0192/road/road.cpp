#include<cstdio>
#include<cctype>
#define RI register int
using namespace std;
const int N=100005;
int n,a[N],lst,pos; long long ans;
class FileInputOutput
{
	private:
		#define S 1<<21
		#define tc() (A==B&&(B=(A=Fin)+fread(Fin,1,S,stdin),A==B)?EOF:*A++)
		char Fin[S],*A,*B;
	public:
		FileInputOutput() { A=B=Fin; }
		inline void read(int &x)
		{
			x=0; char ch; while (!isdigit(ch=tc()));
			while (x=(x<<3)+(x<<1)+(ch&15),isdigit(ch=tc()));
		}
		#undef S
		#undef tc
}F;
inline int max(int a,int b)
{
	return a>b?a:b;
}
inline void solve(int l,int r,int lst)
{
	int t; for (RI i=r;i>=l;--i) t=max(0,a[i]-lst),lst+=t,ans+=t;
}
int main()
{
	freopen("road.in","r",stdin); freopen("road.out","w",stdout);
	RI i; for (F.read(n),i=1;i<=n;++i) F.read(a[i]);
	for (i=pos=1;i<=n;++i) if (a[i]>a[i-1]) solve(pos,i-1,lst),pos=i,lst=a[i-1];
	return solve(pos,n,lst),printf("%lld",ans),0;
}
