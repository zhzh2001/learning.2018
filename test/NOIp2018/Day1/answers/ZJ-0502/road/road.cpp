#include<bits/stdc++.h>
using namespace std;
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-') f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	return x*f;
}
inline void file(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
}
long long n,a[500001],las,ans;
int main(){
	file();
	n=read();las=0;ans=0;
	for (int i=1;i<=n;i++){
		a[i]=read();
	}
	for (int i=1;i<=n;i++){
		if (a[i]>=las) ans+=a[i]-las;
		las=a[i];
	}
	cout<<ans<<endl;
	return 0;
}
