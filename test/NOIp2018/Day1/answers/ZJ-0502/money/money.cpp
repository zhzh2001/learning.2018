#include<bits/stdc++.h>
using namespace std;
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-') f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	return x*f;
}
inline void file(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}
long long n,a[1000001],T,ma,ans;
bool f[1000001],vis[1000001];
int main(){
	file();
	T=read();
	while (T--){
		ma=0;
		n=read();ans=n;
		for (int i=1;i<=n;i++){
			a[i]=read();ma=max(ma,a[i]);vis[i]=0;
		}
		sort(a+1,a+1+n);
		for (int i=1;i<=ma;i++){
			f[i]=0;
		}
		f[0]=1;
		for (int i=1;i<=n;i++){
			for (int j=a[i];j<=ma;j++){
				f[j]|=f[j-a[i]];
			}
			for (int j=i+1;j<=n;j++){
				if (!vis[j]&&f[a[j]]){
					vis[j]=1;ans--;
				}
			}
		}
		cout<<ans<<endl;
	}
	return 0;
}
