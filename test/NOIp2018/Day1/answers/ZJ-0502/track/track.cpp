#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-') f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	return x*f;
}
inline void file(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
}
int n,m,ans,dp[100001],root,cnt,son[100001],fa[100001],fad[100001],head[100001],tot;
struct edge{
	int to,nxt,dis; 
}e[2000001];struct node{
	int w,l,r,sz,czx; 
}g[2000001];
inline int news(int w){
	g[++cnt].w=w;g[cnt].l=g[cnt].r=0;g[cnt].sz=1;g[cnt].czx=rand();
	return cnt;
}
inline void pushup(int rt){
	g[rt].sz=g[g[rt].l].sz+1+g[g[rt].r].sz;
}
void split(int rt,int &a,int &b,int w){
	if (!rt){
		a=b=0;return;
	}
	if (g[rt].w<=w){
		a=rt;split(g[a].r,g[a].r,b,w);pushup(rt);
	}else{
		b=rt;split(g[b].l,a,g[b].l,w);pushup(rt);
	}
}
int merge(int a,int b){
	if (a*b==0) return a+b;
	if (g[a].czx<=g[b].czx){
		g[a].r=merge(g[a].r,b);pushup(a);return a;
	}
	else{
		g[b].l=merge(a,g[b].l);pushup(b);return b;
	}
}
inline void insert(int w){
	int x=0,y=0;
	split(root,x,y,w);
	root=merge(merge(x,news(w)),y);
}
inline void del(int w){
	int x=0,y=0,z=0,a=0;
	split(root,x,y,w);
	split(x,z,a,w-1);
	root=merge(z,merge(merge(g[a].l,g[a].r),y));
}
inline int rank_x(int rot,int ra){
	int rt=rot;
	while (rt){
		if (g[g[rt].l].sz+1==ra) return g[rt].w;
		else if (g[g[rt].l].sz+1>ra) rt=g[rt].l;
		else {ra-=g[g[rt].l].sz+1;rt=g[rt].r;}
	}
	return 0;
} 
inline int nxxt(int w){
	int x=0,y=0,ans=0;
	split(root,x,y,w-1);
	ans=rank_x(y,1);
	root=merge(x,y);
	return ans;
}
inline void made(int from,int to,int dis){
	e[++tot].dis=dis;e[tot].to=to;e[tot].nxt=head[from];
	head[from]=tot;
}
void dfs(int u,int faa){
	for (int i=head[u];i;i=e[i].nxt){
		int v=e[i].to;
		if (v==faa) continue;
		dfs(v,u);
		ans=max(ans,dp[u]+dp[v]+e[i].dis);
		dp[u]=max(dp[u],dp[v]+e[i].dis);
	}
}
inline bool check(int wht){
	int www=m;
	for (int qwq=0,i=1;i<=n;i++){
		qwq+=fad[i];
		if (qwq>=wht) qwq=0,www--;
	}
	if (www<=0) return 1;
	return 0;
}
inline bool check1(int wht){
	int www=m;
	cnt=0;root=0;
	for (int i=2;i<=n;i++){
		insert(fad[i]);
	}
	int mx=rank_x(root,g[root].sz),mxx=rank_x(root,g[root].sz-1);
	while (g[root].sz>1&&mx+mxx>=wht){
		del(mx);
		int qwq=nxxt(wht-mx);
		www--;del(qwq);
		mx=rank_x(root,g[root].sz),mxx=rank_x(root,g[root].sz-1);
	}
	if (www<=0)return 1;
	return 0;
}
int main(){
	file();srand(121321);
	n=read();m=read();
	if (m==1){
		for (int i=1;i<n;i++){
			int x=read(),y=read(),z=read();
			made(x,y,z);made(y,x,z);
		}
		dfs(1,0);
		printf("%d\n",ans);
	}else{
		int l=0,r=0,ans=0;bool flag=1;
		for (int i=1;i<n;i++){
			int x=read(),y=read(),z=read();
			made(x,y,z);made(y,x,z);fad[y]=z;
			if (y!=x+1) flag=0;r+=z;
		}
		if (flag){
			while (l<=r){
				int mid=(l+r)>>1;
				if (check(mid)){
					l=mid+1;ans=mid;
				}else{
					r=mid-1;
				}
			}
			cout<<ans<<endl;
		}else{
			for (int i=head[1];i;i=e[i].nxt){
				int v=e[i].to;
				fad[v]=e[i].dis;
			}
			while (l<=r){
				int mid=(l+r)>>1;
				if (check1(mid)){
					l=mid+1;ans=mid;
				}else{
					r=mid-1;
				}
			}
			cout<<ans<<endl;
		}
	}
	//dfs(1,0);
	//sort(cc+1,cc+1+n,cmp);
	//while (l<=r){
	//	int mid=(l+r)/2;
	//	if (check(mid)){
	//		l=mid+1;ans=mid;
	//	}else r=mid-1;
	//}
	//printf("%d\n",ans);
	return 0;
}
/*
#include<bits/stdc++.h>
using namespace std;
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-') f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	return x*f;
}
inline void file(){
	freopen("track1.in","r",stdin);
	//freopen("track.out","w",stdout);
}
int n,m,root[100001],cnt,son[100001],fa[100001],fad[100001],head[100001],tot;
struct edge{
	int to,nxt,dis; 
}e[2000001];
struct sss{
	int d,id;
}cc[100001];
inline void made(int from,int to,int dis){
	e[++tot].dis=dis;e[tot].to=to;e[tot].nxt=head[from];
	head[from]=tot;
}
struct node{
	int w,l,r,sz,czx; 
}g[2000001];
inline int news(int w){
	g[++cnt].w=w;g[cnt].l=g[cnt].r=0;g[cnt].sz=1;g[cnt].czx=rand();
	return cnt;
}
inline void pushup(int rt){
	g[rt].sz=g[g[rt].l].sz+1+g[g[rt].r].sz;
}
void split(int rt,int &a,int &b,int w){
	if (!rt){
		a=b=0;return;
	}
	if (g[rt].w<=w){
		a=rt;split(g[a].r,g[a].r,b,w);pushup(rt);
	}else{
		b=rt;split(g[b].l,a,g[b].l,w);pushup(rt);
	}
}
int merge(int a,int b){
	if (a*b==0) return a+b;
	if (g[a].czx<=g[b].czx){
		g[a].r=merge(g[a].r,b);pushup(a);return a;
	}
	else{
		g[b].l=merge(a,g[b].l);pushup(b);return b;
	}
}
inline void insert(int &rot,int w){
	int x=0,y=0;
	split(rot,x,y,w);
	rot=merge(merge(x,news(w)),y);
}
inline void del(int &rot,int w){
	int x=0,y=0,z=0,a=0;
	split(rot,x,y,w);
	split(x,z,a,w-1);
	rot=merge(z,merge(merge(g[a].l,g[a].r),y));
}
inline int rank_x(int rot,int ra){
	int rt=rot;
	while (rt){
		if (g[g[rt].l].sz+1==ra) return g[rt].w;
		else if (g[g[rt].l].sz+1>ra) rt=g[rt].l;
		else {ra-=g[g[rt].l].sz+1;rt=g[rt].r;}
	}
	return 0;
} 
inline int nxxt(int &rot,int w){
	int x=0,y=0,ans=0;
	split(rot,x,y,w-1);
	ans=rank_x(y,1);
	rot=merge(x,y);
	return ans;
}
void dfs(int u,int faa){
	fa[u]=faa;cc[u].id=u;cc[u].d=cc[faa].d+1;
	for (int i=head[u];i;i=e[i].nxt){
		int v=e[i].to;
		if (v==faa) continue;
		son[u]++;fad[v]=e[i].dis;
		dfs(v,u);
	}
}
inline int mergee(int rt1,int rt2){
	
}
inline bool check(int wht){
	cnt=0;
	for (int i=1;i<=n;i++){
		root[i]=0;
	}
	int www=m;
	for (int i=1;i<=n;i++){
		insert(root[cc[i].id],-99999999);
		if (son[cc[i].id]!=0){
			for (int j=head[cc[i].id];j;j=e[j].nxt){
				int v=e[j].to;
				if (v==fa[cc[i].id]) continue;
				root[cc[i].id]=mergee(root[cc[i].id],root[v]);
			}
			int qwq=0,qwqw=0;qwq=rank_x(root[cc[i].id],g[root[cc[i].id]].sz);
			while (g[root[cc[i].id]].sz>=1&&qwq>=wht) www--,del(root[cc[i].id],qwq),qwq=rank_x(root[cc[i].id],g[root[cc[i].id]].sz);
			if (www<=0) return 1;
			qwq=rank_x(root[cc[i].id],g[root[cc[i].id]].sz);qwqw=rank_x(root[cc[i].id],g[root[cc[i].id]].sz-1);
			while (g[root[cc[i].id]].sz>1&&qwq+qwqw>=wht){
				www--;del(root[cc[i].id],qwq);del(root[cc[i].id],nxxt(root[cc[i].id],wht-qwq));	
				qwq=rank_x(root[cc[i].id],g[root[cc[i].id]].sz);qwqw=rank_x(root[cc[i].id],g[root[cc[i].id]].sz-1);
			}
			if (www<=0) return 1;
			int maaa=rank_x(root[cc[i].id],g[root[cc[i].id]].sz);
			root[cc[i].id]=0;
			insert(root[cc[i].id],maaa+fad[cc[i].id]);
		}else
			insert(root[cc[i].id],fad[cc[i].id]);
	}
	return 0;
}
inline bool cmp(sss a,sss b){
	return a.d>b.d;
}
int main(){
	file();
	srand(20040404);
	n=read();m=read();
	int l=0,r=0,ans=0;
	for (int i=1;i<n;i++){
		int x=read(),y=read(),z=read();
		made(x,y,z);made(y,x,z);r+=z;
	}
	dfs(1,0);
	sort(cc+1,cc+1+n,cmp);
	while (l<=r){
		int mid=(l+r)/2;
		if (check(mid)){
			l=mid+1;ans=mid;
		}else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
*/
