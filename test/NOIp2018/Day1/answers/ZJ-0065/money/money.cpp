#include <bits/stdc++.h>
using namespace std;
const int inf=2000000000;
int T,n,X,a[105],f[30000],inq[30000],d[30005];
void solve(){
	scanf("%d",&n); int fl=0;
	for (int i=1;i<=n;++i) scanf("%d",&a[i]),fl|=(a[i]==1);
	if (fl) {puts("1"); return;}
	sort(a+1,a+n+1);
	X=a[1];
	for (int i=1;i<=n;++i) if (a[i]!=inf)
	for (int j=i+1;j<=n;++j) if (a[j]!=inf)
		if ((a[j]-a[i])%X==0) a[j]=inf;
	sort(a+1,a+n+1);
	while (a[n]==inf) n--;
	for (int i=0;i<X;++i) f[i]=inf,inq[i]=0;
	d[1]=0; inq[0]=1; f[0]=0;
	int l=1,r=2,k;
	while (l!=r){
		k=d[l]; inq[d[l++]]=0;
		if (l==30000) l=1;
		for (int i=1;i<=n;++i)
		if (f[(k+a[i])%X]>f[k]+a[i]){
			f[(k+a[i])%X]=f[k]+a[i];
			if (!inq[(k+a[i])%X]){
				d[r]=(k+a[i])%X;
				inq[d[r]]=1;
				if (f[d[r]]<f[d[l]]) swap(d[r],d[l]);
				++r; if (r==30000) r=1;
			}
		}
	}
	f[0]=X;
	for (int i=0;i<X;++i){
		for (int j=1;j<=n;++j)
		if (f[(f[i]+a[j])%X]==f[i]+a[j]) inq[(f[i]+a[j])%X]=1;
	}
	int ans=0;
	for (int i=0;i<X;++i) if (f[i]!=inf) ans+=1-inq[i];
	printf("%d\n",ans);
	return;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--) solve();
	return 0;
}
