#include <bits/stdc++.h>
using namespace std;
int n,m,L,R,M,s[50005],f[50005],d[50005],e[50005];
vector <int> a[50005],b[50005];
bool cmp(int x,int y){
	return s[x]<s[y];
}
void dfs(int x,int y){
	for (unsigned i=0;i<a[x].size();++i)
	if (a[x][i]!=y){
		dfs(a[x][i],x);
		f[x]+=f[a[x][i]];
		s[a[x][i]]+=b[x][i];
	}
	int t=2;
	for (unsigned i=0;i<a[x].size();++i)
		if (a[x][i]!=y) d[++t]=a[x][i];
	sort(d+1,d+t+1,cmp);
	int MX=0;
	for (int i=1,j=t;i<j;)
		if (s[d[i]]+s[d[j]]>=M){
			if (d[i]!=0) ++i;
			++MX,--j;
		} else ++i;
	int l=2,r=t,k,TT;
	while (l<r){
		k=(l+r+1)>>1; TT=0;
		for (int i=1;i<k;++i) e[i]=d[i];
		for (int i=k+1;i<=t;++i) e[i-1]=d[i];
		for (int i=1,j=t-1;i<j;){
			if (s[e[i]]+s[e[j]]>=M){
				if (e[i]!=0) ++i;
				++TT,--j; 
			} else ++i;
		}
		if (TT==MX) l=k; else r=k-1;
	}
	f[x]+=MX;
	s[x]=s[d[l]];
}
bool check(){
	//clear
	for (int i=1;i<=n;++i) f[i]=s[i]=0;
	dfs(1,0);
	return f[1]>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;++i){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		a[x].push_back(y);
		a[y].push_back(x);
		b[x].push_back(z);
		b[y].push_back(z);
		R+=z;
	}
	L=0; R/=m;
	while (L<R){
		M=(L+R+1)/2;
		if (check()) L=M; else R=M-1;
	}
	printf("%d\n",L);
	return 0;
}
