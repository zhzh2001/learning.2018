#include<cstdio>
#define maxn 100005
using namespace std;
int n,lst,ans;
inline char nc(){
	static char buf[100000],*L=buf,*R=buf;
	return L==R&&(R=(L=buf)+fread(buf,1,100000,stdin),L==R)?EOF:*L++;
}
inline int read(){
	int ret=0,f=1;char ch=nc();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=nc();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=nc();
	return ret*f;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();lst=read();
	for (int i=2;i<=n;i++){
		int x=read();
		if (x<lst) ans+=lst-x;
		lst=x;
	}
	ans+=lst;
	printf("%d\n",ans);
	return 0;
}
