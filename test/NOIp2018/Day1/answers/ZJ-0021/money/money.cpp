#include<cstdio>
#include<algorithm>
#define maxn 105
#define maxv 25005
using namespace std;
bool vis[maxv];
int T,n,ans,allv,a[maxn];
inline char nc(){
	static char buf[100000],*L=buf,*R=buf;
	return L==R&&(R=(L=buf)+fread(buf,1,100000,stdin),L==R)?EOF:*L++;
}
inline int read(){
	int ret=0,f=1;char ch=nc();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=nc();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=nc();
	return ret*f;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while (T--){
		n=read();
		for (int i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+n+1);allv=a[n];
		ans=0;
		for (int i=0;i<=allv;i++) vis[i]=0;
		vis[0]=1;
		for (int i=1;i<=n;i++){
			if (vis[a[i]]) continue;
			ans++;
			for (int j=a[i];j<=allv;j++)
			if (!vis[j]) vis[j]|=vis[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
