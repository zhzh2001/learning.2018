#include<cstdio>
#include<algorithm>
#define maxn 50005
using namespace std;
int n,m,tot,sum,mid,ans,top,fa[maxn],dis[maxn],que[maxn],lnk[maxn],son[maxn<<1],net[maxn<<1],w[maxn<<1];
inline char nc(){
	static char buf[100000],*L=buf,*R=buf;
	return L==R&&(R=(L=buf)+fread(buf,1,100000,stdin),L==R)?EOF:*L++;
}
inline int read(){
	int ret=0,f=1;char ch=nc();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=nc();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=nc();
	return ret*f;
}
inline int get(int x){return x==fa[x]?x:fa[x]=get(fa[x]);}
inline void add(int x,int y,int z){net[++tot]=lnk[x];son[tot]=y;w[tot]=z;lnk[x]=tot;}
void DFS(int x,int f){
	for (int j=lnk[x];j;j=net[j])
	if (son[j]!=f)	DFS(son[j],x);
	top=0;
	for (int j=lnk[x];j;j=net[j])
	if (son[j]!=f){
		if (dis[son[j]]+w[j]>=mid) sum++;
		else que[++top]=dis[son[j]]+w[j],fa[top]=top;
	}
	sort(que+1,que+top+1);fa[top+1]=top+1;
    int i=1,mx=top;
   while (i<=top){
		if (que[i]+que[mx]<mid){i=get(i+1);continue;} 
    	int j=get(i+1);
    	while ((que[i]+que[j]<mid)&&j<=top) j=get(j+1);
    	if (j>top){i=get(i+1);continue;}
    	sum++;fa[i]=i+1;fa[j]=j+1;
    	if (j==mx) mx--;
    	i=get(i+1);
	}
	dis[x]=0;
	while (top){
		if (get(top)==top){dis[x]=que[top];break;}
		top--;
	}
}
inline bool check(){sum=0;DFS(1,0);return sum>=m;}
void fnd(){
	int L=0,R=1e9;
	while (L<=R){
		mid=((R-L)>>1)+L;
		if (check()) ans=mid,L=mid+1;
		else R=mid-1;
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<n;i++){
		int x=read(),y=read(),z=read();
		add(x,y,z);add(y,x,z);
	}
	fnd();
	printf("%d\n",ans);
	return 0;
}
