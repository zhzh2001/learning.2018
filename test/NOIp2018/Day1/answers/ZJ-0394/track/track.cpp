#include<bits/stdc++.h>
using namespace std;
int n,m;
int e,head[50010],to[100010],Next[100010];
int edge[100010];
void add(int x,int y,int z)
{
	to[++e]=y;
	Next[e]=head[x];
	head[x]=e;
	edge[e]=z;
	return ;
}
int d[50010];
int ans=0;
int pos=0;
void dfs(int x,int fa,int len)
{
	if(fa!=0&&d[x]==1)
	  {
	  	if(len>ans)
	  	  {
	  	  	ans=len;
	  	  	pos=x;
		  }
		return ;
	  }
	for(int i=head[x];i;i=Next[i])
	  {
	  	if(to[i]==fa)
	  	  continue;
	  	dfs(to[i],x,len+edge[i]);
	  }
	return ;
}
void solvem()
{
	dfs(1,0,0);
	dfs(pos,0,0);
	printf("%d\n",ans);
	return ;
}
int L[50010];
bool checkf(int mid)
{
	sort(L+1,L+n);
	int ans=0;
	int r=n-1;
	int l=1;
	while(r>=l&&L[r]>=mid)
	  ans++;
	while(r>l)
	  {
  	  	if(L[l]+L[r]<mid)
  	  	  {
  	  	  	l++;
		  }
  	  	else
  	  	  {
  	  	  	r--;
  	  	  	l++;
  	  	  	ans++;
		  }		
	  }
	if(ans>=m)
	  return true;
	else
	  return false;
}
void solvef()
{
	int l=1;
	int r=1e9;
	while(l<r)
	  {
	  	int mid=(l+r+1)>>1;
	  	if(checkf(mid))
	  	  l=mid;
	  	else
	  	  r=mid-1;
	  }
	printf("%d",l);
	return ;
}
int LL[50010];
bool check(int mid)
{
	int ans=0;
	int len=0;
	int p=1;
	while(p<n)
	  {
	  	len+=LL[p];
	  	if(len>=mid)
	  	  {
	  	  	len=0;
	  	  	ans++;
		  }
		p++;
	  }
	if(ans>=m)
	  return true;
	else
	  return false;
}
void solve()
{
	int l=1;
	int r=1e9;
	while(l<r)
	  {
	  	int mid=(l+r+1)>>1;
	  	if(check(mid))
	  	  l=mid;
	  	else
	  	  r=mid-1;
	  }
	printf("%d",l);
	return ;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	bool flag=true;
	cin>>n>>m;
	for(int i=1;i<n;i++)
	  {
	  	int a,b;
	  	int l;
	  	scanf("%d%d%d",&a,&b,&l);
	  	L[i]=l;
	  	LL[a]=l;
	  	if(a!=1)
	  	  flag=false;
	  	add(a,b,l);
	  	add(b,a,l);
	  	d[a]++;
	  	d[b]++;
	  }
	if(m==1)
	  {
	  	solvem();
	  }
	else
	  {
	    if(flag)
	      solvef();
	    else
	      solve();
	  }
	return 0;
}
