#include<bits/stdc++.h>
using namespace std;
int a[110];
bool full[25010];
void solve()
{
	memset(full,false,sizeof(full));
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	  scanf("%d",&a[i]);
	sort(a+1,a+1+n);
	int ans=0;
	full[0]=true;
	for(int i=1;i<=n;i++)
	  {
	  	if(!full[a[i]])
	  	  ans++;
	  	for(int j=a[i];j<=a[n];j++)
	  	  {
	  	  	if(full[j-a[i]])
	  	  	  full[j]=true;
		  }
	  }
	printf("%d\n",ans);
	return ;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t;
	cin>>t;
	while(t--)
	  {
	  	solve();
	  }
	return 0;
}
