#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int a[10000],n,cnt,maxnum;
bool b[100000];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		cnt=0;
		memset(b,0,sizeof(b));
		scanf("%d",&n);
		for (int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		maxnum=a[n];
		cnt=n;
		for (int i=1;i<=n;i++)
		if (b[a[i]]) cnt--;
		else {
			b[a[i]]=1;
			for (int j=a[i]+1;j<=maxnum;j++)
				if (!b[j]) b[j]=b[j-a[i]];
		} 
		printf("%d\n",cnt);
	}
	
	return 0;
}
