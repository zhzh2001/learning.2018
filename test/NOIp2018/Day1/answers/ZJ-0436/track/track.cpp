#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
bool b[100010];
int a[100010];
int point,len;
int n,m;
int cnt=0,head[100010],nxt[100010],to[100010],w[100010];
int f[100010],c[100010],longest=0,cut[100010];

void search(int k,int num){
	if (k>n-1){
		if (num<=m-1) return;
		int tmp=0,tans=1<<30;
		cut[0]=1;
		for (int i=1;i<n;i++)
		if (cut[i-1]!=cut[i]){
			tans=min(tmp,tans);
			tmp=c[i];
		}else tmp+=c[i];
		longest=max(longest,tans);
		return;
	}
	if (num<=m-1&&cut[k-1]==num){
		cut[k]=num+1;
		search(k+1,num+1);
	}
	cut[k]=num;
	search(k+1,num);
}

void add(int x,int y,int z){
	cnt++;
	w[cnt]=z;
	to[cnt]=y;
	nxt[cnt]=head[x];
	head[x]=cnt;
}
void dfs(int k,int dis){
	if (dis>len){
		len=dis;point=k;
	}
	for (int i=head[k];i;i=nxt[i])
		if (b[to[i]]){
			b[to[i]]=0;
			dfs(to[i],dis+w[i]);
			b[to[i]]=1;
		}
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int sum=0;
	memset(head,0,sizeof(head));
	memset(nxt,0,sizeof(nxt));
	scanf("%d%d",&n,&m);
	int x,y,z,pd_ai_1=1;
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		c[min(x,y)]=z;
		sum+=z;
		if (x!=1&&y!=1) pd_ai_1=0;
		add(x,y,z);
		add(y,x,z);
	}
	if (m==1){
		len=0;
		memset(b,1,sizeof(b));
		b[1]=0;
		dfs(1,0);
		int farest_point=point;
		
		len=0;
		memset(b,1,sizeof(b));
		b[farest_point]=0;
		dfs(farest_point,0);
		printf("%d\n",len);
	} else
	if (pd_ai_1){
		for (int i=1;i<n;i++)
			f[i]=w[i*2];
		sort(f+1,f+n);
		for (int i=n-1;i>=n-m;i--)
			a[m-n+1+i]=f[i];
		for (int i=1;i<=m;i++)
			cout<<a[i]<<';';
		for (int i=n-1-m;i>=1&&n-1-i-m<m;i--)
			a[n-m-i]+=f[i];
		int minst_num1=1<<30;
		for (int i=1;i<=m;i++)
			minst_num1=min(minst_num1,a[i]);
		printf("%d\n",minst_num1);
	}else
	if (n<=20){
		cut[0]=0;
		search(1,1);
		printf("%d\n",longest);
	}else{
		printf("%d\n",(int)sum/m);
	}
	return 0;
}
