#include<bits/stdc++.h>
using namespace std;

int T,n,can[25100],x;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);

	scanf("%d",&T);
	while(T--)
	{
		int mx=0;
		scanf("%d",&n);memset(can,0,sizeof can);can[0]=1;
		for(int i=1;i<=n;i++)scanf("%d",&x),can[x]=1,mx=max(mx,x);
		int ans=0;
		for(int i=1;i<=mx;i++)
		{
			int ok=0;
			for(int j=1;j<i;j++)ok|=can[j]&&can[i-j];
			if(!ok&&can[i])ans++;//cout<<i<<' ';
			can[i]|=ok;
		}
		printf("%d\n",ans);
	}
	return 0;
}
