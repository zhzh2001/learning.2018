#include<bits/stdc++.h>
using namespace std;

const int N=1e5+10;
int n,m,x,y,z;
struct link
{
	int top,fi[N],ne[N],la[N],to[N],l[N];
	void add(int x,int y,int z)
	{
		top++,to[top]=y,l[top]=z;
		if(fi[x]==0)fi[x]=top;else ne[la[x]]=top;
		la[x]=top;
	}
	void adde(int x,int y,int z)
	{
		add(x,y,z);
		add(y,x,z);
	}
}L;
int tmp[N];
vector<int> son[N],used[N];
vector<pair<int,int> > ch[N];
pair<int,int> dfs(int now,int fa,int mn)
{
	//cerr<<now<<' ';
	int tot=0,topson=0,topch=0;
	for(int i=L.fi[now];i;i=L.ne[i])
	if(L.to[i]!=fa)
	{
		pair<int,int> tmp=dfs(L.to[i],now,mn);
		tot+=tmp.first;
		if(tmp.second+L.l[i]<mn)
		{
			topson++;
			if((int)son[now].size()<=topson)
			son[now].push_back(tmp.second+L.l[i]),used[now].push_back(0);
			else son[now][topson]=tmp.second+L.l[i],used[now][topson]=0;
		}
		else tot++;
	}
	for(int i=1;i<=topson;i++)tmp[i]=son[now][i];sort(tmp+1,tmp+1+topson);
	for(int i=1;i<=topson;i++)son[now][i]=tmp[i];

	//cout<<now<<":  ";
	//for(int i=0;i<son.size();i++)cout<<son[i]<<' ';puts("");
	int len=topson;
	if(len==0)return make_pair(tot,0);
	int L=1,R=len;//mx=son[L];
	while(L<R)
	{
		if(son[now][L]+son[now][R]<mn)
		{
			L++;
		}else
		{
			used[now][L]=used[now][R]=1;
			topch++;
			if((int)ch[now].size()<=topch)
				ch[now].push_back(make_pair(L,R));
			else ch[now][topch]=make_pair(L,R);
			L++,R--;
			tot++;
		}
	}
	int mx=0;
	for(int i=len;i>=1;i--)if(!used[now][i]){mx=son[now][i];break;}
	if(topch)
	for(int i=1;i<=topch;i++)if(son[now][ch[now][i].first]+mx>=mn)mx=max(son[now][ch[now][i].second],mx);
	//for(int i=0;i<son.size();i++)cout<<used[i]<<' ';puts("");
	//cout<<now<<' '<<tot<<' '<<mx<<endl;
	return make_pair(tot,mx);
}
int ok(int x)
{
	pair<int,int> mx=dfs(1,0,x);
	return mx.first>=m;
}
int ans;
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);

	scanf("%d%d",&n,&m);for(int i=1;i<=n;i++)son[i].push_back(0),used[i].push_back(0),ch[i].push_back(make_pair(0,0));
	for(int i=1;i<n;i++)scanf("%d%d%d",&x,&y,&z),L.adde(x,y,z);
	int l=0,r=500000000;
	while(l<=r)
	{
		int mid=(l+r)>>1;
		if(ok(mid))ans=mid,l=mid+1;
		else r=mid-1;
	}

	printf("%d",ans);
	return 0;
}
