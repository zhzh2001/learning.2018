#include <bits/stdc++.h>
#define For(a,b,c) for(int a=b;a<=c;++a)
using namespace std;
const int N=50100;
int n,m,x,y,z,tt,hd[N],nt[N*2],to[N*2],go[N*2],du[N];
bool f2=true;
struct _1 {
	int r,D[N];
	void fly (int u,int fa) {
		if (D[u]>D[r]) r=u;
		for(int i=hd[u];i;i=nt[i])
		if (to[i]!=fa) {
			D[to[i]]=D[u]+go[i];
			fly(to[i],u);
		}
	}
	void solve () {
		r=1;
		D[1]=0,fly(1,-1);
		D[r]=0,fly(r,-1);
		printf("%d",D[r]);
	}
}P1;
struct _2 {
	int l,r,mid,A[N];
	bool fly (int x) {
		int cnt=0,s=0;
		For(i,1,n-1) {
			s+=A[i];
			if (s>=x) ++cnt,s=0;
		}
		return cnt>=m;
	}
	void solve () {
		l=r=0;
		For(i,1,n-1) {
			A[i]=go[hd[i]];
			r+=A[i];
		}
		while (l<=r) {
			mid=(l+r)>>1;
			if (fly(mid)) l=mid+1;
			else r=mid-1;
		}
		if (l==0) l=1;
		printf("%d",l-1);
	}
}P2;
struct _3 {
	int l,r,mid,cnt,rt;
	int fly (int u,int fa) {
		int fi=0,se=0;
		for(int i=hd[u];i;i=nt[i])
		if (to[i]!=fa) {
			x=fly(to[i],u)+go[i];
			if (x>fi) se=fi,fi=x;
			else if (x>se) se=x;
		}
		if (fi>=mid&&se>=mid) {
			cnt+=2;
			return 0;
		}
		else if (fi>=mid) {
			++cnt;
			return se;
		}
		if (fi+se>=mid) {
			++cnt;
			return 0;
		}
		return fi;
	}
	bool boom () {
		cnt=0;
		fly(rt,-1);
		return cnt>=m;
	}
	void solve () {
		rt=1;
		l=0;
		r=tt;
		For(i,1,n)
		if (du[i]==1) {
			rt=i;
			break;
		}
		while (l<=r) {
			mid=(l+r)>>1;
			if (boom()) l=mid+1;
			else r=mid-1;
		}
		if (l==0) l=1;
		printf("%d",l-1);
	}
}P3;
int main () {//mem
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	For(i,1,n-1) {
		scanf("%d%d%d",&x,&y,&z),tt+=z,++du[x],++du[y],
		nt[i*2-1]=hd[x],hd[x]=i*2-1,to[i*2-1]=y,go[i*2-1]=z,
		nt[i*2]=hd[y],hd[y]=i*2,to[i*2]=x,go[i*2]=z;
		if (y!=x+1) f2=false;
	}
	if (m==1) P1.solve();
	else if (f2) P2.solve();
	else P3.solve();
	return 0;
}
