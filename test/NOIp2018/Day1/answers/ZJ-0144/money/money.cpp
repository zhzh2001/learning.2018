#include <bits/stdc++.h>
#define For(a,b,c) for(int a=b;a<=c;++a)
#define Dor(a,b,c) for(int a=b;a>=c;--a)
using namespace std;
const int N=110,M=25100;
int T,n,mx,ans,A[N];
bool F[N][M],G[N][M];
int main () {//mem init
//	cerr<<sizeof(F)*2/1048576.0<<endl;
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		scanf("%d",&n);
		For(i,1,n) scanf("%d",&A[i]);
		sort(A+1,A+1+n);
		n=unique(A+1,A+1+n)-A-1;
		mx=A[n];
		memset(F,false,sizeof(F));
		memset(G,false,sizeof(G));
		F[0][0]=G[n+1][0]=true;
		ans=n;
		For(i,1,n) {
			For(j,0,mx) F[i][j]=F[i-1][j];
			For(j,A[i],mx)
				if (F[i][j-A[i]]) F[i][j]=true;
		}
		Dor(i,n,1) {
			For(j,0,mx) G[i][j]=G[i+1][j];
			For(j,A[i],mx)
				if (G[i][j-A[i]]) G[i][j]=true;
		}
		For(i,1,n)
			For(j,0,A[i])
			if (F[i-1][j]&&G[i+1][A[i]-j]) {
				--ans;
				break;
			}
		printf("%d\n",ans);
	}
	return 0;
}
