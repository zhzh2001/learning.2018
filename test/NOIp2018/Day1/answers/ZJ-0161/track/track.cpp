#include <bits/stdc++.h>
using namespace std;

void file() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
}

const int N = 60000 + 5, M = N << 1;

int n, toit[M], nxt[M], link[N], cnt = 0, val[M], m;

void AddEdge(int x, int y, int z) {
	++ cnt; toit[cnt] = y; nxt[cnt] = link[x]; link[x] = cnt; val[cnt] = z;
}

struct node {
	int u, v, w;
	void init() {
		scanf("%d%d%d", &u, &v, &w);
	}
	bool operator < (const node & rhs) const {
		return u < rhs.u;
	}
} Edge[M];


int Subtask1_dis[N]; queue <int> Subtask1_Q;
int Subtask1_bfs(int s) {
	memset(Subtask1_dis, 255, sizeof(Subtask1_dis));
	Subtask1_dis[s] = 0;
	while (! Subtask1_Q.empty()) Subtask1_Q.pop();
	Subtask1_Q.push(s);
	while (! Subtask1_Q.empty()) {
		int x = Subtask1_Q.front(); Subtask1_Q.pop();
		for (int i = link[x]; i; i = nxt[i]) {
			int v = toit[i]; if (Subtask1_dis[v] != -1) continue;
			Subtask1_dis[v] = Subtask1_dis[x] + val[i];
			Subtask1_Q.push(v);
		}
	}
	int pos = 1;
	for (int i = 1; i <= n; ++ i) {
		if (Subtask1_dis[i] > Subtask1_dis[pos]) pos = i;
	}
	return pos;
}

void Subtask1_Solve() {
	for (int i = 1; i < n; ++ i) {
		AddEdge(Edge[i].u, Edge[i].v, Edge[i].w);
		AddEdge(Edge[i].v, Edge[i].u, Edge[i].w);
	}
	int s = Subtask1_bfs(1), t = Subtask1_bfs(s);
	printf("%d\n", Subtask1_dis[t]);		
}

bool check2() {
	for (int i = 1; i < n; ++ i) if (Edge[i].u != 1) return false;
	return true;
}

int Subtask2_a[N], Subtask2_b[N];
void Subtask2_Solve() {
	int tot = n - 1;
	for (int i = 1; i <= tot; ++ i) Subtask2_a[i] = Edge[i].w;
	sort(Subtask2_a + 1, Subtask2_a + 1 + tot);
	if (m <= tot/2) {
		int L = tot - 2 * m + 1, R = tot, sz = 0;
		for (; L < R; ++ L, -- R) {
			++ sz; Subtask2_b[sz] = Subtask2_a[L] + Subtask2_a[R];
		}
		sort(Subtask2_b + 1, Subtask2_b + 1 + sz);
		printf("%d\n", Subtask2_b[1]);
	} else {
		for (int i = 1; i <= m; ++ i) Subtask2_b[i] = Subtask2_a[tot - i + 1];
		sort(Subtask2_b + 1, Subtask2_b + 1 + m);
		int last = tot - m;
		for (int i = 1; i <= m; ++ i) {
			if (last == 0) break;
			Subtask2_b[i] += Subtask2_a[last]; -- last;
		}
		sort(Subtask2_b + 1, Subtask2_b + 1 + m);
		printf("%d\n", Subtask2_b[1]);
	}
}

bool check3() {
	for (int i = 1; i < n; ++ i) {
		if (Edge[i].v != Edge[i].u + 1) return false;
	}
	return true;
}

int Subtask3_a[N];
bool Subtask3_check(int mid) {
	int sum = 0, cur = 0;
	for (int i = 1; i < n; ++ i) {
		if (sum >= mid) {
			++ cur; sum = Subtask3_a[i];
		} else {
			sum += Subtask3_a[i];
		}
	}
	return cur >= m;
}

void Subtask3_Solve() {
	int tot = n - 1;
	sort(Edge + 1, Edge + 1 + tot);
	for (int i = 1; i <= tot; ++ i) {
		Subtask3_a[i] = Edge[i].w;
	}
	int L = 1, R = 0, ans = 1;
	for (int i = 1; i <= tot; ++ i) R += Subtask3_a[i];
	
	while (L <= R) {
		int mid = (L + R) >> 1;
		if (Subtask3_check(mid)) {
			L = mid + 1;
			ans = mid;
		} else R = mid - 1;
	}	
	printf("%d\n", ans);
}


int main() {
	file();
	scanf("%d%d", &n, &m);
	for (int i = 1; i < n; ++ i) {
		Edge[i].init();
	}
	if (m == 1) {
		Subtask1_Solve();
		return 0;
	}
	if (check2()) {
		Subtask2_Solve();
		return 0;
	}
	if (check3()) {
		Subtask3_Solve();
		return 0;
	}
	return 0;
}
