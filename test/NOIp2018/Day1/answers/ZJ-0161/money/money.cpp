#include <bits/stdc++.h>
using namespace std;

void file() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
}

const int N = 100 + 5, M = 25000 + 50;

int n, a[N]; bool dp[M];

void Solve() {
	scanf("%d", &n);
	for (int i = 1; i <= n; ++ i) {
		scanf("%d", &a[i]);
	}
	sort(a + 1, a + 1 + n);
	memset(dp, false, sizeof(dp));
	dp[0] = true;
	int ans = 0;
	for (int i = 1; i <= n; ++ i) {
		if (dp[a[i]]) continue;
		++ ans;
		for (int j = 0; j <= 25000; ++ j) if (dp[j]){
			if (j + a[i] > 25000) break;
			dp[j + a[i]] = true;
		}
	}
	printf("%d\n", ans);
}

int main() {
	file();
	int T; scanf("%d", &T);
	while (T --) {
		Solve();
	}
	return 0;
}
