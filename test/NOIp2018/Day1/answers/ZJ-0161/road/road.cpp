#include <bits/stdc++.h>
using namespace std;

const int N = 100000 + 5;

void file() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
}

int n; long long a[N];

int main() {
	file();
	scanf("%d", &n);
	for (int i = 1; i <= n; ++ i) {
		scanf("%lld", &a[i]);
	}
	a[0] = 0;
	for (int i = n + 1; i >= 1; -- i) {
		a[i] -= a[i - 1];
	}
	long long ans = 0;
	for (int i = 1; i <= n + 1; ++ i) {
		if (a[i] > 0) ans += a[i];
	}
	printf("%lld\n", ans);
	return 0;
}
