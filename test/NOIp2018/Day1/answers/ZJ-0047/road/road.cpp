#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define int long long
using namespace std;
int read()
{
	int x=0,w=0;char c=0;
	while(c<'0' || c>'9') w|=(c=='-'),c=getchar();
	while(c>='0' && c<='9') x=x*10+c-'0',c=getchar();
	return w?-x:x;
}
void write(int x)
{
	if(x<0) putchar('-'),x=-x;
	if(x==0) {putchar('0');return;}
	if(x/10>0) write(x/10);
	putchar('0'+x%10);
}
int n;
int d[100100],lg[100100];
int f[23][200100],p[23][200100];
int dfs(int l,int r,int sum)
{
	if(l>r) return 0;
	if(l==r) return d[l]-sum;
	int k=lg[r-l+1],mid,tmp;
	if(f[k][l]<f[k][r-(1<<(k))+1]) tmp=f[k][l],mid=p[k][l];
	else tmp=f[k][r-(1<<(k))+1],mid=p[k][r-(1<<(k))+1];
	return (tmp-sum)+dfs(l,mid-1,tmp)+dfs(mid+1,r,tmp);
}
signed main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	memset(f,0x7f,sizeof(f));
	n=read();
	for(int i=1;i<=n;i++)
		d[i]=read(),f[0][i]=d[i],p[0][i]=i;
	for(int i=1;i<=n;i++)
		lg[i]=lg[i-1]+(i>=(1<<(lg[i-1]+1)));
	for(int i=1;i<=20;i++)
		for(int j=1;j<=n;j++)
		{
			if(f[i-1][j]<f[i-1][j+(1<<(i-1))])
			{
				f[i][j]=f[i-1][j];
				p[i][j]=p[i-1][j];
			}
			else
			{
				f[i][j]=f[i-1][j+(1<<(i-1))];
				p[i][j]=p[i-1][j+(1<<(i-1))];
			}
		}
	int ans=dfs(1,n,0);
	write(ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
