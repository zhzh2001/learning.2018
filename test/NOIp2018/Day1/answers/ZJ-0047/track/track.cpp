#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define int long long
using namespace std;
int read()
{
	int x=0,w=0;char c=0;
	while(c<'0' || c>'9') w|=(c=='-'),c=getchar();
	while(c>='0' && c<='9') x=x*10+c-'0',c=getchar();
	return w?-x:x;
}
void write(int x)
{
	if(x<0) putchar('-'),x=-x;
	if(x==0) {putchar('0');return;}
	if(x/10>0) write(x/10);
	putchar('0'+x%10);
}
int n,m,sum=0;
int dp[1100][1100],to[50100];
int f[50010],e[100010];
bool check1(int x)
{
	memset(f,0,sizeof(f));
	int cnt=0;
	for(int i=1;i<=n;i++)
	{
		if(f[i]) continue;
		if(e[i]>=x) {cnt++;continue;}
		int j=i+1;
		while(e[j]+e[i]<x && j<n) j++;
		while(j<n && f[j]) j++;
		if(j>=n) continue;
		f[i]=1;f[j]=1;
		cnt++;
	}
	return cnt>=m;
}
bool check2(int x)
{
	int i=1,j=1,cnt=0;
	while(cnt<=m && j<n)
	{
		int res=0;
		while(res<x && j<n) res+=to[j],j++;
		if(res>=x) cnt++;
	}
	return cnt>=m;
}
signed main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	int flag1=1,flag2=1;
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read(),z=read();
		sum+=z;e[i]=z;to[x]=z;
		if(x!=1) flag1=0;
		if(y!=x+1) flag2=0;
	}
	if(m==1) {write(sum);return 0;}
	if(flag1)
	{
		sort(e+1,e+n);
		if(n-1>=m*2)
		{
			int ans=0x7ffffff,k=n-m*2;
			for(int i=n-1;i>=(k+n-1)/2;i--)
				ans=min(ans,e[i]+e[k+n-1-i]);
			write(ans);
		}
		else
		{
			int l=0,r=sum;
			while(l<r)
			{
				int mid=(l+r)/2+1;
				if(check1(mid)) l=mid;
				else r=mid-1;
			}
			write(l);
		}
	}
	if(flag2)
	{
		int l=0,r=sum;
		while(l<r)
		{
			int mid=(l+r)/2+1;
			if(check2(mid)) l=mid;
			else r=mid-1;
		}
		write(l);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
