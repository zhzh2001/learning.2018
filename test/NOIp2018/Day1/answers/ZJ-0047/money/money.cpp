#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int read()
{
	int x=0,w=0;char c=0;
	while(c<'0' || c>'9') w|=(c=='-'),c=getchar();
	while(c>='0' && c<='9') x=x*10+c-'0',c=getchar();
	return w?-x:x;
}
void write(int x)
{
	if(x<0) putchar('-'),x=-x;
	if(x==0) {putchar('0');return;}
	if(x/10>0) write(x/10);
	putchar('0'+x%10);
}
int T,n,m;
int p[25100];
int a[250100],f[25100],b[110];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--)
	{
		memset(f,0,sizeof(f));
		memset(p,0,sizeof(p));
		n=read();m=0;
		for(int i=1;i<=n;i++)
			b[i]=a[i]=read(),m=max(m,a[i]),f[a[i]]=2,p[a[i]]=1;
		int cnt=n;
		for(int i=1;i<=n;i++)
			for(int j=2;a[i]*j<=m;j++)
			{
				f[a[i]*j]=1;
				if(!p[a[i]*j]) a[++cnt]=a[i]*j,p[a[i]*j]=1;
			}
		sort(a+1,a+cnt+1);
		for(int i=a[1]+1;i<=m;i++)
		{
			if(f[i]==1) continue;
			for(int j=1;j<=cnt && a[j]<=i;j++)
				if(f[i-a[j]]) {f[i]=1;break;}
		}
		int ans=n;
		for(int i=1;i<=n;i++)
			if(f[b[i]]==1) ans--;
		write(ans),puts("");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
