#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define rep(i,a,b) for(int i=a;i<=b;++i)
#define rpd(i,a,b) for(int i=a;i>=b;--i)
using namespace std;

const int N=100000+5;

int n,ans=0;int d[N];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	rep(i,1,n)scanf("%d",&d[i]);
	d[0]=0;
	rep(i,1,n)if(d[i]>d[i-1])ans+=d[i]-d[i-1];
	printf("%d\n",ans);
	return 0;
}
