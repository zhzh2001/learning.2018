#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstdlib>
#define rep(i,a,b) for(int i=a;i<=b;++i)
#define rpd(i,a,b) for(int i=a;i>=b;--i)
#define rep1(i,x) for(int i=head[x];i;i=nxt[i])
using namespace std;

const int N=50000+5;

int n,m,tot;bool f=1,g=1;int sum;int t[N],kt[N];
int head[N],nxt[N<<1],b[N<<1],to[N<<1];
void add(int u,int v,int w){
	nxt[++tot]=head[u];head[u]=tot;to[tot]=v;b[tot]=w;
}
void Max(int &x,int y){x=max(x,y);}
int down[N][3],up[N];
void dfs_down(int x,int fa){
	rep1(i,x){
		int p=to[i];if(p==fa)continue;
		dfs_down(p,x);int k=down[p][0]+b[i];
		if(k>down[x][0])down[x][1]=down[x][0],down[x][0]=k;
		else Max(down[x][1],k);
	}
}
void dfs_up(int x,int fa,int bian){
	int k=0;
	if(down[x][0]+bian==down[fa][0])Max(k,down[fa][1]);
	else Max(k,down[fa][0]);Max(k,up[fa]);
	up[x]=k+bian;
	rep1(i,x){
		int p=to[i];if(p==fa)continue;
		dfs_up(p,x,b[i]);
	}
}
bool pd(int x){
	int now=0,s=0;
	rep(i,1,n-1){
		now+=t[i];
		if(now>=x)now=0,s+=1;
	}
	return s>=m;
}
bool pdf(int x){
	int l=2,s=0;
	rpd(i,n,2){
		if(kt[i]>=x){s+=1;continue;}
		while(l<i&&kt[i]+kt[l]<x)l+=1;
		if(l>=i)break;
		s+=1;l+=1;
		if(l>=i)break;
	}
	return s>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	rep(i,1,n-1){
		int u,v,w;scanf("%d%d%d",&u,&v,&w);
		add(u,v,w);add(v,u,w);sum+=w;t[i]=w;kt[v]=w;
		if(v!=u+1)f=0;
		if(u!=1)g=0;
	}
	if(f){
		int l=0,r=sum,ans=0;
		while(l<=r){
			int mid=(l+r)>>1;
			if(pd(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	if(g){
		kt[1]=0;
		sort(kt+1,kt+n+1);
		int l=0,r=sum,ans=0;
		while(l<=r){
			int mid=(l+r)>>1;
			if(pdf(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	dfs_down(1,0);dfs_up(1,0,0);
	int ans=0;
	rep(i,1,n)Max(ans,down[i][0]+up[i]);
	printf("%d\n",ans);
	return 0;
}
