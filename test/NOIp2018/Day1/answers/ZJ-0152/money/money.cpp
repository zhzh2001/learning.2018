#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define rep(i,a,b) for(int i=a;i<=b;++i)
#define rpd(i,a,b) for(int i=a;i>=b;--i)
using namespace std;

const int N=100+5,M=25000+5;

int T,n;int a[N],b[N];bool f[M];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		memset(a,0,sizeof(a));
		rep(i,1,n)scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		memset(b,0,sizeof(b));
		memset(f,false,sizeof(f));
		int ans=1;b[1]=a[1];f[0]=1;
		rep(k,a[1],a[n])f[k]|=f[k-a[1]];
		rep(i,2,n){
			if(f[a[i]])continue;
			ans+=1;b[ans]=a[i];
			rep(k,b[ans],a[n])f[k]|=f[k-b[ans]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
