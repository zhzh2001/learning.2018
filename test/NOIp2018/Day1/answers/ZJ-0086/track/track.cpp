#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cmath>
#include<cctype>
#include<cstring>
#include<string>
#include<vector>
#include<map>
#include<queue>
#include<cstdlib>
#define N 50005
using namespace std;
struct edge{
	int to,next,w;
}e[N<<1];
int ct=1,head[N];
int n,m,vis[N],mx=-1,verg;
int id=0,b[N];
int re(){
	int x=0,t=1;char c=getchar();
	for(;!isdigit(c);c=getchar())if(c=='-')t=-1;
	for(;isdigit(c);c=getchar())x=x*10+c-'0';
	return x*t;
}
void add(int x,int y,int z){
	e[ct].to=y;
	e[ct].next=head[x];
	e[ct].w=z;
	head[x]=ct++;
	e[ct].to=x;
	e[ct].next=head[y];
	e[ct].w=z;
	head[y]=ct++;
}
void dfs(int k,int now){
	vis[k]=1;
	if(now>mx){
		mx=now;
		verg=k;
	}
	for(int i=head[k],tmp;i;i=e[i].next){
		tmp=e[i].to;
		if(!vis[tmp])dfs(tmp,now+e[i].w);
	}
}
void dfs2(int k){
	vis[k]=1;
	for(int i=head[k],tmp;i;i=e[i].next){
		tmp=e[i].to;
		if(!vis[tmp]){
			b[++id]=e[i].w;
			dfs2(tmp);
		}
	}
}
bool check(int len){
	int num=0,now=0;
	for(int i=1;i<=n;i++){
		if(now<len)now+=b[i];
		else {
			num++;
			now=b[i];
		}
	}
	if(num>=m)return 1;
	return 0;
}
int bi(){
	int l=1,r=500000000,mid,ans;
	while(l<=r){
		mid=(l+r)>>1;
		if(check(mid)){
			ans=mid;
			l=mid+1;
		}
		else r=mid-1;
	}
	return ans;
}
bool ck2(int len){
	int l=1,r=n-1,num=0;
	while(b[r]>=len)num++,r--;
	while(l<r){
		while(b[l]+b[r]<len)l++;
		if(l>=r)break;
		r--;
		num++;
	}
	if(num>=m)return 1;
	return 0;
}
int bi2(){
	int l=1,r=500000000,mid,ans;
	while(l<=r){
		mid=(l+r)>>1;
		if(ck2(mid)){
			ans=mid;
			l=mid+1;
		}
		else r=mid-1;
	}
	return ans;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=re();m=re();
	int fl1=1,fl2=1;
	for(int i=1;i<n;i++){
		int x=re(),y=re(),z=re();
		if(y!=x+1)fl2=0;
		if(x!=1)fl1=0;
		add(x,y,z);
	}
	if(m==1){
		dfs(1,0);
		mx=-1;
		memset(vis,0,sizeof vis);
		dfs(verg,0);
		printf("%d\n",mx);
		return 0;
	}
	else if(fl2){
		int lp;
		for(int i=1;i<=n;i++)
			if(!e[head[i]].next){
				lp=i;
				break;
			}
		dfs2(lp);
		printf("%d\n",bi());
		return 0;
	}
	else if(fl1){
		int qaq=0;
		for(int i=head[1];i;i=e[i].next)
			b[++qaq]=e[i].w;
		sort(b+1,b+n);
		printf("%d\n",bi2());
		return 0;
	}
	else {
		int qwq=0;
		for(int i=1;i<=n;i++)
			for(int j=head[i];j;j=e[j].next)
				qwq+=e[j].w;
		srand(19260817);
		int tmp=(rand()*233+rand())%((int)(qwq*0.0618))+12;
		printf("%d\n",tmp);
	}
	return 0;
}
