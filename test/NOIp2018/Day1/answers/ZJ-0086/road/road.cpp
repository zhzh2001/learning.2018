#include<iostream>
#include<cstdio>
#include<cmath>
#include<cctype>
#include<cstring>
#include<string>
#include<vector>
#include<map>
#include<queue>
#include<cstdlib>
#define N 100005
using namespace std;
int n,a[N];
long long ans=0;
int re(){
	int x=0,t=1;char c=getchar();
	for(;!isdigit(c);c=getchar())if(c=='-')t=-1;
	for(;isdigit(c);c=getchar())x=x*10+c-'0';
	return x*t;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=re();
	for(int i=1;i<=n;i++)a[i]=re();
	int now=a[1];
	for(int i=2;i<=n;i++)
	{
		if(a[i]<now)ans+=(long long)now-a[i];
		now=a[i];
	}
	ans+=(long long)a[n];
	printf("%lld\n",ans);
	return 0;
}
