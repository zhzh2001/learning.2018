#include<bits/stdc++.h>
using namespace std;
#define res register int
#define inf 0x3f3f3f3f
#define eps 1e-15
#define LL long long
inline int read(){
	res s=0,w=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')w=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')s=s*10+ch-'0',ch=getchar();
	return s*w;
}
const int N=1e5+10;
namespace MAIN{
	int n,las;
	int ans;
	inline void MAIN(){
		n=read();
		for(res i=1;i<=n;i++){
			res x=read(),y=x;
			x-=las;
			if(x>0)ans+=x;
			las=y;
		}
		printf("%d\n",ans);
	}
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	MAIN::MAIN();
	return 0;
}
