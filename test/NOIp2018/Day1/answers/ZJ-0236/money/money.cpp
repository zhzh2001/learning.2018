#include<bits/stdc++.h>
using namespace std;
#define res register int
#define inf 0x3f3f3f3f
#define eps 1e-15
#define LL long long
inline int read(){
	res s=0,w=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')w=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')s=s*10+ch-'0',ch=getchar();
	return s*w;
}
const int N=1e2+10,V=25000+10; 
namespace MAIN{
	int n,tot;
	int a[N],maxa;
	int dp[V];
	int T;
	inline void MAIN(){
		T=read();
		while(T--){ 
			tot=n=read();
			for(res i=1;i<=n;i++)a[i]=read(),maxa=max(maxa,a[i]);
			dp[0]=1;
			for(res i=1;i<=n;i++)
				for(res j=a[i];j<=maxa;j++)dp[j]+=dp[j-a[i]];
			for(res i=1;i<=n;i++)if(dp[a[i]]>=2)tot--;
			for(res i=0;i<=maxa;i++)dp[i]=0; 
			printf("%d\n",tot);
		} 
	}
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	MAIN::MAIN();
	return 0;
}
