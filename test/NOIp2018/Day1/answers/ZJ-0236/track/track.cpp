#include<bits/stdc++.h>
using namespace std;
#define res register int
#define inf 0x3f3f3f3f
#define eps 1e-15
#define LL long long
inline int read(){
	res s=0,w=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')w=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')s=s*10+ch-'0',ch=getchar();
	return s*w;
}
const int N=5e4+10;
namespace MAIN{
	int n,m;
	struct E{
		int next,to,val;
		E() {}
		E(res next,res to,res val):next(next),to(to),val(val) {}
	}edge[N<<1];
	int head[N],cnt;
	inline void addedge(const res &u,const res &v,const res &w){
		edge[++cnt]=E(head[u],v,w),head[u]=cnt;
		edge[++cnt]=E(head[v],u,w),head[v]=cnt;
	}
	namespace subtask0{
		int dis[N],rt;
		void dfs(const res &x,const res &fax){
			if(dis[x]>dis[rt])rt=x;
			for(res i=head[x];~i;i=edge[i].next){
				res tox=edge[i].to;
				if(tox==fax)continue;
				dis[tox]=dis[x]+edge[i].val;
				dfs(tox,x);
			}
		}
		inline void MAIN(){
			dfs(1,0);
			res Rt=rt;
			rt=0;
			memset(dis,0,sizeof(dis));
			dfs(Rt,0);
			printf("%d\n",dis[rt]);
		}
	}
	bool flag;
	int sum;
	namespace subtask1{
		int dis[N];
		void dfs(const res &x,const res &fax){
			for(res i=head[x];~i;i=edge[i].next){
				res tox=edge[i].to;
				if(tox==fax)continue;
				dis[tox]=edge[i].val;
				dfs(tox,x);
			}
		}
		inline bool check(const res &lim){
			res ret=0,num=0;
			for(res i=2;i<=n;i++){
				ret+=dis[i];
				if(ret>=lim)ret=0,num++;
			}
			return num>=m;
		}
		inline void MAIN(){
			dfs(1,0);
			res l=0,r=sum,ret;
			while(l<=r){
				res mid=(l+r)>>1;
				if(check(mid))l=mid+1,ret=mid;
				else r=mid-1;
			}
			printf("%d\n",ret);
		}
	}
	namespace subtask2{
		int dis[N];
		typedef pair<int,int> Pair;
		#define mp make_pair
		#define fi first
		#define se second 
		#define pb push_back
		bool pts[N];
		Pair dfs(const res &x,const res &fax,const res &lim){
			res ret=0,num=0;
			vector<int> vec;
			for(res i=head[x];~i;i=edge[i].next){
				res tox=edge[i].to;
				if(tox==fax)continue;
				dis[tox]=edge[i].val;
				Pair qaq=dfs(tox,x,lim);
				if(qaq.fi)vec.pb(qaq.fi);
				num+=qaq.se;
			}
			sort(vec.begin(),vec.end());
			res l=0,r=vec.size()-1;
			while(l<r){
				if(vec[l]+vec[r]>=lim)num++,pts[l]=1,pts[r]=1,l++,r--;
				else l++;
			}
			for(res i=0,siz=vec.size();i<siz;i++){
				if(!pts[i])ret=max(ret,vec[i]);
				pts[i]=0;
			}
			if(ret+dis[x]>=lim)return mp(0,num+1);
			return mp(ret+dis[x],num);
		}
		inline void MAIN(){
			res l=0,r=sum,ret;
			while(l<=r){
				res mid=(l+r)>>1;
				if(dfs(1,0,mid).se>=m)l=mid+1,ret=mid;
				else r=mid-1;
			}
			printf("%d\n",ret);
		}
	}
	inline void MAIN(){
		memset(head,-1,sizeof(head));
		n=read(),m=read();
		for(res i=1;i<n;i++){
			res u=read(),v=read(),w=read();
			addedge(u,v,w);
			sum+=w;
			if(v!=u+1)flag=1;
		}
		if(m==1)subtask0::MAIN();
		else if(!flag)subtask1::MAIN();
		else subtask2::MAIN();
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	MAIN::MAIN();
	return 0;
}
