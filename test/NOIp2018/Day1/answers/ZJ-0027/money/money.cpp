#include<bits/stdc++.h>
#define maxn 25005
using namespace std;
inline int read(){
	int x=0,c;while(!isdigit(c=getchar()));
	while(x=x*10+c-'0',isdigit(c=getchar()));
	return x;
}
int n,a[maxn];
int f[maxn];
int main(){
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	int t=read();
	while(t--){
		n=read();int mx=0,tmp=0;
		for(int i=1;i<=n;i++)a[i]=read(),mx=max(mx,a[i]);
		for(int i=1;i<=n;i++)if(a[i]==1)tmp=1;
		if(tmp){printf("1\n");continue;}
		register int ans=n,i,j,k;
		for(i=1;i<=n;i++){
			memset(f,0,sizeof(f));
			f[0]=1;
			for(j=0;j<=a[i];j++){
				if(f[j])
				for(k=1;k<=n;k++)if(k!=i&&j+a[k]<=a[i]){
					f[j+a[k]]=1;
					if(j+a[k]==a[i])break;
				}
				if(f[a[i]])break;
			}
			if(f[a[i]])ans--;
		}
		printf("%d\n",ans);
	}return 0;
}
