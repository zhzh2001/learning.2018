#include<bits/stdc++.h>
#define maxn 50005
using namespace std;
inline int read(){
	int x=0,c;while(!isdigit(c=getchar()));
	while(x=x*10+c-'0',isdigit(c=getchar()));
	return x;
}
struct edge{
	int to,next,len;
}e[maxn<<1];
int last[maxn],cnt,n;
inline void insert(int x,int y,int z){
	e[++cnt].to=y;e[cnt].next=last[x];last[x]=cnt;e[cnt].len=z;
}
namespace work1{
	int dis[maxn];
	void dfs(int x,int fa){
		for(int j=last[x];j;j=e[j].next){
			int y=e[j].to;if(y==fa)continue;
			dis[y]=dis[x]+e[j].len;dfs(y,x);
		}
	}
	void Main(){
		dfs(1,0);int rt=0;
		for(int i=1;i<=n;i++)if(dis[i]>dis[rt])rt=i;
		dis[rt]=0;dfs(rt,0);int res=0;
		for(int i=1;i<=n;i++)if(dis[i]>res)res=dis[i];
		printf("%d\n",res);	
	}
}
int m;
struct date{
	int x,y,len;
}a[maxn];
namespace work2{
	inline bool cmp(date x,date y){return x.len<y.len;}
	inline bool check(int x){
		int res=0,tmp=0;
		for(int i=n-1,j=1;i>=j;i--){
			while(j<i&&a[i].len+a[j].len<x)j++;
			if(i==j)break;
			if(a[i].len+a[j].len>=x)res++,j++;else break;
		}
		return res>=m;
	}
	void Main(){
		sort(a+1,a+n,cmp);
		int l=0,r=20000,mid,res;
		while(l<=r){
			mid=(l+r)>>1;
			if(check(mid))res=mid,l=mid+1;else r=mid-1;
		}printf("%d\n",res);
	}
}
namespace work3{
	inline bool cmp2(date x,date y){return x.x<y.x;}
	inline bool check(int x){
		int res=0,tmp=0;
		for(int i=1;i<n;i++){
			tmp+=a[i].len;if(tmp>=x)res++,tmp=0;
		}
		return res>=m;
	}
	void Main(){
		sort(a+1,a+n,cmp2);
		int l=0,r=500000000,mid,res;
		while(l<=r){
			mid=(l+r)>>1;
			if(check(mid))res=mid,l=mid+1;else r=mid-1;
		}printf("%d\n",res);
	}
}
int g[maxn],f[maxn],tot,s[maxn];
inline bool Cmp(int x,int y){return g[x]<g[y];}
void dfs(int x,int fa,int lim){
	for(int j=last[x];j;j=e[j].next){
		int y=e[j].to;if(y==fa)continue;
		dfs(y,x,lim);f[x]+=f[y];
	}tot=0;
	for(int j=last[x];j;j=e[j].next){
		int y=e[j].to;if(y==fa)continue;
		g[y]=g[y]+e[j].len;
		if(g[y]>=lim)f[x]++,g[y]=0;
		if(g[y])s[++tot]=y;
	}
	sort(s+1,s+tot+1,Cmp);
	for(int i=tot,j=1;i>j;i--){
		while(j<i&&g[s[i]]+g[s[j]]<lim)j++;
		if(i==j)break;
		if(g[s[i]]+g[s[j]]>=lim)f[x]++,g[s[i]]=0,g[s[j]]=0,j++;else break;
	}g[x]=0;
	for(int i=1;i<=tot;i++)g[x]=max(g[x],g[s[i]]);
}
inline bool check(int x){
	memset(f,0,sizeof(f));
	dfs(1,0,x);
	return f[1]>=m;
}
inline void work(){
	int l=0,r=500000000,mid,res;
	while(l<=r){
		mid=(l+r)>>1;
		if(check(mid))res=mid,l=mid+1;else r=mid-1;
	}printf("%d\n",res);
}
int main(){
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	n=read();m=read();int tmp=0,tmp2=0;
	for(int i=1;i<n;i++){
		int x=read(),y=read(),z=read();
		insert(x,y,z);insert(y,x,z);
		a[i].x=x,a[i].y=y;a[i].len=z;if(a[i].x!=1)tmp=1;	
		if(a[i].y!=a[i].x+1)tmp2=1;
	}
	if(m==1)work1::Main();else 
	if(!tmp)work2::Main();else 
	if(!tmp2)work3::Main();else 
	work();
	return 0;
}
/*
10 3
1 2 1
1 3 2
1 8 4
2 4 3
2 5 4
3 6 5
3 7 6
8 9 5
8 10 6
*/
