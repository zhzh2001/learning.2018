#include<bits/stdc++.h>
#define maxn 100005
using namespace std;
inline int read(){
	int x=0,c;while(!isdigit(c=getchar()));
	while(x=x*10+c-'0',isdigit(c=getchar()));
	return x;
}
int n,a[maxn],head,tail,q[maxn];
int main(){
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	tail=0;int ans=0;
	for(int i=1;i<=n;i++){
		if(a[i]>q[tail])ans+=a[i]-q[tail];
		while(tail&&a[i]>=q[tail])tail--;
		q[++tail]=a[i];
	}
	printf("%d\n",ans);return 0;
}
