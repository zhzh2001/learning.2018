#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>

using namespace std;

const int inf=0x3f3f3f3f;
int n,a[110],dis[25000+5],ans;
bool cused[110];

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--)
	{
		for(int i=1;i<=100;i++)
		    a[i]=0;
		memset(cused,0,sizeof(cused));
		memset(dis,0,sizeof(dis));
		scanf("%d",&n);
		ans=n;
		for(int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		for(int i=1;i<=n;i++)
		    dis[a[i]]=i;
		int maxn=a[n];
		if(a[1]==1)
		{
			printf("1\n");
			continue;
		}//如果有1的话，所有的都可以表示出来 ，只有一个数据的时候直接写 
		for(int i=1;i<n;i++)
		{
			if(cused[i]==false)
			{
				int x=a[i];
				for(int j=i+1;j<=n;j++)
					if(cused[j]==false)
					{
						int qwq=x;
						while(qwq<=maxn)
						{
							qwq+=a[j];
						    if(dis[qwq]!=0)
						    {
						    	ans--;
						    	cused[dis[qwq]]=true;
						    	dis[qwq]=0;
							}
						}
					}
			    while(x<=maxn)
			    {
				    x+=a[i];
				    if(dis[x]!=0)
				    {
					    ans--;
					    cused[dis[x]]=true;
					    dis[x]=0;
				    }//判断当前的数的倍数是否存在 
				    for(int j=i+1;j<=n;j++)
					    if(cused[j]==false)
					    {
					        int qwq=x;
						    while(qwq<=maxn)
						    {
						        qwq+=a[j];
							    if(dis[qwq]!=0)
							    {
								    ans--;
								    cused[dis[qwq]]=true;
								    dis[qwq]=0;
							    }	
						    }	
					    }//判断两个数组成的数是否出现在这组数中 
			    }
			}
		}
		printf("%d\n",ans);
	}
	return 0;
} 
