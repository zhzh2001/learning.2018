#include<bits/stdc++.h>
using namespace std;
int t,n,a[105],f[50000];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	for (int z=1;z<=t;z++)
	{
		memset(f,0,sizeof(f));
		scanf("%d",&n);
		int maxn=0;
		for (int i=1;i<=n;i++)
		{
			scanf("%d",&a[i]);
			maxn=max(a[i],maxn);
		}
		int k=0;
		sort(a+1,a+n+1);
		for (int i=1;i<=n;i++)
		{
			if (f[a[i]]==0)
			{
				f[a[i]]=1;
				k++;
			}
			for (int j=a[i]+1;j<=maxn;j++)
			{
				f[j]=max(f[j],f[j-a[i]]);
			}
			bool p=false;
			for (int j=1;j<=n;j++)
			{
				if (f[a[j]]==0) 
				{
					p=true;
					break;
				}
			}
			if (p==false)
			{
				printf("%d\n",k);
				break;
			}
		}
	}
	return 0;
}