#include<bits/stdc++.h>
using namespace std;
int n,m;
struct zwc
{
	int x;
	int y;
	int z;
}q[100005];
int to[200005],val[200005],head[200005],Next[200005],tot;
int f[200005];
void add(int x,int y,int z)
{
	tot++;
	val[tot]=z;
	to[tot]=y;
	Next[tot]=head[x];
	head[x]=tot;
}
bool cmp(zwc x,zwc y)
{
	return x.x<y.x;
}
bool check(int mid)
{
	int t=0; int k=0;
	for (int i=1;i<=n;i++)
	{
		if (t>=mid)
		{
			t=0;
			k++;
		}
		if (t<mid)
		{
			t+=q[i].z;
		}
	}
	if (t>=mid) k++;
	if (k>=m) return true;
	else return false;
}
void dfs(int x,int fa)
{
	for (int i=head[x];i;i=Next[i])
	{
		int v=to[i];
		if (v==fa) continue;
		dfs(v,x);
		f[x]=max(f[x],f[v]+val[i]);
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool p1=false; bool p2=false;
	int sum=0;
	for (int i=1;i<=n-1;i++)
	{
		scanf("%d%d%d",&q[i].x,&q[i].y,&q[i].z);
		sum+=q[i].z;
		if (q[i].x!=1) p1=true;
		if (q[i].y-q[i].x!=1) p2=true;
	}
	if (p2==false)
	{
		sort(q+1,q+n+1,cmp);
		int l=1;
		int r=sum;
		while (l<=r)
		{
			int mid=(l+r)>>1;
			if (check(mid)) l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",r);
		return 0;
	} else
	{
		int ans=0;
		for (int i=1;i<=n;i++)
		{
			add(q[i].x,q[i].y,q[i].z);
			add(q[i].y,q[i].x,q[i].z);
		}
		for (int i=1;i<=n;i++)
		{
			dfs(i,-1);
			ans=max(ans,f[i]);
			for (int j=1;j<=n;j++)
			f[j]=0;
		}
		printf("%d\n",ans);
		return 0;
	}
}