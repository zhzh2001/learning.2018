#include<cstdio>
#include<cstring>

inline int getint()
{
	register int i1,i2;
	for(i2=getchar();i2<'0'||i2>'9';i2=getchar());
	i1=(i2-'0');
	for(i2=getchar();i2>='0'&&i2<='9';i2=getchar())i1=((i1<<3)+(i1<<1)+i2-'0');
	return i1;
}

int n,m,i,j,ans,a[100005];

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=getint();
	for(i=1;i<=n;++i)a[i]=getint();
	m=n+1;a[m]=0;
	ans=0;
	for(i=1;i<=m;++i)
	{
		if(a[i]<a[i-1])ans+=a[i-1]-a[i];
	}
	printf("%d\n",ans);
	return 0;
}
