#include<cstdio>
#include<cstring>

inline int getint()
{
	register int i1,i2;
	for(i2=getchar();i2<'0'||i2>'9';i2=getchar());
	i1=(i2-'0');
	for(i2=getchar();i2>='0'&&i2<='9';i2=getchar())i1=((i1<<3)+(i1<<1)+i2-'0');
	return i1;
}

int n,m,i,j,u,v,w,first[50004],e[100005],nxt[100005],c[100005],tope;
inline void link(int u0,int v0,int w0)
{
	++tope;
	e[tope]=v0;
	nxt[tope]=first[u0];
	first[u0]=tope;
	c[tope]=w0;
	return;
}
int fa[50004];
long long dep[50004];
int q[50004],lq,rq;
inline void bfs()
{
	memset(dep,0,sizeof(dep));
	memset(fa,0,sizeof(fa));
	q[rq=1]=1;
	int i1;
	for(lq=1;lq<=rq;++lq)
	{
		for(i1=first[q[lq]];i1;i1=nxt[i1])
		{
			if(e[i1]==fa[q[lq]])continue;
			fa[e[i1]]=q[lq];
			dep[e[i1]]=dep[q[lq]]+c[i1];
			q[++rq]=e[i1];
		}
	}
	if(rq!=n)puts("error1");
	return;
}
//int f[100005];
long long ans,step;
long long zj[50004];
inline long long zhijing()
{
	int i1;
	long long i2=0,i3,i4;
	memset(zj,0,sizeof(zj));
	for(lq=rq;lq>=1;--lq)
	{
		i3=0;
		for(i1=first[q[lq]];i1;i1=nxt[i1])
		{
			if(e[i1]==fa[q[lq]])continue;
			i4=zj[e[i1]]+c[i1];
			if(i4>zj[q[lq]])
			{
				i3=zj[q[lq]];
				zj[q[lq]]=i4;
			}else if(i4>i3)i3=i4;
		}
		if(i3+zj[q[lq]]>i2)i2=i3+zj[q[lq]];
	}
	return i2;
}
long long dp[1003][1003];
int zs[1003][1003];
//inline void workzs()
//{
//	memset(zs,0,sizeof(zs));
//	int i1,i2;
//	for(i1=1;i1<=n;++i1)
//	{
//		for(i2=i1;fa[i2];i2=fa[i2])
//		{
//			++zs[i2][0];
//			zs[i2][zs[i2][0]]=i1;
//		}
//	}
//	return;
//}
inline bool check(long long x0)
{
	if(x0<=18)
	zs[0][0]=1;
	memset(zs,0,sizeof(zs));
	memset(dp,0,sizeof(dp));
	int i1,i2,i3,i4;
	long long i5,i6,i7;
	for(lq=rq;lq>=1;--lq)
	{
		zs[q[lq]][0]=1;
		zs[q[lq]][1]=q[lq];
		i5=0;
		for(i1=first[q[lq]];i1;i1=nxt[i1])
		{
			if(e[i1]==fa[q[lq]])continue;
			i5=i7=dp[q[lq]][1];
			i6=dp[e[i1]][1];
//			i5+=dp[e[i1]][1];
			i3=zs[q[lq]][0];
			for(i2=1;i2<=zs[e[i1]][0];++i2)
			{
				zs[q[lq]][i2+i3]=zs[e[i1]][i2];
				dp[q[lq]][i2+i3]=dp[e[i1]][i2];//+i5;
			}
			for(i2=zs[e[i1]][0]+i3;i2>i3;--i2)
			{
				for(i4=zs[q[lq]][0];i4>=1;--i4)
				{
					if(dep[zs[q[lq]][i2]]+dep[zs[q[lq]][i4]]-(dep[q[lq]]<<1)>=x0)
					{
						if(dp[q[lq]][i2]+dp[q[lq]][i4]+1>i7)
						i7=dp[q[lq]][i2]+dp[q[lq]][i4]+1;
					}
				}
				dp[q[lq]][i2]+=i5;
			}
			for(i2=zs[q[lq]][0];i2>0;--i2)
			{
				dp[q[lq]][i2]+=i6;
			}
			if(dp[q[lq]][1]<i7)dp[q[lq]][1]=i7;
			zs[q[lq]][0]+=zs[e[i1]][0];
		}
//		printf("%d:%d\n",q[lq],zs[q[lq]][0]);
//		for(i1=1;i1<=zs[q[lq]][0];++i1)printf(i1<zs[q[lq]][0]?"%d ":"%d\n",zs[q[lq]][i1]);
	}
	return dp[1][1]>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=getint();m=getint();
	tope=0;
	memset(first,0,sizeof(first));
	for(i=1;i<n;++i)
	{
		u=getint();v=getint();w=getint();
		link(u,v,w);link(v,u,w);
	}
	bfs();
	if(m<=1)//&&n>1000)
	{
		ans=zhijing();
		printf("%lld\n",ans);
		return 0;
	}
//	workzs();
	ans=1;
	step=10000ll*(long long)(n);
	while(step&(step-1))step+=(step&(-step));
	for(;step>0;step>>=1)if(check(ans+step))ans+=step;
	printf("%lld\n",ans);
	//for(i=1;(i<n)&&(fa[i+1]==i);++i);
	
	return 0;
}
