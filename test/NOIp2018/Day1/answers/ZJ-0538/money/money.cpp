#include<bits/stdc++.h>
using namespace std;
const int N=105,M=25005;
int T,n,a[N],flag[M];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		for (int i=1;i<=n;i++)scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		int m=a[n],ans=0;
		memset(flag,0,sizeof flag);
		flag[0]=1;
		for (int i=1;i<=n;i++)
			if (!flag[a[i]]){
				ans++;
				for (int j=0;j<=m-a[i];j++)flag[j+a[i]]|=flag[j];
			}
		printf("%d\n",ans);
	}
	return 0;
}
