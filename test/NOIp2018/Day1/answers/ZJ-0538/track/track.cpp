#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int ne[N],num,fi[N],zz[N],sl[N],a[N],f[N],dp[N],x,y,z,n,m;
void jb(int x,int y,int z){
	ne[++num]=fi[x];
	fi[x]=num;
	zz[num]=y;
	sl[num]=z;
}
void dfs(int x,int y,int z){
	dp[x]=f[x]=0;
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=y)dfs(zz[i],x,z);
	int tot=0;
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=y){
			dp[x]+=dp[zz[i]];
			if (sl[i]+f[zz[i]]>=z)dp[x]++;
			else a[++tot]=sl[i]+f[zz[i]];
		}	
	sort(a+1,a+tot+1);
	int i=1,j=tot;
	while (1){
		while (i<j&&a[i]+a[j]<z)f[x]=a[i],i++;
		if (i>=j)break;
		dp[x]++;i++;j--;
	}
	if (j>=i){
		f[x]=a[j];
		for (int k=j+1;k<=tot;k++)
			if (a[k-1]+a[2*j-k]>=z)f[x]=a[k];
			else break;
	}
}
int pd(int x){
	dfs(1,0,x);
	if (dp[1]>=m)return 1;
	return 0;
}
int read(){
	int x=0;char c=0;
	for (;c<'0'||c>'9';c=getchar());
	for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
	return x;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	int l=0,r=0;	
	for (int i=1;i<n;i++){
		x=read();y=read();z=read();
		jb(x,y,z);jb(y,x,z);
		r+=z;
	}
	r/=m;r+=2;
	while (l<r){
		int mid=(l+r+1)/2;
		if (pd(mid))l=mid;
		else r=mid-1;
	}
	printf("%d",l);
	return 0;
}
