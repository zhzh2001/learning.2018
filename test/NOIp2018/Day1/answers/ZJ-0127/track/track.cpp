#include <cstdio>
#include <iostream>
using namespace std;

int bf2l,bf2r,cnt,head[100005];
int bf2[100005],bf2tot;
int chu[100005],ru[100005];
int n,m,x,y,z,ans;
int nxt,now,last,maxx;
struct node{
	int to,len,nxt;
}g[100005];
void addedge(int x,int y,int z)
{
	g[++cnt]=(node){y,z,head[x]};
	head[x]=cnt;
}
int dfs_1(int u,int fa,int depth)
{
	if (depth>maxx)
	{
		maxx=depth;
		nxt=u;
	}
	for (int i=head[u];i>0;i=g[i].nxt)
	{
		int v=g[i].to;
		if (v==fa) continue;
		dfs_1(v,u,depth+g[i].len);
	}
}
void dfs_2(int u,int fa)
{
	for (int i=head[u];i>0;i=g[i].nxt)
	{
		int v=g[i].to;
		if (v==fa) continue;
		bf2tot++;bf2[bf2tot]=g[i].len;
		dfs_2(v,u);
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d %d",&n,&m);
	int pd=1;
	for (int i=1;i<=n-1;i++)
	{
		scanf("%d %d %d",&x,&y,&z);
		chu[x]++;ru[y]++;
		if (chu[x]>1) pd=0;
		addedge(x,y,z);
		addedge(y,x,z);
		bf2r+=z;
	}
	if (m==1)
	{
		last=1;
		maxx=0;
		dfs_1(last,0,0);
		now=nxt;
		maxx=0;
		dfs_1(now,0,0);
		while (last!=nxt)
		{
			maxx=0;
			last=now;now=nxt;dfs_1(now,0,0);
		}
		printf("%d\n",maxx);
	}else
	if (pd==1)
	{
		for (int j=1;j<=n;j++)
		if (ru[j]==0)
		{
			dfs_2(j,0);
			break;
		}
		bf2l=0;ans=0;
		while (bf2l<=bf2r)
		{
			int mid=(bf2l+bf2r)>>1;
			int bf2s=0,bf2t=0;
			for (int j=1;j<=bf2tot;j++)
			{
				bf2s+=bf2[j];
				if (bf2s>=mid)
				{
					bf2t++;
					bf2s=0;
				}
			}
			if (bf2t>=m)
			{
				ans=mid;
				bf2l=mid+1;
			}else bf2r=mid-1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
