#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

int n,T,pd[300005],p[300005],a[300005];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T)
	{
		T--;
		scanf("%d",&n);
		int maxx=0;
		for (int i=1;i<=n;i++) 
		{
			scanf("%d",&a[i]);
			maxx=max(maxx,a[i]);
		}
		int ans=0;
		sort(a+1,a+1+n);pd[0]=1;
		memset(pd,0,sizeof(pd));
		for (int i=1;i<=n;i++)
		if (pd[a[i]]==0)
		{
			ans++;
			pd[a[i]]=1;
			p[ans]=a[i];
			for (int j=1;j<=maxx;j++)
			if (pd[j]==0)
			{
				for (int k=1;k<=ans;k++)
				{
					if (j-p[k]<0) break;
					if (pd[j-p[k]]==1) 
					{
						pd[j]=1;break;
					}
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
