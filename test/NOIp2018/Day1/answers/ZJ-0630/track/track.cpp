#include<bits/stdc++.h>
#define N 50007
using namespace std;
struct Edge{
	int to,next,l;
}e[N*2];
int n,m,cnt=0,h[N],dis[N],le[N],la[N];
bool lin=1,ro=1;
void dfs(int x,int pre){
	for (int i=h[x];i;i=e[i].next){
		int v=e[i].to;
		if(v==pre)continue;
		dis[v]=dis[x]+e[i].l;
		dfs(v,x);
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);le[u]=w;la[v]=w;
		if(v!=u+1)lin=0;
		if(u!=1)ro=0;
		e[++cnt].to=v;e[cnt].next=h[u];e[cnt].l=w;h[u]=cnt;
		e[++cnt].to=u;e[cnt].next=h[v];e[cnt].l=w;h[v]=cnt;
	}
	if(m==1){
		int maxl=0,il;
		dfs(1,0);
		for (int i=1;i<=n;i++)
		    if(dis[i]>maxl)il=i,maxl=dis[i];
		memset(dis,0,sizeof(dis));
		dfs(il,0);maxl=0;
		for (int i=1;i<=n;i++)
		    maxl=max(maxl,dis[i]);
		printf("%d\n",maxl);
		return 0;
	}
	if(lin){
	    int r=0,l=0;
	    for (int i=1;i<n;i++)r+=le[i];
	    while(l<r){
	    	int mid=(l+r)/2+1,sum=0,num=0;
	    	for (int i=1;i<n;i++){
	    		if(sum+le[i]<mid)sum+=le[i];
	    		else sum=0,num++;
			}
			if(num>=m)l=mid;
			else r=mid-1;
		}
		printf("%d\n",l);
		return 0;
	}
	if(m==n-1){
		int minl=1000000000;
		for (int i=1;i<n;i++)minl=min(minl,e[2*i].l);
		printf("%d\n",minl);
		return 0;
	}
	if(ro){
		sort(la+2,la+n+1);
		int l=la[2],r=la[n]+la[n-1];
		while(l<r){
			int mid=(l+r)/2+1,num=0;
			int fir=2,las=n;
			while(fir<las){
				if(la[las]>=mid)num++,las--;
				else{
					while(la[las]+la[fir]<mid&&fir<las-1)fir++;
					if(la[las]+la[fir]>=mid)num++;
					las--;fir++;
				}
			}
			if(num>=m)l=mid;
			else r=mid-1;
		}
		printf("%d\n",l);
		return 0;
	}
	return 0;
}
