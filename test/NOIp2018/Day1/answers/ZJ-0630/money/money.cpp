#include<bits/stdc++.h>
using namespace std;
#define N 108
#define M 25000
int n,a[N];
bool f[M+10],vis[N];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for (int i=1;i<=n;i++)scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		memset(f,0,sizeof(f));memset(vis,0,sizeof(vis));
		f[0]=1;int ans=0;
		for (int i=1;i<=n;i++)
		    if(!vis[i]){
			    for(int j=a[i];j<=M;j++)
		            f[j]|=f[j-a[i]];
		        for(int j=i+1;j<=n;j++)
		            if(f[a[j]]&&!vis[j])ans++,vis[j]=1;
		    }
		printf("%d\n",n-ans);
	}
	return 0;
}
