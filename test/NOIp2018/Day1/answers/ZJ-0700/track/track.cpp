#include<cstdio>
#include<cctype>
#include<algorithm>
#define rg register
typedef long long LL;
template<typename T>inline T min(const T x,const T y){return x<y?x:y;}
template<typename T>inline T max(const T x,const T y){return x>y?x:y;}
template<typename T>inline T abs(const T x){return x<0?-x:x;}
template<typename T>inline void mind(T&x,const T y){if(x>y)x=y;}
template<typename T>inline void maxd(T&x,const T y){if(x<y)x=y;}
template<typename T>inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template<typename T>inline void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template<typename T>inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
const int maxn=50005,maxm=100005;
int n,m;
int head[maxn],nxt[maxm],tow[maxm],vau[maxm],tmp;
inline void addb(const int u,const int v,const int w)
{
	tmp++;
	nxt[tmp]=head[u];
	head[u]=tmp;
	tow[tmp]=v;
	vau[tmp]=w;
}
int mid;
int sum[maxn],lenth[maxn];
int Q[maxn],l,r,size;
int L[maxn],R[maxn];
void dfs(const int u,const int fa)
{
	sum[u]=lenth[u]=0;
	for(rg int i=head[u];i;i=nxt[i])
	{
		const int v=tow[i];
		if(v!=fa)dfs(v,u);
	}
	r=0;
	for(rg int i=head[u];i;i=nxt[i])
	{
		const int v=tow[i];
		if(v!=fa)
		{
			if(lenth[v]+vau[i]>=mid)sum[u]++;
			else Q[++r]=lenth[v]+vau[i];
			sum[u]+=sum[v];
		}
	}
	l=r+1,size=r;
	std::sort(Q+1,Q+r+1);
	for(rg int i=0;i<=r+1;i++)L[i]=i-1,R[i]=i+1;
	while(size)
	{
		int pos=R[0];
		const int val=Q[pos];
		R[L[pos]]=R[pos];
		L[R[pos]]=L[pos];
		size--;
		if(l==pos)l=R[pos];
		while(Q[L[l]]+val>=mid)l=L[l];
		if(l==r+1)maxd(lenth[u],val);
		else
		{
			R[L[l]]=R[l];
			L[R[l]]=L[l];
			l=R[l];
			sum[u]++;
			size--;
		}
	}
}
int ll=1,rr,ans;
int main()
{
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	read(n),read(m);
	for(rg int i=1;i<n;i++)
	{
		int u,v,w;read(u),read(v),read(w);
		addb(u,v,w),addb(v,u,w);
		rr+=w;
	}
	while(ll<=rr)
	{
		mid=(ll+rr)>>1;
		dfs(1,0);
		if(sum[1]>=m)ans=mid,ll=mid+1;
		else rr=mid-1;
	}
	print(ans);
	return 0;
}
