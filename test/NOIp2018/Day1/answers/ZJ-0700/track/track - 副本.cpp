#include<cstdio>
#include<cctype>
#include<algorithm>
#include<set>
#define rg register
typedef long long LL;
template<typename T>inline T min(const T x,const T y){return x<y?x:y;}
template<typename T>inline T max(const T x,const T y){return x>y?x:y;}
template<typename T>inline T abs(const T x){return x<0?-x:x;}
template<typename T>inline void mind(T&x,const T y){if(x>y)x=y;}
template<typename T>inline void maxd(T&x,const T y){if(x<y)x=y;}
template<typename T>inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template<typename T>inline void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template<typename T>inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
const int maxn=50001,maxm=100001;
int n,m;
int head[maxn],nxt[maxm],tow[maxm],vau[maxm],tmp;
inline void addb(const int u,const int v,const int w)
{
	tmp++;
	nxt[tmp]=head[u];
	head[u]=tmp;
	tow[tmp]=v;
	vau[tmp]=w;
}
int mid;
int sum[maxn],lenth[maxn];
std::multiset<int>Q[maxn];
std::multiset<int>::iterator pos;
void dfs(const int u,const int fa)
{
	sum[u]=lenth[u]=0,Q[u].clear();
	for(rg int i=head[u];i;i=nxt[i])
	{
		const int v=tow[i];
		if(v!=fa)
		{
			dfs(v,u);
			if(lenth[v]+vau[i]>=mid)sum[u]++;
			else Q[u].insert(lenth[v]+vau[i]);
			sum[u]+=sum[v];
		}
	}
	while(!Q[u].empty())
	{
		pos=Q[u].begin();
		const int val=*pos;
		Q[u].erase(pos);
		pos=Q[u].lower_bound(mid-val);
		if(pos==Q[u].end())maxd(lenth[u],val);
		else Q[u].erase(pos),sum[u]++;
	}
}
int ll=1,rr,ans;
int main()
{
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	read(n),read(m);
	for(rg int i=1;i<n;i++)
	{
		int u,v,w;read(u),read(v),read(w);
		addb(u,v,w),addb(v,u,w);
		rr+=w;
	}
	while(ll<=rr)
	{
		mid=(ll+rr)>>1;
		dfs(1,0);
		if(sum[1]>=m)ans=mid,ll=mid+1;
		else rr=mid-1;
	}
	print(ans);
	return 0;
}
