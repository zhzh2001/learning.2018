#include<cstdio>
#include<cctype>
#include<algorithm>
#define rg register
typedef long long LL;
template<typename T>inline T min(const T x,const T y){return x<y?x:y;}
template<typename T>inline T max(const T x,const T y){return x>y?x:y;}
template<typename T>inline T abs(const T x){return x<0?-x:x;}
template<typename T>inline void mind(T&x,const T y){if(x>y)x=y;}
template<typename T>inline void maxd(T&x,const T y){if(x<y)x=y;}
template<typename T>inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template<typename T>inline void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template<typename T>inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
const int maxn=100005;
int n,a[maxn],ans;
int ll[maxn],rr[maxn];
int tid[maxn];
bool cmp(const int x,const int y){return a[x]<a[y];}
int main()
{
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	read(n);
	for(rg int i=1;i<=n;i++)read(a[i]);
	for(rg int i=1;i<=n;i++)ll[i]=i-1,rr[i]=i+1,tid[i]=i;
	std::sort(tid+1,tid+n+1,cmp);
	for(rg int i=n;i>=1;i--)
	{
		const int u=tid[i];
		ans+=a[u]-max(a[ll[u]],a[rr[u]]);
		rr[ll[u]]=rr[u];
		ll[rr[u]]=ll[u];
	}
	print(ans);
	return 0;
}
