#include<cstdio>
#include<cctype>
#include<algorithm>
#include<cstring>
#define rg register
typedef long long LL;
template<typename T>inline T min(const T x,const T y){return x<y?x:y;}
template<typename T>inline T max(const T x,const T y){return x>y?x:y;}
template<typename T>inline T abs(const T x){return x<0?-x:x;}
template<typename T>inline void mind(T&x,const T y){if(x>y)x=y;}
template<typename T>inline void maxd(T&x,const T y){if(x<y)x=y;}
template<typename T>inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template<typename T>inline void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template<typename T>inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
const int MAX=25000;
int T,n,a[105],ans;
bool vis[25005];
int main()
{
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	read(T);
	while(T--)
	{
		read(n);
		for(rg int i=1;i<=n;i++)read(a[i]);
		std::sort(a+1,a+n+1);
		memset(vis,0,sizeof(vis)),ans=0;
		vis[0]=1;
		for(rg int i=1;i<=n;i++)
		{
			if(vis[a[i]])continue;
			const int limit=MAX-a[i];
			for(rg int j=0;j<=limit;j++)
				if(vis[j])
					vis[j+a[i]]=1;
			ans++;
		}
		print(ans),putchar('\n');
	}
	return 0;
}
