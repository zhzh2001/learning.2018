#include<cstdio>
#include<cctype>
#include<algorithm>
#include<cstring>
#define rg register
typedef long long LL;
template<typename T>inline T min(const T x,const T y){return x<y?x:y;}
template<typename T>inline T max(const T x,const T y){return x>y?x:y;}
template<typename T>inline T abs(const T x){return x<0?-x:x;}
template<typename T>inline void mind(T&x,const T y){if(x>y)x=y;}
template<typename T>inline void maxd(T&x,const T y){if(x<y)x=y;}
template<typename T>inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template<typename T>inline void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template<typename T>inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
const int MAX=25000;
int T,n,a[101],ans;
bool vis[25001];
bool isprime[25001];
inline void get()
{
	memset(isprime,1,sizeof(isprime));
	for(rg int i=2;i<=MAX;i++)
		if(isprime[i])
			for(rg int j=i+i;j<=MAX;j+=i)
				isprime[j]=0;
}

int main()
{
	freopen("money.in","w",stdout);//freopen("money.out","w",stdout);
	get();
	T=20,print(T),putchar('\n');
	while(T--)
	{
		n=100;print(n),putchar('\n');
		memset(vis,0,sizeof(vis)),ans=0;
		vis[0]=1;
		for(rg int i=400;i<=MAX&&n;i++)
		{
			if(vis[i]||!isprime[i])continue;
			print(i),putchar(' ');
			n--;
			for(rg int j=i;j<=MAX;j<<=1)
				for(rg int k=MAX-j;k>=0;k--)
					if(vis[k])
						vis[k+j]=1;
			ans++;
		}
		while(n--)
		{
			print(MAX),putchar(' ');
		}
		
		putchar('\n');
	}
	return 0;
}
