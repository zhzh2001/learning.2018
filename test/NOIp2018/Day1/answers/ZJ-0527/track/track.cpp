#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=50005,maxm=100005;
int n,m,tot,lnk[maxn],w[maxm],son[maxm],nxt[maxm],l[maxn],sum;bool juhuatu=true,lian=true;
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9'){ret=ret*10+ch-'0';ch=getchar();}
	return ret*f;
}
inline void add_e(int x,int y,int z){tot++;son[tot]=y;w[tot]=z;nxt[tot]=lnk[x];lnk[x]=tot;}
/*-----------------------ZhiJing--------------------*/
int MaxDist,rot;
void DFS(int now,int fa,int dist)
{
	if(dist>MaxDist){MaxDist=dist;rot=now;}
	for(int i=lnk[now];i;i=nxt[i])
		if(son[i]!=fa)
			DFS(son[i],now,dist+w[i]);
}
inline void Solve_ZhiJing()
{
	DFS(1,0,0);
	MaxDist=0;
	DFS(rot,0,0);
	printf("%d\n",MaxDist);
}
/*-----------------------JuHuaTu--------------------*/
inline bool check_JuHuaTu(int mid)
{
	int ret=0,p=1;
	for(int i=n-1;i>=p;i--)
	{
		if(l[i]>=mid){ret++;continue;}
		while(l[p]+l[i]<mid&&p<i) p++;
		if(p==i) break;
		ret++;p++;
	}
	return ret>=m;
}
inline void Solve_JuHuaTu()
{
	sort(l+1,l+n);
	int L=1,R=sum,mid;
	while(L<=R)
	{
		mid=L+R>>1;
		check_JuHuaTu(mid)?L=mid+1:R=mid-1;
	}
	printf("%d\n",R);
}
/*-----------------------Lian--------------------*/
int cnt=0;
void DFS2(int now,int fa)
{
	for(int i=lnk[now];i;i=nxt[i])
		if(son[i]!=fa)
			{l[++cnt]=w[i];DFS2(son[i],now);}
}
inline bool check_Lian(int mid)
{
	int now=0,ret=0;
	for(int i=1;i<n;i++)
	{
		now+=l[i];
		if(now>=mid){now=0;ret++;}
	}
	return ret>=m;
}
inline void Solve_Lian()
{
	DFS2(1,0);
	int L=1,R=sum,mid;
	while(L<=R)
	{
		mid=L+R>>1;
		check_Lian(mid)?L=mid+1:R=mid-1;
	}
	printf("%d\n",R);
}
/*-----------------------PianFen--------------------*/
inline void Solve_PianFen()
{
	int mi=2e9;
	for(int i=1;i<n;i++)
		if(l[i]<mi)
			mi=l[i];
	printf("%d\n",mi);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<n;i++)
	{
		int a=read(),b=read(),c=read();l[i]=c;sum+=c;
		if(a!=1) juhuatu=false;if(b!=a+1) lian=false;
		add_e(a,b,c);add_e(b,a,c);
	}
	if(m==1) Solve_ZhiJing();
	else if(juhuatu) Solve_JuHuaTu();
	else if(lian) Solve_Lian();
	else Solve_PianFen();
	return 0;
}
