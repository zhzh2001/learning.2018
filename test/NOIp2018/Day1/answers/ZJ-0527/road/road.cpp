#include<cstdio>
using namespace std;
const int maxn=100005;
int n,A[maxn],ans;
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9'){ret=ret*10+ch-'0';ch=getchar();}
	return ret*f;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
	{
		A[i]=read();
		if(A[i]>A[i-1]) ans+=A[i]-A[i-1];
	}
	printf("%d\n",ans);
	return 0;
}
