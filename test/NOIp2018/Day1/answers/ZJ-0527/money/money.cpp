#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=105,maxa=25005;
int T,n,a[maxn],allv,ans;bool F[maxa],vis[maxa];
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9'){ret=ret*10+ch-'0';ch=getchar();}
	return ret*f;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();allv=0;memset(F,false,sizeof(F));memset(vis,false,sizeof(vis));ans=n;
		for(int i=1;i<=n;i++){a[i]=read();if(a[i]>allv)allv=a[i];vis[a[i]]=true;}
		for(int i=1;i<=n;i++)
			for(int j=1;j+a[i]<=allv;j++)
				if(F[j]||vis[j])
					F[j+a[i]]=true;
		for(int i=1;i<=n;i++)
			if(F[a[i]])
				ans--;
		printf("%d\n",ans);
	}
	return 0;
}
