#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;char ch=getchar();bool p=0;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=100005;
struct edge{int to,nt,di;}e[N*2];
int n,m,u,v,w,head[N],cnt,l,r,mid,ans,sum,dt;
bool t[N];
void add(int u,int v,int w){
	e[++cnt].to=v;
	e[cnt].nt=head[u];
	e[cnt].di=w;
	head[u]=cnt;
}
int dfs(int u,int fa){
	vector<int> v;
	for(int i=head[u];i;i=e[i].nt)
		if(e[i].to!=fa){
			dt=dfs(e[i].to,u)+e[i].di;
			if(dt>=mid)sum++;
			else v.push_back(dt);
		}
	if(v.size()==0)return 0;
	if(v.size()==1)return v[0];
	sort(v.begin(),v.end());
	int mi=0,mx=v.size()-1,mmx=0,mmi;
	for(;mi<mx;){
		while(mi<mx&&v[mi]+v[mx]<mid){
			mmx=v[mi];mi++;
		}
		if(mi<mx){
			t[mi]=1;sum++;
			mi++;mx--;
			if(mi==mx){
				mmx=v[mx];mmi=mi-1;mx++;
				while(mmi>=0&&mx<v.size()){
					while(mmi>=0&&!t[mmi])mmi--;
					if(mmi<0)break;
					if(v[mmi]+mmx>=mid)mmx=v[mx];
					mmi--;
				}
				break;
			}
		}else mmx=v[mx];
	}
	return mmx;
}
bool check(){
	sum=0;dfs(1,0);
	if(sum>=m)return 1;
	return 0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<n;i++){
		u=read();v=read();w=read();
		add(u,v,w);add(v,u,w);
	}
	l=0;r=n*10000;
	while(l<=r){
		mid=(l+r)>>1;
		if(check()){
			ans=mid;
			l=mid+1;
		}else r=mid-1;
	}
	cout<<ans;
	return 0;
}
