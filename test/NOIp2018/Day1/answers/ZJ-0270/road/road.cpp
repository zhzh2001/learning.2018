#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;char ch=getchar();bool p=0;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=100005;
int n,ans,a[N];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		if(a[i]>a[i-1])ans+=a[i]-a[i-1];
	}
	cout<<ans;
	return 0;
}
