#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;char ch=getchar();bool p=0;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=25005;
int n,m,p,ans,a[N];
bool t[N];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--){
		n=read();
		memset(t,0,sizeof(t));
		t[0]=1;ans=0;
		for(int i=1;i<=n;i++)a[i]=read();
		sort(a+1,a+n+1);
		for(int i=1;i<=n;i++){
			if(t[a[i]])continue;
			for(int j=a[i];j<=a[n];j++)t[j]|=t[j-a[i]];
			ans++;
		}
		cout<<ans<<'\n';
	}
	return 0;
}
