#include<cstdio>
#include<cstring>
#include<algorithm>
using std::sort;
const int N=5e4+5;
int n;
int a[N];
bool dp[N];
inline void solve()
{
	int ans=0;
	register int i,j;
	memset(dp,0,sizeof(dp));
	scanf("%d",&n);
	for(i=1;i<=n;i++)
		scanf("%d",&a[i]);
	sort(a+1,a+n+1);
	dp[0]=1;
	for(i=1;i<=n;i++)
		if(!dp[a[i]])
		{
			ans++;
			for(j=a[i];j<=25000;j++)
				if(dp[j-a[i]])
					dp[j]=1;
		}
	printf("%d\n",ans);
	return;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--)
		solve();
	return 0;
}
