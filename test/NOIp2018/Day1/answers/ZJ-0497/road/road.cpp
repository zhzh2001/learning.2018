#include<cstdio>
#include<algorithm>
using std::max;
const int N=1e5+5;
int n,ans;
int a[N];
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int cur=0;
	register int i;
	scanf("%d",&n);
	for(i=1;i<=n;i++)
		scanf("%d",&a[i]);
	for(i=1;i<=n;i++)
		ans+=max(a[i]-cur,0),cur=a[i];
	printf("%d\n",ans);
	return 0;
}
