#include<cstdio>
#include<cstring>
#include<algorithm>
#define first Program_By
#define next Mr_Spade
using std::sort;
inline int read()
{
	int res=0;
	char ch;
	while((ch=getchar())<'0'||ch>'9');
	for(;ch>='0'&&ch<='9';ch=getchar())
		res=res*10+ch-'0';
	return res;
}
const int N=5e4+5;
int n,m,tot;
int v[N<<1],w[N<<1],first[N],next[N<<1];
int deg[N],f[N],g[N];
int ptr;
int pool[N<<3];
int *V[N],*V2[N];
inline void add_edge(int from,int to,int dist)
{
	tot+=2;
	v[tot^1]=from;v[tot]=to;w[tot]=w[tot^1]=dist;
	next[tot]=first[from];first[from]=tot;
	next[tot^1]=first[to];first[to]=tot^1;
	return;
}
inline bool cmp(int a,int b)
{
	return a>b;
}
void dfs(int now,int father,int x)
{
	int res=0,best=0,cur,t,t2;
	register int i,j,go;
	f[now]=0;g[now]=0;t=0;
	V[now][t++]=0;
	for(go=first[now];go;go=next[go])
		if(v[go]!=father)
		{
			dfs(v[go],now,x);res+=f[v[go]];
			V[now][t++]=g[v[go]]+w[go];
			V[now][t++]=0;
		}
	sort(V[now],V[now]+t,cmp);
	i=0;j=t-1;
	for(;i<j;i++)
	{
		while(i<j&&V[now][i]+V[now][j]<x)
			j--;
		if(i<j)
			best++,j--;
	}
	int l=0,r=t-1,mid;
	while(l<r)
	{
		mid=(l+r)>>1;t2=0;
		for(i=0;i<t;i++)
			if(i!=mid)
				V2[now][t2++]=V[now][i];
		cur=0;i=0;j=t2-1;
		for(;i<j;i++)
		{
			while(i<j&&V2[now][i]+V2[now][j]<x)
				j--;
			if(i<j)
				cur++,j--;
		}
		if(cur==best)
			r=mid;
		else
			l=mid+1;
	}
	f[now]=res+best;g[now]=V[now][l];
	return;
}
inline bool check(int x)
{
	dfs(1,0,x);
	return f[1]>=m;
}
void dfs1(int now,int father)
{
	register int go;
	for(go=first[now];go;go=next[go])
		if(v[go]!=father)
			deg[now]++,dfs1(v[go],now);
	V[now]=pool+ptr;ptr+=deg[now]<<1|1;
	V2[now]=pool+ptr;ptr+=deg[now]<<1;
	return;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int x,y,z;
	register int i;
	n=read();m=read();
	for(i=1;i<=n-1;i++)
	{
		x=read();y=read();z=read();
		add_edge(x,y,z);
	}
	dfs1(1,0);
	int l=1,r=1000000000,mid;
	while(l<r)
	{
		mid=(l+r+1)>>1;
		if(check(mid))
			l=mid;
		else
			r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
