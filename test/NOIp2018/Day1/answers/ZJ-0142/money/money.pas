var
  n,i,tt,ans,t,j:longint;
  a:array[0..1001]of longint;
  bo:array[0..250001]of boolean;
begin
  assign(input,'money.in');
  reset(input);
  assign(output,'money.out');
  rewrite(output);
  read(t);
  while t>0 do
    begin
      dec(t);
      read(n);
      for i:=1 to n do read(a[i]);
      for i:=1 to n do
        for j:=i+1 to n do
          if a[i]>a[j] then
            begin
              tt:=a[i];
              a[i]:=a[j];
              a[j]:=tt;
            end;
      bo[0]:=true;
      for i:=1 to 25000 do bo[i]:=false;
      ans:=0;
      for i:=1 to n do
        if bo[a[i]] then continue
        else begin
               inc(ans);
               tt:=25000-a[i];
               for j:=0 to tt do
                 if (bo[j])then bo[j+a[i]]:=true;

             end;

      writeln(ans);
    end;
  close(input);
  close(output);
end.
