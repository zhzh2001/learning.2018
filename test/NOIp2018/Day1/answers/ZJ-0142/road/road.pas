var
  n,i:longint;
  ans:int64;
  a:array[0..1000001]of longint;
begin
  assign(input,'road.in');
  reset(input);
  assign(output,'road.out');
  rewrite(output);
  read(n);
  for i:=1 to n do read(a[i]);
  ans:=a[1];
  for i:=2 to n do
    if a[i]>a[i-1] then ans:=ans+a[i]-a[i-1];
  write(ans);
  close(input);
  close(output);
end.