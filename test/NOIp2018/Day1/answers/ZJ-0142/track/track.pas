var
  n,m,zz,ll,rr,mid,now,la,i,t:longint;
  bo:array[0..100005]of boolean;
  a,b,l,te,dis,qi,q:array[0..100005]of longint;
  boo:boolean;
procedure sort(l1,r: longint);
var
  i,j,x,y: longint;
begin
  i:=l1;
  j:=r;
  x:=a[(l1+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if not(i>j) then
    begin
      y:=a[i];
      a[i]:=a[j];
      a[j]:=y;
      y:=b[i];
      b[i]:=b[j];
      b[j]:=y;
      y:=l[i];
      l[i]:=l[j];
      l[j]:=y;
      inc(i);
      j:=j-1;
    end;
  until i>j;
  if l1<j then sort(l1,j);
  if i<r then sort(i,r);
end;
procedure sort2(l1,r: longint);
var
  i,j,x,y: longint;
begin
  i:=l1;
  j:=r;
  x:=te[(l1+r) div 2];
  repeat
    while te[i]>x do inc(i);
    while x>te[j] do dec(j);
    if not(i>j) then
    begin
      y:=te[i];
      te[i]:=te[j];
      te[j]:=y;
      inc(i);
      j:=j-1;
    end;
  until i>j;
  if l1<j then sort2(l1,j);
  if i<r then sort2(i,r);
end;
procedure buildtree(x:longint);
var
  i:longint;
begin
  for i:=qi[x] to qi[x+1]-1 do
    if not(bo[b[i]])then
      begin
        dis[b[i]]:=l[i];
        bo[b[i]]:=true;
        buildtree(b[i]);
      end;
end;
procedure dfs(x:longint);
var
  flag:boolean;
  i:longint;
begin
  flag:=false;
  for i:=qi[x] to qi[x+1]-1 do
    if not(bo[b[i]])then
      begin
        bo[b[i]]:=true;
        if not flag then q[x]:=zz+1;
        flag:=true;
        dfs(b[i]);
      end;
  if flag=false then
    begin
      if dis[x]>=mid then inc(now)
      else begin
             inc(zz);
             te[zz]:=dis[x];
           end;
    end
  else begin
         if q[x]>zz then
         begin
           if dis[x]>=mid then inc(now)
           else begin
                  inc(zz);
                  te[zz]:=dis[x];
                end;
         end;
         sort2(q[x],zz);
         la:=zz;
         t:=q[x];
         while t<la do
           if te[la]+te[t]>=mid then
           begin
             te[la]:=0;
             te[t]:=0;
             inc(t);
             dec(la);
             inc(now);
           end
           else dec(la);
         for i:=q[x] to zz do
           if te[i]>0 then begin
                             te[q[x]]:=te[i];
                             break;
                           end;
         te[q[x]]:=te[q[x]]+dis[x];
         if te[q[x]]>=mid then
           begin
             inc(now);
             zz:=q[x]-1;
           end
         else zz:=q[x];
       end;
end;
function solve(x:longint):longint;
begin
   for i:=1 to n do bo[i]:=false;
   now:=0;
   zz:=0;
   bo[1]:=true;
   dfs(1);
   solve:=now;
end;

begin
  assign(input,'track.in');
  reset(input);
  assign(output,'track.out');
  rewrite(output);
  read(n,m);
  boo:=true;
  for i:=1 to n-1 do
    begin
      read(a[i],b[i],l[i]);
      a[i+n-1]:=b[i];
      b[i+n-1]:=a[i];
      l[i+n-1]:=l[i];
      if a[i]<>1 then boo:=false;
    end;
  if boo then
    begin
      for i:=1 to n-1 do te[i]:=l[i];
      sort2(1,n-1);
      if m=n-1 then write(te[n-1])
      else if m=1 then write(te[1]+te[2])
           else begin
                  if te[m]+te[m+1]>te[m-1] then write(te[m-1])
                  else write(te[m]+te[m+1]);
                end
    end
  else
  begin
    sort(1,2*n-2);
    a[0]:=0;
    qi[1]:=1;
    for i:=2 to 2*n-2 do
      if a[i]>a[i-1] then qi[a[i]]:=i;
    qi[n+1]:=2*n-1;
    bo[1]:=true;
    buildtree(1);
    ll:=1;
    rr:=1000000000;
    while ll<=rr do
    begin
      mid:=(ll+rr)div 2;
      if solve(mid)>=m then ll:=mid+1
      else rr:=mid-1;
    end;
    write(rr);
  end;
  close(input);
  close(output);
end.


















