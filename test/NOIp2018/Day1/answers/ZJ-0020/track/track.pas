var a,b,l,w:array[1..50000]of longint;
    map:array[1..1000,1..1000]of longint;
    n,m,i,isans,ans,max:longint;
    flag1,flag2:boolean;
 procedure sort(le,ri: longint);
      var
         i,j,x,y: longint;
      begin
         i:=le;
         j:=ri;
         x:=l[(le+ri) div 2];
         repeat
           while l[i]>x do
            inc(i);
           while x>l[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=l[i];
                l[i]:=l[j];
                l[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if le<j then
           sort(le,j);
         if i<ri then
           sort(i,ri);
      end;
procedure dfs(t:longint);
var i,k:longint;
begin
  if w[t]=0 then
  begin
    if ans>max then max:=ans;
    exit;
  end;
  for i:=1 to n do
    if map[t,i]<>0 then
    begin
      k:=map[t,i];
      ans:=ans+k;
      map[t,i]:=0;
      map[i,t]:=0;
      dec(w[i]);
      dfs(i);
      ans:=ans-k;
      map[t,i]:=k;
      map[i,t]:=k;
      inc(w[i]);
    end;
end;
procedure thend;
begin
  close(input);
  close(output);
end;
begin
  assign(input,'track.in');reset(input);
  assign(output,'track.out');rewrite(output);
  readln(n,m);
  flag1:=true;flag2:=true;
  for i:=1 to n-1 do
  begin
    readln(a[i],b[i],l[i]);
    if b[i]<>a[i]+1 then flag1:=false;
    if a[i]<>1 then flag2:=false;
    map[a[i],b[i]]:=l[i];
    map[b[i],a[i]]:=l[i];
    inc(w[a[i]]);
    inc(w[b[i]]);
  end;
  if flag2 then
  begin
    sort(1,n-1);
    writeln(l[m]);
    thend;
    halt;
  end;
  if m=1 then
  begin
    for i:=1 to n do dfs(i);
    writeln(max);
    thend;
    halt;
  end;
end.
