var n,sum,i,j,k:longint;
    a,id:array[-1..100005]of longint;
    ans,min:int64;
procedure solve;
begin
  while true do
  begin
    k:=0;
    sum:=0;
    for i:=1 to n do if a[i]=0 then inc(sum);
    if sum=n then exit;
    for i:=1 to n do
      if (a[i]<>0) then
      begin
        if (a[i-1]=0) then begin inc(k);id[k]:=i;end;
        if (a[i+1]=0) then begin inc(k);id[k]:=i;end;
      end;
    for i:=1 to k do
    begin
      min:=maxlongint;
      for j:=id[i-1] to id[i] do
        if a[j]<min then min:=a[j];
      for j:=id[i-1] to id[i] do
        a[j]:=a[j]-min;
      ans:=ans+min;
    end;
  end;
end;
begin
  assign(input,'road.in');reset(input);
  assign(output,'road.out');rewrite(output);
  readln(n);
  ans:=0;sum:=n;
  for i:=1 to n do read(a[i]);
  solve;
  writeln(ans);
  close(input);
  close(output);
end.
