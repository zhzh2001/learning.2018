#include<bits/stdc++.h>
#define  ll long long
#define fr(i,x,y) for(int i=(x);i<=(y);i++)
#define rf(i,x,y) for(iny i=(x);i>=(y);i--)
#define frl(i,x,y) for(int i=(x);i<(y);i++)
using namespace std;
const int N=100002;
int n,a[N];
ll ans;

void read(int &x){
	char ch=getchar();x=0;
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+(ch^48);
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	fr(i,1,n){
		read(a[i]);
		if (a[i]>a[i-1]) ans+=a[i]-a[i-1];
	}
	cout<<ans<<endl;
	return 0;
}
