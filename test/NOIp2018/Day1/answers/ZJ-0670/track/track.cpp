#include<bits/stdc++.h>
#define  ll long long
#define fr(i,x,y) for(int i=(x);i<=(y);i++)
#define rf(i,x,y) for(iny i=(x);i>=(y);i--)
#define frl(i,x,y) for(int i=(x);i<(y);i++)
using namespace std;
const int N=50005;
const int M=N<<1;
int n,m;
int cnt,head[N],Next[M],v[M],w[M];

void read(int &x){
	char ch=getchar();x=0;
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+(ch^48);
}

void checkmax(int &x,int y){ if (y>x) x=y; }

void add(int x,int y,int z){
	Next[++cnt]=head[x];
	head[x]=cnt;
	v[cnt]=y;
	w[cnt]=z;
}

namespace case1{
	ll ans=0;
	
	ll dfs(int x,int fa){
		ll ret=0,big=0;
		for(int i=head[x];i;i=Next[i])
		 if (v[i]!=fa){
		 	ll vv=dfs(v[i],x)+w[i];
		 	if (vv>big) big=vv;
		 	if (big>ret) swap(big,ret);
		 }
		ans=max(ans,ret+big);
		return ret;
	}
	
	ll solve(){
		dfs(1,0);
		return ans;
	}
}

namespace case2{
	int val[N];
	
	int check(int x){
		int L=0,r=n-1;
		int s=0;
		while(L<r){
			if (val[r]>=x){
				s++;
				r--;
			}else{
				while(val[L]+val[r]<x&&L<r) L++;
				if (L==r) break;
				L++;r--;s++;
			}
		}
		return s>=m;
	}
	
	ll solve(){
		frl(i,1,n) val[i]=w[i*2];
		sort(val+1,val+n);
		int mid,L=0,r=30000;
		while(L<r){
			mid=(L+r+1)>>1;
			if (check(mid)) L=mid;
			 else r=mid-1;
		}
		return L;
	}
}

namespace case3{
	int val[N];
	
	int check(ll x){
		int s=0;
		ll sum=0;
		frl(i,1,n){
			sum+=val[i];
			if (sum>=x) s++,sum=0;
			if (s>=m) return 1;
		}
		return s>=m;
	}
	
	ll solve(){
		ll mid,L=0,r=500000000;
		while(L<r){
			mid=(L+r+1)>>1;
			if (check(mid)) L=mid;
			 else r=mid-1;
		}
		return L;
	}
}

namespace heeeeey{
	ll f[N],val[N];
	int n,s,b[N];
	
	void dfs(int x,int fa,ll d){
		for(int i=head[x];i;i=Next[i])
		 if (v[i]!=fa) dfs(v[i],x,d);
		n=0;
		for(int i=head[x];i;i=Next[i])
		 if (v[i]!=fa) val[++n]=f[v[i]]+w[i],b[n]=0;
		sort(val+1,val+1+n);
		int L=0,r=n;
		while(L<r){
			if (val[r]>=d){
				b[r]=1;
				s++;
				r--;
			}else{
				while(val[L]+val[r]<d&&L<r) L++;
				if (L>=r) break;
				b[L]=b[r]=1;
				L++;r--;s++;
			}
		}
		f[x]=0;
		fr(i,1,n) if (!b[i]) f[x]=val[i];
	}
	
	int check(ll x){
		s=0;
		dfs(1,0,x);
		return s>=m;
	}	
	
	ll solve(){
		ll mid,L=0,r=500000000;
		while(L<r){
			mid=(L+r+1)>>1;
			if (check(mid)) L=mid;
			 else r=mid-1;
		}
		return L;
	}
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int flag1=0,flag2=1,flag3=1;
	read(n);read(m);
	if (m==1) flag1=1;
	int x,y,z;
	fr(i,2,n){
		read(x);read(y);read(z);
		if (x!=1) flag2=0;
		if (y!=x+1) flag3=0;
		case3::val[x]=z;
		add(x,y,z);add(y,x,z);
	}
	ll ans=0;
	if (flag1) ans=case1::solve();
	 else if (flag2) ans=case2::solve();
	  else if (flag3) ans=case3::solve();
	   else ans=heeeeey::solve();
	printf("%lld\n",ans);
	return 0;
}
