#include<bits/stdc++.h>
#define  ll long long
#define fr(i,x,y) for(int i=(x);i<=(y);i++)
#define rf(i,x,y) for(iny i=(x);i>=(y);i--)
#define frl(i,x,y) for(int i=(x);i<(y);i++)
using namespace std;
const int N=102;
const int M=25005;
int T,n,a[N];
int f[M];

void read(int &x){
	char ch=getchar();x=0;
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+(ch^48);
}

void checkmax(int &x,int y){ if (y>x) x=y; }

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	while(T--){
		read(n);
		int mx=0;
		fr(i,1,n) read(a[i]),checkmax(mx,a[i]);
		sort(a+1,a+1+n);
		memset(f,0,sizeof f);
		f[0]=1;int ans=0;
		fr(i,1,n)
		 if (!f[a[i]]){
		 	ans++;
		 	for(int j=0;j+a[i]<=mx;j++)
		 	 if (f[j]) f[j+a[i]]=1;
		 }
		cout<<ans<<endl;
	}
	return 0;
}
