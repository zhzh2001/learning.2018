#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;
int i,gs,dq,l,r,mid,ans,n,m,u,v,w,xb,pd[200005],bcc[200005],zx[200005],cd[200005],last[200005],vv[200005],f1[200005],f2[200005],bc[200005],ll[200005],rr[200005];
void add(int u,int v,int w){
	xb++;
	bcc[xb]=last[u];
	zx[xb]=v;
	cd[xb]=w;
	last[u]=xb;
}
void dfs(int dqq,int fa){
	vv[dqq]=1;
	f1[dqq]=0;
	f2[dqq]=0;
	for (int i=last[dqq];i!=0;i=bcc[i])
		if (zx[i]!=fa){
			dfs(zx[i],dqq);
			
		}
	gs=0;
	for (int i=last[dqq];i!=0;i=bcc[i])
		if (zx[i]!=fa){
			f1[dqq]+=f1[zx[i]];
			if (f2[zx[i]]+cd[i]>=mid){
				f1[dqq]++;
				continue;
			}
			gs++;
			ll[gs]=gs-1;
			rr[gs]=gs+1;
			bc[gs]=f2[zx[i]]+cd[i];
		}
	sort(bc+1,bc+gs+1);
	dq=gs;
	for (int i=1;i<=gs;i=rr[i]){
		if (dq==i){
			if (rr[dq]!=gs+1){
				dq=rr[dq];
				rr[ll[dq]]=rr[dq];
				ll[rr[dq]]=ll[dq];
				f1[dqq]++;
				pd[i]=dqq;
				pd[dq]=dqq;
				dq=rr[dq];
			}
			continue;
		}
		if ((bc[i]+bc[dq]<mid)){
			if (rr[dq]!=gs+1){
				dq=rr[dq];
				rr[ll[dq]]=rr[dq];
				ll[rr[dq]]=ll[dq];
				f1[dqq]++;
				pd[i]=dqq;
				pd[dq]=dqq;
				dq=ll[dq];
			}
			continue;
		}
		while ((ll[dq]!=i)&&(bc[i]+bc[ll[dq]]>=mid))
			dq=ll[dq];
		rr[ll[dq]]=rr[dq];
		ll[rr[dq]]=ll[dq];
		f1[dqq]++;
		pd[i]=dqq;
		pd[dq]=dqq;
		if (ll[dq]!=i) dq=ll[dq];
			else dq=rr[dq];
	}
	for (int i=gs;i>=1;i--)
		if (pd[i]!=dqq){
			f2[dqq]=bc[i];
			break;
		}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<n;i++){
		scanf("%d%d%d",&u,&v,&w);
		add(u,v,w);
		add(v,u,w);
		r+=w;
	}
	l=0;
	r/=m;
	while (l<=r){
		for (i=1;i<=n;i++)
			pd[i]=0;
		mid=(l+r)/2;
//		printf("T_T%d\n",mid);
		dfs(1,1);
		if (f1[1]>=m){
			ans=mid;
			l=mid+1;
		}
		else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
