#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;
int n,t,q,i,j,k,v,zd,gs,xzs,f,bc[200005],bj[200005],pd[200005],a[200005],bcc[200005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	for (q=1;q<=t;q++){
		scanf("%d",&n);
		for (i=1;i<=n;i++)
			scanf("%d",&bc[i]);
		sort(bc+1,bc+n+1);
		for (i=1;i<=n;i++)
			bj[i]=0;
		for (i=1;i<=n;i++)
			for (j=i+1;j<=n;j++)
				if (bc[j]%bc[i]==0) bj[j]=1;
		zd=0;
		gs=0;
		for (i=1;i<=n;i++)
			if (bj[i]==0){
				gs++;
				a[gs]=bc[i];
				zd=max(zd,a[gs]);
			}
		for (i=0;i<=zd;i++)
			pd[i]=0;
		xzs=0;
		for (i=1;i<=gs;i++){
			f=0;
			for (j=1;j<=xzs;j++){
				for (k=1;k<=a[i]/bcc[j];k++){
					if (pd[a[i]-k*bcc[j]]==1){
						f=1;
						break;
					}
				}
				if (f==1) break;
			}
			if (f==0){
				xzs++;
				bcc[xzs]=a[i];
				for (j=1;j<=zd/bcc[xzs];j++){
					pd[j*bcc[xzs]]=1;
					for (k=j*bcc[xzs];k<=zd;k++)
						pd[k]=max(pd[k-j*bcc[xzs]],pd[k]);
				}
			}
		}
		printf("%d\n",xzs);
	}
	return 0;
}
