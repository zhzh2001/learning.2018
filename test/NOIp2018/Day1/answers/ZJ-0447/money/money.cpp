#include <cstdio>
#include <algorithm>
using namespace std;

const int N=110;
int ti,n,a[N],b[N],len,mx,vis[25010],ans;

int main(void){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (scanf("%d",&ti); ti; --ti){
		scanf("%d",&n),mx=0,ans=0;
		for (int i=1; i<=n; ++i) scanf("%d",&a[i]),mx=max(mx,a[i]);
		sort(a+1,a+1+n);
		for (int i=1; i<=mx; ++i) vis[i]=0;
		for (int i=1; i<=n; ++i)
			if (!vis[a[i]]){
				vis[a[i]]=1,++ans;
				for (int j=1; j<=mx-a[i]; ++j) if (vis[j]) vis[j+a[i]]=1;
			}
		printf("%d\n",ans);
	}
	return 0;
}
