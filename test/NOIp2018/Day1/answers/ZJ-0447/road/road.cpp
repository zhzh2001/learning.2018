#include <cstdio>
using namespace std;

const int N=2e5+10;
int n,sk[N],top,ans;

int main(void){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1,x; i<=n; ++i){
		scanf("%d",&x);
		if (sk[top]>x){
			ans+=sk[top]-x;
			while (top&&sk[top]>x) --top;
		}
		if (sk[top]!=x) sk[++top]=x;
	}
	printf("%d\n",ans+sk[top]);
	return 0;
}
