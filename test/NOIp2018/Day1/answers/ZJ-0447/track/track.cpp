#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int N=5e4+10,inf=0x3f3f3f3f;
struct side{
	int to,w,nt;
}s[N<<1];
int n,m,num,h[N],dis[N],rt,ans,w[N],d[N],len;
//int f[1010][1010][2],g[1010][2],lim;
bool mk1,mk2;

inline void add(int x,int y,int w){
	s[++num]=(side){y,w,h[x]},h[x]=num,++d[x];
	s[++num]=(side){x,w,h[y]},h[y]=num,++d[y];
}

inline void so(int x,int fa){
	if (dis[x]>dis[rt]) ans=dis[rt=x];
	for (int i=h[x]; i; i=s[i].nt)
		if (s[i].to!=fa) dis[s[i].to]=dis[x]+s[i].w,so(s[i].to,x);
	return;
}

inline void dfs(int x,int fa){
	for (int i=h[x]; i; i=s[i].nt)
		if (s[i].to!=fa) w[++len]=s[i].w,dfs(s[i].to,x);
	return;
}

inline bool check(int mid){
	int sum=0,cnt=0;
	for (int i=1; i<n; ++i) if ((sum+=w[i])>=mid) sum=0,++cnt;
	return cnt>=m;
}
/*
inline void dp(int x,int fa){
	for (int j=0; j<=m; ++j)
		for (int k=0; k<=1; ++k)
			f[x][j][k]=inf;
	int mk=0;
	f[x][0][0]=f[x][0][1]=0;
	for (int i=h[x]; i; i=s[i].nt)
		if (s[i].to!=fa){
			so(s[i].to,x);
			if (!mk) memcpy(f[x],f[s[i].to],sizeof f[x]),mk=1;
			
		}
}
*/
int main(void){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m),mk1=mk2=1;
	for (int i=1,x,y,v; i<n; ++i){
		scanf("%d%d%d",&x,&y,&v),add(x,y,v),w[i]=v;
		if (x!=1) mk1=0;
		if (x+1!=y) mk2=0;
	}
	if (m==1) return so(1,0),memset(dis,0,sizeof dis),so(rt,0),printf("%d\n",ans),0;
	if (mk1){
		sort(w+1,w+n),ans=2e9;
		if (2*m>n-1){
			for (int i=1; i<=n-1-m; ++i) ans=min(ans,w[i]+w[2*(n-1-m)+1-i]);
			for (int i=2*(n-m-1)+1; i<n; ++i) ans=min(ans,w[i]);
		}
		else 
			for (int i=1; i<=m; ++i) ans=min(ans,w[i]+w[n-i]);
		return printf("%d\n",ans),0;
	}
	if (mk2){
		for (int i=1; i<=n; ++i) if (d[i]==1){dfs(i,0);break;}
		int l=1,r=1e9,mid;
		while (l<=r)
			if (check(mid=l+r>>1)) l=mid+1;
			else r=mid-1;
		return printf("%d\n",r),0;
	}
/*	if (n<=1000){
		int l=1,r=1e9,mid;
		while (l<=r){
			memset(f,0,sizeof f),lim=mid=l+r>>1,dp(1,0);
			if (f[n][m][0]!=0x3f3f3f3f) l=mid+1;
			else r=mid-1;
		}
		return printf("%d\n",r),0;
	}*/
	return 0;
}
