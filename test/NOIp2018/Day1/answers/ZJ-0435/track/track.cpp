#include<bits/stdc++.h>
using namespace std;
bool SS;
const int N=50005;
struct EDGE {
	int num,nxt,dis;
} E[N<<1];
int H[N],tot,flag,FLAG;
void add(int a,int b,int c) {
	E[++tot]=(EDGE) {
		b,H[a],c
	},H[a]=tot;
}
int n,m,a,b,c;
struct P1 {
	int maxn,D;
	void dfs(int now,int lst,int dis) {
		if(dis>maxn)maxn=dis,D=now;
		for(int i=H[now]; i!=-1; i=E[i].nxt) {
			int nxt=E[i].num;
			int val=E[i].dis;
			if(nxt==lst)continue;
			dfs(nxt,now,dis+val);
		}
	}
	void solved() {
		maxn=-1,dfs(1,-1,0);
		maxn=-1,dfs(D,-1,0);
		printf("%d\n",maxn);
	}
} p1;
struct P2 {
	int sum,cnt;
	void dfs(int now,int lst) {
		for(int i=H[now]; i!=-1; i=E[i].nxt) {
			int nxt=E[i].num;
			if(nxt==lst)continue;
			sum+=E[i].dis;
			dfs(nxt,now);
		}
	}
	void redfs(int now,int lst,int dis,int Need) {
		if(dis>=Need)dis=0,cnt++;
		for(int i=H[now]; i!=-1; i=E[i].nxt) {
			int nxt=E[i].num;
			if(nxt==lst)continue;
			redfs(nxt,now,dis+E[i].dis,Need);
		}
	}
	bool check(int num) {
		cnt=0;
		if(1ll*num*m>sum)return false;
		redfs(1,-1,0,num);
		return cnt>=m;
	}
	void solved() {
		sum=0,dfs(1,-1);
		int L=1,R=sum,ans=0;
		while(L<=R) {
			int mid=(L+R)>>1;
			if(check(mid))
				ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
} p2;
struct P3 {
	int val[N],tot,sum;
	void dfs(int now,int lst) {
		for(int i=H[now]; i!=-1; i=E[i].nxt) {
			int nxt=E[i].num;
			if(nxt==lst)continue;
			sum=sum+E[i].dis;
			val[++tot]=E[i].dis;
			dfs(nxt,now);
		}
	}
	bool check(int num) {
		int cnt=0,R=tot,L=1;
		while(R>0&&val[R]>=num)
			R--,cnt++;
		if(cnt>=m)return true;
		while(1) {
			while(L<R&&val[R]+val[L]<num)
				L++;
			if(L<R)cnt++,R--,L++;
			if(cnt>=m||L>=R)break;
		}
		return cnt>=m;
	}
	void solved() {
		sum=0,tot=0,dfs(1,-1);
		sort(val+1,val+1+tot);
		int L=1,R=sum,ans=-1;
		while(L<=R) {
			int mid=(L+R)>>1;
			if(check(mid))
				ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
} p3;
struct P4 {
	int mark[1005],cnt,dis[1005];
	void dfs(int now,int lst,int Need) {
		for(int i=H[now]; i!=-1; i=E[i].nxt) {
			int nxt=E[i].num;
			if(nxt==lst)continue;
			dfs(nxt,now,Need);
			if(dis[nxt]+E[i].dis>=Need)
				cnt++,mark[nxt]=1;
		}
		int maxn1=0,maxn2=0,id1=0,id2=0;
		for(int i=H[now]; i!=-1; i=E[i].nxt) {
			int nxt=E[i].num;
			if(nxt==lst||mark[nxt])continue;
			if(dis[nxt]+E[i].dis>maxn1) {
				maxn2=maxn1,id2=id1;
				maxn1=dis[nxt]+E[i].dis;
				id1=nxt;
			} else if(dis[nxt]+E[i].dis>maxn2)
				maxn2=dis[nxt]+E[i].dis,id2=nxt;
		}
		if(maxn1+maxn2>=Need)
			cnt++,mark[id1]=1,mark[id2]=1;
		for(int i=H[now]; i!=-1; i=E[i].nxt) {
			int nxt=E[i].num;
			if(nxt==lst||mark[nxt])continue;
			dis[now]=max(dis[now],dis[nxt]+E[i].dis);
		}
	}
	bool check(int num) {
		memset(mark,0,sizeof(mark));
		memset(dis,0,sizeof(dis));
		cnt=0,dfs(1,0,num);
		return cnt>=m;
	}
	void solved() {
		int L=1,R=1e9,ans=-1;
		while(L<=R) {
			int mid=(L+R)>>1;
			if(check(mid))
				ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
} p4;
bool TT;
int main() {
//	file memory long long mod const
//	printf("%.9f\n",(&TT-&SS)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(H,-1,sizeof(H));
	for(int i=2; i<=n; i++) {
		scanf("%d%d%d",&a,&b,&c);
		add(a,b,c),add(b,a,c);
		if(b!=a+1)flag=1;
		if(a!=1)FLAG=1;
	}
	if(m==1)p1.solved();
	else if(flag==0)p2.solved();
	else if(FLAG==0)p3.solved();
	else p4.solved();
	return 0;
}
