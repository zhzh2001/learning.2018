#include<bits/stdc++.h>
using namespace std;
bool SS;
const int N=100005;
int n,A[N];
struct P1 {
	void solved() {
		int ans=0;
		while(1) {
			ans++;
			int L=-1;
			for(int i=1; i<=n; i++) {
				if(A[i]>0&&L==-1) {
					L=i;
					break;
				}
			}
			if(L==-1)break;
			for(int i=L; i<=n; i++) {
				if(A[i]>0)A[i]--;
				else break;
			}
		}
		printf("%d\n",ans-1);
	}
} p1;
struct P2 {
	void solved() {
		int now=0,ans=0;
		for(int i=1; i<=n; i++) {
			if(A[i]>now)ans+=A[i]-now,now=A[i];
			if(A[i]<now)now=A[i];
		}
		printf("%d\n",ans);
	}
} p2;
bool TT;
int main() {
//	file memory long long mod const 
//	printf("%.9f\n",(&TT-&SS)/1024.0/1024);
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1; i<=n; i++)
		scanf("%d",&A[i]);
	if(n<=10)
		p1.solved();
	else p2.solved();
	return 0;	
}
