#include<bits/stdc++.h>
using namespace std;
bool SS;
const int N=105;
int T,n,A[N];
struct P1 {
	bool dfs(int now,int cnt,int Limit) {
		if(cnt==Limit)
			return now==0;
		if(now%A[cnt]==0)
			return true;
		int CNT=now/A[cnt];
		for(int i=0; i<=CNT; i++)
			if(dfs(now-i*A[cnt],cnt+1,Limit))
				return true;
		return false;
	}
	bool check(int k) {
		return dfs(A[k],1,k);
	}
	void solved() {
		int ans=n;
		sort(A+1,A+1+n);
		for(int i=n; i>=2; i--)
			if(check(i))ans--;
		printf("%d\n",ans);
	}
} p1;
bool TT;
int main() {
//	file memory long long mod const
//	printf("%.9f\n",(&TT-&SS)/1024.0/1024);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--) {
		scanf("%d",&n);
		for(int i=1; i<=n; i++)
			scanf("%d",&A[i]);
		p1.solved();
	}
	return 0;
}
