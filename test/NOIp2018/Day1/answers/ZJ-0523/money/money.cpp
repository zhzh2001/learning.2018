#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
using namespace std;
int T,M,N,A[105],x,y,Ans;
bool valid[105],F[25005];
inline void read(int &Res){
	char ch=getchar(); bool fl=false;
	for (Res=0;!isdigit(ch);ch=getchar()) if (ch=='-') fl=true;
	for (;isdigit(ch);ch=getchar()) Res=(Res<<3)+(Res<<1)+ch-48;
	if (fl) Res=-Res;
}
int Exgcd(int a,int b,int &x,int &y){
	if (b){int q=Exgcd(b,a%b,y,x); return y-=a/b*x,q;}
		else return x=1,y=0,a;
}
inline void Solve(){
	read(M),N=0,Ans=0,memset(valid,1,sizeof valid);
	for (int i=M;i;--i) read(A[i]);
	sort(A+1,A+1+M),memset(F,0,sizeof F),F[0]=true;
	for (int i=1;i<M;++i) if (valid[i])
		for (int j=M;j>i;--j) if (!(A[j]%A[i])) valid[j]=false;
	for (int i=1;i<=M;++i) if (valid[i]) A[++N]=A[i]; else valid[i]=true;
	for (int i=1;i<N-1;++i) if (valid[i])
		for (int j=i+1;j<N;++j) if (valid[j])
			for (int k=N;k>j;--k) if (valid[k]){
				int d=Exgcd(A[i],A[j],x,y);
				if (A[k]%d) continue;
				x*=A[k]/d,y*=A[k]/d; int g=A[j]/d,t;
				if (x<0) t=x/g-1,(x%=g)+=g;
					else t=x/g,x%=g;
				y+=A[i]/d*t;
				if (y<0) continue;
				valid[k]=false;
			}
	M=N,N=0;
	for (int i=1;i<=M;++i) if (valid[i]) A[++N]=A[i],++Ans;
	for (int i=1;i<=N;++i){
		if (F[A[i]]){--Ans; continue;}
		for (int j=A[i];j<=A[N];++j) F[j]|=F[j-A[i]];
	}
	printf("%d\n",Ans);
}
int main(){
	freopen("money.in","r",stdin),freopen("money.out","w",stdout);
	for (read(T);T;--T) Solve();
	return 0;
}
