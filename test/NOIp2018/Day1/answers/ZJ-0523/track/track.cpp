#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
#include <set>
#include <vector>
using namespace std;
const int maxn=50005,maxe=100005;
int N,M,lnk[maxn],nxt[maxe],son[maxe],w[maxe],tot=1,Sum_w;
inline void read(int &Res){
	char ch=getchar(); bool fl=false;
	for (Res=0;!isdigit(ch);ch=getchar()) if (ch=='-') fl=true;
	for (;isdigit(ch);ch=getchar()) Res=(Res<<3)+(Res<<1)+ch-48;
	if (fl) Res=-Res;
}
inline void Add(int x,int y,int z){nxt[++tot]=lnk[x],son[lnk[x]=tot]=y,w[tot]=z;}
namespace Sub_M_1{
	int dis[maxn],que[maxn],hd,tl,k,Far;
	bool vis[maxn];
	inline void BFS(int St){
		for (memset(vis,0,sizeof vis),hd=dis[St]=0,vis[que[tl=1]=St]=true;hd<tl;)
			for (int j=lnk[k=que[++hd]];j;j=nxt[j]) if (!vis[son[j]])
				vis[que[++tl]=son[j]]=true,dis[son[j]]=dis[k]+w[j];
	}
	inline void Solve(){
		BFS(1),Far=max_element(dis+1,dis+1+N)-dis;
		BFS(Far),printf("%d\n",*max_element(dis+1,dis+1+N));
	}
}
namespace Sub_Fl_1{
	int A[maxn],L,R,mid;
	multiset<int> F;
	multiset<int>::iterator it;
	inline bool check(int x){
		int Ret=0; F.clear();
		for (int i=N;i>1;--i) if (A[i]<x){
			it=F.lower_bound(x-A[i]);
			if (it!=F.end()){
				F.erase(it);
				if ((++Ret)>=M) return true;
			} else F.insert(A[i]);
		} else{
			if ((++Ret)>=M) return true;
		}
		return false;
	}
	inline void Solve(){
		for (int j=lnk[1];j;j=nxt[j]) A[son[j]]=w[j];
		sort(A+2,A+1+N),reverse(A+2,A+1+N);
		for (L=0,R=Sum_w;L<=R;) check(mid=(L+R)>>1)?L=mid+1:R=mid-1;
		printf("%d\n",R);
	}
}
namespace Sub_Link{
	int A[maxn],L,R,mid;
	inline bool check(int x){
		int Ret=0,Sum=0;
		for (int i=1;i<N;++i) if ((Sum+=A[i])>=x){
			if ((++Ret)>=M) return true;
			Sum=0;
		}
		return false;
	}
	inline void Solve(){
		for (int i=1;i<N;++i)
			for (int j=lnk[i];j;j=nxt[j]) if (son[j]>i) A[i]=w[j];
		for (L=0,R=Sum_w;L<=R;) check(mid=(L+R)>>1)?L=mid+1:R=mid-1;
		printf("%d\n",R);
	}
}
//bool vis[maxn],T[maxn<<2];
//int que[maxn],hd,tl,dis[maxn],pre[maxn],cnt,L,R,mid,dfn[maxn],lst[maxn],siz[maxn],dep[maxn],Timed,fa[maxn],pos[maxn],top[maxn],k;
//void Update(int p,int L,int R,int x,int y){
//	if (x<=L&&R<=y){T[p]=true; return;}
//	int mid=(L+R)>>1;
//	if (x<=mid) Update(p<<1,L,mid,x,y);
//	if (y>mid) Update(p<<1|1,mid+1,R,x,y);
//}
//bool Query(int p,int L,int R,int x,int y){
//	if (T[p]) return true;
//	if (x<=L&&R<=y) return false;
//	int mid=(L+R)>>1;
//	if (x<=mid&&Query(p<<1,L,mid,x,y)) return true;
//	if (y>mid&&Query(p<<1|1,mid+1,R,x,y)) return true;
//	return false;
//}
//struct Ad{
//	int x,y,z;
//	inline bool operator < (const Ad &c) const{return z<c.z;}
//}A[1000005];
//inline void BFS(int St){
//	for (memset(vis,0,N+2),pre[St]=hd=dis[St]=0,vis[que[tl=1]=St]=true;hd<tl;)
//		for (int j=lnk[k=que[++hd]];j;j=nxt[j]) if (!vis[son[j]])
//			vis[que[++tl]=son[j]]=true,dis[son[j]]=dis[k]+w[j],pre[son[j]]=j;
//	for (int i=St;i;--i) A[++cnt]=(Ad){i,St,dis[i]};
//}
//void DFS_1(int x){
//	siz[x]=1;
//	for (int j=lnk[x];j;j=nxt[j]) if (!siz[son[j]]) dep[son[j]]=dep[x]+1,fa[son[j]]=x,DFS_1(son[j]),siz[x]+=siz[son[j]];
//}
//inline int Upm(int &x,int y){if (y>x) x=y;}
//void DFS_2(int x,int rt){
//	top[pos[dfn[x]=lst[x]=++Timed]=x]=rt; int Mx=0,id=0;
//	for (int j=lnk[x];j;j=nxt[j]) if (!dfn[son[j]]&&siz[son[j]]>Mx) Mx=siz[id=son[j]];
//	if (!id) return; DFS_2(id,rt),Upm(lst[x],lst[id]);
//	for (int j=lnk[x];j;j=nxt[j]) if (!dfn[son[j]]&&(id^son[j])) DFS_2(son[j],son[j]),Upm(lst[x],lst[son[j]]);
//}
//inline void Modify(int x,int y){
//	while (x^y) if (top[x]^top[y]){
//		if (dep[top[x]]<dep[top[y]]) swap(x,y);
//		Update(1,1,N,dfn[top[x]],dfn[x]),x=fa[top[x]];
//	} else{
//		if (dfn[x]>dfn[y]) swap(x,y);
//		if (dfn[x]<dfn[y]) Update(1,1,N,dfn[x]+1,dfn[y]);
//		y=x;
//	}
//}
//inline bool Get(int x,int y){
//	while (x^y) if (top[x]^top[y]){
//		if (dep[top[x]]<dep[top[y]]) swap(x,y);
//		if (Query(1,1,N,dfn[top[x]],dfn[x])) return false;
//		x=fa[top[x]];
//	} else{
//		if (dfn[x]>dfn[y]) swap(x,y);
//		if (dfn[x]<dfn[y]&&Query(1,1,N,dfn[x]+1,dfn[y])) return false;
//		y=x;
//	}
//	return true;
//}
//inline bool check(int x){
//	int Ret=0;
//	for (int i=lower_bound(A+1,A+1+cnt,(Ad){0,0,x})-A;i<=cnt;++i) if (Get(A[i].x,A[i].y)){
//		if ((++Ret)>=M) return true;
//		Modify(A[i].x,A[i].y);
//	}
//	return false;
//}
int main(){
	freopen("track.in","r",stdin),freopen("track.out","w",stdout);
	read(N),read(M); bool S_Fl=true,S_Lk=true;
	for (int i=1,x,y,z;i<N;++i){
		read(x),read(y),read(z),Add(x,y,z),Add(y,x,z),Sum_w+=z;
		if (x!=1) S_Fl=false;
		if (y!=x+1) S_Lk=false;
	}
	if (M==1) return Sub_M_1::Solve(),0;
	if (S_Lk) return Sub_Link::Solve(),0;
	if (S_Fl) return Sub_Fl_1::Solve(),0;
//	for (int i=N;i;--i) BFS(i);
//	sort(A+1,A+1+cnt),DFS_1(1),DFS_2(1,1);
//	for (L=0,R=Sum_w;L<=R;) check(mid=(L+R)>>1)?L=mid+1:R=mid-1;
	return printf("%d\n",Sum_w/M),0;
}
