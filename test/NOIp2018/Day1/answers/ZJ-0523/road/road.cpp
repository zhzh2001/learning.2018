#include <cstdio>
#include <cctype>
const int maxn=100005;
int N,F[18][maxn],A[maxn],lg2[maxn];
inline void read(int &Res){
	char ch=getchar(); bool fl=false;
	for (Res=0;!isdigit(ch);ch=getchar()) if (ch=='-') fl=true;
	for (;isdigit(ch);ch=getchar()) Res=(Res<<3)+(Res<<1)+ch-48;
	if (fl) Res=-Res;
}
inline int Min(int x,int y){return A[x]<A[y]?x:y;}
inline int Query(int L,int R){
	int w=lg2[R-L+1];
	return Min(F[w][L],F[w][R+1-(1<<w)]);
}
int DFS(int L,int R,int x){
	if (L>R) return 0; if (L==R) return A[L]-x; int mid=Query(L,R);
	return DFS(L,mid-1,A[mid])+DFS(mid+1,R,A[mid])+A[mid]-x;
}
int main(){
	freopen("road.in","r",stdin),freopen("road.out","w",stdout);
	read(N);
	for (int i=1;i<=N;++i) read(A[i]),F[0][i]=i;
	for (int i=2;i<=N;++i) lg2[i]=lg2[i>>1]+1;
	for (int j=1;(1<<j)<=N;++j)
		for (int i=N+1-(1<<j);i>0;--i) F[j][i]=Min(F[j-1][i],F[j-1][i+(1<<(j-1))]);
	return printf("%d\n",DFS(1,N,0)),0;
}
