#include<bits/stdc++.h>
#define N 105
#define ll long long
using namespace std;
int n,m,A[N];
struct P50{
	int dp[25005];
	void solve(){
		int ans=0;
		for(int i=0;i<=m;++i)dp[i]=0;
		dp[0]=1;
		for(int i=1;i<=n;++i){
			int x=A[i];
			if(dp[x])continue;
			ans++;
			for(int j=0;j<=m-x;++j){
				if(dp[j])dp[j+x]=1;
			}
		}
		printf("%d\n",ans);
	}
}P50;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=n;++i)scanf("%d",&A[i]);
		sort(A+1,A+n+1);m=A[n];
//		if(n<=5&&m<=1000)P50.solve();
//		else if(n<=25&&m<=40)
		P50.solve();
	}
	return 0;
}
