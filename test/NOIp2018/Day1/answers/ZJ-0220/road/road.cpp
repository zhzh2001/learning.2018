#include<bits/stdc++.h>
#define ll long long
#define M 100005
using namespace std;
int n,A[M];
struct P70{
	void solve(){
		int ans=0,s=1;
		while(1){
			int L=0,R=n+1,mn=1000000;
			for(int i=s;i<=n;++i){
				if(L==0&&A[i])L=i;
				if(L&&A[i]==0){
					R=i;break;
				}
				if(L)mn=min(mn,A[i]);
			}s=L;
			if(L==0)break;
			R--;ans+=mn;
			for(int i=L;i<=R;++i)A[i]-=mn;
//			printf("ans=%d L=%d R=%d mn=%d:\n",ans,L,R,mn);
//			for(int i=1;i<=n;++i)printf("%d ",A[i]);puts("");
		}
		printf("%d\n",ans);
	}
}P70;
struct P100{
	int mx[5],mn[5];
	void solve(){
		int p=0,s=n+1,ans=0;
		mx[0]=0;mx[1]=0;
		mn[0]=100000;mn[1]=0;
		for(int i=1;i<=n;++i){
			if(i!=1&&A[i]>A[i-1]){
				s=i;break;
			}
			mx[p]=max(mx[p],A[i]);
			mn[p]=min(mn[p],A[i]);
		}
//		printf("s=%d mx=%d mn=%d\n",s,mx[p],mn[p]);
		for(int i=s;i<=n;++i){
			if(A[i]>A[i-1]){
				ans+=max(0,mx[p]-mn[!p]);
				p=!p;
				mx[p]=0;mn[p]=100000;
			}
			mx[p]=max(mx[p],A[i]);
			mn[p]=min(mn[p],A[i]);
		}ans+=max(0,mx[p]-mn[!p]);
		printf("%d\n",ans);
	}
}P100;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;++i)scanf("%d",&A[i]);
	if(n<=1000)P70.solve();
	else P100.solve();
	return 0;
}
