#include<bits/stdc++.h>
#define M 50005
#define ll long long
using namespace std;
int n,m,a[M],b[M],c[M];
struct node{
	int to,cost;
};
vector<node>G[M];
void Add_edge(int a,int b,int c){
	G[a].push_back((node){b,c});
	G[b].push_back((node){a,c});
}
struct P1{
	int s,mx;
	void dfs(int x,int f,int val){
		if(val>mx){
			mx=val;s=x;
		}
		for(int i=0;i<G[x].size();++i){
			node nxt=G[x][i];
			int y=nxt.to;
			if(y==f)continue;
			dfs(y,x,val+nxt.cost);
		}
	}
	void solve(){
		s=1;mx=0;
		dfs(1,1,0);
		dfs(s,s,0);
		printf("%d\n",mx);
	}
}P1;
struct P_lian{
	int A[M];
	bool check(int x){
		int cnt=0,tmp=0;
		for(int i=1;i<n&&cnt<m;++i){
			tmp+=A[i];
			if(tmp>=x){
				tmp=0;
				cnt++;
			}
		}
		return cnt>=m;
	}
	void solve(){
		int mx=0;
		for(int i=1;i<n;++i){
			A[a[i]]=c[i];mx+=c[i];
		}
		int L=1,R=mx,ans=L;
		while(L<=R){
			int mid=(L+R)>>1;
			if(check(mid)){
				L=mid+1;
				ans=mid;
			}else R=mid-1;
		}
		printf("%d\n",ans);
	}
}P_lian;
struct P2{
	bool mark[M];
	bool check(int x){
		for(int i=0;i<n;++i)mark[i]=0;
		int p=n,cnt=0;
		for(int i=1;i<n;++i){
			if(c[i]>=x){
				p=i;break;
			}
		}
		cnt=n-p;
		for(int i=1;i<p;++i)if(!mark[i]){
			mark[i]=1;
			for(int j=2;j<p;++j)if(!mark[j]){
				if(c[i]+c[j]>=x){
					cnt++;mark[j]=1;
					break;
				}
			}
		}
		return cnt>=m;
	}
	void solve(){
		sort(c+1,c+n);
		int L=1,R=0;
		for(int i=1;i<n;++i)R+=c[i];
		int ans=0;
		while(L<=R){
			int mid=(L+R)>>1;
			if(check(mid)){
				L=mid+1;
				ans=mid;
			}else R=mid-1;
		}
		printf("%d\n",ans);
	}
}P2;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d %d",&n,&m);
	bool F_lian=1,F_a1=1;
	for(int i=1;i<n;++i){
		scanf("%d %d %d",&a[i],&b[i],&c[i]);
		if(a[i]!=1)F_a1=0;
		if(b[i]!=a[i]+1)F_lian=0;
		Add_edge(a[i],b[i],c[i]);
	}
	if(m==1)P1.solve();
	else if(F_lian)P_lian.solve();
	else if(F_a1)P2.solve();
	return 0;
}
