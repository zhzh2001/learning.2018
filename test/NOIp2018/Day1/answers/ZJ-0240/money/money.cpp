#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
using namespace std;
ll T,n,m,a[110];
bool f[30100];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	cin>>T;
	while(T--)
	{
		cin>>n;m=n;
		For(i,1,n)cin>>a[i];
		sort(a+1,a+n+1);
		memset(f,false,sizeof(f));
		f[0]=true;
		For(i,1,n)
		{
			if(f[a[i]])
			{
				m--;
				continue;
			}
			For(j,0,a[n])
			{
				if(j+a[i]>a[n])break;
				if(!f[j])continue;
				f[j+a[i]]=true;
			}
		}
		cout<<m<<endl;
	}
	return 0;
}
