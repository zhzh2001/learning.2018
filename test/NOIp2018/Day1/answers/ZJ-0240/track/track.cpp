#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define pb push_back
using namespace std;
vector<int>a[50100];
vector<int>b[50100];
int n,m,ans;
int fa[50100],sd[50100],fa1[50100];
int c[50100];
//int f[1010][1010];
//bool g[1010][1010];
int sz,d[1000100];
bool flag,ff;
void dfs(int x,int fr)
{
	fa[x]=fr;sd[x]=sd[fr]+1;
	For(i,0,a[x].size()-1)
	{
		if(a[x][i]==fr)continue;
		fa1[a[x][i]]=b[x][i];
		dfs(a[x][i],x);
	}
	return;
}
void del(int wz)
{
	d[wz]=d[sz];
	sz--;
	while(wz*2<=sz)
	{
		int mn=wz*2;
		if(d[mn]<d[wz*2+1]&&wz*2+1<=sz)mn=wz*2+1;
		if(d[mn]<d[wz])return;
		swap(d[wz],d[mn]);
		wz=mn;
	}
	return;
}
void up(int wz)
{
	while(d[wz]>d[wz/2]&&wz!=1)
	{
		swap(d[wz],d[wz/2]);
		wz/=2;
	}
	return;
}
void ins(int x)
{
	sz++;
	d[sz]=x;
	up(sz);
	return;
}
void dfs1(int x,int y,int mn)
{
	if(x==n)
	{
		if(y==m)
		{
			ans=max(ans,mn);
		}
		return;
	}
	if(y==m)
	{
		ans=max(ans,mn);
		return;
	}
	if(mn<ans)return;
	int sum=0;
	while(x!=n)
	{
		sum+=c[x];
		x++;
		if(sum>ans)dfs1(x,y+1,min(mn,sum));
	}
	return;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	flag=true;
	ff=true;
	For(i,1,n-1)
	{
		int x,y,z;
		cin>>x>>y>>z;
		a[x].pb(y);a[y].pb(x);
		b[x].pb(z);b[y].pb(z);
		c[i]=-z;
		if(x!=1)flag=false;
		if(y!=x+1)ff=false;
	}
	if(flag)
	{
		sort(c+1,c+n);
		For(i,m+1,n-1)
		{
			c[2*m+1-i]+=c[i];
		}
		sort(c+1,c+m+1);
		cout<<-c[m]<<endl;
		return 0;
	}
	if(ff)
	{
		ans=0;
		For(i,1,n-1)
		{
			For(j,0,a[i].size()-1)
			{
				if(a[i][j]==i+1)
				{
					c[i]=b[i][j];
				}
			}
		}
		dfs1(1,0,1e9+7);
		cout<<ans<<endl;
		return 0;
	}
	sd[0]=0;
	dfs(1,0);
	For(i,1,n)
	{
		For(j,i+1,n)
		{
			int x,y,le;
			x=i;y=j;le=0;
			if(sd[x]>sd[y])swap(x,y);
			while(sd[y]>sd[x])
			{
				le+=fa1[y];
				y=fa[y];
			}
			while(x!=y)
			{
				le+=fa1[x];
				le+=fa1[y];
				x=fa[x];
				y=fa[y];
			}
			ins(le);
//			cout<<i<<" "<<j<<" "<<le<<endl;
		}
	}
	while(m--)
	{
		ans=d[1];
		del(1);
	}
	cout<<ans<<endl;
	return 0;
}
