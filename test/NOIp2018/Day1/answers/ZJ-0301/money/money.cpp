#include <bits/stdc++.h>
#define N 25020
#define SSS 175
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int a[105];
bool f[N];
int q[105], top;

bool can(int x, int p) {
  // printf("dfs %d %d\n", x, p);
  if (f[x]) return true;
  if (p == 1) return f[x] = (x % q[1] == 0);
  if (p == 0) return false;
  for (int i = 0; i <= x; i += q[p]) {
    if (can(x - i, p - 1)) {
      return f[x] = true;
    }
  }
  return false;
}

int main(int argc, char const *argv[]) {
  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);

  int T = read();

  while (T --) {
    int n = read();
    for (int i = 1; i <= n; ++ i) {
      a[i] = read();
    }
    sort(a + 1, a + n + 1);
    top = 0;
    memset(f, 0, sizeof f);
    f[0] = 1;
    for (int i = 1; i <= n; ++ i) {
      if (!can(a[i], top)) {
        q[++ top] = a[i];
      }
    }

    printf("%d\n", top);
  }
  // fprintf(stderr, "%d\n", clock());

  return 0;
}
/*
RP++
*/
