#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

typedef long long ll;
int d[N];

int main(int argc, char const *argv[]) {
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);

  int n = read();
  for (int i = 1; i <= n; ++ i) {
    d[i] = read();
  }
  ll ans = d[1];
  for (int i = 2; i <= n; ++ i) {
    ans += d[i] > d[i - 1] ? d[i] - d[i - 1] : 0;
  }
  printf("%lld\n", ans);

  return 0;
}
