#include<bits/stdc++.h>
#define ll long long
#define M 50005
using namespace std;
bool llq;
struct Edge{
	int lt,to,v;
}edge[M<<1];
int tot,h[M],n,m,deg[M];
void add(int u,int v,int w){
	edge[++tot]=(Edge){h[u],v,w};
	h[u]=tot;
}
struct sc20{
	int dis1[M],dis2[M],ans;
	void dfs(int x,int f){
		dis1[x]=dis2[x]=0;
		for(int i=h[x];i;i=edge[i].lt){
			int y=edge[i].to;
			if (y==f) continue;
			dfs(y,x);
			if (dis1[x]<=dis1[y]+edge[i].v){
				dis2[x]=dis1[x];
				dis1[x]=dis1[y]+edge[i].v;
			}else if (dis2[x]<dis1[y]+edge[i].v)
				dis2[x]=dis1[y]+edge[i].v;
		}
		ans=max(ans,dis1[x]+dis2[x]);
	}
	void solve(){
		ans=0;
		dfs(1,0);
		printf("%d\n",ans);
	}
}p20;
struct sclian{
	int val[M],sum;
	void dfs(int x,int f){
		for(int i=h[x];i;i=edge[i].lt){
			int y=edge[i].to;
			if (y==f) continue;
			val[y]=edge[i].v;
			sum+=val[y];
			dfs(y,x);
		}
	}
	bool check(int x){
		int cnt=0,o=0;
		for(int i=1;i<=n;++i){
			o+=val[i];
			if (o>=x){
				cnt++; o=0;
			}
		}
		return cnt>=m;
	}
	void solve(){
		val[1]=0;
		dfs(1,0);
		int l=1,r=sum+1;
		while (l<r-1){
			int mid=(l+r)>>1;
			if (check(mid)) l=mid;
			else r=mid;
		}
		printf("%d\n",l);
	}
}plian;
bool cmp(int x,int y){
	return x>y;
}
struct sc15{
	int val[M<<1];
	void solve(){
		int l=0,ans=1e9,st=1;
		for(int i=h[1];i;i=edge[i].lt)
			val[++l]=edge[i].v;
		sort(val+1,val+l+1,cmp);
		if (m==n-1){
			printf("%d\n",val[l]);
			return;
		}
		for(int i=1;i<=m;++i)
			ans=min(ans,val[i]+val[2*m-i+1]);
		printf("%d\n",ans);	
	}
}p15;
struct sc25{
	int cnt,dis[M][3];
	void dfs(int x,int f,int p){
		int len=0;
		for(int i=h[x];i;i=edge[i].lt){
			int y=edge[i].to;
			if (y==f) continue;
			dfs(y,x,p);
			dis[x][len++]=dis[y][0]+edge[i].v;
		}
		if (dis[x][0]<dis[x][1]) swap(dis[x][0],dis[x][1]);
		if (dis[x][1]>=p){ cnt+=2; dis[x][0]=dis[x][1]=0;}
		else if (dis[x][0]>=p){ cnt++; swap(dis[x][0],dis[x][1]);}
		else if (dis[x][0]+dis[x][1]>=p){ cnt++; dis[x][0]=dis[x][1]=0;}
	}
	bool check(int x){
		cnt=0;
		dfs(1,0,x);
		return cnt>=m;
	}
	void solve(){
		int l=1,r=0;
		for(int i=1;i<=tot;++i) r+=edge[i].v;
		r=r/2+1;
		while (l<r-1){
			int mid=(l+r)>>1;
			if (check(mid)) l=mid;
			else r=mid;
		}
		printf("%d\n",l);
	}
}p25;
bool LLQ;
int main(){
//	srand(time(NULL));
//	printf("%lf\n",(&LLQ-&llq)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool pd1=1,pd2=1,pd3=1;
	for(int i=1;i<n;++i){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		add(u,v,w); add(v,u,w);
		if (u!=1) pd1=0;
		if (v!=u+1) pd2=0;
		deg[u]++; deg[v]++;
	}
	for(int i=1;i<=n;++i)
		if (deg[i]>3) pd3=0;
	if (m==1)
		p20.solve();
	else if (pd2)
		plian.solve();
	else if (pd1)
		p15.solve();
	else if (pd3)
		p25.solve();
	else printf("%d\n",n);
	return 0;
}
