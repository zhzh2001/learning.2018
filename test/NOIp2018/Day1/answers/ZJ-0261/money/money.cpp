#include<bits/stdc++.h>
#define ll long long
using namespace std;
bool llq;
int n,a[105],T,dp[25005];
bool LLQ;
int main(){
//	srand(time(NULL));
//	printf("%lf\n",(&LLQ-&llq)/1024.0/1024);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		for(int i=1;i<=n;++i) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		for(int i=1;i<=a[n];++i) dp[i]=0;
		dp[0]=1;
		int ans=0;
		for(int i=1;i<=n;++i){
			if (dp[a[i]]) continue;
			ans++;
			for(int j=a[i];j<=a[n];++j) 
				if (dp[j-a[i]]) dp[j]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
