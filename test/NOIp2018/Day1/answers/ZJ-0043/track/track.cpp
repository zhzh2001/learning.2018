#include<bits/stdc++.h>
#define FOR(i,a,b) for(int i=(a),i##_end_=(b);i<=i##_end_;++i)
#define DOR(i,a,b) for(int i=(a),i##_end_=(b);i>=i##_end_;--i)
#define SFOR(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;++i)
#define SDOR(i,a,b) for(int i=(a)-1,i##_end_=(b);i>=i##_end_;--i)
#define debug(x) cerr<<#x<<"="<<x<<endl
#define bug() cerr<<"Surprise MotherFucker"<<endl
#define fi first
#define se second
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
#define M 100086
bool mmm1;
struct Node{
	int to,nxt,co;
}G[M<<1];
int head[M],Tot;
void add_edge(int a,int b,int c){
	G[Tot]=(Node){b,head[a],c};
	head[a]=Tot++;
}
int Fa[M];
int n,m;
int Top[M],dep[M];
struct P30{
	int Maxdis,id;
	void dfs(int x,int f,int d){
		if(d>Maxdis)Maxdis=d,id=x;
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(y==f)continue;
			dfs(y,x,d+G[i].co);
		}
	}
	void solve(){
		Maxdis=id=0;
		dfs(1,0,0);
		dfs(id,0,0);
		cout<<Maxdis<<endl;
	}
}p30;
struct P40{
	int A[M],tot;
	bool mark[M];
	void dfs(int x,int f,int d){
		if(x!=1)A[++tot]=d;
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(y==f)continue;
			dfs(y,x,d+G[i].co);
		}
	}
	bool check(int x){
		int cnt=0;
		int top=1;
		memset(mark,0,sizeof(mark));
		DOR(i,tot,1){
			if(A[i]>=x){
				++cnt;
				continue;
			}
			while(top<i&&(A[i]+A[top]<x||mark[top]))++top;
			if(top<i&&!mark[top]){
				++cnt;
				mark[top]=1;
			}
		}
		return cnt>=m;
	}
	void solve(){
		tot=0;
		dfs(1,0,0);
		sort(A+1,A+tot+1);
		int l=0,r=1e9,res=0;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid)){
				res=mid;
				l=mid+1;
			}else r=mid-1;
		}
		cout<<res<<endl;
	}
}p40;
int v[M];
struct P55{
	int sum[M],Max[M],dp[M];
	bool check(int x){
		dp[1]=0;
		Max[1]=0;
		int cnt=0;
		FOR(i,2,n){
			int p=upper_bound(sum+1,sum+i,sum[i]-x)-sum-1;
			if(!p)dp[i]=0;
			else dp[i]=Max[p]+1;
			Max[i]=max(Max[i-1],dp[i]);
			cnt=max(cnt,dp[i]);
		}
		return cnt>=m;
	}
	void solve(){
		FOR(i,2,n)sum[i]=sum[i-1]+v[i];
		int l=0,r=1e9,res=0;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid)){
				res=mid;
				l=mid+1;
			}else r=mid-1;
		}
		cout<<res<<endl;
	}
}p55;
struct Brute{
	int val[M];
	int fa[M][5];
	int ans;
	void DFS(int x,int f){
		dep[x]=dep[f]+1;
		fa[x][0]=f;
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(y==f)continue;
			val[y]=G[i].co;
			DFS(y,x);
		}
	}
	void Up(int&x,int stp){
		FOR(i,0,4)if(1<<i&stp)x=fa[x][i];
	}
	int LCA(int a,int b){
		if(dep[a]>dep[b])swap(a,b);
		Up(b,dep[b]-dep[a]);
		if(a==b)return a;
		DOR(i,4,0)if(fa[a][i]!=fa[b][i])a=fa[a][i],b=fa[b][i];
		return fa[a][0];
	}
	bool mark[20];
	void dfs(int cnt,int Min){
		if(cnt>m)return;
		if(cnt&&Min<=ans)return;
		if(cnt==m)ans=max(ans,Min);
		FOR(i,1,n){
			FOR(j,i+1,n){
				int a=i,b=j;
				int lca=LCA(a,b);
				bool f=true;
				for(int x=a;x!=lca;x=fa[x][0]){
					if(mark[x]){
						f=false;
						break;
					}
				}
				for(int x=b;x!=lca;x=fa[x][0]){
					if(mark[x]){
						f=false;
						break;
					}
				}
				if(!f)continue;
				int s=0;
				for(int x=a;x!=lca;x=fa[x][0])s+=val[x],mark[x]=1;
				for(int x=b;x!=lca;x=fa[x][0])s+=val[x],mark[x]=1;
				dfs(cnt+1,min(Min,s));
				for(int x=a;x!=lca;x=fa[x][0])mark[x]=0;
				for(int x=b;x!=lca;x=fa[x][0])mark[x]=0;
			}
		}
	}
	void solve(){
		ans=0;
		DFS(1,0);
		FOR(j,1,4)FOR(i,1,n)fa[i][j]=fa[fa[i][j-1]][j-1];
		dfs(0,1e9);
		cout<<ans<<endl;
	}
}Baoli;
struct P75{
	int d[205][205];
	int B[205*205],tot;
	int Find(int x){
		return lower_bound(B+1,B+tot+1,x)-B;
	}
	void solve(){
		/*FOR(i,1,n)FOR(j,1,n)d[i][j]=i==j?0:1e9;
		FOR(i,1,n){
			for(int j=head[i];~j;j=G[j].nxt){
				int y=G[j].to;
				d[i][y]=d[y][i]=G[j].co;
			}
		}
		FOR(k,1,n)FOR(i,1,n)FOR(j,1,n)d[i][j]=min(d[i][j],d[i][k]+d[k][j]);
		FOR(i,1,n)FOR(j,i+1,n)B[++tot]=d[i][j];
		tot=unique(B+1,B+tot+1)-B-1;
		int l=1,r=tot,res=l;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid)){
				res=mid;
				l=mid+1;
			}else r=mid-1;
		}
		cout<<res<<endl;*/
	}
}p75;
bool mmm2;
int main(){
//	debug((&mmm2-&mmm1)/1024/1024);
	memset(head,-1,sizeof(head));
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	bool one=true,chain=true;
	FOR(i,2,n){
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		add_edge(a,b,c);
		add_edge(b,a,c);
		if(a!=1)one=false;
		if(b!=a+1)chain=false;
		v[i]=c;
	}
	if(m==1)p30.solve();
	else if(one)p40.solve();
	else if(chain)p55.solve();
	else if(n<=200)p75.solve();
	return 0;
}
