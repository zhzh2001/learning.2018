#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline void judge(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}
bool f[25005];
int a[105];
int main(){
	judge();
	int T;
	scanf("%d",&T);
	while(T--){
		int n;
		scanf("%d",&n);
		for(int i=1;i<=n;i++){
			scanf("%d",&a[i]);
		}
		sort(a+1,a+n+1);
		memset(f,0,sizeof(f));
		f[0]=1;
		int ans=0;
		for(int i=1;i<=n;i++){
			if(!f[a[i]]){
				ans++;
				for(int j=a[i];j<=a[n];j++)
					f[j]=f[j]||f[j-a[i]];
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
