#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline void judge(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
}
int a[100005],fa[100005];
int st[100005],top;
int main(){
	judge();
	int n;
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	for(int i=1;i<=n;i++){
		while(top&&a[st[top]]>a[i]){
			if(top-1&&a[st[top-1]]>a[i])fa[st[top]]=st[top-1];
			else fa[st[top]]=i;
			top--;
		}
		st[++top]=i;
	}
	while(top){
		fa[st[top]]=st[top-1];
		top--;
	}
	int ans=0;
	for(int i=1;i<=n;i++)ans+=a[i]-a[fa[i]];
//	for(int i=1;i<=n;i++)printf("%d ",fa[i]); puts("");
	printf("%d\n",ans);
	return 0;
}

