#include<cstdio>
#include<cstring>
#include<algorithm>
#include<set>
#include<iostream>
#include<vector>
using namespace std;
inline void judge(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
}
int n,m;
struct edge{
	int s,t,l,nxt;
}e[100005];
int last[50005],e_cnt;
void ins(int a,int b,int c){
	e[e_cnt]=(edge){a,b,c,last[a]};
	last[a]=e_cnt++;
}
int fa[50005];
vector<int> vson[50005];
int f[50005],g[50005];
int X;
int L[50005],R[50005];
bool vis[50005];
void dfs(int x){
	f[x]=0;
	vson[x].clear();
	for(int i=last[x];i!=-1;i=e[i].nxt)if(e[i].t!=fa[x]){
		fa[e[i].t]=x;
		dfs(e[i].t);
		f[x]+=f[e[i].t];
		if(g[e[i].t]+e[i].l>=X)f[x]++;
		else vson[x].push_back(g[e[i].t]+e[i].l);
	}
	g[x]=0;
	sort(vson[x].begin(),vson[x].end());
	int N=vson[x].size();
	for(int i=1;i<=N;i++)L[i]=i-1,R[i]=i+1,vis[i]=0;
	R[0]=1; L[N+1]=N;
	for(int i=1,j=N+1;i<=N;i++)if(!vis[i]){
		if(j==i)j=R[j];
		L[R[i]]=L[i];
		R[L[i]]=R[i];
		while(L[j]>0&&vson[x][L[j]-1]+vson[x][i-1]>=X)j=L[j];
		if(j>N){g[x]=max(g[x],vson[x][i-1]); continue;}
		vis[j]=1;
		L[R[j]]=L[j];
		R[L[j]]=R[j];
		f[x]++;
		j=R[j];
	}
	if(L[N+1]>0)g[x]=max(g[x],vson[x][L[N+1]-1]);
}
bool check(){
	dfs(1);
//	printf("%d\n",X);
//	for(int i=1;i<=n;i++)printf("%d ",f[i]); puts("");
//	for(int i=1;i<=n;i++)printf("%d ",g[i]); puts("");
	return f[1]>=m;
}
int main(){
	judge();
	scanf("%d%d",&n,&m);
	memset(last,-1,sizeof(last));
	for(int i=1;i<n;i++){
		int u,v,l;
		scanf("%d%d%d",&u,&v,&l);
		ins(u,v,l);
		ins(v,u,l);
	}
//	X=30;
//	check();
//	return 0;
	int l=1,r=500000000;
	while(l<r){
		int mid=r-((r-l)>>1);
		X=mid;
		if(check())l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}

