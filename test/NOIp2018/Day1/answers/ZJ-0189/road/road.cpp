#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<string>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=(int)(a);x<=(int)(b);++x)
#define drp(x,a,b) for (re int x=(int)(a);x>=(int)(b);--x)
#define LL long long
#define inf 0x3f3f3f3f
using namespace std;
const int N=200010;
vector<int>num[N];
int ans;
int Log[N],St[20][N],cnt[N],n,a[N];
inline int Query(int x,int y){
	int len=y-x+1;
	int t=Log[len];
	int  res=min(St[t][x],St[t][y-(1<<t)+1]);
	return res;
}
inline void Solve(int l,int r,int now){
	int Mi=Query(l,r);
	int res=(Mi-now);
	ans=ans+res;
	if (l==r) return;
	int L=l,R;
	rep(i,0,cnt[Mi]-1) {
		if (num[Mi][i]<l) continue;
		if (num[Mi][i]==L) {
			L++;
			continue;
		}
		if (num[Mi][i]>r) break;
		R=num[Mi][i]-1;
		if (L<=R)Solve(L,R,now+res);
		L=R+2;
	}
	if (L<=r)Solve(L,r,now+res);
	return;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	rep(i,1,n) {
		scanf("%d",&a[i]),St[0][i]=a[i];
		num[a[i]].push_back(i);
		cnt[a[i]]++;
	}
	Log[0]=-1;
	rep(i,1,n) Log[i]=Log[i/2]+1;
	rep(i,1,Log[n])
		rep(j,1,n)
			St[i][j]=min(St[i-1][j],St[i-1][j+(1<<(i-1))]);
	Solve(1,n,0);
	printf("%lld\n",ans);
	return 0;
}

