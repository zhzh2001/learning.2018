#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<string>
#include<cctype>
#include<ctime>
#include<cstdlib>
#define re register
#define rep(x,a,b) for (re int x=(int)(a);x<=(int)(b);++x)
#define drp(x,a,b) for (re int x=(int)(a);x>=(int)(b);--x)
#define LL long long
#define inf 0x3f3f3f3f
using namespace std;
const int N=10010;
int T,n,a[N],b[N],ans,cnt;
bool dp[N];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	
	while (T--){
		ans=0x3f3f3f3f;
		scanf("%d",&n);
		rep(i,1,n) scanf("%d",&a[i]);
		if (n==2) {
			if ((a[2]%a[1]==0)||(a[1]%a[2]==0)) printf("1\n");
				else printf("2\n");
			return 0;
		}
	//	printf("%d\n",(1<<(n-1))-2);
		rep(i,1,(1<<(n))-2){
			//if (a[2]!=19) printf("%d\n",i);
			memset(dp,0,sizeof (dp));
			rep(j,1,cnt) b[j]=0;
			cnt=0;
			int Ma=0;
			rep(j,1,n) if (i&((1<<(j-1)))){
				b[++cnt]=j,Ma=max(Ma,a[j]);
			}else dp[a[j]]=1;
			if (cnt>=ans) continue;
				rep(k,1,Ma){
					bool flag=0;
					rep(kk,1,n){
						if (i&((1<<(kk-1)))) continue;
						rep(t,1,k/a[kk])
							if (dp[k-a[kk]*t]){
								dp[k]=1;flag=1;break;
							}
						if (flag) break;
					}
				}
			bool g=1;
			rep(j,1,cnt) if (!dp[a[b[j]]]){
				g=0;break;
			}
			//if (g) printf("%d\n",i);
			if (g){
				ans=min(ans,n-cnt);
			//	printf("%d ",ans);
			//	rep(j,1,cnt) printf("%d ",a[b[j]]);
				//printf("\n");
			}
		//	if (i==10) printf("%d %d %d %d %d %d %d\n",dp[19],dp[6],dp[3],dp[10],a[b[1]],a[b[2]],cnt);
		}
		if (ans==0x3f3f3f3f) ans=n;
		printf("%d\n",ans);
	//	printf("\n");
	}
	return 0;
}

