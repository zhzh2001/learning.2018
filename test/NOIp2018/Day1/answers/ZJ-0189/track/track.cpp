#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<string>
#include<cctype>
#include<ctime>
#include<cstdlib>
#define re register
#define rep(x,a,b) for (re int x=(int)(a);x<=(int)(b);++x)
#define drp(x,a,b) for (re int x=(int)(a);x>=(int)(b);--x)
#define LL long long
#define inf 0x3f3f3f3f
using namespace std;
const int N=100010;
int To[N<<1],Nxt[N<<1],Head[N],cnt,val[N],n,m,f[N],ans;
bool vis[N];
inline void add(int u,int v,int w){
	To[++cnt]=v;
	Nxt[cnt]=Head[u];
	Head[u]=cnt;
	val[cnt]=w;
}
inline void dfs(int u){
	for (int i=Head[u];i;i=Nxt[i]){
		int v=To[i],w=val[i];
		if (vis[v]) continue;
		vis[v]=1;
		dfs(v);
		ans=max(ans,f[u]+f[v]+w);
		f[u]=max(f[u],f[v]+w);
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	rep(i,1,n-1){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z);add(y,x,z);
	}
	memset(vis,0,sizeof(vis));
	vis[1]=1;
	dfs(1);
	printf("%d\n",ans);
}

