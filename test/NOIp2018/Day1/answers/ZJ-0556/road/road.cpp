#include<bits/stdc++.h>
using namespace std;
#define N 100003
#define LL long long

int n;
int d[N];
LL ans;

void solve(int l,int r){
	if (l>r) return;
	if (l==r){
		ans+=d[l];
		return;
	}
	int Minn=1e9;
	for (int i=l;i<=r;i++)
	  Minn=min(Minn,d[i]);	
	ans+=Minn;
	int last=l;
	for (int i=l;i<=r;i++){
		d[i]-=Minn;
		if (d[i]==0){
			solve(last,i-1);
			last=i+1;
		}
	}
	solve(last,r);
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
	  scanf("%d",&d[i]);
	solve(1,n);
	printf("%lld\n",ans);
	return 0;	
}
