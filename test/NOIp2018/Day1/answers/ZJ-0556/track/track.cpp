#include<bits/stdc++.h>
using namespace std;
#define N 50003

int n,m,nedge,p,ans;
int head[N],f[N],g[N],tt[N],tt2[N],tt3[N];
struct Edge{
	int to,next,val;
}edge[2*N];
bool vis[N];

inline int read(){
	int X=0,w=0;
	char ch=0;
	while (ch<'0' || ch>'9')
	  w|=ch=='-',ch=getchar();
	while (ch>='0' && ch<='9')
	  X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
	return w ? -X : X;	
}

void addedge(int a,int b,int c){
	edge[nedge].to=b;
	edge[nedge].val=c;
	edge[nedge].next=head[a];
	head[a]=nedge++;	
}

void Find(int u,int fa,int d){
	if (d>ans){
		ans=d;
		p=u;
	}
	for (int i=head[u];i!=-1;i=edge[i].next){
		int v=edge[i].to;
		if (v==fa) continue;
		Find(v,u,d+edge[i].val);	
	}
}

bool cmp(pair<int,int>x,pair<int,int>y){
	return x.second<y.second;	
}

void dfs(int u,int fa,int d,int len){
	f[u]=0;g[u]=d;
	vector< pair<int,int> >vec;
	for (int i=head[u];i!=-1;i=edge[i].next){
		int v=edge[i].to;
		if (v==fa) continue;
		dfs(v,u,edge[i].val,len);
		f[u]+=f[v];
		vec.push_back(make_pair(v,g[v]));
	}
	sort(vec.begin(),vec.end(),cmp);
	for (int i=0;i<vec.size();i++){
		if (vis[vec[i].first]) continue;
		for (int j=i+1;j<vec.size();j++){
			if (vis[vec[j].first]) continue;
			if (vec[i].second+vec[j].second>=len){
				vis[vec[i].first]=1;
				vis[vec[j].first]=1;
				f[u]++;	
				break;
			}
		}
	}
	for (int i=vec.size()-1;i>=0;i--)
	  	if (!vis[vec[i].first]){
	  		g[u]+=vec[i].second;
	  		break;
	  	}
	if (g[u]>=len){
		g[u]=0;
		f[u]++;	
	}
}

bool check(int x){
	memset(vis,0,sizeof(vis));
	dfs(1,0,0,x);
	if (f[1]>=m) return true;
	return false;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	memset(head,-1,sizeof(head));
	bool flag=1,flag2=1;
	for (int i=1;i<n;i++){
		int u=read(),v=read(),w=read();
		addedge(u,v,w);
		addedge(v,u,w);
		tt[min(u,v)]=w;
		tt2[i]=w;
		if (u!=v+1 && u!=v-1) flag=0; 
		if (min(u,v)!=1) flag2=0;
	}
	ans=0;
	if (m==1){	
		Find(1,0,0);
		Find(p,0,0);
		printf("%d\n",ans);
		return 0;	
	}
	if (flag){
		int l=1,r=500000000;
		while (l<=r){
			int mid=(l+r)/2;
			int res=0,tot=0;
			for (int i=1;i<n;i++){
				res+=tt[i];
				if (res>=mid){
					res=0;
					tot++;	
				}
			}
			if (tot>=m){
				l=mid+1;
				ans=mid;	
			}
			else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;	
	}
	int l=1,r=500000000;
	while (l<=r){
		int mid=(l+r)/2;
		if (check(mid)){
			l=mid+1;
			ans=mid;
		}
		else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;	
}
