#include<bits/stdc++.h>
using namespace std;
#define N 103

int T,n,ans;
int a[N],f[300003];

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		for (int i=1;i<=n;i++)
		  scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		ans=n;
		memset(f,0,sizeof(f));
		f[0]=1;
		for (int i=1;i<=n;i++){
			if (f[a[i]]) ans--;
			for (int j=0;j+a[i]<=30000;j++)
			  if (f[j]) f[j+a[i]]=1;
		}
		printf("%d\n",ans);	
	}
	return 0;	
}
