var
n,i,ans:longint;
a,b:array[0..100001]of longint;
procedure dfs(l,r,k:longint);
var
min,i,j:longint;
begin
  if l>r then exit;
  min:=maxlongint;
  for i:=l to r do
   if a[i]<min then begin min:=a[i]; j:=i; end;
  ans:=ans+min-b[k-1];
  b[k]:=min;
  dfs(l,j-1,k+1);
  dfs(j+1,r,k+1);
end;
begin
assign(input,'road.in'); reset(input);
assign(output,'road.out'); rewrite(output);
  fillchar(b,sizeof(b),0);
  readln(n);
  for i:=1 to n do
   read(a[i]);
  ans:=0;
  dfs(1,n,1);
  write(ans);
close(input); close(output);
end.