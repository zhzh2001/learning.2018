#include<bits/stdc++.h>
using namespace std;
const int N=50002;
struct node{
	int to,ne,w;
}e[N<<1];
int x,y,z,cnt,now,i,l,r,mid,ans,tot,h[N],n,m,le[N],ri[N],len,Max;
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
void add(int x,int y,int z){
	e[++tot]=(node){y,h[x],z};
	h[x]=tot;
}
int dfs(int u,int fa){
	vector<int>vec;vec.clear();
	for (int i=h[u],v;i;i=e[i].ne)
		if ((v=e[i].to)!=fa){
			int tmp=dfs(v,u)+e[i].w;
			if (tmp>=now) cnt++;
			else vec.push_back(tmp);
		}
	len=vec.size();
	if (!len) return 0;
	if (len==1) return vec[0];
	sort(vec.begin(),vec.end());
	for (int i=0;i<len;i++){
		int j=i+1;
		for (;j<len;j++)
			if (vec[j]>0 && vec[i]+vec[j]>=now) break;
		if (j==len) continue;
		vec[i]=vec[j]=0;cnt++;
	}
	for (int i=len-1;i>=0;i--)
		if (vec[i]>0) return vec[i];
	return 0;
}
bool chk(int x){
	cnt=0;now=x;
	dfs(1,0);
	return cnt>=m;
}
int dfs1(int u,int fa){
	int mx=0,sec=0;
	for (int i=h[u],v;i;i=e[i].ne)
		if ((v=e[i].to)!=fa){
			int tmp=dfs1(v,u)+e[i].w;
			if (tmp>mx) sec=mx,mx=tmp;
			else if (tmp>sec) sec=tmp;
		}
	if (mx+sec>Max) Max=mx+sec,ans=u;
	return mx;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=rd(),m=rd();
	for (i=1;i<n;i++) x=rd(),y=rd(),z=rd(),add(x,y,z),add(y,x,z);
	if (m!=1){
	l=1;r=5e8;
	while (l<=r){
		mid=l+r>>1;
		if (chk(mid)) ans=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d",ans);
	return 0;
	}else{
	ans=dfs1(1,0);
	ans=dfs1(ans,0);
	printf("%d",Max);
	return 0;
	}
}
