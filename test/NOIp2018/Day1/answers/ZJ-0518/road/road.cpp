#include<bits/stdc++.h>
using namespace std;
const int N=100002;
int n,i,sum,a[N],t;
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=rd();
	for (i=1;i<=n;i++) a[i]=rd();
	sum=a[1];
	for (i=2;i<=n;i++)
		if (a[i]>a[i-1]) t=a[i-1],sum+=a[i]-t;
	printf("%d",sum);
	return 0;
}
