#include<bits/stdc++.h>
using namespace std;
int T,n,i,mx,a[102],j,x,vis[25002],tag,ans;
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=rd();
	for (tag=1;tag<=T;tag++){
		n=rd();
		for (i=1;i<=n;i++) a[i]=rd();
		sort(a+1,a+n+1);
		mx=a[n];ans=0;vis[0]=tag;
		for (i=1;i<=n;i++)
			if (vis[a[i]]!=tag){
				ans++;
				for (j=a[i];j<=mx;j++)
					if (vis[j-a[i]]==tag) vis[j]=tag;
			}
		printf("%d\n",ans);
	}
	return 0;
}
