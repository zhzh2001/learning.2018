#include<bits/stdc++.h>
#define LL long long
#define FOR(x,l,r) for(int x=l,x##_=r;x<=x##_;x++)
#define FR(x,l,r) for(int x=l,x##_=r;x<x##_;x++)
#define DOR(x,l,r) for(int x=r,x##_=l;x>=x##_;x--)
using namespace std;
const int N=100005;
int n,A[N];
LL ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	FOR(i,1,n)scanf("%d",&A[i]);
	int lst=1;
	FOR(i,2,n){
		if(A[i]>A[i-1]){
			ans+=A[lst];
			ans-=A[lst-1];
			lst=i;
		}
		if(i==n){
			ans+=A[lst];
			ans-=A[lst-1];
		}
	}
	printf("%lld\n",ans);
	return 0;
}
