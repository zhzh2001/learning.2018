#include<bits/stdc++.h>
#define LL long long
#define FOR(x,l,r) for(int x=l,x##_=r;x<=x##_;x++)
#define FR(x,l,r) for(int x=l,x##_=r;x<x##_;x++)
#define DOR(x,l,r) for(int x=r,x##_=l;x>=x##_;x--)
using namespace std;
const int N=50005;
bool mem1;
struct Node{
	int x,y,l;
}A[N];
struct node{
	int to,v;
};
vector<node>edge[N];
int n,m;
bool mark[N];
int stk[N],tot,L[N];
struct P0{
	bool check_Link(){
		FR(i,1,n)if(A[i].x!=A[i].y-1)return 0;
		return 1;
	}
	int C[N];
	bool check(int mid){
		int lst=0,cnt=0;
		FR(i,1,n){
			if(C[i]-C[lst]>=mid)cnt++,lst=i;
			if(cnt>=m)return 1;
		}
		return 0;
	}
	void sl_Link(){
		FR(i,1,n)C[A[i].x]=A[i].l;
		FR(i,1,n)C[i]+=C[i-1];
		int L=1,R=1e9,res=0;
		while(L<=R){
			int mid=(L+R)>>1;
			if(check(mid))L=mid+1,res=mid;
			else R=mid-1;
		}printf("%d\n",res);
	}
	int son[N],dis[N];
	void Find(int x,int f){
		son[x]=x,dis[x]=0;
		FR(i,0,edge[x].size()){
			int k=edge[x][i].to,v=edge[x][i].v;
			if(k==f)continue;
			Find(k,x);
			if(dis[k]+v>dis[x])son[x]=son[k],dis[x]=dis[k]+v;
		}
	}
	LL ans;
	void get_ans(int x,int f){
		FR(i,0,edge[x].size()){
			int k=edge[x][i].to,v=edge[x][i].v;
			if(son[k]!=son[x]||k==f)continue;
			get_ans(k,x);
			ans+=v;
		}
	}
	bool check_m_1(){
		return m==1;
	}
	void sl_m_1(){
		Find(1,0);
		int g=son[1];
		Find(g,0);
		ans=0;
		get_ans(g,0);
		printf("%lld\n",ans);
	}
	bool check_a_1(){
		FR(i,1,n)if(A[i].x!=1)return 0;
		return 1;
	}
	int D[N];
	void sl_a_1(){
		FR(i,1,n)C[i]=A[i].l;
		sort(C+1,C+n);
		int tot=0;
		FOR(i,n-m,n-1){
			if((n-m)-(i-(n-m)+1)>0)D[++tot]=C[i]+C[(n-m)-(i-(n-m)+1)];
			else D[++tot]=C[i];
		}
		sort(D+1,D+tot+1);
		printf("%d\n",D[1]);
	}
	void Mark(int x,int f,int V){
//		printf("mark:%d\n",x);
		L[tot]=V;
		stk[++tot]=x;
		mark[x]=1;
		FR(i,0,edge[x].size()){
			int k=edge[x][i].to,v=edge[x][i].v;
			if(son[k]!=son[x]||k==f)continue;
			Mark(k,x,v);
		}
	}
	void dfs(int x,int f){
		dis[x]=0;
		FR(i,0,edge[x].size()){
			int k=edge[x][i].to,v=edge[x][i].v;
			if(k==f||mark[k])continue;
			dfs(k,x);
			if(dis[k]+v>dis[x])dis[x]=dis[k]+v;
		}
	}
	int Add(int x,int mid){
		int l=0,top=0,cnt=0;
		FR(i,0,edge[x].size()){
			int k=edge[x][i].to,v=edge[x][i].v;
			if(mark[k])continue;
			C[++top]=dis[k]+v;
//			printf("%d\n",top);
//			printf("%d\n",C[top]);
		}sort(C+1,C+top+1);
		DOR(i,top,1){
//			printf("%d\n",top);
//			printf("P:%d %d\n",i,l);
			while(C[i]+C[l]<mid&&l<i)printf("%d\n",C[i]+C[l]),l++;
			if(l>=i)break;
			cnt++,l++;
		}return cnt;
	}
	bool check_sl(int mid){
		int lst=0,cnt=0;
//		printf("tot:%d\n",tot);
		FOR(i,1,tot){
//			printf("%d %d %d\n",stk[i],cnt,mid);
			cnt+=Add(stk[i],mid);
//			printf("%d %d %d\n",stk[i],cnt,mid);
			if(L[i]-L[lst]>mid)cnt++,lst=i;
//			printf("%d %d %d\n",stk[i],cnt,mid);
//			if(cnt>=m)return 1;
		}
		printf("%d %d\n",cnt,mid);
//		return 0;
		return cnt>=m;
	}
	void sl(){
		Find(1,0);
		int g=son[1];
//		printf("g:%d\n",g);
		Find(g,0);
//		printf("g:%d\n",son[g]);
		Mark(g,0,0);
		FOR(i,1,tot)dfs(stk[i],0);
		FR(i,1,tot)L[i]+=L[i-1];
		int L=1,R=1e9,res=0;
		while(L<=R){
			int mid=(L+R)>>1;
			if(check_sl(mid))L=mid+1,res=mid;
			else R=mid-1;
		}printf("%d\n",res);
	}
}p0;
bool mem2;
int main(){
//	printf("%lf\n",(&mem2-&mem1)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int x,y,l;
	scanf("%d%d",&n,&m);
	FR(i,1,n){
		scanf("%d%d%d",&x,&y,&l);
		edge[x].push_back((node){y,l});
		edge[y].push_back((node){x,l});
		if(x>y)swap(x,y);
		A[i]=(Node){x,y,l};
	}
	if(p0.check_Link())p0.sl_Link();
	else if(p0.check_m_1())p0.sl_m_1();
	else if(p0.check_a_1())p0.sl_a_1();
	else p0.sl();
	return 0;
}
