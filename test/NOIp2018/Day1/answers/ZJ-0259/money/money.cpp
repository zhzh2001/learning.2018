#include<bits/stdc++.h>
#define LL long long
#define FOR(x,l,r) for(int x=l,x##_=r;x<=x##_;x++)
#define FR(x,l,r) for(int x=l,x##_=r;x<x##_;x++)
#define DOR(x,r,l) for(int x=r,x##_=l;x>=x##_;x--)
using namespace std;
const int N=25005;
int n,A[N];
vector<int>edge[N];
struct P0{
	bool dp[N],mark[N],vis[N],Tak[N];
	int R;
	void Mark(int x){
		int sz=edge[x].size();
		FR(i,1,1<<sz){
			int c=1;
			FR(j,0,sz){
				if((1<<j)&i){
					int k=edge[x][j];
					c*=k;
				}
			}
			mark[c]=1;
		}
	}
	void TK(int x){
		int sz=edge[x].size();
		FR(i,1,1<<sz){
			int c=1;
			FR(j,0,sz){
				if((1<<j)&i){
					int k=edge[x][j];
					c*=k;
				}
			}
			Tak[c]=1;
		}
	}
	void Vis(int x){
		FOR(i,0,R){
			if(vis[i])vis[i+x]=1;
		}
	}
	void sl(){
		if(n==1){
			puts("1");
			return;
		}
		memset(dp,0,sizeof(dp));
		memset(vis,0,sizeof(vis));
		memset(Tak,0,sizeof(Tak));
		memset(mark,0,sizeof(mark));
		sort(A+1,A+n+1);
		R=A[n]*A[n-1]-A[n]-A[n-1];
		dp[0]=1;
		FOR(i,1,n)FOR(j,0,R)if(dp[j])dp[j+A[i]]=1;
		DOR(i,R,1){
			if(!dp[i]){
				if(!mark[i])Mark(i);
			}
			else if(!Tak[i])TK(i);
		}
		int ans=0;
		vis[0]=1;
		FOR(i,1,R){
//			printf("%d:%d %d %d\n",i,mark[i],Tak[i],vis[i]);
			if(!mark[i]&&Tak[i]&&!vis[i]){
//				printf("i:%d\n",i);
				Vis(i);
				ans++;
			}
		}
		printf("%d\n",ans);
	}
}p0;
void init(){
	FOR(i,1,25000)edge[i].clear();
	FOR(i,1,25000){
		int x=i;
		edge[x].push_back(1);
		for(int j=2;j*j<=x;j++){
			if(x%j==0){
				while(x%j==0)x/=j,edge[i].push_back(j);
			}
		}
		if(x>1)edge[i].push_back(x);
	}
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	init();
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		FOR(i,1,n)scanf("%d",&A[i]);
		p0.sl();
	}
	return 0;
}
