#include<bits/stdc++.h>
#define rep(i,a,b)	for(int i=(a);i<=(b);++i)
#define rev(i,a,b)	for(int i=(a);i>=(b);--i)
using namespace std;
typedef long long LL;
void read(int &a){
	a=0;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')	ch=getchar();
	int f=1;if (ch=='-')	f=-1,ch=getchar();
	while (ch>='0'&&ch<='9')	a=a*10+ch-48,ch=getchar();
	a=a*f;
}
void read(LL &a){
	a=0;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')	ch=getchar();
	int f=1;if (ch=='-')	f=-1,ch=getchar();
	while (ch>='0'&&ch<='9')	a=a*10+ch-48,ch=getchar();
	a=a*f;
}
void write(int a){
	if (!a)	return;
	if (a<0)	putchar('-'),write(-a);
	else write(a/10),putchar(a%10+48);
}
void write(LL a){
	if (!a)	return;
	if (a<0)	putchar('-'),write(-a);
	else write(a/10),putchar(a%10+48);
}
int n,x,y,ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);x=0;
	rep(i,1,n)	read(y),ans+=max(0,y-x),x=y;
	write(ans);	
	return 0;
}

