#include<bits/stdc++.h>
#define rep(i,a,b)	for(int i=(a);i<=(b);++i)
#define rev(i,a,b)	for(int i=(a);i>=(b);--i)
using namespace std;
typedef long long LL;
void read(int &a){
	a=0;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')	ch=getchar();
	int f=1;if (ch=='-')	f=-1,ch=getchar();
	while (ch>='0'&&ch<='9')	a=a*10+ch-48,ch=getchar();
	a=a*f;
}
void read(LL &a){
	a=0;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')	ch=getchar();
	int f=1;if (ch=='-')	f=-1,ch=getchar();
	while (ch>='0'&&ch<='9')	a=a*10+ch-48,ch=getchar();
	a=a*f;
}
void write(int a){
	if (!a)	return;
	if (a<0)	putchar('-'),write(-a);
	else write(a/10),putchar(a%10+48);
}
void write(LL a){
	if (!a)	return;
	if (a<0)	putchar('-'),write(-a);
	else write(a/10),putchar(a%10+48);
}
const int N=50005;
struct str1{
	int nxt,y,w;
}g[N<<1];
int	n,m,G,ans; 
int a[N],b[N],c[N],d[N],h[N],f[N];
void addedge(int x,int y,int z){
	g[++G].nxt=h[x],g[G].y=y,g[G].w=z,h[x]=G;
}
void dfs(int x,int fa){
	int m1,m2;m1=m2=0;
	for(int i=h[x];i;i=g[i].nxt){
		if (g[i].y==fa)	continue;
		dfs(g[i].y,x);
		if (m2<f[g[i].y]+g[i].w)	m1=m2,m2=f[g[i].y]+g[i].w;
		else m1=max(m1,f[g[i].y]+g[i].w);
		f[x]=max(f[x],f[g[i].y]+g[i].w);
	}
	ans=max(ans,m1+m2);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n),read(m);
//	m=1
	if (m==1){
		G=0;int x,y,z;
		rep(i,1,n-1)
			read(x),read(y),read(z),addedge(x,y,z),addedge(y,x,z);
		memset(f,0,sizeof(f));
		ans=0;dfs(1,0);
		write(ans),puts("");	
		return 0;
	}
	rep(i,1,n-1)	read(a[i]),read(b[i]),read(d[i]);
	bool flag=false;
//	b[i]=a[i]+1
	flag=true;rep(i,1,n-1)	if (b[i]!=a[i]+1)	flag=false;
	if (flag){
		int num=0;
		rep(i,1,n-1)	c[a[i]]=d[i],num+=d[i];
		rep(i,1,n-1)	d[i]=c[i];	
		int l=1,r=num/m,k=0,mid;
		while (l<=r){
			mid=(l+r)>>1,num=0,k=0;
			rep(i,1,n-1)
				if (num+d[i]>=mid)	k++,num=0;
				else num+=d[i];
			if (k>=m)	ans=mid,l=mid+1;
			else r=mid-1;
		}
		write(ans),puts("");
		return 0;
	}
//	a[i]=1
	flag=true;rep(i,1,n-1)	if (a[i]!=1)	flag=false;
	if (flag){
		sort(d+1,d+n);
		write(d[n-m]),puts("");
		return 0;
	}
	return 0;
}	
