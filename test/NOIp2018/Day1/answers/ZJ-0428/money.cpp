#include<bits/stdc++.h>
#define rep(i,a,b)	for(int i=(a);i<=(b);++i)
#define rev(i,a,b)	for(int i=(a);i>=(b);--i)
using namespace std;
typedef long long LL;
void read(int &a){
	a=0;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')	ch=getchar();
	int f=1;if (ch=='-')	f=-1,ch=getchar();
	while (ch>='0'&&ch<='9')	a=a*10+ch-48,ch=getchar();
	a=a*f;
}
void read(LL &a){
	a=0;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')	ch=getchar();
	int f=1;if (ch=='-')	f=-1,ch=getchar();
	while (ch>='0'&&ch<='9')	a=a*10+ch-48,ch=getchar();
	a=a*f;
}
void write(int a){
	if (!a)	return;
	if (a<0)	putchar('-'),write(-a);
	else write(a/10),putchar(a%10+48);
}
void write(LL a){
	if (!a)	return;
	if (a<0)	putchar('-'),write(-a);
	else write(a/10),putchar(a%10+48);
}
int n,m;
bool f[25005];
int a[105];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T,ans;read(T);
	while (T--){
		read(n);m=0;
		rep(i,1,n)	read(a[i]),m=max(m,a[i]);
		sort(a+1,a+n+1);
		memset(f,false,sizeof(f));	f[0]=true,ans=0;
		rep(i,1,n){
			ans+=!f[a[i]];
			rep(j,a[i],m)	f[j]=f[j]||f[j-a[i]];
		}
		write(ans),puts("");
	}
	return 0;
}
