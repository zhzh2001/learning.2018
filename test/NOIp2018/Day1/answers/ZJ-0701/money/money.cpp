#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iostream>
using namespace std;
int isd(char ch){
	return '0'<=ch&&ch<='9';
}
int read(){
	int x=0;
	char ch=getchar();
	while (!isd(ch))
		ch=getchar();
	while (isd(ch))
		x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return x;
}
const int N=105,V=25005;
int T,n;
int a[N];
int f[V];
void solve(){
	int ans=0;
	memset(f,0,sizeof f);
	n=read();
	for (int i=1;i<=n;i++)
		a[i]=read();
	sort(a+1,a+n+1);
	f[0]=1;
	for (int i=1;i<=n;i++){
		if (f[a[i]])
			continue;
		ans++;
		for (int j=0;j+a[i]<V;j++)
			f[j+a[i]]|=f[j];
	}
	printf("%d\n",ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while (T--)
		solve();
	return 0;
}
