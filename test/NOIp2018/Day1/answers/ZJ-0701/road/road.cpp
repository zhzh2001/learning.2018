#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iostream>
using namespace std;
int isd(char ch){
	return '0'<=ch&&ch<='9';
}
int read(){
	int x=0;
	char ch=getchar();
	while (!isd(ch))
		ch=getchar();
	while (isd(ch))
		x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return x;
}
const int N=100005;
int n;
int a[N];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++)
		a[i]=read();
	for (int i=n;i>=1;i--)
		a[i]-=a[i-1];
	int T0=0,T1=0;
	for (int i=1;i<=n;i++)
		if (a[i]>0)
			T0+=a[i];
		else
			T1-=a[i];
	printf("%d",T0);
	return 0;
}
