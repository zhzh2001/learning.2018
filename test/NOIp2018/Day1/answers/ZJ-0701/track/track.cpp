#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iostream>
using namespace std;
int isd(char ch){
	return '0'<=ch&&ch<='9';
}
int read(){
	int x=0;
	char ch=getchar();
	while (!isd(ch))
		ch=getchar();
	while (isd(ch))
		x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return x;
}
const int N=50005;
struct Graph{
	static const int M=N*2;
	int cnt,y[M],nxt[M],z[M],fst[N];
	void clear(){
		cnt=1;
		memset(fst,0,sizeof fst);
	}
	void add(int a,int b,int c){
		y[++cnt]=b,z[cnt]=c;
		nxt[cnt]=fst[a],fst[a]=cnt;
	}
}g;
int n,m;
int len,ans;
int md[N];
int v[N],vcnt,s[N],scnt,t[N],tcnt;
void dfs(int x,int pre){
	for (int i=g.fst[x];i;i=g.nxt[i])
		if (g.y[i]!=pre)
			dfs(g.y[i],x);
	vcnt=md[x]=0;
	for (int i=g.fst[x];i;i=g.nxt[i])
		if (g.y[i]!=pre)
			v[++vcnt]=md[g.y[i]]+g.z[i];
//	printf("dfs(%d,%d)\n",x,pre);
//	for (int i=1;i<=vcnt;i++)
//		printf("%d ",v[i]); puts("");
	sort(v+1,v+vcnt+1);
	while (vcnt>0&&v[vcnt]>=len)
		vcnt--,ans++;
	scnt=tcnt=0;
	for (int i=1;i<=vcnt;i++){
		while (vcnt>0&&i<vcnt&&v[i]+v[vcnt]>=len)
			s[++scnt]=v[vcnt--];
		if (scnt>0&&v[i]+s[scnt]>=len)
			ans++,scnt--;
		else
			t[++tcnt]=v[i];
	}
	sort(t+1,t+tcnt+1);
	while (scnt>=2){
		ans++;
		scnt-=2;
	}
	if (scnt>0)
		md[x]=s[1];
	else if (tcnt>0)
		md[x]=t[tcnt];
}
bool check(int k){
	len=k,ans=0;
	dfs(1,0);
//	cout << ans << " : ";
//	for (int i=1;i<=n;i++)
//		cout << md[i] << " "; puts("");
	return ans>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	g.clear();
	for (int i=1;i<n;i++){
		int a=read(),b=read(),c=read();
		g.add(a,b,c);
		g.add(b,a,c);
	}
	int L=1,R=50000*10000,ans=1;
	while (L<=R){
		int mid=(L+R)>>1;
		if (check(mid))
			L=mid+1,ans=mid;
		else
			R=mid-1;
	}
	printf("%d",ans);
	return 0;
}
