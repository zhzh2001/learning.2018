#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
using namespace std;
int T,n,a[1005],f[10005],ans,mx;
int read(){
	int x=0,f=1; char ch=getchar();
	while (ch>'9'||ch<'0'){if (ch=='-') ch=getchar();}
	while (ch<='9'&&ch>='0'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
bool cmp(int x,int y){
	return x<y;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while (T--){
		n=read();
		for (int i=1;i<=n;i++){
			a[i]=read();
			mx=max(mx,a[i]);
		}ans=0;
		for (int i=1;i<=mx;i++) f[i]=0;
		sort(a+1,a+n+1,cmp);
		f[0]=1;
		for (int i=1;i<=n;i++){
			if (f[a[i]]==0) ans++; 
			for (int j=a[i];j<=mx;j++){
				if (f[j-a[i]]) f[j]=1;
			}
		}printf("%d\n",ans);
	}return 0;
}
