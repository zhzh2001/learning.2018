#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
using namespace std;
int st,To[100005],u,v,w,F,S[100005],Next[100005],Head[100005],tot,n,m,mid;
int f[100005],g[100005],ans,Val[100005],s[100005],vis[100005];
int read(){
	int x=0,f=1; char ch=getchar();
	while (ch>'9'||ch<'0'){if (ch=='-') ch=getchar();}
	while (ch<='9'&&ch>='0'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void add(int u,int v,int w){
	To[++tot]=v; Val[tot]=w;
	Next[tot]=Head[u];
	Head[u]=tot;
}
void dfs(int u,int fa){
	for (int i=Head[u];i;i=Next[i]){
		int v=To[i];
		if (v==fa) continue;
		dfs(v,u);
		f[u]+=f[v];
	}int cnt=0;
	for (int i=Head[u];i;i=Next[i]){
		int v=To[i];
		if (v==fa) continue;
		if (g[v]+Val[i]>=mid) f[u]++;
		else{
			s[++cnt]=g[v]+Val[i];
		}
	}sort(s+1,s+cnt+1);
	int k=cnt,top=0;
	for (int i=2;i<=cnt;i++)
	if (s[i-1]+s[i]>=mid){
		k=i-1; break;
	}
	for (int i=1;i<=cnt;i++) vis[i]=0;
	for (int i=k+1;i<=cnt;i++){
		F=0; 
		while (s[i]+s[k-1]>=mid&&k>0){
			S[++top]=k; k--;
		}
		if (s[k]+s[i]<mid){
			if (top>0){ 
				vis[S[top]]=1; vis[i]=1;
				top--; f[u]++;
			}else{
				S[++top]=i;
			}
		}else{
			vis[k]=1; vis[i]=1; f[u]++;
			k--;
		}
	}
	for (int i=1;i<=cnt;i++){
		if (!vis[i]) g[u]=max(g[u],s[i]);
	}
	return;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(); m=read();
	for (int i=1;i<n;i++){
		u=read(); v=read(); w=read();
		add(u,v,w); add(v,u,w);
	}
	st=1;
	int l=1,r=1<<30;
	while (l<=r){
		mid=(l+r)>>1;
		for (int i=1;i<=n;i++){
			f[i]=0;
			g[i]=0;
		}
		dfs(st,0);
		if (f[st]>=m){
			l=mid+1;
			ans=mid;
		}else{
			r=mid-1;
		}
	}
	printf("%d",ans);
	return 0;
}
