#include <bits/stdc++.h>
#define int long long
using namespace std;
int n,a[120000],ans;
int read() {
	register int x=0,f=1;
	register char ch=getchar(); 
	while(!(ch>='0' && ch<='9')) {
		if(ch=='-') f=-1;	
		ch=getchar();
	}
	while(ch>='0' && ch<='9') {
		x=x*10+ch-'0';
		ch=getchar();	
	}
	return x*f;
}
void write(int x) {
	if(x>=10) write(x/10);
	register char ch=x%10+'0';
	putchar(ch);	
}
void subtask1() {
	while(1) {
		register bool flag=true;
		register int mn=2147483647,mnat=0;
		for(register int i=1; i<=n; i++) {
			if(a[i]!=0) flag=false;
			if(a[i] && a[i]<mn) {
				mn=a[i];
				mnat=i;
			}
		}
		if(flag) break;
		register int l=mnat,r=mnat;
		while(a[l-1] && l-1>=1) l--;
		while(a[r+1] && r+1<=n) r++;
		ans+=mn;
		for(register int i=l; i<=r; i++) a[i]-=mn;
	}
	write(ans);
}
signed main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	cin>>n;
	int xiao=2147483647;
	for(register int i=1; i<=n; i++) {
		a[i]=read();
		xiao=min(xiao,a[i]);
	}
	if(n<=1000) subtask1();
	else {
		int now=0,ans=0,mn=2147483647;
		for(int i=1; i<=n; i++) {
			if(a[i]==0) {
				now+=(mn-xiao);
				ans+=now;
				now=0;
				mn=2147483647;
			} else {
				mn=min(mn,a[i]);
				if(i==1 || a[i-1]==xiao) continue;
				else now+=abs(a[i]-a[i-1]);
			}
//		cout<<"ans & now"<<ans<<" "<<now<<'\n';
		}
		now+=a[n]-mn;
		now+=(mn-xiao);
		now+=xiao;
		ans+=now;
		cout<<ans;
	}
	fclose(stdin);
	fclose(stdout);
}
