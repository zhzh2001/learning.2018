#include <bits/stdc++.h>
#define int long long
using namespace std;
int t,n,a[120000],mp[26000];
set <int> q2;
bool vis[260];
int read() {
	register int x=0,f=1;
	register char ch=getchar();
	while(!(ch>='0' && ch<='9')) {
		if(ch=='-') f=-1;
		ch=getchar();
	}
	while(ch>='0' && ch<='9') {
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
void write(int x) {
	if(x>=10) write(x/10);
	register char ch=x%10+'0';
	putchar(ch);
}
signed main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	cin>>t;
	while(t--) {
		n=read();
		for(register int i=1; i<=n; i++) a[i]=read();
		sort(a+1,a+n+1);
		int mx=0;
		q2.clear();
		memset(vis,0,sizeof vis);
		memset(mp,0,sizeof mp);
		for(register int i=1; i<=n; i++) {
			q2.insert(a[i]);
			mp[a[i]]=i;
			mx=max(mx,a[i]);
		}
		q2.insert(a[1]);
		while(1) {
			for(register set <int> :: iterator aa=q2.begin(); aa!=q2.end(); aa++)
				for(register int i=1; i<=n; i++) {
					int cc=(*aa)+a[i];
//						if(*aa>mx) goto L1;
//						cout<<cc<<'\n';
					if(*aa==0 || a[i]==0) continue;
					if(cc<=mx && mp[cc]) {
						vis[mp[cc]]=true;
					}
					if(cc>mx*2) goto L1;
					q2.insert(cc);
				}
		}
L1:
		;
		register int ans=0;
		for(register int i=1; i<=n; i++) if(!vis[i]) ans++;
		write(ans);
		puts("");
	}
	fclose(stdin);
	fclose(stdout);
}
