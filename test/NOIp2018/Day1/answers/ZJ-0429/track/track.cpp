#include <bits/stdc++.h>
#define int long long
using namespace std;
int n,m,head[120000],cnt,a[120000],ans;
struct node {int next,to,w;} sxd[240000];
int read() {
	register int x=0,f=1;
	register char ch=getchar(); 
	while(!(ch>='0' && ch<='9')) {
		if(ch=='-') f=-1;	
		ch=getchar();
	}
	while(ch>='0' && ch<='9') {
		x=x*10+ch-'0';
		ch=getchar();	
	}
	return x*f;
}
void write(int x) {
	if(x>=10) write(x/10);
	register char ch=x%10+'0';
	putchar(ch);	
}
bool all_one=true,lian=true;
void add(int u,int v,int w) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	sxd[cnt].w=w;
	head[u]=cnt;
}
bool cmp(int a,int b) {return a>b;}
int f[120000][2];
/*
0:最长的单条链 
1：最长的最大次大链和 
*/
void dfs(int at,int fa) {
	int mx=0,sec=0;
	for(int i=head[at];i;i=sxd[i].next) {
		if(sxd[i].to==fa) continue;	
		dfs(sxd[i].to,at);
		if(f[sxd[i].to][0]+sxd[i].w>mx) {
			sec=mx;
			mx=f[sxd[i].to][0]+sxd[i].w;	
		} else if(f[sxd[i].to][0]+sxd[i].w>sec) {
			sec=f[sxd[i].to][0]+sxd[i].w;	
		}
	}
	f[at][0]=mx;
	f[at][1]=mx+sec;
}
void subtask1() {
	dfs(1,0);
	int mx=0;
	for(int i=1;i<=n;i++) mx=max(mx,max(f[i][0],f[i][1]));
	cout<<mx<<'\n';
	return;		
}
signed main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<n;i++) {
		int u=read(),v=read(),w=read();
		add(u,v,w);
		add(v,u,w);
		a[i]=w;
		if(u!=1) all_one=false;
		if(v!=u+1) lian=false;
	}
	if(all_one) {
		sort(a+1,a+n,cmp);
		if(2*m<=n-1) {
			int mn=2147483647;
			for(int l=1,r=2*m;l<r;l++,r--) {
				mn=min(mn,a[l]+a[r]);	
			}
			cout<<mn;
			return 0;
		} else {
			int dan=2*m-n+1;
			int mn=a[dan];
			for(int l=dan+1,r=n-1;l<r;l++,r--) {
				mn=min(mn,a[l]+a[r]);
			}		
			cout<<mn;
			return 0;
		}	
	}
	if(m==1) {
		subtask1();	
	}
	cout<<a[1]<<'\n';
	fclose(stdin);
	fclose(stdout);
}
