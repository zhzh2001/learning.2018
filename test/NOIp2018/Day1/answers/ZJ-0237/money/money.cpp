#include <bits/stdc++.h>

using namespace std;

bool bmm1;

const int SZN = 100   + 5;
const int SZA = 25000 + 5;

int n;
int li[SZN];

struct P80 {
	int dp[SZA], mk[SZN];

	int solve(void) {
		if (li[1] == 1) return 1;
		memset(mk, 0, sizeof mk);
		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= li[i]; ++j) dp[j] = 0;
			dp[0] = 1;
			for (int j = 1; j < i; ++j) {
				if (mk[j]) continue;
				for (int k = li[j]; k <= li[i]; ++k)
					dp[k] = (dp[k] < dp[k - li[j]] ? dp[k - li[j]] : dp[k]);
			}
			if (dp[li[i]]) mk[i] = 1;
		}
		int cnt = 0;
		for (int i = 1; i <= n; ++i) if (!mk[i]) ++cnt;
		return cnt;
	}
} p80;

/*
 * Todo:
 *   * [x] 文件 IO
 *   * [x] 内存
 *   * [x] 初始化
 *   * [x] 下标未越界
*/

bool bmm2;

int main(void) {
//	printf("%f\n", (&bmm2 - &bmm1) / 1024.0 / 1024.0);
	freopen("money.in",  "r", stdin);
	freopen("money.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T--) {
		scanf("%d", &n);
		for (int i = 1; i <= n; ++i) scanf("%d", &li[i]);
		sort(li + 1, li + n + 1);
		n = unique(li + 1, li + n + 1) - li - 1;
//		printf("n = %d\n", n);
		printf("%d\n", p80.solve());
	}
	return 0;
}

