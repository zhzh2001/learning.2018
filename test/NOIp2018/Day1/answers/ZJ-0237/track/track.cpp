#include <bits/stdc++.h>

using namespace std;

bool bmm1;

const int SIZE = 50000 + 5;
const int INF  = 1 << 29;

struct Edge {
	int v, w, head;
};

int  ecnt;
int  head[SIZE];
Edge edge[SIZE << 1];

void addEdge(int u, int v, int w) {
	edge[++ecnt] = (Edge) {v, w, head[u]};
	head[u]      = ecnt;
	return;
}

void addEdges(int u, int v, int w) {
	addEdge(u, v, w);
	addEdge(v, u, w);
	return;
}

int n, m;

struct P_MEq1 {
	int maxidep, dtt;

	void dfs1(int u, int fa, int dep) {
		if (dep > maxidep) maxidep = dep, dtt = u;
		for (int i = head[u]; ~i; i = edge[i].head) {
			int v = edge[i].v, w = edge[i].w;
			if (v == fa) continue;
			dfs1(v, u, dep + w);
		}
		return;
	}

	int solve(void) {
		maxidep = - 1, dfs1(1, 0, 0);
		maxidep = - 1, dfs1(dtt, 0, 0);
		return maxidep;
	}
} pMEq1;

int li[SIZE];

struct P_AiEq1 {
	int solve(void) {
		sort(li + 1, li + n);
		int mininum = -1;
		for (int i = 1; i <= m; ++i) {
			int xsum = li[i] + (n - i > m ? li[n - i] : 0);
			if (mininum == -1 || mininum > xsum) mininum = xsum;
		}
		return mininum;
	}
} pAiEq1;

struct P_BiEqAiP1 {
	int check(int num) {
		int cnt = 0;
		for (int i = 1; i < n && cnt < m; ) {
			int sum = 0;
			for (; i < n && sum < num; ++i) sum += li[i];
//			printf("cnt = %d, sum = %d, i = %d, num = %d\n", cnt, sum, i, num);
			if (sum < num) return 0;
			++cnt;
		}
		return cnt == m;
	}

	int solve(void) {
		int le = 0, ri = 0, ans = 0;
		for (int i = 1; i < n; ++i) ri += li[i];
		while (le <= ri) {
			int mid = (le + ri) >> 1;
//			printf("le = %d, ri = %d, mid = %d\n", le, ri, mid);
			if (check(mid)) ans = mid, le = mid + 1;
			else ri = mid - 1;
		}
		return ans;
	}
} pBiEqAiP1;

int getint(void) {
	int x = 0, ch = getchar();
	for (; ch <  '0' || ch >  '9'; ch = getchar());
	for (; ch >= '0' && ch <= '9'; x  = (x << 1) + (x << 3) + ch - '0', ch = getchar());
	return x;
}

/*
 * Todo:
 *   * [x] 文件 IO
 *   * [x] 内存
 *   * [x] 初始化
 *   * [x] 下标未越界
*/

bool bmm2;

int main(void) {
//	printf("%f\n", (&bmm2 - &bmm1) / 1024.0 / 1024.0);
	freopen("track.in",  "r", stdin);
	freopen("track.out", "w", stdout);
	memset(head, -1, sizeof head);
	n = getint(), m = getint();
	int isAiEq1 = 1, isBiEqAiP1 = 1;
	for (int i = 1; i < n; ++i) {
		int ui = getint(), vi = getint(), wi = getint();
		addEdges(ui, vi, wi), li[i] = wi;
		if (ui != 1 && vi != 1) isAiEq1 = 0;
		if (vi != ui + 1 && ui != vi + 1) isBiEqAiP1 = 0;
	}
//	printf("isAiEq1 = %d, isBiEqAiP1 = %d\n", isAiEq1, isBiEqAiP1);
	if (m == 1) {
		printf("%d\n", pMEq1.solve());
		return 0;
	}
	if (isAiEq1) {
		printf("%d\n", pAiEq1.solve());
		return 0;
	}
	if (isBiEqAiP1) {
		printf("%d\n", pBiEqAiP1.solve());
		return 0;
	}
	puts("26282");
	return 0;
}

