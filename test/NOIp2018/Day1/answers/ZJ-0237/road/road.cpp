#include <bits/stdc++.h>

using namespace std;

bool bmm1;

const int SIZE = 100000 + 5;

int n;
int li[SIZE];

struct P100 {
	struct Segment {
		int le, ri, x;

		bool operator < (const Segment &sg) const {
			return ri < sg.ri;
		}
	};

	struct Node {
		int ind, num;

		bool operator < (const Node &nd) const {
			return num < nd.num;
		}
	};

	Node nli[SIZE];
	set<Segment> st;

	int solve(void) {
		int res = 0;
		st.insert((Segment) {1, n, 0});
		for (int i = 1; i <= n; ++i) nli[i] = (Node) {i, li[i]};
		sort(nli + 1, nli + n + 1);
		for (int i = 1; i <= n; ++i) {
			Node nd = nli[i];
			set<Segment>::iterator it = st.lower_bound((Segment) {0, nd.ind, 0});
			Segment sg = *it;
			st.erase(it);
			res += nd.num - sg.x;
//			printf("i = %d, res = %d, nd = {%d, %d}, sg = {%d, %d, %d}\n", i, res, nd.ind, nd.num, sg.le, sg.ri, sg.x);
			if (nd.ind > sg.le) st.insert((Segment) {sg.le, nd.ind - 1, nd.num});
			if (nd.ind < sg.ri) st.insert((Segment) {nd.ind + 1, sg.ri, nd.num});
		}
		return res;
	}
} p100;

int getint(void) {
	int x = 0, ch = getchar();
	for (; ch <  '0' || ch >  '9'; ch = getchar());
	for (; ch >= '0' && ch <= '9'; x  = (x << 1) + (x << 3) + ch - '0', ch = getchar());
	return x;
}

/*
 * Todo:
 *   * [x] 文件 IO
 *   * [x] 内存
 *   * [x] 初始化
 *   * [x] 下标未越界
*/

bool bmm2;

int main(void) {
//	printf("%f\n", (&bmm2 - &bmm1) / 1024.0 / 1024.0);
	freopen("road.in",  "r", stdin);
	freopen("road.out", "w", stdout);
	n = getint();
	for (int i = 1; i <= n; ++i) li[i] = getint();
	printf("%d\n", p100.solve());
	return 0;
}

