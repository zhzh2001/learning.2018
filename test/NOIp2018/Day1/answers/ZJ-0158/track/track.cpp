#include <bits/stdc++.h>
using namespace std;
int n,m,ans,u[50010],v[50010],w[100010];
vector<pair<int,int> > g[50010];
int check1(){
	for(int i=2;i<n;i++)
		if(g[i].size()!=2)
			return 0;
	return 1;
}
int checkI(int x){
	int ttt=0;
	for(int i=1,j;i<n;i++){
		int tmp=0;
		for(j=i;j<n;j++){
			tmp+=w[j];
			if(tmp>=x){
				i=j;
				break;
			}
		}
		i=j;
		ttt++;
	}
	return ttt>=m?1:0;
}
int checkII(int x){
	int tmp=0;
	for(int i=1;i<n;i++){
		if(w[i]>=x) tmp++;
		else if(w[i]+w[i+1]>=x) tmp++,i++;
	}
	return tmp>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&u[i],&v[i],&w[i]);
		g[u[i]].push_back(make_pair(v[i],w[i]));
		g[v[i]].push_back(make_pair(u[i],w[i]));
	}
	if(check1()){
		int l=1,r=1000000000,mid;
		while(l<=r){
			mid=(l+r)/2;
			if(checkI(mid))
				ans=mid,l=mid+1;
			else
				r=mid-1;
		}
	}
	if(g[1].size()>=n-1){
		int l=1,r=1000000000,mid;
		while(l<=r){
			mid=(l+r)/2;
			if(checkII(mid))
				ans=mid,l=mid+1;
			else
				r=mid-1;
		}
	}
	cout<<ans;
	return 0;
}
