#include <bits/stdc++.h>
using namespace std;
int T,n,a[110],dp[2510];
int cmp(int a,int b){
	return a>b;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	cin>>T;
	while(T--){
		scanf("%d",&n);
		int ans=n;
		for(int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+n+1,cmp);
		for(int i=1;i<=n;i++){
			memset(dp,0,sizeof(dp));
			dp[0]=1;
			for(int j=1;j<=a[i];j++)
				for(int k=1;k<=n;k++)
					if(k!=i&&a[k]<=j&&a[k]!=-1){
						dp[j]=max(dp[j],dp[j-a[k]]);
					}
			if(dp[a[i]])
				a[i]=-1,ans--;
		}
		printf("%d\n",ans);
	}
	return 0;
}
