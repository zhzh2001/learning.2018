var i,j,k,m,n,a,b,l,max:longint;
    queue:array[0..100000] of longint;
    gt,head,next,v:array[0..100000] of longint;
    dis,g:array[0..50000] of longint;
    f:array[0..50000] of boolean;

procedure qsort(l,r:longint);
  var mid,x,y,t:longint;
  begin
    x:=l;y:=r;mid:=dis[(l+r) div 2];
    repeat
      while dis[x]>mid do inc(x);
      while dis[y]<mid do dec(y);
      if x<=y then
        begin
          t:=dis[x];dis[x]:=dis[y];dis[y]:=t;
          inc(x);dec(y);
        end;
    until x>y;
    if x<r then qsort(x,r);
    if l<y then qsort(l,y);
  end;
procedure spfa(x:longint);
  var l,r,i,t:longint;
  begin
    l:=0;r:=1;queue[r]:=x;f[x]:=true;dis[x]:=0;
    while l<r do
      begin
        inc(l);
        t:=queue[l];
        f[t]:=false;
        i:=head[t];
        while i<>0 do
          begin
            if v[i]+dis[t]<dis[gt[i]] then
              begin
                dis[gt[i]]:=v[i]+dis[t];
                if f[gt[i]]=false then
                  begin
                    inc(r);
                    queue[r]:=gt[i];
                    f[gt[i]]:=true;
                  end;
              end;
            i:=next[i];
          end;
      end;
  end;

begin
  assign(input,'track.in');reset(input);
  assign(output,'track.out');rewrite(output);
  readln(n,m);
  for i:=1 to n-1 do
    begin
      readln(a,b,l);
      gt[i*2]:=b;
      next[i*2]:=head[a];
      head[a]:=i*2;
      v[i*2]:=l;
      gt[i*2-1]:=a;
      next[i*2-1]:=head[b];
      head[b]:=i*2-1;
      v[i*2-1]:=l;
    end;
  for i:=1 to n do dis[i]:=100000000;
  fillchar(f,sizeof(f),false);
  spfa(1);
  for i:=1 to n do
    if dis[i]>max then
      begin
        max:=dis[i];
        l:=i;
      end;
  for i:=1 to n do dis[i]:=100000000;
  fillchar(f,sizeof(f),false);
  spfa(l);
  qsort(1,n);
  write(dis[m]);
  close(input);close(output);
end.
