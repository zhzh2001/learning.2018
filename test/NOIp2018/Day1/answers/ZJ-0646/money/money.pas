var i,j,m,n,t:longint;
    d:array[0..5000] of longint;
    f:array[0..1005] of boolean;
procedure qsort(l,r:longint);
  var mid,x,y,t:longint;
  begin
    x:=l;y:=r;mid:=d[(l+r) div 2];
    repeat
      while d[x]<mid do inc(x);
      while d[y]>mid do dec(y);
      if x<=y then
        begin
          t:=d[x];d[x]:=d[y];d[y]:=t;
          inc(x);dec(y);
        end;
    until x>y;
    if x<r then qsort(x,r);
    if l<y then qsort(l,y);
  end;

procedure check;
  var i,j,ans:longint;
  begin
    ans:=0;
    for i:=1 to n do
      begin
        if f[d[i]]=true then
          begin
            inc(ans);
            f[d[i]]:=false;
            for j:=1 to 1000-d[i] do
              if f[j]=false then f[j+d[i]]:=false;
          end;
      end;
    writeln(ans);
  end;
begin
  assign(input,'money.in');reset(input);
  assign(output,'money.out');rewrite(output);
  readln(t);
  for j:=1 to t do
    begin
      readln(n);
      fillchar(f,sizeof(f),true);
      fillchar(d,sizeof(d),0);
      for i:=1 to n do read(d[i]);
      qsort(1,n);
      if d[1]=1 then
       begin
         writeln(1);
         continue;
       end;
      check;
    end;
  close(input);close(output);
end.
