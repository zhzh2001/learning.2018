#include<cstdio>
#include<cctype>
#include<cmath>
#include<iostream>
#include<algorithm>
#define LL long long
#define N (100005)
using namespace std;
template <typename T> inline void read(T &t){
	t=0;
	char p=getchar(); bool f=0;
	while (!isdigit(p)){
		if (p=='-') f=1;
		p=getchar();
	}
	do{
		t=t*10+p-'0';
		p=getchar();
	}while(isdigit(p));
	if (f) t=-t;
}
int n,minn,a[N];
LL ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for (int i=1;i<=n;i++){
		read(a[i]);
	}
	minn=0;
	for (int i=1;i<=n;i++){
		if (a[i]<minn){
			minn=a[i];
		}
		else{
			ans+=a[i]-minn;
			minn=a[i];
		}
	}
	printf("%lld",ans);
	return 0;
}

