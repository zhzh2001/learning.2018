#include<cstdio>
#include<cctype>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<ctime>
#define LL long long
#define N (100005)
using namespace std;
template <typename T> inline void read(T &t){
	t=0;
	char p=getchar(); bool f=0;
	while (!isdigit(p)){
		if (p=='-') f=1;
		p=getchar();
	}
	do{
		t=t*10+p-'0';
		p=getchar();
	}while(isdigit(p));
	if (f) t=-t;
}
int n,T;
int main(){
	freopen("money.in","w",stdout);
	srand(time(0));
	T=20;
	printf("%d\n",T);
	while (T--){
		n=100;
		printf("%d\n",n);
		for (int i=1;i<=n;i++){
			printf("%d ",rand()%25000+1);
		}
		puts("");
	}
	return 0;
}

