#include<cstdio>
#include<cctype>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<cstring>
#define LL long long
#define N (100005)
using namespace std;
template <typename T> inline void read(T &t){
	t=0;
	char p=getchar(); bool f=0;
	while (!isdigit(p)){
		if (p=='-') f=1;
		p=getchar();
	}
	do{
		t=t*10+p-'0';
		p=getchar();
	}while(isdigit(p));
	if (f) t=-t;
}
int T,n,ans;
int a[N];
bool f[25005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	while (T--){
		read(n);
		ans=n;
		for (int i=1;i<=n;i++) read(a[i]);
		sort(a+1,a+n+1);
		memset(f,0,sizeof(f));
		f[0]=1;
		for (int i=1;i<=n;i++){
			if (f[a[i]]){
				ans--;
				continue;
			}
			for (int j=0;j<=25000-a[i];j++){
				if (f[j]) f[j+a[i]]=1;
			}
		} 
		printf("%d\n",ans);
	}
	return 0;
}

