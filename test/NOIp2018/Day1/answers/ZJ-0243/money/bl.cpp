#include<cstdio>
#include<cctype>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<cstring>
#define LL long long
#define N (100005)
using namespace std;
template <typename T> inline void read(T &t){
	t=0;
	char p=getchar(); bool f=0;
	while (!isdigit(p)){
		if (p=='-') f=1;
		p=getchar();
	}
	do{
		t=t*10+p-'0';
		p=getchar();
	}while(isdigit(p));
	if (f) t=-t;
}
int n,t,ans,T;
int a[N],q[N];
bool choose[N],f[10005];
void out(int tt){
	t=0;
	for (int i=1;i<=n;i++){
		if (choose[i]) q[++t]=a[i];
	}
	memset(f,0,sizeof(f));
	f[0]=1;
	for (int i=1;i<=t;i++){
		for (int j=0;j<=1000-q[i];j++){
			if (f[j]) f[j+q[i]]=1;
		}
	}
	for (int i=1;i<=n;i++){
		if (!f[a[i]]) return; 
	}
	ans=min(ans,tt);
}
void dfs(int x,int tt){
	if (x>n){
		out(tt);
		return;
	}
	choose[x]=1;
	dfs(x+1,tt+1);
	choose[x]=0;
	dfs(x+1,tt);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("bl.out","w",stdout);
	read(T);
	while (T--){
		read(n);
		ans=n;
		for (int i=1;i<=n;i++) read(a[i]);
		sort(a+1,a+n+1);
		dfs(1,0);
		printf("%d\n",ans);
	}
	
	return 0;
}

