#include<cstdio>
#include<cctype>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<ctime>
#define LL long long
#define rand()((rand()<<15)^rand())
#define N (100005)
using namespace std;
template <typename T> inline void read(T &t){
	t=0;
	char p=getchar(); bool f=0;
	while (!isdigit(p)){
		if (p=='-') f=1;
		p=getchar();
	}
	do{
		t=t*10+p-'0';
		p=getchar();
	}while(isdigit(p));
	if (f) t=-t;
}
int n,m;
int main(){
	freopen("track.in","w",stdout);
	srand(time(0));
	n=50000,m=1;
	printf("%d %d\n",n,m);
	for (int i=2;i<=n;i++){
		int x=rand()%(i-1)+1,y=rand()%10000+1;
		printf("%d %d %d\n",i,x,y);
	}
	return 0;
}

