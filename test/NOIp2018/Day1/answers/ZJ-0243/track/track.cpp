#include<cstdio>
#include<cctype>
#include<cmath>
#include<iostream>
#include<algorithm>
#define LL long long
#define N (50005)
using namespace std;
int n,m,tot,ans,t,mid,x,y,z;
struct node{
	int num,maxx;
}f[N];
int son[N<<1],nxt[N<<1],head[N],b[N<<1],fa[N],q[N];
inline void add(int x,int y,int z){
	son[++tot]=y,nxt[tot]=head[x],head[x]=tot,b[tot]=z;
}
template <typename T> void read(T &t){
	t=0;
	char ch=getchar(); bool f=0;
	while (!isdigit(ch)){
		if (ch=='-') f=1;
		ch=getchar();
	}
	do{
		t=t*10+ch-'0';
		ch=getchar();
	}while(isdigit(ch));
	if (f) t=-t;
}
int get(int x){
	int ret=0,j=1;
	for (int i=t;i>=1;i--){
		if (i==x) continue;
		if (q[i]>=mid){
			ret++;
			continue;
		}
		while (q[j]+q[i]<mid&&j<i) j++;
		if (j==x) j++;
		if (j>=i) break;
		ret++;
		j++;
	}
	return ret;
}
void solve(int u){
	sort(q+1,q+t+1);
	int tt=0,j=1;
	for (int i=t;i>=1;i--){
		if (q[i]>=mid){
			tt++;
			continue;
		}
		while (q[j]+q[i]<mid&&j<i) j++;
		if (j>=i) break;
		tt++;
		j++;
	}
	f[u].num+=tt;
	int l=0,r=t,ans=0;
	while (l<=r){
		int mid=l+r>>1;
		if (get(mid)<tt){
			r=mid-1;
		}
		else l=mid+1,ans=mid;
	}
	f[u].maxx=q[ans];
}
void dfs(int u){
	for (int p=head[u];p;p=nxt[p]){
		int v=son[p];
		if (v!=fa[u]){
			fa[v]=u;
			dfs(v);
		}
	}
}
void dfs1(int u){
	f[u].num=f[u].maxx=0;
	for (int p=head[u];p;p=nxt[p]){
		int v=son[p];
		if (v!=fa[u]){
			dfs1(v);
		}
	}
	t=0;
	for (int p=head[u];p;p=nxt[p]){
		int v=son[p];
		if (v!=fa[u]){
			f[u].num+=f[v].num;
			q[++t]=f[v].maxx+b[p];
		}
	}
	if (t!=0) solve(u);
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n),read(m);
	for (int i=1;i<n;i++){
		read(x),read(y),read(z);
		add(x,y,z);
		add(y,x,z);
	}
	dfs(1);
	int l=1,r=500000000;
	while (l<=r){
		mid=l+r>>1;
		dfs1(1);
		if (f[1].num>=m){
			ans=mid;
			l=mid+1;
		}
		else r=mid-1;
	}
	printf("%d",ans);
	return 0;
}

