#include<cstdio>
#include<cctype>
#include<cmath>
#include<iostream>
#include<algorithm>
#define LL long long
#define N (100005)
using namespace std;
template <typename T> inline void read(T &t){
	t=0;
	char p=getchar(); bool f=0;
	while (!isdigit(p)){
		if (p=='-') f=1;
		p=getchar();
	}
	do{
		t=t*10+p-'0';
		p=getchar();
	}while(isdigit(p));
	if (f) t=-t;
}
int n,m,tot,x,y,z,h,t;
int son[N<<1],nxt[N<<1],head[N],b[N<<1],dis[N],fa[N],dis2[N],q[N];
bool vis[N];
inline void add(int x,int y,int z){
	son[++tot]=y,nxt[tot]=head[x],b[tot]=z,head[x]=tot;
}
void dfs(int u){
	for (int p=head[u];p;p=nxt[p]){
		int v=son[p];
		if (v!=fa[u]){
			fa[v]=u;
			dis[v]=dis[u]+b[p];
			dfs(v);
		}
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("bl.out","w",stdout);
	read(n),read(m);
	for (int i=1;i<n;i++){
		read(x),read(y),read(z);
		add(x,y,z),add(y,x,z);
	}
	dfs(1);
	int maxx=1;
	for (int i=2;i<=n;i++){
		if (dis[maxx]<dis[i]) maxx=i;
	}
	h=t=0;
	q[++t]=maxx;
	vis[maxx]=1;
	while (h<t){
		int u=q[++h];
		for (int p=head[u];p;p=nxt[p]){
			int v=son[p];
			if (!vis[v]){
				q[++t]=v;
				vis[v]=1;
				dis2[v]=dis2[u]+b[p];
			}
		}
	}
	int maxdis=0;
	for (int i=1;i<=n;i++){
		if (dis2[i]>maxdis) maxdis=dis2[i];
	}
	printf("%d",maxdis);
	return 0;
}

