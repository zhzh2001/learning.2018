#include<bits/stdc++.h>
#define F_IO(_) freopen(#_".in","r",stdin);freopen(#_".out","w",stdout)
using namespace std;
#define REP(i,j,k) for(int i=(j),i##_END_=(k);i<=i##_END_;i++)
#define DREP(i,j,k) for(int i=(j),i##_END_=(k);i>=i##_END_;i--)
#define LREP(i,x) for(int i=head[x];i!=-1;i=nxt[i])
typedef long long lll;
template<typename T,typename V>
inline bool tomin(T &x,V y){if(x>y){x=y;return true;}return false;}
template<typename T,typename V>
inline bool tomax(T &x,V y){if(x<y){x=y;return true;}return false;}
/****************header****************/
bool BEG_DEF;
const int M=5e4+5;
int n,mm,m,a,b,c,A[M],mid,Mxdis,
    edge_cnt,head[M],to[M<<1],nxt[M<<1],cost[M<<1];
void Addedge(int u,int v,int w){
	to[edge_cnt]=v;
	cost[edge_cnt]=w;
	nxt[edge_cnt]=head[u];
	head[u]=edge_cnt++;
}
void dfs0(int x,int f,int d,int &A){
	if(tomax(Mxdis,d))A=x;
	LREP(i,x){
		int t=to[i],c=cost[i];
		if(t==f)continue;
		dfs0(t,x,d+c,A);
	}
}
bool END_DEF;
int main(){
F_IO(track);
cerr<<"\tMemory: "<<(&END_DEF-&BEG_DEF)/1024.0/1024.0<<"MB"<<endl;
	memset(head,-1,sizeof(head));
	scanf("%d%d",&n,&m);
	REP(i,1,n-1){
		scanf("%d%d%d",&a,&b,&c);
		Addedge(a,b,c);
		Addedge(b,a,c);
	}
	Mxdis=-1;
	int A;dfs0(1,-1,0,A);
	Mxdis=-1;
	int B;dfs0(A,-1,0,B);
	printf("%d\n",Mxdis);
	return 0;
}

