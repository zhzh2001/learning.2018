#include<bits/stdc++.h>
#define F_IO(_) freopen(#_".in","r",stdin);freopen(#_".out","w",stdout)
using namespace std;
#define REP(i,j,k) for(int i=(j),i##_END_=(k);i<=i##_END_;i++)
#define DREP(i,j,k) for(int i=(j),i##_END_=(k);i>=i##_END_;i--)
#define LREP(i,x) for(int i=head[x];i!=-1;i=nxt[i])
typedef long long lll;
template<typename T,typename V>
inline bool tomin(T &x,V y){if(x>y){x=y;return true;}return false;}
template<typename T,typename V>
inline bool tomax(T &x,V y){if(x<y){x=y;return true;}return false;}
/****************header****************/
bool BEG_DEF;
const int M=105;
int n,A[M],B[M],tp,lim;
bool H[25005],mark[25005];
void bru(){
	int &y=B[tp];
	mark[y]=true;
	while(true){
		bool op=false;
		REP(i,y,lim)if(mark[i]){
			REP(j,1,tp)
				if(i+B[j]<=lim){
					if(!mark[i+B[j]]){
						mark[i+B[j]]=true;
						op=true;
					}
				}
				else break;
		}
		if(!op)break;
	}
}
bool END_DEF;
int main(){
F_IO(money);
cerr<<"\tMemory: "<<(&END_DEF-&BEG_DEF)/1024.0/1024.0<<"MB"<<endl;
	int T;
	scanf("%d",&T);
	int Cas;
	while(T--){
		int ans=0,Mx=0;
		tp=0;
		memset(H,0,sizeof(H));
		memset(mark,0,sizeof(mark));
		scanf("%d",&n);
		REP(i,1,n){
			scanf("%d",&A[i]);
			tomax(Mx,A[i]);
			H[A[i]]=true;
		}
		if(n<=1){
			printf("%d\n",n);
			continue;
		}
		sort(A+1,A+1+n);
		lim=Mx;
		REP(i,1,lim){
			if(H[i]&&(!mark[i])){
				++ans;
				B[++tp]=i;
				bru();
			}
		}
		printf("%d\n",max(ans,2));
	}
	return 0;
}

