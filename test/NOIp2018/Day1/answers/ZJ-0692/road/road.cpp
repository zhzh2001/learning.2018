#include<bits/stdc++.h>
#define F_IO(_) freopen(#_".in","r",stdin);freopen(#_".out","w",stdout)
using namespace std;
#define REP(i,j,k) for(int i=(j),i##_END_=(k);i<=i##_END_;i++)
#define DREP(i,j,k) for(int i=(j),i##_END_=(k);i>=i##_END_;i--)
#define LREP(i,x) for(int i=head[x];i!=-1;i=nxt[i])
typedef long long lll;
template<typename T,typename V>
inline bool tomin(T &x,V y){if(x>y){x=y;return true;}return false;}
template<typename T,typename V>
inline bool tomax(T &x,V y){if(x<y){x=y;return true;}return false;}
/****************header****************/
bool BEG_DEF;
const int M=1e5+5,INF=0x3f3f3f3f;
int n,A[M];
void P70(){
	int mn=INF,ans=0,las=1;
	while(true){
		bool non=true;
		mn=INF;las=1;
		REP(i,1,n){
			if(A[i]==0){
				if(mn==INF){las=i+1;continue;}
				REP(j,las,i-1)
					A[j]-=mn;
				ans+=mn;
				las=i+1;
				mn=INF;
				non=false;
				continue;
			}
			if(A[i]<mn)mn=A[i];
		}
		if(mn!=INF){
			REP(j,las,n)
				A[j]-=mn;
			ans+=mn;
			non=false;
		}
		if(non)break;
	}
	printf("%d\n",ans);
}
bool END_DEF;
int main(){
F_IO(road);
cerr<<"\tMemory: "<<(&END_DEF-&BEG_DEF)/1024.0/1024.0<<"MB"<<endl;
	scanf("%d",&n);
	REP(i,1,n)scanf("%d",&A[i]);
//	if(n<=1000)
		P70();
//	else P100();
	return 0;
}

