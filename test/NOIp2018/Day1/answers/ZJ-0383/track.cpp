#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define ma 1000000007
using namespace std;
inline void read(int &x)
{
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
}
inline void read(ll &x)
{
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
}
inline void write(int x)
{
	if (x>9)write(x/10);
	putchar(x%10+48);
}
inline void write(ll x)
{
	if (x>9)write(x/10);
	putchar(x%10+48);
}
int max(int x,int y)
{
	return x>y?x:y;
}
ll max(ll x,ll y)
{
	return x>y?x:y;
}
struct Node{
	int x,y,z;
}d[100010];
ll s;
int n,m,x,y,z,t,tot;
int a[50010],b[50010],h[50010],f[50010][20],g[50010][20],de[50010];
void add(int x,int y,int z)
{
	d[++tot].x=h[x];
	d[tot].y=y;
	d[tot].z=z;
	h[x]=tot;
}
void fac(int x,int y)
{
	f[x][0]=y;
	for (int i=1;i<20;i++)
	f[x][i]=f[f[x][i-1]][i-1],g[x][i]=g[f[x][i-1]][i-1]+g[x][i-1];
	de[x]=de[y]+1;
	for (int i=h[x];i;i=d[i].x)
	{
		g[d[i].y][0]=d[i].z;
		fac(d[i].y,x);
	}
	if (!h[x])
	{
		t++;
		b[t]=x;
	}
}
ll lca(int x,int y)
{
	ll t=0;
	if (de[x]<de[y])swap(x,y);
	for (int i=19;i>=0;i--)
	if (de[f[x][i]]>de[y])t+=g[x][i],x=f[x][i];
	if (x==y)return t;
	for (int i=19;i>=0;i--)
	if (f[x][i]!=f[y][i])t+=g[x][i]+g[y][i],x=f[x][i],y=f[y][i];
	return t+g[x][0]+g[y][0];
}
int tr(int x)
{
	int s=0,j=1;
	for (int i=n;i>0&&i>=j&&s<x;i--)
	{
		if (a[i]>=x)
		{
			s++;
			continue;
		}
		while (a[i]+a[j]<x&&i>j)j++;
		if (a[i]+a[j]>=x&&i<j)s++,j++;
		else break;
	}
	if (s>=m)return 1;
	else return 0;
}
int tr1(int x)
{
	int s=0;
	for (int i=2,j=1;i<=n;i++)
	if (b[i]-b[j]>=x)
	{
		s++;
		j=i;
	}
	if (s>=m)return 1;
	else return 0;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);
	int o=0,p=0;
	for (int i=1;i<n;i++)
	{
		read(x);read(y);read(z);
		if (x>y)swap(x,y);
		add(x,y,z);
		a[y]=x;
		if (x!=1)o=1;
		if (y!=x+1)p=1;
	}
	x=0;
	for (int i=1;i<=n&&x==0;i++)
	if (a[i]==0)x=i;
	if (m==1)
	{
		fac(x,0);
		for (int i=1;i<t;i++)
		for (int j=i+1;j<=t;j++)
		s=max(s,lca(b[i],b[j]));
		write(s);
	}
	else if (!o)
	{
		for (int i=h[1];i;i=d[i].x)
		b[d[i].y]=d[i].z;
		sort(b+1,b+n+1);
		int l=b[1]+b[2],r=1000001,t=b[1]+b[2];
		while (l<=r)
		{
			int mid=(l+r)/2;
			if (tr(mid))l=mid+1,t=mid;
			else r=mid-1;
		}
		write(t);
		return 0;
	}
	else if (!p)
	{
		for (int j=1;j<=n;j++)
		for (int i=h[j];i;i=d[i].x)
		b[d[i].y]=b[j]+d[i].z;
		int l=b[2],r=1000001,t=b[2];
		while (l<=r)
		{
			int mid=(l+r)/2;
			if (tr1(mid))l=mid+1,t=mid;
			else r=mid-1;
		}
		write(t);
	}
	return 0;
}
