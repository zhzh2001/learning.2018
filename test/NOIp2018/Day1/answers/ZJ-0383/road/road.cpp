#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define ma 1000000007
using namespace std;
inline void read(int &x)
{
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
}
inline void read(ll &x)
{
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
}
inline void write(int x)
{
	if (x>9)write(x/10);
	putchar(x%10+48);
}
inline void write(ll x)
{
	if (x>9)write(x/10);
	putchar(x%10+48);
}
struct Node{
	int x,y;
}a[100010];
int n,s=0,b[100010];
int cnm(Node x,Node y)
{
	return x.x<y.x;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for (int i=1;i<=n;i++)
	read(a[i].x),a[i].y=i;
	sort(a+1,a+n+1,cnm);
	for (int i=1;i<=n;i++)
	b[i]=1;
	int t=1,j=1;
	for (int i=a[1].x;i<=a[n].x;i++)
	{
		s+=t;
		while (a[j].x==i)
		{
			b[a[j].y]=0;
			if (b[a[j].y-1]&&b[a[j].y+1])t++;
			if (!b[a[j].y-1]&&b[a[j].y+1]==0)t--;
			j++;
		}
	}
	write(s+a[1].x-1);
	return 0;
}
