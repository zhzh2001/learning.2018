#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define ma 1000000007
using namespace std;
inline void read(int &x)
{
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
}
inline void read(ll &x)
{
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
}
inline void write(int x)
{
	if (x>9)write(x/10);
	putchar(x%10+48);
}
inline void write(ll x)
{
	if (x>9)write(x/10);
	putchar(x%10+48);
}
int T,a[1010],f[25010],ans;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	while (T--)
	{
		int n=0,s=1;
		memset(a,0,sizeof(a));
		read(n);
		for (int i=1;i<=n;i++)
		read(a[i]);
		sort(a+1,a+n+1);
		for (int i=1;i<=a[n];i++)
		f[i]=1;
		for (int i=1;i<=a[n]/a[1];i++)
		f[i*a[1]]=0;
		for (int i=2;i<=n;i++)
		if (f[a[i]])
		{
			f[a[i]]=0;
			s++;
			for (int j=a[1];j<=a[n]-a[i];j++)
			if (!f[j])f[j+a[i]]=0;
		}
		write(s);
		printf("\n");
	}
	return 0;
}
