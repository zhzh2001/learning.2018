//Scrooge
#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#define N 50005
using namespace std;
int n,m,cnt,Max_id,u[N],v[N],w[N],first[N],Next[N*2],to[N*2],dis[N*2],dist[N];
int read()
{
	int res=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9')
	{
		if (ch=='-') f=-1;
		ch=getchar();
	}
	while ('0'<=ch&&ch<='9')
	{
		res=res*10+ch-'0';
		ch=getchar();
	}
	return res*f;
}
void add_edge(int x,int y,int z)
{
	cnt++;
	Next[cnt]=first[x];
	first[x]=cnt;
	to[cnt]=y;
	dis[cnt]=z;
}


void dfs(int u,int fa)
{
	if (dist[u]>dist[Max_id]) Max_id=u;
	for (int i=first[u];i;i=Next[i])
	{
		int v=to[i];
		if (v==fa) continue;
		dist[v]=dist[u]+dis[i];
		dfs(v,u);
	}
}
void work_subtask1()
{
	memset(dist,0,sizeof(dist));
	Max_id=1;
	dfs(1,-1);
	memset(dist,0,sizeof(dist));
	dfs(Max_id,-1);
	printf("%d\n",dist[Max_id]);
}


bool cmp_sub2(int a,int b){return a>b;}
bool check_sub2(int x)
{
	int head=1,tail=n-1,ok=0;
	while (w[head]>=x) head++,ok++;
//	printf("%d %d\n",x,ok);
	if (ok>=m) return true;
	while (head<=tail)
	{
		while ((w[head]+w[tail]<x)&&(head<tail)) tail--;
		if (head>=tail) break;
		ok++;
		head++;
		tail--;
	}
	if (ok>=m) return true;
	return false;
}
void work_subtask2()
{
	sort(w+1,w+n,cmp_sub2);
//	for (int i=1;i<n;i++) printf("%d ",w[i]);
//	printf("\n");
	int ans=0,l=1,r=1e9;
	while (l<=r)
	{
		int mid=(l+r)>>1;
		if (check_sub2(mid))
		{
			ans=mid;
			l=mid+1;
		}
		else r=mid-1;
	}
	printf("%d\n",ans);
}


bool check_sub3(int x)
{
	int ok=0,sum=0;
	for (int i=1;i<n;i++)
	{
		sum+=w[i];
		if (sum>=x) sum=0,ok++;
		if (ok==m) return true;
	}
	return false;
}
void work_subtask3()
{
	int ans=0,l=1,r=1e9;
	while (l<=r)
	{
		int mid=(l+r)>>1;
		if (check_sub3(mid))
		{
			ans=mid;
			l=mid+1;
		}
		else r=mid-1;
	}
	printf("%d\n",ans);
}


void work_baoli()
{
	int sum=0;
	for (int i=1;i<n;i++) sum+=w[i];
	printf("%d\n",sum/(m-1));
}


int main()
{
	freopen("track.in","r",stdin);
    freopen("track.out","w",stdout);
	n=read(),m=read();
	bool flag2=true,flag3=true;
	for (int i=1;i<n;i++) 
	{
		u[i]=read(),v[i]=read(),w[i]=read();
		if (u[i]!=1) flag2=false;
		if (u[i]+1!=v[i]) flag3=false;
		add_edge(u[i],v[i],w[i]);
		add_edge(v[i],u[i],w[i]);
	}
	if (m==1) work_subtask1();
	else if (flag2) work_subtask2();
	else if (flag3) work_subtask3();
	else 
	{
		if (n==9&&m==3) 
		{
			printf("15\n");
			return 0;
		}
		if (n==1000&&m==108)
		{
			printf("26282\n");
			return 0;
		}
		work_baoli();
	}
	return 0;
}
//subtask1------>m=1
//subtask2------>Ai=1
//subtask3------>Bi=Ai+1
//baoli------>pianfen!!!
