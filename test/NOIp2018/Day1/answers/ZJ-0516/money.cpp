//Scrooge
#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#define N 105
using namespace std;
int Test_num,ans,n,a[N];
bool dp[25005];
bool cmp(int a,int b){return a<b;}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&Test_num);
	while (Test_num--)
	{
		ans=0;
		memset(dp,false,sizeof(dp));
		dp[0]=true;
		int Max=0;
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]),Max=max(Max,a[i]);
		sort(a+1,a+n+1,cmp);
		for (int i=1;i<=n;i++)
		if (!dp[a[i]])
		{
			ans++;
			dp[a[i]]=true;
			for (int j=a[i];j<=Max;j++)
			if (dp[j-a[i]]) dp[j]=true;
		}
		printf("%d\n",ans);
	}
	return 0;
}
