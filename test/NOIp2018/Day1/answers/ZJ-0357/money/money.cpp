#include<bits/stdc++.h>
using namespace std;
int T,minl,maxn,ans,n,a[205];
int l[25500];
int f[25500];
int read()
{
	int x=0;
	char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') {
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	return x;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--)
	{
		minl=9999999; maxn=0;
		memset(f,0,sizeof(f));
		n=read();
		for(int i=1;i<=n;i++) {a[i]=read();minl=min(a[i],minl);maxn=max(maxn,a[i]);}
		sort(a+1,a+n+1);
		for(int i=1;i<=maxn/a[1];++i) f[a[1]*i]=1;
		ans=1;
		for(int i=2;i<=n;++i) {
			if(f[a[i]]) continue;
			f[a[i]]=1; ans++;
			for(int j=minl;j<=maxn;++j) {
				if(f[j]) continue;
				for(int k=1;j-a[i]*k>=minl;k++) if(f[j-a[i]*k]) f[j]=1;
			}
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
