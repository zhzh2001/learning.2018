#include<bits/stdc++.h>
using namespace std;
int n,m,from,to,diss,head[50010],num,L1[50010],L2[50010],ans,minl,tt,k[50010],s[50010];
bool flag;
int read()
{
	int x=0;
	char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') {
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar(); 
	}
	return x;
}
struct xx
{
	int next,to,dis;
}way[100010];
void add(int from,int to,int dis)
{
	way[++num].next=head[from];
	way[num].to=to; way[num].dis=dis;
	head[from]=num;
}
void dp(int x,int fa)
{
	for(int i=head[x];i;i=way[i].next)
	{
		int y=way[i].to;
		if(y==fa) continue;
		dp(y,x);
		if(L1[y]+way[i].dis>L1[x]) {L2[x]=L1[x]; L1[x]=L1[y]+way[i].dis;}
		else {
			if(L1[y]+way[i].dis>L2[x]) L2[x]=L1[y]+way[i].dis;
		}
	}
	ans=max(ans,L1[x]+L2[x]);
}
bool cmp(int a,int b)
{
	return a>b;
 } 
bool pd(int x)
{
	int l=0; int sum=0;
	for(int i=1;i<n;++i)
	{
		if(l+k[i]<x) l+=k[i];
		else { l=0; sum++; }
	}
	if(sum>=m) return 1;
	return 0;
}
bool v[50010];
bool pdd(int x)
{
	memset(v,0,sizeof(v));
	int mm=1; int sum=0; 
	for(int i=n-1;i>=1;i--)
	{
		if(v[i]) continue;
		v[i]=1;
		if(s[i]>=x) {sum++;continue;}
		if(s[mm]+s[i]>=x) {v[mm]=1;sum++;mm++; continue;}
		else {
			while(s[mm]+s[i]<x&&mm<i) {v[mm]=1;mm++;}
			if(mm>=i) break;
			sum++;
		}
	}
	if(sum>=m) return 1;
	return 0; 
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(); m=read();
	minl=9999999; flag=1;
	for(int i=1;i<n;i++)
	{
		from=read(); to=read(); diss=read();
		if(from!=1) flag=0;
		add(from,to,diss); add(to,from,diss);
		minl=min(minl,diss); tt+=diss; k[from]=diss;
		s[i]=diss;
	}
	if(m==1) {
		dp(1,0);
		printf("%d",ans);
	}
	else{
		if(m==n-1) printf("%d",minl);
		else {
			if(flag==0)
			{
				int l=minl,r=tt/m;
				while(l<r) {
					int mid=(l+r+1)>>1;
					if(pd(mid)) l=mid;
					else r=mid-1;
				}
				printf("%d",l);
			}
			else {
				sort(s+1,s+n);
				int l=minl,r=tt/m;
				while(l<r) {
					int mid=(l+r+1)>>1;
					if(pdd(mid)) l=mid;
					else r=mid-1;
				}
				printf("%d",l);
			}
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
