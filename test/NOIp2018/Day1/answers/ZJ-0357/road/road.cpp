#include<bits/stdc++.h>
using namespace std;
int n,a[100010],ans,st[100010][21];
vector<int> v[10010];
int read()
{
	int x=0;
	char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9')
	{
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	return x;
}
void pre()
{
	int t=floor(log(1.0*n)/log(2.0))+1;
	for(int j=1;j<=t;++j)
	for(int i=1;i+(1<<j)-1<=n;++i)
	st[i][j]=min(st[i][j-1],st[i+(1<<(j-1))][j-1]);
}
void work(int l,int r,int s)
{
	if(l>r) return;
	int t=floor(log(1.0*(r-l+1))/log(2.0));
	int g=min(st[l][t],st[r-(1<<t)+1][t]);
	ans+=g-s;
	int l1=lower_bound(v[g].begin(),v[g].end(),l)-v[g].begin();
	int r1=(upper_bound(v[g].begin(),v[g].end(),r)-v[g].begin())-1;
	int h=l;
	for(int i=l1;i<=r1;++i) {work(h,v[g][i]-1,g);h=v[g][i]+1;}
	work(v[g][r1]+1,r,g); 
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;++i) {a[i]=read(); v[a[i]].push_back(i);st[i][0]=a[i];}
	pre();
	work(1,n,0);	
	printf("%d",ans);
	return 0;
}
