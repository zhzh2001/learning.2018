#include<bits/stdc++.h>
#define ll long long
#define db double
#define pii pair<int,int>
#define mp make_pair
#define pb push_back
#define A first
#define B second
#define lowbit(p) (p&(-(p)))
using namespace std;
void read(int &x){
	x=0; char c=getchar(); int p=1;
	for (;c<48;c=getchar())if (c=='-')p=-1;
	for (;c>47;c=getchar())x=(x<<1)+(x<<3)+(c^48);
	x*=p;
}
void read(ll &x){
	x=0; char c=getchar(); int p=1;
	for (;c<48;c=getchar())if (c=='-')p=-1;
	for (;c>47;c=getchar())x=(x<<1)+(x<<3)+(c^48);
	x*=p;
}
void Min(int &x,int y){
	if (x>y)x=y;
}
void Min(ll &x,ll y){
	if (x>y)x=y;
}
void Max(int &x,int y){
	if (x<y)x=y;
}
void Max(ll &x,ll y){
	if (x<y)x=y;
}
bool sttt;
#define M 50005
struct ed{
	int x,l,nx;
}e[M<<1];
int ecnt=1,nx[M],n,m,deg[M];
void add(int x,int y,int l){
	e[ecnt]=(ed){y,l,nx[x]};
	nx[x]=ecnt++;
}
struct P1{
	int mx,id;
	void dfs(int f,int x,int l){
		if (l>mx){
			mx=l;
			id=x;
		}
		for (int i=nx[x];i;i=e[i].nx)if (e[i].x!=f){
			dfs(x,e[i].x,l+e[i].l);
		}
	}
	void solve(){
		dfs(1,1,0);
		mx=0;
		dfs(id,id,0);
		printf("%d\n",mx);
	}
}p1;
struct P2{
	ll L,mx1[M],mx2[M];
	int cnt,rt;
	void dfs(int f,int x){
		mx1[x]=0;
		mx2[x]=0;
		for (int i=nx[x];i;i=e[i].nx)if (e[i].x!=f){
			dfs(x,e[i].x);
			if (mx1[e[i].x]+e[i].l>=L){
				cnt++;
			}
			else if (mx1[e[i].x]+e[i].l>mx1[x]){
				mx2[x]=mx1[x];
				mx1[x]=mx1[e[i].x]+e[i].l;
			}
			else if (mx1[e[i].x]+e[i].l>mx2[x]){
				mx2[x]=mx1[e[i].x]+e[i].l;
			}
		}
		if (mx1[x]+mx2[x]>=L){
			cnt++;
			mx1[x]=mx2[x]=0;
		}
	}
	bool chk(ll mid){
		L=mid;
//		printf("rt=%d\n",rt);
		cnt=0;
		dfs(rt,rt);
//		printf("L=%lld\n",L);
//		printf("cnt=%d\n",cnt);
		return cnt>=m;
	}
	void solve(){
		ll l,r,mid,res;
		int i;
		for (i=1;i<=n;i++){
			if (deg[i]<3)rt=i;
		}
		l=1; r=1e9;
		for (;l<=r;){
			mid=(l+r)>>1;
			if (chk(mid)){
				l=mid+1;
				res=mid;
			}
			else{
				r=mid-1;
			}
		}
		printf("%lld\n",res);
	}
}p2;
struct P3{
	ll L,tmp[M],a[M];
	int cnt,tim,mark[M];
	int CHK(int tot,int Pos){
		tim++;
		mark[Pos]=tim;
		int l,r,ct=0;
		l=1; r=tot;
		for (;l<r;){
			for (;l<r&&mark[l]==tim;l++);
			for (;l<r&&mark[r]==tim;r--);
			if (l<r){
				if (tmp[l]+tmp[r]>=L){
					mark[l]=tim;
					mark[r]=tim;
					ct++;
					l++;
					r--;
				}
				else {
					l++;
				}
			}
		}
		return ct;
	}
	ll ck(int tot,int c){
		int l,r,res=0,mid;
		l=1; r=tot;
		for (;l<=r;){
			mid=(l+r)>>1;
			if (CHK(tot,mid)>=c){
				l=mid+1;
				res=mid;	
			}
			else {
				r=mid-1;
			}
		}
		return tmp[res];
	}
	void dfs(int f,int x){
		int tot=0;
		ll mx;
		for (int i=nx[x];i;i=e[i].nx)if (e[i].x!=f){
			dfs(x,e[i].x);
		}
		for (int i=nx[x];i;i=e[i].nx)if (e[i].x!=f){
			mx=a[e[i].x]+e[i].l;
			if (mx>=L){
				cnt++;
			}
			else {
				tmp[++tot]=mx;
			}
		}
		sort(tmp+1,tmp+tot+1);
//		for (int i=1;i<=tot;i++)printf("%lld ",tmp[i]); printf("\n");
		int ct=CHK(tot,0);
//		printf("ct=%d\n",ct);
		cnt+=ct;
		a[x]=ck(tot,ct);
//		printf("a[]=%lld\n",a[x]);
	}
	bool chk(ll mid){
		L=mid;
		cnt=0;
//		printf("-----%lld-----\n",L);
		dfs(1,1);
//		printf("cnt=%d\n",cnt);
		return cnt>=m;
	}
	void solve(){
		ll l,r,mid,res;
		l=1; r=1e9;
		for (;l<=r;){
			mid=(l+r)>>1;
			if (chk(mid)){
				l=mid+1;
				res=mid;
			}
			else{
				r=mid-1;
			}
		}
		printf("%lld\n",res);
	}
}p3;
bool eddd;
int main(){
//	printf("%.2f\n",(&eddd-&sttt)/1024.0/1024.0);
//	freopen("3.in","r",stdin);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n); read(m);
	int i,x,y,l;
	for (i=1;i<n;i++){
		read(x); read(y); read(l);
		deg[x]++;
		deg[y]++;
		add(x,y,l);
		add(y,x,l);
	}
	if (m==1){
		p1.solve();
		return 0;
	}
	int flag=1;
	for (i=1;i<=n;i++){
		if (deg[i]>3)flag=0;
	}
	if (flag){
		p2.solve();
		return 0;
	}
	p3.solve();
	return 0;
}

