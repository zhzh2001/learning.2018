#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;

const int N = 100007;

int n, d[N], Ans;

int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; ++i)
		scanf("%d", &d[i]);
	Ans = 0;
	for (int i = 1; i <= n; ++i)
		if (d[i] > d[i-1])
			Ans += d[i] - d[i-1];
	printf("%d\n", Ans);
	return 0;
}
