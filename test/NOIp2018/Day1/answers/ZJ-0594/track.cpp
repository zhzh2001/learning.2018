#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;

const int N = 50007;

int n, m;

namespace Case1{
	void solve(){
		int Ans = 500000007;
		for (int i = 1; i < n; ++i){
			int u, v, w; scanf("%d%d%d", &u, &v, &w);
			Ans = min(Ans, w);
		}
		printf("%d\n", Ans);
	}
}

namespace Case2{
	int ecnt, head[N], vet[N << 1], nxt[N << 1], val[N << 1];
	int f[N], g[N], Ans;
	inline void eadd(int u, int v, int w){
		vet[++ecnt] = v;
		val[ecnt] = w;
		nxt[ecnt] = head[u];
		head[u] = ecnt;
	}
	void dfs(int u, int pre){
		f[u] = 0; g[u] = 0;
		for (int e = head[u]; e; e = nxt[e]){
			int v = vet[e], w = val[e];
			if (v == pre) continue;
			dfs(v, u);
			if (f[v] + w > f[u]){
				g[u] = f[u];
				f[u] = f[v] + w;
			}else if (f[v] + w > g[u])
				g[u] = f[v] + w;
		}
		Ans = max(Ans, f[u] + g[u]);
	}
	void solve(){
		for (int i = 1; i < n; ++i){
			int u, v, w; scanf("%d%d%d", &u, &v, &w);
			eadd(u, v, w); eadd(v, u, w);
		}
		Ans = 0;
		dfs(1, 0);
		printf("%d\n", Ans);
	}
}

namespace Case3{
	int mid, ecnt, head[N], vet[N << 1], nxt[N << 1], val[N << 1];
	int f[N][2];
	inline void eadd(int u, int v, int w){
		vet[++ecnt] = v;
		val[ecnt] = w;
		nxt[ecnt] = head[u];
		head[u] = ecnt;
	}
	void dfs(int u, int pre){
		f[u][0] = 0; f[u][1] = 0;
		int Max = 0, Second = 0;
		for (int e = head[u]; e; e = nxt[e]){
			int v = vet[e], w = val[e];
			if (v == pre) continue;
			dfs(v, u);
			f[u][0] += f[v][0];
			if (f[v][1] + w >= mid){
				++f[u][0];
				continue;
			}
			if (f[v][1] + w > Max){
				Second = Max;
				Max = f[v][1] + w;
			}else if (f[v][1] + w > Second)
				Second = f[v][1] + w;
		}
		if (Max + Second >= mid) ++f[u][0];
		else f[u][1] = Max;
	}
	bool check(){
		dfs(1, 0);
		return (f[1][0] >= m);
	}
	void solve(){
		for (int i = 1; i < n; ++i){
			int u, v, w; scanf("%d%d%d", &u, &v, &w);
			eadd(u, v, w); eadd(v, u, w);
		}
		int l = 1, r = 500000000, res = -1;
		while (l <= r){
			mid = (l + r) >> 1;
			if (check()){res = mid; l = mid + 1;}
			else{r = mid - 1;}
		}
		printf("%d\n", res);
	}
}

int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (m == n-1) Case1::solve();
	else if (m == 1) Case2::solve();
	else Case3::solve();
	return 0;
}
