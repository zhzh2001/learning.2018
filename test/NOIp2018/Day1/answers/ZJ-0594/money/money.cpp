#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;

const int N = 107;
const int M = 25007;

int T, n, a[N], maxa, ans;
bool dp[M];

int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	while (T--){
		scanf("%d", &n);
		maxa = 0;
		for (int i = 1; i <= n; ++i){
			scanf("%d", &a[i]);
			maxa = max(maxa, a[i]);
		}
		ans = n;
		for (int sta = 0; sta < (1 << n); ++sta){
			int cnt = 0;
			for (int i = 0; i <= maxa; ++i) dp[i] = false;
			dp[0] = true;
			for (int i = 1; i <= n; ++i)
				if ((sta >> i-1) & 1){
					++cnt;
					for (int j = a[i]; j <= maxa; ++j)
						dp[j] = dp[j] || dp[j - a[i]];
				}
			bool check = true;
			for (int i = 1; i <= n; ++i)
				if (!dp[a[i]]){check = false; break;}
			if (check) ans = min(ans, cnt);
		} 
		printf("%d\n", ans);
	}
	return 0;
}
