#include<queue>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#define N 300005
using namespace std;
template <class T>
inline void Rd(T &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c='0';}
	do{
		res=(res<<3)+(res<<1)+(c^48);
	}while(c=getchar(),c>=48);
	res*=k;
}
template <class T>
inline void Pt(T res){
	if(res<0){
		putchar('-');
		res=-res;
	}
	if(res>=10)Pt(res/10);
	putchar(res%10+48);
}
struct edge{
	int v,w,nxt;
}e[N];
int n,m;
int head[N],edgecnt;
int dep[N],dis[N],top[N],sz[N],son[N],fa[N];
int LCA(int u,int v){
	while(top[u]!=top[v]){
		if(dep[top[u]]>dep[top[v]])u=fa[top[u]];
		else v=fa[top[v]];
	}
	return dep[u]<dep[v]?u:v;
}
struct P1{
	priority_queue<int>que;
	void solve(){
		for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
		que.push(dis[i]+dis[j]-2*dis[LCA(i,j)]);
		for(int i=1;i<m;i++)que.pop();
		int ans=que.top();
		Pt(ans);
		putchar('\n');
	}
}P1;
struct P2{
	priority_queue<int>que;
	void solve(){
		for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
		que.push(dis[i]+dis[j]-2*dis[LCA(i,j)]);
		for(int i=1;i<m;i++)que.pop();
		int ans=que.top();
		Pt(ans);
		putchar('\n');
	}
}P2;
struct P3{
	int mx,id;
	void dfs(int x,int f,int d){
		if(d>mx){mx=d;id=x;}
		for(int i=head[x];~i;i=e[i].nxt){
			int v=e[i].v,w=e[i].w;
			if(v!=f)dfs(v,x,d+w);
		}
	}
	void solve(){
		mx=0;
		dfs(1,0,0);
		dfs(id,0,0);
		Pt(mx);
		putchar('\n');
	}
}P3;
void dfs(int x,int f){
	fa[x]=f;
	sz[x]=1;
	for(int i=head[x];~i;i=e[i].nxt){
		int v=e[i].v,w=e[i].w;
		if(v==f)continue;
		dis[v]=dis[x]+w;
		dep[v]=dep[x]+1;
		dfs(v,x);
		if(sz[v]>sz[son[x]])son[x]=v;
		sz[x]+=sz[v];
	}
}
void rdfs(int x,int tp){
	top[x]=tp;
	if(son[x])rdfs(son[x],tp);
	for(int i=head[x];~i;i=e[i].nxt){
		int v=e[i].v;
		if(v==fa[x]||v==son[x])continue;
		rdfs(v,v);
	}
}
void add_edge(int u,int v,int w){
	e[++edgecnt]=(edge){v,w,head[u]};head[u]=edgecnt;
	e[++edgecnt]=(edge){u,w,head[v]};head[v]=edgecnt;
}
//1s 512M
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	Rd(n);Rd(m);
	memset(head,-1,sizeof(head));
	bool check1=1,check2;
	int u,v,w;
	for(int i=1;i<n;i++){
		Rd(u);Rd(v);Rd(w);
		if(u!=1)check1=0;
		if(u+1!=v)check2=0;
		add_edge(u,v,w);
	}
	dfs(1,0);
	rdfs(1,1);
	if(m==1)P3.solve();
	else if(check1)P1.solve();
	else if(check2)P2.solve();
	return 0;
}
