#include<set>
#include<stdio.h>
#include<algorithm>
#include<iostream>
#define N 100005
#define M 10005
using namespace std;
template <class T>
inline void Rd(T &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c='0';}
	do{
		res=(res<<3)+(res<<1)+(c^48);
	}while(c=getchar(),c>=48);
	res*=k;
}
template <class T>
inline void Pt(T res){
	if(res<0){
		putchar('-');
		res=-res;
	}
	if(res>=10)Pt(res/10);
	putchar(res%10+48);
}
int n;
int d[N];
struct P1{
	typedef pair<int,int> P;
	P q[N];
	int ans;
	void solve(){
		ans=0;
		for(int i=1;i<=n;i++)q[i]=P(d[i],i);
		sort(q+1,q+n+1);
		for(int i=1;i<=n;i++){
			int pos=q[i].second;
			if(!d[pos])continue;
			int w=d[pos];
			int L=pos,R=pos;
			while(d[L-1])L--;
			while(d[R+1])R++;
			for(int j=L;j<=R;j++)d[j]-=w;
			ans+=w;
		}
		Pt(ans);
		putchar('\n');
		
	}
}P1;
struct P2{
	struct tree{
		struct node{
			int L,R,val,add;
		}tree[N<<2];
		void build(int L,int R,int p){
			tree[p].L=L,tree[p].R=R,tree[p].val=tree[p].add=0;
			if(L==R){
				tree[p].val=d[L];
				return;
			}
			int mid=(L+R)>>1;
			build(L,mid,p<<1);
			build(mid+1,R,p<<1|1);
		}
		void down(int p){
			if(!tree[p].add)return;
			int L=p<<1,R=p<<1|1,w=tree[p].add;
			tree[L].val+=w;
			tree[L].add+=w;
			tree[R].val+=w;
			tree[R].add+=w;
			tree[p].add=0;
		}
		void add(int L,int R,int x,int p){
			if(tree[p].L==L&&tree[p].R==R){
				tree[p].add+=x;
				tree[p].val+=x;
				return;
			}
			down(p);
			int mid=(tree[p].L+tree[p].R)>>1;
			if(R<=mid)add(L,R,x,p<<1);
			else if(L>mid)add(L,R,x,p<<1|1);
			else{
				add(L,mid,x,p<<1);
				add(mid+1,R,x,p<<1|1);
			}
		}
		int query(int x,int p){
			if(tree[p].L==tree[p].R)return tree[p].val;
			down(p);
			int mid=(tree[p].L+tree[p].R)>>1;
			if(x<=mid)return query(x,p<<1);
			return query(x,p<<1|1);
		}
	}T;
	typedef pair<int,int> P;
	P q[N];
	multiset<int>mt;
	int ans;
	void solve(){
		ans=0;
		for(int i=1;i<=n;i++)q[i]=P(d[i],i);
		sort(q+1,q+n+1);
		T.build(1,n,1);
		mt.insert(0);
		mt.insert(n+1);
		for(int i=1;i<=n;i++){
			int pos=q[i].second;
			if(!d[pos]){mt.insert(pos);continue;}
			int w=T.query(pos,1);
			if(!w){mt.insert(pos);continue;}
			ans+=w;
			int R=*(mt.lower_bound(pos))-1;
			int L=*(--mt.lower_bound(pos))+1;
			T.add(L,R,-w,1);
			mt.insert(pos);
		}
		Pt(ans);
		putchar('\n');
	}
}P2;
//1s 512M
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	Rd(n);
	for(int i=1;i<=n;i++)Rd(d[i]);
	if(n<=1000)P1.solve();
	else P2.solve();
	return 0;	
}
