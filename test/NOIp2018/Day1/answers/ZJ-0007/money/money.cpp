#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#define N 105
#define M 25005
using namespace std;
template <class T>
inline void Rd(T &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c='0';}
	do{
		res=(res<<3)+(res<<1)+(c^48);
	}while(c=getchar(),c>=48);
	res*=k;
}
template <class T>
inline void Pt(T res){
	if(res<0){
		putchar('-');
		res=-res;
	}
	if(res>=10)Pt(res/10);
	putchar(res%10+48);
}
int n,cas;
int A[N];
struct P1{
	void solve(){
		if(A[1]%A[2]==0||A[2]%A[1]==0)Pt(1);
		else Pt(2);
		putchar('\n');
	}
}P1;
struct P2{
	int ans;
	int cnt[M];
	bool dp[M];
	bool mark[N];
	void solve(){
		int mx=0;
		memset(cnt,0,sizeof(cnt));
		memset(mark,0,sizeof(mark));
		ans=0;
		for(int i=1;i<=n;i++){
			mx=max(mx,A[i]);
			cnt[A[i]]++;
		}
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++){
				if(j==i)continue;
				if(A[j]<A[i]&&A[i]%A[j]==0){mark[i]=1;break;}
			}
		}
		for(int i=1;i<=n;i++)
		if(!mark[i]){
			memset(dp,0,sizeof(dp));
			dp[0]=1;
			for(int j=1;j<=n;j++)if(A[j]!=A[i])
			for(int k=0;k<=mx-A[j];k++)
			if(dp[k])dp[k+A[j]]=1;
			if(dp[A[i]])mark[i]=1;
		}
		for(int i=1;i<=n;i++)
		if(!mark[i]){
			if(cnt[A[i]]){
				cnt[A[i]]=0;
				ans++;
			}
		}
		Pt(ans);
		putchar('\n');
	}
}P2;
//1s 512M
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	Rd(cas);
	while(cas--){
		Rd(n);
		for(int i=1;i<=n;i++)Rd(A[i]);
		if(n==2)P1.solve();
		else P2.solve();
	}
	return 0;	
}
