#include <bits/stdc++.h>
using namespace std;

int t, n, a[109], tot, IG[25009], mm, x;

int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &t);
	while (t--)
	{
		scanf("%d", &n);
		mm=0;
		for (int i=1; i<=n; i++)
			scanf("%d", &a[i]), mm=max(mm, a[i]);
		sort(a+1, a+n+1), tot=0, IG[0]=1;
		for (int i=1; i<=mm; i++)
			IG[i]=0;
		for (int i=1; i<=n; i++)
		{
			if (!IG[a[i]]) 
			{
				tot++;
				for (int j=a[i]; j<=mm; j++)
					if (IG[j-a[i]]) IG[j]=1;
			}
		}
		printf("%d\n", tot);
	}
	return 0;
}
