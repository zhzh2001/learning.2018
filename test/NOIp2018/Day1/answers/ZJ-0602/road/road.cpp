#include<iostream>
#include<algorithm>
#include<cstdio>

using namespace std;

const int maxn=100005;

int n,road[maxn];

long long ans;

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	cin>>n;
	for(int i=0;i<n;i++)
		cin>>road[i];
	int deepest=-1;
	int lb=0,pos_dee,rb;
	while(lb<n)
	{
		pos_dee=lb;
		while(pos_dee<n&&road[pos_dee]<=road[pos_dee+1])
			pos_dee++;
		rb=pos_dee+1;
		while(rb<n&&road[rb]>=road[rb+1])
			rb++;
		if(lb!=0)
			ans+=road[pos_dee]-road[lb-1];
		else
			ans+=road[pos_dee];
		lb=rb+1;
	}
	cout<<ans;
	return 0;
}
