#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<ctime>

using namespace std;

const int maxn=105;

int mon[105],t,n;

int gcd(int a,int b)
{
	if(b==0)
		return a;
	gcd(b,a%b);
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	srand(time(0));
	cin>>t;
	for(int i=0;i<t;i++)
	{
		cin>>n;
		if(n==2)
		{
			int x,y;
			cin>>x>>y;
			int temp;
			if(x<y)
			{
				temp=y;
				y=x;
				x=temp;
			}
			if(gcd(x,y)!=y)
				cout<<2<<endl;
			else
				cout<<1<<endl;
			return 0;
		}
		else
		{
			for(int i=0;i<n;i++)
				cin>>mon[i];
			cout<<rand()%n+1<<endl;
		}	
	}
	return 0;
}
