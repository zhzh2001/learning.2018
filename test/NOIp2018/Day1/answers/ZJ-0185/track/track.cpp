#include<bits/stdc++.h>
using namespace std;
#define M 50005
template<class T>void Max(T &x,T y){if(x<y)x=y;}
template<class T>void Min(T &x,T y){if(x>y)x=y;}
struct List{
	int to,nxt,cost;	
}lt[M<<1];
int Hd[M],Esz[M],e_tot;
int n,m;
void Add_edge(int u,int v,int w){
	lt[++e_tot]=(List){v,Hd[u],w};
	Hd[u]=e_tot;Esz[u]++;
}
struct P1{//m=1;
	int Dx,ID;
	void dfs(int x,int f,int d){
		if(d>Dx){Dx=d;ID=x;};
		for(int i=Hd[x];i;i=lt[i].nxt){
			int y=lt[i].to,z=lt[i].cost;
			if(y==f)continue;
			dfs(y,x,d+z);
		}
	}
	void solve(){
		Dx=0;dfs(1,0,0);
		Dx=0;dfs(ID,0,0);
		printf("%d\n",Dx);
	}
}p1;
struct P2{//chain
	int sum[M];
	bool check(){
		for(int i=1;i<=n;i++)
			if(Esz[i]>2)return 0;
		return 1;
	}
	bool Jdge(int x){
		int now=0;
		for(int i=1;i<=m;i++){
			int pos=lower_bound(sum+now,sum+1+n,sum[now]+x)-sum;
			if(pos>n)return 0;
			now=pos;
		}
		return 1;
	}
	void Find(){
		int L=1,R=6e8,ans;
		while(L<=R){
			int mid=L+R>>1;
			if(Jdge(mid)){
				ans=mid;
				L=mid+1;	
			}else R=mid-1;
		}
		printf("%d\n",ans);
	}
	void dfs(int x){
		for(int i=Hd[x];i;i=lt[i].nxt){
			int y=lt[i].to,z=lt[i].cost;
			if(y==x-1)continue;
			sum[y]=sum[x]+z;
			dfs(y);	
		}
	}
	void solve(){
		dfs(1);
		Find();
	}	
}p2;
bool cmp(int a,int b){return a>b;}
struct P3{
	int val[M],cnt;
	void Init(){
		for(int i=Hd[1];i;i=lt[i].nxt)
			val[++cnt]=lt[i].cost;
	}
	void solve(){
		Init();
		sort(val+1,val+1+cnt,cmp);
		int j=m+1;
		int mi=1e9;
		for(int i=m;i>=1;i--){
			int x=val[i];
			if(j<=cnt)x+=val[j++];
			Min(mi,x);
		}
		printf("%d\n",mi);	
	}	
}p3;
int main(){//name memory long long * mod - 切分判断 最值计算 初值 极限数据 
//	printf("%.2lf\n",(sizeof(p1)+sizeof(p2)+sizeof(p3)+sizeof(lt))/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int x,y,z;
	bool flag=1;
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		Add_edge(x,y,z);
		Add_edge(y,x,z);
		if(x!=1)flag=0;
	}
	if(m==1)p1.solve();
	else if(p2.check())p2.solve();
	else if(flag)p3.solve();
	return 0;
}
