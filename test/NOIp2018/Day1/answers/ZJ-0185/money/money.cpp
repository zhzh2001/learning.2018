#include<bits/stdc++.h>
using namespace std;
int A[105],n;
struct P2{
	bool dp[30005];
	void solve(){
		memset(dp,0,sizeof(dp));
		int mxV=25000;
		sort(A+1,A+1+n);
		dp[0]=1;
		int ans=n;
		for(int i=1;i<=n;i++){
			if(dp[A[i]]){
				ans--;
				continue;	
			}
			for(int j=A[i];j<=mxV;j++)
				dp[j]|=dp[j-A[i]];
		}
		printf("%d\n",ans);
	}
}p2;
int main(){//name memory long long * mod - 切分判断 最值计算 初值 极限数据 
//	printf("%.2lf\n",(sizeof(p2))/1024.0/1024);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			scanf("%d",&A[i]);
		p2.solve();
	}
	return 0;
}
