/*Time: O((~n) * n log sum)*/
/*can use set to get faster*/
#include <cstdio>
#include <cstring>
#include <algorithm>
#define maxn 50005
#define maxe (maxn << 1)

using namespace std;

int n, m, ans, sum = 0, lim, v[maxn], c[maxn];
int lnk[maxn], nxt[maxe], son[maxe], w[maxe], tot = 0;
bool flg = true, vis[maxn];

inline int read()
{
	char ch = getchar();
	int ret = 0, f = 1;
	while (ch > '9' || ch < '0')
	{
		if (ch == '-')
			f = -f;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9')
		ret = ret * 10 + ch - '0', ch = getchar();
	return ret * f;
}

void adde(int x, int y, int z)
{
	son[++tot] = y, nxt[tot] = lnk[x], lnk[x] = tot, w[tot] = z;
}

bool check(int lm)
{
	int now = 0, ret = 0;
	for (int i = 1; i <= n; ++i)
	{
		if (now >= lm)
			ret++, now = 0;
		for (int j = lnk[i]; j; j = nxt[j])
		{
			if (son[j] < i)
				continue;
			now += w[j];
			break;
		}
	}
	return ret >= m;
}

int f[maxn];

void DFS(int now, int pre)
{
	for (int j = lnk[now]; j; j = nxt[j])
	{
		if (son[j] == pre)
			continue;
		DFS(son[j], now);
		ans = max(ans, f[now] + f[son[j]] + w[j]);
		f[now] = max(f[now], f[son[j]] + w[j]);
	}
}

int _iDFS(int now, int pre)
{
	int res = 0, len = 0, mx = 0;
	for (int j = lnk[now]; j; j = nxt[j])
	{
		if (son[j] == pre)
			continue;
		res += _iDFS(son[j], now);	
	}
	for (int j = lnk[now]; j; j = nxt[j])
	{
		if (son[j] == pre)
			continue;
		if (v[son[j]] + w[j] >= lim)
			res++;
		else
		{
			c[++len] = v[son[j]] + w[j];
			vis[len] = 0;
		}
	}
	sort(c + 1, c + 1 + len);
	for (int i = 1, j; i <= len; ++i)
	{
		if (vis[i])
			continue;
		for (j = i + 1; j <= len; ++j)
		{
			if ((!vis[j]) && c[j] + c[i] >= lim)
				break;
		}
		if (j <= len)
		{
			vis[i] = vis[j] = 1;
			res++;
		}
	}
	for (int i = len; i > 0; --i)
	{
		if (!vis[i])
		{
			mx = c[i];
			break;
		}
	}
	v[now] = mx;
	return res;
}

bool _icheck(int cfbhe)
{
	lim = cfbhe;
	return _iDFS(1, 0) >= m;
}

int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	n = read(), m = read();
	for (int i = 1; i < n; ++i)
	{
		int x = read(), y = read(), z = read();
		adde(x, y, z), adde(y, x, z);
		sum += z;
		if (x != y + 1 && y != x + 1)
			flg = false;
	}
	if (m == 1)
	{
		DFS(1, 0);
	}
	else if (flg)
	{
		int l = 0, r = sum;
		while (l <= r)
		{
			int mid = ((r - l) >> 1) + l;
			if (check(mid))
				ans = mid, l = mid + 1;
			else
				r = mid - 1;
		}
	}
	else
	{
		int l = 0, r = sum;
		while (l <= r)
		{
			int mid = ((r - l) >> 1) + l;
			if (_icheck(mid))
				ans = mid, l = mid + 1;
			else
				r = mid - 1;
		}
	}
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
