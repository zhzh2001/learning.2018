#include <cstdio>
#include <cstring>
#include <algorithm>
#define maxn 100005

using namespace std;

int n, a[maxn], top = -1;
long long ans = 0;

inline int read()
{
	char ch = getchar();
	int ret = 0, f = 1;
	while (ch > '9' || ch < '0')
	{
		if (ch == '-')
			f = -f;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9')
		ret = ret * 10 + ch - '0', ch = getchar();
	return ret * f;
}

int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	n = read();
	for (int i = 1; i <= n; ++i)
		a[i] = read();
	top = a[1];
	for (int i = 2; i <= n; ++i)
	{
		if (a[i] < top)
			ans += (long long)top - a[i];
		top = a[i];
	}
	ans += 1ll * top;
	printf("%lld\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}