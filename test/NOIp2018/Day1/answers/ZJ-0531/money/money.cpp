#include <cstdio>
#include <cstring>
#include <algorithm>
#define maxn 100005

using namespace std;

int n, a[maxn], vis[maxn * 10], t,mx,  top = -1;
long long ans = 0;

inline int read()
{
	char ch = getchar();
	int ret = 0, f = 1;
	while (ch > '9' || ch < '0')
	{
		if (ch == '-')
			f = -f;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9')
		ret = ret * 10 + ch - '0', ch = getchar();
	return ret * f;
}

int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	t = read();
	while (t--)
	{
		memset(vis, 0, sizeof(vis));
		n = read();
		ans = n;
		mx = -1;
		for (int i = 1; i <= n; ++i)
		{
			a[i] = read();
			mx = max(mx, a[i]);
		}
		vis[0] = 1;
		for (int j = 1; j <= mx; ++j)
		for (int i = 1; i <= n; ++i)
		{
			if (j < a[i])
				continue;
			if (vis[j - a[i]])
				vis[j]++;
		}
		for (int i = 1; i <= n; ++i)
		{
			if (vis[a[i]] > 1)
				ans--;
		}
		printf("%d\n", ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
