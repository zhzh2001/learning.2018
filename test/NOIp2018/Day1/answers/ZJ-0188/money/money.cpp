#include <bits/stdc++.h>
using namespace std;

#define FOR(i,n) for (int i=1;i<=n;i++)
#define REP(i,a,b) for (int i=a;i<=b;i++)

typedef long long ll;

const int N=25000;
/*
前3个点可以直接判断a,b是否为倍数关系 
*/

int n;
int a[N];
bool vis[N];
int mx;
int nowmx;
int ans;
ll x;
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--) {
		scanf("%d",&n);
		FOR(i,n) scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		//FOR(i,n) cout<<a[i]<<" ";
		//cout<<endl;
		mx=nowmx=ans=0;
		memset(vis,0,sizeof vis);
		FOR(i,n) mx=max(mx,a[i]);
		sort(a+1,a+1+n);
		if (a[1]==1) {
			printf("%d\n",1);
			continue;
		}
		FOR(i,n) {
			if (!vis[a[i]]) {
				++ans;
				vis[a[i]]=1;
				nowmx=max(nowmx,a[i]);
			} else continue;
			FOR(j,nowmx) {
				if (vis[j]) {
					FOR(k,mx) {
						x=j+(ll)k*a[i];
						if (x>mx) {
							break;
						}
						vis[x]=1;
						nowmx=max(nowmx,(int)x);
					}
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
