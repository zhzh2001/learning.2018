#include <bits/stdc++.h>
using namespace std;

#define FOR(i,n) for (int i=1;i<=n;i++)
#define REP(i,a,b) for (int i=a;i<=b;i++)

typedef long long ll;

const int N=50000;
/*
给定一棵大小为N的树，你可以选择M条边不交的路径，求你能够选择出的路径中最小长度的最大值 
*/ 
int n,m;
struct edge {
	int to,nxt;
	ll w;
} e[2*N];
int tot;
int head[N];


int now;
ll val;
ll ans;

void insert(int x,int y,int w) {
	e[++tot].to=y,e[++tot].w=w,e[++tot].nxt=head[x],head[x]=tot;
	e[++tot].to=x,e[++tot].w=w,e[++tot].nxt=head[y],head[y]=tot;
}
void dfs(int x,int fa,ll d) {
	if (d>val) {
		now=x;
		val=d;
	}
	int y=0;
	for (int i=head[x];i;i=e[i].nxt) {
		y=e[i].to;
		if (y!=fa) {
			dfs(y,x,d+e[i].w);
		}
	}
}
int a[N];
ll s[N];
int f[N][2];
void dp() {
	
	FOR(i,n-1) {
		a[i]=e[head[i]].w;
		s[i]=s[i-1]+a[i];
	}
	memset(f,-1,sizeof f);
	for (int i=n-1;i>=1;i--) {
		FOR(j,m) {
			if (j==1) {
				f[i][j&1]=a[i];
			} else {
				REP(k,i,n-1) {
					f[i][j&1]=max(f[i][j&1],min(f[k+1][(j-1)&1],s[k]-s[i-1]));
				}
			}
		}
	}
	ans=f[1][m&1];
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout); 
	
	scanf("%d %d",&n,&m);
	int x,y,w;
	bool ok=1;
	FOR(i,n-1) {
		scanf("%d %d %d",&x,&y,&w);
		if (x>y) swap(x,y);
		if (x+1!=y) ok=0;
		insert(x,y,w);
	}
	if (m==1) {
		dfs(1,0,0);
		int t=now;
		now=val=0;
		dfs(t,0,0);
		printf("%lld\n",val);
	} else {
		if (ok) {
			dp();
			printf("%lld\n",ans);
		} else {
			
		}
	}
	return 0;
}
