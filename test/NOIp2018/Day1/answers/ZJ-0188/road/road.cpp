#include <bits/stdc++.h>
using namespace std;

#define FOR(i,n) for (int i=1;i<=n;i++)
#define REP(i,a,b) for (int i=a;i<=b;i++)

typedef long long ll;

const int inf=0x3f3f3f3f;
const int N=100000+10;
/*
给定N个数字（范围在[0,10^4]内）
每次可以选择一个区间（要求里面没有0），将里面所有数字减一
问最少的操作次数使得所有数字变为0 

假设第一个数字=0，那么就不需要管它，转化为n-1的问题
如果第一个数字=x>0，那么我们必须有x个区间覆盖它，尽可能区间更长 

那么如果第一个数字x>0，我们从它出发一直延长到下一个0的前面一个数字（如果没有0就一直到末尾）
把其中的数字全部减去1，ans++

只要一次性减去其中最小数，ans+=这个最小数
那么每次就可以使得一个数字变为0

只要实现，区间找最小值，找下一个0
下一个0可以开一个线段树，如果有0则为1，否则为0 

树状数组好了，可以O(NlogN^2)
 
数字相同的情况，考虑d很小，开个数组存相同数字的位置 

算了。。直接二分，如果最小值为0，那么就表示该区间中有0

seg保存区间最值 
*/
int n;
int a[N];
int seg[4*N],tag[4*N];
int ans;
void build(int k,int l,int r) {
	if (l==r) {
		seg[k]=a[l];
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);
	build(k<<1|1,mid+1,r);
	seg[k]=min(seg[k<<1],seg[k<<1|1]);
}
void down(int k,int l,int r) {
	if (l==r) {
		tag[k]=0;
		return;
	}
	seg[k<<1]+=tag[k];
	seg[k<<1|1]+=tag[k];
	tag[k<<1]+=tag[k];
	tag[k<<1|1]+=tag[k];
	tag[k]=0;
}
void add(int k,int L,int R,int l,int r,int v) {
	down(k,L,R);
	if (l==L&&r==R) {
		seg[k]+=v;
		tag[k]+=v;
		return;
	}
	int mid=(L+R)>>1;
	if (r<=mid) {
		add(k<<1,L,mid,l,r,v);
	} else if (l>mid) {
		add(k<<1|1,mid+1,R,l,r,v);
	} else {
		add(k<<1,L,mid,l,mid,v);
		add(k<<1|1,mid+1,R,mid+1,r,v);
	}
}
int query(int k,int L,int R,int l,int r) {
	down(k,L,R);
	if (l==L&&r==R) return seg[k];
	int mid=(L+R)>>1;
	if (r<=mid) return query(k<<1,L,mid,l,r);
	else if (l>mid) return query(k<<1|1,mid+1,R,l,r);
	else {
		return min(query(k<<1,L,mid,l,mid),query(k<<1|1,mid+1,R,mid+1,r));
	}
}
int nxt(int x) {
	int l=x,r=n;
	int mid,pos=0;
	while (l<=r) {
		mid=(l+r)>>1;
		int t=query(1,1,n,x,mid);
		if (t==0) {
			r=mid-1;
		} else {
			pos=mid;
			l=mid+1;
		}
	}
	return pos;
}
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	FOR(i,n) scanf("%d",&a[i]);
	build(1,1,n);
	int p=0,t=0;
	int now=1;
	
	while (1) {
		if (now>n) break;
		if (query(1,1,n,now,now)==0) {
			now++;
			continue;
		}
		p=nxt(now);
		
		
		//cout<<now<<" "<<p<<endl;
		
		//FOR(j,n) cout<<query(1,1,n,j,j)<<" ";
		//cout<<endl;
		
		
		t=query(1,1,n,now,p);
		ans+=t;
		add(1,1,n,now,p,-t);
	}
	printf("%d\n",ans);
	return 0;
}
