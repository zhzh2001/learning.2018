#include<cstdio>
#include<algorithm>
using namespace std;
int T,n,Ans,a[105],tot;bool f[25005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);Ans=0;
		for(int i=1;i<=n;i++){
			scanf("%d",&a[i]);
			if(a[i]>tot) tot=a[i];
		}
		sort(a+1,a+1+n);f[0]=1;
		for(int i=1;i<=n;i++)
		if(f[a[i]]==0){
			Ans++;
			for(int j=a[i];j<=tot;j++) f[j]|=f[j-a[i]];
		}
		for(int i=0;i<=tot;i++) f[i]=0;
		printf("%d\n",Ans);
	}
	return 0;
}
