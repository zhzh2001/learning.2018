#include<cstdio>
#include<algorithm>
using namespace std;
const int MAXN=50005;
int n,K,Ans,Le,f[MAXN],fa_[MAXN],S[MAXN],Stk[MAXN],qi_[MAXN],que[MAXN],Top;bool vis[MAXN],tt;
struct Edge{
	int tot,lnk[MAXN],son[MAXN<<1],nxt[MAXN<<1],W[MAXN<<1];
	void Add(int x,int y,int z){nxt[++tot]=lnk[x];son[tot]=y;W[tot]=z;lnk[x]=tot;}
}E;
int read(){
	int ret=0;char ch=getchar();bool f=1;
	for(;ch<'0'||'9'<ch;ch=getchar()) f^=!(ch^'-');
	for(;'0'<=ch&&ch<='9';ch=getchar()) ret=ret*10+ch-48;
	return f?ret:-ret;
}
int get(int x){return fa_[x]==x?x:fa_[x]=get(fa_[x]);}
void DFS(int x,int fa){
	f[x]=0,S[x]=0,qi_[x]=Top;
	for(int j=E.lnk[x];j;j=E.nxt[j])
	if(E.son[j]!=fa){
		DFS(E.son[j],x);f[x]+=f[E.son[j]];
		if(S[E.son[j]]+E.W[j]>=Le) f[x]++;
		else Stk[++Top]=S[E.son[j]]+E.W[j];
	}
	int m=0;
	while(Top>qi_[x]) que[++m]=Stk[Top--];
	sort(que+1,que+1+m);
	for(int i=1;i<=m+1;i++) fa_[i]=i;
	for(int i=1;i<=m;i++)
	if(!vis[i]){
		int L=i+1,R=m,j=-1;
		for(int mid=(R+L)>>1;L<=R;mid=(R+L)>>1)
		if(que[mid]+que[i]>=Le) R=mid-1,j=mid;else L=mid+1;
		if(j==-1){S[x]=que[i];continue;}
		if(vis[j]) j=get(j);
		if(j>m){S[x]=que[i];continue;}
		vis[j]=1,fa_[j]=j+1,f[x]++;
	}
	for(int i=0;i<=m;i++) vis[i]=0;
}
bool chck(int mid){Top=0;Le=mid;DFS(1,0);return f[1]>=K;}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),K=read();
	for(int i=1;i<n;i++){int x=read(),y=read(),z=read();E.Add(x,y,z),E.Add(y,x,z);}
	int L=0,R=1000000000;
	for(int mid=((R-L)>>1)+L;L<=R;mid=((R-L)>>1)+L)
	if(chck(mid)) Ans=mid,L=mid+1;else R=mid-1;
	printf("%d\n",Ans);
	return 0;
}
