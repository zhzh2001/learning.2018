#include<cstdio>
#include<algorithm>
using namespace std;
const int MAXN=100005;
typedef long long LL;
int n,vis[MAXN];
LL Ans,K;
struct xcw{
	int x,P;
	bool operator <(const xcw b)const{return x<b.x||(x==b.x&&P<b.P);}
}a[MAXN];
int read(){
	int ret=0;char ch=getchar();bool f=1;
	for(;ch<'0'||'9'<ch;ch=getchar()) f^=!(ch^'-');
	for(;'0'<=ch&&ch<='9';ch=getchar()) ret=ret*10+ch-48;
	return f?ret:-ret;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) a[i]=(xcw){read(),i};
	sort(a+1,a+1+n);K=1;vis[0]=vis[n+1]=1;
	for(int i=1,lst=0;i<=n;i++){
		Ans+=K*(a[i].x-lst);lst=a[i].x;
		K+=1-vis[a[i].P-1]-vis[a[i].P+1];
		vis[a[i].P]=1;
	}
	printf("%lld\n",Ans);
	return 0;
}
