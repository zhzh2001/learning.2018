#include<bits/stdc++.h>
#define debug(x) cerr<<"\t DEBUG: "<<#x<<" = "<<(x)<<endl
using namespace std;
const int maxn=5e4+5;
inline void tomax(int &a,int b){if(a<b)a=b;}
int n,m,x,y,z,num,cnt;
int tot,head[maxn],to[maxn<<1],nxt[maxn<<1],w[maxn<<1];
void add_edge(int u,int v,int c){
	to[++tot]=v;
	nxt[tot]=head[u];
	w[tot]=c;
	head[u]=tot;
}
int val[maxn],stk[maxn],top;
int check2(int l,int r,int x){
	static int tmp[maxn],id;
	id=0;
	for(int i=l;i<=r;i++){
		if(i==x)continue;
		tmp[++id]=stk[i];
	}
	if(id==0)return 0;
	int res=0;
	int i=1,j=id;
	while(tmp[i]>=num && i<=id)res++,i++;
	while(i<j){
		while(tmp[i]+tmp[j]<num && i<j)j--;
		if(i==j)break;
		res++;
		i++;
		j--;
	}
	return res;
}
int check(int l,int r,int x){
	if(l>r)return 0;
	sort(stk+l,stk+r+1,greater<int>());
	int Maxcnt=check2(l,r,r+1),Maxnum=0;
//if(num==3){
//	debug(Maxcnt);
//	debug(l);
//	debug(r);
//}
//debug(l);
//debug(r);
//debug(Maxcnt);
//debug(Maxnum);
//for(int i=l;i<=r;i++)
//	cout<<stk[i]<<" ";
//cout<<endl;
	for(int i=l;i<=r;i++){
		int tmp=check2(l,r,i);
		if(tmp>Maxcnt){
			Maxcnt=tmp;
			Maxnum=stk[i];
		}else if(tmp==Maxcnt && Maxnum<stk[i])
			Maxnum=stk[i];
	}
//	if(x==3){
//		for(int i=l;i<=r;i++)
//			cout<<stk[i]<<" ";
//		debug(num);
//		cout<<Maxcnt<<" "<<Maxnum<<endl;
//	}
	cnt+=Maxcnt;
	return Maxnum;
}
void dfs(int f,int x){
	int last=top;
	for(int i=head[x];i;i=nxt[i]){
		int y=to[i];
		if(y==f)continue;
		dfs(x,y);
		stk[++top]=val[y]+w[i];
	}
	val[x]=check(last+1,top,x);
//	cout<<x<<" "<<cnt<<endl;
	top=last;
}
bool isok(int x){
	cnt=0;
	num=x;
	dfs(0,1);
//	debug(cnt);
//if(x==3){
//	for(int i=1;i<=n;i++)
//		cout<<val[i]<<" ";
//	debug(cnt);
//}
	return cnt>=m;
}
struct M{
	int Mxd,D,dis[maxn];
	void dfs(int f,int x){
		if(dis[x]>Mxd)Mxd=dis[x],D=x;
		for(int i=head[x];i;i=nxt[i]){
			int y=to[i];
			if(y==f)continue;
			dis[y]=dis[x]+w[i];
			dfs(x,y);
		}
	}
	void solve(){
		dfs(0,1);
		dis[D]=0;
		dfs(0,D);
		cout<<dis[D]<<endl;
		exit(0);
	}
}p;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		add_edge(x,y,z);
		add_edge(y,x,z);
	}
	if(m==1 && n>=30000){
		p.solve();
		return 0;
	}
//	cout<<isok(160)<<endl;
	int l=1,r=5e8,mid,ans;
	while(l<=r){
		mid=(l+r)>>1;
		if(isok(mid)){
			ans=mid;
			l=mid+1;
		}else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
/*
5 2
1 2 4
1 3 5
1 4 6
1 5 4
*/
