#include<bits/stdc++.h>
using namespace std;
const int maxn=1e5+5;
int n,ans,A[maxn],mn[maxn][18],Log2[maxn];
int chkmin(int x,int y){
	if(A[x]<A[y])return x;
	return y;
}
void init(){
	for(int i=2;i<maxn;i++)
		Log2[i]=Log2[i>>1]+1;
	for(int i=1;i<18;i++)
		for(int j=1;j+(1<<i-1)<=n;j++)
			mn[j][i]=chkmin(mn[j][i-1],mn[j+(1<<i-1)][i-1]);
}
int Query(int l,int r){
	int k=Log2[r-l+1];
	return chkmin(mn[l][k],mn[r-(1<<k)+1][k]);
}
void dfs(int l,int r,int now){
	if(l>r)return;
	int nxt=Query(l,r);
	ans+=A[nxt]-now;
	dfs(l,nxt-1,A[nxt]);
	dfs(nxt+1,r,A[nxt]);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	cin>>n;
	for(int i=1;i<=n;i++){
		scanf("%d",&A[i]);
		mn[i][0]=i;
	}
	init();
	dfs(1,n,0);
	cout<<ans<<endl;
	return 0;
}
