#include<bits/stdc++.h>
using namespace std;
const int maxn=1005,maxnum=25005;
int QuQ,n,ans,A[maxn];
bool mark[maxnum];
bool check(int x){
	if(mark[x])return false;
	for(int i=x;i<maxnum;i++)
		mark[i]|=mark[i-x];
	return true;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	cin>>QuQ;
	while(QuQ--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			scanf("%d",&A[i]);
		sort(A+1,A+1+n);
		memset(mark,0,sizeof(mark));
		mark[0]=1;
		ans=0;
		for(int i=1;i<=n;i++)
			if(check(A[i]))ans++;
		printf("%d\n",ans);
	}
	return 0;
}
