#include<cstdio>
#include<algorithm>
#include<iostream>
using namespace std;
#define ll long long
#define mn 500005
ll n,m,h[mn],x,y,z,k,s,d[mn],b[mn],qs;
struct node
{
	ll v,n,c;
}e[mn*2];
void add(ll u,ll v,ll c)
{
	e[++s].v=v,e[s].c=c,e[s].n=h[u],h[u]=s;
	e[++s].v=u,e[s].c=c,e[s].n=h[v],h[v]=s;
}
void dfs(ll u,ll fa)
{
	for(ll i=h[u];i;i=e[i].n)
		if(e[i].v!=fa)
			dfs(e[i].v,u);
	s=0;	
	for(ll i=h[u];i;i=e[i].n)
		if(e[i].v!=fa)
			b[++s]=d[e[i].v]+e[i].c;
	ll ma=0,l=0,r=s,mid,kk,i,j,k1,k2;
	sort(b+1,b+s+1);
	j=s;
	while(j&&b[j]>=k)
		ma++,j--;
	for(i=1;i<j;)
		if(b[i]+b[j]>=k)
			ma++,i++,j--;
		else
			i++;
	qs+=ma;
	while(l<r)
	{
		mid=((l+r)>>1)+1,kk=0,k1=1,k2=s;
		if(mid==1)	k1=2;
		if(mid==s)	k2=s-1;
		j=k2;
		while(j&&b[j]>=k)
		{
			kk++,j--;
			if(j==mid)	j--;
		}
		for(i=k1;i<j;)
		{
			if(b[i]+b[j]>=k)
			{
				kk++,i++,j--;
				if(i==mid)	i++;
				if(j==mid)	j--;
			}
			else
			{
				i++;
				if(i==mid)	i++;
			}
		}
		if(kk>=ma)	l=mid;
		else	r=mid-1;
	}
	d[u]=b[l];
}
ll q()
{
	qs=0;
	dfs(1,0);
	return qs;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	ll l,i,r,mid;
	scanf("%lld%lld",&n,&m);
	for(i=1;i<n;i++)
	{
		scanf("%lld%lld%lld",&x,&y,&z);
		add(x,y,z);
	}
	l=0,r=500000000;
	while(l<r)
	{
		k=mid=((l+r)>>1)+1;
		if(q()>=m)
			l=mid;
		else
			r=mid-1;
	}
	printf("%lld",l);
	return 0;
}
