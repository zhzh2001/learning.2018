#include<cstdio>
#include<algorithm>
#include<iostream>
using namespace std;
#define ll long long
#define mn 25005
ll n,f[mn],a[mn],i,j,ans,t;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%lld",&t);f[0]=1;
	while(t--)
	{
		ans=0;
		for(i=1;i<mn;i++)		f[i]=0;
		scanf("%lld",&n);
		for(i=1;i<=n;i++)	scanf("%lld",&a[i]);
		sort(a+1,a+n+1);
		for(i=1;i<=n;i++)
		if(!f[a[i]])
		{
			ans++;
			for(j=a[i];j<mn;j++)
				f[j]|=f[j-a[i]];
		}
		printf("%lld\n",ans);
	}
	return 0;
}
