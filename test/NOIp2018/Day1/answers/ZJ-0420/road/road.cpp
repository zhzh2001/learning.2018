#include<cstdio>
#include<cstdlib>
#include<new>
#include<algorithm>
using namespace std;
struct keng{
	int p,d;
}a[100050];
int cmp(keng a,keng b){
	return a.d<b.d;
}
int v[100050],lans,rans;
struct Node{
	int p,x,r;
	Node *ch[2];
	Node(int p,int x):p(p),x(x){r=rand();ch[0]=ch[1]=NULL;}
}*rt=NULL;
inline void Rotate(Node* &o,int d){
	Node *k=o->ch[d^1];
	o->ch[d^1]=k->ch[d];k->ch[d]=o;o=k;
}
void Ins(Node* &o,int p,int x){
	if(o==NULL)o=new Node(p,x);
	else{
		int d=p<o->p?0:1;
		Ins(o->ch[d],p,x);
		if(o->ch[d]->r>o->r)Rotate(o,d^1);
	}
}
void asklst(Node *o,int p){
	if(o==NULL)return;
	else{
		int d=p<o->p?0:1;
		if(o->p<p&&o->p>lans)lans=o->p;
		asklst(o->ch[d],p);
	}
}
void asknxt(Node *o,int p){
	if(o==NULL)return;
	else{
		int d=p<o->p?0:1;
		if(o->p>p&&o->p<rans)rans=o->p;
		asknxt(o->ch[d],p);
	}
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n,i,ans=0;
	scanf("%d",&n);
	for(i=1;i<=n;i++){
		scanf("%d",&v[i]);
		a[i].p=i;a[i].d=v[i];
	}
	sort(a+1,a+n+1,cmp);
	v[0]=v[n+1]=0;
	for(i=1;i<=n;i++){
		lans=0;rans=n+1;
		asklst(rt,a[i].p);asknxt(rt,a[i].p);
		ans+=a[i].d-max(v[lans],v[rans]);
		Ins(rt,a[i].p,a[i].d);
	}
	printf("%d",ans);
	return 0;
}
