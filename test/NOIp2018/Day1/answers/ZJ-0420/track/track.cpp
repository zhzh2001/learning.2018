#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=50005;
int G[N],to[N<<1],dis[N<<1],nxt[N<<1],sz=-1,n,m;
int f1[N],ans1=0;
int t3[N],f3[N],g3[N];
bool use3[N];
inline void add(int u,int v,int d){
	to[++sz]=v;nxt[sz]=G[u];G[u]=sz;
	to[++sz]=u;nxt[sz]=G[v];G[v]=sz;
	dis[sz^1]=dis[sz]=d;
}
void dfs1(int u,int fa){
	int i,v,t,max1=0,max2=0;
	for(i=G[u];i!=-1;i=nxt[i])if((v=to[i])!=fa){
		dfs1(v,u);t=f1[v]+dis[i];
		if(t>max1){max2=max1;max1=t;}
		else if(t>max2)max2=t;
	}
	if(max1+max2>ans1)ans1=max1+max2;
	f1[u]=max1;
}
inline void Main1(){
	dfs1(1,0);
	printf("%d",ans1);
}
inline bool check2(int k){
	int i,tmp=0,cnt=0;
	for(i=0;i<n-1&&cnt<m;i++){
		tmp+=dis[i<<1];
		if(tmp>=k){tmp=0;cnt++;}
	}
	return cnt==m;
}
inline void Main2(){
	int i,l=1e9,r=0,mid;
	for(i=0;i<n-1;i++){
		if(dis[i<<1]<l)l=dis[i<<1];
		r+=dis[i<<1];
	}
	r/=m;
	while(l<r){
		mid=(l+r+1)>>1;
		if(check2(mid))l=mid;
		else r=mid-1;
	}
	printf("%d",l);
}
inline int erfen(int x,int l,int r){
	int m;
	while(l<r){
		m=(l+r)>>1;
		if(t3[m]>=x)r=m;
		else l=m+1;
	}
	return t3[l]>=x?l:l+1;
}
bool dfs3(int u,int fa,int k){
	int i,j,v,cnt=0;
	for(i=G[u];i!=-1&&f3[u]<m;i=nxt[i])if((v=to[i])!=fa){
		if(dfs3(v,u,k))return 1;
		f3[u]+=f3[v];
	}
	for(i=G[u];i!=-1&&f3[u]<m;i=nxt[i])if((v=to[i])!=fa){
		if(g3[v]+dis[i]>=k)f3[u]++;
		else{
			t3[++cnt]=g3[v]+dis[i];
			use3[cnt]=0;
		}
	}
	if(f3[u]==m)return 1;
	sort(t3+1,t3+cnt+1);
	for(i=1;i<cnt;i++)if(!use3[i]){
		j=erfen(k-t3[i],i+1,cnt);
		while(j<=cnt&&use3[j])j++;
		if(j<=cnt){
			if(++f3[u]==m)return 1;
			use3[i]=use3[j]=1;
		}
	}
	for(i=cnt;i;i--)if(!use3[i]){
		g3[u]=t3[i];
		break;
	}
	return 0;
}
inline bool check3(int k){
	memset(f3,0,sizeof(f3));memset(g3,0,sizeof(g3));
	return dfs3(1,0,k);
}
inline void Main3(){
	int i,l=1e9,r=0,mid;
	for(i=0;i<n-1;i++){
		if(dis[i<<1]<l)l=dis[i<<1];
		r+=dis[i<<1];
	}
	r/=m;
	while(l<r){
		mid=(l+r+1)>>1;
		if(check3(mid))l=mid;
		else r=mid-1;
	}
	printf("%d",l);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int i,u,v,w;
	bool ok=1;
	scanf("%d%d",&n,&m);
	memset(G,-1,sizeof(G));
	for(i=1;i<n;i++){
		scanf("%d%d%d",&u,&v,&w);
		add(u,v,w);if(v-u!=1)ok=0;
	}
	if(m==1)Main1();
	else if(ok)Main2();
	else Main3();
	return 0;
}
