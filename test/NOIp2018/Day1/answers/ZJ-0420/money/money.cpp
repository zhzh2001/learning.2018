#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int a[105];
bool f[25005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T,n,i,j,maxn,ans;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);maxn=0;
		for(i=0;i<n;i++){
			scanf("%d",&a[i]);
			if(a[i]>maxn)maxn=a[i];
		}
		sort(a,a+n);
		memset(f,0,sizeof(f));
		f[0]=1;ans=0;
		for(i=0;i<n;i++)if(!f[a[i]]){
			ans++;
			for(j=a[i];j<=maxn;j++)f[j]|=f[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
