#include<cstdio>
#include<cstring>
#include<algorithm>
#include<iostream>
#include<bitset>
#define rd read()
using namespace std;
typedef bitset<1005> bit;

const int N = 105;
const int M = 1e3 + 5;

int n, T, a[N], ans, maxn, f[M];

int read() {
	int X = 0, p = 1; char c = getchar();
	for (; c > '9' || c < '0'; c = getchar())
		if (c == '-') p = -1;
	for (; c >= '0' && c <= '9'; c = getchar())
		X = X * 10 + c - '0';
	return X * p;
} 

void up(int &A, int B) {
	if (A < B) A = B;
} 

void solve(int x) {
	memset(f, 0, sizeof(f));
	f[0] = 1;
	for (int i = 1; i <= n; ++i) if (i != x) 
		for (int j = 0; j <= a[x] && j + a[i] <= a[x]; ++j) if (f[j])
			f[j + a[i]] = 1;
	if (f[a[x]]) ans--;
} 

int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	T = rd;
	for (; T; --T) {
		n = rd;
		maxn = 0;
		for (int i = 1; i <= n; ++i) 
			up(maxn, a[i] = rd);
		sort(a + 1, a + 1 + n);
		ans = n = unique(a + 1, a + 1 + n) - a - 1;
		for (int i = 1; i <= n; ++i)
			solve(i);
		printf("%d\n", ans);
	} 
} 
