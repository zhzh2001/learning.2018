#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#define rd read()
using namespace std;

const int N = 5e4 + 5, inf = 5e8 + 5;;

int n, m, f[N], a[N], used[N];

struct node {
	int x, w;
}t[N << 1]; 

vector<node> to[N];

int read() {
	int X = 0, p = 1; char c = getchar();
	for (; c > '9' || c < '0'; c = getchar())
		if (c == '-') p = -1;
	for (; c >= '0' && c <= '9'; c = getchar())
		X = X * 10 + c - '0';
	return X * p;
} 

int cmp(const node &A, const node &B) {
	if (a[A.x] != a[B.x]) 
		return a[A.x] < a[B.x];
	else return A.x < B.x;
} 

void dfs(int u, int fa, int low) {
	int up = to[u].size(), fr = u == 1 ? 0 : 1;
	for (int i = 0; i < up; ++i) {
		int nt = to[u][i].x;
		if (nt == fa) continue;
		dfs(nt, u, low);
		a[nt] += to[u][i].w;
	} 
	sort(to[u].begin(), to[u].end(), cmp);
	for (int i = fr; i < up; ++i)
		f[u] += f[to[u][i].x];
	
	for (int i = up - 1; i >= fr; --i)
		if (a[i] >= low) f[u]++, up--;
		else break;

	if (fr == 0 && up - fr == 3) {
		if (a[to[u][0].x] + a[to[u][1].x] >= low) {
			f[u]++; a[u] = a[to[u][2].x];
			return;
		} 
		else if (a[to[u][0].x] + a[to[u][2].x] >= low) {
			f[u]++; a[u] = a[to[u][1].x];
			return;
		} 
		else if (a[to[u][1].x] + a[to[u][2].x] >= low) {
			f[u]++; a[u] = a[to[u][0].x];
			return;
		} 
	} 
	
	if (up - fr >= 2) {
		if (a[to[u][fr].x] + a[to[u][fr + 1].x] >= low) {
			f[u]++; return;
		} 
	} 

	for (int i = up - 1; i >= fr; --i)
		if (!used[to[u][i].x]) {
			a[u] = a[to[u][i].x];
			break;
		} 
} 

int  check(int x) {
	memset(a, 0, sizeof(a));
	memset(f, 0, sizeof(f));
	memset(used, 0, sizeof(used));
	dfs(1, 0, x);
	if (a[1] >= x) f[1]++;
	if (f[1] >= m) return 1;
	else return 0;
} 

int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	n = rd; m = rd;
	for (int i = 1; i < n; ++i) {
		int u = rd, v = rd, w = rd;
		node tmp;
		tmp.x = v; tmp.w = w;
		to[u].push_back(tmp);
		tmp.x = u; tmp.w = w;
		to[v].push_back(tmp);
	} 
	int l = 0, r = inf, ans;
	for (; l <= r;) {
		int mid = (l + r) >> 1;
		if (check(mid)) ans = mid, l = mid + 1;
		else r = mid - 1;
	} 
	printf("%d\n", ans);
} 
