#include<cstdio>
#include<cstring>
#include<algorithm>
#define rd read()
using namespace std;
typedef pair<int, int> P;

const int N = 1e5 + 5, inf = 1e6;

int a[N], ans, n;

inline int read() {
	int X = 0, p = 1; char c = getchar();
	for (; c > '9' || c < '0'; c = getchar())
		if (c == '-') p = -1;
	for (; c >= '0' && c <= '9'; c = getchar())
		X = X * 10 + c - '0';
	return X * p;
} 

inline void down(P &A, P B) {
	if (A > B) A = B;
} 

namespace SegT {
	P Min[N << 2];
#define lson x << 1
#define rson x << 1 | 1
#define mid ((l + r) >> 1)
	
	inline void up(int x) {
		if (Min[lson] > Min[rson])
			Min[x] = Min[rson];
		else Min[x] = Min[lson];
	} 

	void build(int l, int r, int x) {
		if (l == r) {
			Min[x] = P(a[l], l);
			return;
		} 
		build(l, mid, lson);
		build(mid + 1, r, rson);
		up(x);
	} 

	P query(int L, int R, int l, int r, int x) {
		if (L <= l && r <= R)
			return Min[x];
		P res = P(inf, inf);
		if (mid >= L)
			down(res, query(L, R, l, mid, lson));
		if (mid < R)
			down(res, query(L, R, mid + 1, r, rson));
		return res;
	} 
}using namespace SegT; 

void solve(int l, int r, int sum) {
	if (l > r) return;
	P tmp = query(l, r, 1, n, 1);
	ans += tmp.first - sum;
	sum += tmp.first - sum;
	solve(l, tmp.second - 1, sum);
	solve(tmp.second + 1, r, sum);
} 

int main() 
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	n = rd;
	for (register int i = 1; i <= n; ++i)
		a[i] = rd;
	build(1, n, 1);
	solve(1, n, 0);
	printf("%d\n", ans);
} 
