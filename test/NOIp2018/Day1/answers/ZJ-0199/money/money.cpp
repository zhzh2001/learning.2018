#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int T,n,ans,M,x,a[25010],f[25010];
char ch;
void read(int &digit)
{
	digit=0;
	ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9')
	{
		digit=digit*10+ch-'0';
		ch=getchar();
	}
}
void wri(int x)
{
	if (x>9) wri(x/10);
	putchar('0'+x%10);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	while(T--)
	{
		read(n);
		memset(f,0,sizeof(f));
		f[0]=1;
		ans=0;
		M=0;
		for (int i=1;i<=n;i++) read(a[i]);
		sort(a+1,a+n+1);
		M=a[n];
		for (int i=1;i<=n;i++)
		{
			x=a[i];
			if (f[x]==1) continue;
			for (int j=0;j<=M-x;j++)
			if (f[j]) f[j+x]=1;
			ans++;
		}
		wri(ans);
		putchar('\n');
	}
	return 0;
}
