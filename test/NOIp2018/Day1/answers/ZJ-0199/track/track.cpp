#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
struct node{int x,y,z;} e[50010];
struct node2{int s,F;} tmp1[1010],tmp2[1010];
int g[1010],Len1,Len2,Now,M,son[1010][2],dis[1010][2];
int n,m,ty1,ty2,l,r,mid,L,R,cnt,s,len,G,c[1010],root;
int k,first[100010],last[100010],ne[100010],b[100010],a[100010];
char ch;
void read(int &digit)
{
	digit=0;
	ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9')
	{
		digit=digit*10+ch-'0';
		ch=getchar();
	}
}
void wri(int x)
{
	if (x>9) wri(x/10);
	putchar('0'+x%10);
}
bool cmp(node a,node b) {return(a.x<b.x);}
bool cmp2(node a,node b) {return(a.z<b.z);}
void add(int x,int y,int z)
{
	k++;
	a[k]=y;b[k]=z;
	if (first[x]==0) first[x]=k;
	else ne[last[x]]=k;
	last[x]=k;
}
void dfs(int x,int f,int s)
{
	if (s>len) {len=s;G=x;}
	for (int i=first[x];i;i=ne[i])
	if (a[i]!=f) dfs(a[i],x,s+b[i]);
}
void build(int x,int f)
{
	int t=0;
	for (int i=first[x];i;i=ne[i])
	if (a[i]!=f)
	{
		build(a[i],x);
		son[x][t]=a[i];
		dis[x][t]=b[i];
		t++;
	}
}
void dfs3(int x,int s,int F)
{
	for (int i=0;i<=1;i++)
	if (son[x][i]!=0) dfs3(son[x][i],s+dis[x][i],F+g[son[x][i^1]]);
	Len1++;
	tmp1[Len1].s=s;
	tmp1[Len1].F=F+g[x];
}
void dfs4(int x,int s,int F)
{
	for (int i=0;i<=1;i++)
	if (son[x][i]!=0) dfs4(son[x][i],s+dis[x][i],F+g[son[x][i^1]]);
	Len2++;
	tmp2[Len2].s=s;
	tmp2[Len2].F=F+g[x];
}
bool cmptmp(node2 a,node2 b) {return(a.s<b.s);}
void dfs2(int x)
{
	for (int i=0;i<=1;i++)
	if (son[x][i]!=0) dfs2(son[x][i]);
	Len1=1;Len2=1;
	tmp1[1].s=0;
	tmp1[1].F=g[son[x][0]];
	tmp2[1].s=0;
	tmp2[1].F=g[son[x][1]];
	if (son[x][0]!=0) dfs3(son[x][0],dis[x][0],0);
	if (son[x][1]!=0) dfs4(son[x][1],dis[x][1],0);
	sort(tmp1+1,tmp1+Len1+1,cmptmp);
	sort(tmp2+1,tmp2+Len2+1,cmptmp);
	g[x]=g[son[x][1]]+g[son[x][0]];
	Now=Len2;M=0;
	for (int i=1;i<=Len1;i++)
	{
		while(Now>=1&&tmp2[Now].s+tmp1[i].s>=mid) {M=max(M,tmp2[Now].F);Now--;}
		if (Now<Len2) g[x]=max(g[x],tmp1[i].F+M+1);
	}	
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);
	ty1=1;ty2=1;
	for (int i=1;i<n;i++)
	{
		read(e[i].x);read(e[i].y);read(e[i].z);
		if (e[i].x!=1) ty1=0;
		if (e[i].y!=e[i].x+1) ty2=0;
		add(e[i].x,e[i].y,e[i].z);
		add(e[i].y,e[i].x,e[i].z);
	}
	if (m==1)
	{
		G=0;len=0;
		dfs(1,0,0);
		len=0;
		dfs(G,0,0);
		wri(len);
	}
	else if (ty2==1)
	{
		sort(e+1,e+n,cmp);
		l=1;r=500000000;
		while(l<r)
		{
			mid=(l+r)/2;
			cnt=0;s=0;
			for (int i=1;i<n;i++)
			{
				s=s+e[i].z;
				if (s>=mid) {s=0;cnt++;}
			}
			if (cnt<m) r=mid;
			else l=mid+1;
		}
		wri(l-1);
	}
	else if (ty1==1)
	{
		sort(e+1,e+n,cmp2);
		l=1;r=500000000;
		while(l<r)
		{
			mid=(l+r)/2;
			cnt=0;
			L=1;R=n-1;
			while(R>=L&&e[R].z>=mid) {R--;cnt++;}
			while(R>L)
			{
				while(R>L&&e[R].z+e[L].z<mid) L++;
				if (R>L) {cnt++;R--;L++;}
			}
			if (cnt<m) r=mid;
			else l=mid+1;
		}
		wri(l-1);
	}
	else
	{
		memset(c,0,sizeof(c));
		for (int i=1;i<n;i++)
		{
			c[e[i].x]++;
			c[e[i].y]++;
		}
		for  (int i=1;i<=n;i++) if (c[i]<=2) {root=i;break;}
		memset(son,0,sizeof(son));
		memset(dis,0,sizeof(dis));
		build(root,0);
		l=1;r=500000000;
		while(l<r)
		{
			mid=(l+r)/2;
			memset(g,0,sizeof(g));
			dfs2(root);
			cnt=g[root];
			if (cnt<m) r=mid;
			else l=mid+1;
		}
		wri(l-1);
	}
	return 0;
}
