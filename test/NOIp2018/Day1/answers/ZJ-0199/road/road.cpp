#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
struct node{int num,id;} a[100010];
int n,ans,x;
int s[100010],l[100010],r[100010];
char ch;
void read(int &digit)
{
	digit=0;
	ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9')
	{
		digit=digit*10+ch-'0';
		ch=getchar();
	}
}
void wri(int x)
{
	if (x>9) wri(x/10);
	putchar('0'+x%10);
}
bool cmp(node a,node b) {return(a.num>b.num);}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for (int i=1;i<=n;i++)
	{
		read(s[i]);
		a[i].num=s[i];
		a[i].id=i;
	}
	sort(a+1,a+n+1,cmp);
	s[0]=0;
	s[n+1]=0;
	ans=0;
	for (int i=1;i<=n;i++)
	{
		l[i]=i-1;
		r[i]=i+1;
	}
	r[0]=1;
	l[n+1]=n;
	for (int i=1;i<=n;i++)
	{
		x=a[i].id;
		ans=ans+s[x]-max(s[l[x]],s[r[x]]);
		l[r[x]]=l[x];
		r[l[x]]=r[x];
	}
	wri(ans);
	return 0;
}
