#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define N 50007
int n,m;
int hd[N*3],nxt[N*3],to[N*3],l[N*3],cnt=0;
int L[N];
int lian[N];
int du[N];
bool vis[N];
void add(int a,int b,int v)
{
    ++cnt;
    nxt[cnt]=hd[a];
    hd[a]=cnt;
    to[cnt]=b;
    l[cnt]=v;
}

void work1()
{
    int leth[N]={0};
    sort(L+1,L+n);
    int i=n-1;
    for(int j=m;j<=m;++j,--i)
    {
        leth[j]+=L[i];
    }
    for(int j=m;j>=1 && i>=1;--j,--i)
    {
        leth[j]+=L[i];
    }
    int ans=leth[1];
    for(int i=1;i<=m;++i)
    {
        if(ans>leth[i])
        {
            ans=leth[i];
        }
    }
    cout<<ans<<endl;
}

void work2()
{
    const int maxans=550000000;
    int l=1,r=maxans;
    while(l<r)
    {
        int ans=(l+r+1)/2;
        int nowlen=0;
        int nowtiao=0;
        for(int i=1;i<=n-1;++i)
        {
            nowlen+=lian[i];
            if(nowlen>=ans)
            {
                nowlen=0;
                nowtiao++;
            }
        }
        if(nowtiao>=m)
        {
            l=ans;
        }
        else
        {
            r=ans-1;
        }
    }
    cout<<l<<endl;
}

struct nod
{
    int ys,no;
};

nod baoli(int x)
{
    //cout<<"wtf"<<endl;
    if(du[x]==1 || vis[x] || x<=0 || x>=n+1)
    {
        nod ans;
        ans.ys=ans.no=0;
        return ans;
    }
    vis[x]=1;
    if(du[x]==2)
    {
        //cout<<to[hd[x]]<<endl;
        nod ans=baoli(to[hd[x]]);
        //cout<<"ok"<<endl;
        ans.ys+=l[hd[x]];
        //cout<<x<<" "<<ans.ys<<" "<<ans.no<<endl;
        return ans;
    }
    int max1ys=0,max2ys=0,maxno=0;
    for(int i=hd[x];i;i=nxt[i])
    {
        //cout<<4<<" "<<i<<endl;
        if(!vis[to[i]])
        {
            //cout<<to[i]<<endl;
            nod tnod=baoli(to[i]);
            //cout<<"wtf"<<endl;
            int tt=tnod.no;
            if(tt>maxno) maxno=tt;
            tt=tnod.ys+l[i];
            if(tt>max1ys)
            {
                max2ys=max1ys;
                max1ys=tt;
            }
            else if(tt>max2ys)
            {
                max2ys=tt;
            }
        }
    }
    nod ans;
    ans.ys=max1ys;
    ans.no=maxno>max1ys+max2ys?maxno:max1ys+max2ys;
    //cout<<x<<" "<<ans.ys<<" "<<ans.no<<endl;
    return ans;
}

int main()
{
	ios::sync_with_stdio(0);
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
    cin>>n>>m;
    bool flag1=1,flag2=1;
    du[1]=1;
    for(int i=1;i<=n-1;++i)
    {
        int a,b,v;
        cin>>a>>b>>v;
        L[i]=v;
        lian[a]=v;
        du[a]++;
        du[b]++;
        if(a!=1) flag1=0;
        if(a+1!=b) flag2=0;
        add(a,b,v);
        add(b,a,v);
    }
    /*
    for(int i=1;i<=n-1;++i)
    {
        cout<<hd[i]<<" ";
    }
    cout<<endl;
    for(int i=1;i<=2*n-2;++i)
    {
        cout<<nxt[i]<<" ";
    }
    cout<<endl;
    for(int i=1;i<=n;++i)
    {
        cout<<du[i]<<" ";
    }
    cout<<endl;
    */
    //cout<<1<<endl;
    if(flag1)
    {
        work1();
        return 0;
    }
    if(flag2)
    {
        work2();
        return 0;
    }
    if(m==1)
    {
        //cout<<2<<endl;
        nod mayans=baoli(1);
        //cout<<3<<endl;
        int ans=mayans.ys>mayans.no?mayans.ys:mayans.no;
        cout<<ans<<endl;
        return 0;
    }
    srand(19260817);
    while(n--) rand();
    cout<<rand()<<endl;
	return 0;
}

/*
5 1
1 2 6
1 3 7
1 4 5
4 5 10

4 1
1 2 6
1 3 7
1 4 5

7 2
1 2 6
2 3 4
3 4 3
4 5 2
5 6 4
6 7 5

*/
