#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>
#include <utility>
#include <map>
#include <set>
#include <queue>
#include <vector>
using namespace std;
#define ll long long
#define fs first
#define sc second
#define pi pair<int,int>
#define mem(a,b) memset(a,b,sizeof(a))

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
}

int n;
int d[200005];

ll solve(int l,int r)
{
	int i,mi=0x3f3f3f3f,cou=0,ri;
	ll re=0;
	for(i=l;i<=r;i++)
		mi=min(d[i],mi);
	for(i=l;i<=r;i++)
	{
		d[i]-=mi;
		if(d[i]==0) cou++;
	}
	if(cou==r-l+1) return mi;
	else
	{
		re=mi;
		for(i=l;i<=r;i++)
			if(d[i])
			{
				ri=i;
				while(d[ri+1]&&ri<n) ri++;
				re+=solve(i,ri);
				i=ri; 
			}
		return re;
	}	
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int i,r;
	ll ans=0;
	n=read();
	for(i=1;i<=n;i++)
		d[i]=read();
	for(i=1;i<=n;i++)
		if(d[i])
		{
			r=i;
			while(d[r+1]&&r<n) r++;
			ans+=solve(i,r);
			i=r; 
		}
	printf("%lld\n",ans);
	return 0;
}
