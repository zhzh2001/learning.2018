#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>
#include <utility>
#include <map>
#include <set>
#include <queue>
#include <vector>
using namespace std;
#define ll long long
#define fs first
#define sc second
#define pi pair<int,int>
#define mem(a,b) memset(a,b,sizeof(a))

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
}

int n;
int a[1005];
int f[50005];

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int ti,i,j,ma,ans;
	ti=read();
	while(ti--)
	{
		n=read();
		ma=0;
		for(i=1;i<=n;i++)
		{
			a[i]=read();
			ma=max(ma,a[i]);
		}
		mem(f,0);
		f[0]=1;
		for(i=1;i<=n;i++)
			for(j=a[i];j<=ma;j++)
				if(f[j]+f[j-a[i]]>1)
					f[j]=2;
				else
					f[j]+=f[j-a[i]];
		ans=n;
		for(i=1;i<=n;i++)
			if(f[a[i]]>1)
				ans--;
		printf("%d\n",ans);
	}
	return 0;
}
