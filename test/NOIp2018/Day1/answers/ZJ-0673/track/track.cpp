#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>
#include <utility>
#include <map>
#include <set>
#include <queue>
#include <vector>
using namespace std;
#define ll long long
#define fs first
#define sc second
#define pi pair<int,int>
#define mem(a,b) memset(a,b,sizeof(a))

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
}

struct edge
{
	int to,next,v;
}e[120005];

int n,m,cnt,len,sz,anss=0;
int head[60005];
int dis[60005];
int a[60005],fa[60005],vis[60005],ifa[60005],dep[60005],vfa[60005];

void addedge(int x,int y,int w)
{
	e[++cnt].to=y;e[cnt].next=head[x];head[x]=cnt;e[cnt].v=w;
}

void dfs1(int x,int f)
{
	int i,v;
	for(i=head[x];i!=-1;i=e[i].next)
	{
		v=e[i].to;
		if(v==f) continue;
		dis[v]=dis[x]+e[i].v;
		dfs1(v,x);
	}
}

bool judge(int x)
{
	int i,sum=0,k=0;
	for(i=1;i<=len;i++)
	{
		sum+=a[i];
		if(sum>=x)
		{
			sum=0;
			k++;
		}
	}
	if(k>=m)
		return 1;
	return 0;
}

bool cmp(int x,int y)
{
	return x>y;
}

void dfs2(int x)
{
	int i,v;
	for(i=head[x];i!=-1;i=e[i].next)
	{
		v=e[i].to;
		if(v==fa[x]) continue;
		fa[v]=x; ifa[v]=++sz; vfa[v]=e[i].v;
		dep[v]=dep[x]+1;
		dfs2(v);
	}
}

void del_road(int x,int y)
{
	if(dep[x]<dep[y]){int t=x;x=y;y=t;}
	while(dep[x]>dep[y])
	{
		vis[ifa[x]]=0;
		x=fa[x];
	}
	if(x==y) return ;
	while(x!=y)
	{
		vis[ifa[x]]=0;
		x=fa[x];
		vis[ifa[y]]=0;
		y=fa[y];
	}
}

int get_road(int x,int y)
{
	int tx=x,ty=y,re=0;
	if(dep[tx]<dep[ty]){int t=tx;tx=ty;ty=t;t=x;x=y;y=t;}
	while(dep[tx]>dep[ty])
	{
		if(vis[ifa[tx]]) 
		{
			del_road(x,tx);
			return 0;
		}
		vis[ifa[tx]]=1;
		re+=vfa[tx];
		tx=fa[tx];
	}
	if(tx==ty) return re;
	while(tx!=ty)
	{
		if(vis[ifa[tx]]) 
		{
			del_road(x,tx);
			del_road(y,ty);
			return 0;
		}
		vis[ifa[tx]]=1;
		re+=vfa[tx];
		tx=fa[tx];
		if(vis[ifa[ty]]) 
		{
			del_road(x,tx);
			del_road(y,ty);
			return 0;
		}
		vis[ifa[ty]]=1;
		re+=vfa[ty];
		ty=fa[ty];
	}
	return re;
}

void dfs(int x,int now)
{
	int i,j,tnow,v;
	if(x==m)
	{
		if(now>anss)
			anss=now;
		return ;
	}
	for(i=1;i<=n;i++)
		for(j=i+1;j<=n;j++)
			if(v=get_road(i,j))
			{
				tnow=min(now,v);
				dfs(x+1,tnow);
				del_road(i,j);
			}
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int i,j,x,y,w,cou1=0,cou2=0;
	n=read(); m=read();
	for(i=1;i<=n;i++)
		head[i]=-1;
	for(i=1;i<n;i++)
	{
		x=read(),y=read(),w=read();
		if(x==1) cou1++;
		if(y==x+1) cou2++;
		addedge(x,y,w);
		addedge(y,x,w);
	}
	if(m==1)
	{
		int mx=0,id=0;
		dfs1(1,0);
		for(i=1;i<=n;i++)
			if(mx<dis[i])
				id=i,mx=dis[i];
		mem(dis,0);
		dfs1(id,0);
		for(i=1;i<=n;i++)
			mx=max(mx,dis[i]);
		printf("%d\n",mx);
	}
	else if(cou1==n-1)
	{
		for(i=head[1];i!=-1;i=e[i].next)
			a[++len]=e[i].v;
		if(2*m<=len)
		{
			sort(a+1,a+len+1,cmp);
			int ans=0x3f3f3f3f;
			for(i=1;i<=m;i++)
				ans=min(a[i]+a[2*m+1-i],ans);
			printf("%d\n",ans);
		}
		else
		{
			sort(a+1,a+len+1);
			int io=2*m-len,ans=0x3f3f3f3f;
			ans=a[len-io+1];
			for(i=1;i<=(len-io)/2;i++)
				ans=min(a[i]+a[len-io+1-i],ans);
			printf("%d\n",ans);
		}
	}
	else if(cou2==n-1)
	{
		for(i=1;i<=n;i++)
			for(j=head[i];j!=-1;j=e[j].next)
				if(e[j].to==i+1)
					a[++len]=e[j].v;
		int sum=0;
		for(i=1;i<=len;i++)
			sum+=a[i];
		int li=0,ri=sum,mid,ans=0;
		while(li<=ri)
		{
			mid=(li+ri)>>1;
			if(judge(mid))
			{
				ans=mid;
				li=mid+1;
			}
			else
				ri=mid-1;
		}
		printf("%d\n",ans);
	}
	else
	{
		fa[1]=0;dep[1]=0;
		dfs2(1);
		dfs(0,0x3f3f3f3f);
		printf("%d\n",anss);
	}
	return 0;
}
