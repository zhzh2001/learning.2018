#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=105;
int n,a[N];
bool mark[25005];
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int Q=read();
	while(Q--){
		n=read();
		int mx=0;
		for(int i=1;i<=n;i++){
			a[i]=read();
			mx=max(mx,a[i]);
		}
		sort(a+1,a+n+1);
		memset(mark,0,sizeof(mark));
		int Ans=0;
		for(int i=1;i<=n;i++)
			if(!mark[a[i]]){
				mark[a[i]]=1;
				for(int j=1;j+a[i]<=mx;j++)
						mark[j+a[i]]|=mark[j];
				Ans++;
			}
		printf("%d\n",Ans);
	}
	return 0;
}
