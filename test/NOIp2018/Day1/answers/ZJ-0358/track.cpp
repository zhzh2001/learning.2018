#include <cstdio>
#include <algorithm>
using namespace std;
const int N=50005;
int n,m,tot,l,r,mid,Ans;
int L[N],R[N];
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
struct edge{
	int to,val,next;
}e[N+N];
int cnt_edge,last[N];
inline void add_edge(int u,int v,int w){
	e[++cnt_edge]=(edge){v,w,last[u]};last[u]=cnt_edge;
}
struct note{
	int x,len;
}q[N];
bool cmp(note a,note b){
	return a.len>b.len;
}
note dp(int u,int fa){
	note res=(note){0,0};
	if(L[u]>R[u]){
		return res;
	}
	int now=L[u];
	for(int i=last[u];i;i=e[i].next){
		int v=e[i].to;
		if(v==fa)continue;
		q[now]=dp(v,u);
		q[now].len+=e[i].val;
		res.x+=q[now].x;
		if(q[now].len>=mid){
			res.x++;
			q[now].len=0;
		}
		now++;
	}
	if(L[u]==R[u]){
		res.len=q[L[u]].len;
		return res;
	}
	/*
	if(R[u]-L[u]+1==3){
		printf("!!! %d %d %d %d\n",u,q[L[u]].len,q[L[u]+1].len,q[L[u]+2].len);
		if(q[L[u]].len+q[R[u]].len>=mid){
			if(q[L[u]+1].len+q[R[u]].len>=mid){
				res.x++;
				res.len=q[L[u]].len;
			}else{
				res.x++;
				res.len=q[L[u]+1].len;
			}
		}else if(q[L[u]].len+q[L[u]+1].len>=mid){
			res.x++;
			res.len=q[R[u]].len;
		}
		return res;
	}
	*/
	sort(q+L[u],q+R[u]+1,cmp);
	int Len=0;
	int l=L[u],r=R[u];
	for(l=l;l<r;l++){
		while(l<r && q[l].len+q[r].len<mid){
			Len=q[r].len;
			r--;
		}
		if(l<r){
			res.x++;
			r--;
		}else break;
	}
	if(l<=r)res.len=q[l].len;
	else res.len=Len;
	return res;
}
void dfs(int u,int fa,int l){
	L[u]=l;
	for(int i=last[u];i;i=e[i].next){
		int v=e[i].to;
		if(v==fa)continue;
		tot++;
	}
	R[u]=tot;
	for(int i=last[u];i;i=e[i].next){
		int v=e[i].to;
		if(v==fa)continue;
		dfs(v,u,tot+1);
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<n;i++){
		int u=read(),v=read(),w=read();
		add_edge(u,v,w);
		add_edge(v,u,w);
	}
	tot=0;
	dfs(1,0,1);
	l=0,r=500000000,Ans=-1;
	while(l<=r){
		mid=(l+r)>>1;
		note res=dp(1,0);
		if(res.x>=m){
			Ans=mid;
			l=mid+1;
		}else r=mid-1;
	}
	printf("%d\n",Ans);
	return 0;
}
