#include <cstdio>
using namespace std;
const int N=1e5+5;
int n,top,Ans;
int a[N],q[N];
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<=n;i++){
		if(top && q[top]>a[i])
			Ans+=q[top]-a[i];
		while(top && q[top]>a[i])
			top--;
		q[++top]=a[i];
	}
	if(top) Ans+=q[top];
	printf("%d\n",Ans);
	return 0;
}
