#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,ans,mx;
int a[200];
int f[40000];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=n;++i)
			scanf("%d",&a[i]);
		sort(a+1,a+1+n);mx=a[n];
		memset(f,0,sizeof(f));
		f[0]=1;ans=0;
		for(int i=1;i<=n;++i)
			if(!f[a[i]]){
				++ans;
				for(int j=a[i];j<=mx;++j)
					f[j]|=f[j-a[i]];
			}
		printf("%d\n",ans);
	}
	return 0;
}
