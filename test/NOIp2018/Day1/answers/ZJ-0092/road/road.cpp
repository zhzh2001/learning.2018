#include<iostream>
#include<cstdio>
using namespace std;
const int N=1e5+10;
int n,ans=0;
int d[N];

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;++i)
		scanf("%d",&d[i]);
	d[0]=0;
	for(int i=1;i<=n;++i)
		if(d[i]>d[i-1]) ans+=(d[i]-d[i-1]);
	printf("%d\n",ans);
	return 0;
}
