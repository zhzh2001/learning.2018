#include<iostream>
#include<cstdio>
#include<set>
#include<cstring>
#include<algorithm>
using namespace std;
inline int rd(){
	int x=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x;
}
const int N=5e4+10;
int n,m,cnt=0,ans,mid,y,z,M=0;
int hed[N],mx[N],fa[N];
struct a{
	int r,nxt,val;
}e[N<<1];
void ins(int u,int v,int w){
	e[++cnt].r=v;e[cnt].nxt=hed[u];hed[u]=cnt;e[cnt].val=w;
}
multiset<int> S;
multiset<int>::iterator t;
void bfs(int x){
	mx[x]=0;
	for(int i=hed[x];i;i=e[i].nxt)
		if(e[i].r!=fa[x]){
			fa[e[i].r]=x;
			bfs(e[i].r);
			M=max(M,mx[e[i].r]+e[i].val+mx[x]);
			mx[x]=max(mx[x],mx[e[i].r]+e[i].val);
		}
	return;
}
void dfs(int x){
	mx[x]=0;
	for(int i=hed[x];i;i=e[i].nxt)
		if(e[i].r!=fa[x])
			dfs(e[i].r);
	if(ans>=m) return;
	S.clear();
	for(int i=hed[x];i;i=e[i].nxt)
		if(e[i].r!=fa[x]){
			y=mx[e[i].r]+e[i].val;
			if(y>=mid){++ans;continue;}
			else S.insert(y);
		}
	while(!S.empty()){
		t=S.begin();
		y=(*t);
		S.erase(t);
		z=mid-y;
		t=S.lower_bound(z);
		if(t==S.end()) mx[x]=y;
		else{
			S.erase(t);
			++ans;
		}
	}
	return;
}
bool Z=0;
int k=0;
int g[N<<1];
bool cmp(int A,int B){
	return A>B;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=rd();m=rd();
	int u,v,w,sum=0;
	for(int i=1;i<n;++i){
		u=rd();v=rd();w=rd();
		ins(u,v,w);
		ins(v,u,w);
		sum+=w;
		if(u!=1&&v!=1) Z=1;
		else g[++k]=w;
	}
	if(!Z){
		sort(g+1,g+1+k,cmp);
		if(k<m+m)
			for(int i=k+1;i<=m+m;++i) g[i]=0;
		ans=1000000000;
		for(int i=1;i<=m;++i)
			ans=min(ans,g[i]+g[m+m+1-i]);
		printf("%d\n",ans);
		return 0;
	}
	bfs(1);
	sum=sum/m;
	int l=0,r=min(M,sum);
	while(l!=r){
		mid=l+r+1>>1;
		ans=0;
		dfs(1);
		if(ans>=m) l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
