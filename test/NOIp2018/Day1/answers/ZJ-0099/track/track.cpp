#include <bits/stdc++.h>
using namespace std;
#define N 50010
int he[N],ne[N<<1],e[N<<1],va[N<<1],cnt;
int Mx[N],n,m,sum,vis[N],Ne[N];
vector<int>G[N];
inline int read(){
	char ch=getchar(); int x=0;
	for (;ch>'9'||ch<'0';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x;
}
inline bool cmp(const int &a,const int &b){return a>b;}
void dfs(int x,int fa,int len){
	G[x].clear(); Mx[x]=0;
	for (register int i=he[x];i;i=ne[i]){
		if (e[i]==fa) continue;
		dfs(e[i],x,len);
		G[x].push_back(Mx[e[i]]+va[i]);
	}
	sort(G[x].begin(),G[x].end(),cmp);
//	printf("%d::",x); for (int i=0;i<G[x].size();i++)  printf("%d ",G[x][i]); puts("");
	register int l=0,r=G[x].size()-1;
	for (register int i=l;i<=r;i++) Ne[i]=i;
	while (l<=r){if (G[x][l]>=len)sum++,vis[l]=x,l++; else break;}
	if (l>r){Mx[x]=0; return;}
	if (l==r){Mx[x]=G[x][l]; return;}
	if (r-l+1==2){
		if (G[x][l]+G[x][r]<len) Mx[x]=G[x][l];
		else sum++,Mx[x]=0;
		return;
	}
	if (r-l+1==3){
		if (G[x][r]+G[x][r-1]>=len){sum++; Mx[x]=G[x][l]; return;}
		if (G[x][r]+G[x][l]>=len){sum++; Mx[x]=G[x][l+1]; return;}
		if (G[x][l+1]+G[x][l]>=len){sum++; Mx[x]=G[x][r]; return;}
		Mx[x]=G[x][l]; return;
	}
	register int LE=l,RI=G[x].size()-1;
	register int tmp=0;
	while (l<r){
		while (G[x][l]+G[x][r]<len && l<r) r--;
		if (l>=r) break;
		sum++; tmp++; vis[l]=x; Ne[l]=r; l++; r--;
	}
	Ne[l-1]=min(Ne[l-1],l+1);
	if (Ne[l-1]==l){
		for (register int i=0;i<tmp;i++) vis[Ne[l-1-i]]=x;
		for (register int i=0;i<=RI;i++) if (vis[i]!=x){Mx[x]=G[x][i]; return;}
		Mx[x]=0; return;
	}
	for (register int i=0;i<tmp;i++) {
		vis[Ne[l-1]+i]=x;
		Ne[l-i-1]=Ne[l-1]+i;
	}
//	for (int i=0;i<tmp;i++) printf("%d ",Ne[i]); puts("");
	/*if (vis[l]==x){
		for (register int i=0;i<=RI;i++) if (vis[i]!=x){Mx[x]=G[x][i]; return;}
		Mx[x]=0; return;
	}*/
	register int res=G[x][l];
	r=Ne[l-1];
	while (G[x][l]+G[x][r]>=len && r<=RI && l>LE){
		res=G[x][l-1];
		r++; l--;
	}
	Mx[x]=res;
}
bool check(int mid){
//	puts("");
//	printf("len=%d\n",mid);
	sum=0; memset(vis,0,sizeof(vis)); 
	dfs(1,0,mid);
//	for (int i=1;i<=n;i++) printf("%d ",Mx[i]); puts("");
	return sum>=m;
}
void add(int u,int v,int w){ne[++cnt]=he[u];he[u]=cnt;e[cnt]=v;va[cnt]=w;}
int main(){
	freopen("track.in","r",stdin); freopen("track.out","w",stdout);
	n=read(),m=read(); int L=0,R=0;
	for (int i=1;i<n;i++){
		int u=read(),v=read(),w=read();
		add(u,v,w); add(v,u,w);
		R+=w;		
	}
	int ans=0;
	while (L<=R){
		int mid=(L+R)>>1;
		if (check(mid)) ans=mid,L=mid+1;
		else R=mid-1;
	}
	printf("%d\n",ans);
//	cerr<<clock()<<endl;
	return 0;
}
