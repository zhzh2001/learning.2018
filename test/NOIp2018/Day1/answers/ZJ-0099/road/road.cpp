#include <bits/stdc++.h>
using namespace std;
#define N 100100
inline int read(){
	char ch=getchar(); int x=0;
	for (;ch>'9'||ch<'0';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x;
}
int n,d[N],a[N];
int main(){
	freopen("road.in","r",stdin); freopen("road.out","w",stdout);
	n=read();
	d[0]=0;
	for (int i=1;i<=n;i++){
		d[i]=read();
		a[i]=d[i]-d[i-1];
	}
	int ans=0;
	for (int i=1;i<=n;i++){
		if (a[i]>0) ans+=a[i];
	}
	printf("%d\n",ans);
	return 0;
}
