#include <bits/stdc++.h>
using namespace std;
#define N 111111
int n,vis[N],a[N];
void solve(){
	scanf("%d",&n); for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	sort(a+1,a+1+n); memset(vis,0,sizeof(vis));
	int tot=1; vis[0]=1; vis[a[1]]=1;
	for (int i=2;i*a[1]<=25000;i++) vis[a[1]*i]=1;
	for (int i=2;i<=n;i++){
		if (vis[a[i]]) continue;
		tot++;
		for (int j=0;j<=25000;j++)
			if (vis[j]){
				for (int k=1;k*a[i]+j<=25000;k++) vis[k*a[i]+j]=1;
			}
	}
	printf("%d\n",tot);
}
int main(){
	freopen("money.in","r",stdin); freopen("baom.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--){
		solve();
	}
	return 0;
}
