#include <cstdio>
#include <cstring>
#include <algorithm>
int T, n, cnt, s;
int a[200], b[200];
bool flag;
bool f[30000];
void dfs(const int& x, const int& y)
{
	if (x > cnt) return;
	for (register int i = 0; i <= 25000; ++i)
	{
		s += i * b[x];
		if (s <= 25000) f[s] = 1;
		if (s > y) {s -= i * b[x]; return; }
		if (s == y) {flag = 1; return; }
		dfs(x + 1, y);
		if (flag) return;
		s -= i * b[x];
	}
}
bool check(const int& x)
{
	s = flag = 0;
	dfs(1, x);
	return flag;
}
int main()
{
	freopen("money.in", "r+", stdin);
	freopen("money.out", "w+", stdout);
	scanf("%d", &T);
	while (T--)
	{
		cnt = 0;
		memset(f, 0, sizeof f);
		scanf("%d", &n);
		for (register int i = 1; i <= n; ++i) scanf("%d", &a[i]);
		std::sort(a + 1, a + n + 1);
		b[++cnt] = a[1];
		for (register int i = 2; i <= n; ++i)
		{
			if (f[a[i]]) continue;
			if (!check(a[i]))
				b[++cnt] = a[i];
		}
		printf("%d\n", cnt);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
