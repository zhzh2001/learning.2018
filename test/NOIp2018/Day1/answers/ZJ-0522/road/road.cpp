#include <cstdio>
int n, ans;
int a[101000], d[101000];
int main()
{
	freopen("road.in", "r+", stdin);
	freopen("road.out", "w+", stdout);
	scanf("%d", &n);
	for (register int i = 1; i <= n; ++i)
	{
		scanf("%d", &a[i]);
		d[i] = a[i] - a[i - 1];
	}
	for (register int i = 1; i <= n; ++i)
		if (d[i] > 0)
			ans += d[i];
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
