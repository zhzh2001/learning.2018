#include <cstdio>
#include <cstring>
#include <algorithm>
int n, m, ans, cnt, ll, rr, xx, mmid, ss, ccnt, js;
int a[60000], b[60000], l[60000];
int to[120000], nxt[120000], va[120000], h[60000];
int dp[60000];
bool fl1 = 1, fl2 = 1;
bool used[60000];
inline void add_edge(const int& u, const int& v, const int& l)
{
	to[++cnt] = v;
	nxt[cnt] = h[u];
	va[cnt] = l;
	h[u] = cnt;
}
void dfs1(const int& u, int s1 = 0, int s2 = 0)
{
	used[u] = 1;
	for (register int i = h[u]; i; i = nxt[i])
		if (!used[to[i]])
		{
			dfs1(to[i]);
			if (dp[to[i]] + va[i] > s1)
			{
				s2 = s1;
				s1 = dp[to[i]] + va[i];
			}
		}
	dp[u] = s1;
	ans = std::max(ans, s1 + s2);
}
void TR1()
{
	for (register int i = 1; i < n; ++i)
	{
		add_edge(a[i], b[i], l[i]);
		add_edge(b[i], a[i], l[i]);
	}
	dfs1(1);
}
struct Node
{
	int b, l;
}ee[60000];
bool cmp(const Node& x, const Node& y)
{
	return x.l < y.l;
}
int find1(const int& x)
{
	int lll = 1, rrr = n - 1;
	while (lll <= rrr)
	{
		int mmmid = (lll + rrr) >> 1;
		if (ee[mmmid].l == x) return mmmid;
		if (ee[mmmid].l < x) lll = mmmid + 1;
		else rrr = mmmid - 1;
	}
	if (a[rrr] >= x) return rrr;
	else return -1;
}
bool check2(const int& x)
{
	cnt = 0;
	memset(used, 0, sizeof used);
	for (register int i = 1; i < n; ++i)
		if (!used[i])
		{
			if (ee[i].l >= x) {used[i] = 1; ++cnt; continue; }
			int xxx = find1(x - ee[i].l);
			if (xxx == -1 || xxx == i) continue;
			while (used[xxx]) ++xxx;
			if (xxx >= n) continue;
			++cnt; used[i] = used[xxx] = 1;
		}
	return cnt >= m;
}
void TR2()
{
	xx = 0;
	for (register int i = 1; i < n; ++i)
	{
		xx += l[i];
		ee[i].b = b[i];
		ee[i].l = l[i];
	}
	std::sort(ee + 1, ee + n, cmp);
	ll = 1; rr = xx;
	while (ll <= rr)
	{
		mmid = (ll + rr) >> 1;
		if (check2(mmid)) ll = mmid + 1;
		else rr = mmid - 1;
	}
	ans = rr;
}
void dfs2(const int& u, const int& x)
{
	used[u] = 1;
	for (register int i = h[u]; i; i = nxt[i])
		if (!used[to[i]])
		{
			ss += va[i];
			if (ss >= x) {ss = 0; ++ccnt; }
			dfs2(to[i], x);
		}
}
bool check1(const int& x)
{
	ss = ccnt = 0;
	memset(used, 0, sizeof used);
	dfs2(1, x);
	return ccnt >= m;
}
void TR3()
{
	xx = 0;
	for (register int i = 1; i < n; ++i)
	{
		add_edge(a[i], b[i], l[i]);
		add_edge(b[i], a[i], l[i]);
		xx += l[i];
	}
	ll = 1; rr = xx;
	while (ll <= rr)
	{
		mmid = (ll + rr) >> 1;
		if (check1(mmid)) ll = mmid + 1;
		else rr = mmid - 1;
	}
	ans = rr;
}
void TR4()
{
	ans = 0x3f3f3f3f;
	for (register int i = 1; i < n; ++i)
		ans = std::min(ans, l[i]);
}
int main()
{
	freopen("track.in", "r+", stdin);
	freopen("track.out", "w+", stdout);
	scanf("%d%d", &n, &m);
	for (register int i = 1; i < n; ++i)
	{
		scanf("%d%d%d", &a[i], &b[i], &l[i]);
		if (a[i] != 1) fl1 = 0;
		if (b[i] != a[i] + 1) fl2 = 0;
	}
	if (m == 1) TR1();
	else if (fl1) TR2();
	else if (fl2) TR3();
	else TR4();
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
