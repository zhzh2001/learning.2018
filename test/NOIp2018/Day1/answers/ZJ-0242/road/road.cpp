#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;

const int N = 1e5 + 5;

int ans = 0;
int n, d[N];
int tmp1[N], tail1 = 0;
int tmp2[N], tail2 = 0;

struct RANGE {
	int l;
	int r;
	int d;
} Range[N];

bool cmp(RANGE x, RANGE y) {
	int len1 = x.r - x.l + 1;
	int len2 = y.r - y.l + 1;
	if (len1 != len2) return len1 > len2;
	else return x.l < y.l;
}

int main() {
	//从文件输入输出
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	
	scanf("%d", &n);
	for (register int i = 1; i <= n; ++i)
		scanf("%d", &d[i]);
	tmp1[++tail1] = 1;	
	Range[1].l = 1;	
	Range[1].d = d[1];
	for (register int i = 2; i <= n; ++i) {
		Range[i].d = d[i];	
		while (d[tmp1[tail1]] > d[i] && (tail1 != 0)) {
			--tail1;	
		}
		if (tail1 == 0) Range[i].l = 1;
		else Range[i].l = tmp1[tail1] + 1;
		tmp1[++tail1] = i;	
	}
	tmp2[++tail2] = n;
	Range[n].r = n;
	for (register int i = (n - 1); i >= 1; --i) {
		while (d[tmp2[tail2]] > d[i] && (tail2 != 0)) {
			--tail2;	
		}
		if (tail2 == 0) Range[i].r = n;
		else Range[i].r = tmp2[tail2] - 1;
		tmp2[++tail2] = i;
	}
	sort(Range + 1, Range + 1 + n, cmp);
	for (register int i = 1; i <= n; ++i) {
		for (register int j = 1; j < i; ++j) {
			if (Range[j].l <= Range[i].l && Range[j].r >= Range[i].r) 
				Range[i].d -= Range[j].d;
		}
		ans += Range[i].d;
	}
	printf("%d", ans);
	return 0;
}
