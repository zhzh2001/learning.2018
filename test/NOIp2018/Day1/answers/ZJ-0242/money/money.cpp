#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int N = 1e2 + 5;

int t, n;
int a[N];
bool check[N];
bool flag = false;

int read() {
	int s = 0, w = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9') {
		if (ch == '-') w = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9') {
		s = s * 10 + ch - '0';
		ch = getchar();
	}
	return s * w;
}

void dfs(int s, int pos, int rest) {
	if (s == pos) dfs(s , pos + 1, rest); 
	if (pos == (n + 1) && rest != 0) return;	
	if (pos == (n + 1) && rest == 0) {
		flag = true;
		return;
	}
	for (register int i = 0; i * a[pos] <= rest; ++i) {
		if (flag) break;
		dfs(s, pos + 1, rest - (i * a[pos]));
	}
}

bool cmp(int x, int y) {
	return x > y;
}

int main() {
	//从文件输入输出
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);

	t = read();	
	while (t--) {
		n = read();	
		int m = n; //答案
		for (register int i = 1; i <= n; ++i) {
			a[i] = read();	
		}
		if (n == 2) {
			cout << 2 << endl;
			continue;
		}
		sort(a + 1, a + 1 + n, cmp); //将数从大到小排序
		for (register int i = 1; i <= n; ++i) { //看每个数是否能被其他数表示
			memset(check, 0, sizeof(check));	
			flag = false;
			dfs(i, 1, a[i]);
			if (flag) --m;
		}
		printf("%d\n", m);
	}
	return 0;
}
