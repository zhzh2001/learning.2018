#include <bits/stdc++.h>
using namespace std;
int n,m,ip1,ip2,ip3,l_link,r_link,summ,mid,piec,qwq,l,r,layer,notcho,use_r,gugu;
struct node{int to,val;};
vector<node>Link[50050];
vector<int>leng[50050];
int temp[50050],mu[50050];
void dfs(int x,int fa,int lim,int val){
	for (int i=Link[x].size()-1;i>=0;--i)
		if(Link[x][i].to!=fa)
			dfs(Link[x][i].to,x,lim,Link[x][i].val);
	qwq=leng[x].size()-1;
	for (int i=qwq;i>=0;--i)
		temp[i]=leng[x][i];
	sort(temp,temp+qwq+1);
	//cout<<"dfs"<<x<<":";
//	for (int i=0;i<=qwq;++i)
//		cout<<temp[i]<<' ';
//	cout<<endl;
	r=qwq+1;layer=0;gugu=0;
	for (;r>0&&temp[r-1]>=lim;--r) piec++;
	use_r=r-1;notcho=-1;
	for (l=0;l<=r;++l){
		for (;temp[l]+temp[r-1]>=lim&&r>0;--r);
		mu[l]=r;if(mu[l]<=l) break;
	}
	for (int i=l-1;i>=0;--i){
		if(mu[i]>layer+1) notcho=mu[i]-1;
		layer=max(layer+1,mu[i]);
		if(layer<=use_r) gugu++;
		else notcho=max(notcho,i);
	}
	piec+=(use_r-l+1-gugu)/2+gugu;
	//cout<<"notcho="<<notcho<<" piec="<<piec<<endl;
	if(l==0) leng[fa].push_back(temp[use_r]+val);
	else if(notcho<=use_r&&notcho!=-1) leng[fa].push_back(temp[notcho]+val);
	else leng[fa].push_back(val);
	leng[x].clear();
}
bool check(int x){
	//cout<<"check "<<x<<endl;
	piec=0;dfs(1,0,x,0);
	//cout<<"piec="<<piec<<endl;
	if(piec>=m) return 1;
	else return 0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;++i)
		scanf("%d%d%d",&ip1,&ip2,&ip3),summ+=ip3,
		Link[ip1].push_back((node){ip2,ip3}),
		Link[ip2].push_back((node){ip1,ip3});
	l_link=1;r_link=summ/m;
	while(l_link+1<r_link){
		mid=l_link+r_link>>1;
		if(check(mid)) l_link=mid;
		else r_link=mid;
	}
	printf("%d",check(l_link)?l_link:r_link);
	return 0;
}
