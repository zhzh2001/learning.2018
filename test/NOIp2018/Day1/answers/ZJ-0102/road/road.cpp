#include <bits/stdc++.h>
using namespace std;
int n,now,last;
struct node{int val,sit;}num[100050];
bool is_down[100050];
int size=0;
long long ans=0;
bool mycmp(node x,node y){return x.val>y.val;}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;++i)
		scanf("%d",&num[i].val),num[i].sit=i;
	sort(num+1,num+n+1,mycmp);last=num[1].val;
	for (int i=1;i<=n;++i){
		ans+=size*(last-num[i].val);
		now=num[i].sit;
		if(is_down[now-1]&&is_down[now+1]) size--;
		else if((!is_down[now-1])&&(!is_down[now+1])) size++;
		is_down[now]=1;
		last=num[i].val;
	}
	ans+=size*last;
	printf("%lld",ans);
	return 0;
}
