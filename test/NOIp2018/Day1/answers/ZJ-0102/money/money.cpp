#include <bits/stdc++.h>
using namespace std;
int t,n,num[105],ans,maxnum;
bitset<25050>b;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	for (int o=1;o<=t;++o){
		b.reset();b[0]=1;ans=0;maxnum=0;
		scanf("%d",&n);
		for (int i=1;i<=n;++i)
			scanf("%d",&num[i]),maxnum=max(maxnum,num[i]);
		sort(num+1,num+n+1);
		for (int i=1;i<=n;++i){
			if(b[num[i]]==0){
				ans++;
				for (int j=num[i];j<=maxnum;j+=num[i])
					b|=(b<<num[i]);
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
