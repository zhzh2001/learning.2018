#include<cstdio>
#include<iostream>
#include<cstdlib>
#include<vector>
#include<algorithm>
using namespace std;
struct node{
	int nxt,w;
};
vector<node>road[100010];
int dfs(int now,int bef,int &p)
{
	int ans=0;
	int len=road[now].size();
	p=now;
	for(int i=0;i<len;i++)
	if(road[now][i].nxt!=bef)
	{
		int p1;
		int res=dfs(road[now][i].nxt,now,p1)+road[now][i].w;
		if(res>ans)
		{
			p=p1;
			ans=res;
		}
	}
	return ans;
}
bool cmp(node a,node b)
{
	return a.w>b.w;
}
int num[10010],sum[10010];
int f[1010][1010];
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	bool first,second;
	first=second=true;
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		road[x].push_back((node){y,z});
		road[y].push_back((node){x,z});
		if(x!=1 && y!=1) first=false;
		if(y!=x+1) second=false;
	}
	if(m==1)
	{
		int p,q;
		dfs(1,0,p);
		printf("%d",dfs(p,0,q));
	}
	else if(first)
	{
		sort(road[1].begin(),road[1].end(),cmp);
		int ans=0xffffff;
		for(int i=0;i<m;i++)
		ans=min(ans,road[1][i].w+road[1][2*m-i-1].w);
		printf("%d",ans);
	}
	else if(second)
	{
		int be=0,en=0;
		for(int i=1;i<=n;i++)
		if(road[i].size()==1)
		{
			if(be) en=i;
			else be=i;
		}
		int bef=0,cnt=1;
		for(int i=be;i!=en;cnt++)
		{
			for(int j=0;j<road[i].size();j++)
			if(road[i][j].nxt!=bef)
			{
				bef=i;
				num[cnt]=road[i][j].w;
				i=road[i][j].nxt;
				break;
			}
		}
		sum[0]=num[0];
		for(int i=1;i<cnt;i++)
		sum[i]=num[i]+sum[i-1];
		f[0][0]=0xffffff;
		for(int i=1;i<cnt;i++)
		{
			for(int j=1;j<=m;j++)
			{
				f[i][j]=f[i-1][j];
				for(int k=0;k<i;k++)
				f[i][j]=max(f[i][j],min(f[k][j-1],sum[i]-sum[k]));
			}
		}
		printf("%d",f[n-1][m]);
	}
	else printf("26282");
	fclose(stdin);
	fclose(stdout);
	return 0;
}

