#include<cstdio>
#include<iostream>
#include<cstdlib>
#include<cstring>
using namespace std;
int a[110],n;
bool place[25010],have[110];
int que[100010];
int l,r;
int can;
bool dfs(int now,int num)
{
	if(num==0) return true;
	if(now>=n) return false;
	if(!(can&(1<<now))) return dfs(now+1,num);
	if(num%a[now]==0) return true;
	for(int i=0;i*a[now]<=num;i++)
		if(dfs(now+1,num-i*a[now])) return true;
	return false;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int maxn=0;
		scanf("%d",&n);
		memset(place,0,sizeof(place));
		memset(a,0,sizeof(a));
		for(int i=0;i<n;i++)
		{
			scanf("%d",&a[i]);
			maxn=max(a[i],maxn);
		}
		for(int i=0;i<n;i++)
		if(!place[a[i]])
			for(int k=a[i]*2;k<=maxn;k+=a[i]) place[k]=true;
		for(int i=0;i<n;i++)
		if(!place[a[i]])
			que[r++]=a[i];
		if(maxn<=100)
		{
			while(l<r)
			{
				int now=que[l];
				for(int i=l+1;i<r;i++)
				{
					for(int k=min(que[l],que[i])+1;k<=maxn;k++)
					if(!place[k] && k!=que[i] && k!=que[l])
					{
						for(int p=k/que[i];p>=0;p--)
						if((k-p*que[i])%que[l]==0)
						{
							place[k]=true;
							que[r++]=k;
							//printf("%d ",k);
							break;
						}
					}
				}
				l++;
			}
			
			int ans=0;
			for(int i=0;i<n;i++)
			if(!place[a[i]]) ans++;
			printf("%d\n",ans);
		}
		else
		{
			int ans=n;
			for(can=1;can<(1<<n);can++)
			{
				bool ok=true;
				for(int i=0;i<n;i++)
				if(!(can&(1<<a[i])))
				{
					if(!dfs(0,a[i]))
					{
						ok=false;
						break;
					}
				}
				int res=0;
				if(ok)
				{
					int can1=can;
					while(can1)
					{
						res+=can1&1;
						can1>>=1;
					}
					ans=min(ans,res);
				}
			}
			printf("%d\n",ans);
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

