#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=101,V=25001;
char inchar(){static char c[100000],*x=c,*y=c;return x==y&&(y=(x=c)+fread(c,1,100000,stdin),x==y)?EOF:*x++;}
int read(){int c,x=0;while(!isdigit(c=inchar()));while(x=(x<<3)+(x<<1)+c-'0',isdigit(c=inchar()));return x;}
int mx,a[N],f[V];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(int T=read();T--;){
		int n=read(),ans=n;mx=0;memset(f,0,sizeof f);f[0]=1;
		for(int i=1;i<=n;++i) a[i]=read(),mx=max(a[i],mx);
		for(int i=1;i<=n;++i)
			for(int j=a[i];j<=mx;++j)
				if(f[j-a[i]]) ++f[j];
		for(int i=1;i<=n;++i) if(f[a[i]]>=2) --ans;
		printf("%d\n",ans);
	}
	return 0;
}
