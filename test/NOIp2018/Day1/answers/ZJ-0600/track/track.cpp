#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=50001,M=100001;
char inchar(){static char c[100000],*x=c,*y=c;return x==y&&(y=(x=c)+fread(c,1,100000,stdin),x==y)?EOF:*x++;}
int read(){int c,x=0;while(!isdigit(c=inchar()));while(x=(x<<3)+(x<<1)+c-'0',isdigit(c=inchar()));return x;}
int cnt,hed[N],nex[M],rec[M],lon[M];
void add(int u,int v,int d){nex[++cnt]=hed[u],rec[cnt]=v,lon[hed[u]=cnt]=d;}
int n,m;
namespace S1{
	int h,t,q[N],d[N];
	int BFS(int x){
		int id=x;h=t=0;
		q[++t]=x; memset(d,0,sizeof d); d[x]=1;
		while(h<t) for(int u=q[++h],i=hed[u];i;i=nex[i])
			if(d[rec[i]]==0){
				d[q[++t]=rec[i]]=d[u]+lon[i];
				if(d[rec[i]]>d[id]) id=rec[i];
			}
		return id;
	}
	int calc(int x){int id=BFS(x);return d[BFS(id)]-1;}
	void solve(){printf("%d",calc(1));}
}
namespace S2{
	int a[N],b[N];
	int cmp(int a,int b){return a>b;}
	void solve(){
		int now=0;
		for(int i=1;i<=cnt;i+=2)a[++now]=lon[i];
		sort(a+1,a+now+1,cmp);
		if(m*2>=now){
			int w=0,i;
			for(i=1;i<=2*m-now&&i<=now;++i)b[++w]=a[i];
			for(int l=i,r=now;l<r;++l,--r)b[++w]=a[l]+a[r];
			sort(b+1,b+w+1,cmp);printf("%d",b[m]);
		}else{
			int w=0;
			for(int l=1,r=m*2;l<r;++l,--r)b[++w]=a[l]+a[r];
			sort(b+1,b+w+1,cmp);printf("%d",b[m]);
		}
	}
}
namespace S3{
	int ans,val[N],sum[N];
	int check(int mid){
		int now=0,pre=0;
		for(int i=1;i<n;++i)if(sum[i]-pre>=mid) pre=sum[i],++now;
		return now>=m;
	}
	void solve(){
		for(int i=1;i<=n;++i)
			for(int j=hed[i];j;j=nex[j]) if(rec[j]==i+1) val[i]=lon[j];
		for(int i=1;i<n;++i) sum[i]=sum[i-1]+val[i];
		int l=0,r=sum[n-1];
		while(l<=r){
			int mid=l+r>>1;
			if(check(mid)) ans=mid,l=mid+1; else r=mid-1;
		}
		printf("%d",ans);
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();int yes=1;
	for(int i=1;i<n;++i){
		int u=read(),v=read(),l=read();
		add(u,v,l),add(v,u,l);
		if(u!=1) yes=0;
	}
	if(m==1)S1::solve();else if(yes)S2::solve(); else S3::solve();
	return 0;
}
