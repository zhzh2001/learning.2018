#include<cstdio>
#include<cctype>
#include<cmath>
const int N=100001,S=17;
char inchar(){static char c[100000],*x=c,*y=c;return x==y&&(y=(x=c)+fread(c,1,100000,stdin),x==y)?EOF:*x++;}
int read(){int c,x=0;while(!isdigit(c=inchar()));while(x=(x<<3)+(x<<1)+c-'0',isdigit(c=inchar()));return x;}
int d[N],f[N][S];
int query(int l,int r){
	int s=log(r-l+1)/log(2);
	if(d[f[l][s]]<d[f[r-(1<<s)+1][s]])return f[l][s];
	else return f[r-(1<<s)+1][s];
}
int calc(int l,int r,int dec){
	if(l>r)return 0;
	if(l==r)return d[l]-dec;
	int k=query(l,r);
	return calc(l,k-1,d[k])+calc(k+1,r,d[k])+d[k]-dec;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n=read();
	for(int i=1;i<=n;++i) d[i]=read(),f[i][0]=i;
	for(int j=1;(1<<j)<=n;++j)
		for(int i=1;i+(1<<j)-1<=n;++i)
			if(d[f[i][j-1]]<d[f[i+(1<<j-1)][j-1]]) f[i][j]=f[i][j-1];
			else f[i][j]=f[i+(1<<j-1)][j-1];
	printf("%d",calc(1,n,0));
	return 0;
}
