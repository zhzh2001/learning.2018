#include <bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	x=0;char ch;int f=1;ch=getchar();
	for(;!isdigit(ch);) {if(ch=='-') f=-1;ch=getchar();}
	for(;isdigit(ch);) x=x*10+(ch^48),ch=getchar();
	x*=f;
}
int T;
int n,flag,ans,maxn;
int a[105],cnt[25005];
bool ok[25005];
bool dfs(int num,const int &NOT)
{
	if(ok[num]||num==0) return 1;
	for(int i=1;i<=n;i++)
	{
		if(i==NOT) continue;
		if(num-a[i]>=0&&dfs(num-a[i],NOT))
		{
			 ok[num-a[i]]=1;
			 return 1;
		}
	}
	return 0;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	for(;T--;)
	{
		flag=0;ans=0;
		for(int i=1;i<=25000;i++) cnt[i]=0,ok[i]=0;
		
		read(n);
		for(int i=1;i<=n;i++)
		{
			read(a[i]);
			cnt[a[i]]=1;
			ok[a[i]]=1;
			if(a[i]==1) flag=1;
		}
		if(flag)
		{
			puts("1");
			continue ;
		}
		for(int i=1;i<=n;i++)
		{
			for(int j=2;j*a[i]<=n;j++)
			{
				if(cnt[a[i]*j])
					cnt[a[i]*j]=0;
			}
		}
		for(int i=1;i<=n;i++)
		{
			ok[a[i]]=0;
			if(dfs(a[i],i)) cnt[a[i]]=0;
			ok[a[i]]=1;
		}
		for(int i=1;i<=25000;i++) 
			if(cnt[i]) 
				ans++;
		printf("%d\n",ans);
	}
	return 0;
}
/*
2
4
3 19 10 6
5
11 29 13 19 17
*/
