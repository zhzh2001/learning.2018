#include <bits/stdc++.h>
#define INF 0x7fffffff
using namespace std;
inline void read(int &x)
{
	x=0;char ch;int f=1;ch=getchar();
	for(;!isdigit(ch);) {if(ch=='-') f=-1;ch=getchar();}
	for(;isdigit(ch);) x=x*10+(ch^48),ch=getchar();
	x*=f;
}
const int N=50005;
struct EDGEE
{
	int nxt,to,wei;
}edge[N<<1];
int n,m,sum=0,ans;
int head[N],tot,ptot;
int dep[N],f[N][30],stp;
int q[N<<1],Head=1,tail=0;
//int dist[1005][1005];
inline void add(int x,int y,int v)
{
	edge[++tot].nxt=head[x];
	edge[tot].to=y;
	edge[tot].wei=v;
	head[x]=tot;
}
struct data
{
	int d,a,b;
	bool operator <(const data &k) const
	{
		return d<k.d;
	}
}p[1000005];

void bfs()
{
	dep[1]=1;f[1][0]=1;
	q[++tail]=1;
	while(Head<=tail)
	{
		int now=q[Head++];
		for(int i=head[now];i;i=edge[i].nxt)
		{
			int tt=edge[i].to;
			if(dep[tt]) continue;
			dep[tt]=dep[now]+1;
			f[tt][0]=now;
			for(int i=1;i<=stp;i++)
				f[tt][i]=f[f[tt][i-1]][i-1];
			q[++tail]=tt;
		}
	}
}
int lca(int x,int y)
{
	if(dep[x]<dep[y]) swap(x,y);
	for(int i=stp;i>=0;i--)
		if(dep[f[x][i]]>=dep[y]) x=f[x][i];
	if(x==y) return x;
	for(int i=stp;i>=0;i--)
		if(f[x][i]!=f[y][i]) x=f[x][i],y=f[y][i];
	return f[x][0];
}
int maxn,maxp,dis[N];
void dfs(int x,int fa)
{
	if(dis[x]>maxn)
	{
		maxn=dis[x];
		maxp=x;
	}
	for(int i=head[x];i;i=edge[i].nxt)
	{
		int tt=edge[i].to;
		if(tt==fa) continue;
		dis[tt]=dis[x]+edge[i].wei;
		dfs(tt,x);
	}
}
bool check(int x)
{
	int ll=1,rr=ptot,kkk;
	double exp=0.399;
	while(ll<=rr)
	{
		int mid=(ll+rr)>>1;
		if(p[mid].d>=x)
		{
			kkk=mid;
			rr=mid-1;
		}
		else ll=mid+1;
	}
	
	if(ptot-kkk+1<m)
		return 0;
	return 1;
}

int x,y,w;
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);stp=(int)log2(n)+1;
	for(int i=1;i<n;i++)
	{
		 read(x); read(y);read(w);
		 add(x,y,w);
		 add(y,x,w);
		 sum+=w;
	}
	if(m==1)
	{
		maxn=-INF;
		dfs(1,0);
		maxn=-INF;
		dis[maxp]=0;
		dfs(maxp,0);
		printf("%d",maxn);
		return 0;
	}
	
	bfs();dfs(1,0);
	int len;
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=i;j++)
		{
			if(i!=j)
			{
				len=dis[i]+dis[j]-2*dis[lca(i,j)];
//				printf("%d %d :%d\n",i,j,len);
				p[++ptot].d=len;
				p[ptot].a=i;
				p[ptot].b=j;
			}
		}
	}
	sort(p+1,p+ptot+1);
	int l=1,r=sum;
	while(l<=r)
	{
		int mid=(l+r)>>1;
//		cout<<mid<<endl;
		if(check(mid))
		{
			ans=mid;
			l=mid+1;
		}
		else r=mid-1;
	}
	printf("%d",ans);
	return 0;
}
