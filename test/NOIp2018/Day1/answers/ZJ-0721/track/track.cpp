#include <cstdio>
#include <cstring>
#include <algorithm>
#define ll long long
#define N 50005
#define For(i,l,r) for(int i=l;i<=r;i++)
#define Dor(i,l,r) for(int i=l;i>=r;i--)
using namespace std;

struct node{
	ll nxt;
	ll to;
	ll w;
}a[N*2];
ll n,m,head[N*2],dep[N],dis[N],dis2[N],is_root[N],cnt,fa[N][22];
void Add(ll u,ll v,ll w){
	a[++cnt].nxt=head[u];
	head[u]=cnt;
	a[cnt].to=v;
	a[cnt].w=w;
}
void dfs(ll t,ll f){
	fa[t][0]=f;dep[t]=dep[f]+1;
	for(int i=head[t];i;i=a[i].nxt){
		if (a[i].to!=f){
			dis[a[i].to]=dis[t]+a[i].w;
			dfs(a[i].to,t);
		}
	}
}
void dfs2(ll t,ll f){
	for(int i=head[t];i;i=a[i].nxt){
		if (a[i].to!=f){
			dis2[a[i].to]=dis2[t]+a[i].w;
			dfs2(a[i].to,t);
		}
	}
}
void init(){
	scanf("%lld%lld",&n,&m);
	For(i,1,n-1){
		ll x,y,z;
		scanf("%lld%lld%lld",&x,&y,&z);
		Add(x,y,z);
		Add(y,x,z);
		is_root[y]=1;
	}
	For(i,1,n) if (!is_root[i]) dfs(i,0);
	ll maxn1=0,cnt1;
	For(i,1,n) if (dis[i]>maxn1) maxn1=dis[i],cnt1=i;
	dfs2(cnt1,0);
	ll maxn2=0;
	For(i,1,n) if (dis2[i]>maxn2) maxn2=dis2[i];
	printf("%lld\n",maxn2);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	init();
	fclose(stdin);fclose(stdout);
	return 0;
}
