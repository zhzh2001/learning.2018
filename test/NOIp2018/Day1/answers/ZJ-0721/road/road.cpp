#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#define ll long long
#define For(i,l,r) for(int i=l;i<=r;i++)
#define Dor(i,l,r) for(int i=l;i>=r;i--)
#define N 100005
using namespace std;

int n;
struct node{
	int val;
	int pos;
}a[N];
bool cmp(node a,node b){
	return a.val<b.val;
}
ll ans=0,b[N];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n){
		scanf("%d",&a[i].val);
		b[i]=a[i].val;
		a[i].pos=i;
	}
	sort(a+1,a+n+1,cmp);
	For(cnt,1,n){
		if (!b[a[cnt].pos]) continue;
		int l=a[cnt].pos,r=a[cnt].pos;
		while (b[l-1]!=0) l--;
		while (b[r+1]!=0) r++;
		ans+=b[a[cnt].pos];
		int val=b[a[cnt].pos];
		For(i,l,r) b[i]-=val;	
	}
	printf("%lld\n",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}
