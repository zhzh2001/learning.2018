var
        t,n,m,i,j,ans:longint;
        flag:array[0..25005]of boolean;
        a:array[1..105]of longint;
procedure swap(var a,b:longint);
var
        c:longint;
begin
        c:=a;
        a:=b;
        b:=c;
end;
procedure sort(l,r:longint);
var
        i,j,mid:longint;
begin
        i:=l;
        j:=r;
        mid:=a[(l+r)>>1];
        repeat
                while a[i]<mid do inc(i);
                while a[j]>mid do dec(j);
                if i<=j then
                begin
                        swap(a[i],a[j]);
                        inc(i);
                        dec(j);
                end;
        until i>j;
        if i<r then sort(i,r);
        if l<j then sort(l,j);
end;
begin
        assign(input,'money.in');reset(input);
        assign(output,'money.out');rewrite(output);
        read(t);
        while t>0 do
        begin
                dec(t);
                ans:=0;
                read(n);
                for i:=1 to n do
                read(a[i]);
                sort(1,n);
                m:=a[n];
                for i:=1 to m do
                flag[i]:=false;
                for i:=1 to n do
                if flag[a[i]]=false then
                begin
                        inc(ans);
                        flag[a[i]]:=true;
                        for j:=a[i]+1 to m do
                        if flag[j-a[i]] then flag[j]:=true;
                end;
                writeln(ans);
        end;
        close(input);
        close(output);
end.