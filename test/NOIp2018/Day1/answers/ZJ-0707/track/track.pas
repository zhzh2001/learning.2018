var
        n,m,i,u,v,w,now,ans,l,r,mid,top,tot:longint;
        head,a,fa,f:array[1..50005]of longint;
        flag:array[1..50005]of boolean;
        next,go,cost:array[1..100005]of longint;
procedure add(u,v,w:longint);
begin
        inc(top);
        next[top]:=head[u];
        head[u]:=top;
        go[top]:=v;
        cost[top]:=w;
end;
procedure swap(var a,b:longint);
var
        c:longint;
begin
        c:=a;
        a:=b;
        b:=c;
end;
procedure sort(l,r:longint);
var
        i,j,mid:longint;
begin
        i:=l;
        j:=r;
        mid:=a[(l+r)>>1];
        repeat
                while a[i]<mid do inc(i);
                while a[j]>mid do dec(j);
                if i<=j then
                begin
                        swap(a[i],a[j]);
                        inc(i);
                        dec(j);
                end;
        until i>j;
        if i<r then sort(i,r);
        if l<j then sort(l,j);
end;
function doit:longint;
var
        l,r,ans1,ans2,st,le:longint;
begin
        if tot=0 then exit(0);
        if tot=1 then exit(a[tot]);
        sort(1,tot);
        if a[tot]+a[tot-1]<mid then exit(a[tot]);
        for l:=1 to tot do
        flag[l]:=true;
        for st:=1 to tot-1 do
        if a[st]+a[st+1]>=mid then break;
        l:=st;
        r:=l+1;
        ans1:=0;
        while (r<=tot)and(l>=1) do
        begin
                if a[r]+a[l]>=mid then
                begin
                        inc(ans1);
                        flag[l]:=false;
                        dec(l);
                        flag[r]:=false;
                end;
                inc(r);
        end;
        if (l=0)and(r<=tot) then
        begin
                ans1:=ans1+(tot-r+1)>>1;
                le:=((tot-r+1)>>1)<<1;
                for l:=r to r+le-1 do
                flag[l]:=false;
        end;
        now:=now+ans1;
        if tot=ans1<<1 then exit(0);
        l:=st;
        r:=l+1;
        ans2:=0;
        while (r<tot)and(l>=1) do
        begin
                if a[r]+a[l]>=mid then
                begin
                        inc(ans2);
                        dec(l);
                end;
                inc(r);
        end;
        if (l=0)and(r<=tot-1) then ans2:=ans2+((tot-r)>>1)>>1;
        if ans1=ans2 then exit(a[tot]);
        if ans1<tot-st then
        begin
                for l:=tot downto 1 do
                if flag[l] then exit(a[l]);
                exit(0);
        end;
        for l:=1 to st do
        flag[l]:=true;
        l:=1;
        for r:=tot downto st+1 do
        begin
                while (a[l]+a[r]<mid)and(l<=st) do inc(l);
                flag[l]:=false;
        end;
        for l:=st downto 1 do
        if flag[l] then exit(a[l]);
end;
procedure dfs(u:longint);
var
        i,v:longint;
begin
        if now>=m then exit;
        i:=head[u];
        while i>0 do
        begin
                v:=go[i];
                if v=fa[u] then
                begin
                        i:=next[i];
                        continue;
                end;
                fa[v]:=u;
                dfs(v);
                i:=next[i];
        end;
        tot:=0;
        i:=head[u];
        while i>0 do
        begin
                v:=go[i];
                if v=fa[u] then
                begin
                        i:=next[i];
                        continue;
                end;
                if f[v]+cost[i]<mid then
                begin
                        inc(tot);
                        a[tot]:=f[v]+cost[i];
                end
                else inc(now);
                i:=next[i];
        end;
        f[u]:=doit;
end;
begin
        assign(input,'track.in');reset(input);
        assign(output,'track.out');rewrite(output);
        read(n,m);
        for i:=1 to n-1 do
        begin
                read(u,v,w);
                add(u,v,w);
                add(v,u,w);
                r:=r+w;
        end;
        l:=1;
        while l<=r do
        begin
                mid:=(l+r)>>1;
                now:=0;
                for i:=1 to n do
                f[i]:=0;
                dfs(1);
                if now>=m then
                begin
                        l:=mid+1;
                        ans:=mid;
                end
                else r:=mid-1;
        end;
        writeln(ans);
        close(input);
        close(output);
end.
