type rrr=record
  t,next,w:longint;
end;
var
  e:array[0..100010]of rrr;
  head,vis:array[0..100010]of longint;
  maxn,x,i,n,m,y,w,xx:longint;
  tot,ans:int64;
procedure dfs1(x,cnt:longint);
var i:longint;
begin
  if cnt>maxn then begin maxn:=cnt; xx:=x;end;
  i:=head[x];
  while i<>0 do
    begin

      if vis[e[i].t]=0 then begin
       vis[e[i].t]:=1;
       dfs1(e[i].t,cnt+e[i].w);
       end;
      i:=e[i].next;

    end;
end;
procedure dfs2(x,cnt:longint);
var i:longint;
begin
  if cnt>maxn then maxn:=cnt;
  i:=head[x];
  while i<>0 do
    begin
      if vis[e[i].t]=0 then begin
      vis[e[i].t]:=1;
      dfs2(e[i].t,cnt+e[i].w);
      end;
      i:=e[i].next;
    end;
end;
procedure add(u,v,w:longint);
begin
  inc(tot);
  e[tot].t:=v;
  e[tot].w:=w;
  e[tot].next:=head[u];
  head[u]:=tot;
end;
begin
assign(input,'track.in');
reset(input);
assign(output,'track.out');
rewrite(output);
  readln(n,m);
  for i:=1 to n-1 do
    begin
      readln(x,y,w);
      add(x,y,w);
      add(y,x,w);
      ans:=ans+w;
    end;
  if m=1 then
    begin
      dfs1(1,0);
      fillchar(vis,sizeof(vis),0);
      dfs2(xx,0);
      writeln(maxn);
    end  else
  writeln((ans div m)-1);
close(input);
close(output);
end.