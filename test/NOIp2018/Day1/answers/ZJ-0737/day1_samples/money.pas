var
  t,n,i,j,k,tot:longint;
  f:array[0..30000]of longint;
  a:array[0..200]of longint;
  procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
{assign(input,'money.in');
reset(input);
assign(output,'money.out');
rewrite(output); }
  readln(t);
  while t>0 do
    begin
      dec(t);
      readln(n);
      fillchar(f,sizeof(f),0);
      tot:=0;
      for i:=1 to n do
        read(a[i]);
      sort(1,n);
      if a[1]=1 then begin writeln(1); continue; end;
      for i:=0 to a[n] div a[1] do
        f[a[1]*i]:=1;
      for i:=2 to n-1 do
        if f[a[i]]=0 then begin
        for j:=a[i] to a[n] do
          begin
            if f[j]=1 then continue;
            for k:=1 to j div a[i] do
              if f[j-k*a[i]]=1 then begin f[j]:=1; break;end;
          end;
          end else inc(tot);
      if (f[a[n]]=1)and(n<>1) then inc(tot);
      writeln(n-tot);
    end;
{close(input);
close(output); }
end.