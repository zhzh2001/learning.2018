#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#define FOR(i,a,b) for(int i=(a),i##_END_=(b);i<=i##_END_;i++)
#define ROF(i,a,b) for(int i=(a),i##_END_=(b);i>=i##_END_;i--)
#define trav(i,x)  for(int i=head[x]; ~i; i=Nxt[i])
template<typename T,typename _>inline void Max(T &x,_ y) {x=x>y?x:y;}
template<typename T,typename _>inline void Min(T &x,_ y) {x=x<y?x:y;}
template<typename T>void Rd(T &x) {
	static char c;x=0;
	while((c=getchar())<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while((c=getchar())>47);
}
#define N 50005
int n,m;
int head[N],Nxt[N<<1],To[N<<1],Len[N<<1],cntE=-1;
inline void add_edge(const int from,const int to,const int cost) {
	Nxt[++cntE]=head[from];
	To[cntE]=to;
	Len[cntE]=cost;
	head[from]=cntE;
}
int fa[N],dep[N];
namespace Pm1 {
	int mxfis[N],mxsec[N];
	int ans=0;
	void Build(int x,int f) {
		fa[x]=f;
		int y;
		trav(i,x)if((y=To[i])!=f) {
			Build(y,x);
			int nowsum=mxfis[y]+Len[i];
			if(nowsum>=mxfis[x]) {
				mxsec[x]=mxfis[x];
				mxfis[x]=nowsum;
			} else if(nowsum>mxsec[i]) {
				mxsec[x]=nowsum;
			}
		}
		Max(ans,mxfis[x]+mxsec[x]);
	}
	void solve() {
		Build(1,0);
		printf("%d\n",ans);
	}
}
namespace Pai1 {
	void solve() {
		int ans=0;
		std::sort(Len,Len+cntE+1);
		for(int i=cntE,cntm=1;i>=0&&cntm<=2*m;i-=2,cntm++)ans+=Len[i];
		printf("%d\n",ans);
	}
}
namespace Plink {
	int A[N];
	bool check(int x) {
		int cnt=0,sum=0;
		FOR(i,1,n-1) {
			sum+=A[i];
			if(sum>=x)++cnt,sum=0;
		}
		return cnt>=m;
	}
	void solve() {
		FOR(i,1,n-1)A[i]=Len[head[i]];
		int l=0,r=6e8,ans=0;
		while(l<=r) {
			int mid=(l+r)>>1;
			if(check(mid))l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}
#include<vector>
namespace Punknown {
	int res[N];
	bool vis[N];
	int dfs(int x,int f,int num) {
		int y;
		int tmp;
		std::vector<int>mx;
		trav(i,x)if((y=To[i])!=f) {
			tmp=Len[i]+dfs(y,x,num);
			if(tmp>=num)res[x]++;
			else mx.push_back(tmp);
			res[x]+=res[y];
		}
		std::sort(mx.begin(),mx.end());
		memset(vis,0,sizeof vis);
		FOR(i,0,mx.size()-1) {
			int tt=lower_bound(mx.begin(),mx.end(),num-mx[i])-mx.begin();
			while(vis[tt])tt++;
			if(tt<i)vis[i]=vis[tt]=1,res[x]++;
		}
		ROF(i,mx.size()-1,0)if(!vis[i])return mx[i];
		return 0;
	}
	bool check(int x) {
		memset(res,0,sizeof res);
		dfs(1,0,x);
		return res[1]>=m;
	}
	void solve() {
		int l=0,r=6e8,ans=0;
		while(l<=r) {
			int mid=(l+r)>>1;
			if(check(mid))l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(head,-1,sizeof head);
	int x,y,z;
	Rd(n),Rd(m);
	bool ifai1=true;
	bool iflink=true;
	FOR(i,1,n-1) {
		Rd(x),Rd(y),Rd(z);
		add_edge(x,y,z);
		add_edge(y,x,z);
		ifai1&=(x==1);
		iflink&=(y==x+1);
	}
	if(m==1)Pm1::solve();
	else if(ifai1)Pai1::solve();
	else if(iflink)Plink::solve();
	else Punknown::solve();
	return 0;
}
