#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#define FOR(i,a,b) for(int i=(a),i##_END_=(b);i<=i##_END_;i++)
#define ROF(i,a,b) for(int i=(a),i##_END_=(b);i>=i##_END_;i--)
template<typename T,typename _>inline void Max(T &x,_ y) {x=x>y?x:y;}
template<typename T,typename _>inline void Min(T &x,_ y) {x=x<y?x:y;}
template<typename T>void Rd(T &x) {
	static char c;x=0;
	while((c=getchar())<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while((c=getchar())>47);
}

int gcd(int x,int y) {
	return y?gcd(y,x%y):x;
}
void exgcd(int a,int b,long long &x,long long &y) {
	if(!b) {x=1,y=0;return;}
	exgcd(b,a%b,y,x);
	y-=(a/b)*x;
}
#define N 105
int A[N];
bool vis[N];
int n;
int dp[N][25005];
bool dfs(int x,int num) {
	if(!x)return false;
	if(~dp[x][num])return dp[x][num];
	if(!(num%A[x]))return (dp[x][num]=true);
	if(dfs(x-1,num))return (dp[x][num]=true);
	int real=num;
	while(num>A[x]) {
		num-=A[x];
		if(dfs(x-1,num))return (dp[x][real]=true);
	}
	return (dp[x][real]=false);
}

int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T,m;
	Rd(T);
	while(T--) {
		Rd(n);
		FOR(i,1,n)Rd(A[i]),vis[i]=0;
		std::sort(A+1,A+n+1);
		FOR(i,1,n)FOR(j,1,i-1)if(!(A[i]%A[j])) {vis[i]=1;break;}
		m=0;
		FOR(i,1,n)if(!vis[i])A[++m]=A[i];
		if(m==1) {puts("1");continue;}
		n=m;
		int mi=1e9;
		FOR(i,1,n)FOR(j,1,i-1)if(gcd(A[i],A[j])==1)
			Min(mi,A[i]*A[j]);
		while(A[n]>=mi)n--;
		if(n==1) {puts("1");continue;}
		FOR(i,1,n)vis[i]=0;
		ROF(i,n,1) {
			FOR(j,1,i-2) {
				FOR(k,j+1,i-1) {
					long long x,y,d;
					if((d=gcd(A[k],A[j]))!=1) {
						if(d!=gcd(d,A[i]))
							continue;
						int kk=A[k]/d;
						int jj=A[j]/d;
						int ii=A[i]/d;
						exgcd(kk,jj,x,y);
						x=(x*ii%jj+jj)%jj;
						if(kk*x<ii) {vis[i]=1;break;}
						continue;
					}
					exgcd(A[k],A[j],x,y);
					x=(x*A[i]%A[j]+A[j])%A[j];
					if(A[k]*x<A[i]) {vis[i]=1;break;}
				}
				if(vis[i])break;
			}
		}
		m=0;
		FOR(i,1,n)if(!vis[i])A[++m]=A[i];
		n=m;
//		FOR(i,1,n)printf("%d ",A[i]);puts("");
		memset(dp,-1,sizeof dp);
		ROF(i,n,3)if(dfs(i-1,A[i]))n--;
		printf("%d\n",n);
	}
	return 0;
}

