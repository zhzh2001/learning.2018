#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cmath>
#include<cstring>
#define rep(i,a,n) for (int i=a;i<=n;i++)
#define per(i,a,n) for (int i=a;i>=n;i--)
#define Clear(a,x) memset(a,x,sizeof(a))
#define ll long long
#define INF 2000000000
using namespace std;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') f=ch=='-'?-1:f,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*f;
}
const int maxn=100005;
int n,ans;
int d[maxn];
struct node{
	int w,id;
}f[maxn][20];
node mn(node a,node b){
	if (a.w<b.w) return a;
		else return b;
}
void init(){
	rep(i,1,n)
		f[i][0].w=d[i],f[i][0].id=i;
	rep(j,1,17)
		for (int i=1;i+(1<<j)-1<=n;i++)
			f[i][j]=mn(f[i][j-1],f[i+(1<<(j-1))][j-1]);
}
node find(int l,int r){
	int p=floor(log(r-l+1)/log(2));
	return mn(f[l][p],f[r-(1<<p)+1][p]);
}
void solve(int l,int r,int der){
	if (l>r) return;
	node tmp=find(l,r);
	ans+=d[tmp.id]-der;
	solve(l,tmp.id-1,d[tmp.id]);
	solve(tmp.id+1,r,d[tmp.id]);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	rep(i,1,n) d[i]=read();
	init();
	solve(1,n,0);
	printf("%d\n",ans);
	return 0;
}
