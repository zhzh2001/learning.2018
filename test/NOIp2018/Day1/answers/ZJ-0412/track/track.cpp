#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<vector>
#define rep(i,a,n) for (int i=a;i<=n;i++)
#define per(i,a,n) for (int i=a;i>=n;i--)
#define Clear(a,x) memset(a,x,sizeof(a))
#define ll long long
#define INF 2000000000
using namespace std;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') f=ch=='-'?-1:f,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*f;
}
const int maxn=50005;
int n,m,ans,len,num;
int vet[maxn<<1],Next[maxn<<1],head[maxn<<1],road[maxn<<1];
int rd[maxn],d[maxn],dag[maxn];
bool flag1,flag2,flag3;
bool cmp(int a,int b){
	return a>b;
}
void add(int u,int v,int w){
	vet[++len]=v;
	Next[len]=head[u];
	head[u]=len;
	road[len]=w;
}
int dfs(int u,int fa,int d){
	int Max1=d,Max2=0;
	for (int e=head[u];e;e=Next[e]){
		int v=vet[e];
		if (v==fa) continue;
		int tmp=dfs(v,u,d+road[e]);
		if (tmp>=Max1){
			Max2=Max1;
			Max1=tmp;
		}else if (tmp>Max2) Max2=tmp;
	}
	ans=max(ans,Max1+Max2-d*2);
	return Max1;
}
int DFS(int u,int fa,int d,int k){
	int ls=0,rs=0,re=0;
	for (int e=head[u];e;e=Next[e]){
		int v=vet[e];
		if (v==fa) continue;
		int tmp=DFS(v,u,d+road[e],k);
		if (ls==0) ls=tmp;
			else rs=tmp;
	}
	if (ls-d>=k) num++,ls=0;
	if (rs-d>=k) num++,rs=0;
	if (ls+rs-2*d>=k) num++,ls=0,rs=0;
	re=max(max(ls,rs),d);
	return re;
}
bool ok(int k){
	int i=1,j=n-1,num=0;
	while (i<n) 
		if (rd[i]>=k) i++,num++;
			else break;
	while (i<j){
		if (rd[i]+rd[j]>=k) i++,j--,num++;
			else j--;
	}
	return num>=m;
}
bool hk(int k){
	int sum=0,num=0;
	rep(i,1,n-1){
		sum+=d[i];
		if (sum>=k) num++,sum=0;
	}
	return num>=m;
}
bool pd(int k){
	num=0;
	DFS(1,0,0,k);
	return num>=m;
}
void solve4(){
	int l=1,r=INF;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (pd(mid)) l=mid;
			else r=mid-1;
	}
	printf("%d\n",l);
}
void solve3(){
	int l=1,r=INF;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (hk(mid)) l=mid;
			else r=mid-1;
	}
	printf("%d\n",l);
}
void solve2(){
	sort(rd+1,rd+n,cmp);
	int l=0,r=INF;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (ok(mid)) l=mid;
			else r=mid-1;
	}
	printf("%d\n",l);
}
void solve1(){
	dfs(1,0,0);
	printf("%d\n",ans);
}
int find(int u,int fa,int d,int k){
	int Max=0;
	vector<int> dis;
	for (int e=head[u];e;e=Next[e]){
		int v=vet[e];
		if (v==fa) continue;
		int tmp=find(v,u,d+road[e],k);
		dis.push_back(tmp);
	}
	int Long=dis.size();
	sort(dis.begin(),dis.end(),cmp);
	int i=0,j=Long-1;
	while (i<Long) 
		if (dis[i]-d>=k) num++,i++;
			else break;
	while (i<j){
		if (dis[i]+dis[j]-2*d>=k)
			num++,i++,j--;
		else{
			Max=max(Max,dis[j]);
			j--;
		}
	}
	if (i==j) Max=max(dis[i],Max);
	return max(Max,d);
}
bool calc(int k){
	num=0;
	find(1,0,0,k);
	return num>=m;
}
void solve(){
	int l=1,r=INF;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (calc(mid)) l=mid;
			else r=mid-1;
	}
	printf("%d\n",l);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	flag1=1,flag2=1,flag3=1;
	n=read(),m=read();
	rep(i,1,n-1){
		int u=read(),v=read(),w=read();
		rd[i]=w;d[u]=w;dag[u]++,dag[v]++;
		add(u,v,w),add(v,u,w);
		if (u!=1) flag1=0;
		if (v!=u+1) flag2=0;
	}
	rep(i,1,n)
		if (dag[i]>3) flag3=0;
	if (m==1) solve1();
		else if (flag1) solve2();
			else if (flag2) solve3();
				else if (flag3) solve4();
					else solve();
	return 0;
}
