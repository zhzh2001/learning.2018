#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cmath>
#include<cstring>
#define rep(i,a,n) for (int i=a;i<=n;i++)
#define per(i,a,n) for (int i=a;i>=n;i--)
#define Clear(a,x) memset(a,x,sizeof(a))
#define ll long long
#define INF 2000000000
using namespace std;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') f=ch=='-'?-1:f,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*f;
}
int n,N;
int a[105];
int f[25005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while (T--){
		n=read();
		rep(i,1,n) a[i]=read();
		sort(a+1,a+n+1);
		if (a[1]==1||n==1){
			puts("1");
			continue;
		}
		Clear(f,0);
		N=a[n];
		int ans=0;
		rep(i,1,n){
			if (f[a[i]]==0) f[a[i]]=1,ans++;
				else continue;
			rep(j,1,N)
				if (j+a[i]<=N){
					if (f[j]==1) f[j+a[i]]=1;
				}else break;
		}
		printf("%d\n",ans);
	}
	return 0;
}
