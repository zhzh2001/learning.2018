#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++)
#define Out() (fwrite(St,1,Top,stdout));
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
using namespace std;
int Top,fn;static char buf[1000000],St[1000000],*p1=buf,*p2=buf;
const int maxn=105,maxp=(1e7)+5;
int T,n,S,a[maxn],Ans;bool f[maxp],g[maxp];
int read(){
	int ret=0;char ch=gt();
	while(ch<'0'||ch>'9') ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return ret;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		n=read();
		for(int i=1;i<=n;i++) a[i]=read();sort(a+1,a+1+n),n=unique(a+1,a+1+n)-a-1;
		Ans=n,f[0]=1;
		for(int i=1;i<=n;i++){
			if(f[a[i]]){Ans--;continue;}
			for(int j=a[i];j<=a[n];j++) f[j]|=f[j-a[i]];
		}
		for(int i=1;i<=a[n];i++) f[i]=0;
		printf("%d\n",Ans);
	}
	return 0;
}
