#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++)
#define Out() (fwrite(St,1,Top,stdout));
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
using namespace std;
int Top,fn;static char buf[1000000],St[1000000],*p1=buf,*p2=buf;
const int maxn=(1e5)+5;
int n,m,Ans,L=1,R,mid,cnt,f[maxn],fa[maxn],p[maxn];
int tot,lnk[maxn],nxt[maxn<<1],son[maxn<<1],w[maxn<<1];
void add_e(int x,int y,int z){son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot,w[tot]=z;}
int read(){
	int ret=0;char ch=gt();
	while(ch<'0'||ch>'9') ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return ret;
}
int getfa(int x){return fa[x]==x?x:fa[x]=getfa(fa[x]);}
void DFS(int x,int dad){
	for(int j=lnk[x];j;j=nxt[j]) if(son[j]!=dad) DFS(son[j],x);
	p[0]=0;
	for(int j=lnk[x];j;j=nxt[j]) if(son[j]!=dad){
		if(f[son[j]]+w[j]>=mid){cnt++;continue;}
		p[++p[0]]=f[son[j]]+w[j];
	}
	for(int i=1;i<=p[0]+2;i++) fa[i]=i;
	sort(p+1,p+1+p[0]);
	int i,j;
	for(i=1,j=p[0];i<j;i=getfa(i+1)){
		while(i<j&&p[i]+p[j]>=mid) j--;
		int k=getfa(j+1);
		if(k>p[0]) continue;
		cnt++,fa[k]=getfa(k+1);
	}
	int tot=0;
	for(int k=j+1;k<=p[0];k++) if(getfa(k)==k) tot++;
	tot-=(tot&1),cnt+=tot>>1;
	for(int k=j+1;tot&&k<=p[0];k++) if(getfa(k)==k) fa[k]=getfa(k+1),tot--;
	f[x]=0;
	for(int i=p[0];i;i--) if(getfa(i)==i){f[x]=p[i];break;}
}
bool check(int x){
	cnt=0;
	DFS(1,0);
	return (cnt>=m);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read(),z=read();R+=z;
		add_e(x,y,z),add_e(y,x,z);
	}
	R/=m;
	while(L<=R){
	   mid=L+R>>1;
	   if(check(mid)) Ans=mid,L=mid+1;else R=mid-1;
    }
	printf("%d\n",Ans);
	return 0;
}
