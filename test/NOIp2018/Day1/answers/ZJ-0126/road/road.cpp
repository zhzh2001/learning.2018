#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++)
#define Out() (fwrite(St,1,Top,stdout));
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
#define LL long long
using namespace std;
int Top,fn;static char buf[1000000],St[1000000],*p1=buf,*p2=buf;
const int maxn=(1e5)+5;
int n,stk[maxn],top;LL Ans;
int read(){
	int ret=0;char ch=gt();
	while(ch<'0'||ch>'9') ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return ret;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		int x=read();
		if(x==stk[top]) continue;
		if(x>stk[top]) stk[++top]=x;
		else{
			Ans+=stk[top]-x;
			while(top&&stk[top]>=x) top--;
			stk[++top]=x;
		}
	}
	printf("%lld\n",Ans+stk[top]);
	return 0;
}
