#include<bits/stdc++.h>
using namespace std;

int a[100005]={0};

int read()
{
	
	int a=0;
	bool w=0;
	char ch=0;
	while(!isdigit(ch)){w|=ch=='-';ch=getchar();}
	while(isdigit(ch))
	{
		a=(a<<1)+(a<<3)+(ch^48);
		ch=getchar();
	}
	return w?-a:a;
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n;
	n=read();
	for(int i=1;i<=n;i++)
	{
		a[i]=read();
	}
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		if(a[i]>a[i-1])
		ans+=a[i]-a[i-1];
	}
	cout<<ans;
}
