#include<bits/stdc++.h>
using namespace std;
const int N=50005;
int la[N],to[N*2],pr[N*2],v[N*2],cnt;
int f[N],g[N];
int q[N],m;
int n,K,len,x,y,z,ans,M;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void add(int x,int y,int z)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
	v[cnt]=z;
}
inline bool check(int x)
{
	for (int i=1;i<=x;++i)
		if (q[i]+q[x*2+1-i]<K)
			return 0;
	return 1;
}
inline bool pd(int x,int y)
{
	int l=1,r=x*2+1;
	for (int i=1;i<=x;++i)
	{
		if (l==y)
			++l;
		if (r==y)
			--r;
		if (q[l]+q[r]<K)
			return 0;
		++l;
		--r;
	}
	return 1;
}
inline void dfs(int x,int y)
{
	f[x]=g[x]=0;
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=y)
			dfs(to[i],x);
	m=0;
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=y)
		{
			if (g[to[i]]+v[i]>=K)
				++f[x];
			else
				q[++m]=g[to[i]]+v[i];
			f[x]+=f[to[i]];
		}
	if (m!=0)
	{
		int sum=0;
		sort(q+1,q+1+m,greater<int>());
		int l=0,r=m/2;
		while (l<=r)
		{
			int mid=(l+r)/2;
			if (check(mid))
			{
				sum=mid;
				l=mid+1;
			}
			else
				r=mid-1;
		}
		if (sum*2==m)
			g[x]=0;
		else
		{
			if (sum==0)
				g[x]=q[1];
			else
			{
				l=1,r=sum*2+1;
				while (l<=r)
				{
					int mid=(l+r)/2;
					if (pd(sum,mid))
					{
						g[x]=q[mid];
						r=mid-1;
					}
					else
						l=mid+1;
				}
			}
		}
		f[x]+=sum;
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);
	read(M);
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		read(z);
		add(x,y,z);
		add(y,x,z);
		len+=z;
	}
	int l=1,r=len/M;
	while (l<=r)
	{
		K=(l+r)/2;
		dfs(1,0);
		if (f[1]>=M)
		{
			ans=K;
			l=K+1;
		}
		else
			r=K-1;
	}
	// cout<<clock()<<' ';
	cout<<ans;
	return 0;
}