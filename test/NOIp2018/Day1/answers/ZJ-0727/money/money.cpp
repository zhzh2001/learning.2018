#include<bits/stdc++.h>
using namespace std;
const int M=25005,N=105;
int f[M],a[N],n,ans,mx,t;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(t);
	f[0]=1;
	while (t--)
	{
		read(n);
		for (int i=1;i<=n;++i)
			read(a[i]);
		sort(a+1,a+1+n);
		mx=a[n];
		ans=n;
		for (int i=1;i<=n;++i)
		{
			if (f[a[i]])
				--ans;
			else
			{
				for (int j=a[i];j<=mx;++j)
					f[j]|=f[j-a[i]];
			}
		}
		for (int i=1;i<=mx;++i)
			f[i]=0;
		cout<<ans<<endl;
	}
	return 0;
}