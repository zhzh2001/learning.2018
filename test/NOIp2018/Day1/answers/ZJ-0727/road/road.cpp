#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=100005;
int t[N*4],a[N],n;
ll ans;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline int mn(int x,int y)
{
	return a[x]<a[y]?x:y;
}
inline void build(int l,int r,int x)
{
	if (l==r)
	{
		t[x]=l;
		return;
	}
	int mid=(l+r)/2;
	build(l,mid,x*2);
	build(mid+1,r,x*2+1);
	t[x]=mn(t[x*2],t[x*2+1]);
}
inline int ask(int l,int r,int L,int R,int x)
{
	if (l==L&&r==R)
		return t[x];
	int mid=(l+r)/2;
	if (R<=mid)
		return ask(l,mid,L,R,x*2);
	if (L>mid)
		return ask(mid+1,r,L,R,x*2+1);
	return mn(ask(l,mid,L,mid,x*2),ask(mid+1,r,mid+1,R,x*2+1));
}
inline void work(int l,int r,int k)
{
	if (l>r)
		return;
	int p=ask(1,n,l,r,1);
	ans+=a[p]-k;
	work(l,p-1,a[p]);
	work(p+1,r,a[p]);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
		read(a[i]);
	build(1,n,1);
	work(1,n,0);
	cout<<ans;
	return 0;
}