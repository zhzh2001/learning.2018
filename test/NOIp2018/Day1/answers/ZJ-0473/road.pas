var
  n,i:longint;
  x,temp,ans:int64;
begin
  assign(input,'road.in');
  reset(input);
  assign(output,'road.out');
  rewrite(output);
  readln(n);
  for i:=1 to n do
  begin
    read(x);
    if x>temp then ans:=ans+x-temp;
    temp:=x;
  end;
  writeln(ans);
  close(input);
  close(output);
end.