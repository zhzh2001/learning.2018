var
  n,m,i,x,y,z,ans,temp,k:longint;
  l,mid,r:int64;
  first,next,go,num,fa,long,du:array[0..300000]of longint;
procedure add(x,y,z:longint);
begin
  inc(du[x]);
  inc(k);
  next[k]:=first[x];
  first[x]:=k;
  go[k]:=y;
  num[k]:=z;
end;
procedure build(x:longint);
var
  t:longint;
begin
  t:=first[x];
  while t<>0 do
  begin
    if go[t]=fa[x] then
    begin
      t:=next[t];
      continue;
    end;
    fa[go[t]]:=x;
    long[go[t]]:=num[t];
    build(go[t]);
    t:=next[t];
  end;
end;
function work(x,len:longint):longint;
var
  t,temp,max1,max2:longint;
begin
  max1:=0;
  max2:=0;
  temp:=0;
  t:=first[x];
  while t<>0 do
  begin
    if go[t]=fa[x] then
    begin
      t:=next[t];
      continue;
    end;
    temp:=work(go[t],len);
    if temp>max1 then
    begin
      max2:=max1;
      max1:=temp;
    end else
    if temp>max2 then
    max2:=temp;
    t:=next[t];
  end;
  if max1+max2>=len then
  begin
    inc(ans);
    if long[x]>=len then
    begin
      inc(ans);
      exit(0);
    end else
    exit(long[x]);
  end else
  begin
    if long[x]+max1>=len then
    begin
      inc(ans);
      exit(0);
    end else
    exit(long[x]+max1);
  end;
end;
function work2(x:longint):longint;
var
  t,temp,max1,max2:longint;
begin
  max1:=0;
  max2:=0;
  temp:=0;
  t:=first[x];
  while t<>0 do
  begin
    if go[t]=fa[x] then
    begin
      t:=next[t];
      continue;
    end;
    temp:=work2(go[t]);
    if temp>max1 then
    begin
      max2:=max1;
      max1:=temp;
    end else
    if temp>max2 then
    max2:=temp;
    t:=next[t];
  end;
  if max1+max2>ans then ans:=max1+max2;
  if max1+long[x]>ans then ans:=max1+long[x];
  exit(long[x]+max1);
end;
begin
  assign(input,'track.in');
  reset(input);
  assign(output,'track.out');
  rewrite(output);
  readln(n,m);
  for i:=1 to n-1 do
  begin
    readln(x,y,z);
    add(x,y,z);
    add(y,x,z);
  end;
  for i:=1 to n do
  if du[i]>3 then break;
  if du[i]<=3 then
  begin
    for i:=1 to n do
    if du[i]<3 then break;
    build(i);
    l:=0;
    r:=maxlongint;
    while l<r do
    begin
      mid:=l+((r-l)>>1);
      ans:=0;
      temp:=work(i,mid);
      if ans<m then r:=mid else l:=mid+1;
    end;
    writeln(l-1);
  end else
  if m=1 then
  begin
    build(1);
    temp:=work2(1);
    writeln(ans);
  end;
  close(input);
  close(output);
end.
