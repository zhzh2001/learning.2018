#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int rd(void){
	int res=0;char c;
	while(c=getchar(),c>'9'||c<'0');
	while(c>='0'&&c<='9') res=res*10+c-'0',c=getchar();
	return res;
}
const int N=110,M=25500;
int T,n;
int a[N];
int f[M];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++){
			scanf("%d",&a[i]);
		}
		sort(a+1,a+1+n);
		int lim=a[n],res=n;
		memset(f,0,sizeof(f));
		f[0]=1;
		for(int i=1;i<=n;i++){
			if(f[a[i]]){
				res--;
				continue;
			}
			for(int j=0;j<=lim;j++){
				if(f[j]&&j+a[i]<=lim){
					f[j+a[i]]=1;
				}
			}
		}
		printf("%d\n",res);
	}
	return 0;
}
