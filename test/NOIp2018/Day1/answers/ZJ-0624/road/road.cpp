#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;
inline int rd(void){
	int res=0;char c;
	while(c=getchar(),c>'9'||c<'0');
	while(c>='0'&&c<='9') res=res*10+c-'0',c=getchar();
	return res;
}
const int N=1E5+10;
int n;
int a[N];
vector<int>f[N];
long long ans=0;
struct node{int L,R,mi;}p[N<<2];
inline void pl(int l=1,int r=n,int x=1){
	p[x].L=l;
	p[x].R=r;
	if(l==r){
		p[x].mi=a[l];
		return;
	}
	int mid=l+r>>1;
	pl(l,mid,x<<1);
	pl(mid+1,r,x<<1|1);
	p[x].mi=min(p[x<<1].mi,p[x<<1|1].mi);
}
inline int find(int l,int r,int x=1){
	if(p[x].L==l&&p[x].R==r) return p[x].mi;
	int mid=p[x].L+p[x].R>>1;
	if(r<=mid) return find(l,r,x<<1);
	else if(l>mid) return find(l,r,x<<1|1);
	else return min(find(l,mid,x<<1),find(mid+1,r,x<<1|1));
}
inline int ck(int x,int pos){
	int l=0,r=f[x].size()-1,res=r+1;
	while(l<=r){
		int mid=l+r>>1;
		if(f[x][mid]>=pos){
			res=mid;
			r=mid-1;
		}else l=mid+1;
	}
}
inline void sol(int l,int r,int sum){
	if(l>r) return;
	int mi=find(l,r);
//	printf("%d %d %d\n",l,r,sum);
	int fr=ck(mi,l),las=l,gg=f[mi].size();
	ans+=mi-sum;
	while(fr<gg&&f[mi][fr]<=r){
		sol(las,f[mi][fr]-1,mi);
		las=f[mi][fr]+1;
		fr++;
	}
	sol(las,r,mi);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%d",&a[i]);
		f[a[i]].push_back(i);
	}
	pl();
	sol(1,n,0);
	printf("%lld",ans);
	return 0;
}
