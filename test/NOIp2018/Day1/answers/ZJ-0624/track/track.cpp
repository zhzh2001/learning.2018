#include<cstdio>
#include<cstring>
#include<set>
#include<algorithm>
using namespace std;
inline int rd(void){
	int res=0;char c;
	while(c=getchar(),c>'9'||c<'0');
	while(c>='0'&&c<='9') res=res*10+c-'0',c=getchar();
	return res;
}
const int N=5E4+10;
int n,m;
int hd[N],nxt[N<<1],to[N<<1],vl[N<<1],cnt=0;
inline void init(int x,int y,int v){
	cnt++;
	nxt[cnt]=hd[x];
	hd[x]=cnt;
	to[cnt]=y;
	vl[cnt]=v;
}
int len;
int f[N],s[N],q[N];
multiset<int>g;
inline void dfs(int x,int pre){
	int fl=0;s[x]=f[x]=0;int ma=0;
	for(int i=hd[x];i;i=nxt[i]){
		int u=to[i];
		if(u!=pre){
			dfs(u,x);
			s[x]+=s[u];
			fl++;
			ma=max(ma,f[u]+vl[i]);
		}
	}
	if(!fl){
		f[x]=s[x]=0;
		return;
	}
	if(fl>20){
		g.clear();
		for(int i=hd[x];i;i=nxt[i]){
			int u=to[i];
			if(u!=pre){
				if(f[u]+vl[i]>=len) s[x]++;
				else if(ma+f[u]+vl[i]>=len){
					g.insert(f[u]+vl[i]);
				}else f[x]=max(f[x],f[u]+vl[i]);
			}
		}
		while(1){
			if(g.size()<2) break;
			int mi=*g.begin();
			g.erase(g.begin());
			multiset<int>::iterator i=g.lower_bound(len-mi);
			if(i!=g.end()){
				s[x]++;
				g.erase(i);
			}else f[x]=mi;
		}
		if(g.size()) f[x]=*g.begin();
	}else{
		int hh=0;
		for(int i=hd[x];i;i=nxt[i]){
			int u=to[i];
			if(u!=pre){
				if(f[u]+vl[i]>=len) s[x]++;
				else if(ma+f[u]+vl[i]>=len){
					q[++hh]=f[u]+vl[i];
				}else f[x]=max(f[x],f[u]+vl[i]);
			}
		}
		sort(q+1,q+1+hh);
		for(int i=1;i<=hh;i++){
			if(!q[i]) continue;
			for(int j=i+1;j<=hh;j++){
				if(!q[j]) continue;
				if(q[i]+q[j]>=len){
					q[i]=q[j]=0;
					s[x]++;
					break;
				}
			}
		}
		for(int i=hh;i>=1;i--) if(q[i]){
			f[x]=q[i];
			return;
		}
	}
}
inline int ck(int x){
	len=x;
	dfs(1,1);
	return s[1]>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		init(x,y,v);
		init(y,x,v);
	}
	int l=1,r=1E9,res=0;
	while(l<=r){
		int mid=l+r>>1;
		if(ck(mid)){
			res=mid;
			l=mid+1;
		}else r=mid-1;
	}
	printf("%d\n",res);
	return 0;
}
