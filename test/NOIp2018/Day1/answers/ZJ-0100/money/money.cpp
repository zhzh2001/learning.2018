#include <cstdio>
#include <algorithm>
using namespace std;
int T,n,cnt;
int a[200];
int mark[25100];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		cnt=n;
		for (int i=1;i<=a[n];i++) mark[i]=0;
		for (int i=1;i<=n;i++) mark[a[i]]=1;
		for (int i=1;i<=a[n];i++)
			if (mark[i]>0)
				for (int j=1;j<=n;j++)
					if (i+a[j]>a[n]) break;
					else{
						if (mark[i+a[j]]==1) cnt--,mark[i+a[j]]=2;
						if (mark[i+a[j]]==0) mark[i+a[j]]=2;
					}
		printf("%d\n",cnt);
	}
	return 0;
}
