#include <cstdio>
#include <algorithm>
using namespace std;
int n,m;
struct edge{
	int a,b,w,nxt;
}e[101000];
int head[50100],cnt;
int at[50100],bt[50100],wt[50100],chud[50100]={0};
int da[40100]={0},db[40100]={0};
int ans,s[20];
bool flg1,flg2;
void dfs(int u,int fa,int x)
{
	if (x>ans) ans=x;
	for (int p=head[u];p;p=e[p].nxt)
		if (e[p].b!=fa)
			dfs(e[p].b,u,x+e[p].w);
	return;
}
void dfs2(int u,int fa,int x){
	if (chud[u]==1)
	{
		da[u]=db[u]=0;
		if (x>ans) x=ans;
		return;
	}
	for (int i=head[u];i;i=e[i].nxt)
		if (e[i].b!=fa)
		{
			dfs2(e[i].b,u,x+e[i].w);
			if (da[e[i].b]+e[i].w>da[u]) db[u]=da[u],da[u]=da[e[i].b]+e[i].w;
			else if (da[e[i].b]+e[i].w>db[u]) db[u]=da[e[i].b]+e[i].w;
		}
	if (da[u]+db[u]>ans) ans=da[u]+db[u];
	return;
}
bool cmp1(edge x,edge y){return x.a<y.a;}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d %d",&n,&m);
	flg1=1; flg2=1;
	for (int i=1;i<n;i++) {
		scanf("%d %d %d",&at[i],&bt[i],&wt[i]);
		if (bt[i]!=at[i]+1) flg1=0;
		if (at[i]!=1) flg2=0;
	}
	if (flg1 && n<=10) {//Point 2
		for (int i=1;i<n;i++) e[i].a=at[i],e[i].b=bt[i],e[i].w=wt[i];
		sort(e+1,e+n,cmp1);
		s[0]=0;
		for (int i=1;i<n;i++) s[i]=s[i-1]+e[i].w;
		ans=0;
		int v=1<<(n-1);
		for (int k=0;k<v;k++){
			int cc[11];
			for (int i=0;i<=n-2;i++) cc[i]=0;
			int tmp=k,cp=0,c1=0;
			while (tmp) {
				cp++;
				if (tmp&1==1) cc[++c1]=cp;
				tmp>>=1;
			} 
			//printf("%d\n",cp);
			//for (int j=1;j<=c1;j++) printf("%d ",cc[j]);
			//puts("");
			if (c1!=m-1) continue;
			int ansp=2000000000;
			for (int i=1;i<=c1;i++)
				if (ansp>s[cc[i]]-s[cc[i-1]]) ansp=s[cc[i]]-s[cc[i-1]];
			if (ansp>s[n-1]-s[cc[c1]]) ansp=s[n-1]-s[cc[c1]];
			if (ansp>ans) ans=ansp;
 		}
		printf("%d\n",ans);
		return 0;
	}
	if (flg2){//Point 3 5 7 8
		sort(wt+1,wt+n);
		if (m==1){//Point 5 get5
			sort(wt+1,wt+n);
			printf("%d\n",wt[n-2]+wt[n-1]);
			return 0;
		}
		//Point 3 7 8
		
	}
	if (m==1){//Point 1 4 6 get15
		cnt=0;
		for (int i=1;i<n;i++) {
			e[++cnt].a=at[i],e[cnt].b=bt[i],e[cnt].w=wt[i];
			e[cnt].nxt=head[at[i]],head[at[i]]=cnt;
			chud[at[i]]++;
			e[++cnt].a=bt[i],e[cnt].b=at[i],e[cnt].w=wt[i];
			e[cnt].nxt=head[bt[i]],head[bt[i]]=cnt;
			chud[bt[i]]++;
		}
		if (n<=1000){//Point 1 4
			ans=0;
			for (int i=1;i<=n;i++)
				dfs(i,-1,0);
			printf("%d\n",ans);
			return 0;
		}
		else{//Point 6
			ans=0;
			dfs2(n/2,-1,0);
			printf("%d\n",ans);
		} 
		return 0;
	}
	return 0;
}
