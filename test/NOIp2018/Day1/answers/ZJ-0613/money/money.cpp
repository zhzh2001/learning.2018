#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 110
#define M 25010
#define MN 25000
using namespace std;

int T,n,num[N],ans,cnt;
bool ok[M];

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int i,j;
	cin>>T;
	while(T--)
	{
		ans=0;
		memset(ok,0,sizeof(ok));
		ok[0]=1;
		scanf("%d",&n);
		for(i=1;i<=n;i++) scanf("%d",&num[i]);
		sort(num+1,num+n+1);
		for(i=1;i<=n;i++)
		{
			if(ok[num[i]]) continue;
			ans++;
			for(j=num[i];j<=MN;j++)
			{
				ok[j]|=ok[j-num[i]];
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
