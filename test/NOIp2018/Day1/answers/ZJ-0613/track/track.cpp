#include<iostream>
#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
#define C ch=getchar()
#define N 50010
using namespace std;

int n,m,sum,bb,first[N],ds[N],cnt,jz,tot;
bool ay=1;
char ch;
struct Bn
{
	int to,next,quan;
}bn[N<<1];
vector<int>use[N];

inline void add(int u,int v,int w)
{
	bb++;
	bn[bb].to=v;
	bn[bb].next=first[u];
	bn[bb].quan=w;
	first[u]=bb;
}

int dfs(int now,int last)
{
	int p,q,t,mx=0,res=0,l,r,mid,a,b;
	use[now].clear();
	for(p=first[now];p!=-1;p=bn[p].next)
	{
		q=bn[p].to;
		if(q==last) continue;
		t=dfs(q,now)+bn[p].quan;
		t>=jz?(++cnt):(use[now].push_back(t),1);
	}
	sort(use[now].begin(),use[now].end());
	for(a=0,b=(int)use[now].size()-1;a<b;)
	{
		for(;a<b&&use[now][a]+use[now][b]<jz;++a);
		if(a>=b) break;
		++a,--b,res++;
	}
	cnt+=res;
	for(l=0,r=use[now].size();l<r;)
	{
		mid=((l+r)>>1);
		int t=0;
		for(a=(!mid),b=(int)use[now].size()-(mid+1==(int)use[now].size())-1;a<b;)
		{
			for(;a<b&&(use[now][a]+use[now][b]<jz || a==mid);++a);
			if(a>=b) break;
			++a,--b,++t;
			if(a==mid) ++a;
			if(b==mid) --b;
		}
		t==res?l=mid+1:r=mid;
	}
	return l?use[now][l-1]:0;
}

inline bool judge()
{
	int i,j;
	cnt=0;
	dfs(1,-1);
	return cnt>=m;
}

namespace solve1
{
	vector<int>use;
	inline bool judge(int u)
	{
		int p,q,res=0;
		use.clear();
		for(p=first[1];p!=-1;p=bn[p].next)
		{
			if(bn[p].quan>=u) res++;
			else use.push_back(bn[p].quan);
		}
		sort(use.begin(),use.end());
		for(p=0,q=(int)use.size()-1;p<q;)
		{
			for(;p<q&&use[p]+use[q]<u;p++);
			if(p>=q) break;
			res++,p++,q--;
		}
		return res>=m;
	}
	void work()
	{
		int l,r,mid;
		for(l=0,r=sum+1;l<r;)
		{
			mid=((l+r)>>1);
			if(judge(mid)) l=mid+1;
			else r=mid;
		}
		cout<<l-1;
	}
}

inline int read()
{
	static int res;
	for(C;ch<'0';C);
	for(res=ch-'0',C;ch>='0';res=res*10+ch-'0',C);
	return res;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(first,-1,sizeof(first));
	int i,j,p,q,o,l,r,mid,t=0;
	cin>>n>>m;
	for(i=1;i<n;i++)
	{
		p=read(),q=read(),o=read();
		add(p,q,o),add(q,p,o);
		sum+=o;
		ay&=(p==1);
	}
	if(ay)
	{
		solve1::work();
		return 0;
	}
	for(l=0,r=sum+1;l<r;)
	{
		mid=((l+r)>>1);
		jz=mid;
		if(judge()) l=mid+1;
		else r=mid;
	}
	cout<<l-1;
	return 0;
}
