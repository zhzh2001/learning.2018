#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<string>
#include<set>
#define rep(x,a,b) for(int x=(a);x<=(b);x++)
#define drp(x,a,b) for(int x=(a);x>=(b);x--)
#define cross(x,a) for(int x=hd[a];x;x=nx[x])
#define ll long long
#define inf 1000000000
#define maxn 50005
using namespace std;
inline ll rd()
{
	ll f=1,x=0;
	char ch=getchar();
	for(;!isdigit(ch)&&ch!=EOF&&ch!='-';ch=getchar());
	if (ch=='-') ch=getchar(),f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
void write(ll x){if (x>=10) write(x/10),putchar(x%10+'0');else putchar(x+'0');}
inline void wrt(ll x,char ch){if (x<0) x=-x,putchar('-');write(x);if (ch) putchar(ch);}
int n,m;
int nx[maxn<<1],to[maxn<<1],val[maxn<<1],hd[maxn],cnt;
inline void add(int u,int v,int w){nx[++cnt]=hd[u],to[cnt]=v,val[cnt]=w,hd[u]=cnt;}
namespace subtest2
{
int wt,ans;
int dfs(int u,int fa)
{
	int tot=0;
	int num[5];
	cross(i,u)
	if (to[i]!=fa){
		int x=val[i]+dfs(to[i],u);
		if (x>=wt) ans++;
		else num[++tot]=x;
	}
	sort(num+1,num+1+tot);
	int mx=0;
	if (tot==1) mx=num[tot];
	if (tot==2){
		if (num[1]+num[2]>=wt) ans++;
		else mx=num[2];
	}
	if (tot==3){
		if (num[1]+num[2]>=wt) ans++,mx=num[3];
		else if (num[1]+num[3]>=wt) ans++,mx=num[2];
		else if (num[2]+num[3]>=wt) ans++,mx=num[1];
		else mx=num[3];
	}
	return mx;
}
inline bool check(int x)
{
	wt=x;ans=0;dfs(1,1);
	return (ans>=m);
}
int doit()
{
	int l=1,r=n*10000,ans=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if (check(mid)) l=mid+1,ans=mid;
		else r=mid-1;
	}
	wrt(ans,'\n');
}
}
