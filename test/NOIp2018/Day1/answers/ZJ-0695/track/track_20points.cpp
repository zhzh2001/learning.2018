#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<string>
#define rep(x,a,b) for(int x=(a);x<=(b);x++)
#define drp(x,a,b) for(int x=(a);x>=(b);x--)
#define cross(x,a) for(int x=hd[a];x;x=nx[x])
#define ll long long
#define maxn 50005
using namespace std;
inline ll rd()
{
	ll f=1,x=0;
	char ch=getchar();
	for(;!isdigit(ch)&&ch!=EOF&&ch!='-';ch=getchar());
	if (ch=='-') ch=getchar(),f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
void write(ll x){if (x>=10) write(x/10),putchar(x%10+'0');else putchar(x+'0');}
inline void wrt(ll x,char ch){if (x<0) x=-x,putchar('-');write(x);if (ch) putchar(ch);}
int n,m;
int nx[maxn<<1],to[maxn<<1],val[maxn<<1],hd[maxn],cnt;
inline void add(int u,int v,int w){nx[++cnt]=hd[u],to[cnt]=v,val[cnt]=w,hd[u]=cnt;}
int ed;
int q[maxn],head,tail;
int dis[maxn];
bool vis[maxn];
inline int bfs(int u)
{
	memset(dis,0,sizeof dis);
	memset(vis,0,sizeof vis);
	head=tail=0;
	q[++tail]=u;vis[u]=1;
	while(head<tail){
		int tt=q[++head];
		cross(i,tt) if (!vis[to[i]]){
			dis[to[i]]=dis[tt]+val[i];
			vis[to[i]]=1;q[++tail]=to[i];
		}
	}
	int x=0;
	rep(i,1,n)
		if (dis[i]>dis[x]) x=i;
	ed=x;
	return dis[x];
}
int doit()
{
	bfs(1);wrt(bfs(ed),'\n');
}

