#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<string>
#include<set>
#include<vector>
#define rep(x,a,b) for(int x=(a);x<=(b);x++)
#define drp(x,a,b) for(int x=(a);x>=(b);x--)
#define cross(x,a) for(int x=hd[a];x;x=nx[x])
#define ll long long
#define inf 1000000000
#define maxn 50005
using namespace std;
inline ll rd()
{
	ll f=1,x=0;
	char ch=getchar();
	for(;!isdigit(ch)&&ch!=EOF&&ch!='-';ch=getchar());
	if (ch=='-') ch=getchar(),f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
void write(ll x){if (x>=10) write(x/10),putchar(x%10+'0');else putchar(x+'0');}
inline void wrt(ll x,char ch){if (x<0) x=-x,putchar('-');write(x);if (ch) putchar(ch);}
int n,m;
int nx[maxn<<1],to[maxn<<1],val[maxn<<1],hd[maxn],cnt;
inline void add(int u,int v,int w){nx[++cnt]=hd[u],to[cnt]=v,val[cnt]=w,hd[u]=cnt;}
namespace subtest2
{
int wt,ans;
int dfs(int u,int fa)
{
	vector< pair<int,int> > vt1;
	vt1.clear();
	set< pair<int,int> > st;
	st.clear();
	st.insert(make_pair(inf,n+1));
	int tot=0;
	cross(i,u)
	if (to[i]!=fa){
		int x=val[i]+dfs(to[i],u);
		if (x>=wt) ans++;
		else vt1.push_back(make_pair(x,++tot)),st.insert(make_pair(x,tot));
	}
	sort(vt1.begin(),vt1.end());
	int mx=0;
	rep(i,0,int(vt1.size())-1) 
		if ((*st.lower_bound(vt1[i])).second==vt1[i].second){
		int w=wt-vt1[i].first;
		set< pair<int,int> >::iterator it=st.lower_bound(make_pair(w,0));
		if ((*it).second==vt1[i].second) ++it;
		if ((*it).first!=inf) st.erase(it),ans++;
		else mx=vt1[i].first;
		st.erase(st.lower_bound(vt1[i]));
	}
	return mx;
}
inline bool check(int x)
{
	wt=x;ans=0;dfs(1,1);
	return (ans>=m);
}
int doit()
{
	int l=1,r=n*10000,ans=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if (check(mid)) l=mid+1,ans=mid;
		else r=mid-1;
	}
	wrt(ans,'\n');
}
}
namespace subtest1
{
int ed;
int q[maxn],head,tail;
int dis[maxn];
bool vis[maxn];
inline int bfs(int u)
{
	memset(dis,0,sizeof dis);
	memset(vis,0,sizeof vis);
	head=tail=0;
	q[++tail]=u;vis[u]=1;
	while(head<tail){
		int tt=q[++head];
		cross(i,tt) if (!vis[to[i]]){
			dis[to[i]]=dis[tt]+val[i];
			vis[to[i]]=1;q[++tail]=to[i];
		}
	}
	int x=0;
	rep(i,1,n)
		if (dis[i]>dis[x]) x=i;
	ed=x;
	return dis[x];
}
int doit()
{
	bfs(1);wrt(bfs(ed),'\n');
}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=rd(),m=rd();
	rep(i,1,n-1){
		int x=rd(),y=rd(),z=rd();
		add(x,y,z),add(y,x,z);
	}
	if (m==1) subtest1::doit();
	else subtest2::doit();
}
