#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<string>
#define rep(x,a,b) for(int x=(a);x<=(b);x++)
#define drp(x,a,b) for(int x=(a);x>=(b);x--)
#define ll long long
#define maxn 105
using namespace std;
inline ll rd()
{
	ll f=1,x=0;
	char ch=getchar();
	for(;!isdigit(ch)&&ch!=EOF&&ch!='-';ch=getchar());
	if (ch=='-') ch=getchar(),f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
void write(ll x){if (x>=10) write(x/10),putchar(x%10+'0');else putchar(x+'0');}
inline void wrt(ll x,char ch){if (x<0) x=-x,putchar('-');write(x);if (ch) putchar(ch);}
int T,mx;
int n,a[maxn];
bool dp[250005];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=rd();
	while(T--){
		n=rd();
		mx=0;
		rep(i,1,n) a[i]=rd(),mx=max(a[i],mx);
		sort(a+1,a+1+n);
		memset(dp,0,sizeof dp);
		int ans=0;
		dp[0]=1;
		rep(i,1,n)
		if (!dp[a[i]]){
			rep(j,a[i],mx) dp[j]|=dp[j-a[i]];
			ans++;
		}
		wrt(ans,'\n');
	}
} 
