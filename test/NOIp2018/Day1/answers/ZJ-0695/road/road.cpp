#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<string>
#define rep(x,a,b) for(int x=(a);x<=(b);x++)
#define drp(x,a,b) for(int x=(a);x>=(b);x--)
#define ll long long
#define maxn 100005
using namespace std;
inline ll rd()
{
	ll f=1,x=0;
	char ch=getchar();
	for(;!isdigit(ch)&&ch!=EOF&&ch!='-';ch=getchar());
	if (ch=='-') ch=getchar(),f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
void write(ll x){if (x>=10) write(x/10),putchar(x%10+'0');else putchar(x+'0');}
inline void wrt(ll x,char ch){if (x<0) x=-x,putchar('-');write(x);if (ch) putchar(ch);}
int a[maxn],n,id[maxn];
int fa[maxn];
int sum;
ll ans;
inline bool cmp(int A,int B){return a[A]<a[B];}
int find(int x){return fa[x]==x?x:fa[x]=find(fa[x]);}
inline void mge(int u)
{
	if (u>1&&a[u-1]>=a[u]&&find(u-1)!=find(u)) sum--,fa[find(u-1)]=find(u);
	//else if (a[n]>=a[u]&&find(n)!=find(u)) sum--,fa[find(n)]=find(u);
	if (u<n&&a[u+1]>=a[u]&&find(u+1)!=find(u)) sum--,fa[find(u+1)]=find(u);
	//else if (a[1]>=a[u]&&find(1)!=find(u)) sum--,fa[find(1)]=find(u);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=rd();
	rep(i,1,n) a[i]=rd(),id[i]=i,fa[i]=i;
	sort(id+1,id+1+n,cmp);
	drp(i,n,1){
		int j=i,x=id[i];
		while(a[id[j-1]]==a[x]&&j>1) j--;
		if (i!=n) ans+=sum*(a[id[i+1]]-a[x]);
		sum+=i-j+1;
		rep(k,j,i) mge(id[k]);
		i=j;
	}
	wrt(ans+a[id[1]],'\n');
}
