#include<cstdio>
#include<algorithm>
#include<cstring>
#include<vector>
using namespace std;
struct data{int x,y,l;}q[301000],st;
struct edge{int u,v,next,w;}E[301000];
int l,r,mid,n,m,x,y,z,t,dp[50100],tmp[50100],fa[50100],e[50100];
int sum,h,len,head[50100],c,ls[50100];
bool vis[50100];
vector<int>g[50100];
inline int read()
{
  int cnt=0,f=1;char ch=getchar();
  while (ch<'0'||ch>'9'){if (ch=='-')f=-1;ch=getchar();}
  while (ch>='0'&&ch<='9'){cnt=cnt*10+ch-48;ch=getchar();}
  return cnt*f;
}
inline int max(int a,int b){return a>b?a:b;}
inline bool cmp(int a,int b){return dp[a]<dp[b];}
void dfs(int x,int y,int num)
{
  dp[x]=E[num].w;int Max=0;
  for (int i=head[x];i!=0;i=E[i].next)
  if (E[i].v!=y)
  {
  	 fa[E[i].v]=x;
  	 e[E[i].v]=E[i].w;
     dfs(E[i].v,x,i);
	 Max=max(Max,dp[E[i].v]);   
  }
  dp[x]+=Max;
}
inline bool check(int len)
{
  int ret=0;
  memset(vis,false,sizeof(vis));
  h=t=-1;sum=0;
  for (int i=0;i<g[1].size();i++)
   {
   	if (vis[g[1][i]]) continue;
   	if (dp[g[1][i]]>=len) {vis[g[1][i]]=true;data tmp;tmp.x=g[1][i];tmp.y=-1;tmp.l=e[g[1][i]];t=(t+1)%301000;sum++;q[t]=tmp;}
   	else if (g[1].size()>1&&i+1<g[1].size())
   	{
   	  int l=i+1,r=g[1].size()-1,mid;
	  while (l<r)
	  {
	  	mid=(l+r)>>1;
	  	if (dp[g[1][mid]]+dp[g[1][i]]>=len) r=mid;
		else l=mid+1; 
	  }	
	  if (dp[g[1][l]]+dp[g[1][i]]>=len)
	  {
	  	vis[g[1][i]]=true;vis[g[1][l]]=true;
	  	data tmp;tmp.x=g[1][i];tmp.y=g[1][l];tmp.l=e[g[1][i]]+e[g[1][l]];
		t=(t+1)%301000;sum++;q[t]=tmp;
	  }
	}
   }
   while (sum)
   {
     h=(h+1)%301000;sum--;
     st=q[h];
     x=st.x;y=st.y;c=0;
     if (st.l>=len) 
	 {
	  st.l=0;
	  ret++;
	  data tmp;tmp.x=st.x;tmp.y=-1;tmp.l=0;t=(t+1)%301000;sum++;q[t]=tmp;
	  if (st.y!=-1) 
	  {
	  data tmp;tmp.x=st.y;tmp.y=-1;tmp.l=0;t=(t+1)%301000;sum++;q[t]=tmp;
      }
      continue;
	 }
	 for (int i=0;i<g[x].size();i++)
      ls[++c]=g[x][i];
     if (y!=-1)
     for (int i=0;i<g[y].size();i++)
      ls[++c]=g[y][i]; 
     for (int i=1;i<=c;i++)
     {
   	  if (vis[ls[i]]) continue;
   	  if (st.l+dp[ls[i]]>=len) {vis[ls[i]]=true;data tmp;tmp.x=ls[i];tmp.y=-1;tmp.l=st.l+e[ls[i]];t=(t+1)%301000;sum++;q[t]=tmp;}
   	  else if (c>1&&i+1<=c)
   	  {
   	   int l=i+1,r=c,mid;
	   while (l<r)
	   {
	  	mid=(l+r)>>1;
	  	if (st.l+dp[ls[mid]]+dp[ls[i]]>=len) r=mid;
		else l=mid+1; 
	   }	
	   if (st.l+dp[ls[l]]+dp[ls[i]]>=len)
	   {
	  	vis[ls[i]]=true;vis[ls[l]]=true;
	  	data tmp;tmp.x=ls[i];tmp.y=ls[l];tmp.l=st.l+e[ls[i]]+e[ls[l]];
		t=(t+1)%301000;sum++;q[t]=tmp;
	   }
	 }
    }
   }
   return ret>=m;
}
int main()
{
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  n=read();m=read();
  for (int i=1;i<n;i++)
  {
    x=read();y=read();z=read();
	E[i].u=x;E[i].v=y;E[i].w=z;E[i].next=head[x];head[x]=i;t=i+n;
	E[t].u=y;E[t].v=x;E[t].w=z;E[t].next=head[y];head[y]=t;
  }
  dfs(1,0,0);
  
  for (int i=1;i<=n;i++)
  {
    len=0;
	for (int j=head[i];j!=0;j=E[j].next) 
	if (E[i].v!=fa[i]) tmp[++len]=E[j].v;	
    sort(tmp+1,tmp+1+len,cmp);
    for (int j=1;j<=len;j++)
     g[i].push_back(tmp[j]);
  }
  
  l=1;r=5e8;
  
  while (l<r)
  {
    mid=(l+r+1)>>1;
    if (check(mid)) l=mid;
    else r=mid-1;
  }
  printf("%d\n",l);
  fclose(stdin);
  fclose(stdout);
  return 0;
}
