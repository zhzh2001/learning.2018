#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int n,T,a[25100],ans,V;
bool dp[25100];
inline int read()
{
 int cnt=0,f=1;char ch=getchar();
 while (ch<'0'||ch>'9'){if (ch=='-')f=-1;ch=getchar();}
 while (ch>='0'&&ch<='9'){cnt=cnt*10+ch-48;ch=getchar();}
 return cnt*f;
}
inline int max(int a,int b){return a>b?a:b;}
int main()
{
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  T=read();
  while (T--)
  {
    ans=n=read();V=0;
    for (int i=1;i<=n;i++)
     V=max(V,a[i]=read());
    memset(dp,false,sizeof(dp));
    dp[0]=true;
	sort(a+1,a+1+n);
    for (int i=1;i<=n;i++)
     if (dp[a[i]]) ans--;
     else
      {
        for (int j=a[i];j<=V;j++)
         dp[j]=dp[j-a[i]]||dp[j];
	  }
	 printf("%d\n",ans);
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}
