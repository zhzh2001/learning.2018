#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#define ll long long
using namespace std;
int n,len,loc[101000][20];
ll ans,d[101000],Min[101000][20];
inline int read()
{
  int cnt=0,f=1;char ch=getchar();
  while (ch<'0'||ch>'9'){if (ch=='-')f=-1;ch=getchar();}
  while (ch>='0'&&ch<='9'){cnt=cnt*10+ch-48;ch=getchar();}
  return cnt*f;
}
inline int min(int a,int b){return a<b?a:b;}
void dfs(int l,int r,ll del)
{
  if (l>r) return;
  if (l==r) {ans+=d[l]-del;return;}
  int len=log(r-l+1)/log(2),lc;
  ll tmp;
  if (Min[l][len]<Min[r-(1<<len)+1][len])
  {
  	tmp=Min[l][len];
  	lc=loc[l][len];
  }
  else
  {
  	tmp=Min[r-(1<<len)+1][len];
  	lc=loc[r-(1<<len)+1][len];
  }
  ans+=tmp-del;
  dfs(l,lc-1,tmp);
  dfs(lc+1,r,tmp);
}
int main()
{
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
  n=read();
  for (int i=1;i<=n;i++)
  {
   Min[i][0]=d[i]=read();
   loc[i][0]=i;
  }
  len=log(n)/log(2);
  for (int i=1;i<=len;i++)
   for (int j=1;j<=n-(1<<i)+1;j++)
   {
   	if (Min[j][i-1]<Min[j+(1<<i-1)][i-1])
   	{
   	  Min[j][i]=Min[j][i-1];
   	  loc[j][i]=loc[j][i-1];
	}
	else 
	{
	  Min[j][i]=Min[j+(1<<i-1)][i-1];
	  loc[j][i]=loc[j+(1<<i-1)][i-1];
	}
   }
  dfs(1,n,0);
  printf("%lld\n",ans);
  fclose(stdin);
  fclose(stdout);
  return 0;
}
