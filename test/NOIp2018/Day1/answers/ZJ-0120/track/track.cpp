#include<bits/stdc++.h>
using namespace std;
const int N=5e4+10;
int n,m,u,v,w,max1[N],max2[N],r,l=1,mid,Max1[N],f[N],cnt,a[N],tot;
vector<int>edge[N];
vector<int>value[N];
void dfs(int x,int fa){
	max1[x]=max2[x]=0;
	for(int i=0;i<edge[x].size();i++){
		int vv=edge[x][i];
		int ww=value[x][i];
		if(vv==fa)continue;
		f[vv]=x;
		dfs(vv,x);
		if(max1[vv]+ww>max1[x]){
			max2[x]=max1[x];
			max1[x]=max1[vv]+ww;
		}
		else if(max1[vv]+ww>max2[x]){
			max2[x]=max1[vv]+ww;
		}
	}
	r=max(r,max1[x]+max2[x]);
}
void check(int x,int val){
	if(cnt==m)return;
	tot=0;
	if(max1[x]+max2[x]<val){
		Max1[x]=max1[x];
		return;
	}
	for(int i=0;i<edge[x].size();i++){
		int vv=edge[x][i];
		if(vv==f[x])continue;
		check(vv,val);
		if(cnt==m)return;
	}
	for(int i=0;i<edge[x].size();i++){
		int vv=edge[x][i];
		if(vv==f[x])continue;
		int ww=value[x][i];
		a[++tot]=Max1[vv]+ww;
	}
	if(tot==0)return;
	sort(a+1,a+1+tot);
	for(int i=tot;i>=1;i--){
		if(a[i]>=val){
			a[i]=0;
			cnt++;
			tot--;
		}
		else{
			break;
		}
	}
	if(a[tot]+a[tot-1]<val){
		Max1[x]=a[tot];
		return;
	}
	for(int i=1;i<tot;i++){
		if(a[i]==0)continue;
		for(int j=i+1;j<=tot;j++){
			if(a[i]+a[j]>=val){
				cnt++;
				a[i]=a[j]=0;
				break;
			}
		}
	}
	Max1[x]=0;
	for(int i=tot;i>=1;i--){
		if(a[i]>0){
			Max1[x]=a[i];
			for(int j=1;j<=tot;j++)a[j]=0;
			return;
		}
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d %d",&n,&m);
	for(int i=1;i<n;i++){
		scanf("%d %d %d",&u,&v,&w);
		edge[u].push_back(v);edge[v].push_back(u);
		value[u].push_back(w);value[v].push_back(w);
	}
	dfs(1,0);
	if(m==1){
		printf("%d\n",r);
		return 0;
	}
	while(l<=r){
		for(int i=1;i<=n;i++)Max1[i]=0;
		cnt=0;
		mid=(l+r)>>1;
		check(1,mid);
//		cout<<cnt<<" "<<mid<<endl;
		if(cnt>=m)l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
