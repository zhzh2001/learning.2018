#include<bits/stdc++.h>
using namespace std;
int t,n,a[1010],ans;
int b[25010];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		for(int i=1;i<=25000;i++)b[i]=0;
		scanf("%d",&n);ans=n;
		for(int i=1;i<=n;i++){
			scanf("%d",&a[i]);
		}
		sort(a+1,a+1+n);
		for(int i=1;i<=n;i++){
			if(b[a[i]]==1){
				ans--;
				continue;
			}
			for(int j=1;j<=25000;j++){
				if(b[j]==1&&(j%a[i])!=0&&((j+a[i])<=25000)){
					b[j+a[i]]=1;
				}
				else if(j%a[i]==0){
					b[j]=1;
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
