#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
int a[202],b[202],f[100100];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		int n;
		scanf("%d",&n);
		int num=0;
		for (int i=1;i<=n;i++){
			scanf("%d",&a[i]);
			num=std::max(num,a[i]);
		}
		std::sort(a+1,a+n+1);
//		for (int i=1;i<=n;i++) printf("%d\n",a[i]);
		int k=1,m=0;
		for (int i=1;i<=num;i++){
			f[i]=0;
			for (int j=1;j<=m;j++){
				if (f[i-b[j]]) f[i]=1;
			}
//			printf("%d\n",f[i]);
			if (i==a[k]){
				if (!f[i]){
					f[i]=1;
					b[++m]=i;
				}
				k++;
			}
		}
		printf("%d\n",m);
	}
	return 0;
}
