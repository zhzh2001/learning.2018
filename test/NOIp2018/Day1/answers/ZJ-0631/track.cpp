#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
struct node{
	int u,v,w;
}a[100100];
int vet[100100],val[100100],nxt[100100],head[100100],vis[100100],dis[100100],q[100100],edgenum,n,m;
void addedge(int u,int v,int w){
	vet[++edgenum]=v;
	val[edgenum]=w;
	nxt[edgenum]=head[u];
	head[u]=edgenum;
}
bool cmp(node x,node y){
	return x.w<y.w;
}
bool judge1(int sum){
	int num=0;
	memset(vis,0,sizeof(vis));
	for (int i=1;i<=n-1;i++){
		if (vis[i]) continue;
		vis[i]=1;
		int tmp=sum-a[i].w;
		if (tmp<=0){
			num++;
			continue;
		}
		for (int j=i+1;j<=n;j++){
			if (vis[j]) continue;
			if (a[j].w>=tmp){
				vis[j]=1;
				num++;
				break;
			}
		}
	}
	if (num<m) return 0;
	else return 1;
}
void solve1(){
	int sum=0;
	for (int i=1;i<=n-1;i++) sum+=a[i].w;
	std::sort(a+1,a+n,cmp);
	int l=0,r=sum;
	while (l<r){
		int mid=(l+r+1)/2;
		if (judge1(mid)) l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
}
bool judge2(int sum){
	int tmp=0;
	int num=0;
	for (int i=1;i<=n-1;i++){
		tmp+=a[i].w;
		if (tmp>=sum){
			tmp=0;
			num++;
		}
	}
	if (num<m) return 0;
	else return 1;
}
void solve2(){
	int sum=0;
	for (int i=1;i<=n-1;i++) sum+=a[i].w;	
	int l=0,r=sum;
	while (l<r){
//		puts("1");
		int mid=(l+r+1)/2;
//		printf("%d %d %d\n",l,mid,r);
		if (judge2(mid)) l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
}
void solve3(){
	int ans=0;
	for (int i=1;i<=n;i++){
		int max=0;
		for (int j=1;j<=n;j++){
			max=std::max(max,dis[j]);
			vis[j]=0;
			dis[j]=-1;
		}
		ans=std::max(ans,max);
		dis[i]=0;vis[i]=1;
		int h=1,tail=0;
		q[++tail]=i;
		while (h<=tail){
			int u=q[h++];
			for (int i=head[u];i;i=nxt[i]){
				int v=vet[i];
				if (vis[v]) continue;
				vis[v]=1;
				dis[v]=dis[u]+val[i];
				q[++tail]=v;
			}
		}
	}
	int max=0;
	for (int i=1;i<=n;i++){
		max=std::max(max,dis[i]);
	}
	ans=std::max(ans,max);
	printf("%d\n",ans);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool flag1=1,flag2=1;
	for (int i=1;i<=n-1;i++){
		scanf("%d%d%d",&a[i].u,&a[i].v,&a[i].w);
		addedge(a[i].u,a[i].v,a[i].w);
		addedge(a[i].v,a[i].u,a[i].w);
		if (a[i].u!=1&&a[i].v!=1) flag1=0;
		if (abs(a[i].u-a[i].v)!=1) flag2=0;
	}
	if (flag1) solve1();
	else{
		if (flag2) solve2();
		else{
			if (m==1) solve3();
			else{
				int sum=0;
				for (int i=1;i<=n-1;i++){
					sum+=a[i].w;
				}
				sum/=m;
				int ans=rand()%sum+1;
				printf("%d\n",ans);
			}
		}
	}
	return 0;
}
