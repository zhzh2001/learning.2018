#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=1e2+10,M=25e3+10;
int a[N],f[M]; 
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') {x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while (T--)
	{
		int n=read(),ans=n;
		memset(f,0,sizeof f);f[0]=1;
		for (int i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+n+1);
		for (int i=1;i<=n;i++)
		{
			int x=a[i];
			if (f[x]==1) {ans--;continue;}
			for (int j=x;j<=25000;j++)
				if (f[j-x]) f[j]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
