#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=1e5+10;
int a[N]; 
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') {x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n=read(),ans=0;
	for (int i=1;i<=n;i++)
	{
		a[i]=read();
		if (a[i]>a[i-1]) ans+=a[i]-a[i-1];
	}
	printf("%d\n",ans);
	return 0;
}
