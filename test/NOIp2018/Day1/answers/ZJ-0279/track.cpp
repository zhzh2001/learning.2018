#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=5e4+10;
int hed[N],v[N*2],to[N*2],nxt[N*2],fa[N],cnt;
int f[N],g[N],a[N],mid,nn,L,R,j,k,md,s; 
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') {x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void add(int x,int y,int z){to[++cnt]=y;v[cnt]=z;nxt[cnt]=hed[x];hed[x]=cnt;}
void dfs(int t)
{
	for (int i=hed[t];i;i=nxt[i])
		if (to[i]!=fa[t])
			fa[to[i]]=t,dfs(to[i]);
}
void dp(int t)
{
	for (int i=hed[t];i;i=nxt[i])
		if (to[i]!=fa[t]) dp(to[i]);
	nn=0;
	for (int i=hed[t];i;i=nxt[i])
		if (to[i]!=fa[t])
		{
			g[t]+=g[to[i]];
			if (f[to[i]]+v[i]>=mid) g[t]++;
			else a[++nn]=f[to[i]]+v[i];
		}
	sort(a+1,a+nn+1);
	L=1,j=R=nn;
	for (int i=1;i<j;i++)
		if (a[i]+a[j]>=mid) j--;
	j=nn-j;g[t]+=j;
	while (L<R)
	{
		md=(L+R+1)/2;k=nn;s=0;
		for (int i=1;i<k;i++)
		{
			if (i==md) i++;
			if (k==md) k--;
			if (i>=k) break;
			if (a[i]+a[k]>=mid) k--,s++;
		}
		if (s<j) R=md-1; else L=md;
	}
	if (nn==j*2) f[t]=0; else f[t]=a[L];
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n=read(),m=read();
	for (int i=1;i<n;i++)
	{
		int x=read(),y=read(),z=read();
		add(x,y,z);add(y,x,z);
	}
	dfs(1);
	int l=1,r=500000000;
	while (l<r)
	{
		memset(f,0,sizeof f);
		memset(g,0,sizeof g);
		mid=(l+r+1)/2;dp(1);
		if (g[1]>=m) l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
