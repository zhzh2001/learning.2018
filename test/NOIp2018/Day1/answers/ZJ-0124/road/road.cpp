#include <iostream>
#include <vector>
#include <cstdio>
#include <algorithm>
using std::vector;
using std::min;
using std::max;
using std::cin;
using std::cout;
using std::endl;

int road[100001];
int delta[100005];
int valley[10005];
int peak[10005];

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	int n;
	cin >> n;
if (n < 1000) {
	int minn = 100000;
	int maxn = 0;
	int ans = 0;
	for (int i = 0; i < n; i++) {
		cin >> road[i];
		minn = min(minn, road[i]);
		maxn = max(maxn, road[i]);
	}
	road[n] = 0;
	for (int i = minn; i <= maxn; i++) {
		bool enter = 0;
		int local_min = 100000;
		for (int j = 0; j < n + 1; j++) {
			if (road[j] - i > 0) {
				enter = 1;
				local_min = min(local_min, road[j] - i);
			} else if (enter == 1) {
				enter = 0;
				ans++;
			}
		}
		i += local_min - 1;
	}
	cout << ans + minn << endl;
} else {
	road[0] = 0;
	delta[0] = 0;
	int ans = 0;
	int maxn = 0;
	for (int i = 1; i <= n; i++) {
		cin >> road[i];
		road[i]++;
		maxn = max(maxn, road[i]);
		delta[i] = road[i] - road[i - 1];
	}
	delta[n + 1] = 0 - road[n];
	for (int i = 1; i <= n; i++) {
		if (delta[i] > 0 && delta[i + 1] <= 0)
			peak[road[i]]++;
		if (delta[i] < 0 && delta[i + 1] >= 0)
			valley[road[i]]++;
	}
	int step = 1;
	for (int i = 0; i <= maxn; i++) {
		step -= peak[i];
		step += valley[i];
		ans += step;
	}
	cout << ans - 1 << endl;
}
	return 0;
}
