#include <iostream>
#include <cstring>
#include <cstdio>
#include <algorithm>
using std::min;
using std::max;

int T;
int f[25001];
int a[101];

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	while (T--) {
		memset(f, 0, sizeof(f));
		f[0] = 1;
		int n;
		scanf("%d", &n);
		int m = n;
		int minn = 25001;
		int maxn = 0;
		for (int i = 0; i < n; i++) {
				scanf("%d", a + i);
				minn = min(minn, a[i]);
				maxn = max(maxn, a[i]);
		}
		for (int i = minn; i <= maxn; i++)
			for (int j = 0; j < n; j++)
				if (i - a[j] >= 0 && f[i - a[j]])
					f[i]++;
		for (int i = 0; i < n; i++)
			if (f[a[i]] > 1)
				m--;
		printf("%d\n", m);
	}
	return 0;
}
