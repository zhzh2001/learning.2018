#include <cstdio>
#include <algorithm>
#include <vector>
using std::vector;
using std::min;
using std::sort;
using std::max;

struct edg {
	int to;
	int len;
	edg(int i_to, int i_len) {
		to = i_to;
		len = i_len;
	}
};

int n, m;
vector<edg> edge[50001];

bool comp(edg a, edg b) {
	return a.len < b.len;
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d %d", &n, &m);
	for (int i = 1; i <= n - 1; i++) {
		int t_a, t_b, t_l;
		scanf("%d %d %d", &t_a, &t_b, &t_l);
		edge[t_a].push_back(edg(t_b, t_l));
	}
	if (edge[1].size() == n - 1) {
		sort(edge[1].begin(), edge[1].end(), comp);
		if (m * 2 > n - 1) {
			int ans = edge[1][n - m - 1].len;
			printf("%d\n", ans);
			return 0;
		} else {
			int ans = 1000000;
			for (int j = n - 1 - 2 * m; j < n - 1 - m; j++) {
				int t = edge[1][j].len + edge[1][2 * n - 2 - 2 * m - j].len;
				ans = min(t, ans);
			}
			printf("%d\n", ans);
			return 0;
		}
	}
	return 0;
}
