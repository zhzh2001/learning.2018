#include <cmath>
#include <cstdio>
#include <algorithm>
#include <iostream>

using namespace std;
int n;
int Log[100005],a[100005],f[100005];
int Min[100005][20],Minip[100005][20];

bool pd()
{
	for (int i=1;i<=n;i++)
		if (f[i]!=a[i]) return true;
	return false;
}

int getmin(int l,int r)
{
	int t=Log[r-l+1];
	return min(Min[l][t],Min[r-(1 << t)+1][t]);
}

int getip(int l,int r)
{
	int t=Log[r-l+1];
	if (Min[l][t]<Min[r-(1 << t)+1][t]) return Minip[l][t];
	else return Minip[r-(1 << t)+1][t];
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	scanf("%d",&n);
	int Max=1;
	for (int i=1;i<=n;i++) 
	{
		scanf("%d",&a[i]);
		if (a[i]>a[Max]) Max=i;
	}
	
	Log[1]=0;
	for (int i=2;i<=n;i++) Log[i]=Log[i/2]+1;
//	for (int i=1;i<=n;i++) printf("%d ",Log[i]); printf("\n");
	
	for (int i=1;i<=n;i++) 
	{
		Min[i][0]=a[i];
		Minip[i][0]=i;
	}
	for (int j=1;j<=Log[n];j++)
		for (int i=1;i+(1 << j)-1<=n;i++)
		{
			if (Min[i][j-1]<Min[i+(1 << (j-1))][j-1])
			{
				Min[i][j]=Min[i][j-1];
				Minip[i][j]=Minip[i][j-1];
			}
			else
			{
				Min[i][j]=Min[i+(1 << (j-1))][j-1];
				Minip[i][j]=Min[i+(1 << (j-1))][j-1];
			}
		}
	
//	for (int i=1;i<=n;i++)
//	{
//		for (int j=1;i+(1 << j)-1<=n;j++)
//			printf("%d ",Min[i][j]);
//		printf("\n");
//	}
		
	a[0]=0;a[n+1]=0;
	for (int i=1;i<=n;i++) f[i]=0;
	int ans=0;
	int l=1,r;
	while (l<=n)
	{
		r=l+1;
		while (a[r]!=f[r]) r++;
		int t=getmin(l,r-1);
		int t_ip=getip(l,r-1);
		ans+=t-f[t_ip];
		for (int i=l;i<=r-1;i++) f[i]=t;
		l=1;
		while (f[l]==a[l]) l++;
	}
	printf("%d\n",ans);
	return 0;
}
