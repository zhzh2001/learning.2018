#include <cmath>
#include <cstdio>
#include <algorithm>
#include <iostream>

using namespace std;
int ans,n,m;
int dis[1005][1005];
int mark[1005];

void dfs(int depth,int t)
{
	ans=max(ans,t);
	for (int i=1;i<=n;i++)
		if ((mark[i]!=1)&&(dis[depth][i]>0))
		{
			mark[i]=1;
			dfs(i,t+dis[depth][i]);
			mark[i]=0;
		}
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	ans=0;
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n-1;i++)
	{
		int a,b,t;
		scanf("%d%d%d",&a,&b,&t);
		dis[a][b]=t;dis[b][a]=t;
	}
	if (m==1) 
	{
		for (int i=1;i<=n;i++) mark[i]=0;
		for (int i=1;i<=n;i++) 
		{
			mark[i]=1;
			dfs(i,0);
			mark[i]=0;
		}
		printf("%d\n",ans);
	}
	return 0;
}
