#include<bits/stdc++.h>
using namespace std;
int t1,n,i,j,ans,mx,a[1001],f[30001];
int main(){
freopen("money.in","r",stdin);
freopen("money.out","w",stdout);	
	scanf("%d",&t1);
	while(t1--){
		scanf("%d",&n);
		memset(f,0,sizeof(f));
		for(i=1;i<=n;i++)scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		mx=a[n];
		ans=0;
		for(i=1;i<=n;i++){
			if(!f[a[i]]){
				f[a[i]]=1;
				ans++;
				for(j=a[i]+1;j<=mx;j++)f[j]=f[j]||f[j-a[i]];
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}

/*
2
4
3 19 10 6
5
11 29 13 19 17
*/
