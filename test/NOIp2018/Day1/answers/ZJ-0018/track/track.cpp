#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cctype>
inline int getint()
{
	int r = 0,s = 0;char c = getchar();for(;!isdigit(c);c = getchar()) s |= c == '-';
	for(;isdigit(c);c = getchar()) r = (((r << 2) + r) << 1) + (c ^ '0');return s ? -r : r;
}
const int N = 50050;
int n,m;
int head[N],next[N << 1],to[N << 1],val[N << 1],tot = 0;
inline void addedge(int u,int v,int w)
{
	next[++tot] = head[u];to[tot] = v;val[tot] = w;head[u] = tot;
	next[++tot] = head[v];to[tot] = u;val[tot] = w;head[v] = tot;
}
namespace m1
{
	int dep[N];
	inline void dfs1(int u,int fa)
	{
		for(int i = head[u],v = to[i];~i;v = to[i = next[i]])
		{
			if(v == fa) continue;
			dep[v] = dep[u] + val[i];
			dfs1(v,u);
		}
	}
	void main()
	{
		dfs1(1,0);
		int p1 = 0;
		for(int i = 1;i <= n;i++) if(dep[p1] < dep[i]) p1 = i;
		memset(dep,0,sizeof(dep));
		dfs1(p1,0);
		int mx = 0;
		for(int i = 1;i <= n;i++) if(dep[i] > mx) mx = dep[i];
		printf("%d\n",mx);
	}
}
namespace a1
{
	int a[N];
	void main()
	{
		for(int i = 1;i < n;i++)
		{
			a[i] = val[i << 1];
		}
		int sc = n - 1;
		std::sort(a + 1,a + sc + 1);
		if(sc / 2 >= m)
		{
			int l = sc - 2 * m + 1,r = sc;
			int ans = 0x3f3f3f3f;
			while(l < r)
			{
				ans = std::min(ans,a[l] + a[r]);
				l++,r--;
			}
			printf("%d\n",ans);
		}
		else
		{
			if(sc & 1)
			{
				int ans = 0x3f3f3f3f;
				ans = std::min(ans,a[sc]);
				sc--;
				int cnt1 = 2 * (m - sc / 2);
				int cnt2 = m - cnt1;
				int l = 1,r = 2 * cnt2;
				ans = std::min(ans,a[r + 1]);
				while(l < r)
				{
					ans = std::min(ans,a[l] + a[r]);
					l++,r--;
				}
				printf("%d\n",ans);
			}
			else
			{
				int ans = 0x3f3f3f3f;
				int cnt1 = 2 * (m - sc / 2);
				int cnt2 = m - cnt1;
				int l = 1,r = 2 * cnt2;
				ans = std::min(ans,a[r + 1]);
				while(l < r)
				{
					ans = std::min(ans,a[l] + a[r]);
					l++,r--;
				}
				printf("%d\n",ans);
			}
		}
	}
}
namespace chain
{
	int a[N];
	inline bool judge(int x)
	{
		int cnt = 0;
		for(int i = 1,sum = 0;i <= n;i++) if((sum += a[i]) >= x) cnt++,sum = 0;
		return cnt >= m;
	}
	void main()
	{
		for(int i = 1;i < n;i++) a[i] = val[i << 1];
		int l = 0,r = (n - 1) * l,ans = 0;
		while(l <= r)
		{
			int mid = (l + r) >> 1;
			if(judge(mid)) ans = mid,l = mid + 1;
			else r = mid - 1;
		}
		printf("%d\n",ans);
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(head,-1,sizeof(head));
	n = getint();m = getint();
	int flag_a_1 = 1,flag_chain = 1;
	for(int i = 1,u,v,w;i < n;i++)
	{
		u = getint(),v = getint(),w = getint(),addedge(u,v,w);
		if(u != 1) flag_a_1 = 0;
		if(v != u + 1) flag_chain = 0;
	}
	if(m == 1) m1::main();
	else if(flag_a_1) a1::main();
	else if(flag_chain) chain::main();
	else printf("%d\n",rand());
	fclose(stdin);
	fclose(stdout);
	return 0;
}
