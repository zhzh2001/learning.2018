#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cctype>
typedef long long LL;
inline int getint()
{
	int r = 0,s = 0;char c = getchar();for(;!isdigit(c);c = getchar()) s |= c == '-';
	for(;isdigit(c);c = getchar()) r = (((r << 2) + r) << 1) + (c ^ '0');return s ? -r : r;
}
const int N = 100010;
int a[N],n,d[N];
LL sum = 0;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n = getint();
	for(int i = 1;i <= n;i++) a[i] = getint();
	for(int i = 1;i <= n;i++) d[i] = a[i] - a[i - 1];
	for(int i = 1;i <= n;i++) if(d[i] > 0) sum += d[i];
	printf("%lld\n",sum);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
