#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cctype>
const int N = 110,M = 25050;
int n,a[N],b[N];
inline int getint()
{
	int r = 0,s = 0;char c = getchar();for(;!isdigit(c);c = getchar()) s |= c == '-';
	for(;isdigit(c);c = getchar()) r = (((r << 2) + r) << 1) + (c ^ '0');return s ? -r : r;
}
int vis[M],niubi[M],woshabi[M];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T = getint();while(T--)
	{
		n = getint();
		for(int i = 1;i <= n;i++) b[i] = getint();
		std::sort(b + 1,b + n + 1);
		n = std::unique(b + 1,b + n + 1) - b - 1;
		memset(woshabi,0,sizeof(woshabi));
		for(int i = 1;i <= n;i++) a[i] = b[i],woshabi[a[i]] = 1;
		//for(int i = 1;i <= n;i++) printf("%d\n",a[i]);
		memset(niubi,0,sizeof(niubi));
		memset(vis,0,sizeof(vis));
		vis[0] = 1;
		for(int i = 1;i <= n;i++)
		{
			for(int j = 0;j <= 25000;j++)
			{
				if(a[i] + j > 25000) break;
				if(vis[j])
				{
					vis[a[i] + j] = 1;
					if(woshabi[a[i] + j] && j) niubi[a[i] + j] = 1;
				}
			}
		}
		int m = 0;
		for(int i = 1;i <= n;i++) if(!niubi[a[i]]) ++m;
		printf("%d\n",m);
		//puts("-----------");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
