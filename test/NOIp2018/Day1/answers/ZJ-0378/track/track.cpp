#include<bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 100100
using namespace std;
inline int read ()
{
	int s=0,w=1;
	char ch=getchar ();
	while (ch<'0'||ch>'9'){if (ch=='-') w=-1;ch=getchar ();}
	while ('0'<=ch&&ch<='9') s=(s<<1)+(s<<3)+(ch^48),ch=getchar ();
	return s*w;
}
struct edge{
	int v,w,nxt;
}e[MAXN<<1];
int n,m,cnt,s,t,sum,tot,root;
int head[MAXN],dep[MAXN],In[MAXN];
bool flag1=1,flag2=1,flag3=1;
void add (int u,int v,int w)
{
	e[++cnt].v=v;
	e[cnt].w=w;
	e[cnt].nxt=head[u];
	head[u]=cnt;
}
void dfs_l (int u,int fa)
{
	for (int i=head[u];i!=0;i=e[i].nxt)
		if (e[i].v!=fa)
		{
			dep[e[i].v]=dep[u]+e[i].w;
			dfs_l (e[i].v,u);
		}
}
int Max[MAXN];
bool check1 (int k)
{
	int l=1,r=tot;
	while (l<r)
	{
		int mid=(l+r)>>1;
		if (k>=Max[mid]) r=mid;
		else l=mid+1;
	}
	int Sum=tot-l+1;
	if (Max[l]<k) Sum=0,l++;
	r=l-1,l=1;
	while (l<r)
	{
		if (Max[l]+Max[r]>=k) Sum++,l++,r--;
		else l++;
	}
	return Sum>=m;
}
bool check2 (int k)
{
	int Sum=0,Cnt=0;
	for (int i=1;i<=tot;i++)
	{
		Sum+=Max[i];
		if (Sum>=k) Cnt++,Sum=0;
	}
	return Cnt>=m;
}
int f[MAXN];
bool used[MAXN];
int dfs (int u,int fa,int k)
{
	int A[3],tt=0;
	for (int i=head[u];i!=0;i=e[i].nxt)
		if (e[i].v!=fa)
		{
			int tmp=dfs (e[i].v,u,k)+e[i].w;
			f[u]+=f[e[i].v];
			if (tmp>=k) f[u]++;
			else A[++tt]=tmp;
		}
	if (tt==0) return 0;
	if (tt==1) return A[1];
	if (A[1]+A[2]>=k)
	{
		f[u]++;
		return 0;
	}
	return max (A[1],A[2]);
}
bool check3 (int k)
{
	memset (f,0,sizeof (f));
	dfs (root,0,k);
	return f[root]>=m;
}
int main()
{
	freopen ("track.in","r",stdin);
	freopen ("track.out","w",stdout);
	n=read ();m=read ();
	for (int i=1;i<n;i++)
	{
		int u=read (),v=read (),w=read ();
		add (u,v,w);add (v,u,w);
		sum+=w;
		In[u]++;In[v]++;
		if (u!=1&&v!=1) flag1=0;
		if (In[u]>3||In[v]>3) flag3=0;
		if (abs (u-v)!=1) flag2=0;
	}
	if (m==1)
	{
		dfs_l (1,0);
		for (int i=1;i<=n;i++) if (dep[i]>dep[s]) s=i;
		memset (dep,0,sizeof (dep));
		dfs_l (s,0);
		for (int i=1;i<=n;i++) if (dep[i]>dep[t]) t=i;
		printf ("%d",dep[t]);
		return 0;
	}
	if (flag1==1)
	{
		for (int i=head[1];i!=0;i=e[i].nxt)
			Max[++tot]=e[i].w;
		sort (Max+1,Max+tot+1);
		int l=1,r=sum/m;
		while (l<r)
		{
			int mid=(l+r+1)>>1;
			if (check1 (mid)) l=mid;
			else r=mid-1;
		}
		printf ("%d\n",l);
		return 0;
	}
	if (flag2==1)
	{
		for (int i=1;i<n;i++)
			Max[++tot]=e[head[i]].w;
		int l=1,r=sum/m;
		while (l<r)
		{
			int mid=(l+r+1)>>1;
			if (check2 (mid)) l=mid;
			else r=mid-1;
		}
		printf ("%d\n",l);
		return 0;
	}
	if (flag3==1)
	{
		for (int i=1;i<=n;i++)
			if (In[i]<=3)
			{
				root=i;
				break;
			}
		int l=1,r=sum/m;
		while (l<r)
		{
			int mid=(l+r+1)>>1;
			if (check3 (mid)) l=mid;
			else r=mid-1;
		}
		printf ("%d\n",l);
		return 0;
	}
	else printf ("%d\n",rand ());
	return 0;
}
