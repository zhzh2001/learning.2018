#include<bits/stdc++.h>
#define MAXN 200010
using namespace std;
typedef long long ll;
inline ll read ()
{
	ll s=0,w=1;
	char ch=getchar ();
	while (ch<'0'||ch>'9'){if (ch=='-') w=-1;ch=getchar ();}
	while ('0'<=ch&&ch<='9') s=(s<<1)+(s<<3)+(ch^48),ch=getchar ();
	return s*w;
}
ll n,a[MAXN];
ll s[MAXN],p,ans;
int main()
{
	freopen ("road.in","r",stdin);
	freopen ("road.out","w",stdout);
	n=read ();
	for (int i=1;i<=n;i++) a[i]=-1*read ();
	s[++p]=0;
	for (int i=1;i<=n;i++)
	{
		while (s[p]<a[i])
		{
			ans+=(s[p-1]>a[i]?(a[i]-s[p]):(s[p-1]-s[p]));
			p--;
		}
		s[++p]=a[i];
	}
	while (p!=1)
	{
		ans+=s[p-1]-s[p];
		p--;
	}
	printf ("%lld",ans);
	return 0;
}
