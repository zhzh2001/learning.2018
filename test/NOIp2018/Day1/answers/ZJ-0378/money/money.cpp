#include<bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 510
#define MAXV 50000
using namespace std;
int t,n,Maxv,ans,a[MAXN];
bool f[MAXV];
int main()
{
	freopen ("money.in","r",stdin);
	freopen ("money.out","w",stdout);
	scanf ("%d",&t);
	while (t--)
	{
		Maxv=-INF;
		memset (f,0,sizeof (f));
		scanf ("%d",&n);
		ans=n;
		for (int i=1;i<=n;i++)
		{
			scanf ("%d",&a[i]);
			Maxv=max (Maxv,a[i]);
		}
		sort (a+1,a+n+1);
		f[0]=1;
		for (int i=1;i<=n;i++)
		{
			if (f[a[i]]==1)
			{
				ans--;
				continue;
			}
			for (int j=a[i];j<=Maxv;j++)
				if (f[j-a[i]]) f[j]=1;
		}
		printf ("%d\n",ans);
	}
	return 0;
}
