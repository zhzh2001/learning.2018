#include <bits/stdc++.h>
using namespace std;

const int N = 5e4 + 50;

struct edge {
  int nxt, to, len;
} e[N << 1];
int fir[N], cnt;
int n, m;

inline void addedge(int x, int y, int l) {
  e[++ cnt] = (edge) { fir[x], y, l }; fir[x] = cnt;
}

int thresh, tot, deg[N];
vector <int> a[N];

inline int cound(vector <int> &a, int d) {
  int res = 0;
  for (int i = (int) a.size() - 1, j = 0; i > j; i --, j ++) {
    if (i == d) i --;
    if (i <= j) break;
    while (j < i && a[j] + a[i] < thresh) j ++;
    if (j == d) j ++;
    if (i <= j) break;
    res ++;
  }
  return res;
}

inline int Dfs(int x, int f) {
  a[x].clear();
  for (int i = fir[x]; i; i = e[i].nxt)
    if (e[i].to != f) {
      int t = Dfs(e[i].to, x) + e[i].len;
      if (t >= thresh) tot ++;
      else a[x].push_back(t);
    }
  sort(a[x].begin(), a[x].end());
  int brua =  cound(a[x], 2000000);
  tot += brua;
  if (brua * 2 == (int) a[x].size()) return 0;
  int t = 0;
  for (int i = 15; ~ i; i --)
    if (t + (1 << i) < (int) a[x].size()) {
      if ( cound(a[x], t + (1 << i)) == brua ) t += 1 << i;
    }
  return a[x][t];
}

int main() {
  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
  scanf("%d%d", &n, &m);
  for (int i = 1, x, y, l; i < n; i ++) {
    scanf("%d%d%d", &x, &y, &l);
    addedge(x, y, l);
    addedge(y, x, l);
    deg[x] ++; deg[y] ++;
  }
  for (int i = 1; i <= n; i ++) a[i].reserve(deg[i]);
  int L = 1, R = 1e9, ans = 0;
  while (L <= R) {
    tot = 0;
    thresh = (L + R) >> 1;
    tot += (Dfs(1, 0) >= thresh);
    if (tot >= m) L = thresh + 1, ans = thresh;
    else R = thresh - 1;
  }
  printf("%d\n", ans);
  return 0;
}
