#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 50;

int n;
vector <int> pos[N];
bool vis[N];

int main() {
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
  scanf("%d", &n);
  for (int i = 1, a; i <= n; i ++) {
    scanf("%d", &a);
    pos[a].push_back(i);
  }
  int tot = 0, ans = 0;
  for (int i = 10000; i; i --) {
    for (size_t j = 0; j < pos[i].size(); j ++) {
      int k = pos[i][j];
      tot ++;
      vis[k] = 1;
      if (vis[k - 1]) tot --;
      if (vis[k + 1]) tot --;
    }
    ans += tot;
  }
  printf("%d\n", ans);
  return 0;
}
