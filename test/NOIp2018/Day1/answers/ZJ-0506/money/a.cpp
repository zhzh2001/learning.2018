#include <bits/stdc++.h>
using namespace std;

const int N = 26000;

int n, a[200];
bool tab[N], rev[N];

inline void add(bool *a, int v) {
  for (int i = 0; i + v < N; i ++) a[i + v] |= a[i];
}

inline void solve() {
  memset(tab, 0, sizeof tab);
  memset(rev, 0, sizeof rev);
  tab[0] = rev[0] = 1;
  scanf("%d", &n);
  for (int i = 1; i <= n; i ++) {
    scanf("%d", &a[i]);
    add(tab, a[i]);
  }
  int m = 0;
  for (int i = 0; i < N; i ++)
    if (tab[i] != rev[i]) add(rev, i), m ++;
  printf("%d\n", m);
}

int main() {
  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
  int T;
  scanf("%d", &T);
  while (T --) solve();
  return 0;
}
