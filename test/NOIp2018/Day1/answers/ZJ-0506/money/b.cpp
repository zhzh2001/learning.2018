#include <bits/stdc++.h>
using namespace std;

const int T = 260000;

int n, a[200];
bitset <T> tab, rev;

inline void solve() {
  scanf("%d", &n);
  for (int i = 1; i <= n; i ++) scanf("%d", &a[i]);
  tab.reset();
  tab[0] = 1;
  for (int i = 1; i <= n; i ++) {
    int t = a[i];
    while (t < T) {
      tab |= tab << t;
      t <<= 1;
    }
  }
  rev.reset();
  rev[0] = 1;
  int m = 0;
  for (int i = 0; i < T; i ++)
    if (rev[i] != tab[i]) {
      m ++;
      int t = i;
      while (t < T) {
	rev |= rev << t;
	t <<= 1;
      }      
    }
  printf("%d\n", m);
}

int main() {
  int T;
  scanf("%d", &T);
  while (T --) solve();
  return 0;
}
