#include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstdlib>
using namespace std;
int a[105],b[105],n,tt,m,p;
bool f[25005];
bool cmp(int x,int y){return x<y;}
bool judge(int x){
	for(int i=0;i<=x;i++)f[i]=0;
	for(int i=1;i<=m;i++)f[b[i]]=1;
	for(int i=1;i<=x;i++){
		if(f[i])continue;
		for(int j=1;b[j]<=i&&j<=m;j++){
			if(f[i-b[j]]){f[i]=1;break;}
		}
	}
	return !f[x];
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	cin>>tt;
	while(tt--){
		cin>>n;
		m=0;
		for(int i=1;i<=n;i++)cin>>a[i];
		sort(a+1,a+1+n,cmp);
		if(a[1]==1)m=1;
		else{
			b[++m]=a[1];
			for(int i=2;i<=n;i++){
				p=1;
				for(int j=1;j<=m;j++){
					if(a[i]%b[j]==0){p=0;break;}
				}
				if(p)p=judge(a[i]);
				if(p)b[++m]=a[i];
			}
		}
		cout<<m<<endl;
	}
	return 0;
}
