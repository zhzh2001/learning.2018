#include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstdlib>
using namespace std;
int n,m,x,y,v,a,b,tot=0,ans=0,head[50005],to[50005],nxt[50005],w[50005],used[500005];
bool cmp(int x,int y){return x<y;}
void add(int x,int y,int v){
	tot++;
	to[tot]=y;
	w[tot]=v;
	nxt[tot]=head[x];
	head[x]=tot;
}
void dfs(int x,int v){
	if(ans<v)ans=v;
	used[x]=1;
	for(int i=head[x];i;i=nxt[i]){
		if(!used[i])dfs(to[i],v+w[i]);
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	a=1;b=1;
	for(int i=1;i<n;i++){
		cin>>x>>y>>v;
		if(x!=1&&y!=1)a=0;
		if(y!=x+1&&x!=y+1)b=0;
		add(x,y,v);
		add(y,x,v);
	}
	if(a){
		int v[50005];
		for(int i=head[1];i;i=nxt[i]){
			v[to[i]]=w[i];
		}
		sort(v+2,v+n,cmp);
		cout<<v[n-m+1]<<endl;
		return 0;
	}
	if(b){
		int f[2][50005],s[50005];
		for(int i=1;i<=n;i++)s[i+1]=s[i]+w[head[i]];
		for(int i=1;i<=m;i++){
			for(int j=i;j<=n;j++){
				for(int k=2;k<j;k++){
					int d=f[(i-1)&1][k]==0?min(s[k-1],s[j-1]-s[k-1]):min(f[(i-1)&1][k],s[j-1]-s[k-1]);
					if(f[i&1][j]<d)f[i&1][j]=d;
				}
			}
		}
		for(int i=m;i<=n;i++){
			int q=min(f[m&1][i],s[n]-s[i-1]);
			if(!ans||ans<q)ans=q;
		}
		cout<<ans<<endl;
		return 0;
	}
	if(m==1){
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++)used[j]=0;
			dfs(i,0);
		}
		cout<<ans<<endl;
	}
	return 0;
}
