#include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstdlib>
using namespace std;
int a[100005],n;
long long ans=0;
struct tree{
	int l;
	int r;
	int mi;
	int ma;
}r[400005];
void build(int x,int z,int y){
	r[x].l=z;r[x].r=y;
	if(z==y){r[x].mi=r[x].ma=a[z];return;}
	int mid=(z+y)>>1;
	build(x<<1,z,mid);
	build(x<<1|1,mid+1,y);
	r[x].mi=min(r[x<<1].mi,r[x<<1|1].mi);
	r[x].ma=max(r[x<<1].ma,r[x<<1|1].ma);
}
void down(int x,int d){
	if(r[x].mi==r[x].ma)return;
	r[x<<1].mi-=d;
	r[x<<1].ma-=d;
	r[x<<1|1].mi-=d;
	r[x<<1|1].ma-=d;
	down(x<<1,d);
	down(x<<1|1,d);
}
void solve(int x){
	if(r[x].mi==r[x].ma){if(r[x].mi)ans+=r[x].mi;return;}
	if(r[x].mi){ans+=r[x].mi;down(x,r[x].mi);}
	solve(x<<1);
	solve(x<<1|1);
}
int main(){
	//freopen("road.in","r",stdin);
	//freopen("road.out","w",stdout);
	cin>>n;
	for(int i=1;i<=n;i++)cin>>a[i];
	build(1,1,n);
	solve(1);
	cout<<ans<<endl;
	return 0;
}
