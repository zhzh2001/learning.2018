#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
bool vis[25010];
int a[110],n;
inline int max(int a,int b)
{
	return a>b ? a:b;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int kase;
	scanf("%d",&kase);
	for (int ii=1;ii<=kase;ii++)
	{
		int m=0;
		scanf("%d",&n);
		for (int i=1;i<=n;i++)
			scanf("%d",&a[i]),m=max(m,a[i]);
		sort(a+1,a+n+1);
		int ans=0;
		memset(vis,false,sizeof(vis));
		vis[0]=true;
		for (int i=1;i<=n;i++)
			if (!vis[a[i]])
			{
				ans++;
				for (int j=0;j+a[i]<=m;j++)
					if (vis[j])
						vis[j+a[i]]=true;
			}
		printf("%d\n",ans);
	}
}
