#include <cstdio>
#include <cstring>
#include <algorithm>
#include <set>
using namespace std;
const int MAXN=5e4+10;
int n,m,e0;
int frst[MAXN],nxt[MAXN<<1],ed[MAXN<<1],val[MAXN<<1];
inline void add(int a,int b,int c)
{
	nxt[++e0]=frst[a];
	frst[a]=e0;
	ed[e0]=b;
	val[e0]=c;
}
int cnt;
long long len;
long long f[MAXN],a[MAXN];
int an;
struct cmp
{
	bool operator () (const int &x,const int &y) const
	{
		if (a[x]==a[y])
			return x<y;
		return a[x]<a[y];
	}
};
set<int,cmp>s;
typedef set<int>::iterator SET;
inline int solve()
{
	s.clear();
	sort(a+1,a+an+1);
	for (int i=1;i<=an;i++)
		s.insert(i);
	int l=1;
	while (l<an && a[l]+a[l+1]<len)
		l++;
	for (int r=l+1;r<=an;r++)
	{
		s.erase(r);
		a[0]=len-a[r];
		SET i=s.lower_bound(0);
		if (i!=s.end() && *i<r)
			cnt++,s.erase(i);
		else
			s.insert(r);
	}
	if (s.empty())
		return 0;
	SET i=s.end();i--;
	return a[*i];
}
inline void dfs(int x,int fa)
{
	for (int r=frst[x],y=ed[r];r;y=ed[r=nxt[r]])
		if (y!=fa)
			dfs(y,x),f[y]+=val[r];
	an=0;
	for (int r=frst[x],y=ed[r];r;y=ed[r=nxt[r]])
		if (y!=fa)
		{
			if (f[y]>=len)
				cnt++;
			else
				a[++an]=f[y];
		}
	f[x]=solve();
}
inline bool check()
{
	cnt=0;
	dfs(1,0);
	return cnt>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	long long l=0,r=0,ans=0;
	for (int i=1;i<n;i++)
	{
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c),add(a,b,c),add(b,a,c),r+=c;
	}
	while (l<=r)
	{
		len=(l+r)>>1;
		if (check())
			ans=len,l=len+1;
		else
			r=len-1;
	}
	printf("%lld\n",ans);
}
