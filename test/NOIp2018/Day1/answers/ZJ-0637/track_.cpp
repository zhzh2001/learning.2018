#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define N 50010
int n,m,SUM=0;
struct edge{int to,nxt,w;}e[N<<1];int hed[N],cnt=0;
void add(int x,int y,int w){
	e[++cnt].nxt=hed[x];hed[x]=cnt;e[cnt].to=y;e[cnt].w=w;
}
int lim,num[N],s[N];
int tmp[N],tot,pos,tar;
inline int cal(int del){
	int pos=1,ans=0;for(int i=tot;i>0;i--)if(i!=del){
		while(pos<i&&tmp[pos]+tmp[i]<lim)pos++;
		if(pos==del)pos++;
		if(pos<i)ans++,pos++;else break;
	}
	return ans;
}
inline void DFS(int x,int fa){
	//printf("%d ",x);
	for(int i=hed[x];i;i=e[i].nxt)if(e[i].to!=fa){
		DFS(e[i].to,x),num[x]+=num[e[i].to];
		s[e[i].to]+=e[i].w;if(s[e[i].to]>=lim)num[x]++,s[e[i].to]=0;
	}
	tot=0;
	for(int i=hed[x];i;i=e[i].nxt)if(e[i].to!=fa)if(s[e[i].to])tmp[++tot]=s[e[i].to];
	sort(tmp+1,tmp+1+tot);
	
	
	pos=1,tar=0;for(int i=tot;i>0;i--){
		while(pos<i&&tmp[pos]+tmp[i]<lim)pos++;
		if(pos<i)tar++,pos++;else break;
	}
	num[x]+=tar; s[x]=0;
	int L=1,R=tot,mid;
	while(L<=R){
		mid=(L+R)>>1;
		if(cal(mid)==tar)s[x]=tmp[mid],L=mid+1;else R=mid-1;
	}
}
void cas4(){
	int L=1,R=SUM,mid,ans=0;
	while(L<=R){
		mid=(L+R)>>1;printf("mid %d\n",mid);
		lim=mid;for(int i=1;i<=n;i++)s[i]=num[i]=0; DFS(1,0);
		if(num[1]>=m)ans=mid,L=mid+1;else R=mid-1;
	}
	printf("%d\n",ans);
}
int main(){
	freopen("track4.in","r",stdin);
	//freopen("track.ans","w",stdout);
	n=read(),m=read();
	for(int i=1,x,y,w;i<n;i++){
		x=read(),y=read(),w=read(),SUM+=w;
		add(x,y,w),add(y,x,w);
	}
	puts("OK");
	cas4();
}
