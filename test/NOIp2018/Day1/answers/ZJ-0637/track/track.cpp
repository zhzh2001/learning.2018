#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define N 50010
int n,m,SUM=0;
struct edge{int to,nxt,w;}e[N<<1];int hed[N],cnt=0;
void add(int x,int y,int w){
	e[++cnt].nxt=hed[x];hed[x]=cnt;e[cnt].to=y;e[cnt].w=w;
}
int st,mx;
inline void dfs(int x,int fa,int p){
	if(p>mx)mx=p,st=x;
	for(int i=hed[x];i;i=e[i].nxt)if(e[i].to!=fa)dfs(e[i].to,x,p+e[i].w);
}
void cas1(){mx=0,st=1,dfs(1,0,0),dfs(st,0,0);printf("%d\n",mx);}

int d[N];
int pd(int p){
	int num=0,nn=n-1,t=1;
	for(int i=n-1;i>0;i--){if(d[i]>=p)num++,nn=i-1;else break;}
	if(num>=m)return 1;
	for(int i=nn;i>0;i--){
		while(t<i&&d[t]+d[i]<p)t++;
		if(t<i)num++,t++;
	}
	return num>=m;
}
void cas2(){
	int tmp=0;for(int i=hed[1];i;i=e[i].nxt)d[++tmp]=e[i].w;
	sort(d+1,d+n);
	int L=1,R=SUM,mid,ans=0;
	while(L<=R){mid=(L+R)>>1;if(pd(mid))ans=mid,L=mid+1;else R=mid-1;}
	printf("%d\n",ans);
}
int check(int p){
	int num=0,s=0;for(int i=1;i<n;i++){s+=d[i];if(s>=p)num++,s=0;}return num>=m;
}
void cas3(){
	for(int i=1;i<=n;i++)for(int j=hed[i];j;j=e[j].nxt)if(e[j].to==i+1)d[i]=e[j].w;
	int L=1,R=SUM,mid,ans=0;
	while(L<=R){mid=(L+R)>>1;if(check(mid))ans=mid,L=mid+1;else R=mid-1;}
	printf("%d\n",ans);
}
int lim,num[N],s[N];
int A[N],tot,pos,tar;
inline int cal(int del){
	int pp=1,ans=0;for(int i=tot;i>0;i--)if(i!=del){
		while(pp<i&&A[pp]+A[i]<lim)pp++;
		if(pp==del)pp++;
		if(pp<i)ans++,pp++;else break;
	}
	return ans;
}
inline void DFS(int x,int fa){
	for(int i=hed[x];i;i=e[i].nxt)if(e[i].to!=fa){
		DFS(e[i].to,x),num[x]+=num[e[i].to];
		s[e[i].to]+=e[i].w;if(s[e[i].to]>=lim)num[x]++,s[e[i].to]=0;
	}
	tot=0;
	for(int i=hed[x];i;i=e[i].nxt)if(e[i].to!=fa)if(s[e[i].to])A[++tot]=s[e[i].to];
	sort(A+1,A+1+tot);
	
	pos=1,tar=0;for(int i=tot;i>0;i--){
		while(pos<i&&A[pos]+A[i]<lim)pos++;
		if(pos<i)tar++,pos++;else break;
	}
	num[x]+=tar; s[x]=0;
	int L=1,R=tot,mid;
	while(L<=R){
		mid=(L+R)>>1;if(cal(mid)==tar)s[x]=A[mid],L=mid+1;else R=mid-1;
	}
}
void cas4(){
	int L=1,R=SUM,mid,ans=0;
	while(L<=R){
		mid=(L+R)>>1;lim=mid;for(int i=1;i<=n;i++)s[i]=num[i]=0; DFS(1,0);
		if(num[1]>=m)ans=mid,L=mid+1;else R=mid-1;
	}
	printf("%d\n",ans);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	int p1=1,p2=1;
	for(int i=1,x,y,w;i<n;i++){
		x=read(),y=read(),w=read(),SUM+=w;
		if(x!=1&&y!=1)p1=0;
		if(x!=y+1&&y!=x+1)p2=0;
		add(x,y,w),add(y,x,w);
	}
	if(m==1){cas1();return 0;}
	if(p1){cas2();return 0;}
	if(p2){cas3();return 0;}
	cas4();return 0;
}
