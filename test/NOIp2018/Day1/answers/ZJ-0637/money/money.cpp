#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define N 110
int n,a[N],b[25500];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--){
		n=read();for(int i=1;i<=n;i++)a[i]=read();
		sort(a+1,a+1+n);
		int ans=0,x,mx=a[n];
		for(int i=1;i<=mx;i++)b[i]=0; b[0]=1;
		for(int i=1;i<=n;i++)if(!b[a[i]]){
			ans++,x=a[i];
			for(int i=0;i+x<=mx;i++)if(b[i])b[i+x]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
