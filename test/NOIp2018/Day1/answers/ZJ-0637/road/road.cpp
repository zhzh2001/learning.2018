#include<cstdio>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define N 100010
int n,a[N],b[N];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();for(int i=1;i<=n;i++)a[i]=read();
	for(int i=0;i<=n;i++)b[i]=a[i+1]-a[i];
	//for(int i=0;i<=n;i++)printf("%d ",b[i]);puts("");
	int ans=0;for(int i=0;i<=n;i++)if(b[i]>0)ans+=b[i];
	printf("%d\n",ans);
	return 0;
}
