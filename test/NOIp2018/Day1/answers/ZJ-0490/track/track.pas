var
  tot,que:longint;
  t,head,tail,a1,a2,a3,len,a4:longint;
  x,i,j,m,n,k,p:longint;
  dis:array[0..1005,0..1005]of longint;
  next,other,a,w:array[0..200005]of longint;
  f,deep,father:array[0..1005]of longint;
  ans,sum:array[0..1005,0..11]of longint;
begin
  assign(input,'track.in');
  assign(output,'track.out');
  reset(input); rewrite(output);
  readln(n,m);
  for i:=1 to n-1 do
    begin
      readln(a1,a2,a3);
      inc(len);
      next[len]:=a[a1]; a[a1]:=len; other[len]:=a2; w[len]:=a3;
      inc(len);
      next[len]:=a[a2]; a[a2]:=len; other[len]:=a1; w[len]:=a3;
    end;
  head:=0; tail:=1; f[1]:=1;
  while head<tail do
    begin
      inc(head);
      k:=a[f[head]];
      while k<>0 do
        begin
          p:=other[k];
          if father[f[head]]<>p then
            begin
              inc(tail); f[tail]:=p;
              father[p]:=f[head];
              deep[p]:=deep[f[head]]+1;
              ans[p,0]:=f[head]; sum[p,0]:=w[k];
            end;
          k:=next[k];
        end;
    end;
  for j:=1 to 11 do
    for i:=1 to n do
      begin
        sum[i,j]:=sum[i,j-1]+sum[ans[i,j-1],j-1];
        ans[i,j]:=ans[ans[i,j-1],j-1];
      end;
  for i:=1 to n do
    for j:=1 to n do
      begin
        a1:=i; a2:=j; tot:=0;
        if deep[a1]<deep[a2] then
          begin
            a3:=a1; a1:=a2; a2:=a3;
          end;
        a4:=deep[a1]-deep[a2]; t:=0;
        while a4<>0 do
          begin
            if a4 and 1=1 then
              begin
                tot:=tot+sum[a1,t];
                a1:=ans[a1,t];
              end;
            inc(t); a4:=a4 shr 1;
          end;
        if a1=a2 then
          begin
            if que<tot then
              que:=tot;
            continue;
          end;
        for x:=11 downto 0 do
          if ans[a1,x]<>ans[a2,x] then
            begin
              tot:=tot+sum[a1,x]+sum[a2,x];
              a1:=ans[a1,x]; a2:=ans[a2,x];
            end;
        tot:=tot+sum[a1,0]+sum[a2,0];
        if que<tot then
          que:=tot;
      end;
  writeln(que);
  close(input); close(output);
end.