var
  bi:boolean;
  tot,ans:longint;
  len,t,i,j,m,n,k,p:longint;
  a:array[0..105]of longint;
  f:array[0..30005]of boolean;
begin
  assign(input,'money.in');
  assign(output,'money.out');
  reset(input); rewrite(output);
  readln(t);
  while t<>0 do
    begin
      dec(t);
      readln(n); len:=0;
      for i:=1 to n do
        begin
          read(k); bi:=true;
          for j:=1 to len do
            begin
              if k mod a[j]=0 then
                begin
                  bi:=false; break;
                end;
              if a[j] mod k=0 then
                begin
                  a[j]:=k; bi:=false; break;
                end;
            end;
          if bi=true then
            begin
              inc(len); a[len]:=k;
            end;
        end;
      fillchar(f,sizeof(f),false);
      f[0]:=true;
      for i:=1 to len do
        for j:=a[i] to 30005 do
          f[j]:=f[j] or f[j-a[i]];

      for i:=30005 downto 1 do
        if f[i]=false then
          begin
            ans:=i; break;
          end;
      tot:=0;
      for i:=1 to len do
        if a[i]<ans then
          inc(tot);

      writeln(tot);
    end;
  close(input); close(output);
end.