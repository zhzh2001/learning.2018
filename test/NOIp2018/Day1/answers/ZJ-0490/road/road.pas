var
  ans:int64;
  time,t,minn,head,tail:longint;
  i,j,m,n,k,p:longint;
  fx,fy:array[0..5000005]of longint;
  a,b:array[0..100005]of longint;
begin
  assign(input,'road.in');
  assign(output,'road.out');
  reset(input); rewrite(output);
  readln(n);
  for i:=1 to n do
    read(a[i]);
  head:=0; tail:=1; fx[1]:=1; fy[1]:=n;
  while (head<tail)or(time>0) do
    begin
      inc(head);
      if head=5000006 then
        begin
          head:=1; dec(time);
        end;
      if (head>tail)and(time=0) then break;

      if fx[head]>fy[head] then
        continue;

      minn:=maxlongint;
      for i:=fx[head] to fy[head] do
        if minn>a[i] then
          minn:=a[i];
      t:=0;
      ans:=ans+minn;
      for i:=fx[head] to fy[head] do
        begin
          a[i]:=a[i]-minn;
          if a[i]=0 then
            begin
              inc(t); b[t]:=i;
            end;
        end;
      inc(tail);
      if tail=5000006 then
        begin
          tail:=1; inc(time);
        end;
      fx[tail]:=fx[head]; fy[tail]:=b[1]-1;
      for i:=2 to t do
        begin
          inc(tail);
          if tail=5000006 then
            begin
              tail:=1; inc(time);
            end;
          fx[tail]:=b[i-1]+1; fy[tail]:=b[i]-1;
        end;
      inc(tail);
      if tail=5000006 then
        begin
          tail:=1; inc(time);
        end;
      fx[tail]:=b[t]+1; fy[tail]:=fy[head];
    end;
  writeln(ans);
  close(input); close(output);
end.