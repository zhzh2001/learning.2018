#include <bits/stdc++.h>
#define ll long long
#define ull unsigned long long
using namespace std;
const int N=50009,M=50009*2;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return num;
}
int n,m,minn=(1<<31)-1,maxn;
int head[N],nxt[M],edge[M],ver[M],tot=1;
int w[N],w1[N];
int kk,ans=0;
bool cmp(int a,int b){return a<b;}
void add(int u,int v,int w){
	ver[++tot]=v;edge[tot]=w;nxt[tot]=head[u];head[u]=tot;
	ver[++tot]=u;edge[tot]=w;nxt[tot]=head[v];head[v]=tot;
}
bool check(int mid){
	int cnt=0,now=0;
	for(int i=2;i<=n;i++){
		now+=w[i];
		if(now>=mid){
			cnt++;
			now=0;
			if(cnt>=m)return 1;
		}
	}
	return 0;
}
bool check2(int mid){
	int i=2,j=n,cnt=0;
	while(w[j]>=mid&&j>1)j--,cnt++;
	while(i<j){
		while(w[j]+w[i]<mid&&i!=j)i++;
		if(i==j)break;
		i++;j--;
		cnt++;
	}
	return (cnt>=m);
}
void work1(){
	int mid,l=minn,r=maxn;
	while(l<=r){
		mid=(l+r)>>1;
		if(check(mid))l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",r);
}
void work2(){
	sort(w+2,w+1+n,cmp);
	int mid,l=minn,r=maxn;
	while(l<=r){
		mid=(l+r)>>1;
		if(check2(mid))l=mid+1;
		else r=mid-1;
	}
	
	printf("%d\n",r);
}
void dfs(int x,int pre,int val){
	if(val>ans){
		ans=val;
		kk=x;
	}
	for(int i=head[x];i;i=nxt[i]){
		if(ver[i]==pre)continue;
		dfs(ver[i],x,val+edge[i]);
	}
}
void work3(){
	int kkk=rand()%n+1;
	dfs(kkk,kkk,0);
	dfs(kk,kk,0);
	printf("%d\n",ans);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	srand(19260817);
	n=read();m=read();
	if(m>n-1){printf("-1\n");return 0;}
	int flag=1,fflag=1;
	for(int i=1;i<n;i++){
		int u=read(),v=read(),ww=read();
		maxn+=ww;
		minn=min(minn,ww);
		add(u,v,ww);
		if(u!=1)fflag=0;
		if(v!=u+1)flag=0;
		w[v]=ww;
	}
	if(m==n-1){
		printf("%d\n",minn);
		return 0;
	}
	if(m==1)work3();
	else if(flag)work1();
	else if(fflag)work2();
	return 0;
}


