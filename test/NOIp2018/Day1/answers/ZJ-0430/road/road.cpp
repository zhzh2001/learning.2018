#include <bits/stdc++.h>
#define ll long long
#define ull unsigned long long
using namespace std;
const int N=1e5+100;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return num;
}
int n;
int q[N],tt=1;
ll ans=0;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	ans=q[++tt]=read();
	for(int i=2;i<=n;i++){
		int a=read();
		if(a<=q[tt]){
			while(q[tt]>=a&&tt>0)tt--;
			q[++tt]=a;
		}else {
			ans+=a-q[tt];
			q[++tt]=a;
		}
	}
	printf("%lld\n",ans);
	return 0;
}
