#include <bits/stdc++.h>
#define ll long long
#define ull unsigned long long
using namespace std;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return num;
}
int n,a[109],m=0,maxn=0;
bool f[25009];
bool cmp(int a,int b){return a<b;}
void work(){
	memset(f,0,sizeof(f));
	n=read();m=0;
	for(int i=1;i<=n;i++){
		a[i]=read();
		maxn=max(a[i],maxn);
	}
	sort(a+1,a+1+n,cmp);
	for(int i=1;i<=n;i++){
		if(!f[a[i]])m++;
		f[a[i]]=1;
		for(int k=1;k<=maxn;k++){
			if(!f[k])continue;
			f[k]=1;
			for(int j=k+a[i];j<=maxn&&!f[j];j+=a[i])f[j]=1;
		}
	}
	printf("%d\n",m);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int Case=read();
	while(Case--)work();
	return 0;
}
