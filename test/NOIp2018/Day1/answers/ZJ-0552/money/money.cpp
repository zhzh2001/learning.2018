#include<bits/stdc++.h>
using namespace std;
const int N=30000,M=25000;
int T,n,a[N],f[N];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		memset(f,0,sizeof(f));
		f[0]=1;
		int ans=0;
		for(int i=1;i<=n;i++)if(!f[a[i]])
		{
			for(int j=a[i];j<=M;j++)f[j]|=f[j-a[i]];
			ans++;
		}
		printf("%d\n",ans);
	}
	return 0;
}
