#include<bits/stdc++.h>
using namespace std;
const int N=50005,M=5e8;
struct E{int to,v,next;}edge[N<<1];
int n,m,head[N],tot,lim,f[N],g[N],a[N],rt,cnt,b[N],L[N],R[N],len;
struct T{int l,r,v;}t[N*30];
inline int newnode(){cnt++;t[cnt].l=0;t[cnt].r=0;t[cnt].v=0;return cnt;}
void insert(int&x,int l,int r,int v)
{
	if(!x)x=newnode();
	t[x].v++;
	if(l==r)return;
	int mid=(l+r)>>1;
	if(v<=mid)insert(t[x].l,l,mid,v);
	else insert(t[x].r,mid+1,r,v);
}
void get(int u,int l,int r,int left,int right)
{
	if(!u)return;
	if(left<=l&&right>=r){b[++len]=u;L[len]=l;R[len]=r;return;}
	int mid=(l+r)>>1;
	if(left<=mid)get(t[u].l,l,mid,left,right);
	if(right>mid)get(t[u].r,mid+1,r,left,right);
}
int getmin(int u,int l,int r,int left,int right)
{
	t[u].v--;
	if(l==r)return l;
	int mid=(l+r)>>1;
	if(right<=mid)return getmin(t[u].l,l,mid,left,right);
	if(left>mid)return getmin(t[u].r,mid+1,r,left,right);
	if(t[t[u].l].v)return getmin(t[u].l,l,mid,left,right);
	else return getmin(t[u].r,mid+1,r,left,right);
}
int getnxt(int x)
{
	len=0;get(rt,1,M,x,M);
	int flag=0;
	for(int i=1;i<=len;i++)if(t[b[i]].v)return getmin(rt,1,M,L[i],R[i]);
	return M;
}
void dfs(int u,int fa)
{
	for(int i=head[u];i!=-1;i=edge[i].next)if(edge[i].to!=fa)dfs(edge[i].to,u);
	int num=0;
	for(int i=head[u];i!=-1;i=edge[i].next)if(edge[i].to!=fa)a[++num]=g[edge[i].to]+edge[i].v;
	f[u]=0;rt=0;cnt=0;
	for(int i=1;i<=num;i++)
		if(a[i]<lim)insert(rt,1,M,a[i]);
		else f[u]++;
	int mx=0;
	while(t[rt].v)
	{
		int x=getmin(rt,1,M,1,M),y=getnxt(lim-x);
		if(y==M){if(x>mx)mx=x;continue;}
		else f[u]++;
	}
	g[u]=mx;
}
inline bool check(int x)
{
	lim=x;
	dfs(1,0);
	int sum=0;
	for(int i=1;i<=n;i++)sum+=f[i];
	return sum>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(head,-1,sizeof(head));
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		edge[++tot]=(E){y,z,head[x]};head[x]=tot;
		edge[++tot]=(E){x,z,head[y]};head[y]=tot;
	}
	int l=0,r=5e8;
	while(l<r)
	{
		int mid=(l+r+1)>>1;
		if(check(mid))l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
