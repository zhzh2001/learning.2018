#include<bits/stdc++.h>
using namespace std;
const int N=50005,M=5e8;
struct T{int l,r,v,sz,sm,ky;}t[N];
struct E{int to,v,next;}edge[N<<1];
int n,m,head[N],tot,lim,f[N],g[N],a[N],rt,cnt;
inline int newnode(int x){cnt++;t[cnt].l=0;t[cnt].r=0;t[cnt].v=x;t[cnt].sz=1;t[cnt].sm=1;t[cnt].ky=rand();return cnt;}
inline void update(int x){t[x].sz=t[t[x].l].sz+t[t[x].r].sz+t[x].sm;}
inline void lrotate(int&x)
{
	int y=t[x].r;
	t[x].r=t[y].l;
	t[y].l=x;
	update(x);
	update(y);
	x=y;
}
inline void rrotate(int&x)
{
	int y=t[x].l;
	t[x].l=t[y].r;
	t[y].r=x;
	update(x);
	update(y);
	x=y;
}
void ins(int&x,int v)
{puts("a");
	if(!x){x=newnode(v);return;}
	if(t[x].v==v){t[x].sm++;t[x].sz++;return;}
	if(v<t[x].v)
	{
		ins(t[x].l,v);
		if(t[t[x].l].ky>t[x].ky)rrotate(x);
	}
	else
	{
		ins(t[x].r,v);
		if(t[t[x].r].ky>t[x].ky)lrotate(x);
	}
	update(x);
}
void del(int&x,int v)
{puts("b");
	if(t[x].v==v)
	{
		if(t[x].sm>1){t[x].sm--;t[x].sz--;return;}
		while(t[x].l&&t[x].r)
		{
			puts("e");
			if(t[t[x].l].ky>t[t[x].r].ky)rrotate(x),x=t[x].r;
			else lrotate(x),x=t[x].l;
		}
		x=t[x].l+t[x].r;
		return;
	}
	if(v<t[x].v)del(t[x].l,v);
	else del(t[x].r,v);
	update(x);
}
int getrk(int x,int v)
{puts("c");
	if(!x)return 0;
	if(v<=t[x].v)return getrk(t[x].l,v);
	return getrk(t[x].r,v)+t[t[x].l].sz+t[x].sm;
}
int getkth(int x,int k)
{puts("d");
	if(k<=t[t[x].l].sz)return getkth(t[x].l,k);
	if(k<=t[t[x].l].sz+t[x].sm)return t[x].v;
	return getkth(t[x].r,k-t[t[x].l].sz-t[x].sm);
}
inline int getnxt(int x)
{
	int k=getrk(rt,x);
	if(k==t[rt].sz)return M;
	return getkth(rt,k+1);
}
void print(int x)
{
	if(!x)return;
	printf("%d %d %d\n",x,t[x].l,t[x].r);
	print(t[x].l);print(t[x].r);
}
void dfs(int u,int fa)
{
	for(int i=head[u];i!=-1;i=edge[i].next)if(edge[i].to!=fa)dfs(edge[i].to,u);
	int num=0;
	for(int i=head[u];i!=-1;i=edge[i].next)if(edge[i].to!=fa)a[++num]=g[edge[i].to]+edge[i].v;
	memset(t,0,sizeof(t));
	f[u]=0;rt=0;cnt=0;
	for(int i=1;i<=num;i++)
		if(a[i]<lim)ins(rt,a[i]);
		else f[u]++;
	int mx=0;
	while(t[rt].sz)
	{
		//if(u==1)print(rt);
		int x=getkth(rt,1);del(rt,x);
		int y=getnxt(lim-x);
		if(y==M){if(x>mx)mx=x;continue;}
		else f[u]++,del(rt,y);
	}
	g[u]=mx;
}
inline bool check(int x)
{
	lim=x;
	dfs(1,0);
	int sum=0;
	for(int i=1;i<=n;i++)sum+=f[i];
	return sum>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	//freopen("track.out","w",stdout);
	memset(head,-1,sizeof(head));
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		edge[++tot]=(E){y,z,head[x]};head[x]=tot;
		edge[++tot]=(E){x,z,head[y]};head[y]=tot;
	}
	int l=0,r=5e8;
	while(l<r)
	{
		int mid=(l+r+1)>>1;
		if(check(mid))l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
