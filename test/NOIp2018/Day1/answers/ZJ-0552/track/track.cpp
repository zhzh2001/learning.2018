#include<bits/stdc++.h>
using namespace std;
const int N=50005,INF=5e8;
struct E{int to,v,next;}edge[N<<1];
int n,m,head[N],tot,lim,f[N],g[N],a[N],h[N],mxlen;
multiset<int> st;
void getlen(int u,int fa)
{
	int mx1=0,mx2=0;
	for(int i=head[u];i!=-1;i=edge[i].next)if(edge[i].to!=fa)
	{
		getlen(edge[i].to,u);
		int tmp=h[edge[i].to]+edge[i].v;
		if(tmp>h[u])h[u]=tmp;
		if(tmp>mx1)mx2=mx1,mx1=tmp;
		else if(tmp>mx2)mx2=tmp;
	}
	if(mx1+mx2>mxlen)mxlen=mx1+mx2;
}
void dfs(int u,int fa)
{
	for(int i=head[u];i!=-1;i=edge[i].next)if(edge[i].to!=fa)dfs(edge[i].to,u);
	int num=0;
	for(int i=head[u];i!=-1;i=edge[i].next)if(edge[i].to!=fa)a[++num]=g[edge[i].to]+edge[i].v;
	f[u]=0;
	st.clear();
	for(int i=1;i<=num;i++)
		if(a[i]<lim)st.insert(a[i]);
		else f[u]++;
	int mx=0;
	while(st.size())
	{
		int x=*st.begin();st.erase(st.begin());
		multiset<int>::iterator it=st.lower_bound(lim-x);
		if(it==st.end()){if(x>mx)mx=x;continue;}
		else f[u]++,st.erase(it);
	}
	g[u]=mx;
}
inline bool check(int x)
{
	lim=x;
	dfs(1,0);
	int sum=0;
	for(int i=1;i<=n;i++)sum+=f[i];
	return sum>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(head,-1,sizeof(head));
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		edge[++tot]=(E){y,z,head[x]};head[x]=tot;
		edge[++tot]=(E){x,z,head[y]};head[y]=tot;
	}
	getlen(1,0);
	int l=0,r=min(mxlen,INF/m);
	while(l<r)
	{
		int mid=(l+r+1)>>1;
		if(check(mid))l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
