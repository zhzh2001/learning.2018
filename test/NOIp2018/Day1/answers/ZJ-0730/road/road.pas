
var
  n,i,l,j,min:longint;
  ans:int64;
  a:array[-10..110000]of longint;
  o:boolean;
begin
  assign(input,'road.in');reset(input);
  assign(output,'road.out');rewrite(output);
  readln(n);
  for i:=1 to n do read(a[i]);
  inc(n);
  repeat
    o:=true;
    for i:=1 to n do if a[i]>0 then
    begin
      o:=false;
      break;
    end;
    if o then break;
    l:=0;min:=maxlongint;
    for i:=1 to n do
    if (a[i]=0) then
    begin
      if min<>maxlongint then
      begin
        for j:=i-l to i-1 do dec(a[j],min);
        inc(ans,min);
        l:=0;min:=maxlongint;
      end;
    end else
    begin
      if a[i]<min then min:=a[i];
      inc(l);
    end;
  until false;
  writeln(ans);
  close(input);close(output);
end.
