#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("money.in");
ofstream fout("money.out");
const int N=105,M=25005;
int a[N];
bool f[M],del[N];
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int n;
		fin>>n;
		for(int i=1;i<=n;i++)
			fin>>a[i];
		sort(a+1,a+n+1);
		fill(del+1,del+n+1,false);
		for(int i=n;i;i--)
		{
			fill(f+1,f+a[i]+1,false);
			f[0]=true;
			for(int j=1;j<=n;j++)
				if(j!=i&&!del[j])
					for(int k=a[j];k<=a[i];k++)
						f[k]|=f[k-a[j]];
			del[i]=f[a[i]];
		}
		fout<<count(del+1,del+n+1,false)<<endl;
	}
	return 0;
}
