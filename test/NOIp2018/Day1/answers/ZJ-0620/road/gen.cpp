#include<fstream>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("road.in");
const int n=100000;
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<endl;
	uniform_int_distribution<> dn(0,10000);
	for(int i=1;i<=n;i++)
		fout<<dn(gen)<<' ';
	fout<<endl;
	return 0;
}
