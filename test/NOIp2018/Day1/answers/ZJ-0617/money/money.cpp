#include <bits/stdc++.h>
#define res register int
#define N 105
#define M 25005
using namespace std;
int a[N],f[M],n,maxn,ans,T;
bool can[N];
inline int read() {
	res w=0,X=0;
	register char ch=0;
	while(!isdigit(ch)) {
		w|=ch=='-';
		ch=getchar();
	}
	while(isdigit(ch)) {
		X=(X<<1)+(X<<3)+(ch^48);
		ch=getchar();
	}
	return w?-X:X;
}
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(T=read(); T; T--) {
		memset(f,0,sizeof(f));
		memset(can,0,sizeof(can));
		ans=0;
		n=read();
		for(res i=1; i<=n; i++)
			a[i]=read();
		sort(a+1,a+n+1);
		maxn=a[n];
		f[0]=1;
		for(res i=1; i<=n; i++) {
			if(can[i]) continue;
			ans++;
			for(res j=a[i];j<=maxn;j++)
				f[j]|=f[j-a[i]];
			for(res j=i+1; j<=n; j++)
				if(f[a[j]])
					can[j]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}

