#include <bits/stdc++.h>
#define ll long long
#define res register int
#define N 100005
using namespace std;
int a[N],n;
int ans;
inline int read(){
	res w=0,X=0;register char ch=0;
	while(!isdigit(ch)){w|=ch=='-';ch=getchar();}
	while(isdigit(ch)){X=(X<<1)+(X<<3)+(ch^48);ch=getchar();}
	return w?-X:X;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(res i=1;i<=n;i++)
	  a[i]=read();
	for(res i=1;i<=n;i++)
	  ans+=max(a[i]-a[i-1],0);
	printf("%d\n",ans);
	return 0;
}
