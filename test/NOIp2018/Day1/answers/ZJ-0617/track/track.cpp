#include <bits/stdc++.h>
#define res register int
#define N 50005
using namespace std;
int n,m,tot,d[N],total,ans;
bool flag=1,ifok=1,flag1=-1;
inline int read() {
	res w=0,X=0;
	register char ch=0;
	while(!isdigit(ch)) {
		w|=ch=='-';
		ch=getchar();
	}
	while(isdigit(ch)) {
		X=(X<<1)+(X<<3)+(ch^48);
		ch=getchar();
	}
	return w?-X:X;
}
struct papa {
	int to,next,w;
} e[N<<1];
inline void add(res x,res y,res we){
	e[++tot].to=y;
	e[tot].next=d[x];
	d[x]=tot;
	e[tot].w=we;
}
namespace task1 {
	int dis[N],maxn;
	void dfs(res now,res fa) {
		for(res i=d[now]; i; i=e[i].next){
			res x=e[i].to;
			if(x==fa) continue;
			dis[x]=dis[now]+e[i].w;
			dfs(x,now);
		}
	}
	void MAIN(){
		dis[1]=0;
		dfs(1,0);
		for(res i=1;i<=n;i++)
		  if(dis[i]>dis[maxn])
		    maxn=i;
		dis[maxn]=0;
		dfs(maxn,0);
		for(res i=1;i<=n;i++)
		  if(dis[i]>dis[maxn])
		    maxn=i;
		printf("%d\n",dis[maxn]);
	}
}
namespace task2{
	int use;
	void dfs(res now,res fa,res nowdis,res val){
		if(nowdis>=val) nowdis=0,use++; 
		for(res i=d[now];i;i=e[i].next){
			res x=e[i].to;
			if(x==fa) continue;
			dfs(x,now,nowdis+e[i].w,val);
		}
	}
	void MAIN(){
		res l=1,r=total;
		while(l<=r){
			res mid=(l+r)>>1;
			use=0;dfs(1,0,0,mid);
			if(use>=m) l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}
namespace task3{
	priority_queue<int,vector<int>,greater<int> > l;
	int a[N],sum[N];
	void MAIN(){
		for(res i=1;i<=tot;i+=2)
		  a[i]=e[i].w;
		sort(a+1,a+tot+1);
		for(res i=1;i<=m;i++)
		  l.push(sum[i]);
		for(res i=tot;i>=1;i--){
			res now=l.top();
			l.pop();
			now+=a[i];
			l.push(now);
		}
		printf("%d\n",l.top());
	}
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(res i=1;i<n;i++){
		res x=read(),y=read(),we=read();
		total+=we;
		if(y!=x+1) flag=0;
		if(x!=1) ifok=0;
		if(flag1==-1) flag1=we;
		if(flag1!=we) flag1=0;
		add(x,y,we);
		add(y,x,we);
	}
	if(m==1){
		task1::MAIN();
		return 0;
	}
	else if(flag){
		task2::MAIN();
		return 0;
	}
	else if(ifok){
		task3::MAIN();
		return 0;
	}
	else if(flag1){
		printf("%d\n",(n-1)/m*flag1);
		return 0;
	}
	else{
		printf("%d\n",(total-1)/m);
		return 0;
	}
}
/*
6 3
1 2 6
2 3 3
3 4 5
4 5 10
5 6 7
*/
/*
6 5
1 2 2
1 3 3
1 4 4
1 5 5
1 6 6
*/
