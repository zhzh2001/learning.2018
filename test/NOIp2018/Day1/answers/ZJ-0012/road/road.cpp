#include<bits/stdc++.h>
using namespace std;
const int N=1e5+10;

void read(int &x)
{
	char c=getchar();x=0;bool f=0;
	while(!isdigit(c))f|=(c=='-'),c=getchar();
	while(isdigit(c))x=x*10+c-48,c=getchar();
	if(f)x=-x;
}

int n,a[N],ans=0;

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for(int i=1;i<=n;i++)
		read(a[i]);
	int add;
	add=a[1],ans+=a[1];
	for(int i=2;i<=n;i++)
	{
		if(add>a[i])add=a[i];
		ans+=abs(add-a[i]);
		add=a[i];
	}
	printf("%d\n",ans);
	return 0;
}
