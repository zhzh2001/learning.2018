#include<bits/stdc++.h>
using namespace std;
const int N=120;
const int M=26000;

void read(int &x)
{
	char c=getchar();x=0;bool f=0;
	while(!isdigit(c))f|=(c=='-'),c=getchar();
	while(isdigit(c))x=x*10+c-48,c=getchar();
	if(f)x=-x;
}

int n,a[N];
bool vis[M];

int main()
{
	int T,ans;
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	while(T--)
	{
		read(n),ans=n;
		memset(vis,0,sizeof vis);
		for(int i=1;i<=n;i++)
			read(a[i]);
		sort(a+1,a+n+1),vis[0]=1;
		for(int i=1;i<=n;i++)
		{
			if(vis[a[i]])
			{
				--ans;
				continue;
			}
			vis[a[i]]=1;
			for(int j=a[i];j<M;j++)
				if(vis[j-a[i]])vis[j]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
