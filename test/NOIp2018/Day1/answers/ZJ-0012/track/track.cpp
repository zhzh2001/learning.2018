#include<bits/stdc++.h>
using namespace std;
const int N=5e4+10;
const int M=2e5+10;
const int inf=6e8;

void read(int &x)
{
	char c=getchar();x=0;bool f=0;
	while(!isdigit(c))f|=(c=='-'),c=getchar();
	while(isdigit(c))x=x*10+c-48,c=getchar();
	if(f)x=-x;
}

int n,m;
int hd[N],nxt[M],to[M],tot,w[M];
int W,can[N],las[N],tax[N],tnum;
bool used[N];
vector<int>son[N];

void add(int u,int v,int ww)
{
	nxt[++tot]=hd[u],to[tot]=v,w[tot]=ww,hd[u]=tot;
	nxt[++tot]=hd[v],to[tot]=u,w[tot]=ww,hd[v]=tot;
}
void dfs_init(int pos,int f)
{
	for(int i=hd[pos];i!=-1;i=nxt[i])
	{
		if(to[i]!=f)
		{
			son[pos].push_back(to[i]);
			dfs_init(to[i],pos);
		}
	}
}
void dfs(int pos,int f,int tofa)
{
	int nw,tp,bg,mx,ret;
	for(int i=hd[pos];i!=-1;i=nxt[i])
	{
		if(to[i]!=f)
		{
			dfs(to[i],pos,w[i]);
			can[pos]+=can[to[i]];
		}
	}
	tnum=0;
	for(int i=0;i<son[pos].size();i++)
		tax[++tnum]=las[son[pos][i]];
	sort(tax+1,tax+tnum+1);
	for(int i=1;i<=tnum;i++)
		used[i]=0;
	bg=1;
		for(int i=tnum;i>=1;i--)
		{
			if(tax[i]>=W){used[i]=1,++can[pos];continue;}
			while(bg<i&&(tax[i]+tax[bg])<W)++bg;
			if(bg>=i)break;
			else can[pos]++,used[bg]=used[i]=1,++bg;
		}
		las[pos]=0;
		for(int i=tnum;i>=1;i--)
		{
			if(!used[i])
			{
				las[pos]=tax[i];
				break;
			}
		}
		las[pos]+=tofa;
}
bool check(int x)
{
	W=x;
	memset(can,0,sizeof can);
	memset(las,0,sizeof las);
	dfs(1,0,0);
	if(can[1]>=m)return 1;
	return 0;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	srand(517517);
	int u,v,ww;
	read(n),read(m);
	memset(hd,-1,sizeof hd);
	tot=-1;
	for(int i=1;i<n;i++)
	{
		read(u),read(v),read(ww);
		add(u,v,ww);
	}
	dfs_init(1,0);
	int l=0,r=inf,mid,res=0;
	while(l<=r)
	{
		mid=(l+r)/2;
		if(check(mid))res=mid,l=mid+1;
		else r=mid-1;
	}
	if(l<inf&&l>res&&check(l))res=l;
	if(r<inf&&r>res&&check(r))res=r;
	printf("%d\n",res);
	return 0;
}
