#include <cstdio>
using namespace std;
int n,m,ans,a[100009],tp,tm;
inline int READ()
{
	int x=0;char ch;
	while (ch<'0' || ch>'9') ch=getchar();
	while (ch>='0' && ch<='9') {x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=READ();
	for (int i=1;i<=n;i++)
		a[i]=READ();
	a[0]=-1;a[n+1]=-1;
	for (int i=1;i<=n;i++)
		if (a[i]>=a[i-1] && a[i]>=a[i+1]) 
		{
			ans-=tp;
			tp=a[i];
			tm=0;
			ans+=a[i];
		}
		else if (a[i]<=a[i-1] && a[i]<=a[i+1]) 
		{
			ans+=tm;
			tm=a[i];
			tp=0;
			ans-=a[i];
		}
	printf("%d\n",ans);
	return 0;
}
