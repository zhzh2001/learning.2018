#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int n,m,sum=0,SL,SR,SMID,f[50009],g[50009],head[50009],a,b,c,totedge,st[50009],us[50009],len,L,R,MID;
bool ok,taken[50009];
struct EDGE{int to,next,len;}path[100009];
inline void add(int a,int b,int v)
{
	path[++totedge].next=head[a];
	path[totedge].len=v;
	path[totedge].to=b;
	head[a]=totedge;
}
inline void dfs(int,int,int);
inline bool check(int need)
{
	ok=false;
	dfs(1,0,need);
	if (ok) return true;
	return false;
}
inline int READ()
{
	char ch;int x=0;
	while (ch<'0' || ch>'9') ch=getchar();
	while (ch>='0' && ch<='9') {x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=READ();m=READ();
	SL=10009;
	for (int i=1;i<n;i++)
	{
		a=READ();b=READ();c=READ();
		add(a,b,c);
		add(b,a,c);
		SR+=c;
		if (c<SL) SL=c;
	}
	//check(15);
	//for (int i=1;i<=n;i++) printf("%d ",f[i]);printf("\n");
	//for (int i=1;i<=n;i++) printf("%d ",g[i]);printf("\n");
	//return 0;
	while (SL<SR)
	{
		SMID=(SL+SR+1)>>1;
		if (check(SMID)) SL=SMID; else SR=SMID-1;
	}
	printf("%d\n",SL);
	return 0;
}
inline void dfs(int cur,int fat,int need)
{
	f[cur]=g[cur]=0;
	for (int i=head[cur];i;i=path[i].next)
		if (path[i].to!=fat)
		{
			dfs(path[i].to,cur,need);
			if (ok) return;
		}
	us[0]=0;
	for (int i=head[cur];i;i=path[i].next)
		if (path[i].to!=fat)
		{
			if (g[path[i].to]) us[++us[0]]=g[path[i].to];
			f[cur]+=f[path[i].to];
		} else len=path[i].len;
	//us[++us[0]]=len;
	sort(us+1,us+us[0]+1);
	
	st[0]=0;
	for (int i=1;i<=us[0];i++)
	{
		st[++st[0]]=us[i];
		taken[i]=false;
	}
	for (int i=1;i<=st[0];i++)
	{
		if (taken[i]) continue;
		L=i+1;R=st[0];
		while (L<R)
		{
			MID=(L+R)>>1;
			if (st[MID]>=need-st[i]) R=MID; else L=MID+1;
		}
		for (L=L;L<=st[0];L++) if (!taken[L]) break;
		if (L>st[0] || st[L]<need-st[i]) continue;
		taken[L]=true;
		taken[i]=true;
		f[cur]++;
	}
	L=0;
	for (int i=st[0];i>=1;i--) if (!taken[i]) {L=st[i];break;}
	if (fat>0)
	{
		if (L+len>=need) f[cur]++; else g[cur]=L+len;
	}
	if (f[cur]>=m) {ok=true;return;}
}
