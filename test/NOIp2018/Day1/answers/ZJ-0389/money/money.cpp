#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int n,a[109],ans,MAX;
bool f[25009];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int Tt;
	scanf("%d",&Tt);
	while (Tt--)
	{
		ans=0;MAX=0;
		memset(f,0,sizeof(f));
		f[0]=true;
		scanf("%d",&n);
		for (int i=1;i<=n;i++)
		{
			scanf("%d",&a[i]);
			if (a[i]>MAX) MAX=a[i];
		}
		sort(a+1,a+n+1);
		for (int i=1;i<=n;i++)
			if (!f[a[i]])
			{
				ans++;
				for (int j=0;j<=MAX-a[i];j++)
					if (f[j]) f[j+a[i]]=true;
			}
		printf("%d\n",ans);
	}
	return 0;
}
