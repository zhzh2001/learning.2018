#include <set>
#include <cstdio>
typedef std::multiset<int>::iterator ITR;
const int maxn = 51234;
struct edge{ int v, val, nxt;
}e[maxn<<1]; int fir[maxn], num(1);
inline void add(int u, int v, int val){
	e[++num].v = v; e[num].val = val;
	e[num].nxt = fir[u]; fir[u] = num;
}
int n, m, du[maxn], tdu[maxn]; 
std::multiset<int> st[51234];
int q[maxn], leafs, fa[maxn], fval[maxn];
void getfa(int now, int fr){
	for(int itr(fir[now]); itr; itr = e[itr].nxt){
		if(e[itr].v == fr) continue;
		fa[e[itr].v] = now; fval[e[itr].v] = e[itr].val;
		getfa(e[itr].v, now);
	}
}
inline int max(int a, int b){
	return a > b ? a : b;
}
inline int solve(int mid){
	int l(1), r(leafs);
	for(int i(1); i <= n; ++i){
		tdu[i] = du[i];
	}
	int ret(0);
	while(l <= r){
		int now = q[l++], to = fa[now], val = fval[now], Maxvalleft(0);
		while(st[now].size() > 1){
			ITR itr = st[now].begin();
			int nowv = *itr; st[now].erase(itr);
			if(nowv >= mid){
				++ret; continue;
			}
			itr = st[now].lower_bound(mid - nowv);
			if(itr == st[now].end()){
				Maxvalleft = max(Maxvalleft, nowv);
				continue;
			}
			++ret; st[now].erase(itr);
		}
		while(st[now].size()){
			ITR itr = st[now].begin();
			int nowv = *itr; st[now].erase(itr);
			Maxvalleft = max(Maxvalleft, nowv);
		}
		if(Maxvalleft + val >= mid){
			++ret;
		}
		else{
			st[to].insert(Maxvalleft + val);
		}
		--tdu[to]; if(tdu[to] == 1) q[++r] = to;
		st[now].clear();
	}
	return ret;
}
int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	int l(1), r(0), ans(1);
	for(int i(1), u, v, val; i < n; ++i){
		scanf("%d%d%d", &u, &v, &val);
		add(u, v, val); add(v, u, val);
		++du[u]; ++du[v]; r += val;
	}
	++du[1];
	for(int i(2); i <= n; ++i){
		if(du[i] == 1) q[++leafs] = i;
	}
	getfa(1, -1);
	while(l <= r){
		int mid((l + r) >> 1);
		if(solve(mid) >= m){
			l = mid + 1;
			ans = mid;
		}
		else{
			r = mid - 1;
		}
	}
	printf("%d\n", ans);
	return 0;
}

