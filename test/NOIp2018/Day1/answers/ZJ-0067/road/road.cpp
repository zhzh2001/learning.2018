#include <cstdio>
#include <vector>
inline int min(int a, int b){
	return a < b ? a : b;
}
const int maxv = 112345;
const int maxn = 112345;
int d[maxn], st[20][maxn], t[maxn];
std::vector<int> v[maxv];
inline int query(int l, int r){
	int len = r - l + 1;
	return min(st[t[len]][l], st[t[len]][r-(1<<t[len])+1]);
}
inline int binary_search(int p, int val){
	int l(0), r(v[val].size() - 1), ret(0);
	while(l <= r){
		int mid((l + r) >> 1);
		if(v[val][mid] >= p){
			r = mid - 1;
			ret = mid;
		}
		else{
			l = mid + 1;
		}
	}
	return ret;
}// find the first one >= p
int solve(int l, int r, int hd){
	if(r < l) return 0;
	if(l == r) return d[l] - hd;
	int Minval = query(l, r); // first put it all
	int ret(Minval - hd); hd = Minval;
	int p = binary_search(l, Minval);//binary search for the first Minval in the range
	int las = l-1, i(p);
	for(; i < v[Minval].size(); ++i){
		if(v[Minval][i] > r) break;
		ret += solve(las + 1, v[Minval][i]-1, hd);
		las = v[Minval][i];
	}
	ret += solve(las + 1, r, hd);
	return ret;
}
int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	for(int i(2); i < maxn; ++i) t[i] = t[i>>1] + 1;
	int n; scanf("%d", &n);
	for(int i(1); i <= n; ++i){
		scanf("%d", &d[i]);
		st[0][i] = d[i];
		v[d[i]].push_back(i);
	}
	for(int i(1); i <= 20; ++i){
		for(int j(1); j + (1<<(i-1)) <= n; ++j){
			st[i][j] = min(st[i-1][j], st[i-1][j+(1<<(i-1))]);
		}
	}
	printf("%d\n", solve(1, n, 0));
	return 0;
}

