#include <cstdio>
#include <cstring>
#include <algorithm>
int a[112];
bool dp[25123];
int solve(){
	int n; scanf("%d", &n);
	for(int i(1); i <= n; ++i){
		scanf("%d", &a[i]);
	}
	std::sort(a + 1, a + n + 1);
	memset(dp, 0, sizeof(dp));
	dp[0] = 1; int ret(0);
	for(int i(1); i <= n; ++i){
		if(dp[a[i]]) continue;
		++ret;
		for(int j(a[i]); j <= a[n]; ++j){
			dp[j] |= dp[j-a[i]];
		}
	}
	return ret;
}
int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T; scanf("%d", &T);
	while(T--){
		printf("%d\n", solve());
	}
	return 0;
}

