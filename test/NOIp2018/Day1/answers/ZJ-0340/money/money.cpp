#include<bits/stdc++.h>
using namespace std;
const int N =109;
const int M = 25009;
int Read()
{
	int ret=0,nag=1;char c=getchar();
	while(c<'0'||c>'9') {if(c=='-') nag=-1; c=getchar();}
	while(c<='9'&&c>='0') ret=(ret<<3)+(ret<<1)+c-'0',c=getchar();
	return ret*nag;
}
void Print(int x)
{
	if(x<0) putchar('-'),x=-x;
	if(x>9) Print(x/10);
	putchar(x%10+'0');
}
int T,n,a[N],f[M],Ok[N],ans;
bool jud(int k)
{
	register int V=a[k];
	for(int i=0;i<=V;i++) f[i]=0;f[0]=1;
	for(register int i=1;i<=n;i++)
	{
		if((!Ok[i]) && i!=k)
		{
			for(register int j=0;j<=V-a[i];j++)
				if(f[j]) 
				{
					f[j+a[i]]=1;
					if(j+a[i]==a[k]) return 1;
				}
		}
	}
	return 0;
}
int main ()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=Read();
	while(T--)
	{
		ans=0;memset(Ok,0,sizeof(Ok));;
		n=Read();for(int i=1;i<=n;i++) a[i]=Read();
		for(int i=1;i<=n;i++)
			if(jud(i)) Ok[i]=1,ans++;
		Print(n-ans),puts("");
	}
	return 0;
}
/*
2 
4
3 19 10 6
5
11 29 13 19 17
*/
