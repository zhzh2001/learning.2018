#include<bits/stdc++.h>
using namespace std;
#define ll long long
ll Read()
{
	ll ret=0,nag=1;char c=getchar();
	while(c<'0'||c>'9') {if(c=='-') nag=-1; c=getchar();}
	while(c<='9'&&c>='0') ret=(ret<<3)+(ret<<1)+c-'0',c=getchar();
	return ret*nag;
}
void Print(ll x)
{
	if(x<0) putchar('-'),x=-x;
	if(x>9) Print(x/10);
	putchar(x%10+'0');
}
const ll N  = 100009;
ll n,a[N],drt[N],ans;
int main ()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=Read();
	for(ll i=1;i<=n;i++) a[i]=Read();
	for(ll i=1;i<=n;i++) drt[i]=a[i]-a[i-1];
	for(ll i=1;i<=n;i++)	if(drt[i]>0) ans+=drt[i];
	Print(ans);
	return 0;
}
/*
6
4 3 2 5 3 5
*/
