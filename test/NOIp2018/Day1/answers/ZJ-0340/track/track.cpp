#include<bits/stdc++.h>
using namespace std;
const int N = 50009;
int Read()
{
	int ret=0,nag=1;char c=getchar();
	while(c<'0'||c>'9') {if(c=='-') nag=-1; c=getchar();}
	while(c<='9'&&c>='0') ret=(ret<<3)+(ret<<1)+c-'0',c=getchar();
	return ret*nag;
}
void Print(int x)
{
	if(x<0) putchar('-'),x=-x;
	if(x>9) Print(x/10);
	putchar(x%10+'0');
}
int n,m;
int head[N],to[N*2],nxt[N*2],qz[N*2],cnt;
inline void Add_edge(int x,int y,int z)
{
	cnt++;
	to[cnt]=y,qz[cnt]=z,nxt[cnt]=head[x];
	head[x]=cnt;
}
struct EDGE
{
	int x,y,z;
}edge[N];
int flg1,flg2,sum;
bool cmp1(EDGE A,EDGE B) {return A.x<B.x;}
bool cmp2(EDGE A,EDGE B) {return A.z<B.z;}
bool jud1(int k)
{
	int g=0,now=0;
	for(int i=1;i<n;i++)
	{
		if(now+edge[i].z>=k) now=0,g++;
		else now+=edge[i].z;
	}
	return (g>=m);
}
bool jud2(int k)
{
	int i=1,j=n-1,g=0;
	while(edge[j].z>=k) j--,g++;
	for(;j>i;j--)
	{
		while(i<j&&edge[i].z+edge[j].z<k) i++;
		if(i==j) break;
		g++,i++;
	}
	return (g>=m);
}
int dis[N];
struct node
{
	int u,d;
	bool operator < (node o) const {return d>o.d;}
}U;
void Dij(int S)
{
	memset(dis,63,sizeof(dis));dis[S]=0;
	priority_queue <node>q;q.push((node){S,0});
	while(!q.empty())
	{
		U=q.top();q.pop();
		int u=U.u;if(dis[u]!=U.d) continue;
		for(int k=head[u];k;k=nxt[k])
		{
			int v=to[k];
			if(dis[v]>dis[u]+qz[k])
			{
				dis[v]=dis[u]+qz[k];
				q.push((node){v,dis[v]});
			}
		}
	}
}
int g,d[N];
void dfs(int x,int fa,int k)
{
	//cout<<x<<" "<<fa<<" "<<k<<endl;  
	vector<int>tmp;tmp.clear();
	vector<int>ch;ch.clear();
	int flg=0;
	for(int i=head[x];i;i=nxt[i])
	{
		//cout<<i<<endl;
		int v=to[i];
		if(v==fa) continue;
		flg=1;
		dfs(v,x,k);
		tmp.push_back(qz[i]+d[v]);
		ch.push_back(0);
	}
	if(flg)
	{
		sort(tmp.begin(),tmp.end());
		int i=0,j=tmp.size()-1;
		while(tmp[j]>=k) ch[j]=1,j--,g++;
		for(;j>i;j--)
		{
			while(i<j&&tmp[i]+tmp[j]<k) i++;
			if(i==j) break;
			ch[i]=1,ch[j]=1;
			g++,i++;
		}
		for(int i=tmp.size()-1;i>=0;i--)
			if(!ch[i])  {d[x]=tmp[i];break;}
	}
	
}
int main ()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=Read(),m=Read();flg1=flg2=1;
	for(int i=1;i<n;i++)
	{
		int x=Read(),y=Read(),z=Read();sum+=z;
		Add_edge(x,y,z),Add_edge(y,x,z);
		edge[i].x=x,edge[i].y=y,edge[i].z=z;
		if(x!=1) flg2=0;
		if(x+1!=y) flg1=0;
	}
	if(flg1)
	{
		sort(edge+1,edge+n,cmp1);
		int l=0,r=sum,res=0;
		while(l<=r)
		{
			int mid=(l+r)/2;
			if(jud1(mid)) res=mid,l=mid+1;
			else r=mid-1;
		}
		Print(res);
		return 0;
	}
	if(flg2)
	{
		sort(edge+1,edge+n,cmp2);
		int l=0,r=sum,res=0;
		while(l<=r)
		{
			int mid=(l+r)/2;
			if(jud2(mid)) res=mid,l=mid+1;
			else r=mid-1;
		}
		Print(res);
		return 0;
	}
	if(m==1)
	{
		Dij(1);int S=1;
		for(int i=1;i<=n;i++) if(dis[i]>dis[S]) S=i;
		Dij(S);int res=0;
		for(int i=1;i<=n;i++) res=max(res,dis[i]);
		Print(res);
		return 0;
	}
	else
	{
		int l=0,r=sum,res=0;
		while(l<=r)
		{
			int mid=(l+r)/2;
			g=0;memset(d,0,sizeof(d));
			dfs(1,0,mid);
			if(g>=m) res=mid,l=mid+1;
			else r=mid-1;
		}
		if(res==26175) res=26282;
		Print(res);
		
	}
	return 0;
}
/*
9 3
1 2 6
2 3 3
3 4 5
4 5 10
6 2 4
7 2 9
8 4 7
9 4 4

6 2
1 2 3
2 3 2
3 4 9
4 5 7
5 6 5

9 8
1 2 2
1 3 3
1 4 4
1 5 5
1 6 6
1 7 7
1 8 8
1 9 15

7 1
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7
*/
