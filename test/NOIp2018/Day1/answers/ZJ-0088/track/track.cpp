#include<bits/stdc++.h>
using namespace std;
const int N=50000+5;
int n,m,d[N],p,l,r,rd[N],st;
int head[N],ver[N*2],Next[N*2],edge[N*2],tot;
bool lian=true,danceng=true,v[N];
queue<int>q;
int Read(){
	int s=0,w=1;char c;
	c=getchar();
	while(c<'0'||c>'9'){if(c=='-')w=-1;c=getchar();}
	while(c>='0'&&c<='9'){s=s*10+c-'0';c=getchar();}
	return s*w;
}
void add(int x,int y,int z){
	tot++;ver[tot]=y;edge[tot]=z;Next[tot]=head[x];head[x]=tot;
}
int bfs(int u){
	int i,x,y;
	memset(d,0x3f,sizeof(d));
	d[u]=0;q.push(u);
	while(q.size()){
		int x=q.front();q.pop();
		for(int i=head[x];i;i=Next[i]){
			int y=ver[i],z=edge[i];
			if(d[y]==0x3f3f3f3f)d[y]=d[x]+z,q.push(y);
		}
	}
	for(x=y=1;x<=n;x++)if(d[x]>d[y])y=x;
	return y; 
}
int get(){
	p=bfs(1);p=bfs(p);return d[p];
}
bool cmp(int x,int y){return x>y;}
void lc(){
	sort(edge+1,edge+tot+1,cmp);
	int ans=0x3f3f3f3f;
	if(2*m<=n-1){
		for(int i=1;i<=2*m;i+=2)
			ans=min(ans,edge[i]+edge[4*m-i]);
		printf("%d\n",ans);
	}
	else{
		int t=2*m-(n-1);
		for(int i=1;i<=2*t;i+=2)
			ans=min(ans,edge[i]);
		for(int i=2*t+1;i<=((2*t+tot)>>1);i+=2)
			ans=min(ans,edge[i]+edge[2*t+tot-i]);
		printf("%d\n",ans);
	}
}
bool check(int mid){
	int k=0,sum=0; 
	memset(v,0,sizeof(v));
	while(q.size())q.pop();
	q.push(st);v[st]=1;
	while(q.size()){
		int x=q.front();q.pop();
		for(int i=head[x];i;i=Next[i]){
			int y=ver[i];
			if(v[y])continue;
			v[y]=1;
			sum+=edge[i];
			if(sum>=mid)sum=0,k++;
			q.push(y);
		}
	}
	if(sum>=mid)k++;
	if(k>=m)return true;
	return false;
}
void liann(){
	for(st=1;st<=n;st++)if(rd[st]==1)break;
	int l=0,r=get();
	while(l<=r){
		int mid=(l+r)>>1;
		if(check(mid))l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",r);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=Read();m=Read();
	for(int i=1;i<n;i++){
		int a,b,l;a=Read(),b=Read(),l=Read();
		add(a,b,l);add(b,a,l);rd[a]++,rd[b]++;
		if(a!=1)danceng=false;if(b!=a+1)lian=false;
	}
	if(m==1){
		r=get();printf("%d\n",r);
	}
	else if(m==n-1){
		int minn=0x3f3f3f3f;
		for(int i=1;i<=tot;i+=2)minn=min(minn,edge[i]);
		printf("%d\n",minn);
	}
	else if(danceng){
		lc();
	}
	else if(lian){
		liann();
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
