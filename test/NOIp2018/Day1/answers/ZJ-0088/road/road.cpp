#include<bits/stdc++.h>
using namespace std;
int n,a[100000+5],tot=0,ans=0;
void merge(int l,int r){
	int minn=0x3f3f3f3f;
	for(int i=l;i<=r;i++)minn=min(minn,a[i]);
	for(int i=l;i<=r;i++)a[i]-=minn;
	ans+=minn;
	int ll,rr;
	for(ll=l;ll<=r;ll++)if(a[ll])break;
	for(int i=l+1;i<=r;i++){
		if(!a[i]&&a[i-1])rr=i-1,merge(ll,rr);
		else if(!a[i-1]&&a[i])ll=i;
	}
	if(a[r])merge(ll,r);
	return ;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	int l,r;
	for(l=1;l<=n;l++)if(a[l])break;
	for(int i=1;i<=n;i++){
		if(!a[i]&&a[i-1])r=i-1,merge(l,r);
		if(!a[i-1]&&a[i])l=i;
	}
	if(a[n])merge(l,n);
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
