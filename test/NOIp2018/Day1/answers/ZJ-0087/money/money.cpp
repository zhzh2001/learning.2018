#include<bits/stdc++.h>
using namespace std;
const int MAXN=105,MAXM=25005;
int a[MAXN];
bool f[MAXM];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	for (int t=1;t<=T;t++)
	{
		memset(f,false,sizeof(f));
		int n,M=0;
		scanf("%d",&n);
		for (int i=1;i<=n;i++)	
		{
			scanf("%d",&a[i]);
			M=max(M,a[i]);
		}
		sort(a+1,a+1+n);
		if (a[1]==1) 
		{
			printf("%d\n",1);
			continue;
		}
		int tot=1; 
		for (int i=1;i<=M/a[1];i++) f[i*a[1]]=true;
		for (int i=2;i<=n;i++)
			if (!f[a[i]])
			{
				for (int j=1;j<=M/a[i];j++)
					f[j*a[i]]=true;
				tot++;
				f[a[i]]=true;
				for (int j=a[i]+1;j<=M;j++)
				if (!f[j])
				{
					for (int k=1;k<=j/a[i];k++)
					if (f[j-k*a[i]]) f[j]=true;
				}
			}
		printf("%d\n",tot);
	}
	return 0;
}
