#include<bits/stdc++.h>
using namespace std;
const int MAXN=100005,MAXD=10005;
int a[MAXN],ans,n,zx;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	int tot=0;
	for (int i=1;i<=n;i++) 
	{
		scanf("%d",&a[i]);
		if (a[i]==0) tot++;
	}
	a[0]=a[n+1]=0;
	while (tot<n)
	{
		int l=-1;
		for (int i=0;i<=n+1;i++)
		{
			if (a[i]==0)
			{
				if (l!=-1)
				{
					ans+=zx;
					for (int j=l;j<i;j++) 
					{
						a[j]=a[j]-zx;
						if (a[j]==0) tot++;
					}
				}
				while (a[i]==0) i++;
				l=i;
				zx=min(MAXD,a[i]);
			}
			else zx=min(zx,a[i]);
		}
	}
	printf("%d\n",ans);
	return 0;
}