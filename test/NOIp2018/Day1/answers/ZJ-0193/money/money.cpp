#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=105,M=30005;
inline void rd(int &x){
	x=0;static char c;
	while(c=getchar(),c<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),c>=48);
}
int n;
int a[N],dp[M];
int main(){
//	printf("%lf\n",sizeof()/1024.0/1024);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		memset(dp,-1,sizeof(dp));
		dp[0]=1;
		int ans=n;
		for(int i=1;i<=n;i++){
			if(dp[a[i]]!=-1){ans--;continue;}
			for(int j=0;j<=25000;j++)
				if(dp[j]!=-1)dp[j+a[i]]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
