#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=50005;
inline void rd(int &x){
	x=0;static char c;
	while(c=getchar(),c<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),c>=48);
}
int n,m;
struct Path{
	int x,y,z;
	bool operator<(const Path &A)const{return z<A.z;}
}A[N];
bool cmp(Path a,Path b){
	return a.x<b.x;
}
struct Graph{
	int tot,to[N<<1],nxt[N<<1],len[N<<1],head[N];
	void add(int x,int y,int z){tot++;to[tot]=y;len[tot]=z;nxt[tot]=head[x];head[x]=tot;}
	void clear(){memset(head,-1,sizeof(head));tot=0;}
}G;
struct P20{
	int mx,p;
	void dfs(int x,int f,int d){
		if(mx<d)mx=d,p=x;
		for(int i=G.head[x];i!=-1;i=G.nxt[i]){
			int v=G.to[i];
			if(v==f)continue;
			dfs(v,x,d+G.len[i]);
		}
	}
	void solve(){
		mx=0,dfs(1,0,0);
		mx=0,dfs(p,0,0);
		printf("%d\n",mx);
	}
}P_20;
struct Pl{
	int dis[N];
	bool check(int x){
		int lst=0,cnt=0;
		for(int i=1;i<=n;i++)
			if(dis[i]-dis[lst]>=x)lst=i,cnt++;
		return cnt>=m;
	}
	void solve(){
		dis[0]=0;
		sort(A+1,A+n,cmp);
		for(int i=1;i<n;i++)
			dis[i+1]=dis[i]+A[i].z;
		int l=0,r=dis[n]/m+1,ans=-1;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}P_l;
struct Pjuhua{
	int sum;
	multiset<int>S,tmp;
	multiset<int>::iterator it,it2;
	bool check(int x){
//		printf("checking:%d\n",x);
		tmp=S;
		it=tmp.end();it--;
		int cnt=0;
		while(cnt<m){
			if(x>(*it)){
				it2=tmp.lower_bound(x-(*it));
				if(it2==tmp.end()||it2==it)return false;
				tmp.erase(it2);
			}
			tmp.erase(it--);
			cnt++;
		}
		return true;
	}
	void solve(){
		sum=0;
		for(int i=1;i<n;i++){
			S.insert(A[i].z);
			sum+=A[i].z;
		}
		int l=0,r=sum/m+1,ans=-1;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}P_juhua;
int main(){
//	printf("%lf\n",(&mem2-&mem1)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	G.clear();
	rd(n),rd(m);
	int mn=2e9,l_ok=1,j_ok=1;
	for(int i=1;i<n;i++){
		rd(A[i].x),rd(A[i].y),rd(A[i].z);
		if(A[i].y!=A[i].x+1)l_ok=0;
		if(A[i].x!=1)j_ok=0;
		G.add(A[i].x,A[i].y,A[i].z);
		G.add(A[i].y,A[i].x,A[i].z);
		mn=min(mn,A[i].z);
	}
	if(m==1)P_20.solve();
	else if(m==n-1)printf("%d\n",mn);
	else if(j_ok)P_juhua.solve();
	else if(l_ok)P_l.solve();
	return 0;
}
