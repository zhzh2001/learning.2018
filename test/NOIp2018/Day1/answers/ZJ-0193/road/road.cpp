#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=100005;
int n;
int a[N],dp[N];
int main(){
//	printf("%lf\n",sizeof()/1024.0/1024);
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	dp[1]=a[1];
	for(int i=2;i<=n;i++){
		if(a[i]<a[i-1])dp[i]=dp[i-1];
		else dp[i]=dp[i-1]+(a[i]-a[i-1]);
	}
	printf("%d\n",dp[n]);
	return 0;
}
