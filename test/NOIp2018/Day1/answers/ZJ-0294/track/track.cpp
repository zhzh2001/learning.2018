#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int s[50],n,m,i,a,b,c,k,ans,li,ri,ai[50005],ci[50005],zi[50005],h[100005],t[100005],l[100005];
bool bb[50005],bi,bbi;
int sri(){
	char c=getchar();int x=0;
	while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
	return x;
}
void sc(int x){int l=0;
	if(x==0)s[l=1]=0;
	while(x)s[++l]=x%10,x/=10;
	for(;l>0;l--)putchar(s[l]+'0');putchar('\n');
}
int ff(int x){int d,li=0,u=0,y=0,z;
	for(int i=zi[x];i!=-1;i=h[i]){
		d=t[i];
		if(bb[d]==0){
			bb[d]=1;
			z=l[i]+ff(d);
			if(z>u){
				y=u;u=z;
			}else{
				if(z>y)y=z;
			}
		}
	}
	if(u+y>ans)ans=u+y;
	return u;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=sri();m=sri();
	if(m==1){
		memset(zi,-1,sizeof(zi));
		memset(bb,0,sizeof(bb));k=0;
		for(i=1;i<n;i++){
			a=sri();b=sri();c=sri();
			h[++k]=zi[a];zi[a]=k;t[k]=b;l[k]=c;
			h[++k]=zi[b];zi[b]=k;t[k]=a;l[k]=c;
		}ans=0;bb[1]=1;
		ff(1);
		sc(ans);
	}else{
		memset(zi,-1,sizeof(zi));
		k=0;bi=bbi=1;
		for(i=1;i<n;i++){
			a=sri();b=sri();c=sri();
			if(b<a)li=a,a=b,b=li;
			ci[i]=c;ai[a]=c;
			if(a>1)bi=0;if((b-a)!=1)bbi=0;
			h[++k]=zi[a];zi[a]=k;t[k]=b;l[k]=c;
			h[++k]=zi[b];zi[b]=k;t[k]=a;l[k]=c;
		}ans=0;
		if(bi==1){
			sort(ci+1,ci+n);
			a=ci[1];b=ci[n-1]+ci[n-2];
			while(a<b){
				c=((a+b)>>1)+1;k=0;
				li=1;ri=n-1;
				while(li<ri&&k<m){
					if(ci[ri]>=c)k++,ri--;else{
						while(li<ri&&ci[li]+ci[ri]<c)li++;
						if(li<ri&&ci[li]+ci[ri]>=c)k++,li++,ri--;else break;
					}
				}
				if(k>=m)a=c;else b=c-1;
			}
			sc(a);
		}else{
			if(bbi==1){a=ai[1];b=0;
				for(i=1;i<n;i++)b+=ai[i],a=ai[i]<a?ai[i]:a;
				while(a<b){
					c=((a+b)>>1)+1;k=0;li=0;
					for(i=1;i<=n;i++){
						li+=ai[i];if(li>=c)li=0,k++;
						if(k==m)break;
					}
					if(k>=m)a=c;else b=c-1;
				}
				sc(a);
			}
		}		
	}
	return 0;
}
