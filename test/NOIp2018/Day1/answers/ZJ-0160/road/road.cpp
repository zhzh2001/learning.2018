#include <bits/stdc++.h>
using namespace std;

#define rep(i,a,n) for (int i=(a);i<(n);i++)
#define per(i,a,n) for (int i=(a)-1;i>=(n);i--)
#define SZ(x) ((int)x.size())
#define all(x) x.begin(),x.end()
#define pb push_back
#define mp make_pair
#define fi first
#define se second

typedef long long ll;
typedef double db;
typedef pair<int,int> PII;
typedef vector<int> VI;

bool __;
#define N 100005
int n,ans,A[N];

namespace P_2 {
	int ST[N][20];
	int Log[N];
	
	int query_pos(int l,int r) {
		int k=Log[r-l+1],t=r-(1<<k)+1;
		if (A[ST[l][k]]<A[ST[t][k]]) return ST[l][k];
		else return ST[t][k];
	}
	
	void solve(int l,int r,int del) {
		if (l>r) return;
		int pos=query_pos(l,r);
		//printf("l=%d r=%d pos=%d del=%d ans=%d\n",l,r,pos,del,ans);
		ans+=A[pos]-del;
		solve(l,pos-1,A[pos]);
		solve(pos+1,r,A[pos]);
	}
	
	void solve() {
		rep(i,1,n+1) ST[i][0]=i;
		rep(j,1,20) rep(i,1,n+1) {
			if (i+(1<<j)-1>n) break;
			int k=i+(1<<(j-1));
			if (A[ST[i][j-1]]<A[ST[k][j-1]]) ST[i][j]=ST[i][j-1];
			else ST[i][j]=ST[k][j-1];
		}
		Log[0]=-1;
		rep(i,1,N) Log[i]=Log[i>>1]+1;
		solve(1,n,0);
		printf("%d\n",ans);
	}
}

bool ___;
int main() {
	//printf("%.lf MB\n",(&___-&__)/1024.0/1024.0);
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	rep(i,1,n+1) scanf("%d",&A[i]);
	P_2::solve();
	return 0;	
}
