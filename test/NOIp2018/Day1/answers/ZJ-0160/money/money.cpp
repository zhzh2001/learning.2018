#include <bits/stdc++.h>
using namespace std;

#define rep(i,a,n) for (int i=(a);i<(n);i++)
#define per(i,a,n) for (int i=(a)-1;i>=(n);i--)
#define SZ(x) ((int)x.size())
#define all(x) x.begin(),x.end()
#define pb push_back
#define mp make_pair
#define fi first
#define se second

typedef long long ll;
typedef double db;
typedef pair<int,int> PII;
typedef vector<int> VI;

#define V 25005
#define N 105
int _,n,A[N],ans;
bool vis[V];

int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (scanf("%d",&_);_;_--) {
		ans=0;
		memset(vis,0,sizeof(vis));
		scanf("%d",&n);
		rep(i,1,n+1) scanf("%d",&A[i]);
		sort(A+1,A+1+n);
		rep(i,1,n+1) {
			if (!vis[A[i]]) {
				vis[A[i]]=1;
				ans++;
				rep(j,A[i],V) vis[j]|=(vis[j-A[i]]);
			}
		}
		printf("%d\n",ans);
	}
	return 0;	
}
