#include <bits/stdc++.h>
using namespace std;

#define rep(i,a,n) for (int i=(a);i<(n);i++)
#define per(i,a,n) for (int i=(a)-1;i>=(n);i--)
#define SZ(x) ((int)x.size())
#define all(x) x.begin(),x.end()
#define pb push_back
#define mp make_pair
#define fi first
#define se second

typedef long long ll;
typedef double db;
typedef pair<int,int> PII;
typedef vector<int> VI;

bool __;
#define N 50005
#define inf 0x3f3f3f3f
int n,m,cnt,len;
int head[N],nxt[N<<1],to[N<<1],cost[N<<1],tol;
int dp[N],que[N],R;

void add_edge(int u,int v,int w) {
	nxt[++tol]=head[u]; head[u]=tol; to[tol]=v; cost[tol]=w;	
}

void dfs(int u,int f,int mid) {
	dp[u]=0;
	for (int i=head[u];i;i=nxt[i]) {
		int v=to[i];
		if (v==f) continue;
		dfs(v,u,mid);
	}
	R=0;
	for (int i=head[u];i;i=nxt[i]) {
		int v=to[i],w=cost[i];
		if (v==f) continue;
		que[R++]=dp[v]+w;
	}
	sort(que,que+R);
	multiset<int> st;
	multiset<int>::iterator it1,it2;
	int r=R-1;
	while (r>=0&&que[r]>=mid) cnt++,r--;
	rep(i,0,r+1) st.insert(que[i]);
	rep(i,0,r+1) {
		it1=st.find(que[i]);
		if (it1==st.end()) continue;
		it2=st.lower_bound(mid-que[i]);
		if (it1==it2) it2++;
		if (it2!=st.end()) {
			st.erase(it1);
			st.erase(it2);
			cnt++;
		}
	}	
	dp[u]=0;
	if (st.size()>0) dp[u]=*st.rbegin();
}
bool check(int mid) {
	cnt=0;
	dfs(1,0,mid);
	if (cnt>=m) return 1;
	else return 0;
}

bool ___;
int main() {
	//printf("%.lf MB\n",(&___-&__)/1024.0/1024.0);
	//freopen("data.in","r",stdin);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	rep(i,1,n) {
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		len+=w;
		add_edge(u,v,w);
		add_edge(v,u,w);
	}
	//printf("%d\n",check(31));
	int l=0,r=(len+m-1)/m,res=0;
	while (l<=r) {
		int mid=(l+r)>>1; 
		if (check(mid)) res=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",res);
	return 0;	
}
