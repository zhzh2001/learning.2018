#include <bits/stdc++.h>
using namespace std;
#define rep(i,j,k) for(i=j;i<=k;++i)
#define dow(i,j,k) for(i=j;i>=k;--i)
const int N=1e2+10;
const int M=25000+10;
int T,n,a[N],f[M];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);int i,j,k;
		rep(i,1,n)scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		memset(f,0,sizeof(f));f[0]=1;
		rep(k,1,n){
			rep(i,1,(k-1))
				rep(j,a[i],a[k])f[j]=f[j-a[i]]|f[j];
				if(f[a[k]]){
					a[k]=0;
				}
		}
		int ans=0;
		rep(i,1,n)if(a[i])++ans;
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
