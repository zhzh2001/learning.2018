#include <bits/stdc++.h>
using namespace std;
#define rep(i,j,k) for(i=j;i<=k;++i)
#define dow(i,j,k) for(i=j;i>=k;--i)
const int N=1e5+10;
int n,a[N],c[N],ans=0;
int main(){freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	scanf("%d",&n);int i;
	rep(i,1,n)scanf("%d",&a[i]);
	rep(i,1,(n+1)){
		if(a[i]>a[i+1])ans+=a[i]-a[i+1];
	}
	printf("%d",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
