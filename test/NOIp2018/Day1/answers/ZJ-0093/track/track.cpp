#include <bits/stdc++.h>
using namespace std;
const int N=5e4+10;
#define rep(i,j,k) for(i=j;i<=k;++i)
#define dow(i,j,k) for(i=j;i>=k;--i)
struct edge{
	int nxt,to,w;
}e[N<<1];
int head[N],pos,n,m,dp[N],anc[N][17],dep[N],seg[N<<2],f[N],sum[N],a[N];
void pushup(int rt){seg[rt]=max(seg[rt<<1],seg[rt<<1|1]);}
void motify(int rt,int l,int r,int k,int d){
	if(l==r){seg[rt]=d;return;}
	int mid=l+r>>1;
	if(mid>=k)motify(rt<<1,l,mid,k,d);
	else motify(rt<<1|1,mid+1,r,k,d);
	pushup(rt);
}
int query(int rt,int l,int r,int ql,int qr){
	if(ql<=l && r<=qr)return seg[rt];
	int res=0,mid=l+r>>1;
	if(mid>=ql)res=max(res,query(rt<<1,l,mid,ql,qr));
	if(mid+1<=qr)res=max(res,query(rt<<1|1,mid+1,r,ql,qr));
	return res;
}
void add(int u,int v,int len){
	e[++pos]=(edge){head[u],v,len};head[u]=pos;
}
int dfs(int u,int fa){
	int i;dep[u]=dep[fa]+1;
	for(i=head[u];i;i=e[i].nxt){
		int v=e[i].to;
		if(v==fa)continue;dp[v]=dp[u]+e[i].w;
		dfs(v,u);anc[v][0]=u;
	}
}
void s1(){
	dfs(1,0);int i,r;
	r=1;
	rep(i,2,n)if(dp[i]>dp[r])r=i;
	int ans=dp[r];memset(dp,0,sizeof(dp));
	dfs(r,0);
	r=1;
	rep(i,2,n)if(dp[i]>dp[r])r=i;
	ans=dp[r];
	printf("%d",ans);
}
void ini(){
	int i,j;
	rep(j,1,16)rep(i,1,n)anc[i][j]=anc[anc[i][j-1]][j-1];
}
int lca(int u,int v){
	if(dep[u]<dep[v])swap(u,v);
	int i;
	dow(i,16,0)
		if(dep[anc[u][i]]>=dep[v])u=anc[u][i];
	if(u==v)return u;
	dow(i,16,0)if(anc[u][i]!=anc[v][i])u=anc[u][i],v=anc[v][i];
	return anc[u][0];
}
int ck(int k){
	memset(f,0,sizeof(f));int i,ret,lst,mx,j;lst=mx=0;
	rep(i,1,n)if(sum[i]>=k)break;
	rep(i,i,n){
		int l=0,r=i-1;
		while(l<r-1){
			int mid=l+r>>1;
			if(sum[i]-sum[mid]>=k)l=mid;else r=mid-1;
		}
		if(sum[i]-sum[r]>=k)ret=r+1;else ret=l+1;
		rep(j,lst,(ret-1))mx=max(mx,f[j]);lst=ret;
		f[i]=mx+1;
		if(f[i]>=m)return 1;
	}
	return 0;
}
void s2(){
	int i;
	rep(i,1,n)sum[i]=sum[i-1]+a[i];
	int l=0,r=sum[n];
	while(l<r-1){
		int mid=l+r>>1;
		if(ck(mid))l=mid;else r=mid-1;
	}
	if(ck(r))printf("%d",r);else printf("%d",l);
}
int main(){
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);int i;
	rep(i,1,(n-1)){
		int x,y,z;scanf("%d%d%d",&x,&y,&z);add(x,y,z);add(y,x,z);
		if(x==y+1)a[y]=z;if(y==x+1)a[x]=z;
	}
	if(m==1)s1();
	else s2();
	fclose(stdin);fclose(stdout);
	return 0;
}
