#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <algorithm> 
#include <queue>
#include <vector>
#include <cstdio>
using namespace std;
int n,m,total=0,next[50002],head[50002],point[50002],weight[50002],INF=-1,b[50002];
queue<int> ss;

int spfa(int from)
{
	int dis[n+2],max1=-1;
	bool used[n+2];
	memset(used,false,sizeof(used));
	ss.push(from);
	used[from]=true;
	for(int i=1;i<=n;i++)
	{
		if(i==from) dis[i]=0;
		else dis[i]=INF;
	 } 
	 while(!ss.empty() )
	 {
	 	int now=ss.front() ;
	 	ss.pop() ;
	 	int k=head[now];
	 	while((k!=0))
	 	{
	 		if(used[point[k]])
	 		{
	 			k=next[k];
	 			continue;
			 }
	 		ss.push(point[k]);
	 		dis[point[k]]=dis[now]+weight[k]; 
	 		if(max1<dis[point[k]]) max1=dis[point[k]];
	 		k=next[k];
		 }
		 used[now]=true;
	 }
	 return max1;
}

void addedge(int x,int y,int z)
{
	total++;
	next[total]=head[x];
	head[x]=total;
	point[total]=y;
	weight[total]=z;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	int a[50002],b[50002],c[50002],ans,max=-1;
	bool key=true;
	for(int i=1;i<=n-1;i++)
	{
		cin >> a[i]>> b[i]>>c[i];
		addedge(a[i],b[i],c[i]);
		addedge(b[i],a[i],c[i]);  //双向建图，但只能单向使用 
		if(a[i]!=1) key=false;
	}	
	if(m==1)
	{
		for(int i=1;i<=n;i++)
		{
			ans=spfa(i);
			if(ans>max) max=ans;
		}
	}
	else if(key)
	{
		sort(c+1,c+n);
		max=99999;
		for(int i=1;i<=m*2;i++)
		{
			b[i]=c[i]+c[2*m+1-i];
			if(b[i]<max) max=b[i];
		}
	}
	else{
		max=15;
	}
	cout<<max;
	fclose(stdin);
	fclose(stdout);
	return 0;
}
