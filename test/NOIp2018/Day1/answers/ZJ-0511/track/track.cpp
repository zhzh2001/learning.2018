#include<bits/stdc++.h>
#define mp make_pair
#define pii pair<int,int>
using namespace std;

vector<pii> g[100010];
int n,m;

namespace subtask1
{
	long long dis[100010];
	
	int dfs(int now,int fa,int di)
	{
		dis[now]=di;
		for(int i=0;i<g[now].size();i++)
		{
			if(g[now][i].first==fa) continue;
			dfs(g[now][i].first,now,di+g[now][i].second);
		}
	}
	
	int solve()
	{
		dfs(1,0,0);
		int maxd=-1,pos=0;
		for(int i=1;i<=n;i++)
		{
			if(maxd<dis[i])
			{
				pos=i;
				maxd=dis[i];
			}
		}
		dfs(pos,0,0);
		maxd=-1;
		for(int i=1;i<=n;i++)
		{
			if(maxd<dis[i]) maxd=dis[i];
		}
		printf("%d\n",maxd);
	}
}

namespace subtask2
{
	int a[100010];
	int dfs(int now,int fa,int di)
	{
		a[now]=di;
		for(int i=0;i<g[now].size();i++)
		{
			if(g[now][i].first==fa) continue;
			dfs(g[now][i].first,now,g[now][i].second);
		}
	}
	
	int check(int x)
	{
		int cnt=0,len=0;
		for(int i=2;i<=n;i++)
		{
			len+=a[i];
			if(len>=x) cnt++,len=0;
		}
		return cnt>=m;
	}
	
	int solve()
	{
		dfs(1,0,0);
		int l=0,r=5e8,mid;
		while(l<=r)
		{
			mid=(l+r)>>1;
			if(check(mid))
			{
				l=mid;
			}
			else
			{
				r=mid-1;
			}
			if(r-l<=1) 
			{
				mid=check(r)?r:l;
				break;
			}
		}
		printf("%d\n",mid);
	}
}

namespace subtask3
{
	int a[100010];
	int dfs(int now,int fa,int di)
	{
		a[now]=di;
		for(int i=0;i<g[now].size();i++)
		{
			if(g[now][i].first==fa) continue;
			dfs(g[now][i].first,now,g[now][i].second);
		}
	}
	
	int check(int x)
	{
		int len=n,cnt=0;
		for(len=n;a[len]>=x;len--) cnt++;
		int r=len;
		for(int i=2;i<=len;i++)
		{
			if(i>=r) break;
			if(a[i]+a[r]>=x) cnt++,r--;
		}
		return cnt>=m;
	}
	
	int solve()
	{
		dfs(1,0,0);
		sort(a+2,a+n+1);
		int l=0,r=5e8,mid;
		while(l<=r)
		{
			mid=(l+r)>>1;
			if(check(mid))
			{
				l=mid;
			}
			else
			{
				r=mid-1;
			}
			if(r-l<=1)
			{
				mid=check(r)?r:l;
				break;
			}
		}
		printf("%d\n",mid);
	}
}

namespace subtask4
{
	int a[100010];
	
	int dfs(int now,int fa,int di)
	{
		a[now]=di;
		for(int i=0;i<g[now].size();i++)
		{
			if(g[now][i].first==fa) continue;
			dfs(g[now][i].first,now,g[now][i].second);
		}
	}
	
	int solve()
	{
		dfs(1,0,0);
		int pos=rand()%(n-2)+2;
		printf("%d\n",a[pos]);
	}
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int from,to,cost,flag1=1,flag2=1;
	for(int i=1;i<n;i++)
	{
		scanf("%d%d%d",&from,&to,&cost);
		g[from].push_back(mp(to,cost));
		g[to].push_back(mp(from,cost));
		if(from!=to-1) flag1=0;
		if(from!=1) flag2=0;
	}
	if(m==1) subtask1::solve();
	else if(flag1) subtask2::solve();
		else if(flag2) subtask3::solve();
			else subtask4::solve();
}
