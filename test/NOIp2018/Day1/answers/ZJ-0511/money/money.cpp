#include<bits/stdc++.h>
using namespace std;

int T,n;
long long a[110];

namespace subtask2
{
	int flag[110],vis[25010];
	
	int solve()
	{
		memset(vis,0,sizeof(vis));
		memset(flag,0,sizeof(flag));
		vis[0]=1;
		for(int i=1;i<=n;i++)
		{
			if(vis[a[i]])
			{
				flag[i]=1;
				continue;
			} 
			for(int j=0;j<=a[n];j++)
			{
				if(vis[j])
				{
					for(int k=1;k<=a[n];k++)
					{
						if(a[i]*k+j>a[n]) break;
						vis[a[i]*k+j]=1;
					}
				}
			}
		}
		int ans=0;
		for(int i=1;i<=n;i++) 
		{
			if(!flag[i]) ans++;
		}
		printf("%d\n",ans);
	}
}

namespace subtask1
{
	long long x,y;
	
	long long exgcd(long long a,long long b,long long &x,long long &y)
	{
		if(!b) 
		{
			x=1,y=0;
			return a;
		}
		long long res=exgcd(b,a%b,y,x);
		y-=(a/b)*x;
		return res;
	}
	
	int check(int xx,int yy,int zz)
	{
		long long gg=exgcd(xx,yy,x,y);
		if(zz%gg) return 0;
		x*=(zz/gg);
		y*=(zz/gg);
		if(x<0)
		{
			swap(x,y);
			swap(xx,yy);
		}
		int tmp=x/yy;
		x-=tmp*yy;
		y+=tmp*xx;
//		printf("%d %d\n",x,y);
		return (x>=0)&&(y>=0);
	}
	
	int flag[110];
	int solve()
	{
		memset(flag,0,sizeof(flag));
		if(n==1) puts("1");
		else 
		{
			for(int i=1;i<=n;i++)
			{
				for(int j=i;j<=n;j++)
				{
					int gg=exgcd(a[i],a[j],x,y);
					int nowi=a[i]/gg;
					int nowj=a[j]/gg;
					long long lim=1ll*nowi*nowj-nowi-nowj;
					lim*=gg;
					for(int k=j+1;k<=n;k++)
					{
						if(flag[k]) continue;
						if(k==i||k==j) continue;
						if(a[k]%gg) continue;
						if(a[k]>lim)
						{
							flag[k]=1;
						}
						else
						{
							flag[k]=check(a[i]/gg,a[j]/gg,a[k]/gg);
						}
					}
				}
			}
			int ans=0;
			for(int i=1;i<=n;i++)
			{
				if(!flag[i]) ans++;
			}
			printf("%d\n",ans);
		}
	}
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);
		for(int i=1;i<=n;i++) scanf("%lld",&a[i]);
		sort(a+1,a+n+1);
		n=unique(a+1,a+n+1)-a-1;	
		if(n<=25&&a[n]<=1000) subtask2::solve();
		else subtask1::solve();
	}
}
