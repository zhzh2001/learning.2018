#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while ((ch<'0' || ch>'9') && ch!='-') ch=getchar();
	if (ch=='-'){
		f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

const int N=105;
const int M=25005;

bool B[M];int A[N];

void Work(){
	memset(B,0,sizeof(B));
	int n=read(),Max=0,Ans=0;
	Rep(i,1,n) A[i]=read(),Max=max(Max,A[i]);
	sort(A+1,A+n+1);
	B[0]=true;
	Rep(i,1,n){
		if (!B[A[i]]){
			Ans++;
			Rep(j,A[i],Max) B[j]|=B[j-A[i]];
		}
	}
	printf("%d\n",Ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	Rep(i,1,T) Work();
}
