#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while ((ch<'0' || ch>'9') && ch!='-') ch=getchar();
	if (ch=='-'){
		f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

inline int Rand(){
	return rand()<<15|rand();
}
int n,m;
int main(){
	freopen("track.in","w",stdout);
	srand(time(0));
	printf("%d %d\n",n=25000,m=400);
	Rep(i,2,n) printf("%d %d %d\n",1,i,Rand()%10000+1);
}
