#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while ((ch<'0' || ch>'9') && ch!='-') ch=getchar();
	if (ch=='-'){
		f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

const int N=50005;

int n,m,Dgr[N];

int lines,front[N];

struct Edge{
	int next,to,wei;
}E[N*2];
inline void Addline(int u,int v,int w){
	E[++lines]=(Edge){front[u],v,w};front[u]=lines;
}

int Key,Max,Root;
int Fek[N],Sub[N],Dat[N],Bol[N],Bot[N];

int nex[N];
inline int find(int u){
	return nex[u]!=u?nex[u]=find(nex[u]):u;
}

int Cal(int *Dat){
	int Res=0;
	Rep(i,1,*Dat){
		Bot[i]=Bol[i],nex[i]=Bot[i]?i:i+1;
	}
	nex[*Dat+1]=*Dat+1;
	Dep(i,*Dat,1) if (Bot[i]){
		if (Dat[i]>=Key){
			Bot[i]=false;nex[i]=i+1;Res++;continue;
		}
		else{
			int L=1,R=i-1,res=-1;
			while (L<=R){
				int mid=(L+R>>1);
				if (Dat[mid]+Dat[i]>=Key) R=(res=mid)-1;
					else L=mid+1;
			}
			if (res!=-1){
				int pos=find(res);
				if (pos<i){
					Bot[i]=Bot[pos]=false;
					nex[i]=i+1;
					nex[pos]=pos+1;Res++;continue;
				}
			}
		}
	}
	return Res;
}

void Dp(int u,int fa){
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=fa){
			Dp(v,u);
			Fek[u]=Fek[u]+Fek[v];
		}
	}
	*Dat=0;
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=fa){
			++*Dat;
			Dat[*Dat]=Sub[v]+E[i].wei;
		}
	}
	if (*Dat>0){
		sort(Dat+1,Dat+*Dat+1);
		Rep(i,1,*Dat) Bol[i]=true;
		int Maxpair=Cal(Dat),Maxsub=0;
		if (u!=Root){
			int L=1,R=*Dat;
			Bol[R]=false;
			if (Cal(Dat)==Maxpair) L=R;
			Bol[R]=true;
			while (L<=R){
				int mid=(L+R>>1);
				Bol[mid]=false;
				int pair=Cal(Dat);
				if (pair>Maxpair){
					puts("An unbelievable Error.");exit(233);
				}
	//			if (u==4 && mid==1) printf("pair=%d\n",pair);
				if (pair==Maxpair){
					Maxsub=Dat[mid];L=mid+1;
				}
				else R=mid-1;
				Bol[mid]=true;
			}
			Fek[u]+=Maxpair;Sub[u]=Maxsub;
		}
		else{
			Fek[u]+=Maxpair;
		}
	}
	else Sub[u]=0;
}

int Dis[N];

void Dfs(int u,int fa,int dis){
	Dis[u]=dis;
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=fa) Dfs(v,u,dis+E[i].wei);
	}
}
void One(){
	Dfs(1,0,0);int Root=1,Leaf=1;
	Rep(i,1,n) if (Dis[i]>Dis[Root]) Root=i;
	Dfs(Root,0,0);
	Rep(i,1,n) if (Dis[i]>Dis[Leaf]) Leaf=i;
	printf("%d\n",Dis[Leaf]);
}

void Test(int val){
	Key=val;Dp(1,0);
	Rep(i,1,n) printf("i=%d Fek=%d Sub=%d\n",i,Fek[i],Sub[i]);
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
//	printf("%.8lf\n",50000*log2(50000*10000)*log2(50000*10000)*log2(50000));
	n=read(),m=read();int Max=0;
	Rep(i,1,n-1){
		int u=read(),v=read(),w=read();
		Addline(u,v,w),Addline(v,u,w);
		Max=Max+w;
		Dgr[u]++,Dgr[v]++;
	}
	Root=1;
	Rep(i,1,n) if (Dgr[i]>Dgr[Root]) Root=i;
	if (m==1){
		One();return 0;
	}
	int L=0,R=Max,res=-1;
	while (L<=R){
		int mid=(L+R>>1);Key=mid;
		memset(Fek,0,sizeof(Fek));
		memset(Sub,0,sizeof(Sub));
		Dp(Root,0);
		if (Fek[Root]>=m) L=(res=mid)+1;
			else R=mid-1;
	}
	printf("%d\n",res);
}
