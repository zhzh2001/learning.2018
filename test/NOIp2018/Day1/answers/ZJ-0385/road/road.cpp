#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while ((ch<'0' || ch>'9') && ch!='-') ch=getchar();
	if (ch=='-'){
		f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

const int N=100005;

int n,d[N];

struct node{
	int val,pos;
}P[N];

inline bool Cmp(node A,node B){
	return A.val>B.val;
}

int h[N],l[N],r[N];

void Delet(int pos){
	r[l[pos]]=r[pos];
	l[r[pos]]=l[pos];
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	Rep(i,1,n) P[i].pos=i,P[i].val=read();
	Rep(i,1,n) h[i]=P[i].val;
	sort(P+1,P+n+1,Cmp);
	Rep(i,1,n) l[i]=i-1,r[i]=i+1;
	int Ans=0;
	Rep(i,1,n){
		int pos=P[i].pos;
		if (h[l[pos]]>h[r[pos]]){
			Ans+=h[pos]-h[l[pos]];
		}
		else{
			Ans+=h[pos]-h[r[pos]];
		}
		Delet(pos);
	}
	printf("%d\n",Ans);
}
