#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0; char ch=getchar(); bool pos=1;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
const int N=105;
int n,a[N];
bool dq[25005];
int main(){
	freopen("money.in","r",stdin); freopen("money.out","w",stdout);
	int T=read();
	while(T--){
		n=read(); memset(dq,0,sizeof(dq)); dq[0]=1; int ans=0;
		for(int i=1;i<=n;i++)a[i]=read();
		sort(&a[1],&a[n+1]);
		for(int i=1;i<=n;i++)if(!dq[a[i]]){
			ans++;
			for(register int j=a[i];j<=25000;j++)dq[j]|=dq[j-a[i]];
		}
		cout<<ans<<endl;
	}
	return 0;
}
