#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0; char ch=getchar(); bool pos=1;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
const int N=100005,inf=1e9;
int n,top,ans,a[N],q[N],f[N],g[N];
int main(){
	freopen("road.in","r",stdin); freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	top=0;
	for(int i=1;i<=n;i++){
		while(top&&a[q[top]]>a[i])top--;
		f[i]=q[top]; q[++top]=i;
	}
	q[0]=n+1; top=0;
	for(int i=n;i;i--){
		while(top&&a[q[top]]>=a[i])top--;
		g[i]=q[top]; q[++top]=i;
		//cout<<i<<" "<<f[i]<<" "<<g[i]<<endl;
		ans+=a[i]-max(a[f[i]],a[g[i]]);
	}
	cout<<ans<<endl;
	return 0;
}
