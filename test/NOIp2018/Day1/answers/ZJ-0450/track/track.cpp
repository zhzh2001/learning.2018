#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define mp make_pair
inline ll read(){
	ll x=0; char ch=getchar(); bool pos=1;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
const int N=50005,inf=1e9;
int n,m,ans,g[N],q[N],Q[N],tot,mid;
vector<pair<int,int> > v[N];
int get(int ajl){
	int ans=0,zs=0; 
	for(register int i=1;i<=tot;i++)if(i!=ajl)Q[++zs]=q[i];
	for(register int i=1;i<zs;i++)if(Q[i]+Q[zs]>=mid){
		ans++; zs--; 
	}
	return ans;
}
void dfs(int p,int fa){
	g[p]=0;
	for(unsigned j=0;j<v[p].size();j++){
		int u=v[p][j].first;
		if(u==fa)continue;
		dfs(u,p);
		g[u]+=v[p][j].second;
	}
	if(ans>=m)return;
	tot=0;
	for(unsigned j=0;j<v[p].size();j++)if(v[p][j].first!=fa){
		if(g[v[p][j].first]<mid)q[++tot]=g[v[p][j].first]; else ans++;
	}
	sort(&q[1],&q[tot+1]);
	int l=0,r=tot,sum=get(0);
	while(l<r){
		int x=(l+r+1)>>1;
		if(get(x)==sum)l=x; else r=x-1;
	}
	ans+=sum; g[p]=q[l];
	//cout<<f[p]<<" "<<g[p]<<endl;
}
int main(){
	freopen("track.in","r",stdin); freopen("track.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<n;i++){
		int s=read(),t=read(),x=read();
		v[s].push_back(mp(t,x)); v[t].push_back(mp(s,x));
	}
	int l=0,r=inf;
	while(l<r){
		mid=(l+r+1)>>1; ans=0;
		dfs(1,0); 
		if(ans>=m)l=mid; else r=mid-1;
	}
	cout<<l<<endl;
}

