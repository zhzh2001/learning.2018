#include<bits/stdc++.h>
using namespace std;

#define rep(i,l,r) for(int i=l;i<=r;++i)
const int N=1e6+5;
int a[N];

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n;
	cin>>n;
	rep(i,1,n)scanf("%d",a+i);
	int ans=0;
	rep(i,1,n)ans+=max(0,a[i]-a[i-1]);
	cout<<ans;
}
