#include<bits/stdc++.h>
using namespace std;

#define rep(i,l,r) for(int i=l;i<=r;++i)
const int N=100+5,U=25000+5;
int a[N];
bool can[U];

int main()
{
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	int tt;
	cin>>tt;
	while(tt--)
	{
		int n;
		cin>>n;
		rep(i,1,n)scanf("%d",a+i);
		sort(a+1,a+n+1);
		rep(j,0,U-1)can[j]=0;
		can[0]=1;
		int m=0;
		rep(i,1,n)
		if(!can[a[i]]){++m;rep(j,a[i],U-1)can[j]|=can[j-a[i]];}
		printf("%d\n",m);
	}
}
