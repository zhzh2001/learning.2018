#include<bits/stdc++.h>
using namespace std;

template <typename T> void chmin(T &x,const T &y)
{
	if(x>y)x=y;
}
typedef pair<int,int> pii;
#define rep(i,l,r) for(int i=l;i<=r;++i)
#define rep0(i,l,r) for(int i=l;i<int(r);++i)
#define per(i,r,l) for(int i=r;i>=l;--i)
const int N=50000+5;
int n,m;
vector<pii>lk[N];
int f[N],g[N],mid;

int q[N],k;
int get_mx(int q[],int k)
{
	if(k<2)return 0;
	int limit=k/2,j=1;
	int i=k;
	while(1)
	{
		if(q[i]+q[i]<mid)break;
		while(q[i]+q[j]<mid)++j;
		chmin(limit,k-i+(i-j+1)/2);
		if(k-i+1>limit)break;
		--i;
	}
	return k-i;
}
void dfs(int x,int fr)
{
	rep0(i,0,lk[x].size())
	if(lk[x][i].first!=fr)dfs(lk[x][i].first,x);
	f[x]=0;k=0;
	rep0(i,0,lk[x].size())
	{
		pii e=lk[x][i];
		if(e.first==fr)continue;
		f[x]+=f[e.first];
		q[++k]=g[e.first]+e.second;
		if(q[k]>=mid){++f[x];--k;}
	}
	sort(q+1,q+k+1);
	int now=get_mx(q,k);
	f[x]+=now;
	int l=0,r=k+1;
	while(l+1!=r)
	{
		int mid=(l+r)/2;
		int tmp=q[mid];
		rep(i,mid,k-1)q[i]=q[i+1];
		if(get_mx(q,k-1)==now)l=mid;
		else r=mid;
		per(i,k-1,mid)q[i+1]=q[i];
		q[mid]=tmp;
	}
	g[x]=q[l];
}
bool ok()
{
	dfs(1,0);
	return f[1]>=m;
}

int main()
{
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	cin>>n>>m;
	rep(i,1,n-1)
	{
		int x,y,l;
		scanf("%d%d%d",&x,&y,&l);
		lk[x].push_back(pii(y,l));lk[y].push_back(pii(x,l));
	}
	int l=0,r=5e8/m+5;	
	while(l+1!=r)
	{
		mid=(l+r)/2;
		if(ok())l=mid;
		else r=mid;
	}
	cout<<l;
}
