#include <map>
#include <set>
#include <cmath>
#include <queue>
#include <bitset>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define inf 0x3f3f3f3f
//#define int long long
#define maxn 50010
//#define MOD 
using namespace std;
char gc()
{
	static char buf[100000],*p1=buf+100000,*p2=p1;
	if(p1==p2)
	{
		p1=buf;
		p2=buf+fread(buf,1,100000,stdin);
	}
	return *p1++;
}
#define gc getchar
int read()
{
	int d=0,w=1;
	char c=gc();
	for(;c<'0'||c>'9';c=gc())
		if(c=='-') w=-1;
	for(;c>='0'&&c<='9';c=gc())
		d=(d<<1)+(d<<3)+c-48;
	return d*w;
}
void write(int x)
{
	if(x<0)
	{
		putchar('-');
		x=-x;
	}
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void wrs(int x)
{
	write(x);
	putchar(' ');
}
void wln(int x)
{
	write(x);
	putchar('\n');
}
int bb[maxn<<1][3],h[maxn],f[maxn],rr,l,r,ans,n,m,mst,mi,ma;
multiset<int> s[maxn];
void add(int x,int y,int z)
{
	bb[++rr][0]=y;
	bb[rr][1]=z;
	bb[rr][2]=h[x];
	h[x]=rr;
}
void dfs(int k,int fa)
{
	for(int i=h[k];i;i=bb[i][2])
	{
		if(bb[i][0]==fa) continue;
		dfs(bb[i][0],k);
		if(f[bb[i][0]]+bb[i][1]>=mst) ans++;
			else s[k].insert(f[bb[i][0]]+bb[i][1]);
	}
	int tmp=0;
	for(;s[k].size();)
	{
		int x=*s[k].begin();
		set<int>::iterator it=s[k].lower_bound(mst-x);
		if(it==s[k].begin()) it++;
		if(it!=s[k].end())
		{
			ans++;
			s[k].erase(it);
		}
		else tmp=max(tmp,*s[k].begin());
		s[k].erase(s[k].begin());
	}
	f[k]=tmp;
}
int check()
{
//	for(int j=1;j<=n;j++)
//	{
	ans=0;
//	memset(f,0,sizeof f);
//	for(int i=1;i<=n;i++)
//		s[i].clear();
	dfs(1,0);
//	wrs(ans);
//	if(ans>=m) return 1;
	return ans>=m;
//}
//	return 0;
}
signed main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();
	m=read();
	mi=inf;
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read(),z=read();
		add(x,y,z);
		add(y,x,z);
		mi=min(mi,z);
		ma=max(ma,z);
	}
	l=mi;
	r=ma*(n-1)+1;
	while(l<r-1)
	{
//		wrs(l);
//		wln(r);
		mst=l+r>>1;
		if(check()) l=mst;
			else r=mst;
	}
	write(l);
	return 0;
}
