#include <map>
#include <set>
#include <cmath>
#include <queue>
#include <bitset>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define inf 0x3f3f3f3f3f3f3f3f
#define int long long
#define maxn 50010
#define maxx 10000000
//#define MOD 
using namespace std;
char gc()
{
	static char buf[100000],*p1=buf+100000,*p2=p1;
	if(p1==p2)
	{
		p1=buf;
		p2=buf+fread(buf,1,100000,stdin);
	}
	return *p1++;
}
#define gc getchar
int read()
{
	int d=0,w=1;
	char c=gc();
	for(;c<'0'||c>'9';c=gc())
		if(c=='-') w=-1;
	for(;c>='0'&&c<='9';c=gc())
		d=(d<<1)+(d<<3)+c-48;
	return d*w;
}
void write(int x)
{
	if(x<0)
	{
		putchar('-');
		x=-x;
	}
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void wrs(int x)
{
	write(x);
	putchar(' ');
}
void wln(int x)
{
	write(x);
	putchar('\n');
}
int bb[maxn<<1][3],h[maxn],f[maxn],rr,l,r,ans,n,m,mst,mi,ma,siz[maxx],cnt[maxx],val[maxx],son[maxx][2],fa[maxx],root[maxx];
queue<int> q;
void up(int k)
{
	siz[k]=siz[son[k][0]]+siz[son[k][1]]+cnt[k];
}
void rotate(int x)
{
	int p=fa[x],g=fa[p],wih=son[p][1]==x;
	fa[p]=x;
	fa[x]=g;
	fa[son[x][wih^1]]=p;
	son[g][son[g][1]==p]=x;
	son[p][wih]=son[x][wih^1];
	son[x][wih^1]=p;
	up(x);
	up(p);
}
void splay(int wh,int x,int goal)
{
	for(;fa[x]!=goal;rotate(x))
		if(fa[fa[x]]!=goal) rotate((son[fa[fa[x]]][1]==fa[x])^(son[fa[x]][1]==x)?x:fa[x]);
	if(goal==0) root[wh]=x;
}
void find(int wh,int x)
{
	int u=root[wh];
	for(;val[u]!=x&&son[u][val[u]<x];u=son[u][val[u]<x]);
	splay(wh,u,0);
}
int nxt(int wh,int x,int mode)
{
	find(wh,x);
	int u=root[wh];
	if(val[u]>x&&mode||val[u]<x&&!mode) return u;
	u=son[u][mode];
	for(;son[u][!mode];u=son[u][!mode]);
	return u;
}
void insert(int wh,int x)
{
	int lst=nxt(wh,x,0),ntt=nxt(wh,x,1);
	splay(wh,lst,0);
	splay(wh,ntt,lst);
	if(!son[ntt][0])
	{
		int tmp=q.front();
		q.pop();
		son[ntt][0]=tmp;
		val[tmp]=x;
		siz[tmp]=cnt[tmp]=0;
		fa[tmp]=ntt;
		son[tmp][0]=son[tmp][1]=0;
	}
	siz[son[ntt][0]]++;
	cnt[son[ntt][0]]++;
	up(ntt);
	up(lst);
}
void del(int wh,int x)
{
	int lst=nxt(wh,x,0),ntt=nxt(wh,x,1);
	splay(wh,lst,0);
	splay(wh,ntt,lst);
	cnt[son[ntt][0]]--;
	siz[son[ntt][0]]--;
	if(!cnt[son[ntt][0]])
	{
		q.push(son[ntt][0]);
		son[ntt][0]=0;
	}
	up(ntt);
	up(lst);
}
int kth(int wh,int x)
{
	int u=root[wh];
	for(;;)
		if(siz[son[u][0]]+cnt[u]<x)
		{
			x-=siz[son[u][0]]+cnt[u];
			u=son[u][1];
		}
		else if(siz[son[u][0]]>=x) u=son[u][0];
			else return u;
}
int lbd(int wh,int x)
{
	find(wh,x);
	int u=root[wh];
	if(val[u]<x)
	{
		u=son[u][1];
		for(;son[u][0];u=son[u][0]);
	}
	return u==kth(wh,2)?kth(wh,3):u;
}
void add(int x,int y,int z)
{
	bb[++rr][0]=y;
	bb[rr][1]=z;
	bb[rr][2]=h[x];
	h[x]=rr;
}
//void ppp(int k)
//{
//	if(son[k][0])ppp(son[k][0]);
//	wrs(k);
//	wrs(val[k]);
//	wrs(cnt[k]);
//	wrs(siz[k]);
//	wrs(son[k][0]);
//	wrs(son[k][1]);
//	wln(fa[k]);
//	if(son[k][1])ppp(son[k][1]);
//}
void dfs(int k,int fa)
{
//	wrs(k);
	for(int i=h[k];i;i=bb[i][2])
	{
		if(bb[i][0]==fa) continue;
		dfs(bb[i][0],k);
		if(f[bb[i][0]]+bb[i][1]>=mst) ans++;
			else insert(k,f[bb[i][0]]+bb[i][1]);
	}
//	wrs(k);
//	if(k==3)
//	{
//		ppp(root[k]);
//		wrs(3143);
//		wln(siz[root[k]]);
//	}
	int tmp=0;
	for(;siz[root[k]]>2;)
	{
		int x=val[kth(k,2)];
//		wrs(x);
		int it=lbd(k,mst-x);
		if(val[it]!=inf)
		{
			ans++;
			del(k,val[it]);
		}
		else tmp=max(tmp,x);
		del(k,x);
	}
	f[k]=tmp;
}
int check()
{
//	for(int j=1;j<=n;j++)
//	{
	ans=0;
//	memset(f,0,sizeof f);
//	for(int i=1;i<=n;i++)
//		s[i].clear();
	dfs(1,0);
//	wrs(ans);
//	if(ans>=m) return 1;
	return ans>=m;
//}
//	return 0;
}
signed main()
{
//	freopen("track.in","r",stdin);
//	freopen("track.out","w",stdout);
	n=read();
	m=read();
	mi=inf;
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read(),z=read();
		add(x,y,z);
		add(y,x,z);
		mi=min(mi,z);
		ma=max(ma,z);
	}
	for(int i=1;i<=n;i++)
	{
		val[(i<<1)-1]=-inf;
		val[i<<1]=inf;
		fa[i<<1]=root[i]=(i<<1)-1;
		cnt[(i<<1-1)]=cnt[i<<1]=siz[i<<1]=1;
		siz[(i<<1)-1]=2;
		son[(i<<1)-1][1]=i<<1;
	}
	for(int i=(n<<1)+1;i<10000000;q.push(i++));
	l=mi;
	r=ma*(n-1)+1;
	while(l<r-1)
	{
//		wrs(l);
//		wln(r);
		mst=l+r>>1;
		if(check()) l=mst;
			else r=mst;
	}
	write(l);
	return 0;
}
