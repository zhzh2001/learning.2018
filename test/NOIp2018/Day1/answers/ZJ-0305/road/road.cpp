#include <map>
#include <set>
#include <cmath>
#include <queue>
#include <bitset>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
//#define int long long
//#define maxn 
//#define MOD 
using namespace std;
char gc()
{
	static char buf[100000],*p1=buf+100000,*p2=p1;
	if(p1==p2)
	{
		p1=buf;
		p2=buf+fread(buf,1,100000,stdin);
	}
	return *p1++;
}
//#define gc getchar
int read()
{
	int d=0,w=1;
	char c=gc();
	for(;c<'0'||c>'9';c=gc())
		if(c=='-') w=-1;
	for(;c>='0'&&c<='9';c=gc())
		d=(d<<1)+(d<<3)+c-48;
	return d*w;
}
void write(int x)
{
	if(x<0)
	{
		putchar('-');
		x=-x;
	}
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void wrs(int x)
{
	write(x);
	putchar(' ');
}
void wln(int x)
{
	write(x);
	putchar('\n');
}
int lst,n,ans;
signed main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
	{
		int x=read();
		ans+=x-lst>0?x-lst:0;
		lst=x; 
	}
	write(ans);
	return 0;
}
