#include <map>
#include <set>
#include <cmath>
#include <queue>
#include <bitset>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
//#define int long long
//#define maxn 
#define maxai 25010
//#define MOD 
using namespace std;
char gc()
{
	static char buf[100000],*p1=buf+100000,*p2=p1;
	if(p1==p2)
	{
		p1=buf;
		p2=buf+fread(buf,1,100000,stdin);
	}
	return *p1++;
}
//#define gc getchar
int read()
{
	int d=0,w=1;
	char c=gc();
	for(;c<'0'||c>'9';c=gc())
		if(c=='-') w=-1;
	for(;c>='0'&&c<='9';c=gc())
		d=(d<<1)+(d<<3)+c-48;
	return d*w;
}
void write(int x)
{
	if(x<0)
	{
		putchar('-');
		x=-x;
	}
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void wrs(int x)
{
	write(x);
	putchar(' ');
}
void wln(int x)
{
	write(x);
	putchar('\n');
}
int a[maxai],vis[maxai],f[maxai],t,n,ans,ma;
signed main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(t=read();t--;)
	{
		n=read();
		memset(vis,0,sizeof vis);
		ma=0;
		for(int i=1;i<=n;ma=max(a[i++],ma))
			vis[a[i]=read()]=1;
		memset(f,0,sizeof f);
		for(int i=1;i<=n;i++)
			for(int j=a[i];j<=ma;j++)
				f[j]|=f[j-a[i]]|vis[j-a[i]];
		ans=n;
		for(int i=1;i<=n;i++)
			if(f[a[i]]) ans--;
		wln(ans);
	}
	return 0;
}
