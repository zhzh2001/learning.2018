#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define pii pair<int,int>
#define se second
#define fi first
using namespace std;
const int N=50005;
struct edge{
	int to,next,v;
}e[N*2];
int head[N],tot;
int n,m,mid,total;
bool vis[N*2];
void add(int x,int y,int v){
	e[++tot]=(edge){y,head[x],v};
	head[x]=tot;
}
int beg[N],ed[N],dfn[N];
int T,tmp[N*2];
void init_dfs(int x,int fa){
	beg[x]=T+1;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa) dfn[e[i].to]=++T;
	ed[x]=++T;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa) init_dfs(e[i].to,x);
}
int times=0;
int calc(int ban,int l,int r){
	times+=r-l+1;
	For(i,l,r) vis[i]=0;
	if (ban!=-1) vis[ban]=1;
	int sum=0;
	while (l<=r){
		for (;vis[r]&&l<r;r--);
		if (vis[r]||l==r) break;
		vis[r]=1;
		for (;(vis[l]||tmp[l]+tmp[r]<mid)&&l<r;l++);
		if (vis[l]||l==r||tmp[l]+tmp[r]<mid) break;
		sum++; vis[l++]=1; r--;
	}
	return sum;
}
bool wzp[N*2];
int dfs(int x,int fa){
	if (total>=m) return 0;
	int ans=0;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa){
			int v=dfs(e[i].to,x)+e[i].v;
			if (v>=mid) total++,tmp[dfn[e[i].to]]=0;
			else tmp[dfn[e[i].to]]=v;
		}
	if (total>=m) return 0;
	tmp[ed[x]]=0;
	sort(tmp+beg[x],tmp+ed[x]+1);
	if (tmp[ed[x]]*2<mid) return tmp[ed[x]];
	int	mx=calc(-1,beg[x],ed[x]); total+=mx;
	int l=beg[x],r=ed[x],ok=0;
	For(i,l,r) wzp[i]=vis[i];
	while (l<=r){
		int mid=(l+r)/2;
		if (!wzp[mid]) ok=mid,l=mid+1;
		if (calc(mid,beg[x],ed[x])>=mx)
			ok=mid,l=mid+1;
		else r=mid-1;
	}
	return tmp[ok];
}
inline int read(){
	int x=0;
	#define gc getchar
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(); m=read();
	int sum=0;
	For(i,1,n-1){
		int x=read(),y=read(),v=read();
		add(x,y,v); add(y,x,v);
		sum+=v;
	}
	init_dfs(1,0);
	int l=1,r=sum/m,ans=0;
	while (l<=r){
		mid=(l+r)/2;
		total=0; dfs(1,0);
		if (total>=m) ans=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d",ans);
	return 0;
}
/*
7 1
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7
9 3
1 2 6
2 3 3
3 4 5
4 5 10
6 2 4
7 2 9
8 4 7
9 4 4
*/
