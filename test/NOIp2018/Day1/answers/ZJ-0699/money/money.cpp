#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
const int N=25005;
bool b[N];
int a[N],n;
void solve(){
	For(i,0,N-1) b[i]=0;
	b[0]=1;
	scanf("%d",&n);
	For(i,1,n) scanf("%d",&a[i]);
	sort(a+1,a+n+1);
	int ans=0;
	For(i,1,n)
		if (!b[a[i]]){
			ans++;
			For(j,a[i],N-1) b[j]|=b[j-a[i]];
		}
	printf("%d\n",ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--) solve();
	return 0;
}
/*
O(n*a[i]*T)
*/
