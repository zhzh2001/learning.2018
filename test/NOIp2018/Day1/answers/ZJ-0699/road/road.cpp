#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
const int N=100005;
int n,a[N],ans;
vector<int> vec[N];
inline int read(){
	int x=0;
	#define gc getchar
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	For(i,1,n){
		a[i]=read();
		vec[a[i]].push_back(i);
	}
	int sum=0;
	Rep(i,10000,1){
		for (int j=0;j<vec[i].size();j++){
			int x=vec[i][j];
			sum++;
			if (x!=1&&a[x-1]>=i) sum--;
			if (x!=n&&a[x+1]>i) sum--;
		}
		ans+=sum;
	}
	printf("%d",ans);
	return 0;
}
/*
6
4 3 2 5 3 5
*/
