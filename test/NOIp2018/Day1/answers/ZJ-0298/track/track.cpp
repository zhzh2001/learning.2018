#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<queue>
#include<cstdlib>
#include<ctime>
#define il inline
using namespace std;
const int N=5e5+5;
#define getchar() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<20,stdin))?EOF:*p1++)
char buf[1<<21],*p1=buf,*p2=buf;
il int read(){
	int x=0,f=1;char c=getchar();
	for(;!isdigit(c);c=getchar()) if(c=='-') f=-1;
	for(;isdigit(c);c=getchar()) x=(x+(x<<2)<<1)+c-48;
	return x*f;
}
int n,m,k,f1=1,f2,tot,rt,h[N],v[N],d[N],b[N];
struct Edge{int to,nxt,val;}a[N<<1];
il void add(int x,int y,int z){a[++k].to=y,a[k].nxt=h[x],a[k].val=z,h[x]=k;}
queue<int> q;
void solve1(){
	memset(v,0,sizeof(v));
	memset(d,0,sizeof(d));
	v[rt]=1;q.push(rt);int ma=0;
	while(!q.empty()){
		int x=q.front();q.pop();
		for(int i=h[x];i;i=a[i].nxt){
			int y=a[i].to;
			if(v[a[i].to]) continue;
			d[y]=d[x]+a[i].val;
			if(d[y]>d[rt]) rt=y;
			q.push(y);v[y]=1;
		}
	}
	memset(v,0,sizeof(v));
	memset(d,0,sizeof(d));
	q.push(rt);v[rt]=1;
	while(!q.empty()){
		int x=q.front();q.pop();
		for(int i=h[x];i;i=a[i].nxt){
			int y=a[i].to;
			if(v[a[i].to]) continue;
			d[y]=d[x]+a[i].val;
			if(d[y]>ma) ma=d[y];
			q.push(y);v[y]=1;
		}
	}
	printf("%d",ma);
}
il bool check(int t){
	int sum=0,ct=0;
	for(int i=1;i<=tot;++i){
		sum+=b[i];
		if(sum>=t) ++ct,sum=0;
	}
	return ct>=m;
}
void solve2(){
	memset(v,0,sizeof(v));
	memset(d,0,sizeof(d));
	v[rt]=1;q.push(rt);
	while(!q.empty()){
		int x=q.front();q.pop();
		for(int i=h[x];i;i=a[i].nxt){
			int y=a[i].to;
			if(v[a[i].to]) continue;
			b[++tot]=a[i].val;
			q.push(y);v[y]=1;
		}
	}
	int l=1,r=1e9,mid;
	while(l<=r){
		mid=l+r>>1;
		if(check(mid)) l=mid+1;else r=mid-1;
	}
	printf("%d",l-1);
}
il bool cmp(int x,int y){return x>y;}
il bool che(int t){
	int ct=0,ar=tot;
	for(int i=1;i<=tot;++i){
		if(b[i]<t){
			int al=i+1;
			while(al<=ar){
				int mm=al+ar>>1;
				if(b[mm]>=t-b[i]) al=mm+1;else ar=mm-1;
			}
			if(ar>i) ++ct;
		}
		else ++ct;
	}
	return ct>=m;
}
void solve3(){
	memset(v,0,sizeof(v));
	memset(d,0,sizeof(d));
	v[1]=1;q.push(1);
	while(!q.empty()){
		int x=q.front();q.pop();
		for(int i=h[x];i;i=a[i].nxt){
			int y=a[i].to;
			if(v[a[i].to]) continue;
			b[++tot]=a[i].val;
			q.push(y);v[y]=1;
		}
	}
	sort(b+1,b+tot+1,cmp);
	int l=1,r=1e9,mid;
	while(l<=r){
		mid=l+r>>1;
		if(che(mid)) l=mid+1;else r=mid-1;
	}
	printf("%d",l-1);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(int i=1,x,y,z;i<n;++i)
	x=read(),y=read(),z=read(),++d[x],++d[y],add(x,y,z),add(y,x,z);
	for(int i=1;i<=n;++i){
		if(d[i]>2) f1=0;
		if(d[i]==1) rt=i;
	}
	if(d[1]==n-1) f2=1;
	if(m==1){
		solve1();
		return 0;
	}
	if(f1){
		solve2();
		return 0; 
	}
	if(f2){
		solve3();
		return 0; 
	}
	srand(20030628);
	printf("%d",rand()%10000);
	return 0;
}
