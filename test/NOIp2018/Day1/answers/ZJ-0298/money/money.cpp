#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define il inline
using namespace std;
il int read(){
	int x=0;char c=getchar();for(;!isdigit(c);c=getchar());
	for(;isdigit(c);c=getchar()) x=(x+(x<<2)<<1)+c-48;
	return x;
}
int T,n,m,a[105],f[250005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		m=n=read();
		for(int i=1;i<=n;++i) a[i]=read();
		sort(a+1,a+n+1);memset(f,0,sizeof(f));f[0]=1;
		for(int i=1;i<=n;++i){
			if(f[a[i]]) --m;
			else for(int j=a[i];j<=a[n];++j)
			f[j]|=f[j-a[i]];
		}
		printf("%d\n",m);
	}
	return 0;
}
