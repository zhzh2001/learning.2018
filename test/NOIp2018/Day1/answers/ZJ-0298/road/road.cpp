#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define il inline
#define rint register int
using namespace std;
typedef long long ll;
const int N=1e5+5;
#define getchar() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<20,stdin))?EOF:*p1++)
char buf[1<<21],*p1=buf,*p2=buf;
il int read(){
	int x=0;char c=getchar();for(;!isdigit(c);c=getchar());
	for(;isdigit(c);c=getchar()) x=(x+(x<<2)<<1)+c-48;
	return x;
}
int n,m,a[N],b[N],c[N],w[N],h[N];ll ans;
void solve(rint l,rint r,rint d){
	if(l+1==r){
		ans+=w[max(a[l],a[r])]-w[d-1];
		return;
	}
	if(l==r){
		ans+=w[a[l]]-w[d-1];
		return;
	}
	ans+=w[d]-w[d-1];
	rint pos;
	for(rint i=l;i<=r;++i){
		if(a[i]>d&&a[i-1]<=d) pos=i;
		if(a[i+1]<=d&&a[i]>d) solve(pos,i,d+1);
	}
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(rint i=1;i<=n;++i) w[i]=b[i]=a[i]=read();
	sort(b+1,b+n+1);b[0]=-1;
	for(rint i=1;i<=n;++i) if(b[i]^b[m]) b[++m]=b[i],c[b[i]]=m;
	for(rint i=1;i<=n;++i) w[c[a[i]]]=a[i],a[i]=c[a[i]];
	solve(1,n,1);
	printf("%lld",ans);
	return 0;
}
