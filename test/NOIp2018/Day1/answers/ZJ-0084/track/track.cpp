#include<algorithm>
#include<cstring>
#include<cstdio>

using namespace std;

#define rep(i,a,b) for (int i=(a);i<=(b);i++)
#define ms(x,y) memset(x,y,sizeof(x))

const int N=50005,M=100005;

int n,m,le,ri,ans,len,a[N],f[N],g[N],dad[N];
int tot,lnk[N],son[M],w[M],nxt[M];
bool vis[N];

int read()
{
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
void add(int x,int y,int z)
{
	tot++;son[tot]=y;w[tot]=z;nxt[tot]=lnk[x];lnk[x]=tot;
	tot++;son[tot]=x;w[tot]=z;nxt[tot]=lnk[y];lnk[y]=tot;
}
int get(int x)
{
	if (x>len) return len+1;
	return x==dad[x]?x:dad[x]=get(dad[x]);
}
int find(int L,int R,int x,int ned)
{
	int mid,res=len+1;while (L<=R)
	{
		mid=((L+R)>>1);
		if (a[x]+a[mid]>=ned) res=mid,R=mid-1;else L=mid+1;
	}
	return res;
}
void dfs(int x,int fa,int ned)
{
	for (int j=lnk[x];j;j=nxt[j]) if (son[j]!=fa) dfs(son[j],x,ned);
	len=0;for (int j=lnk[x];j;j=nxt[j]) if (son[j]!=fa) f[x]+=f[son[j]],a[++len]=g[son[j]]+w[j];
	if (!len) return;sort(a+1,a+len+1);while (len&&a[len]>=ned) f[x]++,len--;
	rep(i,1,len) vis[i]=1,dad[i]=i;
	rep(i,1,len) if (vis[i])
	{
		int j=get(find(i+1,len,i,ned));
		if (j<=len) vis[i]=0,vis[j]=0,f[x]++,dad[j]=get(j+1);
	}
	rep(i,1,len) if (vis[i]) g[x]=a[i];
}
bool check(int ned) {ms(f,0);ms(g,0);dfs(1,0,ned);return (f[1]>=m);}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	rep(i,2,n) {int x=read(),y=read(),z=read();add(x,y,z);ri+=z;}
	for (int mid;le<=ri;)
	{
		mid=((le+ri)>>1);
		if (check(mid)) ans=mid,le=mid+1;else ri=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
