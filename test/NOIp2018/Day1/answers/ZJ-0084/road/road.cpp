#include<cstdio>

using namespace std;

#define rep(i,a,b) for (int i=(a);i<=(b);i++)

const int N=100005;

int n,ans,a[N];

int read()
{
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();rep(i,1,n) a[i]=read();
	ans=a[1];rep(i,2,n) if (a[i]>a[i-1]) ans+=(a[i]-a[i-1]);
	printf("%d\n",ans);
	return 0;
}
