#include<algorithm>
#include<cstring>
#include<cstdio>

using namespace std;

#define rep(i,a,b) for (int i=(a);i<=(b);i++)
#define ms(x,y) memset(x,y,sizeof(x))

const int N=105,M=25005;

int T,n,m,mx,a[N],b[N];
bool f[M];

int read()
{
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (T=read();T;T--)
	{
		n=read();rep(i,1,n) a[i]=read();
		sort(a+1,a+n+1);mx=a[n];ms(f,0);
		f[0]=1;rep(i,1,n)
		{
			if (f[a[i]]) continue;b[++m]=a[i];
			rep(j,0,mx-b[m]) if (f[j]) f[j+b[m]]=1;
		}
		printf("%d\n",m);m=0;
	}
	return 0;
}
