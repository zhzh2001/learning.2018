#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

const int N=100010;

inline char nc(){
  static char buf[100000],*p1=buf,*p2=buf;
  return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}

inline void read(int &x){
  char c=nc(); x=0;
  for(;c>'9'||c<'0';c=nc());for(;c>='0'&&c<='9';x=x*10+c-'0',c=nc());
}

int ans,n,a[N],l[N],s[N][20];

inline int Query(int x,int y){
  int t=l[y-x+1];
  return min(s[x][t],s[y-(1<<t)+1][t]);
}

void solve(int l,int r,int x){
  if(l>r) return ;
  int y=Query(l,r),L=l,R=r; ans+=y-x;
  while(L<=R){
    if(a[L]==y){
      solve(l,L-1,y); solve(L+1,r,y); return ;
    }
    if(a[R]==y){
      solve(l,R-1,y); solve(R+1,r,y); return ;
    }
    L++; R--;
  }
}

int main(){
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
  read(n);
  for(int i=1;i<=n;i++) read(a[i]),s[i][0]=a[i];
  for(int i=1;i<=n;i++) l[i]=l[i-1]+((1<<(l[i-1]+1))==i);
  for(int k=1;k<=l[n];k++)
    for(int i=1;i+(1<<k)-1<=n;i++)
      s[i][k]=min(s[i][k-1],s[i+(1<<k-1)][k-1]);
  solve(1,n,0);
  printf("%d\n",ans);
  return 0;
}
