#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

const int N=25010;

inline char nc(){
  static char buf[100000],*p1=buf,*p2=buf;
  return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}

inline void read(int &x){
  char c=nc(); x=0;
  for(;c>'9'||c<'0';c=nc());for(;c>='0'&&c<='9';x=x*10+c-'0',c=nc());
}

int n,a[N],f[N],mx;

int main(){
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  int T; read(T);
  while(T--){
    read(n); mx=0;
    for(int i=1;i<=n;i++) read(a[i]),mx=max(mx,a[i]);
    for(int i=0;i<=mx;i++) f[i]=0;
    sort(a+1,a+1+n); int ans=0; f[0]=1;
    for(int i=1;i<=n;i++){
      if(f[a[i]]) continue;
      ans++;
      for(int j=0;j+a[i]<=mx;j++)
	f[j+a[i]]|=f[j];
    }
    printf("%d\n",ans);
  }
  return 0;
}
