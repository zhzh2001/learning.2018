#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

const int N=100010;

inline char nc(){
  static char buf[100000],*p1=buf,*p2=buf;
  return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}

inline void read(int &x){
  char c=nc(); x=0;
  for(;c>'9'||c<'0';c=nc());for(;c>='0'&&c<='9';x=x*10+c-'0',c=nc());
}

int n,m,cnt,G[N];
struct edge{
  int t,nx,l;
}E[N<<1];

inline void addedge(int x,int y,int z){
  E[++cnt].t=y; E[cnt].nx=G[x]; E[cnt].l=z; G[x]=cnt;
  E[++cnt].t=x; E[cnt].nx=G[y]; E[cnt].l=z; G[y]=cnt;
}

int lim,cur,g[N],Q1[N],Q[N],vis[N],t;

int dfs(int x,int f){
  for(int i=G[x];i;i=E[i].nx)
    if(E[i].t!=f) dfs(E[i].t,x);
  t=0;
  for(int i=G[x];i;i=E[i].nx)
    if(E[i].t!=f){
      if(g[E[i].t]+E[i].l>=lim){
	cur++; continue;
      }
      Q[++t]=g[E[i].t]+E[i].l;
    }
  sort(Q+1,Q+1+t); int mx=0;
  for(int i=1,j=t;i<=t;i++){
    if(j<=i) break;
    if(Q[i]+Q[j]>=lim){
      mx++; j--;
    }
  }
  g[x]=0; cur+=mx;
  int l=1,r=t,mid,res=-1;
  while(l<=r){
    mid=l+r>>1; int t1=0,cc=0;
    for(int i=1;i<=t;i++)
      if(i!=mid) Q1[++t1]=Q[i];
    for(int i=1,j=t1;i<=t1;i++){
      if(j<=i) continue;
      if(Q1[i]+Q1[j]>=lim){
	cc++; j--;
      }
    }
    if(cc==mx) l=(res=mid)+1;
    else r=mid-1;
  }
  if(res>=0) g[x]=Q[res];
}

inline int check(int x){
  lim=x; cur=0;
  dfs(1,0);
  return cur;
}

int main(){
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  read(n); read(m);
  for(int i=1,x,y,z;i<n;i++)
    read(x),read(y),read(z),addedge(x,y,z);
  int l=1,r=n*10000,mid,res;
  while(l<=r) check(mid=l+r>>1)>=m?l=(res=mid)+1:r=mid-1;
  printf("%d\n",res);
  return 0;
}
