#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
int edgenum,x,y,t,n,m,H,nex[50005],head[50005],vet[50005],net[50005],f[50005],h[50005],MAX[50005],lep[50005],p[50005];
int l,r;
void addedge(int u,int v,int len){
	edgenum++;
	nex[edgenum]=head[u];
	vet[edgenum]=v;
	net[edgenum]=len;
	head[u]=edgenum;
}
void DFS(int x,int qwe){
	h[x]=qwe;
	if(qwe>H)
		H=qwe;
	for(int i=head[x];i;i=nex[i]){
		if(h[vet[i]]==0)
			DFS(vet[i],qwe+1);
	}
}
bool pd(int s){
	for(int i=1;i<=n;i++){
		f[i]=0;MAX[i]=0;
	}
	for(int i=1;i<=t;i++){
		int u=p[i],qwer=0;
		for(int j=head[u];j;j=nex[j]){
			int v=vet[j],len=net[j];
			if(h[v]>h[u]){
				f[u]+=f[v];
				if(MAX[v]+len>=s){
					f[u]++;
					continue;
				}
				qwer++;
				lep[qwer]=MAX[v]+len;
			}
		}
		sort(lep+1,lep+qwer+1);
		int L=1,R=qwer;
		while(L<R){
			if(lep[L]+lep[R]>=s){
				L++;R--;f[u]++;
			}else{
				MAX[u]=lep[L];
				L++;
			}
		}
		if(L==R) {
			MAX[u]=lep[L];
		}
	}
	if(f[1]>=m)
		return true;
	return false;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=0;i<n-1;i++){
		scanf("%d%d%d",&x,&y,&l);
		addedge(x,y,l);
		addedge(y,x,l);
		r+=l;
	}
	DFS(1,1);
	for(int i=H;i>=1;i--){
		for(int j=1;j<=n;j++)
			if(h[j]==i){
				t++;
				p[t]=j;
			}
	}
	l=1;
	while(l<r){
		int MID=(l+r+1)/2;
		if(pd(MID)){
			l=MID;
		}else r=MID-1;
	}
	printf("%d",l);
}
