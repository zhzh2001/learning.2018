#include<iostream>
#include<cstdio>
using namespace std;
int T,n,f[50005],a[506];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=25000;i++)
			f[i]=-1000000;
		int sum=0,MAX=0;
		for(int i=1;i<=n;i++){
			scanf("%d",&a[i]);
			sum+=a[i];
			if(a[i]>MAX)
				MAX=a[i];
		}
		f[0]=0;
		for(int i=1;i<=n;i++){
			for(int j=a[i];j<=MAX;j++){
				for(int ni=1;ni*a[i]<=j;ni*=2){
						f[j]=max(f[j],f[j-ni*a[i]]+1);
				}
			}
		}
		int ans=n;
		for(int i=1;i<=n;i++){
			if(f[a[i]]>1)
				ans--;
		}
		printf("%d\n",ans);
	}
	return 0;
}
