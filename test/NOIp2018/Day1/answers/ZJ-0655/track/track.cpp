#include<bits/stdc++.h>
using namespace std;
const int M=5e4+5;
bool mmm1;
int n,m,num[M];
int h[M],to[M<<1],v[M<<1],nx[M<<1],tot;
inline void add(int a,int b,int c){
	to[++tot]=b;v[tot]=c;
	nx[tot]=h[a];h[a]=tot;
}
struct P20m_is_1{
	int mxdis,id;
	void dfs(int x,int f,int d){
		if(mxdis<d)mxdis=d,id=x;
		for(int i=h[x];i;i=nx[i]){
			int y=to[i];
			if(y==f)continue;
			dfs(y,x,d+v[i]);
		}
	}
	void solve(){
		mxdis=-1;dfs(1,0,0);
		mxdis=-1;dfs(id,0,0);
		printf("%d\n",mxdis);
	}
}p20_m_is_1;
struct P20_a_is_1{
	int res[M];
	void solve(){
		int cnt=0;
		for(int i=h[1];i;i=nx[i]){
			res[++cnt]=v[i];
		}
		sort(res+1,res+1+cnt);
		int ans=2e9;
		for(int i=n-1;i>=1;i--){
			int ret=res[i];
			if(i>=m*2)ret+=res[i-m*2+1];
			ans=min(ans,ret);
			m--;
			if(!m)break;
		}
		printf("%d\n",ans);
	}
}p20_a_is_1;
struct P_5_lian{
	int dp[1005][1005],sum[1005];//表示用到第几个，用了几段 
	void solve(){
		for(int i=2;i<=n;i++)sum[i]=sum[i-1]+num[i-1];
		for(int i=2;i<=n;i++)dp[i][1]=sum[i];
		for(int j=2;j<=m;j++){
			for(int i=1;i<=n;i++){
				for(int k=1;k<i;k++)
					dp[i][j]=max(dp[i][j],min(dp[k][j-1],sum[i]-sum[k]));
			}
		}
		printf("%d\n",dp[n][m]);
	}
}p_5_lian;
bool mmm2;
int main(){
	//printf("%lf\n",(&mmm2-&mmm1)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool fl=0;
	for(int a,b,c,i=1;i<n;i++){
		scanf("%d%d%d",&a,&b,&c);
		add(a,b,c);add(b,a,c);
		if(a!=1&&b!=1)fl=1;
		num[min(a,b)]=c;
	}
	if(m==1)p20_m_is_1.solve();
	else if(!fl)p20_a_is_1.solve();
	else p_5_lian.solve();
	return 0;
}
