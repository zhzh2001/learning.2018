#include <bits/stdc++.h>
using namespace std;
const int maxn=50005;
int n,m,head[maxn],tot,len[maxn],end;
bool use[maxn];
struct E{int to,len,nxt;}edge[maxn*2];
int M,Ans;
bool cmp(int a,int b){return a>b;}
void init(){memset(head,0,sizeof(head)),tot=0;}
void makedge(int u,int v,int w){edge[++tot].to=v,edge[tot].len=w,edge[tot].nxt=head[u],head[u]=tot;}
int dfs(int u,int fa){
	int start=end+1;
	for (int i=head[u],v;i;i=edge[i].nxt){
		v=edge[i].to; if (v==fa) continue;
		len[++end]=dfs(v,u)+edge[i].len;
	}
	if (start>end) return 0;
	sort(len+start,len+end+1,cmp);
	int i=start,j=end,cnt=0,Cnt;
	while (i<=j && len[i]>=M) cnt++,i++;
	while (i<j)
		if (len[i]+len[j]>=M) cnt++,i++,j--; else j--;
	Ans+=cnt;
	int L=start,R=end,mid,res=0;
	while (L<=R){
		mid=(L+R)>>1;
			i=start,j=end,Cnt=0;
			while (i<=j && len[i]>=M){
				if (i==mid){i++;continue;}
				if (j==mid){j--;continue;}
				Cnt++,i++;
			}
			while (i<j){
				if (i==mid){i++;continue;}
				if (j==mid){j--;continue;} 
				if (len[i]+len[j]>=M) Cnt++,i++,j--; else j--;
			}
		if (cnt==Cnt) res=len[mid],R=mid-1; else L=mid+1;
	}
	end=start-1;
	return res;
}
bool check(int x){
	M=x,Ans=0,dfs(1,0);
	return Ans>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m),init();
	int Len=0;
	for (int i=1,u,v,w;i<n;i++)
		scanf("%d%d%d",&u,&v,&w),makedge(u,v,w),makedge(v,u,w),Len+=w;
	int L=1,R=Len/m,mid,ans=1;
	while (L<=R){
		mid=(L+R)>>1;
		if (check(mid)) ans=mid,L=mid+1; else R=mid-1;
	}
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
