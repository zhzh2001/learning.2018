#include <bits/stdc++.h>
using namespace std;
const int maxn=100005;
int n,a[maxn],Min[maxn][20],ans=0;
int getmin(int L,int R){
	int y=(int)log2(R-L+1);
	if (a[Min[L][y]]<a[Min[R-(1<<y)+1][y]])
		return Min[L][y];
	else
		return Min[R-(1<<y)+1][y];
}
void solve(int L,int R,int deep){
	int k=getmin(L,R);
	ans+=a[k]-deep;
	if (k>L) solve(L,k-1,a[k]);
	if (k<R) solve(k+1,R,a[k]);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]),Min[i][0]=i;
	for (int j=1;(1<<j)<=n;j++)
	for (int i=1;i+(1<<j)-1<=n;i++)
	if (a[Min[i][j-1]]<a[Min[i+(1<<j-1)][j-1]])
		Min[i][j]=Min[i][j-1];
	else
		Min[i][j]=Min[i+(1<<j-1)][j-1];
	solve(1,n,0);
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
