#include <bits/stdc++.h>
using namespace std;
int n,ans,a[105],now;
bool dp[25005];
int gcd(int a,int b){if (!b) return a; else return gcd(b,a%b);}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--){
		scanf("%d",&n),ans=n;
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		int GCD=a[1];
		for (int i=2;i<=n;i++) GCD=gcd(GCD,a[i]);
		for (int i=1;i<=n;i++) a[i]/=GCD;
		sort(a+1,a+n+1),now=1;
		memset(dp,0,sizeof(dp)),dp[0]=1;
		for (int i=0;i<=a[n];i++){
			for (int j=1;j<=n;j++)
				if (i>a[j]) dp[i]|=dp[i-a[j]];
			if (i==a[now]){
				if (dp[i]) ans--; else dp[i]=1;
				now++;
			}
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
