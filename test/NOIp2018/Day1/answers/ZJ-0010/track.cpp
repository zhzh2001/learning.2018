#include <cstdio>
#include <cstring>
#include <algorithm>
#include <set>
using namespace std;

const int N = 5e4 + 5;
const int inf = 1 << 30;

int n, m, cnt, lim, tot = 0, head[N], f[N];
multiset <int> s[N];

struct Edge {
	int to, nxt, val;
} e[N << 1];

inline void add(int from, int to, int val) {
	e[++tot].to = to;
	e[tot].val = val;
	e[tot].nxt = head[from];
	head[from] = tot;
}

inline void read(int &X) {
	X = 0; char ch = 0; int op = 1;
	for(; ch > '9' || ch < '0'; ch = getchar())
		if(ch == '-') op = -1;
	for(; ch >= '0' && ch <= '9'; ch = getchar())
		X = (X << 3) + (X << 1) + ch - 48;
	X *= op;
}

inline void chkMax(int &x, int y) {
	if(y > x) x = y;
}

/*  inline int getPos(int which, int siz, int val) {
	val = lim - val;
	int ln = 0, rn = siz, mid, res = -1;
	for(; ln <= rn; ) {
		mid = ((ln + rn) >> 1);
		if(vec[which][mid] >= val) res = mid, rn = mid - 1;
		else ln = mid + 1;
	}	
	return res;
}   */

/*   void dfs(int x, int fat) {
	vec[x].clear();
	for(int i = head[x]; i; i = e[i].nxt) {
		int y = e[i].to;
		if(y == fat) continue;
		dfs(y, x);
		vec[x].push_back(f[y] + e[i].val);
	}
	
	sort(vec[x].begin(), vec[x].end());
	int vecSiz = vec[x].size() - 1;
	for(; vecSiz; --vecSiz) {
		if(vecSiz == -1) break;
		if(vec[x][vecSiz] < lim) break;
		++cnt;
	}
	for(int i = 0; i <= vecSiz; i++) tag[i] = 0;
	for(int i = vecSiz; i > 0; i--) {
		int pos = getPos(x, i - 1, vec[x][i]);
		if(pos != -1) tag[pos] = 1, tag[i] = 1, ++cnt;
	}
	
	int res = 0;
	for(int i = 0; i <= vecSiz; i++)
		if(tag[i]) tag[i] = 0;
		else chkMax(res, vec[x][i]);
	
	f[x] = res;
}    */

void dfs(int x, int fat) {
//	s[x].clear();
	for(int i = head[x]; i; i = e[i].nxt) {
		int y = e[i].to;
		if(y == fat) continue; 
		dfs(y, x);
		s[x].insert(f[y] + e[i].val);
	} 
	
	for(; !s[x].empty(); ) {
		multiset <int> :: iterator it = (--s[x].end());
		if((*it) >= lim) {
			++cnt;
			s[x].erase(it);
		} else break;
	}
	
	int res = 0;
	for(; !s[x].empty(); ) {
		multiset <int> :: iterator p1 = s[x].begin();
		int tmp = (*p1);
		s[x].erase(p1);		
		multiset <int> :: iterator p2 = s[x].lower_bound(lim - tmp);

		if(p2 == s[x].end()) {
			chkMax(res, tmp);
		} else {
			cnt++;
			s[x].erase(p2); 
		}
	}
	
	f[x] = res;
}

inline bool chk(int mid) {
	lim = mid, cnt = 0;
	for(int i = 1; i <= n; i++) f[i] = 0;
	dfs(1, 0);
	return cnt >= m;
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	
	read(n), read(m);
	int ln = 0, rn = 0, mid, res = inf;
	for(int x, y, v, i = 1; i < n; i++) {
		read(x), read(y), read(v);
		add(x, y, v), add(y, x, v);
		rn += v;
	}
	
	for(; ln <= rn; ) {
		mid = (ln + rn) / 2;
		if(chk(mid)) ln = mid + 1, res = mid;
		else rn = mid - 1;
	}   
	
	printf("%d\n", res);
	return 0;
}
