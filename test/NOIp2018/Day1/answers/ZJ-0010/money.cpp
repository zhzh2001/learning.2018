#include <cstdio>
#include <cstring>
using namespace std;

const int N = 105;
const int M = 25005;
const int Lg = 20;

int testCase, n, mx = 0, a[N], ans;
bool f[Lg][M];

inline void read(int &X) {
	X = 0; char ch = 0; int op = 1;
	for(; ch > '9' || ch < '0'; ch = getchar())
		if(ch == '-') op = -1;
	for(; ch >= '0' && ch <= '9'; ch = getchar())
		X = (X << 3) + (X << 1) + ch - 48;
	X *= op;
}

inline void chkMax(int &x, int y) {
	if(y > x) x = y;
}

void solve(int l, int r, int dep) {
	if(l == r) {
		if(f[dep][a[l]]) ans--;
		return;
	}
	
	++dep;
	for(int i = 0; i <= mx; i++) f[dep][i] = f[dep - 1][i];
	
	int mid = ((l + r) >> 1);
	for(int i = l; i <= mid; i++)
		for(int j = a[i]; j <= mx; j++)
			f[dep][j] |= f[dep][j - a[i]];
	solve(mid + 1, r, dep);
	
	for(int i = 0; i <= mx; i++) f[dep][i] = f[dep - 1][i];
	for(int i = mid + 1; i <= r; i++)
		for(int j = a[i]; j <= mx; j++)
			f[dep][j] |= f[dep][j - a[i]];
	solve(l, mid, dep);
}

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	
	for(read(testCase); testCase--; ) {
		read(n);
		mx = 0;
		for(int i = 1; i <= n; i++) {
			read(a[i]);
			chkMax(mx, a[i]);
		}
		
		for(int i = 1; i <= mx; i++) f[0][i] = 0;
		f[0][0] = 1;
		
		ans = n;
		solve(1, n, 0);
		 
		printf("%d\n", ans);
	}	
	return 0;
}
