#include <cstdio>
#include <cstring>
using namespace std;
typedef long long ll;

const int N = 1e5 + 5;

int n;
ll a[N], b[N];

template <typename T>
inline void read(T &X) {
	X = 0; char ch = 0; T op = 1;
	for(; ch > '9' || ch < '0'; ch = getchar())
		if(ch == '-') op = -1;
	for(; ch >= '0' && ch <= '9'; ch = getchar())
		X = (X << 3) + (X << 1) + ch - 48;
	X *= op;
}

ll abs(ll x) {
	return x > 0 ? x : -x;
}

inline ll max(ll x, ll y) {
	return x > y ? x : y;
}

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	
	read(n);
	for(int i = 1; i <= n; i++) {
		read(a[i]);
		b[i] = a[i] - a[i - 1];
	}
	
/*	for(int i = 1; i <= n; i++)
		printf("%lld ", b[i]);
	printf("\n");   */
	
	ll ps = 0LL, ne = 0LL;
	for(int i = 1; i <= n; i++) {
		if(b[i] > 0) ps += b[i];
		else ne -= b[i];
	}
	
	printf("%lld\n", max(ne, ps));
	return 0;
}
