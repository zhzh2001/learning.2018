#include<bits/stdc++.h>
using  namespace std;
#define ll long long
#define ul unsigned long long
#define MAXN 999999999
int n;
int s[100010];
int cnt=0;
void doit(int l,int r)
{
	if(l>r)
		return;
	int min_=MAXN,tmp;
	for(int i=l; i<=r; i++)
	{
		if(s[i]<min_)
		{
			min_=s[i];
			tmp=i;
		}
	}
	cnt+=s[tmp];
	for(int i=l; i<=r; i++)
	{
		s[i]-=min_;
	}
	doit(l,tmp-1);
	doit(tmp+1,r);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1; i<=n; i++)
		scanf("%d",&s[i]);
	doit(1,n);
	printf("%d\n",cnt);
	return 0;
}
