#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
using namespace std;
const int maxn=1e5+10;
typedef long long LL;

LL a[maxn];
LL diff[maxn];
int n;

template<typename T> inline void chkmin(T &a,T b){if (a>b) a=b;}

template<typename _Tp>
void read(_Tp & x){
	x=0;
	char ch=getchar(),lst=' ';
	while (!isdigit(ch)) {lst=ch; ch=getchar();}
	while (isdigit(ch)) {x=(x<<1)+(x<<3)+(ch&15);ch=getchar();}
	if (lst=='-') x=-x;
}


int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	LL ans=0; 
	for (int i=1;i<=n;++i) read(a[i]);
	for (int i=1;i<=n;++i) diff[i]=a[i]-a[i-1];
	for (int i=1;i<=n;++i) if (diff[i]>0) ans+=diff[i];
	printf("%lld",ans);
	return 0;
}
