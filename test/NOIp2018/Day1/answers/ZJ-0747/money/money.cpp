#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
using namespace std;
const int maxn=1e5+10;
const int N=2010;
typedef long long LL;

LL a[maxn];
LL diff[maxn];
//bool notprime[maxn];
//int prime[maxn],tot;
bool dp[maxn];
bool able[maxn];
int chose[maxn];
int n;

template<typename T> inline void chkmin(T &a,T b){if (a>b) a=b;}

template<typename _Tp>
void read(_Tp & x){
	x=0;
	char ch=getchar(),lst=' ';
	while (!isdigit(ch)) {lst=ch; ch=getchar();}
	while (isdigit(ch)) {x=(x<<1)+(x<<3)+(ch&15);ch=getchar();}
	if (lst=='-') x=-x;
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	read(T);
	/*for (int i=2;i<=N;++i){
		if (!notprime[i]) prime[++tot]=i;
		for (int j=1;j<=tot && prime[j]*i<=N;++j){
			notprime[prime[j]*i]=true;
			if (i%prime[j]==0) break;
		}
	}	*/
	while (T--){
		int ans=0;
		read(n);
		for (int i=1;i<=n;++i) read(a[i]);
		if (n==1) {printf("1\n"); continue;}
		memset(dp,0,sizeof(dp));
		
		dp[0]=true;  //can get
		for (int i=1;i<=n;++i)
		  for (int j=a[i];j<=N;++j)
		    dp[j]|=dp[j-a[i]];
		    if ( dp[1]==true ) { printf("1\n"); continue;}
		    
		  
	      for (int i=2;i<=N;++i){
	      	if (dp[i]){
	      		 chose[++ans]=i;
	      		 memset(able,0,sizeof(able));
	      		 able[0]=true;
	      		 for (int j=1;j <=ans;++j)
	      		   for (int l=chose[i];l<=N;++l)
	      		      able[l]|=able[l-chose[j]];
	      		  for (int i=1;i<=N;++i)
					  if (dp[i] && able[i]) dp[i]=false;     
			  }
	      	
		  }
		  printf("%d\n",ans);
	}
	return 0;
}
