#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<queue>
using namespace std;
const int maxn=5e4+10;
typedef long long LL;

LL d[maxn];
int f[maxn];
int n,m,from,to,dist,cnt;
int set[maxn];
int Ed=1;

struct Edge{
	int to,nxt,dist;
}edges[maxn<<1];
int head[maxn],cte=0;

template<typename T> inline void chkmax(T &a,T b){if (a<b) a=b;}

template<typename _Tp>
void read(_Tp & x){
	x=0;
	char ch=getchar(),lst=' ';
	while (!isdigit(ch)) {lst=ch; ch=getchar();}
	while (isdigit(ch)) {x=(x<<1)+(x<<3)+(ch&15);ch=getchar();}
	if (lst=='-') x=-x;
}

void addedge(int from,int to,int dist){
	edges[++cte]=(Edge){to,head[from],dist};
	head[from]=cte;
}

void dfs(int x,int fa){
	f[x]=fa;
	for (int i=head[x];i;i=edges[i].nxt){
		Edge &e=edges[i];
		if (e.to!=fa) {
			d[e.to]=d[x]+e.dist;
			dfs(e.to,x);
		}
	}
}

bool check(int mid){
	int sum=0;
	int now=1;
	int cnt=0;
	while (now!=Ed){
      sum=0;
	  while (sum<mid && now!=Ed){
	  	
		for (int i=head[now];i;i=edges[i].nxt){
			    Edge &e=edges[i];
				sum+=e.dist;
				now=e.to;
				break;
		  }
		
	  }
	  cnt++;
   }
   if (sum<mid) cnt--;
   if (cnt<=m) return false;
   else return true;
}


int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);
	for (int i=1;i<n;++i){
		read(from);read(to);read(dist);
		addedge(from,to,dist);
		addedge(to,from,dist);
	}
	if (m==1) {
		dfs(1,0);
		int longest=1;
		for (int i=1;i<=n;++i){
			if (d[i]>d[longest]) longest=i;
		}
		memset(d,0,sizeof(d));
		dfs(longest,0);
		int ans=1;
		for (int i=1;i<=n;++i) 
		  if (d[i]>d[ans]) ans=i;
		printf("%lld",d[ans]);
		
	}else {
	   dfs(1,0);
	   for (int i=1;i<=n;++i)
	     if (d[Ed]<d[i]) Ed=i;
	   LL l=0,r=0;
	   for (int i=1;i<=cte;++i)
	     r+=edges[i].dist;
	     r/=2;
	    while (l<r){
	    	LL mid=(l+r)>>1;
	    	if (check(mid)) l=mid+1;
	    	else r=mid;
		}
		printf("%lld",l); 
	}
	
	return 0;
}
