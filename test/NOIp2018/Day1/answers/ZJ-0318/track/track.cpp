#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
#include <vector>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 50005
int n, m, bo = 1;
struct Edge{
	int u, v, w;
	bool operator < (const Edge &res) const {
		return u < res.u;
	}
}a[N];
int edge, to[N << 1], pr[N << 1], tw[N << 1], hd[N];
void addedge(int u, int v, int w){
	to[++edge] = v, tw[edge] = w, pr[edge] = hd[u], hd[u] = edge;
}
namespace M_eq_1{
	int dp[N], ans;
	void dfs(int u, int fa = 0){
		int mx = 0, cmx = 0;
		for (register int i = hd[u], v, w; i; i = pr[i])
			if ((v = to[i]) != fa){
				dfs(v, u), w = dp[v] + tw[i];
				if (w > mx) cmx = mx, mx = w;
				else if (w > cmx) cmx = w;
			}
		ans = std :: max(ans, mx + cmx), dp[u] = mx;
	}
	void Main(){
		for (register int i = 1, u, v, w; i < n; ++i) 
			u = read(), v = read(), w = read(), addedge(u, v, w), addedge(v, u, w);
		ans = 0, dfs(1);
		printf("%d", ans);
	}
}
namespace Flower{
	int l, r, mid, ans;
	bool cmp(Edge x, Edge y){
		return x.w < y.w;
	}
	int check(int x){
		int h = 1, t = n - 1, s = m;
		while (a[t].w >= x) --t, --s;
		while (h < t)
			if (a[h].w + a[t].w >= x) ++h, --t, --s;
			else ++h;
		return s <= 0;
	}
	void Main(){
		std :: sort(a + 1, a + n, cmp);
		l = 0, r = 0, ans = 0;
		for (register int i = 1; i < n; ++i) r += a[i].w;
		r /= m;
		while (l <= r) check(mid = (l + r) >> 1) ? ans = mid, l = mid + 1 : r = mid - 1;
		printf("%d", ans);
	}
}
namespace Link{
	int l, r, mid, ans;
	int check(int x){
		int s = 0, sw = 0;
		for (register int i = 1; i < n; ++i){
			sw += a[i].w;
			if (sw >= x) sw = 0, ++s;
		}
		return s >= m;
	}
	void Main(){
		l = 0, r = 0, ans = 0;
		for (register int i = 1; i < n; ++i) r += a[i].w;
		r /= m;
		while (l <= r) check(mid = (l + r) >> 1) ? ans = mid, l = mid + 1 : r = mid - 1;
		printf("%d", ans);
	}
}
namespace Fake{
	int l, r, mid, ans, s, d[N], rt = 1;
	int dfs(int x, int u, int fa = 0){
		std :: vector<int> a;
		for (register int i = hd[u], v; i; i = pr[i])
			if ((v = to[i]) != fa) a.push_back(dfs(x, v, u) + tw[i]);
		std :: sort(a.begin(), a.end());
		int h = 0, t = a.size() - 1, mx = 0;
		while (t >= 0 && a[t] >= x) --t, ++s;
		while (h < t)
			if (a[h] + a[t] >= x) ++h, --t, ++s;
			else mx = std :: max(mx, a[h]), ++h;
		if (h == t) mx = std :: max(mx, a[h]);
		return mx;
	}
	bool check(int x){
		s = 0, dfs(x, rt);
		return s >= m;
	}
	void Main(){
		for (register int i = 1; i < n; ++i) 
			addedge(a[i].u, a[i].v, a[i].w), addedge(a[i].v, a[i].u, a[i].w), ++d[a[i].u], ++d[a[i].v];
		for (register int i = 1; i <= n; ++i) if (d[i] > 2) { rt = i; break; }
		l = 0, r = 0, ans = 0;
		for (register int i = 1; i < n; ++i) r += a[i].w;
		r /= m;
		while (l <= r) check(mid = (l + r) >> 1) ? ans = mid, l = mid + 1 : r = mid - 1;
		printf("%d", ans);
	}
}
int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	n = read(), m = read();
	if (m == 1) return M_eq_1 :: Main(), 0;
	for (register int i = 1; i < n; ++i)
		a[i].u = read(), a[i].v = read(), a[i].w = read(), bo &= a[i].u + 1 == a[i].v;
	std :: sort(a + 1, a + n);
	if (a[n - 1].u == 1) return Flower :: Main(), 0;
	else if (bo) return Link :: Main(), 0;
	else return Fake :: Main(), 0;
}
