#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
int T, n, a[105], m, dp[25005], ans;
int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	T = read();
	while (T--){
		ans = n = read();
		for (register int i = 1; i <= n; ++i) a[i] = read();
		std :: sort(a + 1, a + 1 + n), m = a[n];
		memset(dp, 0, sizeof dp);
		dp[0] = 1;
		for (register int i = 1; i <= n; ++i){
			if (dp[a[i]]) --ans;
			for (register int j = a[i]; j <= m; ++j)
				dp[j] |= dp[j - a[i]];
		}
		printf("%d\n", ans);
	}
	return 0;
}
