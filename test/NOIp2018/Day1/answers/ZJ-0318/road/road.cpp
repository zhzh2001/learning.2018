#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
int n, a[N], st[N][25], Log[N], ans;
int Min(int x, int y){ return a[x] < a[y] ? x : y; }
int query(int l, int r){
	int t = Log[r - l + 1];
	return Min(st[l][t], st[r - (1 << t) + 1][t]);
}
void solve(int l, int r, int h){
	if (l > r) return;
	int mid = query(l, r);
	ans += a[mid] - h;
	solve(l, mid - 1, a[mid]), solve(mid + 1, r, a[mid]);
}
int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	n = read();
	for (register int i = 1; i <= n; ++i) a[i] = read(), st[i][0] = i;
	Log[1] = 0;
	for (register int i = 2; i <= n; ++i) Log[i] = Log[i >> 1] + 1; 
	for (register int j = 1; j <= Log[n]; ++j)
		for (register int i = 1; i + (1 << j) - 1 <= n; ++i)
			st[i][j] = Min(st[i][j - 1], st[i + (1 << (j - 1))][j - 1]);
	solve(1, n, 0);
	printf("%d", ans);
	return 0;
}
