#include<cstdio>
using namespace std;
const int maxn = 100005;
int d[maxn];
int read()
{
	char ch = getchar();
	while(ch < '0' || ch > '9') ch = getchar();
	int res = 0;
	while(ch >= '0' && ch <= '9') res = (res << 3) + (res << 1) + (ch ^ 48) , ch = getchar();
	return res;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n = read();
	for(int i = 1;i <= n;i++) d[i] = read();
	int ans = 0;
	d[0] = 0;
	for(int i = 1;i <= n;i++)
		if(d[i] > d[i - 1]) ans += d[i] - d[i - 1];
	printf("%d\n",ans);
	return 0;
}
