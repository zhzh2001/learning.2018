#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn = 105;
const int maxa = 25005;
int a[maxn] , b[maxn];
bool f[maxa];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int n;
		scanf("%d",&n);
		for(int i = 1;i <= n;i++) scanf("%d",&a[i]);
		sort(a + 1 , a + n + 1);
		int max_a = a[n];
		f[0] = 1;
		for(int i = 1;i <= max_a;i++) f[i] = 0;
		int m = 0;
		for(int i = 1;i <= n;i++)
		{
			if(f[a[i]]) continue;
			b[++m] = a[i];
			for(int j = b[m];j <= max_a;j++) f[j] |= f[j - b[m]];
		}
		printf("%d\n",m);
	}
	return 0;
}
