#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn = 50005;
const int maxe = 50005;
int n , m;
int edgenum;
int Next[maxe << 1] , vet[maxe << 1] , val[maxe << 1] , head[maxn];
int max(int x , int y){return x > y ? x : y;}
int read()
{
	char ch = getchar();
	while(ch < '0' || ch > '9') ch = getchar();
	int res = 0;
	while(ch >= '0' && ch <= '9') res = (res << 3) + (res << 1) + (ch ^ 48) , ch = getchar();
	return res;
}
void add_edge(int u , int v , int cost)
{
	edgenum++;
	Next[edgenum] = head[u];
	vet[edgenum] = v;
	val[edgenum] = cost;
	head[u] = edgenum;
}
namespace td
{
	int f[maxn][2];
	void dfs(int u , int fa)
	{
		f[u][0] = f[u][1] = 0;
		for(int e = head[u];e;e = Next[e])
		{
			int v = vet[e];
			if(v == fa) continue;
			dfs(v , u);
			if(f[v][0] + val[e] > f[u][0]) f[u][1] = f[u][0] , f[u][0] = f[v][0] + val[e];
			else if(f[v][0] + val[e] > f[u][1]) f[u][1] = f[v][0] + val[e];
		}
	}
	void main()
	{
		dfs(1 , 0);
		int ans = 0;
		for(int i = 1;i <= n;i++) ans = max(ans , f[i][0] + f[i][1]);
		printf("%d\n",ans);
	}
}
namespace csm
{
	int E[maxe];
	bool check(int len)
	{
		int cnt = 0;
		int r;
		for(r = n - 1;r >= 1;r--)
			if(E[r] >= len) cnt++;
			else break;
		int l = 1;
		while(l < r)
		{
			if(E[l] + E[r] < len) l++;
			else cnt++ , l++ , r--;
		}
		return cnt >= m;
	}
	void main()
	{
		for(int i = 1;i < n;i++) E[i] = val[i << 1];
		sort(E + 1 , E + n);
		int l = m , r = 500000000;
		while(l < r)
		{
			int mid = l + r + 1 >> 1;
			if(check(mid)) l = mid;
			else r = mid - 1;
		}
		printf("%d\n",r);
	}
}
namespace seq
{
	int a[maxe];
	bool check(int l)
	{
		int cnt = 0 , sum = 0;
		for(int i = 1;i < n;i++)
		{
			sum += a[i];
			if(sum >= l) sum = 0 , cnt++;
		}
		return cnt >= m;
	}
	void main()
	{
		for(int i = 1;i < n;i++) a[i] = val[i << 1];
		int l = m , r = 500000000;
		while(l < r)
		{
			int mid = l + r + 1 >> 1;
			if(check(mid)) l = mid;
			else r = mid - 1;
		}
		printf("%d\n",r);
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n = read() , m = read();
	bool sub1 = 1 , sub2 = 2;
	for(int i = 1;i < n;i++)
	{
		int u = read() , v = read() , cost = read();
		sub1 &= u == 1 , sub2 &= u + 1 == v;
		add_edge(u , v , cost);
		add_edge(v , u , cost);
	}
	if(m == 1) td::main();
	else if(sub1) csm::main();
	else if(sub2) seq::main();
	return 0;
}
