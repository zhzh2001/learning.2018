#include<bits/stdc++.h>
using namespace std;
const int N = 105, M = 25001;
int T, n, a[N];
bool v[M];
int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	while (T--) {
		scanf("%d", &n);
		for (int i = 1; i <= n; ++i) scanf("%d", a + i);
		sort(a + 1, a + n + 1);
		int ans = 0, mx = a[n];
		memset(v, 0, (mx + 1) * sizeof *v); v[0] = 1;
		for (int i = 1; i <= n; ++i) if (!v[a[i]]) {
			++ans;
			for (int j = a[i]; j <= mx; ++j)
				v[j] |= v[j - a[i]];
		}
		printf("%d\n", ans);
	}
	return 0;
}
