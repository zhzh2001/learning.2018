#include<bits/stdc++.h>
using namespace std;
const int N = 5e4 + 10;
int n, m, lim, pe, fu[N];
int tot, fi[N], a[N << 1], ne[N << 1], c[N << 1];
inline void Add(int x, int y, int z) {
	a[++tot] = y; ne[tot] = fi[x]; fi[x] = tot; c[tot] = z;
}
int val[N];
inline int Calc(int n, int ban) {
	int ans = 0, l = 1;
	for (int i = n; i >= l; --i) {
		if (i == ban) continue;
		if (val[i] >= lim) {
			++ans;
		} else {
			while (l == ban || (l < i && val[i] + val[l] < lim)) ++l;
			if (l >= i) break;
			++ans; ++l;
		}
	}
	return ans;
}
inline void Dfs(int x, int fa) {
	for (int i = fi[x]; i; i = ne[i]) if (a[i] != fa) 
		Dfs(a[i], x);
	int top = 0;
	for (int i = fi[x]; i; i = ne[i]) if (a[i] != fa)
		val[++top] = fu[a[i]] + c[i];
	sort(val + 1, val + top + 1);
	int mx = Calc(top, 0);
	pe += mx; fu[x] = 0;
	for (int l = 1, r = top; l <= r; ) {
		int mid = (l + r) >> 1;
		if (Calc(top, mid) == mx) fu[x] = val[mid], l = mid + 1;
		else r = mid - 1;
	}
}
int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1, x, y, z; i < n; ++i) {
		scanf("%d%d%d", &x, &y, &z);
		Add(x, y, z); Add(y, x, z);
	}
	int ans = 0;
	for (int l = 0, r = n * 10000; l <= r; ) {
		lim = (l + r) >> 1;
		pe = 0;
		Dfs(1, 0);
		if (pe >= m) ans = lim, l = lim + 1;
		else r = lim - 1;
	}
	printf("%d\n", ans);
	return 0;
}
