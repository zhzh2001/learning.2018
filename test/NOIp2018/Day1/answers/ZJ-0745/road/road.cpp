#include<bits/stdc++.h>
using namespace std;
int n;
int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	long long ans = 0;
	for (int i = 1, la = 0, x; i <= n; ++i) {
		scanf("%d", &x);
		if (x > la) ans += x - la;
		la = x;
	}
	printf("%lld\n", ans);
	return 0;
}
