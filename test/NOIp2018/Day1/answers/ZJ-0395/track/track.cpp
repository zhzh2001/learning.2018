#include<bits/stdc++.h>
#define N 51234
using namespace std;
int vis[N],mxi,n,m,xx1,xx2,xx3,x,y,z,dep[N],mx,a[N],l,r,mid,b[N],s,f[N],g[N];
vector <int> v[N],w[N];

int cmp(int a,int b)
{
	return a>b;
}

void dfs(int x)
{
	//printf("%d %d\n",x,v[x].size());
	for (int i=0;i<v[x].size();i++)
	if (!vis[v[x][i]])
	{
		vis[v[x][i]]=1;
		dep[v[x][i]]=w[x][i]+dep[x];
		//printf("%d %d\n",v[x][i],dep[v[x][i]]);
		if (dep[v[x][i]]>mx) mx=dep[v[x][i]],mxi=v[x][i];
		dfs(v[x][i]);
	}
}

int check(int k,int kk)
{
	int sum=0,gs=0;
	for (int i=1;i<=n-1;i++)
	{
		sum=sum+a[i];
		if (sum>=k) sum=0,gs++;
	}
	if (gs>=kk) return 1;
	else return 0;
}

int DFS(int x,int s,int ges)
{
	int ans=0;
	for (int i=0;i<v[x].size();i++)
	if (!vis[v[x][i]])
	{
		vis[v[x][i]]=1;
		//s+=w[x][i];
		if (s+w[x][i]>=ges) ans=DFS(v[x][i],0,ges)+1+DFS(v[x][1-i],0,ges);
		ans=max(ans,DFS(v[x][i],s+w[x][i],ges)+DFS(v[x][1-i],0,ges));
	}
	return ans;
}

int pd(int x)
{
	for (int i=1;i<=n;i++)
	{
		memset(vis,0,sizeof(vis));
		vis[i]=1;
		if (DFS(i,0,x)>=m) return 1;
	}
	return 0;
}

int sx(int x,int s,int ges)
{
	int ans=0,ls=0,lls=0;
	if (s>=ges) lls++,s=0;
	for (int i=0;i<v[x].size();i++)
	if (!vis[v[x][i]])
	{
		vis[v[x][i]]=1;
		//s+=w[x][i];
		f[v[x][i]]=sx(v[x][i],0,ges);
		g[v[x][i]]=sx(v[x][i],s+w[x][i],ges);
		ls+=f[v[x][i]];
	}
	for (int i=0;i<v[x].size();i++)
	{
		ans=max(ans,g[v[x][i]]+ls-f[v[x][i]]);
	}
	return ans+lls;
}

int pdd(int x)
{
	for (int i=1;i<=n;i++)
	{
		memset(vis,0,sizeof(vis));
		memset(f,0,sizeof(f));
		memset(g,0,sizeof(g));
		vis[i]=1;
		if (sx(i,0,x)>=m) return 1;
	}
	return 0;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	xx1=1;xx2=1;xx3=1;
	for (int i=1;i<=n-1;i++) 
	{
		scanf("%d%d%d",&x,&y,&z);
		v[x].push_back(y);w[x].push_back(z);
		v[y].push_back(x);w[y].push_back(z);
		if (x!=1) xx1=0;
		if (y!=x+1) xx2=0; 
		if (v[x].size()>3||v[y].size()>3) xx3=0;
		b[i]=z;
	}
	if (m==1)
	{
		memset(dep,0,sizeof(dep));
		memset(vis,0,sizeof(vis));
		vis[1]=1;mx=0;mxi=0;
		dfs(1);
		//printf("%d %d\n",mx,mxi);
		memset(dep,0,sizeof(dep));
		memset(vis,0,sizeof(vis));
		vis[mxi]=1;mx=0;//mxi=0;
		dfs(mxi);
		printf("%d\n",mx);
	}
	else if (xx1)
	{
		for (int i=2;i<=n;i++) a[i]=w[i][0];
		//for (int i=1;i<=n;i++) printf("%d ",a[i]);putchar('\n');
		sort(a+2,a+n+1,cmp);
		//for (int i=1;i<=n;i++) printf("%d ",a[i]);putchar('\n');
		printf("%d\n",min(a[m+1]+a[m+2],a[m]));
	}
	else if (xx2)
	{
		for (int i=1;i<=n-1;i++)
		for (int j=0;j<v[i].size();j++) if (v[i][j]==i+1) a[i]=w[i][j];
		l=0;r=2143322311;
		for (int i=1;i<=60;i++)
		{
			mid=(l+r)/2;
			if (check(mid,m)) l=mid;
			else r=mid-1;
		}
		printf("%d\n",l);
	}
	else if (m==n-1)
	{
		sort(b+1,b+n,cmp);
		printf("%d\n",b[n-1]);
	}
	else if (xx3)
	{
		mx=0;
		for (int i=1;i<=n;i++) 
		{
			l=1;r=1143322322;
			for (int j=1;j<=60;j++)
			{
				mid=(l+r)/2;
				if (pd(mid)) l=mid;
				else r=mid-1;	
			}	
			if (l>mx) mx=l;
		}
		printf("%d\n",mx);
	}
	else
	{
		mx=0;
		for (int i=1;i<=n;i++) 
		{
			l=1;r=1143322322;
			for (int j=1;j<=60;j++)
			{
				mid=(l+r)/2;
				if (pdd(mid)) l=mid;
				else r=mid-1;	
			}	
			if (l>mx) mx=l;
		}
		printf("%d\n",mx);
	}
	return 0;
}
