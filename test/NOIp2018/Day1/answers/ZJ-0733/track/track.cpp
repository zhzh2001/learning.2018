#include <bits/stdc++.h>
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define per(i,r,l) for(int i=r;i>=l;i--)
#define N 50005
int n,m,a[N],b[N],l[N],s[N],to[N*2],val[N*2];
int L,R,Mid,Ans,mx[N],mxl[N];
int vec[N],cnt,p,discard;
void dfs(int fr,int x){
	rep(i,s[x-1]+1,s[x]){
		int y=to[i];
		if(y==fr) continue;
		dfs(x,y);
	}
	mx[x]=cnt=0;
	rep(i,s[x-1]+1,s[x]){
		int y=to[i];
		if(y==fr) continue;
		mx[x]+=mx[y];
		vec[++cnt]=mxl[y]+val[i];
	}
	std::sort(vec+1,vec+cnt+1);
	while(cnt&&vec[cnt]>=Mid) cnt--,mx[x]++;
	mxl[x]=vec[cnt--];discard=0;
	p=1;while(p<=cnt){
		if(p<cnt&&vec[p]+vec[cnt]>=Mid){
			p++;cnt--;mx[x]++;
		}
		else if(vec[p]+mxl[x]>=Mid){
			p++;mxl[x]=p<=cnt?vec[cnt--]:0;mx[x]++;
		}
		else discard=vec[p++];
	}
	mxl[x]=std::max(mxl[x],discard);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	memset(s,0,sizeof(s));
	rep(i,1,n-1){
		scanf("%d%d%d",&a[i],&b[i],&l[i]);
		s[a[i]]++;s[b[i]]++;
	}
	rep(i,1,n) s[i]+=s[i-1];
	per(i,n,1) s[i]=s[i-1];
	rep(i,1,n-1){
		s[a[i]]++;to[s[a[i]]]=b[i];val[s[a[i]]]=l[i];
		s[b[i]]++;to[s[b[i]]]=a[i];val[s[b[i]]]=l[i];
	}
	
	Ans=0;L=1;R=49999*10000/m;
	while(L<=R){
		Mid=L+R>>1;
		dfs(0,1);
		//printf("!!%d %d\n",Mid,mx[1]);
		if(mx[1]>=m) Ans=Mid,L=Mid+1;
		else R=Mid-1;
	}
	printf("%d\n",Ans);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
