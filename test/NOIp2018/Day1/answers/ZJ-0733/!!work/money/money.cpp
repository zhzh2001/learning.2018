#include <bits/stdc++.h>
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define per(i,r,l) for(int i=r;i>=l;i--)
int T,n,m,a[105],f[25005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		rep(i,1,n) scanf("%d",&a[i]);
		std::sort(a+1,a+n+1);
		memset(f,0,sizeof(f));
		f[0]=1;m=0;
		rep(i,1,n){
			int A=a[i];
			if(!f[A]){
				m++;
				rep(j,A,25000) f[j]|=f[j-A];
			}
		}
		printf("%d\n",m);
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
