#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const long long N=200005;
long long n,m,i,j,k,p,l,t,sum,len,f[N],g[N],b[N],c[N],nxt[N],first[N],W,cv[N],cop[N];
inline void read(long long &x)
{
	x=0; long long ff=1; char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') ff=-ff,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	x*=ff;
}
void add(long long x,long long y,long long z){b[++len]=y; c[len]=z; nxt[len]=first[x]; first[x]=len;}
bool cmp(long long x,long long y){return x>y;}
bool judge2(long long mi,long long y,long long cnt)
{
	long long i;
	for (i=1;i<=mi-1;i++)
	cop[i]=cv[i];
	for (i=mi+1;i<=cnt;i++)
	cop[i-1]=cv[i];
	cnt--;
	long long r=cnt,ans=0;
	for (i=1;i<=cnt;i++)
	{
		if (r<=i) break;
		while (cop[i]+cop[r]<W&&r>i) r--;
		if (r<=i) break;
		if (cop[i]+cop[r]>=W) ans++,r--;
	}
	/*if (mi==2) {
		for (i=1;i<=cnt-1;i++) printf("( %d %d %d ) ",cop[i],ans,y);
		printf("\n");
	}*/
	if (ans==y) return 1;
	else return 0;
}
long long B_s2(long long A,long long cnt)
{
	long long l=1,r=cnt,mi,ans=cv[cnt];
	while (l<=r)
	{
		long long mi=(l+r)>>1;
		if (judge2(mi,A,cnt)) ans=cv[mi],r=mi-1;
		else l=mi+1;
	}
	return ans;
}
void dfs(long long x,long long fa)
{
	long long i,s0=0,cnt=0;
	for (i=first[x];i;i=nxt[i])
	if (b[i]!=fa) dfs(b[i],x);
	for (i=first[x];i;i=nxt[i])
	if (b[i]!=fa) s0+=f[b[i]]+((g[b[i]]+c[i])>=W);
	f[x]=s0;
	for (i=first[x];i;i=nxt[i])
	if (b[i]!=fa)
	{
		if (g[b[i]]+c[i]<W)
		cv[++cnt]=g[b[i]]+c[i];
	} 
	cv[++cnt]=0;
	sort(cv+1,cv+cnt+1,cmp);
	long long r=cnt,ans=0;
	for (i=1;i<=cnt;i++)
	{
		if (r<=i) break;
		while (cv[i]+cv[r]<W&&r>i) r--;
		if (r<=i) break;
		if (cv[i]+cv[r]>=W) ans++,r--; 
	}
	//if (x==4) {for (i=1;i<=cnt;i++) printf("%d %d %d\n",cv[i],ans,W);}
	f[x]=f[x]+ans;
	g[x]=B_s2(ans,cnt);
	//printf("%d %d %d XX\n",x,f[x],g[x]);
}
bool judge(long long M)
{
	W=M;
	memset(f,0,sizeof(f));
	memset(g,0,sizeof(g));
	dfs(1,0);
	//printf("%d %d %d\n",m,M,f[1]);
	if (f[1]>=m) return 1;
	else return 0;
}
long long B_s()
{
	long long l=0,r=sum,mi,ans=0;
	while (l<=r)
	{
		long long mi=(l+r)>>1;
		if (judge(mi)) l=mi+1,ans=mi;
		else r=mi-1;
	}
	return ans;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n); read(m);
	for (i=1;i<=n-1;i++)
	{
		long long x,y,z;
		read(x); read(y); read(z);
		add(x,y,z);
		add(y,x,z);
		sum+=z;
	}
	//judge(16);
	printf("%lld\n",B_s());
	return 0;
}
