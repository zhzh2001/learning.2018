#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=105,M=50005;
int n,i,j,k,p,l,t,a[N],f[M*2],g[M*2];
inline void read(int &x)
{
	x=0; int ff=1; char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') ff=-ff,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	x*=ff;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	read(T);
	while (T--)
	{
		memset(f,0,sizeof(f));
		memset(g,0,sizeof(g));
		int mx=0,ans=0;
		read(n);
		for (i=1;i<=n;i++)
		read(a[i]),mx=max(mx,a[i]);
		f[0]=1; g[0]=1;
		for (i=1;i<=n;i++)
		for (j=0;j<=mx;j++)
		f[j+a[i]]|=f[j];
		for (i=1;i<=mx;i++)
		if (f[i]==1&&g[i]==0)
		{
			ans++;
			for (j=0;j<=mx;j++)
			g[j+i]|=g[j];
		} 
		printf("%d\n",ans);
	}
	return 0;
}
