#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const long long N=100005;
long long n,i,j,k,p,l,t,a[N],b[N],ans;
inline void read(long long &x)
{
	x=0; long long ff=1; char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') ff=-ff,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	x*=ff;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for (i=1;i<=n;i++) read(a[i]),a[i]=-a[i];
	for (i=1;i<=n+1;i++) b[i]=a[i]-a[i-1];
	for (i=1;i<=n+1;i++) if (b[i]>0) ans+=b[i];
	printf("%lld\n",ans);
	return 0;
}
