#include <iostream>
#include <cstdio>
#include <cctype>
#include <algorithm>
#define il inline
#define vd void
#define rg register
#define mn 100005
#define rep(i,x,y) for(rg int i=x;i<=y;++i)
using namespace std;
//const int aa[18]={1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536,131072};
il int rd(){char c;
	while(!isdigit(c=getchar())&&c!='-');
	int f=c=='-'?c=getchar(),1:0,x=c^48;
	while(isdigit(c=getchar())) x=((x+(x<<2))<<1)+(c^48);
	return f?-x:x;
}
//vd rt(int x){x<0?putchar('-'),x=-x:0,putchar((x>=10?rt(x/10),x%10:x)+48);}
int n,ans,a[mn],vis[mn];//,p[18][mn],t[18][mn],P[18][mn];
struct P{
	int v,id;
	il int operator <(const P &p)const{return v>p.v||(v==p.v&&id<p.id);}
}b[mn];
struct L{
	int l,r;
}c[mn];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=rd();
	rep(i,1,n) a[i]=b[i].v=rd(),b[i].id=i,c[i].l=i-1,c[i].r=i+1;
	/*for(rg int j=1;j<18&&aa[j]<=n;++j)
		for(rg int i=1;i+aa[j]-1<=n;++i)
			t[j][i]=t[j-1][t[j-1][i]];
	//for(rg int i=1;i<=n;++i) printf("%d ",t[1][i]);
	for(rg int j=1;j<18&&aa[j]<=n;++j)
		for(rg int i=1;i+aa[j]-1<=n;++i)
			if(p[j-1][i]<p[])*/
	sort(b+1,b+n+1);
	rep(i,2,n) if(a[i]==a[i-1]) vis[i-1]=1,c[i].l=c[i-1].l,c[c[i].l].r=i;
	rep(i,1,n) if(!vis[b[i].id]){
		int A=a[c[b[i].id].l],B=a[c[b[i].id].r],C=a[b[i].id];
		if(A<B){
			ans+=C-B,c[c[b[i].id].r].l=c[b[i].id].l,
			c[c[b[i].id].l].r=c[b[i].id].r;
		}
		else if(A>B){
			ans+=C-A,c[c[b[i].id].l].r=c[b[i].id].r,
			c[c[b[i].id].r].l=c[b[i].id].l;
		}
		else{
			ans+=C-B,vis[c[b[i].id].l]=1,
		//	c[c[b[i].id].l].r=c[c[b[i].id].r].r,
			c[c[b[i].id].r].l=c[c[b[i].id].l].l,
			c[c[c[b[i].id].l].l].r=c[b[i].id].r;
			//c[c[b[i].id].l].r=c[b[i].id].r;
		}
		//rep(j,1,n) printf("%d %d %d\n",c[j].l,c[j].r,vis[j]);
		//printf("%d\n",ans);
		
		//puts("");
	}
	printf("%d",ans);
	//rt(ans);
	return 0;
}
