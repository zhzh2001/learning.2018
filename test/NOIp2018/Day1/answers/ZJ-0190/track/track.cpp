#include <iostream>
#include <cstdio>
#include <cctype>
#include <cstring>
#define il inline
#define vd void
#define rg register
#define mn 50005
using namespace std;
il int rd(){char c;
	while(!isdigit(c=getchar())&&c!='-');
	int f=c=='-'?c=getchar(),1:0,x=c^48;
	while(isdigit(c=getchar())) x=((x+(x<<2))<<1)+(c^48);
	return f?-x:x;
}
vd rt(int x){x<0?putchar('-'),x=-x:0,putchar((x>=10?rt(x/10),x%10:x)+48);}
int n,m,cnt,u,v,w,L,R,x,ans,s,root,mm[mn],h[mn],vis[mn],dis[mn],r[mn];
struct E{int to,nxt,w;}e[mn<<1];
il vd Add(int u,int v,int w){e[++cnt]=(E){v,h[u],w},h[u]=cnt;}
vd Dfs(int u){
	vis[u]=1;
	for(rg int i=h[u];i;i=e[i].nxt){int v=e[i].to;
		if(!vis[v]) vis[v]=1,dis[v]=dis[u]+e[i].w,Dfs(v);
	}
}
il vd Do20(){
	Dfs(2);
	int m=0,t=0;
	for(rg int i=1;i<=n;++i) if(dis[i]>m) m=dis[i],t=i;
	memset(dis,0,sizeof(dis)),memset(vis,0,sizeof(vis)),Dfs(t);
	m=0;
	for(rg int i=1;i<=n;++i) if(dis[i]>m) m=dis[i];
	rt(m);
}
vd dfs(int u,int fa){
//	printf("%d %d\n",u,fa);
	int m1=0,m2=0;
	for(rg int i=h[u];i;i=e[i].nxt){int v=e[i].to;
		if(v!=fa){
			dfs(v,u);
			mm[v]+=e[i].w;
			if(mm[v]>m1) m2=m1,m1=mm[v];
			else if(mm[v]>m2) m2=mm[v];
		}
	}
	//printf("%d %d %d\n",u,m1,m2);
	if(m2>=x){
		s+=2,mm[u]=0;
	}
	else{
		if(m1>=x) ++s,mm[u]=m2;
		else{
			if(m1+m2>=x) ++s,mm[u]=0;
			else mm[u]=m1;
		}
	}
}
il int Check(){
	s=0,dfs(root,0);
	return s>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=rd(),m=rd(); //int f=0;
	for(rg int i=2;i<=n;++i){
		u=rd(),v=rd(),w=rd(),R+=w,Add(u,v,w),Add(v,u,w),++r[u],++r[v];
	//	if(u+1!=v) f=1;
	}
	if(m==1) return Do20(),0;
	for(root=1;root<=n;++root) if(r[root]<3) break;
	//printf("%d\n",root);
	while(L<=R){
		x=(L+R)>>1,Check()?ans=x,L=x+1:R=x-1;
	}
	rt(ans);
	return 0;
}
