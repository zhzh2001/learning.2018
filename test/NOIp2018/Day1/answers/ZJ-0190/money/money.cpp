#include <iostream>
#include <cstdio>
#include <cctype>
#include <algorithm>
#include <cstring>
#define il inline
#define vd void
#define rg register
#define rep(i,x,y) for(rg int i=x;i<=y;++i)
using namespace std;
il int rd(){char c;
	while(!isdigit(c=getchar())&&c!='-');
	int f=c=='-'?c=getchar(),1:0,x=c^48;
	while(isdigit(c=getchar())) x=((x+(x<<2))<<1)+(c^48);
	return f?-x:x;
}
vd rt(int x){x<0?putchar('-'),x=-x:0,putchar((x>=10?rt(x/10),x%10:x)+48);}
int T,n,m,ans,a[105],b[50005];
il int Max(int a,int b){return a>b?a:b;}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=rd();
	while(T--){
		n=rd(),ans=m=0;
		rep(i,1,n) a[i]=rd(),m=Max(m,a[i]);
		sort(a+1,a+n+1);
		rep(j,0,m) b[j]=0;
		//memset(b,0,sizeof(b));
		b[0]=1;
		rep(i,1,n) if(!b[a[i]]){
			++ans; int t=m-a[i];
			rep(j,0,t) if(b[j]) b[j+a[i]]=1;
		}
		rt(ans),putchar('\n');
	}
	return 0;
}
