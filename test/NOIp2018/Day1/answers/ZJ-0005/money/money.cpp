#include<stdio.h>
#include<string.h>
#include<algorithm>
using namespace std;
int T,n,ans,a[105],f[25005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		for(int i=0;i<25005;i++)
			f[i]=0;
		scanf("%d",&n);
		ans=1;
		for(int i=1;i<=n;i++)
			scanf("%d",a+i);
		sort(a+1,a+n+1);
		for(int i=2;i<=n;i++){
			for(int j=a[i-1];j<=a[n];j++)
				f[j]=max(f[j],f[j-a[i-1]]+a[i-1]);
			if(f[a[i]]!=a[i])
				ans++;
		}
		printf("%d\n",ans);
	}
	return 0;
}
