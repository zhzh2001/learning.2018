#include<stdio.h>
#include<string.h>
#define min(x,y) ((x<y)?(x):(y))
int n,m,maxx,ans,sum,minn,nxt[50005],edge[100005],Next[100005],head[50005],l[100005],k[100005],flag[100005],edgenum;
void addedge(int a,int b,int c,int key){
	l[++edgenum]=c;
	edge[edgenum]=b;
	Next[edgenum]=head[a];
	head[a]=edgenum;
	k[edgenum]=key;
}
void dfs1(int u,int dep){
	if(dep>maxx){
		maxx=dep;
		ans=u;
	}
	for(int e=head[u];e;e=Next[e])
		if(!flag[k[e]]){
			flag[k[e]]=1;
			dfs1(edge[e],dep+l[e]);
			flag[k[e]]=0;
		}
}
void dfs2(int u,int dep){
	if(dep>maxx){
		maxx=dep;
		ans=u;
	}
	for(int e=head[u];e;e=Next[e])
		if(!flag[k[e]]){
			flag[k[e]]=1;
			dfs2(edge[e],dep+l[e]);
			flag[k[e]]=0;
		}
}
bool check(int x){
	int tot=0,temp;
	for(int i=1;tot<=n&&i<=m;i++){
		temp=0;
		if(tot==n)
			return 0;
		while(tot<=n&&temp<x)
			temp+=nxt[++tot];
		if(temp<x)
			return 0;
	}
	return 1;
}
int bs(int l,int r){
	if(l==r)
	  return l;
	int mid=(l+r+1)>>1;
	if(check(mid))
		return bs(mid,r);
	else return bs(l,mid-1);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool bi_ai=1;
	minn=1000000000;
	for(int i=1,a,b,c;i<n;i++){
		scanf("%d%d%d",&a,&b,&c);
		sum+=c;
		minn=min(minn,c);
		addedge(a,b,c,i);
		addedge(b,a,c,i);
		nxt[min(a,b)]=c;
		if(a+1!=b&&b+1!=a)
			bi_ai=0;
	}
	if(m==1){
		memset(flag,0,sizeof(flag));
		ans=1;
		maxx=0;
		dfs1(1,0);
		memset(flag,0,sizeof(flag));
		maxx=0;
		dfs2(ans,0);
		printf("%d\n",maxx);
	}
	else if(bi_ai){
		printf("%d\n",bs(minn,sum/m));
	}
	else if(m==n-1)
		printf("%d\n",minn);
	else printf("%d\n",(int)(sum/m*4/5));
	return 0;
}
