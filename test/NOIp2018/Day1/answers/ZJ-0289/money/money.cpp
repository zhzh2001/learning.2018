#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<vector>
#include<set>
#include<map>
using namespace std;
int t,n,a[1000010];
bool f[1000010];
inline int read()
{
	char ch='@';
	int x=0,y=1;
	while (ch!='-' && (ch<'0' || ch>'9')) ch=getchar();
	if (ch=='-') ch=getchar(),y=-1;
	while (ch>='0' && ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*y;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read();
	for (int _=1;_<=t;++_)
	{
		memset(f,false,sizeof(f));
		n=read();
		int tmp=0;
		for (int i=1;i<=n;++i) a[i]=read();
		sort(a+1,a+n+1);
		for (int i=1;i<=n;++i)
		{
			if (f[a[i]]) ++tmp;
			else
			{
				f[a[i]]=true;
				for (int j=1;a[i]+j<=a[n];++j) if (f[j]==true) f[j+a[i]]=true;
			}
		}
		printf("%d\n",n-tmp);
	}
	return 0;
}
