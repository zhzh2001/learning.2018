#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<vector>
#include<set>
#include<queue>
#include<map>
using namespace std;
int n,m,head[1000010],tot=0,len[1000010],dis[1000010];
bool visit[1000010];
priority_queue <pair<int,int>,vector<pair<int,int> >,less<pair<int,int> > > q;
struct link
{
	int to,value,next;
} edge[1000010];
bool flag1=false;
inline int read()
{
	char ch='@';
	int x=0,y=1;
	while (ch!='-' && (ch<'0' || ch>'9')) ch=getchar();
	if (ch=='-') ch=getchar(),y=-1;
	while (ch>='0' && ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*y;
}
void add(int x,int y,int z)
{
	++tot,edge[tot].to=y,edge[tot].value=z,edge[tot].next=head[x],head[x]=tot;
}
int get_dijk(int s)
{
	int ans=0;
	while (!q.empty()) q.pop();
	memset(dis,0,sizeof(dis));
	memset(visit,false,sizeof(visit));
	visit[s]=true,q.push(make_pair(0,s));
	do
	{
		pair<int,int> now=q.top();
		q.pop();
		for (int i=head[now.second];i!=0;i=edge[i].next)
			if (!visit[edge[i].to] && dis[edge[i].to]<dis[now.second]+edge[i].value)
			{
				dis[edge[i].to]=dis[now.second]+edge[i].value;
				if (!visit[edge[i].to]) q.push(make_pair(dis[edge[i].to],edge[i].to)),visit[edge[i].to]=true;
			}
	}while (!q.empty());
	for (int i=1;i<=n;++i) ans=max(ans,dis[i]);
	return ans;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for (int i=1,x,y,z;i<n;++i)
	{
		x=read(),y=read(),z=read(),add(x,y,z),add(y,x,z),len[i]=z;
		if (x!=1) flag1=true;
	}
	if (!flag1)
	{
		int ans=0;
		sort(len+1,len+n+1);
		for (int i=n;i>=max(n-2*m+1,1);--i) ans+=len[i];
		printf("%d",ans);
	}
	if (m==n-1)
	{
		int ans=0;
		for (int i=1;i<=n;++i) ans+=len[i];
		printf("%d",ans);
	}
	if (m==1)
	{
		int ans=0;
		for (int i=1;i<=n;++i) ans=max(ans,get_dijk(i));
		printf("%d",ans);
	}
	return 0;
}
