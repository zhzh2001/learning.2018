#include<cstdio>
#include<cstring>
#include<algorithm>
const int N=51000;
inline int max(int x,int y){return x>y?x:y;}
using namespace std;
struct e{
	int a,b,l;
	inline bool operator <(const e &x)const{
		return l<x.l;
	}
}e[N];
int n,m;
int to[N<<1],nxt[N<<1],w[N<<1],head[N],cnt,ww[N];
inline void add(int u,int v,int val){
	to[++cnt]=v,nxt[cnt]=head[u];
	w[cnt]=val, head[u]=cnt;
}
int dis[N];
void dfs(int u,int ff){
	for(int i=head[u];i;i=nxt[i])
		if(to[i]!=ff){
			dis[to[i]]=dis[u]+w[i];
			dfs(to[i],u);
		}
}
inline int check1(int x){
	int res=m,l=1,r=n-1;
	while(res>0&&r>0&&e[r].l>=x) r--,res--;
	if(res<=0) return 1;
	while(l<r&&res>0){
		while(l<r&&e[l].l+e[r].l<x) l++;
		if(l!=r&&e[l].l+e[r].l>=x) r--,res--,l++;
	}
	return res<=0;
}
inline int check2(int x){
	int res=m,now=1,tmp;
	while(res>0&&now!=n){
		tmp=0;
		while(tmp<x&&now!=n) tmp+=ww[now],now++;
		if(tmp>=x) res--;
	}
	return res<=0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int flg1=1,flg2=1;
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&e[i].a,&e[i].b,&e[i].l);
		add(e[i].a,e[i].b,e[i].l);
		add(e[i].b,e[i].a,e[i].l);
		if(e[i].a!=1) flg1=0;
		if(e[i].b!=e[i].a+1) flg2=0;
	}
	if(m==1){
		int maxn=0,ans=0;
		dfs(1,0);
		for(int i=1;i<=n;i++)
			if(dis[i]>dis[maxn]) maxn=i;
		memset(dis,0,sizeof dis);
		dfs(maxn,0);
		for(int i=1;i<=n;i++)
			ans=max(ans,dis[i]);
		printf("%d\n",ans);
		return 0;
	}
	if(flg1){
		sort(e+1,e+n);
		int l=0,r=500000000,mid,ans=0;
		while(l<=r){
			mid=(l+r)>>1;
			if(check1(mid)) l=mid+1,ans=mid;
				else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	if(flg2){
		for(int i=1;i<n;i++)
			ww[e[i].a]=e[i].l;
		int l=0,r=500000000,mid,ans=0;
		while(l<=r){
			mid=(l+r)>>1;
			if(check2(mid)) l=mid+1,ans=mid;
				else r=mid-1;
		}
		printf("%d\n",ans);	
		return 0;
	}
	return 0;
}
