#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int T,n,a[200],vis[30000],b[200],m,maxn;
inline int max(const int x,const int y){return x>y?x:y;}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		memset(vis,0,sizeof vis);
		m=maxn=0;
		scanf("%d",&n);
		for(int i=1;i<=n;i++){
			scanf("%d",&a[i]);
			maxn=max(maxn,a[i]);
		}
		sort(a+1,a+n+1);
		for(int i=1;i<=n;i++){
			if(vis[a[i]]) continue;
			b[++m]=a[i];
			vis[a[i]]=1;
			for(int j=1;j+a[i]<=maxn;j++)
				if(vis[j]) vis[j+a[i]]=1;
		}
		printf("%d\n",m);
	}
	return 0;
}
