#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <functional>
#include <limits>
#include <utility>
#include <numeric>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
	static const int kBufSiz = 1 << 20;
	char buf[kBufSiz], *S = buf, *T = buf;
	inline bool Fetch() {
		return T = (S = buf) + fread(buf, 1, kBufSiz, stdin), S != T;
	}
	inline char NC() {
		return S == T ? (Fetch() ? *S++ : EOF) : *S++;
	}
	bool isneg;
	template <typename Int> inline void RI(Int &x) {
		x = isneg = 0;
		char ch;
		for (ch = NC(); isspace(ch); ch = NC())
			;
		if (ch == '-') {
			ch = NC(), isneg = 1;
		}
		for (; isdigit(ch); ch = NC())
			x = x * 10 + ch - '0';
		if (isneg) x = -x;
	}
}

using IO::RI;

const int kMaxN = 105, kMaxM = 25005, kInf = 0x3f3f3f3f;
int T, n, a[kMaxN], b[kMaxN];
int mark[kMaxM];

int Solve2(int X, int Y) {
	if (X > Y)
		swap(X, Y);
	return (Y % X == 0) ? 1 : 2;
}

int gcd(int a, int b) {
	if (!b) {
		return a;
	}
	return gcd(b, a % b);
}

int exgcd(int a, int b, int &x, int &y) {
	if (!b) {
		x = 1;
		y = 0;
		return a;
	}
	int ret = exgcd(b, a % b, y, x);
	y -= 1LL * a * b / b;
	return ret;
}

bool Equation(int a, int b, int c) {
	int x, y, g = exgcd(a, b, x, y);
	if (c % g) {
		return 0;
	}
	x = x * c / g;
	y = y * c / g;
	x = (x % (b / g) + b / g) % (b / g);
	y = (c - x * a) / b;
	return x >= 0 && y >= 0 && x + y > 0;
}

inline bool Check(int P, int A, int B) {
	return Equation(A, B, P);
}

int Solve3(int X, int Y, int Z) {
	if (Y % X == 0 && Z % X == 0)
		return 1;
	if (Check(X, Y, Z) || Check(X, Z, Y) || Check(Y, Z, X))
		return 2;
	return 3;
}

bool vis16[20];
int kkk[kMaxN], kcnt;

void DFS(int x, int val) {
	if (x == kcnt + 1) {
		vis16[val] = true;
		return;
	}
	for (int i = 0; i <= 16; ++i) {
		int nex = val + kkk[x] * i;
		if (nex <= 16) {
			DFS(x + 1, nex);
		}
	}
}

int SolveX() {
	sort(a + 1, a + n + 1, greater<int>());
	int ans = kInf;
	for (int i = 0; i < (1 << n); ++i) {
		kcnt = 0;
		memset(vis16, 0x00, sizeof vis16);
		for (int j = 0; j < n; ++j) {
			if ((i >> j) & 1) {
				kkk[++kcnt] = a[j + 1];
				vis16[a[j + 1]] = true;	
			}
		}
		DFS(1, 0);
		bool fullvis = 1;
		for (int j = 0; j < n; ++j) {
			fullvis &= vis16[a[j + 1]];
		}
		if (fullvis) {
			ans = min(ans, kcnt);
		}
	}
	return ans;
}

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	RI(T);
	for (int tt = 1; tt <= T; ++tt) {
		RI(n);
		bool issmall = 1;
		for (int i = 1; i <= n; ++i) {
			RI(a[i]);
			if (a[i] > 16) {
				issmall = 0;
			}
		}
		sort(a + 1, a + n + 1);
		int ans = n;
		if (issmall) { // 15pts
			ans = SolveX();
		} else if (n == 2) { // extra, 15pts
			ans = Solve2(a[1], a[2]);
		} else if (n == 3) { // extra, 15pts
			ans = Solve3(a[1], a[2], a[3]);
		}
		printf("%d\n", ans);
	}
	return 0;
}

// for case $n \le 5$, enumrate set & brute force merge ans, 50pts
