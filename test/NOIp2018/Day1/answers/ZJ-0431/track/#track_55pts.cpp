#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <functional>
#include <limits>
#include <utility>
#include <numeric>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
	static const int kBufSiz = 1 << 20;
	char buf[kBufSiz], *S = buf, *T = buf;
	inline bool Fetch() {
		return T = (S = buf) + fread(buf, 1, kBufSiz, stdin), S != T;
	}
	inline char NC() {
		return S == T ? (Fetch() ? *S++ : EOF) : *S++;
	}
	bool isneg;
	template <typename Int> inline void RI(Int &x) {
		x = isneg = 0;
		char ch;
		for (ch = NC(); isspace(ch); ch = NC())
			;
		if (ch == '-') {
			ch = NC(), isneg = 1;
		}
		for (; isdigit(ch); ch = NC())
			x = x * 10 + ch - '0';
		if (isneg) x = -x;
	}
}

using IO::RI;

const int kMaxN = 50005;
int n, m, W[kMaxN];
int head[kMaxN], ecnt;
struct Edge {
	int to, nxt, w;
} e[kMaxN << 1];
inline void Insert(int x, int y, int w) {
	e[++ecnt] = (Edge) { y, head[x], w };
	head[x] = ecnt;
}
namespace SubTask1 {
int f[2][kMaxN], ans;
void DFS(int x, int fa) {
	for (int i = head[x]; i; i = e[i].nxt) {
		if (e[i].to == fa)
			continue;
		DFS(e[i].to, x);
		if (f[0][e[i].to] + e[i].w > f[0][x]) {
			f[1][x] = f[0][x];
			f[0][x] = f[0][e[i].to] + e[i].w;
		} else if (f[0][e[i].to] + e[i].w > f[1][x]) {
			f[1][x] = f[0][e[i].to] + e[i].w; 
		}
	}
	ans = max(ans, f[0][x] + f[1][x]);
}
int Solve() {
	DFS(1, 0);
	printf("%d\n", ans);
	return 0;
}
}

inline bool CheckFlower(int mid) {
	int l = 1, r = n - 1, cnt = m;
	while (l <= r) {
		if (W[l] >= mid) {
			--cnt;
			++l;
		} else {
			for (; W[l] + W[r] < mid; --r)
				;
			if (l < r) {
				++l, --r;
				--cnt;
			} else {
				break;
			}
		}
	}
	return cnt <= 0;
}

inline bool CheckList(int mid) {
	int cnt = m, sum = 0;
	for (int i = 1; i < n; ++i) {
		sum += W[i];
		if (sum >= mid) {
			--cnt;
			sum = 0;
		}
	}
	return cnt <= 0;
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	RI(n), RI(m);
	bool isflower = 1, islist = 1, isdirec = (m == 1);
	for (int i = 1, a, b; i < n; ++i) {
		RI(a), RI(b), RI(W[i]);
		Insert(a, b, W[i]);
		Insert(b, a, W[i]);
		if (a != 1)
			isflower = 0;
		if (b != a + 1)
			islist = 0;
	}
	if (m == 1) { // 20pts
		SubTask1::Solve();
		return 0;
	}
	if (isflower) { // extra, 15pts
		sort(W + 1, W + n, greater<int>());
		int low = 0, high = 5e8, ans = 0;
		while (low <= high) {
			int mid = (low + high) >> 1;
			if (CheckFlower(mid)) {
				ans = mid, low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		printf("%d\n", ans);
		return 0;
	}
	if (islist) { // extra, 20pts
		int low = 0, high = 5e8, ans = 0;
		while (low <= high) {
			int mid = (low + high) >> 1;
			if (CheckList(mid)) {
				ans = mid, low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		printf("%d\n", ans);
		return 0;
	}
	return 0;
}

