#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int Rand32() {
	return (rand() << 15) + rand();
}

int Uni32(int l, int r) {
	return (Rand32()) % (r - l + 1) + l;
}

int main() {
	freopen("road.in", "w", stdout);
	srand(GetTickCount());
	int n = 100000;
	printf("%d\n", n);
	for (int i = 1; i <= n; ++i) {
		printf("%d%c", Uni32(1, 500), " \n"[i == n]);
	}
	return 0;
}
