#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <functional>
#include <limits>
#include <utility>
#include <numeric>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
	static const int kBufSiz = 1 << 20;
	char buf[kBufSiz], *S = buf, *T = buf;
	inline bool Fetch() {
		return T = (S = buf) + fread(buf, 1, kBufSiz, stdin), S != T;
	}
	inline char NC() {
		return S == T ? (Fetch() ? *S++ : EOF) : *S++;
	}
	bool isneg;
	template <typename Int> inline void RI(Int &x) {
		x = isneg = 0;
		char ch;
		for (ch = NC(); isspace(ch); ch = NC())
			;
		if (ch == '-') {
			ch = NC(), isneg = 1;
		}
		for (; isdigit(ch); ch = NC())
			x = x * 10 + ch - '0';
		if (isneg) x = -x;
	}
}

using IO::RI;

const int kMaxN = 1e5 + 5, kInf = 0x3f3f3f3f;
int n, a[kMaxN], lhs[kMaxN], rhs[kMaxN];
int stkl[kMaxN], stkt;
pii b[kMaxN];

struct SegTree {
	int add[kMaxN << 2];
	inline void Build(int x, int l, int r) {
		add[x] = 0;
		if (l == r) return;
		int mid = (l + r) >> 1;
		Build(x << 1, l, mid);
		Build(x << 1 | 1, mid + 1, r);
	}
	inline void Update(int x, int l, int r, int ql, int qr, int val) {
		if (ql <= l && r <= qr) {
			add[x] += val;
			return;
		}
		int mid = (l + r) >> 1;
		if (qr <= mid) {
			Update(x << 1, l, mid, ql, qr, val);
		} else if (ql > mid) {
			Update(x << 1 | 1, mid + 1, r, ql, qr, val);
		} else {
			Update(x << 1, l, mid, ql, qr, val);
			Update(x << 1 | 1, mid + 1, r, ql, qr, val);
		}
	}
	inline int Query(int x, int l, int r, int pos, int pa) {
		if (l == r) {
			return pa + add[x];
		}
		int mid = (l + r) >> 1;
		if (pos <= mid) {
			return Query(x << 1, l, mid, pos, pa + add[x]);
		} else {
			return Query(x << 1 | 1, mid + 1, r, pos, pa + add[x]);
		}
	}
} seg;

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	RI(n);
	for (int i = 1; i <= n; ++i) {
		RI(a[i]);
	}
	stkl[0] = 0;
	for (int i = 1, t; i <= n; ++i) {
		for (; stkt > 0 && a[stkl[stkt]] >= a[i]; --stkt) {
		}
		lhs[i] = stkl[stkt] + 1;
		stkl[++stkt] = i;
	}
	stkt = 0;
	stkl[0] = n + 1;
	for (int i = n, t; i > 0; --i) {
		for (; stkt > 0 && a[stkl[stkt]] >= a[i]; --stkt) {
		}
		rhs[i] = stkl[stkt] - 1;
		stkl[++stkt] = i;
	}
	for (int i = 1; i <= n; ++i) {
		b[i].first = a[i];
		b[i].second = i;
	}
	sort(b + 1, b + n + 1);
	int ans = 0;
	for (int i = 1; i <= n; ++i) {
		int v = seg.Query(1, 1, n, b[i].second, 0);
		ans += a[b[i].second] - v;
		seg.Update(1, 1, n, lhs[b[i].second], rhs[b[i].second], a[b[i].second] - v);
	}
	printf("%d\n", ans);
	return 0;
}
