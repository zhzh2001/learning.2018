#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <functional>
#include <limits>
#include <utility>
#include <numeric>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
	static const int kBufSiz = 1 << 20;
	char buf[kBufSiz], *S = buf, *T = buf;
	inline bool Fetch() {
		return T = (S = buf) + fread(buf, 1, kBufSiz, stdin), S != T;
	}
	inline char NC() {
		return S == T ? (Fetch() ? *S++ : EOF) : *S++;
	}
	bool isneg;
	template <typename Int> inline void RI(Int &x) {
		x = isneg = 0;
		char ch;
		for (ch = NC(); isspace(ch); ch = NC())
			;
		if (ch == '-') {
			ch = NC(), isneg = 1;
		}
		for (; isdigit(ch); ch = NC())
			x = x * 10 + ch - '0';
		if (isneg) x = -x;
	}
}

using IO::RI;

const int kMaxN = 1e5 + 5, kInf = 0x3f3f3f3f;
int n, a[kMaxN];

// worst case: O(n ^ 2)
int Solve(int l, int r) {
	if (l > r)
		return 0;
	int last = l, minv = kInf, ans = 0;
	for (int i = l; i <= r; ++i) {
		minv = min(minv, a[i]);
	}
	for (int i = l; i <= r; ++i) {
		a[i] -= minv;
	}
	ans += minv;
	for (int i = l; i <= r; ++i) {
		if (a[i] == 0 && (i == l || a[i - 1] != 0)) {
			ans += Solve(last, i - 1);
		}
		if (a[i] == 0 && (i == r || a[i + 1] != 0)) {
			last = i + 1;
		}
	}
	if (a[r] != 0) {
		ans += Solve(last, r);
	}
	return ans;
}

int main() {
	freopen("road.in", "r", stdin);
	freopen("bf.out", "w", stdout);
	RI(n);
	for (int i = 1; i <= n; ++i) {
		RI(a[i]);
	}
	printf("%d\n", Solve(1, n));
	return 0;
}
