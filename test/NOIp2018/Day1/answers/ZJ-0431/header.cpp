#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <functional>
#include <limits>
#include <utility>
#include <numeric>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
	static const int kBufSiz = 1 << 20;
	char buf[kBufSiz], *S = buf, *T = buf;
	inline bool Fetch() {
		return T = (S = buf) + fread(buf, 1, kBufSiz, stdin), S != T;
	}
	inline char NC() {
		return S == T ? (Fetch() ? *S++ : EOF) : *S++;
	}
	bool isneg;
	template <typename Int> inline void RI(Int &x) {
		x = isneg = 0;
		char ch;
		for (ch = NC(); isspace(ch); ch = NC())
			;
		if (ch == '-') {
			ch = NC(), isneg = 1;
		}
		for (; isdigit(ch); ch = NC())
			x = x * 10 + ch - '0';
		if (isneg) x = -x;
	}
}

using IO::RI;

int main() {
	
	return 0;
}
