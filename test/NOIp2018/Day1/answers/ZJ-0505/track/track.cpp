#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;

const int maxn = 50005;
int n, m;
int nxt[maxn * 2], hed[maxn * 2], to[maxn * 2], val[maxn * 2], cnt, ru[maxn], num;
ll dep[maxn];
struct node{
	ll x, y, v;
}a[maxn];

int read(void) {
	char c; while (c = getchar(), c < '0' || c > '9'); int x = c - '0';
	while (c = getchar(), c >= '0' && c <= '9') x = x * 10 + c - '0'; return x;
}

void add(int x, int y, int v) {
	nxt[++ cnt] = hed[x]; hed[x] = cnt; to[cnt] = y; val[cnt] = v;
}  

void dfsdep(int u, int pre) {
	for (int i = hed[u]; i; i = nxt[i]) {
	  int v = to[i];
	    if (v == pre) continue;
	  dep[v] = dep[u] + val[i];
	  dfsdep(v, u);
	}
}

void subtask1(void) {
	dfsdep(1, 0);
	int ind = 0;
	  for (int i = 1; i <= n; ++ i) 
	    if (dep[i] > dep[ind]) ind = i;
	dep[ind] = 0;
	dfsdep(ind, 0);
	  for (int i = 1; i <= n; ++ i) 
	    if (dep[i] > dep[ind]) ind = i;
	printf("%lld", dep[ind]);
}

int cmp(node a,node b) {
	return a.v > b.v;
}

void subtask2(void) {
	sort(a + 1, a + 1 + n, cmp);
	int x = (m - 1) * 2 + 1, y = (m - 1) * 2 + 2;
	printf("%lld", a[x].v + a[y].v);
}

void dfs(int u, int pre, ll now, ll x) {
	for (int i = hed[u]; i ; i = nxt[i]) {
	  int v = to[i];
	    if (v == pre) continue;
	  if (now + val[i] <= x) dfs(v, u, now + val[i], x);
	  else num ++, dfs(v, u, 0, x);
	}
}

void subtask3(void) {
	for (int i = 1; i <= n; ++ i) ru[a[i].x] ++, ru[a[i].y] ++;
	int p;
	for (int i = 1; i <= n; ++ i) 
	  if (ru[i] == 1) {
	    p = i; break;
	  }
    ll l = 0, r = 1e9, mid, res;
	  while (l <= r) {
	  	mid = l + r >> 1;
	  	dfs(p, 0, 0, mid);
	  	  if (num >= m) l = mid + 1, res = mid;
	  	  else r = mid - 1;
	  }
	printf("%lld", res);
}

int main() {
	freopen("track.in", "r", stdin); freopen("track.out", "w", stdout);
	n = read(); m = read();
	int bj1 = 1;
	  for (int i = 1; i < n; ++ i) {
	  	int x = read(), y = read(), v = read();
	  	a[i].x = x; a[i].y = y; a[i].v = v;
	  	add(x, y, v); add(y, x, v);
	  	if (x != 1) bj1 = 0;
	  }
	  if (m == 1) subtask1();
	  else if (bj1) subtask2();
	  else subtask3();
	return 0;
}
