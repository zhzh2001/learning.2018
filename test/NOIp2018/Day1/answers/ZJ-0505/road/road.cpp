#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;

const int maxn = 100005;
int n, b[maxn];
struct node{
	int val, ind;
}a[maxn];
ll ans;

int read(void) {
	char c; while (c = getchar(), c < '0' || c > '9'); int x = c - '0';
	while (c = getchar(), c >= '0' && c <= '9') x = x * 10 + c - '0'; return x;
}

int cmp(node a, node b) {
	return a.val < b.val;
}

int main() {
	freopen("road.in", "r", stdin); freopen("road.out", "w", stdout);
	n = read();
	  for (int i = 1; i <= n; ++ i) {
	  	a[i].val = read(); a[i].ind = i;
	  	b[i] = a[i].val;
	  }
	sort(a + 1, a + 1 + n, cmp);
	  for (int i = 1; i <= n; ++ i) {
	  	ans += b[a[i].ind];
	  	  for (int j = a[i].ind - 1; j ; -- j) {
	  	  	  if (b[j] >= b[a[i].ind]) b[j] -= b[a[i].ind];
	  	      else break;
			}
	  	  for (int j = a[i].ind + 1; j <= n; ++ j) {
	  	  	  if (b[j] >= b[a[i].ind]) b[j] -= b[a[i].ind];
	  	  	  else break;
			}
		b[a[i].ind] = 0;
	  }
 	printf("%lld", ans);
 	return 0;
}
