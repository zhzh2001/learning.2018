#include<cstdio>
using namespace std;

const int maxn = 105;
int t, n, f[2500005], sum, ans, vis[maxn], a[maxn];

int read(void) {
	char c; while (c = getchar(), c < '0' || c > '9'); int x = c - '0';
	while (c = getchar(), c >= '0' && c <= '9') x = x * 10 + c - '0'; return x;
}

int main() {
	freopen("money.in", "r", stdin); freopen("money.out", "w", stdout);
	t = read();
	  while (t --) {
	  	n = read();
	  	ans = n; sum = 0;
	  	  for (int i = 1; i <= n; ++ i) a[i] = read(), sum += a[i], vis[i] = 0;
	  	f[0] = 1;
	  	  for (int i = 1; i <= n; ++ i) {
	  	  	  for (int j = 1; j <= sum; ++ j) f[j] = 0;
	  	  	  for (int j = 1; j <= n; ++ j) {
	  	  	  	    if (j != i && !vis[j]) {
	  	  	  	    	    for (int k = a[j]; k <= sum; ++ k) f[k] |= f[k - a[j]];
						  }
				  }
			  if (f[a[i]]) vis[i] = 1, ans --;
			}
		printf("%d\n", ans);
	  }
	return 0;
}
