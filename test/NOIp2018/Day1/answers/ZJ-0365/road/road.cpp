#include <cstdio>
#include <algorithm>
#include <cmath>

using namespace std;

int rd() {
	int x = 0, f = 1; char c = getchar();
	while (c > '9' || c < '0') f = c == '-' ? -1 : 1, c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x * f;
}

const int inf = 0x7f7f7f7f;
int n, d[100010], a[100010], ans;

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	n = rd();
	for (int i = 1; i <= n; i ++) d[i] = rd();
	for (int i = 1; i <= n; i ++) {
		int l = 1, mn = inf, r = 0, tot = 0;
		bool flag = 1;
		while (l <= n) {
			while (!d[l]) l ++; r = l;
			if (l > n) break;
			while (d[r] && r <= n) mn = min(mn, d[r]), flag = 0, r ++;
			ans += mn, a[l] -= mn, a[r] += mn;
			l = r + 1;
		}
		if (flag) break;
		for (int j = 1; j <= n; j ++) a[j] += a[j - 1];
		for (int j = 1; j <= n; j ++) d[j] += a[j], a[j] = 0;
	}
	printf("%d\n", ans);
	return 0;
}
