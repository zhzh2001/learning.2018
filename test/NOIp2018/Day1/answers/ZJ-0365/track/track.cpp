#include <cstdio>
#include <queue>
#include <algorithm>

using namespace std;

int rd() {
	int x = 0, f = 1; char c = getchar();
	while (c > '9' || c < '0') f = c == '-' ? -1 : 1, c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x * f;
}

const int N = 50010;
int a[N], b[N], c[N], l[N], n, m;

bool cmp(int a, int b) {
	return a > b;
}

void solvea() {
	sort(l + 1, l + n, cmp);
	if (m * 2 <= n) printf("%d\n", l[m * 2] + l[m * 2 - 1]);
	else printf("%d\n", min(l[1], l[n - 1] + l[n]));
	return;
}

void solveb() {
	int sum = 0, tot = 0, now = 0;
	for (int i = 1; i <= n; i ++) sum += l[i];
	int ave = sum / m;
	for (int i = 1; i <= n; i ++) {
		now += l[i];
		if (now >= ave) c[++tot] = now, sum -= now, now = 0, m --;
		if (m > 0) ave = sum / m;
	}
	sort(c + 1, c + tot + 1);
	printf("%d\n", c[1]);
	return;
}

void solvec() {
	sort(l + 1, l + n);
	printf("%d\n", l[1]);
	return;
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	n = rd(), m = rd();
	bool fa = 1, fb = 1;
	for (int i = 1; i < n; i ++) {
		a[i] = rd(), b[i] = rd(), l[i] = rd();
		if (a[i] != 1) fa = 0;
		if (b[i] != a[i] + 1) fb = 0;
	}
	if (m == n - 1) return solvec(), 0;
	if (fa) return solvea(), 0;
	if (fb) return solveb(), 0;
	return 0;
}
