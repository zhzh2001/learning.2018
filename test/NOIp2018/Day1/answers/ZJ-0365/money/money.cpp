#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>

using namespace std;

int rd() {
	int x = 0, f = 1; char c = getchar();
	while (c > '9' || c < '0') f = c == '-' ? -1 : 1, c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x * f;
}

int A[110], a[110], b[25010], c[25010];
int n, ans, mx;
bool flag;

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	for (int T = rd(); T; T --) {
		n = rd(), flag = 0, mx = 0, ans = 0;
		memset(b, 0, sizeof b);
		memset(c, 0, sizeof c);
		for (int i = 1; i <= n; i ++) {
			A[i] = rd();
			if (A[i] == 1) flag = 1;
			mx = max(mx, A[i]);
		}
		for (int i = 1; i <= n; i ++) if (!b[A[i]]) 
			a[++ans] = A[i], c[A[i]] = b[A[i]] = 1;
		n = ans; 
		if (flag) {puts("1"); continue;}
		for (int i = 1; i <= mx; i ++) {
			for (int j = 1; j <= n; j ++) if (i - a[j] > 0) 
				c[i] += c[i - a[j]];
		}
		for (int i = 1; i <= n; i ++) if (c[a[i]] > b[a[i]]) ans --;
		printf("%d\n", ans);
	}
	return 0;
}
