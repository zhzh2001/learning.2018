#include<iostream>
#include<iomanip>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
typedef long long ll;
ll ans1;
int roadnum,mxx,built,road[100005];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	//start
	scanf("%d",&roadnum);
	for(int i=1;i<=roadnum;i++){
		scanf("%d",&road[i]);
	}
	road[roadnum+1]=0;
	mxx=-100000;
	for(int i=1;i<=roadnum;i++){
		if(mxx<=road[i])mxx=road[i];
	}
	built=0;
	ans1=0;
	for(int i=1;i<=mxx;i++){
		for(int j=1;j<=roadnum+1;j++){
			if(road[j]<i&&!built){
				ans1++;
				built=1;
			}
			else if(road[j]>=i){
				built=0;
			}
		}
	}
	printf("%lld",ans1);
	//end
	fclose(stdin);fclose(stdout);
	return 0;
}
