#include<iostream>
#include<iomanip>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
typedef long long ll;
ll nume,m,pandian05,pandian01,cstsum[50005];
struct road{
	ll cst;
	ll sta;
	ll ed;
};
road axa[1000];
bool cmp(road a,road b){
	if(a.cst>b.cst)return true;
	return false;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	//start
	scanf("%lld%lld",&nume,&m);
	for(int i=1;i<=nume-1;i++){
		scanf("%lld%lld%lld",&axa[i].sta,&axa[i].ed,&axa[i].cst);
	}
	pandian05=1;
	for(int i=1;i<=nume-1;i++){
		if(axa[i].sta!=1||m!=1)pandian05=0;
	}
	if(pandian05){
		sort(axa+1,axa+nume,cmp);
		printf("%lld",axa[1].cst+axa[2].cst);
		return 0;
	}
	if(m>=nume-10){
		sort(axa+1,axa+nume,cmp);
		printf("%lld",axa[1].cst);
		return 0;
	}
	//end
	fclose(stdin);fclose(stdout);
	return 0;
}
