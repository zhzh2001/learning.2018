#include<bits/stdc++.h>
#define ll long long
using namespace std;
int read(){
	char c;int x=0,y=1;while(c=getchar(),(c<'0'||c>'9')&&c!='-');
	if(c=='-') y=-1;else x=c-'0';while(c=getchar(),c>='0'&&c<='9')
	x=x*10+c-'0';return x*y;
}
int n,a[100005];
ll ans;
ll solve(int l,int r,int Min){
	if(l>r) return 0;
	if(l==r) return 1ll*a[l]-1ll*Min;
	int b=2e9,p=0;
	for(int i=l;i<=r;i++)
	  if(a[i]<b) b=a[i],p=i;
	return 1ll*b-1ll*Min+solve(l,p-1,b)+solve(p+1,r,b);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	ans=solve(1,n,0);
	printf("%lld",ans);
	return 0;
}
