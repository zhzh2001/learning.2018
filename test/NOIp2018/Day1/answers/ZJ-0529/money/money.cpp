#include<bits/stdc++.h>
#define MAXN 105
using namespace std;
int read(){
	char c;int x=0,y=1;while(c=getchar(),(c<'0'||c>'9')&&c!='-');
	if(c=='-') y=-1;else x=c-'0';while(c=getchar(),c>='0'&&c<='9')
	x=x*10+c-'0';return x*y;
}
int T,n,ans,Max,a[MAXN],vis[70005];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		n=read();ans=0;Max=0;
		for(int i=1;i<=n;i++) a[i]=read(),Max=max(Max,a[i]);
		sort(a+1,a+1+n);vis[0]=1;
		for(int i=1;i<=n;i++){
			if(vis[a[i]]) continue;
			ans++;
			for(int j=0;j<=Max;j++)
			   if(vis[j]&&j+a[i]<=Max) vis[j+a[i]]=1;
		}
		printf("%d\n",ans);
		for(int i=0;i<=Max;i++) vis[i]=0;
	}
	return 0;
}
