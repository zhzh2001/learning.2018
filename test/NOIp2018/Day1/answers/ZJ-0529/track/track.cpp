#include<bits/stdc++.h>
#define MAXN 100005
#define ll long long
using namespace std;
ll read(){
	char c;ll x;while(c=getchar(),c<'0'||c>'9');x=c-'0';
	while(c=getchar(),c>='0'&&c<='9') x=x*10+c-'0';return x;
}
ll n,m,cnt,nxt[MAXN<<1],head[MAXN<<1];
ll l,r,mid,dfn,top,p1,p2,out,Min,g[MAXN];
ll ans;
struct node{
	ll to,val;
}L[MAXN<<1];
void add(ll x,ll y,ll c){
	L[cnt].to=y;L[cnt].val=c;
	nxt[cnt]=head[x];head[x]=cnt;cnt++;
	L[cnt].to=x;L[cnt].val=c;
	nxt[cnt]=head[y];head[y]=cnt;cnt++;
}
namespace link{
	ll dfs(ll p){
		ll ans=0,res=0;int i;
		for(i=1;i<n;i++){
			if(res>=p) ans++,res=0;
			res+=g[i];
		}
		return ans;
	}
}
namespace juhua{
	ll h,t,v[MAXN];
	ll cmp(ll a,ll b){
		return a>b;
	}
	void init(){
		top=0;int i;
		for(i=head[1];i!=-1;i=nxt[i]) 
		   v[++top]=L[i].val;
		sort(v+1,v+1+top,cmp);		
	}
	ll find(ll x){
		ll res=0;
		h=1;t=top;
		while(h<t){
			while(v[h]+v[t]<x&&h<t) t--;
			if(h<t) res++;
			h++;t--;
		}
		return res;
	}
}
ll check(ll x){
	if(p2){
		ll p=link::dfs(x);
		return p>=m;
	}
	else if(p1){
		juhua::init();
		ll p=juhua::find(x);
		return p>=m;
	}
	else{
		return x;
	}
}
namespace dfstree{
	ll Max;
	ll dfs(ll x,ll fa){
		ll p1=0,p2=0;int i;
		for(i=head[x];i!=-1;i=nxt[i]){
			ll to=L[i].to;
			if(to==fa) continue;
			ll num=dfs(to,x)+L[i].val;
			if(num>p1) p2=p1,p1=num;
			else if(num>p2) p2=num;
		}
		Max=max(Max,p1+p2);
		return p1;
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();p1=1;p2=1;
	memset(head,-1,sizeof(head));
	int i;Min=1e18;
	for(i=1;i<n;i++){
		ll x=read(),y=read(),c=read();
		add(x,y,c);g[x]=c;Min=min(Min,c);
		p1*=x;p2&=(y==x+1);
	}
	if(m==n-1){
		printf("%lld",Min);return 0;
	}
	else if(m==1){
		dfstree::dfs(1,0);
		printf("%lld",dfstree::Max);
	}
	else{
		l=1;r=1e18;ans=0;
		while(l<=r){
			mid=(l+r)>>1;
			if(check(mid)) l=mid+1,ans=mid;
			else r=mid-1;
		}
		printf("%lld",ans);			
	}
	return 0;
}
