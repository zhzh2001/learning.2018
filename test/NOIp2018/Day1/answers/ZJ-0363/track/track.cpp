#include<bits/stdc++.h>
using namespace std;
const int N=2e5+5;
int n,m,K,ans;
int hd[N],nxt[N],to[N],w[N],tot;
int read() {
	int x=0; char c;
	while ((c=getchar())<'-');
	do x=x*10+c-'0';
	while ((c=getchar())>'-');
	return x;
}
void add(int a,int b,int c) {
	to[++tot]=b,w[tot]=c,nxt[tot]=hd[a],hd[a]=tot;
}
namespace sbt {
	int c[N][2],sz[N],val[N];
	int po[N],top;
	void init() {
		while (++top<N) po[top]=top;
		--top;
	}
	int nod(int w) {
		int u=po[top--];
		c[u][0]=c[u][1]=0;
		sz[u]=1;
		val[u]=w;
		return u;
	}
	inline void renod(int u) {
		po[++top]=u;
	}
	inline void up(int u) {
		sz[u]=sz[c[u][0]]+sz[c[u][1]]+1;
	}
	void rot(int &u,int d) {
		int s=c[u][d^1];
		c[u][d^1]=c[s][d];
		c[s][d]=u;
		up(u);
		up(u=s);
	}
	void maint(int &u) {
		if (sz[c[c[u][0]][0]]>sz[c[u][1]]) rot(u,1);
		if (sz[c[c[u][1]][1]]>sz[c[u][0]]) rot(u,0);
	}
	void ins(int &u,int v) {
		if (!u) u=v;
		else ins(c[u][val[v]>val[u]],v),up(u),maint(u);
	}
	void del(int &u,int w) {
		--sz[u];
		if (w<val[u]) del(c[u][0],w);
		else if (w>val[u]) del(c[u][1],w);
		else if (!c[u][0]) renod(u),u=c[u][1];
		else {
			int *p=&c[u][0];
			while (c[*p][1]) --sz[*p],p=&c[*p][1];
			val[u]=val[*p];
			renod(*p);
			*p=c[*p][0];
		}
	}
	int gmn(int u) {
		while (c[u][0]) u=c[u][0];
		return val[u];
	}
	int gmx(int u) {
		while (c[u][1]) u=c[u][1];
		return val[u];
	}
	int lb(int u,int w) {
		int re=-1;
		while (u) {
			if (val[u]>=w) re=val[u],u=c[u][0];
			else u=c[u][1];
		}
		return re;
	}
}
int dfs(int u,int f) {
	int rt=0;
	for (int i=hd[u];i;i=nxt[i]) {
		if (to[i]!=f) {
			sbt::ins(rt,sbt::nod(dfs(to[i],u)+w[i]));
		}
	}
	int p1,p2;
	while (rt&&(p1=sbt::gmx(rt))>=K) {
		++ans;
		sbt::del(rt,p1);
	}
	int re=0;
	while (rt) {
		p1=sbt::gmn(rt);
		sbt::del(rt,p1);
		if ((p2=sbt::lb(rt,K-p1))!=-1) {
			++ans;
			sbt::del(rt,p2);
		}
		else re=p1;
	}
	return re;
}
//int dfs(int u,int f) {
//	multiset<int> st;
//	for (int i=hd[u];i;i=nxt[i]) {
//		if (to[i]!=f) {
//			st.insert(dfs(to[i],u)+w[i]);
//		}
//	}
//	multiset<int>::iterator p1,p2;
//	while ((p1=st.end())!=st.begin()&&*--p1>=K) {
//		++ans;
//		st.erase(p1);
//	}
//	int re=0;
//	while ((p1=st.begin())!=st.end()) {
//		int tmp=*p1;
//		st.erase(p1);
//		if ((p2=st.lower_bound(K-tmp))!=st.end()) {
//			++ans;
//			st.erase(p2);
//		}
//		else re=tmp;
//	}
//	return re;
//}
bool check() {
	ans=0;
	dfs(1,0);
	return ans>=m;
}
int main() {
freopen("track.in","r",stdin);
freopen("track.out","w",stdout);
	sbt::init();
	scanf("%d%d",&n,&m);
	int L=0,R=0;
	for (int i=1,x,y,z;i<n;++i) {
		x=read(),y=read(),z=read();
		R+=z;
		add(x,y,z);
		add(y,x,z);
	}
	R=R/m+1;
	while (L<R) {
		K=L+R+1>>1;
		if (check()) L=K;
		else R=K-1;
	}
	printf("%d\n",L);
	return 0;
}
