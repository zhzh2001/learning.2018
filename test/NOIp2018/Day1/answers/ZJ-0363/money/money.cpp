#include<bits/stdc++.h>
using namespace std;
int T,n,a[105];
bool dp[25005];
int main() {
freopen("money.in","r",stdin);
freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		scanf("%d",&n);
		int mx=0,ans=0;
		for (int i=1;i<=n;++i) {
			scanf("%d",a+i);
			mx=max(mx,a[i]);
		}
		sort(a+1,a+n+1);
		for (int i=0;i<=mx;++i) dp[i]=0;
		dp[0]=1;
		for (int i=1;i<=n;++i) {
			if (dp[a[i]]) continue;
			++ans;
			for (int j=a[i];j<=mx;++j) {
				if (dp[j-a[i]]) dp[j]=1;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}

