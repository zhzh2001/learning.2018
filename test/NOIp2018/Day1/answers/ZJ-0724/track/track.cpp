#include <cstdio>
#include <iostream>
#include <cctype>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <bitset>
#include <queue>
#define LL long long
#define rep(i,a,b) for(LL i=a;i<=b;++i)
#define drep(i,a,b) for(LL i=a;i>=b;--i)
using namespace std;
inline LL read()
{
	LL x=0;bool f=0;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=1;ch=getchar();}
	while(isdigit(ch)){x=(((x<<2)+x)<<1)+(ch^48);ch=getchar();}
	return f?-x:x;
}
inline LL maxx(LL x,LL y)
{
	return x>y?x:y;
}

LL n,m;
LL head[50050],to[100050],nxt[100050],val[100050],cnt;
inline void addedge(LL x,LL y,LL z)
{
	to[++cnt]=y;
	nxt[cnt]=head[x];
	val[cnt]=z;
	head[x]=cnt;
}

LL ans=0,mans=0,f[50050][100],a[50050];

inline void dfs(LL x,LL fa)
{
	for(LL i=head[x];i;i=nxt[i])
	{
		LL v=to[i];
		if(v==fa)
		{
			mans=maxx(mans,ans);
			continue;
		}
		ans+=val[i];
		dfs(v,x);
		ans-=val[i];
	}
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	bool flag=1;
	rep(i,1,n-1)
	{
		LL x=read(),y=read(),z=read();
		a[i]=z;
		if(y!=x+1) flag=0;
		addedge(x,y,z);
		addedge(y,x,z);
	}
	if(m==1)
	{
		rep(i,1,n) dfs(i,0);
		printf("%lld\n",mans);
		return 0;
	}
	if(flag==1)
	{
		rep(i,1,n)
		{
			LL sum=0;
			drep(j,i,1)
			{
				sum+=a[j];
				drep(k,min(m,i),1)
					f[i][k]=maxx(f[j-1][k-1],sum);
			}
		}
		printf("%lld\n",f[n-1][m]);
		return 0;
	}
	
	return 0;
}
