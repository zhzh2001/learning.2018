#include <cstdio>
#include <iostream>
#include <cctype>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <bitset>
#include <queue>
#define LL long long
#define rep(i,a,b) for(LL i=a;i<=b;++i)
using namespace std;
inline LL read()
{
	LL x=0;bool f=0;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=1;ch=getchar();}
	while(isdigit(ch)){x=(((x<<2)+x)<<1)+(ch^48);ch=getchar();}
	return f?-x:x;
}

struct Node
{
	LL val,add;
}a[100050];
LL n;

inline bool cmp(Node a,Node b)
{
	return a.val<b.val;
}
inline bool cmp1(Node a,Node b)
{
	return a.add<b.add;
}

LL sum=0,ans=0;

inline void dfs(LL x,LL y)
{
	if(x>n or x>y or y<1) return;
	sort(a+x,a+y+1,cmp);
	LL tmp=sum;
	sum=a[x].val;
	LL mid=a[x].add;
	LL midv=a[x].val;
	sort(a+x,a+y+1,cmp1);
	dfs(x,mid-1);
	dfs(mid+1,y);
	sum=tmp;
	ans+=midv-sum;
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	rep(i,1,n) a[i].val=read(),a[i].add=i;
	dfs(1,n);
	printf("%lld\n",ans);

	return 0;
}
