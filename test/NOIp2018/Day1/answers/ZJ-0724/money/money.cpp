#include <cstdio>
#include <iostream>
#include <cctype>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <bitset>
#define LL long long
#define rep(i,a,b) for(LL i=a;i<=b;++i)
using namespace std;
inline LL read()
{
	LL x=0;bool f=0;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=1;ch=getchar();}
	while(isdigit(ch)){x=(((x<<2)+x)<<1)+(ch^48);ch=getchar();}
	return f?-x:x;
}

inline LL maxx(LL x,LL y)
{
	return x>y?x:y;
}

LL t,a[150];
bool f[25050];

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read();
	while(t--)
	{
		LL n=read();
		rep(i,1,n) a[i]=read();
		sort(a+1,a+1+n);
		LL ans=n;
		memset(f,0,sizeof f);
		f[0]=1;
		LL cnt=0;
		rep(i,1,a[n])
		{
			rep(j,1,cnt) f[i]=maxx(f[i],f[i-a[j]]);
			while(i==a[++cnt]) if(f[i]==1) ans--;
			cnt--;
			f[i]=maxx(f[i],f[i-a[cnt]]);
		}
		printf("%lld\n",ans);
	}
	
	return 0;
}
