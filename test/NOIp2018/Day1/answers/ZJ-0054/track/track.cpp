#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define rap(i,j,k) for(int i=j;i<=k;++i)
#define rapp(i,j,k) for(int i=j;i>=k;--i)
#define dep(i,x) for(int i=link[x];i;i=e[i].n)
const int N=50010;
int n,m,link[N],toto,A,B,dis[N],sum,cnt,ola[N<<1],fir[N],mi[20][N<<1],Dep[N],num,dfn[N],t,a[N],maxn;
bool flag_line=1;
struct node{
	int y,n,v;
}e[N<<1];
inline int read(){
	int NUM=0; bool f=1;
	char c=getchar();
	for(;c<'0'||c>'9';c=getchar())
	if(c=='-')f=0;
	for(;c>='0'&&c<='9';c=getchar())
	NUM=(NUM<<1)+(NUM<<3)+c-48;
	return f?NUM:-NUM;
}
void insert(int x,int y,int z){
	e[++toto].y=y;
	e[toto].v=z;
	e[toto].n=link[x];
	link[x]=toto;
}
void init(){
	n=read();m=read();
	rap(i,1,n-1){
		int x=read(),y=read(),z=read();
		if(y!=x+1) flag_line=0;
		insert(x,y,z); insert(y,x,z);
	}
}
void dfs(int x,int fa,int &a){
	dep(i,x){
		int y=e[i].y;
		if(y==fa)continue;
		dis[y]=dis[x]+e[i].v;
		if(dis[a]<dis[y]) a=y;
		dfs(y,x,a);
	}
}
void find(int x,int fa,int X){
	dep(i,x){
		int y=e[i].y;
		if(y==fa)continue;
		sum+=e[i].v;
		if(sum>=X)sum=0,cnt++;
		find(y,x,X);
	}
}
bool check(int x){
	sum=0;cnt=0;
	find(A,0,x);
	if(cnt>=m)return 1;
	return 0;
}
void LP(int x,int fa){
	Dep[x]=Dep[fa]+1; dfn[x]=++t;
	ola[++num]=x; fir[x]=num;
	dep(i,x){
		int y=e[i].y;
		if(y==fa)continue;
		dis[y]=dis[x]+e[i].v;
		LP(y,x);
		ola[++num]=x;
	}
}
int LCA(int x,int y){
	x=fir[x];y=fir[y];
	if(x>y) swap(x,y);
	int k=log2(y-x+1);
	return Dep[mi[k][x]]<Dep[mi[k][y-(1<<k)+1]]?mi[k][x]:mi[k][y-(1<<k)+1];
}
int D(int x,int y){
	return dis[x]+dis[y]-2*dis[LCA(x,y)];
}
bool cmp(int x,int y){return dfn[x]<dfn[y];}
bool CHECK(int x){
	int now=1;cnt=0;
	rap(i,1,n){
		if(D(now,i)>=x){
			cnt++;now=i+1;
		}
	}
	if(cnt>=m)return 1;
	cnt=0;
	now=0;
	rap(i,0,n-1){
		if(D(now,i)>=x){
			cnt++;now=i+1;
		}
	}
	if(cnt>=m)return 1;
	return 0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	init();
	dfs(1,0,A);
	dis[A]=0;
	dfs(A,0,B);
	maxn=dis[B];
	if(m==1){
		printf("%d",dis[B]);
		return 0;
	}
	if(flag_line){
		int l=1,r=dis[B];
		while(l+1<r){
			int mid=(l+r)>>1;
			if(check(mid)) l=mid;
			else r=mid;
		}
		if(check(r))printf("%d",r);
		else printf("%d",l);
		return 0;
	}
	dis[1]=0;
	LP(1,0);
	rap(i,1,num) mi[0][i]=ola[i];
	for(int i=1;(1<<i)<=num;++i) for(int j=1;j+(1<<i)-1<=num;++j)
		mi[i][j]=Dep[mi[i-1][j]]<Dep[mi[i-1][j+(1<<(i-1))]]?mi[i-1][j]:mi[i-1][j+(1<<(i-1))];
	rap(i,1,n) a[i]=i;
	sort(a+1,a+1+n,cmp);
	a[0]=a[n];
	int l=1,r=maxn;
	while(l+1<r){
		int mid=(l+r)>>1;
		if(CHECK(mid)) l=mid;
		else r=mid;
	}
	if(CHECK(r)) printf("%d",r);
	else printf("%d",l);
	return 0;
}
