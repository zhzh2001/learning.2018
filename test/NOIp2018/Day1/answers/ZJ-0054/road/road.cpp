#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define rap(i,j,k) for(int i=j;i<=k;++i)
#define rapp(i,j,k) for(int i=j;i>=k;--i)
#define dep(i,x) for(int i=link[x];i;i=e[i].n)
const int N=100010;
int st[N],top,n,a[N];
ll ans;
inline int read(){
	int NUM=0; bool f=1;
	char c=getchar();
	for(;c<'0'||c>'9';c=getchar())
	if(c=='-')f=0;
	for(;c>='0'&&c<='9';c=getchar())
	NUM=(NUM<<1)+(NUM<<3)+c-48;
	return f?NUM:-NUM;
}
void init(){
	n=read();
	rap(i,1,n) a[i]=read();
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	init();
	rap(i,1,n){
		int now=0;
		while(top>0&&st[top]>=a[i]){
			now=max(now,st[top]-a[i]);
			top--;
		}
		ans+=now;
		st[++top]=a[i];
	}
	ans+=st[top];
	printf("%lld",ans);
	return 0;
}
