#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define rap(i,j,k) for(int i=j;i<=k;++i)
#define rapp(i,j,k) for(int i=j;i>=k;--i)
#define dep(i,x) for(int i=link[x];i;i=e[i].n)
int T,n,a[110],ans,dp[25010],maxn;
inline int read(){
	int NUM=0; bool f=1;
	char c=getchar();
	for(;c<'0'||c>'9';c=getchar())
	if(c=='-')f=0;
	for(;c>='0'&&c<='9';c=getchar())
	NUM=(NUM<<1)+(NUM<<3)+c-48;
	return f?NUM:-NUM;
}
void init(){
	n=read();
	ans=n;
	rap(i,1,n) a[i]=read();
	sort(a+1,a+1+n);
	maxn=a[n];
}
void work(){
	dp[0]=1;
	rap(i,1,maxn) dp[i]=0;
	rap(i,1,n) dp[a[i]]=2;
	rap(i,1,n){
		if(dp[a[i]]==1)continue;
		dp[a[i]]=1;
		rap(j,0,maxn-a[i]){
			if(dp[j]==1){
				if(dp[j+a[i]]==2)ans--;
				dp[j+a[i]]=1;
			}
		}
	}
	printf("%d\n",ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	rap(i,1,T){
		init();
		work();
	}
	return 0;
}
