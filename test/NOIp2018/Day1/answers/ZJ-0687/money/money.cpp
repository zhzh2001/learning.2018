#include<bits/stdc++.h>
using namespace std;
int T,t,b[110],n,a[110],m;
bool b1[110],b2[110];
inline int read()
{
	int num=0,flag=1;
	char c=getchar();
	for (;c<'0'||c>'9';c=getchar())
	if (c=='-') flag=-1;
	for (;c>='0'&&c<='9';c=getchar())
	num=(num<<3)+(num<<1)+c-48;
	return num*flag;
}
void chushihua()
{
	n=0;
	m=0;
	memset(b1,1,sizeof(b1));
	memset(b2,1,sizeof(b2));
}
void init()
{
	t=read();
	for (int i=1;i<=t;++i)
	b[i]=read();
	sort(b+1,b+t+1);
	for (int i=1;i<t;++i)
	for (int j=i+1;j<=t;++j)
	if (b[j]%b[i]==0) b1[j]=false;
	for (int i=1;i<=t;++i)
	if (b1[i]) a[++n]=b[i];
	sort(a+1,a+n+1);
}
void work1()
{
	m=n;
	printf("%d\n",m);
}
void work2()
{
	m=3;
	for (int i=0;i<=a[3]/a[1];++i)
	for (int j=0;j<=a[3]/a[2];++j)
	if (i*a[1]+j*a[2]==a[3]) m=2;
	printf("%d\n",m);
}
void work3()
{
	for (int k=3;k<=n;++k)
	for (int i=0;i<=a[k]/a[1];++i)
	for (int j=0;j<=a[k]/a[2];++j)
	if (i*a[1]+j*a[2]==a[k]) b2[k]=false;
	if (b2[3])
	{
		for (int i=0;i<=a[4]/a[1];++i)
		for (int j=0;j<=a[4]/a[3];++j)
		if (i*a[1]+j*a[3]==a[4]) b2[4]=false;
		for (int i=0;i<=a[4]/a[2];++i)
		for (int j=0;j<=a[4]/a[3];++j)
		if (i*a[2]+j*a[3]==a[4]) b2[4]=false;
	}
	for (int i=1;i<=n;++i)
	if (b2[i]) m++;
	printf("%d\n",m);
}
void work4()
{
	for (int k=3;k<=n;++k)
	for (int i=0;i<=a[k]/a[1];++i)
	for (int j=0;j<=a[k]/a[2];++j)
	if (i*a[1]+j*a[2]==a[k]) b2[k]=false;
	if (b2[3])
	{
		for (int k=4;k<=n;++k)
		for (int i=0;i<=a[k]/a[1];++i)
		for (int j=0;j<=a[k]/a[3];++j)
		if (i*a[1]+j*a[3]==a[k]) b2[k]=false;
		for (int k=4;k<=n;++k)
		for (int i=0;i<=a[k]/a[2];++i)
		for (int j=0;j<=a[k]/a[3];++j)
		if (i*a[2]+j*a[3]==a[k]) b2[k]=false;
	}
	if (b2[4])
	{
		for (int i=0;i<=a[5]/a[1];++i)
		for (int j=0;j<=a[5]/a[4];++j)
		if (i*a[1]+j*a[3]==a[5]) b2[5]=false;
		for (int i=0;i<=a[5]/a[2];++i)
		for (int j=0;j<=a[5]/a[4];++j)
		if (i*a[2]+j*a[3]==a[5]) b2[5]=false;
		for (int i=0;i<=a[5]/a[3];++i)
		for (int j=0;j<=a[5]/a[4];++j)
		if (i*a[2]+j*a[3]==a[5]) b2[5]=false;
	}
	for (int i=1;i<=n;++i)
	if (b2[i]) m++;
	printf("%d\n",m);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while (T--)
	{
		chushihua();
		init();
		if (n<=2) work1();
		if (n==3) work2();
		if (n==4) work3();
		if (n==5) work4();
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
