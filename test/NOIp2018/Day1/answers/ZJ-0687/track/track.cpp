#include<bits/stdc++.h>
using namespace std;
const int maxn=50010;
int n,m,x,y,z,tot,head[maxn],d[maxn],ans;
bool b[maxn];
struct atree
{
	int y,v,next;
}
a[maxn<<1];
inline int read()
{
	int num=0,flag=1;
	char c=getchar();
	for (;c<'0'||c>'9';c=getchar())
	if (c=='-') flag=-1;
	for (;c>='0'&&c<='9';c=getchar())
	num=(num<<3)+(num<<1)+c-48;
	return num*flag;
}
void add(int x,int y,int z)
{
	a[++tot].y=y;
	a[tot].v=z;
	a[tot].next=head[x];
	head[x]=tot;
}
void init()
{
	n=read();
	m=read();
	for (int i=1;i<n;++i)
	{
		x=read();
		y=read();
		z=read();
		add(x,y,z);
		add(y,x,z);
	}
}
void dp(int x)
{
	b[x]=true;
	for (int i=head[x];i;i=a[i].next)
	{
		int y=a[i].y,z=a[i].v;
		if (b[y]) continue;
		dp(y);
		ans=max(ans,d[x]+d[y]+z);
		d[x]=max(d[x],d[y]+z);
	}
}
void work()
{
	dp(1);
	printf("%d\n",ans);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	init();
	work();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
