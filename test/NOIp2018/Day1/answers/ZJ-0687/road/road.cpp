#include<bits/stdc++.h>
using namespace std;
int n,a[100010],x,y,minn,midd;
long long ans;
struct tree
{
	int mi,dat;
}
t[400010];
inline int read()
{
	int num=0,flag=1;
	char c=getchar();
	for (;c<'0'||c>'9';c=getchar())
	if (c=='-') flag=-1;
	for (;c>='0'&&c<='9';c=getchar())
	num=(num<<3)+(num<<1)+c-48;
	return num*flag;
}
void init()
{
	n=read();
	for (int i=1;i<=n;++i)
	a[i]=read();
}
void build(int l,int r,int o)
{
	if (l==r)
	{
		t[o].mi=a[l];
		t[o].dat=l;
		return;
	}
	int mid=(l+r)>>1;
	build(l,mid,(o<<1));
	build(mid+1,r,(o<<1)+1);
	if (t[(o<<1)].mi<t[(o<<1)+1].mi) t[o].dat=t[(o<<1)].dat;
	else t[o].dat=t[(o<<1)+1].dat;
	t[o].mi=min(t[(o<<1)].mi,t[(o<<1)+1].mi);
}
void ask(int l,int r,int o)
{
	if (x<=l&&y>=r)
	{
		if (t[o].mi<minn)
		{
			minn=t[o].mi;
			midd=t[o].dat;
		}
		return;
	}
	int mid=(l+r)>>1;
	if (x<=mid) ask(l,mid,(o<<1));
	if (y>mid) ask(mid+1,r,(o<<1)+1);
}
void dfs(int l,int r,int del)
{
	x=l;
	y=r;
	minn=(1<<30);
	ask(1,n,1);
	int ans1=minn,ans2=midd;
	ans+=ans1-del;
	if (l<=ans2-1) dfs(l,ans2-1,ans1);
	if (ans2+1<=r) dfs(ans2+1,r,ans1);
}
void work()
{
	build(1,n,1);
	dfs(1,n,0);
	printf("%lld\n",ans);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	init();
	work();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
