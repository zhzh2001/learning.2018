#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
using namespace std;

char tc(){static char tr[10000],*A=tr,*B=tr;return A==B&&(B=(A=tr)+fread(tr,1,10000,stdin),A==B)?EOF:*A++;}
void read(int &x){char c;for(;!isdigit(c=tc()););for(x=c-48;isdigit(c=tc());x=(x<<1)+(x<<3)+c-48);}

const int V=2e2+5;
int T,N,A[V],F[25005],vis[V],Ne[V],Cnt,Ans,Mx;
void init(void){
	memset(F,0,sizeof F);
	memset(A,0,sizeof A);
	memset(vis,0,sizeof vis);
	memset(Ne,0,sizeof Ne);
	Cnt=Ans=Mx=0;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(read(T);T;T--){
		read(N);
		for(int i=1;i<=N;i++)read(A[i]),Mx=max(Mx,A[i]);
		N=unique(A+1,A+N+1)-A-1;
		for(int i=1;i<=N;i++)
			for(int j=1;j<=N;j++)if(i^j)
				if(A[j]>A[i]&&!(A[j]%A[i]))vis[j]=1;
		for(int i=1;i<=N;i++)if(!vis[i])Ne[++Cnt]=A[i];
		sort(Ne+1,Ne+Cnt+1),Ans=Cnt,F[0]=1;
		for(int i=1;i<=Cnt;i++){
			if(F[Ne[i]])--Ans;
			for(int j=Ne[i];j<=Mx;j++)F[j]|=F[j-Ne[i]];
		}
		printf("%d\n",Ans),init();
	}
	return 0;
}
