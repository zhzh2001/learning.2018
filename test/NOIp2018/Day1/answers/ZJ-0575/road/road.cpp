#include <cstdio>
#include <cctype>
#include <cstring>
using namespace std;

char tc(){static char tr[10000],*A=tr,*B=tr;return A==B&&(B=(A=tr)+fread(tr,1,10000,stdin),A==B)?EOF:*A++;}
void read(int &x){char c;for(;!isdigit(c=tc()););for(x=c-48;isdigit(c=tc());x=(x<<1)+(x<<3)+c-48);}

const int V=2e5+5;
long long Ans;
int N,A[V],Lf[V],Rf[V],l[V],h=1,t=1;

void dfs(int L,int R,int Mx){
	if(L>R)return ;
	if(L==R)return (void)(Ans+=A[L]-Mx);
	for(int i=L,j=R;i<=j;i++,j--){
		if(Lf[i]<L&&Rf[i]>R)return (void)(Ans+=A[i]-Mx,dfs(L,i-1,A[i]),dfs(i+1,R,A[i]));
		if(Lf[j]<L&&Rf[j]>R)return (void)(Ans+=A[j]-Mx,dfs(L,j-1,A[j]),dfs(j+1,R,A[j]));
	}
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(N),A[0]=2e9;
	for(int i=1;i<=N;i++)read(A[i]),Rf[i]=N+1;
	for(int i=1;i<=N;i++){
		while(h<=t&&A[l[t]]>A[i])Rf[l[t]]=i,t--;
		l[++t]=i;
	}
	h=t=1;memset(l,0,sizeof l);
	for(int i=N;i;i--){
		while(h<=t&&A[l[t]]>A[i])Lf[l[t]]=i,t--;
		l[++t]=i;
	}
	dfs(1,N,0);
	printf("%lld\n",Ans);
	return 0;
}
