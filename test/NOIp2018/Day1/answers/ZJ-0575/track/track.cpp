#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
using namespace std;

char tc(){static char tr[10000],*A=tr,*B=tr;return A==B&&(B=(A=tr)+fread(tr,1,10000,stdin),A==B)?EOF:*A++;}
void read(long long &x){char c;for(;!isdigit(c=tc()););for(x=c-48;isdigit(c=tc());x=(x<<1)+(x<<3)+c-48);}

const int V=1e5+5;
long long N,M,x,y,c,L,R,Mid,Ans,Mx,Gi[V],tim,A[V];
long long head[V],nxt[V<<1],to[V<<1],w[V<<1],Cnt;
#define Add(x,y,c) (nxt[++Cnt]=head[x],to[Cnt]=y,w[Cnt]=c,head[x]=Cnt)


long long dis[V];
void dfs(long long x,long long fa){
	for(long long i=head[x];i;i=nxt[i])if(to[i]^fa)dis[to[i]]=dis[x]+w[i],dfs(to[i],x);
}
void Work(void){
	dfs(1,0);for(long long i=1;i<=N;i++)if(dis[i]>dis[Mx])Mx=i;
	dis[Mx]=0,dfs(Mx,0),Mx=0;for(long long i=1;i<=N;i++)if(dis[i]>dis[Mx])Mx=i;
	return (void)(printf("%lld\n",dis[Mx]));
}

bool Check(long long Mid){
	long long pre=0,cnt=0;
	for(long long i=1;i<=N;i++){
		pre+=A[i];
		if(pre>=Mid)cnt++,pre=0;
	}
	return cnt>=M;
}

void dfs_build(long long x,long long fa){
	for(long long i=head[x];i;i=nxt[i])if(to[i]^fa)A[++tim]=w[i],dfs_build(to[i],x);
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(N),read(M);
	for(long long i=1;i<N;i++){
		read(x),read(y),read(c),Gi[y]++,Gi[x]++;
		Add(x,y,c),Add(y,x,c),R+=c;
	}
	if(M==1)return Work(),0;
	for(long long i=1;i<=N;i++)if(Gi[i]==1){dfs_build(i,0),--N;break;}
	for(;L<=R;){
		Mid=(L+R)>>1;
		if(Check(Mid))L=Mid+1,Ans=Mid;
		else R=Mid-1;
	}
	printf("%lld\n",Ans);
	return 0;
}
