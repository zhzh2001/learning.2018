#include <bits/stdc++.h>
using namespace std;
int T,n;
int a[1000];
bool f[1000000];
bool cmp(int x,int y)
{
	return x<y;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--)
		{
			memset(f,false,sizeof(f));
			scanf("%d",&n);
			int ma=0;
			for (int i=1;i<=n;i++)
				scanf("%d",&a[i]),ma=max(ma,a[i]);
			sort(a+1,a+1+n,cmp);
	    	f[0]=true;
	    	int tot=0;
			for (int i=1;i<=n;i++)
				if (f[a[i]]==false)
					{
						tot++;
						for (int j=0;j<=ma;j++)
							if (f[j] && a[i]+j<=ma)
								f[a[i]+j]=true;
					}	
			printf("%d\n",tot);			
		} 	
}
