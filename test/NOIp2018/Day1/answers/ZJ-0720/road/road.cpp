#include<bits/stdc++.h>
using namespace std;
int n;
int a[100010];
struct node{
	int x,y,z;
}b[1000010];
struct node1{
	int fa,mi;
}lc[100010][20];
int minx(int x,int y)
{
	if (a[x]<=a[y])
		return x;
	else
		return y;	
} 
int mi(int x,int y)
{
	int q=x;
	for (int i=18;i>=0;i--)
		if (lc[x][i].fa<=y && lc[x][i].fa!=0)
			{
				q=minx(q,lc[x][i].mi);
				x=lc[x][i].fa;
			}
	return q;		
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	for (int i=1;i<n;i++)
		lc[i][0].fa=i+1,lc[i][0].mi=minx(i,i+1);
	for (int i=1;i<=18;i++)
		for (int j=1;j<=n;j++)
			{
				lc[j][i].mi=minx(lc[lc[j][i-1].fa][i-1].mi,lc[j][i-1].mi);
				lc[j][i].fa=lc[lc[j][i-1].fa][i-1].fa;
			}	
	int l=1;
	int r=1;
	b[1].x=1;
	b[1].y=n;
	b[1].z=0;
	int ans=0;
	while (l<=r)
		{
			if (b[l].x==b[l].y)
				{
					ans+=a[b[l].x]-b[l].z;
					l++;
					continue;
				}
			if (b[l].x==b[l].y-1)
				{
					ans+=max(a[b[l].x],a[b[l].y])-b[l].z;
					l++;
					continue;
				}				
			int p=mi(b[l].x,b[l].y);
			//cout<<p<<endl;
			ans+=a[p]-b[l].z;
			if (b[l].x<=p-1)
			{
			r++;
			b[r].x=b[l].x;
			b[r].y=p-1;
			b[r].z=a[p];
			}
			//cout<<b[r].x<<' '<<b[r].y<<' '<<b[r].z<<endl;	
			if (p+1<=b[l].y)
			{
			r++;
			b[r].x=p+1;
			b[r].y=b[l].y;
			b[r].z=a[p];
			}
			//cout<<b[r].x<<' '<<b[r].y<<' '<<b[r].z<<endl;
			l++;			
		}
	printf("%d",ans);	
}
