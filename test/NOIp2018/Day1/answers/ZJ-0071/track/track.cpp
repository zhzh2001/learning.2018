#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 50010
#define K 0.1818
using namespace std;

struct Edge {
	int nxt,to,val;
} e[N<<1];

int head[N],n,m,z,ans,f[N],g[N],c[N],cnt,tot,dis[1010][1010];
bool vis[N];

void add(int u,int v,int w) {
	e[++tot].nxt=head[u];
	e[tot].to=v;
	e[tot].val=w;
	head[u]=tot;
}

void dfs(int u,int s) {
	vis[u]=1;
	if (s>ans) {
		ans=s; z=u;
	}
	for (int i=head[u]; i; i=e[i].nxt) {
		int v=e[i].to;
		if (vis[v]) continue;
		dfs(v,s+e[i].val);
	}
}

void dffs(int u) {
	vis[u]=1;
	for (int i=head[u]; i; i=e[i].nxt) {
		int v=e[i].to;
		if (vis[v]) continue;
		c[++cnt]=e[i].val;
		dffs(v);
	}
}

void ddfs(int u,int x) {
	vis[u]=1;
	for (int i=head[u]; i; i=e[i].nxt) {
		int v=e[i].to;
		if (vis[v]) continue;
		dis[x][v]=dis[x][u]+e[i].val;
		ddfs(v,x);
	}
}

int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool f1=1,f2=1;
	for (int i=1; i<n; i++) {
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		if (v!=u+1) f1=0;
		if (u!=1) f2=0;
		f[u]++; f[v]++;
		add(u,v,w);
		add(v,u,w);
		g[i]=w;
	}
	if (m==1) {
		dfs(1,0); ans=0; 
		memset(vis,0,sizeof(vis));
		dfs(z,0);
		printf("%d\n",ans);
	} else if (f1) {
		for (int i=1; i<=n; i++) if (f[i]==1) z=i;
		dffs(z);
		int l=0,r=500000000;
		while (l<r) {
			int mid=(l+r)>>1;
			int sum=0,zh=0;
			for (int i=1; i<=cnt+1; i++) {
				if (zh+c[i]>=mid) {
					sum++;
					zh=0;
				} else zh+=c[i];
			}
			if (sum>=m) l=mid+1;
			else r=mid;
		}
		printf("%d\n",l-1);
	} else if (f2) {
		std::sort(g+1,g+n);
		int l=0,r=500000000;
		while (l<r) {
			int mid=(l+r)>>1;
			int sum=0,qd=0;
			for (int i=n-1; i>0; i--) {
				while (g[qd]+g[i]<mid&&qd<i) qd++;
				if (qd>=i) break;
				sum++;
				if (g[i]<mid) qd++;
			}
			if (sum>=m) l=mid+1;
			else r=mid;
		}
		printf("%d\n",l-1);
	} else {
		for (int i=1; i<=n; i++) ddfs(i,i);
		int l=0,r=500000000;
		while (l<r) {
			int mid=(l+r)>>1;
			cnt=0;
			for (int i=1; i<=n; ++i)
				for (int k=1; k<=n; ++k)
					if (dis[i][k]>=mid) cnt++;
			if (cnt*K>=m) l=mid+1;
			else r=mid;
		}
		printf("%d\n",l-1);
	}
	return 0;
}
