#include<cstdio>
#include<cstring>
#include<algorithm>

int T,n,a[110],vis[25010];

int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		scanf("%d",&n);
		for (int i=1; i<=n; ++i) {
			scanf("%d",&a[i]);
		}
		std::sort(a+1,a+n+1);
		memset(vis,0,sizeof(vis));
		vis[0]=1;
		for (int i=1; i<=n; ++i)
			for (int k=a[i]; k<=25000; ++k)
				if (vis[k-a[i]]) vis[k]++;
		int ans=0;
		for (int i=1; i<=n; ++i) if (vis[a[i]]==1) ans++;
		printf("%d\n",ans);
	}
	return 0;
}
