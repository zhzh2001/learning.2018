#include<map>
#include<queue>
#include<vector>
#include<stdio.h>
#include<cstdio>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=105,Max=25005;
int f[Max],a[N],n,Ans,Max2;
inline int Read(){
	char c=getchar(); int t=0;
	for (;c>57||c<48;c=getchar());
	for (;c>47&&c<58;c=getchar()) t=t*10+c-48;
	return t;
}	
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (int T=Read();T--;){
		for (int i=0;i<Max;i++) f[i]=0;
		Ans=n=Read(); Max2=0;
		for (int i=1;i<=n;i++)
			a[i]=Read(),Max2=max(Max2,a[i]);
		sort(a+1,a+n+1); f[0]=1;
		for (int i=1;i<=n;i++){
			if (f[a[i]]){Ans--; continue;}
		//	printf("%d\n",a[i]);
			for (int j=a[i];j<=Max2;j++) f[j]|=f[j-a[i]];//printf("f  %d %d\n",i,f[i]);
		}
		printf("%d\n",Ans);
	}
}
