#include<map>
#include<queue>
#include<vector>
#include<stdio.h>
#include<cstdio>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=5e5+5,MAXN=5e8+5,M=2005;
struct T{int next,to,val;} edge[N];
int head[N],n,m,nedge,f[N],Line[N],flag,num,tot,a[N],d[N],Ans;
int dp[M][M],g[N];
inline int Read(){
	char c=getchar(); int t=0;
	for (;c>57||c<48;c=getchar());
	for (;c>47&&c<58;c=getchar()) t=t*10+c-48;
	return t;
}
inline int addline(int x,int y,int z){
	edge[++nedge].next=head[x],edge[nedge].to=y,edge[nedge].val=z,head[x]=nedge;
}
int Dfs(int p,int F){
	for (int i=head[p];i;i=NX)
		if (O!=F) a[++tot]=edge[i].val,Dfs(O,p);
}
int Work(int p,int F){
//	printf("Work %d %d\n",p,F);
	int Max1=0,Max2=0;
	for (int i=head[p];i;i=NX)
		if (O!=F){//printf("Next:%d %d %d\n",p,O,i);
			Work(O,p);
			f[p]=max(f[p],f[O]);
			int num=Line[O]+edge[i].val;
			if (num>Max1) Max2=Max1,Max1=num;
			else Max2=max(Max2,num);
		}//	printf("%d %lld %lld %lld\n",p,Max1,Max2,f[p]);
	f[p]=max(f[p],Max1+Max2); Line[p]=Max1;
}
int Dp(int p,int F,int v){int tot=0,poi[3],E[3];
//	printf("DP:%d %d %d\n",p,F,v);
	for (int i=head[p];i;i=NX)
		if (O!=F){poi[++tot]=O,E[tot]=edge[i].val; Dp(O,p,v);}
	if (tot==0){dp[p][0]=0; return 0;}
	if (tot==1){
		for (int i=0;i<=m;i++){
			if (dp[poi[1]][i]>=0) dp[p][i]=max(dp[p][i],dp[poi[1]][i]+E[1]);
			if (dp[p][i]>=v) dp[p][i+1]=0;
		}
	}
	if (tot==2){int st=0;
		for (int i=0;i<=m;i++)
			if (dp[poi[1]][i]>v||dp[poi[2]][i]>v) dp[p][i]=v+1,st=i;
		for (int i=st;i<=m;i++){
			for (int j=0;j<=i;j++)
				if (dp[poi[1]][j]>=0&&dp[poi[2]][i-j]>=0){
					int xx=dp[poi[1]][j]+E[1],yy=dp[poi[2]][i-j]+E[2];
					dp[p][i]=max(xx,yy);
					if (xx+yy>=v) dp[p][i+1]=0;
				}
			if (dp[p][i]<0) break;
		}
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=Read(),m=Read(); flag=1;
	int flag2=1;
//	printf("%d %d\n",N,MAXN);
	int Min=MAXN;
	for (int i=1;i<n;i++){
		int x=Read(),y=Read(),z=Read();
		Min=min(Min,z); flag2&=(x==1);
		flag&=(y==x+1); d[x]++,d[y]++;
		addline(x,y,z),addline(y,x,z); g[i]=z;
	}
	if (m==n-1){printf("%d\n",Min); return 0;}
	if (m==1){Work(1,0); printf("%d\n",f[1]); return 0;}
	if (flag2){
		sort(g+1,g+n);
		for (int l=0,r=MAXN;l<r;){
			int mid=(l+r)>>1; tot=0; int L=1;
			for (int i=n;i>L;i--){
				if (g[i]>mid){tot++; continue;}
				for (;g[L]+g[i]<mid&&i>L;L++);
				if (i>L) tot++;
			}
			if (tot>=m) Ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",Ans); return 0;
	}
	if (flag){
		for (int i=1;i<=n;i++)
			if (d[i]==1){tot=0; Dfs(i,0); break;}
//		for (int i=1;i<n;i++) printf("%d ",a[i]); printf("\n");
		for (int l=0,r=MAXN;l<r;){
			int mid=(l+r)>>1; num=tot=0;
			for (int i=1;i<n;i++){
				num+=a[i];
				if (num>=mid) num=0,tot++;
			}
//			printf("%d %d %d %d\n",l,r,mid,tot);
			if (tot>=m) Ans=mid,l=mid+1;
			else r=mid;
		}
		printf("%d\n",Ans); return 0;
	}
	flag=1;
	for (int i=1;i<=n;i++) flag&=(d[i]<=3);//printf("%d %d\n",d[i],flag); printf("sd");
	if (flag&&n<=1000){
		for (int l=0,r=MAXN;l<r;){
			//printf("+++++++++++++++++++++++++++");
			int mid=(l+r)>>1;
			for (int i=0;i<M;i++)
				for (int j=0;j<M;j++) dp[i][j]=-MAXN;
			Dp(1,0,mid);
			if (dp[1][m]>=0) Ans=mid,l=mid+1;
			else r=mid;
		}
		printf("%d\n",Ans); return 0;
	}
	printf("%d\n",g[4]+g[5]+g[6]+g[10]);
}
