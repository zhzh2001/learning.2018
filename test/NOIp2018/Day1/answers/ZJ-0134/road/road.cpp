#include<map>
#include<queue>
#include<vector>
#include<stdio.h>
#include<cstdio>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
int n,t,Last,Ans;
inline int Read(){
	char c=getchar(); int t=0;
	for (;c>57||c<48;c=getchar());
	for (;c>47&&c<58;c=getchar()) t=t*10+c-48;
	return t;
}	
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=Read();
	for (int i=1;i<=n;i++)
		t=Read(),Ans+=max(0,t-Last),Last=t;
	printf("%d\n",Ans);
}
