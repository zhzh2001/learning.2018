var
  n,ans:int64;
  i:longint;
  a:array[0..1100000] of longint;
begin
  assign(input,'road.in'); reset(input);
  assign(output,'road.out'); rewrite(output);
  readln(n);
  for i:=1 to n do 
  begin
    read(a[i]);
    if i=1 then ans:=a[1]
	else 
	begin
	  if a[i]>a[i-1] then ans:=ans+(a[i]-a[i-1]);
	end;
  end;
  writeln(ans);
  close(input); close(output);
end.