var
  ans,maxn,i,t,n:longint;
  a:array[0..1000] of longint;
  f:array[0..25000] of boolean;
procedure sort(l,r: longint);
var  i,j,x,y: longint;
begin
	i:=l;  j:=r;
	x:=a[(l+r) div 2];
	repeat
		while a[i]<x do inc(i);
		while x<a[j] do dec(j);
		if not(i>j) then
		begin
			y:=a[i]; a[i]:=a[j]; a[j]:=y;
			inc(i); j:=j-1;
		end;
	until i>j;
	if l<j then sort(l,j);
	if i<r then sort(i,r);
end;
function check(x:longint):longint;
var i,j:longint;
begin
  if f[x]=false then check:=1
  else exit(0);
  for i:=1 to maxn do 
  if x*i<=maxn then f[x*i]:=true
  else break;
  for i:=1 to maxn do 
  begin
    if f[i]=true then 
	begin
		for j:=1 to maxn do 
		if j*x+i>maxn then break
			else f[j*x+i]:=true;
	end;
  end;
	exit(check);
end;
begin
  assign(input,'money.in'); reset(input);
  assign(output,'money.out'); rewrite(output);
  readln(t);
  while t<>0 do 
  begin
    dec(t);
    readln(n);
	maxn:=0;  ans:=0;
	for i:=1 to n do
	begin 
	  read(a[i]);
	  if maxn<a[i] then maxn:=a[i];
    end;
	for i:=1 to maxn do f[i]:=false;
    sort(1,n);
    for i:=1 to n do 
	ans:=ans+check(a[i]);
	writeln(ans);
  end;
  close(input); close(output);
end.