var
  tot,num,n,m,i,u,v,ww,mid,ans,maxx:longint;
  l,r:int64;
  w,head,vet,next,weight,son:array[0..1000000] of longint;
function max(a,b:longint):longint;
begin
  if a>b then exit(a)
  else exit(b);
end;
procedure add(u,v,w:longint);
begin
  inc(tot);
  next[tot]:=head[u];
  head[u]:=tot;
  vet[tot]:=v;
  weight[tot]:=w;
end;
procedure dfs(id,father,kk:longint);
var point:longint;
begin
  if kk>maxx then begin u:=id; maxx:=kk; end;
  point:=head[id];
  while point<>0 do 
  begin
    if vet[point]<>father then dfs(vet[point],id,kk+weight[point]);
    point:=next[point];
  end;
end;
procedure  check(id,father:longint);
var x,point:longint;
begin
  w[id]:=0; x:=0;
  son[1]:=0; son[2]:=0;
  point:=head[id];
  while point<>0 do
  begin
    if vet[point]<>father then 
    begin
	   check(vet[point],id);
		w[vet[point]]:=w[vet[point]]+weight[point];
        if w[vet[point]]>=mid then begin w[vet[point]]:=0; inc(num); end;
		inc(x); son[x]:=w[vet[point]];
    end;
   point:=next[point];
  end;
  if (son[1]+son[2]>=mid) and (x>=2) then begin inc(num); w[id]:=0; end
  else  w[id]:=max(son[1],son[2]);
end;
begin
  assign(input,'track.in'); reset(input);
  assign(output,'track.out'); rewrite(output);
  readln(n,m);
  for i:=1 to n-1 do 
  begin
    readln(u,v,ww);
    add(u,v,ww);
	add(v,u,ww);
  end;
  if m=1 then 
  begin
	maxx:=0;
    dfs(1,0,0);
	maxx:=0;
    dfs(u,0,0);
	writeln(maxx);
    close(input); close(output);
	halt;
  end;
  l:=1; r:=500000000;
  while l<=r do 
  begin
    num:=0;
    mid:=(l+r) div 2;
	check(1,0);
	if num>=m then ans:=mid;
    if num>=m then l:=mid+1
	else r:=mid-1;
  end;
  writeln(ans);
  close(input); close(output);
end.