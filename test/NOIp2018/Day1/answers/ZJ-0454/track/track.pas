var i,j,k,l,m,n,s,t,x,y,z:longint;
a,d:array[0..3000,0..3000]of longint;
p,nex:array[0..10000]of longint;
procedure dfs2(t,u,dd,dm:longint);
begin
 if m-u>n-t then exit;
 if u>m then exit;
 if (t=n)and(u=m) then
 begin
  if dd<dm then dm:=dd;
  if dm>s then s:=dm;
  exit;
 end;
 if dd<dm
  then dfs2(t+1,u+1,nex[t],dd)
  else dfs2(t+1,u+1,nex[t],dm);
 dfs2(t+1,u,dd+nex[t],dm);
end;
procedure dfs(t,dd:longint);
var i:longint;
begin
 if dd>s then s:=dd;
 for i:=1 to a[t,0] do
  if p[a[t,i]]=0 then
  begin
   p[a[t,i]]:=1;
   dfs(a[t,i],dd+d[t,i]);
   p[a[t,i]]:=0;
  end;
end;
begin
 assign(input,'track.in');
 reset(input);
 assign(output,'track.out');
 rewrite(output);
 read(n,m);
 for i:=1 to n-1 do
 begin
  read(x,y,z);
  inc(a[x,0]);
  a[x,a[x,0]]:=y;
  d[x,a[x,0]]:=z;
  inc(a[y,0]);
  a[y,a[y,0]]:=x;
  d[y,a[y,0]]:=z;
 end;
 if m=1 then
 begin
  for i:=1 to n do
  begin
   s:=0;
   p[i]:=1;
   dfs(i,0);
   p[i]:=0;
   if s>k then k:=s;
  end;
  writeln(k);
  close(input);
  close(output);
  halt;
 end;
 t:=0;
 for i:=1 to n do
 begin
  if i=1 then
  begin
   if (a[i,0]<>1)or(a[i,1]<>2) then
   begin
    t:=1;
    break;
   end else nex[i]:=d[i,1];
  end;
  if i=n then
  begin
   if (a[i,0]<>1)or(a[i,1]<>n-1) then
   begin
    t:=1;
    break;
   end else nex[i]:=0;
  end;
  if (i<>1)and(i<>n) then
  begin
   if a[i,0]<>2 then
   begin
    t:=1;
    break;
   end;
   x:=a[i,1];
   y:=a[i,2];
   if not((x=i-1)and(y=i+1)or(x=i+1)and(y=i-1)) then
   begin
    t:=1;
    break;
   end else
   begin
    if x=i+1
     then nex[i]:=d[i,1]
     else nex[i]:=d[i,2];
   end;
  end;
 end;
 if t=0 then
 begin
  dfs2(1,1,0,maxlongint div 10);
  writeln(s);
  close(input);
  close(output);
  halt;
 end;
 close(input);
 close(output);
end.