var i,j,k,l,m,n,s,t,x,y:longint;
a:array[1..100009]of longint;
function dfs(l,r,d:longint):longint;
var i,min,x:longint;
begin
 if l>r then exit(0);
 if l=r then exit(a[l]-d);
 min:=maxlongint;
 for i:=l to r do
  if min>a[i] then
  begin
   min:=a[i];
   x:=i;
  end;
 exit(min-d+dfs(l,x-1,min)+dfs(x+1,r,min));
end;
begin
 assign(input,'road.in');
 reset(input);
 assign(output,'road.out');
 rewrite(output);
 readln(n);
 for i:=1 to n do read(a[i]);
 writeln(dfs(1,n,0));
 close(input);
 close(output);
end.
