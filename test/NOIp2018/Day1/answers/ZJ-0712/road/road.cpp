#include<stdio.h>
#include<math.h>
#include<string.h>
#include<time.h>
#include<ctype.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#include<set>
#include<string>
#include<stack>
#include<bitset>
#include<map>

using namespace std;

#define bug(x) cerr<<#x<<'='<<x<<' '
#define debug(x) cerr<<#x<<'='<<x<<'\n'
#define For(i,a,b) for(int i=a;i<=b;++i)
#define Ror(i,a,b) for(int i=b;a<=i;--i)

typedef long long ll;


template<class T>void rd(T&x){
	char c,f=1;x=0;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c-'0');
	while(c=getchar(),isdigit(c));
	x*=f;
}
template<class T>void pf(T x){
	static int top=0,stk[100];
	if(!x)putchar('0');
	if(x<0)putchar('-'),x=-x;
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]+'0');
}
template<class T>void pt(T x){pf(x);putchar(' ');}
template<class T>void ptn(T x){pf(x);putchar('\n');}
template<class T>void Max(T&x,T y){if(x<y)x=y;}
template<class T>void Min(T&x,T y){if(x>y)x=y;}

const int M=1e5+5;

int n,A[M],ans,res;

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	rd(n);
	For(i,1,n)rd(A[i]);
	For(i,1,n){
		if(A[i]>res)ans+=A[i]-res;
		res=A[i];
	}
	ptn(ans);
	return 0;
}


