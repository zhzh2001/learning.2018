#include<stdio.h>
#include<math.h>
#include<string.h>
#include<time.h>
#include<ctype.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#include<set>
#include<string>
#include<stack>
#include<bitset>
#include<map>

using namespace std;

#define bug(x) cerr<<#x<<'='<<x<<' '
#define debug(x) cerr<<#x<<'='<<x<<'\n'
#define For(i,a,b) for(int i=a;i<=b;++i)
#define Ror(i,a,b) for(int i=b;a<=i;--i)

typedef long long ll;


template<class T>void rd(T&x){
	char c,f=1;x=0;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c-'0');
	while(c=getchar(),isdigit(c));
	x*=f;
}
template<class T>void pf(T x){
	static int top=0,stk[100];
	if(!x)putchar('0');
	if(x<0)putchar('-'),x=-x;
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]+'0');
}
template<class T>void pt(T x){pf(x);putchar(' ');}
template<class T>void ptn(T x){pf(x);putchar('\n');}
template<class T>void Max(T&x,T y){if(x<y)x=y;}
template<class T>void Min(T&x,T y){if(x>y)x=y;}

const int M=50005;

int n,h[M],E_tot,m;

struct E{int t,c,nxt;}G[M<<1];

void Add(int x,int y,int c){
	G[++E_tot]=(E){y,c,h[x]};h[x]=E_tot;
	G[++E_tot]=(E){x,c,h[y]};h[y]=E_tot;
}

int mx,hd,Q[M],num,C[M];

bool vis[M];

void dfs(int x,int d,int f=0){
	if(vis[x])return;
	else vis[x]=1;
	if(f&&d)Q[++num]=d;
	else if(d>mx)mx=d,hd=x;
	for(int i=h[x];i;i=G[i].nxt){
		int y=G[i].t,c=G[i].c;
		dfs(y,d+c,f);
	}
}

bool check1(int st){
	int l=1,r=n,res=0;
	while(l<=r){
		if(C[r]>=st)r--,res++;
		else if(l==r)return 0;
		else {
			while(C[l]+C[r]<st&&l<r)l++;
			if(l<r)l++,r--,res++;
		}
		if(res==m)return 1;
	}return 0;
}

void solve1(){
	n=n-1;
	sort(C+1,C+1+n);
	int l=0,r=mx,res=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if(check1(mid))res=mid,l=mid+1;
		else r=mid-1;
	}
	ptn(res);
}

bool check2(int st){
	int pre=0,cnt=0;
	For(i,1,num)if(Q[i]-pre>=st)cnt++,pre=Q[i];
	return cnt>=m;
}

void solve2(){
	num=0;
	memset(vis,0,sizeof(vis));
	dfs(1,0,1);
	int l=0,r=mx,res=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if(check2(mid))res=mid,l=mid+1;
		else r=mid-1;
	}
	ptn(res);
}

int main(){
	
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	rd(n);rd(m);
	
	bool f=1;
	
	For(i,1,n-1){
		int a,b,c;
		rd(a);
		rd(b);
		rd(c);
		Add(a,b,c);
		C[i]=c;
		if(a!=1&&b!=1)f=0;
	}
	
	mx=0;hd=1;
	dfs(1,0);
	memset(vis,0,sizeof(vis));
	mx=0;
	dfs(hd,0);
	if(m==1)ptn(mx);
	else if(f)solve1();
	else solve2();
	return 0;
}


