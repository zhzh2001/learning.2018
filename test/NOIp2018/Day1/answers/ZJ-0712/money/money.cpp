#include<stdio.h>
#include<math.h>
#include<string.h>
#include<time.h>
#include<ctype.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#include<set>
#include<string>
#include<stack>
#include<bitset>
#include<map>

using namespace std;

#define bug(x) cerr<<#x<<'='<<x<<' '
#define debug(x) cerr<<#x<<'='<<x<<'\n'
#define For(i,a,b) for(int i=a;i<=b;++i)
#define Ror(i,a,b) for(int i=b;a<=i;--i)

typedef long long ll;


template<class T>void rd(T&x){
	char c,f=1;x=0;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c-'0');
	while(c=getchar(),isdigit(c));
	x*=f;
}
template<class T>void pf(T x){
	static int top=0,stk[100];
	if(!x)putchar('0');
	if(x<0)putchar('-'),x=-x;
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]+'0');
}
template<class T>void pt(T x){pf(x);putchar(' ');}
template<class T>void ptn(T x){pf(x);putchar('\n');}
template<class T>void Max(T&x,T y){if(x<y)x=y;}
template<class T>void Min(T&x,T y){if(x>y)x=y;}


const int M=25005;

int Cas,n,m,dp[M],A[M],ans;

int sum,mx,vis[M],mi;

bool use[M];

void init(){
	For(s,0,n-1)use[s]=1;
	For(s,1,n-1){
		if(!use[s])continue;
		For(j,1,mx)dp[j]=0;
		dp[0]=1;
		
		For(j,mi,mx)if(dp[j-mi])dp[j]=1;
		For(j,A[s],mx)if(dp[j-A[s]])dp[j]=1;
		
		For(i,s+1,n-1)if(dp[A[i]])use[i]=0;
	}
	int num=0;
	For(s,0,n-1)if(use[s])A[num++]=A[s];
	n=num;
	mx=A[n-1];
}

void solve(){
	int mask=(1<<n)-1;
	For(s,0,mask){
		m=0;
		For(i,0,n-1){
			if(s&(1<<i)){vis[i]=1;m++;}
			else vis[i]=0;
		}
		For(i,1,mx)dp[i]=0;
		dp[0]=1;
		For(i,0,n-1){
			if(!vis[i])continue;
			For(j,A[i],mx){
				if(dp[j-A[i]])dp[j]=1;
			}
		}
		bool f=1;
		For(i,0,n-1)if(!dp[A[i]])f=0;
		if(f)break;
	}
	ptn(m);
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	rd(Cas);
	while(Cas--){
		rd(n);
		For(i,0,n-1)rd(A[i]);
		sort(A,A+n);
		mx=A[n-1];
		mi=A[0];
		if(n>13)init();
		solve();
	}
	return 0;
}


