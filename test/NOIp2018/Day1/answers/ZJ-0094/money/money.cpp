#include <bits/stdc++.h>
using namespace std;
const int N=110;
const int P=25010;
int a[N],n,ans;
bool f[P];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);	
	int t;
	scanf("%d",&t);
	while (t--){
		scanf("%d",&n);
		for (int i=1; i<=n; ++i) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		memset(f,0,sizeof(f));
		f[0]=1;
		ans=0;
		for (int i=1; i<=n; ++i){
			if (f[a[i]]==1) continue;
			++ans;
			for (int j=a[i]; j<=25000; ++j)
				f[j]|=f[j-a[i]];
		}
		printf("%d\n",ans);
	}
}
