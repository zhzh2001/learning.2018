#include <bits/stdc++.h>
using namespace std;
const int N=50010;
int ne[N<<1],k,fi[N],b[N<<1],c[N<<1];
int num[N],f[N],tp,d[N],tot,z[N];
int lim,n,m;
void add(int x,int y,int z){
	ne[++k]=fi[x]; b[fi[x]=k]=y; c[k]=z;
}
int canbe(int x){
	tot=0;
	for (int i=1; i<=tp; ++i) if (i!=x) z[++tot]=d[i];
	int ret=0,j=1;
	for (int i=tot; i>=1;){
		while (j<i&&z[j]+z[i]<lim) ++j;
		if (j>=i) break;
		else{
			++ret;
			--i;
			++j;
		}
	}
	return ret;
}
void dfs(int x,int fa){
	num[x]=0; f[x]=0;
	for (int j=fi[x]; j; j=ne[j])
		if (b[j]!=fa){
			dfs(b[j],x);
			num[x]+=num[b[j]];
		}
	tp=0;
	for (int j=fi[x]; j; j=ne[j])
		if (b[j]!=fa){
			d[++tp]=(f[b[j]]+c[j]);
		}
	sort(d+1,d+tp+1);
	while (tp&&d[tp]>=lim){
		++num[x];
		--tp;
	}
	int ooo=canbe(0);
	num[x]+=ooo;
	int ret=0;
	for (int l=1,r=tp,mid=(l+r)>>1; l<=r; mid=(l+r)>>1){
		//cerr<<l<<" "<<r<<" "<<mid<<endl;
		if (canbe(mid)==ooo) l=mid+1,ret=mid; else r=mid-1;
	}
	f[x]=d[ret];
	//if (f[x]>=lim) ++num[x],f[x]=0;
	//cerr<<x<<" "<<fa<<" "<<num[x]<<" "<<f[x]<<endl;
}
bool check(int v){
	lim=v;
	dfs(1,0);
	//if (f[1]>=lim) ++num[1];
	//cerr<<num[1]<<endl;
	return num[1]>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);	
	scanf("%d%d",&n,&m);
	int sum=0;
	for (int i=1,x,y,z; i<n; ++i){
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z);
		add(y,x,z);
		sum+=z;
	}
	//cerr<<check(15)<<endl;
	//return 0;
	int ans=0;
	for (int l=0,r=sum/m,mid=(l+r)>>1; l<=r; mid=(l+r)>>1)
		if (check(mid)) ans=mid,l=mid+1; else r=mid-1;
	cout<<ans;
}
