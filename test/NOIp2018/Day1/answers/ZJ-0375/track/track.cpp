#include<cstdio>
#include<algorithm>
using namespace std;
const int oo=1000000012;
int n,m,sum[50012],mn;
struct node{
	int x,y,p;
}s[50012];
inline int read()
{
	int x=0,f=1;char c=getchar();
	while (c<'0'||c>'9') {if (c=='-') f=-1;c=getchar();}
	while (c>='0'&&c<='9') x=x*10+c-48,c=getchar();
	return x*f;
}
inline bool cmp1(node l,node r)
{
	return l.p<r.p;
}
inline bool cmp2(node l,node r)
{
	return l.y<r.y;
}
inline void dfs(int now,int t,int s,int pre)
{
	if (t>m) {mn=max(mn,(s==1000000012)?0:s);return;}
	if (now>n) return;
	dfs(now+1,t+1,min(s,sum[now]-sum[pre]),now);
	dfs(now+1,t,s,pre);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n<=100&&m==1)
	{
		int x,y,z;
		int f[112][112],ans=0;
		for (int i=1; i<=n; i++)
			for (int j=1; j<=n; j++) f[i][j]=oo;
		for (int i=1; i<n; i++)
		{
			x=read(),y=read(),z=read();
			f[x][y]=f[y][x]=z;
		}
		for (int k=1; k<=n; k++)
			for (int i=1; i<=n; i++)
				for (int j=1; j<=n; j++)
				if (f[i][j]>f[i][k]+f[k][j]) f[i][j]=f[i][k]+f[k][j];
		for (int i=1; i<=n; i++)
			for (int j=1; j<=n; j++) ans=max(ans,f[i][j]);
		printf("%d\n",ans);
	}
	else
	{
		bool flag1=1,flag2=1;
		for (int i=1; i<n; i++)
		{
			s[i].x=read(),s[i].y=read();s[i].p=read();
			if (s[i].x!=1) flag1=0;
			if (s[i].y!=s[i].x+1) flag2=0;
		}
		if (flag1)
		{
			sort(s+1,s+n,cmp1);
			int lack=m*2-(n-1);
			mn=s[n-lack].p;
			int l=1,r=n-lack-1;
			while (l<r)
			{
				mn=min(mn,s[l].p+s[r].p);
				l++;r--;
			}
			printf("%d\n",mn);
		}
		else
		if (flag2)
		{
			sort(s+1,s+n,cmp2);
			for (int i=1; i<n; i++) sum[i]=sum[i-1]+s[i].p;
			dfs(1,0,1000000012,0);
			printf("%d\n",mn);
		}
	}
	return 0;
}
