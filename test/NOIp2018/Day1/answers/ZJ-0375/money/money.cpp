#include<cstdio>
#include<algorithm>
using namespace std;
int n,q,a[112],ans,tong[1012],p[112],mx;
bool f[112];
inline bool cmp(int x,int y)
{
	return a[x]>a[y];
}
inline int read()
{
	int x=0,f=1;char c=getchar();
	while (c<'0'||c>'9') {if (c=='-') f=-1;c=getchar();}
	while (c>='0'&&c<='9') x=x*10+c-48,c=getchar();
	return x*f;
}
inline void dfs2(int d,int s,int num)
{
	if (d>num)
	{
		if (tong[s]) tong[s]=0;
		return;
	}
	for (int i=p[d]; i<=mx-s; i+=p[d])
		dfs2(d+1,s+i,num);
}
inline void dfs(int t,int s,int sum)
{
	if (t>n)
	{
		if (s>1)
		dfs2(1,0,s);
		return;
	}
	if (f[t]) dfs(t+1,s,sum);
	else
	{
		dfs(t+1,s,sum);
		if (sum-a[t]<=mx)
		{
		p[s+1]=-a[t];
		dfs(t+1,s+1,sum-a[t]);
		}
	}
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	q=read();
	while (q--)
	{
		n=read();
		mx=0;
		for (int i=1; i<=n; i++) a[i]=-read(),mx=max(mx,-a[i]);
		if (n==2)
		{
			if ((-a[1])%(-a[2])==0||(-a[2])%(-a[1])==0)puts("1");
			else puts("2");
		}
		else
		{
			for (int i=1; i<=mx; i++) tong[i]=0;
			for (int i=1; i<=n; i++) tong[-a[i]]=1;
			ans=0;
			sort(a+1,a+1+n);
			for (int i=1; i<=n; i++) f[i]=0;
			for (int i=1; i<=n; i++)
				for (int j=i+1; j<=n; j++)
				if ((-a[i])%(-a[j])==0) tong[-a[i]]=0,f[i]=1;
			dfs(1,0,0);
			for (int i=1; i<=mx; i++) ans+=tong[i];
			printf("%d\n",ans);
		}
	}
	return 0;
}
