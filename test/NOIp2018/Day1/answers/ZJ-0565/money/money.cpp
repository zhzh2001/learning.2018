#include<bits/stdc++.h>
#define ll long long
#define N 110
#define INF 99999999
#define M 500
#define mem(a,b) memset(a,b,sizeof(a))
#define fsb(a,b,c) for(int a=b;a<=(c);a++)
#define fbs(a,b,c) for(int a=b;a>=(c);a--)
using namespace std;
int T,a[N],n,ans,g[510],f[510],ud[N];
template<typename qw>inline void rll(qw &x){
	x=0;qw f=1;char c=getchar();
	while(!isdigit(c))f=c=='-'?-1:f,c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	x*=f;
}
inline void dfs(int u,int cnt){
	if(cnt>=ans)return;
	if(u==n+1){
		mem(f,0);f[0]=1;
		fsb(j,1,n)
			if(ud[j])
				fsb(i,0,M-a[j])
					if(f[i])f[i+a[j]]=1;
		fsb(i,0,M)if(f[i]!=g[i])return;
		ans=cnt;
		return;
	}
	ud[u]=1;dfs(u+1,cnt+1);ud[u]=0;dfs(u+1,cnt);
}
int main(){
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	rll(T);
	while(T--){
		rll(n);ans=n;
		fsb(i,1,n)rll(a[i]);
		mem(g,0);g[0]=1;
		fsb(j,1,n)
			fsb(i,0,M-a[j])if(g[i])g[i+a[j]]=1;
//		fsb(i,1,M)if(!g[i])printf("%10d\n",i);
		dfs(1,0);
		printf("%d\n",ans);
	}
	return 0;
}
