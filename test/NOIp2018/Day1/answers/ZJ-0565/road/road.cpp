#include<bits/stdc++.h>
#define ll long long
#define N 100010
#define max(a,b) ((a)>(b)?(a):(b))
#define mem(a,b) memset(a,b,sizeof(a))
#define fsb(a,b,c) for(int a=b;a<=(c);a++)
#define fbs(a,b,c) for(int a=b;a>=(c);a--)
using namespace std;
int a[N],n,ans=0,fa[N],po[N],vis[N];
int sz=0;
struct node{
	int l,r;
}s[N];
template<typename qw>inline void rll(qw &x){
	x=0;qw f=1;char c=getchar();
	while(!isdigit(c))f=c=='-'?-1:f,c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	x*=f;
}
inline int getf(int x){
	return fa[x]==x?x:fa[x]=getf(fa[x]);
}
inline void sup(int u){
	int son=u;
	while(son>1&&a[s[son].l]>a[s[son>>1].l]){
		swap(po[s[son].l],po[s[son>>1].l]);
		swap(s[son],s[son>>1]);
		son>>=1;
	}
}
inline void sdown(int u){
	int f=u,son;
	while(f*2<=sz){
		son=f*2;son+=(son+1<=sz&&a[s[son+1].l]>a[s[son].l])?1:0;
		if(a[s[son].l]>a[s[f].l]){
			swap(po[s[son].l],po[s[f].l]);
			swap(s[son],s[f]);f=son;
		}else break;
	}
}
inline void spush(int l,int r){
	s[++sz]=(node){l,r};po[l]=sz;
	sup(sz);
}
inline void spop(int u){
	po[s[u].l]=0;
	if(sz==1){
		sz--;return;
	}
	s[u]=s[sz--];po[s[u].l]=u;
	int l=s[u].l;
	sup(po[l]);sdown(po[l]);
}
int main(){
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	rll(n);
	fsb(i,1,n){
		rll(a[i]);fa[i]=i;
	}
	mem(vis,0);
	fsb(i,1,n){
		if(vis[i])continue;
		int l=i,r=i;
		while(r<=n&&a[r]==a[l]){
			vis[r]=1;
			fa[r]=l;
			r++;
		}
		r--;
		spush(l,r);
	}
//	while(sz>0){
//		printf("%10d %d\n",s[1].l,s[1].r);spop(1);
//		if(sz==1)system("pause");
//	}
	while(sz>1){
		int l=s[1].l,r=s[1].r,L=l-1,R=r+1,fL,fR,mx=0;spop(1);
//		printf("%10d %d %d %d\n",l,r,a[l],getf(r));
//		if(sz==0)system("pause");
//		printf("%d\n",a[s[1].l]);
		if(L>=1){
			fL=getf(L);mx=max(mx,a[fL]);
		}
		if(R<=n){
			fR=getf(R);mx=max(mx,a[fR]);
		}
		ans+=a[l]-mx;a[l]=mx;
		int newl=l,newr=r;
		if(L>=1&&a[l]==a[fL]){
			newl=fL;
			fa[l]=fL;
			spop(po[fL]);
		}
		if(R<=n&&a[l]==a[fR]){
			newr=s[po[fR]].r;
			fa[fR]=newl;
			spop(po[fR]);
		}
		spush(newl,newr);
	}
	printf("%d\n",ans+a[s[1].l]);
	return 0;
}
