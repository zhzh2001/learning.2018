#include<bits/stdc++.h>
#define ll long long
#define N 50010
#define mem(a,b) memset(a,b,sizeof(a))
#define fsb(a,b,c) for(int a=b;a<=(c);a++)
#define fbs(a,b,c) for(int a=b;a>=(c);a--)
using namespace std;
int n,cnt=0,m,x,y,z,head[N],ave=0,l,r,mid,mx[N],mxx[N],ans,flagai=1;
int b[N],c[N];
struct edge{
	int p,nt,l;
}a[N*2];
template<typename qw>inline void rll(qw &x){
	x=0;qw f=1;char c=getchar();
	while(!isdigit(c))f=c=='-'?-1:f,c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	x*=f;
}
inline void add(int x,int y,int z){
	a[++cnt]=(edge){y,head[x],z};head[x]=cnt;
}
inline int check(int x){
	int cnt=0,last=0;
	fsb(i,1,n){
		if(last+c[i]>=x){
			cnt++;last=0;
		}else last+=c[i];
	}
	return cnt>=m;
}
inline void dfs(int u,int fa){
	for(int t=head[u];t!=-1;t=a[t].nt){
		int v=a[t].p;if(v==fa)continue;
		dfs(v,u);
		int l=mx[v]+a[t].l;
		if(l>mx[u]){
			mxx[u]=mx[u];mx[u]=l;
		}else{
			if(l>mxx[u]){
				mxx[u]=l;
			}
		}
	}
	ans=max(ans,mx[u]+mxx[u]);
}
inline int cmp(int a,int b){
	return a>b;
}
inline int check2(int x){
	int cnt=0,i=1,j=n;
	for(;i<=n;i++){
		if(b[i]>=x)cnt++;else break;
	}
	while(i<j){
		while(i<j){
			if(b[i]+b[j]>=x)break;
			j--;
		}
		if(i>=j)return cnt>=m;
		i++;j--;cnt++;
	}
	return cnt>=m;
}
int main(){
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	rll(n);rll(m);mem(head,255);
	if(m>n-1){
		puts("0");return 0;
	}
	fsb(i,2,n){
		rll(x);rll(y);rll(z);
		if(x!=1&&y!=1)flagai=0;b[i-1]=z;
		add(x,y,z);add(y,x,z);ave+=z;
		c[x]=z;
	}
	if(m==1){
		ans=0;
		mem(mx,0);mem(mxx,0);
		dfs(1,0);
		printf("%d\n",ans);
	}else{
		if(flagai){
			n--;
			sort(b+1,b+1+n,cmp);
			l=0;r=ave;
			while(l+5<=r){
				mid=(l+r)>>1;
//				printf("%10d %d\n",mid,check2(mid));
				if(check2(mid))l=mid;else r=mid;
			}
			fbs(i,r,l)
				if(check2(i)){
					printf("%d\n",i);return 0;
				}
		}else{
			n--;l=0;r=ave;
			while(l+5<=r){
				mid=(l+r)>>1;
				if(check(mid))l=mid;else r=mid;
			}
			fbs(i,r,l)
				if(check(i)){
					printf("%d\n",i);return 0;
				}
		}
	}
	return 0;
}
