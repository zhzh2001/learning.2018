#include<bits/stdc++.h>
#define LL long long
inline LL read(){
	char c=getchar();while (c!='-'&&(c>'9'||c<'0'))c=getchar();
	LL k=0,kk=1;if (c=='-')c=getchar(),kk=-1;
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return k*kk;
}void write(LL x){if (x<0)putchar('-'),x=-x;if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(LL x){write(x);puts("");}
using namespace std;
int t,n,a[1010],ans,f[100010],m;
int main(){
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	t=read();f[0]=1;
	while (t--){
		n=read();for (int i=1;i<=n;i++)a[i]=read();
		sort(a+1,a+1+n);ans=0;m=a[n];
		for (int i=1;i<=m;i++)f[i]=0;
		for (int i=1;i<=n;i++)
			if (!f[a[i]]){
				ans++;
				for (int j=a[i];j<=m;j++)f[j]|=f[j-a[i]];
			}
		writeln(ans);
	}
	return 0;
}
