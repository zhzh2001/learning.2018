#include<stdio.h>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<set>
#define ll long long
#define N 100006
using namespace std;
struct seg{
	int x,id;
	seg(int a,int b):x(a),id(b){}
	bool operator <(const seg &rhs)const{
		return id<rhs.id;
	}
};
multiset<seg>s;
multiset<seg>::iterator it;
int n,a[N],r[N];ll ans;
bool cmp(int i,int j){return a[i]<a[j];}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%d",&a[i]);
		r[i]=i;
	}
	sort(r+1,r+n+1,cmp);
	for(int o=1;o<=n;o++){
		int i=r[o];
		if(s.empty()){
			s.insert(seg(a[i],i));
			ans+=a[i];
			continue;
		}
		int mx=-1e9;
		s.insert(seg(a[i],i));
		it=s.lower_bound(seg(a[i],i));
		if(it!=s.begin()){
			it--;
			mx=max(mx,it->x);
			it++;
		}
		it++;
		if(it!=s.end()){
			mx=max(mx,it->x);
			it--;
		}
		ans+=a[i]-mx;
	}
	printf("%lld",ans);
	return 0;
}
