#include<stdio.h>
#include<algorithm>
#include<cstdlib>
#include<cstring>
using namespace std;
int n,ans,m,mx,a[105],cur[25005];
int gcd(int a,int b){return b ? gcd(b,a%b):a;}
bool check(int x){
	for(int i=2;i*i<=x;i++)
		if(x%i==0&&(cur[i]||cur[x/i]))return true;
	if(x!=1&&cur[x])return true;
	return false;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--){
		memset(cur,0,sizeof(cur));
		m=0;
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		if(a[1]==1){puts("1");continue;}
		for(int i=1;i<=n;i++)
			if(a[i]!=a[i-1])a[++m]=a[i];
		if(a[1]==2&&a[2]==3){puts("2");continue;}
		n=m;mx=a[n];
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++){
				if(gcd(a[i],a[j])==1)mx=min(mx,a[i]*a[j]-a[i]-a[j]+1);
				if(a[j]%a[i]==0)continue;
				for(int k1=0;k1*a[i]<mx;k1++)
					for(int k2=0;k2*a[j]+k1*a[i]<=mx;k2++)
						cur[k1*a[i]+k2*a[j]]=1;
			}
		for(int i=mx;i<=a[n];i++)cur[i]=1;
		ans=n;
		for(int i=n;i>1;i--)
			for(int j=1;j<i;j++)
				if(check(a[i]-a[j])){
					ans--;
					break;
				}
		printf("%d\n",ans);
	}
	return 0;
}
