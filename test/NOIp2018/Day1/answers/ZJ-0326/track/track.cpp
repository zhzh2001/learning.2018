#include<stdio.h>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#define ll long long
#define N 50005
using namespace std;
int tot,head[N],vet[N<<1],val[N<<1],Next[N<<1];
void add(int x,int y,int z){
	tot++;
	vet[tot]=y;
	val[tot]=z;
	Next[tot]=head[x];
	head[x]=tot;
}
int n,m,x[N],y[N],z[N],r[N];ll d1[N],d2[N];
void dfs(int u,int fa){
	d1[u]=0;d2[u]=-1e9;
	for(int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if(v==fa)continue;
		dfs(v,u);
		if(d1[u]<d1[v]+val[i]){
			d2[u]=d1[u];
			d1[u]=d1[v]+val[i];
		}else d2[u]=max(d2[u],d1[v]+val[i]);
	}
}
bool cp(int a,int b){return a>b;}
bool cmp(int i,int j){return x[i]==x[j] ? y[i]<y[j]:x[i]<x[j];}
bool check(ll x){
	ll res=0;int cnt=0;
	for(int o=1;o<n;o++){
		int i=r[o];
		res+=z[i];
		if(res>=x){
			res=0;
			cnt++;
		}
	}
	if(cnt>=m)return true;
	else return false;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool flag=true;
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&x[i],&y[i],&z[i]);
		if(x[i]!=1)flag=false;r[i]=i;
	}
	if(flag){
		sort(z+1,z+n,cp);
		int ans=1e9;
		for(int i=1;i<=m;i++)
			ans=min(ans,z[i]+z[m+m-i+1]);
		printf("%d",ans);
		return 0;
	}
	if(m==1){
		for(int i=1;i<n;i++){
			add(x[i],y[i],z[i]);
			add(y[i],x[i],z[i]);
		}
		dfs(1,0);
		ll ans=0;
		for(int i=1;i<=n;i++)
			ans=max(ans,d1[i]+d2[i]);
		printf("%lld",ans);
	}	
	sort(r+1,r+n,cmp);
	ll l=1,r=1e18,ans;
	while(l<=r){
		ll mid=l+r>>1;
		if(check(mid)){
			ans=mid;
			l=mid+1;
		}else r=mid-1;
	}
	printf("%lld",ans);
	return 0;
}
