#include<cstdio>
#include<cstring>
#include<vector>
#include<iostream>
#include<algorithm>

static const int M = 50005;
struct node{
	int to,len;
};
std::vector<node>es[M];
int fpg[M];
//namespace Water{//n<=1000
//	static const int MM  = 1005;
//	int dfn[M],ed[M];
//	int f[MM][MM];
//	int id_tot;
//	int mx[M],mi[M];
//	void rec(int u,int fa){
//		for(int i=x;
//	}
//	void dfs(int u,int fa){
//		dfn[u]=++id_tot;
//		for(int i=0,v;i<es[u].size();++i){
//			v=es[u][i].to;
//			if(v==fa)continue;
//			dfs(v,u);
//		}
//		ed[u]=id_tot;
//		rec(u,fa);
//	}
//	void solve(int n,int m){
//		int rt=1;
//		for(int i=1;i<=n;++i)if(es[i].size()<3)rt=i;
//		dfs(rt,0);
//		printf("%d\n",f[rt][m]);
//	}
//}
namespace Pg{
	int check(int n,int mi){
		int cnt=0,las=0;
		for(int i=1;i<n;++i){
			if(fpg[i]<mi)las=i;
			else ++cnt;//fpg[i]>=mi
		}
		for(int i=1;i<las;){
			if(fpg[i]+fpg[las]>=mi)++cnt,++i,--las;
			else ++i;
		}
		return cnt;
	}
	void solve(int n,int m){
		int L=0,R=0,ans=0;//n-1 edges
		std::sort(fpg+1,fpg+n);
		for(int i=1;i<n;++i)R+=fpg[i];
		while(L<=R){
			int mid=(L+R)>>1;
			if(check(n,mid)>=m)ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}
namespace Dia{
	int fir[M],sec[M],sum[M];
	void dfs(int u,int fa){
		for(int i=0,v,tmp;i<es[u].size();++i){
			v=es[u][i].to;
			if(v==fa)continue;
			dfs(v,u);
			tmp=fir[v]+es[u][i].len;
			if(sum[u]<sum[v])sum[u]=sum[v];
			if(sum[u]<fir[u]+tmp)sum[u]=fir[u]+tmp;
			if(fir[u]<tmp)sec[u]=fir[u],fir[u]=tmp;
			else if(sec[u]<tmp)sec[u]=tmp;
		}
		if(sum[u]<fir[u])sum[u]=fir[u];
	}
	void solve(int n,int m){
		dfs(1,0);
		printf("%d\n",sum[1]);
	}
}
//1s 512M
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	bool AllOne=true;
	for(int i=1,u,v,w;i<n;++i){
		scanf("%d%d%d",&u,&v,&w);
		AllOne&=(u==1||v==1);
		fpg[i]=w;
		es[u].push_back((node){v,w});
		es[v].push_back((node){u,w});
	}
	if(AllOne)Pg::solve(n,m);
	else if(m==1)Dia::solve(n,m);
	//else if(n<=1000)Water::solve(n,m);
	return 0;
}
