#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
static const int M = (int)1e5+5;

int a[M];

namespace Water{
	void solve(int n){
		int ans=0;
		bool flag=false;
		while(!flag){
			flag=true;
			int mi=M,pos=-1;
			for(int i=1;i<=n;++i){
				if(a[i]){
					flag=false;//didn't end
					if(a[i]<mi)mi=a[i],pos=i;
				}
			}
			if(~pos){
				int L=pos,R=pos;
				for(;L>=1&&a[L]>=mi;--L);//L=0||a[L]<mi => a[L]=0
				for(;R<=n&&a[R]>=mi;++R);//a[R]=0
				//(L,R)
				for(int i=L+1;i<R;++i)a[i]-=mi;
				ans+=mi;
			}//else pos=-1 => flag=true
		}
		printf("%d\n",ans);
	}
}
namespace Lar{
	int L[M],R[M];
	int fa[M];
	struct node{
		int h,pos;
		bool operator <(const node &tmp)const{
			if(h!=tmp.h)return h>tmp.h;
			return pos<tmp.pos;
		}
	}Q[M];
	int getfa(int v){
		if(fa[v]==v)return v;
		return fa[v]=getfa(fa[v]);
	}
	void solve(int n){
		int ans=0;
		memset(fa,-1,sizeof(fa));
		for(int i=0;i<=n+1;++i)fa[i]=L[i]=R[i]=i;
		for(int i=1;i<=n;++i)Q[i]=(node){a[i],i};
		std::sort(Q+1,Q+n+1);
		for(int i=1,now,l,r;i<=n;++i)if(Q[i].h){
			now=getfa(Q[i].pos);
			l=getfa(L[now]-1);
			r=getfa(R[now]+1);
			if(a[l]<a[r]){
				ans+=a[now]-a[r];
				fa[now]=r;
				L[r]=L[now];
			}else{//a[l]>=a[r]
				ans+=a[now]-a[l];
				fa[now]=l;
				R[l]=R[now];
			}
		}
		printf("%d\n",ans);
	}
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n;
	scanf("%d",&n);
	for(int i=1;i<=n;++i)scanf("%d",a+i);
	if(n<=10)Water::solve(n);
	else Lar::solve(n);
	return 0;
}
