#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<cmath>
#include<iostream>
#include<algorithm>
static const int N = 105;//the size
static const int M = 25005;//the range of a[i]

int a[M];
bool mark[M];

void solve(int n){
	std::sort(a+1,a+n+1);
	memset(mark,0,sizeof(mark));
	int ans=0;
	mark[0]=true;
	for(int i=1,j,k;i<=n;++i){
		if(mark[a[i]])continue;
		++ans;
		for(j=0,k=a[i];k<M;++j,++k)if(mark[j])mark[k]=true;
	}
	printf("%d\n",ans);
}
//long long mod
//1s 512M
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	for(int cas=1;cas<=T;++cas){
		int n;
		scanf("%d",&n);
		for(int i=1;i<=n;++i)scanf("%d",a+i);
		solve(n);
	}
	return 0;
}
