#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
int n,m,a[50010][510],dis[50010][510],b[50010],minn,f[50010];
inline void read(int &x)
{
	char ch; x=0;
	ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
}
int check(int j)
{
	if (j==1||j==n-1) return dis[j][1]; else return dis[j][2];
}
void dfs(int l,int r)
{
	int mid=(l+r)/2;
	if (l>=r) return;
	int j=1,flag=1;
	for (int i=1;i<=m;i++)
	{
		int s=0;
		while (s<mid&&j<n) s+=check(j),j++;
		if (s<mid)
		{
			flag=0; break;
		}
	}
	if (flag)
	{
		minn=max(minn,mid); dfs(mid+1,r);
	}
		else dfs(l,mid-1);
}
void aria(int x,int y)
{
	minn=max(minn,y);
	for (int j=1;j<=a[x][0];j++)
		if (f[a[x][j]])
		{
			f[a[x][j]]=0; aria(a[x][j],y+dis[x][j]); f[a[x][j]]=1;
		}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n); read(m);
	int flag1=1,flag2=1,total=0;
	for (int i=1;i<n;i++)
	{
		int x,y,z;
		read(x); read(y); read(z);
		if (x!=1) flag1=0;
		if (y!=x+1) flag2=0;
		a[x][0]++; a[x][a[x][0]]=y; dis[x][a[x][0]]=z;
		a[y][0]++; a[y][a[y][0]]=x; dis[y][a[y][0]]=z;
		total+=z;
	}
	if (flag1)
	{
		for (int i=1;i<=a[1][0];i++) b[i]=dis[1][i];
		sort(b+1,b+n);
		if (m==1) printf("%d\n",b[n-2]+b[n-1]);
		else
		{
			minn=20010;
			if (m<=(n-1)/2)
			{
				for (int i=1;i<=m;i++) minn=min(minn,b[n-i]+b[n+i-2*m-1]);
			}
			else
			{
				for (int i=1;i<=n-1-m;i++) minn=min(minn,b[i]+b[2*n-2*m-i-1]);
				minn=min(minn,b[2*n-2*m-1]);
			}
			printf("%d\n",minn);
		}
		return 0;
	}
	if (flag2)
	{
		minn=0;
		dfs(1,(total/m)*2);
		printf("%d\n",minn);
		return 0;
	}
	if (m==1)
	{
		for (int i=1;i<=n;i++) f[i]=1;
		minn=0;
		for (int i=1;i<=n;i++)
		{
			f[i]=0; aria(i,0); f[i]=1;
		}
		printf("%d\n",minn);
	}
	return 0;
}
