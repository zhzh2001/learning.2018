#include<cstdio>
#include<iostream>
#define ll long long
using namespace std;
int n;
ll a[100010];
inline void read(int &x)
{
	char ch; x=0;
	ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
}
inline void read(ll &x)
{
	char ch; x=0;
	ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for (int i=1;i<=n;i++) read(a[i]);
	ll ans=0;
	while (true)
	{
		int l=1; ll minn=100010;
		while (a[l]==0&&l<=n) l++;
		if (l>n) break;
		int i=l;
		while (i<=n)
		{
			while (a[i]!=0&&i<=n)
			{
				if (a[i]<minn) minn=a[i];
				i++;
			}
			for (int j=l;j<i;j++) a[j]-=minn;
			ans+=minn;
			while (a[i]==0&&i<=n) i++;
			l=i; minn=100010;
		}
	}
	printf("%lld\n",ans);
	return 0;
}
