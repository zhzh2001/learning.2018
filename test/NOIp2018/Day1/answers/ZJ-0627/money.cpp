#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
using namespace std;
int n,a[110],f[25010];
inline void read(int &x)
{
	char ch; x=0;
	ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T; read(T);
	f[0]=0;
	while (T--)
	{
		read(n); int maxx=0;
		for (int i=1;i<=n;i++)
		{
			read(a[i]); maxx=max(a[i],maxx);
		}
		for (int i=1;i<=maxx;i++) f[i]=1;
		sort(a+1,a+n+1);
		int s=0;
		for (int i=1;i<=n;i++)
			if (f[a[i]])
			{
				s++;
				for (int j=0;a[i]+j<=maxx;j++)
					if (!f[j]) f[a[i]+j]=0;
			}
		printf("%d\n",s);
	}
	return 0;
}
