#include<bits/stdc++.h>
using namespace std;
int T,n,a[101];
bool dfs(int x,int e,int v,int ev)
{
	if(x==e)
	{
		if(v==ev)return 1;
		else return 0;
	}
	while(v<=ev)
	{
		if(dfs(x+1,e,v,ev))return 1;
		v+=a[x];
	}
	return 0;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("bl.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);
		for(register int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		int ans=0;
		for(register int i=1;i<=n;i++)
			ans+=1-dfs(1,i,0,a[i]);
		printf("%d\n",ans);
	}
}
