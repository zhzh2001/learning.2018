#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
template<typename T>void read(T &a)
{
	char ch=getchar();
	T x=0,t=1;
	while(ch<'0'||ch>'9')
	{
		if(ch=='-')t=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')x*=10,x+=ch-'0',ch=getchar();
	a=x*t;
}
template<typename T>void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);

}
template<typename T>void printfn(T a){print(a);putchar('\n');}
int T;
int n,num[101];
int f[25010];
int mx=0,ans;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	while(T--)
	{
		mx=0;
		read(n);
		ans=n;
		for(register int i=1;i<=n;i++)
			read(num[i]),mx=max(mx,num[i]);
		memset(f,0,sizeof(f));
		sort(num+1,num+1+n);
		f[0]=1;
		for(register int i=1;i<=n;i++)
		{
			if(f[num[i]])
			{
				ans--;
				continue;
			}
			for(register int j=mx;j>=0;j--)
			{
				if(f[j])
				{
					int tmp=num[i];
					while(j+tmp<=mx)
					{
						f[j+tmp]=1;
						tmp+=num[i];
					}
				}
			}
		}
		printfn(ans);
	}
	return 0;
}
