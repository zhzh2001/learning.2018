#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
template<typename T>void read(T &a)
{
	char ch=getchar();
	T x=0,t=1;
	while(ch<'0'||ch>'9')
	{
		if(ch=='-')t=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')x*=10,x+=ch-'0',ch=getchar();
	a=x*t;
}
template<typename T>void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);

}
template<typename T>void printfn(T a){print(a);putchar('\n');}
int l[500001],r[500001];
int mn[500001];
int n,num[100010];
long long ans=0;
void Build(int x,int L,int R)
{
	l[x]=L,r[x]=R;
	if(L==R)
	{
		mn[x]=L;
		return;
	}
	int mid=(L+R)>>1;
	Build(x<<1,L,mid);
	Build((x<<1)+1,mid+1,R);
	mn[x]=num[mn[x<<1]]>num[mn[(x<<1)+1]]?mn[(x<<1)+1]:mn[x<<1];
}
int searchmin(int x,int L,int R)
{
	if(l[x]==L&&r[x]==R)return mn[x];
	int mid=(l[x]+r[x])>>1;
	if(L>mid)return searchmin((x<<1)+1,L,R);
	else if(R<=mid)return searchmin(x<<1,L,R);
	else
	{
		int m1=searchmin(x<<1,L,mid),m2=searchmin((x<<1)+1,mid+1,R);
		return num[m1]<num[m2]?m1:m2;
	}
}
void work(int L,int R,int v)
{
	if(L==R)
	{
		ans+=num[L]-v;
		return;
	}
	if(L>R)return;
	int k=searchmin(1,L,R);
	ans+=num[k]-v;
	work(L,k-1,num[k]),work(k+1,R,num[k]);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for(register int i=1;i<=n;i++)
		read(num[i]);
	Build(1,1,n);
	work(1,n,0);
	printfn(ans);
	return 0;
}
