#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<vector>
using namespace std;
template<typename T>void read(T &a)
{
	char ch=getchar();
	T x=0,t=1;
	while(ch<'0'||ch>'9')
	{
		if(ch=='-')t=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')x*=10,x+=ch-'0',ch=getchar();
	a=x*t;
}
template<typename T>void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);

}
template<typename T>void printfn(T a){print(a);putchar('\n');}
int flag1=1,flag2=1,flag3=0;
int n,m;
vector<int>p[50001],v[50001];
long long dl[50001];
long long siz[50001];
long long ans=0,sum=0;
void dfs(int x)
{
	siz[x]=0;
	long long mx1=0,mx2=0;
	for(register int i=0;i<p[x].size();i++)
	{
		if(siz[p[x][i]]==-1)
		{
			dfs(p[x][i]);
			if(siz[p[x][i]]+v[x][i]>mx1)mx2=mx1,mx1=siz[p[x][i]]+v[x][i];
			else if(siz[p[x][i]]+v[x][i]>mx2)mx2=siz[p[x][i]]+v[x][i];
		}
	}
	ans=max(mx1+mx2,ans);
	siz[x]=mx1;
}
bool check(int x)
{
	long long cnt=0,res=0;
	for(register int i=1;i<=n-1;i++)
	{
		res+=dl[i];
		if(res>=x)cnt++,res=0;
	}
	return cnt>=m;
}
int Cnt=0;
void check2(int x,int V)
{
	long long v1=-1,v2=-1,v3=-1;
	siz[x]=0;
	for(register int i=0;i<p[x].size();i++)
	{
		if(siz[p[x][i]]==-1)
		{
			check2(p[x][i],V);
			if(v1==-1)v1=siz[p[x][i]]+v[x][i];
			else if(v2==-1)v2=siz[p[x][i]]+v[x][i];
			else if(v3==-1)v3=siz[p[x][i]]+v[x][i];
		}
	}
	while(1)
	{
		if(v1<v3)swap(v1,v3);
		if(v1<v2)swap(v1,v2);
		if(v2<v3)swap(v2,v3);
		if(v1==-1)return;
		else if(v2==-1)
		{
			if(v1>=V)
			{
				Cnt++;
				return;
			}
			else siz[x]=V;
			return;
		}
		else if(v3==-1)
		{
			if(v1>=V)Cnt++,v1=-1;
			if(v2>=V)Cnt++,v2=-1;
			if(v1+v2>=V)
			{
				Cnt++;
				return;
			}
			else 
			{
				siz[x]=max((long long)0,max(v1,v2));
				return;
			}
		}
		else
		{
			if(v1>=V)Cnt++,v1=-1;
			if(v2>=V)Cnt++,v2=-1;
			if(v3>=V)Cnt++,v3=-1;
			if(v3==-1)return;
			if(v2==-1)
			{
				siz[x]=v3;
				return;
			}
			if(v1!=-1)
			{
				if(v2+v3>=V)
				{
					siz[x]=v1;
					Cnt++;
					return;
				}
				if(v1+v3>=V)
				{
					siz[x]=v2;
					Cnt++;
					return;
				}
				if(v1+v2>=V)
				{
					siz[x]=v3;
					Cnt++;
					return;
				}
				siz[x]=max(max(v1,v3),v2);
				return;
			}
		}
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n),read(m);
	for(register int i=1;i<n;i++)
	{
		int a,b,c;
		read(a),read(b),read(c);
		sum+=c;
		p[a].push_back(b),p[b].push_back(a);
		v[a].push_back(c),v[b].push_back(c);
		if(a!=1)flag1=0;
		if(b!=a+1)flag2=0;
	}
	if(m==1)
	{
		memset(siz,-1,sizeof(siz));
		dfs(1);
		printfn(ans);
		return 0;
	}
	if(flag1)
	{
		for(register int i=0;i<p[1].size();i++)
			dl[i+1]=v[1][i];
		sort(dl+1,dl+n);
		ans=1<<30;
		for(register int i=n-m;i<=n-1;i++)
		{
			if(n-m-i+n-m-1>0)dl[i]+=dl[n-m-i+n-m-1];
			ans=min(ans,dl[i]);
		}
		printfn(ans);
		return 0;
	}
	if(flag2)
	{
		long long l=0,r=sum;
		memset(siz,0,sizeof(siz));
		int tmp=1;
		for(register int i=1;i<=n-1;i++)
		{
			siz[tmp]=1;
			for(register int j=0;j<p[tmp].size();j++)
			{
				if(siz[p[tmp][j]])continue;
				dl[i]=v[tmp][j];
				tmp=p[tmp][j];
				break;
			}
		}
		while(l<r)
		{
			int mid=((l+r)>>1)+1;
			if(check(mid))l=mid;
			else r=mid-1;
		}
		printfn(l);
		return 0;
	}
	for(register int i=1;i<=n;i++)
	{
		if(p[i].size()<=3&&flag3!=-1)flag3=i;
		if(p[i].size()>4)flag3=-1;
	}
	if(flag3==-1||flag3==0)
	{
		printf("RP++");
		return 0;
	}
	int l=0,r=sum;
	while(l<r)
	{
		Cnt=0;
		int mid=((l+r)>>1)+1;
		memset(siz,-1,sizeof(siz));
		check2(1,mid);
		if(Cnt>=m)l=mid;
		else r=mid-1;
	}
	printfn(l);
	return 0;
}
