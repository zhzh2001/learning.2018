#include <bits/stdc++.h>
using namespace std;
						
const int LIM = 25000;

int T, n, a[2333], f[LIM + 10];

int main(void) {
  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
  scanf("%d", &T);
  while(T--) {
    scanf("%d", &n);
    memset(f, 0, sizeof f);
    f[0] = 1;
    int ans = n;
    for(int i = 1; i <= n; i++) {
      scanf("%d", &a[i]);
    }
    sort(a + 1, a + n + 1);
    for(int i = 1; i <= n; i++) {
      if(f[a[i]]) {
	ans--;
	continue;
      }
      for(int j = 0; j <= LIM - a[i]; j++) {
	if(!f[j]) continue;
	f[j + a[i]] = 1;
      }
    }
    cout << ans << endl;
  }
  return 0;
}
