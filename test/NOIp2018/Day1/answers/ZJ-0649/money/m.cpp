#include <bits/stdc++.h>
using namespace std;

int main(void) {
  int T = 20, MOD = 25000;
  cout << T << endl;
  while(T--) {
    int n = 100;
    cout << n << endl;
    for(int i = 1; i <= n; i++) {
      cout << rand() % MOD + 1 << " ";
    }
    cout << endl;
  }
}
