#include <bits/stdc++.h>
using namespace std;

const int MOD = 10000;

int main(void) {
  freopen("track.in", "w", stdout);
  srand(time(0));
  int n = 10000, m = rand() % 50 + 1;
  printf("%d %d\n", n, m);
  for(int i = 1; i < n; i++)
    printf("%d %d %d\n", i + 1, rand() % i + 1, rand() % MOD + 1);
}
