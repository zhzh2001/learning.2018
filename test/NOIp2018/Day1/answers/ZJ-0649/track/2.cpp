#include <bits/stdc++.h>
using namespace std;

const int N = 55555;

vector<pair<int, int> > g[N];
int val[N], n, m;
pair<int, int> f[N];

void Solve(int x, int fa, int L) {
  f[x] = make_pair(0, 0);
  vector<int> tmp;
  for(int i = 0; i < g[x].size(); i++) {
    int v = g[x][i].first, w = g[x][i].second;
    if(v == fa) continue;
    Solve(v, x, L);
    f[x].first += f[v].first;
    if(f[v].second + w >= L) {
      f[x].first++;
    } else {
      tmp.push_back(f[v].second + w);
    }
  }
  sort(tmp.begin(), tmp.end());
  if(tmp.size() <= 12) {
    for(int i = 0; i < tmp.size(); i++) {
      if(!tmp[i])
	continue;
      for(int j = i + 1; j < tmp.size(); j++)
	if(tmp[i] + tmp[j] >= L) {
	  tmp[i] = tmp[j] = 0;
	  f[x].first++;
	  break;
	}
    }
    for(int i = 0; i < tmp.size(); i++)
      f[x].second = max(f[x].second, tmp[i]);
    return;
  }

  SegT::Init();
  root = 0;
  for(int i = 0; i < tmp.size(); i++)
    SegT::Modify(root, 1, 10000, tmp[i]);
  for(int i = 0; i < tmp.size(); i++) {
    if(mp.find(tmp[i]) == mp.end()) continue;
    map<int, int>::iterator it = mp.lower_bound(L - tmp[i]);
    if(it == mp.end() || (it->first == tmp[i] && it->second == 1))
      continue;
    f[x].first++;
    if(!--mp[it->first])
      mp.erase(it);
    it = mp.find(tmp[i]);
    if(!--mp[it->first])
      mp.erase(it);
  }
  if(mp.size()) {
    f[x].second = mp.rbegin()->first;
  }
}

bool Check(int x) {
  Solve(1, 0, x);
  return f[1].first >= m;
}

int main(void) {
  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
  scanf("%d%d", &n, &m);
  int sum = 0, mi = 1e5;
  for(int i = 1; i < n; i++) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    g[u].push_back(make_pair(v, w));
    g[v].push_back(make_pair(u, w));
    sum += w;
    mi = min(mi, w);
  }
  int l = mi, r = sum / m, Ans = mi;
  while(l <= r) {
    int mid = l + r >> 1;
    if(Check(mid)) {
      Ans = mid;
      l = mid + 1;
    } else {
      r = mid - 1;
    }
  }
  cout << Ans << endl;
  return 0;
}
