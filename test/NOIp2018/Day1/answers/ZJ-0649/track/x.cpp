#include <bits/stdc++.h>
using namespace std;

const int N = 55555;

vector<pair<int, int> > g[N];
int val[N], n, m;
pair<int, int> f[N];

int Calc(vector<int> &tmp, int L) {
  int ans = 0;
  int i = tmp.size() - 1, j = 0;
  while(i >= 0 && tmp[i] >= L) {
    ans++;
    i--;
  }
  for(; ~i; i--) {
    while(j < i && tmp[j] + tmp[i] < L)
      j++;
    if(j >= i) 
      break;
    ans++;
    j++;
  }
  return ans;
}

void Solve(int x, int fa, int L) {
  f[x] = make_pair(0, 0);
  vector<int> tmp;
  for(int i = 0; i < g[x].size(); i++) {
    int v = g[x][i].first, w = g[x][i].second;
    if(v == fa) continue;
    Solve(v, x, L);
    f[x].first += f[v].first;
    tmp.push_back(f[v].second + w);
  }
  sort(tmp.begin(), tmp.end());
  int oo = Calc(tmp, L);
  f[x].first += oo;
  for(int i = tmp.size() - 1; ~i; i--) {
    vector<int> pmt;
    for(int j = 0; j < tmp.size(); j++)
      if(i != j)
	pmt.push_back(tmp[j]);
    if(Calc(pmt, L) == oo) {
      f[x].second = tmp[i];
      break;
    }
    if(f[x].second)
      break;
  }
  //cerr << x << " " << f[x].first << " " << f[x].second << endl;
}

bool Check(int x) {
  Solve(1, 0, x);
  return f[1].first >= m;
}

int main(void) {
  freopen("track.in", "r", stdin);
  freopen("track.ans", "w", stdout);
  scanf("%d%d", &n, &m);
  for(int i = 1; i < n; i++) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    g[u].push_back(make_pair(v, w));
    g[v].push_back(make_pair(u, w));
  }
  int l = 1, r = 5e8, Ans = 1;
  /*
  Check(15);
  return 0;
  */
  while(l <= r) {
    int mid = l + r >> 1;
    if(Check(mid)) {
      Ans = mid;
      l = mid + 1;
    } else {
      r = mid - 1;
    }
  }
  cout << Ans << endl;
  return 0;
}
