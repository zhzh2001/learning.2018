#include <bits/stdc++.h>
using namespace std;

int n;

int main(void) {
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
  scanf("%d", &n);
  int lst = 0, Ans = 0;
  for(int i = 1, x; i <= n; i++) {
    scanf("%d", &x);
    Ans += abs(x - lst);
    lst = x;
  }
  Ans += lst;
  cout << Ans / 2 << endl;
  return 0;
}
