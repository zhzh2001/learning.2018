#include<bits/stdc++.h>
#define FOR(i,a,b) for(int i=(a),i##_end_=(b);i<=i##_end_;i++)
#define DOR(i,a,b) for(int i=(a),i##_end_=(b);i>=i##_end_;i--)
#define ll long long
#define M 100005
using namespace std;

int n;
int ans;
int A[M];

struct node{
	int L,R;
	int Min;
	int Add;
	int pos;
}tree[M<<2];

struct Segment_Tree{


	void Up(int p){
		tree[p].Min=min(tree[p<<1].Min,tree[p<<1|1].Min);
		if(tree[p].Min==tree[p<<1].Min)tree[p].pos=tree[p<<1].pos;
		else tree[p].pos=tree[p<<1|1].pos;
	}

	void Down(int p){
		if(tree[p].Add==0)return;
		int t=tree[p].Add;
		tree[p<<1].Add+=t;
		tree[p<<1].Min+=t;
		tree[p<<1|1].Add+=t;
		tree[p<<1|1].Min+=t;
		tree[p].Add=0;
	}

	void build(int L,int R,int p){
		tree[p].L=L;
		tree[p].R=R;
		if(L==R){
			tree[p].Min=A[L];
			tree[p].pos=L;
			return;
		}
		int mid=(L+R)>>1;
		build(L,mid,p<<1);
		build(mid+1,R,p<<1|1);
		Up(p);
	}

	void update(int L,int R,int a,int p){
		if(tree[p].L==L&&tree[p].R==R){
			tree[p].Min+=a;
			tree[p].Add+=a;
			return;
		}
		Down(p);
		int mid=(tree[p].L+tree[p].R)>>1;
		if(mid>=R)update(L,R,a,p<<1);
		else if(mid<L)update(L,R,a,p<<1|1);
		else update(L,mid,a,p<<1),update(mid+1,R,a,p<<1|1);
		Up(p);
	}

	node query(int L,int R,int p){
		if(tree[p].L==L&&tree[p].R==R)return tree[p];
		Down(p);
		int mid=(tree[p].L+tree[p].R)>>1;
		if(mid>=R)return query(L,R,p<<1);
		else if(mid<L)return query(L,R,p<<1|1);
		else {
			node a=query(L,mid,p<<1);
			node b=query(mid+1,R,p<<1|1);
			node c=a;
			if(c.Min>b.Min){
				c.Min=b.Min;
				c.pos=b.pos;
			}
			return c;
		}
	}

}T;

void solve(int L,int R){
	if(L>R)return;
	node p=T.query(L,R,1);
	ans+=p.Min;
	T.update(L,R,-p.Min,1);
	solve(L,p.pos-1);
	solve(p.pos+1,R);
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);	
	scanf("%d",&n);
	FOR(i,1,n)scanf("%d",&A[i]);
	T.build(1,n,1);
	solve(1,n);
	printf("%d\n",ans);
	return 0;
}
