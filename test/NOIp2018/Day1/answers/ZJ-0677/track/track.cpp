#include<bits/stdc++.h>
#define FOR(i,a,b) for(int i=(a),i##_end_=(b);i<=i##_end_;i++)
#define DOR(i,a,b) for(int i=(a),i##_end_=(b);i>=i##_end_;i--)
#define EOR(i,x) for(int i=Head[x];~i;i=Nxt[i])
#define ll long long
#define M 50005
using namespace std;

int n,m;

int tol;
int Head[M];
int Nxt[M<<1];

struct Edge{
	int to,cost;
}G[M<<1];

void Add_edge(int a,int b,int c){
	G[tol]=(Edge){b,c};
	Nxt[tol]=Head[a];
	Head[a]=tol++;
}

int dis[M];

struct PCHAIN{

	int val[M];

	bool check(int len){
		int now=1;
		int cnt=0;
		while(true){
			int L=now,R=n,res=-1;
			while(L<=R){
				int mid=(L+R)>>1;
				if(val[mid]-val[now]>=len)res=mid,R=mid-1;
				else L=mid+1;
			}
			if(res==-1)return false;
			cnt++;
			now=res;
			if(cnt>=m)return true;
		}
		return false;
	}

	void solve(){
		FOR(i,1,n)val[i]=val[i-1]+dis[i];
		int L=0,R=1e9,res=0;
		while(L<=R){
			int mid=(L+R)>>1;
			if(check(mid))res=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",res);
	}

}pchain;

struct PZHIJING{

	int rt1,rt2;
	int Max;
	
	void dfs(int x,int f,int d){
		if(d>Max)Max=d,rt1=x;
		EOR(i,x){
			Edge a=G[i];
			if(a.to==f)continue;
			dfs(a.to,x,d+a.cost);
		}
	}

	void solve(){
		dfs(1,0,0);
		Max=0;
		rt2=rt1;
		dfs(rt1,0,0);
		printf("%d\n",Max);
	}

}pzhijing;

struct PJUHUA{

	int Tmp[M];
	int top;

	bool check(int len){
		int tot=0;
		int pos=top;
		DOR(i,top,1){
			if(Tmp[i]>=len)tot++;
			else {
				pos=i;
				break;
			}
		}
		if(tot>=m)return true;
		for(int i=1;i<pos;i++){
			if(Tmp[i]+Tmp[pos]>=len){
				tot++;
				pos--;
			}
		}
		return tot>=m;
	}

	void solve(){
		FOR(i,2,n)Tmp[++top]=dis[i];
		sort(Tmp+1,Tmp+top+1);
		int L=1,R=1e9,res=-1;
		while(L<=R){
			int mid=(L+R)>>1;
			if(check(mid))res=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",res);
	}

}pjuhua;

struct PDP{

	int l;
	int mark[M];
	int top;
	int Fa[M];
	int dep[M];

	struct node{
		int cnt;//不计算最长的那一条 
		int Max;
		int pos;
		
		bool operator < (const node &a)const{
			return Max<a.Max;
		}
		
	}dp[M];

	multiset<node>mt1,mt2;
	multiset<node>::iterator it,it1;

	void dfs(int x,int f,int d){
		Fa[x]=f;
		dep[x]=dep[f]+d;
		int flag=0;
		EOR(i,x){
			Edge a=G[i];
			int y=a.to;
			if(y==f)continue;
			flag=1;
			int v=a.cost;
			if(v>l)v=l;
			dfs(y,x,v);
			dp[x].cnt+=dp[y].cnt;
		}
		if(!flag){
			if(d>=l){
				dp[x].Max=0;
				dp[x].pos=Fa[x];
				dp[x].cnt=1;
			}
			else {
				dp[x].Max=d;
				dp[x].pos=x;
				dp[x].cnt=0;
			}
			return;
		}
		mt1.clear();
		mt2.clear();
		EOR(i,x){
			Edge a=G[i];
			int y=a.to;
			if(y==f)continue;
			node b=dp[y];
			b.cnt=y;
			mt1.insert(b);
			mt2.insert(b);
		}
		for(it=mt1.begin();it!=mt1.end();it++){
			node a=*it;
			if(mark[a.cnt])continue;
			it1=mt2.find(*it);
			mt2.erase(it1);
			node b=a;
			b.Max=l-a.Max;
			it1=mt2.lower_bound(b);
			if(it1!=mt2.end()){
				mark[a.cnt]=1;
				node t=*it1;
				mark[t.cnt]=1;
				mt2.erase(it1);
				dp[x].cnt++;
			}
		}
		int pos=0,ff=0,res=0;
		EOR(i,x){
			Edge a=G[i];
			if(a.to==f||mark[a.to])continue;
			int y=a.to;
			if(dp[y].Max+d>=l){
				int now=dp[y].pos;
				while(now!=Fa[x]){
					if(dep[Fa[now]]-dep[Fa[x]]>=l){
						now=Fa[now];
					}
					else break;
				}
				int tt=dep[Fa[now]]-dep[Fa[x]];
				if(tt>res||f==0){
					res=tt;
					pos=Fa[now];
				}
				ff=1;
			}
			else if(!ff){
				int tt=dp[y].Max+d;
				if(tt>res){
					res=tt;
					pos=dp[y].pos;
				}
			}
		}
		dp[x].cnt+=ff;
		dp[x].Max=res;
		dp[x].pos=pos;
	}

	bool check(int len){
		l=len;
		dfs(1,0,0);
		return dp[1].cnt>=m;
	}

	void solve(){
		int L=0,R=1e9,res=0;
		while(L<=R){
			memset(mark,0,sizeof(mark));
			FOR(i,1,n){
				dp[i].Max=dp[i].cnt=dp[i].pos=0;
			}
			int mid=(L+R)>>1;
			if(check(mid)){
				res=mid,L=mid+1;
			}
			else R=mid-1;
		}
		printf("%d\n",res);
	}

}pdp;

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(Head,-1,sizeof(Head));
	scanf("%d%d",&n,&m);
	int flag1=1;//链 
	int flag2=(m==1);//m=1(找直径)
	int flag3=1;
	FOR(i,2,n){
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		if(a>b)swap(a,b);
		if(b!=a+1)flag1=0;
		if(a!=1)flag3=0;
		dis[b]=c;
		Add_edge(a,b,c);
		Add_edge(b,a,c);
	}
	if(flag1)pchain.solve();
	else if(flag2)pzhijing.solve();
	else if(flag3)pjuhua.solve();
	else pdp.solve();
	return 0;
}
