#include<bits/stdc++.h>
#define FOR(i,a,b) for(int i=(a),i##_end_=(b);i<=i##_end_;i++)
#define DOR(i,a,b) for(int i=(a),i##_end_=(b);i>=i##_end_;i--)
#define ll long long
#define M 105
#define N 25005
using namespace std;

int T;
int n,m;
int A[M];
int dp[N];
int now[N];
int Tmp[N];

bool check(int x){
	if(dp[x]==0)return false;
	memcpy(Tmp,now,sizeof(Tmp));
	FOR(i,0,N-5){
		if(Tmp[i]==0)continue;
		if(i+x>m)break;
		if(dp[i+x]==0)return false;
		Tmp[i+x]=1;
	}
	return true;
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		memset(dp,0,sizeof(dp));
		memset(now,0,sizeof(now));
		scanf("%d",&n);
		FOR(i,1,n)scanf("%d",&A[i]);
		sort(A+1,A+n+1);
		m=A[n];
		dp[0]=1;
		FOR(i,1,n){
			dp[A[i]]=1;
			FOR(j,0,m){
				if(dp[j]==0)continue;
				if(j+A[i]>m)break;
				dp[A[i]+j]=1;
			}
		}
		now[0]=1;
		int ans=0;
		FOR(i,1,N-5){
			if(ans==n)break;
			if(now[i])continue;
			if(check(i)==0)continue;
			ans++;
			FOR(j,0,N-5){
				if(j+i>m)break;
				if(now[j]==0)continue;
				now[j+i]=1;
			}
			int f=1;
			FOR(j,0,m){
				if(dp[j]!=now[j]){
					f=0;
					break;
				}
			}
			if(f)break;
		}
		printf("%d\n",ans);
	}
	return 0;
}
