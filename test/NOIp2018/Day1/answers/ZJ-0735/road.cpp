#include<bits/stdc++.h>
#define INF 1000000
#define N 200010
using namespace std;
int ans,a[N],k,n;
int e[N],d[N],g[N];
struct node{
	int x,id;
} f[N][20];
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<=n;i++) f[i][0].x=a[i],f[i][0].id=i;
	k=1;
	while ((1<<k)<n) k++; 
	for (int i=1;i<=k;i++)
		for (int j=1;j<=n;j++)
			if (f[j][i-1].x<f[j+(1<<(i-1))][i-1].x)
				{
					f[j][i].x=f[j][i-1].x;
					f[j][i].id=f[j][i-1].id;
				}
			else
				{
					f[j][i].x=f[j+(1<<(i-1))][i-1].x;
					f[j][i].id=f[j+(1<<(i-1))][i-1].id;
				}	
	int t=1,w=1;
	e[t]=1;d[t]=n;g[t]=0;
	while (t<=w)
		{
			int mn=INF,p=0,x=e[t];
			for (int i=k;i>=0;i--)
			if (x+(1<<i)-1<=d[t])
				{
					if (f[x][i].x<=mn)
						{
							mn=f[x][i].x;
							p=f[x][i].id;
						}
					x=x+(1<<i);
				}
			ans=ans+(mn-g[t]);
			if (p-1>=e[t]) { w++; e[w]=e[t]; d[w]=p-1; g[w]=mn;	}
			if (p+1<=d[t]) { w++; e[w]=p+1; d[w]=d[t]; g[w]=mn; }
			t++;
		}
	printf("%d\n",ans);
	return 0;
}
