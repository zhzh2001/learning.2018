#include<bits/stdc++.h>
#define N 200
#define M 100000
using namespace std;
bool f[M];
int a[N],n;
int max(int x,int y) { return x>y?x:y; }
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int test;
	scanf("%d",&test);
	while (test--)
		{
			scanf("%d",&n);
			int mx=0;
			for (int i=1;i<=n;i++) scanf("%d",&a[i]),mx=max(mx,a[i]);
			sort(a+1,a+n+1);
			int cnt=n;
			memset(f,0,sizeof(f));
			f[0]=true;
			for (int i=1;i<=n;i++)
				{
					if (f[a[i]]) { cnt--; continue;	} 
					for (int j=0;j<=mx;j++)
						if (f[j])
							f[j+a[i]]=true;
				}
			printf("%d\n",cnt);
		}
	return 0;
}
