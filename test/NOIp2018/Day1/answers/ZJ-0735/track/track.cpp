#include<bits/stdc++.h>
#define N 50100
#define INF 666666666
using namespace std;
vector< pair<int,int> > f[N];
int dis[N],n,m,ans,a[N],now,b[N],tot;
bool vis[N];
void zj(int x,int fa)
{
	int len=f[x].size();
	for (int i=0;i<len;i++)
		{
			int p=f[x][i].first;
			if (p==fa) continue;
			dis[p]=dis[x]+f[x][i].second;
			zj(p,x);
		}
}
void tsdfs(int x,int fa)
{
	int len=f[x].size();
	for (int i=0;i<len;i++)
		{
			int p=f[x][i].first;
			if (p==fa) continue;
			a[x]=f[x][i].second;
			tsdfs(p,x);
		}
}
bool pdts(int x)
{
	int sum=0,p=0;
	for (int i=1;i<n;i++)
		{
			sum=sum+a[i];
			if (sum>=x) 
				{
					p++;
					sum=0;
				}
		}
	if (p>=m) return true; else return false;
}
void dfs(int x,int fa)
{
	int len=f[x].size();
	for (int i=0;i<len;i++)
		{
			int p=f[x][i].first;
			if (p==fa) continue;
			dfs(p,x);
			dis[p]=dis[p]+f[x][i].second;
		}
	int cnt=0,sum=0;
	for (int i=0;i<len;i++)
		{
			int p=f[x][i].first;
			if (p==fa) continue;
			if (dis[p]>=now) { sum++; continue; }
			cnt++;
			b[cnt]=dis[p];
		}
	sort(b+1,b+cnt+1);
	if (cnt==1) dis[x]=b[1];
	else
		{
			int l=2;
			memset(vis,0,sizeof(vis));
			for (int i=1;i<=cnt;i++)
				{
					bool ff=false;
					for (int j=i+1;j<=cnt;j++)
						if (b[i]+b[j]>=now&&vis[j]==false)
							{
								sum++;
								ff=true;
								vis[j]=true;
								break;
							}	
					if (!ff) dis[x]=b[i];
				}
				
		}
	tot=tot+sum;
}
bool pd(int x)
{
	now=x;
	tot=0;
	memset(dis,0,sizeof(dis));
	dfs(1,0);
	if (tot>=m) return true; else return false;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool ts=true;
	int s=0;
	for (int i=1;i<n;i++)
		{
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			f[x].push_back(make_pair(y,z));
			f[y].push_back(make_pair(x,z));
			if (y!=x+1) ts=false;
			s=s+z;
		}
	if (m==1)
		{
			memset(dis,0,sizeof(dis));
			zj(1,0);
			int k=0,mx=0;
			for (int i=1;i<=n;i++) if (dis[i]>mx) { mx=dis[i]; k=i;	}
			memset(dis,0,sizeof(dis));
			zj(k,0);
			mx=0;
			for (int i=1;i<=n;i++) if (dis[i]>mx) mx=dis[i]; 
			printf("%d\n",mx);
			return 0;
		}
	if (ts) tsdfs(1,0);
	int l=0,r=s,ans=0;
	while (l<=r)
		{
			bool flag=false;
			int mid=(l+r)/2;
			if (ts) flag=pdts(mid); else flag=pd(mid);
			if (flag)
				{
					ans=mid;
					l=mid+1;
				}
			else
				r=mid-1;
		}
	printf("%d\n",ans);
	return 0;
}
