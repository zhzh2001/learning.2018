#include<cstdio>
#include<algorithm>
using namespace std;
int read()
{
	int x=0,w=1;
	char  c=getchar();
	while(c<'0'||c>'9'){if(c=='-')w=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x*w;
}
int a[105]={0},flag;
void dfs(int sum,int num,int tar)
{
	if(sum>a[tar]||flag)
		return ;
	if(sum==a[tar])
	{
		flag=1;
		return ;
	}
	for(int i=1;i<=a[tar]/a[num];i++)
		dfs(sum+i*a[num],num+1,tar);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t=read(),tot,n,v[105],cnt,k;
	while(t--)
	{
		n=read();
		tot=n-1;
		for(int i=1;i<=n;i++)
		{
			a[i]=read();
			v[i]=1;
		}
		sort(a+1,a+1+n);
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++)
				if(a[j]%a[i]==0)
				{
					v[j]=0;
					tot--;
				}
		for(k=2;k<=n;k++)
			if(v[k])
				break;
		if(k>n)
		{
			printf("1\n");
			continue;
		}
		if(n==2)
		{
			printf("2\n"); 
			continue;
		}
		for(int i=2;i<=n;i++)
		{
			if(!v[i])
				continue;
			flag=0;
			dfs(0,1,i);
			if(flag)
				v[i]=0;
		}
		int ans=0;
		for(int i=1;i<=n;i++)
			ans+=v[i];
		printf("%d",ans);
	}	
	return 0;
}
