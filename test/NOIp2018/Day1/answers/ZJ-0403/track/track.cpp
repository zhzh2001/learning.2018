#include<cstdio>
#include<algorithm>
using namespace std;
const int MAXN=50003;
int read()
{
	int x=0,w=1;
	char  c=getchar();
	while(c<'0'||c>'9'){if(c=='-')w=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x*w;
}
struct EDGE
{
	int from,to,next,w;
}e[MAXN*2];
int head[MAXN]={0},cnt=0,p[MAXN][30],dep[MAXN]={0},d[MAXN]={0};
int dis[MAXN],n,m;
void add(int u,int v,int w)
{
	e[++cnt].from=u;
	e[cnt].to=v;
	e[cnt].w=w;
	e[cnt].next=head[u];
	head[u]=cnt;
}
void dfs(int u,int fa)
{
	dep[u]=dep[fa]+1;
	p[u][0]=fa;
	for(int i=1;(1<<i)<=dep[u];i++)
		p[u][i]=p[p[u][i-1]][i-1];
	for(int i=head[u];i;i=e[i].next)
	{
		int v=e[i].to,w=e[i].w;
		if(v==fa)
			continue;
		d[v]=d[u]+w;
		dfs(v,u);
	}
}
int LCA(int a,int b)
{
	if(dep[a]<dep[b])
		swap(a,b);
	for(int i=20;i>=0;i--)
	{
		if(dep[a]-(1<<i)>=dep[b])
			a=p[a][i];
	}
	if(a==b)
		return a;
	for(int i=20;i>=0;i--)
	{
		if(p[a][i]!=p[b][i])
		{
			a=p[a][i];
			b=p[b][i];
		}
	}
	return p[a][0];
}
bool cmp(int a,int b)
{
	return a>b;
}
int check(int mid)
{
	int sum=0,cnt=0;
	for(int i=2;i<=n;i++)
	{
		sum+=d[i];
		if(sum>=mid)
		{
			sum=0;
			cnt++;
		}
	}
	return cnt>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	int u,v,w,ans=0,ans1=0,flag=1,Min=1<<30;
	int sum=0;
	for(int i=1;i<n;i++)
	{
		u=read(),v=read(),w=read();
		if(u>v) swap(u,v);
		if(u!=1)
			flag=0;
		dis[v]=w;
		Min=min(Min,w);
		sum+=w;
		add(u,v,w);
		add(v,u,w);
	}
	if(m==n-1)
	{
		printf("%d\n",Min);
		return 0;
	}
	if(flag)
	{
		
		sort(dis+2,dis+n+1,cmp);
		if(m*2>n)
		{
			printf("%d\n",dis[m+1]);
			return 0;
		}
		else
		{
			int i=2,j=2*m+1;
			ans=1<<30;
			while(i<j)
			{
				ans=(ans,dis[i]+dis[j]);
				i++;
				j--;
			}
			printf("%d\n",ans);
			return 0;
		}
	}
	dfs(1,0);
	if(m==1)
	{
		for(int i=1;i<=n;i++)
			for(int j=1;j<i;j++)
			{
				int lca=LCA(i,j);
				ans=max(ans,d[i]+d[j]-d[lca]*2);
			}
		printf("%d\n",ans);
		return 0;
	}
	int l=1,r=w;
	while(l<=r)
	{
		int mid=(l+r)/2;
		if(check(mid))
		{
			ans=mid;
			l=mid+1;	
		}
		else
			r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
