#include<cstdio>
//#include<ctime>
using namespace std;
const int MAXN=100003;
typedef long long ll;
int read()
{
	int x=0,w=1;
	char  c=getchar();
	while(c<'0'||c>'9'){if(c=='-')w=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x*w;
}
int n,d[MAXN],duan[MAXN],cnt=0;
ll tian(int l,int r)
{
	if(l>r)
		return 0;
	if(l==r)
		return (ll)d[l];
	ll ans=0;
	int Min=1<<30,k=0;
	for(int i=l;i<=r;i++)
	{
		if(d[i]<Min)
		{
			Min=d[i];
			k=i;
		}
	}
	ans+=Min;
	for(int i=l;i<=r;i++)
	{
		d[i]-=Min;
	}
	ans+=tian(l,k-1);
	ans+=tian(k+1,r);
	return ans;
}
int main()
{
	//double st=clock(),en;
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);	
	n=read();
	for(int i=1;i<=n;i++)
	{
		d[i]=read();
		if(!d[i])
			duan[++cnt]=i;
	}
	ll ans=tian(1,duan[1]-1)+tian(duan[cnt]+1,n);
	for(int i=2;i<=cnt;i++)
		ans+=tian(duan[i-1]+1,duan[i]-1);
	printf("%lld\n",ans);
	//en=clock();
	//printf("%.2lf ms\n",en-st);
	return 0;
}
