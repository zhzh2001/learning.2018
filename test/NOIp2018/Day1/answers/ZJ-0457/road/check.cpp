#include<bits/stdc++.h>
#define M 100005
using namespace std;
int n,A[M];
struct PBL{
	void solve(){
		int mx=0;
		for(int i=1;i<=n;i++)mx=max(mx,A[i]);
		int ans=0;
		for(int i=1;i<=mx;i++){
			int c=0;
			for(int j=1;j<=n+1;j++)if(j>n||A[j]<i)c+=(A[j-1]>=i);
//			printf("%d %d\n",i,c);
			ans+=c;
		}cout<<ans<<endl;
	}
}pbl;
struct P100{
	int L[M],R[M];
	struct node{
		int id,d;
		bool operator <(const node &_)const{
			return d>_.d;
		}
	};
	priority_queue<node>Q;
	void solve(){
		int mx=0;
		for(int i=1;i<=n;i++)L[i]=i-1,R[i]=i+1,mx=max(mx,A[i]);	
		R[0]=1;L[n+1]=n;
		for(int i=1;i<=n;i++)Q.push((node){i,A[i]});
		long long ans=0;
		for(int i=0,c=1;i<=mx;i++){
			ans+=c*(i>0);
//			printf("%d %d\n",i,c);
			while(!Q.empty()&&Q.top().d<=i){
				int x=Q.top().id;Q.pop();
				if(x==1){
					if(R[x]>2)c--;
				}else if(x==n){
					if(L[x]<n-1)c--;			
				}else {
					if(L[x]<x-1&&R[x]>x+1)c--;
					else if(L[x]==x-1&&R[x]==x+1)c++;
				}
				L[R[x]]=L[x];
				R[L[x]]=R[x];
			}
		}cout<<ans<<endl;
	}
}p100;
int main(){//road
	freopen("road.in","r",stdin);
	freopen("check.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&A[i]);
	pbl.solve();
//	p100.solve();
	return 0;
}
