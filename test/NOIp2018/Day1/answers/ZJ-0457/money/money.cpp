#include<bits/stdc++.h>
#define M 105
#define P 25000
using namespace std;
bool ___1;
int A[M],n;
struct P65{
	bool can[P+5],res[P+5];
	int bin[(1<<13)+1],tmp[15];
	void solve(){
		for(int i=1;i<13;i++)bin[1<<i]=i;
		int base=(1<<n)-1,mx=0;
		for(int i=1;i<=n;i++)mx=max(mx,A[i]);
		memset(can,0,sizeof(can));
		can[0]=1;
		for(int i=1;i<=n;i++)
			for(int j=A[i];j<=mx;j++)
				can[j]|=can[j-A[i]];
		int ans=n;
		for(int i=1;i<=base;i++){
			int ii=i,c=0;
			while(ii){
				tmp[++c]=bin[ii&-ii]+1;
				ii&=ii-1;
			}
			memset(res,0,sizeof(res));res[0]=1;
			for(int j=1;j<=c;j++)
				for(int q=A[tmp[j]];q<=mx;q++)
					res[q]|=res[q-A[tmp[j]]];
			bool f=1;
			for(int j=1;f&&j<=mx;j++)if(res[j]!=can[j])f=0;
			if(f)ans=min(ans,c);
		}cout<<ans<<endl;
	}
}p65;
struct P100{
	bool can[P+5];
	void solve(){
		memset(can,0,sizeof(can));
		sort(A+1,A+n+1);
		can[0]=1;
		int ans=0;
		for(int i=1;i<=n;i++){
			if(can[A[i]])continue;
			ans++;
			for(int j=A[i];j<=P;j++)
				can[j]|=can[j-A[i]];
		}printf("%d\n",ans);
	}	
}p100;
bool ___2;
int main(){//money case_clear
//	printf("%lf\n",(&___2-&___1)/1024.0/1024);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int Time;scanf("%d",&Time);
	while(Time--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",&A[i]);
//		p65.solve();
		p100.solve();
	}
	return 0;
}
