#include<bits/stdc++.h>
#define M 50005
using namespace std;
bool ___1;
int n,m;
struct edge{
	int to,co,fr;
}E[M<<1];
int lst[M],tot;
void addedge(int a,int b,int c){
	E[++tot]=(edge){b,c,lst[a]};
	lst[a]=tot;
}
struct P20{
	int mxd,pos;
	void dfs(int v,int f,int di){
		if(mxd<di)mxd=di,pos=v;
		for(int i=lst[v];i;i=E[i].fr){
			int u=E[i].to;
			if(u==f)continue;
			dfs(u,v,di+E[i].co);
		}
	}
	void solve(){
		mxd=-1;dfs(1,0,0);
		mxd=-1;dfs(pos,0,0);
		printf("%d\n",mxd);
	}
}p20;
int deg[M];
struct P40{
	bool check(){
		return deg[1]<2&&deg[n]<2;
	}
	int dis[M];
	void dfs(int v,int f){
		for(int i=lst[v];i;i=E[i].fr){
			int u=E[i].to;
			if(u==f)continue;
			dis[u]=dis[v]+E[i].co;
			dfs(u,v);
		}
	}
	bool check(int x){
		int lst=0,c=0;
		for(int i=2;i<=n;i++)
			if(dis[i]-dis[lst]>=x)lst=i,c++;
		return c>=m;
	}
	void solve(){
		dfs(1,0);
		int l=1,r=dis[n],res=0;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))l=mid+1,res=mid;
			else r=mid-1;
		}printf("%d\n",res);
	}
}p40;
struct P55{
	int dis[M];
	bool check(int x){
		int r=n,l=2,c=0;
		while(c<m&&l<=r){
			if(dis[r]>=x)r--,c++;
			else if(l<r&&dis[r]+dis[l]>=x)r--,l++,c++;
			else l++;
		}return c>=m;
	}
	void solve(){
		for(int i=lst[1];i;i=E[i].fr)
			dis[E[i].to]=E[i].co;
		sort(dis+2,dis+n+1);
		int l=1,r=5e8,res=1;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))l=mid+1,res=mid;
			else r=mid-1;
		}printf("%d\n",res);
	}
}p55;
struct P{
	int all,tmp[M];
	bool mark[M];
	vector<int>D[1005];
	void dfs(int v,int f,int x){
		D[v].clear();bool lf=1;
		for(int i=lst[v];i;i=E[i].fr){
			int u=E[i].to;
			if(u==f)continue;
			lf=0;dfs(u,v,x);
		}
		if(!lf&&all<m){
			int len=0;
			for(int i=lst[v];i;i=E[i].fr){
				int u=E[i].to;
				if(u==f)continue;
				sort(D[u].begin(),D[u].end());
				tmp[++len]=D[u][D[u].size()-1]+E[i].co;
				mark[len]=0;
			}sort(tmp+1,tmp+len+1);
			int l=1,r=len;
			while(r>=l&&all<m){
				if(tmp[r]>=x)mark[r]=1,r--,all++;
				else if(l<r&&tmp[r]+tmp[l]>=x)mark[l]=mark[r]=1,l++,r--,all++;
				else l++;
			}
			for(int i=1;i<=len;i++){
				if(!mark[i])D[v].push_back(tmp[i]);
				mark[i]=0;
			}
		}if(D[v].empty())D[v].push_back(0);
	}
	bool check(int x){
		all=0;memset(mark,0,sizeof(mark));
		dfs(1,0,x);
		return all>=m;
	}
	void solve(){
		int l=1,r=5e8,res=1;
		while(l<=r){
			int mid=((long long)l+r)>>1;
			if(check(mid))l=mid+1,res=mid;
			else r=mid-1;
		}printf("%d\n",res);
	}
}p;
bool ___2;
int main(){//track
//	printf("%lf\n",(&___2-&___1)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m==n-1){
		int mn=2e9;
		for(int i=1,a,b,c;i<n;i++){
			scanf("%d%d%d",&a,&b,&c);
			mn=min(mn,c);
		}printf("%d\n",mn);
	}else{
		for(int i=1,a,b,c;i<n;i++){
			scanf("%d%d%d",&a,&b,&c);
			deg[a]++,deg[b]++;
			addedge(a,b,c);
			addedge(b,a,c);
		}
		if(m==1)p20.solve();
		else if(p40.check())p40.solve();
		else if(deg[1]==n-1)p55.solve();
		else p.solve();
	}
	return 0;
}
