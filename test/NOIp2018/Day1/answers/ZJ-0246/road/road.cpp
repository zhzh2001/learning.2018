#include<bits/stdc++.h>
#define ll long long
#define M 100005
using namespace std;
bool pppp1;

int n,A[M];
struct Tree{
	int Mn[M<<2],W[M<<2];
	void Up(int p){
		int l=p<<1,r=p<<1|1;
		if(Mn[l]<=Mn[r])Mn[p]=Mn[l],W[p]=W[l];
		else Mn[p]=Mn[r],W[p]=W[r];
	}
	void Build(int L,int R,int p){
		if(L==R){
			Mn[p]=A[L];
			W[p]=L;
			return;
		}
		int mid=(L+R)>>1;
		Build(L,mid,p<<1);
		Build(mid+1,R,p<<1|1);
		Up(p);
	}
	int Query(int L,int R,int l,int r,int p){
		if(L==R)return W[p];
		int mid=(L+R)>>1;
		if(mid>=r)return Query(L,mid,l,r,p<<1);
		else if(mid<l)return Query(mid+1,R,l,r,p<<1|1);
		else{
			int l1=Query(L,mid,l,mid,p<<1),r1=Query(mid+1,R,mid+1,r,p<<1|1);
			if(A[l1]<=A[r1])return l1;
			else return r1;
		}
	}
}tree;
ll ans;
void solve(int L,int R,int las){
	if(L>R)return;
	int mn=tree.Query(1,n,L,R,1);
	ans+=A[mn]-las;
	solve(L,mn-1,A[mn]);
	solve(mn+1,R,A[mn]);
}

bool pppp2;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
//	printf("%.2lf\n",(&pppp2-&pppp1)/1024.0/1024);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&A[i]);
	tree.Build(1,n,1);
	solve(1,n,0);
	printf("%lld\n",ans);
	return 0;
}
