#include<bits/stdc++.h>
#define M 50005
using namespace std;
bool pppp1;

int n,m;
int head[M],tot;
struct Edge{int nxt,to,v;}G[M*2];
void addedge(int x,int y,int v){
	G[tot]=(Edge){head[x],y,v};
	head[x]=tot++;
}
struct P1{
	int fa[20],dep[20],dis[20],Id[20],id;
	void f1(int x,int fa1){
		fa[x]=fa1;
		dep[x]=dep[fa1]+1;
		for(int i=head[x];i!=-1;i=G[i].nxt){
			int y=G[i].to;
			if(y==fa1)continue;
			Id[y]=++id;
			dis[y]=G[i].v;
			f1(y,x);
		}
	}
	int ans,Q[20];
	int Judge(int x,int y){
		int d=0;
		while(x!=y){
			if(dep[x]<dep[y])swap(x,y);
			d+=dis[x];
			if(Q[Id[x]])return -1;
			x=fa[x];
		}
		return d;
	}
	void Mark(int x,int y,int v){
		while(x!=y){
			if(dep[x]<dep[y])swap(x,y);
			Q[Id[x]]+=v;
			x=fa[x];
		}
	}
	void Get(int x,int now){
		if(x==m+1){
			ans=max(ans,now);
			return;
		}
		if(now<ans)return;
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++){
				int p=Judge(i,j);
				if(p==-1)continue;
				Mark(i,j,1);
				Get(x+1,min(now,p));
				Mark(i,j,-1);
			}
	}
	void solve(){
		f1(1,0);
		ans=0;
		Get(1,1e9);
		printf("%d\n",ans);
	}
}P1;

struct P2{
	int rt,Mx;
	void f1(int x,int fa1,int d){
		if(d>Mx)Mx=d,rt=x;
		for(int i=head[x];i!=-1;i=G[i].nxt){
			int y=G[i].to;
			if(y==fa1)continue;
			f1(y,x,d+G[i].v);
		}
	} 
	void solve(){
		rt=0,Mx=-1;
		f1(1,0,0);
		Mx=-1;
		f1(rt,0,0);
		printf("%d\n",Mx);
	}
}P2;

struct P3{
	int n1,V[M];
	void solve(){
		for(int i=head[1];i!=-1;i=G[i].nxt)V[++n1]=G[i].v;
		sort(V+1,V+1+n1);
		if(m*2<=n1){
			int ans=1e9;
			int L=n1-2*m+1,R=n1;
			while(L<R){
				ans=min(ans,V[L]+V[R]);
				L++;R--;
			}
			printf("%d\n",ans);
		}else{
			int a=n1-m;
			int L=1,R=2*a,ans=1e9;
			while(L<R){
				ans=min(ans,V[L]+V[R]);
				L++;R--;
			}
			for(int i=2*a+1;i<=n1;i++)ans=min(ans,V[i]);
			printf("%d\n",ans);
		}
	}
}P3;

struct P4{
	int V[M];
	bool Judge(int x){
		int cnt=0,now=0;
		for(int i=1;i<n;i++){
			now+=V[i];
			if(now>=x)now=0,cnt++;
		}
		return cnt>=m;
	}
	void solve(){
		for(int i=1;i<n;i++)
			for(int j=head[i];j!=-1;j=G[j].nxt)
				if(G[j].to==i+1)V[i]=G[j].v;
		int L=0,R=500000000,ans=0;
		while(L<=R){
			int mid=(L+R)>>1;
			if(Judge(mid))L=mid+1,ans=mid;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}P4;

int All;
struct P5{
	int dp[M],W[M];
	multiset<int>B;
	multiset<int>::iterator it,it1;
	void f1(int x,int fa1,int len){
		dp[x]=W[x]=0;
		for(int i=head[x];i!=-1;i=G[i].nxt){
			int y=G[i].to;
			if(y==fa1)continue;
			f1(y,x,len);
		}
		for(int i=head[x];i!=-1;i=G[i].nxt){
			int y=G[i].to;
			if(y==fa1)continue;
			dp[x]+=dp[y];
			if(W[y]+G[i].v>=len)dp[x]++;
			else B.insert(W[y]+G[i].v);
		}
		while(1){
			if(B.size()==0)break;
			it=B.begin();
			B.erase(it);
			if(B.size()==0){
				W[x]=max(W[x],*it);
				break;
			}
			int ned=len-*it;
			it1=B.lower_bound(ned);
			if(it1!=B.end()){
				dp[x]++;
				B.erase(it1);
			}else{
				W[x]=max(W[x],*it);
			}
		}
		if(B.size()!=0){
			it=B.end();
			it--;
			W[x]=max(W[x],*it);
		}
//		if(len==15){
//			cerr<<x<<" "<<dp[x]<<" "<<W[x]<<" "<<B.size()<<endl;
//		}
		B.clear();
	}
	bool Judge(int x){
		f1(1,0,x);
		return dp[1]>=m;
	}
	void solve(){
		int L=0,R=All,ans=0;
		while(L<=R){
			int mid=(L+R)>>1;
			if(Judge(mid))L=mid+1,ans=mid;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}P5;

bool pppp2;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
//	printf("%.2lf\n",(&pppp2-&pppp1)/1024.0/1024);
	memset(head,-1,sizeof head);
	scanf("%d%d",&n,&m);
//	int p1=1,p2=1;
	for(int i=1;i<n;i++){
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		addedge(x,y,v);
		addedge(y,x,v);
		All+=v;
//		if(x!=1)p1=0;
//		if(y!=x+1)p2=0;
	}
//	if(n<=15)P1.solve();
//	else if(m==1)P2.solve();
//	else if(p1)P3.solve();
//	else if(p2)P4.solve();
//	else 
	P5.solve();
	return 0;
}
