#include<bits/stdc++.h>
#define M 105
#define N 25005
using namespace std;
bool pppp1;

int n,Mx,A[M],B[M];

struct P1{
	int B[M],n1;
	int dp[M][N];
	int Judge(int x){
		for(int i=1;i<=n1;i++)
			for(int j=0;j<=A[x];j++)dp[i][j]=0;
		dp[0][0]=1;
		for(int i=1;i<=n1;i++){
			for(int j=0;j+B[i]<=A[x];j++)
				dp[i][j+B[i]]|=(dp[i-1][j]|dp[i][j]);
			for(int j=0;j<=A[x];j++)dp[i][j]|=dp[i-1][j];
		}
		return dp[n1][A[x]];
	}
	void solve(){
		sort(A+1,A+1+n);
		n1=0;
		B[++n1]=A[1];
		for(int i=2;i<=n;i++)
			if(!Judge(i))B[++n1]=A[i];
		printf("%d\n",n1);
	}
}P1;

struct P2{
	int ans,dp[M][N];
	void Add(int x){
		ans++;
		for(int i=0;i+A[x]<=A[n];i++)
			dp[ans][i+A[x]]|=(dp[ans-1][i]|dp[ans][i]);
		for(int i=0;i<=A[n];i++)
			dp[ans][i]|=dp[ans-1][i];
	}
	void solve(){
		sort(A+1,A+1+n);
		ans=0;
		for(int i=1;i<=n;i++)
			for(int j=0;j<=A[n];j++)dp[i][j]=0;
		dp[0][0]=1;
		Add(1);
		for(int i=2;i<=n;i++){
			if(!dp[ans][A[i]])Add(i);
		}
		printf("%d\n",ans);
	}
}P2;

bool pppp2;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
//	printf("%.2lf\n",(&pppp2-&pppp1)/1024.0/1024);
	int cas;
	scanf("%d",&cas);
	while(cas--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",&A[i]);
//		P1.solve();
		P2.solve();
	}
	return 0;
}
/*
2
4
3 19 10 6
5
11 29 13 19 17
*/
