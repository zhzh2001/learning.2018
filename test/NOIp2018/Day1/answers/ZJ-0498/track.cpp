#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
int n,m,ans1,Ecnt,ans2;
int a[50005],MAX[50005];
bool vis[50005];
struct Edge{int nxt,to,val;}E[100005];int head[50005];
void add(int x,int y,int z){
	E[++Ecnt].to=y;
	E[Ecnt].nxt=head[x];
	E[Ecnt].val=z;
	head[x]=Ecnt;
}
void dfs1(int pre,int u){
	MAX[u]=0;
	for (int i=head[u];i;i=E[i].nxt){
		if (E[i].to==pre) continue;
		dfs1(u,E[i].to);
		MAX[u]=max(MAX[u],MAX[E[i].to]+E[i].val);
	}
	int cnt=0;
	for (int i=head[u];i;i=E[i].nxt){
		if (E[i].to==pre) continue;
		a[++cnt]=MAX[E[i].to]+E[i].val;
	}
	if (cnt>0){
		if (cnt==1) ans1=max(ans1,a[1]);
		 else{
		 	sort(a+1,a+1+cnt);
		 	ans1=max(ans1,a[cnt-1]+a[cnt]);
		 }
	}
}
void do1(){
	ans1=0;
	dfs1(0,1);
	printf("%d\n",ans1);
}
void do3(){
	int cnt=0;
	for (int i=head[1];i;i=E[i].nxt) a[++cnt]=E[i].val;
	sort(a+1,a+1+cnt);
	int L=1,R=a[cnt]+a[cnt-1];
	while (L<R){
		int mid=(L+R+1)>>1;
		int tmp=0,i=1,j=cnt;
		bool ok=0;
		while (j){
			if (tmp==m){ok=1;break;}
			if (a[j]>=mid){j--,tmp++;continue;}
			while (i<j && a[i]+a[j]<mid) i++;
			if (i>=j){ok=0;break;}
			if (a[i]+a[j]>=mid){i++,j--,tmp++;continue;}
			 else break;
		}
		if (ok) L=mid; else R=mid-1;
	}
	printf("%d\n",L);
}
void do4(){
	int cnt=0,R=0;
	for (int i=1;i<n;i++)
		for (int j=head[i];j;j=E[j].nxt)
			if (E[j].to==i+1) a[++cnt]=E[j].val,R+=E[j].val;
	int L=1;
	while (L<R){
		int mid=(L+R+1)>>1;
		int tmp=0,tt=0;
		for (int i=1;i<=cnt;i++){
			tt+=a[i];
			if (tt>=mid){tt=0;tmp++;continue;}
		}
		if (tmp>=m) L=mid; else R=mid-1;
	}
	printf("%d\n",L);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int x,y,z,MMM=100000;
	bool ai1=1,biai1=1;
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		if (x>y) swap(x,y);
		add(x,y,z),add(y,x,z);
		if (x>1) ai1=0;
		if (y!=x+1) biai1=0;
		MMM=min(MMM,z);
	}
	if (m==1) do1(); else
	if (m==n-1) printf("%d\n",MMM); else
	if (ai1) do3(); else
	if (biai1) do4();
	return 0;
}
