#include<cstring>
#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;
int n,a[100005];ll ans;
int f[100005][25];
int getmin(int l,int r){
	int k=(double)log((double)r-l+1)/(double)log(2.0);
	if (a[f[l][k]]>a[f[r-(1<<k)+1][k]])
		return f[r-(1<<k)+1][k];
	return f[l][k];
}
void dfs(int L,int R,int d){
	if (L>R) return;
	int MIN=getmin(L,R);
	ans+=(ll)(a[MIN]-d);
	dfs(L,MIN-1,a[MIN]);
	dfs(MIN+1,R,a[MIN]);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%d",&a[i]);
		f[i][0]=i;
	}
	for (int j=1;j<=17;j++)
		for (int i=1;i<=n;i++){
			if (i+(1<<j)-1>n) break;
			if (a[f[i][j-1]]<a[f[i+(1<<(j-1))][j-1]])
				f[i][j]=f[i][j-1];
			 else f[i][j]=f[i+(1<<(j-1))][j-1];
		}
	ans=0LL;
	dfs(1,n,0);
	printf("%lld\n",ans);
	return 0;
}
