#include<iostream>
#include<cstdlib>
#include<algorithm>
#include<cstdio>
#include<vector>
#include<cstring>
using namespace std;
int cas,n,a[1009];
bool f[3000009];
int main()
{
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  scanf("%d",&cas);
  while(cas--)
  {
  	scanf("%d",&n);
  	int sum=0,ans=0;
  	for (int i=1;i<=n;i++) scanf("%d",&a[i]),sum+=a[i];
  	sort(a+1,a+1+n);
  	for (int i=1;i<=sum;i++) f[i]=false;
  	f[0]=true;
  	for (int i=1;i<=n;i++)
  	if (!f[a[i]]) 
  	{
  	  for (int j=a[i];j<=sum;j++) f[j]=f[j]|f[j-a[i]];
  	  ans++;
	}
	printf("%d\n",ans);
  }
  return 0;
}
