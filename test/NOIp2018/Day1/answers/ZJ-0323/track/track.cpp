#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<vector>
using namespace std;
int n,m,siz=0,head[1000009],b[1000009],e[1000009];
int f[1000009],g[1000009],ndep,qaq;
struct ding{
  int to,nex,w;
}edge[1000009];
struct ding2{
  int p1,p2;
};
void add(int u,int v,int val)
{
  edge[++siz]=(ding){v,head[u],val};head[u]=siz;
}
ding2 work(int len,int k)
{
  int l=1,r=len,nowmax=0,ans=0;
  for (int i=1;i<=len;i++) b[i]=e[i];
  while (l<r)
  {
  	if (b[l]+b[r]>=k) b[l]=b[r]=0,ans++,l++,r--;
  	else l++;
  }
  for (int i=1;i<=len;i++) nowmax=max(nowmax,b[i]);
  return ((ding2){ans,nowmax});
}
void dfs(int x,int fa,int k)
{
  int cnt=0;
  for (int i=head[x];i;i=edge[i].nex)
  {
  	int to=edge[i].to;
  	if (to==fa) continue;
  	dfs(to,x,k); f[x]+=f[to];
  }
  for (int i=head[x];i;i=edge[i].nex)
  {
  	int to=edge[i].to;
  	if (to==fa) continue;
  	if (g[to]+edge[i].w<k)e[++cnt]=g[to]+edge[i].w;
  	else f[x]++;
  }
  int l=1,r=cnt,tmp=0;
  sort(e+1,e+1+cnt);
  //if (k==30) cout<<x<<" "<<to<<" "<<e[1]<<" "<<e[i].w<<endl;
  //if ((k==26237)&&(x==2)) 
  //{
  //}
  ding2 now=work(cnt,k); tmp=now.p1; g[x]=now.p2;f[x]+=tmp;
  while (l<=r)
  {
    int mid=(l+r)>>1;
    now=work(mid,k);
	if (now.p1<tmp) l=mid+1;
	else g[x]=max(now.p2,g[x]),r=mid-1;
  } 
}
bool check(int len)
{
  if (n<=200)
  {
  	for (int i=1;i<=n;i++)
  	{
  	  for (int j=1;j<=n;j++) f[j]=0;
	  dfs(i,i,len);
	  if (f[i]>=m) return true;	
	}
  }
  else
  {
    for (int i=1;i<=n;i++) f[i]=0,g[i]=0;
    dfs(qaq,qaq,len);
    if (f[qaq]>=m) return true;
  }
  return false;
}
void dfs2(int x,int fa,int dep)
{
  if (dep>ndep) ndep=dep,qaq=x; 
  for (int i=head[x];i;i=edge[i].nex)
  {
  	int to=edge[i].to;
  	if (to==fa) continue;
  	dfs2(to,x,dep+edge[i].w);
  }
}
int main()
{
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  scanf("%d%d",&n,&m);
  int x,y,z,lef=2000000000,righ=0,ans=0;
  for (int i=1;i<n;i++)
  {
  	scanf("%d%d%d",&x,&y,&z);
  	lef=min(lef,z);righ+=z;
  	add(x,y,z);add(y,x,z);
  }
  qaq=1;ndep=0;
  dfs2(1,1,0);
  while (lef<=righ)
  {
  	int mid=(lef+righ)>>1;
  	if (check(mid)) ans=mid,lef=mid+1;
  	else righ=mid-1;
  }
  printf("%d\n",ans);
  return 0;
}
