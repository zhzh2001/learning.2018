#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<vector>
using namespace std;
int n,a[200009],lef[200009],righ[200009],q[200009];
int main()
{
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
  scanf("%d",&n);
  for (int i=1;i<=n;i++) scanf("%d",&a[i]);
  int t=0;
  for (int i=1;i<=n;i++)
  {
  	while ((t!=0)&&(a[q[t]]>a[i])) t--;
  	if (t!=0) lef[i]=q[t]; else lef[i]=0;
  	q[++t]=i;
  }
  t=0;
  for (int i=n;i>=1;i--)
  {
  	while ((t!=0)&&(a[q[t]]>=a[i])) t--;
  	if (t!=0) righ[i]=q[t];else righ[i]=n+1;
  	q[++t]=i;
  }
  int ans=0;
  for (int i=1;i<=n;i++)
  {
  	if (a[i]==0) continue;
  	ans+=max(a[i]-max(a[lef[i]],a[righ[i]]),0);
  	//cout<<lef[i]<<" "<<righ[i]<<endl;
  }
  printf("%d\n",ans);
  return 0;
}
