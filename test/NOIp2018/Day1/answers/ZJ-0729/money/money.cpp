#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,u) for (register int i=first[u];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0,f=1;char c=getchar();
	while ((c<'0'||c>'9')&&(c!='-')) c=getchar();
	if (c=='-'){f=-1;c=getchar();}
	while (c>='0'&&c<='9') x=x*10+c-'0',c=getchar();
	return x*f;
}
const int N = 110;
int T,n,a[N];
const int maxn = 25010;
int ans,dp[maxn];
inline void solve(){
	sort(a+1,a+1+n),ans=0;
	dp[0]=1;
	For(i,1,n){
		if (dp[a[i]]) continue;
		For(j,0,a[n]-a[i]) if (dp[j]) dp[j+a[i]]=1;
		ans++;
	}
	printf("%d\n",ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while (T--){
		n=read();
		For(i,1,n) a[i]=read();
		solve();
		For(i,0,a[n]) dp[i]=0;
	}
	return 0;
}
/*
2 
4
3 19 10 6
5
11 29 13 19 17
*/
