#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,u) for (register int i=first[u];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0,f=1;char c=getchar();
	while ((c<'0'||c>'9')&&(c!='-')) c=getchar();
	if (c=='-'){f=-1;c=getchar();}
	while (c>='0'&&c<='9') x=x*10+c-'0',c=getchar();
	return x*f;
}
const int N = 5e4+10;
int n,m,x,y,z,l,r,mid,ans;
int tot,first[N],to[N<<1],val[N<<1],last[N<<1];
inline void Add(int x,int y,int z){to[++tot]=y,val[tot]=z,last[tot]=first[x],first[x]=tot;}
namespace Subtask1{
	int ans,dp[N];
	inline void dfs(int u,int fa){
		int ma=0,Ma=0;
		cross(i,u){
			int v=to[i];
			if (v==fa) continue;
			dfs(v,u),dp[u]=max(dp[u],dp[v]+val[i]);
			if (dp[v]+val[i]>ma) Ma=ma,ma=dp[v]+val[i];
			else if (dp[v]+val[i]>Ma) Ma=dp[v]+val[i];
		}
		ans=max(ans,Ma+ma);
	}
	inline void Main(){
		dfs(1,0),printf("%d\n",ans);
	}
}
namespace Subtask2{
	int cnt,a[N];
	inline void dfs(int u,int fa){
		cross(i,u){
			int v=to[i];
			if (v==fa) continue;
			a[++cnt]=val[i],dfs(v,u);
		}
	}
	inline int calc(int x){
		int sum=0,ret=0;
		For(i,1,n){
			sum+=a[i];
			if (sum>=x) sum=0,++ret;
		}
		return ret;
	}
	inline void solve(){
		while (l<=r){
			mid=l+r>>1;
			if (calc(mid)>=m) l=mid+1,ans=mid;
				else r=mid-1;
		}
		printf("%d\n",ans);
	}
	inline void Main(){
		dfs(1,0),solve();
	}
}
namespace Subtask3{
	int cnt,a[N];
	inline void dfs(int u,int fa){
		cross(i,u){
			int v=to[i];
			if (v==fa) continue;
			a[++cnt]=val[i],dfs(v,u);
		}
	}
	inline int calc(int x){
		int l=1,ret=0,Cnt=cnt;
		while (a[Cnt]>=x&&Cnt) Cnt--,ret++;
		Dow(i,Cnt,1){
			if (l>=i) break;
			while (a[i]+a[l]<x&&l<i) l++;
			if (l==i) break;ret++,l++;
		}
		return ret;
	}
	inline void solve(){
		while (l<=r){
			mid=l+r>>1;
			if (calc(mid)>=m) l=mid+1,ans=mid;
				else r=mid-1;
		}
		printf("%d\n",ans);
	}
	inline void Main(){
		dfs(1,0);
		sort(a+1,a+1+cnt);
		solve();
	}
}
int ret,dp[N];
inline void dfs(int u,int fa,int x){
	vector<int>q;
	multiset<int>s;
	multiset<int>::iterator it;
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		dfs(v,u,x);
		if (dp[v]+val[i]>=x) ++ret;
			else q.push_back(dp[v]+val[i]),s.insert(dp[v]+val[i]);
	}
	if (q.empty()) return;
	sort(q.begin(),q.end());
	int siz=q.size(),sum=siz;
	For(i,0,siz-1){
		if (sum<=1) break;it=s.end();
		if (s.find(q[i])==it) continue;
		if (q[i]+(*(--it))<x){dp[u]=max(dp[u],q[i]);s.erase(s.find(q[i])),sum--;continue;}
		s.erase(s.find(q[i])),sum--;
		it=s.upper_bound(x-q[i]-1);
		s.erase(it),sum--,ret++;
	}
	if (sum) dp[u]=max(dp[u],*(--s.end()));
}
inline bool check(int x){
	ret=0,memset(dp,0,sizeof dp);
	dfs(1,0,x);
//	For(i,1,n) printf("%d ",dp[i]);printf("%d\n",x);
	return ret>=m;	
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();bool flag=1,Flag=1;
	For(i,1,n-1){
		x=read(),y=read(),z=read(),Add(x,y,z),Add(y,x,z),r+=z;
		if (y!=x+1) flag=0;
		if (x!=1) Flag=0;
	}
	if (m==1) return Subtask1::Main(),0;
	if (flag) return Subtask2::Main(),0;
	if (Flag) return Subtask3::Main(),0;
	while (l<=r){
		mid=l+r>>1;
		if (check(mid)) l=mid+1,ans=mid;
			else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
