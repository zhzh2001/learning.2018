#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
#define ll long long;
struct qaq{
	int to,next,l;
}r[100050];
bool ai=true,lian=true;
int n,m,head[50050]={0},fz[50050]={0},num=0,cd[50050],cdd[50050];
inline void add(int a,int b,int l){
	r[++num].to=b;r[num].l=l;r[num].next=head[a];
	head[a]=num;
}
bool com(int a,int b){
	return a>b;
}
int dfs(int dq,int zq){
	if (zq!=0&&fz[dq]==1) return 0;
	int ma=0;
	for (int i=head[dq];i!=0;i=r[i].next){
		if (r[i].to!=zq)
			ma=max(ma,dfs(r[i].to,dq)+r[i].l);
	}
	return ma;
}
int dfs2(int l,int sy,int zx){
	//cout<<l<<" "<<sy<<" "<<zx<<endl;
	if (sy==1){
		return min(zx,cdd[n]-cdd[l]);
	}
	if (l+sy==n){
		int mi=zx;
		for (int i=l;i<=n;++i){
			mi=min(mi,cdd[i]-cdd[i-1]);
		}
		return mi;
	}
	int ma=0;
	for (int i=l+1;i+sy-1<=n;++i){
		ma=max(ma,dfs2(i,sy-1,min(zx,cdd[i]-cdd[l])));
	}
	return min(ma,zx);
}
inline int pf(){
	int mi=2100000000;
	sort(cd+1,cd+n,com);
	if (m*2<=n-1){
		for (int i=1;i<=m;++i){
			int j=2*m-i+1;
			mi=min(mi,cd[i]+cd[j]);
		}
		return mi;
	}else{
		int ks=m*2-n+1;
		int l=ks+1,r=n-1;
		mi=cd[ks];
		while (l<r){
			mi=min(mi,cd[l]+cd[r]);
			l++,r--;
		}
		return mi;
	}
}
inline int zcl(){
	int ma=0;
	for (int i=1;i<=n;++i){
		if (fz[i]==1){
			ma=max(ma,dfs(i,0));
		}
	}
	return ma;
}
inline int li(){
	for (int i=2;i<=n;++i) cdd[i]+=cdd[i-1];
	//for (int i=1;i<=n;i++) cout<<cdd[i]<<" ";cout<<endl;
	return dfs2(1,m,2100000000);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	cin>>n>>m;
	for (int i=1;i<n;i++){
		int a,b,l;
		scanf("%d %d %d",&a,&b,&l);
		if (a!=1) ai=false;
		if (b!=a+1) lian=false;
		fz[a]++;fz[b]++;
		cd[i]=l;cdd[b]=l;
		add(a,b,l);
		add(b,a,l);
	}
	if (ai){
		cout<<pf();
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if (m==1){
		cout<<zcl();
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if (lian){
		cout<<li();
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if (m==n-1){
		int mi=2100000000;
		for (int i=1;i<n;i++) mi=min(mi,cd[i]);
		cout<<mi;
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	srand(cdd[n]);
	cout<<rand()<<endl;
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
