#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
#define ll long long;
int t,n,a[150],ans;
bool b[150];
bool check(int dq,int s){
	for (int i=dq;i>=1;i--){
		if (!b[i]||a[i]>s) continue; 
		if (s%a[i]==0) return true;
		for (int j=s/a[i];j>=1;j--){
			if (check(min(dq,i-1),s-j*a[i])) return true;
		}
	}
	return false;
} 
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	cin>>t;
	while (t--){
		scanf("%d",&n); ans=n;
		for (int i=1;i<=n;i++) b[i]=true;
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		if (a[1]==1){
			cout<<1<<endl;
			continue;
		}
		for (int i=2;i<=n;i++){
			if (check(i-1,a[i])){
				ans--;
				b[i]=false;
			} 
		}
		cout<<ans<<endl;
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
