#include<cstdio>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
using namespace std;

#define LL long long
#define MAXN  100005

LL n;
LL a[MAXN],ans;

void rd()
{
	scanf("%lld",&n);
	for(int i = 1; i <= n; i ++)
		scanf("%lld",&a[i]);
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	rd();
	for(int i = 1; i <= n; i ++)
		if(a[i] > a[i-1])
			ans += a[i]-a[i-1];
	cout<<ans;
	return 0;
}
/*
6
4 3 2 5 3 5
*/

