#include<bits/stdc++.h>
using namespace std;

#define MAXN 100005
#define INF 0x3f3f3f3f

struct aa
{
	int x,y,l,ls;
}b[MAXN];
int t[MAXN],cnt;
int n,m;
bool ra = 1,rb = 1;
int a[MAXN];

void jb(int x,int y,int l)
{
	cnt ++;
	b[cnt].x = x;
	b[cnt].y = y;
	b[cnt].l = l;
	b[cnt].ls = t[x];
	t[x] = cnt;
}

void rd()
{
	scanf("%d%d",&n,&m);
	for(int i = 1; i < n; i ++)
	{
		int x,y,l;
		scanf("%d%d%d",&x,&y,&l);
		if(x + 1 != y) ra = 0;
		if(x != 1) rb = 0;
		jb(x,y,l);
		jb(y,x,l);
	}
}

bool qRa_judge(int x)
{
	int r = 0;
	int s = 0;
	for(int i = 1; i < n; i ++)
	{
		s += b[t[i]].l;
		if(s >= x)
		{
			r ++;
			s = 0;
		}
	}
	return  r >= m;
}

void qRa()
{
	int l = 1,r = INF;
	while(l + 3 < r)
	{
		int mid = (l+r)>>1;
		if(qRa_judge(mid)) l = mid;
		else r = mid;
	}
	for(int i = r; i >= l; i --)
	if(qRa_judge(i)) {
		cout<<i;
		return;
	}
}

bool qRb_judge(int x) {
	int l = 1,r = n-1,rp = 0;
	while(l < r) {
		if(a[r] >= x) 
		{
			rp ++;
			r --;
		}
		else
		if(a[r] + a[l] >= x)
		{
			rp ++;
			r --;
			l ++;
		}
		else
			l ++;
	}
	if(l == r && a[r] > x) rp ++;
	return rp >= m;
}

void qRb() {
	for(int i = t[1]; i != 0; i = b[i].ls) {
		a[b[i].y-1] = b[i].l;
	}
	sort(a+1,a+n);
	int l = 1,r = INF;
	while(l + 3 < r)
	{
		int mid = (l+r)>>1;
		if(qRb_judge(mid)) l = mid;
		else r = mid;
	}
	
	for(int i = r; i >= l; i --)
	if(qRb_judge(i)) {
		cout<<i;
		return;
	}	
}

int d[MAXN];
void dfs(int x,int l)
{
//	cout<<x<<" "<<l<<"\n";
	d[x] = l;
	for(int i = t[x]; i != 0; i = b[i].ls)
	{
		int y = b[i].y;
		if(d[y] > l + b[i].l)
		dfs(y,l + b[i].l);
	}
}

void qRc() {
	memset(d,0x3f,sizeof(d));	
	dfs(1,0);
	int u,l = 0;
	//for(int i = 1; i <= n; i ++)
	//	cout<<d[i]<<"\n";
	for(int i = 1; i <= n; i ++)
		if(d[i] > l) {
			l = d[i];
			u = i;
		}
	memset(d,0x3f,sizeof(d));
	dfs(u,0);
	l = 0;
	for(int i = 1; i <= n; i ++)
		if(d[i] > l) {
			l = d[i];
		}
	cout<<l;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	rd();
	if(ra)
	{
		qRa();
		return 0;
	}
	if(rb)
	{
		qRb();
		return 0;
	}
	if(m == 1)
	{
		qRc();
		return 0;
	}
	return 0;
}
/*
7 3
1 2 6
2 3 3
3 4 4
4 5 2
5 6 3
6 7 2
*/
