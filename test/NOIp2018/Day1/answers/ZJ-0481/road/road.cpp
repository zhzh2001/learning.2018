#include<iostream>
#include<cstdio>
#include<cstdlib>
using namespace std;
int d[100010];
int n;
long long ans;
inline int read() {
	int x=0;
	char c=getchar();
	while(c<'0') c=getchar();
	while(c>='0') x=x*10+(c^'0'),c=getchar();
	return x;
}
void s(int l,int r) {
	if(d[l]==0)
		l++;
	if(d[r]==0)
		r--;
	if(l==r) {
		ans+=d[l];
		return ;
	}
	if(r-l==1) {
		ans+=max(d[l],d[r]);
		return ;
	}
	if(l>r) return ;
	int mi=100010,k=l;
	for(int i=l; i<=r; i++) {
		if(d[i]<mi) {
			mi=d[i];
		}
	}
	for(int i=l; i<=r; i++) {
		d[i]-=mi;
		if(d[i]==0&&k<=i-1) {
			s(k,i-1);
			k=i+1;
		}
	}
	if(k<=r)
		s(k,r);
	ans+=mi;
}
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1; i<=n; i++)
		d[i]=read();
	s(1,n);
	printf("%lld\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
