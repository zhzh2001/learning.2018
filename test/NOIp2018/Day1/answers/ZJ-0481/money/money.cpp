#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cstdlib>
using namespace std;
int t,n,sum,l,r;
int mo[110],us[110],ans,q[110],dp[25010],ma;
inline int read() {
	int x=0;
	char c=getchar();
	while(c<'0') c=getchar();
	while(c>='0') x=x*10+(c^'0'),c=getchar();
	return x;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read();
	while(t--)
	{
	 n=read();
	 ans=0;ma=0;
	 memset(mo,0,sizeof(mo));
	 memset(us,0,sizeof(us));
	 memset(q,0,sizeof(q));
	 memset(dp,0,sizeof(dp));
	 for(int i=1;i<=n;i++)
	  mo[i]=read(),ma=max(ma,mo[i]);
	 sort(mo+1,mo+n+1);
	 for(int i=2;i<=n;i++)
	 {
	  for(int j=1;j<i;j++)
	  {
	   if(mo[i]%mo[j]==0)
	     us[i]=1;
	  }
     }
     for(int i=1;i<=n;i++)
      ans+=1-us[i];
     if(ans==1)
	 {
	   puts("1");
	   continue;	
	 } 
     for(int i=1;i<=n;i++)
     {
      if(us[i]==0)
	  {
	    q[1]=i;
		break;	
	  }	
	 }
	 for(int i=q[1]+1;i<=n;i++)
     {
      if(us[i]==0)
	  {
	    q[2]=i;
		break;	
	  }	
	 }
     ans=2;
     dp[0]=1;
     for(int i=1;i<=ans;i++)
     {
      l=mo[q[i]];	
      for(int j=l;j<=ma;j++)
         dp[j]=max(dp[j],dp[j-l]);	
	 }
     for(int i=q[2]+1;i<=n;i++)
	 {
	  if(!us[i]&&dp[mo[i]]==0)
	  {
	    ans++;
		q[ans]=i;
		l=mo[i];
		for(int j=l;j<=25000;j++)
		 dp[j]=max(dp[j],dp[j-l]);	
	  }	
	 }    
     printf("%d\n",ans); 
	}
	fclose(stdin);
	fclose(stdout); 
	return 0;
}
