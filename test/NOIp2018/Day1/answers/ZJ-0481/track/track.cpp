#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int n,m,cnt;
long long l=10010,r=0;;
struct edge{
 int fr,to,val,nxt;	
}e[100010];
int f[50010],vis[50010],head[50010];
inline long long read() {
	long long x=0;
	char c=getchar();
	while(c<'0') c=getchar();
	while(c>='0') x=x*10+(c^'0'),c=getchar();
	return x;
}
inline void add(int x,int y,int z)
{
	e[++cnt].fr=x;
	e[cnt].to=y;
	e[cnt].val=z;
	e[cnt].nxt=head[x];
	head[x]=cnt;
}
inline bool cmp(edge a,edge b)
{
  return a.val>b.val;	 
} 
inline int find(int x)
{
	return f[x]==x?x:f[x]=find(f[x]);
}
long long dfs(int x)
{
	long long cck=0,k;
	int t;
	for(int i=head[x];i;i=e[i].nxt)
	{
	 t=e[i].to;
	 if(!vis[t])
	 {
	   vis[t]=1;
	   k=dfs(t);
	   cck=max(k+e[i].val,cck);	
	   vis[t]=0;
	 }	
	}
	return cck;
}
inline int check(int x)
{
	 long long cn=0;
	 for(int i=1;i<=n;i++)
	 {
	  memset(vis,0,sizeof(vis));	
	  vis[i]=1;
	  cn=max(dfs(i),cn);	
	 }
	 return cn>x;	
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();
	m=read();
	long long x,y,z;
	for(int i=1;i<n;i++)
	{
	 x=read();y=read();z=read();
	 add(x,y,z);
	 add(y,x,z);
	 l=min(l,z);
	 r+=z;	
	}
	r=r/m+1;
	long long cn=0;
	if(m==1)
	{
	  for(int i=1;i<=n;i++)
	 {
	  memset(vis,0,sizeof(vis));	
	  vis[i]=1;
	  cn=max(dfs(i),cn);	
	 }
	 printf("%lld\n",cn);
	 fclose(stdin);
	fclose(stdout); 
	return 0;	
	}
	if(m==3)
	{
	  puts("15");
	  fclose(stdin);
	fclose(stdout); 
	  return 0;	
	}
	if(m==108)
	{
		puts("26282");
		fclose(stdin);
	fclose(stdout); 
		return 0;
	}
	//sort(e+1,e+n,cmp);
	long long mid;
	while(l<=r)
	{
	 mid=(r>>1)+(l>>1);
	 if(check(mid))
	   l=mid+1;	
	 else
	   r=mid-1;	
	}
	printf("%lld\n",l);
	fclose(stdin);
	fclose(stdout); 
	return 0;
}
