#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define INF 1e9
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

int q[5000005];
int vet[100005],val[100005],Next[100005],head1[500005];
int u[50005],v[50005],cost[50005];
int N,M,num;
int dis[50005],flag1[50005];

void add(int u1,int v1,int cost1)
{
	vet[++num]=v1;
	val[num]=cost1;
	Next[num]=head1[u1];
	head1[u1]=num;
}

void SPFA(int S)
{
	fo(i,1,N)
	{
		dis[i]=INF;
		flag1[i]=0;
	}
	dis[S]=0;
	flag1[S]=1;
	int t=0; int w=0;
	q[t]=S;
	while (t <= w)
	{
		int u1=q[t];
		t++;
		for (int e=head1[u1]; e != -1; e=Next[e])
		{
			int V=vet[e];
			int Cost=val[e];
			if (dis[V] > dis[u1] + Cost)
			{
				dis[V]=dis[u1] + Cost;
				if (! flag1[V])
				{
					flag1[V]=1;
					q[++w]=V;
				}
			}
		}
		flag1[u1]=0;
	}
}

bool check(int X)
{
	int tmp1=0; int tmp2=1;
	bool flagX=0;
	fo(i,1,N)
	{
		if (tmp1 + cost[i] < X)
		{
			tmp1=tmp1 + cost[i];
		}
		else 
		if (tmp1 + cost[i] == X)
		{
			flagX=1;
			tmp1=tmp1 + cost[i];
		}
		else
		{
			tmp2++;
			tmp1=cost[i];
		}
	}
	return (tmp2 >= M) && flagX;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&N,&M);
	bool flagX=1; bool flagY=1;
	fo(i,1,N - 1)
	{
		scanf("%d%d%d",&u[i],&v[i],&cost[i]);
		if (v[i] - u[i] != 1) flagX=0;
		if (cost[i] != 1) flagY=0;
	}
	if (M == 1)
	{
		cl(head1,-1);
		fo(i,1,N - 1)
		{
			add(u[i],v[i],cost[i]);
			add(v[i],u[i],cost[i]);
		}
		int Ans=0;
		fo(i,1,N)
		{
			SPFA(i);
			fo(j,1,N)
			{
				Ans=max(Ans,dis[j]);
			}
		}
		printf("%d\n",Ans);
	}
	else
	{
		if (flagX)
		{
			int tmp=0;
			fo(i,1,N)
			{
				tmp+=cost[i];
			}
			int L1=0; int R1=tmp;
			while (L1 < R1)
			{
				int mid=(L1 + R1 + 1) >> 1;
				if (check(mid)) L1=mid;
				else R1=mid - 1;
			}
			printf("%d\n",L1);
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
 } 
