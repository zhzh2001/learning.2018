#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

int flag1[250005];
bool flag2[250005];
int a[1000005];
int N,N1;

void dfs(int t,int Sum)
{
	if (flag2[Sum] > 1)
	{
		return;
	}
	if (t > N1)
	{
		flag1[Sum]++;
		return;
	}
	fo(i,0,(a[N1] - Sum) / a[t])
	{
		dfs(t + 1,Sum + i * a[t]);
	}
}

int find1(int l,int r,int Key)
{
	while (l < r)
	{
		int mid=(l + r) >> 1;
		if (a[mid] < Key)
		{
			l=mid + 1;
		}
		else
		{
			r=mid;
		}
	}
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int _;
	scanf("%d",&_);
	while (_--)
	{
		scanf("%d",&N);
		fo(i,1,N)
		{
			scanf("%d",&a[i]);
		}
		sort(a + 1,a + N + 1);
		cl(flag1,0);
		fo(i,1,N)
		{
			fo(j,1,i - 1)
			{
				if (a[i] % a[j] == 0)
				{
					flag1[i]=1;
				}
			}
		}
		N1=0;
		fo(i,1,N)
		{
			if (! flag1[i])
			{
				a[++N1]=a[i];
			}
		}
		cl(flag2,0);
		cl(flag1,0);
		dfs(1,0);
		fo(i,1,a[N1])
		{
			if (flag1[i] > 1)
			{
				int X=find1(1,N1,i);
				if (a[X] == i)
				{
					flag2[X]=1;
				}
			}
		}
		int tmp=0;
		fo(i,1,N1)
		{
			if (! flag2[i])
			{
				tmp++;
			}
		}
		printf("%d\n",tmp);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
