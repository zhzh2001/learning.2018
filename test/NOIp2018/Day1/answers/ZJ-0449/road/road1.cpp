#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define INF 1e9
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

int N,N1;
int tmp;
int a[1000005],b[1000005];

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.in","w",stdout);
	printf("9000\n");
	fo(i,1,9000)
	{
		printf("%d ",i);
	}
	fclose(stdout);
	freopen("road.out","w",stdout);
	scanf("%d",&N);
	fo(i,1,N)
	{
		scanf("%d",&a[i]);
		tmp+=a[i];
	}
	N1=0;
	fo(i,1,N)
	{
		if (a[i] != a[i - 1])
		{
			b[++N1]=a[i];
		}
	}
	b[N1 + 1]=0;
	int Ans=0; int Now=0;
	while (tmp > 0)
	{
		int L=0; int Min1=INF;
		fo(i,1,N1 + 1)
		{
			if (b[i])
			{
				Min1=min(Min1,b[i]);
			}
			else
			{
				tmp-=(i - L - 1) * Min1;
				fo(j,L + 1,i - 1)
				{
					b[j]-=Min1;
				}
				if ((i - L - 1 > 0)) Ans+=Min1;
				L=i; Min1=INF;
			}
		}
	}
	printf("%d\n",Ans);
	return 0;
}
