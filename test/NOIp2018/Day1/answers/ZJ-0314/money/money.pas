var
  t,n,i,j,k,tt,max,ans:longint;
  a:array[0..200] of longint;
  t1,t2:array[0..30000] of boolean;
begin
  assign(input,'money.in');reset(input);
  assign(output,'money.out');rewrite(output);
  readln(t);
  for i:=1 to t do begin
    readln(n);
    fillchar(a,sizeof(a),0);
    for j:=1 to n do
      read(a[j]);
    readln;
    for j:=1 to n-1 do
      for k:=n downto j+1 do
        if a[k]<a[k-1] then begin
          tt:=a[k];
          a[k]:=a[k-1];
          a[k-1]:=tt;
        end;
    max:=a[n];
    fillchar(t1,sizeof(t1),false);
    t1[0]:=true;
    for k:=1 to n do begin
      for j:=0 to max-a[k] do
        if (t1[j]) and (not t1[j+a[k]]) then t1[j+a[k]]:=true;
    end;
    fillchar(t2,sizeof(t2),false);
    t2[0]:=true;
    ans:=0;
    for j:=1 to max do begin
      if not t1[j] then continue;
      if t2[j] then continue;
      ans:=ans+1;
      for k:=0 to max-j do
        if (t2[k]) and (not t2[k+j]) then t2[k+j]:=true;
    end;
    writeln(ans);
  end;
  close(input);close(output);
end.