var
  n,m,i,j,x,y,z,min,max,l,r,q,cnt,o:longint;
  other,w,head,next,que,f,c,a:array[0..500000] of longint;
  b:array[0..500000] of boolean;
  t:boolean;
procedure add(x,y,z:longint);
begin
  cnt:=cnt+1;
  other[cnt]:=y;
  w[cnt]:=z;
  next[cnt]:=head[x];
  head[x]:=cnt;
end;
procedure qsort(l,r:longint);
var
  i,j,mid,t:longint;
begin
  i:=l;
  j:=r;
  mid:=c[(l+r) div 2];
  repeat
    while c[i]<mid do i:=i+1;
    while mid<c[j] do j:=j-1;
    if i<=j then begin
                   t:=c[i];
                   c[i]:=c[j];
                   c[j]:=t;
                   i:=i+1;
                   j:=j-1;
                 end;
  until i>j;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;
begin
  assign(input,'track.in');reset(input);
  assign(output,'track.out');rewrite(output);
  readln(n,m);
  min:=1000000;
  t:=true;
  for i:=1 to n-1 do begin
    readln(x,y,z);
    if y<>x+1 then t:=false;
    a[x]:=a[x]+1;
    a[y]:=a[y]+1;
    add(x,y,z);
    add(y,x,z);
    if min>z then min:=z;
  end;
  if m=n-1 then begin
                  writeln(min);
                  close(input);close(output);
                  exit;
                end;
  if t then begin
              for i:=1 to n do
                if a[i]=1 then break;
              fillchar(b,sizeof(b),false);
              o:=0;
              x:=head[i];
              while x<>0 do begin
                if not b[x] then begin
                  o:=o+1;
                  c[o]:=w[x];
                  b[x]:=true;
                  if x mod 2=0 then b[x-1]:=true
                               else b[x+1]:=true;
                  i:=other[x];
                  x:=head[i];
                end
                else x:=next[x];
              end;
              qsort(1,o);
              while o>m do begin
                c[1]:=c[1]+c[2];
                c[2]:=maxlongint;
                qsort(1,o);
                o:=o-1;
              end;
              writeln(c[1]);
              close(input);close(output);
              exit;
            end;
  if m=1 then begin
                max:=0;
                for i:=1 to n do begin
                  l:=0;
                  r:=1;
                  fillchar(que,sizeof(que),0);
                  que[1]:=i;
                  fillchar(f,sizeof(f),0);
                  fillchar(b,sizeof(b),false);
                  f[i]:=0;
                  while l<r do begin
                    l:=l+1;
                    q:=que[l];
                    x:=head[q];
                    while x<>0 do begin
                      if (not b[x]) and (f[other[x]]<f[q]+w[x]) then begin
                                                                       b[x]:=true;
                                                                       if x mod 2=0 then b[x-1]:=true
                                                                                    else b[x+1]:=true;
                                                                       f[other[x]]:=f[q]+w[x];
                                                                       r:=r+1;
                                                                       que[r]:=other[x];
                                                                     end;
                      x:=next[x];
                    end;
                  end;
                  for j:=1 to n do
                    if f[j]>max then max:=f[j];
                end;
                writeln(max);
                close(input);close(output);
                exit;
              end;
end.