var
  n,i,ans:longint;
  a:array[0..150000] of longint;
begin
  assign(input,'road.in');reset(input);
  assign(output,'road.out');rewrite(output);
  readln(n);
  ans:=0;
  fillchar(a,sizeof(a),0);
  for i:=1 to n do begin
    read(a[i]);
    if a[i]>a[i-1] then ans:=ans+a[i]-a[i-1];
  end;
  readln;
  writeln(ans);
  close(input);close(output);
end.