#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;

template <class T> inline 
void read(T &x) {
	int f = 1; x = 0;
	char ch = getchar();
	while (ch < '0' || ch > '9') {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;
}

const int N = 25000 + 5;

int n, ans, mn;
int a[N], D[N], d[N], vis[N], can[N];

struct node {
	int x, d;
	node(int _x = 0, int _d = 0) {
		x = _x, d = _d;
	}
	bool operator < (const node &T) const {
		return d > T.d;
	}
};

priority_queue<node> Q;

void solve() {
	for (int i = 0; i < mn; i++) d[i] = INF, vis[i] = 0; d[0] = mn;
	Q.push(node(0, mn));
	while (!Q.empty()) {
		node p = Q.top(); Q.pop();
		int x = p.x;
		if (vis[x]) continue;
		vis[x] = 1;
		for (int i = 1; i <= n; i++) {
			if (!can[i]) continue;
			int y = (x + a[i]) % mn;
			if (d[y] > d[x] + a[i]) {
				d[y] = d[x] + a[i];
				Q.push(node(y, d[y]));
			}
		}
	}
}

void dfs(int x, int cnt) {
	if (cnt >= ans) return;
	if (x == n + 1) {
		solve();
		int flag = 1;
		for (int i = 0; i < mn; i++) if (d[i] != D[i]) flag = 0;
		if (flag) ans = min(ans, cnt);
		return;
	}
	can[x] = 0;
	dfs(x + 1, cnt);
	can[x] = 1;
	dfs(x + 1, cnt + 1);
	can[x] = 0;
}

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T;
	read(T);
	while (T--) {
		read(n);
		mn = INF;
		for (int i = 1; i <= n; i++) read(a[i]), mn = min(mn, a[i]);
		for (int i = 1; i <= n; i++) can[i] = 1;
		solve();
		for (int i = 1; i <= n; i++) can[i] = 0;
		for (int i = 0; i < mn; i++) D[i] = d[i];
		ans = n;
		dfs(1, 0);
		printf("%d\n", min(n, ans + 1));		
	}
	return 0;
}

