#include <bits/stdc++.h>
using namespace std;

template <class T> inline 
void read(T &x) {
	int f = 1; x = 0;
	char ch = getchar();
	while (ch < '0' || ch > '9') {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;
}

const int N = 100000 + 5;

int n;
int a[N];

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	read(n);
	for (int i = 1; i <= n; i++) read(a[i]);
	int ans = 0;
	for (int i = 1; i <= n; i++) if (a[i] > a[i - 1]) ans += a[i] - a[i - 1];
	printf("%d\n", ans);
	return 0;
}
