#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;

template <class T> inline 
void read(T &x) {
	int f = 1; x = 0;
	char ch = getchar();
	while (ch < '0' || ch > '9') {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;
}

const int N = 50000 + 5;

int n, m, L, cnt;
vector <int> v[N], w[N];
int mx[N], c[N], vis[N];

struct node {
	int x, d;
	node(int _x = 0, int _d = 0) {
		x = _x, d = _d;
	}
	bool operator < (const node &T) const {
		return d > T.d;
	}
};

priority_queue<node> Q;

void dfs(int x, int p) {
	for (int i = 0; i < v[x].size(); i++) {
		int y = v[x][i];
		if (y == p) continue;
		dfs(y, x);
	}
	mx[x] = 0; 
	int num = 0;
	for (int i = 0; i < v[x].size(); i++) {
		int y = v[x][i];
		if (y == p) continue;
		c[++num] = mx[y] + w[x][i];
	}
	sort(c + 1, c + num + 1); 
	for (int i = 1; i <= num; i++) vis[i] = 0;
	for (int i = 1; i <= num; i++) if (c[i] >= L) cnt++, vis[i] = 1;
	for (int i = 1, j = num; i <= num; i++) {
		if (vis[i]) continue;
		while (j && c[i] + c[j] >= L) {
			if (!vis[j]) Q.push(node(j, c[j]));
			j--;
		}
		if (!Q.empty()) {
			node p = Q.top(); Q.pop();
			if (i == p.x) {
				if (!Q.empty()) {
					node t = Q.top(); Q.pop();
					cnt++;
					vis[i] = vis[t.x] = 1;
				} else {
					Q.push(node(i, c[i]));
				}
			} else {
				cnt++;
				vis[i] = vis[p.x] = 1;
			}
		}
	}
	while (!Q.empty()) Q.pop();
	for (int i = 1; i <= num; i++) if (!vis[i]) mx[x] = max(mx[x], c[i]);
}

int check(int mid) {
	cnt = 0; L = mid;
	dfs(1, 0);
	return cnt;
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	read(n), read(m);
	for (int i = 1, x, y, z; i < n; i++) {
		read(x), read(y), read(z);
		v[x].push_back(y); w[x].push_back(z);
		v[y].push_back(x); w[y].push_back(z);
	}
	int l = 0, r = INF, ans = 0;
	while (l <= r) {
		int mid = (l + r) >> 1;
		if (check(mid) >= m) {
			ans = mid;
			l = mid + 1;
		} else r = mid - 1;
	}
	printf("%d\n", ans);
	return 0;
}
