#include<cstdio>
const int N=1e5+3;
int n,ans,d[N],a[N*3],x[N*3];
void B(int p,int L,int R)
{
	if(L==R){scanf("%d",d+L),a[p]=d[x[p]=L];return;}
	B(p<<1,L,L+R>>1),B(p<<1|1,L+R+2>>1,R);
	if(a[p<<1]<a[p<<1|1])a[p]=a[p<<1],x[p]=x[p<<1];else
	a[p]=a[p<<1|1],x[p]=x[p<<1|1];
}
int MIN(int p,int L,int R,int l,int r)
{
	if(l==L&&r==R)return x[p];
	int mid=L+R>>1;
	if(l>mid)return MIN(p<<1|1,mid+1,R,l,r);
	if(r<=mid)return MIN(p<<1,L,mid,l,r);
	int lm=MIN(p<<1,L,mid,l,mid),rm=MIN(p<<1|1,mid+1,R,mid+1,r);
	return d[lm]<d[rm]?lm:rm;
}
void sol(int L,int R,int s)
{
	if(L>R)return;
	int k=MIN(1,1,n,L,R);
	ans+=d[k]-s;
	sol(L,k-1,d[k]);
	sol(k+1,R,d[k]);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n),B(1,1,n);
	sol(1,n,0),printf("%d",ans);
}
