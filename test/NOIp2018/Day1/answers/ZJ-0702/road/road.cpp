#include <cstdio>
#include <algorithm>
using namespace std;

const int N = 101000;
long long ans;
int n;
int a[N], b[N][19], lg[N];
int mi[1000];

int query(int l, int r)
{
	int t = lg[r - l + 1];
	if (a[b[l][t]] < a[b[r - mi[t] + 1][t]])
		return b[l][t];
	else
		return b[r - mi[t] + 1][t];
}

void work(int l, int r, int now)
{
	int mid = query(l, r);
	ans += a[mid] - now;
	if (l <= mid - 1)
		work(l, mid - 1, a[mid]);
	if (mid + 1 <= r)
		work(mid + 1, r, a[mid]);
}

int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	mi[0] = 1;
	for (int i = 1; i <= 20; i++)
		mi[i] = mi[i - 1] << 1;
	for (int i = 2; i <= n; i++)
		lg[i] = lg[i >> 1] + 1;
	for (int i = 1; i <= n; i++)
		b[i][0] = i;
	for (int i = 1; i <= 18; i++)
		for (int j = 1; j <= n - mi[i] + 1; j++)
			if (a[b[j][i - 1]] < a[b[j + mi[i - 1]][i - 1]])
				b[j][i] = b[j][i - 1];
			else
				b[j][i] = b[j + mi[i - 1]][i - 1];
	work(1, n, 0);
	printf("%lld\n", ans);
	return 0;
}
