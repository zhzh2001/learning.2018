#include <cstdio>
#include <algorithm>
#include <set>
using namespace std;

const int N = 50500;
struct Edge
{
    int nxt, to, val;
} eg[N << 1];
int head[N], en, n, m, f[N], g[N], mid, vis[N], A[N], cnt;
multiset<int> S;

void set_edge(int u, int v, int w)
{
    eg[++en] = (Edge) {head[u], v, w};
    head[u] = en;
}

void dfs(int u, int fa)
{
    f[u] = g[u] = 0;
    for (int e = head[u]; e; e = eg[e].nxt)
    {
        int v = eg[e].to;
        if (v == fa) continue;
        dfs(v, u);
        f[u] += f[v];
    }
    cnt = 0;
    for (int e = head[u]; e; e = eg[e].nxt)
    {
        int v = eg[e].to;
        if (v == fa) continue;
        A[++cnt] = g[v] + eg[e].val;
    }
    sort(A + 1, A + cnt + 1);
    while (cnt > 0 && A[cnt] >= mid)
        cnt--, f[u]++;
    S.clear();
    for (int i = 1; i <= cnt; i++)
        S.insert(A[i]);
    for (int i = 1; i <= cnt; i++)
    {
        multiset<int> :: iterator j = S.lower_bound(A[i]);
        if ((*j) == A[i])
        {
            S.erase(j);
            j = S.lower_bound(mid - A[i]);
            if (j != S.end())
            {
                S.erase(j);
                f[u]++;
            } else
                g[u] = max(g[u], A[i]);
        }
    }
    if (!S.empty())
    {
        multiset<int> :: iterator j = S.end();
        j--;
        g[u] = max(g[u], (*j));
    }
}

int main()
{
    freopen("track.in", "r", stdin);
    freopen("track.out", "w", stdout);
    scanf("%d%d", &n, &m);
    for (int i = 1; i < n; i++)
    {
        int u, v, w;
        scanf("%d%d%d", &u, &v, &w);
        set_edge(u, v, w);
        set_edge(v, u, w);
    }
    int l = 0, r = 500000000;
    while (l < r)
    {
        mid = l + r + 1 >> 1;
        dfs(1, 0);
        if (f[1] >= m)
            l = mid;
        else
            r = mid - 1;
    }
    printf("%d\n", l);
}
