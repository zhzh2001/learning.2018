#include <cstdio>
#include <algorithm>
using namespace std;

int n, f[30000], a[10000];

int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int cas;
	scanf("%d", &cas);
	while (cas--)
	{
		scanf("%d", &n);
		for (int i = 1; i <= n; i++)
			scanf("%d", &a[i]);
		sort(a + 1, a + n + 1);
		f[0] = 1;
		for (int i = 1; i <= 25000; i++)
			f[i] = 0;
		int ans = 0;
		for (int i = 1; i <= n; i++)
			if (!f[a[i]])
			{
				ans++;
				for (int j = 0; j <= 25000 - a[i]; j++)
					f[j + a[i]] |= f[j];
			}
		printf("%d\n", ans);
	}
}
