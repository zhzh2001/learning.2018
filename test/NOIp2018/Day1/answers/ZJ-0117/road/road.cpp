#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=100006;
inline char nc(){static char buf[100000],*i=buf,*j=buf;return i==j&&(j=(i=buf)+fread(buf,1,100000,stdin),i==j)?EOF:*i++;}
inline int _read(){char ch=nc();int sum=0;while(!(ch>='0'&&ch<='9'))ch=nc();
while(ch>='0'&&ch<='9')sum=sum*10+ch-48,ch=nc();return sum;}
int n,ans,a[maxn];
int main(){
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	n=_read();for(int i=1;i<=n;i++){
		a[i]=_read();if(a[i]>a[i-1])ans+=a[i]-a[i-1];
	}return printf("%d",ans),0;
}
