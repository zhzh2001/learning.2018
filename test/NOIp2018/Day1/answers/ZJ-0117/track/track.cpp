#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=50006;
inline char nc(){static char buf[100000],*i=buf,*j=buf;return i==j&&(j=(i=buf)+fread(buf,1,100000,stdin),i==j)?EOF:*i++;}
inline int _read(){char ch=nc();int sum=0;while(!(ch>='0'&&ch<='9'))ch=nc();
while(ch>='0'&&ch<='9')sum=sum*10+ch-48,ch=nc();return sum;}
int n,m,X,D,L,R,ans,tot,a[maxn],lnk[maxn],son[maxn*2],nxt[maxn*2],w[maxn*2];
bool vis[maxn];
void add(int x,int y,int z){nxt[++tot]=lnk[x];son[tot]=y;lnk[x]=tot;w[tot]=z;}
void dfs(int x,int d,int fa){
	if(d>D)X=x,D=d;a[x]=d;
	for(int j=lnk[x];j;j=nxt[j]) if(son[j]!=fa)dfs(son[j],d+w[j],x);
}
bool work(int x){
	int sum=0;
	for(int i=1,j=1;i<=n;i=j){
		for(;j<=n&&a[j]-a[i]<x;j++);
		if(j<=n&&a[j]-a[i]>=x)sum++;
	}return sum>=m;
}
bool work1(int x){
	int p=lower_bound(a+1,a+1+n,x)-a,sum=n-p+1;
	memset(vis,1,sizeof(vis));
	for(int i=p-1,j=1;i>=2;i--) if(vis[i]){
		while((i==j||a[i]+a[j]<x||!vis[j])&&j<p)j++;
		if(i!=j&&j<p&&a[i]+a[j]>=x&&vis[j])vis[j]=vis[i]=0,sum++;
	}return sum>=m;
}
int main(){
	freopen("track.in","r",stdin);freopen("track.out","w",stdout);
	n=_read();m=_read();bool check=1;
	for(int i=1,x,y,z;i<n;i++){
		x=_read(),y=_read(),z=_read(),add(x,y,z),add(y,x,z);
		if(y!=x+1)check=0;
	}
	if(m==1){
		D=0;X=0;dfs(1,0,0);D=0;dfs(X,0,0);
		ans=D;
	}else if(check){
		dfs(1,0,0);L=0;R=a[n];
		while(L<=R){
			int mid=L+R>>1;
			if(work(mid))L=mid+1;else R=mid-1;
		}ans=R;
	}else{
		dfs(1,0,0);sort(a+1,a+1+n);L=a[2];R=0;for(int i=1;i<=n;i++)R+=a[i];
		while(L<=R){
			int mid=L+R>>1;
			if(work1(mid))L=mid+1;else R=mid-1;
		}ans=R;
	}return printf("%d",ans),0;
}
