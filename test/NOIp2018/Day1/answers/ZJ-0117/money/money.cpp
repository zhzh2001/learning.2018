#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=106,maxs=25000;
inline char nc(){static char buf[100000],*i=buf,*j=buf;return i==j&&(j=(i=buf)+fread(buf,1,100000,stdin),i==j)?EOF:*i++;}
inline int _read(){char ch=nc();int sum=0;while(!(ch>='0'&&ch<='9'))ch=nc();
while(ch>='0'&&ch<='9')sum=sum*10+ch-48,ch=nc();return sum;}
int T,n,ans,a[maxn];
bool f[maxs+6];
int main(){
	freopen("money.in","r",stdin);freopen("money.out","w",stdout);
	T=_read();
	while(T--){
		ans=0;memset(f,0,sizeof(f));f[0]=1;
		n=_read();for(int i=1;i<=n;i++)a[i]=_read();sort(a+1,a+1+n);
		for(int i=1;i<=n;i++) if(!f[a[i]]){
			ans++;
			for(int j=a[i];j<=maxs;j++) if(f[j-a[i]])f[j]=1;
		}printf("%d\n",ans);
	}return 0;
}
