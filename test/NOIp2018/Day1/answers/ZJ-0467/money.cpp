#include<cstdio>
#include<cstring>
#include<algorithm>
int b[25005];
int a[105];
int n;
void work(){
	int ans=0;
	for (int i=1;i<=n;i++){
		int t=a[i];
		if (!b[t]) ans++;
		int p=25000-t;
		b[t]=1;
		for (int j=1;j<=p;j++)
			if (b[j]) b[j+t]=1;
	}
	printf("%d\n",ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		for (int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		std::sort(a+1,a+1+n);
		memset(b,0,sizeof(b));
		work();
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
