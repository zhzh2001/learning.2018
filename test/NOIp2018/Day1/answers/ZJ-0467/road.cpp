#include<stdio.h>
#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))
int n;
int a[100005];
int t[100005];
int ans;
void dfs(){
	int m=0;
	a[0]=a[1]-1;
	for (int i=1;i<=n;i++)
		if (a[i]!=a[i-1]) t[++m]=a[i];
	if (m==1){
		ans+=t[1];
		return;
	}
	n=m;m=1;
	for (int i=2;i<n;i++)
		if ((t[i]>t[i-1])==(t[i]>t[i+1]))
			a[++m]=t[i];
	a[++m]=t[n];
	n=m;
	if (n==1){
		ans+=a[1];
		return;
	}
	if (a[1]>a[2]){
		ans+=a[1]-a[2];
		a[1]=a[2];
	}
	for (int i=2;i<n;i++)
		if (a[i]>a[i-1]) ans+=a[i]-max(a[i+1],a[i-1]),a[i]=max(a[i+1],a[i-1]);
	if (a[n]>a[n-1]) ans+=a[n]-a[n-1],a[n]=a[n-1];
	dfs();
}
void dfs1(int l,int r){
	if (l==r){
		ans+=a[l];
		return;
	}
	if (l>r)
		return;
	int p=1000000;
	for (int i=l;i<=r;i++)
		p=min(p,a[i]);
	for (int i=l;i<=r;i++)
		a[i]-=p;
	ans+=p;
	int be=l;
	for (int i=l;i<=r;i++){
		if (a[i]==0){
			dfs1(be,i-1);
			be=i+1;
		}
	}
	if (a[r]!=0) dfs1(be,r);
	return;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	if (n>1000){
		dfs();
	}else{
		dfs1(1,n);
	}
	printf("%d",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
