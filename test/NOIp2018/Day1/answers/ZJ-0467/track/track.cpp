#include<cstdio>
#include<algorithm>
#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))
typedef long long ll;
int n,m;
struct node{
	int to,next,s,b,QWQ;
}l[100005];
ll sum=0;
int len[50005];
int t2=1,t1=1;
int head[50005],tot;
int read(){
	int x=0;char c=getchar();
	while (c<'0'||c>'9') c=getchar();
	while (c>='0'&&c<='9') x=(x<<1)+(x<<3)+(c^48),c=getchar();
	return x;
}
#define read read()
void add(int x,int y,int s){
	l[++tot].next=head[x];
	l[tot].to=y;
	l[tot].s=s;
	head[x]=tot;
}
int dfs(int x,int h,ll sum,ll s){
	if (s<=sum){
		if (h==m) return 1;
		int k=0;
		for (int i=1;i<=n;i++)
			k|=dfs(i,h+1,0,s);
		return k;
	}
	int k=0;
	for (int i=head[x];i;i=l[i].next){
		if (l[i].b) continue;
		l[i].b=l[l[i].QWQ].b=1;
		k|=dfs(l[i].to,h,sum+l[i].s,s);
		l[i].b=l[l[i].QWQ].b=0;
	}
	return k;
}
int pd(ll s){
	if (!t2){
		return dfs(0,0,s,s);
	}
	int tot=0;
	ll sum=0;
	for (int i=1;i<n;i++){
		sum+=l[head[i]].s;
		if (sum>=s) sum=0,tot++;
	}
	if (tot>=m) return 1;
	return 0;
}
void work(){
	ll l=1,r=(sum)/m;
	ll ans=0;
	while (l<=r){
		ll mid=(l+r)>>1;
		if (pd(mid)){
			l=mid+1;
			ans=mid;
		}else r=mid-1;
	}
	printf("%lld\n",ans);
}
int cmp(int a,int b){return a>b;}
void work2(){
	int tot=0;
	for (int i=head[1];i;i=l[i].next)
		len[++tot]=l[i].s;
	std::sort(len+1,len+1+tot,cmp);
	ll ans=100000000000;
	if (n-1>=m+m){
		for (int i=1;i<=m;i++){
			ans=min(len[i]+len[m+m+1-i],ans);
		}
	}else{
		int x=m+m-n+1;
		for (int i=1;i<=x;i++)
			ans=min(ans,len[i]);
		for (int i=1;i<=m-x;i++){
			ans=min(ans,len[x+i]+len[m+m-x-i+1]);
		}
	}
	printf("%lld\n",ans);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read;m=read;
	for (int i=1;i<n;i++){
		int x=read,y=read,s=read;
		if (x!=1) t1=0;
		if (y!=x+1) t2=0;
		sum+=(ll)s;
		add(x,y,s);
		add(y,x,s);
		l[tot].QWQ=tot-1;
		l[tot-1].QWQ=tot;
	}
	if (t1){
		work2();
	}else work();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
