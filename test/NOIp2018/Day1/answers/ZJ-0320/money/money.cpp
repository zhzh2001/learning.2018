#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <algorithm>
using namespace std;
const int maxn = 110;
const int maxm = 25500;
int n,t,num = 0,ans = 0;
int a[maxn];
bool vis[maxm],b[maxm];

inline bool cmp(int x,int y)
{
	return x < y;
}

void work(int x)
{
	for(int i = x; i <= a[n]; i += x)
	{
		if(!vis[i])
		{
			vis[i] = true;
			if(b[i])
				num++;
		}
		for(int j = a[1]; j <= a[n]; ++j)
		{
			if(vis[j-i] && !vis[j])
			{
				vis[j] = true;
				if(b[j])
					num++;
			}
		}
	}
	int k = 0;
	for(int i = 1; i <= n; ++i)
	{
		if(!vis[a[i]])
		{
			k = a[i];
			break;
		}
	}
	if(k == 0)
		return ;
	else
	{
		ans++;
		work(k);
	}
	return ;
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		memset(b,0,sizeof(b));
		memset(vis,0,sizeof(vis));
		ans = 0;
		num = 0;
		scanf("%d",&n);
		for(int i = 1; i <= n; ++i)
		{
			scanf("%d",&a[i]);
			b[a[i]] = 1;
		}
		sort(a+1,a+1+n,cmp);
		ans++;
		work(a[1]);
		printf("%d\n",ans);
	}
	return 0;
}
