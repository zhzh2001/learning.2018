#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <algorithm>
using namespace std;
const int maxn = 100010;
int n,ans = 0;
int a[maxn];
bool flag = true;
bool vis[maxn];

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i = 1; i <= n; ++i)
		scanf("%d",&a[i]);
	vis[0] = 0;
	while(flag)
	{
		flag = 0;
		for(int i = 1; i <= n; ++i)
		{
			if(a[i] > 0)
			{
				a[i]--;
				if(vis[i-1] == 0)
				{
					ans++;
					vis[i-1] = 0;
				}
				vis[i] = 1;
				if(a[i] > 0)
					flag = 1;
			}
		}
	}
	printf("%d\n",ans);
	return 0;
}
