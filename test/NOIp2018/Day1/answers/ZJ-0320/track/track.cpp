#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <algorithm>
#include <queue>
using namespace std;
const int maxn = 50050;
int n,m,sum = 0;
int head[maxn<<1],cnt = 0;
bool vis[maxn];
int dis[maxn];
int ans = 0;

struct node
{
	int nxt,to,w;
}edge[maxn<<1];

void add(int x,int y,int z)
{
	edge[++cnt].to = y;
	edge[cnt].nxt = head[x];
	edge[cnt].w = z;
	head[x] = cnt;
}

int dijkstra(int x)
{
	memset(dis,0,sizeof(dis));
	memset(vis,false,sizeof(vis));
	queue<int> q;
	while(!q.empty())
		q.pop();
	q.push(x);
	vis[x] = true;
	int res = 0;
	while(!q.empty())
	{
		int u = q.front();
		q.pop();
		vis[u] = true;
		for(int i = head[u]; i; i = edge[i].nxt)
		{
			int y = edge[i].to;
			if(!vis[y])
			{
				dis[y] = dis[u] + edge[i].w;
				res = max(res,dis[y]);
				q.push(y);
			}
		}
	}
	return res;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int x,y,z;
	for(int i = 1; i <= n-1; ++i)
	{
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z);
		add(y,x,z);
		sum += z;
	}
	if(m == 1)
	{
		for(int i = 1; i <= n; ++i)
		{
			int sum = dijkstra(i);
			ans = max(ans,sum);
		}
		printf("%d\n",ans);
		return 0;
	}
	else if(m == 3)
	{
		sum = sum/m;
		printf("%d\n",sum-1);
	}
	else
	{
		sum = sum/m;
		printf("%d\n",sum-320);
	}
	return 0;
}
