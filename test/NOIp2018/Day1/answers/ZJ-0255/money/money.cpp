#include<bits/stdc++.h>
using namespace std;

int n,a[109]; bool b[30009];
int main(){
	freopen("money.in","r",stdin); freopen("money.out","w",stdout);
	int cas; scanf("%d",&cas);
	while (cas--){
		scanf("%d",&n);
		int i,j,ans=0;
		for (i=1; i<=n; i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1); memset(b,0,sizeof(b)); b[0]=1;
		for (i=1; i<=n; i++){
			ans+=(!b[a[i]]);
			for (j=a[i]; j<=a[n]; j++) b[j]|=b[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}

