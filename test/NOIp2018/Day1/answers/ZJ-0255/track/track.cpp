#include<bits/stdc++.h>
#define N 50009
using namespace std;

int n,m,tp,tot,mid,fst[N],pnt[N<<1],nxt[N<<1],len[N<<1],fa[N],h[N],q[N];
struct node{ int x,y; }f[N];
void add(int x,int y,int z){
	pnt[++tot]=y; len[tot]=z; nxt[tot]=fst[x]; fst[x]=tot;
}
int check(int k){
	int i,j,tmp=0;
	for (i=1; i<=tp && q[i]>=mid; i++) tmp+=(i!=k);
	for (j=tp; i<j; i++){
		if (i==k) continue;
		for (; i<j && (j==k || q[i]+q[j]<mid); j--);
		if (i<j){ tmp++; j--; }
	}
	return tmp;
}
void dfs(int x){
	int i,y; h[++tp]=x;
	for (i=fst[x]; i; i=nxt[i]){
		y=pnt[i];
		if (y!=fa[x]){ fa[y]=x; dfs(y); }
	}
}
void work(int x){
	int i,y;
	tp=0; f[x]=(node){0,0};
	for (i=fst[x]; i; i=nxt[i]){
		y=pnt[i];
		if (y!=fa[x]){
			q[++tp]=f[y].y+len[i];
			f[x].x+=f[y].x;
		}
	}
	q[tp+1]=0; sort(q+1,q+tp+1);
	for (i=1; i<=(tp>>1); i++) swap(q[i],q[tp-i+1]);
	int mx=check(tp+1),l=1,r=tp+1,tmp;
	while (l<r){
		tmp=l+r>>1;
		if (check(tmp)==mx) r=tmp; else l=tmp+1;
	}
	f[x]=(node){f[x].x+mx,q[l]};
}
int main(){
	freopen("track.in","r",stdin); freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int i,x,y,z;
	for (i=1; i<n; i++){
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z); add(y,x,z);
	}
	dfs(1);
	int l=0,r=500000000;
	while (l<r){
		mid=l+r+1>>1;
		for (i=n; i; i--) work(h[i]);
		if (f[1].x>=m) l=mid; else r=mid-1;	
	}
	printf("%d\n",l);
	return 0;
}

