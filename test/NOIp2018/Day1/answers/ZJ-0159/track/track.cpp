#include <cstdio>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <vector>
#include <queue>
using namespace std ;

#define pb push_back
#define mp make_pair
#define fi first
#define se second
typedef long long ll ;
typedef pair <int, int> Pii ;

const int iinf = (1ll << 31) - 1 ;
const int N = 50010 ;


int n, m, ans = 0, cnt ;
bool flag = true ;
vector <Pii > e[N] ;
int dis[N], a[N] , ANS[N] ;

bool check2() {
	int sum = 0 ;
	for (int i = 1; i <= n; i++) sum += ((int) e[i].size() == 1) ;
	return sum == 2 ;
}

void dfs(int rt, int fat) {
	for (int i = 0; i < (int) e[rt].size(); i++) {
		int to = e[rt][i].fi ;
		if (to == fat) continue ;
		if (dis[rt] + e[rt][i].se > dis[to]) {
			dis[to] = dis[rt] + e[rt][i].se ;
			ans = max(dis[to], ans) ;
			dfs(to, rt) ;
		}
	}
}

void subtask1() {
	for (int i = 1; i <= n; i++) 
	if ((int) e[i].size() == 1) {
		dfs(i, -1) ;
		break ;
	}
	printf("%d\n", ans) ;
}

int judge(int x) {
	int sum = 0, val = 0 ;
	for (int i = 1; i <= cnt; i++) {
		if (val > x) sum++, val = 0 ;
		val += a[i] ;
	}
	if (val > x) sum++ ;
	return sum >= m ;
}

void subtask2() {
	int root ;
	for (int i = 1; i <= n; i++)
	if ((int) e[i].size() == 1) {
		root = 1 ;
		break ;
	}
	int now = root, fa = -1, dis = -1 ;
	while (1) {
		int tmp = now ;
		if (e[now][0].fi == fa)  {
			if ((int) e[now].size() == 1) break ;
			dis = e[now][1].se, now = e[now][1].fi ;
		}
		else dis = e[now][0].se, now = e[now][0].fi ;
		a[++cnt] = dis ;
		fa = tmp ;
	} 
	int l = 0, r = iinf, mid ;
	while (l <= r) {
		mid = (l + r) >> 1 ;
		if (judge(mid)) l = mid + 1 ;
		else r = mid - 1 ;
	}
	printf("%d\n", l) ;
} 

bool cmp(int a, int b) {
	return a > b ;
}

void subtask3() {
	for (int i = 0; i < (int) e[1].size(); i++) a[++cnt] = e[1][i].se ;
	sort(a + 1, a + n, cmp) ;
	priority_queue <int, vector<int>, greater<int> > q ;
	for (int i = 1; i <= m; i++) q.push(a[i]) ;
	int now = m + 1 ;
	while (now < n) {
		int x = q.top() ;
		q.pop() ;
		q.push(x + a[now]) ;
		now++ ;
	}
	ans = q.top() ;
	printf("%d\n", ans) ;
}

int main() {
	freopen("track.in", "r", stdin) ;
	freopen("track.out", "w", stdout) ;
	scanf("%d%d", &n, &m) ;
	for (int i = 1; i < n; i++) {
		int a, b, c ;
		scanf("%d%d%d", &a, &b, &c) ;
		e[a].pb(mp(b, c)) ;
		e[b].pb(mp(a, c)) ;
		flag &= (a == 1) ;
	}
	if (m == 1) subtask1() ; // 求树的直径 
	else if (check2()) subtask2() ; //链的部分 
	else if (flag) subtask3() ; // 起始为1 
	else cout << rand() << endl ;
	return 0 ;
}
