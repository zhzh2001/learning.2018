#include <cstdio>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstring>
using namespace std ;

const int N = 100010 ;
typedef long long ll ;

int n ;
int a[N] ;
ll ans ;

int main() {
	freopen("road.in", "r", stdin) ;
	freopen("road.out", "w", stdout) ;
	scanf("%d", &n) ;
	for (int i = 1; i <= n; i++) scanf("%d", &a[i]) ;
	ans = a[1] ;
	for (int i = 2; i <= n; i++) 
	if (a[i] > a[i - 1]) ans += a[i] - a[i - 1] ;
	printf("%lld\n", ans) ;
	return 0 ;
}
