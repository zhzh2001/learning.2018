#include <cstdio>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <vector>
using namespace std ;

#define pb push_back

const int N = 110 ;
typedef long long ll ;

int a[N] ;
int n, T ;

int gcd(int a, int b) {
	if (!b) return a ;
	else return gcd(b, a % b) ;
}

int exgcd(int a, int b, int &x, int &y) {
	if (!b) {
		x = 1, y = 0 ;
		return a ;
	}
	int d = exgcd(b, a % b, y, x) ;
	y -= (a / b) * x ;
	return d ;
}

void select() {
	sort(a + 1, a + n + 1) ;
	for (int i = 1; i <= n; i++) { // is one of the beishu 
		bool flag = true ;
		for (int j = 1; j < i; j++)
		if (a[i] % a[j] == 0 && a[j] != -1) {
			flag = false ;
			break ;
		}
		if (!flag) a[i] = -1 ;
	}
	vector <int> v ;
	for (int i = 1; i <= n; i++) if (a[i] != -1) v.pb(a[i]) ;
	for (int i = 0; i < (int) v.size(); i++) a[i + 1] = v[i] ;
	n = v.size() ;
}

void subtask2() { // 3 intergers, a[1] * a + a[2] * b = a[3] or a[2] | a[1]
	sort(a + 1, a + n + 1) ;
	int x, y, ans = 3 ;
	int GCD = exgcd(a[1], a[2], x, y) ;
	if (a[3] % GCD == 0) {
		if ((a[3] - a[2] * y) % a[1] == 0) ans-- ;
	}
	printf("%d\n", ans) ;
}

int main() {
	freopen("money.in", "r", stdin) ;
	freopen("money.out", "w", stdout) ;
	scanf("%d", &T) ;
	while (T--) {
		scanf("%d", &n) ;
		for (int i = 1; i <= n; i++) scanf("%d", &a[i]) ;
		select() ;
		if (n <= 2) cout << n << endl ;
		else if (n == 3) subtask2() ;
		else cout << n << endl ;
	}
	return 0 ;
}
