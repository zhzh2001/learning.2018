#include<cstdio>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstring>
#define LL long long
#define ULL unsigned long long
#define sk() (putchar(' '))
#define ek() (putchar('\n'))
#define MAXN 105
using namespace std;
int Read(){
	int X=0,flag=1;char ch=0;
	while (ch<'0'||ch>'9'){
		if (ch=='-') flag=-1;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9')
	  X=(X<<3)+(X<<1)+ch-48,ch=getchar();
	return X*flag;
}
void Write(int x){
	if (x<0) putchar('-'),x=-x;
	if (x>9) Write(x/10);
	putchar(x%10+48);
}
int n;
int a[MAXN];
bool f[25005];
int num;
void solve(){
	n=Read();
	num=0;
	for (int i=1;i<=n;i++) num=max(num,a[i]=Read());
	sort(a+1,a+n+1);
	for (int i=1;i<=num;i++) f[i]=0;
	f[0]=1;
	int ans=0;
	for (int i=1;i<=n;i++)
	  if (!f[a[i]]){
	  	ans++;
	  	for (int j=a[i];j<=num;j++)
	  	    f[j]=f[j]||f[j-a[i]];
	  }
	Write(ans);ek();
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=Read();
	while (T--)
	  solve();
	fclose(stdin);fclose(stdout);
	return 0;
}
