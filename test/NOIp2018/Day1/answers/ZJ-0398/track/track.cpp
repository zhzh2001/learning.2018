#include<cstdio>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<set>
#define LL long long
#define ULL unsigned long long
#define sk() (putchar(' '))
#define ek() (putchar('\n'))
#define MAXN 50005
#define sit set<sNode>::iterator
using namespace std;
int Read(){
	int X=0,flag=1;char ch=0;
	while (ch<'0'||ch>'9'){
		if (ch=='-') flag=-1;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9')
	  X=(X<<3)+(X<<1)+ch-48,ch=getchar();
	return X*flag;
}
void Write(int x){
	if (x<0) putchar('-'),x=-x;
	if (x>9) Write(x/10);
	putchar(x%10+48);
}
int n,m;
struct Node{
	int x,y,l;
	Node(int xx=0,int yy=0,int ll=0){
		x=xx;y=yy;l=ll;
	}
} Edge[MAXN<<1];
int edgenum,nxt[MAXN<<1],head[MAXN];
void addedge(int x,int y,int l){
	Edge[++edgenum]=Node(x,y,l);
	nxt[edgenum]=head[x];
	head[x]=edgenum;
}
int w[MAXN];
void dfs(int now,int fa,int l){
	w[now]=l;
	for (int i=head[now];i;i=nxt[i]){
		int tmp=Edge[i].y;
		if (tmp==fa) continue;
		dfs(tmp,now,Edge[i].l);
	}
}
int f[MAXN],g[MAXN];
struct sNode{
	int x,l;
	sNode(int xx=0,int ll=0){
		x=xx;l=ll;
	}
	friend bool operator < (sNode X,sNode Y){
		return (X.l<Y.l)||((X.l==Y.l)&&(X.x<Y.x));
	}
};
set<sNode> s;
void dp(int now,int fa,int lim){
	f[now]=g[now]=0;
	for (int i=head[now];i;i=nxt[i]){
		int tmp=Edge[i].y;
		if (tmp==fa) continue;
		dp(tmp,now,lim);
		f[now]+=f[tmp];
	}
	s.clear();
	for (int i=head[now];i;i=nxt[i]){
		int tmp=Edge[i].y;
		if (tmp==fa) continue;
		s.insert(sNode(tmp,g[tmp]));
	}
	int mx=0;
	while (!s.empty()){
		sit it=s.begin();
		sNode ss=*it;
		s.erase(it);
		sit itt=s.lower_bound(sNode(0,lim-ss.l));
		if (itt!=s.end()){
			f[now]++;
			s.erase(itt);
		}
		else{
			mx=max(mx,ss.l);
		}
	}
	g[now]=w[now]+mx;
	if (g[now]>=lim) f[now]++,g[now]=0;
}
bool check(int lim){
	dp(1,0,lim);
	if (f[1]>=m) return 1;
	return 0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=Read(),m=Read();
	int L=0,R=0;
	for (int i=1;i<n;i++){
		int x=Read(),y=Read(),l=Read();
		R+=l;
		addedge(x,y,l);addedge(y,x,l);
	}
	dfs(1,0,0);
	int ans=0;
	R/=m;
	while (L<=R){
		int mid=L+(R-L)/2;
		if (check(mid)) ans=mid,L=mid+1;
		else R=mid-1;
	}
	Write(ans);ek();
	fclose(stdin);fclose(stdout);
	return 0;
}
