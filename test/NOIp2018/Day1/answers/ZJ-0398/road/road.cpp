#include<cstdio>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstring>
#define LL long long
#define ULL unsigned long long
#define sk() (putchar(' '))
#define ek() (putchar('\n'))
#define MAXN 1000005
using namespace std;
int Read(){
	int X=0,flag=1;char ch=0;
	while (ch<'0'||ch>'9'){
		if (ch=='-') flag=-1;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9')
	  X=(X<<3)+(X<<1)+ch-48,ch=getchar();
	return X*flag;
}
void Write(int x){
	if (x<0) putchar('-'),x=-x;
	if (x>9) Write(x/10);
	putchar(x%10+48);
}
int n;
int a[MAXN];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=Read();
	for (int i=1;i<=n;i++) a[i]=Read();
	a[n+1]=0;
	int ans=0;
	for (int i=2;i<=n+1;i++)
	  if (a[i]<a[i-1])
	    ans+=a[i-1]-a[i];
	Write(ans);ek();
	fclose(stdin);fclose(stdout);
	return 0;
}
