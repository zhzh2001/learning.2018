#include<bits/stdc++.h>
using namespace std;
int x,y,z;
inline int read()
{  int x=0,fl=1; char ch=getchar();
   while (ch<'0' || ch>'9')  { if (ch=='-')  fl=-1; ch=getchar();}
   while (ch>='0' && ch<='9')  { x=x*10+ch-'0'; ch=getchar();}
   return x*fl;
}
int T,n,a[1000],vis[30000],ans;
int main()
{  freopen("money.in","r",stdin);
   freopen("money.out","w",stdout);
     T=read();
     while (T--)
     {  n=read();  ans=0;
        memset(vis,0,sizeof(vis));
        vis[0]=1;  
        for (int i=1;i<=n;i++)  a[i]=read();
        sort(a+1,a+1+n);
        for (int i=1;i<=n;i++)
        {  if (!vis[a[i]])  
		   {  ans++;
		      for (int j=a[i];j<=a[n];j++)
		      if (vis[j-a[i]])  vis[j]=1;
		   }
		}
		cout<<ans<<endl;
	 }
   return 0;
}
/*
2 
4
3 19 10 6
5
11 29 13 19 17
*/
