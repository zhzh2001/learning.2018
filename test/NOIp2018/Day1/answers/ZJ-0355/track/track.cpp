#include<bits/stdc++.h>
using namespace std;
const int maxn=200000;
int l,r,n,k,q,Ans,tot,w[maxn],next1[maxn],to[maxn],zhi[maxn],head[maxn],mx[maxn],ans[maxn];
vector<int> p[maxn];
int x,y,z;
inline int read()
{  int x=0,fl=1; char ch=getchar();
   while (ch<'0' || ch>'9')  { if (ch=='-')  fl=-1; ch=getchar();}
   while (ch>='0' && ch<='9')  { x=x*10+ch-'0'; ch=getchar();}
   return x*fl;
}
void add(int x,int y,int z)
{  next1[++tot]=head[x];
   head[x]=tot;
   to[tot]=y;
   zhi[tot]=z;
}
void deal(int x)
{  int len=p[x].size(),ans1=0,ans2=0,ans3=0;
   //cout<<"qwq"<<x<<endl;
   for (int i=1;i<=len;i++)
   {  w[i]=p[x][i-1]; }
   //cout<<endl;
   sort(w+1,w+1+len);
   int l=0,r=len;
   while (w[r]>=q && r>0)  ans1++,r--;
   while (r>l)  
   {  while (w[l]+w[r]<q && l<r) ans2=w[l],l++;
      if (l<r && w[l]+w[r]>=q)  ans1++;
      l++; r--;
   }
  // cout<<ans1<<' '<<ans2<<endl;
  int l1=1,r1=len;
   while (l1<=r1)
   {  int mid=(l1+r1)/2;
      int l=0,r=len; ans3=0;
     while (w[r]>=q && r>0)  
	 {if (r!=mid) ans3++;r--;}
	 if (r==mid) r--;
     while (r>l)  
     {  while ((w[l]+w[r]<q && l<r)||(l==mid) ) l++;
      if (l<r && w[l]+w[r]>=q)  ans3++;
      l++; r--;
      if (l==mid) l++;
      if (r==mid) r--;
      }
      if (ans3==ans1)  {ans2=max(ans2,w[mid]),l1=mid+1;}  else { r1=mid-1;}
   }
   mx[x]=ans2;
   ans[x]+=ans1;
}
void dfs(int x,int fa)
{  ans[x]=0; p[x].clear();
   for (int i=head[x];i;i=next1[i])
   {  int v=to[i];
      if (v==fa)  continue;
      dfs(v,x);
      ans[x]+=ans[v];
      p[x].push_back(mx[v]+zhi[i]);
   }
   deal(x);
  // cout<<x<<' '<<ans[x]<<' '<<mx[x]<<endl;
}
int  check(int x)
{ dfs(1,1);
  if (ans[1]>=k)  return  1; else  return 0;
} 
int main()
{  freopen("track.in","r",stdin);
   freopen("track.out","w",stdout);
    n=read(); k=read();
    for (int i=1;i<=n-1;i++)
    {  x=read(); y=read(); z=read(); r=r+z;
	   add(x,y,z);  add(y,x,z);}
	l=0;
	while (l<=r)
	{  int mid=(l+r)/2; q=mid;
	 //  cout<<"!!"<<mid<<endl;
	   if (check(mid)) {  Ans=mid; l=mid+1;}  else {  r=mid-1;}
	}
	cout<<Ans<<endl;
   return 0;
}
/*
9 3
1 2 6
2 3 3
3 4 5
4 5 10
6 2 4
7 2 9
8 4 7
9 4 4
*/
