#include<bits/stdc++.h>

using namespace std;
typedef long long LL;
const int N = 1e5 + 7;

int T;
int n;
int a[N];

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	while (T--) {
		scanf("%d", &n);
		for (int i = 1; i <= n; ++ i) scanf("%d", &a[i]);
		cout << n << endl;
	}
	return 0;
}
