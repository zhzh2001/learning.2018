#include<bits/stdc++.h>

using namespace std;
typedef long long LL;
const int N = 1e5 + 7;

int min(int x, int y) {
	return x < y ? x : y;
}



int n;
int d[N];
int minn = 10007;
int ans = 0;


bool check(int x) {
	for (int i = 1; i <= x; ++ i) {
		if (d[i] == 0) return false;
	}
	return true;
}

void dele(int l, int r) {
	for (int i = l; i <= r; ++ i) {
		d[i]--;
	}
}

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; ++ i) {
		scanf("%d", &d[i]);
		minn = min(minn, d[i]);
	}
	ans += minn;
	for (int i = 1; i <= n; ++ i) {
		d[i] -= minn;
	}
	
	cout << ans;
	return 0;
}
