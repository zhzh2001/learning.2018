type node=record
  x,y,z:longint;
end;
var
  pd1,pd2:boolean;
  a:array[1..100000]of node;
  d:array[1..1000,1..1000]of longint;
  t:array[1..1000]of boolean;
  summ1,max,j,k,left,right,mid,sum,n,m,i:longint;
procedure qsort(l,h:longint);
var
  i,j,mid:longint;
  temp:node;
begin
  i:=l; j:=h; mid:=a[(i+j) div 2].z;
  repeat
    while a[i].z>mid do inc(i);
    while a[j].z<mid do dec(j);
    if i<=j then begin
      temp:=a[i]; a[i]:=a[j]; a[j]:=temp;
      inc(i); dec(j);
    end;
  until i>j;
  if i<h then qsort(i,h);
  if l<j then qsort(l,j);
end;
function check(x:longint):boolean;
var
  i,summ,sumn:longint;
begin
  summ:=0; sumn:=0;
  for i:=1 to n-1 do begin
    if sumn<x then sumn:=sumn+a[i].z;
    if sumn>=x then begin
      summ:=summ+1;
      sumn:=0;
    end;
  end;
  exit(summ>=m);
end;
procedure dfs(x:longint);
var
  i:longint;
begin
  if summ1>max then max:=summ1;
  for i:=1 to n do
    if (d[x,i]<>0) and (t[i]) then begin
      summ1:=summ1+d[x,i];
      t[i]:=false;
      dfs(i);
      summ1:=summ1-d[x,i];
      t[i]:=true;
    end;
end;
begin
  assign(input,'track.in');reset(input);
  assign(output,'track.out');rewrite(output);
  readln(n,m);
  pd1:=true;
  pd2:=true;
  sum:=0; max:=0;
  fillchar(a,sizeof(a),0);
  for i:=1 to n-1 do begin
    readln(a[i].x,a[i].y,a[i].z);
    sum:=sum+a[i].z;
    if a[i].x<>1 then pd1:=false;
    if a[i].y-1<>a[i].x then pd2:=false;
    d[a[i].x,a[i].y]:=a[i].z;
    d[a[i].y,a[i].x]:=a[i].z;
  end;
  if (m=1) and not pd1 then begin
    for i:=1 to n do begin
      summ1:=0;
      fillchar(t,sizeof(t),true);
      t[i]:=false;
      dfs(i);
    end;
    writeln(max);
    close(input);close(output);
    exit;
  end;
  if pd2 and not pd1 then begin
    left:=1; right:=sum div m;
    while left<=right do begin
      mid:=(left+right) div 2;
      if check(mid) then left:=mid+1
                    else right:=mid-1;
    end;
    if check(left) then writeln(left)
                    else writeln(right);
    close(input);close(output);
    exit;
  end;
  qsort(1,n-1);
  if pd1 and (m=1) then begin
    writeln(a[n-1].z+a[n-2].z);
    close(input);close(output);
    exit;
  end;
  writeln('15');
  close(input);close(output);
end.


