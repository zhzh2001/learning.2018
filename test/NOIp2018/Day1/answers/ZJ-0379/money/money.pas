var
  a:array[1..100000]of longint;
  max,tt,ans,i,t,n,j:longint;
  f:array[1..100000]of boolean;
procedure qsort(l,h:longint);
var
  i,j,mid,temp:longint;
begin
  i:=l;j:=h; mid:=a[(i+j) div 2];
  repeat
    while a[i]<mid do inc(i);
    while a[j]>mid do dec(j);
    if i<=j then begin
      temp:=a[i]; a[i]:=a[j]; a[j]:=temp;
      inc(i); dec(j);
    end;
  until i>j;
  if i<h then qsort(i,h);
  if l<j then qsort(l,j);
end;
procedure dfs(x,y:longint);
var
  i:longint;
begin
  if (a[x]<>y) and (f[y]) and (f[a[x]]) then begin
    dec(ans);
    f[a[x]]:=false;
    exit;
  end;
  for i:=1 to n do
    if (y>a[i]) and (f[a[i]]) then dfs(x,y-a[i]);
end;
begin
  assign(input,'money.in');reset(input);
  assign(output,'money.out');rewrite(output);
  readln(t);
  for tt:=1 to t  do begin
    readln(n);
    ans:=n;
    max:=0;
    fillchar(a,sizeof(a),0);
    fillchar(f,sizeof(f),false);
    for j:=1 to n do begin
      read(a[j]);
      if max<a[j] then max:=a[j];
    end;
    qsort(1,n);
    if max<100000 then begin
      for j:=1 to n do
        f[a[j]]:=true;
      for j:=1 to n do
        for i:=2 to 1000 div a[j] do
          if f[a[j]*i] then begin
            dec(ans);
            f[a[j]*i]:=false;
          end;
      for j:=1 to n do dfs(j,a[j]);
    end;
      writeln(ans);
  end;
  close(input);close(output);
end.
