var
  a:array[1..100000]of longint;
  i,n,k,min,ans:longint;
  flag:boolean;
begin
  assign(input,'road.in');reset(input);
  assign(output,'road.out');rewrite(output);
  readln(n);
  fillchar(a,sizeof(a),0);
  min:=maxlongint;
  for i:=1 to n do begin
    read(a[i]);
    if a[i]<min then min:=a[i];
  end;
  flag:=true;
  k:=1;
  ans:=0;
  while flag do begin
    if min>0 then begin
      for i:=k to n do begin
        if a[i]=0 then break;
        a[i]:=a[i]-min;
      end;
      ans:=ans+min;
    end;
    while (a[k]=0) and (k<=n) do inc(k);
    min:=maxlongint;
    for i:=k to n do begin
      if a[i]=0 then break;
      if min>a[i] then min:=a[i];
    end;
    if (min=0) or (min>1000000) then flag:=false;
  end;
  writeln(ans);
  close(input);close(output);
end.

