#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <map>
using namespace std;

template <class T>void Rd(T &x){
	x=0;
	char c;
	while(c=getchar(),!isdigit(c));
	do{
		x=(x<<3)+(x<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
}

template <class T>void Max(T &x,T y){if(x<y)x=y;}
template <class T>void Min(T &x,T y){if(x>y)x=y;}

#define M 50005

struct list{
	int to,v,nx;
}lis[M<<1];
int tot,head[M];

void Add(int x,int y,int v){
	lis[++tot]=(list){y,v,head[x]};
	head[x]=tot;
}

int n,m;

struct _25{
	
	int md,mp,ans;
	
	void FDFS(int x,int p,int d){
		if(d>md)md=d,mp=x;
		for(int i=head[x];i;i=lis[i].nx){
			int to=lis[i].to;
			if(to==p)continue;
			FDFS(to,x,d+lis[i].v);
		}
	}
	
	void SDFS(int x,int p,int d){
		Max(ans,d);
		for(int i=head[x];i;i=lis[i].nx){
			int to=lis[i].to;
			if(to==p)continue;
			SDFS(to,x,d+lis[i].v);
		}
	}
	
	void solve(){
		md=0,mp=1;
		FDFS(1,0,0);
		ans=0;
		SDFS(mp,0,0);
		printf("%d\n",ans);
	}
	
}P20;

struct _2{
	
	int tot,dis[M];
	
	void DFS(int x,int p){
		for(int i=head[x];i;i=lis[i].nx){
			int to=lis[i].to;
			if(to==p)continue;
			dis[++tot]=lis[i].v;
			DFS(to,x);
		}
	}
	
	bool check(int v){
		int res=m,t=tot;
		while(res>0&&dis[t]>=v)res--,t--;
		if(res==0)return true;
		for(int i=1;i<t;i++){
			if(dis[i]+dis[t]>=v)res--,t--;
			if(res==0)return true;
		}
		return res==0;
	}
	
	void solve(){
		DFS(1,0);
		sort(dis+1,dis+tot+1);
		int l=dis[n-m],r=dis[n-1]+dis[n-2],ans=dis[n-m];
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
	
}P2;

struct _3{
	
	int dis[M];
	
	void DFS(int x,int p,int d){
		dis[x]=d;
		for(int i=head[x];i;i=lis[i].nx){
			int to=lis[i].to;
			if(to==p)continue;
			DFS(to,x,d+lis[i].v);
		}
	}
	
	bool check(int v){
		int pr=1,res=m;
		for(int i=1;i<=n;i++){
			if(dis[i]-dis[pr-1]>=v)res--,pr=i+1;
			if(res==0)return true;
		}
		return res==0;
	}
	
	void solve(){
		DFS(1,0,0);
		int l=1,r=(1<<30),ans=1;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
	
}P3;

int de[M];

struct _4{
	
	int dis[M],fa[M],sz[M],mxd[M],ms[M];
	int res;
	bool mark[M];
	
	void DFS(int x,int p,int d){
		dis[x]=d,fa[x]=p,sz[x]=1,mxd[x]=d,ms[x]=x;
		for(int i=head[x];i;i=lis[i].nx){
			int to=lis[i].to;
			if(to==p)continue;
			DFS(to,x,d+lis[i].v);
			sz[x]+=sz[to];
			if(mxd[to]>mxd[x])mxd[x]=mxd[to],ms[x]=ms[to];
		}
	}
	
	void Dsu(int x,int p,int d,int v){
		int d1=0,d2=0;
		for(int i=head[x];i;i=lis[i].nx){
			int to=lis[i].to;
			if(to==p)continue;
			Dsu(to,x,d+lis[i].v,v);
			if(mxd[to]-d>=v)res--;
			else if(d1==0)d1=mxd[to];
			else d2=mxd[to];
		}
		mxd[x]=d;
		if(d1+d2-2*d>=v)res--;
		else Max(mxd[x],d1),Max(mxd[x],d2);
	}
	
	bool check(int v,int rt){
		res=m;
		Dsu(rt,0,0,v);
		return res<=0;
	}
	
	void solve(){
		int rt=1;
		for(int i=1;i<=n;i++)
			if(de[i]==1)rt=i;
		DFS(rt,0,0);
		int l=1,r=(1<<30),ans=1;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid,rt))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
	
}P4;

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	Rd(n),Rd(m);
	bool f1=true,f2=true,f3=true;
	for(int i=1,x,y,v;i<n;i++){
		Rd(x),Rd(y),Rd(v);
		Add(x,y,v);
		Add(y,x,v);
		de[x]++,de[y]++;
		if(x!=1&&y!=1)f1=false;
		if(y!=x+1&&x!=y+1)f2=false;
		if(de[x]>3||de[y]>3)f3=false;
	}
	if(m==1)P20.solve();
	else if(f1==true)P2.solve();
	else if(f2==true)P3.solve();
	else if(f3==true)P4.solve();
	return 0;
}
