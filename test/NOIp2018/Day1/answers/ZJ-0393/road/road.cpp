#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

template <class T>void Rd(T &x){
	x=0;
	char c;
	while(c=getchar(),!isdigit(c));
	do{
		x=(x<<3)+(x<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
}

template <class T>void Min(T &x,T y){if(x>y)x=y;}
template <class T>void Max(T &x,T y){if(x<y)x=y;}

#define M 100005
#define oo 1061109567

int n,A[M];

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	Rd(n);
	for(int i=1;i<=n;i++)Rd(A[i]);
	int ans=0,mi=0,mx=0;
	for(int i=1;i<=n;i++){
		if(mx>A[i])ans+=mx-mi,mi=mx=A[i];
		else mx=A[i];
		if(mi>A[i])ans+=mi-A[i],mi=A[i];
	}
	printf("%d\n",ans+mx-mi);
	return 0;
}
