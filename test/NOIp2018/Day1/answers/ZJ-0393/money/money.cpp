#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

template <class T>void Rd(T &x){
	x=0;
	char c;
	while(c=getchar(),!isdigit(c));
	do{
		x=(x<<3)+(x<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
}

#define M 105
#define N 25005

int n,A[M];
bool dp[25005];

void solve(){
	memset(dp,false,sizeof(dp));
	Rd(n);
	for(int i=1;i<=n;i++)Rd(A[i]);
	sort(A+1,A+n+1);
	int ans=n;
	for(int i=1,x,up=A[n];i<=n;i++){
		x=A[i];
		if(dp[x]==true){ans--;continue;}
		dp[x]=true;
		for(int j=A[1];j<=up;j++){
			if(dp[j]==false)continue;
			for(int t=j+x;t<=up;t+=x)
				dp[t]=true;
		}
		while(n>i&&dp[up]==true)n--,ans--,up=A[n];
	}
	printf("%d\n",ans);
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	Rd(T);
	while(T--)solve();
	return 0;
}
