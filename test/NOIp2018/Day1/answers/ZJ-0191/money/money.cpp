#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int a[1010],f[25005];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while (cas--)
	{
		int n;
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		int maxa=a[n],ans=0;
		memset(f,0,sizeof(f));
		f[0]=1;
		for (int i=1;i<=n;i++)
		{
			if (f[a[i]]) continue;
			ans++;
			for (int j=a[i];j<=maxa;j++)
				if (f[j-a[i]]) f[j]=1;
			int flag=1;
			for (int j=1;j<=n;j++)
				if (!f[a[j]])
				{
					flag=0;
					break;
				}
			if (flag)
			{
				printf("%d\n",ans);
				break;
			}
		}
	}
	return 0;
}
