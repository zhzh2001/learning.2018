#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int m,f[100010],g[100010],a[100010];
int Next[200010],vet[200010],vel[200010],num,head[100010];
void add(int u,int v,int w)
{
	Next[++num]=head[u];
	head[u]=num;
	vet[num]=v;
	vel[num]=w;
}
void dfs(int u,int fa,int x)
{
	f[u]=0;
	g[u]=0;
	for (int i=head[u];i;i=Next[i])
	{
		int v=vet[i];
		if (v!=fa) 
		{
			dfs(v,u,x);
			f[u]+=f[v];
		}
	}
	int cnt=0;
	for (int i=head[u];i;i=Next[i])
	{
		int v=vet[i];
		if (v!=fa) a[++cnt]=g[v]+vel[i];
	}
	int tmp=0;
	sort(a+1,a+1+cnt);
	while (cnt&&a[cnt]>=x) tmp++,cnt--;
	int ll=1,tt=tmp;
	for (int i=cnt;i;i--)
	{
		while (ll<cnt&&a[i]+a[ll]<x) ll++;
		if (ll<i) ll++,tmp++;
	}
	if ((tmp-tt)*2==cnt)
	{
		f[u]+=tmp;
		g[u]=0;
		return;
	}
	int l=1,r=cnt;
	while (l<r)
	{
		int mid=(l+r+1)>>1;
		int ll=1,ttt=tt;
		for (int i=cnt;i;i--)
		{
			if (i==mid) continue;
			while (ll<cnt&&(a[i]+a[ll]<x||ll==mid)) ll++;
			if (ll<i) ll++,ttt++;
		}
		if (ttt==tmp) l=mid;
		else r=mid-1;
	}
	f[u]+=tmp;
	g[u]=a[l];
}
int bs(int l,int r)
{
	if (l==r) return l;
	int mid=(l+r+1)>>1;
	dfs(1,0,mid);
	if (f[1]>=m) return bs(mid,r);
	return bs(l,mid-1);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n,cntw=0;
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++)
	{
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		add(u,v,w);
		add(v,u,w);
		cntw+=w;
	}
	printf("%d\n",bs(0,cntw));
	return 0;
}
