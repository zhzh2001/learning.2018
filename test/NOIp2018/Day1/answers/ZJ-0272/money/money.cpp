#include <bits/stdc++.h>
using namespace std;
const int maxn=105;
const int maxa=25005;
int n,a[maxn],maxx_;
bool f[maxa];
inline int read_()
{
    int res_=0,ch;
    for(ch=getchar();ch<'0' || ch>'9';ch=getchar());
    for(;'0'<=ch && ch<='9';ch=getchar()) res_=res_*10+(ch-'0');
    return res_;
}
int main()
{
    int T,ans_;
    register int i,j;
    freopen("money.in","r",stdin); freopen("money.out","w",stdout);
    T=read_();
    while(T--)
    {
        n=read_();
        maxx_=0;
        for(i=0;i<n;i++)
            maxx_=max(maxx_,a[i]=read_());
//        f.reset();
//        f[0]=1;
        for(i=1;i<=maxx_;i++)
            f[i]=0;
        f[0]=1;
        sort(a,a+n);
        ans_=0;
        for(i=0;i<n;i++)
        {
            if(f[a[i]]) continue;
            ans_++;
            for(j=a[i];j<=maxx_;j++)
                if(f[j-a[i]])
                    f[j]=1;
        }
        printf("%d\n",ans_);
    }
    fclose(stdin); fclose(stdout);
    return 0;
}
