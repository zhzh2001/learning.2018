#include <bits/stdc++.h>
using namespace std;
const int maxn=50005;
int n,k,he_[maxn],ne_[maxn<<1],to_[maxn<<1],val_[maxn<<1],tot_;
int maxx_[maxn],len_[maxn],temp_[maxn];
inline void add_edge(int u,int v,int c)
{
    ne_[tot_]=he_[u];
    to_[tot_]=v;
    val_[tot_]=c;
    he_[u]=tot_++;
}
void dfs(int u,int fa)
{
    int i,j,t,l,r,mid_,cnt_;
    maxx_[u]=0; len_[u]=0;
    for(i=he_[u];i!=-1;i=ne_[i])
        if(to_[i]!=fa)
        {
            dfs(to_[i],u);
            if(len_[to_[i]]+val_[i]>=k)
                maxx_[u]+=maxx_[to_[i]]+1;
            else maxx_[u]+=maxx_[to_[i]];
        }
    tot_=0;
    for(i=he_[u];i!=-1;i=ne_[i])
        if(to_[i]!=fa && len_[to_[i]]+val_[i]<k)
            temp_[tot_++]=len_[to_[i]]+val_[i];
    if(!tot_) return;
    sort(temp_,temp_+tot_);
    for(i=0,j=tot_-1,t=0;i<j;)
        if(temp_[i]+temp_[j]<k)
            i++;
        else t++,i++,j--;
    maxx_[u]+=t;
    if((t<<1)==tot_) return;
    l=0,r=tot_-1;
    while(l<r)
    {
        mid_=(l+r+1)>>1;
        for(i=0,j=tot_-1,cnt_=0;i<j;)
            if(i==mid_) i++;
            else if(j==mid_) j--;
            else if(temp_[i]+temp_[j]<k)
                i++;
            else cnt_++,i++,j--;
        if(cnt_==t) l=mid_;
        else r=mid_-1;
    }
    len_[u]=temp_[l];
}
int main()
{
    int i,u,v,c,m,l,r;
    freopen("track.in","r",stdin); freopen("track.out","w",stdout);
    l=0; r=0;
    scanf("%d%d",&n,&m);
    for(i=1;i<=n;i++) he_[i]=-1;
    tot_=0;
    for(i=1;i<n;i++)
    {
        scanf("%d%d%d",&u,&v,&c);
        add_edge(u,v,c);
        add_edge(v,u,c);
        r+=c;
    }
    while(l<r)
    {
        k=l+((r-l+1)>>1);
        dfs(1,-1);
        if(maxx_[1]>=m) l=k;
        else r=k-1;
    }
    printf("%d\n",l);
    fclose(stdin); fclose(stdout);
    return 0;
}
