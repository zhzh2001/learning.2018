#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <string>
#include <set>
using namespace std;

const int MAXN=50011;
struct edge {
  int x, y, w, nxt;
} E[MAXN<<1];
int head[MAXN], dp[MAXN], dis[MAXN];
int tmp[MAXN], vis[MAXN];
set<pair<int, int> > S;
int n, m, tot, limit;

void read(int &x) {
  x=0; int f=1; char ch=getchar();
  while(!isdigit(ch)){if(ch=='-')f=-1;if(ch==EOF)return;ch=getchar();}
  while(isdigit(ch)){x=x*10+(ch-'0');ch=getchar();} x*=f;
}

void AddEdge(int x, int y, int w) {
  E[++tot]=(edge){x, y, w, head[x]};
  head[x]=tot;
}

void DFS(int x, int f) {
  dp[x]=dis[x]=0;
  for(int i=head[x];~i;i=E[i].nxt) {
    int to=E[i].y;
    if(to!=f) {
      DFS(to, x);
    }
  }
  int top=0;
  for(int i=head[x];~i;i=E[i].nxt) {
    int to=E[i].y;
    if(to!=f) {
      dp[x]+=dp[to];
      if(dis[to]+E[i].w>=limit) {
	++dp[x];
      } else {
	tmp[++top]=dis[to]+E[i].w;
	vis[top]=0;
      }
    }
  }
  if(top==0) return;
  S.clear();
  sort(tmp+1, tmp+1+top);
  //printf("Node %d :", x);
  for(int i=1;i<=top;++i) {
    //printf(" %d", tmp[i]);
    S.insert(make_pair(tmp[i], i));
  }
  //puts("");
  for(int i=1;i<=top;++i) {
    if(!vis[i]) {
      S.erase(make_pair(tmp[i], i));
      set<pair<int, int> >::iterator sit=S.lower_bound(make_pair(limit-tmp[i], 0));
      if(sit!=S.end()) {
	vis[i]=vis[(*sit).second]=1;
	++dp[x];
	S.erase(sit);
      } else {
	S.insert(make_pair(tmp[i], i));
      }
    }
  }
  dis[x]=(S.size()>0 ? (*(--S.end())).first : 0);
}

bool Check(int W) {
  limit=W;
  DFS(1, 0);
  /*
  for(int i=1;i<=n;++i) {
    printf("%d : %d , %d\n", i, dp[i], dis[i]);
  }
  //*/
  return dp[1]>=m;
}

int main() {
  memset(head, -1, sizeof head);
  //scanf("%d%d", &n, &m);
  read(n); read(m);
  for(int i=1, x, y, z;i<n;++i) {
    //scanf("%d%d%d", &x, &y, &z);
    read(x); read(y); read(z);
    AddEdge(x, y, z);
    AddEdge(y, x, z);
  }
  //return 0;
  int L=0, R=500000000, ans=0;
  while(L<=R) {
    int mid=(L+R)>>1;
    if(Check(mid)) {
      ans=mid;
      L=mid+1;
    } else {
      R=mid-1;
    }
  }
  printf("%d\n", ans);
  return 0;
}
