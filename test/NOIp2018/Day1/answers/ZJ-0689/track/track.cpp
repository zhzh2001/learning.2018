#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <string>
#include <set>
using namespace std;

typedef pair<int, int> PII;
const int MAXN=50011;
struct edge {
  int x, y, w, nxt;
} E[MAXN<<1];
int head[MAXN], dp[MAXN], dis[MAXN];
int tmp[MAXN], vis[MAXN];
set<pair<int, int> > S;
int n, m, tot, limit;

void read(int &x) {
  x=0; int f=1; char ch=getchar();
  while(!isdigit(ch)){if(ch=='-')f=-1;if(ch==EOF)return;ch=getchar();}
  while(isdigit(ch)){x=x*10+(ch-'0');ch=getchar();} x*=f;
}

namespace treap {
  const int MAXSZ=150011;
  struct node {
    int ls, rs, pr, siz;
    PII val;
  };
  node T[MAXN];
  int tot=0, root=0;

  void Clear() {
    tot=0; root=0;
  }

  void Pull(int rt) {
    T[rt].siz=T[T[rt].ls].siz+T[T[rt].rs].siz+1;
  }

  int NewNode(PII v) {
    T[++tot]=(node){0, 0, rand(), 1, v};
    return tot;
  }

  void Split(int rt, int &x, int &y, PII v){
    //printf("S %d\n", rt);
    if(rt==0) {
      x=y=0;
      return;
    }
    if(T[rt].val<v) {
      x=rt;
      Split(T[rt].rs, T[x].rs, y, v);
    } else {
      y=rt;
      Split(T[rt].ls, x, T[y].ls, v);
    }
    Pull(rt);
  }

  int Merge(int x, int y) {
    //printf("M %d %d\n", x, y);
    if(x==0 || y==0) {
      return x==0 ? y : x;
    }
    if(T[x].pr<T[y].pr) {
      T[x].rs=Merge(T[x].rs, y);
      Pull(x);
      return x;
    } else {
      T[y].ls=Merge(x, T[y].ls);
      Pull(y);
      return y;
    }
  }

  void Insert(PII v) {
    int L, R;
    Split(root, L, R, v);
    root=Merge(Merge(L, NewNode(v)), R);
  }

  void Erase(PII v) {
    int L, R, P, Q;
    Split(root, L, R, v);
    Split(R, P, Q, make_pair(v.first, v.second+1));
    root=Merge(L, Merge(Merge(T[P].ls, T[P].rs), Q));
  }

  int FindByRank(int rt, int rnk) {
    while(true) {
      if(T[T[rt].ls].siz+1==rnk) return rt;
      if(T[T[rt].ls].siz>=rnk) rt=T[rt].ls;
      else {
	rnk-=T[T[rt].ls].siz+1;
	rt=T[rt].rs;
      }
    }
  }

  int FindByValue(int rt, PII v) {
    while(true) {
      if(T[rt].val==v) return rt;
      if(T[rt].val<v) rt=T[rt].ls;
      else rt=T[rt].rs;
    }
  }

  PII LowerBound(PII v) {
    int L, R;
    Split(root, L, R, v);
    PII res;
    if(R==0 || T[R].siz==0) res=make_pair(-1, -1);
    else res=T[FindByRank(R, 1)].val;
    root=Merge(L, R);
    return res;
  }

  int GetMax() {
    if(T[root].siz==0) return 0;
    else return T[FindByRank(root, T[root].siz)].val.first;
  }
}

void AddEdge(int x, int y, int w) {
  E[++tot]=(edge){x, y, w, head[x]};
  head[x]=tot;
}

void DFS(int x, int f) {
  dp[x]=dis[x]=0;
  for(int i=head[x];~i;i=E[i].nxt) {
    int to=E[i].y;
    if(to!=f) {
      DFS(to, x);
    }
  }
  int top=0;
  for(int i=head[x];~i;i=E[i].nxt) {
    int to=E[i].y;
    if(to!=f) {
      dp[x]+=dp[to];
      if(dis[to]+E[i].w>=limit) {
	++dp[x];
      } else {
	tmp[++top]=dis[to]+E[i].w;
	vis[top]=0;
      }
    }
  }
  if(top==0) return;
  treap::Clear();
  sort(tmp+1, tmp+1+top);
  //printf("Node %d :", x);
  for(int i=1;i<=top;++i) {
    //printf(" %d", tmp[i]);
    treap::Insert(make_pair(tmp[i], i));
  }
  //puts("");
  for(int i=1;i<=top;++i) {
    if(!vis[i]) {
      treap::Erase(make_pair(tmp[i], i));
      PII now=treap::LowerBound(make_pair(limit-tmp[i], 0));
      if(now!=make_pair(-1, -1)) {
	vis[i]=vis[now.second]=1;
	++dp[x];
	treap::Erase(now);
      } else {
	treap::Insert(make_pair(tmp[i], i));
      }
    }
  }
  dis[x]=treap::GetMax();
}

bool Check(int W) {
  limit=W;
  DFS(1, 0);
  /*
  for(int i=1;i<=n;++i) {
    printf("%d : %d , %d\n", i, dp[i], dis[i]);
  }
  //*/
  return dp[1]>=m;
}

int main() {
  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
  memset(head, -1, sizeof head);
  //scanf("%d%d", &n, &m);
  read(n); read(m);
  for(int i=1, x, y, z;i<n;++i) {
    //scanf("%d%d%d", &x, &y, &z);
    read(x); read(y); read(z);
    AddEdge(x, y, z);
    AddEdge(y, x, z);
  }
  //return 0;
  int L=0, R=500000000, ans=0;
  while(L<=R) {
    int mid=(L+R)>>1;
    if(Check(mid)) {
      ans=mid;
      L=mid+1;
    } else {
      R=mid-1;
    }
  }
  printf("%d\n", ans);
  fclose(stdin);
  fclose(stdout);
  return 0;
}
