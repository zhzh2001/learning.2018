#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
using namespace std;

const int MAXN=111, MAXV=33333;
int a[MAXN], f[MAXV];
int n, m, ans;

int main() {
  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
  int T;
  scanf("%d", &T);
  while(T--) {
    scanf("%d", &n);
    for(int i=1;i<=n;++i) {
      scanf("%d", &a[i]);
    }
    memset(f, 0, sizeof f);
    sort(a+1, a+1+n);
    f[0]=1;
    ans=0;
    for(int i=1;i<=n;++i) {
      if(!f[a[i]]) {
	++ans;
	for(int j=a[i];j<=25000;++j) {
	  f[j]|=f[j-a[i]];
	}
      }
    }
    printf("%d\n", ans);
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}
