#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
using namespace std;

const int MAXN=100011;
int a[MAXN];
int n, ans;

int main() {
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
  scanf("%d", &n);
  for(int i=1;i<=n;++i) {
    scanf("%d", &a[i]);
  }
  for(int i=2;i<=n;++i) {
    if(a[i]<a[i-1]) {
      ans+=abs(a[i-1]-a[i]);
    }
  }
  ans+=a[n];
  printf("%d\n", ans);
  fclose(stdin);
  fclose(stdout);
  return 0;
}
