#include "bits/stdc++.h"
#define my_lowbit(x) ((x)&(-(x)))
#define ll long long
#define N 500001
using namespace std;

int n,a[N],c[N];
ll my_ans;

inline void my_read(int &x)
{
	int s=1;
	char ch=0;
	x=0;
	while(ch^'-'&&!isdigit(ch))
		ch=getchar();
	if(ch=='-')
	{
		s=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=(x<<1)+(x<<3)+ch-'0';
		ch=getchar();
	}
	x*=s;
}

inline int my_mindepth(int x,int y)
{
	return a[x]<a[y]?x:y;
}

inline int my_query(int l,int r)
{
	int t=0;
	while(l<=r)
		if(r-my_lowbit(r)>=l)
			t=my_mindepth(t,c[r]),r-=my_lowbit(r);
		else
			t=my_mindepth(t,r),r--;
	return t;
}

void my_dfs(int l,int r,int t)
{
	if(l==r)
	{
		my_ans+=a[l]-t;
		return;
	}
	int my_mid=my_query(l,r);
	my_ans+=a[my_mid]-t;
	if(l<my_mid)
		my_dfs(l,my_mid-1,a[my_mid]);
	if(my_mid<r)
		my_dfs(my_mid+1,r,a[my_mid]);
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	my_read(n);
	a[0]=INT_MAX;
	for(int i=1;i<=n;i++)
		my_read(a[i]);
	for(int j=1;j<=n;j++)
		for(int i=j;i<=n;i+=my_lowbit(i))
			c[i]=my_mindepth(c[i],j);
	my_dfs(1,n,0);
	printf("%lld",my_ans);
	return 0;
}
