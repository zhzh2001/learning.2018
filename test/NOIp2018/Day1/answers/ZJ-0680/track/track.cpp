#include "bits/stdc++.h"
#define N 500001
using namespace std;

int n,m,u[N],v[N],w[N],a[N];
int st1=1,st2=1;
int k=0,b[N],c[N],my_first[N],my_next[N],my_last[N];
int f[N],my_depth[N],sq[N];
int l,r,my_mid;

inline void my_read(int &x)
{
	int s=1;
	char ch=0;
	x=0;
	while(ch^'-'&&!isdigit(ch))
		ch=getchar();
	if(ch=='-')
	{
		s=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=(x<<1)+(x<<3)+ch-'0';
		ch=getchar();
	}
	x*=s;
}

int my_cmp(int x,int y)
{
	if(my_depth[x]==my_depth[y])
		return f[x]<f[y];
	return my_depth[x]<my_depth[y];
}

inline void my_add(int x,int y,int z)
{
	k++;
	b[k]=y;
	c[k]=z;
	if(my_first[x]==0)
		my_first[x]=k;
	else
		my_next[my_last[x]]=k;
	my_last[x]=k;
}

int my_judge(int x)
{
	int s=0,p=0;
	for(int i=1;i<n;i++)
	{
		p+=a[i];
		if(p>=x)
			p=0,s++;
	}
	return s>=m;
}

int my_check(int x)
{
	int s=0,l=0,r=0,my_mid=0;
	memset(f,0,sizeof(f));
	for(int i=n-1;i;i--)
	{
		if(f[i])
			continue;
		f[i]=1;
		if(a[i]>=x)
			s++;
		else
		{
			l=1,r=i-1,my_mid=0;
			while(l<=r)
			{
				my_mid=(l+r)>>1;
				if(a[i]+a[my_mid]>=x)
					r=my_mid-1;
				else
					l=my_mid+1;
			}
			while(f[l])
				l++;
			if(a[i]+a[l]>=x)
				f[l]=1,s++;
			else
				return s>=m;
		}
	}
	return s>=m;
}

int my_search(int x,int t)
{
	f[x]=1;
	if(t>=my_mid)
		return 1;
	int p=my_first[x];
	while(p)
	{
		if(my_depth[b[p]]>my_depth[x]&&!f[b[p]])
			if(my_search(b[p],t+c[p]))
				return 1;
		p=my_next[p];
	}
	p=my_first[x];
	while(p)
	{
		if(my_depth[b[p]]<my_depth[x]&&!f[b[p]])
			if(my_search(b[p],t+c[p]))
				return 1;
		p=my_next[p];
	}
	f[x]=0;
	return 0;
}

int my_cal()
{
	int s=0;
	memset(f,0,sizeof(f));
	for(int i=n;i;i--)
	{
		if(!f[i])
			s+=my_search(sq[i],0);
	}
	return s>=m;
}

void my_dfs(int x,int y)
{
	my_depth[x]=my_depth[y]+1;
	int p=my_first[x];
	while(p)
	{
		if(b[p]^y)
		{
			f[b[p]]=f[x]+c[p];
			my_dfs(b[p],x);
		}
		p=my_next[p];
	}
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	my_read(n),my_read(m);
if(m==n-1)
{
	int ans=INT_MAX;
	for(int i=1;i<n;i++)
	{
		my_read(u[i]),my_read(v[i]),my_read(w[i]);
		ans=min(ans,w[i]);
	}
	printf("%d",ans);
	return 0;
}
	for(int i=1;i<n;i++)
	{
		my_read(u[i]),my_read(v[i]),my_read(w[i]);
		if(u[i]^1)
			st1=0;
		if(v[i]^(u[i]+1))
			st2=0;
		my_add(u[i],v[i],w[i]);
		my_add(v[i],u[i],w[i]);
	}
if(st2)
{
	for(int i=1;i<n;i++)
		a[u[i]]=w[i],r+=w[i];
	r/=m;
	l=1;
	while(l<=r)
	{
		my_mid=(l+r)>>1;
		if(my_judge(my_mid))
			l=my_mid+1;
		else
			r=my_mid-1;
	}
	printf("%d",r);
	return 0;
}
if(st1)
{
	memcpy(a,w,sizeof(a));
	sort(a+1,a+n);
	for(int i=1;i<n;i++)
		r+=w[i];
	r/=m;
	l=1;
	while(l<=r)
	{
		my_mid=(l+r)>>1;
		if(my_check(my_mid))
			l=my_mid+1;
		else
			r=my_mid-1;
	}
	printf("%d",r);
	return 0;
}
if(m==1)
{
	int U=0;
	my_dfs(1,1);
	for(int i=1;i<=n;i++)
		if(f[i]>f[U])
			U=i;
	memset(f,0,sizeof(f));
	my_dfs(U,U);
	U=0;
	for(int i=1;i<=n;i++)
		if(f[i]>f[U])
			U=i;
	printf("%d",f[U]);
	return 0;
}
	my_dfs(1,1);
	for(int i=1;i<=n;i++)
		sq[i]=i;
	sort(sq+1,sq+n+1,my_cmp);
	for(int i=1;i<n;i++)
		r+=w[i];
	r/=m;
	l=1;
	while(l<=r)
	{
		my_mid=(l+r)>>1;
		if(my_cal())
			l=my_mid+1;
		else
			r=my_mid-1;
	}
	printf("%d",r);
	return 0;
}
