#include "bits/stdc++.h"
#define N 25001
using namespace std;

int T,n,my_ans,a[1001],f[N];

inline void my_read(int &x)
{
	int s=1;
	char ch=0;
	x=0;
	while(ch^'-'&&!isdigit(ch))
		ch=getchar();
	if(ch=='-')
	{
		s=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=(x<<1)+(x<<3)+ch-'0';
		ch=getchar();
	}
	x*=s;
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	my_read(T);
	while(T--)
	{
		memset(f,0,sizeof(f));
		f[0]=1;
		my_read(n);
		my_ans=n;
		for(int i=1;i<=n;i++)
			my_read(a[i]);
		sort(a+1,a+n+1);
		for(int i=1;i<=n;i++)
		{
			if(f[a[i]])
			{
				my_ans--;
				continue;
			}
			for(int j=a[i];j<N;j++)
				if(f[j-a[i]])
					f[j]=1;
		}
		printf("%d\n",my_ans);
	}
	return 0;
}
