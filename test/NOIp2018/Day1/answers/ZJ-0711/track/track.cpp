#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
inline int read() {
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar();	}
	while(ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar();	}
	return x*f;
}
const int MAXN=51000;
struct Node {
	int to,nxt,w;
} edge[MAXN<<1];
int N,M,A[MAXN],head[MAXN],idx=0;
inline void ade(const int&u,const int&v,const int&w) {
	edge[++idx].to=v; edge[idx].w=w; edge[idx].nxt=head[u]; head[u]=idx;
}
int dp[MAXN][2];
void dfs(const int&fa,const int&u) {
	int mx1=0,mx2=0;
	for(int i=head[u],v;~i;i=edge[i].nxt) if(edge[i].to!=fa) {
		v=edge[i].to; dfs(u,v);
		if(edge[i].w+dp[v][0]>mx1) mx2=mx1,mx1=edge[i].w+dp[v][0];
		else if(edge[i].w+dp[v][0]>mx2) mx2=edge[i].w+dp[v][0];
	}
	dp[u][0]=mx1;
	if(mx2==0) dp[u][1]=0; else dp[u][1]=mx1+mx2;
}
bool check(const int&k) {
	int s=0,cnt=0;
	for(int i=1;i<N;++i) {
		s+=A[i];
		if(s>=k) ++cnt,s=0;
	}
	return cnt>=M;
}
int cur=0;
void dfs2(const int&fa,const int&u) {
	for(int i=head[u],v;~i;i=edge[i].nxt) if(edge[i].to!=fa) {
		A[++cur]=edge[i].w;
		v=edge[i].to; dfs(u,v);
	}
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(head,-1,sizeof head);
	N=read(),M=read();
	bool flg=1;
	for(int i=1,u,v;i<N;++i) {
		u=read(),v=read(),A[i]=read();
		if(v!=u+1) flg=0;
		ade(u,v,A[i]); ade(v,u,A[i]);
	}
	if(M==1) {
		dfs(0,1);
		int ans=0;
		for(int i=1;i<=N;++i) ans=max(ans,max(dp[i][0],dp[i][1]));
		printf("%d\n",ans);
	} else if(flg) {
		int l=0,r=500000000,mid,ans;
		while(l<=r) {
			mid=l+r>>1;
			if(check(mid)) ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	} else {
		dfs2(0,1);
		int l=0,r=500000000,mid,ans;
		while(l<=r) {
			mid=l+r>>1;
			if(check(mid)) ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}

	return 0;
}

