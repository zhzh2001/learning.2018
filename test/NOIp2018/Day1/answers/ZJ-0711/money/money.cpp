#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
inline int read() {
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar();	}
	while(ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar();	}
	return x*f;
}
const int MAXN=110,MAXM=26000;
int N,A[MAXN],vis[MAXN],dp[MAXN][MAXM];
inline bool dfs(const int&k,const int&s) {
	if(~dp[k][s]) return dp[k][s];
	if(k==1) return dp[k][s]=(s%A[vis[k]]==0);
	for(int i=s/A[vis[k]];i>=0;--i) if(dfs(k-1,s-i*A[vis[k]])) return dp[k][s]=1;
	return dp[k][s]=0;
}
void work() {
	memset(dp,-1,sizeof dp);
	N=read(); for(int i=1;i<=N;++i) A[i]=read();
	sort(A+1,A+1+N); N=unique(A+1,A+1+N)-A-1;
	vis[0]=1; vis[1]=1;
	for(int j=2;j<=N;++j) if(!dfs(vis[0],A[j])) vis[++vis[0]]=j;
	printf("%d\n",vis[0]);
}
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while(T--) work();	

	return 0;
}

