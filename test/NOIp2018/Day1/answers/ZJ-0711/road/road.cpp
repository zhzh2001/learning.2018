#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar();	}
	while(ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar();	}
	return x*f;
}
const int MAXN=110000;
struct P {
	int v,id;
	bool operator < (const P&b) const {
		if(v==b.v) return id<b.id;
		else return v<b.v;
	}
} B[MAXN];
int N,A[MAXN],L[MAXN],R[MAXN],S[MAXN],top=0,C[MAXN];
inline void updata(int i,const int&p) { for(;i<=N;i+=i&-i) C[i]+=p; }
inline void updata(const int&l,const int&r,const int&p) { updata(l,p); updata(r+1,-p); }
inline int query(int i) {int s=0; for(;i;i-=i&-i) s+=C[i]; return s; }
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	N=read();
	for(int i=1;i<=N;++i) {
		A[i]=read();
		B[i].v=A[i]; B[i].id=i;
	}
	S[++top]=1;
	for(int i=2;i<=N;++i) {
		while(top>0&&A[S[top]]>A[i]) { R[S[top]]=i-1; --top; }
		S[++top]=i;
	}
	while(top>0) { R[S[top]]=N; --top; }
	
	S[++top]=N;
	for(int i=N-1;i>=1;--i) {
		while(top>0&&A[S[top]]>A[i]) { L[S[top]]=i+1; --top; }
		S[++top]=i;
	}
	while(top>0) { L[S[top]]=1; --top; }
	
	
	sort(B+1,B+1+N);
	int ans=0;
	for(int i=1,k;i<=N;++i) {
		k=query(B[i].id);
		ans+=B[i].v-k;
		updata(L[B[i].id],R[B[i].id],B[i].v-k);
	}
	printf("%d\n",ans);
	
	return 0;
}
