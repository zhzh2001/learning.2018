#include<bits/stdc++.h>
#define res register int 
using namespace std;
int a[105], ans, T, n, maxx;
bool f[25005];
inline int read()
{
	int s = 0, f = 1; char ch = getchar();
	while (!isdigit(ch)) { if (ch == '-') f = -1; ch = getchar(); }
	while (isdigit(ch)) s = (s << 1) + (s << 3) + ch - '0', ch = getchar();
	return s * f;
}

inline int max(int x, int y)
{
	return x > y ? x : y;
}

void solve()
{
	ans = 0;
	memset(f, 0, sizeof f);
	maxx = 0; f[0] = 1;
	for (res i = 1; i <= n; i++)
		maxx = max(maxx, a[i]);
	for (res i = 1; i <= n; i++)
	    if (!f[a[i]])
	    {
	    	for (res j = a[i]; j <= maxx; j++)
	            f[j] = f[j] | f[j - a[i]];
	        ans++;
		}
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T = read();
	while (T--)
	{
		n = read();
		for (res i = 1; i <= n; i++)
		    a[i] = read();
		sort(a + 1, a + 1 + n);
		solve();
		printf("%d\n", ans);
	}
}
