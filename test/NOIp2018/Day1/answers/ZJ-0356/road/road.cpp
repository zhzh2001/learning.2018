#include<bits/stdc++.h>
#define res register int 
#define maxn 100005
using namespace std;
int n, a[maxn], sum;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d", &n);
	for (res i = 1; i <= n; i++)
	    scanf("%d", &a[i]);
	for (res i = 1; i <= n; i++)
	    if (a[i] > a[i-1]) sum += (a[i] - a[i-1]);
	printf("%d", sum);
}
