#include<bits/stdc++.h>
#define maxn 50005
#define res register int 
using namespace std;
queue<int> que;
int ver[maxn<<1], head[maxn], nex[maxn<<1], cap[maxn<<1], deg[maxn];
int dept[maxn], dp[maxn], n, m, a[maxn], ret;
bool vis[maxn];
inline int max(int x, int y)
{
	return x > y ? x : y;
}

inline int read()
{
	int s = 0, f = 1; char ch = getchar();
	while (!isdigit(ch)) { if (ch == '-') f = -1; ch = getchar(); }
	while (isdigit(ch)) s = (s << 1) + (s << 3) + ch - '0', ch = getchar();
	return s * f;
}

void add_edge(int x, int y, int z)
{
	ver[++ver[0]] = y; nex[ver[0]] = head[x]; head[x] = ver[0]; cap[ver[0]] = z; deg[y]++;
}

void bfs(int s)
{
	que.push(s); vis[s] = 1;
	while (que.size())
    {
    	int x = que.front(); que.pop();
    	for (res i = head[x]; i; i = nex[i])
    	{
    		int y = ver[i];
    		if (vis[y]) continue;
			dept[y] = dept[x] + cap[i];
			vis[y] = 1;
    		que.push(y);
		}
	}
}

void zhijin()
{
	bfs(1);
	int s = 0;
	for (res i = 1; i <= n; i++)
	    if (dept[i] > dept[s]) s = i;
	memset(dept, 0, sizeof dept);
	memset(vis, 0, sizeof vis);
	bfs(s);
	int ans = 0;
	for (res i = 1; i <= n; i++)
	    ans = max(ans, dept[i]);
	printf("%d", ans);
	exit(0);
}

int check(int bound)
{
	int sum = 0, ans = 0;
	for (res i = 1; i < n; i++)
	{
		sum += a[i];
		if (sum >= bound) 
		{
			ans++;
			sum = 0;
		}
	}
	return ans;
}

bool pd_flower()
{
	for (res i = 2; i <= 2 * (n - 1); i += 2)
	    if (ver[i] != 1) return false;
	return true;
}

bool pd_lian()
{
	for (res i = 2; i <= 2 * (n - 1); i += 2)
		if (ver[i]  + 1 != ver[i - 1]) return false; 
	return true;
}

void lian()
{
	int s = 0;
	for (res i = 2; i <= 2 * n - 2; i += 2)
		a[ver[i]] = cap[i];
	for (res i = 29; i >= 0; i--)
	    if (check(s + (1 << i)) >= m) s += (1 << i);
	printf("%d", s + 1);
	exit(0);
}

int check1(int bound)
{
	int end = n, ans = 0;
	while (a[end - 1] >= bound) end--, ans++;
	int start = 1;
	while (start < end)
	{
		end--;
		while (start < end && a[start] + a[end] < bound) start++;
		if (a[start] + a[end] >= bound && start < end) ans++;
		start++;
	}
	return ans;
}

void flower()
{
	for (res i = 2; i <= 2 * n - 2; i += 2)
		a[ver[i - 1] - 1] = cap[i];
	sort(a + 1, a + n);
	int s = 0;
	for (res i = 29; i >= 0; i--)
	    if (check1(s + (1 << i)) >= m) s += (1 << i);
	printf("%d", s);
	exit(0);
}

void dfs(int x, int f, int bound)
{
	int firmax = 0, secmax = 0;
	for (res i = head[x]; i; i = nex[i])
	{
		int y = ver[i];
		if (y == f) continue;
		dfs(y, x, bound);
		if (cap[i] + dp[y] >= firmax)
		{
			secmax = firmax; 
			firmax = cap[i] + dp[y];
		}
		else 
		if (firmax > cap[i] + dp[y] && cap[i] + dp[y] >= secmax)
		    secmax = cap[i] + dp[y];
	}
	if (secmax + firmax >= bound) ret++;
	    else dp[x] = firmax;
}

void solve()
{
	int s;
	for (res i = 1; i <= n; i++)
	    if (deg[i] == 1) 
	    {
	    	s = i; break;
		}
	int bound = 0;
	for (res i = 29; i >= 0; i--)
	{
		ret = 0;
		memset(dp, 0, sizeof dp);
		dfs(s, 0, bound + (1 << i));
		if (ret >= m) bound += (1 << i);
	}
	printf("%d", bound);
	exit(0);
}

int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	n = read(); m = read();
	bool flag = 0;
	for (res i = 1; i < n; i++)
	{
		int x = read(), y = read(), z = read();
		add_edge(x, y, z); add_edge(y, x, z);
	}
	if (pd_lian()) lian(); else 
	if (pd_flower()) 
	    flower(); else 
	if (m == 1) zhijin(); else 
	    solve();
}
