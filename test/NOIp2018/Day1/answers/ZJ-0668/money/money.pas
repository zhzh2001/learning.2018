program money;
var
 a:array[0..1050]of longint;
 f:array[0..25050]of boolean;
 t,i,max,n,j,ans:longint;
procedure swap(var x,y:longint);
var t:longint;
 begin t:=x;x:=y;y:=t;end;
procedure qsort(l,r:longint);
var i,j,mid:longint;
 begin
  i:=l;j:=r;mid:=a[(l+r) div 2];
  repeat
   while a[i]<mid do inc(i);
   while a[j]>mid do dec(j);
   if i<=j then
    begin
     swap(a[i],a[j]);
     inc(i);
     dec(j);
    end;
  until i>j;
  if j>l then qsort(l,j);
  if i<r then qsort(i,r);
 end;
begin
 assign(input,'money.in');reset(input);
 assign(output,'money.out');rewrite(output);
 readln(t);
 while t>0 do
  begin
   readln(n);
   ans:=n;
   max:=-maxlongint;
   fillchar(f,sizeof(f),false);
   for i:=1 to n do
    begin
     read(a[i]);
     if a[i]>max then max:=a[i];
    end;
   qsort(1,n);
   for i:=1 to n do
    if f[a[i]]=false then
     begin
      f[a[i]]:=true;
      for j:=1 to max do
       if f[j]and(j+a[i]<=max) then f[j+a[i]]:=true;
     end
    else dec(ans);
   writeln(ans);
   dec(t);
  end;
 close(input);
 close(output);
end.
