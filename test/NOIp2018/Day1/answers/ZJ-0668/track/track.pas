program track;
var
 a,b,va,order:Array[0..50050]of longint;
 w:array[0..1050,0..1050]of longint;
 f:array[0..1050]of boolean;
 n,m,i,ans,sum,count,cnt,max,u,v,z:longint;
 flaga,flagb:boolean;
function min(a,b:longint):longint;
 begin if a>b then exit(b) else exit(a);end;
procedure swap(var x,y:longint);
var t:longint;
 begin t:=x;x:=y;y:=t;end;
procedure qsorta(l,r:longint);
var i,j,mid:longint;
 begin
  i:=l;j:=r;mid:=va[(l+r) div 2];
  repeat
   while va[i]<mid do inc(i);
   while va[j]>mid do dec(j);
   if i<=j then
    begin
     swap(va[i],va[j]);
     inc(i);
     dec(j);
    end;
  until i>j;
  if j>l then qsorta(l,j);
  if i<r then qsorta(i,r);
 end;
procedure qsortb(l,r:longint);
var i,j,mid:longint;
 begin
  i:=l;j:=r;mid:=a[(l+r) div 2];
  repeat
   while a[i]<mid do inc(i);
   while a[j]>mid do dec(j);
   if i<=j then
    begin
     swap(a[i],a[j]);
     swap(order[i],order[j]);
     inc(i);
     dec(j);
    end;
  until i>j;
  if j>l then qsortb(l,j);
  if i<r then qsortb(i,r);
 end;
procedure dfs(tt:longint);
var i:longint;
    flag:boolean;
 begin
  flag:=true;
  for i:=1 to n-1 do
   if (i<>tt)and f[i] and(w[tt,i]<>0) then
    begin
     flag:=false;
     f[i]:=false;
     ans:=ans+w[tt,i];
     dfs(i);
     ans:=ans-w[tt,i];
     f[i]:=true;
    end;
  if flag then
   if ans>max then max:=ans;
 end;
begin
 assign(input,'track.in');reset(input);
 assign(output,'track.out');rewrite(output);
 readln(n,m);
 flaga:=true;
 flagb:=true;
 if (n<=1000)and(m=1)then
  begin
   for i:=1 to n-1 do
    begin
     readln(u,v,z);
     w[u,v]:=z;
     w[v,u]:=z;
    end;
   for i:=1 to n do
    begin
     fillchar(f,sizeof(f),true);
     f[i]:=false;
     dfs(i);
     f[i]:=true;
    end;
   writeln(max);
  end
 else
  begin
   for i:=1 to n-1 do
    begin
     readln(a[i],b[i],va[i]);
     if a[i]<>1 then flaga:=false;
     if b[i]<>a[i]+1 then flagb:=false;
    end;
   if flaga then begin qsorta(1,n-1);writeln(va[n-m]); end
   else if flagb then
    begin
     for i:=1 to n-1 do order[i]:=i;
     qsortb(1,n-1);
     for i:=1 to n-1 do sum:=sum+va[order[i]];
     for i:=1 to n-1 do
      begin
       count:=count+va[order[i]];
       if min(count,sum-count)>ans then ans:=min(count,sum-count);
      end;
     writeln(ans);
    end
  end;
 close(input);
 close(output);
end.