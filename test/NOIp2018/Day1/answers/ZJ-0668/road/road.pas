program road;
var
 d:array[0..100050]of longint;
 n,i,ans:longint;
function find(l,r,k:longint):longint;
var
 min,i:longint;
 begin
  min:=maxlongint;
  for i:=l to r-1 do
   if (d[i]-k>=0) and (d[i]-k<min) then min:=d[i]-k;
  exit(min);
 end;
procedure sort(l,r,k:longint);
var
 cnt,i,t:longint;
 begin
  if r-1=l then begin inc(ans,d[l]-k);exit;end;
  t:=find(l,r,k);
  inc(k,t);
  inc(ans,t);
  cnt:=l;
  while d[cnt]-k<=0 do inc(cnt);
  for i:=l to r do
  if (cnt<i) and (d[i]-k<=0) then
    begin
     sort(cnt,i,k);
     cnt:=i+1;
    end;
 end;
begin
 assign(input,'road.in');reset(input);
 assign(output,'road.out');rewrite(output);
 readln(n);
 for i:=1 to n do read(d[i]);
 d[n+1]:=0;
 sort(1,n+1,0);
 writeln(ans);
 close(input);
 close(output);
end.
