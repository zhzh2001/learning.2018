#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
typedef long long ll;
using namespace std;
const int N=1e5+5,M=1e4+5;
vector<int> P[M];
int re()
{
	int all=0,key=1;char a=getchar();
	while(a<'0'||a>'9'){if(a=='-')key=-1;a=getchar();}
	while(a<='9'&&a>='0'){all=(all<<3)+(all<<1)+a-'0';a=getchar();}
	return all*key;
}
int tree[N<<2],lazy[N<<2],num[N],n,w[N];
ll anw=0;
void pushup(int l,int r,int root){
	tree[root]=min(tree[root<<1],tree[root<<1|1]);
}
void pushdown(int l,int r,int root){
	int mid=l+r>>1;
	if(lazy[root]!=0)
	{
		lazy[root<<1]+=lazy[root];
		lazy[root<<1|1]+=lazy[root];
		tree[root<<1]-=lazy[root];
		tree[root<<1|1]-=lazy[root];
		lazy[root]=0;
		pushup(l,r,root);
		return;
	}
}
void build(int l,int r,int root)
{
	int mid=l+r>>1;
	if(l==r){
		tree[root]=num[l];
		return;
	}
	else{
		build(l,mid,root<<1);
		build(mid+1,r,root<<1|1);
		pushup(l,r,root);
	}
}
void update(int ql,int qr,int l,int r,int root,int value)
{
	int mid=l+r>>1;
	if(ql<=l&&qr>=r){
		tree[root]-=value;
		lazy[root]+=value;
		return;
	}
	else{
		pushdown(l,r,root);
		if(ql<=mid) update(ql,qr,l,mid,root<<1,value);
		if(qr>mid) update(ql,qr,mid+1,r,root<<1|1,value);
		pushup(l,r,root);
	}
}
int query(int ql,int qr,int l,int r,int root)
{
	int mid=l+r>>1,minn=1e9;
	if(ql<=l&&qr>=r)
		return tree[root];
	else{
		pushdown(l,r,root);
		if(ql<=mid) minn=min(minn,query(ql,qr,l,mid,root<<1));
		if(qr>mid) minn=min(minn,query(ql,qr,mid+1,r,root<<1|1));
		pushup(l,r,root);
	}
	return minn;
}
int search1(int k,int l,int r,int lef)
{
	int minn=1e9,i;
	for(i=0;i<P[k].size();i++)
		if(P[k][i]>=lef) return i;
}
int search2(int k,int l,int r,int rig)
{
	int maxn=-1,i;
	for(i=0;i<P[k].size();i++)
	{
		if(P[k][i]>rig) break;
		maxn=max(maxn,i);
	}
	return maxn;
}
void solve(int l,int r,int tot)
{
	if(l>r) return;
	int minn=query(l,r,1,n,1),sum;
	if(minn==0) return;
	update(l,r,1,n,1,minn);
	sum=minn+tot;
	anw=anw+(ll)minn;
	int i,before=-1;
	int lef,rig;
	if(l==r) return;
	lef=search1(sum,1,P[sum].size()-1,l);
	rig=search2(sum,1,P[sum].size()-1,r);
	for(i=lef;i<=rig;i++)
	{
		if(i==lef)
			solve(l,P[sum][i]-1,sum);
		else solve(P[sum][i-1]+1,P[sum][i]-1,sum);
	}
	solve(P[sum][rig]+1,r,sum);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int i,all=0;
	n=re();
	for(i=0;i<=10000;i++) P[i].push_back(0);
	for(i=1;i<=n;i++){
		num[i]=re();
		P[num[i]].push_back(i);
		if(num[i]==0) w[++all]=i;
	}
	build(1,n,1);
	if(all>=1)
	{
		for(i=1;i<=all;i++)
		{
			if(i==1) solve(1,w[i]-1,0);
			else solve(w[i-1]+1,w[i]-1,0);
		}
		solve(w[all]+1,n,0);
	}
	if(all==0)
		solve(1,n,0);
	printf("%lld\n",anw);
	return 0;
}
