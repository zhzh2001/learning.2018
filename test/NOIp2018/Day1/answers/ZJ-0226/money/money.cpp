#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=105,M=25005;
int re()
{
	int all=0,key=1;char a=getchar();
	while(a<'0'||a>'9'){if(a=='-')key=-1;a=getchar();}
	while(a<='9'&&a>='0'){all=(all<<3)+(all<<1)+a-'0';a=getchar();}
	return all*key;
}
bool cou[M];
int num[N];
bool cmp(int x,int y){
	return x<y;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=re();
	while(T--)
	{
		memset(cou,false,sizeof(cou));
		int i,n=re(),j,tot=0,maxn=-1;
		for(i=1;i<=n;i++) num[i]=re(),maxn=max(maxn,num[i]);
		sort(num+1,num+1+n,cmp);
		for(i=1;i<=n;i++)
		{
			if(cou[num[i]]==true) continue;
			tot++;
			cou[num[i]]=true;
			for(j=num[i]+1;j<=maxn;j++)
				cou[j]|=cou[j-num[i]];
		}
		printf("%d\n",tot);
	}
	return 0;
}
