#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
const int N=3e4+5,P=5e6+5;
using namespace std;
typedef long long ll;
ll re()
{
	ll all=0,key=1;char a=getchar();
	while(a<'0'||a>'9'){if(a=='-')key=-1;a=getchar();}
	while(a<='9'&&a>='0'){all=(all<<3)+(all<<1)+a-'0';a=getchar();}
	return all*key;
}
struct edge{
	ll nxt,to,value;
}e[N<<1];
ll first[N],tot,dis[N];
int line[P],fa[N];
bool init[N];
void add(ll x,ll y,ll z)
{
	e[++tot].to=y;
	e[tot].value=z;
	e[tot].nxt=first[x];
	first[x]=tot;
}
ll point,maxn;
void bfs(ll src)
{
	ll i;
	maxn=0;
	memset(init,false,sizeof(init));
	memset(dis,0,sizeof(dis));
	memset(fa,0,sizeof(fa));
	init[src]=true;
	line[1]=src;dis[src]=0;
	fa[src]=src;
	ll head=0,tail=1,to,u;
	while(head!=tail)
	{
		head++;
		u=line[head];
		init[u]=false;
		for(i=first[u];i;i=e[i].nxt)
		{
			to=e[i].to;
			if(dis[to]<dis[u]+e[i].value)
			{
				dis[to]=dis[u]+e[i].value;
				if(dis[to]>maxn) maxn=dis[to],point=to;
				if(init[to]==false&&(!fa[to]))
				{
					init[to]=true;
					tail++;
					line[tail]=to;
					fa[to]=to;
				}
			}
		}
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	ll n,m,i,x,y,z;
	n=re();m=re();
	for(i=1;i<=n-1;i++){
		x=re();y=re();z=re();
		add(x,y,z);
		add(y,x,z);
	}
	bfs(1);
	bfs(point);
	printf("%lld\n",maxn);
	return 0;
}
