#include<bits/stdc++.h>
using namespace std;
bool _begin;
#define N 50005
int n,m;
int H[N],totedge;
struct edge{int to,n,val;}e[N<<1];
#define link(a,b,c) e[++totedge]=(edge){H[a],b,c},H[a]=totedge
namespace P1{
	int Mx,ps;
	void dfs(int f,int x,int d){
		if(d>Mx)Mx=d,ps=x;
		for(int i=H[x];i;i=e[i].to){
			int y=e[i].n;
			if(y==f)continue;
			dfs(x,y,d+e[i].val);
		}
	}
	void solve(){
		Mx=-1;
		dfs(0,1,0);
		Mx=-1;
		dfs(0,ps,0);
		cout<<Mx<<endl;
	}
}
namespace P2{
	int val[N];
	void add(int a,int l){
		val[a]=l;
	}
	bool chk(int x){
		int cnt=0;
		int cur=0;
		for(int i=1;i<n;i++){
			cur+=val[i];
			if(cur>=x)cur=0,cnt++;
		}
		return cnt>=m;
	}
	void solve(){
		int sum=0;
		for(int i=1;i<n;i++)sum+=val[i];
		int l=0,r=sum/m,ans=0;
		while(l<=r){
			int mid=l+r>>1;
			if(chk(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		cout<<ans<<endl;
	}
}
namespace P3{
	int val[N];
	void add(int b,int l){
		val[b]=l;
	}
	bool chk(int x){
		int l=2,r=n,cnt=0;
		while(l<r){
			if(val[r]>=x)cnt++,r--;
			else if(val[r]+val[l]>=x)cnt++,r--,l++;
			else l++;
		}
		if(l==r)cnt+=val[l]>=x;
		return cnt>=m;
	}
	void solve(){
		int sum=0;
		for(int i=2;i<=n;i++)sum+=val[i];
		sort(val+2,val+1+n);
		int l=0,r=sum/m,ans=0;
		while(l<=r){
			int mid=l+r>>1;
			if(chk(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		cout<<ans<<endl;
	}
}
bool _end;
int main(){//file LL mod memset...
//	printf("%lf\n",(&_end-&_begin)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	bool f2=1,f3=1;
	for(int i=1;i<n;i++){
		int a,b,l;
		scanf("%d%d%d",&a,&b,&l);
		link(a,b,l),link(b,a,l);
		P2::add(a,l);
		P3::add(b,l);
		if(a+1!=b)f2=0;
		if(a!=1)f3=0;
	}
	if(m==1)P1::solve();
	else if(f2)P2::solve();
	else P3::solve();
	return 0;
}
