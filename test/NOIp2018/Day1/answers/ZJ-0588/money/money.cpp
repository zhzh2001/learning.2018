#include<bits/stdc++.h>
using namespace std;
bool _begin;
#define N 105
#define M 25005
int n,A[N];
namespace P15{
	int mark1[20],cas;
	int mark2[20],True;
	int B[N];
	bool cmp(){
		True++;
		mark2[0]=True;
		for(int i=1;i<=*B;i++)
			for(int j=0;j<=16;j++)
				if(mark2[j]==True)
					for(int k=1;j+k*B[i]<=16;k++)
						mark2[j+k*B[i]]=True;
		for(int i=0;i<=16;i++)
			if((mark1[i]==cas)==(mark2[i]==True));
			else return 0;
		return 1;
	}
	void solve(){
		mark1[0]=++cas;
		for(int i=1;i<=n;i++)
			for(int j=0;j<=16;j++)
				if(mark1[j]==cas)
					for(int k=1;j+k*A[i]<=16;k++)
						mark1[j+k*A[i]]=cas;
		int ans=n;
		for(int i=1;i<(1<<16);i++){
			*B=0;
			for(int j=0;j<16;j++)
				if(i&1<<j)
					B[++*B]=j+1;
			if(*B<ans&&cmp())ans=*B;
		}
		cout<<ans<<endl;
	}
}
namespace P50{
	int inf;
	int mark1[M],cas;
	int mark2[M],True;
	int B[N];
	bool cmp(){
		True++;
		mark2[0]=True;
		for(int i=1;i<=*B;i++)
			for(int j=0;j<=inf;j++)
				if(mark2[j]==True)
					for(int k=1;j+k*B[i]<=inf;k++)
						mark2[j+k*B[i]]=True;
		for(int i=0;i<=inf;i++)
			if((mark1[i]==cas)==(mark2[i]==True));
			else return 0;
		return 1;
	}
	void solve(){
		inf=0;
		for(int i=1;i<=n;i++)inf=max(inf,A[i]);
		mark1[0]=++cas;
		for(int i=1;i<=n;i++)
			for(int j=0;j<=inf;j++)
				if(mark1[j]==cas)
					for(int k=1;j+k*A[i]<=inf;k++)
						mark1[j+k*A[i]]=cas;
		int ans=n;
		for(int i=0;i<(1<<n);i++){
			*B=0;
			for(int j=0;j<n;j++)
				if(i&1<<j)
					B[++*B]=A[j+1];
			if(*B<ans&&cmp())ans=*B;
		}
		cout<<ans<<endl;
	}
}
namespace P100{
	int dp[M],True;
	void solve(){
		sort(A+1,A+1+n);
		int mx=A[n],ans=0;
		dp[0]=++True;
		for(int i=1;i<=n;i++){
			if(dp[A[i]]==True)continue;
			ans++;
			for(int j=0;j<=mx;j++)
				if(dp[j]==True)
					for(int k=0;j+(1<<k)*A[i]<=mx;k++)
						dp[j+(1<<k)*A[i]]=True;
		}
		cout<<ans<<endl;
	}
}
bool _end;
int main(){//file LL mod memset...
//	printf("%lf\n",(&_end-&_begin)/1024.0/1024);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	cin>>T;
	while(T--){
		scanf("%d",&n);
		int mx=0;
		for(int i=1;i<=n;i++)scanf("%d",&A[i]),mx=max(mx,A[i]);
		if(mx<=16)P15::solve();
		else if(n<=5)P50::solve();
		else P100::solve();
	}
	return 0;
}
