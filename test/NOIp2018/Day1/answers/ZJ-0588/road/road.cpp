#include<bits/stdc++.h>
using namespace std;
bool _begin;
#define N 100005
int n,A[N];
namespace P100{
	struct ST{
		int Mi[N][20],lg[N];
		#define Min(i,j) (A[i]<A[j]?i:j)
		void Init(){
			for(int i=2;i<=n;i++)lg[i]=lg[i>>1]+1;
			for(int i=1;i<=n;i++)Mi[i][0]=i;
			for(int j=1;(1<<j)<=n;j++)
				for(int i=1;i+(1<<j)-1<=n;i++)
					Mi[i][j]=Min(Mi[i][j-1],Mi[i+(1<<j-1)][j-1]);
		}
		int Query(int l,int r){
			int k=lg[r-l+1];
			return Min(Mi[l][k],Mi[r-(1<<k)+1][k]);
		}
	}st;
	int solve(int l,int r,int cur){
		if(l>r)return 0;
		int mid=st.Query(l,r);
		return solve(l,mid-1,A[mid])+solve(mid+1,r,A[mid])+A[mid]-cur;
	}
	void solve(){
		st.Init();
		cout<<solve(1,n,0)<<endl;
	}
}
bool _end;
int main(){//file LL mod memset...
//	printf("%lf\n",(&_end-&_begin)/1024.0/1024);
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	cin>>n;
	for(int i=1;i<=n;i++)scanf("%d",&A[i]);
	P100::solve();
	return 0;
}
