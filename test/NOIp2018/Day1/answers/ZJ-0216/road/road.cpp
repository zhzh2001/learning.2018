#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<string>
#include<algorithm>
#include<cstdlib>
using namespace std;
long long road[111111];
long long ans[111111];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	long long n,i;
	long long res=0;
	cin>>n;
	for(i=1;i<=n;i++){
		scanf("%lld",road+i);
		ans[i]=road[i];
	}
	for(i=2;i<=n;i++){
		if(road[i]<=road[i-1])ans[i]=0;
		else ans[i]=road[i]-road[i-1];
	}
	for(i=1;i<=n;i++)res+=ans[i];
	cout<<res;
	return 0;
}
