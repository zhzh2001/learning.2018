#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<string>
#include<algorithm>
#include<cstdlib>
using namespace std;
long long in[111];
int vis[25555];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t,n,i,j;
	long long temp,ans;
	cin>>t;
	for(;t>=1;t--){
		scanf("%d",&n);
		memset(vis,0,sizeof(vis));
		for(i=1;i<=n;i++)scanf("%lld",in+i);
		sort(in+1,in+1+n);
		temp=in[1];
		while(temp<=in[n]){
			vis[temp]=1;
			temp+=in[1];	
		}
		for(i=2;i<=n;i++){
			if(vis[in[i]])in[i]=0;
			else vis[in[i]]=1;
			for(j=in[1];j<=in[n]-in[i];j++){
				if(!vis[j+in[i]]){
					if(vis[j]){
						vis[j+in[i]]=1;
					}
				}
			}
		}
		ans=0;
		for(i=1;i<=n;i++)if(in[i])ans++;	
		printf("%lld\n",ans);
	}
	return 0;
}
