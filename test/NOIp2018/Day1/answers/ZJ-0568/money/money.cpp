#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <cmath>
using namespace std;
const int MAXN = 25555;
int A[MAXN];
bool f[MAXN];
void Solve()
{
	int n;
	cin >> n;
	for (int i = 1; i <= n; ++i)
		cin >> A[i];
	memset(f, false, sizeof f);
	sort(A + 1, A + n + 1);
	f[0] = true;
	int m = n, MAX = A[n];
	for (int i = 1; i <= n; ++i)
	{
		if (f[A[i]]) --m;
		for (int j = A[i]; j <= MAX; ++j)
			f[j] |= f[j - A[i]]; 
	}
	cout << m << endl;
}
int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T;
	cin >> T;
	while (T--)
	{
		Solve();
	}
	
	return 0;
}
