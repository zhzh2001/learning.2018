#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <set>
#include <vector>
using namespace std;
const int MAXN = 55555;
int n, m, SUM;
namespace Graph
{
	int head[MAXN], next[MAXN << 1], to[MAXN << 1], weight[MAXN << 1], e;
	int rt, f[MAXN], g[MAXN], gs[MAXN], use[MAXN], M;
	void init()
	{
		memset(head, 0, sizeof head);
		e = 0;
	}
	void addEdge(int u, int v, int w)
	{
		++e;
		next[e] = head[u];
		to[e] = v;
		weight[e] = w;
		head[u] = e;
	}
	void calc1(int u, int fa, int w)
	{
		int f_ans = 0, g_ans = 0;
		int t = 0;
		vector<int> s;
		for (int i = head[u]; i; i = next[i])
		{
			int v = to[i];
			if (v == fa) continue;
			++t;
			s.push_back(g[v]);
		}
		sort(s.begin(), s.end());
		int g_max = 0;
		while(!s.empty())
		{
			int top = *s.begin();
			s.erase(s.begin());
			vector<int>::iterator tt = lower_bound(s.begin(), s.end(), M - top);
			if (tt == s.end())
			{
				g_max = max(g_max, top);
			}
			else
			{
				s.erase(tt);
				++f_ans;
			}
		}
		if (!s.empty())
		{
			vector<int>::iterator tt = s.end();
			--tt;
			g_max = max(g_max, *tt);
		}
		g_ans = g_max + w;
		if (g_ans >= M)
		{
			++f_ans;
			g_ans = 0;
		}
		f[u] = f_ans;
		g[u] = g_ans;
		s.clear();	}
	void calc2(int u, int fa, int w)
	{
		int f_ans = 0, g_ans = 0;
		int t = 0;
		for (int i = head[u]; i; i = next[i])
		{
			int v = to[i];
			if (v == fa) continue;
			++t;
			use[t] = false;
			gs[t] = g[v];
		}
		sort(gs + 1, gs + t + 1);
		for (int i = 1, j = t; i <= t && i < j; ++i)
		{
			while (gs[j] + gs[i] >= M && i + 1 < j && gs[j - 1] + gs[i] >= M) --j;
			if (gs[j] + gs[i] >= M)
			{
				use[i] = true;
				use[j] = true;
				++f_ans;
				--j;
			}
		}
		int g_max = 0;
		for (int i = 1; i <= t; ++i) if (!use[i])
		{
			if (gs[i] >= M)
			{
				++f_ans;
				continue;
			}
			g_max = max(g_max, gs[i]);
		}
		g_ans = g_max + w;
		if (f_ans > f[u])
		{
			f[u] = f_ans;
			g[u] = g_ans;
		}
		else if (f_ans == f[u])
		{
			g[u] = max(g[u], g_ans);
		}
	}
	void dfs(int u, int fa, int w)
	{
		int ch_size = 0;
		int f_sum = 0;
		for (int i = head[u]; i; i = next[i])
		{
			int v = to[i];
			if (v == fa) continue;
			dfs(v, u, weight[i]);
			++ch_size;
			f_sum += f[v];
		}
		calc1(u, fa, w);
//		calc2(u, fa, w);
		f[u] += f_sum;
	}
	void Solve()
	{
		rt = 1;
		int l = 0, r = SUM;
		while (l < r)
		{
			memset(f, 0, sizeof f);
			memset(g, 0, sizeof g);
			M = (l + r + 1) >> 1;
			dfs(rt, 0, 0);
			if (g[rt] >= M)
				++f[rt];
			if (f[rt] >= m)
				l = M;
			else
				r = M - 1;
		}
		printf("%d\n", l);
		
		
	}
}
int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	Graph::init();
	scanf("%d%d", &n, &m);
	for (int i = 1; i < n; ++i)
	{
		int u, v, w;
		scanf("%d%d%d", &u, &v, &w);
		Graph::addEdge(u, v, w);
		Graph::addEdge(v, u, w);
		SUM += w;
	}
	Graph::Solve();
	return 0;
}
