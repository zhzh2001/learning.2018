#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <cmath>
using namespace std;
const int MAXN = 111111;
int A[MAXN];
int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	int n;
	cin >> n;
	long long ans = 0;
	for (int i = 1; i <= n; ++i)
		cin >> A[i];
	ans = A[1];
	for (int i = 2; i <= n; ++i)
	{
		if (A[i] > A[i - 1])
			ans += A[i] - A[i - 1];
	}
	cout << ans << endl;
	
	return 0;
}
