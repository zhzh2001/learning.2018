#include <iostream>
using namespace std;
const int MAXN = 111111;
int A[MAXN];
int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	int n;
	cin >> n;
	long long ans = 0, res = 1ll << 50;
	int k = 1;
	for (int i = 1; i <= n; ++i)
		cin >> A[i];
	for (int j = 1; j <= n; ++j)
	{
		
		ans = A[j];
		for (int i = j + 1; i <= n; ++i)
		{
			if (A[i] > A[i - 1])
				ans += A[i] - A[i - 1];
		}
		if (j != 1 && A[1] > A[n])
			ans += A[1] - A[n];
		for (int i = 2; i <= j - 1; ++i)
		{
			if (A[i] > A[i - 1])
				ans += A[i] - A[i - 1];
		}
		res = min(res, ans);
	}
	cout << res << endl;
	
	return 0;
}
