#include <bits/stdc++.h>
using namespace std;
int T,n,a[105],Max,Min,aij;
bool X[25005],Y[25005],u[105];
void dfs(int i,int x)
{
	if (x>=Min)
	    return;
	if (i>n)
	{
		for (int i=1;i<=Max;i++)
		    Y[i]=false;
		for (int ii=1;ii<=n;ii++)
		    if (u[ii])
		        for (int jj=a[ii];jj<=Max;jj++)
		            if (Y[jj-a[ii]])
		                Y[jj]=true;
		for (int i=1;i<=Max;i++)
		    if (X[i]^Y[i])
		        return;
		Min=min(Min,x);
		return;
	}
	u[i]=true;
	dfs(i+1,x+1);
	u[i]=false;
	dfs(i+1,x);
	return;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	X[0]=true;
	Y[0]=true;
	while (T--)
	{
		Max=0;
		scanf("%d",&n);
		Min=n;
		for (int i=1;i<=n;i++)
		{
			scanf("%d",&a[i]);
			Max=max(Max,a[i]);
			u[i]=true;
		}
		for (int i=1;i<=Max;i++)
		    X[i]=false;
		for (int i=1;i<=n;i++)
		    for (int j=a[i];j<=Max;j++)
		        if (X[j-a[i]])
		            X[j]=true;
		dfs(1,0);
		printf("%d\n",Min);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
