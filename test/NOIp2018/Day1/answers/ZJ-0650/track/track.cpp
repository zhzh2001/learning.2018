#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cctype>
#include<algorithm>
#define BUF 65536
#define getchar z_getchar
#define putchar z_putchar
char ibuf[BUF], obuf[BUF], *icur = ibuf + BUF, *ocur = obuf;
int icnt;
inline void flush(){fwrite(obuf, 1, ocur - obuf, stdout);}
inline char getchar(){
	if(icur == ibuf + BUF){
		icnt = fread(ibuf, 1, BUF, stdin);
		icur = ibuf;
	}
	return ((icur - ibuf) < icnt) ? *icur++ : -1;
}
inline void putchar(char c){
	if(ocur == obuf + BUF){
		fwrite(obuf, 1, BUF, stdout);
		ocur = obuf;
	}
	*ocur++ = c;
}
inline int getint(){
	register char ch = getchar();
	int x = 0;
	while(!isdigit(ch))ch = getchar();
	for(;isdigit(ch);ch = getchar())x = (((x << 2) + x) << 1) + (ch ^ 48);
	return x;
}
inline void putint(int x){
	if(!x)return putchar(48);
	static int s[32];
	int t = 0;
	for(;x;x /= 10)s[++t] = x % 10;
	for(;t;t--)putchar(s[t] ^ 48);
}
#define MAXN 50010
int n, m, c, s;
int head[MAXN], to[MAXN << 1], next[MAXN << 1], val[MAXN << 1], tot = 0;
inline void $(int u, int v, int w){
	next[tot] = head[u], to[tot] = v, val[tot] = w, head[u] = tot++;
	next[tot] = head[v], to[tot] = u, val[tot] = w, head[v] = tot++;
}
int Q[MAXN], H, T, in[MAXN], out[MAXN], fa[MAXN], len[MAXN];
inline void bfs(){
	Q[H = T = 1] = 1;
	while(H <= T){
		int x = Q[H++];
		in[x] = T + 1;
		for(int i = head[x];~i;i = next[i]){
			if(to[i] == fa[x])continue;
			fa[to[i]] = x;
			len[to[i]] = val[i];
			Q[++T] = to[i];
		}
		out[x] = T;
	}
}
int f[MAXN], g[MAXN], v[MAXN];
inline bool judge(int mid){
//	fprintf(stderr, "=== %d\n", mid);
	memset(f, 0, sizeof(f));
	memset(g, 0, sizeof(g));
	memset(v, 0, sizeof(v));
	for(int i = n;i >= 1;i--){
		int x = Q[i];
		int l = in[x], r = out[x];
		for(int j = l;j <= r;j++)g[i] += g[j];
		if(l < r)std::sort(f + l, f + r + 1);
		int limit = r;
		while(limit >= in[x] && f[limit] >= mid)v[limit] = 1, g[i]++, limit--;
		r = limit;
		int G = 0, tmp;
		while(l < r){
			while(l < r && f[l] + f[r] < mid)l++;
			if(l < r && f[l] + f[r] >= mid){
				G++; l++; r--;
			}
		}
		g[i] += G;
		if(limit - (2 * G + 1) + 1 >= in[x]){
			tmp = 0;
			l = limit - (2 * G + 1) + 1;
			r = l + G;
			for(int j = l + G - 1;j >= l;j--){
				while(r <= limit && f[r] + f[j] < mid)r++;
				if(r <= limit) tmp++, r++;
			}
			if(tmp == G){
				r = l + G;
				for(int j = l + G - 1;j >= l;j--){
					while(r <= limit && f[r] + f[j] < mid)r++;
					if(r <= limit) v[j] = v[r] = 1, r++;
				}
				goto succ;
			}
			
			tmp = 0;
			l = limit - (2 * G + 1) + 1;
			r = limit - G + 1;
			for(int j = limit;j >= r;j--){
				while(l < r && f[l] + f[j] < mid)l++;
				if(l < r) tmp++, l++;
			}
			if(tmp == G){
				r = limit - G + 1;
				l = limit - (2 * G + 1) + 1;
				for(int j = limit;j >= r;j--){
					while(l < r && f[l] + f[j] < mid)l++;
					if(l < r) v[l] = v[j] = 1, l++;
				}
				goto succ;
			}
		}
		for(int j = limit - 2 * G + 1;j <= limit;j++)v[j] = 1;
		succ:
		for(int j = out[x];j >= in[x];j--){
			if(!v[j]){
				f[i] = f[j];
				break;
			}
		}
		f[i] += len[x];
//		fprintf(stderr, "%d: %d - %d\n", i, f[i], g[i]);
	}
	return g[1] >= m;
}
int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	memset(head, -1, sizeof(head));
	n = getint(); m = getint();
	for(int i = 1;i < n;i++){
		int u = getint(), v = getint(), w = getint();
		s += w;
		$(u, v, w);
	}
	bfs();
	int l = 0, r = s, ans = 0;
	while(l <= r){
		int mid = (l + r) >> 1;
		if(judge(mid)) ans = mid, l = mid + 1;
		else r = mid - 1;
	}
	putint(ans);
	putchar(10);
	flush();
}
