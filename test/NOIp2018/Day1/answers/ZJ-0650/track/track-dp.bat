@echo off
:loop
track-dmk %random% >track.in
track-baoli 2>track.err.ans
cp track.out track.ans
track 2>track.err.out
fc track.out track.ans
if not errorlevel 1 goto loop
pause
