#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cctype>
#include<algorithm>
#define BUF 65536
#define getchar z_getchar
#define putchar z_putchar
char ibuf[BUF], obuf[BUF], *icur = ibuf + BUF, *ocur = obuf;
int icnt;
inline void flush(){fwrite(obuf, 1, ocur - obuf, stdout);}
inline char getchar(){
	if(icur == ibuf + BUF){
		icnt = fread(ibuf, 1, BUF, stdin);
		icur = ibuf;
	}
	return ((icur - ibuf) < icnt) ? *icur++ : -1;
}
inline void putchar(char c){
	if(ocur == obuf + BUF){
		fwrite(obuf, 1, BUF, stdout);
		ocur = obuf;
	}
	*ocur++ = c;
}
inline int getint(){
	register char ch = getchar();
	int x = 0;
	while(!isdigit(ch))ch = getchar();
	for(;isdigit(ch);ch = getchar())x = (((x << 2) + x) << 1) + (ch ^ 48);
	return x;
}
inline void putint(int x){
	if(!x)return putchar(48);
	static int s[32];
	int t = 0;
	for(;x;x /= 10)s[++t] = x % 10;
	for(;t;t--)putchar(s[t] ^ 48);
}
#define MAXV 25010
int n, a[110], vis[MAXV];
int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	for(int T = getint();T;T--){
		n = getint();
		for(int i = 1;i <= n;i++)a[i] = getint();
		std::sort(a + 1, a + n + 1);
		memset(vis, 0, sizeof(vis));
		vis[0] = 1;
		int ans = 0;
		for(int i = 1;i <= n;i++){
			if(vis[a[i]]){
				ans++;
				continue;
			}
			for(int j = a[i];j < MAXV;j++)vis[j] |= vis[j - a[i]];
		}
		putint(n - ans);
		putchar(10);
	}
	flush();
}
