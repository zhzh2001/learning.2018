#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cctype>
#include<algorithm>
#define MAXN 100010
typedef long long lnt;
int n, a[MAXN];
lnt ans = 0;
int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
    scanf("%d", &n);
    for(int i = 1;i <= n;i++)scanf("%d", &a[i]);
    for(int i = 1;i <= n;i++){
        int delta = a[i] - a[i - 1];
        if(delta > 0)ans += delta;
    }
    printf("%lld\n", ans);
    return 0;
}
