#include<bits/stdc++.h>
using namespace std;

namespace cyb
{

#define N 320304
int ans,a[N],n;

inline int read()
{
	char c=getchar(); int ret=0;
	while(c<48||c>57)c=getchar();
	while(c>=48&&c<=57)ret=ret*10+c-48,c=getchar();
	return ret;
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	n=read();
	for(int i=1;i<=n;++i)a[i]=read();
	
	for(int i=1;i<=n;++i){
		if(a[i]<=a[i-1])continue;
		ans+=a[i]-a[i-1];
	}
	
	printf("%d\n",ans);
	
	return 0;
}

}

int main()
{
	return cyb::main();
}
