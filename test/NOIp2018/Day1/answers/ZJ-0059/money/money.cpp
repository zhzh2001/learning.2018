#include<bits/stdc++.h>
using namespace std;

namespace cyb
{

inline int read()
{
	char c=getchar(); int ret=0;
	while(c<48||c>57)c=getchar();
	while(c>=48&&c<=57)ret=ret*10+c-48,c=getchar();
	return ret;
}

int s,fl,T,n,ma,a[105],f[50005]; 

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	T=read();
	while(T--) {
		n=read(); ma=0;
		for(int i=1;i<=n;++i)a[i]=read(),ma=max(ma,a[i]);
		if(n==1) {
			puts("1");
			continue;
		}
		if(n==2) {
			if(a[1]%a[2]==0||a[2]%a[1]==0)puts("1");
				else puts("2");
			continue;
		}
		sort(a+1,a+n+1);
		n=unique(a+1,a+n+1)-a-1;
		s=0; memset(f,0,sizeof(f)); f[0]=1;
		for(int i=1;i<=n;++i){
			fl=0;
			for(int j=a[i]; j<=ma;++j){
				if(!f[j]&&f[j-a[i]]){
					fl=1;
					f[j]=1;
				}
			}
			s+=fl;
		}
		printf("%d\n",s);
	}
	return 0;
}

}

int main()
{
	return cyb::main();
}
