#include<bits/stdc++.h>
using namespace std;

inline void chmax(int &a, const int &b)
{
	a=max(a,b);
}

#define N 100045
int n,m,tot,c[N][3],a[N];

namespace wo1
{
int ans,f[50005],g[50005];
	
void dfs(int p, int pr)
{
	int u;
	for(int o=a[p]; o; o=c[o][1]){
		u=c[o][0];
		if(u==pr)continue;
		dfs(u,p);
		if(f[p]<f[u]+c[o][2]){
			g[p]=f[p];
			f[p]=f[u]+c[o][2];
		}else chmax(g[p],f[u]+c[o][2]);
	}
	ans=max(ans,f[p]+g[p]);
}

inline int main()
{
	dfs(1,0);
	printf("%d\n",ans);
	return 0;
}

}

namespace wo2
{

int ans,h,d[N],l,r,mi,ss,u,v;
inline int main()
{
	for(int o=a[1]; o; o=c[o][1])d[++h]=c[o][2];
	sort(d+1,d+h+1);	
	if(h==1){
		printf("%d\n",d[1]);
		return 0;
	}
	l=1; r=d[h]+d[h-1];
	while(l<=r){
		mi=(l+r)>>1;
		u=h; ss=m;
		while(u && ss && d[u]>=mi)--ss,--u;
		if(!ss){
			ans=mi;
			r=mi-1;
			continue;
		}
		v=1;
		for(int i=1;i<=ss;++i){
			while(v<u&&d[v]+d[u]<mi)++v;
			if(v>=u){
				r=mi-1;
				goto lll;
			}
			--u;
			++v;
		}
		ans=mi;
		l=mi+1;
		lll:;
	}
	printf("%d\n",ans);
	return 0;
}

}

namespace wo3
{
int u,d[N],h,l,r,ans,mi,ss,dd;	
void dfs(int p, int pr)
{
	int u;
	for(int o=a[p]; o; o=c[o][1]){
		u=c[o][0];
		if(u==pr)continue;
		d[++h]=c[o][2];
		dfs(u,p);
	}
}

inline int main()
{
	dfs(1,0);
	l=1; r=499950000;
	while(l<=r){
		mi=(l+r)>>1; ss=0; dd=m;
		for(int i=1;i<=h;++i){
			ss+=d[i];
			if(ss>=mi){
				ss=0;
				--dd;
			}
		}
		if(dd<=0){
			ans=mi;
			l=mi+1;
		}else r=mi-1;
	}
	printf("%d\n",ans);
	return 0;
}

}


namespace wo4
{

int u,v,k[N],f[N],g[N],l,r,mi,ans;

void dfs(int p, int pr)
{
	vector<int>d;
	int u;
	for(int o=a[p]; o; o=c[o][1]){
		u=c[o][0];
		if(u==pr)continue;
		dfs(u,p);
		f[p]+=f[u];
		d.push_back(g[u]+c[o][2]);
	}
	if(d.empty())return;
	int tt=0;
	for(vector<int>::iterator it=d.begin(); it!=d.end();++it)k[++tt]=*it;
	k[++tt]=0;
	sort(k+1,k+tt+1);
	while(tt && k[tt]>=mi) --tt,++f[p];
	if(!tt)return;
	if(tt==1){
		g[p]=k[1];
		return;
	}
	int ma=0,kk=k[tt],ss;
	for(int i=1;i<=tt; ++i){
		u=1; v=tt; ss=0;
		while(u<v){
			if(v==i)--v;
			while(u<v&&k[u]+k[v]<mi)++u;
			if(u==i)++u;
			if(u>=v)break;
			++ss;
			++u;
			--v;
		}
		if(ss>ma){
			ma=ss;
			kk=k[i];
		}else if(ss==ma)chmax(kk,k[i]);
	}
	f[p]+=ma;
	g[p]=kk;
}

inline int main()
{
	l=1; r=499950000;
	while(l<=r){
		mi=(l+r)>>1;
		memset(f,0,sizeof(f));
		memset(g,0,sizeof(g));
		dfs(1,0);
		if(f[1]>=m){
			ans=mi;
			l=mi+1;
		}else r=mi-1;
	}
	printf("%d\n",ans);
	return 0;
}

}

namespace cyb
{
	
int u,v,w,fl1,fl2;

inline int read()
{
	char c=getchar(); int ret=0;
	while(c<48||c>57)c=getchar();
	while(c>=48&&c<=57)ret=ret*10+c-48,c=getchar();
	return ret;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	n=read(); m=read(); fl1=fl2=1;
	for(int i=1;i<n;++i){
		u=read(); v=read(); w=read();
		if(u!=1)fl1=0;
		if(v!=u+1)fl2=0;
		c[++tot][0]=v; c[tot][1]=a[u]; a[u]=tot; c[tot][2]=w;
		c[++tot][0]=u; c[tot][1]=a[v]; a[v]=tot; c[tot][2]=w;
	}
	
	if(m==1)return wo1::main();
	if(fl1)return wo2::main();
	if(fl2)return wo3::main();
	return wo4::main();
}

}

int main()
{
	return cyb::main();
}
