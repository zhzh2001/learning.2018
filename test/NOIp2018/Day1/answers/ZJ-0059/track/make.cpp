#include<bits/stdc++.h>
using namespace std;
inline int read()
{
	char c=getchar(); int ret=0;
	while(c<48||c>57)c=getchar();
	while(c>=48&&c<=57)ret=ret*10+c-48,c=getchar();
	return ret;
}

int u,v,t,T,n,a[100005],f[100005];

inline int fid(int p)
{
	if(f[p]==p)return p;
	return f[p]=fid(f[p]);
}

int main()
{
	freopen("t.in","r",stdin);
	t=read();
	srand(t);
	freopen("t.in","w",stdout);
	printf("%d",t+1);
	freopen("track.in","w",stdout);
	
	n=rand()%10000+40000; int m=rand()%1000;m=min(m,n-1);
	printf("%d %d\n",n,m);
	
	for(int i=1;i<=n;++i)f[i]=i;
	
	for(int i=2;i<=n;++i){
		
		do {
			u=(rand()*rand()+rand())%n+1; v=(rand()*rand()+rand())%n+1;
		}while(fid(u)==fid(v));
		
		printf("%d %d %d\n",u,v,rand()+20);
		u=fid(u); v=fid(v);
		f[u]=v;
	}
	
	return 0;
}

