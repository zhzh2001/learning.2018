#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>
#include <memory>

#include <algorithm>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <utility>
#include <bitset>

typedef long long ll ;
using namespace std;

//defs=============================================
int T=20,N=100;
map<int,int> vis;
//main=============================================
int main() {
	srand((ll)(new char));
	freopen("money.in","w",stdout);
	printf("%d\n",T);
	while(T--) {
	vis.clear();
		printf("%d\n",N);
		int t;
		for(int i=1;i<=N;++i) printf("%d ",t=rand()%2000+20000),++vis[t];
		puts("");
		cerr<<vis.size()<<endl;
	}
}
