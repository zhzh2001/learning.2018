#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>
#include <memory>

#include <algorithm>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <utility>
#include <bitset>

typedef long long ll ;
using namespace std;
void file() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}
//defs=============================================
int T,N;
int a[10100];
int UP;

int f[101000];
int can[101000];
void work();
//main=============================================
int main() {
	file();	
	scanf("%d",&T);
	while(T--) {
		scanf("%d",&N);
		//printf("N=%d\n",N);
		for(int i=1;i<=N;++i) scanf("%d",a+i);
		sort(a+1,a+N+1);
		for(int i=1;i<=N;++i) UP=max(a[i],UP);
		work();
	}
	return 0;
}

void work() {
	memset(f,0,sizeof f);memset(can,0,sizeof can);
	f[0]=1;
	for(int i=1;i<=N;++i) {
	//printf("a[%d]=%d\n",i,a[i]);
		for(int j=0;j<=UP;++j) {
			if(j-a[i]>=0) {
				f[j]|=f[j-a[i]];
			}
		}
		if(i<N&&f[a[i+1]]) can[i+1]=true;//,printf("a[%d] can be presented from[1,%d]\n",i+1,i);
	}
	int ans=N;
	for(int i=1;i<=N;++i) if(can[i]) --ans;
	printf("%d\n",ans);
}
