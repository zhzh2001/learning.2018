#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>
#include <memory>

#include <algorithm>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <utility>
#include <bitset>

typedef long long ll ;
#define go(st) for(int i=head[st],ed=E[i].ed,vv=E[i].vv;i;i=E[i].nxt,ed=E[i].ed,vv=E[i].vv)
using namespace std;

void file() {
	freopen("track.in","r",stdin);
	freopen("std.out","w",stdout);
}
//defs=========================================================
const int MAXN=100010;
int N,M;
ll sum;

struct edge {int ed,nxt,vv;}E[MAXN*4];
int head[MAXN*2],Ecnt;
void addEdge(int st,int ed,int vv) {
	E[++Ecnt].ed=ed,E[Ecnt].nxt=head[st],head[st]=Ecnt;E[Ecnt].vv=vv;
}
void work();
void dfs(int st);

ll f[MAXN],g[MAXN];
ll ans;
int fa[MAXN];
//main=========================================================
int main() {
	file();
	scanf("%d%d",&N,&M);
	for(int i=1;i<N;++i) {
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		addEdge(x,y,v);
		addEdge(y,x,v);
		sum+=v;
	}
	work();
	return 0;
}

void work() {
	dfs(1);
	printf("%lld\n",ans);
}


void dfs(int st) {
	go(st) if(ed!=fa[st]) {
		fa[ed]=st;
		dfs(ed);
		f[st]=max(f[st],g[ed]+vv);
		f[st]=max(f[st],g[st]+g[ed]+vv);
		g[st]=max(g[st],g[ed]+vv);
	}
	ans=max(ans,f[st]);
	ans=max(ans,g[st]);
}
