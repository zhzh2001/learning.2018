#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>
#include <memory>

#include <algorithm>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <utility>
#include <bitset>

typedef long long ll ;
#define go(st) for(int i=head[st],ed=E[i].ed,vv=E[i].vv;i;i=E[i].nxt,ed=E[i].ed,vv=E[i].vv)
using namespace std;

void file() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
}
//defs=========================================================
const int MAXN=100010;
int N,M;
ll sum;

struct edge {int ed,nxt,vv;}E[MAXN*4];
int head[MAXN*2],Ecnt;
void addEdge(int st,int ed,int vv) {
	E[++Ecnt].ed=ed,E[Ecnt].nxt=head[st],head[st]=Ecnt;E[Ecnt].vv=vv;
}
void init();
void work();
void dfs(int st);
void deal(int st);

ll f[MAXN],g[MAXN];
ll LIM;
int fa[MAXN];
ll q[MAXN],qcnt;
bool va[MAXN];
//main=========================================================
int main() {
	file();
	scanf("%d%d",&N,&M);
	for(int i=1;i<N;++i) {
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		addEdge(x,y,v);
		addEdge(y,x,v);
		sum+=v;
	}
	work();
	return 0;
}


void init() {
	memset(f,0,sizeof f),memset(g,0,sizeof g);
	memset(va,0,sizeof va);
	qcnt=0;
}

ll judge(ll mid) {
	init();
	LIM=mid;
	dfs(1);
	return f[1];
}

void work() {
	ll L=1,R=sum/M+10;
	ll ans;
	while(L<=R) {
		ll mid=(L+R)>>1;
		ll num=judge(mid);
		//printf("\n\njudge(%lld) num=%lld\n\n",mid,num);
		if(num==M) {
			ans=mid;
			L=mid+1;
		} else if(num<M) R=mid-1;
		else if(num>M) L=mid+1,ans=mid;
	}
	printf("%lld\n",ans);
	//judge(15);
}

ll get_chainnum() {
	ll res=0;
	int L=1,R=qcnt;
	while(L<R) {
		while(va[R]!=0) --R;
		while(va[L]!=0) ++L;
		if(va[L]==0&&L<R&&q[L]+q[R]>=LIM) /*printf("merged (%lld,%lld)\n",q[L],q[R]),*/++res,++L,--R;
		else ++L;
	}
	//printf("res=%lld\n",res);
	return res;
}

void deal(int st) {
	ll maxv=get_chainnum();
	f[st]+=maxv;
	ll L=1,R=qcnt,ans=0;
	while(L<=R) {
		ll mid=(L+R)>>1;
		//printf("try remove %lld\n",q[mid]);
		va[mid]=true;
		ll t=get_chainnum();
		//printf("chainnum=%lld\n",t);
		va[mid]=false;
		if(t==maxv) ans=mid,L=mid+1;
		else if(t>maxv) L=mid+1,ans=mid;
		else R=mid-1;
	}
	g[st]=q[ans];
}

void dfs(int st) {
	qcnt=0;
	go(st) if(ed!=fa[st]) {
		fa[ed]=st;
		dfs(ed);
		f[st]+=f[ed];
	}
	qcnt=0;
	go(st) if(ed!=fa[st]) {
		ll t=g[ed]+vv;
		if(t>=LIM) ++f[st];
		else q[++qcnt]=t;
	}
	sort(q+1,q+qcnt+1);
	
	//printf("get:");for(int i=1;i<=qcnt;++i) printf("%lld ",q[i]);puts("");
	
	deal(st);
	//printf("f[%d]=%lld g[%d]=%lld\n",st,f[st],st,g[st]);
}
