#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>
#include <memory>

#include <algorithm>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <utility>
#include <bitset>

typedef long long ll ;
using namespace std;
void file() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
}
//defs========================================================
const int MAXN=200010;
ll N;
ll a[MAXN];
ll ans;
//main========================================================
int main() {
	file();
	scanf("%lld",&N);
	for(int i=1;i<=N;++i) {
		scanf("%lld",a+i);
		if(a[i]>a[i-1]) ans+=a[i]-a[i-1];
	}
	printf("%lld\n",ans);
	return 0;
}
