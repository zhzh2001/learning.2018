#include<bits/stdc++.h>
#define FOR(x,y,z) for(int x=y,x##_=z;x<=x##_;x++)
#define DOR(x,y,z) for(int x=y,x##_=z;x>=x##_;x--)
#define FOR_edge(x,y) for(int E_=head[x],y=edge[E_].e;E_;E_=edge[E_].e,y=edge[E_].e)
#define FOR__edge(x,y,z) for(int E_=head[x],y=edge[E_].e,z=edge[E_].c;E_;E_=edge[E_].e,y=edge[E_].e,z=edge[E_].c)
#define sf scanf
#define pf printf
#define szf(x) sizeof(x)
#define clr(x,y) memset(x,y,szf(x))
#define File_IO(_) freopen(#_".in","r",stdin),freopen(#_".out","w",stdout)
#define File_I(_) freopen(#_".in","r",stdin)
#define File_O(_) freopen(#_".out","w",stdout)
#define ll long long
#define db double
#define ct clock_t
#define ck() clock()
#define read2(x,y) read(x),read(y)
#define read3(x,y,z) read2(x,y),read(z)
using namespace std;
bool mem1;
template<class T>void tomin(T &x,T y){if(x>y)x=y;}
template<class T>void tomax(T &x,T y){if(x<y)x=y;}
template<class T>void read(T &x){
	char c;
	int f=1;
	x=0;
	while(c=getchar(),c<'0'||c>'9')if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^48);
	while(c=getchar(),c>='0'&&c<='9');
	x*=f;
}
const int maxn=1e5+5;
int n;
int h[maxn];
namespace P70{
	struct Node{
		int l,r,p,v;
		bool operator <(const Node &A)const
		{
			return h[p]>h[A.p];
		}
	};
	priority_queue<Node>q;
	struct ST{
		struct node{
			int p;
			bool operator <(const node &A)const
			{
				return h[p]<h[A.p];
			}
		}mn[maxn][20];
		int cnt[maxn];
		void build(){
			FOR(i,2,n)cnt[i]=cnt[i>>1]+1;
			FOR(i,1,n)mn[i][0]=(node){i};
			FOR(j,1,17)FOR(i,1,n-(1<<j)+1)
				mn[i][j]=min(mn[i][j-1],mn[i+(1<<j-1)][j-1]);
		}
		int query(int l,int r){
			int k=cnt[r-l+1];
			return min(mn[l][k],mn[r-(1<<k)+1][k]).p;
		}
	}st;
	void Push(int l,int r,int v){
		if(l>r)return;
		q.push((Node){l,r,st.query(l,r),v});
	}
	void solve(){
		st.build();
		Push(1,n,0);
		int ans=0;
		while(!q.empty()){
			Node tmp=q.top();q.pop();
			int l=tmp.l,r=tmp.r,p=tmp.p,v=tmp.v;
			ans+=h[p]-v;
			v=h[p];
			Push(l,p-1,v);
			Push(p+1,r,v);
		}
		pf("%d",ans);
	}
}
namespace P100{
	struct node{
		int p;
		bool operator <(const node &A)const
		{
			return h[p]>h[A.p];
		}
	}a[maxn];
	int l[maxn],r[maxn];
	void solve(){
		 FOR(i,1,n)a[i]=(node){i};
		 sort(a+1,a+n+1);
		 int ans=0;
		 h[0]=2e9;
		 FOR(i,1,n){
		 	l[i]=i-1;
		 	r[i]=i+1;
		 }
		 r[0]=1,l[0]=n,r[n]=0;
		 FOR(i,1,n){
		 	int val=0;
		 	int p=a[i].p;
		 	if(h[l[p]]<=h[p])tomax(val,h[l[p]]);
		 	if(h[r[p]]<=h[p])tomax(val,h[r[p]]);
		 	ans+=h[p]-val;
		 	l[r[p]]=l[p];
		 	r[l[p]]=r[p];
//		 	pf("%d %d %d\n",p,h[p],val);
		 }
		 pf("%d",ans);
	}
}
bool mem2;
int main(){//road
//	system("fc road2.ans road.out");
//	File_I(road2);
//	File_O(road);
//	pf("%lf\n",(&mem2-&mem1)/1024.0/1024);
	File_IO(road);
//	ct s=ck();
	read(n);
	FOR(i,1,n)read(h[i]);
	if(n<=1000)P70::solve();
	else P100::solve();
//	cout<<'\n'<<ck()-s<<endl;
	return 0;
}
