#include<bits/stdc++.h>
#define FOR(x,y,z) for(int x=y,x##_=z;x<=x##_;x++)
#define DOR(x,y,z) for(int x=y,x##_=z;x>=x##_;x--)
#define FOR_edge(x,y) for(int E_=head[x],y=edge[E_].e;E_;E_=edge[E_].e,y=edge[E_].e)
#define FOR__edge(x,y,z) for(int E_=head[x],y=edge[E_].e,z=edge[E_].c;E_;E_=edge[E_].e,y=edge[E_].e,z=edge[E_].c)
#define sf scanf
#define pf printf
#define szf(x) sizeof(x)
#define clr(x,y) memset(x,y,szf(x))
#define File_IO(_) freopen(#_".in","r",stdin),freopen(#_".out","w",stdout)
#define File_I(_) freopen(#_".in","r",stdin)
#define File_O(_) freopen(#_".out","w",stdout)
#define ll long long
#define db double
#define ct clock_t
#define ck() clock()
#define read2(x,y) read(x),read(y)
#define read3(x,y,z) read2(x,y),read(z)
using namespace std;
bool mem1;
template<class T>void tomin(T &x,T y){if(x>y)x=y;}
template<class T>void tomax(T &x,T y){if(x<y)x=y;}
template<class T>void read(T &x){
	char c;
	int f=1;
	x=0;
	while(c=getchar(),c<'0'||c>'9')if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^48);
	while(c=getchar(),c>='0'&&c<='9');
	x*=f;
}
int n,a[105];
namespace P100{
	bool dp[25005];
	void solve(){
		clr(dp,0);
		dp[0]=1;
		sort(a+1,a+n+1);
		n=unique(a+1,a+n+1)-a-1;
		int ans=0;
		FOR(i,1,n){
			if(!dp[a[i]]){
				ans++;
				FOR(j,0,25000-a[i])
					dp[j+a[i]]|=dp[j];
			}
		}
		pf("%d\n",ans);
	}
}
bool mem2;
int main(){//money
//	system("fc money2.ans money.out");
//	File_I(money2);
//	File_O(money);
//	pf("%lf\n",(&mem2-&mem1)/1024.0/1024);
	File_IO(money);
	int T;
	read(T);
//	ct s=ck();
	while(T--){
		read(n);
		FOR(i,1,n)read(a[i]);
		P100::solve();
	}
//	cout<<ck()-s<<endl;
	return 0;
}
