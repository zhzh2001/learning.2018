#include<bits/stdc++.h>
#define FOR(x,y,z) for(int x=y,x##_=z;x<=x##_;x++)
#define DOR(x,y,z) for(int x=y,x##_=z;x>=x##_;x--)
#define FOR__edge(x,y,z) for(int E_=head[x],y=edge[E_].e,z=edge[E_].c;E_;E_=edge[E_].to,y=edge[E_].e,z=edge[E_].c)
#define sf scanf
#define pf printf
#define szf(x) sizeof(x)
#define clr(x,y) memset(x,y,szf(x))
#define File_IO(_) freopen(#_".in","r",stdin),freopen(#_".out","w",stdout)
#define File_I(_) freopen(#_".in","r",stdin)
#define File_O(_) freopen(#_".out","w",stdout)
#define ll long long
#define db double
#define ct clock_t
#define ck() clock()
#define read2(x,y) read(x),read(y)
#define read3(x,y,z) read2(x,y),read(z)
using namespace std;
bool mem1;
template<class T>void tomin(T &x,T y){if(x>y)x=y;}
template<class T>void tomax(T &x,T y){if(x<y)x=y;}
template<class T>void read(T &x){
	char c;
	int f=1;
	x=0;
	while(c=getchar(),c<'0'||c>'9')if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^48);
	while(c=getchar(),c>='0'&&c<='9');
	x*=f;
}
const int maxn=5e4+5;
struct Edge{
	int e,c,to;
}edge[maxn<<1];
int head[maxn],tot;
void Add(int x,int y,int z){
	edge[++tot]=(Edge){y,z,head[x]};
	head[x]=tot;
}
int n,m;
namespace P20{//5+15
	int E,D;
	void dfs(int u,int f,int d){
		if(d>D){
			D=d;
			E=u;
		}
		FOR__edge(u,v,w){
			if(v==f)continue;
			dfs(v,u,d+w);
		}
	}
	void solve(){
		D=-1,dfs(1,0,0);
		D=-1,dfs(E,0,0);
		pf("%d",D);
	}
}
namespace P40{//5+15
	int eg[maxn];
	bool Check(int x){
		FOR(u,1,n)FOR__edge(u,v,w){
			if(v==u+1){
				eg[u]=w;
				break;
			}
		}
		int y=1;
		int cnt=0;
		while(y<n){
			int res=0;
			while(y<n&&res<x)res+=eg[y++];
			if(res<x)return 0;
			cnt++;
			if(cnt>=m)return 1;
		}
		return 0;
	}
	void solve(int r){
		int l=1,mid,ans;
		while(l<=r){
			mid=l+r>>1;
			if(Check(mid)){
				ans=mid;
				l=mid+1;
			}else r=mid-1;
		}
		pf("%d",ans);
	}
}
namespace P55{
	int tot;
	int eg[maxn];
	bool Check(int x){
		int cnt=0,l=1,r=n;
		DOR(i,tot,1){
			if(eg[i]<x)break;
			r=i;
			cnt++;
			if(cnt>=m)return 1;
		}
		r--;
		while(l<r){
			if(eg[l]+eg[r]>=x){
				cnt++;
				r--;
			}
			l++;
			if(cnt>=m)return 1;
		}
		return 0;
	}
	void solve(int r){
		tot=0;
		FOR__edge(1,v,w)
			eg[++tot]=w;
		sort(eg+1,eg+n);
		int l=1,mid,ans;
		while(l<=r){
			mid=l+r>>1;
			if(Check(mid)){
				ans=mid;
				l=mid+1; 
			}else r=mid-1;
		}
		pf("%d",ans);
	}
}
bool mem2;
int main(){//track
//	system("fc track2.ans track.out");
//	File_I(track2);
//	File_O(track);
//	pf("%lf\n",(&mem2-&mem1)/1024.0/1024);
	File_IO(track);
	read2(n,m);
	int x,y,z;
	bool f_Link=1,f_fl=1;
	int sum=0;
	FOR(i,2,n){
		read3(x,y,z);
		Add(x,y,z);
		Add(y,x,z);
		f_Link&=(y==x+1);
		f_fl&=(x==1);
		sum+=z;
	}
	if(m==1)P20::solve();
	else if(f_Link)P40::solve(sum);
	else if(f_fl)P55::solve(sum);
	return 0;
}
