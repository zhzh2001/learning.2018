#include<cstdio>
#include<cstring>
#include<algorithm>
#define FOR(i,x,y) for(int i=(x),i##_END=(y);i<=i##_END;++i)
#define DOR(i,x,y) for(int i=(x),i##_END=(y);i>=i##_END;--i)
using namespace std;

const int M=25005;
int A[105];
bool dp[M];

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	int T;
	scanf("%d",&T);
	while(T--){
		memset(dp,0,sizeof dp);
		int n;
		scanf("%d",&n);
		FOR(i,1,n) scanf("%d",&A[i]);
		sort(A+1,A+n+1);
		int ans=0;
		dp[0]=1;
		FOR(i,1,n) {
			if(dp[A[i]])continue;
			++ans;
			FOR(j,0,25000-A[i])if(dp[j])
				dp[j+A[i]]=1;
		}
		printf("%d\n",ans);
	}
	
	return 0;
}
