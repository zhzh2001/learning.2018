#include<bits/stdc++.h>
#define FOR(i,x,y) for(int i=(x),i##_END=(y);i<=i##_END;++i)
#define DOR(i,x,y) for(int i=(x),i##_END=(y);i>=i##_END;--i)
using namespace std;
template<const int maxn,const int maxm>struct Link_list{
	int head[maxn],nxt[maxm],V[maxm],tot;
	void add(int a,int b){nxt[++tot]=head[a];head[a]=tot;V[tot]=b;}
	int& operator [] (const int &x){return V[x];}
	#define LFOR(i,x,a) for(int i=(a).head[x];i;i=(a).nxt[i])
};

inline bool chk_mi(int &x,const int &y){return y<x?x=y,true:false;}
inline bool chk_mx(int &x,const int &y){return x<y?x=y,true:false;}

const int M=50005;
Link_list<M,M<<1> E,V;
int n,m;

namespace P20{
	int mx,G;
	void dfs(int x,int f,int d){
		if(d>mx)mx=d,G=x;
		LFOR(i,x,E){
			int y=E[i];
			if(y==f) continue;
			dfs(y,x,d+V[i]);
		}
	}
	void solve(){
		mx=0,G=0;
		dfs(1,0,0);
		mx=0;
		dfs(G,0,0);
		printf("%d\n",mx);
	}
}

namespace P1{
	const int N=1005;
	int dp[N][N],sz[N];
	int Res;
	void dfs(int x,int f){
		sz[x]=1;
		dp[x][0]=0;
		LFOR(qwq,x,E){
			int y=E[qwq];
			if(y==f) continue;
			dfs(y,x);
			
			FOR(i,0,sz[y]) if(dp[y][i]!=-1) dp[y][i]+=V[qwq];
			DOR(i,sz[y],0) if(dp[y][i]>=Res) chk_mx(dp[y][i+1],0);
			DOR(i,sz[x],0) if(dp[x][i]!=-1) DOR(j,sz[y],0) if(dp[y][j]!=-1){
				if(dp[x][i]+dp[y][j]>=Res)chk_mx(dp[x][i+j+1],0);
				chk_mx(dp[x][i+j],max(dp[x][i],dp[y][j]));
			}
			
			sz[x]+=sz[y];
		}
		DOR(i,sz[x],0) if(dp[x][i]>=Res) chk_mx(dp[x][i+1],0);
	}
	bool check(int res){
		Res=res;
		memset(dp,-1,sizeof dp);
		dfs(1,0);
		return dp[1][m]!=-1;
	}
	void solve(){
		cerr<<666;
		int l=0,r=5e8,ans=-1;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid)){
				ans=mid;
				l=mid+1;
			}else r=mid-1;
		}
		printf("%d\n",ans);
	}
}

namespace P2{
	int S[M];
	bool check(int D){
		int res=0,now=0;
		FOR(i,1,n-1){
			now+=S[i];
			if(now>=D)now=0,res++;
		}
		return res>=m;
	}
	void solve(){
		FOR(x,1,n-1){
			LFOR(i,x,E){
				if(E[i]==x+1)S[x]=V[i];
			}
		}
		int l=0,r=5e8,ans=-1;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid)){
				ans=mid;
				r=mid-1;
			}else l=mid+1;
		}
		printf("%d\n",ans);
	}
}

namespace P3{
	const int N=16;
	int dp[N][1<<N],fa[N],D[N],Id[N],dep[N],Cost[1<<N];
	void dfs(int x,int f){
		fa[x]=f,dep[x]=dep[f]+1;
		LFOR(i,x,E){
			int y=E[i];
			if(y==f) continue;
			D[y]=V[i];
			Id[y]=(i-1)/2;
			dfs(y,x);
		}
	}
	void deal(int x,int y){
		int v=0,s=0;
		while(x!=y){
			if(dep[x]>dep[y]){
				s|=(1<<Id[x]);
				v+=D[x];
				x=fa[x];
			}else {
				s|=(1<<Id[y]);
				v+=D[y];
				y=fa[y];
			}
		}
		Cost[s]=v;
	}
	void solve(){
		dfs(1,0);
		memset(Cost,-1,sizeof Cost);
		memset(dp,-1,sizeof dp);
		FOR(i,1,n) FOR(j,i+1,n) {
			deal(i,j);
		}
		dp[0][0]=1e9;
		FOR(j,0,m-1)FOR(i,0,(1<<n)-1)if(dp[j][i]!=-1){
			int res=(1<<n)-i-1;
			for(int k=res;k;k=(k-1)&res)if(Cost[k]!=-1){
				int &tmp=dp[j+1][i|k];
//				printf("j=%d i=%d\n",j+1,i|k);
				if(tmp==-1)tmp=min(Cost[k],dp[j][i]);
				else tmp=max(tmp,min(Cost[k],dp[j][i]));
			}
				
		}
		int ans=-1;
		FOR(i,0,(1<<n)-1)chk_mx(ans,dp[m][i]);
		printf("%d\n",ans);
	}
}

namespace SHUI{
	struct node{
		int c,l;
	}dp[M];
	int Res;
	vector<int>G[M];
	bool mark[M];
	
	struct Segment_tree{
		int tree[M<<2];
		void update(int x,int a,int l,int r,int p){
			if(l==r){tree[p]=a;return;}
			int mid=(l+r)>>1;
			if(mid>=x)update(x,a,l,mid,p<<1);
			else update(x,a,mid+1,r,p<<1|1);
			tree[p]=max(tree[p<<1],tree[p<<1|1]);
		}
		int query(int x,int l,int r,int p){
			if(tree[p]<x)return -1;
			if(l==r){
				if(tree[p]<x)return -1;
				else return l;
			}
			int mid=(l+r)>>1;
			if(tree[p<<1]>=x)return query(x,l,mid,p<<1);
			else return query(x,mid+1,r,p<<1|1);
		}
	}T;
	int Sz[M];
	void DFS(int x,int f){
		Sz[x]=0;
		LFOR(i,x,E){
			int y=E[i];
			if(y==f) continue;
			DFS(y,x);
			Sz[x]++;
		}
		if(Sz[x])G[x].resize(Sz[x]);
	}
	void dfs(int x,int f){
		if(!Sz[x])return;
		int t=0;
		LFOR(i,x,E){
			int y=E[i];
			if(y==f) continue;
			dfs(y,x);
			dp[x].c+=dp[y].c;
			G[x][t++]=V[i]+dp[y].l;
		}
		sort(G[x].begin(),G[x].end());
		int sz=G[x].size()-1;
		if(sz==-1)return;
		FOR(i,0,sz)mark[i]=false;
		FOR(i,0,sz){
			if(G[x][i]>=Res)mark[i]=true,dp[x].c++;
			else T.update(i,G[x][i],0,sz,1);//printf("add:%d,%d\n",i,G[x][i]);
		}
//		printf("::::%d\n",T.tree[1]);
		FOR(i,0,sz){
			if(mark[i]) continue;
			T.update(i,0,0,sz,1);
			int t=T.query(max(1,Res-G[x][i]),0,sz,1);
//			printf("Res=%d t=%d %d\n",Res,t,max(1,Res-G[x][i]));
			if(t==-1) chk_mx(dp[x].l,G[x][i]);
			else dp[x].c++,mark[t]=true,T.update(t,0,0,sz,1);
		}
	}
	bool check(int res){
//		cerr<<res<<endl;
		memset(T.tree,0,sizeof T.tree);
		Res=res;
		FOR(i,1,n)dp[i]=(node){0,0};
		dfs(1,0);
		return dp[1].c>=m;
	}
	void solve(){
		DFS(1,0);
		int l=0,r=5e8,ans=-1;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid)){
				ans=mid;
				l=mid+1;
			}else r=mid-1;
		}
		printf("%d\n",ans);
	}
}
int deg[M];

int main(){
//	printf("%f\n",sizeof (&emmm-&emm)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	
	FOR(i,2,n){
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		E.add(a,b),V.add(a,c);
		E.add(b,a),V.add(b,c);
		deg[a]++,deg[b]++;
	}
	bool p1=true;
	int p2=0;
	FOR(i,1,n) {
		if(deg[i]>3)p1=false;
		if(deg[i]==1)p2++;
	}
	
	if(m==1)P20::solve();
	else if(p1)P1::solve();
	else if(p2==2)P2::solve();
	else if(n<=15)P3::solve();
	else SHUI::solve();
	return 0;
}
