#include<cstdio>
#include<cstring>
#include<iostream>
#define FOR(i,x,y) for(int i=(x),i##_END=(y);i<=i##_END;++i)
#define DOR(i,x,y) for(int i=(x),i##_END=(y);i>=i##_END;--i)
typedef long long LL;
using namespace std;

inline bool chk_mi(int &x,const int &y){return y<x?x=y,true:false;}
inline bool chk_mx(int &x,const int &y){return x<y?x=y,true:false;}

const int M=100005;
int A[M],L[M],R[M];

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n;
	scanf("%d",&n);
	FOR(i,1,n) scanf("%d",&A[i]);
	
	L[1]=1;
	FOR(i,2,n) {
		int p=i;
		while(A[p-1]>A[i]&&p)p=L[p-1];
		L[i]=p;
	}
	R[n]=n;
	DOR(i,n-1,1){
		int p=i;
		while(A[p+1]>=A[i]&&p<n)p=R[p+1];
		R[i]=p;
	}
	int ans=0;
	FOR(i,1,n){
		int l=A[L[i]-1],r=A[R[i]+1];
		int a=max(l,r);
		ans+=A[i]-a;
	}
	printf("%d\n",ans);
	
	return 0;
}
