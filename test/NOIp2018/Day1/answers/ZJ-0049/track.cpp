#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
int roadto[200200],road[200200],roadl[200200],wz[200200];
int f1[200200],f2[200200];
int f[1010][1010];
int n,m,tot,ans,UP;

int getnum()
{
  int num=0;
  char c=getchar();
  while(c<'0' || c>'9') c=getchar();
  while(c>='0' && c<='9') num=num*10+c-'0',c=getchar();
  return num;
}

void conect(int x,int y,int z)
{
  roadto[++tot]=wz[x],road[tot]=y,roadl[tot]=z,wz[x]=tot;
  roadto[++tot]=wz[y],road[tot]=x,roadl[tot]=z,wz[y]=tot;
}

void qwe(int x,int c,int fr)
{
  int t=wz[x];
  while(t)
    {
      if(road[t]!=fr)
        {
		  f1[c]=roadl[t];
		  qwe(road[t],c+1,x);
		}
	  t=roadto[t];
	}
}

void dfs(int x,int fr)
{
  int t=wz[x],m1=0,m2=0,tt;
  while(t)
    {
      if(road[t]!=fr)
        {
		  dfs(road[t],x);
		  tt=f2[road[t]]+roadl[t];
		  if(tt>m1) m2=m1,m1=tt;
		  else if(tt>m2) m2=f2[road[t]];
		}
	  t=roadto[t];
	}
  f1[x]=m1+m2,f2[x]=m1;
  ans=max(ans,f1[x]);
}

bool yz(int x)
{
  int t2=0,t1=0,j;
  for(int i=1;i<n;i++)
    {
	  t1+=f1[i];
	  if(t1>=x) t1=0,t2++;
	  if(t2>=m) return true;
	}
  return false;
}

void ok(int x,int fr)
{
  int t=wz[x],s1=0,s2=0,i,j,l1=0,l2=0;
  while(t)
    {
      if(road[t]!=fr)
        {
		  ok(road[t],x);
		  if(!s1) s1=road[t],l1=roadl[t];
		  else s2=road[t],l2=roadl[t];
		}
	  t=roadto[t];
	}
  f[x][0]=max(l1+f[s1][0],l2+f[s2][0]);
  for(i=1;i<=m;i++)
    {
      if(f[x][i-1]>=UP) f[x][i]=0;
      else f[x][i]=-1;
      if(f[x][i]<0)
       for(j=0;j<i;j++)
	    {
	      if(f[s1][j]<0 || f[s2][i-j-1]<0) continue;
		  if(f[s1][j]+f[s2][i-j-1]+l1+l2>=UP)
		  {f[x][i]=0;
		  break;}
		}
	  if(i>1)
	   for(j=0;j<i-1;j++)
	    {
	      if(f[s1][j]<0 || f[s2][i-j-2]<0) continue;
		  if(f[s1][j]+l1>=UP && f[s2][i-j-2]+l2>=UP)
		  {f[x][i]=0;
		  break;}
		}
	   for(j=0;j<i;j++)
	    {
	      if(f[s1][j]<0 || f[s2][i-j-1]<0) continue;
		  if(f[s1][j]+l1>=UP)
            {
			f[x][i]=max(f[x][i],f[s2][i-j-1]+l2);
		    }
		  else if(f[s2][i-j-1]+l2>=UP)
		     {
			   f[x][i]=max(f[x][i],f[s1][j]+l1);
			 }
		}
	  for(j=0;j<=i;j++)
	    {
	      if(f[s1][j]<0 || f[s2][i-j]<0) continue;
		  f[x][i]=max(f[x][i],f[s1][j]+l1);
		  f[x][i]=max(f[x][i],f[s2][i-j]+l2);
		}
	  
	}
}

int main()
{
  int i,ty1,ty2,ty3,x,y,z;
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  n=getnum(),m=getnum(),ty1=ty2=ty3=1;
  if(m!=1) ty1=0;
  for(i=1;i<n;i++)
    {
	  x=getnum(),y=getnum(),z=getnum();
	  if(x!=1 && y!=1) ty2=0;
	  if(abs(x-y)!=1) ty3=0;
	  conect(x,y,z);
	}
  if(ty1)
    {
	  ans=0,dfs(1,0);
	  printf("%d\n",ans);
	}
  else if(ty2)
    {
	  for(i=2;i<=tot;i+=2) f1[i/2]=roadl[i];
	  sort(f1+1,f1+n);
	  if(m==n-1) printf("%d\n",f1[1]);
	  else if(n==3) printf("%d\n",f1[1]+f1[2]);
	  else printf("%d\n",min(f1[n-m-1]+f1[n-m],f1[n-m+1]));
	}
  else if(ty3)
    {
	  for(i=1;i<=n;i++) 
	    if(!roadto[wz[i]])
	      {
		    qwe(i,1,0);
		    break;
		  }
	  int mid,l=1,r=500000001;
	  while(1)
	    {
		  mid=(l+r)/2;
		  if(yz(mid)) 
		   {
		    if(l==mid) break;
		    l=mid;
		   }
		  else r=mid;
		}
	  printf("%d\n",l);
	}
  else
    {
	  int l=1,r=500000001;
	  while(1)
	    {
	      for(i=1;i<=m;i++) f[0][i]=-1;
		  UP=(l+r)/2,ok(1,0);
		  if(f[1][m]>=0) 
		   {
		    if(l==UP) break;
		    l=UP;
		   }
		  else r=UP;
		}
	  printf("%d\n",l);
	}
}
