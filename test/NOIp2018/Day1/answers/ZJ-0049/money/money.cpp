#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
int b[25020],a[250020];
int n,MAXN,ans;

int getnum()
{
  int num=0;
  char c=getchar();
  while(c<'0' || c>'9') c=getchar();
  while(c>='0' && c<='9') num=num*10+c-'0',c=getchar();
  return num;
}

int main()
{
  int T,i,j,t;
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  T=getnum(),b[0]=1;
  while(T--)
  {
  scanf("%d",&n),ans=n;
  for(i=1;i<=n;i++) a[i]=getnum();
  sort(a+1,a+1+n),MAXN=a[n];
  memset(b,0,sizeof(b)),b[0]=1;
  for(i=1;i<=n;i++)
    {
	  if(b[a[i]]) {ans--;continue;}
	  t=MAXN-a[i];
	  for(j=0;j<=t;j++) if(b[j]) b[j+a[i]]=1;
	}
  printf("%d\n",ans);
  }
}
