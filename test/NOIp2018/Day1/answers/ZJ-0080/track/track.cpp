#include<bits/stdc++.h>
#define M 50005
using namespace std;
bool cur1;
int tot,pr[M<<1],to[M<<1],val[M<<1],la[M<<1],n,m;
void add(int x,int y,int z) {
	to[++tot]=y,pr[tot]=la[x],val[tot]=z,la[x]=tot;
}
struct P20 {
	int ans,Mx[M];
	void dfs(int x,int f) {
		Mx[x]=0;
		for(int i=la[x]; i!=-1; i=pr[i]) {
			int y=to[i];
			if(y==f)continue;
			dfs(y,x);
			int now=Mx[y]+val[i];
			ans=max(ans,Mx[x]+now);
			if(now>Mx[x])Mx[x]=now;
		}
	}
	void solve() {
		ans=0;
		dfs(1,0);
		printf("%d\n",ans);
	}
} p20;
struct P40 {
	int dis[M];
	void dfs(int x,int f) {
		for(int i=la[x]; i!=-1; i=pr[i]) {
			int y=to[i];
			if(y==f)continue;
			dis[y]=dis[x]+val[i],dfs(y,x);
		}
	}
	bool chk(int x) {
		int y=0,cnt=0;
		for(int i=1; i<=n; i++) {
			if(dis[i]-dis[y]<x)continue;
			y=i,cnt++;
		}
		if(cnt<m)return false;
		return true;
	}
	void solve() {
		dfs(1,0);
		int l=0,r=dis[n],ans=0;
		while(l<=r) {
			int mid=(l+r)>>1;
			if(chk(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
} p40;
struct P60 {
	int dis[M];
	void dfs(int x,int f) {
		for(int i=la[x]; i!=-1; i=pr[i]) {
			int y=to[i];
			if(y==f)continue;
			dis[y]=dis[x]+val[i],dfs(y,x);
		}
	}
	bool chk(int x) {
		int cnt=0,l=1;
		for(int i=n; i>=1; i--) {
			if(dis[i]>=x)cnt++;
			else {
				while(l+1<i&&dis[l]+dis[i]<x)l++;
				if(l>=i||dis[l]+dis[i]<x)return false;
				cnt++,l++;
			}
			if(cnt==m)return true;
		}
		return false;
	}
	void solve() {
		dfs(1,0);
		sort(dis+1,dis+n+1);
		int l=0,r=dis[n]+dis[n-1],ans=0;
		while(l<=r) {
			int mid=(l+r)>>1;
			if(chk(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
} p60;
int dep[M],dis[M],top[M],son[M],fa[M],sz[M];
struct node {
	int x,y,v,lca;
	bool operator<(const node&_)const {
		if(dep[lca]!=dep[_.lca])return dep[lca]>dep[_.lca];
		return v<_.v;
	}
} A[500005];
struct P75 {
	bool mark[M];
	void dfs(int x,int f) {
		fa[x]=f,son[x]=0,sz[x]=1;
		for(int i=la[x]; i!=-1; i=pr[i]) {
			int y=to[i];
			if(y==f)continue;
			dep[y]=dep[x]+1,dis[y]=dis[x]+val[i];
			dfs(y,x);
			if(sz[y]>sz[son[x]])son[x]=y;
			sz[x]+=sz[y];
		}
	}
	void dfs_top(int x,int f,int tp) {
		top[x]=tp;
		if(son[x])dfs_top(son[x],x,tp);
		for(int i=la[x]; i!=-1; i=pr[i]) {
			int y=to[i];
			if(y==f||y==son[x])continue;
			dfs_top(y,x,y);
		}
	}
	int LCA(int x,int y) {
		while(top[x]!=top[y]) {
			if(dep[top[x]]>dep[top[y]])swap(x,y);
			y=fa[top[y]];
		}
		return dep[x]>dep[y]?y:x;
	}
	bool okk(node a) {
		int x=a.x,y=a.y,lca=a.lca;
		while(x!=lca) {
			if(mark[x])return false;
			x=fa[x];
		}
		while(y!=lca) {
			if(mark[y])return false;
			y=fa[y];
		}
		return true;
	}
	void Add(node a) {
		int x=a.x,y=a.y,lca=a.lca;
		while(x!=lca)mark[x]=1,x=fa[x];
		while(y!=lca)mark[y]=1,y=fa[y];
	}
	int b;
	bool chk(int x) {
		int cnt=0;
		memset(mark,0,sizeof(mark));
		for(int i=1; i<=b; i++) {
			if(A[i].v<x)continue;
			if(!okk(A[i]))continue;
			Add(A[i]);
			cnt++;
			if(cnt==m)return true;
		}
		return false;
	}
	void solve() {
		b=0;
		int Mx=0;
		dfs(1,1),dfs_top(1,1,1);
		for(int i=1; i<=n; i++)
			for(int j=i+1; j<=n; j++) {
				int lca=LCA(i,j);
				A[++b]=(node) {
					i,j,dis[i]+dis[j]-2*dis[lca],lca
				};
				Mx=max(Mx,dis[i]+dis[j]-2*dis[lca]);
			}
		sort(A+1,A+b+1);
		int l=0,r=Mx,ans=0;
		while(l<=r) {
			int mid=(l+r)>>1;
			if(chk(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
} p75;
bool f1,fl;
bool cur2;
int main() {
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	tot=0;
	f1=fl=1;
	memset(la,-1,sizeof(la));
	scanf("%d%d",&n,&m);
	for(int i=1; i<n; i++) {
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		if(b!=a+1)f1=0;
		if(a!=1)fl=0;
		add(a,b,c),add(b,a,c);
	}
	if(m==1)
		p20.solve();
	else if(f1)
		p40.solve();
	else if(fl)
		p60.solve();
	else
		p75.solve();
	return 0;
}
