#include<bits/stdc++.h>
#define M 105
using namespace std;
bool cur1;
int T,n,a[M];
struct P50 {
	int c[20];
	bool vis[1005];
	bool chk(int sz) {
		for(int i=0; i<=a[n]; i++)vis[i]=0;
		vis[0]=1;
		for(int i=1; i<=sz; i++) {
			int x=c[i];
			for(int j=a[x]; j<=a[n]; j++)
				if(vis[j-a[x]])vis[j]=1;
		}
		for(int i=1; i<=n; i++)if(!vis[a[i]])return false;
		return true;
	}
	void solve() {
		int ans=100;
		for(int i=0; i<(1<<n); i++) {
			int sz=0;
			for(int j=0; j<n; j++)
				if(i&(1<<j))c[++sz]=j+1;
			if(chk(sz))ans=min(ans,sz);
		}
		printf("%d\n",ans);
	}
} p50;
struct P100 {
	int ans;
	bool vis[25005];
	void solve() {
		ans=0;
		memset(vis,0,sizeof(vis));
		vis[0]=1;
		for(int i=1; i<=n; i++) {
			if(vis[a[i]])continue;
			ans++;
			for(int j=a[i]; j<=a[n]; j++)
				if(vis[j-a[i]])vis[j]=1;
		}
		printf("%d\n",ans);
	}
} p100;
bool cur2;
int main() {
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--) {
		scanf("%d",&n);
		for(int i=1; i<=n; i++)scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		if(n<=13)
			p50.solve();
		else
			p100.solve();
	}
	return 0;
}
