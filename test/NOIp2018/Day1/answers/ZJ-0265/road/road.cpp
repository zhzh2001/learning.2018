#include<bits/stdc++.h>
using namespace std;
int n,ans,b[100005],l[100005],r[100005];
struct node{int x,id;}a[100005];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	for (;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1;
	for (;isdigit(ch);ch=getchar()) x=(x+(x<<2)<<1)+(ch^48);
	return x*f;
}
bool cmp(const node &x,const node &y){return x.x>y.x;}
int max(int a,int b){return a>b?a:b;}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) b[i]=a[i].x=read(),a[i].id=i,l[i]=i-1,r[i]=i+1;
	sort(a+1,a+n+1,cmp);
	for (int i=1;i<=n;i++)
	{
		int t=a[i].id;
		ans+=b[t]-max(b[l[t]],b[r[t]]);
		l[r[t]]=l[t],r[l[t]]=r[t];
	}
	printf("%d",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
