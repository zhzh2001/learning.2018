#include<bits/stdc++.h>
using namespace std;
int n,T,ans,a[105];
bool b[25005];
int max(int a,int b){return a>b?a:b;}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		ans=n;
		memset(b,0,sizeof(b));
		b[0]=1;
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		for (int i=1;i<=n;i++)
		{
			if (b[a[i]]) ans--;
			else for (int j=a[i];j<=a[n];j++)
				if (b[j-a[i]]) b[j]=1;
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
