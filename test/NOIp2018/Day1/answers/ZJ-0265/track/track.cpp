#include<bits/stdc++.h>
using namespace std;
int n,m,l,r,tot,a[50005],ver[100005],edge[100005],nxt[100005],head[50005],f[50005],g[50005],v[50005][5];
bool fl=1;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	for (;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1;
	for (;isdigit(ch);ch=getchar()) x=(x+(x<<2)<<1)+(ch^48);
	return x*f;
}
int min(int x,int y){return x<y?x:y;}
int max(int x,int y){return x>y?x:y;}
void add(int x,int y,int z){ver[++tot]=y,edge[tot]=z,nxt[tot]=head[x],head[x]=tot;}
void dfs(int x,int p)
{
	for (int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];
		if (y!=p)
		{
			dfs(y,x);
			int z=f[y]+edge[i];
			r=max(r,f[x]+z);
			f[x]=max(f[x],z);
		}
	}
}
void dp(int x,int p,int s)
{
	for (int i=head[x];i;i=nxt[i])
	{
		int y=ver[i],z=edge[i];
		if (y!=p)
		{
			dp(y,x,s);
			f[x]+=f[y];
			if (g[y]+z>=s) f[x]++;
			else v[x][++v[x][0]]=g[y]+z;
		}
	}
	sort(v[x]+1,v[x]+4);
	if (v[x][1]+v[x][2]>=s) f[x]++,g[x]=v[x][3];
	else if (v[x][1]+v[x][3]>=s) f[x]++,g[x]=v[x][2];
	else if (v[x][2]+v[x][3]>=s) f[x]++,g[x]=v[x][1];
	else g[x]=v[x][3];
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<n;i++)
	{
		int x=read(),y=read(),z=read();
		if (x>1&&y>1) fl=0;
		l=l?min(l,z):z;
		add(x,y,z),add(y,x,z);
	}
	dfs(n>>1,0);
	if (m==1){printf("%d",r);fclose(stdin);fclose(stdout);return 0;}
	if (fl)
	{
		for (int i=1;i<n;i++) a[i]=edge[i<<1];
		sort(a+1,a+n);
		while (l<r)
		{
			int mid=l+r+1>>1,ans=0,j=1;
			for (int i=n-1;i>j;i--)
			{
				while (j<i&&a[j]+a[i]<mid) j++;
				if (j<i) ans++;
			}
			if (ans>=m) l=mid;
			else r=mid-1;	
		}
		printf("%d",l);
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	while (l<r)
	{
		int mid=l+r+1>>1;
		memset(f,0,sizeof(f));
		memset(g,0,sizeof(g));
		memset(v,0,sizeof(v));
		dp(n>>1,0,mid);
		if (f[n>>1]>=m) l=mid;
		else r=mid-1;
	}
	printf("%d",l);
	fclose(stdin);
	fclose(stdout);
	return 0; 
}
