#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++)
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
#define Out() (fwrite(St,1,Top,stdout))
using namespace std;
int Top,fn,nb[100];static char buf[1000000],St[1000000],*p1=buf,*p2=buf;
int T,n,a[105],allv,ans;
bool f[25005];
int read(){
	int ret=0;char ch=gt();
	while(ch>'9'||ch<'0') ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return ret;
}
void write(int x){fn=0;do nb[fn++]=x%10,x/=10;while(x);while(fn) pt(nb[--fn]+'0');}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		n=read();
		for(int i=1;i<=n;i++) a[i]=read();sort(a+1,a+1+n);allv=a[n];
		memset(f,0,sizeof f);f[0]=1;ans=0;
		for(int i=1;i<=n;i++){
			if(f[a[i]]) continue;
			ans++;
			for(int j=a[i];j<=allv;j++) f[j]|=f[j-a[i]];
		}
		write(ans);pt('\n');
	}
	return Out(),0;
}
