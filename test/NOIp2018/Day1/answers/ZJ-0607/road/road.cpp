#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++)
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
#define Out() (fwrite(St,1,Top,stdout))
using namespace std;
int Top,fn,nb[100];static char buf[1000000],St[1000000],*p1=buf,*p2=buf;
const int maxn=100005;
int n,lon,ans;
struct yu{
	int x,id;
	bool operator <(const yu b)const{return x<b.x||(x==b.x&&id<b.id);}
}a[maxn];
struct RMQ{
	int x,idL,idR;
}f[maxn][20];
int read(){
	int ret=0;char ch=gt();
	while(ch>'9'||ch<'0') ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return ret;
}
void write(int x){fn=0;do nb[fn++]=x%10,x/=10;while(x);while(fn) pt(nb[--fn]+'0');}
RMQ get(int lo,int ro){
	int loLR=log2(ro-lo+1),L=lo,R=ro-(1<<loLR)+1;RMQ p;
	if(f[L][loLR].x<f[R][loLR].x) p=f[L][loLR];
	else if(f[L][loLR].x>f[R][loLR].x) p=f[R][loLR];
	else p=(RMQ){f[L][loLR].x,f[L][loLR].idL,f[R][loLR].idR};
	return p;
}
void DFS(int L,int R,int lst){
	if(L>R) return;
	RMQ x=get(L,R);ans+=x.x-lst;
	if(L==R) return;
	for(int i=x.idL,j=L;i<=x.idR;i++) DFS(j,a[i].id-1,x.x),j=a[i].id+1;
	DFS(a[x.idR].id+1,R,x.x);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();lon=log2(n);
	for(int i=1;i<=n;i++) a[i]=(yu){read(),i};
	sort(a+1,a+1+n);
	for(int i=1;i<=n;i++) f[a[i].id][0]=(RMQ){a[i].x,i,i};
	for(int j=1;j<=lon;j++)
	for(int i=1,ti=n-(1<<j)+1,L,R;i<=ti;i++){
		L=i,R=i+(1<<j-1);
		if(f[L][j-1].x<f[R][j-1].x) f[i][j]=f[L][j-1];
		else if(f[L][j-1].x>f[R][j-1].x) f[i][j]=f[R][j-1];
		else f[i][j]=(RMQ){f[L][j-1].x,f[L][j-1].idL,f[R][j-1].idR};
	}
	DFS(1,n,0);
	write(ans);pt('\n');
	return Out(),0;
}
