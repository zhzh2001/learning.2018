#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++)
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
#define Out() (fwrite(St,1,Top,stdout))
using namespace std;
int Top,fn,nb[100];static char buf[1000000],St[1000000],*p1=buf,*p2=buf;
const int maxn=50005,maxe=100005;
int n,m,son[maxe],lnk[maxn],nxt[maxe],w[maxe],tot,f[maxn],vis[maxn],ans,A[maxn],B[maxn],all,HA,allall;
bool CK1=1,CK2=1,ff;
int read(){
	int ret=0;char ch=gt();
	while(ch>'9'||ch<'0') ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return ret;
}
void write(int x){fn=0;do nb[fn++]=x%10,x/=10;while(x);while(fn) pt(nb[--fn]+'0');}
void add_e(int x,int y,int z){son[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;w[tot]=z;}
void DFS(int x,int dep){
	vis[x]=1;f[x]=dep;int MAX1=0,MAX2=0;
	for(int j=lnk[x];j;j=nxt[j]) if(!vis[son[j]]){
		DFS(son[j],dep+w[j]);
		if(f[son[j]]>MAX2) MAX2=f[son[j]];if(MAX2>MAX1) swap(MAX1,MAX2);
	}
	f[x]=max(f[x],MAX1);ans=max(ans,MAX1+MAX2-(dep<<1));
}
bool cmpMAXtoMIN(const int &x,const int &y){return x>y;}
bool check(int x){
	for(int i=1,lst=1,tot;i<=m;i++){
		tot=0;while(lst<=n&&tot<x) tot+=A[lst++];
		if(tot<x) return 0;
	}
	return 1;
}
bool check_B(int x){
	int cnt=0;
	for(int i=1,lst=1;i<=n;i++) if(B[i]-B[lst]>=x) lst=i,cnt++;
	return cnt>=m;
}
int gttt(int x){return (x&1)?x+1:x-1;}
void DFS_(int x,int now,int cnt){
	if(ff) return;if(cnt>=m){ff=1;return;}
	if(cnt+allall<m) return;
	for(int j=lnk[x],jw;j;j=nxt[j]) if(!vis[j]){
		vis[j]=vis[gttt(j)]=1;allall--;
		jw=now+w[j];if(jw>=HA){for(int i=1;i<=n;i++) DFS_(i,0,cnt+1);}
		else DFS_(son[j],jw,cnt);
		vis[j]=vis[gttt(j)]=0;allall++;
	}
}
bool check_C(int x){
	HA=x;ff=0;memset(vis,0,sizeof vis);allall=n-1;
	for(int i=1;i<=n;i++) DFS_(i,0,0);
	return ff;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	for(int i=1,x,y,z;i<n;i++){
		x=read(),y=read(),z=read(),add_e(x,y,z),add_e(y,x,z),all+=z;
		if(x^1) CK1=0;if(y!=x+1) CK2=0;
	}
	if(m==1){
		DFS(1,0);
	}else
	if(CK1){
		for(int j=lnk[1];j;j=nxt[j]) A[++A[0]]=w[j];
		sort(A+1,A+1+n,cmpMAXtoMIN);
		int L=0,R=A[1]+A[2],mid;
		while(L<=R){
			mid=L+R>>1;
			if(check(mid)) ans=mid,L=mid+1;else R=mid-1;
		}
	}else
	if(CK2){
		for(int i=1;i<n;i++)
		for(int j=lnk[i];j;j=nxt[j]) if(son[j]==i+1) B[i+1]=B[i]+w[j];
		int L=0,R=B[n],mid;
		while(L<=R){
			mid=L+R>>1;
			if(check_B(mid)) ans=mid,L=mid+1;else R=mid-1;
		}
	}
	else{
		int L=0,R=all/m,mid;
		while(L<=R){
			mid=L+R>>1;
			if(check_C(mid)) ans=mid,L=mid+1;else R=mid-1;
		}
	}
	write(ans);pt('\n');
	return Out(),0;
}
