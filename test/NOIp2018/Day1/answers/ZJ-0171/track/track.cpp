#include<bits/stdc++.h>

using namespace std;

#define N 300000

int n,m,x,y,z,ans,xb,all,f[N],g[N],head[N],tot,d[N],q[N],fa[N];
struct edge{int v,l,nxt;}e[N];

void add(int x,int y,int z){
	e[++tot].v=y; e[tot].l=z; e[tot].nxt=head[x]; head[x]=tot;
}

void dfs(int u){
	f[u]=0; g[u]=0; q[++xb]=u;
	for (int i=head[u],v;i;i=e[i].nxt)
		if ((v=e[i].v)!=fa[u]){fa[v]=u; dfs(v);}
}

bool judge(int x){
	for (int i=n;i;--i){
		int u=q[i],t=0; f[u]=g[u]=0;
		for (int j=head[u],v;j;j=e[j].nxt)
			if ((v=e[j].v)!=fa[u]){
				f[u]+=f[v];
				d[++t]=g[v]+e[j].l;
			}
		sort(d+1,d+t+1);
		int num=0;
		for (int j=t,k=0;j>k;--j){
			if (d[j]>=x){++num; continue;}
			for (;k<j&&d[k]+d[j]<x;++k);
			if (k==j) break;
			++k; ++num;
		}
		f[u]+=num;
		if (f[u]>=m) return 1;
		int l=1,r=t;
		while (l<=r){
			int mid=(l+r)>>1,now=0;
			for (int j=t,k=0;j>k;--j){
				if (j==mid) continue;
				if (d[j]>=x){++now; continue;}
				for (;k<j&&d[k]+d[j]<x;++k);
				if (k==mid) ++k;
				if (k>=j) break;
				++k; ++now;
			}
			if (now==num){g[u]=d[mid]; l=mid+1;}
			else r=mid-1;
		}
	}
	return f[1]>=m;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;++i){
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z); add(y,x,z);
		all+=z;
	}
	dfs(1);
	int l=0,r=all/m;
	while (l<=r){
		int mid=(l+r)>>1;
		if (judge(mid)){ans=mid; l=mid+1;}
		else r=mid-1;
	}
	printf("%d\n",ans);
	
	return 0;
}
			
