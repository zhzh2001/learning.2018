#include<bits/stdc++.h>

using namespace std;

int n,m,x,y,z,ans,mx,tot,head[100000];
struct edge{int v,l,i,nxt;}e[100000];
bool vis[20];

void add(int x,int y,int z,int w){
	e[++tot].v=y; e[tot].l=z; e[tot].i=w; e[tot].nxt=head[x]; head[x]=tot;
}

void dfs(int u,int l){
	bool fl=0;
	for (int i=head[u];i;i=e[i].nxt)
		if (!vis[e[i].i]&&rand()%3==0){
			fl=1;
			vis[e[i].i]=1;
			dfs(e[i].v,l+e[i].l);
		}
	if (fl) return;
	mx=min(mx,l);
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.ans","w",stdout);
	srand((unsigned long long)new char+time(NULL));
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;++i){
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z,i); add(y,x,z,i);
	}
	for (int clk=1;clk<=2e8;++clk){
		memset(vis,0,sizeof vis);
		mx=1e9;
		for (int i=1;i<=m;++i){
			int x=rand()%n+1;
			dfs(x,0);
		}
		if (mx>ans) cerr<<mx<<endl;
		ans=max(ans,mx);
	}
	printf("%d\n",ans);
	cerr<<ans<<endl;
	
	return 0;
}
			
