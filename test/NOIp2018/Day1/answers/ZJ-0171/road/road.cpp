#include<bits/stdc++.h>

using namespace std;

#define LL long long
#define N 300000

LL n,ans,a[N];

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%lld",&n);
	for (LL i=1;i<=n;++i)
		scanf("%lld",a+i);
	for (LL i=1;i<=n;++i)
		ans+=max(0LL,a[i]-a[i-1]);
	printf("%lld\n",ans);
	
	return 0;
}
			
