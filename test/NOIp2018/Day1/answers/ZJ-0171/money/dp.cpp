#include<bits/stdc++.h>

using namespace std;

int t,n,ans,a[10000];
bool f[30000],g[30000];

int main(){
	freopen("money.in","r",stdin);
	freopen("money.ans","w",stdout);
	scanf("%d",&t);
	while (t--){
		scanf("%d",&n);
		for (int i=1;i<=n;++i) scanf("%d",a+i);
		memset(f,0,sizeof f); f[0]=1;
		for (int i=1;i<=n;++i)
			for (int j=a[i];j<=25000;++j)
				f[j]|=f[j-a[i]];
		memset(g,0,sizeof g); g[0]=1; ans=0;
		for (int i=0;i<=25000;++i)
			if (g[i]!=f[i]){
				++ans;
				for (int j=i;j<=25000;++j)
					g[j]|=g[j-i];
			}
		printf("%d\n",ans); cerr<<ans<<' ';
	}
	cerr<<endl;
	
	return 0;
}
			
