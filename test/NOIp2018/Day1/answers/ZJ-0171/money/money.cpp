#include<bits/stdc++.h>

using namespace std;

#define N 300
#define M 80000

int t,m,ans,n,a[N];
bool f[M];

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t); m=25000;
	while (t--){
		memset(f,0,sizeof f); f[0]=1;
		scanf("%d",&n);
		for (int i=1;i<=n;++i)
			scanf("%d",a+i);
		sort(a+1,a+n+1); ans=0;
		for (int i=1;i<=n;++i){
			if (f[a[i]]) continue;
			++ans;
			for (int j=a[i];j<=m;++j)
				f[j]|=f[j-a[i]];
		}
		printf("%d\n",ans);
	}
	
	return 0;
}
			
