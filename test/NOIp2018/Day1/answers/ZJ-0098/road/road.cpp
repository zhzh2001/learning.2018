#include<cstdio>
#include<string>
#include<cstring>
using namespace std;
const int maxn=100005;
int n,d[maxn],ans;
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) d[i]=read();
	int las=0;
	for (int i=1;i<=n;i++)
		if (d[i]>las) ans+=d[i]-las,las=d[i];
		else las=d[i];
	printf("%d\n",ans);
	return 0;
}
