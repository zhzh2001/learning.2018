#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=105,maxm=25005;
int T,n,a[maxn],ans;
bool f[maxm];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		memset(f,0,sizeof(f));
		f[0]=1; ans=0;
		for (int i=1;i<=n;i++){
			if (f[a[i]]) continue; else ans++;
			for (int j=a[i];j<=25000;j++)
			if (f[j-a[i]]) f[j]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
