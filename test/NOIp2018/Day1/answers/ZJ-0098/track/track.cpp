#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
#include<map>
using namespace std;
const int maxn=50005;
int n,m,cnt,ans,lnk[maxn],son[2*maxn],nxt[2*maxn],w[2*maxn],tot,sum;
int L[maxn],R[maxn],a[2*maxn];
int que[maxn];
map<int,int> s;
bool vis[maxn];
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void adde(int x,int y,int z){
	son[++tot]=y,w[tot]=z,nxt[tot]=lnk[x],lnk[x]=tot;
}
int dfs(int x,int fa,int pd){
	int n1=0;
	for (int j=lnk[x];j;j=nxt[j])
	if (son[j]!=fa) {
		int p=dfs(son[j],x,pd)+w[j];
		if (p>=pd) sum++;
		else n1++,a[L[x]-1+n1]=p;
	}
	for (int i=1;i<=n1;i++) que[i]=a[L[x]+i-1];
	s.clear();
	map<int,int>::iterator it;
	sort(que+1,que+1+n1);
	for (int i=1;i<=n1;i++) s[que[i]]++,vis[i]=0;
	for (int i=1;i<=n1;i++)
	if (s.find(que[i])!=s.end()){
		int p=pd-que[i];
		it=s.lower_bound(p);
		if (it==s.end()) continue;
		if ((it->first)==que[i]){
			if ((it->second)>1) {
				s[que[i]]-=2,sum++;
				if (s[que[i]]==0) s.erase(que[i]);
			}
		} else {
			sum++;
			s[que[i]]--; s[it->first]--;
			if (s[que[i]]==0) s.erase(que[i]);
			if (s[it->first]==0) s.erase(it->first);
		}
	}
	if (s.size()==0) return 0;
	it=s.end(); it--;
	return (it->first);
}
bool check(int x){
	sum=0;
	dfs(1,0,x);
	if (sum>=m) return 1; return 0;
}
void pre(int x,int fa){
	L[x]=cnt+1; R[x]=L[x]-1;
	for (int j=lnk[x];j;j=nxt[j])
	if (son[j]!=fa) R[x]++;
	cnt=R[x];
	for (int j=lnk[x];j;j=nxt[j])
	if (son[j]!=fa) pre(son[j],x);  
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	int l=0,r=0;
	for (int i=1;i<n;i++){
		int x=read(),y=read(),z=read();
		adde(x,y,z); adde(y,x,z); r+=z;
	}
	cnt=0; pre(1,0);
	while (l<=r){
		int mid=((r-l)>>1)+l;
		if (check(mid)) ans=mid,l=mid+1; else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
