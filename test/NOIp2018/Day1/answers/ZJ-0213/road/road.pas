var n,i,ans:longint;
a:array[-100..100050]of longint;
function find(l,r,s:longint):longint;
var i,min,k,x,y:longint;
begin
  min:=10001;
  for i:=l to r do
    begin
      if a[i]<min then begin min:=a[i]; k:=i; end;
    end;
  if k-1>=l then x:=find(l,k-1,min)
  else x:=0;
  if k+1<=r then y:=find(k+1,r,min)
  else y:=0;
  exit(x+y+min-s);
end;
begin
  assign(input,'road.in');
  reset(input);
  assign(output,'road.out');
  rewrite(output);
  read(n);
  for i:=1 to n do
    begin
      read(a[i]);
    end;
  writeln(find(1,n,0));
  close(input);
  close(output);
end.
                                               l