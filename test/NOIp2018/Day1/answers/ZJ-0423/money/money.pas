var
  o,n,i,ans,j,tot,t,max,k:longint;
  bo:boolean;
  a,b:array[0..110] of longint;
  flag:array[0..400000] of boolean;

begin
  assign(input,'money.in');
  assign(output,'money.out');
  reset(input);
  rewrite(output);

  readln(o);
  while o>0 do
    begin
      dec(o);
      readln(n);
      for i:=1 to n do read(a[i]);
      readln;
      ans:=1;
      for i:=1 to n-1 do
        for j:=i+1 to n do
          if a[i]>a[j] then
            begin
              t:=a[i];a[i]:=a[j];a[j]:=t;
            end;
      if a[1]=1 then
        begin
          writeln(1);
          continue;
        end;
      tot:=0;
      fillchar(flag,sizeof(flag),0);
      for i:=1 to n do
        if not flag[i] then
          begin
            inc(tot);
            b[tot]:=a[i];
            flag[i]:=true;
            for j:=i+1 to n do
              if a[j] mod a[i]=0 then flag[j]:=true;
          end;
      max:=b[tot];
      fillchar(flag,sizeof(flag),0);
      for i:=1 to tot do
        begin
          t:=b[i];
          while t<=max do
            begin
              flag[t]:=true;
              t:=t+b[i];
            end;
        end;
      for i:=2 to tot do
        begin
          bo:=false;
          for j:=b[1] to b[i] div 2+1 do
            begin
              if flag[j] and flag[b[i]-j] then
                begin
                  bo:=true;
                  break;
                end;
            end;
        end;
      if not bo then inc(ans);
      writeln(ans);
    end;

  close(input);
  close(output);
end.
