var
  n,i:longint;
  ans:int64;
  a:array[0..100000] of longint;
  pos,minn,flag:array[0..600000] of longint;

procedure build(k,l,r:longint);
var
  mid:longint;
begin
  if l=r then
    begin
      minn[k]:=a[l];
      pos[k]:=l;
      exit;
    end;
  mid:=(l+r) div 2;
  build(k*2,l,mid);
  build(k*2+1,mid+1,r);
  if minn[k*2]<minn[k*2+1] then
    begin
      minn[k]:=minn[k*2];pos[k]:=pos[k*2];
    end
    else
    begin
      minn[k]:=minn[k*2+1];pos[k]:=pos[k*2+1];
    end;
end;

procedure pushdown(k:longint);
begin
  if flag[k]<>0 then
    begin
      flag[k*2]:=flag[k*2]+flag[k];
      flag[k*2+1]:=flag[k*2+1]+flag[k];
      minn[k*2]:=minn[k*2]+flag[k];
      minn[k*2+1]:=minn[k*2+1]+flag[k];
      flag[k]:=0;
    end;
end;

function query(l,r,x,y,k:longint):longint;
var
  mid,pos1,pos2:longint;
begin
  if (x<=l) and (y>=r) then exit(k);
  mid:=(l+r) div 2;
  pushdown(k);
  pos1:=-1;pos2:=-1;
  if x<=mid then pos1:=query(l,mid,x,y,k*2);
  if y>mid then pos2:=query(mid+1,r,x,y,k*2+1);
  if pos1=-1 then exit(pos2);
  if pos2=-1 then exit(pos1);
  if minn[pos1]<minn[pos2] then exit(pos1)
    else exit(pos2);
end;

procedure change(l,r,x,y,k,v:longint);
var
  mid:longint;
begin
  if (x<=l) and (y>=r) then
    begin
      minn[k]:=minn[k]-v;
      flag[k]:=flag[k]-v;
      exit;
    end;
  mid:=(l+r) div 2;
  {pushdown(k);}
  if x<=mid then change(l,mid,x,y,k*2,v);
  if y>mid then change(mid+1,r,x,y,k*2+1,v);
  if minn[k*2]<minn[k*2+1] then
    begin
      minn[k]:=minn[k*2];pos[k]:=pos[k*2];
    end
    else
    begin
      minn[k]:=minn[k*2+1];pos[k]:=pos[k*2+1];
    end;
end;

procedure dfs(l,r:longint);
var
  ask:longint;
begin
  if l>r then exit;
  if l=r then
    begin
      ans:=ans+minn[query(1,n,l,l,1)];
      exit;
    end;
  ask:=query(1,n,l,r,1);
  if minn[ask]=0 then
    begin
      if l<pos[ask] then dfs(l,pos[ask]-1);
      if r>pos[ask] then dfs(pos[ask]+1,r);
      exit;
    end;
  ans:=ans+minn[ask];
  change(1,n,l,r,1,minn[ask]);
  if l<pos[ask] then dfs(l,pos[ask]-1);
  if pos[ask]<r then dfs(pos[ask]+1,r);
end;

begin
  assign(input,'road.in');
  assign(output,'road.out');
  reset(input);
  rewrite(output);

  readln(n);
  for i:=1 to n do read(a[i]);
  build(1,1,n);
  ans:=0;
  dfs(1,n);
  write(ans);

  close(input);
  close(output);
end.
