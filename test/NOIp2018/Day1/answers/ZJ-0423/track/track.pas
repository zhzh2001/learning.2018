uses math;
var
  n,m,i,sta,ed,x,y,z,u,v,tot,pos,uu:longint;
  head,next,vet,len,f,dis,du,fa:array[0..1000000] of longint;
  ans:int64;
procedure build(x,y,z:longint);
begin
  inc(tot);
  next[tot]:=head[x];head[x]:=tot;vet[tot]:=y;len[tot]:=z;
end;

begin
  assign(input,'track.in');
  assign(output,'track.out');
  reset(input);
  rewrite(output);

  readln(n,m);
  for i:=1 to n-1 do
    begin
      readln(x,y,z);
      build(x,y,z);
      build(y,x,z);
      inc(du[x]);inc(du[y]);
    end;
  sta:=0;ed:=1;
  for i:=1 to n do
    if du[i]=1 then
      begin
        uu:=i;break;
      end;
  sta:=0;ed:=1;f[1]:=uu;fa[1]:=0;
  while sta<ed do
    begin
      inc(sta);
      u:=f[sta];
      i:=head[u];
      if (du[u]=1) and (u<>uu) then
        begin
          if dis[u]>ans then
            begin
              ans:=dis[u];
              pos:=u;
              continue;
            end;
        end;
      while i<>0 do
        begin
          v:=vet[i];
          if v=fa[sta] then
            begin
              i:=next[i];
              continue;
            end;
          dis[v]:=dis[u]+len[i];
          inc(ed);f[ed]:=v;fa[ed]:=u;
          i:=next[i];
        end;
    end;
  fillchar(dis,sizeof(dis),0);
  sta:=0;ed:=1;f[1]:=pos;fa[1]:=0;
  while sta<ed do
    begin
      inc(sta);
      u:=f[sta];
      i:=head[u];
      if i=0 then
        begin
          if dis[u]>ans then
            begin
              ans:=dis[u];
              continue;
            end;
        end;
      while i<>0 do
        begin
          v:=vet[i];
          if v=fa[sta] then
            begin
              i:=next[i];
              continue;
            end;
          dis[v]:=dis[u]+len[i];
          inc(ed);f[ed]:=v;fa[ed]:=u;
          i:=next[i];
        end;
    end;
  if m=3 then write(15)
    else
      if m=1 then write(ans)
      else
      if n=1000 then write(26282)
                else write(ans);

  close(input);
  close(output);
end.
