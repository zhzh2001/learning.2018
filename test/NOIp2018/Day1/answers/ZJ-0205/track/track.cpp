#include <iostream>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <cstdio>
#include <vector>
#include <queue>
#include <map>
#include <set>
using namespace std;
int n,m,top=-1,dis[50011],vis[50011],head,tail,mi=1e9;
struct edge
{
	int st,ed,val;
	edge* nest;
}e[100111],*v[50011];
struct que{int num,dist;}qu[50011];
void addedge(int st,int ed,int val)
{
	e[++top].st=st;
	e[top].ed=ed;
	e[top].val=val;
	e[top].nest=v[st];
	v[st]=&e[top];
}
void bfs(int nowst,int &pp)
{
	memset(dis,0,sizeof dis);
	memset(vis,0,sizeof vis);
	edge* ne;dis[nowst]=0;vis[nowst]=1;
	head=tail=0;
	qu[++tail].num=nowst;qu[tail].dist=0;
	int now,nowdis;
	while(head<tail)
	{
		head++;now=qu[head].num;nowdis=qu[head].dist;
		dis[now]=nowdis;
		for(ne=v[now];ne;ne=ne->nest)
		{
			if(vis[ne->ed]) continue;
			vis[ne->ed]=1;qu[++tail].dist=nowdis+ne->val;
			qu[tail].num=ne->ed;
		}
	}
	pp=nowst;
	for(int i=1;i<=n;i++) if(dis[i]>dis[pp]) pp=i;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int i,j,from,to,cc,pos1,pos2;
	scanf("%d%d",&n,&m);
	for(i=1;i<=n-1;i++)
	{
		scanf("%d%d%d",&from,&to,&cc);
		addedge(from,to,cc);
		addedge(to,from,cc);
		if(cc<mi) mi=cc;
	}
	if(m==n-1)
	{
		printf("%d",mi);
		return 0;
	}
	bfs(1,pos1);
	bfs(pos1,pos2);
	printf("%d",dis[pos2]);
	return 0;
}
