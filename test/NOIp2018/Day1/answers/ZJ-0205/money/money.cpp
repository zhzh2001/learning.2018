#include <iostream>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <cstdio>
#include <vector>
#include <queue>
#include <map>
#include <set>
using namespace std;
int n,vis[111],t,ans=0;
int d[111],dp[30011],ma,isc[30011];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int i,j,k;
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d",&n);
		memset(vis,0,sizeof vis);ma=0;
		memset(isc,0,sizeof isc);
	    for(i=1;i<=n;i++)scanf("%d",&d[i]);
	    sort(d+1,d+n+1);
	    n=unique(d+1,d+n+1)-d-1;
	    for(i=1;i<=n;i++)isc[d[i]]=i;
	    for(i=1;i<=n;i++) if(d[i]>ma) ma=d[i];
	    memset(dp,0,sizeof dp);
	    dp[0]=1;
	    for(i=1;i<=ma;i++)
		{
			if(isc[i])
			{
				for(j=1;j<=n;j++)
				{
					if(j==isc[i]) continue;
					if(i-d[j]>=0) dp[i]|=dp[i-d[j]];
					if(dp[i]) vis[isc[i]]=1;
				}
				dp[i]=1;
			}
			else for(j=1;j<=n;j++)if(i-d[j]>=0) dp[i]|=dp[i-d[j]];
		}
		ans=0;
		for(i=1;i<=n;i++) if(!vis[i]) ans++;
		printf("%d\n",ans);
	}
	return 0;
}
