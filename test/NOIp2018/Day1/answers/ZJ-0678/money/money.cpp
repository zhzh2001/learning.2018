#include<bits/stdc++.h>
#define N 105
using namespace std;
const int Maxn=25000;
int a[N];
bool vis[Maxn+10];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T,n;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		int ans=0;
		memset(vis,0,sizeof vis);
		vis[0]=1;
		for(int i=1;i<=n;i++){
			if(vis[a[i]])continue;
			ans++;
			for(int j=a[i];j<=Maxn;j++){
				if(vis[j-a[i]])vis[j]=1;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
