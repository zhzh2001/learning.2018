#include<bits/stdc++.h>
#define N 500010
using namespace std;
int n,m,a,b,c;
int fir[N],nex[N*2],val[N*2],point[N*2],e=1;
void add(int a,int b,int c){
	point[++e]=b,val[e]=c,nex[e]=fir[a],fir[a]=e;
}
int num=0;
int s[N],tot,op[N];
bool vis[N];
int dep[N];
void DFS(int x,int fa,int hh){
	for(int i=fir[x];i!=-1;i=nex[i]){
		int to=point[i];
		if(to==fa)continue;
		DFS(to,x,hh);
	}
	tot=0;
	for(int i=fir[x];i!=-1;i=nex[i]){
		int to=point[i],cost=val[i];
		if(to==fa)continue;
		s[++tot]=dep[to]+cost;
		vis[tot]=0,op[tot]=0;
	}
//	cout<<"!"<<x<<" "<<tot<<endl;
//	for(int i=1;i<=tot;i++)cout<<s[i]<<" ";cout<<endl;
	sort(s+1,s+tot+1);
	while(tot&&s[tot]>=hh)num++,tot--;
	int g=tot;
	for(int i=1;i<g;i++){
		if(s[i]+s[g]<hh)continue;
		vis[i]=vis[g]=1,op[g]=i;
		g--;
		num++;
	}
	for(int i=g+1;i<=tot;i++){
		if(op[i]){
			if(!vis[i-1]&&s[op[i]]+s[i-1]>=hh){
				op[i-1]=op[i],vis[i-1]=1;
				vis[i]=0,op[i]=0;
			}
		} 
	}
	dep[x]=0;
	for(int i=tot;i>=1;i--){
		if(!vis[i]){
			dep[x]=s[i];
			break;
		}
	}
//	cout<<dep[x]<<" "<<num<<endl;
}
bool judge(int x){
	num=0;
	DFS(1,-1,x);
//	cout<<"!!!"<<num<<endl;
	return num>=m;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(fir,-1,sizeof fir);
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&a,&b,&c);
		add(a,b,c),add(b,a,c);
	}
	int L=1,R=1e9,ans=-1;
	while(L<=R){
		int mid=(L+R)>>1;
		if(judge(mid))L=mid+1,ans=mid;
		else R=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
