#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x,a; char ch;
	ch=getchar(); x=1;
	while (ch>'9' || ch<'0'){
		if (ch=='-') x=-1;
		ch=getchar();
	}
	a=0;
	while (ch<='9' && ch>='0'){
		a=a*10+(ch-'0'); ch=getchar();
	}
	return a*x;
}
inline int gcd(int x,int y){
	if (y==0) return x;
	return gcd(y,x%y);
}
int ans,n,k,m;
int a[105],b[105];
bool vis[105],f[25005];
int main(){
	freopen("money.in","r",stdin); freopen("money.out","w",stdout);
	int T=read();
	while (T--){
		
		n=read();
		for (int i=1;i<=n;i++){
			a[i]=read(); //k=gcd(a[i],k); 
			vis[i]=false;
		}
		
		sort(a+1,a+n+1);
		for (int i=1;i<n;i++){
			for (int j=i+1;j<=n;j++){
				if (vis[j]) continue;
				if (a[j]%a[i]==0) vis[j]=true;
			}
		}
		m=0;
		for (int i=1;i<=n;i++){
			if (!vis[i]) b[++m]=a[i];
		}
		
		k=0;
		for (int i=1;i<=m;i++){
			k=gcd(k,b[i]);
		}
		bool fs=false;
		for (int i=1;i<=m;i++){
			b[i]/=k; //maxs=max(maxs,b[i]);
			if (b[i]==1){
				printf("1\n"); fs=true; break;
			}
		}
		if (fs) continue;
		
		sort(b+1,b+m+1); 
		f[0]=true; for (int i=1;i<=b[m];i++) f[i]=false;
		ans=0;
		for (int i=1;i<=m;i++){
			if (f[b[i]]) continue;
			ans++;
			int nowk=b[m]-b[i];
			for (int j=0;j<=nowk;j++){
				if (f[j]) f[j+b[i]]=true;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}

