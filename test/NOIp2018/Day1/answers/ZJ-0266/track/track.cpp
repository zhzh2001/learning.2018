#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x,a; char ch;
	ch=getchar(); x=1;
	while (ch>'9' || ch<'0'){
		if (ch=='-') x=-1;
		ch=getchar();
	}
	a=0;
	while (ch<='9' && ch>='0'){
		a=a*10+(ch-'0'); ch=getchar();
	}
	return a*x;
}
struct edge_arr{
	int to,next,l;
}edge[100005];
int head[500005],num=0;
inline void lianshi(int u,int v,int l){
	edge[++num].to=v; edge[num].l=l; edge[num].next=head[u]; head[u]=num;
}
int dep[50005],son_num[50005],heavy_son[50005],father[50005],maxs[50005][2],ans;
inline void find_d(int now,int fa){
	dep[now]=dep[fa]+1; father[now]=fa; son_num[now]=1; heavy_son[now]=0;
	maxs[now][0]=0; maxs[now][1]=0;
	for (int i=head[now];i;i=edge[i].next){
		if (edge[i].to!=fa){
			find_d(edge[i].to,now);
			son_num[now]+=son_num[edge[i].to];
			if (!heavy_son[now] || son_num[heavy_son[now]]<son_num[edge[i].to]) heavy_son[now]=edge[i].to;
			if (maxs[now][0]<=maxs[edge[i].to][0]+edge[i].l){
				maxs[now][1]=maxs[now][0];
				maxs[now][0]=maxs[edge[i].to][0]+edge[i].l;
			}
			else{
				if (maxs[now][1]<=maxs[edge[i].to][0]+edge[i].l){
					maxs[now][1]=maxs[edge[i].to][0]+edge[i].l;
				}
			}
		}
	}
	ans=max(ans,maxs[now][0]+maxs[now][1]);
}
int top[50005];
inline void dfs2(int now,int t){
	top[now]=t;
	if (!heavy_son[now]) return;
	dfs2(heavy_son[now],t);
	for (int i=head[now];i;i=edge[i].next){
		if (edge[i].to!=father[now] && edge[i].to!=heavy_son[now]){
			dfs2(edge[i].to,edge[i].to);
		}
	}
}
int n,m,l[50005],mid;
inline bool judge(int now){
	int nows=0,sums=0;
	for (int i=1;i<n;i++){
		nows+=l[i];
		if (nows>=now){
			nows=0; sums++; if (sums>=m) return true;
		}
	}
	nows=0,sums=0;
	for (int i=n-1;i>=1;i--){
		nows+=l[i];
		if (nows>=now){
			nows=0; sums++; if (sums>=m) return true;
		}
	}
	return false;
}
inline bool judge2(int now){
	int i=1,j=n-1,sums=0;
	while (i<=j){
		while (i<j && l[i]+l[j]<now) i++;
		//if (l[j]<now && i==j) sums++;
		if (l[j]<now && i==j) break;
		sums++; 
		if (l[j]<now) i++; 
		j--;
	}
	if (sums>=m) return true;
	return false;
}
int dp[50005],lson[50005],rson[50005],ls[50005],rs[50005];
inline void dfs_judge(int now,int k){
	if (now==0) return;
	maxs[now][0]=0; maxs[now][1]=0;
	dfs_judge(lson[now],k); dfs_judge(rson[now],k);
	dp[now]=dp[lson[now]]+dp[rson[now]];
	maxs[now][0]=maxs[lson[now]][0]+ls[now];
	maxs[now][1]=maxs[rson[now]][0]+rs[now];
	if (maxs[now][0]>=k){
		maxs[now][0]=0; dp[now]++;
	}
	if (maxs[now][1]>=k){
		maxs[now][1]=0; dp[now]++;
	}
	if (maxs[now][0]+maxs[now][1]>=k){
		maxs[now][0]=0; maxs[now][1]=0; dp[now]++;
	}
	maxs[now][0]=max(maxs[now][0],maxs[now][1]);
}
inline bool judge3(int now){
	for (int i=0;i<=n;i++) dp[i]=0,maxs[i][0]=0,maxs[i][1]=0;
	dfs_judge(1,now);
	if (dp[1]>=m) return true;
	return false;
}

int b[50005];
inline void dfs_judge2(int now,int k){
	maxs[now][0]=0; 
	int nums=0; dp[now]=0;
	for (int i=head[now];i;i=edge[i].next){
		if (edge[i].to!=father[now]){
			dfs_judge2(edge[i].to,k);
			b[++nums]=maxs[edge[i].to][0]+edge[i].l; dp[now]+=dp[edge[i].to];
		}
	}
	if (nums>0) sort(b+1,b+nums+1);
	for (;nums&&b[nums]>=k;nums--) dp[now]++;
	if (nums>0){
		int i=1,j=nums;
		while (i<j){
			while (i<j && b[i]+b[j]<k){
				maxs[now][0]=b[i];
				i++;
			}
			//if (l[j]<now && i==j) sums++;
			if (i==j) break;
			dp[now]++; 
			i++; j--;
		}
		if (i==j) maxs[now][0]=b[j];
	}
}
inline bool judge4(int now){
	//for (int i=0;i<=n;i++) dp[i]=0;
	dp[0]=0;
	dfs_judge2(1,now);
	//for (int i=1;i<=n;i++) printf("i=%d   dp=%d   maxs=%d\n",i,dp[i],maxs[i][0]);
	if (dp[1]>=m) return true;
	return false;
}

int main(){
	freopen("track.in","r",stdin); freopen("track.out","w",stdout);
	n=read(),m=read();bool fs=true,fs2=true,fs3=true; //return 0;
	for (int i=1;i<n;i++){
		int u=read(),v=read(); l[i]=read();
		if (v!=u+1){
			fs=false;
		}
		if (u!=1){
			fs2=false;
		}
		lianshi(u,v,l[i]); lianshi(v,u,l[i]);
	}
	dep[0]=0; ans=0; //return 0;
	find_d(1,0); //return 0;
	dfs2(1,1); //return 0;
	if (m==1){
		printf("%d\n",ans); return 0;
	}
	int L=0,R=ans;
	if (fs){
		while (L<=R){
			mid=L+R>>1;
			if (judge(mid)) L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",R); return 0;
	}
	if (fs2){
		sort(l+1,l+n);
		//for (int i=1;i<n;i++) printf("%d\n",l[i]); return 0;
		while (L<=R){
			mid=L+R>>1;
			if (judge2(mid)) L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",R); return 0;
	}
	for (int i=1;i<=n;i++){
		int nums=0;
		for (int j=head[i];j;j=edge[j].next){
			nums++;
			if (edge[j].to!=father[i]){
				if (lson[i]) rson[i]=edge[j].to,rs[i]=edge[j].l;
				else lson[i]=edge[j].to,ls[i]=edge[j].l;
			}
		}
		if (nums>3){
			fs3=false; break;
		}
	}
	if (fs3){
		//for (int i=1;i<=n;i++) printf("%d %d\n",lson[i],rson[i]);
		while (L<=R){
			mid=L+R>>1;
			if (judge3(mid)) L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",R); return 0;
	}
	
	//printf("%d\n",judge4(15));
	//return 0;
	while (L<=R){
		mid=L+R>>1;
		if (judge4(mid)) L=mid+1;
		else R=mid-1;
	}
	printf("%d\n",R);
	return 0;
}

