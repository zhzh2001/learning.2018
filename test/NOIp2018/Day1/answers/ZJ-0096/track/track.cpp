#include<iostream>
#include<cstdio>
using namespace std;
struct tree{
	int to,nxt,w;
}edge[100006];
int n,m,u,v,w,cnt=0,point[50006],father[50006],ma=0;
long long dp[50006][4];
inline void dfs1(int now,int fa)
{
	int u;
	for (int i=point[now];i;i=edge[i].nxt)
	{
		u=edge[i].to;
		if (u!=fa)
		{
			dfs1(u,now);
			if (dp[now][0]<dp[u][0]+edge[i].w) 
			{
				dp[now][2]=dp[now][0];
				dp[now][0]=dp[u][0]+edge[i].w;
				dp[now][1]=u;
			}
			else
			{
				if (dp[now][2]<dp[u][0]+edge[i].w)
				{
					dp[now][2]=dp[u][0]+edge[i].w;
					dp[now][3]=u;
				}
			}
		}
		else
		{
			father[now]=i;
		}
	}
}
inline void dfs2(int now,int fa)
{
	if (fa!=0)
	{
		if (dp[fa][1]!=now)
		{
			if (dp[now][0]<(dp[fa][0]+edge[father[now]].w))
			{
				dp[now][0]=dp[fa][0]+edge[father[now]].w;
				dp[now][1]=0;
			}
		}
		else
		{
			if (dp[now][0]<(dp[fa][2]+edge[father[now]].w))
			{
				dp[now][0]=dp[fa][2]+edge[father[now]].w;
				dp[now][1]=0;
			}
		}
	}
	int u;
	for (int i=point[now];i;i=edge[i].nxt)
	{
		u=edge[i].to;
		if (u!=fa)
		{
			dfs2(u,now);
			
		}
	}
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++)
	{
		scanf("%d%d%d",&u,&v,&w);
		cnt++;
		edge[cnt].nxt=point[u];
		point[u]=cnt;
		edge[cnt].to=v;
		edge[cnt].w=w;
		cnt++;
		edge[cnt].nxt=point[v];
		point[v]=cnt;
		edge[cnt].to=u;
		edge[cnt].w=w;
	}
	if (m==1)
	{
		dfs1(1,0);
		dfs2(1,0);
		for (int i=1;i<=n;i++)
		{
			if (ma<dp[i][0]) ma=dp[i][0];
		}
		printf("%d\n",ma);
	}
	return 0;
}
