#include <cstdio>
int N, f[1000001];
int main()
{
	f[0] = 1;
	scanf("%d", &N);
	for (int i = 0, x; i < N; i++)
	{
		scanf("%d", &x);
		for (int j = x; j <= 1000000; j++)
			f[j] |= f[j - x];
	}
	for (int j = 0; j <= 1000000; j++)
		if (!f[j])
			printf("%d\n", j);
	return 0;
}
