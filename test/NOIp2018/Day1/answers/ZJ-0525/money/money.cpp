#include <cstdio>
#include <algorithm>
int TC;
int N, a[100], f[25001];
int main()
{
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	for (scanf("%d", &TC); TC--; )
	{
		scanf("%d", &N);
		for (int i = 0; i < N; i++)
			scanf("%d", a + i);
		std::sort(a, a + N);
		N = std::unique(a, a + N) - a;
		for (int i = 0; i <= 25000; i++)
			f[i] = 0;
		f[0] = 1;
		int ans = 0;
		for (int i = 0; i < N; i++)
			if (!f[a[i]])
			{
				ans++;
				for (int j = a[i]; j <= 25000; j++)
					f[j] |= f[j - a[i]];
			}
		printf("%d\n", ans);
	}
	return 0;
}
