#include <cstdio>
#include <algorithm>
int N, last, cur, O;
int main()
{
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &N);
	for (int i = 1; i <= N; i++)
	{
		scanf("%d", &cur);
		O += std::max(0, cur - last);
		last = cur;
	}
	printf("%d\n", O);
	return 0;
}
