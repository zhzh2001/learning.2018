#include <cstdio>
#include <algorithm>
int N, M;
int head[50001], next[99999], to[99999], len[99999], E;
int q[50001], fa[50001];
int f[50001], g[50001];
int p[50001], L;
int left[50001], right[50001], vi[50001];
bool check(int mid)
{
	for (int i = N; i; i--)
	{
		int u = q[i];
		f[u] = 0;
		L = 0;
		for (int e = head[u]; e; e = next[e])
			if (to[e] != fa[u])
			{
				f[u] += f[to[e]];
				if (g[to[e]] + len[e] >= mid)
					f[u]++;
				else
					p[++L] = g[to[e]] + len[e];
			}
		std::sort(p + 1, p + L + 1);
		for (int j = 0; j <= L; j++)
		{
			left[j + 1] = j;
			right[j] = j + 1;
			vi[j] = 0;
		}
		for (int l = 1, r = L + 1; l <= L; )
		{
			if (l == r)
				r = right[r];
			else
			{
				while (left[r] > l && p[l] + p[left[r]] >= mid)
					r = left[r];
			}
			if (r == L + 1)
				l = right[l];
			else
			{
				f[u]++;
				right[left[r]] = right[r];
				left[right[r]] = left[r];
				r = right[r];
				right[left[l]] = right[l];
				left[right[l]] = left[l];
				l = right[l];
			}
		}
		g[u] = p[left[L + 1]];
	}
	return f[1] >= M;
}
int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &N, &M);
	for (int i = 1, u, v, l; i < N; i++)
	{
		scanf("%d%d%d", &u, &v, &l);
		next[++E] = head[u], to[E] = v, len[E] = l, head[u] = E;
		next[++E] = head[v], to[E] = u, len[E] = l, head[v] = E;
	}
	int H = 0, T = 1, u;
	q[1] = 1;
	while (H < T)
		for (int e = head[u = q[++H]]; e; e = next[e])
			if (to[e] != fa[u])
				fa[q[++T] = to[e]] = u;
	int l = 1, r = (N - 1) * 10000;
	while (l < r)
	{
		int m = l + r + 1 >> 1;
		if (check(m))
			l = m;
		else
			r = m - 1;
	}
	printf("%d\n", l);
	return 0;
}
