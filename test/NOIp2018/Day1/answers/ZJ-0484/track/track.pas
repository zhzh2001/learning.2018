var n,m,i,j,k,x,y,z,tot,l,r,u,v,e,point,maxn,mid,ans,minn,root,cnt:longint;
flag,flagg:boolean;
head,a,b,c,q,dist,d:array[0..60005] of longint;
next,vet,len:Array[0..110005] of longint;
mark:array[0..60005] of boolean;
dp:array[0..60005] of longint;
//xlb
 
function maxx(a,b:longint):longint;
begin if a>B then exit(a);exit(b);end;

   procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]>x do
            inc(i);
           while x>a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;

procedure add(x,y,z:longint);
begin inc(tot);next[tot]:=head[x];head[x]:=tot;vet[tot]:=y;len[tot]:=z;end;

procedure special_1;  //m=1
begin
l:=0;r:=1;q[1]:=1;dist[1]:=0;mark[1]:=true;
while l<r do begin
	inc(l);
	u:=q[l];
	e:=head[u];
	while e<>0 do begin
		v:=vet[e];
		if not mark[v] then begin
			mark[v]:=true;
			inc(r);
			q[r]:=v;
			dist[r]:=dist[l]+len[e];
			if dist[r]>maxn then begin
				maxn:=dist[r];
				point:=v;
				end;
			end;
		e:=next[e];
		end;
	end;
l:=0;r:=1;q[1]:=point;dist[1]:=0;
for i:=1 to n do mark[i]:=false;mark[point]:=true;
while l<r do begin
	inc(l);
	u:=q[l];
	e:=head[u];
	while e<>0 do begin
		v:=vet[e];
		if not mark[v] then begin
			mark[v]:=true;
			inc(r);
			q[r]:=v;
			dist[r]:=dist[l]+len[e];
			if dist[r]>maxn then begin
				maxn:=dist[r];
				point:=v;
				end;
			end;
		e:=next[e];
		end;
	end;
writeln(maxn);
end;

function checkk(x:longint):boolean;
var i,j,count:longint;
begin
i:=1;j:=n-1;
count:=0;
while i<=j do
	if a[i]>=x then begin inc(i);inc(count);end
	else if i=j then inc(i) else
	if a[i]+a[j]>=x then begin inc(i);dec(j);count:=count+1;end
	else dec(j);
if count>=m then exit(true);
exit(false);
end;

procedure special_2;//ai=1
begin
sort(1,n);
l:=0;r:=500000000;
while l<=r do begin
	mid:=(l+r)>>1;
	if checkk(mid) then begin ans:=mid;l:=mid+1;end
		else r:=mid-1;
	end;
writeln(ans);
end;

function check(x:longint):boolean;
var sum,count:longint;
begin
sum:=0;count:=0;
for i:=1 to n-1 do begin
	sum:=sum+a[i];
	if sum>=x then begin inc(count);sum:=0;end;
	end;
if count>=m then exit(true);
exit(false);
end;

procedure special_3;//bi=ai+1
begin
l:=0;r:=500000000;
while l<=r do begin
	mid:=(l+r)>>1;
	if check(mid) then begin ans:=mid;l:=mid+1;end
		else r:=mid-1;
	end;
writeln(ans);
end;

procedure special_4;//m=n-1
begin
writeln(minn);
end;

procedure dfs(u,fa:longint);
var e,v,left,right,lenleft,lenright:longint;
begin
e:=head[u];
left:=0;right:=0;dp[u]:=0;
while e<>0 do begin
	v:=vet[e];
	if v<>fa then if left=0 then begin left:=v;lenleft:=len[e];end else begin right:=v;lenright:=len[e];end;
	e:=next[e];
	end;
if left>0 then
dfs(left,u);
if right>0 then
dfs(right,u);
if right>0 then begin
	if (dp[left]+lenleft>=y)and(dp[right]+lenright>=y) then inc(cnt,2) else
	if dp[left]+lenleft>=y then begin inc(cnt);dp[u]:=dp[right]+lenright;end else
	if dp[right]+lenright>=y then begin inc(cnt);dp[u]:=dp[left]+lenleft;end else
	if dp[right]+dp[left]+lenleft+lenright>=y then inc(cnt) else
		dp[u]:=maxx(dp[right]+lenright,dp[left]+lenleft);
	end else if left>0 then begin
	if dp[left]+len[left]>=y then inc(cnt)
		else dp[u]:=dp[left]+lenleft;
	end;
end;

function checkkk(x:longint):boolean;
begin
cnt:=0;
y:=x;
dfs(root,0);
if cnt>=m then exit(true) else exit(false);
end;

procedure special_5; //no choices
begin
for i:=1 to n do if d[i]=1 then break;
root:=i;
l:=0;r:=500000000;
while l<=r do begin
	mid:=(l+r)>>1;
	if checkkk(mid) then begin ans:=mid;l:=mid+1;end
		else r:=mid-1;
	end;
writeln(ans);
end;

begin
assign(input,'track.in');
assign(output,'track.out');
reset(input);
rewrite(output);
readln(n,m);
flag:=true;
flagg:=true;
minn:=maxlongint;
for i:=1 to n-1 do begin
	readln(x,y,z);
	if x<>1 then flagg:=false;
	if y<>x+1 then flag:=false;
        add(y,x,z);
	add(x,y,z);
	inc(d[x]);
	inc(d[y]);
	if z<minn then minn:=z;
	a[i]:=z;
	end;
if m=1 then special_1
	else if flagg then special_2
		else if flag then special_3
			else if m=n-1 then special_4
				else special_5;
close(input);
close(output);
end.