#include<bits/stdc++.h>
using namespace std;
int T;
int n,m,a[110];
int f[30000],sum,maxn,ans;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	cin>>T;
	while(T--){
		memset(f,0,sizeof f);
		sum=0;maxn=0;
		scanf("%d",&n);
		for(int i=1;i<=n;++i){
			scanf("%d",a+i);
		}
		sort(a+1,a+n+1);
		m=unique(a+1,a+n+1)-a-1;
		maxn=a[m];//cout<<maxn<<endl;
		f[0]=1;ans=0;
		for(int i=1;i<=n;++i){
			if(f[a[i]])continue;ans++;
			for(int j=0;j+a[i]<=maxn;++j){
				f[j+a[i]]|=f[j];
			}sum=0;
			for(int j=1;j<=n;++j){
				sum+=f[a[j]];
			}
			if(sum==n){
				printf("%d\n",ans);
				break;
			}
		}
	}
	return 0;
}
