#include<bits/stdc++.h>
using namespace std;
int n,m;
int tot,bian[100010],nxt[100010],head[50010];
long long zhi[100010],e[50010],sum;
int rd[50010];
long long mp[100010];
inline void add(int x,int y,long long z){
	tot++,bian[tot]=y,zhi[tot]=z,nxt[tot]=head[x],head[x]=tot;
}
//priority_queue<int,vector<int>,greater<int> >mnq;
//priority_queue<int>mxq;
long long dfs1(int rt,int f){
	long long ans=0;
	for(int i=head[rt];i;i=nxt[i]){
		if(bian[i]==f)continue;
		if(!mp[i]){
			mp[i]=dfs1(bian[i],rt);
		}
		ans=max(ans,zhi[i]+mp[i]);
	}
	return ans;
}
bool check(long long x){
	int k=0;
	long long now=0;
	for(int i=1;i<n;++i){
		now+=e[i];
		if(now>=x)k++,now=0;
	}
	return (k>=m); 
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	cin>>n>>m;
	int bo=1,boo=1;
	for(int i=1;i<n;++i){
		int x,y;
		long long z;
		scanf("%d%d%lld",&x,&y,&z);
		e[i]=z;sum+=z;
		if(x!=1)bo=0;
		if(y!=x+1)boo=0;
		add(x,y,z);rd[x]++;
		add(y,x,z);rd[y]++;
	}
	if(m==1){
		long long ans=0;
		for(int i=1;i<=n;++i){
			if(rd[i]==1){
				ans=max(ans,dfs1(i,0));
			}
		}
		cout<<ans<<endl;
		return 0;
	}
	if(bo){
		sort(e+1,e+n,greater<long long>());
		long long ans=1ll<<40;
		for(int i=1;i<=m;++i){
			ans=min(ans,e[i]+e[2*m-i+1]);
		}
		cout<<ans<<endl;
		return 0;
	}
	if(boo){
		for(int i=1;i<n;++i){
			for(int j=head[i];j;j=nxt[j]){
				if(bian[i]==i+1)e[i]=zhi[i];
			}
		}
		long long l=1,r=sum,ans;
		while(l<r){
			int mid=l+(r-l)/2;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid;
		}
		cout<<ans<<endl;
		return 0;
	}
}
