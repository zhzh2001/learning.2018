#include<cctype>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 50005 
#define F inline
using namespace std;
struct edge{ int nxt,to,d; }ed[N<<1];
int n,m,k,ans,h[N],q[N],d[N]; bool f[N];
F char readc(){
	static char buf[100000],*l=buf,*r=buf;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	return l==r?EOF:*l++;
}
F int _read(){
	int x=0; char ch=readc();
	while (!isdigit(ch)) ch=readc();
	while (isdigit(ch)) x=(x<<3)+(x<<1)+(ch^48),ch=readc();
	return x;
}
#define add(x,y,z) ed[++k]=(edge){h[x],y,z},h[x]=k
F void dd1(){
	int l=0,r=1,x=0;
	for (int i=1;i<=n;i++) f[i]=false;
	f[1]=true,d[1]=0,q[1]=1;
	while (l<r){
		int x=q[++l];
		for (int i=h[x],v;i;i=ed[i].nxt){
			if (f[v=ed[i].to]) continue;
			d[v]=d[x]+ed[i].d,q[++r]=v,f[v]=true;
		}
	}
	for (int i=1;i<=n;i++) if (d[x]<d[i]) x=i;
	for (int i=1;i<=n;i++) f[i]=false;
	f[x]=true,d[x]=0,l=0,r=1,q[1]=x;
	while (l<r){
		int x=q[++l];
		for (int i=h[x],v;i;i=ed[i].nxt){
			if (f[v=ed[i].to]) continue;
			d[v]=d[x]+ed[i].d,q[++r]=v,f[v]=true;
		}
	}
	for (int i=1;i<=n;i++) if (d[x]<d[i]) x=i;
	printf("%d\n",d[x]);
}
F void dd2(){
	int mn=1e9,l,ll;
	for (int i=h[1];i;i=ed[i].nxt)
		d[++d[0]]=ed[i].d;
	sort(d+1,d+d[0]+1),l=d[0]-m;
	ll=max(1,d[0]-m*2); 
	for (int i=ll;i<=l;i++)
		d[2*l+1-i]+=d[i];
	for (int i=l+1;i<=d[0];i++)
		mn=min(mn,d[i]);
	printf("%d\n",mn);
}
F bool pd1(int mid){
	int s=0,sum=0;
	for (int i=2;i<=n;i++)
		if (s+d[i]<mid) s+=d[i];
		else sum++,s=0;
	return sum>=m;
}
F void dd3(){
	int l=0,r=1e9,mid,ans;
	for (int x=2;x<=n;x++)
	for (int i=h[x];i;i=ed[i].nxt)
		if (ed[i].to==x-1) d[x]=ed[i].d;
	while (l<=r)
		if (pd1(mid=l+r>>1)) ans=mid,l=mid+1;
		else r=mid-1;
	printf("%d\n",ans);
}
F bool dfs(int x,int s,int mid){
	if (s>=mid) return true;
	for (int i=h[x];i;i=ed[i].nxt)
		if (!f[i+1>>1]){
			f[i+1>>1]=true;
			if (dfs(ed[i].to,s+ed[i].d,mid))
				return true;
			f[i+1>>1]=false;
		}
	return false;
}
F bool pd(int mid){
	int sum=0;
	for (int i=1;i<=n;i++) f[i]=false;
	for (int i=1;i<=n;i++) if (dfs(i,0,mid)) sum++;
	return sum>=m;
}
F void dd(){
	int l=0,r=1e9,mid,ans;
	while (l<=r){
		mid=l+r>>1;
		if (pd(mid=l+r>>1)) ans=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",ans);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=_read(),m=_read(); bool f1=true,f2=true;
	for (int i=1;i<n;i++){
		int x=_read(),y=_read(),z=_read();
		add(x,y,z),add(y,x,z);
		if (x!=1) f1=false;	if (y!=x+1) f2=false;
	}
	if (m==1) return dd1(),0;
	if (f1) return dd2(),0;
	if (f2) return dd3(),0;
	return dd(),0;
}
