#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 105
#define M 25005
using namespace std;
int T,n,a[N],f[M];
bool flag[M];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for (scanf("%d",&T);T;T--){
		scanf("%d",&n); int mx=0,mn=1e9,ans=n;
		for (int i=1;i<=n;i++)
			scanf("%d",&a[i]),mx=max(mx,a[i]),mn=min(mn,a[i]);
		if (mn==1){ puts("1"); continue; }
		for (int i=0;i<=mx;i++) f[i]=0;
		for (int i=1;i<=n;i++){
			for (int j=a[i];j<=mx;j+=a[i]) f[j]++;
			if (f[a[i]]>1) continue;
			for (int j=1;j<=mx;j++){
				if (!f[j]) continue;
				for (int x=1;x*a[i]<=mx;x++)
				for (int y=1;x*a[i]+y*j<=mx;y++)
					f[x*a[i]+y*j]++;
			}
		}
		for (int i=1;i<=mx;i++)
			if (f[i]) for (int j=i*2;j<=mx;j+=i) f[j]++;
		for (int i=1;i<=n;i++)
			if (f[a[i]]>1) ans--;
		printf("%d\n",ans);
	}
	return 0;
}
