#include<cctype>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 100005
#define F inline
using namespace std;
int n,ans,d[N],q[N];
F char readc(){
	static char buf[100000],*l=buf,*r=buf;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	return l==r?EOF:*l++;
}
F int _read(){
	int x=0; char ch=readc();
	while (!isdigit(ch)) ch=readc();
	while (isdigit(ch)) x=(x<<3)+(x<<1)+(ch^48),ch=readc();
	return x;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=_read(); int l=1,r=0;
	for (int i=1;i<=n;i++) d[i]=_read();
	for (int i=1;i<=n+1;i++){
		int mx=d[i];
		while (l<=r&&d[q[r]]>=d[i])
			mx=max(mx,d[q[r]]),r--;
		ans+=mx-d[i],q[++r]=i;
	}
	return printf("%d\n",ans),0;
}
