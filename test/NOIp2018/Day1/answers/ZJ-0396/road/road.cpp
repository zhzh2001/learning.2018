#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int M=(int)1e5+5;
int n,a[M];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	int ans=0,las=0;
	for(int i=1;i<=n;i++){
		if(las<a[i])ans+=a[i]-las;
		las=a[i];
	}
	printf("%d\n",ans);
	return 0;
}
