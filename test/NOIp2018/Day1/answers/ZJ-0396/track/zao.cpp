#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<ctime>
#include<iostream>
#include<algorithm>
using namespace std;
const int M=(int)1e5+5;
int n,m,a[M];
int main(){
	srand(time(NULL));
	freopen("track.in","w",stdout);
	n=50000,m=rand()*rand()%n+1;
	for(int i=1;i<=n;i++)a[i]=i;
	for(int i=1;i<=10000000;i++)swap(a[rand()*rand()%n+1],a[rand()*rand()%n+1]);
	printf("%d %d\n",n,m);
	for(int i=2;i<=n;i++)printf("%d %d %d\n",a[i],a[rand()*rand()%(i-1)+1],rand()*rand()%10000+21);
	return 0;
}
