#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<set>
using namespace std;
const int M=(int)5e4+5;
void Rd(int &res){
	res=0;static char p;
	while(p=getchar(),p<'0');
	do{
		res=(res*10)+(p^48);
	}while(p=getchar(),p>='0');
}
struct W{
	int to,nx,d;
}Lis[M*2];
int Head[M],tot;
void eAdd(int x,int y,int d){
	Lis[++tot]=(W){y,Head[x],d};
	Head[x]=tot;
}
int n,m,suml,mil;
struct SHUI{
	int ned;
	pair<int,int>dp[M],tmp[M];
	int pl[M],sz;
	void dfs(int x,int f,int fl){
		for(int i=Head[x],to;i&&(to=Lis[i].to,1);i=Lis[i].nx)
			if(to!=f)
				dfs(to,x,Lis[i].d);
		sz=0;
		for(int i=Head[x],to;i&&(to=Lis[i].to,1);i=Lis[i].nx)
			if(to!=f)
				tmp[sz++]=dp[to];
		//���ϸ���
		pair<int,int>fb;
		fb.first=fb.second=0;
		for(int i=0;i<sz;i++){
			pl[i]=tmp[i].second;
			fb.first+=tmp[i].first;
		}
		pl[sz]=fl;
		sort(pl,pl+sz+1);
		for(int l=0,r=sz;l<r;){
			if(pl[l]+pl[r]>=ned)l++,r--,fb.first++;
			else l++;
		}
		//���Ӹ���
		pair<int,int>nfb;
		nfb.first=nfb.second=0;
		for(int i=0;i<sz;i++){
			pl[i]=tmp[i].second;
			nfb.first+=tmp[i].first;
		}
		sort(pl,pl+sz);
		multiset<int>st;
		multiset<int>::iterator it;
		for(int i=0;i<sz;i++){
			it=st.lower_bound(ned-pl[i]);
			if(it!=st.end()){
				nfb.first++;
				st.erase(it);
			}else {
				st.insert(pl[i]);
			}
		}
		if(st.size()>=1){
			it=st.end();
			it--;
			nfb.second=(*it)+fl;
			if(nfb.second>=ned)nfb.second=0,nfb.first++;
		}else {
			nfb.second=fl;
			if(nfb.second>=ned)nfb.second=0,nfb.first++;
		}
		dp[x]=max(fb,nfb);
	}
	bool check(){
		dfs(1,1,0);
		return dp[1].first>=m;
	}
	void solve(){
		int L=mil,R=suml/m+1,mid,ans=0;
		while(L<=R){
			mid=(L+R)>>1;
			ned=mid;
			if(check())L=mid+1,ans=mid;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}PPP;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	Rd(n),Rd(m);
	mil=10001;
	for(int i=1,x,y,d;i<n;i++){
		Rd(x),Rd(y),Rd(d);
		eAdd(x,y,d),eAdd(y,x,d);
		suml+=d;
		if(d<mil)mil=d;
	}
	PPP.solve();
	return 0;
}
