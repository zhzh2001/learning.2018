#include <bits/stdc++.h>
#define mp make_pair
#define ft first
#define sc second
#define pb push_back
#define pii pair<int, int>
#define rep(i, a, b) for (int i = (a); i <= (b); ++i)
#define dep(i, a, b) for (int i = (a); i >= (b); --i)
using namespace std;

int qInt() {
  int x = 0; char ch = getchar();
  while (ch < '0' || ch > '9') ch = getchar();
  while (ch >= '0' && ch <= '9') (x *= 10) += (ch - '0'), ch = getchar();
  return x;
}

int main() {
 
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
  int n = qInt();
  int bef = 0, Ans = 0;
  rep(i, 1, n) {
    int x = qInt();
    Ans += max(0, x - bef);
    bef = x;
  }
  cout << Ans << endl;
  return 0;

}
