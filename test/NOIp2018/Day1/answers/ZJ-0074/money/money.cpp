#include <bits/stdc++.h>
#define mp make_pair
#define ft first
#define sc second
#define pb push_back
#define pii pair<int, int>
#define rep(i, a, b) for (int i = (a); i <= (b); ++i)
#define dep(i, a, b) for (int i = (a); i >= (b); --i)
using namespace std;

int qInt() {
  int x = 0; char ch = getchar();
  while (ch < '0' || ch > '9') ch = getchar();
  while (ch >= '0' && ch <= '9') (x *= 10) += (ch - '0'), ch = getchar();
  return x;
}

const int Max = 25000;
int s[110], vis[Max + 10];

int main() {

  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
  int T = qInt();
  for (; T--; ) {
    int n = qInt();
    rep(i, 1, n) s[i] = qInt();
    memset(vis, 0, sizeof vis);
    vis[0] = 1;
    rep(i, 1, n) {
      rep(j, 0, Max)
	if (j + s[i] > Max) break;
	else {
	  vis[j + s[i]] += vis[j];
	  if (vis[j + s[i]] >= 2) vis[j + s[i]] = 2;
	}
    }
    int tot = 0;
    rep(i, 1, n) if (vis[s[i]] == 1) tot ++;
    printf("%d\n", tot);
  }
  return 0;

}
