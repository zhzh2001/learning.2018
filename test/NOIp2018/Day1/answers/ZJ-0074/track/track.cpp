#include <bits/stdc++.h>
#define mp make_pair
#define ft first
#define sc second
#define pb push_back
#define pii pair<int, int>
#define rep(i, a, b) for (int i = (a); i <= (b); ++i)
#define dep(i, a, b) for (int i = (a); i >= (b); --i)
using namespace std;

int qInt() {
  int x = 0; char ch = getchar();
  while (ch < '0' || ch > '9') ch = getchar();
  while (ch >= '0' && ch <= '9') (x *= 10) += (ch - '0'), ch = getchar();
  return x;
}

const int M = 100000 + 10;

int fst[M], lst[M], des[M], val[M], nxt[M], cnt = 0, n, m;
pii dp[M];

void add(int u, int v, int w) {
  if (!fst[u]) fst[u] = ++cnt;
  else nxt[lst[u]] = ++cnt;
  des[cnt] = v, lst[u] = cnt, val[cnt] = w;
}

int w[M];

void dfs(int u, int fa, int x) {
  dp[u] = mp(0, 0);
  int len = 0;
  for (int i = fst[u]; i; i = nxt[i])
    if (des[i] != fa) {
      dfs(des[i], u, x);
      dp[u].ft += dp[des[i]].ft;
    }
  w[0] = 0;
  for (int i = fst[u]; i; i = nxt[i])
    if (des[i] != fa)
      w[++len] = dp[des[i]].sc + val[i];
  sort(w + 1, w + len + 1);
  int res = 0;
  while (len && w[len] >= x) len --, res ++;
  int lf = 1;
  int Max = 0;
  dep(i, len, 1) {
    while (lf < i && w[lf] + w[i] < x) lf ++;
    if (lf >= i) break;
    Max ++, lf ++;
  }
  int l = 0, r = len, ck;
  while (l < r) {
    int mid = (l + r + 1) / 2;
    lf = 1, ck = 0;
    dep(i, len, 1) {
      if (i == mid) continue;
      while (lf < i && w[lf] + w[i] < x) lf ++;
      if (lf == mid) lf ++;
      if (lf >= i) break;
      ck ++, lf ++;
    }
    if (ck == Max) l = mid;
    else r = mid - 1;
  }
  dp[u].ft += (Max + res);
  dp[u].sc = w[l];
}

int check(int x) {
  dfs(1, 0, x);
  return  (dp[1].ft >= m);
}

int main() {

  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
  n = qInt(), m = qInt();
  rep(i, 1, n - 1) {
    int u = qInt(), v = qInt(), w = qInt();
    add(u, v, w), add(v, u, w);
  }
  int l = 1, r = 1000000000;
  while (l < r) {
    int mid = (l + r + 1) / 2;
    if (check(mid)) l = mid;
    else r = mid - 1;
  }
  cout << l << endl;
  return 0;
  
}
