#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
typedef pair<int,int> pr;
#define fi first
#define se second
bool vis[100010];
int fa[100010],Val[100010],used[100010],ans,n,m,u,v,w;
pr check(int x,int y){
	bool vis[1010];
	memset(vis,0,sizeof vis);
	for(int cur=x;cur;cur=fa[cur])
		vis[cur]=1;
	int lca=0;
	for(int cur=y;cur;cur=fa[cur])
		if(vis[cur]){lca=cur;break;}
	bool flag=1;
	for(int cur=x;cur!=lca;cur=fa[cur])
		if(used[cur])
			flag=0;
	for(int cur=y;cur!=lca;cur=fa[cur])
		if(used[cur])
			flag=0;
	if(!flag)
		return make_pair(-1,0);
	//printf("%d %d lca=%d\n",x,y,lca);
	int ans=0;
	for(int cur=x;cur!=lca;cur=fa[cur])
		used[cur]=1,ans+=Val[cur];
	for(int cur=y;cur!=lca;cur=fa[cur])
		used[cur]=1,ans+=Val[cur];
	return make_pair(lca,ans);
}
void undo(int x,int y,int lca){
	for(int cur=x;cur!=lca;cur=fa[cur])
		used[cur]=0;
	for(int cur=y;cur!=lca;cur=fa[cur])
		used[cur]=0;
}
void dfs(int x,int mn){
	//printf("%d %d\n",x,mn);
	if(x>m){
		ans=max(ans,mn);
		return;
	}
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			{
				//vis[i]=vis[j]=1;
				pr tmp=check(i,j);
				if(tmp.fi>0){
					//for(int i=1;i<=x;i++)
					//	printf(" ");
					//printf("chose (%d,%d)\n",i,j);
					dfs(x+1,min(mn,tmp.se));
					undo(i,j,tmp.fi);
				}
				//vis[i]=vis[j]=0;
			}
}

int main(){
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&u,&v,&w);
		fa[v]=u;Val[v]=w;
	}
	dfs(1,0x3f3f3f3f);
	printf("%d\n",ans);
}
