#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
#define fi first
#define se second
typedef pair<ll,ll> pr;
int sonid[100010],n,m,u,v,w;
pr son[1010][1010];

int Head[100010],tot,Next[200010],To[200010],Val[200010];

void addedge(int u,int v,int w){
	tot++;
	Next[tot]=Head[u];
	Head[u]=tot;
	To[tot]=v;
	Val[tot]=w;
}
pr calc(int u,ll mid,int dl){
	int head=1;
	pr res=make_pair(0,dl);
	for(int i=sonid[u];i>=head;i--){
		if(i==dl)continue;
		if(son[u][i].fi>=mid){
			res.fi++;
			continue;
		}
		while(head<i){
			if(head==dl){head++;continue;}
			if(son[u][head].fi+son[u][i].fi>=mid)
				break;
			else
				head++;
		}
		if(head<i&&son[u][head].fi+son[u][i].fi>=mid)
			res.fi++,head++;
	}
	return res;
}

pr dfs(int u,int f,ll mid){
	sonid[u]=0;
	pr res=make_pair(0,0);
	for(int it=Head[u];it;it=Next[it]){
		int v=To[it];
		if(v!=f){
			pr tmp=dfs(v,u,mid);
			res.fi+=tmp.fi;
			son[u][++sonid[u]]=make_pair(tmp.se+Val[it],v);
		}
	}
	sort(son[u]+1,son[u]+1+sonid[u]);
	/*printf("in Node %d\n",u);
	for(int i=1;i<=sonid[u];i++)
		printf("(%d,%d) ",son[u][i].fi,son[u][i].se);
	printf("\n");*/
	pr cs=calc(u,mid,0);
	int l=1,r=sonid[u];
	while(l<=r){
		int m=(l+r)>>1;
		pr tmp=calc(u,mid,m);
		if(tmp.fi>cs.fi)
			cs=tmp;
		else if(tmp.fi==cs.fi&&tmp.se>cs.se)
			cs=tmp;
		if(tmp.fi>=cs.fi)
			l=m+1;
		else
			r=m-1;
	}
	res.fi+=cs.fi;
	res.se=son[u][cs.se].fi;
	//printf("dfs(%d) res=(%lld,%lld)\n",u,res.fi,res.se);
	return res;
}
		

bool check(ll mid){
	pr tmp=dfs(1,1,mid);
	return (tmp.fi>=m);
}
int main(){
	scanf("%d%d",&n,&m);
	ll l=0,r=0,ans=0;
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&u,&v,&w);
		r+=w;
		addedge(u,v,w);
		addedge(v,u,w);
	}
	while(l<=r){
		ll mid=(l+r)>>1;
		//printf("check:%lld %lld %lld\n",l,r,mid);
		if(check(mid))
			l=mid+1,ans=mid;
		else
			r=mid-1;
	}
	printf("%lld\n",ans);
}
	
