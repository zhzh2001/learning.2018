#include<stdio.h>
#include<algorithm>
using namespace std;
typedef long long ll;
#define fi first
#define se second
typedef pair<ll,ll> pr;
int n,m,u,v,w;
int bg[100010],ed[100010];
pr son[100010];

int Head[100010],tot,Next[200010],To[200010],Val[200010];

void addedge(int u,int v,int w){
	tot++;
	Next[tot]=Head[u];
	Head[u]=tot;
	To[tot]=v;
	Val[tot]=w;
}
pr calc(int u,ll mid,int dl,int sonid){
	int head=1;
	pr res=make_pair(0,dl);
	for(int i=sonid;i>=head;i--){
		if(i==dl)continue;
		if(son[bg[u]+i-1].fi>=mid){
			res.fi++;
			continue;
		}
		while(head<i){
			if(head==dl){head++;continue;}
			if(son[bg[u]+head-1].fi+son[bg[u]+i-1].fi>=mid)
				break;
			else
				head++;
		}
		if(head<i&&son[bg[u]+head-1].fi+son[bg[u]+i-1].fi>=mid)
			res.fi++,head++;
	}
	return res;
}

pr dfs(int u,int f,ll mid){
	int sonid=0;
	pr res=make_pair(0,0);
	for(int it=Head[u];it;it=Next[it]){
		int v=To[it];
		if(v!=f){
			pr tmp=dfs(v,u,mid);
			res.fi+=tmp.fi;
			sonid++;
			son[bg[u]+sonid-1]=make_pair(tmp.se+Val[it],v);
		}
	}
	if(ed[u]>=bg[u]){
		sort(son+bg[u],son+1+ed[u]);
		/*printf("in Node %d\n",u);
		for(int i=1;i<=sonid[u];i++)
			printf("(%d,%d) ",son[u][i].fi,son[u][i].se);
		printf("\n");*/
		pr cs=calc(u,mid,0,sonid);
		int l=1,r=sonid;
		while(l<=r){
			int m=(l+r)>>1;
			pr tmp=calc(u,mid,m,sonid);
			if(tmp.fi>cs.fi)
				cs=tmp;
			else if(tmp.fi==cs.fi&&tmp.se>cs.se)
				cs=tmp;
			if(tmp.fi>=cs.fi)
				l=m+1;
			else
				r=m-1;
		}
		res.fi+=cs.fi;
		res.se=(cs.se==0)?0:son[bg[u]+cs.se-1].fi;
		//printf("dfs(%d) res=(%lld,%lld)\n",u,res.fi,res.se);
		return res;
	}else{
		return make_pair(0,0);
	}
}
		

bool check(ll mid){
	pr tmp=dfs(1,1,mid);
	return (tmp.fi>=m);
}
int start=1;
void build(int u,int f){
	
	int sonnum=0;
	for(int it=Head[u];it;it=Next[it]){
		int v=To[it];
		if(v!=f){
			build(v,u);
			sonnum++;
		}
	}
	bg[u]=start;
	ed[u]=start+sonnum-1;
	start+=sonnum;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	ll l=0,r=0,ans=0;
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&u,&v,&w);
		r+=w;
		addedge(u,v,w);
		addedge(v,u,w);
	}
	build(1,1);
	while(l<=r){
		ll mid=(l+r)>>1;
		//printf("check:%lld %lld %lld\n",l,r,mid);
		if(check(mid))
			l=mid+1,ans=mid;
		else
			r=mid-1;
	}
	printf("%lld\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
	
