#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,a[100010],mx,f[100010],cnt,ans;
int main(){
	int t;
	scanf("%d",&t);
	while(t--){
		scanf("%d",&n);
		mx=0;
		for(int i=0;i<n;i++){
			scanf("%d",&a[i]);
			mx=max(mx,a[i]);
		}
		sort(a,a+n);
		ans=0x3f3f3f3f;
		for(int sta=0;sta<(1<<n);sta++){
			memset(f,0,sizeof f);
			f[0]=1;
			cnt=0;
			for(int i=0;i<n;i++)
				if((sta>>i)&1){
					for(int j=a[i];j<=mx;j++)
						f[j]|=f[j-a[i]];
					cnt++;
				}
			bool flag=0;
			for(int i=0;i<n;i++)
				if(!f[a[i]]){
					flag=1;
					break;
				}
			if(!flag)
				ans=min(ans,cnt);
		}
		printf("%d\n",ans);
	}
}
