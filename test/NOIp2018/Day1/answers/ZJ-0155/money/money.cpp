#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int t,n,a[110],mx,ans;
bool f[100010];

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d",&n);
		mx=0;
		for(int i=1;i<=n;i++){
			scanf("%d",&a[i]);
			mx=max(mx,a[i]);
		}
		sort(a+1,a+1+n);
		memset(f,0,sizeof f);
		f[0]=1;ans=0;
		for(int i=1;i<=n;i++){
			if(!f[a[i]])
				ans++;
			for(int j=a[i];j<=mx;j++)
				f[j]|=f[j-a[i]];
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
