/*by dennyqi*/
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <queue>
#define int long long
using namespace std;
const int MAXN = 50010;
const int MAXM = 100010;
const int INF = 0x3f3f3f3f;
inline int Max(int a, int b){ return (a>b) ? a : b; }
inline int Min(int a, int b){ return (a<b) ? a : b; }
inline int read(){
	int x = 0, zf = 1; register char c = getchar();
	while(c ^ '-' && (c < '0' || c > '9')) c = getchar();
	if(c == '-') zf = -1, c = getchar();
	while(c >= '0' && c <= '9') x = (x<<3) + (x<<1) + c - '0', c = getchar(); return x * zf;
}
int N,M,x,y,z,Lian,Tot;
int first[MAXN],nxt[MAXM],to[MAXM],cost[MAXM],cnt;
int d[MAXN];
int c[MAXN],sum[MAXN];
inline void link(int u, int v, int w){
	to[++cnt] = v, cost[cnt] = w, nxt[cnt] = first[u], first[u] = cnt;
}
void Dfs(int u, int Fa){
	int v;
	for(int i = first[u]; i; i = nxt[i]){
		if((v = to[i]) == Fa) continue;
		d[v] = d[u] + cost[i];
		Dfs(v, u);
	}
}
inline int LongestLian(){
	int S = 1,u,v,S2 = 1;
	memset(d, 0, sizeof d);
	Dfs(S, 0);
	int mx = -1;
	for(int i = 1; i <= N; ++i){
		if(d[i] > mx){
			mx = d[i];
			S2 = i;
		}
	}
	memset(d, 0, sizeof d);
	Dfs(S2, 0);
	int Ans = (-1);
	for(int i = 1; i <= N; ++i){
		Ans = Max(Ans, d[i]);
	}
	return Ans;
}
inline bool judge(int k){
	int s = 1,Cnt=0;
	for(int i = 1; i < N; ++i){
		if(sum[i]-sum[s-1] < k) continue;
		Cnt++;
		s = i+1;
	}
	return Cnt >= M;
}
inline int SolveLian(){
	int L = 1, R = sum[N-1], Mid, Ans;
	while(L <= R){
		Mid = (L + R) / 2;
		if(judge(Mid)){
			L = Mid + 1;
			Ans = Mid;
		}
		else{
			R = Mid - 1;
		}
	}
	return Ans;
}
#undef int
int main(){
#define int long long
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	N = read(), M = read();
	Lian = 1;
	for(int i = 1; i < N; ++i){
		x = read(), y = read(), z = read();
		c[i] = z;
		sum[i] = sum[i-1] + c[i];
		if(y != x+1){
			Lian = 0;
		}
		link(x, y, z);
		link(y, x, z);
	}
	if(M == 1){
		printf("%lld", LongestLian());
		return 0;
	}
	if(Lian){
		printf("%lld", SolveLian());
		return 0;
	}
	printf("%lld", sum[N-1]/M - 1);
	return 0;
}
