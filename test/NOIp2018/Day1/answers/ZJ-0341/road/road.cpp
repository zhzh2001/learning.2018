/*by dennyqi*/
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <queue>
#include <cmath>
#define int long long
using namespace std;
const int MAXN = 100010;
const int INF = 0x3f3f3f3f;
inline int Max(int a, int b){ return (a>b) ? a : b; }
inline int Min(int a, int b){ return (a<b) ? a : b; }
inline int read(){
	int x = 0, zf = 1; register char c = getchar();
	while(c ^ '-' && (c < '0' || c > '9')) c = getchar();
	if(c == '-') zf = -1, c = getchar();
	while(c >= '0' && c <= '9') x = (x<<3) + (x<<1) + c - '0', c = getchar(); return x * zf;
}
int N,Ans;
int a[MAXN],f[MAXN][25],p[MAXN][25];
inline void ST(){
	for(int j = 1; (1<<j) <= N; ++j){
		for(int i = 1; i + (1<<j) - 1 <= N; ++i){
			if(f[i][j-1] < f[i+(1<<(j-1))][j-1]){
				f[i][j] = f[i][j-1];
				p[i][j] = p[i][j-1];
			}
			else{
				f[i][j] = f[i+(1<<(j-1))][j-1];
				p[i][j] = p[i+(1<<(j-1))][j-1];
			}
		}
	}
}
inline int Find(int l, int r){
	int k = log(r-l+1) / log(2);
	if(f[l][k] < f[r-(1<<k)+1][k]){
		return p[l][k];
	}
	else{
		return p[r-(1<<k)+1][k];
	}
}
void Solve(int l, int r, int d){
	if(l >= r){
		Ans += a[l] - d;
//		printf("Ans += %d - %d\n",a[l],d);
		return;
	}
	int pos = Find(l,r);
//	printf("l=%d r=%d pos=%d\n",l,r,pos);
	Ans += a[pos] - d;
//	printf("Ans += %d - %d\n",a[pos],d);
	if(l <= pos-1) Solve(l,pos-1,a[pos]);
	if(pos+1 <= r) Solve(pos+1, r, a[pos]);
}
#undef int
int main(){
#define int long long
	freopen("road.in","r",stdin);
	freopen("road.out", "w", stdout);
	N = read();
	for(int i = 1; i <= N; ++i){
		a[i] = read();
		f[i][0] = a[i];
		p[i][0] = i;
	}
	ST();
/*	for(int i = 1; i <= N; ++i){
		for(int j = 0; j <= 3; ++j){
			printf("p[%d][%d] = %d\n",i,j,p[i][j]);
		}
	}*/
	Solve(1, N, 0);
	printf("%lld", Ans);
	return 0;
}
