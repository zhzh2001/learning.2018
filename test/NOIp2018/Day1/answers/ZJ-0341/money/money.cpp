/*by dennyqi*/
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <queue>
#define ll long long
using namespace std;
const int MAXN = 10010;
const int MAXM = 20010;
const int INF = 0x3f3f3f3f;
inline int Max(int a, int b){ return (a>b) ? a : b; }
inline int Min(int a, int b){ return (a<b) ? a : b; }
inline int read(){
	int x = 0, zf = 1; register char c = getchar();
	while(c ^ '-' && (c < '0' || c > '9')) c = getchar();
	if(c == '-') zf = -1, c = getchar();
	while(c >= '0' && c <= '9') x = (x<<3) + (x<<1) + c - '0', c = getchar(); return x * zf;
}
int T,N,Ans,Tot;
int a[110],b[110],c[110],jdg[110];
inline int get_sumb(){
	int res = 0;
	for(int i = 1; i <= N; ++i){
		res += b[i];
	}
	return res;
}
inline bool AllFinished(){
	for(int i = 1; i <= N; ++i){
		if(b[i] && (!jdg[i])) return 0;
	}
	return 1;
}
int Dfs2(int x, int s){
	if(AllFinished()) return 1;
	if(x > N){
//		printf("s=%d\n",s);
		for(int i = 1; i <= N; ++i){
			if(b[i] && s==a[i]){
				jdg[i] = 1;
//				printf("jdg[%d]=1\n",i);
			}
		}
		if(AllFinished()) return 1;
		return 0;
	}
	if(b[x]){
		if(Dfs2(x+1, s)) return 1;
	}
	else{
		if(Dfs2(x+1, s)) return 1;
		for(int i = 1; s+a[x]*i <= Tot; ++i){
			if(Dfs2(x+1, s+a[x]*i)) return 1;
		}
	}
	return 0;
}
inline bool Check(){
	memset(c, 0, sizeof c);
	memset(jdg, 0, sizeof jdg);
/*	printf("b: ");
	for(int i = 1; i <= N; ++i){
		printf("%d ",b[i]);
	}
	printf("\n");
	printf("jdg: ");
	for(int i = 1; i <= N; ++i){
		printf("%d ",jdg[i]);
	}
	printf("\n");*/
	Dfs2(1, 0);
}
void Dfs(int x){
	if(x > N){
		if(N-get_sumb() >= Ans) return;
		if(Check()){
//			printf("-> %d\n",N-get_sumb());
			Ans = Min(Ans, N-get_sumb());
//			printf("Ans = %d\n", Ans);
		}
		return;
	}
	Dfs(x+1);
	b[x] = 1;
	Dfs(x+1);
	b[x] = 0;
}
inline int solve(){
	if(N == 1){
		return 1;
	}
	if(N == 2){
		if(a[1] > a[2]) swap(a[1], a[2]);
		if(a[2] % a[1] == 0) return 1;
		else return 2;
	}
	Ans = N;
	memset(b,0,sizeof b);
	Dfs(1);
	return Ans;
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T = read();
	while(T--){
		N = read();
		Tot = 0;
		for(int i = 1; i <= N; ++i){
			a[i] = read();
			Tot = Max(Tot, a[i]);
		}
		printf("%d\n", solve());
	}
	return 0;
}
