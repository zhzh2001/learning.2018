#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
using namespace std;

int n,m,t,maxn,a[108],dp[25008],ans;

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	scanf("%d",&t);
	for(int ii=1;ii<=t;ii++){
		ans=0;
		memset(dp,0,sizeof(dp));
		scanf("%d",&n);
		maxn=0;
		for(int i=1;i<=n;i++){
			scanf("%d",&a[i]);
			maxn=max(a[i],maxn);
		}
		sort(a+1,a+n+1);
		dp[0]=1;
		for(int i=1;i<=n;i++){
			if(dp[a[i]]==1){
				ans++;
				continue;
			}
			for(int j=a[i];j<=maxn;j++)
				if(dp[j-a[i]]) dp[j]=1;
		}
		printf("%d\n",n-ans);
	}
}
