#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

int tot,n,m,head[100008],nex[100008],vet[100008],len[100008];
int bo[50008],ans,s1,num,road[100008],a[100008],sum;
int ez[50008][4],leng[50008],dp[50008],gg[50008];
bool boo;

void add(int x,int y,int z){
	tot++;
	nex[tot]=head[x];
	head[x]=tot;
	vet[tot]=y;
	len[tot]=z;
}

void dfs(int u,int s){
	if(s>ans){
		ans=s; s1=u;
	}
	for(int i=head[u];i;i=nex[i]){
		int y=vet[i];
		if(!bo[y]){
			bo[y]=1;
			dfs(y,s+len[i]);
			bo[y]=0;
		}
	}
}

void solve1(){
	memset(bo,0,sizeof(bo));
	bo[1]=1;
	dfs(1,0);
	memset(bo,0,sizeof(bo));
	bo[s1]=1;
	dfs(s1,0);
	printf("%d",ans);
}

void solve2(){
	n--;
	sort(road+1,road+n+1);
	int res=2000000008;
	for(int i=n-m+1;i<=n;i++)
		if((n-m+1-(i-n+m))<=0) res=min(res,road[i]);
		else res=min(res,road[i]+road[n-m+1-(i-n+m)]);
	printf("%d",res);
}

bool check(int k){
	int ss=0,kk=0;
	for(int i=1;i<=n;i++){
		ss+=a[i];
		if(ss>=k){
			ss=0; kk++;
		}
	}
	return kk>=m;
}

void solve3(){
	n--;
	int l=0,r=sum,res=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if(check(mid)){
			res=mid;
			l=mid+1;
		}else r=mid-1;
	}
	printf("%d",res);
}

void dfs1(int u,int s){
	for(int i=head[u];i;i=nex[i]){
		int y=vet[i];
		if(!bo[y]){
			ez[u][0]++;
			ez[u][ez[u][0]]=y;
			bo[y]=1;
			leng[y]=len[i];
			dfs1(y,s+len[i]);
			bo[y]=0;
		}
	}
}

void tree(int u,int flag){
	for(int i=head[u];i;i=nex[i]){
		int y=vet[i];
		if(!bo[y]){
			bo[y]=1;
			tree(y,flag);
			bo[y]=0;
			dp[u]+=dp[y];
		}
	}
	if(ez[u][0]==0) return;
	if(ez[u][0]==1){
		int aa=ez[u][1];
		if(leng[aa]>=flag) dp[u]++;
		else leng[u]+=leng[aa];
	}
	if(ez[u][0]==2){
		int aa=ez[u][1],bb=ez[u][2];
		if(leng[aa]<leng[bb]) swap(aa,bb);
		if(leng[bb]>=flag){
			dp[u]+=2;
		}else
		if(leng[aa]>=flag){
			dp[u]++; leng[u]+=leng[bb];
		}else
		if(leng[aa]+leng[bb]>=flag){
			dp[u]++;
		}else leng[u]+=leng[aa];
	}
	if(ez[u][0]==3){
		int aa=ez[u][1],bb=ez[u][2],cc=ez[u][3];
		if(leng[aa]<leng[bb]) swap(aa,bb);
		if(leng[bb]<leng[cc]) swap(bb,cc);
		if(leng[aa]<leng[bb]) swap(aa,bb);
		if(leng[cc]>=flag){
			dp[u]+=3;
		}else if(leng[bb]>=flag){
			dp[u]+=2; leng[u]+=leng[cc];
		}else
		if(leng[bb]+leng[cc]>=flag&&leng[aa]>=flag){
			dp[u]+=2;
		}else
		if(leng[bb]+leng[cc]>=flag){
			dp[u]++; leng[u]+=leng[aa];
		}else if(leng[aa]+leng[cc]>=flag){
			dp[u]++; leng[u]+=leng[bb];
		}else if(leng[aa]+leng[bb]>=flag){
			dp[u]++; leng[u]+=leng[cc];
		}else leng[u]+=leng[aa];
	}
}

void solve4(){
	memset(bo,0,sizeof(bo));
	bo[1]=1;
	dfs1(1,0);
	int l=0,r=sum,res=0;
	for(int i=1;i<=n;i++)
		gg[i]=leng[i];
	while(l<=r){
		memset(dp,0,sizeof(dp));
		int mid=(l+r)>>1;
		tree(1,mid);
		if(dp[1]>=m){
			res=mid;
			l=mid+1;
		}else r=mid-1;
		for(int i=1;i<=n;i++)
			leng[i]=gg[i];
	}
	printf("%d",res);
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	scanf("%d%d",&n,&m); num=0; boo=true; sum=0;
	for(int i=1;i<n;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		if(x>y) swap(x,y);
		if(y!=x+1) boo=false;
		add(x,y,z);
		add(y,x,z);
		road[i]=z; a[x]=z; sum+=z;
		if(x==1) num++;
	}
	if(m==1){
		solve1();
	}else if(num==n-1){
		solve2();
	}else if(boo){
		solve3();
	}else solve4();
}
