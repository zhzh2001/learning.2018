#include<cstring>
#include<cmath>
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<map>

#define N 50007
#define inf 1000000007

#define zz map<int,int>::iterator
#define fb(i,x) for(int i=x;i!=-1;i=nxt[i])
#define fo(i,x,y) for(int i=x;i<=y;i++)
#define fd(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
template<typename T>
inline void read(T &x)
{
	int f=1;x=0;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	x=x*f;
}

int n,m,g,num,ci;
int cnt,hed[N],nxt[N<<1],rea[N<<1],val[N<<1];
map<int,int>p[N];

void add(int u,int v,int z)
{
	nxt[++cnt]=hed[u];
	hed[u]=cnt;
	rea[cnt]=v;
	val[cnt]=z;
}
void dfs(int u,int f,int z)
{
	fb(i,hed[u])
	{
		int v=rea[i];
		if (v==f) continue;
		dfs(v,u,val[i]);
	}
	
	int mx=0;
	zz ty=p[u].begin(),t=ty;
	t++;
	for(;(*t).first!=inf;)
	{
		zz s;
		while((*t).second>=1)
		{
			s=p[u].lower_bound(num-(*t).first);
			if (s==t)
			{
				if ((*t).second>=2)
				{
					g++;
					(*t).second-=2;
				}
				else
				{
					s++;
					if ((*s).first>num) break;
					g++;
					(*s).second--;
					(*t).second--;
					if ((*s).second==0) p[u].erase(s);
				}
			}
			else
			{
				if ((*s).first>num) break;
				g++;
				(*s).second--;
				(*t).second--;
				if ((*s).second==0) p[u].erase(s);
			}
		}
		if ((*t).second>0) mx=(*t).first;
		p[u].erase(t);
		t=ty;
		t++;
	}
	if (z+mx>=num) g++;
	else p[f][z+mx]++;
}
bool judge()
{
	g=0;
	dfs(1,0,0);
	return g>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	memset(hed,-1,sizeof(hed));
	
	read(n),read(m);
	int l=1,r=0,x,y,z;
	fo(i,1,n-1)
	{
		read(x),read(y),read(z);
		add(x,y,z),add(y,x,z);
		r+=z;
	}
	fo(i,1,n)
		p[i][inf]=1,p[i][0]=1;
	r/=m;r+=1;
	while(l<r)
	{
		int mid=(l+r+1)>>1;num=mid;
		if (judge()) l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
	return 0;
}
