#include<cstring>
#include<cmath>
#include<iostream>
#include<cstdio>
#include<algorithm>

#define N 107
#define M 25007
#define ll long long

#define fo(i,x,y) for(int i=x;i<=y;i++)
#define fd(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
template<typename T>
inline void read(T &x)
{
	int f=1;x=0;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	x=x*f;
}

int n;
int a[N];
bool boo[M];

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	
	boo[0]=true;
	int T;read(T);
	while(T--)
	{
		read(n);
		fo(i,1,n)read(a[i]);
		sort(a+1,a+n+1);
		
		int ans=0;
		fo(i,1,n)
		{
			if (!boo[a[i]])
			{
				ans++;
				fo(j,0,a[n]-a[i])
					if (boo[j]) boo[j+a[i]]=true;
			}
		}
		printf("%d\n",ans);
		fo(i,1,a[n]) boo[i]=false;
	}
	return 0;
}
