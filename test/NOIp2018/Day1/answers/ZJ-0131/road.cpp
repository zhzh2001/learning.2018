#include<cstring>
#include<cmath>
#include<iostream>
#include<cstdio>
#include<algorithm>

#define N 100007
#define ll long long

#define fo(i,x,y) for(int i=x;i<=y;i++)
#define fd(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
template<typename T>
inline void read(T &x)
{
	int f=1;x=0;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	x=x*f;
}

int n;
int a[N];

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	read(n);
	fo(i,1,n)read(a[i]);
	int ans=0;
	fo(i,1,n)
		if (a[i]>a[i-1]) ans+=a[i]-a[i-1];
	printf("%d\n",ans);
	return 0;
	
}
