#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=105,N=25005;
int CAS,n,mx,ans,A[M];
bool dp[N];

int main(){
	srand(time(NULL));
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	rd(CAS);
	for(int cas=1;cas<=CAS;cas++){
		memset(A,0,sizeof(A));
		memset(dp,0,sizeof(dp));
		ans=mx=0;
		dp[0]=1;
		rd(n);
		for(int i=1;i<=n;i++)rd(A[i]),MAX(mx,A[i]);
		sort(A+1,A+1+n);
		for(int i=1;i<=n;i++){
			if(dp[A[i]])continue;
			ans++;
			for(int j=0;j+A[i]<=mx;j++)if(dp[j])dp[j+A[i]]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
