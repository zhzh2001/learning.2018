#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=105,N=25005;
int CAS,n,mx,ans,A[M];
bitset<N>dp,use;
bool check(int k){
	dp.reset();
	use.reset();
	dp[0]=true;
	for(int i=1;i<=n;i++)if(i!=k&&A[i])use[A[i]]=true;
	for(int i=0;i<A[k];i++)if(dp[i])dp|=use<<i;
	return dp[A[k]]==false;
}
int main(){
	srand(time(NULL));
	freopen("money.in","r",stdin);
	freopen("std.out","w",stdout);
	rd(CAS);
	for(int cas=1;cas<=CAS;cas++){
		memset(A,0,sizeof(A));
		ans=mx=0;
		rd(n);
		for(int i=1;i<=n;i++)rd(A[i]),MAX(mx,A[i]);
		sort(A+1,A+1+n);
		for(int i=1;i<n;i++)if(A[i]==A[i+1])A[i]=0;
		for(int i=1;i<=n;i++)if(check(i))ans++;
		printf("%d\n",ans);
	}
	return 0;
}
