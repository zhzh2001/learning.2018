#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=1e5+5;
int n,A[M],mi[M<<2];
void build(int l=1,int r=n,int p=1){
	if(l==r){
		mi[p]=l;
		return;
	}
	int mid=l+r>>1;
	build(l,mid,p<<1);
	build(mid+1,r,p<<1|1);
	if(A[mi[p<<1]]<=A[mi[p<<1|1]])mi[p]=mi[p<<1];
	else mi[p]=mi[p<<1|1];
}
int query(int a,int b,int l=1,int r=n,int p=1){
	if(l>b||r<a)return M-1;
	if(l>=a&&r<=b)return mi[p];
	int mid=l+r>>1;
	int L=query(a,b,l,mid,p<<1);
	int R=query(a,b,mid+1,r,p<<1|1);
	return A[L]<=A[R]?L:R;
}
int dfs(int l,int r,int d){
	if(l==r)return A[l]-d;
	int mid=query(l,r);
	int ans=A[mid]-d;
	if(mid>l)ans+=dfs(l,mid-1,A[mid]);
	if(mid<r)ans+=dfs(mid+1,r,A[mid]);
	return ans;
}
int main(){
	srand(time(NULL));
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	rd(n);
	for(int i=1;i<=n;i++)rd(A[i]);
	A[M-1]=1e9;
	build();
	printf("%d\n",dfs(1,n,0));
	return 0;
}
