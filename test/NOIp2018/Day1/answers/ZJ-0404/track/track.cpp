#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=1e5+5;
int n,m,tot,head[M],to[M<<1],co[M<<1],nxt[M<<1];
void add(int a,int b,int c){
	to[++tot]=b;
	co[tot]=c;
	nxt[tot]=head[a];
	head[a]=tot;
}
int is_chain=1,all_co=0,cost[M<<1];
int all_a1=1;
int mxde,de[M];
namespace P20{
	int mxID,mx;
	void dfs(int x,int f,int d){
		if(d>mx)mxID=x,mx=d;
		for(int i=head[x];i;i=nxt[i]){
			int y=to[i];
			if(y==f)continue;
			dfs(y,x,d+co[i]);
		}
	}
	void solve(){
		dfs(1,0,0);
		dfs(mxID,0,0);
		printf("%d\n",mx);
	}
}
namespace P40{
	bool check(int mid){
		int cnt=0,sum=0;
		for(int i=1;i<n;i++){
			sum+=cost[i];
			if(sum>=mid)cnt++,sum=0;
		}
		return cnt>=m;
	}
	void solve(){
		int l=1,r=all_co/m,res=0;
		while(l<=r){
			int mid=l+r>>1;
			if(check(mid))res=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",res);
	}
}
namespace P55{
	void solve(){
		n--;
		sort(cost+1,cost+1+n);
		for(int i=1;i<=n/2;i++)swap(cost[i],cost[n+1-i]);
		int ans=all_co;
		for(int i=1;i<=m;i++)MIN(ans,cost[i]+cost[2*m+1-i]);
		printf("%d\n",ans);
	}
}
namespace P80{
	void solve(){
		
	}
}
int main(){
	srand(time(NULL));
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	rd(n),rd(m);
	for(int i=1;i<n;i++){
		int a,b,c;
		rd(a),rd(b),rd(c);
		add(a,b,c),add(b,a,c);
		all_co+=c;
		cost[i]=c;
		de[a]++,de[b]++;
		if(a+1!=b)is_chain=0;
		if(a!=1)all_a1=0;
	}
	for(int i=1;i<=n;i++)MAX(mxde,de[i]);
	if(0);
	else if(m==1)P20::solve();
	else if(is_chain)P40::solve();
	else if(all_a1)P55::solve();
	else if(mxde<=3)P80::solve();
//	else P80::solve();
	return 0;
}
