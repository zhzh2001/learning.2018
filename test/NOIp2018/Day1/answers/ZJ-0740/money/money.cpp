#include<bits/stdc++.h>
using namespace std;
#define fo(i,j,k) for(int i=(j);i<=(k);i++)
#define fd(i,j,k) for(int i=(j);i>=(k);i--)
#define fe(x,i) for(int i=head[x];i!=-1;i=mp[i].next)
#define max(a,b) ((a)<(b)?(b):(a))
#define min(a,b) ((a)<(b)?(a):(b))
int n,t,cnt;
int num[201],used[201];
int f[50001];
void read(int &x)
{
	char c=getchar();int b=1;
	for(;c<'0'||c>'9';c=getchar()) if (c=='-') b=-1;
	for(x=0;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	x*=b;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(t);
	while (t--)
	{
		memset(used,0,sizeof(used));
		cnt=0;
		read(n);
		fo(i,1,n)
		read(num[i]);
		sort(num+1,num+n+1);
		fo(i,1,n)
		fo(j,i+1,n)
		if (num[j]%num[i]==0) used[j]=1;
		fo(i,1,n)
		if (!used[i]) num[++cnt]=num[i];
		n=cnt;
		memset(f,0,sizeof(f));
		f[0]=1;
		fo(i,1,25000)
		if (i*num[1]<=25000) f[i*num[1]]=1;
		else break;
		fo(i,2,n)
		{
			if (f[num[i]]) 
			{
				cnt--;
				continue;
			}
			fo(j,0,25000)
			if (f[j]&&j+num[i]<=25000)
			f[j+num[i]]=1;
		}
		printf("%d\n",cnt);
	}
	return 0;
}
