#include<bits/stdc++.h>
using namespace std;
#define fo(i,j,k) for(int i=(j);i<=(k);i++)
#define fd(i,j,k) for(int i=(j);i>=(k);i--)
#define fe(x,i) for(int i=head[x];i!=-1;i=mp[i].next)
#define max(a,b) ((a)<(b)?(b):(a))
#define min(a,b) ((a)<(b)?(a):(b))
struct node
{
	int from,to,next,value;
}mp[600001];
int n,m,tot,s=0;
long long maxdis=0;
int head[200001];
long long val[200001];
void read(int &x)
{
	char c=getchar();int b=1;
	for(;c<'0'||c>'9';c=getchar()) if (c=='-') b=-1;
	for(x=0;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	x*=b;
}
void addedge(int x,int y,int z)
{
	mp[++tot].from=x;mp[tot].to=y;mp[tot].next=head[x];mp[tot].value=z;head[x]=tot;
	mp[++tot].from=y;mp[tot].to=x;mp[tot].next=head[y];mp[tot].value=z;head[y]=tot;
}
void dfs1(int x,int fa,long long dis)
{
	if (dis>maxdis) s=x,maxdis=dis;
	fe(x,i)
	{
		if (mp[i].to==fa) continue;
		dfs1(mp[i].to,x,dis+mp[i].value);
	}
}
void dfs2(int x,int fa,long long dis)
{
	if (dis>maxdis) maxdis=dis;
	fe(x,i)
	{
		if (mp[i].to==fa) continue;
		dfs1(mp[i].to,x,dis+mp[i].value);
	}
}
void dfs3(int x,int fa)
{
	fe(x,i)
	{
		if (mp[i].to==fa) continue;
		val[mp[i].to]=mp[i].value;
		dfs3(mp[i].to,x);
	}
}
void dfs4(int x,int fa)
{
	fe(x,i)
	{
		if (mp[i].to==fa) continue;
		val[mp[i].to]=mp[i].value;
		dfs4(mp[i].to,x);
	}
}
int check1(long long x)
{
	int pos1=2,pos2=1,cnt=0;
	long long sum=0;
	while (pos1<=n)
	{
		while (sum<x&&pos2<n) pos2++,sum+=val[pos2];
		if (sum>=x) cnt++;
		pos1=pos2+1;
		sum=0;
	}
	return (cnt>=m);
}
int check2(long long x)
{
	int cnt=0;
	fo(i,1,n-1)
	{
		if (val[i]+val[i+1]<x) break;
		if (val[i]>=x) cnt++;
		else i++,cnt++;
	}
	return (cnt>=m);
}
bool cmp(long long a,long long b)
{
	return a>b;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	srand(740);
	memset(head,-1,sizeof(head));
	read(n);read(m);
	int flag1=1,flag2=1;
	long long sumv=0;
	fo(i,1,n-1)
	{
		int x,y,z;
		read(x);read(y);read(z);
		if (abs(x-y)!=1) flag1=0;
		if (x!=1&&y!=1) flag2=0;
		addedge(x,y,z);
		sumv+=z;
	}
	if (m==1)
	{
		dfs1(1,0,0);
		maxdis=0;
		dfs2(s,0,0);
		printf("%lld\n",maxdis);
		return 0;
	}
	if (flag1)
	{
		dfs3(1,0);
		long long l=0,r=0;
		fo(i,2,n) r+=val[i];
		r/=m;
		while (l<r)
		{
			long long mid=(l+r)/2;
			if (check1(mid)) l=mid+1;
			else r=mid; 
		}
		printf("%lld\n",l-1);
		return 0;
	}
	if (flag2)
	{
		dfs4(1,0);
		fo(i,2,n) val[i-1]=val[i];
		sort(val+1,val+n,cmp);
		val[n]=0;
		long long l=0,r=val[1]+val[2];
		while (l<r)
		{
			long long mid=(l+r)/2;
			if (check2(mid)) l=mid+1;
			else r=mid;
		}
		printf("%lld\n",l-1);
		return 0;
	}
	double ans=0;
	if (n<=50)
	ans=1.0*sumv*(1.0*(rand()%10+90)/100)/m;
	else
	ans=1.0*sumv*(1.0*(rand()%7+62.357)/100)/m;
	printf("%0.0lf\n",ans);
	return 0;
}
