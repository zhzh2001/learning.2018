#include<bits/stdc++.h>
using namespace std;
#define fo(i,j,k) for(int i=(j);i<=(k);i++)
#define fd(i,j,k) for(int i=(j);i>=(k);i--)
#define fe(x,i) for(int i=head[x];i!=-1;i=mp[i].next)
#define max(a,b) ((a)<(b)?(b):(a))
#define min(a,b) ((a)<(b)?(a):(b))
int n,maxx=0,minn=1e+9;
long long ans=0;
int h[100001];
int used[200001];
vector<int> pos[10001];
void read(int &x)
{
	char c=getchar();int b=1;
	for(;c<'0'||c>'9';c=getchar()) if (c=='-') b=-1;
	for(x=0;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	x*=b;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	fo(i,1,n) read(h[i]),maxx=max(h[i],maxx),minn=min(h[i],minn),pos[h[i]].push_back(i);
	ans=minn;
	int len=1;
	used[0]=used[n+1]=1;
	fo(i,minn+1,maxx)
	{
		fo(j,0,(int)pos[i-1].size()-1) 
		{
			int k=pos[i-1][j];
			used[k]=1;
			if (used[k+1]==1&&used[k-1]==1) len--;
			if (k==1||k==n) continue;
			if (used[k-1]==0&&used[k+1]==0) len++;
		}
		ans+=len;
	}
	printf("%lld\n",ans);
	return 0;
}
