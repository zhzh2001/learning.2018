#include<bits/stdc++.h>
using namespace std;
#define maxn 50010
#define LL long long 
struct egde
{
	int y,v,next;
}e[maxn*2];
struct Orz
{
	int x,y,lca;
	LL val;
}a[1000010];
int cnt=0;
int linkk[maxn],len=1;
int N,M,Root,k=0,T;
LL max1[maxn],max2[maxn];
LL dis[maxn];
bool vis[maxn*2];
int deg[maxn],p[maxn];
LL d=0,Sum=0,ans=0;
bool flag=1,sig=1;
int f[20][maxn];
int deep[maxn];
bool mycmp(Orz a,Orz b) {
	return a.val<b.val;
}
int read()
{
	int s=1,w=0;
	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())
		if(ch=='-') s=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())
		w=(w<<3)+(w<<1)+ch-48;
	return s*w;
}
void insert(int xx,int yy,int vv) {
	e[++len].next=linkk[xx];
	e[len].y=yy;
	e[len].v=vv;
	linkk[xx]=len;
}
void init()
{
	N=read();M=read();
	for(int i=1;i<N;++i) {
		int s,t,v;
		s=read();t=read();v=read();
		++deg[s];++deg[t];
		Sum+=v;
		if(t!=s+1) flag=0;
		if(s!=1) sig=0;
		insert(s,t,v);
		insert(t,s,v);
	}
}
void DFS(int x,int fa) {
	for(int i=linkk[x];i;i=e[i].next) {
		int y=e[i].y;
		if(y==fa) continue;
		DFS(y,x);
		if(max1[y]+e[i].v>max1[x]) {
			max2[x]=max1[x];
			max1[x]=max1[y]+e[i].v;
		}
		else if(max1[y]+e[i].v>max2[x]) 
			max2[x]=max1[y]+e[i].v;
	}
	d=max(d,max1[x]+max2[x]);
}		
void work1() {
	DFS(1,0);
	printf("%lld\n",d);
}
void dfs(int x,int fa) {
	p[++k]=x;
	for(int i=linkk[x];i;i=e[i].next) {
		int y=e[i].y;
		if(y==fa) continue;
		dis[y]=dis[x]+e[i].v;
		deep[y]=deep[x]+1;
		f[0][y]=x;
		dfs(y,x);
	}
}
bool check(LL x) {
	int c=0,now=p[1];
	for(int i=1;i<=k;++i) {
		if(dis[p[i]]-dis[now]>=x) c++,now=p[i];
		if(c>=M) return 1;
	}
	return 0;
}
void work2() {
	Root=0;
	for(int i=1;i<=N;++i) 
		if(deg[i]==1) { Root=i; break; }
	dis[Root]=0;
	dfs(Root,0);
	LL L=0,R=Sum;
	while(L<R) {
		LL mid=(L+R)>>1;
		if(check(mid)) ans=max(ans,mid),L=mid+1;
		else R=mid-1;
	}
	if(check(L)) ans=max(ans,L);
	printf("%lld\n",ans);
}
bool another(LL x) {
	int c=0,j=2;
	for(int i=N;i;--i) {
		if(dis[i]>=x) { c++; continue; }
		while(j<i&&dis[i]+dis[j]<x) ++j;
		if(j>=i) break;
		++j;++c;
		if(c>=M) return 1;
	}
	return 0;
}
void work3() {
	dis[1]=0;ans=0;
	for(int i=linkk[1];i;i=e[i].next) {
		int y=e[i].y;
		dis[y]=e[i].v;
	}
	sort(dis+1,dis+1+N);
	LL L=0,R=Sum;
	while(L<R) {
		LL mid=(L+R)>>1;
		if(another(mid)) ans=max(ans,mid),L=mid+1;
		else R=mid-1;
	}
	if(another(L)) ans=max(ans,L);
	printf("%lld\n",ans);
}
int get(int x,int y) {
	if(deep[x]>deep[y]) swap(x,y);
	for(int i=T;i>=0;--i) 
		if(deep[f[i][y]]>=deep[x]) y=f[i][y];
	if(x==y) return x;
	for(int i=T;i>=0;--i) 
		if(f[i][y]!=f[i][x]) y=f[i][y],x=f[i][x];
	return f[0][x];
}
void Dfs(int x,int END) {
	if(x==END) return;
	for(int i=linkk[x];i;i=e[i].next) {
		int y=e[i].y;
		if(deep[y]>=deep[x]) continue;
		if(vis[i]||vis[i^1]) { flag=0; return; }
		Dfs(y,END);
	}
}
void change(int x,int END) {
	if(x==END) return;
	for(int i=linkk[x];i;i=e[i].next) {
		int y=e[i].y;
		if(deep[y]>=deep[x]) continue;
		vis[i]=vis[i^1]=1;
		change(y,END);
	}
}
bool find(LL x) {
	int c=0;
	memset(vis,0,sizeof(vis));
	for(int i=1;i<=cnt;++i)
		if(a[i].val>=x) {
			flag=1;
			Dfs(a[i].x,a[i].lca);
			Dfs(a[i].y,a[i].lca);
			if(!flag) continue;
			c++;
			change(a[i].x,a[i].lca);
			change(a[i].y,a[i].lca);
			if(c>=M) return 1;
		}
	return 0;
}
void work4() {
	ans=0;deep[1]=1;
	dfs(1,0);
	T=log(N)/log(2)+1;
	for(int i=1;i<=T;++i) 
		for(int j=1;j<=N;++j)
			f[i][j]=f[i-1][f[i-1][j]];
	for(int i=1;i<=N;++i) 
		for(int j=i+1;j<=N;++j) {
			int Lca=get(i,j);
			a[++cnt]=(Orz){i,j,Lca,dis[i]+dis[j]-dis[Lca]*2};
		}
	sort(a+1,a+1+cnt,mycmp);
	LL L=0,R=Sum;
	while(L<R) {
		LL mid=(L+R)>>1;
		if(find(mid)) ans=max(ans,mid),L=mid+1;
		else R=mid-1;
	}
	if(find(L)) ans=max(ans,L);
	printf("%lld\n",ans);
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	init();
	if(M==1) work1();
	else if(flag) work2();
	else if(sig) work3();
	else work4();
	return 0;
}
