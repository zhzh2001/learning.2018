#include<bits/stdc++.h>
using namespace std;
#define maxn 100010
int a[maxn],b[maxn];
int N;
long long ans=0;
int read()
{
	int s=1,w=0;
	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())
		if(ch=='-') s=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())
		w=(w<<3)+(w<<1)+ch-48;
	return s*w;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	N=read();
	a[0]=0;
	for(int i=1;i<=N;++i) {
		a[i]=read();
		b[i]=a[i]-a[i-1];
	}
	for(int i=1;i<=N;++i) 
		if(b[i]>0) ans+=b[i];
	printf("%lld\n",ans);
	return 0;
}
