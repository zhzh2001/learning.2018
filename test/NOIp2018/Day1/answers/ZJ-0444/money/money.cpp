#include<bits/stdc++.h>
using namespace std;
#define maxn 30000
int f[maxn];
int N,M=0,T;
int Max=0;
int a[maxn];
int read()
{
	int s=1,w=0;
	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())
		if(ch=='-') s=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())
		w=(w<<3)+(w<<1)+ch-48;
	return s*w;
}
void init()
{
	Max=0;
	N=read();
	for(int i=1;i<=N;++i) {
		a[i]=read();
		Max=max(Max,a[i]);
	}
}
void work()
{
	memset(f,0,sizeof(f));
	f[0]=1;M=0;
	sort(a+1,a+1+N);
	for(int i=1;i<=N;++i) {
		if(!f[a[i]]) M++;
		f[a[i]]=1;
		for(int j=a[i];j<=Max;++j) {
			if(f[j-a[i]]) f[j]=1;
		}
	}
	printf("%d\n",M);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--) {
		init();
		work();
	}	
	return 0;
}
