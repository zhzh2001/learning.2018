#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<string>
using namespace std;
long long t,n;
long long a[200];
long long m,ans;
bool f;
bool sure(long long x);
void add(long long x,long long now);
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%lld",&t);
	for(long long i=1;i<=t;i++){
		m=0;
		scanf("%lld",&n);
		for(long long j=1;j<=n;j++)
			scanf("%d",&a[j]);
		sort(a+1,a+n+1);
		m++;
		for(long long j=2;j<=n;j++){
			if(!sure(j))m++;
		}
		printf("%lld\n",m);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
bool sure(long long x){
	f=false;
	for(long long k=x-1;k>0;k--){
		ans=0;
		add(k,x);
	}
	if(f)return true;
	return false;
}
void add(long long x,long long now){
	if(!f){
		long long c=0;
		while(ans<a[now]){
			c++;
			ans+=a[x];
			if(ans==a[now]){
				f=true;
				return;
			}
			for(long long k=x-1;k>0;k--)
				add(k,now);
		}
		ans-=a[x]*c;
	}
}
