#include<bits/stdc++.h>
using namespace std;
typedef long long LL;
const int N=111;
const int M=30303;
int a[N];
bool ok[M];
bool ok2[M];
void work(){
	int n;
	scanf("%d",&n);
	for(int i=0;i<n;i++)
		scanf("%d",&a[i]);
	memset(ok,false,sizeof(ok));
	memset(ok2,false,sizeof(ok2));
	ok[0]=true;
	for(int i=0;i<n;i++){
		for(int j=0;j+a[i]<M;j++)
			ok[j+a[i]]|=ok[j];
	}
//	for(int i=0;i<100;i++)
//		if(ok[i])printf("%d ",i);
	int ans=0;
	ok2[0]=true;
	for(int i=1;i<M;i++){
		if(ok[i]&&!ok2[i]){
			ans++;
			for(int j=0;j+i<M;j++){
				ok2[j+i]|=ok2[j];
			}
		}
	}
	printf("%d\n",ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--)
		work();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
