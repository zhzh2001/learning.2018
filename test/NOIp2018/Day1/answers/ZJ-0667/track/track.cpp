#include<bits/stdc++.h>
using namespace std;
typedef long long LL;
const int N=50505;
int n,m;
struct E{
	int v,w;
};
vector<E> es[N];
inline void ae(int u,int v,int w){
	es[u].push_back((E){v,w});
}
#define Fs(x) \
for(vector<E>::iterator it=es[x].begin();\
it!=es[x].end();it++)
int up[N];
bool cmp(const E a,const E b){
	return up[a.v]<up[b.v];
}
int res,nx;
int tmp[N];
int banmax(int n,int ban){
	int l=0,r=n-1;
	if(l==ban)l++;
	if(r==ban)r--;
	int tres=0;
	while(l<r){
		while(l<r&&tmp[l]+tmp[r]<nx){
			l++;
			if(l==ban)l++;
		}
		if(l>=r)break;
		if(tmp[l]+tmp[r]>=nx){
			tres++;
			r--;
			if(r==ban)r--;
			l++;
			if(l==ban)l++;
		} 
	}
	return tres;
}
void dfs(int x){
	Fs(x){
		dfs(it->v);
		if(res>=m)return;
		up[it->v]+=it->w;
	}
	int sc=es[x].size();
	for(int i=0;i<sc;i++)tmp[i]=up[es[x][i].v];
	sort(tmp,tmp+sc);
//	printf("dfs %d:",x);
//	Fs(x)printf("[%d]%d ",it->v,up[it->v]);
//	printf("\n");
	while(sc>0&&tmp[sc-1]>=nx){
		sc--,res++;
	}
	int mres=banmax(sc,-1);
	res+=mres;
	if(res>=m)return;
	int l=-1,r=sc-1;
	while(l<r){
		int mid=(l+r+1)>>1;
		if(banmax(sc,mid)==mres){
			l=mid;
		}else{
			r=mid-1;
		}
	}
	if(l==-1)up[x]=0;
	else up[x]=tmp[l];
}
const int root=1;
bool test(int x){
//	printf("test %d\n",x);
	res=0,nx=x;
	dfs(root);
//	printf("res=%d\n",res);
	return res>=m;
}
void dfs0(int x,int fa){
	Fs(x){
		if(it->v==fa){
			swap(*it,*(es[x].end()-1));
		}
		if(it->v!=fa)dfs0(it->v,x);
	}
	if(x!=root)es[x].pop_back();
//	printf("%d:",x);
//	Fs(x)printf("%d ",it->v);
//	printf("\n");
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int sum=0;
	for(int i=0;i<n-1;i++){
		int a,b,l;
		scanf("%d%d%d",&a,&b,&l);
		ae(a,b,l);
		ae(b,a,l);
		sum+=l;
	}
	dfs0(root,-1);
	int l=0,r=sum;
	while(l<r){
		int mid=(l+r+1)>>1;
		if(test(mid))l=mid;
		else r=mid-1;
	}
	printf("%d",l);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
