#include<bits/stdc++.h>
using namespace std;
typedef long long LL;
const int N=100100;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	int n;
	scanf("%d",&n);
	int now,last=0;
	LL ans=0;
	for(int i=0;i<n;i++){
		scanf("%d",&now);
		if(now>last)ans+=now-last;
		last=now;
	}
	printf("%lld",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
