#include<bits/stdc++.h>
#define rint register int
#define rep(i,a,b) for (rint i=(a),_E=(b); i<=_E; ++i)
#define per(i,a,b) for (rint i=(a),_E=(b); i>=_E; --i)
#define REP(i,n) for (rint i=(0),_E=(n); i<_E; ++i)
#define loop(k,a) for (rint k=head[a]; k; k=e[k].link)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
using namespace std;
typedef pair<int,int> pii;
typedef vector<int> vi;
typedef long long ll;

template <class T> inline void read(T &x) {
	char ch = getchar(); int op = 0; x = 0;
	while (!isdigit(ch)) op |= (ch == '-'), ch = getchar();
	while (isdigit(ch)) x = x * 10 + ch - '0', ch = getchar();
	if (op) x = -x;
}

const int N = 100005;
struct edge {
	int v, w, link;
} e[N << 1];
int head[N], tot = 1;
int n, K;

inline void addEdge(int a, int b, int c) {
	e[++tot] = (edge) {b, c, head[a]};
	head[a] = tot;
}

int f[N], lim, cnt;

inline int calc(vi &a, int k) {
	int res = 0, l = 0;
	per (i, a.size() - 1, 0) if (i != k) {
		while (l < i && (l == k || a[l] + a[i] < lim)) ++l;
		if (l >= i) break;
		++ res, ++ l;
	}
	return res;
}

int doit(vi &a) {
	sort(a.begin(), a.end());
	int l = -1, r = a.size() - 1, mid;
	int best = calc(a, r + 1);
	while (l < r) {
		mid = (l + r + 1) >> 1;
		if (calc(a, mid) == best) l = mid;
		else r = mid - 1;
	}
	cnt += best;
	if (l == -1) return 0;
	return a[l];
}

void dfs(int u, int fa) {
	vi tmp;
	loop (k, u)
		if (e[k].v != fa) {
			dfs(e[k].v, u);
			f[e[k].v] += e[k].w;
			if (f[e[k].v] >= lim) ++ cnt;
			else tmp.pb(f[e[k].v]);
		}
	if (!tmp.empty()) f[u] = doit(tmp);
	else f[u] = 0;
}

bool check(int x) {
	lim = x, cnt = 0;
	dfs(1, 0);
	return cnt >= K;
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);

	read(n), read(K);
	int a, b, c, sum = 0;
	rep (i, 1, n - 1) {
		read(a), read(b), read(c);
		addEdge(a, b, c);
		addEdge(b, a, c);
		sum += c;
	}
	
	int l = 0, r = sum / K, mid;
	while (l < r) {
		mid = (l + r + 1) >> 1;
		if (check(mid)) l = mid;
		else r = mid - 1;
	}
	printf("%d\n", l);

	return 0;
}
