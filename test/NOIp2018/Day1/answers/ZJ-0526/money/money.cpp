#include<bits/stdc++.h>
#define rint register int
#define rep(i,a,b) for (rint i=(a),_E=(b); i<=_E; ++i)
#define per(i,a,b) for (rint i=(a),_E=(b); i>=_E; --i)
#define REP(i,n) for (rint i=(0),_E=(n); i<_E; ++i)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
using namespace std;
typedef pair<int,int> pii;
typedef vector<int> vi;
typedef long long ll;

template <class T> inline void read(T &x) {
	char ch = getchar(); int op = 0; x = 0;
	while (!isdigit(ch)) op |= (ch == '-'), ch = getchar();
	while (isdigit(ch)) x = x * 10 + ch - '0', ch = getchar();
	if (op) x = -x;
}

const int N = 250005;
const int inf = 1e9;
int a[N], q[N], n;
ll d[N];

void Init() {
	memset(d, 0x3f, sizeof d);
}

inline int gcd(int a, int b) {
	return !b ? a : gcd(b, a % b);
}

void Main() {
	read(n);
	rep (i, 1, n) read(a[i]);
	sort(a + 1, a + n + 1);

	int ans = 1, g, tot, L, R, p, now, nxt;
	ll mn;
	d[0] = 0; 
	rep (i, 2, n) {
		if (a[i] >= d[a[i] % a[1]]) {
			continue;
		}
		++ ans;
		g = gcd(a[1], a[i]);
		tot = a[1] / g;
		rep (r, 0, g - 1) {
			L = 0, R = 0;
			mn = d[r], p = r;
			rep (t, 0, tot - 1)
				if (d[t * g + r] < mn)
					mn = d[p = t * g + r];
			q[R++] = p;
			while (L < R) {
				now = q[L++];
				nxt = (now + a[i]) % a[1];
				d[nxt] = min(d[nxt], d[now] + a[i]);
				if (nxt != p) q[R++] = nxt;
			}
		}
	}
	printf("%d\n", ans);
}

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);

	int T;
	read(T);
	while (T--) {
		Init();
		Main();
	}

	return 0;
}
