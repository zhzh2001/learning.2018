#include<bits/stdc++.h>
#define rint register int
#define rep(i,a,b) for (rint i=(a),_E=(b); i<=_E; ++i)
#define per(i,a,b) for (rint i=(a),_E=(b); i>=_E; --i)
#define REP(i,n) for (rint i=(0),_E=(n); i<_E; ++i)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
using namespace std;
typedef pair<int,int> pii;
typedef vector<int> vi;
typedef long long ll;

template <class T> inline void read(T &x) {
	char ch = getchar(); int op = 0; x = 0;
	while (!isdigit(ch)) op |= (ch == '-'), ch = getchar();
	while (isdigit(ch)) x = x * 10 + ch - '0', ch = getchar();
	if (op) x = -x;
}

const int N = 1000005;
int n, x[N];
ll ans;

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);

	read(n);
	rep (i, 1, n) read(x[i]);
	rep (i, 1, n)
		if (x[i] > x[i - 1])
			ans += x[i] - x[i - 1];
	cout << ans << endl;

	return 0;
}
