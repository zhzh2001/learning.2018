var
i,j,t,step,n,max,min,ans:longint;
a:array[0..500]of longint;
b:array[-26000..26000]of int64;
begin
  assign(input,'money.in');reset(input);
  assign(output,'money.out');rewrite(output);
  read(t);
  for step:=1 to t do
  begin
    read(n);
    max:=0;
    min:=maxlongint;
    for i:=1 to n do
    begin
      read(a[i]);
      if a[i]>max then max:=a[i];
      if a[i]<min then min:=a[i];
    end;
    fillchar(b,sizeof(b),0);
    b[0]:=1;
    for i:=min to max do
      for j:=1 to n do
      if b[i-a[j]]>0 then inc(b[i]);
    ans:=n;
    for i:=1 to n do
      if b[a[i]]>1 then dec(ans);
    writeln(ans);
  end;
  close(input);
  close(output);
end.
