#include <bits/stdc++.h>
#define LL long long
using namespace std;
void fff(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
}
int read(){
	int x=0,m=1;char ch=getchar();
	while((ch>'9'||ch<'0')&&ch!='-') ch=getchar();
	if(ch=='-') m=-1,ch=getchar();
	while(ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*m;
}
const int N=100100;
int n;
int a[N];
int main(){
	fff();
	n=read();
	LL ans=0;
	for(int i=1;i<=n;i++) {
		a[i]=read();
		if(a[i]>a[i-1]) ans+=(LL)a[i]-a[i-1];
	}
	cout<<ans;
}
