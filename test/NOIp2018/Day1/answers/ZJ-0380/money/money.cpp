#include <bits/stdc++.h>
using namespace std;
void fff(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}
int read(){
	int x=0,m=1;char ch=getchar();
	while((ch>'9'||ch<'0')&&ch!='-') ch=getchar();
	if(ch=='-') m=-1,ch=getchar();
	while(ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*m;
}
const int N=110;
const int M=25010;
int n;
int a[N];
int f[M];
bool vis[M];
int main(){
	fff();
	int T;T=read();
	while(T--){
		n=read();
		memset(f,0,sizeof(f));
		memset(vis,false,sizeof(vis));
		for(int i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+n+1);
		if(a[1]==1){
			printf("%d\n",1);
			continue;
		}
		
		for(int i=1;i<=n;i++){
			for(int j=a[i];j<=a[n];j++){
				if(j==a[i]||vis[j-a[i]]){
					vis[j]=true;
					f[j]++;
				}
			}
		}
		int cnt=0;
		for(int i=1;i<=n;i++){
			if(f[a[i]]==1) cnt++;
		}
		printf("%d\n",cnt);
	}
}
