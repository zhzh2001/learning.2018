#include <bits/stdc++.h>
#define gc getchar()
using namespace std;
const int N=109;
const int M=25009;
int n,a[N],vis[M],ans;
int read()
{
	int x=1;
	char ch;
	while (ch=gc,ch<'0'||ch>'9') if (ch=='-') x=-1;
	int s=ch-'0';
	while (ch=gc,ch>='0'&&ch<='9') s=s*10+ch-'0';
	return s*x;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T=read();
	while (T--)
	{
		n=read(),ans=0;
		for (int i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+n+1);
		memset(vis,0,sizeof(vis));
		vis[0]=1;
		for (int i=1;i<=n;i++)
		{
			if (vis[a[i]]) continue;
			ans++;
			for (int j=0;j+a[i]<=25000;j++)
				if (vis[j]) vis[j+a[i]]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}

