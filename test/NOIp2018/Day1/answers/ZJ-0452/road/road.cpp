#include <bits/stdc++.h>
#define gc getchar()
using namespace std;
const int N=100009;
int n,a[N],ans;
int read()
{
	int x=1;
	char ch;
	while (ch=gc,ch<'0'||ch>'9') if (ch=='-') x=-1;
	int s=ch-'0';
	while (ch=gc,ch>='0'&&ch<='9') s=s*10+ch-'0';
	return s*x;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read(),a[0]=0;
	for (int i=1;i<=n;i++)
	{
		a[i]=read();
		if (a[i]>a[i-1]) ans+=a[i]-a[i-1];
	}
	printf("%d\n",ans);
	return 0;
}

