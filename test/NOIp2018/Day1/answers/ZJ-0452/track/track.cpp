#include <bits/stdc++.h>
#define gc getchar()
using namespace std;
const int N=50009;
int n,m;
int first[N],number;
int fa[N],dfn[N],id[N],cnt;
int Max[N],Pre[N],Next[N],a[N];
struct edge
{
	int to,next,val;
	void add(int x,int y,int z)
	{
		to=y,next=first[x],first[x]=number,val=z;
	}
}e[N<<1];
int read()
{
	int x=1;
	char ch;
	while (ch=gc,ch<'0'||ch>'9') if (ch=='-') x=-1;
	int s=ch-'0';
	while (ch=gc,ch>='0'&&ch<='9') s=s*10+ch-'0';
	return s*x;
}
void dfs(int x)
{
	id[dfn[x]=++cnt]=x;
	for (int i=first[x];i;i=e[i].next)
		if (e[i].to!=fa[x]) fa[e[i].to]=x,dfs(e[i].to);
}
bool chk(int lim)
{
	int ret=0;
	for (int i=1;i<=n;i++) Max[i]=0;
	for (int i=n,l,r;i;i--)
	{
		int x=id[i],sz=0;
		for (int j=first[x];j;j=e[j].next)
			if (e[j].to!=fa[x])
			{
				if (Max[e[j].to]+e[j].val>=lim) ret++;
				else a[++sz]=Max[e[j].to]+e[j].val;
			}
		int tmp=ret;
		sort(a+1,a+sz+1);
		for (int j=1;j<=sz+1;j++) Pre[j]=j-1,Next[j-1]=j;
		for (l=1,r=sz;l<=sz;)
		{
			while (a[l]+a[Pre[r]]>=lim&&Pre[r]>l) r=Pre[r];
			if (r>l&&a[l]+a[r]>=lim)
			{
				ret++;
				Pre[Next[l]]=Pre[l];
				Next[Pre[l]]=Next[l];
				Pre[Next[r]]=Pre[r];
				Next[Pre[r]]=Next[r];
				l=Next[Pre[l]];
				r=Next[r];
				if (r==l) r=Next[r];
				if (r==sz+1) r=Pre[r];
			}
			else Max[x]=max(Max[x],a[l]),l=Next[l];
		}
	}
	return ret>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	int l=0,r=1,ans=0;
	for (int i=1;i<n;i++)
	{
		int x=read(),y=read(),z=read();
		e[++number].add(x,y,z),e[++number].add(y,x,z);
		r+=z;
	}
	dfs(1);
	while (l<=r)
	{
		int mid=(l+r>>1);
		if (chk(mid)) ans=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}

