#include<cstdio>
#include<algorithm>
using namespace std;
int n,m;
struct T
{
	int x,y,z;
}a[210000];
int dp[21000][21];
int du[21];
int maxx;
int main()
{
	freopen("track.in","r",stdin);
	freopen("check.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++) scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
	dp[0][0]=1e9;
	for (int i=0;i<(1<<(n-1));i++)
	{
		int d=(1<<n)-1-i;
		for (int j=d;j;j--,j&=d)
		{
			int sum=0;
			for (int k=1;k<=n;k++) du[k]=0;
			for (int k=1;k<=(n-1);k++)
			if (j&(1<<(k-1)))
			{
				du[a[k].x]++;
				du[a[k].y]++;
				sum+=a[k].z;
				}	
			int flag=1,num=0;
			for (int k=1;k<=n;k++)
			if (du[k]!=1&&du[k]!=2&&du[k]!=0) {
				flag=0;
				break;
			} else if (du[k]==1) num++;
			if (num!=2||!flag) continue;
			for (int k=1;k<=m;k++)
				dp[i+j][k]=max(dp[i+j][k],min(sum,dp[i][k-1]));
			maxx=max(maxx,dp[i+j][m]); 
		}
	}
	printf("%d\n",maxx);
}
