#include<cstdio>
#include<algorithm>
#include<vector>
using namespace std;
pair<int,int>tree[210000];
int n,m;
int addnum,vel[410000],ne[410000],head[410000],key[410000];
void add(int u,int v,int k)
{
	addnum++,vel[addnum]=v,key[addnum]=k,ne[addnum]=head[u],head[u]=addnum;
}
void dfs(int u,int fa,int x)
{
	vector<int>o;
	o.clear();
	for (int e=head[u];e;e=ne[e])
	{
		int v=vel[e];
		if (v==fa) continue;
		dfs(v,u,x);
		tree[v].second+=key[e];
		tree[u].first+=tree[v].first;
		if (tree[v].second>=x) tree[u].first++;
		else o.push_back(tree[v].second);
	}
	if (o.empty()) return;
	sort(o.begin(),o.end());
	int dl=0,dr=o.size()-1,num=0;
	while (dl<dr)
	{
		if (o[dl]+o[dr]>=x)
		{
			num++;
			dl++,dr--;
		} else dl++;
	}
	dl=1,dr=o.size()-1;
	int numt=0;
	while(dl<dr)
	{
		if (o[dl]+o[dr]>=x)
		{
			numt++;
			dl++,dr--;
		} else dl++;
	}
	if (numt!=num)
	{
		tree[u].first+=num;
		return;
	}
	int l=0,r=o.size()-1;
	while (l<r)
	{
		int mid=(l+r+1)>>1,nums=0;
		dl=0,dr=o.size()-1;
		while (dl<dr)
		{
			if (dl==mid) dl++;
			if (dr==mid) dr--;
			if (dl>=dr) break;
			if (o[dl]+o[dr]>=x)
			{
				nums++;
				dl++,dr--;
			} else dl++;
		}
		if (nums==num) l=mid; else r=mid-1;
	}
	tree[u].first+=num;
	tree[u].second=o[l];
}
int check(int x)
{
	for (int i=1;i<=n;i++) tree[i]=make_pair(0,0);
	dfs(1,-1,x);
	return tree[1].first>=m;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z),add(y,x,z);
	}
	int l=1,r=1e9;
	while (l<r)
	{
		int mid=(l+r+1)>>1;
		if (check(mid)) l=mid;
		else r=mid-1;
	}
	printf("%d\n",l);
}
