#include<cstdio>
#include<algorithm>
using namespace std;
int n;
int a[210];
int dp[25010];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		int ans=n;
		for (int j=1;j<=25000;j++) dp[j]=1e9;
		dp[0]=0;
		for (int i=1;i<=n;i++)
		{
			if (dp[a[i]]!=1e9) ans--;
			for (int j=a[i];j<=25000;j++)
				dp[j]=min(dp[j],dp[j-a[i]]+1);
		}
		printf("%d\n",ans);
	}
}
