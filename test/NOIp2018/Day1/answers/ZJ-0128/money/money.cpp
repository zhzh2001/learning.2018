#include<bits/stdc++.h>
#define rep(i,x,y) for (int i=(x);i<=(y);i++)
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
const int N=105,M=26005;
int n,a[N],ans,Max; bool f[M];
int Main(){
	scanf("%d",&n); ans=n;
	rep (i,1,n) scanf("%d",&a[i]);
	sort(a+1,a+1+n); Max=a[n];
	memset(f,0,sizeof(f)); f[0]=1;
	rep (i,1,n){
		if (f[a[i]]){ans--; continue;}
		rep (j,a[i],Max) f[j]|=f[j-a[i]];
	}
	printf("%d\n",ans);
	return 0;
}
int main(){
	freopen("money.in","r",stdin); freopen("money.out","w",stdout);
	int T; scanf("%d",&T); while (T--) Main();
	return 0;
}
