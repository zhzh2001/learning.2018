#include<bits/stdc++.h>
#define rep(i,x,y) for (int i=(x);i<=(y);i++)
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
#define ll long long
using namespace std;
#define gc getchar
int read(){
	char ch=gc(); int x=0,op=1;
	for (;!isdigit(ch);ch=gc()) if (ch=='-') op=-1;
	for (;isdigit(ch);ch=gc()) x=(x<<1)+(x<<3)+ch-48;
	return x*op;
}
const int N=1e5+5;
int n,m,x,y,w,cnt,head[N],lim,mx[N],q[N],res,rt,rd[N],a[N],b[N]; bool flag1,flag2,flag3,vis[N];
struct edge{int to,nxt,w;}e[N<<1];
void adde(int x,int y,int w){e[++cnt].to=y; e[cnt].nxt=head[x]; head[x]=cnt; e[cnt].w=w;}
void dfs(int u,int par){
	mx[u]=0;
	for (int i=head[u],v;i;i=e[i].nxt)
		if (v=e[i].to,v!=par) dfs(v,u);
	int top=0;
	for (int i=head[u],v;i;i=e[i].nxt)
		if (v=e[i].to,v!=par) q[++top]=mx[v]+e[i].w;
	if (!top) return;
	sort(q+1,q+1+top,greater<int>());
	if (top==1){
		if (q[1]>=lim) res++; else mx[u]=q[1];
		return;
	}
	if (top==2){
		if (q[2]>=lim) res++,mx[u]=q[1];
		else if (q[1]>=lim) res++,mx[u]=q[2];
		else if (q[1]+q[2]>=lim) res++,mx[u]=0;
		else mx[u]=q[1];
		return;
	}
	if (flag3&&top==3){
		if (q[1]+q[2]>=lim) res++;
		return;
	}
	int i=1,j=top,tmp=0;
	rep (k,1,top) vis[k]=0;
	while (i<=top&&q[i]>=lim) vis[i]=1,i++,tmp++;
	for (;i<j;i++){
		while (i<j&&q[i]+q[j]<lim) j--;
		if (i>=j) break; vis[j]=1; tmp++; j--;
	}
	res+=tmp;
	rep (k,1,top) if (!vis[k]){mx[u]=q[k]; break;}
}
bool check(int x){
	lim=x; res=0;
	if (flag1){
//		cerr<<"AC\n";
		int i=1,j=n-1;
		while (i<n&&a[i]>=lim) res++,i++;
		for (;i<j;i++){
			while (i<j&&a[i]+a[j]<lim) j--;
			if (i<j) res++,j--; else break;
		}
	} else if (flag2){
//		cerr<<"AC\n";
		for (int i=1,j;i<=n-1;){
			if (b[i]>=lim){res++,i++; continue;}
			j=i+1; int sum=b[i];
			while (j<n){
				sum+=b[j];
				if (sum>=lim){res++; break;} j++;
			}
			i=j+1;
		}
//		cerr<<res<<'\n';
	} else{
		dfs(rt,0);
//		cerr<<rt<<' '<<"AC\n";
	}
	return res>=m;
}
int main(){
	freopen("track.in","r",stdin); freopen("track.out","w",stdout);
	n=read(),m=read();
	int l=0,r=0,mid; flag1=1,flag2=1,flag3=1;
	rep (i,1,n-1){
		x=read(),y=read(),w=read(),adde(x,y,w),adde(y,x,w); rd[x]++,rd[y]++;
		r+=w,flag1&=x==1,flag2&=y==x+1; a[i]=b[i]=w;
	}
	sort(a+1,a+n,greater<int>());
	rt=1;
	rep (i,1,n){if (rd[i]>=3) rt=i; flag3&=rd[i]<=3;}
//	cerr<<check(31)<<'\n';
	while (l<=r){
		mid=(l+r)>>1;
		if (check(mid)) l=mid+1; else r=mid-1;
	}
	printf("%d\n",l-1);
	return 0;
}
/*
9 3
1 2 6
2 3 3
3 4 5
4 5 10
6 2 4
7 2 9
8 4 7
9 4 4

6 4
1 2 5
1 3 3
1 4 3
1 5 1
1 6 4

7 1
1 2 10
1 3 5
2 4 9
2 5 8
3 6 6
3 7 7
*/
