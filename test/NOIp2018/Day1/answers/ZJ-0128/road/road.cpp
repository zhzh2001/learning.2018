#include<bits/stdc++.h>
#define rep(i,x,y) for (int i=(x);i<=(y);i++)
#define ll long long
using namespace std;
const int N=1e5+5;
int n,a[N]; ll sum;
int main(){
	freopen("road.in","r",stdin); freopen("road.out","w",stdout);
	scanf("%d",&n);
	rep (i,1,n){
		scanf("%d",&a[i]);
		if (a[i]>a[i-1]) sum+=a[i]-a[i-1];
	}
	printf("%lld\n",sum);
	return 0;
}
