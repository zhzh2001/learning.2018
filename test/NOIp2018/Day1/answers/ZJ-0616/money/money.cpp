#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define N 110 
#define A 25010 

int n,a[N],t,ans;
bool f[A],pd[A];

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		memset(pd,0,sizeof(pd));
		memset(f,0,sizeof(f));
		f[0]=1;
		ans=0;
		scanf("%d",&n);
		for (int i=1;i<=n;++i) scanf("%d",a+i);
		sort(a+1,a+n+1);
		for (int i=1;i<=n;++i)
		{
			if (pd[a[i]]) continue;
			for (int j=a[i];j<=a[n];++j)
			{
				if ((!(f[j])) && (!(j==a[i])) && (f[j-a[i]])) pd[j]=1;
				f[j]=(f[j] || f[j-a[i]]);
			}
		}
		for (int i=1;i<=n;++i)
		{
			if (!pd[a[i]]) ++ans;
		}
		printf("%d\n",ans);
	}
	return 0;
}
