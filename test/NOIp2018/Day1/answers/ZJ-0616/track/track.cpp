#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define N 100010 

struct bian
{
	int a,b,l;
} e[N*2];

int n,m,lc[N],rc[N],fa[N],gen,fir[N],nex[N],p,vlc[N],vrc[N],vis[N];
LL ans;
bool fla;

bool cmp(bian x,bian y)
{
	return x.l>y.l;
}

bool cmp1(bian x,bian y)
{
	return x.a<y.a;
}

void dfs(int i)
{
	for (int j=fir[i];j;j=nex[j])
	{
		if (e[j].b==fa[i]) continue;
		if (!lc[i])
		{
			lc[i]=e[j].b;
			fa[e[j].b]=i;
			vlc[i]=e[j].l;
			dfs(lc[i]);
		}
		else if (!rc[i])
		{
			rc[i]=e[j].b;
			fa[e[j].b]=i;
			vrc[i]=e[j].l;
			dfs(rc[i]);
		}
	}
	return;
}

int dfs1(int i)
{
	if (!lc[i]) return 0;
	int lret=0,rret=0;
	if (lc[i]) lret=vlc[i]+dfs1(lc[i]);
	if (rc[i]) rret=vrc[i]+dfs1(rc[i]);
	return max(lret,rret);
}

int dfs2(int i)
{
	int ret=0;
	vis[i]=1;
	for (int j=fir[i];j;j=nex[j])
	{
		if (!vis[e[j].b]) ret=max(ret,e[j].l+dfs2(e[j].b));
	}
	return ret;
}

bool yanzheng(int changdu)
{
	LL sum=0;
	int co=0;
	for (int i=1;i<n;++i)
	{
		sum+=e[i].l;
		if (sum>=changdu)
		{
			sum=0;
			++co;
			if (co>=m) return 1;
		}
	}
	return 0;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n<=5 && m==1)
	{
		for (int i=1;i<n;++i) 
		{
			++p;
			scanf("%d%d%d",&e[p].a,&e[p].b,&e[p].l);
			++p;
			e[p].a=e[p-1].b; e[p].b=e[p-1].a; e[p].l=e[p-1].l;
			if (fir[e[p-1].a]) nex[p-1]=fir[e[p-1].a];
			fir[e[p-1].a]=p-1;
			if (fir[e[p].a]) nex[p]=fir[e[p].a];
			fir[e[p].a]=p;
		}
		for (int i=1;i<=n;++i)
		{
			memset(vis,0,sizeof(vis));
			ans=max(ans,(LL)dfs2(i));
		}
		printf("%lld\n",ans);
		return 0;
		/*gen=1;
		fa[gen]=1;
		dfs(1);
		printf("%d\n",dfs1(1));*/
	}
	else
	{
		for (int i=1;i<n;++i) scanf("%d%d%d",&e[i].a,&e[i].b,&e[i].l);
		fla=0;
		for (int i=1;i<n;++i) if (e[i].a!=1) fla=1;
		if (!fla)
		{
			sort(e+1,e+n,cmp);
			if (m==n-1)
			{
				printf("%d\n",e[n-1].l);
				return 0;
			}
			else if (m==1)
			{
				if (n==2) printf("%d\n",e[1].l);
				else printf("%d\n",e[1].l+e[2].l);
				return 0;
			}
			else
			{
				if (((n-1)/2)>=m)
				{
					ans=(LL)e[1].l+e[m*2].l;
					for (int i=2;i<=m;++i)
					{
						ans=min(ans,(LL)e[i].l+e[m*2-i+1].l);
					}
					printf("%lld\n",ans);
					return 0;
				}
				else
				{
					ans=(LL)e[m*2-n+1].l;
					for (int i=m*2-n+2;i<=n-1;++i)
					{
						ans=min(ans,(LL)e[i].l+e[n-1-(i-(m*2-n+2)+1)+1].l);
					}
					printf("%lld\n",ans);
					return 0;
				}
			}
		}
		else 
		{
			fla=0;
			for (int i=1;i<n;++i) if (e[i].a+1!=e[i].b) fla=1;
			if (!fla)
			{
				ans=0;
				sort(e+1,e+n,cmp1);
				LL xj=1,sj=0;
				for (int i=1;i<n;++i) sj+=(LL)e[i].l;
				sj/=(LL)m;
				while (xj<=sj)
				{
					LL mi=(xj+sj)/2;
					if (yanzheng(mi))
					{
						ans=max(ans,mi);
						xj=mi+1;
					}
					else
					{
						sj=mi-1;
					}
				}
				printf("%lld\n",ans);
				return 0;
			}
			else
			{
				for (int i=n;i<=n*2-2;++i) 
				{
					e[i].a=e[i-n+1].b;
					e[i].b=e[i-n+1].a;
					e[i].l=e[i-n+1].l;
					if (fir[e[i].a]) nex[i]=fir[e[i].a];
					fir[e[i].a]=i;
				}
				for (int i=1;i<=n;++i)
				{
					memset(vis,0,sizeof(vis));
					ans=max(ans,(LL)dfs2(i));
				}
				printf("%lld\n",ans);
				return 0;
			}
		}
	}
	return 0;
}
