#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define N 101000

using namespace std;

int a[N],n;
ll ans;

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	a[0]=0;
	for (int i=1;i<=n;i++)
	    if (a[i]>a[i-1]) ans+=a[i]-a[i-1];
	printf("%lld\n",ans);
	
	return 0;
}
