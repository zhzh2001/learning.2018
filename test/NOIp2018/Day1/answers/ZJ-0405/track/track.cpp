#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#define ll long long
#define N 51000
#define M 101000

using namespace std;

struct node{
	int u,v;
	ll val;
}a[1001000],c[1001000];

bool vis[N];
int head[N],de[N],Next[M],vet[M],val[M],dep[N],f[N][30],
    n,m,x,y,z,t,tot,num;
ll g[N][30],sum[N];

void add(int x,int y,int z){
	Next[++tot]=head[x];
	head[x]=tot;
	vet[tot]=y;
	val[tot]=z;
}

int find(int x){
	int l=1,r=num,ans;
	while (l<=r){
		int mid=(l+r)>>1;
		if (a[mid].val<x) l=mid+1;else ans=mid,r=mid-1;
	}
	return ans;
}

bool cmp2(node x,node y){
	return x.u<y.u;
}

bool check(int x){
	int t=find(x),cnt=0,sum=0;
	memset(vis,0,sizeof(vis));
	for (int i=t;i<=num;i++){
		if (!vis[a[i].u] && !vis[a[i].v]){
			vis[a[i].u]=true;
			vis[a[i].v]=true;
			sum++;
		}
	}
	return sum>=m;
}

bool cmp(node a,node b){
	return a.val<b.val;
}

bool check2(int x){
	int cnt=0,last=0;
	for (int i=1;i<=n;i++)
	    if (sum[i]-sum[last]>=x) cnt++,last=i;
	return cnt>=m;
}

void dfs(int u){
	for (int i=1;i<=t;i++){
		f[u][i]=f[f[u][i-1]][i-1];
		if (!f[u][i]) break;
		g[u][i]=g[u][i-1]+g[f[u][i-1]][i-1];
	}
	for (int e=head[u];e;e=Next[e]){
		int v=vet[e];
		if (v==f[u][0]) continue;
		f[v][0]=u;
		g[v][0]=val[e];
		dep[v]=dep[u]+1;
		dfs(v);
	}
}

ll lca(int x,int y){
	ll len=0;
	if (dep[x]<dep[y]) swap(x,y);
	for (int i=t;i>=0;i--){
		if (f[x][i] && dep[f[x][i]]>=dep[y]) len+=g[x][i],x=f[x][i];
		if (dep[x]<=dep[y]) break;
	}
	for (int i=t;i>=0;i--)
		if (f[x][i]!=f[y][i]) len+=g[x][i]+g[y][i],x=f[x][i],y=f[y][i];
	if (x!=y) len+=g[x][0]+g[y][0],x=f[x][0],y=f[y][0];
	return len;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	tot=0;
	bool flag=true,flag2=true;
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		if (y!=x+1) flag=false;
		if (x!=1) flag2=false;
		add(x,y,z);
		add(y,x,z);
	}
	//m==1
	if (m==1){
		t=log(n)/log(2);
	    dep[1]=0;
    	dfs(1);
    	int Max=0;
    	for (int i=1;i<=n;i++) Max=max(Max,dep[i]);
    	ll ans=0;
    	for (int i=1;i<n;i++)
    	    if (dep[i]==Max)
	            for (int j=i+1;j<=n;j++)
				    if (dep[j]==Max)
					    ans=max(ans,lca(i,j));
		printf("%lld\n",ans);
	}
	//�� 
	if (flag){
		memset(de,0,sizeof(de));
		for (int i=1;i<=n;i++)
			for (int e=head[i];e;e=Next[e]) de[vet[e]]++;
	    for (int i=1;i<=n;i++) 
		    if (de[i]==1){
	    	    x=i;
	    	    break;
	    	}
	    memset(vis,false,sizeof(vis));
	    int i=1;
	    while (x){
	    	vis[x]=true;
	    	int e=head[x];
	    	if (vis[vet[e]]) e=Next[e];
	    	sum[i]=sum[i-1]+val[e];
	    	x=vet[e];
	    	i++;
		}
		ll l=0,r=sum[n-1],ans;
		while (l<=r){
			ll mid=(l+r)>>1;
			if (check2(mid)) ans=mid,l=mid+1;else r=mid-1;
		}		
	    printf("%lld\n",ans);
	    
	    return 0;
	}
	//a[i]==1
	if (flag2){
		for (int i=2;i<n;i++)
		    for (int j=i+1;j<=n;j++){
		    	a[++num].val=val[head[i]]+val[head[j]];
		    	a[num].u=i;
		    	a[num].v=j;
			}
		sort(a+1,a+num+1,cmp);
		int l=0,r=a[num].val;
    	ll ans=0;
    	while (l<=r){
	    	int mid=(l+r)>>1;
	    	if (check(mid)) ans=mid,l=mid+1;else r=mid-1;
    	}
	    printf("%lld\n",ans);
	}
	
	
	
	return 0;
}
