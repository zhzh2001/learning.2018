#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define N 110

using namespace std;

bool vis[1001000];
int a[N],t,n;

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);

	scanf("%d",&t);
	while (t--){
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	    sort(a+1,a+n+1);
	    int ans=0;
	    memset(vis,false,sizeof(vis));
	    vis[0]=true;
	    for (int i=1;i<=n;i++)
	        if (!vis[a[i]]){
	        	ans++;
	        	for (int j=a[i];j<=a[n];j++)
	        	    if (vis[j-a[i]]) vis[j]=true;
			}
		printf("%d\n",ans);
	}
	
	return 0;
}
