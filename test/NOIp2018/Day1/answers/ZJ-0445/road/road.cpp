#include<bits/stdc++.h>
#define For(i,l,r) for(int i=(l);i<=(r);++i)
#define ReFor(i,l,r) for(int i=(l);i>=(r);--i)
using namespace std;
const int N=1e5+9;
int n,m,ans,cnt=1;
int a[N],data[N],pos[N],nxt[N];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n){
		scanf("%d",&a[i]);
		data[i]=a[i];
		nxt[i]=pos[a[i]];
		pos[a[i]]=i;
	}
	sort(data+1,data+n+1);
	int m=unique(data+1,data+n+1)-data-1;
	For(i,1,m){
		ans+=cnt*(data[i]-data[i-1]);
		int j=pos[data[i]];
		while(j){
			if(j==1&&a[j]>=a[j+1]) --cnt;
			if(j==n&&a[j]>=a[j-1]) --cnt;
			if(j!=n&&j!=1&&a[j]<a[j+1]&&a[j]<a[j-1]) ++cnt;
			if(j!=n&&j!=1&&a[j]>a[j+1]&&a[j]>=a[j-1]) --cnt;
			a[j]++;
			j=nxt[j];
		}
		j=pos[data[i]];while(j) a[j]--,j=nxt[j];
	}
	printf("%d\n",ans);
	return 0;
}
