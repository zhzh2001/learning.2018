#include<bits/stdc++.h>
#define For(i,l,r) for(int i=(l);i<=(r);++i)
#define ReFor(i,l,r) for(int i=(l);i>=(r);--i)
using namespace std;
const int N=1e2+9,NUM=2e4+5e3+9;
int n,ans=0;
int a[N];
bool vis[NUM];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--){
		memset(vis,0,sizeof(vis));ans=0;
		scanf("%d",&n);
		For(i,1,n) scanf("%d",&a[i]);
		sort(a+1,a+n+1);
		For(i,1,n){
			if(!vis[a[i]]){
				++ans;
				vis[a[i]]=1;
				For(j,1,a[n]){
					if(vis[j]) for(int k=1;j+a[i]*k<=a[n]&&!vis[j+a[i]*k];++k){
						vis[j+a[i]*k]=1;
					}
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
