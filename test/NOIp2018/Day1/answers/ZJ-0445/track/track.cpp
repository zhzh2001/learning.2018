#include<bits/stdc++.h>
#define For(i,l,r) for(int i=(l);i<=(r);++i)
#define ReFor(i,l,r) for(int i=(l);i>=(r);--i)
using namespace std;
const int N=5e4+9;
int n,m,ans,tot;
bool flag1=1,flag2=1;
int f[N],ff[N];
int fa[N],size[N],t_size[N],len[N];
vector<int>e_son[N];
struct edge{
	int to,next,val;
}e[N];
int head[N],cnt;
void add(int u,int v,int w){
	e[++cnt].to=v;e[cnt].val=w;e[cnt].next=head[u];head[u]=cnt;
}
void build(int u){
	size[u]=1;
	for(int i=head[u],v=e[i].to,w=e[i].val;i;i=e[i].next){
		if(fa[u]==v) continue;
		fa[v]=u;len[v]=w;build(v);size[u]+=size[v];
		len[v]=w;
	}
}
bool f2(int k){
	int pos=1,tot=0,cnt_=0;
	while(pos<n){
		tot+=ff[pos];
		if(tot>=k) tot=0,cnt_++;
		++pos;
	}
	if(cnt_>=m) return 1;
	return 0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	For(i,1,n-1){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		if(u!=1) flag1=0;
		if(v!=u+1) flag2=0;
		f[i]=w;ff[u]=w;
		add(u,v,w);add(v,u,w);
	}
	if(flag1){
		sort(f+1,f+n);
		ans=f[n-m*2]+f[n-1];
		int l=n-m*2,r=n-1;
		if(l<=r){
			ans=min(ans,f[n-m*2]+f[n-1]);
			l--;r++;
		}
		printf("%d\n",ans);
		return 0;
	}
	if(flag2){
		int l=0,r=500000000;
		while(l<=r){
			int mid=(l+r)>>1;
			if(f2(mid)) ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	return 0;
}
