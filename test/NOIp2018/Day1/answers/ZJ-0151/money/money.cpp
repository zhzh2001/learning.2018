#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int Maxw=25000;
const int maxn=110;
bool f[Maxw+10];
int a[maxn],n;
void work(){
	scanf("%d",&n);
	for (int i=1; i<=n; i++) scanf("%d",&a[i]);
	sort(a+1,a+1+n);
	memset(f,0,sizeof(f));
	f[0]=1;
	int m=n;
	for (int i=1; i<=n; i++){
		if (f[a[i]]){
			m--;
			continue;
		}
		for (int j=a[i]; j<=Maxw; j++) if (f[j-a[i]]) f[j]=1;
	}
	printf("%d\n",m);
}
int T;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		work();
	}
	return 0;
}
