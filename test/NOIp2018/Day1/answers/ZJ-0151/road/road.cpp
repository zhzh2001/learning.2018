#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=100100;
int L[maxn],R[maxn],w[maxn],f[maxn],a[maxn],ans=0,n;
pair<int,int>g[maxn];
void rd(int &x){
	char c=getchar(); x=0;
	while (c<'0' || c>'9') c=getchar();
	while (c>='0' && c<='9') x=x*10+c-'0',c=getchar();
}
int find(int x){
	return f[x]==x?x:f[x]=find(f[x]);
}
void get(int x,int y){
	if (y<0 || y>n) return;
	int fx=find(x),fy=find(y);
	if (w[fx]>w[fy]) return;
	f[fy]=fx;
	if (L[fy]<L[fx])L[fx]=L[fy];
	if (R[fy]>R[fx])R[fx]=R[fy];
	ans+=w[fy]-w[fx];
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1; i<=n; i++) rd(a[i]);
	for (int i=0; i<=n; i++) f[i]=i,w[i]=a[i],L[i]=R[i]=i;
	for (int i=0; i<=n; i++) g[i]=make_pair(a[i],i);
	sort(g,g+1+n);
	for (int i=n; i>=0; i--){
		int p=g[i].second;
		get(p,L[p]-1);
		get(p,R[p]+1);
	}
	printf("%d\n",ans);
	return 0;
}
