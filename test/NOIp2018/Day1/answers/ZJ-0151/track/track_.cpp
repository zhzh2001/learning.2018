#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;
const int maxn=50100;
vector<int>g[maxn];
int to[maxn<<1],len[maxn<<1],nex[maxn<<1],lnk[maxn],dp[maxn],low[maxn],Mid,e,n,m;
int vis[maxn];
void rd(int &x){
	char c=getchar(); x=0;
	while (c<'0' || c>'9') c=getchar();
	while (c>='0' && c<='9') x=x*10+c-'0',c=getchar();
}
void add(int u,int v,int l){
	to[++e]=v; len[e]=l; nex[e]=lnk[u]; lnk[u]=e;
}
void dfs(int u,int fa){
	for (int i=lnk[u]; i; i=nex[i]){
		int v=to[i];
		if (v==fa) continue;
		dfs(v,u);
		dp[u]+=dp[v];
		if (low[v]+len[i]>=Mid) dp[u]++;
		else g[u].push_back(low[v]+len[i]);
	}
	if (g[u].size()){
		sort(g[u].begin(),g[u].end());
		int p=g[u].size()-1,mx=0,cnt=0;
		for (int i=0; i<=p; i++){
			if (i==p){
				if (g[u][p]>mx) mx=g[u][p];
				cnt++;
				break;
			}
			while (p-1>i && g[u][i]+g[u][p-1]>=Mid){
				if (g[u][p]>mx) mx=g[u][p];
				cnt++;
				p--;
			}
			if (g[u][i]+g[u][p]>=Mid) dp[u]++,p--;
			else low[u]=g[u][i];
		}
		dp[u]+=cnt/2;
		if (cnt&1) low[u]=mx;
	}
}
void cle(){
	for (int i=1; i<=n; i++) g[i].clear();
	memset(dp,0,sizeof(dp));
	memset(low,0,sizeof(low));
}
void work(){
	int l=1,r=1e9;
	while (l<r){
		Mid=l+r+1>>1;
		cle();
		dfs(1,0);
		if (dp[1]>=m) l=Mid; else r=Mid-1;
	}
	printf("%d\n",l);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,u,v,l; i<n; i++){
		rd(u); rd(v); rd(l);
		add(u,v,l);
		add(v,u,l);
	}
	work();
	return 0;
}
