#include<cstdio>
#include<cstring>
#include<set>
#include<algorithm>
using namespace std;
const int maxn=50100;
set<int>g[maxn];
int to[maxn<<1],len[maxn<<1],nex[maxn<<1],lnk[maxn],dp[maxn],low[maxn],Mid,e,n,m;
int vis[maxn];
void rd(int &x){
	char c=getchar(); x=0;
	while (c<'0' || c>'9') c=getchar();
	while (c>='0' && c<='9') x=x*10+c-'0',c=getchar();
}
void add(int u,int v,int l){
	to[++e]=v; len[e]=l; nex[e]=lnk[u]; lnk[u]=e;
}
void dfs(int u,int fa){
	for (int i=lnk[u]; i; i=nex[i]){
		int v=to[i];
		if (v==fa) continue;
		dfs(v,u);
		dp[u]+=dp[v];
		if (low[v]+len[i]>=Mid) dp[u]++;
		else g[u].insert(low[v]+len[i]);
	}
	while (!g[u].empty()){
		set<int>::iterator it=g[u].begin();
		int v=*it; g[u].erase(it);
		int w=Mid-v;
		it=g[u].lower_bound(w);
		if (it!=g[u].end()){
			dp[u]++;
			g[u].erase(it);
		}else if (v>low[u]) low[u]=v;
	}
}
void cle(){
	for (int i=1; i<=n; i++) g[i].clear();
	memset(dp,0,sizeof(dp));
	memset(low,0,sizeof(low));
}
void work(){
	int l=1,r=1e9;
	while (l<r){
		Mid=l+r+1>>1;
		cle();
		dfs(1,0);
		if (dp[1]>=m) l=Mid; else r=Mid-1;
	}
	printf("%d\n",l);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,u,v,l; i<n; i++){
		rd(u); rd(v); rd(l);
		add(u,v,l);
		add(v,u,l);
	}
	work();
	return 0;
}
