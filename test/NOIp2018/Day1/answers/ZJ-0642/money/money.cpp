#include<bits/stdc++.h>
#define FO(i,a,b) for (int i=a;i<=b;i++)
#define DO(i,a,b) for (int i=a;i>=b;i--)
using namespace std;

int a[110],f[25010];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		int n,mx=0,ans=0;
		scanf("%d",&n);
		FO(i,1,n) scanf("%d",&a[i]),mx=max(mx,a[i]);
		sort(a+1,a+n+1);
		memset(f,0,sizeof f);
		f[0]=1;
		FO(i,1,n){
			if (f[a[i]]) continue;
			ans++;
			FO(j,1,mx) if (j>=a[i]) if (f[j-a[i]]) f[j]=1;
		}
		printf("%d\n",ans);
	}
}
