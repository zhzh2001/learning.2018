#include<bits/stdc++.h>
#define FO(i,a,b) for (int i=a;i<=b;i++)
#define DO(i,a,b) for (int i=a;i>=b;i--)
using namespace std;

const int maxn=50010;
struct edg{
	int v,next,w;
}g[maxn<<1];
int head[maxn],n,m,tot,ind,mx,sum,root,e[maxn],used[maxn];
#define go(x) for (int i=head[x];i;i=g[i].next)
void add(int u,int v,int w){
	g[++tot].v=v;g[tot].next=head[u];head[u]=tot;g[tot].w=w;
}
void dfs(int x,int fa,int value){
	if (value>mx) {mx=value;root=x;}
	go(x){
		int v=g[i].v;
		if (v==fa) continue;
		dfs(v,x,value+g[i].w);
	}
}
void dfs2(int x,int fa){
	go(x){
		int v=g[i].v;
		if (v==fa) continue;
		e[++ind]=g[i].w;
		sum+=g[i].w;
		dfs2(v,x);
	}
}
int check1(int mid){
	int edge=0;
	memset(used,0,sizeof used);
	FO(i,1,ind){
		if (used[i]) continue;
		if (e[i]>=mid){
			used[i]=1;edge++;if (edge==m) return 1;
			continue;
		}
		int pos=lower_bound(e+1,e+ind+1,mid-e[i])-e;
		int now=pos;
		while ((used[now]||now==i)&&now<=ind) now++;
		if (now>ind) return 0;
		used[i]=1;
		used[now]=1;edge++;
		if (edge==m) return 1;
	}
	return 0;
}
int check2(int mid){
	int now=0,edge=0;
	FO(i,1,ind){
		now+=e[i];
		if (now>=mid) now=0,edge++;
		if (edge==m) return 1;
	}
	return 0;
}
int cmp(int x,int y){
	return x>y;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	tot=1;
	int u,v,w;
	int flag1=1,flag2=1,flag3=1;
	scanf("%d%d",&n,&m);
	FO(i,1,n-1){
		scanf("%d%d%d",&u,&v,&w);
		if (u>v) swap(u,v);
		add(u,v,w);add(v,u,w);
		if (u!=1) flag1=0;
		if (v!=u+1) flag2=0;
	}
	if (m==1){
		mx=0;
		dfs(1,0,0);
		mx=0;
		dfs(root,0,0);
		printf("%d\n",mx);
		return 0;	
	}
	if (flag1){
		ind=0;sum=0;
		FO(i,2,tot) if (i%2==0) e[++ind]=g[i].w,sum+=g[i].w;
		sort(e+1,e+ind+1,cmp);
		int l=1,r=sum/m,ans;
		while (l<=r){
			int mid=(l+r)>> 1;
			if (check1(mid)) ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	if (flag2){
		ind=0;
		sum=0;
		dfs2(1,0);
		int l=1,r=sum/m,ans;
		while (l<=r){
			int mid=(l+r)>> 1;
			if (check2(mid)) ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	
}
