#include<bits/stdc++.h>
#define FO(i,a,b) for (int i=a;i<=b;i++)
#define DO(i,a,b) for (int i=a;i>=b;i--)
#define lson x<<1,l,m
#define rson x<<1|1,m+1,r
using namespace std;

const int maxn=100010;
struct node{
	int v,id;
}a[maxn];
int n,s[maxn],l[maxn],r[maxn],p[maxn],tag[maxn<<2],v[maxn<<2];

int cmp(node x,node y){
	return x.v<y.v;
}
void pushdown(int x,int l,int r){
	if (tag[x]){
		v[x<<1]+=tag[x];v[x<<1|1]+=tag[x];
		tag[x<<1]+=tag[x];tag[x<<1|1]+=tag[x];
		tag[x]=0;
	}
}
void update(int x,int l,int r,int ql,int qr,int k){
	if (ql<=l&&qr>=r) {
		v[x]+=k,tag[x]+=k;return;
	}
	int m=(l+r)>> 1;
	pushdown(x,l,r);
	if (qr<=m) update(lson,ql,qr,k);
	else if (ql>m) update(rson,ql,qr,k);
	else update(lson,ql,m,k),update(rson,m+1,qr,k);
}
int query(int x,int l,int r,int q){
	if (l==r) return v[x];
	int m=(l+r)>> 1;
	pushdown(x,l,r);
	if (q<=m) return query(lson,q);
	else return query(rson,q);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	FO(i,1,n) scanf("%d",&a[i].v),a[i].id=i;
	int top=0,res;
	FO(i,1,n){
		res=0;
		while (top&&s[top]>=a[i].v) res=p[top],p[top]=0,top--;
		s[++top]=a[i].v;p[top]=res;
		if (!p[top]) p[top]=i;
		l[i]=p[top];
	}
	memset(p,0,sizeof p);memset(s,0,sizeof s);
	top=0;
	DO(i,n,1){
		res=0;
		while (top&&s[top]>=a[i].v) res=p[top],p[top]=0,top--;
		s[++top]=a[i].v;p[top]=res;
		if (!p[top]) p[top]=i;
		r[i]=p[top];
	}
	sort(a+1,a+n+1,cmp);
	int ans=0;
	FO(i,1,n){
		int now=query(1,1,n,a[i].id);
		if (now==a[i].v) continue;
		ans+=a[i].v-now;
		update(1,1,n,l[a[i].id],r[a[i].id],a[i].v-now);
	}
	printf("%d\n",ans);
	return 0;
}
