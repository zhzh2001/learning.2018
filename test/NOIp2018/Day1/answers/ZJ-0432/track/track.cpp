#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
#include<vector>
#define N 55555
#define ll long long
using namespace std;
ll n,m;
vector<pair<ll,ll> > v[N];
ll dis[N];
ll vis[N];
void dfs(ll x,ll fa) {
	for(ll i=0; i<v[x].size(); i++) {
		ll to=v[x][i].first;
		if(to==fa)continue;
		dis[to]=dis[x]+v[x][i].second;
		dfs(to,x);
		}
	}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	for(ll i=1; i<n; i++) {
		ll a,b,c;
		scanf("%lld%lld%lld",&a,&b,&c);
		v[a].push_back(make_pair(b,c));
		v[b].push_back(make_pair(a,c));
		}
	memset(dis,0,sizeof(dis));
	dfs((1+n)>>1,0);
	ll key=0;
	for(ll i=1; i<=n; i++)if(dis[key]<dis[i])key=i;
	memset(dis,0,sizeof(dis));
	dfs(key,0);
	ll maxx=0;
	for(ll i=1; i<=n; i++)if(dis[i]>maxx)maxx=dis[i];
	cout<<maxx;
	return 0;

	}


