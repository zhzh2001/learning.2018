#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
using namespace std;
int n,x,plk[205],cnt,fi,se;
int input[205];
bool ban[1800005];
int T;
int sett(int a,int b) {
	if(a>b)swap(a,b);
	if(a==1)return a;
	else if(a==2)return b;
	else return (a*b-a-b);
	}
void add(int k) {
	int maxx=sett(fi,se);
	for(int i=k; i<=maxx; i++)
		if(ban[i-k])ban[i]=true;
	}
bool check(int x) {
	if(x>sett(fi,se))return false;
	if(ban[x])return false;
	return true;
	}
int gcd(int a,int b) {
	if(b==0)return a;
	return gcd(b,a%b);
	}
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--) {
		cnt=0;
		fi=1e3;
		se=1e3;
		int minn=-1;
		memset(ban,false,sizeof(ban));
		ban[0]=true;
		scanf("%d",&n);
		for(int i=1; i<=n; i++)scanf("%d",&input[i]);
		sort(input+1,input+n+1);
		for(int i=1; i<=n; i++)
			for(int r=1; r<=n; r++)
				if((gcd(input[i],input[r])==1)&&((sett(input[i],input[r])<minn)||(minn==-1))) {
					minn=sett(input[i],input[r]);
					fi=min(input[i],input[r]);
					se=max(input[i],input[r]);
					}
		for(int i=1; i<=n; i++)
			if(check(input[i])) {
				plk[++cnt]=input[i];
				add(input[i]);
				}
		cout<<cnt<<endl;
			}
	}
/*
8
13 11 19 16 3 15 20 17

37
36 64 48 50 40 59 53 57 69 62 58 68 49 63 39 38 70 55 56 21 54 14 67 22 25 66 61 37 65 44 45 27 29 8 2 51 60
*/
