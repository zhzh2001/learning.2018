#include <cstdio>
#include <cstring>
#include <algorithm>
#include <queue>
#include <vector>
const int MAXN = 100 + 10;
const int MAXA = 25000 + 10;


int n;
int a[MAXN];
namespace solver1 {
  int dis[MAXA], prev[MAXA];
  std::vector <int> prevs[MAXA];
  std::vector <int> have[MAXN];
  bool visit[MAXN];
  bool check(int x) {
    for (int i = 0; i < (int) have[x].size(); i++) {
      int u = have[x][i];
      if (prev[u] == x) {
        bool flag = 0;
        for (int j = 0; j < (int) prevs[u].size(); j++) {
          int v = prevs[u][j];
          if (v == x || visit[j]) continue;
          flag = 1;
          prev[u] = j;
          break;
        }

        if (!flag) return 0;
      }
    }
    return 1;

  }
  void bfs(int x) {
    for (int i = 0; i < x; i++) {
      dis[i] = 1e9, prev[i] = -1;
      prevs[i].clear();
    }
    dis[0] = 0;

    typedef std::pair <int, int> Pii;
    std::priority_queue <Pii, std::vector <Pii>, std::greater<Pii> > pq;
    pq.push(std::make_pair(0, 0));

    while(!pq.empty()) {
      Pii tmp = pq.top(); pq.pop();
      
      int d = tmp.first, u = tmp.second;
      if (dis[u] != d) continue;

      for (int i = 2; i <= n; i++) {
        int v = (u + a[i]) % x;
        int w = a[i];
        if (dis[u] + w < dis[v]) {
          dis[v] = dis[u] + w;
          prev[v] = i;
          prevs[v].clear();
          prevs[v].push_back(i);
          pq.push(std::make_pair(dis[v], v));
        } else if (dis[u] + w == dis[v]) {
          prevs[v].push_back(i);
        }
      }
    }
  }
  void main() {
    std::sort(a + 1, a + n + 1);
    n = std::unique(a + 1, a + n + 1) - (a + 1);
    memset(visit, 0, sizeof visit);

    for (int i = 1; i <= n; i++) have[i].clear();
    
    bfs(a[1]);

    int ans = n;
    for (int i = 0; i < a[1]; i++) {
      for (int j = 0; j < (int) prevs[i].size(); j++) {
        have[prevs[i][j]].push_back(i);
      }
    }

    for (int i = 2; i <= n; i++) {
      if (check(i)) {
        visit[i] = 1;
        ans--;
      }
    }

    printf("%d\n", ans);
    
    
  }
}
int main() {
#ifndef LOCAL
  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
#endif
  int t;
  scanf("%d", &t);
  while(t--) {
    scanf("%d", &n);
    for (int i = 1; i <= n; i++) {
      scanf("%d", a + i);
    }
    
    solver1::main();
  }
}
