#include <cstdio>
#include <cstring>
#include <algorithm>
#include <set>
const int MAXN = 50000 + 10;

struct Edge {
  int v, w, next;
}edge[MAXN << 1];
int head[MAXN], tail, n, m;
void insert(int u, int v, int w) {
  edge[++tail] = (Edge) {v, w, head[u]}; head[u] = tail;
}
namespace solver1 {
  int val, ans;
  int dfs(int u, int fa) {
    std::multiset<int> s;
    for (int i = head[u]; i; i = edge[i].next) {
      int v = edge[i].v;
      if (v == fa) continue;
      int w = dfs(v, u) + edge[i].w;
      if (w >= val) ans++;
      else {
        s.insert(w);
      }
    }
    int res = 0;
    while(!s.empty()) {
      int x = *s.begin();
      s.erase(s.begin());
      std::multiset<int>::iterator it = s.lower_bound(val - x);
      if (it == s.end()) {
        res = std::max(res, x);
        continue;
      }
      ans++;
      s.erase(it);
    }
    return res;
  }
  bool check(int x) {
    val = x;
    ans = 0;
    dfs(1, 0);
    return ans >= m;
  }
  void main() {
    int l = 0, r = 1e9, ans = 0;
    while(l <= r) {
      int mid = (l + r) >> 1;
      if (check(mid)) {
        l = mid + 1;
        ans = mid;
      } else {
        r = mid - 1;
      }
    }

    printf("%d\n", ans);
  }
}
int main() {
#ifndef LOCAL
  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
#endif

  scanf("%d%d", &n, &m);
  //static int deg[MAXN];
  for (int i = 1; i < n; i++) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    insert(u, v, w);
    insert(v, u, w);
    //deg[u]++;
    //deg[v]++;
  }
  //int max_deg = 0;
  //for (int i = 1; i <= n; i++) max_deg = std::max(max_deg, deg[i]);
  //fprintf(stderr, "%d\n", max_deg);
  solver1::main();
}
