#include <cstdio>
#include <cstring>
#include <algorithm>
const int MAXN = 1e5 + 10;

int n;
int a[MAXN];
int main() {
#ifndef LOCAL
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
#endif
  scanf("%d", &n);
  long long ans = 0;
  for (int i = 1; i <= n; i++) {
    scanf("%d", a + i);
    ans += std::max(a[i] - a[i - 1], 0);
  }
  printf("%lld\n", ans);
}
