#include <bits/stdc++.h>
using namespace std;
#define file "money"
int read(){
	int x=0;
	char c=getchar();
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
int a[102];
bool dp[25002];
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int T=read();
	while(T--){
		memset(dp,0,sizeof(dp));
		dp[0]=1;
		int n=read();
		for(int i=1;i<=n;i++)
			a[i]=read();
		sort(a+1,a+n+1);
		int ans=0;
		for(int i=1;i<=n;i++){
			if (dp[a[i]])
				continue;
			ans++;
			for(int j=a[i];j<=25000;j++)
				dp[j]|=dp[j-a[i]];
		}
		printf("%d\n",ans);
	}
}
