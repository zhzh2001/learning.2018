#include <bits/stdc++.h>
using namespace std;
#define file "road"
int read(){
	int x=0;
	char c=getchar();
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
int a[100002];
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=n;i;i--)
		a[i]-=a[i-1];
	int ans=0;
	for(int i=1;i<=n;i++)
		if (a[i]>0)
			ans+=a[i];
	printf("%d",ans);
}
