#include <bits/stdc++.h>
using namespace std;
#define file "track"
typedef pair<int,int> pii;
int read(){
	int x=0;
	char c=getchar();
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
const int maxn=50002;
inline void tense(int &x,int y){
	if (x<y)
		x=y;
}
struct graph{
	int n,m;
	struct edge{
		int to,cost,next;
	}e[maxn*2];
	int first[maxn];
	void addedge(int from,int to,int cost){
		e[++m]=(edge){to,cost,first[from]};
		first[from]=m;
	}
	int f[maxn],g[maxn];
	int fa[maxn];
	int now[maxn],tmp[maxn];
	bool vis[maxn];
	int x;
	void dfs(int u){
		f[u]=g[u]=0;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa[u])
				continue;
			fa[v]=u;
			dfs(v);
			f[u]+=f[v];
		}
		int cur=0;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa[u])
				continue;
			now[++cur]=g[v]+e[i].cost;
		}
		sort(now+1,now+cur+1);
		while(now[cur]>=x)
			cur--,f[u]++;
		for(int i=1;i<=cur;i++)
			vis[i]=0;
		int j=cur;
		int tat=0,lol=0,tl=0;
		for(int i=1;i<=cur;i++){
			if (now[i]*2>=x){
				if (!vis[i])
					lol++,tl=now[i];
				continue;
			}
			while(j>i && now[i]+now[j]>=x){
				tmp[++tat]=j;
				j--;
			}
			if (tat){
				vis[i]=vis[tmp[tat]]=1;
				f[u]++;
				tat--;
			}else tense(g[u],now[i]);
		}
		f[u]+=lol/2;
		if (lol%2)
			g[u]=tl;
	}
	int check(int x){
		this->x=x;
		dfs(1);
		return f[1];
	}
}g;
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=g.n=read(),m=read();
	for(int i=1;i<n;i++){
		int u=read(),v=read(),l=read();
		g.addedge(u,v,l);
		g.addedge(v,u,l);
	}
	int l=0,r=n*10000;
	while(l<r){
		int mid=(l+r+1)/2;
		if (g.check(mid)>=m)
			l=mid;
		else r=mid-1;
	}
	printf("%d",l);
}
