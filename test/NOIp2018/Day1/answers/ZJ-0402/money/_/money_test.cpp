#include<cstdio>
#include<algorithm>
#include<cstring>
#define N 105
int T,n,a[N],dp[26666],dp2[26666];
int main(){
	freopen("money.in","r",stdin);
	freopen("money_test.out","w",stdout);
	for(scanf("%d",&T);T--;){
		scanf("%d",&n);
		for(int i=1;i<=n;++i)scanf("%d",a+i);
		std::sort(a+1,a+n+1);
		memset(dp,0,sizeof dp);*dp=1;
		int ans=0;
		for(int i=1;i<=n;++i)
		if(!dp[a[i]]){
			++ans;
			for(int j=a[i];j<=25000;++j)
			if(dp[j-a[i]])dp[j]=1;
		}
		memset(dp2,0,sizeof dp2);*dp2=1;
		for(int i=1;i<=n;++i){
			for(int j=a[i];j<=25000;++j)
			if(dp2[j-a[i]])dp2[j]=1;
		}
		for(int i=0;i<=25000;++i)if(dp[i]!=dp2[i]){
			puts("nan");
		}
		printf("%d\n",ans);
	}
	return 0;
}
