#include<cstdio>
#include<algorithm>
#include<cstring>
#define N 105
int T,n,a[N],dp[26666];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(scanf("%d",&T);T--;){
		scanf("%d",&n);
		for(int i=1;i<=n;++i)scanf("%d",a+i);
		std::sort(a+1,a+n+1);
		memset(dp,0,sizeof dp);*dp=1;
		int ans=0;
		for(int i=1;i<=n;++i)
		if(!dp[a[i]]){
			++ans;
			for(int j=a[i];j<=25000;++j)
			if(dp[j-a[i]])dp[j]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
