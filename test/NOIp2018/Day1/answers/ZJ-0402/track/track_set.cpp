//Chtholly Nota Seniorious is the cutest!
#include<cstdio>
#include<cstring>
#include<set>
#include<algorithm>
#define N 50505
int n,m,head[N],cnt=0,tot;
int f[N],lim,a[N],dfn[N],idx=0,idfn[N],tt[N],nnt[N],dis[N],hh[N],cc=0;
struct edge{
	int to,nxt,dis;
}e[N<<1];
void dfs(){
	std::multiset<int>s;
	for(int i=n;i;--i){
		for(int j=hh[i];j;j=nnt[j]){
			if(f[tt[j]]+dis[j]>=lim)++tot;else
			s.insert(f[tt[j]]+dis[j]);
		}
		while(!s.empty()){
			std::multiset<int>::iterator it=s.lower_bound(lim-*s.begin());
			if(it==s.begin())++it;
			if(it!=s.end()){
				++tot;
				s.erase(it);
			}else{
				f[i]=*s.begin();
			}
			s.erase(s.begin());
		}
	}
}
bool check(int now){
	lim=now;
	memset(f,0,sizeof f);
	tot=0;
	dfs();
	return tot>=m;
}
void build(int now,int pre){
	int u=++idx;
	for(int i=head[now];i;i=e[i].nxt)
	if(e[i].to!=pre){
		int v=idx+1;
		tt[++cc]=v;nnt[cc]=hh[u];dis[hh[u]=cc]=e[i].dis;
		build(e[i].to,now);
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;++i){
		int u,v,t;
		scanf("%d%d%d",&u,&v,&t);
		e[++cnt]=(edge){v,head[u],t};head[u]=cnt;
		e[++cnt]=(edge){u,head[v],t};head[v]=cnt;
	}
	build(1,0);
	int l=0,r=1e9,ans=0;
	while(l<=r){
		const int mid=l+r>>1;
		if(check(mid))l=(ans=mid)+1;else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
