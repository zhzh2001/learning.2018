#include<bits/stdc++.h>
using namespace std;
int T,n,m,ans,a[1000];
bool flag[30000];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n); m=0; ans=0;
		for (int i=1;i<=n;i++) scanf("%d",&a[i]),m=max(m,a[i]);
		sort(a+1,a+n+1);
		memset(flag,0,sizeof(flag));
		flag[0]=1;
		for (int i=1;i<=n;i++){
			if (flag[a[i]]==0) ans++;
			for (int j=0;j<=m;j++)
				if (flag[j]&&j+a[i]<=m) flag[j+a[i]]=1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
