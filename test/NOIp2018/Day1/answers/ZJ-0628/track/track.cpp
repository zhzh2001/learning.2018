#include<bits/stdc++.h>
using namespace std;
const int N=50010,M=5010;
int n,m,l,r,mid,st1,ans,dis[N],f[N<<1],u[N],v[N],le[N];
int num,vet[N<<1],len[N<<1],nex[N<<1],hea[N];
int dp[M][M];
bool flag1,flag2;
void read(int &x){
	int tmp=1; char ch=getchar(); x=0;
	while (ch<'0'||ch>'9'){if (ch=='-') tmp=-1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48; ch=getchar();}
	x*=tmp;
}
void add(int u,int v,int le){
	vet[++num]=v; len[num]=le; nex[num]=hea[u]; hea[u]=num;
}
void BFS(int st){
	int l=1,r=1,now;
	dis[st]=0; f[1]=st;
	while (l<=r){
		now=f[l]; l++;
		for (int i=hea[now];i;i=nex[i])
			if (dis[vet[i]]>dis[now]+len[i]){
				dis[vet[i]]=dis[now]+len[i]; r++; f[r]=vet[i];
			}
	}
}
bool check1(){
	int ll=1,rr=n-1,tot=0;
	while (ll<rr){
		if (le[ll]+le[rr]>=mid) tot++,ll++,rr--;
		else ll++;
	}
	return (tot>=m);
}
bool cmp(int x,int y){
	return u[x]<u[y];
}
bool check2(){
	int tot=0,now=0;
	for (int i=1;i<n;i++){
		now+=le[f[i]];
		if (now>=mid) tot++,now=0;
	}
	return (tot>=m);
}
inline int maxt(int x,int y){
	return (x>y)?x:y;
}
void dfs(int u,int fa){
	for (int i=hea[u];i;i=nex[i])
		if (vet[i]!=fa){
			dfs(vet[i],u);
			for (int j=m;j>=0;j--)
				if (dp[u][j]!=-1)
					for (int k=0;k<=m-j;k++)
						if (dp[vet[i]][k]!=-1){
							int t1,t2;
							t1=dp[u][j]; t2=dp[vet[i]][k]+len[i];
							dp[u][j+k]=maxt(dp[u][j+k],maxt(t1,t2));
							if (t2>=mid) dp[u][j+k+1]=maxt(dp[u][j+k+1],t1);
							if (t1+t2>=mid) dp[u][j+k+1]=maxt(dp[u][j+k+1],0);
						}
		}
}
bool check3(){
	memset(dp,-1,sizeof(dp));
	for (int i=1;i<=n;i++) dp[i][0]=0;
	dfs(1,0); return (dp[1][m]>-1);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n); read(m); flag1=1; flag2=1;
	for (int i=1;i<n;i++){
		read(u[i]); read(v[i]); read(le[i]);
		if (u[i]>v[i]) swap(u[i],v[i]);
		if (u[i]!=1) flag1=0;
		if (v[i]-u[i]!=1) flag2=0;
	}
	if (m==1){
		for (int i=1;i<n;i++) add(u[i],v[i],le[i]),add(v[i],u[i],le[i]);
		memset(dis,127,sizeof(dis));
		BFS(1); st1=1;
		for (int i=1;i<=n;i++)
			if (dis[i]>dis[st1]) st1=i;
		memset(dis,127,sizeof(dis));
		BFS(st1); ans=dis[1];
		for (int i=1;i<=n;i++)
			if (dis[i]>ans) ans=dis[i];
		printf("%d\n",ans);
	}else if (flag1){
		sort(le+1,le+n);
		l=1; r=1<<30;
		while (l<r){
			mid=(l+r+1)>>1;
			if (check1()) l=mid; else r=mid-1;
		}
		printf("%d\n",l);
	}else if (flag2){
		for (int i=1;i<n;i++) f[i]=i;
		sort(f+1,f+n,cmp);
		l=1; r=1<<30;
		while (l<r){
			mid=(l+r+1)>>1;
			if (check2()) l=mid; else r=mid-1;
		}
		printf("%d\n",l);
	}else{
		for (int i=1;i<n;i++) add(u[i],v[i],le[i]),add(v[i],u[i],le[i]);
		l=1; r=1<<30;
		while (l<r){
			mid=(l+r+1)>>1;
			if (check3()) l=mid; else r=mid-1;
		}
		printf("%d\n",l);
	}
	return 0;
}
