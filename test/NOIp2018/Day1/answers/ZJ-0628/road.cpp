#include<bits/stdc++.h>
using namespace std;
const int N=100010;
long long ans;
int n,a[N],f[N][20],t,w,l[N<<2],r[N<<2],d[N<<2],x,now;
void read(int &x){
	int tmp=1; char ch=getchar(); x=0;
	while (ch<'0'||ch>'9'){if (ch=='-') tmp=-1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48; ch=getchar();}
	x*=tmp;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for (int i=1;i<=n;i++) read(a[i]),f[i][0]=i;
	for (int i=1;i<=17;i++)
		for (int j=1;j<=n-(1<<i)+1;j++)
			if (a[f[j][i-1]]<a[f[j+(1<<(i-1))][i-1]]) f[j][i]=f[j][i-1];
			else f[j][i]=f[j+(1<<(i-1))][i-1];
	t=1; w=1; l[1]=1; r[1]=n; d[1]=0;
	while (t<=w){
		if (l[t]>r[t]){
			t++; continue;
		}
		x=log(r[t]-l[t]+1)/log(2);
		if (a[f[l[t]][x]]<a[f[r[t]-(1<<x)+1][x]]) now=f[l[t]][x];
		else now=f[r[t]-(1<<x)+1][x];
		ans+=a[now]-d[t];
		if (l[t]!=r[t]){
			w++; l[w]=l[t]; r[w]=now-1; d[w]=a[now];
			w++; l[w]=now+1; r[w]=r[t]; d[w]=a[now];
		}
		t++;
	}
	printf("%lld\n",ans);
	return 0;
}
