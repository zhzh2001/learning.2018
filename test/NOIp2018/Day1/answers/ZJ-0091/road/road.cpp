#include <bits/stdc++.h>


#define OPEN(file) \
	freopen(#file".in", "r", stdin), \
	freopen(#file".out", "w", stdout)


int read(void)
{
	int x = 0, c = getchar();
	for (; !isdigit(c); c = getchar());
	for (;  isdigit(c); c = getchar()) {
		x = (x << 3) + (x << 1) + c - 48;
	}
	return x;
}


int a[100086];
long long ans = 0;


int main(void)
{
	OPEN(road);
	int n = read();
	for (int i = 1; i <= n; ++i) {
		a[i] = read();
	}
	for (int i = 1; i <= n; ++i) {
		if (a[i] > a[i - 1]) {
			ans += a[i] - a[i - 1];
		}
	}
	printf("%lld", ans);
	return 0;
}
