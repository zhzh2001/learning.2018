#include <bits/stdc++.h>


#define OPEN(file) \
	freopen(#file".in", "r", stdin), \
	freopen(#file".out", "w", stdout)


int read(void)
{
	int x = 0, c = getchar();
	for (; !isdigit(c); c = getchar());
	for (;  isdigit(c); c = getchar()) {
		x = (x << 3) + (x << 1) + c - 48;
	}
	return x;
}


int a[10086];
bool mark[10086];


int main(void)
{
	OPEN(money);
	int _t = read();
	while (_t--) {
		int n = read();
		for (int i = 1; i <= n; ++i) {
			a[i] = read();
		}
		std::sort(a + 1, a + 1 + n);
		int ans = n;
		memset(mark, false, sizeof mark);
		if (n == 2) {
			if (a[2] % a[1] == 0) {
				ans = 1;
			} else {
				ans = 2;
			}
		} else if (n == 3) {
			ans = 3;
			for (int i = 0; i <= a[3] / a[1]; ++i) {
				if ((a[3] - (a[1] * i)) % a[2] == 0) {
					ans = 2;
				}
			}
			if (a[2] % a[1] == 0) {
				--ans;
			}
		} else if (n == 4) {
			ans = 4;
			for (int i = 0; i <= a[4] / a[1]; ++i) {
				for (int j = 0; j <= (a[4] - i * a[1]) / a[2]; ++j) {
					if ((a[4] - (a[1] * i) - (a[2] * j)) % a[3] == 0) {
						ans = 3;
					}
				}
			}
			for (int i = 0; i <= a[3] / a[1]; ++i) {
				if ((a[3] - (a[1] * i)) % a[2] == 0) {
					--ans;
					break;
				}
			}
			if (a[2] % a[1] == 0) {
				--ans;
			}
		} else if (n == 5) {
			ans = 5;
			for (int i = 0; i <= a[5] / a[1]; ++i) {
				for (int j = 0; j <= (a[5] - i * a[1]) / a[2]; ++j) {
					for (int k = 0; k <= (a[5] - i * a[1] - j * a[2]) / a[3]; ++k) {
						if ((a[5] - (a[1] * i) - (a[2] * j) - (a[3] * k)) % a[4] == 0) {
							ans = 4;
						}
					}
				}
			}
			bool flag = false;
			for (int i = 0; i <= a[4] / a[1]; ++i) {
				for (int j = 0; j <= (a[4] - i * a[1]) / a[2]; ++j) {
					if ((a[4] - (a[1] * i) - (a[2] * j)) % a[3] == 0) {
						flag = true;
					}
				}
			}
			if (flag) {
				--ans;
			}
			for (int i = 0; i <= a[3] / a[1]; ++i) {
				if ((a[3] - (a[1] * i)) % a[2] == 0) {
					--ans;
					break;
				}
			}
			if (a[2] % a[1] == 0) {
				--ans;
			}
		} else {
			memset(mark, 0, sizeof mark);
			for (int i = 2; i <= n; ++i) {
				for (int j = 1; j < i; ++j) {
					if (mark[j]) {
						continue;
					}
					for (int k = j; k < i; ++k) {
						if (mark[k]) {
							continue;
						}
						for (int l = k; l < i; ++l) {
							if (mark[l]) {
								continue;
							}
							for (int p = 0; p <= a[i] / a[j]; ++p) {
								for (int q = 0; q <= (a[i] - p * a[j]) / a[k]; ++q) {
									if ((a[i] - a[j] * p - a[k] * q) % a[l] == 0) {
										mark[i] = true;
									}
								}
							}
						}
					}
				}
			}
			for (int i = 1; i <= n; ++i) {
				if (mark[i]) {
					--ans;
				}
			}
		}
		printf("%d\n", ans);
	}
	return 0;
}
