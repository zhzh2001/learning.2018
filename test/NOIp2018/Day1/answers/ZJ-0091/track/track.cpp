#include <bits/stdc++.h>


#define OPEN(file) \
	freopen(#file".in", "r", stdin), \
	freopen(#file".out", "w", stdout)


int read(void)
{
	int x = 0, c = getchar();
	for (; !isdigit(c); c = getchar());
	for (;  isdigit(c); c = getchar()) {
		x = (x << 3) + (x << 1) + c - 48;
	}
	return x;
}


struct Edge {
	int next, to, len;
} edge[100086];

int cnt, head[100086];


void link(int x, int y, int z)
{
	edge[++cnt] = (Edge) {head[x], y, z};
	head[x]     = cnt;
	return;
}


namespace P20
{
	int frs[100086], dis[100086], distan[100086];

	void dfs(int x, int f)
	{
		frs[x] = x;
		for (int i = head[x]; i; i = edge[i].next) {
			int y = edge[i].to;
			if (y == f) {
				continue;
			}
			dis[y] = edge[i].len;
			dfs(y, x);
			if (dis[y] + distan[y] > distan[x]) {
				distan[x] = dis[y] + distan[y];
				frs[x] = frs[y];
			}
		}
		return;
	}

	void solve(void)
	{
		dfs(1, 0);
		int q = frs[1];
		memset(distan, 0, sizeof distan);
		dfs(q, 0);
		printf("%d\n", distan[q]);
		return;
	}
}


int main(void)
{
	OPEN(track);
	int n = read(), m = read();
	for (int i = 1; i < n; ++i) {
		int a = read(), b = read(), c = read();
		link(a, b, c), link(b, a, c);
	}
	P20::solve();
	return 0;
}
