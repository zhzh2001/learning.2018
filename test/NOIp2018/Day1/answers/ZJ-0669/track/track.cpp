#include<cstdio>
#include<cctype>
#include<algorithm>
#define maxn 50050
using namespace std;
inline int read() {
	int x = 0;char c=getchar();
	while(!isdigit(c)) c=getchar();
	while(isdigit(c)) x=(x<<3)+(x<<1)+c-48,c=getchar();return x;
}
struct Edge{
	int to,dis,next;
}e[maxn];
int n,m,head[maxn],vis[maxn],cnt,ans,in[maxn],flag = true,flag2 = true;
inline int cmp(Edge x,Edge y) {return x.dis < y.dis;}
inline void add(int u,int v,int w) { e[++cnt].to=v;e[cnt].dis = w;e[cnt].next=head[u];head[u]=cnt; }

void dfs(int u,int fa,int dis) {
	if(dis > ans) {
		ans = dis;
	}
	for(int i=head[u];i;i=e[i].next) {
		int v=e[i].to;
		if(v==fa) continue;
			dfs(v,u,dis+e[i].dis);
	}
}

int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	
	n=read(),m=read();
	for(int i=1;i<n;i++) {
		int x=read(),y=read(),w=read();
		if(x!=1) flag = false;
		if(y!=x+1) flag2 = false;
		add(x,y,w);add(y,x,w);in[y]++;in[x]++;
	}
	
	if(m == n-1) {
		sort(e+1,e+n,cmp);
		printf("%d",e[1].dis);
		return 0;
	}
	
	if(m == 1) {
		sort(in+1,in+1+n);
		for(int i=1;i<=n;i++){
			if(in[i]!=in[1]) continue;
			dfs(i,-1,0);
		}
		printf("%d",ans);
		return 0;
	}
	
	return 0;
}
