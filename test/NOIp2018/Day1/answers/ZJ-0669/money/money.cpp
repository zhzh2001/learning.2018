#include<cstdio>
#include<cctype>
#include<algorithm>
#include<cstring>
#define maxn 150
using namespace std;
inline int read() {
	int x = 0;char c=getchar();
	while(!isdigit(c)) c=getchar();
	while(isdigit(c)) x=(x<<3)+(x<<1)+c-48,c=getchar();return x;
}

int T,n,a[maxn],vis[maxn],flag2 = false;
int flag;
inline int cmp(int x,int y) { return x<y; }
inline void dfs(int x,int num) {
	if(x==0) {
		flag2 = true;
		return;
	}
	for(int i=1;i<=n;i++) {
		if(i == num) continue;
		if(a[i]>x) return;
		if(x-a[i]>=0) {
			dfs(x-a[i],num);
		}
	}
}
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--) {
		memset(vis,0,sizeof(vis));
		flag = false;
		n = read();
		for(int i=1;i<=n;i++) {
			a[i]=read();
			if(a[i] == 1) {
				flag = true;
			}
		}
		
		if(flag) { printf("1\n");return 0; }
		
		sort(a+1,a+1+n,cmp);
		for(int i=2;i<=n;i++) {
			flag2 = false;
			dfs(a[i],i);
			if(flag2) {
				vis[i] = 1;
			}
		}
		int ret = 0;
		for(int i=1;i<=n;i++) if(vis[i]) ret++;
		printf("%d\n",n-ret); 
	}
	return 0;
}
