#include<cstdio>
#include<cctype>
#include<stack>
#include<algorithm>
#define maxn 100050
using namespace std;
inline int read() {
	int x = 0;char c=getchar();
	while(!isdigit(c)) c=getchar();
	while(isdigit(c)) x=(x<<3)+(x<<1)+c-48,c=getchar();return x;
}

int n,a[maxn],vis[maxn];
int s[maxn],top=0;
int flag = true;
int ans = 0;
int main() {
	//freopen("test.in","r",stdin);
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	
	n = read();for(int i=1;i<=n;i++) {a[i] = read();}
	for(int i=1;i<=n;i++) if(a[i]!=a[i+1]) flag = false;
	if(flag) {
		printf("%d",a[1]);
		return 0;
	}
	
	for(int i=1;i<=n;i++) {
		ans = max(ans,a[i]);
	}
	printf("%d",ans);
	return 0;	
}
