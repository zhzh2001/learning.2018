#include <cstdio>
#include <cstring>
#include <iostream>
#include <cmath>
using namespace std;
const int maxn = 100005;
int n, a[maxn], b[maxn];
long long res = 0;

int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++) scanf("%d", a + i);
	a[0] = 0;
	for (int i = 1; i <= n; i++) b[i] = a[i] - a[i - 1];
	for (int i = 1; i <= n; i++)
		if (b[i] > 0) res += 1ll * b[i];
	printf("%lld\n", res); 
	return 0;
}
