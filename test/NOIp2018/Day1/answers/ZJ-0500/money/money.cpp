#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>
#define maxn 105
using namespace std;

int n, a[maxn], res, T;
bool dp[25005];

int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	while (T--){
		scanf("%d", &n);
		for (int i = 1; i <= n; i++) scanf("%d", a + i);
		sort(a + 1, a + n + 1);
		
		res = n;
		memset(dp, false, sizeof dp); dp[0] = true;
		for (int i = 1; i <= n; i++){
			if (dp[a[i]]) res--;
			for (int j = 0; j + a[i] < 25005; j++)
				if (dp[j]) dp[j + a[i]] = true;
		}
		printf("%d\n", res);
	}
	return 0;
}
