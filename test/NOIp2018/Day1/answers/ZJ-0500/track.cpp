#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#define maxn 50005
using namespace std;

struct Edg{
	int u, v, l;
	bool operator <(Edg E)const{
		return l > E.l;
	}
}edg[maxn];
bool cmp(Edg x, Edg y){
	return x.u < y.u;
}

struct node{
	int to, nxt, l;
}e[maxn << 1];
int head[maxn], tot = -1;
void addedge(int u, int v, int l){
	e[++tot].to = v, e[tot].nxt = head[u], e[tot].l = l;
	head[u] = tot;
}

int n, m, res = -1; 

//for m = 1
int fdp[maxn];
void dp(int cur, int fa){
	for (int i = head[cur]; i != -1; i = e[i].nxt){
		if (e[i].to != fa){
			fdp[e[i].to] = fdp[cur] + e[i].l;
			dp(e[i].to, cur);
//			fdp[cur] = max(fdp[cur], e[i].l + fdp[e[i].to]);
		}
	}
}

//for spe_2
bool check(int l){
	int sum = 0, cnt = 0;
	for (int i = 1; i < n; i++){
		sum += edg[i].l;
		if (sum >= l){
			cnt++;
			sum = 0;
		}
	}
	if (cnt >= m) return true;
	return false;
}

int sum[maxn];
int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	
	memset(head, -1, sizeof head);
	scanf("%d%d", &n, &m);
	
	bool spe_1 = true, spe_2 = true;
	for (int i = 1; i < n; i++){
		scanf("%d%d%d", &edg[i].u, &edg[i].v, &edg[i].l); 
		addedge(edg[i].u, edg[i].v, edg[i].l), addedge(edg[i].v, edg[i].u, edg[i].l);
		if (edg[i].u != 1) spe_1 = false;
		if (edg[i].v != edg[i].u + 1) spe_2 = false;
		sum[i] = sum[i - 1] + edg[i].l;
	}
	
	//task #1 #4 #5 #6
	if (m == 1){
		memset(fdp, 0, sizeof fdp);
		dp(1, 0);
		int p, Max = -1;
		for (int i = 1; i <= n; i++)
			if (fdp[i] > Max) p = i, Max = fdp[i];
		memset(fdp, 0, sizeof fdp); res = -1;
		dp(p, 0);
		for (int i = 1; i <= n; i++)
			res = max(res, fdp[i]);
		printf("%d\n", res);
		return 0;
	}
	//task #3 #7 #8
	if (spe_1){
		sort(edg + 1, edg + n);
		res = 0x3f3f3f3f;
		for (int i = 1; i <= m; i++)
			res = min(res, edg[i].l + edg[2 * m - i + 1].l);
		printf("%d\n", res);
		return 0;
	}
	//task #2 #9 #10 #11
	if (spe_2){
		sort(edg + 1, edg + n, cmp);
		int l = 1, r = sum[n - 1];
		while (l <= r){
			int mid = (l + r) >> 1;
			if (check(mid)) l = mid + 1, res = mid;
			else r = mid - 1;
		}
		printf("%d\n", res);
		return 0;
	}
	printf("%d\n", edg[1].l + edg[n - 1].l);
	return 0;
}
