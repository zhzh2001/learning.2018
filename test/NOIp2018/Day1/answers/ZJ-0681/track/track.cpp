#include <bits/stdc++.h>
using namespace std;
#define Rint register int
typedef long long ll;
inline ll read(){
	ll x=0,w=1;char c=getchar();
	while(!isdigit(c)){
		if(c=='-')w=-1;
		c=getchar();
	}
	while(isdigit(c)){
		x=(x<<1)+(x<<3)+c-'0';
		c=getchar();
	}
	return x*w;
}
ll hd[50010],eds[100010],nxt[100010],val[100010],et=0;
inline void ad(ll u,ll v,ll w){
	eds[++et]=v;
	nxt[et]=hd[u];
	hd[u]=et;
	val[et]=w;
}
ll nr[50010];
ll dfs(ll u,ll pa,ll ord){
	ll maxx=0,tmp;
	for (Rint i=hd[u];i;i=nxt[i])
		if(eds[i]!=pa){
			if((tmp=dfs(eds[i],u,i))>maxx){
				maxx=tmp;
				nr[u]=eds[i];
			}
		}
	return maxx+val[ord];
}
ll n,m,all_sum=0;
ll check_line(ll x){
	ll res=0,tmp=0;
	for (Rint i=1;i<n;i++)
		for (Rint j=hd[i];j;j=nxt[j])
			if(eds[j]==i+1){
				tmp+=val[j];
				if(tmp>=x){
					res++;
					tmp=0;
				}
			}
	return res;
}
ll cnt[50010],XR,f[50010];
bool usd[50010];
ll lgtmp[50010],lgptr;
bool cmp(ll a,ll b){
	return f[a]<f[b];
}
void luan_gao(ll u,ll pa,ll ord){
	for (Rint i=hd[u];i;i=nxt[i])
		if(eds[i]!=pa){
			luan_gao(eds[i],u,i);
			cnt[u]+=cnt[eds[i]];
		}
	lgptr=0;
	for (Rint i=hd[u];i;i=nxt[i])
		if(eds[i]!=pa)
			lgtmp[++lgptr]=eds[i];
	sort(lgtmp+1,lgtmp+1+lgptr,cmp);
	while(lgptr&&f[lgtmp[lgptr]]>=XR){
		usd[lgtmp[lgptr]]=true;
		cnt[u]++;
		lgptr--;
	}
	for (Rint i=1;i<lgptr;i++)
		if(!usd[lgtmp[i]]){
			for (Rint j=i+1;j<=lgptr;j++)
				if(!usd[lgtmp[j]]&&f[lgtmp[i]]+f[lgtmp[j]]>=XR){
					usd[lgtmp[i]]=usd[lgtmp[j]]=true;
					cnt[u]++;
					break;
				}
		}
	for (Rint i=lgptr;i>=1;i--)
		if(!usd[lgtmp[i]]){
			f[u]=f[lgtmp[i]];
			break;
		}
	f[u]+=val[ord];
}
ll check(ll x){
	memset(cnt,0,sizeof(cnt));
	memset(f,0,sizeof(f));
	memset(usd,0,sizeof(usd));
	XR=x;
	luan_gao(1,0,0);
	ll res=cnt[1];
	return res;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	bool isline=true;
	n=read(),m=read();
	ll t1,t2,t3;
	for (Rint i=1;i<n;i++){
		t1=read(),t2=read(),t3=read();
		all_sum+=t3;
		if(t2!=t1+1)isline=false;
		ad(t1,t2,t3);
		ad(t2,t1,t3);
	}
	if(m==1){
		memset(nr,0,sizeof(nr));
		dfs(1,0,0);
		ll ptr=1;
		while(nr[ptr])ptr=nr[ptr];
		printf("%lld\n",dfs(ptr,0,0));
	}else if(isline){
		ll l=0,r=all_sum,mid,ans=-1;
		while(l<=r){
			mid=(l+r)>>1;
			if(check_line(mid)>=m){
				ans=mid;
				l=mid+1;
			}else r=mid-1;
		}
		printf("%lld\n",ans);
	}else{
		ll l=0,r=all_sum,mid,ans=-1;
		while(l<=r){
			mid=(l+r)>>1;
			if(check(mid)>=m){
				ans=mid;
				l=mid+1;
			}else r=mid-1;
		}
		printf("%lld\n",ans);
	}
	return 0;
}
