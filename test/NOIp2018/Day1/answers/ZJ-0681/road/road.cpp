#include <bits/stdc++.h>
using namespace std;
#define Rint register int
typedef long long ll;
const ll INF=0x3f3f3f3f3f3f3f3f;
inline ll read(){
	ll x=0,w=1;char c=getchar();
	while(!isdigit(c)){
		if(c=='-')w=-1;
		c=getchar();
	}
	while(isdigit(c)){
		x=(x<<1)+(x<<3)+c-'0';
		c=getchar();
	}
	return x*w;
}
ll src[200010],maxx,n,lst,res;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (Rint i=1;i<=n;i++)
		src[i]=read();
	maxx=src[1],lst=res=0;
	src[0]=INF;
	src[n+1]=INF;
	for (Rint i=2;i<=n+1;i++){
		if(src[i]>src[i-1]){
			res+=maxx-lst;
			lst=src[i-1];
			maxx=src[i];
		}
	}
	printf("%lld\n",res);
	return 0;
}
