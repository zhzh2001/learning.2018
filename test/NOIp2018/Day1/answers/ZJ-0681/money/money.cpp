#include <bits/stdc++.h>
using namespace std;
#define Rint register int
typedef long long ll;
inline ll read(){
	ll x=0,w=1;char c=getchar();
	while(!isdigit(c)){
		if(c=='-')w=-1;
		c=getchar();
	}
	while(isdigit(c)){
		x=(x<<1)+(x<<3)+c-'0';
		c=getchar();
	}
	return x*w;
}
ll T,n,src[210],maxx;
bool f[500010];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		memset(f,0,sizeof(f));
		f[0]=true;
		maxx=0;
		n=read();
		for (Rint i=1;i<=n;i++)
			src[i]=read(),maxx=max(maxx,src[i]);
		sort(src+1,src+1+n);
		ll res=n;
		for (Rint i=1;i<=n;i++){
			if(f[src[i]]){
				res--;
				continue;
			}
			for (Rint j=src[i];j<=maxx;j++)
				if(f[j-src[i]])
					f[j]=true;
		}
		printf("%lld\n",res);
	}
	return 0;
}
