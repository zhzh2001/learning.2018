#include <cstdio>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstring>
using namespace std;
inline int read() {
	int f=1,x=0;
	char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9') {
		x=x*10+c-'0';
		c=getchar();
	}
	return f*x;
}
const int maxn=1e5+5;
int n,m,g,o,p,k;
int lnk[maxn],nxt[maxn],s[maxn],w[maxn],cnt,ans;
int t1,w1,h[maxn*10],dis[maxn],l,r,mid;
int num,num1;
int xx[maxn];
int vis[maxn];
void adde(int x,int y,int z) {
	s[++cnt]=y;
	w[cnt]=z;
	nxt[cnt]=lnk[x];
	lnk[x]=cnt;
}
void doing(int x) {
	t1=0;
	w1=1;
	h[1]=x;
	memset(dis,0,sizeof(dis));
	memset(vis,0,sizeof(vis));
	vis[x]=1;
	while (t1<w1) {

		t1++;
		for (int i=lnk[h[t1]]; i!=-1; i=nxt[i])
			if (!vis[s[i]]) {
				w1++;
				h[w1]=s[i];
				dis[s[i]]=dis[h[t1]]+w[i];
				vis[s[i]]=1;
			}
	}
	int mx=0;
	for (int i=1; i<=n; i++)
		if (dis[i]>mx) mx=dis[i],p=i;
}
bool check(int x) {
	int q=0;
	g=0;
	for (int i=1; i<=n-1; i++) {
		if (g+xx[i]>=x) g=0,q++;
		else g+=xx[i];
	}
	if (g>=x) q++;
	int u=0;
	g=0;
	for (int i=n-1; i>=1; i--)
		if (g+xx[i]>=x) g=0,u++;
		else g+=xx[i];
	if (g>=x) u++;
	if (q>=m||u>=m) return 1;
	else return 0;
}
bool check1(int x){
	int q=0,s1=0;g=0;
	for (k=n-1;k>=2;k--)
	if (xx[k]>=x) s1++;
    else break;
    int v=2;
    while (xx[v]+xx[k]<x&&k>v)
    {
    	v++;
	}
    if (k>v)
    {
    	while (k>v){
    		if (xx[v]+xx[k]>=x) s1++;
    		v++;
    		k--;
		}
	}
	if (s1>=m) return 1;
    else return 0;
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	memset(lnk,-1,sizeof(lnk));
	n=read();
	m=read();
	for (int i=1; i<=n-1; i++) {
		int x=read(),y=read(),z=read();
		if (x==1) num++;
		if (y==x+1) num1++;
		adde(x,y,z);
		adde(y,x,z);
	}
	if (m==1) {
		doing(1);
		doing(p);
		printf("%d",dis[p]);
	} else if (num1==n-1) {
		l=2e9+1e8;
		for (int i=1; i<=n-1; i++)
			xx[s[i*2-1]-1]=w[i*2-1];
		for (int i=1; i<=n-1; i++)
			l=min(l,xx[i]),r+=xx[i];
		while (l<=r) {
			int mid=(l+r)>>1;
			if (check(mid)) ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	} else if (num==n-1) {
		l=2e9+1e8;
		for (int i=1; i<=n-1; i++)
			xx[s[i*2-1]]=w[i*2-1];
		for (int i=2; i<=n-1; i++)
			l=min(l,xx[i]),r+=xx[i];
			sort(xx+2,xx+n);
		while (l<=r) {
			int mid=(l+r)>>1;
			if (check1(mid)) ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
	return 0;
}
