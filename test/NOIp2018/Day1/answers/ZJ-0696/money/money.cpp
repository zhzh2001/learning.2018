#include <cstdio>
#include <iostream>
#include <cmath>
#include <cstring>
#include <algorithm>
#define max(x,y) x>y?x:y
using namespace std;
inline int read(){
	int f=1,x=0;
	char c=getchar();
	while (c<'0'||c>'9'){if (c=='-') f=-1;c=getchar();}
	while (c>='0'&&c<='9'){x=x*10+c-'0';c=getchar();}
	return f*x;
}
const int maxn=1e6+5;
const int maxm=105;
int n,m,g,h,o,p,ans,num;
int mx,f[maxn],a[maxm],sum;
int vis[maxn],b[maxn],c[maxm];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t=read();
	while (t--)
	{
	memset(f,0,sizeof(f));
	memset(vis,0,sizeof(vis));
	memset(b,0,sizeof(b));
	n=read();
	sum=0;num=0;
	for (int i=1;i<=n;i++) c[i]=read(),sum=max(sum,c[i]);
	f[0]=1;
	sort(c+1,c+n+1);
	c[0]=-1;
	for (int i=1;i<=n;i++)
	if (c[i]!=c[i-1]) num++,a[num]=c[i];
	ans=num;
	for (int i=1;i<=num;i++) 
	{
	vis[a[i]]=1,b[a[i]]=i;
    }
	for (int i=1;i<=num;i++)
	{
		if (vis[a[i]]==0) continue;
		for (int j=a[i];j<=sum;j++)
		if (f[j-a[i]]) {
		
			if (vis[j]==1&&b[j]>i) vis[j]=0,ans--;	
			f[j]=1;
		}
	}
	printf("%d\n",ans);
}
	return 0;
}
