#include <cstdio>
#include <iostream>
#include <cstring>
#include <cmath>
#include <algorithm>
#define min(x,y) x>y?y:x
using namespace std;
inline int read(){
	int f=1,x=0;
	char c=getchar();
	while (c<'0'||c>'9'){if (c=='-') f=-1;c=getchar();}
	while (c>='0'&&c<='9'){x=x*10+c-'0';c=getchar();}
	return f*x;
}
const int maxn=1e5+5;
int n,m,g,h,o,p;
int a[maxn],b[maxn];
long long ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();p=2e9+1e8;
	for (int i=1;i<=n;i++)
	a[i]=read(),p=min(a[i],p);
	ans=p;
	for (int i=1;i<=n;i++)
	a[i]-=p;
	int j=1;
	while (j<=n){
		o=0;
		while (a[j]>=o&&j<=n) o=a[j++];
		j--;
		ans+=a[j];
		j++;
		while (a[j]<=a[j-1]&&j<=n) j++;
		if (a[j]!=0) ans-=a[j-1];
	}
	printf("%lld\n",ans);
	return 0;
}
