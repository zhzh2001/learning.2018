#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int MAXN=100005;
int n,a[MAXN],num[4*MAXN];
ll tree[4*MAXN];
int find(int x,int y,int l,int r,int t)
{
	if(x<=l && r<=y)	return num[t];
	int mid=(l+r)>>1;
	if(mid>=y)
		return find(x,y,l,mid,t<<1);
	if(x>mid)
		return find(x,y,mid+1,r,t<<1|1);
	int id1=find(x,mid,l,mid,t<<1),id2=find(mid+1,y,mid+1,r,t<<1|1);
	if(a[id1]<a[id2])	return id1;
	else	return id2;
}
ll getans(int l,int r,int s)
{
	if(l==r)	return a[l]-s;
	int t=find(l,r,1,n,1);
	ll ans=a[t]-s;
	if(t-1>=l)	ans=ans+getans(l,t-1,a[t]);
	if(t+1<=r)	ans=ans+getans(t+1,r,a[t]);
	return ans;
}
void build(int l,int r,int t)
{
	if(l==r)
	{
		tree[t]=a[l];num[t]=l;return;
	}
	int mid=(l+r)>>1;
	build(l,mid,t<<1);build(mid+1,r,t<<1|1);
	if(tree[t<<1]<tree[t<<1|1])
	{
		tree[t]=tree[t<<1];num[t]=num[t<<1];
	}
	else
	{
		tree[t]=tree[t<<1|1];num[t]=num[t<<1|1];
	}
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	build(1,n,1);
	printf("%lld",getans(1,n,0));
	return 0;
}
