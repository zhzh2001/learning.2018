#include<bits/stdc++.h>
using namespace std;
int head[50005],next[100005],edge[100005],data[100005],n,m,num=0;
int f[50005][25],s[50005],b[100005],dep[50005],nodenum=0,ed[50005];
bool cmp(int x,int y)
{
	return x>y;
}
void add(int x,int y,int z)
{
	nodenum++;next[nodenum]=head[x];
	head[x]=nodenum;edge[nodenum]=y;
	data[nodenum]=z;
}
int lca(int x,int y)
{
	if(dep[x]<dep[y])	swap(x,y);
	for(int i=20;i>=0;i--)
	{
		if(dep[f[x][i]]<dep[y])	continue;
		x=f[x][i];
		if(x==y)	return x;
	}
	for(int i=20;i>=0;i--)
	if(f[x][i]!=f[y][i])
	{	
		x=f[x][i];y=f[y][i];
	}
	return f[x][0];
}
void dfs(int u,int pre)
{
	if(dep[u]==0)	dep[u]=dep[pre]+1;
	f[u][0]=pre;
	for(int i=1;i<=20;i++)
		f[u][i]=f[f[u][i-1]][i-1];
	for(int i=head[u];i;i=next[i])
	{
		int v=edge[i];
		if(v==pre)	continue;
		if(s[v]==0)	s[v]=s[u]+data[i];
		dfs(v,u);
	}	
	return;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int x,y,z;
	for(int i=1;i<n;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		ed[i]=z;
		add(x,y,z);add(y,x,z);	
	}
	dfs(1,0);
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=i-1;j++)
		{
			int k=lca(i,j);
			ans=max(ans,s[i]+s[j]-2*s[k]);
		}
	sort(ed+1,ed+n,cmp);
	if(m==1)
		printf("%d",ans);
	if(m==n-1)
		printf("%d",ed[n-1]);
}
