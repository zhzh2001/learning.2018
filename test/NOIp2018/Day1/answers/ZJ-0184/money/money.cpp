#include<bits/stdc++.h>
using namespace std;
int n,A[105];
struct P0 {
	bool Mark[15];
	int Tmp[15],Str[15];
	bool Pailie(int Now,int Len) {
		if(Now==Len+1) {
			bool Check=true;
			for(int i=1; i<=Len; i++)
				if(Str[i]%A[Tmp[i]]!=0)Check=false;
			return Check;
		}
		for(register int i=1; i<=Len; i++) {
			if(!Mark[i]&&Str[Now]%A[i]==0) {
				Mark[i]=1;
				Tmp[Now]=i;
				if(Pailie(Now+1,Len))return true;
				Mark[i]=0;
			}
		}
		return false;
	}
	bool Dfs(int Now,int Sum,int Len) { //现在第几个 还剩多少 要多少个
		if(Now==Len) {
			Str[Now]=Sum;
			if(Now!=1&&Str[Now]<Str[Now-1])return false;
			if(Pailie(1,Len))return true;
			return false;
		}
		for(register int i=0; i<=Sum-(Len-Now); i++) {
			Str[Now]=i;
			if(Now!=1&&Str[Now]<Str[Now-1])continue;
			if(Dfs(Now+1,Sum-i,Len))return true;
		}
		return false;
	}
	void Solve() {
		int k=0;
		for(int i=1; i<=n; i++)
			if(A[i]!=A[i-1]||i==1)
				A[++k]=A[i];
		int Ans=k;
		for(int i=k; i>=2; i--) {
			memset(Mark,0,sizeof(Mark));
			if(Dfs(1,A[i],i-1))Ans--;
		}
		printf("%d\n",Ans);
	}
} P0;
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--) {
		scanf("%d",&n);
		for(int i=1; i<=n; i++)scanf("%d",&A[i]);
		sort(A+1,A+n+1);
		P0.Solve();
	}
	return 0;
}
