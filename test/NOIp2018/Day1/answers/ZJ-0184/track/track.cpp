#include<bits/stdc++.h>
using namespace std;
int n,m;
struct node {
	int To,Cost;
};
vector<node>Edge[50005];
struct P20 {
	int Mxdis,Dx;
	void Dfs(int Now,int From,int Dis) {
		if(Dis>Mxdis)Mxdis=Dis,Dx=Now;
		for(int i=0; i<(int)Edge[Now].size(); i++) {
			int Nxt=Edge[Now][i].To;
			if(Nxt==From)continue;
			Dfs(Nxt,Now,Dis+Edge[Now][i].Cost);
		}
	}
	void Solve() {
		Mxdis=-1,Dfs(1,0,0);
		Mxdis=-1,Dfs(Dx,0,0);
		printf("%d\n",Mxdis);
	}
} P20;
struct P40 {
	int A[50005];
	bool Check(int Num) {
		int Cnt=0,Sum=0;
		for(int i=1; i<=n; i++) {
			Sum+=A[i];
			if(Sum>=Num)Cnt++,Sum=0;
		}
		return Cnt>=m;
	}
	void Solve() {
		for(int i=1; i<n; i++)
			for(int j=0; j<(int)Edge[i].size(); j++) {
				int Nxt=Edge[i][j].To;
				if(Nxt!=i-1)A[Nxt]=Edge[i][j].Cost;
			}
		int L=1,R=1e9,Ans;
		while(L<=R) {
			int Mid=(L+R)>>1;
			if(Check(Mid))Ans=Mid,L=Mid+1;
			else R=Mid-1;
		}
		printf("%d\n",Ans);
	}
} P40;
struct P0 {
	int A[50005];
	bool Mark[50005];
	bool Check(int Num) {
		memset(Mark,0,sizeof(Mark));
		int Cnt=0;
		for(int i=1; i<=A[0]; i++) {
			if(Mark[i])continue;
			for(int j=i+1; j<=A[0]; j++) {
				if(Mark[j])continue;
				if(A[i]+A[j]>=Num) {
					Cnt++;
					Mark[i]=Mark[j]=1;
				}
			}
			if(Cnt>=m)return true;
		}
		return false;
	}
	void Solve() {
		A[0]=0;
		for(int i=0; i<(int)Edge[1].size(); i++) {
			int Nxt=Edge[1][i].To;
			A[++A[0]]=Edge[1][i].Cost;
		}
		sort(A+1,A+A[0]+1);
		int L=1,R=1e9,Ans;
		while(L<=R) {
			int Mid=(L+R)>>1;
			if(Check(Mid))Ans=Mid,L=Mid+1;
			else R=Mid-1;
		}
		printf("%d\n",Ans);
	}
} P0;
bool P_Lian;
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	P_Lian=true;
	scanf("%d%d",&n,&m);
	for(int i=1,u,v,c; i<n; i++) {
		scanf("%d%d%d",&u,&v,&c);
		if(u>v)swap(u,v);
		Edge[u].push_back(node {v,c});
		Edge[v].push_back(node {u,c});
		if(u!=v-1)P_Lian=false;
	}
	if(m==1)P20.Solve();
	else if(P_Lian)P40.Solve();
	else P0.Solve();
	return 0;
}
