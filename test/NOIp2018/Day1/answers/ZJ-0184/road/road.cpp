#include<bits/stdc++.h>
using namespace std;
int n,Ans;
int A[100005];
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1; i<=n; i++)scanf("%d",&A[i]);
	for(int i=1; i<=n; i++) {
		bool Check=(A[i]!=0);
		if(A[i-1]>A[i])Check=false;
		if(A[i+1]>A[i])Check=false;
		if(Check) {
			Ans+=A[i];
			int L=i,R=i;
			for(int j=i-1; j>=1; j--) {
				if(A[j]>A[j+1]||A[j]==0)break;
				L=j;
			}
			for(int j=i+1; j<=n; j++) {
				if(A[j]>A[j-1]||A[j]==0)break;
				R=j;
			}
			int Mn=A[R];
			for(register int j=R+1; j<=n; j++) {
				if(Mn==0)break;
				if(A[j]<Mn)Mn=A[j];
				A[j]-=Mn;
			}
			for(int j=L; j<=R; j++)A[j]=0;
		}
	}
	printf("%d\n",Ans);
	return 0;
}
