#include <cstdio>
#include <vector>
#include <algorithm>

const int MAXN = 50005;
const int INF = 0x3f3f3f3f;

int n, m, ans, siz, tot, sum, mid, cnt, fir;
std::vector <int> g[MAXN];

template <typename T>
inline void read(T &x) {
	int fl = 0, ch;
	while (ch = getchar(), ch < 48 || 57 < ch) fl ^= !(ch ^ 45); x = (ch & 15);
	while (ch = getchar(), 47 < ch && ch < 58) x = x * 10 + (ch & 15);
	if (fl) x = -x;
}

struct ed {
	ed *nxt;
	int to, w;
} pool[MAXN << 1], *p = pool, *lnk[MAXN];

inline void add_e(int u, int v, int w) { *++p = (ed) { lnk[u], v, w}, lnk[u] = p; }

inline int dfs(int u, int fa) {
	g[u].clear();
	for (ed *i = lnk[u]; i; i = i->nxt)
		if (i->to != fa) {
			int t = dfs(i->to, u) + i->w;
			if (t >= mid) ++tot;
			else g[u].push_back(t);
		}
	siz = (int)g[u].size();
	if (siz > 0) {
		std::sort(g[u].begin(), g[u].end());
		if (siz == 1) return *g[u].rbegin();
		int i = 0, j = 0; fir = -1, cnt = 0;
		for (; j < siz; ++j)
			if (g[u][j] >= (mid + 1 >> 1)) break;
		i = j - 1;
		for (; i >= 0;) {
			while (j < siz && g[u][i] + g[u][j] < mid) fir = g[u][j], ++j, ++cnt;
			if (j == siz) break;
			--i; ++j, ++tot;
		}
		cnt += siz - j;
		tot += cnt >> 1;
		if (cnt & 1) return j == siz ? fir : *g[u].rbegin();
		else return i >= 0 ? g[u][i] : 0;
	} else return 0;
}

inline bool chk() {
	tot = 0;
	dfs(1, 1);
	return tot >= m;
}

int main() {
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	read(n), read(m);
	for (int i = 1, u, v, w; i < n; ++i) {
		read(u), read(v), read(w); sum += w;
		add_e(u, v, w), add_e(v, u, w);
	}
	int l = 1, r = sum;
	while (l <= r) {
		mid = (r - l >> 1) + l;
		if (chk()) ans = mid, l = mid + 1;
		else r = mid - 1;
	}
	printf("%d\n", ans);
	return 0;
}
