#include <cstdio>
#include <cstring>
#include <algorithm>

const int MAXN = 105;
const int MAXV = 25005;

int T;
int n, m;
int a[MAXN];
bool f[MAXV];

int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	while (T--) {
		memset(f, 0, sizeof(f)); f[0] = 1;
		scanf("%d", &n); m = 0;
		for (int i = 1; i <= n; ++i) scanf("%d", &a[i]);
		std::sort(a + 1, a + 1 + n);
		for (int i = 1; i <= n; ++i) if (!f[a[i]]) {
			++m;
			for (int j = a[i]; j <= a[n]; ++j) f[j] |= f[j - a[i]];
		}
		printf("%d\n", m);
	}
	return 0;
}
