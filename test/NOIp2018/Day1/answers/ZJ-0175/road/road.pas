var
    n,i,h,j:longint;
    st:array[0..100001,0..21] of longint;
    ans:int64;
    a,lg:array[0..200001] of longint;
function min(x,y:longint):longint;
begin
    if a[x]<a[y] then exit(x) else exit(y);
end;

function getmin(l,r:longint):longint;
var h:longint;
begin
    if l=r then exit(st[l,0]);
    h:=trunc(ln(r-l+1)/ln(2));
    getmin:=min(st[l,h],st[r-lg[h]+1,h]);
end;

procedure dfs(l,r,x:longint);
var y,delta:longint;
begin
    if l>r then exit;
    y:=getmin(l,r);
    delta:=a[y]-x;
    ans:=ans+delta;
    dfs(l,y-1,x+delta);dfs(y+1,r,x+delta);
end;

begin
    assign(input,'road.in');reset(input);
    assign(output,'road.out');rewrite(output);
    readln(n);
    for i:=1 to n do
        read(a[i]);
    lg[0]:=1;
    for i:=1 to 20 do lg[i]:=lg[i-1]*2;
    h:=trunc(ln(n)/ln(2))+1;
    for i:=1 to n do
        st[i,0]:=i;
    for i:=1 to h do
        for j:=1 to n-lg[i]+1 do
            st[j,i]:=min(st[j,i-1],st[j+lg[i-1],i-1]);
    dfs(1,n,0);
    writeln(ans);
    close(input);close(output);
end.
