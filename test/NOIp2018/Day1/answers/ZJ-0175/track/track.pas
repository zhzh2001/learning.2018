type
    node=record
        pos,dis:longint;
    end;

var
    n,m,x,y,z,start,finish,len,w,l,r,mid,ans,i:longint;
    fa,dis,long,q,first,next:array[0..200000] of longint;
    vis:array[0..200000] of boolean;
    edge:array[0..200000] of node;
    flag:boolean;

procedure add(x,y,z:longint);
begin
    inc(len);edge[len].pos:=y;edge[len].dis:=z;next[len]:=first[x];first[x]:=len;
end;

procedure sort(l,r: longint);
var i,j,x,y: longint;
begin
    i:=l;j:=r;
    x:=dis[(l+r) div 2];
    repeat
        while dis[i]<x do inc(i);
        while x<dis[j] do dec(j);
        if not(i>j) then
            begin
                y:=dis[i];dis[i]:=dis[j];dis[j]:=y;
                inc(i);dec(j);
            end;
    until i>j;
    if l<j then sort(l,j);
    if i<r then sort(i,r);
end;

procedure dfs(x:longint);
var i,y:longint;
begin
    vis[x]:=true;
    i:=first[x];
    while i>-1 do
        begin
            y:=edge[i].pos;
            if (vis[y]=false) then
                begin
                    dis[y]:=dis[x]+edge[i].dis;
                    fa[y]:=x;long[y]:=edge[i].dis;
                    dfs(y);
                end;
            i:=next[i];
        end;
end;

function pd(k:longint):boolean;
var  cnt,sum,i:longint;
begin
    cnt:=0;sum:=0;
    for i:=1 to w do
        begin
            sum:=sum+long[q[i]];
            if sum>=k then
                begin
                    cnt:=cnt+1;sum:=0;
                end;
        end;
    if cnt>=m then exit(true);
    exit(false);
end;

function jd(k:longint):boolean;
var l,r,cnt:longint;
begin
    l:=1;r:=n;cnt:=0;
    while dis[r]>=k do
        begin
            dec(r);inc(cnt);
        end;
    while l<r do
        begin
            if dis[l]+dis[r]>=k then
                begin
                    inc(cnt);inc(l);dec(r);
                end else inc(l);
        end;
    if cnt>=m then exit(true) else exit(false);
end;

begin
    assign(input,'track.in');reset(input);
    assign(output,'track.out');rewrite(output);
    fillchar(first,sizeof(first),255);
    readln(n,m);
    flag:=true;
    for i:=1 to n-1 do
        begin
            readln(x,y,z);
            if x>1 then flag:=false;
            add(x,y,z);add(y,x,z);
        end;
    if flag=false then
        begin
            dfs(1);
            for i:=1 to n do
            if dis[i]>dis[start] then
                start:=i;
            for i:=1 to n do
                begin
                    dis[i]:=0;vis[i]:=false;fa[i]:=0;long[i]:=0;
                end;
            dfs(start);
            for i:=1 to n do
                if dis[i]>dis[finish] then
            finish:=i;
            x:=finish;
            while x<>0 do
                begin
                    inc(w);q[w]:=x;
                    x:=fa[x];
                end;
            l:=1;r:=dis[finish];
            while l<=r do
                begin
                    mid:=(l+r)>>1;
                    if pd(mid) then
                        begin
                            ans:=mid;l:=mid+1;
                        end else r:=mid-1;
                end;
            writeln(ans);
            close(input);close(output);
            halt;
        end;
    w:=0;
    for i:=1 to n do
        begin
            inc(w);
            dis[w]:=edge[i*2-1].dis;
        end;
    sort(1,n);
    l:=1;r:=dis[n]+dis[n-1];
    while l<=r do
        begin
            mid:=(l+r)>>1;
            if jd(mid) then
                begin
                    ans:=mid;l:=mid+1;
                end else r:=mid-1;
        end;
    writeln(ans);
    close(input);close(output);
end.
