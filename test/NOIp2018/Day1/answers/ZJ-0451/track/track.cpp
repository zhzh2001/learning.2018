#include <bits/stdc++.h>
using namespace std;

const int N = 5e4 + 5;
const int M = N * 2;

int fir[N], ne[M], to[M], cnt, n, m, x, y, C[M], z;

void add(int x, int y, int z) {
  ne[++ cnt] = fir[x];
  fir[x] = cnt;
  to[cnt] = y;
  C[cnt] = z;
}

void link(int x, int y, int z) {
  add(x, y, z);
  add(y, x, z);
}

#define Foreachson(i, x) for(int i = fir[x]; i; i = ne[i])

void io() {
  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
}

int sta, tot = 0;
int who[N], MA;

#define lowbit(x) (x & (-x))

namespace BIT {
  int S[N];
  void add(int x, int v) {
    ++ x;
    for(int i = x; i < N; i += lowbit(i))
      S[i] += v;
  }
  int Sum(int x) {
    ++ x;
    int res = 0;
    for(int i = x; i; i -= lowbit(i)) {
      res += S[i];
    }
    return res;
  }
  int lower_bound(int x) {
    int l = x, r = MA - 1, res = -1;
    int now = Sum(x - 1);
    while(l <= r) {
      int mid = (l + r) >> 1;
      if(Sum(mid) - now) {
	res = mid;
	r = mid - 1;
      }
      else l = mid + 1;
    }
    return res;
  }
}

int dfs(int x, int f) {
  vector <int> Son;
  Son.clear();
  Foreachson(i, x) {
    int V = to[i];
    if(V == f) continue;
    Son.push_back(dfs(V, x) + C[i]);
    if(Son.back() >= sta) {
      ++ tot;
      Son.pop_back();
    }
  }
  int MAX = 0;
  sort(Son.begin(), Son.end());
  //BIT :: init(Son.size());
  MA = Son.size();
  for(int i = 0; i < (int) Son.size(); ++ i)
    BIT :: add(i, 1);
  who[0] = Son.size();
  for(int i = 0; i < (int) Son.size(); ++ i) {
    while(who[i] > 0 && Son[(who[i]) - 1] + Son[i] >= sta) -- who[i];
    who[i + 1] = who[i];
  }
  for(int i = 0; i < (int) Son.size(); ++ i) {
    if(BIT :: Sum(i) - BIT :: Sum(i - 1) == 0) {
      continue;
    }
    int now = Son[i];
    BIT :: add(i, -1);
    int it = BIT :: lower_bound(who[i]);
    if(it == -1) {
      MAX = max(MAX, now);
      continue;
    }
    else {
      ++ tot;
      BIT :: add(it, -1);
    }
  }
  return MAX;
}

bool pd(int x) {
  sta = x;
  tot = 0;
  dfs(1, 0);
  return tot >= m;
}

int main() {
  io();
  scanf("%d%d", &n, &m);
  for(int i = 1; i < n; ++ i) {
    scanf("%d%d%d", &x, &y, &z);
    link(x, y, z);
  }
  //pd(30);
  //return 0;
  int l = 0, r = 5e8, res = 0;
  while(l <= r) {
    int mid = (l + r) >> 1;
    //cerr << mid << endl;
    if(pd(mid)) {
      //cerr << mid <<" ok" << endl;
      res = mid, l = mid + 1;
    }
    else r = mid - 1;
  }
  cout << res << endl;
}
