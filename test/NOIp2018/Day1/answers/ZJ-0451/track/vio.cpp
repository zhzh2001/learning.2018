#include <bits/stdc++.h>
using namespace std;

const int N = 25;
const int M = N * 2;
const int MAX = (1 << 20) + 1;

int n, m, x, y, z, fir[N], ne[M], to[M], C[M];

int dp[N][MAX], trans[MAX];

#define Foreachson(i, x) for(int i = fir[x]; i; i = ne[i])

struct Edge {
  int x, y, z;
  void input() {
    scanf("%d%d%d", &x, &y, &z);
  }
}E[M];

int cnt[N];

int main() {
  scanf("%d%d", &n, &m);
  for(int i = 0; i < n - 1; ++ i) {
    E[i].input();
  }
  for(int i = 0; i < (1 << (n - 1)); ++ i) {
    memset(cnt, 0, sizeof(cnt));
    int All = 0;
    for(int j = 0; j < n - 1; ++ j)
      if((i >> j) & 1) {
	All += E[j].z;
	++ cnt[E[j].x];
	++ cnt[E[j].y];
      }
    int t1 = 0, t2 = 0;
    bool ok = 1;
    for(int j = 1; j <= n; ++ j) {
      if(cnt[j] > 2) {
	ok = 0; break;
      }
      if(cnt[j] == 1) ++ t1;
      if(cnt[j] == 2) ++ t2;
    }
    if(t1 != 2) ok = 0;
    if(!ok) continue;
    else {
      trans[i] = All;
    }
  }
  for(int i = 0; i < (1 << (n - 1)); ++ i)
    dp[0][i] = 2e9;
  for(int i = 0; i < m; ++ i) {
    for(int j = 0; j < (1 << (n - 1)); ++ j) {
      for(int k = 0; k < (1 << (n - 1)); ++  k) {
	if((j & k) == 0) {
	  dp[i + 1][j | k] = max(dp[i + 1][j | k], min(dp[i][j], trans[k]));
	}
      }
    }
  }
  int ans = 0;
  for(int i = 0; i < (1 << (n - 1)); ++ i)
    ans = max(ans, dp[m][i]);
  cout << ans << endl;
}
