#include <bits/stdc++.h>
using namespace std;

const int N = 5e4 + 5;
const int M = N * 2;

int fir[N], ne[M], to[M], cnt, n, m, x, y, C[M], z;

void add(int x, int y, int z) {
  ne[++ cnt] = fir[x];
  fir[x] = cnt;
  to[cnt] = y;
  C[cnt] = z;
}

void link(int x, int y, int z) {
  add(x, y, z);
  add(y, x, z);
}

#define Foreachson(i, x) for(int i = fir[x]; i; i = ne[i])

void io() {
  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
}

int sta, tot = 0;

int dfs(int x, int f) {
  vector <int> Son;
  Son.clear();
  Foreachson(i, x) {
    int V = to[i];
    if(V == f) continue;
    Son.push_back(dfs(V, x) + C[i]);
    if(Son.back() >= sta) {
      ++ tot;
      Son.pop_back();
    }
  }
  int MAX = 0;
  sort(Son.begin(), Son.end());
  set <pair <int, int> > S;
  S.clear();
  set < pair <int, int> > :: iterator it;
  for(int i = 0; i < (int) Son.size(); ++ i)
    S.insert(make_pair(Son[i], i));
  for(int i = 0; i < (int) Son.size(); ++ i) {
    if(S.find(make_pair(Son[i], i)) == S.end()) {
      continue;
    }
    int now = Son[i];
    S.erase(make_pair(now, i));
    it = S.upper_bound(make_pair(sta - Son[i], -1));
    if(it == S.end()) {
      MAX = max(MAX, now);
      continue;
    }
    else {
      ++ tot;
      S.erase(it);
    }
  }
  return MAX;
}

bool pd(int x) {
  sta = x;
  tot = 0;
  dfs(1, 0);
  return tot >= m;
}

int main() {
  scanf("%d%d", &n, &m);
  for(int i = 1; i < n; ++ i) {
    scanf("%d%d%d", &x, &y, &z);
    link(x, y, z);
  }
  //pd(30);
  //return 0;
  int l = 0, r = 5e8, res = 0;
  while(l <= r) {
    int mid = (l + r) >> 1;
    //cerr << mid << endl;
    if(pd(mid)) {
      //cerr << mid <<" ok" << endl;
      res = mid, l = mid + 1;
    }
    else r = mid - 1;
  }
  cout << res << endl;
}
