#include <bits/stdc++.h>
using namespace std;

const int N = 2.5e4 + 5;
const int MAX = 2.5e4;

bool vis[N];
int n, m, x, y, A[N];

void io() {
  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
}

int main() {
  io();
  int t;
  for(cin >> t; t --;) {
    scanf("%d", &n);
    for(int i = 1; i <= n; ++ i) scanf("%d", &A[i]);
    int tot = 0;
    sort(A + 1, A + n + 1);
    for(int i = 1; i <= MAX; ++ i) vis[i] = 0;
    for(int i = 1; i <= n; ++ i) {
      if(!vis[A[i]]) {
	++ tot;
	vis[A[i]] = 1;
	int now = A[i] + 1;
	for(register int j = 1; now <= MAX; ++ j, ++ now) {
	  vis[now] |= vis[j]; 
	}
      }
    }
    printf("%d\n", tot);
  }
}
