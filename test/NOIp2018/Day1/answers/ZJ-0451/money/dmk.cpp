#include <bits/stdc++.h>
using namespace std;

const int N = 111111;
const int t = 20;
const int n = 100;
const int MAX = 2.5e4;

int main() {
  freopen("data", "w", stdout);
  cout << t << endl;
  for(int i = 1; i <= t; ++ i) {
    cout << n << endl;
    for(int i = 1; i <= n; ++ i) cout << rand() % MAX + 1 << endl;
  }
}
