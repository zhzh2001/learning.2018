#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 5;

void io() {
  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
}

int A[N], n;

int main() {
  io();
  scanf("%d", &n);
  for(int i = 1; i <= n; ++ i) scanf("%d", &A[i]);
  for(int i = n; i >= 1; -- i) A[i] -= A[i - 1];
  long long Sum = 0;
  for(int i = 1; i <= n; ++ i) {
    if(A[i] > 0) Sum += A[i];
  }
  cout << Sum << endl;
}
