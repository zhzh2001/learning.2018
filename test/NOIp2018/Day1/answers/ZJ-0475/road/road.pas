var
  n,i,x,y,ans:longint;
begin
  assign(input,'road.in');
  assign(output,'road.out');
  reset(input);
  rewrite(output);

  readln(n);
  y:=0;
  ans:=0;
  for i:=1 to n do
  begin
    read(x);
    if x>y then inc(ans,x-y);
    y:=x;
  end;
  writeln(ans);

  close(input);
  close(output);
end.
