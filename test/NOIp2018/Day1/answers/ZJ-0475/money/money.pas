var
  t,tt,i,j,n,ans:longint;
  a:array[0..200]of longint;
  f:array[0..1000000]of boolean;
  procedure qs(l,r:longint);
  var
    i,j,mid,t:longint;
  begin
    i:=l;j:=r;
    mid:=a[(l+r)div 2];
    repeat
      while a[i]<mid do inc(i);
      while a[j]>mid do dec(j);
      if i<=j then
      begin
        t:=a[i];
        a[i]:=a[j];
        a[j]:=t;
        inc(i);
        dec(j);
      end;
    until i>j;
    if l<j then qs(l,j);
    if i<r then qs(i,r);
  end;
begin
  assign(input,'money.in');
  assign(output,'money.out');
  reset(input);
  rewrite(output);

  readln(t);
  for tt:=1 to t do
  begin
    readln(n);
    for i:=1 to n do
      read(a[i]);
    qs(1,n);
    fillchar(f,sizeof(f),false);
    f[0]:=true;
    ans:=0;
    for i:=1 to n do
      if f[a[i]]=false then
      begin
        inc(ans);
        for j:=a[i] to a[n] do
          if f[j-a[i]] then f[j]:=true;
      end;
    writeln(ans);
  end;

  close(input);
  close(output);
end.