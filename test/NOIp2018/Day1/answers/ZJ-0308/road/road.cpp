/*program by mangoyang*/
#include<bits/stdc++.h>
#define inf (0x7f7f7f7f)
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
typedef long long ll;
using namespace std;
template <class T>
inline void read(T &x){
	int ch = 0, f = 0; x = 0;
	for(; !isdigit(ch); ch = getchar()) if(ch == '-') f = 1;
	for(; isdigit(ch); ch = getchar()) x = x * 10 + ch - 48;
	if(f) x = -x;
}
const int N = 1000005;
int a[N], st[N], ch[N][2], n, ans, top;
inline void dfs(int u, int ad){
	ans += a[u] - ad;
	if(ch[u][0]) dfs(ch[u][0], a[u]);
	if(ch[u][1]) dfs(ch[u][1], a[u]);
}
int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	read(n);
	for(int i = 1; i <= n; i++) read(a[i]);
	for(int i = 1; i <= n; i++){
		while(top && a[i] <= a[st[top]]) top--;
		ch[i][0] = ch[st[top]][1], ch[st[top]][1] = i, st[++top] = i;
	}
	dfs(ch[0][1], 0);
	cout << ans;
	return 0;
}
