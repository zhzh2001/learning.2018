/*program by mangoyang*/
#include<bits/stdc++.h>
#define inf (0x7f7f7f7f)
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
typedef long long ll;
using namespace std;
template <class T>
inline void read(T &x){
	int ch = 0, f = 0; x = 0;
	for(; !isdigit(ch); ch = getchar()) if(ch == '-') f = 1;
	for(; isdigit(ch); ch = getchar()) x = x * 10 + ch - 48;
	if(f) x = -x;
}
const int N = 500005;
int a[N], b[N], nxt[N], head[N], cnt;
int vis[N], res, lim, n, m;
struct Node{ 
	int id, x; 
	bool operator < (const Node &A) const{ return x > A.x; }
}; priority_queue<Node> pq;
inline void add(int x, int y, int z){
	a[++cnt] = y, b[cnt] = z, nxt[cnt] = head[x], head[x] = cnt;
}
inline int dfs(int u, int fa){
	vector<int> c; c.clear();
	for(int p = head[u]; p; p = nxt[p]){
		int v = a[p];
		if(v != fa){
			int now = dfs(v, u) + b[p];
			if(now >= lim) res++;
			else c.push_back(now);
		}
	}
	int p = c.size() - 1; sort(c.begin(), c.end());
	for(int i = 0; i < (int) c.size(); i++) vis[i] = 0;
	for(int i = 0; i < (int) c.size(); i++) if(!vis[i]){
		while(p > i && c[p] + c[i] >= lim) pq.push((Node){p, c[p]}), p--;
		if(!pq.empty()){
			Node now = pq.top(); pq.pop();
			if(now.id == i){
				if(pq.empty()) continue;
				now = pq.top(); pq.pop();
			}
			vis[i] = vis[now.id] = 1, res++;
		}	
	}
	int mx = 0;
	for(int i = 0; i < (int) c.size(); i++) if(!vis[i]) mx = c[i];
	while(!pq.empty()) pq.pop();
	return mx;
}
inline int check(int mid){
	lim = mid, res = 0, dfs(1, 0); return res >= m;
}
int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	read(n), read(m);
	for(int i = 1, x, y, z; i < n; i++){
		read(x), read(y), read(z);
		add(x, y, z), add(y, x, z);
	}
	int l = 0, r = n * 10000, ans = 0;
	while(l <= r){
		int mid = (l + r) >> 1;
		if(check(mid)) l = mid + 1, ans = mid;
		else r = mid - 1;
	}
	cout << ans << endl;
	return 0;
}	
