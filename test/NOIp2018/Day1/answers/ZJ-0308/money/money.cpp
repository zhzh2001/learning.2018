/*program by mangoyang*/
#include<bits/stdc++.h>
#define inf (0x7f7f7f7f)
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
typedef long long ll;
using namespace std;
template <class T>
inline void read(T &x){
	int ch = 0, f = 0; x = 0;
	for(; !isdigit(ch); ch = getchar()) if(ch == '-') f = 1;
	for(; isdigit(ch); ch = getchar()) x = x * 10 + ch - 48;
	if(f) x = -x;
}
const int N = 100005, lim = 25000;
set<int> s;
queue<int> q;
int dis[N], a[N], c[N], vis[N], inq[N], cx[N], tot, n;
inline void solve(){
	read(n);
	for(int i = 1; i <= n; i++) read(a[i]);
	sort(a + 1, a + n + 1);
	memset(vis, 0, sizeof(vis));
	memset(cx, 0, sizeof(cx));
	tot = 0;
	for(int i = 1; i <= n; i++) if(!vis[a[i]]){
		c[++tot] = a[i];
		for(int j = a[i]; j <= lim; j += a[i]) vis[j] = 1;
	}
	for(int i = 1; i < c[1]; i++) dis[i] = inf / 3;
	dis[0] = 0, inq[0] = 1, q.push(0);
	while(!q.empty()){
		int u = q.front(); q.pop();
		for(int i = 1; i <= tot; i++){
			int v = (u + c[i]) % c[1];
			if(dis[u] + c[i] < dis[v]){
				dis[v] = dis[u] + c[i], cx[v] = i;
				if(!inq[v]) inq[v] = 1, q.push(v);
			}
			else if(dis[u] + c[i] == dis[v]){
				if(c[i] != cx[v]) cx[v] = 0;
			}
		}
		inq[u] = 0;
	}
	s.clear(); s.insert(1);
	for(int i = 1; i < c[1]; i++) if(cx[i]) s.insert(cx[i]);
	printf("%d\n", s.size());
}
int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T; read(T); while(T--) solve();
	return 0;
}
