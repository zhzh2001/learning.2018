/*program by mangoyang*/
#include<bits/stdc++.h>
#define inf (0x7f7f7f7f)
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
typedef long long ll;
using namespace std;
template <class T>
inline void read(T &x){
	int ch = 0, f = 0; x = 0;
	for(; !isdigit(ch); ch = getchar()) if(ch == '-') f = 1;
	for(; isdigit(ch); ch = getchar()) x = x * 10 + ch - 48;
	if(f) x = -x;
}
const int N = 100005, lim = 25000;
set<int> s;
queue<int> q;
int dis[N], a[N], c[N], vis[N], has[N], rel[N], ans, inq[N], cx[N], tot, n;
namespace spfa{
	inline void gao(int tot){
		if(has[1]) return;
		for(int i = 1; i < c[1]; i++) dis[i] = inf / 3;
		dis[0] = 0, inq[0] = 1, q.push(0);
		while(!q.empty()){
			int u = q.front(); q.pop();
			for(int i = 1; i <= n; i++) if(!has[i]){
				int v = (u + c[i]) % c[1];
				if(dis[u] + c[i] < dis[v]){
					dis[v] = dis[u] + c[i], cx[v] = i;
					if(!inq[v]) inq[v] = 1, q.push(v);
				}
			}
			inq[u] = 0;
		}
		if(tot == n) for(int i = 1; i < c[1]; i++) rel[i] = dis[i];
		else for(int i = 1; i < c[1]; i++) if(dis[i] != rel[i]) return;
		ans = min(ans, tot);
	}
}
inline void dfs(int x, int tot){
	if(x == n + 1) return (void)(spfa::gao(tot));
	has[x] = 1, dfs(x + 1, tot);
	has[x] = 0, dfs(x + 1, tot + 1);
}
inline void solve(){
	read(n);
	for(int i = 1; i <= n; i++) read(c[i]); ans = inf;
	sort(c + 1, c + n + 1);
	spfa::gao(n), dfs(1, 0);
	printf("%d\n", ans);
}
int main(){
	//freopen("1.in", "r", stdin);
	//freopen("money.in", "r", stdin);
	//freopen("money.out", "w", stdout);
	int T; read(T); while(T--) solve();
	return 0;
}
