#include<bits/stdc++.h>

using std::cin;
using std::cout;
using std::endl;

const int MAXN=50005;

struct Edge
{
	int dis;
	int to;
	int next;
}e[2*MAXN];

struct Node
{
	int num;
	int val;
	bool operator <(const Node& a) const {return val<a.val;}
	bool operator >(const Node& a) const {return val>=a.val;}
};

int head[MAXN],v[MAXN],dist[MAXN],ans[MAXN];

bool cmp(int a,int b)
{
	return a>b;
}

int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	int n,m;
	memset(head,-1,sizeof(head));
	cin>>n>>m;
	int edge_num=1;
	int count=0;
	for(int i=1;i<n;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		e[edge_num].dis=z;
		e[edge_num].to=y;
		e[edge_num].next=head[x];
		head[x]=edge_num;
		edge_num++;
		e[edge_num].dis=z;
		e[edge_num].to=x;
		e[edge_num].next=head[y];
		head[y]=edge_num;
		edge_num++;
	}
	for(int i=1;i<=n;i++)
	{
		memset(v,0,sizeof(v));
		memset(dist,-1,sizeof(v));
		std::priority_queue<Node,std::vector<Node>,std::greater<Node> >q;
		Node a;
		a.num=i;
		dist[i]=0;
		a.val=dist[i];
		q.push(a);
		while(!q.empty())
		{
			Node b;
			b=q.top();
			q.pop();
			if(v[b.num])
				continue;
			v[b.num]=1;
			int now=head[b.num];
			while(now!=-1)
			{
				if(dist[e[now].to]==-1||dist[e[now].to]>dist[b.num]+e[now].dis)
				{
					dist[e[now].to]=dist[b.num]+e[now].dis;
					Node tmp;
					tmp.num=e[now].to;
					tmp.val=dist[e[now].to];
					q.push(tmp);
				}
				now=e[now].next;
			}
		}
		int max=0;
		for(int j=1;j<=n;j++)
			if(max<dist[j])
				max=dist[j];
		ans[count]=max;
		count++;
	}
	std::sort(ans,ans+count,cmp);
	cout<<ans[m-1];
	return 0;
}
