#include<bits/stdc++.h>

using std::cin;
using std::cout;
using std::endl;

int a[100005],count=0,n;

void find(int l,int r)
{
	if(l>=r)
	{
		if(l==r)
			count+=a[l];
		return;
	}
	int min=l;
	for(int i=l;i<=r;i++)
		if(a[min]>a[i])
			min=i;
	count+=a[min];
	int tag=a[min];
	if(a[min]==0)
	{
		for(int i=l;i<=r;i++)
			a[i]-=tag;
		find(l,min-1);
		find(min+1,r);
	}
	else
	{
		for(int i=l;i<=r;i++)
			a[i]-=tag;
		find(l,r);
	}
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	cin>>n;
	for(int i=0;i<n;i++)
		scanf("%d",&a[i]);
	int l,r;
	l=0;r=n-1;
	find(l,r);
	cout<<count;
	return 0;
}
