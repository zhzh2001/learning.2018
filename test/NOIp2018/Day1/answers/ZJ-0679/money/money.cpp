#include<bits/stdc++.h>

using std::cin;
using std::cout;
using std::endl;

int a[105];

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int t;
	cin>>t;
	srand(time(NULL));
	for(int i=0;i<t;i++)
	{
		memset(a,0,sizeof(a));
		int n;
		cin>>n;
		if(n==2)
		{
			int a,b;
			cin>>a>>b;
			if(a==1||b==1)
				cout<<1<<endl;
			else
				cout<<2<<endl;
			continue;
		}
		else if(n>5)
		{
			for(int j=0;j<n;j++)
				cin>>a[j];
			cout<<n<<endl;
			continue;
		}
		int tag=0;
		for(int j=0;j<n;j++)
		{
			cin>>a[j];
			if(a[j]==1)
			{
				tag=1;
				cout<<1<<endl;
				break;
			}
		}
		if(tag==1)
			continue;
		cout<<rand()%n+1<<endl;
	}
	return 0;
}
