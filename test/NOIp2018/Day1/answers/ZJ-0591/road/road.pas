program road;
 uses math;
 var
  a:array[0..200001] of longint;
  i,n,smax,ssum:longint;
 begin
  assign(input,'road.in');
  assign(output,'road.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
   read(a[i]);
  readln;
  for i:=1 to n do
   a[n+i]:=a[i];
  a[0]:=0;
  smax:=0;
  ssum:=0;
  for i:=1 to n do
   if a[i]>a[i-1] then inc(ssum,a[i]-a[i-1]);
  writeln(ssum);
  smax:=ssum;
  for i:=1 to n-1 do
   begin
    if a[i+1]<a[i] then inc(ssum,a[i+1]-a[i]);
    if a[i+n]>a[i+n-1] then inc(ssum,a[i+n]-a[i+n-1]);
    smax:=min(smax,ssum);
   end;
  //writeln(ssum);
  close(input);
  close(output);
 end.
