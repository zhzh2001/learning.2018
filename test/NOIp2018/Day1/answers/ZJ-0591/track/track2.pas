program track;
 type
  wwb=^wb;
  wb=record
   n:wwb;
   t,v:longint;
  end;
 var
  b:array[0..100001] of wb;
  a,d,f,h,DFS,DFN:array[0..50001] of longint;
  i,m,n,o,p,x,y,z:longint;
  DFSs,lmax,mc:longint;
 procedure hahainc(x,y,z:longint);
  begin
   inc(p);
   b[p].n:=@b[h[x]];
   b[p].t:=y;
   b[p].v:=z;
   h[x]:=p;
  end;
 procedure hahagdb(l,r:longint);
  var
   c:array[0..50001] of longint;
   i,j,o,p,m,t:longint;
  begin
   m:=(l+r)>>1;
   if l>=r then exit;
   hahagdb(l,m);
   hahagdb(m+1,r);
   o:=l;
   p:=m+1;
   for i:=l to r do
    if (a[o]<=a[p]) and (o<=m) or (p=r+1) then
     begin
      c[i]:=a[o];
      inc(o);
     end
                               else
     begin
      c[i]:=a[p];
      inc(p);
     end;
   for i:=l to r do
    a[i]:=c[i];
  end;
 procedure hahaDFS(k,x:longint);
  var
   i:longint;
   j:wwb;
  begin
   //exit;
   inc(DFSs);
   DFS[k]:=DFSs;
   DFN[k]:=DFSs;
   j:=@b[h[k]];
   while j<>@b[0] do
    begin
     i:=j^.t;
     j:=j^.n;
     if i=x then continue;
     hahaDFS(i,k);
     DFN[k]:=DFS[i];
    end;
  end;
 function hahathree_force_icecream(k,x:longint):longint;
  var
   i,kc,l,o,p:longint;
   j:wwb;
  begin
   hahathree_force_icecream:=0;
   l:=0;
   j:=@b[h[k]];
   while j<>@b[0] do
    begin
     i:=j^.t;
     kc:=j^.v;
     j:=j^.n;
     if i=x then continue;
     inc(hahathree_force_icecream,hahathree_force_icecream(i,k));
     inc(a[DFS[i]],kc);
     //if a[i]>=mc then inc(hahathree_force_icecream)
                 //else
     //begin
      //inc(l);
      //d[l]:=a[i];
      //inc(l);
      //d[l]:=a[i];
     //end;
    end;
   //j:=@b[h[k]];
   //while j<>@b[0] do
    //begin
     //i:=j^.t;
     //j:=j^.n;
     //if i=x then continue;
    //end;
   hahagdb(DFS[k]+1,DFN[k]);
   {for i:=1 to l do
    t[i]:=i;
   for i:=1 to l do
    if t[i]=i then
     begin
      k:=mc-d[i];}
   a[DFS[k]]:=0;
   l:=DFN[k];
   while (a[l]>=mc) and (l>=DFS[k]) do
    begin
     inc(hahathree_force_icecream);
     dec(l);
    end;
   o:=1;
   p:=0;
   for i:=DFS[k]+1 to l do
    begin
     //if i=l then //a[k]:=d[i];
      //begin
       //inc(p);
       //f[p]:=d[i];
       //break;
      //end;
     if i>l then break;
     while (a[l]+a[i]>=mc) and (l>i) do
      begin
       inc(p);
       f[p]:=a[l];
       dec(l);
      end;
     if p>0 then//d[i]+d[l]>=mc then
      begin
       inc(hahathree_force_icecream);
       dec(p);
      end
                      else a[DFS[k]]:=a[i];
    end;
   inc(hahathree_force_icecream,p>>1);
   if p and 1=1 then a[DFS[k]]:=f[1];
  end;
 begin
  {assign(input,'track.in');
  assign(output,'track.out');
  reset(input);
  rewrite(output);}
  filldword(h,sizeof(h)>>2,0);
  p:=0;
  lmax:=0;
  readln(n,m);
  for i:=1 to n-1 do
   begin
    readln(x,y,z);
    hahainc(x,y,z);
    hahainc(y,x,z);
    inc(lmax,z);
   end;
  DFSs:=0;
  hahaDFS(n>>1+1,0);
  o:=1;
  p:=lmax;
  while o<=p do
   begin
    mc:=(o+p)>>1;
    //mc:=17;
    if hahathree_force_icecream(n>>1+1,0)>=m then o:=mc+1
                                             else p:=mc-1;
   end;
  writeln(p);
  close(input);
  close(output);
 end.
