#include<iostream>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iomanip>
#include<algorithm>
#include<cmath>
#define inf 0x3f3f3f3f
using namespace std;
const int maxn=105;
int T,n,ans;
int a[maxn];
int main()
{
  freopen("money.in","r",stdin);
  freopen("money.out","w",stdout);
  int i,x;
  scanf("%d",&T);
  while(T--)
  {
  	scanf("%d",&n);
  	for(i=1;i<=n;i++)
  	  scanf("%d",&a[i]);
  	if(n==2)
  	{
  	  if(a[1]>a[2]) swap(a[1],a[2]);
	  if(a[2]%a[1]==0) ans=1;
	  else ans=2;	
	}
	else if(n==3)
	{
	  ans=3;
	  bool flag=true;
	  sort(a+1,a+n+1);
	  for(i=2;i<=3;i++)
	  	if(a[i]%a[1]!=0) flag=false;
	  if(flag) ans=1;
	  else
	  {
	  	if(a[2]%a[1]==0 || a[3]%a[2]==0 || a[3]%a[1]==0) ans=2;
	  	else 
	  	{
	  	  int N=a[3]/a[1];
	  	  for(x=1;x<=N;x++)
	  	    if((a[3]-a[1]*x)%a[2]==0)
			{
			  ans=2;
			  break;  	
			}	
		}
	  }
	}
	else if(n==4)
	{
	  ans=4;
	  bool flag=true;
	  sort(a+1,a+n+1);
	  for(i=2;i<=4;i++)
	    if(a[i]%a[1]!=0) flag=false;
	if(flag) ans=1;
	else
	{
	  if((a[2]%a[1]==0 && a[3]%a[1]==0)||(a[2]%a[1]==0 && a[4]%a[1]==0) ||(a[3]%a[1]==0 && a[4]%a[1]==0))
	    ans=2;
	  else
	  {
	  	 if(a[2]%a[1]==0)
	  	 {
	  	   ans=3;
	  	   int N=a[4]/a[1];
	  	   for(x=1;x<=N;x++)
	  	     if((a[4]-a[1]*x)%a[3]==0)
			 {
			   ans=2;
		 	   break;  	
			 }		
		 }
		 else
		 {
		   if(a[3]%a[1]==0)
		   {
		   	 ans=3;
		     int N=a[4]/a[1];
		     for(x=1;x<=N;x++)
		       if((a[4]-a[1]*x)%a[2]==0)
		       {
		       	ans=2;
		       	break;
			   }
		   }
		   else
		   {
		   	if(a[4]%a[1]==0)
		   	{
		   	   int N=a[3]/a[1];
	  	       for(x=1;x<=N;x++)
	  	       if((a[3]-a[1]*x)%a[2]==0)
			   {
			     ans=2;
			     break;  	
			   }
			}
		   }
		 }
	  }
	}
	}
	else ans=n;
	printf("%d\n",ans);
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}
