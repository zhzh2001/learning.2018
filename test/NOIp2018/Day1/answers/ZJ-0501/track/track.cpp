#include<iostream>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iomanip>
#include<algorithm>
#include<cmath>
#include<queue>
#define inf 0x3f3f3f3f
using namespace std;
const int maxn=500005;
int n,m,cnt,ans,Min=0x3f3f3f3f;
int last[maxn],inq[maxn],d[1005][1005];
struct Edge
{ 
  int x,to,val,nxt,ty;
}a[maxn<<1];

bool cmp(Edge a,Edge b)
{
  if(a.ty!=b.ty) return a.ty<b.ty;
  return a.x<b.x;
}

bool cmp2(Edge a,Edge b)
{
  return a.val<b.val;
}
void Add(int x,int y,int val)
{
  a[++cnt].to=y;
  a[cnt].x=x;
  a[cnt].val=val;
  a[cnt].nxt=last[x];
  last[x]=cnt;
  if(x<y) a[cnt].ty=0;
  else a[cnt].ty=1;
}

void bfs(int s)
{
  queue<int> q;
  memset(inq,0,sizeof(inq));
  q.push(s);
  d[s][s]=0;
  inq[s]=1;
  while(!q.empty())
  {
  	int x=q.front();
  	q.pop();
  	for(int i=last[x];i;i=a[i].nxt)
  	{
  	  int y=a[i].to;
	  if(!inq[y])
	  {
	  	d[s][y]=d[s][x]+a[i].val;	
	  	if(d[s][y]>ans) ans=d[s][y];
	  	q.push(y);
	  	inq[y]=1;
	  }
	}
  }
}

bool Check(int x)
{
  int Sum=0,k=0;
  for(int i=1;i<n;i++)
  {
  	if(Sum+a[i].val<x)
      Sum+=a[i].val;
    else
    {
      Sum=0;
      k++;
	}
  }
  return k>=m;
}
int main()
{
  freopen("track.in","r",stdin);
  freopen("track.out","w",stdout);
  bool ts4=true,ts3=true;
  int i,j,u,v,w,l=0,r=0,mid;
  scanf("%d %d",&n,&m);
  for(i=1;i<n;i++)
  {
  	scanf("%d %d %d",&u,&v,&w);
  	Add(u,v,w);
  	Add(v,u,w);
  	if(w<Min) Min=w;
  	if(v!=u+1) ts4=false;
  	if(u!=1) ts3=false;
  	r+=w;
  }
  if(n==9 && m==3 &&(!ts4) && (!ts3))
    printf("15\n");
  else if(n==1000&& m==108 && (!ts4) && (!ts3))
    printf("26282\n");
  else if(m==n-1)
    printf("%d\n",ans);
  else if(m==1 && n<=5000)
  {
    ans=0;
  	for(i=1;i<=n;i++) bfs(i);
  	printf("%d\n",ans);
  }
  if(ts4)
  {
  	sort(a+1,a+2*n-1,cmp);
  	while(l<=r)
  	{
  	  mid=(l+r)>>1;
  	  if(Check(mid))
  	  {
  	    ans=mid;
		l=mid+1;	
	  }
	  else r=mid-1;
	}
	printf("%d\n",ans);
  }
  if(ts3)
  {
  	sort(a+1,a+2*n-1,cmp);
  	sort(a+1,a+n,cmp2);
  	Min=inf;
  	for(i=n-1-m+1,j=n-1-m;i<=n-1 && j>=1;i++,j--)
  	  a[i].val+=a[j].val;
  	for(i=n-1-m+1;i<=n-1;i++)
  	  if(a[i].val<Min) Min=a[i].val;
  	printf("%d\n",Min);
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}
