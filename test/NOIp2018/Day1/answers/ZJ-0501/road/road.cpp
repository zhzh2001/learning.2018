#include<iostream>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iomanip>
#include<algorithm>
#include<cmath>
#define inf 0x3f3f3f3f
using namespace std;
const int maxn=100005;
int n,Min;
long long ans;
int a[maxn];
inline int read()
{
  int x=0,f=1;
  char c=getchar();
  while(c<'0' || c>'9')
  {
  	if(c=='-') f=-1;
  	c=getchar();
  }
  while(c>='0' && c<='9')
  {
  	x=(x<<1)+(x<<3)+c-'0';
  	c=getchar();
  }
  return x*f;
}
int main()
{
  freopen("road.in","r",stdin);
  freopen("road.out","w",stdout);
  int i,l=1,r;
  n=read();
  for(i=1;i<=n;++i) a[i]=read();
  while(l<=n)
  {
  	while(!a[l] && l<=n) l++;
  	Min=inf;
  	r=l;
  	while(a[r])
  	{
  	  if(a[r]<Min) Min=a[r];
	  r++;
	}
	for(i=l;i<r;++i)
	  a[i]-=Min;
	if(Min!=inf) ans+=Min;
  }
  printf("%lld\n",ans);
  fclose(stdin);
  fclose(stdout);
  return 0;
}
