#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<queue>
#include<set>
#include<map>
#include<stack>
#define ll long long
#define mp make_pair
#define pb push_back
using namespace std;
const int maxn=100000+200,inff=100000000;
typedef pair<int ,int> pii;

struct tree{
	int mini,tag;
}t[maxn<<3];

pii list[maxn];
int n,d[maxn],x,y,z,ans;

inline int ls(int o){	return o<<1;	}
inline int rs(int o){	return o<<1|1;	}

inline void pushdown(int o,int l,int r){
	
	
}

void build(int o,int l,int r){
	t[o].tag=0;
	t[o].mini=inff;
	if(l==r){
		t[o].mini=d[l];
		return ;
	}
	int m=(l+r)>>1;
	build(ls(o),l,m);
	build(rs(o),m+1,r);
	t[o].mini=min(t[ls(o)].mini,t[rs(o)].mini);
}

int query(int o,int l,int r,int gl,int gr){
	if(gl<=l&&r<=gr){
		return t[o].mini;
	}
	int res=inff,m=(l+r)>>1;
	if(m>=gl)res=min(query(ls(o),l,m,gl,gr),res);
	if(m<gr)res=min(query(rs(o),m+1,r,gl,gr),res);
	return res;
}

int lower(int k,int left){
	int l=1,r=n;
	while(l<r){
		int mid=(l+r)>>1;
		if(list[mid].first>k||(list[mid].first==k&&list[mid].second>=left))r=mid;
		else l=mid+1;
	}
	return l;
}

void dfs(int l,int r,int d){
	if(l>r)return ;
	int k=query(1,1,n,l,r),start=lower(k,l);
	ans+=k-d;
	while(list[start].second<l)start++;
	dfs(l,list[start].second-1,k);
	while(start<n&&list[start+1].first==k){
		if(list[start+1].second-list[start].second>1){
			if(list[start].second+1>l&&list[start+1].second-1<r)
				dfs(list[start].second+1,list[start+1].second-1,k);
			else break;
		}
			
		start++;
	}
	dfs(list[start].second+1,r,k);
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%d",&d[i]);
		list[i]=mp(d[i],i);
	}
	build(1,1,n);
	sort(list+1,list+1+n);
	dfs(1,n,0);
	cout<<ans<<endl;
	return 0;
}
