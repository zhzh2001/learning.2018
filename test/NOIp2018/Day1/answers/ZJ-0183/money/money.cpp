#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<queue>
#include<set>
#include<map>
#include<stack>
#define ll long long
#define mp make_pair
#define pb push_back
using namespace std;
const int maxn=100000+200,inff=100000000,tot=6000;
typedef pair<int ,int> pii;

int f[2][maxn],a[200],n,m,x,T;

void dp(int c,int k){
	for(int i=k;i<=tot;i++)f[c][i]|=f[c][i-k];
}

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		memset(f,0,sizeof(f));
		f[1][0]=f[0][0]=1;
		scanf("%d",&n);
		for(int i=0;i<n;i++){
			scanf("%d",&a[i]);
			dp(0,a[i]);
		}
		m=0;
		for(int i=0;i<=tot;i++) if(f[0][i]!=f[1][i]){
			dp(1,i);
			m++;
		}
		cout<<m<<endl;
	}
	return 0;
}

