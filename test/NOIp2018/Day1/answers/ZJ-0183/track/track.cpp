#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<queue>
#include<set>
#include<map>
#include<stack>
#define ll long long
#define mp make_pair
#define pb push_back
using namespace std;
const int maxn=100000+200,inff=100000000,tot=6000;
typedef pair<int ,int> pii;

vector<ll> g[maxn];
ll sum[maxn],v[maxn];
ll m,n,x,y,z,ans=0;

struct edge{
	ll from,to,dist;
	edge(int u,int v,int d):from(u),to(v),dist(d){}
};
vector<edge> edges;

inline ll max(ll a,ll b){	return a>b?a:b;		}

void add(int u,int v,int d){
	edges.pb(edge(u,v,d));
	g[u].pb(edges.size()-1);
	edges.pb(edge(v,u,d));
	g[u].pb(edges.size()-1);
}

int dfs(int u){
	
	v[u]=1;
	int max1=0,max2=0;
	for(int i=0;i<g[u].size();i++) {
		edge& e=edges[g[u][i]];int x=e.to;
		if(v[x])continue ;	
		int t=dfs(x)+e.dist;
		if(t>max1){
			max2=max1;
			max1=t;
		} else if(t>max2) max2=t;
	}
	ans=max(ans,max1+max2);
	return max1;
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	
		for(int i=1;i<n;i++){
			scanf("%lld%lld%lld",&x,&y,&z);
			add(x,y,z);
		}
		memset(v,0,sizeof(v));
		dfs(1);
		cout<<ans<<endl;
	
	return 0;
}
