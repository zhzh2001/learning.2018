#include <cstdio>
#include <algorithm>
using namespace std;
int ans,n,m,a[50005],b[50005],l[50005];
bool ff;
bool f[10];
int g[10][10];
inline int read()
{
	int x=0;char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-48,ch=getchar();
	return x;
}
void dfs(int x,int y)
{
	if (y>ans) ans=y;
	for (int i=1;i<=n;i++)
	if (g[x][i]>0&&f[i]==0) f[i]=1,dfs(i,y+g[x][i]),f[i]=0;
	return;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n-1;i++)
	{
		a[i]=read();
		if (a[i]!=1) ff=1;
		b[i]=read();l[i]=read();
	}
	if (m==n-1)
	{
		ans=l[1];
		for (int i=2;i<=n-1;i++)
		ans=min(l[i],ans);
		printf("%d",ans);
		return 0;
	}
	if (m==1&&n<=5)
	{
		ans=0;
		for (int i=1;i<=n-1;i++)
		g[a[i]][b[i]]=l[i],g[b[i]][a[i]]=l[i];
		for (int i=1;i<=n;i++)
		f[i]=1,dfs(i,0),f[i]=0;
		printf("%d",ans);
		return 0;
	}
	sort(l+1,l+n);
	if (ff==1)
	{
		printf("%d",l[n-1]);
		return 0;
	}
	ans=1<<29;
	while (m>(n-1)/2) ans=min(ans,l[n-1]),m--,n--;
	for (int i=1;i<=m;i++)
	ans=min(ans,l[n-1-i+1]+l[n-1-m-m+i]);
	printf("%d",ans);
}
