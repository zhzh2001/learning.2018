#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <string>
using namespace std;
int a[100005],ans,l,r,s,n;
bool f[100005];
struct node{
	int v,pos;
}b[100005];
inline bool cmp(node x,node y)
{
	if (x.v==y.v) return x.pos<y.pos;
	return x.v<y.v;
}
inline int read()
{
	int x=0;char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-48,ch=getchar();
	return x;
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<=n;i++) b[i].v=a[i],b[i].pos=i;
	sort(b+1,b+n+1,cmp);
	for (int i=1;i<=n;i++)
	if (f[b[i].pos]==0)
	{
		f[b[i].pos]=1;
		l=b[i].pos;
		r=l;
		s=l;
		while (l>0&&a[l-1]>=a[s])
		{
			l--;
			if (a[l]==a[s]) f[l]=1;
		}
		while (r<=n&&a[r+1]>=a[s])
		{
			r++;
			if (a[r]==a[s]) f[r]=1;
		}
		r++;
		l--;
		if (l==-1) l=0;
		if (r==n+2) r=n+1;
		ans+=a[s]-max(a[r],a[l]);
	}
	printf("%d",ans);
}
