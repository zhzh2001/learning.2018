#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
int a[105],T,ans,n,mmax;
bool f[25005];
inline int read()
{
	int x=0;char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-48,ch=getchar();
	return x;
}

void dfs(int x,int y,int z)
{
	if (y>1) f[x]=1;
	for (int i=z;i<=n-1;i++)
	if (a[i]+x<=mmax&&f[a[i]+x]==0) dfs(a[i]+x,y+1,i);
	return;
}

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while (T--)
	{
		n=read();
		mmax=0;
		for (int i=1;i<=n;i++)
		a[i]=read(),mmax=max(mmax,a[i]);
		for (int i=0;i<=mmax;i++)
		f[i]=0;
		sort(a+1,a+n+1);
		dfs(0,0,1);
		ans=0;
		for (int i=1;i<=n;i++)
		if (f[a[i]]==0) ans++;
		printf("%d\n",ans);
	}
}
