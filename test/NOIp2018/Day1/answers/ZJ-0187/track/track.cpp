#include<bits/stdc++.h>
using namespace std;
inline int read () {
	int num = 0; char c = ' '; bool flag = true;
	for (;c > '9' || c < '0'; c = getchar ())
		if (c == '-') flag = false;
	for (;c >= '0' && c <= '9'; num = (num << 1) + (num << 3) + c - 48, c = getchar ());
	return flag ? num : - num;
}
namespace Graph {
	const int maxn = 50020;
	struct node {
		int y, next, val;
	} a[maxn << 1];
	int head[maxn], top;
	void insert (int x, int y, int v) {
		a[top].y = y;
		a[top].val = v;
		a[top].next = head[x];
		head[x] = top ++;
	}
} using namespace Graph;
namespace INIT {
	int n, m, edge[maxn << 1];
	bool sub2flag = true, sub3flag = true;
	void init () {
		memset (head, -1, sizeof head);
		n = read (); m = read ();
		for (int i = 1;i < n;i ++) {
			int x = read ();
			int y = read ();
			int v = read ();
			if (x != 1) sub2flag = false;
			if (y != x + 1) sub3flag = false;
			insert (x, y, v);
			insert (y, x, v);
			edge[i] = v;
		}
	}
} using namespace INIT;
namespace BFS {
	int dis[maxn];
	bool vis[maxn];
	int q[maxn], l, r;
	void bfs (int s) {
		memset (vis, 0, sizeof vis);
		q[l = r = 1] = s;
		vis[s] = true; dis[s] = 0;
		while (l <= r) {
			int x = q[l ++];
			for (int i = head[x];i + 1;i = a[i].next) {
				int y = a[i].y;
				if (!vis[y]) {
					dis[y] = dis[x] + a[i].val;
					q[++ r] = y;
					vis[y] = true;
				}
			}
		}
	}
} using namespace BFS;
namespace sub1 {
	void work () {
		bfs (1);
		int x = 1;
		for (int i = 1;i <= n;i ++)
			if (dis[i] > dis[x]) x = i;
		bfs (x);
		x = 0;
		for (int i = 1;i <= n;i ++)
			x = max (x, dis[i]);
		printf ("%d\n", x);
	}
} using namespace sub1;
namespace sub2 {
	bool vis[maxn];
	int N;
	bool mycmp (int a, int b) {
		return a > b;
	}
	bool check (int mid) {
		int j = N;
		for (int i = 1;i <= N;i ++) {
			if (edge[i] + edge[j] < mid) continue;
			while (edge[i] + edge[j] >= mid && j >= 1) j --;
			
		}
	}
	void work () {
		if (m > n / 2) N = m << 1, sort (edge + 1, edge + 1 + N);
		else sort (edge + 1, edge + n), N = m << 1;
		int l = 0, r = 5e8;
		while (l < r) {
			int mid = (l + r) >> 1;
			if (check (mid)) l = mid;
				r = mid + 1;
		}
	}
} using namespace sub2;
namespace sub3 {
	bool check (int mid) {//能否做出m条赛道、 
		int sum = 0, ans = 1;
		for (int i = 1;i < n;i ++)
			if (sum >= mid) {
				ans ++; sum = edge[i];
			}
			else {
				sum += edge[i];
			}
		if (sum < mid) return false;
		return ans >= m;
	}
	void work () {
		int l = 0, r = 5e8;
		while (l < r) {
			int mid = (l + r + 1) >> 1;
			if (check (mid)) l = mid;
				else r = mid - 1;
		}
		printf ("%d\n", l);
	}
} using namespace sub3;
int main () {
	freopen ("track.in","r",stdin);
	freopen ("track.out","w",stdout);
	init ();
	if (m == 1) {
		sub1 :: work ();
		return 0;
	}
	if (sub2flag) {
		sub2 :: work ();
		return 0;
	}
	if (sub3flag) {
		sub3 :: work ();
		return 0;
	}
	return 0;
}
