#include<bits/stdc++.h>
using namespace std;
inline int read () {
	int num = 0; char c = ' '; bool flag = true;
	for (;c > '9' || c < '0'; c = getchar ())
		if (c == '-') flag = false;
	for (;c >= '0' && c <= '9'; num = (num << 1) + (num << 3) + c - 48, c = getchar ());
	return flag ? num : - num;
}
namespace INIT {
	const int maxn = 100020;
	int n, a[maxn];
	void init () {
		n = read ();
		for (int i = 1;i <= n;i ++)
			a[i] = read ();
	}
} using namespace INIT;
namespace force {
	int ans = 0;
	void work () {
		while (true) {
			int x = 0;
			for (int i = 1;i <= n;i ++)
				if (a[i] > a[x]) x = i;
			if (a[x] == 0) break;
			int l, r;
			for (l = x;l >= 1 && a[l] != 0;l --);
			for (r = x;r <= n && a[r] != 0;r ++);
			int delta = a[x];
			for (int i = l + 1;i < r;i ++)
				delta = min (a[i], delta);
			for (int i = l + 1;i < r;i ++)
				a[i] -= delta;
			ans += delta;
		}
		printf ("%d\n", ans);
	}
} using namespace force;
int main () {
	freopen ("road.in","r",stdin);
	freopen ("road.out","w",stdout);
	init ();
	work ();
	return 0;
}
