#include<bits/stdc++.h>
using namespace std;
inline int read () {
	int num = 0; char c = ' '; bool flag = true;
	for (;c > '9' || c < '0'; c = getchar ())
		if (c == '-') flag = false;
	for (;c >= '0' && c <= '9'; num = (num << 1) + (num << 3) + c - 48, c = getchar ());
	return flag ? num : - num;
}
namespace INIT {
	const int maxn = 120;
	int n, a[maxn];
	void init () {
		n = read ();
		for (int i = 1;i <= n;i ++)
			a[i] = read ();
		sort (a + 1, a + 1 + n);
	}
} using namespace INIT;
namespace WORK {
	const int Rong = 5020;
	bool f[Rong], g[Rong];
	void work () {
		int ans = 0;
		memset (f, 0, sizeof f);
		memset (g, 0, sizeof g);
		f[0] = g[0] = 1;
		for (int i = 1;i <= n;i ++) {
			for (int j = 0;j <= Rong - a[i];j ++)
				f[j + a[i]] |= f[j];
			bool flag = 0;
			for (int j = 0;j <= Rong;j ++)
				if (f[j] && !g[j]) {
					flag = 1;
					break;
				}
			if (flag) ans ++;
			if (flag)
				for (int j = 0;j <= Rong - a[i];j ++)
					g[j + a[i]] |= g[j];
		}
		printf ("%d\n", ans);
	}
} using namespace WORK;
int main () {
	freopen ("money.in","r",stdin);
	freopen ("money.out","w",stdout);
	int T = read ();
	while (T --) {
		init ();
		work ();
	}
	return 0;
}
