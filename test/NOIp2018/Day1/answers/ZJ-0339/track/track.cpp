#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <algorithm> 
#include <cstring> 
#include <cmath> 
using namespace std;

const int N = 5e4+100; 
int n,m; 

int to[N<<1],nxt[N<<1],last[N],w[N],len; 
void ins(int x,int y,int z) 
{
	to[++len]=y,nxt[len]=last[x],last[x]=len,w[len]=z; 
}

int dis[N],fa[N],dep[N],size[N]; 
void dfs(int x)
{
	size[x]=1; 
	for(int k=last[x];k;k=nxt[k]) 
	{
		int y=to[k]; 
		if(y!=fa[x]) 
		{
			dis[y]=dis[x]+w[k]; 
			fa[y]=x; 
			dep[y]=dep[x]+1; 
			dfs(y); 
			size[x]+=size[y]; 
		}
	}
}

int st[N][20]; 
void init() 
{
	for(int i=1;i<=n;++i) st[i][0]=fa[i]; 
	for(int i=1;i<=18;++i) 
	  for(int j=1;j<=n;++j) 
	    st[j][i]=st[st[j][i-1]][i-1]; 
}

int lca(int x,int y) 
{
	if(dep[x]>dep[y]) swap(x,y); 
	for(int i=18;i>=0;--i) 
	  if(dep[x]<=dep[st[y][i]]) y=st[y][i]; 
	if(x==y) return x; 
	for(int i=18;i>=0;--i) 
	  if(st[x][i]!=st[y][i]) 
	  {
	  	x=st[x][i]; 
	  	y=st[y][i]; 
	   }  
	return st[x][0]; 
}

bool check(int k) 
{
	int now = n,cnt=0; 
	while(cnt<=m)  
	{
		bool bj=0; 
		for(int i=0;i<=18;++i) 
		  if(dis[now]-dis[st[now][i]]>=k) 
		  {
		  	bj=1; 
		  	cnt++; 
		  	now=st[now][i]; 
		  	break; 
		  }
		if(!bj) break; 
	}
	return cnt>=m; 
}

int main()
{
	int x,y,z; 
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout); 
	bool sta1=1; 
	scanf("%d%d",&n,&m); 
	for(int i=1;i<n;++i) 
	{
		scanf("%d%d%d",&x,&y,&z); 
		ins(x,y,z),ins(y,x,z); 
		if(y!=x+1) sta1=0; 
	}
	if(m==1 && n<=5000) 
	{
		int ans=0;  
		fa[1]=1; 
		dep[1]=1; 
		dfs(1); 
		init(); 
		for(int i=1;i<=n;++i)  
		  for(int j=i+1;j<=n;++j) 
		    ans=max(ans,dis[i]+dis[j]-dis[lca(i,j)]); 
		printf("%d\n",ans); 
	} 
	else if(sta1) 
	{
		int ans; 
		dfs(1); 
		init(); 
		int l=1,r=0x3f3f3f3f; 
		while(l<=r) 
		{
			int mid = (l+r)>>1; 
			if(check(mid)) ans=mid,l=mid+1; 
			else r=mid-1; 
		} 
		printf("%d\n",ans); 
	}
	else 
	{
		if(m==1) 
		{
			int ans=0;  
			fa[1]=1; 
			dep[1]=1; 
			dfs(1); 
			init(); 
			for(int i=1;i<=n;++i)  
		 	 for(int j=i+1;j<=n;++j) 
		 	   	if(size[i]==0 && size[j]==0)  
		 	 	{
		  			if(i==j) continue; 
		  			ans=max(ans,dis[i]+dis[j]-dis[lca(i,j)]); 
		 	 	}
		printf("%d\n",ans); 
		} 
		else if(m==n-1) 
		{
			int ans=0; 
			for(int i=1;i<=len;++i) ans=max(ans,w[i]); 
			printf("%d\n",ans); 
		} 
		else 
		{
			int ans; 
			dfs(1); 
			init(); 
			int l=1,r=0x3f3f3f3f; 
			while(l<=r) 
			{
				int mid = (l+r)>>1; 
				if(check(mid)) ans=mid,l=mid+1; 
				else r=mid-1; 
			} 
			printf("%d\n",ans); 
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0; 
}

