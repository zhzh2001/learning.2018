#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <algorithm> 
#include <cstring> 
#include <cmath> 
using namespace std;

int n,a[110];  
bool vis[25100]; 

int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout); 
	int T; 
	scanf("%d",&T); 
	while(T--) 
	{
		int cnt=0; 
		memset(vis,0,sizeof(vis)); 
		scanf("%d",&n); 
		for(int i=1;i<=n;++i) scanf("%d",&a[i]); 
		sort(a+1,a+n+1); 
		for(int i=1;i<=n;++i) 
		{
			if(vis[a[i]]) continue;  
			for(int j=a[i];j<=25000;j+=a[i]) vis[j]=1; 
			for(int j=1;j<=25000,j+a[i]<=25000;++j) 
			  if(vis[j]) vis[j+a[i]]=1;  
			cnt++; 
		}  
		printf("%d\n",cnt); 
	}
	fclose(stdin);
	fclose(stdout);
	return 0; 
}

