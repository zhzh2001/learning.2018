#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <algorithm> 
#include <cstring> 
#include <cmath> 
using namespace std;

const int N = 1e5+100; 
int n,a[N],nxt[N],pre[N];  
int c[N]; 

int lowbit(int x) 
{
	return x&(-x); 
}

void updata(int x,int k) 
{
	if(x==0) x=1; 
	while(x<=10000) 
	{
		c[x]=k; 
		x+=lowbit(x); 
	} 
}

int Min(int a,int b)
{
	return a<b?a:b; 
}

int Max(int a,int b) 
{
	return a>b?a:b; 
}

int querymin(int x) 
{
	int res=n+1; 
	while(x) 
	{
		res=Min(c[x],res); 
		x-=lowbit(x); 
	 }  
	 return res; 
}

int querymax(int x) 
{
	int res=0; 
	while(x) 
	{
		res=Max(res,c[x]); 
		x-=lowbit(x); 
	} 
	return res; 
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout); 
	scanf("%d",&n); 
	for(int i=1;i<=n;++i) scanf("%d",&a[i]); 	
	for(int i=1;i<=10000;++i) c[i]=n+1; 
	for(int i=n;i>=1;--i) 
	{
		nxt[i]=querymin(a[i]); 
		updata(a[i],i); 
		if(a[nxt[i]]==a[i]) nxt[i]=nxt[nxt[i]]; 
	}	
	for(int i=1;i<=10000;++i) c[i]=0; 
	for(int i=1;i<=n;++i) 
	{
		pre[i]=querymax(a[i]); 
		updata(a[i],i); 
	}
	//for(int i=1;i<=n;++i) 
	  //cout<<pre[i]<<" "<<nxt[i]<<endl; 
	long long ans=0; 
	for(int i=1;i<=n;++i) 
	  ans+=Min(abs(a[pre[i]]-a[i]),abs(a[nxt[i]]-a[i])); 		

	printf("%lld\n",ans);  
	fclose(stdin);
	fclose(stdout);
	return 0; 
}

