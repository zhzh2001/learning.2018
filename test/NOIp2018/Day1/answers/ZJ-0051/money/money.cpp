#include<cstdio>
#include<algorithm>
using namespace std;
const int N=25050;
int n,w[N],dp[N];
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
		int V=0,ans=0;
		scanf("%d",&n);
		for(int i=1;i<=n;i++) scanf("%d",&w[i]),V=max(V,w[i]);
		sort(w+1,w+n+1);
		n=unique(w+1,w+n+1)-w-1;
		for(int i=0;i<=V;i++) dp[i]=0;
		dp[0]=1;
		for(int i=1;i<=n;i++)
		{
			if (dp[w[i]]) continue;
			ans++;
			for(int j=w[i];j<=V;j++) dp[j]|=dp[j-w[i]];
		}		
		printf("%d\n",ans);
	}
	fclose(stdin),fclose(stdout);
	return 0;
}
