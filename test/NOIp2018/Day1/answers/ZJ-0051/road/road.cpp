#include<cstdio>
#include<stack>
using namespace std;
const int N=1e5+50;
int L[N],R[N],w[N],n,rt,ans=0;
stack<int> S;
void dfs(int o,int pre)
{
	if (!o) return;
	ans+=w[o]-pre;
	dfs(L[o],w[o]);
	dfs(R[o],w[o]);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&w[i]);
		if (S.empty()||w[S.top()]<=w[i]) 
		{
			if (!S.empty()) R[S.top()]=i;
			S.push(i);
		}	
		else
		{
			while (!S.empty()&&w[S.top()]>w[i]) L[i]=S.top(),S.pop();
			if (!S.empty()) R[S.top()]=i;
			S.push(i);
		}
	}
	while (!S.empty()) rt=S.top(),S.pop();
	dfs(rt,0);
	printf("%d\n",ans);
	fclose(stdin),fclose(stdout);
	return 0;
}
