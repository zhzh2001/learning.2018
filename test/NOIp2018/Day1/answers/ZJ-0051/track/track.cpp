#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;
const int N=100050;
int n,m,dep[N],cnt=0,w[N],r[N],used[N];
int sz[N],rt=0,S=0;
vector<int> e[N],g[N];
void add(int u,int v,int val)
{
	e[u].push_back(v);
	g[u].push_back(val);
}
void dfs(int o,int fa,int K)
{
	for(int i=0;i<(int)e[o].size();i++)
	{
		int to=e[o][i],val=g[o][i];
		if (to==fa) continue;
		dep[to]=dep[o]+val;
		dfs(to,o,K);
	}
	int idx=0;
	for(int i=0;i<(int)e[o].size();i++)
	{
		int to=e[o][i],val=g[o][i];
		if (to==fa) continue;
		if (w[to]+val>=K){cnt++;continue;}
		r[++idx]=w[to]+val,used[idx]=0;
	}
	sort(r+1,r+idx+1);
	for(int L=1,R=idx;L<R;L++)
		if (r[L]+r[R]>=K) cnt++,used[L]=used[R--]=1;
	int mx=0;
	for(int i=1;i<=idx;i++)
		if (!used[i]) mx=max(mx,r[i]);
	w[o]=mx;
}
bool check(int K)
{
	cnt=0,dfs(rt,rt,K);
	return cnt>=m;
}
void find(int o,int fa,int &x)
{
	if (!x||dep[o]>dep[x]) x=o;
	sz[o]=1;
	int mx=0;
	for(int i=0;i<(int)e[o].size();i++)
	{
		int to=e[o][i],val=g[o][i];
		if (to==fa) continue;
		dep[to]=dep[o]+val;
		find(to,o,x);
		sz[o]+=sz[to];
		mx=max(sz[o],mx);
	}
	mx=max(n-sz[o],mx);
	if (!S||mx<S) S=mx,rt=o;
}
int calc()
{
	int st=0,ed=0;
	dep[1]=0,find(1,1,st);
	dep[st]=0,find(st,st,ed);
	return dep[ed];
}
namespace Force
{
	using namespace std;
	const int N=20,rt=1;
	struct edge{int u,v,val;}t[N];
	int pre[N],fa[N],d[N],dp[N][1<<N],w[1<<N],c[1<<N],cnt=0;
	vector<int> e[N],g[N];
	void add(int u,int v)
	{
		e[u].push_back(v);
	}
	void dfs(int o,int fa)
	{
		for(int i=0;i<(int)e[o].size();i++)
		{
			int to=e[o][i],val=g[o][i];
			if (to==fa) continue;
			d[to]=d[o]+1;
			dfs(to,o);
			pre[to]=o;
		}
	}
	void F(int o,int fa,int &flag)
	{
		if (e[o].size()>2) flag=0;
		for(int i=0;i<(int)e[o].size();i++)
		{
			int to=e[o][i];
			if (to==fa) continue;
			F(to,o,flag);
		}
	}
	int find(int x){return fa[x]==x?x:fa[x]=find(fa[x]);}
	void unite(int u,int v){fa[find(u)]=find(v);}
	int LCA(int u,int v)
	{
		if (d[u]<d[v]) swap(u,v);
		while (d[u]>d[v]) u=pre[u];
		while (u!=v) u=pre[u],v=pre[v];
		return u;
	}
	void solve()
	{
	//	freopen("track2.in","r",stdin);
		for(int i=1;i<n;i++)
		{
			int u,v,val;
			scanf("%d%d%d",&u,&v,&val);
			t[i]=(edge){u,v,val};
		}
		int K=n-1;
		dfs(rt,rt);
		for(int s=0;s<(1<<K);s++)
		{
			for(int i=1;i<=n;i++) e[i].clear();
			for(int i=1;i<=n;i++) fa[i]=i;
			int st=0,ed=0,flag=1,sum=0;
			for(int i=1;i<=K;i++)
				if ((s>>(i-1))&1) 
				{
					int u=t[i].u,v=t[i].v;
					if (!st||d[u]<d[st]) st=u;
					if (!st||d[v]<d[st]) st=v;
					if (!ed||d[u]>d[ed]) ed=u;
					if (!ed||d[v]>d[ed]) ed=v;
					unite(t[i].u,t[i].v);
					add(u,v),add(v,u);
				}
			for(int i=1;i<=K;i++)
				if ((s>>(i-1))&1) 
				{
					int u=t[i].u,v=t[i].v;
					if (find(u)!=find(st)) flag=0;
					if (find(v)!=find(st)) flag=0;
					sum+=t[i].val;
				}
			F(ed,ed,flag);
			if (!flag) continue;
	//		for(int i=1;i<=K;i++)
	//			if ((s>>(i-1))&1) 
	//			{
	//				int u=t[i].u,v=t[i].v;
	//				if (LCA(ed,u)!=st&&LCA(ed,u)!=u) flag=0;
	//				if (LCA(ed,v)!=st&&LCA(ed,v)!=v) flag=0;
	//			}
			if (!flag) continue;
			w[++cnt]=s,c[cnt]=sum;
		} 
		dp[0][0]=1<<30;
		for(int s=0;s<(1<<K);s++)
			for(int i=0;i<m;i++)
				for(int j=1;j<=cnt;j++)
					if (!(s&w[j])) dp[i+1][s|w[j]]=max(dp[i+1][s|w[j]],min(dp[i][s],c[j]));
		int ans=0;
		for(int s=0;s<(1<<K);s++)
			ans=max(ans,dp[m][s]);
		printf("%d",ans);			
	}
};
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m!=1&&n<=10)
	{
		Force::solve();
		fclose(stdin),fclose(stdout);
		return 0;
	}
	for(int i=1;i<n;i++)
	{
		int u,v,val;
		scanf("%d%d%d",&u,&v,&val);
		add(u,v,val),add(v,u,val);
	}
	int L=1,R=calc();
	if (m==1) 
	{
		printf("%d\n",R);
		fclose(stdin),fclose(stdout);
		return 0;
	}
	while (L<R)
	{
		int mid=(L+R+1)>>1;
		if (check(mid))
			L=mid;
		else
			R=mid-1;
	}
	printf("%d\n",R);
	fclose(stdin),fclose(stdout);
	return 0;
}
