#include<iostream>
#include<cstdio>
#include<algorithm>

int n,m;

struct edge{
	int to,nxt,v;
}E[2000000];
int H[1000000],tot;
void add_edge(int a,int b,int v){
	E[++tot]=(edge){b,H[a],v};H[a]=tot;
	E[++tot]=(edge){a,H[b],v};H[b]=tot;
}

int a,b,v;

int depst,depth;

void findfar(int now,int fa,int dis){
	if (dis>depth) depth=dis,depst=now;
	for (int i=H[now];i;i=E[i].nxt){
		if (E[i].to==fa) continue;
		findfar(E[i].to,now,dis+E[i].v);
	}
}

bool IsChain=true;

int Val[100000],Tot;
int deg[100000];

void GetDisIntoArr(int now,int fa){
	for (int i=H[now];i;i=E[i].nxt){
		if (E[i].to==fa) continue;
		Val[++Tot]=E[i].v;
		GetDisIntoArr(E[i].to,now);
	}
}

std::pair<int,int> DPonTree(int now,int fa,int goal){
	int cnt=0,A=0,B=0,C=0;
	for (int i=H[now];i;i=E[i].nxt){
		if (E[i].to==fa) continue;
		std::pair<int,int> Nxt=DPonTree(E[i].to,now,goal);
		C=Nxt.second+E[i].v;
		if (!A) A=C;
		else if (!B) B=C;
		cnt+=Nxt.first;
	}
	if (A>=goal) cnt++,A=0;
	if (B>=goal) cnt++,B=0;
	if (A+B>=goal){
		cnt++;
		A=B=0;
	}
	if (B>A) std::swap(A,B);
	return std::make_pair(cnt,A);
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&a,&b,&v);
		add_edge(a,b,v);
		IsChain&=(b==a+1);
		deg[a]++;
		deg[b]++;
	}
	if (m==1){
		findfar(1,0,0);
		depth=0;
		findfar(depst,0,0);
		printf("%d\n",depth);
	}
	else if (IsChain){
		GetDisIntoArr(1,0);
		int L=1,R=500000000;
		while (L<R){
			int mdl=(L+R+1)>>1;
			int cnt=0,sum=0;
			for (int i=1;i<n;i++){
				sum+=Val[i];
				if (sum>=mdl) cnt++,sum=0;
			}
			if (cnt>=m) L=mdl;
			else R=mdl-1;
		}
		printf("%d\n",L);
	}
	else{
		int root;
		for (int i=1;i<=n;i++) if (deg[i]<3) root=i;
		int L=1,R=500000000;
		while (L<R){
			int mdl=(L+R+1)>>1;
			if (DPonTree(root,0,mdl).first<m) R=mdl-1;
			else L=mdl;
		}
		printf("%d\n",L);
	}
}
