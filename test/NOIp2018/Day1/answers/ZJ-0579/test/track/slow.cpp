#include<iostream>
#include<cstdio>

int n,m;

struct edge{
	int to,nxt,v;
}E[10000];
int H[4000],tot;
void add_edge(int a,int b,int v){
	E[++tot]=(edge){b,H[a],v};H[a]=tot;
	E[++tot]=(edge){a,H[b],v};H[b]=tot;
}

int Fa[2000][15];
int dep[2000],dis[2000];

void Pre(int now,int fa){
	Fa[now][0]=fa;
	dep[now]=dep[fa]+1;
	for (int i=H[now];i;i=E[i].nxt){
		if (E[i].to==fa) continue;
		dis[E[i].to]=dis[now]+E[i].v;
		Pre(E[i].to,now);
	}
}

void init(){
	for (int i=1;i<15;i++)
		for (int j=1;j<=n;j++)
			Fa[j][i]=Fa[Fa[j][i-1]][i-1];
}

int LCA(int a,int b){
	if (dep[a]<dep[b]) std::swap(a,b);
	int dt=dep[a]-dep[b];
	for (int i=14;~i;i--) if (dt>=(1<<i)) a=Fa[a][i],dt-=(1<<i);
	if (a==b) return a;
	for (int i=14;~i;i--) if (Fa[a][i]!=Fa[b][i]) a=Fa[a][i],b=Fa[b][i];
	return Fa[a][0];
}

int ans;
int a,b,v;

int main(){
	freopen("track.in","r",stdin);
	scanf("%d%*d",&n);
	for (int i=1;i<n;i++){
		scanf("%d%d%d",&a,&b,&v);
		add_edge(a,b,v);
	}
	Pre(1,0),init();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++){
			int lca=LCA(i,j);
			int d=dis[i]+dis[j]-dis[lca]-dis[lca];
			ans=std::max(ans,d);
		}
	printf("%d\n",ans);
}
