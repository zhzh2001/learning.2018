#include<iostream>
#include<cstdio>
#include<set>
#include<cstring>

int T;
int n;
int A[200],ans;
std::set<int> U;
int met[30000];

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		U.clear();
		for (int i=1;i<=n;i++) scanf("%d",&A[i]),U.insert(A[i]);
		n=0;
		for (std::set<int>::iterator it=U.begin();it!=U.end();it++) A[++n]=*it;
		ans=n;
		memset(met,0,sizeof(met));
		met[0]=1;
		for (int i=0;i<=25000;i++){
			for (int j=1;j<=n;j++) if (A[j]==i&&met[i]>1) ans--;
			if (met[i]){
				for (int j=1;i+A[j]<=25000&&j<=n;j++)
					met[i+A[j]]++;
			}
		}
		printf("%d\n",ans);
	}
}
