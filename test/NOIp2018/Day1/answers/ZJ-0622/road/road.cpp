#include<iostream>
#include<cstdio>
#define maxn 100100
using namespace std;
int n,pre=0,ans=0,a[maxn];
void read(int &x){
	char ch=getchar();int sym;
	for(;(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if (ch=='-'){sym=-1;ch=getchar();}else sym=1;
	for(x=0;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-48;
	x*=sym;
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for (int i=1;i<=n;i++){read(a[i]);}
	for (int i=1;i<=n;i++){
		if (a[i]>pre){ans+=a[i]-pre;}
		pre=a[i];
	}
	printf("%d\n",ans);
	return 0;
}
