#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 150
#define maxM 25600
using namespace std;
int T,n,tot,x,TOP,ans,a[maxn],Flag[maxn],Money[maxM];
inline void read(int &x){
	char ch=getchar();int sym;
	for(;(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if (ch=='-'){sym=-1;ch=getchar();}else sym=1;
	for(x=0;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-48;
	x*=sym;
}
void Work(){
	read(n);
	TOP=0;tot=0;
	memset(Money,0,sizeof(Money));
	for (int i=1;i<=n;i++){
		read(x);
		if (Money[x]!=0) continue;
		a[++tot]=x;
		Flag[tot]=1;
		Money[x]=tot;
		TOP=max(TOP,x);
	}
	n=tot;
	for (int i=1;i<=n;i++){
		if (Flag[i]){
			for (int j=a[i]+1;j<=TOP;j++){
				if (Money[j-a[i]]!=0){
					if(Money[j]>0){Flag[Money[j]]=0;}
							  else{Money[j]=-1;}
				}
			}
		}
	}
	ans=0;
	for (int i=1;i<=n;i++){
		if (Flag[i]) ans++;
	}
	printf("%d\n",ans);
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	read(T);
	while (T--){
		Work();
	}
	return 0;
}
