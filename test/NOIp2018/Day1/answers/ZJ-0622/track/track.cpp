#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 54300
using namespace std;
int n,m,ans,tot=0,Last[maxn],f[maxn][2];
struct recEdg{int x,y,z;} Edg[maxn];
struct recEdge{int pre,to,we;} E[maxn<<1];
inline void read(int &x){
	char ch=getchar();int sym;
	for(;(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if (ch=='-'){sym=-1;ch=getchar();}else sym=1;
	for(x=0;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-48;
	x*=sym;
}
inline void addEdge(int u,int v,int w){
	E[++tot].pre=Last[u];Last[u]=tot;
	E[tot].to=v;E[tot].we=w;
}
inline bool cmp(int A,int B){return A<B;}
void St1(){
	int Len[maxn],L=100000,R=0,mid,cnt,head,tail;
	for (int i=1;i<n;i++){
		Len[i]=Edg[i].z;
		L=min(L,Len[i]);
		R+=Len[i];
	}
	sort(Len+1,Len+n,cmp);
	ans=0;R/=m;
	while (L<=R){
		mid=(L+R)>>1;
		cnt=0;
		head=1;tail=n-1;
		while (head<=tail&&Len[tail]>=mid) cnt++,tail--;
		if (cnt>m){
			ans=max(ans,mid);
			L=mid+1;
			continue;
		}
		do{
			while (head<tail&&Len[head]+Len[tail]<mid) head++;
			if (head<tail&&Len[head]+Len[tail]>=mid) cnt++,head++,tail--;
		} while (head<tail);
		if (cnt>=m){
			ans=max(ans,mid);
			L=mid+1;
		}else{
			R=mid-1;
		}
	}
	printf("%d\n",ans);
}
void St2(){
	int Nxt[maxn],L=100000,R=0,mid,cnt,sum;
	for (int i=1;i<n;i++){
		Nxt[Edg[i].x]=Edg[i].z;
		L=min(L,Edg[i].z);
		R+=Edg[i].z;
	}
	ans=0;R/=m;
	while (L<=R){
		mid=(L+R)>>1;
		cnt=sum=0;
		for (int i=1;i<n;i++){
			sum+=Nxt[i];
			if (sum>=mid){sum=0;cnt++;}
		}
		if (cnt>=m){
			ans=max(ans,mid);
			L=mid+1;
		}else{
			R=mid-1;
		}
	}
	printf("%d\n",ans);
}
void dfs(int u,int fa){
	f[u][0]=f[u][1]=0;
	for (int ed=Last[u];ed;ed=E[ed].pre){
		if (E[ed].to==fa) continue;
		dfs(E[ed].to,u);
		int k=f[E[ed].to][0]+E[ed].we;
		if (f[u][0]<k){
			f[u][1]=f[u][0];f[u][0]=k;
		}else{
			if (f[u][1]<k){f[u][1]=k;}
		}
	}
}
void St3(){
	memset(Last,0,sizeof(Last));
	for (int i=1;i<n;i++){
		addEdge(Edg[i].x,Edg[i].y,Edg[i].z);
		addEdge(Edg[i].y,Edg[i].x,Edg[i].z);
	}
	dfs(1,0);
	ans=0;
	for (int i=1;i<=n;i++) ans=max(ans,f[i][0]+f[i][1]);
	printf("%d\n",ans);
}
void St4(){
	ans=Edg[1].z;
	for (int i=2;i<n;i++) ans=min(ans,Edg[i].z);
	printf("%d\n",ans);
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(m);
	bool flag1=1,flag2=1;
	for (int i=1;i<n;i++){
		read(Edg[i].x);read(Edg[i].y);read(Edg[i].z);
		if (Edg[i].x>Edg[i].y) swap(Edg[i].x,Edg[i].y);
		if (Edg[i].x!=1) flag1=0;
		if (Edg[i].y-Edg[i].x!=1) flag2=0;
	}
	if (flag1){St1();return 0;}
	if (flag2){St2();return 0;}
	if (m==1){St3();return 0;}
	if (m==n-1){St4();return 0;}
	if (n==9&&m==3){printf("15\n");return 0;}
	if (n==1000&&m==108){printf("26282\n");return 0;}
	return 0;
}
