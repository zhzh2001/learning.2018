#include<bits/stdc++.h>
#define N 50005
using namespace std;
vector<int>P[N],C[N];
int n,m;
struct p20{
	int ans,id;
	void dfs(int p,int lastt,int Cost){
		if(Cost>ans){
			id=p;
			ans=Cost;
		}
		for(int i=0;i<P[p].size();i++){
			int y=P[p][i];
			if(y==lastt)continue;
			dfs(y,p,Cost+C[p][i]);
		}
	}
	void solve(){
		dfs(1,1,0);
		dfs(id,id,0);
		printf("%d\n",ans);
	}
}P20;
struct anotherp20{
	int R[N];
	void dfs(int p,int lastt){
		for(int i=0;i<P[p].size();i++){
			int y=P[p][i];
			if(y==lastt)continue;
			R[p]=C[p][i];
			dfs(y,p);
		}
	}
	bool ck(int x){
		int cnt=0,sum=0;
		for(int i=1;i<n;i++){
			sum+=R[i];
			if(sum>=x)cnt++,sum=0;
		}
		if(cnt>=m)return 1;
		else return 0;
	}
	void solve(){
		dfs(1,1);
		int l=0,r=555555555,res;
		while(l<=r){
			int mid=(l+r)>>1;
			if(ck(mid))l=mid+1,res=mid;
			else r=mid-1;
		}
		printf("%d\n",res);
	}
}lP20;
struct p15{
	int E[N];
	bool ck(int x){
		int j=1;
		int cnt=0,sum=0;
		for(int i=n-1;i>=1;i--){
			if(E[i]>=x)cnt++;
			else {
				if(i>j){
					while(E[i]+E[j]<x&&i>j)j++;
					if(i==j)break;
					cnt++,j++;
				}
				else break;
			}
		}
		if(cnt>=m)return 1;
		else return 0;
	}
	void solve(){
		for(int i=0;i<P[1].size();i++){
			E[i+1]=C[1][i];
		}
		sort(E+1,E+n);
		int l=0,r=555555555,res;
		while(l<=r){
			int mid=(l+r)>>1;
			if(ck(mid))l=mid+1,res=mid;
			else r=mid-1;
		}
		printf("%d\n",res);
	}
}P15;
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d %d",&n,&m);
	int i;
	bool f=1;
	bool f2=1;
	for(i=1;i<n;i++){
		int a,b,c;
		scanf("%d %d %d",&a,&b,&c);
		if(b!=a+1)f=0;
		if(a!=1)f2=0;
		P[a].push_back(b);
		C[a].push_back(c);
		P[b].push_back(a);
		C[b].push_back(c);
	}
	if(m==1)P20.solve();
	if(f==1)lP20.solve();
	if(f2==1)P15.solve();
	return 0;
}
