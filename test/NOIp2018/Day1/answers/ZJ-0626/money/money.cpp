#include<bits/stdc++.h>
using namespace std;
int vis[25005];
int A[105];
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	cin>>T;
	while(T--) {
		int n,i;
		scanf("%d",&n);
		memset(vis,0,sizeof(vis));
		for(i=1; i<=n; i++) {
			scanf("%d",&A[i]);
		}
		sort(A+1,A+n+1);
		int upp=A[n];
		int cnt=0;
		vis[0]=1;
		for(i=1; i<=n; i++) {
			if(vis[A[i]])continue;
			cnt++;
			for(int j=0; j<=upp; j++) {
				if(j+A[i]>upp)break;
				if(vis[j])vis[j+A[i]]=1;
			}
		}
		printf("%d\n",cnt);
	}
	return 0;
}
