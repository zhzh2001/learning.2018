#include <cstring>
#include <cstdio>
#include <algorithm>
#include <queue>
#include <set>
using namespace std;

typedef long long li;

namespace io {
  const int SIZE = 2e6;
  char buff[SIZE], *l, *r;
  char getch(void) {
    if (l == r) r = (l = buff) + fread(buff, 1, SIZE, stdin);
    return l == r ? EOF : *l++;
  }
  template<typename T> void read(T &x) {
    x = 0;
    T tmp = 1;
    char c = getch();
    while ((c < '0' || c > '9') && c != '-') c = getch();
    if (c == '-') tmp = -1, c = getch();
    while (c >= '0' && c <= '9') x = x * 10 + c - '0', c = getch();
    x *= tmp;
  }
}
using io::read;

const int maxn = 50010;
const int inf = 1e9;
int n, m, d[maxn];
pair< pair<int, int>, int > roads[maxn];

struct edge {
  int to, len, nxt;
} e[maxn << 1];
int head[maxn], m0 = 0;
inline void addedge(int x, int y, int len) {
  e[++m0] = (edge) {y, len, head[x]};
  head[x] = m0;
}

namespace solver1 {
  // only one track
  int dis[maxn];
  int Getdis(int Ax) {
    static queue<int> q;
    for (int i = 1; i <= n; ++i) dis[i] = inf;
    
    dis[Ax] = 0;
    q.push(Ax);
    while (!q.empty()) {
      int x = q.front();
      q.pop();
      for (int i = head[x]; i; i = e[i].nxt) {
	int y = e[i].to;
	if (dis[y] > dis[x] + e[i].len) {
	  dis[y] = dis[x] + e[i].len;
	  q.push(y);
	}
      }
    }
    
    int mx = 1;
    for (int i = 2; i <= n; ++i)
      if (dis[i] > dis[mx]) mx = i;
    return mx;
  }
  void main(void) {
    int u = Getdis(1);
    int v = Getdis(u);
    printf("%d\n", dis[v]);
  }
}

namespace solver2 {
  // chain
  int val[maxn];
  
  bool Check(void) {
    for (int i = 1; i < n; ++i) {
      int x = roads[i].first.first;
      int y = roads[i].first.second;
      if (y != x + 1) return 0;
    }
    return 1;
  }

  bool Solve(int lim) {
    int now = 0;
    int cnt = m;
    for (int i = 1; i < n; ++i) {
      now += val[i];
      if (now >= lim) --cnt, now = 0;
    }
    return cnt <= 0;
  }
  void main(void) {
    for (int i = 1; i < n; ++i)
      val[i] = roads[i].second;
    int l = 0, r = (int)1e9, ans = l;
    while (l <= r) {
      int mid = (l + r) >> 1;
      if (Solve(mid)) ans = mid, l = mid + 1;
      else r = mid - 1;
    }
    printf("%d\n", ans);
  }
}

namespace solver3 {
  // d <= 3
  bool Check(void) {
    for (int i = 1; i <= n; ++i)
      if (d[i] > 3) return 0;
    return 1;
  }

  int rt, lc[maxn], rc[maxn], pathl[maxn], pathr[maxn];
  int f[maxn], cnt, lim;

  void Dfs(int x, int pre) {
    for (int i = head[x]; i; i = e[i].nxt) {
      int y = e[i].to;
      if (y == pre) continue;
      if (lc[x] == -1) {
	lc[x] = y;
	pathl[x] = e[i].len;
      } else {
	rc[x] = y;
	pathr[x] = e[i].len;
      }
      Dfs(y, x);
    }
  }
  void Solve(int x, int pre) {
    if (~lc[x]) Solve(lc[x], x);
    if (~rc[x]) Solve(rc[x], x);

    if (~lc[x] && ~rc[x]) {
      int xl = f[lc[x]] + pathl[x];
      int xr = f[rc[x]] + pathr[x];
      if (xl >= lim && xr >= lim) {
	--cnt;
	f[x] = max(xl, xr);
      } else if (xl >= lim) {
	--cnt;
	f[x] = xr;
      } else if (xr >= lim) {
	--cnt;
	f[x] = xl;
      } else {
	f[x] = max(xl, xr);
      }
    } else if (~lc[x]){
      if (f[lc[x]] + pathl[x] >= lim) {
	--cnt;
	f[x] = 0;
      } else {
	f[x] = f[lc[x]] + pathl[x];
      }
    } else {
      f[x] = 0;
    }
  }
  void main(void) {
    for (int i = 1; i <= n; ++i)
      if (d[i] == 1) {
	rt = i;
	break;
      }

    for (int i = 1; i <= n; ++i)
      lc[i] = rc[i] = -1;
    Dfs(rt, 0);

    int l = 0, r = (int)1e9, ans = l;
    while (l <= r) {
      int mid = (l + r) >> 1;

      cnt = m;
      lim = mid;
      Solve(rt, 0);

      if (cnt <= 0) ans = mid, l = mid + 1;
      else r = mid - 1;
    }
    printf("%d\n", ans);
  }
}

namespace solver4 {
  // a = 1
  bool Check(void) {
    for (int i = 1; i < n; ++i)
      if (roads[i].first.first != 1) return 0;
    return 1;
  }

  multiset<int> val;
  bool Solve(int lim) {
    int cnt = m;
    val.clear();
    for (int i = 1; i < n; ++i) {
      int now = roads[i].second;
      if (now >= lim) --cnt;
      else val.insert(now);
    }
    while (val.begin() != val.end()) {
      multiset<int>::iterator r = val.end();
      --r;
      multiset<int>::iterator l = val.lower_bound(lim - (*r));
      if (l == val.end() || l == r) break;
      --cnt;
      val.erase(l);
      r = val.end(), --r;
      val.erase(r);
    }
    return cnt <= 0;
  }
  void main(void) {
    int l = 0, r = (int)1e9, ans = l;
    while (l <= r) {
      int mid = (l + r) >> 1;
      if (Solve(mid)) ans = mid, l = mid + 1;
      else r = mid - 1;
    }
    printf("%d\n", ans);
  }
}

int main(void) {

  freopen("track.in", "r", stdin);
  freopen("track.out", "w", stdout);
  
  read(n), read(m);
  for (int i = 1, x, y, z; i < n; ++i) {
    read(x), read(y), read(z);
    if (x > y) swap(x, y);
    roads[i] = make_pair(make_pair(x, y), z);
    ++d[x], ++d[y];
    addedge(x, y, z);
    addedge(y, x, z);
  }

  if (m == 1) {
    solver1::main();
    return 0;
  }
  if (solver2::Check()) {
    solver2::main();
    return 0;
  }
  if (solver3::Check()) {
    solver3::main();
    return 0;
  }
  if (solver4::Check()) {
    solver4::main();
    return 0;
  }

  return 0;
}
