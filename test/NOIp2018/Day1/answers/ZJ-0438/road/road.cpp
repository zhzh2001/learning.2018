#include <cstring>
#include <cstdio>
#include <algorithm>
using namespace std;

namespace io {
  const int SIZE = 2e6;
  char buff[SIZE], *l, *r;
  char getch(void) {
    if (l == r) r = (l = buff) + fread(buff, 1, SIZE, stdin);
    return l == r ? EOF : *l++;
  }
  template<typename T> void read(T &x) {
    x = 0;
    T tmp = 1;
    char c = getch();
    while ((c < '0' || c > '9') && c != '-') c = getch();
    if (c == '-') tmp = -1, c = getch();
    while (c >= '0' && c <= '9') x = x * 10 + c - '0', c = getch();
    x *= tmp;
  }
}
using io::read;

const int maxn = 1e5 + 10;
int n, a[maxn];

namespace solver1 {
  int val[maxn], cnt[maxn];
  void main(void) {
    a[0] = a[n + 1] = 0;
    for (int i = 1; i <= n; ++i) val[i] = a[i];
    val[n + 1] = 0;
    ++n;
    sort(val + 1, val + 1 + n);
    int m = unique(val + 1, val + 1 + n) - val - 1;
    int pre = 1;
    for (int i = 1; i <= n; ++i) {
      int pos = lower_bound(val + 1, val + 1 + m, a[i]) - val;
      if (pos < pre) {
	++cnt[pre], --cnt[pos];
      }
      pre = pos;
    }
    for (int i = m - 1; i; --i)
      cnt[i] += cnt[i + 1];
    int ans = 0;
    for (int i = 2; i <= m; ++i)
      ans += (val[i] - val[i - 1]) * cnt[i];
    printf("%d\n", ans);
  }
}

int main(void) {

  freopen("road.in", "r", stdin);
  freopen("road.out", "w", stdout);
  
  read(n);
  for (int i = 1; i <= n; ++i) read(a[i]);

  solver1::main();
  
  return 0;
}
