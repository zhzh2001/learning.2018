#include <cstring>
#include <cstdio>
#include <algorithm>
using namespace std;

namespace io {
  const int SIZE = 2e6;
  char buff[SIZE], *l, *r;
  char getch(void) {
    if (l == r) r = (l = buff) + fread(buff, 1, SIZE, stdin);
    return l == r ? EOF : *l++;
  }
  template<typename T> void read(T &x) {
    x = 0;
    T tmp = 1;
    char c = getch();
    while ((c < '0' || c > '9') && c != '-') c = getch();
    if (c == '-') tmp = -1, c = getch();
    while (c >= '0' && c <= '9') x = x * 10 + c - '0', c = getch();
    x *= tmp;
  }
}
using io::read;

const int maxn = 110;
const int maxval = 25000;
int n, a[maxn];

bool f[maxval + 10];
int val[maxn], m;

bool Check(int t) {
  m = 0;
  for (int i = 1; i <= n; ++i)
    if (i != t) val[++m] = a[i];
  memset(f, 0, sizeof f);
  f[0] = 1;
  for (int i = 1; i <= m; ++i)
    for (int j = val[i]; j <= a[t]; ++j)
      f[j] |= f[j - val[i]];
  return f[a[t]];
}

int cas;
int main(void) {

  freopen("money.in", "r", stdin);
  freopen("money.out", "w", stdout);
  
  read(cas);
  while (cas--) {

    read(n);
    for (int i = 1; i <= n; ++i) read(a[i]);
    sort(a + 1, a + 1 + n);

    if (n == 1 || a[1] == 1) {
      puts("1");
      continue;
    }

    int ans = 0;
    for (int i = 1; i <= n; ++i) {
      ans += Check(i);
    }
    printf("%d\n", n - ans);
    
  }

  return 0;
}
