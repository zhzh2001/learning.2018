#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
const int N=105;
int a[N];
bool vis[30000];
int n,m,k;
void read(int &x){
	char ch=getchar();x=0;int f=0;
	while (ch<'0'||ch>'9') f|=(ch=='-'),ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	if (f>0) x=-x;
}
void wwrite(int x){
	if (x>9) wwrite(x/10);
	putchar(x%10+'0');
}
inline void write(int x,bool cmp){
	if (x<0) putchar('-'),x=-x;
	wwrite(x);
	if (cmp) putchar(' '); else putchar('\n');
}
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;read(T);
	while (T--){
		memset(vis,0,sizeof(vis));
		read(n);
		for (int i=1;i<=n;++i) read(a[i]);
		sort(a+1,a+n+1);int Max=a[n];
		int ans=0;
		for (int i=1;i<=n;++i)
		if (!vis[a[i]]){
			++ans;
			for (int j=1;j<=Max;++j)
			if (vis[j]){
				for (int k=j;k<=Max;k+=a[i]) vis[k]=1;
			}
			for (int j=a[i];j<=Max;j+=a[i]) vis[j]=1;
		}
		write(ans,0);
	}
	return 0;
}
