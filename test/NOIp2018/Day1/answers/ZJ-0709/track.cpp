#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=100005;
struct EDGE{
	int u,v,va;
}E[N];
struct node {
	int a,b;
}ff[N];
inline int max(int a,int b){return (a>b)?a:b;}
inline int min(int a,int b){return (a<b)?a:b;}
int head[N],vet[N],val[N],Next[N],edge;
int n,m,k;
bool vis[N];
void read(int &x){
	char ch=getchar();x=0;int f=0;
	while (ch<'0'||ch>'9') f|=(ch=='-'),ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	if (f>0) x=-x;
}
inline void add(int u,int v,int va){
	Next[++edge]=head[u];
	head[u]=edge;
	vet[edge]=v;
	val[edge]=va;
}
void dfs1(int u,int father){
	for (int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if (v==father) continue;
		dfs1(v,u);
		if (ff[v].a+val[i]>ff[u].a) ff[u].b=ff[u].a,ff[u].a=ff[v].a+val[i];
			else if (ff[v].a+val[i]>ff[u].b) ff[u].b=ff[v].a+val[i];
	}
}
bool dfs(int kk,int now,int va,int gol){
	if (kk>k) return 1;
	if (va>=gol){
		for (int i=1;i<=n;++i)
			if (dfs(kk+1,i,0,gol)) return 1;
		return 0;
	}
	for (int i=head[now];i;i=Next[i]){
		if (!vis[i]){
			vis[i]=vis[i^1]=1;
			if (dfs(kk,vet[i],va+val[i],gol)) return 1;
			vis[i]=vis[i^1]=0;
		}
	}
	return 0;
}
bool check(int x){
	for (int i=1;i<=n;++i) 
		if (dfs(1,i,0,x)) return 1;
	return 0;
}
bool cmp(EDGE A,EDGE B){
	return A.u<B.u;
}
bool cmp2(EDGE A,EDGE B){
	return A.va>B.va;
}
bool check2(int x){
	int now=0,sum=0;
	for (int i=1;i<n;++i){
		now+=E[i].va;
		if (now>=x) ++sum,now=0;
	}
	return sum>=k;
}
bool check3(int x){
	int sum=0;
	for (int i=1;i<n;++i){
		if (!vis[i]){
			if (E[i].va>=x) ++sum;
			else {
				int l=i+1,r=n-1,ans=0;
				while (l<=r) {
					int mid=l+r>>1;
					if (E[i].va+E[mid].va>=x) ans=mid,l=mid+1;
						else r=mid-1;
					}
				if (ans!=0) vis[ans]=1,++sum;
			}
		}
	}
	return sum>=k;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	read(n);read(k);edge=1;
	bool flag=1,flag2=1;
	for (int i=1;i<n;++i){
		read(E[i].u);read(E[i].v);read(E[i].va);
		if (E[i].v!=E[i].u+1) flag=0;
		if (E[i].u!=1) flag2=0;
		add(E[i].u,E[i].v,E[i].va);
		add(E[i].u,E[i].v,E[i].va);
	}
	if (k==1){
		dfs1(1,0);
		int ans=0;
		for (int i=1;i<=n;++i)
			if (ans<ff[i].a+ff[i].b) ans=ff[i].a+ff[i].b;
		printf("%d\n",ans);
		return 0; 
	}
	if (k==(n-1)){
		int ans=1000000000;
		for (int i=2;i<=edge;++i)
			ans=min(ans,val[i]);
		printf("%d\n",ans);
		return 0;
	}
	if (flag){
		int ans=0;
		sort(E+1,E+n,cmp);
		int l=1,r=1e9;
		while (l<=r){
			int mid=l+r>>1;
			if (check2(mid)) ans=mid,l=mid+1;
				else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	if (flag2){
		int ans=0;
		sort(E+1,E+n,cmp2);
		int l=1,r=1e9;
		while (l<=r){
			int mid=l+r>>1;
			if (check3(mid)) ans=mid,l=mid+1;
				else r=mid-1;
		}
		printf("%d\n",ans);
		return 0;
	}
	int ans=0;
	int l=1,r=1e9;
	while (l<=r){
		memset(vis,0,sizeof(vis));
		int mid=l+r>>1;
		if (check(mid)) ans=mid,l=mid+1;
			else r=mid-1;
	}
	printf("%d\n",ans);
	return 0;
}
