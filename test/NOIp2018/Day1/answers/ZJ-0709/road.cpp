#include <cstdio>
using namespace std;
int n;
int a[100500];
void read(int &x){
	char ch=getchar();x=0;int f=0;
	while (ch<'0'||ch>'9') f|=(ch=='-'),ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	if (f>0) x=-x;
}
void wwrite(long long x){
	if (x>9) wwrite(x/10);
	putchar(x%10+'0');
}
inline void write(long long x){
	if (x<0) putchar('-'),x=-x;
	wwrite(x);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i) read(a[i]);
	long long ans=a[1];
	int now=a[1];
	for (int i=2;i<=n;++i){
		if (a[i]<=now) now=a[i];
			else ans=ans+(a[i]-now),now=a[i];
	}
	write(ans);
	return 0;
}
