#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int n,a[110],dp[30000],tot,MAX;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--)
	{
		tot=0;
		memset(dp,0,sizeof(dp));
		scanf("%d",&n);
		for (int i=1;i<=n;i++)
		{
			scanf("%d",&a[i]);
			MAX=max(MAX,a[i]);
		}
		dp[0]=1;
		for (int i=1;i<=n;i++)
			for (int j=a[i];j<=MAX;j++)
				if (dp[j-a[i]]>0) dp[j]++;
		for (int i=1;i<=n;i++) if (dp[a[i]]>1) tot++;
		tot=n-tot;
		printf("%d\n",tot);
	}
	return 0;
}
