#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
struct edge{int v,w,k,nxt;}e[100010];
int n,m,head[50010],cnt=1,size[50010],fa[50010],deep[50010];
int belong[50010],len[50010],fal[50010],d[1000][1000],Max,a[2000];
bool vis[50010],flag;
void add_edge(int u,int v,int w)
{
	e[++cnt].v=v;e[cnt].w=w;e[cnt].k=0;e[cnt].nxt=head[u];head[u]=cnt;
	e[++cnt].v=u;e[cnt].w=w;e[cnt].k=0;e[cnt].nxt=head[v];head[v]=cnt;	
}
void dfs(int u)
{
	size[u]=1;
	for (int i=head[u];i;i=e[i].nxt)
	{
		int v=e[i].v; if (v==fa[u]) continue;
		deep[v]=deep[u]+1; fa[v]=u;
		dfs(v);
		size[u]+=size[v];
	}
	return;
}
void dfs(int u,int chain)
{
	belong[u]=chain; int k=0,w;
	for (int i=head[u];i;i=e[i].nxt)
	{
		int v=e[i].v;
		if (deep[v]>deep[u] && size[v]>size[k]) k=v,w=e[i].w;
	}
	if (k==0) return;
	fal[k]=w; len[k]=len[u]+w; dfs(k,chain);
	for (int i=head[u];i;i=e[i].nxt)
	{
		int v=e[i].v;
		if (deep[v]>deep[u] && v!=k) 
		{fal[v]=e[i].w; len[v]=0; dfs(v,v);}
	}	
	return;
}
void dfs_l(int u,int l,int fa,int &p,int &MAX)
{
	if (l>MAX) {MAX=l; p=u;}
	for (int i=head[u];i;i=e[i].nxt)
	{
		int v=e[i].v,w=e[i].w; if (v==fa) continue;
		dfs_l(v,l+w,u,p,MAX);
	}
	return;
}
int solve(int x,int y)
{
	int ANS=0;
	if (x==y) return 0;
	while (belong[x]!=belong[y])
	{
		if (deep[belong[x]]<deep[belong[y]]) swap(x,y);
		ANS+=len[x]+fal[belong[x]];
		x=fa[belong[x]];
	}
	if (deep[x]<deep[y]) swap(x,y);
	ANS+=len[x]-len[y];
	belong[x]+=0;
	belong[y]+=0;
	return ANS;
}
void dfs(int la,int x,int MIN)
{
	if (x==m-1)
	{
		MIN=min(MIN,a[n]-a[la]);
		Max=max(MIN,Max);
		return;
	}
	for (int i=la+1;i<=n;i++)
	{
		dfs(i,x+1,min(MIN,a[i]-a[la]));
	}
	return;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m==1)
	{
		for (int i=1;i<n;i++)
		{
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			add_edge(x,y,z);
		}
		int x=0,y=0,maxx=-1,maxy=-1;
		dfs(1); dfs(1,1);
		dfs_l(1,0,0,x,maxx);
		dfs_l(2,0,0,y,maxy);
		int Ans=solve(x,y);
		printf("%d\n",Ans);
		return 0;
	}
	for (int i=1;i<n;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		d[x][y]=d[y][x]=z;
	}
	for (int i=1;i<=n;i++) a[i]=a[i-1]+d[i][i-1];
	Max=-1;
	dfs(1,0,1000000000);
	printf("%d\n",Max);
	return 0;
}
