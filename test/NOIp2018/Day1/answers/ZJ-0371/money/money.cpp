#include <bits/stdc++.h>
using namespace std;
int a[105]; int c[25005];
int main(){
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while(T --){
		int mx = 0, n, m;
		scanf("%d", &n); m = n;
		for(int i = 0; i < n; i ++){
			scanf("%d", &a[i]); mx = max(mx, a[i]);
		}
		memset(c, 0, sizeof c);
		c[0] = 1;
		for(int i = 0; i < n; i ++)
			for(int j = 0; j <= mx - a[i]; j ++)
				if(c[j] > 0) c[j + a[i]] ++;
		for(int i = 0; i < n; i ++){
			c[a[i]] --; if(c[a[i]] > 0) m --;
		}
		printf("%d\n", m);
	}
	return 0;
}
