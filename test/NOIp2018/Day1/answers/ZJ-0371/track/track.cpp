#include <bits/stdc++.h>
using namespace std;
const int INF = 2147483647;
int l[50005];
struct Node{
	int mx1, mx2, sum;
}nd[50005];
bool mark[50005];
void change(int value, int id){
	if(value >= nd[id].mx1){
		nd[id].mx2 = nd[id].mx1;
		nd[id].mx1 = value;
	}
	else if(value > nd[id].mx2)
		nd[id].mx2 = value;
}
int vet[100005], val[100005], nxt[100005];
int head[50005], Enum = 0;
void ins(int u, int v, int len){
	vet[Enum] = v; nxt[Enum] = head[u];
	val[Enum] = len; head[u] = Enum;
	Enum ++;
}
void dfs(int id){
	nd[id].mx1 = nd[id].sum;
	nd[id].mx2 = -1; mark[id] = true;
	for(int e = head[id]; e != -1; e = nxt[e]){
		int v = vet[e]; if(mark[v]) continue;
		nd[v].sum = nd[id].sum + val[e];
		dfs(v);
		change(nd[v].mx1, id);
	}
}
int main(){
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	int n, m, a, b;
	scanf("%d%d", &n, &m);
	bool Two = true;
	memset(head, 255, sizeof head);
	for(int i = 1; i < n; i ++){
		scanf("%d%d%d", &a, &b, &l[i]);
		if(a != 1) Two = false;
		ins(a, b, l[i]); ins(b, a, l[i]);
	}
	if(m == 1){
		memset(mark, false, sizeof mark);
		nd[1].sum = 0; dfs(1);
		int ans = 0;
		for(int i = 1; i < n; i ++){
			nd[i].mx1 -= nd[i].sum;
			nd[i].mx2 -= nd[i].sum;
		}
		for(int i = 1; i < n; i ++)
			ans = max(nd[i].mx1 + nd[i].mx2, ans);
		printf("%d\n", ans);
	}
	else if(Two){
		sort(l + 1, l + n);
		if(n > 2 * m){
			int ans = INF;
			for(int i = 1; i <= m; i ++)
				ans = min(ans, l[i] + l[n - i]);
			printf("%d\n", ans);
		}
		else{
			int num = 2 * m, last = n + n - num - 1;
			int ans = l[last];
			for(int i = 1; i <= last; i ++)
				ans = min(ans, l[i] + l[last + 1 - i]);
			printf("%d\n", ans);
		}
	}
	return 0;
}
