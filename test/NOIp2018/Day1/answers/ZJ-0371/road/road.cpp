#include <bits/stdc++.h>
using namespace std;
const int INF = 2147483647;
int a[100005];
long long ans(int l, int r){
	if(l > r) return 0;
	int mn = INF; long long sum = 0;
	for(int i = l; i <= r; i ++)
		mn = min(mn, a[i]);
	int left = l; sum += mn;
	for(int i = l; i <= r; i ++){
		a[i] -= mn;
		if(a[i] == 0){
			sum += ans(left, i - 1); left = i + 1;
		}
	}
	return sum + ans(left, r);
}
int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	int n;
	scanf("%d", &n);
	for(int i = 0; i < n; i ++) scanf("%d", &a[i]);
	printf("%lld\n", ans(0, n - 1));
	return 0;
}
