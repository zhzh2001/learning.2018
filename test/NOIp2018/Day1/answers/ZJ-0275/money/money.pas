var
a:Array[0..110] of longint;
f:Array[0..2510] of boolean;
t,n,i,ans:longint;
procedure qsort(l,r:longint);
var t,i,j,mid:longint;
begin
    i:=l; j:=r;
    mid:=a[(l+r) div 2];
    while i<=j do
    begin
        while a[i]<mid do inc(i);
        while a[j]>mid do dec(j);
        if i<=j then
        begin
            t:=a[i]; a[i]:=a[j]; a[j]:=t;
            inc(i); dec(j);
        end;
    end;
    if l<j then qsort(l,j);
    if i<r then qsort(i,r);
end;
procedure dfs(k,sum,num1,num2:longint);
var i,flag1,flag2:longint; flag:boolean;
begin
    if sum>a[n] then exit;
    if k>n then
    begin
        flag:=true;
        if (num1=n-1)and(num2=1) then flag:=false;
        if flag then f[sum]:=true;
        exit;
    end;
    for i:=0 to a[n] div a[k] do
    begin
        if i=0 then flag1:=1 else flag1:=0;
        if i=1 then flag2:=1 else flag2:=0;
        dfs(k+1,sum+i*a[k],num1+flag1,num2+flag2);
    end;
end;
begin
assign(input,'money.in');
assign(output,'money.out');
reset(input);
rewrite(output);
    readln(t);
    while t>0 do
    begin
        readln(n);
        fillchar(f,sizeof(f),false);
        for i:=1 to n do read(a[i]);
        qsort(1,n);
        if a[1]=1 then writeln(1) else
        begin
            dfs(1,0,0,0);
            ans:=n;
            for i:=1 to n do if f[a[i]] then dec(ans);
            writeln(ans);
        end;
        dec(t);
    end;
close(input);
close(output);
end.
