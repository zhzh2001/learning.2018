var
f,son,fa:Array[0..500010] of longint;
vis:array[0..500010] of boolean;
nxt,head,v,w:array[0..100010] of longint;
n,m,i,u,vv,ans,enum,max1,max2,x,y,z,max_ans,sum:longint;
flag1,flag2:boolean;
procedure addedge(x,y,z:longint);
begin
    inc(enum);
    v[enum]:=y;
    w[enum]:=z;
    nxt[enum]:=head[x];
    head[x]:=enum;
end;
procedure dfs(t:longint);
var  i,vv:longint;
begin
    i:=head[t];
    while i<>0 do
    begin
        vv:=v[i];
        if vis[vv] then break;
        f[vv]:=f[t]+w[i];
        vis[vv]:=true;
        dfs(vv);
        i:=nxt[i];
    end;
end;
procedure qsort(l,r:longint);
var i,j,t,mid:longint;
begin
    i:=l; j:=r;
    mid:=f[(l+r) div 2];
    while i<=j do
    begin
        while f[i]>mid do inc(i);
        while f[j]<mid do dec(j);
        if i<=j then
        begin
            t:=f[i]; f[i]:=f[j]; f[j]:=t;
            inc(i); dec(j);
        end;
    end;
    if l<j then qsort(l,j);
    if i<r then qsort(i,r);
end;
procedure dfs2(t,k,last,min:longint);
begin
    if k+n-t<m-1 then exit;
    if min<max_ans then exit;
    if t=n then
    begin
        if f[n]-f[last]<min then min:=f[n]-f[last];
        if (k=m-1)and(min>max_ans) then max_ans:=min;
        exit;
    end;
    if f[t]-f[last]<min then dfs2(t+1,k+1,t,f[t]-f[last])
    else dfs2(t+1,k+1,t,min);
    dfs2(t+1,k,last,min);
end;
begin
assign(input,'track.in');
assign(output,'track.out');
reset(input);
rewrite(output);
    readln(n,m);
    flag1:=true;  flag2:=true;
    for i:=1 to n-1 do
    begin
        readln(x,y,z);
        if y<>x+1 then flag1:=false;
        addedge(x,y,z);
        addedge(y,x,z);
        inc(son[x]); fa[y]:=x;
        if x<>1 then flag2:=false;
        sum:=sum+z;
    end;
    if m=1 then
    begin
        vis[1]:=true;
        dfs(1);
        for i:=1 to n do
        if son[i]=0 then
        begin
            if f[i]>f[max1] then max1:=i
            else if (f[i]>f[max2])and((fa[i]<>fa[max1])or
            ((fa[i]=1)and(fa[max1]=1)))then max2:=i;
        end;
        writeln(f[max1]+f[max2]);
    end
    else
    if flag2 then
    begin
        vis[1]:=true;
        dfs(1);
        qsort(2,n);
        for i:=2 to m+1 do ans:=ans+f[i];
        writeln(ans);
    end else
    if flag1 then
    begin
        vis[1]:=true;
        dfs(1);
        dfs2(1,0,0,1000000000);
        writeln(max_ans);
    end else
    if m=n-1 then
    begin
        writeln(sum);
    end;
close(input);
close(output);
end.
