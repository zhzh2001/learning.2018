#include<cstdio>
#include<string>
#include<algorithm>
#define R_ register
inline char gc() {
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read() {
	int ret=0,f=1,ch=gc();
	for (; !isdigit(ch); ch=gc()) if (ch=='-') f=-f;
	for (; isdigit(ch); ch=gc()) ret=ret*10+ch-48;
	return ret*f;
}
const int maxn=1000005;
bool vis[maxn],B[maxn];
int N,Ans,cnt,mn,a[maxn],A[maxn];
inline void Calc() {
	mn=A[cnt]*A[cnt-1];
	for (R_ int i=1; i<=mn; ++i) B[i]=0;B[0]=1;
	for (R_ int i=1; i<=mn; ++i)
		for (R_ int j=1; !B[i]&&i>=A[j]&&j<=cnt; ++j)
			if (B[i-A[j]]) B[i]=1;
}
inline void check(int tot) {
	for (R_ int i=1; i<=mn; ++i) vis[i]=0;vis[0]=1;
	for (R_ int i=1; i<=mn; ++i)
		for (R_ int j=1; !vis[i]&&j<=tot; ++j)
			if (i>=a[j]&&vis[i-a[j]]) vis[i]=1;
	for (R_ int i=1; i<=mn; ++i) if (vis[i]^B[i]) return ;
	Ans=tot;
}
void dfs(int x,int tot) {
	if (tot>=Ans) return ;
	if (x>cnt) {
		check(tot);
		return ;
	}
	a[tot+1]=A[x];
	dfs(x+1,tot+1);
	dfs(x+1,tot);
}
int main() {
	freopen("money.in","r",stdin);
	freopen("money_check.out","w",stdout);
	R_ int T,i,j,La,Lb;
	for (T=read(); T--; printf("%d\n",Ans)) {
		for (N=read(),i=1; i<=N; ++i) a[i]=read(),vis[i]=1;
		std::sort(a+1,a+1+N);
		for (i=1; i<=N; ++i) if (vis[i])
			for (j=i+1; j<=N; ++j) if (a[j]%a[i]==0) vis[j]=0;
		mn=a[N]*a[N]+1;
		for (i=1; i<=N; ++i) if (vis[i])
			for (j=i+1; j<=N; ++j) if (vis[j]) {
				mn=std::min(a[i]*a[j],mn);
				La=i,Lb=j;
			}
		for (i=N; i; --i) if (a[i]>mn&&i!=La&&i!=Lb) vis[i]=0;
		for (i=1,cnt=0; i<=N; ++i) if (vis[i]) A[++cnt]=a[i];
//		for (i=1; i<=cnt; ++i) printf("%d ",A[i]);printf("\n");
		Ans=cnt;
		
//		printf("%d\n",cnt);
		if (cnt==1) continue;
		Calc();
		dfs(1,0);
	}
	return 0;
}
