#include<cstdio>
#include<string>
#include<algorithm>
#define R_ register
inline char gc() {
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read() {
	int ret=0,f=1,ch=gc();
	for (; !isdigit(ch); ch=gc()) if (ch=='-') f=-f;
	for (; isdigit(ch); ch=gc()) ret=ret*10+ch-48;
	return ret*f;
}
const int maxn=1005,maxMN=1e7+5;
bool vis[maxMN];
int N,Ans,cnt,mn,a[maxn],A[maxn];
inline int gcd(int x,int y) {return y==0?x:gcd(y,x%y);}
int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	R_ int T,i,j,La,Lb;
	for (T=read(); T--; printf("%d\n",Ans)) {
		for (N=read(),i=1; i<=N; ++i) a[i]=read(),vis[i]=1;
		std::sort(a+1,a+1+N);
		for (i=1; i<=N; ++i) if (vis[i])
			for (j=i+1; j<=N; ++j) if (a[j]%a[i]==0) vis[j]=0;
		mn=a[N]*a[N]+1,La=Lb=-1;
//		for (i=1; i<=N; ++i) printf("%d ",vis[i]);puts("---------------");
		for (i=1; i<=N; ++i) if (vis[i])
			for (j=i+1; j<=N; ++j) if (vis[j]&&gcd(a[i],a[j])==1) {
				mn=std::min(a[i]*a[j]-a[i]-a[j],mn);
				La=i,Lb=j;
			}
//		printf("%d\n",mn);
//		for (i=1; i<=N; ++i) printf("%d ",vis[i]);puts("---------------");
		for (i=N; i; --i) if (a[i]>mn&&i!=La&&i!=Lb) vis[i]=0;
//		for (i=1; i<=N; ++i) printf("%d ",vis[i]);puts("---------------");
		for (i=1,cnt=0; i<=N; ++i) if (vis[i]) A[++cnt]=a[i];
//		for (i=1; i<=cnt; ++i) printf("%d ",A[i]);printf("\n");
		Ans=cnt;
		if (cnt<=2) continue;
		Ans=2;
		mn=A[1]*A[2];
		for (i=1; i<=mn; ++i) vis[i]=0;
		vis[0]=1,vis[A[1]]=1,vis[A[2]]=1;
		for (i=A[1]+1; i<=mn; ++i)
			if (vis[i-A[1]]||i>A[2]&&vis[i-A[2]]) vis[i]=1;
		for (i=3; i<=cnt; ++i) {
			if (A[i]>mn) break;
			if (!vis[A[i]]) {
				++Ans;
				for (j=0; j<=mn-A[i]; ++j)
					if (vis[j]) vis[j+A[i]]=1;
				while (vis[mn]&&mn>0) --mn;
			}
		}
	}
	return 0;
}
