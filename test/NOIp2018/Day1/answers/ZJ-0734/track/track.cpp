#include<set>
#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
#define R_ register
inline char gc() {
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read() {
	int ret=0,f=1,ch=gc();
	for (; !isdigit(ch); ch=gc()) if (ch=='-') f=-f;
	for (; isdigit(ch); ch=gc()) ret=ret*10+ch-48;
	return ret*f;
}
const int maxn=2e5+5;
bool flg1,flg2;
int N,M,tot,rt,mn,mx,son[maxn],nxt[maxn],w[maxn],lnk[maxn],dis[maxn];
inline int Abs(int x) {return x>0?x:-x;}
inline void add_edge(int z,int y,int x) {
	if (Abs(x-y)!=1) flg1=0;
	if (x!=1&&y!=1) flg2=0;
	mn=std::min(z,mn),mx=std::max(mx,z);
	son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot,w[tot]=z;
	son[++tot]=x,nxt[tot]=lnk[y],lnk[y]=tot,w[tot]=z;
}
void dfs1(int x,int pre) {
	if (dis[x]>dis[rt]) rt=x;
	for (R_ int k=lnk[x],v; v=son[k],k; k=nxt[k]) if (v^pre)
		dis[v]=dis[x]+w[k],dfs1(v,x);
}
inline bool check(int x) {
	int cnt=0,now=0;
	for (int i=2; i<=N; ++i) {
		now+=dis[i]-dis[i-1];
		if (now>=x) ++cnt,now=0;
	}
	return cnt>=M;
}
inline bool check1(int x) {
	std :: set<int> S;
	int cnt=0,i;
	for (i=N; i; --i) if (dis[i]>=x) ++cnt;else break;
//	printf("%d\n",i);
	if (cnt>=M) return 1;
	for (; i>1; --i) S.insert(dis[i]);
	for (; cnt<M&&S.size()>1; ) {
		int a=*S.rbegin();
		S.erase(a);
		int b=x-a;
		int c=*S.rbegin();
		if (c<b) break;
		S.erase(S.lower_bound(b));
		++cnt;
	}
	return cnt>=M;
}
int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	R_ int i;
	flg1=1,flg2=1,mn=1e5,mx=0;
	for (N=read(),M=read(),i=1; i<N; ++i) add_edge(read(),read(),read());
	if (M==1) {
		dfs1(1,0);
		memset(dis,0,sizeof dis);
		dfs1(rt,0);
		printf("%lld\n",dis[rt]);
		return 0;
	}
	if (flg1) {
		dfs1(1,0);
		int L=mn,R=mx*(N-1),Ans=L;
		while (L<=R) {
			int mid=L+(R-L>>1);
			if (check(mid)) Ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",Ans);
		return 0;
	}
	if (flg2) {
		dfs1(1,0);
		std::sort(dis+1,dis+1+N);
		int L=dis[1],R=dis[N],Ans=dis[1];
		while (L<=R) {
			int mid=L+(R-L>>1);
			if (check1(mid)) Ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",Ans);
		return 0;
	}
	return 0;
}
