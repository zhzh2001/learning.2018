#include<cmath>
#include<cstdio>
#include<string>
#define R_ register
inline char gc() {
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read() {
	int ret=0,f=1,ch=gc();
	for (; !isdigit(ch); ch=gc()) if (ch=='-') f=-f;
	for (; isdigit(ch); ch=gc()) ret=ret*10+ch-48;
	return ret*f;
}
const int maxn=1e5+5;
int N,Ans,f[maxn][20],lg[maxn];
inline int Query(int L,int R) {
	int k=lg[R-L+1];
	return std::min(f[L][k],f[R-(1<<k)+1][k]);
}
void dfs(int L,int R,int lev) {
	if (L>R) return ;
	if (L==R) {
		Ans+=f[L][0]-lev;
		return ;
	}
	int mn=Query(L,R),l=L,r=R;
	Ans+=mn-lev;
	while (l<=r) {
		if (f[l][0]==mn) {
			dfs(L,l-1,mn),dfs(l+1,R,mn);
			return ;
		}
		if (f[r][0]==mn) {
			dfs(L,r-1,mn),dfs(r+1,R,mn);
			return ;
		}
		++l,--r;
	}
}
int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	R_ int i,j;
	for (N=read(),i=1; i<=N; ++i) f[i][0]=read();
	for (i=2; i<=N; ++i) lg[i]=lg[i>>1]+1;
	for (j=1; j<20; ++j)
		for (i=1; i+(1<<j-1)<=N; ++i)
			f[i][j]=std::min(f[i][j-1],f[i+(1<<j-1)][j-1]);
	dfs(1,N,0);
	printf("%d\n",Ans);
	return 0;
}
