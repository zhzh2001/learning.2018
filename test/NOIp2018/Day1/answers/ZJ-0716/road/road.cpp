#include <bits/stdc++.h>
using namespace std;
bool mmm1;
#define M 100005
int n;
int A[M];

struct P70 {
	int ans;
	void dfs(int L,int R) {
		int mn=(int)1e9;
		for(int i=L;i<=R;++i)
			mn=min(mn,A[i]);
		ans+=mn;
		int lst=L-1;
		for(int i=L;i<=R;++i) {
			A[i]-=mn;
			if(A[i]==0) {
				if(lst+1<=i-1) {
					dfs(lst+1,i-1);
				}
				lst=i;
			}
		}
		if(lst+1<=R) dfs(lst+1,R);
	}
	void solve() {
		ans=0;
		dfs(1,n);
		printf("%d\n",ans);
	}
} p70;

//void read(int &x) {
//	char c;
//	do c=getchar(); while(c<'0'||c>'9');
//	x=c^48;
//	while(c=getchar(),c>='0'&&c<='9') x=(x<<1)+(x<<3)+(c^48);
//}

struct P100 {
	void solve() {
		int ans=A[1];
		for(int i=1;i<n;++i) {
			if(A[i+1]>A[i]) ans+=A[i+1]-A[i];
		}
		printf("%d\n",ans);
	}
} p100;

bool mmm2;
int main() {
//	cout<<(&mmm2-&mmm1)/1024.0/1024<<endl;
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;++i) scanf("%d",A+i);
	if(n<=1000) p70.solve();
	else p100.solve();
	return 0;
}

