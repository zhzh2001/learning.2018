#include <bits/stdc++.h>
using namespace std;
bool mmm1;
#define M 50005
int n,m;

struct Edge {
	int to,nxt,len;
}E[M<<1];
int head[M],ptr;
void add_edge(int a,int b,int c) {
	E[++ptr]=(Edge){b,head[a],c};
	head[a]=ptr;
}
#define forgraph(x) for(int i=head[x];i!=0;i=E[i].nxt)

struct P1 {
	int mxdis;
	int dfs(int x,int f) {
		int fi=0,se=0;
		forgraph(x) {
			int y=E[i].to;
			if(y==f) continue;
			int len=dfs(y,x)+E[i].len;
			if(fi<len) se=fi,fi=len;
			else if(se<len) se=len;
		}
		mxdis=max(mxdis,fi+se);
		return fi;
	}
	void solve() {
		dfs(1,0);
		printf("%d\n",mxdis);
	}
} p1;

int Len[M];

struct Pjuhua {
	bool check(int d) {
		int i=1,j=n-1;
		int cnt=0;
		while(j>0 && Len[j]>=d) j--,cnt++;
		while(i<j) {
			while(i<j && Len[i]+Len[j]<d) i++;
			if(i<j) cnt++;
			j--;
		}
		return cnt>=m;
	}
	void solve() {
		sort(Len+1,Len+n);
		int L=0,R=Len[n-1]+Len[n-2];
		int ans;
		while(L<=R) {
			int mid=(L+R)>>1;
			if(check(mid)) ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
} pjuhua;

bool mmm2;
int main() {
//	cout<<(&mmm2-&mmm1)/1024.0/1024<<endl;
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	int fjuhua=1;
	for(int i=1;i<n;++i) {
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		add_edge(a,b,c);
		add_edge(b,a,c);
		Len[i]=c;
		if(a!=1) fjuhua=0;
	}
	if(m==1) p1.solve();
	else if(fjuhua) pjuhua.solve();
	else pjuhua.solve();
	return 0;
}

