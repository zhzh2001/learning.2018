#include <bits/stdc++.h>
using namespace std;
bool mmm1;

#define M 105
int n;
int A[M];

long long gcd(long long a,long long b) {
	if(b==0) return a;
	return gcd(b,a%b);
}

struct P30 {
	int stmp;
	int vis1[100005];
	int vis2[100005];
	int m,lcm;
	int B[M];
	bool ff(int x) {
		if(x>=lcm) return 1;
		if(vis1[x]==0) return 0;
		if(vis2[x]==stmp) return 1;
		vis2[x]=stmp;
		for(int i=1;i<=m;++i)
			if(!ff(x+B[i])) return 0;
		return 1;
	}
	bool chk() {
		if(!ff(0)) return 0;
		for(int i=1;i<lcm;++i)
			if((vis1[i]>0)!=(vis2[i]==stmp))
				return 0;
		return 1;
	}
	bool dfs(int x) {
		if(x>m) {
			stmp++;
			return chk();
		}
		for(int i=1;i<=1000;++i) {
			B[x]=i;
			if(dfs(x+1)) return 1;
		}
		return 0;
	}
	void get(int x) {
		if(x>=lcm) return;
		if(vis1[x]) return;
		vis1[x]=1;
		for(int i=1;i<=n;++i)
			get(x+A[i]);
	}
	void solve() {
		int ans=n;
		int g=1;
		memset(vis1,0,sizeof vis1);
		memset(vis2,0,sizeof vis2);
		stmp=0;
		m=1;
		for(int i=1;i<=n;++i) {
			m*=A[i];
			g=gcd(g,A[i]);
		}
		lcm=m/g;
		get(0);
		for(int i=1;i<n;++i) {
			m=i;
			if(dfs(1)) {
				ans=i;
				break;
			}
		}
		printf("%d\n",ans);
	}
} p30;

struct Pun {
	int stmp;
	int vis1[100005];
	int vis2[100005];
	long long m,lcm;
	int B[M];
	bool ff(int x) {
		if(x>=101) return 1;
		if(vis1[x]==0) return 0;
		if(vis2[x]==stmp) return 1;
		vis2[x]=stmp;
		for(int i=1;i<=m;++i)
			if(!ff(x+B[i])) return 0;
		return 1;
	}
	bool chk() {
		stmp++;
		if(!ff(0)) return 0;
		for(int i=1;i<101;++i)
			if((vis1[i]>0)!=(vis2[i]==stmp))
				return 0;
		return 1;
	}
	void get(int x) {
		if(x>=10001) return;
		if(vis1[x]) return;
		vis1[x]=1;
		for(int i=1;i<=n;++i)
			get(x+A[i]);
	}
	void solve() {
		int ans=n;
		memset(vis1,0,sizeof vis1);
		memset(vis2,0,sizeof vis2);
		stmp=0;
		m=1;
		int g=1;
		for(int i=1;i<=n;++i) m*=A[i],g=gcd(g,A[i]);
		lcm=m/g;
		get(0);
		for(int i=1; i<1<<16; ++i) {
			int cnt=0;
			for(int j=0; j<16; ++j) {
				if(i&1<<j) {
					B[++cnt]=A[j+1];
				}
			}
			m=cnt;
			if(chk()) ans=min(ans,cnt);
		}
		printf("%d\n",ans);
	}
} pun;

bool mmm2;
int main() { //multi cases ,clear!
//	cout<<(&mmm2-&mmm1)/1024.0/1024<<endl;
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--) {
		scanf("%d",&n);
		for(int i=1; i<=n; ++i) {
			scanf("%d",A+i);
		}
		if(n<=3) p30.solve();
		else pun.solve();
	}
	return 0;
}

