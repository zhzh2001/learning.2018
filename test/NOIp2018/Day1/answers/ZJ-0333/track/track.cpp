#include<cstdio>
#include<vector>
#include<algorithm>
#include<string.h>
#include<iostream>
using namespace std;
const int MAXN=5e4+10;
struct node{
	int u,to,w;
};
node edge[MAXN<<1];
int head[MAXN],f[MAXN],last[MAXN];
vector<int>q[MAXN];
int n,m,x,y,z,k,w,ans;
bool b[MAXN];
void fc(){
	fclose(stdin);
	fclose(stdout);
}
void add(int x,int y,int z)
{
	edge[k].u=y;
	edge[k].to=head[x];
	edge[k].w=z;
	head[x]=k++;
}
void dfs(int now,int fa,int x)
{
	for (int i=head[now];i!=-1;i=edge[i].to)
	{
		int u=edge[i].u;
		if (u==fa) continue;
		dfs(u,now,x);
		f[u]+=edge[i].w;
	    if (f[u]>=x) w++;
	    else q[now].push_back(f[u]);
	}
	if (q[now].empty()) return;
	sort(q[now].begin(),q[now].end());
	int len=q[now].size()-1;
	for (int i=0;i<=len;i++) b[i]=0;
	for (int i=0;i<=len;i++) last[i]=0;
	int t=len;
	for (int i=0;i<=len;i++)
	 if (i<t){
	 if (q[now][i]+q[now][t]>=x) {
	 	w++; b[i]=1; b[t]=1; last[t]=q[now][i]; t--;
	 } 
     } else break;
     for (int i=len;i>=0;i--) if (!b[i]) {
     	f[now]=q[now][i]; break;
	 }
	 for (int i=t;i<=len;i++)
	  if (b[i-1]==0&&q[now][i-1]+last[i]>=x) {
	  	b[i-1]=1; b[i]=0; f[now]=max(f[now],q[now][i]);
	  }
	q[now].clear();
}
bool pan(int x)
{
	 w=0;
	 memset(f,0,sizeof(f));
     dfs(1,0,x);
     return w>=m;
}
void solve(int l,int r)
{
    if (l>r) return;
	int mid=(l+r)/2; 	
	if (pan(mid)) 
	{
		ans=mid; solve(mid+1,r);
	}
	else solve(l,mid-1);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1) {
		cout << "������" << endl;  fc(); return 0;
	}
    int sum=0;
    memset(head,-1,sizeof(head));
	for (int i=1;i<n;i++) {
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z); add(y,x,z); sum+=z;
	}
	solve(0,sum);
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
}
