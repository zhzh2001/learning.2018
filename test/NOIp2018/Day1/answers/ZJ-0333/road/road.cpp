#include<cstdio>
#include<math.h>
using namespace std;
const int MAXN=1e5+10;
int a[MAXN];
int ans,n;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) {
		scanf("%d",&a[i]);
	}
	a[0]=0;
	for (int i=1;i<=n;i++) ans+=abs(a[i]-a[i-1]);
	ans+=a[n];
	printf("%d\n",ans/2);
	fclose(stdin);
	fclose(stdout);
}
