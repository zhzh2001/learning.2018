#include<cstdio>
#include<algorithm>
#include<string.h>
using namespace std;
const int MAXN=110;
const int MAX=25010;
int T,a[MAXN],n,a_max;
bool f[MAX];
bool pan(int x)
{
	if (f[a[x]]) return 0;
	for (int i=0;i<=a_max-a[x];i++)
	 if (f[i]) f[i+a[x]]=1;
	return 1; 
}
void solve()
{
	sort(a+1,a+n+1);
	int ans=0;
	for (int i=1;i<=n;i++) 
	if (pan(i)) ans++;
	printf("%d\n",ans);
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		memset(f,0,sizeof(f));
		f[0]=1;
		a_max=0;
		for (int i=1;i<=n;i++) {
			scanf("%d",&a[i]); a_max=max(a_max,a[i]);
		}
		solve();
	}
	fclose(stdin);
	fclose(stdout);
}
