#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0'|| ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 50005;
int n,m;
int to[N*2],nxt[N*2],w[N*2],head[N],cnt;
int num,mid;
int dep[N];
multiset<int> son[N];
bool isleaf[N];
int in[N];

inline void insert(int a,int b,int c){
	to[++cnt] = b; nxt[cnt] = head[a]; head[a] = cnt; w[cnt] = c;
}

void dfs(int x,int fa){
	if(isleaf[x]) return;
	son[x].clear();
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa){
			dfs(to[i],x);
			if(w[i]+dep[to[i]] < mid)
				son[x].insert(w[i]+dep[to[i]]);
			else ++num;
		}
	set<int> ::iterator it1 = son[x].begin();
	while(it1 != son[x].end()){
		set<int> ::iterator it2 = son[x].lower_bound(mid-(*it1));
		if(it2 == it1) ++it2;
		if(it2 == son[x].end()){
			++it1;
		}else{
			++num;
			set<int> ::iterator it3 = it1;
			++it3;
			if(it3 == it2) ++it3;
			son[x].erase(it1);
			son[x].erase(it2);
			it1 = it3;
		}
	}
	if(son[x].empty()) dep[x] = 0;
	else dep[x] = *(--son[x].end());
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n = read(); m = read();
	int tot = 0,rt = 0;
	for(int i = 1;i < n;++i){
		int a = read(),b = read(),c = read();
		insert(a,b,c); insert(b,a,c);
		++in[a]; ++in[b];
		tot += c;
	}
	for(int i = 1;i <= n;++i) if(in[i] == 1){rt = i; break;}
	for(int i = rt+1;i <= n;++i)
		if(in[i] == 1) isleaf[i] = true;
	int ans = 0,l = 1,r = tot;
	while(l <= r){
		mid = (l+r)>>1;
		num = 0;
		dfs(rt,rt);
		if(num >= m){
			l = mid+1;
			ans = mid;
		}else r = mid-1;
	}
	printf("%d\n",ans);
	return 0;
}