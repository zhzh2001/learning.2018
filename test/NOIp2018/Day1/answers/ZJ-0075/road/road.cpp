#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0'|| ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int a[N];
int n;
ll ans;

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;++i){
		a[i] = read();
		if(a[i] > a[i-1]){
			ans += a[i]-a[i-1];
		}
	}
	printf("%lld\n",ans);
	return 0;
}