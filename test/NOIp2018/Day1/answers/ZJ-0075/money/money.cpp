#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0'|| ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 105,M = 25005;
int n;
int a[N];
bool dp[M];

int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int T = read();
	while(T--){
		n = read(); int m = n,num;
		for(int i = 1;i <= n;++i){
			a[i] = read();
		}
		sort(a+1,a+n+1);
		num = a[n];
		for(int i = 1;i <= num;++i) dp[i] = 0;
		dp[0] = 1;
		for(int i = 1;i <= n;++i){
			if(dp[a[i]]) --m;
			else{
				for(int b = a[i];b <= num;++b)
					if(dp[b-a[i]]) dp[b] = true;
			}
		}
		printf("%d\n",m);
	}
	return 0;
}