#include<bits/stdc++.h>
using namespace std;

inline void F()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}
const int maxn=1000;
int n,m,a[maxn],maxa,tong[25010];
int main()
{
	F();
    int T;
    scanf("%d",&T);
    while (T--)
      {
      	scanf("%d",&n);
      	bool p=true;
      	memset(tong,0,sizeof(tong));
      	for (int i=1; i<=n; i++)
      	  {
      	  	scanf("%d",&a[i]);
      	  	if (a[i]==1) p=false;
      	  	tong[a[i]]=1;
      	  	maxa=max(maxa,a[i]);
      	  }
      	sort(a+1,a+n+1);
      	if (!p) {printf("%d\n",1); continue;} 
      	for (int i=1; i<=n; i++)
      	  for (int j=a[i]; j<=maxa; j++)
      	    {
      	      if (tong[j]==1&&j+a[i]<=maxa) tong[j+a[i]]=1;
      	    }
		int ans=0;
		for (int i=1; i<=n; i++)
		  for (int j=1; j<a[i]; j++)
		    	if (tong[a[i]-j]!=0&&tong[j]!=0) 
				{
				  //cout<<a[i]<<' '<<tong[a[i]-j]<<' '<<tong[j]<<endl;
				  ans++;
				  break;
			    }
		printf("%d\n",n-ans);
      }
    
}
