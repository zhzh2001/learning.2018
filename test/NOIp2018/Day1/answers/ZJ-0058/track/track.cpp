#include<bits/stdc++.h>
using namespace std;
const int maxn=50010;
int cnt,n,m,head[maxn];
long long sum[maxn],gen[maxn],ansm1,ansgen,anslian,fsum[maxn][3],num;
struct node
{
	int v,nxt,w;
}e[maxn<<1];
inline void add(int u,int v,int w)
{
	e[++cnt].v=v;
	e[cnt].w=w;
	e[cnt].nxt=head[u];
	head[u]=cnt;
}
inline void dfsm1(int u,int fa)
{
	int max1=0,max2=0;
	for (int i=head[u]; i; i=e[i].nxt)
	  {
	  	int v=e[i].v;
	  	if (v==fa) continue;
	  	//sum[v]+=e[i].w;
		dfsm1(v,u);
		sum[u]=max(sum[u],sum[v]+e[i].w);
		if (sum[u]>max1) max2=max1,max1=sum[u];
		  else if (sum[u]>max2) max2=sum[u];
	  }
	if (u==1) ansm1=max1+max2;
}
inline void dfsgen(int u,int fa)
{
	int max1=0,max2=0,gencnt;
	for (int i=head[u]; i; i=e[i].nxt)
	  {
	  	int v=e[i].v;
	  	if (v==fa) continue;
	    gen[++gencnt]=e[i].w;
	  }
    sort(gen+1,gen+gencnt+1);
    ansgen=gen[gencnt]+gen[gencnt-1];
    if (2*m<=gencnt)
    {
      int i=max(gencnt-2*m+1,1),j=gencnt,mid=(i+j)/2;
      while (i<=mid)
        {
        	ansgen=min(gen[i]+gen[j],ansgen);
        	i++; j--;
        }
    }
    else
    {
      int i=max(gencnt-2*m+1,1),j=gencnt,mid=(i+j)/2;
      while (i<=mid)
        {
        	if (i==j) ansgen=min(ansgen,gen[i]);
        	else ansgen=min(gen[i]+gen[j],ansgen);
        	i++; j--;
        }
    }
      
}
inline void dfslian(int u,int fa)
{
	int max1=0,max2=0,tot=1;
	for (int i=head[u]; i; i=e[i].nxt)
	  {
	  	int v=e[i].v;
	  	if (v==fa) continue;
	  	sum[++tot]=sum[tot-1]+e[i].w;
	  	v=u;
	  }
	for (int i=1; i<=tot; i++)
	  {
	  	fsum[i][1]=max(fsum[i-1][0],fsum[i][1]+sum[i]);
	  }
}
inline void F()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
}
int main()
{
	F();
	scanf("%d%d",&n,&m);
	bool pgen=true,plian=true;
	for (int i=1; i<n; i++)
	  {
	  	int x,y,z;
	  	scanf("%d%d%d",&x,&y,&z);
	  	if (x!=1) pgen=false;
	  	if (y!=x+1) plian=false;
	  	num+=z;
	  	add(x,y,z);
	  	add(y,x,z);
	  }
	if (m==1)
	  {
	  	dfsm1(1,0);
	  	printf("%lld",ansm1);
	  	return 0;
	  }
	if (pgen)
	  {
	  	dfsgen(1,0);
	  	printf("%lld",ansgen);
	  	return 0;
	  }
	if (plian)
	  {
	  	dfslian(1,0);
	  	printf("%lld",anslian);
	  	return 0;
	  }
	printf("%d",num/m);
}
