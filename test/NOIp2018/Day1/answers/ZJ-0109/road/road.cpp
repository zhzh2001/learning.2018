#include<bits/stdc++.h>
#define pii pair<int,int>
#define fi first
#define se second
#define N 100007
using namespace std;
int n,usd[N],anp; long long ans;
pii a[N];
int main () {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i].fi),a[i].se=i;
	sort(a+1,a+n+1);
	for (int i=n,j;i>=1;i=j-1) {
		j=i;
		while (a[j-1].fi==a[i].fi) j--;
		if (a[j].fi==0) continue;
		for (int k=i;k>=j;k--) {
			usd[a[k].se]=1; anp++;
			if (usd[a[k].se-1]) anp--;
			if (usd[a[k].se+1]) anp--;
		}
	   ans=ans+1ll*anp*(a[j].fi-a[j-1].fi);
	}
	printf("%lld\n",ans); return 0;
}
