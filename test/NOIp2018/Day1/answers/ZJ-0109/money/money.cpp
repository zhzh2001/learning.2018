#include<bits/stdc++.h>
using namespace std;
int T,n,dp[107][25007],usd[25007],ans,a[107],top;
int main () {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		scanf("%d",&n); ans=n;
		memset(dp,0,sizeof dp);
		dp[0][0]=1;
		for (int i=1;i<=n;i++) scanf("%d",a+i);
		sort(a+1,a+n+1);
		for (int i=1;i<=n;i++) {
			if (dp[i-1][a[i]]) ans--;
			for (int j=0;j<a[i];j++) usd[j]=dp[i-1][j],dp[i][j]=dp[i-1][j];
			top=0;
			for (int j=a[i];j<=a[n];j++) {
				usd[top]|=dp[i-1][j];
				dp[i][j]=usd[top]; top++;
				if (top>=a[i]) top-=a[i];
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
