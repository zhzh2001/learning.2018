#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
bool v[25003];
int T,n,ans,a[103],x,y;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T-->0){
		n=read();
		memset(v,0,sizeof(v));
		v[0]=1;
		ans=0;
		for(int i=1;i<=n;i++){
			a[i]=read();
		}
		sort(a+1,a+n+1);
		y=a[n];
		for(int i=1;i<=n;i++){
			x=a[i];
			if(!v[x]){
				ans++;
				for(int i=x;i<=y;i++){
					v[i]|=v[i-x];
				}
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
