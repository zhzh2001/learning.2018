#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,m,a[100003],v,x;
vector<int>e[100003];
LL ans;
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	m=read();
	n=0;
	for(int i=1;i<=m;i++){
		a[++n]=read();
		if(n!=1&&a[n]==a[n-1])n--;
	}
	v=1;
	for(int i=1;i<=n;i++){
		e[a[i]].push_back(i);
		if(i!=1&&i!=n&&a[i]==0)v++;
	}
	a[0]=a[n+1]=0;
	ans=0;
	for(int i=1;i<=10000;i++){
		ans+=v;
		for(int j=0;j<e[i].size();j++){
			x=e[i][j];
			if(a[x-1]<i&&a[x+1]<i){
				v--;
			}else if(a[x-1]>i&&a[x+1]>i){
				v++;
			}
		}
	}
	printf("%lld\n",ans);
	return 0;
}
