#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,m,x,y,z,vv;
struct st{
	int v,w;
	st(){
		v=w=0;
	}
	st(int _v,int _w){
		v=_v;
		w=_w;
	}
};
vector<st>e[50003];
int gtv(vector<int>&a,int vv,int mid){
	if(a.empty())return 0;
	int l=0,r=a.size()-1,ans=0;
	if(r==mid)r--;
	while(r>=0&&a[r]>=vv){
		r--;
		ans++;
		if(r==mid)r--;
	}
	while(l<r){
		while(l<r&&(a[l]+a[r]<vv||l==mid))l++;
		if(l>=r)break;
		ans++;
		l++;
		r--;
		if(r==mid)r--;
		if(l==mid)l++;
	}
	return ans;
}
st gtk(vector<int>&a,int vv){
	if(a.empty())return st(0,0);
	sort(a.begin(),a.end());
	int ansv=gtv(a,vv,a.size());
	if(gtv(a,vv,0)<ansv)return st(ansv,0);
	int l=0,r=a.size()-1,mid;
	while(l<r){
		mid=(r-l+1)/2+l;
		if(gtv(a,vv,mid)<ansv)r=mid-1;
		else l=mid;
	}
	return st(ansv,a[l]);
}
st dfs(int u,int fa,int vv){
	vector<int>a;
	st x;
	int vs=0;
	int es=e[u].size();
	for(int i=0;i<es;i++){
		if(e[u][i].v==fa)continue;
		x=dfs(e[u][i].v,u,vv);
		vs+=x.v;
		a.push_back(x.w+e[u][i].w);
	}
	x=gtk(a,vv);
	x.v+=vs;
	return x;
}
int bs(int l,int r){
	int mid;
	while(l<r){
		mid=(l+r+1)/2;
		if(dfs(1,-1,mid).v>=m)l=mid;
		else r=mid-1;
	}
	return l;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<n;i++){
		x=read();
		y=read();
		z=read();
		e[x].push_back(st(y,z));
		e[y].push_back(st(x,z));
		vv+=z;
	}
	printf("%d\n",bs(0,vv));
	return 0;
}
