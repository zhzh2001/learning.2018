#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdlib>
using namespace std;
int N,M;
int Iceloki[50005],Cranky[50005],Junk[50005];
int MarkJ[50005];
int Head[50005];
struct lowiro{
	int to,Next,C4cat;
}	E[100005];
int tot=0;
void add(int x,int y,int l){
	tot++;
	E[tot].C4cat=l;
	E[tot].to=y;
	E[tot].Next=Head[x];
	Head[x]=tot;
}
//L Chapter        Conflict       ??????????Specta
int First_Judgement_ai_1_bi(){
	for(int i=1;i<N;i++)MarkJ[i]=1;
	for(int i=1;i<N;i++)if(MarkJ[Iceloki[i]])MarkJ[Iceloki[i]]=0;else return 0;
	return 1;	
}
void Result1(int Total){
	sort(Junk+1,Junk+N+1);
	for(int i=1;i<M;i++)Total-=Junk[i];
	printf("%d\n",Total);
}
int Dfs(int x,int pre,int Deep){//cout<<Deep<<endl;
	if(Deep+M>N)return 0;
	int UI=0,flag=false;
	for(int i=Head[x];i!=-1;i=E[i].Next){
		int y=E[i].to,z=E[i].C4cat;
		if(y==pre)continue;
		else UI=max(UI,z+Dfs(y,x,Deep+1)),flag=true;
	}
	return flag?UI:0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&N,&M);
	for(int i=1;i<=N;i++)Head[i]=-1;
	int Total=0,Max=0;
	for(int i=1;i<N;i++){
		scanf("%d%d%d",&Iceloki[i],&Cranky[i],&Junk[i]);
		if(Iceloki[i]>Cranky[i])swap(Iceloki[i],Cranky[i]);
		Total+=Junk[i];
		Max=max(Max,Junk[i]);
		add(Iceloki[i],Cranky[i],Junk[i]);
		add(Cranky[i],Iceloki[i],Junk[i]);
	}
	if(M==N-1){
		printf("%d\n",Max);
		return 0;
	}
	//First Judgement:	ai+1=bi
	int p=First_Judgement_ai_1_bi();
	if(p){
		Result1(Total);
		return 0;
	}
	p=0;
	for(int i=1;i<=N;i++)p=max(p,Dfs(i,-1,1));
	printf("%d\n",p);
	fclose(stdin);fclose(stdout);
	return 0;
}
