#include<bits/stdc++.h>
#define rep(i,j,k) for(int i = j;i <= k;++i)
using namespace std;
int rd()
{
	int sum = 0;char c = getchar();bool flag = true;
	while(c < '0' || c > '9'){if(c == '-') flag = false;c = getchar();}
	while(c >= '0' && c <= '9') sum = sum*10+c-48,c = getchar();
	if(flag) return sum;
	else return -sum;
}
int n;
int a[101000];
int mn[101000][20];
int ans;
int ask(int l,int r)
{
	int k = log2(r-l+1);
	return a[mn[l][k]] > a[mn[r-(1<<k)+1][k]] ? mn[r-(1<<k)+1][k] : mn[l][k];
}
void solve(int l,int r,int now)
{
	if(l>r) return;
	int pos = ask(l,r);
	ans += a[pos]-now;
	solve(l,pos-1,a[pos]);solve(pos+1,r,a[pos]);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n = rd();
	rep(i,1,n) a[i] = rd(),mn[i][0] = i;
	for(int j = 1;(1<<j) <= n;++j)
	    for(int i = 1;i+(1<<j)-1 <= n;++i)
	        mn[i][j] = a[mn[i][j-1]] > a[mn[i+(1<<(j-1))][j-1]] ? mn[i+(1<<(j-1))][j-1] : mn[i][j-1];
	solve(1,n,0);
	printf("%d\n",ans);
	return 0;
}
