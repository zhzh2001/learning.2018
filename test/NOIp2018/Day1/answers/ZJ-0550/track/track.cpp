#include<bits/stdc++.h>
#define rep(i,j,k) for(int i = j;i <= k;++i)
#define repp(i,j,k) for(int i = j;i >= k;--i)
#define ll long long
using namespace std;
int rd()
{
	int sum = 0;char c = getchar();bool flag = true;
	while(c < '0' || c > '9'){if(c == '-') flag = false;c = getchar();}
	while(c >= '0' && c <= '9') sum = sum*10+c-48,c = getchar();
	if(flag) return sum;
	else return -sum;
}
int n,m;
int cnt,f[50100],mid;
int linkk[50100],t;
struct node{int n,y,v;}e[101000];
int tmp[50100],tot;
bool flag[50100];
void insert(int x,int y,int z)
{
	e[++t].y = y;e[t].n = linkk[x];e[t].v = z;linkk[x] = t;
	e[++t].y = x;e[t].n = linkk[y];e[t].v = z;linkk[y] = t;
}
void init()
{
	n = rd();m = rd();
	rep(i,1,n-1)
	{
		int x = rd(),y = rd(),z = rd();
		insert(x,y,z);
	}
}
int check2(int x)
{
	int now = 0;
	int i = 1,j = tot;
	while(i <= j)
	{
		if(i == x) i++;
		else if(j == x) j--;
		else if(tmp[i] >= mid) i++,now++;
		else if(tmp[j] >= mid) j--,now++;
		else if(i < j && tmp[i]+tmp[j] >= mid) i++,j--,now++;
		else i++;
	}
	return now;
}
int solve(int x)
{
	int now = x;
	int l = 0,r = tot;
	repp(i,tot,1) if(flag[i]) {l = i;break;}
	if(l == r) return tmp[l];
	while(l + 1 < r)
	{
		int mid = l+r>>1;
		if(check2(mid) == now) l = mid;
		else r = mid;
	}
	if(check2(r) == now) return tmp[r];
	else return tmp[l];
}
void dfs(int x,int fa)
{
	f[x] = 0;
	for(int i = linkk[x];i;i = e[i].n)
	    if(e[i].y != fa)
	    	dfs(e[i].y,x);
	tot = 0;
	for(int i = linkk[x];i;i = e[i].n)
	    if(e[i].y != fa)
	        tmp[++tot] = f[e[i].y] + e[i].v,flag[tot] = true;
	sort(tmp+1,tmp+tot+1);
	int i = 1,j = tot,now = 0;
	while(i <= j)
	{
		if(tmp[i] >= mid) flag[i] = false,i++,cnt++,now++;
		else if(tmp[j] >= mid) flag[j] = false,j--,cnt++,now++;
		else if(i < j && tmp[i]+tmp[j] >= mid) flag[i] = flag[j] = false,i++,j--,cnt++,now++;
		else i++;
	}
	f[x] = solve(now);
}
bool check()
{
	cnt = 0;
	dfs(1,0);
	if(cnt >= m) return true;
	else return false;
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	init();
	int l = 0,r = 1e9;
	while(l + 1 < r)
	{
		mid = l+r>>1;
		if(check()) l = mid;
		else r = mid;
    }
    mid = r;
	if(check()) printf("%d",r);
	else printf("%d",l);
	return 0;
}
