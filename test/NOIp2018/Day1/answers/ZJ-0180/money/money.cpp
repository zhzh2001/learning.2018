#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int f[50000];
int a[25];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	int i,j,t,n,ans;
	scanf("%d",&t);
	while (t--){
	  scanf("%d",&n);
	  memset(a,0,sizeof(a));
	  memset(f,0,sizeof(f));
	  ans=0;
	  for (i=1;i<=n;i++) scanf("%d",&a[i]);
	  sort(a+1,a+n+1);
	  f[0]=1;
	  for (i=1;i<=n;i++)
	    if (!f[a[i]]){
	    	ans++;
	    	for (j=a[i];j<=a[n];j++) f[j]=f[j-a[i]];
		}   
	  cout<<ans<<endl;	
	}
	fclose(stdin);
	fclose(stdout);
    return 0;
}
		  
