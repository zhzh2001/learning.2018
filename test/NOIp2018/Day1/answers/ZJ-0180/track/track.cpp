#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
struct check{
   int from,to,val,lj;	
}f[30050];
int b[50050]={0};
int main(){
	freopen("track.in","r",stdin);
    freopen("track.out","w",stdout);
	int n,m,i,k,j,ans=0,x,y,z;
	cin>>n>>m;
	memset(f,0,sizeof(f));
	for (i=1;i<=n-1;i++){
		cin>>x>>y>>z;
		f[i].lj++;
		f[i].from=x;
		f[i].to=y;
		f[i].val=z;
	}
	if (m==1){
		for (k=1;k<=n-1;k++)
		  for (i=1;i<=n-1;i++)
		    for (j=1;j<=n-1;j++)
		      if (f[j].lj>0 && f[i].to==f[j].from) b[j]=max(b[j],f[i].val);
			  else if (f[j].lj>0 && f[i].to==f[k].from && f[k].to==f[j].from) b[j]=max(b[j],f[i].val+f[k].val); 
	}
    for (i=1;i<=n-1;i++)
      ans=max(ans,b[i]);
    cout<<ans<<endl;
    fclose(stdin);
	fclose(stdout);
	return 0;
}
