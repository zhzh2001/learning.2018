uses math;
type e=record
  too,next,zh:int64;
  end;
var n,m,x,y,t,z,ans:int64; i:longint;
    b:array[0..30000]of boolean;
    head,p:array[0..60000]of int64;
    edge:array[0..60000]of e;
procedure add(a,b,c:longint);
begin
 inc(t);
 edge[t].too:=b;
 edge[t].zh:=c;
 edge[t].next:=head[a];
 head[a]:=t;
end;
procedure dfs(u,k:int64);
var i,v:int64;
begin
 ans:=max(ans,k);
 i:=head[u];
 while i>0 do
 begin
  v:=edge[i].too;
  if not b[v] then
  begin
   b[v]:=true;
   dfs(v,k+edge[i].zh);
   b[v]:=false;
  end;
  i:=edge[i].next;
 end;
end;
begin
 assign(input,'track.in');
 assign(output,'track.out');
 reset(input);
 rewrite(output);
 read(n,m);
 for i:=2 to n do
 begin
  read(x,y,z);
  inc(p[x]);
  inc(p[y]);
  add(x,y,z);
  add(y,x,z);
 end;
 for i:=n downto 1 do
 begin
  fillchar(b,sizeof(b),false);
  b[i]:=true;
  dfs(i,0);
 end;
 writeln(ans);
 close(input);
 close(output);
end.