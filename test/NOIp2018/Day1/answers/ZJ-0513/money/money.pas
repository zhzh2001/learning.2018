var t,n,i,j,k,ans:longint;
    a:array[0..100]of longint;
    b:array[0..25000]of boolean;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
 assign(input,'money.in');
 assign(output,'money.out');
 reset(input);
 rewrite(output);
 read(t);
 for k:=1 to t do
 begin
  read(n); ans:=n;
  fillchar(b,sizeof(b),false);
  for i:=1 to n do read(a[i]);
  sort(1,n);
  for i:=1 to 25000 do
  begin
   if a[1]*i>a[n] then break;
   b[a[1]*i]:=true;
  end;
  for i:=2 to n do
  begin
   if b[a[i]] then
   begin
    dec(ans);
    continue;
   end;
   if i=n then break;
   for j:=1 to a[n] do
    if b[j] then b[j+a[i]]:=true;
   for j:=1 to 25000 do
   begin
    if a[i]*j>a[n] then break;
    b[a[i]*j]:=true;
   end;
  end;
  writeln(ans);
 end;
 close(input);
 close(output); 
end.