uses math;
var i:longint; ans,x,y,n,sum,l,r,mid,xx,yy:int64;
    a,tree,b,c:array[0..500000]of int64;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=b[(l+r) div 2];
         repeat
           while b[i]<x do
            inc(i);
           while x<b[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=b[i];b[i]:=b[j];b[j]:=y;
                y:=c[i];c[i]:=c[j];c[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
function erfen1(k:int64):int64;
begin
 l:=0; r:=n;
 while l<r do
 begin
  mid:=(l+r+1)div 2;
  if b[mid]<k then l:=mid
  else r:=mid-1;
 end;
 exit(l);
end;
function erfen2(k:int64):int64;
begin
 l:=1; r:=n+1;
 while l<r do
 begin
  mid:=(l+r)div 2;
  if b[mid]>k then r:=mid
  else l:=mid+1;
 end;
 exit(r);
end;
procedure build(nod,l,r:int64);
begin
 if l=r then
 begin
  tree[nod]:=a[l];
  exit;
 end;
 build(nod*2,l,(l+r)div 2);
 build(nod*2+1,(l+r)div 2+1,r);
 tree[nod]:=min(tree[nod*2],tree[nod*2+1]);
end;
function find(nod,l,r,ll,rr:int64):int64;
begin
 if (l=ll)and(r=rr) then exit(tree[nod]);
 if rr<=(l+r)div 2 then exit(find(nod*2,l,(l+r)div 2,ll,rr));
 if ll>(l+r)div 2 then exit(find(nod*2+1,(l+r)div 2+1,r,ll,rr));
 exit(min(find(nod*2,l,(l+r)div 2,ll,(l+r)div 2),
 find(nod*2+1,(l+r)div 2+1,r,(l+r)div 2+1,rr)));
end;
procedure solve(l,r,k:int64);
var w:int64; i:longint;
begin
 //writeln(l,' ',r,' ',k,' ',ans);
 if l>r then exit;
 if l=r then
 begin
  if a[l]>k then ans:=ans+a[l]-k;
  exit;
 end;
 sum:=find(1,1,n,l,r);
 xx:=erfen1(sum);
 yy:=erfen2(sum);
  //writeln('########');
 for i:=xx+1 to yy-1 do
  if (c[i]<=r)and(c[i]>=l) then
  begin
   w:=c[i];
   break;
  end;
 if a[w]<=k then
 begin
  solve(l,w-1,k);
  solve(w+1,r,k);
 end
 else begin
  ans:=ans+a[w]-k;
  solve(l,w-1,a[w]);
  solve(w+1,r,a[w]);
 end;
end;
begin
 assign(input,'road.in');
 assign(output,'road.out');
 reset(input);
 rewrite(output);
 read(n);
 for i:=1 to n do
 begin
  read(a[i]);
  b[i]:=a[i];
  c[i]:=i;
 end;
 sort(1,n);
 build(1,1,n);
 solve(1,n,0);
 writeln(ans);
 close(input);
 close(output);
end.