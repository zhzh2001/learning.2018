#include <cstdio>
#include <cstdlib>

int n;
int all[112345];

namespace seg {
	int val[812345], pos[812345], add[812345];
	void build(int n, int left, int right) {
		add[n] = 0;
		if (left == right) {
			val[n] = all[left];
			pos[n] = left;
			return;
		}
		int mid(left + right >> 1);
		build(n << 1, left, mid);
		build(n << 1 | 1, mid + 1, right);
		val[n] = val[n << 1]; pos[n] = pos[n << 1];
		if (val[n] > val[n << 1 | 1]) {
			val[n] = val[n << 1 | 1]; pos[n] = pos[n << 1 | 1];
		}
	}
	inline void push_down(int n) {
		add[n << 1] += add[n];
		add[n << 1 | 1] += add[n];
		val[n] += add[n];
		add[n] = 0;
	}
	void quary(int n, int left, int right, int l, int r, int& min, int& p) {
		push_down(n);
		if (left == l && right == r) {
			if (val[n] < min) {
				min = val[n];
				p = pos[n];
			}
			return;
		}
		int mid(left + right >> 1);
		if (mid >= r) quary(n << 1, left, mid, l, r, min, p);
		else if (mid < l) quary(n << 1 | 1, mid + 1, right, l, r, min, p);
		else {
			quary(n << 1, left, mid, l, mid, min, p);
			quary(n << 1 | 1, mid + 1, right, mid + 1, r, min, p);
		}
	}
	void modify(int n, int left, int right, int l, int r, int v) {
		if (left == l && right == r) {
			add[n] += v;
			push_down(n);
			return;
		}
		int mid(left + right >> 1);
		if (mid >= r) modify(n << 1, left, mid, l, r, v);
		else if (mid < l) modify(n << 1 | 1, mid + 1, right, l, r, v);
		else {
			modify(n << 1, left, mid, l, mid, v);
			modify(n << 1 | 1, mid + 1, right, mid + 1, r, v);
		}
		push_down(n << 1); push_down(n << 1 | 1);
		val[n] = val[n << 1]; pos[n] = pos[n << 1];
		if (val[n] > val[n << 1 | 1]) {
			val[n] = val[n << 1 | 1];
			pos[n] = pos[n << 1 | 1];
		} 
	}
}

long long ans(0);
void dfs(int left, int right) {
	if (left > right) return;
	int min(112345), pos;
	seg::quary(1, 1, n, left, right, min, pos);
	ans += min;
	seg::modify(1, 1, n, left, right, -min);
	dfs(left, pos - 1);
	dfs(pos + 1, right);
}

int main() {
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	scanf("%d", &n);
	for (int i(1); i <= n; ++i) {
		scanf("%d", &all[i]);
	}
	seg::build(1, 1, n);
	dfs(1, n);
	printf("%lld\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
