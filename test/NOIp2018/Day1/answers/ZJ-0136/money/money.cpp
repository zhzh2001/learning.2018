#include <cstdio>
#include <cstdlib>

#include <algorithm>

int dp[25123], NextVis;
bool removed[1123];
int T, n, all[1123];
int main() {
	freopen("money.in", "r", stdin);
	freopen("money.out", "w", stdout);
	scanf("%d", &T);
	while (T--) {
		scanf("%d", &n);
		for (int i(0); i != n; ++i) scanf("%d", &all[i]);
		std::sort(all, all + n);
		++NextVis;
		dp[0] = NextVis;
		for (int i(0); i != n; ++i) {
			if (dp[all[i]] == NextVis) {
				removed[i] = true;
			} else {
				removed[i] = false;
			}
			for (int j(all[i]); j < 25123; ++j) {
				if (dp[j - all[i]] == NextVis)
					dp[j] = NextVis;
			}
		}
		int cnt(0);
		for (int i(0); i != n; ++i) {
			if (!removed[i]) ++cnt;
		}
		printf("%d\n", cnt);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
