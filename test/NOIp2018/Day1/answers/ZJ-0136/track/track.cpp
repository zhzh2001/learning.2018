#include <cstdio>
#include <cstdlib>

#include <algorithm>

namespace my {
	template <class T>
	class queue {
		public:
			queue() : beg(0), end(0) {
				
			}
			bool empty() const {
				return beg == end;
			}
			void push(const T& x) {
				all[end++] = x;
			}
			T pop() {
				return all[beg++];
			}
			void clear() {
				beg = end = 0;
			}
			int beg, end;
			T all[112345];
	};
	class edge {
	public:
		int to, len, next;
	}all[112345];
	int head[51234], end(2);
	inline void add(int fr, int to, int len) {
		all[end].to = to;
		all[end].len = len;
		all[end].next = head[fr];
		head[fr] = end++;
	}
	int n, m;
	int bVis[51234], pre[51234], NextVis;
	int d[51234], cntd;
	bool ind[51234];
	int find_d() {
		int maxdep(0), pos(1);
		queue<int> que, dep;
		que.push(1); dep.push(0);
		bVis[1] = ++NextVis;
		while (!que.empty()) {
			int now(que.pop()), d(dep.pop());
			if (d > maxdep) {
				maxdep = d;
				pos = now;
			}
			for (int i(head[now]); i; i = all[i].next) {
				if (bVis[all[i].to] != NextVis) {
					que.push(all[i].to);
					dep.push(d + all[i].len);
					bVis[all[i].to] = NextVis;
				}
			}
		}
		que.clear(); dep.clear();
		que.push(pos); dep.push(0);
		maxdep = 0;
		bVis[pos] = ++NextVis;
		while (!que.empty()) {
			int now(que.pop()), d(dep.pop());
			if (d > maxdep) {
				maxdep = d;
				pos = now;
			}
			for (int i(head[now]); i; i = all[i].next) {
				if (bVis[all[i].to] != NextVis) {
					que.push(all[i].to);
					dep.push(d + all[i].len);
					bVis[all[i].to] = NextVis;
					pre[all[i].to] = now;
				}
			}
		}
		while (pos != 0) {
			d[cntd++] = pos;
			ind[pos] = true;
			pos = pre[pos];
		}
		return maxdep;
	}
	int maxlen[51234];
	int maxhead[51234];
	void dfs(int now, int fr) {
		maxlen[now] = 0;
		for (int i(head[now]); i; i = all[i].next) {
			if (all[i].to != fr) {
				dfs(all[i].to, now);
				if (maxlen[all[i].to] > maxlen[now]) {
					maxlen[now] = maxlen[all[i].to];
					maxhead[now] = maxhead[all[i].to];
				}
			}
		}
	}
	void init_maxlen() {
		for (int i(0); i != cntd; ++i) {
			for (int j(head[d[i]]); j; j = all[j].next) {
				dfs(all[j].to, d[i]);
			}
		}
	}
	bool all1(true), all2(true);
	int len[51234], endlen;
	void solve1() {
		for (int i(head[1]); i; i = all[i].next) {
			len[endlen++] = all[i].len;
		}
		std::sort(len, len + endlen);
		int left(0), right(21234), ans;
		while (right >= left) {
			int mid(left + right >> 1);
			int back(endlen - 1), cnt(0);
			for (int i(0); i < back; ++i) {
				if (len[i] + len[back] >= mid) {
					++cnt;
					--back;
				}
			}
			if (len[back] >= mid) ++cnt;
			if (cnt >= m) {
				ans = mid;
				left = mid + 1;
			} else {
				right = mid - 1;
			}
		}
		printf("%d\n", ans);
	}
	void solve2() {
		int beg;
		for (int i(1); i <= n; ++i) {
			if (all[head[i]].next == 0) {
				beg = i;
				break;
			}
		}
		int left(0), right(5e8), ans;
		while (left <= right) {
			int mid(left + right >> 1);
			int now(beg), front(-1);
			int cnt(0), l(0);
			while (all[head[now]].next || now == beg) {
				for (int i(head[now]); i; i = all[i].next) {
					if (all[i].to != front) {
						now = all[i].to;
						l += all[i].len;
						if (l >= mid) {
							l = 0;
							++cnt;
						}
						front = now;
						break;
					}
				}
			}
			if (cnt >= m) {
				left = mid + 1;
				ans = mid;
			} else {
				right = mid - 1;
			}
		}
		printf("%d\n", ans);
	}
	int forbid[51234];
	void solve3() {
		find_d();
		init_maxlen();
		int left(0), right(5e8), mid, ans(0);
		while (left <= right) {
			int mid(left + right >> 1);
			if (mid == 3) {
				mid = 3;
			}
			++NextVis;
			int now(d[0]), cnt(0), len(0), first(1);
			while (first <= cntd) {
				int old(now);
				for (int i(head[now]); i; i = all[i].next) {
					if (!ind[all[i].to] && forbid[all[i].to] != NextVis) {
						if (len + maxlen[all[i].to] >= mid) {
							forbid[all[i].to] = NextVis;
							++cnt;
							len = 0;
							int temp(0);
							now = -1;
							for (int j(head[i]); j; j = all[j].next) {
								if (!ind[all[j].to] && forbid[all[j].to] != NextVis && maxlen[all[j].to] > temp) {
									temp = maxlen[all[j].to];
									now = all[j].to;
								}
							}
							if (now == -1) {
								now = d[first++];
							}
							break;
						}
					}
				}
				if (now == old) {
					for (int j(head[now]); j; j = all[j].next) {
						if (ind[all[j].to]) {
							now = all[j].to;
							len += all[j].len;
							++first;
							break;
						}
					}
				}
				if (len >= mid) {
					for (int j(head[now]); j; j = all[j].next) {
						if (all[j].to == d[first]) {
							len = all[j].len;
							break;
						}
					}
					now = d[first++];
					++cnt;
				}
			}
			if (cnt >= mid) {
				ans = mid;
				left = mid + 1;
			} else {
				right = mid - 1;
			}
		}
		printf("%d\n", ans);
	}
	void MaiN() {
		freopen("track.in", "r", stdin);
		freopen("track.out", "w", stdout);
		scanf("%d%d", &n, &m);
		for (int i(1); i != n; ++i) {
			int f, t, l;
			scanf("%d%d%d", &f, &t, &l);
			add(f, t, l);
			add(t, f, l);
			if (f + 1 != t) all2 = false;
			if (f != 1) all1 = false;
		}
		if (m == 1) {
			printf("%d\n", find_d());
		} else if (all1) {
			solve1();
		} else if (all2) {
			solve2();
		} else {
			solve3();
		}
		fclose(stdin);
		fclose(stdout);
	}
}
int main() {
	my::MaiN();
	return 0;
}
/*
4
2
1 2 2
2 3 3
2 4 3
*/
