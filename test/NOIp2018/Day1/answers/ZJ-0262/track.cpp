#include <bits/stdc++.h>

#define rep(i , l , r) for(int i = (l) , ___ = (r) ; i <= ___ ; ++i)
#define per(i , r , l) for(int i = (r) , ___ = (l) ; i >= ___ ; --i)

using namespace std;

template<typename T> inline T read(T &f)
{
	f = 0 ; int x = 1 ; char c = getchar();
	while(!isdigit(c))
		x = c == '-' ? -1 : 1 , c = getchar();
	while(isdigit(c))
		(f *= 10) += c & 15 , c = getchar();
	return f *= x;
}

template<typename T> inline int chkmin(T &x , const T &y) { return x > y ? x = y , 1 : 0; }
template<typename T> inline int chkmax(T &x , const T &y) { return x < y ? x = y , 1 : 0; }

const int N = 50000 + 5;

int n , m , u , v , w;

struct Edge
{
	int to , w; Edge *nxt;
} E[N<<1] , *pre[N];

void addedge(int u , int v , int w)
{
	static int tot = 0 ;
	E[++tot] = (Edge){v , w , pre[u]} , pre[u] = E + tot;
	E[++tot] = (Edge){u , w , pre[v]} , pre[v] = E + tot;
}

// simple
namespace task0
{
	const int GN = 20 + 5;
	
	int id[GN] , sum[GN] , vis[GN] , fa[GN][6] , dep[GN] , Ans;
	
	void dfs(int u , int f)
	{
		static int dc = 0;
		dep[u] = dep[f] + 1 , fa[u][0] = f;
		rep(i , 1 , 5)
			fa[u][i] = fa[fa[u][i - 1]][i - 1];
		for(Edge *i = pre[u] ; i ; i = i->nxt)
		{
			int v = i->to;
			if(v == f) continue ;
			sum[v] = sum[u] + i->w;
			id[v] = ++dc , dfs(v , u);
		}
	}
	
	int lca(int u , int v)
	{
		if(dep[u] < dep[v]) swap(u , v);
		int d = dep[u] - dep[v];
		for(int i = 0 ; d ; d >>= 1 , ++i)
		{
			if(d & 1) u = fa[u][i];
		}
		if(u == v) return u;
		per(i , 5 , 0) if(fa[u][i] != fa[v][i])
		{
			u = fa[u][i] , v = fa[v][i];
		}
		return fa[u][0];
	}
	
	int check(int x , int y)
	{
		int d = lca(x , y);
		while(x != d)
		{
			if(vis[id[x]]) return 0;
			x = fa[x][0];
		}
		while(y != d)
		{
			if(vis[id[y]]) return 0;
			y = fa[y][0];
		}
		return 1;
	}
	
	void modify(int x , int y , int v)
	{
		int d = lca(x , y);
		while(x != d)
		{
			vis[id[x]] = v;
			x = fa[x][0];
		}
		while(y != d)
		{
			vis[id[y]] = v;
			y = fa[y][0];
		}
	}
	
	void solve(int cur , int mn)
	{
		if(cur == m)
		{
			chkmax(Ans , mn);
			return ;
		}
		rep(i , 1 , n)
		rep(j , 1 , n)
		{
			if(i == j) continue;
			if(check(i , j) == 0) continue ;
			int d = sum[i] + sum[j] - 2 * sum[lca(i , j)];
			modify(i , j , 1);
			if(mn == -1) solve(cur + 1 , d);
			else solve(cur + 1 , min(mn , d));
			modify(i , j , 0);
		}
	}
	
	void main()
	{
		dfs(1 , 0) , solve(0 , -1);
		cout << Ans << endl;
	}
}

// m = 1
namespace task1
{
	int dis[N] , rt;
	void dfs(int u , int fa)
	{
		if(dis[rt] < dis[u]) rt = u;
		for(Edge *i = pre[u] ; i ; i = i->nxt)
		{
			int v = i->to;
			if(v == fa) continue ;
			dis[v] = dis[u] + i->w , dfs(v , u);
		}
	}
	void main()
	{
		rt = 1 , dfs(1 , 0);
		dis[rt] = 0 , dfs(rt , 0);
		printf("%d\n" , dis[rt]);
	}
}

int val[N];

// chain
namespace task2
{
	int Ans;
	
	void dfs(int u , int fa)
	{
		static int dc = 0;
		for(Edge *i = pre[u] ; i ; i = i->nxt)
		{
			int v = i->to;
			if(v == fa) continue ;
			val[++dc] = i->w , dfs(v , u);
		}
	}
	
	int check(int v)
	{
		int cnt = 0 , s = 0;
		rep(i , 1 , n)
		{
			if(val[i] >= v)
			{
				cnt++;
				continue ;
			}
			if(s + val[i] >= v)
			{
				s = 0 , cnt++;
				continue;
			}
			s += val[i];
		}
		// if(s) cnt++;
		return cnt >= m;
	}
	
	void main()
	{
		dfs(1 , 0) , n--;
		int l = N , r = 0; 
		rep(i , 1 , n) chkmin(l , val[i]) , r += val[i];
		int mid = 0;
		while(l <= r)
		{
			mid = (l + r) >> 1;
			if(check(mid))
				Ans = mid , l = mid + 1;
			else
				r = mid - 1;
		}
		cout << Ans << endl;
	}
}

namespace task3
{
	int Ans;
	
	int comp(const Edge &x , const Edge &y)
	{
		return x.w < y.w;
	}
	
	int check(int v)
	{
		int cnt = 0 , s = 0;
		rep(i , 1 , n)
		{
			if(val[i] >= v)
			{
				cnt++;
				continue ;
			}
			if(s + val[i] >= v)
			{
				s = 0 , cnt++;
				continue;
			}
			s += val[i];
		}
		return cnt >= m;
	}
	
	void main()
	{
		n--;
		rep(i , 1 , n) val[i] = E[2 * i].w;
		sort(val + 1 , val + 1 + n);
		int l = n - m , r = l + 1;
		while(l >= 1 && r <= n)
		{
			val[r] += val[l];
			r++ , l--;
		}
		r = n - m + 1;
		Ans = val[r];
		rep(i , r , n) chkmin(Ans , val[i]);
		cout << Ans << endl;
	}
}

namespace task4
{
	const int GN = N;
	
	int id[GN] , sum[GN] , vis[GN] , fa[GN][21] , dep[GN] , Ans;
	
	void dfs(int u , int f)
	{
		static int dc = 0;
		dep[u] = dep[f] + 1 , fa[u][0] = f;
		rep(i , 1 , 20)
			fa[u][i] = fa[fa[u][i - 1]][i - 1];
		for(Edge *i = pre[u] ; i ; i = i->nxt)
		{
			int v = i->to;
			if(v == f) continue ;
			sum[v] = sum[u] + i->w;
			id[v] = ++dc , dfs(v , u);
		}
	}
	
	int lca(int u , int v)
	{
		if(dep[u] < dep[v]) swap(u , v);
		int d = dep[u] - dep[v];
		for(int i = 0 ; d ; d >>= 1 , ++i)
		{
			if(d & 1) u = fa[u][i];
		}
		if(u == v) return u;
		per(i , 20 , 0) if(fa[u][i] != fa[v][i])
		{
			u = fa[u][i] , v = fa[v][i];
		}
		return fa[u][0];
	}
	
	int check(int x , int y)
	{
		int d = lca(x , y);
		while(x != d)
		{
			if(vis[id[x]]) return 0;
			x = fa[x][0];
		}
		while(y != d)
		{
			if(vis[id[y]]) return 0;
			y = fa[y][0];
		}
		return 1;
	}
	
	void modify(int x , int y , int v)
	{
		int d = lca(x , y);
		while(x != d)
		{
			vis[id[x]] = v;
			x = fa[x][0];
		}
		while(y != d)
		{
			vis[id[y]] = v;
			y = fa[y][0];
		}
	}
	
	void make()
	{
		rep(i , 1 , n) vis[i] = 0;
		int mn = -1;
		rep(i , 1 , m)
		{
			int l = rand() % n + 1 , r = rand() % n + 1;
			while(l == r || check(l , r) == 0)
			{
				l = rand() % n + 1;
				r = rand() % n + 1;
			}
			modify(l , r , 1);
			int d = sum[l] + sum[r] - 2 * sum[lca(l , r)];
			if(mn == -1) mn = d;
			else chkmin(mn , d);
		}
		chkmax(Ans , mn);
	}
	
	void main()
	{
		srand(n + m + E[1].w) , dfs(1 , 0);
		int ITER = 250000 / n;
		rep(i , 1 , ITER) make();
		cout << Ans << endl;
	}
}

int f2 = 1 , f3 = 1 , f4 = 1;

int deg[N] , mn;

int main()
{
	freopen("track.in" , "r" , stdin);
	freopen("track.out" , "w" , stdout);
	read(n) , read(m);
	rep(i , 1 , n - 1)
	{
		read(u) , read(v) , read(w);
		if(v != u + 1) f2 = 0;
		if(u != 1) f3 = 0;
		addedge(u , v , w);
		deg[u]++ , deg[v]++;
		if(deg[u] > 3 || deg[v] > 3) f4 = 0;
	}
	// task4::main();
//	// task0::main();
	if(m == 1)  task1::main();
	else if(f2) task2::main();
	else if(f3) task3::main();
	else if(m == n - 1) printf("%d\n" , mn);
	// else if(f4) task4::main();
	else task4::main();
	return 0;
}

/*
5 3
1 2 3
1 3 4
1 4 5
1 5 6
*/
