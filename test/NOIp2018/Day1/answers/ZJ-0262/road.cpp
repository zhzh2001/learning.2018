#include <bits/stdc++.h>

#define rep(i , l , r) for(int i = (l) , ___ = (r) ; i <= ___ ; ++i)
#define per(i , r , l) for(int i = (r) , ___ = (l) ; i >= ___ ; --i)

using namespace std;

template<typename T> inline T read(T &f)
{
	f = 0 ; int x = 1 ; char c = getchar();
	while(!isdigit(c))
		x = c == '-' ? -1 : 1 , c = getchar();
	while(isdigit(c))
		(f *= 10) += c & 15 , c = getchar();
	return f *= x;
}

template<typename T> inline int chkmin(T &x , const T &y) { return x > y ? x = y , 1 : 0; }
template<typename T> inline int chkmax(T &x , const T &y) { return x < y ? x = y , 1 : 0; }

const int N = 100000 + 5;

int n , A[N] , Ans;

int main()
{
	freopen("road.in" , "r" , stdin);
	freopen("road.out" , "w" , stdout);
	read(n);
	rep(i , 1 , n)
	{
		read(A[i]) ;
		if(A[i] > A[i - 1]) Ans += A[i] - A[i - 1];
	}
	cout << Ans << endl;
	return 0;
}
