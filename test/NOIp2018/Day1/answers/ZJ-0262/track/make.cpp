#include <bits/stdc++.h>

#define rep(i , l , r) for(int i = (l) , ___ = (r) ; i <= ___ ; ++i)
#define per(i , r , l) for(int i = (r) , ___ = (l) ; i >= ___ ; --i)

using namespace std;

template<typename T> inline T read(T &f)
{
	f = 0 ; int x = 1 ; char c = getchar();
	while(!isdigit(c))
		x = c == '-' ? -1 : 1 , c = getchar();
	while(isdigit(c))
		(f *= 10) += c & 15 , c = getchar();
	return f *= x;
}

template<typename T> inline int chkmin(T &x , const T &y) { return x > y ? x = y , 1 : 0; }
template<typename T> inline int chkmax(T &x , const T &y) { return x < y ? x = y , 1 : 0; }

int n , m;

namespace chain
{
	void main()
	{
		rep(i , 2 , n)
		{
			cout << i - 1 << ' ' << i << ' ' << rand() << endl;
		}
	}
}

namespace juhua
{
	void main()
	{
		rep(i , 2 , n)
		{
			cout << 1 << ' ' << i << ' ' << rand() << endl;
		}
	}
}

int main()
{
	srand(time(NULL));
	n = rand() % 10 + 1 , m = rand() % n + 1;
	cout << n << ' ' << m << endl;
	// chain::main();
	juhua::main();
	return 0;
}

