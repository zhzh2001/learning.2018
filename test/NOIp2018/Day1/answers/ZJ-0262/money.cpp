#include <bits/stdc++.h>

#define rep(i , l , r) for(int i = (l) , ___ = (r) ; i <= ___ ; ++i)
#define per(i , r , l) for(int i = (r) , ___ = (l) ; i >= ___ ; --i)

using namespace std;

template<typename T> inline T read(T &f)
{
	f = 0 ; int x = 1 ; char c = getchar();
	while(!isdigit(c))
		x = c == '-' ? -1 : 1 , c = getchar();
	while(isdigit(c))
		(f *= 10) += c & 15 , c = getchar();
	return f *= x;
}

template<typename T> inline int chkmin(T &x , const T &y) { return x > y ? x = y , 1 : 0; }
template<typename T> inline int chkmax(T &x , const T &y) { return x < y ? x = y , 1 : 0; }

const int N = 100 + 5 , M = 25000 + 5;

int T , n , A[N]; bool f[M];

namespace task
{
	void main()
	{
		read(n);
		rep(i , 1 , n) read(A[i]);
		sort(A + 1 , A + 1 + n);
		int Ans = 0;
		f[0] = 1;
		rep(i , 1 , n)
		{
			if(f[A[i]]) continue;
			rep(j , A[i] , A[n])
			{
				f[j] |= f[j - A[i]];
			}
			Ans++;
		}
		printf("%d\n" , Ans);
		rep(i , 0 , A[n]) f[i] = 0;
	}
}

int main()
{
	freopen("money.in" , "r" , stdin);
	freopen("money.out" , "w" , stdout);
	read(T);
	while(T--) task::main();
	return 0;
}

