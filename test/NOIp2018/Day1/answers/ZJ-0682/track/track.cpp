#include <iostream>
#include <cstdio>
#include <algorithm>
#include <set>
#include <vector>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline void kkk(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
}
const ll N=1e5+10;
ll head[N],edge,n,k,l,r,mid,num;
struct node{ll to,net,pay;}e[N<<1];
inline void addedge(ll x,ll y,ll z){
	e[++edge].to=y,e[edge].net=head[x],head[x]=edge,e[edge].pay=z;
	e[++edge].to=x,e[edge].net=head[y],head[y]=edge,e[edge].pay=z;
}
inline ll dfs(ll x,ll fa){
	multiset<ll>S;
	vector<ll>s;
	for (ll i=head[x];i;i=e[i].net){
		if (e[i].to==fa) continue;
		ll k=dfs(e[i].to,x)+e[i].pay;
		s.push_back(k);
	}
	sort(s.begin(),s.end());
	set<ll>::iterator it2;
	for (ll i=0;i<(ll)s.size();++i){
		if (s[i]>=mid){
			++num;
			continue;
		}
		it2=S.lower_bound(mid-s[i]);
		if (it2!=S.end()){
			S.erase(it2);
			++num;
		}else{
			S.insert(s[i]);
		}
	}
	if (S.empty()) return 0;
	return *(--S.end());
}
inline void solve(){
	num=0;
	dfs(1,0);
}
int main(){
	kkk();
	read(n),read(k);
	bool flag=1;
	ll t1=0,t2=0;
	for (ll i=1,x,y,z;i<n;++i){
		read(x),read(y),read(z);
		if (x!=1) flag=0;
		addedge(x,y,z);
		r+=z;
		if (z>t1) t2=t1,t1=z;
		else t2=max(t2,z);
	}
	if (flag&&k==1){
		wr(t1+t2);
		return 0;
	}
	r/=k;
	for (;l<r;){
		mid=(l+r)>>1;
		solve();
		if (num>=k) l=mid+1;
		else r=mid;
	}
	wr(r-1);
	return 0;
}
/*
好像是树形DP+二分答案 
试着打一个吧。。。 
别忘了边数*2  
*/
