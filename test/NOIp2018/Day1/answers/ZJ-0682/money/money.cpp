#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline void kkk(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
}
const ll N=25010;
ll n,a[N],T,ans,Max;
bool f[N],g[N];
/*inline void exgcd(ll a,ll b,ll &x,ll &y){
	if (!b){
		x=1,y=0;
		return;
	}
	exgcd(b,a%b,y,x);
	y-=a/b*x;
}
inline ll gcd(ll a,ll b){return b?gcd(b,a%b):a;}
inline bool check(ll a,ll b,ll z){
	if (z>a*b-a-b) return 1;
	ll x,y,t=gcd(a,b);
	a/=t,b/=t;
	if (z%t!=0) return 0;
	z/=t;
	exgcd(a,b,x,y);
	x*=z,y*=z;
	while (x<0) x+=b,y-=a;
	while (y<0) x-=b,y+=a;
//	cout<<a*t<<' '<<b*t<<' '<<x<<' '<<y<<' '<<z*t<<endl;
	return x>=0&&y>=0;
}*/
int main(){
	kkk();
	for (read(T);T;--T){
		read(n);
		Max=0;
		for (ll i=1;i<=n;++i) read(a[i]),Max=max(Max,a[i]);
		memset(f,0,sizeof f);
		memset(g,0,sizeof g);
		f[0]=1;
		for (ll i=1;i<=n;++i){
			for (ll j=a[i];j<=Max;++j)
				f[j]|=f[j-a[i]];
		}
		g[0]=1;
		ans=0;
		for (ll i=1;i<=Max;++i)
			if (f[i]!=g[i]){
				++ans;
				for (ll j=i;j<=Max;++j)
					g[j]|=g[j-i];
			}
		wr(ans);puts("");
	}
	return 0;
}
/*
1
28
21 26 49 54 47 39 38 15 30 24 29 35 42 22 43 55 17 37 51 13 48 45 36 11 52 53 8 50
���Ӷȣ�
O(n*Max*T)  
*/
