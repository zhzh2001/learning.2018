#include<bits/stdc++.h>
#define M 100005
#define ll long long
using namespace std;
int n,m,tot;
int head[M];
struct edge{
	int t,c,nxt;
}E[M<<1];
void addedge(int f,int t,int c){
	E[++tot]=(edge){t,c,head[f]};
	head[f]=tot;
}
struct P20{
	int d,dlen;
	void dfs(int x,int f,int D){
		if(D>dlen) d=x,dlen=D;
		for(int i=head[x];i;i=E[i].nxt){
			int v=E[i].t,c=E[i].c;
			if(v==f) continue;
			dfs(v,x,D+c);
		}
	}
	void solve(){
		dlen=-1,dfs(1,0,0);
		dlen=-1,dfs(d,0,0);
		printf("%d\n",dlen);
	}
}D;
struct Pxx{
	int cnt,limit,tmp[1005][1005];
	bool mark[1005][1005];
	int dfs(int x,int f){
		int sz=0;
		for(int i=head[x];i;i=E[i].nxt){
			int v=E[i].t;
			if(v==f) continue;
			tmp[x][++sz]=dfs(v,x)+E[i].c;
		}
		if(sz==0) return 0;
		sort(tmp[x]+1,tmp[x]+sz+1);
		//for(int i=1;i<=sz;i++) printf("%d ",tmp[x][i]);puts("");
		int L=1,R=sz;
		while(R>=1&&tmp[x][R]>=limit) cnt++,mark[x][R]=1,R--;
		for(;R>L;R--){
			while(L<R&&tmp[x][L]+tmp[x][R]<limit) L++;
			if(L==R||tmp[x][L]+tmp[x][R]<limit) break;
			mark[x][L]=mark[x][R]=1,cnt++;L++;
		}
		for(int i=sz;i;i--) if(!mark[x][i]) return tmp[x][i];
		return 0;
	}
	bool check(int x){
		memset(mark,0,sizeof(mark));
		memset(tmp,0,sizeof(tmp));
		limit=x,cnt=0;
		dfs(1,0);
		return cnt>=m;
	}
	void solve(){
		int L=1,R=1e9,ans=-1;
		while(L<=R){
			int mid=L+R>>1;
			if(check(mid)) ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}C;

struct Pshui{
	int cnt,limit,tmp[M][4];
	bool mark[M][4];
	int dfs(int x,int f){
		int sz=0;
		for(int i=head[x];i;i=E[i].nxt){
			int v=E[i].t,c=E[i].c;
			if(v==f) continue;
			tmp[x][++sz]=dfs(v,x)+c;
		}
		if(sz==0) return 0;
		sort(tmp[x]+1,tmp[x]+sz+1);
		int L=1,R=sz;
		while(R>=1&&tmp[x][R]>=limit) cnt++,mark[x][R]=1,R--;
		for(;R>L;R--){
			while(L<R&&tmp[x][L]+tmp[x][R]<limit) L++;
			if(L==R||tmp[x][L]+tmp[x][R]<limit) break;
			mark[x][L]=mark[x][R]=1,cnt++;L++;
		}
		for(int i=sz;i;i--) if(!mark[x][i]) return tmp[x][i];
		return 0;
	}
	bool check(int x){
		memset(mark,0,sizeof(mark));
		memset(tmp,0,sizeof(tmp));
		limit=x,cnt=0;
		dfs(1,0);
		return cnt>=m;
	}
	void solve(){
		int L=1,R=1e9,ans=-1;
		while(L<=R){
			int mid=L+R>>1;
			if(check(mid)) ans=mid,L=mid+1;
			else R=mid-1;
		}
		printf("%d\n",ans);
	}
}A;

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1,f,t,c;i<n;i++){
		scanf("%d%d%d",&f,&t,&c);
		addedge(f,t,c);
		addedge(t,f,c);
	}
	if(m==1) D.solve();
	else if(n<=1000) C.solve();
	else A.solve();
	return 0;
}
