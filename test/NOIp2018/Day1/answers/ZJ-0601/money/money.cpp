#include<bits/stdc++.h>
#define M 105
#define ll long long
using namespace std;
int n,a[M],T;
struct Pxx{
	int mx;
	bool mark[25005];
	void Mark(int x){
		for(int i=0;i<=mx-x;i++)
			if(mark[i]) mark[i+x]=1;
	}
	void solve(){
		memset(mark,0,sizeof(mark));
		mark[0]=1,sort(a+1,a+n+1);
		mx=a[n];
		int cnt=1;
		Mark(a[1]);
		for(int i=2;i<=n;i++){
			if(!mark[a[i]]) Mark(a[i]),cnt++;
		}
		printf("%d\n",cnt);
	}
}C;
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++) scanf("%d",&a[i]);
		C.solve();
	}
	return 0;
}
