#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;
const int NN = 1e5+5, MM = 2e5+5;
int n, m, u, v, w, Head[NN], Next[MM], vet[MM], val[MM], edgenum;
bool Chrysanthemum = 1, Chain = 1;
inline int Read() {
	int s=0, w=1;
	char ch=getchar();
	while (ch<'0' || ch>'9') {
		if (ch == '-') w = -w;
		ch=getchar();
	}
	while (ch>='0' && ch<='9')
		s = s*10 + ch-'0', ch=getchar();
	return s*w;
}
inline void add(int u, int v, int w) {
	Next[++edgenum] = Head[u];
	Head[u] = edgenum;
	vet[edgenum] = v;
	val[edgenum] = w;
}
namespace task1 {
	const int N = 20;
	int father[N], edge[N], len[N][N], ans;
	bool flag[N][N][N], vis[N], f[N];
	void dfs(int u, int fa) {
		father[u] = fa;
		for (int e=Head[u]; e; e=Next[e]) {
			int v=vet[e], w=val[e];
			if (v ^ fa)
				edge[v] = w, dfs(v, u);
		}
	}
	inline void Mark(int u, int v) {
		int U = u, V = v;
		for (int i=1; i<=n; i++) vis[i] = 0;
		for (int i=u; i; i=father[i]) vis[i] = 1;
		for (; !vis[v]; v=father[v]) flag[U][V][v] = 1, len[U][V] += edge[v];
		for (; u^v; u=father[u]) flag[U][V][u] = 1, len[U][V] += edge[u];
	}
	bool chk(bool *x, bool *y) {
		for (int i=1; i<=n; i++)
			if (x[i] && y[i]) return 0;
		return 1;
	}
	void OR(bool *x, bool *y) {
		for (int i=1; i<=n; i++)
			x[i] |= y[i];
	}
	void XOR(bool *x, bool *y) {
		for (int i=1; i<=n; i++)
			x[i] ^= y[i];
	}
	inline void DFS(int t, int Min) {
		if (Min <= ans) return ;
		if (t > m) {
			ans = max(ans, Min);
			return ;
		}
		for (int i=1; i<=n; i++)
			for (int j=i+1; j<=n; j++)
				if (chk(flag[i][j], f)) {
					OR(f, flag[i][j]);
					DFS(t+1, min(Min, len[i][j]));
					XOR(f, flag[i][j]);
				}
	}
	inline void solve() {
		dfs(1, 0);
		for (int i=1; i<=n; i++)
			for (int j=i+1; j<=n; j++)
				Mark(i, j);
		DFS(1, 1e9);
		printf ("%d\n", ans);
	}
}
namespace task2 {
	int ans;
	int treeDP(int u, int fa) {
		int Fmax = 0, Smax = 0;
		for (int e=Head[u]; e; e=Next[e]) {
			int v=vet[e], w=val[e];
			if (v == fa) continue;
			int t = treeDP(v, u) + w;
			if (t >= Fmax) Smax = Fmax, Fmax = t;
			else if (t > Smax) Smax = t;
		} ans = max(ans, Fmax + Smax);
		return Fmax;
	}
	inline void solve() {
		treeDP(1, 0);
		printf ("%d\n", ans);
	}
}
namespace task3 {
	const int N = 1e5+5;
	int l, r, a[N]; bool used[N];
	bool chk(int x) {
		for (int i=1; i<n; i++)
			used[i] = 0;
		int R = lower_bound(a+1, a+n, x) - a - 1, 
			L = lower_bound(a+1, a+n, x - a[R]) - a, 
			rem = m - (n - 1 - R);
		if (rem <= 0) return 1;
		if (n - L < m) return 0;
		for (int i=1; i<=rem; i++) {
			if (L >= R) return 0;
			while (used[R]) R--;
			while (used[L] || a[L] + a[R] < x) L++;
			if (L >= R) return 0;
			used[L] = used[R] = 1;
		} return 1;
	}
	inline void solve() {
		for (int i=2; i<=n; i++)
			a[i-1] = val[Head[i]];
		sort(a+1, a+n);
		l = 1, r = a[n-1] + a[n-2];
		while (l < r) {
			int mid = l + r + 1 >> 1;
			if (chk(mid)) l = mid;
			else r = mid - 1;
		} printf ("%d\n", l);
	}
}
namespace task4 {
	const int N = 1e5+5;
	int a[N], l, r, sum;
	inline bool chk(int x) {
		int now = 0, cnt = 0;
		for (int i=1; i<n; i++) {
			now += a[i];
			if (now >= x) now = 0, ++cnt;
			if (cnt >= m) return 1;
		} return 0;
	}
	inline void solve() {
		for (int i=1; i<n; i++) {
			if (vet[Head[i]] == i+1)
				a[i] = val[Head[i]];
			else if (vet[Next[Head[i]]] == i+1)
				a[i] = val[Next[Head[i]]];
			sum += a[i];
		} l = 1, r = sum;
		while (l < r) {
			int mid = l + r + 1 >> 1;
			if (chk(mid)) l = mid;
			else r = mid-1;
		} printf ("%d\n", l);
	}
}
int main() {
	freopen ("track.in", "r", stdin);
	freopen ("track.out", "w", stdout);
	n = Read(), m = Read();
	for (int i=1; i<n; i++) {
		u = Read(), v = Read(), w = Read(), 
		add(u, v, w), add(v, u, w);
		if (u ^ 1) Chrysanthemum = 0;
		if (v ^ u+1) Chain = 0;
	}
	if (m == 1) task2:: solve();
	else if (n <= 10) task1:: solve();
	else if (Chrysanthemum) task3:: solve();
	else if (Chain) task4:: solve()              ;
	return 0;
}
/*
10 4
1 2 3
1 3 2
1 4 2
1 5 8
1 6 5
1 7 1
1 8 4
1 9 1
1 10 2

8 3
1 2 2
2 3 3
3 4 1
4 5 4
5 6 4
6 7 1
7 8 9
*/
