#include <cstdio>
#include <iostream>
using namespace std;
int n, x, ans, lst;
int main() {
	freopen ("road.in", "r", stdin);
	freopen ("road.out", "w", stdout);
	scanf ("%d", &n);
	for (int i=1; i<=n; i++) {
		scanf ("%d", &x);
		if (x > lst) ans += x - lst;
		lst = x;
	} printf ("%d\n", ans);
	return 0;
}
