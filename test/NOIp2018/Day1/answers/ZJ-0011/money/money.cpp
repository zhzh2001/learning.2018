#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;
int T, n, ans, mx, a[233]; bool dp[25555];
int main() {
	freopen ("money.in", "r", stdin);
	freopen ("money.out", "w", stdout);
	scanf ("%d", &T), dp[0] = 1;
	while (T--) {
		scanf ("%d", &n), ans = mx = 0;
		for (int i=1; i<=n; i++)
			scanf ("%d", &a[i]), mx = max(mx, a[i]);
		for (int i=1; i<=mx; i++)
			dp[i] = 0;
		sort(a+1, a+1+n);
		for (int i=1; i<=n; i++)
			if (!dp[a[i]]) {
				++ ans;
				for (int j=0; j<=a[n]-a[i]; j++)
					dp[j+a[i]] |= dp[j];
			}
		printf ("%d\n", ans);
	} return 0;
}
