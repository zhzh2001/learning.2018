#include <bits/stdc++.h>
using namespace std;
#define rep(i,a,b) for(int i=(a);i<=(b);++i)

int n, d[100005];
int num[100005];
int Max=0;
long long ans=0;

int main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d", &n);
	rep(i,1,n)	scanf("%d", &d[i]), Max=max(Max,d[i]);
	bool f;
	rep(h,1,Max) {
		f=0;
		rep(i,1,n) {
			if(d[i]>=h)	f=1;
			else {
				if(f)	++ans;
				f=0;
			}
		}
		if(f)	++ans;
	}
	printf("%lld\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
