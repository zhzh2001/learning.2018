#include <bits/stdc++.h>
using namespace std;
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
const int N=25000;
int T, n, a[500];
int f[N+50], g[N+50];

/*int GCD(int a,int b) {
	return b?GCD(b,a%b):a;
}*/

int Max;
int sol() {
	memset(f,0,sizeof(f));
	memset(g,0,sizeof(g));
	int ans=0; f[0]=1,g[0]=1;
	rep(i,1,n)rep(z,0,Max-a[i])if(f[z])	f[z+a[i]]=1;
	rep(i,1,Max)if(f[i]&&!g[i]) {
		g[i]=1;
		rep(z,0,Max-i)if(g[z])	g[z+i]=1;
		++ans;
	}
	return ans;
}

int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d", &T);
	while(T--) {
		scanf("%d", &n); Max=0;
		rep(i,1,n)	scanf("%d", &a[i]),Max=max(Max,a[i]);
		printf("%d\n", sol());
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

/*
2
4
3 19 10 6
5
11 29 13 19 17
*/
