#include <bits/stdc++.h>
using namespace std;
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
const int N = 100005;
int n, m;

int cnt=0,nxt[N],h[N],to[N],w[N];
void Add(int a,int b,int l) {nxt[++cnt]=h[a],h[a]=cnt,to[cnt]=b,w[cnt]=l;}

int f[N], vis[N];
vector<int> v[N];
int num=0;

void dfs(int u,int fa,int x) {
	f[u]=0; v[u].clear();
	for(int i=h[u];i;i=nxt[i])if(to[i]!=fa) {
		dfs(to[i],u,x);
		if(f[to[i]]+w[i]>=x)	num++;
		else v[u].push_back(f[to[i]]+w[i]);
	}
	if(v[u].size()==0)	return;
	sort(v[u].begin(),v[u].end());
	int t=0;
	if(v[u].size()==1)	t=v[u][0];
	else {
		int siz=v[u].size()-1;
		rep(i,0,siz)if(!vis[i]) {
			rep(j,i+1,siz)if(!vis[j]&&v[u][i]+v[u][j]>=x){num++,vis[i]=vis[j]=1;break;}
		}
		rep(i,0,siz)if(!vis[i]) t=max(t,v[u][i]);else vis[i]=0;
	}
	f[u] = t;
}

bool check(int x) {
	num=0;
	dfs(1,0,x);
	return num>=m;
}

int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d", &n, &m);
	int a, b, L;
	rep(i,2,n) {
		scanf("%d%d%d", &a, &b, &L);
		Add(a,b,L), Add(b,a,L);
	}
	int l=1, r=1000000000, mid;
	while(l<=r) {
		mid = (l+r)>>1;
		if(check(mid))	l=mid+1;
		else r=mid-1;
	}
	printf("%d\n", r);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
// qwqwq
