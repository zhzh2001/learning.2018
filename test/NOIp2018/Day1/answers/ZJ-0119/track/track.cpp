#include <bits/stdc++.h>
using namespace std;
#define ref(i,x,y)for(int i=x;i<=y;++i)
#define def(i,x,y)for(int i=x;i>=y;--i)
#define mp make_pair
#define fi first
#define se second
#define pb push_back
#define SZ(x) ((int)x.size())
typedef long long LL;
typedef pair<int,int> PII;
int read(){
	char c=getchar();int d=0,f=1;
	for(;c<'0'||c>'9';c=getchar())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';d=d*10+c-48,c=getchar());
	return d*f;
}
const int N=50010;
int n,m,len,du[N],q[N],p[N]; bool mk[N];
PII dp[N];
vector<PII> e[N];
void work(){
	memset(dp,0,sizeof dp);
	memset(du,0,sizeof du);
	memset(mk,0,sizeof mk);
	ref(i,1,n)ref(j,0,SZ(e[i])-1)
		du[e[i][j].fi]++;
	int h=0,t=0;
	du[1]++;
	ref(i,1,n)if(du[i]<=1)q[++t]=i;
	while(h++<t){
		int x=q[h];
		mk[x]=1;
		int s1=0,s2=0,s3=0; p[0]=0;
		ref(i,0,SZ(e[x])-1){
			int y=e[x][i].fi,s=e[x][i].se;
			if(!mk[y])continue;
			s1+=dp[y].fi;
			int ss=dp[y].se+s;
			if(ss>=len)s1++;else p[++p[0]]=ss;
		}
		sort(p+1,p+p[0]+1);
		for(int c1=1,c2=p[0];c1<c2;++c1){
			if(p[c2]+p[c1]<len)continue;
			s1++; s2++; c2--;
		}
		if(s2*2==p[0])s3=0;else{
			int*a=&p[p[0]-s2*2];
			bool fg=1;
			ref(i,0,s2-1)if(a[i]+a[s2*2-i]<len)fg=0;
			if(fg){
				bool flag=0;
				def(i,s2-1,0)if(a[i]+a[s2*2-i-1]<len)
					{s3=a[s2*2-i-1];flag=1;break;}
				if(!flag)s3=a[s2*2];
			}else{
				ref(i,0,s2-1)if(a[i]+a[s2*2-i]<len)
					{s3=a[i];break;}
			}
		}
		dp[x]=mp(s1,s3);
		ref(i,0,SZ(e[x])-1){
			int y=e[x][i].fi;
			if(mk[y])continue;
			du[y]--; if(du[y]==1)q[++t]=y;
		}
	}
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	ref(i,2,n){
		int x=read(),y=read(),s=read();
		e[x].pb(mp(y,s));e[y].pb(mp(x,s));
	}
	int L=1,R=500000000;
	while(L<R){
		len=(L+R+1)>>1;
		work();
		if(dp[1].fi>=m)L=len;else R=len-1;
	}
	cout<<L<<endl;
	return 0;
}
