#include <bits/stdc++.h>
using namespace std;
#define ref(i,x,y)for(int i=x;i<=y;++i)
const int N=100100;
int read(){
	char c=getchar();int d=0,f=1;
	for(;c<'0'||c>'9';c=getchar())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';d=d*10+c-48,c=getchar());
	return d*f;
}
int n,a[N];
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	ref(i,1,n)a[i]=read();
	long long ans=0;
	ref(i,1,n+1)ans+=abs(a[i]-a[i-1]);
	ans/=2;
	cout<<ans<<endl;
	return 0;
}
