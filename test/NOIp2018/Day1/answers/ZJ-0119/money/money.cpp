//
//	money is not as good as happiness
//
#include <bits/stdc++.h>
using namespace std;
#define ref(i,x,y)for(int i=x;i<=y;++i)
int read(){
	char c=getchar();int d=0,f=1;
	for(;c<'0'||c>'9';c=getchar())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';d=d*10+c-48,c=getchar());
	return d*f;
}
const int N=25000;
int T,n,a[110],f[N+1];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	T=read();
	while(T--){
		n=read();
		ref(i,1,n)a[i]=read();
		sort(a+1,a+n+1);
		int ans=0;
		memset(f,0,sizeof f);
		f[0]=1;
		ref(i,1,n){
			if(f[a[i]])continue;
			ans++;
			ref(j,a[i],N)if(f[j-a[i]])f[j]=1;
		}
		cout<<ans<<endl;
	}
	return 0;
}
