#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
const int N=210,M=30010;
int T,n,a[N],f[M];
void pre(int Max)
{
	for (int i=0;i<=Max;i++) f[i]=0;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		int Max=0;
		for (int i=1;i<=n;i++) { scanf("%d",&a[i]);Max=max(Max,a[i]);}
		sort(a+1,a+n+1);
		pre(Max);
		f[0]=1;
		int ans=0;
		for (int i=1;i<=n;i++)
		{
			if (f[a[i]]) continue;
			for (int j=0;j<=Max-a[i];j++)
				if (f[j]) f[j+a[i]]=1;
			ans++;
		}
		printf("%d\n",ans);
	}
	return 0;
}
