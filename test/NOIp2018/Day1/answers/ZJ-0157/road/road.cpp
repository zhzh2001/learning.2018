#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
const int N=200100;
int n,a[N];
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	ll ans=0;
	for (int i=1;i<=n;i++)
	{
		int x=a[i];
		if (i!=1) x=max(a[i]-a[i-1],0);
		ans+=x;
	}
	printf("%lld\n",ans);
	return 0;
}
