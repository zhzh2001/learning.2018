#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
int a[105];
bool f[25005];
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	for(int T=read();T;T--){
		int n=read();
		for(int i=1;i<=n;i++)a[i]=read();
		sort(a+1,a+n+1);
		memset(f,0,sizeof(f));
		f[0]=1;
		int ans=0;
		for(int i=1;i<=n;i++){
			if(f[a[i]])continue;
			ans++;
			for(int j=a[i];j<=a[n];j++)
				f[j]|=f[j-a[i]];
		}
		printf("%d\n",ans);
	}
	return 0;
}
