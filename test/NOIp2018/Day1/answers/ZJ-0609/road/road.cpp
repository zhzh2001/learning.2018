#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int N=1e5+5;
int n,ans,a[N];
struct Segtree{
	struct Data{
		int l,r,p;
	}tree[N<<2];
	#define lson (k<<1)
	#define rson (k<<1|1)
	void pushup(int k){
		if(a[tree[lson].p]<a[tree[rson].p])tree[k].p=tree[lson].p;
		else tree[k].p=tree[rson].p;
	}
	void build(int k,int l,int r){
		tree[k].l=l,tree[k].r=r;
		if(l==r){
			tree[k].p=l;
			return;
		}
		int mid=(l+r)>>1;
		build(lson,l,mid);
		build(rson,mid+1,r);
		pushup(k);
	}
	int query(int k,int l,int r){
		if(tree[k].l>r||tree[k].r<l)return 0;
		if(l<=tree[k].l&&tree[k].r<=r)return tree[k].p;
		int L=query(lson,l,r),R=query(rson,l,r);
		if(a[L]<a[R])return L;
		return R;
	}
	#undef lson
	#undef rson
}tr;
void solve(int l,int r,int del){
	if(l>r)return;
	int p=tr.query(1,l,r);
	ans+=a[p]-del;
	solve(l,p-1,a[p]),solve(p+1,r,a[p]);
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	a[0]=1e9;
	tr.build(1,1,n);
	solve(1,n,0);
	printf("%d",ans);
	return 0;
}
