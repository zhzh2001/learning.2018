#include<set>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int N=5e4+5;
int nedge,head[N];
struct Edge{
	int to,nxt,id,w;
}edge[N<<1];
int x[N],y[N],z[N];
void add(int x,int y,int z,int id){
	edge[++nedge].to=y;
	edge[nedge].id=id;
	edge[nedge].w=z;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
int n,m;
int dis[N];
namespace bf1{//�� 
	int a[N];
	bool check(int x){
		int num=m;
		int lst=0;
		for(int i=1;i<n&&num;i++)
			if(a[i]-a[lst]>=x)num--,lst=i;
		return num==0;
	}
	void work(){
		for(int i=1;i<n;i++)a[i]=z[i];
		for(int i=1;i<=n;i++)a[i]+=a[i-1];
		int l=1,r=10000*n,ans=0;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d",ans);
	}
}
namespace bf2{//ֱ�� 
	void dfs(int x,int fa){
		for(int i=head[x];i;i=edge[i].nxt){
			int y=edge[i].to;
			if(y==fa)continue;
			dis[y]=dis[x]+edge[i].w;
			dfs(y,x);
		}
	}
	void work(){
		memset(dis,0,sizeof(dis));
		dfs(1,0);
		int p=1;
		for(int i=1;i<=n;i++)
			if(dis[i]>dis[p])p=i;
		memset(dis,0,sizeof(dis));
		dfs(p,0);
		int ans=0;
		for(int i=1;i<=n;i++)ans=max(ans,dis[i]);
		printf("%d",ans);
	}
}
namespace bf3{//�ջ��� 
	set<int>s;
	bool check(int x){
		s.clear();
		for(int i=1;i<n;i++)s.insert(z[i]);
		int num=m;
		while(num--){
			if(s.empty())return 0;
			int p=*s.rbegin();
			s.erase(--s.end());
			if(p>=x)continue;
			set<int>::iterator it=s.lower_bound(x-p);
			if(it==s.end())return 0;
			s.erase(it);
		}
		return 1;
	}
	void work(){
		int l=1,r=10000*n,ans=0;
		while(l<=r){
			int mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d",ans);
	}
}
int fa[N],d[N],deg[N],q[N],p[N];
void dfs1(int x,int f){
	fa[x]=f;
	d[x]=d[f]+1;
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		if(y==f){
			p[x]=edge[i].id;
			continue;
		}
		dfs1(y,x);
	}
}
bool used[N],vis[N],v[N];
void dfs(int x,int fa){
	vis[x]=1;
	for(int i=head[x];i;i=edge[i].nxt){
		if(used[edge[i].id])continue;
		int y=edge[i].to;
		if(y==fa)continue;
		dis[y]=dis[x]+edge[i].w;
		dfs(y,x);
	}
}
bool check(int x){
	memset(v,0,sizeof(v));
	memset(used,0,sizeof(used));
	int l=0,r=0,num=m;
	for(int i=2;i<=n;i++)
		if(deg[i]==1)q[++r]=i;
	while(l<r){
		int a=q[++l];
		if(v[a])continue;
		dis[a]=0;
		memset(vis,0,sizeof(vis));
		dfs(a,0);
		int b=0;
		for(int i=1;i<=n;i++)
			if(vis[i]&&dis[i]>=x&&d[i]>d[b])b=i;
		if(!b)continue;
		num--;
		if(!num)return 1;
		while(a!=b){
			if(d[a]<d[b])swap(a,b);
			used[p[a]]=1;
			a=fa[a];
			v[a]=1;
		}
		q[++r]=fa[a];
	}
	return 0;
}
int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	bool f1=1,f2=1;
	for(int i=1;i<n;i++){
		x[i]=read(),y[i]=read(),z[i]=read();
		deg[x[i]]++,deg[y[i]]++;
		f1&=(x[i]==1);
		f2&=(x[i]+1==y[i]);
	}
	if(f2){
		bf1::work();//�� 
		return 0;
	}
	for(int i=1;i<n;i++){
		add(x[i],y[i],z[i],i);
		add(y[i],x[i],z[i],i);
	}
	if(m==1){
		bf2::work();//ֱ�� 
		return 0;
	}
	if(f1){
		bf3::work();//�ջ��� 
		return 0;
	}
	dfs1(1,0);
	int l=1,r=10000*n,ans=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if(check(mid))ans=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%d",ans);
	return 0;
}
