#include<bits/stdc++.h>
#define int long long
using namespace std;
struct Edge{
	int x,y,l;
}a[50001];
bool cmp_flower(Edge x,Edge y){
	return x.l>y.l;
}
int n,m,ans;
bool flag_flower,flag_line;
signed main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	flag_flower=flag_line=1;
	for(int i=1;i<n;i++){
		scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].l);
		if(a[i].x!=1&&a[i].y!=1)
			flag_flower=0;
		if(a[i].x!=a[i].y-1&&a[i].x!=a[i].y+1)
			flag_line=0;
	}
	if(flag_flower){
		sort(a+1,a+n,cmp_flower);
		if(m>1){
			if(m<=n-2)
				ans=min(a[m-1].l,a[m].l+a[m+1].l);
			else
				ans=a[m].l;
		}
		else
			ans=a[1].l+a[2].l;
		return printf("%lld",ans),0;
	}
	if(flag_line){
		int sum=0;
		for(int i=1;i<n;i++)
			sum+=a[i].l;
		sum/=m;
		ans=sum;
		int x=1,y=0;
		while(x<n){
			if(y+a[x].l>sum){
				ans=min(ans,y);
				y=0;
			}
			else
				y+=a[x].l;
			x++;
		}
		return printf("%lld",ans),0;
	}
	return 0;
}
