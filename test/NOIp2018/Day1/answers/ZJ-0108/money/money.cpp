#include<bits/stdc++.h>
#define int long long
using namespace std;
int t,n,ans,maxx,x;
int a[1005];
bool flag[50005];
signed main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%lld",&t);
	while(t--){
		scanf("%lld",&n);
		ans=0;
		for(int i=1;i<=n;i++){
			scanf("%lld",&a[i]);
			maxx=max(maxx,a[i]);
		}
		sort(a+1,a+1+n);
		for(int i=1;i<=maxx;i++)
			flag[i]=0;
		for(int i=1;i<=n;i++){
			if(flag[a[i]]){
				ans++;
				continue;
			}
			flag[a[i]]=1;
			for(int j=1;j<=maxx;j++)
				if(flag[j]){
					x=j;
					while(x<maxx){
						x+=a[i];
						flag[x]=1;
					}
				}
		}
		printf("%lld\n",n-ans);
	}
	return 0;
}
