#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<cctype>
#define xep(i,x) for(int i=head[x];i;i=nxt[i])
#define inf 0x3f3f3f3f

using namespace std;

const int N=50010;
int n,m,cnt=0,st=1,ed=1,tot=0,num,ans=0,lxm;
int head[N],nxt[N<<1],to[N<<1],val[N<<1],tag[N<<1];
int sum[N],res[N],vis[N];
bool fg1,fg2;

inline void add(int x,int y,int v){
	to[++cnt]=y;nxt[cnt]=head[x];
	head[x]=cnt;val[cnt]=v;
}

inline void dfs(int x,int ff){
	xep(i,x){
		int v=to[i];
		if(v==ff) continue;
		sum[v]=max(sum[x]+val[i],sum[v]);
		dfs(v,x);
	}
}

inline void dfs2(int x,int ff){
	xep(i,x){
		int v=to[i];
		if(v==ff||tag[i]==1) continue;
		else vis[v]=1,lxm+=val[i],dfs2(v,x);//染色 
	}
}

inline void dfs1(int x,int ff,int k){
	if(k==m){
		memset(vis,0,sizeof(vis));
		tot=0;num=inf;
		for(int i=1;i<=n;i++){
			if(vis[i]==0){
				lxm=0;vis[i]=1;
				dfs2(i,0);
				num=min(num,lxm);
			}
		}
		ans=max(ans,num);
		return;
	}
	xep(i,x){
		int v=to[i];
		if(v==ff) continue;
		tag[i]=1;dfs1(v,x,k+1);
		tag[i]=0;dfs1(v,x,k);
	}
}

int main(){
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		if(x!=1) fg1=1;//菊花图 
		if(y!=x+1) fg2=1; // 链 
		add(x,y,v),add(y,x,v);
	}
	if(m==1){
		memset(sum,0,sizeof(sum));
		dfs(1,0);
		for(int i=1;i<=n;i++)
			if(sum[i]>sum[st]) st=i;
		memset(sum,0,sizeof(sum));
		dfs(st,0);
		for(int i=1;i<=n;i++)
			if(sum[i]>sum[ed]) ed=i;
		printf("%d\n",sum[ed]);
		return 0;
	}
	if(!fg1){//菊花图 
		memset(res,0,sizeof(res));
		dfs(1,0);
		sort(sum+2,sum+1+n);
		for(int i=n;i>=n-m+1;i--)
			res[n-i+1]+=sum[i];
		for(int i=n-m;i>=max(2,n-2*m+1);i--){
			res[m-n+m+i]+=sum[i];
		}
		int maxn=inf;
		for(int i=1;i<=m;i++)
			maxn=min(maxn,res[i]);
		printf("%d\n",maxn);
		return 0;
	}
	if(!fg2){//链的情况 
		dfs1(1,0,0); 
		printf("%d\n",ans);
//		for(int i=)
		return 0;
	}
	return 0;
}
