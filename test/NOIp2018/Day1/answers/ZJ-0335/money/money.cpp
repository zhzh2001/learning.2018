#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<cctype>

using namespace std;

const int N=110;
int T,n,cnt,ans=0,now,tot;
int a[N],num[25010];
bool get[25010];
// get 判断数是否已经可以被表示
// num 存当前这个序号的数的倍数 
int main(){
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		memset(num,0,sizeof(num));
		memset(get,false,sizeof(get));
		cnt=tot=ans=0;
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			scanf("%d",&a[i]);
		sort(a+1,a+1+n);
		for(int i=1;i<=n;i++)
			if(get[a[i]]==0){
				ans++;now=a[i];
				while(now<=a[n]) 
					get[now]=1,num[++tot]=now,now+=a[i];
				for(int j=a[i];j<=a[n];j++)
					if(get[j]==1)
						for(int k=1;k<=tot;k++)
							if(j+num[k]<=a[n]) get[j+num[k]]=1;
			}
		printf("%d\n",ans);
	}
	return 0;
}
