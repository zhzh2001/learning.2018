#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
#include<set>
#include<cctype>
#include<cmath>
#define inf 0x3f3f3f3f
#define ll long long

using namespace std;

const int N=100010;
int n,l,r,cnt=0,unit;
ll ans=0;
int a[N],vis[N];

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	memset(vis,0,sizeof(vis));
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	a[0]=a[n+1]=0;
	while(cnt<n){
		for(int i=1;i<=n;i++){
			if(a[i+1]>a[i]&&a[i-1]>a[i]) continue;
			l=r=i;
			while(a[l-1]<=a[l]&&l>0) l--;
			while(a[r+1]<=a[r]&&r<=n) r++;
//			if(l!=i&&r!=i){
				unit=max(a[l],a[r]);ans+=1ll*(a[i]-unit);
//				printf("%d %d %d\n",unit,l,r);
				if(l==0) l++;if(r==n+1) r--;
				for(int j=l;j<=r;j++){
					if(a[j]>=unit) 
						a[j]=unit;
					if(a[j]==0) cnt++;
				}
//			}
		}
	}
	printf("%lld\n",ans);
	return 0;
}
