#include <bits/stdc++.h>

using namespace std;

const int len1 = 110,len2 = 25010;
int T,n,p,ans,a[len1],b[len1],f[len2],dp[len1][len2];

inline bool check(int k) {
	int pp;
	for (int i=1;i<=p;i++) if (b[i] == k) {pp = i-1;break;}
	if (pp == 0) return false;
	for (int i=0;i<=pp;i++) {
		for (int j=0;j<k;j++) dp[i][j] = 0;
		dp[i][k] = 1;
	}
	for (int i=1;i<=pp;i++) {
		for (int j=0;j<=k;j++) {
			if (dp[i-1][j] == 0) continue;
			for (int u=0;u<=j/b[i];u++) {
				dp[i][j-u*b[i]] = 1;
			}
		}
	}
	if (dp[pp][0] == 1) return true;
	else return false;
}

int main() {
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		int mx = 0;
		scanf("%d",&n);
		memset(f,0,sizeof f);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]),f[a[i]] = 1,mx = max(mx,a[i]);
		sort(a+1,a+n+1);
		for (int i=1;i<=mx;i++) {
			if (f[i] == 0) continue;
			for (int j=1;j<=n;j++) if (i != a[j] && i%a[j] == 0) {f[i] = 0;break;}
		}
		p = mx = ans = 0;
		for (int i=1;i<=n;i++) if (f[a[i]] == 1) b[++p] = a[i],mx = max(mx,b[p]);
		for (int i=1;i<=mx;i++) {
			if (f[i] == 0) continue;
			if (check(i)) continue;
			ans++;
		}
		printf("%d\n",ans);
	}
}
