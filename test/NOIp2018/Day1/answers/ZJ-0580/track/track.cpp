#include <bits/stdc++.h>

using namespace std;

const int len = 50010;
int n,m,k,kk,mn,pf[len],vis[len],dis[len],head[len];
queue <int> q;
bool f1,f2;

struct node {
	int w,cnt,nxt;
}e[len<<1];

inline void add(int u,int v,int w) {
	e[++k] = (node){w,v,head[u]},head[u] = k;
}

inline void spfa(int s) {
	q.push(s);
	for (int i=1;i<=n;i++) dis[i] = 0;
	vis[s] = 1;
	while (!q.empty()) {
		int u = q.front();
		for (int i=head[u];i!=0;i=e[i].nxt) {
			int v = e[i].cnt;
		 	if (!vis[v] && dis[v] < dis[u]+e[i].w) {
				dis[v] = dis[u]+e[i].w;
				if (!vis[v]) q.push(v),vis[v] = 1;
			}
		}
		q.pop();
	}
	return;
}

inline bool cmp(int a,int b) {
	return a > b;
}

inline void dfs1(int res,int resl) {
	if (res == 0) {mn = max(mn,resl);return;}
	for (int i=1;i<=kk;i++) {
		if (!vis[i]) {
			vis[i] = 1,dfs1(res-1,min(resl,pf[i])),vis[i] = 0;
		}
	}
	for (int i=1;i<=kk;i++) {
		for (int j=1;j<=kk;j++) {
			if (i == j) continue;
			if (vis[i] == 1 || vis[j] == 1) continue;
			vis[i] = vis[j] = 1;
			dfs1(res-1,min(resl,pf[i]+pf[j]));
			vis[i] = vis[j] = 0;
		}
	}
	return;
}

inline void dfs2(int pos,int res,int resl) {
	if (res == 0) {mn = max(mn,resl);return;}
	if (pos > kk) return;
	int sum = 0;
	dfs2(pos+1,res,resl);
	for (int i=pos;i<=kk;i++) {
		sum += pf[i];
		dfs2(i+1,res-1,min(resl,sum));
	}
}

int main() {
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	scanf("%d%d",&n,&m);
	k = kk = 0;
	f1 = f2 = true;
	for (int i=1;i<n;i++) {
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		add(u,v,w),add(v,u,w);
		pf[++kk] = w;
		if (u != 1) f1 = false;
		if (v != u+1) f2 = false;
	}
	if (m == 1 && m <= 5000) {
		int mx = 0;
		for (int i=1;i<=n;i++) {
			memset(vis,0,sizeof vis);
			spfa(i);
			for (int i=1;i<=n;i++) mx = max(mx,dis[i]);
		}
		printf("%d\n",mx);
	}
	else if (m == 1 && !f1) {
		sort(pf+1,pf+kk+1,cmp);
		printf("%d\n",pf[1]+pf[2]);
	}
	else if (f1) {
		memset(vis,0,sizeof vis);
		mn = 0;
		dfs1(m,1e9+10);
		printf("%d\n",mn);
	}
	else if (f2) {
		mn = 0;
		dfs2(1,m,1e9+10);
		printf("%d\n",mn);
	}
	else if (m == n-1) {
		mn = 1e9+10;
		for (int i=1;i<=kk;i++) mn = min(mn,pf[i]);
		printf("%d\n",mn);
	}
	else {
		sort(pf+1,pf+kk+1,cmp);
		printf("%d\n",pf[m]);
	}
	return 0;
}
