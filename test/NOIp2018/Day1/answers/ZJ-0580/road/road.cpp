#include <bits/stdc++.h>
#define int long long

using namespace std;

const int len = 1e5+10;
int n,ans,a[len];

signed main() {
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%lld",&n);
	for (int i=1;i<=n;i++) scanf("%lld",&a[i]);
	ans = 0;
	a[0] = 0;
	for (int i=1;i<=n;i++) {
		if (a[i] < a[i-1]) continue;
		else ans += a[i]-a[i-1];
	}
	printf("%lld\n",ans);
	return 0;
}
