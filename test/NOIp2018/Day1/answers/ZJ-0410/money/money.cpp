#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int maxn=100000+10;
const int maxt=10000+10;
inline ll read()
{
	int f=1,x=0;
	char c=getchar();
	while(c<'0' || c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0' && c<='9'){x=x*10+c-'0';c=getchar();}
	return f*x;
}
ll  t,n,m,now=0;
ll dat[maxn];
bool vis[maxn];
void baoli(ll x,ll j)
{
	if(x>m)
	{
		if(now==dat[j]){vis[j]=true;m--;}
		return;
	}
	for(int i=1;i<=n;i++)
	{
		if(!vis[i] && i!=j)
		{
			vis[i]=true;
			ll a=0;
			while(a*dat[i]<=dat[j] && now<=dat[j])
			{
				now+=a*dat[i];
				baoli(x+1,j);
				now-=a*dat[i];
				a++;
			}
			vis[i]=false;
			return ;
		}
	}
}
void work(void)
{
	n=read();
	m=n;
	for(int i=1;i<=n;i++)
	{
		dat[i]=read();
	}
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
		{
			if(dat[j]%dat[i]==0 && i!=j && !vis[j])
			{
				vis[j]=true;
				m--;
			}
		}
	}
	for(int i=1;i<=n;i++)
	{
		if(!vis[i])
			baoli(1,i);
	}
	cout<<m<<endl;
}
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read();
	for(int i=1;i<=t;i++)
	{
		work();
	}
	return 0;
}
