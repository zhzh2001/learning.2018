#include <bits/stdc++.h>
#define ll long long
#define sp printf("now is %lld,ans is %lld",tree[p],ans);system("pause")
using namespace std;
const int maxn=100000+10;
const int maxt=10000+10;
inline ll read()
{
	int f=1,x=0;
	char c=getchar();
	while(c<'0' || c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0' && c<='9'){x=x*10+c-'0';c=getchar();}
	return f*x;
}
ll  n;
ll tree[maxn<<2],dat[maxn],ans=0;
inline ll ls(ll p){return p<<1;}
inline ll rs(ll p){return p<<1|1;}
inline void push_up(ll p){tree[p]=min(tree[ls(p)],tree[rs(p)]);}
inline void build(ll p, ll l, ll r)
{
	if(l==r){tree[p]=dat[l];return;}
	ll mid=(l+r)>>1;
	build(ls(p),l,mid);
	build(rs(p),mid+1,r);
	push_up(p);
}
inline void update(ll p,ll l,ll r,ll k)
{
	tree[p]-=k;
	if(l==r){return;}
	ll mid=(l+r)>>1;
	update(ls(p),l,mid,k);
	update(rs(p),mid+1,r,k);
	push_up(p);
}
void dfs(ll p,ll l, ll r)
{
	if(l==r){ans+=tree[p];tree[p]=0;return;}
	if(tree[p])
	{
		ans+=tree[p];
		update(p,l,r,tree[p]);
	}
	ll mid=(l+r)>>1;
	dfs(ls(p),l,mid);
	dfs(rs(p),mid+1,r);
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
	{
		dat[i]=read();
	}
	build(1,1,n);
	dfs(1,1,n);
	cout<<ans<<endl;
	return 0;
}
