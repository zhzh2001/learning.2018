#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define C getchar()-48
inline ll read()
{
	ll s=0,r=1;
	char c=C;
	for(;c<0||c>9;c=C) if(c==-3) r=-1;
	for(;c>=0&&c<=9;c=C) s=(s<<3)+(s<<1)+c;
	return s*r;
}
const int N=50010,inf=1e9+7;
int n,m;
int p,p2;
int link[N],e[N<<1],nxt[N<<1],v[N<<1],top;
int bian[N];
int zd[N],cd[N],ans;
int dis[N];
int dp[1010][1010];
int dfn[N],w[N],ed[N],topd;
inline void into(int xx,int yy,int vv)
{
	e[++top]=yy,nxt[top]=link[xx],link[xx]=top,v[top]=vv;
}

inline void dfs(int x,int fa)
{
	for(int i=link[x];i;i=nxt[i])
	{
		int u=e[i];
		if(u==fa) continue;
		dfs(u,x);
		int dd=zd[u]+v[i];
		if(dd>zd[x])
		{
			cd[x]=zd[x];
			zd[x]=dd;
		}
		else if(dd>cd[x])
		{
			cd[x]=dd;
		}
	}
	ans=max(ans,zd[x]+cd[x]);
}
void work1()
{
	dfs(1,0);
	cout<<ans;
	exit(0);
}


inline bool pd(int a,int b)
{
	return a>b;
}
void work2()
{
	sort(bian+1,bian+n,pd);
	int len=m*2;ans=1e9;
	if(len<=n-1)
	for(int i=1;i<=m;i++) ans=min(ans,bian[i]+bian[len-i+1]);
	else 
	{
		int duo=len-(n-1),sheng=(n-1)-duo;
		for(int i=1;i<=duo;i++) ans=min(ans,bian[i]);
		for(int i=1;i<=sheng;i++) ans=min(ans,bian[i+duo]+bian[sheng-i+1+duo]);
	}
	cout<<ans;
	exit(0);
}


inline void qdfs(int x,int fa)
{
	for(int i=link[x];i;i=nxt[i])
	{
		int u=e[i];
		if(u==fa) continue;
		dis[u]=dis[x]+v[i];
		qdfs(u,x);
	}
}
void work3()
{
	dp[1][0]=inf;
	qdfs(1,0);
	for(int i=2;i<=n;i++)
	for(int k=1;k<=min(m,i-1);k++)
	for(int o=1;o<i;o++)
		dp[i][k]=max(dp[i][k],min(dp[o][k-1],dis[i]-dis[o]));
	cout<<dp[n][m];
	exit(0);
}


inline void fdfn(int x,int fa)
{
	dfn[++topd]=x,w[x]=topd;
	for(int i=link[x];i;i=nxt[i])
	{
		int u=e[i];
		if(u==fa) continue;
		fdfn(u,x);
	}
	ed[x]=topd;
}
/*
inline void find(int x,int fa)
{
	for(int i=link[x];i;i=nxt[i])
    {
    	int u=e[i];
    	if(u==fa) continue;
    	find(u,x);
	}
	cout<<x<<"FFFFFFFFFFFFFFFFF"<<endl;
	for(int i=w[x];i<=ed[x];i++)
	for(int k1=0;k1<=m-1;k1++)
	for(int o=i+1;o<=ed[x];o++)
	for(int k2=0;k2<=m-1-k1;k2++)
	{
		cout<<" i "<<i<<" o "<<o<<" z1 "<<dp[i][k1]<<" z2 "<<dp[o][k2]<<" z3 "<<dis[i]+dis[o]-2*dis[x]<<" w1 "<<k1<<" w2 "<<k2<<endl;
		dp[x][k1+k2+1]=max(dp[x][k1+k2+1],min(min(dp[i][k1],dp[o][k2]),dis[i]+dis[o]-2*dis[x]));
		cout<<dp[x][k1+k2+1]<<endl;
	}
}*/
inline void find(int x,int fa)
{
	for(int i=link[x];i;i=nxt[i])
    {
    	int u=e[i];
    	if(u==fa) continue;
    	find(u,x);
	}
//	cout<<x<<"FFFFFFFFFFFFFFFFF"<<endl;
	for(int i=w[x];i<=ed[x];i++)
	for(int k1=0;k1<=m-1;k1++)
	{
//		cout<<" i "<<i<<" o "<<o<<" z1 "<<dp[i][k1]<<" z2 "<<dp[o][k2]<<" z3 "<<dis[i]+dis[o]-2*dis[x]<<" w1 "<<k1<<" w2 "<<k2<<endl;
		dp[x][k1+1]=max(dp[x][k1+1],min(dp[i][k1],dis[i]-dis[x]));
//		cout<<dp[x][k1+k2+1]<<endl;
	}
}
void worked()
{
	for(int i=1;i<=n;i++) dp[i][0]=inf;
	qdfs(1,0);
	fdfn(1,0);
	find(1,0);
	cout<<dp[1][m];
	exit(0);
}
int main()
{
	freopen("track.in","r",stdin);
	freopen("track.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read(),v=read();
		if(x>y) swap(x,y); 
		if(x==1) p++;
		if(y==x+1) p2++;
		into(x,y,v);into(y,x,v);bian[i]=v;
	}
	if(m==1) work1();
	else if(p+1==n) work2();
	else if(p2+1==n&&n<=1000) work3();
	else worked();
 	return 0;
}
