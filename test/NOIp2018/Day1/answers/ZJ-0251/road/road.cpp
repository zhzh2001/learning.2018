#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define C getchar()-48
inline ll read()
{
	ll s=0,r=1;
	char c=C;
	for(;c<0||c>9;c=C) if(c==-3) r=-1;
	for(;c>=0&&c<=9;c=C) s=(s<<3)+(s<<1)+c;
	return s*r;
}
const int N=100010;
int n;
ll a[N];
ll ans;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	ans=a[1];
	for(int i=2;i<=n;i++)
	if(a[i]>a[i-1])
	  ans+=a[i]-a[i-1];
	cout<<ans;
	return 0;
}
