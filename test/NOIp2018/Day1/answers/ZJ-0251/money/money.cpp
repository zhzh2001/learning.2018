#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define C getchar()-48
inline ll read()
{
	ll s=0,r=1;
	char c=C;
	for(;c<0||c>9;c=C) if(c==-3) r=-1;
	for(;c>=0&&c<=9;c=C) s=(s<<3)+(s<<1)+c;
	return s*r;
}
const int N=110;
int t;
int n;
int a[N];
int dp[25010];
int ans,mx;
int main()
{
	freopen("money.in","r",stdin);
	freopen("money.out","w",stdout);
	t=read();dp[0]=1;
	while(t--)
	{
	    n=read();ans=0;mx=0;
		for(int i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+1+n);
		mx=a[n];
		for(int i=1;i<=mx;i++) dp[i]=0;
		for(int i=1;i<=n;i++)
		if(!dp[a[i]])
		{
			ans++;
			int z=a[i];
			for(int o=z;o<=mx;o++)
		      dp[o]|=dp[o-z];
		}		
		printf("%d\n",ans);
	}
	return 0;
}
