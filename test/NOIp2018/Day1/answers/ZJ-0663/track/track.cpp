#include <bits/stdc++.h>

using namespace std;

const int Maxn = 5e4+7;
const int N3 = 1e3+7;
const int INF = 0x3f3f3f3f;

int n, m, ans;
int b[Maxn<<1], l[Maxn<<1], fir[Maxn], nex[Maxn<<1], tot;
int p1, p2, tree_D, vis[Maxn];
int spj_a = 1, spj_b = 1;
int e[N3][N3];

inline void add_edge(int u, int v, int w)
{
	b[++tot] = v; l[tot] = w; nex[tot] = fir[u]; fir[u] = tot;
}

// vis表示点是否访问 
void tree_d(int cur, int len)
{
	if(len > tree_D)
	{
		tree_D = len;
		p2 = cur;
	}
	vis[cur] = 1;
	for(int i = fir[cur], to; i; i = nex[i])
	{
		to = b[i];
		if(!vis[to])
			tree_d(to, len+l[i]);
	}
}

inline bool check(int len, vector<int> &tmp)
{
	int i, cur = 0, cnt = 0;
	for(i = 2; i <= n; ++i)
	{
		// cur >= len 才能分成一段 
		cur += tmp[i];
		if(cur >= len) cur = 0, ++cnt;
	}
	return cnt >= m;
}

// e[i][j]表示边是否访问 
// 不断 dfs 找边删边找到 m 条边的最大值 
// cur 当前点 len 累计长度(仅第cnt条) cnt 正在找第几条边 , minl 累计最小长度 
void brute(int cur, int len, int cnt, int minl)
{
	// 瞎几把更新, 不一定选全 m 边,贪心当前len一定比最优解小 
	ans = max(ans, min(len, minl)); 
	// 新的一轮,选择起点 
	if(cur == 0)
	{
		for(int i = 1; i <= n; ++i)
			brute(i, len, cnt, minl);
		return; 
	}
	// 老的一轮 
	int child = 0; 
	for(int i = fir[cur], to; i; i = nex[i])
	{
		to = b[i];
		if(!e[cur][to])
		{
			child++;
			e[cur][to] = e[to][cur] = 1;
			brute(to, len+l[i], cnt, minl);
			e[cur][to] = e[to][cur] = 0;
		}
	}
	// 下不去了,到终点了,那就下一条边 
	if(!child && cnt+1 <= m) brute(0, 0, cnt+1, min(minl, len));
	// 找到底不一定是最优解,暴力全找!!!
	//if(cnt+1 <= m) brute(0, 0, cnt+1, min(minl, len));
}

int main()
{
	freopen("track.in", "r", stdin);
	freopen("track.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i = 1, u, v, w; i < n; ++i)
	{
		scanf("%d%d%d", &u, &v, &w);
		add_edge(u, v, w);
		add_edge(v, u, w);
		if(u != 1) spj_a = 0; // ai == 1
		if(u+1 != v) spj_b = 0; // bi == ai+1
	}
	if(m == 1)
	{
		// 树的直径
		srand(time(NULL));
		p1 = rand()*rand()%n+1;
		tree_d(p1, 0);
		memset(vis, 0, sizeof vis);
		tree_d(p2, 0);
		printf("%d\n", tree_D);
		return 0;
	}
	if(m == n-1)
	{
		for(int i = 1; i <= tot; i += 2)
			ans += l[i];
		printf("%d\n", ans);
		return 0;
	}
	if(spj_a)
	{
		// choose m pair
		// cause two side edge, every l has two the same value
		// tot = 2(n-1)
		// (1,2m) (2,2m-1) (3, 2m-3)
		vector<int> tmp;
		for(int i = 1; i <= tot; i += 2) tmp.push_back(l[i]);
		sort(tmp.begin(), tmp.end());
		reverse(tmp.begin(), tmp.end());
		m = min(m, (n-1)/2); // 2m <= n-1
		ans = tmp[0]+tmp[1];
		for(int i = 1; i <= m; ++i)
			ans = min(ans, tmp[i-1]+tmp[2*m-i]);
		printf("%d\n", ans);
		return 0;
	}
	if(spj_b)
	{
		// 把一条线分成 m 段,最大长度 
		int ll = l[1], rr = 0, mid;
		vector<int> tmp(n+1, 0);
		for(int i = 2; i <= n; ++i)
			for(int j = fir[i]; j; j = nex[j])
				if(b[j] == i-1)
				{ 
					tmp[i] = l[j];
					ll = min(ll, l[j]);
					rr += l[j];
					break;
				}
		// tmp[i] i-1 to i
		while(ll < rr)
		{
			mid = (ll+rr+1)>>1;
			if(check(mid, tmp)) ll = mid;
			else rr = mid-1;
		}
		printf("%d\n", ll);
		return 0;
	}
	// 接下来只考虑分支不超过 3 的情况 
	// 那个,还是算了,放弃思考
	// 尝试暴力枚举,乱搞算法 
	if(n <= 200)
	{
		brute(0, 0, 1, INF);
		printf("%d\n", ans); 
	} 
	// 欧皇一波 
	else
	{
		srand(time(NULL));
		printf("%d\n", rand()*rand());
	}
	return 0;
}
