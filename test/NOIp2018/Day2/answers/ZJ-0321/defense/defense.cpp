#include<cstdio>
#include<iostream>
using namespace std;
typedef long long LL;
const int N=100010;
int n,m,head[N],cnt,a,x,b,y,p[N],fa[N];
LL f[N][2];
char s[100];
struct E{int to,nxt;}e[N<<1];
void Judge(){freopen("defense.in","r",stdin);freopen("defense.out","w",stdout);}
int read(){int d=0,f=1; char c=getchar(); while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();} while (c>='0'&&c<='9') d=d*10+c-48,c=getchar(); return d*f;}
void add(int x,int y){e[++cnt]=(E){y,head[x]}; head[x]=cnt;}
void dfs(int u)
{
	f[u][1]=p[u];
	for (int i=head[u];i;i=e[i].nxt)
	{
		int v=e[i].to;
		if (v==fa[u]) continue;
		fa[v]=u; dfs(v);
		f[u][1]+=min(f[v][0],f[v][1]);
		f[u][0]+=f[v][1];
		if (f[u][0]>1E16) f[u][0]=1E16;
		if (f[u][1]>1E16) f[u][1]=1E16;
	}
	if (u==a) f[u][x^1]=1E16;
	if (u==b) f[u][y^1]=1E16;
	if (f[u][0]>1E16) f[u][0]=1E16;
	if (f[u][1]>1E16) f[u][1]=1E16;
}
int main()
{
	Judge();
	n=read(); m=read(); scanf("%s",s);
	for (int i=1;i<=n;i++) p[i]=read();
	for (int i=1;i<n;i++)
	{
		int u=read(),v=read();
		add(u,v); add(v,u);
	}
	while (m--)
	{
		a=read(),x=read(),b=read(),y=read();
		for (int i=1;i<=n;i++) f[i][0]=f[i][1]=0;
		dfs(1);
		LL ans=min(f[1][0],f[1][1]);
		if (ans==1E16) puts("-1");
		else printf("%lld\n",ans);
	}
	return 0;
}
