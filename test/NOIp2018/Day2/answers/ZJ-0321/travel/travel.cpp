#include<cstdio>
#include<algorithm>
using namespace std;
const int N=5010;
int n,m,head[N],cnt,fa[N],a[N][N],ljj,pjy,tmp,lca,t1,t2,f[N],fn,eden;
int A[N],b[N],c[N],d[N],an,bn,cn,dn,dep[N],ans[N];
struct E{int to,nxt;}e[N<<1];
bool vis[N];
void Judge(){freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);}
int read(){int d=0,f=1; char c=getchar(); while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();} while (c>='0'&&c<='9') d=d*10+c-48,c=getchar(); return d*f;}
void add(int x,int y){e[++cnt]=(E){y,head[x]}; head[x]=cnt;}
void dfs(int u)
{
	vis[u]=1;
	for (int i=head[u];i;i=e[i].nxt)
	{
		int v=e[i].to;
		if (v==fa[u]) continue;
		if (vis[v]) {ljj=u,pjy=v,tmp=i; continue;}
		a[u][++a[u][0]]=v; fa[v]=u; dep[v]=dep[u]+1;
		dfs(v);
	}
}
void dfs2(int u)
{
	if (m==n-1) printf("%d ",u); else A[++an]=u;
	if (u==ljj) t1=an; if (u==pjy) t2=an;
	for (int i=1;i<=a[u][0];i++) dfs2(a[u][i]);
}
void dfs3(int u)
{
	if (vis[u]) return;
	b[++bn]=u; vis[u]=1;
	for (int i=1;i<=a[u][0];i++)
	{
		if (vis[lca]&&(a[u][i]==ljj||a[u][i]==pjy)) continue;
		dfs3(a[u][i]);
	}
}
void dfs4(int u)
{
	if (vis[u]) return;
	c[++cn]=u; vis[u]=1;
	for (int i=1;i<=a[u][0];i++) dfs4(a[u][i]);
}
void dfs5(int u)
{
	if (vis[u]) return;
	d[++dn]=u; vis[u]=1;
	for (int i=1;i<=a[u][0];i++) dfs5(a[u][i]);
}
void dfs6(int u)
{
	if (vis[u]) return;
	vis[u]=1; f[++fn]=u;
	int q[N],nn=0;
	for (int i=head[u];i;i=e[i].nxt) q[++nn]=e[i].to;
	sort(q+1,q+1+nn);
	for (int i=1;i<=nn;i++) if (q[i]!=tmp||vis[ljj]) dfs6(q[i]);
}
void dfs7(int u)
{
	if (vis[u]) return;
	vis[u]=1; f[++fn]=u;
	int q[N],nn=0;
	for (int i=head[u];i;i=e[i].nxt) q[++nn]=e[i].to;
	sort(q+1,q+1+nn);
	for (int i=1;i<=nn;i++) if (q[i]!=tmp||vis[eden]) dfs7(q[i]);
}
int main()
{
	Judge();
	n=read(); m=read();
	for (int i=1;i<=m;i++)
	{
		int u=read(),v=read();
		add(u,v); add(v,u);
	}
	dfs(1);
	for (int i=1;i<=n;i++) if (a[i][0]>0) sort(a[i]+1,a[i]+1+a[i][0]);
	dfs2(1);
	if (m==n)
	{
		for (int i=1;i<=n;i++) ans[i]=A[i];
		for (int i=1;i<=n;i++) vis[i]=0;
		int x=ljj,y=pjy;
		while (x!=y)
			if (dep[x]>dep[y]) x=fa[x]; else y=fa[y];
		lca=x;
		if (t1>t2) swap(ljj,pjy);
		x=ljj;
		while (1)
		{
			for (int i=1;i<=n;i++) vis[i]=0; fn=0;
			tmp=x;
			dfs6(1);
			for (int i=1;i<=n;i++) if (f[i]!=0&&f[i]<ans[i])
			{
				for (int j=1;j<=n;j++) ans[i]=f[i];
				break;
			}
			if (x==lca) break;
			x=fa[x];
		}
		x=pjy; int eden=0;
		if (x!=lca)
		{
			while (fa[x]!=lca) x=fa[x];
			eden=x;
		}
		x=pjy;
		while (x!=lca)
		{
			for (int i=1;i<=n;i++) vis[i]=0; fn=0;
			tmp=x;
			dfs7(1);
			for (int i=1;i<=n;i++) if (f[i]!=0&&f[i]<ans[i])
			{
				for (int j=1;j<=n;j++) ans[i]=f[i];
				break;
			}
			if (x==lca) break;
			x=fa[x];
		}
		a[ljj][++a[ljj][0]]=pjy;
		a[pjy][++a[pjy][0]]=ljj;
		sort(a[ljj]+1,a[ljj]+1+a[ljj][0]);
		sort(a[pjy]+1,a[pjy]+1+a[pjy][0]);
		for (int i=1;i<=n;i++) vis[i]=0;
		dfs5(1);
		for (int i=1;i<=n;i++) vis[i]=0;
		if (fa[ljj]) a[ljj][++a[ljj][0]]=fa[ljj];
		if (fa[pjy]) a[pjy][++a[pjy][0]]=fa[pjy];
		sort(a[ljj]+1,a[ljj]+1+a[ljj][0]);
		sort(a[pjy]+1,a[pjy]+1+a[pjy][0]);
		dfs3(1);
		for (int i=1;i<=n;i++) vis[i]=0;
		dfs4(1);
		/*for (int i=1;i<=n;i++) if (b[i]!=0&&b[i]<ans[i])
		{
			for (int j=1;j<=n;j++) ans[j]=b[j];
			break;
		}
		for (int i=1;i<=n;i++) if (c[i]!=0&&c[i]<ans[i])
		{
			for (int j=1;j<=n;j++) ans[j]=c[j];
			break;
		}
		for (int i=1;i<=n;i++) if (d[i]!=0&&d[i]<ans[i])
		{
			for (int j=1;j<=n;j++) ans[j]=d[j];
			break;
		}*/
		for (int i=1;i<=n;i++) printf("%d ",ans[i]);
	}
	return 0;
}
