#include<cstdio>
#include<cstdlib>
#include<string>
#include<iostream>
#include<algorithm>
using namespace std;
const int N=9,M=1000010,mod=1E9+7;
int n,m,f[M][1<<3],F[10][1<<8],tot;
int ljj;
int a[1<<8][1<<8],ans;
void Judge(){freopen("game.in","r",stdin);freopen("game.out","w",stdout);}
int read(){int d=0,f=1; char c=getchar(); while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();} while (c>='0'&&c<='9') d=d*10+c-48,c=getchar(); return d*f;}
void add(int &x,int y){x+=y; if (x>=mod) x-=mod;}
string b[1001],c[1001];
void dfs(int x,int y,string t,string tt)
{
	t+=(char)(a[x][y]+'0');
	if (x==n&&y==m)
	{
		tot++;
		b[tot]=t; c[tot]=tt;
		return;
	}
	string tmp=tt;
	tmp+=(char)(48);
	if (x<n) dfs(x+1,y,t,tmp);
	tmp=tt;
	tmp+=(char)(49);
	if (y<m) dfs(x,y+1,t,tmp);
}
bool ch(string a,string b)
{
	for (int i=0;i<a.length();i++) if (a[i]>b[i]) return 1; else if (a[i]<b[i]) return 0;
}
void check()
{
	for (int i=1;i<=tot;i++)
		for (int j=1;j<=tot;j++) if (i!=j)
			if (ch(c[i],c[j])&&ch(b[i],b[j]))
				return;
	ans++;
}
void dfs2(int x,int y)
{
	if (y>m) y=1,x++;
	if (x>n) {tot=0; dfs(1,1,"",""); check(); return;}
	a[x][y]=0; dfs2(x,y+1); a[x][y]=1; dfs2(x,y+1);
}
int main()
{
	Judge();
	n=read(); m=read();
	if (n<=3&&m<=3)
	{
		ans=0;
		dfs2(1,1);
		printf("%d",ans);
		return 0;
	}
	int t=1<<n;
	for (int i=0;i<t;i++)
		for (int j=0;j<t;j++)
		{
			a[i][j]=1;
			for (int k=1;k<n;k++)
				if ((i&(1<<k))&&!(j&(1<<(k-1))))
					a[i][j]=0;
		}
	if (n<=3)
	{
		for (int i=0;i<t;i++) f[1][i]=1;
		for (int i=2;i<=m;i++)
			for (int j=0;j<t;j++)
				for (int k=0;k<t;k++)
					if (a[k][j])
						add(f[i][j],f[i-1][k]);
		int ans=0;
		for (int i=0;i<t;i++) add(ans,f[m][i]);
		printf("%d",ans);
	}
	else
	{
		for (int i=0;i<t;i++) F[1][i]=1;
		for (int i=2;i<=m;i++)
			for (int j=0;j<t;j++)
				for (int k=0;k<t;k++)
					if (a[k][j])
						add(F[i][j],F[i-1][k]);
		int ans=0;
		for (int i=0;i<t;i++) add(ans,F[m][i]);
		printf("%d",ans);
	}
	return 0;
}
