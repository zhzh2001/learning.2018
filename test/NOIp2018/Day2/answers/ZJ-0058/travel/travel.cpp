#include<bits/stdc++.h>
using namespace std;

inline void F()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
}
const int maxn=5010;
int n,m,head[maxn],cnt,du[maxn],vis[maxn];
struct node
{
	int v,nxt;
}e[maxn<<1];
inline void add(int u,int v)
{
	e[++cnt].v=v;
	e[cnt].nxt=head[u];
	head[u]=cnt;
}
inline void dfs1(int x,int fa)
{
	int g[maxn],gcnt=0;
	printf("%d ",x);
	for (int i=head[x]; i; i=e[i].nxt)  g[++gcnt]=e[i].v;
	sort(g+1,g+gcnt+1);
	//if (x==1)
	 //for (int i=1; i<=gcnt; i++) cout<<g[i]<<' '; 
	for (int i=1; i<=gcnt; i++)
	if (g[i]!=fa)  dfs1(g[i],x);
	  
}
inline void dfs2(int x)
{
	int minx=99999;
	vis[x]=1;
	printf("%d ",x);
	for (int i=head[x]; i; i=e[i].nxt)  
	  if (!vis[e[i].v])minx=min(e[i].v,minx);
	if (minx!=99999) dfs2(minx);
}
int main()
{
  F();
  scanf("%d%d",&n,&m);
  bool p2=true;
  for (int i=1; i<=m; i++)
    {
    	int x,y;
    	scanf("%d%d",&x,&y);
    	add(x,y);
    	add(y,x);
    	du[x]++;
    	du[y]++;
    	if (du[x]>2||du[y]>2) p2=false;
    }
  if (m==n-1) {dfs1(1,0); return 0;}
  if (p2) {dfs2(1); return 0;}
}
