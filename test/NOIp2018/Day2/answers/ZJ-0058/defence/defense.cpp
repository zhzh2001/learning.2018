#include<bits/stdc++.h>
using namespace std;

inline void F()
{
	freopen("defence.in","r",stdin);
	freopen("defence.out","w",stdout);
}
const int maxn=5010;
long long n,m,head[maxn],a[maxn],fa[maxn],cnt,du[maxn];
long long f[maxn][5];
struct node
{
	int v,nxt;
}e[maxn<<1];
inline void add(int u,int v)
{
	e[++cnt].v=v;
	e[cnt].nxt=head[u];
	head[u]=cnt;
}
inline void dp(int u,int fath)
{
	long long minest=99999999999;
	int minj=0;
	bool p=false;
	for (int i=head[u]; i; i=e[i].nxt)
	  {
	  	int vv=e[i].v;
	  	//cout<<vv<<' '<<fath<<endl;
	  	if (vv==fath) continue;
	  	p=true;
	  	dp(vv,u);
	  	fa[vv]=u;
	  	//cout<<"CCCF"<<endl;
	  	//cout<<u<<" "<<vv<<endl;
	  	if (f[vv][1]<minest) minj=vv,minest=f[vv][1];
		f[u][0]+=min(f[vv][0],f[vv][1]);
	    f[u][1]=min(f[vv][1],min(f[vv][0],f[vv][2]));
	    f[u][2]=min(f[vv][1],min(f[vv][0],f[vv][2]));
	  }
	if (!p) f[u][1]=a[u],f[u][0]=0,f[u][2]=a[fath];
	if (p) f[u][0]=f[u][0]-min(f[minj][0],f[minj][1])+minest,f[u][1]+=a[u],f[u][2]+=a[fath];
	cout<<u<<"  son "<<f[u][0]<<" self "<<f[u][1]<<"  father "<<f[u][2]<<endl;
}
int main()
{
  F();
  scanf("%d%d",&n,&m);
  string c3;
  cin>>c3;
  for (int i=1; i<=n; i++) scanf("%d",&a[i]);
  for (int i=1; i<n; i++)
    {            
    	int x,y;
    	scanf("%d%d",&x,&y);
    	add(x,y);
    	du[x]++;
    	du[y]++;
    	add(y,x);
    }
  dp(1,0);
  long long res=min(f[1][1],f[1][0]);
  while (m--)
    {
    	int x,yx,y,yy;
    	scanf("%d%d%d%d",&x,&yx,&y,&yy);
    	bool p=true;
    	if (du[x]==1||du[y]==1)
    	for (int i=head[x]; i; i=e[i].nxt)
    	  if (e[i].v==y) {printf("-1\n"),p=false;}
    	if (!p) continue;
    	memset(f,0,sizeof(f));
    	dp(1,0);
    	printf("%lld\n",min(f[1][1],f[1][0]));
    }
}
