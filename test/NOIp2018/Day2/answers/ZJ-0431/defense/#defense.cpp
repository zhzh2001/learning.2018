#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <limits>
#include <functional>
#include <numeric>
#include <utility>
#include <set>
using namespace std;

typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
bool isneg;
template <typename Int> inline void RI(Int &x) {
	x = isneg = 0;
	char ch;
	for (ch = getchar(); isspace(ch); ch = getchar())
		;
	if (ch == '-') {
		isneg = 1, ch = getchar();
	}
	for (; isdigit(ch); ch = getchar())
		x = x * 10 + ch - '0';
	if (isneg) x = -x;
}
}

using IO::RI;

const int kMaxN = 1e5 + 5, kInf = 0x3f3f3f3f;
int n, m, cost[kMaxN];
char typ[4];
int head[kMaxN], ecnt;
struct Edge {
	int to, nxt;
} e[kMaxN << 1];

void Insert(int x, int y) {
	e[++ecnt] = (Edge) { y, head[x] };
	head[x] = ecnt;
}

set<pii> S;

namespace SubTask1 {
int f[2][2005]; // 0, don't choose; 1, choose
int A, X, B, Y;
int Get(int x, int stat) {
	if (x == A && stat == X)
		return 0;
	if (x == B && stat == Y)
		return 0;
	return 1;
}
void DFS(int x, int fa) {
	f[1][x] = cost[x];
	f[0][x] = 0;
	if (!Get(x, 0)) {
		f[1][x] = kInf;
	} else if (!Get(x, 1)) {
		f[0][x] = kInf;	
	}
	bool flag = true, flag1 = true;
	for (int i = head[x]; i; i = e[i].nxt) {
		int to = e[i].to;
		if (to == fa)
			continue;
		DFS(to, x);
		f[0][x] += f[1][to];
		f[1][x] += min(f[1][to], f[0][to]);
	}
}
int Solve() {
	for (int i = 1; i <= m; ++i) {
		RI(A), RI(X), RI(B), RI(Y);
		if (X == 0 && Y == 0 && S.find(make_pair(A, B)) != S.end()) {
			puts("-1");
			continue;
		}
		memset(f, 0x3f, sizeof f);
		DFS(1, 0);
		printf("%d\n", min(f[0][1], f[1][1]));
	}
	return 0;
}
}

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	RI(n), RI(m), scanf("%s", &typ);
	bool islist = 0;
	if (typ[0] == 'A')
		islist = 1;
	for (int i = 1; i <= n; ++i) {
		RI(cost[i]);
	}
	for (int i = 1, u, v; i < n; ++i) {
		RI(u), RI(v);
		Insert(u, v);
		Insert(v, u);
		S.insert(make_pair(u, v));
		S.insert(make_pair(v, u));
	}
	if (n <= 2000) { // O(n \times m), 44pts
		SubTask1::Solve();
		return 0;
	}
	return 0;
}
