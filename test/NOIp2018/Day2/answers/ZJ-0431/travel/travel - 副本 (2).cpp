#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <limits>
#include <functional>
#include <numeric>
#include <utility>
#include <vector>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
inline bool Fetch() {
	return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T;
}
inline char NC() {
	return S == T ? (Fetch() ? *S++ : EOF) : *S++;
}
bool isneg;
template <typename Int> inline void RI(Int &x) {
	x = isneg = 0;
	char ch;
	for (ch = NC(); isspace(ch); ch = NC())
		;
	if (ch == '-') {
		isneg = 1, ch = NC();
	}
	for (; isdigit(ch); ch = NC())
		x = x * 10 + ch - '0';
	if (isneg) x = -x;
}
}

using IO::RI;

const int kMaxN = 5005, kInf = 0x3f3f3f3f;
int n, m, ans[kMaxN];
vector<int> G[kMaxN];

namespace SubTask1 {
void DFS1(int x, int f) {
	ans[++ans[0]] = x;
	for (int i = 0; i < (int)G[x].size(); ++i) {
		int to = G[x][i];
		if (to == f)
			continue;
		DFS1(to, x);
	}
}
int Solve() {
	DFS1(1, 0);
	for (int i = 1; i <= ans[0]; ++i) {
		printf("%d%c", ans[i], " \n"[i == ans[0]]);
	}
	return 0;
}
}

namespace SubTask2 {
bool vis[kMaxN], isfind, oncyc[kMaxN];
int dep[kMaxN], stkd[kMaxN], belong[kMaxN];
void FindCycle(int x, int fa) {
	vis[x] = true;
	dep[x] = dep[fa] + 1;
	stkd[dep[x]] = x;
	for (int i = 0; i < (int)G[x].size(); ++i) {
		int to = G[x][i];
		if (to == fa)
			continue;
		if (vis[to]) { // find circle
			for (int i = dep[x]; i >= dep[to]; --i) {
				oncyc[stkd[i]] = true;
				printf("%d\n", stkd[i]);
			}
			isfind = 1;
			vis[x] = false;
			return;
		}
		FindCycle(to, x);
	}
	vis[x] = false;
}
void DFS1(int x, int fa) {
	belong[x] = belong[fa];
	for (int i = 0; i < (int)G[x].size(); ++i) {
		int to = G[x][i];
		if (to == fa || oncyc[to])
			continue;
		DFS1(to, x);
	}
}
void DFS4(int x, int f) {
	ans[++ans[0]] = x;
	vis[x] = true;
	printf("4 %d\n", x);
	for (int i = 0; i < (int)G[x].size(); ++i) {
		int to = G[x][i];
		if (vis[to])
			continue;
		if (oncyc[to])
			continue;
		DFS4(to, x);
	}
}
void DFS3(int x, int f, int cho) {
	if (cho != -1 && x > cho) {
		return;
	}
	ans[++ans[0]] = x;
	printf("3-%d %d\n", cho != -1, x);
	vis[x] = true;
	for (int i = 0; i < (int)G[x].size(); ++i) {
		int to = G[x][i];
		if (vis[to])
			continue;
		if (oncyc[to]) {
			DFS3(to, x, cho);
		} else {
			DFS4(to, x);
		}
	}
}
void DFS2(int x, int f) {
	ans[++ans[0]] = x;
	printf("2 %d\n", x);
	vis[x] = true;
	if (oncyc[x]) {
		int cnt = 0;
		for (int i = 0; i < (int)G[x].size(); ++i) {
			int to = G[x][i];
			if (vis[to])
				continue;
			if (oncyc[to]) {
				int cho = 0;
				if (++cnt == 1) {
					for (int j = i + 1; j < (int)G[x].size(); ++j) {
						int to = G[x][j];
						if (vis[to])
							continue;
						cho = to;
						break;
					}
				} else {
					cho = -1;
				}
				DFS3(to, x, cho);
			}
		}
	} else {
		for (int i = 0; i < (int)G[x].size(); ++i) {
			int to = G[x][i];
			if (vis[to])
				continue;
			DFS2(to, x);
		}
	}
}
int Solve() {
	isfind = 0;
	FindCycle(1, 0);
	for (int i = 1; i <= n; ++i) {
		if (oncyc[i]) {
			belong[i] = i;
			DFS1(i, i);
		}
	}
	DFS2(1, 0);
	for (int i = 1; i <= ans[0]; ++i) {
		printf("%d%c", ans[i], " \n"[i == ans[0]]);
	}
	return 0;
}
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	RI(n), RI(m);
	for (int i = 1, u, v; i <= m; ++i) {
		RI(u), RI(v);
		G[u].push_back(v);
		G[v].push_back(u);
	}
	for (int i = 1; i <= n; ++i) {
		sort(G[i].begin(), G[i].end());
	}
	if (m == n - 1) { // 60pts
		SubTask1::Solve();
		return 0;
	} else { // 40pts
		SubTask2::Solve();
		return 0;
	}
	return 0;
}
