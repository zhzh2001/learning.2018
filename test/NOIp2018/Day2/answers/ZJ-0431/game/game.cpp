#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <limits>
#include <functional>
#include <numeric>
#include <utility>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 8, kMod = 1e9 + 7;

int n, m, a[10][10], last, ans;
bool flag;

void Check(int x, int y, int stat) {
	if (x == n && y == m) {
		if (stat < last) {
			flag = 0;
		}
		last = stat;
		return;
	}
	if (x > n || y > m)
		return;
	Check(x + 1, y, stat << 1 | a[x + 1][y]);
	if (!flag)
		return;
	Check(x, y + 1, stat << 1 | a[x][y + 1]);
}

void DFS1(int x, int y) {
	if (y > m) {
		++x, y = 1;
		if (x == n + 1) {
			flag = true;
			last = 0;
			Check(1, 1, a[1][1]);
			ans += flag;
			return;
		}
	}
	a[x][y] = 0;
	DFS1(x, y + 1);
	a[x][y] = 1;
	DFS1(x, y + 1);
}

ll QuickPow(ll x, ll y, ll p) {
	ll ret = 1;
	x %= p;
	for (; y; y >>= 1, x = x * x % p) {
		if (y & 1)
			ret = ret * x % p;
	}
	return ret;
}

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m); 
//	if (false) {
	if (n <= 4 && m <= 4) { // 20pts
		DFS1(1, 1);
		printf("%d\n", ans);
		return 0;
	} else if (n <= 5) {
		if (n == 1) {
			printf("%lld\n", QuickPow(2, m, kMod));
		}
		if (n == 2) { // 30pts
			printf("%lld\n", 4LL * QuickPow(3, m - 1, kMod) % kMod);
		}
		if (n == 3) { // 15pts
			printf("%lld\n", QuickPow(3, m - 3, kMod) * 112LL % kMod);
		}
		if (n == 4) {
			printf("%lld\n", QuickPow(3, m - 4, kMod) * 912LL % kMod);
		}
		if (n == 5) {
			printf("%lld\n", QuickPow(3, m - 5, kMod) * 7136LL % kMod);
		}
		return 0;
	}
	return 0;
}
