#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define INF 1e9
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

int N,M;
int u1[5005],v1[5005];
int du[5005];
int a[5005][5005];
int Ans[5005];
int tmp[5005];
bool flag1[5005];
int Len1=0;

void print()
{
	fo(i,1,N - 1)
	{
		printf("%d ",Ans[i]);
	}
	printf("%d\n",Ans[N]);
}

bool check()
{
	bool flag=1;
	fo(i,1,N)
	{
		flag=flag & flag1[i];
	}
	return flag;
}

void dfs(int u,int tmp)
{
	if (! flag1[u]) Ans[++Len1]=u;
	flag1[u]=1;
	fo(i,1,N)
	{
		if ((! flag1[i]) && a[u][i])
		{
			dfs(i,tmp - 1);
		}
	}
}

void dfs1(int u)
{
	if (! flag1[u]) tmp[++Len1]=u;
	flag1[u]=1;
	fo(i,1,N)
	{
		if ((! flag1[i]) && a[u][i])
		{
			dfs1(i);
		}
	}
}

bool Small()
{
	fo(i,1,N)
	{
		if (Ans[i] == tmp[i]) continue;
		if (Ans[i] > tmp[i]) return 1;
		if (Ans[i] < tmp[i]) return 0;
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&N,&M);
	fo(i,1,M)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		u1[i]=u; v1[i]=v;
		a[u][v]=a[v][u]=1;
		du[u]++; du[v]++;
	}
	if (M == N - 1)
	{
		cl(Ans,0);
		cl(flag1,0);
		dfs(1,N);
		print();
	}
	else
	{
		fo(i,1,N)
		{
			Ans[i]=INF;
		}
		fo(i,1,M)
		{
			if (du[u1[i]] == 1 || du[v1[i]] == 1)
			{
				continue;
			}
			a[u1[i]][v1[i]]=a[v1[i]][u1[i]]=0;
			Len1=0; cl(flag1,0);
			dfs1(1);
			if (Small())
			{
				fo(i,1,N)
				{
					Ans[i]=tmp[i];
				}
			}
			a[u1[i]][v1[i]]=a[v1[i]][u1[i]]=1;
		}
		print();
	}
	return 0;
}
