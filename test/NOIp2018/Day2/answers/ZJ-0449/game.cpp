#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

int Len1,Tmp;
ll Mod1=1000000007;
ll mi[4000005];
char a[5005][5005];
string SX[1005];
string SY[1005];
int N,M;

ll calc(int X)
{
	return (mi[X] - (ll) mi[X / 2] * (ll) (mi[X / 2] - 1) / 2 % Mod1 + Mod1) % Mod1;
}

ll add(ll a,ll b)
{
	return (a + b) % Mod1;
}

void dfs1(int X1,int Y1,string S1,string S2)
{
	S2=S2 + a[X1][Y1];
	if (X1 == N && Y1 == M)
	{
		SX[++Len1]=S1;
		SY[Len1]=S2;
		return;
	}
	if (X1 + 1 <= N) dfs1(X1 + 1,Y1,S1 + 'D',S2);
	if (Y1 + 1 <= M) dfs1(X1,Y1 + 1,S1 + 'R',S2);	
}

bool Bigger(int XX,int YY)
{
	fo(i,0,N + M - 2 - 1)
	{
		if (SX[XX][i] < SX[YY][i])
		{
			return 0;
		}
		if (SX[XX][i] > SX[YY][i])
		{
			return 1;
		}
	}
	return 0;
}

bool Smaller(int XX,int YY)
{
	fo(i,0,SY[XX].length() - 1)
	{
		if (SY[XX][i] > SY[YY][i])
		{
			return 0;
		}
		if (SY[XX][i] < SY[YY][i])
		{
			return 1;
		}
	}
	return 1;
}

bool check()
{
	fo(i,1,Len1)
	{
		fo(j,1,Len1)
		{
			if (i == j) continue;
			if (Bigger(i,j) && ! Smaller(i,j))
			{
				return 0;
			}
		}
	}
	return 1;
}

void dfs(int X1,int Y1)
{
	if (X1 == N && Y1 == M)
	{
		a[N][M]='0';
		Len1=0;
		dfs1(1,1,"","");
		if (check())
		{
			Tmp++;
			if (Tmp >= Mod1) Tmp-=Mod1;
		}
		a[N][M]='1';
		Len1=0;
		dfs1(1,1,"","");
		if (check())
		{
			Tmp++;
			if (Tmp >= Mod1) Tmp-=Mod1;
		}
		return;
	}
	a[X1][Y1]='0';
	int NextX=X1; int NextY=Y1 + 1;
	if (NextY > M) 
	{
		NextY=1;
		NextX++;
	}
	dfs(NextX,NextY);
	a[X1][Y1]='1';
	dfs(NextX,NextY);
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&N,&M);
	if (N > M)
	{
		swap(N,M);
	}
	if (N == 1 || M == 1)
	{
		puts("0");
		return 0;
	}
	mi[0]=1;
	fo(i,1,4 * M)
	{
		mi[i]=mi[i - 1] * (ll) (3) % Mod1;
	}
	if (N == 2)
	{
		ll Ans=12;
		Ans=Ans * mi[M - 2] % Mod1;
		printf("%lld\n",Ans);
	}
	else if (N == 3)
	{
		ll Ans=112;
		Ans=Ans * mi[M - 3] % Mod1;
		printf("%lld\n",Ans);
	}
	else 
	{
		Tmp=0;
		dfs(1,1);
		printf("%d\n",Tmp);
	}
	return 0;
}
