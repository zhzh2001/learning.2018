#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define INF 1e9
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

int dp[100005][3];
int p[1000005];
int N,M,Min1;
char S1[15];
int flag1[100005];
int num;
int vet[200005],Next[200005],head1[200005];
int u1[100005],v1[100005];


void add(int u,int v)
{
	vet[++num]=v;
	Next[num]=head1[u];
	head1[u]=num;
}

void dfs(int u,int fa,int Sum,int t)
{
	if (flag1[u] == 0 || flag1[u] == -1) t++;
	if (t == N)
	{
		Min1=min(Min1,Sum);
		return;
	}
	if (flag1[u] == -1)
	{
		if (u == 1)
		{
			for (int e=head1[u]; e != -1; e=Next[e])
			{
				int V=vet[e];
				if (V == u || flag1[V] > 0) continue;
				dfs(V,u,Sum,t);
			}
		}
		else
		{
			if (flag1[fa] != 1)
			{
				return;
			}
			for (int e=head1[u]; e != -1; e=Next[e])
			{
				int V=vet[e];
				if (V == u || flag1[V] > 0) continue;
				dfs(V,u,Sum,t);
			}
		}
	}
	else if (u == 1 || flag1[fa] == 1)
	{
		flag1[u]=2;
		for (int e=head1[u]; e != -1; e=Next[e])
		{
			int V=vet[e];
			if (V == u || flag1[V] > 0) continue;
			dfs(V,u,Sum,t);
		}
		flag1[u]=1;
		for (int e=head1[u]; e != -1; e=Next[e])
		{
			int V=vet[e];
			if (V == u || flag1[V] > 0) continue;
			dfs(V,u,Sum + p[u],t);
		}
	}
	else if (flag1[fa] == 2 || flag1[fa] == -1)
	{
		flag1[u]=1;
		for (int e=head1[u]; e != -1; e=Next[e])
		{
			int V=vet[e];
			if (V == u || flag1[V]  > 0) continue;
			dfs(V,u,Sum + p[u],t);
		}
	}
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&N,&M);
	cl(head1,-1);
	scanf("%s",S1);
	fo(i,1,N)
	{
		scanf("%d",&p[i]);
	}
	fo(i,1,N - 1)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		u1[i]=u; v1[i]=v;
		if (u1[i] > v1[i]) swap(u1[i],v1[i]);
		add(u,v);
		add(v,u);
	}
	if (S1[0] == 'A')
	{
		dp[0][0]=dp[0][1]=0;
		fo(XXX,1,M)
		{
			cl(flag1,0);
			int A1,X1,B1,Y1;
			scanf("%d%d%d%d",&A1,&X1,&B1,&Y1);
			if (X1 == 0) flag1[A1]=-1;
			if (Y1 == 0) flag1[B1]=-1;
			if (X1 == 1) flag1[A1]=1;
			if (Y1 == 1) flag1[B1]=1;
			fo(i,1,N)
			{
				dp[i][0]=dp[i][1]=INF;
			}
			fo(i,1,N)
			{
				if (flag1[i] == 0 && flag1[i - 1] == 0)
				{
					dp[i][1]=min(dp[i - 1][0] + p[i],dp[i - 1][1] + p[i]);
					dp[i][0]=dp[i - 1][1];
				}
				if (flag1[i - 1] == -1)
				{
					dp[i][1]=dp[i - 1][0] + p[i];
				}
				if (flag1[i] == -1)
				{
					dp[i][0]=dp[i - 1][1] + p[i];
				}
				if (flag1[i] == 1)
				{
					dp[i][1]=min(dp[i - 1][0] + p[i],dp[i - 1][1] + p[i]);
				}
				if (flag1[i - 1] == 1)
				{
					dp[i][1]=dp[i - 1][1] + p[i];
					dp[i][0]=dp[i - 1][1];
				}
			}
			printf("%d\n",min(dp[N][0],dp[N][1]));
		}
	}
	else
	if (N <= 10)
	{
		fo(i,1,M)
		{
			int A1,X1,B1,Y1;
			scanf("%d%d%d%d",&A1,&X1,&B1,&Y1);
			bool flagX=1;
			if (Y1 + X1 == 0)
			{
				int TX=A1; int TY=B1;
				if (TX > TY) swap(TX,TY);
				fo(j,1,N)
				{
					if (u1[j] == TX && v1[j] == TY)
					{
						flagX=0;
						break;
					}
				}
			}
			int Ans=0;
			if (X1 == 0) flag1[A1]=-1;
			if (X1 == 1) Ans+=p[A1];
			if (Y1 == 0) flag1[B1]=-1;
			if (Y1 == 1) Ans+=p[B1];
			Min1=INF;
			if (flagX) dfs(1,0,Ans,0);
			if (Min1 == INF)
			{
				puts("-1");
			}
			else
			{
				printf("%d\n",Min1);
			}
			cl(flag1,0);
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
