#include<bits/stdc++.h>
#define M 1005
using namespace std;
int n,m,a,b,x,y;
long long s[M],dp[2][M];
char A[5];
vector<int>E[M];
void dfs(int now,int f) {
	dp[0][now]=0;
	dp[1][now]=s[now];
	for(int i=0; i<E[now].size(); i++) {
		int v=E[now][i];
		if(v==f)continue;
		dfs(v,now);
		dp[0][now]+=dp[1][v];
		dp[1][now]+=min(dp[0][v],dp[1][v]);
	}
	if(now==a)dp[1-x][now]=1e15;
	if(now==b)dp[1-y][now]=1e15;
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,A);
	for(int i=1; i<=n; i++)scanf("%lld",&s[i]);
	for(int i=1;i<n;i++){
		int c,d;
		scanf("%d%d",&c,&d);
		E[c].push_back(d);
		E[d].push_back(c);
	}
	while(m--) {
		bool f=false;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		dfs(1,0);
		long long ans;
		ans=min(dp[1][1],dp[0][1]);
		if(ans>1e14)ans=-1;
		printf("%lld\n",ans);
	}
	return 0;
}
