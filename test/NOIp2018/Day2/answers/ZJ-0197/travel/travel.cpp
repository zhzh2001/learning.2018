#include<bits/stdc++.h>
using namespace std;
int n,m,st,H,stt,L[5005],fa[5005],now[5005],ans[5005],dep[5005],stk[5005],ffa[5005];
vector<int>E[5005];
bool used=true,how[5005],mark[5005];
struct p60 {
	void dfs(int x,int f) {
		fa[x]=f;
		if(!mark[x])ans[++st]=x;
		if(st==n)return ;
		mark[x]=true;
		int to=1e9;
		for(int i=0; i<E[x].size(); i++) {
			int v=E[x][i];
			if(v==f)continue;
			if(!mark[v])to=min(to,v);
		}
		if(to==1e9)dfs(fa[x],fa[fa[x]]);
		else dfs(to,x);
	}
	void solve() {
		dfs(1,0);
		for(int i=1; i<=n; i++)printf("%d ",ans[i]);
	}
} P60;
struct px {
	int how1,how2;
	void Find(int x,int f) {
		if(how1&&how2)return ;
		if(mark[x]) {
			how1=x;
			how2=f;
			return ;
		}
		dep[x]=dep[f]+1;
		mark[x]=true;
		fa[x]=f;
		for(int i=0; i<E[x].size(); i++) {
			int v=E[x][i];
			if(v==f)continue;
			Find(v,x);
			if(how1&&how2)return ;
		}
	}
	void dfs(int x,int f) {
		if(!mark[x])now[++st]=x;
		if(st==n)return ;
		ffa[x]=f;
		mark[x]=true;
		int to=1e9;
		for(int i=0; i<E[x].size(); i++) {
			int v=E[x][i];
			if(v==f||(v==H&&x==fa[H])||(v==fa[H]&&x==H))continue;
			if(!mark[v])to=min(to,v);
		}
		if(to==1e9)dfs(ffa[x],ffa[ffa[x]]);
		else dfs(to,x);
	}
	void solve() {
		how1=how2=0;
		ans[1]=2;
		Find(1,0);
		int x=how1,y=how2;
		while(x!=y) {
			if(dep[x]>dep[y]) {
				how[x]=true;
				stk[++stt]=x;
				x=fa[x];
			} else {
				how[y]=true;
				stk[++stt]=y;
				y=fa[y];
			}
		}
		if(n>=1000)stt=min(stt,50);
		for(int i=1; i<=stt; i++) {
			H=stk[i];
			st=0;
			memset(mark,0,sizeof(mark));
			dfs(1,0);
			for(int j=1; j<=n; j++) {
				if(now[j]>ans[j])break;
				if(now[j]<ans[j]) {
					for(int k=1; k<=n; k++)ans[k]=now[k];
					break;
				}
			}
		}
		for(int i=1; i<n; i++)printf("%d ",ans[i]);
		printf("%d",ans[n]);
	}
} Px;
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1; i<=m; i++) {
		int a,b;
		scanf("%d%d",&a,&b);
		E[a].push_back(b);
		E[b].push_back(a);
	}
	if(m==n-1)P60.solve();
	else Px.solve();
	return 0;
}
