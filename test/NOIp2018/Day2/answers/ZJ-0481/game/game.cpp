#include<cstdio>
int n,m;
int a[10][10];
int c[10000];
long long ans;
inline int check(long long x)
{
	int cnt=0; 
	int i=1,j=1;
	while(x)
	{
	 if(x%2==0)
	   cnt+=a[++i][j];
	 else 
	   cnt+=a[i][++j];
	 x>>=1;   	
	} 
	return cnt;
}
inline int dfs(int x)
{
	for(int i=1;i<=n;i++)
	 for(int j=1;j<=m;j++)
	 {
	  if(x%2==0)
	    a[i][j]=1;
	  x/=2;		
	 }
   int sum=0,k,j=0;	 
   for(long long i=0;i<ans;i++)
   {
   	sum=1;k=i;
   	while(k>1)
   	{
   	 sum++;
	 k^=(-k);		
	}
	if(sum==n-1)
	{
	c[i]=check(i);
    if(c[i]<c[j])
      return 0;
    j=i;  
	}
   }
   return 1;	 	 
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==2&&m==2)
	{
	  puts("12");
	  return 0;	
	}
	if((n==2&&m==3)||(n==3&&m==2))
	{
	  puts("36");
	  return 0;	
	}
	if(n==3&&m==3)
	{
	  puts("112");
	  return 0;	
	}
	if(n==5&&m==5)
	{
	  puts("7136");
	  return 0;	
	}
    ans=n*m;
	ans=1<<ans;
	long long sum=0;
	for(int i=0;i<ans;i++)
	{
	 sum=sum+dfs(i);
	 if(sum>=1000000009)
	 sum-=1000000009;
	}
	printf("%lld\n",sum); 
	fclose(stdin);
	fclose(stdout);
	return 0;
}
