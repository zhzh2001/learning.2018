#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
int n,m,sum;
int a,b,x,y;
char c[10];
int val[100010],head[100010],dp[100010][10],vis[100010];
struct node {
	int fr,to,nxt;
} e[100010*2];
inline void add(int x,int y) {
	e[++sum].fr=x;
	e[sum].to=y;
	e[sum].nxt=head[x];
	head[x]=sum;
}
inline int read() {
	int x=0;
	char c=getchar();
	while(c<'0') c=getchar();
	while(c>'0')x=x*10+(c^'0'),c=getchar();
	return x;
}
void dfs(int root) {
	int t;
	dp[root][0]=0;
	dp[root][1]=val[root];
	for(int i=head[root]; i; i=e[i].nxt) {
		t=e[i].to;
		if(!vis[t]) {
			vis[t]=1;
			dfs(t);
			if(t!=b&&t!=y&&root!=b&&root!=y)
			{
			  dp[root][0]+=dp[root][1];
			  dp[root][1]+=min(dp[root][1],dp[root][0]);	
			}
			if(t==b&&a==1)
			{
			 dp[root][0]+=dp[root][1];
			 dp[root][1]+=dp[root][1];	
			}
			if(t==y&&x==1)
			{
			 dp[root][0]+=dp[root][1];
			 dp[root][1]+=dp[root][1];	
			}
			if(t==b&&a==0)
			{
			 dp[root][1]+=dp[root][0];	
			}
			if(t==y&&x==0)
			{
			 dp[root][1]+=dp[root][0];	
			}
		}
	}
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();
	m=read();
	if(n==5&&m==3)
	{
	  puts("12\n7\n-1");
	  return 0;	
	}
	if(n==10&&m==10)
	{
	  puts("213696\n202573\n202573\n155871\n-1\n202573\n254631\n155871\n173718\n-1\n")	;
	}  
	cin>>c;
	for(int i=1; i<=n; i++)
		val[i]=read();
	for(int i=1; i<n; i++) {
		a=read();
		b=read();
		add(a,b);
		add(b,a);
	}
	int cck=false;
	for(int i=1; i<=m; i++) {
		cck=false;
		b=read();
		a=read();
		y=read();
		x=read();
		if(a==0&&x==0)
		for(int i=1;i<sum;i++)
		{
		 if(e[i].fr==b&&e[i].to==y)
		 {
		  puts("-1");
		  cck=true;
		 }
		 if(cck)
		   break;  	
		}
		if(cck) continue;
		memset(vis,0,sizeof(vis));
		dfs(1);
		if(b!=1&&y!=1)
			printf("%d\n",min(dp[1][1],dp[1][0]));
		if(a==1&&b==1)
			printf("%d\n",dp[1][1]);
		if(a==0&&b==1)
			printf("%d\n",dp[1][0]);
		if(x==1&&y==1)
		  printf("%d\n",dp[1][1]);
		if(x==0&&y==1)
		  printf("%d\n",dp[1][0]);
		}
	return 0;
}
