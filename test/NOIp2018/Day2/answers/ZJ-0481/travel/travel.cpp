#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
using namespace std;
struct node {
	int fr,to;
} e[10010];
int cnt[5010],vis[5010],p[5010],f[5010],mp[5010];
int n,m,sum;
bool cck=false;
inline bool cmp(node a,node b) {
	if(a.fr==b.fr) return a.to<b.to;
	return a.fr<b.fr;
}
inline void add(int x,int y)
{
	e[++sum].fr=x;
	e[sum].to=y;
}
void print()
{
	for(int i=1;i<=n;i++)
	 printf("%d ",p[i]);
	puts("");
	cck=true; 
}
void dfs(int k,int st) {
	int t;
	if(cck)
	  return ;
	if(st>=n)
	  print();
	for(int i=cnt[k];i<cnt[k+1]; i++) {
		t=e[i].to;
		if(!vis[t]) {
		  vis[t]=1;
		  p[st+1]=t;
		  f[t]=k;	
          dfs(t,st+1);
          vis[t]=0;
          p[st+1]=0;
          f[t]=0;
		}
		if(vis[t]&&f[k]==t)
		{
		  dfs(t,st);
		}
	}
	
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	int a,b;
	for(int i=1; i<=m; i++)
	{
		scanf("%d%d",&a,&b);
		add(a,b);
		add(b,a);
	}		
	sort(e+1,e+sum,cmp);
	int j=1;
	for(int i=1; i<=n; i++) {
		cnt[i]=j;
		while(1)
		{
		 if(e[j].fr==i)
		   j++;	  
		 else 
		   break;
		 if(j>sum-1)
		   break;    
		}
    }
	for(int i=1; i<=n; i++) {
		if(cck)
		  break;
		memset(vis,0,sizeof(vis));
		memset(p,0,sizeof(p));
		memset(f,0,sizeof(f));
		vis[i]=1;
		p[1]=i;
		dfs(i,1);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
