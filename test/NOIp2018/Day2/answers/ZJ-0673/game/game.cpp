#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>
#include <utility>
#include <queue>
#include <vector>
#include <map>
#include <set>
using namespace std;
#define ll long long
#define fs first
#define sc second
#define pi pair<int,int>
#define mem(a,b) memset(a,b,sizeof(a))

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
}

const int mod=1e9+7;

int n,m;
int a[15][15];
int ans;
int s[1000005];

void add(int x)
{
	for(int i=x;i<=(1<<(n+m-1))+1;i+=i&(-i))
		s[i]+=1;
}

int ask(int x)
{
	int re=0;
	for(int i=x;i>0;i-=i&(-i))
		re+=s[i];
	return re;
}

bool judge()
{
	int i,j,cou,x,y,io;
	for(i=0;i<=(1<<8)+1;i++)
		s[i]=0;
	for(i=0;i<=(1<<(n+m-2))-1;i++)
	{
		cou=0;
		for(j=0;j<=n+m-3;j++)
			if((1<<j)&i)
				cou++;
		if(cou!=m-1) continue;
		x=1,y=1;io=a[x][y];
		for(j=n+m-3;j>=0;j--)
		{
			if((1<<j)&i)
				y++;
			else
				x++;		
			io=io*2+a[x][y];
		}
		io++;
		if(ask(io-1)>0)
			return 0;
		add(io);
	}
	return 1;
}

void dfs(int x,int y)
{
	if(x==n&&y==m)
	{
		a[x][y]=1;
		if(judge())
			ans=(ans+1)%mod;
		a[x][y]=0;
		if(judge())
			ans=(ans+1)%mod;
		return ;
	}		
	if(y==m)
	{
		a[x][y]=1;
		dfs(x+1,1);
		a[x][y]=0;
		dfs(x+1,1);
	}
	else if(x==1&&y==1)
	{
		a[x][y]=0;
		dfs(x,y+1);
	}
	else
	{
		a[x][y]=1;
		dfs(x,y+1);
		a[x][y]=0;
		dfs(x,y+1);
	}
}

int pw(int x,int y)
{
	int re=1,ai=x;
	while(y)
	{
		if(y&1)
			re=(1ll*re*ai)%mod;
		ai=(1ll*ai*ai)%mod;
		y>>=1;	
	}
	return re;
} 

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if(n==1)
		printf("%d\n",pw(2,m)%mod);
	else if(m==1)
		printf("%d\n",pw(2,n)%mod);
	else if(n<=3&&m<=3)
	{
		dfs(1,1);
		if(m==1)
			printf("%d\n",ans);
		else
			printf("%d\n",ans*2);
	}
	else if(n==2)
	{
		if(m==2) printf("12\n");
		else
			printf("%d\n",(int)(12ll*pw(3,m-2)%mod));
	}
	else if(n==3)
	{
		if(m==3) printf("112\n");
		else
			printf("%d\n",(int)(112ll*pw(3,m-3)%mod));
	}
	else
	{
		dfs(1,1);
		if(m==1)
			printf("%d\n",ans);
		else
			printf("%d\n",ans*2);
	}
	return 0;
}

