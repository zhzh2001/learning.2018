#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>
#include <utility>
#include <queue>
#include <vector>
#include <map>
#include <set>
using namespace std;
#define ll long long
#define fs first
#define sc second
#define pi pair<int,int>
#define mem(a,b) memset(a,b,sizeof(a))

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
}

struct edge
{
	int to,next;
}e[300005];

int n,m,cnt;
int head[100005];
char type[15];
int p[200005],v[200005];
ll dp[2][200005];

void addedge(int x,int y)
{
	e[++cnt].to=y;e[cnt].next=head[x];head[x]=cnt;
}

void dfs(int x,int f)
{
	int i,vi,on1=0;
	for(i=head[x];i!=-1;i=e[i].next)
	{
		vi=e[i].to;
		if(vi==f) continue;
		on1=1;
		dfs(vi,x);
	}
	if(!on1)
	{
		if(v[x]==1) dp[1][x]=p[x];
		else if(v[x]==-1) dp[0][x]=0;
		else
		{
			dp[1][x]=p[x];
			dp[0][x]=0;
		}
	}
	else
	{
		if(v[x]==1) dp[1][x]=p[x];
		else if(v[x]==-1) dp[0][x]=0;
		else
		{
			dp[1][x]=p[x];
			dp[0][x]=0;
		}
		for(i=head[x];i!=-1;i=e[i].next)
		{
			vi=e[i].to;
			if(vi==f) continue;
			if(v[x]==1) dp[1][x]+=min(dp[0][vi],dp[1][vi]);
			else if(v[x]==-1) dp[0][x]+=dp[1][vi];
			else
			{
				dp[1][x]+=min(dp[0][vi],dp[1][vi]);
				dp[0][x]+=dp[1][vi];
			}
		}
	}
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int i,j,x,y,a,b;
	n=read();m=read();
	scanf("%s",type);
	if(type[0]=='A')
	{
		for(i=1;i<=n;i++)
			p[i]=read();
		for(i=1;i<n;i++)
			x=read(),y=read();
		for(i=1;i<=m;i++)
		{
			a=read(),x=read(),b=read(),y=read();
			for(j=0;j<=n;j++)
				v[j]=0;
			if(x==0) v[a]=-1;
			else if(x==1) v[a]=1;
			if(y==0) v[b]=-1;
			else if(y==1) v[b]=1;
			for(j=0;j<=n;j++)
			{
				dp[0][j]=0x3f3f3f3f;
				dp[1][j]=0x3f3f3f3f;
			}
			if(v[1]==-1) dp[0][1]=0;
			else if(v[1]==1) dp[1][1]=p[1];
			else
			{
				dp[0][1]=0;
				dp[1][1]=p[1];
			}
			for(j=2;j<=n;j++)
			{
				if(v[j]==0)
				{
					dp[0][j]=dp[1][j-1];
					dp[1][j]=min(dp[1][j-1],dp[0][j-1])+p[j];
				}
				else if(v[j]==-1)
					dp[0][j]=dp[1][j-1];
				else if(v[j]==1)
					dp[1][j]=min(dp[1][j-1],dp[0][j-1])+p[j];
			}
			printf("%lld\n",min(dp[0][n],dp[1][n]));
		}
	}
	else
	{
		for(i=1;i<=n;i++)
			head[i]=-1;
		for(i=1;i<=n;i++)
			p[i]=read();
		for(i=1;i<n;i++)
		{
			x=read(),y=read();
			addedge(x,y);
			addedge(y,x);
		}
		for(i=1;i<=m;i++)
		{
			a=read(),x=read(),b=read(),y=read();
			for(j=0;j<=n;j++)
				v[j]=0;
			if(x==0) v[a]=-1;
			else if(x==1) v[a]=1;
			if(y==0) v[b]=-1;
			else if(y==1) v[b]=1;
			for(j=0;j<=n;j++)
			{
				dp[0][j]=0x3f3f3f3f;
				dp[1][j]=0x3f3f3f3f;
			}
			dfs(1,0);
			if(dp[1][1]>=0x3f3f3f3f&&dp[0][1]>=0x3f3f3f3f)
				printf("-1\n");
			else
				printf("%lld\n",min(dp[1][1],dp[0][1]));
		}
	}
	return 0;
}

