#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>
#include <utility>
#include <queue>
#include <vector>
#include <map>
#include <set>
using namespace std;
#define ll long long
#define fs first
#define sc second
#define pi pair<int,int>
#define mem(a,b) memset(a,b,sizeof(a))

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
}

struct edge
{
	int to,next,v;
}e[50005];

struct bian
{
	int x,y;
}a[50005];

int n,m,cnt,len,sz,ans1;
int head[10005];
int ans[10005],cou[10005];
int vis[5005][5005],to[50005],vii[50005];

void addedge(int x,int y)
{
	e[++cnt].to=y;e[cnt].next=head[x];head[x]=cnt;
}

bool cmp(bian xi,bian yi)
{
	return xi.x==yi.x?xi.y>yi.y:xi.x<yi.x;
}

void dfs(int x,int f)
{
	int i,v;
	ans[++sz]=x;
	for(i=head[x];i!=-1;i=e[i].next)
	{
		v=e[i].to;
		if(v==f) continue;
		dfs(v,x);
	}
}

void dfs2(int x,int f)
{
	int i,v;
	cou[++sz]=x;
	if(sz>n) return;
	for(i=head[x];i!=-1;i=e[i].next)
	{
		v=e[i].to;
		if(v==f||e[i].v==1) continue;
		dfs2(v,x);
		if(sz>n) return ;
	}
}

void update()
{
	int on1=0,i;
	if(ans1==0)
	{
		for(i=1;i<=n;i++)
			ans[i]=cou[i];
		ans1=1;
		return ;
	}
	for(i=1;i<=n;i++)
		if(cou[i]<ans[i])
			on1=1;
		else if(cou[i]>ans[i])
			break;
	if(on1==1)
	{
		for(i=1;i<=n;i++)
			ans[i]=cou[i];
		ans1=1;
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int i,x,y;
	n=read(); m=read();
	for(i=1;i<=n;i++)
		head[i]=-1;
	for(i=1;i<=m;i++)
	{
		x=read(),y=read();
		a[++len].x=x;
		a[len].y=y;
		a[++len].x=y;
		a[len].y=x;
	}
	if(m==n-1)
	{
		sort(a+1,a+len+1,cmp);
		for(i=1;i<=len;i++)
			addedge(a[i].x,a[i].y);
		dfs(1,0);
		for(i=1;i<n;i++)
			printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}
	else if(m==n)
	{
		sort(a+1,a+len+1,cmp);
		for(i=1;i<=len;i++)
		{
			addedge(a[i].x,a[i].y);
			if(vis[a[i].x][a[i].y]==0)
			{
				vis[a[i].x][a[i].y]=cnt;
				vis[a[i].y][a[i].x]=cnt;
			}
			else
			{
				to[i]=vis[a[i].y][a[i].x];
				to[vis[a[i].y][a[i].x]]=i;
			}
		}
		for(i=1;i<=len;i++)
			if(vii[i]==0&&vii[to[i]]==0)
			{
				e[i].v=1;
				e[to[i]].v=1;
				sz=0;
				dfs2(1,0);
				if(sz==n)
					update();
				e[i].v=0;
				e[to[i]].v=0;
				vii[i]=1;
				vii[to[i]]=1;
			}
		for(i=1;i<n;i++)
			printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}
	return 0;
}

