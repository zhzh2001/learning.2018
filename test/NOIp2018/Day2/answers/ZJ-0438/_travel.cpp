#include <cstring>
#include <cstdio>
#include <algorithm>
#include <vector>
using namespace std;

namespace io {
  const int SIZE = 2e6;
  char buff[SIZE], *l, *r;
  char getch(void) {
    if (l == r) r = (l = buff) + fread(buff, 1, SIZE, stdin);
    return l == r ? EOF : *l++;
  }
  template<typename T> void read(T &x) {
    x = 0;
    T tmp = 1;
    char c = getch();
    while ((c < '0' || c > '9') && c != '-') c = getch();
    if (c == '-') tmp = -1, c = getch();
    while (c >= '0' && c <= '9') x = x * 10 + c - '0', c = getch();
    x *= tmp;
  }
}
using io::read;

const int maxn = 5010;
int n, m;
vector<int> g[maxn];

namespace solver1 {
  void Dfs(int x, int pre) {
    printf("%d ", x);
    for (int i = 0; i < (int)g[x].size(); ++i) {
      int y = g[x][i];
      if (y == pre) continue;
      Dfs(y, x);
    }
  }
  void main(void) {
    for (int i = 1; i <= n; ++i)
      sort(g[i].begin(), g[i].end());
    Dfs(1, 0);
  }
}

namespace solver2 {
  int par[maxn], seq[maxn], n0 = 0;
  bool vis[maxn], on_cycle[maxn];

  void Find(int x, int pre) {
    vis[x] = 1;
    par[x] = pre;
    for (int i = 0; i < (int)g[x].size() && n0 == 0; ++i) {
      int y = g[x][i];
      if (y == pre) continue;
      if (!vis[y]) {
	Find(y, x);
      } else {
	int u = x;
	while (true) {
	  seq[++n0] = u;
	  on_cycle[u] = 1;
	  if (u == y) break;
	  u = par[u];
	}
	return;
      }
    }
  }

  int rt;
  vector<int> sub[maxn], G[maxn], cyc[maxn];
  void Dfs(int x, int pre) {
    if (x != rt) sub[rt].push_back(x);
    par[x] = pre;
    for (int i = 0; i < (int)G[x].size(); ++i) {
      int y = G[x][i];
      if (y == pre) continue;
      Dfs(y, x);
    }
  }

  vector<int> ans;
  void solve(int x) {
    vis[x] = 1;
    ans.push_back(x);
    for (int i = 0; i < (int)cyc[x].size(); ++i) {
      int y = cyc[x][i];
      if (vis[y]) continue;
      if (!on_cycle[y]) {
	for (int j = 0; j < (int)sub[x].size(); ++j)
	  ans.push_back(sub[x][j]);
	continue;
      }
      solve(y);
    }
  }
  namespace condition1 {
    void main(void) {
      for (int i = 1; i <= n0; ++i)
	if ((int)sub[seq[i]].size() > 0) cyc[seq[i]].push_back(sub[seq[i]][0]);
      for (int i = 1; i <= n; ++i)
	sort(cyc[i].begin(), cyc[i].end());
      solve(1);
      for (int i = 0; i < (int)ans.size(); ++i)
	printf("%d\n", ans[i]);
    }
  }
  namespace condition2 {
    int u;
    vector<int> e[maxn], tmp;

    void add(int x, int pre) {
      for (int i = 0; i < (int)g[x].size(); ++i) {
	int y = g[x][i];
	if (y == pre) continue;
	if (y == u) {
	  e[x].push_back(ans[0]);
	  continue;
	}
	e[x].push_back(y);
	add(y, x);
      }
    }
    void reDfs(int x, int pre) {
      if (x != rt) sub[rt].push_back(x);
      for (int i = 0; i < (int)G[x].size(); ++i) {
	int y = G[x][i];
	if (y == pre || y == u) continue;
	Dfs(y, x);
      }
    }
    void reSolve(int x) {
      vis[x] = 1;
      ans.push_back(x);
      for (int i = 0; i < (int)e[x].size(); ++i) {
	int y = e[x][i];
	if (vis[y]) continue;
	if (y == u) {
	  for (int j = 0; j < (int)tmp.size(); ++j)
	    ans.push_back(tmp[j]);
	  continue;
	}
	reSolve(y);
      }
    }
    
    void main(void) {
      u = 1;
      while (!on_cycle[par[u]]) u = par[u];
      rt = par[u];
      sub[rt].clear();
      reDfs(rt, 0);
      u = rt;
      
      for (int i = 1; i <= n0; ++i)
	if ((int)sub[seq[i]].size() > 0) cyc[seq[i]].push_back(sub[seq[i]][0]);
      for (int i = 1; i <= n; ++i)
	sort(cyc[i].begin(), cyc[i].end());
      solve(u);
      
      add(1, 0);
      for (int i = 1; i <= n; ++i)
	sort(e[i].begin(), e[i].end());
      tmp = ans, ans.clear();
      memset(vis, 0, sizeof vis);
      reSolve(1);

      for (int i = 0; i < (int)ans.size(); ++i)
	printf("%d ", ans[i]);
    }
  }
  void main(void) {
    memset(vis, 0, sizeof vis);
    memset(on_cycle, 0, sizeof on_cycle);
    Find(1, 0);
    
    for (int i = 1; i <= n; ++i) {
      for (int j = 0; j < (int)g[i].size(); ++j) {
	int x = g[i][j];
	if (on_cycle[x]) continue;
	G[i].push_back(x);
      }
      sort(G[i].begin(), G[i].end());
    }
    for (int i = 1; i <= n0; ++i) {
      rt = seq[i];
      Dfs(seq[i], 0);
    }

    memset(vis, 0, sizeof vis);
    for (int i = 1; i < n0; ++i) {
      cyc[seq[i]].push_back(seq[i + 1]);
      cyc[seq[i + 1]].push_back(seq[i]);
    }
    cyc[seq[n0]].push_back(seq[1]);
    cyc[seq[1]].push_back(seq[n0]);

    bool flag = 0;
    for (int i = 1; i <= n0; ++i)
      if (seq[i] == 1) flag = 1;
    if (flag) {
      condition1::main();
    } else {
      condition2::main();
    }
  }
}

int main(void) {
  
  read(n), read(m);
  for (int i = 1, x, y; i <= m; ++i) {
    read(x), read(y);
    g[x].push_back(y);
    g[y].push_back(x);
  }

  if (m == n - 1) {
    solver1::main();
  } else {
    solver2::main();
  }

  return 0;
}
