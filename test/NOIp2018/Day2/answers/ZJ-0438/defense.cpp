#include <cstring>
#include <cstdio>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long li;

namespace io {
  const int SIZE = 2e6;
  char buff[SIZE], *l, *r;
  char getch(void) {
    if (l == r) r = (l = buff) + fread(buff, 1, SIZE, stdin);
    return l == r ? EOF : *l++;
  }
  template<typename T> void read(T &x) {
    x = 0;
    T tmp = 1;
    char c = getch();
    while ((c < '0' || c > '9') && c != '-') c = getch();
    if (c == '-') tmp = -1, c = getch();
    while (c >= '0' && c <= '9') x = x * 10 + c - '0', c = getch();
    x *= tmp;
  }
}
using io::read;


const int maxn = 1e5 + 10;
const li inf = 1e18;
int n, m, a[maxn];
int ty1, ty2;
vector<int> g[maxn];

namespace subtask1 {
  int lim[maxn];
  li f[maxn][2];

  void Dfs(int x, int pre) {
    f[x][0] = 0;
    f[x][1] = a[x];
    for (int i = 0; i < (int)g[x].size(); ++i) {
      int y = g[x][i];
      if (y == pre) continue;
      Dfs(y, x);
      if (f[x][0] < inf) {
	if (f[y][1] < inf) {
	  f[x][0] += f[y][1];
	} else {
	  f[x][0] = inf;
	}
      }
      if (f[x][1] < inf) {
	if (min(f[y][0], f[y][1]) < inf) {
	  f[x][1] += min(f[y][0], f[y][1]);
	} else {
	  f[x][1] = inf;
	}
      }
    }
    if (~lim[x]) f[x][lim[x]] = inf;
  }
  void main(void) {
    memset(lim, -1, sizeof lim);
    for (int u, v, x, y; m--;) {
      read(u), read(x), read(v), read(y);
      lim[u] = x ^ 1;
      if (ty2 != 1) lim[v] = y ^ 1;

      Dfs(1, 0);
      li ans = inf;
      ans = min(f[1][0], f[1][1]);
      if (ans < inf) printf("%lld\n", ans);
      else puts("-1");
      
      lim[u] = -1;
      if (ty2 != 1) lim[v] = -1;
    }
  }
}

int main(void) {

  read(n), read(m);
  {
    char c = io::getch();
    while (c < 'A' || c > 'C') c = io::getch();
    ty1 = c - 'A';
    c = io::getch();
    ty2 = c - '0';
  }
  for (int i = 1; i <= n; ++i) read(a[i]);
  for (int i = 1, x, y; i < n; ++i) {
    read(x), read(y);
    g[x].push_back(y);
    g[y].push_back(x);
  }
  
  if (n <= 2000 && m <= 2000) {
    subtask1::main();
    return 0;
  }

  return 0;
}
