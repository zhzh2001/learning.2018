#include <cstring>
#include <cstdio>
#include <algorithm>
using namespace std;

namespace io {
  const int SIZE = 2e6;
  char buff[SIZE], *l, *r;
  char getch(void) {
    if (l == r) r = (l = buff) + fread(buff, 1, SIZE, stdin);
    return l == r ? EOF : *l++;
  }
  template<typename T> void read(T &x) {
    x = 0;
    T tmp = 1;
    char c = getch();
    while ((c < '0' || c > '9') && c != '-') c = getch();
    if (c == '-') tmp = -1, c = getch();
    while (c >= '0' && c <= '9') x = x * 10 + c - '0', c = getch();
    x *= tmp;
  }
}
using io::read;


const int maxn = 8, maxm = 1e6 + 10;
int n, m;

namespace brute {
  int ans, a[maxn][maxn];

  void Dfs(int x, int y) {
    if (x > n) {
      ++ans;
      for (int i = 1; i <= n; ++i) {
	for (int j = 1; j <= m; ++j) printf("%d ", a[i][j]);
	puts("");
      }
      puts("---");
      return;
    }
    if (a[x - 1][y + 1] == 1) {
      a[x][y] = 1;
      if (y == m) Dfs(x + 1, 1);
      else Dfs(x, y + 1);
    } else {
      a[x][y] = 0;
      if (y == m) Dfs(x + 1, 1);
      else Dfs(x, y + 1);

      a[x][y] = 1;
      if (y == m) Dfs(x + 1, 1);
      else Dfs(x, y + 1);
    }
  }
  void main(void) {
    ans = 0;
    Dfs(1, 1);
    printf("%d\n", ans);
  }
}

int main(void) {

  read(n), read(m);
    brute::main();
    return 0;
  

  return 0;
}
