#include <cstring>
#include <cstdio>
#include <algorithm>
#include <vector>
using namespace std;

namespace io {
  const int SIZE = 2e6;
  char buff[SIZE], *l, *r;
  char getch(void) {
    if (l == r) r = (l = buff) + fread(buff, 1, SIZE, stdin);
    return l == r ? EOF : *l++;
  }
  template<typename T> void read(T &x) {
    x = 0;
    T tmp = 1;
    char c = getch();
    while ((c < '0' || c > '9') && c != '-') c = getch();
    if (c == '-') tmp = -1, c = getch();
    while (c >= '0' && c <= '9') x = x * 10 + c - '0', c = getch();
    x *= tmp;
  }
}
using io::read;

const int maxn = 5010;
int n, m;
vector<int> g[maxn];

namespace solver1 {
  void Dfs(int x, int pre) {
    printf("%d ", x);
    for (int i = 0; i < (int)g[x].size(); ++i) {
      int y = g[x][i];
      if (y == pre) continue;
      Dfs(y, x);
    }
  }
  void main(void) {
    for (int i = 1; i <= n; ++i)
      sort(g[i].begin(), g[i].end());
    Dfs(1, 0);
  }
}

namespace solver2 {
  int par[maxn], seq[maxn], n0 = 0;
  bool vis[maxn], on_cycle[maxn];

  void Find(int x, int pre) {
    vis[x] = 1;
    par[x] = pre;
    for (int i = 0; i < (int)g[x].size() && n0 == 0; ++i) {
      int y = g[x][i];
      if (y == pre) continue;
      if (!vis[y]) {
	Find(y, x);
      } else {
	int u = x;
	while (true) {
	  seq[++n0] = u;
	  on_cycle[u] = 1;
	  if (u == y) break;
	  u = par[u];
	}
	return;
      }
    }
  }

  int rt;
  vector<int> sub[maxn], G[maxn];
  void Dfs(int x, int pre) {
    sub[rt].push_back(x);
    par[x] = pre;
    for (int i = 0; i < (int)G[x].size(); ++i) {
      int y = G[x][i];
      if (y == pre) continue;
      Dfs(y, x);
    }
  }

  int lim;
  void reDfs(int x, int pre) {
    sub[1].push_back(x);
    for (int i = 0; i < (int)G[x].size(); ++i) {
      int y = G[x][i];
      if (y == pre || y == lim) continue;
      reDfs(y, x);
    }
  }
  void reDfs1(int x, int pre) {
    sub[rt].push_back(x);
    for (int i = 0; i < (int)G[x].size(); ++i) {
      int y = G[x][i];
      if (y == pre || y == 1) continue;
      reDfs1(y, x);
    }
  }

  int id, ty;
  vector<int> cyc, tmp, ans;
  bool check(vector<int> &a, vector<int> &b) {
    for (int i = 0; i < (int)a.size(); ++i) {
      if (a[i] == b[i]) continue;
      return a[i] < b[i];
    }
    return 0;
  }
  void get_order(int i) {
    cyc.clear();
    if (id == i) {
      int pos = id;
      while (true) {
	cyc.push_back(seq[pos]);
	if (pos == 1) pos = n0 - 1;
	else --pos;
	if (pos == id) break;
      }
    } else if (i + 1 == id) {
      int pos = id;
      while (true) {
	cyc.push_back(seq[pos]);
	if (pos == n0) pos = 2;
	else ++pos;
	if (pos == id) break;
      }
    } else {
      cyc.push_back(seq[id]);
      if (seq[id - 1] < seq[id + 1]) {
	int pos = id - 1;
	while (true) {
	  cyc.push_back(seq[pos]);
	  if (pos == i + 1) break;
	  if (pos == 1) pos = n0 - 1;
	  else --pos;
	}
	pos = id + 1;
	while ((int)cyc.size() < n0 - 1) {
	  cyc.push_back(seq[pos]);
	  if (pos == n0) pos = 2;
	  else ++pos;
	}
      } else {
	int pos = id + 1;
	while (true) {
	  cyc.push_back(seq[pos]);
	  if (pos == i) break;
	  if (pos == n0) pos = 2;
	  else ++pos;
	}
	pos = id - 1;
	while ((int)cyc.size() < n0 - 1) {
	  cyc.push_back(seq[pos]);
	  if (pos == 1) pos = n0 - 1;
	  else --pos;
	}
      }
    }
  }
  void Solve(void) {
    tmp.clear();
    for (int i = 0; i < (int)cyc.size(); ++i) {
      int u = cyc[i];
      for (int j = 0; j < (int)sub[u].size(); ++j)
	tmp.push_back(sub[u][j]);
    }
  }
  
  void main(void) {
    memset(vis, 0, sizeof vis);
    memset(on_cycle, 0, sizeof on_cycle);
    Find(1, 0);
    
    for (int i = 1; i <= n; ++i) {
      for (int j = 0; j < (int)g[i].size(); ++j) {
	int x = g[i][j];
	if (on_cycle[x]) continue;
	G[i].push_back(x);
      }
      sort(G[i].begin(), G[i].end());
    }
    for (int i = 1; i <= n0; ++i) {
      rt = seq[i];
      Dfs(seq[i], 0);
    }

    id = -1, ty = 1;
    for (int i = 1; i <= n0; ++i)
      if (seq[i] == 1) id = i;

    if (id == -1) {
      ty = 2;
      lim = par[1];
      sub[1].clear();
      reDfs(1, 0);

      int u = 1;
      while (!on_cycle[u]) u = par[u];
      for (int i = 1; i <= n0; ++i)
	if (seq[i] == u) id = i;
      sub[u].clear(), rt = u;
      reDfs1(u, 0);
    }

    seq[++n0] = seq[1];
    for (int i = 1; i < n0; ++i) {
      get_order(i);
      Solve();
      if (i == 1 || check(tmp, ans)) ans = tmp;
    }

    if (ty == 1) {
      for (int i = 0; i < (int)ans.size(); ++i)
	printf("%d ", ans[i]);
    } else {
      vector<int> path;
      int u = par[1];
      while (!on_cycle[u]) {
	path.push_back(u);
	u = par[u];
      }
      for (int i = 0; i < (int)ans.size(); ++i)
	path.push_back(ans[i]);

      if ((int)path.size() == 0) {
	for (int i = 0; i < (int)sub[1].size(); ++i)
	  printf("%d ", sub[1][i]);
      } else if ((int)sub[1].size() <= 1) {
	printf("1 ");
	for (int i = 0; i < (int)path.size(); ++i)
	  printf("%d ", path[i]);
      } else {
	if (sub[1][1] < path[0]) {
	  for (int i = 0; i < (int)sub[1].size(); ++i)
	    printf("%d ", sub[1][i]);
	  for (int i = 0; i < (int)path.size(); ++i)
	    printf("%d ", path[i]);
	} else {
	  printf("1 ");
	  for (int i = 0; i < (int)path.size(); ++i)
	    printf("%d ", path[i]);
	  for (int i = 1; i < (int)sub[1].size(); ++i)
	    printf("%d ", sub[1][i]);
	}
      }
    }
  }
}

int main(void) {
  read(n), read(m);
  for (int i = 1, x, y; i <= m; ++i) {
    read(x), read(y);
    g[x].push_back(y);
    g[y].push_back(x);
  }

  if (m == n - 1) {
    solver1::main();
  } else {
    solver2::main();
  }

  return 0;
}
