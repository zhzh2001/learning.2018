#include <cstdio>
#include <cstring>
#include <algorithm>

void read(int &t) {
  char c = getchar();
  for (; c < '0' || c > '9'; c = getchar())
    ;
  for (t = 0; c >= '0' && c <= '9'; t = t * 10 + c - '0', c = getchar())
    ;
}

int const MOD = 1000000007;

int power(int a, int b) {
  int ret = 1;
  for (; b; b >>= 1)
    b & 1 && (ret = 1ll * ret * a % MOD), a = 1ll * a * a % MOD;
  return ret;
}

int main() {
#ifndef DEBUG
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);
#endif
  int n, m;
  read(n), read(m);
  if (n > m)
    std::swap(n, m);
  if (n == 1)
    printf("%d\n", power(2, m));
  else if (n == 2)
    printf("%d\n", power(3, m - 1) * 4 % MOD);
  else if (n == 3)
    puts("112");
  return 0;
}

