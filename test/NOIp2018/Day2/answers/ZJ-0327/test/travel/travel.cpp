#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

void read(int &t) {
  char c = getchar();
  for (; c < '0' || c > '9'; c = getchar())
    ;
  for (t = 0; c >= '0' && c <= '9'; t = t * 10 + c - '0', c = getchar())
    ;
}

int const N = 5005;

std::vector<int> e[N];

void dfs1(int t, int fa) {
  int s = e[t].size();
  printf("%d ", t);
  for (int i = 0; i < s; ++i)
    if (e[t][i] != fa)
      dfs1(e[t][i], t);
}

int top, sta[N], in[N], vis[N];
int rc, round[N];
int ans[N], cur[N];

void dfs2(int t, int fa) {
  int s = e[t].size();
  sta[top++] = t, in[t] = 1;
  for (int i = 0; i < s; ++i)
    if (e[t][i] != fa && !vis[e[t][i]])
      if (in[e[t][i]]) {
        for (int j = top - 1; sta[j] != e[t][i]; --j)
          round[rc++] = sta[j];
        round[rc++] = e[t][i];
      } else
        dfs2(e[t][i], t);
  --top, in[t] = 0, vis[t] = 1;
}

int equal(int a, int b, int c, int d) {
  return a == c && b == d || a == d && b == c;
}

int k;

void dfs3(int t, int fa, int u, int v) {
  int s = e[t].size();
  cur[k++] = t;
  for (int i = 0; i < s; ++i)
    if (e[t][i] != fa && !equal(u, v, t, e[t][i]))
      dfs3(e[t][i], t, u, v);
}

void update(int n) {
  int l = 0;
  for (int i = 0; i < n; ++i)
    if (cur[i] < ans[i]) {
      l = 1;
      break;
    } else if (cur[i] > ans[i])
      break;
  if (l)
    for (int i = 0; i < n; ++i)
      ans[i] = cur[i];
}

int main() {
#ifndef DEBUG
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);
#endif
  int n, m;
  read(n), read(m);
  for (int i = 1; i <= m; ++i) {
    int u, v;
    read(u), read(v);
    e[u].push_back(v), e[v].push_back(u);
  }
  for (int i = 1; i <= n; ++i)
    std::sort(e[i].begin(), e[i].end());
  if (m == n - 1)
    dfs1(1, 0);
  else {
    dfs2(1, 0), ans[0] = n + 1;
    for (int i = 1; i < rc; ++i)
      k = 0, dfs3(1, 0, round[i - 1], round[i]), update(n);
    k = 0, dfs3(1, 0, round[0], round[rc - 1]), update(n);
    for (int i = 0; i < n; ++i)
      printf("%d ", ans[i]);
  }
  puts("");
  return 0;
}

