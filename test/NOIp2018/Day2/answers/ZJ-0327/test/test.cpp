#include <cstdio>
#include <cstring>
#include <algorithm>

void read(int &t) {
  char c = getchar();
  for (; c < '0' || c > '9'; c = getchar())
    ;
  for (t = 0; c >= '0' && c <= '9'; t = t * 10 + c - '0', c = getchar())
    ;
}

int main() {
#ifndef DEBUG
  freopen("", "r", stdin);
  freopen("", "w", stdout);
#endif
  return 0;
}

