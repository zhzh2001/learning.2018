#include <cstdio>
#include <cstring>
#include <algorithm>

void read(int &t) {
  char c = getchar();
  for (; c < '0' || c > '9'; c = getchar())
    ;
  for (t = 0; c >= '0' && c <= '9'; t = t * 10 + c - '0', c = getchar())
    ;
}

int const N = 100005;
long long MAX = 10000000005;

struct Edge {
  int v, nxt;
} e[N << 1];

int ne, h[N], p[N];
long long f[N][2];

void add_edge(int u, int v) {
  e[++ne] = (Edge){v, h[u]}, h[u] = ne;
  e[++ne] = (Edge){u, h[v]}, h[v] = ne;
}

void dfs(int t, int fa, int a, int x, int b, int y) {
  for (int i = h[t]; i; i = e[i].nxt)
    if (e[i].v != fa)
      dfs(e[i].v, t, a, x, b, y);
  int flag = -1;
  if (a == t)
    flag = x;
  else if (b == t)
    flag = y;
  f[t][0] = 0, f[t][1] = p[t];
  for (int i = h[t]; i; i = e[i].nxt)
    if (e[i].v != fa)
      f[t][0] += f[e[i].v][1], f[t][1] += std::min(f[e[i].v][0], f[e[i].v][1]);
  if (~flag)
    f[t][!flag] = MAX;
}

char type[5];
long long pre[N][2], suf[N][2];

int main() {
#ifndef DEBUG
  freopen("defense.in", "r", stdin);
  freopen("defense.out", "w", stdout);
#endif
  int n, m;
  read(n), read(m), scanf("%s", type);
  for (int i = 1; i <= n; ++i)
    read(p[i]);
  for (int i = 1; i < n; ++i) {
    int u, v;
    read(u), read(v);
    add_edge(u, v);
  }
  if (n <= 2000 && m <= 2000)
    for (; m--;) {
      int a, x, b, y;
      read(a), read(x), read(b), read(y);
      dfs(1, 0, a, x, b, y);
      long long ans = std::min(f[1][0], f[1][1]);
      printf("%lld\n", ans >= MAX ? -1 : ans);
    }
  else if (type[1] == '1') {
    pre[1][0] = MAX, pre[1][1] = p[1];
    for (int i = 2; i <= n; ++i)
      pre[i][0] = pre[i - 1][1], pre[i][1] = std::min(pre[i - 1][0], pre[i - 1][1]) + p[i];
    suf[n][0] = 0, suf[n][1] = p[n];
    for (int i = n - 1; i; --i)
      suf[i][0] = suf[i + 1][1], suf[i][1] = std::min(suf[i + 1][0], suf[i + 1][1]) + p[i];
    for (; m--;) {
      int a, x, b, y;
      read(a), read(x), read(b), read(y);
      long long ans = 0;
      if (y)
        ans += std::min(pre[b - 1][0], pre[b - 1][1]) + std::min(suf[b + 1][0], suf[b + 1][1]) + p[b];
      else
        ans += pre[b - 1][1] + suf[b + 1][1];
      printf("%lld\n", ans >= MAX ? -1 : ans);
    }
  } else if (type[1] == '2') {
    pre[1][0] = 0, pre[1][1] = p[1];
    for (int i = 2; i <= n; ++i)
      pre[i][0] = pre[i - 1][1], pre[i][1] = std::min(pre[i - 1][0], pre[i - 1][1]) + p[i];
    suf[n][0] = 0, suf[n][1] = p[n];
    for (int i = n - 1; i; --i)
      suf[i][0] = suf[i + 1][1], suf[i][1] = std::min(suf[i + 1][0], suf[i + 1][1]) + p[i];
    for (; m--;) {
      int a, x, b, y;
      read(a), read(x), read(b), read(y);
      if (!x && !y) {
        puts("-1");
        continue;
      }
      if (a > b)
        std::swap(a, b), std::swap(x, y);
      long long ans = 0;
      if (x)
        ans += std::min(pre[a - 1][0], pre[a - 1][1]) + p[a];
      else
        ans += pre[a - 1][1];
      if (y)
        ans += std::min(suf[b + 1][0], suf[b + 1][1]) + p[b];
      else
        ans += suf[b + 1][1];
      printf("%lld\n", ans >= MAX ? -1 : ans);
    }
  }
  return 0;
}

