#include<stdio.h>
#include<string.h>
int n,m,a,x,b,y,edgenum=0,head[5005],Next[10005],edge[10005],p[100005],vis[100005],fa[100005],f[100005][2];
void addedge(int u,int v){
	edge[++edgenum]=v;
	Next[edgenum]=head[u];
	head[u]=edgenum;
}
void dfs(int u,int father=0){
	bool flag=1;
	fa[u]=father;
	f[u][0]=f[u][1]=1;
	if(a==u)
		f[u][x^1]=0;
	if(b==u)
		f[u][y^1]=0;
	for(int e=head[u];e;e=Next[e])
		if(!vis[edge[e]]){
			vis[edge[e]]=1;
			flag=1;
			dfs(edge[e]);
			if(f[u][0]!=-1)
				f[u][0]*=f[edge[e]][1];
			if(f[u][0]<0)
				f[u][0]=-1;
			if(f[u][1]!=-1)
				f[u][1]*=f[edge[e]][0]+f[edge[e]][1];
			if(f[u][1]<0)
				f[u][1]=-1;
		}
}
void quary(int u){
	for(int v=fa[u];v;v=fa[v]){
		f[u][0]=f[u][1]=1;
		for(int e=head[v];e;e=Next[e]){
			f[u][0]*=f[edge[e]][1];
			f[u][1]*=f[edge[e]][0]+f[edge[e]][1];
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char type[3];
	scanf("%d%d%s",&n,&m,type);
	for(int i=1;i<=n;i++)
		scanf("%d",p+i);
	for(int i=1,u,v;i<n;i++){
		scanf("%d%d",&u,&v);
		addedge(u,v);
		addedge(v,u);
	}
	dfs(1);
	for(int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		memset(f,0,sizeof(f));
		f[a][x^1]=0;
		f[b][y^1]=0;
		if(n>2000){
			quary(a);
			quary(b);
		}
		else dfs(1);
		if(a==1&&b==1){
			printf("%d\n",(x==y)?f[a][x]:-1);
			continue;
		}
		if(b==1){
			a^=b^=a^=b;
			x^=y^=x^=y;
		}
		if(a==1)
			printf("%d\n",f[1][x]);
		else printf("%d\n",(f[1][0]+f[1][1]==0)?0:f[1][0]+f[1][1]);
	}
	return 0;
}
