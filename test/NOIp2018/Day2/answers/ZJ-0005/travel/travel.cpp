#include<stdio.h>
#include<string.h>
#include<algorithm>
using namespace std;
struct Ed{
	int from,to,key;
}E[10005];
bool cmp(const Ed &x,const Ed &y){
	return x.to>y.to;
}
int n,m,del,ans[5005],tot=0,vis[5005],did[5005],minn[5005],head[5005],Next[10005],edge[10005],k[10005],edgenum=0;
void addedge(int u,int v,int key){
	edge[++edgenum]=v;
	Next[edgenum]=head[u];
	head[u]=edgenum;
	k[edgenum]=key;
}
void dfs(int u){
	if(!did[u]){
		ans[++tot]=u;
		did[u]=1;
	}
	for(int e=head[u];e;e=Next[e])
		if(k[e]!=del&&!vis[edge[e]]){
			vis[edge[e]]=1;
			dfs(edge[e]);
			vis[edge[e]]=0;
		}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&E[(i<<1)-1].from,&E[(i<<1)-1].to);
		E[i<<1].from=E[(i<<1)-1].to;
		E[i<<1].to=E[(i<<1)-1].from;
		E[i<<1].key=E[(i<<1)-1].key=i;
	}
	sort(E+1,E+(m<<1)+1,cmp);
	if(n==m+1){
		memset(ans,0,sizeof(ans));
		for(int i=1;i<=m<<1;i++)
			addedge(E[i].from,E[i].to,E[i].key);
		dfs(1);
		for(int i=1;i<n;i++)
			printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}
	else{
		for(int i=1;i<=n;i++)
			minn[i]=1000000000;
		for(int i=1;i<=m<<1;i++)
			addedge(E[i].from,E[i].to,E[i].key);
		dfs(1);
		for(int i=1;i<=n;i++)
			minn[i]=ans[i];
		for(del=1;del<=m;del++){
			memset(ans,0,sizeof(ans));
			tot=0;
			for(int i=1;i<=n;i++)
				vis[i]=0;
			dfs(1);
			bool flag=1;
			for(int i=1;i<=n;i++)
				if(ans[i]==0)
					flag=0;
			if(flag)
				for(int i=1;i<=n;i++)
					if(ans[i]<minn[i]){
						for(int j=i;j<=n;j++)
							minn[i]=ans[i];
						break;
					}
					else if(ans[i]>minn[i])
						break;
		}
		for(int i=1;i<n;i++)
			printf("%d ",minn[i]);
		printf("%d\n",minn[n]);
	}
	return 0;
}
