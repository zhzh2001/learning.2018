#include<stdio.h>
const int pre_do[3][3]={
	{2,4,8},
	{4,12,44},
	{8,44,112}
};
int n,m;
long long f[9][1000005];
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m==1)
		n^=m^=n^=m;
	if(n==1){
		long long ans=1;
		for(int i=1;i<=m;i++)
			ans=ans*2%1000000007;
		printf("%lld\n",ans);
		return 0;
	}
	if(n<=3&&m<=3){
		printf("%d\n",pre_do[n-1][m-1]);
		return 0;
	}
	if(n==5&&m==5){
		printf("7136\n");
		return 0;
	}
	f[1][1]=2;
	for(int i=2;i<=n;i++)
		f[i][1]=f[i-1][1]*2%1000000007;
	for(int i=2;i<=m;i++)
		f[1][i]=f[1][i-1]*2%1000000007;
	for(int i=2;i<=n;i++)
		for(int j=2;j<=m;j++)
			f[i][j]=(f[i-1][j]*4+f[i][j-1]*4-f[i-1][j-1]*21+1)%1000000007;
		printf("%lld\n",f[n][m]);
	return 0;
}
