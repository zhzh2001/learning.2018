#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
const int N = 5050;
vector <int> g[N];
int head[N], cnt, n, m, vis[N], opt[N], now, fa[N], count_del, dfs_vis[N], ans[N], flag;
pair <int, int> del[N];
//inline void addedge(int x, int y)
//{
//	edg[++cnt].to = y, edg[cnt].nxt = head[x], head[x] = cnt;
//}
void dfs(int u)
{
	opt[++now] = u, vis[u] = 1;
	sort(&g[u][0], &g[u][g[u].size()]);
	for(int i = 0; i < g[u].size(); i ++)
	if(!vis[g[u][i]])
		dfs(g[u][i]);
}
bool dfs2(int u, int ban)
{
	opt[++now] = u, vis[u] = 1;
	if(opt[now] < ans[now])	flag = 1;
	if(opt[now] > ans[now] && !flag)	return 0;
	ans[now] = opt[now];
	for(int i = 0; i < g[u].size(); i ++)
	if(!vis[g[u][i]] && !((u == del[ban].first && g[u][i] == del[ban].second) || (u == del[ban].second && g[u][i] == del[ban].first)))
		if(!dfs2(g[u][i], ban))	return 0;
	return 1;
}
bool findr(int x, int pre)
{
	if(dfs_vis[x] && fa[x] != pre)
	{	
		fa[x] = pre, dfs_vis[x] = 1;
		int no = x;
		while(fa[no] != x)
		{
			del[++count_del] = make_pair(no, fa[no]);
			no = fa[no];
		}
		del[++count_del] = make_pair(no, fa[no]);
		return 1;
	}
	fa[x] = pre, dfs_vis[x] = 1;
	for(int i = 0; i < g[x].size(); i ++){
		if(g[x][i] != fa[x])
			findr(g[x][i], x);
	}
	return 0;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d", &n, &m);
	if(m == n - 1)
	{
		for(int i = 1; i < n; i ++)
		{
			int tmp, tmmp;
			scanf("%d%d", &tmp, &tmmp);
			g[tmp].push_back(tmmp);
			g[tmmp].push_back(tmp);
		}
		dfs(1);
		for(int i = 1; i < n; i ++)
		printf("%d ", opt[i]);
		printf("%d\n", opt[n]);
	}
	if(m == n)
	{
		for(int i = 1; i <= n; i ++)
		{
			int tmp, tmmp;
			scanf("%d%d", &tmp, &tmmp);
			g[tmp].push_back(tmmp);
			g[tmmp].push_back(tmp);
		}
		findr(1, 0);
		for(int i = 1; i <= n; i ++)
		sort(&g[i][0], &g[i][g[i].size()]);
		memset(ans, 0x3f, sizeof(ans));
		for(int i = 1; i <= count_del; i ++)
		{
			now = 0;
			memset(vis, 0, sizeof(vis));
			flag = 0, dfs2(1, i);
		}
		for(int i = 1; i < n; i ++)
		printf("%d ", ans[i]);
		printf("%d\n", ans[n]);
	}
	return 0;
}
