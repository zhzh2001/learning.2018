#include<cstdio>
#include<algorithm>
#include<iostream>
#include<vector>
#include<cstring>
using namespace std;
const int N = 100050;
vector<int> g[N];

int val[N], n, m, tot[3], vis[N], mark[N];
char type[10];
inline void dfs(int u, int cnt)
{
	mark[u] = cnt, tot[cnt] += val[u];
	vis[u] = 1;
	for(int i = 0; i < g[u].size(); i ++)
	{
		if(!vis[g[u][i]])
		dfs(g[u][i], cnt^1);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d", &n, &m);
	scanf("%s", type);
	for(int i = 1; i <= n; i ++)
	{
		scanf("%d", val+i);
	}
	for(int i = 1; i < n; i ++)
	{
		int tmp, tmmp;
		scanf("%d%d", &tmp, &tmmp);
		g[tmp].push_back(tmmp);
		g[tmmp].push_back(tmp);
	}
	dfs(1, 0);
	while(m --)
	{
		int x, X, y, Y;
		scanf("%d%d%d%d", &x, &X, &y, &Y);
		if(X == 0 && Y == 0)
		{
			if((mark[x]&1) == (mark[y]&1))	printf("%d\n", tot[(mark[x]&1) ^ 1]);
			else	if(min(x, y) + 1 == max(x, y))
			{
				printf("-1\n");
			}
			else
			{
				printf("%d\n", min(tot[mark[x]&1] - val[x] + val[x-1] + val[x+1], tot[mark[y]&1] - val[y] + val[y-1] + val[y+1]));
			}
		}
		if(X == 1 && Y == 0)
		{
			if((mark[x]&1) == (mark[y]&1))	printf("%d\n", min(tot[(mark[x]&1)] - val[y] + val[y-1] + val[y+1], tot[(mark[y]&1)^1] + val[x]));
			else	printf("%d\n", tot[(mark[x]&1)]);
		}
		if(X == 1 && Y == 1)
		{
			if((mark[x]&1) == (mark[y]&1))	printf("%d\n", tot[(mark[x]&1)]);
			else printf("%d\n", min(val[x] + tot[mark[y]&1], val[y] + tot[mark[x]&1]));
		}
		if(X == 0 && Y == 1)
		{
			if((mark[x]&1) == (mark[y]&1))	printf("%d\n", min(tot[(mark[x]&1)] - val[x] + val[x-1] + val[x+1], tot[(mark[x]&1)^1] + val[y]));
			else	printf("%d\n", tot[(mark[y]&1)]);
		}
	}
	return 0;
}
