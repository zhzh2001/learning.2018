#include<cstdio>
#include<cstring>
#include<iostream>
#include<math.h>
using namespace std;
const int mod = 1e9+7;
int f[260][100001][2], n, m, vis[260]; 


int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d", &n, &m);
	int tot = pow(2, n-1);
	for(int i = 0; i < (tot << 1 | 1); i ++)
	f[i][1][0] = 1;
	for(int j = 1; j < m; j ++)
	{
		for(int i = 0; i < tot; i ++)
		{
			memset(vis, 0, sizeof(vis));
			if(j < 2)
			for(int k = 0; k < tot; k ++)
			{
					int l = k & i;
					if(!vis[l] && l == i)
					{	
						f[l << 1][j+1][0] += f[i][j][0] + f[tot ^ i][j][0], f[l << 1][j+1][0] %= mod;
						f[l << 1 | 1][j+1][0] += f[i][j][0] + f[tot ^ i][j][0], f[l << 1 | 1][j+1][0] %= mod;
						vis[l] = 1;
					}
					else if(!vis[l])
					{
						
						f[l << 1][j+1][1] += f[i][j][0] + f[tot ^ i][j][0] + f[i][j][1] + f[tot ^ i][j][1], f[l << 1][j+1][0] %= mod;
						f[l << 1 | 1][j+1][1] += f[i][j][0] + f[tot ^ i][j][0] + f[i][j][1] + f[tot ^ i][j][1], f[l << 1 | 1][j+1][0] %= mod;
						vis[l] = 1;
					}
			}
			else
			{
				
				int l = (tot - 1) & i, tmp = pow(2, n - 2) - 1;
				f[l << 1][j+1][0] += f[i][j][0] + f[tot ^ i][j][0], f[l << 1][j+1][0] %= mod;
				f[l << 1 | 1][j+1][0] += f[i][j][0] + f[tot ^ i][j][0], f[l << 1 | 1][j+1][0] %= mod;
				
				if((tmp & i) != l)	l = tmp & i, f[l << 1][j+1][0] += f[i][j][0] + f[tot ^ i][j][0], f[l << 1][j+1][0] %= mod, f[l << 1 | 1][j+1][0] += f[i][j][0] + f[tot ^ i][j][0], f[l << 1 | 1][j+1][0] %= mod;
				
				for(int k = 0; k < tot; k ++)
				{
					int l = k & i;
					if(!vis[l])
					{	
						f[l << 1][j+1][1] += f[i][j][1] + f[tot ^ i][j][1], f[l << 1][j+1][0] %= mod;
						f[l << 1 | 1][j+1][1] += f[i][j][1] + f[tot ^ i][j][1], f[l << 1 | 1][j+1][0] %= mod;
						vis[l] = 1;
					}
				}		
			}
		}
	}
	int ans = 0;
	for(int i = 0; i < (tot << 1 | 1); i ++)
	ans = (ans + f[i][m][0] + f[i][m][1]) % mod;
	printf("%d\n", ans);
	return 0;
}

