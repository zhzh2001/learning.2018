#include <cstdio>
#include <algorithm>
#include <cstring>
const int N=1e5+5,M=2e5+5;
const long long inf=1LL<<60;
int n,m,tot,lnk[N],ter[M],nxt[M],val[N],a,x,b,y;
long long f[N][2];

void add(int u,int v) {
	ter[++tot]=v,nxt[tot]=lnk[u],lnk[u]=tot;
}
namespace dp {
	void dfs(int u,int fa) {
		f[u][1]=val[u],f[u][0]=0;
		for(int i=lnk[u];i;i=nxt[i]) {
			int v=ter[i];
			if(v==fa) continue;
			dfs(v,u);
			long long ret=std::min(f[v][0],f[v][1]);
			if(ret>=inf) f[u][1]=inf;
			else f[u][1]+=ret;
			if(f[v][1]>=inf) f[u][0]=inf;
			else f[u][0]+=f[v][1];
		}
		if(u==a&&x==1) f[u][0]=inf;
		if(u==a&&x==0) f[u][1]=inf;
		if(u==b&&y==1) f[u][0]=inf;
		if(u==b&&y==0) f[u][1]=inf;
	}
	void main() {
		while(m--) {
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(a==b&&x!=y) {
				puts("-1");
				continue;
			}
//			for(int i=1;i<=n;++i) f[i][0]=f[i][1]=0;
			dfs(1,0);
			long long ans=std::min(f[1][1],f[1][0]);
			if(ans>=inf) puts("-1");
			else printf("%lld\n",ans);
		}
	}
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char opt[5];
	scanf("%d %d %s",&n,&m,opt);
	for(int i=1;i<=n;++i) scanf("%d",&val[i]);
	for(int i=1;i<n;++i) {
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v),add(v,u);
	}
	dp::main();
	return 0;
}
