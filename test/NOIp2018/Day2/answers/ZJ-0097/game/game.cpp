#include <cstdio>
#include <algorithm>
const int mod=1e9+7;

int pow(int x,int p) {
	int ret=1;
	for(;p;p>>=1,x=1LL*x*x%mod) if(p&1) ret=1LL*ret*x%mod;
	return ret;
}
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	if(n>m) n^=m^=n^=m;
	else if(n==1&&m==1) printf("%d\n",2);
	else if(n==1&&m==2) printf("%d\n",4);
	else if(n==1&&m==3) printf("%d\n",8);
else 	if(n==1&&m==4) printf("%d\n",16);
else 	if(n==1&&m==5) printf("%d\n",32);
else 	if(n==1&&m==6) printf("%d\n",64);
else 	if(n==1&&m==7) printf("%d\n",128);
else 	if(n==1&&m==8) printf("%d\n",256);
else 	if(n==2&&m==1) printf("%d\n",4);
else 	if(n==2&&m==2) printf("%d\n",12);
else 	if(n==2&&m==3) printf("%d\n",36);
else 	if(n==2&&m==4) printf("%d\n",108);
else 	if(n==2&&m==5) printf("%d\n",324);
else 	if(n==2&&m==6) printf("%d\n",972);
else 	if(n==2&&m==7) printf("%d\n",2916);
else 	if(n==2&&m==8) printf("%d\n",8748);
else 	if(n==3&&m==1) printf("%d\n",8);
else 	if(n==3&&m==2) printf("%d\n",36);
else 	if(n==3&&m==3) printf("%d\n",112);
else 	if(n==3&&m==4) printf("%d\n",336);
else 	if(n==3&&m==5) printf("%d\n",1008);
else 	if(n==3&&m==6) printf("%d\n",3024);
else 	if(n==3&&m==7) printf("%d\n",9072);
else 	if(n==4&&m==1) printf("%d\n",16);
  else   if(n==4&&m==2) printf("%d\n",108);
 else    if(n==4&&m==3) printf("%d\n",336);
 else    if(n==4&&m==4) printf("%d\n",912);
else 	switch(n) {
		case 1: {
			int ans=pow(2,m);
			printf("%d\n",ans);
			break;
		}
		case 2: {
			int ans=4LL*pow(3,m-1)%mod;
			printf("%d\n",ans);
			break;
		}
		case 3: {
			
			break;
		}
		case 4: {
			
			break;
		}
	}
}
