#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
const int N=5e4+5,M=1e5+5;
int n,m,tot,lnk[N],ter[M],nxt[M];
int cnt,ans[N];

void add(int u,int v) {
	ter[++tot]=v;
	nxt[tot]=lnk[u];
	lnk[u]=tot;
}
namespace subtask {
	void dfs(int u,int fa) {
		ans[++cnt]=u;
		std::vector<int> son;
		for(int i=lnk[u];i;i=nxt[i]) {
			int v=ter[i];
			if(v^fa) son.push_back(v);
		}
		std::sort(son.begin(),son.end());
		for(int i=0;i<(int)son.size();++i) {
			dfs(son[i],u);
		}
	}
	void main() {
		for(int u,v,i=1;i<=m;++i) {
			scanf("%d%d",&u,&v);
			add(u,v),add(v,u);
		}
		dfs(1,0);
		for(int i=1;i<=n;++i) {
			printf("%d%c",ans[i]," \n"[i==n]);
		}
	}
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m==n-1) subtask::main();
}
