#include<bits/stdc++.h>
using namespace std;
#define rap(i,j,k) for(int i=j;i<=k;++i)
#define rapp(i,j,k) for(int i=j;i>=k;--i)
#define dep(i,x) for(int i=link[x];i;i=e[i].n)
#define ll long long
#define Pii pair< int,int >
#define mp make_pair
int n,m,link[5010],toto,dfn[5010],t,ans[5010],st[5010],top,now[5010],cir[5010],tot;
bool flag,vis[5010],OK,nev[10010],iscir[5010],used[10010];
struct node{
	int y,n;
}e[10010];
inline int read(){
	int NUM=0; bool f=1;
	char c=getchar();
	for(;c<'0'||c>'9';c=getchar())
	if(c=='-')f=0;
	for(;c>='0'&&c<='9';c=getchar())
	NUM=(NUM<<1)+(NUM<<3)+c-48;
	return f?NUM:-NUM;
}
priority_queue< Pii,vector< Pii >,greater< Pii > > q;
void insert(int x,int y){
	e[++toto].y=y;
	e[toto].n=link[x];
	link[x]=toto;
}
void init(){
	n=read();m=read();
	rap(i,1,m){
		int x=read(),y=read();
		insert(x,y); insert(y,x);
	}
}
bool cmp(int a,int b){return dfn[a]<dfn[b];}
void dfs(int x,int fa){
	dfn[x]=++t;
	int now[5010]={},cnt=0;
	dep(i,x){
		int y=e[i].y;
		if(y==fa||dfn[y]!=0||nev[i])continue;
		now[++cnt]=y;
	}
	sort(now+1,now+1+cnt);
	rap(i,1,cnt)
		dfs(now[i],x);
}
void find_cir(int x,int fa){
	if(OK)return;
	if(vis[x]){
		int y;
		do{
			y=st[top--]; cir[++tot]=y;
			iscir[y]=1;
		}while(y!=x);
		OK=1;
		return;
	}
	vis[x]=1; st[++top]=x;
	dep(i,x){
		int y=e[i].y;
		if(y==fa)continue;
		find_cir(y,x);
		if(OK)return;
	}
	top--;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	init();
	if(m==n)flag=1;
	rap(i,1,n) ans[i]=i;
	if(!flag){
		dfs(1,0);
		sort(ans+1,ans+1+n,cmp);
		rap(i,1,n) printf("%d ",ans[i]);
		return 0;
	}
	find_cir(1,0);
	rap(i,1,n) ans[i]=1e9,now[i]=i;
	rap(i,1,tot){
		int x=cir[i];
		dep(j,x){
			int y=e[j].y;
			if(iscir[y]&&!used[j]){
				nev[j]=1; used[j]=1;
				t=0;
				dfs(1,0);
				sort(now+1,now+1+n,cmp);
				rap(k,1,n){
					if(now[k]<ans[k]){
						rap(l,1,n)ans[l]=now[l];
						break;
					}
				}
				nev[j]=0;
			}
		}
	}
	rap(i,1,n) printf("%d ",ans[i]);
	return 0;
}
