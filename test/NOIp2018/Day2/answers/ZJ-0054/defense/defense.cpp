#include<bits/stdc++.h>
using namespace std;
#define rap(i,j,k) for(int i=j;i<=k;++i)
#define rapp(i,j,k) for(int i=j;i>=k;--i)
#define dep(i,x) for(int i=link[x];i;i=e[i].n)
#define ll long long
int n,m,w[100010],link[100010],toto;
ll dp[2010],f[2010];
int vol[100010];
bool flag=1;
char cz[5];
struct node{
	int y,n;
}e[200010];
inline int read(){
	int NUM=0; bool f=1;
	char c=getchar();
	for(;c<'0'||c>'9';c=getchar())
	if(c=='-')f=0;
	for(;c>='0'&&c<='9';c=getchar())
	NUM=(NUM<<1)+(NUM<<3)+c-48;
	return f?NUM:-NUM;
}
void insert(int x,int y){
	e[++toto].y=y;
	e[toto].n=link[x];
	link[x]=toto;
}
void init(){
	n=read();m=read();scanf("%s",cz+1);
	rap(i,1,n) w[i]=read();
	rap(i,1,n-1){
		int x=read(),y=read();
		insert(x,y);
		insert(y,x);
	}
}
void solve(int x,int fa){
	dep(i,x){
		int y=e[i].y;
		if(y==fa)continue;
		solve(y,x);
		f[x]+=min(f[y],dp[y]);
//		printf("%d %d %d\n",x,f[y],dp[y]);
		dp[x]+=f[y];
	}
	f[x]+=w[x];
//	printf("%d %lld %lld\n",x,dp[x],f[x]);
	if(vol[x]==0) f[x]=1e8;
	if(vol[x]==1) dp[x]=1e8;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	init();
	if(n<=2000&&m<=2000){
		rap(i,1,n) vol[i]=2;
		rap(i,1,m){
			int a=read(),b=read(),c=read(),d=read();
			vol[a]=b;vol[c]=d;
			flag=1;
			rap(i,1,n) f[i]=dp[i]=0;
			solve(1,0);
			dep(i,a) if(e[i].y==c){
				if(b==0&&d==0)flag=0;
				break;
			}
			if(!flag)puts("-1");
			else
			printf("%lld\n",min(dp[1],f[1]));
			vol[a]=2;vol[c]=2;
		}
		return 0;
	}
	return 0;
}
