#include<bits/stdc++.h>
using namespace std;
#define rap(i,j,k) for(int i=j;i<=k;++i)
#define rapp(i,j,k) for(int i=j;i>=k;--i)
#define ll long long
const int mod=1e9+7;
int n,m;
inline int read(){
	int NUM=0; bool f=1;
	char c=getchar();
	for(;c<'0'||c>'9';c=getchar())
	if(c=='-')f=0;
	for(;c>='0'&&c<='9';c=getchar())
	NUM=(NUM<<1)+(NUM<<3)+c-48;
	return f?NUM:-NUM;
}
void init(){
	n=read();m=read();
}
ll power(ll x,int y){
	ll ans=1;
	for(;y;y>>=1){
		if(y&1) ans=ans*x%mod;
		x=x*x%mod;
	}
	return ans;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	init();
	if(m==1){
		printf("%lld",power(2,n));
		return 0;
	}
	if(n==3&&m==3){
		printf("112");
		return 0;
	}
	if(n==2&&m==2){
		printf("12");
		return 0;
	}
	if(n<=2){
		printf("%lld",power(n+1,m)*4%mod);
		return 0;
	}
	return 0;
}
