/*by DennyQi*/
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <queue>
using namespace std;
const int MAXN = 5010;
const int MAXM = 10010;
const int INF = 0x3f3f3f3f;
inline int Max(int a, int b){ return (a>b) ? a : b; }
inline int Min(int a, int b){ return (a<b) ? a : b; }
inline int read(){
	int x = 0, zf = 1; register char c = getchar();
	while(c ^ '-' && (c < '0' || c > '9')) c = getchar();
	if(c == '-') zf = -1, c = getchar();
	while(c >= '0' && c <= '9') x = (x<<3) + (x<<1) + c - '0', c = getchar(); return x * zf;
}
int N,M,top,x,y;
int first[MAXN],nxt[MAXM],to[MAXM],cnt;
int ans[MAXN];
priority_queue <int, vector<int>, greater<int> > v[MAXN];
inline void link(int u, int v){
	to[++cnt] = v, nxt[cnt] = first[u], first[u] = cnt;
}
void Dfs(int u, int Fa){
	ans[++top] = u;
	for(int i = first[u]; i; i = nxt[i]){
		if(to[i] == Fa) continue;
		v[u].push(to[i]);
	}
	int cur;
	while(!v[u].empty()){
		cur = v[u].top();
		v[u].pop();
		Dfs(cur, u);
	}
}
inline void SolveTree(){
	for(int i = 1; i < N; ++i){
		x = read(), y = read();
		link(x, y);
		link(y, x);
	}
	Dfs(1, 0);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	N = read(), M = read();
	SolveTree();
	for(int i = 1; i <= N; ++i){
		printf("%d ", ans[i]);
	}
	return 0;
}
