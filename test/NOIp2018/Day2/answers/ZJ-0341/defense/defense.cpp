/*by DennyQi*/
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <queue>
#define int long long
using namespace std;
const int MAXN = 100010;
const int MAXM = 200010;
const int INF = 0x3f3f3f3f;
inline int Max(int a, int b){ return (a>b) ? a : b; }
inline int Min(int a, int b){ return (a<b) ? a : b; }
inline int read(){
	int x = 0, zf = 1; register char c = getchar();
	while(c ^ '-' && (c < '0' || c > '9')) c = getchar();
	if(c == '-') zf = -1, c = getchar();
	while(c >= '0' && c <= '9') x = (x<<3) + (x<<1) + c - '0', c = getchar(); return x * zf;
}
bool fail;
int N,M,x,y,a,b;
char _typ[5];
int p[MAXN],dp[MAXN][2];
int first[MAXN],nxt[MAXM],to[MAXM],cnt;
inline void link(int u, int v){
	to[++cnt] = v, nxt[cnt] = first[u], first[u] = cnt;
}
inline int mustvebe(int u){
	if(u==a){
		return x;
	}
	if(u==b){
		return y;
	}
	return -1;
}
void DP(int u, int Fa){
	int v;
	if(dp[u][0] != -1) dp[u][0] = 0;
	if(dp[u][1] != -1) dp[u][1] = p[u];
//	printf("u=%d\n",u);
	for(int i = first[u]; i; i = nxt[i]){
		if((v = to[i]) == Fa) continue;
		DP(v, u);
		if(dp[u][0] != -1){
			if(dp[v][1] == -1){
				dp[u][0] = -1;
			}
			else{
//				printf("dp[%d][0] += dp[%d][1]\n",u,v);
				dp[u][0] += dp[v][1];
			}	
		}
		
		if(dp[u][1] != -1){
			if(dp[v][0] == -1 && dp[v][1] == -1){
				dp[u][1] = -1;
				continue;
			}
			if(dp[v][0] == -1){
				dp[u][1] += dp[v][1];
			}
			else if(dp[v][1] == -1){
				dp[u][1] += dp[v][0];
			}
			else{
				dp[u][1] += Min(dp[v][0], dp[v][1]);
			}	
		}
		
	}
}
/*inline void debug(){
	for(int i = 1; i <= N; ++i){
		printf("dp[%d][0]=%d dp[%d][1]=%d\n",i,dp[i][0],i,dp[i][1]);
	}
}*/
#undef int
int main(){
#define int long long
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	N = read(), M = read();
	scanf("%s", _typ);
	for(int i = 1; i <= N; ++i) p[i] = read();
	for(int i = 1; i < N; ++i){
		x = read(), y = read();
//		printf("%d %d\n",x,y);
		link(x, y);
		link(y, x);
	}
	if(N <= 2000 && M <= 2000){
		for(int i = 1; i <= M; ++i){
			a = read(), x = read();
			b = read(), y = read();
			memset(dp, 0, sizeof dp);
//			printf("%d %d %d %d\n",a,x,b,y);
			dp[a][x^1] = -1;
			dp[b][y^1] = -1;
			DP(1, 0);
//			debug();
//			printf(">>>>> %d %d\n",dp[1][0],dp[1][1]);
			if(dp[1][0]==-1 && dp[1][1]==-1){
				printf("-1\n");
				continue;
			}
			if(dp[1][0] != -1 && dp[1][1] != -1){
				printf("%lld\n", Min(dp[1][0], dp[1][1]));
			}
			else if(dp[1][0] == -1){
				printf("%lld\n", dp[1][1]);
			}
			else{
				printf("%lld\n", dp[1][0]);
			}
		}	
		return 0;
	}
	
	return 0;
}
