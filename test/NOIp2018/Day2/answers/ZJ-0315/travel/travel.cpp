#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<string>
#define N 5010
using namespace std;
int T[N][N],n,m,a[N],ans[N],t=0,f[N],tt=1;
bool bo[N];
inline void Deal_First()
{
	memset(bo,0,sizeof(bo));
	memset(T,0,sizeof(T));
	memset(ans,0,sizeof(ans));
	bo[1]=1;
	return;
}
inline void mrz_add(int k1,int k2)
{
	T[k1][0]++;
	T[k1][T[k1][0]]=k2;
	T[k2][0]++;
	T[k2][T[k2][0]]=k1;
	return;
}
void mrz_DFS_a(int kk)
{
	t++;
	ans[t]=kk;
	if(t==n) return;
	for(int i=f[kk]+1;i<=T[kk][0];i++) if(bo[T[kk][i]]==0)
	{
		f[kk]=i;
		bo[T[kk][i]]=1;
		mrz_DFS_a(T[kk][i]);
	}
	return;
}
void mrz_DFS_b(int kk)
{
	if(tt==n)
	{
		bool bb=0;
		for(int i=1;i<=n;i++) if(f[i]<ans[i]) 
		{
			bb=1;
			break;
		} else if(f[i]>ans[i]) break;
		if(bb==1) for(int i=1;i<=n;i++) ans[i]=f[i];
		return;
	}
	for(int i=1;i<=T[kk][0];i++) if(bo[T[kk][i]]==0)
	{
		bo[T[kk][i]]=1;
		tt++;
		f[tt]=T[kk][i];
		mrz_DFS_b(T[kk][i]);
		tt--;
		bo[T[kk][i]]=0;
	}
	if(kk!=1) 
	{
		for(int i=tt;i>=1;i--) if(f[i]==kk)
		{
			mrz_DFS_b(f[i-1]);
			break;	
		}
	}
	return;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	Deal_First();
	int u,v;
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&u,&v);
		mrz_add(u,v);
	}
	if(m==n-1)
	{
		for(int i=1;i<=n;i++)
		{
			for(int j=1;j<=T[i][0];j++) a[j]=T[i][j];
			sort(a+1,a+T[i][0]+1);
			for(int j=1;j<=T[i][0];j++) T[i][j]=a[j];
		}
		mrz_DFS_a(1);
		for(int i=1;i<=n;i++) printf("%d ",ans[i]);
	} else
	{
		f[1]=1;
		for(int i=1;i<=n;i++) ans[i]=n;
		mrz_DFS_b(1);
		for(int i=1;i<=n;i++) printf("%d ",ans[i]);
	}
	return 0;
}
