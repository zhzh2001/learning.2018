#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<string>
#define mod 1000000007
using namespace std;
int a[80],n,m,f[100001],tx=0,tt=0,sh_a[100001],sh_b[100001],C[100001][20],s[20],two[9],ans;
void DFS_a(int kn,int km)
{
	bool bo=0;
	if(kn>1)
	{
		tt++;
		s[tt]=0;
		DFS_a(kn-1,km);
		tt--;
		bo=1;
	}
	if(km>1)
	{
		tt++;
		s[tt]=1;
		DFS_a(kn,km-1);
		tt--;
		bo=1;
	}
	if(bo==0)
	{
		tx++;
		f[tx]=tt;
		for(int i=1;i<=tt;i++) C[tx][i]=s[i]; 
	}
	return;
}
void DFS_b(int x)
{
	if(x>n*m)
	{
		int aa[20],kx,ky;
		for(int i=1;i<=tx;i++)
		{
			kx=1;
			ky=1;
			aa[1]=a[1];
			for(int j=1;j<=f[i];j++)
			{
				if(C[i][j]==1) ky++;
				else kx++;
				aa[j+1]=a[(kx-1)*m+ky];
			}
			int SS=0;
			for(int j=f[i]+1;j>=1;j--) SS+=two[f[i]+1-j]*aa[j];
			sh_b[i]=SS;
		}
		bool boo=0;
		for(int i=1;i<=tx;i++)
		{
			for(int j=1;j<=tx;j++) if(sh_a[i]>sh_a[j])
			{
				if(sh_b[i]>sh_b[j])
				{
					boo=1;
					break;
				}
			}
			if(boo==1) break;
		}
		if(boo==0) ans++;
	} else
	{
		a[x]=1;
		DFS_b(x+1);
		a[x]=0;
		DFS_b(x+1);
	}
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	two[0]=1;
	two[1]=2;
	two[2]=4;
	two[3]=8;
	two[4]=16;
	two[5]=32;
	two[6]=64;
	two[7]=128;
	two[8]=256;
	two[9]=512;
	two[10]=1024;
	two[11]=2048;
	two[12]=2048*2;
	two[13]=2048*4;
	two[14]=2048*8;
	two[15]=2048*16;
	memset(f,0,sizeof(f));
	scanf("%d%d",&n,&m);
	DFS_a(n,m);
	for(int i=1;i<=tx;i++)
	{
		int SS=0;
		for(int j=f[i];j>=1;j--) SS+=two[f[i]-j]*C[i][j];
		sh_a[i]=SS;
	}
	ans=0;
	DFS_b(1);
	cout<<ans<<endl;
	return 0;
}
