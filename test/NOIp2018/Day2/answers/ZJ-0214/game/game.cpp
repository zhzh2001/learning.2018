#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<cmath>
using namespace std;
typedef long long ll;
const ll mod=1e9+7,size=1e6+10;
ll n,m,f[10][size][2];
ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
ll dfs(ll x,ll y,ll c){
	if(x>n||y>m||x<1||y<1) return 0;
	if(x==n&&y==m) return 1;
	if(f[x][y][c]) return f[x][y][c];
	ll res=0;
	if(x==n){
		res=((res+dfs(x,y+1,0)%mod)+dfs(x,y+1,1))%mod;
	}
	else if(y==m){
		res=((res+dfs(x+1,y,0))%mod+dfs(x+1,y,1))%mod;
	}
	else{
		res=(res+3*dfs(x+1,y+1,0)%mod+3*dfs(x+1,y+1,1)%mod)%mod;
	}
	return f[x][y][c]=res;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();m=read();
	dfs(1ll,1ll,0);
	printf("%lld\n",(2*f[1][1][0])%mod);
	fclose(stdin);fclose(stdout);
	return 0;
}
