#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<cmath>
using namespace std;
typedef long long ll;
const int size=5e3+10,maxn=1e6+10;
int n,m,top,tot,cnt,v[size],fa[size],du[size],ins[maxn],vis[size][size],e[size][size],sta[maxn];
ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
void dfs(int x){
	if(x==0) return;
	if(!ins[x]){
		ins[x]=1;sta[++top]=x;
	}
	if(du[x]){
		for(int i=1;i<=n;i++){
			if(e[x][i]&&!vis[x][i]){
				fa[i]=x;du[x]--;du[i]--;
				vis[x][i]=vis[i][x]=1;
				dfs(i);break;
			}
		}
	}
	else{
		dfs(fa[x]);
	}
}
void dfs2(int x){
	if(!ins[x]){
		ins[x]=1;sta[++top]=x;
	}
	for(int i=1;i<=n;i++){
		if(e[x][i]&&vis[x][i]==0){
			vis[x][i]=1;dfs2(i);
		}
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		e[x][y]=e[y][x]=1;du[x]++;du[y]++;
	}
	if(m==n-1) dfs(1);
	else{
		dfs2(1);
	}
	if(m==n-1){
		for(int i=1;i<=n;i++){
			printf("%d ",sta[i]);
		}
	}
	else{
		for(int i=1;i<=n;i++){
			printf("%d ",sta[i]);
		}	
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
