#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<cmath>
using namespace std;
typedef long long ll;
const ll size=1e5+10,maxn=2e3+10,inf=1e18;
ll n,m,tot,flag,dp[size][2],v[size],p[size],head[size],ver[size*2],nxt[size*2];char ch[20];
void add(ll x,ll y){
	ver[++tot]=y;nxt[tot]=head[x];head[x]=tot;
}
ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
void dfs(ll x,ll f){
	if(flag==1) return;
	if(v[x]==1){
		dp[x][0]=0;dp[x][1]=inf;
		for(ll i=head[x];i;i=nxt[i]){
			ll y=ver[i];
			if(y==f) continue;
			dfs(y,x);
			if(v[y]==1){
				flag=1;return;
			}
			dp[x][0]+=dp[y][1];
		}
	}
	else if(v[x]==2){
		dp[x][0]=inf;dp[x][1]=p[x];
		for(ll i=head[x];i;i=nxt[i]){
			ll y=ver[i];
			if(y==f) continue;
			dfs(y,x);
			dp[x][1]+=min(dp[y][0],dp[y][1]);
		}
	}
	else{
		dp[x][0]=0;dp[x][1]=p[x];
		for(ll i=head[x];i;i=nxt[i]){
			ll y=ver[i];
			if(y==f) continue;
			dfs(y,x);
			dp[x][0]+=dp[y][1];
			dp[x][1]+=min(dp[y][0],dp[y][1]);
		}
	}
}
int main(){
	freopen("defence.in","r",stdin);
	freopen("defence.out","w",stdout);
	n=read();m=read();scanf("%s",ch);
	for(ll i=1;i<=n;i++) p[i]=read();
	for(ll i=1;i<n;i++){
		ll x=read(),y=read();add(x,y);add(y,x);
	}	
		for(ll i=1;i<=m;i++){
			flag=0;
			ll a=read(),x=read(),b=read(),y=read();
			v[a]=x+1;v[b]=y+1;
			dfs(1,0);	
			if(flag){
				printf("-1\n");
			}
			else{
				ll t=min(dp[1][0],dp[1][1]);
				if(t==inf) printf("-1\n");else printf("%lld\n",t);
			}
			v[a]=0;v[b]=0;
		}
	
	fclose(stdin);fclose(stdout);
	return 0;
}
