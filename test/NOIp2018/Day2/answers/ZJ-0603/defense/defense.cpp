#include <bits/stdc++.h>
#define gc getchar()
#define ll long long
#define go(i,x) for (int i=head[x];i;i=Next[i])
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define N 200005
#define inf 1000000000000
#define int ll
using namespace std;
inline int read(){
	char c;int f=1,x=0;
	for (c=gc;c<'0'||c>'9';c=gc) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc) x=x*10+c-'0';
	return (f==1)?x:-x;
}
inline void wr(ll x){
	if (x<0){putchar('-');wr(-x);}
	else{if (x>=10) wr(x/10);putchar('0'+x%10);}
}
inline void wri(ll x){wr(x);putchar(' ');}
inline void wrn(ll x){wr(x);puts("");}
int n,m,p[N],a,x,b,y;
ll ans,f[N],g1[N],g2[N];
char str[N];
int Next[N*2],head[N],to[N*2],nedge;
#define V to[i]
void add(int a,int b){
	Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;
}
void dfs(int x){
	f[x]=2;g1[x]=p[x];g2[x]=0;
	//g1[x]表示这个点选，g2[x]表示这个点不选 
	go(i,x){
		if (f[V]==2) continue;
		if (f[V]==0){g2[x]=inf;continue;}
		if (f[V]==1) continue;
		dfs(V);g1[x]+=min(g1[V],g2[V]);g2[x]+=g1[V];
	}
}
void solve1(){
	F(i,1,m){
		F(j,1,n) f[j]=-1;
		ans=0;
		a=read();x=read();b=read();y=read();
		f[a]=x;f[b]=y;
		if (x) ans+=p[a];if (y) ans+=p[b];
		if (x==y&&y==0){
			int pd=1;
			for (int j=head[a];j;j=Next[j]){
				if (to[j]==b) pd=0;
			}
			if (pd==0){
				puts("-1");
				continue;
			}
		}
		F(j,1,n){
			if (f[j]==-1){
				dfs(j);
				ans+=min(g1[j],g2[j]);
			}
		}
		wrn(ans);
	}
}
signed main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",str+1);
	F(i,1,n) p[i]=read();
	F(i,1,n-1){
		x=read();y=read();add(x,y);add(y,x);
	}
	if (n<=2000){
		solve1();return 0;
	}
	return 0;
}
