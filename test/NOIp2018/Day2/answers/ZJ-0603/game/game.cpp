#include <bits/stdc++.h>
#define gc getchar()
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define N 1000505
#define mod 1000000007
using namespace std;
inline int read(){
	char c;int f=1,x=0;
	for (c=gc;c<'0'||c>'9';c=gc) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc) x=x*10+c-'0';
	return (f==1)?x:-x;
}
inline void wr(int x){
	if (x<0){putchar('-');wr(-x);}
	else{if (x>=10) wr(x/10);putchar('0'+x%10);}
}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);puts("");}
int n,m,ans,z[20],z2[20],f[N],fx[N],fy[N],g[20],g2[20];
inline void add(int &x,int k){
	x+=k;x-=(x>=mod)?mod:0;
}
int check(int x,int y,int a,int b){
	int l=2,r=f[a]+1;
	F(i,l,r) z[i]=((i-l)<x)?0:1;
	if (fx[b]>fx[a]) l-=(fx[b]-fx[a]);
	F(i,l,l+f[b]-1) z2[i]=((i-l)<y)?0:1;
	int fl=l,fr=l+f[b]-1;
	int pd=1;
	F(i,2,f[a]){
		if (z[i]==z[i+1]){
			if (i-1>=fl&&i<=fr){
				if (z2[i-1]!=z2[i]) pd=0;
			}
		}
	}
	return pd;
}
void solve(int a,int b){
	F(i,0,f[a]){
		F(j,0,f[b]){
			if (check(i,j,a,b)) add(g2[j],g[i]);
		}
	}
	F(i,0,f[b]) g[i]=g2[i],g2[i]=0;
	F(i,f[b]+1,f[a]) g[i]=0;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();m=read();
	F(i,1,n+m-1){
		f[i]=min(n+m-i,min(i,min(n,m)));
	} 
	F(i,1,n){
		fx[i]=i;fy[i]=1;
	}
	F(i,2,m){
		fx[i+n-1]=n;fy[i+n-1]=i;
	}
	ans=0;
	for (int i=1;i<=n+m-1;i+=2){
		if (i==1){
			g[0]=g[1]=1;continue;
		}
		solve(i-2,i);
	}
	F(i,0,min(m,9)) add(ans,g[i]),g[i]=g2[i]=0;
	for (int i=2;i<=n+m-1;i+=2){
		if (i==2){
			g[0]=g[1]=g[2]=1;continue;
		}
		solve(i-2,i);
	}
	int t=0;
	F(i,0,min(m,9)) add(t,g[i]);
	ans=1LL*ans*t%mod;
	wrn(ans);
	return 0;
}
