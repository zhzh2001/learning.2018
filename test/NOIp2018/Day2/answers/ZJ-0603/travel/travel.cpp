#include <bits/stdc++.h>
#define gc getchar()
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,x) for (int i=head[x];i;i=Next[i])
#define pb push_back
#define be begin()
#define en end()
#define N 10005
using namespace std;
inline int read(){
	char c;int f=1,x=0;
	for (c=gc;c<'0'||c>'9';c=gc) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc) x=x*10+c-'0';
	return (f==1)?x:-x;
}
inline void wr(int x){
	if (x<0){putchar('-');wr(-x);}
	else{if (x>=10) wr(x/10);putchar('0'+x%10);}
}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);puts("");}
int n,m,ans[N],fa[N],vis[N],tot,x,y,b[N],fx,fy,h[N],hl,pd;
int Next[N*2],head[N],to[N*2],nedge;
#define V to[i]
void add(int a,int b){
	Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;
}
void g_h(int x){
	vis[x]=1;
	go(i,x){
		if (V==fa[x]) continue;
		if (vis[V]){
			if (hl>0) continue;
			for (int t=x;t!=V;t=fa[t]){
				h[++hl]=t;
			}
			h[++hl]=V;
			continue;
		}
		fa[V]=x;g_h(V);
	}
}
vector <int> a[N];
void dfs(int x){
	ans[++tot]=x;
	go(i,x){
		if (V==fa[x]) continue;
		fa[V]=x;a[x].pb(V);
	}
	if (a[x].empty()) return;
	sort(a[x].be,a[x].en);
	int t=a[x].size()-1;
	F(i,0,t) dfs(a[x][i]);
}
void solve1(){
	//处理树的情况
	dfs(1); 
	F(i,1,n) printf("%d ",ans[i]);
}
/*void dfs2(int x){
	//处理环的情况
	ans[++tot]=x;vis[x]=1;
	yk+=flag[x];
	go(i,x){
		if (vis[V]) continue;
		a[x].pb(V);
	}
	sort(a.be,m)
}*/
void dfs2(int x){
	if (pd==1) return;
	b[++tot]=x;vis[x]=1;
	if (b[tot]>ans[tot]&&pd==0){pd=1;return;}
	if (b[tot]<ans[tot]) pd=-1;
	if (a[x].empty()) return;
	int t=a[x].size()-1;
	int p=0;
	if (x==fx) p=fy;
	if (x==fy) p=fx;
	F(i,0,t){
		if (vis[a[x][i]]) continue;
		if (a[x][i]==p) continue;
		dfs2(a[x][i]);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	F(i,1,m){
		x=read();y=read();add(x,y);add(y,x);
	}
	if (m==n-1){
		solve1();return 0;
	}
	g_h(1);
	F(i,1,n) vis[i]=0;
	F(j,1,n){
		go(i,j){
			a[j].pb(V);
		}
		sort(a[j].be,a[j].en);
	}
	F(i,1,n) ans[i]=n+1;
	D(i,hl,1){
		tot=0;pd=0;
		fx=h[i];fy=h[(i==hl)?1:i+1];
		F(j,1,n) vis[j]=0;
		dfs2(1);
		if (pd==-1){
			F(j,1,n) ans[j]=b[j];
		}
	}
	F(i,1,n) wri(ans[i]);
	return 0;
}
