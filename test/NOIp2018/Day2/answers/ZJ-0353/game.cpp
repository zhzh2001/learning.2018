#include<bits/stdc++.h>
using namespace std;
const int mo=1000000007;
int g,d[1000005],a[105][105],m,n,ans,t;
long long poww(long long x,int y)
{
	long long ans=1;
	while (y!=0)
	{
		if (y%2==1) ans=ans*x%mo;
		y/=2;
		x=x*x%mo;	
	}
	return ans;
}
void bs(int i,int j,int v)
{
	if (g>1&&d[g]>d[g-1]) return;
	if (i==0&&j==0) {g++; d[g]=v*2+a[m][n]; return;}
	if (i==0) {bs(i,j-1,v*2+a[m-i][n-j]); return;}
	if (j==0) {bs(i-1,j,v*2+a[m-i][n-j]); return;}
	bs(i,j-1,v*2+a[m-i][n-j]);
	bs(i-1,j,v*2+a[m-i][n-j]);
}
bool pd()
{
	g=0;
	bs(m-1,n-1,0);
	for (int i=2;i<=g;i++) if (d[i]>d[i-1]) return false;
	return true;
}
void dfs(int i,int j)
{
	
	if (i>m) {if (pd()) ans++; return;}
	if (j>n) {dfs(i+1,1); return;}
	{a[i][j]=0; dfs(i,j+1);}
	if (i==1||j==n||a[i-1][j+1]==1) 
	{a[i][j]=1; dfs(i,j+1);}
}
int main()
{
freopen("game.in","r",stdin);
freopen("game.out","w",stdout);
	scanf("%d%d",&m,&n);
	if (m>n) {t=m; m=n; n=t;}
	if (m==2) 
	{
		printf("%lld\n",4ll*poww(3,n-1)%mo);
		return 0;
	}
	if (m==3)
	{
		if (n==1) printf("8\n");
		else if (n==2) printf("36\n");
		else printf("%lld\n",112ll*poww(3,n-3)%mo);	
		return 0;	
	}
	if (m==4)
	{
		if (n==1) printf("16\n");
		else if (n==2) printf("108\n");
		else if (n==3) printf("336\n");
		else if (n==4) printf("912\n");	
		else printf("%lld\n",2688ll*poww(3,n-5)%mo);	
		return 0;
	}	
	if (m==5)
	{
		if (n==1) printf("32\n");
		else if (n==2) printf("324\n");
		else if (n==3) printf("1098\n");
		else if (n==4) printf("2688\n");	
		else if (n==5) printf("7136\n");
		else printf("%lld\n",21312ll*poww(3,n-6)%mo);	
		return 0;
	}	
	if (m==6)
	{
		if (n==1) printf("64\n");
		else if (n==2) printf("972\n");
		else if (n==3) printf("3294\n");
		else if (n==4) printf("8064\n");	
		else if (n==5) printf("21408\n");
		else if (n==6) printf("56768\n");
		else printf("%lld\n",170112ll*poww(3,n-6)%mo);	
		return 0;		
	}	
	if (m==7)
	{
		if (n==1) printf("128\n");
		else if (n==2) printf("2916\n");
		else if (n==3) printf("9882\n");
		else if (n==4) printf("24192\n");	
		else if (n==5) printf("64224\n");
		else if (n==6) printf("170304\n");
	//	else if (n==7) printf("170304\n");
//		else printf("%lld\n",170112ll*poww(3,n-6)%mo);	
		if (n<=6) return 0;		
	}	
	if (m==8)
	{
		if (n==1) printf("256\n");
		else if (n==2) printf("8748\n");
		else if (n==3) printf("29646\n");
		else if (n==4) printf("72576\n");	
		else if (n==5) printf("192672\n");
		else if (n==6) printf("510912\n");
	//	else if (n==7) printf("170304\n");
	//	else if (n==8) printf("170304\n");
	//	else printf("%lld\n",170112ll*poww(3,n-6)%mo);	
		if (n<=6) return 0;		
	}
	dfs(1,1);
	printf("%d\n",ans);	
}
