#include<cstdio>
#include<cstring>
#include<vector>
#include<iostream>
#include<algorithm>
#define ll long long
static const int M = (int)1e5+5;

std::vector<int>es[M];

int cost[M];
char type[4];
template<class T>
inline bool chkmi(T &a,T b){
	if(a==-1||a>b){
		a=b;
		return true;
	}
	return false;
}
namespace Water{
	bool Must[M],Mustnt[M];
	ll f[2][M],a;
	void dfs(int u,int fa){
		for(int i=0,v;i<es[u].size();++i){
			v=es[u][i];
			if(v==fa)continue;
			dfs(v,u);
		}
		//choose u
		if(!Mustnt[u]){
			f[1][u]=cost[u];
			for(int i=0,v;i<es[u].size();++i){
				v=es[u][i];
				if(v==fa)continue;
				a=-1;
				if(~f[0][v])chkmi(a,f[0][v]);
				if(~f[1][v])chkmi(a,f[1][v]);
				if(~a)f[1][u]+=a;
				else{
					f[1][u]=-1;
					break;
				}
			}
		}
		if(!Must[u]){
			f[0][u]=0;
			for(int i=0,v;i<es[u].size();++i){
				v=es[u][i];
				if(v==fa)continue;
				if(~f[1][v])f[0][u]+=f[1][v];
				else{
					f[0][u]=-1;
					break;
				}
			}
		}
	}
	void solve(int n,int m){
		ll ans=-1;
		for(int i=1,a,b,x,y;i<=m;++i){
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(x)Must[a]=true;
			else Mustnt[a]=true;
			if(y)Must[b]=true;
			else Mustnt[b]=true;
			memset(f[0]+1,-1,sizeof(ll)*n);
			memset(f[1]+1,-1,sizeof(ll)*n);
			dfs(1,0);
			ans=-1;
			if(~f[0][1])chkmi(ans,f[0][1]);
			if(~f[1][1])chkmi(ans,f[1][1]);
			printf("%lld\n",ans);
			Must[a]=Must[b]=Mustnt[a]=Mustnt[b]=false;
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m;
	scanf("%d%d%s",&n,&m,type);
	for(int i=1;i<=n;++i)scanf("%d",cost+i);
	for(int u,v,i=1;i<n;++i){
		scanf("%d%d",&u,&v);
		es[u].push_back(v);
		es[v].push_back(u);
	}
	Water::solve(n,m);
	return 0;
}
