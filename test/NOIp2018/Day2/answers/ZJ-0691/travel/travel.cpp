#include<cstdio>
#include<cstring>
#include<vector>
#include<iostream>
#include<algorithm>
static const int M = 5005;
struct node{
	int to,id;
	bool operator <(const node &tmp)const{
		return to<tmp.to;
	}
};
std::vector<node>es[M];
namespace Water{
	int id_tot;
	int que[M];
	void dfs(int u,int fa){
		que[++id_tot]=u;
		for(int i=0,v;i<es[u].size();++i){
			v=es[u][i].to;
			if(v==fa)continue;
			dfs(v,u);
		}
	}
	void solve(int n){
		dfs(1,0);
		for(int i=1;i<=n;++i){
			printf("%d",que[i]);
			if(i<n)putchar(' ');
		}
		puts("");
	}
}
namespace Lar{//m==n
	int fa[M],fid[M];
	int S,T,forbid;
	int dfn[M],id_tot;
	int que[M];
	int ans[M];
	void init(int u){
		dfn[u]=++id_tot;
		ans[id_tot]=u;
		for(int i=0,v;i<es[u].size();++i){
			v=es[u][i].to;
			if(v==fa[u])continue;
			if(!dfn[v]){
				fid[v]=es[u][i].id;
				fa[v]=u;
				init(v);
			}else if(dfn[v]<dfn[u])S=v,T=u;
		}
	}
	bool cmp(int n){//que ?<ans
		for(int i=1;i<=n;++i){
			if(ans[i]!=que[i])return que[i]<ans[i];
		}
		return false;
	}
	void dfs(int u,int f){
		que[++id_tot]=u;
		for(int i=0,v;i<es[u].size();++i){
			v=es[u][i].to;
			if(v==f||es[u][i].id==forbid)continue;
			dfs(v,u);
		}
	}
	void solve(int n){
		id_tot=0;
		init(1);
		while(T!=S){
			forbid=fid[T];
			id_tot=0;
			dfs(1,0);
			if(cmp(n))memcpy(ans+1,que+1,sizeof(int)*n);
			T=fa[T];
		}
		for(int i=1;i<=n;++i){
			printf("%d",ans[i]);
			if(i<n)putchar(' ');
		}
		puts("");
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1,u,v;i<=m;++i){
		scanf("%d%d",&u,&v);
		es[u].push_back((node){v,i});
		es[v].push_back((node){u,i});
	}
	for(int i=1;i<=n;++i)std::sort(es[i].begin(),es[i].end());
	if(m<n)Water::solve(n);
	else Lar::solve(n);
	return 0;
}
