#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
static const int P = (int)1e9+7;
namespace Water{
	int a[10][10];
	int A[20],B[20];
	bool check(int n,int m){
		for(int i=1;i<n;++i){
			for(int j=1;j<m;++j){
				if(a[i+1][j]<a[i][j+1])return false;
				if(a[i+1][j]>a[i][j+1])continue;
				int sa=0,sb=0;
				for(int k=j+1;k<m;++k)A[++sa]=a[i+1][k];
				for(int k=i+1;k<n;++k)A[++sa]=a[k][m];
				for(int k=i+1;k<n;++k)B[++sb]=a[k][j+1];
				for(int k=j+1;k<m;++k)B[++sb]=a[n][k];
				//A>=B
				for(int k=1;k<=sa;++k){
					if(A[k]<B[k])return false;
					if(A[k]>B[k])break;
				}
			}
		}
		return true;
	}
	void solve(int n,int m){
		int tot=1<<(n*m),ans=0;
		for(int i=0,mask;i<tot;++i){
			mask=i;
			for(int j=1;j<=n;++j)
				for(int k=1;k<=m;++k)a[j][k]=mask&1,mask>>=1;
			if(check(n,m))++ans;
		}
		ans%=P;
		printf("%d\n",ans);
	}
}
namespace One{
	void solve(int n,int m){
		int ans=1;
		for(int i=1;i<=m;++i)ans=(ans<<1)%P;
		printf("%d\n",ans);
	}
}
namespace Two{
	void solve(int n,int m){
		int ans=1;
		for(int i=1;i<m;++i)ans=(3ll*ans)%P;
		ans=(4ll*ans)%P;
		printf("%d\n",ans);
	}
}
namespace Lar{
	void solve(int n,int m){
		int ans=0;
		for(int i=2;i<=m;++i){
		}
		ans=(ans<<1)%P;
		printf("%d\n",ans);
	}
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	if(n<=3&&m<=3)Water::solve(n,m);
	else if(n==1)One::solve(n,m);
	else if(n==2)Two::solve(n,m);
	else Lar::solve(n,m);
	return 0;
}
