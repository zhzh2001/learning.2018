#include <cstdio>
#include <cstdlib>
#include <cstring>

class edge {
	public:
		int to, next;
}all[1123456];
int end(2), head[112345];
inline void add(int fr, int to) {
	all[end].to = to;
	all[end].next = head[fr];
	head[fr] = end++;
}
char cmd[112];
int n, m;
int con[112345], cost[112345];
int state[112345], lg[112345];
int cnt, c;
void dfs(int now) {
	if (now == n + 1){
		memset(lg, 0, sizeof(lg));
		int cnt(0), tem(0);
		for (int i(1); i <= n; ++i) {
			if (!state[i]) continue;
			if (lg[i] == 0) ++cnt, lg[i] = 1;
			tem += cost[i];
			for (int j(head[i]); j; j = all[j].next) {
				if (lg[all[j].to] == 0) {
					lg[all[j].to] = 1;
					++cnt;
				}
			}
		}
		if (cnt == n && tem < c) {
			c = tem;
		}
		return;
	}
	if (con[now] == 0) {
		state[now] = false;
		dfs(now + 1);
	} else if (con[now] == 1) {
		state[now] = true;
		dfs(now + 1);
	} else {
		state[now] = true;
		dfs(now + 1);
		state[now] = false;
		dfs(now + 1);
	}
	state[now] = false;
}

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d%s", &n, &m, cmd);
	for (int i(1); i <= n; ++i) scanf("%d", &cost[i]);
	for (int i(0); i != n - 1; ++i) {
		int fr, to;
		scanf("%d%d", &fr, &to);
		add(fr, to);
		add(to, fr);
	}
	memset(con, -1, sizeof(con));
	while (m--) {
		int a, b, c, d;
		scanf("%d%d%d%d", &a, &b, &c, &d);
		con[a] = b; con[c] = d;
		::c = 112345678;
		dfs(1);
		con[a] = -1; con[c] = -1;
		if (::c == 112345678) ::c = -1;
		printf("%d\n", ::c);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
