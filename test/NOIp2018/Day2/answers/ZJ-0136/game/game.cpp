#include <cstdio>
#include <cstdlib>

#include <string>

namespace N3 {
	char map[12][12];
	class item {
		public:
			std::string a, b;
	}all[1123456];
	int n, m, end;
	
	void dfsway(int x, int y, std::string now, std::string val) {
		if (x == n && y == m) {
			all[end].a = now;
			all[end].b = val + map[x][y];
			++end;
			return;
		}
		if (x != n) {
			dfsway(x + 1, y, now + 'D', val + map[x][y]);
		}
		if (y != m) {
			dfsway(x, y + 1, now + 'R', val + map[x][y]);
		}
	}
	bool judge() {
		/*for (int i(1); i < n; ++i) {
			for (int j(2); j <= m; ++j) {
				if (map[i][j] == '1' && map[i + 1][j - 1] == '0') return false;
			}
		}
		return true;*/
		end = 0;
		dfsway(1, 1, std::string(), std::string());
		for (int i(0); i != end; ++i) {
			for (int j(0); j != end; ++j) {
				if (all[i].a > all[j].a && all[i].b > all[j].b)
				{
					return false;
				}
			}
		}
		return true;
	}
	int ans(0);
	void dfs(int x, int y) {
		if (y == m + 1) {
			++x;
			y = 1;
		}
		if (x == n + 1) {
			if (judge()) {
				++ans;
			}
			return;
		}
		map[x][y] = '0';
		dfs(x, y + 1);
		map[x][y] = '1';
		dfs(x, y + 1);
	}
}
const int mod(1e9+7);

int dp[10][1123456];
int n, m;
int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n <= 3 && m <= 3) {
		N3::n = n;
		N3::m = m;
		N3::dfs(1, 1);
		printf("%d\n", N3::ans);
		return 0;
	}
	for (int i(0); i != (1 << m); ++i) {
		dp[0][i] = 1;
	}
	for (int i(0); i != n - 1; ++i) {
		for (int j(0); j != (1 << m); ++j) {
			int org(j << 1);
			if (org & (1 << m)) org ^= 1 << m;
			int state = (1 << m) - 1 - org;
			for (int k(state); ; k = state & (k - 1)) {
				dp[i + 1][org | k] = (dp[i + 1][org | k] + dp[i][j]) % mod;
				if (k == 0) break;
			}
		}
	}
	int ans(0);
	for (int i(0); i != (1 << m); ++i) {
		ans = (ans + dp[n - 1][i]) % mod;
	}
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
