#include <cstdio>
#include <cstdlib>

#include <queue>
#include <set>

class edge {
	public:
		int to, next;
}all[112345];
int head[51234], end(2);
void add(int fr, int to) {
	all[end].to = to;
	all[end].next = head[fr];
	head[fr] = end++;
}

bool vising[51234];
bool incir[51234];
int cir[51234], endcir;
int find_cir(int now, int fr) {
	if (vising[now]) return now;
	vising[now] = true;
	for (int i(head[now]); i; i = all[i].next) {
		if (all[i].to != fr) {
			int v(find_cir(all[i].to, now));
			if (v) {
				incir[now] = true;
				cir[endcir++] = now;
				if (v == now) return 0;
				return v;
			}
		}
	}
	return 0;
}
int dep[51234];
void bfs() {
	std::queue<int> que;
	for (int i(0); i != endcir; ++i) {
		dep[cir[i]] = 1;
		que.push(cir[i]);
	}
	while (!que.empty()) {
		int now(que.front());
		que.pop();
		for (int i(head[now]); i; i = all[i].next) {
			if (dep[all[i].to] == 0) {
				dep[all[i].to] = dep[now] + 1;
				que.push(all[i].to);
			}
		}
	}
}
int ans[51234], endans;
void dfs_tree(int now, int fr) {
	ans[endans++] = now;
	std::set<int> choose;
	for (int i(head[now]); i; i = all[i].next) {
		if (all[i].to != fr) {
			choose.insert(all[i].to);
		}
	}
	for (std::set<int>::iterator itr(choose.begin()); itr != choose.end(); ++itr) {
		dfs_tree(*itr, now);
	}
}
bool bVis[51234];
int minone(1123456);
void dfs(int now, bool flag) {
	if (now == 64) {
		now = 64;
	}
	if (minone == now) minone = 1123456;
	ans[endans++] = now;
	bVis[now] = true;
	std::set<int> choose;
	int up(0), oth(1123456);
	for (int i(head[now]); i ; i = all[i].next) {
		if (!bVis[all[i].to]) {
			if (dep[all[i].to] > dep[now]) {
				choose.insert(all[i].to);
			} else {
				if (up) oth = all[i].to;
				else up = all[i].to;
			}
		}
	}
	if (up > oth) up ^= oth ^= up ^= oth;
	if (up == 0) {
		for (std::set<int>::iterator itr(choose.begin()); itr != choose.end(); ++itr) {
			dfs_tree(*itr, now);
			bVis[*itr] = true;
		}
		return;
	}
	if (!incir[now] || flag) {
		std::set<int>::iterator itr(choose.begin());
		while (itr != choose.end()) {
			if (*itr > up) break;
			bVis[*itr] = true;
			dfs_tree(*itr, now);
			++itr;
		}
		dfs(up, flag);
		while (itr != choose.end()) {
			bVis[*itr] = true;
			dfs_tree(*itr, now);
			++itr;
		}
	} else {
		std::set<int>::iterator itr(choose.begin());
		while (itr != choose.end()) {
			if (*itr > up) break;
			bVis[*itr] = true;
			dfs_tree(*itr, now);
			++itr;
		}
		if (minone < up) {
			return;
		}
		if (oth != 1123456) {
			minone = oth;
			if (itr != choose.end() && *itr < oth) {
				minone = *itr;
			}
		} else {
			if (itr != choose.end()) {
				minone = *itr;
			}
		}
		dfs(up, false);
		while (itr != choose.end()) {
			if (*itr > oth) break;
			bVis[*itr] = true;
			dfs_tree(*itr, now);
			++itr;
		}
		if (oth != 1123456) {
			dfs(oth, true);
		}
		while (itr != choose.end()) {
			bVis[*itr] = true;
			dfs_tree(*itr, now);
			++itr;
		}
	}
}
int n, m;
int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i(0); i != m; ++i) {
		int f, t;
		scanf("%d%d", &f, &t);
		add(f, t);
		add(t, f);
	}
	if (m == n - 1) {
		dfs_tree(1, 0);
	} else {
		find_cir(1, 0);
		bfs();
		dfs(1, 0);
	}
	for (int i(0); i != endans; ++i) {
		if (i) printf(" ");
		printf("%d", ans[i]);
	}
	printf("\n");
	fclose(stdin);
	fclose(stdout);
	return 0;
}
