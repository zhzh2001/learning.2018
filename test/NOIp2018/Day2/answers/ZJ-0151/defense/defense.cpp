#include<cstdio>
#include<cstring>
#include<algorithm>
const long long inf=1e18;
using namespace std;
const int maxn=100010;
long long g[maxn],f[maxn],p[maxn];
int to[maxn<<1],nex[maxn<<1],lnk[maxn],vis[maxn],e,n,m;
char str[10];
void add(int u,int v){
	to[++e]=v; nex[e]=lnk[u]; lnk[u]=e;
}
void dfs(int u,int fa){
	f[u]=p[u];
	g[u]=0;
	for (int i=lnk[u]; i; i=nex[i]){
		int v=to[i];
		if (v==fa) continue;
		dfs(v,u);
		f[u]+=g[v];
		g[u]+=f[v];
	}
	if (vis[u]==1) g[u]=inf;
	if (vis[u]==0) f[u]=inf;
	if (f[u]<g[u]) g[u]=f[u];
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,str);
	for (int i=1; i<=n; i++) scanf("%lld",&p[i]);
	for (int i=1,u,v; i<n; i++) scanf("%d%d",&u,&v),add(v,u),add(u,v);
	if (n<=10000){
		memset(vis,-1,sizeof(vis));
		for (int i=1,a,x,b,y; i<=m; i++){
			scanf("%d%d%d%d",&a,&x,&b,&y);
			vis[a]=x;
			vis[b]=y;
			dfs(1,0);
			vis[a]=vis[b]=-1;
			if (g[1]>=inf) puts("-1");
			else printf("%lld\n",g[1]);
		}
	}
	return 0;
}
