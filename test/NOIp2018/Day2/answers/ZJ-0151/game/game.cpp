#include<cstdio>
const int mod=1e9+7;
int Pow(int x,int u){
	int c=1;
	for(;u;u>>=1,x=1LL*x*x%mod) if (u&1) c=1LL*c*x%mod;
	return c;
}
int n,m;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1){
		printf("%d\n",Pow(2,m));
	}
	if (n==2){
		printf("%lld\n",4LL*Pow(3,m-1)%mod);
	}
	if (n==3){
		if (m==1) printf("%d\n",8);
		else if (m==2) printf("%d\n",36);
		else{
			puts("112");
		}
	}
}
