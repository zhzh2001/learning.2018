#include<cstdio>
#include<vector>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=5010;
vector<pair<int,int> >g[maxn];
int id[maxn],ans[maxn],vis[maxn],total,de,n,m;
void dfs(int u){
	id[++total]=u; vis[u]=1;
	for (vector<pair<int,int> >::iterator it=g[u].begin(); it!=g[u].end(); it++){
		if (vis[(*it).first] || (*it).second==de) continue;
		dfs((*it).first);
	}
}
void work(){
	total=0;
	memset(vis,0,sizeof(vis));
	dfs(1);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,u,v; i<=m; i++) scanf("%d%d",&u,&v),g[u].push_back(make_pair(v,i)),g[v].push_back(make_pair(u,i));
	for (int i=1; i<=n; i++) sort(g[i].begin(),g[i].end());
	de=0;
	if (m==n-1){
		work();
		for (int i=1; i<=n; i++) printf("%d ",id[i]);
		return 0;
	}
	ans[1]=n+1;
	for (de=1; de<=m; de++){
		work();
		if (total==n){
			bool flag=0;
			for (int i=1; i<=n; i++) if (id[i]<ans[i]){
				flag=1;
				break;
			}else if (id[i]>ans[i]) break;
			if (flag)
				for (int i=1; i<=n; i++) ans[i]=id[i];
		}
	}
	for (int i=1; i<=n; i++) printf("%d ",ans[i]);
	return 0;
}
