#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=5e3+100;
struct Edge{
	int u,v;
}edge[maxn];
int degree[maxn];
vector<int>g[maxn];
int n,m;
void read(int &x){
	x=0;int fl=1;char tmp=getchar();
	while(tmp<'0'||tmp>'9'){if(tmp=='-') fl=-fl;tmp=getchar();}
	while(tmp>='0'&&tmp<='9') x=(x<<1)+(x<<3)+tmp-'0' ,tmp=getchar();
	x=x*fl;
}
void dfs(int u,int fa){
	vector<int>::iterator it;
	for(it=g[u].begin();it!=g[u].end();it++){
		int v=*it;
		if(v==fa) continue;
		printf(" %d",v);
		dfs(v,u);
	}
}
int fu,fv;
bool vis[maxn];
int ans[maxn];
int p[maxn],cnt;
void dfs_m(int u,int fa){
	p[++cnt]=u;vis[u]=1;
	vector<int>::iterator it;
	for(it=g[u].begin();it!=g[u].end();it++){
		int v=*it;
		if(vis[v]) continue;
		if((u==fu&&v==fv)||(u==fv&&v==fu)) continue;
		dfs_m(v,u);
	}
}
void get_ans(){
	for(int i=1;i<=n;i++)
		if(p[i]>ans[i]) return;
		else if(p[i]<ans[i]) break;
	for(int i=1;i<=n;i++)
		ans[i]=p[i];
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<=m;i++){
		int x,y;read(x),read(y);
		edge[i].u=x,edge[i].v=y;
		degree[x]++,degree[y]++;
		g[x].push_back(y);
		g[y].push_back(x);
	}
	for(int i=1;i<=n;i++){
		sort(g[i].begin(),g[i].end());
	}
	if(m==n-1){
		printf("1");
		dfs(1,-1);
	}
	else{
		memset(ans,0x3f,sizeof(ans));
		for(int i=1;i<=m;i++){
			if(degree[edge[i].u]>1&&degree[edge[i].v]>1){
				fu=edge[i].u,fv=edge[i].v;
				memset(vis,0,sizeof(vis));
				cnt=0,dfs_m(1,-1);
				get_ans();
			}
		}
		printf("1");
		for(int i=2;i<=n;i++)
			printf(" %d",ans[i]);
	}
	return 0;
}
