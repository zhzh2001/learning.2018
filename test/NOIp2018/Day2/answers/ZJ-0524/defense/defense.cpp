#include<map>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=1e5+1000;
struct Edge{
	int v,nxt;
}edge[maxn<<1];
int head[maxn],tot;
int n,m;
int x,bx,y,by;
char s[5];
int p[maxn];
inline void read(int &x){
	x=0;char tmp=getchar();
	while(tmp<'0'||tmp>'9') tmp=getchar();
	while(tmp>='0'&&tmp<='9') x=(x<<1)+(x<<3)+tmp-'0',tmp=getchar();
}
void add_edge(int x,int y){
	edge[tot]=(Edge){y,head[x]},head[x]=tot++;
}
map<pair<int,int>,int>mp;
int f[maxn][2];
void dfs(int u,int fa){
	f[u][0]=0,f[u][1]=p[u];
	for(int i=head[u];i!=-1;i=edge[i].nxt){
		int v=edge[i].v;
		if(v==fa) continue;
		dfs(v,u);
		f[u][0]=f[u][0]+f[v][1];
		f[u][1]=f[u][1]+min(f[v][1],f[v][0]);
	}
	if(u==x) f[x][bx^1]=1<<29;
	if(u==y) f[y][by^1]=1<<29;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof(head));
	cin>>n>>m>>s;
	for(int i=1;i<=n;i++)
		read(p[i]);
	for(int i=1;i<n;i++){
		int x,y;read(x),read(y);
		add_edge(x,y),add_edge(y,x);
		mp[make_pair(x,y)]=1,mp[make_pair(y,x)]=1;
	}
	if(m<=10000){
		for(int i=1;i<=m;i++){
			read(x),read(bx),read(y),read(by);
			if(!bx&&!by&&mp[make_pair(x,y)]){
				printf("-1\n");continue;
			}
			memset(f,0x1f,sizeof(f));
			dfs(1,-1);
			printf("%d\n",min(f[1][0],f[1][1]));
//			printf("%d %d\n",f[4][0],f[4][1]);
		}
	}
	else{
	}
	return 0;
}
