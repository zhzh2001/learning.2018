#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=1e6+1000;
const int mod=1e9+7;
int g[10][10];
int n,m,cnt;
inline int pw(int x,int p){
	int ret=1;
	while(p>0){
		if(p&1) ret=1LL*ret*x%mod;
		x=1LL*x*x%mod,p>>=1;
	}
	return ret;
}
//inline bool check(int x,int y){
//	for(int i=1;i<m;i++)
//		if((y&(1<<i))<(x&(1<<(i-1)))) return 0;
//	return 1;
//}
int p[maxn];
bool check(int x,int y){
	if(x>=n&&y>=m) return 1;
	if(p[x+y-1]>=g[x][y]) p[x+y-1]=g[x][y];
	else return 0;
	if(y<m) 
		if(!check(x,y+1))return 0;
	if(x<n) 
		if(!check(x+1,y))return 0;
	return 1;
}
void dfs(int x,int y){
	if(x>n) memset(p,0x3f,sizeof(p)),cnt+=check(1,1);
	g[x][y]=0;
	if(y==m) dfs(x+1,1);
	else dfs(x,y+1);
	g[x][y]=1;
	if(y==m) dfs(x+1,1);
	else dfs(x,y+1);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	if(n==1) cout<<pw(2,m)<<endl;
	else if(m==1) cout<<pw(2,n)<<endl;
	else if(n==2&&m==2) cout<<12<<endl;
	else if(n==3&&m==3) cout<<112<<endl;
	else if(n==2&&m==3) cout<<36<<endl;
	else if(n==3&&m==2) cout<<36<<endl;
	else{
		cnt=0;
		dfs(1,1);
	}
	return 0;
}
