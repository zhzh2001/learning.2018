#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iomanip>
#include<algorithm>
#define inf 0x3f3f3f3f
using namespace std;
const int maxn=5005;
int n,m,cnt,p=1,fto=0x3f3f3f3f,wto=0;
int last[maxn],vis[maxn],ans[maxn]={0,1};
struct Edge
{
  int to,nxt;
}a[maxn<<1];

void Add(int x,int y)
{
  a[++cnt].to=y;
  a[cnt].nxt=last[x];
  last[x]=cnt;
}

void dfs(int x,int fa)
{
  int minson;
  do
  {
    minson=inf;
  	for(int i=last[x];i;i=a[i].nxt)
    {
  	  int y=a[i].to;
  	  if(y==fa || vis[y]) continue;
  	  if(y<minson)
        minson=y;
    }
    if(minson!=inf)
    {
      vis[minson]=1;
      ans[++p]=minson;
      dfs(minson,x);	
	}
  }while(minson!=inf);
} 

void dfs2(int x,int f)
{
  for(int i=last[x];i;i=a[i].nxt)
  {
    int y=a[i].to;
    if(y==f || vis[y]) continue;
    if(y>wto) return;
	vis[y]=1;
    ans[++p]=y;
    dfs2(y,x);
  }
}

void dfs3(int x,int f)
{
  for(int i=last[x];i;i=a[i].nxt)
  {
  	int y=a[i].to;
  	if(y==f || vis[y]) continue;
  	vis[y]=1;
  	ans[++p]=y;
  	dfs3(y,x);
  }
}

int main()
{
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  int i,u,v;
  scanf("%d %d",&n,&m);
  for(i=1;i<=m;i++)
  {
  	scanf("%d %d",&u,&v);
  	Add(u,v);
  	Add(v,u);
  }
  if(m==n-1)
    dfs(1,0);
  if(m==n)
  {
  	for(i=last[1];i;i=a[i].nxt)
  	{
  	  if(a[i].to>wto) wto=a[i].to;
	  if(a[i].to<fto) fto=a[i].to;	
	}
	ans[++p]=fto;
  	dfs2(fto,1);
  	ans[++p]=wto;
  	dfs3(wto,1);
  }
  for(i=1;i<=n;i++)
    printf("%d ",ans[i]);
  printf("\n");
  fclose(stdin);
  fclose(stdout);
  return 0;
}
