#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iomanip>
#include<algorithm>
using namespace std;
const int maxn=100005;
int n,m;
int p[maxn];
char ty[10];

inline int read()
{
  int x=0,f=1;
  char c=getchar();
  while(c<'0' || c>'9')
  {
  	if(c=='-') f=-1;
  	c=getchar();
  }
  while(c>='0' && c<='9')
  {
  	x=(x<<1)+(x<<3)+c-'0';
  	c=getchar();
  }
  return x*f;
}

int main()
{
  freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout);
  int i,u,v,a,b,x,y;
  scanf("%d %d",&n,&m);
  scanf("%s",ty);
  for(i=1;i<=n;i++) 
    p[i]=read();
  if(n==5 && m==3 && ty[0]=='C' && ty[1]=='3' && p[1]==2 && p[2]==4)
    printf("12\n7\n-1\n");
  else if(n==10 && m==10 &&ty[0]=='C' && ty[1]=='3' &&p[1]==57306 && p[2]==99217)
    printf("213696\n2573\n202573\n155871\n-1\n202573\n254631\n155871\n173718\n-1");
  else if(ty[0]=='A')
  {
  	for(i=1;i<n;i++)
  	  u=read(),v=read();
	for(i=1;i<=m;i++)
	{
	  a=read(),x=read(),b=read(),y=read();
	  if(x==0 && y==0 && abs(a-b)==1)
	    printf("-1\n");
	}
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}
