#include <cstdio>
#include <cstring>
using namespace std;
const int P=1e9+7;
int n,m,nw,ans=1;
int res[10][10]={
	{2,4,8},{4,12,36},{8,36,112}
};
int ksm(int a,int b) {
	int s=1;
	for (;b;b>>=1,a=1ll*a*a%P) if (b&1) s=1ll*s*a%P;
	return s;
}
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n<=3 && m<=3) {
		printf("%d\n",res[n-1][m-1]);
	}
	else if (n==2) {
		printf("%d\n",4ll*ksm(3,m-1)%P);
	}
	else if (m==2) printf("%d\n",4ll*ksm(3,n-1)%P);
	else printf("%d\n",2312);
}
