#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;
const int N=5005;
int n,m,tp;
vector<int>E[N];
int res[N],ans[N],vis[N],st[N],huan[N];
bool d[N][N];
void dfs(int u,int fa) {
	res[++*res]=u;
	for (int i=0;i<E[u].size();i++) {
		int t=E[u][i];
		if (t==fa || d[u][t]==1) continue;
		dfs(t,u);
	}
}
void findH(int u,int fa) {
	st[++tp]=u;vis[u]=1;
	for (int i=0;i<E[u].size();i++) {
		int t=E[u][i];
		if (t==fa) continue;
		if (vis[t]) {
			if (*huan) continue;
			for (int i=tp;st[i]!=t && i;i--) {
				huan[++*huan]=st[i];
			}
			huan[++*huan]=t;
		}
		else findH(t,u);
	}
	tp--;
}
bool check() {
	if (ans[1]==0) return 1;
	for (int i=1;i<=n;i++) {
		if (res[i]<ans[i]) return 1;
		if (res[i]>ans[i]) return 0;
	}
	return 0;
}
void solve2() {
	findH(1,0);
	for (int i=1;i<=*huan;i++) {
		int u=huan[i],v=huan[i%*huan+1];
		d[u][v]=d[v][u]=1;
		res[0]=0;
		dfs(1,0);
		if (check()) for (int i=1;i<=n;i++) ans[i]=res[i];
		d[u][v]=d[v][u]=0;
	}
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,u,v;i<=m;i++) {
		scanf("%d%d",&u,&v);
		E[u].push_back(v);
		E[v].push_back(u);
	}
	for (int i=1;i<=n;i++) sort(E[i].begin(),E[i].end());
	if (m==n-1) {
		res[0]=0;
		dfs(1,0);
		for (int i=1;i<=n;i++) ans[i]=res[i];
	}
	else solve2();
	for (int i=1;i<n;i++) printf("%d ",ans[i]);
	printf("%d\n",ans[n]);
}
