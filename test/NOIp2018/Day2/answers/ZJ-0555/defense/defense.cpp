#include <cstdio>
#include <algorithm>
using namespace std;
#define ll long long
const int N=1e5+7;
struct edge {
	int t,nxt;
}e[N<<1];
int n,m,cnt,a,x,b,y;
int head[N],v[N];
ll f[N][2];
char s[100];
void add(int u,int t) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;
}
void dfs(int u,int fa) {
	f[u][0]=0;f[u][1]=v[u];
	if (u==a) f[u][x^1]=1e15;
	if (u==b) f[u][y^1]=1e15;
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==fa) continue;
		dfs(t,u);
		f[u][1]+=min(f[t][0],f[t][1]);
		f[u][0]+=f[t][1];
	}
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for (int i=1;i<=n;i++) scanf("%d",&v[i]);
	for (int i=1,u,v;i<n;i++) {
		scanf("%d%d",&u,&v);
		add(u,v);add(v,u);
	}
	if (s[1]=='1') {
		a=1,x=1;
		dfs(1,0);
		while (m--) printf("%d\n",f[1][1]);
		return 0;
	}
	
	for (int i=1,flg;i<=m;i++) {
		scanf("%d%d%d%d",&a,&x,&b,&y);
		flg=0;
		if (x==0 && y==0) {
			for (int j=head[a];j;j=e[j].nxt) if (e[j].t==b) {flg=1;break;}
		}
		if (flg) {puts("-1");continue;}
		dfs(1,0);
		printf("%lld\n",min(f[1][0],f[1][1]));
	}
}
