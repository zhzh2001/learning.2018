#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;

const int maxn=5005,maxe=10005;
int n,e,fa[maxn],X,Y;
int son[maxe],nxt[maxe],lnk[maxn],tot,ID[maxe];
inline void add(int x,int y,int id){
	son[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;ID[tot]=id;
}
inline int getfa(int x) {return fa[x]==x?x:fa[x]=getfa(fa[x]);}
int t[maxn],ans[maxn];
vector<int> V[maxn];
void gett(int x,int fa){
	t[++t[0]]=x;
	for (int i=0;i<V[x].size();i++)
	 if (V[x][i]!=fa){
	 	if (x==X&&V[x][i]==Y||x==Y&&V[x][i]==X) continue;
	 	gett(V[x][i],x);
	 }
}
int p[maxn];
void getpath(int x,int T,int fa){
	p[++p[0]]=x;
	if (x==T) return;
	for (int i=0;i<V[x].size();i++)
	 if (V[x][i]!=fa){
	 	if (x==X&&V[x][i]==Y||x==Y&&V[x][i]==X) continue;
	 	getpath(V[x][i],T,x);
	 	if (p[p[0]]==T) return;
	 }
	p[0]--;
}
bool zdx(int *a,int *b){
	for (int i=1;i<=n;i++)
	 if (a[i]!=b[i]) return a[i]<b[i];
	return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&e);tot=1;
	for (int i=1;i<=n;i++) fa[i]=i;
	X=0,Y=0;
	for (int i=1,x,y;i<=e;i++){
		scanf("%d%d",&x,&y);
		if (getfa(x)!=getfa(y)) fa[getfa(x)]=getfa(y);else X=x,Y=y;
		V[x].push_back(y);V[y].push_back(x);
	}
	for (int i=1;i<=n;i++) sort(V[i].begin(),V[i].end());
	if (e==n-1){
		t[0]=0;gett(1,1);
		for (int i=1;i<=n;i++) printf("%d ",t[i]);
		return 0;
	}else{
		getpath(X,Y,0);
		t[0]=0;gett(1,1);
		for (int i=1;i<=n;i++) ans[i]=t[i];
		for (int i=1;i<p[0];i++){
			X=p[i],Y=p[i+1];
			t[0]=0;gett(1,1);
			if (zdx(t,ans)) for (int i=1;i<=n;i++) ans[i]=t[i];
		}
		for (int i=1;i<=n;i++) printf("%d ",ans[i]);
		return 0;
	}
	return 0;
}
