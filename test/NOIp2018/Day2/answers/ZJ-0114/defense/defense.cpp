#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
inline char nc(){
	static char buf[100000],*l=buf,*r=buf;
	return l==r&&(r=(l=buf)+fread(buf,1,100000,stdin),l==r)?EOF:*l++;
}
inline int red(){
	int res=0,f=1;char ch=nc();
	while (ch<'0'||'9'<ch) {if (ch=='-') f=-f;ch=nc();}
	while ('0'<=ch&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}

const int maxn=100005,maxe=200005;
const ll INF=1e11;
int n,q,qx,a,qy,b;char ty[5];
ll p[maxn],f[maxn][2];
int son[maxe],nxt[maxe],lnk[maxn],tot;
inline void add(int x,int y){
	son[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;
}
void dfs(int x,int fa){
	f[x][0]=0; f[x][1]=p[x];
	for (int j=lnk[x];j;j=nxt[j])
	 if (son[j]!=fa){
	 	dfs(son[j],x);
	 	f[x][0]+=f[son[j]][1];
	 	f[x][1]+=min(f[son[j]][0],f[son[j]][1]);
	 }
	if (x==a) f[a][qx^1]=INF;
	if (x==b) f[b][qy^1]=INF;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&q,ty);
	for (int i=1;i<=n;i++) p[i]=red();
	for (int i=1,x,y;i<n;i++) x=red(),y=red(),add(x,y),add(y,x);
//	if (n<=2000){
		while (q--){
			a=red(),qx=red(),b=red(),qy=red();
			dfs(a,a);
			if (f[a][qx]>=INF) puts("-1");else printf("%lld\n",f[a][qx]);
		}
		return 0;
//	}
	return 0;
}
