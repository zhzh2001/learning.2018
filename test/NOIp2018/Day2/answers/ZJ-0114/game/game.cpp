#include<cstdio>
#include<cstring>

const int maxn=10;
#define ID(x,y) (((x)-1)*m+(y))
int n,m,a[maxn][maxn];
int X[maxn],Y[maxn],sx[maxn][maxn],sy[maxn][maxn],len;
char S[maxn],ss[maxn][maxn];
void dfs(int x,int y,int stp){
	X[stp]=x,Y[stp]=y;
	if (x==n&&y==m){
		len++;
		for (int i=1;i<=n+m-1;i++) sx[len][i]=X[i],sy[len][i]=Y[i];
		for (int i=1;i<=n+m-2;i++) ss[len][i]=S[i];
		return;
	}
	if (x<n) S[stp]='D',dfs(x+1,y,stp+1);
	if (y<m) S[stp]='R',dfs(x,y+1,stp+1);
}
bool check(int x,int y){
	for (int i=1;i<=n+m-1;i++)
	 if (a[sx[x][i]][sy[x][i]]!=a[sx[y][i]][sy[y][i]]) return a[sx[x][i]][sy[x][i]]<a[sx[y][i]][sy[y][i]];
	return 1;
}
const int MOD=1e9+7;
int f[1000000][4];
#define up(x,y) ((x+=y)%=MOD)
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n<=3&&m<=3){
//	if (0){
		dfs(1,1,1);
		int ans=0;
		for (int s=0;s<(1<<n*m);s++){
			for (int i=1;i<=n;i++)
			 for (int j=1;j<=m;j++) a[i][j]=(s>>ID(i,j)-1)&1;
			bool suc=1;
			for (int i=1;i<=len;i++)
			 for (int j=1;j<=len;j++)
			  if (strcmp(ss[i]+1,ss[j]+1)>0)
			   if (!check(i,j)) suc=0;
			if (suc) ans++;
		}
		printf("%d\n",ans);
	}else{
		f[1][0]=f[1][1]=f[1][2]=f[1][3]=1;
		for (int i=2;i<=m;i++){
			up(f[i][0],f[i-1][0]);
			up(f[i][1],f[i-1][0]);
			up(f[i][1],f[i-1][2]);
			up(f[i][0],f[i-1][1]);
			up(f[i][1],f[i-1][1]);
			up(f[i][1],f[i-1][3]);
			up(f[i][2],f[i-1][0]);
			up(f[i][3],f[i-1][0]);
			up(f[i][3],f[i-1][2]);
			up(f[i][2],f[i-1][1]);
			up(f[i][3],f[i-1][1]);
			up(f[i][3],f[i-1][3]);
		}
		int ans=0;
		for (int i=0;i<4;i++) up(ans,f[m][i]);
		printf("%d\n",ans);
	}
	return 0;
}
