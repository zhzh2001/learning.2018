#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const long long mo=1e9+7;
long long n,m,i,j,k,p,l,t;
inline void read(long long &x)
{
	char ch=getchar(); x=0; long long ff=1;
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') ff=-1,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	x*=ff;
}
long long pow(long long x,long long y)
{
	long long ans=1;
	while (y)
	{
		if (y%2==1) ans=(ans*x)%mo;
		y/=2;
		x=(x*x)%mo;
	}
	return ans;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n); read(m);
	long long bb=4;
	if (n==1&&m==1) printf("2\n");
	else printf("%lld\n",(bb*pow(3,n+m-3))%mo);
}
