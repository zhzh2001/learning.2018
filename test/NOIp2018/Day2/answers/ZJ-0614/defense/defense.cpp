#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const long long N=4005;
const long long inf=1e12+7;
long long n,i,j,k,l,t,m,xx,f[N][2],nxt[N*2],b[N*2],A,B,X,Y,first[N],len,p[N],fg,F[N],g[N][2];
inline void read(long long &x)
{
	char ch=getchar(); x=0; long long ff=1;
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') ff=-1,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	x*=ff;
}
long long min(long long x,long long y)
{
	if (x>y) return y;
	else return x;
}
void add(long long x,long long y){b[++len]=y; nxt[len]=first[x]; first[x]=len;}
void dp(long long x,long long fa)
{
	long long i;
	for (i=first[x];i;i=nxt[i])
	if (b[i]!=fa) F[b[i]]=x,dp(b[i],x);
	f[x][1]=p[x];
	for (i=first[x];i;i=nxt[i])
	if (b[i]!=fa) 
	{
		f[x][0]+=f[b[i]][1],f[x][1]+=min(f[b[i]][0],f[b[i]][1]);
		if ((b[i]==A&&x==B)||(b[i]==B&&x==A)) fg++;
	}
	if (A==x) {
		if (X==1) f[x][0]=inf;
		else f[x][1]=inf;
	} 
	if (B==x) {
		if (Y==1) f[x][0]=inf;
		else f[x][1]=inf;
	} 
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	long long xx;
	read(n); read(m); read(xx);
	for (i=1;i<=n;i++) read(p[i]);
	for (i=1;i<=n-1;i++)
	{
		long long x,y;
		read(x); read(y);
		add(x,y); add(y,x);
	}
	if (n<=2000)
	{
		for (i=1;i<=m;i++)
		{
			fg=0;
			read(A);  read(X); read(B); read(Y);
			if (X==0&&Y==0) fg=1;
			memset(f,0,sizeof(f));
			dp(1,0);
			if (fg==2) printf("-1\n");
			else printf("%lld\n",min(f[1][0],f[1][1]));
		}
	}
	else 
	{
		dp(1,0);	
		for (i=1;i<=n;i++)
		g[i][0]=f[i][0],g[i][1]=f[i][1];
		for (i=1;i<=m;i++)
		{
			read(A); read(X); read(B); read(Y);
			if (Y==0) f[B][1]=inf;
			else f[B][0]=inf;
			long long lst=B;
			for (long long j=F[B];j;j=F[j])
			f[j][0]+=f[lst][1]-g[lst][1],f[j][1]+=min(f[lst][0],f[lst][1])-min(g[lst][0],g[lst][1]),lst=j;
			printf("%lld\n",f[1][1]);
			for (long long j=B;j;j=F[j])
			f[j][0]=g[j][0],f[j][1]=g[j][1];
		}
	}
}
