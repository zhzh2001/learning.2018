#include<vector>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=10005;
vector<int>  s[N],s1[N],ton[N];
int n,i,j,k,p,l,t,m,nxt[N],first[N],b[N],tot,len,num[N],son[N],vis[N],tnum[N],F[N],Cnt[N],cir[N],st[N],Banx,Bany,Tot,op,r0[N],r1[N],cv;
inline void read(int &x)
{
	char ch=getchar(); x=0; int ff=1;
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') ff=-1,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	x*=ff;
}
void add(int x,int y){b[++len]=y; nxt[len]=first[x]; first[x]=len;}
void dfs0(int x,int fa)
{
	num[++tot]=x;
	int i,cnt=0;
	for (i=first[x];i;i=nxt[i])
	if (b[i]!=fa) son[++cnt]=b[i];
	sort(son+1,son+cnt+1);
	for (i=1;i<=cnt;i++) s[x].push_back(son[i]);
	for (i=0;i<s[x].size();i++)
		dfs0(s[x][i],x);
}
void circle(int x,int y)
{
	int i,top=0,tt1=0;
	for (i=x;i;i=F[i]) Cnt[i]++;
	for (i=y;i;i=F[i]) Cnt[i]++;
	for (i=x;i;i=F[i])
		if (Cnt[i]==2) {top=i; break;}
	for (i=x;i!=top;i=F[i]) cir[++Tot]=i;
	cir[++Tot]=top;
}
void dfs1(int x,int fa)
{
	int i;
	vis[x]=1;
	for (i=first[x];i;i=nxt[i])
	if (b[i]!=fa)
	{
		if (vis[b[i]]==0) F[b[i]]=x,dfs1(b[i],x);
		else if (op==0) op=1,circle(x,b[i]);
	} 
}
void dfs2(int x,int fa)
{
	int i;
	for (i=first[x];i;i=nxt[i])
	if (b[i]!=fa&&(x!=Banx||b[i]!=Bany)&&(x!=Bany||b[i]!=Banx)) r0[++cv]=x,r1[cv]=b[i],dfs2(b[i],x);
}
void dfs3(int x)
{
	num[++tot]=x;
	int i,tt=s[x].size();
	for (i=0;i<tt;i++) dfs3(s[x][i]);
}
bool judge()
{
	int i;
	for (i=1;i<=n;i++)
	if (num[i]<tnum[i]) return 1;
	else if (num[i]>tnum[i]) return 0;
	return 0;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n); read(m);
	for (i=1;i<=m;i++)
	{
		int x,y;
		read(x); read(y);
		add(x,y); add(y,x);
	}
	if (m==n-1) 
	{
		dfs0(1,0);
		for (i=1;i<=n;i++) tnum[i]=num[i];
	}
	else 
	{
		memset(vis,0,sizeof(vis));
		dfs1(1,0);
		//printf("%d\n",Tot);
		//for (i=1;i<=Tot;i++) printf("%d\n",cir[i]);
		//return 0;
		for (i=1;i<=Tot;i++)
		{
			memset(ton,0,sizeof(ton));
			for (j=1;j<=n;j++) s[j].clear();
			Banx=cir[i],Bany=cir[i+1];
			if (i==Tot) Bany=cir[1];
			tot=0; cv=0;
			//printf("%d %d TTT\n",Banx,Bany);
			dfs2(1,0);
			for (j=1;j<=cv;j++)
			ton[r1[j]].push_back(r0[j]);
			for (j=1;j<=n;j++)
			{
				int rr=ton[j].size();
				for (k=0;k<rr;k++)
				s[ton[j][k]].push_back(j);
			}
			/*for (j=1;j<=n;j++)
			{
				printf("%d | ",j);
				for (k=0;k<s[j].size();k++) printf("%d ",s[j][k]);
				printf("\n");
			}*/
			dfs3(1);
			if (i==1||judge())
			{
				for (j=1;j<=n;j++)
				tnum[j]=num[j];
			}
		}
	}
	for (i=1;i<=n;i++) printf("%d ",tnum[i]);
}
