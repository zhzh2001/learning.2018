#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
struct cqz{int x,y,k;}e[N];
inline bool operator<(cqz i,cqz j){
	return i.y>j.y;
}
int T,L=1,h[N],ne[N],to[N],id[N],v[N],n,m;
int ans[N],res[N],sm;
void addl(int x,int y,int k){
	ne[++L]=h[x];h[x]=L;to[L]=y;id[L]=k;
}
void dfs(int x){
	res[++sm]=x;
	for(int k=h[x];k;k=ne[k])
	if(v[id[k]]<T)v[id[k]]=T,dfs(to[k]);
}
void check(){
	if(sm<n)return;
	for(int i=1;i<=n;i++)
	if(ans[i]!=res[i]){
		if(ans[i]<res[i])return;
		else break;
	}for(int i=1;i<=n;i++)ans[i]=res[i];
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int i,x,y;
	n=read();m=read();
	for(i=1;i<=m;i++){
		x=read();y=read();
		e[i*2-1]=(cqz){x,y,i};
		e[i*2]=(cqz){y,x,i};
	}
	ans[1]=n;
	sort(e+1,e+m+m+1);
	for(i=1;i<=m+m;i++)addl(e[i].x,e[i].y,e[i].k);
	if(m==n-1)T++,sm=0,dfs(1),check();
	else{
		for(i=1;i<=m;i++)
			T++,v[i]=T,sm=0,dfs(1),check();
	}
	for(i=1;i<n;i++)printf("%d ",ans[i]);
	printf("%d\n",ans[n]);
	return 0;
}
