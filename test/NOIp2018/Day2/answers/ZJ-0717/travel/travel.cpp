#include<bits/stdc++.h>

using namespace std;

struct son{
	int ch[5001],cnt;
}s[5001];

int n,m;
int mp[5001][5001];
int vis[5001],fa[5001];

bool cmp(int x,int y){
	return x>y;
}

void dfs(int x){
	printf("%d ",x);
	vis[x]=1;
	for(int i=1;i<=s[x].cnt;i++){
		if(s[x].ch[i]!=fa[x])
			fa[s[x].ch[i]]=x;
	}
	for(int i=s[x].cnt;i;i--){
		if(s[x].ch[i]!=fa[x] && !vis[s[x].ch[i]])
			dfs(s[x].ch[i]);
	}
	return;
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d %d",&n,&m);
	int u,v;
	for(int i=1;i<=n;i++) s[i].cnt=0;
	for(int i=1;i<=m;i++){
		scanf("%d %d",&u,&v);
		mp[u][v]=mp[v][u]=1;
		s[u].ch[++s[u].cnt]=v;
		s[v].ch[++s[v].cnt]=u;
	}
	for(int i=1;i<=n;i++) sort(s[i].ch+1,s[i].ch+s[i].cnt+1,cmp);
	dfs(1);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
