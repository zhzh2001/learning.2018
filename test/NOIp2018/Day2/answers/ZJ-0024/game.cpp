#include <iostream>
#include <cstdio>
#include <cmath>
#include <queue>
#include <cstring>
#include <algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0; bool f=0; char ch=getchar();
	while (ch<'0' || ch>'9'){if (ch=='-') f=1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48; ch=getchar();}
	return (f)?-x:x;
}
const int N=200007;
const int P=1e9+7;
int n,m,num,ans,f[17][17];
int b[1007][N],c[1007][N],tr[N],t[N];
ll ksm(ll a,int k){
	ll res=1;
	while (k){
		if (k&1) res=res*a%P;
		a=a*a%P;
		k>>=1;
	}
	return res;
}
void dfs(int x,int y,int k){
	if (x==n&&y==m){
		++num;
		for (int i=1;i<=k;++i) b[num][i]=tr[i],c[num][i]=t[i];
		return;
	}
	if (y<m){tr[k]=1; t[k]=f[x][y+1]; dfs(x,y+1,k+1);}
	if (x<n){tr[k]=0; t[k]=f[x+1][y]; dfs(x+1,y,k+1);}
}
inline bool ET_b(int x,int y){
	for (int i=1;i<n+m-1;++i)
		if (b[x][i]<b[y][i]) return 0;
			else if (b[x][i]>b[y][i]) return 1;
	return 0;
}
inline bool ET_c(int x,int y){
	for (int i=1;i<n+m-1;++i)
		if (c[x][i]<c[y][i]) return 0;
			else if (c[x][i]>c[y][i]) return 1;
	return 0;
}
inline bool check(){
	for (int i=1;i<=num;++i)
	for (int j=1;j<=num;++j){
		if (i==j) continue;
		if (ET_b(i,j) && ET_c(i,j)) return false;
	}
	return true;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	n=read(); m=read();
	if (n>m) swap(n,m);
	if (n==1){
		long long ans=ksm(2,m);
		printf("%lld\n",ans);
		return 0;
	}
	if (n==2){
		long long ans=4;
		ans=ans*ksm(3,m-1)%P;
		printf("%lld\n",ans);
		return 0;
	}
	int len=n*m,nn=1<<len;
	for (int sta=0;sta<nn;++sta){
		int xx=1,yy=1;
		for (int i=0;i<len;++i){
			f[xx][yy]=sta&(1<<i);
			if (yy==m){++xx; yy=0;}
			++yy;
		}
		num=0;
		dfs(1,1,1);
		if (check()) ++ans;
	}
	printf("%d\n",ans);
	return 0;
}
