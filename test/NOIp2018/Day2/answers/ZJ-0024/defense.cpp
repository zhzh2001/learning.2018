#include <iostream>
#include <cstdio>
#include <cmath>
#include <queue>
#include <cstring>
#include <algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0; bool f=0; char ch=getchar();
	while (ch<'0' || ch>'9'){if (ch=='-') f=1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48; ch=getchar();}
	return (f)?-x:x;
}
const int N=100007;
const int Inf=0x3f3f3f3f;
const long long oo=1LL<<36;
struct JOKER{int b,nxt;}e[N<<1];
int n,cas,tot,A,ax,B,bx,head[N],a[N];
int uu[N],vv[N],tag[N];
ll f[N][2];
char ch[7];
bool p[N];
inline void add(int x,int y){
	e[++tot].b=y;
	e[tot].nxt=head[x];
	head[x]=tot;
}
inline bool checkq(){
	if (ax!=0 || bx!=0) return false;
	for (int i=head[A];i;i=e[i].nxt)
		if (e[i].b==B) return true;
	return false;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	
	n=read(); cas=read();
	scanf("%s",ch+1);
	for (int i=1;i<=n;++i) a[i]=read();
	for (int i=1;i<n;++i){
		uu[i]=read(); vv[i]=read();
		add(uu[i],vv[i]); add(vv[i],uu[i]);
	}
	if (n<=10&&cas<=10){
		int nn=1<<n;
		while (cas--){
			A=read(),ax=read(),B=read(),bx=read();
			int ans=Inf;
			if (checkq()){ans=-1; printf("%d\n",ans); continue;}
			for (int sta=0;sta<nn;++sta){
				int res=0;
				for (int i=1;i<=n;++i) p[i]=0;
				for (int i=1;i<=n;++i)
					if ((1<<(i-1))&sta){p[i]=1; res+=a[i];}
				if (p[A]!=ax || p[B]!=bx) continue;
				bool flag=1;
				for (int i=1;i<n;++i)
					if (p[uu[i]]==0 && p[vv[i]]==0){flag=0; break;}
				if (flag) ans=min(ans,res);
			}
			if (ans==Inf) ans=-1;
			printf("%d\n",ans);
		}
		return 0;
	}
	if (ch[0]==A){
		while (cas--){
			for (int i=1;i<=n;++i) f[i][0]=f[i][1]=oo;
			f[n+1][1]=f[n+1][0]=0;
			A=read(),ax=read(),B=read(),bx=read();
			if (checkq()){int ans=-1; printf("%d\n",ans); continue;}
			tag[A]=(ax==1)?2:1; tag[B]=(bx==1)?2:1;
			for (int i=n;i>0;--i){
				if (tag[i]==2){
					f[i][0]=oo;
					f[i][1]=min(f[i+1][0],f[i+1][1])+a[i];
					continue;
				}
				if (tag[i]==1){
					f[i][1]=oo;
					f[i][0]=f[i+1][1];
					continue;
				}
				f[i][0]=f[i+1][1];
				f[i][1]=min(f[i+1][0],f[i+1][1])+a[i];
			}
			ll ans=min(f[1][0],f[1][1]);
			if (ans==oo) ans=-1;
			printf("%lld\n",ans);
			tag[A]=tag[B]=0;
		}
		return 0;
	}
	return 0;
}
