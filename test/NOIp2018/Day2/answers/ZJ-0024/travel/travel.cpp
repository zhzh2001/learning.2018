#include <iostream>
#include <cstdio>
#include <cmath>
#include <queue>
#include <cstring>
#include <algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0; bool f=0; char ch=getchar();
	while (ch<'0' || ch>'9'){if (ch=='-') f=1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48; ch=getchar();}
	return (f)?-x:x;
}
const int N=5007;
int n,m,num,rnk[N],t[N],a[N][N];
int len,top,tagx,tagy,q[N],b[N],ans[N];
bool vis[N],inq[N];
bool cmp(int a,int b){return a<b;}
void find(int u,int fa){
	vis[u]=inq[u]=1; q[++top]=u;
	for (int i=1;i<=a[u][0];++i){
		int v=a[u][i];
		if (v==fa) continue;
		if (!vis[v]) find(v,u);
		else{
			if (inq[v]){
				int now=0;
				while (now!=v){
					now=q[top--];
					b[++len]=now;
					inq[now]=0;
				}
			}
		}
	}
	if (q[top]==u) --top; inq[u]=0;
}
inline bool check(int x,int y){return (x==tagx && y==tagy);}
void dfs(int u){
	vis[u]=1; rnk[++num]=u;
	for (int i=1;i<=a[u][0];++i){
		int v=a[u][i];
		if (vis[v] || check(u,v) || check(v,u)) continue;
		dfs(v);
	}
}
inline bool ET(){
	for (int i=1;i<=n;++i)
		if (ans[i]<rnk[i]) return false;
			else if (ans[i]>rnk[i]) return true;
	return false;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	n=read(); m=read();
	for (int i=1;i<=m;++i){
		int x=read(),y=read();
		a[x][++a[x][0]]=y;
		a[y][++a[y][0]]=x;	
	}
	for (int i=1;i<=n;++i){
		for (int j=1;j<=a[i][0];++j) t[j]=a[i][j];
		sort(t+1,t+a[i][0]+1,cmp);
		for (int j=1;j<=a[i][0];++j) a[i][j]=t[j];
	}
	
	if (m==n-1){
		tagx=tagy=0;
		dfs(1);
		for (int i=1;i<=n;++i){
			printf("%d",rnk[i]);
			(i==n)?printf("\n"):printf(" ");
		}
		return 0;
	}
	find(1,0); b[len+1]=b[1];
	for (int i=1;i<=n;++i) ans[i]=n+1; 
	for (int i=1;i<=len;++i){
		tagx=b[i]; tagy=b[i+1];
		num=0;
		memset(vis,0,sizeof(vis));
		dfs(1);
		if (ET())
			for (int i=1;i<=n;++i) ans[i]=rnk[i];
	}
	for (int i=1;i<=n;++i){
		printf("%d",ans[i]);
		(i==n)?printf("\n"):printf(" ");
	}
	return 0;
}
