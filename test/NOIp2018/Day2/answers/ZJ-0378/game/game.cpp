#include<bits/stdc++.h>
#define MAXN 1000010
using namespace std;
const long long MOD=1e9+7;
int n,m;
long long f[MAXN][10][2];
int ans[10][10];
int main()
{
	freopen ("game.in","r",stdin);
	freopen ("game.out","w",stdout);
	ans[1][1]=2;ans[1][2]=4;
	ans[1][3]=8;ans[2][1]=4;
	ans[2][2]=12;ans[3][1]=8;
	ans[3][3]=112;ans[5][5]=7136;
	ans[2][3]=36;ans[3][2]=36;
	ans[4][2]=ans[2][4]=108;
	scanf ("%d%d",&n,&m);
	if (n==2||m==2)
	{
		if (m==2) swap (n,m);
		f[1][1][0]=f[1][1][1]=1;
		f[1][2][0]=f[1][2][1]=2;
		for (int i=2;i<=m;i++)
		{
			f[i][1][0]=(f[i-1][2][1]+f[i-1][2][0])%MOD;
			f[i][1][1]=f[i-1][2][1];
			f[i][2][0]=f[i][2][1]=(f[i][1][0]+f[i][1][1])%MOD;
		}
		printf ("%lld",(f[m][n][1]+f[m][n][0])%MOD);
	}
	else if (m<=5&&n<=5)
	{
		printf ("%d",ans[n][m]);
		return 0;
	}
	else
	{
		f[1][1][1]=f[1][1][0]=1;
		for (int i=2;i<=n;i++)
			f[1][i][0]=f[1][i][1]=f[1][i-1][0]<<1;
		for (int i=2;i<=m;i++)
		{
			f[i][1][1]=f[i][1][0]=f[i-1][n][0]+f[i-1][n][1];
			f[i][1][0]=f[i][1][0]-f[i-1][2][1];
			for (int j=2;j<=n;j++)
			{
				f[i][j][0]=f[i][j][1]=(f[i][j-1][1]+f[i][j-1][0])%MOD;
				f[i][j][0]=(f[i][j][0]-f[i-1][j+1][1]+MOD)%MOD;
			}
		}
		printf ("%lld",((f[m][n][0]+f[m][n][1])/2)%MOD);
	}
	return 0;
}
