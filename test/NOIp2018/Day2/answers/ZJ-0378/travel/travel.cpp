#include<bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 10010
using namespace std;
int n,m,cnt;
int e[MAXN][MAXN],p[MAXN];
int ans[MAXN],pp,flag;
bool used[MAXN];
int In[MAXN];
queue<int>q;
void DFS (int u,int minn)
{
	used[u]=1;
	ans[++pp]=u;
	for (int i=1;i<=p[u];i++)
		if (!used[e[u][i]])
		{
			if (In[u]==1||flag==-1) DFS (e[u][i],INF);
			else if (flag==0)
			{
				int pos=i+1;
				while (used[e[i][pos]]) pos++;
				flag=1;
				DFS (e[u][i],e[u][pos]);
			}
			else
			{
				if ((In[e[u][i]]!=1&&e[u][i]<minn)||In[e[u][i]]==1)
					DFS (e[u][i],In[e[u][i]]==1?INF:minn);
				else
				{
					flag=-1;
					for (int j=i+1;j<=p[u];j++)
						if (!used[e[u][j]])
							DFS (e[u][j],INF);
					return;
				}
			}
		}
}
void topu ()
{
	for (int i=1;i<=n;i++)
		if (In[i]==1)
			q.push (i);
	while (!q.empty ())
	{
		int u=q.front ();q.pop ();
		for (int i=1;i<=p[u];i++)
			if (In[e[u][i]]!=1)
			{
				In[e[u][i]]--;
				if (In[e[u][i]]==1) q.push (e[u][i]);
			}
	}
}
int main()
{
	freopen ("travel.in","r",stdin);
	freopen ("travel.out","w",stdout);
	scanf ("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		int u,v;
		scanf ("%d%d",&u,&v);
		e[u][++p[u]]=v;In[v]++;
		e[v][++p[v]]=u;In[u]++;
	}
	for (int i=1;i<=n;i++)
		if (p[i]!=0)
			sort (e[i]+1,e[i]+p[i]+1);
	topu ();
	DFS (1,INF);
	for (int i=1;i<=pp;i++)
		printf ("%d ",ans[i]);
	return 0;
}
