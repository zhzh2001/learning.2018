#include<bits/stdc++.h>
#define MAXN 100010
#define INF 0x3f3f3f3f
using namespace std;
struct edge{
	int v,nxt;
}e[MAXN];
int n,m,cnt,a,x,b,y;;
int p[MAXN],head[MAXN],f[MAXN][2];
char opt[10];
void add (int u,int v)
{
	e[++cnt].v=v;
	e[cnt].nxt=head[u];
	head[u]=cnt;
}
void dfs (int u,int fa)
{
	for (int i=head[u];i!=0;i=e[i].nxt)
		if (e[i].v!=fa)
		{
			dfs (e[i].v,u);
			if (f[u][1]!=INF) f[u][1]+=min (f[e[i].v][1],f[e[i].v][0]);
			if (f[u][0]!=INF) f[u][0]+=f[e[i].v][1];
		}
	if (f[u][1]!=INF) f[u][1]+=p[u];
}
int main()
{
	freopen ("defense.in","r",stdin);
	freopen ("defense.out","w",stdout);
	scanf ("%d%d",&n,&m);
	scanf ("%s",opt+1);
	for (int i=1;i<=n;i++)
		scanf ("%d",&p[i]);
	for (int i=1;i<n;i++)
	{
		int u,v;
		scanf ("%d%d",&u,&v);
		add (u,v);add (v,u);
	}
	while (m--)
	{
		memset (f,0,sizeof (f));
		scanf ("%d%d%d%d",&a,&x,&b,&y);
		f[a][x^1]=f[b][y^1]=INF;
		dfs (1,0);
		int ans=min (f[1][0],f[1][1]);
		if (ans>=INF) printf ("-1\n");
		else printf ("%d\n",ans);
	}
	return 0;
}
