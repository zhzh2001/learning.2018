#include<bits/stdc++.h>
#define FOR(x,y,z) for(int x=y,x##_=z;x<=x##_;x++)
#define DOR(x,y,z) for(int x=y,x##_=z;x>=x##_;x--)
#define FOR_edge(x,y) for(int E_=head[x],y=edge[E_].e;E_;E_=edge[E_].to,y=edge[E_].e)
#define read2(x,y) read(x),read(y)
#define read3(x,y,z) read2(x,y),read(z)
#define read4(x,y,z,w) read3(x,y,z),read(w)
#define pf printf
#define sf scanf
#define szf(x) sizeof(x)
#define clr(x,y) memset(x,y,szf(x))
#define ll long long
#define db double
#define ct clock_t
#define ck() clock()
#define File_IO(_) freopen(#_".in","r",stdin),freopen(#_".out","w",stdout)
#define File_I(_) freopen(#_".in","r",stdin)
#define File_O(_) freopen(#_".out","w",stdout)
using namespace std;
bool mem1;
template<class T>void tomin(T &x,T y){if(x>y)x=y;}
template<class T>void tomax(T &x,T y){if(x<y)x=y;}
template<class T>void read(T &x){
	char c;
	int f=1;
	x=0;
	while(c=getchar(),c<'0'||c>'9')if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^48);
	while(c=getchar(),c>='0'&&c<='9');
	x*=f;
}
const int maxn=1e5+5;
int n,m;
int P[maxn];
char Typ[5];
struct Edge{
	int e,to;
}edge[maxn<<1];
int head[maxn],tot;
void Add(int x,int y){
	edge[++tot]=(Edge){y,head[x]};
	head[x]=tot;
}
namespace P44{
	ll dp[maxn][2];
	int x[3],y[3];
	const ll Inf=1e18;
	bool dfs(int u,int f){
		dp[u][0]=0;
		dp[u][1]=P[u];
		FOR_edge(u,v){
			if(v==f)continue;
			if(dfs(v,u))return 1;
			dp[u][0]+=dp[v][1];
			dp[u][1]+=min(dp[v][0],dp[v][1]);
		}
		FOR(i,0,1)if(u==x[i]){
			dp[u][!y[i]]=Inf;
			break;
		}
		return dp[u][0]>=Inf&&dp[u][1]>=Inf;
	}
	void solve(){
		while(m--){
			read4(x[0],y[0],x[1],y[1]);
			if(dfs(1,0))puts("-1");
			else pf("%lld\n",min(dp[1][0],dp[1][1]));
		}
	}
}
bool mem2;
// long long
int main(){//defense
//	system("fc defense2.ans defense.out");
//	File_I(defense3);
//	File_O(defense);
//	pf("%lf\n",(&mem2-&mem1)/1024.0/1024);
	File_IO(defense);
	sf("%d%d%s",&n,&m,Typ);
	FOR(i,1,n)read(P[i]);
	int x,y;
	FOR(i,2,n){
		read2(x,y);
		Add(x,y);
		Add(y,x);
	}
	P44::solve();
	return 0;
}
