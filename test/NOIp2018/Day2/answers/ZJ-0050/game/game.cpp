#include<bits/stdc++.h>
#define FOR(x,y,z) for(int x=y,x##_=z;x<=x##_;x++)
#define DOR(x,y,z) for(int x=y,x##_=z;x>=x##_;x--)
#define FOR_edge(x,y) for(int E_=head[x],y=edge[E_].e;E_;E_=edge[E_].to,y=edge[E_].e)
#define FOR__edge(x,y,z) for(int E_=head[x],y=edge[E_].e,z=edge[E_].c;E_;E_=edge[E_].to,y=edge[E_].e,z=edge[E_].c)
#define read2(x,y) read(x),read(y)
#define read3(x,y,z) read2(x,y),read(z)
#define pf printf
#define sf scanf
#define szf(x) sizeof(x)
#define clr(x,y) memset(x,y,szf(x))
#define ll long long
#define db double
#define ct clock_t
#define ck() clock()
#define File_IO(_) freopen(#_".in","r",stdin),freopen(#_".out","w",stdout)
#define File_I(_) freopen(#_".in","r",stdin)
#define File_O(_) freopen(#_".out","w",stdout)
using namespace std;
bool mem1;
template<class T>void tomin(T &x,T y){if(x>y)x=y;}
template<class T>void tomax(T &x,T y){if(x<y)x=y;}
template<class T>void read(T &x){
	char c;
	int f=1;
	x=0;
	while(c=getchar(),c<'0'||c>'9')if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^48);
	while(c=getchar(),c>='0'&&c<='9');
	x*=f;
}
const int maxn=1e6+5;
const int P=1e9+7;
int n,m;
void Check(int &x,int y){
	if(y>=P)y-=P;
	x=y;
}
namespace P1{
	void solve(){
		ll ans=1;
		FOR(i,1,m)ans=ans*2%P;
		pf("%lld",ans);
	}
}
namespace P2{
	void solve(){
		ll ans=12;
		FOR(i,3,m)ans=ans*3%P;
		pf("%lld",ans);
	}
}
namespace P3{
	void solve(){
		ll ans=112;
		FOR(i,4,m)ans=ans*3%P;
		pf("%lld",ans);
	}
}
namespace P4{
	void solve(){
		if(m==4)pf("%d",228*4);
		else if(m==5)pf("%d",672*4);
		else{
			ll ans=672*4;
			FOR(i,6,m)ans=ans*3%P;
			pf("%lld",ans);
		}
	}
}
namespace P100{
	int f[]={0,0,0,0,0,1784};
	void solve(){
		if(m==n)pf("%d",f[m]*4);
		else if(m==n+1)pf("%d",(f[n]*3-(n-3)*12)*4);
		else{
			ll ans=f[n]*3-(n-3)*12;
			FOR(i,n+2,m)ans=ans*3%P;
			pf("%lld",ans*4%P);
		}
	}
}
bool mem2;
int main(){//game
//	system("fc game3.ans game.out");
//	File_I(game4);
//	File_O(game);
//	pf("%lf\n",(&mem2-&mem1)/1024.0/1024);
	File_IO(game);
	read2(n,m);
	if(n>m)swap(n,m);
	if(n==1)P1::solve();
	else if(n==2)P2::solve();
	else if(n==3)P3::solve();
	else if(n==4)P4::solve();
	else P100::solve();
	return 0;
}
