#include<bits/stdc++.h>
#define FOR(x,y,z) for(int x=y,x##_=z;x<=x##_;x++)
#define DOR(x,y,z) for(int x=y,x##_=z;x>=x##_;x--)
#define FOR_edge(x,y) for(int E_=head[x],y=edge[E_].e;E_;E_=edge[E_].to,y=edge[E_].e)
#define FOR__edge(x,y,z) for(int E_=head[x],y=edge[E_].e,z=edge[E_].c;E_;E_=edge[E_].to,y=edge[E_].e,z=edge[E_].c)
#define read2(x,y) read(x),read(y)
#define read3(x,y,z) read2(x,y),read(z)
#define pf printf
#define sf scanf
#define szf(x) sizeof(x)
#define clr(x,y) memset(x,y,szf(x))
#define ll long long
#define db double
#define ct clock_t
#define ck() clock()
#define File_IO(_) freopen(#_".in","r",stdin),freopen(#_".out","w",stdout)
#define File_I(_) freopen(#_".in","r",stdin)
#define File_O(_) freopen(#_".out","w",stdout)
using namespace std;
bool mem1;
template<class T>void tomin(T &x,T y){if(x>y)x=y;}
template<class T>void tomax(T &x,T y){if(x<y)x=y;}
template<class T>void read(T &x){
	char c;
	int f=1;
	x=0;
	while(c=getchar(),c<'0'||c>'9')if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^48);
	while(c=getchar(),c>='0'&&c<='9');
	x*=f;
}
const int maxn=5005;
int n,m;
vector<int>edge[maxn];
namespace P0{
	int ans[maxn],res[maxn];
	int f[maxn];
	bool mark[maxn];
	void dfs(int u,int x){
		if(x==n){
			FOR(i,1,n){
				if(ans[i]>res[i]){
					FOR(j,i,n)ans[j]=res[j];
					break;
				}
				if(ans[i]<res[i])break;
			}
			return;
		}
		FOR(i,0,(int)edge[u].size()-1){
			int v=edge[u][i];
			if(mark[v])continue;
			f[v]=u;
			mark[v]=1;
			res[x+1]=v;
			dfs(v,x+1);
			mark[v]=0;
		}
		if(f[u])dfs(f[u],x);
	}
	void solve(){
		int x,y;
		FOR(i,1,n)edge[i].clear();
		FOR(i,1,m){
			read2(x,y);
			edge[x].push_back(y);
			edge[y].push_back(x);
		}
		FOR(i,1,n)sort(edge[i].begin(),edge[i].end());
		clr(ans,67);
		mark[1]=1;
		res[1]=1;
		dfs(1,1);
		FOR(i,1,n){
			pf("%d",ans[i]);
			if(i<n)pf(" ");
		}
	}
}
namespace P60{
	int tot;
	void dfs(int u,int f){
		pf("%d",u);
		if((++tot)<n)pf(" ");
		FOR(i,0,(int)edge[u].size()-1){
			int v=edge[u][i];
			if(v==f)continue;
			dfs(v,u);
		}
	}
	void solve(){
		int x,y;
		FOR(i,1,n)edge[i].clear();
		FOR(i,1,m){
			read2(x,y);
			edge[x].push_back(y);
			edge[y].push_back(x);
		}
		FOR(i,1,n)sort(edge[i].begin(),edge[i].end());
		tot=0;
		dfs(1,0);
	}
}
namespace P100{
	int res[maxn],ans[maxn];
	int eg[2];
	int fa[maxn];
	bool flag;
	bool dfs(int u,int f){
		res[++res[0]]=u;
		if(flag&&u>ans[res[0]])return 1;
		if(u<ans[res[0]])flag=0;
		FOR(i,0,(int)edge[u].size()-1){
			int v=edge[u][i];
			if(v==f)continue;
			if(u==eg[0]&&v==eg[1])continue;
			if(u==eg[1]&&v==eg[0])continue;
			if(dfs(v,u))return 1;
		}
		return 0;
	}
	void redfs(int u,int f){
		fa[u]=f;
		FOR(i,0,(int)edge[u].size()-1){
			int v=edge[u][i];
			if(v==f)continue;
			if(u==eg[0]&&v==eg[1])continue;
			if(u==eg[1]&&v==eg[0])continue;
			redfs(v,u);
		}
	}
	int getfa(int x){
		return x==fa[x]?x:fa[x]=getfa(fa[x]);
	}
	void Calc(int x,int y){
		flag=1;
		eg[0]=x,eg[1]=y,res[0]=0;
		if(!dfs(1,0))
			if(res[0]==n)
				memcpy(ans,res,szf(res));
	}
	void solve(){
		int x,y;
		FOR(i,1,n)edge[i].clear();
		FOR(i,1,n)fa[i]=i;
		FOR(i,1,m){
			read2(x,y);
			int fx=getfa(x),fy=getfa(y);
			if(fx==fy)eg[0]=x,eg[1]=y;
			else fa[fx]=fy;
			edge[x].push_back(y);
			edge[y].push_back(x);
		}
		FOR(i,1,n)sort(edge[i].begin(),edge[i].end());
		redfs(eg[0],0);
		int s=eg[0],e=eg[1];
		clr(ans,67);
		Calc(s,e);
		do{
			Calc(e,fa[e]);
			e=fa[e];
		}while(s!=e);
		FOR(i,1,n){
			pf("%d",ans[i]);
			if(i<n)pf(" ");
		}
	}
}
bool mem2;
int main(){//travel
//	system("fc travel4.ans travel.out");
//	File_I(travel4);
//	File_O(travel);
//	pf("%lf\n",(&mem2-&mem1)/1024.0/1024);
	File_IO(travel);
	read2(n,m);
//	P0::solve();
	if(m==n-1)P60::solve();
	else if(n<=10)P0::solve();
	else P100::solve();
	return 0;
}
