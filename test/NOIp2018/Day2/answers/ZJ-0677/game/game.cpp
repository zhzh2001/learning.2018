#include<bits/stdc++.h>
#define FOR(i,a,b) for(int i=(a),i##_end_=(b);i<=i##_end_;i++)
#define DOR(i,a,b) for(int i=(a),i##_end_=(b);i>=i##_end_;i--)
#define ll long long
#define M 1000005
#define P 1000000007
using namespace std;

int n,m;

struct P20{

	int ans;
	int A[10][10];

	struct node{
		string a,b;
	
		node(){
			a="";
			b="";
		}
		
	}Tmp[M];

	int cnt;

	void Get(int x,int y,node a){
		if(x>n||y>m)return;
		a.b+=A[x][y];
		if(x==n&&y==m){
			Tmp[++cnt]=a;
			return;
		}
		node t=a;
		t.a+='D';
		Get(x+1,y,t);
		t=a;
		t.a+='R';
		Get(x,y+1,t);
	}

	bool check(){
		node t;
		cnt=0;
		t.a=t.b="";
		Get(1,1,t);
		int flag=1;
		FOR(i,1,cnt){
			if(flag==0)break;
			FOR(j,i+1,cnt){
				if(flag==0)break;
				if(Tmp[i].a>Tmp[j].a){
					if(Tmp[i].b>Tmp[j].b)flag=0;
				}
				else {
					if(Tmp[i].b<Tmp[j].b)flag=0;
				}
			}
		}
		return flag;
	}

	void dfs(int x,int y){
		if(y==m+1)x++,y=1;
		if(x==n+1){
			if(check()){
				ans++;
			}
			return;
		}
		A[x][y]=0;
		dfs(x,y+1);
		A[x][y]=1;
		dfs(x,y+1);
	}

	void solve(){
		ans=0;
		dfs(1,1);
		printf("%d\n",ans);
	}

}p20;

struct P50{

	void solve(){
		ll ans=4;
		FOR(i,1,m-1)ans=ans*3%P;
		printf("%lld\n",ans);
	}

}p50;

struct P65{

	ll Ans[M];	
	
	void solve(){
		Ans[1]=8;
		Ans[2]=36;
		Ans[3]=112;
		FOR(i,4,m)Ans[i]=Ans[i-1]*3%P;
		printf("%lld\n",Ans[m]);
	}

}p65;

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n<=3&&m<=3)p20.solve();
	else if(n==2)p50.solve();
	else if(n==3)p65.solve();

	return 0;
}
