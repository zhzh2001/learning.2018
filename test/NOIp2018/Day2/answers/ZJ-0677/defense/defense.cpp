#include<bits/stdc++.h>
#define FOR(i,a,b) for(int i=(a),i##_end_=(b);i<=i##_end_;i++)
#define DOR(i,a,b) for(int i=(a),i##_end_=(b);i>=i##_end_;i--)
#define ll long long
#define M 100005
#define N 2005
#define INF 1e13
using namespace std;

template<class _>void chkmin(_ &a,_ b){if(a>b)a=b;}
template<class _>void chkmax(_ &a,_ b){if(a<b)a=b;}

int n,m;
ll A[M];
char s[5];

vector<int>G[M];

struct P50{

	ll dp[N][2];//0 פ�� 1 ��פ��
	int X,f1,Y,f2;

	void dfs(int x,int f){
		int flag=0;
		ll res1=0;
		ll res2=0;
		for(int i=0;i<(int)G[x].size();i++){
			int y=G[x][i];
			if(y==f)continue;
			flag=1;
			dfs(y,x);
			res1+=min(dp[y][0],dp[y][1]);
			res2+=dp[y][0];
		}
		if(!flag){
			dp[x][0]=A[x];
			dp[x][1]=0;
		}
		else {
			dp[x][0]=res1+A[x];
			dp[x][1]=res2;
		}
		if(x==X){
			if(f1==0)dp[x][0]=INF;
			else dp[x][1]=INF;
		}
		if(x==Y){
			if(f2==0)dp[x][0]=INF;
			else dp[x][1]=INF;
		}
	}

	void solve(){
		FOR(i,1,m){
			scanf("%d%d%d%d",&X,&f1,&Y,&f2);
			FOR(j,1,n){
				dp[j][0]=dp[j][1]=INF;
			}
			dfs(1,0);
			ll ans=min(dp[1][0],dp[1][1]);
			if(ans>=INF)puts("-1");
			else printf("%lld\n",ans);
		}
	}

}p50;

struct Segment_Tree{

	struct node{
		int L,R;
		ll Min[2][2];
	}tree[M<<2];

	void up(int p){
		FOR(L,0,1){
			FOR(R,0,1){
				tree[p].Min[L][R]=tree[p<<1].Min[L][0]+tree[p<<1|1].Min[1][R];
				chkmin(tree[p].Min[L][R],tree[p<<1].Min[L][1]+tree[p<<1|1].Min[0][R]);
				chkmin(tree[p].Min[L][R],tree[p<<1].Min[L][1]+tree[p<<1|1].Min[1][R]);
				if(tree[p].Min[L][R]>=INF)tree[p].Min[L][R]=INF;
			}
		}
	}

	void build(int L,int R,int p){
		tree[p].L=L;
		tree[p].R=R;
		FOR(i,0,1){
			FOR(j,0,1){
				tree[p].Min[i][j]=INF;
			}
		}
		if(L==R){
			tree[p].Min[0][0]=0;
			tree[p].Min[1][1]=A[L];
			return;
		}
		int mid=(L+R)>>1;
		build(L,mid,p<<1);
		build(mid+1,R,p<<1|1);
		up(p);
	}

	void update(int x,int f,ll v,int p){
		if(tree[p].L==tree[p].R){
			tree[p].Min[f][f]=v;
			return;
		}
		int mid=(tree[p].L+tree[p].R)>>1;
		if(x<=mid)update(x,f,v,p<<1);
		else update(x,f,v,p<<1|1);
		up(p);
	}

}T;

struct P70{

	int X,f1,Y,f2;	
	ll Tmp[M][2];

	void solve(){
		FOR(i,1,n)Tmp[i][1]=A[i];
		T.build(1,n,1);
		FOR(i,1,m){
			scanf("%d%d%d%d",&X,&f1,&Y,&f2);
			T.update(X,f1^1,INF,1);
			T.update(Y,f2^1,INF,1);
			ll ans=INF;			
			FOR(l,0,1){
				FOR(r,0,1){
					chkmin(ans,T.tree[1].Min[l][r]);
				}
			}
			T.update(X,f1^1,Tmp[X][f1^1],1);
			T.update(Y,f2^1,Tmp[Y][f2^1],1);
			if(ans>=INF)puts("-1");
			else printf("%lld\n",ans);
		}
	
	}

}p70;

struct PSHUI{

	int Fa[M];
	ll dp[M][2];//0 פ�� 1 ��פ��
	ll res1[M];
	ll res2[M];
	ll Tmp[M][2];
	int X,f1,Y,f2;

	void dfs(int x,int f){
		int flag=0;
		Fa[x]=f;
		for(int i=0;i<(int)G[x].size();i++){
			int y=G[x][i];
			if(y==f)continue;
			flag=1;
			dfs(y,x);
			res1[x]+=min(dp[y][0],dp[y][1]);
			res2[x]+=dp[y][0];
		}
		if(!flag){
			dp[x][0]=A[x];
			dp[x][1]=0;
		}
		else {
			dp[x][0]=res1[x]+A[x];
			dp[x][1]=res2[x];
		}
	}

	void Change(int x,int f){
		if(f==1){
			Tmp[x][0]=dp[x][0];
			Tmp[x][1]=dp[x][1];
		}
		else {
			dp[x][0]=Tmp[x][0];
			dp[x][1]=Tmp[x][1];
		}
		if(x==1)return;
		Change(Fa[x],f);
	}

	void calc(int x,int f){
		dp[x][0]=res1[x]+A[x]-min(Tmp[f][0],Tmp[f][1])+min(dp[f][0],dp[f][1]);
		dp[x][1]=res2[x]-Tmp[f][0]+dp[f][0];
		if(x==1)return;
		calc(Fa[x],x);
	}

	void solve(){
		dfs(1,0);
		FOR(i,1,m){
			scanf("%d%d%d%d",&X,&f1,&Y,&f2);
			Change(Y,1);
			if(f2==1)dp[Y][1]=INF;
			else dp[Y][0]=INF;
			calc(Fa[Y],Y);
			if(dp[1][0]>=INF)puts("-1");
			else printf("%lld\n",dp[1][0]);
			Change(Y,0);
		}
	}	

}pshui;

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s+1);
	FOR(i,1,n)scanf("%lld",&A[i]);
	FOR(i,2,n){
		int a,b;
		scanf("%d%d",&a,&b);
		G[a].push_back(b);
		G[b].push_back(a);
	}
	if(n<=2000&&m<=2000)p50.solve();
	else if(s[1]=='A')p70.solve();
	else pshui.solve();
	return 0;
}
