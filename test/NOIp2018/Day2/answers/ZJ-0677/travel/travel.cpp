#include<bits/stdc++.h>
#define FOR(i,a,b) for(int i=(a),i##_end_=(b);i<=i##_end_;i++)
#define DOR(i,a,b) for(int i=(a),i##_end_=(b);i>=i##_end_;i--)
#define ll long long
#define M 5005
using namespace std;

template<class _>void chkmin(_ &a,_ b){if(a>b)a=b;}
template<class _>void chkmax(_ &a,_ b){if(a<b)a=b;}

int n,m;

struct node{
	int to,id;
	
	bool operator <(const node &a)const{
		return to<a.to;
	}
	
};

vector<node>G[M];

struct P60{

	void dfs(int x,int f){
		printf("%d ",x);
		for(int i=0;i<(int)G[x].size();i++){
			int y=G[x][i].to;
			if(y==f)continue;
			dfs(y,x);
		}
	}

	void solve(){
		FOR(i,1,n)sort(G[i].begin(),G[i].end());
		dfs(1,0);
		puts("");
	}

}p60;

struct P100{

	int low[M];
	int dfn[M];
	int Stk[M];
	int dfn_cnt;
	int top;

	int tid;
	int mark[M];
	int Line[M];
	int Ans1[M];
	int Ans2[M];
	int Tmp[M];
	int cnt;

	void Tarjan(int x,int ID){
		low[x]=dfn[x]=++dfn_cnt;
		Stk[++top]=x;
		for(int i=0;i<(int)G[x].size();i++){
			int y=G[x][i].to;
			if(ID==G[x][i].id)continue;
			if(!dfn[y]){
				Tarjan(y,G[x][i].id);
				chkmin(low[x],low[y]);
			}
			else chkmin(low[x],dfn[y]);
		}
		if(low[x]==dfn[x]){
			int y;
			if(Stk[top]==x){
				top--;
				return;
			}
			do{
				y=Stk[top--];
				Line[++tid]=y;
				mark[y]=1;
			}while(y!=x);
		}
	}

	int vis;
	int X,Y;

	void dfs(int x,int f){
//		cerr<<x<<" "<<f<<"xxx"<<endl;
		Ans2[++cnt]=x;
		if(mark[x]&&vis==0){
			vis=x;
			int flag=0;
			for(int i=0;i<(int)G[x].size();i++){
				int y=G[x][i].to;
				if(y==f)continue;
				if(mark[y]&&flag)continue;
				flag|=mark[y];
				dfs(y,x);
			}
			return;
		}
		int t=0;
		for(int i=0;i<(int)G[x].size();i++){
			int y=G[x][i].to;
			if(y==f)continue;
//			cerr<<y<<" "<<mark[y]<<"xxx"<<endl;
			if(mark[y]&&y!=vis)t=y;
			else if(!mark[y])dfs(y,x);
		}
		if(t)dfs(t,x);
	}

	void Solve(int x,int f){
		Tmp[++cnt]=x;
		for(int i=0;i<(int)G[x].size();i++){
			int y=G[x][i].to;
			if(y==f)continue;
			if((x==X&&y==Y)||(x==Y&&y==X))continue;
			Solve(y,x);
		}
	}

	bool check(){
		FOR(i,1,n){
			if(Ans1[i]<Tmp[i])return false;
			if(Ans1[i]>Tmp[i])return true;
		}
		return false;
	}

	void solve(){
		FOR(i,1,n)sort(G[i].begin(),G[i].end());
		FOR(i,1,n){
			if(dfn[i])continue;
			Tarjan(i,0);
		}
		Line[tid+1]=Line[1];
		FOR(i,1,tid){
			X=Line[i];
			Y=Line[i+1];
			cnt=0;
			Solve(1,0);
			if(check()||Ans1[1]==0){
				FOR(j,1,n)Ans1[j]=Tmp[j];
			}
		}
		cnt=0;
		int f=1;
		dfs(1,0);
		FOR(i,1,n){
			if(Ans1[i]<Ans2[i])break;
			if(Ans1[i]>Ans2[i]){
				f=2;
				break;
			}
		}
		FOR(i,1,n){
			int t=Ans1[i];
			if(f==2)t=Ans2[i];
			printf("%d ",t);
		}
		puts("");
	}

}p100;

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	FOR(i,1,m){
		int a,b;
		scanf("%d%d",&a,&b);
		G[a].push_back((node){b,i});
		G[b].push_back((node){a,i});
	}
	if(m==n-1)p60.solve();
	else p100.solve(); 
	return 0;
}
