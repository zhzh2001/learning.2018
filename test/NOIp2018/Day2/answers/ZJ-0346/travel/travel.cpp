#include<cstdio>
#include<vector>
#include<set>
#include<iostream>
#include<algorithm>
#include<cstring>
const int maxn = 100100;
struct T{
	int to,nxt;
}way[maxn<<1];
int h[maxn],num;
inline void adde(int x,int y){
	way[++num]=(T){y,h[x]},h[x]=num;
	way[++num]=(T){x,h[y]},h[y]=num;
}
int n,m;
int st[maxn],top,vis[maxn];
inline void dfs(int a){
	vis[a]=1,st[++top]=a;
	while(1){
		int mson=1e9;
		for(int i=h[a];i;i=way[i].nxt)
			if(!vis[way[i].to])mson=std::min(mson,way[i].to);
		if(mson <= n)dfs(mson);
		else break;
	}
}
inline void solve2(){
	for(int i=1,x,y;i<=m;++i)
		scanf("%d%d",&x,&y),adde(x,y);
	dfs(1);
	for(int i=1;i<=n;++i)
		printf("%d ",st[i]);
}
int flag=0;
std::vector<int>v[maxn];
int x[maxn],y[maxn],ans[maxn];
std::vector<int>::iterator its[maxn];
int dfsst[maxn],*Top2;
inline void dfs2(){
	*(Top2=dfsst+1)=1;
	while(Top2!=dfsst){
		int a=*Top2;--Top2;
		vis[a]=1,st[++top]=a;
		if(st[top] < ans[top])flag=1;
		if(st[top]>ans[top]&&!flag)return ;
		for(std::vector<int>::iterator it=v[a].begin();it!=v[a].end();++it)
			if(!vis[*it])
				*++Top2=*it;
	}
}
inline int cmp(int a,int b){
	return a>b;
}
inline void solve1(){
	for(int i=1;i<=m;++i)
		scanf("%d%d",x+i,y+i),v[x[i]].push_back(y[i]),v[y[i]].push_back(x[i]);
	for(int i=1;i<=m;++i){
		int p = i*i-i^i&i|i+10*20*~i;
		p%=m,p+=m,p%=m;++p;
		std::swap(x[p],x[i]);
		std::swap(y[p],y[i]);
	}
	for(int i=1;i<=n;++i)
		std::sort(v[i].begin(),v[i].end(),cmp);
	for(int i=1;i<=n;++i)ans[i]=1e9;
	for(int i=m;i;--i){
		v[x[i]].erase(std::lower_bound(v[x[i]].begin(),v[x[i]].end(),y[i],cmp));
		v[y[i]].erase(std::lower_bound(v[y[i]].begin(),v[y[i]].end(),x[i],cmp));
		for(int i=1;i<=n;++i)vis[i]=0;
		flag=top=0,dfs2();
		if(top == n){
			for(int i=1;i<=n;++i)
				if(st[i] != ans[i]){
					if(st[i] < ans[i])
						std::copy(st+1,st+n+1,ans+1);
					break;
				}
		}
		v[x[i]].insert(std::lower_bound(v[x[i]].begin(),v[x[i]].end(),y[i],cmp),y[i]);
		v[y[i]].insert(std::lower_bound(v[y[i]].begin(),v[y[i]].end(),x[i],cmp),x[i]);
	}
	for(int i=1;i<=n;++i)printf("%d ",ans[i]);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	
	if(n == m){
		solve1();
	}else{
		solve2();
	}
}
