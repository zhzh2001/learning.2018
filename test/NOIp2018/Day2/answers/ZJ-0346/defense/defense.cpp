#include<cstdio>
#include<cctype>
#include<iostream>
#include<algorithm>
#include<set>
const int maxn = 300100;
struct T{
	int to,nxt;
}way[maxn<<1];
int h[maxn],num;
std::set<int> s[maxn];
inline void adde(int x,int y){
	way[++num]=(T){y,h[x]},h[x]=num;
	way[++num]=(T){x,h[y]},h[y]=num;
	s[x].insert(y);
	s[y].insert(x);
}
int x,ch;
inline int read(){
	while(!isdigit(ch=getchar()));x=ch&15;
	while(isdigit(ch=getchar()))x=x*10+(ch&15);
	return x;
}
int n,m;
typedef long long ll;
ll p[maxn];
ll f[maxn],g[maxn];
int vis[maxn],fa[maxn];
inline void dp(int a){
	vis[a]=1;
	g[a]=0,f[a]=p[a];
	for(int i=h[a];i;i=way[i].nxt)
		if(!vis[way[i].to]){
			dp(way[i].to);
			fa[way[i].to]=a;
			g[a]+=f[way[i].to];
			f[a]+=std::min(g[way[i].to],f[way[i].to]);
		}
	vis[a]=0;
}
int st[maxn],top;
inline void redp1(int a){
	if(!a)return ;
	top=0;
	while(a)st[++top]=a,a=fa[a];
	for(int a=st[top];top;a=st[--top]){
		g[fa[a]]-=f[a];
		f[fa[a]]-=std::min(g[a],f[a]);
	}
}
inline void redp2(int a){
	while(a){
		g[fa[a]]+=f[a];
		f[fa[a]]+=std::min(g[a],f[a]);
		a=fa[a];
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),read();
	for(int i=1;i<=n;++i)p[i]=read();
	for(int i=1;i<n;++i)adde(read(),read());
	dp(1);
	for(int i=1;i<=m;++i){
		ll a=read(),x=read(),b=read(),y=read();
		if(s[a].count(b) && !x && !y){
			puts("-1");
			continue;
		}
		x=x?-1e16:1e16;
		y=y?-1e16:1e16;
		redp1(a),f[a]+=x,redp2(a);
		redp1(b),f[b]+=y,redp2(b);
		ll ans=std::min(g[1],f[1]);
		redp1(a),f[a]-=x,redp2(a);
		redp1(b),f[b]-=y,redp2(b);
		if(x<0)ans-=x;
		if(y<0)ans-=y;
		printf("%lld\n",ans);
	}
}
