var
	n,m,i,tot,u,v,l,r,num:longint;
	next,vet,head,ans,a:array[0..1000000] of longint;
	flag:array[0..10000] of boolean;
procedure sort(l,r: longint);
var
	i,j,x,y: longint;
begin
	i:=l; j:=r;
	x:=a[(l+r) div 2];
	repeat
		while a[i]<x do inc(i);
		while x<a[j] do dec(j);
		if not(i>j) then
		begin
			y:=a[i]; a[i]:=a[j]; a[j]:=y;
			inc(i); j:=j-1;
		end;
	until i>j;
	if l<j then sort(l,j);
	if i<r then sort(i,r);
end;
procedure add(u,v:longint);
begin
	inc(tot);
	next[tot]:=head[u]; head[u]:=tot;
	vet[tot]:=v;
end;
procedure dfs(id:longint);
var xa,xb,point,i:longint;
begin
	xa:=r+1; xb:=xa-1;
	flag[id]:=true;
	point:=head[id];
	while point<>0 do 
	begin
		if flag[vet[point]]=false then 
		begin
			inc(xb);
			a[xb]:=vet[point];
		end;
		point:=next[point];
	end;
	if xb>=xa then 
	begin
		r:=xb;
		sort(xa,xb);
		for i:=xa to xb do 
		begin
			inc(num);
			ans[num]:=a[i];
			dfs(a[i]);
		end;
	end;
end;
begin
	assign(input,'travel.in'); reset(input);
	assign(output,'travel.out'); rewrite(output);
	readln(n,m);
	for i:=1 to m do 
	begin
		readln(u,v);
		add(u,v);
		add(v,u);
	end;
	l:=1; r:=0; num:=1;
	ans[1]:=1;
	dfs(1);
	for i:=1 to n do 
	write(ans[i],' ');
	close(input); close(output);
end.