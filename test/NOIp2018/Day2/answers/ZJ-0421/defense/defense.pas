var
  tot,n,m,u,v,x,y,i:longint;
  head,next,vet,w:array[0..200000] of longint;
  f:array[0..100100,0..1] of longint;
  flag:array[0..110000,0..1] of boolean;
  s:string;
function min(a,b:longint):longint;
begin
  if a>b then exit(b)
  else exit(a);
end;
procedure add(u,v:longint);
begin
  inc(tot);
  next[tot]:=head[u];
  head[u]:=tot;
  vet[tot]:=v;
end;
procedure dfs(id,father:longint);
var point:longint;
begin
  point:=head[id];
  flag[id][0]:=false; flag[id][1]:=false;
  flag[u][x xor 1]:=true; flag[v][y xor 1]:=true;
  f[id][1]:=w[id]; f[id][0]:=0;
  while point<>0 do 
  begin
    if vet[point]<>father then 
	begin
	  dfs(vet[point],id);
	  if (flag[id][0]=false) then 
	  begin
	    if flag[vet[point]][1]=false then f[id][0]:=f[id][0]+f[vet[point]][1]
		else flag[id][0]:=true;
	  end;
	  if (flag[id][1]=false) then 
	  begin
	    if (flag[vet[point]][0] and flag[vet[point]][1])=true then flag[id][1]:=true
		else
	    begin	
		  if (flag[vet[point]][0]=false) then 
	  	  begin
		    if flag[vet[point]][1]=false then f[id][1]:=f[id][1]+min(f[vet[point]][0],f[vet[point]][1])
		    else f[id][1]:=f[id][1]+f[vet[point]][0];
		  end
		  else f[id][1]:=f[id][1]+f[vet[point]][1];
        end;	  
	  end;
	end;
    point:=next[point];
  end;
end;
begin
  assign(input,'defense.in'); reset(input);
  assign(output,'defense.out'); rewrite(output);
  readln(n,m,s);
  for i:=1 to n do  read(w[i]);
  for i:=1 to n-1 do 
  begin
    readln(u,v);
    add(u,v);
	add(v,u);
  end;
  if ((n<=2000) and (m<=2000)) then 
  begin
	for i:=1 to m do 
	begin
	  readln(u,x,v,y);
      dfs(1,0);
	  if (flag[1][1] and flag[1][0]=true) then writeln(-1)
	  else 
	  begin
	    if flag[1][0]=false then 
		begin
		  if flag[1][1]=false then writeln(min(f[1][1],f[1][0]))
		  else writeln(f[1][0]);
		end 
		else writeln(f[1][1]);
	  end;
	end;
  end;
  close(input); close(output);
end.
