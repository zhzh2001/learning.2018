var
  num,i,j,n,m,ans:longint;
  map:array[0..100,0..100] of longint;
  a,b:array[0..4000000] of longint;
procedure sort(l,r: longint);
var
	i,j,x,y: longint;
begin
	i:=l; j:=r;
	x:=a[(l+r) div 2];
	repeat
		while a[i]<x do inc(i);
		while x<a[j] do dec(j);
		if not(i>j) then
		begin
			y:=a[i]; a[i]:=a[j]; a[j]:=y;
			y:=b[i]; b[i]:=b[j]; b[j]:=y;
			inc(i); j:=j-1;
		end;
	until i>j;
	if l<j then sort(l,j);
	if i<r then sort(i,r);
end;
procedure solve(x,y,k1,k2:longint);
begin
  if x>n-1 then exit;
  if y>m-1 then exit;
  k1:=k1*2+map[x][y];
  if (x=n-1) and (y=m-1) then 
  begin
    inc(num);
    b[num]:=k1;
    a[num]:=k2;
	exit;
  end;
  solve(x+1,y,k1,k2*2);
  solve(x,y+1,k1,k2*2+1);
end;
function find:boolean;
begin
  num:=0;
  solve(0,0,0,0);
  if num<>0 then sort(1,num);
  //writeln(num);
  for i:=2 to num do 
  if b[i]>b[i-1] then exit(false);
  exit(true);
end;
procedure dfs(x,y:longint);
begin
  if (x=n-1) and (y=m-1) then
  begin
    {for i:=0 to n-1 do 
	begin
	  for j:=0 to m-1 do 
	  write(map[i][j]);
	  writeln;
	end;}
    map[x][y]:=1;
    if find=true then inc(ans);
	map[x][y]:=0;
    if find=true then inc(ans);
	exit;
  end;
  if y=m-1 then dfs(x+1,0) else dfs(x,y+1);
  map[x,y]:=1;
  if y=m-1 then dfs(x+1,0) else dfs(x,y+1);
  map[x][y]:=0;
end;
begin
   assign(input,'game.in'); reset(input);
   assign(output,'game.out'); rewrite(output);
   readln(n,m);
   dfs(0,0);
   writeln(ans);
   close(input); close(output);
end.