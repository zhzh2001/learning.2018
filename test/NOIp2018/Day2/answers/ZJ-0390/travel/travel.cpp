#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
const int N=5005;
using namespace std;
int n,m,x,y,i,g,v[N],h[N*2];
struct Node{
	int pre,nxt;
}tree[N*2],a[N*2];
void read(int &a)
{	a=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') a=a*10+ch-48,ch=getchar(); 
}
void add(int x,int y)
{tree[++g].pre=h[x];tree[g].nxt=y;h[x]=g;}
inline bool cmp(Node a,Node b)
{return a.pre<b.pre||a.pre==b.pre&&a.nxt>b.nxt;}
void dfs(int x)
{	v[x]=1;printf("%d ",x);
	for(int i=h[x];i;i=tree[i].pre)
	if (!v[tree[i].nxt]) dfs(tree[i].nxt);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);read(m);
	for(i=1;i<=m;i++)
	{	read(x);read(y);
		a[i*2-1].pre=x;a[i*2-1].nxt=y;
		a[i*2].pre=y;a[i*2].nxt=x;
	}
	sort(a+1,a+2*m+1,cmp);
	for(i=1;i<=2*m;i++)	add(a[i].pre,a[i].nxt);
	memset(v,0,sizeof(v));
	if (m==n-1) dfs(1);
}
