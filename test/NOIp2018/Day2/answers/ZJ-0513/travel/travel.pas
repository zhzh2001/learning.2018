var n,m,x,y,t:int64; i,j:longint;
    a,b,zhan:array[0..5000]of int64;
    vis:array[0..5000]of boolean;
    g:array[0..5000,0..5000]of longint;
procedure swap(var a,b:int64);
var p:int64;
begin
 p:=a; a:=b; b:=p;
end;
procedure sort(l,r: longint);
      var
         i,j,x,y,xx: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         xx:=b[(l+r)div 2];
         repeat
           while (a[i]<x)or(a[i]=x)and(b[i]<xx) do
            inc(i);
           while (x<a[j])or(x=a[j])and(xx<b[j]) do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                y:=b[i];
                b[i]:=b[j];
                b[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
procedure dfs(u:int64);
var i:longint;
begin
 vis[u]:=true; inc(t); zhan[t]:=u;
 for i:=1 to g[u,0] do
  if not vis[g[u,i]] then dfs(g[u,i]);
end;
begin
 assign(input,'travel.in');
assign(output,'travel.out');
reset(input);
rewrite(output);
 read(n,m);
 for i:=1 to m do
 begin
  read(a[i],b[i]);
  if a[i]>b[i] then swap(a[i],b[i]);
 end;
 sort(1,m);
 for i:=1 to m do
 begin
  inc(g[a[i],0]);
  g[a[i],g[a[i],0]]:=b[i];
  inc(g[b[i],0]);
  g[b[i],g[b[i],0]]:=a[i];
 end;
 //for i:=1 to n do
 // for j:=1 to g[i,0] do writeln(i,' ',j,' ',g[i,j]);
 dfs(1);
 for i:=1 to t do write(zhan[i],' ');
 close(input);
 close(output); 
end.