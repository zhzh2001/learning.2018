uses math;
type e=record
  too,next:int64;
  end;
var n,m,k,t,x,y,xx,yy:int64; i,j:longint;
    s,ss:ansistring;
    a,father:array[0..100000]of int64;
    head:array[0..400000]of int64;
    edge:array[0..400000]of e;
    f:array[0..2000,0..1]of int64;
    l,r:array[0..100000,0..1]of int64;
procedure add(a,b:int64);
begin
 inc(t);
 edge[t].too:=b;
 edge[t].next:=head[a];
 head[a]:=t;
end;
procedure swap(var a,b:int64);
var p:int64;
begin
 p:=a; a:=b; b:=p;
end;
function dfs(u,fa,k:int64):int64;
var i,v:int64;
begin
 father[u]:=fa;
 if f[u,k]>0 then exit(f[u,k]);
 if k=0 then
 begin
  i:=head[u];
  while i>0 do
  begin
   v:=edge[i].too;
   if v<>fa then
   f[u,k]:=f[u,k]+dfs(v,u,1);
   i:=edge[i].next;
  end;
  exit(f[u,k]);
 end
 else begin
  i:=head[u];
  while i>0 do
  begin
   v:=edge[i].too;
   if v<>fa then
   f[u,k]:=f[u,k]+min(dfs(v,u,0),dfs(v,u,1));
   i:=edge[i].next;
  end;
  f[u,k]:=f[u,k]+a[u];
  exit(f[u,k]);
 end;
end;
function dfs2(u,fa,k:int64):int64;
var i,v:int64;
begin
 if r[u,k]>0 then exit(r[u,k]);
 if k=0 then
 begin
  i:=head[u];
  while i>0 do
  begin
   v:=edge[i].too;
   if v<>fa then
   r[u,k]:=r[u,k]+dfs2(v,u,1);
   i:=edge[i].next;
  end;
  exit(r[u,k]);
 end
 else begin
  i:=head[u];
  while i>0 do
  begin
   v:=edge[i].too;
   if v<>fa then
   r[u,k]:=r[u,k]+min(dfs2(v,u,0),dfs2(v,u,1));
   i:=edge[i].next;
  end;
  r[u,k]:=r[u,k]+a[u];
  exit(r[u,k]);
 end;
end;
function dfs3(u,fa,k:int64):int64;
var i,v:int64;
begin
 if l[u,k]>0 then exit(l[u,k]);
 if k=0 then
 begin
  i:=head[u];
  while i>0 do
  begin
   v:=edge[i].too;
   if v<>fa then
   l[u,k]:=l[u,k]+dfs3(v,u,1);
   i:=edge[i].next;
  end;
  exit(l[u,k]);
 end
 else begin
  i:=head[u];
  while i>0 do
  begin
   v:=edge[i].too;
   if v<>fa then
   begin
    if v<>1 then
   l[u,k]:=l[u,k]+min(dfs3(v,u,0),dfs3(v,u,1))
   else l[u,k]:=l[u,k]+a[1];
   end;
   i:=edge[i].next;
  end;
  l[u,k]:=l[u,k]+a[u];
  exit(l[u,k]);
 end;
end;
begin
assign(input,'defense.in');
assign(output,'defense.out');
reset(input);
rewrite(output);
 readln(s); ss:='';
 for i:=1 to length(s) do
 begin
  if s[i]=' ' then begin inc(k); continue; end;
  if k=0 then n:=n*10+ord(s[i])-48;
  if k=1 then m:=m*10+ord(s[i])-48;
  if k=2 then ss:=ss+s[i];
 end;
 for i:=1 to n do read(a[i]);
 if (ss='A1')and(n=100000) then
 begin
  x:=dfs2(1,0,0);
  y:=dfs2(1,0,1);
  x:=dfs3(n,0,0);
  y:=dfs3(n,0,1);
  for i:=1 to m do
  begin
   read(x,xx,y,yy);
   if yy=0 then
   begin
    writeln(l[y-1,1]+r[y+1,1]);
   end
   else begin
    writeln(min(l[y-1,0],l[y-1,1])+
    min(r[y+1,0],r[y+1,1]));
   end;
  end;
 end
 else begin
 for i:=2 to n do
 begin
  read(x,y);
  add(x,y);
  add(y,x);
 end;
 for i:=1 to m do
 begin
  fillchar(f,sizeof(f),0);
  read(x,xx,y,yy);
  if x>y then
  begin
   swap(x,y);
   swap(xx,yy);
  end;
  if xx=1 then
   f[x,0]:=30000000001
  else f[x,1]:=30000000001;
  if yy=1 then
   f[y,0]:=30000000001
  else f[y,1]:=30000000001;
   x:=dfs(1,0,0);
   y:=dfs(1,0,1);
  //for j:=1 to n do
   //writeln(f[j,0],' ',f[j,1]);
  if (x>=30000000001)and(y>=30000000001)
  then writeln(-1)
  else writeln(min(x,y));
 end;
 end;
 close(input);
 close(output);
end.