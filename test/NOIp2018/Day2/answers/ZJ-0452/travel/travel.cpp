#include <bits/stdc++.h>
#define gc getchar()
using namespace std;
const int N=5009;
int n,m,vis[N],a[N],b[N],cnt1,cnt2,lim;
vector<int> e[N];
int Ans[N],now[N];
int read()
{
	int x=1;
	char ch;
	while (ch=gc,ch<'0'||ch>'9') if (ch=='-') x=-1;
	int s=ch-'0';
	while (ch=gc,ch>='0'&&ch<='9') s=s*10+ch-'0';
	return s*x;
}
void dfs(int x,int y)
{
	Ans[cnt1++]=x;
	for (int i=0;i<(int)e[x].size();i++)
		if (e[x][i]!=y) dfs(e[x][i],x);
}
void Dfs(int x)
{
	vis[x]=1,now[cnt2++]=x;
	for (int i=0;i<(int)e[x].size();i++)
		if (!vis[e[x][i]]&&(!(x==a[lim]&&e[x][i]==b[lim]))&&(!(x==b[lim]&&e[x][i]==a[lim])))
			Dfs(e[x][i]);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=m;i++)
	{
		int x=read(),y=read();
		e[x].push_back(y),e[y].push_back(x);
		a[i]=x,b[i]=y;
	}
	for (int i=1;i<=n;i++) sort(e[i].begin(),e[i].end());
	if (m==n-1)
	{
		dfs(1,0);
		for (int i=0;i<n;i++) printf("%d%c",Ans[i],(i==n-1?'\n':' '));
		return 0;
	}
	cnt1=0;
	for (int i=n;i>=1;i--) Ans[cnt1++]=i;
	for (lim=1;lim<=m;lim++)
	{
		memset(vis,0,sizeof(vis));
		cnt2=0;
		Dfs(1);
		if (cnt2<n) continue;
		int j=0;
		while (now[j]==Ans[j]&&j<n) j++;
		if (j<n&&now[j]<Ans[j])
			for (int k=0;k<n;k++) Ans[k]=now[k];
	}
	for (int i=0;i<n;i++) printf("%d%c",Ans[i],(i==n-1?'\n':' '));
	return 0;
}

