#include <bits/stdc++.h>
#define gc getchar()
#define sz(a) a.size()
using namespace std;
typedef long long ll;
const int mod=1000000007;
const int N=1000009;
int n,m,a[10],b[10],n1,n2,dp[2][10],num[20],pos1[10],pos2[10];
int f[N];
vector<int> v1[9][9],v2[9][9],v3[9][9],v4[9][9],v5[9][9],v6[9][9];
int read()
{
	int x=1;
	char ch;
	while (ch=gc,ch<'0'||ch>'9') if (ch=='-') x=-1;
	int s=ch-'0';
	while (ch=gc,ch>='0'&&ch<='9') s=s*10+ch-'0';
	return s*x;
}
void add(int &x,int y)
{
	x=(x+y>=mod?x+y-mod:x+y);
}
void work()
{
	for (int i=2;i<=8;i++)
	{
		for (int j=0;j<=i;j++)
		{
			v1[i][j].push_back(0);
			v1[i][j].push_back(i);
			if (j!=0&&j!=i) v1[i][j].push_back(j);
			if (i<=6)
			{
				v2[i][j].push_back(0);
				v2[i][j].push_back(i+2);
				for (int k=0;k<(int)v1[i][j].size();k++)
					v2[i][j].push_back(v1[i][j][k]+1);
			}
			if (i>=4)
			{
				for (int k=0;k<(int)v1[i-2][min(max(j-1,0),i-2)].size();k++)
					v3[i][j].push_back(v1[i-2][min(max(j-1,0),i-2)][k]);
			}
			if (i==2)
			{
				v4[i][j].push_back(0);
				v4[i][j].push_back(1);
				v4[i][j].push_back(2);
			}
			else
			{
				for (int k=0;k<(int)v1[i-1][max(j-1,0)].size();k++)
					v4[i][j].push_back(v1[i-1][max(j-1,0)][k]);
				v4[i][j].push_back(i);
			}
			if (i<=7)
			{
				for (int k=0;k<(int)v1[i][j].size();k++)
					v5[i][j].push_back(v1[i][j][k]);
				v5[i][j].push_back(i+1);
			}
			if (i>=3)
			{
				for (int k=0;k<(int)v1[i-1][min(j,i-1)].size();k++)
					v6[i][j].push_back(v1[i-1][min(j,i-1)][k]);
			}
		}
	}
	for (int i=1;i<=n+m-3;i++)
	{
		if (i<m) num[i]=i+1;
		else if (n+m-2-i<m) num[i]=n+m-2-i+1;
		else num[i]=m;
		if (i%2) a[++n1]=num[i],pos1[n1]=i;
		else b[++n2]=num[i],pos2[n2]=i;
	}
	int ret1=0,ret2=0;
	for (int i=0;i<=a[1];i++) dp[0][i]=1;
	int now=0,last=1;
	for (int i=2;i<=n1;i++)
	{
		last=now,now^=1;
		for (int j=0;j<=a[i];j++) dp[now][j]=0;
		for (int j=0;j<=a[i-1];j++)
		{
			if (a[i-1]==a[i]&&a[i]==num[pos1[i]-1]-1)
			{
				for (int k=0;k<(int)v1[a[i-1]][j].size();k++)
					add(dp[now][v1[a[i-1]][j][k]],dp[last][j]);
			}
			if (a[i-1]==a[i]-2)
			{
				for (int k=0;k<(int)v2[a[i-1]][j].size();k++)
					add(dp[now][v2[a[i-1]][j][k]],dp[last][j]);
			}
			if (a[i-1]==a[i]+2)
			{
				for (int k=0;k<(int)v3[a[i-1]][j].size();k++)
					add(dp[now][v3[a[i-1]][j][k]],dp[last][j]);
			}
			if (a[i-1]==a[i]&&a[i]==num[pos1[i]-1])
			{
				for (int k=0;k<(int)v4[a[i-1]][j].size();k++)
					add(dp[now][v4[a[i-1]][j][k]],dp[last][j]);
			}
			if (a[i-1]==a[i]-1)
			{
				for (int k=0;k<(int)v5[a[i-1]][j].size();k++)
					add(dp[now][v5[a[i-1]][j][k]],dp[last][j]);
			}
			if (a[i-1]==a[i]+1)
			{
				for (int k=0;k<(int)v6[a[i-1]][j].size();k++)
					add(dp[now][v6[a[i-1]][j][k]],dp[last][j]);
			}
		}
	}
	for (int i=0;i<=a[n1];i++) add(ret1,dp[now][i]);
	for (int i=0;i<=b[1];i++) dp[0][i]=1;
	now=0,last=1;
	for (int i=2;i<=n2;i++)
	{
		last=now,now^=1;
		for (int j=0;j<=b[i];j++) dp[now][j]=0;
		for (int j=0;j<=b[i-1];j++)
		{
			if (b[i-1]==b[i]&&b[i]==num[pos2[i]-1]-1)
			{
				for (int k=0;k<(int)v1[b[i-1]][j].size();k++)
					add(dp[now][v1[b[i-1]][j][k]],dp[last][j]);
			}
			if (b[i-1]==b[i]-2)
			{
				for (int k=0;k<(int)v2[b[i-1]][j].size();k++)
					add(dp[now][v2[b[i-1]][j][k]],dp[last][j]);
			}
			if (b[i-1]==b[i]+2)
			{
				for (int k=0;k<(int)v3[b[i-1]][j].size();k++)
					add(dp[now][v3[b[i-1]][j][k]],dp[last][j]);
			}
			if (b[i-1]==b[i]&&b[i]==num[pos2[i]-1])
			{
				for (int k=0;k<(int)v4[b[i-1]][j].size();k++)
					add(dp[now][v4[b[i-1]][j][k]],dp[last][j]);
			}
			if (b[i-1]==b[i]-1)
			{
				for (int k=0;k<(int)v5[b[i-1]][j].size();k++)
					add(dp[now][v5[b[i-1]][j][k]],dp[last][j]);
			}
			if (b[i-1]==b[i]+1)
			{
				for (int k=0;k<(int)v6[b[i-1]][j].size();k++)
					add(dp[now][v6[b[i-1]][j][k]],dp[last][j]);
			}
		}
	}
	for (int i=0;i<=b[n2];i++) add(ret2,dp[now][i]);
	int ret=(ll)ret1*ret2%mod*4%mod;
	printf("%d\n",ret);
}
int ksm(int x,int y,int z=1)
{
	for (;y;y>>=1,x=(ll)x*x%mod)
		if (y&1) z=(ll)z*x%mod;
	return z;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if (n<m) swap(n,m);
	if (m==1)
	{
		printf("%d\n",ksm(2,n));
		return 0;
	}
	if (m==2)
	{
		printf("%d\n",ksm(3,n-1,4));
		return 0;
	}
	work();
	return 0;
}

