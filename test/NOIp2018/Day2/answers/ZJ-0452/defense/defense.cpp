#include <bits/stdc++.h>
#define gc getchar()
using namespace std;
typedef long long ll;
const int N=100009;
const ll inf=(ll)1e15;
char typ[10];
int n,m,first[N],number,p[N],c[N];
ll dp[2][N];
struct edge
{
	int to,next;
	void add(int x,int y)
	{
		to=y,next=first[x],first[x]=number;
	}
}e[N<<1];
int read()
{
	int x=1;
	char ch;
	while (ch=gc,ch<'0'||ch>'9') if (ch=='-') x=-1;
	int s=ch-'0';
	while (ch=gc,ch>='0'&&ch<='9') s=s*10+ch-'0';
	return s*x;
}
void dfs(int x,int y)
{
	dp[0][x]=0,dp[1][x]=p[x];
	for (int i=first[x];i;i=e[i].next)
		if (e[i].to!=y)
		{
			dfs(e[i].to,x);
			dp[0][x]+=dp[1][e[i].to];
			dp[1][x]+=min(dp[0][e[i].to],dp[1][e[i].to]);
		}
	if (c[x]==0) dp[1][x]=inf;
	if (c[x]==1) dp[0][x]=inf;
	dp[0][x]=min(dp[0][x],inf),dp[1][x]=min(dp[1][x],inf);
}
ll F[2][N],G[2][N];
void work1()
{
	F[0][1]=0,F[1][1]=p[1];
	for (int i=2;i<=n;i++)
	{
		F[0][i]=F[1][i-1];
		F[1][i]=p[i]+min(F[0][i-1],F[1][i-1]);
	}
	G[0][n]=0,G[1][n]=p[n];
	for (int i=n-1;i;i--)
	{
		G[0][i]=G[1][i+1];
		G[1][i]=p[i]+min(G[0][i+1],G[1][i+1]);
	}
	while (m--)
	{
		int a=read(),x=read(),b=read(),y=read();
		if (a>b) swap(a,b),swap(x,y);
		if ((x|y)==0) puts("-1");
		else printf("%lld\n",F[x][a]+G[y][b]);
	}
}
void work2()
{
	F[0][1]=inf,F[1][1]=p[1];
	for (int i=2;i<=n;i++)
	{
		F[0][i]=F[1][i-1];
		F[1][i]=p[i]+min(F[0][i-1],F[1][i-1]);
	}
	G[0][n]=0,G[1][n]=p[n];
	for (int i=n-1;i;i--)
	{
		G[0][i]=G[1][i+1];
		G[1][i]=p[i]+min(G[0][i+1],G[1][i+1]);
	}
	while (m--)
	{
		int a=read(),x=read(),b=read(),y=read();
		if (y==1) printf("%lld\n",min(F[0][b-1],F[1][b-1])+G[y][b]);
		else printf("%lld\n",F[1][b-1]+G[y][b]);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();
	scanf("%s",&typ);
	for (int i=1;i<=n;i++) p[i]=read();
	for (int i=1;i<n;i++)
	{
		int x=read(),y=read();
		e[++number].add(x,y),e[++number].add(y,x);
	}
	if (n>2000&&typ[0]=='A'&&typ[1]=='2')
	{
		work1();
		return 0;
	}
	if (n>2000&&typ[0]=='A'&&typ[1]=='1')
	{
		work2();
		return 0;
	}
	memset(c,-1,sizeof(c));
	while (m--)
	{
		int a=read(),x=read(),b=read(),y=read();
		c[a]=x,c[b]=y;
		dfs(1,0);
		printf("%lld\n",min(dp[0][1],dp[1][1])==inf?-1:min(dp[0][1],dp[1][1]));
		c[a]=c[b]=-1;
	}
	return 0;
}
