#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++)
#define Out() (fwrite(St,1,Top,stdout))
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
#define LL long long
int Top,fn;static char buf[1000000],St[1000000],nb[100],*p1=buf,*p2=buf;
using namespace std;
const int maxn=(1e5)+5;const LL INF=(LL)1<<60;
int n,Q,p[maxn],k,kk,k_,kk_;LL Ans,f[maxn][3];char Py_c,Py_n;
int tot,lnk[maxn],nxt[maxn<<1],son[maxn<<1];
void add_e(int x,int y){son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot;}
struct ff{
	int x,y;
	bool operator <(const ff b)const{return x<b.x||(x==b.x&&y<b.y);}
};
map<ff,bool>g;
int read(){
	int ret=0;char ch=gt();
	while(ch<'0'||ch>'9') ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return ret;
}
void write(LL x){fn=0;do nb[++fn]=x%10+'0',x/=10;while(x);while(fn) pt(nb[fn--]);}
void getpy(){char ch=gt();while(ch<'A'||ch>'C') ch=gt();Py_c=ch,Py_n=gt();}
void DFS(int x,int fa){
	f[x][0]=0,f[x][1]=p[x];
	LL tp;
	for(int j=lnk[x];j;j=nxt[j])if(son[j]!=fa){
		DFS(son[j],x);
		if(f[son[j]][1]==INF) f[x][0]=INF;
		if(f[x][0]!=INF) f[x][0]+=f[son[j]][1];
		if(f[son[j]][0]<f[son[j]][1]) tp=f[son[j]][0];else tp=f[son[j]][1];
		if(tp==INF) f[x][1]=INF;
		if(f[x][1]!=INF) f[x][1]+=tp;
	}
	if(k==x) f[x][kk^1]=INF;
	if(k_==x) f[x][kk_^1]=INF;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),Q=read(),getpy();
	for(int i=1;i<=n;i++) p[i]=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		g[(ff){x,y}]=1;
		add_e(x,y),add_e(y,x);
	}
	while(Q--){
		k=read(),kk=read(),k_=read(),kk_=read();
	    if(!kk&&!kk_&&g[(ff){k,k_}]){pt('-'),pt('1'),pt('\n');continue;}
		DFS(1,0);
		Ans=(f[1][0]<f[1][1]?f[1][0]:f[1][1]);
		if(Ans!=INF) write(Ans),pt('\n');else pt('-'),pt('1'),pt('\n');
	}
	return Out(),0;
}
