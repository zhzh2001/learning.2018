#include<bits/stdc++.h>
#define LL long long
using namespace std;
const LL TT=(1e9)+7;
int n,m;
LL work(int a,int b){
	LL s=1,w=a;
	while(b){
		if(b&1) (s*=w)%=TT;
		(w*=w)%=TT,b>>=1;
	}
	return s;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n>m) swap(n,m);
	if(n==1) printf("%lld\n",work(2,m));
	if(n==2) printf("%lld\n",(work(3,m-1)<<2)%TT);
	if(n==3&&m==3) puts("112");
	return 0;
}
