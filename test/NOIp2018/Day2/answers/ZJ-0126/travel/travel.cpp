#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++)
#define Out() (fwrite(St,1,Top,stdout))
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
int Top,fn;static char buf[1000000],St[1000000],nb[100],*p1=buf,*p2=buf;
using namespace std;
const int maxn=5005;
int n,m,cnt,top,Fa[maxn],now;bool inq[maxn],vis[maxn],flg;
int tot,lnk[maxn],nxt[maxn<<1],son[maxn<<1];
void add_e(int x,int y){son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot;}
int read(){
	int ret=0;char ch=gt();
	while(ch<'0'||ch>'9') ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return ret;
}
void write(int x){fn=0;do nb[++fn]=x%10+'0',x/=10;while(x);while(fn) pt(nb[fn--]);}
void DFS(int x,int fa){
	write(x),pt(++cnt==n?'\n':' ');
	vector<int>p;p.clear();
	for(int j=lnk[x];j;j=nxt[j])if(son[j]!=fa) p.push_back(son[j]);
	sort(p.begin(),p.end());
	for(int j=0,tj=p.size();j<tj;j++) DFS(p[j],x);
}
void find(int x,int dad){
	if(vis[x]){
		top=x,inq[x]=1;
		for(int i=Fa[x];i!=x;i=Fa[i]) inq[i]=1;
		return;
	}
	vis[x]=1;
	for(int j=lnk[x];j;j=nxt[j]) if(son[j]!=dad&&!inq[son[j]]) find(son[j],Fa[son[j]]=x);
}
void solve(int x,int fa){
	vis[x]=1,write(x),pt(++cnt==n?'\n':' ');
	vector<int>p;p.clear();
	for(int j=lnk[x];j;j=nxt[j])if(son[j]!=fa&&!vis[son[j]]) p.push_back(son[j]);
	sort(p.begin(),p.end());
	if(!inq[x]||flg){for(int j=0,tj=p.size();j<tj;j++) solve(p[j],x);return;}
	if(x==top){
		bool ok=0;
		for(int j=0,tj=p.size();j<tj;j++) if(inq[p[j]]){if(!ok) ok=1;else now=p[j];}
		for(int j=0,tj=p.size();j<tj;j++){solve(p[j],x);}
		return;
	}
	int pos;
	for(int j=0,tj=p.size();j<tj;j++) if(inq[p[j]]) pos=p[j];
	if(pos<now){for(int j=0,tj=p.size();j<tj;j++) solve(p[j],x);return;}
	flg=1;for(int j=0,tj=p.size();j<tj;j++) if(p[j]!=pos) solve(p[j],x);
}
void work(){
	find(1,0);
	memset(vis,0,sizeof vis),solve(1,0);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		add_e(x,y),add_e(y,x);
	}
	if(m==n-1) DFS(1,0);
	  else work();
	return Out(),0;
}
