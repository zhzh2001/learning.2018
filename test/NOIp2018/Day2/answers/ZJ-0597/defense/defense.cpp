#include <map>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define N 5500
#define M 5500
#define int long long
#define INF (1ll<<51)
using namespace std;
int n,m;
int fst[N],nxt[M<<1],to[M<<1],cnt = 1;
int c[N],q[N];
int f[N][2];
map<long long,bool>mp[N];
char s[101];
inline void addedge(int x,int y){
	cnt++;
	nxt[cnt] = fst[x];
	fst[x] = cnt;
	to[cnt] = y;
}
void dfs(int u,int fa,int jj){
//	if(jj >= 2)return;
	vector<int>vv;
	int jb = 0;
	for(int i = fst[u];i != -1;i = nxt[i]){
		int v = to[i];
		if(v == fa)continue;
		dfs(v,u,jj + (q[u] != -1));
//		if(q[v] == 1){
//			f[u][0] = f[v][1];
//			f[u][1] = f[v][1] + c[u];
//		}
//		else if(q[v] == 0){
//			f[u][1] = f[v][0] + c[u];
//			f[u][0] = 0x3f3f3f3f;
//		}
//		else{
//			f[u][1] = min(f[v][0],f[v][1]) + c[u];
//			f[u][0] = f[v][1];
//		}
		f[u][1] = min(f[v][0],f[v][1]) + c[u];
		f[u][0] = f[v][1];
		if(q[u] == 0)f[u][1] = INF;
		if(q[u] == 1)f[u][0] = INF;
		vv.push_back(v);
		jb += f[v][1];
		jb = min(jb,INF);
	}
	f[u][0] = jb;
	f[u][1] = c[u];
	for(int i = 0;i < vv.size();i++){
		int v = vv[i];
		f[u][1] += min(f[v][0],f[v][1]);
	}
//	bool jbjb = 0;
	if(q[u] == 0)f[u][1] = INF;
	if(q[u] == 1)f[u][0] = INF;
}
signed main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(fst,-1,sizeof fst);
	memset(q,-1,sizeof q);
	cin>>n>>m;
	scanf("%s",s);
	for(int i = 1;i <= n;i++)
		scanf("%lld",&c[i]);
	for(int i = 1;i < n;i++){
		int x,y;
		scanf("%lld %lld",&x,&y);
		addedge(x,y);
		addedge(y,x);
		mp[x][y] = mp[y][x] = 1;
	}
	bool fst = 1;
	for(int i = 1;i <= m;i++){
		int a,x,b,y;
		scanf("%lld %lld %lld %lld",&a,&x,&b,&y);
		if(mp[a][b] && !x && !y){
			puts("-1");
			continue;
		}
		q[a] = x,q[b] = y;
		if(fst)dfs(1,0,0);
		else dfs(1,0,0);
		q[a] = q[b] = -1;
		fst = 0;
		int ans = 0;
//		if(a == 1)ans = f[1][x];
//		else if(b == 1)ans = f[1][b];
		ans = min(f[1][0],f[1][1]);
//		for(int i = 1;i <= n;i++){
//			printf("%d:%d %d\n",i,f[i][0],f[i][1]);
//		}
		printf("%lld\n",ans);
		
	}
	return 0;
}
