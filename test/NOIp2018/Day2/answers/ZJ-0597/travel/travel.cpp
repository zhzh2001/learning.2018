#include <queue>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define N 5500
#define M 5500
#define it vector<int>::iterator
using namespace std;
int n,m;
int ans[N],ans2[N],len = 0;
int fst[N],nxt[M<<1],to[M<<1],cnt = 1;
char used[N],inq[N];
char kd[M<<1];
//priority_queue<int,vector<int>,greater<int> >q,qq;

inline void addedge(int x,int y){
	cnt++;
	nxt[cnt] = fst[x];
	fst[x] = cnt;
	to[cnt] = y;
}
/*
void dfs(int u){
	printf("<%d>\n",u);
	ans[++len] = u;
	used[u] = 1;
	for(int i = fst[u];i != -1;i = nxt[i])
		if(!inq[to[i]] && !used[to[i]])
			q.push(to[i]),inq[to[i]] = 1;
	if(q.empty())return;
//	qq = q;
//	if(1){
//		while(!qq.empty()){
//			int y = qq.top();
//			cout<<y<<endl;
//			qq.pop();
//		}
//		if(u == 6)
//			return;
//	}
	int v = q.top();
	q.pop();
	dfs(v);
}
*/
void dfs(int u,int fa,int *aa){
	aa[++len] = u;
	vector<int>vec;
	for(int i = fst[u];i != -1;i = nxt[i]){
		int v = to[i];
		if(v == fa || kd[i])continue;
		vec.push_back(v);
	}
	if(vec.empty())return;
	sort(vec.begin(),vec.end());
	for(it i = vec.begin();i != vec.end();i++)
		dfs(*i,u,aa);
}
#define pr pair<int,int>
#define mp make_pair 
int fa[N];
int dep[N];
int kill[N],tot = 0;
bool hv = 0;
void pre(int u,int v){
	if(hv)return;
	hv = 1;
	for(int i = fst[u];i != -1;i = nxt[i])
		if(to[i] == v){
			kill[++tot] = i;
			break;
		}
//	cout<<u<<" "<<v<<endl;
//	for(int i = 1;i <= n;i++)
//		printf("fa %d :%d\n",i,fa[i]);
	if(dep[u] < dep[v])swap(u,v);
	while(dep[u] > dep[v])kill[++tot] = fa[u],u = to[fa[u]];
	while(u != v){
		kill[++tot] = fa[u],u = to[fa[u]];
		kill[++tot] = fa[v],v = to[fa[v]];
	}
}
queue<int>q;
void bfs(){
	memset(inq,0,sizeof inq);
	memset(fa,0,sizeof fa);
	q.push(1);
	inq[1] = 1;
	dep[1] = 1;
	while(!q.empty()){
		int u = q.front();
		q.pop();
		for(int i = fst[u];i != -1;i = nxt[i]){
			int v = to[i];
			if(inq[v] && v != to[fa[u]])
				pre(u,v);
			if(inq[v])continue;
			fa[v] = i ^ 1;
			dep[v] = dep[u] + 1;
			q.push(v);
			inq[v] = 1;
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(fst,-1,sizeof fst);
	memset(used,0,sizeof used);
	memset(inq,0,sizeof inq);
	memset(kd,0,sizeof kd);
	memset(ans,0x3f,sizeof ans);
	cin>>n>>m;
	for(int i = 1;i <= m;i++){
		int x,y;
		scanf("%d %d",&x,&y);
		addedge(x,y);
		addedge(y,x);
	}
	if(m == n - 1){
		len = 0;
		dfs(1,0,ans);
		
	}
	else{
		bfs();
		for(int i = 1;i <= tot;i++){
//			printf("kill %d\n",to[kill[i]]);
			kd[kill[i]] = kd[kill[i] ^ 1] = 1;
			len = 0;
			dfs(1,0,ans2);
			for(int j = 1;j <= n;j++){
				if(ans[j] != ans2[j]){
					if(ans2[j] < ans[j]){
						memcpy(ans,ans2,sizeof ans);
//						puts("cp");
					}
					break;
				}
//				printf("%d ",ans[j]);
			}
//			puts("");
			kd[kill[i]] = kd[kill[i] ^ 1] = 0;
		}
		for(int i = 1;i <= n;i++){
			printf("%d",ans[i]);
			if(i != n)putchar(' ');
		}
	}
	/*
	dfs(1);
	for(int i = 1;i <= n;i++)
		printf("%d ",ans[i]);
	*/
	return 0;
}
