#include<cstdio>
#include<iostream>
using namespace std;
const long long P=1e9+7;
long long f[9]={0,2,12,112,912,7136,0,0,0};
long long d[9]={0,2,3,3,0,0,0,0,0};
int power(long long x,long long y){
	if(y==0)
		return 1;
	if(y==1){
		return x;
	}
	if(y&1)
		return (1ll*power((x*x)%P,y>>1)*x)%P; 
	return (1ll*power((x*x)%P,y>>1))%P;
}
long long solve(long long x,long long y){
	if(x==y)
		return f[x];
	return (f[x]*power(d[x],y-x))%P;
}
long long ans;
long long n,m;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);

	scanf("%lld%lld",&n,&m);
	if(n<m){
		ans=solve(n,m);
	}else{
		ans=solve(m,n);
	}
	cout<<ans;
}
