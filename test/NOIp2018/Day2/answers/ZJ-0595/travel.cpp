#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
struct edge {
	int u,v;
} e[10005];
bool cmp(edge x,edge y) {
	return x.v>y.v;
}
int vet[10005],nxt[10005],head[5005],vis[5005];
int edgenum;
int n,m,u,v;
int tway[5005],ind;
void add(int u,int v) {
	vet[++edgenum]=v;
	nxt[edgenum]=head[u];
	head[u]=edgenum;
}
void dfs(int u) {
	vis[u]=1;
	tway[++ind]=u;
	for(int e=head[u]; e; e=nxt[e]) {
		int v=vet[e];
		if(!vis[v]) 
			dfs(v);
	}
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1; i<=m; i++) {
		scanf("%d%d",&u,&v);
		e[i*2-1].u=u;
		e[i*2-1].v=v;
		e[i*2].u=v;
		e[i*2].v=u;
	}
	vet[0]=1e9;
	sort(e+1,e+m+m+1,cmp);
	for(int i=1; i<=m+m; i++)
		add(e[i].u,e[i].v);
	dfs(1);
	for(int i=1;i<=ind;i++)
		printf("%d%c",tway[i],(i==ind?'\n':' '));
	return 0;
}
