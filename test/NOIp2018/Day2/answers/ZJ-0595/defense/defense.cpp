#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m;
char dt[10];
int val[100005];
int vet[200005],nxt[200005],head[100005],vis[100005];
long long dp[100005][2];
int edgenum,u,v,a,x,b,y;
void add(int u,int v) {
	vet[++edgenum]=v;
	nxt[edgenum]=head[u];
	head[u]=edgenum;
}
void solve(int u){
	vis[u]=1;
	dp[u][0]=0;
	dp[u][1]=val[u];
	for(int e=head[u];e;e=nxt[e]){
		int v=vet[e];
		if(!vis[v]){
			solve(v);
			dp[u][0]+=dp[v][1];
			dp[u][1]+=min(dp[v][1],dp[v][0]);
		}
	}
	vis[u]=0;
	if(u==a)
		dp[a][1-x]=dp[0][0];
	if(u==b)
		dp[b][1-y]=dp[0][0];
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,dt);
	for(int i=1;i<=n;i++)
		scanf("%d",&val[i]);
	for(int i=1;i<n;i++){
		scanf("%d%d",&u,&v);
		add(u,v);
		add(v,u);
	}
	for(int i=0;i<=n;i++)
		dp[i][0]=dp[i][1]=1e9+7;
	while(m--){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		solve(1);
		long long ans=min(dp[1][1],dp[1][0]);
		if(ans>=dp[0][0])
			printf("-1\n");else
			printf("%lld\n",ans);
	}
} 
