#include<bits/stdc++.h>
#define ll long long

using namespace std;

const ll mod=1e9+7;
ll n,m,ans,k[85],sz,v[3][3];
string w[85];

void dfs(int x,int y,string s,ll sm)
{
	if(x==n-1&&y==m-1)
	{
		w[++sz]=s;
		k[sz]=sm;
		return;
	}
	sm<<=1;
	string t=s;
	if(x<n-1)
	{
		s+='D';
		sm+=v[x+1][y];
		dfs(x+1,y,s,sm);
		s=t;
		sm-=v[x+1][y];
	}
	if(y<m-1)
	{
		s+='R';
		sm+=v[x][y+1];
		dfs(x,y+1,s,sm);
		s=t;
		sm-=v[x][y+1];
	}
}

void sol(int x,int y)
{
	if(x>=n)
	{
		sz=0;
		string s;
		dfs(0,0,s,v[0][0]);
		for(int i=1; i<sz; i++)
		{
			for(int j=i+1; j<=sz; j++)
			{
				if(w[i]>w[j])
				{
					if(k[i]>k[j])
					{
						return;
					}
				}
				if(w[i]<w[j])
				{
					if(k[i]<k[j])
					{
						return;
					}
				}
			}
		}
		ans++;
		ans%=mod;
		return;
	}
	if(y>=m)
	{
		sol(x+1,0);
		return;
	}
	v[x][y]=0;
	sol(x,y+1);
	v[x][y]=1;
	sol(x,y+1);
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if(n<=3&&m<=3)
	{
		sol(0,0);
		printf("%lld",ans);
		return 0;
	}
	return 0;
}
