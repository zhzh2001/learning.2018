#include<bits/stdc++.h>

using namespace std;

int read()
{
	int x=0;
	char c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c<='9'&&c>='0')x=x*10+c-'0',c=getchar();
	return x;
}

int n,m,cnt,fir[5010],nxt[10010],to[10010],fa[5010];
int sz,ans[5010],du[5010],go[1010][2];
bool vis[5010];

int add(int u,int v)
{
	to[++cnt]=v;
	nxt[cnt]=fir[u];
	fir[u]=cnt;
}

void dfs(int u)
{
	ans[++sz]=u;
	vector<int>t;
	vis[u]=1;
	for(int i=fir[u]; i; i=nxt[i])
	{
		if(to[i]==fa[u])continue;
		t.push_back(to[i]);
	}
	sort(t.begin(),t.end());
	for(int i=0; i<t.size(); i++)
	{
		int v=t[i];
		fa[v]=u;
		dfs(v);
	}
}

void bl(int x)
{
	vis[x]=1;
	for(int i=fir[x]; i; i=nxt[i])
	{
		if(to[i]==go[x][0]||vis[to[i]])continue;
		go[to[i]][0]=x;
		go[x][1]=to[i];
		bl(to[i]);
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1; i<=m; i++)
	{
		int u=read(),v=read();
		add(u,v);
		add(v,u);
		du[u]++;
		du[v]++;
	}
	bool huan=1;
	if(m!=n)huan=0;
	for(int i=1; i<=n; i++)if(du[i]!=2)huan=0;
	if(huan)
	{
		go[1][0]=to[fir[1]];
		bl(1);
		printf("1 ");
		memset(vis,0,sizeof(vis));
		vis[1]=1;
		int X=go[1][0],Y=go[1][1];
		if(X>Y)
		{
			while(X>Y)
			{
				printf("%d ",Y);
				vis[Y]=1;
				Y=go[Y][1];
			}
			while(!vis[X])
			{
				printf("%d ",X);
				vis[X]=1;
				X=go[X][0];
			}
		}
		else
		{
			while(X<Y)
			{
				printf("%d ",X);
				vis[X]=1;
				X=go[X][1];
			}
			while(!vis[Y])
			{
				printf("%d ",Y);
				vis[Y]=1;
				Y=go[Y][0];
			}
		}
		return 0;
	}
	fa[1]=0;
	dfs(1);
	for(int i=1; i<=sz; i++)printf("%d ",ans[i]);
	return 0;
}
