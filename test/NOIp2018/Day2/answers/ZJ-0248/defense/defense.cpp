#include<bits/stdc++.h>

using namespace std;

int read()
{
	int x=0;
	char c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c<='9'&&c>='0')x=x*10+c-'0',c=getchar();
	return x;
}

const int inf=0x7f7f7f7f;
int n,Q,cnt,fir[20],nxt[30],to[30],a[2010];
int A,B,X,Y,ans,f[2010][2];
char dk[3];
bool vis[20];

int add(int u,int v)
{
	to[++cnt]=v;
	nxt[cnt]=fir[u];
	fir[u]=cnt;
}

bool dfs(int u,int fa)
{
	for(int i=fir[u]; i; i=nxt[i])
	{
		int v=to[i];
		if(v==fa)continue;
		if(vis[u]==0&&vis[v]==0)return 0;
		if(!dfs(v,u))return 0;
	}
	return 1;
}

void sol(int x)
{
	if(x==n+1)
	{
		if(!dfs(1,0))return;
		int sum=0;
		for(int i=1; i<=n; i++)
		{
			if(vis[i])sum+=a[i];
		}
		if(ans==-1)ans=sum;
		ans=min(ans,sum);
		return;
	}
	if(x==A)
	{
		vis[x]=X;
		sol(x+1);
		return;
	}
	if(x==B)
	{
		vis[x]=Y;
		sol(x+1);
		return;
	}
	vis[x]=0;
	sol(x+1);
	vis[x]=1;
	sol(x+1);
}

void solve()
{
	memset(f,inf,sizeof(f));
	f[1][1]=a[1];
	f[1][0]=0;
	if(A==1)
	{
		if(X==1)f[1][0]=inf;
		if(X==0)f[1][1]=inf;
	}
	for(int i=2; i<=n; i++)
	{
		f[i][1]=min(f[i-1][1],f[i-1][0])+a[i];
		f[i][1]=min(f[i][1],inf);
		f[i][0]=f[i-1][1];
		if(i==A)
		{
			if(X==1)f[i][0]=inf;
			if(X==0)f[i][1]=inf;
		}
		if(i==B)
		{
			if(Y==1)f[i][0]=inf;
			if(Y==0)f[i][1]=inf;
		}
	}
	ans=min(f[n][1],f[n][0]);
	if(ans==inf)ans=-1;
	printf("%d\n",ans);
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&Q,dk);
	for(int i=1; i<=n; i++)a[i]=read();
	for(int i=1; i<n; i++)
	{
		int u=read(),v=read();
		if(n<=10)
		{
			add(u,v);
			add(v,u);
		}
	}
	while(Q--)
	{
		A=read(),X=read(),B=read(),Y=read();
		if(dk[0]=='A')
		{
			solve();
			continue;
		}
		if(n<=10)
		{
			ans=-1;
			sol(1);
			printf("%d\n",ans);
		}
	}
	return 0;
}
