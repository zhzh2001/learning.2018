#include<cstdio>
#define maxn 100005
template<typename T>void in(T &x) {
	char ch=getchar();
	bool flag=0;
	x=0;
	while(ch<'0'||ch>'9') flag|=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if(flag) x=-x;
	return ;
}

int f[10][1000001][2];

int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	in(n);
	in(m);
	f[1][1][1]=f[1][1][0]=1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			if(i==1&&j==1) continue;
			if(j-1==0) f[i][j-1][0]=1;
			if(i-1==0) f[i-1][j][0]=1;
			f[i][j][0]=f[i][j-1][0]*f[i-1][j][0]+f[i][j-1][1]*(f[i-1][j][0]+f[i-1][j][1]);
			f[i][j][1]=f[i][j-1][0]*f[i-1][j][0]+f[i][j-1][1]*(f[i-1][j][0]+f[i-1][j][1]);
		}
	printf("%d",f[n][n][0]+f[n][n][1]);
}


