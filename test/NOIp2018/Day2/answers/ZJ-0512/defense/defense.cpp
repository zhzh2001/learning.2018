#include<cstdio>
#include<algorithm>
#define maxn 100005
#define inf 0x3f3f3f3f
using namespace std;
template<typename T>void in(T &x) {
	char ch=getchar();
	bool flag=0;
	x=0;
	while(ch<'0'||ch>'9') flag|=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if(flag) x=-x;
	return ;
}

struct edge {
	int nxt,to;
} e[maxn<<1];

int head[maxn],cnt;

void add(int u,int v) {
	e[++cnt].to=v;
	e[cnt].nxt=head[u];
	head[u]=cnt;
	return ;
}

int p[maxn],n,m;
char pt[10];

void input() {
	in(n);
	in(m);
	scanf("%s",pt+1);
	for(int i=1; i<=n; i++)
		in(p[i]);
	for(int u,v,i=1; i<n; i++) {
		in(u);
		in(v);
		add(u,v);
		add(v,u);
	}
	return ;
}

long long dp[maxn][3]; // 0 ���� 1 ������ 2 ѡ
int x,y,a,b;

void dfs(int u,int fa) {
	if(u==a) {
		if(x==1) {
			dp[u][0]=dp[u][1]=inf;
			dp[u][2]=p[u];
			for(int v,i=head[u]; i; i=e[i].nxt) {
				v=e[i].to;
				if(v==fa) continue;
				dp[u][2]+=min(min(dp[v][0],dp[v][1]),dp[v][2]);
			}
		} else {
			dp[u][0]=dp[u][1]=0;
			dp[u][2]=inf;
			bool asd=0;
			long long tp=inf;
			for(int v,i=head[u]; i; i=e[i].nxt) {
				v=e[i].to;
				if(v==fa) continue;
				dfs(v,u);
				if(asd) {
					dp[u][0]+=min(dp[v][0],dp[v][2]);
					continue;
				}
				if(dp[v][2]<=dp[v][0]) {
					asd=1;
					dp[u][0]+=dp[v][2];
				} else {
					tp=min(tp,dp[v][2]-dp[v][0]);
					dp[u][0]+=dp[v][0];
				}

			}
			if(!asd) dp[u][1]=dp[u][0],dp[u][0]+=tp;
			else dp[u][1]=inf;
		}
	} else if(u==b) {
		if(y==1) {
			dp[u][0]=dp[u][1]=inf;
			dp[u][2]=p[u];
			for(int v,i=head[u]; i; i=e[i].nxt) {
				v=e[i].to;
				if(v==fa) continue;
				dp[u][2]+=min(min(dp[v][0],dp[v][1]),dp[v][2]);
			}
		} else {
			dp[u][0]=dp[u][1]=0;
			dp[u][2]=inf;
			bool asd=0;
			long long tp=inf;
			for(int v,i=head[u]; i; i=e[i].nxt) {
				v=e[i].to;
				if(v==fa) continue;
				dfs(v,u);
				if(asd) {
					dp[u][0]+=min(dp[v][0],dp[v][2]);
					continue;
				}
				if(dp[v][2]<=dp[v][0]) {
					asd=1;
					dp[u][0]+=dp[v][2];
				} else {
					tp=min(tp,dp[v][2]-dp[v][0]);
					dp[u][0]+=dp[v][0];
				}

			}
			if(!asd) dp[u][1]=dp[u][0],dp[u][0]+=tp;
			else dp[u][1]=inf;
		}
	} else {
		dp[u][0]=dp[u][1]=0;
		dp[u][2]=p[u];
		bool asd=0;
		long long tp=inf;
		for(int v,i=head[u]; i; i=e[i].nxt) {
			v=e[i].to;
			if(v==fa) continue;
			dfs(v,u);
			dp[u][2]+=min(min(dp[v][0],dp[v][1]),dp[v][2]);
			if(asd) {
				dp[u][0]+=min(dp[v][0],dp[v][2]);
				continue;
			}
			if(dp[v][2]<=dp[v][0]) {
				asd=1;
				dp[u][0]+=dp[v][2];
			} else {
				tp=min(tp,dp[v][2]-dp[v][0]);
				dp[u][0]+=dp[v][0];
			}
		}
		if(!asd) dp[u][1]=dp[u][0],dp[u][0]+=tp;
		else dp[u][1]=inf;
	}
}

void work() {
	for(int i=1; i<=m; i++) {
		in(a);
		in(x);
		in(b);
		in(y);
		dfs(1,0);
		long long tp=min(dp[1][0],dp[1][2]);
		if(tp>=inf) printf("-1\n");
		else printf("%lld\n",tp);
	}
}

int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	input();
	work();
}


