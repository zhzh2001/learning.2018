#include<cstdio>
#define maxn 5005
template<typename T>void in(T &x) {
	char ch=getchar();
	bool flag=0;
	x=0;
	while(ch<'0'||ch>'9') flag|=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if(flag) x=-x;
	return ;
}

struct edge {
	int nxt,to;
} e[maxn<<1];

int head[maxn],cnt;

void add(int u,int v) {
	e[++cnt].to=v;
	e[cnt].nxt=head[u];
	head[u]=cnt;
	return ;
}

int ans[maxn],tp[maxn];
int vis[maxn];
int back[maxn];
int n,m;

void input() {
	in(n);
	in(m);
	for(int u,v,i=1; i<=m; i++) {
		in(u);
		in(v);
		add(u,v);
		add(v,u);
	}
	return ;
}

void dfs(int u,int cct,bool sm) {
	if(cct==n) {
		for(int i=1;i<=n;i++)
			ans[i]=tp[i];
		return ;
	}
	for(int v,i=head[u]; i; i=e[i].nxt) {
		v=e[i].to;
		if(vis[v]) continue;
		if(!sm&&v>ans[cct+1]) continue;
		vis[v]=1;
		tp[cct+1]=v;
		back[v]=u;
		if(sm)	dfs(v,cct+1,1);
		else if(v<ans[cct+1]) dfs(v,cct+1,1);
		else dfs(v,cct+1,0);
		back[v]=0;
		vis[v]=0;
	}
	if(back[u]) dfs(back[u],cct,sm);
	return ;
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	input();
	for(int i=1; i<=n; i++)
		ans[i]=n+1;
	vis[1]=1;
	ans[1]=1;
	tp[1]=1;
	dfs(1,1,0);
	for(int i=1;i<n;i++)
		printf("%d ",ans[i]);
	printf("%d",ans[n]);
	return 0;
}





