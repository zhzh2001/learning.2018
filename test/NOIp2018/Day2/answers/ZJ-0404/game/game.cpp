#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int P=1e9+7;
int n,m;
int fast(int a,int b){
	int res=1;
	while(b){
		if(b&1)res=1ll*res*a%P;
		a=1ll*a*a%P;
		b>>=1;
	}
	return res;
}
namespace P20{
	const int N=8;
	int A[N][N],mark[N][N];
	bool check(){
		memset(mark,0,sizeof(mark));
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				if(i!=0&&j!=m-1&&A[i][j]<A[i-1][j+1])return 0;
				if(i!=0&&j!=0&&A[i-1][j]==A[i][j-1])mark[i][j]=1;
				if(i&&mark[i-1][j]||j&&mark[i][j-1])mark[i][j]=1;
				if(mark[i][j]&&i!=n-1&&j!=m-1&&A[i+1][j]!=A[i][j+1])return 0;
			}
		}
		return 1;
	}
	void solve(){
		int ans=0;
		for(int x=0;x<(1<<n*m);x++){
			for(int i=0;i<n;i++)
				for(int j=0;j<m;j++)
					A[i][j]=((x&1<<i*m+j)>0);
			ans+=check();
		}
		printf("%d\n",ans);
	}
}
namespace P50{
	void solve(){
		printf("%d\n",1ll*fast(3,m-1)*4%P);
	}
}
namespace P65{
	void solve(){
		if(m==1)puts("8");
		else if(m==2)puts("36");
		else printf("%d\n",1ll*fast(3,m-3)*112%P);
	}
}
namespace P100{
	int dp[256][9][25];
	inline void add(int &a,int b){
		a+=b;
		if(a>=P)a-=P;
	}
	bool num(int x,int i){
		return (x&1<<i)>0;
	}
	int check(int i,int k,int j){
		for(int t=1;t<n;t++)if(num(i,t)<num(j,t-1))return -1;
		for(int t=k;t<n-1;t++)if(num(i,t+1)!=num(j,t))return -1;
		for(int t=1;t<k;t++)if(num(i,t)==num(j,t-1))return t;
		return k;
	}
	void solve(){
		int mm=m;
		MIN(m,20);
		for(int i=0;i<(1<<n);i++)dp[i][n][0]=1;
		for(int now=0;now<m-1;now++){
			for(int i=0;i<(1<<n);i++){
				for(int k=1;k<=n;k++){
					if(dp[i][k][now]==0)continue;
					for(int j=0;j<(1<<n);j++){
						int res=check(i,k,j);
						if(~res)add(dp[j][res][now+1],dp[i][k][now]);
					}
				}
			}
		}
		int ans=0;
		for(int i=0;i<(1<<n);i++)
			for(int k=1;k<=n;k++)
				add(ans,dp[i][k][m-1]);
		if(mm>m)ans=1ll*ans*fast(3,mm-m)%P;
		printf("%d\n",ans);
		
	}
}
int main(){
	srand(time(NULL));
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	rd(n),rd(m);
	if(0);
	else if(n<=3&&m<=3)P20::solve();
	else if(n==2)P50::solve();
	else if(n==3)P65::solve();
	else P100::solve();
	return 0;
}
