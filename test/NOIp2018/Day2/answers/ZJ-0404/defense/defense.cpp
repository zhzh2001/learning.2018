#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=1e5+5;
int n,m,tot,head[M],to[M<<1],nxt[M<<1],A[M];
char str[15];
inline void add(int a,int b){
	to[++tot]=b;
	nxt[tot]=head[a];
	head[a]=tot;
}
namespace P20{
	const int M=2005;
	const int INF=1e9;
	int dp[M][2];
	void Solve(){
		int a,x,b,y;
		rd(a),rd(x),rd(b),rd(y);
		memset(dp,63,sizeof(dp));
		dp[1][0]=0;
		dp[1][1]=A[1];
		for(int i=1;i<n;i++){
			if(i==a)dp[i][!x]=INF;
			if(i==b)dp[i][!y]=INF;
			MIN(dp[i+1][1],dp[i][0]+A[i+1]);
			MIN(dp[i+1][1],dp[i][1]+A[i+1]);
			MIN(dp[i+1][0],dp[i][1]);
		}
		int ans=min(dp[n][0],dp[n][1]);
		if(ans>=INF)ans=-1;
		printf("%d\n",ans);
	}
	void solve(){
		for(int cas=1;cas<=m;cas++)Solve();
	}
}
int main(){
	srand(time(NULL));
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	rd(n),rd(m);scanf("%s",str);
	for(int i=1;i<=n;i++)rd(A[i]);
	for(int i=1;i<n;i++){
		int a,b;
		rd(a),rd(b);
		add(a,b),add(b,a);
	}
	if(0);
	else if(str[0]=='A'&&n<=2000)P20::solve();
	
	return 0;
}
