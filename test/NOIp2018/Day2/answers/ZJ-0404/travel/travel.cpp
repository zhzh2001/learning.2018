#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=5005;
int n,m,tot,head[M],to[M<<1],nxt[M<<1];
inline void add(int a,int b){
	to[++tot]=b;
	nxt[tot]=head[a];
	head[a]=tot;
}
namespace P60{
	vector<int>edge[M];
	int ans[M];
	void dfs(int x,int f){
		ans[++ans[0]]=x;
		for(int i=head[x];i;i=nxt[i])edge[x].push_back(to[i]);
		sort(edge[x].begin(),edge[x].end());
		for(int i=0,sz=edge[x].size();i<sz;i++){
			int y=edge[x][i];
			if(y==f)continue;
			dfs(y,x);
		}
	}
	void solve(){
		dfs(1,0);
		for(int i=1;i<=n;i++)printf("%d%c",ans[i],i==n?'\n':' ');
	}
}
namespace P100{
	vector<int>edge[M];
	int mark[M],stk[M],top,Q[M],cnt;
	void init(int x,int f){
		mark[x]=1;
		stk[++top]=x;
		for(int i=head[x];i;i=nxt[i])edge[x].push_back(to[i]);
		sort(edge[x].begin(),edge[x].end());
		for(int i=0,sz=edge[x].size();i<sz;i++){
			int y=edge[x][i];
			if(y==f)continue;
			if(mark[y]){
				if(cnt==0){
					for(int k=top;k>=1;k--){
						Q[++cnt]=stk[k];
						if(stk[k]==y)break;
					}
				}
				continue;
			}
			init(y,x);
		}
		stk[top--]=0;
	}
	int A,B,id,ans[M],res[M];
	void dfs(int x,int f){
		res[++id]=x;
		for(int i=0,sz=edge[x].size();i<sz;i++){
			int y=edge[x][i];
			if(y==f)continue;
			if(x==A&&y==B||x==B&&y==A)continue;
			dfs(y,x);
		}
	}
	void check(){
//		for(int i=1;i<=n;i++)printf("%d ",res[i]);puts(" ! ");////
		if(ans[1]==0){
			for(int i=1;i<=n;i++)ans[i]=res[i];
			return;
		}
		for(int i=1;i<=n;i++){
			if(res[i]<ans[i]){
				for(int k=1;k<=n;k++)ans[k]=res[k];
				return;
			}
			else if(res[i]>ans[i])return;
		}
	}
	void solve(){
		init(1,0);
//		for(int i=1;i<=cnt;i++)printf("%d ",Q[i]);puts("");
		Q[cnt+1]=Q[1];
		for(int i=1;i<=cnt;i++){
			A=Q[i],B=Q[i+1],id=0;
			dfs(1,0);
			check();
		}
		for(int i=1;i<=n;i++)printf("%d%c",ans[i],i==n?'\n':' ');
	}
}
int main(){
	srand(time(NULL));
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	rd(n),rd(m);
	for(int i=1;i<=m;i++){
		int a,b;
		rd(a),rd(b);
		add(a,b),add(b,a);
	}
	if(0);
	else if(m==n-1)P60::solve();
	else P100::solve();
	return 0;
}
