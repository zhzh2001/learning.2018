#include<bits/stdc++.h>
using namespace std;
inline int read () {
	int num = 0; char c = ' '; bool flag = true;
	for (; c > '9' || c < '0'; c = getchar ())
		if (c == '-') flag = false;
	for (;c >= '0' && c <= '9'; num = (num << 1) + (num << 3) + c - 48, c = getchar ());
	return flag ? num : -num;
}
const int p = 1e9 + 7;
int n, m;
namespace sub2 {
	int qpow (int a, int b) {
		long long ans = 1, now = a;
		while (b) {
			if (b & 1) {
				ans *= now;
				ans %= p;
			}
			now *= now;
			now %= p;
			b >>= 1;
		}
		return ans % p;
	}
	void work () {
		int ans = qpow (2, m + 1) + 4;
		printf ("%d\n", ans);
	}
}
namespace Zy {
	const int maxn = 9;
	const int maxm = 1000000;
	bool valid[1 << maxn][1 << maxn];
	void prework () {
		memset (valid, 1, sizeof valid);
		for (int i = 0;i < 1 << n;i ++)
			for (int j = 0;j < 1 << n;j ++)
				for (int k = 1;k < n;k ++) {
					int x = (i >> k) & 1;
					int y = (j >> (k - 1)) & 1;
					if (x < y) valid[i][j] = false;
				}
	}
	int f[1000020][10];
	void dp1 () {
		for (int i = 0;i < 1 << n;i ++)
			f[1][i] = 1;
		for (int j = 2;j <= m;j ++)
			for (int i = 0;i < 1 << n;i ++)
				for (int k = 0;k < 1 << n;k ++)
					if (valid[i][k]) {
						f[j][k] += f[j - 1][i];
						f[j][k] %= p;
					}
		int ans = 0;
		for (int i = 0;i < 1 << n;i ++) {
			ans += f[m][i];
			ans %= p;
		}
		printf ("%d\n", ans);
	}
	int g[1 << 10][10];
	void dp2 () {
		for (int i = 0;i < 1 << n;i ++)
			g[1][i] = 1;
		for (int j = 2;j <= m;j ++)
			for (int i = 0;i < 1 << n;i ++)
				for (int k = 0;k < 1 << n;k ++)
					if (valid[i][k]) {
						g[j][k] += g[j - 1][i];
						g[j][k] %= p;
					}
		int ans = 0;
		for (int i = 0;i < 1 << n;i ++) {
			ans += g[m][i];
			ans %= p;
		}
		printf ("%d\n", ans);
	}
} using namespace Zy;
int main () {
	freopen ("game.in","r",stdin);
	freopen ("game.out","w",stdout);
	n = read (); m = read ();
	if (n == 2) {
		sub2 :: work ();
		return 0;
	}
	prework ();
	if (m > 8)
		dp1 ();
	else dp2 ();
	fclose (stdin);
	fclose (stdout);
	return 0;
}
