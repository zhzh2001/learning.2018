#include<bits/stdc++.h>
using namespace std;
inline int read () {
	int num = 0; char c = ' '; bool flag = true;
	for (; c > '9' || c < '0'; c = getchar ())
		if (c == '-') flag = false;
	for (;c >= '0' && c <= '9'; num = (num << 1) + (num << 3) + c - 48, c = getchar ());
	return flag ? num : -num;
}
namespace graph {
	const int maxn = 100020;
	struct node {
		int y, next;
	} a[maxn << 1];
	int head[maxn], top = 0;
	void insert (int x, int y) {
		a[top].y = y;
		a[top].next = head[x];
		head[x] = top ++;
	}
} using namespace graph;
namespace INIT {
	int n, m, v[maxn];
	struct ques {
		int a, x, b, y;
	} q[maxn];
	char type[10];
	void init () {
		memset (head, -1, sizeof head);
		n = read (); m = read ();
		scanf ("%s", type + 1);
		for (int i = 1;i <= n;i ++)
			v[i] = read ();
		for (int i = 1;i < n;i ++) {
			int x = read ();
			int y = read ();
			insert (x, y);
			insert (y, x);
		}
		for (int i = 1;i <= m;i ++) {
			q[i].a = read ();
			q[i].x = read ();
			q[i].b = read ();
			q[i].y = read ();
		}
	}
} using namespace INIT;
namespace force {
	int h[maxn];
	int ans = 0x7fffffff;
	bool check1 (int i) {
		if (h[q[i].a] != q[i].x) return false;
		if (h[q[i].b] != q[i].y) return false;
		return true;
	}
	bool check2 () {
		for (int i = 1;i <= n;i ++)
			for (int j = head[i];j + 1;j = a[j].next) {
				int y = a[j].y;
				if (h[i] == 0 && h[y] == 0) return false;
			}
		return true;
	}
	void dfs (int x, int now, int lll) {
		if (x == n + 1) {
			if (check1 (lll) && check2 ()) {
				ans = min (ans, now);
			}
			return ;
		}
		h[x] = 1;
		dfs (x + 1, now + v[x], lll);
		h[x] = 0;
		dfs (x + 1, now, lll);
	}
} using namespace force;
namespace DP {
	int f[2020][2]; bool must[2020], forbid[2020];
	void dp (int x, int fa) {
		f[x][1] = v[x];
		if (forbid[x]) f[x][1] = 0x7fffffff;
		f[x][0] = 0;
		for (int i = head[x];i + 1;i = a[i].next) {
			int y = a[i].y;
			if (y == fa) continue;
			dp (y, x);
			f[x][1] = min (f[y][1], f[y][0]);
			f[x][0] = min (f[y][1], f[y][0]);
			if (forbid[x])
				f[x][1] = 0x7fffffff;
			if (forbid[y])
				f[x][0] = 0x7fffffff;
			if (forbid[fa])
				f[x][0] = 0x7fffffff;
			if (must[x])
				f[x][0] = 0x7fffffff;
		}
	}
} using namespace DP;
int main () {
	freopen ("defense.in","r",stdin);
	freopen ("defense.out","w",stdout);
	init ();
	if (n <= 10) {
		for (int i = 1;i <= m; i++) {
			ans = 0x7fffffff;
			dfs (1, 0, i);
			if (ans == 0x7fffffff) ans = -1;
			printf ("%d\n", ans);
		}
		return 0;
	}
	for (int i = 1;i <= m;i ++) {
		if (q[i].x) must[q[i].a] = true;
			else forbid[q[i].a] = true;
		if (q[i].y) must[q[i].b] = true;
			else forbid[q[i].b] = true;
		memset (f, 0, sizeof f);
		dp (1, 0);
		must[q[i].a] = forbid[q[i].a] = false;
		must[q[i].b] = forbid[q[i].b] = false;
		int ans = min (f[1][0], f[1][1]);
		if (ans == 0x7fffffff) ans = -1;
		printf ("%d\n", ans);
	}
	fclose (stdin);
	fclose (stdout);
	return 0;
}
