#include<bits/stdc++.h>
using namespace std;
inline int read () {
	int num = 0; char c = ' '; bool flag = true;
	for (; c > '9' || c < '0'; c = getchar ())
		if (c == '-') flag = false;
	for (;c >= '0' && c <= '9'; num = (num << 1) + (num << 3) + c - 48, c = getchar ());
	return flag ? num : -num;
}
namespace Graph {
	const int maxn = 5020;
	struct node {
		int y, next;
	} a[maxn];
	int head[maxn], top = 0;
	void insert (int x, int y) {
		a[top].y = y;
		a[top].next = head[x];
		head[x] = top ++;
	}
} using namespace Graph;
namespace INIT {
	bool flag__ = true;
	int n, m, deg[maxn];
	void init () {
		memset (head, -1, sizeof head);
		n = read (); m = read ();
		for (int i = 1;i <= m;i ++) {
			int x = read ();
			int y = read ();
			insert (x, y);
			insert (y, x);
			deg[x] ++; deg[y] ++;
			if (deg[x] > 2 || deg[y] > 2) flag__ = false;
		}
	}
} using namespace INIT;
namespace Find {
	bool On[maxn], vis[maxn], in_sta[maxn], have;
	int sta[maxn], top = 0;
	void dfs (int x, int fa) {
		vis[x] = in_sta[x] = true;
		for (int i = head[x];i + 1;i = a[i].next) {
			int y = a[i].y;
			if (!vis[y]) dfs (y, x);
			if (in_sta[y] && y != fa) {
				int u = sta[top];
				while (u != y) {
					On[u] = true;
					u = sta[-- top];
				}
				On[y] = true;
			}
		}
		top --; in_sta[x] = false;
	}
} using namespace Find;
namespace Tree {
	struct QX {
		int l, r;
	} q[maxn];
	int p[maxn], fa[maxn], num;
	bool vis[maxn];
	void work () {
		memset (vis, 0, sizeof vis);
		q[fa[0]].l = q[fa[0]].r = 1;
		p[++ num] = 1; fa[1] = 0;
		for (int i = 1;i <= n;i ++) {
			int x = 0x7fffffff;
			for (int j = q[fa[p[num]]].l;j <= q[fa[p[num]]].r;j ++)
				if (!vis[p[j]]) x = min (x, p[j]);
			while (x == 0x7fffffff) {
				num = q[fa[p[num]]].l - 1;
				for (int j = q[fa[p[num]]].l;j <= q[fa[p[num]]].r;j ++)
				if (!vis[p[j]]) x = min (x, p[j]);
			}
			vis[x] = true;
			printf ("%d", x); if (i == n) printf ("\n"); else printf (" ");
			q[x].l = num + 1;
			for (int j = head[x];j + 1;j = a[j].next) {
				int y = a[j].y; 
				if (!vis[y]) p[++ num] = y, fa[y] = x;
			}
			q[x].r = num;
		}
	}
} using namespace Tree;
namespace another {
	void work () {
		bool vis[maxn];
		int x = 0, pre;
		for (int i = 1;i <= n;i ++) {
			x = 0x7fffffff;
			if (i == 1) x = 1;
			else  {
				for (int j = head[pre];j + 1;j = a[j].next) {
					int y = a[j].y;
					if (!vis[y]) x = min (x, y);
				}
			}
			vis[x] = true;
			printf ("%d", x);
			if (i != n) printf (" ");
			pre = x;
		}
		printf ("\n");
	}
} using namespace another;
int main () {
	freopen ("travel.in","r",stdin);
	freopen ("travel.out","w",stdout);
	init ();
	/* have = false;
	memset (Find :: vis, 0, sizeof Find :: vis);
	memset (in_sta, 0, sizeof in_sta);
	memset (On, 0, sizeof On);
	dfs (1, 0);*/
	if (m == n - 1) {
		Tree :: work ();
		return 0;
	}
		another :: work ();
	fclose (stdin);
	fclose (stdout);
	return 0;
}
