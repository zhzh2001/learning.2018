#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>
using namespace std;

const int N = 5005;
const int inf = 1 << 30;

int n, m, cnt = 0, ans[N];
vector <int> e[N];

inline void read(int &X) {
	X = 0; char ch = 0; int op = 1;
	for(; ch > '9' || ch < '0'; ch = getchar())
		if(ch == '-') op = -1;
	for(; ch >= '0' && ch <= '9'; ch = getchar())
		X = (X << 3) + (X << 1) + ch - 48;
	X *= op;
}

namespace Solve1 {
	
	void dfs(int x, int fat) {
		int vecSiz = e[x].size();
		for(int i = 0; i < vecSiz; i++) {
			int y = e[x][i];
			if(y == fat) continue;
			ans[++cnt] = y;
			dfs(y, x);
		}
	}
	
	void work() {		
		ans[++cnt] = 1;
		dfs(1, 0);
		
		for(int i = 1; i <= n; i++) {
			printf("%d", ans[i]);
			if(i == n) putchar('\n');
			else putchar(' ');
		}
	}
	
}

namespace Solve2 {
	int top, stk[N], sum, cir[N], dx, dy, res[N];
	bool inc[N], vis[N];
	
	void getCir(int x, int fat) {
//		int pre = top;
		stk[++top] = x, vis[x] = 1;
		int vecSiz = e[x].size();
		for(int i = 0; i < vecSiz; i++) {
			int y = e[x][i];
			if(y == fat) continue;
			if(vis[y]) {
				sum = 0;
				for(; stk[top] != y; --top) {
					inc[stk[top]] = 1;
					vis[stk[top]] = 0;
					cir[++sum] = stk[top];
				}
				inc[y] = 1, cir[++sum] = y, vis[y] = 0, top--;
			} else getCir(y, x);
		}
//		top = pre;
		if(vis[x]) --top, vis[x] = 0;
	}
	
	inline bool bet() {
		for(int i = 1; i <= n; i++)
			if(res[i] != ans[i]) return res[i] < ans[i];
		return 0;
	}
	
	void dfs(int x, int fat) {
		int vecSiz = e[x].size();
		for(int i = 0; i < vecSiz; i++) {
			int y = e[x][i];
			if(y == fat) continue;
			if((x == dx && y == dy) || (x == dy && y == dx)) continue;
			res[++cnt] = y;
			dfs(y, x);
		}
	}
	
	void work() {
		top = 0;
		getCir(1, 0);
		
/*		for(int i = 1; i <= sum; i++)
			printf("%d ", cir[i]);
		printf("\n");   */
		
		for(int i = 1; i <= n; i++) ans[i] = inf;
		
		for(int i = 1; i < sum; i++) {
			dx = cir[i], dy = cir[i + 1];
			res[cnt = 1] = 1;
			dfs(1, 0);
			if(bet()) {
				for(int j = 1; j <= n; j++)
					ans[j] = res[j];
			}
		}
		dx = cir[1], dy = cir[sum];
		res[cnt = 1] = 1;
		dfs(1, 0);
		if(bet()) {
			for(int j = 1; j <= n; j++)
				ans[j] = res[j];
		}
		
		for(int i = 1; i <= n; i++) {
			printf("%d", ans[i]);
			if(i == n) putchar('\n');
			else putchar(' ');
		}
	}
	
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	
	read(n), read(m);
	for(int x, y, i = 1; i <= m; i++) {
		read(x), read(y);
		e[x].push_back(y), e[y].push_back(x);
	}
	
	for(int i = 1; i <= n; i++) 
		sort(e[i].begin(), e[i].end());
	
	if(m == n - 1) Solve1 :: work();
	else Solve2 :: work();
	
	return 0;
}
