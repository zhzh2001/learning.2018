#include <cstdio>
#include <cstring>
#include <cstdlib>
using namespace std;
typedef long long ll;

const int N = 1e6 + 5;
const int P = 1e9 + 7;
const int inf = 1 << 30;

int n, m, ans[5][5];
int f[N][4];

inline void inc(int &x, int y) {
	x += y;
	if(x >= P) x -= P;
}

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	
	scanf("%d%d", &n, &m);
	if(n <= 3 && m <= 3) {
		ans[1][1] = 2, ans[1][2] = 4, ans[1][3] = 8;
		ans[2][1] = 4, ans[2][2] = 12, ans[2][3] = 36;
		ans[3][1] = 8, ans[3][2] = 36, ans[3][3] = 112;
		printf("%d\n", ans[n][m]);
		return 0;
	}  
	f[1][0] = f[1][1] = f[1][2] = f[1][3] = 1;
	for(int i = 2; i <= m; i++) {
		inc(f[i][0], f[i - 1][0]), inc(f[i][0], f[i - 1][1]);
		inc(f[i][1], f[i - 1][0]), inc(f[i][1], f[i - 1][1]);
		inc(f[i][1], f[i - 1][2]), inc(f[i][1], f[i - 1][3]);
		inc(f[i][2], f[i - 1][0]), inc(f[i][2], f[i - 1][1]);
		inc(f[i][3], f[i - 1][0]), inc(f[i][3], f[i - 1][1]);
		inc(f[i][3], f[i - 1][2]), inc(f[i][3], f[i - 1][3]);
	}
	int a = 0;
	inc(a, f[m][0]), inc(a, f[m][1]), inc(a, f[m][2]), inc(a, f[m][3]);
	printf("%d\n", a);
	return 0;
}
