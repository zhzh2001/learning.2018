#include <cstdio>
#include <cstring>
using namespace std;
typedef long long ll;

const int N = 2005;
const ll inf = 1LL << 60;

int n, qn, tot = 0, head[N], fa[N], type[N];
ll a[N], f[N][2], g[N];
bool ex[N][N];

struct Edge {
	int to, nxt;
} e[N << 1];

inline void add(int from, int to) {
	e[++tot].to = to;
	e[tot].nxt = head[from];
	head[from] = tot;
}

template <typename T>
inline void read(T &X) {
	X = 0; char ch = 0; T op = 1;
	for(; ch > '9' || ch < '0'; ch = getchar())
		if(ch == '-') op = -1;
	for(; ch >= '0' && ch <= '9'; ch = getchar())
		X = (X << 3) + (X << 1) + ch - 48;
	X *= op;
}

inline ll min(ll x, ll y) {
	return x > y ? y : x;
}

template <typename T>
inline void chkMin(T &x, T y) {
	if(y < x) x = y;
}

void preDfs(int x, int fat) {
	fa[x] = fat;
	for(int i = head[x]; i; i = e[i].nxt) {
		int y = e[i].to;
		if(y == fat) continue;
		preDfs(y, x);
	}
}

void dfs(int x, int fat) {
	f[x][0] = 0LL, f[x][1] = a[x], g[x] = inf;
	for(int i = head[x]; i; i = e[i].nxt) {
		int y = e[i].to;
		if(y == fat) continue;
		dfs(y, x);
		f[x][0] += f[y][1], f[x][1] += g[y];
	}
	if(type[x] != -1) g[x] = f[x][type[x]], type[x] = -1;
	else g[x] = min(f[x][0], f[x][1]);
}

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
		
	read(n), read(qn);
	char typ[5]; scanf("%s", typ + 1);
	for(int i = 1; i <= n; i++) read(a[i]);
	for(int x, y, i = 1; i < n; i++) {
		read(x), read(y);
		add(x, y), add(y, x);
		ex[x][y] = ex[y][x] = 1;
	}
	
	preDfs(1, 0);
	
	for(int i = 1; i <= n; i++) type[i] = -1;
	for(int posx, posy, tx, ty; qn--; ) {
		read(posx), read(tx), read(posy), read(ty);
/*		for(int i = 1; i <= n; i++) 
			f[i][0] = f[i][1] = 0, g[i] = inf;    */
		if(ex[posx][posy] && !tx && !ty) puts("-1");
		else {
			type[posx] = tx, type[posy] = ty;
			if(!tx && fa[posx]) type[fa[posx]] = 1; 
			if(!ty && fa[posy]) type[fa[posy]] = 1;
			dfs(1, 0);
			printf("%lld\n", g[1]);
		}
	}
	return 0;
}
