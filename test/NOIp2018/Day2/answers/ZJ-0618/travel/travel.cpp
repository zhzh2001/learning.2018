#include<bits/stdc++.h>
using namespace std;
#define fo(i,a,b) for(i=(a);i<=(b);++i)
#define fd(i,a,b) for(i=(a);i>=(b);--i)
#define out(i,v,now) for(LL i=v.be[now],to=v.v[i];~i;i=v.ne[i],to=v.v[i])
#define FP freopen
#define PF printf
#define SF scanf
typedef long long LL;
const LL maxn=5100;
struct adj_list{
	LL be[maxn],ne[maxn<<1],v[maxn<<1],cnt;
	void init(){
		memset(be,-1,sizeof(be)); cnt=0;
	}
	void add(LL x,LL y){
		ne[cnt]=be[x];
		be[x]=cnt;
		v[cnt]=y;
		++cnt;
	}
}v;
vector<LL> vec[maxn];
LL cyc[maxn],num,ans[maxn<<1],n,m,vis[maxn],sta[maxn],top,len,cant[maxn],insta[maxn];
LL dfs(LL now,LL fa,LL flag){
	++len;
	if (now>ans[len] && !flag) flag=-1;
	if (now<ans[len] && !flag) flag=1;
	if (flag==1) ans[len]=now;
	out(i,v,now){
		if (to!=fa && cant[now]!=to){
			flag=dfs(to,now,flag);
		}
	}
	return flag;
}
void pre(LL now,LL fa){
	vis[now]=1;
	out(i,v,now)
	{
		if (to!=fa){
			if (!vis[to]) pre(to,now);
		}
		vec[now].push_back(to);
	}
	sort(vec[now].begin(),vec[now].end());
}
void find_cyc(LL now,LL in,LL fa){
	sta[++top]=now; insta[now]=1;
	vis[now]=1;
	out(i,v,now) if (to!=fa){
		if (!vis[to]) find_cyc(to,i,now);
		else if (insta[to]){
			LL temp=top;
			while (sta[temp+1]!=to){
				cyc[num++]=sta[temp];
				--temp;
			}
		}
	}
	--top; insta[now]=0;
}
void pri(){
	LL i;
	fo(i,1,n) PF("%lld ",ans[i]);
}
int main(){
	FP("travel.in","r",stdin);
	FP("travel.out","w",stdout);
	LL i,x,y;
	memset(ans,0x3f,sizeof(ans)); v.init();
	SF("%lld%lld",&n,&m);
	fo(i,1,m){
		SF("%lld%lld",&x,&y);
		v.add(x,y); v.add(y,x);
	}
	pre(1,-1);
	LL j;
	v.init();
	fo(i,1,n) fd(j,(LL)vec[i].size()-1,0){
		v.add(i,vec[i][j]);
//		cerr<<i<<' '<<vec[i][j]<<endl;
	}
	if (m==n-1){
		len=0;
		memset(vis,0,sizeof(vis));
		dfs(1,-1,0);
		pri();
	}
	else{
		memset(vis,0,sizeof(vis));
		find_cyc(1,-1,-1);
		fo(i,0,num-1){
			len=0;
			cant[cyc[i]]=cyc[(i+1)%num];
			cant[cyc[(i+1)%num]]=cyc[i];
			dfs(1,-1,0);
			cant[cyc[i]]=0; cant[cyc[(i+1)%num]]=0;
		}
		pri();
	}
	return 0;
}
