#include<bits/stdc++.h>
using namespace std;
#define fo(i,a,b) for(i=(a);i<=(b);++i)
#define fd(i,a,b) for(i=(a);i>=(b);--i)
#define out(i,v,now) for(LL i=v.be[now],to=v.v[i];~i;i=v.ne[i],to=v.v[i])
#define FP freopen
#define PF printf
#define SF scanf
typedef long long LL;
const LL maxn=6;
LL a[maxn][maxn],ans,n,m,cnt;
string w[1<<10],s[1<<10],noww,nows;
void get(LL x,LL y,LL len){
	if (x==n-1 && y==m-1){
		w[cnt]=noww; s[cnt++]=nows;
		return;
	}
	if (y<m-1){
		noww[len]='R'; nows[len]=a[x][y+1]+'0';
		get(x,y+1,len+1);
	}
	if (x<n-1){
		noww[len]='D'; nows[len]=a[x+1][y]+'0';
		get(x+1,y,len+1);
	}
}
LL check(){
	noww=nows="     "; cnt=0;
	get(0,0,0);
	LL i,j;
//	fo(i,0,cnt-1) cerr<<w[i]<<' '<<s[i]<<endl;
	fo(i,0,cnt-1) fo(j,0,cnt-1) if (s[i]>s[j] && w[i]>w[j]) return 0;
	return 1;
}
void ss(LL x,LL y){
	if (x==n){
		if (check()) ++ans;
		return;
	}
	a[x][y]=0;
	ss(x+((y+1)/m),(y+1)%m);
	a[x][y]=1;
	ss(x+((y+1)/m),(y+1)%m);
}
int main(){
	FP("game.in","r",stdin);
	FP("game.out","w",stdout);
	SF("%lld%lld",&n,&m);
	ss(0,0);
	PF("%lld",ans);
	return 0;
}
