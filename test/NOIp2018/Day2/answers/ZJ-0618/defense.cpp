#include<bits/stdc++.h>
using namespace std;
#define fo(i,a,b) for(i=(a);i<=(b);++i)
#define fd(i,a,b) for(i=(a);i>=(b);--i)
#define out(i,v,now) for(LL i=v.be[now],to=v.v[i];~i;i=v.ne[i],to=v.v[i])
#define FP freopen
#define PF printf
#define SF scanf
typedef long long LL;
const LL maxn=101000;
LL n,m,p[maxn],c[maxn];
string s;
struct adj_list{
	LL be[maxn],ne[maxn<<1],v[maxn<<1],cnt;
	void init(){
		memset(be,-1,sizeof(be)); cnt=0;
	}
	void add(LL x,LL y){
		ne[cnt]=be[x];
		be[x]=cnt;
		v[cnt]=y;
		++cnt;
	}
}v;
namespace task1{
	LL f[5100][2];
	void dfs(LL now,LL fa){
		f[now][0]=f[now][1]=INT_MAX;
		out(i,v,now) if (to!=fa) dfs(to,now);
		if (~c[now]){
			f[now][1]=p[now];
			out(i,v,now)
			if (to!=fa) f[now][1]+=min(f[to][0],f[to][1]);
		}
		if (c[now]!=1){
			f[now][0]=0;
			out(i,v,now)
			if (to!=fa) f[now][0]+=f[to][1];
		}
	}
	void solve(){
		v.init();
		LL x,y,i,a,b;
		fo(i,2,n){
			SF("%lld%lld",&x,&y);
			v.add(x,y); v.add(y,x);
		}
		fo(i,1,m){
			SF("%lld%lld%lld%lld",&a,&x,&b,&y);
			if (x==0) c[a]=-1; else c[a]=1;
			if (y==0) c[b]=-1; else c[b]=1;
			dfs(1,-1);
			LL ans=min(f[1][1],f[1][0]);
			if (ans<INT_MAX) PF("%lld\n",ans);
			else PF("-1\n");
			c[a]=0; c[b]=0;
		}
	}
}
namespace task2{
	LL f[maxn][2],father[maxn];
	void pre(LL now,LL fa){
		f[now][0]=f[now][1]=INT_MAX;
		father[now]=fa;
		out(i,v,now) if (to!=fa) pre(to,now);
		if (~c[now]){
			f[now][1]=p[now];
			out(i,v,now)
			if (to!=fa) f[now][1]+=min(f[to][0],f[to][1]);
		}
		if (c[now]!=1){
			f[now][0]=0;
			out(i,v,now)
			if (to!=fa) f[now][0]+=f[to][1];
		}
	}
	void dfs(LL now){
		f[now][0]=f[now][1]=INT_MAX;
		if (~c[now]){
			f[now][1]=p[now];
			out(i,v,now)
			if (to!=father[now]) f[now][1]+=min(f[to][0],f[to][1]);
		}
		if (c[now]!=1){
			f[now][0]=0;
			out(i,v,now)
			if (to!=father[now]) f[now][0]+=f[to][1];
		}
		if (~father[now]) dfs(father[now]);
	}
	void solve(){
		v.init();
		LL x,y,i,a,b;
		fo(i,2,n){
			SF("%lld%lld",&x,&y);
			v.add(x,y); v.add(y,x);
		}
		pre(1,-1);
		fo(i,1,m){
			SF("%lld%lld%lld%lld",&a,&x,&b,&y);
			if (x==0) c[a]=-1; else c[a]=1;
			if (y==0) c[b]=-1; else c[b]=1;
			dfs(a); dfs(b);
			LL ans=min(f[1][1],f[1][0]);
			if (ans<INT_MAX) PF("%lld\n",ans);
			else PF("-1\n");
			c[a]=0; c[b]=0;
			dfs(a); dfs(b);
		}
	}
}
/*namespace task3{
	struct node{
		LL l[2],r[2];
		node operator +(const )
	};
	void solve(){
		
	}
}*/
int main(){
	FP("defense.in","r",stdin);
	FP("defense.out","w",stdout);
	cin>>n>>m>>s;
	LL i;
	fo(i,1,n) SF("%lld",&p[i]);
	if (n<=5000) task1::solve();
	else if (s[0]=='B') task2::solve();
//	else if (s[0]=='C') task3::solve();
	return 0;
}
