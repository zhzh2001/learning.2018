#include<iostream>
#include<cstring>
#include<cstdio>
#define N 5005
using namespace std;

int head[N],tot=0,cnt=1,ans[N],anc[N],n,m;
bool visit[N];

struct Edge{
	int to,next;
}edge[N<<1];

int Find(int x){ (anc[x]==x)?x:Find(anc[x]); }
void Union(int x,int y){ anc[y]=x; }

void add(int u,int v){
	edge[++tot].to=v;
	edge[tot].next=head[u];
	head[u]=tot;
}

void dfs(int u){
	visit[u]=true;
	for (int i=head[u];i;i=edge[i].next){
		int v=edge[i].to;
		if (!visit[v]) Union(u,v);
	}
	for (int i=u+1;i<=n;i++){
		if (visit[i]) continue;
		if (Find(i)==1&&anc[i]!=u&&anc[u]!=i&&anc[i]!=anc[u]){
			ans[cnt--]=0;
			visit[u]=false;
			for (int j=head[u];j;j=edge[j].next){
				int v=edge[j].to;
				if (!visit[v]) anc[v]=v;
			}
			return;
		}
	}
		
	for (int i=1;i<=n;i++){
		if (Find(i)==1&&!visit[i]) ans[++cnt]=i,dfs(i);
	}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	int x,y;
	for (int i=1;i<=n;i++) anc[i]=i;
	for (int i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		add(x,y); add(y,x);
	}
	memset(ans,127,sizeof(ans));
	ans[1]=1;
	dfs(1);
	for (int i=1;i<=n;i++) printf("%d ",ans[i]);
}
