#include<iostream>
#include<cstring>
#include<cstdio>
#define Mod 1000000007
using namespace std;

int n,m;
long long ans=1;

int power(long long x,int k){
	long long s=1;
	while (k){
		if (k&1) s=(s*x)%Mod;
		x=(x*x)%Mod; k>>=1;
	}
	return s;
}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	for (int i=1;i<n;i++) ans=ans*(i+1)%Mod;
	ans=(ans*ans)%Mod;
	ans=(ans*power(n+1,m-n+1))%Mod;
	for (int i=2;i<n;i++){
		ans=(ans-ans/(i+1)+ans/(power(i+1,2))+Mod)%Mod;
	}
	printf("%lld\n",ans);
	return 0;
}
