#include<bits/stdc++.h>
#define For(i,a,b) for(int i=(a),_en=(b);i<=_en;i++)
#define Tor(i,x) for(int i=head[x];i;i=to[i].nx)
#define M 1000005
using namespace std;
int head[M],tot,n,m;
int v[M];
char op[3];
struct name{int id,nx;}to[M<<1];
void add(int x,int y){to[++tot]=(name){y,head[x]};head[x]=tot;}
struct P55{
	int a,A,b,B,dp[M][2];
	void dfs(int x,int f){
		dp[x][0]=dp[x][1]=0;
		if(x==a)dp[x][A^1]=1e9;
		if(x==b)dp[x][B^1]=1e9;
		Tor(i,x){
			int y=to[i].id;
			if(y==f)continue;
			dfs(y,x);
			dp[x][1]+=min(dp[y][0],dp[y][1]);
			dp[x][0]+=dp[y][1];
		}dp[x][1]+=v[x];
	}
	bool ck(){
		if(A||B)return 0;
		Tor(i,a)if(to[i].id==b)return 1;
		return 0;
	}
	void solve(){
		while(m--){
			scanf("%d%d%d%d",&a,&A,&b,&B);
			if(ck()){puts("-1");continue;}dfs(1,0);
			printf("%d\n",min(dp[1][0],dp[1][1]));
		}
	}
}p55;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,op+1);
	For(i,1,n)scanf("%d",&v[i]);
	For(i,1,n-1){
		int x,y;scanf("%d%d",&x,&y);
		add(x,y);add(y,x);
	}
//	if(n<=2000&&m<=2000)
	p55.solve();
	return 0;
}
