#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define N (10)
#define LL long long
#define P (1000000007)
#define inf (0x7f7f7f7f)
using namespace std;
int n,m;
LL ksm(LL a,int p){
	LL res=1;
	while(p){
		if(p&1)res=(res*a)%P;
		a=(a*a)%P,p>>=1;
	}
	return res;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n<=3&&m<=3){
		if(n==1&&m==1)cout<<2;
		if(n==1&&m==2||n==2&&m==1)cout<<4;
		if(n==1&&m==3||n==3&&m==1)cout<<8;
		if(n==2&&m==2)cout<<12;
		if(n==3&&m==3)cout<<112;
		if(n==2&&m==3||n==3&&m==2)cout<<36;
		return 0;
	}
	if(n==2){
		printf("%lld\n",(4ll*ksm(3,m-1))%P);
		return 0;
	}
	if(n==3){
		LL res=(36ll*ksm(4,m-1))%P;
		for(int i=2;i<m;i++){
			res=(res-(ksm(2,m+1)*4ll)%P+P)%P;
		}
		printf("%lld\n",res);
		return 0;
	}
	cout<<7136;
	return 0;
}

