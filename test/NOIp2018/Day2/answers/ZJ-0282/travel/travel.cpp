#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define N (5010)
#define LL long long
#define inf (0x7f7f7f7f)
using namespace std;
int n,m,fi[N],ne[N<<1],b[N<<1],E;
int c[N][N],ind,a[N],ans[N],q[N],ru[N],pp[N],ck[N],cir[N],sum;
bool vis[N];
void add(int x,int y){
	ne[++E]=fi[x],fi[x]=E,b[E]=y,ru[y]++;
}
void dfs(int u,int pre){
	int cnt=0; ans[++ind]=u;
	for(int i=fi[u];i;i=ne[i]){
		int v=b[i];
		if(v==pre)continue;
		c[u][++cnt]=v;
	}
	sort(c[u]+1,c[u]+cnt+1);
	for(int i=1;i<=cnt;i++)
	dfs(c[u][i],u);
}
void dfs1(int u,int pre){
	//cout<<u<<" "<<sum<<endl;
	cir[++sum]=u,vis[u]=1;
	for(int i=fi[u];i;i=ne[i]){
		int v=b[i];
		if(vis[v]||v==pre)continue;
		dfs1(v,u);
	}
}
void check(int u,int pre,int L,int R){
	int cnt=0; ck[++ind]=u;
	//cout<<u<<" "<<L<<" "<<R<<endl;
	for(int i=fi[u];i;i=ne[i]){
		int v=b[i];
		if(v==pre)continue;
		if(v==L&&u==R||v==R&&u==L)continue;
		c[u][++cnt]=v;
	}
	sort(c[u]+1,c[u]+cnt+1);
	for(int i=1;i<=cnt;i++)
	check(c[u][i],u,L,R);
}
bool min_(){
	for(int i=1;i<=n;i++)
	if(ck[i]<ans[i])return 1;
	else if(ck[i]>ans[i])return 0;
	return 0;
}
void topsort(){
	int h=0,t=0;
	for(int i=1;i<=n;i++)
	if(ru[i]==1)q[++t]=i,vis[i]=1;
	while(h<t){
		int u=q[++h];
		for(int i=fi[u];i;i=ne[i]){
			int v=b[i];
			if(vis[v])continue;
			ru[v]--;
			if(ru[v]<=1&&!vis[v])q[++t]=v,vis[v]=1;
		}
	}
	for(int i=1;i<=n;i++)
	if(!vis[i]){
		dfs1(i,0);
		//cout<<sum<<endl;
		break;
	}
	//for(int i=1;i<=sum;i++)
	//cout<<i<<" "<<cir[i]<<" ";
	//return;
	memset(ans,inf,sizeof(ans));
	for(int i=1;i<sum;i++){
		ind=0; //cout<<cir[i]<<" "<<cir[i+1]<<endl;
		check(1,0,cir[i],cir[i+1]);
		if(min_()){
			for(int j=1;j<=n;j++)ans[j]=ck[j];
		}
	}
	check(1,0,cir[sum],cir[1]);
	if(min_())
	for(int j=1;j<=n;j++)ans[j]=ck[j];
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y),add(y,x);
	}
	if(m==n-1){
		dfs(1,0);
		for(int i=1;i<=n;i++)printf("%d ",ans[i]);
		return 0;
	}
	topsort();
	for(int i=1;i<=n;i++)printf("%d ",ans[i]);
	return 0;
}
