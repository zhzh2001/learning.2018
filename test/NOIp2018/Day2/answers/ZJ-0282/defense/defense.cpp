#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define N (100010)
#define P (1000000007)
#define LL long long
#define inf (0x7f7f7f)
using namespace std;
int n,m,fi[N],ne[N<<1],b[N<<1],E,w[N],fa[N];
LL f[N][2],g[N][2];
bool ch[N];
bool vis[N]; char str[10];
void add(int x,int y){
	ne[++E]=fi[x],fi[x]=E,b[E]=y;
}
void dp(int u,int pre){
	if(f[u][1]!=inf)f[u][1]=w[u];
	if(f[u][0]!=inf)f[u][0]=0;
	for(int i=fi[u];i;i=ne[i]){
		int v=b[i];
		if(v==pre)continue;
		fa[v]=u,dp(v,u);
		(f[u][0]+=f[v][1]);
		if(f[u][0]!=inf)f[u][0]%=P;
		f[u][1]+=min(f[v][0],f[v][1]);
		if(f[u][1]!=inf)f[u][1]%=P;
	}
	//cout<<u<<" "<<f[u][0]<<" "<<f[u][1]<<endl;
}
void fg(){
	for(int i=1;i<=n;i++)
	f[i][0]=g[i][0],f[i][1]=g[i][1];
}
void dfs2(int u){
	bool p=ch[u];
	for(int i=fi[u];i;i=ne[i]){
		int v=b[i];
		if(v==fa[u])continue;
		if(!p)ch[v]=1;
		else if(f[v][0]<f[v][1])ch[v]=0;else ch[v]=1;
		dfs2(v);
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,str+1);
	//puts("fuck");
	for(int i=1;i<=n;i++)scanf("%d",&w[i]);
	for(int i=1;i<n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y),add(y,x);
	}
	dp(1,0);
	//for(int i=1;i<=n;i++)cout<<fa[i]<<" "; puts("");
	for(int i=1;i<=n;i++)g[i][0]=f[i][0],g[i][1]=f[i][1];
	if(n<=3000){
		while(m--){
			int u,v,op1,op2;
			scanf("%d%d%d%d",&u,&op1,&v,&op2);
			//if(u>v)swap(u,v),swap(op1,op2);
			if(fa[u]==v||fa[v]==u){
				if(!op1&&!op2){
					puts("-1"); continue;
				}
			}
			//memset(f,0,sizeof(f));
			f[u][op1^1]=inf,f[v][op2^1]=inf;
			dp(1,0);
			printf("%lld\n",min(f[1][0],f[1][1]));
			fg();
		}
		return 0;
	}
	
	if(f[1][1]<f[1][0])ch[1]=1;else ch[1]=0;
	dfs2(1);
	for(int i=1;i<=n;i++)printf("%d ",ch[i]);
	while(m--){
		int u,v,op1,op2;
		scanf("%d%d%d%d",&u,&op1,&v,&op2);
		if(u>v)swap(u,v),swap(op1,op2);
		if(fa[u]==v||fa[v]==u){
			if(!op1&&!op2){
				puts("-1"); continue;
			}
		}
		//cout<<u<<" "<<v<<endl;
		LL inc=0;
		if(op1==0){
			int th=fa[u];
			if(!ch[th])
			inc+=(f[th][1]-f[th][0]+P);
		}
		else{
			if(!ch[u])
			inc+=(f[u][1]-f[u][0]+P);
		}
		if(op2==0){
			int th=fa[v];
			if(!ch[th])
			inc+=(f[th][1]-f[th][0]+P);
		}
		else{
			if(!ch[v])
			inc+=(f[v][1]-f[v][0]+P);
		}
		inc%=P;
		//cout<<inc<<endl;
		printf("%lld\n",(min(f[1][0],f[1][1])+inc)%P);
	}
	return 0;
}

