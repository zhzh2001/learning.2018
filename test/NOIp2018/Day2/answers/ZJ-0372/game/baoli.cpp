#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);puts("");
}
const ll N=500,mod=1e9+7;
ll n,m,all,total,ans,mp[1000][10],rns,cnt;
struct juc{ll m[N][N];}a,b,c;
juc operator *(juc a,juc b){
	For(i,1,cnt)For(j,1,cnt)c.m[i][j]=0;
	For(i,1,cnt)For(j,1,cnt)For(k,1,cnt)
	Add(c.m[i][k],a.m[i][j]*b.m[j][k]);
	return c;
}
ll ppow(ll x,ll k){
	ll ans=1;
	for(;k;k>>=1,Mul(x,x))if (k&1)Mul(ans,x);
	return ans;
}
void merge(ll a,ll B,ll c,ll d){
	if (!mp[a][B])mp[a][B]=++cnt;
	if (!mp[c][d])mp[c][d]=++cnt;
	b.m[mp[c][d]][mp[a][B]]=1;
}
int main(){
	n=read(),m=read();
	if (n==1||m==1)return writeln(ppow(2,n*m)),0;
	all=(1<<(n-1));
		rep(i,0,all)rep(j,0,all)For(A,0,n-2)if (((j&i)==j)){
			ll frog=1,to=A;
			For(w,0,n-2){
				ll a=i>>w&1,b=j>>w&1;
				if (w>A&&a!=b){frog=0;	break;	}
				if (a==b)Min(to,w);
			}
			if (frog){
				merge(all/2+j/2,to,i,A);
				merge(j/2,to,i,A);
			}
		}
	rep(i,0,all)
		a.m[1][mp[i][n-2]]=2;
	for(--m;m;m>>=1,b=b*b)if (m&1)a=a*b;
	For(i,1,cnt)Add(ans,a.m[1][i]);
	writeln(ans);
}
