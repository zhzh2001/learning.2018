#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);puts("");
}
const ll N=200,mod=1e9+7;
ll n,m,all,ans,a[N][N];
void dfs(ll x,ll y){
	if (y==-1)--x,y=n-1;
	if (x==-1){
		++ans;
		return;
	}
	
	if (x<n-1&&y<n-1){
		if (a[x][y+1]>a[x+1][y])return;
		if (a[x][y+1]==a[x+1][y]){
			For(i,x+1,n-2)For(j,y+1,n-2)
			if (a[i+1][j]!=a[i][j+1])return;
		}
	}
	
	a[x][y]=0;
	dfs(x,y-1);
	a[x][y]=1;
	dfs(x,y-1);
}
int main(){
	n=read();
	dfs(n-1,n-1);
	writeln(ans);
}
