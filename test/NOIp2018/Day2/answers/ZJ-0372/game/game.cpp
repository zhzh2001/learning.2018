#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);puts("");
}
const ll N=200,mod=1e9+7;
ll n,m,all,total,ans,f[200][10],g[200][10],mp[200][10],rns;
ll ppow(ll x,ll k){
	ll ans=1;
	for(;k;k>>=1,Mul(x,x))if (k&1)Mul(ans,x);
	return ans;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if (n==1||m==1)return writeln(ppow(2,n*m)),0;
	all=(1<<(n-1));
	rep(i,0,all)f[i][n-2]=2;
	For(ci,1,m-1){
		rep(i,0,all)For(j,0,n-2)g[i][j]=0;total=0;
		rep(i,0,all)rep(j,0,all)For(A,0,n-2)if (((j&i)==j)&&f[i][A]){
			ll frog=1,to=A;
			For(w,0,n-2){
				ll a=i>>w&1,b=j>>w&1;
				if (w>A&&a!=b){frog=0;	break;	}
				if (a==b)Min(to,w);
			}
			if (frog){
				Add(g[all/2+j/2][to],f[i][A]);
				Add(g[j/2][to],f[i][A]);
				Add(total,f[i][A]*2);
			}
		}rep(i,0,all)For(j,0,n-2)f[i][j]=g[i][j];
	}writeln(total);
}
