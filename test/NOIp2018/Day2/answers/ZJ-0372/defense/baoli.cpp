#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);puts("");
}
const ll N=100100;
vector<ll>g[N];
char s[N];
ll f[N][2],a[N];
ll n,m,X,Y,mark1,mark2;
namespace work1{
void dfs(ll x,ll fa){
	f[x][0]=0;	f[x][1]=a[x];
	rep(i,0,g[x].size())if (g[x][i]!=fa){
		dfs(g[x][i],x);
		f[x][1]+=min(f[g[x][i]][0],f[g[x][i]][1]);
		f[x][0]+=f[g[x][i]][1];
	}
	if (x==X)f[x][mark1^1]=1e18;
	if (x==Y)f[x][mark2^1]=1e18;
}
void work(){
	
	For(i,1,n)a[i]=read();
	rep(i,1,n){
		ll x=read(),y=read();
		g[x].push_back(y);
		g[y].push_back(x);
	}
	For(i,1,m){
		X=read(),mark1=read();Y=read(),mark2=read();
		dfs(1,0);
		ll ans=min(f[1][0],f[1][1]);
		if (ans>1e17)puts("-1");
		else	writeln(ans);
	}
}
}
namespace work2{
struct dt{ll m[2][2];}tr[N*4],A,B ;
dt merge(dt a,dt b){
	dt tmp;
	For(i,0,1)For(j,0,1)tmp.m[i][j]=1e18;
	For(x1,0,1)For(x2,0,1)For(x3,0,1)For(x4,0,1)
	if (x2|x3)Min(tmp.m[x1][x4],a.m[x1][x2]+b.m[x3][x4]);
	return tmp;
}
void build(ll l,ll r,ll p){
	if (l==r){
		tr[p].m[0][0]=0;
		tr[p].m[1][1]=a[1];
		tr[p].m[0][1]=tr[p].m[1][0]=1e18;
		return;
	}
	ll mid=(l+r)>>1;
	build(l,mid,p<<1);
	build(mid+1,r,p<<1|1);
	tr[p]=merge(tr[p<<1],tr[p<<1|1]);
}
dt query(ll l,ll r,ll p,ll s,ll t){
	if (l==s&&r==t)return tr[p];
	ll mid=(l+r)>>1;
	if (t<=mid)return query(l,mid,p<<1,s,t);
	else if (s>mid)return query(mid+1,r,p<<1|1,s,t);
	else return merge(query(l,mid,p<<1,s,mid),query(mid+1,r,p<<1|1,mid+1,t));
}
void work(){
	For(i,1,n)a[i]=read();
	A.m[0][0]=A.m[0][1]=A.m[1][0]=1e18;
	B.m[1][1]=B.m[0][1]=B.m[1][0]=1e18;
	For(i,1,n-1)read(),read();
	build(1,n,1);
	For(i,1,m){
		ll s=read(),mark1=read(),t=read(),mark2=read();
		if (s>t)swap(s,t),swap(mark1,mark2);
		dt tmp;memset(tmp.m,0,sizeof tmp.m);
		if (s>1)tmp=query(1,n,1,1,s-1);
		if (mark1==1){
			A.m[1][1]=a[s];
			if (s>1)tmp=merge(tmp,A);
			else	tmp=A;
		}else{
			if (s>1)tmp=merge(tmp,B);
			else	tmp=B;
		}
		if (s+1<t){
			tmp=merge(tmp,query(1,n,1,s+1,t-1));
		}
		if (mark2==1){
			A.m[1][1]=a[t];
			tmp=merge(tmp,A);
		}else	tmp=merge(tmp,B);
		if (t<n)tmp=merge(tmp,query(1,n,1,t+1,n));
		ll ans=1e18;
		For(z1,0,1)For(z2,0,1)Min(ans,tmp.m[z1][z2]);
		if (ans>1e17)puts("-1");
		else	writeln(ans);
	}
}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",s+1);
	if (n<=2000)work1::work();
	else if (s[1]=='A')work2::work();
}
