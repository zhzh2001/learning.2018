#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);puts("");
}
const ll N=5010;
vector<ll>g[N],lnk[N];
ll ned,cur,Return,first,n,m,vis[N],ans[N],dfn[N],q[N],a[N],b[N],x[N],y[N],u,v;
void Find(){
	ll he=0,ta=0;
	For(i,1,n)if (dfn[i]==1)q[++ta]=i;
	for(;he!=ta;){
		ll x=q[++he];
		rep(i,0,g[x].size())
			if (--dfn[g[x][i]]==1)	q[++ta]=g[x][i];
	}
	ned=ta!=n;
}
void dfs(ll x){
	b[++b[0]]=x;vis[x]=1;
	rep(i,0,g[x].size()){
		if (u==x&&v==g[x][i]){
			continue;
		}
		if (u==g[x][i]&&v==x){
			continue;
		}
		if (!vis[g[x][i]])dfs(g[x][i]);
	}
}
bool check(){
	For(i,1,n)if (a[i]<b[i])return 0;
	else if (a[i]>b[i])return 1;
	return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	For(i,1,m){
		x[i]=read(),y[i]=read();
		g[x[i]].push_back(y[i]);
		g[y[i]].push_back(x[i]);
		++dfn[x[i]];++dfn[y[i]];
	}
	For(i,1,n)sort(g[i].begin(),g[i].end());
	Find();
	if (!ned){
		dfs(1);
		For(i,1,b[0])write(b[i]),putchar(' ');
		return 0;
	}
	For(i,1,n)a[i]=n+1;
	For(i,1,m)if (dfn[x[i]]==2&&dfn[y[i]]==2){
		u=x[i],v=y[i];
		For(j,1,n)vis[j]=0;
		b[0]=0;
		dfs(1);
		if (check())For(j,1,n)a[j]=b[j];
	}
	For(i,1,n)write(a[i]),putchar(' ');
}
