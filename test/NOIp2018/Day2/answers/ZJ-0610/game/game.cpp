#include<cstdio>
using namespace std;
int n,m;
const int MOD = 1e9+7;
namespace subtask1 {
  int w[100],s[100],num;
  int a[6][6],ans;
  void move(int x,int y,int c,int d) {
    if (x==n && y==m) {
      w[++num]=c,s[num]=d;
      return;
    }
    if (y<m) move(x,y+1,(c<<1)+1,(d<<1)+a[x][y+1]);
    if (x<n) move(x+1,y,c<<1,(d<<1)+a[x+1][y]);
  }
  bool check() {
    num=0;
    move(1,1,0,0);
    for (int i=1;i<=num;++i)
      for (int j=i+1;j<=num;++j)
        if (w[i]>w[j] && s[i]>s[j]) return 0;
    return 1;
  }
  void dfs(int x,int y) {
    if (x==n && y==m) {
      if (check()) ++ans;
      return;
    }
    a[x][y]=0;
    if (y<m) dfs(x,y+1);
    else dfs(x+1,1);
    a[x][y]=1;
    if (y<m) dfs(x,y+1);
    else dfs(x+1,1);
  }
  inline void solve() {
    dfs(1,1);
    printf("%d\n",ans<<1);
  }
}
namespace subtask2 {
  typedef long long ll;
  ll pw(ll x,ll n,ll p) {
    ll res=1;
	while (n) {
	  if (n&1) res=res*x%p;
	  x=x*x%p;
	  n>>=1;
    }
    return res;
  }
  inline void solve() {
    ll ans=pw(3,m-1,MOD);
    ans=ans*4%MOD;
    printf("%lld\n",ans);
  }
}
int main(){
  freopen("game.in","r",stdin);
  freopen("game.out","w",stdout);
  scanf("%d%d",&n,&m);
  if (n==2) subtask2 :: solve();
  else subtask1 :: solve();
  return 0;
}
