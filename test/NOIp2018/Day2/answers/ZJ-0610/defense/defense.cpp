#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int MAXN = 1e5+6;
struct node {
  int to,nxt;
} vet[MAXN<<1];
int h[MAXN],num,p[MAXN],n,m;
inline void add(int u,int v) { vet[++num]=(node){v,h[u]}; h[u]=num; }
namespace subtask1 {
  int g[MAXN],ans;
  int a,b,x,y;
  bool check(int u,int last) {
    for (int e=h[u];e;e=vet[e].nxt) {
      int v=vet[e].to;
      if (v==last) continue;
      if ((g[u]|g[v])==0) return 0;
      if (!check(v,u)) return 0;
    }
    return 1;
  }
  void dfs(int t,int s) {
    if (s>ans) return;
    if (t>n) {
      if (check(1,0)) ans=s;
      return;
    }
    if (t!=a && t!=b) {
      g[t]=0; dfs(t+1,s+p[t]*g[t]);
      g[t]=1; dfs(t+1,s+p[t]*g[t]);
    }
    else {
      if (t==a) g[t]=x,dfs(t+1,s+p[t]*g[t]);
      if (t==b) g[t]=y,dfs(t+1,s+p[t]*g[t]);
    }
  }
  inline void solve() {
    while (m--) {
      scanf("%d%d%d%d",&a,&x,&b,&y);
      ans=1e9;
      dfs(1,0);
      if (ans>=1e9) puts("-1");
      else printf("%d\n",ans);
    }	
  }
}
namespace subtask2{
  typedef long long ll;
  int a,b,x,y;
  ll f[MAXN][2];
  void dfs(int u,int last) {
    for (int e=h[u];e;e=vet[e].nxt) {
	  int v=vet[e].to;
	  if (v==last) continue;
	  dfs(v,u);
    }
    if (u==a) {
      if (x==0) {
        ll sum=0;
	    for (int e=h[u];e;e=vet[e].nxt) {
		  int v=vet[e].to;
		  if (v==last) continue;
		  sum+=f[v][1];
	    }
	    f[u][0]=min(f[u][0],sum);
	  }
	  else {
	   	ll sum=0;
	    for (int e=h[u];e;e=vet[e].nxt) {
		  int v=vet[e].to;
		  if (v==last) continue;
		  sum+=min(f[v][1],f[v][0]);
	    }
	    f[u][1]=min(f[u][1],sum+p[u]);
	  }
    }
    else if (u==b) {
      if (y==0) {
        ll sum=0;
	    for (int e=h[u];e;e=vet[e].nxt) {
		  int v=vet[e].to;
		  if (v==last) continue;
		  sum+=f[v][1];
	    }
	    f[u][0]=min(f[u][0],sum);
	  }
	  else {
	   	ll sum=0;
	    for (int e=h[u];e;e=vet[e].nxt) {
		  int v=vet[e].to;
		  if (v==last) continue;
		  sum+=min(f[v][1],f[v][0]);
	    }
	    f[u][1]=min(f[u][1],sum+p[u]);
	  }
    }
    else {
        ll sum=0;
	    for (int e=h[u];e;e=vet[e].nxt) {
		  int v=vet[e].to;
		  if (v==last) continue;
		  sum+=f[v][1];
	    }
	    f[u][0]=min(f[u][0],sum);
	   	sum=0;
	    for (int e=h[u];e;e=vet[e].nxt) {
		  int v=vet[e].to;
		  if (v==last) continue;
		  sum+=min(f[v][1],f[v][0]);
	    }
	    f[u][1]=min(f[u][1],sum+p[u]);	
    }
  }
  void solve() {
    while (m--) {
      scanf("%d%d%d%d",&a,&x,&b,&y);
      memset(f,0x3f,sizeof(f));
	  dfs(1,0);
	  ll ans=min(f[1][0],f[1][1]);
	  if (ans>1e9) puts("-1");
	  else printf("%lld\n",ans);	
    }
  }
}
char st[10];
int main() {
  freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout);
  scanf("%d%d%s",&n,&m,st+1);
  for (int i=1;i<=n;++i)
    scanf("%d",&p[i]);
  for (int i=1;i<n;++i) { 
    int u,v;
    scanf("%d%d",&u,&v);
    add(u,v); add(v,u);
  }
  if (n<=10) subtask1 :: solve();
  else subtask2 :: solve();
  return 0;
}
