#include<cstdio>
#include<algorithm>
using namespace std;
const int MAXN = 5006;
int n,m;
namespace subtask1 {
  int live;
  int dis[MAXN][MAXN],ans[MAXN];
  bool vis[MAXN];
  void dfs(int u) {
    if (live==0) return;
    for (int v=1;v<=n;++v) 
      if (dis[u][v] && !vis[v]) {
        ans[live--]=v;
        vis[v]=1;
        dfs(v);
      }
  }
  inline void solve() {
    for (int i=1;i<=m;++i) {
      int u,v;
      scanf("%d%d",&u,&v);
      dis[u][v]=1;dis[v][u]=1;
    }
    ans[n]=1;vis[1]=1;
    live=n-1;
    dfs(1);
    for (int i=n;i>=1;--i) {
      printf("%d",ans[i]);
      (i==1 ? puts("") : putchar(' '));
    }
  }
}
namespace subtask2 {
  int dis[MAXN][MAXN],ans[MAXN],live,fa[MAXN],son[MAXN],dep[MAXN];
  int x,y;
  bool vis[MAXN];
  void dfs(int u) {
    if (live==0) return;
    for (int v=1;v<=n;++v) 
      if (dis[u][v] && !vis[v]) {
        ans[live--]=v;
        vis[v]=1;
        dfs(v);
      }
  }
  void init(int u,int last) {
  	dep[u]=dep[last]+1;
  	fa[u]=last;
    for (int v=1;v<=n;++v) 
	  if (dis[u][v] && v!=last) {
	    if (!dep[v]) init(v,u);
	    else if (dep[v]<dep[u]) x=u,y=last,fa[u]=v;
      }
  }
  inline void solve() {
    for (int i=1;i<=m;++i) {
      int u,v;
	  scanf("%d%d",&u,&v);
	  dis[u][v]=1,dis[v][u]=1;
    }
    init(1,0);
    while (fa[x]!=fa[y]) {
	  son[fa[y]]=y;
	  y=fa[y];
	}
	while (y<x) y=son[y];
	dis[fa[y]][y]=0,dis[y][fa[y]]=0;
    ans[n]=1;vis[1]=1;
    live=n-1;
    dfs(1);
    for (int i=n;i>=1;--i) {
      printf("%d",ans[i]);
      (i==1 ? puts("") : putchar(' '));
    }
  }	
}
int main() {
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  scanf("%d%d",&n,&m);
  if (m==n-1) subtask1 :: solve();
  if (m==n) subtask2 :: solve();
  return 0;
}
