#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#define LL long long
using namespace std;

const LL INF = 1e11 + 10;
const LL Maxn = 100010;
LL n, m, Val[ Maxn ], Dp[ Maxn ][ 2 ];
char State[ 10 ];
struct edge {
	LL To, Next;
};
edge Edge[ Maxn << 1 ];
LL Start[ Maxn ], UsedSpace;
LL a, x, b, y;

void AddEdge( LL x, LL y ) {
	Edge[ ++UsedSpace ] = ( edge ) { y, Start[ x ] };
	Start[ x ] = UsedSpace;
	return;
}

void Init() {
	scanf( "%lld%lld", &n, &m );
	scanf( "%s", State );
	for( LL i = 1; i <= n; ++i ) scanf( "%lld", &Val[ i ] );
	for( LL i = 1; i < n; ++i ) {
		LL x, y; scanf( "%lld%lld", &x, &y );
		AddEdge( x, y ); AddEdge( y, x );
	}
	return;
}

void Dfs( LL u, LL Father ) {
	Dp[ u ][ 0 ] = 0;
	Dp[ u ][ 1 ] = Val[ u ];
	for( LL t = Start[ u ]; t; t = Edge[ t ].Next ) {
		LL v = Edge[ t ].To;
		if( v == Father ) continue;
		Dfs( v, u );
		Dp[ u ][ 0 ] += Dp[ v ][ 1 ];
		Dp[ u ][ 1 ] += min( Dp[ v ][ 0 ], Dp[ v ][ 1 ] );
	}
	if( u == a ) 
		if( x == 1 ) Dp[ a ][ 0 ] = INF; 
		else Dp[ a ][ 1 ] = INF;
	if( u == b ) 
		if( y == 1 ) Dp[ b ][ 0 ] = INF;
		else Dp[ b ][ 1 ] = INF;
	return;
}

void Work() {
	for( LL i = 1; i <= m; ++i ) {
		scanf( "%lld%lld%lld%lld", &a, &x, &b, &y );
		for( LL i = 1; i <= n; ++i ) Dp[ i ][ 0 ] = Dp[ i ][ 1 ] = INF;
		Dfs( 1, 1 );
		LL Ans = min( Dp[ 1 ][ 0 ], Dp[ 1 ][ 1 ] );
		if( Ans >= INF ) Ans = -1;
		printf( "%lld\n", Ans );
	}
	return;
}

int main() {
	freopen( "defense.in", "r", stdin );
	freopen( "defense.out", "w", stdout );
	Init();
	Work();
	return 0;
}
