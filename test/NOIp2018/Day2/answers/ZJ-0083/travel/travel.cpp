#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#define LL long long
using namespace std;

const int Maxn = 5010;
int n, m, Map[ Maxn ][ Maxn ], Num[ Maxn ];
bool Vis[ Maxn ];
int Father[ Maxn ], DelU, DelV;
int NumRec, Rec[ Maxn ];

int Last( int x ) {
	if( Father[ x ] != Map[ x ][ Num[ x ] ] ) return Map[ x ][ Num[ x ] ];
	return Map[ x ][ Num[ x ] - 1 ];
}

void Dfs( int u, int Father_ ) {
//	printf( "u=%d\n", u );
	Vis[ u ] = true;
	Father[ u ] = Father_;
	for( int i = 1; i <= Num[ u ]; ++i ) {
		int v = Map[ u ][ i ];
		if( v == Father_ ) continue;
//		printf( "  v=%d\n", v );
		if( !Vis[ v ] ) {
			Dfs( v, u );
//			printf( "u=%d num=%d\n", u, NumRec );
			if( NumRec ) break;
			continue;
		}
		if( Vis[ v ] ) {
//			printf( "*\n" );
			Rec[ ++NumRec ] = u;
			int t = u;
			while( t != v ) {
				t = Father[ t ];
				Rec[ ++NumRec ] = t;
			}
//			printf( "%d\n", NumRec );
//			for( int j = 1; j <= NumRec; ++j ) printf( "%d ", Rec[ j ] ); printf( "\n" );
//			for( int j = 1; j <= NumRec; ++j ) printf( "%d ", Last( Rec[ j ] ) ); printf( "\n" );
			for( int j = NumRec - 1; j >= 1; --j ) 
				if( Rec[ j ] > Rec[ 1 ] && Rec[ j ] == Last( Rec[ j + 1 ] ) ) {
					DelU = Rec[ j + 1 ]; DelV = Rec[ j ];
					break;
				}
			if( DelU == 0 ) {
				DelU = Rec[ NumRec ]; DelV = Rec[ 1 ];
			}
//			printf( "%d %d\n", DelU, DelV );
			break;
		}
	}
	return;
}

void Dfs2( int u ) {
	Vis[ u ] = true;
	printf( "%d ", u );
	for( int i = 1; i <= Num[ u ]; ++i ) {
		int v = Map[ u ][ i ];
		if( Vis[ v ] ) continue;
		if( ( DelU == u && DelV == v ) || ( DelU == v && DelV == u ) ) continue;
		Dfs2( v );
	}
	return;
}

void Work2() {
	Dfs( 1, 0 );
	memset( Vis, false, sizeof( Vis ) );
	Dfs2( 1 );
	printf( "\n" );
	return;
}

void Init() {
	scanf( "%d%d", &n, &m );
	for( int i = 1; i <= m; ++i ) {
		int x, y; scanf( "%d%d", &x, &y );
		Map[ x ][ ++Num[ x ] ] = y;
		Map[ y ][ ++Num[ y ] ] = x;
	}
	for( int i = 1; i <= n; ++i ) sort( Map[ i ] + 1, Map[ i ] + Num[ i ] + 1 );
	memset( Vis, false, sizeof( Vis ) );
	return;
}

void Work1( int u ) {
	Vis[ u ] = true;
	printf( "%d ", u );
	for( int i = 1; i <= Num[ u ]; ++i ) {
		int v = Map[ u ][ i ];
		if( Vis[ v ] ) continue;
		Work1( v );
	}
	return;
}

int main() {
	freopen( "travel.in", "r", stdin );
	freopen( "travel.out", "w", stdout );
	Init();
	if( m == n - 1 ) Work1( 1 ), printf( "\n" ); 
	else Work2();
	return 0;
}
