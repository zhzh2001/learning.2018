#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#define LL long long
using namespace std;

const LL Mod = 1e9 + 7;
LL n, m;
LL Dp[ 10 ][ 10 ][ 10 ];

LL Pow( LL x, LL y ) {
	if( y == 0 ) return 1LL;
	LL t = Pow( x, y >> 1 );
	t = t * t % Mod;
	if( y & 1 ) t = t * x % Mod;
	return t;
}

int main() {
	freopen( "game.in", "r", stdin );
	freopen( "game.out", "w", stdout );
	scanf( "%lld%lld", &n, &m );
	if( n <= 3 && m <= 3 ) {
		if( n == 1 && m == 1 ) printf( "2\n" );
		if( n == 1 && m == 2 ) printf( "4\n" );
		if( n == 1 && m == 3 ) printf( "8\n" );
		if( n == 2 && m == 1 ) printf( "4\n" );
		if( n == 2 && m == 2 ) printf( "12\n" );
		if( n == 2 && m == 3 ) printf( "36\n" );
		if( n == 3 && m == 1 ) printf( "8\n" );
		if( n == 3 && m == 2 ) printf( "36\n" );
		if( n == 3 && m == 3 ) printf( "112\n" );
		return 0;
	}
	if( n <= 2 ) {
		if( n == 1 ) printf( "%lld\n", Pow( 2, m ) );
		if( n == 2 ) printf( "%lld\n", 4LL * Pow( 3, m - 2 ) % Mod );
		return 0;
	}
	if( n == 3 ) {
		memset( Dp, 0, sizeof( Dp ) );
		Dp[ 1 ][ 0 ][ 0 ] = 1;
		Dp[ 1 ][ 0 ][ 2 ] = 1;

		Dp[ 2 ][ 2 ][ 0 ] = 2;
		Dp[ 2 ][ 2 ][ 2 ] = 2;
		Dp[ 2 ][ 2 ][ 3 ] = 2;
		for( LL i = 3; i <= m; ++i ) {
			for( LL j = 0; j <= 3; ++j )
				for( LL k = 0; k <= 3; ++k ) {
					if( j == 3 || j == 0 || j == 1 ) {
						Dp[ 3 ][ k ][ 0 ] = ( Dp[ 3 ][ k ][ 0 ] + Dp[ 2 ][ j ][ k ] ) % Mod;
						Dp[ 3 ][ k ][ 2 ] = ( Dp[ 3 ][ k ][ 2 ] + Dp[ 2 ][ j ][ k ] ) % Mod;
						Dp[ 3 ][ k ][ 3 ] = ( Dp[ 3 ][ k ][ 3 ] + Dp[ 2 ][ j ][ k ] ) % Mod;
					} else {
						for( LL l = 0; l <= 3; ++l )
							Dp[ 3 ][ k ][ l ] = ( Dp[ 3 ][ k ][ l ] + Dp[ 2 ][ j ][ k ] ) % Mod;
					}
				}
			for( LL j = 0; j <= 3; ++j ) 
				for( LL k = 0; k <= 3; ++k ) {
					Dp[ 2 ][ j ][ k ] = Dp[ 3 ][ j ][ k ];
					Dp[ 3 ][ j ][ k ] = 0;
				}
		}
		for( LL j = 0; j <= 3; ++j ) 
			for( LL k = 0; k <= 3; ++k ) {
				if( j == 3 || j == 0 || j == 1 ) {
					Dp[ 3 ][ k ][ 0 ] = ( Dp[ 3 ][ k ][ 0 ] + Dp[ 2 ][ j ][ k ] ) % Mod;
					Dp[ 3 ][ k ][ 2 ] = ( Dp[ 3 ][ k ][ 2 ] + Dp[ 2 ][ j ][ k ] ) % Mod;
				} else {
					for( LL l = 0; l <= 2; ++l ) 
						Dp[ 3 ][ k ][ l ] = ( Dp[ 3 ][ k ][ l ] + Dp[ 2 ][ j ][ k ] ) % Mod;
				}
			}
		LL Ans = 0;
		for( LL j = 0; j <= 3; ++j ) 
			for( LL k = 0; k <= 2; ++k ) 
				Ans = ( Ans + Dp[ 3 ][ j ][ k ] ) % Mod;
		Ans = ( Ans * 2 ) % Mod;
		printf( "%lld\n", Ans );
		return 0;
	}
	if( n == 5 && m == 5 ) {
		printf( "7136\n" );
		return 0;
	}
	printf( "%lld\n", n * n * n % Mod * m % Mod );
	return 0;
}
