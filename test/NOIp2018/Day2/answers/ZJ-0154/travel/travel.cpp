#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<vector>
#define M 20010
using namespace std;
int n,m,cnt;
int a[M],b[M];
bool vis[M];
vector<int>v[M];
int read()
{
	int x=0,tmp=1;
	char c=getchar();
	while (c<'0'||c>'9')
	{
		if (c=='-')
			tmp=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=(x<<3)+(x<<1)+c-48;
		c=getchar();
	}
	return x*tmp;
}
void dfs(int x)
{
	if (!vis[x])
	{
		a[++cnt]=x; 
		vis[x]=1;
	}
	while (b[x]<v[x].size())
	{
		while (vis[v[x][b[x]]]&&b[x]<v[x].size())
			b[x]++;
		if (b[x]>=v[x].size())
		{
			return;
		}
		int k=v[x][b[x]++];
		dfs(k);
	}
}
void work1()
{
	for (int i=1; i <= m; i++)
	{
		int x=read(),y=read();
		v[x].push_back(y);
		v[y].push_back(x);
	}
	for (int i=1; i <= n; i++)
		sort(v[i].begin(),v[i].end());
	vis[0]=1; dfs(1);
	for (int i=1; i <= n; i++)
		printf("%d ",a[i]);
	printf("\n");
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(); m=read();
	work1();	
	return 0;
}
