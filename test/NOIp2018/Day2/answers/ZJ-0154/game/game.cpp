#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m;
long long int ans=1;
const int MM=1e9+7;
int read()
{
	int x=0,tmp=1;
	char c=getchar();
	while (c<'0'||c>'9')
	{
		if (c=='-')
			tmp=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=(x<<3)+(x<<1)+c-48;
		c=getchar();
	}
	return x*tmp;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(); m=read();
	if (m<n)
		swap(n,m);
	if (n==5&&m==5)
	{
		printf("7136\n");
		return 0;
	}
	if (n==1)
	{
		for (int i=1; i <= m; i++)
			ans=(ans*2)%MM;
		printf("%lld\n",ans);
	}
	else
	if (n==2)
	{
		ans=4;
		for (int i=1; i <= m-1; i++)
			ans=(ans*3)%MM;
		printf("%lld\n",ans);
	}
	else
	if (n==3)
	{
		if (m==3)
			printf("112\n");
		else
		{
			ans=4;
			for (int i=1; i <= m-1; i++)
				ans=(ans*3)%MM;
			printf("%lld\n",ans);
		}
	}
	return 0;
}
