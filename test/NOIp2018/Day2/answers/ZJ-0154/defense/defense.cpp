#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define M 200010
#define INF 0x3f3f3f3f
using namespace std;
int n,m;
long long int ans=INF<<1;
string s;
int p[M],xx[M],yy[M];
bool vis[2010][2010];
long long int f[M][2];
int read() {
	int x=0,tmp=1;
	char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-')
			tmp=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9') {
		x=(x<<3)+(x<<1)+c-48;
		c=getchar();
	}
	return x*tmp;
}
void work1() {
	while (m--) {
		ans=INF;
		int a=read(),x=read(),b=read(),y=read();
		if (vis[a][b]&&x==0&&y==0) {
			printf("-1\n");
			continue;
		}
		memset(f,0x3f,sizeof(f));
		f[0][0]=0;
		f[0][1]=0;
		for (int i=1; i <= n; i++) {
			if (i==a) {
				if (x==1) {
					f[i][1]=min(f[i-1][0],f[i-1][1])+p[i];
					f[i][0]=INF;
				} else {
					f[i][1]=INF;
					f[i][0]=f[i-1][0];
				}
			} else if (i==b) {
				if (y==1) {
					f[i][1]=min(f[i-1][0],f[i-1][1])+p[i];
					f[i][0]=INF;
				} else {
					f[i][1]=INF;
					f[i][0]=f[i-1][0];
				}
			} else {
				f[i][1]=min(f[i-1][0],f[i-1][1])+p[i];
				f[i][0]=f[i-1][0];
			}
		}
		printf("%lld\n",min(f[n][0],f[n][1]));
	}
}
void work2() {
	while (m--) {
		ans=INF;
		int a=read(),x=read(),b=read(),y=read();
		if (vis[a][b]&&x==0&&y==0) {
			printf("-1\n");
			continue;
		}
		for (int S=0; S < (1<<n); S++) {
			if ((S&(1<<(a-1))!=x)||(S&(1<<(b-1))!=y))
				continue;
			bool boo=1;
			for (int i=1; i <= n-1; i++) {
				int a1=S&(1<<(xx[i]-1)),a2=S&(1<<(yy[i]-1));
				if (a1==0&&a2==0)
				{
					boo=0;
					break;
				}
			}
			if (!boo)
				continue;
			long long int sum=0;
			for (int i=0; i < n; i++)
				if (S&(1<<i))
					sum+=p[i+1];
			ans=min(ans,sum);
		}
		printf("%lld\n",ans);
	}
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();
	m=read();
	cin >> s;
	for (int i=1; i <= n; i++)
		p[i]=read();
	for (int i=1; i <= n-1; i++) {
		xx[i]=read();
		yy[i]=read();
		vis[xx[i]][yy[i]]=1;
		vis[yy[i]][xx[i]]=1;
	}
	if (s[0]=='A')
		work1();
	else
		work2();
	return 0;
}
