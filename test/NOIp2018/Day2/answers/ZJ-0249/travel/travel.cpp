#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>

using namespace std;

int n,m,deep=1;

vector<int> g[5003];
int pre[5003],a[5003],quan[5003];
bool vis[5003];


void dfs1(int x)
{
	vis[x]=true;a[x]=deep++;
	for(int i=0;i<g[x].size();i++){
		if(!vis[g[x][i]]){
			dfs1(g[x][i]);
		}
	}
}

int dfsycl(int x)//这里写错了,遇到m==n的仙人掌图的时候其实权值不是这样算的 
{
	for(int i=0;i<g[x].size();i++){
		if(g[x][i] && g[x][i]!=pre[x] ){
			pre[g[x][i]]=x;
			quan[x]+=dfsycl(g[x][i]);
		}
	}
	return quan[x];
}

int dfsycl2(int x)//这里写错了,遇到m==n的仙人掌图的时候其实权值不是这样算的 
{
	vis[x]=true;
	for(int i=0;i<g[x].size();i++){
		if(g[x][i] && g[x][i]!=pre[x] && !vis[g[x][i]]){
			pre[g[x][i]]=x;
			quan[x]+=dfsycl(g[x][i]);
		}
	}
	return quan[x];
}

bool cmp(int x,int y)
{
	if(quan[x]==quan[y]) return x<y;
	return quan[x]>quan[y];
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>m;
	for(int i=1;i<=n;i++){quan[i]=n-i+1;}
	for(int i=1;i<=m;i++){
		int u,v;
		cin>>u>>v;
		g[u].push_back(v);
		g[v].push_back(u);
	}
	if(m!=n){
		dfsycl(1);
	}else{
		dfsycl2(1);
		memset(vis,0,sizeof(vis));
	}
	for(int i=1;i<=n;i++){
		if(!g[i].empty()){
			sort(g[i].begin(),g[i].end(),cmp);
		}
	}
	pre[1]=0;
	dfs1(1);
	for(int i=1;i<=n;i++){cout<<a[i]<<" ";}
	return 0;
}
