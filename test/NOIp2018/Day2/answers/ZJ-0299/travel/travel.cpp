#include<bits/stdc++.h>
#define LL long long
#define pb push_back
using namespace std;
inline LL read(){
	char c=getchar();while (c!='-'&&(c>'9'||c<'0'))c=getchar();
	LL k=0,kk=1;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return k*kk;
}void write(LL x){if (x<0)putchar('-'),x=-x;if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(LL x){write(x);puts("");}
int ff[10010],l[10010],r[10010],x,y,n,m,faq;
int flag[10010],fh[10010],ffa[10010];
vector<int>v[10010];
void clc(int x,int fa){//求出基环树上所有点 
	int d=v[x].size();fh[x]=fa;flag[x]=1;
	for (int i=0;i<d&&!faq;i++)if (v[x][i]!=fa){
		if (flag[v[x][i]]&&!faq){
			ff[x]=1;int dd=v[x][i];
			while (x!=dd)x=fh[x],ff[x]=1;
			faq=1;return;
		}else clc(v[x][i],x);
	}
}
signed main(){
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=m;i++)x=read(),y=read(),v[x].pb(y),v[y].pb(x);
	if (n==m)clc(1,0);faq=0;
	for (int i=1;i<=n;i++){
		flag[i]=fh[i]=0;
		l[i]=0;r[i]=v[i].size()-1;
		v[i].pb(0);sort(&v[i][0],&v[i][r[i]+1]);
	}int d=1;
	for (int i=1;i<=n;i++){
		write(d);if (i!=n)putchar(' ');flag[d]=1;
	//	if (i==9)cout<<1<<endl;
		while (flag[v[d][l[d]]]&&l[d]<=r[d])l[d]++;
		while(l[d]>r[d]){
			d=fh[d];if (d==0)break;
			while (flag[v[d][l[d]]]&&l[d]<=r[d])l[d]++;
		}if (d==0)break;
	//	if (i==9)cout<<endl<<' '<<v[d][0]<<' '<<v[d][1]<<endl;
		if (ff[d]&&!faq&&l[d]==r[d]&&ff[v[d][l[d]]]){
			//遍历完除了环以外的所有孩子的时候可以向上跳
			int j=fh[d];
			while (flag[v[j][l[j]]]&&l[j]<=r[j])l[j]++;
			while (ff[j]&&l[j]>r[j]){//向上找第一个有儿子的环上点 
				j=ffa[j];if (!j)break;
				while (flag[v[j][l[j]]]&&l[j]<=r[j])l[j]++;
			}ffa[d]=j;
			if (j&&ff[j]&&flag[j]){
				while (flag[v[j][l[j]]]&&l[j]<=r[j])l[j]++;
				if (l[j]<=r[j]&&v[j][l[j]]<v[d][l[d]])d=j,faq=1;
			}
		}fh[v[d][l[d]]]=d;d=v[d][l[d]];
	}
	return 0;
}
