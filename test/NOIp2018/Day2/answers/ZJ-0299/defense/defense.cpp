#include<bits/stdc++.h>
#define LL long long
#define int long long
using namespace std;
inline LL read(){
	char c=getchar();while (c!='-'&&(c>'9'||c<'0'))c=getchar();
	LL k=0,kk=1;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return k*kk;
}void write(LL x){if (x<0)putchar('-'),x=-x;if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(LL x){write(x);puts("");}
int f[100010],g[100010],p[100010],a[200010][2],last[100010];
int ll,rr,xx,yy,ans,n,m,x,y,kk;
char s[10];
void dfs(int x,int fa){
	//f[x]表示当前放了,g[x]表示当前没放但被子树覆盖,h[x]表示没放也没被覆盖 
	f[x]=p[x];g[x]=0;int dd=1e15;
	for (int i=last[x];i;i=a[i][0])if (a[i][1]!=fa){
		dfs(a[i][1],x);f[x]+=min(f[a[i][1]],g[a[i][1]]);
		g[x]+=f[a[i][1]];
	}
	if (ll==x){
		if (xx==1)g[x]=1e16;else f[x]=1e16;
	}else if (rr==x){
		if (yy==1)g[x]=1e16;else f[x]=1e16;
	}
}void doit(int x,int y){a[++kk][0]=last[x];a[kk][1]=y;last[x]=kk;}
void clc(int x,int fa){//换根操作 
	if (fa){
		int xx=f[fa]-min(f[x],g[x]),yy=g[fa]-f[x];
		f[x]+=min(xx,yy);g[x]+=xx;
	}for (int i=last[x];i;i=a[i][0])
		if (a[i][1]!=fa)clc(a[i][1],x);
}
signed main(){
	freopen("defense.in","r",stdin);freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",s);
	for (int i=1;i<=n;i++)p[i]=read();
	for (int i=1;i<n;i++)x=read(),y=read(),doit(x,y),doit(y,x);
	if (s[1]=='1'){
		dfs(1,0);
		g[1]=1e16;clc(1,0);
		for (int i=1;i<=m;i++){
			ll=read();xx=read();rr=read();yy=read();
			if (yy)ans=f[rr];else ans=g[rr];
			if (ans>1e15)puts("-1");else writeln(ans); 
		}
		return 0;
	}
	for (int i=1;i<=m;i++){
		ll=read();xx=read();rr=read();yy=read();
		dfs(1,0);ans=min(f[1],g[1]);
		if (ans>1e15)puts("-1");else writeln(ans); 
	} 
	return 0;
}
