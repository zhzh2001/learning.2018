#include<cstdio>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<vector>
#include<ctype.h>

#define ll long long
using namespace std;

const int N = 100005;
const ll inf = 1e13;
int n, m, num, root, fa[N], son[N], siz[N], b[N], w[N], h[N], e[N<<1], pre[N<<1], ch[N][2];
ll a[N], f[N][2], lf[N][2];
inline void add(int x, int y){ e[++num]=y, pre[num]=h[x], h[x]=num;}
void dfs(int u){
	f[u][1]=a[u], f[u][0]=0;
	for(int i=h[u]; i; i=pre[i]) if(e[i]!=fa[u]){
		fa[e[i]]=u, dfs(e[i]);
		f[u][0]+=f[e[i]][1], f[u][1]+=min(f[e[i]][0], f[e[i]][1]);
	}
}
inline void modify(int x, ll y){
	a[x]=y;
}
int main(){
	freopen("defense.in", "r", stdin);
	freopen("bf.out", "w", stdout);
	scanf("%d%d", &n, &m);
	while(getchar()!='\n');
	for(int i=1; i<=n; ++i) scanf("%lld", a+i);
	for(int i=1; i<n; ++i){
		static int x, y;
		scanf("%d%d", &x, &y);
		add(x, y), add(y, x);
	}
	for(int i=1; i<=m; ++i){
		static int x, y, xx, yy, X, Y;
		scanf("%d%d%d%d", &x, &xx, &y, &yy);
		X=a[x], Y=a[y];
		modify(x, (xx?-inf:inf)), modify(y, (yy?-inf:inf));
		dfs(1);
		ll ans=(xx?X+inf:0)+(yy?Y+inf:0)+min(f[1][0], f[1][1]);
		printf("%lld\n", ans>=inf?-1:ans);
		modify(x, X), modify(y, Y);
	}
	return 0;
}
