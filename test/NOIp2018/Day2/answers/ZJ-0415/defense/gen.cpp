#include<cstdio>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<vector>
#include<ctype.h>
#include<time.h>

#define ll long long
using namespace std;
#define rand() ((rand()<<15)^rand())
int main(){
	srand(time(0));
	freopen("defense.in", "w", stdout);
	int n=100000, m=100;
	printf("%d %d C3\n", n, m);
	for(int i=1; i<=n; ++i) printf("%d ", 100000); puts("");
	for(int i=1; i<n; ++i) printf("%d %d\n", i-rand()%i, i+1);
	for(int i=1; i<=m; ++i){
		int x=rand()%n+1, y=rand()%n+1;
		while(x==y) y=rand()%n+1;
		printf("%d %d %d %d\n", x, rand()&1, y, rand()&1);
	}
	return 0;
}
