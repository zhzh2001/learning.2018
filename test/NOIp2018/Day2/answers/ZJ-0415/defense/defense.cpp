#include<cstdio>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<vector>
#include<ctype.h>

#define ll long long
using namespace std;

const int N = 100005;
const ll inf = 1e13;
int n, m, num, root, fa[N], son[N], siz[N], b[N], w[N], h[N], e[N<<1], pre[N<<1], ch[N][2];
ll a[N], f[N][2], lf[N][2];
struct matrix{
	ll a[2][2];
	matrix(){}
	matrix(ll x, ll y){ a[0][0]=inf, a[0][1]=x, a[1][0]=a[1][1]=y;}
	inline matrix operator *(const matrix &rhs)const{
		static matrix ans;
		ans.a[0][0]=min(a[0][0]+rhs.a[0][0], a[0][1]+rhs.a[1][0]);
		ans.a[0][1]=min(a[0][0]+rhs.a[0][1], a[0][1]+rhs.a[1][1]);
		ans.a[1][0]=min(a[1][0]+rhs.a[0][0], a[1][1]+rhs.a[1][0]);
		ans.a[1][1]=min(a[1][0]+rhs.a[0][1], a[1][1]+rhs.a[1][1]);
		return ans;
	}
}s[N], g[N];
inline void add(int x, int y){ e[++num]=y, pre[num]=h[x], h[x]=num;}
inline void update(int u){ s[u]=s[ch[u][0]]*g[u]*s[ch[u][1]];}
int divide(int l, int r){
	if(l>r) return 0;
	int sum=0, t, u, now=0;
	for(int i=l; i<=r; ++i) sum+=w[b[i]];
	for(int i=l; i<=r; ++i) if((now+=w[b[i]])*2>=sum) t=i, i=r;
	u=b[t];
	fa[ch[u][0]=divide(l, t-1)]=u, fa[ch[u][1]=divide(t+1, r)]=u, update(u);
	f[u][1]=min(s[u].a[1][0], s[u].a[1][1]), f[u][0]=min(s[u].a[0][0], s[u].a[0][1]);
	return u;
}
inline int build(int x){
	int cnt=0;
	for(int i=x; i; i=son[i]) b[++cnt]=i;
	return divide(1, cnt);
}
void dfs(int u){
	siz[u]=1;
	f[u][1]=a[u];
	for(int i=h[u]; i; i=pre[i]) if(e[i]!=fa[u]){
		fa[e[i]]=u, dfs(e[i]), siz[u]+=siz[e[i]];
		f[u][0]+=f[e[i]][1], f[u][1]+=min(f[e[i]][0], f[e[i]][1]);
		if(siz[e[i]]>siz[son[u]]) son[u]=e[i];
	}
	w[u]=siz[u]-siz[son[u]];
	lf[u][0]=f[u][0]-f[son[u]][1];
	lf[u][1]=f[u][1]-min(f[son[u]][0], f[son[u]][1]);
	g[u]=matrix(lf[u][0], lf[u][1]);
	for(int i=h[u]; i; i=pre[i]) if(e[i]!=fa[u] && e[i]!=son[u]) fa[build(e[i])]=u;
}
inline void modify(int x, ll y){
	lf[x][1]+=y-a[x], a[x]=y;
	while(x){
		g[x]=matrix(lf[x][0], lf[x][1]);
		update(x);
		if(ch[fa[x]][0]!=x && ch[fa[x]][1]!=x){
			lf[fa[x]][0]-=f[x][1], lf[fa[x]][1]-=min(f[x][0], f[x][1]);
			f[x][1]=min(s[x].a[1][0], s[x].a[1][1]), f[x][0]=min(s[x].a[0][0], s[x].a[0][1]);
			lf[fa[x]][0]+=f[x][1], lf[fa[x]][1]+=min(f[x][0], f[x][1]);
		}
		x=fa[x];
	}
}
int main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d", &n, &m);
	while(getchar()!='\n');
	for(int i=1; i<=n; ++i) scanf("%lld", a+i);
	for(int i=1; i<n; ++i){
		static int x, y;
		scanf("%d%d", &x, &y);
		add(x, y), add(y, x);
	}
	s[0].a[0][1]=s[0].a[1][0]=inf;
	dfs(1), fa[root=build(1)]=0;
//	for(int i=1; i<=n; ++i) printf(">>%d\n", fa[i]);
//	printf(">>%lld %lld %lld %lld\n", s[root].a[0][0], s[root].a[0][1], s[root].a[1][0], s[root].a[1][1]);
	for(int i=1; i<=m; ++i){
		static int x, y, xx, yy, X, Y;
		scanf("%d%d%d%d", &x, &xx, &y, &yy);
		X=a[x], Y=a[y];
		modify(x, (xx?-inf:inf)), modify(y, (yy?-inf:inf));
		ll ans=(xx?X+inf:0)+(yy?Y+inf:0)+min(f[root][0], f[root][1]);
		printf("%lld\n", ans<inf?ans:-1);
		modify(x, X), modify(y, Y);
	}
	return 0;
}
