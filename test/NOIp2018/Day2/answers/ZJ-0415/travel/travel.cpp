#include<cstdio>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<vector>
#include<ctype.h>

#define ll long long
using namespace std;

const int N = 5005;
int num, n, m, id, top, cnt, flag, vis[N], ans[N], s[N], stk[N], h[N], e[N<<1], pre[N<<1];
bool ban[N];
vector<int> f[N];
inline void add(int x, int y){ e[++num]=y, pre[num]=h[x], h[x]=num;}
void dfs(int u, int fa=0){
	printf("%d ", u);
	for(int i=h[u]; i; i=pre[i]) if(e[i]!=fa) dfs(e[i], u);
}

void dfs2(int u, int fa=0){
//	printf(">>%d\n", u);
	vis[u]=++id, stk[++top]=u;
	for(int i=h[u]; i; i=pre[i]) if(e[i]!=fa){
		if(vis[e[i]]){
			if(vis[e[i]]>vis[u]) continue;
			s[++cnt]=e[i];
			for(int j=top; stk[j]!=e[i]; --j) s[++cnt]=stk[j];
//			printf("dfs2:%d %d\n", u, cnt);
		}
		else dfs2(e[i], u);
	}
	--top;
}
void dfs3(int u, int fa=0){
	++id;
	if(u<ans[id]) flag=2;
	if(flag==2) ans[id]=u;
	if(!flag && u>ans[id]){
		flag=1;
		return;
	}
	for(int i=h[u]; i; i=pre[i]) if(e[i]!=fa && !(ban[u] && ban[e[i]])){
		dfs3(e[i], u);
		if(flag==1) return;
	}
}
inline void subtask2(){
	dfs2(1);
	ans[1]=n;
//	for(int i=1; i<=cnt; ++i) printf("[%d]", s[i]);
	s[++cnt]=s[1];
	for(int i=1; i<cnt; ++i){
		ban[s[i]]=ban[s[i+1]]=1;
		id=flag=0, dfs3(1);
		ban[s[i]]=ban[s[i+1]]=0;
	}
	for(int i=1; i<=n; ++i) printf("%d ", ans[i]);
}

int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i=1; i<=m; ++i){
		static int x, y;
		scanf("%d%d", &x, &y);
		f[x].push_back(y), f[y].push_back(x);
	}
	for(int i=1; i<=n; ++i) sort(f[i].begin(), f[i].end());
	for(int i=1; i<=n; ++i) for(int j=f[i].size()-1; ~j; --j) add(i, f[i][j]);
	if(m==n-1) dfs(1); else subtask2();
	return 0;
}
