#include<cstdio>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<vector>
#include<ctype.h>

#define ll long long
using namespace std;

const int N = 1000015, M = 10, P = 1000000007;
int n, m, ans=1, s[N], x[N], y[N], f[N][M];
int main(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if(n==1 && m==1) return puts("2"), 0;
	for(int i=1; i<=n; ++i) for(int j=1; j<=m; ++j) ++s[i+j], x[i+j]=i, y[i+j]=j;
	for(int i=2; i<=3; ++i) for(int j=0; j<=s[i]; ++j) f[i][j]=1;
	for(int i=2; i<=n+m-2; ++i) for(int j=0; j<=s[i]; ++j){
//		printf("f:%d %d:%d\n", i, j, f[i][j]);
//		int X=x[i]-j+1, Y=y[i]+j-1;
//		++X, ++Y;
//		printf(">>%d %d\n", X, Y);
//		if(0<=x[i+2]-X+1 && x[i+2]-X+1<=s[i+2]) (f[i+2][x[i+2]-X+1]+=f[i][j])%=P;
		for(int k=0; k<=s[i+2]; ++k){
			int X=x[i+2]-k+1;
			--X;
			X=x[i]-X+1;
			//printf("--%d\n", X);
			if(X==j || X<=0 || X>=s[i] || k==0 || k==s[i+2]) (f[i+2][k]+=f[i][j])%=P;
		}
	}
	for(int i=n+m-1; i<=n+m; ++i){
		int sum=0;
		for(int j=0; j<=s[i]; ++j) (sum+=f[i][j])%=P;
//		printf(">>%d\n", sum);
		ans=(ll)ans*sum%P;
	}
	return printf("%d", ans), 0;
}
