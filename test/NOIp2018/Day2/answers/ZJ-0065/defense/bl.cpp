#include <bits/stdc++.h>
using namespace std;
#define LL long long
const LL inf=1e15;
int n,m,XX,YY,XO,YO,t;
char TY[5];
LL f[100005],g[100005],P[100005],ans;
vector <int> a[100005];
void play(int x,int y){
	f[x]=P[x]; g[x]=0;
	for (unsigned i=0;i<a[x].size();++i)
	if (a[x][i]!=y){
		play(a[x][i],x);
		f[x]+=min(f[a[x][i]],g[a[x][i]]);
		if (f[x]>inf) f[x]=inf;
		g[x]+=f[a[x][i]];
		if (g[x]>inf) g[x]=inf;
	}
	if (XX==x){
		if (XO) g[x]=inf; else f[x]=inf;
	}else
	if (YY==x){
		if (YO) g[x]=inf; else f[x]=inf;
	}
}
struct O{
	LL oo,ii,io,oi;
}F[200005];
int lc[200005],rc[200005];
O operator*(const O &a,const O &b){
	O c;
	c.oo=min(min(a.oo+b.io,min(a.oi+b.io,a.oi+b.oo)),inf);
	c.oi=min(min(a.oo+b.ii,min(a.oi+b.ii,a.oi+b.oi)),inf);
	c.io=min(min(a.io+b.io,min(a.ii+b.io,a.ii+b.oo)),inf);
	c.ii=min(min(a.io+b.ii,min(a.ii+b.ii,a.ii+b.oi)),inf);
	return c;
}
O operator+(const O &a,const O &b){
	O c;
	c.oo=min(min(a.oo+b.oo,a.oi+b.io),inf);
	c.oi=min(min(a.oo+b.oi,a.oi+b.ii),inf);
	c.io=min(min(a.io+b.oo,a.ii+b.io),inf);
	c.ii=min(min(a.io+b.oi,a.ii+b.ii),inf);
	return c;
}
int build(int p,int q){
	int u=++t;
	if (p==q){
		F[u].oo=0;
		F[u].ii=P[p];
		F[u].oi=F[u].io=inf;
		return u;
	}
	lc[u]=build(p,(p+q)/2);
	rc[u]=build((p+q)/2+1,q);
	F[u]=F[lc[u]]*F[rc[u]];
	return u;
}
O qiu(int u,int l,int r,int p,int q){
	if (p==l&&r==q) return F[u];
	if (l>p+q>>1) return qiu(rc[u],l,r,(p+q>>1)+1,q); else
	if (r<=p+q>>1) return qiu(lc[u],l,r,p,p+q>>1); else
	return qiu(lc[u],l,p+q>>1,p,p+q>>1)*qiu(rc[u],(p+q>>1)+1,r,(p+q>>1)+1,q);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,TY);
	for (int i=1;i<=n;++i) scanf("%lld",&P[i]);
	for (int i=1;i<n;++i){
		int x,y;
		scanf("%d%d",&x,&y);
		a[x].push_back(y);
		a[y].push_back(x);
	}
	if (n<=2000&&m<=2000){
		for (int i=1;i<=m;++i){
			scanf("%d%d%d%d",&XX,&XO,&YY,&YO);
			play(1,0);
			ans=min(f[1],g[1]);
			printf("%lld\n",ans==inf?-1:ans);
		}
		return 0;
	}
	{
		t=0; build(1,n);
		while (m--){
			scanf("%d%d%d%d",&XX,&XO,&YY,&YO);
			if (XX>YY) swap(XX,YY),swap(XO,YO);
			O A=qiu(1,1,XX,1,n),
			  B=qiu(1,XX,YY,1,n),
			  C=qiu(1,YY,n,1,n);
			if (XO) A.io=A.oo=B.oi=B.oo=inf;
				else A.ii=A.oi=B.ii=B.io=inf;
			if (YO) B.io=B.oo=C.oi=C.oo=inf;
				else B.ii=B.oi=C.ii=C.io=inf;
			A=A+B+C;
			ans=min(min(A.io,A.oi),min(A.ii,A.oo));
			if (ans==inf) puts("-1"); else{
				if (XO) ans-=P[XX];
				if (YO) ans-=P[YY];
				printf("%lld\n",ans);
			}
		}
	}
	return 0;
}
