#include <bits/stdc++.h>
#define N 100010
#define INF (1ll<<60)
using namespace std;
typedef long long ll;
inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
struct edge{int v,next;}vs[N<<1];
int n,m,vl[N],a,x,b,y,fa[N],ee,st[N],dep[N],ins[N];
ll F[N][2],G[N][2],td[N][2];
char s[10];
inline void addedge(int u,int v)
{
	vs[++ee].v=v;vs[ee].next=st[u];st[u]=ee;
}
inline void dfs(int rt,int pr)
{
	for(int i=st[rt];i;i=vs[i].next)
	{
		if(vs[i].v==pr) continue;
		fa[vs[i].v]=rt; dep[vs[i].v]=dep[rt]+1;
		dfs(vs[i].v,rt);
		td[rt][0]+=F[vs[i].v][1];
		td[rt][1]+=min(F[vs[i].v][1],F[vs[i].v][0]);
	}
	F[rt][0]=td[rt][0]; F[rt][1]=td[rt][1]+vl[rt];
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(); m=read();
	scanf("%s",s);
	for(int i=1;i<=n;i++) vl[i]=read();
	for(int i=1;i<n;i++)
	{
		int u=read(), v=read();
		addedge(u,v); addedge(v,u);
	}
	dfs(1,0);
		for(int i=1;i<=m;i++)
		{
			a=read(); x=read(); b=read(); y=read(); ins[0]=0;
			if(dep[a]>dep[b]) swap(a,b), swap(x,y);
			if(fa[b]==a&&!x&&!y){puts("-1"); continue;}
			G[a][x^1]=G[b][y^1]=INF; 
			G[a][x]=F[a][x]; G[b][y]=F[b][y];
			ins[++ins[0]]=a; ins[++ins[0]]=b;
			while(a!=b)
			{
				ll d0=td[fa[b]][0],d1=td[fa[b]][1];
				d0-=F[b][1]; d0+=G[b][1];
				d1-=min(F[b][0],F[b][1]);
				d1+=min(G[b][0],G[b][1]);				
				if(fa[a]==fa[b])
				{
					d0-=F[a][1]; d0+=G[a][1];
					d1-=min(F[a][0],F[a][1]);
					d1+=min(G[a][0],G[a][1]);
				}				
				G[fa[b]][0]=G[fa[b]][0]==INF? INF:d0; 
				G[fa[b]][1]=G[fa[b]][1]==INF? INF:d1+vl[fa[b]];	
				if(fa[a]==fa[b]) a=b=fa[a];
				else b=fa[b];
				ins[++ins[0]]=b;
				if(dep[a]>dep[b]) swap(a,b);
			}
			while(b!=1)
			{
				ll d0=td[fa[b]][0],d1=td[fa[b]][1];
				d0-=F[b][1]; d0+=G[b][1];
				d1-=min(F[b][0],F[b][1]);
				d1+=min(G[b][0],G[b][1]);
				G[fa[b]][0]=G[fa[b]][0]==INF? INF:d0; 
				G[fa[b]][1]=G[fa[b]][1]==INF? INF:d1+vl[fa[b]];
				b=fa[b]; ins[++ins[0]]=b;				
			}
			printf("%lld\n",min(G[1][0],G[1][1]));
			for(int i=1;i<=ins[0];i++) G[ins[i]][0]=G[ins[i]][1]=0;
		}
	return 0;
}
