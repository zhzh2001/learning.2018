#include <bits/stdc++.h>
#define int long long
using namespace std;

int n,m,bk[10][10],Ans;
inline int cs(int x)
{
	int cnt=0; while(x)
		cnt+=x&1,x/=2; return cnt;
}
inline int vs(int x)
{
	int sx=0,sy=0,ret=bk[sx][sy];
	for(int i=0;i<n+m-2;i++)
	{
		if(x&(1ll<<(n+m-2-i-1))) sy++;
		else sx++;
		ret=ret*2+bk[sx][sy];
	}
	return ret;
}
signed main()
{
	cin >> n >> m;
	for(int i=0;i<(1ll<<(n*m));i++)
	{
		int fl=1;
		for(int j=0;j<n;j++)
			for(int k=0;k<m;k++)
				bk[j][k]=bool(i&(1ll<<(j*m+k)));
		for(int j=0;j<(1ll<<(n+m-2))&&fl;j++) if(cs(j)==m-1)
			for(int k=0;k<j&&fl;k++) if(cs(k)==m-1)
				if(vs(j)>vs(k)) fl=0;
		if(fl)
		{
			Ans++;
			for(int i=0;i<n;i++,puts(""))
				for(int j=0;j<m;j++) printf("%d ",bk[i][j]);
			puts("");
		}
	}
	cout << Ans << endl;
}
