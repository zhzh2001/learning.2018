#include <bits/stdc++.h>
#define mod 1000000007
using namespace std;

inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
inline int qpow(int bas,int pw)
{
	int ret=1; for(;pw;pw>>=1,bas=1ll*bas*bas%mod)
		if(pw&1) ret=1ll*ret*bas%mod; return ret;
}
int n,m;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin >> n >> m;
	if(n>m) swap(n,m);
	if(n==1) cout << 1ll*2*qpow(2,m-1) << endl;
	if(n==2) cout << 1ll*12*qpow(3,m-2)%mod << endl;
	if(n==3) cout << 1ll*112*qpow(3,m-3)%mod << endl;
	return 0;	
}
