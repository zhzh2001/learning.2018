#include <bits/stdc++.h>
#define N 6010
using namespace std;

inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
int n,m;
vector <int> V[N];
inline void dfs(int rt,int pr)
{
	printf("%d ",rt);
	for(int i=0;i<(int)V[rt].size();i++)
		if(V[rt][i]!=pr)  dfs(V[rt][i],rt);
}
namespace CASE1
{
	int incir[N],vis[N],fa[N],fl,ks;
	inline void findcir(int rt,int pr)
	{
		vis[rt]=1;
		for(int i=0;i<(int)V[rt].size();i++)
		{
			if(V[rt][i]==pr) continue;
			if(vis[V[rt][i]]&&!incir[V[rt][i]])
			{
				incir[rt]=incir[V[rt][i]]=1;
				int x=fa[rt]; while(x!=V[rt][i]) 
					incir[x]=1,x=fa[x];
				continue;
			}
			if(!vis[V[rt][i]]) fa[V[rt][i]]=rt, findcir(V[rt][i],rt);
		}
	}
	inline void print(int rt)
	{
		if(vis[rt]) return ;
		vis[rt]=1; printf("%d ",rt);
		if(incir[rt])
		{
			if(!fl)
			{ 
			for(int i=V[rt].size()-1;i>=0&&ks==-1;i--)
				if(incir[V[rt][i]]) ks=V[rt][i];
				fl=1;
			}
			for(int i=0;i<(int)V[rt].size();i++)
			{
				if(fl&&ks!=-1&&ks<V[rt][i])
				{
					for(int j=i;j<(int)V[rt].size();j++)
						if(!incir[V[rt][i]]) print(V[rt][i]);
					ks=-1; break;
				}
				else print(V[rt][i]);
			}
		}
		else 
		{
			for(int i=0;i<(int)V[rt].size();i++)
				if(!vis[V[rt][i]]) print(V[rt][i]);
		}
	}
	inline void solve()
	{
		fl=0; ks=-1;
		for(int i=1;i<=n;i++)
		{
			int u=read(), v=read();
			V[u].push_back(v);
			V[v].push_back(u);
		}
		for(int i=1;i<=n;i++) sort(V[i].begin(),V[i].end());
		findcir(1,0); 
		memset(vis,0,sizeof vis);
		print(1); puts("");
	}
}
namespace CASE2
{
	inline void solve()
	{
		for(int i=1;i<n;i++)
		{
			int u=read(), v=read();
			V[u].push_back(v);
			V[v].push_back(u);
		}
		for(int i=1;i<=n;i++) sort(V[i].begin(),V[i].end());
		dfs(1,0); puts("");
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(); m=read();
	if(m==n) CASE1::solve();
	else CASE2::solve();
	return 0;
}
