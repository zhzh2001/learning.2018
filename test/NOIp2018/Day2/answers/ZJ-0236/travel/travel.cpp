#include<bits/stdc++.h>
using namespace std;
#define res register int
#define LL long long
#define inf 0x3f3f3f3f
#define eps 1e-15
inline int read(){
	res s=0,w=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')w=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')s=s*10+ch-'0',ch=getchar();
	return s*w; 
}
const int N=5e3+10;
namespace MAIN{
	int n,m;
	struct E{
		int next,to;
		E() {}
		E(res next,res to):next(next),to(to) {}
	}edge[N<<1];
	int head[N],cnt;
	inline void addedge(const res &u,const res &v){
		edge[++cnt]=E(head[u],v),head[u]=cnt;
		edge[++cnt]=E(head[v],u),head[v]=cnt;
	}
	#define pb push_back
	namespace subtask0{
		priority_queue<int,vector<int>,greater<int> >Q;
		int st[N],top,vis[N];
		void dfs(const res &x){
			st[++top]=x,vis[x]=1;
			vector<int> vec;
			for(res i=head[x];~i;i=edge[i].next){
				res tox=edge[i].to;
				if(vis[tox])continue;
				vec.pb(tox);
			}
			sort(vec.begin(),vec.end());
			for(res i=0,siz=vec.size();i<siz;i++)dfs(vec[i]);
		}
		inline void MAIN(){
			dfs(1);
			for(res i=1;i<=n;i++)printf("%d ",st[i]);
		}
	}
	bool lj[N][N];
	namespace subtask1{
		int st[N],top,vis[N];
		priority_queue<int,vector<int>,greater<int> >Q;
		bool flag;
		int las;
		inline void bfs(){
			Q.push(1),vis[1]=1;
			while(top<n){
				res u;
				if(flag){
					for(res i=head[las];~i;i=edge[i].next){
						res tox=edge[i].to;
						if(vis[tox])continue;
						u=tox,vis[tox]=1;
					}
				}
				else u=Q.top(),Q.pop();
				st[++top]=u;
				if(!lj[las][u]&&u!=1)flag=1;
				for(res i=head[u];~i;i=edge[i].next){
					res tox=edge[i].to;
					if(vis[tox])continue;
					Q.push(tox),vis[tox]=1;
				}
				las=u;
			}
		}
		inline void MAIN(){
			bfs();
			for(res i=1;i<=n;i++)printf("%d ",st[i]);
		}
	}
	inline void MAIN(){
		n=read(),m=read();
		memset(head,-1,sizeof(head));
		for(res i=1;i<=m;i++){
			res u=read(),v=read();
			addedge(u,v);
			lj[u][v]=lj[v][u]=1;
		}
		if(m==n-1)subtask0::MAIN();
		else subtask1::MAIN();
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	MAIN::MAIN();
	return 0;
}
