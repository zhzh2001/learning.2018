#include<bits/stdc++.h>
using namespace std;
#define res register int
#define LL long long
#define inf 0x3f3f3f3f
#define eps 1e-15
inline int read(){
	res s=0,w=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')w=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')s=s*10+ch-'0',ch=getchar();
	return s*w; 
}
namespace MAIN{
	int n,m;
	char str[5];
	namespace subtask0{
		const int N=2e3+10;
		struct E{
			int next,to;
			E() {}
			E(res next,res to):next(next),to(to) {}
		}edge[N<<1];
		int head[N],cnt;
		inline void addedge(const res &u,const res &v){
			edge[++cnt]=E(head[u],v),head[u]=cnt;
			edge[++cnt]=E(head[v],u),head[v]=cnt;
		}
		int p[N];
		int dp[N][2];
		int bx[N];
		bool lj[N][N];
		inline void MAIN(){
			memset(head,-1,sizeof(head));
			for(res i=1;i<=n;i++)p[i]=read();
			for(res i=1;i<n;i++){
				res u=read(),v=read();
				addedge(u,v);
				lj[u][v]=lj[v][u]=1;
			}
			memset(bx,-1,sizeof(bx));
			while(m--){
				res a=read(),x=read(),b=read(),y=read();
				memset(dp,inf,sizeof(dp));
				if(!x&&!y&&lj[a][b]){puts("-1");continue;}
				bx[a]=x,bx[b]=y;
				if(bx[1]==-1)dp[1][0]=0,dp[1][1]=p[1];
				if(bx[1]==1)dp[1][1]=1;
				if(bx[1]==0)dp[1][0]=0;
				for(res i=2;i<=n;i++){
					if(bx[i]==1)dp[i][1]=min(dp[i-1][0],dp[i-1][1])+p[i];
					if(bx[i]==0)dp[i][0]=dp[i-1][1];
					if(bx[i]==-1)dp[i][0]=dp[i-1][1],dp[i][1]=min(dp[i-1][0],dp[i-1][1])+p[i];
				}
				bx[a]=-1,bx[b]=-1;
				res ans=inf;
				if(bx[n]!=0)ans=min(ans,dp[n][0]);
				if(bx[n]!=1)ans=min(ans,dp[n][1]);
				printf("%d\n",ans);
			}
		}
	}
	inline void MAIN(){
		n=read(),m=read();
		scanf("%s",str+1);
		subtask0::MAIN();
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	MAIN::MAIN();
	return 0;
}
