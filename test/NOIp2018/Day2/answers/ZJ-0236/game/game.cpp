#include<bits/stdc++.h>
using namespace std;
#define res register int
#define LL long long
#define inf 0x3f3f3f3f
#define eps 1e-15
inline int read(){
	res s=0,w=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')w=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')s=s*10+ch-'0',ch=getchar();
	return s*w; 
}
const int kcz=1e9+7;
namespace MAIN{
	int n,m;
	int dp[1000000+10][(1<<8)+10];
	inline void add(res &x,const res &y){
		x+=y;
		x>=kcz?x-=kcz:1;
	}
	int ans;
	void write(const res &x){
		if(x>1)write(x/2);
		putchar('0'+x%2);
	}
	inline int qpow(res x,res y){
		res ret=1;
		while(y){
			if(y&1)ret=1LL*ret*x%kcz;
			x=1LL*x*x%kcz,y>>=1;
		}
		return ret;
	}
	inline void MAIN(){
		n=read(),m=read();
		for(res S=0;S<(1<<n);S++)dp[1][S]=1;
		if(m==1){printf("%d\n",qpow(2,n));return;}
		if(n==1){printf("%d\n",qpow(2,m));return;}
		if(n==3&&m==3){puts("112");return;}
		if(n==2){printf("%d\n",1LL*qpow(3,m-1)*4%kcz);return;}
		if(m==2){printf("%d\n",1LL*qpow(3,n-1)*4%kcz);return;}
		for(res i=2;i<=m;i++){
			for(res S=0;S<(1<<n);S++){
				res SS=S>>1;
//				write(S),puts("");
//				write(SS),puts("");
//				write(S),printf(" %d\n",i);
				for(res PS=SS;;PS=(PS-1)&SS){
//					write(PS),puts("");
//					write(PS),printf(" %d ",dp[i-1][PS]),write(PS+(1<<(n-1))),printf(" %d\n",dp[i-1][PS+(1<<(n-1))]);
					add(dp[i][S],dp[i-1][PS]),add(dp[i][S],dp[i-1][PS+(1<<(n-1))]);
//					write(PS+(1<<(n-1))),puts("");
					if(PS==0)break;
				}
//				puts("...................");
//				printf("%d\n",dp[i][S]);
				if(i==m)add(ans,dp[i][S]);	
			}
		}
		printf("%d\n",ans);
	}
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	MAIN::MAIN();
	return 0;
}
