#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#define N 5050
#define ms
#define LL long long
using namespace std;
struct node{
	int u,v;
} b[N];
int dy[N],use[N];
int sd[N],ldl[N],rdl[N],fa[N],dl[N];
int n,m;
int dkl,dkr;

int cmp(node x,node y)
{
	if (x.u!=y.u) return x.u<y.u;
	return x.v<y.v;
}
int src(int k)
{
	use[k]=1; printf("%d ",k);
	for (int i=dy[k];i<dy[k+1];++i)
	  if ((k!=dkl || b[i].v!=dkr) && (k!=dkr || b[i].v!=dkl))
	    if (!use[b[i].v]) src(b[i].v);
	return 0;
}
int fid(int x,int y)
{
	int ls=1,rs=1;
	while (sd[x]>sd[y]) {
		ldl[++ls]=x; x=fa[x];
	}
	while (sd[x]<sd[y]) {
		rdl[++rs]=y; y=fa[y];
	}
	while (x!=y) {
		ldl[++ls]=x; x=fa[x];
		rdl[++rs]=y; y=fa[y];
	}
	if (ldl[ls]<rdl[rs])
	{
		while (ls>0 && ldl[ls]<rdl[rs]) ls--;
		if (ls>0) 
		{
			dkl=ldl[ls]; 
			dkr=ldl[ls+1];
			return 0;
		}
		while (ls+1<rs && rdl[ls]<rdl[rs]) ls++;
		dkl=rdl[ls];
		dkr=rdl[ls+1];
		return 0;
	}
	while (rs>0 && ldl[ls]>rdl[rs]) rs--;
	if (rs>0) 
	{
		dkl=rdl[rs]; 
		dkr=rdl[rs+1];
		return 0;
	}
	while (rs+1<ls && ldl[ls]>ldl[rs]) rs++;
	dkl=ldl[rs];
	dkr=ldl[rs+1];
	return 0;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d %d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		scanf("%d %d",&b[i].u,&b[i].v);
		b[m+i].u=b[i].v; b[m+i].v=b[i].u;
	}
	  
	sort(b+1,b+1+2*m,cmp);
	for (int i=1;i<=2*m;i++)
	  if (!dy[b[i].u]) dy[b[i].u]=i;
	dy[n+1]=2*m+1;
	
	if (m==(n-1)) {
		src(1); return 0;
	}
	
	int h=0,t=1,lwz=0,lwf=0; 
	dl[1]=1; fa[1]=1;
	while (h<t)
	{
		++h;
		for (int i=dy[dl[h]];i<dy[dl[h]+1];++i)
		  if (!fa[b[i].v]) 
		  {
		  	dl[++t]=b[i].v;
		  	fa[dl[t]]=dl[h];
		  	sd[dl[t]]=sd[dl[h]]+1;
		  }
		   else if (fa[dl[h]]!=b[i].v){
		   	lwz=b[i].v; lwf=dl[h]; break;
		   }
		if (lwz) break;
	}
	ldl[1]=lwz; rdl[1]=lwz;
	fid(fa[lwz],lwf);
	src(1);
	return 0;
}
