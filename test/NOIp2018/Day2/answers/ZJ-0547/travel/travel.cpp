#include<bits/stdc++.h>
using namespace std;
int n , m;
vector<int> E[5005];
vector<int> ans , ans2;
void dfs(int fa,int u)
{
	sort(E[u].begin() , E[u].end());
	ans2.push_back(u);
	for(int i = 0;i < E[u].size();i++){
		if(E[u][i] != fa){
			dfs(u , E[u][i]);
		}
	}
	return;
}
struct edge
{
	int u , v;
};
vector<edge> E2;
int p[5005];
int find(int x)
{
	return p[x] == x ? x : p[x] = find(p[x]);
}
bool check(int x)
{
	for(int i = 1;i <= n;i++) p[i] = i;
	for(int i = 0;i < E2.size();i++){
		if(i != x){
			int a = find(E2[i].u) , b = find(E2[i].v);
			if(a == b) return 0;
			p[a] = b;
		}
	}
	return 1;
}
void Rebuild(int x)
{
	for(int i = 1;i <= n;i++) E[i].clear();
	for(int i = 0;i < E2.size();i++){
		if(i == x) continue;
		E[E2[i].u].push_back(E2[i].v);
		E[E2[i].v].push_back(E2[i].u);
	}
	return;
}
void update()
{
	if(ans.size() == 0){
		for(int i = 0;i < ans2.size();i++){
			ans.push_back(ans2[i]);
		}
		return;
	}
	for(int i = 0;i < ans.size();i++){
		if(ans[i] < ans2[i]) return;
		else if(ans[i] > ans2[i]) break;
	}
	ans.clear();
	for(int i = 0;i < ans2.size();i++) ans.push_back(ans2[i]);
	return;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i = 1;i <= m;i++){
		int u , v;scanf("%d%d",&u,&v);
		E[u].push_back(v);E[v].push_back(u);
		edge w;w.u = u;w.v = v;
		E2.push_back(w);
	}
	if(m == n - 1){
		dfs(0 , 1);
		for(int i = 0;i < ans2.size();i++){
			if(i != ans2.size() - 1) printf("%d ",ans2[i]);
			else printf("%d",ans2[i]);
		}
		return 0;
	}
	for(int i = 0;i < E2.size();i++){
		if(check(i)){
			Rebuild(i);ans2.clear();
			dfs(0 , 1);
			update();
		}
	}
	for(int i = 0;i < ans.size();i++){
		if(i != ans.size() - 1)printf("%d ",ans[i]);
		else printf("%d",ans[i]);
	}
	return 0;
}
