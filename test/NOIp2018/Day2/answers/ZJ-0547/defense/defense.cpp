#include<bits/stdc++.h>
using namespace std;
vector<int> E[100005];
int n , m;
long long f[100005] , g[100005];
long long rf[100005] , rg[100005];
long long rrf[100005] , rrg[100005];
int vis[100005];
int fa[100005] , p[100005];
void dfs(int pf,int u)
{
	f[u] = g[u] = 0;fa[u] = pf;f[u] = p[u];
	for(int i = 0;i < E[u].size();i++){
		if(E[u][i] != pf){
			dfs(u , E[u][i]);
			f[u] += min(f[E[u][i]] , g[E[u][i]]);
			g[u] += f[E[u][i]];
		}
	}
	return;
}
void upd(int a,int x,int b,int y)
{
	rf[a] = f[a];rg[a] = g[a];
	if(x) rg[a] = 1e16;
	else rf[a] = 1e16;
	//printf("set %d , %lld %lld\n",a,rf[a],rg[a]);
	int lst = a;a = fa[a];vis[lst] = vis[a] = m + 1;
	while(a != 0){
		rf[a] = f[a];rg[a] = g[a];
		rf[a] = (rf[a] - min(f[lst] , g[lst]) + min(rf[lst] , rg[lst]));
		rg[a] = (rg[a] - f[lst] + rf[lst]);lst = a;
	//	printf("set %d , %lld %lld\n",a,rf[a],rg[a]);
		a = fa[a];vis[a] = m + 1;
	}
	if(vis[b] == m + 1) rrf[b] = rf[b] , rrg[b] = rg[b];
	else rf[b] = rrf[b] = f[b] , rg[b] = rrg[b] = g[b];
	if(y) rrg[b] = 1e16;
	else rrf[b] = 1e16;
//	printf("Set %d , %lld %lld\n",b,rrf[b],rrg[b]);
	lst = b;b = fa[b];
	while(b != 0){
		if(vis[b] == m + 1){
			rrf[b] = rf[b] , rrg[b] = rg[b];
			rrf[b] = (rrf[b] - min(rf[lst] , rg[lst]) + min(rrf[lst] , rrg[lst]));
		//	printf("  %d , %lld %lld\n",lst,min(rf[lst] , rg[lst]) ,min(rrf[lst] , rrg[lst]));
			rrg[b] = (rrg[b] - rf[lst] + rrf[lst]);
		}
		else {
			rf[b] = f[b] , rg[b] = g[b];
			rrf[b] = f[b] , rrg[b] = g[b];
			rrf[b] = (rrf[b] - min(f[lst] , g[lst]) + min(rrf[lst] , rrg[lst]));
			rrg[b] = (rrg[b] - f[lst] + rrf[lst]);
		}
		//printf("set %d , %lld %lld\n",b,rrf[b],rrg[b]);
		lst = b;b = fa[b];
	}
	return;
}
int ba,bx,bb,by;
void ddfs(int pf,int u)
{
	f[u] = p[u];g[u] = 0;
	if(u == ba && bx) g[u] = 1e16;
	if(u == ba && !bx) f[u] = 1e16;
	if(u == bb && by) g[u] = 1e16;
	if(u == bb && !by) f[u] = 1e16;
	for(int i = 0;i < E[u].size();i++){
		if(E[u][i] != pf){
			ddfs(u , E[u][i]);
			f[u] += min(f[E[u][i]] , g[E[u][i]]);
			g[u] += f[E[u][i]];
		}
	}
	return;
}
void brute()
{
	while(m--){
		scanf("%d%d%d%d",&ba,&bx,&bb,&by);
		ddfs(0 , 1);
		long long w = min(f[1] , g[1]);
		if(w >=(1e15)) puts("-1");
		else cout<<w<<endl;
	}
	return;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);string s;cin>>s;
	for(int i = 1;i <= n;i++){
		scanf("%d",&p[i]);
	}
	for(int i = 1;i < n;i++){
		int u , v;scanf("%d%d",&u,&v);E[u].push_back(v);E[v].push_back(u);
	}
	if(n <= 2000){
		brute();return 0;
	}
	dfs(0 , 1);
	while(m--){
		int a , x , b , y;scanf("%d%d%d%d",&a,&x,&b,&y);
		upd(a , x , b , y);
		long long w = min(rrf[1] , rrg[1]);
		if(w >= (1e15)) puts("-1");
		else printf("%lld\n",min(rrf[1] , rrg[1]));
	}
	return 0;
}
