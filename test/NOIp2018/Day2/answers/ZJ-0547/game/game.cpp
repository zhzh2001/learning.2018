#include<bits/stdc++.h>
using namespace std;
int n , m;
const int mod = 1e9 + 7;
int a[10][10];
int f[10][10] , g[10][10];
int power(int a,int b)
{
	int ans = 1;
	while(b){
		if(b & 1) ans = (1LL * ans * a) % mod;
		a = (1LL * a * a) % mod;b >>= 1;
	}
	return ans;
}
bool check()
{
	for(int i = n - 1;i >= 0;i--){
		for(int j = m-1;j >= 0;j--){
			int c = n - 1 - i + m - 1 - j;
			f[i][j] = g[i][j] = 0;
			f[i][j] = (a[i][j] << c) + max(f[i+1][j] , f[i][j+1]);
			g[i][j] = 1e9;
			if(i + 1 < n) g[i][j] = (a[i][j] << c) + g[i+1][j];
			if(j + 1 < m) g[i][j] = min(g[i][j] , (a[i][j] << c) + g[i][j+1]);
			if(i == n-1 && j == m-1) g[i][j] = a[i][j];
		}
	}
	for(int i = 0;i < n;i++){
		for(int j = 0;j < m;j++){
			if(i+1 >= n || j - 1 < 0) continue;
			if(f[i][j] > g[i+1][j-1]) return 0;
		}
	}
	return 1;
}
void brute()
{
	int ans = 0;
	for(int i = 0;i < (1<<(n*m));i++){
		for(int j = 0;j < n;j++){
			for(int k = 0;k < m;k++){
				int num = (j*m+k);
				a[j][k] = ((i >> num) & 1);
			}
		}
		if(check()){
			ans++;
		}
	}
	cout<<ans<<endl;return;
}
int h4[10] = {16,100,336,912,2688,8064};
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n > m) swap(n , m);
	if(n == 1){
		printf("%d\n",power(2 , m));return 0;
	}
	if(n == 2){
		int t = (4LL * power(3,m-1)) % mod;
		printf("%d\n",t);return 0;
	}
	if(n * m <= 9){
		brute();return 0;
	}
	if(n == 3){
		int t = (112LL * power(3 , m - 3)) % mod;
		printf("%d\n",t);return 0;
	}
	if(n == 4){
		if(m <= 6) printf("%d\n",h4[m-1]);
		else{
			int t = (1LL * h4[5] * power(3 , m - 6)) % mod;
			printf("%d\n",t);
		}
		return 0;
	}
	if(n == 5){
		printf("7136");return 0;
	}
	return 0;
}
