#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn_=5005,maxm_=maxn_*2;
int n,m,tot,tp,lnk[maxn_],son[maxm_],nxt[maxm_],ans[5*maxn_],st[10*maxn_];
int h[maxn_],f[maxn_],len,v[maxn_],mp[maxn_][maxn_],flg_,pd_,u[maxn_];
int rad()
{
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
void add_(int x,int y){son[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;}
void dfs1(int x)
{
	if (v[x]) return;
	v[x]=1;
	ans[++ans[0]]=x;
	int L=tp+1,R;
	for (int i=lnk[x];i>0;i=nxt[i])
	  if (!v[son[i]]) st[++tp]=son[i];
	R=tp;
	if (L>R) return;
	sort(st+L,st+R+1);
	for (int i=L;i<=R;++i) dfs1(st[i]);
	tp=L-1;
}
void P1_()
{
	dfs1(1);
	for (int i=1;i<n;++i) printf("%d ",ans[i]);
	printf("%d",ans[n]);
}
void put_(int x,int fax_)
{
	++len;h[len]=x;f[x]=fax_;
	for (int fa=len;fa>1;fa>>=1)
	  if (h[fa]<h[fa>>1]) {swap(h[fa],h[fa>>1]);swap(f[h[fa]],f[h[fa>>1]]);}
	  else break;
}
void del_()
{
	h[1]=h[len];f[h[1]]=f[h[len]];--len;
	for (int fa=1,son=2;son<=len;son<<=1)
	{
		son|=(int)(((son|1)<=len)&&(h[son|1]<h[son]));
		if (h[fa]>h[son]) {swap(h[fa],h[son]);swap(f[h[fa]],f[h[son]]);fa=son;}
		else break;
	}
}
void dfs2(int x)
{
	v[x]=1;
	int pd=0;
	for (int i=lnk[x];i!=0&&flg_==0;i=nxt[i])
	  if (!v[son[i]]) dfs2(son[i]);
	  else {if (!pd) pd=1;else {flg_=son[i];break;}}
	if (flg_!=0&&pd_) u[x]=flg_;
	if (x==flg_) pd_=0;
}
void dfs3(int x)
{
	if (v[x]) return;
	v[x]=1;
	ans[++ans[0]]=x;
	int L=tp+1,R;
	if (u[x]!=flg_||pd_==0)
	{
		for (int i=lnk[x];i;i=nxt[i])
		{
			if (v[son[i]]) continue;
			st[++tp]=son[i];
		}
		R=tp;
		if (L>R) return;
		sort(st+L,st+R+1);
		for (int i=L;i<=R;++i) dfs3(st[i]);
		tp=L-1;
	}else
	{
		int tt=0;
		int L=tp+1,R;
		for (int i=lnk[x];i;i=nxt[i])
		{
			if (v[son[i]]) continue;
			put_(son[i],x);++tt;
		}
		{
			while (tt>0)
			{
				if (f[h[1]]==x||pd_==0) {--tt;int p=h[1];del_();dfs3(p);}
				else{
					pd_=0;
					int p=f[h[1]];
					v[p]=0;
					for (int i=lnk[x];i;i=nxt[i]) if (!v[son[i]]&&u[son[i]]!=flg_) st[++tp]=son[i];
					R=tp;
					if (L>R) return;
					sort(st+L,st+R+1);
					for (int i=L;i<=R;++i) dfs3(st[i]);
					dfs3(p);
					tp=L-1;
					break;
				}
			}
		}
	}
}
void P2_()
{
	flg_=0;pd_=1;
	dfs2(1);
	memset(v,0,sizeof v);
	pd_=1;
	dfs3(1);
	memset(v,0,sizeof v);
	n=ans[0];
	printf("%d",ans[1]);v[ans[1]]=1;
	for (int i=2;i<=n;++i)
	  if (!v[ans[i]]) {printf(" %d",ans[i]);v[ans[i]]=1;}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=rad();m=rad();
	for (int i=1;i<=m;++i)
	{
		int x=rad(),y=rad();
		add_(x,y);add_(y,x);
		mp[x][y]=mp[y][x]=1;
	}
	if (m==n-1) P1_();
	else P2_();
	return 0;
}

