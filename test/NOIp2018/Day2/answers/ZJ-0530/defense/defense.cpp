#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=1e5+5,maxm=maxn*2;
int n,m,tot,lnk[maxn],nxt[maxm],son[maxm],a[maxn],s[5],t[5],id[maxn];
long long f[maxn][2];
int rad()
{
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
void add_(int x,int y){son[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;}
void mi_(long long&x,long long y){if (y<1e17) x+=y;else x=1e17;}
void dfs(int x,int fa)
{
	int pd=0;
	f[x][0]=0;f[x][1]=a[x];
	for (int i=lnk[x];i;i=nxt[i])
	{
		if (fa==son[i]) continue;
		dfs(son[i],x);
		pd=1;
		if (x!=s[1]&&x!=s[2])
		{
			mi_(f[x][0],f[son[i]][1]);
			mi_(f[x][1],min(f[son[i]][0],f[son[i]][1]));
		}else
		if (x==s[1])
		{
			if (t[1]==0) mi_(f[x][0],f[son[i]][1]);else
			{
				id[son[i]]=(f[son[i]][0]<f[son[i]][1])?0:1;
				mi_(f[x][1],f[son[i]][id[son[i]]]);
			}
		}else if (x==s[2])
		{
			if (t[2]==0) mi_(f[x][0],f[son[i]][1]);else
			{
				id[son[i]]=(f[son[i]][0]<f[son[i]][1])?0:1;
				mi_(f[x][1],f[son[i]][id[son[i]]]);
			}
		}
	}
	if (!pd)
	{
		f[x][0]=0;f[x][1]=a[x];
		if (x==s[1]) f[x][t[1]^1]=(long long)(1e17);
		if (x==s[2]) f[x][t[2]^1]=(long long)(1e17);
	}
	if (x==s[1]) f[x][t[1]^1]=(long long)(1e17);
	if (x==s[2]) f[x][t[2]^1]=(long long)(1e17);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=rad();m=rad();rad();
	for (int i=1;i<=n;++i) a[i]=rad();
	for (int i=1;i<n;++i)
	{
		int x=rad(),y=rad();
		add_(x,y);add_(y,x);
	}
	for (int i=1;i<=m;++i)
	{
		s[1]=rad();t[1]=rad();
		s[2]=rad();t[2]=rad();
		if (s[1]==s[2]&&t[1]!=t[2]) {puts("-1");continue;}
		dfs(1,0);
		long long ans;
		if (s[1]==1) ans=f[1][t[1]];else
		if (s[2]==1) ans=f[1][t[2]];else
		ans=min(f[1][0],f[1][1]);
		if (ans>=1e17) puts("-1");else printf("%lld\n",ans);
	}
	return 0;
}
