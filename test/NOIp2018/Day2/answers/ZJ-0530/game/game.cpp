#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=15,maxm=1000005;
int n,m,ans,a[maxn][maxm],s1[maxn*2],s2[maxn*2];
int rad()
{
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int g(int x)
{
	int s=0;
	while (x) {s+=x&1;x>>=1;}
	return s; 
}
void check_()
{
	int Len=n+m-2;
	for (int i=(1<<n+m-2)-1;i>=0;--i)
	{
		if (g(i)!=n-1) continue;
		int x=i,y,S1=0;s1[0]=0;
		for (int t=1;t<=Len;++t) {s1[t]=x&1;x>>=1;}
		x=y=1;
		for (int t=Len;t>=0;--t)
		{
			S1=S1<<1|a[x][y];
			if (s1[t]) ++y;else ++x;
		}
		for (int j=i-1;j>=0;--j)
		{
			if (g(j)!=n-1) continue;
			int xx=j,yy,S2=0;s2[0]=0;
			for (int t=1;t<=Len;++t) {s2[t]=xx&1;xx>>=1;}
			xx=yy=1;
			for (int t=Len;t>=0;--t)
			{
				S2=S2<<1|a[xx][yy];
				if (s2[t]) ++yy;else ++xx;
			}
			if (S1>S2) return;
		}
	}
	++ans;
}
void dfs(int x,int y)
{
	if (y>m) {y=1;++x;}
	if (x>n) {check_();return;}
	a[x][y]=0;
	dfs(x,y+1);
	a[x][y]=1;
	dfs(x,y+1);
}
int qsm(int x,int y)
{
	long long s=1,w=x;
	int tt=1e9+7;
	while (y)
	{
		if (y&1) s=s*w%tt;
		w=w*w%tt;y>>=1;
	}
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
//	for (int T=rad();T;--T)
	{
		n=rad();m=rad();
		if (n==1)
		{
			printf("%d",qsm(2,m));
			return 0;
		}
		if (n==2&&m>8)
		{
			printf("%lld",1ll*qsm(4,m-3)*24%((int)(1e9+7)));
			return 0;
		}
		ans=0;
		dfs(1,1);
		printf("%d\n",ans);
	}
	return 0;
}
