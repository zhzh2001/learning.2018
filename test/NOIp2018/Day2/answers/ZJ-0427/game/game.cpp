#include <bits/stdc++.h>
using namespace std;
const int p=1e9+7;
int n,m,f[2000005];
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i)
	{
		f[i]++;
		int x=i;
		for (int j=2;j<=m;++j)
			f[++x]++;
	}
	int ans=1;
	for (int i=1;i<=2*n;++i)
		ans=(ans*(f[i]+1))%p;
	printf("%d\n",ans);
	return 0;
}
