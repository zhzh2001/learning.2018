#include <bits/stdc++.h>
using namespace std;
const int M=1e9+7;
const int P=1e6+10;
int f[P][9];
int n,m;
int lim(int x){
	if (x<=n) return x;
	if (n+m-x<=n) return n+m-x;
	return n;
}
int main(){
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	f[1][0]=f[1][1]=1;
	for (int i=2; i<=n+m-1; ++i){
		int a=lim(i-1);
		int b=lim(i);
		for (int j=0; j<=a; ++j)
			for (int k=(j<b?j:b); k<=b; ++k)
				(f[i][k]+=f[i-1][j])%=M;
		for (int j=0; j<=b; ++j) cerr<<i<<" "<<j<<" "<<f[i][j]<<endl;
	}
	cout<<(f[n+m-1][0]+f[n+m-1][1])%M;
}
