#include <bits/stdc++.h>
using namespace std;
const int N=100010;
const long long INF=1e10+7;
long long f[N][2];
int n,m,p[N],b1,s1,b2,s2;
int ne[N<<1],k,fi[N],b[N<<1];
void add(int x,int y){
	ne[++k]=fi[x]; b[fi[x]=k]=y;
}
void dfs(int x,int fa){
	f[x][0]=0; f[x][1]=p[x];
	for (int j=fi[x]; j; j=ne[j])
		if (b[j]!=fa){
			dfs(b[j],x);
			f[x][0]+=f[b[j]][1];
			f[x][1]+=min(f[b[j]][0],f[b[j]][1]);
		}
	if (x==b1){
		f[x][s1^1]=INF;
	}
	else if (x==b2){
		f[x][s2^1]=INF;
	}
}
int main(){
	scanf("%d%d",&n,&m);
	scanf("%*s");
	for (int i=1; i<=n; ++i) scanf("%d",&p[i]);
	for (int i=1; i<n; ++i){
		int x,y; scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	for (int i=1; i<=m; ++i){
		scanf("%d%d%d%d",&b1,&s1,&b2,&s2);
		dfs(1,0);
		long long t=min(f[1][0],f[1][1]);
		cout<<(t<INF?t:-1)<<'\n';
	}
}
