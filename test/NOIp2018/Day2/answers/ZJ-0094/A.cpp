#include <bits/stdc++.h>
using namespace std;
const int N=5010;
int clk,tlk[N],n,m;
vector<int> g[N];
void dfs(int x,int fa){
	tlk[++clk]=x;
	for (vector<int>::iterator it=g[x].begin(); it!=g[x].end(); ++it){
		int v=*it;
		if (v!=fa){
			dfs(v,x);
		}
	}
}
int ban1,ban2;
void ddfs(int x,int fa){
	tlk[++clk]=x;
	for (vector<int>::iterator it=g[x].begin(); it!=g[x].end(); ++it){
		int v=*it;
		if (v!=fa&&!((x==ban1&&v==ban2)||(x==ban2&&v==ban1))){
			ddfs(v,x);
		}
	}
}
int top,st[N],gg[N];
int beb,mtop,ans[N];
bool getloop(int x,int fa){
	//cerr<<"gal"<<x<<" "<<fa<<endl;
	st[++top]=x;
	gg[x]=top;
	for (vector<int>::iterator it=g[x].begin(); it!=g[x].end(); ++it){
		int v=*it;
		if (v!=fa){
			if (gg[v]){
				beb=gg[v];
				return 1;
			}
			if (getloop(v,x)) return 1;
		}
	}
	--top;
	gg[x]=0;
	return 0;
}
bool bbb(int *a,int *b){
	for (int i=1; i<=n; ++i) if (a[i]!=b[i]) return a[i]>b[i];
	return 0;
}
int main(){
	scanf("%d%d",&n,&m);
	for (int i=1; i<=m; ++i){
		int x,y;
		scanf("%d%d",&x,&y);
		g[x].push_back(y);
		g[y].push_back(x);
	}
	for (int i=1; i<=n; ++i)
		sort(g[i].begin(),g[i].end());
	if (m==n-1){
		dfs(1,0);
		for (int i=1; i<=n; ++i) cout<<tlk[i]<<' ';
	}
	else{
		getloop(1,0);
		//cerr<<"!!!!"<<endl;
		//for (int i=1; i<=top; ++i) cerr<<st[i]<<" ";
		for (int i=beb; i<=top; ++i){
			ban1=st[i];
			ban2=(i==top?st[beb]:st[i+1]);
			//cerr<<"ban"<<ban1<<" "<<ban2<<" "<<beb<<endl;
			clk=0;
			ddfs(1,0);
			if (mtop==0||bbb(ans,tlk)){
				mtop=clk;
				for (int i=1; i<=mtop; ++i) ans[i]=tlk[i];
			}
		}
		for (int i=1; i<=n; ++i) cout<<ans[i]<<' ';
	}
}
