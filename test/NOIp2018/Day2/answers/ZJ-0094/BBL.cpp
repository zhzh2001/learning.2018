#pragma GCC optimize(3)
#pragma GCC optimize("Ofast")
#pragma GCC optimize("inline")
#include <bits/stdc++.h>
using namespace std;
const int N=10;
int n,m,dd;
bool mp[N][N];
int main(){
	scanf("%d%d",&n,&m);
	int ans=0;
	for (int i=0; i<(1<<n*m); ++i){
		for (int j=0; j<n; ++j)
			for (int k=0; k<m; ++k)
				if (i>>(j*m+k)&1) mp[j][k]=1; else mp[j][k]=0;
		if (mp[0][0]||mp[n-1][m-1]) continue;
		int mi=-INT_MAX,cnt=1;
		for (int j=0; j<(1<<(n+m-2)); ++j){
			int now=mp[0][0],px=0,py=0;
			bool fl=0;
			for (int k=n+m-3; k>=0; --k)
				if (j>>k&1){
					px++;
					//cerr<<"K"<<k<<endl;
					if (px>=n){
						fl=1;
						break;
					}
					now=(now<<1)|mp[px][py];
				}
			    else{
					py++;
					//cerr<<"KK"<<k<<endl;
					if (py>=m){
						fl=1;
						break;
					}
					now=(now<<1)|mp[px][py];
				}
			//cerr<<i<<" "<<j<<" "<<now<<" "<<fl<<" "<<px<<" "<<py<<" "<<mi<<endl;
			if (fl) continue;
			if (now<mi){
				cnt=0;
				break;
			}
			mi=now;
		}
		if (cnt&&mp[0][0]==0&&mp[0][1]==0&&mp[1][0]==1){
			cerr<<"@@@@@@@@@"<<i<<endl;
			for (int i=0; i<n; ++i,cout<<endl)
				for (int j=0; j<m; ++j)
					cout<<mp[i][j]<<" ";
			++dd;
		}
		ans+=cnt;
		//break;
	}
	cerr<<dd<<endl;
	cout<<ans*4;
}
