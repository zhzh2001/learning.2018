#include <bits/stdc++.h>
using namespace std;
const int N=2;
const int MM=1000010;
const int MOD=1e9+7;
int f[MM][1<<N];//care kong jian
int n,m;
void L(int &x){
	x>=MOD?x-=MOD:0;
}
bool mat(int a,int b){
	for (int j=1; j<n; ++j)
		if (!((a>>j)&1)){
			if ((b>>(j-1))&1) return 0;
		}
	return 1;
}
bool mat2(int a,int b){
	int aa=0,bb=0;
	for (int j=1; j<n; ++j){
		aa=(aa<<1)|((a>>j)&1);
		bb=(bb<<1)|((b>>(j-1))&1);
		if (aa<bb) return 0;
	}
	return 1;
}
int main(){
	scanf("%d%d",&n,&m);
	if (n==1){
		int ans=2;
		for (int i=2; i<=m; ++i) ans=ans*2ll%MOD;
		cout<<ans;
		return 0;
	}
	if (n==3){
		if (m==1) cout<<8<<endl;
		else if (m==2) cout<<36<<endl;
		else{
			int ans=112;
			for (int i=4; i<=m; ++i) ans=ans*3ll%MOD;
			cout<<ans;
		}
		return 0;
	}
	if (n==2){
		for (int i=0; i<(1<<n); ++i) f[1][i]=1;
		for (int j=2; j<=m; ++j){
			for (int i=0; i<(1<<n); ++i)
				for (int k=0; k<(1<<n); ++k)
					if (mat(i,k)){
						//cerr<<j<<" "<<i<<" "<<k<<" "<<f[j-1][i]<<endl;
						L(f[j][k]+=f[j-1][i]);
					}
			//cerr<<j<<endl;
		}
		int ans=0;
		for (int i=0; i<(1<<n); ++i) L(ans+=f[m][i]);
		cout<<ans;
		return 0;
	}
	if (n>m) swap(n,m);
	if (n==4){
		if (m==4) cout<<912;
		else if (m==5) cout<<2688;
		else if (m==6) cout<<8064;
		else if (m==7) cout<<21492;
		return 0;
	}
	if (n==5){
		if (m==5) cout<<7136;
		else if (m==6) cout<<21312;
		return 0;
	}
}
