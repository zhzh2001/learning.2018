#include<bits/stdc++.h>
using namespace std;
const int N=100000+5;
int n,m,p[N],rd[N],st,a,x,b,y,ans,top;
int head[N],ver[2*N],Next[2*N],tot;
char c[5];
bool v[N];
void add(int x,int y){
	tot++;ver[tot]=y;Next[tot]=head[x];head[x]=tot;
}
void dfs(int u,int sum,int pre,int k){
	if(sum>=ans)return ;
	if(((u==a&&x==0)||(u==b&&y==0))&&pre==0)return ;
	if(k>n){ans=min(ans,sum);return ;}
	v[u]=1;
	for(int i=head[u];i;i=Next[i]){
		int vv=ver[i];
		if(v[vv]&&k<n)continue;
		if(u==a&&x==0)dfs(vv,sum,0,k+1);
		else if(u==a&&x==1)dfs(vv,sum+p[u],1,k+1);
		else if(u==b&&y==0)dfs(vv,sum,0,k+1);
		else if(u==b&&y==1)dfs(vv,sum+p[u],1,k+1);
		else if(pre==1||pre==-1)dfs(vv,sum,0,k+1),dfs(vv,sum+p[u],1,k+1);
		else if(pre==0)dfs(vv,sum+p[u],1,k+1);
	}
	v[u]=0;
}
void solve(){
	memset(v,0,sizeof(v));
	ans=0x3f3f3f3f;
	dfs(st,0,-1,1);
	if(ans!=0x3f3f3f3f)printf("%d\n",ans);
	else printf("-1\n");
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);scanf("%s",c);
	for(int i=1;i<=n;i++)scanf("%d",&p[i]);
	for(int i=1;i<n;i++){
		int u,vv;scanf("%d%d",&u,&vv);
		add(u,vv);add(vv,u);rd[u]++,rd[vv]++;
	}
	for(st=1;st<=n;st++)if(rd[st]==1)break;
	for(int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
