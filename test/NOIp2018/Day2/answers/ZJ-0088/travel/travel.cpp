#include<bits/stdc++.h>
using namespace std;
const int N=5000+5;
int n,m,ans[N];
int head[N],ver[2*N],Next[2*N],tot;
bool v[N];
int Read(){
	int s=0,w=1;char c;
	c=getchar();
	while(c<'0'||c>'9'){if(c=='-')w=-1;c=getchar();}
	while(c>='0'&&c<='9'){s=s*10+c-'0';c=getchar();}
	return s*w;
}
void add(int x,int y){
	tot++;ver[tot]=y;Next[tot]=head[x];head[x]=tot;
}
void solve_tree(){
	int cnt=0;
	ans[++cnt]=1;v[1]=1;
	while(cnt<n){
		int minn=0x3f3f3f3f;
		for(int i=cnt;i>=1;i--){
			int x=ans[i];
			for(int j=head[x];j;j=Next[j]){
				int y=ver[j];
				if(v[y])continue;
				minn=min(y,minn);
			}
			if(minn!=0x3f3f3f3f)break;
		}
		ans[++cnt]=minn;v[minn]=1;
	}
	for(int i=1;i<=n;i++)printf("%d ",ans[i]);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(v,0,sizeof(v));
	n=Read();m=Read();
	for(int i=1;i<=m;i++){
		int x,y;x=Read();y=Read();
		add(x,y);add(y,x);
	}
	if(m==n-1)solve_tree();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
