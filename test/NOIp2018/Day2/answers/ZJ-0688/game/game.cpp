#include <bits/stdc++.h>
using namespace std;
#define ll long long
const ll mod=1e9+7;

int n,m;
int dp[50][1<<10],a[50],b[50];
ll ans;

bool check(int x,int y) {
	for (int i=1;i<n;i++)
		if ((x>>i&1)<(y>>(i-1)&1)) return 0;
	return 1;
}

bool che() {
	int now=0;
	for (int i=1;i<m;i++) b[i]=0;
	for (int i=m;i<=n+m-2;i++) b[i]=1;
	do {
		int nx=0,ny=0,nz=a[0]&1;
		for (int i=1;i<=n+m-2;i++) {
			if (!b[i]) ny++; else nx++;
			nz=nz*2+(a[ny]>>nx&1);
		}
		if (nz<now) return 0;
		now=nz;
	} while (next_permutation(b+1,b+n+m-1));
	return 1;
}

void dfs(int x,int last) {
	if (x==m) {
		if (che()) ans++;
		return;
	}
	for (int i=0;i<(1<<n);i++)
		if (check(last,i)) {
			a[x]=i;
			dfs(x+1,i);
		}
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	if (n==1) {
		ans=1;
		for (;m;m--) ans=ans*2%mod;
		printf("%lld\n",ans);
		return 0;
	}
	if (n==2) {
		ans=4;
		for (--m;m;m--) ans=ans*3%mod;
		printf("%lld\n",ans);
		return 0;
	}
	if (n==3) {
		ans=112;
		for (m-=3;m;m--) ans=ans*3%mod;
		printf("%lld\n",ans);
		return 0;
	}
	if (n==4) {
		if (m==4) puts("912");
		ans=912;
		for (m-=4;m;m--) ans=ans*3%mod;
		printf("%lld\n",ans);
		return 0;
	}	
	
	dfs(0,(1<<n)-1);
	printf("%lld\n",ans%mod);
	return 0;
}
