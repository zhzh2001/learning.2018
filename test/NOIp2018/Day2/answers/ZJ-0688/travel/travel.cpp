#include <bits/stdc++.h>
using namespace std;
#define N 10010

int n,m,cnt,tot,clo,top,b0,b1,scc;
int a[N],ans[N],u[N],v[N],low[N],dfn[N],st[N],ins[N],bel[N],sz[N];
vector<int> G[N];

void dfs(int x,int fa) {
	a[++tot]=x;
	for (int i=0;i<(int)G[x].size();i++)
		if (G[x][i]!=fa) {
			if ((x==b0 && G[x][i]==b1) || (x==b1 && G[x][i]==b0)) continue;
			dfs(G[x][i],x);	
		}
}

void tarjan(int x,int fa) {
	low[x]=dfn[x]=++clo;
	st[++top]=x; ins[x]=1;
	for (int i=0;i<(int)G[x].size();i++) {
		int v=G[x][i];
		if (v==fa) continue;
		if (!dfn[v]) {
			tarjan(v,x);
			low[x]=min(low[x],low[v]);
		} else
		if (ins[v]) low[x]=min(low[x],dfn[v]);
	}
	if (low[x]==dfn[x]) {
		int now=0; scc++;
		while (now!=x) {
			now=st[top--];
			bel[now]=scc;
			sz[scc]++;
		}
	}
}

bool smaller(int* a,int* b) {
	for (int i=1;i<=n;i++) {
		if (a[i]<b[i]) return 1;
		if (a[i]>b[i]) return 0;
	}
	return 0;
}

int x,y;
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++) {
		scanf("%d%d",&x,&y);
		G[x].push_back(y);
		G[y].push_back(x);
		u[i]=x; v[i]=y;
	}
	for (int i=1;i<=n;i++) ans[i]=n;
	for (int i=1;i<=n;i++) sort(G[i].begin(),G[i].end());
	if (m==n-1) {
		dfs(1,0);
		for (int i=1;i<=n;i++) printf(i==n ? "%d\n" : "%d ",a[i]);
	} else {
		tarjan(1,0);
		for (int i=1;i<=m;i++)
			if (sz[bel[u[i]]]>1 && sz[bel[v[i]]]>1) {
				b0=u[i]; b1=v[i];
				tot=0; dfs(1,0);
				if (smaller(a,ans)) memcpy(ans,a,sizeof(a));
			}
		for (int i=1;i<=n;i++) printf(i==n ? "%d\n" : "%d ",ans[i]);	
	}
	return 0;
}
