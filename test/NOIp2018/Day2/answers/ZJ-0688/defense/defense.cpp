#include <bits/stdc++.h>
using namespace std;
#define N 200100
#define ll long long
#define INF 0x3f3f3f3f3f3f

struct edge {int nex,to;}e[N<<1];
int n,m,cnt;
int head[N],ban[N][2],fa[N];
ll f[N][2],p[N];
char ch[5];

void add(int x,int y) {
	e[++cnt].to=y;
	e[cnt].nex=head[x];
	head[x]=cnt;
}

void dfs(int x,int father) {
	f[x][0]=0; f[x][1]=p[x];
	fa[x]=father;
	for (int i=head[x];i;i=e[i].nex) {
		int v=e[i].to;
		if (v==fa[x]) continue;
		dfs(v,x);
		f[x][0]+=f[v][1];
		f[x][1]+=min(f[v][0],f[v][1]);
	}
	if (ban[x][0]) f[x][1]=INF;
	if (ban[x][1]) f[x][0]=INF;
}

int a,b,x,y;
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,ch);
	for (int i=1;i<=n;i++) scanf("%lld",&p[i]);
	for (int i=1;i<n;i++) {
		scanf("%d%d",&x,&y);
		add(x,y); add(y,x);
	}
	while (m--) {
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if (x==0 && y==0) {
			int flag=0;
			for (int i=head[a];i;i=e[i].nex)
				if (e[i].to==b) {flag=1; break;}
			if (flag==1) {puts("-1"); continue;}
		}
		ban[a][x]=ban[b][y]=1;
		dfs(1,0);
		printf("%lld\n",min(f[1][0],f[1][1]));
		ban[a][x]=ban[b][y]=0;
	}
	return 0;
}
