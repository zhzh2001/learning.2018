#include<cstdio>
#include<iostream>
#include<algorithm>
const int N=5005;
int d[N][N],low[N],dfn[N],n,m,ans[N],tot,num;
bool bri[N][N],vis[N];
using namespace std;
inline int minn(int x,int y){
	if(x<y)return x;else return y;
}
void read(int &x){
	char ch;bool fu=false;
	while((ch=getchar())<'0'||ch>'9')if(ch=='-')fu=true;
	x=ch-'0';
	while((ch=getchar())>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0';
	if(fu)x=-x;
}
void tarjan(int x,int fa){
	dfn[x]=low[x]=++num;
	for(int i=1,y;i<=d[x][0];i++){
		if(!dfn[y=d[x][i]]){
			tarjan(y,x);
			low[x]=minn(low[x],low[y]);
			if(dfn[x]<low[y])bri[x][y]=bri[y][x]=true;
		}
		else if(y!=fa)
		       low[x]=minn(low[x],dfn[y]);
	}
}
void dfs(int x){
	for(int i=1;i<=d[x][0];i++){
		int y=d[x][i];
		if(vis[y])continue;
		vis[y]=1;ans[++tot]=y;
		if(bri[x][y]){
			dfs(y);
		}
		else{
			while(vis[d[x][i]]&&i<=d[x][0])i++;
			int j=1;
			while(vis[d[y][j]]&&j<=d[y][0])j++;
			if(i<=d[x][0]&&j<=d[y][0]){//dou he fa
				int t=minn(d[x][i],d[y][j]);
				vis[t]=1;ans[++tot]=t;
				dfs(t);
				dfs(y);
				continue;
			}
			if(i>d[x][0]&&j>d[y][0]){
				dfs(y);continue;
			}
			if(i<=d[x][0]){
				int t=d[x][i];
				vis[t]=1;ans[++tot]=t;
				dfs(t);
				dfs(y);
				continue;
			}
			if(j<=d[y][0]){
				int t=d[y][j];
				vis[t]=1;ans[++tot]=t;
				dfs(t);
				dfs(y);
				continue;
			}
		}
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);read(m);
	for(int i=1;i<=m;i++){
		int u,v;read(u);read(v);
		d[u][++d[u][0]]=v;d[v][++d[v][0]]=u;
	}
	for(int i=1;i<=n;i++)sort(d[i]+1,d[i]+1+d[i][0]);
	tarjan(1,0);
	vis[1]=1;ans[++tot]=1;
	dfs(1);
	for(int i=1;i<=tot;i++)printf("%d ",ans[i]);
	return 0;
}
