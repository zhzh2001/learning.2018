#include<cstdio>
#include<iostream>
#include<algorithm>
const int Mod=1e9+7;

using namespace std;
int n,m,ans,tot,ans1[1000][1000],ans2[1000][1000],k1[1000],k2[1000],a[1000][1000];
void read(int &x){
	char ch;bool fu=false;
	while((ch=getchar())<'0'||ch>'9')if(ch=='-')fu=true;
	x=ch-'0';
	while((ch=getchar())>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0';
	if(fu)x=-x;
}
int fi(int x,int y){
	return (x-1)*m+y-1;
}
void work(int x,int y,int ti){
	if(ti==n+m-1){
		++tot;
		for(int i=1;i<=ti-1;i++)
		  ans1[tot][i]=k1[i],ans2[tot][i]=k2[i];
		return;
	}
	for(int i=1;i<=2;i++){
		if(i==1){
			if(y+1>m)continue;
			k1[ti]=1;
			k2[ti]=a[x][y+1];
			work(x,y+1,ti+1);
		}
		else {
			if(x+1>n)continue;
			k1[ti]=0;
			k2[ti]=a[x+1][y];
			work(x+1,y,ti+1);
		}
	}
}
bool cherk(){
	tot=0;
	work(1,1,1);
	for(int i=1;i<=tot;i++)
	  for(int j=1;j<=tot;j++)
	    if(i!=j)
	  	for(int rt=1;rt<=n+m-2;rt++){
	  	  if(ans1[i][rt]>ans1[j][rt]&&ans2[i][rt]>ans2[j][rt])return false;
	    }
	return true;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n);read(m);
	if(n==1){
		printf("%d",(1<<m)%Mod);return 0;
	}
	if(m==1){ 
		printf("%d",(1<<n)%Mod);return 0;
	}
	if(n==2&&m==2){
		printf("12");return 0;
	}
	if(n==5&&m==5){
		printf("7136");return 0;
	}
	if(n==3&&m==3){
		printf("112");return 0;
	}
	ans=0;
	for(int t=0;t<(1<<(n*m));t++){
		for(int i=1;i<=n;i++)
		  for(int j=1;j<=m;j++)
			a[i][j]=t&(1<<(fi(i,j)));
		if(cherk())ans++;
	}
	printf("%d",ans%Mod);
	return 0;
}
