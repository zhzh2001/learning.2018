#include<bits/stdc++.h>
using namespace std;
const int P=1e9+7;
int n,m;
int A[10]={0,8,36,112};
long long fast(long long a,int b){
	long long res=1;
	while(b>0){
		if(b&1)res=res*a%P;
		a=a*a%P;
		b>>=1;
	}
	return res;
}
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d %d",&n,&m);
	if(n==1){
		printf("%lld\n",fast(2,m));
	}else if(n==2){
		printf("%lld\n",1ll*4*fast(3,m-1)%P);	
	}else if(n==3){
		if(m<=3)printf("%d\n",A[m]);
		else printf("%lld\n",1ll*112*fast(3,m-3)%P);
	}
	return 0;
}
