#include<bits/stdc++.h>
using namespace std;
const int M=5005;
int n,m,A[M],ans[M],tot;
vector<int>G[M];
int noa,nob;
void dfs1(int x,int f) {
	A[++tot]=x;
	for(int i=0; i<G[x].size(); i++) {
		int y=G[x][i];
		if(y==f||x==noa&&y==nob||x==nob&&y==noa)continue;
		dfs1(y,x);
	}
}
bool vis[M],flag;
int stk[M],top,que[M],len;
void dfs2(int x,int f) {
	if(flag)return;
	if(vis[x]) {
		len=0;
		do que[++len]=stk[top];
		while(stk[top--]!=x);
		flag=1;
		return;
	}
	stk[++top]=x;
	vis[x]=1;
	for(int i=0; i<G[x].size(); i++) {
		int y=G[x][i];
		if(y==f)continue;
		dfs2(y,x);
	}
	vis[x]=0;
	top--;
}
void check() {
	/*for(int i=1;i<=n;i++){
		printf("%d ",A[i]);
	}
	printf("\n");*/
	if(ans[1]==0) {
		for(int i=1; i<=n; i++) {
			ans[i]=A[i];
		}
	}else{
		for(int i=1;i<=n;i++){
			if(ans[i]==A[i])continue;
			if(A[i]>ans[i])return;
			break;
		}
		for(int i=1;i<=n;i++){
			ans[i]=A[i];
		}
	} 
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int a,b;
	scanf("%d %d",&n,&m);
	for(int i=1; i<=m; i++) {
		scanf("%d %d",&a,&b);
		G[a].push_back(b);
		G[b].push_back(a);
	}
	for(int i=1; i<=n; i++) {
		sort(G[i].begin(),G[i].end());
	}
	if(m==n-1) {
		tot=0;
		dfs1(1,0);
		for(int i=1; i<=n; i++) {
			printf("%d ",A[i]);
		}
	} else {
		dfs2(1,0);
		que[0]=que[len];
		for(int i=1; i<=len; i++) {
			noa=que[i];
			nob=que[i-1];
			//printf("noa=%d nob=%d\n",noa,nob);
			tot=0;
			dfs1(1,0);
			check();
		}
		for(int i=1; i<=n; i++) {
			printf("%d ",ans[i]);
		}
	}
	return 0;
}
