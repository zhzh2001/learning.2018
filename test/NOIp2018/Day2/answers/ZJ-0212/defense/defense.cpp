#include<bits/stdc++.h>
using namespace std;
const int M=100005;
int n,m,pi[M],fa[M],dep[M];
char op[10];
int head[M],asdf;
struct edge {
	int to,nxt;
} G[M*2];
void add_edge(int a,int b) {
	G[++asdf].to=b;
	G[asdf].nxt=head[a];
	head[a]=asdf;
}
long long dp[M][2];
bool vis[M][2];
void dfs(int x,int f) {
	fa[x]=f;
	dep[x]=dep[f]=1;
	dp[x][0]=0,dp[x][1]=pi[x];
	for(int i=head[x]; i; i=G[i].nxt) {
		int y=G[i].to;
		if(y==f)continue;
		dfs(y,x);
		dp[x][0]+=dp[y][1];
		dp[x][1]+=min(dp[y][1],dp[y][0]);
	}
	if(vis[x][0])dp[x][1]=1e18;
	if(vis[x][1])dp[x][0]=1e18;
	//printf("dp[%d] [0]=%lld [1]=%lld\n",x,dp[x][0],dp[x][1]);
}
void solve1() {
	int a,b,x,y;
	for(int i=1; i<=m; i++) {
		scanf("%d %d %d %d",&a,&x,&b,&y);
		vis[a][x]=1;
		vis[b][y]=1;
		dfs(1,0);
		vis[a][x]=0;
		vis[b][y]=0;
		long long ans=min(dp[1][0],dp[1][1]);
		printf("%lld\n",ans>=1e18?-1:ans);
	}
}
struct node {
	long long val[2][2];
} tree[M*4];
void Up(node &rt,node Ls,node Rs) {
	rt.val[0][0]=1e18;
	rt.val[0][1]=1e18;
	rt.val[1][0]=1e18;
	rt.val[1][1]=1e18;
	for(int i=0; i<2; i++) {
		for(int j=0; j<2; j++) {
			rt.val[i][j]=min(rt.val[i][j],Ls.val[i][0]+Rs.val[1][j]);
			rt.val[i][j]=min(rt.val[i][j],Ls.val[i][1]+Rs.val[0][j]);
			rt.val[i][j]=min(rt.val[i][j],Ls.val[i][1]+Rs.val[1][j]);
		}
	}
}
void build(int L,int R,int p) {
	if(L==R) {
		tree[p].val[0][0]=0;
		tree[p].val[1][1]=pi[L];
		tree[p].val[0][1]=1e18;
		tree[p].val[1][0]=1e18;
		return;
	}
	int mid=(L+R)>>1;
	build(L,mid,p<<1);
	build(mid+1,R,p<<1|1);
	Up(tree[p],tree[p<<1],tree[p<<1|1]);
}
void update(int x,int t,long long a,int L,int R,int p) {
	if(L==R) {
		tree[p].val[t][t]=a;
		return;
	}
	int mid=(L+R)>>1;
	if(x<=mid)update(x,t,a,L,mid,p<<1);
	else update(x,t,a,mid+1,R,p<<1|1);
	Up(tree[p],tree[p<<1],tree[p<<1|1]);
}
void solve2() {
	int a,b,x,y;
	build(1,n,1);
	for(int i=1; i<=m; i++) {
		scanf("%d %d %d %d",&a,&x,&b,&y);
		update(a,!x,1e18,1,n,1);
		update(b,!y,1e18,1,n,1);
		long long ans=1e18;
		ans=min(ans,tree[1].val[0][0]);
		ans=min(ans,tree[1].val[0][1]);
		ans=min(ans,tree[1].val[1][0]);
		ans=min(ans,tree[1].val[1][1]);
		printf("%lld\n",ans>=1e18?-1:ans);
		update(a,!x,(!x)==0?0:pi[a],1,n,1);
		update(b,!y,(!y)==0?0:pi[b],1,n,1);
	}
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int a,x,b,y;
	scanf("%d %d %s",&n,&m,op);
	for(int i=1; i<=n; i++) {
		scanf("%d",&pi[i]);
	}
	for(int i=1; i<n; i++) {
		scanf("%d %d",&a,&b);
		add_edge(a,b);
		add_edge(b,a);
	}
	if(op[0]=='A') {
		solve2();
	} else {
		solve1();
	}
	return 0;
}
