#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=5005;
inline void rd(int &x){
	x=0;static char c;
	while(c=getchar(),c<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),c>=48);
}
vector<int>E[N],V[N];
int n,m,X[N],Y[N];
int tmp[N],ct,ans[N],Ans[N];
bool cmp(int a[],int b[]){
	for(int i=1;i<=n;i++)
		if(a[i]!=b[i])return a[i]<b[i];
	return false;
}
struct P60{
	void dfs(int x,int f){
		tmp[++ct]=x;
		for(int i=0;i<(int)E[x].size();i++){
			int v=E[x][i];
			if(v==f)continue;
			dfs(v,x);
		}
	}
	void solve(){
		for(int i=1;i<=n;i++)sort(E[i].begin(),E[i].end());
		memset(ans,63,sizeof(ans));
		for(int i=1;i<=n;i++){
			ct=0,dfs(i,0);
			if(cmp(tmp,ans))
				for(int i=1;i<=n;i++)ans[i]=tmp[i];
		}
	}
}P_60;
struct P40{
	int mark[N],t;
	bool cycle_dfs(int x,int f){
		mark[x]=t;
		for(int i=0;i<(int)V[x].size();i++){
			int v=V[x][i];
			if(v==f)continue;
			if(mark[v]==t)return false;
			else if(mark[v])continue;
			if(!cycle_dfs(v,x))return false;
		}
		return true;
	}
	void dfs(int x,int f){
		tmp[++ct]=x;
		for(int i=0;i<(int)V[x].size();i++){
			int v=V[x][i];
			if(v==f)continue;
			dfs(v,x);
		}
	}	
	void solve(){
		memset(Ans,63,sizeof(Ans));
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++)V[j].clear(),mark[j]=0;
			for(int j=1;j<=n;j++){
				if(j==i)continue;
				V[X[j]].push_back(Y[j]);
				V[Y[j]].push_back(X[j]);
			}
			bool ok=1;
			for(int j=1;j<=n;j++){
				t++;
				if(mark[j])continue;
				if(!cycle_dfs(j,0)){
					ok=0;
					break;
				}
			}
			if(!ok)continue;
			memset(ans,63,sizeof(ans));
			for(int j=1;j<=n;j++)sort(V[j].begin(),V[j].end());
			for(int j=1;j<=n;j++){
				ct=0,dfs(j,0);
				if(cmp(tmp,ans))
					for(int k=1;k<=n;k++)ans[k]=tmp[k];
			}
			if(cmp(ans,Ans))
				for(int j=1;j<=n;j++)Ans[j]=ans[j];
		}
	}
}P_40;
int main(){//ll file memory mod
//	printf("%lf\n",sizeof()/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	rd(n),rd(m);
	for(int i=1;i<=m;i++){
		rd(X[i]),rd(Y[i]);
		E[X[i]].push_back(Y[i]);
		E[Y[i]].push_back(X[i]);
	}
	if(m==n-1){
		P_60.solve();
		printf("%d",ans[1]);
		for(int i=2;i<=n;i++)printf(" %d",ans[i]);
	}
	else {
		P_40.solve();
		printf("%d",Ans[1]);
		for(int i=2;i<=n;i++)printf(" %d",Ans[i]);
	}
	return 0;
}
