#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=100005;
inline void rd(int &x){
	x=0;static char c;
	while(c=getchar(),c<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),c>=48);
}
int n,m;
int cost[N];
struct Graph{
	int tot,to[N<<1],nxt[N<<1],head[N];
	void add(int x,int y){tot++;to[tot]=y;nxt[tot]=head[x];head[x]=tot;}
	void clear(){memset(head,-1,sizeof(head));}
}G;
ll dp[N][2];
int a,X,b,Y;//表示以i为根的子树，1为买0为不买 
void dfs(int x,int f){
	bool leaf=1;
	for(int i=G.head[x];i!=-1;i=G.nxt[i]){
		int v=G.to[i];
		if(v==f)continue;
		leaf=0;
		dfs(v,x);
		dp[x][0]+=dp[v][1];
		dp[x][1]+=min(dp[v][0],dp[v][1]);
		if(x==a)dp[x][X^1]=1e18;
		if(x==b)dp[x][Y^1]=1e18;
	}
	if(leaf){
		dp[x][0]=0;
		dp[x][1]=cost[x];
		if(x==a)dp[x][X^1]=1e18;
		if(x==b)dp[x][Y^1]=1e18;
	}
	else dp[x][1]+=cost[x];
//	printf("x:%d dp[x][0]:%lld dp[x][1]:%lld\n",x,dp[x][0],dp[x][1]);
}
int main(){//ll file memory mod
//	printf("%lf\n",sizeof()/1024.0/1024);
	freopen("defence.in","r",stdin);
	freopen("defence.out","w",stdout);
	char str[10];
	G.clear();
	rd(n),rd(m),scanf("%s",str);
	for(int i=1;i<=n;i++)rd(cost[i]);
	for(int i=1;i<n;i++){
		int x,y;
		rd(x),rd(y);
		G.add(x,y),G.add(y,x);
	}
	while(m--){
		rd(a),rd(X),rd(b),rd(Y);
		for(int i=1;i<=n;i++)dp[i][0]=dp[i][1]=0;
		dfs(1,0);
		if(min(dp[1][0],dp[1][1])>=1e18)puts("-1");
		else printf("%lld\n",min(dp[1][0],dp[1][1]));
	}
	return 0;
}
