#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int P=1e9+7;
int n,m;
struct P30{
	ll qpow(ll a,ll b){
		ll ans=1;
		while(b){
			if(b&1)ans=(ans*a)%P;
			a=(a*a)%P;
			b>>=1;
		}
		return ans;
	}
	void solve(){
		printf("%lld\n",qpow(3,m-1)*4%P);
	}
}P_30;
int ans[][3]={{2,4,8},{2,12,36},{8,36,112}};
struct P20{
	void solve(){
		printf("%d\n",ans[n-1][m-1]);
	}
}P_20;
int main(){//ll file memory mod
//	printf("%lf\n",sizeof()/1024.0/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n<=3&&m<=3)P_20.solve();
	else if(n==2)P_30.solve();
	else {
		ll ans=1;
		int cnt=0;
		for(int i=1;i<=n;i++)
			ans=(ans*(i+1))%P;
		for(int i=n+1;i<=m-n;i++)
			ans=(ans*(n+1))%P;
		cnt=n;
		for(int i=m-n+1;i<=m;i++)
			ans=(ans*(cnt--))%P;
		printf("%lld\n",ans);
	}
	return 0;
}
