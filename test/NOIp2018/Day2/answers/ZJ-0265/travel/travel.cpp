#include<bits/stdc++.h>
using namespace std;
int n,m,r,z,o,q,tmp[5005],ans[5005];
bool b[5005],used[5005];
vector <int> v[5005];
void dfs(int x,int p)
{
	printf("%d ",x);
	int l=v[x].size();
	for (int i=0;i<l;i++)
	{
		int y=v[x][i];
		if (y!=p) dfs(y,x);
	}
}
void dfs2(int x,int p)
{
	tmp[++tmp[0]]=x;
	int l=v[x].size();
	for (int i=0;i<l;i++)
	{
		int y=v[x][i];
		if (y!=p&&(x!=o||y!=q)&&(x!=q||y!=o)) dfs2(y,x);
	}
}
void dfs1(int x,int p)
{
	used[x]=1;
	for (int i=v[x].size()-1;i>=0;i--)
	{
		int y=v[x][i];
		if (y!=p)
		{
			if (used[y])
			{
				r=y,b[x]=1;
				o=x,q=y,tmp[0]=0,dfs2(1,0);
				bool fl=0;
				for (int i=1;i<=n;i++)
					if (tmp[i]!=ans[i])
					{
						if (tmp[i]<ans[i]) fl=1;
						break;
					}
				if (fl) for (int i=1;i<=n;i++) ans[i]=tmp[i];
				return;
			}
			dfs1(y,x);
		}
		if (r)
		{
			if (r>0)
			{
				b[x]=1,o=x,q=y,tmp[0]=0,dfs2(1,0);
				bool fl=0;
				for (int i=1;i<=n;i++)
					if (tmp[i]!=ans[i])
					{
						if (tmp[i]<ans[i]) fl=1;
						break;
					}
				if (fl) for (int i=1;i<=n;i++) ans[i]=tmp[i];
			}
			if (r==x) r=-1;
			return;
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		v[x].push_back(y);
		v[y].push_back(x);
	}
	for (int i=1;i<=n;i++) sort(v[i].begin(),v[i].end());
	if (m<n){dfs(1,0);fclose(stdin);fclose(stdout);return 0;}
	ans[1]=10000;
	dfs1(1,0);
	for (int i=1;i<=n;i++) printf("%d ",ans[i]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
