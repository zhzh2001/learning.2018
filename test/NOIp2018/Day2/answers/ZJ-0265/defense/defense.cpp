#include<bits/stdc++.h>
#define ll long long
#define inf (ll)1<<60
using namespace std;
int n,m,tot,ans,b,c,d,e,a[100005],ver[200005],nxt[200005],head[100005];
ll f[2][100005];
char s[2];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	for (;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1;
	for (;isdigit(ch);ch=getchar()) x=(x+(x<<2)<<1)+(ch^48);
	return x*f;
}
void swap(int &x,int &y){int t=x;x=y,y=t;}
int min(int x,int y){return x<y?x:y;}
void add(int x,int y){ver[++tot]=y,nxt[tot]=head[x],head[x]=tot;}
void dfs(int x,int p)
{
	f[0][x]=0,f[1][x]=a[x];
	if (x==e) f[c^1][x]=inf;
	if (x==b) f[d^1][x]=inf;
	for (int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];
		if (y!=p)
		{
			dfs(y,x);
			f[0][x]+=f[1][y];
			f[1][x]+=min(f[0][y],f[1][y]);
		}
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),scanf("%s",s);
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<n;i++)
	{
		int x=read(),y=read();
		add(x,y),add(y,x);
	}
	while (m--)
	{
		e=read(),c=read(),b=read(),d=read();
		if (e>b) swap(e,b),swap(c,d);
	//	memset(f,63,sizeof(f));
		dfs(1,0);
		ll ans=min(f[0][1],f[1][1]);
		if (ans>=inf) printf("-1\n");
		else printf("%lld\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
