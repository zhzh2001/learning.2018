#include<bits/stdc++.h>
#define mod 1000000007
#define ll long long
using namespace std;
int n,m,ans=1;
int pow(int a,int b)
{
	int ans=1;
	while (b)
	{
		if (b&1) ans=(ll)ans*a%mod;
		a=(ll)a*a%mod,b>>=1;
	}
	return ans;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	if (n==1) printf("%d",pow(2,m));
	else if (n==2) printf("%d",((ll)pow(3,m-1)<<2)%mod);
	else printf("112");
	fclose(stdin);
	fclose(stdout);
	return 0;
}
