#if 1
#include<iostream>
#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<set>
#include<map>
#include<queue>
#include<utility>
#include<stdlib.h>
#else
#include<bits/stdc++.h>
#endif

#if 0
#include<cmath>
#endif

#define REP(i, l, r) for (int i = (l); i <= (r); ++i)
#define RREP(i, r, l) for (int i = (r); i >= (l); --i)
#define rep(i, l, r) for (int i = (l); i < (r); ++i)
#define rrep(i, r, l) for (int i = (r); i > (l); --i)
#define foredge(i, u) for (int i = la[u]; i; i = ne[i])
#define mem(a) memset(a, 0, sizeof(a))
#define memid(a) memset(a, 0x3f, sizeof(a))
#define memax(a) memset(a, 0x7f, sizeof(a))
#define dbg(x) cout << #x << " = " << x << endl
#define tpn typename
#define fr(a) freopen(a, "r", stdin);
#define fw(a) freopen(a, "w", stdout);

using namespace std;

typedef long long ll;

template<tpn A> inline void Swap(A &x,A &y){
	x ^= y, y ^= x, x ^= y;
}
template<tpn A> inline A Max(const A &x,const A &y){
	return x > y ? x : y;
}
template<tpn A> inline A Min(const A &x,const A &y){
	return x < y ? x : y;
}
template<tpn A> inline A Abs(const A &x){
	return x < 0 ? -x : x;
}
template<tpn A> inline void read(A &x){
	A neg = 1;
	char c;
	do c = getchar();
	while ((c < '0' || c > '9') && c != '-');
	if (c == '-') neg = -1, c = getchar();
	x = 0;
	do{
		x = x * 10 + c - 48;
		c = getchar();
	}while (c >= '0' && c <= '9');
}
template<tpn A> inline void read(A &x,A &y){
	read(x), read(y);
}
template<tpn A> inline void read(A &x,A &y,A &a,A &b){
	read(x), read(y), read(a), read(b);
}
const int N = 1000005;
const int p = 1000000007;
int f[N][8];
int n, m, nn;
int main(){
	fr("game.in");
	fw("game.out");
	read(n, m);
	nn = 1 << n;
	rep(i, 0, nn) f[1][i] = 1;
	REP(i, 2, m){
		rep(ii, 0, nn)
		rep(jj, 0, nn)
		if ((((jj + nn >> 1) ^ (nn - 1)) & ii) == 0) (f[i][ii] += f[i-1][jj]) %= p;
	}
//	rep(i, 0, nn) printf("%d ", f[m][i]);
	rep(i, 1, nn) (f[m][0] += f[m][i]) %= p;
	printf("%d\n", f[m][0]);
	return 0;
}
