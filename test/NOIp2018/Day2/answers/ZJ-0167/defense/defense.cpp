#if 1
#include<iostream>
#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<set>
#include<map>
#include<queue>
#include<utility>
#include<stdlib.h>
#else
#include<bits/stdc++.h>
#endif

#if 0
#include<cmath>
#endif

#define REP(i, l, r) for (int i = (l); i <= (r); ++i)
#define RREP(i, r, l) for (int i = (r); i >= (l); --i)
#define rep(i, l, r) for (int i = (l); i < (r); ++i)
#define rrep(i, r, l) for (int i = (r); i > (l); --i)
#define foredge(i, u) for (int i = la[u]; i; i = ne[i])
#define mem(a) memset(a, 0, sizeof(a))
#define memid(a) memset(a, 0x3f, sizeof(a))
#define memax(a) memset(a, 0x7f, sizeof(a))
#define dbg(x) cout << #x << " = " << x << endl
#define tpn typename
#define fr(a) freopen(a, "r", stdin);
#define fw(a) freopen(a, "w", stdout);

using namespace std;

typedef long long ll;

template<tpn A> inline void Swap(A &x,A &y){
	x ^= y, y ^= x, x ^= y;
}
template<tpn A> inline A Max(const A &x,const A &y){
	return x > y ? x : y;
}
template<tpn A> inline A Min(const A &x,const A &y){
	return x < y ? x : y;
}
template<tpn A> inline A Abs(const A &x){
	return x < 0 ? -x : x;
}
template<tpn A> inline void read(A &x){
	A neg = 1;
	char c;
	do c = getchar();
	while ((c < '0' || c > '9') && c != '-');
	if (c == '-') neg = -1, c = getchar();
	x = 0;
	do{
		x = x * 10 + c - 48;
		c = getchar();
	}while (c >= '0' && c <= '9');
}
template<tpn A> inline void read(A &x,A &y){
	read(x), read(y);
}
template<tpn A> inline void read(A &x,A &y,A &a,A &b){
	read(x), read(y), read(a), read(b);
}
const int N = 2005;
int la[N], ne[N << 1], en[N << 1], top;
inline void add(int x,int y){
	ne[++top] = la[x];
	en[top] = y;
	la[x] = top;
}
int n, m, x, y, a, b, nn, w[N];
char opt[5];
int temp[N], cost;
int vis[N];
inline void pd(){
	int sum = 0;
	mem(vis);
	REP(i, 1, top){
		sum += w[temp[i]];
		foredge(j, temp[i]) vis[j + 1 >> 1] = 1;
	}
	rep(i, 1, n) if (!vis[i]) return;
	cost = Min(cost, sum);
}
int main(){
	fr("defense.in");
	fw("defense.out");
	read(n, m);
	REP(i, 1, n) read(w[i]);
	nn = 1 << n;
	scanf("%s", opt);
	rep(i, 1, n){
		read(x, y);
		add(x, y);
		add(y, x);
	}
	top = 0;
	REP(ii, 1, m){
		read(a, x, b, y);
		--a, --b;
		cost = 0x7fffffff;
		rep(i, 0, nn){
			if ((bool)(i & (1 << a)) != x || (bool)(i & (1 << b)) != y) continue;
			top = 0;
			rep(j, 0, n) if (i & 1 << j) temp[++top] = j + 1;
			pd();
		}
		if (cost == 0x7fffffff) puts("-1");
		else printf("%d\n", cost);
	}
	return 0;
}
