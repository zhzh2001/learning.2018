#if 1
#include<iostream>
#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<set>
#include<map>
#include<queue>
#include<utility>
#include<stdlib.h>
#else
#include<bits/stdc++.h>
#endif

#if 0
#include<cmath>
#endif

#define REP(i, l, r) for (int i = (l); i <= (r); ++i)
#define RREP(i, r, l) for (int i = (r); i >= (l); --i)
#define rep(i, l, r) for (int i = (l); i < (r); ++i)
#define rrep(i, r, l) for (int i = (r); i > (l); --i)
#define foredge(i, u) for (int i = la[u]; i; i = ne[i])
#define mem(a) memset(a, 0, sizeof(a))
#define memid(a) memset(a, 0x3f, sizeof(a))
#define memax(a) memset(a, 0x7f, sizeof(a))
#define dbg(x) cout << #x << " = " << x << endl
#define tpn typename
#define fr(a) freopen(a, "r", stdin);
#define fw(a) freopen(a, "w", stdout);

using namespace std;

typedef long long ll;

template<tpn A> inline void Swap(A &x,A &y){
	x ^= y, y ^= x, x ^= y;
}
template<tpn A> inline A Max(const A &x,const A &y){
	return x > y ? x : y;
}
template<tpn A> inline A Min(const A &x,const A &y){
	return x < y ? x : y;
}
template<tpn A> inline A Abs(const A &x){
	return x < 0 ? -x : x;
}
template<tpn A> inline void read(A &x){
	A neg = 1;
	char c;
	do c = getchar();
	while ((c < '0' || c > '9') && c != '-');
	if (c == '-') neg = -1, c = getchar();
	x = 0;
	do{
		x = x * 10 + c - 48;
		c = getchar();
	}while (c >= '0' && c <= '9');
}
template<tpn A> inline void read(A &x,A &y){
	read(x), read(y);
}
template<tpn A> inline void read(A &x,A &y,A &a,A &b){
	read(x), read(y), read(a), read(b);
}
const int N = 5005;
int la[N], ne[N << 1], en[N << 1], top;
inline void add(int x,int y){
	ne[++top] = la[x];
	en[top] = y;
	la[x] = top;
}
int n, m, x, y;
int vis[N];
priority_queue<int, vector<int>, greater<int> > q[N];
namespace tree{
	inline void dfs(int u){
		printf("%d ", u);
		vis[u] = 1;
		foredge(i, u)
		if (!vis[en[i]]) q[u].push(en[i]);
		while (!q[u].empty()){
			dfs(q[u].top());
			q[u].pop();
		}
	}
	inline void solve(){
		dfs(1);
		cout << endl;
	}
}
namespace circleTree{
	int iscircle[N], rt, rtson, flag;
	int temp[N], top;
	inline void prework(int u,int p){
		if (vis[u]){
			REP(i, 1, top){
				if (temp[i] == u) flag = 1;
				if (flag) iscircle[temp[i]] = 1;
			}
			return;
		}
		vis[u] = 1;
		temp[++top] = u;
		foredge(i, u)
		if (en[i] != p){
			prework(en[i], u);
			if (flag) return;
		}
		temp[top--] = 0;
	}
	int pre[N];
	inline void dfs(int u){
		printf("%d ", u);
		vis[u] = 1;
		if (iscircle[u] && !rt){
			rt = u;
			foredge(i, u)
			if (!vis[en[i]]) q[u].push(en[i]);
			while (!q[u].empty()){
				rtson = q[u].top();
				q[u].pop();
				if (vis[rtson]) continue;
				pre[++top] = u;
				dfs(rtson);
				pre[top--] = 0;
			}
			return;
		}
		if (!rt || !iscircle[u] || flag){
			foredge(i, u)
			if (!vis[en[i]]) q[u].push(en[i]);
			while (!q[u].empty()){
				dfs(q[u].top());
				q[u].pop();
			}
		}
		else{
			int son = 0;
			foredge(i, u)
			if (!vis[en[i]]){
				q[u].push(en[i]);
				if (!iscircle[en[i]]) ++son;
			}
			while (!q[u].empty()){
				x = q[u].top(), y = q[pre[top]].top();
				if (x <= y || son){
					if (!iscircle[x]) --son;
					if (son) pre[++top] = u;
					q[u].pop();
					dfs(x);
					if (son) pre[top--] = 0;
				}
				else{
					flag = 1;
					q[u].pop();
				}
			}
		}
	}
	inline void solve(){
		prework(1, 0);
		mem(vis);
		flag = top = 0;
		dfs(1);
		cout << endl;
	}
}
int main(){
	fr("travel.in");
	fw("travel.out");
	read(n, m);
	REP(i, 1, m){
		read(x, y);
		add(x, y);
		add(y, x);
	}
	if (m == n - 1) tree::solve();
	else circleTree::solve();
	return 0;
}
