#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define mod 1000000007
using namespace std;
template<typename T>void read(T &a)
{
	T x=0,t=1;
	char ch=getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-')t=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')x*=10,x+=ch-'0',ch=getchar();
	a=x*t;
}
template<typename T>void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template<typename T>void printfn(T a){print(a);putchar('\n');}
int n,m;
long long base[301][301],ans[301][301],res[301][301];
bool check(int x,int y)
{
	for(register int i=1;i<=(1<<(n-2));i<<=1)
	{
		int x1=x&i,x2=y&(i<<1);
		if(x1>x2)return 0;
	}
	return 1;
}
void putout(int x)
{
	for(register int i=1;i<=n;i++)
	{
		putchar('0'+(x&1));
		x>>=1;
	}
}
void Pow(int x)
{
	int tmp=1;
	while(tmp<=x)
	{
		if(tmp&x)
		{
			memset(res,0,sizeof(res));
			for(register int i=0;i<=(1<<n)-1;i++)
				for(register int j=0;j<=(1<<n)-1;j++)
					for(register int k=0;k<(1<<n)-1;k++)
						res[i][k]+=ans[i][j]*base[j][k],res[i][k]%=mod;
			memcpy(ans,res,sizeof(res));
		}
		tmp<<=1;
		memset(res,0,sizeof(res));
		for(register int i=0;i<=(1<<n)-1;i++)
			for(register int j=0;j<=(1<<n)-1;j++)
				for(register int k=0;k<(1<<n)-1;k++)
					res[i][k]+=base[i][j]*base[j][k],res[i][k]%=mod;
		memcpy(base,res,sizeof(res));
	}
}
int main()
{
	read(n),read(m);
	for(register int i=0;i<=(1<<n)-1;i++)
		for(register int j=0;j<=(1<<n)-1;j++)
			if(check(i,j))base[i][j]=1,printf("base["),putout(i),printf("]["),putout(j),printf("]=1\n");
	memcpy(ans,base,sizeof(base));
	Pow(m-2);
	long long Ans=0;
	for(register int i=0;i<=(1<<n)-1;i++)
		for(register int j=0;j<=(1<<n)-1;j++)
			Ans+=ans[i][j],Ans%=mod,printf("ans["),putout(i),printf("]["),putout(j),printf("]=%d\n",ans[i][j]);
	printfn(Ans);
}
