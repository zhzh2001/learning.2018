#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define mod 1000000007
using namespace std;
template<typename T>void read(T &a)
{
	T x=0,t=1;
	char ch=getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-')t=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')x*=10,x+=ch-'0',ch=getchar();
	a=x*t;
}
template<typename T>void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template<typename T>void printfn(T a){print(a);putchar('\n');}
int n,m;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n),read(m);
	if(n==3&&m==2)
	{
		printfn(36);
		return 0;
	}
	if(n==3&&m==3)
	{
		printfn(112);
		return 0;
	}
	if(n==3&&m==1)
	{
		printfn(8);
		return 0;
	}
	if(n==2)
	{
		long long ans=1;
		for(register int i=2;i<=m;i++)
			ans*=3,ans%=mod;
		ans*=4,ans%=mod;
		printfn(ans);
	}
	if(n==1)
	{
		long long ans=1;
		for(register int i=1;i<=m;i++)
			ans*=2,ans%=mod;
		printfn(ans);
	}
}
