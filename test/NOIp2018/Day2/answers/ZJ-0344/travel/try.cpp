#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<vector>
#include<set>
using namespace std;
template<typename T>void read(T &a)
{
	T x=0,t=1;
	char ch=getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-')t=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')x*=10,x+=ch-'0',ch=getchar();
	a=x*t;
}
template<typename T>void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template<typename T>void printfn(T a){print(a);putchar('\n');}
int n,m;
vector<int>p[6011];
int dl[6001],ldl=0;
int huan[6001],lh=0,onhuan[6001];
int vis[6001],f[6001];
bool dfs1(int x)
{
	vis[x]=1;
	dl[++ldl]=x;
	for(register int i=0;i<p[x].size();i++)
	{
		int v=p[x][i];
		if(f[x]==v)continue;
		if(vis[v])
		{
			int tmp=ldl;
			while(dl[tmp]!=v)huan[++lh]=dl[tmp],onhuan[dl[tmp]]=1,tmp--;
			huan[++lh]=v;
			onhuan[v]=1;
			return 1;
		}
		f[v]=x;
		if(dfs1(v))return 1;
	}
	ldl--;
	return 0;
}
void dfs2(int x)
{
	dl[++ldl]=x;
	vis[x]=1;
	int tmp=0;
	for(register int i=0;i<p[x].size();i++)
	{
		int v=p[x][i];
		if(vis[v])continue;
		tmp=v;
		dfs2(v);
	}
}
int main()
{
//	freopen("travel.in","r",stdin);
//	freopen("travel.out","w",stdout);
	read(n),read(m);
	for(register int i=1;i<=m;i++)
	{
		int a,b;
		read(a),read(b);
		p[a].push_back(b);
		p[b].push_back(a);
	}
	for(register int i=1;i<=n;i++)
		sort(p[i].begin(),p[i].end());
	dfs1(1);
	memset(vis,0,sizeof(vis));
	printfn(lh);
	for(register int i=1;i<=lh;i++)
		printf("%d ",huan[i]);
	printf("\n");
	if(m==n-1)
	{
		dfs2(1);
		for(register int i=1;i<=n;i++)
			print(dl[i]),putchar(' ');
		return 0;
	}
	else
	{
		if(!onhuan[1])
		{
			dfs3(1);
		}
	}
}
