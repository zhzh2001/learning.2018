#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<vector>
#include<set>
using namespace std;
template<typename T>void read(T &a)
{
	T x=0,t=1;
	char ch=getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-')t=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')x*=10,x+=ch-'0',ch=getchar();
	a=x*t;
}
template<typename T>void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template<typename T>void printfn(T a){print(a);putchar('\n');}
int n,m;
vector<int>p[6011];
int dl[6001],ldl=0;
int vis[6001],f[6001],bef[6001];
int sondl[6001];
int tag[6001];
int dfn[6001];
int x1,x2;
void dfs1(int x)
{
	vis[x]=1;
	for(register int i=0;i<p[x].size();i++)
	{
		int v=p[x][i];
		if(f[x]==v)continue;
		if(vis[v])
		{
			if(dfn[x]<dfn[v])x1=x,x2=v;
			else x1=v,x2=x;
			continue;
		}
		f[v]=x,dfn[v]=dfn[x]+1;
		dfs1(v);
	}
}
void dfs2(int x)
{
	dl[++ldl]=x;
	vis[x]=1;
	int tmp=0;
	for(register int i=0;i<p[x].size();i++)
	{
		int v=p[x][i];
		if(vis[v])continue;
		if(tag[x]==3&&!vis[x2])
		{
			if(x2<v)dfs2(x2);
		}
		if(tmp)bef[v]=tmp;
		tmp=v;
		dfs2(v);
	}
}
void Tag(int x)
{
	tag[x]|=1;
	for(register int i=0;i<p[x].size();i++)
	{
		int v=p[x][i];
		if(f[x]==v||v==x2)continue;
		Tag(v);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n),read(m);
	for(register int i=1;i<=m;i++)
	{
		int a,b;
		read(a),read(b);
		p[a].push_back(b);
		p[b].push_back(a);
	}
	for(register int i=1;i<=n;i++)
		sort(p[i].begin(),p[i].end());
	bef[1]=1<30;
	dfs1(1);
	memset(vis,0,sizeof(vis));
	Tag(x1);
	int tmp=x2;
	while(tmp)tag[tmp]|=2,tmp=f[tmp];
	tag[x1]|=2;
	dfs2(1);
	for(register int i=1;i<=n;i++)
		print(dl[i]),putchar(' ');
	return 0;
}
