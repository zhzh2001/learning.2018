#include<iostream>
#include<vector>
#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
template<typename T>void read(T &a)
{
	T x=0,t=1;
	char ch=getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-')t=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')x*=10,x+=ch-'0',ch=getchar();
	a=x*t;
}
template<typename T>void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template<typename T>void printfn(T a){print(a);putchar('\n');}
int n,m;
char type[101];
int q[100001];
int A,X,B,Y;
int f[100001][2];
int vis[100001];
vector<int>p[100001];
void dfs(int x)
{
	vis[x]=1;
	for(register int i=0;i<p[x].size();i++)
	{
		int v=p[x][i];
		if(vis[v])continue;
		dfs(v);
		f[x][1]+=min(f[v][0],f[v][1]);
		f[x][0]+=f[v][1];
	}
	f[x][1]+=q[x];
//	printf("f[%d][0]=%d f[%d][1]=%d mn=%d\n",x,f[x][0],x,f[x][1],mn);
	if(x==A)
	{
		if(X)f[x][0]=f[x][1];
		else f[x][1]=1<<30;
	}
	if(x==B)
	{
		if(Y)f[x][0]=f[x][1];
		else f[x][1]=1<<30;
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n),read(m);
	gets(type+1);
	for(register int i=1;i<=n;i++)
		read(q[i]);
	for(register int i=1;i<n;i++)
	{
		int a,b;
		read(a),read(b);
		p[a].push_back(b);
		p[b].push_back(a);
	}
	while(m--)
	{
		read(A),read(X),read(B),read(Y);
		memset(f,0,sizeof(f));
		memset(vis,0,sizeof(vis));
		dfs(1);
	//	for(register int i=1;i<=n;i++)
	//		printf("f[%d][0]=%d f[%d][1]=%d\n",i,f[i][0],i,f[i][1]);
		long long ans=min(f[1][0],f[1][1]);
		if(ans>=1<<30)printfn(-1);
		else printfn(ans);
	}
}
