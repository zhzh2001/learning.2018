#include<cstdio>
using namespace std;
const int maxn = 2005;
const int maxe = 2005;
const int oo = 1000000000;
int ans;
int edgenum;
int Next[maxe << 1] , vet[maxe << 1] , head[maxn];
int w[maxn];
int col[maxn];
int f[maxn][3];
inline int min(int x , int y){return x < y ? x : y;}
void add_edge(int u , int v)
{
	edgenum++;
	Next[edgenum] = head[u];
	vet[edgenum] = v;
	head[u] = edgenum;
}
void dfs(int u , int fa)
{
	f[u][0] = w[u] , f[u][1] = oo , f[u][2] = 0;
	for(int e = head[u];e;e = Next[e])
	{
		int v = vet[e];
		if(v == fa) continue;
		dfs(v , u);
		f[u][0] += f[v][2];
		if(col[v] == 1) f[u][1] = oo;
		if(f[u][1] != oo) f[u][1] += f[v][0];
		if(col[fa] == 1) f[u][2] = oo;
		if(f[u][2] != oo) f[u][2] += min(f[v][0] , f[v][1]);
//		if(col[v] == 2) f[u][2] = min(oo , f[u][2] + w[v]);
	}
	if(col[fa] == 2) f[u][1] = min(oo , f[u][1] + w[fa]);
	if(col[u] == 1) f[u][0] = oo;
//	if(col[u] == 2) f[u][2] = min(oo , f[u][2] + w[u]) , f[u][1] = min(oo , f[u][1] + w[u]);
	if(fa == 1) ans = min(ans , f[u][0]);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n , m;
	char type[10];
	scanf("%d%d%s",&n,&m,type);
	for(int i = 1;i <= n;i++) scanf("%d",&w[i]);
	for(int i = 1;i < n;i++)
	{
		int u , v;
		scanf("%d%d",&u,&v);
		add_edge(u , v) , add_edge(v , u);
	}
	while(m--)
	{
		int u , op1 , v , op2;
		scanf("%d%d%d%d",&u,&op1,&v,&op2);
		col[u] = op1 + 1 , col[v] = op2 + 1;
		ans = oo;
		dfs(1 , 0);
		ans = min(min(f[1][0] , f[1][1]) , ans);
		printf("%d\n",ans == oo ? -1 : ans);
		col[u] = 0 , col[v] = 0;
	}
	return 0;
}
