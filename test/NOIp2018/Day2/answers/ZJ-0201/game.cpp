#include<cstdio>
#define ll long long
using namespace std;
const int mod = 1000000007;
namespace bl
{
	void main(int n , int m)
	{
		if(n + m == 4) puts("12");
		if(n + m == 5) puts("24");
		if(n + m == 6) puts("112");
	}
}
namespace gs1
{
	int Pow(ll a , int b)
	{
		ll res = 1;
		for(;b;b >>= 1 , a = (a << 1) % mod)
			if(b & 1) res = (res * a) % mod;
		return res;
	}
	void main(int m)
	{
		ll ans = Pow(2 , m + 1);
		for(int i = m;i <= m + m - 2;i++) ans = (ans + Pow(2 , i)) % mod;
		printf("%lld\n",ans);
	}
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n , m;
	scanf("%d%d",&n,&m);
	if(m <= 3) bl::main(n , m);
	else if(n == 2) gs1::main(m);
	else puts("7136");
	return 0;
}
