#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;
const int maxn = 5005;
const int oo = 1000000000;
vector < int > E[maxn];
namespace tree
{
	int num;
	int a[maxn];
	void dfs(int u , int fa)
	{
		a[++num] = u;
		for(int e = 0;e < E[u].size();e++)
		{
			int v = E[u][e];
			if(v == fa) continue;
			dfs(v , u);
		}
	}
	void main(int n , int m)
	{
		while(m--)
		{
			int u , v;
			scanf("%d%d",&u,&v);
			E[u].push_back(v) , E[v].push_back(u);
		}
		for(int i = 1;i <= n;i++) sort(E[i].begin() , E[i].end());
		num = 0;
		dfs(1 , 0);
		for(int i = 1;i <= n;i++) printf("%d%c",a[i] , i == n ? '\n' : ' ');
	}
}
namespace ct
{
	int N;
	int Time , top;
	int dfn[maxn] , low[maxn] , stk[maxn];
	int num;
	int a[maxn];
	bool c[maxn] , vis[maxn];
	void tarjan(int u , int pre)
	{
		dfn[u] = low[u] = ++Time;
		stk[++top] = u;
		for(int e = 0;e < E[u].size();e++)
		{
			int v = E[u][e];
			if(v == pre) continue;
			if(!dfn[v]) tarjan(v , u) , low[u] = min(low[u] , low[v]);
			else low[u] = min(low[u] , dfn[v]);
		}
		if(dfn[u] == low[u])
		{
			if(stk[top] == u){top--; return;}
			while(stk[top] != u) c[stk[top--]] = 1;
			c[stk[top--]] = 1;
		}
	}
	int search()
	{
		int res = oo;
		for(int u = 1;u <= N;u++)
		{
			if(!c[u] || c[u] && !vis[u]) continue;
			for(int e = 0;e < E[u].size();e++)
			{
				int v = E[u][e];
				if(vis[v]) continue;
				res = min(res , v);
			}
		}
		return res;
	}
	void dfs(int u)
	{
		vis[u] = 1;
		a[++num] = u;
		if(!c[u])
		{
			for(int e = 0;e < E[u].size();e++)
			{
				int v = E[u][e];
				if(vis[v]) continue;
				dfs(v);
			}
		}
		else
		{
			int v = search();
			while(v != oo) dfs(v) , v = search();
		}
	}
	void main(int n , int m)
	{
		N = n;
		while(m--)
		{
			int u , v;
			scanf("%d%d",&u,&v);
			E[u].push_back(v) , E[v].push_back(u);
		}
		for(int i = 1;i <= n;i++) sort(E[i].begin() , E[i].end());
		Time = 0 , top = 0;
		for(int i = 1;i <= n;i++) dfn[i] = 0;
		tarjan(1 , 0);
		for(int i = 1;i <= n;i++) vis[i] = 0;
		num = 0;
		dfs(1);
		for(int i = 1;i <= n;i++) printf("%d%c",a[i] , i == n ? '\n' : ' ');
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n , m;
	scanf("%d%d",&n,&m);
	if(m == n - 1) tree::main(n , m);
	else ct::main(n , m);
	return 0;
}
