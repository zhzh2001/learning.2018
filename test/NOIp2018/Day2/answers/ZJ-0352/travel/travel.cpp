#include<bits/stdc++.h>
using namespace std;
const int M=5005;

int n,m;
vector<int>E[M];
void Link(int u,int v) {
	E[u].push_back(v);
}

struct node {
	int A[M];
} P,Min;
bool Compare(node &a,node &b) {
	for(int i=1; i<=n; i++)
		if(a.A[i] != b.A[i])
			return a.A[i] < b.A[i];
	return false;
}

int Dfnc;
void Travel_Tree(int u,int f,int ava,int avb) {
//	printf("dfsing %d %d\n",u,f);
	P.A[++Dfnc]=u;
	for(int i=0; i<(int)E[u].size(); i++) {
		int v=E[u][i];
		if(v == f || (u == ava && v == avb) || (u == avb && v == ava))continue;
		Travel_Tree(v,u,ava,avb);

	}
}

struct P60 {
	void Solve() {
		Travel_Tree(1,-1,-1,-1);
		for(int i=1; i<n; i++)printf("%d ",P.A[i]);
		printf("%d\n",P.A[n]);
	}
} p_60;

struct P100 {
	bool Inq[M];
	int Q[M],Deg[M];
	void Topo_Sort() {
		int L=0,R=0;
		for(int i=1; i<=n; i++)Deg[i]=(int)E[i].size();
		for(int i=1; i<=n; i++)if(Deg[i] == 1)Q[R++]=i;
		while(L<R) {
			int u=Q[L++];
			Inq[u]=true;
			for(int i=0; i<(int)E[u].size(); i++) {
				int v=E[u][i];
				Deg[v]--;
				if(Deg[v] == 1)Q[R++]=v;
			}
		}
	}
	void Solve() {
		Topo_Sort();
		for(int i=1; i<=n; i++)Min.A[i]=n-i+1;
		for(int u=1; u<=n; u++)
			for(int i=0; i<(int)E[u].size(); i++) {
				int v=E[u][i];
				if(u>v || Inq[u] || Inq[v])continue;
				Dfnc=0;
				Travel_Tree(1,-1,u,v);
				if(Compare(P,Min))Min=P;
			}
		for(int i=1; i<n; i++)printf("%d ",Min.A[i]);
		printf("%d\n",Min.A[n]);
	}
} p_100;

int main() {

//	printf("Th mecordciop'a %.5lf MB.\n",(&mem2-&mem1)/1024.0/1024);

	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);

	scanf("%d%d",&n,&m);
	for(int i=1; i<=m; i++) {
		int u,v;
		scanf("%d%d",&u,&v);
		Link(u,v);
		Link(v,u);
	}
	for(int i=1; i<=n; i++)sort(E[i].begin(),E[i].end());
	if(m == n-1)p_60.Solve();
	else p_100.Solve();
	return 0;
}
