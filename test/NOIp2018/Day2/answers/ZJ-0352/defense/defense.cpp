#include<bits/stdc++.h>
using namespace std;
const int M=2e5+5;

int n,m;
int P[M];
char Fea[15];

struct Edge {
	int v,to;
} E[M<<1];
int Head[M],e_tot;
void Link(int u,int v) {
	E[++e_tot]=(Edge) {
		v,Head[u]
	};
	Head[u]=e_tot;
}
int Mst[M];

struct P44 {
	long long dp[2][M];
	void dfs(int u,int f) {
		dp[0][u]=0,dp[1][u]=P[u];
		for(int i=Head[u]; i!=0; i=E[i].to) {
			int v=E[i].v;
			if(v == f)continue;
			dfs(v,u);
			dp[0][u]+=dp[1][v];
			dp[1][u]+=min(dp[1][v],dp[0][v]);
		}
		if(Mst[u] == 1)dp[0][u]=2e18;
		if(Mst[u] == 0)dp[1][u]=2e18;
	}
	void Solve() {
		for(int i=1; i<=m; i++) {
			int u,v;
			scanf("%d",&u);
			scanf("%d",&Mst[u]);
			scanf("%d",&v);
			scanf("%d",&Mst[v]);
			dfs(1,-1);
			long long ans=min(dp[0][1],dp[1][1]);
			if(ans >= 1e18)ans=-1;
			printf("%lld\n",ans);
			Mst[u]=Mst[v]=-1;
		}
	}
} p_44;

struct P60 {
	long long In[2][M],Out[2][M];
	int fa[M];
	void dfs_dp(int u) {
		In[0][u]=0,In[1][u]=P[u];
		for(int i=Head[u]; i!=0; i=E[i].to) {
			int v=E[i].v;
			if(v == fa[u])continue;
			fa[v]=u;
			dfs_dp(v);
			In[0][u]+=In[1][v];
			In[1][u]+=min(In[1][v],In[0][v]);
		}
	}
	void re_dfs_dp(int u) {
		if(u!=1) {
			int f=fa[u];
			Out[0][u]=Out[1][f]+In[1][f]-P[f]-min(In[1][u],In[0][u]);
			Out[1][u]=min(Out[0][u],min(Out[0][f],Out[1][f])+In[0][f]-In[1][u])+P[u];
//			printf("%d In %lld %lld Out %lld %lld\n",u,In[0][u],In[1][u],Out[0][u],Out[1][u]);
		} else Out[1][u]=P[u];
		for(int i=Head[u]; i!=0; i=E[i].to) {
			int v=E[i].v;
			if(v == fa[u])continue;
			re_dfs_dp(v);
		}
	}
	void Init_In_Out() {
		dfs_dp(1);
		re_dfs_dp(1);
	}
	void Solve() {
		Init_In_Out();
		for(int i=1; i<=m; i++) {
			int u,v;
			scanf("%d",&u);
			scanf("%d",&Mst[u]);
			scanf("%d",&v);
			scanf("%d",&Mst[v]);
			if(fa[u] == v)swap(u,v);
			if(Mst[u] == 0 && Mst[v] == 0)puts("-1");
			else printf("%lld\n",In[Mst[v]][v]+Out[Mst[u]][u]+
				            (Mst[u] ? (In[1][u]-min(In[1][v],In[0][v]))-P[u] : (In[0][u]-In[1][v])));
			Mst[u]=Mst[v]=-1;
		}
	}
} p_60;

int main() {

//	printf("Th mecordciop'a %.5lf MB.\n",(&mem2-&mem1)/1024.0/1024);

	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);

	scanf("%d%d%s",&n,&m,Fea);
	for(int i=1; i<=n; i++)scanf("%d",&P[i]);
	for(int i=1; i<n; i++) {
		int u,v;
		scanf("%d%d",&u,&v);
		Link(u,v);
		Link(v,u);
	}
	for(int i=1; i<=n; i++)Mst[i]=-1;
	if(Fea[1]=='2')p_60.Solve();
	else p_44.Solve();
	return 0;
}
