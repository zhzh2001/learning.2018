#include <cstdio>
#include <cctype>
#include <vector>
#include <algorithm>
#define maxn 5005
#define maxm 5005

inline char nc() {
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2) {
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}

inline int read() {
	int x=0,f=0;
	char ch=0;
	while (f^=(ch=='-'),!isdigit(ch=nc()));
	while (x=x*10+ch-'0',isdigit(ch=nc()));
	if (f) return -x;return x;
}
int N,M,Ans[maxn],fa[maxn],poi,used;
std::vector<int> e[maxn];
bool Vis[maxn];
void PreDfs(int x) {
	for (int i=0;i<e[x].size();i++)
		if (!fa[e[x][i]]) fa[e[x][i]]=x,PreDfs(e[x][i]);
}
void Dfs(int x) {
	Vis[Ans[++Ans[0]]=x]=1;
	int y;
	for (int i=0;i<e[x].size();i++) {
		if (used==1&&x!=poi) break;
		y=e[x][i];
		if (fa[x]^e[x][i]&&Vis[e[x][i]]) {
			if (used) continue;
			int flg=-1;
			for (int j=Ans[0];Ans[j]!=e[x][i];j--) if (Ans[j]>x) flg=j;
			if (flg==-1){used=2;continue;}
			for (int j=Ans[0];j>=flg;j--) Vis[Ans[j]]=0;
			poi=Ans[Ans[0]=flg-1];
			used=1;
			fa[x]=e[x][i];
			return;
		} else if (fa[x]^e[x][i]) Dfs(e[x][i]);
	}
	if (used==1&&x!=poi) fa[x]=y;
	if (used==1&&x==poi)
		used=2;
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	N=read(),M=read();
	for (int i=1,x,y;i<=M;i++)
		x=read(),y=read(),e[x].push_back(y),e[y].push_back(x);
	for (int i=1;i<=N;i++)
		std::sort(e[i].begin(),e[i].end());
	fa[1]=-1,PreDfs(1);
	Dfs(1);
	for (int i=1;i<=N;i++)
		printf("%d%c",Ans[i],i^N?' ':'\n');
	return 0;
}
