#include <cstdio>
#include <cctype>
#include <cstdlib>
#include <cstring>
#define maxn 100005
#define LL long long

inline int read() {
	int x=0,f=0;
	char ch=0;
	while (f^=(ch=='-'),!isdigit(ch=getchar()));
	while (x=x*10+ch-'0',isdigit(ch=getchar()));
	if (f) return -x;return x;
}

int N,M,cst[maxn];
char typ[4];
inline int min(int x,int y) {
	if (x<y) return x;return y;
}
void Work1() {
	LL sum[2]={0,0};
	cst[0]=cst[N+1]=1e9;
	for (int i=1;i<=N;i++)
		cst[i]=read(),sum[i&1]+=cst[i];
	for (int i=1,x,y,a,b;i<=M;i++) {
		x=read(),a=read(),y=read(),b=read();
		if ((a==0&&b==0)&&(x-y==1||y-x==1)) {printf("%d\n",-1);continue;}
		if (x&1) {
			if (a) {
				if (y&1) {
					if (b) printf("%d\n",sum[1]);
					else printf("%d\n",sum[1]-cst[b]+min(cst[b+1],cst[b-1]));
				}else {
					if (b) printf("%d\n",min(sum[0]+cst[a],sum[1]+cst[b]));
					else printf("%d\n",sum[1]);
				}
			} else {
				if (y&1) {
					if (b) printf("%d\n",sum[0]+cst[b]);
					else printf("%d\n",sum[0]);
				}else {
					if (b) printf("%d\n",sum[0]);
					else printf("%d\n",sum[0]-cst[b]+min(cst[b+1],cst[b-1]));
				}
			}
		} else {
			if (a) {
				if (!(y&1)) {
					if (b) printf("%d\n",sum[0]);
					else printf("%d\n",sum[0]-cst[b]+min(cst[b+1],cst[b-1]));
				}else {
					if (b) printf("%d\n",min(sum[1]+cst[a],sum[0]+cst[b]));
					else printf("%d\n",sum[0]);
				}
			} else {
				if (!(y&1)) {
					if (b) printf("%d\n",sum[0]+cst[b]);
					else printf("%d\n",sum[1]);
				}else {
					if (b) printf("%d\n",sum[1]);
					else printf("%d\n",sum[1]-cst[b]+min(cst[b+1],cst[b-1]));
				}
			}
		}
	}
	exit(0);
}
int f[maxn][2],vis[maxn];
void Work2() {
	for (int i=1;i<=N;i++)
		cst[i]=read();
	for (int i=1,x,a,y,b;i<=M;i++) {
		x=read(),a=read(),y=read(),b=read();
		if ((a==0&&b==0)&&(x-y==1||y-x==1)) {printf("%d\n",-1);continue;}
		memset(f,63,sizeof(f)),f[0][1]=f[0][0]=0;
		for (int j=1;j<=N;j++) {
			if ((i!=x||a!=1)&&(j!=y||b!=1)) f[i][0]=f[i-1][1];
			if ((i!=x||a!=0)&&(i!=y||b!=0)) f[i][1]=min(f[i-1][1],f[i-1][0])+cst[i];
		}
		printf("%d\n",min(f[N][1],f[N-1][1]));
	}
	exit(0);
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	N=read(),M=read();
	scanf("%s",typ);
	if (typ[0]=='A'&&N>2000) Work1();else if (typ[0]=='A') Work2();
	return 888;
	return 0;
}
