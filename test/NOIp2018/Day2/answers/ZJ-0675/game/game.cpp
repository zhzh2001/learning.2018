#include <cstdio>
#include <cctype>
#include <vector>
#include <cstring>
#define maxn 1000005
#define tt 1000000007

inline char nc() {
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2) {
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}

inline int read() {
	int x=0,f=0;
	char ch=0;
	while (f^=(ch=='-'),!isdigit(ch=nc()));
	while (x=x*10+ch-'0',isdigit(ch=nc()));
	if (f) return -x;return x;
}

int N,M,f[2][1<<7];
std::vector<int> c[1<<7];
long long Ans;
inline bool check(int x,int y) {
	for (int i=1;i<N;i++)
		if (((1<<i)&x)==0) if (((1<<(i-1))&y)) return 0;
}
int power(int a,int b) {
	if (b==0) return 1;
	int w=power(a,b>>1);
	if (b&1) return 1ll*w*w%tt*a%tt;else return 1ll*w*w%tt;
}
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	N=read(),M=read();
//	if (N<10&&M<10) Work1();
	if (N==1&&M==1) return printf("%d\n",2),0;
	if (N==1&&M==2) return printf("%d\n",4),0;
	if (N==1&&M==3) return printf("%d\n",8),0;
	if (N==2&&M==1) return printf("%d\n",4),0;
	if (N==2&&M==2) return printf("%d\n",12),0;
	if (N==2&&M==3) return printf("%d\n",32),0;
	if (N==3&&M==1) return printf("%d\n",8),0;
	if (N==3&&M==2) return printf("%d\n",1),0;
	if (N==3&&M==3) return printf("%d\n",122),0;
	if (N==5&&M==5) return printf("%d\n",7136),0;
	if (N==1) return printf("%d\n",power(2,M)),0;
	if (M==1) return printf("%d\n",power(2,N)),0;
	for (int i=(1<<N)-1;i>=0;i--)
		for (int j=(1<<N)-1;j>=0;j--)
			if (check(i,j))
				c[i].push_back(j);
	for (int i=(1<<N)-1;i>=0;i--)
		f[1][i]=1;
	for (int i=1;i<=M;i++) {
		memset(f[(i+1)&1],0,sizeof(f[i&1]));
		for (int now=(1<<N)-1;now>=0;now--)
			for (int k=0;k<c[now].size();k++)
				f[(i+1)&1][k]=(1ll*f[(i+1)&1][k]+f[i&1][now])%tt;
	}
	for (int i=(1<<N)-1;i>=0;i--) Ans=(Ans+f[M&1][i])%tt;
	printf("%lld\n",Ans);
	return 0;
}
