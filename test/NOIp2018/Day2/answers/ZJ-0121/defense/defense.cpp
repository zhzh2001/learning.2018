#include<bits/stdc++.h>
#define LL long long 
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define dep(i,r,l) for(int i=r;i>=l;i--)
#define fy 200212180
#define N 10010
using namespace std;
int n,m,i,j,k,l,r,x,y,num;
int a[N],b[N],c[N],v[N],boo[2010][2010],dp[N][2];
int read()
{
	int x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
void add(int l,int r)
{
	a[++num]=r; b[num]=c[l]; c[l]=num;
	a[++num]=l; b[num]=c[r]; c[r]=num;
}
void dfs(int k,int fa)
{
	int sum1=0,sum2=0;
	for(int j=c[k];j;j=b[j])
	  if(a[j]!=fa)
	  {
	  	dfs(a[j],k);
	  	sum1+=min(dp[a[j]][1],dp[a[j]][0]);
	  	sum2+=dp[a[j]][1];
	  }
	if(dp[k][1]!=fy+1) dp[k][1]=sum1+v[k];
	if(dp[k][0]!=fy+1) dp[k][0]=sum2;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(); m=read(); read(); num=0;
	rep(i,1,n) v[i]=read();
	rep(i,1,n-1)
	{
		l=read(); r=read();
		add(l,r);
		boo[l][r]=1; boo[r][l]=1;
	}
	rep(i,1,m)
	{
		l=read(); x=read(); r=read(); y=read();
		if(boo[l][r]&&!x&&!y){printf("-1\n"); continue;}
		rep(i,1,n) dp[i][1]=fy,dp[i][0]=fy;
		dp[l][1-x]=fy+1; dp[r][1-y]=fy+1;
		dfs(1,0);
		printf("%d\n",min(dp[1][1],dp[1][0]));
	}
	return 0;
}
