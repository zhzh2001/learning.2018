#include<bits/stdc++.h>
#define LL long long 
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define dep(i,r,l) for(int i=r;i>=l;i--)
#define fy 20021218
#define N 5010
#define maxn 100010
using namespace std;
int n,m,i,j,k,l,r,num,cnt,top;
int a[maxn],b[maxn],c[maxn],boo[maxn],visit[maxn],Q[maxn],f[N][N];
int dfn[maxn],low[maxn];
void add(int l,int r)
{
	a[++num]=r; b[num]=c[l]; c[l]=num;
	a[++num]=l; b[num]=c[r]; c[r]=num;
}
void get_ans1(int k)
{
	printf("%d ",k); int n=0; visit[k]=1;
	for(int j=c[k];j;j=b[j]) 
	  if(!visit[a[j]]) f[k][++n]=a[j];
	sort(f[k]+1,f[k]+n+1);
	rep(i,1,n) get_ans1(f[k][i]);
}
void Tarjan(int k,int fa)
{
	dfn[k]=low[k]=++cnt;
	Q[++top]=k;
	for(int j=c[k];j;j=b[j])
	{
		if(a[j]!=fa)
		{
			if(dfn[a[j]]) low[k]=min(low[k],dfn[a[j]]);
			else
			{
				Tarjan(a[j],k);
				low[k]=min(low[k],low[a[j]]);
			}
		}
	}
	if(dfn[k]==low[k])
	{
		if(Q[top]==k) top--;
		else
		{
			while(Q[top]!=k)
			{
				boo[Q[top]]=1;
				top--;
			}
			boo[Q[top]]=1;
			top--;
		}
	}
}
void get_ans(int k,int begin)
{
	printf("%d ",k); int n=m=0; visit[k]=1;
	for(int j=c[k];j;j=b[j]) 
	  if(!visit[a[j]]) 
	  {
	  	if(boo[a[j]]) 
	  	{
	  		if(a[j]<begin) f[k][++n]=a[j];
		}
		else f[k][++n]=a[j];
	  }
	sort(f[k]+1,f[k]+n+1);
	rep(i,1,n)
	{
		if(boo[f[k][i]]) get_ans(f[k][i],begin);
		else get_ans1(f[k][i]);
	}
}
void get_ans3(int k)
{
	printf("%d ",k); int n=m=0; visit[k]=1;
	for(int j=c[k];j;j=b[j]) 
	  if(!visit[a[j]]) f[k][++n]=a[j];
	sort(f[k]+1,f[k]+n+1); int h=0;
	dep(i,n,1) if(boo[f[k][i]]){h=f[k][i]; break;}
	rep(i,1,n) 
	{
		if(boo[f[k][i]]&&h){get_ans(f[k][i],h); h=0;}
		else get_ans1(f[k][i]);
	}
}
void get_ans2(int k)
{
	printf("%d ",k); int n=0; visit[k]=1;
	for(int j=c[k];j;j=b[j]) 
	  if(!visit[a[j]]) f[k][++n]=a[j];
	sort(f[k]+1,f[k]+n+1);
	rep(i,1,n) 
	{
		if(boo[f[k][i]]) get_ans3(f[k][i]);
		else get_ans2(f[k][i]);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m); num=0;
	if(m<n)
	{
		rep(i,1,m)
		{
			scanf("%d%d",&l,&r);
			add(l,r);
		}
		get_ans1(1);
	}
	else
	{
		rep(i,1,m)
		{
			scanf("%d%d",&l,&r);
			add(l,r);
		}
		top=cnt=0; Tarjan(1,0);
		if(boo[1]) get_ans3(1);
		else get_ans2(1);
	}
	return 0;
}
