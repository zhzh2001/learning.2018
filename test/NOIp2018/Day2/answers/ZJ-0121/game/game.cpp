#include<bits/stdc++.h>
#define LL long long 
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define dep(i,r,l) for(int i=r;i>=l;i--)
#define fy 1000000007
#define N 10
#define M 1000010
using namespace std;
LL n,m,i,j,k,l,r;
LL dp[N][M],sum[N+M];
void swap(LL &a,LL &b){LL t=a; a=b; b=t;}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m); k=0;
	if(n>m) swap(n,m);
	dp[1][1]=1; dp[2][0]=1; sum[2]=2;
	rep(i,3,n+1)
	  rep(j,0,i-1)   
	  {
	  	if(i-j-1>1&&i-j<=n&&j-1>=1&&j<m) dp[i-j][j]=dp[i-j-1][j-1]*(i-1);
	  	else dp[i-j][j]=sum[i-1];
	  	sum[i]+=dp[i-j][j];
	  }
	k++;
	rep(i,n+2,m+1)
	{
		rep(j,0,i-k-1)  
		{
			if(i-k-j-1>1&&i-k-j<=n&&j>=1&&j+1<m) dp[i-k-j][j+1]=dp[i-k-j-1][j]*(i-k);
			else dp[i-k-j][j+1]=sum[i-1];
			sum[i]+=dp[i-k-j][j+1];
		}
	}
	rep(i,m+2,n+m+1)
	{
		dep(j,n+1,i-m)  
		{
			if(j-1>1&&j<=n&&i-k-j>=1&&i-k-j+1<m) dp[j][i-k-j+1]=dp[j-1][i-k-j]*(n+m+3-i);
			else dp[j][i-k-j+1]=sum[i-1];
			sum[i]+=dp[j][i-k-j+1];
		}
	}
	printf("%lld",dp[n][m]+dp[n+1][m-1]);
	return 0;
}
