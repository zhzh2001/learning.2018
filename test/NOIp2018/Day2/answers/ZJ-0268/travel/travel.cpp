#include<bits/stdc++.h>
#define M 5005
using namespace std;
bool mmm1;
struct E{
	int from,to,nx;
}edge[M<<1];
int tot,head[M];
void Addedge(int a,int b){
	edge[++tot].to=b;
	edge[tot].nx=head[a];
	edge[tot].from=a;
	head[a]=tot;
}
struct p60{
	vector<int>way[M];
	int Ans[M],id,fa[M];
	void dfs(int now){
		Ans[++id]=now;
		for(int i=head[now];i;i=edge[i].nx){
			int nxt=edge[i].to;
			if(nxt==fa[now])continue;
			way[now].push_back(nxt);
		}
		sort(way[now].begin(),way[now].end());
		for(int i=0;i<(int)way[now].size();i++){
			int nxt=way[now][i];
			fa[nxt]=now;
			dfs(nxt);
		}
	}
	void solve(int n){
		for(int i=1;i<=n;i++)way[i].clear();
		id=0,fa[1]=-1;
		dfs(1);
		for(int i=1;i<id;i++)printf("%d ",Ans[i]);
		printf("%d\n",Ans[id]);
	}
}P60;
struct p100{
	vector<int>way[M];
	queue<int>Q;
	int deg[M],n;
	bool done[M];
	void Topo(){
		while(!Q.empty()){
			int now=Q.front();
			Q.pop();
			for(int i=head[now];i;i=edge[i].nx){
				int nxt=edge[i].to;
				deg[nxt]--;
				if(deg[nxt]<=1&&!done[nxt]){
					Q.push(nxt);
					done[nxt]=1;
				}
			}
		}
	}
	int Ans[M],id,fa[M],lima,limb;
	int res[M],n_res;
	void dfs(int now){
		//printf("%d\n",now);
		res[++n_res]=now;
		//if(n_res>n)return;
		for(int i=0;i<(int)way[now].size();i++){
			int nxt=way[now][i];
			if(nxt==fa[now])continue;
			if(now==lima&&nxt==limb)continue;
			if(now==limb&&nxt==lima)continue;
			fa[nxt]=now;
			dfs(nxt);
		}
	}
	bool check(int n){
		if(id==0)return 1;
		for(int i=1;i<=n;i++){
			if(Ans[i]!=res[i])return res[i]<Ans[i];
		}
		return false;
	}
	void Init(int n){
		for(int now=1;now<=n;now++){
			for(int i=head[now];i;i=edge[i].nx){
				int nxt=edge[i].to;
				way[now].push_back(nxt);
			}
			sort(way[now].begin(),way[now].end());
		}
	}
	void solve(int n){
		this->n=n;
		memset(done,0,sizeof(done));
		while(!Q.empty())Q.pop();
		memset(deg,0,sizeof(deg));
		for(int i=1;i<=n;i++)way[i].clear();
		id=0,fa[1]=-1;
		for(int i=1;i<=tot;i+=2)deg[edge[i].from]++,deg[edge[i].to]++;
		for(int i=1;i<=n;i++){
			if(deg[i]==1){
				Q.push(i);
				done[i]=1;
			}
		}
		Topo();
		Init(n);
		//for(int i=1;i<=n;i++)if(!done[i])printf("%d",i);
		//puts("");
		for(int i=1;i<=tot;i+=2){
			if(done[edge[i].from]||done[edge[i].to])continue;
			lima=edge[i].from;
			limb=edge[i].to;
			n_res=0;
			dfs(1);
			if(check(n)){
				id=n_res;
				for(int i=1;i<=n_res;i++)Ans[i]=res[i];
			}
		}
		for(int i=1;i<id;i++)printf("%d ",Ans[i]);
		printf("%d\n",Ans[id]);
	}
}P100;
bool mmm2;
int main(){
	//printf("%lf\n",(&mmm2-&mmm1)/1024.0/1024.0);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int a,b;
		scanf("%d%d",&a,&b);
		Addedge(a,b);
		Addedge(b,a);
	}
	if(m==n-1)P60.solve(n);
	else P100.solve(n);
	return 0;
}
