#include<bits/stdc++.h>
#define M 100005
using namespace std;
bool mmm1;
char S[10];
struct E{
	int to,nx;
}edge[M<<1];
int tot,head[M];
void Addedge(int a,int b){
	edge[++tot].to=b;
	edge[tot].nx=head[a];
	head[a]=tot;
}
struct Que{
	int a,x,b,y;
}Q[M];
int Cost[M];
struct p50{
	int id;
	long long dp[M][2],oo;
	int fa[M];
	void dfs(int now){
		dp[now][0]=0;
		dp[now][1]=Cost[now];
		for(int i=head[now];i;i=edge[i].nx){
			int nxt=edge[i].to;
			if(nxt==fa[now])continue;
			fa[nxt]=now;
			dfs(nxt);
			if(dp[nxt][1]==oo)dp[now][0]=oo;
			if(dp[now][0]!=oo)dp[now][0]+=dp[nxt][1];
			if(min(dp[nxt][0],dp[nxt][1])==oo)dp[now][1]=oo;
			if(dp[now][1]!=oo)dp[now][1]+=min(dp[nxt][0],dp[nxt][1]);
		}
		if(now==Q[id].a){
			if(Q[id].x==1)dp[now][0]=oo;
			else dp[now][1]=oo;
		}
		if(now==Q[id].b){
			if(Q[id].y==1)dp[now][0]=oo;
			else dp[now][1]=oo;
		}
	}
	void solve(int m){
		oo=1e15;
		for(int i=1;i<=m;i++){
			id=i;
			fa[1]=-1;
			dfs(1);
			long long ans=min(dp[1][0],dp[1][1]);
			if(ans==oo)puts("-1");
			else printf("%lld\n",ans);
		}
	}
}P50;
struct p12{
	long long oo;
	long long Ldp[M][2],Rdp[M][2];
	void solve(int n,int m){
		oo=1e15;
		memset(Ldp,123,sizeof(Ldp));
		memset(Rdp,123,sizeof(Rdp));
		Ldp[0][0]=Ldp[0][1]=0;
		Rdp[n+1][0]=Rdp[n+1][1]=0;
		for(int i=1;i<=n;i++){
			Ldp[i][0]=Ldp[i-1][1];
			Ldp[i][1]=min(Ldp[i-1][0],Ldp[i-1][1])+Cost[i];
		}
		for(int i=n;i>=1;i--){
			Rdp[i][0]=Rdp[i+1][1];
			Rdp[i][1]=min(Rdp[i+1][0],Rdp[i+1][1])+Cost[i];
		}
		for(int i=1;i<=m;i++){
			if(Q[i].x==0&&Q[i].y==0){
				puts("-1");
				continue;
			}
			long long ans=0;
			if(Q[i].a>Q[i].b){
				swap(Q[i].a,Q[i].b);
				swap(Q[i].x,Q[i].y);
			}
			ans=Ldp[Q[i].a][Q[i].x]+Rdp[Q[i].b][Q[i].y];
			printf("%lld\n",ans);
		}
	}
}P12;
struct p8{
	long long oo;
	long long Ldp[M][2],Rdp[M][2];
	void solve(int n,int m){
		oo=1e15;
		memset(Ldp,123,sizeof(Ldp));
		memset(Rdp,123,sizeof(Rdp));
		Ldp[0][0]=Ldp[0][1]=0;
		Rdp[n+1][0]=Rdp[n+1][1]=0;
		int C=Cost[1];
		Cost[1]=0;
		for(int i=1;i<=n;i++){
			Ldp[i][0]=Ldp[i-1][1];
			Ldp[i][1]=min(Ldp[i-1][0],Ldp[i-1][1])+Cost[i];
		}
		for(int i=n;i>=1;i--){
			Rdp[i][0]=Rdp[i+1][1];
			Rdp[i][1]=min(Rdp[i+1][0],Rdp[i+1][1])+Cost[i];
		}
		for(int i=1;i<=m;i++){
			if(Q[i].x==0&&Q[i].y==0){
				puts("-1");
				continue;
			}
			long long ans=C;
			if(Q[i].b==1){
				swap(Q[i].a,Q[i].b);
				swap(Q[i].x,Q[i].y);
			}
			ans+=1ll*Ldp[Q[i].b][Q[i].y]+Rdp[Q[i].b][Q[i].y]-(Q[i].y==1)*(Cost[Q[i].b]);
			printf("%lld\n",ans);
		}
	}
}P8;
bool mmm2;
int main(){
	//printf("%lf\n",(&mmm2-&mmm1)/1024.0/1024.0);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m;
	scanf("%d%d%s",&n,&m,S);
	for(int i=1;i<=n;i++)scanf("%d",&Cost[i]);
	for(int i=1;i<n;i++){
		int a,b;
		scanf("%d%d",&a,&b);
		Addedge(a,b);
		Addedge(b,a);
	}
	for(int i=1;i<=m;i++)scanf("%d%d%d%d",&Q[i].a,&Q[i].x,&Q[i].b,&Q[i].y);
	//P12.solve(n,m);
	//if(S[0]=='A'&&S[1]=='1')P8.solve(n,m);
	if(n<=2000&&m<=2000)P50.solve(m);
	else if(S[0]=='A'&&S[1]=='2')P12.solve(n,m);
	else if(S[0]=='A'&&S[1]=='1')P8.solve(n,m);
	return 0;
}
