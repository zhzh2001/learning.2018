#include<bits/stdc++.h>
#define P 1000000007
using namespace std;
bool mmm1;
struct p20{
	int Q[1<<19];
	int id,n,m;
	int Count(int x,int y){
		return (x-1)*m+y;
	}
	void dfs(int x,int y,int chs){
		if(x==n&&y==m){
			//printf("%d\n",chs);
			Q[++id]=chs;
			return;
		}
		if(x<n)dfs(x+1,y,chs|(1<<Count(x+1,y)));
		if(y<m)dfs(x,y+1,chs|(1<<Count(x,y+1)));
	}
	int num[1<<19],ans;
	bool is_ok(){
		for(int i=1;i<n;i++){
			for(int j=1;j<m;j++){
				if(num[Count(i+1,j)]<num[Count(i,j+1)])return false;
			}
		}
		return true;
	}
	bool check(){
		if(!is_ok())return false;
		int pre=1e9;
		for(int i=1;i<=id;i++){
			int res=0;
			for(int j=1;j<=n*m;j++){
				if(((1<<j)&Q[i]))res=res*2+num[j];
			}
			//printf("res=%d\n",res);
			if(res>pre)return false;
			pre=res;
		}
		/*for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++)printf("%d ",num[(i-1)*m+j]);
			puts("");
		}
		puts("");
		puts("AC");*/
		return true;
	}
	void redfs(int step){
		if(step==n*m+1){
			ans+=check();
			return;
		}
		num[step]=1;
		redfs(step+1);
		num[step]=0;
		redfs(step+1);
	}
	void solve(int n,int m){
		this->n=n,this->m=m;
		id=0;
		ans=0;
		dfs(1,1,2);
		redfs(1);
		printf("%d\n",ans);
	}
}P20;
long long mul(long long a,int b){
	long long res=1ll;
	while(b){
		if(b&1)res=1ll*res*a%P;
		a=1ll*a*a%P;
		b>>=1;
	}
	return res;
}
struct p50{
	void solve(int m){
		long long ans=4ll*mul(3,m-1)%P;
		printf("%lld\n",ans);
	}
}P50;
struct p30{
	void solve(int m){
		long long ans=112ll*mul(3,m-3)%P;
		printf("%lld\n",ans);
	}
}P30;
bool mmm2;
int main(){
	//printf("%lf\n",(&mmm2-&mmm1)/1024.0/1024.0);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	//P50.solve(m);
	//P20.solve(n,m);
	//return 0;
	if(n==2)P50.solve(m);
	else if(m==2)P50.solve(n);
	else if(n==3)P30.solve(m);
	else if(m==3)P30.solve(n);
	else P20.solve(n,m);
	return 0;
}
