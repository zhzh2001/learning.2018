var
  a:array[0..5005,0..5005]of longint;
  e:array[0..400005,0..2]of longint;
  h,nx,d,na,ra,ddfn,llow,q,w,ii:array[0..20005]of longint;
  c:array[0..20005]of boolean;
  n,m,i,j,nm,ss,t,fl,qw:longint;
  ff:boolean;
procedure cn(x,y:longint);
begin
  inc(nm);
  d[nm]:=y;
  nx[nm]:=h[x];
  h[x]:=nm;
end;
procedure s(u,l,r:longint);
      var
         i,j,x,y:longint;
      begin
         i:=l;
         j:=r;
         x:=a[u][(l+r) div 2];
         repeat
           while a[u][i]<x do
            inc(i);
           while x<a[u][j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[u][i];
                a[u][i]:=a[u][j];
                a[u][j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           s(u,l,j);
         if i<r then
           s(u,i,r);
      end;
procedure f(u,fa:longint);
var
  i,v,ll:longint;
begin
  inc(na[0]);
  na[na[0]]:=u;
  a[u][0]:=0;
  ll:=h[u];
  while ll<>0 do
  begin
    v:=d[ll];
    if (v=fa) or c[ll] then
    begin
      ll:=nx[ll];
      continue;
    end;
    inc(a[u][0]);
    a[u][a[u][0]]:=v;
    ll:=nx[ll];
  end;
  if a[u][0]=0 then
    exit;
  s(u,1,a[u][0]);
  for i:=1 to a[u][0] do
    f(a[u][i],u);
end;
procedure tj(u,fa:longint);
var
  v,ll:longint;
begin
  inc(ss);
  ddfn[u]:=ss;
  llow[u]:=ss;
  inc(t);
  q[t]:=u;
  ll:=h[u];
  while ll<>0 do
  begin
    v:=d[ll];
    if ddfn[v]=0 then
    begin
      tj(v,u);
      if llow[v]<llow[u] then
        llow[u]:=llow[v];
    end else
      if v<>fa then
      begin
        if ddfn[v]<llow[u] then
          llow[u]:=ddfn[v];
      end;
    ll:=nx[ll];
  end;
  if ddfn[u]=llow[u] then
  begin
    inc(fl);
    repeat
      ii[q[t]]:=fl;
      dec(t);
    until q[t+1]=u;
  end;
end;
begin
  assign(input,'travel.in'); reset(input);
  assign(output,'travel.out'); rewrite(output);
  readln(n,m);
  nm:=0;
  qw:=0;
  for i:=1 to n do
  begin
    h[i]:=0;
    ra[i]:=n+n+n;
    a[i][0]:=0;
    ddfn[i]:=0;
    llow[i]:=0;
    ii[i]:=0;
  end;
  for i:=1 to m+m do
    c[i]:=false;
  for i:=1 to m do
  begin
    readln(e[i][1],e[i][2]);
    cn(e[i][1],e[i][2]);
    cn(e[i][2],e[i][1]);
  end;
  if m=n-1 then
  begin
    na[0]:=0;
    f(1,0);
    for i:=1 to n-1 do
      write(na[i],' ');
    writeln(na[n]);
  end else
  begin
    ss:=0;
    fl:=0;
    tj(1,0);
    for i:=1 to m do
      if ii[e[i][1]]=ii[e[i][2]] then
      begin
        inc(qw);
        w[qw]:=i;
      end;
    for i:=1 to qw do
    begin
      c[w[i]*2-1]:=true;
      c[w[i]*2]:=true;
      na[0]:=0;
      f(1,0);
      ff:=false;
      for j:=1 to n do
        if na[j]<>ra[j] then
        begin
          if na[j]<ra[j] then
            ff:=true;
          break;
        end;
      if ff then
      begin
        for j:=1 to n do
          ra[j]:=na[j];
      end;
      c[w[i]*2-1]:=false;
      c[w[i]*2]:=false;
    end;
    for i:=1 to n-1 do
      write(ra[i],' ');
    writeln(ra[n]);
  end;
  close(input);close(output);
end.