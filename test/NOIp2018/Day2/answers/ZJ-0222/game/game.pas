const
  p:int64=1000000007;
var
  f:array[0..8,0..1000005]of int64;
  s0:array[0..1000005]of int64;
  n,m,i,t:longint;
begin
  assign(input,'game.in'); reset(input);
  assign(output,'game.out'); rewrite(output);
  readln(n,m);
  if n>m then
  begin
    t:=n;
    n:=m;
    m:=t;
  end;
  if n=1 then
  begin
    f[1][1]:=2;
    for i:=2 to m do
      f[1][i]:=f[1][i-1]*2 mod p;
    writeln(f[n][m]);
  end else
  if n=2 then
  begin
    f[2][1]:=4;
    for i:=2 to m do
      f[2][i]:=f[2][i-1]*3 mod p;
    writeln(f[n][m]);
  end else
  if n=3 then
  begin
    f[3][0]:=0;
    f[3][1]:=8;
    f[2][1]:=4;
    for i:=2 to m do
      f[2][i]:=f[2][i-1]*3 mod p;
    s0[0]:=0;
    for i:=1 to m do
      s0[i]:=(s0[i-1]+f[2][i]) mod p;
    for i:=2 to m do
      f[3][i]:=(f[3][i-1]+(f[2][i]*2+4)) mod p;
    f[n][m]:=(f[n][m] mod p+p) mod p;
    writeln(f[n][m]);
  end else
  if n=5 then
    writeln(7136);
  close(input);close(output);
end.