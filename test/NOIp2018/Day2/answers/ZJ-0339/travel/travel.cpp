#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring> 
#include <vector> 
using namespace std;

int n,m; 
vector <int> v[5050]; 
bool vis[5050]; 

void dfs(int x) 
{
	printf("%d ",x); 
	vis[x]=1; 
	for(int i=0;i<v[x].size();++i) 
	  if(!vis[v[x][i]]) 
		dfs(v[x][i]); 
}

int sta[5050],top,len,cirx[5050],ciry[5050]; 
bool used[5050],bj; 
void find(int x,int fa)  
{ 
	if(bj) return ; 
	used[x]=1; 
	sta[++top]=x; 
	for(int i=0;i<v[x].size()&&!bj;++i) 
	  if(v[x][i]!=fa) 
	  {
	  	if(used[v[x][i]]) 
	  	{
	  		int t=v[x][i];  
	  		while(sta[top]!=v[x][i]) 
	  		{
	  			cirx[++len]=t; 
	  			ciry[len]=sta[top]; 
				t=sta[top]; 
				top--;    
			  }  
			cirx[++len]=t; 
			ciry[len]=sta[top]; 
			bj=1; 
			return ; 
		  }
		  else find(v[x][i],x); 
	  }
}

int tx,ty,ans[5050],tmp[5050],siz;   
void dfs1(int x) 
{
	tmp[++siz]=x; 
	vis[x]=1; 
	for(int i=0;i<v[x].size();++i) 
	{
	  if((x==tx && v[x][i]==ty) || (x==ty && v[x][i]==tx)) continue; 
	  if(!vis[v[x][i]] ) 
		dfs1(v[x][i]); 		
	}
}

void check() 
{
	for(int i=1;i<=n;++i) 
	  if(tmp[i]<ans[i]) 
	  {
	  	while(i<=n) 
		  {
		  	ans[i]=tmp[i]; 
		  	++i; 
		  }
	  } 
	  else if(tmp[i]>ans[i]) break; 

}

int main()
{
	int x,y; 
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m); 		
	for(int i=1;i<=m;++i) 
	{
		scanf("%d%d",&x,&y); 
		v[x].push_back(y); 
		v[y].push_back(x); 
	} 
	for(int i=1;i<=n;++i) sort(v[i].begin(),v[i].end()); 
	if(n==m) 
	{
		for(int i=1;i<=n;++i) ans[i]=0x3f3f3f3f; 
		find(1,1); 
		for(int i=1;i<=len;++i) 
		{
		  tx=cirx[i],ty=ciry[i]; 
		  siz=0; 
		  for(int i=1;i<=n;++i) vis[i]=0; 
		  dfs1(1); 			
		  check(); 
		}
		for(int i=1;i<=n;++i) printf("%d ",ans[i]); 
	} 
	else dfs(1); 
	fclose(stdin);
	fclose(stdout);
	return 0;
}


