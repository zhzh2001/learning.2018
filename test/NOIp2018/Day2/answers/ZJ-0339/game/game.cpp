#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
using namespace std;

const int mod = 1e9+7; 
int n,m; 
bool mp[10][300][300]; 
int dp[100100][300]; 

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m); 
	mp[2][0][0]=1,mp[2][0][2]=1; 
	mp[2][1][0]=1,mp[2][1][2]=1; 
	mp[2][2][0]=1,mp[2][2][1]=1,mp[2][2][2]=1,mp[2][2][3]=1; 
	mp[2][3][0]=1,mp[2][3][1]=1,mp[2][3][2]=1,mp[2][3][3]=1; 
	for(int i=3;i<=n;++i) 
	  for(int j=0;j<(1<<i);++j) 
	    for(int k=0;k<(1<<i);++k) 
	      mp[i][j][k]=mp[i-1][j>>1][k>>1]&mp[i-1][j%(1<<(i-1))][k%(1<<(i-1))]; 
	/*for(int i=0;i<(1<<3);++i) 
	  for(int j=0;j<(1<<3);++j) 
	    cout<<i<<" "<<j<<" "<<mp[3][i][j]<<endl; */
	for(int i=0;i<(1<<n);++i) dp[1][i]=1; 
	for(int i=2;i<=m;++i) 
	  for(int j=0;j<(1<<n);++j) 
	    for(int k=0;k<(1<<n);++k) 
	      if(mp[n][j][k]) 
	        dp[i][j]=(dp[i][j]+dp[i-1][k])%mod; 
	int ans = 0; 
	for(int i=0;i<(1<<n);++i) ans=(ans+dp[m][i])%mod; 
	printf("%d\n",ans); 
	fclose(stdin);
	fclose(stdout);
	return 0;
}


