#include <bits/stdc++.h>
#define rep(i, a, b) for (int i = (a); i <= (b); ++i)
#define dep(i, a, b) for (int i = (a); i >= (b); --i)
#define mp make_pair
#define ft first
#define sc second
#define pb push_back
#define pii pair<int, int>
using namespace std;

int qInt() {
  int x = 0; char ch = getchar();
  while (ch < '0' || ch > '9') ch = getchar();
  while (ch >= '0' && ch <= '9') (x *= 10) += (ch - '0'), ch = getchar();
  return x;
}

vector<int> vec[5010];
int w[5010], len = 0, vis[5010];

void dfs(int u, int fa) {
  printf("%d ", u);
  if (vec[u].empty()) return ;
  for (int i = 0; i < vec[u].size(); ++i)
    if (vec[u][i] != fa)
      dfs(vec[u][i], u);
}

int dfs_c(int u, int fa) {
  vis[u] = 1;
  if (vec[u].empty()) return -1;
  for (int i = 0; i < vec[u].size(); ++i)
    if (vec[u][i] != fa) {
      if (vis[vec[u][i]]) {
	len = 1;
	w[1] = u;
	return vec[u][i];
      }
      int d = dfs_c(vec[u][i], u);
      if (d != -1) {
	w[++len] = u;
	if (u == d) d = -1;
	return d;
      }
    }
  return -1;
}

int now = 0, n, m;
int Ans[5010], res[5010];

void update() {
  int flag = 0;
  rep(i, 1, n)
    if (Ans[i] < res[i]) return ;
    else if (Ans[i] > res[i]) {
      flag = 1;
      break;
    }
  if (flag) {
    rep(i, 1, n) Ans[i] = res[i];
  }
  return ;
}

void dfs_gao(int u, int fa, int nou, int nov) {
  res[++now] = u;
  if (vec[u].empty()) return ;
  for (int i = 0; i < vec[u].size(); ++i)
    if (vec[u][i] != fa) {
      if ((u == nou && vec[u][i] == nov) || (u == nov && vec[u][i] == nou)) continue;
      dfs_gao(vec[u][i], u, nou, nov);
    }
}

int main() {
  
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);
  n = qInt(), m = qInt();
  rep(i, 1, m) {
    int u = qInt(), v = qInt();
    vec[u].pb(v), vec[v].pb(u);
  }
  rep(i, 1, n) if (!vec[i].empty()) sort(vec[i].begin(), vec[i].end());
  if (m == n - 1) {
    dfs(1, 0);
    return 0;
  }
  else {
    rep(i, 1, n) Ans[i] = n + 1;
    dfs_c(1, 0);
    rep(i, 2, len) {
      now = 0;
      dfs_gao(1, 0, w[i], w[i - 1]);
      update();
    }
    now = 0;
    dfs_gao(1, 0, w[len], w[1]);
    update();
    rep(i, 1, n) printf("%d ", Ans[i]);
  }
  return 0;

}
