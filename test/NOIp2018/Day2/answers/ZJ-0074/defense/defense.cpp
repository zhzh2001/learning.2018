#include <bits/stdc++.h>
#define rep(i, a, b) for (int i = (a); i <= (b); ++i)
#define dep(i, a, b) for (int i = (a); i >= (b); --i)
#define mp make_pair
#define ft first
#define sc second
#define pb push_back
#define int long long
#define pii pair<int, int>
using namespace std;

int qInt() {
  int x = 0; char ch = getchar();
  while (ch < '0' || ch > '9') ch = getchar();
  while (ch >= '0' && ch <= '9') (x *= 10) += (ch - '0'), ch = getchar();
  return x;
}

const int Inf = 1e12;

const int N = 200000 + 10;

int fst[N], lst[N], nxt[N], des[N], dp[N][2][2], cnt = 0, a, x, b, y, s[N];

void add(int u, int v) {
  if (!fst[u]) fst[u] = ++cnt;
  else nxt[lst[u]] = ++cnt;
  des[cnt] = v, lst[u] = cnt;
}

void dfs(int u, int fa) {
  dp[u][0][0] = dp[u][0][1] = dp[u][1][1] = 0;
  for (int i = fst[u]; i; i = nxt[i])
    if (des[i] != fa)
      dfs(des[i], u);
  int flag = 0;
  for (int i = fst[u]; i; i = nxt[i])
    if (des[i] != fa) {
      dp[u][1][1] += min(dp[des[i]][0][0], min(dp[des[i]][0][1], dp[des[i]][1][1]));
      dp[u][0][1] += dp[des[i]][1][1];
      dp[u][0][0] += min(dp[des[i]][0][1], dp[des[i]][1][1]);
      flag = 1;
    }
  dp[u][1][1] += s[u];
  if (!flag) dp[u][0][1] = Inf;
  if (a == u) {
    if (x == 1) dp[u][0][1] = dp[u][0][0] = Inf;
    else dp[u][1][1] = Inf;
  }
  if (b == u) {
    if (y == 1) dp[u][0][1] = dp[u][0][0] = Inf;
    else dp[u][1][1] = Inf;
  }
}

signed main() {

  freopen("defense.in", "r", stdin);
  freopen("defense.out", "w", stdout);
  int n = qInt(), m = qInt(); string st; cin >> st;
  rep(i, 1, n) s[i] = qInt();
  rep(i, 1, n - 1) {
    int u = qInt(), v = qInt();
    add(u, v), add(v, u);
  }
  for (; m --; ) {
    a = qInt(), x = qInt(), b = qInt(), y = qInt();
    if (a == b && x != y) {
      puts("-1");
      continue;
    }
    rep(i, 1, n) dp[i][0][0] = dp[i][0][1] = dp[i][1][1] = Inf;
    if (x == 0 && y == 0) {
      int flag = 0;
      for (int i = fst[a]; i; i = nxt[i])
	if (des[i] == b) {
	  flag = 1;
	  break;
	}
      if (flag) {
	puts("-1");
	continue;
      }
    }
    dfs(1, 0);
    printf("%lld\n", min(dp[1][1][1], dp[1][0][1]));
  }
  return 0;

}
