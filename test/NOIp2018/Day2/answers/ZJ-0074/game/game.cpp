#include <bits/stdc++.h>
#define rep(i, a, b) for (int i = (a); i <= (b); ++i)
#define dep(i, a, b) for (int i = (a); i >= (b); --i)
#define mp make_pair
#define ft first
#define sc second
#define pb push_back
#define pii pair<int, int>
using namespace std;

int qInt() {
  int x = 0; char ch = getchar();
  while (ch < '0' || ch > '9') ch = getchar();
  while (ch >= '0' && ch <= '9') (x *= 10) += (ch - '0'), ch = getchar();
  return x;
}

const int Mod = 1e9 + 7;
long long Pow(long long a, long long b) {
  long long Ans = 1;
  while (b) {
    if (b & 1) (Ans *= a) %= Mod;
    b >>= 1;
    (a *= a) %= Mod;
  }
  return Ans;
}

vector<int> vec[1010]; 
long long dp[2][1010];

int n, m;

int main() {

  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);
  n = qInt(), m = qInt();
  if (n <= 0 || m <= 0) {
    puts("0");
    return 0;
  }
  if (n == 1) {
    printf("%lld\n", Pow(2ll, m));
    return 0;
  }
  if (n == 2) {
    long long Ans = 4;
    Ans *= Pow(3, m - 1);
    Ans %= Mod;
    cout << Ans << endl;
    return 0;
  }
  long long Ans = Pow(2, m + 2);
  rep(i, 3, m) {
    Ans += 2ll * 3ll * Pow(2, m + 2 - i) % Mod;
    if (Ans >= Mod) Ans -= Mod;
  }
  cout << Ans * 2 % Mod << endl;
  return 0;

}
