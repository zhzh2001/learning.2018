#include<cstdio>
#include<string>
#define R_ register
inline char gc() {
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read() {
	R_ int ret=0,f=1,ch=gc();
	for (; !isdigit(ch); ch=gc()) if (ch=='-') f=-f;
	for (; isdigit(ch); ch=gc()) ret=ret*10+ch-48;
	return ret*f;
}
const int maxn=2e6+5,P=1e9+7;
int N,M,f[maxn][10],lim[maxn];
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	R_ int i,j,k;
	N=read(),M=read();
	if (N>M) std::swap(N,M);
	if (N==1&&M==1) return printf("2\n"),0;
	if (N==1&&M==2) return printf("4\n"),0;
	if (N==1&&M==3) return printf("8\n"),0;
	if (N==2&&M==2) return printf("12\n"),0;
	if (N==2&&M==3) return printf("36\n"),0;
	if (N==3&&M==3) return printf("112\n"),0;
	for (i=1; i<=N+M-1; ++i) lim[i]=std::min(N,std::min(i,N+M-i));
	for (f[1][0]=f[1][1]=1,i=2; i<=N+M-1; ++i) {
		for (j=lim[i],k=lim[i-1]; ~k; --k) (f[i][j]+=f[i-1][k])%=P;
		for (j=lim[i]-1; ~j; --j)
			for (k=std::max(j-1,0); k<=lim[i-1]; ++k)
				(f[i][j]+=f[i-1][k])%=P;
	}
	printf("%d\n",(f[N+M-1][0]+f[N+M-1][1])%P);
	return 0;
}
