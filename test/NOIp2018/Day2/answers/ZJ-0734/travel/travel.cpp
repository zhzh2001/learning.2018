#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#define R_ register
inline char gc() {
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read() {
	R_ int ret=0,f=1,ch=gc();
	for (; !isdigit(ch); ch=gc()) if (ch=='-') f=-f;
	for (; isdigit(ch); ch=gc()) ret=ret*10+ch-48;
	return ret*f;
}
const int maxn=1e4+5;
bool vis[maxn];
int dfn[maxn],low[maxn],sta[maxn],bel[maxn],sum[maxn],top,tar;
int N,M,A,B,cnt,tot,son[maxn],nxt[maxn],lnk[maxn],Ans[maxn],deg[maxn];
inline void add_edge(int x,int y) {
	son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot,++deg[x];
	son[++tot]=x,nxt[tot]=lnk[y],lnk[y]=tot,++deg[y];
}
void DFS(int x,int pre) {
	Ans[++cnt]=x;
	std :: vector <int> a;
	for (R_ int k=lnk[x],v; v=son[k],k; k=nxt[k]) if (v^pre) a.push_back(v);
	std::sort(a.begin(),a.end());
	for (R_ int i=0; i<(int)a.size(); ++i) DFS(a[i],x);
}
inline void tarjan(int x) {
	dfn[x]=low[x]=++cnt;
	vis[x]=1,sta[++top]=x;
	for (R_ int k=lnk[x],v; v=son[k],k; k=nxt[k])
		if (!dfn[v]) tarjan(v),low[x]=std::min(low[x],low[v]);
		else if (vis[v]) low[x]=std::min(low[x],dfn[v]);
	if (low[x]==dfn[x]) {
		++tar;
		int now;
		do {
			now=sta[top--];
			bel[now]=tar;
			++sum[now];
		} while (now^x);
	}
}
inline bool check() {
	for (R_ int i=1; i<=N; ++i) if (deg[i]>2) return 0;
	return 1;
}
void dfs1(int x) {
	R_ int k,v,V=N+1;
	vis[x]=1;
	Ans[++cnt]=x;
	for (k=lnk[x]; v=son[k],k; k=nxt[k]) if (!vis[v]&&v<A) V=v;
	if (V<N) dfs1(V);
}
void dfs2(int x) {
	R_ int k,v;
	for (k=lnk[x]; v=son[k],k; k=nxt[k])
		if (!vis[v]) vis[v]=1,Ans[++cnt]=v,dfs2(v);
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	R_ int i;
	for (N=read(),M=read(),i=1; i<=M; ++i) add_edge(read(),read());
	if (M<N) {
		DFS(1,0);
		for (i=1; i<=N; ++i) printf("%d ",Ans[i]);
		return 0;
	}
	if (check()) {
		for (int k=lnk[1]; k; k=nxt[k])
			if (!A) A=son[k];else B=son[k];
		if (A<B) std::swap(A,B);
		dfs1(1);
		dfs2(1);
		for (i=1; i<=N; ++i) printf("%d ",Ans[i]);
		return 0;
	}
	for (i=1; i<=N; ++i) printf("%d ",i);
	return 0;
}
