#include<cstdio>
#include<string>
#define R_ register
inline char gc() {
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read() {
	R_ int ret=0,f=1,ch=gc();
	for (; !isdigit(ch); ch=gc()) if (ch=='-') f=-f;
	for (; isdigit(ch); ch=gc()) ret=ret*10+ch-48;
	return ret*f;
}
const int maxn=1e5+5;
typedef long long LL;
LL f[maxn][2];
bool F[maxn][2];
int N,M,ty,A,X,B,Y,tot,son[maxn],nxt[maxn],lnk[maxn],p[maxn];
inline bool check(int x,bool flg,bool fa) {
	if (!fa&&!flg) return 0;
	if (x==A&&X!=flg) return 0;
	if (x==B&&Y!=flg) return 0;
	return 1;
}
inline void add_edge(int x,int y) {
	son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot;
	son[++tot]=x,nxt[tot]=lnk[y],lnk[y]=tot;
}
LL dfs(int x,bool flg,int pre=0) {
	if (F[x][flg]) return f[x][flg];
	LL ret=flg?p[x]:0;
	for (R_ int k=lnk[x],v; v=son[k],k; k=nxt[k]) if (v^pre) {
		LL S=-1,mn=1ll<<60;
		if (check(v,1,flg)) {
			S=dfs(v,1,x);
			if (~S) mn=std::min(S,mn);
		}
		if (check(v,0,flg)) {
			S=dfs(v,0,x);
			if (~S) mn=std::min(S,mn);
		}
		if (mn==(1ll<<60)) return -1;
		ret+=mn;
	}
	return F[x][flg]=1,f[x][flg]=ret;
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	R_ int i,j;
	for (N=read(),M=read(),ty=read(),i=1; i<=N; ++i) p[i]=read();
	for (i=1; i<N; ++i) add_edge(read(),read());
	for (i=1; i<=M; ++i) {
		A=read(),X=read(),B=read(),Y=read();
		if (A>B) std::swap(A,B),std::swap(X,Y);
		if (ty==2&&!X&&!Y) {printf("-1\n");continue;}
		if (A==1) {
			for (j=1; j<=N; ++j) F[j][0]=F[j][1]=0;
			printf("%d\n",dfs(1,X));
		} else {
			LL f1=dfs(1,0);
			for (j=1; j<=N; ++j) F[j][0]=F[j][1]=0;
			LL f2=dfs(1,1);
			for (j=1; j<=N; ++j) F[j][0]=F[j][1]=0;
			if (f1>f2) std::swap(f1,f2);
			if (f2==-1) printf("-1\n");
			else if (f1==-1) printf("%lld\n",f2);
			else printf("%lld\n",f1);
		}
	}
	return 0;
}
