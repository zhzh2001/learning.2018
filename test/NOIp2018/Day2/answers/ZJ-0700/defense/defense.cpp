#include<cstdio>
#include<cctype>
#define rg register
typedef long long LL;
template<typename T>inline T min(const T x,const T y){return x<y?x:y;}
template<typename T>inline T max(const T x,const T y){return x>y?x:y;}
template<typename T>inline T abs(const T x){return x<0?-x:x;}
template<typename T>inline void mind(T&x,const T y){if(x>y)x=y;}
template<typename T>inline void maxd(T&x,const T y){if(x<y)x=y;}
template<typename T>inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template<typename T>inline void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template<typename T>inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
const int maxn=100001,maxm=200001;
int n,m;
char type[2];
int val[maxn],head[maxn],nxt[maxm],tow[maxm],tmp;
inline void addb(const int u,const int v)
{
	tmp++;
	nxt[tmp]=head[u];
	head[u]=tmp;
	tow[tmp]=v;
}
int a,x,b,y;
LL son[maxn][2],son1[maxn][2];
void dfs(const int u,const int fa)
{	
	son[u][0]=0,son[u][1]=val[u];
	for(rg int i=head[u];i;i=nxt[i])
	{
		const int v=tow[i];
		if(v!=fa)
		{
			dfs(v,u);
			son[u][0]+=son[v][1];
			son[u][1]+=min(son[v][0],son[v][1]);
		}
	}
	if(u==b)son[u][y^1]=10000000001;
}
int main()
{
	freopen("defense.in","r",stdin);freopen("defense.out","w",stdout);
	read(n),read(m);
	scanf("%s",type);
	for(rg int i=1;i<=n;i++)read(val[i]);
	for(rg int i=1;i<n;i++)
	{
		int u,v;read(u),read(v);
		addb(u,v),addb(v,u);
	}
	if(n<=5000&&m<=5000)
	{
		for(rg int i=1;i<=m;i++)
		{
			read(a),read(x),read(b),read(y);
			dfs(a,0);
			if(son[a][x]>10000000000)print(-1);
			else print(son[a][x]);
			putchar('\n');
		}
		return 0;
	}
	if(type[0]=='A'&&type[1]=='2')
	{
		for(rg int i=1;i<=n;i++)
		{
			son[i][0]=0,son[i][1]=val[i];
			son1[i][0]=0,son1[i][1]=val[i];
		}
		for(rg int i=n-1;i>=1;i--)
		{
			son[i][0]+=son[i+1][1];
			son[i][1]+=min(son[i+1][0],son[i+1][1]);
		}
		for(rg int i=2;i<=n;i++)
		{
			son1[i][0]+=son1[i-1][1];
			son1[i][1]+=min(son1[i-1][0],son1[i-1][1]);
		}
		for(rg int i=1;i<=m;i++)
		{
			read(a),read(x),read(b),read(y);
			if(a>b)
			{
				a=b;
				const int z=x;x=y;y=z;
			}
			if(x==0&&y==0)print(-1);
			else print(son1[a][x]+son[a+1][y]);
			putchar('\n');
		}
		return 0;
	}
	return 0;
}
