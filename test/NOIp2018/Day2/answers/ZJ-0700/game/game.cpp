#include<cstdio>
#include<cctype>
#include<cstring>
#define rg register
typedef long long LL;
template<typename T>inline T min(const T x,const T y){return x<y?x:y;}
template<typename T>inline T max(const T x,const T y){return x>y?x:y;}
template<typename T>inline T abs(const T x){return x<0?-x:x;}
template<typename T>inline void mind(T&x,const T y){if(x>y)x=y;}
template<typename T>inline void maxd(T&x,const T y){if(x<y)x=y;}
template<typename T>inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template<typename T>inline void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template<typename T>inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
const LL mod=1000000007;
int n,m,bit[18];
inline LL pow(LL x,LL y)
{
	LL res=1;
	for(;y;y>>=1,x=x*x%mod)if(y&1)res=res*x%mod;
	return res;
}
LL dp[2][64],ans;
int main()
{
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	bit[1]=1;
	for(rg int i=2;i<=17;i++)bit[i]=bit[i-1]<<1;
	read(n),read(m);
	if(m==1)
	{
		print(pow(2,n));
		return 0;
	}
	if(n==1)
	{
		print(pow(2,m));
		return 0;
	}
	if(n==2)
	{
	//	print(pow(3,m-1)*4);
		for(rg int j=0;j<4;j++)dp[1][j]=1;
		for(rg int i=1;i<m;i++)
		{
			const int dq=i&1,nxt=dq^1;
			dp[nxt][0]=(dp[dq][0]+dp[dq][1]+dp[dq][2]+dp[dq][3])%mod;
			dp[nxt][1]=(dp[dq][2]+dp[dq][3])%mod;
			dp[nxt][2]=(dp[dq][0]+dp[dq][1]+dp[dq][2]+dp[dq][3])%mod;
			dp[nxt][3]=(dp[dq][2]+dp[dq][3])%mod;
		}
		for(rg int j=0;j<4;j++)(ans+=dp[m&1][j])%=mod;
		print(ans);
		return 0;
	}
	for(rg int i=0;i<bit[n+1];i++)dp[1][i]=1;
	for(rg int i=1;i<m;i++)
	{
		const int dq=i&1,nxt=dq^1;
		memset(dp[nxt],0,sizeof(dp[nxt]));
		for(rg int j=0;j<bit[n+n+1];j++)
			if(dp[dq][j])
			{
				int mother=(j>>1)&(bit[n+1]-1),father=0;
				for(rg int k=2;k<n;k++)if((j&bit[k+n])&&(mother&bit[k]))mother^=bit[k],father^=bit[k];
				mother|=bit[n];
				for(rg int k=mother;;k=(k-1)&mother)
				{
					int zt=0;
					const int K=k|father;
					for(rg int l=2;l<n;l++)if(((j&bit[l])!=0)==((K&bit[l-1])!=0))zt|=bit[l+n];
					(dp[nxt][zt|K]+=dp[dq][j])%=mod;
					if(k==0)break;
				}
			}
	}
	for(rg int j=0;j<bit[n+n+1];j++)(ans+=dp[m&1][j])%=mod;
	print(ans);
	return 0;
}
