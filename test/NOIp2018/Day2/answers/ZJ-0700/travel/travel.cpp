#include<cstdio>
#include<cctype>
#include<cstring>
#define rg register
typedef long long LL;
template<typename T>inline T min(const T x,const T y){return x<y?x:y;}
template<typename T>inline T max(const T x,const T y){return x>y?x:y;}
template<typename T>inline T abs(const T x){return x<0?-x:x;}
template<typename T>inline void mind(T&x,const T y){if(x>y)x=y;}
template<typename T>inline void maxd(T&x,const T y){if(x<y)x=y;}
template<typename T>inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template<typename T>inline void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template<typename T>inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
const int maxn=5005;
int n,m,edge[maxn][maxn];
int ans[maxn],tot;bool vis[maxn];
namespace subtask1
{
	void dfs(const int u)
	{
		if(vis[u])return;
		vis[u]=1;ans[++tot]=u;
		for(rg int i=1;i<=n;i++)if(edge[u][i])dfs(i);
	}
};
namespace subtask2
{
	int stack[maxn],top;
	bool ishuan[maxn];
	bool check(const int u,const int fa)
	{
		vis[u]=1;stack[++top]=u;
		for(rg int i=1;i<=n;i++)
			if(edge[u][i]&&i!=fa)
			{
				if(vis[i])
				{
					while(stack[top]!=i)ishuan[stack[top]]=1,top--;
					ishuan[i]=1;
					return 1;
				}
				else if(check(i,u))return 1;
			}
		return 0;
	}
	int ever,id;
	void dfs(const int u)
	{
		if(vis[u])return;
		vis[u]=1;ans[++tot]=u;
		for(rg int i=1;i<=n;i++)if(edge[u][i])dfs(i);
	}
	void dfs2(const int u,const int fa)
	{
		if(vis[u])return;
		vis[u]=1;ans[++tot]=u;
		if(ishuan[u]&&!id)
		{
			id=u;
			for(rg int i=1;i<=n;i++)
				if(edge[u][i]&&i!=fa)
				{
					ever++;
					if(ever==2)
					{
						ever=i;
						break;
					}
				}
			for(rg int i=1;i<=n;i++)
				if(edge[u][i]&&i!=fa)
				{
					dfs2(i,u);
					const int val=ans[tot],tp=tot;
					vis[u]=0,tot--;
					dfs(u);
					ans[tp]=val;
					break;
				}
		}
		else
		{
			for(rg int i=1;i<=n;i++)
				if(edge[u][i]&&i!=fa)
				{
					if(ishuan[i]&&id)
					{
						int ID=0x7f7f7f7f;
						for(rg int j=i+1;j<=n;j++)
							if(edge[u][j]&&j!=fa)
								mind(ID,j);
						if(ID!=0x7f7f7f7f)ever=ID;
						if(i>ever)continue;
						else dfs2(i,u);
					}
					else dfs2(i,u);
				}
		}
	}
};
int main()
{
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	read(n),read(m);
	for(rg int i=1;i<=m;i++)
	{
		int u,v;read(u),read(v);
		edge[u][v]=edge[v][u]=1;
	}
	if(m==n-1)subtask1::dfs(1);
	else
	{
		subtask2::check(1,0);
		memset(vis,0,sizeof(vis));
		subtask2::dfs2(1,0);
	}
	for(rg int i=1;i<=n;i++)
	{
		print(ans[i]);
		if(i!=n)putchar(' ');
	}
	return 0;
}
