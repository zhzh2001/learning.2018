#include <cstdio>
#include <algorithm>
const long long MOD = 1000000007;
long long POWER(long long a, long long b)
{
	long long r = 1;
	for (; b; b >>= 1)
	{
		if (b & 1)
			r = r * a % MOD;
		a = a * a % MOD;
	}
	return r;
}
int N, M;
int main()
{
	scanf("%d%d", &N, &M);
	if (N > M)
		std::swap(N, M);
	if (N == 1)
	{
		printf("%lld\n", POWER(2, M));
		return 0;
	}
	if (N == 2)
	{
		printf("%lld\n", 2 * POWER(3, M - 1) * 2 % MOD);
		return 0;
	}
	if (N == 3)
	{
		printf("%lld\n", 112 * POWER(3, M - 3) % MOD);
		return 0;
	}
	long long O = 0;
	O = (O + POWER(4, N - 2) * POWER(3, M - N) % MOD * POWER(2, N + 1)) % MOD;
	printf("%lld\n", O);
	return 0;
}
