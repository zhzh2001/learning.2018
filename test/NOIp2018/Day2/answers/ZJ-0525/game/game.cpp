#include <cstdio>
#include <algorithm>
const long long MOD = 1000000007;
int N, M, b[21][1000];
long long O;
long long POWER(long long a, long long b)
{
	long long r = 1;
	for (; b; b >>= 1)
	{
		if (b & 1)
			r = r * a % MOD;
		a = a * a % MOD;
	}
	return r;
}
void DFS(int x, int y)
{
	if (x == N)
	{
		long long T = 4;
		for (int i = 2; i <= N + M - 2; i++)
		{
			int count = 0;
			for (int x = 1; x <= N; x++)
			{
				int y = i - x;
				if (1 <= y && y < M && b[x][y] == 1)
					count++;
			}
			if (count > 1)
				return;
			if (count == 0)
				T = T * 2 % MOD;
		}
		O += T;
		return;
	}
	b[x][y] = 0;
	if (y == M - 1)
		DFS(x + 1, 1);
	else
		DFS(x, y + 1);
	if (x >= 3 && y >= 3)
		return;
	for (int i = 1; i < x; i++)
		for (int j = 1; j < y; j++)
			if (!b[i][j])
				return;
	for (int i = 1; i < x; i++)
		if (1 <= x + y - i && x + y - i < M && b[i][x + y - i])
			return;
	b[x][y] = 1;
	if (y == M - 1)
		DFS(x + 1, 1);
	else
		DFS(x, y + 1);
}
int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &N, &M);
	if (N > M)
		std::swap(N, M);
	if (N == 1)
	{
		printf("%lld\n", POWER(2, M));
		return 0;
	}
	int D = std::max(0, M - N - 2);
	M -= D;
	DFS(1, 1);
	printf("%lld\n", O % MOD * POWER(3, D) % MOD);
	return 0;
}
