#include <cstdio>
#include <vector>
#include <algorithm>
int N, M, O[5001];
std::vector < std::pair < int, int > > E[5001];
int q[5001], fa[5001], vi[5001], ref[5001];
std::vector < std::pair < int, int > >::iterator ue[5001];
void make(int exc)
{
	for (int i = 1; i <= N; i++)
		vi[i] = 0;
	int D = 0, TIME = 0;
	q[++D] = 1;
	ue[D] = E[1].begin();
	fa[1] = 0;
	vi[1] = 1;
	ref[++TIME] = 1;
	while (D)
	{
		while (ue[D] != E[q[D]].end() && (ue[D] -> first == fa[q[D]] || ue[D] -> second == exc))
			ue[D]++;
		if (ue[D] == E[q[D]].end())
			D--;
		else
		{
			int To = ue[D]++ -> first;
			if (vi[To])
				return;
			fa[To] = q[D];
			vi[To] = 1;
			ref[++TIME] = To;
			q[++D] = To;
			ue[D] = E[To].begin();
		}
	}
	if (TIME < N)
		return;
	int diff = 1;
	while (diff <= N && O[diff] == ref[diff])
		diff++;
	if (diff <= N && O[diff] > ref[diff])
		std::copy(ref + 1, ref + N + 1, O + 1);
}
int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &N, &M);
	for (int i = 1, u, v; i <= M; i++)
	{
		scanf("%d%d", &u, &v);
		E[u].push_back(std::make_pair(v, i));
		E[v].push_back(std::make_pair(u, i));
	}
	for (int i = 1; i <= N; i++)
		std::sort(E[i].begin(), E[i].end());
	O[1] = N + 1;
	if (M == N - 1)
		make(0);
	else
		for (int i = 1; i <= M; i++)
			make(i);
	for (int i = 1; i <= N; i++)
		printf("%d%c", O[i], " \n"[i == N]);
	return 0;
}
