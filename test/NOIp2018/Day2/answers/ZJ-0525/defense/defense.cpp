#include <cstdio>
#include <algorithm>
const long long INF = 1LL << 50;
int N, Q, head[100001], next[199999], to[199999], E;
long long a[100001];
long long f0[100001], f1[100001], g0[100001], g1[100001];
int q[100001], ue[100001], fa[100001], dep[100001], size[100001], son[100001], left[100001], right[100001], ref[100001], top[100001];
void BFS()
{
	int H = 0, T = 1, u;
	q[1] = 1;
	while (H < T)
		for (int e = head[u = q[++H]]; e; e = next[e])
			if (to[e] != fa[u])
			{
				fa[q[++T] = to[e]] = u;
				dep[to[e]] = dep[u] + 1;
			}
	for (int i = 1; i <= N; i++)
		size[i] = 1;
	for (int i = N; i > 1; i--)
	{
		size[fa[q[i]]] += size[q[i]];
		if (size[q[i]] > size[son[fa[q[i]]]])
			son[fa[q[i]]] = q[i];
	}
	for (int i = N; i; i--)
	{
		int u = q[i];
		f0[u] = 0;
		f1[u] = a[u];
		for (int e = head[u]; e; e = next[e])
			if (to[e] != fa[u])
			{
				f0[u] += f1[to[e]];
				f1[u] += std::min(f0[to[e]], f1[to[e]]);
			}
	}
	for (int i = 1; i <= N; i++)
	{
		int u = q[i];
		for (int e = head[u]; e; e = next[e])
			if (to[e] != fa[u])
			{
				g0[to[e]] = f0[u] - f1[to[e]] + g1[u];
				g1[to[e]] = f1[u] - std::min(f0[to[e]], f1[to[e]]) + std::min(g0[u], g1[u]);
			}
	}
}
void DFS()
{
	int D = 0, TIME = 0;
	q[++D] = 1;
	ue[D] = head[1];
	ref[left[1] = right[1] = ++TIME] = 1;
	top[1] = 1;
	while (D)
		if (son[q[D]] && !left[son[q[D]]])
		{
			int To = son[q[D]];
			ref[left[To] = right[To] = ++TIME] = To;
			top[To] = top[q[D]];
			q[++D] = To;
			ue[D] = head[To];
		}
		else
		{
			while (ue[D] && (to[ue[D]] == fa[q[D]] || to[ue[D]] == son[q[D]]))
				ue[D] = next[ue[D]];
			if (ue[D])
			{
				int To = to[ue[D]];
				ue[D] = next[ue[D]];
				ref[left[To] = right[To] = ++TIME] = To;
				top[To] = To;
				q[++D] = To;
				ue[D] = head[To];
			}
			else if (--D)
				right[q[D]] = right[q[D + 1]];
		}
}
struct node
{
	long long a00, a01, a10, a11;
	friend inline node operator + (const node &a, const node &b)
	{
		return (node)
		{
			std::min(std::min(a.a00 + b.a10, a.a01 + b.a00), a.a01 + b.a10),
			std::min(std::min(a.a00 + b.a11, a.a01 + b.a01), a.a01 + b.a11),
			std::min(std::min(a.a10 + b.a10, a.a11 + b.a00), a.a11 + b.a10),
			std::min(std::min(a.a10 + b.a11, a.a11 + b.a01), a.a11 + b.a11)
		};
	}
	inline node reverse()
	{
		return (node) { a00, a10, a01, a11 };
	}
}
T[262144];
inline node single(long long w0, long long w1)
{
	return (node) { w0, INF, INF, w1 };
}
void build(int p, int l, int r)
{
	if (l == r)
	{
		T[p] = single(f0[ref[l]] - (son[ref[l]] ? f1[son[ref[l]]] : 0), f1[ref[l]] - (son[ref[l]] ? std::min(f0[son[ref[l]]], f1[son[ref[l]]]) : 0));
		return;
	}
	int m = l + r >> 1;
	build(p << 1, l, m);
	build(p << 1 | 1, m + 1, r);
	T[p] = T[p << 1] + T[p << 1 | 1];
}
node G(int p, int l, int r, int L, int R)
{
	if (L <= l && r <= R)
		return T[p];
	int m = l + r >> 1;
	if (R <= m)
		return G(p << 1, l, m, L, R);
	if (L > m)
		return G(p << 1 | 1, m + 1, r, L, R);
	return G(p << 1, l, m, L, R) + G(p << 1 | 1, m + 1, r, L, R);
}
long long query(int u, int cu, int v, int cv)
{
	if (dep[u] < dep[v])
	{
		std::swap(u, v);
		std::swap(cu, cv);
	}
	if (left[v] <= left[u] && right[u] <= right[v])
	{
		int du = 0;
		node L = single(f0[u], f1[u]);
		while (top[u] != top[v])
		{
			if (du)
				L = L + single(f0[u] - f1[du], f1[u] - std::min(f0[du], f1[du]));
			if (u != top[u])
				L = L + G(1, 1, N, left[top[u]], left[u] - 1).reverse();
			du = top[u];
			u = fa[du];
		}
		if (u == v)
			L = L + single(g0[du], g1[du]);
		else
		{
			if (du)
				L = L + single(f0[u] - f1[du], f1[u] - std::min(f0[du], f1[du]));
			int near = ref[left[v] + 1];
			if (u != near)
				L = L + G(1, 1, N, left[near], left[u] - 1).reverse();
			L = L + single(g0[near], g1[near]);
		}
		if (cu == 0)
			return cv ? L.a01 : L.a00;
		else
			return cv ? L.a11 : L.a10;
	}
	else
	{
		int du = 0, dv = 0;
		node L = single(f0[u], f1[u]), R = single(f0[v], f1[v]);
		while (top[u] != top[v])
			if (dep[top[u]] > dep[top[v]])
			{
				if (du)
					L = L + single(f0[u] - f1[du], f1[u] - std::min(f0[du], f1[du]));
				if (u != top[u])
					L = L + G(1, 1, N, left[top[u]], left[u] - 1).reverse();
				du = top[u];
				u = fa[du];
			}
			else
			{
				if (dv)
					R = single(f0[v] - f1[dv], f1[v] - std::min(f0[dv], f1[dv])) + R;
				if (v != top[v])
					R = G(1, 1, N, left[top[v]], left[v] - 1) + R;
				dv = top[v];
				v = fa[dv];
			}
		node ans;
		if (u == v)
			ans = L + single(g0[du] - f1[dv], g1[du] - std::min(f0[dv], f1[dv])) + R;
		else if (left[u] <= left[v] && right[v] <= right[u])
		{
			if (dv)
				R = single(f0[v] - f1[dv], f1[v] - std::min(f0[dv], f1[dv])) + R;
			int near = ref[left[u] + 1];
			if (v != near)
				R = G(1, 1, N, left[near], left[v] - 1) + R;
			dv = near;
			ans = L + single(g0[du] - f1[dv], g1[du] - std::min(f0[dv], f1[dv])) + R;
		}
		else
		{
			if (du)
				L = L + single(f0[u] - f1[du], f1[u] - std::min(f0[du], f1[du]));
			int near = ref[left[v] + 1];
			if (u != near)
				L = L + G(1, 1, N, left[near], left[u] - 1).reverse();
			du = near;
			ans = L + single(g0[du] - f1[dv], g1[du] - std::min(f0[dv], f1[dv])) + R;
		}
		if (cu == 0)
			return cv ? ans.a01 : ans.a00;
		else
			return cv ? ans.a11 : ans.a10;
	}
}
int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d%*s", &N, &Q);
	for (int i = 1; i <= N; i++)
		scanf("%lld", a + i);
	for (int i = 1, u, v; i < N; i++)
	{
		scanf("%d%d", &u, &v);
		next[++E] = head[u], to[E] = v, head[u] = E;
		next[++E] = head[v], to[E] = u, head[v] = E;
	}
	BFS();
	DFS();
	build(1, 1, N);
	while (Q--)
	{
		int u, cu, v, cv;
		scanf("%d%d%d%d", &u, &cu, &v, &cv);
		long long _ = query(u, cu, v, cv);
		if (_ >= INF)
			puts("-1");
		else
			printf("%lld\n", _);
	}
	return 0;
}
