#include<bits/stdc++.h>
#define ll long long
#define N 100010
#define INF 999999999
#define min(a,b) ((a)<(b)?(a):(b))
#define mem(a,b) memset(a,b,sizeof(a))
#define fsb(a,b,c) for(int a=b;a<=(c);a++)
#define fbs(a,b,c) for(int a=b;a>=(c);a--)
using namespace std;
int n,m,fa[N],cost[N],f[N],cnt=0,head[N],x,y,A,B,vis[N],ison[N],lca,ans;
char s[10];
struct edge{
	int p,nt;
}a[N*2];
template<typename qw>inline void rll(qw &x){
	qw f=1;x=0;char c=getchar();
	while(!isdigit(c))f=c=='-'?-1:f,c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	x*=f;
}
inline void add(int x,int y){
	a[++cnt]=(edge){y,head[x]};head[x]=cnt;
}
inline void dfs(int u,int f){
	fa[u]=f;
	for(int t=head[u];t!=-1;t=a[t].nt){
		int v=a[t].p;
		if(v==f) continue;
		dfs(v,u);
	}
	
}
inline void dfs2(int u,int now){
	if(now>=ans)return;
	if(u==n+1){
		fsb(i,1,n)
			if(vis[i]==0){
				for(int t=head[i];t!=-1;t=a[t].nt)
					if(vis[a[t].p]==0)return;
			}
		ans=min(ans,now);return;
	}
	if(vis[u]!=-1)dfs2(u+1,now);else{
		vis[u]=0;dfs2(u+1,now);
		vis[u]=1;dfs2(u+1,now+cost[u]);
		vis[u]=-1;
	}
}
int main(){
	freopen("defense.in","r",stdin);freopen("defense.out","w",stdout);
	rll(n);rll(m);scanf("%s",s);
	fsb(i,1,n)rll(cost[i]);
	mem(head,255);
	fsb(i,1,n-1){
		rll(x);rll(y);add(x,y);add(y,x);
	}
	if(n<=100){
		fsb(i,1,m){
			ans=INF;
			rll(A);rll(x);rll(B);rll(y);
			mem(vis,255);
			vis[A]=x;vis[B]=y;
			dfs2(1,cost[A]*x+cost[B]*y);
			ans=ans==INF?-1:ans;
			printf("%d\n",ans);
		}
	}else{
		dfs(1,0);
		fsb(i,1,m){
			rll(A);rll(x);rll(B);rll(y);
			if(x+y==0){
				int flag=0;
				for(int t=head[A];t!=-1;t=a[t].nt){
					if(a[t].p==B){
						flag=1;break;
					}
				}
				if(flag){
					puts("-1");continue;
				}
			}
			int t=A;
			while(t!=0){
				vis[t]=1;t=fa[t];
			}
			t=B;
			while(t!=0){
				ison[t]=1;
				if(vis[t]){
					lca=t;break;
				}
				t=fa[t];
			}
		}
	}
	return 0;
}
