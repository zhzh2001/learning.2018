#include<bits/stdc++.h>
#define M 5005
using namespace std;
bool cur1;
int n,m;
vector<int>G[M];
int ans[M],f[M],tot;
struct p60 {
	void dfs(int u) {
		ans[++tot]=u;
		for(int i=0; i<(int)G[u].size(); i++) {
			int v=G[u][i];
			if(f[u]==v)continue;
			f[v]=u;
			dfs(v);
		}
	}
	void solve() {
		tot=0;
		for(int i=1; i<=m; i++) {
			int a,b;
			scanf("%d%d",&a,&b);
			G[a].push_back(b);
			G[b].push_back(a);
		}
		for(int i=1; i<=n; i++)sort(G[i].begin(),G[i].end());
		dfs(1);
		for(int i=1; i<=n; i++)printf("%d ",ans[i]);
		puts("");
	}
} p60;
struct p100 {
	int deg[M],res[M],loop[M],q[M],a,b,ans[M],tot;
	void dfs(int u) {
		res[++tot]=u;
		for(int i=0; i<(int)G[u].size(); i++) {
			int v=G[u][i];
			if(f[u]==v)continue;
			if((a==u&&v==b)||(a==v&&b==u))continue;
			f[v]=u;
			dfs(v);
		}
	}
	void solve() {
		memset(deg,0,sizeof(deg));
		memset(ans,0,sizeof(ans));
		for(int i=1; i<=m; i++) {
			int a,b;
			scanf("%d%d",&a,&b);
			G[a].push_back(b);
			G[b].push_back(a);
			deg[a]++;
			deg[b]++;
		}
		int l=1,r=0;
		for(int i=1; i<=n; i++) {
			sort(G[i].begin(),G[i].end());
			loop[i]=1;
			if(deg[i]==1)q[++r]=i;
		}
		while(l<=r) {
			int u=q[l++];
			loop[u]=0;
			for(int i=0; i<(int)G[u].size(); i++) {
				int v=G[u][i];
				if(--deg[v]==1)q[++r]=v;
			}
		}
		for(int i=1; i<=n; i++) {
			for(int j=0; j<(int)G[i].size(); j++) {
				int v=G[i][j];
				if(v<i)continue;
				if(!loop[i]||!loop[v])continue;
				a=i,b=v;
				tot=0;
				dfs(1);
				for(int i=1; i<=n; i++)
					if(res[i]<ans[i]||ans[i]==0) {
						memcpy(ans,res,sizeof(res));
						break;
					}
					else if(ans[i]<res[i])break;
			}
		}
		for(int i=1;i<=n;i++)printf("%d ",ans[i]);
	}
}p100;
bool cur2;
int main() { //const long long memory file
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024.0);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n-1==m)p60.solve();
	else p100.solve();
	return 0;
}
