#include<bits/stdc++.h>
#define P 1000000007
using namespace std;
bool cur1;
int n,m;
struct p20 {
	int s[10][10];
	int res[20],pre[20],flag;
	void dfs(int x,int y,int dep) {
		res[dep]=s[x][y];
		if(x==n-1&&y==m-1) {
			for(int i=1; i<=n+m-1; i++) {
				if(res[i]>pre[i]) {
					flag=0;
					break;
				} else if(res[i]<pre[i]) {
					memcpy(pre,res,sizeof(res));
					break;
				} else pre[i]=res[i];
			}
			return;
		}
		if(x+1<n&&flag)dfs(x+1,y,dep+1);
		if(y+1<m&&flag)dfs(x,y+1,dep+1);
	}
	void solve() {
		int ans=0;
		for(int i=0; i<(1<<(m*n)); i++) {
			for(int j=1; j<=n+m-1; j++)pre[j]=1;
			memset(s,0,sizeof(s));
			for(int j=0; j<n; j++)
				for(int k=0; k<m; k++)
					if(i&(1<<(j*m+k)))s[j][k]=1;
			flag=1;
			dfs(0,0,1);
			ans+=flag;
		}
		printf("%d\n",ans);
	}
} p20;
struct p30 {
	void solve() {
		int res=4;
		for(int i=m-1; i>=1; i--) {
			res=res*3%P;
		}
		printf("%d\n",res);
	}
} p30;
struct p1 {
	void solve() {
		int ans=1;
		for(int i=1; i<=m; i++)ans=ans*2%P;
		printf("%d\n",ans);
	}
}p1;
struct p3{
	void solve(){
		int ans=112;
		for(int i=1;i<=m-3;i++)ans=1ll*ans*3%P;
		printf("%d\n",ans);
	}
}p3;
bool cur2;
int main() { //const long long memory file
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024.0);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==1)p1.solve();
	else if(n==2)p30.solve();
	else if(n==3&&m>=3)p3.solve();
	else p20.solve();
	return 0;
}
