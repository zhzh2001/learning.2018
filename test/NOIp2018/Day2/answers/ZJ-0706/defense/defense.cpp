#include<bits/stdc++.h>
#define M 100005
using namespace std;
bool cur1;
char s[10];
int n,m,cnt;
int head[M],to[2*M],nxt[2*M],op[M],p[M],f[M];
void Add(int a,int b) {
	to[++cnt]=b;
	nxt[cnt]=head[a];
	head[a]=cnt;
}
struct p40 {
	long long dp[2][2005];
	void dfs(int u) {
		long long sum1=0,sum2=0;
		bool flag=0;
		for(int i=head[u]; ~i; i=nxt[i]) {
			int v=to[i];
			if(f[u]==v)continue;
			f[v]=u;
			dfs(v);
			if(op[v]==0)flag=1,sum1+=dp[0][v];
			else if(op[v]==1)sum1+=dp[1][v],sum2+=dp[1][v];
			else sum1+=min(dp[1][v],dp[0][v]),sum2+=dp[1][v];
		}
		if(flag&&op[u]!=0)dp[1][u]=p[u]+sum1;
		if(!flag) {
			if(op[u]!=0)dp[1][u]=p[u]+sum1;
			if(op[u]!=1)dp[0][u]=sum2;
		}
	}
	void solve() {
		memset(op,-1,sizeof(op));
		while(m--) {
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			op[a]=x;
			op[b]=y;
			memset(dp,0x3f,sizeof(dp));
			dfs(1);
			op[a]=op[b]=-1;
			long long ans=min(dp[0][1],dp[1][1]);
			printf("%lld\n",ans>1e10?-1:ans);
		}
	}
} p40;
struct pA2 {
	long long dpl[2][M],dpr[2][M];
	void solve() {
		memset(dpl,0x3f,sizeof(dpl));
		memset(dpr,0x3f,sizeof(dpr));
		dpl[1][1]=p[1],dpl[0][1]=0;
		for(int i=2; i<=n; i++)dpl[0][i]=dpl[1][i-1],dpl[1][i]=min(dpl[1][i-1],dpl[0][i-1])+p[i];
		dpr[1][n]=p[n],dpr[0][n]=0;
		for(int i=n-1; i>=1; i--)dpr[0][i]=dpr[1][i+1],dpr[1][i]=min(dpr[1][i+1],dpr[0][i+1])+p[i];
		while(m--) {
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(a>b)swap(a,b),swap(x,y);
			if(x==y&&x==0)puts("-1");
			else {
				long long ans=dpl[x][a]+dpr[y][b];
				printf("%lld\n",ans>1e10?-1:ans);
			}
		}
	}
} pA2;
bool cur2;
int main() { //const long long memory file
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024.0);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof(head));
	scanf("%d%d%s",&n,&m,s);
	for(int i=1; i<=n; i++)scanf("%d",&p[i]);
	for(int i=1; i<n; i++) {
		int a,b;
		scanf("%d%d",&a,&b);
		Add(a,b);
		Add(b,a);
	}
	if(s[0]=='A'&&s[1]=='2')pA2.solve();
	else p40.solve();
	return 0;
}
