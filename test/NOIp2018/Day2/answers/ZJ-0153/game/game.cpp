#include<bits/stdc++.h>
#define R(i,s,t) for(int i=s;i<=t;i++)
using namespace std;
int a[10][10],n,m;
bool out(int x,int y) {
	return x<0||x>=n||y<0||y>=m;
}
bool chk(int num) {
	R(i,0,n-1) R(j,0,m-1) a[i][j]=num&1<<i*m+j;
	R(i,1,m-1) R(j,1,n-1) if(a[j][i-1]<a[j-1][i]) return false;
	R(i,0,n-1) for(int t=0;i+t<=n&&t<=m;t++) {
		int x=i+t,y=t;
		int a1=x+1,b1=y,a2=x,b2=y+1,a3=x+2,b3=y+1,a4=x+1,b4=y+2;
		if(out(a1,b1)||out(a2,b2)||out(a3,b3)||out(a4,b4)) continue;
		if(a[a1][b1]==a[a2][b2]&&a[a3][b3]>a[a4][b4]) return false;
	}
	return true;
}
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	int lim=n*m,ans=0;
	R(i,0,(1<<lim)-1) ans+=chk(i);
	cout<<ans<<endl;
	return 0;
}
