#include<bits/stdc++.h>
#define R(i,s,t) for(int i=s;i<=t;i++)
#define TO vec[u][i]
using namespace std;
const int maxn=5005;
vector<int> vec[maxn];
struct Tree {
	void dfs(int u,int r) {
		cout<<u<<' ';
		for(int i=0;i<vec[u].size();i++) if(TO!=r) dfs(TO,u);
	}
	void work() {
		dfs(1,0); exit(0);
	}
}w1;
struct Circle {
	bool vis[maxn],inc[maxn],ok;
	int p[maxn],d[maxn];
	int v1,v2;
	void dfs1(int u,int r,int dep) {
		p[u]=r; vis[u]=true; d[u]=dep;
		for(int i=0;i<vec[u].size();i++)
			if(TO!=r) {
				if(vis[TO]) v1=u,v2=TO;
				else dfs1(TO,u,dep+1);
			}
	}
	void lca() {
		if(d[v1]<d[v2]) swap(v1,v2);
		while(d[v1]>d[v2]) inc[v1]=true,v1=p[v1];
		while(v1!=v2) {
			inc[v1]=inc[v2]=true;
			v1=p[v1]; v2=p[v2];
		}
		inc[v1]=true;
	}
	void dfs(int u,int r,int id) {
		if(vis[u]) return; vis[u]=true;
		cout<<u<<' ';
		vector<int> vc;
		for(int i=0;i<vec[u].size();i++) if(TO!=r) vc.push_back(TO);
		int s=vc.size();
		if(inc[u]&&ok&&id) {
			if(vc[s-1]>id) {
				ok=false;
				for(int i=0;i<s-1;i++) dfs(vc[i],u,0);
			} else {
				for(int i=0;i<s-1;i++) dfs(vc[i],u,vc[i+1]);
				dfs(vc[s-1],u,id);
			}
		} else {
			for(int i=0;i<s-1;i++) dfs(vc[i],u,vc[i+1]);
			if(s) dfs(vc[s-1],u,id);
		}
	}
	void work() {
		dfs1(1,0,0);
		lca();
		memset(vis,0,sizeof vis);
		ok=true; dfs(1,0,0);
		exit(0);
	}
}w2;
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n,m; cin>>n>>m;
	R(i,1,m) {
		int u,v; cin>>u>>v;
		vec[u].push_back(v);
		vec[v].push_back(u);
	}
	R(i,1,n) sort(vec[i].begin(),vec[i].end());
	if(m==n-1) w1.work();
	else w2.work();
	return 0;
}
