#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <cstring>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
inline void write(int x){
	static int c[23],n;
	n=0;
	if(x<0){
		putchar('-');
		x=-x;
	}
	if(x==0){
		putchar('0');
		return;
	}
	while(x>0){
		c[n++]=x%10;
		x/=10;
	}
	while(n-->0){
		putchar(c[n]+'0');
	}
}
#undef dd
int n,m,x,y,lis[5003],a[5003],b[5003],bt;
bool vis[5003];
vector<int>tc;
vector<int>e[5003];
int wtcnt=0;
void wt(int x){
	write(x);
	wtcnt++;
	if(wtcnt<n)putchar(' ');
}
void dtr(int u,int fa){
	wt(u);
	int ss=e[u].size();
	for(int i=0;i<ss;i++){
		if(e[u][i]!=fa)dtr(e[u][i],u);
	}
}
int itc=0;
bool gtc(int u,int fa){
	if(vis[u]){
		if(itc==0)itc=u;
		return 1;
	}
	vis[u]=1;
	int ss=e[u].size();
	bool hs=0;
	for(int i=0;i<ss;i++){
		if(e[u][i]!=fa){
			hs|=gtc(e[u][i],u);
		}
	}
	if(hs){
		tc.push_back(u);
		if(u==itc)itc=-1;
		else return 1;
	}
	return 0;
}
bool cmp(){
	for(int i=1;i<=n;i++){
		if(a[i]<b[i])return 0;
		if(a[i]>b[i])return 1;
	}
	return 0;
}
int gg=0;
void dfs(int u,int fa){
	if(vis[u])return;
	vis[u]=1;
	b[++bt]=u;
	int x,y;
	int ss=e[u].size();
	for(int i=0;i<ss;i++){
		if(u==gg&&lis[e[u][i]])continue;
		if(e[u][i]!=fa)dfs(e[u][i],u);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=m;i++){
		x=read();
		y=read();
		e[x].push_back(y);
		e[y].push_back(x);
	}
	for(int i=1;i<=n;i++){
		sort(e[i].begin(),e[i].end());
	}
	if(m<n){
		dtr(1,-1);
		return 0;
	}
	gtc(1,-1);
	memset(vis,0,sizeof(vis));
	for(int i=0;i<tc.size();i++){
		lis[tc[i]]=i+1;
	}
	for(int i=1;i<=n;i++)a[i]=n;
	for(int i=0;i<tc.size()-1;i++){
		gg=tc[i];
		bt=0;
		memset(vis,0,sizeof(vis));
		dfs(1,-1);
		if(cmp()){
			memcpy(a,b,sizeof(a));
		}
	}
	for(int i=1;i<=n;i++)wt(a[i]);
	return 0;
}
