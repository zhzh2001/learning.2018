#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <cstring>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
struct st{
	int a,b;
	st(){
		a=b=0;
	}
	st(int _a,int _b){
		a=_a;
		b=_b;
	}
};
const int oo=1<<29;
int n,x,a,y,b,m,p[2003];
char cc[23];
vector<int>e[2003];
st dfs(int u,int fa){
	st ans,xx;
	ans=st(p[u],0);
	for(int i=0;i<e[u].size();i++){
		if(e[u][i]==fa)continue;
		xx=dfs(e[u][i],u);
		ans.a+=min(xx.a,xx.b);
		ans.b+=xx.a;
		if(ans.a>oo)ans.a=oo;
		if(ans.b>oo)ans.b=oo;
	}
	if(u==x&&a==0)ans.a=oo;
	if(u==x&&a==1)ans.b=oo;
	if(u==y&&b==0)ans.a=oo;
	if(u==y&&b==1)ans.b=oo;
	return ans;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();
	m=read();
	scanf("%s",cc);
	for(int i=1;i<=n;i++){
		p[i]=read();
	}
	for(int i=1;i<n;i++){
		x=read();
		y=read();
		e[x].push_back(y);
		e[y].push_back(x);
	}
	for(int i=1;i<=m;i++){
		x=read();
		a=read();
		y=read();
		b=read();
		st ans=dfs(1,-1);
		if(min(ans.a,ans.b)==oo){
			puts("-1");
		}else{
			printf("%d\n",min(ans.a,ans.b));
		}
	}
	return 0;
}
