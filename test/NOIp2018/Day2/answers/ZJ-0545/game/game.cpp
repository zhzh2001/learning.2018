#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <cstring>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,m,f[9],g[9];
const int md=1000000007;
inline void add(int&a,int b){
	a+=b;if(a>=md)a-=md;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();
	m=read();
	if(m<n)swap(n,m);
	f[0]=f[1]=1;
	int k=1;
	int p;
	for(int i=2;i<=n;i++){
		memset(g,0,sizeof(g));
		p=k;
		add(p,k);
		add(k,p);
		for(int j=0;j<=i;j++){
			g[j]=k;
			if(j-1!=-1&&j-1!=0&&j-1!=i-1)add(g[j],f[j-1]);
			if(j!=0&&j!=i-1)add(g[j],f[j]);
		}
		memcpy(f,g,sizeof(f));
	}
	for(int i=1;i<=m-n;i++){
		memset(g,0,sizeof(g));
		p=k;
		add(p,k);
		add(k,p);
		for(int j=0;j<=i;j++){
			g[j]=k;
			if(j-1!=-1&&j-1!=0&&j-1!=i)add(g[j],f[j-1]);
			if(j!=0&&j!=i)add(g[j],f[j]);
		}
		memcpy(f,g,sizeof(f));
	}
	for(int i=n-1;i>=1;i--){
		memset(g,0,sizeof(g));
		p=k;
		add(p,k);
		add(k,p);
		for(int j=0;j<=i;j++){
			g[j]=k;
			if(j+1!=0&&j+1!=i+1)add(g[j],f[j+1]);
			if(j!=0&&j!=i+1)add(g[j],f[j]);
		}
		memcpy(f,g,sizeof(f));
	}
	add(f[0],f[1]);
	printf("%d\n",f[0]);
	return 0;
}
