#include<bits/stdc++.h>
#define N 5005
using namespace std;
int n, m, tmp[N], hj;
int son[N][N], can[N], vis[N], f[N];
string s, ans;
void print(int k, int fa) {
	printf("%d ", k);
	for (int i = 1; i <= tmp[k]; i ++)
		if (son[k][i] != fa) {
			print(son[k][i], k);
		}
	return;
}
void print1(int k, int fa) {
	vis[k] = 1;
	printf("%d ", k);
	for (int i = 1; i <= tmp[k]; i ++)
		if (!vis[son[k][i]]) {
			int tmpp = hj;
			if (son[k][i] > hj) {
				hj = 2147483647;
				print1(tmpp, 1);
			}
			else print1(son[k][i], k);
		}
	return;
}
string change(int t) {
	string ret = "";
	while (t) {
		ret = (char)(t % 10 + '0') + ret;
		t /= 10;
	}
	return ret;
}
void check() {
	if (s < ans) ans = s;
}
void search(int k, int fa, int cnt) {
	if (cnt == n) {check(); return;}
	for (int i = 1; i <= tmp[k]; i ++) {
		if (son[k][i] == fa) {
			search(son[k][i], f[son[k][i]], cnt);
		} else if (!vis[son[k][i]]) {
			string sr = s;
			vis[son[k][i]] = 1;
			f[son[k][i]] = k;
			s = s + change(son[k][i]) + " ";
			search(son[k][i], k, cnt + 1);
			vis[son[k][i]] = 0;
			s = sr;
		}
	}
}
int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	memset(son, 0, sizeof(son));
	memset(vis, 0, sizeof(vis));
	memset(tmp, 0, sizeof(tmp));
	memset(can, 0, sizeof(can));
	scanf("%d %d", &n, &m);
	for (int i = 1; i <= m; i ++) {
		int s, t;
		scanf("%d %d", &s, &t);
		son[s][++ tmp[s]] = t;
		son[t][++ tmp[t]] = s;
	}
	for (int i = 1; i <= n; i ++) sort(son[i] + 1, son[i] + tmp[i] + 1);
	if (m == n - 1) {
		print(1, 0);
		return 0;
	}
	bool bo = 1;
	for (int i = 1; i <= n; i ++) {
		if (tmp[i] != 2) bo = 0;
	}
	if (bo) {
		hj = son[1][2];
		print1(1, 0);
		return 0;
	}
	f[1] = 0; vis[1] = 1;
	ans = "9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 ";
	s = "1 ";
	search(1, 0, 1);
	cout << ans << endl;
	return 0;
}
/*
6 5
1 3
2 3
2 5
3 4
4 6
*/