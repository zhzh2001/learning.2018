#include<bits/stdc++.h>
#define ll long long
const ll Mod = 1000000007;
using namespace std;
ll n, m, ans;
ll poww(ll a, ll b) {
	ll ret = 1, tmp = a;
	while (b) {
		if (b % 2) ret = ret * tmp % Mod;
		tmp = tmp * tmp % Mod;
		b >>= 1;
	}
	return ret;
}
int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d %d", &n, &m);
	if (n > m) swap(n, m);
	if (n == 1) {
		cout << poww(2, m) % Mod << endl;
	}
	if (n == 2) {
		cout << poww(3, m - 1) * 4 % Mod << endl;
	}
	if (n == 3) {
		cout << poww(3, m - 3) * 112 % Mod << endl;
	}
	if (n == 4 && m == 4) {
		cout << "912\n";
	}
	if (n == 4 && m != 4) {
		cout << 896 * poww(3, m - 4) % Mod << endl;
	}
	if (n == 5 && m == 5) {
		cout << "7136\n";
	}
	if (n == 5 && m != 5) {
		cout << 7136 * poww(3, m - 5) % Mod << endl;
	}
	return 0;
}
