 #include<bits/stdc++.h>
#define N 2005
using namespace std;
string s;
int n, m, c[N];
int son[N];
int oz[N][N], zo[N][N], f[N][N][4];
int minn(int a, int b, int c, int d) {
	return min(min(a, b), min(c, d));
}
int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	cin >> n >> m >> s;
	for (int i = 1; i <= n; i ++) cin >> c[i];
	if (s[0] == 'A') {
		for (int i = 1; i < n; i ++) {
			int l, r; cin >> l >> r;
		}
		for (int i = 1; i <= n; i ++) {
			oz[i][i] = c[i]; zo[i][i] = 0;
			oz[i][i + 1] = c[i]; zo[i][i + 1] = c[i + 1];
			for (int j = i + 2; j <= n; j ++) {
				if ((j - i) % 2) {
					oz[i][j] = oz[i][j - 1]; zo[i][j] = zo[i][j - 2] + c[j];
				} else {
					oz[i][j] = oz[i][j - 2] + c[j]; zo[i][j] = zo[i][j - 1];
				}
			}
		}
		for (int i = 1; i <= n; i ++) {
			f[i][i][0] = f[i][i][1] = f[i][i][2] = 0;
			f[i][i][3] = c[i];
		}
		for (int i = 1; i <= n; i ++) {
			for (int j = i; j <= n; j ++) {
				f[i][j][0] = min(f[i][j - 1][1], f[i][j - 1][3]);
				f[i][j][1] = minn(f[i][j - 1][1], f[i][j - 1][3], f[i][j - 1][0], f[i][j - 1][2]) + c[j];
				f[i][j][2] = f[i][j - 1][3];
				f[i][j][3] = min(f[i][j - 1][2], f[i][j - 1][3]) + c[j];
			}
		}
		for (int i = 1; i <= m; i ++) {
			int x, bx, y, by;
			cin >> x >> bx >> y >> by;
			if (x > y) {
				swap(x, y); swap(bx, by);
			}
			if ((x - y == 1 || y - x == 1) && (bx | by) == 0 || (x == y && bx != by)) {
				puts("-1");
			} else if (x == y && bx == 1) {
				cout << minn(f[1][x - 1][0], f[1][x - 1][1], f[1][x - 1][2], f[1][x - 1][3])
				+ c[x] + minn(f[x + 1][n][0], f[x + 1][n][1], f[x + 1][n][2], f[x + 1][n][3]) << endl;
			} else if (x == y && bx == 0) {
				cout << min(f[1][x - 1][1], f[1][x - 1][3]) + min(f[x + 1][n][2], f[x + 1][n][3]) << endl;
			} else {
				int ans = 0;
				if (bx == 0) {
					ans += min(f[1][x - 1][1], f[1][x - 1][3]);
					if (by == 0) ans += min(f[y + 1][n][2], f[y + 1][n][3]) + f[x + 1][y - 1][3];
					else ans += minn(f[y + 1][n][0], f[y + 1][n][1], f[y + 1][n][2], f[y + 1][n][3])
						 + min(f[x + 1][y - 1][2], f[x + 1][y - 1][3]);
				} else {
					ans += minn(f[1][x - 1][1], f[1][x - 1][3], f[1][x - 1][0], f[1][x - 1][2]);
					if (by == 0) ans += min(f[y + 1][n][2], f[y + 1][n][3]) + min(f[x + 1][y - 1][3], f[x + 1][y - 1][1]);
					else ans += minn(f[y + 1][n][0], f[y + 1][n][1], f[y + 1][n][2], f[y + 1][n][3])
						 + minn(f[x + 1][y - 1][2], f[x + 1][y - 1][3], f[x + 1][y - 1][0], f[x + 1][y - 1][2]);
				}
				cout << ans << endl;
			}
		}
	}
}