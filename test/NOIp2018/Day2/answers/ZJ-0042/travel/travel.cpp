#include <bits/stdc++.h>
#define MAXN 5005
#define INF 0X3F3F3F3F
#define rep(i,x,y) for (int i=x;i<=y;i++)
#define per(i,x,y) for (int i=x;i>=y;i--)
using namespace std;

bool vis[MAXN],cir[MAXN],flag;
int n,m,x,y,r,ff,gg,cnt,eNum,num[MAXN],head[MAXN],S[MAXN];
priority_queue< int,vector<int>,greater<int> > Q[MAXN];

struct Edge {
	int to,nxt;
} edge[MAXN<<1];

struct Node {
	int id,fa;
	Node() {}
	Node(int id,int fa):id(id),fa(fa) {}
	bool operator > (const Node &x) const {
		return id>x.id;
	}
};

priority_queue< Node,vector<Node>,greater<Node> > Qc;

void add(int x,int y) {
	edge[++eNum].to=y;
	edge[eNum].nxt=head[x];
	head[x]=eNum;
}

void get_ring(int x,int fa) {
	if (flag) return;
	if (vis[x]) {
		flag=true;
		int l=1;
		gg=0;
		while (S[l]!=x) l++;
		ff=S[l-1];
		rep(i,l,r) cir[S[i]]=true;
		for (int i=head[x];~i;i=edge[i].nxt)
			if (cir[edge[i].to]) gg=max(gg,edge[i].to);
		rep(i,l,r) Q[S[i]].push(gg);
		return;
	}
	vis[x]=true;
	for (int i=head[x];~i;i=edge[i].nxt) {
		int to=edge[i].to;
		if (to==fa) continue;
		S[++r]=x;
		get_ring(to,x);
		r--;
	}
}

void solve(int x,int fa) {
	vis[x]=true;
	num[++cnt]=x;
	{
		for (int i=head[x];~i;i=edge[i].nxt)
			if (edge[i].to!=fa&&!vis[edge[i].to]) Q[x].push(edge[i].to);
			while (!Q[x].empty()) {
				int tmp=Q[x].top();
				Q[x].pop();
				if (!vis[tmp]) {
					if (tmp==gg&&flag) flag=false,solve(gg,ff);
					else solve(tmp,x);
				}
			}
	}
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(head,-1,sizeof(head));
	rep(i,1,m) {
		scanf("%d%d",&x,&y);
		add(x,y),add(y,x);
	}
	get_ring(1,0);
	memset(vis,false,sizeof(vis));
	solve(1,0);
	rep(i,1,n-1) printf("%d ",num[i]);
	printf("%d\n",num[n]);
	return 0;
}

