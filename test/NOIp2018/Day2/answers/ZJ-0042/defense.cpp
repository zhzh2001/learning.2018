#include <bits/stdc++.h>
#define MAXN 100005
#define rep(i,x,y) for (int i=x;i<=y;i++)
#define per(i,x,y) for (int i=x;i>=y;i--)
using namespace std;

struct Edge {
	int to,nxt;
} edge[MAXN<<1];

typedef long long ll;
const ll INF=0X3F3F3F3F3F3F3F3F;

char type[233];
int n,m,x,y,a,b,ax,by,eNum,head[MAXN];
ll p[MAXN],f[MAXN][2];

void add(int x,int y) {
	edge[++eNum].to=y;
	edge[eNum].nxt=head[x];
	head[x]=eNum;
}

void dfs(int x,int fa) {
	f[x][0]=0;
	f[x][1]=p[x];
	for (int i=head[x];~i;i=edge[i].nxt) {
		int to=edge[i].to;
		if (to==fa) continue;
		dfs(to,x);
		if (f[x][0]!=INF) {
			if (f[to][1]==INF) f[x][0]=INF;
			else f[x][0]+=f[to][1];
		}
		if (f[x][1]!=INF) {
			if (min(f[to][0],f[to][1])==INF) f[x][1]=INF;
			else f[x][1]+=min(f[to][0],f[to][1]);
		}
	}
	if (x==a) f[x][!ax]=INF;
	if (x==b) f[x][!by]=INF;
}

int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof(head));
	scanf("%d%d %s",&n,&m,type);
	rep(i,1,n) scanf("%d",&p[i]);
	rep(i,1,n-1) {
		scanf("%d%d",&x,&y);
		add(x,y),add(y,x);
	}
	rep(i,1,m) {
		scanf("%d%d%d%d",&a,&ax,&b,&by);
		f[0][0]=f[0][1]=0;
		dfs(1,0);
		if (min(f[1][0],f[1][1])==INF) puts("-1");
		else printf("%lld\n",min(f[1][0],f[1][1]));
	}
	return 0;
}
