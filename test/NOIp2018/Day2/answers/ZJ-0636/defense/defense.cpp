#include<bits/stdc++.h>
using namespace std;

#define MAXN (200005)
#define LL long long
#define INF ((LL)1<<40)

LL n,m;
struct aa
{
	LL x,y,ls;
}b[MAXN];
LL t[MAXN],cnt;
LL f[MAXN][2];
bool g[MAXN][2];
LL a,xa,bb,xb;
LL p[MAXN];
LL fa[MAXN];
string tp;

void jb(LL x,LL y)
{
	cnt ++;
	b[cnt].x = x;
	b[cnt].y = y;
	b[cnt].ls = t[x];
	t[x] = cnt;
}

void rd()
{
	scanf("%lld%lld",&n,&m);
	cin >> tp;
	for(int i = 1; i <= n; i ++)
		scanf("%lld",&p[i]);
	for(int i = 1; i <= n-1; i ++)
	{
		LL x,y;
		scanf("%lld%lld",&x,&y);
		jb(x,y);
		jb(y,x);
	}
}

LL dp(LL x,LL y)
{
	if(g[x][y]) return f[x][y];
	g[x][y] = 1;
	//cout<<x<<" "<<y<<" "<<a<<":"<<xa<<" "<<bb<<":"<<xb<<"TAT\n";
	if(x == a && y != xa) return f[x][y] = INF;
	if(x == bb && y != xb) return f[x][y] = INF;
//	cout<<"QAQ\n";
	for(int i = t[x]; i != 0; i = b[i].ls)
	{
		LL v = b[i].y;
		if(v == fa[x]) continue;
		if(y == 0) f[x][y] += dp(v,1);
		if(y == 1) f[x][y] += min(dp(v,0),dp(v,1));
	} 
	if(y == 1) f[x][y] += p[x];
	//cout<<x<<" "<<y<<" "<<f[x][y]<<"\n";
	return f[x][y];
}

void ddd(int x)
{
	for(int i = t[x]; i != 0; i = b[i].ls)
	{
		int y = b[i].y;
		if(y != fa[x])
		{
			fa[y] = x;
			ddd(y);
		}
	}
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	
	rd();
	ddd(1);
	for(int i = 1; i <= m; i ++)
	{
		scanf("%lld%lld%lld%lld",&a,&xa,&bb,&xb);
		memset(f,0,sizeof(f));
		memset(g,0,sizeof(g));
		LL rp = min(dp(1,0),dp(1,1));
		if(rp >= INF) printf("-1\n");
		else printf("%lld\n",rp);
	}
	return 0;
}
