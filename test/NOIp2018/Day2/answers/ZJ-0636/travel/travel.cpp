#include<bits/stdc++.h>
using namespace std;

#define MAXN 10005
int n,m,nn;
vector<int>s[MAXN];
vector<bool>ss[MAXN];
int t[MAXN];
int ans[MAXN];
int cnt;
int fa[MAXN],c[MAXN],d[MAXN];

void jb(int x,int y)
{
	s[x].push_back(y);
	s[y].push_back(x);
	ss[x].push_back(1);
	ss[y].push_back(1);
}

void rd()
{
	scanf("%d%d",&n,&m);
	for(int i = 1; i <= m; i ++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		jb(x,y);
	}
	for(int i = 1; i <= n; i ++)
		sort(s[i].begin(),s[i].end());
}

void dfs(int x)
{
	cnt ++;
	t[cnt] = x;
	for(int i = 0; i < s[x].size(); i ++)
	{
		int y = s[x][i];
		if(y != fa[x] && ss[x][i])
		{
		//	cout<<x<<"->"<<y<<" "<<ss[x][i]<<"\n";
			fa[y] = x;
			dfs(y);
		}
	}
}

bool fh = 0;
bool b[MAXN]; 
int lp[MAXN];
int mm;
void ddd(int x,int h)
{
	//cout<<x<<" "<<h<<" "<<b[x]<<"\n";
	if(b[x]) 
	{
	//	cout<<x<<"QAQ\n";
		fh = 1;
		c[h] = x;
		mm = h;
		return;
	}
	b[x] = 1;
	c[h] = x;
	for(int i = 0; i < s[x].size(); i ++)
	{
		int y = s[x][i];
		if(y != lp[x])
		{
			lp[y] = x;
			ddd(y,h+1);
		}
		if(fh) break;
	}	
}

bool bj()
{
	for(int i = 1; i <= n; i ++)
	{
		if(t[i] > ans[i]) return 0;
		if(t[i] < ans[i]) return 1;
	}
	return 0;
}

void fz()
{
	if(bj())
	{
		for(int i = 1; i <= n; i ++) ans[i] = t[i];
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	rd();
	if(m == n-1)
	{
		cnt = 0;
		dfs(1);
		for(int i = 1; i <= n; i ++)
			printf("%d ",t[i]);
	}else{
		memset(b,0,sizeof(b));
		ddd(1,1);
		for(int i = 1; i <= n; i ++)
			ans[i] = n+1;
		//for(int i = 1; i <= mm; i ++)
		//	cout<<c[i]<<"\n";
		for(int i = mm-1; i >= 1; i --)
			if(c[i] == c[mm])
			{
				for(int j = i; j <= mm; j ++)
				{
					nn ++;
					d[nn] = c[j];
				}
				break;
			};
		for(int i = 1; i < nn; i ++)
		{
			for(int k = 0; k < s[d[i]].size(); k ++)
				if(s[d[i]][k] == d[i+1]) ss[d[i]][k] = 0;
			
			for(int k = 0; k < s[d[i+1]].size(); k ++)
				if(s[d[i+1]][k] == d[i]) ss[d[i+1]][k] = 0;
			memset(fa,0,sizeof(fa));
			cnt = 0;
				
			dfs(1);
			fz();
			
			for(int k = 0; k < s[d[i]].size(); k ++)
				if(s[d[i]][k] == d[i+1]) ss[d[i]][k] = 1;
			
			for(int k = 0; k < s[d[i+1]].size(); k ++)
				if(s[d[i+1]][k] == d[i]) ss[d[i+1]][k] = 1;
				
		}	
		for(int i = 1; i <= n; i ++)
			printf("%d ",ans[i]);
	}
	return 0;
}
