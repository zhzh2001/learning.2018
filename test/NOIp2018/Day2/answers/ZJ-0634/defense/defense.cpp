#include<bits/stdc++.h>
using namespace std;
int n,m;
string s;
int a[100010];
int tot,bian[200010],nxt[200010],head[100010];
int d[100010],dp[100010][2],fa[100010];
inline void add(int x,int y){
	tot++,bian[tot]=y,nxt[tot]=head[x],head[x]=tot;
}
void init(int x,int f){
	for(int i=head[x];i;i=nxt[i]){
		if(bian[i]==f)continue;
		fa[bian[i]]=x;
		init(bian[i],x);
	}
}
int dfs(int x,int f,int b){
	if(dp[x][b]!=-1)return dp[x][b];
	if(!b&&d[x]==0)return 1e9;
	int ans=1e9;
	if(b&&d[x]!=1){
	    int ret=0;
	    for(int i=head[x];i;i=nxt[i]){
		    if(bian[i]==f)continue;
		    ret+=dfs(bian[i],x,0);
		    if(ret>=1e9)break;
	    }
	    ans=min(ans,ret);
	}
	if(d[x]!=0){
	    int ret=0;
	    for(int i=head[x];i;i=nxt[i]){
		    if(bian[i]==f)continue;
		    ret+=dfs(bian[i],x,1);
		    if(ret>=1e9)break;
	    }
	    ans=min(ans,ret+a[x]);
	}
	return dp[x][b]=ans;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m>>s;
	for(int i=1;i<=n;++i)scanf("%d",a+i);
	for(int i=1;i<n;++i){
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	init(1,0);
	for(int i=1;i<=m;++i){
		memset(d,-1,sizeof d);
		memset(dp,-1,sizeof dp); 
		int aa,bb,cc,dd;
		scanf("%d%d%d%d",&aa,&bb,&cc,&dd);
		d[aa]=bb,d[cc]=dd;
		if(bb==0&&dd==0){
		    int bo=1;
			for(int i=head[aa];i;i=nxt[i]){
				if(bian[i]==cc)bo=0;
			}
			if(bo==0){
				puts("-1");
				continue;
			}
		}
		printf("%d\n",dfs(1,0,1));
//		for(int j=aa;j!=1;j=fa[j]){
//			dp[j][0]=dp[j][1]=-1;
//		}
//		for(int j=cc;j!=1;j=fa[j]){
//			dp[j][0]=dp[j][1]=-1;
//		}
	}
}
