#include<bits/stdc++.h>
using namespace std;
const int p=1e9+7;
int n,m;
int mp[20][20],ans,bo,now;
void dfs2(int x,int y,int z){
	if(x==n&&y==m){
		if(z>now)bo=0;
		now=min(now,z);
		return;
	}
	if(x!=n)dfs2(x+1,y,z*2+mp[x+1][y]);
	if(y!=m)dfs2(x,y+1,z*2+mp[x][y+1]);
}
void ok(){
	now=1e9;
	dfs2(1,1,0);
}
void dfs(int x,int y){
	if(x==n+1)y++,x=1;
	if(y==m+1){
		bo=1;
		ok();
		ans+=bo;
//		ans%=p;
//		if(bo){
//		for(int i=1;i<=n;++i){
//			for(int j=1;j<=m;++j){
//				printf("%d ",mp[i][j]);
//			}puts("");
//		}puts("");
//			
//		}
		return ;
	}
	if(mp[x+1][y-1])mp[x][y]=1,dfs(x+1,y);
	mp[x][y]=0;
	dfs(x+1,y);
}
long long mpow(long long a,int n){
	long long ret=1;
	while(n){
		if(n&1)(ret*=a)%=p;
		(a*=a)%=p;
		n/=2;
	}
	return ret;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	if(n==1){
		printf("%lld\n",mpow(2,m));
		return 0;
	}
	if(n==2){
		printf("%lld\n",4ll*mpow(3,m-1)%p);
		return 0;
	}
	if(n==3){
		if(m==1)puts("8");
		else if(m==2)puts("44");
		else printf("%lld\n",112ll*mpow(3,m-3)%p);
		return 0;
	}
	if(n==4){
		if(m==1)puts("16");
		else if(m==2)puts("108");
		else if(m==3)puts("336");
		else if(m==4)puts("912");
		else printf("%lld\n",2688ll*mpow(3,m-5)%p);
		return 0;
	}
	if(n==5){
		if(m==1)puts("32");
		else if(m==2)puts("324");
		else if(m==3)puts("1008");
		else if(m==4)puts("2688");
		else if(m==5)puts("7136");
		else printf("%lld",21312ll*mpow(3,m-6));
		return 0;
	}
	for(int i=0;i<=n+1;i++){
		for(int j=0;j<=m+1;++j){
			mp[i][j]=1;
		}
	}
	for(int i=1;i<=n;++i){
		for(int j=1;j<=m;++j){
			mp[i][j]=0;
		}
	}ans=0;
	dfs(1,1);
	cout<<ans<<endl;
}
