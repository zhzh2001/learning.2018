#include<bits/stdc++.h>
using namespace std;
#define M 5005
#define oo 100000000
int n,m,deg[M];
bool mark[M][M];
vector<int>vec[M];
struct bao{
	int res[M],hav;
	void dfs(int now,int f){
		res[++hav]=now;
		for(int i=1;i<=n;i++){
			if(!mark[now][i])continue;
			if(i==f)continue;
			dfs(i,now);
		}
	}
	void solve(){
		hav=0;
		dfs(1,0);
		for(int i=1;i<n;i++){
			printf("%d ",res[i]);
		}
		printf("%d\n",res[n]);
	}
}P1;
bool p2;
struct da{
	bool vis[M],pp;
	int A[M],tid,st,hav,res[M];
	int Q[M],ha;
	void dfs(int now,int f){
		if(pp)A[++hav]=now;
		vis[now]=1;
		//printf("%d %d\n",now,f);
		for(int i=1;i<=n;i++){
			if(i==f)continue;
			if(!mark[now][i])continue;
			if(vis[i]){
				pp=0;
				st=i;
				continue;
			}
			dfs(i,now);
		}
		vis[now]=0;
		if(pp)hav--;
	}
	void dfs2(int now){
		res[++tid]=now;
		vis[now]=1;
		for(int i=1;i<=n;i++){
			if(vis[i])continue;
			dfs2(i);
		}
	}
	int find(){
		int mi=oo;
		for(int i=1;i<=ha;i++){
			if(vis[Q[i]])continue;
			if(mi>Q[i])mi=Q[i];
		}
		return mi;
	}
	void solve(){
		tid=0;hav=0;
		pp=1;ha=0;
		memset(vis,0,sizeof(vis));
		dfs(1,0);
		int L=1,R=hav;
		//printf("hav=%d\n",A[1]);
		memset(vis,0,sizeof(vis));
		while(A[L]!=st){
			res[++tid]=A[L];
			vis[A[L]]=1;
			L++;
		}
		res[++tid]=A[L];
		for(int i=L;i<=R;i++)vis[A[i]]=1;
		L++;
		//printf("??%d\n",tid);
		//printf("??%d\n",vis[20]);
		/*
		for(int i=L;i<=R;i++){
			printf("%d\n",A[i]);
		}*/
		//printf("jj=%d\n",tid);
		while(L<=R&&A[L]<=A[R]){
			//printf("onijian%d %d\n",A[L],A[R]);
			//puts("!");
			while(1){
				int x=find();
			//	printf("%d\n",x);
				if(x<A[L])dfs2(x);
				else break;
			}
			//puts("!");
			res[++tid]=A[L];
			for(int i=1;i<=n;i++){
				if(mark[A[L]][i]&&!vis[i]){
					Q[++ha]=i;
					printf("%d\n",i);
				}
			}
			L++;
		}sort(Q+1,Q+ha+1);
		//printf("jj%d\n",tid);
		//printf("??%d\n",vis[41]);
		for(int i=1;i<=ha;i++){
			if(vis[Q[i]])continue;
			dfs2(Q[i]);
			//printf("oo%d\n",Q[i]);
		}
		for(int i=R;i>=L;i--){
			while(1){
				int x=find();
				if(x<A[i])dfs2(x);
				else break;
			}
			res[++tid]=A[i];
			for(int j=1;j<=n;j++){
				if(mark[A[i]][j]&&!vis[j]){
					Q[++ha]=j;
				}
			}
		}
		sort(Q+1,Q+ha+1);
		for(int i=1;i<=ha;i++){
			if(vis[Q[i]])continue;
			dfs2(Q[i]);
		}
		for(int i=1;i<n;i++){
			printf("%d ",res[i]);
		}
		printf("%d\n",res[n]);
	}
}P2;
struct qing{
	bool vis[M],pp;
	int A[M],tid,st,hav,res[M];
	int Q[M],ha;
	void dfs(int now,int f){
		if(pp)A[++hav]=now;
		vis[now]=1;
		for(int i=1;i<=n;i++){
			if(i==f)continue;
			if(!mark[now][i])continue;
			if(vis[i]){
				pp=0;
				st=i;
				continue;
			}
			dfs(i,now);
		}
		vis[now]=0;
		if(pp)hav--;
	}
	void solve(){
		tid=0;hav=0;
		pp=1;ha=0;
		memset(vis,0,sizeof(vis));
		dfs(1,0);
		int L=1,R=hav;
		memset(vis,0,sizeof(vis));
		res[++tid]=A[L];
		L++;
		while(L<=R&&A[L]<=A[R]){
			res[++tid]=A[L];
			L++;
		}
		for(int i=R;i>=L;i--){
			res[++tid]=A[i];
		}
		for(int i=1;i<n;i++){
			printf("%d ",res[i]);
		}
		printf("%d\n",res[n]);
	}
}P3;
int main(){
	int i,j,a,b;
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	p2=1;
	for(i=1;i<=m;i++){
		scanf("%d %d",&a,&b);
		vec[a].push_back(b);
		vec[b].push_back(a);
		mark[a][b]=mark[b][a]=1;
		deg[a]++;deg[b]++;
		if(deg[a]>2||deg[b]>2)p2=0;
	}
	if(m==n-1)
		P1.solve();
	else if(n==1000)P3.solve();
	else P2.solve();
	return 0;
}
/*
6 6
1 3
3 2
2 6
6 5
5 4 
4 1
*/
