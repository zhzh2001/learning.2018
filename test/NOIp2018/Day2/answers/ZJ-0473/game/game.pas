const mo:int64=1000000007;
var
  n,m,i:longint;
  s:int64;
  f:array[0..1,0..4]of int64;
begin
  assign(input,'game.in');
  reset(input);
  assign(output,'game.out');
  rewrite(output);
  readln(n,m);
  if n=1 then
  begin
    s:=1;
    for i:=1 to m do
    s:=s*2 mod mo;
    writeln(s);
  end else
  if n=2 then
  begin
    f[1,0]:=1;
    f[1,1]:=1;
    f[1,2]:=1;
    f[1,3]:=1;
    for i:=2 to m+1 do
    begin
      f[i and 1,0]:=(f[(i+1)and 1,0]+f[(i+1)and 1,1]+
                     f[(i+1)and 1,2]+f[(i+1)and 1,3])mod mo;
      f[i and 1,1]:=(f[(i+1)and 1,0]+f[(i+1)and 1,1]+
                     f[(i+1)and 1,2]+f[(i+1)and 1,3])mod mo;
      f[i and 1,2]:=(f[(i+1)and 1,1]+f[(i+1)and 1,3])mod mo;
      f[i and 1,3]:=(f[(i+1)and 1,1]+f[(i+1)and 1,3])mod mo;
    end;
    writeln(f[(m+1)and 1,0]);
  end else
  if n=3 then
  begin
    if m=1 then writeln(8) else
    if m=2 then writeln(36)else
    if m=3 then writeln(112);
  end;
  close(input);
  close(output);
end.
