const max:int64=maxlongint*100;
var
  st:string;
  n,m,i,x,y,x1,y1,x2,y2,len:longint;
  first,next,go,fa,num:array[0..1000000]of longint;
  p:array[0..1000000,0..1]of longint;
  f:array[0..1000000,0..1]of int64;
  temp,temp2:int64;
procedure add(x,y:longint);
begin
  inc(len);
  next[len]:=first[x];
  first[x]:=len;
  go[len]:=y;
end;
procedure build(x:longint);
var
  t:longint;
begin
  t:=first[x];
  while t<>0 do
  begin
    if go[t]=fa[x] then
    begin
      t:=next[t];
      continue;
    end;
    fa[go[t]]:=x;
    build(go[t]);
    t:=next[t];
  end;
end;
function dfs(x,y:longint):int64;
var
  t:longint;
  temp,temp2,sum:int64;
begin
  if (x=x1)and(y<>y1) then exit(max);
  if (x=x2)and(y<>y2) then exit(max);
  if p[x,y]=i then
  exit(f[x,y]);
  p[x,y]:=i;
  t:=first[x];
  if y=0 then
  sum:=0 else
  sum:=num[x];
  while t<>0 do
  begin
    if go[t]=fa[x] then
    begin
      t:=next[t];
      continue;
    end;
    temp:=dfs(go[t],1);
    if y=1 then
    begin
      temp2:=dfs(go[t],0);
      if temp2<temp then
      temp:=temp2;
    end;
    sum:=sum+temp;
    t:=next[t];
  end;
  f[x,y]:=sum;
  exit(sum);
end;
begin
  assign(input,'defense.in');
  reset(input);
  assign(output,'defense.out');
  rewrite(output);
  readln(st);
  val(copy(st,1,pos(' ',st)-1),n);
  delete(st,1,pos(' ',st));
  val(copy(st,1,pos(' ',st)-1),m);
  for i:=1 to n do
  read(num[i]);
  for i:=1 to n-1 do
  begin
    readln(x,y);
    add(x,y);
    add(y,x);
  end;
  build(1);
  for i:=1 to m do
  begin
    readln(x1,y1,x2,y2);
    temp:=dfs(1,0);
    temp2:=dfs(1,1);
    if temp2<temp then temp:=temp2;
    if temp>=max then writeln(-1) else
    writeln(temp);
  end;
  close(input);
  close(output);
  close(input);
  close(output);
end.