#include<iostream>
#include<cstdio>
#include<cstring>
#include<map>
#define ll long long
#define P pair<ll,ll>
#define mp make_pair
#define fi first
#define se second
#define INF 0x3f3f3f3f3f3f3f3f
#define N 100100
using namespace std;

ll n,m,bb,num[N],first[N];
char str[5];
struct Bn
{
	ll to,next;
}bn[N<<1];
map<P,bool>mm;

inline void add(ll u,ll v)
{
	bb++;
	bn[bb].to=v;
	bn[bb].next=first[u];
	first[u]=bb;
}

namespace solve1
{
	ll qz,zt,dp[2010][2];
	ll dfs(ll now,ll u,ll last)
	{
		if(now==qz&&u!=zt) return INF;
		if(dp[now][u]!=-1) return dp[now][u];
		ll p,q,res=0;
		for(p=first[now];p!=-1;p=bn[p].next)
		{
			q=bn[p].to;
			if(q==last) continue;
			if(u) res+=min(dfs(q,1,now)+num[q],dfs(q,0,now));
			else res+=dfs(q,1,now)+num[q];
		}
		return dp[now][u]=res;
	}
	void work()
	{
		ll i,j,a,p,b,q;
		while(m--)
		{
			scanf("%lld%lld%lld%lld",&a,&p,&qz,&zt);
			if(mm[mp(a,qz)] && !p && !zt)
			{
				puts("-1");
				continue;
			}
			memset(dp,-1,sizeof(dp));
			printf("%lld\n",dfs(a,p,-1)+p*num[a]);
		}
	}
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(first,-1,sizeof(first));
	ll i,j,p,q;
	cin>>n>>m;
	scanf("%s",str+1);
	for(i=1;i<=n;i++) scanf("%lld",&num[i]);
	for(i=1;i<n;i++)
	{
		scanf("%lld%lld",&p,&q);
		add(p,q),add(q,p);
		mm[mp(p,q)]=mm[mp(q,p)]=1;
	}
	solve1::work();
	return 0;
}
