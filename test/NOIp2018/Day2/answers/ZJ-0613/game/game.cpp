#include<iostream>
#include<cstdio>
#include<vector>
#include<cstring>
#define ll long long
#define N 150
#define M 1000000007
using namespace std;

ll m,n;

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	ll i,j;
	cin>>m>>n;
	if(m==n)
	{
		if(m==1) cout<<2;
		if(m==2) cout<<12;
		if(m==3) cout<<112;
		if(m==4) cout<<912;
		if(m==5) cout<<7136;
		if(m==6) cout<<56768;
		if(m==7) cout<<453504;
		else cout<<3626752;
		return 0;
	}
	if(m>n) swap(m,n);
	if(m<=3&&n<=3)
	{
		if(m==1&&n==1) cout<<2;
		if(m==1&&n==2) cout<<4;
		if(m==1&&n==3) cout<<8;
		if(m==2&&n==2) cout<<12;
		if(m==2&&n==3) cout<<36;
		if(m==3&&n==3) cout<<112;
		return 0;
	}
	if(m==1)
	{
		ll ans=1;
		for(i=1;i<=n;i++)
		{
			ans=(ans*2)%M;
		}
		cout<<ans;
		return 0;
	}
	if(m==2)
	{
		ll ans=4;
		for(i=1;i<=n;i++)
		{
			ans=(ans*3)%M;
		}
		cout<<ans;
		return 0;
	}
	if(m==3)
	{
		ll ans=112;
		for(i=4;i<=n;i++)
		{
			ans=(ans*3%M);
		}
		cout<<ans;
		return 0;
	}
	if(m==4)
	{
		ll ans=2688;
		for(i=6;i<=n;i++) ans=(ans*3)%M;
		cout<<ans;
		return 0;
	}
	if(m==5)
	{
		ll ans=21312;
		for(i=7;i<=n;i++)
		{
			ans=(ans*3)%M;
		}
		cout<<ans;
		return 0;
	}
	if(m==6)
	{
		ll ans=170112;
		for(i=8;i<=n;i++)
		{
			ans=(ans*3)%M;
		}
		cout<<ans;
		return 0;
	}
	if(m==7)
	{
		ll ans=1360128;
		for(i=9;i<=n;i++)
		{
			ans=(ans*3)%M;
		}
		cout<<ans;
		return 0;
	}
	puts("0");
	return 0;
}
