#include<iostream>
#include<cstdio>
#include<vector>
#include<algorithm>
#include<cstring>
#define P pair<int,int>
#define mp make_pair
#define fi first
#define se second
#define N 5010
using namespace std;

int n,m,sta[N],an[N],aa,ans[N],a[N],b[N],top;
bool vis[N],find;
P gg;
vector<int>to[N],huan;

namespace solve1
{
	int ans[N],aa;
	void dfs(int now,int last)
	{
		ans[++aa]=now;
		int i,j,t;
		for(i=0;i<to[now].size();i++)
		{
			t=to[now][i];
			if(t==last) continue;
			dfs(t,now);
		}
	}
	void work()
	{
		int i,j,p,q;
		for(i=1;i<=m;i++)
		{
			scanf("%d%d",&p,&q);
			to[p].push_back(q);
			to[q].push_back(p);
		}
		for(i=1;i<=n;i++) sort(to[i].begin(),to[i].end());
		dfs(1,-1);
		for(i=1;i<=n;i++) printf("%d ",ans[i]);
	}
}

void dfs(int now)
{
	int i,j,t;
	an[++aa]=now;
	vis[now]=1;
	for(i=0;i<to[now].size();i++)
	{
		t=to[now][i];
		if(vis[t] || mp(now,t)==gg || mp(t,now)==gg) continue;
		dfs(t);
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int i,j,p,q;
	cin>>n>>m;
	if(m==n-1)
	{
		solve1::work();
		return 0;
	}
	for(i=1;i<=m;i++)
	{
		scanf("%d%d",&p,&q);
		to[p].push_back(q);
		to[q].push_back(p);
		a[i]=p,b[i]=q;
	}
	for(i=1;i<=n;i++) sort(to[i].begin(),to[i].end());
	ans[1]=2;
	for(i=1;i<=m;i++)
	{
		memset(vis,0,sizeof(vis));
		gg=mp(a[i],b[i]);
		aa=0;
		dfs(1);
		if(aa<n) continue;
		for(j=1;j<=n;j++)
		{
			if(ans[j]!=an[j]) break;
		}
		if(ans[j]>an[j])
		{
			for(j=1;j<=n;j++) ans[j]=an[j];
		}
	}
	for(i=1;i<=n;i++) printf("%d ",ans[i]);
}
