#include<cstdio>
#include<queue>
#include<algorithm>
using namespace std;
int s,n,m,in[5012];
bool f[5012][5012];
int fa[5012];
int b[5012];
priority_queue<pair<int,int> > q;
inline void dfs(int t,int father)
{
	s++;
	b[t]=1;
	printf("%d%c",t,(s==n)?'\n':' ');
	for (int i=1; i<=n; i++)
	if (f[t][i]&&i!=father&&!b[i]) dfs(i,t);
}
inline int find(int x)
{
	if (fa[x]!=x) fa[x]=find(fa[x]);
	return fa[x];
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==m+1)
	{
		int x,y;
		for (int i=1; i<=m; i++)
		{
			scanf("%d%d",&x,&y);
			f[x][y]=f[y][x]=1;
		}
		dfs(1,0);
	}
	else
	{
		int x,y;
		bool flag=1;
		//for (int i=1; i<=n; i++) fa[i]=i;
		for (int i=1; i<=m; i++)
		{
			scanf("%d%d",&x,&y);
			//if (find(x)==y) color(x,y);
			//else fa[x]=y;
			in[x]++,in[y]++;
			if (in[x]>2) flag=0;
			f[x][y]=f[y][x]=1;
		}
		if (flag)
		{
			putchar('1');
			b[1]=-1;
			for (int i=1; i<=n; i++)
			if (f[1][i]) q.push(make_pair(-i,i)),b[i]=-1;
			while (!q.empty())
			{
				int u=-q.top().first;q.pop();
				if  (b[u]==1) continue;
				b[u]=1;
				printf(" %d",u);
				for (int i=1; i<=n; i++)
				if (f[u][i]&&b[i]==0) q.push(make_pair(-i,i));
			}
		}
		else
		{
			dfs(1,0);
		}
	}
	return 0;
}
