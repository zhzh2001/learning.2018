#include<cstdio>
#include<algorithm>
using namespace std;
int a,b,s,n,m,c[112],ans;
bool p[112],f[112][112];
inline bool check()
{
	for (int i=1; i<=n; i++)
		for (int j=1; j<=n; j++)
		if (f[i][j]&&(!p[i])&&(!p[j])) return 0;
	return 1;
}
inline void dfs(int t,int s)
{
	if (t>n) {if (check()) ans=min(ans,s); return;}
	if (t==a||t==b) dfs(t+1,s);
	else{
	p[t]=1;
	dfs(t+1,s+c[t]);
	p[t]=0;
	dfs(t+1,s);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char st[12];
	scanf("%d%d%s",&n,&m,st);
	for (int i=1; i<=n; i++) scanf("%d",&c[i]);
	if (n<=10)
	{
	int x,y;
	for (int i=1; i<n; i++)
	{
		scanf("%d%d",&x,&y);
		f[x][y]=f[y][x]=1;
	}
	while (m--)
	{
		int a1,b1;
		for (int i=1; i<=n; i++) p[i]=0;
		ans=1000000012;
		scanf("%d%d%d%d",&a,&a1,&b,&b1);
		if ((!a1)&&(!b1))
		{
			if (f[a][b]) {puts("-1");continue;}
		}
		int beg=0;
		if (a1) beg=c[a],p[a]=1;
		if (b1) beg+=c[b],p[b]=1;
		dfs(1,beg);
		printf("%d\n",ans);
	}
	}
	else
	{
		int x,y,a1,b1;
		for (int i=1; i<=n; i++) scanf("%d%d",&x,&y);
		scanf("%d%d%d%d",&a,&a1,&b,&b1);
		for (int i=2; i<=n; i++)
		{
			if (i==a||i==b) continue;
			if ((i-1)==a) p[i]=!a1;
			if ((i-1)==b) p[i]=!b1;
			if (!p[i-1]) p[i]=1;
			else if (c[i+1]>c[i]) p[i]=1;
		}
		for (int i=1; i<=n; i++)
		if (p[i]) ans+=c[i];
		printf("%d\n",ans);
	}
	return 0;
} 
