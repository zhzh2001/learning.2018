#include<bits/stdc++.h>
using namespace std;
#define ll long long
const ll INF = 0x7fffffff;//
const ll N   = 100009;
ll Read()
{
	ll ret=0,nag=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') nag=-1;c=getchar();}
	while(c<='9'&&c>='0') ret=ret*10+c-'0',c=getchar();
	return ret*nag;
}
void Print(ll x)
{
	if(x<0) putchar('-'),x=-x;
	if(x>9) Print(x/10);
	putchar(x%10+'0');
}
ll n,Q;
ll p[N],f[N][3];
ll head[N],to[N*2],nxt[N*2],cnt,A,X,B,Y;
inline void Add_edge(ll x,ll y)
{
	cnt++;
	to[cnt]=y,nxt[cnt]=head[x];
	head[x]=cnt;
}
string yyc;
void dfs(ll x,ll fa)
{
	//cout<<x<<" "<<fa<<endl;
	ll flg=0;vector<ll>g;
	for(ll k=head[x];k;k=nxt[k])
	{
		ll v=to[k];if(v==fa) continue;
		dfs(v,x);flg=1;g.push_back(v);
		if(!((x==A&&X==1) || (x==B&&Y==1)))	f[x][0]+=f[v][2];
		else f[x][0]=INF;
		if(!((x==A&&X==0) || (x==B&&Y==0))) f[x][1]+=min(f[v][0],min(f[v][1],f[v][2]));//,cout<<"*"<<x<<" "<<v<<endl;
		else f[x][1]=INF;
	}
	if(!flg) 
	{
		if(!((x==A&&X==1) || (x==B&&Y==1)))	f[x][0]=0,f[x][2]=INF;
		else f[x][0]=f[x][2]=INF;
		if(!((x==A&&X==0) || (x==B&&Y==0))) f[x][1]=p[x];
		else f[x][1]=INF;
	}
	else 
	{
		if(!((x==A&&X==1) || (x==B&&Y==1)))
		{
			for(ll i=0;i<g.size();i++)
			{
				ll now=f[g[i]][1];
				for(ll j=0;j<g.size();j++)
				{
					if(i!=j) now+=min(f[g[j]][1],f[g[j]][2]);
				}
				f[x][2]=min(f[x][2],now);
			}
		}
		else f[x][2]=INF;
		if(!((x==A&&X==0) || (x==B&&Y==0))) f[x][1]+=p[x];
		else f[x][1]=INF;
	}
	
}
int main ()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=Read(),Q=Read();cin>>yyc;
	for(ll i=1;i<=n;i++) p[i]=Read();
	for(ll i=1;i<n;i++)
	{
		ll x=Read(),y=Read();
		Add_edge(x,y),Add_edge(y,x);
	} 
	if(yyc[0]=='A')
	{
		
		while(Q--)
		{
			A=Read(),X=Read(),B=Read(),Y=Read();
			if(abs(A-B)==1&&(X==0)&&(Y==0)) puts("-1");
			else
			{
				for(register int i=0;i<=n;i++) f[i][0]=f[i][1]=f[i][2]=INF;
				f[0][0]=f[0][2]=0;
				for(register int i=1;i<=n;i++)
				{
					if(!((i==A&&X==1) || (i==B&&Y==1)))
					{
						f[i][0]=f[i-1][2];
						f[i][2]=f[i-1][1];
					}
					if(!((i==A&&X==0) || (i==B&&Y==0)))
						f[i][1]=min(min(f[i-1][0],f[i-1][1]),f[i-1][2])+p[i];
				}
				Print(min(f[n][1],f[n][2])),puts("");
			}
		}
	}
	else
	{
		while(Q--)
		{
			A=Read(),X=Read(),B=Read(),Y=Read();
			for(ll i=0;i<=n;i++) f[i][0]=f[i][1]=0,f[i][2]=INF;
			dfs(1,0);
			ll ans=min(f[1][1],f[1][2]);
			if(ans==INF) puts("-1");
			else Print(min(f[1][1],f[1][2])),puts("");
			//for(int i=1;i<=n;i++)
			//	cout<<f[i][0]<<" "<<f[i][1]<<" "<<f[i][2]<<endl;
		}
	}
	return 0;
}
/*
5 4 A1
9 3 2 5 1
1 2
2 3
3 4
4 5
0 0 0 0
0 0 0 0
2 0 3 0
2 0 4 0

5 3 C3
2 4 1 3 9
1 5
5 2
5 3
3 4
1 0 3 0
2 1 3 1
1 0 5 0
*/
