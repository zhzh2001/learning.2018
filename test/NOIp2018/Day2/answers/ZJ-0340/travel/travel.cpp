#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int INF = 0x7fffffff;
const int N   = 5009;
int Read()
{
	int ret=0,nag=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') nag=-1;c=getchar();}
	while(c<='9'&&c>='0') ret=ret*10+c-'0',c=getchar();
	return ret*nag;
}
void Print(int x)
{
	if(x<0) putchar('-'),x=-x;
	if(x>9) Print(x/10);
	putchar(x%10+'0');
}
int n,m,ans[N],len;
int tmp[N];
vector<int>G[N];
void dfs(int x,int fa)
{
	ans[++len]=x;
	for(int i=0;i<G[x].size();i++)
	{
		int v=G[x][i];
		if(v==fa) continue;
		dfs(v,x);
	}
}
int main ()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=Read(),m=Read();
	for(int i=1;i<=m;i++)
	{
		int x=Read(),y=Read();
		G[x].push_back(y),G[y].push_back(x);
	}
	for(int i=1;i<=n;i++)	
	{
		memset(tmp,0,sizeof(tmp));
		for(int j=0;j<G[i].size();j++) tmp[j]=G[i][j];
		sort(tmp,tmp+G[i].size());
		for(int j=0;j<G[i].size();j++) G[i][j]=tmp[j];
	}
	/*for(int i=1;i<=n;i++)
	{
		for(int j=0;j<G[i].size();j++)
			cout<<G[i][j]<<" ";
		cout<<endl;
	}*/
	//if(m==n-1)
	//{
		dfs(1,0);
		for(int i=1;i<=len;i++) Print(ans[i]),putchar(' ');
	//}
	return 0;
}
/*
6 5
1 3
2 3
2 5
3 4
4 6

11 10
1 4
4 6
6 2
2 5
5 10
5 11
2 7
7 8
1 3
3 9
*/
