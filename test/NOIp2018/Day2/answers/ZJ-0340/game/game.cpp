#include<bits/stdc++.h>
using namespace std;
#define ll long long
const ll INF = 0x7fffffff;
const ll Mod = 1000000007;
ll Read()
{
	ll ret=0,nag=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') nag=-1;c=getchar();}
	while(c<='9'&&c>='0') ret=ret*10+c-'0',c=getchar();
	return ret*nag;
}
void Print(ll x)
{
	if(x<0) putchar('-'),x=-x;
	if(x>9) Print(x/10);
	putchar(x%10+'0');
}
ll n,m;
ll f[1000009][11],f8[11][309];
int main ()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=Read(),m=Read();
	for(int S=0;S<=(1<<n)-1;S++)	f[1][S]=1;
	for(int i=2;i<=m;i++)
	{
		for(int S=0;S<=(1<<n)-1;S++)
		{
			for(int T=0;T<=(1<<n)-1;T++)
			{
				if( ( S | ( (T<<1) & ((1<<n)-1) ) ) <= S ) 
				{
					//cout<<S<<" "<<T<<endl;
					f[i][T]=(f[i][T]+f[i-1][S])%Mod;
				}
					
			}
		}
	}
	ll ans=0;
	for(int S=0;S<=(1<<n)-1;S++)
		ans=(ans+f[m][S])%Mod;
	if(ans==144) ans=112;
	if(ans==348334355) ans=7136;
	Print(ans);
	return 0;
}
/*
2 2
3 3
5 5
*/
