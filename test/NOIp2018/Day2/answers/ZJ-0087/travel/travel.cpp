#include<bits/stdc++.h>
using namespace std;
const int MAXN=5005;
struct Edge{
	int v,Next;
}e[MAXN*2];
int Head[MAXN],tot,n,m;
bool f[MAXN][MAXN],vis[MAXN];
priority_queue<int> q;
void add(int u,int v)
{
	e[++tot]=Edge{v,Head[u]};
	Head[u]=tot;
}
void dfs(int u)
{
	int tot=0;
	while (tot!=n)
	{
		tot++;
		int x=q.top();
		printf("%d ",n-x+1);
		q.pop();
		for (int i=Head[x];i!=-1;i=e[i].Next)
		if (vis[e[i].v]==false)
		{
			q.push(e[i].v);
			vis[e[i].v]=true;
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(Head,-1,sizeof(Head));
	for (int i=1;i<=m;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		u=n-u+1; v=n-v+1;
		add(u,v); add(v,u);
	}
	memset(vis,false,sizeof(vis));
	vis[n]=true;q.push(n);
	dfs(n);
	return 0;
}
