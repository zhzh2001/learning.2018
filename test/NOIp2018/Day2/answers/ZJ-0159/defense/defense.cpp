#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <vector>
#include <climits>
using namespace std ;

#define pb push_back
#define SZ(x) ((int) (x.size()))

const int N = 100010 ;
const int iinf = 2147483647 ;

vector <int> e[N] ;
int n, m, ans, cnt ;
char op[10] ; 
int col[N], p[N], a[2], b[2], A[N], dp[N][2] ;

bool ok() {
	for (int i = 1; i <= n; i++) 
	if (!col[i]) {
		for (int j = 0; j < SZ(e[i]); j++) {
			int to = e[i][j] ;
			if (!col[to]) return false ;
		}
	}
	return true ;
}

int calc() {
	int res = 0 ;
	for (int i = 1; i <= n; i++) res += col[i] * p[i] ;
	return res ;
}

void dfs(int k) {
	if (k == n + 1) {
		if (ok()) ans = min(ans, calc()) ;
		return ;
	}
	if (k == a[0]) col[k] = b[0], dfs(k + 1) ;
	else if (k == a[1]) col[k] = b[1], dfs(k + 1) ;
	else {
		for (int i = 0; i <= 1; i++) {
			col[k] = i ;
			dfs(k + 1) ;
		}
	}
}

void subtask1() {
	while (m--) {
		ans = iinf ;
		scanf("%d%d%d%d", &a[0], &b[0], &a[1], &b[1]) ;
		dfs(1) ;
		if (ans == iinf) puts("-1") ;
		else printf("%d\n", ans) ;
	}
	exit(0) ;
}

void subtask2() {
	int now, fa = -1 ;
	for (int i = 1; i <= n; i++)
	if (SZ(e[i]) == 1) {
		now = i ;
		break ;
	} 
	while (1) {
		A[++cnt] = now ;
		int tmp = now ;
		if (e[now][0] == fa) {
			if (SZ(e[now]) == 1) break ;
			now = e[now][1] ;
		}
		else now = e[now][0] ;
		fa = tmp ;
	}
//	for (int i = 1; i <= n; i++) cout << A[i] << " " ;
//	puts("") ;
	for (int i = 1; i <= n; i++) col[A[i]] = i ; // new id
//	for (int i = 1; i <= n; i++) cout << col[i] << " " ;
//	puts("") ;
	while (m--) {
		ans = iinf ;
		scanf("%d%d%d%d", &a[0], &b[0], &a[1], &b[1]) ;
		memset(dp, 0, sizeof(dp)) ;
		for (int i = 0; i <= 1; i++) {
			dp[col[a[i]]][b[i]] = (b[i] ? p[a[i]] : 0) ;
			dp[col[a[i]]][b[i] ^ 1] = iinf ;
		}
		if (abs(col[a[0]] - col[a[1]]) == 1 && !b[0] && !b[1]) {
			puts("-1") ;
			continue ;
		}
		for (int i = 1; i <= n; i++) {
			dp[i][0] = min(iinf*1ll, 1ll*dp[i][0] + dp[i - 1][1]) ;
			dp[i][1] = min(iinf*1ll, 1ll*dp[i][1] + min(dp[i - 1][0], dp[i - 1][1]) + p[col[i]]);
		}
//		for (int i = 0; i <= 1; i++) {
//			for (int j = 1; j <= n; j++) cout << dp[j][i] << " " ;
//			puts("") ;
//		}
//		int ans = min(dp[n][0], dp[n][1]) ;
//		for (int i = 1; i <= n; i++) {
//			dp[i][0] = min(iinf*1ll, 1ll*dp[i][0] + dp[i + 1][1]) ;
//			dp[i][1] = min(iinf*1ll, 1ll*dp[i][1] + min(dp[i + 1][0], dp[i + 1][1]) + p[col[i]]) ;
//		}
//		for (int i = 0; i <= 1; i++) {
//			for (int j = 1; j <= n; j++) cout << dp[j][i] << " " ;
//			puts("") ;
//		}
		ans = min(dp[n][0], dp[n][1]) ;
		printf("%d\n", ans) ;
	}
	exit(0) ;
}

int main() {
	freopen("defense.in", "r", stdin) ;
	freopen("defense.out", "w", stdout) ;
	scanf("%d%d%s", &n, &m, op) ;
	for (int i = 1; i <= n; i++) scanf("%d", &p[i]) ;
	for (int i = 1; i < n; i++) {
		int a, b ;
		scanf("%d%d", &a, &b) ;
		e[a].pb(b) ;
		e[b].pb(a) ;
	}
	
	if (n <= 10 && m <= 10) subtask1() ; // dfs
	if (op[0] = 'A') subtask2() ;// lists
	for (int i = 1; i <= m; i++) puts("-1") ;
}
