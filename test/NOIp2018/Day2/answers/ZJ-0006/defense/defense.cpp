//minamoto
#include<bits/stdc++.h>
#define IT vector<int>::iterator
#define ll long long
using namespace std;
const int N=1e5+5;const ll inf=1e11;
int head[N],Next[N<<1],ver[N<<1],tot=1;
inline void add(int u,int v){ver[++tot]=v,Next[tot]=head[u],head[u]=tot;}
int n,m,p[N];char s[5];vector<int>mp[N];IT it;
namespace solve1{
	ll f[N][2];int st[N];
	void dfs(int u,int fa){
		if(st[u]!=-1){
			f[u][st[u]]=st[u]?p[u]:0,f[u][st[u]^1]=inf;
		}else f[u][0]=0,f[u][1]=p[u];
		for(int i=head[u];i;i=Next[i])
		if(ver[i]!=fa){
			int v=ver[i];dfs(v,u);
			f[u][1]+=min(f[v][0],f[v][1]);
			f[u][0]+=f[v][1];
		}
	}
	void MAIN(){
		memset(st,-1,sizeof(st));
		for(int i=1;i<=n;++i)sort(mp[i].begin(),mp[i].end());
		while(m--){
			int u,x,v,y;scanf("%d%d%d%d",&u,&x,&v,&y);
			if(!x&&!y){
				it=lower_bound(mp[u].begin(),mp[u].end(),v);
				if(it!=mp[u].end()&&(*it)==v){
					puts("-1");continue;
				}
			}
			st[u]=x,st[v]=y;
			dfs(1,0);
			st[u]=-1,st[v]=-1;
			printf("%lld\n",min(f[1][0],f[1][1]));
		}
	}
}
namespace solve2{
	ll f[N][2][2],ans;
	void MAIN(){
		for(int i=2;i<=n;++i){
			f[i][0][0]=0,f[i][0][1]=p[i];
			f[i][0][0]+=f[i-1][0][1];
			f[i][0][1]+=min(f[i-1][0][0],f[i-1][0][1]);
		}
		for(int i=n;i>=2;--i){
			f[i][1][0]=0,f[i][1][1]=p[i];
			f[i][1][0]+=f[i+1][1][1];
			f[i][1][1]+=min(f[i+1][1][0],f[i+1][1][1]);
		}
		while(m--){
			int u,x,v,y;scanf("%d%d%d%d",&u,&x,&v,&y);
			if(!x&&!y&&v==2){puts("-1");continue;}
			ans=p[1];
			ans+=f[v][0][y]+f[v][1][y]-(y?p[v]:0);
			printf("%lld\n",ans);
		}
	}
}
namespace solve3{
	ll f[N][2][2],ans;
	void MAIN(){
		for(int i=1;i<=n;++i){
			f[i][0][0]=0,f[i][0][1]=p[i];
			f[i][0][0]+=f[i-1][0][1];
			f[i][0][1]+=min(f[i-1][0][0],f[i-1][0][1]);
		}
		for(int i=n;i;--i){
			f[i][1][0]=0,f[i][1][1]=p[i];
			f[i][1][0]+=f[i+1][1][1];
			f[i][1][1]+=min(f[i+1][1][0],f[i+1][1][1]);
		}
		while(m--){
			int u,x,v,y;scanf("%d%d%d%d",&u,&x,&v,&y);
			if(!x&&!y){puts("-1");continue;}
			if(u>v)swap(u,v);ans=0;
			ans=f[u][0][x]+f[v][1][y];
			printf("%lld\n",ans);
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for(int i=1;i<=n;++i)scanf("%d",&p[i]);
	for(int i=1,u,v;i<n;++i)scanf("%d%d",&u,&v),add(u,v),add(v,u),mp[u].push_back(v),mp[v].push_back(u);
	if(n<=2000)solve1::MAIN();
	else if(s[1]=='A'){
		if(s[2]=='1')solve2::MAIN();
		else if(s[2]=='2')solve3::MAIN();
		else solve1::MAIN();
	}else solve1::MAIN();
	return 0;
}
