//minamoto
#include<bits/stdc++.h>
#define mem(a) (memset(a,0,sizeof(a)))
using namespace std;
const int N=5055;
int to[N][N],n,m;
namespace solve1{
	int vis[N];
	void dfs(int u){
		vis[u]=1,printf("%d ",u);
		sort(to[u]+1,to[u]+1+to[u][0]);
		for(int i=1;i<=to[u][0];++i)
		if(!vis[to[u][i]])dfs(to[u][i]);
	}
	void MAIN(){
		dfs(1);
	}
}
namespace solve2{
	int vis[N],ans[N],st[N],cir[N],Pre[N],tot,m,U,V;
	bool check(){
		if(tot!=n)return false;
		for(int i=1;i<=n;++i)if(st[i]!=ans[i])return st[i]<ans[i];
		return false;
	}
	bool find(int u,int fa){
		vis[u]=1,st[++tot]=u,Pre[u]=fa;
		for(int i=1;i<=to[u][0];++i)
		if(to[u][i]!=fa){
			if(!vis[to[u][i]]){
				if(find(to[u][i],u))return true;
			}
			else{
				int v=to[u][i];
				while(u!=v)cir[++m]=u,u=Pre[u];
				cir[++m]=v;return true;
			}
		}
		--tot;return false;
	}
	void dfs(int u){
		vis[u]=1,st[++tot]=u;
		for(int i=1;i<=to[u][0];++i)
		if((u!=U||to[u][i]!=V)&&(u!=V||to[u][i]!=U)){
			if(!vis[to[u][i]])dfs(to[u][i]);
		}
	}
	void MAIN(){
		for(int i=1;i<=n;++i)ans[i]=n+1;
		for(int u=1;u<=n;++u)sort(to[u]+1,to[u]+to[u][0]+1);
		mem(vis),find(1,0);
		cir[m+1]=cir[1];
		for(int i=1;i<=m;++i){
			mem(vis),tot=0,U=cir[i],V=cir[i+1],dfs(1);
			if(check()){
				for(int i=1;i<=n;++i)ans[i]=st[i];
			}
		}
		for(int i=1;i<=n;++i)printf("%d ",ans[i]);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1,u,v;i<=m;++i)
	scanf("%d%d",&u,&v),to[u][++to[u][0]]=v,to[v][++to[v][0]]=u;
	m==n-1?solve1::MAIN():solve2::MAIN();
	return 0;
}
