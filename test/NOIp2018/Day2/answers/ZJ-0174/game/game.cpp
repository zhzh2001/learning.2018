#include<iostream>
#include<cstring>
#include<cstdio>
#include<vector>
#include<bitset>
using namespace std;
const int gsd = 1e9 + 7;
#define ll long long
int n, m;
int a[5][5];
ll qpow(int x, int y){
	ll s = 1, base = x;
	while(y){
		if(y & 1) s *= base, s %= gsd;
		base *= base, base %= gsd;
		y >>= 1;
	}
	return s;
}
ll solve(int x){
	if(x == 0) return 1;
	return (qpow(2, 2 * x - 2) + 2 * solve(x - 1)) % gsd;
}
int main(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if(n == 2){
		printf("%lld\n", 4 * solve(m - 1));
	}else{
		if(n == 0 || m == 0) puts("0");
		a[1][1] = 1;
		a[1][2] = a[2][1] = 4;
		a[1][3] = a[3][1] = 8;
		a[2][2] = 12;
		a[2][3] = a[3][2] = 40;
		a[3][3] = 112;
		printf("%d\n", a[n][m]);
	}
	return 0;
}
