#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdio>
#include<vector>
using namespace std;
const int N = 10010;
typedef pair<int, int> P;
#define ver first
#define id second
vector<P> E[N];
int n, m, tot, now, stop[N], ans[N];
void add(int x, int y){ E[x].push_back(P(y, ++now)); E[y].push_back(P(x, now));}
bool vis[N];
void dfs1(int x){
	ans[++tot] = x; vis[x] = 1;
	for(int i = 0; i < E[x].size(); i++){
		int v = E[x][i].ver;
		if(vis[v] || stop[E[x][i].id]) continue;
		dfs1(v);
	}
}
int sta[N], tp, cnt, dfn[N];
bool cur[N];
void fcur(int x, int pre){
	dfn[x] = ++cnt; sta[++tp] = x;
	for(int i = 0; i < E[x].size(); i++){
		int v = E[x][i].ver;
		if(E[x][i].id == pre) continue;
		if(dfn[v]){
			do{
				cur[sta[tp]] = 1; tp--;
			}while(dfn[sta[tp]] >= dfn[v] && tp);
		}
		else fcur(v, E[x][i].id);
	}
	tp--;
}
bool flg;
vector<int> vec;
void dfs2(int x){
	vis[x] = 1;
	for(int i = 0; i < E[x].size(); i++){
		int v = E[x][i].ver;
		if(cur[x] && cur[v]) vec.push_back(E[x][i].id);
		if(!vis[v]) dfs2(v);
	}
}
int realans[N];
bool cmp(int *a, int *b){
	for(int i = 1; i <= tot; i++)
	  if(a[i] != b[i]) return a[i] > b[i];
	return 0;
}
int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m); int x, y;
	for(int i = 1; i <= m; i++){
		scanf("%d%d", &x, &y);
		add(x, y);
	}
//	for(int i = 0; i < E[35].size(); i++)
//	  printf("%d ", E[35][i].ver);
//	 puts("");
	for(int i = 1; i <= n; i++)
	  sort(E[i].begin(), E[i].end());
	memset(realans, 0x3f, sizeof(realans));
	if(m == n - 1){
		dfs1(1);
		for(int i  = 1; i <= tot; i++)
		  realans[i] = ans[i];
	}
	else{
		fcur(1, 0); 
		dfs2(1);
		for(int i = 0; i < vec.size(); i++){
			stop[vec[i]] = 1;
			memset(vis, 0, sizeof(vis));
			tot = 0; dfs1(1);
			stop[vec[i]] = 0;
			if(cmp(realans, ans))
			  for(int i = 1; i <= tot; i++)
				realans[i] = ans[i];
		}
	}
	for(int i = 1; i <= tot; i++)
	  printf("%d ", realans[i]);
	return 0;
}
