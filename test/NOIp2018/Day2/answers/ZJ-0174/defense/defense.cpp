#include<iostream>
#include<cstring>
#include<cstdio>
using namespace std;
const int N = 2010;
int head[N], now;
struct edges{ int to, next; }edge[N << 1];
void add(int u, int v){ edge[++now] = (edges){v, head[u]}; head[u] = now;}
int n, m, loc[N][2], p[N];
long long dp[N][2];
char sb[5];
struct Query{ int x, y, a, b;}q[N];
void dfs(int x, int fa){
	if(!loc[x][0]) dp[x][0] = 0;
	if(!loc[x][1]) dp[x][1] = p[x];
	for(int i = head[x]; i; i = edge[i].next){
		int v = edge[i].to;
		if(v == fa) continue;
		dfs(v, x);
		if(!loc[x][0]) dp[x][0] += dp[v][1];
		if(!loc[x][1]) dp[x][1] += min(dp[v][1], dp[v][0]);
	}
}
int main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d%s", &n, &m, sb); int x, y;
	for(int i = 1; i <= n; i++)  scanf("%d", &p[i]);
	for(int i = 1; i < n; i++){
		scanf("%d%d", &x, &y);
		add(x, y); add(y, x);
	}
	int a, b;
	for(int i = 1; i <= m; i++){
		scanf("%d%d%d%d", &q[i].a, &q[i].x, &q[i].b, &q[i].y);
		for(int j = 1; j <= n; j++)
		  dp[j][0] = dp[j][1] = 2e9;
		memset(loc, 0, sizeof(loc));
		loc[q[i].a][q[i].x ^ 1] = 1;
		loc[q[i].b][q[i].y ^ 1] = 1;
		dfs(1, 0);
		if(min(dp[1][0], dp[1][1]) >= 1e9) puts("-1");
		else printf("%lld\n", min(dp[1][1], dp[1][0]));
	}
	return 0;
}
