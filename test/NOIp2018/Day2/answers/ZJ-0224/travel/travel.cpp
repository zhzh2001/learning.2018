#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=5010;
int n,m,vis[N],ans[N][2],cnt=0;
struct ppap{int x,y;}a[N];
vector<int>e[N];
int px,py;
int nedge=0,p[2*N],nex[2*N],head[2*N];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline bool check(int x,int y){
	if(x>y)swap(x,y);
	if(px==x&&py==y)return 0;
	return 1;
}
inline void dfs(int x){
	ans[++cnt][1]=x;vis[x]=1;
	for(int k=head[x];k;k=nex[k])if(!vis[p[k]]&&check(x,p[k]))dfs(p[k]);
}
inline bool pd(){
	if(cnt<n)return 0;
	for(int i=1;i<=n;i++){
		if(ans[i][0]>ans[i][1])return 1;
		if(ans[i][0]<ans[i][1])return 0;
	}
	return 0;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		a[i].x=read(),a[i].y=read();
		e[a[i].x].push_back(a[i].y);
		e[a[i].y].push_back(a[i].x);
	}
	for(int i=1;i<=n;i++){
		sort(e[i].begin(),e[i].end());
		for(int j=e[i].size()-1;j>=0;j--)addedge(i,e[i][j]);
	}
	if(m==n){
		for(int i=1;i<=n;i++)ans[i][0]=1e9;
		for(int i=1;i<=m;i++){
			px=min(a[i].x,a[i].y);py=max(a[i].x,a[i].y);
			memset(vis,0,sizeof vis);
			cnt=0;dfs(1);
			if(pd())for(int i=1;i<=n;i++)ans[i][0]=ans[i][1];
		}
		for(int i=1;i<=n;i++)write(ans[i][0]),putchar(' ');	
	}else{
		cnt=0;dfs(1);
		for(int i=1;i<=n;i++)write(ans[i][1]),putchar(' ');
	}
	return 0;
}
