#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
const ll N=1e5+10;
ll n,m,a[N],b[N],fa[N],f[N][2],df[N][2],px,py;
ll nedge=0,p[2*N],nex[2*N],head[2*N];
char op[10];
inline void addedge(ll a,ll b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(ll x,ll Fa){
	fa[x]=Fa;f[x][0]=f[x][1]=0;
	for(ll k=head[x];k;k=nex[k])if(p[k]!=Fa){
		dfs(p[k],x);
		f[x][0]+=f[p[k]][1];
		f[x][1]+=min(f[p[k]][0],f[p[k]][1]);
	}
	f[x][1]+=a[x];
	if(b[x]==1)f[x][1]=1e18;
	if(b[x]==2)f[x][0]=1e18;
}
inline void Main(){
	ll la[2]={0};
	for(;m;m--){
		b[la[0]]=b[la[1]]=0;
		for(ll i=0;i<2;i++){
			ll x=read(),v=read();
			b[x]=v+1;
			la[i]=x;
		}
		dfs(1,0);
		if(f[1][0]>=1e18&&f[1][1]>=1e18)puts("-1");
		else writeln(min(f[1][0],f[1][1]));
	}
	exit(0);
}
inline void MAIN(){
	for(;m;m--){
		for(int i=0;i<2;i++){
			px=read();py=read();
		}
		int x=px;
		df[x][0]=f[x][0];df[x][1]=f[x][1];
		if(py==0)df[x][1]=1e18;
		if(py==1)df[x][0]=1e18;
		while(x!=1){
			df[fa[x]][0]=f[fa[x]][0];df[fa[x]][1]=f[fa[x]][1];
			df[fa[x]][0]+=df[x][1]-f[x][1];
			df[fa[x]][1]+=min(df[x][1],df[x][0])-min(f[x][1],f[x][0]);
			x=fa[x];
		}
		if(df[1][1]>=1e18)puts("-1");
		else writeln(df[1][1]);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",op);
	for(ll i=1;i<=n;i++)a[i]=read();
	for(ll i=1;i<n;i++){
		ll x=read(),y=read();
		addedge(x,y);addedge(y,x);
	}
	if(n<=2000)Main();
	dfs(1,0);
	if(op[0]=='B')MAIN();
	return 0;
}
