#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
const ll MOD=1e9+7;
ll ans=0,n,m,b[20][20];
ll kt[100][2];
inline bool pd(){
	for(ll i=1;i<=n+m-1;i++){
		if(kt[i][0]<kt[i][1])return 1;
		if(kt[i][0]>kt[i][1])return 0;
	}
	return 1;
}
inline bool check(ll x,ll y,ll cnt){
	kt[cnt][1]=b[x][y];
	if(x==n&&y==m){
		if(pd()){
			for(ll i=1;i<=n+m-1;i++)kt[i][0]=kt[i][1];
			return 1;
		}else return 0;
	}
	if(y<m)if(!check(x,y+1,cnt+1))return 0;
	if(x<n)if(!check(x+1,y,cnt+1))return 0;
	return 1;
}
inline void dfs(ll x,ll y){
	if(y==m+1){
		memset(kt,0,sizeof kt);
		if(check(1,1,1))ans++;
		return;
	}
	ll i=x,j=y;
	while(i&&j<=m){
		b[i][j]=0;
		if(x<n)dfs(x+1,y);
		else dfs(x,y+1);
		b[i][j]=1;
		i--;j++;
	}
	if(x<n)dfs(x+1,y);
	else dfs(x,y+1);
	i=x,j=y;
	while(i&&j<=m){
		b[i][j]=0;
		i--;j++;
	}
}
inline ll mi(ll a,ll b){
	ll x=1,y=a;
	for(;b;b>>=1,y=y*y%MOD)if(b&1)x=x*y%MOD;
	return x;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();m=read();
	if(n>m)swap(n,m);
	if(n==1){
		writeln(mi(2,m)%MOD);
		return 0;
	}
	if(n==2){
		writeln(mi(3,m-1)*4%MOD);
		return 0;
	}
	if(n==3){
		writeln(mi(3,m-3)*112%MOD);
		return 0;
	}
	if(n==4){
		if(m==4)writeln(912);
		ans=2688;writeln(ans*mi(3,m-n-1)%MOD);
		return 0;
	}
	if(n==5){
		if(m==5)writeln(7136);
		ans=21312;writeln(ans*mi(3,m-n-1)%MOD);
		return 0;
	}
	if(n==6){
		if(m==6)writeln(56768);
		ans=170112;writeln(ans*mi(3,m-n-1)%MOD);
		return 0;
	}
	if(n==m){
		dfs(1,1);
		writeln(ans);
		return 0;
	}
	int M=m;m=n+1;ans=0;
	dfs(1,1);writeln(ans*mi(3,M-n-1)%MOD);
	return 0;
}
