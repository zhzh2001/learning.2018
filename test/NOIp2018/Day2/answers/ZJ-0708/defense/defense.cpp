#include<bits/stdc++.h>
#define INF 1000000007
using namespace std;
int ans,f[100005][3],a[100005];
int q1,q2,p1,p2,n,m;
char s[10];
int head[200005],Next[200005],to[200005],tot;
void add(int x,int y)
{
	tot++;
	to[tot]=y;
	Next[tot]=head[x];
	head[x]=tot;
}
void dfs(int u,int pre)
{
	f[u][0]=0;
	f[u][1]=a[u];
	int res1=0; int res2=0;
	for (int i=head[u];i;i=Next[i])
	{
		int v=to[i];
		if (v==pre) continue;
		dfs(v,u);
		res1+=f[v][1];
		res2+=min(f[v][0],f[v][1]);
		if (res1>=INF) res1=INF;
		if (res2>=INF) res2=INF;
	}
	f[u][0]=res1;
	if (res2!=INF) f[u][1]+=res2; else
	f[u][1]=res2;
	if (u==q1&&p1==1) f[u][0]=INF;
	if (u==q1&&p1==0) f[u][1]=INF;
	if (u==q2&&p2==1) f[u][0]=INF;
	if (u==q2&&p2==0) f[u][1]=INF;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for (int i=1;i<=n;i++)
	scanf("%d",&a[i]);
	for (int i=1;i<=n-1;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&q1,&p1,&q2,&p2);
		dfs(1,1);
		int ans=min(f[1][0],f[1][1]);
		if (ans==INF) printf("-1\n"); else printf("%d\n",ans);
	}
}