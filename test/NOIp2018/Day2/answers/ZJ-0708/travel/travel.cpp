#include<bits/stdc++.h>
#define INF 100000007
using namespace std;
int head[10005],Next[10005],to[10005],tot;
int a[10005],vis[10005],n,m;
int fa[10005],ans[10005];
void add(int x,int y)
{
	tot++;
	to[tot]=y;
	Next[tot]=head[x];
	head[x]=tot;
}
void dfs(int u,int pre,int gf,int k)
{
	if (k>n) return;
	int minn=INF;
	for (int i=head[u];i;i=Next[i])
	{
		int v=to[i];
		if (v==pre) continue;
		if (vis[v]==1) continue;
		if (minn>v)
		{
			minn=v;
		}	
	}
	if (minn!=INF)
	{
		vis[minn]=1;
		a[k]=minn;
		fa[minn]=u;
		dfs(minn,u,pre,k+1);
	} else
	{
		if (pre!=-1)
		dfs(pre,gf,fa[gf],k);
	}
}
void dfs2(int u,int pre,int gf,int k)
{
	if (k>n) 
	{
		bool p=false;
		for (int i=1;i<=n;i++)
		{
			if (ans[i]>=a[k]) continue;
			p=true;
			break;
		}
		if (p==false)
		for (int i=1;i<=n;i++)
		ans[i]=a[i];
		return;
	}
	int minn=INF;
	for (int i=head[u];i;i=Next[i])
	{
		int v=to[i];
		if (v==pre) continue;
		if (vis[v]==1) continue;
		if (minn>v)
		{
			minn=v;
		}	
	}
	if (minn!=INF)
	{
		vis[minn]=1;
		a[k]=minn;
		fa[minn]=u;
		dfs2(minn,u,pre,k+1);
		vis[minn]=0;
	} 
	if (pre!=-1) dfs2(pre,gf,fa[gf],k);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	if (n==m+1)
	{
		a[1]=1;
		fa[1]=-1;
		vis[1]=1;
		dfs(1,-1,-1,2);
		for (int i=1;i<=n-1;i++)
		printf("%d ",a[i]);
		printf("%d\n",a[n]);
		return 0;
	} else
	{
		for (int i=1;i<=n;i++)
		ans[i]=INF;
		a[1]=1;
		fa[1]=-1;
		vis[1]=1;
		dfs2(1,-1,-1,2);
		for (int i=1;i<=n-1;i++)
		printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}
}