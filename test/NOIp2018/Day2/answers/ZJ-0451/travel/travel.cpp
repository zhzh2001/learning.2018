#include <bits/stdc++.h>
using namespace std;

const int N = 5e3 + 5;
const int M = N * 2;

int fir[N], ne[M], to[M], cnt, x, y, n, m, Bx, By;

vector <int> G[N];

struct Edge {
  int x, y;
  void input(void) {
    scanf("%d%d", &x, &y);
  }
}E[M];

int P[N], fa[N];

int getf(int x) {
  return (fa[x] == x) ? x : fa[x] = getf(fa[x]);
}

int All = 0, B[N];

void dfs(int x, int f) {
  B[++ All] = x;
  for(int i = 0; i < (int) G[x].size(); ++ i) {
    if(x == Bx && G[x][i] == By) continue;
    if(x == By && G[x][i] == Bx) continue;
    int V = G[x][i];
    if(V == f) continue;
    dfs(V, x);
  }
}

void pd(void) {
  for(int i = 1; i <= n; ++ i) {
    if(B[i] < P[i]) {
      for(int j = 1; j <= n; ++ j) P[j] = B[j];
      break;
    }
    else if(B[i] > P[i]) return;
  }
}

void solve() {
  for(int i = 1; i <= n; ++ i) fa[i] = i;
  All = 0;
  for(int i = 1; i <= m; ++ i) {
    if(E[i].x == Bx && E[i].y == By) continue;
    fa[getf(E[i].x)] = getf(E[i].y);
  }
  int tot = 0;
  for(int i = 1; i <= n; ++ i) tot += (fa[i] == i);
  if(tot >= 2) return;
  dfs(1, 0);
  pd();
}

void io() {
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);
}

int main() {
  io();
  scanf("%d%d", &n, &m);
  for(int i = 1; i <= m; ++ i) {
    E[i].input();
    G[E[i].x].push_back(E[i].y);
    G[E[i].y].push_back(E[i].x);
  }
  for(int i = 1; i <= n; ++ i) sort(G[i].begin(), G[i].end());
  if(m == n - 1) {
    P[1] = n;
    solve();
    for(int i = 1; i <= n; ++ i) printf("%d ", P[i]);
    puts("");
    return 0;
  }
  else {
    P[1] = n;
    for(int i = 1; i <= m; ++ i) {
      Bx = E[i].x, By = E[i].y;
      solve();
    }
    for(int i = 1; i <= n; ++ i) printf("%d ", P[i]);
    puts("");
    return 0;
  }
}
