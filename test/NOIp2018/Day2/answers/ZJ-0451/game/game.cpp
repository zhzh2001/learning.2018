#include <bits/stdc++.h>
using namespace std;

const int N = 105;
const int mod = 1e9 + 7;

namespace {
  int add(int x) {return (x >= mod) ? x - mod : x;}
  int sub(int x) {return (x < 0) ? x + mod : x;}
  void Add(int &x, int y) {x = add(x + y);}
  void Sub(int &x, int y) {x = sub(x - y);}
  int Pow(int x, int y = mod - 2) {
    int res = 1;
    for(; y; y >>= 1, x = 1LL * x * x % mod) {
      if(y & 1) {
	res = 1LL * res * x % mod;
      }
    }
    return res;
  }
}

int dp[N][N], n, m, x, y;

bool pd(int x, int y) {
  if(x == 0 && y == 1) return 0;
  return 1;
}

int main() {
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);
  cin >> n >> m;
  if(n == 1) {
    cout << Pow(2, m) << endl;
    return 0;
  }
  else if(n == 2) {
    if(m == 1) {
      cout << 4 << endl;
      return 0;
    }
    else {
      int ans = 12;
      for(int i = 3; i <= m; ++ i) {
	ans = 1LL * ans * 3 % mod;
      }
      cout << ans << endl;
    }
  }
  else if(n == 3) {
    if(m == 1) {
      cout << 8 << endl;
    }
    else if(m == 2) {
      cout << 36 << endl;
    }
    else {
      int ans = 112;
      for(int i = 4; i <= m; ++ i) {
	ans = 1LL * ans * 3 % mod;
      }
      cout << ans << endl;
    }
  }
}
