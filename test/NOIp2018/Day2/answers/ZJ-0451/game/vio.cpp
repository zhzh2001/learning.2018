#include <bits/stdc++.h>
using namespace std;

const int N = 123;

int C[N][N], n, m, x, y, ans;

int bc(int x) {
  int res = 0;
  for(; x; x >>= 1)
    if(x & 1) ++ res;
  return res;
}

bool pd(int x) {
  return bc(x) == n - 1;
}

bool pd() {
  for(int a = 0; a < n; ++ a) {
    for(int b = 0; b < m; ++ b) {
      if(a + 1 < n && b + 1 < m) {
	if(C[a + 1][b] == 0 && C[a][b + 1] == 1) return 0;
      }
    }
  }
  for(int i = 0; i < (1 << (n + m - 2)); ++ i) {
    if(!pd(i)) continue;
    for(int j = i + 1; j < (1 << (n + m - 2)); ++ j) if(pd(j)){
      vector <int> A, B;
      A.clear(); B.clear();
      for(int k = (n + m - 3); k >= 0; -- k) {
	A.push_back(!!(i & (1 << k)));
	B.push_back(!!(j & (1 << k)));
      }
      int xA = 0, yA = 0, xB = 0, yB = 0;
      for(int p = 0; p < (int) A.size(); ++ p) {
	if(!A[p]) ++ yA; else ++ xA;
	if(!B[p]) ++ yB; else ++ xB;
	if(C[xA][yA] > C[xB][yB]) {
	  //cerr << i <<" " << j << endl;
	  //for(int a = 0; a < n; ++ a) {
	  //for(int b = 0; b < m; ++ b) {
	  //cout << C[a][b] <<" "; 
	  //}
	  // puts("");
	  //}
	  //puts("");
	  return 0;
	}
	else if(C[xA][yA] < C[xB][yB]) break;
      }
    }
  }
  return 1;
}

int main() {
  cin >> n >> m;
  //cerr << bc(7) << endl;
  for(int i = 0; i < (1 << (n * m)); ++ i) {
    for(int a = 0; a < n; ++ a) {
      for(int b = 0; b < m; ++ b) {
	C[a][b] = (i >> (a * m + b)) & 1;
      }
    }
    ans += pd();
  }
  cout << ans << endl;
}
