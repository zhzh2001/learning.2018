#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 5;
const int M = N * 2;

int fir[N], ne[M], to[M], cnt, n, m, Cost[N], x, y;

#define Foreachson(i, x) for(int i = fir[x]; i; i = ne[i])

void add(int x, int y) {
  ne[++ cnt] = fir[x];
  fir[x] = cnt;
  to[cnt] = y;
}

void link(int x, int y) {
  add(x, y);
  add(y, x);
}

#define Foreachson(i, x) for(int i = fir[x]; i; i = ne[i])

string type;

namespace solver1 {
  int fa[N], dep[N], tim, vis[N];
  long long dp[N][2], DP[N][2];

  void dfs(int x, int f) {
    fa[x] = f;
    dep[x] = dep[f] + 1;
    dp[x][0] = 0;
    dp[x][1] = Cost[x];
    Foreachson(i, x) {
      int V = to[i];
      if(V == f) continue;
      dfs(V, x);
      dp[x][1] += min(dp[V][0], dp[V][1]);
      dp[x][0] += dp[V][1];
    }
  }

  int LCA(int x, int y) {
    if(dep[x] > dep[y]) swap(x, y);
    if(x == y) return x;
    return LCA(fa[y], x);
  }

  vector <int> re;

  void add(int x) {
    if(vis[x] != tim) {
      vis[x] = tim;
      re.push_back(x);
    }
  }

  void Qu(int x) {
    dp[fa[x]][1] -= min(dp[x][0], dp[x][1]);
    dp[fa[x]][0] -= dp[x][1];
    return;
  }

  void Add(int x) {
    dp[fa[x]][1] += min(dp[x][0], dp[x][1]);
    dp[fa[x]][0] += dp[x][1];   
  }
  
  void chg(int a, int x, int L) {
    vector <int> who;
    who.clear();
    who.push_back(a);
    int it = a;
    while(a != L) {
      a = fa[a];
      who.push_back(a);				
      //cerr << a <<" " << x << " " << L << endl;
    }
    for(int i = (int)who.size() - 1; i >= 0; -- i)
      Qu(who[i]);
    if(x == 0) dp[it][1] = 1e17;
    else if(x == 1) dp[it][0] = 1e17;
    for(int i = 0; i < (int) who.size(); ++ i) {
      Add(who[i]);
      add(who[i]);
    }
  }
  
  void solve() {
    dfs(1, 0);
    for(int i = 1; i <= n; ++ i) {
      DP[i][0] = dp[i][0];
      DP[i][1] = dp[i][1];
    }
    //for(int i = 1; i <= n; ++ i) cerr << dp[i][0] <<" " << dp[i][1] << endl;
    for(int i = 1; i <= m; ++ i) {
      int a, x, b, y;
      scanf("%d%d%d%d", &a, &x, &b, &y);
      re.clear();
      int L = LCA(a, b);
      if(dep[a] > dep[b]) {
	swap(a, b);
	swap(x, y);
      }
      if(L == a && dep[b] - dep[a] == 1 && (!x && !y)) {
	puts("-1");
	continue;
      }
      //cerr << a <<" " << x << " " << b << " " << y <<" " << L << endl;
      ++ tim;
      chg(b, y, 1);
      if(a != L) {
	chg(a, x, 1);
	chg(L, -1, 1);
      }
      else chg(a, x, 1);
      //cerr << dp[2][0] <<" " << dp[2][1] << endl;
      //cerr << dp[3][0] <<" " << dp[3][1] << endl;
      //cerr << Cost[2] << endl;
      //for(int i = 1; i <= n; ++ i) cerr << dp[i][0] <<" " << dp[i][1] << endl;
      //puts("");
      printf("%lld\n", min(dp[1][0], dp[1][1]));
      //for(int i = 1; i <= n; ++ i) re.push_back(i);
      for(int i = 0; i < (int) re.size(); ++ i) {
	dp[re[i]][0] = DP[re[i]][0];
	dp[re[i]][1] = DP[re[i]][1];
      }
    }
  }
}

namespace solver2 {
  void solve() {
  }
}

int main() {
  freopen("defense.in", "r", stdin);
  freopen("defense.out", "w", stdout);
  scanf("%d%d", &n, &m);
  cin >> type;
  for(int i = 1; i <= n; ++ i) scanf("%d", &Cost[i]);
  for(int i = 1; i < n; ++ i) {
    scanf("%d%d", &x, &y);
    link(x, y);
  }
  if(n <= 2000 && m <= 2000) {
    solver1 :: solve();
    return 0;
  }
  else {
    if(type[0] == 'A') {
      solver2 :: solve();
      return 0;
    }
    else if(type[0] == 'B') {
      solver1 :: solve();
      return 0;
    }
  }
}
