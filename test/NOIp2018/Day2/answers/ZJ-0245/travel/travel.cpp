#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
bool e[5005][5005];
bool v[5005];
int n,m;
int m1,m2;
void dfs1(int x)
{
	printf("%d ",x);
	v[x]=1;
	for(int i=1;i<=n;i++)
	{
		if(e[i][x]&&!v[i])
		{
			v[i]=1;
			dfs1(i);
		}
	}
}
void dfs2(int x)
{
	printf("%d ",x);
	v[x]=1;
	for(int i=1;i<=n;i++)
	{
		if(!v[i])
		{
			for(int j=1;j<=n;j++)
			{
				if(e[i][j]&&v[j])
				{
					v[i]=1;
					dfs2(i);
					return;
				}
			}
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d", &n,&m);
	for(int i=0;i<m;i++)
	{
		int u,v;
		scanf("%d%d", &u,&v);
		e[u][v]=1;
		e[v][u]=1;
	}
	if (m==n-1)
	 dfs1(1);
	else
	dfs2(1);
	return 0;
}
