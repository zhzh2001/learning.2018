#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
const int INF=2000000000;
int n,m;
char s[3];
struct edge
{
	int v,next;
}e[200005];
int head[200005];
int pi[100005];
int tot;
void add(int u,int v)
{
	e[++tot].v=v;
	e[tot].next=head[u];
	head[u]=tot;
}
long long dp[100005][2];
int a,b,x,y;
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	if (s[0]=='A')
	{
		for(int i=1;i<n;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		
	}
	for(int i=1;i<=n;i++)
	{
		int w;
		scanf("%d",&w);
		pi[i]=w;
	}
		while(m--)
		{
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(abs(a-b)==1&&x==0&&y==0)
			{
				printf("%d\n", -1);
				continue;
			}
			for(int i=1;i<=n;i++)
			{
				if(i==a)
				{
					if(x==1)
					{
					dp[i][0]=INF;
					dp[i][1]=min(dp[i-1][1],dp[i-1][0])+pi[i];
					}
					else
					{
						dp[i][0]=dp[i-1][1];
						dp[1][0]=0;
					}
				}
				else if(i==b)
				{
					if(y==1)
					{
					dp[i][0]=INF;
					dp[i][1]=min(dp[i-1][1],dp[i-1][0])+pi[i];
					}
					else
					{
						dp[i][0]=dp[i-1][1];
						dp[i][1]=INF;
					}
				}
				else
				{
					dp[i][0]=dp[i-1][1];
					dp[i][1]=min(dp[i-1][1],dp[i-1][0])+pi[i];
				}
			}
			printf("%ld\n",min(dp[n][0],dp[n][1]));
		}
	}
	
	return 0;
}
