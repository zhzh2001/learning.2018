#include "bits/stdc++.h"
#define N 500001
#define P 9999999999999999
using namespace std;

char ch[10];
int n,m,u,v,u0,v0,my_cost[N];
long long f[N][2];
int k,a[N],b[N],my_first[N],my_next[N],my_last[N];

inline void my_read(int &x)
{
	int s=1;
	char ch=0;
	x=0;
	while(ch^'-'&&!isdigit(ch))
		ch=getchar();
	if(ch=='-')
	{
		s=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=(x<<1)+(x<<3)+ch-'0';
		ch=getchar();
	}
	x*=s;
}

inline bool my_yes(int x)
{
	return ((u==x&&u0==1)||(v==x&&v0==1));
}

inline bool my_no(int x)
{
	return ((u==x&&u0==0)||(v==x&&v0==0));
}

inline void my_add(int x,int y)
{
	k++;
	a[k]=x;
	b[k]=y;
	if(my_first[x]==0)
		my_first[x]=k;
	else
		my_next[my_last[x]]=k;
	my_last[x]=k;
}

void my_dfs(int x,int y)
{
	int p=my_first[x];
	if(x^1&&!my_next[p])
	{
		if(my_yes(x))
		{
			f[x][1]=my_cost[x];
			f[x][0]=P;
			return;
		}
		if(my_no(x))
		{
			f[x][1]=P;
			f[x][0]=0;
			return;
		}
		f[x][1]=my_cost[x];
		f[x][0]=0;
		return;
	}
	if(my_yes(x))
	{
		f[x][0]=P;
		f[x][1]=my_cost[x];
	}
	else
	{
		if(my_no(x))
		{
			f[x][0]=0;
			f[x][1]=P;
		}
		else
		{
			f[x][0]=0;
			f[x][1]=my_cost[x];
		}
	}
	while(p)
	{
		if(b[p]^y)
		{
			my_dfs(b[p],x);
			if(my_yes(x))
				f[x][1]+=min(f[b[p]][0],f[b[p]][1]);
			else
			{
				if(my_no(x))
					f[x][0]+=f[b[p]][1];
				else
				{
					f[x][0]+=f[b[p]][1];
					f[x][1]+=min(f[b[p]][0],f[b[p]][1]);
				}
			}
		}
		p=my_next[p];
	}
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	my_read(n),my_read(m),scanf("%s",ch);
	for(int i=1;i<=n;i++)
		my_read(my_cost[i]);
	for(int i=1;i<n;i++)
	{
		my_read(u),my_read(v);
		my_add(u,v),my_add(v,u);
	}
	for(int i=1;i<=m;i++)
	{
		my_read(u),my_read(u0),my_read(v),my_read(v0);
		if(u0==0&&v0==0)
		{
			int w=0;
			for(int i=1;i<=k;i++)
				if(a[i]==u&&b[i]==v)
				{
					printf("-1\n");
					w=1;
					break;
				}
			if(w)
				continue;
		}
		my_dfs(1,1);
		if(my_yes(1))
		{
			printf("%lld\n",f[1][1]);
			continue;
		}
		if(my_no(1))
		{
			printf("%lld\n",f[1][0]);
			continue;
		}
		printf("%lld\n",min(f[1][0],f[1][1]));
	}
	return 0;
}
