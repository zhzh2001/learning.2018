#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define maxn 5008
int n,m;
vector<int> temp;
vector<int> ans;
vector<int> v1[maxn];
bool vis[maxn];
stack<int> s1;
bool flag=false;
bool loop[maxn];
int first[maxn];
int nxt[maxn*2];
int last[maxn];
int to[maxn*2];
int from[maxn*2];
bool not_ok[maxn*2];
int cnt=0;
void addedge(int a,int b){
	if(!first[a])first[a]=++cnt;
	else nxt[last[a]]=++cnt;
	last[a]=cnt;
	to[cnt]=b;
}
void dfs_tan(int u,int fa){
	int len1=v1[u].size(),v;
	ans.pb(u);
	vis[u]=1;
	priority_queue<int,vector<int>,greater<int> > pq;
	for(int i=0;i<len1;i++){
		v=v1[u][i];
		if(!vis[v])pq.push(v);
	}
	while(!pq.empty()){
		int f=pq.top();
		pq.pop();
		dfs_tan(f,u);
	}
}
void solve1(){
	dfs_tan(1,-1);
	int len1=ans.size();
	for(int i=0;i<len1;i++){
		printf("%d ",ans[i]);
	}
	printf("\n");
}
void find_loop(int u,int fa){
	if(flag)return ;
	vis[u]=1;
	int len1=v1[u].size();
	s1.push(u);
	for(int i=0;i<len1;i++){
		int v=v1[u][i];
		if(v!=fa&&!vis[v]){
			find_loop(v,u);
		}else if(vis[v]&&v!=fa){
			int f;
			do{
				f=s1.top();
				s1.pop();
				loop[f]=1;
			}while(f!=v);
			flag=1;
		}
		if(flag)return ;
	}
	s1.pop();
}
void print(vector<int> v1){
	int len1=v1.size();
	for(int i=0;i<len1;i++){
		printf("%d ",v1[i]);
	}
	printf("\n");
} 
int tot;
bool mi=false;
void dfs_tan2(int u,int fa){
	tot++;
	temp.pb(u);
	if(!ans.empty()&&temp[tot]<ans[tot])mi=1;
	else if(!ans.empty()&&temp[tot]>ans[tot]&&!mi){flag=1;return ;}
	priority_queue<int,vector<int>,greater<int> > pq;
	for(int q=first[u];q;q=nxt[q]){
		if(flag)return ;
		if(to[q]!=fa&&!not_ok[q]){
			pq.push(to[q]);
		}
	}
	while(!pq.empty()){
		int f=pq.top();
		pq.pop();
		if(flag)return ;
		dfs_tan2(f,u);
	}
}
void solve2(){
	find_loop(1,-1);
	for(int i=1;i<=cnt;i+=2){
		if(loop[to[i]]&&loop[to[i+1]]){
			not_ok[i]=not_ok[i+1]=1;
			flag=false;tot=-1;
			temp.clear();
			mi=false;
			dfs_tan2(1,-1);
			if(!flag)ans=temp;
			not_ok[i]=not_ok[i+1]=false;
		}
	}
	print(ans);
}
void file(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
}
int main(){
	file();
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int a,b;
		scanf("%d%d",&a,&b);
		v1[a].pb(b);
		v1[b].pb(a);
		addedge(a,b);
		addedge(b,a);
	} 
	if(m==n-1){
		solve1();
	}else{
		solve2();
	}
	return 0;
}
