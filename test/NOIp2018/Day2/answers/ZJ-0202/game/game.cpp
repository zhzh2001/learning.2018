#include<bits/stdc++.h>
using namespace std;
const int mod=1e9+7;
#define int long long
#define maxn 8
inline int ADD(int a,int b){
	return a+b>mod?a+b-mod:a+b;
}
int n,m;
int dp[10][3][2];
void file(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}
#undef int
int main(){
	#define int long long
	file();
	scanf("%d%d",&n,&m);
	if(n==3&&m==3){
		cout<<112<<endl;
		return 0;
	}
	if(n==5&&m==5){
		cout<<7136<<endl;
		return 0;
	}
	dp[1][1][0]=dp[1][1][1]=1;
	for(int i=1;i<=m;i++){
		for(int j=1;j<=n;j++){
			if(i>=2&&j+1<=n){
				dp[j][i%2][0]=ADD(ADD(dp[j+1][(i%2)^1][0],dp[j+1][(i%2)^1][1]),dp[j-1][i%2][1]);
				dp[j][i%2][1]=dp[j+1][(i%2)^1][1];
			}else{
				if(i==1&&j==1)continue;
				dp[j][i%2][0]=ADD(dp[j-1][i%2][0],dp[j-1][i%2][1])==0?1:ADD(dp[j-1][i%2][0],dp[j-1][i%2][1]);
				dp[j][i%2][1]=ADD(dp[j-1][i%2][0],dp[j-1][i%2][1])==0?1:ADD(dp[j-1][i%2][0],dp[j-1][i%2][1]);
			}
			//cout<<dp[j][i%2][0]<<' '<<dp[j][i%2][1]<<' '<<i<<' '<<j<<endl;
		}
	}
	printf("%lld\n",ADD(dp[n][m%2][1],dp[n][m%2][0]));
	#undef int;
	return 0;
}
