#include <fstream>

using namespace std;

typedef long long i64;

const i64 MOD = 1e9+7;

i64 pow(i64 a, i64 b, i64 m) {
   i64 r = 1;
   for (; b; b >>= 1, a = a * a % m) if (b & 1) r = r * a % m;
   return r;
}

int main() {
   ifstream fin("game.in");
   ofstream fout("game.out");
   fin.sync_with_stdio(false);
   fout.sync_with_stdio(false);
   int n, m;
   fin >> n >> m;
   if (n == 1) fout << pow(2, m, MOD) << endl;
   if (n == 2) {
      i64 dp = 1, w = 1;
      for (int i = 1; i < m; i++) dp *= 2, dp += w, dp %= MOD, w = w * 4 % MOD;
      fout << dp * 4 % MOD << endl;
   }
} 
