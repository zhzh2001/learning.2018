#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

const int N = 5000;

int n, m;
int ans[N], f[N];
vector<int> g[N];

int bu, bv;
void setBlock(int a, int b) { bu = a, bv = b; }
bool isBlock(int a, int b) { return (a == bu && b == bv) || (a == bv && b == bu); }

int tdf;
int vis[N];

void dfs(int p) {
   vis[tdf++] = p;
   for (int _ = 0; _ < g[p].size(); _++) {
      int i = g[p][_];
      if (i == f[p] || isBlock(p, i)) continue;
      f[i] = p;
      dfs(i);
   }
}

bool ins[N];
int stk[N], tst, cs[N], tcs;

bool cir(int p) {
   ins[p] = true;
   stk[tst++] = p;
   for (int _ = 0; _ < g[p].size(); _++) {
      int i = g[p][_];
      if (i == f[p]) continue;
      f[i] = p;
      if (ins[i]) {
         while (stk[--tst] != i) {
            cs[tcs++] = stk[tst];
         }
         cs[tcs++] = stk[tst];
         return true;
      }
      if (cir(i)) return true;
   }
   tst--;
   ins[p] = false;
   return false;
}

void solve1() {
   tdf = 0;
   dfs(0);
   for (int i = 0; i < n; i++) ans[i] = vis[i];
}

bool cmp() {
   for (int i = 0; i < n; i++) if (ans[i] != vis[i]) return vis[i] < ans[i];
   return false;
}

void CAS() {
   if (cmp()) for (int i = 0; i < n; i++) ans[i] = vis[i];
}

void solve2() {
   cir(0);
   // for (int i = 0; i < tcs; i++) cout << cs[i] << " "; cout << endl;
   setBlock(cs[0], cs[tcs - 1]);
   tdf = 0;
   dfs(0);
   for (int i = 0; i < n; i++) ans[i] = vis[i];
   for (int i = 1; i < tcs; i++) setBlock(cs[i], cs[i - 1]), tdf = 0, dfs(0), CAS();
}

int main() {
   ifstream fin("travel.in");
   ofstream fout("travel.out");
   fin.sync_with_stdio(false);
   fout.sync_with_stdio(false);
   fin >> n >> m;
   for (int i = 0, a, b; i < m; i++) {
      fin >> a >> b;
      a--, b--;
      g[a].push_back(b);
      g[b].push_back(a);
   }
   for (int i = 0; i < n; i++) sort(g[i].begin(), g[i].end());
   if (m == n - 1) solve1();
   else solve2();
   for (int i = 0; i < n; i++) fout << ans[i] + 1 << " ";
   fout << endl;
}
