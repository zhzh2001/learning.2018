#include <fstream>
#include <string>
#include <vector>

using namespace std;

typedef long long i64;

const int N = 100000;

int n, m;
string type;
i64 p[N];
vector<int> g[N];
int deg[N];

int tdf;
int dfn[N];
int pos[N];
int s0, s1;
int ss0[N], ss1[N];
bool vis[N];

void dfs(int p) {
   dfn[tdf] = p;
   pos[p] = tdf++;
   vis[p] = true;
   for (int _ = 0; _ < g[p].size(); _++) {
      int i = g[p][_];
      if (vis[i]) continue;
      dfs(i);
   }
}

int solve(int a, int x, int b, int y) {
   int pa = pos[a], pb = pos[b];
   if (x == 0 && y == 0) {
      if (pa > pb) swap(a, b), swap(x, y), swap(pa, pb);
      if (pa + 1 == pb) return -1;
      if (pa % 2 == pb % 2) return (pa % 2) ? s0 : s1;
      int sa = (pa % 2 ? ss1[n - 1] - ss1[a] + ss0[a] : ss0[n - 1] - ss0[a] + ss1[a]) + p[dfn[pa + 1]];
      int sb = (pb % 2 ? ss0[n - 1] - ss0[b] + ss1[b] : ss1[n - 1] - ss1[b] + ss0[b]) + p[dfn[pb - 1]];
      return min(sa, sb);
   }
   if (x == 1 && y == 1) {
      if (pa % 2 == pb % 2) return (pa % 2) ? s1 : s0;
      int sa = (pa % 2) ? s1 : s0;
      int sb = (pb % 2) ? s1 : s0;
      return min(sa + p[b], sb + p[a]);
   }
   if (x == 0) swap(a, b), swap(x, y), swap(pa, pb);
   return (pa % 2) ? s1 : s0;
}

int main() {
   ifstream fin("defense.in");
   ofstream fout("defense.out");
   fin.sync_with_stdio(false);
   fout.sync_with_stdio(false);
   fin >> n >> m >> type;
   for (int i = 0; i < n; i++) fin >> p[i];
   for (int i = 1, a, b; i < n; i++) {
      fin >> a >> b;
      a--, b--;
      g[a].push_back(b);
      g[b].push_back(a);
      deg[a]++, deg[b]++;
   }
   for (int i = 0; i < n; i++) if (deg[i] == 1) { dfs(i); break; }
   for (int i = 0; i < n; i++) { 
      if (i) ss0[i] = ss0[i - 1], ss1[i] = ss1[i - 1];
      if (pos[i] % 2) s1 += p[i], ss1[i] += p[i]; else s0 += p[i], ss0[i] += p[i]; 
   }
   for (int i = 0, a, x, b, y; i < m; i++) {
      fin >> a >> x >> b >> y;
      fout << solve(a - 1, x, b - 1, y) << endl;
   }
}
