#include <fstream>
#include <string>
#include <vector>

using namespace std;

typedef long long i64;

const int N = 100000;

int n, m;
string type;
i64 p[N];
vector<int> g[N];

int d[N], s[2], c[N], f[N];

void dfs(int p) {
   for (int _ = 0; _ < g[p].size(); _++) {
      int i = g[p][_];
      if (f[p] == i) continue;
      c[p] += p[i];
      d[i] = d[p] + 1;
      f[i] = p;
      dfs(i);
   }
}

int solve(int a, int x, int b, int y) {
   int da = d[a], db = d[b];
   if (x == 0 && y == 0) {
      if (a == f[b] || b == f[a]) return -1;
      if (da % 2 == db % 2) return s[1 - da % 2];
      int sa = s[da % 2] - p[a];
      int sb = s[db % 2] - p[b];
}

int main() {
   ifstream fin("defense.in");
   ofstream fout("defense.out");
   fin.sync_with_stdio(false);
   fout.sync_with_stdio(false);
   fin >> n >> m >> type;
   for (int i = 0; i < n; i++) fin >> p[i];
   for (int i = 1, a, b; i < n; i++) {
      fin >> a >> b;
      a--, b--;
      g[a].push_back(b);
      g[b].push_back(a);
      deg[a]++, deg[b]++;
   }
   dfs(0);
   for (int i = 0; i < n; i++) s[d[i] % 2] += p[i];
   for (int i = 0, a, x, b, y; i < m; i++) {
      fin >> a >> x >> b >> y;
      fout << solve(a - 1, x, b - 1, y) << endl;
   }
}
