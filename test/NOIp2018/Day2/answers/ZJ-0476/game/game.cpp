#include <cstdio>
#include <algorithm>
using namespace std;
typedef long long ll;
const ll mod=1e9+7;
ll n,m,k,ans,tot,a[9][1000005];
ll fac[1000005];
ll ksm(ll x,ll y){
  ll ans=1;
  for (;y;y>>=1,x=x*x%mod) if (y&1) ans=ans*x%mod;
  return ans;
}
void dfs(ll x,ll y){
  if (x==n+1){
    ll flag=0;
    for (ll i=1;i<=n;i++)
      for (ll j=1;j<=m;j++)
        if (i>1&&j<m){
          if (a[i-1][j+1]>a[i][j]){
          	flag=1;
          	break;
		  }
		}
	if (!flag) ans++;
	return;
  }
  for(int i=0;i<2;i++){
    a[x][y]=i;
    if (y<m) dfs(x,y+1);
    else dfs(x+1,1);
  }
}
int main(){
  freopen("game.in","r",stdin);
  freopen("game.out","w",stdout);
  scanf("%lld%lld",&n,&m);
  fac[0]=1;
  for (ll i=1;i<=1e6;i++) fac[i]=fac[i-1]*2%mod;
  if (n<=3&&m<=3){
  if (n==3&&m==3){puts("112");return 0;}
  else {
  dfs(1,1);
  printf("%lld\n",ans);
  return 0;
  }
  }
  if (n<=2){
  	if (n==1) {
	  printf("%lld\n",fac[m]);
      return 0;
	 }
	else
	  {
	   printf("%lld\n",4*ksm(3,m-1)%mod);
	   return 0;
	  }
  }
  return 0;
}
