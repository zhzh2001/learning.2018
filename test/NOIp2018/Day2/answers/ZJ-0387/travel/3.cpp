#include<iostream>
#include<cstdio>
using namespace std;
int n,m,t,edgenum,x,y,ead[10005],nex[10005],a[100005],vet[10005],V[10005];
void addedge(int u,int v){
	nex[++edgenum]=ead[u];
	vet[edgenum]=v;
	ead[u]=edgenum;
}
void DFS(int x){
	int p[5005]={0};
	if(x==0)
		return;
	printf("%d ",x);
	V[x]=1;
	int g=100000;
	for(int i=ead[x];i;i=nex[i]){
		int v=vet[i];
		if(V[v]==0){
			p[v]=1;
		}
	}
	for(int i=1;i<=n;i++){
		if(p[i]==1&&V[i]==0){
			DFS(i);
		}
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		addedge(x,y);
		addedge(y,x);
	}
	V[1]=1;
	DFS(1);
	return 0;
}
