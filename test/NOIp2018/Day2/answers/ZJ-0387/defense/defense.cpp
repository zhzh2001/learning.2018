#include<iostream>
#include<cstdio>
using namespace std;
int edgenum,n,m,x,y,a,b,hea[100005],nex[100005],vet[100005],h[100005],H,f[100005][5],q[100005][5],num[100005];
char c[10005];
void addedge(int u,int v){
	edgenum++;
	nex[edgenum]=hea[u];
	vet[edgenum]=v;
	hea[u]=edgenum;
}
void DFS(int x,int qwe){
	h[x]=qwe;
	if(qwe>H)
		H=qwe;
	for(int i=hea[x];i;i=nex[i]){
		if(h[vet[i]]==0)
			DFS(vet[i],qwe+1);
	}
}
void le(){
	for(int i=1;i<=n;i++){
		if(h[i]==H){
			f[i][0]=0;
			f[i][1]=num[i];
		}
	}
	for(int qwe=H-1;qwe>=1;qwe--){
		for(int i=1;i<=n;i++){
			if(h[i]==qwe){
				for(int j=hea[i];j;j=nex[j]){
					int v=vet[j];
					if(h[v]>h[i]){
						f[i][0]=f[i][0]+f[v][1];
						if(f[v][0]!=-1){
							f[i][1]=min(f[i][1]+f[v][0],f[i][1]+f[v][1]);
						}else{
							f[i][1]=f[i][1]+f[v][1];
						}
					}
				}
				f[i][1]+=num[i];
			}
		}
	}
}
void pd(int a,int x,int b,int y){
	for(int i=1;i<=n;i++){
		if(h[i]<max(h[a],h[b])){
			q[i][0]=0;
			q[i][1]=0;
		}
	}
	q[a][(x+1)%2]=-1;q[b][(y+1)%2]=-1;
	q[a][x]=f[a][x];q[b][y]=f[b][y];
	for(int qwe=max(h[a],h[b])-1;qwe>=1;qwe--){
		for(int i=1;i<=n;i++){
			if(h[i]==qwe){
				if(q[i][0]!=-1)
				q[i][0]=0;if(q[i][1]!=-1)q[i][1]=num[i];
				for(int j=hea[i];j;j=nex[j]){
					int v=vet[j];
					if(h[v]>h[i]){
						if(q[v][0]!=-1&&q[v][1]!=-1){
							if(q[i][0]!=-1)
								q[i][0]=q[i][0]+q[i][1];
							if(q[i][1]!=-1)
								q[i][1]=min(q[i][1]+q[v][0],q[i][1]+q[v][1]);
						}else{
							if(q[v][0]==-1){
								if(q[i][0]!=-1)
									q[i][0]=q[i][0]+q[i][1];
								if(q[i][1]!=-1){
									q[i][1]=q[i][1]+q[v][1];
								}
							}else{
								if(q[v][0]!=-1){
									q[i][0]=-1;
									if(q[i][1]!=-1)
										q[i][1]=q[i][1]+q[v][0];
								}else{
									q[i][0]=-1;
									q[i][1]=-1;
								}
							}
						}
					}
				}
			}
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,c);
	for(int i=1;i<=n;i++){
		scanf("%d",&num[i]);
	}
	for(int i=1;i<n;i++){
		scanf("%d%d",&x,&y);
		addedge(x,y);
		addedge(y,x);
	}
	DFS(1,1);
	le();
	for(int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		pd(a,x,b,y);
		printf("%d\n",min(q[1][0],q[1][1]));
	}
	return 0;
}
