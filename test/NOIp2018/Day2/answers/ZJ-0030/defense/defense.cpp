#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>

using namespace std;

int n,cst[100010],m;
char type[2];

int hhead[100010],tto[200010],nnext[200010],ne,conn[2010][2010],vis[200010],wk[200010][2];
long long int f[200010][2];

int x,y;

void ae(int u,int v)
{
	tto[++ne]=v;
	nnext[ne]=hhead[u];
	hhead[u]=ne;
	if(n<=2000) conn[u][v]=1;
}

long long llmin(long long a,long long b)
{
	if(a<b) return a;
	else return b;
}

void dfs(int pos,int zt)
{
	if(wk[pos][zt]) return;
	if(pos==x&&zt!=y)
	{
		f[pos][zt]=99999;
		return;
	}
	vis[pos]=1;
	if(pos==x)
	{
		if(!y)
		for(int i=hhead[pos];i;i=nnext[i])
		{
			if(vis[tto[i]]) continue;
			dfs(tto[i],1);
			f[pos][0]+=f[tto[i]][1];
		}
		else
		for(int i=hhead[pos];i;i=nnext[i])
		{
			if(vis[tto[i]]) continue;
			dfs(tto[i],1);
			dfs(tto[i],0);
			f[pos][1]+=llmin(f[tto[i]][1],f[tto[i]][0]);
		}
	}
	else
	if(!zt)
	{
		for(int i=hhead[pos];i;i=nnext[i])
		{
			if(vis[tto[i]]) continue;
			dfs(tto[i],1);
			f[pos][0]+=f[tto[i]][1];
		}
	}
	else
		for(int i=hhead[pos];i;i=nnext[i])
		{
			if(vis[tto[i]]) continue;
			dfs(tto[i],1);
			dfs(tto[i],0);
			f[pos][1]+=llmin(f[tto[i]][1],f[tto[i]][0]);
		}
	if(zt==1) f[pos][1]+=(long long)cst[pos];
	wk[pos][zt]=1;
	vis[pos]=0;
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%s",type);
	for(int i=1;i<=n;i++) scanf("%d",&cst[i]);
	for(int i=1;i<n;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		ae(u,v);
		ae(v,u);
	}
	for(int i=1;i<=m;i++)
	{
		int a,b;
		scanf("%d%d%d%d",&a,&b,&x,&y);
		if(!b&&!y)
		{
			if(n<=2)
			{
				if(conn[a][x])
				{
					printf("-1\n");
					continue;
				}
			}
			else
			{
				for(int i=hhead[a];i;i=nnext[i])
				{
//					printf("%d\n",tto[i]);	
					if(tto[i]==x)
					{
						printf("-1\n");
						goto nextcase;
					}
				}
			}
		}
		memset(vis,0,sizeof(vis));
		memset(f,0,sizeof(f));
		memset(wk,0,sizeof(wk));
		dfs(a,b);
		printf("%lld\n",f[a][b]);
		nextcase:
			;
	}
	return 0;
}

