#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>

using namespace std;

int n,m,cne[5010][5010],cnt[5010],vis[5010],incr[5010],iodus[5010],mark[5010],del[5010],ins[5010];

int zhefan,firstincir=1,yizhefan,cut1,cut2,findcir,xxx,x,finished;

bool Cmp(int a,int b)
{
	int sa[10],sb[10];
	int pa=0,pb=0;
	while(a)
	{
		sa[++pa]=a%10;
		a/=10;
	}
	while(b)
	{
		sb[++pb]=b%10;
		b/=10;
	}
	if(pa>pb) return 0;
	if(pa<pb) return 1;
	while(1)
	{
		if(sa[pa]>sb[pb]) return 0;
		if(sa[pa]<sb[pb]) return 1;
		pa--;
		pb--;
		if(pa<0) pa=0;
		if(pb<0) pb=0;
	}
}

void dfs(int x,int from)
{
	if(finished) return;
	if(ins[x])
	{
		findcir=1;
		incr[x]=1;
		xxx=x;
		return;
	}
	ins[x]=1;
	for(int i=1;i<=cnt[x];i++)
	{
		if(cne[x][i]!=from)
			dfs(cne[x][i],x);
		if(findcir) break;
	}
	if(findcir)
	{
		if(x!=xxx)
		{
			incr[x]=1;
			return;
		}
		else
		{
			findcir=0;
			finished=1;
		}
	}
	ins[x]=0;
}

void work(int x)
{
	
	printf("%d ",x);
	vis[x]=1;
	sort(cne[x]+1,cne[x]+1+cnt[x],Cmp);
	for(int i=1;i<=cnt[x];i++)
		if(!vis[cne[x][i]]) work(cne[x][i]);
}

void work3(int x)
{
	printf("%d ",x);
	vis[x]=1;
	sort(cne[x]+1,cne[x]+1+cnt[x],Cmp);
	for(int i=1;i<=cnt[x];i++)
		if(x==cut1&&cne[x][i]==cut2) continue;
		else if(x==cut2&&cne[x][i]==cut1) continue;
		else if(!vis[cne[x][i]]) work3(cne[x][i]);
	return;

}

void work2(int x)
{
	if(yizhefan) return;
	vis[x]=1;
	sort(cne[x]+1,cne[x]+1+cnt[x],Cmp);
	if(incr[x]&&firstincir)
	{
		firstincir=0;
		int a1=0,a2=0;
		for(int i=1;i<=cnt[x];i++)
		{
			if(incr[cne[x][i]]&&!a1) a1=cne[x][i];
			else if(incr[cne[x][i]]&&a1&&!a2)
			{
				a2=cne[x][i];
				break;
			}
		}
		zhefan=a2;
		work2(a1);
	}
	else if(incr[x])
	{
		int nextpt=0;
		for(int i=1;i<=cnt[x];i++)
		{
			if(incr[cne[x][i]]&&!vis[cne[x][i]])
			{
				nextpt=cne[x][i];
				break;
			}
		}
		if(nextpt==zhefan)
		{
			yizhefan=1;
			cut1=x;
			cut2=zhefan;
		}
		else if(Cmp(nextpt,zhefan))
		{
			work2(nextpt);
		}
		else
		{
			yizhefan=1;
			cut1=x;
			cut2=nextpt;
		}
	}
	else
	{
		for(int i=1;i<=cnt[x];i++)
			if(!vis[cne[x][i]]) work2(cne[x][i]);
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m==n-1)
	{
		for(int i=1;i<=m;i++)
		{
			int u,v;
			scanf("%d%d",&u,&v);
			cne[u][++cnt[u]]=v;
			cne[v][++cnt[v]]=u;
		}
		work(1);
	}
	else
	{
		for(int i=1;i<=m;i++)
		{
			int u,v;
			scanf("%d%d",&u,&v);
			cne[u][++cnt[u]]=v;
			cne[v][++cnt[v]]=u;
			iodus[u]++;
			iodus[v]++;
		}
		for(int i=1;i<=n;i++)
			if(iodus[i]==1) dfs(i,0);
		work2(1);
		memset(vis,0,sizeof(vis));
		work3(1);
	}
	return 0;
}

