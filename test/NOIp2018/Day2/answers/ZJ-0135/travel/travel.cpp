#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;
const int N=5005;
int n,m,u,v,num,cnt,top,ct;
int head[N],point[N<<1],Next[N<<1];
int a[N],c[N],Ans[N],ANS[N],stack[N],vis[N];
bool mark[N][N];
vector<int> G[N];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x*f;
}
void add(int u,int v)
{
	point[++num]=v;
	Next[num]=head[u];
	head[u]=num;
}
void dfs(int now,int pre)
{
	Ans[++cnt]=now;
	G[now].clear();
	for (int i=head[now]; i; i=Next[i])
	if (!mark[now][point[i]])
	{
		int v=point[i];
		if (v==pre) continue;
		G[now].push_back(v);
	}
	int tot=G[now].size();
	for (int i=1; i<=tot; i++)
		a[i]=G[now][i-1];
	sort(a+1,a+tot+1);
	G[now].clear();
	for (int i=1; i<=tot; i++)
		G[now].push_back(a[i]);
	for (int i=0; i<tot; i++) 
		dfs(G[now][i],now);
}
void find(int now,int pre)
{
	stack[++top]=now;
	vis[now]=1;
	for (int i=head[now]; i; i=Next[i])
	{
		int v=point[i];
		if (v==pre) continue;
		if (vis[v])
		{
			int u=0;
			while (u!=v)
			{
				u=stack[top--];
				c[++ct]=u;
			}
		}
		else find(v,now);
		if (ct) return;
	}
	stack[top--]=0;
}
void dfs2(int now,int pre,int id)
{
	if (vis[now]) return;
	vis[now]=1;
	for (int i=head[now]; i; i=Next[i])
	{
		int v=point[i];
		if (v==pre) continue;
		if (v==c[id%ct+1]) 
		{
			mark[now][v]=mark[v][now]=1;
			cnt=0;
			dfs(1,0);
			for (int j=1; j<=n; j++)
				if (Ans[j]<ANS[j])
				{
					for (int k=1; k<=n; k++)
						ANS[k]=Ans[k];
					break;
				}
				else if (Ans[j]>ANS[j]) break;
			mark[now][v]=mark[v][now]=0;
			dfs2(v,now,id+1);
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(); m=read();
	for (int i=1; i<=m; i++)
	{
		u=read(); v=read();
		add(u,v); add(v,u);
	}
	if (m==n-1)
	{
		dfs(1,0);
		for (int i=1; i<n; i++) printf("%d ",Ans[i]);
		printf("%d\n",Ans[n]);
	}
	else
	{
		find(1,0);;
		for (int i=1; i<=n; i++) ANS[i]=n+1,vis[i]=0;
		dfs2(c[1],0,1);
		for (int i=1; i<n; i++) printf("%d ",ANS[i]);
		printf("%d\n",ANS[n]);
	}
	return 0;
}
