#include<cstdio>
#include<cstring>
using namespace std;
const int N=1e5+5;
int n,Q,u,v,ans,a,b,x,y,num;
int head[N],point[N<<1],Next[N<<1];
int cost[N],p[N];
char pd[10];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x*f;
}
void add(int u,int v)
{
	point[++num]=v;
	Next[num]=head[u];
	head[u]=num;
}
void dfs(int k,int s)
{
	if (s>=ans) return;
	if (k>n)
	{
		int flag;
		for (int i=1; i<=n; i++)
		{
			flag=1;
			for (int j=head[i]; j; j=Next[j])
			{
				int v=point[j];
				if (p[i]==1 || p[i]==3 || p[v]==1 || p[v]==3) flag=flag;
				else flag=0; 
			}
			if (!flag) return;
		}
		if (s<ans) ans=s;
		return;
	}
	if (!p[k])
	{
		p[k]=1;
		dfs(k+1,s+cost[k]);
		p[k]=0;
		dfs(k+1,s);
	}
	else
	{
		if (p[k]==3) dfs(k+1,s+cost[k]);
		else dfs(k+1,s);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(); Q=read(); scanf("%s",pd+1);
	for (int i=1; i<=n; i++) cost[i]=read();
	for (int i=1; i<=n-1; i++)
	{
		u=read(); v=read();
		add(u,v); add(v,u);
	}
	while (Q--)
	{
		memset(p,0,sizeof(p));
		a=read(); x=read(); b=read(); y=read();
		x+=2; y+=2;
		p[a]=x; p[b]=y;
		ans=1e9;
		dfs(1,0);
		if (ans==1e9) printf("-1\n");
		else printf("%d\n",ans);
	}
	return 0;
}
