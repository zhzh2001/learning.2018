#include <cstdio>
#include <iostream>
#include <algorithm>
#include <string>
#include <map>
using namespace std;
const int MAXN = 1e5+10;
const int MAXM = 1e5+10;
struct Edge{
	int u, v, w;
	bool operator<(Edge e)const{
		return w < e.w;
	}
};
map<int, Edge> edge;
struct Edges{
	int nxt, to, id;
}edges[MAXN*2];
int val, price[MAXN], head[MAXN], n, m, u, v, nume;
bool visit[MAXN];
string opt;
void add(int u, int v, int id){
	edges[++nume].nxt = head[u];
	edges[nume].to = v;
	edges[nume].id = id;
	head[u] = nume;
}
int main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d%s", &n, &m, &opt);
	for(int i = 1; i <= n; i++)
		scanf("%d", &price[i]);
	for(int i = 1; i <= n-1; i++){
		scanf("%d%d", &u, &v);
		add(u, v, i);
		add(v, u, i);
		edge[i] = (Edge){u, v, min(price[u], price[v])};
	}
	cerr << "��ѩ�������¹\nЦ�������б�ԧ" << endl;
	return 0;
} 
