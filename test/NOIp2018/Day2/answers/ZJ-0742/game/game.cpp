#include <cstdio>
#include <iostream>
using namespace std;
const int MOD = 1e9 + 7;
int n, m;
long long quickPow(int a, int p){
	long long ans = 1;
	long long base = a;
	while(p > 0){
		if(p & 1){
			ans *= base;
			ans %= MOD;
		}
		base *= base;
		base %= MOD;
		p >>= 1;
	}
	return ans%MOD;
}
int main(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if(n == 2)
		printf("%d", (4 * quickPow(3, m-1) % MOD) % MOD);
	if(n == 1)
		printf("%d", (quickPow(2, m)) % MOD);
	if(n == 3 && m == 2)
		printf("36");
	if(n == 3 && m == 3)
		printf("112");
		
	return 0;
}
