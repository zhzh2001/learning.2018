#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
#define ll long long
ll n,m,a[10][1000000],ans;
string f1[10][10],f2[10][10],cc[2];
ll p=1000000007;
void dfs(ll x,ll y)
{
	if(x==n&&y==m)
	{
		ll ff=1;
		f1[n][m]=f2[n][m]="";
		for(ll i=n;i>=1&&ff;i--)
			for(ll j=m;j>=1;j--)
			if(i!=n||j!=m)
			{
				if(i==n)
					f1[i][j]=f1[i][j+1],f2[i][j]=f2[i][j+1];
				else
				if(j==m)
					f1[i][j]=f1[i+1][j],f2[i][j]=f2[i+1][j];
				else
				{
					f1[i][j]=max(f1[i+1][j],f1[i][j+1]),f2[i][j]=min(f2[i+1][j],f2[i][j+1]);
					if(f2[i+1][j]<f1[i][j+1])
					{
						ff=0;
						break;
					}
				}
				f1[i][j]=cc[a[i][j]]+f1[i][j];
				f2[i][j]=cc[a[i][j]]+f2[i][j];
			}
		ans+=ff;
	}
	else
	{
		for(ll i=0;i<2;i++)
		{
			a[x][y]=i;
			if(y==m)	dfs(x+1,1);
			else	dfs(x,y+1);
		}
	}
}
ll pow(ll x,ll y)
{
	ll kk=1;
	for(ll i=1;i<=y;i<<=1)
	{
		if(i&y)	kk=(kk*x)%p;
		x=(x*x)%p;
	}
	return kk;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if(n>m)	swap(n,m);
	if(n<=3)
	{
		if(n==3)
			ans=112*pow(3,m-3)%p;
		else
		if(n==2)
			ans=4*pow(3,m-1)%p;
		else
			ans=pow(2,m);
	}
	else
	{
		cc[0]="0";
		cc[1]="1";
		if(n==5&&m==5)	ans=7136;
		else	if(n==4&&m==5)	ans=2688;
		else	dfs(1,2),ans=ans*4%p;
	}
	printf("%lld\n",ans);
	return 0;
}
