#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
using namespace std;
#define ll long long
#define mn 100005
ll n,m,p[mn],f1[mn],f0[mn],ty2,d[mn],a,b,x,i,y,inf=90000000000;
char ty1,kg;
vector<ll>	g[mn];
struct node
{
	ll x,y,a,b,ans;
}q[mn];
ll dfs(ll u)
{
	for(ll i=0;i<g[u].size();i++)
		if(!d[g[u][i]])
		{
			d[g[u][i]]=d[u]+1,dfs(g[u][i]);
		}
}
ll dfs1(ll u)
{
	f1[u]=p[u],f0[u]=0;
	for(ll i=0;i<g[u].size();i++)
		if(d[g[u][i]]==d[u]+1)
		{
			ll v=g[u][i];
			dfs1(v);
			f1[u]+=min(f1[v],f0[v]);
			f0[u]+=f1[v];
		}
	if(u==x)
	{
		if(a)
			f0[u]=inf;
		else
			f1[u]=inf;
	}
	if(u==y)
	{
		if(b)
			f0[u]=inf;
		else
			f1[u]=inf;
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%lld%lld%c%c%lld",&n,&m,&kg,&ty1,&ty2);
	for(i=1;i<=n;i++)	
		scanf("%lld",&p[i]);
	g[0].push_back(1);
	for(i=1;i<n;i++)
	{
		scanf("%lld%lld",&x,&y);
		g[x].push_back(y),g[y].push_back(x);
	}
	d[0]=1,dfs(0);
	for(i=1;i<=m;i++)
	{
		scanf("%lld%lld%lld%lld",&q[i].x,&q[i].a,&q[i].y,&q[i].b);
		x=q[i].x,a=q[i].a,y=q[i].y,b=q[i].b;
		dfs1(0);
		q[i].ans=min(f0[0],f1[0]);
	}
	for(i=1;i<=m;i++)
	{
		if(q[i].ans>=inf)	q[i].ans=-1;
		printf("%lld\n",q[i].ans);		
	}
	return 0;
}
