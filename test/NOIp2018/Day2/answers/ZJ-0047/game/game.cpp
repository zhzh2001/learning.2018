#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int read()
{
	int x=0,w=0;char c=0;
	while(c<'0' || c>'9') w|=(c=='-'),c=getchar();
	while(c>='0' && c<='9') x=x*10+c-'0',c=getchar();
	return w?-x:x;
}
void write(int x)
{
	if(x<0) putchar('-');
	if(x==0) {putchar('0');return;}
	if(x/10>0) write(x/10);
	putchar('0'+x%10);
}
const int mod=1e9+7;
int n,m,f[10][10];
int a[10],b[10],ans=0;
int dp[3][1000010];
bool check(int x,int y)
{
	for(int i=1;i<=n;i++)
		a[n-i+1]=x%2,x/=2;
	for(int i=1;i<=n;i++)
		b[n-i+1]=y%2,y/=2;
	for(int i=2;i<=n;i++)
		if(a[i-1]==1 && b[i]==0) return 0;
	return 1;
}
void dfs(int dep)
{
	if(dep==m) return;
	memset(dp[(dep+1)%2],0,sizeof(dp[(dep+1)%2]));
	for(int i=0;i<(1<<n);i++)
	{
		for(int j=0;j<(1<<n);j++)
		{
			if(!check(i,j)) continue;
			dp[(dep+1)%2][j]=(dp[(dep+1)%2][j]+dp[dep%2][i])%mod;
		}
	}
	dfs(dep+1);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	memset(f,0,sizeof(f));
	n=read(),m=read();
	for(int i=0;i<(1<<n);i++)
		dp[1][i]=1;
	dfs(1);
	int sum=0;
	for(int i=0;i<(1<<n);i++)
		sum=(sum+dp[m%2][i])%mod;
	write(sum);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
