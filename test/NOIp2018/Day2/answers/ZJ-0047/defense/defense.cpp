#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define int long long
using namespace std;
int read()
{
	int x=0,w=0;char c=0;
	while(c<'0' || c>'9') w|=(c=='-'),c=getchar();
	while(c>='0' && c<='9') x=x*10+c-'0',c=getchar();
	return w?-x:x;
}
void write(int x)
{
	if(x<0) putchar('-'),x=-x;
	if(x==0) {putchar('0');return;}
	if(x/10>0) write(x/10);
	putchar('0'+x%10);
}
char tmp[10];
int n,m,cnt=0;
int p[100010],col[100010];
int head[200010],nxt[200010],to[200010];
void addedge(int x,int y)
{
	cnt++;
	nxt[cnt]=head[x];
	head[x]=cnt;
	to[cnt]=y;
}
int dfs(int u,int fa)
{
	int res=0;
	if(col[u]) res+=p[u];
	for(int i=head[u];i!=-1;i=nxt[i])
	{
		int tmp=0x7fffffff,k;
		int v=to[i];
		if(v==fa) continue;
		if(col[v]!=-1)
		{
			if(!col[v] && !col[u]) return -1;
			k=dfs(v,u);
			if(k!=-1) tmp=min(k,tmp);
		}
		else
		{
			if(!col[u])
			{
				col[v]=1;k=dfs(v,u);
				if(k!=-1) tmp=min(tmp,k);
			}
			else
			{
				col[v]=0;k=dfs(v,u);
				if(k!=-1) tmp=min(tmp,k);
				col[v]=1;k=dfs(v,u);
				if(k!=-1) tmp=min(tmp,k);
			}
		}
		if(tmp==0x7fffff) return -1;
		res+=tmp;
	}
	return res;
}
signed main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof(head));
	n=read(),m=read();
	scanf("%s",tmp);
	for(int i=1;i<=n;i++)
		p[i]=read();
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read();
		addedge(x,y);
		addedge(y,x);
	}
	while(m--)
	{
		memset(col,-1,sizeof(col));
		int a=read(),b=read(),c=read(),d=read();
		if(!b && !d)
		{
			int flag=1;
			for(int i=head[a];i!=-1;i=nxt[i])
			{
				int v=to[i];
				if(v==c) {write(-1);puts("");flag=0;break;}
			}
			if(!flag) continue;
		}
		col[a]=b,col[c]=d;
		write(dfs(a,0));puts("");
	}
//	fclose(stdin);
//	fclose(stdout);
	return 0;
}
