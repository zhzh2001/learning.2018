#include<vector>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int read()
{
	int x=0,w=0;char c=0;
	while(c<'0' || c>'9') w|=(c=='-'),c=getchar();
	while(c>='0' && c<='9') x=x*10+c-'0',c=getchar();
	return w?-x:x;
}
void write(int x)
{
	if(x<0) putchar('-');
	if(x==0) {putchar('0');return;}
	if(x/10>0) write(x/10);
	putchar('0'+x%10);
}
int n,m,cnt=0;
int q[40010];
int f[5010],dep[5010];
int in[5010],ans[5010];
vector <int> to[5010];
void bfs()
{
	int l=0,r=1;
	q[1]=1,dep[1]=1,in[1]=1;
	while(l<r)
	{
		int u=q[++l];
		for(int i=0;i<to[u].size();i++)
		{
			int v=to[u][i];
			if(!dep[v]) dep[v]=dep[u]+1,q[++r]=v,in[v]++;
			else if(dep[v]>=dep[u]+1) in[v]++;
		}
	}
}
void dfs(int u,int fa)
{
	ans[++cnt]=u;f[u]=2;
	sort(to[u].begin(),to[u].end());
	int tmp=0x7ffff;
	for(int i=1;i<=n;i++)
		if(f[i]==1) tmp=min(tmp,i);
	for(int i=0;i<to[u].size();i++)
	{
		int v=to[u][i];
		if(f[v]==2) continue;
		in[v]--,f[v]=1;
	}
	for(int i=0;i<to[u].size();i++)
	{
		int v=to[u][i];
		if(f[v]==2) continue;
		if(v<tmp) dfs(v,u);
		else if(!in[v]) dfs(v,u);
	}	
}	
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++)
	{
		int x=read(),y=read();
		to[x].push_back(y);
		to[y].push_back(x);
	}
	bfs();
	dfs(1,0);
	for(int i=1;i<=n;i++)
		write(ans[i]),putchar(' ');
	fclose(stdin);
	fclose(stdout);
	return 0;
}
