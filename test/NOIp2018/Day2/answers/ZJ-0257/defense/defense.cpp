#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
const int N=100004;
typedef long long ll;
char s[10];
ll f[N][2];
int p[N];
int n,m,a,sa,b,sb,head[N],nex[N<<1],to[N<<1],cnt;
inline void read(int &x){
	char c=getchar();x=0;int flg=1;
	for(;!isdigit(c);c=getchar())if(c=='-')flg=-1;
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+c-'0';
	x*=flg;
}
inline void addedge(int u,int v){
	nex[++cnt]=head[u],head[u]=cnt,to[cnt]=v;
}
inline void solve(int x,int fa){
	if(x==a)f[a][sa]=(sa==1)?p[a]:0,f[a][1-sa]=-1;else if(x==b)f[b][sb]=(sb==1)?p[b]:0,f[b][1-sb]=-1;else f[x][1]=p[x],f[x][0]=0;
	for(int i=head[x];i;i=nex[i])if(to[i]!=fa){
		solve(to[i],x);
		if(f[x][1]!=-1){
			if(f[to[i]][0]==-1 && f[to[i]][1]==-1)f[x][1]=f[x][0]=-1;
			else if(f[to[i]][0]==-1)f[x][1]+=f[to[i]][1];
			else if(f[to[i]][1]==-1)f[x][1]+=f[to[i]][0];
			else f[x][1]+=min(f[to[i]][0],f[to[i]][1]);
		}
		if(f[x][0]!=-1){
			if(f[to[i]][1]==-1)f[x][0]=-1;else f[x][0]+=f[to[i]][1];
		}
	}
	return;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n),read(m);scanf("%s",s);
	for(int i=1;i<=n;i++)read(p[i]);
	for(int u,v,i=1;i<n;i++)read(u),read(v),addedge(u,v),addedge(v,u);
	for(;m;m--){
		read(a),read(sa),read(b),read(sb);
		if(a==b&&sa!=sb)puts("-1");else{
			solve(1,0);
			if(f[1][0]==-1 && f[1][1]==-1)puts("-1");
			else if(f[1][0]==-1)printf("%lld\n",f[1][1]);
			else if(f[1][1]==-1)printf("%lld\n",f[1][0]);
			else printf("%lld\n",min(f[1][0],f[1][1]));
		}
	}
	return 0;
}
