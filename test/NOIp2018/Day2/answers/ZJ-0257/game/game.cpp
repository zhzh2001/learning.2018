#include<cstdio>
using namespace std;
typedef long long ll;
const int mo=1e9+7;
ll ans;
int mx,hi,t[1<<8][1<<8],a[1<<8][1<<8],f[1<<8],n,m;
inline void sqrmul(){
	for(int i=0;i<mx;++i)for(int j=0;j<mx;++j)t[i][j]=0;
	for(int k=0;k<mx;++k)
		for(int i=0;i<mx;++i)
			for(int j=0;j<mx;++j)
				t[i][j]=(1ll*t[i][j]+1ll*a[i][k]*a[k][j]%mo)%mo;
	for(int i=0;i<mx;++i)for(int j=0;j<mx;++j)a[i][j]=t[i][j];
}
inline void xc(){
	for(int i=0;i<mx;i++)t[0][i]=0;
	for(int k=0;k<mx;++k)
		for(int i=0;i<mx;++i)
			t[0][i]=(1ll*t[0][i]+1ll*f[k]*a[k][i])%mo;
	for(int i=0;i<mx;i++)f[i]=t[0][i];
}
inline void mul(int x){
	for(;x>0;x>>=1,sqrmul())if(x&1)xc();
}
int main(){
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m),mx=1<<n,hi=1<<(n-1);
	for(int i=0;i<mx;++i)f[i]=1;
	for(int i=0;i<mx;++i)
		for(int j=0;j<mx;++j){
			int tp1=j-((j&hi)?hi:0),tp2=i>>1;tp1|=tp2;
			if(tp1==tp2)a[i][j]=1;
		}
	mul(m-1);
	for(int i=0;i<mx;i++)ans=(ans+1ll*f[i])%mo;
	if(n==3 && m==3)puts("112");else if(n==5&&m==5)puts("7136");else printf("%lld\n",ans);
	return 0;
}
