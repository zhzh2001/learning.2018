#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
inline void read(int &x){
	char c=getchar();x=0;
	for(;!isdigit(c);c=getchar());
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+c-'0';
}
const int N=5005;
bool vis[N];
int n,m,head[N],nex[N<<1],to[N<<1],cnt,q[N],top;
inline void addedge(int u,int v){
	nex[++cnt]=head[u],head[u]=cnt,to[cnt]=v;
}
inline void dfs(int x,int fa){
	int t[N],tot=0;q[++top]=x,vis[x]=1;
	for(int i=head[x];i;i=nex[i])if(!vis[to[i]] && to[i]!=fa)t[++tot]=to[i];
	sort(t+1,t+tot+1);
	for(int i=1;i<=tot;i++)if(!vis[t[i]])dfs(t[i],x);
	return;
}
int main(){
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	read(n),read(m);
	for(int u,v,i=1;i<=m;i++)read(u),read(v),addedge(u,v),addedge(v,u);
	dfs(1,0);
	for(int i=1;i<=top;i++)printf("%d ",q[i]);
	return 0;
}
