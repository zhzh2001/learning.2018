#include <stdio.h>
#include <assert.h>
#include <setjmp.h>
#include <algorithm>
#include <vector>
#include <bitset>
using namespace std;

const int N = 5001;
vector<int> a[N];
vector<int> c[N];
vector<int> ans;
vector<int> record;
bitset<N> vis;
jmp_buf mn;

static void dfs_pre(const int cur, const int par=0) {
	if (vis.test(cur)) {
		const int li = find(record.begin(), record.end(), cur) - record.begin() + 1;
		for (int i = li; i < record.size(); ++i) {
			if (record[i] > record.back()) {
				{
					vector<int> &ll = c[record[i - 1]];
					ll.erase(find(ll.begin(), ll.end(), record[i]));
				}
				{
					vector<int> &ll = c[record[i]];
					ll.erase(find(ll.begin(), ll.end(), record[i - 1]));
				}
				longjmp(mn, 1);
			}
		}
		assert(0);
	}
	record.push_back(cur);
	vis.set(cur);
	for (int i = 0; i < c[cur].size(); ++i) {
		const int v = c[cur][i];
		if (v == par) continue;
		dfs_pre(v, cur);
	}
	record.pop_back();
	vis.reset(cur);
}

static void dfs(const int cur, const int par=0) {
	for (int i = 0; i < c[cur].size(); ++i) {
		const int v = c[cur][i];
		if (par == v) continue;
		a[cur].push_back(v);
		dfs(v, cur);
	}
	sort(a[cur].begin(), a[cur].end());
}

static void dfs_ans(const int cur) {
	ans.push_back(cur);
	for (int i = 0; i < a[cur].size(); ++i) {
		const int v = a[cur][i];
		dfs_ans(v);
	}
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	int n, m;
	scanf("%d%d", &n, &m);
	for (int i = 0; i < m; ++i) {
		int a, b;
		scanf("%d%d", &a, &b);
		c[a].push_back(b);
		c[b].push_back(a);
	}
	if (setjmp(mn) == 0) {
		dfs_pre(1);
	}
	dfs(1);
	dfs_ans(1);
	for (int i = 0; i < ans.size(); ++i) {
		printf("%d ", ans[i]);
	}
	putchar('\n');
	return 0;
}
