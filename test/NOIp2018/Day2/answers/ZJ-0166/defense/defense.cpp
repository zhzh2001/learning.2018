#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <vector>
using namespace std;

const int N = 100001;
int n, m;
int v[N];
vector<int> es[N];
int l[N];
int r[N];
int q[N];
int f[N][2];

static void build_tree(const int cur, const int par=0) {
	vector<int> children;
	for (int i = 0; i < es[cur].size(); ++i) {
		const int v = es[cur][i];
		if (v == par) continue;
		children.push_back(v);
	}
	if (children.empty()) return;
	int pch = l[cur] = children[0];
	for (int i = 1; i < children.size(); ++i) {
		pch = r[pch] = children[i];
	}
	for (int i = 0; i < children.size(); ++i)
		build_tree(children[i], cur);
}

static int dp(const int cur, const int put) {
	if (cur == 0) return 0;
	if (f[cur][put] != -1) {
		return f[cur][put];
	}
	const int qq = q[cur] == 0 ? -1 : q[cur] == 1 ? 0 : 1;
	int &ans = f[cur][put];
	if (put == 1) {
		ans = qq == -1 || qq == 1 ? min(dp(l[cur], 0) + dp(r[cur], 1), dp(l[cur], 1) + dp(r[cur], 1)) + v[cur] : 0x3f3f3f3f;
	} else {
		ans = min(qq == -1 || qq == 0 ? min(dp(l[cur], 1) + dp(r[cur], 0), dp(l[cur], 1) + dp(r[cur], 1)) : 0x3f3f3f3f, qq == -1 || qq == 1 ? dp(l[cur], 0) + dp(r[cur], 0) + v[cur] : 0x3f3f3f3f);
	}
	return ans;
}

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d%*s", &n, &m);
	for (int i = 1; i <= n; ++i)
		scanf("%d", v + i);
	for (int i = 1; i <= n - 1; ++i) {
		int a, b;
		scanf("%d%d", &a, &b);
		es[a].push_back(b);
		es[b].push_back(a);
	}
	build_tree(1);
	for (int i = 0; i < m; ++i) {
		int a, x, b, y;
		scanf("%d%d%d%d", &a, &x, &b, &y);
		if (x == 0 && y == 0 && find(es[a].begin(), es[a].end(), b) != es[a].end()) {
			puts("-1");
			continue;
		}
		q[a] = x + 1;
		q[b] = y + 1;
		memset(f, -1, sizeof(f));
		const int ans = min(dp(1, 0), dp(1, 1));
		q[a] = q[b] = 0;
		printf("%d\n", ans);
	}
}
