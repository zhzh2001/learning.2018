#include <stdio.h>
#include <algorithm>
using namespace std;

int n, m;
const int N = 9;
const int M = 9;
int mp[N][M];

static bool check() {
	return true;
}

static int play(const int i, const int j) {
	int ans = 0;
	for (int k = 0; k < 2; ++k) {
		mp[i][j] = k;
		if (i == n && j == m) ans += check();
		else if (j == m) ans += play(i + 1, 1);
		else ans += play(i, j + 1);
	}
	return ans;
}

int main() {
	scanf("%d%d", &n, &m);
	printf("%d\n", play(1, 1));
	return 0;
}
