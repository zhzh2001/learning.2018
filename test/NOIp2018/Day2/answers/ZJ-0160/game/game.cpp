#include <bits/stdc++.h>
using namespace std;

#define rep(i,a,n) for (int i=(a);i<(n);i++)
#define per(i,a,n) for (int i=(a)-1;i>=(n);i--)
#define SZ(x) ((int)x.size())
#define all(x) x.begin(),x.end()
#define pb push_back
#define mp make_pair
#define fi first
#define se second

typedef long long ll;
typedef double db;
typedef pair<int,int> PII;
typedef vector<int> VI;

const int mod=1e9+7;
#define N 9
int n,m;

namespace P_1 {
	int A[4][4];
	string str[1000];
	int cnt,ans,clk;
	
	void dfs1(int x,int y,int ct,string S) {
		if (A[x][y]==1) S=S+'1';
		else S=S+'0';
		if (ct==n+m-1) {
			if (A[x][y]) str[++cnt]=S;
			else str[++cnt]=S;
			return;
		}
		if (y+1<=m) dfs1(x,y+1,ct+1,S);
		if (x+1<=n) dfs1(x+1,y,ct+1,S);
	}
	void check() {
		cnt=0;
		string T="";
		dfs1(1,1,1,T);
		bool flag=1;
//		for (int i=1;i<=cnt;i++) {
//			cout<<str[i]<<endl;	
//		}
		//puts("~~~~");
		clk++;
		for (int i=1;i<=cnt;i++) for (int j=i+1;j<=cnt;j++) {
			if (str[i]>str[j]) {flag=0; break;}
		}
		if (flag) ans++;
	}
	void dfs(int x,int y) {
		if (y==m+1) y=1,x=x+1;
		if (x==n+1) {
			check();
			return;
		}
		A[x][y]=1;
		dfs(x,y+1);
		A[x][y]=0;
		dfs(x,y+1);
	}
	void solve() {
		ans=clk=0;
		dfs(1,1);
		printf("%d\n",ans);
	}	
}

namespace P_2 {
	#define M 1000005
	ll pow_mod(ll x,ll k) {
		ll res=1;
		while (k) {
			if (k&1) res=res*x%mod;
			k>>=1;
			x=x*x%mod;
		}
		return res;
	}
	void solve() {
		ll ans=pow_mod(3,n-1)*4%mod;
		printf("%lld\n",ans);
	}
}

int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (0);
	else if (n<=3&&m<=3) P_1::solve();
	else P_2::solve();
	return 0;	
}
