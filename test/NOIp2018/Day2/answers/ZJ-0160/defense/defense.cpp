#include <bits/stdc++.h>
using namespace std;

#define rep(i,a,n) for (int i=(a);i<(n);i++)
#define per(i,a,n) for (int i=(a)-1;i>=(n);i--)
#define SZ(x) ((int)x.size())
#define all(x) x.begin(),x.end()
#define pb push_back
#define mp make_pair
#define fi first
#define se second

typedef long long ll;
typedef double db;
typedef pair<int,int> PII;
typedef vector<int> VI;

#define N 100005
#define inf 1e17
int n,m;
char str[20];
int head[N],nxt[N<<1],to[N<<1],tol;
int val[N];

void add_edge(int u,int v) {
	nxt[++tol]=head[u]; head[u]=tol; to[tol]=v;	
}

namespace P_1 {
	ll dp[N][2];
	int id[N];
	int a,b,x,y;
	int fa[N],child[N];
	
	void dfs1(int u,int f) {
		fa[u]=f;
		child[u]=1;
		for (int i=head[u];i;i=nxt[i]) {
			int v=to[i];
			if (v==f) continue;
			child[u]=0;
			dfs1(v,u);	
		}
	}
	void dfs(int u,int f) {
		dp[u][0]=0,dp[u][1]=val[u];
		ll sum0=0,sum1=0,mi=inf;
		bool flag1=0,flag=0;
		for (int i=head[u];i;i=nxt[i]) {
			int v=to[i];
			if (v==f) continue;
			dfs(v,u);
			if (id[v]==-1) {
				if (child[v]) {
					flag=1;
					sum0+=dp[v][1];
				}else {
					sum1+=min(dp[v][0],dp[v][1]);
					if (dp[v][1]>=dp[v][0]) {
						flag=1;
						sum0+=dp[v][1];
					}else {
						sum0+=dp[v][0];
						mi=min(mi,dp[v][1]-dp[v][0]);
					}
				}
			}else if (id[v]==1) {
				flag=1;
				sum1+=dp[v][1];
				sum0+=dp[v][1];
			}else {
				if (child[v]) {
					sum1+=0;
					flag1=1;
				}else {
					sum1+=dp[v][0];
					if (dp[v][0]>=inf) flag1=1;
					sum0+=dp[v][0];
				}
			}
		}
		if (flag1||id[u]==1) dp[u][0]=inf;
		else if (flag||child[u]) dp[u][0]=sum0;
		else dp[u][0]=sum0+mi;
		
		dp[u][1]+=sum1;
		if (id[u]==0) dp[u][1]=inf;
		//printf("u=%d id=%d sum0=%lld dp[u][0]=%lld dp[u][1]=%lld\n",u,id[u],sum0,dp[u][0],dp[u][1]);
	}
	void solve() {
		memset(id,-1,sizeof(id));
		dfs1(1,0);
		while (m--) {
			scanf("%d%d%d%d",&a,&x,&b,&y);
			id[a]=x,id[b]=y;
			dfs(1,0);
			ll ans=min(dp[1][0],dp[1][1]);
			id[a]=id[b]=-1;
			printf("%lld\n",ans>=inf?-1:ans);
		}
	}	
}

int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,str);
	rep(i,1,n+1) scanf("%d",&val[i]);
	rep(i,1,n) {
		int u,v;
		scanf("%d%d",&u,&v);
		add_edge(u,v);
		add_edge(v,u);
	}
	P_1::solve();
	return 0;	
}
