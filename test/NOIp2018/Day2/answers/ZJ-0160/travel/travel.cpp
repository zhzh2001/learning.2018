#include <bits/stdc++.h>
using namespace std;

#define rep(i,a,n) for (int i=(a);i<(n);i++)
#define per(i,a,n) for (int i=(a)-1;i>=(n);i--)
#define SZ(x) ((int)x.size())
#define all(x) x.begin(),x.end()
#define pb push_back
#define mp make_pair
#define fi first
#define se second

typedef long long ll;
typedef double db;
typedef pair<int,int> PII;
typedef vector<int> VI;

#define N 5005
int n,m;
int head[N],nxt[N<<1],to[N<<1],tol;

void add_edge(int u,int v) {
	nxt[++tol]=head[u];
	head[u]=tol;
	to[tol]=v;
}

namespace P_1 {
	VI e[N];
	int ans[N],cnt;

	void dfs(int u,int f) {
		ans[++cnt]=u;
		rep(i,0,SZ(e[u])) {
			int v=e[u][i];
			if (v==f) continue;
			dfs(v,u);
		}
	}
	void solve() {
		rep(i,1,m+1) {
			int u,v;
			scanf("%d%d",&u,&v);
			e[u].pb(v);
			e[v].pb(u);
		}
		rep(i,1,n+1) sort(all(e[i]));
		dfs(1,0);
		rep(i,1,cnt+1) printf("%d%c",ans[i]," \n"[i==cnt]);
	}
}

namespace P_2 {
	VI e[N];
	int ans[N],cnt;
	bool vis[N];

	void dfs(int u,int f) {
		ans[++cnt]=u;
		vis[u]=1;
		rep(i,0,SZ(e[u])) {
			int v=e[u][i];
			if (v==f||vis[v]) continue;
			dfs(v,u);
		}
	}
	void solve() {
		rep(i,1,m+1) {
			int u,v;
			scanf("%d%d",&u,&v);
			e[u].pb(v);
			e[v].pb(u);
		}
		rep(i,1,n+1) sort(all(e[i]));
		dfs(1,0);
		rep(i,1,cnt+1) printf("%d%c",ans[i]," \n"[i==cnt]);
	}
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m==n-1) P_1::solve();
	else P_2::solve();
	return 0;
}
