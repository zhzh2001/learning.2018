#include<bits/stdc++.h>
using namespace std;
int n,m,f[5010],u,v,cu[5010],cv[5010],cnt,nw,vis[5010],dep[5010],fa[5010],ans[5010],tans[5010],sz;
vector<int>e[5010];
int getf(int x){
	if(f[x]==x)return x;
	return f[x]=getf(f[x]);
}
void dfs_ans(int x){
	vis[x]=1;
	printf("%d ",x);
	for(int i=0;i<e[x].size();i++){
		if(!vis[e[x][i]])dfs_ans(e[x][i]);
	}
}
bool ct(int x,int y){
	return (x!=cu[nw]||y!=cv[nw])&&(x!=cv[nw]||y!=cu[nw]);
}
void Getd(int x){
	//cout<<"GETD "<<x<<"\n";
	vis[x]=1;
	for(int i=0;i<e[x].size();i++){
		if(!vis[e[x][i]]&&ct(x,e[x][i])){
			dep[e[x][i]]=dep[x]+1;
			Getd(e[x][i]);
		}else if(vis[e[x][i]]){
			fa[x]=e[x][i];
		}
	}
	vis[x]=0;
}
void Findc(int x,int y){
	while(x!=y){
		if(dep[x]<dep[y])swap(x,y);
		cu[++cnt]=fa[x];
		cv[ cnt ]=x;
		x=fa[x];
	}
}
void dfs(int x){
	tans[++sz]=x;
	vis[x]=1;
	for(int i=0;i<e[x].size();i++){
		if(!vis[e[x][i]]&&ct(x,e[x][i])){
			//cout<<"FIND "<<x<<"->"<<e[x][i]<<"\n";
			dfs(e[x][i]);
		}
	}
	vis[x]=0;
}
void Upd(){
	for(int i=1;i<=n;i++){
		if(ans[i]<tans[i])return;
		else if(ans[i]>tans[i]){
			memcpy(ans,tans,sizeof(tans));
		}
	}
	return;
}int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)f[i]=i;
	for(int i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		if(getf(u)==getf(v)){
			cu[++cnt]=u;
			cv[ cnt ]=v;
		}else{
			f[f[u]]=f[v];
		}
		e[u].push_back(v);
		e[v].push_back(u);
	}
	for(int i=1;i<=n;i++)sort(e[i].begin(),e[i].end());
	if(!cu&&!cv){
		dfs_ans(1);
	}else{
		nw=1;
		dep[1]=1;
		Getd(1);
		Findc(cu[nw],cv[nw]);
		dfs(1);
		memcpy(ans,tans,sizeof(tans));
		//cout<<"DEL["<<cu[nw]<<","<<cv[nw]<<"]\n";
		for(nw=2;nw<=cnt;nw++){
			//cout<<"DEL["<<cu[nw]<<","<<cv[nw]<<"]\n";
			sz=0;
			dfs(1);
			/*cout<<"TANS:";
			for(int i=1;i<=n;i++){
				cout<<tans[i]<<" ";
			}
			puts("");*/
			Upd();
		}
		for(int i=1;i<=n;i++){
			printf("%d ",ans[i]);
		}
	}
	return 0;
}
