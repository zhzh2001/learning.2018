#include<bits/stdc++.h>
using namespace std;
int n,m,u,v,p[100010],a,b,vis[100010];
long long dp[100010][3];
char op1,op2;
void getc(char &x){
	x=getchar();
	while(!isdigit(x)&&!isalpha(x))x=getchar();
}
vector<int>e[2010];
void Merge(int x,int y){
	dp[x][1]+=dp[y][0];
	dp[x][0]+=min(dp[y][0],dp[y][1]);
}
void TDP(int x){
	vis[x]=1;
	if((a==x&&u==0)||(b==x&&v==0))dp[x][0]=1e18;
	else dp[x][0]=p[x];
	dp[x][1]=0;
	for(int i=0;i<e[x].size();i++){
		if(!vis[e[x][i]]){
			TDP(e[x][i]);
			Merge(x,e[x][i]);
		}
	}
	if((a==x&&u==1)||(b==x&&v==1))dp[x][1]=1e9;
	vis[x]=0;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	getc(op1);
	getc(op2);
	for(int i=1;i<=n;i++)scanf("%d",&p[i]);
	for(int i=1;i<n;i++){
		scanf("%d%d",&u,&v);
		e[u].push_back(v);
		e[v].push_back(u);
	}
	while(m--){
		scanf("%d%d%d%d",&a,&u,&b,&v);
		TDP(1);
		printf("%lld\n",min(dp[1][0],dp[1][1])>=1e18?-1:min(dp[1][0],dp[1][1]));
	}
	return 0;
}
