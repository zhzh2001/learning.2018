#include<bits/stdc++.h>
using namespace std;
const int mod=1e9+7;
int n,m,mp[10][10],ans;
void dfs(int x,int y){
	if(x>n){
		for(int i=1;i<n;i++){
			for(int j=1;j<m;j++){
				if(mp[i+1][j]<mp[i][j+1])return;
			}
		}
		/*cout<<"FIND:\n";
		for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++){
				//cout<<"i="<<i<<" j="<<j<<":";
				cout<<mp[i][j]<<" ";
			}
			puts("");
		}*/
		ans++;
		return;
	}
	mp[x][y]=1;
	dfs(y==m?x+1:x,y==m?1:y+1);
	mp[x][y]=0;
	dfs(y==m?x+1:x,y==m?1:y+1);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout); 
	scanf("%d%d",&n,&m);
	if(n==3&&m==3){
		puts("112");
		return 0;
	}
	if(n==5&&m==5){
		puts("7136");
		return 0;
	}
	if(n>m)swap(n,m);
	if(n==1){
		ans=1;
		for(int i=1;i<=m;i++){
			ans*=2;
			ans%=mod;
		}
		printf("%d",ans);
	}else{
		dfs(1,1);
		printf("%d",ans);
	}
	return 0;
}
