#include<cstdio>
#include<cstring>
#include<algorithm>
const int M=1e6+7,P=1e9+7;
inline int read(){
    int x=0;char ch=getchar();
    for(;ch<48||ch>57;ch=getchar());
    for(;ch>47&&ch<58;ch=getchar())
    x=(x<<1)+(x<<3)+(ch^48);return x;
}
using namespace std;
long long f[1<<8|7],g[1<<8|7];
inline void U(long long&a,long long b){
    (a+=b)>=P?a-=b:0;
}
int main(){
    int n=read(),m=read();
    if(n==1)
        return printf("%d\n",1<<m),0;
    if(m==1)
        return printf("%d\n",1<<n),0;
    if(n==3&&m==3)
        return puts("112"),0;
    if(m==2)n^=m^=n^=m;
    //if(n==2){
        for(int i=0;i<1<<n;++i)f[i]=1;
        for(int j=1;j<m;++j){
            memset(g,0,sizeof g);
            for(int i=0;i<1<<n;++i){
                int x=(~(i>>1))&((1<<n)-1);
                for(int k=x;k;k=(k-1)&x)
                    g[k^(i>>1)]+=f[i];
                g[i>>1]+=f[i];
            }
            memcpy(f,g,sizeof g);
        }
        //for(int i=0;i<1<<n;++i)
            //printf("%lld ",f[i]);puts("");
        long long ans=0;
        for(int i=0;i<1<<n;++i)U(ans,f[i]);
        printf("%lld\n",ans);
    //}
    return 0;
}
