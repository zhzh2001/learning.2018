#include<cstdio>
#include<algorithm>
#define N 5005
inline int read(){
    int x=0;char ch=getchar();
    for(;ch<48||ch>57;ch=getchar());
    for(;ch>47&&ch<58;ch=getchar())
    x=(x<<1)+(x<<3)+(ch^48);return x;
}
using namespace std;
int arr[N<<1],nxt[N<<1],fir[N],vis[N],ans[N],tot,e;
void ADD(int u,int v){
    arr[++e]=v;nxt[e]=fir[u];fir[u]=e;
}
void DFS(int u,int fa){
    ans[++tot]=u,vis[u]=1;int cnt=0;
    for(int i=fir[u];i;i=nxt[i])
        if(arr[i]^fa)++cnt;
    for(int j=0;j<cnt;++j){
        int mxn=1e9;
        for(int i=fir[u];i;i=nxt[i]){
            int v=arr[i];
            if(!vis[v]&&v!=fa)
                mxn=min(mxn,v);
        }
        DFS(mxn,u);
    }
}
int fg,las=1e9,bok[N],ffg,f[N],_u,_v;
void dfs(int u,int fa){
    //printf("%d %d %d\n",u,fa,las);
    ans[++tot]=u,vis[u]=1;
    for(;;){
        int cnt=0,mxn=1e9;
        for(int i=fir[u];i;i=nxt[i]){
            int v=arr[i];
            if(!vis[v]&&v!=fa)
                mxn=min(mxn,v),++cnt;
        }
        //printf("%d %d %d\n",u,mxn,cnt);
        if(mxn==1e9)return ;if(bok[u]&&bok[mxn]){
            //printf("%d %d %d %d %d\n",u,cnt,fg,mxn,las);
            if(cnt==1&&!fg&&las<mxn){fg=1;return ;}
            vis[mxn]=1;int mnn=1e9;
            for(int i=fir[u];i;i=nxt[i]){
                int v=arr[i];
                if(!vis[v]&&v!=fa&&!bok[v])
                    mnn=min(mnn,v);
            }
            if(mnn!=1e9)las=mnn;
            else if(!ffg)
                for(int i=fir[u];i;i=nxt[i]){
                    int v=arr[i];
                    if(!vis[v]&&bok[v])
                        las=v;
                }
        }
        if(bok[u])ffg=1;
        dfs(mxn,u);
    }
}
int Fin(int x){return f[x]==x?x:f[x]=Fin(f[x]);}
int merge(int u,int v){
    u=Fin(u),v=Fin(v);
    if(u^v)f[u]=v;
    return u^v;
}
int fat[N],dep[N];
void Dfs(int u){
    dep[u]=dep[fat[u]]+1;
    for(int i=fir[u];i;i=nxt[i])
        if(arr[i]!=fat[u])
            fat[arr[i]]=u,Dfs(arr[i]);
}
void get(int u,int v){
    for(;u^v;bok[u]=1,u=fat[u])
        if(dep[u]<dep[v])
            u^=v^=u^=v;
    bok[u]=1;
}
int main(){
    int n=read(),m=read();
    if(m+1==n){
        for(int i=0,u,v;i<m;++i)
            u=read(),v=read(),
            ADD(u,v),ADD(v,u);
        DFS(1,0);
        for(int i=1;i<=n;++i)
            printf("%d ",ans[i]);
        return 0;
    }
    for(int i=1;i<=n;++i)f[i]=i;
    for(int i=0,u,v;i<m;++i){
        u=read(),v=read();
        if(merge(u,v))
            ADD(u,v),ADD(v,u);
        else _u=u,_v=v;
    }Dfs(1);get(_u,_v);
    ADD(_u,_v),ADD(_v,_u);
    //for(int i=1;i<=n;++i)
        //printf("!%d\n",bok[i]);
    dfs(1,0);
    for(int i=1;i<=tot;++i)
        printf("%d ",ans[i]);
    return 0;
}
