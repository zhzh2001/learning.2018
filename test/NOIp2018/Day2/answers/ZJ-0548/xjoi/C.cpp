#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 100005
const long long inf=1e12;
inline int read(){
    int x=0;char ch=getchar();
    for(;ch<48||ch>57;ch=getchar());
    for(;ch>47&&ch<58;ch=getchar())
    x=(x<<1)+(x<<3)+(ch^48);return x;
}
using namespace std;
int vis[N],arr[N<<1],fir[N],nxt[N<<1],e,val[N];
void ADD(int u,int v){
    arr[++e]=v;nxt[e]=fir[u];fir[u]=e;
}
long long f[N][3];
void dfs(int u,int fa){
    f[u][0]=f[u][1]=f[u][2]=0;
    long long mxn=1e12,cnt=0;
    for(int i=fir[u],v;i;i=nxt[i])
        if((v=arr[i])^fa){
            dfs(v,u),f[u][2]+=f[v][0],
            f[u][1]+=min(min(f[v][0],f[v][1]),f[v][2]);
            if(f[v][1]>f[v][0])
                f[u][0]+=f[v][0],mxn=min(mxn,f[v][1]-f[v][0]);
            else ++cnt,f[u][0]+=f[v][1];
        }
    f[u][1]+=val[u];if(!cnt)f[u][0]+=mxn;
    //printf("%d %lld %lld %lld\n",u,f[u][0],f[u][1],f[u][2]);
    if(~vis[u]){
        if(vis[u])
            f[u][0]=f[u][2]=inf;
        else f[u][1]=inf;
    }
}
char s[5];
int main(){
    memset(vis,-1,sizeof vis);
    int n=read(),m=read();scanf("%s",s);
    for(int i=1;i<=n;++i)val[i]=read();
    for(int i=1,u,v;i<n;++i)
        u=read(),v=read(),
        ADD(u,v),ADD(v,u);
    if(n<=2000&&m<=2000){
        for(;m--;){
            int a=read();vis[a]=read();
            int b=read();vis[b]=read();
            dfs(1,0);vis[a]=vis[b]=-1;
            long long ans=min(f[1][0],f[1][1]);
            //for(int i=1;i<=n;++i)
                //printf("%d: %lld %lld %lld\n",i,f[i][0],f[i][1],f[i][2]);
            printf("%lld\n",ans<inf?ans:-1);
        }
        return 0;
    }
    if(*s=='A'){
        return 0;
    }
    return 0;
}
