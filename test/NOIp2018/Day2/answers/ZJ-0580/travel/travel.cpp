#include <bits/stdc++.h>

using namespace std;

const int len = 5010;
int n,m,k,p,f[len],vis[len],head[len];
//priority_queue <int,vector<int>,greater<int> > q;

struct node {
	int cnt,nxt;
}e[len<<1];

inline void add(int u,int v) {
	e[++k] = (node){v,head[u]},head[u] = k;
}

/*inline void bfs() {
	q.push(1);
	while (!q.empty()) {
		int u = q.top();
		vis[u] = 1,q.pop(),f[++p] = u;
		for (int i=head[u];i!=0;i=e[i].nxt) {
			int v = e[i].cnt;
			if (vis[v] == 0) q.push(v); 
		}
	}
}*/

inline void dfs(int u) {
	f[++p] = u,vis[u] = 1;
	int kk = 0,b[len];
	for (int i=head[u];i!=0;i=e[i].nxt) if (!vis[e[i].cnt]) b[++kk] = e[i].cnt;
	if (kk == 0) return;
	sort(b+1,b+kk+1);
	for (int i=1;i<=kk;i++) dfs(b[i]);
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	k = p = 0;
	for (int i=1;i<=m;i++) {
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v),add(v,u);
	}
	memset(vis,0,sizeof vis);
	dfs(1);
	for (int i=1;i<=n;i++) printf("%d ",f[i]);
}
