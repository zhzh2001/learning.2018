#include <bits/stdc++.h>

using namespace std;

const int len = 100010;
int n,m,p[len],vis[len],f[len][2];
string s;

inline int work() {
	for (int i=2;i<=n;i++) {
		if (vis[i] == 0 && f[i-1][1] == -1) return -1;
		if (vis[i] == 1) {
			f[i][0] = -1;
			if (f[i-1][0] == -1) f[i][1] = f[i-1][1]+p[i];
			else if (f[i-1][1] == -1) f[i-1][1] = f[i-1][0]+p[i];
			else f[i][1] = min(f[i-1][0],f[i-1][1])+p[i];
		}
		else if (vis[i] == 0) {
			f[i][1] = -1;
			if (f[i-1][0] == -1) f[i][0] = f[i-1][1];
			else if (f[i-1][1] == -1) f[i][0] = f[i-1][0];
			else f[i][0] = f[i-1][1];
		}
		else {
			if (f[i-1][0] == -1) f[i][0] = f[i-1][1],f[i][1] = f[i-1][0]+p[i];
			else if (f[i-1][1] == -1) f[i][0] = f[i-1][0],f[i][1] = f[i-1][1]+p[i];
			else f[i][0] = f[i-1][1],f[i][1] = min(f[i-1][0],f[i-1][1])+p[i];
		}
	}
	return min(f[n][0],f[n][1]);
}

int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m),cin >> s;
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++) {
		int u,v;
		scanf("%d%d",&u,&v);
	}
	for (int i=1;i<=m;i++) {
		int a,x,b,y;
		memset(vis,-1,sizeof vis);
		memset(f,0,sizeof f);
		scanf("%d%d%d%d",&a,&x,&b,&y);
		vis[a] = x,vis[b] = y;
		if (vis[1] == 1) f[1][1] = p[1],f[1][0] = -1;
		else if (vis[1] == 0) f[1][1] = -1;
		else f[1][1] = p[1];
		printf("%d\n",work());
	}
}
