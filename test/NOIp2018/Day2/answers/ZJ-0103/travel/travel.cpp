#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=5005,maxe=10005;
int n,e,tot,m,k,kk,lnk[maxn],son[maxe],nxt[maxe],ans[maxn],fa[maxn],lft[maxn],rgh[maxn],now[maxn];
bool vs[maxn],BCC[maxn],pd,bl;
struct data{
	int x,y;
	data (int x=0,int y=0):x(x),y(y){}
	bool operator < (const data b)const{
		return x>b.x||(x==b.x&&y>b.y);
	}
}a[maxe];
void _add(int x,int y){son[++tot]=y; nxt[tot]=lnk[x]; lnk[x]=tot;}
void _dfsa(int x,int dad){
	for (int j=lnk[x];j;j=nxt[j])
		if (son[j]!=dad&&!vs[son[j]]){
			ans[++m]=son[j]; vs[son[j]]=1; _dfsa(son[j],x);
		}
}
void _worka(){
	ans[m=1]=1; vs[1]=1; _dfsa(1,0);
}
void _findB(int s,int t){
	rgh[s]=t; BCC[s]=1; pd=0;
	for (int x=s,y=fa[s];x!=t;x=y,y=fa[y]){
		lft[x]=y; rgh[y]=x; BCC[y]=1;
	}
	lft[t]=s; BCC[t]=1;
}
void _dfsb(int x,int dad){
	for (int j=lnk[x];j;j=nxt[j])
		if (son[j]!=dad){
			if (vs[son[j]]) {if (pd) _findB(x,son[j]); continue;}
			vs[son[j]]=1; fa[son[j]]=x; _dfsb(son[j],x);
		}
}
/*void _dfsc(int x,int dad){
	ans[++m]=x;
	if (BCC[x]){
		if (bl){bl=0; pd=1; k=max(lft[x],rgh[x]); kk=x;}
		int te=0;
		for (int j=lnk[x];j;j=nxt[j])
			if (son[j]!=dad){
				if (BCC[son[j]]&&son[j]!=k) {te=son[j]; continue;}
				if (BCC[son[j]]&&son[j]==k&&kk==x) continue;
				vs[son[j]]=1; _dfsc(son[j],x);
		}	
		if (te>k&&pd){pd=0; vs[k]=1; _dfsc(k,kk);} else {vs[te]=1; _dfsc(te,x);}
		return;
	}
	for (int j=lnk[x];j;j=nxt[j])
		if (son[j]!=dad){
			vs[son[j]]=1; _dfsc(son[j],x);
		}
}*/
void _dfsd(int x,int dad,int y,int z){
	for (int j=lnk[x];j;j=nxt[j])
		if (son[j]!=dad&&((y!=x||z!=son[j])&&(y!=son[j]||z!=x))){
			now[++m]=son[j]; vs[son[j]]=1; _dfsd(son[j],x,y,z);
		}
}
void _check(){
	for (int i=1;i<=n;i++){
		if (ans[i]>now[i]){
			for (int j=1;j<=n;j++) ans[j]=now[j];
			return;
		}
		if (ans[i]<now[i]) return;
	}
}
void _workb(){
	memset(vs,0,sizeof(vs));
	memset(BCC,0,sizeof(BCC));
	vs[1]=pd=1; _dfsb(1,0);
//	m=0; bl=1; _dfsc(1,0);
	for (int i=1;i<=n;i++) ans[i]=n-i+1;
	for (int i=1;i<=n;i++)
		if (BCC[i]){
			now[m=1]=1;
			_dfsd(1,0,i,lft[i]);
			_check();
		}
//	for (int i=1;i<=n;i++)
//		if (BCC[i]) printf("%d %d %d\n",i,lft[i],rgh[i]);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&e); m=tot=0;
	for (int i=1,x,y;i<=e;i++){
		scanf("%d%d",&x,&y);
		a[++m]=data(x,y);  a[++m]=data(y,x);
	}
	sort(a+1,a+m+1);
	memset(vs,0,sizeof(vs));
	memset(lnk,0,sizeof(lnk));
	for (int i=1;i<=m;i++) _add(a[i].x,a[i].y);
	if (n==e) _workb(); else _worka();
	for (int i=1;i<n;i++) printf("%d ",ans[i]); printf("%d",ans[n]);
	return 0;
}
