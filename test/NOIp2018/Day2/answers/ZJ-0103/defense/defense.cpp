#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;
const int maxn=100005,maxe=200005;
int n,m,tot,son[maxe],nxt[maxe],lnk[maxn],a[maxn],que[maxn],fa[maxn];
LL f[maxn][2];
inline char nc(){
	static char buf[100000],*pa=buf,*pb=buf;
	return pa==pb&&(pb=(pa=buf)+fread(buf,1,100000,stdin),pa==pb)?EOF:*pa++;
}
inline void readi(int &x){
	x=0; char ch=nc();
	while ('0'>ch||ch>'9') ch=nc();
	while ('0'<=ch&&ch<='9'){x=x*10+ch-'0'; ch=nc();}
}
void _add(int x,int y){son[++tot]=y; nxt[tot]=lnk[x]; lnk[x]=tot;}
void _bfs(){
	int hed=0,til=1; que[1]=1;
	while (hed!=til){
		int x=que[++hed];
		for (int j=lnk[x];j;j=nxt[j])
			if (son[j]!=fa[x]){
				que[++til]=son[j];
				fa[son[j]]=x;
			}
	}
}
int minF(int x,int y){
	if (x==-1&&y==-1) return -1;
	if (x==-1) return y;
	else return (y==-1)?x:min(x,y);
}
LL _work(int xa,int xb,int ya,int yb){
	if (ya+yb==0&&(fa[xa]==xb||fa[xb]==xa)) return -1;
	for (int i=n;i;i--){
		int x=que[i];
		f[x][0]=0; f[x][1]=a[x];
		bool pda=1,pdb=1;
		for (int j=lnk[x];j;j=nxt[j])
			if (son[j]!=fa[x]){
				if (pda){
					if (f[son[j]][1]==-1) {f[x][0]=-1; pda=0;}
					else f[x][0]+=f[son[j]][1];
				}
				if (pdb){
					int mn=minF(f[son[j]][0],f[son[j]][1]);
					if (mn==-1){f[x][1]=-1; pdb=0;}
					else f[x][1]+=mn;
				}
			}
		if (x==xa) f[x][1-xb]=-1;
		if (x==ya) f[x][1-yb]=-1;
	}
	return minF(f[1][0],f[1][1]);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	readi(n); readi(m); readi(a[1]); tot=0;
	for (int i=1;i<=n;i++) readi(a[i]);
	memset(lnk,0,sizeof(lnk));
	for (int i=1,x,y;i<n;i++){
		readi(x); readi(y);
		_add(x,y); _add(y,x);
	}
	_bfs();
	for (int i=1,xa,xb,ya,yb;i<=m;i++){
		readi(xa); readi(xb); readi(ya); readi(yb);
		printf("%lld\n",_work(xa,xb,ya,yb));
	}
}
