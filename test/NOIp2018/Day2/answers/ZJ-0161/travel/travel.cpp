#include <bits/stdc++.h>
using namespace std;

const int N = 5000 + 5, M = N << 1;

int n, m;
vector <int> G[N];

namespace Subtask1 {
	
	bool vis[N];
	int ans[N]; int tot = 0;
	
	void dfs(int x) {
		ans[++ tot] = x; vis[x] = true;
		for (int i = 0; i < (int) G[x].size(); ++ i) {
			if (vis[G[x][i]]) continue;
			dfs(G[x][i]);
		}
	}
	void Solve() {
		memset(vis, false, sizeof(vis));
		dfs(1);
		for (int i = 1; i < n; ++ i) printf("%d ", ans[i]); printf("%d\n", ans[n]);
	}
}

namespace Subtask2 {
	
	bool con[N][N], vis[N];
	int sta[N], top = 0;
	vector <int> circle;
	bool ok = false;
	
	void dfs(int x, int fat) {
		if (ok) return;
		sta[++ top] = x; vis[x] = true;
		for (int i = 0; i < (int) G[x].size(); ++ i) {
			if (ok) return;
			int v = G[x][i]; if (v == fat) continue;
			if (! vis[v]) dfs(v, x);
			else {
				while (1) {
					circle.push_back(sta[top]); -- top;
					int x = sta[top]; if (x == v) break;
				}
				circle.push_back(v);
				ok = true;
				return;
			}
		}
		-- top; vis[x] = false;
	}
	
	void Findcircle() {
		memset(vis, false, sizeof(vis)); circle.clear();
		dfs(1, 0);
		circle.push_back(circle[0]);
	}
	
	int ans[N], p[N], numans = -1, tot = 0;
	void DFS(int x) {
		p[++ tot] = x; vis[x] = true;
		for (int i = 0; i < (int) G[x].size(); ++ i) {
			int v = G[x][i]; if (vis[v] || !con[x][v]) continue;
			DFS(v);
		}
	}
	
	bool check() {
		for (int i = 1; i <= n; ++ i) if (ans[i] != p[i]) return p[i] < ans[i];
		return false;
	}
	
	void Solve() {
		memset(con, true, sizeof(con));
		Findcircle();
		for (int i = 0; i < (int) circle.size() - 1; ++ i) {
			memset(vis, false, sizeof(vis)); tot = 0;
			con[circle[i]][circle[i + 1]] = con[circle[i + 1]][circle[i]] = false;
			DFS(1);
			if (numans == -1) {
				numans = n;
				for (int j = 1; j <= n; ++ j) ans[j] = p[j];
			} else if (check()){
				for (int j = 1; j <= n; ++ j) ans[j] = p[j];
			}
			con[circle[i]][circle[i + 1]] = con[circle[i + 1]][circle[i]] = true;
		}
		for (int i = 1; i < n; ++ i) printf("%d ", ans[i]);	printf("%d\n", ans[n]);
	}
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; ++ i) {
		int x, y; scanf("%d%d", &x, &y);
		G[x].push_back(y); G[y].push_back(x);
	}
	for (int i = 1; i <= n; ++ i) sort(G[i].begin(), G[i].end());
	if (m == n - 1) {
		Subtask1 :: Solve();
		return 0;
	}
	if (m == n) {
		Subtask2 :: Solve();
		return 0;
	}
	return 0;
}
