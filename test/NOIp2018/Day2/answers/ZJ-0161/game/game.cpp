#include <bits/stdc++.h>
#define ll long long
using namespace std;

const int N = 10, M = 1000000 + 5, Mod = 1e9 + 7;

int n, m;
ll dp[10][M];

int len1, len2, num1[10], num2[10];
bool check(int s, int t) {
	len1 = len2 = 0;
	for (int i = 1; i <= n; ++ i) {
		num1[i] = (s >> (i - 1))&1;
		num2[i] = (t >> (i - 1))&1;
	}
	for (int i = 1; i < n; ++ i) {
		if (num2[i] == 0) continue;
		if (num1[i + 1] == 0) return false;
	}
	return true;
}

void Solve1() {
	int MaxState = (1 << n);
	for (int i = 0; i < MaxState; ++ i) dp[i][1] = 1;
	for (int i = 2; i <= m; ++ i) {
		for (int j = 0; j < MaxState; ++ j) {
			for (int k = 0; k < MaxState; ++ k) {
				if (check(j, k)) {
					dp[j][i] += dp[k][i - 1];
					if (dp[j][i] >= Mod) dp[j][i] -= Mod;
				}
			}
		}
	}	
	ll ans = 0;
	for (int i = 0; i < MaxState; ++ i) {
		ans += dp[i][m];
		if (ans >= Mod) ans -= Mod;
	}
	printf("%lld\n", ans);
}


int main(void) {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
//	if (n <= 3 && m <= 3) {
//		Brute();
//		return 0;
//	}
		Solve1();

	return 0;
}
