#include <bits/stdc++.h>
#define ll long long
using namespace std;

const int N = 1e5 + 5, M = N << 1;

int n, m, toit[M], link[N], nxt[N], cnt = 0;

void AddEdge(int x, int y) {
	++ cnt; toit[cnt] = y; nxt[cnt] = link[x]; link[x] = cnt;
}

char op[5];
map<pair<int, int>, bool> Mp;

ll dp[N][2], p[N], f[N][2]; int lim[N];

void dfs1(int x, int fat) {// 1-> not 2 -> must	
	for (int i = link[x]; i; i = nxt[i]) {
		int v = toit[i]; if (v == fat) continue;
		dfs1(v, x);
	}	
	if (lim[x] == 0) {
		ll sum1 = 0, sum2 = 0;		
		bool ok0 = true;
		for (int i = link[x]; i; i = nxt[i]) {
			int v = toit[i]; if (v == fat) continue;
			sum1 += dp[v][1];
			if (lim[v] == 0) sum2 += min(dp[v][1], dp[v][0]);
			else if (lim[v] == 1) sum2 += dp[v][0];
			else if (lim[v] == 2) sum2 += dp[v][1];
			if (lim[v] == 1) ok0 = false;
		}
		if (ok0) dp[x][0] = sum1; else dp[x][0] = 1e12;
		dp[x][1] = sum2 + p[x];
	} else if (lim[x] == 2) { // �� 
		dp[x][0] = 1e12; dp[x][1] = p[x];
		for (int i = link[x]; i; i = nxt[i]) {
			int v = toit[i]; if (v == fat) continue;
			if (lim[v] == 0) dp[x][1] += min(dp[v][0], dp[v][1]);
			else if (lim[v] == 1) dp[x][1] += dp[v][0];
			else if (lim[v] == 2) dp[x][1] += dp[v][1];
		}
	} else if (lim[x] == 1) { //bu �� 
		dp[x][1] = 1e12; dp[x][0] = 0;
		for (int i = link[x]; i; i = nxt[i]) {
			int v = toit[i]; if (v == fat) continue;
			dp[x][0] += dp[v][1];
		}
	}
//	printf("x = %d lim = %d, dp[x][0] = %lld dp[x][1] = %lld\n", x, lim[x], dp[x][0], dp[x][1]);
}

void Solve1() {
	memset(lim, 0, sizeof(lim));
	memset(dp, 0, sizeof(dp));
	for (int i = 1; i <= m; ++ i) {
		int a, b, x, y; scanf("%d%d%d%d", &a, &x, &b, &y);
		if (x == 0 && y == 0 && Mp[make_pair(a, b)] == true) {
			puts("-1"); continue;
		}
		lim[a] = x + 1; lim[b] = y + 1;
		dfs1(1, 0);
		lim[a] = 0; lim[b] = 0;
		printf("%lld\n", min(dp[1][0], dp[1][1]));
//		puts("?");
	}
}

void dfs2(int x, int fat) { // dp �� 
	for (int i = link[x]; i; i = nxt[i]) {
		int v = toit[i]; if (v == fat) continue;
		dfs2(v, x);
	}
	ll sum1 = 0, sum2 = 0;	
	for (int i = link[x]; i; i = nxt[i]) {
		int v = toit[i]; if (v == fat) continue;
		sum1 += dp[v][1];
		sum2 += min(dp[v][1], dp[v][0]);
	}
	dp[x][0] = sum1;
	dp[x][1] = sum2 + p[x];
}

void dfs3(int x, int fat) { // f �� 
	for (int i = link[x]; i; i = nxt[i]) {
		int v = toit[i]; if (v == fat) continue;
		dfs3(v, x);
	}
	ll sum1 = 0, sum2 = 0;	
	for (int i = link[x]; i; i = nxt[i]) {
		int v = toit[i]; if (v == fat) continue;
		sum1 += f[v][1];
		sum2 += min(f[v][1], f[v][0]);
	}
	f[x][0] = sum1;
	f[x][1] = sum2 + p[x];
}

void Solve2() {
	dfs2(1, 0);
	dfs3(n, 0);
	for (int i = 1; i <= m; ++ i) {
		int a, x, b, y; scanf("%d%d%d%d", &a, &x, &b, &y);
		if (x == 0 && y == 0 && Mp[make_pair(a, b)] == true) {
			puts("-1"); continue;
		}
		if (a > b) swap(a, b);
		if (x == 0 && y == 1) {
			printf("%lld\n", f[a][0] + dp[b][1]);
		} else if (x == 1 && y == 0) {
			printf("%lld\n", f[a][1] + dp[b][0]);
		} else if (x == 1 && y == 1) {
			printf("%lld\n", f[a][1] + dp[b][1]);
		}
	}
}

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d", &n, &m); scanf("%s", op + 1); Mp.clear();
	for (int i = 1; i <= n; ++ i) scanf("%lld", &p[i]);
	for (int i = 1; i < n; ++ i) {
		int x, y; scanf("%d%d", &x, &y);
		AddEdge(x, y); AddEdge(y, x);
		Mp[make_pair(x, y)] = Mp[make_pair(y, x)] = true;
	}
	if (m <= 2000) {
		Solve1();
		return 0;
	}
	if (op[1] == 'A' && op[2] == '2') {
		Solve2();
		return 0;
	}
	return 0;
}
/*
5 12 A2
5 6 7 3 2
1 2
2 3
3 4
4 5
1 1 2 0
1 0 2 1
1 1 2 1
2 1 3 0
2 0 3 1
2 1 3 1
3 1 4 0
3 0 4 1
3 1 4 1
4 1 5 1
4 0 5 1
4 1 5 0
*/
