#include<bits/stdc++.h>
#define ll long long
#define M 100005
using namespace std;
bool cur1;
int n,m,pr[M<<1],to[M<<1],la[M<<1],a[M],tot;
char s[5];
void add(int x,int y) {
	to[++tot]=y,pr[tot]=la[x],la[x]=tot;
}
struct P40 {
	int dp[M][2],vis[M];
	void Min(int &x,int y) {
		if(y==-1)return;
		if(x==-1||x>y)x=y;
	}
	bool okk;
	void dfs(int x,int f) {
		if(!okk)return;
		if(vis[x]==0)dp[x][0]=0;
		else if(vis[x]==1)dp[x][1]=a[x];
		else dp[x][0]=0,dp[x][1]=a[x];
		for(int i=la[x]; i!=-1; i=pr[i]) {
			int y=to[i];
			if(y==f)continue;
			dfs(y,x);
			if(vis[x]==0&&dp[y][1]==-1||dp[y][1]==-1&&dp[y][0]==-1)okk=0;
			else {
				if(vis[x]!=1&&dp[y][1]!=-1)dp[x][0]+=dp[y][1];
				if(vis[x]!=0) {
					int now;
					if(dp[y][0]==-1)now=dp[y][1];
					else if(dp[y][1]==-1)now=dp[y][0];
					else now=min(dp[y][1],dp[y][0]);
					dp[x][1]+=now;
				}
			}
			if(!okk)return;
		}
	}
	void solve() {
		while(m--) {
			for(int i=0;i<=n;i++)vis[i]=-1,dp[i][0]=dp[i][1]=-1;
			int x,f1,f2,y;
			scanf("%d%d%d%d",&x,&f1,&y,&f2);
			vis[x]=f1,vis[y]=f2;
			if(vis[x]==0)for(int i=la[x]; i!=-1; i=pr[i])if(to[i]!=y)vis[to[i]]=1;
			if(vis[y]==0)for(int i=la[y]; i!=-1; i=pr[i])if(to[i]!=x)vis[to[i]]=1;
			okk=1,dfs(1,0);
			if(!okk)printf("-1\n");
			else {
				int ans=-1;
				Min(ans,dp[1][0]),Min(ans,dp[1][1]);
				printf("%d\n",ans);
			}
		}
	}
} p40;
bool cur2;
int main() {
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	memset(la,-1,sizeof(la));
	for(int i=1; i<=n; i++)scanf("%d",&a[i]);
	for(int i=1; i<n; i++) {
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y),add(y,x);
	}
	p40.solve();
	return 0;
}
