#include<bits/stdc++.h>
#define ll long long
const int Mod=1e9+7;
using namespace std;
bool cur1;
int n,m,sx[]= {1,0},sy[]= {0,1};
struct P20 {
	bool mark[40];
	int sz;
	struct node {
		int a,b;
	} A[40];
	void dfs(int x,int y,int a,int b) {
		if(x==n&&m==y) {
			A[++sz]=(node) {
				a,b
			};
			return;
		}
		for(int i=0; i<2; i++) {
			int xx=x+sx[i],yy=y+sy[i];
			if(xx>n||yy>m)continue;
			dfs(xx,yy,a*2+i,b*2+mark[(xx-1)*m+yy]);
		}
	}
	bool chk() {
		sz=0;
		dfs(1,1,0,mark[1]);
		for(int i=1; i<=sz; i++)
			for(int j=i+1; j<=sz; j++) {
				if(A[i].a>A[j].a&&A[i].b>A[j].b)return false;
				if(A[i].a<A[j].a&&A[i].b<A[j].b)return false;
			}
		return true;
	}
	void solve() {
		int k=n*m,ans=0;
		for(int i=0; i<(1<<k); i++) {
			for(int j=0; j<k; j++)mark[j+1]=((i&(1<<j))>0);
			if(chk())ans=(ans+1)%Mod;
		}
		ans=ans%Mod;
		printf("%d\n",ans);
	}
} p20;
struct P50 {
	int mul(int x,int y) {
		int res=1;
		while(y) {
			if(y&1)res=1ll*res*x%Mod;
			x=1ll*x*x%Mod;
			y>>=1;
		}
		return res;
	}
	void solve() {
		if(n>=m)swap(n,m);
		if(n==1)printf("%d\n",mul(2,m));
		else if(n==2)printf("%d\n",1ll*12*mul(3,m-2)%Mod);
		else printf("%d\n",1ll*112*mul(3,m-3)%Mod);
	}
} p50;
struct P100 {
	int dp[10][2],ans;
	void solve(){
		ans=1;
		if(n>m)swap(n,m);
		dp[1][0]=dp[1][1]=1;
		for(int i=2;i<=8;i++){
			dp[i][0]=(dp[i-1][0]+dp[i-1][1])%Mod;
			dp[i][1]=(dp[i-1][1])%Mod;
		}
		for(int i=1;i<=n;i++)ans=1ll*ans*(dp[i][0]+dp[i][1])%Mod;
		for(int i=2;i<=m;i++)ans=1ll*ans*(dp[min(m-i+1,n)][0]+dp[min(m-i+1,n)][1])%Mod;
		printf("%d\n",ans);
	}
}p100;
bool cur2;
int main() {
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n<=3&&m<=3)
		p20.solve();
	else if(n<=3)
		p50.solve();
	else 
		p100.solve();
	return 0;
}
