#include<bits/stdc++.h>
#define ll long long
#define M 5005
using namespace std;
bool cur1;
int n,m,de[M];
vector<int>G[M];
struct P60 {
	void dfs(int x,int f) {
		printf("%d ",x);
		for(int i=0; i<G[x].size(); i++) {
			int y=G[x][i];
			if(y==f)continue;
			dfs(y,x);
		}
	}
	void solve() {
		for(int i=1; i<=n; i++)sort(G[i].begin(),G[i].end());
		dfs(1,0);
		printf("\n");
	}
} p60;
struct P100 {
	bool fl,vis[M],in_loop[M],okk;
	queue<int>Q;
	int nxt;
	void dfs(int x,int f) {
		if(vis[x])return;
		vis[x]=1;
		printf("%d ",x);
		if(!fl&&in_loop[x]) {
			int cnt=0;
			fl=1;
			for(int i=0; i<G[x].size(); i++) {
				int y=G[x][i];
				if(y==f)continue;
				if(in_loop[y])cnt++;
				if(cnt==2) {
					nxt=y;
					break;
				}
			}
		}
		for(int i=0;i<G[x].size();i++){
			int y=G[x][i];
			if(y==f)continue;
			if(!okk&&in_loop[y]&&y>nxt){
				okk=1;
				continue;
			}
			dfs(y,x);
		}
	}
	void solve() {
		fl=0;
		nxt=1e9;
		memset(in_loop,1,sizeof(in_loop));
		memset(vis,0,sizeof(vis));
		for(int i=1; i<=n; i++)sort(G[i].begin(),G[i].end());
		while(!Q.empty())Q.pop();
		for(int i=1; i<=n; i++)if(de[i]==1)Q.push(i);
		while(!Q.empty()) {
			int x=Q.front();
			Q.pop();
			in_loop[x]=0;
			for(int i=0; i<G[x].size(); i++) {
				int y=G[x][i];
				if((--de[y])==1)Q.push(y);
			}
		}
		dfs(1,0);
		printf("\n");
	}
} p100;
bool cur2;
int main() {
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)G[i].clear();
	for(int i=1; i<=m; i++) {
		int a,b;
		scanf("%d%d",&a,&b);
		G[a].push_back(b);
		G[b].push_back(a);
		de[a]++,de[b]++;
	}
	if(m==n-1)
		p60.solve();
	else
		p100.solve();
	return 0;
}
