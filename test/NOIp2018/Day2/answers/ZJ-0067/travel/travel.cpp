#include <cstdio>
#include <vector>
#include <algorithm>
const int maxn = 5123;
struct edge{ int v, nxt;
}e[maxn<<1]; int fir[maxn], num(1);
inline void add(int u, int v){
	e[++num].v = v; e[num].nxt = fir[u]; fir[u] = num;
}
int n, m, ans[maxn], dfs_clock;
std::vector<int> v[maxn];
void dfst(int now, int fr){
	ans[++dfs_clock] = now;
	for(int itr(fir[now]); itr; itr = e[itr].nxt){
		if(e[itr].v == fr) continue;
		v[now].push_back(e[itr].v);
	}
	std::sort(v[now].begin(), v[now].end());
	for(int i(0); i < v[now].size(); ++i){
		dfst(v[now][i], now);
	}
	v[now].clear();
}
void print(){
	for(int i(1); i < n; ++i){
		printf("%d ", ans[i]);
	}
	printf("%d\n", ans[n]);
}
void solvetree(){
	dfst(1, -1);
	print();
}
int in[maxn], q[maxn], fa[maxn]; 
bool ontre[maxn];
void getcir(){
	int l(1), r(0);
	for(int i(1); i <= n; ++i){
		if(in[i] == 1){
			ontre[i] = true;
			q[++r] = i;
		}
	}
	while(l <= r){
		int now = q[l++];
		for(int itr(fir[now]); itr; itr = e[itr].nxt){
			if(in[e[itr].v] > 1){
				--in[e[itr].v]; fa[now] = e[itr].v;
				if(in[e[itr].v] == 1){
					ontre[e[itr].v] = true;
					q[++r] = e[itr].v;
				}
			}
		}
	}
}
int move_one_up(int now, int fr){
	ans[++dfs_clock] = now;
	if(!ontre[now]) return now;
	for(int itr(fir[now]); itr; itr = e[itr].nxt){
		if(e[itr].v == fr || e[itr].v == fa[now]) continue;
		v[now].push_back(e[itr].v);
	}
	std::sort(v[now].begin(), v[now].end());
	for(int i(0); i < v[now].size(); ++i){
		move_one_up(v[now][i], now);
	}
	v[now].clear();
	return move_one_up(fa[now], now);
}
void dfstree(int now, int fr){
	ans[++dfs_clock] = now;
	for(int itr(fir[now]); itr; itr = e[itr].nxt){
		if(!ontre[e[itr].v] || e[itr].v == fr) continue;
		v[now].push_back(e[itr].v);
	}
	std::sort(v[now].begin(), v[now].end());
	for(int i(0); i < v[now].size(); ++i){
		dfstree(v[now][i], now);
	}
}
void getseq(int now, int fr, std::vector<int>&x){
	x.push_back(now);
	for(int itr(fir[now]); itr; itr = e[itr].nxt){
		if(!ontre[e[itr].v] || e[itr].v == fr) continue;
		v[now].push_back(e[itr].v);
	}
	std::sort(v[now].begin(), v[now].end());
	for(int i(0); i < v[now].size(); ++i){
		getseq(v[now][i], now, x);
	}
	v[now].clear();
}
std::vector<int> notvis, son[maxn];
bool vis[maxn];
void solvectree(){
	getcir(); // find the circle
	int now = 1;
	if(ontre[1]) now = move_one_up(1, -1);
	else getseq(1, -1, son[1]), ans[++dfs_clock] = 1;
	vis[now] = true;
	
	int l1(-1), r1(-1);
	for(int itr(fir[now]); itr; itr = e[itr].nxt){
		if(ontre[e[itr].v] || vis[e[itr].v]) continue;
		if(l1 == -1) l1 = e[itr].v;
		else r1 = e[itr].v;
	}
	if(l1 > r1) l1^=r1^=l1^=r1; // get l1, r1
	if(son[1].size() > 1 && son[1][1] < l1){
		for(int j(1); j < son[1].size(); ++j){
			ans[++dfs_clock] = son[1][j];
		}
		son[1].clear();
	}
	while(1){
		ans[++dfs_clock] = l1; vis[l1] = true; int tmp(-1);
		for(int itr(fir[l1]); itr; itr = e[itr].nxt){
			if(ontre[e[itr].v] || vis[e[itr].v]) continue;
			tmp = e[itr].v; break;
		}
		getseq(l1, -1, son[l1]);
		if(tmp == -1 || tmp > r1){
			notvis.push_back(l1);
			break;
		}
		if(son[l1].size() > 1 && son[l1][1] < tmp){
			for(int i(1); i < son[l1].size(); ++i){
				ans[++dfs_clock] = son[l1][i];
			}
		}
		else{
			notvis.push_back(l1);
		}
		l1 = tmp;
	}
	for(int i(notvis.size()-1); i>=0; --i){
		for(int j(1); j < son[notvis[i]].size(); ++j){
			ans[++dfs_clock] = son[notvis[i]][j];
		}
	}
	notvis.clear(); 
	if(son[1].size() > 1 && son[1][1] < r1){
		for(int j(1); j < son[1].size(); ++j){
			ans[++dfs_clock] = son[1][j];
		}
		son[1].clear();
	}
	while(1){
		if(vis[r1]) break;
		ans[++dfs_clock] = r1; vis[r1] = true; int tmp(-1);
		for(int itr(fir[r1]); itr; itr = e[itr].nxt){
			if(ontre[e[itr].v] || vis[e[itr].v]) continue;
			tmp = e[itr].v; break;
		}
		getseq(r1, -1, son[r1]);
		if(tmp == -1){
			notvis.push_back(r1);
			break;
		}
		if(son[r1].size() > 1 && son[r1][1] < tmp){
			for(int i(1); i < son[r1].size(); ++i){
				ans[++dfs_clock] = son[r1][i];
			}
		}
		else{
			notvis.push_back(r1);
		}
		r1 = tmp;
	}
	for(int i(notvis.size()-1); i>=0; --i){
		for(int j(1); j < son[notvis[i]].size(); ++j){
			ans[++dfs_clock] = son[notvis[i]][j];
		}
	}
	if(son[1].size() > 1){
		for(int j(1); j < son[1].size(); ++j){
			ans[++dfs_clock] = son[1][j];
		}
		son[1].clear();
	}
	print();
}
int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i(1), u, v; i <= m; ++i){
		scanf("%d%d", &u, &v);
		add(u, v); add(v, u);
		++in[u]; ++in[v];
	}
	if(m == n - 1){
		solvetree();
	}
	else{
		solvectree();
	}
	return 0;
}

