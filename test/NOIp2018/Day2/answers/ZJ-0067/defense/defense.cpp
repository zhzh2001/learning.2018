#include <cstdio>
#include <cstring>
typedef long long ll;
const ll inf = 1e12;
const int maxn = 112345;
struct edge{ int v, nxt;
}e[maxn<<1]; int fir[maxn], num(1);
inline void add(int u, int v){
	e[++num].v = v; e[num].nxt = fir[u]; fir[u] = num;
}
inline ll min(ll a, ll b){
	return a < b ? a : b;
}
ll dp[maxn][3]; ll val[maxn];
int a1, c1, a2, c2;
void DP(int now, int fr){
	dp[now][0] = dp[now][1] = dp[now][2] = inf;
	ll t0(0), t1(0), t2(0), tc(inf); bool flag(false);
	for(int itr(fir[now]); itr; itr = e[itr].nxt){
		if(e[itr].v == fr) continue;
		DP(e[itr].v, now);
		t0 += dp[e[itr].v][1];
		t2 += dp[e[itr].v][0];
		if(dp[e[itr].v][2] + val[e[itr].v] < dp[e[itr].v][1]){
			t1 += dp[e[itr].v][2] + val[e[itr].v];
			flag = true;
		}
		else{
			t1 += dp[e[itr].v][1];
		}
		tc = min(tc, dp[e[itr].v][2] + val[e[itr].v] - dp[e[itr].v][1]);
	}
	if(!flag) t1 += tc;
	//t1 = true, 0 ������
	//t2 = true 2 ������ 
	dp[now][0] = min(dp[now][0], t0);
	dp[now][1] = min(dp[now][1], t1);
	dp[now][2] = min(dp[now][2], t2);
	int c(-1);
	if(now == a1) c = c1;
	if(now == a2) c = c2;
	if(c == 1){
		dp[now][2] = min(dp[now][0], min(dp[now][1], dp[now][2]));
		dp[now][0] = dp[now][1] = inf;
	}
	else if(c == 0){
		dp[now][2] = inf;
	}
}
int main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	int n, m;
	scanf("%d%d%*s", &n, &m);
	for(int i(1); i <= n; ++i){
		scanf("%d", &val[i]);
	}
	add(1, 0); add(0, 1); val[0] = inf;
	for(int i(1), u, v; i < n; ++i){
		scanf("%d%d", &u, &v);
		add(u, v); add(v, u);
	}
	while(m--){
		scanf("%d%d%d%d", &a1, &c1, &a2, &c2);
		DP(0, -1);
		ll r = min(dp[0][0], dp[0][1]);
		printf("%lld\n", r >= inf ? -1 : r);
	}
	return 0;
}

