#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline void kkk(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}
const ll P=1e9+7;
ll a[10][10],ans,n,m;
ll t[10000],num;/*
inline void fid(ll x,ll y,ll now){
	if (x==n&&y==m){
		t[++num]=now;
		return;
	}
	if (x<n) fid(x+1,y,now+(a[x][y]<<(x+y-2)));
	if (y<m) fid(x,y+1,now+(a[x][y]<<(x+y-2)));
}*/
/*inline bool pd(){
//	fid(1,1,0);
	ll t1=0,t2=0;
	for (ll i=1;i<=n;++i) t1=t1*2+(a[1][i]<<(i-1));
	for (ll i=2;i<=m;++i) t1=t1*2+(a[i][m]<<(m+i-3));
	for (ll i=1;i<=m;++i) t2=t2*2+(a[i][1]<<(i-1));
	for (ll i=2;i<=n;++i) t2=t2*2+(a[n][i]<<(n+i-3));
	return t1>=t2;
}
inline void dfs(ll x,ll y){
	if (y>m) ++x,y=1;
	if (x>n){
		ans=(ans+pd())%P;
		return;
	}
	a[x][y]=0;
	dfs(x,y+1);
	a[x][y]=1;
	dfs(x,y+1);
}*/
inline ll pow(ll x,ll y){
	ll ret=1;
	for (;y;y>>=1,x=x*x%P) if (y&1) ret=ret*x%P;
	return ret;
}
int main(){
	kkk();
	read(n),read(m);
	if (n==1) return wr(pow(2,m)),0;
	if (m==1) return wr(pow(2,n)),0;
	if (n==2) return wr(4*pow(3,m-1)%P),0;
	if (n==3) return wr(16*pow(7,m-2)%P),0;
	if (n==5&&m==5) return puts("7136"),0;
/*	if (n<=3&&m<=3){
		dfs(1,1);
		wr(ans);
		return 0;
	}*/
	return 0;
}
