#include <iostream>
#include <cstdio>
#include <cstring>
#include <vector>
using namespace std;
typedef long long ll;
#define gc getchar
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline void kkk(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
}
const ll N=1e5+10;
ll n,m,p[N],f[N][2],A,B,X,Y,ans;
char type[3];
vector<ll>e[N];
inline void dp(ll x,ll fa){
	f[x][1]=p[x];
	for (ll i=0;i<(ll)e[x].size();++i){
		if (e[x][i]==fa) continue;
		dp(e[x][i],x);
		if (x==A){
			if (X==1) f[x][1]+=min(f[e[x][i]][1],f[e[x][i]][0]);
			else f[x][0]+=f[e[x][i]][1];
		}else
		if (x==B){
			if (Y==1) f[x][1]+=min(f[e[x][i]][1],f[e[x][i]][0]);
			else f[x][0]+=f[e[x][i]][1];
		}else{
			f[x][1]+=min(f[e[x][i]][1],f[e[x][i]][0]);
			f[x][0]+=f[e[x][i]][1];
		}
	}
	if (x==A) f[x][!X]=0x3f3f3f3f3f;
	if (x==B) f[x][!Y]=0x3f3f3f3f3f;
}
int main(){
	kkk();
	read(n),read(m),scanf("%s",type);
	for (ll i=1;i<=n;++i) read(p[i]);
	for (ll i=1,x,y;i<n;++i){
		read(x),read(y);
		e[x].push_back(y);
		e[y].push_back(x);
	}
	for (ll i=1;i<=m;++i){
		read(A),read(X),read(B),read(Y);
		memset(f,0,sizeof f);
		dp(1,0);
		ans=min(f[1][0],f[1][1]);
		wr(ans>=5e9?-1:ans);
		puts("");
	}
	return 0;
}
