#include<bits/stdc++.h>
using namespace std;
const int N=2e5+20;

inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-') f=-1;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

char type[3];
int n,m,tot,p[N],num[N];
int ver[N],nex[N],head[N];
inline void add(int x,int y)
{ver[++tot]=y;nex[tot]=head[x];head[x]=tot;}

struct type_A
{
	int l,r,cnt,s[N],pos[N],la[N];
	
	inline void dfs(int x,int fa)
	{
		s[++cnt]=x;pos[x]=cnt;
		for(int i=head[x];i;i=nex[i])
		if(ver[i]!=fa) dfs(ver[i],x);
	}
	
	inline void query()
	{
		int ans=0;
		int a=read(),x=read(),b=read(),y=read();
		for(int i=1;i<=n;i++) la[i]=1;
		if(pos[a]>pos[b]) swap(a,b),swap(x,y);
		if(pos[a]+1==pos[b])
			if(!x && !y) {printf("-1\n");return ;}
		
		if(x) la[pos[a]]=2,ans+=p[a];else la[pos[a]]=0;
		if(y) la[pos[b]]=2,ans+=p[b];else la[pos[b]]=0;
		
		for(int i=pos[a]-1;i;i--)
		if(la[i+1]==2) la[i]=0;
		else if(!la[i+1]) la[i]=2,ans+=p[s[i]];
		
		for(int i=pos[b]+1;i<=cnt;i++)
		if(la[i-1]==2) la[i]=0;
		else if(!la[i-1]) la[i]=2,ans+=p[s[i]];
		
		int i,j;
		for(i=pos[a]+1,j=pos[b]-1;i<j;i++,j--)
		{
			if(la[i-1]==2) la[i]=0;
			else if(!la[i-1]) la[i]=2,ans+=p[s[i]];
			if(la[j+1]==2) la[j]=0;
			else if(!la[j+1]) la[j]=2,ans+=p[s[j]];
		}
		if(i==j)
			{if(!la[i-1] || !la[j+1]) la[i]=2,ans+=p[s[i]];}
		else if(!la[i] && !la[j])
			if(p[s[i]]<p[s[j]]) ans+=p[s[i]],la[i]=2;
			else ans+=p[s[j]],la[j]=2;
		printf("%d\n",ans);
	}
	
	inline void work()
	{
		l=0,r=0;
		for(int i=1;i<=n;i++)
		if(num[i]==1) l=r,r=i;
		dfs(l,0);
		while(m--) query();
	}
}A;

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();scanf("%s",type);
	for(int i=1;i<=n;i++) p[i]=read();
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read();
		num[x]++;num[y]++;
		add(x,y);add(y,x);
	}
	if(type[0]=='A') A.work();
	
	return 0;
}
