#include<bits/stdc++.h>
using namespace std;
const int N=6005;

inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-') f=-1;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int n,m,tot,vis[N];
struct node{int x,y;}a[N];
inline bool cmp1(node a,node b)
{return a.x<b.x || a.x==b.x&&a.y>b.y;}

int ver[N],nex[N],head[N];
inline void add(int x,int y)
{ver[++tot]=y;nex[tot]=head[x];head[x]=tot;}

inline void dfs1(int x,int fa)
{
	printf("%d ",x);int y=N;
	for(int i=head[x];i;i=nex[i])
	if(ver[i]!=fa) dfs1(ver[i],x);
}
inline void solve1()//��
{
	for(int i=1;i<=m*2;i+=2)
	a[i].x=a[i+1].y=read(),a[i].y=a[i+1].x=read();
	std::sort(a+1,a+m*2+1,cmp1);
	for(int i=1;i<=m*2;i++) add(a[i].x,a[i].y);
	dfs1(1,0);
}

inline void dfs2(int x,int fa)
{
	printf("%d ",x);vis[x]=1;
	for(int i=head[x];i;i=nex[i])
	if(ver[i]!=fa && !vis[ver[i]])
		dfs2(ver[i],x);
}

inline void solve2()//����
{
	for(int i=1;i<=m*2;i+=2)
	a[i].x=a[i+1].y=read(),a[i].y=a[i+1].x=read();
	for(int i=1;i<=m*2;i++) add(a[i].x,a[i].y);
	dfs2(1,0);
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	if(m<n) {solve1();return 0;}
	solve2();return 0;
}
