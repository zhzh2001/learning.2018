#include <bits/stdc++.h>
using namespace std;
int n,m,val[100050],ip1,ip2,a,xx,b,yy;
int father[100050];
int f[2][100050];
vector<int>Link[100050];
string type;
void dfs(int x,int fa){
	bool flag=0;
	for (int i=Link[x].size()-1;i>=0;--i)
		if(Link[x][i]!=fa){
			dfs(Link[x][i],x);
			f[0][x]+=f[1][Link[x][i]];
			f[1][x]+=min(f[1][Link[x][i]],f[0][Link[x][i]]);
			if((xx==0&&Link[x][i]==a)||(yy==0&&Link[x][i]==b)) flag=1;
		}
	f[1][x]+=val[x];
	if(flag||(xx==1&&x==a)||(yy==1&&x==b))
		f[0][x]=f[1][x];
	if((xx==0&&x==a)||(yy==0&&x==b))
		f[1][x]=f[0][x];
}
void predfs(int x,int fa){
	father[x]=fa;
	for (int i=Link[x].size()-1;i>=0;--i)
		if(Link[x][i]!=fa)
			predfs(Link[x][i],x);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	cin>>type;
	for (int i=1;i<=n;++i)
		scanf("%d",&val[i]);
	for (int i=1;i<n;++i)
		scanf("%d%d",&ip1,&ip2),
		Link[ip1].push_back(ip2),
		Link[ip2].push_back(ip1);
	predfs(1,1);
	for (int i=1;i<=m;++i){
		scanf("%d%d%d%d",&a,&xx,&b,&yy);
		if((!xx)&&(!yy)&&(father[a]==b||father[b]==a)){
			puts("-1");continue;
		}
		memset(f,0,sizeof(f));
		dfs(1,0);
		printf("%d\n",min(f[0][1],f[1][1]));
	}
	return 0;
}
