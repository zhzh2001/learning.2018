#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
int n,m,p;
bool v[5005],f[5005],a[5005][5005];
void Cyc(int x){
	if(!v[x])return;
	v[x]=0;f[x]=1;
	for(int y=1;y<=n;y++)
		if(a[x][y])Cyc(y);
}
bool vis[5005];
void init(int x,int fa){
	if(v[x]){
		v[x]=0;p=x,f[x]=1;
		Cyc(fa);
		return;
	}
	if(vis[x])return;
	vis[x]=v[x]=1;
	for(int y=1;y<=n;y++)
		if(a[x][y]&&y!=fa)init(y,x);
	vis[x]=v[x]=0;
}
void dfs(int x){
	if(vis[x])return;
	vis[x]=1;
	printf("%d ",x);
	for(int y=1;y<=n;y++)
		if(a[x][y])dfs(y);
}
int Min,Minn;
void work(int x){
	f[x]=0;
	for(int y=1;y<=n;y++)
		if(a[x][y]&&f[y]){
			if(y>Minn){
				a[x][y]=a[y][x]=0;
				return;
			}
			work(y);
		}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		a[x][y]=a[y][x]=1;
	}
	init(1,0);
	for(int i=1;i<=n&&p;i++)
		if(a[p][i]&&f[i]){
			if(Min){
				Minn=i;
				break;
			}
			Min=i;
		}
	f[p]=0;
	work(Min);
	memset(vis,0,sizeof(vis));
	dfs(1);
	return 0;
}
