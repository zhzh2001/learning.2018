#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int N=20;
char opt[5];
int nedge,head[N];
struct Edge{
	int to,nxt;
}edge[N<<1];
void add(int x,int y){
	edge[++nedge].to=y;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
int n,m,ans,p[N];
bool used[N],lim[N],vis[N];
void dfs(int x){
	if(x>n){
		int tmp=0;
		memset(vis,0,sizeof(vis));
		for(int u=1;u<=n;u++)
			if(used[u]){
				tmp+=p[u];
				vis[u]=1;
				for(int i=head[u];i;i=edge[i].nxt)
					vis[edge[i].to]=1;
			}
		for(int i=1;i<=n;i++)
			if(!vis[i])return;
		ans=min(ans,tmp);
		return;
	}
	dfs(x+1);
	if(lim[x])return;
	used[x]=1;
	dfs(x+1);
	used[x]=0;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,opt);
	for(int i=1;i<=n;i++)scanf("%d",p+i);
	for(int i=1;i<n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y),add(y,x);
	}
	while(m--){
		int a,x,b,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		memset(used,0,sizeof(used));
		lim[a]=lim[b]=1;
		if(x)used[a]=1;
		if(y)used[b]=1;
		ans=1e9;
		dfs(1);
		if(ans!=1e9)printf("%d\n",ans);
		else puts("-1");
		lim[a]=lim[b]=0;
	}
	return 0;
}
