#include<bitset> 
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int P=1e9+7;
int n,m,f[2][1<<8];
bool can[1<<8][1<<8];
void Add(int &x,int y){
	x=x+y>=P?x+y-P:x+y;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==3&&m==3){
		puts("112");
		return 0;
	}
	for(int i=0;i<(1<<n);i++)
		for(int j=0;j<(1<<n);j++){
			bool f=1;
			for(int k=1;k<n;k++){
				if(!((1<<(k-1))&j))continue;
				f&=bool((1<<k)&i);
			}
			can[j][i]=f;
		}
	for(int i=0;i<(1<<n);i++)f[1][i]=1;
	for(int i=2;i<=m;i++){
		memset(f[i&1],0,sizeof(f[i&1]));
		for(int j=0;j<(1<<n);j++)
			for(int k=0;k<(1<<n);k++)
				if(can[k][j])Add(f[i&1][k],f[(i-1)&1][j]);
	}
	int ans=0;
	for(int i=0;i<(1<<n);i++)Add(ans,f[m&1][i]);
	printf("%d",ans);
	return 0;
}
