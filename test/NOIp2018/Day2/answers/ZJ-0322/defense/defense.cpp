#include<bits/stdc++.h>
#define N 100005
using namespace std;
struct troye{ int to,nxt;}E[N*2];
int f[N],p[N],head[N],Ans,w[N],n,m,a,b,x,y,cnt;
char s[N];
void add(int u,int v){ E[++cnt].to=v;E[cnt].nxt=head[u];head[u]=cnt;}
bool pd(){
	memset(p,0,sizeof(p));
	for (int i=1;i<=n;i++)
	if (f[i]==1){
		p[i]=1;
		for (int j=head[i];j;j=E[j].nxt){
			int v=E[j].to;
			p[v]=1;
		}
	}
	for (int i=1;i<=n;i++) if (!p[i]) return false;
	return true;
}
void DFS(int t,int s){
	if (s>Ans) return;
	if (t>n){
		if (s<Ans){ if (pd()) Ans=min(Ans,s);}
		return;
	}
	if (t==a) DFS(t+1,s+x*w[a]);else
	if (t==b) DFS(t+1,s+y*w[b]);else {
		for (int i=0;i<=1;i++){
			if (i==1){
				f[t]=1;
				DFS(t+1,s+w[t]);
				f[t]=0;
			}else DFS(t+1,s);
		}
	}
}
void casea(){
	while (m--){
		Ans=10000000;
		for (int i=1;i<=n;i++) f[i]=0;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		f[a]=x; f[b]=y;
		DFS(1,0);
		if (Ans==10000000) puts("-1");else printf("%d\n",Ans);
	}
}
int main(){
	freopen("defense.in","r",stdin);freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for (int i=1;i<=n;i++) scanf("%d",&w[i]);
	for (int i=1;i<n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v),add(v,u);
	}
	casea();
	return 0;
}
