#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int a,x; char ch; x=1;
	ch=getchar();
	while (ch>'9' || ch<'0'){
		if (ch=='-') x=-1;
		ch=getchar();
	}
	a=0;
	while (ch>='0' && ch<='9'){
		a=a*10+(ch-'0'); ch=getchar();
	}
	return a*x;
}
struct edge_arr{
	int to,next;
}edge[100005];
int head[50005],num=0;
inline void lianshi(int u,int v){
	edge[++num].to=v; edge[num].next=head[u]; head[u]=num;
}
bool vis[5005];
int b[5005],n,m,numk;
inline void dfs(int now,int fa){
	vis[now]=true; b[++numk]=now;
	int nums=0,c[5005];
	int mins=2147483647;
	for (int i=head[now];i;i=edge[i].next){
		if (edge[i].to!=fa && !vis[edge[i].to]){
			c[++nums]=edge[i].to;
		}
	}
	if (nums>0){
		sort(c+1,c+nums+1);
		for (int i=1;i<=nums;i++){
			dfs(c[i],now);
		}
	}
}
int l[100005],r[100005];
inline void work(){
	numk=0; b[++numk]=1;
	int nows=max(l[1],r[1]),now=min(l[1],r[1]); vis[1]=true; vis[now]=true;
	while (true){
		b[++numk]=now; 
		if (!vis[l[now]]){
			vis[l[now]]=true; now=l[now];
			if (!vis[nows] && now>nows){
				vis[now]=false; vis[nows]=true;
				now=nows;
			}
		}
		else{
			if (!vis[r[now]]){
				vis[r[now]]=true; now=r[now];
				if (!vis[nows] && now>nows){
					vis[now]=false; vis[nows]=true;
					now=nows;
				}
			}
			else{
				break;
			}
		}
	}
}
int main(){
	freopen("travel.in","r",stdin); freopen("travel.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=m;i++){
		int u=read(),v=read();
		lianshi(u,v); lianshi(v,u);
		if (l[u]) r[u]=v; else l[u]=v;
		if (l[v]) r[v]=u; else l[v]=u;
	}
	numk=0;
	if (n!=m) dfs(1,0); else work();
	for (int i=1;i<numk;i++) printf("%d ",b[i]); printf("%d\n",b[numk]);
	return 0;
}

