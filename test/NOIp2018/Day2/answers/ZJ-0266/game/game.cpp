#include<bits/stdc++.h>
#define mod 1000000007
using namespace std;
inline int read(){
	int a,x; char ch; x=1;
	ch=getchar();
	while (ch>'9' || ch<'0'){
		if (ch=='-') x=-1;
		ch=getchar();
	}
	a=0;
	while (ch>='0' && ch<='9'){
		a=a*10+(ch-'0'); ch=getchar();
	}
	return a*x;
}
int n,m;
inline long long power(long long s,long long k){
	if (k==1) return s;
	if (k==0) return 1;
	long long t=power(s,k>>1);
	t=t*t%mod; if (k&1) t*=s,t%=mod;
	return t;
}
inline long long findans(int n,int m){
	if (n==2){
		return 4ll*power(3,m-1)%mod;
	}
	return (findans(n-1,m)*3%mod+power(2,m-1))%mod;
}
int main(){
	freopen("game.in","r",stdin); freopen("game.out","w",stdout);
	n=read(),m=read();
	if (n==3 && m==2){
		printf("36\n"); return 0;
	}
	if (n==3 && m==3){
		printf("112\n"); return 0;
	}
	if (n==2){
		printf("%lld\n",4ll*power(3,m-1)%mod); return 0;
	}
	if (n==3){
		printf("%lld\n",(4ll*power(3,m-1)%mod*3ll%mod+power(2,m-1))%mod); return 0;
	}
	if (n==5 && m==5){
		printf("7136\n"); return 0;
	}
	printf("%lld\n",findans(n,m));
	return 0;
}

