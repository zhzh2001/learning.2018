#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int a,x; char ch; x=1;
	ch=getchar();
	while (ch>'9' || ch<'0'){
		if (ch=='-') x=-1;
		ch=getchar();
	}
	a=0;
	while (ch>='0' && ch<='9'){
		a=a*10+(ch-'0'); ch=getchar();
	}
	return a*x;
}
struct edge_arr{
	int to,next;
}edge[200005];
int head[100005],num=0;
inline void lianshi(int u,int v){
	edge[++num].to=v; edge[num].next=head[u]; head[u]=num;
}
long long dp[100005][5],dps[100005][5];
int p[100005],father[100005],dep[100005],n,m;
char ch1,ch2;
inline void tree_dp(int now,int fa){
	dp[now][0]=0; dp[now][1]=p[now]; father[now]=fa; dep[now]=dep[fa]+1;
	for (int i=head[now];i;i=edge[i].next){
		if (edge[i].to!=fa){
			tree_dp(edge[i].to,now);
			dp[now][0]+=dp[edge[i].to][1];
			dp[now][1]+=min(dp[edge[i].to][0],dp[edge[i].to][1]);
		}
	}
	dps[now][0]=dp[now][0],dps[now][1]=dp[now][1];
}
inline void huanyuan(int now){
	if (now==0) return;
	dps[now][0]=dp[now][0],dps[now][1]=dp[now][1];
	huanyuan(father[now]);
}
inline void change(int now,int q){
	if (now==1) return;
	if (q==2){
			dps[father[now]][2]=dps[father[now]][0]; dps[father[now]][3]=dps[father[now]][1];
		dps[father[now]][0]-=dps[now][3]; dps[father[now]][0]+=dps[now][1];
		dps[father[now]][1]-=min(dps[now][2],dps[now][3]); dps[father[now]][1]+=min(dps[now][0],dps[now][1]);
		
		change(father[now],2);
		return;
	}
	if (q==1){
			dps[father[now]][2]=dps[father[now]][0]; dps[father[now]][3]=dps[father[now]][1];
		dps[now][0]=2147483647;
		dps[father[now]][0]-=dps[now][3]; dps[father[now]][0]+=dps[now][1];
		dps[father[now]][1]-=min(dps[now][2],dps[now][3]); dps[father[now]][1]+=dps[now][1];
			
		change(father[now],2);
		return;
	}
	if (q==0){
			dps[father[now]][2]=dps[father[now]][0]; dps[father[now]][3]=dps[father[now]][1];
		dps[now][1]=2147483647;
		dps[father[now]][0]-=dps[now][3]; dps[father[now]][0]+=dps[now][1];
		dps[father[now]][1]-=min(dps[now][2],dps[now][3]); dps[father[now]][1]+=dps[now][0];
	
		change(father[now],1);
		return;
	}
}
int main(){
	freopen("defense.in","r",stdin); freopen("defense.out","w",stdout);
	n=read(),m=read();
	ch1=getchar(); while (ch1!='A' && ch1!='B' && ch1!='C') ch1=getchar();
	ch2=getchar(); while (ch2!='1' && ch2!='2' && ch2!='3') ch2=getchar();
	for (int i=1;i<=n;i++){
		p[i]=read();
	}
	for (int i=1;i<n;i++){
		int u=read(),v=read();
		lianshi(u,v); lianshi(v,u);
	}
	tree_dp(1,0);
	for (int i=1;i<=m;i++){
		int a=read(),x=read(),b=read(),y=read();
		if (a==b && x!=y){
			printf("-1\n"); continue;
		}
		if (father[a]==b || father[b]==a){
			if (x==0 && y==0){
				printf("-1\n"); continue;
			}
		}
		if (dep[a]<dep[b]) swap(a,b),swap(x,y);
		dps[a][2]=dp[a][0]; dps[a][3]=dp[a][1];
		change(a,x); 
		
		//for (int j=0;j<=3;j++) printf("%lld ",dp[1][j]); printf("\n");
		
		dps[b][2]=dp[b][0]; dps[b][3]=dp[b][1];
		change(b,y);
		
		//for (int j=0;j<=3;j++) printf("%lld ",dp[1][j]); printf("\n");
		
		if (a!=1 && b!=1) printf("%lld\n",min(dps[1][0],dps[1][1]));
		else{
			if (a==1) printf("%lld\n",dps[1][x]);
			if (b==1) printf("%lld\n",dps[1][y]);
		}
		huanyuan(a); huanyuan(b);
	}
	return 0;
}

