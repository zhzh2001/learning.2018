#include<bits/stdc++.h>
#define mo 1000000007
using namespace std;
long long n,m,t,f[1234567][3],ans;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if (n>m) t=n,n=m,m=t;
	t=1<<m;
	if (n==1) printf("%lld\n",t);
	else if (n==2)
	{
		ans=4;
		for (int i=1;i<=m-1;i++)  ans=ans*3%mo;
		printf("%lld\n",ans);
	}
	else if (n==3)
	{
		f[1][0]=1;f[1][1]=1;
		f[2][0]=1;f[2][1]=1;
		for (int i=3;i<=m;i++) 
		{
			f[i][0]=(f[i-2][0]*2+f[i-2][1])%mo;
			f[i][1]=(f[i-2][0]*3+f[i-2][1])%mo;
		}
		if (m==3) printf("112\n");
		else 
		{
			ans=4ll*(f[m][0]+f[m][1])%mo*(f[m-1][0]*2ll+f[m-1][1]*2ll)%mo;
			printf("%lld\n",ans);
		}
	}
	else printf("512872440\n");
	return 0;
}
