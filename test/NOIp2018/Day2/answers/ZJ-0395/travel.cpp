#include<bits/stdc++.h>
#define N 5123
#define sight(ch) ((ch>='0')&&(ch<='9'))
using namespace std;
int n,m,vis[N],ans[N],ss[N],b[N],f[N],t,bo,tt,tot,size[N],x,y;
struct rrsb
{
	int x,y;
}a[N];
vector <int> v[N];

int cmp(rrsb a,rrsb b)
{
	return (a.x<b.x||(a.x==b.x&&a.y<b.y));
}

int getf(int x)
{
	if (f[x]==x) return x;
	f[x]=getf(f[x]);return f[x];
}

void read(int &x)
{
	char ch=getchar();
    for (;!sight(ch);ch=getchar());
    for (;sight(ch);ch=getchar()) x=x*10+ch-'0';
}

void dfs(int x)
{
	tot++,ans[tot]=x;
	for (int i=0;i<v[x].size();i++)
	if (!vis[v[x][i]])
	{
		vis[v[x][i]]=1;
		dfs(v[x][i]);
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n),read(m);
	for (int i=1;i<=m;i++)
	{
		read(a[i].x),read(a[i].y);
		if (a[i].x>a[i].y) t=a[i].x,a[i].x=a[i].y,a[i].y=t;
	}
	sort(a+1,a+m+1,cmp);
	if (n==m+1)
	{
		for (int i=1;i<=m;i++) 
		v[a[i].x].push_back(a[i].y),v[a[i].y].push_back(a[i].x);
		for (int i=1;i<=n;i++) vis[i]=0;
		vis[1]=1;tot=0;
		dfs(1);
		for (int i=1;i<=tot-1;i++) printf("%d ",ans[i]);printf("%d",ans[tot]);
		putchar('\n');
	}
	else 
	{
		for (int i=1;i<=m;i++)
		{
			bo=1;
			for (int j=1;j<=n;j++) f[j]=j,size[j]=1;
			for (int j=1;j<=m;j++)
			if (i!=j&&bo)
			{
				x=getf(a[j].x),y=getf(a[j].y);
				if (x==y)
				{
					bo=0;break;
				}
				else 
				{
					if (size[x]>size[y]) f[y]=x,size[x]+=size[y];
					else f[x]=y,size[y]+=size[x];
				}
			}
			if (bo) tt++,b[tt]=i;
		}
		for (int u=1;u<=tt;u++)
		{
			for (int i=1;i<=n;i++) v[i].clear();
			for (int i=1;i<=m;i++) 
			if (b[u]!=i)
			v[a[i].x].push_back(a[i].y),v[a[i].y].push_back(a[i].x);
			for (int i=1;i<=n;i++) vis[i]=0;
			vis[1]=1;tot=0;
			dfs(1);
			bo=0;
			for (int i=1;i<=n;i++) 
			if (ans[i]<ss[i]) {bo=1;break;}
			else if (ans[i]>ss[i]) break;
			if (u==1) bo=1;
			if (bo) 
			for (int i=1;i<=n;i++) ss[i]=ans[i];
		}
		for (int i=1;i<=n-1;i++) printf("%d ",ss[i]);printf("%d",ss[n]);
		putchar('\n');
	}
	return 0;
}
