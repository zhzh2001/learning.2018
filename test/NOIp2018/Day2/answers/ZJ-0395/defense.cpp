#include<bits/stdc++.h>
#define N 2222
#define sight(ch) ((ch>='0')&&(ch<='9'))
#define min(a,b) ((a)>(b)?(b):(a))
using namespace std;
int n,m,x1,xx1,x2,xx2,vis[N],f[N][2],a[N],x,y;
vector <int > v[N];
char ch[111];

int pd(int x,int y)
{
	for (int i=0;i<v[x].size();i++)
	if (v[x][i]==y) return 1;
	return 0;
}

void dfs(int x)
{
	int ans=0,anss=0;
	for (int i=0;i<v[x].size();i++)
	if (!vis[v[x][i]])
	{
		vis[v[x][i]]=1;
		dfs(v[x][i]);
		ans+=min(f[v[x][i]][0],f[v[x][i]][1]);
		anss+=f[v[x][i]][1];	
	}
	if (f[x][1]==0) f[x][1]=ans+a[x];
	if (f[x][0]==0) f[x][0]=anss;
	//printf("%d %d %d\n",x,f[x][0],f[x][1]);
}

void read(int &x)
{
	x=0;char ch=getchar();
    for (;!sight(ch);ch=getchar());
    for (;sight(ch);ch=getchar()) x=x*10+ch-'0';
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n);read(m);scanf("%s",ch+1);
	for (int i=1;i<=n;i++) read(a[i]);
	for (int i=1;i<=n-1;i++)
	{
		read(x),read(y);
		v[x].push_back(y);
		v[y].push_back(x);
	}
	while (m--)
	{
		read(x1),read(x2);read(xx1),read(xx2);
		for (int i=1;i<=n;i++) vis[i]=0;
		vis[1]=1;//f[1][1]=a[1];f[1][0]=0;
		memset(f,0,sizeof(f));
		if (x2==0) f[x1][1]=611433223;else f[x1][0]=611433223;
		if (xx2==0) f[xx1][1]=611433223;else f[xx1][0]=611433223;
		if (x2==xx2&&x2==0&&pd(x1,xx1)) printf("-1\n");
		else
		{
			dfs(1);
			if (min(f[1][1],f[1][0])>600000000) printf("-1\n");
			else printf("%d\n",min(f[1][1],f[1][0]));
		}
	}
	return 0;
}
