var
  ans,a,head,p,u,v,qu,next,vet:array[0..10005]of longint;
  instack,inh,vis:array[0..5005]of boolean;
  n,m,i,j,edgenum,time,num:longint;
procedure addedge(u,v:longint);
var
  t,fa:longint;
begin
  inc(edgenum);
  t:=head[u];
  fa:=0;
  while (t<>0)and(vet[t]<v) do
  begin
    fa:=t;
    t:=next[t];
  end;
  if fa=0 then head[u]:=edgenum else next[fa]:=edgenum;
  next[edgenum]:=t;
  vet[edgenum]:=v;
end;
procedure dfs2(x:longint);
var
  e,v,t:longint;
begin
  e:=head[x];
  instack[x]:=true;
  while e>0 do
  begin
    v:=vet[e];
    if not instack[v] then
    begin
      p[v]:=x;
      dfs2(v);
    end;
    if (v<>p[x])and(instack[v]) then
    begin
      t:=x;
      while t<>v do
      begin
        inh[t]:=true;
        t:=p[t];
      end;
      inh[v]:=true;
    end;
    e:=next[e];
  end;
  instack[x]:=false;
end;
procedure dfs(x:longint);
var
  e,y:longint;
begin
  inc(time);
  a[time]:=x;
  vis[x]:=true;
  e:=head[x];
  while e>0 do
  begin
    y:=vet[e];
    if not((x=u[qu[i]])and(y=v[qu[i]]) or (x=v[qu[i]])and(y=u[qu[i]])) then
      if not vis[y] then dfs(y);
    e:=next[e];
  end;
end;
begin
  assign(input,'travel.in');reset(input);
  assign(output,'travel.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
  begin
    readln(u[i],v[i]);
    addedge(u[i],v[i]);
    addedge(v[i],u[i]);
  end;
  if m=n-1 then
  begin
    i:=0;qu[0]:=0;
    dfs(1);
    for i:=1 to n do write(a[i],' ');
    writeln;
  end else
  begin
    dfs2(1);
    for i:=1 to n do ans[i]:=n;
    for i:=1 to m do
      if inh[u[i]] and inh[v[i]] then
      begin
        inc(num);
        qu[num]:=i;
      end;
    for i:=1 to num do
    begin
      dfs(1);
      time:=0;
      fillchar(p,sizeof(p),0);
      fillchar(vis,sizeof(vis),false);
      for j:=1 to n do
        if ans[j]<>a[j] then break;
      if ans[j]>a[j] then
      begin
        for j:=j to n do
          ans[j]:=a[j];
      end;
    end;
    for i:=1 to n do write(ans[i],' ');
    writeln;
  end;
  close(input);
  close(output);
end.
