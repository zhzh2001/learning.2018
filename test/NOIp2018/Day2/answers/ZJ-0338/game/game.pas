var
  n,m,t,p:longint;
  ans:int64;
function mi(b,m,p:longint):int64;
var
  q:int64;
begin
  if m=0 then exit(1);
  q:=mi(b,m shr 1,p);
  if m shr 1 shl 1=m then exit(q*q mod p) else exit(q*q mod p *b mod p);
end;
begin
  assign(input,'game.in');reset(input);
  assign(output,'game.out');rewrite(output);
  readln(n,m);
  if n>m then
  begin
    p:=n;n:=m;m:=p;
  end;
  p:=1000000007;
  t:=n*n-2*n;
  t:=t*t-2;
  if n=2 then t:=3 else
  if n=3 then t:=14 else
  if n=5 then t:=223;
  ans:=mi(n+1,m-n,p);
  ans:=ans*(1<<n) mod p;
  ans:=ans*t mod p;
  writeln(ans);
  close(input);
  close(output);
end.
