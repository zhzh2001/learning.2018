var
  f1,f:array[0..100005,0..1]of int64;
  head,next,vet,p:array[0..200005]of longint;
  n,m,edgenum,i,j,u,v,x,y,z,w:longint;
  ty:string;
function min(x,y:int64):int64;
begin
  if x<y then exit(x) else exit(y);
end;
procedure dfs(u:longint);
var
  e,v:longint;
begin
  e:=head[u];
  while e>0 do
  begin
    v:=vet[e];
    if v<>p[u] then
    begin
      p[v]:=u;
      dfs(v);
      f[u][1]:=f[u][1]+min(f[v][1],f[v][0]);
      f[u][0]:=f[u][0]+f[v][1];
    end;
    e:=next[e];
  end;
end;
procedure change(x,y:longint);
begin
  f1[x][1-y]:=maxlongint*1000;
  while p[x]>0 do
  begin
    f1[p[x]][1]:=f1[p[x]][1]-min(f[x][1],f[x][0])+min(f1[x][1],f1[x][0]);
    f1[p[x]][0]:=f1[p[x]][0]-f[x][1]+f1[x][1];
    x:=p[x];
  end;
end;
procedure addedge(u,v:longint);
begin
  inc(edgenum);
  next[edgenum]:=head[u];
  head[u]:=edgenum;
  vet[edgenum]:=v;
end;
begin
  assign(input,'defense2.in');reset(input);
  assign(output,'defense2.out');rewrite(output);
  readln(n,m,ty);
  for i:=1 to n do
    read(f[i][1]);
  for i:=1 to n-1 do
  begin
    readln(u,v);
    addedge(u,v);
    addedge(v,u);
  end;
  dfs(1);
  for i:=1 to n do
    for j:=0 to 1 do f1[i][j]:=f[i][j];
  for i:=1 to m do
  begin
    readln(x,y,z,w);
    change(x,y);
    change(z,w);
    if min(f1[1][1],f1[1][0])>=maxlongint*1000 then writeln(-1) else
    writeln(min(f1[1][1],f1[1][0]));
    while x>0 do
    begin
      f1[x][0]:=f[x][0];
      f1[x][1]:=f[x][1];
      x:=p[x];
    end;
    while z>0 do
    begin
      f1[z][0]:=f[z][0];
      f1[z][1]:=f[z][1];
      z:=p[z];
    end;
  end;
  close(input);
  close(output);
end.
