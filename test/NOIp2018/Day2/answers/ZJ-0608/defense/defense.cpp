#include <bits/stdc++.h>
using namespace std;
void judge(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
}
inline void read(int &x){
	x=0; char ch=getchar();
	while (!isdigit(ch)) ch=getchar();
	while (isdigit(ch)){
		x=x*10+ch-'0';
		ch=getchar();
	}
}
const int N=110000;
const int long long inf=100000000;
int n,m,A,B,X,Y,p[N],t[N];
long long f[N][2],sum,alb1[N],alb2[N];
string s;
bool ff[N];
struct info{
	int to,nxt;
}e[N<<1];
int head[N],opt;
void add(int x,int y){
	e[++opt]=(info){y,head[x]};
	head[x]=opt;
}
void dfs(int u,int fa){
	f[u][0]=0; f[u][1]=p[u];
	if (u==A){
		if (X==0) f[u][0]=0,f[u][1]=inf;
		else f[u][0]=inf,f[u][1]=p[u];
	}else if (u==B){
		if (Y==0) f[u][0]=0,f[u][1]=inf;
		else f[u][0]=inf,f[u][1]=p[u];
	}
	if (f[fa][1]==inf) f[u][0]=inf;
	for (int i=head[u];i;i=e[i].nxt){
		int k=e[i].to;
		if (k==fa) continue;
		dfs(k,u); //cout<<f[5][1]<<" "<<f[5][0]<<endl;
		if ((u==A&&X==0)||(u==B&&Y==0));
		else f[u][1]+=min(f[k][0],f[k][1]);
		if ((u==A&&X)||(u==B&&Y));
		else f[u][0]+=f[k][1];
	}
}
int main(){
	judge();
	cin>>n>>m>>s;
	for (int i=1;i<=n;i++) read(p[i]);
	for (int i=1;i<n;i++){
		int x,y; read(x),read(y);
		add(x,y),add(y,x);
	}
	if (s[0]=='A'&&n==100000){
		for (int i=1;i<=n;i++){
			if (p[i]<p[i-1]+p[i+1]) ff[i]=1,sum+=p[i];
			else ff[i-1]=ff[i+1]=1,sum+=p[i-1]+p[i+1];
			alb1[i]=p[i]; alb2[i]=p[i-1]+p[i+1];
		}
		for (int i=1;i<=m;i++){
			read(A),read(X),read(B),read(Y);
			if (abs(A-B)==1){printf("-1\n"); continue;}
			long long t=sum;
			if (ff[A]!=X){
				if (ff[A]==1) t-=alb1[A],t+=alb2[A];
				else t-=alb2[A],t+=alb1[A];
			}
			if (ff[B]!=Y){
				if (ff[B]==1) t-=alb1[B],t+=alb2[B];
				else t-=alb2[B],t+=alb1[B];
			}
			printf("%lld\n",t);
		}
		return 0;
	}
	for (int i=1;i<=m;i++){
		read(A),read(X),read(B),read(Y);
		memset(f,0,sizeof(f));
		dfs(1,0);
		long long ans=min(f[1][0],f[1][1]);
		if (ans>=inf) ans=-1;
		printf("%lld\n",ans);
	}
	return 0;
}