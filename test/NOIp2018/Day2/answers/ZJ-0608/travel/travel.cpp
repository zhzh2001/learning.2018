#include <bits/stdc++.h>
using namespace std;
void judge(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
}
inline void read(int &x){
	x=0; char ch=getchar();
	while (!isdigit(ch)) ch=getchar();
	while (isdigit(ch)){
		x=x*10+ch-'0';
		ch=getchar();
	}
}
const int N=5100;
int n,m,pre[N],zyk[N],cnt,wzp[N],d[N];
int X,Y,M[N][N];
bool used[N];
vector<int> v[N];
struct info{
	int to,nxt;
}e[N<<1];
int head[N],opt;
void add(int x,int y){
	e[++opt]=(info){y,head[x]};
	head[x]=opt; M[x][y]=opt;
}
void dfs(int u,int fa){
	if (used[u]) return;
	used[u]=1;
	zyk[++cnt]=u;
	int t=v[u].size();
	for (int i=0;i<t;i++){
		int k=v[u][i];
		if (k==fa) continue;
		if (!(M[u][k]||M[k][u])) add(u,k);
		dfs(k,u);
	}
}
#define pb push_back
int dep,dfn[N],top,low[N],s[N],c[N],co,sum[N];
bool vis[N];
void tarjan(int u){
	dfn[u]=low[u]=++dep;
	s[++top]=u;
	vis[u]=1;
	for (int i=head[u];i;i=e[i].nxt){
		int k=e[i].to;
		if (!dfn[k]) tarjan(k),low[u]=min(low[u],low[k]);
		else if (vis[k]) low[u]=min(low[u],dfn[k]);
	}
	if (low[u]==dfn[u]){
		c[u]=++co; vis[u]=0;
		sum[co]=1;
		while (u!=s[top]){
			vis[s[top]]=0;
			sum[co]++;
			c[s[top--]]=co;
		}
		top--;
	}
}
int alb[N],myh,fu[N],fu2[N],fy,hh;
bool f[N];
void dfs2(int u,int fa){
	wzp[++hh]=u;
	int t=v[u].size();
	for (int i=0;i<t;i++){
		int k=v[u][i];
		if (k==fa||(k==Y&&u==X)||(k==X&&u==Y)) continue;
		dfs2(k,u);
	}
}
int main(){
	judge();
	read(n),read(m);
	for (int i=1;i<=m;i++){
		int x,y; read(x),read(y);
		v[x].pb(y); v[y].pb(x);
	}
	for (int i=1;i<=n;i++) sort(v[i].begin(),v[i].end());
	dfs(1,0);
	if (m==n-1){
		for (int i=1;i<=n;i++) printf("%d ",zyk[i]);
		return 0;
	}
	for (int i=1;i<=n;i++) if (!dfn[i]) tarjan(i);
	for (int i=1;i<=n;i++){
		if (sum[c[i]]>1) alb[++myh]=i; 
	}
	for (int i=1;i<=myh;i++){
		for (int j=1;j<=myh;j++){
			if (i==j) continue;
			if (M[alb[i]][alb[j]]) fu[++fy]=alb[i],fu2[fy]=alb[j];
		}
	}
	for (int i=1;i<=fy;i++){
		hh=0; X=fu[i],Y=fu2[i];
		memset(used,0,sizeof(used));
		dfs2(1,0);\
		if (i!=1){
			bool flag=1;
			for (int j=1;j<=n;j++){
				if (wzp[j]!=zyk[j]){
					flag=(wzp[j]<zyk[j]);
					break;
				}
			}
			if (flag) for (int j=1;j<=n;j++) zyk[j]=wzp[j];
		}else for (int j=1;j<=n;j++) zyk[j]=wzp[j];
	}
	for (int i=1;i<=n;i++) printf("%d ",zyk[i]);
	return 0;
}