#include <bits/stdc++.h>
using namespace std;
void judge(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}
inline void read(int &x){
	x=0; char ch=getchar();
	while (!isdigit(ch)) ch=getchar();
	while (isdigit(ch)){
		x=x*10+ch-'0';
		ch=getchar();
	}
}
const int N=10;
int n,m;
namespace subtask1{
	const int mo=100000007;
	string s[110000],w[110000];
	int opt,ans;
	char num[N][N];
	bool used[N][N];
	void dfs2(int x,int y,string a,string b){
		if (x<1||y<1||x>n||y>m) return;
		if (x==n&&y==m){
			b=b+num[x][y];
			s[++opt]=a,w[opt]=b;
			return;
		}
		dfs2(x+1,y,a+'D',b+num[x][y]);
		dfs2(x,y+1,a+'R',b+num[x][y]);
	}
	bool check(string a,string b){
		int len=a.length();
		for (int i=0;i<len;i++){
			if (a[i]!=b[i]) return a[i]>b[i];
		}
		return 0;
	}
	void dfs1(int x,int y){
		if (x==n+1){
			opt=0; bool flag=1;
			dfs2(1,1,"","");
			/*for (int i=1;i<=opt;i++) cout<<s[i]<<" "<<w[i]<<endl;
				cout<<check(s[1],s[2])<<endl;
				cout<<endl;*/
			for (int i=1;i<=opt;i++){
				if (!flag) break;
				for (int j=1;j<=opt;j++){
					if (!flag) break;
					if (i==j) continue;
					if (check(s[i],s[j])){
						if (check(w[i],w[j])) flag=0;
					}
				}
			}
			ans=ans+flag;
			return;
		}
		if (y==m+1) return (void) (dfs1(x+1,1));
		num[x][y]='0'; dfs1(x,y+1);
		num[x][y]='1'; dfs1(x,y+1);
	}
	void Main(){
		dfs1(1,1);
		printf("%d\n",ans);
	}
}
namespace subtask2{
	const int mo=100000007;
	int pow(int p){
		int res=1,x=3;
		while (p){
			if (p&1) res=res*x%mo;
			x=x*x%mo; p>>=1;
		}
		return res;
	}
	void Main(){
		int t=pow(m-1);
		printf("%d\n",t*1ll*4%mo);
	}
}
int main(){
	judge();
	read(n),read(m);
	if (n<=3&&m<=3) return subtask1::Main(),0;
	if (n==2&&m>3) return subtask2::Main(),0;
	return 0;
}
