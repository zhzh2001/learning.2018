// Sooke bless me.
// LJC00118 bless me.
#include<bits/stdc++.h>

#define ll long long
#define INF 2147483647

int Inp(){
	int Neg = 1;
	char c = getchar();
	while(c < '0' || c > '9'){
		if(c == '-')
			 Neg = -1;
		c = getchar();
	}
	int Sum = 0;
	while(c >= '0' && c <= '9'){
		Sum = Sum * 10 + c - '0';
		c = getchar();
	}
	return Sum * Neg;
}

int Head[100010];
int End[200010];
int Next[200010];
int Cou = 0;
void Link(int a, int b){
	Next[++Cou] = Head[a];
	Head[a] = Cou;
	End[Cou] = b;
}

ll f0[100010];
ll f1[100010];
int Fa[100010];
ll c[100010];
int a, xxxx, b, yyyy;

ll min(ll a, ll b){
	return a < b ? a : b;
}

void Dfs(int Cur, int Last){
	Fa[Cur] = Last;
	ll Sum0 = 0;
	ll Sum1 = 0;
	for(int x = Head[Cur]; x != -1; x = Next[x]){
		if(End[x] != Last){
			Dfs(End[x], Cur);
			Sum0 += f1[End[x]];
			Sum1 += min(f1[End[x]], f0[End[x]]);
		}
	}
	f0[Cur] = Sum0;
	f1[Cur] = Sum1 + c[Cur];
	if(a == Cur){
		if(xxxx == 0)
			f1[Cur] = INF;
		else
			f0[Cur] = INF;
	}
	if(b == Cur){
		if(yyyy == 0)
			f1[Cur] = INF;
		else
			f0[Cur] = INF;
	}
}

int main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	memset(Head, -1, sizeof(Head));
	int n = Inp();
	int m = Inp();
	char Type[10];
	scanf("%s", Type);
	for(int i = 1; i <= n; i++)
		c[i] = Inp();
	for(int i = 1; i < n; i++){
		int a = Inp();
		int b = Inp();
		Link(a, b);
		Link(b, a);
	}
	if(Type[1] == '1'){
		Dfs(1, 0);
		for(int i = 1; i <= m; i++){
			Inp(), Inp();
			b = Inp();
			yyyy = Inp();
			ll Cur0 = f0[b];
			ll Cur1 = f1[b];
			if(yyyy == 0)
				Cur1 = INF;
			else
				Cur0 = INF;
			while(b != 1){
				ll ff0 = f0[Fa[b]] - f1[b];
				ll ff1 = f1[Fa[b]] - min(f0[b], f1[b]);
				ff0 += Cur1;
				ff1 += min(Cur0, Cur1);
				Cur0 = ff0;
				Cur1 = ff1;
				b = Fa[b];
			}
			printf("%lld\n", Cur1 >= INF ? -1 : Cur1);
		}
		return 0;
	}
	for(int i = 1; i <= m; i++){
		a = Inp();
		xxxx = Inp();
		b = Inp();
		yyyy = Inp();
		if(a == b && xxxx != yyyy){
			printf("-1\n");
			continue;
		}
		Dfs(1, -1);
		ll Ans = min(f0[1], f1[1]);
		printf("%lld\n", Ans >= INF ? -1 : Ans);
	}
}
