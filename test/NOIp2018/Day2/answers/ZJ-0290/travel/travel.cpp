// Sooke bless me.
// LJC00118 bless me.
#include<bits/stdc++.h>

#define ll long long
#define INF 2147483647

int Inp(){
	int Neg = 1;
	char c = getchar();
	while(c < '0' || c > '9'){
		if(c == '-')
			 Neg = -1;
		c = getchar();
	}
	int Sum = 0;
	while(c >= '0' && c <= '9'){
		Sum = Sum * 10 + c - '0';
		c = getchar();
	}
	return Sum * Neg;
}

int Head[5010];
int End[10010];
int Next[10010];
int Ans[5010];
int u[5010];
int v[5010];
int Min[5010];
bool Used[5010];

int Cou = 0;
int Index = 0;
void Link(int a, int b){
	Next[++Cou] = Head[a];
	Head[a] = Cou;
	End[Cou] = b;
}

void Dfs(int Cur){
	Used[Cur] = true;
	Ans[++Index] = Cur;
	int Son = 0;
	for(int x = Head[Cur]; x != -1; x = Next[x])
		Son++;
	int s[Son + 5];
	int Cnt = 0;
	for(int x = Head[Cur]; x != -1; x = Next[x])
		if(!Used[End[x]])
			s[++Cnt] = End[x];
	std::sort(s + 1, s + Cnt + 1);
	
	for(int i = 1; i <= Cnt; i++)
		Dfs(s[i]);
}

int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	memset(Head, -1, sizeof(Head));
	int n = Inp();
	int m = Inp();
	for(int i = 1; i <= m; i++){
		u[i] = Inp();
		v[i] = Inp();
		Link(u[i], v[i]);
		Link(v[i], u[i]);
	}
	if(m == n - 1){
		Index = 0;
		Dfs(1);
		for(int i = 1; i < n; i++)
			printf("%d ", Ans[i]);
		printf("%d", Ans[n]);
	} else {
		memset(Min, 127, sizeof(Min));
		for(int t = 1; t <= m; t++){
			memset(Head, -1, sizeof(Head));
			Cou = 0;
			for(int i = 1; i <= n; i++)
				if(i != t){
					Link(u[i], v[i]);
					Link(v[i], u[i]);
				}
			Index = 0;
			memset(Used, false, sizeof(Used));
			Dfs(1);
			bool flg = false;
			
			for(int i = 1; i <= n; i++){
				if(Ans[i] < Min[i]){
					flg = true;
					break;
				} else if(Ans[i] > Min[i]){
					flg = false;
					break;
				}
			}
			
			if(Index < n)
				continue;
			if(flg)
				memcpy(Min, Ans, sizeof(Ans));
		}
		for(int i = 1; i < n; i++)
			printf("%d ", Min[i]);
		printf("%d", Min[n]);
	}
}
