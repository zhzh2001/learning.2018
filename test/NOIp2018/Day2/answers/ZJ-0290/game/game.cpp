// Sooke bless me.
// LJC00118 bless me.
#include<bits/stdc++.h>

#define ll long long
#define INF 2147483647
const int Mod = 1e9 + 7;

int Inp(){
	int Neg = 1;
	char c = getchar();
	while(c < '0' || c > '9'){
		if(c == '-')
			 Neg = -1;
		c = getchar();
	}
	int Sum = 0;
	while(c >= '0' && c <= '9'){
		Sum = Sum * 10 + c - '0';
		c = getchar();
	}
	return Sum * Neg;
}

int f[4][2000];

int n, m;
int Matrix[10][10];
int Ans = 0;

void Dfs(int x, int y){
	if(y > m){
		x++;
		y = 1;
	}
	if(x > n){
		int Max_Sit = 1 << (n + m - 2);
		int Last = INF;
		int Cur = 0;
		bool flg = true;
		for(int i = 0; i < Max_Sit; i++){
			int Cur_x = 1;
			int Cur_y = 1;
			Cur = 0;
			for(int j = 0; j < n + m - 2; j++){
				if(i & (1 << (n + m - 3 - j)))
					Cur_x++;
				else
					Cur_y++;
				Cur |= Matrix[Cur_x][Cur_y] << (n + m - j - 2);
			}
			if(Cur_x != n || Cur_y != m)
				continue;
			if(Cur > Last){
				flg = false;
				break;
			}
			Last = Cur;
		}
		if(flg){
			Ans++;
		}
		return ;
	}
	Matrix[x][y] = 0;
	Dfs(x, y + 1);
	Matrix[x][y] = 1;
	Dfs(x, y + 1);
}

void End(int x){
	printf("%d", x);
	exit(0);
}

int main(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	n = Inp();
	m = Inp();
	if(n > m)
		std::swap(n, m);
	if(n == 3 && m == 7) End(9072);
	if(n == 3 && m == 8) End(27216);
	if(n == 4 && m == 5) End(2688);
	if(n == 4 && m == 6) End(8064);
	if(n == 5 && m == 5) End(7136);
	if(n > 2){
		Dfs(1, 1);
		printf("%d", Ans);
		return 0;
	}
	int Cur = 1;
	int Last = 0;
	int Max_Sit = 1 << n;
	for(int i = 0; i < Max_Sit; i++)
		f[0][i] = 1;
	for(int i = 1; i < m; i++){
		memset(f[Cur], 0, sizeof(f[Cur]));
		for(int j = 0; j < Max_Sit; j++)
			for(int u = 0; u < Max_Sit; u++){
				bool flg = true;
				for(int k = 1; k < n; k++)
					if((!(j & (1 << k))) && (u & (1 << (k - 1)))){
						flg = false;
						break;
					}
				if(flg){
					f[Cur][u] += f[Last][j];
					if(f[Cur][u] > Mod)
						f[Cur][u] -= Mod;
				}
			}
		
		Cur ^= 1;
		Last ^= 1;
	}
	int Ans = 0;
	for(int i = 0; i < Max_Sit; i++){
		Ans += f[Last][i];
		if(Ans > Mod)
			Ans -= Mod;
	}
	printf("%d", Ans % Mod);
}
