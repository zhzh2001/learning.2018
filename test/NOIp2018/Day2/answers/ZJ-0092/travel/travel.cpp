#include<iostream>
#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;
const int N=5050;
int n,m,cnt=0,uu,vv;
vector<int> ve[N];
int cnts=0;
int ls[N],rs[N];
int ans[N],hed[N];
int cntp;
int p[N],fa[N];
bool c[N],vis[N],mp[N][N];
struct a{
	int r,nxt;
}e[N<<1];
void insert(int u,int v){
	e[++cnt].r=v;e[cnt].nxt=hed[u];hed[u]=cnt;
}
void bfs(int x){
	vis[x]=1;
	for(int i=hed[x];i;i=e[i].nxt)
		if(e[i].r!=fa[x]){
			if(vis[e[i].r]){
				uu=x;vv=e[i].r;
			}
			else {
				fa[e[i].r]=x;
				bfs(e[i].r);
			}
		}
}
void dfs(int x){
	p[++cntp]=x;
	for(int i=hed[x];i;i=e[i].nxt)
		if(e[i].r!=fa[x]&&mp[x][e[i].r]==0){
			fa[e[i].r]=x;
			dfs(e[i].r);
		}
}
void chk(){
	for(int i=1;i<=n;++i) 
		if(p[i]>ans[i]) return;
		else if(p[i]<ans[i]) break;
	for(int i=1;i<=n;++i) ans[i]=p[i];
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;++i) ans[i]=n+1;
	int u,v;
	for(int i=1;i<=m;++i){
		scanf("%d%d",&u,&v);
		ve[u].push_back(v);
		ve[v].push_back(u);
	}
	for(int i=1;i<=n;++i) sort(ve[i].begin(),ve[i].end());
	for(int i=1;i<=n;++i)
		for(int j=ve[i].size()-1;j>=0;--j)
			insert(i,ve[i][j]);
	if(m<n){
		cntp=0;
		dfs(1);
		for(int i=1;i<=n;++i){
			printf("%d",p[i]);
			if(i==n) puts("");
			else putchar(' ');
		}
		return 0;
	}
	bfs(1);
	swap(uu,vv);
	++cnts;ls[cnts]=uu;rs[cnts]=vv;
	while(uu!=vv){
		++cnts;ls[cnts]=uu;rs[cnts]=fa[uu];
		uu=fa[uu];
	}
	for(int i=1;i<=cnts;++i){
		mp[rs[i]][ls[i]]=mp[ls[i]][rs[i]]=1;
		cntp=0;
		dfs(1);
		chk();
		mp[rs[i]][ls[i]]=mp[ls[i]][rs[i]]=0;
	}
	for(int i=1;i<=n;++i){
		printf("%d",ans[i]);
		if(i==n) puts("");
		else putchar(' ');
	}
	return 0;
}
