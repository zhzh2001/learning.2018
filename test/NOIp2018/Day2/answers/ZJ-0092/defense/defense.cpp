#include<iostream>
#include<cstdio>
#define LL long long
using namespace std;
const int N=1e5+10;
const LL inf=1e11;
LL ans;
int n,m,cnt,A,X,B,Y;
int fa[N],hed[N];
LL val[N],f[N][2][2];
LL sum[N];
char s[20];
struct a{
	int r,nxt;
}e[N<<1];
void insert(int u,int v){
	e[++cnt].r=v;e[cnt].nxt=hed[u];hed[u]=cnt;
}
LL g[N];
int cc=0;
int qq[N];
void dfs(int x){
	f[x][0][0]=0;
	f[x][0][1]=inf;
	f[x][1][1]=val[x];
	sum[x]=0;
	for(int i=hed[x];i;i=e[i].nxt)
		if(e[i].r!=fa[x]){
			fa[e[i].r]=x;
			g[e[i].r]=inf;
			dfs(e[i].r);
			f[x][0][0]+=f[e[i].r][0][1];
			f[x][1][1]+=min(f[e[i].r][0][0],min(f[e[i].r][0][1],f[e[i].r][1][1]));
			sum[x]+=min(f[e[i].r][1][1],f[e[i].r][0][1]);
		}
	LL PP=inf,SS=inf;
	for(int i=hed[x];i;i=e[i].nxt)
		if(e[i].r!=fa[x]){
			qq[++cc]=e[i].r;
			sum[x]-=min(f[e[i].r][1][1],f[e[i].r][0][1]);
			SS=min(SS,sum[x]+f[e[i].r][1][1]);
			g[e[i].r]=min(g[e[i].r],SS);
			f[x][0][1]=min(f[x][0][1],sum[x]+f[e[i].r][1][1]);
			sum[x]+=min(f[e[i].r][1][1],f[e[i].r][0][1]);
		}
	for(int i=cc;i>=1;--i)
		if(qq[i]!=fa[x]){
			sum[x]-=min(f[qq[i]][1][1],f[qq[i]][0][1]);
			PP=min(PP,sum[x]+f[qq[i]][1][1]);
			g[qq[i]]=min(g[qq[i]],PP);
			sum[x]+=min(f[qq[i]][1][1],f[qq[i]][0][1]);
		}
	if(x==A)f[x][X^1][0]=f[x][X^1][1]=inf;
	if(x==B)f[x][Y^1][0]=f[x][Y^1][1]=inf;
	f[x][0][0]=min(f[x][0][0],inf);
	f[x][0][1]=min(f[x][0][1],inf);
	f[x][1][1]=min(f[x][1][1],inf);
}
void del(int x,int y){
	f[x][0][0]-=f[y][0][1];
	f[x][1][1]-=min(f[y][0][0],min(f[y][0][1],f[y][1][1]));
	sum[x]-=min(f[y][1][1],f[y][0][1]);
	
	f[x][0][1]=g[y];
	
	f[x][0][0]=min(f[x][0][0],inf);
	f[x][0][1]=min(f[x][0][1],inf);
	f[x][1][1]=min(f[x][1][1],inf);
}
void add(int x,int y){
	f[x][0][0]+=f[y][0][1];
	f[x][1][1]+=min(f[y][0][0],min(f[y][0][1],f[y][1][1]));
	sum[x]+=min(f[y][1][1],f[y][0][1]);
	
	sum[x]-=min(f[y][1][1],f[y][0][1]);
	f[x][0][1]=min(f[x][0][1],sum[x]+f[y][1][1]);
	sum[x]+=min(f[y][1][1],f[y][0][1]);
	
	f[x][0][0]=min(f[x][0][0],inf);
	f[x][0][1]=min(f[x][0][1],inf);
	f[x][1][1]=min(f[x][1][1],inf);
}
LL pre[N][2],suf[N][2];
int top=0;
int sta[N];
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%s",s+1);
	for(int i=1;i<=n;++i) scanf("%lld",&val[i]);
	int u,v;
	for(int i=1;i<n;++i){
		scanf("%d%d",&u,&v);
		insert(u,v);
		insert(v,u);
	}
	if(n<=2000&&m<=2000){
		while(m--){
			scanf("%d%d%d%d",&A,&X,&B,&Y);
			dfs(1);
			ans=min(f[1][0][1],f[1][1][1]);
			if(ans>=inf) printf("-1\n");
			else printf("%lld\n",ans);
		}
		return 0;
	}
	if(s[1]=='A'){
		if(s[2]=='1'){
			pre[1][0]=inf;
			pre[1][1]=val[1];
			for(int i=2;i<=n;++i){
				pre[i][1]=min(pre[i-1][0],pre[i-1][1])+val[i];
				pre[i][0]=pre[i-1][1];
			}
			suf[n][0]=0;
			suf[n][1]=val[n];
			for(int i=n-1;i>=1;--i){
				suf[i][1]=min(suf[i+1][0],suf[i+1][1])+val[i];
				suf[i][0]=suf[i+1][1];
			}
			while(m--){
				scanf("%d%d%d%d",&A,&X,&B,&Y);
				if(Y==0) ans=pre[B-1][1]+suf[B][Y];
				else ans=min(pre[B-1][0],pre[B-1][1])+suf[B][Y];
				if(ans>=inf) printf("-1\n");
				else printf("%lld\n",ans);
			}
			return 0;
		}
		else if(s[2]=='2'){
			pre[1][0]=0;
			pre[1][1]=val[1];
			for(int i=2;i<=n;++i){
				pre[i][1]=min(pre[i-1][0],pre[i-1][1])+val[i];
				pre[i][0]=pre[i-1][1];
			}
			suf[n][0]=0;
			suf[n][1]=val[n];
			for(int i=n-1;i>=1;--i){
				suf[i][1]=min(suf[i+1][0],suf[i+1][1])+val[i];
				suf[i][0]=suf[i+1][1];
			}
			while(m--){
				scanf("%d%d%d%d",&A,&X,&B,&Y);
				if(A>B){swap(A,B);swap(X,Y);}
				if(X==0&&Y==0) ans=inf;
				else ans=pre[A][X]+suf[B][Y];
				if(ans>=inf) printf("-1\n");
				else printf("%lld\n",ans);
			}
			return 0;
		}
	}
	LL aaa,bbb;
	if(s[1]=='B'){
		dfs(1);
		while(m--){
			scanf("%d%d%d%d",&A,&X,&B,&Y);
			u=B;
			while(1){
				sta[++top]=u;
				if(u==1) break;
				u=fa[u];
			}
			for(int i=top;i>1;--i) del(sta[i],sta[i-1]);
			aaa=f[B][Y^1][0];
			bbb=f[B][Y^1][1];
			f[B][Y^1][0]=f[B][Y^1][1]=inf;
			for(int i=top;i>1;--i) add(sta[i],sta[i-1]);
			ans=f[1][1][1];
			if(ans>=inf) printf("-1\n");
			else printf("%lld\n",ans);
			for(int i=top;i>1;--i) del(sta[i],sta[i-1]);
			f[B][Y^1][0]=aaa;
			f[B][Y^1][1]=bbb;
			for(int i=top;i>1;--i) add(sta[i],sta[i-1]);
		}
		return 0;
	}
}
