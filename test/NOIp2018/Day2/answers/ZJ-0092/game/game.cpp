#include<iostream>
#include<cstdio>
#define LL long long
using namespace std;
const LL mod=1e9+7;
int n,m;
LL ans;
LL pow(LL x,LL y){
	LL re=1;
	while(y){
		if(y&1) re=re*x%mod;
		x=x*x%mod;y>>=1;
	}
	return re;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m<n) swap(n,m);
	if(n==1){
		ans=pow((LL)2,(LL)m);
		printf("%lld\n",ans);
		return 0;
	}
	if(n==2){
		ans=pow((LL)3,(LL)(m-1));
		ans=(ans+ans)%mod;
		ans=(ans+ans)%mod;
		printf("%lld\n",ans);
		return 0;
	}
	LL re;
	if(n==3){
		ans=pow((LL)3,(LL)(m-3));
		ans=(LL)16*ans%mod;
		for(LL k=1;k<=m-2;++k){
			re=3;
			if(k==m-2) re=9;
			else{
				if(k+1<=m-2) re=re*4;
				if(k+2<=m-2) re=re*pow((LL)3,(LL)(m-2-(k+2)+1))%mod;
				re=(re+re)%mod;
			}
			ans=(ans+re)%mod;
		}
		ans=(ans+3)%mod;
		ans=(ans+ans)%mod;
		ans=(ans+ans)%mod;
		printf("%lld\n",ans);
		return 0;
	}
}
