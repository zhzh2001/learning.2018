#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,u) for (register int i=first[u];i;i=last[i])
#define mp make_pair
using namespace std;
typedef long long ll;
typedef pair<int,int> pa;
inline ll read(){
	ll x=0,f=1;char c=getchar();
	while ((c<'0'||c>'9')&&(c!='-')) c=getchar();
	if (c=='-') f=-1,c=getchar();
	while (c>='0'&&c<='9') x=x*10+c-'0',c=getchar();
	return x*f;
}
const int N = 300010;
int n,m,x,y,a,b;
ll p[N];
char opt[5];
set<pa>s;
int tot,first[N],last[N<<1],to[N<<1];
inline void Add(int x,int y){to[++tot]=y,last[tot]=first[x],first[x]=tot;}
inline ll min(ll a,ll b){return a<b?a:b;}
namespace Subtask1{
	ll dp[N][2];
	inline void dfs(int u,int fa){
		if (u==x&&a==1||u==y&&b==1) dp[u][0]=-1;
			else dp[u][0]=0;
		if (u==x&&a==0||u==y&&b==0) dp[u][1]=-1; 
			else dp[u][1]=p[u];
		cross(i,u){
			int v=to[i];
			if (v==fa) continue;
			dfs(v,u);
			if (dp[u][0]!=-1) 
				if (dp[v][1]==-1) dp[u][0]=-1; 
					else dp[u][0]+=dp[v][1];
			if (dp[u][1]!=-1) dp[u][1]+=min(dp[v][0]==-1?1e9:dp[v][0],dp[v][1]==-1?1e9:dp[v][1]);
		}
	}
	inline void Main(){
		For(i,1,m){
			x=read(),a=read(),y=read(),b=read();
			if (a==0&&b==0){
				if (s.find(mp(x,y))!=s.end()){puts("-1");continue;}
			}
			dfs(1,0);
			if (dp[1][0]==-1) printf("%lld\n",dp[1][1]);
			else if (dp[1][1]==-1) printf("%lld\n",dp[1][0]);
			else printf("%lld\n",min(dp[1][0],dp[1][1]));
			//For(i,1,n) printf("%lld %lld\n",dp[i][0],dp[i][1]);puts("");
			//printf("%lld\n",min((dp[1][0]==-1?1e9:dp[1][0]),(dp[1][1]==-1?1e9:dp[1][1])));
		}
	}
}
namespace Subtask2{
	ll f[N][2],g[N][2];
	inline void  Main(){
		f[1][0]=0,f[1][1]=p[1];
		For(i,2,n) f[i][0]=f[i-1][1],f[i][1]=min(f[i-1][1],f[i-1][0])+p[i];
		g[n][0]=0,g[n][1]=p[n];
		Dow(i,n-1,1) g[i][0]=g[i+1][1],g[i][1]=min(g[i+1][1],g[i+1][0])+p[i];
	//	For(i,1,n) printf("%lld ",f[i][0]);puts("");
	//	For(i,1,n) printf("%lld ",f[i][1]);puts("");
	//	For(i,1,n) printf("%lld ",g[i][0]);puts("");
	//	For(i,1,n) printf("%lld ",g[i][1]);puts("");
		while (m--){
			x=read(),a=read(),y=read(),b=read();
			if (x>y) swap(x,y);
			if (a==0&&b==0){puts("-1");continue;}
			ll ans=0;
			if (a==1&&b==1){
				ans=p[x]+p[y]+min(g[y+1][0],g[y+1][1])+min(f[x-1][0],f[x-1][1]);
				printf("%lld\n",ans);
			} else if (a==1){
				ans=p[x]+g[y+1][1]+min(f[x-1][0],f[x-1][1]);
				printf("%lld\n",ans);
			} else {
				ans=p[y]+f[x-1][1]+min(g[y+1][0],g[y+1][1]);
				printf("%lld\n",ans);
			}
		}
	}
}
namespace Subtask3{
	ll f[N][2],g[N][2];
	inline void  Main(){
		f[1][0]=p[1],f[1][1]=p[1],f[2][1]=f[1][1]+p[2],f[2][0]=f[1][1];
		For(i,3,n) f[i][0]=f[i-1][1],f[i][1]=min(f[i-1][1],f[i-1][0])+p[i];
		g[n][0]=0,g[n][1]=p[n];
		Dow(i,n-1,2) g[i][0]=g[i+1][1],g[i][1]=min(g[i+1][1],g[i+1][0])+p[i];
		g[1][0]=p[1];
		while (m--){
			x=read(),a=read(),y=read(),b=read();
			ll ans=0;
			if (b==1) ans=min(g[y+1][0],g[y+1][1])+p[y]+min(f[y-1][0],f[y-1][1]);
				else ans=g[y+1][1]+f[y-1][1];
			printf("%lld\n",ans);
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),scanf("%s",opt+1);
	For(i,1,n) p[i]=read();
	For(i,1,n-1) x=read(),y=read(),Add(x,y),Add(y,x),s.insert(mp(x,y)),s.insert(mp(y,x));
	if (n<=2000) return Subtask1::Main(),0;
	if (opt[1]=='A'&&opt[2]=='2') return Subtask2::Main(),0;
	if (opt[1]=='A'&&opt[2]=='1') return Subtask3::Main(),0;
}
/*
7 2 A2
2 1 3 4 5 2 1
1 2
2 3
3 4
4 5
5 6
6 7
4 1 5 0
4 0 5 1
*/
