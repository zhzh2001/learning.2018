#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,u) for (register int i=first[u];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0,f=1;char c=getchar();
	while ((c<'0'||c>'9')&&(c!='-')) c=getchar();
	if (c=='-') f=-1,c=getchar();
	while (c>='0'&&c<='9') x=x*10+c-'0',c=getchar();
	return x*f;
}
int n,m;
namespace Subtask1{
	int tot,L[50000];
	inline void dfs(int l,int r,int sum,int p){
		if (l==n&&r==m){L[++tot]=sum;return;}
		if (l<n) dfs(l+1,r,sum,p>>1);
		if (r<m) dfs(l,r+1,sum+p,p>>1);
	}
	int ans,a[10][10],v[50000];
	inline bool check(){
		For(i,1,tot){
			int l=1,r=1,p=n+m-2;v[i]=a[1][1]*(1<<p);
			Dow(j,n+m-3,0){
				if (L[i]&(1<<j)) r++;
					else l++;
				v[i]+=a[l][r]*(1<<p),p--;
			}
		}
	//	For(i,1,n){
	//		For(j,1,m) printf("%d",a[i][j]);
	//		puts("");
	//	}
	//	For(i,1,tot) printf("%d %d\n",L[i],v[i]);puts("");
		For(i,1,tot)
			For(j,1,tot) if (L[i]>L[j]&&v[i]>v[j]) return 0;
		return 1;
	}	
	inline void dfs(int l,int r){
		if (l>n){ans+=check();return;}
		a[l][r]=0;
		if (r<m) dfs(l,r+1);
			else dfs(l+1,1);
		a[l][r]=1;
		if (r<m) dfs(l,r+1);
			else dfs(l+1,1);
	}
	inline void Main(){
		dfs(1,1,0,1<<(n+m-3));
		dfs(1,1);
		printf("%d\n",ans);
	}
}
const int mod = 1e9+7;
inline int power(int a,int b){
	int ans=1;
	for (;b;b>>=1,a=1ll*a*a%mod) if (b&1) ans=1ll*ans*a%mod;
	return ans;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if (n<=3&&m<=3) return Subtask1::Main(),0;
	if (n==1) return printf("%d\n",power(2,m)),0;
	if (n==2) return printf("%d\n",4ll*power(3,m-1)%mod),0;
	if (n==3) return printf("%d\n",112ll*power(3,m-3)%mod),0;
}
