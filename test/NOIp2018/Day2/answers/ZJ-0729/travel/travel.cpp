#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,u) for (register int i=first[u];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0,f=1;char c=getchar();
	while ((c<'0'||c>'9')&&(c!='-')) c=getchar();
	if (c=='-') f=-1,c=getchar();
	while (c>='0'&&c<='9') x=x*10+c-'0',c=getchar();
	return x*f;
}
const int N = 5010;
int n,m,x,y;
vector<int>son[N];
int tot,first[N],to[N<<1],last[N<<1];
inline void Add(int x,int y){to[++tot]=y,last[tot]=first[x],first[x]=tot;}
namespace Subtask1{
	inline void dfs(int u,int fa){
		vector<int>son;printf("%d ",u);
		cross(i,u){
			int v=to[i];
			if (v==fa) continue;
			son.push_back(v);
		}
		if (son.empty()) return;
		sort(son.begin(),son.end());
		For(i,0,son.size()-1) dfs(son[i],u);
	}
}
int q[N],top,h[N],cnt;
bool vis[N],flag;
inline void dfs(int u,int fa){
	vis[u]=1,q[++top]=u;
	cross(i,u){
		int v=to[i];
		if (flag) return;
		if (v==fa) continue;
		if (vis[v]){
			while (q[top+1]!=v) h[++cnt]=q[top--];
			flag=1;return;
		} else dfs(v,u); 
	}
	top--;
}
int Cnt,now[N],ans[N];
inline void Dfs(int u,int fa){
	now[++Cnt]=u;
	For(i,0,son[u].size()-1){
		int v=son[u][i];
		if (v==fa||(u==x&&v==y)||(u==y&&v==x)) continue;
		Dfs(v,u);
	}
}
inline bool check(){
	For(i,1,n) 
		if (ans[i]<now[i]) return 0;
		else if (ans[i]>now[i]) return 1;
	return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	For(i,1,m) x=read(),y=read(),Add(x,y),Add(y,x),son[x].push_back(y),son[y].push_back(x);
	if (m==n-1) return Subtask1::dfs(1,0),0;
	For(i,1,n) sort(son[i].begin(),son[i].end());
	dfs(1,0);
	x=h[1],y=h[2],Dfs(1,0);
	For(i,1,n) ans[i]=now[i];
	For(i,2,cnt-1){
		Cnt=0,x=h[i],y=h[i+1],Dfs(1,0);
		if (check()) For(i,1,n) ans[i]=now[i];
	}
	x=h[cnt],y=h[1],Cnt=0,Dfs(1,0);
	if (check()) For(i,1,n) ans[i]=now[i];
	For(i,1,n) printf("%d ",ans[i]);
}
