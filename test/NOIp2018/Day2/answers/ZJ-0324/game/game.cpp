#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdio>
#include<cmath>
#define ll long long
#define rep(i,j,k) for(int i=j;i<=k;++i)
#define drp(i,j,k) for(int i=j;i>=k;--i)
#define cross(i,x) for(int i=h[x];i;i=e[i].n)
using namespace std;
char buf[25];
const ll p=1000000007;
ll ans;
int n,m;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+(ch^48);ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if(x<0){putchar('-');x=-x;}
	if(x==0){putchar('0');return;}
	int cnt=0;
	while(x){buf[++cnt]='0'+x%10;x/=10;}
	drp(i,cnt,1) putchar(buf[i]);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if(n==1||m==1){
		ans=1;
		rep(i,1,n*m) ans=(ans*2)%p;
	}
	else if(n==2||m==2){
		ans=2*2;
		rep(i,1,max(n,m)-1) ans=(ans*3)%p;
	}
	else if(n==3&&m==3) ans=112;
	write(ans);
	return 0;
}
