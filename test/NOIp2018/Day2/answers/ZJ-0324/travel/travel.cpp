#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdio>
#include<cmath>
#define rep(i,j,k) for(int i=j;i<=k;++i)
#define drp(i,j,k) for(int i=j;i>=k;--i)
#define cross(i,x) for(int i=h[x];i;i=e[i].n)
using namespace std;
char buf[25];
const int N=5010;
struct edge{
	int n,t;
} e[N*2];
struct node{
	int x,y,w;
} a[N*2];
int h[N],d[N],td[N];
bool b[N];
int cnt,num,n,m;
bool flag,tt;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+(ch^48);ch=getchar();}
	return x*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}
	if(x==0){putchar('0');return;}
	int cnt=0;
	while(x){buf[++cnt]='0'+x%10;x/=10;}
	drp(i,cnt,1) putchar(buf[i]);
}
inline void addedge(int x,int y){
	e[++cnt]=(edge){h[x],y};h[x]=cnt;
}
void dfs(int x){
	//write(x);putchar('\n');
	td[++num]=x;
	if(flag==false) return;
	if(tt&&td[num]>d[num]){
		flag=false;
		return;
	}
	if(td[num]<d[num]) tt=false;
	b[x]=false;
	cross(i,x){
		int y=e[i].t;
		if(b[y]){
			dfs(y);
		}
	}
}
inline bool cmp(node x,node y){
	if(x.x==y.x) return x.y>y.y;
		else return x.x<y.x;
}	
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	rep(i,1,m){
		a[i*2-1].x=a[i*2].y=read(),a[i*2-1].y=a[i*2].x=read();
		a[i*2-1].w=a[i*2].w=i;
	}	
	sort(a+1,a+1+2*m,cmp);
	if(n==m+1){
		rep(i,1,n) h[i]=0;cnt=0;
		rep(i,1,n) d[i]=n-i+1;
		rep(i,1,m*2) addedge(a[i].x,a[i].y);
		memset(b,true,sizeof(b));flag=true;tt=true;num=0;
		dfs(1);
		rep(i,1,n) d[i]=td[i];
	}
		else{
			rep(i,1,n) d[i]=n-i+1;
			rep(i,1,m){
				rep(j,1,n) h[j]=0;
				cnt=0;
				rep(j,1,m*2)
					if(a[j].w!=i){
						addedge(a[j].x,a[j].y);
					}
				num=0;flag=true;tt=true;
				memset(b,true,sizeof(b));
				dfs(1);
				if(flag&&num==n) 
					rep(j,1,n) d[j]=td[j];
			}
		}
	rep(i,1,n-1) write(d[i]),putchar(' ');
	write(d[n]);putchar('\n');
	return 0;
}
