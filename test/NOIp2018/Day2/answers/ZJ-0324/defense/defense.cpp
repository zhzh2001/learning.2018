#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdio>
#include<cmath>
#define ll long long
#define rep(i,j,k) for(int i=j;i<=k;++i)
#define drp(i,j,k) for(int i=j;i>=k;--i)
#define cross(i,x) for(int i=h[x];i;i=e[i].n)
using namespace std;
char buf[25];
const ll INF=30000000010;
const int N=100010;
struct edge{
	int n,t;
} e[N*2];
ll f[N][2];
int n,m,cnt,tx,ty,rx,ry;
int a[N],h[N],fa[N];
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+(ch^48);ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if(x<0){putchar('-');x=-x;}
	if(x==0){putchar('0');return;}
	int cnt=0;
	while(x){buf[++cnt]='0'+x%10;x/=10;}
	drp(i,cnt,1) putchar(buf[i]);
}
inline void addedge(int x,int y){
	e[++cnt]=(edge){h[x],y};h[x]=cnt;
}
void dfs(int x){
	ll sum=0;
	f[x][0]=f[x][1]=0;
	cross(i,x){
		int y=e[i].t;
		if(y!=fa[x]){
			fa[y]=x;dfs(y);
			f[x][0]+=f[y][1];
			f[x][1]+=min(f[y][0],f[y][1]);
		}
	}
	f[x][1]+=a[x];
	if(x==tx) f[x][1^ty]=INF;
	if(x==rx) f[x][1^ry]=INF;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();
	int tmp=read();
	rep(i,1,n) a[i]=read();
	rep(i,1,n-1){
		int x=read(),y=read();
		addedge(x,y);addedge(y,x);
	}
	rep(j,1,m){
		tx=read(),ty=read(),rx=read(),ry=read();
		dfs(1);
		ll data=min(f[1][0],f[1][1]);
		if(data<INF){write(data);putchar('\n');}
			else{write(-1);putchar('\n');}
	}
	return 0;
}
