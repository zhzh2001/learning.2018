#include<bits/stdc++.h>
using namespace std;

int read() {
	int x=0,f=1;char ch=getchar();
	for(;ch<'0' || ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0' && ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}

const int N=8,M=1e6+7,mod=1e9+7;
int n,m;

namespace Subtask1 {
	void Solve() {
		int Ans=1;
		for(int i=1;i<=m;++i)Ans=2LL*Ans%mod;
		printf("%d\n",Ans);
	}
}
namespace Subtask2 {
	void Solve() {
		int Ans=1;
		for(int i=1;i<m;++i)Ans=3LL*Ans%mod;
		Ans=4LL*Ans%mod;
		printf("%d\n",Ans);
	}
}

int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if(m<n)swap(n,m);
	if(n==1)Subtask1::Solve();
	else if(n==2)Subtask2::Solve();
	else if(n==3 && m==3)puts("112");
	return 0;
} 
