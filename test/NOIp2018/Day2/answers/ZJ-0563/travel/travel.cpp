#include<bits/stdc++.h>
using namespace std;

int read() {
	int x=0,f=1;char ch=getchar();
	for(;ch<'0' || ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0' && ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}

const int N=5e3+7;
int n,m;
vector<int> E[N];

namespace Subtask1 {
	void DFS(int x,int f) {
		printf("%d ",x);
		for(int i=0;i<(int)E[x].size();++i) {
			int v=E[x][i];if(v==f)continue;
			DFS(v,x);
		}
	}
	void Solve() {
		for(int i=1;i<=m;++i) {
			int x=read(),y=read();
			E[x].push_back(y);E[y].push_back(x);
		}
		for(int i=1;i<=n;++i)sort(E[i].begin(),E[i].end());
		DFS(1,0);
		puts("");
	}
}
namespace Subtask2 {
	int fa[N],dep[N],Px,Py;
	int Ans[N],tmp[N];
	int dfn,flag;
	int getfa(int x) { return (fa[x]==x ? x : fa[x]=getfa(fa[x])); }
	bool Merge(int x,int y) {
		x=getfa(x);y=getfa(y);
		if(x==y)return 0;
		fa[x]=y;return 1;
	}
	void PreDFS(int x,int f) {
		fa[x]=f;dep[x]=dep[f]+1;
		for(int i=0;i<(int)E[x].size();++i) {
			int v=E[x][i];if(v==f)continue;
			if((x==Px && v==Py) || (x==Py && v==Px))continue;
			PreDFS(v,x);
		}
	}
	void DFS(int x,int f,int Tx,int Ty) {
		++dfn;
		if(!flag && x>Ans[dfn]){ flag=-1;return; }
		if(!flag && x<Ans[dfn])flag=1;
		tmp[dfn]=x;
		if(flag && dfn==n) {
			for(int i=1;i<=n;++i)Ans[i]=tmp[i];
			return;
		}
		for(int i=0;i<(int)E[x].size();++i) {
			int v=E[x][i];if(v==f)continue;
			if((x==Tx && v==Ty) || (x==Ty && v==Tx))continue;
			DFS(v,x,Tx,Ty);
			if(flag==-1)return;
		}
	}
	void Calc(int Tx,int Ty) {
		dfn=0;flag=0;
		DFS(1,0,Tx,Ty);
	}
	void Main() {
		for(int i=1;i<=n;++i)Ans[i]=n;
		Calc(Px,Py);
		if(dep[Px]<dep[Py])swap(Px,Py);
		while(dep[Px]>dep[Py])Calc(Px,fa[Px]),Px=fa[Px];
		if(Px==Py)return;
		while(Px!=Py)Calc(Px,fa[Px]),Calc(Py,fa[Py]),Px=fa[Px],Py=fa[Py];
	}
	void Solve() {
		for(int i=1;i<=n;++i) fa[i]=i;
		for(int i=1;i<=m;++i) {
			int x=read(),y=read();
			E[x].push_back(y);E[y].push_back(x);
			if(!Merge(x,y))Px=x,Py=y;
		}
		for(int i=1;i<=n;++i)sort(E[i].begin(),E[i].end());
		PreDFS(1,0);
		Main();
		for(int i=1;i<=n;++i)printf("%d ",Ans[i]);
		puts("");
	}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	if(m==n-1)Subtask1::Solve();
	else if(m==n)Subtask2::Solve();
	return 0;
}
