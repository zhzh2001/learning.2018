#include<bits/stdc++.h>
using namespace std;

int read() {
	int x=0,f=1;char ch=getchar();
	for(;ch<'0' || ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0' && ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}

typedef long long LL;
const int N=1e5+7;
const LL INF=1e12;
int n,m,A[N],to[N<<1],ne[N<<1],head[N],cnt=0,fa[N],dep[N];
char s[10];

void AddEdge(int x,int y) {
	to[++cnt]=y;ne[cnt]=head[x];head[x]=cnt;
}
void PreDFS(int x,int f) {
	
	fa[x]=f;dep[x]=dep[f]+1;
	for(int i=head[x];i;i=ne[i])if(to[i]!=f) {
		PreDFS(to[i],x);
	}
}

namespace Subtask1 {
	int limit[N];
	LL F[N][2];
	void DP(int x,int f) {
		F[x][0]=0;F[x][1]=A[x];
		for(int i=head[x];i;i=ne[i])if(to[i]!=f) {
			int v=to[i];DP(v,x);
			F[x][0]=min(F[x][0]+F[v][1],INF);
			F[x][1]=min(F[x][1]+min(F[v][0],F[v][1]),INF);
		}
		if(limit[x]!=-1)F[x][!limit[x]]=INF;
	}
	void Solve() {
		memset(limit,-1,sizeof(limit));
		while(m--) {
			int x=read(),tpx=read(),y=read(),tpy=read();
			if((fa[x]==y || fa[y]==x) && (tpx==0 && tpy==0)){ 
				puts("-1");continue; 
			}
			limit[x]=tpx;limit[y]=tpy;
			DP(1,0);printf("%lld\n",min(F[1][0],F[1][1]));
			limit[x]=limit[y]=-1;
		}
	}
}
namespace Subtask2 {
	struct Matrix {
		LL w[2][2];
		Matrix operator * (const Matrix &B) const {
			Matrix C;
			for(int i=0;i<2;++i)for(int j=0;j<2;++j)C.w[i][j]=INF;
			for(int i=0;i<2;++i)for(int j=0;j<2;++j)for(int k=0;k<2;++k){
				if(w[i][k]<INF && B.w[k][j]<INF)
				C.w[i][j]=min(C.w[i][j],w[i][k]+B.w[k][j]);
			}
			return C;
		}
		void Reset() {
			w[0][0]=w[0][1]=0;
			w[1][0]=w[1][1]=INF;
		}
	}Base;
	
	namespace Seg {
		Matrix val[N<<2];
		void update(int x) {
			val[x]=val[x<<1]*val[x<<1|1];
		}
		void Build(int x,int l,int r) {
			if(l==r) {
				val[x].w[0][0]=INF;val[x].w[1][0]=0;
				val[x].w[0][1]=A[l];val[x].w[1][1]=A[l];
				return;
			}
			int mid=(l+r)>>1;
			Build(x<<1,l,mid);Build(x<<1|1,mid+1,r);
			update(x);
		}
		Matrix Query(int x,int l,int r,int L,int R) {
			if(l>=L && r<=R)return val[x];
			int mid=(l+r)>>1;
			if(R<=mid)return Query(x<<1,l,mid,L,R);
			else if(L>=mid+1)return Query(x<<1|1,mid+1,r,L,R);
			else return Query(x<<1,l,mid,L,R)*Query(x<<1|1,mid+1,r,L,R);
		}
	}
	LL Query(int L,int R) {
		if(L>R)return 0;
		Matrix X=Base*Seg::Query(1,1,n,L,R);
		
//		printf("%d %d\n",L,R);
//		printf("%d %d\n",X.w[0][0],X.w[0][1]);
		return min(X.w[0][0],X.w[0][1]);
	}
	void Solve() {
		Base.Reset();
		Seg::Build(1,1,n);
		while(m--) {
			int x=read(),tpx=read(),y=read(),tpy=read();
			if((fa[x]==y || fa[y]==x) && (tpx==0 && tpy==0)){ 
				puts("-1");continue; 
			}
			if(x>y)swap(x,y),swap(tpx,tpy);
			int L1=x-1,R1=x+1,L2=y-1,R2=y+1;
			if(!tpx)--L1,++R1;if(!tpy)--L2,++R2;
			LL Ans=Query(1,L1)+Query(R1,L2)+Query(R2,n)+1LL*tpx*A[x]+1LL*tpy*A[y]+1LL*(!tpx)*(A[x-1]+A[x+1])+1LL*(!tpy)*(A[y-1]+A[y+1]);
			printf("%lld\n",Ans);
		}
	}
}

int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();scanf("%s",s);
	for(int i=1;i<=n;++i)A[i]=read();
	for(int i=1;i<n;++i) {
		int x=read(),y=read();
		AddEdge(x,y);AddEdge(y,x);
	}
	PreDFS(1,0);
	if(n<=2000)Subtask1::Solve();
	else 
	if(s[0]=='A')Subtask2::Solve();
	return 0;
}
