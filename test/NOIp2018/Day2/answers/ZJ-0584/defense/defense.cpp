#include<cstdio>
int n,m;
struct line
{
	int u;
	int v;
	int next;
}li[100100];
int b[100100],a[100100],first[100100],an=0,ans=999999999,cut=0;
bool sw[100100];
void ad(int x,int y)
{
	cut++;
	li[cut].u=x;
	li[cut].v=y;
	li[cut].next=first[x];
	first[x]=cut;
	return;
}
void dfs(int u)
{
	if(u==n+1)
	{
		int tmp=0;
		for(int i=1;i<=n;i++)
		sw[i]=false;
		for(int i=1;i<=n;i++)
			if(b[i]==1)
			{
				sw[i]=true;
				tmp+=a[i];
				for(int k=first[i];k!=-1;k=li[k].next)
				{
					if(sw[li[k].v]==false)
					{
						sw[li[k].v]=true;
					}
				}
			}
		int to=0;
		for(int i=1;i<=n;i++)
			if(sw[i]==true)
				to++;
		if(ans>tmp&&to==n)
		{
			ans=tmp;
			an=1;
		}
		return;
	}
	if(b[u]==0)
	{
		b[u]=1;
		dfs(u+1);
		b[u]=-1;
		dfs(u+1);
		b[u]=0;
	}
	else
		dfs(u+1);
	return;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char s[5];
	int i,x,y,z,f;
	scanf("%d%d%s",&n,&m,&s);
	for(i=1;i<=n;i++)
	{
		scanf("%d",&a[i]);
		first[i]=-1;
	}
	for(i=1;i<=n-1;i++)
	{
		scanf("%d%d",&x,&y);
		ad(x,y);
		ad(y,x);
	}
	while(m--)
	{
		an=0;ans=999999999;
		for(i=1;i<=n;i++){b[i]=0;}
		scanf("%d%d%d%d",&x,&y,&z,&f);
		if(y==0)	b[x]=-1;
		else	{b[x]=1;}
		if(f==0)	b[z]=-1;
		else	{b[z]=1;}
		dfs(1);
		if(an==0)
			printf("-1\n");
		else
			printf("%d\n",ans);
	}
	return 0;
}
