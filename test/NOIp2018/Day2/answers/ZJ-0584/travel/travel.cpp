#include<cstdio>
#include<algorithm>
using namespace std;
struct line
{
	int u;
	int v;
	int next;
}li[5100];
int first[10100];
int dl[5100],to=0;
int n,m,cut=0,h[5100],size=0;
int bl[5100];
bool flag,used[5100];
int mi[5100];
bool cmp(int a,int b)
{
	return a>b;
}
void ad(int x,int y)
{
	cut++;
	li[cut].u=x;
	li[cut].v=y;
	li[cut].next=first[x];
	first[x]=cut;
	return;
}
void pr()
{
	int i;
	bool chang=false;
	for(i=1;i<=n;i++)
	{
		if(mi[i]>dl[i])
		{
			chang=true;
			break;
		}
		if(mi[i]<dl[i])
		{
			chang=false;
			break;
		}
	}
	if(chang==true)
	{
		for(i=1;i<=n;i++)
			mi[i]=dl[i];
	}
	return;
}
void dfs(int u)
{
	int k,mi=999999;
	if(to==n)
	{
		pr();
		flag=true;
		return;
	}
	for(k=first[u];k!=-1;k=li[k].next)
	{
		if(used[li[k].v]==false)
		{
			used[li[k].v]=true;
			bl[li[k].v]=li[k].u;
			to++;
			dl[to]=li[k].v;
			dfs(li[k].v);
			to--;
			bl[li[k].v]=-1;
			used[li[k].v]=false;
		}
	}
	if(bl[u]!=-1)
		dfs(bl[u]);
	return;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int i,x,y,kz,kk;
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;i++)
	{
		first[i]=-1;
		used[i]=false;
		bl[i]=-1;
		mi[i]=99999;
	}
	for(i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y);
		ad(x,y);
		ad(y,x);
	}
	flag=false;
	for(i=1;i<=n;i++)
	{
		to++;
		dl[to]=i;
		used[i]=true;
		dfs(i);
		used[i]=false;
		if(flag=true)
			break;
	}
	for(i=1;i<=n;i++)
		printf("%d ",mi[i]);
	return 0;
}
