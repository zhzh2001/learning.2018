#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin))?EOF:*p1++)
#define Out() (fwrite(St,1,Top,stdout))
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
using namespace std;
const int TT=1000000007;
int fn,Top;
static char St[1000000],buf[1000000],nb[100],*p1=buf,*p2=buf;
int N,M;
int g[10][1000005],num[10][1000005],F[10][1000005];
inline int read(){
	int ret=0;bool f=0;char ch=gt();
	while(ch<'0'||ch>'9') f^=(ch=='-'),ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return f?-ret:ret;
}
inline void write(int x){
	fn=0,x<0?pt('-'),x=-x:x;
	do nb[++fn]=x%10+'0',x/=10;while(x);
	while(fn>0) pt(nb[fn--]);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	N=read(),M=read();
	if(N==1||M==1) return write(0),Out(),0;
	if(N==2&&M==2) return write(12),Out(),0;
	if(N==3&&M==3) return write(112),Out(),0;
	return Out(),0;
}
