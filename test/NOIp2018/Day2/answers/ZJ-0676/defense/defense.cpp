#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin))?EOF:*p1++)
#define Out() (fwrite(St,1,Top,stdout))
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
using namespace std;
const int maxn=300005,maxe=600005;
int fn,Top;
static char St[1000000],buf[1000000],nb[100],*p1=buf,*p2=buf;
int N,M,e;
int F[maxn][2];
int P[maxn],fa[maxn],num[maxn];
int lnk[maxn],son[maxe],nxt[maxe];
inline int read(){
	int ret=0;bool f=0;char ch=gt();
	while(ch<'0'||ch>'9') f^=(ch=='-'),ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return f?-ret:ret;
}
inline void write(int x){
	fn=0,x<0?pt('-'),x=-x:x;
	do nb[++fn]=x%10+'0',x/=10;while(x);
	while(fn>0) pt(nb[fn--]);
}
inline void Add(int x,int y){son[++e]=y,nxt[e]=lnk[x],lnk[x]=e;}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	N=read(),M=read();
	if(gt()=='A'){
		for(int i=1;i<=N;i++) P[i]=read();
		for(int i=1,x,y;i<N;i++) x=read(),y=read(),Add(x,y),Add(y,x);
		for(int a,b,x,y;M--;){
			memset(F,0,sizeof F);
			a=read(),x=read(),b=read(),y=read();
			if((a==b-1||b==a-1)&&!x&&!y) write(-1);
			else for(int i=1;i<=N;i++){
				if(i!=a&&i!=b) F[i][0]=F[i-1][1],F[i][1]=min(F[i-1][1]+P[i],F[i-1][0]+P[i]);
				else if(i==a){
					if(!x) F[i][0]=F[i-1][1],F[i][1]=1<<30;
					else F[i][1]=min(F[i-1][0]+P[i],F[i-1][1]+P[i]),F[i][0]=1<<30;
				}
				else if(i==b){
					if(!y) F[i][0]=F[i-1][1],F[i][1]=1<<30;
					else F[i][1]=min(F[i-1][0]+P[i],F[i-1][1]+P[i]),F[i][0]=1<<30;
				}
			}
			printf("%d\n",max(F[N][0],F[N][1]));
		}
	}
	return Out(),0;
}

