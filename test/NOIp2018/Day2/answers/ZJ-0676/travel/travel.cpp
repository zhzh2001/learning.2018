#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin))?EOF:*p1++)
#define Out() (fwrite(St,1,Top,stdout))
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
using namespace std;
int fn,Top;
static char St[1000000],buf[1000000],nb[100],*p1=buf,*p2=buf;
int N,M,e,len,cnt,num;
int fa[5005],A[5005],Ans[5005];
int t[5005],id[5005],son[5005][5005];
bool No,Yes,NotNed[5005],vis[5005],flg[5005][5005];
struct zrz{int x,y;}E[5005];
inline int read(){
	int ret=0;bool f=0;char ch=gt();
	while(ch<'0'||ch>'9') f^=(ch=='-'),ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return f?-ret:ret;
}
inline int F(int x){return fa[x]==x?x:fa[x]=F(fa[x]);}
inline void write(int x){
	fn=0,x<0?pt('-'),x=-x:x;
	do nb[++fn]=x%10+'0',x/=10;while(x);
	while(fn>0) pt(nb[fn--]);
}
inline void Dfs(int x){
	vis[x]=1,write(x),pt(++cnt==N?'\n':' ');
	for(;id[x]<=t[x];id[x]++)
	  if(!vis[son[x][id[x]]]) Dfs(son[x][id[x]]);
}
inline void Dfs_(int x){
	vis[x]=1,A[++len]=x;
	if(A[len]<Ans[len]) Yes=1;
	if(A[len]>Ans[len]&&!Yes) return (void)(No=1);
	if(len==N){
		for(int i=1;i<=N;i++) Ans[i]=A[i];
		return (void)(Yes=1);
	}
	for(;id[x]<=t[x]&&!No;id[x]++)
	  if(!vis[son[x][id[x]]]&&!flg[x][son[x][id[x]]]) Dfs_(son[x][id[x]]);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	N=read(),M=read();
	if(N-1==M){
		for(int i=1;i<=M;i++){
			int x=read(),y=read();
			son[x][++t[x]]=y,son[y][++t[y]]=x;
		}
		for(int i=1;i<=N;i++)
		  id[i]=1,sort(son[i]+1,son[i]+1+t[i]);
		return Dfs(1),Out(),0;
	}
	else{
		for(int i=1;i<=M;i++){
			E[++e]=(zrz){read(),read()};
			son[E[i].x][++t[E[i].x]]=E[i].y;
			son[E[i].y][++t[E[i].y]]=E[i].x;
		}
		for(int i=1;i<=N;i++)
		  Ans[i]=1<<30,sort(son[i]+1,son[i]+1+t[i]);
		Ans[1]=1;
		for(int i=1;i<=e;i++){
			num=0;
			for(int j=1;j<=N;j++) fa[j]=j;
			for(int j=1,fx,fy;j<=e;j++) if(j^i){
				fx=F(E[j].x),fy=F(E[j].y);
				if(fx!=fy) fa[fx]=fy;
			}
			for(int j=1;j<=N;j++) if(F(j)==j) num++;
			NotNed[i]=(num==1);
		}
		for(int i=1;i<=e;i++) if(NotNed[i]){
			for(int j=1;j<=N;j++) id[j]=1,vis[j]=0;
			len=No=Yes=0,vis[1]=1;
			flg[E[i].x][E[i].y]=flg[E[i].y][E[i].x]=1;
			Dfs_(1),flg[E[i].x][E[i].y]=flg[E[i].y][E[i].x]=0;
		}
		for(int i=1;i<=N;i++) write(Ans[i]),pt(i==N?'\n':' ');
		return Out(),0;
	}
	return Out(),0;
}
