#include <bits/stdc++.h>
using namespace std;
int n,m,cnt,a[5005],nex[10005],to[10005],x,y;
bool f[5005];
priority_queue<int> q;
void add(int x,int y)
{
	nex[++cnt]=a[x];a[x]=cnt;
	to[cnt]=y;
}
void sc(int x)
{
	int b[5005],p=0;
	for (int i=a[x];i;i=nex[i])
	  if (f[to[i]]==0)
	  	b[++p]=to[i];
	if (p==0) return;
    sort(b+1,b+p+1);
    for (int i=1;i<=p;i++)
    {
    	f[b[i]]=1;
    	printf(" %d",b[i]);
    	sc(b[i]);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(a,0,sizeof(a));
	cnt=0;
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	if (m==n-1)
	{
		memset(f,0,sizeof(f));
		f[1]=1;
		printf("1");
		sc(1);
	}
	return 0;
}
