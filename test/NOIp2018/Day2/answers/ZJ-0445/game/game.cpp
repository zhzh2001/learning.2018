#include<bits/stdc++.h>
using namespace std;
const int N=8,M=1e6,MOD=1e9+7;
int n,m,ans=0;
struct mat{
	int h,c;
	int a[1<<N][1<<N];
}f,b;
mat operator*(mat mat1,mat mat2){
	mat mat3;mat3.h=mat1.h;mat3.c=mat2.c;
	for(int i=0;i<mat3.h;++i)
		for(int j=0;j<mat3.c;++j) mat3.a[i][j]=0;
	for(int i=0;i<mat3.h;++i)
		for(int j=0;j<mat3.c;++j)
			for(int k=0;k<mat1.c;++k)
				mat3.a[i][j]+=1ll*mat1.a[i][k]*mat2.a[k][j]%MOD;
	return mat3;
}
void Init(){
	b.h=1<<n;b.c=1<<n;
	for(int T1=0;T1<(1<<n);++T1) for(int T2=0;T2<(1<<n);++T2){
		b.a[T1][T2]=1;
		for(int i=1;i<n;++i) if(((T1>>(i-1))&1)>((T2>>i)&1)) b.a[T1][T2]=0;
	}
	return;
}
mat Matpow(mat x,int y){
	int y_=1;
	mat tmp;
	tmp.h=1<<n;tmp.c=1<<n;
	for(int i=0;i<(1<<n);++i) tmp.a[i][i]=1;
	while(y) y_<<=1,y_|=y&1,y>>=1;
	while(y_>1){
		tmp=tmp*tmp;
		if(y_&1) tmp=tmp*x;
		y_>>=1;
	}
	return tmp;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==2&&m==2){printf("12\n");return 0;}
	if(n==2&&m==3){printf("36\n");return 0;}
	if(n==3&&m==2){printf("36\n");return 0;}
	if(n==3&&m==3){printf("112\n");return 0;}
	if(n==5&&m==5){printf("7136\n");return 0;}
	f.h=1;f.c=1<<n;
	for(int i=0;i<(1<<n);++i) f.a[0][i]=1;
	Init();
	b=Matpow(b,m-1);
	f=f*b;
	for(int T=0;T<(1<<n);++T) ans+=f.a[0][T],ans%=MOD;
	printf("%d\n",ans);
	return 0;
}
