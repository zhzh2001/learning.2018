#include<bits/stdc++.h>
using namespace std;
const int N=3e5+9;
const long long INF=3e10+9;
int n,m;
int t[N];
long long ans,f[N],f_[N];
string s;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);cin>>s;
	for(int i=1;i<=n;++i) scanf("%d",&t[i]);
	f[0]=INF;f[1]=t[1];f[2]=t[2];
	for(int i=3;i<=n;++i){
		f[i]=t[i]+min(f[i-1],min(f[i-2],f[i-3]));
	}
	f_[n+1]=INF;f_[n]=t[n];f_[n-1]=t[n-1];
	for(int i=n-2;i>=1;--i){
		f_[i]=t[i]+min(f[i+1],min(f[i+2],f[i+3]));
	}
	for(int i=1;i<n;++i){
		int u,v;
		scanf("%d%d",&u,&v);
	}
	for(int i=1;i<=m;++i){
		int a,x,b,y;ans=0;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if(x) ans+=f[x];else ans+=f[x-2];
		if(x) ans+=f_[y];else ans+=f_[y+2];
		printf("%lld\n",ans);
		
	}
	return 0;
}
