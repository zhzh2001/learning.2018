var
    s:string;
    p,t:array[1..1000000]of longint;
    n,m,i,j,q,x,y,min,max,u,v,a,b:longint;
begin
    assign(input,'defense.in');
    reset(input);
    assign(output,'defense.out');
    rewrite(output);
    read(n,m);
    readln(s);
    for i:=1 to n do read(p[i]);
    for i:=1 to n-1 do read(u,v);
    for i:=1 to m do begin
        read(a,x,b,y);
        t[a]:=x;
        t[b]:=y;
        if (abs(b-a)=1) and(x=0)and(y=0)then begin
            writeln(-1);
            continue;
        end;
        q:=0;
        if a>b then begin
            max:=a;
            min:=b;
        end else begin
            max:=b;
            min:=a;
        end;
        for j:=min-1 downto 1 do
        if(min-j)mod 2=0 then q:=q+p[j]*t[min]
        else q:=q+p[j]*(1-t[min]);
        for j:=min+1 to max-1 do
        if(j-min)mod 2=0 then q:=q+p[j]*t[min]
        else q:=q+p[j]*(1-t[min]);
        for j:=max+1 to n do
        if(j-max)mod 2=0 then q:=q+p[j]*t[max]
        else q:=q+p[j]*(1-t[max]);
        writeln(q+p[a]*t[a]+p[b]*t[b]);
    end;
    close(input);
    close(output);
end.