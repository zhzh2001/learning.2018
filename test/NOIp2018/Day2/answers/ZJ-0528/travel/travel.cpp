#include<bits/stdc++.h>
#define x first
#define y second
#define FOR(a,b,c) for(int a=(b),a##_end=(c);a<=a##_end;++a)
#define ROF(a,b,c) for(int a=(b),a##_end=(c);a>=a##_end;--a)
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int INF=0x3f3f3f3f,N=5010;
template<class T>inline bool chkmin(T &A,const T B){return B<A?A=B,1:0;}
template<class T>inline bool chkmax(T &A,const T B){return A<B?A=B,1:0;}
int n,m,lim,tot,A[N],ans[N],vis[N];
struct edge{int y,id;};
bool cmp(edge a,edge b){return a.y<b.y;}
vector<edge>E[N];
void dfs(int x,int f){
	if(vis[x])return;
	A[++tot]=x;vis[x]=1;
	FOR(i,0,E[x].size()-1){
		edge y=E[x][i];
		if(y.id==lim||y.y==f)continue;
		dfs(y.y,x);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int x,y;
	scanf("%d%d",&n,&m);
	FOR(i,1,m){
		scanf("%d%d",&x,&y);
		E[x].push_back((edge){y,i});
		E[y].push_back((edge){x,i});
	}
	FOR(i,1,n)sort(E[i].begin(),E[i].end(),cmp);
	ans[1]=INF;
	if(m==n-1){
		dfs(1,0);
		FOR(i,1,n)printf("%d%c",A[i]," \n"[i==n]);
	}else{
		FOR(i,1,m){
			lim=i;
			memset(vis,tot=0,sizeof(vis));
			dfs(1,0);
			if(tot!=n)continue;
			FOR(i,1,n)if(A[i]!=ans[i]){
				if(A[i]<ans[i])memcpy(ans,A,sizeof(A));
				break;
			}
		}
		FOR(i,1,n)printf("%d%c",ans[i]," \n"[i==n]);
	}
	return 0;
}
