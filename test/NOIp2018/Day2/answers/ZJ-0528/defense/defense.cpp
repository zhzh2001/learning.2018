#include<bits/stdc++.h>
#define x first
#define y second
#define FOR(a,b,c) for(int a=(b),a##_end=(c);a<=a##_end;++a)
#define ROF(a,b,c) for(int a=(b),a##_end=(c);a>=a##_end;--a)
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int INF=0x3f3f3f3f,N=1e5+10;
const ll INFL=0x3f3f3f3f3f3f3f3f;
template<class T>inline bool chkmin(T &A,const T B){return B<A?A=B,1:0;}
template<class T>inline bool chkmax(T &A,const T B){return A<B?A=B,1:0;}
template<const int Maxn,const int Maxm>struct Link_List{
	int tot,Head[Maxn],Next[Maxm],W[Maxm];
	inline int operator [] (int x){return W[x];}
	inline void Add(int x,int y){Next[++tot]=Head[x];W[Head[x]=tot]=y;}
	#define LFOR(a,b,c) for(int a=(b).Head[c];a;a=(b).Next[a])
};Link_List<N,N<<1>E;
int n,m,a,b,u,v,A[N];
int fa[N];
ll dp[N][2],tmp[N][2];
char type[10];
void dfs(int x,int f){
	int y;
	fa[x]=f;
	LFOR(i,E,x)if((y=E[i])!=f)
		dfs(y,x);
}
namespace P1{
	bool check(){return n<=2000&&m<=2000;}
	void dfs(int x){
		int y;
		dp[x][0]=dp[x][1]=0;
		LFOR(i,E,x)if((y=E[i])!=fa[x]){
			dfs(y);
			dp[x][0]+=dp[y][1];
			dp[x][1]+=min(dp[y][0],dp[y][1]);
		}
		dp[x][1]+=A[x];
		if(u==x)dp[x][!a]=INFL;
		if(v==x)dp[x][!b]=INFL;
	}
	void Main(){
		memset(dp,0,sizeof(dp));
		while(m--){
			scanf("%d%d%d%d",&u,&a,&v,&b);
			if((fa[u]==v||fa[v]==u)&&!a&&!b){puts("-1");continue;}
			dfs(1);
			printf("%lld\n",min(dp[1][0],dp[1][1]));
		}
	}
}
namespace P2{
	bool check(){return type[1]=='B';}
	void Main(){
		P1::dfs(1);
		while(m--){
			int las;
			scanf("%d%d%d%d",&u,&a,&v,&b);
			tmp[v][b]=dp[v][b];
			tmp[v][!b]=INFL;
			las=v;
			v=fa[v];
			while(v){
				tmp[v][0]=dp[v][0];
				tmp[v][1]=dp[v][1];
				tmp[v][0]-=dp[las][1];
				tmp[v][1]-=min(dp[las][0],dp[las][1]);
				tmp[v][0]+=tmp[las][1];
				tmp[v][1]+=min(tmp[las][0],tmp[las][1]);
				las=v;
				v=fa[v];
			}
			printf("%lld\n",tmp[1][1]);
		}
	}
}
namespace P3{
	struct node{
		ll f[2][2];
		node(){memset(f,63,sizeof(f));}
		node operator + (const node &B)const{
			node C;
			FOR(a,0,1)FOR(b,0,1)if(f[a][b]!=INFL)
				FOR(c,0,1)FOR(d,0,1)if(B.f[c][d]!=INFL){
					if(!b&&!c)continue;
					chkmin(C.f[a][d],f[a][b]+B.f[c][d]);
				}
			return C;
		}
	};
	struct Segment_Tree{
		#define lson pl,mid,p<<1
		#define rson mid+1,pr,p<<1|1
		node t[N<<2];
		void Build(int pl,int pr,int p){
			if(pl==pr){
				t[p].f[0][0]=0;
				t[p].f[1][1]=A[pl];
				return;
			}
			int mid=(pl+pr)>>1;
			Build(lson);
			Build(rson);
			t[p]=t[p<<1]+t[p<<1|1];
		}
		void Update(int pl,int pr,int p,int u,int a){
			if(pl==pr){
				if(a==-1){
					t[p].f[0][0]=0;
					t[p].f[1][1]=A[pl];
				}else t[p].f[!a][!a]=INFL;
				return;
			}
			int mid=(pl+pr)>>1;
			if(u<=mid)Update(lson,u,a);
			else Update(rson,u,a);
			t[p]=t[p<<1]+t[p<<1|1];
		}
	}Tr;
	bool check(){return type[1]=='A';}
	void Main(){
		Tr.Build(1,n,1);
		while(m--){
			scanf("%d%d%d%d",&u,&a,&v,&b);
			if((fa[u]==v||fa[v]==u)&&!a&&!b){puts("-1");continue;}
			Tr.Update(1,n,1,u,a);
			Tr.Update(1,n,1,v,b);
			node res=Tr.t[1];
			ll ans=INFL;
			chkmin(ans,min(res.f[0][1],res.f[0][0]));
			chkmin(ans,min(res.f[1][1],res.f[1][0]));
			printf("%lld\n",ans);
			Tr.Update(1,n,1,u,-1);
			Tr.Update(1,n,1,v,-1);
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int x,y;
	scanf("%d%d%s",&n,&m,type+1);
	FOR(i,1,n)scanf("%d",&A[i]);
	FOR(i,1,n-1){
		scanf("%d%d",&x,&y);
		E.Add(x,y);E.Add(y,x);
	}
	dfs(1,0);
	if(P1::check())P1::Main();
	else if(P2::check())P2::Main();
	else P3::Main();
	return 0;
}
