#include<bits/stdc++.h>
#define x first
#define y second
#define FOR(a,b,c) for(int a=(b),a##_end=(c);a<=a##_end;++a)
#define ROF(a,b,c) for(int a=(b),a##_end=(c);a>=a##_end;--a)
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int INF=0x3f3f3f3f,N=1e5+10,P=1e9+7;
template<class T>inline bool chkmin(T &A,const T B){return B<A?A=B,1:0;}
template<class T>inline bool chkmax(T &A,const T B){return A<B?A=B,1:0;}
int n,m;
int A[100][100],Mn;
ll Pow(ll x,ll k){
	ll res=1;
	for(;k;k>>=1){
		if(k&1)res=res*x%P;
		x=x*x%P;
	}
	return res;
}
namespace P1{
	bool check(){return n<=3&&m<=3;}
	bool dfs(int x,int y,int sum){
		sum=sum<<1|A[x][y];
		if(x==n-1&&y==m-1){
			if(sum>=Mn){
				chkmax(Mn,sum);
				return true;
			}
			return false;
		}
		if(x<n-1)if(!dfs(x+1,y,sum))return false;
		if(y<m-1)if(!dfs(x,y+1,sum))return false;
		return true;
	}
	void Main(){
		int ans=0;
		FOR(S,0,(1<<n*m)-1){
			FOR(i,0,n*m-1)A[i/m][i%m]=S>>i&1;
			Mn=0;
			if(dfs(0,0,0))
				++ans;
		}
		printf("%d\n",ans);
	}
}
namespace P2{
	bool check(){return n<=2;}
	void Main(){
		if(n==1)printf("%lld\n",Pow(2,m));
		else printf("%lld\n",4*Pow(3,m-1)%P);
	}
}
namespace P3{
	bool check(){return n==3;}
	void Main(){
		if(m==1)printf("8\n");
		else if(m==2)printf("36\n");
		else printf("%lld\n",112*Pow(3,m-3)%P);
	}
}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n>m)swap(n,m);
	if(P1::check())
		P1::Main();
	else if(P2::check())
		P2::Main();
	else if(P3::check())
		P3::Main();
	return 0;
}
