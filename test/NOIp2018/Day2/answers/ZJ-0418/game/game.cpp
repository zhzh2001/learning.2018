#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const int MOD = 1000000007;

int n, m, ans;

inline void mul(int &x, int a) {
	x = ((long long)x * a) % MOD;
}

namespace work1 {
	int mp[10][10];
	inline bool check(int a, int b, int c, int d, int r1, int r2, int s1, int s2) {
		if (a > n || b > m || c > n || d > m) return true;
		s1 = s1 << 1 | mp[a][b];
		s2 = s2 << 1 | mp[c][d];
		if (r1 > r2 && s1 < s2)
			return false;
		bool res = 1;
		res &= check(a + 1, b, c + 1, d, r1 << 1 | 1, r2 << 1 | 1, s1, s2);
		res &= check(a + 1, b, c, d + 1, r1 << 1 | 1, r2 << 1, s1, s2);
		res &= check(a, b + 1, c + 1, d, r1 << 1, r2 << 1 | 1, s1, s2);
		res &= check(a, b + 1, c, d + 1, r1 << 1, r2 << 1, s1, s2);
		return res;
	}
	inline void dfs(int x, int y) {
		if (x > n) {
			if (check(1, 1, 1, 1, 0, 0, 0, 0))
				++ans;
			return;
		}
		if (y > m) return dfs(x + 1, 1);
		mp[x][y] = 0; dfs(x, y + 1);
		mp[x][y] = 1; dfs(x, y + 1);
	}
	void solve(void) {
		dfs(1, 1);
		cout << ans << endl;
	}
}
namespace work2 {
	void solve(void) {
		if (n > m) swap(n, m);
		ans = 1;
		for (int i = 1; i < n; i++) {
			mul(ans, i + 1);
			mul(ans, i + 1);
		}
		for (int i = 1; i <= m - n + 1; i++)
			mul(ans, n + 1);
		cout << ans << endl;
	}
}

int main(void) {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d %d", &n, &m);
	if (n <= 3 && m <= 3) work1::solve();
	else work2::solve();
	return 0;
}
