#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const int N = 101010;

inline char get(void) {
	static char buf[100000], *s = buf, *t = buf;
	if (s == t) {
		t = (s = buf) + fread(buf, 1, 100000, stdin);
		if (s == t) return EOF;
	}
	return *s++;
}
inline void read(int &x) {
	static char c;
	for (c = get(); c < '0' || c > '9'; c = get());
	for (x = 0; c >= '0' && c <= '9'; c = get())
		x = x * 10 + c - '0';
}

struct edge {
	int to, next;
	edge(int t = 0, int n = 0): to(t), next(n) {}
} G[N << 1];
int head[N], p[N];
int n, m, a, b, c, d, x, y, gcnt;
char t1, t2;

inline void addEdge(int from, int to) {
	G[++gcnt] = edge(to, head[from]); head[from] = gcnt;
	G[++gcnt] = edge(from, head[to]); head[to] = gcnt;
}

namespace work1 {
	const int INF = 1 << 29;
	
	int dp[N][2];
	inline void dfs(int u, int f) {
		int to;
		dp[u][0] = dp[u][1] = INF;
		int g0 = 0, g1 = 0;
		for (int i = head[u]; i; i = G[i].next) {
			to = G[i].to; if (to == f) continue;
			dfs(to, u);
			g0 = min(INF, g0 + dp[to][1]);
			g1 = min(INF, g1 + min(dp[to][0], dp[to][1]));
		}
		if ((u == a && b == 0) || (u == c && d == 0)) {
			dp[u][0] = min(dp[u][0], g0);
		} else if ((u == a && b == 1) || (u == c && d == 1)) {
			dp[u][1] = min(dp[u][1], g1 + p[u]);
		} else {
			dp[u][0] = min(dp[u][0], g0);
			dp[u][1] = min(dp[u][1], g1 + p[u]);
		}
	}
	void solve(void) {
		for (int i = 1; i <= m; i++) {
			read(a); read(b); read(c); read(d);
			dfs(1, 0);
			int ans = min(dp[1][0], dp[1][1]);
			if (ans >= INF) ans = -1;
			printf("%d\n", ans);
		}
	}
}
namespace work2 {
	const long long INF = 1ll << 60;
	
	long long dp[N << 2][2][2];
	inline void pushUp(int o) {
		int ls = o << 1, rs = o << 1 | 1;
		for (int i = 0; i < 2; i++)
			for (int j = 0; j < 2; j++) {
				dp[o][i][j] = INF;
				dp[o][i][j] = min(dp[ls][i][0] + dp[rs][1][j], dp[o][i][j]);
				dp[o][i][j] = min(dp[ls][i][1] + dp[rs][0][j], dp[o][i][j]);
				dp[o][i][j] = min(dp[ls][i][1] + dp[rs][1][j], dp[o][i][j]);
			}
	}
	inline void init(int o, int pos) {
		dp[o][0][0] = 0;
		dp[o][1][1] = p[pos];
		dp[o][0][1] = dp[o][1][0] = INF;
	}
	inline void modify(int o, int l, int r, int pos, int x, int tp) {
		if (l == r) {
			init(o, l);
			if (tp == 0) dp[o][x ^ 1][x ^ 1] = INF;
			return;
		}
		int mid = (l + r) >> 1;
		if (pos <= mid) modify(o << 1, l, mid, pos, x, tp);
		else modify(o << 1 | 1, mid + 1, r, pos, x, tp);
		pushUp(o);
	}
	inline void build(int o, int l, int r) {
		if (l == r) return init(o, l);
		int mid = (l + r) >> 1;
		build(o << 1, l, mid);
		build(o << 1 | 1, mid + 1, r);
		pushUp(o);
	}
	void solve(void) {
		build(1, 1, n);
		for (int i = 1; i <= m; i++) {
			read(a); read(b); read(c); read(d);
			modify(1, 1, n, a, b, 0);
			modify(1, 1, n, c, d, 0);
			long long ans = INF;
			ans = min(dp[1][0][0], ans);
			ans = min(dp[1][0][1], ans);
			ans = min(dp[1][1][0], ans);
			ans = min(dp[1][1][1], ans);
			if (ans >= INF) ans = -1;
			printf("%lld\n", ans);
			modify(1, 1, n, a, b, 1);
			modify(1, 1, n, c, d, 1);
		}
	}
}
namespace work3 {
	const long long INF = 1ll << 60;
	
	long long dp[N][2];
	int fa[N];
	inline void dfs(int u) {
		int to;
		dp[u][0] = dp[u][1] = 1e10;
		long long g0 = 0, g1 = 0;
		for (int i = head[u]; i; i = G[i].next) {
			to = G[i].to; if (to == fa[u]) continue;
			fa[to] = u; dfs(to);
			g0 = g0 + dp[to][1];
			g1 = g1 + min(dp[to][0], dp[to][1]);
		}
		dp[u][0] = min(dp[u][0], g0);
		dp[u][1] = min(dp[u][1], g1 + p[u]);
	}
	inline void update(int u, int x, int tp) {
		static int sta[N];
		*sta = 0;
		for (int v = u; fa[v]; v = fa[v]) sta[++*sta] = v;
		while (*sta > 0) {
			int v = sta[*sta];
			dp[fa[v]][0] -= dp[v][1];
			dp[fa[v]][1] -= min(dp[v][1], dp[v][0]);
			--*sta;
		}
		dp[u][x ^ 1] = INF;
		if (tp == 1) {
			dp[u][0] = 0;
			dp[u][1] = p[u];
			for (int i = head[u]; i; i = G[i].next) {
				int to = G[i].to; if (to == fa[u]) continue;
				dp[u][0] = dp[u][0] + dp[to][1];
				dp[u][1] = dp[u][1] + min(dp[to][0], dp[to][1]);
			}
		}
		for (int v = u; fa[v]; v = fa[v]) {
			dp[fa[v]][0] += dp[v][1];
			dp[fa[v]][1] += min(dp[v][1], dp[v][0]);
		}
	}
	void solve(void) {
		dfs(1);
		for (int i = 1; i <= m; i++) {
			read(a); read(b); read(c); read(d);
			update(a, b, 0);
			update(c, d, 0);
			long long ans = min(dp[1][0], dp[1][1]);
			if (ans >= 1e10) ans = -1;
			printf("%lld\n", ans);
			update(a, b, 1);
			update(c, d, 1);
		}
	}
}

int main(void) {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	read(n); read(m);
	t1 = get();
	while (t1 < 'A' || t1 > 'C') t1 = get();
	t2 = get();
	for (int i = 1; i <= n; i++) read(p[i]);
	for (int i = 1; i < n; i++) {
		read(x); read(y);
		addEdge(x, y);
	}
	if (n <= 2000) work1::solve();
	else if (t1 == 'A') work2::solve();
	else if (t1 == 'B') work3::solve();
	return 0;
}
