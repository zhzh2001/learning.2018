#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const int N = 5050;

vector<int> G[N];
int n, m, x, y;
int ans[N];

namespace work1 {
	inline void dfs(int u, int f) {
		ans[++*ans] = u;
		for (int i = 0; i < G[u].size(); i++)
			if (G[u][i] != f)
				dfs(G[u][i], u);
	}
	void solve(void) {
		*ans = 0;
		dfs(1, 0);
		for (int i = 1; i <= n; i++)
			printf("%d%c", ans[i], i == n ? '\n' : ' ');
	}
}
namespace work2 {
	int bx, by, ck;
	int vis[N], sta[N], cyc[N];
	int res[N];
	inline int equ(int x, int y, int _x, int _y) {
		return (x == _x && y == _y) || (x == _y && y == _x);
	}
	inline void dfs1(int u, int f) {
		int to; vis[u] = 1;
		if (!ck) sta[++*sta] = u;
		for (int i = 0; i < G[u].size(); i++) {
			to = G[u][i]; if (to == f) continue;
			if (vis[to]) {
				if (ck) continue;
				while (true) {
					cyc[++*cyc] = sta[*sta];
					if (sta[*sta] == to) break;
					--*sta;
				}
				ck = 1;
				continue;
			}
			dfs1(to, u);
		}
		if (!ck) --*sta;
	}
	inline void dfs(int u, int f) {
		res[++*res] = u;
		for (int i = 0; i < G[u].size(); i++)
			if (G[u][i] != f && !equ(u, G[u][i], bx, by))
				dfs(G[u][i], u);
	}
	void solve(void) {
		*sta = *cyc = ck = 0;
		dfs1(1, 0);
		ans[1] = 19260817;
		for (int i = 1; i <= *cyc; i++) {
			bx = cyc[i];
			by = cyc[i % *cyc + 1];
			*res = 0;
			dfs(1, 0);
			for (int i = 1; i <= n; i++)
				if (res[i] < ans[i]) {
					for (int i = 1; i <= n; i++)
						ans[i] = res[i];
					break;
				} else if (res[i] > ans[i]) break;
		}
		for (int i = 1; i <= n; i++)
			printf("%d%c", ans[i], i == n ? '\n' : ' ');
	}
}

int main(void) {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d %d", &n, &m);
	for (int i = 1; i <= m; i++) {
		scanf("%d %d", &x, &y);
		G[x].push_back(y);
		G[y].push_back(x);
	}
	for (int i = 1; i <= n; i++)
		sort(G[i].begin(), G[i].end());
	if (m == n - 1) work1::solve();
	else work2::solve();
	return 0;
}
