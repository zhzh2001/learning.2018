program travels;
 var
  i,m,n,x,y:longint;
 begin
  assign(output,'travel6.in');
  rewrite(output);
  randomize;
  n:=5000;
  m:=n;
  writeln(n,' ',m);
  for i:=2 to n do
   writeln(i,' ',random(i-1)+1);
  x:=random(n)+1;
  y:=x;
  while x=y do y:=random(n)+1;
  writeln(x,' ',y);
  close(output);
 end.
