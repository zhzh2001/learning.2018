program defense;
 uses math;
 type
  wwb=^wb;
  wb=record
   n:wwb;
   t:longint;
  end;
 var
  b:array[0..200001] of wb;
  h,f,d,dp,dn,dpc,dnc:array[0..100001] of int64;
  i,j,m,n,p,x,y,xc,yc:longint;
 procedure hahainc(x,y:longint);
  begin
   inc(p);
   b[p].n:=@b[h[x]];
   b[p].t:=y;
   h[x]:=p;
  end;
 procedure hahaDFS(k,x:longint);
  var
   i:longint;
   j:wwb;
  begin
   j:=@b[h[k]];
   dn[k]:=0;
   dp[k]:=d[k];
   f[k]:=x;
   while j<>@b[0] do
    begin
     i:=j^.t;
     j:=j^.n;
     if i=x then continue;
     hahaDFS(i,k);
     inc(dn[k],dp[i]);
     inc(dp[k],min(dn[i],dp[i]));
    end;
  end;
 procedure hahathink(x,y:longint);
  var
   i,p:longint;
   j:wwb;
  begin
   if y=1 then dnc[x]:=-1 //10082088200000
          else dpc[x]:=-1;//10082088200000;
   p:=f[x];
   while p<>0 do
    begin
     if dnc[p]<>-1 then dnc[p]:=0;
     if dpc[p]<>-1 then dpc[p]:=d[p];
     j:=@b[h[p]];
     while j<>@b[0] do
      begin
       i:=j^.t;
       j:=j^.n;
       if i=f[p] then continue;
       if (dnc[p]<>-1) {and (dpc[i]<>-1)} then
        if dpc[i]<>-1 then inc(dnc[p],dpc[i])
                      else dnc[p]:=-1;
       if dpc[p]<>-1 then
        if dnc[i]=-1 then inc(dpc[p],dpc[i])
                    else if dpc[i]=-1 then inc(dpc[p],dnc[i])
                                     else inc(dpc[p],min(dnc[i],dpc[i]));
      end;
     p:=f[p];
    end;
  end;
 begin
  assign(input,'defense.in');
  assign(output,'defense.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do
   read(d[i]);
  readln;
  filldword(h,sizeof(h)>>2,0);
  p:=0;
  for i:=1 to n-1 do
   begin
    readln(x,y);
    hahainc(x,y);
    hahainc(y,x);
   end;
  //filldword(d,sizeof(d)>>2,0);
  hahaDFS(1,0);
  for i:=1 to m do
   begin
    readln(x,xc,y,yc);
    for j:=1 to n do
     begin
      dpc[j]:=dp[j];
      dnc[j]:=dn[j];
     end;
    hahathink(x,xc);
    hahathink(y,yc);
    if ((f[x]=y) or (f[y]=x)) and (xc=0) and (yc=0) then writeln(-1)
                                                    else if dpc[1]=-1 then writeln(dnc[1])
                                                                      else if dnc[1]=-1 then writeln(dpc[1])
                                                                                        else writeln(min(dnc[1],dpc[1]));
   end;
  close(input);
  close(output);
 end.
