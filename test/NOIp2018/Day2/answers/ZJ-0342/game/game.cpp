#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <queue>
#define rep(i,l,r) for(int i=(l);i<=(r);++i)
#define per(i,r,l) for(int i=(r);i>=(l);--i)
const int P=1e9+7;
typedef long long ll;
int a[18][18],b[200],cnt,ans,n,m;
void dfs(int i,int j,int S){
	if(i==n&&j==m) b[++cnt]=(S<<1)|a[i][j];
	if(i<n) dfs(i+1,j,(S<<1)|a[i+1][j]);
	if(j<m) dfs(i,j+1,(S<<1)|a[i][j+1]);
}
ll ksm(ll a,ll b){
	ll ans=1;
	for(;b;b>>=1,a=a*a%P) if(b&1) ans=ans*a%P;
	return ans;
}
int main(){
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n>m) std::swap(n,m);
	if(n==1){
		printf("%lld\n",ksm(2,m));
		return 0;
	}
	if(n==2){
		printf("%lld\n",4*ksm(3,m-1)%P);
		return 0;
	}
	if(n==3){
		printf("%lld\n",112*ksm(3,m-3)%P);
		return 0;
	}
	rep(S,0,(1<<(n*m))-1){
		rep(d,1,n*m){
			int i=(d-1)/m+1,j=(d-1)%m+1;
			a[i][j]=!!(S&(1<<(d-1)));
		}
		cnt=0;
		dfs(1,1,a[1][1]);ans++;
		rep(i,1,cnt-1) if(b[i]<b[i+1]){
			//rep(i,1,n){rep(j,1,m) printf("%d ",a[i][j]);puts("");}puts("");
			ans--;break;
		}
	}
	printf("%d\n",ans);
	return 0;
}
