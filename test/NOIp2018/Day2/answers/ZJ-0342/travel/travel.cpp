#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <queue>
#define rep(i,l,r) for(int i=(l);i<=(r);++i)
#define per(i,r,l) for(int i=(r);i>=(l);--i)
const int maxn=10010;
int n,m,u,v,s[maxn],tp,p[maxn],fa[maxn],fin;
std::vector<int> G[maxn];
bool vis[maxn],w[maxn],fxk;
void dfs(int u,int fa){
	s[++tp]=u;vis[u]=true;
	int l=G[u].size();
	rep(i,0,l-1) if(G[u][i]!=fa) dfs(G[u][i],u);
}
void find(int u){
	vis[u]=true;w[u]=true;
	int l=G[u].size();
	rep(i,0,l-1) if(G[u][i]!=fa[u]){
		if(!w[G[u][i]]) fa[G[u][i]]=u,find(G[u][i]);
		else if(vis[G[u][i]])++p[u],--p[fa[G[u][i]]];
	}
	vis[u]=false;
}
void DFS(int u){
	vis[u]=true;
	int l=G[u].size();
	rep(i,0,l-1) if(G[u][i]!=fa[u]&&!vis[G[u][i]]) DFS(G[u][i]),p[u]+=p[G[u][i]];
}
void solve(int u,int id){
	//printf("%d %d %d\n",u,id,p[u]);
	if(!fin&&p[u]) fin=u;
	vis[u]=true;
	int l=G[u].size();
	s[++tp]=u;int x=0;
	rep(i,0,l-1){
		if(vis[G[u][i]]) continue;
		if(p[G[u][i]]){
			++x;
			if(x==2&&u==fin) fxk=true;
		}
		if(p[u]&&!p[G[u][i]]) fa[G[u][i]]=u,dfs(G[u][i],u);
		if(!p[u]&&!p[G[u][i]]) solve(G[u][i],n+1);
		if(p[G[u][i]]){
			if(fxk){
				solve(G[u][i],n+1);
				continue;
			}
			if(i==l-1||(i==l-2&&G[u][i+1]==fa[u])){
				if(G[u][i]>id) return;
				else{fa[G[u][i]]=u,solve(G[u][i],id);continue;}
			}
			if(p[u]){
				fa[G[u][i]]=u;
				if(G[u][i+1]!=fa[u]) solve(G[u][i],G[u][i+1]);
				else solve(G[u][i],G[u][i+2]);
			}
			else solve(G[u][i],n+1);
		}
	}
}
int main(){
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	rep(i,1,m) scanf("%d%d",&u,&v),G[u].push_back(v),G[v].push_back(u);
	rep(i,1,n) sort(G[i].begin(),G[i].end());
	if(n-1==m){
		dfs(1,0);
		rep(i,1,n-1) printf("%d ",s[i]);
		printf("%d\n",s[n]);
	}
	else if(n==m){
		find(1);memset(vis,false,sizeof(vis));
		DFS(1);memset(vis,false,sizeof(vis));
		//rep(i,1,20) if(p[i]) printf("%d ",i);puts("");
		solve(1,n+1);
		rep(i,1,n-1) printf("%d ",s[i]);
		printf("%d\n",s[n]);
	}
	return 0;
}
