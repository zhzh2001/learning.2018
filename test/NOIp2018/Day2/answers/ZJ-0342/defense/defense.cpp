#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <queue>
#define rep(i,l,r) for(int i=(l);i<=(r);++i)
#define per(i,r,l) for(int i=(r);i>=(l);--i)
int n,m,u[1000],v[1000],a,x,b,y,ans,w,A[1000];
char s[10];
int main(){
	freopen("defense.in","r",stdin);freopen("defense.out","w",stdout);
	scanf("%d %d %s",&n,&m,s);
	rep(i,1,n) scanf("%d",&A[i]);
	rep(i,1,n-1) scanf("%d%d",&u[i],&v[i]);
	rep(i,1,m){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		ans=1e9;
		rep(S,0,(1<<n)-1){
			int p=!!(S&(1<<(a-1))),q=!!(S&(1<<(b-1)));
			if((p^x)||(q^y)) continue;
			w=0;
			rep(i,1,n) if(S&(1<<(i-1))) w+=A[i];
			if(w>=ans) continue;
			bool flag=true;
			rep(i,1,n-1){
				int x=!!(S&(1<<(u[i]-1))),y=!!(S&(1<<(v[i]-1)));
				if(!(x||y)){
					flag=false;
					break;
				}
			}
			if(flag) ans=w;
		}
		if(ans==1e9) puts("-1");
		else printf("%d\n",ans);
	}
	return 0;
}
