#include<bits/stdc++.h>
using namespace std;
const int M=5005;
bool mmm1;
int n,m,deg[M];
vector<int>P[M];
struct P60{
	int res[M],cnt;
	void dfs(int x,int f){
		res[++cnt]=x;
		for(int i=0;i<(int)P[x].size();i++){
			int y=P[x][i];
			if(y==f)continue;
			dfs(y,x);
		}
	}
	void solve(){
		dfs(1,0);
		for(int i=1;i<=n;i++)
			printf("%d%c",res[i]," \n"[i==n]);
	}
}p60;
struct P40{
	int q[M],in[M],id1,id2;
	int ans[M],res[M],cnt;
	bool mark[M];
	void topo(){
		int l=0,r=0;
		for(int i=1;i<=n;i++)
			if(deg[i]==1)q[r++]=i;
		while(l<r){
			int v=q[l++];
			mark[v]=1;
			for(int i=0;i<(int)P[v].size();i++){
				int y=P[v][i];
				deg[y]--;
				if(deg[y]==1)q[r++]=y;
			}
		}
	}
	void dfs(int x,int f){
		res[++cnt]=x;
		for(int i=0;i<(int)P[x].size();i++){
			int y=P[x][i];
			if(y==f||(id1==x&&id2==y)||(id1==y&&id2==x))continue;
			dfs(y,x);
		}
	}
	void solve(){
		topo();                   
		int num=0;
		for(int i=1;i<=n;i++)if(!mark[i]){
			while(!mark[i]){
				mark[i]=1;
				in[++num]=i;
				for(int j=0;j<(int)P[i].size();j++){
					int y=P[i][j];
					if(!mark[y]){
						i=y;break;
					}
				}
			}
			break;
		}
		in[++num]=in[1];
		ans[1]=2e9;
		for(int i=1;i<num;i++){
			id1=in[i],id2=in[i+1];
			cnt=0;dfs(1,0);
			bool f=0;
			for(int j=1;j<=n;j++){
				if(res[j]<ans[j]){f=1;break;}
				else if(res[j]>ans[j])break;
			}
			if(f){
				for(int j=1;j<=n;j++)ans[j]=res[j];
			}
		} 
		for(int i=1;i<=n;i++){
			printf("%d%c",ans[i]," \n"[i==n]);
		}                                 
	}
}p40;
bool mmm2;
int main(){
	//printf("%lf\n",(&mmm2-&mmm1)/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int a,b,i=1;i<=m;i++){
		scanf("%d%d",&a,&b);
		P[a].push_back(b);
		P[b].push_back(a);
		deg[a]++,deg[b]++;
	}
	for(int i=1;i<=n;i++)sort(P[i].begin(),P[i].end());
	if(m==n-1)p60.solve();
	else p40.solve();
	return 0;
}
