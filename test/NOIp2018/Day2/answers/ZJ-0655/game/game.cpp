#include<bits/stdc++.h>
using namespace std;
const int P=1e9+7;
bool mmm1;
int n,m;
struct P20{
	int a[10][10],pre,ans;
	bool f;
	void check(int x,int y,int res){
		if(!f)return;
		res=res*2+a[x][y];
		if(x==n&&y==m){
			if(res>pre)f=0;
			pre=res;
			return;
		}
		if(x<n)check(x+1,y,res);
		if(y<m)check(x,y+1,res);
	}
	void dfs(int x,int y){
		if(x==n+1)y++,x=1;
		if(y>m){
			f=1;pre=2e9;
			check(1,1,0);
			ans+=f;
			if(ans==P)ans=0;
			return;
		}
		a[x][y]=0;
		dfs(x+1,y);
		a[x][y]=1;
		dfs(x+1,y);
	}
	void solve(){
		ans=0;
		dfs(1,1);
		printf("%d\n",ans);
	}
}p20;
struct P30{
	void solve(){
		long long ans=1;
		for(int i=1;i<m;i++)
			ans=ans*3%P;
		printf("%lld\n",ans*4%P);
	}
}p30;
struct Pa15{
	void solve(){
		if(m==1)puts("8");
		else if(m==2)puts("36");
		else {
			long long ans=112;
			for(int i=3;i<m;i++)
				ans=ans*3%P;
			printf("%lld\n",ans);
		}
	}
}pa15;
bool mmm2;
int main(){
	//printf("%lf\n",(&mmm2-&mmm1)/1024.0/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n<=3&&m<=3)p20.solve();
	else if(n==1){
		long long ans=1;
		for(int i=1;i<=m;i++)ans=ans*2%P;
		printf("%lld\n",ans);
	}
	else if(n==2)p30.solve();
	else pa15.solve();
	return 0;
}
