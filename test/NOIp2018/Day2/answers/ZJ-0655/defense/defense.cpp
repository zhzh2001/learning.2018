#include<bits/stdc++.h>
using namespace std;
const int M=1e5+5,INF=(int)1e12;
bool mmm1;
int n,m,v[M];
int h[M],to[M<<1],nx[M<<1],tot;
inline void add(int a,int b){
	to[++tot]=b;
	nx[tot]=h[a];
	h[a]=tot;
}
struct P44{
	int id1,a,id2,b;
	long long dp[M][2];
	void dfs(int x,int f){
		long long ret1=0,ret2=0;
		for(int i=h[x];i;i=nx[i]){
			int y=to[i];
			if(y==f)continue;
			dfs(y,x);
			ret1+=dp[y][1];
			ret2+=min(dp[y][0],dp[y][1]);
		}
		dp[x][0]=ret1;
		dp[x][1]=ret2+v[x];
		if(x==id1)dp[x][!a]=INF;
		if(x==id2)dp[x][!b]=INF;
	}
	void solve(){
		for(int i=1;i<=m;i++){
			scanf("%d%d%d%d",&id1,&a,&id2,&b);
			dfs(1,0);
			long long ans=min(dp[1][0],dp[1][1]);
			if(ans>=INF)ans=-1;
			printf("%lld\n",ans);
		}
	}
}p44;
char s[5];
bool mmm2;
int main(){
	//printf("%lf\n",(&mmm2-&mmm1)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for(int i=1;i<=n;i++)scanf("%d",&v[i]);
	for(int a,b,i=1;i<n;i++){
		scanf("%d%d",&a,&b);
		add(a,b);add(b,a);
	}
	p44.solve();
	return 0;
}


