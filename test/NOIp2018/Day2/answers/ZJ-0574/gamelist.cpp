#include<bits/stdc++.h>
using namespace std;

#define Uni all right
#define REP(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;++i)
#define DREP(i,a,b) for(int i=(a),i##_end_=(b);i>i##_end_;--i)
#define LREP(i,a) for(int i=Head[a];i;i=Next[i])
#define LL long long
#define Mod 1000000007

static const int M=100004;
int n,m,t,Ans;

namespace P1{
	char C[14][14];
	string ls;
	bool t;
	bool DFS(int x,int y,string s){
		if(x+1==n && y+1==m){
			if(t && s<ls)
				return 0;
			t=1,ls=s;
			return 1;
		}
		if(y+1<m)if(!DFS(x,y+1,s+C[x][y+1]))return 0;
		if(x+1<n)if(!DFS(x+1,y,s+C[x+1][y]))return 0;
	}
	void Find(int x){
		if(x==n+m-2){
			t=0;
			Ans+=DFS(0,0,"");
//			cerr<<"----"<<endl;
//			REP(i,0,n){
//				REP(j,0,m)
//					cerr<<C[i][j];
//				cerr<<endl;
//				}
			return;
		}
		Find(x+1);
		DREP(i,n-1,-1)REP(j,0,m)if(i+j==x){
			C[i][j]='1';Find(x+1);
		}
		DREP(i,n-1,-1)REP(j,0,m)if(i+j==x)C[i][j]='0';
	}
	void Solve(){
		REP(i,0,n)REP(j,0,m)C[i][j]='0';
		Find(1);
		Ans<<=2;
	}
}
int Pow(int k,int p){
	int x=1;
	for(;p;k=(LL)k*k%Mod,p>>=1)if(p&1)x=(LL)x*k%Mod;
	return x;
}
int main(){
//	freopen("game.in","r",stdin);
//	freopen("gamelist.out","w",stdout);
	
	scanf("%d%d",&n,&m);
//	if(n<=3 && m<=3)
	P1::Solve();
//	else if(n==1)Ans=n+1;
//	else if(n==2)Ans=4ll*Pow(3,m-1)%Mod;
//	else if(n==3)Ans=112ll*Pow(3,m-3)%Mod;
	
	printf("%d\n",Ans);
	
	return 0;
}
