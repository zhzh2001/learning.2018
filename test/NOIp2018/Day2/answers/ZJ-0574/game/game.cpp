#include<bits/stdc++.h>
using namespace std;

#define Uni all right
#define REP(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;++i)
#define DREP(i,a,b) for(int i=(a),i##_end_=(b);i>i##_end_;--i)
#define LREP(i,a) for(int i=Head[a];i;i=Next[i])
#define LL long long
#define Mod 1000000007

static const int M=100004;
int n,m,t,Ans;

/*
namespace P1{
	char C[14][14];
	string D[1004];
	void DFS(int x,int y,string s){
		if(x+1==n && y+1==m){
			D[t++]=s;
			return;
		}
		if(y+1<m)DFS(x,y+1,s+C[x][y+1]);
		if(x+1<n)DFS(x+1,y,s+C[x+1][y]);
	}
	void Solve(){
		REP(S,0,1<<n*m){
			REP(i,0,n*m)
				C[i/m][i%m]=((S>>i)&1)+'0';
//			cerr<<"----"<<endl;
//			REP(i,0,n){
//				REP(j,0,m)
//					cerr<<C[i][j];
//				cerr<<endl;
//			}
			t=0;
			DFS(0,0,"");
			bool Flag=1;
			REP(i,1,t)if(D[i]<D[i-1]){
				Flag=0;break;
			}
//			cerr<<Flag<<endl;
			Ans+=Flag;
		}
	}
}
*/
int Pow(int k,int p){
	int x=1;
	for(;p;k=(LL)k*k%Mod,p>>=1)if(p&1)x=(LL)x*k%Mod;
	return x;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	if(n>m)swap(n,m);
	if(n==1)Ans=Pow(2,m);
	else if(n==2)Ans=4ll*Pow(3,m-1)%Mod;
	else if(n==3)Ans=112ll*Pow(3,m-3)%Mod;
	else if(n==4)Ans=(m==4)?912:2688ll*Pow(3,m-5)%Mod;
	else if(n==5)Ans=(m==5)?7136:21312ll*Pow(3,m-6)%Mod;
	else if(n==6)Ans=(m==6)?56768:170112ll*Pow(3,m-7)%Mod;
	
	printf("%d\n",Ans);
	
	return 0;
}
