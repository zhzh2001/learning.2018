#include<bits/stdc++.h>
using namespace std;

#define Uni all right
#define REP(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;++i)
#define DREP(i,a,b) for(int i=(a),i##_end_=(b);i>i##_end_;--i)
#define LREP(i,a) for(int i=Head[a];i;i=Next[i])
#define LL long long

static const int M=5004;

int n,m,t,Ans[M];
bool Mark[M];
vector<int>E[M];
namespace P1{
	void DFS(int x,int f){
		Ans[++t]=x;
		REP(i,0,E[x].size())if(E[x][i]!=f)
			DFS(E[x][i],x);
	}
	void Solve(){
		DFS(1,0);
	}
};
namespace P2{
	int Fa[M],Dep[M],qx,qy;
	int D[M],s,k;
	
	void DFS(int x,int f){
		int y;
		Fa[x]=f,Dep[x]=Dep[f]+1;
		REP(i,0,E[x].size())if((y=E[x][i])!=f){
			if(!Dep[y])DFS(y,x);
			else qx=x,qy=y;
		}
	}
	int LCA(int x,int y){
		while(x!=y)
			if(Dep[x]>Dep[y])x=Fa[x];
			else y=Fa[y];
		return x;
	}
	void Get(int x,int f){
		int y;
		if(k==0 && x!=Ans[t]){
			if(x>Ans[t]){
				k=-1;
				return;
			}
			else k=1;
		}
		Ans[t++]=x;
		REP(i,0,E[x].size())if((y=E[x][i])!=f){
			if(x==qx && y==qy)continue;
			if(x==qy && y==qx)continue;
			Get(y,x);
			if(k==-1)return;
		}
	}
	void Solve(){
		DFS(1,0);
		int qc=LCA(qx,qy);
		for(int u=qx;u!=qc;u=Fa[u])D[s++]=u;
		reverse(D,D+s);
		for(int u=qy;u!=qc;u=Fa[u])D[s++]=u;
		D[s++]=qc;
		D[s]=D[0];
		
		REP(i,1,n+1)Ans[i]=0x3f3f3f3f;
		REP(i,0,s){
			qx=D[i],qy=D[i+1];
			t=1,k=0;
			Get(1,0);
		}
	}
};
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	REP(i,0,m){
		int u,v;
		scanf("%d%d",&u,&v);
		E[u].push_back(v);
		E[v].push_back(u);
	}
	REP(i,1,n+1)sort(E[i].begin(),E[i].end());
	
	if(m==n-1)P1::Solve();
	else P2::Solve();
	
	REP(i,1,n+1)printf("%d%c",Ans[i]," \n"[i==n]);
	
	return 0;
}
