#include<bits/stdc++.h>
using namespace std;

#define Uni all right
#define REP(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;++i)
#define DREP(i,a,b) for(int i=(a),i##_end_=(b);i>i##_end_;--i)
#define LREP(i,a) for(int i=Head[a];i;i=Next[i])
#define LL long long
#define Mod 1000000007
#define INF 0x3f3f3f3f3f3f3f3f

static const int M=100004;
LL F[M],G[M];
char Type[4];
int n,m,Cost[M];

int Head[M],V[M<<1],Next[M<<1],tot;
void Add_Edge(int u,int v){
	Next[++tot]=Head[u],V[Head[u]=tot]=v;
}
namespace P1{
	static const int N=2004;
	int D[N],Fa[N],t;
	void DFS(int x,int f){
		D[++t]=x,Fa[x]=f;
		LREP(i,x)if(V[i]!=f)
			DFS(V[i],x);
	}
	void Solve(){
		DFS(1,0);
		while(m--){
			int a,s1,b,s2;
			scanf("%d%d%d%d",&a,&s1,&b,&s2);
			REP(i,1,n+1)F[i]=G[i]=0;
			DREP(i,n,0){
				int x=D[i],f=Fa[x];
				
				if(x==a)s1?G[x]=INF:F[x]=INF;
				if(x==b)s2?G[x]=INF:F[x]=INF;
				
				F[x]+=Cost[x];
				G[x]=min(G[x],F[x]);
				
				F[f]+=G[x];
				G[f]+=F[x];
			}
			LL Ans=min(F[1],G[1]);
			if(Ans>=INF)puts("-1");
			else printf("%lld\n",Ans);
		}
	}
}
struct Node{
	LL FF,FG,GG,GF;
	Node operator +(const Node &_){
		Node Tmp;
		Tmp.FF=min(FF+_.GF,FG+_.FF);
		Tmp.FG=min(FF+_.GG,FG+_.FG);
		Tmp.GF=min(GF+_.GF,GG+_.FF);
		Tmp.GG=min(GF+_.GG,GG+_.FG);
		Tmp.GF=min(Tmp.GF,Tmp.FF);
		Tmp.GG=min(Tmp.GG,Tmp.FG);
		return Tmp;
	}
};
namespace P2{
	Node Tr[M<<2],D1,D2,TmpD;
	int Pos[M];
	#define lp (p<<1)
	#define rp (p<<1|1)
	#define lson l,mid,lp
	#define rson mid+1,r,rp
	#define Rot 1,n,1
	
	void Build(int l,int r,int p){
		if(l==r){
			Tr[p].GG=Tr[p].GF=0;
			Tr[p].FF=Tr[p].FG=Cost[l];
			Pos[l]=p;
			return;
		}
		int mid=l+r>>1;
		Build(lson);
		Build(rson);
		Tr[p]=Tr[lp]+Tr[rp];
	}
	void Updata(int l,int r,int p,int a,int s){
		if(l==r){
			if(s==0)Tr[p].FF=Tr[p].FG=INF;
			else if(s==1)Tr[p].GF=Tr[p].GG=INF;
			else Tr[p]=TmpD;
			return;
		}
		int mid=l+r>>1;
		if(a<=mid)Updata(lson,a,s);
		else Updata(rson,a,s);
		Tr[p]=Tr[lp]+Tr[rp];
	}
	void Solve(){
		Build(Rot);
		while(m--){
			int a,s1,b,s2;
			scanf("%d%d%d%d",&a,&s1,&b,&s2);
			D1=Tr[Pos[a]],D2=Tr[Pos[b]];
			Updata(Rot,a,s1);
			Updata(Rot,b,s2);
			
			LL Ans=min(min(Tr[1].FF,Tr[1].FG),min(Tr[1].GF,Tr[1].GG));
			if(Ans>=INF)puts("-1");
			else printf("%lld\n",Ans);
			
			TmpD=D1;
			Updata(Rot,a,2);
			TmpD=D2;
			Updata(Rot,b,2);
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense_c.out","w",stdout);
	
	scanf("%d%d%s",&n,&m,Type);
	
	REP(i,1,n+1)scanf("%d",&Cost[i]);
	REP(i,1,n){
		int u,v;
		scanf("%d%d",&u,&v);
		Add_Edge(u,v);
		Add_Edge(v,u);
	}
	if(n<=2000 && m<=2000)P1::Solve();
	else if(Type[0]=='A')P2::Solve();
	
	return 0;
}
