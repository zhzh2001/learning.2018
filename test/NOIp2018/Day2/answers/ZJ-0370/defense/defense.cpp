#include<bits/stdc++.h>
using namespace std;
long long cnt,n,m,x,y,xx,yy,a[200005],f[200005],dp[200005][2],q[200005],head[100005],to[200005];
char ch[1001];
bool f1;
void add(int x,int y)
{
	q[++cnt]=head[x];to[cnt]=y;head[x]=cnt;
}
void dfs(long long t,long long s)
{
	for (long long i=head[t];i!=0;i=q[i])
	  if (to[i]!=s)
	  {
	  	dfs(to[i],t);
	    if (f[t]!=2)
	      dp[t][0]=dp[t][0]+dp[to[i]][1];
	    if (f[t]!=1)
	      dp[t][1]=dp[t][1]+min(dp[to[i]][0],dp[to[i]][1]);
	  }
	if (f[t]!=1)
	  dp[t][1]=dp[t][1]+a[t];
	if (f[t]==1)
	  dp[t][1]=10000000000000000;
	if (f[t]==2)
	  dp[t][0]=10000000000000000;
}
int main()
{
    freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%lld%lld",&n,&m);
    scanf("%s",ch);
    for (long long i=1;i<=n;i++)
      scanf("%lld",&a[i]);
    for (long long i=1;i<n;i++)
    {
    	scanf("%lld%lld",&x,&y);
    	add(x,y);
    	add(y,x);    	
	}
	if (n<=2000 && m<=2000)
	{
		for (long long i=1;i<=m;i++)
		{
			memset(dp,0,sizeof(dp));
			scanf("%lld%lld%lld%lld",&x,&y,&xx,&yy);
			f[x]=y+1;
			f[xx]=yy+1;
 			dfs(1,0);
 			if (min(dp[1][0],dp[1][1])>=10000000000000000)
 			  printf("-1\n");
 			else
			  printf("%lld\n",min(dp[1][0],dp[1][1]));
			f[x]=0;
			f[xx]=0;
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
