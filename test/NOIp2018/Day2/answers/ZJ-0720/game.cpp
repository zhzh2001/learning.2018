#include<bits/stdc++.h>
using namespace std;
int n,m;
int f[300][300];
long long dp[2][300]; 
long long mod=1e9+7;
bool cheak(int x,int y)
{
	x=x>>1;
	bool ft=true;
	int q=n-1;
	while (q && ft)
		{
			int xx=x%2;
			int yy=y%2;
			if (yy==1 && xx!=1)
				ft=false;
			x=x>>1;
			y=y>>1;	
			q--;
		}
	return ft;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=0;i<(1<<(n));i++)
		for (int j=0;j<(1<<(n));j++)
			if (cheak(i,j))
				{
					f[i][0]++;
					f[i][f[i][0]]=j;
				}						
	int q1=0;int q2=1;
	for (int i=0;i<(1<<(n));i++)
		dp[q1][i]=1;			
	for (int i=1;i<m;i++)
		{
			for (int j=0;j<(1<<(n));j++)
				for (int k=1;k<=f[j][0];k++)
					dp[q2][f[j][k]]=(dp[q2][f[j][k]]+dp[q1][j])%mod;
			for (int j=0;j<(1<<(n));j++)
				dp[q1][j]=0;
			swap(q1,q2);			
		}
	long long ans=0;					
	for (int i=0;i<(1<<(n));i++)
		ans=(ans+dp[q1][i])%mod;
	if (n==2)	
		printf("%lld",ans);
	else
		printf("%lld",(ans*111111112*7)%mod);				
}
