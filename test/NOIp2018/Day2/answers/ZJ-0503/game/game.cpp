#include <iostream>
#include <cstdio>
#define int long long 
using namespace std;
const int N=100,mod=1e9+7;
int ans,n,m,tot,cnt,kk[N],tmp1[N],tmp2[N],a[N][N],v[1000][N],w[N];
bool comp(int x,int y){
	int t1=0,xx=1,yy=1; tmp1[++t1]=a[1][1];
	for(int i=1;i<=tot;i++){
		if(v[x][i]==1){
			yy++; tmp1[++t1]=a[xx][yy];
		}
		else {
			xx++; tmp1[++t1]=a[xx][yy];
		}
	}
	int t2=0; xx=1,yy=1; tmp2[++t2]=a[1][1];
	for(int i=1;i<=tot;i++){
		if(v[y][i]==1){
			yy++; tmp2[++t2]=a[xx][yy];
		}
		else {
			xx++; tmp2[++t2]=a[xx][yy];
		}
	}
	for(int i=1;i<=t1;i++){
		if(tmp1[i]>tmp2[i])return 0;
		if(tmp1[i]<tmp2[i])return 1;
	}
	return 1;
}
bool check(){
	for(int i=1;i<=cnt;i++){
		for(int j=1;j<=cnt;j++){
			if(i==j)continue;
			for(int k=1;k<=tot;k++){
				if(v[i][k]>v[j][k]){
				    if(!comp(i,j))return 0;		
				}
				if(v[i][k]<v[j][k])break;
			}
		}
	}
	return 1;
}
void dfs2(int rt,int sum){
	if(sum==m-1){
		cnt++;
		for(int i=1;i<=tot;i++)v[cnt][i]=w[i];
		return ;
	}
	if(rt>tot) return ;
	dfs2(rt+1,sum);
	w[rt]=1;
	dfs2(rt+1,sum+1);
	w[rt]=0;
}
void dfs1(int rt){
	if(rt>n*m){
		int kkk=0;
		for(int i=1;i<=n;i++)
		   for(int j=1;j<=m;j++)a[i][j]=kk[++kkk];
		if(check())ans=(ans+1)%mod;
		return;
	}
	dfs1(rt+1);
	kk[rt]=1;
	dfs1(rt+1);
	kk[rt]=0;
}
signed main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	tot=n+m-2;
	dfs2(1,0);
	dfs1(1);
	cout<<ans%mod<<endl;
	return 0;
}
