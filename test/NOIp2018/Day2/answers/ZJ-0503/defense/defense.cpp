#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
void wr(int x){if(x<0){putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
void wrn(int x){wr(x);putchar('\n');}
int dp[3000][3],w[3000],vis[3000],ss[3000];
int js(int l,int r){
	int ff=1;
	for(int i=l;i<=r;i++)if(!ss[i]){
		ff=0;break;
	}
	if(ff) return 0;
	memset(dp,0x3f3f3f,sizeof dp);
	dp[l][0]=0;dp[l][1]=w[l];
	for(int i=l+1;i<=r;i++){
		dp[i][0]=dp[i-1][1];
		dp[i][1]=min(dp[i-1][0],dp[i-1][1])+w[i];
	}
	return min(dp[r][0],dp[r][1]);
}
string type;
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n=read(),m=read();
	cin>>type;
	for(int i=1;i<=n;i++)w[i]=read();
	for(int i=1;i<n;i++){
		int u=read(),y=read();
	}
	while(m--){
		int ans=0; memset(vis,0,sizeof vis);
		memset(ss,0,sizeof ss);
		int flag=1;
		int a=read(),x=read(),b=read(),y=read();
		if(x==0&&y==0){
			if(abs(b-a)==1){
				puts("-1"); flag=0;
			}
		}
		if(!flag)continue;
		if(x==0){
			vis[a]=1; 
			if(a-1>=1)ans+=w[a-1],vis[a-1]=1;
			if(a+1<=n)ans+=w[a+1],vis[a+1]=1;
			if(a-2>=1) ss[a-2]=1; if(a+2<=n) ss[a+2]=1;
		}
		if(x==1){
			vis[a]=1; ans+=w[a];
			if(a-1>=1) ss[a-1]=1; if(a+1<=n) ss[a+1]=1;
		}
		if(y==0){
			vis[b]=1;
			if(b-1>=1)ans+=w[b-1],vis[b-1]=1;
			if(b+1<=n)ans+=w[b+1],vis[b+1]=1;
			if(b-2>=1) ss[b-2]=1; if(b+2<=n) ss[b+2]=1;
		}
		if(y==1){
		    vis[b]=1,ans+=w[b];
		    if(b-1>=1) ss[b-1]=1; if(b+1<=n) ss[b+1]=1;
		}
		int l,r;
		for(int i=1;i<=n;i++){
			while(vis[i])i++;
			l=i;
			while(!vis[i])i++; i--;
			r=min(i,n);
			ans+=js(l,r);
		}
		wrn(ans);
	}
	return 0;
}
