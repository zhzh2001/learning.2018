#include <iostream>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
void wr(int x){if(x<0){putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
void wri(int x){wr(x);putchar(' ');}
int flag,n,m,tot,ans[10000];
int a[5010][5010];
void dfs(int rt,int fa){
	ans[++tot]=rt;
	for(int i=1;i<=n;i++){
		if(a[rt][i]&&i!=fa)dfs(i,rt);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		a[x][y]=1,a[y][x]=1;
	}
	dfs(1,0);
	for(int i=1;i<=tot;i++)wri(ans[i]);
	return 0;
}
