#include <cstdio>
#include <cctype>
#include <queue>
#include <vector>
#include <cstring>
#define rg register
using namespace std;
const int maxn = 5005;
inline void redi (int & a) {
	rg char ch = getchar ();a = 0;
	while (!isdigit (ch)) ch = getchar ();
	while (isdigit (ch)) a = a * 10 +ch - 48,ch = getchar ();
}
int b[maxn];
int ans[maxn];
int n,m;
int to_[maxn*2],next_[maxn*2],head_[maxn];
int blk;
int siz;
inline void addedge (int frm,int t) {
	next_[++ siz] = head_[frm];
	to_[siz] = t;
	head_[frm] = siz;
}
int inq[maxn];
int ap=1;
void dfs (int curr) {
	if(ap == n) return;
	priority_queue<int,vector<int>,greater<int> >qi;
	for (rg int i = head_[curr];i;i = next_[i]) {
		if ((!b[to_[i]])) {
			qi.push(to_[i]);
			b[to_[i]] = 1;
		}
	}
	while (!qi.empty()) {
		ans[++ap]=qi.top();
		qi.pop();
		dfs(ans[ap]);
	}
}
int flag2;
void dfs2 (int curr) {
	if(ap == n) return;
	priority_queue<int,vector<int>,greater<int> >qi;
	for (rg int i = head_[curr];i;i = next_[i]) {
		if ((!b[to_[i]])&&((i!=blk*2)&&(i!=blk*2-1))) {
			qi.push(to_[i]);
			b[to_[i]] = 1;
		}
	}
	int t1t;
	while (!qi.empty()) {
		t1t=qi.top();
		++ap;
		if(t1t>ans[ap]&&flag2!=-1) {
			flag2=1;return;
		}
		else if(t1t<ans[ap]) {
			if(flag2!=-1) {
				flag2=-1;
			}
		}
		ans[ap]=t1t;
		qi.pop();
		dfs2(t1t);
		if(flag2==1) return;
	}
}
int main () {
	freopen ("travel.in","r",stdin);freopen ("travel.out","w",stdout);
	redi (n),redi (m);
	if(n!=m) {
		rg int t1,t2;
		for (rg int i = 1;i <= m;++ i) {
			redi (t1),redi (t2);
			addedge(t1,t2);addedge(t2,t1);
		}
		b[1]=1;
		ans[1]=1;
		dfs(1);
		for (rg int i = 1;i <= n;++ i) {
			printf ("%d ",ans[i]);
		}
	}
	else {
		rg int t1,t2;
		for (rg int i = 1;i <= m;++ i) {
			redi (t1),redi (t2);
			addedge(t1,t2);addedge(t2,t1);
		}
		b[1]=1;
		ans[1]=1;
		dfs(1);
		for(rg int i = 1;i <= n;++i) {
			memset(b,0,sizeof(b));
			blk=i;
			ap=1;
			flag2=0;
			b[1]=1;
			dfs2(1);
		}
		for (rg int i = 1;i <= n;++ i) {
			printf ("%d ",ans[i]);
		}
	}
	return 0;
}
