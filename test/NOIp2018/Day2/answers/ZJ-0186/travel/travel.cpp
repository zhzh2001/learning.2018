#include<bits/stdc++.h>
using namespace std;

const int maxn = 5010;

int n,m;
int v[maxn][maxn],cnt[maxn];
bool vis[maxn];

int chn[maxn],tot;
int ans[maxn];

inline void read(int &x)
{
	x = 0;int f = 1;char s = getchar();
	for(;s < '0' || s > '9';s = getchar()) if(s == '-') f = - 1;
	for(;s >= '0' && s <= '9';s = getchar()) x = (x << 3) + (x << 1) + s - 48;
	x *= f;
}

void DFS(int x)
{
	vis[x] = 1;chn[++ tot] = x;
	for(int i = 1;i <= cnt[x];++ i)
		if(! vis[ v[x][i] ]) DFS(v[x][i]);
}

void _ex()
{
	for(int i = 1;i <= n;++ i) ans[i] = n + 1;
	for(int i = 1;i <= n;++ i)
		for(int j = 1;j <= cnt[i];++ j) 
		{
			int tmp = v[i][j];
			v[i][j] = 0;tot = 0;
			memset(vis,0,sizeof(vis));vis[0] = 1;
			DFS(1);v[i][j] = tmp;
			bool flag = 0;
			for(int k = 1;k <= n;++ k)
				if(chn[k] ^ ans[k]){
					flag = (chn[k] < ans[k]);break;
				}
			for(int k = 1;k <= n;++ k) 
				if(! vis[k]){
					flag = 0;break;
				}
			if(flag) for(int k = 1;k <= n;++ k) ans[k] = chn[k];
		}
	for(int i = 1;i <= n;++ i) chn[i] = ans[i];
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	read(n);read(m);
	int x,y;
	for(int i = 1;i <= m;++ i) read(x),read(y),v[x][ ++ cnt[x] ] = y,v[y][ ++ cnt[y] ] = x;
	
	for(int i = 1;i <= n;++ i) sort(v[i] + 1,v[i] + cnt[i] + 1);
	if(m == n) _ex();
	else DFS(1);
	
	for(int i = 1;i <= n;++ i) printf("%d",chn[i]),i == tot ? 0 : putchar(' ');
	
	fclose(stdin);fclose(stdout);
	return 0;
}
