#include<bits/stdc++.h>
using namespace std;

const int maxn = 100000 + 10;
typedef long long ll;

int n,m;
char typ[3];
int v[maxn];

int Link[maxn],tot;
struct edge{
	int y,nxt;
}e[maxn << 1];

ll f[maxn][2];
ll g[maxn][2];

inline void read(int &x)
{
	x = 0;int f = 1;char s = getchar();
	for(;s < '0' || s > '9';s = getchar()) if(s == '-') f = - 1;
	for(;s >= '0' && s <= '9';s = getchar()) x = (x << 3) + (x << 1) + s - 48;
	x *= f;
}

inline void Insert(int x,int y) {e[++ tot].y = y;e[tot].nxt = Link[x];Link[x] = tot;}

inline ll Min(ll a,ll b) {return a < b ? a : b;}

void dfs(int x,int pre)
{
	if(f[x][1] != - 1) f[x][1] = v[x];
	for(int i = Link[x];i;i = e[i].nxt)
		if(e[i].y ^ pre){
			int y = e[i].y;
			dfs(y,x);
			if(f[x][0] != - 1 && f[y][1] != - 1) f[x][0] += f[y][1];
			else if(f[x][0] != - 1 && f[y][1] == - 1) f[x][0] = - 1;
			if(f[x][1] != - 1){
				if(f[y][0] != - 1 && f[y][1] != - 1) f[x][1] += Min(f[y][0],f[y][1]);
				if(f[y][0] == - 1 && f[y][1] != - 1) f[x][1] += f[y][1];
				if(f[y][0] != - 1 && f[y][1] == - 1) f[x][1] += f[y][0];
				if(f[y][0] == - 1 && f[y][1] == - 1) f[x][1] = - 1;
			}
		}
}

void _44pts()
{
	for(int i = 1;i <= m;++ i)
	{
		int x,y,a,b;
		read(x);read(a);read(y);read(b);
		memset(f,0,sizeof(f));
		f[x][1 - a] = - 1;f[y][1 - b] = - 1;
		dfs(1,0);
		//cout<<f[1][1]<<endl;
		if(f[1][0] == - 1 && f[1][1] != - 1) printf("%lld\n",f[1][1]);
		if(f[1][0] != - 1 && f[1][1] == - 1) printf("%lld\n",f[1][0]);
		if(f[1][0] == - 1 && f[1][1] == - 1) puts("-1");
		if(f[1][0] != - 1 && f[1][1] != - 1) printf("%lld\n",Min(f[1][0],f[1][1]));
		//cout<<endl;
	}
}

void ex_12pts()
{
	dfs(1,0);
	for(int i = 1;i <= n;++ i) g[i][0] = f[i][0],g[i][1] = f[i][1];
	memset(f,0,sizeof(f));
	dfs(n,0);
	for(int i = 1;i <= m;++ i)
	{
		int x,y,a,b;
		read(x);read(a);read(y);read(b);
		if(x > y) swap(x,y),swap(a,b);
		ll ans = g[y][b] + f[x][a];
		if(a == 0 && b == 0) ans = - 1;
		printf("%lld\n",ans);
	}
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	
	read(n);read(m);scanf("%s",typ + 1);
	for(int i = 1;i <= n;++ i) read(v[i]);
	int x,y;
	for(int i = 1;i < n;++ i) read(x),read(y),Insert(x,y),Insert(y,x);
	
	if((long long) n * m <= (2e8)) _44pts();
	else if(typ[1] == 'A' && typ[2] == '2') ex_12pts();
	
	fclose(stdin);fclose(stdout);
	return 0;
}
