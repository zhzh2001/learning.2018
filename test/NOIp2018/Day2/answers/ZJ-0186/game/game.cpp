#include<bits/stdc++.h>
using namespace std;

const int maxn = 10,maxm = 1000000 + 10,p = 1e9 + 7;

int n,m;
int f[maxn][maxm][2];

int c[maxm],inv[maxm];

int a[20][20];

inline void read(int &x)
{
	x = 0;int f = 1;char s = getchar();
	for(;s < '0' || s > '9';s = getchar()) if(s == '-') f = - 1;
	for(;s >= '0' && s <= '9';s = getchar()) x = (x << 3) + (x << 1) + s - 48;
	x *= f;
}

inline int Add(int a,int b) {return (long long) a + b >= p ? (long long) a + b - p : a + b;}
inline int C(int n,int m) {return (long long) c[m] * inv[n] % p * inv[m - n] % p;}

inline int Pow(int a,int b)
{
	int res = 1;
	for(;b;b >>= 1) (b & 1) ? res = (long long) res * a % p : 0,a = (long long) a * a % p;
	return res;
}

void _Guess()
{
	f[1][1][0] = f[1][1][1] = 1;
	for(int i = 1;i <= n;++ i)
		for(int j = 1;j <= m;++ j)
		{
			f[i + 1][j][1] = Add(Add(f[i + 1][j][1],f[i][j][0]),f[i][j][1]);
			
			f[i][j + 1][0] = Add(Add(f[i][j + 1][0],f[i][j][0]),f[i][j][1]);
			f[i][j + 1][1] = Add(Add(f[i][j + 1][1],f[i][j][0]),f[i][j][1]);
			
			if(j == m) f[i + 1][j][0] = Add(Add(f[i + 1][j][0],f[i][j][0]),f[i][j][1]);
		}
	//cout<<f[3][1][1]+f[3][1][0]<<endl;
	printf("%d",Add(f[n][m][0],f[n][m][1]));
}

void Init()
{
	c[0] = c[1] = 1;
	for(int i = 2;i <= m;++ i) c[i] = (long long) c[i - 1] * i % p;
	inv[0] = inv[1] = 1;
	for(int i = 2;i <= m;++ i) inv[i] = (long long) (p - p / i) * inv[p % i] % p;
	for(int i = 2;i <= m;++ i) inv[i] = (long long) inv[i - 1] * inv[i] % p;
	
} 

void _20pts()
{
	int cnt = 0;
	for(int s = 0;s < (1 << (n * m));++ s)
	{
		for(int i = 1;i <= n;++ i)
		for(int j = 1;j <= m;++ j) a[i][j] = (s >> ((i - 1) * m + j - 1)) & 1;
		
		bool flag = 1;
		for(int i = 1;i < n;++ i)
		for(int j = 2;j <= m;++ j) 
			if((i != 1 || j != 1) && a[i][j] > a[i + 1][j - 1]){
				flag = 0;goto end;
			}
		end :
		cnt += flag;
	}
	printf("%d",cnt);
}

void ex_30pts()
{
	int ans = 0;
	for(int i = 0;i <= m - 1;++ i) ans = Add(ans,C(i,m - 1) * Pow(2,i) % p);
	ans = (long long) 4 * ans % p * Pow(2,m - 2) % p;
	//cout<<ans<<endl;
	printf("%d",ans);
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	read(n);read(m);
	Init();
	if(n == 1) printf("%d",Pow(2,m));	
	else if(m == 1) printf("%d",Pow(2,n));
	else if(n * m <= 20) _20pts();
	else if(n == 2) ex_30pts(); 
	else printf("4");
	
	fclose(stdin);fclose(stdout);
	return 0;
}
