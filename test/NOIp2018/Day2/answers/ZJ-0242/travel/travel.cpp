#include <iostream>
#include <cstdio>
#include <vector>
#include <cstring>
#include <algorithm>

using namespace std;

const int N = 5e3 + 5;
const int M = 1e4 + 5;
const int INF = 0x3f3f3f3f;

int n, m;
int u, v;
int node_start = INF; //起点
int head[N];
int cnt = 0;
int in_num[N];   //每个点的入度
int stack_tmp[N], tail = 0;
int low[N];
int dfn[N];
bool vis[N];     //判断每个点是否被走过
bool is_root[N]; //判断是否是环的根

struct EDGE {
	int s;
	int e;
	int nxt;
} Edge[M];

int read() { //读入优化
	int s = 0, w = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9') {
		if (ch == '-') w = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9') {
		s = s * 10 + ch - '0';
		ch = getchar();
	}
	return s * w;
}

void add(int u, int v) {
	++cnt;
	Edge[cnt].s = u;
	Edge[cnt].e = v;
	Edge[cnt].nxt = head[u];
	head[u] = cnt;
}

inline int Min(int x, int y) {
	return x <= y ? x : y;
}

void dfs_tree(int node) {
	printf("%d ", node);
	vector <int> tmp;
	for (register int i = head[node]; i; i = Edge[i].nxt) {
		if (vis[Edge[i].e])	continue;
		tmp.push_back(Edge[i].e);
		vis[Edge[i].e] = true;
	}
	sort(tmp.begin(), tmp.end());
	for (register int i = 0; i < (int) tmp.size(); ++i) {
		dfs_tree(tmp[i]);
	}
}

void dfs_find_root(int node) {

}

int main() {
	//文件输入输出
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	
	n = read(), m = read();
	for (register int i = 1; i <= m; ++i) {
		u = read(), v = read();
		node_start = Min(u, node_start); //找起点
		node_start = Min(v, node_start);
		add(u, v);
		add(v, u);
	}
	if (m == (n - 1)) { //如果这个图是一棵树
		vis[node_start] = true;
		dfs_tree(node_start);
		printf("\n");
		return 0; //结束遍历 
	}
	vis[node_start] = true;
	dfs_find_root(node_start);	
	memset(vis, 0, sizeof(vis));
	return 0;
}
