#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int MOD = 1e9 + 7;
const int N = 10;
const int M = 1e5 + 5;

int n, m;
vector <int> tmp;
vector <int> num;

int read() {
	int s = 0, w = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9') {
		if (ch == '-') w = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9') {
		s = s * 10 + ch - '0';
		ch = getchar();
	}
	return s * w;
}

void dfs(int x, int y) {
	if (y + 1 <= n) {
		tmp.push_back(1);
		dfs(x, y + 1);
		tmp.pop();	
	}
	if (x + 1 <= n) {
		tmp.push_back(0);
		dfs(x + 1, y);
	}
}

int main() {
	//从文件输入输出
    freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);

	n= read(), m = read();	
	if (n == 2 && m == 2) cout << 12 << endl;
	else if (n == 3 && m == 3) cout << 112 << endl;
	else if (n == 5 && m == 5) cout << 7136 << endl;
	dfs(1, 1);
	return 0;
}
