#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;

const int M = 3e5 + 5;

int ans = 0;
int n, m;
int u, v;
int a, b, x, y;
int p[100000];
char type[5];

struct EDGE {
	int s;
	int e;
	int nxt;
} Edge[M];

void add(int u, int v) {
	++cnt;
	Edge[cnt].s = u;
	Edge[cnt].e = v;
	Edge[cnt].nxt = head[u];
	head[u] = cnt;
}

int dp[100000][5];

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);

	scanf("%d %d %s", &n, &m, type + 1);
	for (register int i = 1; i <= n; ++i) 
		scanf("%d", &p[i]);
	for (register int i = 1; i <= (n - 1); ++i) {
		scanf("%d %d", &u, &v);
		add(u, v);
		add(v, u);
	}	
	for (register int i = 1; i <= m; ++i) {
		scanf("%d %d %d %d", &a, &x, &b, &y);
		if (type == "A2") {
			if (x == 1) ans += p[a];
			if (y == 1) ans += p[b];
			for (register int i = 1; i <= n; ++i)
				dp[i][0] = 0, dp[i][1] = 0;
			dp[1][1] = p[1];
			for (register int i = 2; i <= n; ++i) {
				dp[i][1] = min(dp[i - 1][0], dp[i - 1][1]) + p[i];
				dp[i][0] += dp[i - 1][1];
			}
			printf("%d\n", dp[n][0], dp[n][1]);
		}
	}
	return 0;
}
