#include <cstdio>
#define int long long
const int maxn = 1e6 + 5;
const int mo = 1e9 + 7;
using namespace std;

int pow(int x, int t){
	x %= mo; int res = 1;
	while (t > 0){
		if (t & 1) (res *= x) %= mo;
		(x *= x) %= mo;
		t >>= 1;
	}
	return res;
}
int n, m;
signed main(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%lld%lld", &n, &m);
	if (n == 1){
		printf("%lld\n", pow(2, m) % mo);
		return 0;
	}
	if (n == 2){
		printf("%lld\n", 4 * pow(3, m - 1) % mo);
	}
	if (n == 3){
		if (m == 1) puts("8");
		else if (m == 2) puts("36");
		else printf("%lld\n", 4 * 28 * pow(3, m - 3) % mo);
	}
	if (n == 4){
		if (m == 1) puts("16");
		if (m == 2) puts("108");
		if (m == 3) puts("336");
		if (m == 4) puts("912");
		if (m == 5) puts("2688");
	}
	if (n == 5){
		if (m == 1) puts("32");
		if (m == 2) puts("324");
		if (m == 3) puts("1008");
		if (m == 4) puts("2688");
		if (m == 5) puts("7136");
	}
	return 0;
}
