#include <cstdio>
#include <cstring>
#include <algorithm>
#define maxn 100005
#define int long long
using namespace std;
const int INF = 0x3f3f3f3f3f3f3f3f;
int n, m, f[maxn][2], p[maxn];
char type[5];
signed main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%lld%lld%s", &n, &m, type);
	if (type[0] == 'A'){
		for (int i = 1; i <= n; i++)
			scanf("%lld", p + i);
		for (int u, v, i = 1; i < n; i++){
			scanf("%lld%lld", &u, &v);
		}
		while (m--){
			int a, ax, b, bx;
			memset(f, 0, sizeof f);
			scanf("%lld%lld%lld%lld", &a, &ax, &b, &bx);
			if (ax == 1) f[a][0] = INF;
			else f[a][1] = INF;
			if (bx == 1) f[b][0] = INF;
			else f[b][1] = INF;
			
			for (int i = 1; i <= n; i++){
				if (f[i][0] != INF) f[i][0] = f[i - 1][1];
				if (f[i][1] != INF) f[i][1] = min(INF, min(f[i - 1][0], f[i - 1][1]) + p[i]);
			}
			if (min(f[n][0], f[n][1]) == INF) puts("-1");
			else printf("%lld\n", min(f[n][0], f[n][1]));
		}
	}
	return 0;
}
