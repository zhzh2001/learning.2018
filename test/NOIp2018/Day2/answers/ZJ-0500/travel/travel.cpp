#include <cstdio>
#include <queue>
#include <vector>
#include <cstring>
#define maxn 5005
using namespace std;

struct nodeE{
	int u, v;
}E[maxn];

struct node{
	int to, nxt;
}e[maxn << 1];
int head[maxn], tot = -1;
void addedge(int u, int v){
	e[++tot].to = v, e[tot].nxt = head[u];
	head[u] = tot;
}
int n, m, res[maxn], cnt = 0;
bool vis[maxn];
void dfs(int cur){
	res[++cnt] = cur;
	vis[cur] = true;
	if (cnt >= n) return;
	priority_queue <int> q; while (!q.empty()) q.pop();
	for (int i = head[cur]; i != -1; i = e[i].nxt){
		if (!vis[e[i].to]) q.push(-e[i].to);
	}
	while (!q.empty()){
		dfs(-q.top());
		q.pop();
	}
}

int f[maxn];
int getf(int x){return x == f[x] ? x : f[x] = getf(f[x]);}
bool isc[maxn];
bool vis2[maxn];
bool findway(int cur, int fa){
	if (vis2[cur]) return true;
	vis2[cur] = true;
	for (int i = head[cur]; i != -1; i = e[i].nxt){
		if (e[i].to == fa) continue;
		if (findway(e[i].to, cur)){
			isc[cur] = true ,isc[e[i].to] = true;
			return true;
		}
	}
	vis2[cur] = false;
	return false;
}
void find_circle(){
	int tag;
	for (int i = 1; i <= n; i++) f[i] = i;
	for (int i = 1; i <= m; i++)
		if (getf(E[i].u) != getf(E[i].v)) f[getf(E[i].u)] = getf(E[i].v);
		else{
			tag = getf(E[i].u);
			isc[tag] = true;
		}
	findway(tag, 0);
}
bool used = false;
void dfs2(int cur){
	if (!vis[cur]){
		res[++cnt] = cur;
		vis[cur] = true;
	}
	if (cnt >= n) return;
	if (!isc[cur] || used){
		priority_queue <int> q; while (!q.empty()) q.pop();
		for (int i = head[cur]; i != -1; i = e[i].nxt){
			if (!vis[e[i].to]) q.push(-e[i].to);
		}
		while (!q.empty()){
			dfs2(-q.top());
			q.pop();
		}
	}
	else{
		used = true;
		priority_queue <int>  q; while (!q.empty()) q.pop();
		for (int i = head[cur]; i != -1; i = e[i].nxt){
			if (!vis[e[i].to]) q.push(-e[i].to);
		}
		res[++cnt] = -q.top(); vis[-q.top()] = true; q.pop();
		priority_queue <int> q2; while (!q2.empty()) q2.pop();
		for (int i = head[res[cnt]]; i != -1; i = e[i].nxt){
			if (!vis[e[i].to] && e[i].to != cur) q2.push(-e[i].to);
		}
		
		if (-q2.top() < -q.top()){
			dfs2(-q2.top());
		}
		else dfs2(-q.top());
	}
}

int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	memset(head, -1, sizeof head);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; i++){
		scanf("%d%d", &E[i].u, &E[i].v);
		addedge(E[i].u, E[i].v), addedge(E[i].v, E[i].u);
	}
	if (m == n - 1){
		dfs(1);
		for (int i = 1; i <= n; i++)
			printf("%d ", res[i]);
		putchar('\n');
	}
	if (m == n){
		find_circle();
		dfs2(1);
		for (int i = 1; i <= n; i++)
			printf("%d ", res[i]);
		putchar('\n');
	}
	return 0;
}
