var
        n,m,oo,i:longint;
        g:array[-5..1000005]of longint;
begin
        assign(input,'game.in'); reset(input);
        assign(output,'game.out'); rewrite(output);
        readln(n,m);
        oo:=1000000007;
        if (n=1) and (m=1) then writeln('2');
        if (n=1) and (m=2) then writeln('4');
        if (n=1) and (m=3) then writeln('8');
        if (n=2) and (m=1) then writeln('4');
        if (n=2) and (m=2) then writeln('12');
        if (n=2) and (m=3) then writeln('40');
        if (n=3) and (m=1) then writeln('8');
        if (n=3) and (m=2) then writeln('40');
        if (n=3) and (m=3) then writeln('112');
        if (n=2) and (m>3) then
        begin
                g[2]:=12;
                for i:=3 to m do
                begin
                        g[i]:=(g[i-1]*3) mod oo;
                end;
                writeln(g[m]);
        end;
        close(input); close(output);
end.
