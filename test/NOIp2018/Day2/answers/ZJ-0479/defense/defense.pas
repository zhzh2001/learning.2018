var
        n,m,t,i,j,x,y,l,r:longint;
        ch:char;
        h:array[-5..100005,0..1]of longint;
        a:array[-5..100005]of longint;
function min(x,y:longint):longint;
begin
        if x<y then exit(x)
        else exit(y);
end;
begin
        assign(input,'defense.in'); reset(input);
        assign(output,'defense.out'); rewrite(output);
        read(n,m); read(ch); read(ch); read(t);
        for i:=1 to n do read(a[i]);
        if ch='A' then
        begin
                for i:=1 to n-1 do readln(x,y);
                for j:=1 to m do
                begin
                        readln(l,x,r,y);
                        if (x=0) and (y=0) and (abs(l-r)=1) then
                        begin
                                writeln('-1');
                                continue;
                        end;
                        h[l,1-x]:=-1;
                        h[r,1-x]:=-1;
                        for i:=1 to n do
                        begin
                                if h[i,1]=0 then
                                begin
                                        h[i,1]:=1000000009;
                                        if h[i-1,1]<>-1 then h[i,1]:=h[i-1,1]+a[i];
                                        if (h[i-1,0]<>-1) and (h[i-1,0]+a[i]<h[i,1]) then h[i,1]:=h[i-1,0]+a[i];
                                end;
                                if h[i,0]=0 then
                                begin
                                        if h[i-1,1]<>-1 then h[i,0]:=h[i-1,1]
                                        else h[i,0]:=-1;
                                end;
                        end;
                        if h[i,0]=-1 then writeln(h[i,1]);
                        if h[i,1]=-1 then writeln(h[i,0]);
                        if (h[i,1]<>-1) and (h[i,0]<>-1) then writeln(min(h[i,1],h[i,0]));
                end;
        end;
        close(input); close(output);
end.