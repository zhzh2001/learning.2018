var
        n,m,i,x,y,ans,mil:longint;
        f:array[-5..5005,-5..5005]of longint;
        g,h:array[-5..5005]of longint;
procedure dfs(t,b:longint);
var
        i:longint;
begin
        for i:=1 to n do
        begin
                if (i<>b) and (f[t,i]=1) then
                begin
                        if ans=n-1 then writeln(i)
                        else write(i,' ');
                        ans:=ans+1;
                        dfs(i,t);
                end;
        end;
end;
procedure solve(t,k,long:longint);
var
        i,max,flag,j:longint;
begin
        g[t]:=k;
        h[long]:=t;
        for i:=1 to n do
        begin
                if (f[t,i]=1) and (i<>k) then
                begin
                        if g[i]=0 then
                        begin
                                g[i]:=t;
                                solve(i,t,long+1);
                        end
                        else
                        begin
                                if mil=1 then continue;
                                mil:=1;
                                if g[i]=t then continue;
                                max:=long+1;
                                flag:=long;
                                while h[flag]<>i do
                                begin
                                        if h[flag]>t then
                                        begin
                                                max:=flag;
                                        end;
                                        flag:=flag-1;
                                end;
                                h[long+1]:=i;
                                for j:=max to long do g[h[j]]:=h[j+1];
                        end;
                end;
        end;
end;
procedure fwrite(t:longint);
var
        i:longint;
begin
        if ans<n then write(t,' ')
        else writeln(t);
        ans:=ans+1;
        if ans>n then exit;
        for i:=1 to n do
        begin
                if g[i]=t then fwrite(i);
        end;
end;
begin
        assign(input,'travel.in'); reset(input);
        assign(output,'travel.out'); rewrite(output);
        readln(n,m);
        for i:=1 to m do
        begin
                readln(x,y);
                f[x,y]:=1;
                f[y,x]:=1;
        end;
        if m=n-1 then
        begin
                ans:=1;
                if n=1 then writeln('1')
                else write('1 ');
                dfs(1,0);
        end
        else
        begin
                mil:=0;
                ans:=1;
                solve(1,-1,1);
                fwrite(1);
        end;
        close(input); close(output);
end.