#include<bits/stdc++.h>
using namespace std;
int n,m;
struct Edge{
	int a,b;
}edge[5010];
bool cmp(Edge x,Edge y)
{
	return x.a<y.a||(x.a==y.a&&x.b<y.b);
}
int e,head[5010],to[10010],Next[10010];
void add(int x,int y)
{
	to[++e]=y;
	Next[e]=head[x];
	head[x]=e;
	return ;
}
int path[5010];
int vis[5010];
int tim;
void dfs(int x,int fa)
{
	path[++path[0]]=x;
	vis[x]=tim;
	for(int i=head[x];i;i=Next[i])
	  {
	  	if(to[i]==fa)
	  	  continue;
	  	if(vis[to[i]]==tim)
	  	  continue;
	  	dfs(to[i],x);
	  }
	return ;
}
void solveone()
{
	tim++;
	for(int i=m;i>=1;i--)
	  {
	  	add(edge[i].a,edge[i].b);
	  	add(edge[i].b,edge[i].a);
	  }
	dfs(1,0);
	for(int i=1;i<=path[0];i++)
	  printf("%d ",path[i]);
	return ;
}
int ans[5010];
void check()
{
	if(path[0]<n)
	  return ;
	int p=1;
	while(p<=path[0])
	  {
	  	if(ans[p]==path[p])
	  	  {
	  	  	p++;
		  }
		else if(ans[p]<path[p])
		  {
		  	return ;
		  }
		else
		  {
		  	for(int i=1;i<=n;i++)
		  	  ans[i]=path[i];
		  	return ;
		  }
	  }
	return ;
}
void solvetwo()
{
	for(int i=1;i<=n;i++)
	  ans[i]=1e9;
	for(int j=1;j<=m;j++)
	  {
	  	tim++;
	  	for(int i=1;i<=n;i++)
	  	  head[i]=0;
	  	e=0;
	  	path[0]=0;
	  	for(int i=m;i>=1;i--)
		  {
		  	if(i==j)
		  	  continue;
		  	add(edge[i].a,edge[i].b);
		  	add(edge[i].b,edge[i].a);
		  }
		dfs(1,0);
		check();
	  }
	for(int i=1;i<=n;i++)
	  printf("%d ",ans[i]);
	return ;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<=m;i++)
	  {
	  	scanf("%d%d",&edge[i].a,&edge[i].b);
	  	if(edge[i].a>edge[i].b)
	  	  swap(edge[i].a,edge[i].b);
	  }
	sort(edge+1,edge+1+m,cmp);
	if(n-1==m)
	  {
	  	solveone();
	  }
	else
	  {
	  	solvetwo();
	  }
	return 0;
}
