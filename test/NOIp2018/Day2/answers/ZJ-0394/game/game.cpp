#include<bits/stdc++.h>
using namespace std;
const long long base=1e9+7;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	cin>>n>>m;
	if(n>m)
	  swap(n,m);
	long long ans=1;
	if(n==1)
	  {
	  	ans=2;
	  	for(int i=2;i<=m;i++)
	  	  ans=(ans*2)%base;
	  }
	else if(n==2)
	  {
	  	ans=4;
	  	for(int i=2;i<=m;i++)
	  	  ans=(ans*3)%base;
	  }
	else if(n==3)
	  {
	  	if(m==1)
	  	  {
	  	  	ans=8;
	  	  }
	  	else if(m==2)
	  	  {
	  	  	ans=36;
		  }
		else if(m==3)
		  {
		  	ans=112;
		  }
		else
		  {
		  	ans=112;
		  	for(int i=4;i<=m;i++)
		  	  ans=(ans*3)%base;
		  }
	  }
	else
	  {
	  	if(n==5&&m==5)
	  	  ans=7136;
	  	else if(n==4)
	  	  {
	  	  	if(m==1)
	  	  	  ans=16;
	  	  	if(m==2)
	  	  	  ans=108;
	  	  	if(m==3)
	  	  	  ans=336;
	  	  	if(m==4)
	  	  	  ans=1112;
	  	  	if(m==5)
	  	  	  ans=3216;
		  }
		else if(n==5)
		  {
		    if(m==1)
			  ans=32;
			if(m==2)
			  ans=324;
			if(m==3)
			  ans=1008;
			if(m==4)
			  ans=3216;	
		  }
	  }
	cout<<ans;
	return 0;
}
