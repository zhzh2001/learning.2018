#include<bits/stdc++.h>
using namespace std;
char type[1231];
long long  p[100010];
int e,head[100010],to[200010],Next[200010];
void add(int x,int y)
{
	to[++e]=y;
	Next[e]=head[x];
	head[x]=e;
	return ;
}
int mp[2010][2010];
long long f[100010][2];
void dp(int x,int fa,int a,int xx,int b,int y)
{
	f[x][1]=p[x];
	for(int i=head[x];i;i=Next[i])
	  {
	  	if(to[i]==fa)
	  	  continue;
	  	dp(to[i],x,a,xx,b,y);
	  	f[x][1]+=min(f[to[i]][1],f[to[i]][0]);
	  	f[x][0]+=f[to[i]][1];
	  }
	if(x==a)
	  {
	  	if(xx)
	  	  f[x][0]=1e15;
	  	else
	  	  f[x][1]=1e15;
	  }
	if(x==b)
	  {
	  	if(y)
	  	  f[x][0]=1e15;
	  	else
	  	  f[x][1]=1e15;
	  }
	return ;
}
void solve(int a,int x,int b,int y)
{
	memset(f,0,sizeof(f));
	dp(1,0,a,x,b,y);
	long long ans=min(f[1][1],f[1][0]);
	if(ans>=1e15)
	  printf("-1\n");
	else
	  printf("%lld\n",ans);
	return ;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	scanf("%s",type);
	for(int i=1;i<=n;i++)
	  scanf("%lld",&p[i]);
	for(int i=1;i<n;i++)
	  {
	  	int a,b;
	  	scanf("%d%d",&a,&b);
	  	add(a,b);
	  	add(b,a);
	  	mp[a][b]=mp[b][a]=1;
	  }
	for(int i=1;i<=m;i++)
	  {
	  	int a,x,b,y;
	  	scanf("%d%d%d%d",&a,&x,&b,&y);
	  	if(x==0&&y==0&&mp[a][b])
	  	  printf("-1\n");
	  	else
	  	  solve(a,x,b,y);
	  }
	return 0;
}
