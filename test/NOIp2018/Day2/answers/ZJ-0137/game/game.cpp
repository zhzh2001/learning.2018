#include <bits/stdc++.h>
using namespace std;
#define int long long
const int md=1000000007;
int n,m;
inline void biao()
{
	if(n==1){printf("%lld\n",(1<<m));}
	else if(n==2){if(m==1)puts("4");else if(m==2)puts("12");else if(m==3)puts("36");}
	else if(n==3){if(m==1)puts("8");else if(m==2)puts("36");else if(m==3)puts("112");}
}
inline int ksm(int x,int y)
{
	int re=1LL;
	while(y)
	{
		if(y&1)re=1LL*re*x%md; x=1LL*x*x%md; y>>=1;
	}return re;
}
signed main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int ans; scanf("%lld%lld",&n,&m);
	if(n<=3&&m<=3)
	{
		biao();return 0;
	}
	else if(n==2)
	{
		ans=ksm(3LL,m-1)%md; ans=ans*4LL%md; printf("%lld\n",ans); return 0;
	}
}
