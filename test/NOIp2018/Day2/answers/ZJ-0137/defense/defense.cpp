#include <bits/stdc++.h>
using namespace std;
#define int long long
inline int read()
{
	int f=0,s=0; char c;
	while(!isdigit(c)){f|=(c=='-');c=getchar();}
	while(isdigit(c)){s=s*10+c-'0';c=getchar();}
	return (f)?(-s):s;
}
#define R(x) x=read()
#define PB push_back
const int N=100005,B=2005,inf=0x7fffffffff;
int n,m,v[N],bo[N],f[N][2];
vector<int>e[N];
inline void dfs(int x,int la)
{
	if(bo[x]!=2)f[x][0]=0; if(bo[x]!=1)f[x][1]=v[x];
	for(vector<int>::iterator it=e[x].begin();it!=e[x].end();it++)if((*it)!=la)
	{
		dfs((*it),x);
		if(bo[x]!=2)
		{
			f[x][0]+=f[(*it)][1];
		}
		if(bo[x]!=1)
		{
			f[x][1]+=min(f[(*it)][0],f[(*it)][1]);
		}
	}
}
int fa[N];
inline void Dfs(int x,int la)
{
	for(vector<int>::iterator it=e[x].begin();it!=e[x].end();it++)if((*it)!=la)
	{
		fa[(*it)]=x; Dfs((*it),x);
	}
}
signed main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char s[5]; int i,j,a,x,b,y,re,ga,gb,gx,gy,tmp1,tmp2; R(n); R(m); scanf("%s",s+1); for(i=1;i<=n;i++)R(v[i]);
	for(i=1;i<n;i++)
	{
		R(x); R(y); e[x].PB(y); e[y].PB(x);
	}
	if(n<=2000)
	{
		for(i=1;i<=m;i++)
		{
			for(j=1;j<=n;j++){bo[j]=0;f[j][0]=f[j][1]=inf;} R(a); R(x); R(b); R(y);
			bo[a]=x+1; bo[b]=y+1; dfs(1,0); re=min(f[1][0],f[1][1]);
			if(re>=inf)re=-1; printf("%lld\n",re);
		}
	}
	else
	{
		fa[1]=0; Dfs(1,0); memset(bo,0,sizeof bo); for(i=1;i<=n;i++)f[i][0]=f[i][1]=inf; dfs(1,0);
		for(i=1;i<=m;i++)
		{
			R(a); R(x); R(b); R(y); bool pl=0;
			if((!x)&&(!y))
			{
				for(vector<int>::iterator it=e[a].begin();it!=e[a].end();it++)
				{
					if((*it)==b){pl=1;break;}
				}if(pl){puts("-1");continue;}
			}ga=a; gx=x; gb=b; gy=y; tmp1=f[a][x]; tmp2=f[b][y]; f[a][x]=f[b][y]=inf; x=fa[a]; y=fa[b];
			while(x>1)
			{
				f[x][0]=0; f[x][1]=v[x];
				for(vector<int>::iterator it=e[x].begin();it!=e[x].end();it++)if(*it!=fa[x])
				{
					f[x][0]+=f[(*it)][1]; f[x][1]+=min(f[(*it)][0],f[(*it)][1]);
				}x=fa[x];
			}
			x=y;
			while(x>1)
			{
				f[x][0]=0; f[x][1]=v[x];
				for(vector<int>::iterator it=e[x].begin();it!=e[x].end();it++)if(*it!=fa[x])
				{
					f[x][0]+=f[(*it)][1]; f[x][1]+=min(f[(*it)][0],f[(*it)][1]);
				}x=fa[x];
			}
			x=1; f[x][0]=0; f[x][1]=v[x];
			for(vector<int>::iterator it=e[x].begin();it!=e[x].end();it++)if(*it!=fa[x])
			{
				f[x][0]+=f[(*it)][1]; f[x][1]+=min(f[(*it)][0],f[(*it)][1]);
			}re=min(f[1][0],f[1][1]); if(re>1e10)re=-1; printf("%lld\n",re); f[ga][gx]=tmp1; f[gb][gy]=tmp2;
		}
	}
}
