#include <fstream>
#include <cstring>
#include <cstdio>
#include <set>

using namespace std;

ifstream cin("travel.in");
ofstream cout("travel.out");

set<int> map[5010];

bool f[5010];

int dfs(int start)
{
	cout<<start<<" ";
	set<int>::iterator it;
	for (it=map[start].begin();it!=map[start].end();++it)
		if (!f[*it]){
			f[*it]=true;
			dfs(*it);
		}
	return 0;
}

int xia[5010];
bool huanzhong[5010];

int shan(int x,int y)
{
	map[x].erase(y);
	map[y].erase(x);
	return 0;
}

int zhao(int start,int yeye)
{
	set<int>::iterator it;
	
	for (it=map[start].begin();it!=map[start].end();++it)
	{
		if (*it==yeye) continue;
		if (xia[*it]) 
		{
			int x=*it,maxn=*it,y,qzygs=start;
			while (x!=start) 
			{
				huanzhong[x]=true;
				y=x;
				x=xia[x];
				if (x>maxn)
					maxn=x,qzygs=y;
			}
			huanzhong[start]=true;
			if (qzygs<xia[maxn])
			{
				shan(qzygs,maxn);
			}
			else
			{
				shan(xia[maxn],maxn);
			}
			return -1;
		}
		xia[start]=*it;
		if (zhao(*it,start)==-1) return -1;
		xia[start]=0;
	}
	return 0;
}

int main()
{
	int n,m,i,x,y;
	cin>>n>>m;
	memset(f,false,sizeof(f));
	for (i=1;i<=m;i++)
	{
		cin>>x>>y;
		map[x].insert(y);
		map[y].insert(x);
	}
	if (m==n-1)
	{
		f[1]=true;
		dfs(1);
		cout<<endl;
		return 0;
	}
	memset(xia,0,sizeof(xia));
	memset(huanzhong,false,sizeof(huanzhong));
	zhao(1,0);
	f[1]=true;
	dfs(1);
	cout<<endl;
	return 0;
}

