#include <fstream>
#include <cstdio>

using namespace std;

ifstream cin("game.in");
ofstream cout("game.out");

const int mo=1e9+7;

int main()
{
	int n,i,m,ans=1;
	cin>>n>>m;
	for (i=1;i<=min(n,m);i++) 
		ans=(ans*(i+1))%mo;
	for (i=min(n,m)+1;i<=max(n,m);i++)
		ans=(ans*(min(n,m)+1))%mo;
	for (i=max(n,m);i<n+m-1;i++)
		ans=(ans*(n+m-i))%mo;
	cout<<ans<<endl;
	return 0;
}

