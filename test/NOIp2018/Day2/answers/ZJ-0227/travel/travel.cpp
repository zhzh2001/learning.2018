#include <bits/stdc++.h>
using namespace std;
#define maxn 5005
#define res register int
#define rep(a,b,c) for((a)=(b);(a)<=(c);++(a))
inline void read(int &x){
	static int f;static char ch;
	for(x=f=0,ch=getchar();!isdigit(ch);f|=ch=='-',ch=getchar());
	for(;isdigit(ch);x=(x<<1)+(x<<3)+ch-48,ch=getchar());
	x=f?-x:x;
	return;
}
inline void print(res x){
	if(!x){
		putchar(48);
		return;
	}
	if(x<0){
		putchar('-');
		x=-x;
	}
	static int Top=-1,Stack[20];
	for(;x;Stack[++Top]=x%10,x/=10);
	for(;~Top;putchar(Stack[Top--]+48));
	return;
}

struct Edge_read{
	int u,v,index;
	bool operator < (const Edge_read &t)const{return v>t.v;}
}E[maxn<<1];
struct Edge{
	int nxt,to,index;
}e[maxn<<1];
int cnt,head[maxn];
inline void addedge(res u,res v,res index){
	e[++cnt].nxt=head[u];
	e[cnt].to=v;
	e[cnt].index=index;
	head[u]=cnt;
	return;
}

int tot,seq[maxn];
bool vis[maxn];
void dfs_(res u,res ee){
	seq[++tot]=u;
	vis[u]=true;
	res i,v;
	for(v=e[i=head[u]].to;i;v=e[i=e[i].nxt].to)
		if((!vis[v])&&e[i].index!=ee)
			dfs_(v,ee);
	return;
}

int n,m;
int ans[maxn];
inline bool cmp(){
	res i;
	rep(i,1,n){
		if(ans[i]<seq[i])return false;
		if(ans[i]>seq[i])return true;
	}
	return false;
}
inline void dfs(res ee){
	tot=0;
	res i;
	rep(i,1,n)vis[i]=false;
	dfs_(1,ee);
	if(tot!=n)return;
	if(cmp()){
		rep(i,1,n)ans[i]=seq[i];
	}
	return;
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);
	read(m);
	res i;
	cnt=0;
	rep(i,1,n)head[i]=0;
	rep(i,1,m){
		read(E[i].u);
		read(E[i].v);
		E[i+m].u=E[i].v;
		E[i+m].v=E[i].u;
		E[i].index=i;
		E[i+m].index=i;
	}
	sort(E+1,E+2*m+1);
	rep(i,1,2*m)addedge(E[i].u,E[i].v,E[i].index);
	rep(i,1,n)ans[i]=n+1;
	if(m==n-1)dfs(0);
	else {
		rep(i,1,n)dfs(i);
	}
	rep(i,1,n){
		print(ans[i]);
		putchar(' ');
	}
	putchar('\n');
	fclose(stdin);
	fclose(stdout);
	return 0;
}
