#include <bits/stdc++.h>
using namespace std;
#define maxn 100005
#define res register int
#define rep(a,b,c) for((a)=(b);(a)<=(c);++(a))
#define _min(a,b) ((a)<(b)?(a):(b))
#define _swap(a,b) ((a)^=(b)^=(a)^=(b))
inline void read(int &x){
	static int f;static char ch;
	for(x=f=0,ch=getchar();!isdigit(ch);f|=ch=='-',ch=getchar());
	for(;isdigit(ch);x=(x<<1)+(x<<3)+ch-48,ch=getchar());
	x=f?-x:x;
	return;
}
inline void print(res x){
	if(!x){
		putchar(48);
		return;
	}
	if(x<0){
		putchar('-');
		x=-x;
	}
	static int Top=-1,Stack[20];
	for(;x;Stack[++Top]=x%10,x/=10);
	for(;~Top;putchar(Stack[Top--]+48));
	return;
}

int n,m;
int val[maxn];
struct Edge{
	int nxt,to;
}e[maxn<<1];
int cnt,head[maxn];
inline void addedge(res u,res v){
	e[++cnt].nxt=head[u];
	e[cnt].to=v;
	head[u]=cnt;
}

namespace solve1{
	int fa[2005];
	void dfs(res u){
		res i,v;
		for(v=e[i=head[u]].to;i;v=e[i=e[i].nxt].to)
			if(v!=fa[u]){
				fa[v]=u;
				dfs(v);
			}
	}
	int f[2005],g[2005],lim[2005];	//f[i]choose,g[i]notchoose
	bool flag;
	void DP(res u){
		f[u]=val[u];
		g[u]=0;
		res i,v;
		for(v=e[i=head[u]].to;i;v=e[i=e[i].nxt].to)
			if(v!=fa[u]){
				DP(v);
				if(flag)return;
				if(!lim[v]){
					f[u]+=_min(f[v],g[v]);
					g[u]+=f[v];
				}
				else if(lim[v]==1){
					f[u]+=f[v];
					g[u]+=f[v];
				}
				else {
					if(!(~lim[u])){
						flag=true;
						return;
					}
					lim[u]=1;
					f[u]+=g[v];
				}
			}
		return;
	}
	void solve(){
		dfs(1);
		res i,a,b,x,y;
		for(;m;--m){
			rep(i,1,n)lim[i]=0;
			read(a);
			read(x);
			read(b);
			read(y);
			if((fa[a]==b||fa[b]==a)&&(!x)&&(!y)){
				print(-1);
				putchar('\n');
				continue;
			}
			lim[a]=(x?1:-1);
			lim[b]=(y?1:-1);
			flag=false;
			DP(1);
			if(flag)print(-1);
			else if(!lim[1])print(_min(f[1],g[1]));
			else if(~lim[1])print(f[1]);
			else print(g[1]);
			putchar('\n');
		}
	}
}

namespace solve2{
//	int dep[maxn],size[maxn],son[maxn],fa[maxn],top[maxn];
//	void dfs1(res u){
//		size[u]=1;
//		dep[u]=dep[fa[u]]+1;
//		res mx=0,i,v;
//		for(v=e[i=head[u]].to;i;v=e[i=e[i].nxt].to)
//			if(v!=fa[u]){
//				fa[v]=u;
//				dfs1(v);
//				size[u]+=size[v];
//				if(size[v]>mx){
//					mx=size[v];
//					son[u]=v;
//				}
//			}
//		return;
//	}
//	void dfs2(res u,res topf){
//		top[u]=topf;
//		if(!son[u])return;
//		dfs2(son[u],topf);
//		res i,v;
//		for(v=e[i=head[u]].to;i;v=e[i=e[i].nxt].to)
//			if(v!=fa[u]&&v!=son[u])
//				dfs2(v,v);
//	}
//	inline int LCA(res u,res v){
//		for(;top[u]!=top[v];){
//			if(dep[top[u]]<dep[top[v]])_swap(u,v);
//			u=fa[top[u]];
//		}
//		return dep[u]<dep[v]?u:v;
//	}
	int fa[maxn],dep[maxn];
	void dfs(res u){
		res i,v;
		for(v=e[i=head[u]].to;i;v=e[i=e[i].nxt].to)
			if(v!=fa[u]){
				fa[v]=u;
				dep[v]=dep[u]+1;
				dfs(v);
			}
	}
	int f[maxn],g[maxn];
	void DP(res u){
		f[u]=val[u];
		g[u]=0;
		res i,v;
		for(v=e[i=head[u]].to;i;v=e[i=e[i].nxt].to)
			if(v!=fa[u]){
				DP(v);
				f[u]+=_min(f[v],g[v]);
				g[u]+=f[v];
			}
		return;
	}
	int ff[maxn],gg[maxn],lim[maxn];
	void reDP(res v){
		res u=fa[v];
		lim[u]=0;
		ff[u]=f[u]-_min(f[v],g[v]);
		gg[u]=g[u]-f[v];
		if(lim[v]==3*m+1){
			ff[u]+=ff[v];
			gg[u]+=ff[v];
		}
		else if(lim[v]==3*m-1){
			lim[u]=1;
			ff[u]+=gg[v];
		}
		else {
			ff[u]+=_min(ff[v],gg[v]);
			gg[u]+=ff[v];
		}
		return;
	}
	void solve(){
		res i,a,b,x,y;
		rep(i,1,n)fa[i]=dep[i]=0;
		dfs(1);
		DP(1);
		for(;m;--m){
			read(a);
			read(x);
			read(b);
			read(y);
			if(x){
				ff[a]=f[a];
				lim[a]=3*m+1;
			}
			else {
				gg[a]=g[a];
				lim[a]=3*m-1;
			}
			if(y){
				ff[b]=f[b];
				lim[b]=3*m+1;
			}
			else {
				gg[b]=g[b];
				lim[b]=3*m-1;
			}
			res u=a,v=b;
			if((fa[a]==b||fa[b]==a)&&(!x)&&(!y)){
				print(-1);
				putchar('\n');
				continue;
			}
			for(;a!=b;){
				if(dep[a]<dep[b])_swap(a,b);
				reDP(a);
				a=fa[a];
			}
			lim[u]=x?3*m+1:3*m-1;
			lim[v]=y?3*m+1:3*m-1;
			for(;a!=1;a=fa[a])reDP(a);
			if(lim[1]==3*m+1)print(ff[1]);
			else if(lim[1]==3*m-1)print(gg[1]);
			else print(_min(ff[1],gg[1]));
			putchar('\n');
		}
	}
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n);
	read(m);
	char type1=getchar();
	for(;!isalpha(type1);type1=getchar());
	char type2=getchar();
	for(;!isdigit(type2);type2=getchar());
	res i,u,v;
	rep(i,1,n)read(val[i]);
	rep(i,1,n-1){
		read(u);
		read(v);
		addedge(u,v);
		addedge(v,u);
	}
	if(n<=2000&&m<=2000){
		solve1::solve();
		return 0;
	}
	if(type2=='B'){
		solve2::solve();
		return 0;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
