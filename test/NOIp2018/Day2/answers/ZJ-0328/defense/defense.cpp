#include<bits/stdc++.h>
using namespace std;
#define rep(x,y,z) for(int x=y; x<=z; x++)
#define downrep(x,y,z) for(int x=y; x>=z; x--)
#define LL long long
#define repedge(x,y) for(int x=hed[y]; ~x; x=edge[x].nex)
#define ms(x,y,z) memset(x,y,sizeof(z))
const int N=100005;
const LL inf=1e9;
int n,m,hed[N],nedge;
LL p[N];
char Cas[15];
struct Edge{ int to,nex; }edge[N<<1];
void addedge(int a,int b){
	edge[nedge].to=b; edge[nedge].nex=hed[a]; hed[a]=nedge++;
}
class Poi_1{
	public:
	int fa[N],a,b,ac,bc;
	LL f[N][2];
	void dfs(int k){
		f[k][0]=0; f[k][1]=p[k];
		repedge(i,k){
			int v=edge[i].to; if (v==fa[k]) continue;
			fa[v]=k; dfs(v);
			f[k][0]+=f[v][1]; f[k][1]+=min(f[v][0],f[v][1]);
		}
		if (a==k) f[k][ac^1]=inf;
		if (b==k) f[k][bc^1]=inf;
	}
	int init(){
		rep(i,1,m){
			scanf("%d%d%d%d",&a,&ac,&b,&bc);
			rep(j,1,n) f[j][0]=f[j][1]=inf;
			dfs(1);
		    LL ans=min(f[1][0],f[1][1]);
		    if (ans>=inf) ans=-1; 
		    printf("%lld\n",ans);
		}
		return 0;
	}
}P1;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,&Cas); nedge=0; ms(hed,-1,hed);
	rep(i,1,n) scanf("%lld",&p[i]);
	rep(i,1,n-1){ int a,b; scanf("%d%d",&a,&b);
	   addedge(a,b); addedge(b,a); }
	if ((n<=2000)&&(m<=2000)) return P1.init();
	return 0;
}
