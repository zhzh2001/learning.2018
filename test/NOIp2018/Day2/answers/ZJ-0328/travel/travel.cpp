#include<bits/stdc++.h>
using namespace std;
#define rep(x,y,z) for(int x=y; x<=z; x++)
#define downrep(x,y,z) for(int x=y; x>=z; x--)
#define LL long long
#define repedge(x,y) for(int x=hed[y]; ~x; x=edge[x].nex)
#define ms(x,y,z) memset(x,y,sizeof(z))
const int N=5005;
int n,m,ans[N],nw[N],tp,dfn[N],tot,fa[N],huan_tp,huan_dw,huan[N],vis[N],fenge;
vector<int> e[N];
void find_huan(int k,int pre){
	int t=e[k].size(); dfn[k]=++tot; fa[k]=pre;
	rep(i,0,t-1){ 
	   int v=e[k][i]; if (v==pre) continue;
	   if (!dfn[v]) find_huan(v,k); else if (dfn[v]>dfn[k]){
	   	  huan_tp=k; for(int j=v; j!=k; j=fa[j]) huan[j]=1; 
		  huan[k]=1; huan_dw=v;
	   }
	}
}
void dfs(int k,int pre){
	nw[++tp]=k; vis[k]=1; int t=e[k].size(); int pd=0;
	rep(i,0,t-1){
		int v=e[k][i]; if ((v==pre)||(vis[v])) continue;
		if ((k==fenge)&&(huan[v])&&(!pd)){ pd=1; continue; }
		dfs(v,k); 
	}
}
int check(){
	rep(i,1,n) if (nw[i]<ans[i]) return 1; 
	else if (nw[i]>ans[i]) return 0;
	return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m); 
	rep(i,1,m){ int a,b; scanf("%d%d",&a,&b);
	  e[a].push_back(b); e[b].push_back(a); }
	rep(i,1,n) if (e[i].size()) sort(e[i].begin(),e[i].end());
	if (m==n-1){ 
	   dfs(1,1);
	   rep(i,1,n) if (i==n) printf("%d\n",nw[i]); else printf("%d ",nw[i]);
	   return 0;
	}
	find_huan(1,1);
	rep(i,1,n) ans[i]=n;
	for(int i=huan_dw; i!=huan_tp; i=fa[i]){
		fenge=i; rep(j,1,n) vis[j]=0; tp=0;
		dfs(1,1); 
        if (check()) rep(j,1,n) ans[j]=nw[j];
	}
	fenge=huan_tp; rep(j,1,n) vis[j]=0; tp=0;
	dfs(1,1);
	if (check()) rep(j,1,n) ans[j]=nw[j]; 
	rep(i,1,n) if (i==n) printf("%d\n",ans[i]); else printf("%d ",ans[i]);
	return 0;
}
