#include<bits/stdc++.h>
using namespace std;
#define rep(x,y,z) for(int x=y; x<=z; x++)
#define downrep(x,y,z) for(int x=y; x>=z; x--)
#define LL long long
#define repedge(x,y) for(int x=hed[y]; ~x; x=edge[x].nex)
#define ms(x,y,z) memset(x,y,sizeof(z))
int n,m;
class Poi_1{
public:
int tot,a[15][15],w[35],nw[35],ans,tp;
int dfs(int x,int y){
	int res=1;
	if ((x==n)&&(y==m)){
		rep(i,1,tp) if (w[i]>nw[i]) return 0;
		else if (w[i]<nw[i]){ rep(j,1,tp) nw[j]=w[j]; return 1; }
		return 1;
	}
	if ((x<=0)||(x>n)||(y<=0)||(y>m)) return 1;
	w[++tp]=a[x+1][y]; res&=dfs(x+1,y); --tp; if (!res) return 0;
	w[++tp]=a[x][y+1]; res&=dfs(x,y+1); --tp;
	return res;
}
int check(int x){
	rep(i,1,tot){
		int h=(i%m)? (i/m+1):(i/m); int l=(i%m)? (i%m):m;
		a[h][l]=(((1<<(i-1))&x)>0);
	}
	tp=0; rep(i,1,n+m) nw[i]=1e5;
	return dfs(1,1); 
}
int init(){
	tot=n*m; ans=0;
	rep(i,0,(1<<tot)-1)
	if (check(i)) ++ans;
	printf("%d\n",ans);
	return 0; 
}
}P1;
const int N=300;
const LL mod=1e9+7;
int tot;
struct mat{ 
    LL num[N][N]; void init(){ ms(num,0,num); } 
    void one(){ init(); rep(i,0,N-1) num[i][i]=1;}
}A,B,c,res;
mat multi(mat &a,mat &b){
	c.init();
	rep(i,0,tot-1) rep(j,0,tot-1) rep(k,0,tot-1)
	c.num[i][j]=(c.num[i][j]+a.num[i][k]*b.num[k][j])%mod;
	return c;
}
mat pw(mat &a,LL b){
	res.one(); 
	while(b){ if (b&1) res=multi(res,a); a=multi(a,a); b>>=1; }
	return res;
}
class Poi_2{
public:
LL ans;
int check(int x,int y){
	rep(i,0,n-2) if ((((1<<i)&y)>0)>(((1<<(i+1))&x)>0)) return 0;
	return 1;
}
int init(){
	tot=(1<<n);
	rep(i,0,tot-1) rep(j,0,tot-1) 
	B.num[i][j]=check(i,j); 
	rep(i,0,tot-1) A.num[1][i]=1; B=pw(B,m-1);
	A=multi(A,B); ans=0;
    rep(i,0,tot-1) ans=(ans+A.num[1][i])%mod; ans=(ans%mod+mod)%mod;
    printf("%lld\n",ans);
    return 0;
}   
}P2;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m); 
	if ((n<=3)&&(m<=3)) return P1.init();
	if ((n<=2)||(m<=2)) return P2.init();
	return P1.init();
}
