#include <fstream>
#include <utility>
#include <cstring>
#include <algorithm>
std::ifstream Fin("defense.in");
const int max = 100000;
int p[max | 1],idx[max | 1];
std::pair <int,int> edge[max - 1 << 1 | 1];
inline void link(int u,int v)
{
	static int cnt;
	edge[++ cnt] = (std::pair <int,int>){v,idx[u]},idx[u] = cnt;
}
int a,x,b,y;
typedef long long LL;
LL F[max | 1],G[max | 1];
inline void chk_min(LL & x,LL y)
{
	if (x > y)
		x = y;
}
const LL inf = 0x3f3f3f3f3f3f3f;
void DFS(int cur,int fa)
{
	LL & F = ::F[cur],& G = ::G[cur],delta = inf;
	bool flag = cur > 1 && ! edge[idx[cur]].second;
	for (int i = idx[cur];i;i = edge[i].second)
	{
		int to = edge[i].first;
		if (to != fa)
		{
			DFS(to,cur);
			if (::F[to] < ::G[to])
			{
				F += ::F[to],chk_min(delta,::G[to] - ::F[to]);
				G += ::F[to];
			}
			else
			{
				F += ::G[to],flag = 1;
				G += ::G[to];
			}
		}
	}
	if (! flag)
		F += delta;
	G += p[cur];
	if (cur == a)
		(! x ? G : F) = inf;
	
	if (cur == b)
		(! x ? G : F) = inf;
}
std::ofstream Fout("defense.out");
int main()
{
	std::ios_base::sync_with_stdio(0),Fin.tie(0);
	int n,m;
	char type0;
	int type1;
	Fin >> n >> m >> type0 >> type1;
	for (int i = 1;i <= n;++ i)
		Fin >> p[i];
	while (-- n)
	{
		int u,v;
		Fin >> u >> v;
		link(u,v),link(v,u);
	}
	while (m --)
	{
		Fin >> a >> x >> b >> y;
		std::memset(F,0,sizeof F),std::memset(G,0,sizeof G),DFS(1,0);
		LL ans = std::min(F[1],G[1]);
		Fout << (ans < inf ? ans : - 1) << '\n';
	}
}
