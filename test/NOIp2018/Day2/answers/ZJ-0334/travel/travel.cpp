#include <cstdio>
#include <cstring>
#include <algorithm>
#include <set>
#include <vector>

#define ri register int
#define pb push_back
#define mp make_pair
#define px first
#define py second

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int INF = 0x3f3f3f3f;

const int MAXN = 10005;

int N, M;

vector<pii> e[MAXN];

inline int read() {
	int ch = getchar(), x = 0;
	while (ch < '0' || ch > '9') ch = getchar();
	while ('0' <= ch && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x;
}

set<int> cango;
vector<int> ans;

int vis[MAXN];
int vis2[MAXN];
int isson[MAXN];
int vise[MAXN];
int flag = 0;

inline void dfs2(int u) {
	vis2[u] = 1;
	for (int i = 0; i < e[u].size(); ++i) {
		int v = e[u][i].px, no = e[u][i].py;
		if (!vis2[v] && !vise[no]) dfs2(v);
	}
}



inline int check(int u, int v) {
	memset(vis2, 0, sizeof(vis2));
	dfs2(v);
	for (int i = 1; i <= N; ++i) 
		if (!vis2[i] && !vis[i]) return 0;
	return 1;
}



inline void dfs(int u, int f) {	
	vis[u] = 1;
	ans.pb(u);
	cango.erase(u);
	memset(isson, 0, sizeof(isson));
	for (int i = 0; i < e[u].size(); ++i) {
		int v = e[u][i].px;		
		if (vis[v]) continue;
		isson[v] = 1;
		cango.insert(v);
	}
	set<int>::iterator it = cango.begin();
	while (!check(u, *it) && it != cango.end()) ++it;
	if (it != cango.end()) {
		for (int i = 0; i < e[u].size(); ++i) 
			if (e[u][i].px == *it) vise[e[u][i].py] = 1;
		dfs(*it, u);
	}

}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d %d", &N, &M);
	for (int i = 1; i <= M; ++i) {
		int u = read();
		int v = read();
		e[u].pb(mp(v, i));
		e[v].pb(mp(u, M + i));
	}
	
	cango.insert(1);
	dfs(1, -1);
	for (int i = 0; i < N; ++i) printf("%d ", ans[i]);
	return 0;
}
