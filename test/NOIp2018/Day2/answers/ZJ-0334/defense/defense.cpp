#include <cstdio>
#include <cstring>
#include <algorithm>

#define ri register int
#define pb push_back
#define mp make_pair
#define px first
#define py second

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int INF = 0x3f3f3f3f;

int N, M;

const int MAXN = 100005;

int mx;

int a[MAXN], p[MAXN], q[MAXN];

inline void dfs(int u, int s, int pre) {
	if (s >= mx) return;
	if (u > N) {
		mx = s;
		return;
	}
	if (q[u] == 1) {
		dfs(u + 1, s + p[u], 1);
		return;
	} 
	if (q[u] == 0) {
		if (pre == 0) return;
		dfs(u + 1, s, 0);
		return;
	}
	
	dfs(u + 1, s + p[u], 1);
	if (pre) dfs(u + 1, s, 0);
}

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d %d", &N, &M);
	while (getchar() != '\n');
	for (int i = 1; i <= N; ++i) scanf("%d", &p[i]);
	for (int i = 1; i < N; ++i) scanf("%d", &a[i]);
	for (int i = 1; i <= M; ++i) {
		int x, _x, y, _y;
		scanf("%d %d %d %d", &x, &_x, &y, &_y);
		memset(q, -1, sizeof(q));
		if (x + 1 == y || x - 1 == y) printf("-1\n");
		else {
			mx = INF;
			q[x] = _x, q[y] = _y;
			dfs(1, 0, 1);			
			printf("%d\n", mx);
		}
	}
	return 0;
}
