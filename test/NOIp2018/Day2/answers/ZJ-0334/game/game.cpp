#include <cstdio>
#include <cstring>
#include <algorithm>

#define ri register int
#define pb push_back
#define mp make_pair
#define px first
#define py second

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int INF = 0x3f3f3f3f;
const int MOD = 1e9 + 7;

int N, M;

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d %d", &N, &M);
	if (N == 1) {
		ll ans = 1;
		for (int i = 1; i < M; ++i) ans = ans * 2 % MOD;
		printf("%lld\n", ans);
	} else if (N == 2) {
		ll ans = 4;
		for (int i = 1; i < M; ++i) ans = ans * 3 % MOD;
		printf("%lld\n", ans);
	} else if (N == 3) {
		printf("112\n");
	} else {
		printf("7136\n");
	}
	return 0;
}
