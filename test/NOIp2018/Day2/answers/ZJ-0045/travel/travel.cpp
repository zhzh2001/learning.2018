#include <bits/stdc++.h>
#define rep(i,a,b) for(int i(a),i##_END_(b);i<=i##_END_;i++)
#define drep(i,a,b) for(int i(a),i##_END_(b);i>=i##_END_;i--)
#define File(_) freopen(#_".in","r",stdin),freopen(#_".out","w",stdout)
template<class T> inline bool tomax(T &a,T b){return a<b?a=b,1:0;}
template<class T> inline bool tomin(T &a,T b){return a>b?a=b,1:0;}
const int MN=5005;
bool cur1;
std::vector<int> mp[MN];
namespace P60{
	void dfs(int o,int f){
		printf("%d ",o);
		for(int i=0;i<mp[o].size();i++)
			if(mp[o][i]!=f)
				dfs(mp[o][i],o);
	}
	int main_(){
		dfs(1,0);puts("");
		return 0;
	}
}
int a[MN],b[MN],n,m;
namespace P40{
	std::vector<int> ans,now;
	int k,cnt;
	bool vis[MN];
	void dfs(int o){
		if(vis[o])return;
		vis[o]=true;
		now.push_back(o);cnt++;
		for(int i=0;i<mp[o].size();i++){
			int v=mp[o][i];
			if(o==a[k]&&v==b[k])continue;
			if(o==b[k]&&v==a[k])continue;
			dfs(v);
		}
	}
	bool cmp(std::vector<int> &a,std::vector<int> &b){
		for(int i=0;i<a.size();i++)
			if(a[i]!=b[i])
				return a[i]<b[i];
		return false;
	}
	int main_(){
		rep(i,1,m){
			k=i;now.clear();cnt=0;
			memset(vis,0,sizeof vis);dfs(1);
			if(cnt!=n)continue;
			if(ans.empty())ans=now;
			else if(cmp(now,ans))
				ans=now;
		}
		for(int i=0;i<ans.size();i++)
			printf("%d ",ans[i]);
		return 0;
	}
}
bool cur2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	File(travel);
	scanf("%d%d",&n,&m);
	rep(i,1,m){
		scanf("%d%d",a+i,b+i);
		mp[a[i]].push_back(b[i]);
		mp[b[i]].push_back(a[i]);
	}
	rep(i,1,n)
		std::sort(mp[i].begin(),mp[i].end());
	if(m==n-1)return P60::main_();
	return P40::main_();
}
