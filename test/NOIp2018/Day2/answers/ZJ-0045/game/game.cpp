#include <bits/stdc++.h>
#define rep(i,a,b) for(int i(a),i##_END_(b);i<=i##_END_;i++)
#define drep(i,a,b) for(int i(a),i##_END_(b);i>=i##_END_;i--)
#define File(_) freopen(#_".in","r",stdin),freopen(#_".out","w",stdout)
template<class T> inline bool tomax(T &a,T b){return a<b?a=b,1:0;}
template<class T> inline bool tomin(T &a,T b){return a>b?a=b,1:0;}
const int MOD=1e9+7;
typedef long long ll;
bool cur1;
int n,m;
int qikpow(int a,int k){
	int ans=1;
	for(;k;k>>=1,a=(ll)a*a%MOD)
		if(k&1)ans=(ll)ans*a%MOD;
	return ans;
}
namespace P20{
	const int ANS[3][3]={{2,4,8},
	                     {4,12,36},
						 {8,36,112}};
	int main_(){
		printf("%d\n",ANS[n-1][m-1]);
		return 0;
	}
}
namespace P30{
	int main_(){
		if(n==2){
			int ans=(ll)qikpow(3,m-1)*4%MOD;
			printf("%d\n",ans);
		}
		else {
			int ans=qikpow(2,m);
			printf("%d\n",ans);
		}
		return 0;
	}
}
bool cur2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	File(game);
	scanf("%d%d",&n,&m);
	if(n>m)std::swap(n,m);
	if(n<=3&&m<=3)return P20::main_();
	if(n<=2)return P30::main_();
	return 0;
}
