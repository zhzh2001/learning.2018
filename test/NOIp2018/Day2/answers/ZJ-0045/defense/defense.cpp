#include <bits/stdc++.h>
#define rep(i,a,b) for(int i(a),i##_END_(b);i<=i##_END_;i++)
#define drep(i,a,b) for(int i(a),i##_END_(b);i>=i##_END_;i--)
#define erep(k,G,o) for(int k=G.HEAD[o];k;k=G.NXT[k])
#define File(_) freopen(#_".in","r",stdin),freopen(#_".out","w",stdout)
template<class T> inline bool tomax(T &a,T b){return a<b?a=b,1:0;}
template<class T> inline bool tomin(T &a,T b){return a>b?a=b,1:0;}
template<int N,int M,class T> struct Link{
	int HEAD[N],NXT[M],tot;T W[M];
	void add(int x,T w){NXT[++tot]=HEAD[x];W[HEAD[x]=tot]=w;}
	T& operator [] (int x){return W[x];}
};
typedef long long ll;
const int MN=1e5+5;
bool cur1;
Link<MN,MN<<1,int> G;
int n,m;
char type[5];
int p[MN];
namespace P44{
	ll dp[MN][2];
	ll dfs(int o,int f,int b){
		if(~dp[o][b])return dp[o][b];
		dp[o][b]=(b?p[o]:0);
		erep(k,G,o){
			int v=G[k];
			if(v==f)continue;
			if(b)dp[o][b]+=std::min(dfs(v,o,0),dfs(v,o,1));
			else dp[o][b]+=dfs(v,o,1);
		}
		return dp[o][b];
	}
	int main_(){
		rep(i,1,m){
			int a,b,x,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			ll ans;
			if(x==0&&y==0)
				erep(k,G,a)
					if(G[k]==b){
						puts("-1");
						goto end;
					}
			memset(dp,-1,sizeof dp);
			dp[a][x^1]=dp[b][y^1]=4e18;
			ans=std::min(dfs(1,0,1),dfs(1,0,0));
			printf("%lld\n",ans);
		  end:;
		}
		return 0;
	}
}
namespace PA1{
	ll dp[2][MN][2];
	void init(){
		dp[0][1][0]=4e18;
		dp[0][1][1]=p[1];
		rep(i,2,n){
			dp[0][i][0]=dp[0][i-1][1];
			dp[0][i][1]=std::min(dp[0][i-1][0],dp[0][i-1][1])+p[i];
		}
		drep(i,n,2){
			dp[1][i][0]=dp[1][i+1][1];
			dp[1][i][1]=std::min(dp[1][i+1][0],dp[1][i+1][1])+p[i];
		}
		dp[1][1][0]=4e18;
		dp[1][1][1]=std::min(dp[1][2][0],dp[1][2][1])+p[1];
	}
	int main_(){
		init();
		rep(i,1,m){
			int a,x,b,y;
			ll ans=4e18;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(y==0)
				ans=dp[0][b-1][1]+dp[1][b+1][1];
			else {
				rep(i,0,1)
					rep(j,0,1)
						tomin(ans,dp[0][b-1][i]+dp[1][b+1][j]+p[b]);
			}
			printf("%lld\n",ans);
		}
		return 0;
	}
}
namespace PA2{
	ll dp[2][MN][2];
	void init(){
		rep(i,1,n){
			dp[0][i][0]=dp[0][i-1][1];
			dp[0][i][1]=std::min(dp[0][i-1][0],dp[0][i-1][1])+p[i];
		}
		drep(i,n,1){
			dp[1][i][0]=dp[1][i+1][1];
			dp[1][i][1]=std::min(dp[1][i+1][0],dp[1][i+1][1])+p[i];
		}
	}
	int main_(){
		init();
		rep(i,1,m){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(a>b)std::swap(a,b),std::swap(x,y);
			ll ans=4e18;
			if(x==0&&y==0)puts("-1");
			else {
				drep(i,1,x^1)
					drep(j,1,y^1)
						tomin(ans,dp[0][a-1][i]+dp[1][b+1][j]+(x?p[a]:0)+(y?p[b]:0));
				printf("%lld\n",ans);
			}
		}
		return 0;
	}
}
bool cur2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	File(defense);
	scanf("%d%d%s",&n,&m,type);
	rep(i,1,n)scanf("%d",p+i);
	rep(i,2,n){
		int a,b;
		scanf("%d%d",&a,&b);
		G.add(a,b);G.add(b,a);
	}
	if(strcmp(type,"A1")==0)return PA1::main_();
	if(strcmp(type,"A2")==0)return PA2::main_();
	return P44::main_();
}
