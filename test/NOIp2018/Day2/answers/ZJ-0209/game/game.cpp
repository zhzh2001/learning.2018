#include<cstdio>
#include<algorithm>
#define LL long long
using namespace std;
const int maxn=1000005,tt=1e9+7;
int n,m,ans;
LL qsm(LL w,int b){LL num=1;while(b){if (b&1) num=num*w%tt;w=w*w%tt;b>>=1;} return num;}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	if (n==1){
		printf("%lld\n",qsm(2,m));
		return 0;
	}
	if (n==2){
		printf("%lld\n",qsm(3,m-n)*12%tt);
		return 0;
	}
	if (n==3){
		printf("%lld\n",qsm(4,m-n)*112%tt);
		return 0;
	}
	if (n==5){
		printf("%lld\n",qsm(6,m-n)*7136%tt);
		return 0;
	}
	return 0;
}
