#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=100005,INF=1e9;
int tot,lnk[maxn],son[2*maxn],nxt[2*maxn];
int f[maxn][3],n,m,c[maxn],ans,p[maxn];
inline int _read(){
	int num=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') num=num*10+ch-48,ch=getchar();
	return num;
}
void add(int x,int y){nxt[++tot]=lnk[x];lnk[x]=tot;son[tot]=y;}
void DFS(int x,int fa){
	int s1=0,s2=0,s3=INF,pd=0;
	for (int j=lnk[x];j;j=nxt[j]) if (son[j]!=fa){
		DFS(son[j],x);
		s1+=min(f[son[j]][0],min(f[son[j]][1],f[son[j]][2]));
		if (f[son[j]][2]<=f[son[j]][1]){
			s2+=f[son[j]][2];pd=1;
		}else s2+=f[son[j]][1];
		s3=min(f[son[j]][2]-f[son[j]][1],s3);
	}
	f[x][2]=s1+p[x];f[x][0]=s2;
	if (pd) f[x][1]=s2;else f[x][1]=s2+s3;
	if (c[x]==0) f[x][2]=INF; 
	if (c[x]==1) f[x][0]=INF,f[x][1]=INF;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=_read();m=_read();_read();
	for (int i=1;i<=n;i++) p[i]=_read();
	for (int i=1;i<n;i++){
		int x=_read(),y=_read();
		add(x,y);add(y,x);
	}
	memset(c,255,sizeof(c));
	while(m--){
		int x=_read();c[x]=_read();
		int y=_read();c[y]=_read();
		DFS(1,0);c[x]=c[y]=-1;ans=min(f[1][1],f[1][2]);
		if (ans>=INF) printf("-1\n");else printf("%d\n",ans);
	}
	return 0;
}
