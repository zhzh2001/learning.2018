#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=5005;
int tot=-1,lnk[maxn],son[2*maxn],nxt[2*maxn];
int n,m,ans[maxn][maxn],vis[maxn],tim,pd,f[maxn],h[maxn],now,zg;
bool dis[maxn][maxn],c[maxn],z[maxn];
void add(int x,int y){nxt[++tot]=lnk[x];lnk[x]=tot;son[tot]=y;}
void DFS(int x){
	vis[x]=tim;ans[tim][++ans[tim][0]]=x;
	for (int j=lnk[x];j!=-1;j=nxt[j]) if (vis[son[j]]!=tim&&(j/2)!=pd) DFS(son[j]);
}
void DFS(int x,int fa){
	c[x]=1;
	for (int j=lnk[x];j!=-1;j=nxt[j]) if (son[j]!=fa){
		if (c[son[j]]){
			if (zg) continue;
			z[j/2]=1;int y=x;zg=1;
			while(y!=son[j]) z[h[y]]=1,y=f[y];
		}else{
			f[son[j]]=x;h[son[j]]=j/2;
			DFS(son[j],x);
		}
	}
}
int work(int x,int y){
	for (int i=1;i<=n;i++){
		if (ans[x][i]<ans[y][i]) return x;
		if (ans[y][i]<ans[x][i]) return y;
	}
	return x;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(lnk,255,sizeof(lnk));
	scanf("%d%d",&n,&m);
	for (int i=1,x,y;i<=m;i++){
		scanf("%d%d",&x,&y);
		if (x>y) swap(x,y);dis[x][y]=1;
	}
	for (int i=n;i>=1;i--)
	for (int j=n;j>=1;j--) if (dis[i][j]) add(i,j),add(j,i);
	if (m==n-1){
		++tim;pd=-1;DFS(1);
		for (int i=1;i<=n;i++) printf("%d ",ans[1][i]);
		return 0;
	}
	DFS(1,0);
	for (int i=0;i<n;i++) if (z[i]) ++tim,pd=i,DFS(1);
	now=1;for (int i=2;i<=tim;i++) now=work(now,i);
	for (int i=1;i<=n;i++) printf("%d ",ans[now][i]);
	return 0;
}
