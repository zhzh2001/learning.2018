#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define pb push_back
#define fi first
#define sd second
using namespace std;
ll n,m,ans;
ll a[10][10];
int lzq;
pair<ll,ll>b[1001000];
ll mo;
void dfs1(int x,int y,ll sum1,ll sum2)
{
	if(x==n&&y==m)
	{
		lzq++;
		b[lzq].fi=sum1;
		b[lzq].sd=sum2;
//		cout<<sum1<<" "<<sum2<<endl;
		return;
	}
	if(x<n)dfs1(x+1,y,sum1*2,sum2*2+a[x+1][y]);
	if(y<m)dfs1(x,y+1,sum1*2+1,sum2*2+a[x][y+1]);
}
bool pd()
{
	lzq=0;
	dfs1(1,1,(ll)0,a[1][1]);
	sort(b+1,b+lzq+1);
	For(i,1,lzq-1)
	{
		if(b[i].sd<b[i+1].sd)return false;
	}
	return true;
}
void dfs(int x,int y)
{
	if(y==m+1)
	{
		dfs(x+1,1);
		return;
	}
	if(x==n+1)
	{
		if(pd())
		{
			ans++;
/*			For(i,1,n)
			{
				For(j,1,m)cout<<a[i][j];
				cout<<endl;
			}
			cout<<endl;*/
		}
		return;
	}
	a[x][y]=1;
	dfs(x,y+1);
	if(x>1&&y<m)if(a[x-1][y+1]==1)
	{
		return;
	}
	a[x][y]=0;
	dfs(x,y+1);
	return;
}
ll ksm(ll x,ll y)
{
	ll z=1;
	while(y>0)
	{
		if(y%2==1)
		{
			z*=x;
			z%=mo;
		}
		x=x*x%mo;
		y/=2;
//		cout<<y<<endl;
	}
	return z;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	mo=1e9+7;
	if(n==2)
	{
		ans=4*ksm(3,m-1)%mo;
		ans%=mo;
		cout<<ans<<endl;
		return 0;
	}
	if(n==3&&m>3)
	{
		ans=112*ksm(3,m-3)%mo;
		ans%=mo;
		cout<<ans<<endl;
		return 0;
	}
	ans=0;
	dfs(1,1);
	cout<<ans<<endl;
	return 0;
}
