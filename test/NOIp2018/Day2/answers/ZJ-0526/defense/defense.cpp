#include<bits/stdc++.h>
#define rint register int
#define rep(i,a,b) for (rint i=(a),_E=(b); i<=_E; ++i)
#define per(i,a,b) for (rint i=(a),_E=(b); i>=_E; --i)
#define REP(i,n) for (rint i=(0),_E=(n); i<_E; ++i)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
using namespace std;
typedef pair<int,int> pii;
typedef vector<int> vi;
typedef long long ll;

template <class T> inline void read(T &x) {
	char ch = getchar(); int op = 0; x = 0;
	while (!isdigit(ch)) op |= (ch == '-'), ch = getchar();
	while (isdigit(ch)) x = x * 10 + ch - '0', ch = getchar();
	if (op) x = -x;
}

const int N = 100005;
const ll inf = 1e18;
ll f[N], g[N], F[N], G[N], p[N];
int father[N], n, m, tmp, a, b, x, y;
vi e[N];

//f,P have
//g,Q not have
void dfs1(int u, int fa) {
	ll P = 0, Q = 0;
	father[u] = fa;
	int sz = e[u].size(), v;
	REP (i, sz) {
		v = e[u][i];
		if (v == fa) continue;
		dfs1(v, u);
		Q += min(f[v], g[v]);
		P += f[v];
	}
	f[u] = Q + p[u];
	g[u] = P;
}

//f,P have
//g,Q not have
void dfs2(int u, int fa, ll PP, ll QQ) {
	F[u] = PP; G[u] = QQ;
	ll P = PP + g[u], Q = QQ + f[u];
	int sz = e[u].size(), v;
	REP (i, sz) {
		v = e[u][i];
		if (v == fa) continue;
		dfs2(v, u, Q - min(f[v], g[v]), P - f[v]);
	}
}

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	read(n), read(m), read(tmp);
	rep (i, 1, n) read(p[i]);
	rep (i, 1, n - 1) {
		read(a), read(b);
		e[a].pb(b), e[b].pb(a);
	}
	dfs1(1, 0);
	dfs2(1, 0, 0, 0);
	while (m--) {
		read(a), read(x);
		read(b), read(y);
		if (!x && !y) {
			puts("-1");
			continue;
		}
		ll ans = 0;
		if (father[b] == a) {
			swap(a, b);
			swap(x, y);
		}
		if (x) ans += f[a];
		else ans += g[a];
		if (y) ans += F[a];
		else ans += G[a];
		cout << ans << endl;
	}
	return 0;
}
