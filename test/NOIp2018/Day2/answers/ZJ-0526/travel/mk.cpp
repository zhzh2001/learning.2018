#include<bits/stdc++.h>
#define rint register int
#define rep(i,a,b) for (rint i=(a),_E=(b); i<=_E; ++i)
#define per(i,a,b) for (rint i=(a),_E=(b); i>=_E; --i)
#define REP(i,n) for (rint i=(0),_E=(n); i<_E; ++i)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
using namespace std;
typedef pair<int,int> pii;
typedef vector<int> vi;
typedef long long ll;

inline int R(int x) {
	return rand() % x + 1;
}

int main() {
	srand(time(NULL) ^ (long int) (new char));
	freopen("travel.in", "w", stdout);

	int n = 5000;
	if (R(2) == 0) {
		printf("%d %d\n", n, n - 1);
		rep (i, 2, n)
			printf("%d %d\n", i, max(1, i - R(R(n))));
	}
	else {
		set<pii> s; int tmp;
		printf("%d %d\n", n, n);
		rep (i, 2, n) {
			printf("%d %d\n", i, tmp = max(1, i - R(3)));
			s.insert(mp(i, tmp));
			s.insert(mp(tmp, i));
		}
		int a, b;
		do {
			a = R(n);
			b = R(n);
		} while (a == b || s.find(mp(a, b)) != s.end());
		printf("%d %d\n", a, b);
	}

	return 0;
}
