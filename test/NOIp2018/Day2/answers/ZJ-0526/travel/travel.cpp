#include<bits/stdc++.h>
#define rint register int
#define rep(i,a,b) for (rint i=(a),_E=(b); i<=_E; ++i)
#define per(i,a,b) for (rint i=(a),_E=(b); i>=_E; --i)
#define REP(i,n) for (rint i=(0),_E=(n); i<_E; ++i)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
using namespace std;
typedef pair<int,int> pii;
typedef vector<int> vi;
typedef long long ll;

template <class T> inline void read(T &x) {
	char ch = getchar(); int op = 0; x = 0;
	while (!isdigit(ch)) op |= (ch == '-'), ch = getchar();
	while (isdigit(ch)) x = x * 10 + ch - '0', ch = getchar();
	if (op) x = -x;
}

const int N = 5005;

namespace Task1 {
	vi e[N], ans;
	int n, a, b;
	
	void dfs(int u, int fa) {
		ans.pb(u);
		sort(e[u].begin(), e[u].end());
		REP (i, e[u].size())
			if (e[u][i] != fa) dfs(e[u][i], u);
	}
	
	void solve(int _n) {
		n = _n;
		rep (i, 1, n - 1) {
			read(a), read(b);
			e[a].pb(b), e[b].pb(a);
		}
		dfs(1, 0);
		REP (i, n)
			printf("%d ", ans[i]);
	}
}

namespace Task2 {
	struct edge {
		int v, link;
	} e[N << 1];
	int head[N], best[N], now[N], ring[N];
	int n, tot, cr, cur, a, b, mark_a, mark_b;
	vi g[N];
	
	inline void addEdge(int a, int b) {
		e[++tot] = (edge) {b, head[a]};
		head[a] = tot;
	}
	
	int ins[N];
	int get(int u, int fa) {
		int tmp;
		ins[u] = 1;
		REP (i, g[u].size())
			if (g[u][i] != fa) {
				if (ins[g[u][i]]) {
					ring[++cr] = u;
					return g[u][i];
				}
				tmp = get(g[u][i], u);
				if (tmp) {
					ring[++cr] = u;
					if (tmp != u) return tmp;
					return 0;
				}
			}
		ins[u] = 0;
		return 0;
	}
	
	#define loop(k,a) for (rint k=head[a]; k; k=e[k].link)
	void dfs(int u, int fa, int now[N]) {
		// printf("%d %d\n", u, fa);
		now[++cur] = u;
		loop (k, u)
			if (e[k].v != fa 
				&& !(u == mark_a && e[k].v == mark_b) 
				&& !(u == mark_b && e[k].v == mark_a)) {
					dfs(e[k].v, u, now);
			}
	}
	
	void upd(int a[N], int b[N]) {
		int flag = 0;
		rep (i, 1, n)
			if (a[i] != b[i]) {
				if (b[i] < a[i]) { flag = 1; break; }
				else return;
			}
		if (!flag) return;
		rep (i, 1, n)
			a[i] = b[i];
	}
	
	void solve(int _n) {
		n = _n;
		rep (i, 1, n) {
			read(a), read(b);
			g[a].pb(b), g[b].pb(a);
		}
		rep (i, 1, n) {
			sort(g[i].begin(), g[i].end(), greater<int>());
			REP (j, g[i].size())
				addEdge(i, g[i][j]);
		}
		get(1, 0);
		mark_a = ring[1], mark_b = ring[cr];
		dfs(1, 0, best);
		rep (i, 1, cr - 1) {
			mark_a = ring[i], mark_b = ring[i + 1];
			cur = 0;
			dfs(1, 0, now);
			upd(best, now);
		}
		rep (i, 1, n) printf("%d ", best[i]);
	}
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);

	int n, m;
	read(n), read(m);
	if (m == n - 1) Task1::solve(n);
	else Task2::solve(n);

	return 0;
}
