#include<bits/stdc++.h>
#define rint register int
#define rep(i,a,b) for (rint i=(a),_E=(b); i<=_E; ++i)
#define per(i,a,b) for (rint i=(a),_E=(b); i>=_E; --i)
#define REP(i,n) for (rint i=(0),_E=(n); i<_E; ++i)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
using namespace std;
typedef pair<int,int> pii;
typedef vector<int> vi;
typedef long long ll;

template <class T> inline void read(T &x) {
	char ch = getchar(); int op = 0; x = 0;
	while (!isdigit(ch)) op |= (ch == '-'), ch = getchar();
	while (isdigit(ch)) x = x * 10 + ch - '0', ch = getchar();
	if (op) x = -x;
}

const int N = 20;
int a[N][N], now[N], last[N];
int n, m, ans;

bool doit(int x, int y, int k) {
	if (x == n && y == m) {
		rep (i, 1, k - 1)
			if (now[i] > last[i]) break;
			else if (now[i] < last[i]) return 0;
		rep (i, 1, k - 1) last[i] = now[i];
		return 1;
	}
	now[k] = a[x][y];
	if (y < m && !doit(x, y + 1, k + 1)) return 0;
	if (x < n && !doit(x + 1, y, k + 1)) return 0;
	return 1;
}

void judge() {
	memset(last, 0, sizeof last);
	if (doit(1, 1, 1)) {
	/*
		if (rand() % 30 == 5) {
			rep (i, 1, n) {
				rep (j, 1, m)
					printf("%d", a[i][j]);
				puts("");
			}
			puts("");
		}
	*/
		++ ans;
	}
}

void dfs(int x, int y) {
	if (x == n + 1 && y == 1) {
		judge();
		return;
	}
	if (a[x-1][y+1] != 1) {
		if (y == m) dfs(x + 1, 1);
		else dfs(x, y + 1);
	}
	a[x][y] = 1;
	if (y == m) dfs(x + 1, 1);
	else dfs(x, y + 1);
	a[x][y] = 0;
}

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	read(n), read(m);
	dfs(1, 1);
	printf("%d\n", ans);
	return 0;
}
