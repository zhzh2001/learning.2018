#include<bits/stdc++.h>
#define rint register int
#define rep(i,a,b) for (rint i=(a),_E=(b); i<=_E; ++i)
#define per(i,a,b) for (rint i=(a),_E=(b); i>=_E; --i)
#define REP(i,n) for (rint i=(0),_E=(n); i<_E; ++i)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
using namespace std;
typedef pair<int,int> pii;
typedef vector<int> vi;
typedef long long ll;

template <class T> inline void read(T &x) {
	char ch = getchar(); int op = 0; x = 0;
	while (!isdigit(ch)) op |= (ch == '-'), ch = getchar();
	while (isdigit(ch)) x = x * 10 + ch - '0', ch = getchar();
	if (op) x = -x;
}

const int N = 256;
const int mod = 1000000007;
int A[N][N], B[N][N], bin[N][N], res[N][N], tmp[N][N];
int n, m, ALL, base, sz;

inline void mul(int a[N][N], int b[N][N], int c[N][N]) {
	REP (i, sz) REP (j, sz) {
		tmp[i][j] = 0;
		REP (k, sz) 
			tmp[i][j] = (tmp[i][j] + 1ll * a[i][k] * b[k][j]) % mod;
	}
	REP (i, sz) REP (j, sz) c[i][j] = tmp[i][j];
}

inline void power(int a[N][N], int p) {
	REP (i, sz) REP (j, sz)
		bin[i][j] = a[i][j], res[i][j] = (i == j);
	while (p) {
		if (p & 1) mul(res, bin, res);
		mul(bin, bin, bin); p >>= 1;
	}
	REP (i, sz) REP (j, sz)
		a[i][j] = res[i][j];
}

void doit() {
	sz = 1 << n;
	base = 1 << (n - 1);
	ALL = base - 1;
	//trans
	//0 to 0
	rep (s, 0, ALL) {
		rep (t, 0, ALL) {
			if (!(t & 1)) continue;
			int flag = 1;
			rep (i, 1, n - 2)
				if ((t >> i & 1) < (s >> (i - 1) & 1)) {
					flag = 0;
				}
			//printf("! %d %d\n", s, t);
			if (flag) B[t][s] ++;
		}
	}
	//1 to 0
	rep (s, 0, ALL) {
		rep (t, 0, ALL) {
			int flag = 1;
			rep (i, 1, n - 2)
				if ((t >> i & 1) < (s >> (i - 1) & 1)) {
					flag = 0;
				}
			//printf("# %d %d\n", s, t);
			if (flag) B[t][s + base] ++;
		}
	}
	//1 to 1
	rep (s, 0, ALL) {
		int t = (s << 1) & ALL;
		B[t + base][s + base] ++;
		B[t + 1 + base][s + base] ++;
	}
	//start
	rep (s, 0, ALL) {
		A[s][0] = 1;
		A[s + base][0] = 1;
	}
}

int main() {
//	freopen("game.in", "r", stdin);
//	freopen("game.out", "w", stdout);

	read(n), read(m);
	doit();
	power(B, m - 1);
	mul(B, A, A);
	REP (i, sz) {
		REP (j, sz)
			printf("%d ", B[i][j]);
		puts("");
	}
	int ans = 0;
	rep (i, 0, ALL)
		ans = (ans + A[i][0]) % mod;
	ans = ans * 2 % mod;
	printf("%d\n", ans);

	return 0;
}
