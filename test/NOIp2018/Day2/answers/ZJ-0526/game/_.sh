#!bin/bash
g++ mk.cpp -o mk -Ofast
g++ std.cpp -o std -Ofast
g++ xxx.cpp -o xxx
while true; do
	./mk
	./std
	./xxx
	if diff -b std.out xxx.out; then
		echo AC
	else
		echo WA
		exit 0
	fi
done
