#include<bits/stdc++.h>
#define FOR(i,x,y) for(int i=(x),i##END=(y);i<=i##END;++i)
#define DOR(i,x,y) for(int i=(x),i##END=(y);i>=i##END;--i)
typedef long long LL;
using namespace std;
template<const int maxn,const int maxm>struct Linked_list
{
	int head[maxn],to[maxm],nxt[maxm],tot;
	Linked_list(){clear();}
	void clear(){memset(head,-1,sizeof(head));tot=0;}
	void add(int u,int v){to[++tot]=v,nxt[tot]=head[u],head[u]=tot;}
	#define EOR(i,G,u) for(int i=G.head[u];~i;i=G.nxt[i])
};
const int N=5005;
vector<int>E[N];
Linked_list<N,N<<1>G;
int n,m,cnt,ans[N],cur[N];
bool vis[N];
int stk[N],tp;
int U[N],tot;

void dfs1(int u,int f)
{
	printf("%d",u);
	cnt++;
	if(cnt==n)puts("");
	else printf(" ");
	EOR(i,G,u)
	{
		int v=G.to[i];
		if(v==f)continue;
		dfs1(v,u);
	}
}
bool dfs2(int u,int f)
{
	if(vis[u])
	{
		U[++tot]=u;
		while(stk[tp]!=u)
		{
			U[++tot]=stk[tp];
			tp--;
		}
		return true;
	}
	vis[u]=1;
	stk[++tp]=u;
	EOR(i,G,u)
	{
		int v=G.to[i];
		if(v==f)continue;
		if(dfs2(v,u))return true;
	}
	tp--;
	return false;
}
void dfs3(int u,int f,int fu,int fv)
{
	cur[++cnt]=u;
	EOR(i,G,u)
	{
		int v=G.to[i];
		if(v==f||(u==fu&&v==fv)||(u==fv&&v==fu))continue;
		dfs3(v,u,fu,fv);
	}
}
void chk_ans()
{
	FOR(i,1,n)if(cur[i]!=ans[i])
	{
		if(cur[i]<ans[i])
		{
			FOR(j,1,n)ans[j]=cur[j];
			return;
		}
		else return;
	}
}

int main()//file LL memory
{
//	printf("%.2lf\n",(&mmr2-&mmr1)/1024.0/1024.0);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	FOR(i,1,n)E[i].clear();
	FOR(i,1,m)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		E[u].push_back(v);
		E[v].push_back(u);
	}
	FOR(i,1,n)sort(E[i].begin(),E[i].end());
	FOR(u,1,n)
		DOR(i,(int)E[u].size()-1,0)
		{
			int v=E[u][i];
			G.add(u,v);
		}
	if(m==n-1)
	{
		dfs1(1,0);
		return 0;
	}
	dfs2(1,0);
	ans[1]=n+1;
	FOR(i,1,tot-1)
	{
		cnt=0;
		dfs3(1,0,U[i],U[i+1]);
		chk_ans();
	}
	FOR(i,1,n-1)printf("%d ",ans[i]);
	printf("%d\n",ans[n]);
	return 0;
}
