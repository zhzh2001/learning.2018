#include<bits/stdc++.h>
#define FOR(i,x,y) for(int i=(x),i##END=(y);i<=i##END;++i)
#define DOR(i,x,y) for(int i=(x),i##END=(y);i>=i##END;--i)
typedef long long LL;
using namespace std;
const int N=1e5+5;
const int Ns=2005;
template<const int maxn,const int maxm>struct Linked_list
{
	int head[maxn],to[maxm],nxt[maxm],tot;
	Linked_list(){clear();}
	void clear(){memset(head,-1,sizeof(head));tot=0;}
	void add(int u,int v){to[++tot]=v,nxt[tot]=head[u],head[u]=tot;}
	#define EOR(i,G,u) for(int i=G.head[u];~i;i=G.nxt[i])
};
Linked_list<N,N<<1>G;
int n,m;char str[10];
int P[N],tot;
int pw[N];
LL dp[N][2],f[N][2];
bool mark[Ns];

bool Draw(int u,int f,int To,int *P,int &len)
{
	P[++len]=u;
	if(u==To)return true;
	EOR(i,G,u)
	{
		int v=G.to[i];
		if(v==f)continue;
		if(Draw(v,u,To,P,len))return true;
		len--;
	}
	return false;
}
void dfs(int u,int f)
{
	dp[u][0]=0,dp[u][1]=pw[u];
	EOR(i,G,u)
	{
		int v=G.to[i];
		if(v==f||mark[v])continue;
		dfs(v,u);
		dp[u][0]+=dp[v][1];
		dp[u][1]+=min(dp[v][1],dp[v][0]);
	}
}
struct node
{
	int L,R;LL sum[2][2];
	node operator +(const node &_)const
	{
		node res;
		res.L=L,res.R=_.R;
		FOR(i,0,1)FOR(j,0,1)
		{
			res.sum[i][j]=sum[i][1]+_.sum[1][j];
			if(res.sum[i][j]>sum[i][0]+_.sum[1][j])res.sum[i][j]=sum[i][0]+_.sum[1][j];
			if(res.sum[i][j]>sum[i][1]+_.sum[0][j])res.sum[i][j]=sum[i][1]+_.sum[0][j];
		}
		return res;
	}
	void tag_up(int x)
	{
		sum[1][0]=1e13;
		sum[0][1]=1e13;
		sum[0][0]=0;
		sum[1][1]=x;
	}
};
struct SegmentTree
{
	node nd[N<<2];
	void build(int k,int L,int R,int *arr)
	{
		if(L==R)
		{
			nd[k].L=nd[k].R=L;
			nd[k].tag_up(arr[L]);
			return;
		}
		build(k<<1,L,(L+R)>>1,arr);
		build(k<<1|1,((L+R)>>1)+1,R,arr);
		nd[k]=nd[k<<1]+nd[k<<1|1];
	}
	node query(int k,int L,int R)
	{
		if(L<=nd[k].L&&nd[k].R<=R)return nd[k];
		int mid=(nd[k].L+nd[k].R)>>1;
		if(R<=mid)return query(k<<1,L,R);
		else if(L>mid)return query(k<<1|1,L,R);
		else return query(k<<1,L,R)+query(k<<1|1,L,R);
	}
}ST;

int main()//file LL memory
{
//	printf("%.2lf\n",(&mmr2-&mmr1)/1024.0/1024.0);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,str);
	FOR(i,1,n)scanf("%d",&pw[i]);
	FOR(i,1,n-1)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		G.add(u,v),G.add(v,u);
	}
	if(n<=2000&&m<=2000)
	{
		while(m--)
		{
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			tot=0;Draw(a,0,b,P,tot);
			if(tot==2&&x==0&&y==0)
			{
				printf("-1\n");
				continue;
			}
			memset(mark,0,sizeof(mark));
			FOR(i,1,tot)mark[P[i]]=1;
			FOR(i,1,tot)dfs(P[i],0);
			f[1][x]=dp[P[1]][x];
			f[1][1-x]=1e13;
			FOR(i,2,tot-1)
			{
				f[i][1]=dp[P[i]][1]+min(f[i-1][0],f[i-1][1]);
				f[i][0]=dp[P[i]][0]+f[i-1][1];
			}
			if(y)printf("%lld\n",dp[P[tot]][1]+min(f[tot-1][0],f[tot-1][1]));
			else printf("%lld\n",dp[P[tot]][0]+f[tot-1][1]);
		}
		return 0;
	}
	ST.build(1,1,n,pw);
	while(m--)
	{
		int a,x,b,y;LL ans=0;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if(a>b)swap(a,b),swap(x,y);
		if(a+1==b&&!x&&!y)
		{
			printf("-1\n");
			continue;
		}
		if(a!=1)
		{
			node res=ST.query(1,1,a-1);
			if(x==1)ans+=min(min(res.sum[0][0],res.sum[0][1]),min(res.sum[1][0],res.sum[1][1]));
			else ans+=min(res.sum[0][1],res.sum[1][1]);
		}
		if(b!=n)
		{
			node res=ST.query(1,b+1,n);
			if(y==1)ans+=min(min(res.sum[0][0],res.sum[0][1]),min(res.sum[1][0],res.sum[1][1]));
			else ans+=min(res.sum[1][0],res.sum[1][1]);
		}
		ans+=ST.query(1,a,b).sum[x][y];
		printf("%lld\n",ans);
	}
	return 0;
}
