#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
const ll MAXN=1e5+2333;
const ll INF=0x3f3f3f3f;

ll n,m;
ll val[MAXN],opt[MAXN];
ll f[MAXN][2];
char Type[10];
vector<ll> G[MAXN];

ll read() {
	ll x=0,f=1; char ch=getchar();
	while (!isdigit(ch)) { if (ch=='-') f=-1; ch=getchar(); }
	while (isdigit(ch)) x=x*10+ch-'0',ch=getchar(); return x*f;
}

void DFS(ll x,ll p) {
	for (vector<ll>::iterator it=G[x].begin();it!=G[x].end();it++) {
		if (*it!=p) {
			DFS(*it,x);
			f[x][0]+=f[*it][1];
			f[x][1]+=min(f[*it][0],f[*it][1]);
		}
	}
	f[x][1]+=val[x];
	if (opt[x]!=-1)
		f[x][1-opt[x]]=INF;
}

int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%lld%lld%s",&n,&m,Type+1);
	for (ll i=1;i<=n;i++)
		val[i]=read();
	for (ll i=1,u,v;i<n;i++) {
		u=read(),v=read();
		G[u].push_back(v);
		G[v].push_back(u);
	}
	memset(opt,-1,sizeof(opt));
	ll a,b,x,y,tmp;
	for (ll i=1;i<=m;i++) {
		a=read(),x=read(),b=read(),y=read();
		opt[a]=x,opt[b]=y;
		memset(f,0,sizeof(f));
		DFS(1,0);
		if (opt[1]==-1) {
			tmp=min(f[1][0],f[1][1]);
			printf("%lld\n",tmp<INF ? tmp:-1);
		}
		else {
			printf("%lld\n",f[1][opt[1]]<INF ? f[1][opt[1]]:-1);
		}
		opt[a]=-1,opt[b]=-1;
	}
	return 0;
}
