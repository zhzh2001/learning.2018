#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
const ll moo=1e9+7;
const ll MAXM=1e6+233;

ll n,m;
ll TP[4][4]= { {0,0,0,0},
				{0,2,4,8},
				{0,4,12,36},
				{0,8,36,112}};

ll read() {
	ll x=0,f=1; char ch=getchar();
	while (!isdigit(ch)) { if (ch=='-') f=-1; ch=getchar(); }
	while (isdigit(ch)) x=x*10+ch-'0',ch=getchar(); return x*f;
}

void solveOne() {
	printf("%lld\n",TP[n][m]);
}

ll QPM(ll x,ll y) {
	ll tmp=1;
	while (y) {
		if (y&1) tmp=(tmp*x)%moo;
		y>>=1;
		x=(x*x)%moo;
	}
	return tmp;
}

void solveTwo() {
	printf("%lld\n",(12*QPM(3,m-2))%moo); 
}

void solveThree() {
	printf("%lld\n",(112*QPM(3,m-3))%moo);
}

void solveFour() {
	printf("%lld\n",(912*QPM(3,m-4))%moo);
}

void solveFive() {
	printf("%lld\n",(7136*QPM(3,m-5)));
}

int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if (n>m) swap(n,m);
	if (n<=3&&m<=3) solveOne();
	else if (n==1) printf("%lld\n",QPM(2,m));
	else if (n==2) solveTwo();
	else if (n==3) solveThree();
	else if (n==4) solveFour();
	else if (n==5) solveFive();
	return 0;
}
