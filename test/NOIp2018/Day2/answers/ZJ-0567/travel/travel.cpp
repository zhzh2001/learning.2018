#include<bits/stdc++.h>
using namespace std;

const int MAXN=5e3+233;

int n,m;
vector<int> G[MAXN];
int vis[MAXN],pre[MAXN],inc[MAXN];


int read() {
	int x=0,f=1; char ch=getchar();
	while (!isdigit(ch)) { if (ch=='-') f=-1; ch=getchar(); }
	while (isdigit(ch)) x=x*10+ch-'0',ch=getchar(); return x*f;
}

void DFSTree(int x,int p) {
	if (!vis[x]) vis[x]=1,printf("%d ",x);
	for (vector<int>::iterator it=G[x].begin();it!=G[x].end();it++)
		if (!vis[*it])
			DFSTree(*it,x);
}

void solveOne() {
	DFSTree(1,0);
}

int GT=0;

void findC(int x,int p) {
	vis[x]=1,pre[x]=p;
	for (vector<int>::iterator it=G[x].begin();it!=G[x].end();it++) {
		if (*it!=p) {
			if (!vis[*it])
				findC(*it,x);
			else {
				inc[x]=1;
				do {
					x=pre[x];
					inc[x]=1;	
				} while (x!=*it);
				GT=1;
				return;
			}
		}
		if (GT)
			return;
	}
}

int GG=0;

void DFSTP(int x,int p,int tp) {
	if (!vis[x]) vis[x]=1,printf("%d ",x);
	for (vector<int>::iterator it=G[x].begin();it!=G[x].end();it++) {
		if (!vis[*it]) {
			if (GG) DFSTree(*it,x);	
			else if (!inc[*it])
				DFSTree(*it,x);
			else {
				vector<int>::iterator kt=it;
				kt++;
				if (*it>tp&&kt==G[x].end()) {
					GG=1;
					break;
				}
				int tmp=0x3f3f3f3f;
				while (kt!=G[x].end()&&vis[*kt]) kt++;
				if (kt!=G[x].end())
					tmp=*kt;
				else 
					tmp=tp;
				DFSTP(*it,x,tmp);
			}
		}
	}
}

void DFSPRE(int x,int p) {
	vis[x]=1;
	printf("%d ",x);
	for (vector<int>::iterator it=G[x].begin();it!=G[x].end();it++) {
		if (!vis[*it]&&*it!=p) {
			if (!inc[*it])
				DFSPRE(*it,x);
			else {
				int v=*it,tp=0x3f3f3f3f;
				DFSTP(v,x,tp);
			}
		}
	}
}

void solveTwo() {
	findC(1,0);
//	for (int i=1;i<=n;i++)
//		if (inc[i])
//			cout << i << " ";
//	cout << "\n\n\n";
	memset(vis,0,sizeof(vis));
	DFSPRE(1,0);
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for (int i=1,u,v;i<=m;i++) {
		u=read(),v=read();
		G[u].push_back(v);
		G[v].push_back(u);
	}
	for (int i=1;i<=n;i++)
		sort(G[i].begin(),G[i].end());
	if (m==n-1) solveOne();
	else solveTwo();
	return 0;
}
/*#include<bits/stdc++.h>
using namespace std;

const int MAXN=5e3+233;

int n,m;
vector<int> G[MAXN];
int vis[MAXN],pre[MAXN],inc[MAXN];


int read() {
	int x=0,f=1; char ch=getchar();
	while (!isdigit(ch)) { if (ch=='-') f=-1; ch=getchar(); }
	while (isdigit(ch)) x=x*10+ch-'0',ch=getchar(); return x*f;
}

void DFSTree(int x,int p) {
	if (!vis[x]) vis[x]=1,printf("%d ",x);
	for (vector<int>::iterator it=G[x].begin();it!=G[x].end();it++)
		if (!vis[*it])
			DFSTree(*it,x);
}

void solveOne() {
	DFSTree(1,0);
}

int GT=0;

void findC(int x,int p) {
	vis[x]=1,pre[x]=p;
	for (vector<int>::iterator it=G[x].begin();it!=G[x].end();it++) {
		if (*it!=p) {
			if (!vis[*it])
				findC(*it,x);
			else {
				int tmp=x;
				inc[x]=1;
				do {
					x=pre[x];
					inc[x]=1;	
				} while (x!=*it);
				GT=1;
				return;
			}
		}
		if (GT)
			return;
	}
}

int st,GG;

void DFSTP(int x,int p,int tp) {
	if (!vis[x]) vis[x]=1,printf("%d ",x);
	for (vector<int>::iterator it=G[x].begin();it!=G[x].end();it++) {
		if (!vis[*it]) {
			if (GG) DFSTree(*it,x);	
			else if (!inc[*it])
				DFSTree(*it,x);
			else if (*it<tp)
				DFSTP(*it,x,tp);
//			else if (it!=G[x].end()){
//				int tmp=0x3f3f3f3f;
//				vector<int>::iterator kt=it;
//				kt++;
//				while (vis[*kt]&&kt!=G[x].end()) kt++;
//				if (kt!=G[x].end())
//					tmp=*kt;
//				DFSTP(*it,x,*kt);
//			}
			else if (!GG) {
				GG=1;
				continue;
			}
		}
	}
}

void DFSPRE(int x,int p) {
	vis[x]=1;
	printf("%d ",x);
	for (vector<int>::iterator it=G[x].begin();it!=G[x].end();it++) {
		if (!vis[*it]&&*it!=p) {
			if (!inc[*it])
				DFSPRE(*it,x);
			else {
				int v=*it,tmp=0,tp=0x3f3f3f3f;
				for (vector<int>::iterator kt=G[v].begin();kt!=G[v].end();kt++) {
					if (!vis[*kt]&&inc[*kt]) tmp++;
					if (tmp==2) {
						tp=*kt;
						break;
					}
				}
				st=v;
				DFSTP(v,x,tp);
			}
		}
	}
}

void solveTwo() {
	findC(1,0);
//	for (int i=1;i<=n;i++)
//		if (inc[i])
//			cout << i << " ";
//	cout << "\n\n\n";
	memset(vis,0,sizeof(vis));
	DFSPRE(1,0);
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for (int i=1,u,v;i<=m;i++) {
		u=read(),v=read();
		G[u].push_back(v);
		G[v].push_back(u);
	}
	for (int i=1;i<=n;i++)
		sort(G[i].begin(),G[i].end());
	if (m==n-1) solveOne();
	else solveTwo();
	return 0;
}*/
