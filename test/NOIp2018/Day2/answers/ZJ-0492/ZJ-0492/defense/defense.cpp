#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>
#include <memory>

#include <algorithm>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <vector>
#include <list>
#include <utility>
#include <bitset>
#define go(st) for(int i=head[st],ed=E[i].ed;i;i=E[i].nxt,ed=E[i].ed)
typedef long long ll;
using namespace std;
void file() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
}
//defs====================================
const int MAXN=100010;
const ll INF=1e15;
struct edge {int ed,nxt,vv;}E[3*MAXN];
int head[MAXN],Ecnt;
void addEdge(int st,int ed) {
	E[++Ecnt].ed=ed,E[Ecnt].nxt=head[st],head[st]=Ecnt;
}
ll val[MAXN];
char tp[MAXN];
int N,M;
int AA,BB,XX,YY;
ll f[MAXN][3];
int fa[MAXN];
void init();
void dfs(int st);
//main====================================
int main() {
file();
	scanf("%d%d",&N,&M);scanf("%s",tp);
	for(int i=1;i<=N;++i) scanf("%lld",val+i);
	for(int i=1;i<N;++i) {
		int x,y;
		scanf("%d%d",&x,&y);
		addEdge(x,y),addEdge(y,x);
	}
	for(int i=1;i<=M;++i) {
		scanf("%d%d%d%d",&AA,&XX,&BB,&YY);
		init();
		dfs(1);
		ll ans=min(f[1][0],f[1][1]);
		if(ans<INF) printf("%lld\n",ans);
		else printf("-1\n");
	}
	return 0;
}

void init() {
	memset(f,0,sizeof f);
}
void dfs(int st) {
	f[st][0]=0,f[st][1]=val[st];//f[st][2]=0;
/*	if(st==AA) f[st][XX^1]=INF;
	if(st==BB) f[st][YY^1]=INF;*/
	int cnt=0;
	go(st) if(ed!=fa[st]){ 
	++cnt;
		fa[ed]=st;
		dfs(ed);
		f[st][1]+=min(f[ed][0],f[ed][1]);
		f[st][0]+=f[ed][1];
	}
	
	if(st==AA) {
		if(XX==0) f[st][1]=INF;
		else f[st][0]=INF;
	}
	if(st==BB) {
		if(YY==0) f[st][1]=INF;
		else f[st][0]=INF;
	}
//	printf("f[%d][%d]=%lld f[%d][%d]=%lld\n",st,0,f[st][0],st,1,f[st][1]);
}
