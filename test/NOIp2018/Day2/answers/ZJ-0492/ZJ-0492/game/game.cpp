#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>
#include <memory>

#include <algorithm>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <vector>
#include <list>
#include <utility>
#include <bitset>

typedef long long ll;
using namespace std;
void file() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}
//defs===========================================================
const int mod=1e9+7;
int N,M;
int a[50][500];
bool check();
int ans=0;
void dfs(int x,int y,int pos);
pair<string,string> b[101000];int cnt;
bool judge();
void solve_2();
void solve_3();
//main===========================================================

int main() {
file();
	//freopen("my.out","w",stdout);
	scanf("%d%d",&N,&M);
	if(N==2&&M>=2) {
		solve_2();
		return 0;
	} else if(N==3&&M>=3) {
		solve_3();
		return 0;
	}
	for(int S=0;S<(1<<(N*M));++S) {
		for(int i=1;i<=N;++i) for(int j=1;j<=M;++j) {
			if(S&(1<<((i-1)*M+j-1))) a[i][j]=true;
			else a[i][j]=false;
		}
		

		if(check()) {
			++ans;
		
			//printf("Yes!\n");
			//for(int i=1;i<=N;++i,puts("")) for(int j=1;j<=M;++j) printf("%d ",a[i][j]);
			
			
		} else{
	/*		if(judge()) {
				puts("Mat:");
		a[1][1]=a[N][M]=0;
		for(int i=1;i<=N;++i,puts("")) for(int j=1;j<=M;++j) printf("%d ",a[i][j]);

bool ret=false;
	for(int i=1;i<=cnt;++i)  {
		for(int j=1;j<=cnt;++j) if(i!=j) {
			if(b[i].first>b[j].first&&!(b[i].second<=b[j].second)) {
				cout<<b[i].first<<" "<<b[j].first<<"  "<<b[i].second<<" "<<b[j].second<<endl;
				ret=true;
				break;
			}
		}
		
		if(ret) break;
	}*/

		}
	}
	printf("%d\n",ans%mod);
	return 0;
}
char sq[100];
char ss[100];
void dfs(int x,int y,int pos=0) {
//printf("dfs in (%d,%d) pos=%d\n",x,y,pos);
	if(x==N&&y==M) {
		++cnt;
		for(int i=1;i<=N+M-2;++i) b[cnt].first+=sq[i],b[cnt].second+=ss[i];
		//cout<<b[cnt].first<<" "<<b[cnt].second<<endl;
		return;
	}
	if(x<N) {
		sq[pos+1]='D';
		ss[pos+1]=a[x+1][y]+'0';
		dfs(x+1,y,pos+1);
	} 
	if(y<M) {
		sq[pos+1]='R';
		ss[pos+1]=a[x][y+1]+'0';
		dfs(x,y+1,pos+1);
	}
}

bool judge() {
	return a[2][1]>=a[1][2]&&a[3][1]>=a[2][2]&&a[2][2]>=a[1][3]&&a[3][2]>=a[2][3];
}
bool check() {
memset(ss,0,sizeof ss),memset(sq,0,sizeof sq);
for(int i=1;i<=cnt;++i) b[i]=make_pair("","");
	cnt=0;
	dfs(1,1);
	sort(b+1,b+cnt+1);
	for(int i=1;i<=cnt;++i)  {
		for(int j=1;j<=cnt;++j) if(i!=j) {
			if(b[i].first>b[j].first&&!(b[i].second<=b[j].second)) {
				//cout<<b[i].first<<" "<<b[j].first<<"  "<<b[i].second<<" "<<b[j].second<<endl;
				return false;
			}
		}
	}
	return true;
}

ll qpow(ll x,ll y) {
	ll ans=1;
	for(;y;y>>=1,x=(x*x)%mod) if(y&1) ans=(ans*x)%mod;
	return ans;
}
void solve_2() {
	ll ans=qpow(3,M-1)*4%mod;
	printf("%lld\n",ans);
}
void solve_3() {
	ll ans=qpow(3,M-3)*112%mod;
	printf("%lld\n",ans);
}
