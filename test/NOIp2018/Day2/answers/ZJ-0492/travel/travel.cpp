#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>
#include <memory>

#include <algorithm>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <vector>
#include <list>
#include <utility>
#include <bitset>

typedef long long ll;
using namespace std;
void file() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
}
//defs=======================================
vector<int> E[50010];
int N,M;
bool vis[50100];
int dX,dY;

int a[50100],cnt;
int ans[50100];

void dfs_tree(int st);
bool bigger();
//main=======================================
int main() {
	file();
	scanf("%d%d",&N,&M);
	for(int i=1;i<=M;++i) {
		int x,y;
		scanf("%d%d",&x,&y);
		E[x].push_back(y),E[y].push_back(x);
	}
	for(int i=1;i<=N;++i) sort(E[i].begin(),E[i].end());
	
	if(M==N-1) {
		dfs_tree(1);
		for(int i=1;i<=N;++i) ans[i]=a[i];
	} else {
		for(int i=1;i<=N;++i) {
			for(int j=0;j<E[i].size();++j) {
				dX=i,dY=E[i][j];
				memset(vis,0,sizeof vis);
				cnt=0;
				dfs_tree(1);
				
				if(cnt==N) {
					if(bigger()) {
						for(int i=1;i<=N;++i) ans[i]=a[i];
					}
				}
			}
		}
	}
	for(int i=1;i<N;++i) printf("%d ",ans[i]);
	printf("%d\n",ans[N]);
	return 0;
}

bool bigger() {
	if(ans[1]==0) return true;
	for(int i=1;i<=N;++i) {
		if(ans[i]==0||a[i]<ans[i]) return true;
		else if(a[i]>ans[i]) return false;
	}
	return false;
}


void dfs_tree(int st) {
	vis[st]=true;
	a[++cnt]=st;
	for(int i=0;i<E[st].size();++i) {
		int ed=E[st][i];
		if( (st==dX&&ed==dY) || (st==dY&&ed==dX) ) continue;
		if(!vis[ed]) {
			dfs_tree(ed);
		}
	}
}
