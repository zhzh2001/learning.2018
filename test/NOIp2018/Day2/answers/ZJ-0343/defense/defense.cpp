#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define nc() getchar()
inline int read(){
	int x=0;char ch=nc();for(;ch<'0'||ch>'9';ch=nc());
	for(;ch<='9'&&ch>='0';x=x*10+ch-48,ch=nc());return x;
}
char ch[3];
#define N 100010
int i,j,k,m,n,x,y,f[N][2],last[N],cnt,w[N],x1,y1;
struct edge{int to,next;}e[N<<1];
inline void add(int u,int v){
	e[++cnt]=(edge){v,last[u]},last[u]=cnt;
	e[++cnt]=(edge){u,last[v]},last[v]=cnt;
}
#define min(a,b) ((a)<(b)?(a):(b))
inline bool check(int o,int d){
	if(o==x||o==y){
		if(o==x)return d==x1;
		else return d==y1;
	}else return 1;
}
void dfs(int o,int fa){
	int flag=0;
	for(int i=last[o],t;i;i=e[i].next)if((t=e[i].to)!=fa){
		flag=1,dfs(t,o);
		if(o==x||o==y){
			if(o==x){
				if(x1==0){
					if(!(t==y&&y1==0))f[o][0]=min(f[o][0],f[t][1]);
				}
				if(x1==1){
					if(!(t==y&&y1==0))f[o][1]=min(f[o][1],w[o]+f[t][0]);
					if(!(t==y&&y1==1))f[o][1]=min(f[o][1],w[o]+f[t][1]);
				}
			}else{
				swap(x1,y1),swap(x,y);
				if(x1==0){
					if(!(t==y&&y1==0))f[o][0]=min(f[o][0],f[t][1]);
				}
				if(x1==1){
					if(!(t==y&&y1==0))f[o][1]=min(f[o][1],w[o]+f[t][0]);
					if(!(t==y&&y1==1))f[o][1]=min(f[o][1],w[o]+f[t][1]);
				}
				swap(x1,y1),swap(x,y);
			}
		}else{
			if(check(t,1))f[o][0]=min(f[o][0],f[t][1]);
			if(check(t,0))f[o][1]=min(f[o][1],w[o]+f[t][0]);
			if(check(t,1))f[o][1]=min(f[o][1],w[o]+f[t][1]);
		}
	}
	if(!flag&&f[o][1]!=1000000000)f[o][1]=w[o];
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),scanf("%s",ch+1);
	memset(f,0x7f,sizeof f);
	for(i=1;i<=n;++i)w[i]=read();//,f[i][1]=w[i];
	for(i=1;i<n;++i)add(read(),read());
	while(m--){
		x=read(),x1=read(),y=read(),y1=read();
		f[x][x1^1]=1000000000,f[y][y1^1]=1000000000;
		dfs(1,0);
		int ans=min(f[1][1],f[1][0]);
		if(ans>=1000000000)puts("-1");
		else printf("%d\n",ans);
		//printf("%d\n",min(f[1][1],f[1][0]));
		memset(f,0x7f,sizeof f);
		//for(i=1;i<=n;++i)f[i][1]=w[i];
	}	
}
