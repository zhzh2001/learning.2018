#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define nc() getchar()
inline int read(){
	int x=0;char ch=nc();for(;ch<'0'||ch>'9';ch=nc());
	for(;ch<='9'&&ch>='0';x=x*10+ch-48,ch=nc());return x;
}
#define N 10050
int i,h[N],ct,j,k,m,n,x,y,cnt,X,Y,last[N],now,ans[N],L,R,fa[N],q[N],pre[N];
struct edge{
	int to,next;
}e[N<<1];
struct node{
	int x,y;
}b[N];
inline void add(int u,int v){
	e[++cnt]=(edge){v,last[u]},last[u]=cnt;
//	e[++cnt]=(edge){u,last[v]},last[v]=cnt;
}
bool vis[N];
inline bool cmp(node a,node b){return a.x>b.x||((a.x==b.x)&&(a.y>b.y));}
void dfs(int x){
	vis[x]=1,ans[++now]=x;
	for(int i=last[x],y;i;i=e[i].next)if(!vis[y=e[i].to])dfs(y);
}
int find(int x){return x==fa[x]?x:fa[x]=find(fa[x]);}
inline void findit(int x,int y){
	L=1,q[++R]=x,vis[x]=1;
	while(L<=R){
		int o=q[L++];
		if(o==y)break;
		for(int i=last[o];i;i=e[i].next)if(!vis[e[i].to]){
			vis[e[i].to]=1,q[++R]=e[i].to,pre[e[i].to]=o;
		}
	}
	while(y)h[++ct]=y,y=pre[y];
}
inline bool checkit(int x,int y){
	if(x==X&&y==Y)return 0;
	if(x==Y&&y==X)return 0;
	return 1;
}
void dfs2(int x){
	vis[x]=1,q[++now]=x;
	for(int i=last[x],y;i;i=e[i].next)
		if(!vis[y=e[i].to])if(checkit(x,y))dfs2(y);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	if(m==n-1){
		for(i=1;i<=m;++i){
			b[i].x=read(),b[i].y=read();
			b[m+i].y=b[i].x,b[m+i].x=b[i].y;
		}
		sort(b+1,b+2*m+1,cmp);
		for(i=1;i<=2*m;++i)add(b[i].x,b[i].y);//,printf("%d %d\n",b[i].x,b[i].y)
		dfs(1);
		for(i=1;i<n;++i)printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}
	else{
		int flag=0;
		for(i=1;i<=n;++i)fa[i]=i;
		for(i=1;i<=m;++i){
			b[i].x=read(),b[i].y=read();
			b[m+i].y=b[i].x,b[m+i].x=b[i].y;
			if(!flag){
				int p=find(b[i].x),q=find(b[i].y);
				if(p!=q)fa[q]=p;
				else findit(b[i].x,b[i].y),flag=1;
			}
			add(b[i].x,b[i].y),add(b[i].y,b[i].x);
		}
		cnt=now=0;
		memset(last,0,sizeof last);
		sort(b+1,b+2*m+1,cmp);
		for(i=1;i<=n;++i)ans[i]=n-i+1;
		for(i=1;i<=2*m;++i)add(b[i].x,b[i].y);//,printf("%d %d\n",b[i].x,b[i].y)
		for(i=1;i<ct;++i){
			memset(vis,0,sizeof vis);
			memset(q,0,sizeof q);
			now=0,X=h[i],Y=h[i+1];
			dfs2(1);
			int flg=0;
			if(now==n)for(j=1;j<=n;++j)if(q[j]<ans[j]){flg=1;break;}
			else if(q[j]>ans[j])break;
			if(flg)for(j=1;j<=n;++j)ans[j]=q[j];
		}
		memset(vis,0,sizeof vis);
		memset(q,0,sizeof q);
		now=0,X=h[1],Y=h[ct];
		dfs2(1);
		int flg=0;
		if(now==n)for(j=1;j<=n;++j)if(q[j]<ans[j]){flg=1;break;}
		else if(q[j]>ans[j])break;
		if(flg)for(j=1;j<=n;++j)ans[j]=q[j];
		for(i=1;i<n;++i)printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}
}
