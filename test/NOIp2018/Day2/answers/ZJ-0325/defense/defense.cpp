#include<bits/stdc++.h>
#define N 155555
using namespace std;
int n,m,s,nxt[N*2],head[N*2],vet[N*2],num,fa[N],dd[N],id,x1,x2,x3,x4,dp[N][3];
int a[N];
bool tflag[N];
void add_edge(int u,int v){
	nxt[++num]=head[u];head[u]=num;vet[num]=v;
}
void dfs1(int u,int f){
	if (u!=x1) fa[u]=f;
	bool ff=true;
	for (int e=head[u];e;e=nxt[e]){
		int v=vet[e];
		if (v==f) continue;
		ff=false;
		dfs1(v,u);
	}
	if (ff) {dd[++id]=u;tflag[u]=true;}
}
void dfs2(int u,int f){
	bool pdd=false;int gg=0;
	if (tflag[u]) return ;
	for (int e=head[u];e;e=nxt[e]){
		int v=vet[e];if (v==f) continue;
		if (v==x3){pdd=true;gg=v;break;}
	}
	if (!pdd){
		int mini=1000000000;
		dp[u][1]=a[u];
		for (int e=head[u];e;e=nxt[e]){
			int v=vet[e];if (v==f) continue;
			dfs2(v,u);
			dp[u][1]+=min(dp[v][0],dp[v][1]);
			dp[u][0]+=dp[v][1];
		}
	}else {
		if (x4==1){dp[u][1]=a[u];
			for (int e=head[u];e;e=nxt[e]){
				int v=vet[e];if (v==f) continue;
				dfs2(v,u);
				if (v!=gg){dp[u][1]+=min(dp[v][0],dp[v][1]);
					dp[u][0]+=dp[v][1];
				}else {dp[u][1]+=dp[v][1];dp[u][0]+=dp[v][1];}
			}
		}else
		{
			dp[u][0]=1000000000;dp[u][1]=a[u];
			for (int e=head[u];e;e=nxt[e]){
				int v=vet[e];if (v==f) continue;
				dfs2(v,u);
			    if (v!=gg) dp[u][1]+=min(dp[v][0],dp[v][1]);else dp[u][1]+=dp[v][0];
			}
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char s[100];
	scanf("%d %d %s",&n,&m,&s);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<n;i++){
		int x,y;scanf("%d%d",&x,&y);
		add_edge(x,y);add_edge(y,x);
	}
	while(m--){
		for (int i=1;i<=n;i++) {dp[i][0]=dp[i][1]=0;tflag[i]=false;}
		id=0;
		scanf("%d%d%d%d",&x1,&x2,&x3,&x4);
		dfs1(x1,x1);
		if (x2==0) if (x4==0) if ((fa[x1]==x3)||(fa[x3]==x1)){printf("-1\n");continue;}
		if (!tflag[x3]){
			for (int i=1;i<=id;i++) {dp[dd[i]][0]=0;dp[dd[i]][1]=a[dd[i]];}
		}else {
			if (x4==0) for (int i=1;i<=id;i++)
			  if (dd[i]!=x3) {dp[dd[i]][0]=0;dp[dd[i]][1]=a[dd[i]];}else
			  {dp[dd[i]][1]=1000000000;dp[dd[i]][0]=0;}
			if (x4==1) for (int i=1;i<=id;i++)
			  if (dd[i]!=x3) {dp[dd[i]][0]=0;dp[dd[i]][1]=a[dd[i]];}else
			  {dp[dd[i]][1]=a[dd[i]];dp[dd[i]][0]=1000000000;}
		}
		
		dfs2(x1,x1);
		printf("%d\n",dp[x1][x2]);
	}
	return 0;
}
