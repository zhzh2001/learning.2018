#include<bits/stdc++.h>
#define N 155555
using namespace std;
int nxt[N*2],head[N*2],vet[N*2],num,ans[N],ansid,n,m,numb;
bool flag[N];
void add_edge(int u,int v){
	nxt[++num]=head[u];
	head[u]=num;vet[num]=v;
}
void dfs(int u){
	numb++;
	int xb=0;
	for (int e=head[u];e;e=nxt[e]){
		int v=vet[e];if (flag[v]) continue;
		if (numb==n) return ;
		xb++;
	}
	while(xb--){
		int mini=1000000000;
		for (int e=head[u];e;e=nxt[e]){
			int v=vet[e];
			if (numb==n) return ;
			if (flag[v]) continue;
			if (v<mini) mini=v;
		}
		if (numb==n) return ;
		flag[mini]=true;
		ans[++ansid]=mini;
		dfs(mini);
	}
}
void solve1(){
	ans[++ansid]=1;flag[1]=true;
	dfs(1);
	for (int i=1;i<=ansid;i++) printf("%d ",ans[i]);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		int x,y;scanf("%d%d",&x,&y);add_edge(x,y);add_edge(y,x);
	}
	solve1();
	return 0;
}
