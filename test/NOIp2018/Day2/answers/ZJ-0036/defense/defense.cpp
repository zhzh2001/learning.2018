#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int maxn=100005;
const ll oo=1e15;
int n,m,head[maxn],tot,fa[maxn]; char ch[5];
struct E{int to,nxt;}edge[maxn*2];
int num[maxn][2];
ll p[maxn],dp[maxn][2],DP[maxn][2];
void makedge(int u,int v){
	edge[++tot].to=v,edge[tot].nxt=head[u],head[u]=tot;
}
void dfs(int u){
	dp[u][0]=0,dp[u][1]=p[u];
	for (int i=head[u],v;i;i=edge[i].nxt){
		v=edge[i].to; if (v==fa[u]) continue; fa[v]=u,dfs(v);
		dp[u][1]+=min(dp[v][0],dp[v][1]);
		dp[u][0]+=dp[v][1];
	}
}
void del(int u,int v){
	if (u){
		del(fa[u],u);
		if (num[v][1]>0) num[u][0]--; else dp[u][0]-=dp[v][1];
		if (num[v][0]>0 && num[v][1]>0) num[u][1]--;
		else{
			ll res=oo;
			if (!num[v][0]) res=min(res,dp[v][0]);
			if (!num[v][1]) res=min(res,dp[v][1]);
			dp[u][1]-=res;
		}
	}
}
void ins(int u,int v){
	if (u){
		if (num[v][1]>0) num[u][0]++; else dp[u][0]+=dp[v][1];
		if (num[v][0]>0 && num[v][1]>0) num[u][1]++;
		else{
			ll res=oo;
			if (!num[v][0]) res=min(res,dp[v][0]);
			if (!num[v][1]) res=min(res,dp[v][1]);
			dp[u][1]+=res;
		}
		ins(fa[u],u);
	}
}
void solve(){
	dfs(1);
	for (int i=1,x,a,y,b;i<=m;i++){
		scanf("%d%d%d%d",&x,&a,&y,&b);
		del(fa[x],x),num[x][a^1]++,ins(fa[x],x);
		del(fa[y],y),num[y][b^1]++,ins(fa[y],y);
		ll ans=oo;
		if (!num[1][0]) ans=min(ans,dp[1][0]);
		if (!num[1][1]) ans=min(ans,dp[1][1]);
		if (ans==oo) puts("-1"); else printf("%lld\n",ans);
		del(fa[y],y),num[y][b^1]--,ins(fa[y],y);
		del(fa[x],x),num[x][a^1]--,ins(fa[x],x);
	}
}
void solveA2(){
	dp[1][1]=p[1];
	for (int i=2;i<=n;i++) dp[i][0]=dp[i-1][1],dp[i][1]=min(dp[i-1][0],dp[i-1][1])+p[i];
	DP[n][1]=p[n];
	for (int i=n-1;i>=1;i--) DP[i][0]=DP[i+1][1],DP[i][1]=min(DP[i+1][0],DP[i+1][1])+p[i];
	for (int i=1,x,a,y,b;i<=m;i++){
		scanf("%d%d%d%d",&x,&a,&y,&b);
		if (y<x) swap(x,y),swap(a,b);
		if (a==0 && b==0) puts("-1");
		if (a==1 && b==0)
			printf("%lld\n",min(dp[x-1][0],dp[x-1][1])+p[x]+DP[y+1][1]);
		if (a==0 && b==1)
			printf("%lld\n",dp[x-1][1]+p[y]+min(DP[y+1][0],DP[y+1][1]));
		if (a==1 && b==1)
			printf("%lld\n",min(dp[x-1][0],dp[x-1][1])+p[x]+p[y]+min(DP[y+1][0],DP[y+1][1]));
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,&ch);
	for (int i=1;i<=n;i++) scanf("%lld",&p[i]);
	for (int i=1,u,v;i<n;i++) scanf("%d%d",&u,&v),makedge(u,v),makedge(v,u);
	if (n<=2000 && m<=2000) solve();
	else if (ch[0]=='A' && ch[1]=='2') solveA2();
	return 0;
}
