#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll mod=1000000007;
int n,m; ll ans,two[1000010],thr[1000010];
ll solve1(){return two[m];}
ll solve2(){return 4*thr[m-1]%mod;}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m); if (m<n) swap(n,m);
	two[0]=1; for (int i=1;i<=n+m;i++) two[i]=two[i-1]*2%mod;
	thr[0]=1; for (int i=1;i<=n+m;i++) thr[i]=thr[i-1]*3%mod;
	if (n==1) printf("%lld\n",solve1());
	if (n==2) printf("%lld\n",solve2());
	if (n==3 && m==3) puts("112");
	if (n==5 && m==5) puts("7136");
	return 0;
}
