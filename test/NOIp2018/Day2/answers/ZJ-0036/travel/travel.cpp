#include <bits/stdc++.h>
using namespace std;
const int maxn=5005;
int n,m,head[maxn],tot,A[maxn],now,p[maxn],ans[maxn],id[maxn][maxn];
bool Map[maxn][maxn];
struct E{int to,nxt;bool flag;}edge[maxn*2];
int front,rear,Q[maxn]; bool vis[maxn];
void makedge(int u,int v){
	edge[++tot].to=v,edge[tot].nxt=head[u],head[u]=tot;
	id[u][v]=tot;
}
bool bfs(){
	memset(vis,0,sizeof(vis));
	front=rear=0,Q[rear++]=1,vis[1]=1;
	while (front<rear){
		int u=Q[front++];
		for (int i=head[u],v;i;i=edge[i].nxt)
		if (!edge[i].flag){
			v=edge[i].to; if (vis[v]) continue; Q[rear++]=v,vis[v]=1;
		}
	}
	for (int i=1;i<=n;i++) if (!vis[i]) return 0;
	return 1;
}
void dfs(int u,int fa,int end){
	p[++now]=u;
	int start=end+1;
	for (int i=head[u],v;i;i=edge[i].nxt)
	if (!edge[i].flag){
		v=edge[i].to; if (v==fa) continue; A[++end]=v; 
	}
	if (start>end) return;
	for (int i=start;i<=end;i++) dfs(A[i],u,end);
}
void update(){
	for (int i=1;i<=n;i++){
		if (p[i]>ans[i]) return;
		if (p[i]<ans[i]) break;
	}
	for (int i=1;i<=n;i++) ans[i]=p[i];
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,u,v;i<=m;i++)
		scanf("%d%d",&u,&v),Map[u][v]=Map[v][u]=1;
	for (int i=1;i<=n;i++)
	for (int j=n;j>=1;j--)
	if (Map[i][j])
		makedge(i,j);
	if (m==n-1){
		now=0,dfs(1,0,0);
		for (int i=1;i<=n;i++) printf("%d ",p[i]);
	}
	else{
		ans[1]=1e8;
		for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
		if (Map[i][j]){
			edge[id[i][j]].flag=edge[id[j][i]].flag=1;
			if (bfs())
				now=0,dfs(1,0,0),update();
			edge[id[i][j]].flag=edge[id[j][i]].flag=0;
		}
		for (int i=1;i<=n;i++) printf("%d ",ans[i]);
	}
	return 0;
}
