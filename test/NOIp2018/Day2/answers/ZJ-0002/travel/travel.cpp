#include <bits/stdc++.h>
using namespace std;
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
const int N = 10005;
int n, m;
int nxt[N], to[N], h[N], frm[N], cnt, ru[N];
void add(int u,int v) {
	nxt[++cnt]=h[u],frm[cnt]=u,h[u]=cnt,to[cnt]=v;
}
int q[N], ans[N], siz=0;
vector<int> v[N];

void dfs1(int u,int fa) {
	if(u==0)	return ;
	v[u].clear();
	q[++siz]=u;
	for(int i=h[u];i;i=nxt[i])if(to[i]!=fa)	v[u].push_back(to[i]);
	sort(v[u].begin(),v[u].end());
	for(int i=0;i<v[u].size();++i)	dfs1(v[u][i],u);
}

void sol1() {
	dfs1(1,0);
	rep(i,1,siz)	printf("%d ", q[i]);
}
queue<int> que;
int vis[N];
void topo() {
	rep(i,1,n)if(ru[i]==1)vis[i]=1,que.push(i);
	int u;
	while(!que.empty()) {
		u=que.front(); que.pop();
		for(int i=h[u];i;i=nxt[i])if(!vis[to[i]]) {
			ru[to[i]]--;
			if(ru[to[i]]==1)	vis[to[i]]=1,que.push(to[i]);
		}
	}
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d", &n, &m);
	int a, b;
	rep(i,1,m) {
		scanf("%d%d", &a, &b);
		add(a,b), add(b,a);
		ru[a]++, ru[b]++;
	}
	if(m==n-1)	sol1();
	else {
		int tmp, tmp2, k;
		topo();
		memset(ans,0x3f,sizeof(ans));
		rep(i,1,m) {
			k = i*2;
			if(!vis[frm[k]]&&!vis[to[k]]) {
				tmp=to[k], tmp2=to[k-1];
				to[k]=to[k-1]=0;
				siz=0;
				dfs1(1,0);
				to[k]=tmp, to[k-1]=tmp2;
				rep(z,1,n)if(ans[z]>q[z]) {
					memcpy(ans,q,sizeof(ans));
					break;
				}
			}
		}
		rep(i,1,n)printf("%d ", ans[i]);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
