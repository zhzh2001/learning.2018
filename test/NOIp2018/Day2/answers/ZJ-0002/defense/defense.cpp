#include <bits/stdc++.h>
using namespace std;
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
const int N = 4005;
int n, m, type, p[2005];
char opt;
int nxt[N], to[N], h[N], cnt;
void add(int u,int v) {
	nxt[++cnt]=h[u],h[u]=cnt,to[cnt]=v;
}
int dp[100005][2][2], vis[100005];

void sol() {
	memset(dp,0x3f,sizeof(dp));
	dp[0][0][0]=dp[0][0][1]=0;
	rep(i,1,n) {
		if(vis[i]) {
			if(vis[i]==1) {
				dp[i][0][0]=dp[i-1][0][1];
				dp[i][0][1]=dp[i-1][1][1];
			}
			else {
				dp[i][1][1]=min(dp[i-1][0][0],min(dp[i-1][0][1],dp[i-1][1][1]))+p[i];
			}
			continue;
		}
		dp[i][0][0]=dp[i-1][0][1];
		dp[i][0][1]=dp[i-1][1][1];
		dp[i][1][1]=min(dp[i-1][0][0],min(dp[i-1][0][1],dp[i-1][1][1]))+p[i];
	}
	printf("%d\n", min(dp[n][1][1],dp[n][0][1]));
}

int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d %d %c%d", &n, &m, &opt, &type);
	rep(i,1,n)	scanf("%d", &p[i]);
	int a, b, x, y;
	rep(i,2,n) {
		scanf("%d%d", &a, &b);
		add(a,b), add(b,a);
	}
	while(m--) {
		scanf("%d%d%d%d", &a, &x, &b, &y);
		vis[a]=x+1, vis[b]=y+1;
		if(a==b&&x!=y) {puts("-1");vis[a]=0;continue;}
		sol();
		vis[a]=0, vis[b]=0;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
