#include <cstdio>
#include <algorithm>

using namespace std;

typedef long long LL;

const int N = 1000005;
const int MOD = (int)1e9 + 7;

int n, m;

int Pow(int x, int b) {
  int r = 1;
  for (; b; b >>= 1, x = (LL)x * x % MOD)
    if (b & 1) r = (LL)r * x % MOD;
  return r;
}

int main() {
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);
  scanf("%d%d", &n, &m);
  if (n > m) swap(n, m);
  if (n == 1) {
    printf("%d\n", Pow(2, m));
    return 0;
  }
  if (n == 2) {
    int p = Pow(3, m - 1);
    printf("%lld\n", 4LL * p % MOD);
    return 0;
  }
  {
    static int f[N][2];
    f[3][0] = 2 * 4;
    f[3][1] = 4 * 4;
    for (int i = 4; i <= m; ++i) {
      f[i][0] = f[i - 1][0];
      f[i][1] = (f[i - 1][1] * 3LL + f[i - 1][0] * 3LL) % MOD;
    }
    int re = (f[m][0] * 3LL + f[m][1] * 2LL) % MOD;
    printf("%d\n", re * 2 % MOD);
  }
  return 0;
}
