#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

int n, m;
char cas[5];

namespace So1 {
  const int N = 2005;
  const int INF = (int)1e9 + 7;

  int qa, qb, qx, qy;
  int a[N], f[N][2];

  int yu, la[N], to[N << 1], pr[N << 1];
  inline void Ade(int a, int b) {
    to[++yu] = b, pr[yu] = la[a], la[a] = yu;
  }

  void Dfs(int x, int fat) {
    f[x][0] = 0, f[x][1] = a[x];
    for (int i = la[x]; i; i = pr[i]) {
      if (to[i] == fat) continue;
      Dfs(to[i], x);
      f[x][0] += f[to[i]][1];
      if (f[x][0] > INF) f[x][0] = INF;
      f[x][1] += min(f[to[i]][0], f[to[i]][1]);
      if (f[x][1] > INF) f[x][1] = INF;
    }
    if (x == qx) f[x][qa ^ 1] = INF;
    if (x == qy) f[x][qb ^ 1] = INF;
  }
  
  void Main() {
    for (int i = 1; i <= n; ++i) {
      scanf("%d", &a[i]);
    }
    for (int i = 1, x, y; i < n; ++i) {
      scanf("%d%d", &x, &y);
      Ade(x, y), Ade(y, x);
    }
    for (; m--; ) {
      scanf("%d%d%d%d", &qx, &qa, &qy, &qb);
      Dfs(1, 0);
      int re = min(f[1][0], f[1][1]);
      printf("%d\n", re == INF? -1 : re);
    }
  }
}

namespace So2 {
  typedef long long LL;
  const int N = 100005;
  const LL INF = (LL)1e17 + 7;

  int a[N], fa[N];
  LL f[N][2], g[N][2];
  
  int yu, la[N], to[N << 1], pr[N << 1];
  inline void Ade(int a, int b) {
    to[++yu] = b, pr[yu] = la[a], la[a] = yu;
  }

  void Dfs0(int x, int fat) {
    f[x][0] = 0, f[x][1] = a[x];
    for (int i = la[x]; i; i = pr[i]) {
      if (to[i] == fat) continue;
      fa[to[i]] = x;
      Dfs0(to[i], x);
      f[x][0] += f[to[i]][1];
      f[x][1] += min(f[to[i]][0], f[to[i]][1]);
    }
  }
  void Dfs1(int x, int fat) {
    for (int i = la[x]; i; i = pr[i]) {
      int v = to[i];
      if (v == fat) continue;
      LL up1 = g[x][1] + f[x][1] - min(f[v][0], f[v][1]);
      LL up0 = g[x][0] + f[x][0] - f[v][1];
      g[v][0] = up1;
      g[v][1] = min(up0, up1);
      Dfs1(v, x);
    }
  }
  
  void Main() {
    for (int i = 1; i <= n; ++i) {
      scanf("%d", &a[i]);
    }
    for (int i = 1, x, y; i < n; ++i) {
      scanf("%d%d", &x, &y);
      Ade(x, y), Ade(y, x);
    }
    Dfs0(1, 0);
    Dfs1(1, 0);
    for (int a, b, x, y; m--; ) {
      scanf("%d%d%d%d", &x, &a, &y, &b);
      if (fa[x] == y) swap(x, y), swap(a, b);
      if (!a && !b) {
        printf("-1\n");
      } else {
        LL fx = g[x][a] + f[x][a];
        if (a == 0) fx -= f[y][1];
        else fx -= min(f[y][0], f[y][1]);
        printf("%lld\n", fx + f[y][b]);
      }
    }
  }
}

namespace So3 {
  typedef long long LL;
  const int N = 100005;
  const LL INF = (LL)1e17 + 7;

  int a[N], fa[N];
  LL f[N][2], g[N][2];
  
  int yu, la[N], to[N << 1], pr[N << 1];
  inline void Ade(int a, int b) {
    to[++yu] = b, pr[yu] = la[a], la[a] = yu;
  }

  void Dfs(int x, int fat) {
    f[x][0] = 0, f[x][1] = a[x];
    for (int i = la[x]; i; i = pr[i]) {
      if (to[i] == fat) continue;
      fa[to[i]] = x;
      Dfs(to[i], x);
      f[x][0] += f[to[i]][1];
      f[x][1] += min(f[to[i]][0], f[to[i]][1]);
    }
  }
  
  void Main() {
    for (int i = 1; i <= n; ++i) {
      scanf("%d", &a[i]);
    }
    for (int i = 1, x, y; i < n; ++i) {
      scanf("%d%d", &x, &y);
      Ade(x, y), Ade(y, x);
    }
    Dfs(1, 0);
    for (int a, b, x, y; m--; ) {
      scanf("%d%d%d%d", &x, &a, &y, &b);
      g[y][b] = f[y][b];
      g[y][b ^ 1] = INF;
      for (int t = y; t != 1; t = fa[t]) {
        int ft = fa[t];
        g[ft][0] = f[ft][0] - f[t][1];
        g[ft][1] = f[ft][1] - min(f[t][0], f[t][1]);
        g[ft][0] += g[t][1];
        if (g[ft][0] > INF) g[ft][0] = INF;
        g[ft][1] += min(g[t][0], g[t][1]);
        if (g[ft][1] > INF) g[ft][1] = INF;
      }
      LL re = g[1][a];
      printf("%lld\n", re == INF? -1 : re);
    }
  }
}

int main() {
  freopen("defense.in", "r", stdin);
  freopen("defense.out", "w", stdout);
  scanf("%d%d%s", &n, &m, cas);
  if (n <= 2000 && m <= 2000) {
    So1::Main();
  } else if (cas[1] == '2') {
    So2::Main();
  } else {
    So3::Main();
  }
  return 0;
}
