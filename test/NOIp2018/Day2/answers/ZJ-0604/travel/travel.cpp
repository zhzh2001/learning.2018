#include <queue>
#include <cstdio>
#include <vector>
#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;

const int N = 5005;

int n, m, tx, ty, clk;
int fa[N], dfn[N];
int ans[N], li[N];
vector<int> g[N];
vector<pair<int, int> > seg;

void Update() {
  for (int i = 1; i <= n; ++i) {
    if (ans[i] < li[i]) return;
    if (ans[i] > li[i]) break;
  }
  for (int i = 1; i <= n; ++i) {
    ans[i] = li[i];
  }
}

void Dfs_(int x, int fat) {
  dfn[x] = ++clk;
  for (int i = 0; i < (int)g[x].size(); ++i) {
    int v = g[x][i];
    if (v == fat) continue;
    if (!dfn[v]) {
      fa[v] = x;
      Dfs_(v, x);
    } else if (dfn[v] < dfn[x]) {
      //cerr << "find circle. " << x << endl;
      seg.push_back(make_pair(x, v));
      for (int t = x; t != v; t = fa[t]) {
        seg.push_back(make_pair(t, fa[t]));
      }
    }
  }
}

void Dfs(int x, int fat) {
  li[++*li] = x;
  for (int i = 0; i < (int)g[x].size(); ++i) {
    int v = g[x][i];
    if (v == fat) continue;
    if (x == tx && v == ty) continue;
    if (x == ty && v == tx) continue;
    Dfs(v, x);
  }
}

int main() {
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);
  memset(ans, 0x3f, sizeof ans);
  scanf("%d%d", &n, &m);
  for (int i = 1, x, y; i <= m; ++i) {
    scanf("%d%d", &x, &y);
    g[x].push_back(y);
    g[y].push_back(x);
  }
  for (int i = 1; i <= n; ++i) {
    sort(g[i].begin(), g[i].end());
  }
  if (m == n - 1) {
    *li = 0;
    Dfs(1, 0);
    Update();
  } else {
    Dfs_(1, 0);
    for (int i = 0; i < (int)seg.size(); ++i) {
      tx = seg[i].first;
      ty = seg[i].second;
      *li = 0;
      Dfs(1, 0);
      Update();
    }
  }
  for (int i = 1; i <= n; ++i) {
    printf("%d%c", ans[i], " \n"[i == n]);
  }
  return 0;
}
