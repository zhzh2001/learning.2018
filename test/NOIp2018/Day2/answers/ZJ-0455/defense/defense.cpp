#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
#include<vector>
using namespace std;
vector<int>g[100009];
int n,m,a,b,x,y,type;
long long f[100009][2],p[100009];
char ch;
void chkmin(int&a,const int&b){a=min(a,b);}
void dfs(int u,int fat){
	if(g[u].size()==1&&fat){
		if(u!=a&&u!=b)
			f[u][0]=0,f[u][1]=p[u];
		else
			(u==a)?(f[u][x]=(x?p[u]:0)):(f[u][y]=(y?p[u]:0));
		return;
	}
	for(int i=0;i<g[u].size();i++)
		if(g[u][i]!=fat)
			dfs(g[u][i],u);
	if(u==a&&x||u==b&&y){
		long long tmp=0;
		for(int i=0;i<g[u].size();i++)
			if(g[u][i]!=fat)
				tmp+=min(f[g[u][i]][0],f[g[u][i]][1]);
		f[u][1]=tmp+p[u];
	}else if(u==a&&!x||u==b&&!y){
		long long sum=0;
		for(int i=0;i<g[u].size();i++)
			if(g[u][i]!=fat)
				sum+=f[g[u][i]][1];
		f[u][0]=sum;
	}else{
		long long sum=0;
		for(int i=0;i<g[u].size();i++)
			if(g[u][i]!=fat)
				sum+=f[g[u][i]][1];
		f[u][0]=sum;
		long long tmp=0;
		for(int i=0;i<g[u].size();i++)
			if(g[u][i]!=fat)
				tmp+=min(f[g[u][i]][0],f[g[u][i]][1]);
		f[u][1]=tmp+p[u];
	}
}
int main(){
	freopen("defense.in","r",stdin),freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	while(ch=getchar(),!isalpha(ch));
	scanf("%d",&type);
	for(int i=1;i<=n;i++)
		scanf("%lld",&p[i]);
	for(int i=1;i<=n-1;i++){
		int u,v;scanf("%d%d",&u,&v);
		g[u].push_back(v),g[v].push_back(u);
	}
	for(int i=1;i<=m;i++){
		memset(f,0x3f,sizeof(f));
		scanf("%d%d%d%d",&a,&x,&b,&y);
		dfs(a,0);
		printf("%lld\n",f[a][x]>=0x3f3f3f3f3f3f3f3fll?-1:f[a][x]);
		/*for(int i=1;i<=n;i++)
			printf("%d %d\n",f[i][0],f[i][1]);*/
	}
	return 0;
}

