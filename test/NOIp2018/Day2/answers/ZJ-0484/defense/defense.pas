var n,m,i,j,k,tot,x,y,a,b,u,v,e:longint;
head,p:array[0..100005] of longint;
next,vet:array[0..200005] of longint;
c,cc:char;
flag:boolean;
dp:Array[0..100005,0..2] of int64;
ans:int64;

function min(a,b:int64):int64;
begin if a>b then exit(b);exit(a);end;

procedure add(x,y:longint);
begin inc(tot);next[tot]:=head[x];head[x]:=tot;vet[tot]:=y;end;

procedure dfs(u,fa:longint);
var e,v:longint;
begin
dp[u][0]:=0;
dp[u][1]:=p[u];
e:=head[u];
while e<>0 do begin
	v:=vet[e];
	if v<>fa then begin
		dfs(v,u);
		dp[u][1]:=dp[u][1]+min(dp[v][1],dp[v][0]);
		dp[u][0]:=dp[u][0]+dp[v][1];
		end;
	e:=next[e];
	end;
if x=u then begin
	if a=0 then dp[u][1]:=maxlongint*100
		else begin dp[u][1]:=dp[u][1]+(a-1)*p[u];dp[u][0]:=maxlongint*100;end;
	end;
if y=u then begin
	if b=0 then dp[u][1]:=maxlongint*100
		else begin dp[u][1]:=dp[u][1]+(b-1)*p[u];dp[u][0]:=maxlongint*100;end;
	end;
end;

procedure special_1;
begin
for m:=m downto 1 do begin
	readln(x,a,y,b);
	if (a=0)and(b=0) then begin
		e:=head[x];
		flag:=false;
		while e<>0 do begin
			v:=vet[e];
			if v=y then begin flag:=true;break;end;
			e:=next[e];
			end;
		if flag then writeln(-1);
		if flag then continue;
		end;
	dfs(1,0);
	ans:=min(dp[1][1],dp[1][0]);
        if ans<>maxlongint*100 then
	writeln(ans) else writeln(-1);
	end;
end;

begin
assign(input,'defense.in');
assign(output,'defense.out');
reset(input);
rewrite(output);
readln(n,m,c,c,cc);
for i:=1 to n do read(p[i]);
for i:=1 to n-1 do begin
	readln(x,y);
	add(x,y);
	add(y,x);
	end;
if (n<=2000)and(m<=2000) then special_1;
close(input);
close(output);
end.
