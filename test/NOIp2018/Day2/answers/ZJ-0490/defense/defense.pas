var
  s:char;
  t:string;
  bi:boolean;
  ans:int64;
  head,tail,a1,a2,len,a3,a4:longint;
  i,j,m,n,k,p:longint;
  num,value,f,father,a:array[0..100005]of longint;
  next,other:array[0..200005]of longint;
  dp:array[0..100005,0..1]of int64;
function min(x,y:int64):int64;
begin
  if x<y then exit(x)
  else exit(y);
end;
function dfs(u,opt:longint):int64;
var
  ci:boolean;
  k,p:longint;
begin
  if dp[u,opt]<>0 then
    exit(dp[u,opt]);
  if num[u]=0 then
    begin
      if ((u=a1)and(a2=1))or((u=a3)and(a4=1))or((u<>a1)and(u<>a3)) then
        dp[u,1]:=value[u]
      else dp[u,1]:=maxlongint*1000;
      if ((u=a1)and(a2=0))or((u=a3)and(a4=0))or((u<>a1)and(u<>a3)) then
        dp[u,0]:=0
      else dp[u,0]:=maxlongint*1000;
      exit(dp[u,opt]);
    end;
  if opt=0 then
    begin
      ci:=true;
      k:=a[u];
      while k<>0 do
        begin
          p:=other[k];
          if father[u]=p then
            begin
              k:=next[k]; continue;
            end;
          if ((p=a1)and(a2=0))or((p=a3)and(a4=0)) then
            begin
              ci:=false; break;
            end;
          k:=next[k];
        end;
      if ci=false then
        begin
          dp[u,opt]:=maxlongint*1000;
          exit(dp[u,opt]);
        end;
      k:=a[u];
      while k<>0 do
        begin
          p:=other[k];
          if father[u]=p then
            begin
              k:=next[k]; continue;
            end;
          dp[u,opt]:=dp[u,opt]+dfs(p,1);
          k:=next[k];
        end;
      exit(dp[u,opt]);
    end
  else
    begin
      k:=a[u]; dp[u,1]:=value[u];
      while k<>0 do
        begin
          p:=other[k];
          if father[u]<>p then
            begin
              if p=a1 then
                dp[u,opt]:=dp[u,opt]+dfs(p,a2)
              else
              if p=a3 then
                dp[u,opt]:=dp[u,opt]+dfs(p,a4)
              else
                dp[u,opt]:=dp[u,opt]+min(dfs(p,0),dfs(p,1));
            end;
          k:=next[k];
        end;
      exit(dp[u,opt]);
    end;
end;
begin
  assign(input,'defense.in');
  assign(output,'defense.out');
  reset(input); rewrite(output);
  read(n,m); read(s); readln(t);
  for i:=1 to n do
    read(value[i]);
  readln;
  for i:=1 to n-1 do
    begin
      readln(a1,a2);
      inc(len);
      next[len]:=a[a1]; a[a1]:=len; other[len]:=a2;
      inc(len);
      next[len]:=a[a2]; a[a2]:=len; other[len]:=a1;
    end;
  head:=0; tail:=1; f[1]:=1;
  while head<tail do
    begin
      inc(head);
      k:=a[f[head]];
      while k<>0 do
        begin
          p:=other[k];
          if father[f[head]]<>p then
            begin
              inc(tail); f[tail]:=p;
              father[p]:=f[head];
              inc(num[f[head]]);
            end;
          k:=next[k];
        end;
    end;
  while m<>0 do
    begin
      dec(m);
      readln(a1,a2,a3,a4); bi:=false;
      k:=a[a1];
      while k<>0 do
        begin
          p:=other[k];
          if p=a3 then
            begin
              bi:=true; break;
            end;
          k:=next[k];
        end;
      if (bi=true)and(a2=0)and(a4=0) then
        begin
          writeln('-1'); continue;
        end;
      fillchar(dp,sizeof(dp),0);
      dp[a1,1-a2]:=maxlongint*1000; dp[a3,1-a4]:=maxlongint*1000;
      ans:=min(dfs(1,0),dfs(1,1));
      if ans>=maxlongint then
        writeln('-1')
      else writeln(ans);
    end;
  close(input); close(output);
end.