var
  ans:int64;
  i,j,m,n,k,p:longint;
begin
  assign(input,'game.in');
  assign(output,'game.out');
  reset(input); rewrite(output);
  p:=1000000007;
  readln(n,m);
  if n=1 then
    begin
      ans:=1;
      for i:=1 to m do
        ans:=(ans*2) mod p;
      writeln(ans);
    end
  else
  if n=2 then
    begin
      ans:=4;
      for i:=2 to m do
        ans:=(ans*3) mod p;
      writeln(ans);
    end;
  close(input); close(output);
end.