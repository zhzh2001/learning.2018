#include<bits/stdc++.h>
#define M 5005
using namespace std;
int n,m;
vector<int> E[M];
struct Pxx{
	int a,b,ans[M],res[M],tt;
	int fa[M],dep[M];
	bool vis[M];
	void predfs(int x,int f){
		vis[x]=1;
		fa[x]=f,dep[x]=dep[f]+1;
		for(int i=0;i<(int)E[x].size();i++){
			int v=E[x][i];
			if(v==f) continue;
			if(vis[v]) a=x,b=v;
			else predfs(v,x);
		}
	}
	void dfs(int x,int f){
		res[++tt]=x;
		for(int i=0;i<(int)E[x].size();i++){
			int v=E[x][i];
			if(v==f||v==a&&x==b||v==b&&x==a) continue;
			dfs(v,x);
		}
	}
	bool check(){
		for(int i=1;i<=n;i++){
			if(res[i]<ans[i]) return 1;
			else if(res[i]>ans[i]) return 0;
		}
		return 0; 
	}
	void solve(){
		a=b=-1;
		predfs(1,0);
		if(a==-1&&b==-1){
			tt=0;
			dfs(1,0);
			printf("%d",res[1]);
			for(int i=2;i<=n;i++) printf(" %d",res[i]);
		}
		else{
			tt=0;
			dfs(1,0);
			for(int i=1;i<=n;i++) ans[i]=res[i];
			int x=a,y=b;
			while(x!=y){
				if(dep[x]<dep[y]) swap(x,y);
				tt=0,a=x,b=fa[x];
				dfs(1,0);
				if(check()) for(int i=1;i<=n;i++) ans[i]=res[i];
				x=fa[x];
			}
			printf("%d",ans[1]);
			for(int i=2;i<=n;i++) printf(" %d",ans[i]);
		}
	}
}C;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1,f,t;i<=m;i++){
		scanf("%d%d",&f,&t);
		E[f].push_back(t),E[t].push_back(f);
	}
	for(int i=1;i<=n;i++) sort(E[i].begin(),E[i].end());
	C.solve();
	return 0;
} 
