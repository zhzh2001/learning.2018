#include<bits/stdc++.h>
#define M 100005
#define ll long long
#define P 100000007
using namespace std;
int n,m,tot,head[M];
char s[20];
struct edge{
	int t,nxt;
}E[M<<1];
void addedge(int f,int t){
	E[++tot]=(edge){t,head[f]};
	head[f]=tot;
}
int val[M];
struct Pxx{
	ll dp[M][2];
	int mark[M],flag;
	void dfs(int x,int f){
		ll res1=0,res2=0;
		for(int i=head[x];i;i=E[i].nxt){
			int v=E[i].t;
			if(v==f) continue;
			dfs(v,x);
			if(mark[v]==2&&mark[x]==2){
				flag=0;return;
			}
			res1+=min(dp[v][0],dp[v][1]);
			res2+=dp[v][1];
		}
		if(mark[x]!=1) dp[x][0]=res2;
		if(mark[x]!=2) dp[x][1]=res1+val[x];
	}
	void solve(){
		for(int i=1,a,b,x,y;i<=m;i++){
			memset(dp,63,sizeof(dp));
			memset(mark,0,sizeof(mark));
			flag=1;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(x) mark[a]=1;
			else mark[a]=2;
			if(y) mark[b]=1;
			else mark[b]=2;
			dfs(1,0);
			if(!flag) puts("-1");
			else printf("%lld\n",min(dp[1][0],dp[1][1]));
		}
	}
}C;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s+1);
	for(int i=1;i<=n;i++) scanf("%d",&val[i]);
	for(int i=1,f,t;i<n;i++){
		scanf("%d%d",&f,&t);
		addedge(f,t),addedge(t,f);
	}
	C.solve();
	return 0;
}
