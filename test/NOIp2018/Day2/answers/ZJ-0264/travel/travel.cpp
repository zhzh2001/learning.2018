#include <bits/stdc++.h>
#define res register int
#define ll long long
#define pb push_back
#define mp make_pair
#define fr first
#define sc second
#define gc getchar()
#define INF 1000000007
#define MAXN 5005
using namespace std;

inline int read()
{
	int ch=gc,f=0;
	int x=0;
	while(ch<'0'||ch>'9')
	{
		f|=ch=='-';
		ch=gc;
	}
	while(ch>='0'&&ch<='9')
	{
		x=x*10+ch-'0';
		ch=gc;
	}
	return f?-x:x;
}
struct eric
{
	int to,next;
}edge[MAXN<<1];
int head[MAXN],size=1;
int n,m;
int ans[MAXN],cnt,vis[MAXN];
int stak[MAXN],top,vstak[MAXN],vtop,instack[MAXN],ined[MAXN],nstak[MAXN],ntop;
int dfn[MAXN],low[MAXN],indeex,dad[MAXN],rd[MAXN];
int topo[MAXN],topohead=1,topotail;
priority_queue<int,vector<int>,greater<int> > bob;
inline void add(int x,int y)
{
	edge[++size].to=y;
	++rd[y];
	edge[size].next=head[x];
	head[x]=size;
}
inline void dfs(int pos,int fa)
{
	if(ined[pos])
	return;
	ans[++cnt]=pos;
	ined[pos]=1;
	priority_queue<int,vector<int>,greater<int> > bob;
	for(int i=head[pos];i;i=edge[i].next)
	{
		int v=edge[i].to;
		if(v==fa||ined[v])
		continue;
		bob.push(v);
	}
	while(!bob.empty())
	{
		dfs(bob.top(),pos);
		bob.pop();
	}
}
inline void toposort()
{
	for(int i=1;i<=n;i++)
	{
		if(rd[i]==1)
		topo[++topotail]=i,vis[i]=1;
	}
	while(topohead<=topotail)
	{
		int u=topo[topohead++];
		for(int i=head[u];i;i=edge[i].next)
		{
			int v=edge[i].to;
			if(vis[v])
			continue;
			if((--rd[v])==1)
			topo[++topotail]=v,vis[v]=1;
		}
	}
}
inline void onit(int from)
{
	int u=from,to=0,bak;
	stak[top=1]=from;
	ans[++cnt]=from;
	ined[from]=instack[from]=1;
	for(int i=head[u];i;i=edge[i].next)
	{
		int v=edge[i].to;
		if(vis[v])
		vstak[++vtop]=v;
		else
		if(to)
		bak=v;
		else
		to=v;
	}
	if(to>bak)
	swap(to,bak);
	if(vstak[top]<to&&vstak[vtop])
	dfs(vstak[vtop],from);
	while(top)
	{
		if(stak[top]==bak)
		{
			int pp=vtop;
			while(pp)
			{
				if(ined[vstak[vtop]]||!vstak[vtop])
				pp--;
				else
				break;
			}
			if(!pp||vstak[pp]>bak)
			{
				ans[++cnt]=bak;
				ined[bak]=1;
				for(int i=head[bak];i;i=edge[i].next)
				{
					int v=edge[i].to;
					if(vis[v])
					dfs(v,bak);
				}
			}
			while(pp>1)
			{
				if(!ined[vstak[pp]]&&vstak[vtop])
				dfs(vstak[pp],stak[pp]);
				pp--;
			}
			if(!ined[bak])
			{
				if(!ined[vstak[1]]&&vstak[1]<bak&&vstak[top])
				{
					dfs(vstak[1],from);
				}
				ans[++cnt]=bak;
				ined[bak]=1;
				for(int i=head[bak];i;i=edge[i].next)
				{
					int v=edge[i].to;
					if(vis[v])
					dfs(v,bak);
				}
			}
			dfs(vstak[1],from);
		}
		if(top>1)
		for(int i=head[stak[top]];i;i=edge[i].next)
		{
			int v=edge[i].to;
			if(vis[v])
			vstak[++vtop]=v;
			if(instack[v]||vis[v])
			continue;
			to=v;
		}
		if(to>bak)
		{
			while(vtop)
			{
				if(!ined[vstak[vtop]]&&vstak[vtop])
				dfs(vstak[vtop],stak[top]);
				--vtop;
			}
			top=0;
		}
		if(!top)
		break;
		if(vstak[vtop]<to&&vstak[top])
		dfs(vstak[vtop],stak[top]);
		stak[++top]=to;
		ans[++cnt]=to;
		ined[to]=1;
	}
	if(vstak[1]<bak)
	dfs(vstak[1],from);
	dfs(bak,from);
}
inline void dfs2(int pos,int fa)
{
	if(ined[pos])
	return;
	if(!vis[pos])
	{
		onit(pos);
		return;
	}
	ans[++cnt]=pos;
	ined[pos]=1;
	priority_queue<int,vector<int>,greater<int> > bob;
	for(int i=head[pos];i;i=edge[i].next)
	{
		int v=edge[i].to;
		if(v==fa||ined[v])
		continue;
		bob.push(v);
	}
	while(!bob.empty())
	{
		dfs2(bob.top(),pos);
		bob.pop();
	}
}
inline void jihuan()
{
	if(!vis[1])
	onit(1);
	else
	{
		int tt=0,bb=0;
		ined[1]=ans[++cnt]=1;
		for(int i=head[1];i;i=edge[i].next)
		{
			int v=edge[i].to;
			if(!tt)
			tt=v;
			else
			bb=v;
		}
		if(tt>bb&&bb)
		swap(tt,bb);
		dfs2(tt,1);
		if(bb)
		dfs2(bb,1);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	int x,y;
	for(int i=1;i<=m;i++)
	{
		x=read(),y=read();
		add(x,y);
		add(y,x);
	}
	ined[0]=1;
	if(m==n-1)
	dfs(1,0);
	else
	{
		toposort();
		jihuan();
	}
	for(int i=1;i<=n;i++)
	printf("%d ",ans[i]);
	printf("\n");
	return 0;
}
