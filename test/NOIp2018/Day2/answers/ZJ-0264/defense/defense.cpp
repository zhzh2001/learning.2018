#include <bits/stdc++.h>
#define res register int
#define ll long long
#define pb push_back
#define mp make_pair
#define fr first
#define sc second
#define gc getchar()
#define INF 1000000007
#define MAXN 5005
using namespace std;

inline int read()
{
	int ch=gc,f=0;
	int x=0;
	while(ch<'0'||ch>'9')
	{
		f|=ch=='-';
		ch=gc;
	}
	while(ch>='0'&&ch<='9')
	{
		x=x*10+ch-'0';
		ch=gc;
	}
	return f?-x:x;
}
struct eric
{
	int to,next;
}edge[MAXN<<1];
int head[MAXN],size=1;
int n,m,typr;
int must[MAXN];
ll a[MAXN],f[MAXN][3],g[MAXN][3];
inline void add(int x,int y)
{
	edge[++size].to=y;
	edge[size].next=head[x];
	head[x]=size;
}
inline void dp(int pos,int fa)
{
	ll minx=(ll)INF<<10;
	g[pos][0]=g[pos][1]=g[pos][2]=0;
	for(int i=head[pos];i;i=edge[i].next)
	{
		int v=edge[i].to;
		if(v==fa)
		continue;
		dp(v,pos);
		g[pos][2]+=min(g[v][1],g[v][0]);
		minx=min(minx,g[v][1]-g[v][0]);
		g[pos][1]+=min(g[v][1],g[v][2]);
	}
	if(minx>0)
	g[pos][0]=g[pos][2]+minx;
	else
	g[pos][0]=g[pos][2];
	g[pos][1]+=a[pos];
}
inline void dfs(int pos,int fa)
{
	ll minx=(ll)INF<<10;
	if(typr==1&&pos>1)
	{
		f[pos][0]=g[pos][0];
		f[pos][1]=g[pos][1];
		f[pos][2]=g[pos][2];
		if(must[pos]==0)
		f[pos][1]=(ll)INF<<10;
		if(must[pos]==1)
		f[pos][0]=(ll)INF<<10;
		return;
	}
	f[pos][0]=f[pos][1]=f[pos][2]=0;
	for(int i=head[pos];i;i=edge[i].next)
	{
		int v=edge[i].to;
		if(v==fa)
		continue;
		dfs(v,pos);
		f[pos][2]+=min(must[v]==0?((ll)INF<<10):f[v][1],must[v]==1?((ll)INF<<10):f[v][0]);
		if(must[v]==-1)
		minx=min(minx,f[v][1]-f[v][0]);
		if(must[v]==1)
		minx=0;
		f[pos][1]+=min(must[v]==0?((ll)INF<<10):f[v][1],must[v]==1?((ll)INF<<10):f[v][2]);
	}
	if(minx>0)
	f[pos][0]=f[pos][2]+minx;
	else
	f[pos][0]=f[pos][2];
	f[pos][1]+=a[pos];
	if(must[pos]==0)
	f[pos][1]=(ll)INF<<10;
	if(must[pos]==1)
	f[pos][2]=f[pos][0]=(ll)INF<<10;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),typr=read();
	for(int i=1;i<=n;i++)
	{
		a[i]=read();
	}
	int x,y,opt1,opt2;
	for(int i=1;i<n;i++)
	{
		x=read(),y=read();
		add(x,y);
		add(y,x);
	}
	memset(must,-1,sizeof must);
	while(m--)
	{
		x=read(),opt1=read(),y=read(),opt2=read();
		must[x]=opt1,must[y]=opt2;
		dfs(1,0);
		ll out=min(must[1]==0?((ll)INF<<10):f[1][1],must[1]==1?(ll)(INF<<10):f[1][0]);
		printf("%lld\n",out>((ll)INF<<5)?-1:out);
		must[x]=-1;
		must[y]=-1;
	}
}
