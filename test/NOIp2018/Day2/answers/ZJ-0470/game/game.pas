var
n,m,t,i:longint;
s:int64;
begin
  assign(input,'game.in');reset(input);
  assign(output,'game.out');rewrite(output);
  read(n,m);
  if n>m then begin t:=n;n:=m;m:=t;end;
  if (n=1) and (m=1) then s:=1;
  if (n=1) and (m=2) then s:=4;
  if (n=2) and (m=2) then s:=12;
  if (n=2) and (m=3) then s:=36;
  if (n=3) and (m=3) then s:=112;
  if (n=2) and (m>3) then
   begin
     s:=4;
     for i:=1 to m-2 do
      s:=(s*3) mod 1000000007;
   end;
  if (n=5) and (m=5) then s:=7136;
  writeln(s);
  close(input);close(output);
end.