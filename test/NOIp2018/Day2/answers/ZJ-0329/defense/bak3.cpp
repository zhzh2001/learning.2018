#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>

const int MAXN = 100010;
typedef long long LL;
int head[MAXN], nxt[MAXN << 1], to[MAXN << 1], tot;
inline void addedge(int b, int e) {
	nxt[++tot] = head[b]; head[b] = tot; to[tot] = e;
	nxt[++tot] = head[e]; head[e] = tot; to[tot] = b;
}
int val[MAXN], n, t1, t2, t3, t4, Q, fa[MAXN];
LL f[MAXN][2], f2[MAXN][2];
const LL INF = 0x3f3f3f3f3f3f3f3fLL;
inline void getmin(LL & x, const LL y) {
	if (x > y) x = y;
}
void dfs(int u) {
	f[u][0] = 0;
	f[u][1] = val[u];
	for (int i = head[u]; i; i = nxt[i]) 
		if (to[i] != fa[u]) {
			fa[to[i]] = u;
			dfs(to[i]);
			f[u][0] += f[to[i]][1];
			f[u][1] += std::min(f[to[i]][0], f[to[i]][1]);
			if (f[u][0] >= INF) f[u][0] = INF;
			if (f[u][1] >= INF) f[u][1] = INF;
		}
}
void make(int u) {
	while (u) {
		if (u == t1) {
			if (t2 == 1) f2[u][0] = INF;
			else f2[u][1] = INF;
		} if (u == t3) {
			if (t4 == 1) f2[u][0] = INF;
			else f2[u][1] = INF;
		}
		if (f[fa[u]][1] != INF) {
			f2[fa[u]][1] = f[fa[u]][1] - std::min(f[u][0], f[u][1]) + std::min(f[u][0], f[u][1]);
		}
		if (f[fa[u]][0] != INF) {
			f2[fa[u]][0] = f[fa[u]][0] - f[u][1] + f[u][1];
		}
		if (f2[u][0] >= INF) f2[u][0] = INF;
		if (f2[u][1] >= INF) f2[u][1] = INF;
		u = fa[u];
	}
}
char buf[100];
inline LL turn(LL x) { return x >= INF ? -1 : x; }
int main() {
//	freopen("defense.in", "r", stdin);
//	freopen("defense.out", "w", stdout);
	scanf("%d%d%s", &n, &Q, buf);
	for (int i = 1; i <= n; ++i)
		scanf("%d", &val[i]);
	for (int i = 1; i != n; ++i) {
		scanf("%d%d", &t1, &t2);
		addedge(t1, t2);
	}
	dfs(1);
	while (Q --> 0) {
		scanf("%d%d%d%d", &t1, &t2, &t3, &t4);
		f2[t1][0] = f[t1][0]; f2[t1][1] = f[t1][1];
		f2[t2][0] = f[t2][0]; f2[t2][1] = f[t2][1];
		make(t1); make(t2);
		printf("%lld\n", turn(std::min(f2[1][0], f2[1][1])));
	}
	return 0;
}
