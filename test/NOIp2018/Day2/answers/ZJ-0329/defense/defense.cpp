#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>

const int MAXN = 100010;
typedef long long LL;
int head[MAXN], nxt[MAXN << 1], to[MAXN << 1], tot;
inline void addedge(int b, int e) {
	nxt[++tot] = head[b]; head[b] = tot; to[tot] = e;
	nxt[++tot] = head[e]; head[e] = tot; to[tot] = b;
}
int val[MAXN], n, t1, t2, t3, t4, Q;
LL f[MAXN][2];
const LL INF = 0x3f3f3f3f3f3f3f3fLL;
inline void getmin(LL & x, const LL y) {
	if (x > y) x = y;
}
void dfs(int u, int fa) {
	f[u][0] = 0;
	f[u][1] = val[u];
	for (int i = head[u]; i; i = nxt[i]) 
		if (to[i] != fa) {
			dfs(to[i], u);
			f[u][0] += f[to[i]][1];
			f[u][1] += std::min(f[to[i]][0], f[to[i]][1]);
		}
	if (u == t1) {
		if (t2 == 1) f[u][0] = INF;
		else f[u][1] = INF;
	} if (u == t3) {
		if (t4 == 1) f[u][0] = INF;
		else f[u][1] = INF;
	}
}
char buf[100];
inline LL turn(LL x) { return x >= INF ? -1 : x; }
int pre[MAXN][2], suc[MAXN][2];
int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d%s", &n, &Q, buf);
	for (int i = 1; i <= n; ++i)
		scanf("%d", &val[i]);
	for (int i = 1; i != n; ++i) {
		scanf("%d%d", &t1, &t2);
		addedge(t1, t2);
	}
//	if (*buf == 'A') {
//		pre[1][0] = 0; pre[1][1] = val[1];
//		suc[n][0] = 0; suc[n][1] = val[n];
//		for (int i = 2; i <= n; ++i) {
//			pre[i][0] += pre[
//		}
//		while (Q --> 0) {
//			scanf("%d%d%d%d", &t1, &t2, &t3, &t4);
//			if (t1 > t2) std::swap(t1, t2), std::swap(t3, t4);
//			if (t1 + 1 == t2) puts("-1");
//			else printf("%lld\n", );
//		}
//	} else 
	while (Q --> 0) {
		scanf("%d%d%d%d", &t1, &t2, &t3, &t4);
		dfs(1, 0);
		printf("%lld\n", turn(std::min(f[1][0], f[1][1])));
	}
	return 0;
}
