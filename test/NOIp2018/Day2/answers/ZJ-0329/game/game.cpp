#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>

const int Dollar1 = 1000000007;
typedef long long LL;
inline void add(int & x, const int y) { x += y; if (x >= Dollar1) x -= Dollar1; }
inline int up(const int x) { return x >= Dollar1 ? x - Dollar1 : x; }
inline int mul(const int x, const int y) {
	return static_cast<LL> (x) * y % Dollar1; 
}
inline int fastpow(int a, int b) {
	int res = 1;
	while (b) {
		if (b & 1) res = mul(res, a);
		a = mul(a, a);
		b >>= 1;
	}
	return res;
}
// int f[1000][8][2];
int n, m;
int map[10][10], ans = 0, lst[20], dep, now[20];
bool can; 
void dfs2(int x, int y) {
	if (x == n - 1 && y == m - 1) {
		bool x = true;
		for (int i = 1; i <= dep; ++i)
			if (now[i] != lst[i]) { x = now[i] > lst[i]; break; }
		memcpy(lst, now, sizeof now);
		can &= x;
		return ;
	}
	now[++dep] = map[x][y];
	if (y + 1 != m) dfs2(x, y + 1);
	if (!can) return ;
	if (x + 1 != n) dfs2(x + 1, y);
	if (!can) return ;
	--dep;
}
bool judge() {
	memset(lst, -1, sizeof lst);
	dep = 0;
	can = true;
	dfs2(0, 0);
	return can;
}
void dfs(int x, int y) {
	if (x == n) {
		ans += judge();
//		if (can) {
//		std::cerr << "FETCH : " << std::endl;
//		for (int i = 0; i != n; ++i, std::cerr << std::endl)
//			for (int j = 0; j != n; ++j)
//				std::cerr << map[i][j];
//		}
		return ;
	}
	map[x][y] = 0;
	if (y == m - 1) dfs(x + 1, 0); else dfs(x, y + 1);
	map[x][y] = 1;
	if (y == m - 1) dfs(x + 1, 0); else dfs(x, y + 1);
}
int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n == 1) {
		printf("%d\n", fastpow(2, m));
		return 0;
	}
	if (n == 2) {
		printf("%d\n", mul(fastpow(3, m - 1), 4));
		return 0;
	}
	if (n == 3) {
		if (m == 1) puts("8");
		else if (m == 2) puts("36");
		else printf("%d\n", mul(112, fastpow(3, m - 3)));
		return 0;
	} 
	if (n == 4) {
		if (m == 1) puts("16");
		else if (m == 2) puts("108");
		else if (m == 3) puts("336");
		else if (m == 4) puts("912");
		else printf("%d\n", mul(2688, fastpow(3, m - 5)));
		return 0;
	}
	if (n == 5) {
		if (m == 1) puts("32");
		else if (m == 2) puts("324");
		else if (m == 3) puts("1008");
		else if (m == 4) puts("2668");
		else if (m == 5) puts("7136");
		return 0;
	}
	// 把表打到8, 13即可AC
	dfs(0, 0);
	printf("%d\n", ans);
	return 0;
}
