#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>

const int Dollar1 = 1000000007;
typedef long long LL;
inline void add(int & x, const int y) { x += y; if (x >= Dollar1) x -= Dollar1; }
inline int up(const int x) { return x >= Dollar1 ? x - Dollar1 : x; }
inline int mul(const int x, const int y) {
	return static_cast<LL> (x) * y % Dollar1; 
}
int f[1000][8][2];
int n, m;
int main() {
	scanf("%d%d", &n, &m);
	for (int i = 0; i != n; ++i) f[0][i][0] = f[0][i][1] = 1;
	for (int i = 1; i != m; ++i) {
		f[i][0][0] = f[i][0][1] = up(f[i - 1][0][0] + f[i - 1][0][1]);
		for (int j = 1; j != n; ++j) {
			add(f[i][j][0], mul(f[i][j - 1][0], f[i - 1][j][0]));
			add(f[i][j][0], mul(f[i][j - 1][0], f[i - 1][j][1]));
			add(f[i][j][0], mul(f[i][j - 1][1], f[i - 1][j][1]));
			f[i][j][1] = f[i][j][0];
		}
		for (int j = 0; j != n; ++j) std::cerr << f[i][j][0] << " "; std::cerr << std::endl;
	}
	printf("%d\n", up(f[m - 1][n - 1][0] + f[m - 1][n - 1][1]));
	return 0;
}
