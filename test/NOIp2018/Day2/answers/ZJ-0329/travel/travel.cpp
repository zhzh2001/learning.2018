#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

const int MAXN = 5010;
int ansl[MAXN], bak, n, t1, t2, m, li[MAXN], fuc;
int head[MAXN], nxt[MAXN << 1], to[MAXN << 1], tot;
inline void addedge(int b, int e) {
	nxt[++tot] = head[b]; head[b] = tot; to[tot] = e;
}
std::vector<int> graph[MAXN];
bool vis[MAXN];
int xs[MAXN], ys[MAXN], cx, cy;
void dfs(int u) {
	if (vis[u]) return ;
	li[++bak] = u; vis[u] = true;
	if (fuc != 2 && li[bak] > ansl[bak]) {
		fuc = 1; 
		return ;
	} else if (li[bak] < ansl[bak]) fuc = 2;
	for (int i = head[u]; i; i = nxt[i]) {
		const int T = to[i];
		if (!vis[T]) {
			if (u == cx && T == cy) continue;
			if (u == cy && T == cx) continue;
			dfs(T);
			if (fuc == 1) return ;
		}
	}
}
int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; ++i) {
		scanf("%d%d", &xs[i], &ys[i]);
		graph[xs[i]].push_back(ys[i]);
		graph[ys[i]].push_back(xs[i]);
	}
	for (int i = 1; i <= n; ++i) {
		std::sort(graph[i].begin(), graph[i].end());
		for (int j = graph[i].size() - 1; j >= 0; --j)
			addedge(i, graph[i][j]);
	}
	if (n == m + 1) {
		memset(ansl, 0x3f, sizeof ansl);
		fuc = 0;
		dfs(1);
		memcpy(ansl, li, sizeof li);
	} else {
		memset(ansl, 0x3f, sizeof ansl);
		for (int i = 1; i <= m; ++i) {
			cx = xs[i]; cy = ys[i];
			bak = 0;
			for (int j = 1; j <= n; ++j) vis[j] = 0;
			fuc = 0;
			dfs(1);
			if (bak != n) continue;
			if (fuc != 1) for (int j = 1; j <= n; ++j) ansl[j] = li[j];
		}
	}
	for (int i = 1; i <= n; ++i) printf("%d%c", ansl[i], " \n"[i == n]);
	return 0;
}
