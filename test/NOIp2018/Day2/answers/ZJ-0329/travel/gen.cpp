#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>
using namespace std;
int fa[5050];
int main() {
	freopen("travel.in", "w", stdout);
	srand(time(0));
	int n = 5000, m = n - (rand() & 1);
	cout << n << " " << m << endl;
	for (int i = 1; i != n; ++i)
		cout << i + 1 << " " << (fa[i + 1] = rand() % i + 1) << endl;
	if (n == m) {
		int a, b;
		do {
			a = rand() % n + 1, b = rand() % n + 1;
			if (a > b) swap(a, b);
		} while (a == b || fa[b] == a) ;
		cout << a << " " << b << endl;
	}
	return 0;
}
