#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>

const int MAXN = 5010;
int can[MAXN], vis[MAXN], head[MAXN], nxt[MAXN << 1], to[MAXN << 1], tot;
inline void addedge(int b, int e) {
	nxt[++tot] = head[b]; head[b] = tot; to[tot] = e;
	nxt[++tot] = head[e]; head[e] = tot; to[tot] = b;
}
int ansl[MAXN], bak, n, t1, t2, m;
void make(int u) {
	vis[u] = true;
	for (int i = head[u]; i; i = nxt[i]) can[to[i]] = true; 
}
int main() {
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; ++i) {
		scanf("%d%d", &t1, &t2);
		addedge(t1, t2);
	}
	ansl[bak = 1] = 1;
	make(1);
	for (int i = 2; i <= n; ++i) {
		for (int j = 1; j <= n; ++j) std::cerr << can[j]; std::cerr << std::endl;
		for (int j = 1; j <= n; ++j) std::cerr << vis[j]; std::cerr << std::endl;
		std::cerr << std::endl;
		for (int j = 2; j <= n; ++j)
			if (can[j] && !vis[j]) {
				make(j);
				ansl[++bak] = j;
				break;
			}
	}
	for (int i = 1; i <= n; ++i) printf("%d ", ansl[i]);
	return 0;
}
