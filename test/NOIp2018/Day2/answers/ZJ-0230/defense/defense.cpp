#include<cstdio>
#include<iostream>
#include<cstdlib>
#include<vector>
#include<cstring>
#define inf 10000000000
using namespace std;
long long f[100010][2];//0:不选根,1:选根 
long long p[100010];
bool vis[100010];
short int must[100010];
vector<int>road[100010];
void dfs(int now)
{
	vis[now]=true;
	if(f[now][1]) return;
	long long res1=0,res2=p[now];
	int len=road[now].size();
	for(int i=0;i<len;i++)
	if(!vis[road[now][i]])
	{
		int nxt=road[now][i];
		dfs(nxt);
		res1+=f[nxt][1];
		if(res1>inf) res1=inf;
		res2+=min(f[nxt][0],f[nxt][1]);
		if(res2>inf) res2=inf;
	}
	if(must[now]==1) f[now][0]=inf;
	else f[now][0]=res1;
	if(must[now]==2) f[now][1]=inf;
	else f[now][1]=res2;
	
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m;
	char mode[3]="";
	scanf("%d%d%s",&n,&m,mode);
	for(int i=1;i<=n;i++) scanf("%lld",&p[i]);
	for(int i=1;i<n;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		road[x].push_back(y);
		road[y].push_back(x);
	}
	for(int i=0;i<m;i++)
	{
		memset(must,0,sizeof(must));
		memset(f,0,sizeof(f));
		memset(vis,0,sizeof(vis));
		int a,x,b,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		must[a]=x?1:2;
		must[b]=y?1:2;
		dfs(1);
		if(f[1][0]>=inf && f[1][1]>=inf) puts("-1");
		else printf("%lld\n",min(f[1][0],f[1][1]));
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

