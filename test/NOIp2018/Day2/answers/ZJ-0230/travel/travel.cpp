#include<cstdio>
#include<iostream>
#include<cstdlib>
#include<vector>
#include<algorithm>
#include<cstring>
using namespace std;
vector<int>road[5010];
vector<int>other[5010];
vector<bool>cut[5010];
int ans[5010],res[5010],n,cnt;
bool vis[5010];
void dfs(int now)
{
	ans[cnt++]=now;
	vis[now]=true;
	int len=road[now].size();
	for(int i=0;i<len;i++)
	if(!vis[road[now][i]] && !cut[now][i])
		dfs(road[now][i]);
	vis[now]=false;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int m;
	scanf("%d%d",&n,&m);
	for(int i=0;i<m;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		road[x].push_back(y);
		other[x].push_back(road[y].size()+1);
		cut[x].push_back(false);
		road[y].push_back(x);
		other[y].push_back(road[x].size());
		cut[y].push_back(false);
	}
	
	for(int i=1;i<=n;i++)
	sort(road[i].begin(),road[i].end());
	if(m==n-1)
	{
		dfs(1);
		for(int i=0;i<cnt;i++)
		printf("%d ",ans[i]);
	}
	else if(m==n)
	{
		for(int i=1;i<=n;i++)
		{
			int len=road[i].size();
			for(int j=0;j<len;j++)
			if(road[i][j]>i)
			{
				memset(ans,0,sizeof(ans));
				cut[i][j]=true;
				cut[road[i][j]][other[i][j]]=true;
				cnt=0;
				dfs(1);
				cut[i][j]=false;
				cut[road[i][j]][other[i][j]]=false;
				if(cnt<n) continue;
				//printf("cut:%d->%d\n",i,road[i][j]);
				bool ok=false;
				for(int k=0;k<n && !ok;k++)
				if(!res[k] || res[k]>ans[k]) ok=true;
				else if(res[k]<ans[k]) break;
				if(ok)
				{
					for(int k=0;k<n;k++) res[k]=ans[k];
				}
			}
		}
		for(int i=0;i<n;i++) printf("%d ",res[i]);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
