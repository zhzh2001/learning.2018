const
  mo=1000000007;
var
  n,m,i:longint;
  f:array[0..3,0..1000001]of int64;
  ans:int64;
begin
  assign(input,'game.in');
  reset(input);
  assign(output,'game.out');
  rewrite(output);
  read(n,m);
  f[0,2]:=4;
  f[1,2]:=4;
  f[2,2]:=2;
  f[3,2]:=2;
  if n=1 then
  begin
    ans:=1;
    for i:=1 to m do
      ans:=(ans*2)mod mo;
  end
  else if m=1 then
  begin
    ans:=1;
    for i:=1 to n do
      ans:=(ans*2)mod mo;
  end
  else if n=2 then
    begin
      for i:=3 to m do
        begin
        f[0,i]:=(f[0,i-1]+f[1,i-1]+f[3,i-1]+f[2,i-1])mod mo;
        f[1,i]:=(f[1,i-1]+f[2,i-1]+f[0,i-1]+f[3,i-1])mod mo;
        f[2,i]:=(f[3,i-1]+f[1,i-1])mod mo;
        f[3,i]:=(f[3,i-1]+f[1,i-1])mod mo;
        end;
      ans:=(f[0,m]+f[1,m]+f[2,m]+f[3,m])mod mo;
    end
  else if n=3 then
    begin
      if (m=2)then ans:=36
      else begin
              for i:=3 to m do
               begin
               f[0,i]:=(f[0,i-1]+f[1,i-1]+f[3,i-1]+f[2,i-1])mod mo;
               f[1,i]:=(f[1,i-1]+f[2,i-1]+f[0,i-1]+f[3,i-1])mod mo;
               f[2,i]:=(f[3,i-1]+f[1,i-1])mod mo;
               f[3,i]:=(f[3,i-1]+f[1,i-1])mod mo;
               end;
              ans:=(f[0,m]+f[1,m]+f[2,m]+f[3,m])mod mo;
              ans:=(ans*3)mod mo;
           end;
      if (m=3)then ans:=112;
    end
  else ans:=0;
  write(ans);
  close(input);
  close(output);
end.
