#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int maxn=5000+10;
inline ll read()
{
	ll f=1,x=0;
	char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=x*10+c-'0';c=getchar();}
	return f*x;
}
struct node
{
	ll to,next;
}e[maxn];
ll head[maxn],cnt=0,from[maxn],ans[maxn],cnt_ans=0;
ll n,m;
bool vis[maxn];
void add(ll f,ll to)
{
	cnt++;
	e[cnt].to=to;
	e[cnt].next=head[f];
	head[f]=cnt;
}
void work()
{
	priority_queue<ll>Q;
	vis[1]=true;
	cnt_ans++;
	ans[cnt_ans]=1;
	for(ll p=head[1];p;p=e[p].next)
	{
		Q.push(-e[p].to);
	}
	for(ll i=1;i<=n-1;i++)
	{
		ll now=-Q.top();
		Q.pop();
		vis[now]=true;
		cnt_ans++;
		ans[cnt_ans]=now;
		for(ll p=head[now];p;p=e[p].next)
		{
			if(!vis[e[p].to])
			{
				Q.push(-e[p].to);
			}
		}
	}
	for(ll i=1;i<=cnt_ans;i++)
	{
		cout<<ans[i]<<" ";
	}
}
void work2(ll x)
{
	cnt_ans++;
	ans[cnt_ans]=x;
	vis[x]=true;
	priority_queue<ll>Q;
	for(ll p=head[x];p;p=e[p].next)
	{if(!vis[e[p].to])Q.push(-e[p].to);}
	while(!Q.empty())
	{
		ll now=-Q.top();
		Q.pop();
		work2(now);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	ll x=0,y=0;
	for(ll i=1;i<=m;i++)
	{
		x=read();y=read();
		add(x,y);
		add(y,x);
	}
	if(m==n-1)
	{
		work2(1);
		for(ll i=1;i<=cnt_ans;i++)
		{cout<<ans[i]<<" ";}
	}
	return 0;
}