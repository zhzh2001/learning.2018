#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int maxn=100000+10;
inline ll read()
{
	ll f=1,x=0;
	char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=x*10+c-'0';c=getchar();}
	return f*x;
}
ll n,m,num;
char type;
struct node
{
	ll to,next;
}e[maxn];
ll head[maxn],cnt=0;
void add(ll f,ll to)
{
	cnt++;
	e[cnt].to=to;
	e[cnt].next=head[f];
	head[f]=cnt;
}
ll cost[maxn];
ll dp[maxn][3];
ll worka(ll a,ll x,ll b,ll y)
{
	if(abs(a-b)==1 && x==0 && y==0)return -1;
	ll a1=0,a2=0,b1=0,b2=0;
	if(x==0){a1=a-1;a2=a+1;}
	if(y==0){b1=b-1;b2=b+1;}
	for(int i=1;i<=n;i++)
	{
		if(i!=a && i!=b && i!=a1 && i!=a2 && i!=b1 && i!=b2)
		{
			dp[i][0]=dp[i-1][1]+cost[i];
			dp[i][1]=min(dp[i-1][0],dp[i-1][2]);
			dp[i][2]=dp[i][1]+cost[i];
		}
		if(i==a)
		{
			if(x==1)
			{
				dp[i][0]=dp[i-1][1]+cost[i];
				dp[i][1]=dp[i-1][0]+cost[i];
				dp[i][2]=min(dp[i-1][0],dp[i-1][2])+cost[i];
			}
			if(x==0)
			{
				dp[i][0]=dp[i-1][1];
				dp[i][1]=dp[i-1][0];
				dp[i][2]=min(dp[i-1][0],dp[i-1][2]);
			}
		}
		if(i==b)
		{
			if(y==1)
			{
				dp[i][0]=dp[i-1][1]+cost[i];
				dp[i][1]=dp[i-1][0]+cost[i];
				dp[i][2]=min(dp[i-1][0],dp[i-1][2])+cost[i];
			}
			if(y==0)
			{
				dp[i][0]=dp[i-1][1];
				dp[i][1]=dp[i-1][0];
				dp[i][2]=min(dp[i-1][0],dp[i-1][2]);
			}
		}
		if(i==a1 || i==a2 || i==b1 || i==b2)
		{
			dp[i][0]=dp[i-1][1]+cost[i];
			dp[i][1]=dp[i-1][0]+cost[i];
			dp[i][2]=min(dp[i-1][0],dp[i-1][2])+cost[i];
		}
	}
	ll mint=min(dp[n][0],dp[n][1]);
	mint=min(mint,dp[n][2]);
	return mint;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%lld %lld %c %lld",&n,&m,&type,&num);
	for(ll i=1;i<=n;i++)cost[i]=read();
	ll x=0,y=0,a=0,b=0;
	for(ll i=1;i<n;i++)
	{
		x=read();y=read();
		add(x,y);
		add(y,x);
	}
	for(ll i=1;i<=m;i++)
	{
		a=read();x=read();b=read();y=read();
		if(type=='A')cout<<worka(a,x,b,y)<<endl;
	}
	return 0;
}