#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
#define gc() getchar()
template <typename tp>
inline void read(tp& x) {
  x = 0; char tmp; bool key = 0;
  for (tmp = gc() ; !isdigit(tmp) ; tmp = gc())
    key = (tmp == '-');
  for ( ; isdigit(tmp) ; tmp = gc())
    x = (x << 3) + (x << 1) + tmp - '0';
  if (key) x = -x;
}
const int N = 5010;
struct data {
  int x,y;
} dat[N];
vector<int> ch[N];
int vis[N],lop[N],cnt,rt,n,m;
int getlop(int pos,int fa) {
  if (vis[pos]) {
    rt = pos;
    return 1;
  }
  vis[pos] = 1;
  int tmp;
  for (int t = 0 ; t < (int)ch[pos].size() ; ++ t) {
    int v = ch[pos][t];
    if (v == fa) continue;
    tmp = getlop(v,pos);
    if (tmp) {
      if (tmp == 1) {
	lop[++cnt] = pos;
	if (pos != rt) return 1;
      }
      return 2;
    }
  }
  return 0;
}
int dfn[N],dcnt,ans[N],curx,cury;
void dfs(int pos,int fa) {
  dfn[++dcnt] = pos;
  int v;
  for (int t = 0 ; t < (int)ch[pos].size() ; ++ t) {
    v = ch[pos][t];
    if (v == fa || (pos == curx && v == cury) || (pos == cury && v == curx))
      continue;
    dfs(v,pos);
  }
}
void doit() {
  dcnt = 0;
  dfs(1,0);
  int key = 0;
  for (int i = 1 ; i <= n ; ++ i) {
    if (ans[i] != dfn[i]) {
      if (ans[i] > dfn[i]) key = 1;
      break;
    }
  }
  if (key)
    for (int i = 1 ; i <= n ; ++ i)
      ans[i] = dfn[i];
}
int main() {
  int x,y;
  read(n), read(m);
  for (int i = 1 ; i <= m ; ++ i) {
    read(x), read(y);
    if (x > y) swap(x,y);
    dat[i] = (data) {x,y};
    ch[x].push_back(y);
    ch[y].push_back(x);
  }
  for (int i = 1 ; i <= n ; ++ i)
    ans[i] = n + 1, sort(ch[i].begin(),ch[i].end());
  if (m == n-1) {
    doit();
    for (int i = 1 ; i <= n ; ++ i)
      printf("%d ",ans[i]);
    puts("");
    return 0;
  }
  getlop(1,0);
  for (int i = 1 ; i <= cnt ; ++ i) {
    curx = lop[i], cury = lop[(i + cnt - 2) % cnt + 1];
    doit();
  }
  for (int i = 1 ; i <= n ; ++ i)
    printf("%d ",ans[i]);
  puts("");
  return 0;
}
