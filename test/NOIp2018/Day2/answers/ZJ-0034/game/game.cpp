#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
#define gc() getchar()
template <typename tp>
inline void read(tp& x) {
  x = 0; char tmp; bool key = 0;
  for (tmp = gc() ; !isdigit(tmp) ; tmp = gc())
    key = (tmp == '-');
  for ( ; isdigit(tmp) ; tmp = gc())
    x = (x << 3) + (x << 1) + tmp - '0';
  if (key) x = -x;
}
int n,m;
namespace task1 {
  const int N = 10;
  int mat[N][N];
  string s[N * N];
  int ans,cnt;
  void fsd(int x,int y,string cur) {
    cur += mat[x][y] + '0';
    if (x == n && y == m) {
      s[++cnt] = cur;
      return;
    }
    if (y != m) fsd(x,y+1,cur);
    if (x != n) fsd(x+1,y,cur);
  }
  bool check() {
    for (int i = 1 ; i <= n * m ; ++ i)
      s[i] = "";
    cnt = 0;
    fsd(1,1,"");
    for (int i = 1 ; i < cnt ; ++ i) {
      if (s[i] > s[i+1]) return 0;
    }
    return 1;
  }
  void output() {
    for (int i = 1 ; i <= n ; ++ i, putchar('\n'))
      for (int j = 1 ; j <= m ; ++ j)
	cerr << mat[i][j];
    puts("");
  }
  void dfs(int x,int y) {
    if (y > m) {
      dfs(x+1,1);
      return;
    }
    if (x > n) {
      if (check()) {
	//       output();
	++ ans;
      }
      return;
    }
    for (int i = 0 ; i < 2 ; ++ i) { 
      mat[x][y] = i;
      dfs(x,y+1);
    }
  }
  int main() {
    dfs(1,1);
    cout << ans << endl;
    return 0;
  }  
}
namespace task2 {
  const int N = 4, MOD = (int)(1e9 + 7), M = 1000010;
  int dp[M][N][1 << 2],ans;
  int main() {
    for (int s = 0 ; s < (1 << n) ; ++ s)
      dp[1][0][s] = 1;
    for (int i = 2 ; i <= m ; ++ i) {
      for (int j = 0 ; j < n ; ++ j) {
	for (int s = 0 ; s < (1 << n) ; ++ s) {
	  for (int p = j ; p < n ; ++ p) {
	    if (p > 0) {
	      int v = (s & ((1 << n >> 1) - 1)) >> (p-1);
	      if ((v >> 1) == ((1 << (n - p - 1)) - 1)) {
		int tmp = s & ((1 << p >> 1) - 1);
		(dp[i][p][(tmp << 1) | ((v&1) * (1 << p))] += dp[i-1][j][s]) %= MOD;
		(dp[i][p][(tmp << 1) | 1 | ((v&1) * (1 << p))] += dp[i-1][j][s]) %= MOD;
		if (p == j && (v&1) == 1) {
		  (dp[i][p][tmp << 1] += dp[i-1][j][s]) %= MOD;
		  (dp[i][p][(tmp << 1) | 1] += dp[i-1][j][s]) %= MOD;
		}
	      }
	    } else {
	      if ((s & ((1 << n >> 1) - 1)) != (1 << n >> 1) - 1) continue;
	      (dp[i][p][0] += dp[i-1][j][s]) %= MOD;
	      (dp[i][p][1] += dp[i-1][j][s]) %= MOD;
	    }
	  }
	}
      }
    }
    for (int j = 0 ; j < n ; ++ j)
      for (int s = 0 ; s < (1 << n) ; ++ s)
	(ans += dp[m][j][s]) %= MOD;
    printf("%d\n",ans);
    return 0;
  }
}
int main() {
  freopen("game.in","r",stdin);
  freopen("game.out","w",stdout);
  read(n), read(m);
  if (n <= 4 && m <= 4) task1::main();
  else if (n <= 2) task2::main();
  else;
  return 0;
}
