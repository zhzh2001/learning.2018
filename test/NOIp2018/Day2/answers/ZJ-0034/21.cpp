#include <bits/stdc++.h>
using namespace std;
const int N = 10;
int mat[N][N];
string s[N * N];
int n,m,ans,cnt;
void fsd(int x,int y,string cur) {
  cur += mat[x][y] + '0';
  if (x == n && y == m) {
    s[++cnt] = cur;
    return;
  }
  if (y != m) fsd(x,y+1,cur);
  if (x != n) fsd(x+1,y,cur);
}
bool check() {
  for (int i = 1 ; i <= n * m ; ++ i)
    s[i] = "";
  cnt = 0;
  fsd(1,1,"");
  for (int i = 1 ; i < cnt ; ++ i) {
    if (s[i] > s[i+1]) return 0;
  }
  return 1;
}
void output() {
  for (int i = 1 ; i <= n ; ++ i, putchar('\n'))
    for (int j = 1 ; j <= m ; ++ j)
      cerr << mat[i][j];
  puts("");
}
void dfs(int x,int y) {
  if (y > m) {
    dfs(x+1,1);
    return;
  }
  if (x > n) {
    if (check()) {
      output();
      ++ ans;
    }
    return;
  }
  for (int i = 0 ; i < 2 ; ++ i)
    mat[x][y] = i, dfs(x,y+1);
}
int main() {
  cin >> n >> m;
  dfs(1,1);
  cout << ans << endl;
  return 0;
}
