#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;
#define gc() getchar()
template <typename tp>
inline void read(tp& x) {
  x = 0; char tmp; bool key = 0;
  for (tmp = gc() ; !isdigit(tmp) ; tmp = gc())
    key = (tmp == '-');
  for ( ; isdigit(tmp) ; tmp = gc())
    x = (x << 3) + (x << 1) + tmp - '0';
  if (key) x = -x;
}
const int N = 100010;
typedef long long ll;
const ll INF = 0x3f3f3f3f3f3f3f3f;
int n,m,val[N];
namespace A1 {
  struct edge {
    int la,b,v;
  };
  typedef long long ll;
  typedef pair<ll,int> pli;
  struct cmp {
    bool operator () (const pli& a,const pli& b) {
      return a.first > b.first;
    }
  };
  priority_queue<pli,vector<pli>,cmp> q;
  struct graph {
    edge con[N << 1];
    int tot,fir[N],st;
    void add(int from,int to,int val) {
      con[++tot] = (edge) {fir[from],to,val};
      fir[from] = tot;
    }
    ll dis[N];
    void dijkstra() {
      memset(dis,0x3f,sizeof dis);
      dis[st] = 0;
      q.push(pli(dis[st],st));
      for (int pos ; !q.empty() ; ) {
	pos = q.top().second;
	ll tmp = q.top().first;
	q.pop();
	if (tmp != dis[pos]) continue;
	for (int i = fir[pos] ; i ; i = con[i].la) {
	  if (dis[con[i].b] > dis[pos] + con[i].v) {
	    dis[con[i].b] = dis[pos] + con[i].v;
	    q.push(pli(dis[con[i].b],con[i].b));
	  }
	}
      }
    }
  } g1,g2;
  void add_edge(int x,int y,int z) {
    g1.add(x,y,z);
    g2.add(y,x,z);
  }
  struct node {
    ll mn;
  } t[N << 2];
  void build(int x=1,int lp=0,int rp=n+1) {
    t[x].mn = INF;
    if (lp == rp) return;
    int mid = (lp + rp) >> 1;
    build(x<<1,lp,mid);
    build(x<<1|1,mid+1,rp);
  }
  void modify(int l,int r,ll v,int x=1,int lp=0,int rp=n+1) {
    if (l > rp || lp > r) return;
    if (lp >= l && rp <= r) {
      t[x].mn = min(t[x].mn,v);
      return;
    }
    int mid = (lp + rp) >> 1;
    modify(l,r,v,x<<1,lp,mid);
    modify(l,r,v,x<<1|1,mid+1,rp);
  }
  ll query(int p,int x=1,int lp=0,int rp=n+1) {
    if (lp == rp) return t[x].mn;
    int mid = (lp + rp) >> 1;
    ll ret = t[x].mn;
    if (p <= mid) ret = min(ret,query(p,x<<1,lp,mid));
    else ret = min(ret,query(p,x<<1|1,mid+1,rp));
    return ret;
  }
  void doit(int x,int y,int z) {
    if (x + 1 > y - 1) return;
    modify(x + 1,y - 1,0ll + z + g1.dis[x] + g2.dis[y]);
  }
  int main() {
    int x,y,u,v;
    int tst = 0, ten = n + 1;
    g1.st = tst;
    g2.st = ten;
    for (int i = 1 ; i <= n ; ++ i) {
      if (i - 1 <= 0) add_edge(tst,i,val[i]);
      else add_edge(i-1,i,val[i]);
      if (i - 2 <= 0);
      else add_edge(i-2,i,val[i]);
      if (i + 1 > n) add_edge(i,ten,0);
      if (i + 2 > n) add_edge(i,ten,0);
    }
    g1.dijkstra();
    g2.dijkstra();
    build();
    for (int i = 1 ; i <= n ; ++ i) {
      if (i - 1 <= 0) doit(tst,i,val[i]);
      else doit(i-1,i,val[i]);
      if (i - 2 <= 0);
      else doit(i-2,i,val[i]);
      if (i + 1 > n) doit(i,ten,0);
      if (i + 2 > n) doit(i,ten,0);
    }
    for (int i = 1 ; i <= m ; ++ i) {
      read(u), read(x), read(v), read(y);
      if (y == 1) {
	printf("%lld\n",g1.dis[v] + g2.dis[v]);
      } else {
	printf("%lld\n",query(v));
      }
    }
    return 0;
  }
}
struct edge {
  int la,b;
} con[N << 1];
int tot,fir[N];
void add(int from,int to) {
  con[++tot] = (edge) {fir[from],to};
  fir[from] = tot;
}
ll dp[N][2];
int ned[N],key;
char type[10];
void dfs(int pos,int fa) {
  dp[pos][0] = 0, dp[pos][1] = val[pos];
  for (int i = fir[pos] ; i ; i = con[i].la) {
    if (con[i].b == fa) continue;
    dfs(con[i].b,pos);
    dp[pos][1] += min(dp[con[i].b][0],dp[con[i].b][1]);
    dp[pos][0] += dp[con[i].b][1];
  }
  if (~ned[pos]) {
    dp[pos][ned[pos]^1] = INF;
  }
  dp[pos][0] = min(dp[pos][0],INF);
  dp[pos][1] = min(dp[pos][1],INF);
  if (dp[pos][0] == INF && dp[pos][1] == INF)
    key = 0;
}
void doit() {
  key = 1;
  memset(dp,0x3f,sizeof dp);
  dfs(1,0);
  ll ans = min(dp[1][0],dp[1][1]);
  if (ans >= INF || (!key)) puts("-1");
  else printf("%lld\n",ans);
}
int main() {
  int x,y,u,v;
  read(n), read(m);
  scanf("%s",type);
  for (int i = 1 ; i <= n ; ++ i)
    read(val[i]);
  for (int i = 1 ; i < n ; ++ i) {
    read(x), read(y);
    add(x,y);
    add(y,x);
  }
  memset(ned,-1,sizeof ned);
  for (int i = 1 ; i <= m ; ++ i) {
    read(u), read(x), read(v), read(y);
    if (u == v && x != y) {
      puts("-1");
      continue;
    }
    ned[u] = x, ned[v] = y;
    doit();
    ned[u] = -1, ned[v] = -1;
  }
  return 0;
}
