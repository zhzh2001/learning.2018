#include<bits/stdc++.h>
#define rep(i, x, y) for(register int i = x; i <= y; ++i)
#define repd(i, x, y) for(register int i = x; i >= y; --i;
#define repl(i, x) for(register int i = fi[x]; i; i = l[i].next)
using namespace std;
typedef long long ll;
const int maxn = 100010;
const ll INF = 1LL<<60;
struct line{
	int y, next;
}l[maxn<<1];
struct node{
	ll a[2][2];
}t[maxn<<2];
int n, m, w[maxn];
int lcnt, fi[maxn];
int flag[maxn];
ll f[maxn][2];
char typ[5];
int read(){
	int num = 0, flag = 1; char c = getchar();
	for(; c < '0' || c > '9'; c = getchar())
		if(c == '-') flag = 0;
	for(; c >= '0' && c <= '9'; c = getchar())
		num = (num<<3)+(num<<1)+c-48;
	return flag? num: -num;
}
void make_line(int x, int y){
	l[++lcnt].y = y; l[lcnt].next = fi[x]; fi[x] = lcnt;
}

void init(){
	n = read(); m = read();
	scanf("%s", typ+1);
	rep(i, 1 ,n) w[i] = read();
	rep(i, 1, n-1){
		int x = read(), y = read();
		make_line(x, y); make_line(y, x);
	}
}

void get_f(int x, int fa){
	f[x][1] = f[x][0] = 0;
	ll sum_0 = 0, sum_1 = 0;
	int flag_0 = 1, flag_1 = 1;
	repl(i, x){
		int v = l[i].y;
		if(v == fa) continue;
		get_f(v, x);
		if(f[v][1] < INF) sum_0 += f[v][1];
		else flag_0 = 0;
		if(min(f[v][0], f[v][1]) < INF) sum_1 += min(f[v][0], f[v][1]);
		else flag_1 = 0;
	}
	if(flag_0 && flag[x] != 1) f[x][0] = sum_0;
	else f[x][0] = INF;
	if(flag_1 && flag[x] != -1) f[x][1] = sum_1+w[x];
	else f[x][1] = INF;
}

void work_1(){
	while(m--){
		int a = read(), x = read(), b = read(), y = read();
		flag[a] = x? 1: -1;
		flag[b] = y? 1: -1;
		get_f(1, 0);
		ll ans = min(f[1][0], f[1][1]);
		printf("%lld\n", ans < INF? ans: -1LL);
		flag[a] = flag[b] = 0;
	}
}

node upd(node x, node y){
	node res;
	res.a[0][0] = min(INF, x.a[0][0]+y.a[1][0]);
	res.a[0][0] = min(res.a[0][0], x.a[0][1]+min(y.a[0][0], y.a[1][0]));
	res.a[0][1] = min(INF, x.a[0][0]+y.a[1][1]);
	res.a[0][1] = min(res.a[0][1], x.a[0][1]+min(y.a[0][1], y.a[1][1]));
	res.a[1][0] = min(INF, x.a[1][0]+y.a[1][0]);
	res.a[1][0] = min(res.a[1][0], x.a[1][1]+min(y.a[0][0], y.a[1][0]));
	res.a[1][1] = min(INF, x.a[1][0]+y.a[1][1]);
	res.a[1][1] = min(res.a[1][1], x.a[1][1]+min(y.a[0][1], y.a[1][1]));
	return res;
}

void build(int k, int l, int r){
	if(l == r){
		t[k].a[0][0] = 0;
		t[k].a[1][1] = w[l];
		t[k].a[1][0] = t[k].a[0][1] = INF;
		return;
	}
	int mid = (l+r)>>1;
	build(k<<1, l, mid); build(k<<1|1, mid+1, r);
	t[k] = upd(t[k<<1], t[k<<1|1]);
}

ll query(int k, int l, int r, int L, int R, int x, int y){
	if(L > R) return 0;
	if(L > r || R < l) return 0;
	if(l == L && R == r) return t[k].a[x][y];
	int mid = (l+r)>>1;
	if(R <= mid) return query(k<<1, l, mid, L, R, x, y);
	if(L > mid) return query(k<<1|1, mid+1, r, L, R, x, y);
	ll res = query(k<<1, l, mid, L, min(mid, R), x, 0)
			+query(k<<1|1, mid+1, r, max(L, mid+1), R, 1, y);
	res = min(res, query(k<<1, l, mid, L, min(mid, R), x, 1)
			+query(k<<1|1, mid+1, r, max(L, mid+1), R, 0, y));
	res = min(res, query(k<<1, l, mid, L, min(mid, R), x, 1)
			+query(k<<1|1, mid+1, r, max(L, mid+1), R, 1, y));
	return res;
}

void work_2(){
	build(1, 1, n);
	while(m--){
		int a = read(), x = read(), b = read(), y = read();
		if(a > b) swap(a, b), swap(x, y);
		if(b == a+1 && x+y == 0){
			puts("-1"); continue;
		}
		ll ans = 0;
		if(a != 1) ans += min(query(1, 1, n, 1, a, 0, x), query(1, 1, n, 1, a, 1, x));
		else ans += x? w[1]: 0;
		if(b != n) ans += min(query(1, 1, n, b, n, y, 0), query(1, 1, n, b, n, y, 1));
		else ans += y? w[n]: 0;
		ll mi = INF;
		if(x && y){
			mi = min(mi, query(1, 1, n, a+1, b-1, 0, 0));
			mi = min(mi, query(1, 1, n, a+1, b-1, 0, 1));
			mi = min(mi, query(1, 1, n, a+1, b-1, 1, 0));
			mi = min(mi, query(1, 1, n, a+1, b-1, 1, 1));
		}
		if(x == 0 && y){
			mi = min(mi, query(1, 1, n, a+1, b-1, 1, 0));
			mi = min(mi, query(1, 1, n, a+1, b-1, 1, 1));
		}
		if(x && y == 0){
			mi = min(mi, query(1, 1, n, a+1, b-1, 0, 1));
			mi = min(mi, query(1, 1, n, a+1, b-1, 1, 1));
		}
		if(x == 0 && y == 0)
			mi = min(mi, query(1, 1, n, a+1, b-1, 1, 1));
		printf("%lld\n", ans+mi);
	}
}

int main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	init();
	if(n <= 2000) work_1();
	else if(typ[1] == 'A') work_2();
	return 0;
}
