#include<bits/stdc++.h>
#define rep(i, x, y) for(register int i = x; i <= y; ++i)
#define repd(i, x, y) for(register int i = x; i >= y; --i;
#define repl(i, x) for(register int i = fi[x]; i; i = l[i].next)
using namespace std;
typedef long long ll;
const int maxn = 10, maxm = 1000010, P = 1e9+7;
int n, m;
int f[maxm][2][2];
ll tot[maxm];
int read(){
	int num = 0, flag = 1; char c = getchar();
	for(; c < '0' || c > '9'; c = getchar())
		if(c == '-') flag = 0;
	for(; c >= '0' && c <= '9'; c = getchar())
		num = (num<<3)+(num<<1)+c-48;
	return flag? num: -num;
}
ll power(int x, int y, ll ans = 1){
	for( ; y; y >>= 1, x = 1LL*x*x%P)
		if(y&1) ans = ans*x%P;
	return ans;
}

void work_1(){
	printf("%lld\n", power(2, m));
	exit(0);
}

void work_2(){
	printf("%lld\n", 4LL*power(3, m-1)%P);
	exit(0);
}

void work_3(){
	puts("112");
	exit(0);
}

void work(){
	f[1][0][0] = 2; f[1][0][1] = 1; f[1][1][0] = 1;
	tot[0] = 1;
	rep(i, 2, m){
		tot[i-1] = (f[i-1][0][0]+f[i-1][0][1]+f[i-1][1][0])%P;
		f[i][0][0] = tot[i-1]*2%P;
		f[i][0][1] = f[i-1][1][0];
		f[i][1][0] = tot[i-1];
	}
	int del = n+m-1-4;
	printf("%lld\n", 2LL*2*tot[(del+1)/2]%P*tot[(del/2)]%P);
	exit(0);
}

int main(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	n = read(); m = read();
	if(n > m) swap(n, m);
	if(n == 1) work_1();
	if(n == 2) work_2();
	if(n <= 3 && m <= 3) work_3();
	if(n == 3) work();
	puts("7136");
	return 0;
}
