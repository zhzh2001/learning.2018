#include<bits/stdc++.h>
#define rep(i, x, y) for(register int i = x; i <= y; ++i)
#define repd(i, x, y) for(register int i = x; i >= y; --i;
#define repl(i, x) for(register int i = fi[x]; i; i = l[i].next)
#define p_b push_back
using namespace std;
const int maxn = 5010;
struct line{
	int y, id, next;
}l[maxn<<1];
int n, m, lcnt, fi[maxn], x[maxn], y[maxn];
int top, st[maxn], vis[maxn], flag[maxn];
int fa[maxn], cle[maxn];
int len, ans[maxn], now[maxn];
int ok[maxn][maxn];
vector<int> son[maxn];
int read(){
	int num = 0, flag = 1; char c = getchar();
	for(; c < '0' || c > '9'; c = getchar())
		if(c == '-') flag = 0;
	for(; c >= '0' && c <= '9'; c = getchar())
		num = (num<<3)+(num<<1)+c-48;
	return flag? num: -num;
}
void make_line(int x, int y, int id){
	l[++lcnt].y = y; l[lcnt].next = fi[x];
	fi[x] = lcnt; l[lcnt].id = id;
}

void init(){
	n = read(); m = read();
	rep(i, 1, m){
		x[i] = read(); y[i] = read();
		make_line(x[i], y[i], i);
		make_line(y[i], x[i], i);
	}
}

void get_son(int x, int f){
	repl(i, x){
		int v = l[i].y;
		if(v == f) continue;
		get_son(v, x);
		son[x].p_b(v);
	}
	sort(son[x].begin(), son[x].end());
}

void get_ans(int x, int f){
	printf("%d ", x);
	rep(i, 0, (int)son[x].size()-1)
		get_ans(son[x][i], x);
}

void work_1(){
	get_son(1, 0);
	get_ans(1, 0);
}

void get_cir(int x, int f){
	if(vis[x]){
		for(int i = top; i; --i){
			flag[st[i]] = 1;
			if(st[i] == x) break;
		}
		return;
	}
	vis[x] = 1; st[++top] = x;
	repl(i, x){
		int v = l[i].y;
		if(v == f) continue;
		get_cir(v, x);
	}
	vis[x] = 0; --top;
}

void get_res(int x, int f){
	now[++len] = x;
	rep(i, 0, (int)son[x].size()-1)
		if(!ok[x][son[x][i]] && son[x][i] != f)
			get_res(son[x][i], x);
}

void check(){
	if(!ans[1]){
		rep(i, 1, n) ans[i] = now[i];
		return;
	}
	rep(i, 1, n){
		if(ans[i] == now[i]) continue;
		if(ans[i] < now[i]) return;
		break;
	}
	rep(i, 1, n) ans[i] = now[i];
}

void work_2(){
	rep(x, 1, n){
		repl(i, x) son[x].p_b(l[i].y);
		sort(son[x].begin(), son[x].end());
	}
	get_cir(1, 0);
	rep(i, 1, n-1){
		if(flag[x[i]] && flag[y[i]]){
			ok[x[i]][y[i]] = ok[y[i]][x[i]] = 1;
			len = 0;
			get_res(1, 0);
			ok[x[i]][y[i]] = ok[y[i]][x[i]] = 0;
			check();
		}
	}
	rep(i, 1, n) printf("%d ", ans[i]);
}

int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	init();
	if(m == n-1) work_1();
	else work_2();
	return 0;
}
