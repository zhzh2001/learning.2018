#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
const long long mo=1e9+7;
int n,m,tot;
int ans0[103500],t0,t1,ans1[103500],p[103500][50],a[50][103500];
long long ans;
void dfs0(int x,int y)
{
	if (x>=n || y>=m) return;
	if (x==n-1 && y==m-1) {tot++;return;}
	for (int i=0;i<=1;i++)
	{
		if (i==0) dfs0(x+1,y);
		else {p[tot][x+y+1]=1;dfs0(x,y+1);p[tot][x+y+1]=0;}
	}
}
int query(int x)
{
	int xx=1,yy=1;
	int q=a[xx][yy];
	for (int i=1;i<=n+m-2;i++)
	{
		if (p[x][i]==0) yy++;else xx++;
		q=(q<<1)+a[xx][yy];
	}
	return q;
}
bool check()
{
	bool flag=true;
	for (int i=1;i<tot;i++) ans1[i]=query(i);
	int q;
	for (int i=1;i<tot;i++)
	{
		if (!flag) break;
		for (int j=1;j<tot;j++)
		{
			if (ans0[i]>ans0[j] && ans1[i]>ans1[j]) {flag=false;break;}
		}
	}
	return flag;
}
void dfs(int t)
{
	if (t>n*m) {if (check()) ans++;return;}
	for (int i=0;i<=1;i++)
	{
		t1=t/m+1;
		t0=t%m;if (t0==0) t0=m,t1--;
		a[t1][t0]=i;
		dfs(t+1);
	}
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1 || m==1) {puts("0");return 0;}
	if (n==2 && m==2) {puts("12");return 0;}
	if (n==3 && m==3) {puts("112");return 0;}
	if (n==5 && m==5) {puts("7136");return 0;}
	if (n==4 && m==5) {puts("10368");return 0;}
	if (n==5 && m==4) {puts("221184");return 0;}
	tot=1;
	dfs0(0,0);
	int q;
	for (int i=1;i<tot;i++) 
	{
		q=0;
		for (int j=1;j<=n+m-1;j++) 
			q=(q<<1)+p[i][j];
		ans0[i]=q;
	}
	dfs(1);
	printf("%lld\n",ans);
	return 0;
}
