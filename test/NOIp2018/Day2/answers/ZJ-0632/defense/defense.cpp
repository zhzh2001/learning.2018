#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
const long long mo=1e9+7;
int n,m,a,xa,b,yb,min0,cnt,t[103500],nxt[203500],to[203500],head[203500],p[103500],xx[103500],yy[103500];
char st[1000];
void add(int x,int y)
{
	cnt++;nxt[cnt]=head[x];to[cnt]=y;head[x]=cnt;
}
bool check()
{
	bool flag=true;
	for (int i=1;i<=n-1;i++)
		if (t[xx[i]]==0 && t[yy[i]]==0) {flag=false;break;}
	if (t[a]!=xa || t[b]!=yb) flag=false;
	return flag;
	
}
void dfs(int x,int ans)
{
	if (x>n){if (check()) min0=min(min0,ans); 
	return;} 
	if (ans>min0) return;
	for (int i=0;i<=1;i++)
	{
		if (!i) {dfs(x+1,ans);}
		else {t[x]=1;dfs(x+1,ans+p[x]);t[x]=0;}
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);scanf("%s",st+1);
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<=n-1;i++)
	{
		scanf("%d%d",&xx[i],&yy[i]);
		add(xx[i],yy[i]);add(yy[i],xx[i]);
	}
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&a,&xa,&b,&yb);
		min0=1e9+7;
		dfs(1,0);if (min0!=1e9+7) printf("%d\n",min0);else puts("-1");	
	}
	return 0;
}
