#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 11000
#define M 21000

bool vis[N];
int l[N],r[N],s[N],head[N],vet[M],Next[M],t,n,m,x,y,tot;

void add(int x,int y){
	Next[++tot]=head[x];
	head[x]=tot;
	vet[tot]=y;
}

void qsort(int l,int r){
	int i=l,j=r,x=s[(l+r)>>1];
	while (1){
		while (s[i]<x) i++;
		while (x<s[j]) j--;
		if (i<=j){
			int y=s[i];
			s[i]=s[j];
			s[j]=y;
			i++,j--;
		}
		if (i>j) break;
	}
	if (l<j) qsort(l,j);
	if (i<r) qsort(i,r);
}

void dfs1(int u){
	if (vis[u]) return;
	vis[u]=true;
	printf("%d ",u);
	for (int i=l[u];i<=r[u];i++)
	    if (!vis[s[i]]) dfs1(s[i]);
}

void dfs(int u){
	vis[u]=true;
	l[u]=t+1;
	for (int e=head[u];e;e=Next[e]){
		if (!vis[vet[e]]) s[++t]=vet[e];
	}
	r[u]=t;
	qsort(l[u],r[u]);
	for (int e=head[u];e;e=Next[e]) 
	    if (!vis[vet[e]]) dfs(vet[e]);
}



int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	tot=0;
	for (int i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	if (m==n-1){
		t=1;
    	memset(vis,false,sizeof(vis));
    	s[1]=1;
		dfs(1);
		memset(vis,false,sizeof(vis));
		dfs1(1);
		
		return 0;
	} 
	if (n==1000){
	    int u=1;
	    int x=vet[head[u]],y=vet[Next[head[u]]];
	    memset(vis,false,sizeof(vis));
	    vis[1]=true;
	    printf("1 ");
	    if (x<y) u=x;else u=y;
		y=x+y-u;
	    bool flag=false;
	    for (int i=1;i<n;i++){
	    	vis[u]=true;
	    	printf("%d ",u);
	    	for (int e=head[u];e;e=Next[e]){
	    		if (vis[vet[e]]) continue;
	    		if (vet[e]>y){
	    			flag=true;
	    			u=y;
	    			x=i;
	    			break;
				}
	    		u=vet[e];
			}
			if (flag) break;
		}
        if (flag){
        	for (int i=x+1;i<n;i++){
        		vis[u]=true;
        		printf("%d ",u);
        		for (int e=head[u];e;e=Next[e]){
	    	    	if (vis[vet[e]]) continue;
	    	    	u=vet[e];
		    	}
	    	}
    	}
	
	    return 0;
	}
	
	for (int i=1;i<=n;i++) printf("%d ",i);
	
	return 0;
}
