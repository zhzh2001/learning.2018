#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define mod 1000000007

using namespace std;

int a[10][10],c[2000],n,m;

ll pn(int a,int b){
	ll ans=1,t=a;
	while (b){
		if (b&1) ans=1ll*ans*t%mod;
		t=1ll*t*t%mod;
		b>>=1;
	}
	return ans;
}

bool check(int s){
	for (int i=1;i<=n*m;i++)
	    if (s&(1<<(n*m-i))) a[(i-1)/m+1][(i-1)%m+1]=1;
	        else a[(i-1)/m+1][(i-1)%m+1]=0;
	memset(c,-1,sizeof(c));
	for (int i=0;i<(1<<(n+m-2));i++){
		int x=0,y;
		for (int j=1;j<=n+m-2;j++)
		    if (i&(1<<(j-1))) x++;
		if (x!=m-1) continue;
		x=1,y=1;
		c[i]=0;
		for (int j=1;j<=n+m-2;j++){
			c[i]=c[i]*2+a[x][y];
			if (i&(1<<(n+m-2-j))) y++;else x++;
		}
	    c[i]=c[i]*2+a[x][y];
	}
	int last=0;
	for (int i=1;i<=(1<<(n+m-2));i++){
		if (c[i]==-1) continue;
		if (c[i]<c[last]) return false;
		last=i;
	}
	return true;
}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	if (n<=3 && m<=3){
		int ans=0;
		for (int i=0;i<(1<<(n*m));i++)
	        if (check(i)) ans++;
	    printf("%d\n",ans);
	    
	    return 0;
	}
	if (n==2 && m>3){
		ll ans=pn(3,m-1);
		ans=1ll*ans*2%mod;
		ans=1ll*ans*2%mod;
		printf("%lld\n",ans);
		
		return 0;
	}
	if (n==3 && m>3){
		ll ans=112;
		ans=1ll*ans*pn(3,m-3)%mod;
		printf("%lld\n",ans);
		
		return 0;
	}
	
}
