#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long

using namespace std;

char ch[5];
int n,m,x1,Y1,x2,y2;
ll p[101000],f[101000][2],g[101000][2];

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	
	scanf("%d%d%s",&n,&m,&ch);
	for (int i=1;i<=n;i++) scanf("%lld",&p[i]);
	if (ch[0]=='A' && ch[1]=='3'){
		for (int i=1;i<n;i++) scanf("%d%d",&x1,&x2);
		for (int i=1;i<=m;i++){
			scanf("%d%d%d%d",&x1,&Y1,&x2,&y2);
			if (Y1==0 && y2==0)
			    if ((x1==x2-1)||(x1==x2+1)){
			    	printf("-1\n");
			    	continue;
				}
			memset(f,0,sizeof(f));
			f[x1][1-Y1]=2e13;
			f[x2][1-y2]=2e13;
			for (int i=1;i<=n;i++){
				if (f[i][0]!=2e13) f[i][0]=f[i-1][1];
				if (f[i][1]!=2e13) f[i][1]=min(f[i-1][0],f[i-1][1])+p[i];				
			}
			if (f[n][0]>f[n][1]) printf("%lld\n",f[n][1]);
			    else printf("%lld\n",f[n][0]);
		}
		
		return 0;
	}
	
	if (ch[0]=='A' && ch[1]=='2'){
		for (int i=1;i<n;i++) scanf("%d%d",&x1,&x2);
		memset(f,0,sizeof(f));
		memset(g,0,sizeof(g));
		for (int i=1;i<=n;i++){
			f[i][0]=f[i-1][1];
			f[i][1]=min(f[i-1][0],f[i-1][1])+p[i];				
		}
		for (int i=n;i;i--){
			g[i][0]=g[i+1][1];
			g[i][1]=min(g[i+1][0],g[i+1][1])+p[i];
		}
		for (int i=1;i<=m;i++){
			scanf("%d%d%d%d",&x1,&Y1,&x2,&y2);
			if (Y1==0 && y2==0){
			    printf("-1\n");
			    continue;
			}
			ll ans=f[x1][Y1]+g[x2][y2];
			printf("%lld\n",ans);
		}
		
		return 0;
	}
	
	if (ch[0]=='A' && ch[1]=='1'){
		for (int i=1;i<n;i++) scanf("%d%d",&x1,&x2);
		memset(f,0,sizeof(f));
		memset(g,0,sizeof(g));
		f[1][0]=2e13;
		f[1][1]=p[1];
		for (int i=2;i<=n;i++){
			f[i][0]=f[i-1][1];
			f[i][1]=min(f[i-1][0],f[i-1][1])+p[i];				
		}
		for (int i=n;i>1;i--){
			g[i][0]=g[i+1][1];
			g[i][1]=min(g[i+1][0],g[i+1][1])+p[i];
		}
		g[1][0]=2e13;
		g[1][1]=min(g[2][0],g[2][1])+p[1];
		for (int i=1;i<=m;i++){
			scanf("%d%d%d%d",&x1,&Y1,&x2,&y2);
		    ll ans;
			if (y2==0) ans=f[x2][y2]+g[x2+1][1];
			    else ans=f[x2][y2]+min(g[x2+1][0],g[x2+1][1]);
			printf("%lld\n",ans);
		}
		
		return 0;
	}
}
