#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
const int N=100005;
int l,n,m,x,y,a,b,nxt[N<<1],last[N],to[N<<1],p[N],size[N];
ll dp[N][2][2],g[2][2],ans;
char str[123];
void add(int x,int y)
{
	nxt[++l]=last[x];
	last[x]=l;
	to[l]=y;
}
void dfs(int u,int fa)
{
	dp[u][0][0]=0;dp[u][1][1]=p[u];
	dp[u][0][1]=dp[u][1][0]=1e18;
	for (int x=last[u];x;x=nxt[x])
	{
		int v=to[x];
		if (v==fa)	continue;
		dfs(v,u);
		for (int i=0;i<2;++i)
			for (int j=0;j<2;++j)
			{
				g[i][j]=dp[u][i][j];
				dp[u][i][j]=1e18;
			}
		dp[u][0][0]=g[0][0]+dp[v][0][1];
		dp[u][0][1]=min(g[0][0]+dp[v][1][1],g[0][1]+dp[v][1][1]);
		dp[u][0][1]=min(dp[u][0][1],g[0][1]+dp[v][0][1]);
		dp[u][1][1]=min(g[0][0]+dp[v][0][1]+p[u],g[0][0]+dp[v][1][1]+p[u]);
		dp[u][1][1]=min(dp[u][1][1],g[0][1]+dp[v][0][1]+p[u]);
		dp[u][1][1]=min(dp[u][1][1],g[0][1]+dp[v][1][1]+p[u]);
		dp[u][1][1]=min(dp[u][1][1],g[1][1]+dp[v][0][1]);
		dp[u][1][1]=min(dp[u][1][1],g[1][1]+dp[v][1][1]);
		dp[u][1][1]=min(dp[u][1][1],g[1][1]+dp[v][0][0]);
		dp[u][1][1]=min(dp[u][1][1],g[0][1]+dp[v][0][0]+p[u]);
		dp[u][1][1]=min(dp[u][1][1],g[0][0]+dp[v][0][0]+p[u]);
		size[u]+=size[v];
	}
	if (size[u])	dp[u][0][0]=1e18;
	size[u]++;
	if (u==a)	dp[u][x^1][1]=dp[u][x^1][0]=1e18;
	if (u==b)	dp[u][y^1][1]=dp[u][y^1][0]=1e18;
//	cout<<dp[u][0][1]<<' '<<dp[u][1][1]<<' '<<u<<endl;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,str);
	for (int i=1;i<=n;++i)	scanf("%d",&p[i]);
	for (int i=1;i<n;++i)
	{
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	while (m--)
	{
		scanf("%d%d%d%d",&a,&x,&b,&y);
		dfs(1,0);
		ans=min(dp[1][0][1],dp[1][1][1]);
		if (ans>=1e18)	ans=-1;
		printf("%lld\n",ans);
	}
	return 0;
}
