#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
const int N=5005;
struct node
{
	int x,y,i;
}a[N*2];
int n,m,x,y,cnt;
int ans[N],now[N],c[N][N];
bool vis[N];
bool cmp(node a,node b)
{
	return a.y<b.y;
}
void dfs(int t)
{
	now[++cnt]=t;
	vis[t]=1;
	for (int i=1;i<=c[t][0];++i)
		if (!vis[c[t][i]])
			dfs(c[t][i]);
}
bool check()
{
	if (cnt<n)	return 0;
	if (ans[1]==0)	return 1;
	for (int i=1;i<=n;++i)
		if (now[i]<ans[i])	return 1;
		else if (now[i]>ans[i])	return 0;
	return 0;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;++i)
	{
		scanf("%d%d",&x,&y);
		a[i*2-1]=(node){x,y,i};
		a[i*2]=(node){y,x,i};
	}
	sort(a+1,a+m+m+1,cmp);
	if (n==m)
	{
		for (int t=1;t<=m;++t)
		{
			for (int i=1;i<=n;++i)	c[i][0]=vis[i]=0;cnt=0;
			for (int i=1;i<=m+m;++i)
				if (a[i].i!=t)
				{
					x=a[i].x;y=a[i].y;
					c[x][++c[x][0]]=y;
				}
			dfs(1);	
			if (check())	for (int i=1;i<=n;++i)	ans[i]=now[i];
		}
	}
	else
	{
		for (int i=1;i<=m+m;++i)
		{
			x=a[i].x;y=a[i].y;
			c[x][++c[x][0]]=y;
		}
		dfs(1);
		for (int i=1;i<=n;++i)	ans[i]=now[i];
	}
	for (int i=1;i<=n;++i)	printf("%d%c",ans[i]," \n"[i==n]);
	return 0;
}
