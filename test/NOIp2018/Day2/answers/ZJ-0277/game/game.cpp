#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
const int mo=1e9+7;
int n,m,ans,cot,po;
char a[10000][100],b[10000][100],c[100][100],w[100],s[100];
void dfs2(int x,int y)
{
	if (x==n && y==m)
	{
		++cot;
		for (int i=1;i<=n+m-2;++i)
			a[cot][i]=w[i],
			b[cot][i]=s[i];
		return;
	}
	if (x<n)
	{
		w[x+y-1]='D';
		s[x+y-1]=c[x][y];
		dfs2(x+1,y);
	}
	if (y<m)
	{
		w[x+y-1]='R';		
		s[x+y-1]=c[x][y];
		dfs2(x,y+1);
	}
}
bool TRY(char a[],char b[])
{
	for (int i=1;i<=n+m-2;++i)
		if (a[i]>b[i])	return 1;
		else if (a[i]<b[i])	return 0;
	return 0;
}
bool check()
{
	cot=0;
	dfs2(1,1);
	for (int i=1;i<=cot;++i)
		for (int j=1;j<=cot;++j)
			if (i!=j)
			if (TRY(a[i],a[j]) && TRY(b[i],b[j]))	return 0;
	return 1;
}
void dfs(int x,int y)
{
	if (x>n)
	{
		if (check())	++ans;
		++po;
		return;
	}	
	for (int o=0;o<2;++o)
	{
		c[x][y]=o+48;
		if (y<m)	dfs(x,y+1);
		else dfs(x+1,1);
	}
}
int exp(int x,int n)
{
	int s=1;
	while (n)
	{
		if (n&1)	s=(ll)s*x%mo;
		n>>=1;
		x=(ll)x*x%mo;
	}
	return s;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==2)	printf("%lld\n",4LL*exp(3,m-1)%mo);
	else	
	if (n==3)
	{
		if (m==1)	puts("8");
		else if (m==2)	puts("36");
		else if (m==3)	puts("112");
		else cout<<112LL*exp(3,m-3)%mo<<endl;
	}
	else
	{
		dfs(1,1);
		printf("%d\n",ans);
	}
	return 0;
}
