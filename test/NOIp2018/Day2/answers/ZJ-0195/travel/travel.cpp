#include<cstdio>

bool f[5005],a[5005][5005],c[10005],vis,se;
int n,m,x,y,p,Next[10005],num,d[10005],Head[5005],to[10005],q[10005],t,h,son[5005][5005],fi,nxt,c1,c2;

int read() {
	char ch=getchar(),l='+';int t=0;
	while (ch<'0'||ch>'9')	l=ch,ch=getchar();
	while (ch>='0'&&ch<='9')t=(t<<1)+(t<<3)+ch-48,ch=getchar();
	return l=='-'?-t:t;
}

void Add_Edge(int x,int y){
	Next[++num]=Head[x];
	Head[x]=num;
	to[num]=y;
}

void dfs(int u) {
	f[u]=1;
	std::printf("%d ",u);
	for (int i=1;i<=n;i++)
		if (!f[i]&&a[u][i])
			dfs(i);
}

void dfs1(int u,int pre,int nxt) {
	std::printf("%d ",u);
	f[u]=1;
	if (c[u]||se)
	{
		for (int i=1;i<=n;i++)
			if (!f[i]&&a[u][i])
				dfs1(i,u,nxt);
		return;
	}else
	{
		if (!vis)
		{
			vis=1;fi=u;
			for (int i=1;i<=n;i++)
			{
				if (f[i]||!a[u][i])	continue;
				if (!c[i])
					c2=i;
				son[u][++son[u][0]]=i;
			}
			for (int i=1;i<=son[u][0];i++)
			{
				if (f[son[u][i]])	continue;
				dfs1(son[u][i],u,son[u][i+1]);
				if (!c[son[u][i]])
					se=1;
			}
		}else
		{
			int i,cu=-1 ;
			for (i=1;i<=n;i++)
			{
				if (f[i]||!a[u][i])	continue;
				son[u][++son[u][0]]=i;
				if (!c[i])
					cu=i;
			}
			for (i=1;i<=son[u][0];i++)
			{
				if (son[u][i]==cu)
					break;
				dfs1(son[u][i],u,son[u][i+1]);
			}
			if (cu==son[u][son[u][0]])
				if (cu>nxt)
					return;
			for (;i<=son[u][0];i++)
				dfs1(son[u][i],u,son[u][i+1]);
		}
	}
	
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();
	m=read();
	if (m==n-1)
	{
		for (int i=1;i<=m;i++)
		{
			x=read(),y=read();
			a[x][y]=a[y][x]=1;
		}
		dfs(1);
		return 0;
	}
	num=1;vis=0;
	for (int i=1;i<=m;i++)
	{
		x=read(),y=read();
		Add_Edge(x,y);
		Add_Edge(y,x);
		d[x]++,d[y]++;
		a[x][y]=a[y][x]=1;
	}
	for (int i=1;i<=n;i++)
		if (d[i]==1)
			q[++t]=i,c[i]=1;
	for (h=1;h<=t;h++)
		for (int e=Head[q[h]];e;e=Next[e])
		{
			d[to[e]]--;
			if (d[to[e]]==1)
				q[++t]=to[e],c[to[e]]=1;
		}
	h=t=1;q[1]=1;
	dfs1(1,0,n+1);
	return 0;
}
