#include<cstdio>
#include<algorithm>

typedef long long ll;
bool fl;
int Next[200005],num,Head[100005],to[200005],p[100005],a,b,ai,bi,n,m,x,y;
char s[3];
ll f[100005][2];

ll min(ll x,ll y) {
	if (x==-1)	return y;
	if (y==-1)	return x;
	return std::min(x,y);
}

void Add_Edge(int x,int y) {
	Next[++num]=Head[x];
	Head[x]=num;
	to[num]=y;
}

void dfs1(int u,int pre) {
	f[u][1]=p[u];
	for (int e=Head[u]; e; e=Next[e]) {
		if (to[e]==pre)	continue;
		int v=to[e];
		dfs1(to[e],u);
		if (f[u][0]!=-1&&f[v][1]!=-1)
			f[u][0]+=f[v][1];
		else
			f[u][0]=-1;
		f[u][1]+=min(f[v][0],f[v][1]);
		if (ai==0&&bi==0&&(a==u&&b==v||a==v&&b==u))
			fl=1;
	}
	if (u==a)
		f[u][1-ai]=-1;
	if (u==b)
		f[u][1-bi]=-1;
}

int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for (int i=1; i<=n; i++)
		scanf("%d",&p[i]);
	for (int i=1; i<n; i++) {
		scanf("%d%d",&x,&y);
		Add_Edge(x,y);
		Add_Edge(y,x);
	}
	for (int i=1; i<=m; i++) {
		scanf("%d%d%d%d",&a,&ai,&b,&bi);
		for (int i=1; i<=n; i++)
			f[i][0]=f[i][1]=0;
		fl=0;
		dfs1(1,0);
		if (fl)	printf("-1\n");
		else
			printf("%lld\n",min(f[1][0],f[1][1]));
	}
}
