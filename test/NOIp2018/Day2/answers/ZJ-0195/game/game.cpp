#include<cstdio>
#include<iostream>

typedef long long ll;
const int P=1e9+7;
int n,m;

ll Pow(ll x,ll y) {
	ll t=1;
	for (;y;y>>=1,(x*=x)%=P)
		if (y&1)	(t*=x)%=P;
	return t;	
}

int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	std::scanf("%d%d",&n,&m);
	if (n>m)	std::swap(n,m);
	if (n==2){
		ll t=0;
		t=4*Pow(3,m-1);
		t%=P;
		std::printf("%lld\n",t);
		return 0;
	}
	if (n==1) {
		std::printf("%lld\n",Pow(2,m));
		return 0;
	}
	if (n==3&&m==3)
	{
		std::printf("112\n");
		return 0;
	}
}
