var
  n,m,i,a,b,x,y,cnt:longint;
  ch,ch1,ch2:char;
  ans:int64;
  w,v,nxt,head,fa:array[0..100005] of longint;
  f,g:array[0..100005,0..1] of int64;
procedure add(a,b:longint);
begin
  inc(cnt);
  v[cnt]:=b;
  nxt[cnt]:=head[a];
  head[a]:=cnt;
end;
function min(a,b:int64):int64;
begin
  if a<b then exit(a);
  exit(b);
end;
procedure dfs(u:longint);
var
  i:longint;
begin
  f[u,0]:=0;f[u,1]:=0;
  i:=head[u];
  while i<>0 do
  begin
    if v[i]<>fa[u] then
    begin
      fa[v[i]]:=u;
      dfs(v[i]);
      inc(f[u,0],f[v[i],1]);
      inc(f[u,1],min(f[v[i],0],f[v[i],1]));
    end;
    i:=nxt[i];
  end;
  inc(f[u,1],w[u]);
  if (u=a) then f[u,1-x]:=10000000005;
  if (u=b) then f[u,1-y]:=10000000005;
end;
begin
  assign(input,'defense.in');reset(input);
  assign(output,'defense.out');rewrite(output);
  read(n,m,ch,ch1,ch2);
  for i:=1 to n do
  read(w[i]);
  for i:=1 to n-1 do
  begin
    read(a,b);
    add(a,b);
    add(b,a);
  end;
  if (ch1='A') and (ch2='2') then
  begin
    for i:=1 to n do
    begin
      f[i,0]:=f[i-1,1];
      f[i,1]:=min(f[i-1,1],f[i-1,0])+w[i];
    end;
    for i:=n downto 1 do
    begin
      g[i,0]:=g[i+1,1];
      g[i,1]:=min(g[i+1,1],g[i+1,0])+w[i];
    end;
    for i:=1 to m do
    begin
      read(a,x,b,y);
      if (x=0) and (y=0) then writeln(-1) else
      begin
        ans:=0;
        if a=b-1 then
        begin
          if x=0 then inc(ans,f[a-1,1])
          else inc(ans,min(f[a-1,0],f[a-1,1])+w[a]);
          if y=0 then inc(ans,g[b+1,1])
          else inc(ans,min(g[b+1,0],g[b+1,1])+w[b]);
        end else
        begin
          if x=0 then inc(ans,g[a+1,1])
          else inc(ans,min(g[a+1,0],g[a+1,1])+w[a]);
          if y=0 then inc(ans,f[b-1,1])
          else inc(ans,min(f[b-1,0],f[b-1,1])+w[b]);
        end;
        writeln(ans);
      end;
    end;
    close(input);
    close(output);
    halt;
  end;
  for i:=1 to m do
  begin
    read(a,x,b,y);
    if (fa[a]=b) or (fa[b]=a) and (x=0) and (y=0) then writeln(-1) else
    begin
      dfs(1);
      writeln(min(f[1,0],f[1,1]));
    end;
  end;
  close(input);
  close(output);
end.
