var
  n,m,i,j,t,cnt:longint;
  a,b,s,ans,v,nxt,head:array[0..100005] of longint;
  d:array[0..5005,0..5005] of longint;
  p:array[0..100005] of boolean;
procedure add(a,b:longint);
begin
  inc(cnt);
  v[cnt]:=b;
  nxt[cnt]:=head[a];
  head[a]:=cnt;
end;
procedure qsort(k,l,r:longint);
var
  i,j,x,mid:longint;
begin
  i:=l;j:=r;mid:=d[k,(l+r)>>1];
  repeat
    while d[k,i]<mid do inc(i);
    while d[k,j]>mid do dec(j);
    if i<=j then
    begin
      x:=d[k,i];d[k,i]:=d[k,j];d[k,j]:=x;
      inc(i);dec(j);
    end;
  until i>j;
  if i<r then qsort(k,i,r);
  if j>l then qsort(k,l,j);
end;
function pd:boolean;
var
  i:longint;
begin
  for i:=1 to n do
  if s[i]<ans[i] then exit(true)
  else if s[i]>ans[i] then exit(false);
  exit(false);
end;
procedure dfs(u:longint);
var
  i,k:longint;
begin
  p[u]:=true;s[t]:=u;
  inc(t);
  if t>n then
  begin
    if pd then
    begin
      for i:=1 to n do
      ans[i]:=s[i];
    end;
    exit;
  end;
  i:=head[u];
  k:=0;
  while i<>0 do
  begin
    if not p[v[i]] then
    begin
      inc(k);
      d[u,k]:=v[i];
    end;
    i:=nxt[i];
  end;
  qsort(u,1,k);
  for i:=1 to k do
  dfs(d[u,i]);
end;
begin
  assign(input,'travel.in');reset(input);
  assign(output,'travel.out');rewrite(output);
  read(n,m);
  for i:=1 to m do
  begin
    read(a[i],b[i]);
  end;
  for i:=1 to n do
  ans[i]:=n+1;
  if m=n-1 then
  begin
    for i:=1 to m do
    begin
      add(a[i],b[i]);
      add(b[i],a[i]);
    end;
    t:=1;
    dfs(1);
  end else
  begin
    for j:=1 to m do
    begin
      for i:=1 to n do
      p[i]:=false;
      cnt:=0;
      for i:=1 to n do
      head[i]:=0;
      for i:=1 to m do
      if i<>j then
      begin
        add(a[i],b[i]);
        add(b[i],a[i]);
      end;
      t:=1;
      dfs(1);
    end;
  end;
  for i:=1 to n-1 do
  write(ans[i],' ');
  writeln(ans[n]);
  close(input);
  close(output);
end.
