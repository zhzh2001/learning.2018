const p=1000000007;
var
  n,m,i:longint;
  ans:int64;
begin
  assign(input,'game.in');reset(input);
  assign(output,'game.out');rewrite(output);
  readln(n,m);
  if n=2 then
  begin
    ans:=4;
    for i:=1 to n-1 do
    ans:=ans*3 mod p;
  end else
  begin
    if (n=1) and (m=1) then ans:=2;
    if (n=1) and (m=2) then ans:=4;
    if (n=1) and (m=3) then ans:=8;
    if (n=2) and (m=1) then ans:=4;
    if (n=2) and (m=2) then ans:=12;
    if (n=2) and (m=3) then ans:=36;
    if (n=3) and (m=1) then ans:=8;
    if (n=3) and (m=2) then ans:=36;
    if (n=3) and (m=3) then ans:=112;
  end;
  writeln(ans);
  close(input);
  close(output);
end.
