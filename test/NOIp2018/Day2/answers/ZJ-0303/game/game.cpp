#include<cstdio>
using namespace std;
const int N = 1e6 + 10;
const int M = (1 << 8) + 10;
const int mod = 1e9 + 7;
int f[2][M], a[M][M], L[M], n;
inline int re()
{
	int x = 0;
	char c = getchar();
	bool p = 0;
	for (; c < '0' || c > '9'; c = getchar())
		p |= c == '-';
	for (; c >= '0' && c <= '9'; c = getchar())
		x = x * 10 + c - '0';
	return p ? -x : x;
}
bool judge(int x, int y)
{
	for (int i = n - 2; ~i; i--)
		if (((y >> (i + 1)) & 1) < ((x >> i) & 1))
			return false;
	return true;
}
int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	int i, j, m, o, k;
	n = re();
	m = re();
	o = (1 << n) - 1;
	for (i = 0; i <= o; i++)
		for (j = 0; j <= o; j++)
			if (judge(i, j))
				a[i][++L[i]] = j;
	for (i = 0; i <= o; i++)
		f[1][i] = 1;
	int s;
	for (i = 2; i <= m; i++)
		for (j = 0; j <= o; j++)
		{
			for (s = 0, k = 1; k <= L[j]; k++)
				s = (1LL * s + f[(i & 1) ^ 1][a[j][k]]) % mod;
			f[i & 1][j] = s;
		}
	for (s = i = 0; i <= o; i++)
		s = (1LL * s + f[m & 1][i]) % mod;
	printf("%d", s);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
