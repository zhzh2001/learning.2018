#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N = 5010;
const int M = 1e4 + 10;
int an[N], a[N][N], b[N], tr[N][N], L[N], f[N], cir[N], st, k, n, l;
bool v[N];
inline int re()
{
	int x = 0;
	char c = getchar();
	bool p = 0;
	for (; c < '0' || c > '9'; c = getchar())
		p |= c == '-';
	for (; c >= '0' && c <= '9'; c = getchar())
		x = x * 10 + c - '0';
	return p ? -x : x;
}
void dfs(int x, int fa)
{
	an[++k] = x;
	int i, y;
	for (i = 1; i <= b[x]; i++)
		if ((y = a[x][i]) ^ fa)
			dfs(y, x);
}
bool dfs_1(int x, int fa)
{
	f[x] = fa;
	int i, y;
	for (i = 1; i <= b[x]; i++)
		if ((y = a[x][i]) ^ fa)
		{
			if (f[y])
			{
				st = x;
				f[y] = -1;
				return true;
			}
			if (dfs_1(y, x))
				return true;
		}
	return false;
}
void dfs_2(int x, int fa, int S)
{
	if (x ^ cir[S])
		tr[S][++L[S]] = x;
	int i;
	for (i = 1; i <= b[x]; i++)
		if (a[x][i] ^ fa && !v[a[x][i]])
			dfs_2(a[x][i], x, S);
}
void dfs_3(int x, int fa)
{
	an[++k] = x;
	int i;
	for (i = 1; i <= b[x]; i++)
		if (a[x][i]^ fa && a[x][i] ^ cir[1])
			dfs_3(a[x][i], x);
}
void dfs_4(int nw, bool hs, int oth, int nx, bool fk)
{
	if (nw > l || nw < 1)
		return;
	if (v[cir[nw]])
		return;
	v[cir[nw]] = 1;
	an[++k] = cir[nw];
	if (k >= n)
		return;
	if (hs)
	{
		if (fk)
		{
			if (!L[nw])
			{
				if (cir[nw + nx] < cir[oth])
					dfs_4(nw + nx, hs, oth, nx, fk);
				else
					dfs_4(oth, hs, nw + nx, -nx, !fk);
			}
			else
			{
				if (cir[nw + nx] < tr[nw][1])
				{
					dfs_4(nw + nx, !hs, oth, nx, !fk);
					for (int i = 1; i <= L[nw]; i++)
						an[++k] = tr[nw][i];
				}
				else
				{
					for (int i = 1; i <= L[nw]; i++)
						an[++k] = tr[nw][i];
					if (cir[nw + nx] < cir[oth])
						dfs_4(nw + nx, hs, oth, nx, fk);
					else
						dfs_4(oth, hs, nw + nx, -nx, !fk);
				}
			}
		}
		else
		{
			if (!L[nw])
				dfs_4(nw + nx, hs, oth, nw, fk);
			else
			{
				if (cir[nw + nx] < tr[nw][1])
				{
					dfs_4(nw + nx, !hs, oth, nx, !fk);
					for (int i = 1; i <= L[nw]; i++)
						an[++k] = tr[nw][i];
				}
				else
				{
					for (int i = 1; i <= L[nw]; i++)
						an[++k] = tr[nw][i];
					dfs_4(nw + nx, hs, oth, nx, fk);
				}
			}
		}
	}
	else
	{
		if (!L[nw])
			dfs_4(nw + nx, hs, oth, nx, fk);
		else
		{
			for (int i = 1; i <= L[nw]; i++)
				an[++k] = tr[nw][i];
			dfs_4(nw + nx, hs, oth, nx, fk);
		}
	}
}
int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	int i, m, x, y;
	n = re();
	m = re();
	for (i = 1; i <= m; i++)
	{
		x = re();
		y = re();
		a[x][++b[x]] = y;
		a[y][++b[y]] = x;
	}
	for (i = 1; i <= n; i++)
		sort(a[i] + 1, a[i] + b[i] + 1);
	if (m == n - 1)
	{
		dfs(1, 0);
		for (i = 1; i <= n; i++)
			printf("%d ", an[i]);
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	dfs_1(1, 0);
	for (i = st; ~i; i = f[i])
	{
		l++;
		v[i] = 1;
	}
	int j = l;
	for (i = st; ~i; i = f[i])
		cir[j--] = i;
	for (i = 1; i <= l; i++)
		dfs_2(cir[i], 0, i);
	memset(v, 0, sizeof(v));
	if (cir[1] ^ 1)
	{
		L[1] = 0;
		dfs_3(1, 0);
		dfs_4(1, 1, l, 1, 1);
	}
	else
		dfs_4(1, 1, l, 1, 1);
	for (i = 1; i <= k; i++)
		printf("%d ", an[i]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
