#include<cstdio>
#include<cstring>
using namespace std;
const int N = 1e5 + 10;
const int M = 2e5 + 10;
int fi[N], di[M], ne[M], val[N], f[N][3], l, X, Y, A, B;
inline int re()
{
	int x = 0;
	char c = getchar();
	bool p = 0;
	for (; c < '0' || c > '9'; c = getchar())
		p |= c == '-';
	for (; c >= '0' && c <= '9'; c = getchar())
		x = x * 10 + c - '0';
	return p ? -x : x;
}
inline void add(int x, int y)
{
	di[++l] = y;
	ne[l] = fi[x];
	fi[x] = l;
}
inline int minn(int x, int y){ return x < y ? x : y; }
void dfs(int x, int fa)
{
	f[x][1] = val[x];
	if (x == A)
	{
		if (X)
			f[x][0] = f[x][2] = 1e9;
		else
			f[x][1] = 1e9;
	}
	else
		if (x == B)
		{
			if (Y)
				f[x][0] = f[x][2] = 1e9;
			else
				f[x][1] = 1e9;
		}
	int i, y;
	for (i = fi[x]; i; i = ne[i])
		if ((y = di[i]) ^ fa)
		{
			dfs(y, x);
			f[x][1] += minn(f[y][0], minn(f[y][1], f[y][2]));
			f[x][0] += f[y][1];
			if (x ^ 1)
				f[x][2] += minn(!f[y][0] ? 1e9 : f[y][0], f[y][1]);
		}
}
int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	int i, n, m, x, y;
	char c[5];
	n = re();
	m = re();
	scanf("%s", c);
	for (i = 1; i <= n; i++)
		val[i] = re();
	for (i = 1; i < n; i++)
	{
		x = re();
		y = re();
		add(x, y);
		add(y, x);
	}
	for (i = 1; i <= m; i++)
	{
		A = re();
		X = re();
		B = re();
		Y = re();
		memset(f, 0, sizeof(f));
		dfs(1, 0);
		if (minn(f[1][0], f[1][1]) > 1e8)
		{
			printf("-1\n");
			continue;
		}
		if (A == 1)
			X ? printf("%d\n", f[1][1]) : printf("%d\n", f[1][0]);
		else
			if (B == 1)
				Y ? printf("%d\n", f[1][1]) : printf("%d\n", f[1][0]);
			else
				printf("%d\n", minn(f[1][0], f[1][1]));
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
