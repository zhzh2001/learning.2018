#include<bits/stdc++.h>
using namespace std;
int n,m,aa,bb,tt;
long long f[100005][2],g[100005][2],a[100005],min1,min2,ans;
bool vis[100005];
vector<int > v[100005];
char st[1000];
long long inf=1000000000000;
void dfs(int x,int y)
{
	if (x==bb&&!tt) return;
	vis[x]=1;
	for (int i=0;i<v[x].size();i++)
	if (!vis[v[x][i]]) 
	{
		if (f[x][1]!=inf) f[v[x][i]][0]=min(f[v[x][i]][0],f[x][1]),f[v[x][i]][1]=min(f[v[x][i]][1],f[x][1]+a[v[x][i]]);
		if (f[x][0]!=inf) f[v[x][i]][1]=min(f[v[x][i]][1],f[x][0]+a[v[x][i]]);
		dfs(v[x][i],x);
	}
}
void dfss(int x,int y)
{
	vis[x]=1;
	for (int i=0;i<v[x].size();i++)
	if (!vis[v[x][i]]) 
	{
		if (g[x][1]!=inf) g[v[x][i]][0]=min(g[v[x][i]][0],g[x][1]),g[v[x][i]][1]=min(g[v[x][i]][1],g[x][1]+a[v[x][i]]);
		if (g[x][0]!=inf) g[v[x][i]][1]=min(g[v[x][i]][1],g[x][0]+a[v[x][i]]);
		dfss(v[x][i],x);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%s",st);
	for (int i=1;i<=n;i++)
	scanf("%lld",&a[i]);
	int x,y;
	for (int i=1;i<n;i++)
	scanf("%d%d",&x,&y),v[x].push_back(y),v[y].push_back(x);
	if (st[1]=='1')
	{
		for (int j=1;j<=n;j++) f[j][0]=f[j][1]=inf,vis[j]=0;
		f[1][1]=a[1];
		dfs(1,0);
		for (int j=1;j<=n;j++) g[j][0]=g[j][1]=inf,vis[j]=0;
		g[n][0]=0,g[n][1]=a[n];
		dfss(n,0);
		for (int i=1;i<=m;i++)
		{
			scanf("%d%d%d%d",&aa,&x,&bb,&y);
			ans=f[bb][y]+g[bb][y];
			if (y==1) ans-=a[bb];
			if (ans>=inf) ans=-1;
			printf("%lld\n",ans);
		}
		return 0;
	}
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&aa,&x,&bb,&y);
		for (int j=1;j<=n;j++) f[j][0]=f[j][1]=inf,vis[j]=0;
		if (x==1) f[aa][1]=a[aa];else f[aa][0]=0;
		tt=0;
		dfs(aa,0);
		if (y==1) f[bb][0]=inf;else f[bb][1]=inf;
		tt=1;
		dfs(bb,0);
		min1=min2=1e16;
		if (f[1][0]>=0) min1=min(min1,f[1][0]);
		if (f[1][1]>=0) min1=min(min1,f[1][1]);
		if (f[n][0]>=0) min2=min(min2,f[n][0]);
		if (f[n][1]>=0) min2=min(min2,f[n][1]);
		ans=min1+min2;
		if (aa==1) ans=min2;
		if (aa==n) ans=min1;
		if (ans>=inf) ans=-1;
		printf("%lld\n",ans);
	}
	return 0;
}
