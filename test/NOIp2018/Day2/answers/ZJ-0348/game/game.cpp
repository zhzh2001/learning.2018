#include<bits/stdc++.h>
using namespace std;
#define mo 1000000007
long long n,m,ans;
long long q(long long a,long long b)
{
	long long s=1;
	while (b)
	{
		if (b%2) s=s*a%mo;
		b/=2;
		a=a*a%mo;
	}
	return s;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if (n>m) swap(n,m);
	if (n==1) 
	{
		ans=q(2,m)%mo;
	}
	if (n==2)
	{
		ans=4LL*q(3,m-1)%mo;
	}
	if (n==3)
	{
		ans=112LL*q(3,m-3)%mo;
	}
	if (n==4)
	{
		if (m==4) ans=912;
		else ans=2688LL*q(3,m-5)%mo;
	}
	if (n==5)
	{
		ans=7136LL*q(3,m-5)%mo;
	}
	printf("%lld",ans);
	return 0;
}
