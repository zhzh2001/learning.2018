#include <cstdio>
#include <queue>
#include <algorithm>
struct Node_Edge
{
	int nxt, to;
}e[11000];
std::priority_queue < int, std::vector<int>, std::greater<int> > qq;
int n, m, u, v, cnt, cnt2, s;
int h[6000], ans[6000];
bool flag;
bool used[6000];
inline void add_edge(const int& u, const int& v)
{
	e[++cnt].nxt = h[u];
	e[cnt].to = v;
	h[u] = cnt;
}

void dfs(const int& u)
{
	ans[++cnt2] = u;
	used[u] = 1;
	if (cnt2 == n)
	{
		flag = 1;
		return;
	}
	int cnt3 = 0;
	int xx[6000];
	for (register int i = h[u]; i; i = e[i].nxt)
		xx[++cnt3] = e[i].to;
	std::sort(xx + 1, xx + cnt3 + 1);
	for (register int i = 1; i <= cnt3; ++i)
		if (!used[xx[i]])
		{
			dfs(xx[i]);
			if (flag) return;
		}
}

int main()
{
	freopen("travel.in", "r+", stdin);
	freopen("travel.out", "w+", stdout);
	scanf("%d%d", &n, &m);
	for (register int i = 1; i <= m; ++i)
	{
		scanf("%d%d", &u, &v);
		add_edge(u, v); add_edge(v, u);
	}
	dfs(1);
	for (register int i = 1; i <= n; ++i)
		printf("%d%c", ans[i], i == n ? '\n' : ' ');
	fclose(stdin);
	fclose(stdout);
}
