#include <cstdio>
typedef long long ll;
const int mod = 1000000007;
int dp[2][600], f[5][5];
int n, m, N, ans, xxx;
int get(int x, int s = 0)
{
	for (; x; x >>= 1) if (x & 1) ++s;
	return s;
}
void add(int& x, const int& v)
{
	x = ((ll)x + v) % mod;
}
bool check(int j, int k)
{
	k = (j << 1) & k;
	xxx = j > ((N - 1) >> 1);
	return get(j) == get(k) + xxx;
}
int main()
{
	freopen("game.in", "r+", stdin);
	freopen("game.out", "w+", stdout);
	scanf("%d%d", &n, &m);
	if (n <= 3 && m <= 3)
	{
		f[1][1] = 2;
		f[1][2] = 4;
		f[1][3] = 8;
		f[2][1] = 4;
		f[2][2] = 12;
		f[2][3] = 36;
		f[3][1] = 8;
		f[3][2] = 36;
		f[3][3] = 112;
		printf("%d\n", f[n][m]);
	}	
	else
	{
		N = 1 << n;
		for (register int i = 0; i < N; ++i)
			dp[1][i] = 1;
		for (register int i = 2; i <= m; ++i)
		{
			for (register int j = 0; j < N; ++j) dp[i & 1][j] = 0;
			for (register int k = 0; k < N; ++k)
				for (register int j = 0; j < N; ++j)
					if (check(j, k))
						add(dp[i & 1][k], dp[(i - 1) & 1][j]); 
		}
		for (register int i = 0; i < N; ++i) add(ans, dp[m & 1][i]);
		printf("%d\n", ans);
	}
	fclose(stdin);
	fclose(stdout);
}
