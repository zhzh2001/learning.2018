#include <cstdio>
#include <cstring>
#include <algorithm>
int n, m, u, v, cnt, x, y, ss;
int p[200000], fa[200000], ff[200000];
int to[300000], nxt[300000], h[200000];
long long dp[200000][2];
char st[100];
int add(const int& x, const int& y)
{
	if (x == -1 || y == -1)
	{
		if (x == -1 && y == -1) return -1;
		return x + y + 1;
	}
	return x + y;
}
void add_edge(const int& u, const int& v)
{
	to[++cnt] = v;
	nxt[cnt] = h[u];
	h[u] = cnt;
}
void dfs(const int& u)
{
	for (register int i = h[u]; i; i = nxt[i])
		if (to[i] != fa[u])
		{
			fa[to[i]] = u;
			dfs(to[i]);
			dp[u][0] += dp[to[i]][1];
			dp[u][1] += std::min(dp[to[i]][1], dp[to[i]][0]);
		}
	dp[u][1] += p[u];
	if (ff[u] == 1) dp[u][0] = 0x3f3f3f3f;
	if (ff[u] == 0) dp[u][1] = 0x3f3f3f3f;
}
int main()
{
	freopen("defense.in", "r+", stdin);
	freopen("defense.out", "w+", stdout);
	scanf("%d%d%s", &n, &m, &st);
	for (register int i = 1; i <= n; ++i) scanf("%d", &p[i]), ss += p[i];
	for (register int i = 1; i < n; ++i)
	{
		scanf("%d%d", &u, &v);
		add_edge(u, v); add_edge(v, u);
	}
	memset(ff, -1, sizeof ff);
	for (register int i = 1; i <= m; ++i)
	{
		memset(dp, 0, sizeof dp);
		scanf("%d%d%d%d", &u, &x, &v, &y);
		ff[u] = x; ff[v] = y;
		fa[1] = 1; dfs(1);
		if (dp[1][1] > ss && dp[1][0] > ss) puts("-1");
		else printf("%lld\n", std::min(dp[1][1], dp[1][0]));
		ff[u] = ff[v] = -1;
	}
	fclose(stdin);
	fclose(stdout);
}
