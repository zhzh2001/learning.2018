#include<iostream>
#include<cstdio>
#include<cstring>

using namespace std;

long long inf=1e16;
int n,m,p[100010],u,v,a,x,b,y,nex[200010],head[100010],vet[200010],f[100010],tot;\
long long dp[100010][2];
char s[10];

void add(int u,int v){
	nex[++tot]=head[u];
	head[u]=tot;
	vet[tot]=v;
}

void dfs(int u){
	dp[u][1]+=p[u];
	for (int i=head[u];i;i=nex[i]){
		int v=vet[i];
		if (v!=f[u]){
			f[v]=u;
			dfs(v);
			dp[u][0]+=dp[v][1];
			dp[u][1]+=min(dp[v][0],dp[v][1]);
		}
	}
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s+1);
	for (int i=1;i<=n;i++)
		scanf("%d",&p[i]);
	for (int i=1;i<n;i++){
		scanf("%d%d",&u,&v);
		add(u,v);
		add(v,u);
	}
	if ((long long)n*m>=200000000){
		dfs(1);
		for (int i=1;i<=m;i++)
			if (min(dp[1][1],dp[1][0])<inf) printf("%lld\n",min(dp[1][1],dp[1][0]));
			else puts("-1");
		return 0;
	}
	for (int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		memset(dp,0,sizeof(dp));
		memset(f,0,sizeof(f));
		dp[a][x^1]=dp[b][y^1]=inf;
		dfs(1);
		if (min(dp[1][1],dp[1][0])<inf) printf("%lld\n",min(dp[1][1],dp[1][0]));
		else puts("-1");
	}
	return 0;
}
