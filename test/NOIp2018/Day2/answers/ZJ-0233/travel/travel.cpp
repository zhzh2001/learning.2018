#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>

using namespace std;

int n,m,f[5010],nex[10010],head[5010],vet[10010],
	tot,u,v,p1,p2,p3,dfn[5010],cnt,flag[5010],p,pp;

struct hi{
	int u,v;
}eg[10010];

bool cmp(hi x,hi y){
	return x.v>y.v;
}

void add(int u,int v){
	nex[++tot]=head[u];
	head[u]=tot;
	vet[tot]=v;
}

void dfs1(int u){
	printf("%d ",u);
	for (int i=head[u];i;i=nex[i]){
		int v=vet[i];
		if (v!=f[u]){
			f[v]=u;
			dfs1(v);
		}
	}
}

void dfs2(int u){
	dfn[u]=++cnt;
	for (int i=head[u];i&&!p1;i=nex[i]){
		int v=vet[i];
		if (v!=f[u]){
			if (dfn[v]){
				p1=u;
				p2=f[u];
				p3=v;
				break;
			}
			f[v]=u;
			dfs2(v);
		}
	}
}

void dfs3(int u){
	printf("%d ",u);
	for (int i=head[u];i;i=nex[i]){
		int v=vet[i];
		if (!f[v]&&v!=1){
			if (!p&&flag[u]&&flag[v]){
				for (int j=head[u];j;j=nex[j]){
					int vv=vet[j];
					if (vv!=v&&flag[vv]) p=vv;
				}
			}
			if (!pp&&flag[u]&&flag[v]&&v>p){
				pp=1;
				continue;
			}
			f[v]=u;
			dfs3(v);
		}
	}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		eg[++tot].u=u;
		eg[tot].v=v;
		eg[++tot].u=v;
		eg[tot].v=u;
	}
	tot=0;
	sort(eg+1,eg+2*m+1,cmp);
	for (int i=1;i<=2*m;i++)
		add(eg[i].u,eg[i].v);
	if (m==n-1){
		f[1]=0;
		dfs1(1);
		return 0;
	}
	if (m==n){
		f[1]=0;
		dfs2(1);
		flag[p1]=flag[p2]=1;
		while (p2!=p3){
			p2=f[p2];
			flag[p2]=1;
		}
		memset(f,0,sizeof(f));
		dfs3(1);
		return 0;
	}
	return 0;
}
