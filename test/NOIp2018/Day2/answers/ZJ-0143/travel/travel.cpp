#include<bits/stdc++.h>
using namespace std;
#define INF 0x7fffffff
#define MEM(a,b) memset(a,b,sizeof(a))
#define ll long long
#define fui(i,a,b,c) for(int i=(a);i<=(b);i+=(c))
#define fdi(i,a,b,c) for(int i=(a);i>=(b);i-=(c))
#define fel(i,u) for(int i=hd[u];i;i=dg[i].nxt)
#define maxn 5010
int n,m;
int mp[maxn][maxn];
char vst[maxn];
int sta[maxn],top;
int blg[maxn],num[maxn];
int dfn[maxn],low[maxn],cnt,ins;
template<class T>
inline T read(T &n){
	n=0;int t=1;double x=10;char ch;
	for(ch=getchar();!isdigit(ch)&&ch!='-';ch=getchar());
	(ch=='-')?t=-1:n=ch-'0';
	for(ch=getchar();isdigit(ch);ch=getchar())n=n*10+ch-'0';
	if(ch=='.')for(ch=getchar();isdigit(ch);ch=getchar(),x*=10)n+=(ch-'0')/x;
	return n*=t;
}
template<class T>
T write(T n){if(n<0)putchar('-'),n=-n;if(n>=10)write(n/10);putchar(n%10+'0');return n;}
template<class T>T writeln(T n){write(n);puts("");return n;}
void init(){
	read(n),read(m);int x,y;
	fui(i,1,m,1)read(x),read(y),mp[x][++mp[x][0]]=y,mp[y][++mp[y][0]]=x;
	fui(i,1,n,1)sort(mp[i]+1,mp[i]+mp[i][0]+1);
}
void dfs(int u){
	vst[u]=1;sta[++top]=u;
	fui(i,1,mp[u][0],1)if(!vst[mp[u][i]])dfs(mp[u][i]);
}
void tarjan(int u,int f){
	dfn[u]=low[u]=++cnt;sta[++top]=u;
	fui(i,1,mp[u][0],1)if(mp[u][i]!=f){
		if(!dfn[mp[u][i]])tarjan(mp[u][i],u);
		low[u]=min(low[u],low[mp[u][i]]);
	}
	if(dfn[u]==low[u]){
		++ins;
		while(sta[top]!=u){
			blg[sta[top]]=ins;num[ins]++;top--;
		}
		blg[u]=ins;num[ins]++;top--;
	}
}
void dfs(int u,char rev,int oth){
//	cout<<u<<' '<<(rev?1:0)<<' '<<oth<<endl;
	sta[++top]=u,vst[u]=1;
	if(rev){
		int a;a=1;while(num[blg[mp[u][a]]]==1||vst[mp[u][a]])a++;
		fui(i,1,a-1,1)if(!vst[mp[u][i]])dfs(mp[u][i],0,0);
		if(mp[u][a]<oth)dfs(mp[u][a],1,oth);
		fui(i,a+1,mp[u][0],1)if(!vst[mp[u][i]])dfs(mp[u][i],0,0);return;
	}
	if(num[blg[u]]>1&&oth){
		int a,b;a=1;while(num[blg[mp[u][a]]]==1)a++;
		b=a+1;while(num[blg[mp[u][b]]]==1)b++;
		fui(i,1,a-1,1)if(!vst[mp[u][i]])dfs(mp[u][i],0,0);
		dfs(mp[u][a],1,mp[u][b]);
		fui(i,a+1,mp[u][0],1)if(!vst[mp[u][i]])dfs(mp[u][i],0,0);return;
	}
	fui(i,1,mp[u][0],1)if(!vst[mp[u][i]])dfs(mp[u][i],0,oth);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	init();
//	fui(i,1,n,1){cout<<i<<':';fui(j,1,mp[i][0],1)cout<<mp[i][j]<<' ';puts("");}
	if(m==n-1){
		dfs(1);
		fui(i,1,n,1)write(sta[i]),putchar(' ');
		return 0;
	}
	tarjan(1,1);
//	fui(i,1,n,1)cout<<dfn[i]<<' ';puts("");
//	fui(i,1,n,1)cout<<low[i]<<' ';puts("");
//	fui(i,1,n,1)cout<<blg[i]<<' ';puts("");
//	fui(i,1,n,1)cout<<num[blg[i]]<<' ';puts("");
	dfs(1,num[blg[1]]>1,1);
//	puts("-------------");
	fui(i,1,n,1)write(sta[i]),putchar(' ');
	return 0;
}
