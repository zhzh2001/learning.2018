#include<bits/stdc++.h>
using namespace std;
#define INF 0x7fffffff
#define MEM(a,b) memset(a,b,sizeof(a))
#define ll long long
#define fui(i,a,b,c) for(int i=(a);i<=(b);i+=(c))
#define fdi(i,a,b,c) for(int i=(a);i>=(b);i-=(c))
#define fel(i,u) for(int i=hd[u];i;i=dg[i].nxt)
#define maxn 100010
int n,m;
int cs1,cs2;
ll p[maxn];
struct Edge{
	int to,nxt;
	Edge(){}
	Edge(int x,int y){to=x,nxt=y;}
}dg[maxn<<1];
int hd[maxn],inb;
ll f[maxn][2],g[maxn][2];
int fa[maxn],dep[maxn];
ll frt[maxn][2],bhd[maxn][2];
template<class T>
inline T read(T &n){
	n=0;int t=1;double x=10;char ch;
	for(ch=getchar();!isdigit(ch)&&ch!='-';ch=getchar());
	(ch=='-')?t=-1:n=ch-'0';
	for(ch=getchar();isdigit(ch);ch=getchar())n=n*10+ch-'0';
	if(ch=='.')for(ch=getchar();isdigit(ch);ch=getchar(),x*=10)n+=(ch-'0')/x;
	return n*=t;
}
template<class T>
T write(T n){if(n<0)putchar('-'),n=-n;if(n>=10)write(n/10);putchar(n%10+'0');return n;}
template<class T>T writeln(T n){write(n);puts("");return n;}
void rad(){
	char ch;
	for(ch=getchar();ch!='A'&&ch!='B'&&ch!='C';ch=getchar());
	cs1=ch-'A'+1;ch=getchar();cs2=ch-'0';
}
void add(int x,int y){dg[++inb]=(Edge){y,hd[x]};hd[x]=inb;}
void init(){
	read(n),read(m);rad();int x,y;
	fui(i,1,n,1)read(p[i]);
	fui(i,2,n,1){read(x),read(y);add(x,y),add(y,x);}
}
void t1dfs(int u,int ff){
	fa[u]=ff;dep[u]=dep[ff]+1;
	f[u][1]=p[u];
	fel(i,u)if(dg[i].to!=ff){
		t1dfs(dg[i].to,u);
		f[u][0]+=f[dg[i].to][1],f[u][1]+=min(f[dg[i].to][0],f[dg[i].to][1]);
	}
	g[u][0]=f[u][0],g[u][1]=f[u][1];
}
void hehehe(int a,int x,int b,int y){
	if(dep[a]<dep[b])swap(a,b),swap(x,y);
	int c=fa[a],d=fa[b];
	g[a][x^1]=g[b][y^1]=INF;
	while(dep[a]>dep[b]){
		g[c][0]-=f[a][1],g[c][1]-=min(f[a][0],f[a][1]);
		g[c][0]+=g[a][1];g[c][1]+=min(g[a][0],g[a][1]);
		g[a][0]=f[a][0],g[a][1]=f[a][1];a=c,c=fa[c];
	}
	while(a!=b){
		g[c][0]-=f[a][1],g[c][1]-=min(f[a][0],f[a][1]);
		g[c][0]+=g[a][1];g[c][1]+=min(g[a][0],g[a][1]);
		g[a][0]=f[a][0],g[a][1]=f[a][1];a=c,c=fa[c];
		g[d][0]-=f[b][1],g[d][1]-=min(f[b][0],f[b][1]);
		g[d][0]+=g[b][1];g[d][1]+=min(g[b][0],g[b][1]);
		g[b][0]=f[b][0],g[b][1]=f[b][1];b=d,d=fa[d];
	}
	while(a!=1){
		g[c][0]-=f[a][1],g[c][1]-=min(f[a][0],f[a][1]);
		g[c][0]+=g[a][1];g[c][1]+=min(g[a][0],g[a][1]);
		g[a][0]=f[a][0],g[a][1]=f[a][1];a=c,c=fa[c];
	}
}
int TP1(){
	t1dfs(1,1);int a,b,x,y;
	fui(i,1,m,1){
		read(a),read(x),read(b),read(y);
		if(!x&&!y&&(fa[a]==b||fa[b]==a)){puts("-1");continue;}
		hehehe(a,x,b,y);
		writeln(min(g[1][0],g[1][1]));
		g[1][0]=f[1][0],g[1][1]=f[1][1];
	}
	return 0;
}
//int TP2(){
//	fui(i,1,n,1)frt[i][0]=frt[i-1][1],frt[i][1]=p[i]+min(frt[i-1][0],frt[i-1][1]);
//	fdi(i,n,1,1)bhd[i][0]=bhd[i+1][1],bhd[i][1]=p[i]+min(bhd[i+1][0],bhd[i+1][1]);
//	int a,b,x,y,qqq[5],top;
//	ll aaa;
//	fui(i,1,m,1){
//		read(a),read(x),read(b),read(y);
//		if(!x&&!y&&(a-1==b||b-1==a)){puts("-1");continue;}
//		if(a>b)swap(a,b),swap(x,y);
//		top=aaa=0;
//		if(!x){
//			if(a!=1)qqq[++top]==a-1;
//			qqq[++top]=a+1;
//		}
//		else qqq[++top]=a;
//		if(!y){
//			if(b!=n)qqq[++top]==b+1;
//			qqq[++top]=b-1;
//		}
//		else qqq[++top]=b;
//		sort(qqq+1,qqq+top+1);
//		aaa=frt[qqq[1]][1]+bhd[qqq[top]][1];
//		fui(i,2,n,1)aaa+=frt
//	}
//	return 0;
//}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	init();
	if(n<=2000&&m<=2000||cs1==2)return TP1();
//	if(cs1==1)return TP2();
	puts("YXAKIOI");
	return 0;
}
