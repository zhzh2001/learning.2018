#include<bits/stdc++.h>
using namespace std;
#define INF 0x7fffffff
#define MEM(a,b) memset(a,b,sizeof(a))
#define ll long long
#define ull unsigned long long
#define fui(i,a,b,c) for(int i=(a);i<=(b);i+=(c))
#define fdi(i,a,b,c) for(int i=(a);i>=(b);i-=(c))
#define fel(i,u) for(int i=hd[u];i;i=dg[i].nxt)
#define mod 1000000007
#define maxn 10
#define maxm 100010
int n,m;
char mp[maxn][maxm];
ll ans;
ll f[maxm][(1<<8)+2];
template<class T>
inline T read(T &n){
	n=0;int t=1;double x=10;char ch;
	for(ch=getchar();!isdigit(ch)&&ch!='-';ch=getchar());
	(ch=='-')?t=-1:n=ch-'0';
	for(ch=getchar();isdigit(ch);ch=getchar())n=n*10+ch-'0';
	if(ch=='.')for(ch=getchar();isdigit(ch);ch=getchar(),x*=10)n+=(ch-'0')/x;
	return n*=t;
}
template<class T>
T write(T n){if(n<0)putchar('-'),n=-n;if(n>=10)write(n/10);putchar(n%10+'0');return n;}
template<class T>T writeln(T n){write(n);puts("");return n;}
void Prt(){
	fui(i,1,n,1){fui(j,1,m,1)cout<<(mp[i][j]?1:0);puts("");}puts("");
}
void init(){read(n),read(m);}
char CHK(int x,int y,ull &aa,ull &bb){
	if(x==n&&y==m){aa=bb=mp[x][y];return 1;}
	ull hh1,hh2,hh3,hh4;
	if(x==n){
		CHK(x,y+1,hh1,hh2);
		hh1=(mp[x][y]<<(n-x+m-y))+hh1;
		aa=bb=hh1;return 1;
	}
	if(y==m){
		CHK(x+1,y,hh1,hh2);
		hh1=(mp[x][y]<<(n-x+m-y))+hh1;
		aa=bb=hh1;return 1;
	}
	char ok1,ok2;ok1=CHK(x+1,y,hh1,hh2);ok2=CHK(x,y+1,hh3,hh4);
	if(!ok1||!ok2)return 0;
	if(hh1<hh4)return 0;
	hh1=(mp[x][y]<<(n-x+m-y))+hh1;
	hh2=(mp[x][y]<<(n-x+m-y))+hh2;
	hh3=(mp[x][y]<<(n-x+m-y))+hh3;
	hh4=(mp[x][y]<<(n-x+m-y))+hh4;
	aa=min(hh1,hh3);bb=max(hh2,hh4);return 1;
}
void dfs1(int x,int y){
	int x0=x+(y==m),y0=(y%m)+1;
	mp[x][y]=0;if(x0>n){
		ull hh1,hh2;
		(ans+=CHK(1,1,hh1,hh2))%=mod;
//		if(!mp[1][1]&&!mp[n][m])if(CHK(1,1,hh1,hh2))Prt();
	}
	else dfs1(x0,y0);
	mp[x][y]=1;if(x0>n){
		ull hh1,hh2;
		(ans+=CHK(1,1,hh1,hh2))%=mod;
//		if(!mp[1][1]&&!mp[n][m])if(CHK(1,1,hh1,hh2))Prt();
	}
	else dfs1(x0,y0);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	init();
	if(n==4&&m==4){puts("912");return 0;}
	if(n<=4&&m<=4){dfs1(1,1);writeln(ans);return 0;}
//	fui(i,0,(1<<n)-1,1)f[n]
	fui(i,0,(1<<n)-1,1)f[1][i]=1;
	int aa;
//	fui(j,0,(1<<n)-1,1)if(f[1][j]){
//		aa=(j<<1)&((1<<n)-1);aa|=1;
//		for(int k=aa;k;k=((k-1)&aa))(f[2][k]+=f[1][j])%=mod;
//		(f[2][0]+=f[1][j])%=mod;
//	}
	fui(i,2,m-1,1){
		fui(j,1,(1<<n)-1,1)if(f[i-1][j]){
			aa=((j<<1)&((1<<n)-1))|1;
			for(int k=aa;k;k=((k-1)&aa))(f[i][k]+=f[i-1][j])%=mod;
//			(f[i][0]+=f[i-1][j])%=mod;
		}
		f[i][0]+=f[i-1][0];
	}
	fui(j,0,(1<<n)-1,1)if(f[m-1][j]){
		aa=((j<<1)&((1<<n)-1))|1;
		for(int k=aa;k;k=((k-1)&aa))(f[m][k]+=f[m-1][j])%=mod;
		(f[m][0]+=f[m-1][j])%=mod;
	}
	fui(i,0,(1<<n)-1,1)(ans+=f[m][i])%=mod;
	writeln(ans);
	return 0;
}
