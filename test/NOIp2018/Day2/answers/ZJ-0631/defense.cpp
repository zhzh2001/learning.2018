#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
int vet[200200],nxt[200200],head[100100],vis[100100],f[100100][2],flag[100100][2],w[100100],edgenum,n,m;
void addedge(int u,int v){
	vet[++edgenum]=v;
	nxt[edgenum]=head[u];
	head[u]=edgenum;
}
void dfs(int u,int a,int x,int b,int y){
	if (vis[u]) return;
	vis[u]=1;
	if (head[u]==0){
		if (u==a) f[u][x]=w[u]*(x==1);
		else if (u==b) f[u][y]=w[u]*(y==1);
			 else f[u][0]=0,f[u][1]=w[u];
		return;
	}
	for (int i=head[u];i;i=nxt[i]){
		int v=vet[i];
		dfs(v,a,x,b,y);
		if (u==a){
			if (x){
				int min;
				if (f[v][0]==-1&&f[v][1]==-1){
					f[u][x]=-1;return;
				}
				if (f[v][0]==-1) min=f[v][1];
				else if (f[v][1]==-1) min=f[v][0];
					 else min=std::min(f[v][0],f[v][1]);
				f[u][x]+=min;
			}
			else{
				if (f[v][1]==-1){
					f[u][x]=-1;return;
				}
				f[u][x]+=f[v][1];
			}
		}
		else{
			if (u==b){
				if (y){
					int min;
					if (f[v][0]==-1&&f[v][1]==-1){
						f[u][y]=-1;return;
					}
					if (f[v][0]==-1) min=f[v][1];
					else if (f[v][1]==-1) min=f[v][0];
						 else min=std::min(f[v][0],f[v][1]);
					f[u][y]+=min;
				}
				else{
					if (f[v][1]==-1){
						f[u][y]=-1;return;
					}
					f[u][y]+=f[v][1];
				}
			}
			else{
				if (f[v][1]==-1||flag[u][0]==1){
					f[u][0]=-1;flag[u][0]=1;
				}
				else{
					f[u][0]+=f[v][1];
				}
				int min;
				if (f[v][0]==-1&&f[v][1]==-1||flag[u][1]==1){
					f[u][1]=-1;flag[u][1]=1;
				}
				if (f[v][0]==-1) min=f[v][1];
				else if (f[v][1]==-1) min=f[v][0];
					 else min=std::min(f[v][0],f[v][1]);
				f[u][1]+=min;
			}
		}
	}
	if (f[u][1]!=-1) f[u][1]+=w[u];
}
void solve1(){
	while (m--){
		int a,x,b,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		memset(f,-1,sizeof(f));
		memset(vis,0,sizeof(vis));
		dfs(1,a,x,b,y);
		if (f[1][1]==-1&&f[1][0]==-1) puts("-1");
		else{
			int ans;
			if (f[1][1]==-1) ans=f[1][0];
			else if (f[1][0]==-1) ans=f[1][1];
				 else ans=std::min(f[1][1],f[1][0]);
			printf("%d\n",ans);
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char t[20];
	scanf("%d%d %s",&n,&m,t);
	for (int i=1;i<=n;i++) scanf("%d",&w[i]);
	for (int i=1;i<=n-1;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		addedge(u,v);
		addedge(v,u);
	}
	if (n<=2000 && m<=2000) solve1();
	else{
		while (m--){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			puts("-1");
		}
	}
	return 0;
}
