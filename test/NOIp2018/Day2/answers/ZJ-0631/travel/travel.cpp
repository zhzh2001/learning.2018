#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
/*#include<queue>
std::priority_queue<int,std::vector<int>,std::greater<int> >Q;*/
struct node{
	int u,v;
}b[10010];
int vet[10010],nxt[10010],head[10010],a[10010],ans[10010],edgenum,num;
bool flag[5050][5050],vis[10010];
void addedge(int u,int v){
	vet[++edgenum]=v;
	nxt[edgenum]=head[u];
	head[u]=edgenum;
}
bool cmp(node x,node y){
	return x.u<y.u || x.u==y.u&&x.v>y.v;
}
void dfs(int u){
	a[++num]=u;
//	printf("%d\n",u);
	for (int i=head[u];i;i=nxt[i]){
		int v=vet[i];
		if (vis[v]) continue;
		vis[v]=1;
		dfs(v);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d",&b[i].u,&b[i].v);
		b[i+m].u=b[i].v,b[i+m].v=b[i].u;
	}
	std::sort(b+1,b+m+m+1,cmp);
	for (int i=1;i<=2*m;i++){
		addedge(b[i].u,b[i].v);
	}
	if (m==n-1){
		num=0;
		vis[1]=1;
		dfs(1);
	}
	else{
		for (int i=1;i<=n;i++) ans[i]=1e9+7;
		for (int i=1;i<=m+m;i++){
			if (flag[b[i].v][b[i].u]|flag[b[i].u][b[i].v]) continue;
//			puts("111111111");
			flag[b[i].v][b[i].u]=flag[b[i].u][b[i].v]=1;
//			printf("%d %d\n",b[i].u,b[i].v);
			edgenum=0;
			memset(head,0,sizeof(head));
			memset(nxt,0,sizeof(nxt));
			memset(vet,0,sizeof(vet));
			for (int j=1;j<=m+m;j++){
				if (j==i || b[j].u==b[i].v&&b[j].v==b[i].u) continue;
				addedge(b[j].u,b[j].v);
//				printf("%d %d\n",b[j].u,b[j].v);
			}
			num=0;
			memset(vis,0,sizeof(vis));
			vis[1]=1;
			dfs(1);
//			printf("%d\n",num);
			if (num!=n) continue;
			for (int j=1;j<=n;j++){
				if (a[j]==ans[j]) continue;
				if (a[j]>ans[j]) break;
				if (a[j]<ans[j]){
					for (int k=1;k<=n;k++){
						ans[k]=a[k];
					}
				}
			}
//			for (int j=1;j<=n;j++){
//				printf("%d ",ans[j]);
//			}
//			puts("");
		}
		for (int i=1;i<=n;i++) a[i]=ans[i];
	}
	for (int i=1;i<=n-1;i++){
		printf("%d ",a[i]);
	}
	printf("%d\n",a[n]);
	return 0;
}
