#include <bits/stdc++.h>
#define N 300020
#define mod 1000000007
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int p[N];
int to[N<<1], nxt[N<<1], head[N], cnt;
void insert(int x, int y) {
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
  to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}
char t[5];
typedef long long ll;
inline ll safe_min(ll a, ll b) {
  if (a == -1) return b;
  if (b == -1) return a;
  return min(a, b);
}

namespace subtask1 {
ll f[N][2];
void dfs(int x, int fa) {
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == fa) continue;
    dfs(to[i], x);
  }
  if (f[x][0] != -1) {
    f[x][0] = 0;
    for (int i = head[x]; i; i = nxt[i]) {
      if (to[i] == fa) continue;
      if (f[to[i]][1] == -1) {
        f[x][0] = -1;
        break;
      }
      f[x][0] += f[to[i]][1];
    }
  }
  if (f[x][1] != -1) {
    f[x][1] = p[x];
    for (int i = head[x]; i; i = nxt[i]) {
      if (to[i] == fa) continue;
      if (f[to[i]][0] == -1 && f[to[i]][1] == -1) {
        f[x][1] = -1;
        break;
      }
      f[x][1] += safe_min(f[to[i]][0], f[to[i]][1]);
    }
  }
}
void work(int n, int m) {
  for (int i = 1; i <= m; ++ i) {
    memset(f, 0, sizeof f);
    int x = read(), a = read();
    int y = read(), b = read();
    f[x][!a] = -1;
    f[y][!b] = -1;
    dfs(1, 0);
    printf("%d\n", safe_min(f[1][0], f[1][1]));
  }
}
}

namespace subtask2 {
void work(int n, int m) {
  return subtask1::work(n, m);
}
}

int main(int argc, char const *argv[]) {
  freopen("defense.in", "r", stdin);
  freopen("defense.out", "w", stdout);

  int n = read(), m = read();
  scanf("%s", t);
  for (int i = 1; i <= n; ++ i) {
    p[i] = read();
  }
  for (int i = 1; i < n; ++ i) {
    int x = read(), y = read();
    insert(x, y);
  }
  if (n <= 2000 && m <= 2000) {
    subtask1::work(n, m);
  } else {
    subtask2::work(n, m);
  }

  return 0;
}

/*
44
*/