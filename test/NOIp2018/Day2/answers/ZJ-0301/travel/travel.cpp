#include <bits/stdc++.h>
#define N 5020
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int fa[N];
int find(int x) {
  return x == fa[x] ? x : fa[x] = find(fa[x]);
}
int merge(int x, int y) {
  fa[find(x)] = find(y);
}

int to[N<<1], nxt[N<<1], head[N], cnt;
void insert(int x, int y) {
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
  to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}

int fakex, fakey;
int huan[N], top = 1;

bool zhao_huan(int x, int f) {
  huan[top] = x;
  if (x == fakey) {
    return true;
  }
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    ++ top;
    if (zhao_huan(to[i], x)) {
      return true;
    }
    -- top;
  }
  return false;
}

inline bool cmp(int *A, int *B) {
  for (int i = 1; A[i] && B[i]; ++ i) {
    if (A[i] != B[i]) {
      return A[i] > B[i];
    }
  }
  return true;
}

int fuckx, fucky;
int tmp1[N], tmp2[N];
int *A = tmp1, *B = tmp2;
int top2;
void work_dfs(int x, int f) {
  B[++ top2] = x;
  vector<int> vec;
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    if ((x == fuckx && to[i] == fucky) ||
        (x == fucky && to[i] == fuckx)) {
      continue;
    }
    vec.push_back(to[i]);
  }
  sort(vec.begin(), vec.end());
  for (size_t i = 0; i < vec.size(); ++ i) {
    work_dfs(vec[i], x);
  }
}
// ignore the edge of (x, y)
void query_for_ans(int x, int y) {
  fuckx = x;
  fucky = y;
  top2 = 0;
  work_dfs(1, 0);
  if (cmp(A, B)) {
    swap(A, B);
  }
}

void resolve_huantaoshu(int n, int m) {
  for (int i = 1; i <= n; ++ i) {
    fa[i] = i;
  }
  for (int i = 1; i <= m; ++ i) {
    int x = read(), y = read();
    if (find(x) == find(y)) {
      fakex = x;
      fakey = y;
    } else {
      insert(x, y);
      merge(x, y);
    }
  }
  zhao_huan(fakex, 0);
  // chong bian
  if (top == 2) {
    query_for_ans(0, 0);
    for (int i = 1; i <= n; ++ i) {
      printf("%d%c", A[i], i == n ? '\n' : ' ');
    }
    return;
  }
  insert(fakex, fakey);
  query_for_ans(fakex, fakey);
  for (int i = 2; i <= top; ++ i) {
    query_for_ans(huan[i], huan[i - 1]);
  }
  for (int i = 1; i <= n; ++ i) {
    printf("%d%c", A[i], i == n ? '\n' : ' ');
  }
}

void resolve_shu(int n, int m) {
  for (int i = 1; i <= m; ++ i) {
    int x = read(), y = read();
    insert(x, y);
  }
  query_for_ans(0, 0);
  for (int i = 1; i <= n; ++ i) {
    printf("%d%c", A[i], i == n ? '\n' : ' ');
  }
}

int main(int argc, char const *argv[]) {
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);

  int n = read(), m = read();
  if (n == m) resolve_huantaoshu(n, m);
  else resolve_shu(n, m);

  return 0;
}
/*
fuck you leather man
100
*/