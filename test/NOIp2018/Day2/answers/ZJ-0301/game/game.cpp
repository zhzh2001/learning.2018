#include <bits/stdc++.h>
#define N 10
#define mod 1000000007
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

typedef long long ll;
ll fast_pow(ll x, ll y) {
  ll z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}

int n, m;
namespace baosou {
int a[N][N], last, valid;
void dfs2(int x, int y, int v) {
  // printf("%d %d %d\n", x, y, v);
  if (x == n - 1 && y == m - 1) {
    if (v < last) {
      valid = false;
    }
    last = v;
    return;
  }
  if (y != m - 1) dfs2(x, y + 1, v << 1 | a[x][y + 1]);
  if (x != n - 1) dfs2(x + 1, y, v << 1 | a[x + 1][y]);
}
int ans;
void dfs1(int x, int y) {
  // printf("%d %d\n", x, y);
  if (y == m) ++ x, y = 0;
  if (x == n) {
    last = 0;
    valid = true;
    dfs2(0, 0, a[0][0]);
    ans += valid;
    return;
  }
  a[x][y] = 0;
  dfs1(x, y + 1);
  a[x][y] = 1;
  dfs1(x, y + 1);
}

int baosou(int n, int m) {
  dfs1(0, 0);
  return ans;
}
} // namespace baosou

namespace dabiao {
int resolve(int n, int m) {
  if (n > m) swap(n, m);
  if (n == 1) {
    return fast_pow(2, m);
  }
  if (n == 2) {
    return 4 * fast_pow(3, m - 1) % mod;
  }
  if (n == 3) {
    return 112 * fast_pow(3, m - 3) % mod;
  }
  if (n == 4) {
    if (m == 4) {
      return 912;
    } else {
      return 2688 * fast_pow(3, m - 5) % mod;
    }
  }
  if (n == 5 && m == 5) {
    return 7136;
  }
  return baosou::baosou(n, m);
}
}

int main(int argc, char const *argv[]) {
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);

  n = read(); m = read();
  printf("%d\n", dabiao::resolve(n, m));

  return 0;
}
/*
1:  2   4    8   16   32   64
2:  4  12   36  108  324  972 2916 8748
3:  8  36  112  336 1008 3024
4: 16 108  336  912 2688 8064
5: 32 324 1008 2688 7136 ????

13*5 = 65
*/