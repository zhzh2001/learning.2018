#include<cstdio>
#include<cstring>
#include<iostream>
#include<cmath>
#include<algorithm>
using namespace std;
const int mod=1e9+7;
int f[10000005][10],n,m,ans;
bool judge(int j,int k)
{
	for (int i=2;i<=n;i++)
		if (((j>>(n-i))&1)<((k>>(n-i+1))&1)) return 0;
	return 1;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=0;i<=((1<<n)-1);i++) f[1][i]=1;
	for (int i=2;i<=m;i++)
	{
		for (int j=0;j<=((1<<n)-1);j++)
			for (int k=0;k<=((1<<n)-1);k++)
			{
				if (judge(j,k)) f[i][k]=(f[i][k]+f[i-1][j])%mod;
			}
	}
	for (int i=0;i<=((1<<n)-1);i++) ans=(ans+f[m][i])%mod;
	printf("%d",ans);
	return 0;
}
