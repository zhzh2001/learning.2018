#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<queue>
using namespace std;
int n,m,tot,cnt,a[5005],b[5005],top;
int Head[5005],Next[5005],ret[5005];
bool vis[5005];
int ans[5005],Ans[5005],t[5005];
inline void ins(int x,int y)
{
	ret[++tot]=y;
	Next[tot]=Head[x];
	Head[x]=tot;
}
void dfs(int now,int f)
{
	ans[++cnt]=now;int st=top;vis[now]=1;
	for (int i=Head[now];i;i=Next[i])
	{
		if (vis[ret[i]]) continue;
		t[++top]=ret[i];
	}
	int ed=top;
	sort(t+st+1,t+ed+1);
	for (int i=st+1;i<=ed;i++) dfs(t[i],now);
}
void solve1()
{
	for (int i=1;i<=m;i++) ins(a[i],b[i]),ins(b[i],a[i]);
	dfs(1,0);
	for (int i=1;i<=cnt;i++) printf("%d ",ans[i]);
}
void judge()
{
	if (cnt<n) return;bool flag=0;
	for (int i=1;i<=n;i++)
	{
		if (Ans[i]>ans[i]){flag=1;break;}
		else if (Ans[i]==ans[i]) continue;
		else break;
	}
	if (flag) for (int i=1;i<=n;i++) Ans[i]=ans[i];
}
void solve2()
{
	Ans[1]=n;
	for (int i=1;i<=m;i++)
	{
		tot=0;for (int j=1;j<=n;j++) Head[j]=0,vis[j]=0;
		for (int j=1;j<=m;j++)
		{
			if (j==i) continue;
			ins(a[j],b[j]);ins(b[j],a[j]);
		}
		top=cnt=0;dfs(1,0);
		judge();
	}
	for (int i=1;i<=n;i++) printf("%d ",Ans[i]);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
		scanf("%d%d",&a[i],&b[i]);
	if (m==n-1) solve1();
	else solve2();
	return 0;
}
