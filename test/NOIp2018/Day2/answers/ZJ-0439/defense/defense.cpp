#include<cstdio>
#include<cstring>
#include<iostream>
#include<cmath>
#include<algorithm>
using namespace std;
char ch[10];
int tot,Head[100005],Next[200005],ret[200005];
int n,m,p[100005];
long long f[100005][5];
inline void ins(int x,int y)
{
	ret[++tot]=y;
	Next[tot]=Head[x];Head[x]=tot;
}
void dfs(int now,int fa)
{
	for (int i=Head[now];i;i=Next[i])
	{
		if (ret[i]==fa) continue;
		dfs(ret[i],now);
	}
	for (int i=Head[now];i;i=Next[i])
	{
		if (ret[i]==fa) continue;
		f[now][0]+=f[ret[i]][1];
		f[now][1]+=min(f[ret[i]][1],f[ret[i]][0]);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,ch);
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++)
	{
		int x,y;scanf("%d%d",&x,&y);
		ins(x,y);ins(y,x);
	}
	while (m--)
	{
		for (int i=1;i<=n;i++)
		{
			f[i][1]=p[i];
			f[i][0]=0;
		}
		int a,b,x,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if (x==0) f[a][1]=2e9;
		if (y==0) f[b][1]=2e9;
		if (x==1) f[a][0]=2e9;
		if (y==1) f[b][0]=2e9;
		dfs(1,0);
		if (f[1][1]>=2e9&&f[1][0]>=2e9) puts("-1");
		else printf("%lld\n",min(f[1][0],f[1][1]));
	}
	return 0;
}
