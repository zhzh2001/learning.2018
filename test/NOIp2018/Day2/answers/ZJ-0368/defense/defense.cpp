#include <cstdio>
#include <cctype>
namespace R {
	int x, ch;
	inline int read() {
		ch = getchar();
		while (isspace(ch)) ch = getchar();
		for (x = ch & 15, ch = getchar(); isdigit(ch); ch = getchar()) x = x * 10 + (ch & 15);
		return x;
	}
}
using R::read;
inline int min(int a, int b) {return a < b ? a : b;}
inline long long min(long long a, long long b) {return a < b ? a : b;}

#define maxn 100010
const long long inf = 0x3f3f3f3f3f3f3f3f;

int head[maxn], cnt;
struct Edge {
	int to, nxt;
} e[maxn << 1];
inline void add(int a, int b) {
	e[++cnt] = (Edge) {b, head[a]}; head[a] = cnt;
}

char t1;
int n, m, t2;
int w[maxn];
bool X_Z[maxn], Choose[maxn], halt;

long long f[maxn][2];
void dfs(int u, int fa = 0) {
	f[u][0] = 0, f[u][1] = w[u];
	for (int i = head[u]; i; i = e[i].nxt) {
		int v = e[i].to;
		if (v != fa) {
			dfs(v, u);
			if (halt) return ;
			f[u][0] += f[v][1];
			f[u][1] += min(f[v][1], f[v][0]);
			if (f[u][0] > inf) f[u][0] = inf;
			if (f[u][1] > inf) f[u][1] = inf;
		}
	}
	if (X_Z[u]) f[u][Choose[u] ^ 1] = inf;
	if (f[u][0] == inf && f[u][1] == inf) halt = true;
}


int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d %c%d", &n, &m, &t1, &t2);
	for (int i = 1; i <= n; i++) scanf("%d", w + i);
	for (int i = 1, a, b; i < n; i++) {
		a = read(), b = read();
		add(a, b);
		add(b, a);
	}
	while (m --> 0) {
		int A = read(), X = read(), B = read(), Y = read();
		if (A == B && X != Y) {
			puts("-1");
			continue;
		}
		if (t2 == 2 && !X && !Y) {
			puts("-1");
			continue;
		}
		X_Z[A] = X_Z[B] = true;
		Choose[A] = X, Choose[B] = Y;
		halt = false;
		dfs(1);
		if (!halt) printf("%lld\n", min(f[1][0], f[1][1]));
		else puts("-1");
		X_Z[A] = X_Z[B] = false;
	}
	return 0;
}
