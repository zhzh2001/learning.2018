#include <cstdio>
#include <algorithm>
#include <vector>
#define maxn 5010
inline int min(int a, int b) {return a < b ? a : b;}

int head[maxn], cnt = 1;
struct Edge {
	int to, nxt;
	bool can;
} e[maxn << 1];
inline void add(int a, int b) {
	e[++cnt] = (Edge) {b, head[a], true}; head[a] = cnt;
}
int n, m;

namespace Work1 {
	int ans[maxn], idx;
	void dfs(int u, int fa = 0) {
		ans[++idx] = u;
		std::vector<int> V; V.clear();
		for (int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if (v != fa) V.push_back(v);
		}
		std::sort(V.begin(), V.end());
		for (std::vector<int>::iterator it = V.begin(); it != V.end(); it++) {
			dfs(*it, u);
		}
	}
	int main() {
		for (int i = 1, a, b; i < n; i++) {
			scanf("%d%d", &a, &b);
			add(a, b);
			add(b, a);
		}
		dfs(1);
		for (int i = 1; i <= n; i++) {
			printf("%d", ans[i]);
			putchar(i == n ? '\n' : ' ');
		}
		return 0;
	}
}


namespace Work2 {
	int C;
	int ans[maxn], p[maxn], idx;
	bool vis[maxn];
	
	int res[maxn], scc;
	int in_C = 0;
	bool used_C = false;
	
	void dfs(int u, int fa = 0, int last = n + 1) {
		vis[u] = true;
		ans[++idx] = u;
		std::vector<int> V; V.clear();
		for (int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if (!vis[v] && v != fa) V.push_back(v);
		}
		std::sort(V.begin(), V.end());
		for (std::vector<int>::iterator it = V.begin(); it != V.end(); it++) if (!vis[*it]) {
			if (res[u] == res[*it]) {
				if (in_C == u) used_C = true;
				if (!in_C) in_C = u;
				if (used_C) {
					dfs(*it, u, n + 1);
				} else {
					if (*it < last) {
						int tmp;
						if ((it + 1) != V.end()) tmp = *(it + 1);
						else tmp = last;
						dfs(*it, u, tmp);
					}
				}
			} else dfs(*it, u, n + 1);
		}
	}
	
	int DFN[maxn], low[maxn];
	int S[maxn], top;
	void tarjan(int u, int father = 0) {
		DFN[u] = low[u] = ++idx;
		S[++top] = u;
		int v;
		for (int i = head[u]; i; i = e[i].nxt) if (i ^ father ^ 1) {
			v = e[i].to;
			if (!DFN[v]) {
				tarjan(v, i);
				low[u] = min(low[u], low[v]);
			} else low[u] = min(low[u], DFN[v]);
		}
		if (DFN[u] == low[u]) {
			scc++;
			do {
				v = S[top--];
				res[v] = scc;
			} while (v != u);
		}
	}
	
	inline bool check() {
		for (int i = 1; i <= n; i++) if (ans[i] != p[i]) {
			return p[i] < ans[i];
		}
		return false;
	}
	
	void dfs1(int u, int fa = 0) {
		vis[u] = true;
		ans[++idx] = u;
		std::vector<int> V; V.clear();
		for (int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if (!vis[v] && v != fa) V.push_back(v);
		}
		std::sort(V.begin(), V.end());
		for (std::vector<int>::iterator it = V.begin(); it != V.end(); it++) {
			dfs1(*it, u);
		}
	}
	
	void dfs2(int u, int fa = 0) {
		p[++idx] = u;
		std::vector<int> V; V.clear();
		for (int i = head[u]; i; i = e[i].nxt) if (e[i].can) {
			int v = e[i].to;
			if (v != fa) V.push_back(v);
		}
		std::sort(V.begin(), V.end());
		for (std::vector<int>::iterator it = V.begin(); it != V.end(); it++) {
			dfs2(*it, u);
		}
	}
	
	int main() {
		for (int i = 0, a, b; i < n; i++) {
			scanf("%d%d", &a, &b);
			add(a, b);
			add(b, a);
		}
		tarjan(1);
		if (scc == 1) {
			idx = 0;
			dfs1(1);
			for (int i = 1; i <= n; i++) {
				printf("%d", ans[i]);
				putchar(i == n ? '\n' : ' ');
			}
			return 0;
		}
		for (int i = 1; i <= n; i++) ans[i] = n;
		if (n < 500) {
			for (int i = 2; i <= cnt; i += 2) {
				int u = e[i ^ 1].to, v = e[i].to;
				if (res[u] == res[v]) {
					idx = 0;
					e[i].can = e[i ^ 1].can = false;
					dfs2(1);
					if (check()) {
						for (int j = 1; j <= n; j++) ans[j] = p[j];
					}
					e[i].can = e[i ^ 1].can = true;
				}
			}
			for (int i = 1; i <= n; i++) {
				printf("%d", ans[i]);
				putchar(i == n ? '\n' : ' ');
			}
			return 0;
		}
		for (int i = 2; i <= cnt; i += 2) {
			int u = e[i ^ 1].to, v = e[i].to;
			if (res[u] == res[v]) {
				C = res[u];
				break;
			}
		}
		idx = 0;
		dfs(1);
		for (int i = 1; i <= n; i++) {
			printf("%d", ans[i]);
			putchar(i == n ? '\n' : ' ');
		}
		return 0;
	}
}


int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n - 1 == m) {
		return Work1::main();
	}
	if (n == m) {
		return Work2::main();
	}
	return 0;
}
