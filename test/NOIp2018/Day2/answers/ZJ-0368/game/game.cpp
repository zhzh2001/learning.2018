#include <cstdio>
#include <algorithm>
#define maxn 11
const int mod = 1e9 + 7;
int n, m, ans;
int s[1 << 13], V[1 << 13], cnt;
int p[maxn][maxn];
void check(int x, int y, int now, int sta) {
	if (x + y >= n + m) {
		s[sta] = now;
		return ;
	}
	if (x + 1 <= n) check(x + 1, y, now << 1 | p[x + 1][y], sta << 1 | 1);
	if (y + 1 <= m) check(x, y + 1, now << 1 | p[x][y + 1], sta << 1);
}
bool solve() {
	check(1, 1, 0, 0);
	for (int i = 1; i <= cnt; i++) if (s[V[i]] < s[V[i - 1]]) return false;
	return true;
}
void dfs(int x) {
	if (x >= n * m) {
		if (solve()) ans++;
		return ;
	}
	int a = x / m + 1, b = x % m + 1;
	p[a][b] = 1;
	dfs(x + 1);
	p[a][b] = 0;
	dfs(x + 1);
}
inline int count(int x) {
	int res = 0;
	while (x) {
		res += x & 1;
		x >>= 1;
	}
	return res;
}
inline int pw(int base, int p) {
	int res = 1;
	for (; p; p >>= 1, base = static_cast<long long> (base) * base % mod) if (p & 1) res = static_cast<long long> (res) * base % mod;
	return res;
}
int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n > m) std::swap(n, m);
	if (n == 1) {
		printf("%d\n", pw(2, m));
		return 0;
	}
	if (n == 2) {
		printf("%lld\n", pw(3, m - 1) * 4ll % mod);
		return 0;
	}
	if (n == 3) {
		printf("%lld\n", pw(3, m - 3) * 112ll % mod);
		return 0;
	}
	if (n == 4 && m != 4) {
		printf("%lld\n", pw(3, m - 5) * 2688ll % mod);
		return 0;
	}
	
	int tmp = (1 << n - 1) - 1;
	for (int i = tmp; i <= (tmp << m - 1); i++) if (count(i) == n - 1) V[++cnt] = i;
	dfs(0);
	printf("%d\n", ans);
	return 0;
}
