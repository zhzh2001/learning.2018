#include<bits/stdc++.h>
#define N 200005
using namespace std;
struct Edge{
	int to,nxt;
}E[N*2];
char s;
bool skip;
long long dp[N][2],n,m,x,y,a,b,p[N],Head[N],tot;
void addedge(int x,int y){
	E[++tot]=(Edge){y,Head[x]};
	Head[x]=tot;
}
void dfs(int v,int fa){
//	dp[v][1]=p[v]; //dp[v][0]=1023456789;
	dp[v][1]=p[v]; dp[v][0]=0;
	for (int i=Head[v];i;i=E[i].nxt){
		if (E[i].to==fa) continue;
		dfs(E[i].to,v);
//		if ((v!=a || x==0)&&(v!=b || y==0))
		dp[v][0]+=dp[E[i].to][1];
		if (E[i].to==a && x==0)
			/*dp[v][0]-=dp[E[i].to][1],*/dp[v][0]+=dp[E[i].to][0];
		if (E[i].to==b && y==0)
		/*	dp[v][0]-=dp[E[i].to][1],*/dp[v][0]+=dp[E[i].to][0];
//		if ((v!=a || x==1)&&(v!=b || y==1))
		if ((E[i].to!=a)&&(E[i].to!=b))
			dp[v][1]+=min(dp[E[i].to][0],dp[E[i].to][1]);
		if (E[i].to==a)
			if (x==0) 
				dp[v][1]+=dp[E[i].to][0]; else
				dp[v][1]+=dp[E[i].to][1];
		if (E[i].to==b)
			if (y==0)
				dp[v][1]+=dp[E[i].to][0]; else
				dp[v][1]+=dp[E[i].to][1];
	}
//	dp[v][1]=min(dp[v][1],p[v]);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%lld%lld%s",&n,&m,&s);
	for (int i=1;i<=n;i++) scanf("%lld",&p[i]);
	for (int i=1;i<n;i++){
		scanf("%lld%lld",&x,&y);
		addedge(x,y);
		addedge(y,x);
	}
	for (int i=1;i<=m;i++){
		scanf("%lld%lld%lld%lld",&a,&x,&b,&y);
		skip=false;
		for (int j=Head[a];j;j=E[j].nxt)
			if (E[j].to==b) skip=true;
		if (skip){
			if (x==0 && y==0){
				printf("-1\n");
				continue;
			}
		} 
		dfs(1,0);
		if (a==1 && x==0) dp[1][1]=dp[1][0];
		if (a==1 && x==1) dp[1][0]=dp[1][1];
		if (b==1 && y==0) dp[1][1]=dp[1][0];
		if (b==1 && y==1) dp[1][0]=dp[1][1];
		printf("%lld\n",min(dp[1][0],dp[1][1]));
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
