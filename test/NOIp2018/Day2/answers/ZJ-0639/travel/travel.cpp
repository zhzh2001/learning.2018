#include<bits/stdc++.h>
#define N 10005
using namespace std;
struct Edge{
	int to,nxt;
}E[N*2];
int Head[N],x,y,n,m,tot,colcnt,col[N],dfn[N],tim,low[N],Q[N*2],l,r,xx[N],yy[N],a[N],cnt,ans[N];
bool vis[N];
void addedge(int x,int y){
	E[++tot]=(Edge){y,Head[x]};
	Head[x]=tot;
}
void dfs(int v,int fa){
	a[++cnt]=v;
	vis[v]=true;
	for (int i=Head[v];i;i=E[i].nxt){
		int minn=1023456789;
		for (int j=Head[v];j;j=E[j].nxt){
			if (E[j].to==fa) continue;
			if (vis[E[j].to]) continue;
			minn=min(minn,E[j].to);
		}
		if (minn!=1023456789)
			dfs(minn,v);
	}
}
void read(){
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
		scanf("%d%d",&xx[i],&yy[i]);
}
void solve_tree(){
	for (int i=1;i<=m;i++){
		addedge(xx[i],yy[i]);
		addedge(yy[i],xx[i]);
	}
	for (int i=0;i<=n;i++) vis[i]=false;
	dfs(1,0);
	for (int i=1;i<=n;i++)
		printf("%d ",a[i]);
}
void solve(){
	for (int i=1;i<=n;i++) ans[i]=n;
	for (int k=1;k<=m;k++){
		tot=0; 
		memset(E,0,sizeof(E));
		memset(Head,0,sizeof(Head)); memset(a,0,sizeof(a));
		cnt=0;
		for (int i=1;i<=m;i++){
			if (i==k) continue;
			addedge(xx[i],yy[i]);
			addedge(yy[i],xx[i]);
		}
		for (int i=0;i<=n;i++) vis[i]=false;
		dfs(1,0);
		if (cnt==n){
			for (int i=1;i<=n;i++){
				if (ans[i]==a[i]) continue;
				if (ans[i]<a[i]) break;
				for (int j=1;j<=n;j++)
					ans[j]=a[j];
				break;
			}
		}
	}
	for (int i=1;i<=n;i++)
		printf("%d ",ans[i]);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read();
	if (m==n)
		solve(); else 
		solve_tree();
	fclose(stdin);fclose(stdout);
	return 0;
}

