#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define sqz main
#define ll long long
#define rep(i, a, b) for (int i = (a); i <= (b); i++)
#define per(i, a, b) for (int i = (a); i >= (b); i--)
#define Rep(i, a, b) for (int i = (a); i < (b); i++)
#define travel(i, u) for (int i = head[u]; ~i; i = edge[i].next)

const int INF = 1e9, Mo = INF + 7;
const int N = 1000000;
const double eps = 1e-6, pi = acos(-1.0);
ll read()
{
	ll x = 0; int zf = 1; char ch;
	while (ch != '-' && (ch < '0' || ch > '9')) ch = getchar();
	if (ch == '-') zf = -1, ch = getchar();
	while (ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x * zf;
}
void write(ll y)
{
	if (y < 0) putchar('-'), y = -y;
	if (y > 9) write(y / 10);
	putchar(y % 10 + '0');
}

int n, m, F[N + 5][10][10];
ll ans = 0;
ll pow(ll a, ll b, ll Mo)
{
	ll ans = 1;
	while (b)
	{
		if (b & 1) ans = ans * a % Mo;
		a = a * a % Mo;
		b >>= 1;
	}
	return ans;
}
int Check(int x, int y, int z)
{
	Rep(i, 2, n)
	{
		int a = (x & (1 << i - 1)) ? 1 : 0, b = (y & (1 << i - 2)) ? 1 : 0;
		int c = (y & (1 << i)) ? 1 : 0, d = (z & (1 << i - 1)) ? 1 : 0;
		if (a == c && b > d) return 0;
	}
	return 1;
}
int sqz()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	n = read(), m = read();
	if (n <= 3 && m <= 3)
	{
		if (n > m) swap(n, m);
		if (n == 1 && m == 1) puts("2");
		if (n == 1 && m == 2) puts("4");
		if (n == 1 && m == 3) puts("8");
		if (n == 2 && m == 2) puts("12");
		if (n == 2 && m == 3) puts("36");
		if (n == 3 && m == 3) puts("112");
		fclose(stdin); fclose(stdout);
		return 0;
	}
	if (n == 2) printf("%lld\n", pow(3, m - 1, Mo) * 4 % Mo);
	else if (m == 2) printf("%lld\n", pow(3, n - 1, Mo) * 4 % Mo);
	else
	{
		Rep(i, 0, 1 << n) F[1][i][(1 << n) - 1] = 1;
		Rep(i, 0, 1 << n)
			Rep(j, 0, 1 << n) F[2][i][j] = 1;
		rep(i, 3, m)
			Rep(j, 0, 1 << n)
			{
				int ttt = 0;
				int t = j >> 1;
				for (int x = t; x < (1 << n - 1); x = t | (x + 1))
				{
					int k = x;
					int p = (k >> 1);
					for (int y = p; y < (1 << n - 1); y = p | (y + 1))
					{
						int l = y;
						if (Check(l, k, j)) (F[i][j][k] += F[i - 1][k][l]) %= Mo;
						l = (1 << n - 1) + y;
						if (Check(l, k, j)) (F[i][j][k] += F[i - 1][k][l]) %= Mo;
					}
					if (i == m) ans = (ans + F[i][j][k]) % Mo;
					ttt += F[i][j][k];
					k = (1 << n - 1) + x;
					p = (k >> 1);
					for (int y = p; y < (1 << n - 1); y = p | (y + 1))
					{
						int l = y;
						if (Check(l, k, j)) (F[i][j][k] += F[i - 1][k][l]) %= Mo;
						l = (1 << n - 1) + y;
						if (Check(l, k, j)) (F[i][j][k] += F[i - 1][k][l]) %= Mo;
					}
					if (i == m) ans = (ans + F[i][j][k]) % Mo;
					ttt += F[i][j][k];
				}
			}
		printf("%lld\n", ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
