#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
#define sqz main
#define ll long long
#define rep(i, a, b) for (int i = (a); i <= (b); i++)
#define per(i, a, b) for (int i = (a); i >= (b); i--)
#define Rep(i, a, b) for (int i = (a); i < (b); i++)
#define travel(i, u) for (int i = head[u]; ~i; i = edge[i].next)

const int INF = 1e9, N = 5000;
const double eps = 1e-6, pi = acos(-1.0);
ll read()
{
	ll x = 0; int zf = 1; char ch;
	while (ch != '-' && (ch < '0' || ch > '9')) ch = getchar();
	if (ch == '-') zf = -1, ch = getchar();
	while (ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x * zf;
}
void write(ll y)
{
	if (y < 0) putchar('-'), y = -y;
	if (y > 9) write(y / 10);
	putchar(y % 10 + '0');
}

int T[N + 5], X[N + 5], Ans[N + 5], P[N + 5], Q[N + 5], Flag[N + 5];
int n, m, cnt;
vector<int> Edge[N + 5];
void dfs(int u, int fa)
{
	T[++cnt] = u; Flag[u] = 1;
	Rep(i, 0, Edge[u].size())
	{
		int v = Edge[u][i];
		if (v == fa || v == X[u] || Flag[v]) continue;
		dfs(v, u);
	}
}
void Check()
{
	int flag = 1;
	rep(i, 1, n) if (Ans[i]) flag = 0;
	if (flag)
	{
		rep(i, 1, n) Ans[i] = T[i];
		return;
	}
	rep(i, 1, n)
	{
		if (Ans[i] == T[i]) continue;
		if (Ans[i] < T[i]) return;
		rep(j, i, n) Ans[j] = T[j];
		return;
	}
}
int sqz()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	n = read(), m = read();
	rep(i, 1, m) 
	{
		int u = read(), v = read();
		Edge[u].push_back(v);
		Edge[v].push_back(u);
		P[i] = u, Q[i] = v;
	}
	rep(i, 1, n) sort(Edge[i].begin(), Edge[i].end());
	if (m == n - 1) 
	{
		cnt = 0;
		dfs(1, 0);
		rep(i, 1, n) printf("%d%c", T[i], i == n ? '\n' : ' ');
	}
	else
	{
		rep(i, 1, m)
		{
			rep(j, 1, n) Flag[j] = 0;
			cnt = 0;
			X[P[i]] = Q[i];
			X[Q[i]] = P[i];
			dfs(1, 0);
			if (cnt == n) Check();
			X[P[i]] = 0;
			X[Q[i]] = 0;
		}
		rep(i, 1, n) printf("%d%c", Ans[i], i == n ? '\n' : ' ');
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
/*
6 5
1 3
2 3
2 5
3 4
4 6

1 3 2 5 4 6
*//*
6 6
1 3
2 3
2 5
3 4
4 6
7 8

1 3 2 4 5 6
*/
