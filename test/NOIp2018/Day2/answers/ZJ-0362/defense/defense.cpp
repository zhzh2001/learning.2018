#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define sqz main
#define ll long long
#define rep(i, a, b) for (int i = (a); i <= (b); i++)
#define per(i, a, b) for (int i = (a); i >= (b); i--)
#define Rep(i, a, b) for (int i = (a); i < (b); i++)
#define travel(i, u) for (int i = head[u]; ~i; i = edge[i].next)

const ll INF = 1e10;
const int N = 100000;
const double eps = 1e-6, pi = acos(-1.0);
ll read()
{
	ll x = 0; int zf = 1; char ch;
	while (ch != '-' && (ch < '0' || ch > '9')) ch = getchar();
	if (ch == '-') zf = -1, ch = getchar();
	while (ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x * zf;
}
void write(ll y)
{
	if (y < 0) putchar('-'), y = -y;
	if (y > 9) write(y / 10);
	putchar(y % 10 + '0');
}

ll F[N + 5][2];
int head[N + 5], T[N + 5], X[N + 5];
int edgenum = 0;
char st[5];
struct node
{
	int vet, next;
}edge[2 * N + 5];
void addedge(int u, int v)
{
	edge[edgenum].vet = v;
	edge[edgenum].next = head[u];
	head[u] = edgenum++;
}
void dfs(int u, int fa)
{
	F[u][0] = 0, F[u][1] = X[u];
	travel(i, u)
	{
		int v = edge[i].vet;
		if (v == fa) continue;
		dfs(v, u);
		F[u][0] += F[v][1];
		F[u][1] += min(F[v][0], F[v][1]);
	}
	if (T[u] != -1) F[u][1 - T[u]] = INF;
}
int sqz()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	int n = read(), m = read();
	scanf("%s", st);
	rep(i, 1, n) X[i] = read(), head[i] = -1;
	Rep(i, 1, n)
	{
		int u = read(), v = read();
		addedge(u, v), addedge(v, u);
	}
	if (n <= 10000)
	{
		while (m--)
		{
			rep(i, 1, n) T[i] = -1;
			int a = read(), x = read(), b = read(), y = read();
			T[a] = x, T[b] = y;
			dfs(1, 0);
			ll t = min(F[1][1], F[1][0]);
			printf("%lld\n", t >= INF ? -1 : t);
		}
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
