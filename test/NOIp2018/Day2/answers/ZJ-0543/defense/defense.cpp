#include<iostream>
#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
inline int rad(){
	int ret=0,flg=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-')flg=-1;ch=getchar();}
	while (isdigit(ch))ret=(ret<<3)+(ret<<1)+ch-'0',ch=getchar();
	return ret*flg;
}
const int maxn=1e5+6,INF=0x7fffffff;
int n,m,f[maxn][2],lnk[maxn],nxt[2*maxn],w[2*maxn],p[maxn],vis[maxn],sof,tot,son[maxn];
void dp(int x,int fa){
	f[x][0]=0,f[x][1]=p[x];
	for (int i=lnk[x];i;i=nxt[i]){
		int u=son[i];
		if (u==fa)continue;
		dp(u,x);
		if (!vis[x]){
			if (!vis[u]){sof=0;return;}
			f[x][0]+=f[u][1];
		}else if (vis[x]==1){
			if (!vis[u])f[x][1]+=f[u][0];else
			if (vis[u]==1)f[x][1]+=f[u][1];else
			f[x][1]+=min(f[u][0],f[u][1]);
		}else{
			if (!vis[u])vis[x]=1;else f[x][0]+=f[u][1];
			if (!vis[u])f[x][1]+=f[u][0];else
			if (vis[u]==1)f[x][1]+=f[u][1];else
			f[x][1]+=min(f[u][0],f[u][1]);
		}
	}
//	printf("%d %d %d %d\n",x,f[x][0],f[x][1],vis[x]);
}
inline void add(int x,int y){
	son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	string tpe;
	cin>>n>>m>>tpe;
	for (int i=1;i<=n;i++)p[i]=rad();
	for (int i=1;i<n;i++){
		int x=rad(),y=rad();
		add(x,y),add(y,x);
	}
	int a=0,b=0,x=0,y=0;
	for (int i=1;i<=m;i++){
		for (int j=1;j<=n;j++)vis[j]=2;
		vis[a]=vis[b]=2,sof=1;
		a=rad(),x=rad(),b=rad(),y=rad();
		vis[a]=x,vis[b]=y;
		dp(1,0);
		if (!sof)printf("-1\n");else
		if (!vis[1])printf("%d\n",f[1][0]);else
		if (vis[1]==1)printf("%d\n",f[1][1]);else
		printf("%d\n",min(f[1][0],f[1][1]));
	}
	return 0;
}
