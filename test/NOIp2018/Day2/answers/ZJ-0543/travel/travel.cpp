#include<cstdio>
#include<cctype>
#include<algorithm>
#include<queue>
using namespace std;
inline int rad(){
	int ret=0,flg=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-')flg=-1;ch=getchar();}
	while (isdigit(ch))ret=(ret<<3)+(ret<<1)+ch-'0',ch=getchar();
	return ret*flg;
}
const int maxn=5006,INF=0x7fffffff;
int lnk[maxn],nxt[2*maxn],son[2*maxn],vis[maxn],n,m,ans[maxn],til,hed,que[maxn],tot,cnt,rot[maxn][maxn];
inline int cmp(const int x,const int y){
	for (int i=1;i<=min(rot[x][0],rot[y][0]);i++){
		if (rot[x][i]<rot[y][i])return 1;
		if (rot[y][i]<rot[x][i])return 0;
	}
	return 0;
}
void get_rot(int x,int fa){
	rot[x][rot[x][0]=1]=x;
	int id[maxn];id[0]=0;
	for (int i=lnk[x];i;i=nxt[i]){
		int u=son[i];
		if (u==fa)continue;
		get_rot(u,x),id[++id[0]]=u;
	}
	sort(id+1,id+1+id[0],cmp);
	for (int i=1;i<=id[0];i++)
	for (int j=1;j<=rot[id[i]][0];j++)rot[x][++rot[x][0]]=rot[id[i]][j];
}
inline void add(int x,int y){
	son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot;
}
inline void work_1(){
	for (int i=1;i<=m;i++){
		int x=rad(),y=rad();
		add(x,y),add(y,x);
	}
	get_rot(1,0);
	for (int i=1;i<=rot[1][0];i++)printf("%d%c",rot[1][i],i==rot[1][0]?'\n':' ');
}
struct ypc{
	int x;
	inline bool operator < (const ypc b)const{
		return x>b.x;
	}
};
priority_queue<ypc>hep;
inline void work_2(){
	for (int i=1;i<=m;i++){
		int x=rad(),y=rad();
		add(x,y),add(y,x);
	}
	int ans[maxn];ans[ans[0]=1]=1;
	que[til=1]=1,hed=0,vis[1]=1;
	while (hed!=til){
		int x=que[++hed];
		for (int i=lnk[x];i;i=nxt[i]){
			int u=son[i];
			if (vis[u])continue;
			hep.push((ypc){u}),vis[que[++til]=u]=1;
		}
		ypc tmp=hep.top();hep.pop();
		ans[++ans[0]]=tmp.x;
	}
	for (int i=1;i<=n;i++)printf("%d%c",ans[i],i==n?'\n':' ');
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=rad(),m=rad();
	if (m<n)work_1();else work_2();
	return 0;
}
