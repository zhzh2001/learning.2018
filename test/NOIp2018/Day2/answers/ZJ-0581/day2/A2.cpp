#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
template<class T>inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(; isdigit(ch);ch=getchar())x=x*10+(ch-48);
	x*=f;
}
inline void Setfile(string Name="") {
	if(Name=="")
		return;
	freopen((Name+".in").c_str(),"r",stdin);
	freopen((Name+".out").c_str(),"w",stdout);
}
const int maxn=5005;
struct Edge {
	int u,v,f;
	Edge(int u=0,int v=0):u(u),v(v){}
}e[maxn<<1];
int n,m,ans[maxn],res[maxn],len,bel[maxn],dfn[maxn],low[maxn],clk,cnt,stk[maxn],tp,ins[maxn],sz[maxn],no=-1,del,bidu,bidv,vis[maxn];
vector<int>G[maxn];
set<int>now;
inline void calc(int u=1,int p=0) {
	res[++len]=u;
	if(bel[p]==no) {
		if(!del&&now.size()&&*now.begin()<u) {
			res[len--]=0,del=1,bidu=p,bidv=u;
			return;
		}
	}
	vis[u]=1;
	if(now.find(u)!=now.end())
		now.erase(now.find(u));
	cerr<<"Calc "<<u<<" "<<p<<" "<<len<<" "<<res[len]<<" "<<bidu<<" "<<bidv<<" "<<del<<endl;
	for(set<int>::iterator it=now.begin();it!=now.end();++it)
		cout<<*it<<" ";
	cout<<endl;
	vector<int>son;
	int fa1=0,fa2=0;
	for(int i=0;i<(int)G[u].size();++i) {
		int k=G[u][i],v=e[k].v;
		if(v==p||(bidu==u&&bidv==v)||(bidu==v&&bidv==u))
			continue;
		son.push_back(v);
		if(bel[u]==no)
			now.insert(v);
	}
	int tot=son.size();
	sort(son.begin(),son.end());
	/*for(int i=0;i<tot;++i)
		cout<<son[i]<<" ";
	cout<<endl;*/
	for(int i=0;i<tot;++i)
		if(!vis[son[i]])
			calc(son[i],u);
}
inline void Tarjan(int u=1,int p=0) {
	//cerr<<"!!!Tarjan :"<<u<<endl;
	dfn[u]=low[u]=++clk,stk[++tp]=u,ins[u]=1;
	for(int i=0;i<(int)G[u].size();++i) {
		int k=G[u][i],v=e[k].v;
		if(v==p)
			continue;
		//cerr<<"!!!!"<<k<<" "<<u<<" -> "<<v<<endl;
		if(!dfn[v])
			Tarjan(v,u),low[u]=min(low[u],low[v]);
		else if(ins[v])
			low[u]=min(low[u],low[v]);
	}
	if(low[u]==dfn[u]) {
		++cnt;
		while(1) {
			int x=stk[tp--];
			bel[x]=cnt;
			ins[x]=0;
			sz[cnt]++;
			if(x==u)
				break;
		}
		if(!~no||sz[cnt]>sz[no])
			no=cnt;
	}
}
int main() {
	Setfile();
	read(n),read(m);
	for(int i=1,x,y;i<=m;++i)
		read(x),read(y),e[i]=Edge(x,y),e[i+m]=Edge(y,x),G[x].push_back(i),G[y].push_back(i+m);
	if(n==m+1) {
		len=0;
		calc();
		for(int i=1;i<=n;++i)
			printf("%d ",res[i]);
		puts("");
		return 0;
	}
	else {
		Tarjan();
		if(bel[1]==no)
			now.insert(1);
		calc();
		for(int i=1;i<=n;++i)
			printf("%d ",res[i]);
		puts("");
	}
	return 0;
}
