#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
template<class T>inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(; isdigit(ch);ch=getchar())x=x*10+(ch-48);
	x*=f;
}
inline void Setfile(string Name="") {
	if(Name=="")
		return;
	freopen((Name+".in").c_str(),"r",stdin);
	freopen((Name+".out").c_str(),"w",stdout);
}
const int mod=1e9+7,mx=1<<8;
inline int Add(int x,int y) {
	return (x+=y)>=mod?x-mod:x;
}
inline int Sub(int x,int y) {
	return (x-=y)<0?x+mod:x;
}
inline int Mul(int x,int y) {
	return 1ll*x*y%mod;
}
inline int Pow(int x,int y) {
	int res=1;
	for(;y;x=Mul(x,x),y>>=1)
		if(y&1)
			res=Mul(res,x);
	return res;
}
int n,m,f[2][mx];
inline bool check(int x,int y) {
	for(int i=0;i<n-1;++i)
		if((y>>i&1)>(x>>(i+1)&1))
			return 0;
	return 1;
}
int main() {
	Setfile();
	read(n),read(m);
	if(n>m)
		swap(n,m);
	if(n==1)
		return 0*printf("%d\n",Pow(2,m));
	if(n==2)
		return 0*printf("%d\n",Mul(4,Pow(3,m-1)));
	if(n==3)
		return 0*printf("%d\n",Mul(112,Pow(3,m-3)));
	if(n==4)
		return 0*printf("%d\n",m==4?912:Mul(2688,Pow(3,m-5)));
	int cur=0;
	for(int i=0;i<1<<n;++i)
		f[cur][i]=1;
	for(int i=2;i<=m;++i) {
		cur^=1;
		memset(f[cur],0,sizeof f[cur]);
		for(int j=0;j<1<<n;++j)
			for(int k=0;k<1<<n;++k)
				if(f[cur^1][k]&&check(k,j))
					f[cur][j]=Add(f[cur][j],f[cur^1][k]);
	}
	int ans=0;
	for(int i=0;i<1<<n;++i)
		ans=Add(ans,f[cur][i]);
	printf("%d\n",ans);
	return 0;
}
