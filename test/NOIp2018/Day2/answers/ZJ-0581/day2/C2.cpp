#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
template<class T>inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(; isdigit(ch);ch=getchar())x=x*10+(ch-48);
	x*=f;
}
inline void Setfile(string Name="") {
	if(Name=="")
		return;
	freopen((Name+".in").c_str(),"r",stdin);
	freopen((Name+".out").c_str(),"w",stdout);
}
const ll inf=1e15;
int n,q,a,x,b,y,val[100005],vis[100005];
char type[10];
vector<int>G[100005];
ll f[100005][2],res;
namespace nn {
	inline void chk(int u) {
		if(u==a)
			f[u][!x]=inf;
		if(u==b)
			f[u][!y]=inf;
	}
	inline void calc(int u=1,int p=0) {
		for(int i=0;i<2;++i)
			f[u][i]=inf;
		if(G[u].size()==1&&G[u][0]==p) {
			f[u][0]=0;
			f[u][1]=val[u];
			chk(u);
			return;
		}
		vector<int>son;
		for(int i=0;i<(int)G[u].size();++i) {
			int v=G[u][i];
			if(v==p)
				continue;
			calc(v,u);
			son.push_back(v);
		}
		int tot=son.size();
		f[u][0]=0;
		for(int i=0;i<tot;++i)
			f[u][0]+=f[son[i]][1];
		f[u][1]=val[u];
		for(int i=0;i<tot;++i)
			f[u][1]+=min(f[son[i]][0],f[son[i]][1]);
		for(int i=0;i<2;++i)
			f[u][i]=min(f[u][i],inf);
		chk(u);
	}
	inline void solve() {
		while(q--) {
			read(a),read(x),read(b),read(y),calc();
			/*for(int i=1;i<=n;++i)
				cout<<f[i][0]<<" "<<f[i][1]<<endl;*/
			ll ans=min(f[1][0],f[1][1]);
			printf("%lld\n",ans==inf?-1:ans);
		}
	}
}
int fa[100005];
inline void mark(int u=1,int p=0) {
	f[u][0]=f[u][1]=inf,fa[u]=p;
	if(G[u].size()==1&&G[u][0]==p) {
		f[u][0]=0;
		f[u][1]=val[u];
		return;
	}
	for(int i=0;i<(int)G[u].size();++i) {
		int v=G[u][i];
		if(v==p)
			continue;
		mark(v,u);
		f[u][0]=f[v][1];
		f[u][1]=min(f[v][0],f[v][1])+val[u];
	}
}
inline void doit(int u,int s,int pre,int lst) {
	if(s==1) {
		res+=val[u];
		if(pre) {
			if(u==2&&vis[1])
				res-=val[1];
			if(u>2&&vis[u-1]&&vis[u-2])
				res-=val[u-1];
		}
		if(lst) {
			if(u==n-1&&vis[n])
				res-=val[n];
			if(u<n-1&&vis[u+1]&&vis[u+2])
				res-=val[u+1];
		}
	}
	else {
		res-=val[u];
		if(u>1&&!vis[u-1])
			doit(u-1,1,1,0);
		if(u<n&&!vis[u+1])
			doit(u+1,1,0,1);
	}
}
int main() {
	Setfile();
	read(n),read(q);
	scanf("%s",type);
	for(int i=1;i<=n;++i)
		read(val[i]);
	for(int i=1,u,v;i<n;++i)
		read(u),read(v),G[u].push_back(v),G[v].push_back(u);
	if(type[0]=='A') {
		mark();
		if(f[1][0]<=f[1][1])
			vis[1]=0;
		else
			vis[1]=1;
		for(int i=2;i<=n;++i)
			if(vis[i-1]) {
				if(f[i][0]==f[i-1][1]-val[i-1])
					vis[i]=0;
				else
					vis[i]=1;
			}
			else
				vis[i]=1;
		ll ans=min(f[1][0],f[1][1]);
		for(int i=1;i<=n;++i)
			cout<<vis[i]<<" ";
		cout<<endl;
		while(q--) {
			read(a),read(x),read(b),read(y);
			if(!x&&!y&&abs(a-b)==1) {
				puts("-1");
				continue;
			}
			res=ans;
			if(vis[a]!=x)
				doit(a,x,1,1);
			if(vis[b]!=y)
				doit(b,y,1,1);
			printf("%lld\n",res);
		}
		return 0;
	}
	return 0;
}
