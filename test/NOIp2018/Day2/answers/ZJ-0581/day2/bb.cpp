#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
template<class T>inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(; isdigit(ch);ch=getchar())x=x*10+(ch-48);
	x*=f;
}
inline void Setfile(string Name="") {
	if(Name=="")
		return;
	freopen((Name+".in").c_str(),"r",stdin);
	freopen((Name+".out").c_str(),"w",stdout);
}
int n,m,bk[30][30],pa[1<<26],ans;
inline void solve(int x,int y,int st,int ts) {
	if(x==n&&y==m) {
		pa[st]=ts+1;
		return;
	}
	if(y<m)
		solve(x,y+1,st<<1|1,ts<<1|bk[x][y+1]);
	if(x<n)
		solve(x+1,y,st<<1,ts<<1|bk[x+1][y]);
}
inline bool check() {
	solve(1,1,0,bk[1][1]);
	vector<int>vec;
	for(int i=0;i<1<<(n+m-2);++i)
		if(pa[i]) {cerr<<i<<" "<<pa[i]<<endl;
			vec.push_back(pa[i]);}
	for(int i=1;i<vec.size();++i)
		if(vec[i]>vec[i-1])
			return 0;
	return 1;
}
inline void dfs(int x,int y) {
	//cerr<<"!!!"<<x<<" "<<y<<endl;
	if(x==n+1) {
		if(check())
			ans++;
		else {cerr<<"----------"<<endl;
			for(int i=1;i<=n;++i,puts(""))
				for(int j=1;j<=m;++j)
					cout<<bk[i][j]<<" ";cerr<<"----------"<<endl;
		}
		return;
	}
	bk[x][y]=0;
	dfs(x+(y==m),y==m?1:y+1);
	bk[x][y]=1;
	dfs(x+(y==m),y==m?1:y+1);
	bk[x][y]=0;
}
int main() {
	Setfile();
	read(n),read(m);
	dfs(1,1);
	cout<<ans<<endl;
	return 0;
}
