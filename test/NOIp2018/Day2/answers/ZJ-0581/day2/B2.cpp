#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
template<class T>inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(; isdigit(ch);ch=getchar())x=x*10+(ch-48);
	x*=f;
}
inline void Setfile(string Name="") {
	if(Name=="")
		return;
	freopen((Name+".in").c_str(),"r",stdin);
	freopen((Name+".out").c_str(),"w",stdout);
}
const int mod=1e9+7,mx=1<<8;
inline int Add(int x,int y) {
	return (x+=y)>=mod?x-mod:x;
}
inline int Sub(int x,int y) {
	return (x-=y)<0?x+mod:x;
}
inline int Mul(int x,int y) {
	return 1ll*x*y%mod;
}
int n,m,f[1000005][mx];
inline bool check(int x,int y) {
	for(int i=0;i<n-1;++i)
		if((y>>i&1)>(x>>(i+1)&1))
			return 0;
	return 1;
}
int main() {
	Setfile();
	read(n),read(m);
	int cur=0;
	for(int i=0;i<1<<n;++i)
		f[1][i]=1;
	for(int i=2;i<=m;++i)
		for(int j=0;j<1<<n;++j)
			for(int k=0;k<1<<n;++k)
				if(f[i-1][k]&&check(k,j)) {
					//cerr<<k<<" -> "<<j<<endl;
					f[i][j]=Add(f[i][j],f[i-1][k]);
				}
	for(int i=1;i<=m;++i,puts(""))
		for(int j=0;j<1<<n;++j)
			cout<<f[i][j]<<" ";
	int ans=0;
	for(int i=0;i<1<<n;++i)
		ans=Add(ans,f[m][i]);
	printf("%d\n",ans);
	return 0;
}
