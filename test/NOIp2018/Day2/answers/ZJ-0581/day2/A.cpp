#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
template<class T>inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(; isdigit(ch);ch=getchar())x=x*10+(ch-48);
	x*=f;
}
inline void Setfile(string Name="") {
	if(Name=="")
		return;
	freopen((Name+".in").c_str(),"r",stdin);
	freopen((Name+".out").c_str(),"w",stdout);
}
const int maxn=5005;
struct Edge {
	int u,v,f;
	Edge(int u=0,int v=0,int f=1):u(u),v(v),f(f){}
}e[maxn<<1];
int n,m,ans[maxn],res[maxn],len,bel[maxn],dfn[maxn],low[maxn],clk,cnt,stk[maxn],tp,ins[maxn],no,sam,worse;
vector<int>G[maxn];
inline void calc(int u=1,int p=0) {
	res[++len]=u;
	//cerr<<"Calc "<<u<<" "<<p<<" "<<len<<" "<<res[len]<<" "<<ans[len]<<" "<<no<<" "<<sam<<endl;
	if(no&&sam&&res[len]>ans[len]) {
		worse=1;
		return;
	}
	sam&=res[len]==ans[len];
	vector<int>son;
	for(int i=0;i<(int)G[u].size();++i) {
		int k=G[u][i],v=e[k].v;
		if(!e[k].f)
			continue;
		if(v==p)
			continue;
		son.push_back(v);
	}
	int tot=son.size();
	sort(son.begin(),son.end());
	/*for(int i=0;i<tot;++i)
		cout<<son[i]<<" ";
	cout<<endl;*/
	for(int i=0;i<tot;++i) {
		calc(son[i],u);
		if(worse)
			return;
	}
}
inline void Tarjan(int u=1,int p=0) {
	//cerr<<"!!!Tarjan :"<<u<<endl;
	dfn[u]=low[u]=++clk,stk[++tp]=u,ins[u]=1;
	for(int i=0;i<(int)G[u].size();++i) {
		int k=G[u][i],v=e[k].v;
		if(v==p)
			continue;
		//cerr<<"!!!!"<<k<<" "<<u<<" -> "<<v<<endl;
		if(!dfn[v])
			Tarjan(v,u),low[u]=min(low[u],low[v]);
		else if(ins[v])
			low[u]=min(low[u],low[v]);
	}
	if(low[u]==dfn[u]) {
		++cnt;
		while(1) {
			int x=stk[tp--];
			bel[x]=cnt;
			ins[x]=0;
			if(x==u)
				break;
		}
	}
}
inline void chkmin() {
//	cerr<<worse<<endl;
	if(worse)
		return;
	if(!no) {
		for(int i=1;i<=n;++i)
			ans[i]=res[i];
		no=1;
		return;
	}
	for(int i=1;i<=n;++i)
		if(ans[i]<res[i])
			return;
		else if(ans[i]>res[i])
			break;
	for(int i=1;i<=n;++i)
		ans[i]=res[i];
}
int main() {
	Setfile();
	read(n),read(m);
	for(int i=1,x,y;i<=m;++i)
		read(x),read(y),e[i]=Edge(x,y),e[i+m]=Edge(y,x),G[x].push_back(i),G[y].push_back(i+m);
	if(n==m+1) {
		len=0;
		calc();
		for(int i=1;i<=n;++i)
			printf("%d ",res[i]);
		puts("");
		return 0;
	}
	else {
		Tarjan();
		/*for(int i=1;i<=n;++i)
			cout<<bel[i]<<" "<<dfn[i]<<" "<<low[i]<<endl;*/
		for(int i=m;i;--i)
			if(bel[e[i].u]==bel[e[i].v]) {
				//cerr<<"Now close "<<e[i].u<<" "<<e[i].v<<endl;
				e[i].f=0,e[i+m].f=0;
				len=0,sam=1,worse=0;
				calc();
				chkmin();
				e[i].f=1,e[i+m].f=1;
			}
		for(int i=1;i<=n;++i)
			printf("%d ",ans[i]);
		puts("");
	}
	return 0;
}
