#include <bits/stdc++.h>
using namespace std;
#define N 111111
#define INF 1111111
#define M 5555
int w,n,m;
bool flag[N];
int way[N];
int a[M][M];
int tot;
int c[N],d[N],b[N],lp[N];
priority_queue <int,vector<int>,greater<int> > q;
void sc(int x){
	if (!flag[x]){
		w++;way[w]=x;
	}
	flag[x]=true;
	int minx=INF;
	for (int i=1;i<=n;++i){
		if (a[x][i] && !flag[i]){
			sc(i);
		}
	}
}
void add(int x,int y){
	tot++;d[tot]=y;b[tot]=c[x];c[x]=tot;
}
void getloop(int x,int tg,int last){
	flag[x]=true;
	for (int i=c[x];i;i=b[i]){
		int to=d[i];
		if (to!=last){
			if (to==tg){
				lp[tg]=true;
				return;
			}
			if (!flag[to]) getloop(to,tg,x);
		}
	}
}
int sc1(int x){
	if (!flag[x]){
		w++;way[w]=x;
	}
	int sp=false;
	if (lp[x]){
		for (int i=1;i<=n;++i){
			if (a[x][i] && !lp[i] && !flag[i]){
				sp=true;
			}
		}	
	}
	flag[x]=true;
	if (sp){
		for (int i=1;i<=n;++i){
			if (a[x][i] && !flag[i]){
				sc(i);
			}
		}		
	}else{
		for (int i=c[x];i;i=b[i]){
			if (a[x][d[i]] && !flag[d[i]]){
				q.push(d[i]);
			}
 		}
 		if (q.empty()) return 0;
 		int to=q.top();
 		q.pop();
 		sc1(to);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;++i){
		int x,y;
		scanf("%d%d",&x,&y);
		a[x][y]=true;
		a[y][x]=true;
		add(x,y);
		add(y,x);
	}
	if (m==n-1) sc(1);
	else{
		for (int i=1;i<=n;++i){
			memset(flag,0,sizeof(flag));
			getloop(i,i,-1);
		}
		memset(flag,0,sizeof(flag));
		sc1(1);
	}
	for (int i=1;i<w;++i) printf("%d ",way[i]);
	printf("%d\n",way[w]);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
