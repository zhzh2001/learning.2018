#include <bits/stdc++.h>
using namespace std;
#define N 5555
#define Mod 1000000007
#define LL long long
char d[N];
char way[N][N];
int n,m,w;
int c[N][N];
void start(int t,int x,int y){
	if (x==n && y==m){
		w++;
		for (int i=1;i<=n+m-2;++i){
			way[w][i]=d[i];
		}
	}
	if (x>n || y>m) return;
	d[t]='D';
	start(t+1,x+1,y);
	d[t]='R';
	start(t+1,x,y+1);
}
int ans;
void sc(int x,int y){
	if (x==n && y>m){
		bool flag=true;
		for (int i=1;i<=w;++i){
			for (int j=1;j<=w;++j){
				if (i==j) continue;
				bool ok=true;
				if (way[i]>way[j]){
					int xx1=1,xx2=1,yy1=1,yy2=1;
					for (int k=1;k<=n+m-2;++k){
						if (way[i][k]=='R') yy1++;
						else xx1++;
						if (way[j][k]=='R') yy2++;
						else xx2++;
						if (c[xx1][yy1]>c[xx2][yy2]){
							ok=false;
							break;
						}
						else if (c[xx1][yy1]<c[xx2][yy2]){
							break;
						}
					}
				}
				if (!ok){
					flag=false;
					break;
				}
			}
		}
		if (flag) ans++;
		return;
	}
	if (y>m){
		sc(x+1,1);
		return;
	}
	c[x][y]=1;
	sc(x,y+1);
	c[x][y]=0;
	sc(x,y+1);
}
int inv(int x){
	if (x==0) return 0;
	if (x==1) return 1;
	return Mod-(Mod/x)*inv(Mod%x)%Mod;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	if (n==2){
		LL sum=4;
		for (int i=1;i<m;++i){
			sum=sum*3%Mod;
		}
		cout<<sum<<endl;
		fclose(stdin);
		fclose(stdout);
		return 0;
	}else if (n==3){
		if (m==1) printf("8\n");
		else if (m==2) printf("36\n");
		else{
			LL sum=112;
			for (int i=1;i<m-2;++i){
				sum=sum*3%Mod;
			}				
			cout<<sum<<endl;
			fclose(stdin);
			fclose(stdout);
			return 0;			
		}
		
	}else{
		start(1,1,1);
		sc(1,1);
		cout<<ans<<endl;	
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
