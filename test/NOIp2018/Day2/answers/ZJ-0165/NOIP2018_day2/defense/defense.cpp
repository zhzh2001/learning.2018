#include<iostream>
#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#define MAXN 100001
#define INF 0x3fffffff
using namespace std;
int nN, nM, nW[MAXN], nFirst[MAXN], nNext[MAXN<<1], nTo[MAXN<<1], nDp[MAXN][2];
string sP;
void vAdd(int nA, int nB, int i);
int nCalc();
bool vDfs(int nRoot, int nFather);
int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	int i, nA, nB, x, y;
	cin>>nN>>nM>>sP;
	for(i=1;i<=nN;i++)
		scanf("%d", &nW[i]);
	memset(nFirst, -1, sizeof(nFirst));
	for(i=1;i<nN;i++)
	{
		scanf("%d %d", &nA, &nB);
		vAdd(nA, nB, (i<<1)-1);
		vAdd(nB, nA, i<<1);
	}
	while(nM--)
	{
		scanf("%d %d %d %d", &nA, &x, &nB, &y);
		memset(nDp, 0, sizeof(nDp));
		nDp[nA][(x+1)%2]=-1;
		nDp[nB][(y+1)%2]=-1;
		printf("%d\n", nCalc());
	}
	return 0;
}
void vAdd(int nA, int nB, int i)
{
	nNext[i]=nFirst[nA];
	nFirst[nA]=i;
	nTo[i]=nB;
}
int nCalc()
{
	int i, nAns=INF;
	if(!vDfs(1, 0))
		return -1;
	if(nDp[1][0]!=-1)
		nAns=min(nAns, nDp[1][0]);
	if(nDp[1][1]!=-1)
		nAns=min(nAns, nDp[1][1]);
	if(nAns==INF)
		nAns=-1;
	return nAns;
}
bool vDfs(int nRoot, int nFather)
{
	int i, nA;
	if(nDp[nRoot][1]!=-1)
		nDp[nRoot][1]=nW[nRoot];
	for(i=nFirst[nRoot];i!=-1;i=nNext[i])
	{
		if(nTo[i]!=nFather)
		{
			if(!vDfs(nTo[i], nRoot))
				return false;
			if(nDp[nTo[i]][1]==-1)
				nDp[nRoot][0]=-1;
			if(nDp[nRoot][0]!=-1)
				nDp[nRoot][0]+=nDp[nTo[i]][1];
			nA=INF;
			if(nDp[nRoot][1]==-1)
				continue;
			if(nDp[nTo[i]][1]!=-1)
				nA=nDp[nTo[i]][1];
			if(nDp[nTo[i]][0]!=-1)
				nA=min(nA, nDp[nTo[i]][0]);
			nDp[nRoot][1]+=nA;
		}
	}
	if(nDp[nRoot][0]==-1&&nDp[nRoot][1]==-1)
		return false;
	return true;
}
