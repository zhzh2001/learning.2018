#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
#include<algorithm>
#define MAXN 5001
using namespace std;
int nTo[MAXN<<1], nAns[MAXN], nEdge[MAXN][MAXN], nN, nLen, nFrom[MAXN], nT[MAXN], nJump[MAXN][1], nDth[MAXN], nE;
bool bVis[MAXN];
void vAdd(int nA, int nB, int i);
void vDfs(int nID);
bool bMyCmp(const int &nA, const int &nB);
void vBuild(int nRoot, int nFather);
int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	int nM, i, nA, nB;
	scanf("%d %d", &nN, &nM);
	for(i=1;i<=nM;i++)
	{
		scanf("%d %d", &nA, &nB);
		vAdd(nA, nB, (i<<1)-1);
		vAdd(nB, nA, i<<1);
	}
	vBuild(1, 0);
	for(i=1;i<=nN;i++)
		sort(nEdge[i]+1, nEdge[i]+1+nEdge[i][0], bMyCmp);
	nLen=0;
	vDfs(1);
	for(i=1;i<nN;i++)
		printf("%d ", nAns[i]);
	printf("%d\n", nAns[nN]);
	return 0;
}
bool bMyCmp(const int &nA, const int &nB)
{
	return nTo[nA]<nTo[nB];
}
void vAdd(int nA, int nB, int i)
{
	nEdge[nA][0]++;
	nEdge[nA][nEdge[nA][0]]=i;
	nTo[i]=nB;
}
void vDfs(int nID)
{
	int nAi;
	nLen++;
	nAns[nLen]=nID;
	bVis[nID]=true;
	for(nAi=1;nAi<=nEdge[nID][0];nAi++)
	{
		if(nTo[nEdge[nID][nAi]]==-1)
			continue;
		if(!bVis[nTo[nEdge[nID][nAi]]])
			vDfs(nTo[nEdge[nID][nAi]]);
	}
}
void vBuild(int nRoot, int nFather)
{
	int i;
	nJump[nRoot][0]=nFather;
	nDth[nRoot]=nDth[nFather]+1;
	for(i=1;i<=nEdge[nRoot][0];i++)
	{
		if(nTo[nEdge[nRoot][i]]==nFather)
			continue;
		if(nTo[nEdge[nRoot][i]]==-1)
			continue;
		if(nJump[nTo[nEdge[nRoot][i]]][0]!=0)
		{
			nE++;
			nFrom[nE]=nRoot;
			nT[nE]=nTo[nEdge[nRoot][i]];
			nTo[nEdge[nRoot][i]]=-1;
		}
		vBuild(nTo[nEdge[nRoot][i]], nRoot);
	}
}
