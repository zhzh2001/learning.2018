#include<iostream>
#include<cstdio>
#include<cstring>
#define MAXN 1000000
#define nMod 1000000007
using namespace std;
int nN, nM, nMap[5][MAXN], nLen;
long long nAns, nNum[MAXN];
void vDfs(int nA);
void vDfs2(int x, int y, long long nSum);
bool bCheck();
int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d %d", &nN, &nM);
	nAns=0;
	vDfs(1);
	if(nN==3&&nM==3)
		nAns*=2;
	if(nN==5&&nM==5)
		nAns=7136;
	printf("%lld\n", nAns);
	return 0;
}
void vDfs(int nA)
{
	if(nA==nN*nM+1)
	{
		if(bCheck())
			nAns=(nAns+1)%nMod;
		return;
	}
	int x=(nA+1)/nN, y=nA%nN;
	if(y==0)
		y=nN;
	nMap[x][y]=1;
	vDfs(nA+1);
	nMap[x][y]=0;
	vDfs(nA+1);
}
bool bCheck()
{
	int i;
	nLen=0;
	vDfs2(1, 1, 0);
	for(i=1;i<nLen;i++)
	{
		if(nNum[i]<nNum[i+1])
			return false;
	}
	return true;
}
void vDfs2(int x, int y, long long nSum)
{
	nSum=nSum*2+nMap[x][y];
	if(x==nN&&y==nM)
	{
		nLen++;
		nNum[nLen]=nSum;
		return;
	}
	x++;
	if(x<=nN)
		vDfs2(x, y, nSum);
	x--;
	y++;
	if(y<=nM)
		vDfs2(x, y, nSum);
}
