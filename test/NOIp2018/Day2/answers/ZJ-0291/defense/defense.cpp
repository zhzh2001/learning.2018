#include<bits/stdc++.h>
#define int long long
using namespace std;
int n,m,u,v,p[100100],tag[100100];
int f[100100][2];
int head[100100],cnt;
char s[10];
struct node{
	int nxt,to;
}edge[200100];
void addedge(int x,int y){
	edge[++cnt].to=y;
	edge[cnt].nxt=head[x];
	head[x]=cnt;
}
void dfs(int x,int fa){
	for(int i=head[x];i;i=edge[i].nxt){
		int upup=edge[i].to;
		if(upup==fa) continue;
		dfs(upup,x);
		f[x][0]+=f[upup][1];
		if(f[upup][0]==-1 && f[upup][1]==-1) f[x][1]+=0;
		else if(f[upup][0]==-1) f[x][1]+=f[upup][1];
		else if(f[upup][1]==-1) f[x][1]+=f[upup][0];
		else f[x][1]+=min(f[upup][0],f[upup][1]);
	}
	f[x][1]+=p[x];
	if(tag[x]==0) f[x][1]=-1;
	if(tag[x]==1) f[x][0]=-1;
}
signed main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%lld%lld%s",&n,&m,&s);
	for(int i=1;i<=n;i++) scanf("%lld",&p[i]);
	for(int i=1;i<n;i++){
		scanf("%lld%lld",&u,&v);
		addedge(u,v);
		addedge(v,u);
	}
	while(m--){
		memset(tag,-1,sizeof(tag));
		int x1,x2,y1,y2;
		scanf("%lld%lld%lld%lld",&x1,&x2,&y1,&y2);
		if(x2==0){
			for(int i=head[x1];i;i=edge[i].nxt)
				tag[edge[i].to]=1;
			tag[x1]=0;			
		}
		if(y2==0){
			if(tag[y1]==1){
				puts("-1");
				continue;
			}
			tag[y1]=0;
			for(int i=head[y1];i;i=edge[i].nxt)
				tag[edge[i].to]=1;
		}
		if(y2==1) tag[y1]=1;
		if(x2==1) tag[x1]=1;
		memset(f,0,sizeof(f));
		dfs(1,0);
		if(f[1][1]==-1 && f[1][0]==-1)puts("-1");
		else if(f[1][1]==-1) printf("%lld\n",f[1][0]);
		else if(f[1][0]==-1) printf("%lld\n",f[1][1]);
		else printf("%lld\n",min(f[1][1],f[1][0]));
	}
	return 0;
	fclose(stdin);
	fclose(stdout); 
}
