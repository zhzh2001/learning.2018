#include<bits/stdc++.h>
#define int long long
#define inf 1000000007
using namespace std;
int n,m,u[10010],v[10010],Min=inf,head[10010],cnt=0,tot=0;
int ans[10010],Ans[10010],father[10010],sum;
struct node{
	int to,nxt;
}edge[20010];
int find(int x){
	if(x==father[x]) return x;
	else return father[x]=find(father[x]);
}
void Union(int x,int y){
	if(find(x)==find(y)) return;
	sum--;
	father[find(x)]=find(y);
}
void addedge(int x,int y){
	edge[++cnt].to=y;
	edge[cnt].nxt=head[x];
	head[x]=cnt;
}
bool check(){
	for(int i=1;i<=n;i++){
		if(ans[i]<Ans[i]) return true;
		if(ans[i]>Ans[i]) return false;
	}
	return false;
}
void dfs(int x,int fa){
	priority_queue<int,vector<int>,greater<int> > qu;
	ans[++tot]=x;
	for(int i=head[x];i;i=edge[i].nxt){
		int upup=edge[i].to;
		if(upup==fa) continue;
		qu.push(upup);
	}
	while(!qu.empty()){
		dfs(qu.top(),x);
		qu.pop();
	}
}
signed main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%lld%lld",&u[i],&v[i]);
		addedge(u[i],v[i]);
		addedge(v[i],u[i]);
		Min=min(Min,min(u[i],v[i]));
	}
	if(m==n-1){
		dfs(Min,0);
		for(int i=1;i<=n;i++)
			printf("%lld ",ans[i]);
		puts("");
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	memset(Ans,0x3f,sizeof(Ans));
	for(int i=1;i<=m;i++){
		cnt=0;
		sum=n;
		memset(head,0,sizeof(head));
		for(int j=1;j<=n;j++)
			father[j]=j;
		tot=0;
		for(int j=1;j<=m;j++){
			if(i==j) continue;
			addedge(u[j],v[j]);
			addedge(v[j],u[j]);	
			Union(u[j],v[j]);	
		}
		if(sum>1) continue;
		dfs(Min,0);
		if(tot<n) continue;
		if(check()){
			for(int j=1;j<=n;j++)
				Ans[j]=ans[j];
		}		
	}
	for(int i=1;i<=n;i++)
		printf("%lld ",Ans[i]);
	puts("");
	fclose(stdin);
	fclose(stdout);
	return 0;
}
