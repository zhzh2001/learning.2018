#include<bits/stdc++.h>
#define int long long
#define mod 1000000007
using namespace std;
int ans;
int n,m;
int quickpow(int x,int y){
	if(x==0) return 0;
	if(x==1) return 1;
	if(y==0) return 1;
	if(y==1) return x;
	if(y%2==0) return quickpow(x*x%mod,y/2);
	return quickpow(x*x%mod,y/2)*x%mod; 
}
signed main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if(n<=2){
		ans=4*quickpow(3,m-1)%mod;
		printf("%lld",ans);
		return 0;	
	}
	if(n==3 && m==1) printf("8\n");
	if(n==3 && m==2) printf("36\n");
	if(n==3 && m==3) printf("112\n");
	if(n==5 && m==5) printf("7136\n");
	fclose(stdin);
	fclose(stdout);
	return 0;
}
