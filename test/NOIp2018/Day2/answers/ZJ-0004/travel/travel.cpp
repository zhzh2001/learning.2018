#include <iostream>
#include <cstdio>
#include <cstring>
#include <queue>

using namespace std;

const int N=5000+10;
int n,m,tot;
int ans[N];
bool used[N];
priority_queue <int> q[N];

void dfs(int now)
{
	int a[N];
	for(int i=1;i<=n;i++)
	    a[i]=0;
	ans[++tot]=now;
	used[now]=true;
	int qwq=q[now].size();
	for(int i=qwq;i>=1;i--)
	{
		a[i]=q[now].top();
		q[now].pop();
	}
	for(int i=1;i<=qwq;i++)
	{
		int x=a[i];
		if(used[x]==false)
			dfs(x);
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	int flag=false;//用来判断是否成环 
	for(int i=1;i<=m;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		q[u].push(v);q[v].push(u);
	}
	dfs(1);
	for(int i=1;i<=n;i++)
	    printf("%d ",ans[i]);
	return 0;
}
