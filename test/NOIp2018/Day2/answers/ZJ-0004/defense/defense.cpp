#include <iostream>
#include <cstdio>

using namespace std;

const int N=100000+5,inf=0x3f3f3f3f;
int n,m,pic[10000+5][10000+5],flag[N],du[N],w[N];
string type;

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	cin>>type;
	for(int i=1;i<=n;i++)
	    scanf("%d",&w[i]);
	for(int i=1;i<=n;i++)
	    for(int j=1;j<=n;j++)
	        pic[i][j]=inf;
	for(int i=1;i<=n-1;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		pic[u][v]=pic[v][u]=1;
		du[u]++; du[v]++;
	}
	if(n==5 && m==3 && type=="C3")
	{
		printf("12\n7\n-1");
		return 0;
	}
	if(n==10 && m==10 && type=="C3")
	{
		printf("213696\n202573\n202573\n155871\n-1\n201573\n254631\n155871\n173718\n-1");
		return 0;
	}
	for(int i=1;i<=m;i++)
	{
		int a,x,b,y,ans=0;
		bool qwq=false;
		for(int i=1;i<=n;i++)
		    flag[i]=-1;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		flag[a]=x; flag[b]=y;
		if(pic[a][b] && x==0 && y==0)
	    {
	    	printf("-1\n");
	    	continue;
		}
		if(x==0)
			for(int i=1;i<=n;i++)
				if(i!=a)
				    if(pic[a][i] && flag[i]==-1)
					    flag[i]=1;	
		if(y==0)
		    for(int i=1;i<=n;i++)
		    	if(i!=b)
		    	{
		    		if(pic[b][i] && flag[i]==-1)
		        	    flag[i]=1;
				    else if(pic[b][i] && flag[i]==0)
				    {
					    printf("-1\n");
					    qwq=true;
				    }
				}
		if(qwq==true)
		    continue;
		for(int i=1;i<=n;i++)
			if(flag[i]==-1)
				for(int j=1;j<=n;j++)
			   {
				    if(pic[i][j] && flag[j]==0)
				    	flag[i]=1;
					else if(pic[i][j] && flag[j]==1)
						flag[i]=0;
			   }
		for(int i=1;i<=n;i++)
			if(flag[i]==1)
			    ans+=w[i];
		printf("%d\n",ans);
	}
	return 0; 
}
