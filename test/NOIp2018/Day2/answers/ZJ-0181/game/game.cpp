#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define P(x) putchar(x + 48)
#define pb push_back

const ll maxn = 1e6 + 5;
const ll mod = 1e9 + 7;

void read(ll &a) {
	a = 0;
	ll f = 1LL;
	char ch = getchar();
	while(ch > '9' || ch < '0') {
		if(ch == '-')f = (ll)(-1);
		ch = getchar();
	}
	while(ch <= '9' && ch >= '0') {
		a = (a << 3) + (a << 1) + ch - '0';
		ch = getchar();
	}
	a *= f;
}

void write(ll a) {
	if(a < 0)P(-3),write(-a);
	if(a > 9)write(a / 10);
	P(a % 10);
}

ll f[10][maxn],n,m;

void upd(ll &a,ll &b) {
	a = (a + b) % mod;
}

int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n);
	read(m);
	f[1][1] = f[1][1] = 1;
	for(int i = 2;i <= m;i++)f[1][i] = (f[1][i - 1] * 2) % mod;
	for(int i = 2;i <= n;i++)f[i][1] = (f[i - 1][1] * 2) % mod;
	f[2][2] = 12;
	write(f[n][m]);
}
