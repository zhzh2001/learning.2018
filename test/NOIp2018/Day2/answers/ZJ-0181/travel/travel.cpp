#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define P(x) putchar(x + 48)
#define pb push_back

const ll maxn = 5e3 + 5;

void read(ll &a) {
	a = 0;
	ll f = 1LL;
	char ch = getchar();
	while(ch > '9' || ch < '0') {
		if(ch == '-')f = (ll)(-1);
		ch = getchar();
	}
	while(ch <= '9' && ch >= '0') {
		a = (a << 3) + (a << 1) + ch - '0';
		ch = getchar();
	}
	a *= f;
}

void write(ll a) {
	if(a < 0)P(-3),write(-a);
	if(a > 9)write(a / 10);
	P(a % 10);
}

ll n,m,u,v,ans[maxn],tot,f = maxn,ff,q;

vector<ll> edge[maxn];

stack<ll>Z;

bool vis[maxn],h[maxn],flag;

void dfs1(ll x,ll fa) {
	ans[++tot] = x;
	sort(edge[x].begin(),edge[x].end());
	for(int i = 0; i < edge[x].size(); i++) {
		if(edge[x][i] == fa)continue;
		dfs1(edge[x][i],x);
	}
}

void find(ll x,ll fa) {
	Z.push(x);
	vis[x] = 1;
	for(int i = 0; i < edge[x].size(); i++) {
		if(flag)return;
		if(edge[x][i] == fa)continue;
		if(vis[edge[x][i]]) {
			while(1) {
				h[Z.top()] = 1;
				if(Z.top() == edge[x][i]) {
					flag = 1;
					return;
				}
				Z.pop();
			}
		}
		find(edge[x][i],x);
	}
	Z.pop();
}

void dfs2(ll x,ll fa) {
	ans[++tot] = x;
	sort(edge[x].begin(),edge[x].end());
	if(h[x] && ff == 0) {
		ff = x;
		bool flag = 0;
		for(int i = 0;i < edge[x].size();i++)
			if(h[edge[x][i]]) {
				if(flag) {
					f = edge[x][i];
					break;
				}
				flag = 1;
			}
	}
	for(int i = 0; i < edge[x].size(); i++) {
		if(edge[x][i] == fa || edge[x][i] == q)continue;
		if(h[edge[x][i]] && h[x] && (i == edge[x].size() - 1 ||(i == edge[x].size() - 2 && edge[x][edge[x].size() - 1] == fa)) && edge[x][i] > f) {
			q = x;
			f = maxn;
			return;
		}
		dfs2(edge[x][i],x);
	}
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);
	read(m);
	for(int i = 1; i <= m; i++) {
		read(u);
		read(v);
		edge[u].pb(v);
		edge[v].pb(u);
	}
	if(m == n - 1)dfs1(1,0);
	else {
		find(1,0);
		dfs2(1,0);
	}
	for(int i = 1; i <= n; i++) {
		write(ans[i]);
		putchar(' ');
	}
	return 0;
}

/*

һ����...
lbwnb,�������bb...
m = n - 1 || m = n...

6 5
1 3
2 3
2 5
3 4
4 6

*/

