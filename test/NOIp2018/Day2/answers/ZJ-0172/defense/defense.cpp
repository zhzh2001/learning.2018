#include<bits/stdc++.h>
using namespace std;

const int MAXN = 110000;
int n,m;
int link[MAXN],o;
struct node{
	int to,next;
}a[MAXN * 2];
int p[MAXN];
string s;
int f[MAXN];

inline void in(int x,int y){
	a[++o].next = link[x];
	a[o].to = y;
	link[x] = o;
}

void init(){
	cin>>n>>m>>s;
	int x,y;
	for(int i = 1;i <= n;i++) scanf("%d",&p[i]);
	for(int i = 1;i < n;i++){
		scanf("%d%d",&x,&y);
		in(x,y);
		in(y,x);
	}
}

int d1,d2,v1,v2;
long long an;

void work_1(){
	for(int e = 1;e <= m;e++){
		an = 0;
		scanf("%d%d%d%d",&d1,&v1,&d2,&v2);
		if(abs(d1 - d2) == 1 && !v1 && !v2){
			printf("-1\n");
			continue;
		}
		if(d1 > d2) swap(d1,d2),swap(v1,v2);
		if(v1) f[d1] = 1,an += p[d1];else f[d1] = 0;
		if(v2) f[d2] = 1,an += p[d2];else f[d2] = 0;
		for(int i = d1 - 1;i;i--){
			if(f[i + 1]) f[i] = 0;else
			f[i] = 1,an += p[i];
		}
		for(int i = d1 + 1;i <= n;i++){
			if(i == d2) continue;
			if(f[i - 1] || (f[i + 1] && i + 1 == d2)) f[i] = 0;else
			f[i] = 1,an += p[i];
		}
		printf("%lld\n",an);
	}
}

int sum[MAXN];

void dfs(int x,int fa){
	for(int i = link[x];i;i = a[i].next){
		if(a[i].to == fa) continue;
		sum[x] += p[a[i].to];
		dfs(a[i].to,x);
	}
}

void find(int x,int fa){
	int gg[110],tail = 0,flag = 1;
	for(int i = link[x];i;i = a[i].next){
		int y = a[i].to;
		if(f[y] == 2 || f[fa] == 2) f[x] = 1;
		gg[++tail] = y;
	}
	for(int i = 1;i <= tail;i++) find(gg[i],x);
}

void work_2(){
	dfs(1,0);
	for(int e = 1;e <= m;e++){
		an = 0;
		scanf("%d%d%d%d",&d1,&v1,&d2,&v2);
		//if(d1 > d2) swap(d1,d2),swap(v1,v2);
		bool flag = 0;
		for(int i = link[d1];i;i = a[i].next){
			if(a[i].to == d2) flag = 1;
		}
		if(flag && !v1 && !v2){
			printf("-1\n");
			continue;
		}
		memset(f,0,sizeof f);
		if(v1) f[d1] = 1;else f[d2] = 2;
		if(v2) f[d2] = 1;else f[d2] = 2;
		find(1,0);
	}
}

void work(){
	if(s[0] == 'A') work_1();else
	if(s[1] == '1') work_2();
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	init();
	work();
	return 0;
}
