#include<bits/stdc++.h>
#define INF 210000000
#define ll long long
#define N 310000
using namespace std;
char s[100];
bool vis[20][20],g[N];
vector<int> f[N];
int n,m,ans;
int b[N],a[N];
int dp[N][2];
void dfs(int x,int y)
{
	if (y>=ans) return;
	if (x>n)
		{
			memset(vis,0,sizeof(vis));
			for (int i=1;i<=n;i++)
				if (b[i])
					{
						int len=f[i].size();
						for (int j=0;j<len;j++) 
							{
								vis[i][f[i][j]]=true;
								vis[f[i][j]][i]=true;
							}
					}
			bool flag=true;
			for (int i=1;i<=n;i++)
			
				{
					int len=f[i].size();
					for (int j=0;j<len;j++) 
						if (!vis[i][f[i][j]])
							{
								flag=false;
								break;
							}
					if (!flag) break;
				}
			if (flag) 
				{
					ans=y;
				}
			return;
		}
	if (g[x]) dfs(x+1,(b[x]?y+a[x]:y)); else
	{
		b[x]=1;
		dfs(x+1,y+a[x]);
		b[x]=0;
		dfs(x+1,y);
	}
}
void sc(int x,int fa)
{
	int len=f[x].size();
	if (len==1&&fa==f[x][0])
		{
			if (g[x])
				{
					if (b[x]==1) dp[x][1]=a[x]; else dp[x][0]=0;
					dp[x][1-b[x]]=INF;
				}
			else
				{
					dp[x][1]=a[x];
					dp[x][0]=0;
				}
			return;
		}
	for (int i=0;i<len;i++)
		{
			int p=f[x][i];
			if (p==fa) continue;
			sc(p,x);
			if (g[x])
				{
					if (b[x]==1) dp[x][1]=dp[x][1]+min(dp[p][0],dp[p][1]); else
						{
							if (dp[p][1]<INF) dp[x][0]=dp[x][0]+dp[p][1]; else dp[x][0]=INF;
						}
					dp[x][1-b[x]]=INF;
				}
			else
				{
					dp[x][1]=dp[x][1]+min(dp[p][0],dp[p][1]);
					if (dp[p][1]<INF) dp[x][0]=dp[x][0]+dp[p][1]; else dp[x][0]=INF;
				}
		}
	dp[x][1]=dp[x][1]+a[x];
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,&s);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<n;i++)
		{
			int x,y;
			scanf("%d%d",&x,&y);
			f[x].push_back(y);
			f[y].push_back(x);
		}
	if (n<=10&&m<=10)
		{
			for (int i=1;i<=m;i++)
				{
					int x,xflag,y,yflag;
					ans=214748364;
					scanf("%d%d%d%d",&x,&xflag,&y,&yflag);
					g[x]=true;g[y]=true;
					b[x]=xflag;b[y]=yflag;
					dfs(1,0);
					g[x]=false;g[y]=false;
					if (ans==214748364) printf("-1\n"); else printf("%d\n",ans);
				}
			return 0;
		}
	if (n<=2000&&m<=2000)
		{
			for (int i=1;i<=m;i++)
				{
					int x,xflag,y,yflag;
					scanf("%d%d%d%d",&x,&xflag,&y,&yflag);
					memset(dp,0,sizeof(dp));
					g[x]=true;g[y]=true;
					b[x]=xflag;b[y]=yflag;
					sc(1,0);
					g[x]=false;g[y]=false;
					int t=min(dp[1][0],dp[1][1]);
					if (t>=INF) printf("-1\n"); else printf("%d\n",t);
				}
			return 0;
		}
	return 0;
}
