#include<bits/stdc++.h>
#define N 10000
using namespace std;
vector<int> f[N];
int n,m,nowx,nowy,cnt;
struct node{
	int x,y;
} e[N];
int b[N],ans[N];
bool vis[N];
void dfs(int x,int fa)
{
	int len=f[x].size();
	for (int i=0;i<len;i++)
		{
			int p=f[x][i];
			if (p==fa) continue;
			printf(" %d",p);
			dfs(p,x);
		}
}
void sc(int x)
{
	int len=f[x].size();
	for (int i=0;i<len;i++)
		{
			int p=f[x][i];
			if (vis[p]) continue;
			if ((x==nowx&&p==nowy)||(x==nowy&&p==nowx)) continue;
			cnt++;
			b[cnt]=p;
			vis[p]=true;
			sc(p);
		}
}
int main()
{
	
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
		{
			int x,y;
			scanf("%d%d",&x,&y);
			f[x].push_back(y);
			f[y].push_back(x);
			e[i].x=x;e[i].y=y;
		}
	if (m==n-1)
		{
			for (int i=1;i<=n;i++)
				if (f[i].size()>=2)
					sort(f[i].begin(),f[i].end());
			printf("1");
			dfs(1,0);
			printf("\n");
			return 0;
		}
	if (m==n)
		{
			for (int i=1;i<=n;i++)
				if (f[i].size()>=2)
					sort(f[i].begin(),f[i].end());
			ans[1]=66666666;
			for (int i=1;i<=m;i++)
				{
					memset(vis,0,sizeof(vis));
					vis[1]=true;nowx=e[i].x;nowy=e[i].y;
					cnt=1;b[1]=1;
					sc(1);
					if (cnt!=n) continue;
					bool flag=false;
					for (int j=1;j<=n;j++)
						if (b[j]<ans[j])
							{
								flag=true;
								break;
							}
						else
						if (b[j]>ans[j])
							{
								flag=false;
								break;
							}
					if (flag==true)
						{
							for (int j=1;j<=n;j++) ans[j]=b[j];
						}
				}
			for (int i=1;i<n;i++) printf("%d ",ans[i]);
			printf("%d\n",ans[n]);
			return 0;	
		}
	return 0;
}
