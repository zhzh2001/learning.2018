#include<bits/stdc++.h>
#define ll long long
using namespace std;
const ll mo=1000000007;
int n,m,mn;
ll ans;
bool flag;
int b[10][10];
ll fst(ll x,ll y)
{
	ll res=1;
	while (y>0)
		{
			if (y%2==1) res=(res*x)%mo;
			x=(x*x)%mo;
			y=y/2;
		}
	return res;
}
void pd(int x,int y,int z)
{
	if (flag) return;
	if (x==n&&y==m)
		{
			if (z<mn) 
				{
					flag=true;
					return;
				}
			mn=z;
			return;
		}
	if (y+1<=m) pd(x,y+1,z*2+b[x][y+1]);
	if (x+1<=n) pd(x+1,y,z*2+b[x+1][y]);
}
void bs(int x,int y)
{
	if (x>n)
		{
			mn=-1;
			flag=false;
			pd(1,1,b[1][1]);
			if (flag==false) ans++;
			return;
		}
	b[x][y]=1;
	if (y+1<=m) bs(x,y+1); else bs(x+1,1);
	b[x][y]=0;
	if (y+1<=m) bs(x,y+1); else bs(x+1,1);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n+m<=8)
		{
			ans=0;
			bs(1,1);
			printf("%d\n",ans);
			return 0;
		}
	if (n==1)
		{
			printf("%lld\n",fst(2*1ll,m*1ll)%mo);
			return 0;
		}
	if (n==2)
		{
			ans=4*1ll*fst(3*1ll,m*1ll-1ll)%mo;
			printf("%lld\n",ans);
			return 0;
		}
	if (n==3)
		{
			if (m==1) printf("8\n"); else
			if (m==2) printf("36\n"); else
				{
					ans=112*1ll*fst(3*1ll,m*1ll-3*1ll)%mo;
					printf("%lld\n",ans);
				}
			return 0;
		}
	return 0;
}
