var i,j,k,l,m,n,s,t,x,y:longint;
a,nex,p,dia:array[0..10009]of longint;
procedure swap(var a,b:longint);
var t:longint;
begin
 t:=a;
 a:=b;
 b:=t;
end;
procedure dfs(k:longint);
var i,j,min:longint;
begin
 inc(t);
 p[k]:=1;
 dia[t]:=k;
 j:=k;
 while a[j]<>0 do
 begin
  if p[a[j]]=0 then dfs(a[j]);
  j:=nex[j];
 end;
end;
begin
 assign(input,'travel.in');
 reset(input);
 assign(output,'travel.out');
 rewrite(output);
 read(n,m);
 l:=n;
 for i:=1 to m do
 begin
  read(x,y);
  t:=y;
  if a[x]=0 then a[x]:=t else
  begin
   j:=x;
   while a[j]<>0 do
   begin
    if t<a[j] then swap(t,a[j]);
    if nex[j]=0 then s:=j;
    j:=nex[j];
   end;
   inc(l);
   a[l]:=t;
   nex[s]:=l;
  end;
  t:=x;
  if a[y]=0 then a[y]:=t else
  begin
   j:=y;
   while a[j]<>0 do
   begin
    if t<a[j] then swap(t,a[j]);
    if nex[j]=0 then s:=j;
    j:=nex[j];
   end;
   inc(l);
   a[l]:=t;
   nex[s]:=l;
  end;
 end;
 t:=0;
 dfs(1);
 for i:=1 to n-1 do write(dia[i],' ');
 writeln(dia[n]);
 close(input);
 close(output);
end.

