var i,j,k,l,m,n,s,t,x,y:longint;
a:array[0..8,0..1000009]of int64;
begin
 assign(input,'game.in');
 reset(input);
 assign(output,'game.out');
 rewrite(output);
 x:=1000000007;
 readln(n,m);
 for i:=0 to n do a[i,0]:=1;
 for j:=0 to m do a[0,j]:=1;
 for i:=1 to n do
  a[i,1]:=a[i-1,1]*2 mod x;
 for j:=1 to m do
  a[1,j]:=a[1,j-1]*2 mod x;
 for i:=2 to n do
  a[i,2]:=a[i-1,2]*3 mod x;
 for j:=2 to m do
  a[2,j]:=a[2,j-1]*3 mod x;
 a[5,5]:=7136;
 a[3,3]:=112;
 a[3,4]:=896;
 a[4,3]:=896;
 writeln(a[n,m]);
 close(input);
 close(output);
end.
