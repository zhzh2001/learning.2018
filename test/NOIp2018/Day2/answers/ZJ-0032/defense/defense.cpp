#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
typedef long long LL;
const int maxn=100005,maxe=200005;
int n,m,x,y,hed,til,tot,t;
int vis[maxn],que[maxn],in[maxn],fa[maxn];
int lnk[maxn],nxt[maxe],to[maxe];
LL f[maxn][2],a[maxn];
bool p1,p2;
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if(ch=='-') f=-f;ch=getchar();}
	while(ch>=48&&ch<=57) {ret=ret*10+ch-48;ch=getchar();}
	return ret*f;
}
void adde(int x,int y){to[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;}
void dfs(int x,int f){fa[x]=f;for(int j=lnk[x];j;j=nxt[j]) if(to[j]!=f) in[x]++,dfs(to[j],x);}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();read();
	for(int i=1;i<=n;i++) a[i]=read();
	for(int i=1;i<n;i++)
	{
		int S=read(),T=read();
		adde(S,T);adde(T,S);
	}
	dfs(1,0);
	if(n*m<=2e8)
	{
		memset(vis,-1,sizeof vis);
		for(int i=2;i<=n;i++) if(!nxt[lnk[i]]) que[++til]=i;t=til;
		while(hed^til) if(!--in[fa[que[++hed]]]) que[++til]=fa[que[hed]];
		while(m--)
		{
			for(int i=1;i<=n;i++) f[i][0]=f[i][1]=1e12;
			x=read();p1=read();y=read();p2=read();
			if((fa[x]==y||fa[y]==x)&&!p1&&!p2){printf("-1\n");continue;}
			vis[x]=p1;vis[y]=p2;
			for(int i=1;i<=t;i++)
			{
				int u=que[i];
				if(vis[u]!=0) f[u][1]=a[u];
				if(vis[u]!=1) f[u][0]=0;
			}
			for(int i=t+1;i<=n;i++)
			{
				int u=que[i],Min=1e9;
				if(vis[u]!=0) f[u][1]=a[u];
				if(vis[u]!=1) f[u][0]=0;
				for(int j=lnk[u];j;j=nxt[j])
				{
					if(to[j]==fa[u]) continue;
					if(vis[u]!=1) f[u][0]+=f[to[j]][1];
					if(vis[u]!=0) f[u][1]+=min(f[to[j]][0],f[to[j]][1]);
				}
			}
			printf("%d\n",min(f[que[n]][0],f[que[n]][1]));
			vis[x]=vis[y]=-1;
		}
	}
	return 0;
}

