#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long LL;
const int TT=1e9+7;
const int t[5][5]={
{0,0,0,0,0},
{0,2,4,6,16},
{0,0,12,36,108},
{0,0,0,112,336},
{0,0,0,0,912}};
int n,m;
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if(ch=='-') f=-f;ch=getchar();}
	while(ch>=48&&ch<=57) {ret=ret*10+ch-48;ch=getchar();}
	return ret*f;
}
LL ksm(LL x,int y)
{
	LL s=1;
	while(y)
	{
		if(y&1) s=(s*x)%TT;
		x=(x*x)%TT;
		y/=2;
	}
	return s;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();m=read();
	if(n>m) swap(n,m);
	if(n<=4&&m<=4){printf("%d\n",t[n][m]);return 0;}
	if(n==1){printf("%d\n",ksm(2,m));return 0;}
	if(n==2){printf("%d\n",(ksm(3,m-1)*4)%TT);return 0;}
	if(n==3){printf("%d\n",(ksm(3,m-3)*112)%TT);return 0;}
	return 0;
}

