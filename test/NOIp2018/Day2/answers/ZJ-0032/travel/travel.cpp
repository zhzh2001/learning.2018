#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=5005,maxe=10005;
int n,m,tot,top,indx,num;
int lnk[maxn],nxt[maxe],to[maxe],fa[maxe];
int p[maxn],b[maxn][maxn];/*m==n-1*/
int id[maxn],stck[maxn],low[maxn],dfn[maxn],t[maxn];
bool vis[maxe],flg[maxn];/*m==n*/
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if(ch=='-') f=-f;ch=getchar();}
	while(ch>=48&&ch<=57) {ret=ret*10+ch-48;ch=getchar();}
	return ret*f;
}
void adde(int x,int y){to[++tot]=y;fa[tot]=x;nxt[tot]=lnk[x];lnk[x]=tot;}
void dfs(int x,int f)
{
	int cnt=0;
	p[++p[0]]=x;
	for(int j=lnk[x];j;j=nxt[j]) if(to[j]!=f&&!vis[j]) b[x][++cnt]=to[j];
	sort(b[x]+1,b[x]+1+cnt);
	for(int i=1;i<=cnt;i++) dfs(b[x][i],x);
}
void tarjan(int x,int f)
{
	low[x]=dfn[x]=++indx;
	stck[++top]=x;flg[x]=1;
	for(int j=lnk[x];j;j=nxt[j])
	{
		int v=to[j];
		if(v==f) continue;
		if(!dfn[v])
		{
			tarjan(v,x);
			low[x]=min(low[x],low[v]);
		}
		else if(flg[v]) low[x]=min(low[x],dfn[v]);
	}
	if(dfn[x]==low[x])
	{
		num++;
		while(stck[top+1]!=x)
		{
			id[stck[top]]=num;
			flg[stck[top]]=0;
			top--;
		}
	}
}
void work()
{
	for(int i=1;i<=n;i++)
	{
		if(t[i]<p[i]) return;
		if(t[i]==p[i]) continue;
		for(int j=1;j<=n;j++) t[j]=p[j];
		return;
	}
}
void print(int *a){for(int i=1;i<=n;i++) printf("%d%c",a[i],i==n?'\n':' ');}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++)
	{
		int S=read(),T=read();
		adde(S,T);adde(T,S);
	}
	if(m==n-1) dfs(1,0),print(p);
	else
	{
		tarjan(1,0);t[1]=1000000;
		for(int i=1;i<=tot;i+=2)
		{
			if(id[fa[i]]!=id[to[i]]) continue;
			vis[i]=vis[i+1]=1;
			memset(p,63,sizeof p);p[0]=0;
			dfs(1,0);
			work();
			vis[i]=vis[i+1]=0;
		}
		print(t);
	}
	return 0;
}

