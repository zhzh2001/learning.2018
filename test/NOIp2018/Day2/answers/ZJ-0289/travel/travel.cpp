#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<vector>
#include<set>
#include<queue>
using namespace std;
int n,m,tot=0,head[100010],ans[100010],tp,nowx,nowy,an[100010];
bool visit[100010];
vector <int> to[100010];
struct edge
{
	int x,y;
} road[100010];
int read()
{
	char ch='@';
	int x=0,y=1;
	while ((ch!='-') && (ch<'0' || ch>'9')) ch=getchar();
	if (ch=='-') y=-1,ch=getchar();
	while (ch>='0' && ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*y;
}
void dfs(int now)
{
	printf("%d ",now);
	for (int i=0;i<to[now].size();++i)
		if (!visit[to[now][i]])
		{
			visit[to[now][i]]=true;
			dfs(to[now][i]);
		}
}
void dfss(int now)
{
	++tp,ans[tp]=now;
	for (register int i=0;i<to[now].size();++i)
		if (!visit[to[now][i]])
		{
			if ((now==nowx && to[now][i]==nowy) || (now==nowy && to[now][i]==nowx)) continue;
			visit[to[now][i]]=true;
			dfss(to[now][i]);
		}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(visit,false,sizeof(visit));
	n=read(),m=read();
	for (int i=1,x,y;i<=m;++i) x=read(),y=read(),to[x].push_back(y),to[y].push_back(x),road[i].x=x,road[i].y=y;
	for (int i=1;i<=n;++i) sort(to[i].begin(),to[i].end());
	if (m==n-1)
	{
		visit[1]=true;
		dfs(1);
		return 0;
	}
	else
	{
		memset(an,63,sizeof(an));
		for (int i=1;i<=m;++i)
		{
			bool flag=false;
			nowx=road[i].x,nowy=road[i].y,tp=0;
			memset(visit,false,sizeof(visit));
			visit[1]=true;
			dfss(1);
			if (tp<n) continue;
			for (int i=1;i<=n;++i)
				if (an[i]>ans[i])
				{
					flag=true;
					break;
				}
				else if (an[i]<ans[i]) break;
			if (flag)
				for (int i=1;i<=n;++i) an[i]=ans[i];
		}
		for (int i=1;i<=n;++i) printf("%d ",an[i]);
		return 0;
	}
	return 0;
}
