#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<vector>
#include<set>
using namespace std;
int n,m,val[1000010],dlc,tot=0,head[1000010],p[1000010];
string s1;
char opt;
bool visit[1000010];
struct link
{
	int to,next;
} edge[1000010];
int read()
{
	char ch='@';
	int x=0,y=1;
	while ((ch!='-') && (ch<'0' || ch>'9'))
	{
		ch=getchar();
		if (ch>='A' && ch<='Z') opt=ch;
	}
	if (ch=='-') y=-1,ch=getchar();
	while (ch>='0' && ch<='9')
	{
		x=x*10+ch-'0',ch=getchar();
		if (ch>='A' && ch<='Z') opt=ch;
	}
	return x*y;
}
void add(int x,int y)
{
	++tot,edge[tot].to=y,edge[tot].next=head[x],head[x]=tot;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),dlc=read();
	for (int i=1;i<=n;++i) val[i]=read();
	for (int i=1,x,y;i<n;++i) x=read(),y=read(),add(x,y),add(y,x);
	if (opt=='A')
	{
		for (int _=1,a,b,x,y;_<=m;++_)
		{
			memset(visit,false,sizeof(visit));
			int an=0;
			a=read(),x=read(),b=read(),y=read();
			if (x==1) visit[a]=visit[a-1]=visit[a+1]=true,an+=val[a];
			else visit[a]=true;
			if (y==1) visit[b]=visit[b-1]=visit[b+1]=true,an+=val[b];
			else visit[b]=true;
			bool flag=false;
			if (x==0 && y==0 && abs(a-b)<=1) printf("-1\n");
			else
			{
				for (int i=1;i<=n;++i)
					if (!visit[i])
					{
						if (i==n)
						{
							if (visit[i-1]) an+=val[i];
							else an+=min(val[i],val[i-1]);
						}
						if (visit[i-1] && visit[i+1]) an+=val[i];
						else if (visit[i-1]) an+=min(val[i],val[i+1]),visit[i+1]=true;
						else if (visit[i+1]) an+=min(val[i],val[i-1]),visit[i-1]=true;
						else
						{
							an+=min(val[i-1],min(val[i],val[i+1]));
							visit[i+1]=true;
						}
					}
				printf("%d\n",an);
			}
		}
	}
}
