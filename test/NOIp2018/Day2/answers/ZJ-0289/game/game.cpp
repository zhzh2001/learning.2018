#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<vector>
#include<set>
using namespace std;
const int mod=1e9+7;
int dp[10][1050000],n,m;
int cheng(int x,int y)
{
	return (x==0 || y==0)?0:(int)(((long long)x*(long long)y)%(long long)mod);
}
int read()
{
	char ch='@';
	int x=0,y=1;
	while ((ch!='-') && (ch<'0' || ch>'9')) ch=getchar();
	if (ch=='-') y=-1,ch=getchar();
	while (ch>='0' && ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*y;
}
int qpow(int p,int k)
{
	--k;
	int ans=1;
	while (k>0)
	{
		if (k%2==1) ans=cheng(ans,p);
		k/=2;
		p=cheng(p,p);
	}
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if (n==2)
	{
		printf("%d",cheng(qpow(2,m),qpow(2,m)-1));
	}
	else if (n==3)
	{
		if (m==3) cout<<"112";
		else
		{
			if (m==1) cout<<"0";
			else if (m==2) cout<<"60";
			else cout<<"0";
		}
	}
	else if (n==1)
	{
		printf("0");
	}
	else
	{
		if (n==5 && m==5) cout<<"7136";
		else printf("0");
	}
	return 0;
}
