#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define rep(i,a,n) for (int i=a;i<=n;i++)
#define per(i,a,n) for (int i=a;i>=n;i--)
#define Clear(a,x) memset(a,x,sizeof(a))
#define ll long long
#define INF 2000000000000
using namespace std;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') f=ch=='-'?-1:f,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*f;
}
const int maxn=100005;
int n,m;
char opt[10];
int p[maxn];
int head[maxn],Next[maxn<<1],vet[maxn<<1];
ll f[maxn][2];
int a,b,x,y,len;
void add(int u,int v){
	vet[++len]=v;
	Next[len]=head[u];
	head[u]=len;
}
void dfs(int u,int fa){
	f[u][0]=0;
	f[u][1]=p[u];
	if (u==a&&x==1||u==b&&y==1) f[u][0]=INF;
	if (u==a&&x==0||u==b&&y==0) f[u][1]=INF;
	for (int e=head[u];e;e=Next[e]){
		int v=vet[e];
		if (v==fa) continue;
		dfs(v,u);
	}
	for (int e=head[u];e;e=Next[e]){
		int v=vet[e];
		if (v==fa) continue;
		if ((u!=a||(u==a&&x!=1))&&(u!=b||(u==b&&y!=1)))
			f[u][0]+=f[v][1];
		if ((u!=a||(u==a&&x!=0))&&(u!=b||(u==b&&y!=0)))
			f[u][1]+=min(f[v][0],f[v][1]);
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();
	scanf("%s",opt);
	rep(i,1,n)
		p[i]=read();
	rep(i,1,n-1){
		int u=read(),v=read();
		add(u,v),add(v,u);
	}
	dfs(1,0);
	while (m--){
		a=read(),x=read(),b=read(),y=read();
		bool flag=1;
		if (x==0&&y==0){
			for (int e=head[a];e;e=Next[e]){
				int v=vet[e];
				if (v==b){
					flag=0;
					break;
				}
			}
			if (!flag){
				puts("-1");
				continue;
			}
		}
		dfs(1,0);
		printf("%lld\n",min(f[1][0],f[1][1]));
	}
	return 0;
}
