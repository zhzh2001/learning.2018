#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<queue>
#define rep(i,a,n) for (int i=a;i<=n;i++)
#define per(i,a,n) for (int i=a;i>=n;i--)
#define Clear(a,x) memset(a,x,sizeof(a))
#define ll long long
#define INF 2000000000
using namespace std;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') f=ch=='-'?-1:f,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*f;
}
const int maxn=5005;
int map[maxn][maxn];
int ans[maxn];
int head[maxn],vet[maxn<<1],Next[maxn<<1];
int dfn[maxn],low[maxn],q[maxn],a[maxn];
bool inque[maxn],flag[maxn];
int top,Time,st,ls,rs;
int len,n,m,Long,lena;
//priority_queue<int,vector<int>,greater<int> > Q;
void add(int u,int v){
	vet[++len]=v;
	Next[len]=head[u];
	head[u]=len;
}
void dfs(int u,int fa){
	printf("%d ",u);
	rep(i,1,n)
		if (map[u][i]==1&&i!=fa) dfs(i,u); 
}
void solve1(){
	dfs(1,0);
	puts("");
}
void tarjan(int u,int fa){
	dfn[u]=low[u]=++Time;
	q[++top]=u;
	for (int e=head[u];e;e=Next[e]){
		int v=vet[e];
		if (v==fa) continue;
		if (!dfn[v]){
			tarjan(v,u);
			low[u]=min(low[u],low[v]);
		}else low[u]=min(low[u],dfn[v]);
	}
	if (low[u]==dfn[u]){
		if (q[top]==u){
			top--;
			return;
		}
		while (q[top]!=u){
			a[++Long]=q[top];
			inque[q[top]]=1;
			top--;
		}
		inque[u]=1;
		a[++Long]=u;
		top--;
	}
}
void find(int u){
	flag[u]=1;
	ans[++lena]=u;
	rep(i,1,n)
		if (map[u][i]&&!flag[i]) find(i);
}
void search(int u){
	ans[++lena]=u;
	flag[u]=1;
	rep(i,1,n){
		if (map[u][i]==1&&!inque[i]&&!flag[i]) find(i);
		if (map[u][i]==1&&inque[i]&&!flag[i]&&i<rs) search(i);
	}
}
void solve(int u){
	ans[++lena]=u;
	flag[u]=1;
	ls=0,rs=0;
	rep(i,1,n)
		if (map[u][i]==1&&inque[i]&&!flag[i])
			if (ls==0) ls=i;
				else rs=i;
	if (ls>rs) swap(ls,rs);
	rep(i,1,ls-1)
		if (map[u][i]==1&&!inque[i]&&!flag[i]) find(i);
	search(ls);
	rep(i,ls+1,rs-1)
		if (map[u][i]==1&&!inque[i]&&!flag[i]) find(i);
	int tmp=rs;
	rs=INF;
	search(tmp);
}
void DFS(int u,int fa){
	if (inque[u]){
		solve(u);
		return;
	}
	flag[u]=1;
	ans[++lena]=u;
	rep(i,1,n)
		if (map[u][i]==1&&!flag[i]) DFS(i,u);
}
void solve2(){
	tarjan(1,0);
	lena=0;
	DFS(1,0);
	rep(i,1,n-1)
		printf("%d ",ans[i]);
	printf("%d\n",ans[n]);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	rep(i,1,m){
		int u=read(),v=read();
		map[u][v]=map[v][u]=1;
		add(u,v),add(v,u);
	}
	if (n-1==m) solve1();
		else solve2();
	return 0;
}
