#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

vector<int>a[5005];
int b[5005];
int n,m,u,v;
bool f[5005];
bool dfs(int x,int fa)
{
	f[x]=1;
	for (int i=0;i<a[x].size();i++)
	if (a[x][i]!=fa)
	{
		if (f[a[x][i]]){
			b[++v]=a[x][i];
			b[++v]=x;
			return 1;
		}
		if (dfs(a[x][i],x)){
			if (b[1]==x) return 0;
			b[++v]=x;
			return 1;
		}
	}
	return 0;
}
void dfs1(int x,int fa)
{
	printf("%d ",x);
	for (int i=0;i<a[x].size();i++)
	if (a[x][i]!=-1&&a[x][i]!=fa) dfs1(a[x][i],x);
}
inline int read()
{
	int x=0;char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-48,ch=getchar();
	return x;
}
int main()
{
 	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=m;i++)
	{
		u=read();v=read();
		a[u].push_back(v);
		a[v].push_back(u);
	}
	if (m==n){
		v=0;
		dfs(1,1);
		if (b[2]>b[v])
		{
			u=2+(v-1)/2-1;
			for (int i=2;i<=u;i++)
			{
				int t=b[i];
				b[i]=b[v-(i-1)+1];
				b[v-(i-1)+1]=t;
			}
		}
		for (int i=3;i<=v;i++)
		if (b[i]>=b[v])
		{
			u=b[i];
			v=b[i-1];
			break;
		}
		for (int i=0;i<a[u].size();i++)
		if (a[u][i]==v) a[u][i]=-1;
		for (int i=0;i<a[v].size();i++)
		if (a[v][i]==u) a[v][i]=-1;
	}
	for (int i=1;i<=n;i++)
	{
		u=a[i].size();
		for (int j=0;j<u;j++)
		b[j]=a[i][j];
		sort(b,b+u);
		a[i].clear();
		for (int j=0;j<u;j++)
		a[i].push_back(b[j]);
	}
	dfs1(1,1);
}

