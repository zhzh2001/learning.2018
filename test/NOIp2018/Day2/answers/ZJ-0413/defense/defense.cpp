#include <cstdio>
#include <algorithm>
#include <vector>
#include <cstring>
#include <string>
#include <cmath>
using namespace std;
int n,m,u,v,pos1,pos2,p1,p2,p[100005],f[2010][2];
long long ffl[100005][2],ffr[100005][2];
bool g[2005][2005];
vector<int>a[100005];
inline int read()
{
	int x=0;char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-48,ch=getchar();
	return x;
}
inline void write(long long x)
{
	if (x<0) x=-x,putchar('-');
	if (x==0) putchar(48);
	int len=0,ch[15];
	while (x) ch[++len]=x%10,x/=10;
	while (len) putchar(ch[len]+48),len--;
	putchar('\n');
}
void dfs(int x,int fa)
{
	f[x][1]=p[x];
	if (x==pos1) f[x][1-p1]=200000000;
	if (x==pos2) f[x][1-p2]=200000000;
	if (a[x].size()==1&&x!=1)	return;
	for (int i=0;i<a[x].size();i++)
	if (a[x][i]!=fa)
	{
		dfs(a[x][i],x);
		f[x][0]+=f[a[x][i]][1];
		f[x][1]+=min(f[a[x][i]][0],f[a[x][i]][1]);
	}
	if (x==pos1) f[x][1-p1]=200000000;
	if (x==pos2) f[x][1-p2]=200000000;
	return;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();u=read();
	for (int i=1;i<=n;i++)
	p[i]=read();
	if (n==100000)
	{
		for (int i=1;i<n;i++)
		u=read(),v=read();
		for (int i=1;i<=n;i++)
		{
			ffl[i][0]=ffl[i-1][1];
			ffl[i][1]=(long long)p[i]+min(ffl[i-1][1],ffl[i-1][0]);
		}
		for (int i=n;i>=1;i--)
		{
			ffr[i][0]=ffr[i+1][1];
			ffr[i][1]=(long long)p[i]+min(ffr[i+1][1],ffr[i+1][0]);
		}
		while (m--)
		{
			pos1=read();p1=read();
			pos2=read();p2=read();
			if (pos1>pos2)
			{
				swap(pos1,pos2);
				swap(p1,p2);
			}
			long long ans=0;
			if (p1==1) ans+=ffl[pos1][1];
			if (p2==1) ans+=ffr[pos2][1];
			if (p1==0) ans+=ffl[pos1][0];
			if (p2==0) ans+=ffr[pos2][0];
			if (pos1==pos2-1&&p1+p2==0) ans=-1;
			write(ans);
		}
		return 0;
	}
	for (int i=1;i<n;i++)
	{
		u=read();v=read();
		g[u][v]=1;g[v][u]=1;
		a[u].push_back(v);
		a[v].push_back(u);
	}
	while (m--)
	{
		for (int i=1;i<=n;i++)
		f[i][0]=0,f[i][1]=0;
		pos1=read();p1=read();pos2=read();p2=read();
		if (g[pos1][pos2]&&p1==0&&p2==0) puts("-1");
		else dfs(1,1),printf("%d\n",min(f[1][1],f[1][0]));
	}
}
