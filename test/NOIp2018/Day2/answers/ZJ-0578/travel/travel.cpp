#include<bits/stdc++.h>
#define FOR(i,x,y) for(int i=(x);i<=(y);i++)
#define DOR(i,x,y) for(int i=(x);i>=(y);i--)
#define LL long long
#define INF 2000000000
#define NM 5005
using namespace std;
int ver[NM<<1],head[NM],nex[NM<<1],tot,fg,ans[NM],cnt,n,m,vis[NM],bo[NM],FG,top,stk[NM];
priority_queue<int> q;
void add(int x,int y){
	ver[++tot]=y; nex[tot]=head[x]; head[x]=tot;
}
void DFS(int x,int fa){
	vis[x]=1;
	stk[++top]=x;
	for(int i=head[x];i;i=nex[i]){
		int y=ver[i];
		if(y==fa) continue;
		if(vis[y]){
			while(stk[top]!=y){
				bo[stk[top]]=1;
				top--;
			}
			bo[stk[top]]=1;
			FG=1; return;
		}
		DFS(y,x);
		if(FG) return;
	}
	top--;
}
void dfs(int x){
	int MIN=INF;
	vis[x]=1;
	ans[++cnt]=x;
	priority_queue<int> Q;
	while(!Q.empty()) Q.pop();
	for(int i=head[x];i;i=nex[i]){
		int y=ver[i];
		if(vis[y]) continue;
		Q.push(-y);
		if(bo[x]) q.push(-y);
	}
	while(!Q.empty()){
		MIN=-Q.top(); Q.pop();
		int now;
		if(bo[x]&&fg&&MIN!=now){
			now=-q.top(); q.pop();
		}
		if(bo[x]&&fg){
			if(MIN!=now){
				fg=0; dfs(now);
			}
			else dfs(MIN);
		}
		else{
			dfs(MIN);
		}
	}
}
int main(){  //long long   mod   neicun
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	FOR(i,1,m){
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y); add(y,x);
	}
	if(m==n){
		DFS(1,0);
		memset(vis,0,sizeof(vis));
		fg=1;
	}
	dfs(1);
	FOR(i,1,n) printf("%d ",ans[i]);
	fclose(stdin); fclose(stdout);
	return 0;
}
