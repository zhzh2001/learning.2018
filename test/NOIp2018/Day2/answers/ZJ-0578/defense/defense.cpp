#include<bits/stdc++.h>
#define FOR(i,x,y) for(int i=(x);i<=(y);i++)
#define DOR(i,x,y) for(int i=(x);i>=(y);i--)
#define LL long long
#define NM 100005
#define INF 1000000000
using namespace std;
int ver[NM<<1],head[NM],nex[NM<<1],tot,n,m,p[NM],f[NM][2];
char bru[10];
void add(int x,int y){
	ver[++tot]=y; nex[tot]=head[x]; head[x]=tot;
}
int main(){  //long long   mod   neicun
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%s",bru);
	FOR(i,1,n) scanf("%d",&p[i]);
	FOR(i,1,n-1){
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y); add(y,x);
	}
	memset(f,63,sizeof(f));
	f[0][0]=0; f[0][1]=0;
	while(m--){
		int x,v,y,u;
		scanf("%d%d%d%d",&x,&v,&y,&u);
		FOR(i,1,n){
			if(i==x){
				if(v){
					f[i][1]=min(f[i-1][1],f[i-1][0])+p[i];
				}
				else {
					f[i][0]=f[i-1][1];
				}
			}
			if(i==y){
				if(y){
					f[i][1]=min(f[i-1][1],f[i-1][0])+p[i];
				}
				else {
					f[i][0]=f[i-1][1];
				}
			}
			f[i][0]=f[i-1][1];
			f[i][1]=min(f[i-1][1],f[i-1][0])+p[i];
		}
		int ans=min(f[n][0],f[n][1]);
		if(ans>INF) puts("-1");
		else printf("%d\n",ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
