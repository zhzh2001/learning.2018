#include<bits/stdc++.h>
#define FOR(i,x,y) for(int i=(x);i<=(y);i++)
#define DOR(i,x,y) for(int i=(x);i>=(y);i--)
#define LL long long
#define NM 1000005
#define mod 1000000007
using namespace std;
int n,m,a[10][NM];
LL ans;
bool check(){
	FOR(i,1,n-1){
		FOR(j,1,m-1){
			if(a[i][j+1]<a[i+1][j]) return 0;
		}
	}
	return 1;
}
void dfs(int x,int y,int tot){
	if(tot==n*m){
		if(check()) ans++;
		return;
	}
	if(y>m) x++,y=1;
	a[x][y]=0;
	dfs(x,y+1,tot+1);
	a[x][y]=1;
	dfs(x,y+1,tot+1);
	
}
int main(){  //long long   mod   neicun
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	ans=1;
	if(n==1) puts("1");
	else if(n==2){
		FOR(i,1,m-1){
			ans=ans*3%mod;
		}
		ans=ans*4%mod;
		printf("%lld\n",ans);
	}
	else if(n==3){
		FOR(i,1,m-2){
			ans=ans*7%mod;
		}
		ans=ans*16%mod;
		printf("%lld\n",ans);
	}
	else{
		ans=0;
		dfs(1,1,0);
		printf("%lld\n",ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
