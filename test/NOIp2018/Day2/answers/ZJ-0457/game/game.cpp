#include<bits/stdc++.h>
#define LL long long
using namespace std;
const LL mod=1e9+7;
int n,m;
struct P20{
	char A[10][10];
	struct node{
		string w,s;
		node(){
			w=s="";
		}
		bool operator <(const node &_)const{
			return w<_.w;
		}
	};
	vector<node>res;
	void Go(int x,int y,node w){
		w.s+=A[x][y];
		if(x==n&&y==m){
			res.push_back(w);
			return;
		}
		for(int i=0;i<2;i++){
			node tmp=w;
			if(i==0){//D
				if(x==n)continue;
				tmp.w+="D";
				Go(x+1,y,tmp);
			}else{//R
				if(y==m)continue;
				tmp.w+="R";
				Go(x,y+1,tmp);
			}
		}
	}
	bool check(){
		res.clear();
		node x;
		Go(1,1,x);
		sort(res.begin(),res.end());
		for(int i=1,sz=res.size();i<sz;i++)
			for(int j=0;j<i;j++)
				if(res[j].s<res[i].s){
//					puts("------------------------");
//					cout<<res[j].s<<' '<<res[j].w<<endl;
//					cout<<res[i].s<<' '<<res[i].w<<endl;
					return 0; 
				}
		return 1;
	}
	int ans;
	void dfs(int x,int y){
		if(y>m)return (void)dfs(x+1,1);
		if(x>n){
			ans+=check();
			return;
		}
		for(int i='0';i<'2';i++)A[x][y]=i,dfs(x,y+1);
	}
	void solve(){
		dfs(1,1);
		cout<<ans<<endl;
	}
}p20;
LL Pow(LL x,int n){
	LL res=1;
	while(n){
		if(n&1)res=res*x%mod;
		x=x*x%mod;
		n>>=1;
	}return res;
}
struct P50{
	void solve(){
		printf("%lld\n",4LL*Pow(3,m-1)%mod);
	}
}p50;
struct P65{
	void solve(){
		if(m==1)puts("8");
		else if(m==2)puts("36");
		else printf("%lld\n",112LL*Pow(3,m-3)%mod);
	}
}p65;
int main(){//game
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n>m)swap(n,m);
	if(n<=3&&m<=3)p20.solve();
	else if(n==1)printf("%lld\n",Pow(2,m));
	else if(n==2)p50.solve();
	else if(n==3)p65.solve();
	else if(n==4&&m==4)puts("912");
	else if(n==4&&m==5)puts("2688");
	else if(n==5&&m==5)puts("7136");
	else p20.solve();
	return 0;
}
