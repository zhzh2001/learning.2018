#include<bits/stdc++.h>
#define M 5005
using namespace std;
bool ___1;
int n,m;
vector<int>G[M];
struct P60 {
	bool vis[M];
	void dfs(int v,int f) {
		printf("%d ",v);
		for(int i=0,sz=G[v].size(); i<sz; i++) {
			int u=G[v][i];
			if(u==f)continue;
			dfs(u,v);
		}
	}
	void solve() {
		dfs(1,0);
		puts("");
	}
} p60;
struct PLOOP{
	bool check(){
		for(int i=1;i<=n;i++)if(G[i].size()>2)return 0;
		return 1;
	}
	int stk[M],top;
	void dfs(int v,int f){
		stk[++top]=v;
		for(int i=0,sz=G[v].size();i<sz;i++){
			int u=G[v][i];
			if(u==f)continue;
			if(u==1)return;
			dfs(u,v);
		}
	}
	int ans[M];
	void solve(){
		dfs(1,0);
		int len=0;ans[++len]=1;
		int l=2,r=n;
		if(stk[r]<stk[l]){
			while(l<=r&&stk[r]<stk[2])ans[++len]=stk[r--];
			while(l<=r)ans[++len]=stk[l++];
		}else{
			while(l<=r&&stk[l]<stk[r])ans[++len]=stk[l++];
			while(l<=r)ans[++len]=stk[r--];
		}
		for(int i=1;i<=n;i++)printf("%d ",ans[i]);puts("");
	}
}ploop;
struct P100{
	int stk[M],top,dfn[M],L;
	void dfs(int v,int f){
		if(L)return;
		stk[++top]=v;
		dfn[v]=top;
		for(int i=0,sz=G[v].size();i<sz;i++){
			int u=G[v][i];
			if(u==f)continue;
			if(dfn[u])L=dfn[u];
			else dfs(u,v);
			if(L)return;
		}top--;
	}
	int lop[M<<1],len;
	int dela,delb;
	vector<int>ans,res;
	void dfs1(int v,int f){
		res.push_back(v);
		for(int i=0;i<(int)G[v].size();i++){
			int u=G[v][i];
			if(u==f)continue;
			if((u==dela&&v==delb)||(u==delb&&v==dela))continue;
			dfs1(u,v);
		}
	}
	void solve(){
		dfs(1,0);
		for(int i=L;i<=top;i++)lop[++len]=stk[i];
		for(int i=1;i<=len;i++)lop[i+len]=lop[i];
		for(int i=2;i<=len;i++){
			dela=lop[i],delb=lop[i+len-1];
			res.clear();
			dfs1(1,0);
			if(ans.empty())for(int j=0;j<n;j++)ans.push_back(res[j]);
			else{
				int flg=0;
				for(int j=0;!flg&&j<n;j++)
					if(res[j]<ans[j])flg=1;
					else if(ans[j]<res[j])break;
				if(flg){
					ans.clear();
					for(int j=0;j<n;j++)ans.push_back(res[j]);
				}
			}
		}
		for(int i=0;i<n;i++){
			printf("%d",ans[i]);
			if(i<n-1)putchar(' ');
			else putchar('\n');
		}
	}
}p100;
bool ___2;
int main() { //travel
//	printf("%lf\n",(&___2-&___1)/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1,a,b; i<=m; i++) {
		scanf("%d%d",&a,&b);
		G[a].push_back(b);
		G[b].push_back(a);
	}
	for(int i=1; i<=n; i++)sort(G[i].begin(),G[i].end());
	if(m==n-1)p60.solve();
	else p100.solve();
	return 0;
}
