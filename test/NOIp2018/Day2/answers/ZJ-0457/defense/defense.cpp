#include<bits/stdc++.h>
#define M 100005
#define LL long long
using namespace std;
bool ___1;
int n,m;
char Type[10];
struct edge{
	int to,fr;
}E[M<<1];
int lst[M],tot;
int co[M];
void addedge(int a,int b){
	E[++tot]=(edge){b,lst[a]};
	lst[a]=tot;
}
void change(LL &x,LL t){
	x=max(x,t);
}
struct P44{
	LL dp[2][M];
	int fa[M];
	void dfsInit(int v,int f){
		for(int i=lst[v];i;i=E[i].fr){
			int u=E[i].to;
			if(u==f)continue;
			fa[u]=v;
			dfsInit(u,v);
		}
	}
	void dfs(int v){
		change(dp[0][v],0),change(dp[1][v],co[v]);
		for(int i=lst[v];i;i=E[i].fr){
			int u=E[i].to;
			if(u==fa[v])continue;
			dfs(u);
			dp[0][v]+=dp[1][u];
			dp[1][v]+=min(dp[1][u],dp[0][u]);
		}	
	}
	void solve(){
		dfsInit(1,0);
		for(int i=1,a,x,b,y;i<=m;i++){
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(fa[a]==b||fa[b]==a)if(!x&&!y){puts("-1");continue;}
			memset(dp,0,sizeof(dp));
			dp[!x][a]=2e9;
			dp[!y][b]=2e9;
			dfs(1);
			LL ans=min(dp[1][1],dp[0][1]);
			if(ans>=2e9)ans=-1;
			printf("%lld\n",ans);
		}
	}	
}p44;
struct P{
	LL dp[2][M],tmp[2][M];
	int fa[M],dep[M];
	void Up(int v,int u){
		dp[1][v]+=min(dp[0][u],dp[1][u]);
		dp[0][v]+=dp[1][u];
	}
	void dfsInit(int v,int f){
		dp[1][v]=co[v],dp[0][v]=0;
		dep[v]=dep[f]+1;
		for(int i=lst[v];i;i=E[i].fr){
			int u=E[i].to;
			if(u==f)continue;
			fa[u]=v;
			dfsInit(u,v);
			Up(v,u);
		}
	}
	void Work(int &a){
		int f=fa[a];
		dp[1][f]-=min(tmp[0][a],tmp[1][a]);
		dp[0][f]-=tmp[1][a];
		Up(f,a);
		a=fa[a];
	}
	void solve(){
		dfsInit(1,0);
		for(int i=1,a,x,b,y;i<=m;i++){
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(fa[a]==b||fa[b]==a)if(!x&&!y){puts("-1");continue;}
			if(dep[a]<dep[b])swap(a,b),swap(x,y);
			int aa=a,bb=b;
			for(int j=a;j;j=fa[j])tmp[1][j]=dp[1][j],tmp[0][j]=dp[0][j];
			for(int j=b;j;j=fa[j])tmp[1][j]=dp[1][j],tmp[0][j]=dp[0][j];
			int del=dep[a]-dep[b];
			dp[!x][a]=dp[!y][b]=2e15;
			while(del--)Work(a);
			while(a!=b)Work(a),Work(b);
			while(a)Work(a);
			LL ans=min(dp[1][1],dp[0][1]);
			printf("%lld\n",ans);
			for(int j=aa;j;j=fa[j])dp[1][j]=tmp[1][j],dp[0][j]=tmp[0][j];
			for(int j=bb;j;j=fa[j])dp[1][j]=tmp[1][j],dp[0][j]=tmp[0][j];
		}
	}	
}p;
bool ___2;
int main(){//defense
//	printf("%lf\n",(&___2-&___1)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d %d %s",&n,&m,Type);
	for(int i=1;i<=n;i++)scanf("%d",&co[i]);
	for(int i=1,a,b;i<n;i++){
		scanf("%d%d",&a,&b);
		addedge(a,b);
		addedge(b,a);
	}
	if(n<=2000)p44.solve();
	else p.solve();
	return 0;
}
