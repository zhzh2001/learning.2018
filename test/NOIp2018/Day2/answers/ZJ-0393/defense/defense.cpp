#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

template <class T>void Rd(T &x){
	x=0;
	char c;
	while(c=getchar(),!isdigit(c));
	do{
		x=(x<<3)+(x<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
}

#define M 100005
#define oo 1061109567

struct list{
	int to,nx;
}lis[M<<1];
int tot,head[M];
void Add(int x,int y){
	lis[++tot]=(list){y,head[x]};
	head[x]=tot;
}

int n,m;
int A[M];

struct _1{
	
	int mk[1005];
	int dp[1005][2];
	
	void DFS(int x,int p){
		dp[x][0]=0,dp[x][1]=A[x];
		if(mk[x]!=-1)dp[x][1-mk[x]]=oo;
//		cout<<x<<" "<<p<<endl;
		for(int i=head[x];i;i=lis[i].nx){
			int to=lis[i].to;
			if(to==p)continue;
			DFS(to,x);
			if(mk[x]!=1)dp[x][0]+=dp[to][1];
			if(mk[x]!=0)dp[x][1]+=min(dp[to][1],dp[to][0]);
		}
	}
	
	bool check(int a,int b){
		for(int i=head[a];i;i=lis[i].nx)
			if(b==lis[i].to)return true;
		return false;
	}
	
	void solve(){
		for(int i=1;i<=n;i++)mk[i]=-1;
		for(int a,b;m--;){
//			cout<<m<<endl;
			Rd(a),Rd(mk[a]),Rd(b),Rd(mk[b]);
			DFS(1,0);
//			cout<<a<<" "<<mk[a]<<" "<<b<<" "<<mk[b]<<endl;
			if(mk[a]==0&&mk[b]==0&&check(a,b))puts("-1");
			else printf("%d\n",min(dp[1][0],dp[1][1]));
			mk[a]=mk[b]=-1;
		}
			
	}
	
}P44;

struct _2{
	
	long long dp[2][2][M];
	
	void solve(){
		memset(dp,0,sizeof(dp));
		for(int i=1;i<=n;i++){
			dp[0][0][i]=dp[0][1][i-1];
			dp[0][1][i]=min(dp[0][1][i-1],dp[0][0][i-1])+A[i];
		}
		for(int i=n;i>=1;i--){
			dp[1][0][i]=dp[1][1][i+1];
			dp[1][1][i]=min(dp[1][1][i+1],dp[1][0][i+1])+A[i];
		}
		for(int a,pa,b,pb;m--;){
			Rd(a),Rd(pa),Rd(b),Rd(pb);
			if(a>b)swap(a,b);
			long long ans=dp[0][pa][a]+dp[1][pb][b];
			if(pa==0&&pb==0)puts("-1");
			else printf("%lld\n",ans);
		}
	}
	
}P2;

struct _3{
	
	long long dp[2][2][M];
	
	void solve(){
		memset(dp,0,sizeof(dp));
		for(int i=1;i<=n;i++){
			dp[0][1][i]=A[i]+min(dp[0][1][i-1],dp[0][0][i-1]);
			if(i==1)dp[0][0][i]=oo;
			else dp[0][0][i]=dp[0][1][i-1];
		}
		for(int i=n;i>=1;i--){
			dp[1][1][i]=A[i]+min(dp[1][1][i+1],dp[1][0][i+1]);
			if(i==1)dp[1][0][i]=oo;
			else dp[1][0][i]=dp[1][1][i+1];
		}
		for(int a,b,pa,pb;m--;){
			Rd(a),Rd(pa),Rd(b),Rd(pb);
			if(a>b)swap(a,b);
			long long ans;
			if(pb==0)ans=dp[1][0][b]+dp[0][1][b-1];
			else ans=dp[1][1][b]+min(dp[0][0][b-1],dp[0][1][b-1]);
			printf("%lld\n",ans);
		}
	}
	
}P3;

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char str[10];
	Rd(n),Rd(m),scanf("%s",str);
	for(int i=1;i<=n;i++)Rd(A[i]);
	for(int i=1,x,y;i<n;i++){
		Rd(x),Rd(y);
		Add(x,y),Add(y,x);
	}
	if(0);
	if(n<=1000)P44.solve();
	else if(str[0]=='A'&&str[1]=='2')P2.solve();
	else if(str[0]=='A'&&str[1]=='1')P3.solve();
	return 0;
}
