#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

template <class T>void Rd(T &x){
	x=0;
	char c;
	while(c=getchar(),!isdigit(c));
	do{
		x=(x<<3)+(x<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
}

#define M 5005

vector<int>v[M];
struct edge{
	int a,b;
}eg[M];
int n,m;

struct _1{
	
	void DFS(int x,int p){
		printf("%d ",x);
		for(int i=0,up=v[x].size();i<up;i++){
			int to=v[x][i];
			if(to==p)continue;
			DFS(to,x);
		}
	}
	
	void solve(){
		DFS(1,0);
		puts("");
	}
	
}P1;

bool mark[M][M];

struct _2{
	
	int ans[M],res[M],t;
	bool mrk[M];
	
	void DFS(int x,int p){
		res[++t]=x;
		mrk[x]=true;
		for(int i=0,up=v[x].size();i<up;i++){
			int to=v[x][i];
			if(mrk[to]==true||mark[x][to]==true)continue;
			DFS(to,x);
		}
	}
	
	void solve(){
		memset(ans,63,sizeof(ans));
//		for(int i=0,up=v[5].size();i<up;i++)
//			printf("5 %d\n",v[5][i]);
		for(int i=1,x,y;i<=n;i++){
			x=eg[i].a,y=eg[i].b;
			mark[x][y]=mark[y][x]=true;t=0;
			DFS(1,0);
//			if(res[1]==1&&res[2]==35&&res[3]==5){
//				cout<<x<<" "<<y<<endl;
//				cout<<t<<endl;
//				for(int j=1;j<=t;j++)
//					cout<<res[j]<<" ";
//				cout<<endl;
//			}
			if(t==n){
				bool f=false;
				for(int j=1;j<=n;j++){
					if(res[j]<ans[j]){f=true;break;}
					if(res[j]>ans[j])break;
				}
				if(f==true){
					for(int j=1;j<=n;j++)
						ans[j]=res[j];
				}
			}
			for(int j=1;j<=n;j++)mrk[j]=false;
			mark[y][x]=mark[x][y]=false;
		}
		for(int i=1;i<=n;i++)
			printf("%d ",ans[i]);
		puts("");
	}
	
}P2;

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	Rd(n),Rd(m);
	for(int i=1,x,y;i<=m;i++){
		Rd(x),Rd(y);
		eg[i]=(edge){x,y};
		v[x].push_back(y);
		v[y].push_back(x);
	}
	for(int i=1;i<=n;i++)sort(v[i].begin(),v[i].end());
	if(n-1==m)P1.solve();
	else P2.solve();
	return 0;
}
