#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

template <class T>void Rd(T &x){
	x=0;
	char c;
	while(c=getchar(),!isdigit(c));
	do{
		x=(x<<3)+(x<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
}

#define P 1000000007

template <class T>void Add(T &x,T y){
	x+=y;
	if(x>=P)x-=P;
}

int n,m;

int Fast(int x,int k){
	int res=1;
	while(k){
		if(k&1)res=1ll*res*x%P;
		k>>=1,x=1ll*x*x%P;
	}
	return res;
}

struct _1{
	
	void solve(){
		int ans=Fast(3,m-1);
		ans=1ll*ans*4%P;
		printf("%d\n",ans);
	}
	
}P1;

struct _2{
	
	int dp[15][15];
	
	int rec(int x,int y){
		if(dp[x][y]!=-1)return dp[x][y];
		int res=0;
		
		Add(res,res);
		Add(res,res);
		return dp[x][y]=res;
	}
	
	void solve(){
		
	}
	
}P2;

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	Rd(n),Rd(m);
	if(n>m)swap(n,m);
	if(0);
	if(n==1)printf("%d\n",Fast(2,m));
	else if(n==2)P1.solve();
	else if(n==3&&m==3)puts("112");
	else if(n<=8&&m<=8)P2.solve();
	return 0;
}
