#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cctype>
#include <bitset>
#include <queue>
#define ll long long
#define rep(i,a,b) for(ll i=a;i<=b;++i)
#define drep(i,a,b) for(ll i=a;i>=b;--i)
using namespace std;
inline ll read()
{
	bool f=0;ll x=0;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=1;ch=getchar();}
	while(isdigit(ch)){x=((x+(x<<2))<<1)+(ch^48);ch=getchar();}
	return f?-x:x;
}
inline ll maxx(ll x,ll y)
{
	return x>y?x:y;
}
inline ll minn(ll x,ll y)
{
	return x<y?x:y;
}
inline ll gcd(ll a,ll b)
{
	if(!b) return a;
	return gcd(b,a%b);
}
inline ll lcm(ll a,ll b)
{
	return a/gcd(a,b)*b;
}

ll n,m,p[100050];
string type;
ll head[100050],to[200050],nxt[200050],cnt,pd[100050];
struct Node
{
	ll x,fa,dep;
}em[100050];
queue<Node>q;
inline void addedge(ll x,ll y)
{
	to[++cnt]=y;
	nxt[cnt]=head[x];
	head[x]=cnt;
}

ll ddp=1;

inline void bfs(ll x,ll fa)
{
	q.push((Node){x,fa,1});
	while(!q.empty())
	{
		Node tmp=q.front();
		if(tmp.dep!=ddp) ddp++;
	}
}

ll f[100050][2],ans[100050][2];

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();
	cin>>type;
	rep(i,1,n) p[i]=read();
	if(type[0]=='A')
	{
		rep(i,1,n-1) ll u=read(),v=read();
		rep(i,1,n)
		{
			f[i][0]=f[i-1][1];
			f[i][1]=minn(f[i-1][0],f[i-1][1])+p[i];
		}
		while(m--)
		{
			memset(ans,0,sizeof ans);
			ll a=read(),x=read(),b=read(),y=read();
			ll tmp=minn(a,b);
			ans[tmp][0]=f[tmp-1][1];
			ans[tmp][1]=minn(f[tmp-1][0],f[tmp-1][1])+p[tmp];
			if(tmp==a) ans[a][x?0:1]=0x7f;
			if(tmp==b) ans[b][y?0:1]=0x7f;
			rep(i,tmp+1,n)
			{
				ans[i][0]=ans[i-1][1];
				ans[i][1]=minn(ans[i-1][0],ans[i-1][1])+p[i];
				if(i==a) ans[a][x?0:1]=0x7f;
				if(i==b) ans[b][y?0:1]=0x7f;
			}
			if(ans[n][0]>=0x7f and ans[n][1]>=0x7f) puts("-1");
			else printf("%lld\n",minn(ans[n][0],ans[n][1]));
		}
		return 0;
	}
	else puts("-1");
	rep(i,1,n-1)
	{
		ll u=read(),v=read();
		addedge(u,v);
		addedge(v,u);
	}
	while(m--)
	{
		ll a=read(),x=read(),b=read(),y=read();
		if(x==0) pd[a]=-1;
		if(y==0) pd[b]=-1;
		bfs(a,0);
	}
	
	return 0;
}

