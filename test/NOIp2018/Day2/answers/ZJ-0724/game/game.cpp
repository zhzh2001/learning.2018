#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cctype>
#include <bitset>
#define ll long long
#define rep(i,a,b) for(ll i=a;i<=b;++i)
#define drep(i,a,b) for(ll i=a;i>=b;--i)
using namespace std;
const int MOD=1000000007;
inline ll read()
{
	bool f=0;ll x=0;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=1;ch=getchar();}
	while(isdigit(ch)){x=((x+(x<<2))<<1)+(ch^48);ch=getchar();}
	return f?-x:x;
}
inline ll maxx(ll x,ll y)
{
	return x>y?x:y;
}
inline ll minn(ll x,ll y)
{
	return x<y?x:y;
}
inline ll gcd(ll a,ll b)
{
	if(!b) return a;
	return gcd(b,a%b);
}
inline ll lcm(ll a,ll b)
{
	return a/gcd(a,b)*b;
}

ll n,m;
ll f[1000050];
inline ll qpow(ll x,ll p)
{
	ll ans=1;
	rep(i,1,p) ans=ans*x%MOD;
	return ans;
}
inline ll ksm(ll x,ll p)
{
	ll ans=1,cnt=0,tmp=x,em;
	while(p)
	{
		x=tmp;
		for(ll i=1;i<=p;i+=i)
		{
			cnt++;
			if(cnt&1) ans=ans*x%MOD;
			x=x*x%MOD;
			em=i;
		}
		p-=em;
	}
	return ans;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if(n==1)
	{
		printf("%lld\n",qpow(2,m));
		return 0;
	}
	if(n==3 and m==2)
	{
		puts("40");
		return 0;
	}
	if(n==2 and m<=10000)
	{
		f[1]=4;
		rep(i,2,m) f[i]=f[i-1]*((ll)qpow(2,m-i)+2)%MOD;
		printf("%lld\n",f[m]);
	}
	if(n==2 and m>10000)
	{
		f[1]=4;
		rep(i,2,m) f[i]=f[i-1]*((ll)ksm(2,m-i)+2)%MOD;
		printf("%lld\n",f[m]);
	}

	return 0;
}
