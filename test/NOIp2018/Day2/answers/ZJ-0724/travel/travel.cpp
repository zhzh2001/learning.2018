#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cctype>
#include <bitset>
#define ll long long
#define rep(i,a,b) for(ll i=a;i<=b;++i)
#define drep(i,a,b) for(ll i=a;i>=b;--i)
using namespace std;
inline ll read()
{
	bool f=0;ll x=0;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=1;ch=getchar();}
	while(isdigit(ch)){x=((x+(x<<2))<<1)+(ch^48);ch=getchar();}
	return f?-x:x;
}
inline ll maxx(ll x,ll y)
{
	return x>y?x:y;
}
inline ll minn(ll x,ll y)
{
	return x<y?x:y;
}
inline ll gcd(ll a,ll b)
{
	if(!b) return a;
	return gcd(b,a%b);
}
inline ll lcm(ll a,ll b)
{
	return a/gcd(a,b)*b;
}
ll head[5050],to[10050],nxt[10050],cnt;
inline void addedge(ll x,ll y)
{
	to[++cnt]=y;
	nxt[cnt]=head[x];
	head[x]=cnt;
}

bool vis[5050];
inline void dfs(ll x)
{
	printf("%lld ",x);
	vis[x]=1;
	ll flag=1;
	while(flag==1)
	{
		flag=0;
		ll miin=0x7f;
		for(ll i=head[x];i;i=nxt[i])
		{
			ll v=to[i];
			if(vis[v]) continue;
			flag=1;
			miin=minn(v,miin);
		}
		if(flag==1) dfs(miin);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	ll n=read(),m=read();
	rep(i,1,m)
	{
		ll u=read(),v=read();
		addedge(u,v);
		addedge(v,u);
	}
	dfs(1);
	
	return 0;
}
