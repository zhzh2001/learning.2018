#include <bits/stdc++.h>
#define rep(x,y,z) for(int x=(y);x<=(z);x++)
using namespace std;

int u[5100],v[5100],fir[5100],nxt[5100];
int tot=0;

inline void add(int x,int y)
{
	nxt[++tot]=fir[x];
	fir[x]=tot;
}

int main()
{
	//freopen("travel.in","r",stdin);
	//freopen("travel.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	rep(i,1,m)
	{
		scanf("%d%d",&u[i],&v[i]);
		add(u[i],v[i]);
		add(v[i],u[i]);
	}
	return 0;
}
