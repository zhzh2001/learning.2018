//Maggie
#include <cstdio>
#include <iostream>
#include <cstring>

using namespace std;

const int inf = 1e9+7;
const int N = 1e5+5;

int tot,Next[N*2],head[N*2],vet[N*2];
int p[N];
long long f[N][2];
char ch[10];
int n,m,a,x,y,b;

void add(int u,int v){
	Next[++tot] = head[u];
	vet[tot] = v;
	head[u] = tot;
}

void dfs(int u,int father){
	f[u][1]=p[u];f[u][0]=0;
	for (int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if (v == father) continue;
		dfs(v,u);
		f[u][0]+=f[v][1];
		if (v == a && x == 0) //f[u][0]-=f[v][1]
			f[u][0]+=f[v][0];
		if (v == b && y == 0) //f[u][0]-=f[v][1];
			f[u][0]+=f[v][0];
		if ((v!=a)&&(v!=b))
			f[u][1]+=min(f[v][0],f[v][1]);
		if (v==a)
			if (x==0) f[u][1]+=f[v][0];else
				f[u][1]+=f[v][1];
		if (v==b)
			if (y==0) f[u][1]+=f[v][0];else
				f[u][1]+=f[v][1];
	/*	if ((v != a || x==0) && (v != b || x==0))
			f[v][0]+=min(f[u][0],f[v][1]);
		if ((v != a || x==1) && (v != b || y==1))
			f[v][1]+=min(f[u][1],min(f[v][0],f[v][1]));*/
	}
	//f[u][1]=min(f[u][1],p[u]);
}

/*void dfs2(int u,int father){
	f[u][1]=p[u];f[u][0]=0;
	for (int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if (v == father) continue;
		dfs(v,u);
		f[u][0]+=f[v][1];
		if (v == a && x == 0) //f[u][0]-=f[v][1]
			f[u][0]+=f[v][0];
		if (v == b && y == 0) //f[u][0]-=f[v][1];
			f[u][0]+=f[v][0];
		if ((v!=a)&&(v!=b))
			f[u][1]+=min(f[v][0],f[v][1]);
		if (v==a)
			if (x==0) f[u][1]+=f[v][0];else
				f[u][1]+=f[v][1];
		if (v==b)
			if (y==0) f[u][1]+=f[v][0];else
				f[u][1]+=f[v][1];
	/*	if ((v != a || x==0) && (v != b || x==0))
			f[v][0]+=min(f[u][0],f[v][1]);
		if ((v != a || x==1) && (v != b || y==1))
			f[v][1]+=min(f[u][1],min(f[v][0],f[v][1]));*/
//	}
	//f[u][1]=min(f[u][1],p[u]);
//}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,&ch);
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);
		add(v,u);
	}
	bool flag = true;
//	memset(f,0x3f,sizeof(f));
	for (int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		flag=false;
		for (int j=head[a];j;j=Next[j])
			if (vet[j]==b) flag=true;
		if (flag==true)
			if (x==0 && y==0){
				printf("-1\n");
				continue;
			}
	//	memset(f,0x3f,sizeof(f));
	//	if (x==0) f[a][1]=inf;
	//	if (y==0) f[b][1]=inf;
		dfs(1,0);
		if (a==1 && x==0) f[1][1]=f[1][0];
		if (a==1 && x==1) f[1][0]=f[1][1];
	//	for (int i=1;i<=n;i++) printf("%d %d",f[i][0],f[i][1]);
	//	if (a==1 && x==0) f[1][1]=f[1][0];
	//	if (a==1 && x==1) f[1][0]=f[1][1];
		if (b==1 && y==0) f[1][1]=f[1][0];
		if (b==1 && y==1) f[1][0]=f[1][1];
		printf("%lld\n",min(f[1][0],f[1][1]));
	}
	return 0;
}
