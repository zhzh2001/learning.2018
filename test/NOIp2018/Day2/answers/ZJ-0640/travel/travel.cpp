//Maggie;
#include <cstdio>
#include <iostream>
#include <cstring>

using namespace std;

const int N = 5005;
const int inf = 1e9+7;

int cnt;
int vis[2*N],head[2*N],vet[2*N],Next[2*N],tot;
int Ans[N],ans[N];
int x[N],y[N];
int minv;
int n,m;

void add(int u,int v){
	Next[++tot]=head[u];
	vet[tot]=v;
	head[u]=tot;
}

void dfs(int u,int father){
	printf("%d ",u);
//	if (u == inf) return;
	vis[u] = true;
	minv=inf;
	for (int i=head[u];i;i=Next[i]){
		for (int j=head[u];j;j=Next[j]){
			int v=vet[j];
			if (v == father) continue;
			if (vis[v]) continue;
			minv = min(minv,v);
		}
		if (minv!=inf)
			ans[++cnt]=minv,dfs(minv,u);
	}	
}

void Dfs(int u,int father){
	vis[u] = true;
	minv=inf;
	for (int i=head[u];i;i=Next[i]){
		for (int j=head[u];j;j=Next[j]){
			int v=vet[j];
			if (v == father) continue;
			if (vis[v]) continue;
			minv = min(minv,v);
		}
		if (minv!=inf)
			ans[++cnt]=minv,Dfs(minv,u);
	}	
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);
		add(v,u);
		x[i]=u;y[i]=v;
	}
	if (m==n-1){
		memset(vis,0,sizeof(vis));
		cnt=1;
		ans[cnt]=1;
		dfs(1,0);
		//for (int i=1;i<=cnt;i++) printf("%d ",ans[i]);
	}
	if (m==n){
		for (int i=1;i<=n;i++) Ans[i] = inf;
		for (int i=1;i<=m;i++){
			cnt=1;ans[cnt]=1;tot=0;
			memset(head,0,sizeof(head));
			memset(Next,0,sizeof(Next));
			memset(vet,0,sizeof(vet));
			for (int j=1;j<=m;j++)
			if (i != j){
				add(x[j],y[j]);
				add(y[j],x[j]);
			}
			memset(vis,0,sizeof(vis));
			Dfs(1,0);
			if (cnt == n){
				for (int j=1;j<=n;j++){
					if (Ans[j] == ans[j]) continue;
					if (Ans[j] < ans[j]) break;
					for (int k=1;k<=n;k++)
						Ans[k] = ans[k];
					break;
 				}
			}
		}
		for (int i=1;i<=n;i++) printf("%d ",Ans[i]);
	}
	return 0;
}
