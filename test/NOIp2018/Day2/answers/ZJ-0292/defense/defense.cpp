#include<bits/stdc++.h>
using namespace std;
void fre()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
}
inline int read()
{
	int num = 0, flag = 1;char c = getchar();
	for(;c<'0'||c>'9';c = getchar())
	  if (c == '-') flag = -1;
	for(;c<='9'&&c>='0';c = getchar())
	  num = (num<<1)+(num<<3)+c-48;
	return num * flag;
}
#define LL long long
#define inf 1e12
int n, m, tote = 0;
char c[5];
int a[100010] = {};
struct edge
{
	int y,next;
}e[1000100];
int linke[100010] = {}, fa[100010] = {};
LL f[100010][2] = {};
void insert(int x,int y)
{
	e[++tote] = (edge){y,linke[x]};linke[x] = tote;
}
void dfs(int x,int fa)
{
	LL ans1 = 0, ans2 = 0;
	for(int i = linke[x];i;i = e[i].next)
	  {
	  	int y = e[i].y;
	  	if (y == fa) continue;
	    dfs(y,x);
		ans1 += f[y][1];
	    ans2 += min(f[y][0],f[y][1]);
	  }
	f[x][0] = max(ans1,f[x][0]);
	f[x][1] = max(ans2+a[x],f[x][1]);
}
int main()
{
	fre();
	n = read(), m = read();scanf("%s",c+1);
	for(int i = 1;i <= n;++i)
	  a[i] = read();
	for(int i = 1;i < n;++i)
	  {
	  	int x = read(), y = read();
	  	insert(x,y);insert(y,x);
	  }
	while (m--)
	  {
	  	int a = read(), x = read(), b = read(), y = read();
	  	if (!x&&!y&&(fa[a] == b||fa[b] == a)) puts("-1");
	  	else 
	  	  {
	  	  	for(int i = 1;i <= n;++i) 
	          f[i][1] = f[i][0] = 0;
	        f[a][x^1] = inf, f[b][y^1] = inf;
	        dfs(1,0);
	        LL ans = min(f[1][0],f[1][1]);
	        if (ans >= inf) puts("-1");
	        else printf("%lld\n",ans);
		  }
	  }
	return 0;
}
