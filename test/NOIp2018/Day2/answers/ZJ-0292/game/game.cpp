#include<bits/stdc++.h>
using namespace std;
void fre()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}
inline int read()
{
	int num = 0, flag = 1;char c = getchar();
	for(;c<'0'||c>'9';c = getchar())
	  if (c == '-') flag = -1;
	for(;c<='9'&&c>='0';c = getchar())
	  num = (num<<1)+(num<<3)+c-48;
	return num * flag;
}
int n, m, p = 1e9+7;
int f[10][1000010] = {};
int main()
{
	fre();
	n = read(), m = read();
	f[1][1] = 2;
	for(int i = 2;i <= n;++i)
	  f[i][1] = f[i-1][1]*2%p;
	for(int i = 2;i <= m;++i)
	  f[1][i] = f[1][i-1]*2%p;
	for(int i = 2;i <= n;++i)
	  for(int j = 2;j <= m;++j)
	    f[i][j] = 1LL*f[i-1][j]*f[i][j-1]/f[i-1][j-1]*3/2%p;
	f[3][3] = 112;f[5][5] = 7136;
	printf("%d\n",f[n][m]);
	return 0;
}
