#include <stdio.h>
#define mod %1000000007
const int mmap[8][8]={{1,1,1,1,1,1,1,1},{0,0,0,0,1,1,1,1},{0,0,0,0,0,0,1,1},{1,1,1,1,1,1,1,1},{0,0,0,1,0,0,1,1},{0,0,0,0,1,1,1,1},{0,0,0,0,0,0,1,1}};
int dp1[4],dp2[4],n,m;
long long sqr(long long x) {
	return x*x mod;
}
long long FMI(int exp,int di)
{
	long long ans=1;
	if(exp&1)
	{
		--exp;
		ans*=di;
	}
	return exp==0?ans:((ans*sqr(FMI(exp>>1,di)))mod);
}
	
long long solve3()
{
	long long dp1[8]={1,1,1,1,1,1,1,1};
	long long dp2[8];
	for(int i=1;i<m;i++)
	{
		for(int j=0;j<8;j++)
		{
			dp2[j]=0;
			for(int k=0;k<8;k++)
			{
				dp2[j]+=mmap[j][k]*dp1[k];
			}
		}
		for(int j=0;j<8;j++)
		{
			dp1[j]=dp2[j]%1000000007;
		}
	}
	long long ans=0;
	for(int j=0;j<8;j++)
	{
		ans=+dp1[j];
	}
	return ans%1000000007;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m<n) m+=n,n=m-n,m-=n;
	if(n==1) printf("%lld",FMI(m,2)mod);
	else if(n==2) printf("%lld\n",(4*FMI(m-1,3))mod);
	else if(n==3) printf("%lld\n",solve3());
}
