#include <stdio.h>
#include <vector>
#include <algorithm>
using std::vector;
using std::pair;
int n,m;
long int p[100003];
long int dp[100003][2];
long int min (long int a,long int b ){
	if(a==-1) return b;
	if(b==-1) return a;
	return a<b?a:b;
}
void solve1()
{
	for(register int i=0;i<n;i++)
	{
		scanf("%ld",&p[i]);
	}
	for(register int i=1;i<n;i++)
	{
		int a,b;
		scanf("%d%d",&a,&b);
	}
	for(register int i=0;i<m;i++)
	{
		int a1,b1,a2,b2;
		scanf("%d%d%d%d",&a1,&b1,&a2,&b2);
		dp[0][0]=0;
		dp[0][1]=p[0];
		bool flag=true;
		for(register int j=1;j<n;++j)
		{
			if(j==a1-1)
			{
				if(b1)
				{
					dp[j][0]=-1;
					dp[j][1]=min(dp[j-1][0],dp[j-1][1])+p[j];
				}
				else
				{
					dp[j][1]=-1;
					dp[j][0]=dp[j-1][1];
				}
			}
			else if(j==a2-1)
			{
				if(b2)
				{
					dp[j][0]=-1;
					dp[j][1]=min(dp[j-1][0],dp[j-1][1])+p[j];
				}
				else
				{
					dp[j][1]=-1;
					dp[j][0]=dp[j-1][1];
				}
			}
			else
			{
				dp[j][0]=dp[j-1][1];
				dp[j][1]=min(dp[j-1][1],dp[j-1][0])+p[j];
			}
			if(dp[j][0]==-1&&dp[j][1]==-1)
			{
				flag=false;
				break;
			}
		}
		printf("%ld\n",flag?min(dp[n-1][0],dp[n-1][1]):-1);
	}
}
int deepth[100003];
int du[5003];
int pos[5003];
pair<int,int> tmp;
vector <pair<int,int> >E;
void dfs(int x,int LP)
{
	for(register int i=pos[x];E[i].first==x;i++)
	{
		if(E[i].second==LP) continue;
		deepth[E[i].second]=deepth[x]+1;
		dfs(E[i].second);
	}
}
void solve2()
{
	for(register int i=0;i<n;i++)
	{
		scanf("%ld",&p[i]);
		du[i]=0; 
	}
	for(register int i=1;i<n;i++)
	{
		int a,b;
		scanf("%d%d",&a,&b);
		tmp.first=a;tmp.second=b;
		E.insert(tmp);
		tmp.first=b;tmp.second=a;
		E.insert(tmp);
		++du[a],++du[b];
	}
	sort(E.begig(),E.end());
	pos[0]=0;
	for(register int i=0;i<n;++i) pos[i+1]=pos[i]+du[i];
	deepth[0]=1;
	dfs(0,-1);
	for()
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char ch,int type;
	scanf("%d%d",&n,&m);
	do
	{
		scanf("%c",&ch);
	}while(ch<'A'||ch>'C');
	scanf("%d",&type);
	if(ch=='A') solve1();
	else solve2();
}
