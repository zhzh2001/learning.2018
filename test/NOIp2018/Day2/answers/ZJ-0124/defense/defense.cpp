#include <cstdio>
#include <vector>
#include <algorithm>
using std::min;
using std::vector;

int n, m;
char str[3];
vector<int> edg[100001];
int p[100001];

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d %d", &n, &m);
	scanf("%s", str);
	for (int i = 0; i < n; i++) scanf("%d", p + i);
	for (int i = 0; i < n - 1; i++) {
		int x, y;
		scanf("%d %d", &x, &y);
		x--;
		y--;
		edg[x].push_back(y);
		edg[y].push_back(x);
	}
	for (int i = 0; i < m; i++) {
		int a, x, b, y;
		scanf("%d %d %d %d", &a, &x, &b, &y);
		a--;
		b--;
		int k = (1 << n);
		int ans = 1 << 30;
		for (int j = 0; j < k; j++)
			if ((j & (1 << a)) == (x << a) && (j & (1 << b)) == (y << b)) {
				int tot = 0;
				bool is_valid = true;
				for (int l = 0; l < n; l++) {
					bool local_valid = true;
					if ((j & (1 << l)) == 0) {
						for (int q = 0; q < edg[l].size(); q++) {
							if ((j & (1 << edg[l][q])) == 0) {
								local_valid = false;
								break;
							}
						}
					}
					if (local_valid == false) {
						is_valid = false;
						break;
					}
					if (j & (1 << l)) {
						tot += p[l];
					}
				}
				if (is_valid) {
					ans = min(ans, tot);
				}
			}
		if (ans == (1 << 30)) printf("-1\n");
		else printf("%d\n", ans);
	}
	return 0;
}
