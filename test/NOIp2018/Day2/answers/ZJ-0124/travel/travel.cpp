#include <iostream>
#include <vector>
#include <cstdio>
#include <queue>
#include <functional>
#include <algorithm>
using std::sort;
using std::queue;
using std::greater;
using std::priority_queue;
using std::vector;

int n, m;
vector<int> edg[5001];
int fa[5001];
int rank[5001];
int vis[5001];
vector<int> ans;

int find(int x) {
	if (fa[x] == x) return x;
	int ret = find(fa[x]);
	fa[x] = ret;
	return ret;
}

int uni(int x, int y) {
	int fx = find(x);
	int fy = find(y);
	if (rank[fx] < rank[fy]) {
		fa[fx] = fy;
		rank[fy] += rank[fx];
	} else {
		fa[fy] = fx;
		rank[fx] += rank[fy];
	}
}

void min_sp() {
	priority_queue< int, vector<int>, greater<int> > q;
	for (int i = 0; i < edg[1].size(); i++)
		q.push(edg[1][i]);
	ans.push_back(1);
	vis[1] = 1;
	while (!q.empty()) {
		int head = q.top();
		q.pop();
		int fh = find(head);
		int fr = find(1);
		vis[head] = 1;
		if (fh != fr) {
			ans.push_back(head);
			uni(head, 1);
			for (int i = 0; i < edg[head].size(); i++) {
				int fe = find(edg[head][i]);
				int fr = find(1);
				if (fe != fr && vis[edg[head][i]] == 0)
					q.push(edg[head][i]);
			}
		}
	}
}

void greedy() {
	int cur = 1;
	int nxt = 0;
	int cnt = 1;
	ans.push_back(1);
	vis[1] = 1;
	while (cnt != n) {
		bool is_enter = false;
		for (int i = 0; i < edg[cur].size(); i++)
			if (!vis[edg[cur][i]]) {
				vis[edg[cur][i]] = 1;
				nxt = edg[cur][i];
				ans.push_back(nxt);
				cnt++;
				is_enter = true;
				break;
			}
		if (!is_enter) {
			nxt = cur;
			cur = fa[cur];
		} else {
			fa[nxt] = cur;
			cur = nxt;
		}
	}
}

bool comp(int a, int b) {
	return a < b;
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d %d", &n, &m);
	for (int i = 0; i < m; i++) {
		int x, y;
		scanf("%d %d", &x, &y);
		edg[x].push_back(y);
		edg[y].push_back(x);
	}
	for (int i = 0; i < n; i++)
		sort(edg[i].begin(), edg[i].end(), comp);
	if (m == n - 1) {
		greedy();
	} else {
		for (int i = 0; i < n; i++) fa[i] = i;
		min_sp();
	}
	for (int i = 0; i < ans.size(); i++)
		printf("%d ", ans[i]);
	putchar('\n');
	return 0;
}
