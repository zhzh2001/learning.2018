#include<bits/stdc++.h>
using namespace std;

const int maxn=5005;

vector <int> e[maxn];

int n,m,nedge=0,nod=0,vis[maxn];

void dfs(int v,int fa){
	vis[v]=1;
	printf("%d ",v);
	for (int i=0;i<e[v].size();i++){
		int k=e[v][i];
		if (k==fa||vis[k]) continue;
		dfs(k,v);
	}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(vis,0,sizeof(vis));
	vis[1]=1;
	scanf("%d%d",&n,&m);
	while(m--){
		int a,b;
		scanf("%d%d",&a,&b);
		e[a].push_back(b);
		e[b].push_back(a);
	}
	for (int i=1;i<=n;i++){
		sort(e[i].begin(),e[i].end());
	}
	dfs(1,0);
	return 0;
}
