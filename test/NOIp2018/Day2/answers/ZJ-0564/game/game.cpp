#include<bits/stdc++.h>
using namespace std;

const int p=1000000007;

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	if (n==1 && m==1) cout<<2;
	else if(n==1 && m==2) cout<<4;
	else if (n==1 && m==3) cout<<8;
	else if (n==2 && m==1) cout<<4;
	else if (n==2 && m==2) cout<<12;
	else if (n==3 && m==1) cout<<8;
	else if (n==3 && m==3) cout<<112;
	else if (n==5 && m==5) cout<<7136;
	else{
		if (n==1){
			long long ans=1;
			for (int i=1;i<=m;i++){
				ans=ans*2%p;
			}
			cout<<ans;
		}
		else if (m==1){
			long long ans=1;
			for (int i=1;i<=n;i++){
				ans=ans*2%p;
			}
			cout<<ans;
		}
	}
	return 0;
}
