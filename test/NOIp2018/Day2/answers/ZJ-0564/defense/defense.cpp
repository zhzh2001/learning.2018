#include<bits/stdc++.h>
using namespace std;

const int maxn=100005;

long long f[maxn][2];

struct Edge{
	int next,to;
}edge[maxn*2];

int n,m,nedge=0,a,b,x,y,boo;
string s;
int w[maxn],son[maxn],head[maxn]; 

void addedge(int a,int b){
	edge[nedge].to=b;
	edge[nedge].next=head[a];
	head[a]=nedge++;
}

void dfs1(int v,int fa){
	son[v]=1;
	for (int i=head[v];i!=-1;i=edge[i].next){
		int u=edge[i].to;
		if (u==fa) continue;
		dfs1(u,v);
		son[v]+=son[u];
	}
}

int check(int v){
	if (v==a) return x;
	if (v==b) return y;
	return -1;
}

void dfs(int v,int fa){
	if (boo==0) return;
	if (son[v]==1){
		if (check(v)==1){
			f[v][0]=-1;
			f[v][1]=w[v];
		}
		else if (check(v)==0){
			f[v][0]=0;
			f[v][1]=-1;
		}
		else{
			f[v][0]=0;
			f[v][1]=w[v];
		}
		return;
	}
	f[v][0]=0;
	f[v][1]=w[v];
	for (int i=head[v];i!=-1;i=edge[i].next){
		int u=edge[i].to;
		if (u==fa) continue;
		dfs(u,v);
		if (check(v)==1||f[v][0]==-1){
			f[v][0]=-1;
			if (f[u][0]==-1||check(u)==1) f[v][1]+=f[u][1];
			else if (f[u][1]==-1||check(u)==0) f[v][1]+=f[u][0];
			else f[v][1]+=min(f[u][0],f[u][1]); 
		}
		else if (check(v)==0||f[v][1]==-1){
			f[v][1]=-1;
			if (check(u)==0||f[u][1]==-1){
				boo=0;
				return;
			}
			else{
				f[v][0]+=f[u][1];
			}
		}
		else{
			if (f[u][0]==-1||check(u)==1){
				f[v][0]+=f[u][1];
				f[v][1]+=f[u][1];
			}
			else if (f[u][1]==-1||check(u)==0){
				f[v][0]=-1;
				f[v][1]+=f[u][0];
			}
			else{
				f[v][0]+=f[u][1];
				f[v][1]+=min(f[u][1],f[u][0]);
			}
		}
	}
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof(head));
	cin>>n>>m>>s;
	if (s[1]=='2'){
		for (int i=1;i<=m;i++){
			cout<<-1<<endl;
		}
		return 0;
	}
	for (int i=1;i<=n;i++){
		scanf("%d",&w[i]);
	}
	for (int i=1;i<n;i++){
		int aa,bb;
		scanf("%d%d",&aa,&bb);
		addedge(aa,bb);
		addedge(bb,aa);
	}
	dfs1(1,0);
	while(m--){
		boo=1;
		scanf("%d%d%d%d",&a,&x,&b,&y);
	/*	if (x==0 && y==0){
			for (int i=head[a];i!=-1;i=edge[i].next){
				if (edge[i].to==b) {boo=0;break;}
			}
		}*/
		dfs(1,0);
		if (boo==0) printf("-1\n");
		else if (check(1)==1||f[1][0]==-1) printf("%lld\n",f[1][1]);
		else if (check(1)==0||f[1][1]==-1) printf("%lld\n",f[1][0]);
		else printf("%lld\n",min(f[1][0],f[1][1]));
	}
	return 0;
}

/*6 100 C3
1 2 3 4 5 6
1 2
2 3
2 4
1 5
1 6
5 1 6 1
*/
