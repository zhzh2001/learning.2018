#include <set>
#include <map>
#include <cmath>
#include <queue>
#include <bitset>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
//#define maxn 
#define MOD 1000000007
#define int long long
using namespace std;
char gc()
{
	static char buf[100000],*p1=buf+100000,*p2=p1;
	if(p1==p2)
	{
		p1=buf;
		p2=buf+fread(buf,1,100000,stdin);
	}
	return *p1++;
}
#define gc getchar
int read()
{
	int d=0,w=1;
	char c=gc();
	for(;c<'0'||c>'9';c=gc())
		if(c=='-') w=-1;
	for(;c>='0'&&c<='9';c=gc())
		d=(d<<1)+(d<<3)+c-48;
	return d*w;
}
void write(int x)
{
	if(x<0)
	{
		putchar('-');
		x=-x;
	}
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void wrs(int x)
{
	write(x);
	putchar(' ');
}
void wln(int x)
{
	write(x);
	putchar('\n');
}
int n,m;
int power(int x,int y)
{
	int ans=1;
	for(;y;y>>=1)
	{
		if(y&1) (ans*=x)%=MOD;
		(x*=x)%=MOD;
	}
	return ans%MOD;
}
signed main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();
	m=read();
	if(n>m) swap(n,m);
	if(n==1) write(power(2,m));
	if(n==2) write(power(3,m-2)*12%MOD);
	if(n==3) write(power(3,m-3)*112%MOD);
	if(n==4&&m==4) write(912);
	if(n==4&&m==5) write(2688);
	if(n==5&&m==5) write(7136);
	return 0;
}
