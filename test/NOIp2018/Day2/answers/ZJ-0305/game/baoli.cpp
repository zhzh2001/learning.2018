#include <set>
#include <map>
#include <cmath>
#include <queue>
#include <bitset>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
//#define maxn 
//#define MOD 
//#define int long long
using namespace std;
char gc()
{
	static char buf[100000],*p1=buf+100000,*p2=p1;
	if(p1==p2)
	{
		p1=buf;
		p2=buf+fread(buf,1,100000,stdin);
	}
	return *p1++;
}
#define gc getchar
int read()
{
	int d=0,w=1;
	char c=gc();
	for(;c<'0'||c>'9';c=gc())
		if(c=='-') w=-1;
	for(;c>='0'&&c<='9';c=gc())
		d=(d<<1)+(d<<3)+c-48;
	return d*w;
}
void write(int x)
{
	if(x<0)
	{
		putchar('-');
		x=-x;
	}
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void wrs(int x)
{
	write(x);
	putchar(' ');
}
void wln(int x)
{
	write(x);
	putchar('\n');
}
struct node
{
	int a,b;
	friend bool operator < (node a,node b)
	{
		return a.a<b.a;
	}
};
int a[100][100],n,m,ans;
vector<node> b;
void ddfs(int x,int y,int haha,int ha)
{
	if(x==n&&y==m)
	{
		node tmp;
		tmp.a=haha;
		tmp.b=ha;
		b.push_back(tmp);
		return;
	}
	if(x<n) ddfs(x+1,y,haha<<1,(ha<<1)+a[x+1][y]);
	if(y<m) ddfs(x,y+1,haha<<1|1,(ha<<1)+a[x][y+1]);
}
int check()
{
	for(int i=1;i<b.size();i++)
		if(b[i].b>b[i-1].b) return 0;
	return 1;
}
void dfs(int x,int y)
{
	if(x>n)
	{
		b.clear();
		ddfs(1,1,0,a[1][1]);
		sort(b.begin(),b.end());
		ans+=check();
		return;
	}
	a[x][y]=0;
	dfs(x+(y+1>m),y+1>m?1:y+1);
	a[x][y]=1;
	dfs(x+(y+1>m),y+1>m?1:y+1);
}
signed main()
{
//	freopen(".in","r",stdin);
//	freopen(".out","w",stdout);
	n=read();
	m=read();
	dfs(1,1);
	write(ans);
	return 0;
}
