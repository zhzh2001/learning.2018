#include <set>
#include <map>
#include <cmath>
#include <queue>
#include <bitset>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 5010
//#define MOD 
//#define int long long
using namespace std;
char gc()
{
	static char buf[100000],*p1=buf+100000,*p2=p1;
	if(p1==p2)
	{
		p1=buf;
		p2=buf+fread(buf,1,100000,stdin);
	}
	return *p1++;
}
#define gc getchar
int read()
{
	int d=0,w=1;
	char c=gc();
	for(;c<'0'||c>'9';c=gc())
		if(c=='-') w=-1;
	for(;c>='0'&&c<='9';c=gc())
		d=(d<<1)+(d<<3)+c-48;
	return d*w;
}
void write(int x)
{
	if(x<0)
	{
		putchar('-');
		x=-x;
	}
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void wrs(int x)
{
	write(x);
	putchar(' ');
}
void wln(int x)
{
	write(x);
	putchar('\n');
}
int n,m,ans[maxn],r,vis[maxn],aans[maxn],xxx,yyy,bb[maxn][2];
vector<int> a[maxn];
void dfs(int k,int fa)
{
//	wrs(k);
	vis[k]=1;
	ans[++r]=k;
	for(int i=0;i<a[k].size();i++)
		if(a[k][i]!=fa&&(k!=xxx||a[k][i]!=yyy)&&(k!=yyy||a[k][i]!=xxx)&&!vis[a[k][i]]) dfs(a[k][i],k);
}
int check()
{
	for(int i=1;i<=n;i++)
		if(!vis[i]) return 0;
	for(int i=1;i<=n;i++)
		if(ans[i]<aans[i]) return 1;
			else if(ans[i]>aans[i]) return 0;
	return 0;
}
signed main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=m;i++)
	{
		int x=read(),y=read();
		bb[i][0]=x;
		bb[i][1]=y;
		a[x].push_back(y);
		a[y].push_back(x);
	}
	for(int i=1;i<=n;i++)
		sort(a[i].begin(),a[i].end());
	if(m==n-1)
	{
		dfs(1,0);
		for(int i=1;i<=n;wrs(ans[i++]));
		return 0;
	}
	for(int i=1;i<=n;i++)
		aans[i]=n;
	for(int i=1;i<=m;i++)
	{
		xxx=bb[i][0];
		yyy=bb[i][1];
		r=0;
		memset(vis,0,sizeof vis);
		dfs(1,0);
		if(check())
			for(int j=1;j<=n;j++)
				aans[j]=ans[j];
	}
	for(int i=1;i<=n;wrs(aans[i++]));
	return 0;
}
