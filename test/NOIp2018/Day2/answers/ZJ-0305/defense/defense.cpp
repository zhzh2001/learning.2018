#include <set>
#include <map>
#include <cmath>
#include <queue>
#include <bitset>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100010
#define inf 0x3f3f3f3f3f3f3f3f
//#define MOD 
#define int long long
using namespace std;
char gc()
{
	static char buf[100000],*p1=buf+100000,*p2=p1;
	if(p1==p2)
	{
		p1=buf;
		p2=buf+fread(buf,1,100000,stdin);
	}
	return *p1++;
}
#define gc getchar
int read()
{
	int d=0,w=1;
	char c=gc();
	for(;c<'0'||c>'9';c=gc())
		if(c=='-') w=-1;
	for(;c>='0'&&c<='9';c=gc())
		d=(d<<1)+(d<<3)+c-48;
	return d*w;
}
void write(int x)
{
	if(x<0)
	{
		putchar('-');
		x=-x;
	}
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void wrs(int x)
{
	write(x);
	putchar(' ');
}
void wln(int x)
{
	write(x);
	putchar('\n');
}
int tp,p[maxn],n,m,bb[maxn<<1][2],h[maxn],f[maxn][2],a,b,x,y,r,g[maxn][2],fffa[maxn];
void add(int x,int y)
{
	bb[++r][0]=y;
	bb[r][1]=h[x];
	h[x]=r;
}
void dfs(int k,int fa)
{
	for(int i=h[k];i;i=bb[i][1])
	{
		if(bb[i][0]==fa) continue;
		dfs(bb[i][0],k);
		f[k][0]+=f[bb[i][0]][1];
		f[k][1]+=min(f[bb[i][0]][1],f[bb[i][0]][0]);
		if(f[k][0]>inf) f[k][0]=inf;
		if(f[k][1]>inf) f[k][1]=inf;
	}
	f[k][1]+=p[k];
	if(f[k][1]>inf) f[k][1]=inf;
	if(k==a)
	{
		if(x==0) f[k][1]=inf;
			else f[k][0]=inf;
	}
	if(k==b)
	{
		if(y==0) f[k][1]=inf;
			else f[k][0]=inf;
	}
}
void dfs2(int k,int fa)
{
	if(k!=1) g[k][0]=g[fa][1]+f[fa][1]-min(f[k][1],f[k][0]);
	if(k!=1) g[k][1]=min(g[fa][1]+f[fa][1]-min(f[k][1],f[k][0]),g[fa][0]+f[fa][0]-f[k][1]);
	for(int i=h[k];i;i=bb[i][1])
	{
		if(bb[i][0]==fa) continue;
		dfs2(bb[i][0],k);
	}
}
void haha(int k,int fa)
{
	fffa[k]=fa;
	for(int i=h[k];i;i=bb[i][1])
	{
		if(bb[i][0]==fa) continue;
		haha(bb[i][0],k);
	}
}
signed main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();
	m=read();
	tp=read();
//	wln(tp);
	for(int i=1;i<=n;p[i++]=read());
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read();
		add(x,y);
		add(y,x);
	}
	if(n<=2000&&m<=2000||tp==3)
	{
		for(int i=1;i<=m;i++)
		{
			a=read();
			x=read();
			b=read();
			y=read();
			memset(f,0,sizeof f);
			dfs(1,0);
//			for(int i=1;i<=n;i++)
//				wrs(f[i][0]),wln(f[i][1]);
			wln(min(f[1][0],f[1][1])<inf?min(f[1][0],f[1][1]):-1);
		}
		return 0;
	}
	dfs(1,0);
	dfs2(1,0);
//	for(int i=1;i<=n;i++)
//	{
//		wrs(f[i][0]);
//		wrs(f[i][1]);
//		wln(min(g[i][0]+f[i][0],g[i][1]+f[i][1]));
//	}
	if(tp==1)
		for(int i=1;i<=m;i++)
		{
			a=read();
			x=read();
			b=read();
			y=read();
			wln(g[b][y]+f[b][y]);
		}
	if(tp==2)
	{
		haha(1,0);
		for(int i=1;i<=m;i++)
		{
			a=read();
			x=read();
			b=read();
			y=read();
			if(x==y&&x==0)
			{
				wln(-1);
				continue;
			}
			int ans;
			if(fffa[b]==a)
			{
				if(x==1) ans=f[a][1]+g[a][1]-min(f[b][0],f[b][1]);
					else ans=f[a][0]+g[a][0]-f[b][1];
				if(y==1) ans+=f[b][1];
					else ans+=f[b][0];
			}
			else
			{
				if(y==1) ans=f[b][1]+g[b][1]-min(f[a][0],f[a][1]);
					else ans=f[b][0]+g[b][0]-f[a][1];
				if(x==1) ans+=f[a][1];
					else ans+=f[a][0];
			}
			wln(ans);
		}
	}
	return 0;
}
