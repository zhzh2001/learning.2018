#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <vector>
#include <queue>
using namespace std;

vector<int> ve[5005], ans;
bool vis[5005];

void addedge(int u, int v){
	ve[u].push_back(v);
}
int s, t, fa[5005];

void dfs(int u){
	vis[u] = 1;
	ans.push_back(u);
	for(int i = 0; i < ve[u].size(); i++){
		int v = ve[u][i];
		if(vis[v]){
			if(v != fa[u]){
				s = u;
				t = v;
			}
			continue;
		}
		fa[v] = u;
		dfs(v);
	}
}

int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	int n, m;
	scanf("%d%d", &n, &m);
	for(int i = 1; i <= m; i++){
		int x, y;
		scanf("%d%d", &x, &y);
		addedge(x, y);
		addedge(y, x);
	}
	for(int i = 1; i <= n; i++) sort(ve[i].begin(), ve[i].end());
	dfs(1);
	if(m == n - 1){
		for(int i = 0; i < ans.size(); i++) printf("%d ", ans[i]);
		puts("");
		return 0;
	}
	return 0;
}
