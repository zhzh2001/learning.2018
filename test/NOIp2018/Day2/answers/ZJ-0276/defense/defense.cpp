#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <vector>
using namespace std;

vector<int> ve[2005];
int a, x, b, y;
int dp[2005][2], num[2005];
char st[5];

void addedge(int u, int v){
	ve[u].push_back(v);
}

const int INF = 1 << 30;

void dfs(int u, int fa){
	dp[u][1] = num[u];
	for(int i = 0; i < ve[u].size(); i++){
		int v = ve[u][i];
		if(v == fa) continue;
		dfs(v, u);
		if(v == a){
			if(!x) dp[u][0] = INF, dp[u][1] += dp[v][0];
			else dp[u][0] += dp[v][1], dp[u][1] += dp[v][1];
			continue;
		}
		if(v == b){
			if(!y) dp[u][0] = INF, dp[u][1] += dp[v][0];
			else dp[u][0] += dp[v][1], dp[u][1] += dp[v][1];
			continue;
		}
		dp[u][0] += dp[v][1];
		dp[u][1] += min(dp[v][0], dp[v][1]);
	}
}

int main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	int n, m;
	scanf("%d%d%s", &n, &m, st);
	for(int i = 1; i <= n; i++) scanf("%d", &num[i]);
	for(int i = 1; i < n; i++){
		int x, y;
		scanf("%d%d", &x, &y);
		addedge(x, y);
		addedge(y, x);
	}
	for(int i = 1; i <= m; i++){
		memset(dp, 0, sizeof(dp));
		scanf("%d%d%d%d", &a, &x, &b, &y);
		int flag = 0;
		for(int i = 0; i < ve[a].size(); i++)
			if(ve[a][i] == b && !x && !y){
				puts("-1");
				flag = 1;
				break;
			}
		if(flag) continue;
		dfs(1, 0);
		if(a == 1) printf("%d\n", dp[1][x]);
		else printf("%d\n", min(dp[1][0], dp[1][1]));
	}
	return 0;
}
