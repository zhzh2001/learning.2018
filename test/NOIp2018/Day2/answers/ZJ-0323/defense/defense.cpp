#include<iostream>
#include<cstring>
#include<cstdlib>
#include<cstdio>
#include<algorithm>
#include<vector>
#define maxn 200009
#define maxx 200000000000
using namespace std;
typedef long long ll;
struct ding{
  int to,nex;
}edge[maxn];
int head[maxn],cnt=0,n,m,a[maxn];
ll dp[maxn][2];
void add(int u,int v){edge[++cnt]=(ding){v,head[u]};head[u]=cnt;}
void dfs(int x,int fa)
{
  for (int i=head[x];i;i=edge[i].nex)
  {
    int to=edge[i].to;
    if (to==fa) continue;
    dfs(to,x);
    dp[x][1]+=min(dp[to][1],dp[to][0]);
    dp[x][0]+=dp[to][1];
    dp[x][1]=min(maxx,dp[x][1]);
    dp[x][0]=min(maxx,dp[x][0]);
  }
   dp[x][1]=min(maxx,dp[x][1]);
   dp[x][0]=min(maxx,dp[x][0]);
}
char s[100];
int main()
{
  freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout); 
  scanf("%d%d%s",&n,&m,s);
  int u,v,x,y;
  for (int i=1;i<=n;i++) scanf("%d",&a[i]);
  //cout<<"!"<<endl;
  for (int i=1;i<n;i++) 
  {
  	scanf("%d%d",&u,&v);
    add(u,v);add(v,u);
  }
  for (int i=1;i<=m;i++)
  {
    scanf("%d%d%d%d",&u,&x,&v,&y);
    for (int j=1;j<=n;j++) dp[j][0]=0,dp[j][1]=a[j];
    dp[u][x^1]=maxx; dp[v][x^1]=maxx;
    dfs(1,1);
    if (min(dp[1][0],dp[1][1])==maxx) printf("-1\n");
    else printf("%lld\n",min(dp[1][0],dp[1][1]));
  }
  return 0;
}
