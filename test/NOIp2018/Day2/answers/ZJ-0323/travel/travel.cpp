#include<iostream>
#include<cstring>
#include<cstdio>
#include<algorithm>
#include<cstdlib>
#include<vector>
#include<queue>
#include<stack>
#define maxn 200009
using namespace std;
struct ding{
   int to,nex,nod;
}edge[maxn];
stack<int>q;
int n,m,ans[maxn],head[maxn],Ins[maxn],siz=0,cnt=0,huan=0;
int a[2][maxn],dfn[maxn],low[maxn],qaq=0,fr[maxn],f[maxn];
vector<int>e[maxn];
void tarjan(int x)
{
  dfn[x]=low[x]=++qaq;q.push(x);
  for (int i=head[x];i;i=edge[i].nex)
  {
  	int to=edge[i].to;
  	if (!dfn[to]) fr[to]=edge[i].nod,tarjan(to),low[x]=min(low[x],low[to]);
  	else if (edge[i].nod!=fr[x]) low[x]=min(low[x],dfn[to]);
  }
  if (low[x]==dfn[x])
  {
  	if (q.top()==x) q.pop();
  	else
  	{
  	  while (q.top()!=x) huan++,Ins[q.top()]=true,q.pop();
	  q.pop();Ins[x]=true;huan++;
    }
  }
}
void add(int u,int v,int w)
{
  edge[++siz]=(ding){v,head[u],w};head[u]=siz;
}
void dfs(int x,int fa)
{
  ans[++cnt]=x;int len=0;
  //if (Ins[x]) {work(x,fa);return;}
  for (int i=head[x];i;i=edge[i].nex)
  {
    int to=edge[i].to;
	if (to==fa) continue;
	e[x].push_back(to);
	len++;
  } 
  if (len>0) sort(e[x].begin(),e[x].end());
  for (int i=0;i<len;i++) dfs(e[x][i],x); 
}
int main()
{
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  scanf("%d%d",&n,&m);
  for (int i=1;i<=m;i++)
  {
  	int u,v;
  	scanf("%d%d",&u,&v);
  	add(u,v,i);add(v,u,i);
  }
  //for (int i=1;i<=n;i++) vis[i]=false;
  cnt=0;
  for (int i=1;i<=n;i++) if (!dfn[i]) tarjan(i);
  dfs(1,1);
  for (int i=1;i<n;i++) printf("%d ",ans[i]);
  printf("%d\n",ans[n]);
  return 0;
}
