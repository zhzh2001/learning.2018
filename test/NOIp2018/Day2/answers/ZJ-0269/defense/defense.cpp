#include <bits/stdc++.h>
using namespace std;
#define file "defense"
typedef long long ll;
const ll INF=100000000000LL;
int read(){
	char c=getchar();
	int x=0;
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
const int maxn=100002;
struct graph{
	int n,m;
	struct edge{
		int to,next;
	}e[maxn*2];
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	int a,x,b,y;
	ll dp[100002][2];
	int p[100002];
	void dfs(int u,int fa){
		dp[u][0]=0;
		dp[u][1]=p[u];
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa)
				continue;
			dfs(v,u);
			dp[u][1]+=min(dp[v][0],dp[v][1]);
			dp[u][0]+=dp[v][1];
		}
		if (u==a)
			dp[u][x^1]=INF;
		if (u==b)
			dp[u][y^1]=INF;
	}
	ll work(){
		dfs(1,0);
		return min(dp[1][0],dp[1][1]);
	}
}g;
char s[5];
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read(),m=read();
	scanf("%s",s);
	for(int i=1;i<=n;i++)
		g.p[i]=read();
	for(int i=1;i<n;i++){
		int u=read(),v=read();
		g.addedge(u,v);
		g.addedge(v,u);
	}
	for(int i=1;i<=m;i++){
		g.a=read(),g.x=read(),g.b=read(),g.y=read();
		ll ans=g.work();
		if (ans>=INF)
			puts("-1");
		else printf("%lld\n",ans);
	}
}
