#include <bits/stdc++.h>
using namespace std;
#define file "game"
typedef long long ll;
const int mod=1000000007;
int read(){
	char c=getchar();
	int x=0;
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
ll power(ll x,ll y){
	ll ans=1;
	x%=mod;
	while(y){
		if (y&1)
			ans=ans*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return ans;
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read(),m=read();
	if (n==3 && m==3)
		return puts("112"),0;
	if (n>m)
		swap(n,m);
	if (n==1){ //2^m
		printf("%lld",power(2,m));
		return 0;
	}
	if (n==2){
		printf("%lld",4*power(3,m-1)%mod);
		return 0;
	}
}
