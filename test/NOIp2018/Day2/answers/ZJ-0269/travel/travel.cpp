#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> pii;
#define file "travel"
int read(){
	char c=getchar();
	int x=0;
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
struct graph{
	int n,m;
	int e[5002][5002],cnt[5002];
	void addedge(int u,int v){
		e[u][++cnt[u]]=v;
	}
	void addbiedge(int u,int v){
		m++;
		addedge(u,v);
		addedge(v,u);
	}
	int fa[5002];
	bool vis[5002];
	bool flag;
	pii cir[5002];
	int cur;
	void dfs(int u){
		vis[u]=1;
		if (flag)
			return;
		for(int i=1;i<=cnt[u];i++){
			int v=e[u][i];
			if (v==fa[u])
				continue;
			if (vis[v]){
				int o=u;
				cir[++cur]=make_pair(u,v);
				while(o!=v){
					cir[++cur]=make_pair(o,fa[o]);
					o=fa[o];
				}
				flag=1;
				return;
			}
			fa[v]=u;
			dfs(v);
			if (flag)
				return;
		}
	}
	int du,dv;
	int ans[5002],now[5002],cl;
	void check(){
		if (!ans[1]){
			for(int i=1;i<=n;i++)
				ans[i]=now[i];
			return;
		}
		bool flag=false;
		for(int i=1;i<=n;i++)
			if (ans[i]!=now[i]){
				flag=now[i]<ans[i];
				break;
			}
		if (!flag)
			return;
		for(int i=1;i<=n;i++)
			ans[i]=now[i];
	}
	void dfs2(int u,int fa){
		now[++cl]=u;
		for(int i=1;i<=cnt[u];i++){
			int v=e[u][i];
			if (v==fa || (u==du && v==dv) || (u==dv && v==du))
				continue;
			dfs2(v,u);
		}
	}
	void work(){
		for(int i=1;i<=n;i++)
			sort(e[i]+1,e[i]+cnt[i]+1);
		if (m==n-1){
			du=dv=0;
			cl=0;
			dfs2(1,0);
			check();
			return;
		}
		dfs(1);
		for(int i=1;i<=cur;i++){
			du=cir[i].first,dv=cir[i].second;
			cl=0;
			dfs2(1,0);
			check();
		}
	}
}g;
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=g.n=read(),m=read();
	for(int i=1;i<=m;i++){
		int u=read(),v=read();
		g.addbiedge(u,v);
	}
	g.work();
	for(int i=1;i<=n;i++)
		printf("%d%c",g.ans[i]," \n"[i==n]);
}
