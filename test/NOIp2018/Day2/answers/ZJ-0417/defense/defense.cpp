#include <bits/stdc++.h>
#define INF 100000000000ll
#define N 200001
using namespace std;
int n,m,E,P,PA,Q,QA,fa[N],a[N],fir[N],nex[N*2],to[N*2];
long long fl[N],mf[N];
void add(int x,int y)
{
	to[++E]=y;nex[E]=fir[x];fir[x]=E;
}
void dfs(int now)
{
	long long sum=0,sum2=0;
	for(int i=fir[now];i;i=nex[i])
	if(to[i]!=fa[now])
	{
		dfs(to[i]);
		long long ha=min(fl[to[i]],mf[to[i]]);
		if(ha==INF)
		{
			fl[now]=mf[now]=INF;
			return;
		}
		sum+=ha;
		if(sum2<INF && fl[to[i]]<INF)
			sum2+=fl[to[i]];
		else
			sum2=INF;
	}
	fl[now]=sum+a[now];
	mf[now]=sum2;
	if(P==now)
	{
		if(PA) mf[now]=INF;
		else fl[now]=INF;
	}
	if(Q==now)
	{
		if(QA) mf[now]=INF;
		else fl[now]=INF;
	}
}
void build(int now,int fat)
{
	fa[now]=fat;
	for(int i=fir[now];i;i=nex[i])
	if(to[i]!=fat) build(to[i],now);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char type[10];
	scanf("%d%d%s",&n,&m,type);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	for(int i=1;i<n;i++)
	{
		int p,q;
		scanf("%d%d",&p,&q);
		add(p,q);add(q,p);
	}
	build(1,0);
	if(type[1]=='1')
	{
		dfs(1);
		for(int i=1;i<=m;i++)
		{
			int a,b,c,d;
			scanf("%d%d%d%d",&a,&b,&c,&d);
			if(b&&d)printf("%d\n",fl[1]);
			else
			if(b||d)puts("-1");
			else
			printf("%d\n",mf[1]);
		}
	}
	else
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&P,&PA,&Q,&QA);
		dfs(1);
		long long ans=min(fl[1],mf[1]);
		if(ans==INF) puts("-1");
		else printf("%lld\n",ans);
	}
	return 0;
}
