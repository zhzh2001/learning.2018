#include <bits/stdc++.h>
#define N 200001
using namespace std;
int cirlen,n,m,E,fir[N],nex[N*2],to[N*2],tem[N],son[N],bro[N],fa[N],cirlis[N];
bool vis[N],cir[N],cu[N];
void add(int x,int y)
{
	to[++E]=y;nex[E]=fir[x];fir[x]=E;
}
void build(int now,int fat)
{
	int len=0;vis[now]=1;fa[now]=fat;
	for(int i=fir[now];i;i=nex[i])
	if(!vis[to[i]] &&(!(cu[now]&&cu[to[i]]))) tem[++len]=to[i];
	sort(tem+1,tem+len+1);
	for(int i=1;i<len;i++)
		bro[tem[i]]=tem[i+1];
	son[now]=len?tem[1]:0;
	for(int i=son[now];i;i=bro[i])
	if(!vis[i])
		build(i,now);
	else
	{
		for(int j=i;j!=now;j=fa[j])
		{
			cirlis[++cirlen]=j;
			cir[j]=1;
		}
		if(cirlis[1]>cirlis[cirlen])
			for(int i=1;i<cirlen-i+1;i++)
				swap(cirlis[i],cirlis[cirlen-i+1]);
		int gg=bro[cirlis[1]],lala=2;
		while(cirlis[lala]<gg) lala++;
		cu[cirlis[lala-1]]=cu[cirlis[lala]]=1;
	}
}
void dfs(int now)
{
	printf("%d ",now);
	for(int i=son[now];i;i=bro[i])
		dfs(i);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		int p,q;
		scanf("%d%d",&p,&q);
		add(p,q);add(q,p);
	}
	build(1,0);
	if(m==n)
	{
		memset(vis,0,sizeof vis);
		memset(bro,0,sizeof bro);
		build(1,0);
	}
	dfs(1);
	return 0;
}
