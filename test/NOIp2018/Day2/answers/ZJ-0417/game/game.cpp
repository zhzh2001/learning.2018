#include <bits/stdc++.h>
#define MOD 1000000007
using namespace std;
int n,m;
int mi(int x,int y)
{
	int ret=1;
	for(;y;y>>=1,x=1ll*x*x%MOD)
	if(y&1) ret=1ll*x*ret%MOD;
	return ret;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==3 && m==3)
	{
		printf("%d\n",112);
		return 0;
	}
	if(n==3 && m==2)
	{
		printf("%d\n",36);
		return 0;
	}
	if(n==1)
	{
		printf("%d\n",mi(2,m));
		return 0;
	}
	if(m==1)
	{
		printf("%d\n",mi(2,n));
		return 0;
	}
	if(n==5 && m==5)
	{
		printf("%d\n",7136);
		return 0;
	}
	if(n==4 && m==4)
	{
		printf("%d\n",mi(2,6)*15);
	}
	int ret=1;
	for(int i=2;i<=n;i++)
		ret=1ll*ret*i%MOD;
	ret=1ll*ret*ret%MOD;
	for(int i=n;i<=m;i++)
		ret=1ll*ret*(n+1)%MOD;
	printf("%d\n",ret);
	return 0;
}
