var
    head, len : array[0..200000] of longint;
    heap : array[0..5000, 0..5000] of longint;
    edge : array[0..200000] of record
        t, next : longint;
    end;
    n, m, x, y, i, num, u, v : longint;
    vis, vis1 : array[0..200000] of boolean;

procedure swap(var x, y : longint);
var
    tmp : longint;

begin
    tmp := x; x := y; y := tmp;
end;

procedure add(x, y : longint);

begin
    inc(num);
    edge[num].t := y;
    edge[num].next := head[x];
    head[x] := num;
end;

procedure push(x, u : longint);
var
    i : longint;

begin
    inc(len[u]); i := len[u];
    heap[u][i] := x;
    while i > 1 do
    begin
        if heap[u][i] < heap[u][i >> 1] then
        begin
            swap(heap[u][i], heap[u][i >> 1]);
            i := i >> 1;
        end else break;
    end;
end;

procedure pop(u : longint);
var
    i, x : longint;

begin
    heap[u][1] := heap[u][len[u]];
    dec(len[u]); i := 1;
    while (i << 1) <= len[u] do
    begin
        if ((i << 1) or 1 > len[u]) or (heap[u][i << 1] < heap[u][(i << 1) or 1]) then
            x := i << 1 else x := (i << 1) or 1;
        if heap[u][i] > heap[u][x] then
        begin
            swap(heap[u][i], heap[u][x]);
            i := x;
        end else break;
    end;
end;

procedure dfs(u : longint);
var
    i, v : longint;

begin
    write(u,' ');
    vis[u] := true;
    len[u] := 0;
    i := head[u];
    while i <> 0 do
    begin
        v := edge[i].t;
        if not vis[v] then push(v, u);
        i := edge[i].next;
    end;
    while len[u] > 0 do
    begin
        dfs(heap[u][1]); pop(u);
    end;
end;

procedure do1;

begin
    dfs(1);
end;

procedure do2;

begin
    push(1, 1);
    while len[1] > 0 do
    begin
        u := heap[1][1];
        pop(1);
        if vis[u] then continue;
        vis[u] := true;
        write(u,' ');
        i := head[u];
        while i <> 0 do
        begin
            v := edge[i].t;
            if not vis[v] and not vis1[v] then push(v, 1);
            i := edge[i].next;
        end;
    end;
end;

begin
    assign(input,'travel.in'); reset(input);
    assign(output,'travel.out'); rewrite(output);
    readln(n, m);
    for i := 1 to m do
    begin
        readln(x, y);
        add(x, y); add(y, x);
    end;
    if m = n - 1 then do1 else do2;
    close(input); close(output);
end.