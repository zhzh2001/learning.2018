var
    dp : array[0..200000, 0..1] of int64;
    edge : array[0..200000] of record
        t, next : longint;
    end;
    head, money : array[0..200000] of int64;
    flag, flag1 : array[0..100000] of boolean;
    n, m, i, x, y, num : longint;
    ch1, ch2, kong : char;

procedure add(x, y : longint);

begin
    inc(num);
    edge[num].t := y;
    edge[num].next := head[x];
    head[x] := num;
end;

function max(x, y : int64) : int64;

begin
    if x > y then exit(x) else exit(y);
end;

function min(x, y : int64) : int64;

begin
    if x < y then exit(x) else exit(y);
end;

procedure dfs(u, pre : longint);
var
    i, v : longint;
    leaf : boolean;

begin
    leaf := true;
    i := head[u];
    dp[u][0] := 0; dp[u][1] := 0;
    while i <> 0 do
    begin
        v := edge[i].t;
        if v <> pre then
        begin
            leaf := false;
            dfs(v, u);
            inc(dp[u][0], dp[v][1]);
            inc(dp[u][1], min(dp[v][1], dp[v][0]));
        end;
        i := edge[i].next;
    end;
    if leaf then
    begin
        dp[u][1] := 0; dp[u][0] := 0;
    end;
    inc(dp[u][1], money[u]);
    if flag[u] then dp[u][0] := maxlongint * 20;
    if flag1[u] then dp[u][1] := maxlongint * 20;
end;

procedure do1;
var
    i, a, x, b, y, j : longint;
    ans : int64;

begin
    for i := 1 to m do
    begin
        readln(a, x, b, y);
        for j := 1 to n do
        begin
            flag[j] := false;
            flag1[j] := false;
        end;
        flag[a] := x = 1; flag[b] := y = 1;
        flag1[a] := x = 0; flag1[b] := y = 0;
        for j := 1 to n do
        begin
            dp[j][0] := maxlongint * 20;
            dp[j][1] := maxlongint * 20;
        end;
        dfs(1, 0);
        ans := min(dp[1][0], dp[1][1]);
        if ans >= maxlongint * 20 then
            writeln(-1) else
            writeln(ans);
    end;
end;

begin
    assign(input,'defense.in'); reset(input);
    assign(output,'defense.out'); rewrite(output);
    readln(n, m, kong, ch1, ch2);
    for i := 1 to n do read(money[i]);
    for i := 1 to n - 1 do
    begin
        readln(x, y);
        add(x, y); add(y, x);
    end;
    do1;
    close(input); close(output);
end.