const
    p = 1000000007;
var
    a : array[0..1000, 0..1000] of char;
    flag : boolean;
    s, num : array[0..1000000] of ansistring;
    n, m, tot : longint;
    ans : int64;
    sum : ansistring;

function max(x, y : longint) : longint;

begin
    if x > y then exit(x) else exit(y);
end;

procedure check;
var
    i, j, x, y : longint;

begin
    flag := true;
    for i := 1 to tot do
    begin
        x := 1; y := 1;
        num[i] := '';
        for j := 1 to length(s[i]) do
        begin
            if s[i][j] = 'R' then inc(y) else inc(x);
            num[i] := num[i] + a[x][y];
        end;
        for j := 1 to i - 1 do
            if num[i] < num[j] then
            begin
                flag := false;
                break;
            end;
        if not flag then break;
    end;
    if flag then ans := (ans + 1) mod p;
end;

procedure work(i, j : longint);

begin
    if (i = n) and (j = m) then
    begin
        inc(tot);
        s[tot] := sum;
        exit;
    end;
    if j < m then
    begin
        sum := sum + 'R';
        work(i, j + 1);
        delete(sum, length(sum), 1);
    end;
    if i < n then
    begin
        sum := sum + 'D';
        work(i + 1, j);
        delete(sum, length(sum), 1);
    end;
end;

procedure dfs(i, j : longint);

begin
    if i > n then
    begin
        check; exit;
    end;
    if j < m then
    begin
        a[i][j] := '0';
        dfs(i, j + 1);
        a[i][j] := '1';
        dfs(i, j + 1);
    end else
    begin
        a[i][j] := '0';
        dfs(i + 1, 1);
        a[i][j] := '1';
        dfs(i + 1, 1);
    end;
end;

procedure do4;
var
    i, j : longint;

begin
    if n = 4 then j := m else j := n;
    if j = 4 then
        writeln(912) else
    begin
        ans := 2688;
        for i := 6 to j do ans := ans * 3 mod p;
        writeln(ans);
    end;
end;

procedure do3;
var
    i, j : longint;

begin
    if n = 3 then j := m else j := n;
    if j = 1 then
        writeln(8) else
    if j = 2 then
        writeln(36) else
    begin
        ans := 112;
        for i := 4 to j do ans := ans * 3 mod p;
        writeln(ans);
    end;
end;

procedure do2;
var
    i, j : longint;

begin
    if n = 2 then j := m else j := n;
    ans := 4;
    for i := 2 to j do ans := ans * 3 mod p;
    writeln(ans);
end;

procedure do1;
var
    i : longint;

begin
    ans := 1;
    for i := 1 to max(n, m) do ans := ans * 2 mod p;
    writeln(ans);
end;

begin
    assign(input,'game.in'); reset(input);
    assign(output,'game.out'); rewrite(output);
    readln(n, m);
    if (n = 1) or (m = 1) then
        do1 else
    if (n = 2) or (m = 2) then
        do2 else
    if (n = 3) or (m = 3) then
        do3 else
    if (n = 4) or (m = 4) then
        do4 else
    begin
        work(1, 1);
        dfs(1, 1);
        writeln(ans);
    end;
    close(input); close(output);
end.