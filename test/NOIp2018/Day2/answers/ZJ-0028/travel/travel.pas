var
tt:boolean;
o:array[0..10] of longint;
vv:array[0..5000,0..5000] of boolean;
vis:array[0..50000] of boolean;
q,c,sum,dfn,low,stack,e,last,head:array[0..50000] of longint;
n,maxx,v,m,cnt,i,l,num,x,y,index:longint;
que:array[0..5000,0..5000] of longint;
procedure add(x,y:longint);
begin
  inc(cnt);
  e[cnt]:=y;
  last[cnt]:=head[x];
  head[x]:=cnt;
end;
procedure qsort(l,r,x:longint);
var i,j,t,m:longint;
begin
  i:=l;j:=r;m:=que[(l+r) div 2,x];
  repeat
    while que[i,x]<m do inc(i);
    while que[j,x]>m do dec(j);
    if not (i>j) then
      begin
        t:=que[i,x];que[i,x]:=que[j,x];que[j,x]:=t;
        inc(i);dec(j);
      end;
  until i>j;
  if i<r then qsort(i,r,x);
  if l<j then qsort(l,j,x);
end;
function max(a,b:longint):longint;begin if a>b then exit(a) else exit(b);end;
function min(a,b:longint):longint;begin if a<b then exit(a) else exit(b);end;
procedure dfs(x:longint);
var i,v,tot:longint;
begin
  write(x,' ');
  vis[x]:=true;
  i:=head[x];
  tot:=0;
  while i<>0 do
    begin
      v:=e[i];
      if (vis[v])or(vv[x,v]) then begin i:=last[i];continue;end;
      inc(tot);
      que[tot,x]:=v;
      i:=last[i];
    end;
  qsort(1,tot,x);
  for i:=1 to tot do dfs(que[i,x]);
end;
procedure tarjan(x,fa:longint);
var i,v:longint;
begin
  inc(num);dfn[x]:=num;low[x]:=num;
  inc(index);stack[index]:=x;
  vis[x]:=true;
  i:=head[x];
  while i<>0 do
    begin
      v:=e[i];
      if fa=v then begin i:=last[i];continue;end;
      if dfn[v]=0 then
        begin
          tarjan(v,x);
          low[x]:=min(low[v],low[x]);
        end
      else if vis[v] then low[x]:=min(low[v],low[x]);
      i:=last[i];
    end;
  if dfn[x]=low[x] then
    begin
      inc(l);
      repeat
        inc(sum[l]);
        c[stack[index]]:=l;
        vis[stack[index]]:=false;
        dec(index);
      until x=stack[index+1];
    end;
end;
begin
assign(input,'travel.in');reset(input);
assign(output,'travel.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
    begin
      read(x,y);
      add(x,y);add(y,x);
    end;
  if m=n-1 then dfs(1);
  if m=n then
    begin
      for i:=1 to n do
        if dfn[i]=0 then tarjan(i,0);
      num:=0;
      for i:=1 to n do
        if sum[c[i]]>1 then begin inc(num);q[num]:=i;end;
      maxx:=0;
      for i:=1 to n do maxx:=max(maxx,q[i]);
      i:=head[maxx];
      num:=0;
      while i<>0 do
        begin
          v:=e[i];
          if c[v]<>c[x] then begin i:=last[i];continue;end;
          inc(num);
          o[num]:=v;
          i:=last[i];
        end;
      if o[1]<o[2] then
        begin
          vv[o[1],maxx]:=true;vv[maxx,o[1]]:=true;
        end
      else begin vv[o[2],maxx]:=true;vv[maxx,o[2]]:=true;end;
      dfs(1);
    end;
close(input);close(output);
 
end.

