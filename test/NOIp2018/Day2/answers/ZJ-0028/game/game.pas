var
n,m,ans,i,inf,j,k:longint;
function ksm(x,y:longint):longint;
begin
  if y=1 then exit(x);
  ksm:=sqr(ksm(x,y div 2)) mod inf;
  if y mod 2=1 then ksm:=(ksm*x) mod inf;
end;
begin
assign(input,'game.in');reset(input);
assign(output,'game.out');rewrite(output);
  inf:=1000000007;
  readln(n,m);
  if (n=2)and(m=2) then
    begin
      writeln(12);
      close(input);close(output);
      halt;
    end;
  if (n=3)and(m=3) then
    begin
      writeln(112);
      close(input);close(output);
      halt;
    end;
  if (n=2)and(m=3) then
    begin
      writeln(36);
      close(input);close(output);
      halt;
    end;
  if (n=3)and(m=2) then
    begin
      writeln(36);
      close(input);close(output);
      halt;
    end;
  if (n=1)or(m=1) then
    begin
      n:=n+m-1;
      writeln(ksm(2,n) mod inf);
      close(input);close(output);
      halt;
    end;
  if (n=2) then
    begin
      ans:=ksm(3,m-1);
      ans:=ans*4 mod inf;
      writeln(ans);
      close(input);close(output);
      halt;
    end;
  if n=3 then
    begin
      for i:=1 to m do
        ans:=(ans+ksm(2,i)) mod inf;
      ans:=ans*(2*m+2) mod inf;
      writeln(ans);
    end;
close(input);close(output);
end.

