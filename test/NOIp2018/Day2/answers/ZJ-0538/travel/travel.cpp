#include<bits/stdc++.h>
using namespace std;
const int N=5005;
int ans[N],b[N][N],flag[N],n,xx[N],yy[N],m,a[N];
int pd(){
	if (a[0]<n)return 0;
	if (ans[0]==0)return 1;
	for (int i=1;i<=n;i++)
		if (a[i]!=ans[i])return a[i]<ans[i];
	return 0;	
}
void dfs(int x,int y){
	printf("%d ",x);
	for (int i=1;i<=b[x][0];i++)
		if (b[x][i]!=y)dfs(b[x][i],x);
}
void dfs2(int x,int y,int z){
	a[++a[0]]=x;flag[x]=1;
	for (int i=1;i<=b[x][0];i++)
		if (!flag[b[x][i]]&&(x!=xx[z]||b[x][i]!=yy[z])&&
		(b[x][i]!=xx[z]||x!=yy[z]))dfs2(b[x][i],x,z);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d",&xx[i],&yy[i]);
		b[xx[i]][++b[xx[i]][0]]=yy[i];
		b[yy[i]][++b[yy[i]][0]]=xx[i];
	}
	for (int i=1;i<=n;i++)sort(b[i]+1,b[i]+b[i][0]+1);
	if (m==n-1){
		dfs(1,0);
		return 0;
	}
	for (int i=1;i<=m;i++){
		a[0]=0;
		memset(flag,0,sizeof flag);
		dfs2(1,0,i);
		if (pd())for (int j=0;j<=n;j++)ans[j]=a[j];
	}
	for (int i=1;i<=n;i++)printf("%d ",ans[i]);
	return 0;
}
