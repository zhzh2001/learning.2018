#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll N=200005;
ll dp[N][2],x,y,f[N][2],g[N][2],num[N],fi[N],ne[N],zz[N],tot,a,b,c,d,n,m;
char s[100];
void jb(ll x,ll y){
	ne[++tot]=fi[x];
	fi[x]=tot;
	zz[tot]=y;
}
void dfs(ll x,ll y){
	dp[x][0]=dp[x][1]=0;
	for (ll i=fi[x];i;i=ne[i])
		if (zz[i]!=y)dfs(zz[i],x);
	if (!(x==a&&c==0)&&!(x==b&&d==0)){
		dp[x][1]=num[x];
		for (ll i=fi[x];i;i=ne[i])
			if (zz[i]!=y)dp[x][1]+=min(dp[zz[i]][1],dp[zz[i]][0]);
	}
	else dp[x][1]=1e16;
	if (!(x==a&&c==1)&&!(x==b&&d==1)){
		dp[x][0]=0;
		for (ll i=fi[x];i;i=ne[i])
			if (zz[i]!=y)dp[x][0]+=dp[zz[i]][1];
	}
	else dp[x][0]=1e16;
}
ll read(){
	ll x=0;char c=0;
	for (;c<'0'||c>'9';c=getchar());
	for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
	return x;
}
void write(ll x){
	if (x>=10)write(x/10);
	putchar(x%10+48);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",s);
	for (ll i=1;i<=n;i++)num[i]=read();
	for (ll i=1;i<n;i++){
		x=read();y=read();
		jb(x,y);jb(y,x);
	}	
	if (n>2000&&s[0]=='A'&&(s[1]='1'||s[1]=='2')){
		for (ll i=n;i;i--){
			g[i][0]=g[i+1][1];
			g[i][1]=min(g[i+1][0],g[i+1][1])+num[i];
		}
		for (ll i=1;i<=n;i++){
			f[i][0]=f[i-1][1];
			f[i][1]=min(f[i-1][0],f[i-1][1])+num[i];
		}
		if (s[1]=='2'){
			while (m--){
				a=read();c=read();b=read();d=read();
				if (a>b){
					swap(a,b);
					swap(b,d);
				}
				if (c==0&&d==0)puts("-1");
				else write(f[a][c]+g[b][d]),puts("");
			}
		}
		else {
			memset(f,0,sizeof f);
			f[1][0]=1e16;f[1][1]=num[1];
			for (ll i=2;i<=n;i++){
				f[i][0]=f[i-1][1];
				f[i][1]=min(f[i-1][0],f[i-1][1])+num[i];
			}
			while (m--){
				a=read();c=read();b=read();d=read();
				if (a>b){
					swap(a,b);
					swap(b,d);
				}
				ll ans=f[b][d]+g[b][d];
				if (d==1)ans-=num[b];
				write(ans),puts("");
			}
		}
		return 0;
	}
	while (m--){
		a=read();c=read();b=read();d=read();
		dfs(1,0);
		ll ans=min(dp[1][0],dp[1][1]);
		if (ans>=1e16)puts("-1");
		else write(ans),puts("");
	}
	return 0;
}
