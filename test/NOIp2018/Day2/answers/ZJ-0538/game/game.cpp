#include<bits/stdc++.h>
using namespace std;
const int M=1e9+7;
int n,m,b[100],ans,cnt,a[100][100],flag;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1){
		long long ans=1;
		for (int i=1;i<=m;i++)(ans*=2)%=M;
		printf("%lld",ans);
		return 0;
	}
	if (n==2){
		long long ans=4;
		for (int i=1;i<m;i++)(ans*=3)%=M;
		printf("%lld\n",ans);
		return 0;
	}
	if (n==3){
		if (m==1){
			puts("8");
			return 0;
		}
		if (m==2){
			puts("36");
			return 0;
		}
		long long ans=112;
		for (int i=1;i<=m-3;i++)(ans*=3)%=M;
		printf("%lld\n",ans);
		return 0;
	}
	return 0;
}
