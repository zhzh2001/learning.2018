//game
#include <bits/stdc++.h>
#define ll long long
#define lop(i, x, y) for (register int i = (x), _E = (y); i <= _E; ++i)
#define pol(i, x, y) for (register int i = (x), _E = (y); i >= _E; --i)
using namespace std;

const int mo = 1e9 + 7;
ll ksm (ll a, ll b, ll ret = 1) {
	for ( ; b; b >>= 1) {
		if (b & 1) ret = ret * a % mo;
		a = a * a % mo;
	}
	return ret;
}

int n, m;
ll solve2 () {return ksm(3, m - 1) * 2 * 2 % mo;}
ll solve3 () {if (m == 3) return 112;}
int main () {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	cin >> n >> m; if (n > m) swap(n, m);
	if (n == 1) return printf("%lld\n", ksm(2, m)), 0;
	if (n == 2) return printf("%lld\n", solve2()), 0;
	if (n == 3) return printf("%lld\n", solve3()), 0;
	return 0;
}
