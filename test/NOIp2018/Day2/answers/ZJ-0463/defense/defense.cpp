//defense
#include <bits/stdc++.h>
#define ll long long
#define lop(i, x, y) for (register int i = (x), _E = (y); i <= _E; ++i)
#define pol(i, x, y) for (register int i = (x), _E = (y); i >= _E; --i)
using namespace std;

inline int read () {
	int x = 0; char ch = getchar();
	for ( ; ch < '0' || ch > '9'; ch = getchar());
	for ( ; ch >= '0' && ch <= '9'; ch = getchar())
		x = x * 10 + ch - 48;
	return x;
}

const int N = 1e5 + 5;
int n, Q, a[N], fob[2]; char op[10];
int tot, lnk[N], nxt[N << 1], son[N << 1];
int dep[N], fa[N];
ll f[N][2];

void add (int x, int y) {
	nxt[++tot] = lnk[x], lnk[x] = tot, son[tot] = y;
}
void dfs (int x, int p) {
	dep[x] = dep[p] + 1, fa[x] = p;
	f[x][0] = 0, f[x][1] = a[x];
	for (int j = lnk[x]; j; j = nxt[j]) if (son[j] ^ p) {
		dfs(son[j], x);
		f[x][0] += f[son[j]][1];
		if (son[j] == fob[0]) f[x][1] += f[son[j]][fob[1]];
		else f[x][1] += min(f[son[j]][0], f[son[j]][1]);
	}
}

ll g[2][N][2];
void solve_A1 () {
	dfs(1, 0);
	memcpy(g[0], f, sizeof g[0]);
	dfs(n, 0);
	memcpy(g[1], f, sizeof g[1]);
	lop (qwq, 1, Q) {
		int x = read(), xr = read(), y = read(), yr = read();
		if (!xr && !yr) {puts("-1"); continue;}
		if (x > y) swap(x, y), swap(xr, yr);
		printf("%lld\n", g[1][x][xr] + g[0][y][yr]);
	}
}
int main () {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	n = read(), Q = read(), scanf("%s", op);
	lop (i, 1, n) a[i] = read();
	lop (i, 1, n - 1) {
		int x = read(), y = read();
		add(x, y), add(y, x);
	}
	if (Q > 10000 && op[0] == 'A' && op[1] == '1') return solve_A1(), 0;
	lop (qaq, 1, Q) {
		int x = read(), xr = read(), y = read(), yr = read();
		if ((x == fa[y] || y == fa[x]) && !xr && !yr) {puts("-1"); continue;}
		fob[0] = y, fob[1] = yr, dfs(x, 0);
		printf("%d\n", f[x][xr]);
	}
	return 0;
}
