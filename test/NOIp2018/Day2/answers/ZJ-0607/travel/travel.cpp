#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++)
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
#define Out() (fwrite(St,1,Top,stdout))
using namespace std;
int Top,fn,nb[100];static char buf[1000000],St[1000000],*p1=buf,*p2=buf;
const int maxn=5005;
int n,m,g[maxn][maxn],a[maxn],b[maxn],ans[maxn]={0,2},p[maxn],A,B,L,R;
bool vis[maxn],flg;
int read(){
	int ret=0;char ch=gt();
	while(ch>'9'||ch<'0') ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return ret;
}
void write(int x){fn=0;do nb[++fn]=x%10,x/=10;while(x);while(fn) pt(nb[fn--]+'0');}
void DFS(int x){
	p[++p[0]]=x;vis[x]=1;
	for(int i=1;i<=g[x][0];i++) if((!(x==A&&g[x][i]==B))&&(!(x==B&&g[x][i]==A))&&!vis[g[x][i]]) DFS(g[x][i]);
}
void dfs(int x,int fa){
	vis[x]=1;a[b[x]=++a[0]]=x;
	for(int i=1;i<=g[x][0];i++) if(!vis[g[x][i]]) dfs(g[x][i],x);
	else{if(g[x][i]^fa&&!flg) L=b[g[x][i]],R=a[0],flg=1;}
}
void work(){
	int i;
	for(i=1;i<=n;i++) if(p[i]^ans[i]){if(p[i]>ans[i]) return;break;}
	for(i;i<=n;i++) ans[i]=p[i];
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1,x,y;i<=m;i++) x=read(),y=read(),g[x][++g[x][0]]=y,g[y][++g[y][0]]=x;
	for(int i=1;i<=n;i++) sort(g[i]+1,g[i]+1+g[i][0]);
	if(m==n){
		dfs(1,0);
		for(int i=L+1;i<=R;i++) A=a[i-1],B=a[i],p[0]=0,memset(vis,0,sizeof vis),DFS(1),work();
		for(int i=1;i<=n;i++) write(ans[i]),pt(' ');
	}
	else{
		DFS(1);
		for(int i=1;i<=n;i++) write(p[i]),pt(' ');
	}
	return Out(),0;
}
