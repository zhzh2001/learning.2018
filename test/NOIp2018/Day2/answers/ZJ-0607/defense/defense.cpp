#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++)
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
#define Out() (fwrite(St,1,Top,stdout))
using namespace std;
int Top,fn,nb[100];static char buf[1000000],St[1000000],*p1=buf,*p2=buf;
const int maxn=100005,maxe=200005;
int n,m,p[maxn],son[maxe],lnk[maxn],nxt[maxe],tot,f[maxn][2],fa[maxn],INF,que[maxn],hed,tal,ans;
bool vis[maxn];
char A;int B;
int read(){
	int ret=0;char ch=gt();
	while(ch>'9'||ch<'0') ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return ret;
}
void write(int x){if(x<0) pt('-'),x=-x;fn=0;do nb[++fn]=x%10,x/=10;while(x);while(fn) pt(nb[fn--]+'0');}
void add_e(int x,int y){son[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;}
void BFS(){
	hed=0,tal=1;que[1]=1;vis[1]=1;
	while(hed^tal) for(int j=lnk[que[++hed]];j;j=nxt[j])
	 if(!vis[son[j]]) vis[son[j]]=1,fa[++tal]=hed,que[tal]=son[j];
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();A=gt();B=gt()-'0';
	for(int i=1;i<=n;i++) p[i]=read();
	for(int i=1,x,y;i<n;i++) x=read(),y=read(),add_e(x,y),add_e(y,x);
	BFS();
	for(int i=1,a,x,b,y;i<=m;i++){
		a=read();x=read();b=read();y=read();
		memset(f,63,sizeof f);INF=f[0][0];
		for(int i=fa[n]+1;i<=n;i++){if((!(que[i]==a&&x))&&(!(que[i]==y)&&x)) f[i][0]=0;if((!(que[i]==a&&!x))&&(!(que[i]==y)&&!x)) f[i][1]=p[i];}
		for(int i=n,j,u,v;i>=1;i--){
			j=fa[i];u=que[i];v=que[j];
			if(f[i][1]^INF&&(!(v==a&&x))&&(!(v==y)&&x)){if(f[j][0]==INF) f[j][0]=0;f[j][0]+=f[i][1];}
			if((!(v==a&&!x))&&(!(v==y)&&!x)){
				if(f[i][0]^INF) f[j][1]=min(f[j][1],f[i][0]+p[v]);
				if(f[i][1]^INF) f[j][1]=min(f[j][1],f[i][1]+p[v]);
			}
		}
		ans=min(f[1][0],f[1][1]);if(ans==INF) write(-1);else write(ans);pt('\n');
	}
	return Out(),0;
}
