#include<bits/stdc++.h>
#define gt() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++)
#define pt(ch) (Top<1000000?St[Top++]=ch:(Out(),St[(Top=0)++]=ch))
#define Out() (fwrite(St,1,Top,stdout))
using namespace std;
const int TT=1000000007;
typedef long long LL;
int Top,fn,nb[100];static char buf[1000000],St[1000000],*p1=buf,*p2=buf;
int n,m,A[10][1000005],ans;
LL p[1000015];
int read(){
	int ret=0;char ch=gt();
	while(ch>'9'||ch<'0') ch=gt();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=gt();
	return ret;
}
void write(int x){fn=0;do nb[++fn]=x%10,x/=10;while(x);while(fn) pt(nb[fn--]+'0');}
int ksm(int a,int b){
	LL w=a,s=1;
	while(b){
		if(b&1) (s*=w)%=TT;
		(w*=w)%=TT;
		b>>=1;
	}
	return s;
}
bool check(){
	for(int i=0,x,y,ti=(1<<n+n-2)-1;i<=ti;i++){
		x=0;y=0;if(p[i]==-1) continue;p[i]=A[1][1];bool flg=1;
		for(int j=n+n-3;j>=0;j--){
			if(i&(1<<j)){if(y==n-1){flg=0;break;}y++;}
			else{if(x==n-1){flg=0;break;}x++;}
			p[i]=p[i]*2+A[x][y];
		}
		if(!flg) p[i]=-1;
	}
	for(int i=0,ti=(1<<n+n-2)-1;i<=ti;i++) if(~p[i])
	for(int j=i+1;j<=ti;j++) if(~p[j]) if(p[j]>p[i]) return 0;
	return 1;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();m=read();if(n>m) swap(n,m);
	if(n==1) write(ksm(2,m));
	else if(n==2) write(((LL)ksm(3,m-1)<<2)%TT);
	else if(n==3&&m==3) write(112);
	else if(n==5) write(7136ll*ksm(5,m-n)%TT);
	else{
		for(LL s=0,ts=(1ll<<n*n)-1;s<=ts;s++){
			for(int i=0,cnt=0;i<n;i++) for(int j=0;j<n;j++) A[i][j]=bool(s&(1<<cnt)),cnt++;
			if(check()){ans++;if(ans==TT) ans=0;}
		}
		write((LL)ans*ksm(n,m-n)%TT);
	}
	return Out(),0;
}
