#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cassert>

using namespace std;

#define fi first
#define se second
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
#define bug(x) cerr<<#x<<" = "<<(x)<<" "
#define debug(x) cerr<<#x<<" = "<<(x)<<"\n"

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
int n,m;
struct P_0{
	int id[60][60],que[100][100],que_cnt,stk[100],stk_cnt;
	bool judge(int t){
		rep(i,1,que_cnt){
			rep(j,0,stk_cnt){
				if(((t>>que[i-1][j])&1)<((t>>que[i][j])&1))return false;
				if(((t>>que[i-1][j])&1)>((t>>que[i][j])&1))break;
			}
		}
		return true;
	}
	void dfs(int x,int y){
		if(x>=n||y>=m)return ;
		stk[stk_cnt++]=id[x][y];
		if(x==n-1&&y==m-1){
			memcpy(que[que_cnt++],stk,stk_cnt<<2);
//			rep(i,0,stk_cnt)printf("%d ",que[que_cnt-1][i]);
//			debug("FINISH");
			--stk_cnt;
			return ;
		}
		dfs(x+1,y);
		dfs(x,y+1);
		--stk_cnt;
	}
	void work(){
		int p = 0;
		rep(i,0,n)rep(j,0,m)
			id[i][j]=p++;
		dfs(0,0);
		stk_cnt=n+m-1;
		int ans=0;
		rep(t,0,1<<p)ans+=judge(t);
		cout<<ans<<endl;
	}
}P0;
const int mod=(int)1e9+7;
int Pow(int x,int y){
	int res=1;
	for(;y;y>>=1,x=(ll)x*x%mod)
		if(y&1)res=(ll)res*x%mod;
	return res;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==1||m==1)cout<<Pow(2,n+m-1)<<endl;
	else if(n<=3&&m<=3)P0.work();
	else if(n==2)cout<<(ll)Pow(3,m-1)*4%mod<<endl;
	else if(n==3)cout<<(ll)Pow(3,m-3)*112%mod<<endl;
	else if(n==4)cout<<(ll)Pow(3,m-4)*912%mod<<endl;
	else if(n==5)cout<<(ll)Pow(3,m-5)*7136%mod<<endl;
	return 0;
}
