#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cassert>

using namespace std;

#define fi first
#define se second
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
#define bug(x) cerr<<#x<<" = "<<(x)<<" "
#define debug(x) cerr<<#x<<" = "<<(x)<<"\n"

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

const int N = 5005;

int head[N],Ans[N],ans_cnt,tot_edge,n,m;
pii G[N<<1],E[N];

struct P_0{
	void dfs(int x,int par){
		Ans[++ans_cnt]=x;
		for(int i=head[x];~i;i=G[i].se)
			if(G[i].fi!=par)
				dfs(G[i].fi,x);
	}
	void work(){
		dfs(1,0);
		rep(i,1,n)printf("%d ",Ans[i]);
		printf("%d\n",Ans[n]);
	}
}P0;
struct P_1{
	int deg[N],que[N],que_cnt;
	bool mark[N],vis[N];
	int par[N],rt;
	void dfs(int x){
		for(int i=head[x];~i;i=G[i].se){
			int y=G[i].fi;
			if(y==par[x]||mark[y])continue;
			par[y]=x;
			dfs(y);
		}
	}
	int cans[N],cans_cnt;//环上的答案
	int zhilian[N],zhilian_cnt;//支链上的回溯答案 
	int flag[N],sec;
	void dfs_cans(int x,int par){
		cans[++cans_cnt]=x;
		flag[x]=true;
		for(int i=head[x];~i;i=G[i].se){
			int y=G[i].fi;
			if(y==par||flag[y])continue;
			dfs_cans(y,x);
		}
	}
	int tmp[N];
	void dfs_fir(int x){
		if(flag[x]){
			return ;
		}
		cans[++cans_cnt]=x;
		flag[x]=true;
		for(int i=head[x];i;i=G[i].se){
			int y=G[i].fi;
			if(flag[y])continue;
			if(mark[y]){//在环上 与第二个比较 
				// 剩下的支链  y/sec 
				int c=0;
				for(int j=G[i].se;~j;j=G[j].se){
					if(flag[G[j].fi]||vis[G[j].fi])continue;
					tmp[++c]=G[j].fi;//把支链头拿出来 
				}
				while(c)zhilian[++zhilian_cnt]=tmp[c],--c;
				if(zhilian_cnt==0&&sec<y){//表示走第二妙一点 
					return ;
				}else if(zhilian_cnt>0&&zhilian[zhilian_cnt]<y){
					while(zhilian_cnt){
//						bug(zhilian[zhilian_cnt]);debug(par[zhilian[zhilian_cnt]]);
						dfs_cans(zhilian[zhilian_cnt],par[zhilian[zhilian_cnt]]),--zhilian_cnt;
//						if(flag[1]==1){
//							bug(x),debug(y);
//							bug(mark[x]),debug(mark[y]);
//						}
					}
					return ;
				}else dfs_fir(y);
				break;
			}else {//在支链  必须走 要么走  要么看看要不要回去 
				dfs_cans(y,x);
			}
		}
	}
	void dfs_ANS(int x,int par){
//		bug(x),debug(par);
		if(x==rt){
			rep(i,1,cans_cnt+1)Ans[++ans_cnt]=cans[i];
			return ;
		}
//		debug("IN");
//		assert(!flag[x]);
		Ans[++ans_cnt]=x;
		for(int i=head[x];~i;i=G[i].se){
			int y=G[i].fi;
			if(y==par)continue;
//			assert(!flag[y]);
			dfs_ANS(y,x);
		}
	}
	void work(){
		{//先写出环 
			rep(i,0,m)++deg[E[i].fi],++deg[E[i].se];
			int l=1,r=0;
			rep(i,1,n+1)if(deg[i]==1){
				que[++r]=i;
			}
			while(l<=r){
				int x=que[l++];
				--deg[x];
				for(int i=head[x];~i;i=G[i].se){
					--deg[G[i].fi];
					if(deg[G[i].fi]==1)que[++r]=G[i].fi;
				}
			}
			memset(que,0,sizeof que);
			rep(t,1,n+1)if(deg[t]==2){
				int x=t;
				while(!mark[x]){
					mark[x]=true;
					que[++que_cnt]=x;
					for(int i=head[x];~i;i=G[i].se){
						int y=G[i].fi;
						if(deg[y]==2&&!mark[y]){
							x=y;
							break;
						}
					}
				}
				break;
			}
			que[0]=que[que_cnt];
			que[que_cnt+1]=que[1];
//			debug(que_cnt);
			rep(i,1,que_cnt+1)
				dfs(que[i]);
		}
//		debug("IN");
//		debug(que_cnt);
		{//把链1去掉 
			rt=1;
			while(!mark[rt])vis[rt]=1,rt=par[rt];//vis掉  表示处理环时不处理它 
			//找到链上一点 
			int id;//在第i个 
			rep(i,1,que_cnt+1)if(que[i]==rt)id=i;
			cans[++cans_cnt]=rt;
			flag[rt]=true;
			for(int i=head[rt];~i;i=G[i].se){
				int y=G[i].fi;
				if(vis[y])continue;// 1 
				if(!mark[y]){
					dfs_cans(y,rt);
				}else {
					if(flag[que[id-1]]||flag[que[id+1]]){
						if(flag[y])continue;
						//没有搞定 
						dfs_cans(y,rt);
					}else {//第一个 
						sec=que[id-1]+que[id+1]-y;//第二个是谁 
						dfs_fir(y);
					}
				}
			}
//			debug(cans_cnt);
//			rep(i,1,cans_cnt+1)bug(i),debug(cans[i]);
		}
		dfs_ANS(1,0);
		rep(i,1,n)printf("%d ",Ans[i]);
		printf("%d\n",Ans[n]);
	}
}P1;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	rep(i,0,m){
		scanf("%d%d",&E[i].fi,&E[i].se);
		if(E[i].fi>E[i].se)swap(E[i].fi,E[i].se);
	}
	sort(E,E+m);
	memset(head,-1,sizeof head);
	per(i,0,m){
		G[tot_edge]=pii(E[i].fi,head[E[i].se]),head[E[i].se]=tot_edge++;
		G[tot_edge]=pii(E[i].se,head[E[i].fi]),head[E[i].fi]=tot_edge++;
	}
	if(0);
	else if(m==n-1)P0.work();
	else if(m==n)P1.work();
	return 0;
}
