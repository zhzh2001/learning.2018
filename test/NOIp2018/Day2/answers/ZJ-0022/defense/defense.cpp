#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cassert>

using namespace std;

#define fi first
#define se second
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
#define bug(x) cerr<<#x<<" = "<<(x)<<" "
#define debug(x) cerr<<#x<<" = "<<(x)<<"\n"

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

void rd(int &x){
	static char c;
	x=0;
	while(c=getchar(),c<48);
	do x=x*10+(c&15);
	while(c=getchar(),c>47);
}
template<class T>void prin(T x){
	if(x<0)x=-x,putchar('-');
	else if(!x){putchar('0');return ;}
	static int stk[100],tp;
	while(x)stk[++tp]=x%10,x/=10;
	while(tp)putchar(stk[tp--]^48);
}
template<class T>void Min(T &a,T b){if(b<a)a=b;}
template<class T>void ptn(T a){prin(a);putchar('\n');}

const int N = (int)1e5+5,M=2005,INF=1061109567;

int head[N],tot_edge,n,m,p[N];
pii G[N<<1];
char OP[5];
struct P_0{
	int que[M],que_cnt,par[M],dp[2][M];
	void dfs(int x){
		for(int i=head[x];~i;i=G[i].se)
			if(G[i].fi!=par[x]){
				par[G[i].fi]=x;
				dfs(G[i].fi);
			}
		que[que_cnt++]=x;
	}
	void work(){
		dfs(1);
		for(int a,b,fa,fb,x;m--;){
			rd(a),rd(fa),rd(b),rd(fb);
			if(!fa&&!fb&&(par[a]==b||par[b]==a))ptn(-1);
			else {
				memset(dp,0,sizeof dp);
				rep(i,0,n){
					x=que[i];
					dp[1][x]+=p[x];
					if(a==x){
						dp[fa^1][x]=INF;
					}else if(b==x){
						dp[fb^1][x]=INF;
					}
					dp[0][par[x]]+=dp[1][x];
					dp[1][par[x]]+=min(dp[0][x],dp[1][x]);
				}
				int ans=min(dp[0][1],dp[1][1]);
				if(ans>=INF)ans=-1;
				ptn(ans);
			}
		}
	}
}P0;
//const ll inf=(ll)1e15;
//struct P_1{
//	ll tr[4][N<<2];
//	void up(int o){
//		static int i,j;
//		for(i=0;i<2;++i)
//			for(j=0;j<2;++j)
//				tr[i<<1|j][o]=min(min(tr[i<<1][o<<1]+tr[2|j][o<<1|1],
//									tr[i<<1|1][o<<1]+tr[2|j][o<<1|1]),
//									tr[i<<1|1][o<<1]+tr[j][o<<1|1]);
//	}
//	void build(int l,int r,int o){
//		if(l==r){
//			tr[1][o]=tr[2][o]=inf;
//			tr[3][o]=p[l];
//			return ;
//		}
//		int mid=(l+r)>>1;
//		build(l,mid,o<<1);
//		build(mid+1,r,o<<1|1);
//		up(o);
//	}
//	ll V;
//	void update(int l,int r,int p,int x){
//		if(l==r){
//		}
//	}
//	void work(){
//		
//	}
//}P1;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,OP);
	rep(i,1,n+1)rd(p[i]);
	memset(head,-1,sizeof head);
	for(int i=1,x,y;i<n;++i){
		rd(x),rd(y);
		G[tot_edge]=pii(y,head[x]),head[x]=tot_edge++;
		G[tot_edge]=pii(x,head[y]),head[y]=tot_edge++;
	}
	P0.work();
//	else if(OP[0]=='A')P1.work();
	return 0;
}
