#include <cstdio>
#include <algorithm>
#include <cstring>
#include <ctype.h>
using namespace std;
typedef pair<int,int> pairs;
const int maxn=5005,INF=0x7fffffff;
int n,m,ans[maxn],tmp[maxn];
pairs edge[maxn];
bool g[maxn][maxn],vis[maxn];
inline int read(){
	int ret=0; char ch=getchar(); bool fl=0;
	while (!isdigit(ch))
		fl^=!(ch^'-'),ch=getchar();
	while (isdigit(ch))
		ret=((ret+(ret<<2))<<1)+ch-'0',ch=getchar();
	return fl?-ret:ret;
}
void DFS1(int x){
	tmp[++tmp[0]]=x,vis[x]=1;
	for (int j=1;j<=n;j++)
		if (!vis[j]&&g[x][j])
			DFS1(j);
}
inline bool check(){
	for (int i=1;i<=n;i++)
		if (ans[i]!=tmp[i]){
			if (ans[i]>tmp[i])
				return 1;
			else
			 	return 0;
		}
	return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=n;i++)
		ans[i]=INF;
	for (int i=1;i<=m;i++){
		edge[i].first=read(),edge[i].second=read();
		g[edge[i].first][edge[i].second]=1;
		g[edge[i].second][edge[i].first]=1;
	}
	if (m==n-1){
		memset(vis,0,sizeof(vis));
		tmp[0]=0,DFS1(1);
		memcpy(ans,tmp,sizeof(tmp));		
	} else{
		for (int i=1;i<=m;i++){
			g[edge[i].first][edge[i].second]=0;
			g[edge[i].second][edge[i].first]=0;
			memset(vis,0,sizeof(vis));
			tmp[0]=0,DFS1(1);
			if (tmp[0]==n&&check())
				memcpy(ans,tmp,sizeof(tmp));	
			g[edge[i].first][edge[i].second]=1;
			g[edge[i].second][edge[i].first]=1;			
		}
	}
	for (int i=1;i<=n;i++)
		printf(i==n?"%d\n":"%d ",ans[i]);
	return 0;
}
