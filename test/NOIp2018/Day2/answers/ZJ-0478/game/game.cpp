#include <cstdio>
#include <algorithm>
#include <ctype.h>
#include <cstring>
using namespace std;
typedef long long LL;
const int maxn=1005,tt=1000000007;
int que[maxn][maxn],len,n,m,c[maxn],ans;
int s1[maxn],s2[maxn];
bool g[maxn][maxn];
void DFS(int step,int x,int y){
	if (x==n&&y==m){
		len++;
		memcpy(que[len],c,sizeof(c));
		return;
	}
	if (x<n){
		c[step]=0;
		DFS(step+1,x+1,y);
	}
	if (y<m){
		c[step]=1;
		DFS(step+1,x,y+1);
	}
}
void build(int *s,int *nxt){
	int x=1,y=1;
	s[1]=g[x][y];
	for (int i=1;i<=n+m-2;i++){
		if (nxt[i]==1)
			y++;
		else
			x++;
		s[i+1]=g[x][y];
	}
}
inline bool check(int *x,int *y){
	for (int i=1;i<=n+m-2;i++)
		if (x[i]!=y[i]){
			if (x[i]>y[i])
				return 1;
			else
				return 0;
		}
	return 0;
}
inline bool check(){
	for (int i=1;i<=len;i++)
		for (int j=1;j<=len;j++)
			if (i!=j){
				build(s1,que[i]),build(s2,que[j]);
				if (check(que[i],que[j])){
					if (check(s1,s2))
						return 0;
				}
			}
	return 1;
}
void DFS(int x,int y){
	if (x>n){
		if (check())
			ans++;
		return;
	}
	if (y==m){
		g[x][y]=0,DFS(x+1,1);
		g[x][y]=1,DFS(x+1,1);
	} else{
		g[x][y]=0,DFS(x,y+1);
		g[x][y]=1,DFS(x,y+1);
	}
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n<=3&&m<=3){
		len=0,DFS(1,1,1),DFS(1,1);
		printf("%d\n",ans);
	} else{
		ans=4;
		for (int i=2;i<=m;i++)
			ans=(LL)ans*3%tt;
		printf("%d\n",ans);
	}
	return 0;
}
