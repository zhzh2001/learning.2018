#include <cstdio>
#include <algorithm>
#include <cstring>
#include <ctype.h>
using namespace std;
typedef long long LL;
const int maxn=100005;
const LL INF=0x7fffffffffffffff;
int n,m,a[maxn];
int son[maxn<<1],lnk[maxn],nxt[maxn<<1],tot,ent[maxn],que[maxn],fa[maxn],head,tail;
LL f[maxn][2],ans;
char s[5];
inline int read(){
	int ret=0; char ch=getchar(); bool fl=0;
	while (!isdigit(ch))
		fl^=!(ch^'-'),ch=getchar();
	while (isdigit(ch))
		ret=((ret+(ret<<2))<<1)+ch-'0',ch=getchar();
	return fl?-ret:ret;
}
inline void add(int x,int y){
	son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot;
}
void DFS(int x,int y){
	for (int j=lnk[x];j;j=nxt[j])
		if (son[j]!=y){
			fa[son[j]]=x,ent[x]++;
			DFS(son[j],x);
		}
}
void Build_que(){
	head=tail=0;
	for (int i=1;i<=n;i++)
		if (ent[i]==0)
			que[++tail]=i;
	while (head!=tail){
		head++;
		if ((--ent[fa[que[head]]])==0)
			que[++tail]=fa[que[head]];
	}
}
void DP(){
	for (int pos=1;pos<=n;pos++){
		int x=que[pos];
		LL sum=0;
		if (f[x][0]!=INF)
			f[x][0]=0;
		for (int j=lnk[x];j;j=nxt[j])
			if (son[j]!=fa[x]){
				if (f[x][0]!=INF){
					if (f[son[j]][1]==INF)
						f[x][0]=INF;
					else
						f[x][0]+=f[son[j]][1];
				}
				if (sum!=INF){
					int tmp=min(f[son[j]][0],f[son[j]][1]);
					if (tmp==INF)
						sum=INF;
					else
						sum+=tmp;
				}
			}
		if (sum!=INF&&f[x][1]!=INF)
			f[x][1]=sum+a[x];
		else
			f[x][1]=INF;
		if (f[x][0]==f[x][1]&&f[x][0]==INF){
			f[1][0]=f[1][1]=INF;
			return;
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s+1);
	for (int i=1;i<=n;i++)
		a[i]=read();
	for (int i=1;i<n;i++){
		int x=read(),y=read();
		add(x,y),add(y,x);
	}
	DFS(1,-1);
	Build_que();
	for (int t=1;t<=m;t++){
		int x=read(),y=read(),xx=read(),yy=read();
		memset(f,127,sizeof(f));
		f[x][!y]=INF,f[xx][!yy]=INF;
		DP();
		ans=min(f[1][0],f[1][1]);
		if (ans==INF)
			printf("%d\n",-1);
		else
			printf("%lld\n",ans);
	}
	return 0;
}
