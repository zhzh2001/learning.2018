#include<vector>
#include<queue>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 5005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,head[N],tot=1,cnt,now,vis[N],b[N],q[N],top,flag[N],boo,ans[N];
struct edge{ ll fr,to,nxt; }e[N<<1];
void add(ll u,ll v) { e[++tot]=(edge){u,v,head[u]}; head[u]=tot; }
void dfs(ll u,ll last){
	vector<ll> tmp; printf("%d ",u);
	for (ll i=head[u],v=e[i].to;i;i=e[i].nxt,v=e[i].to){
		if (v==last) continue;
		tmp.push_back(v);
	}
	sort(tmp.begin(),tmp.end());
	ll l=0,r=tmp.size()-1;
	rep(i,l,r) dfs(tmp[i],u);
}void work(ll u,ll last){
	vector<ll> tmp; b[++cnt]=u;
	for (ll i=head[u],v=e[i].to;i;i=e[i].nxt,v=e[i].to){
		if (v==last||i==now||i==(now^1)) continue;
		tmp.push_back(v);
	}
	if (((int)tmp.size())==0) return;
	sort(tmp.begin(),tmp.end());
	ll l=0,r=tmp.size()-1;
	rep(i,l,r) work(tmp[i],u);
}
void find(ll u,ll last){
	q[++top]=u; flag[u]=1;
	for (ll i=head[u],v=e[i].to;i;i=e[i].nxt,v=e[i].to){
		if (v==last) continue; if (boo) return;
		if (flag[v]){
			while (q[top]!=v){
				vis[q[top]]=1;
				--top;
			}
			vis[v]=1; boo=1;
			return;
		}else find(v,u);
	}
	flag[u]=0; --top;
}
ll check(){
	rep(i,1,n)
		if (ans[i]>b[i]) return 1;
		else if (ans[i]<b[i]) return 0;
	return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(ans,7,sizeof ans);
	n=read(); m=read();
	rep(i,1,m){
		ll u=read(),v=read();
		add(u,v); add(v,u);
	}
	if (n-1==m){
		dfs(1,-1);
		return 0;
	}else{
		find(1,-1);
		rep(i,1,m){
			if (vis[e[i<<1].fr]&&vis[e[i<<1].to]){
				cnt=0;
				now=i<<1; work(1,-1);
				if (check()) rep(i,1,n) ans[i]=b[i];
			}
		}
		rep(i,1,n) printf("%d ",ans[i]);
	}
}
/*
6 6
1 3
2 3
2 5
3 4
4 5
4 6

*/
