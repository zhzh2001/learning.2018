#include<vector>
#include<queue>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 5005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,a[N][N],cnt,c[N],b[N],ans;
ll judge(ll x,ll y,ll n1,ll n2){
	if (x==n&&y==m){
		ll boo=1;
		rep(i,1,cnt) if (n1>b[i]&&n2<c[i]||n1<b[i]&&n2>c[i]) boo=0;
		if (boo) b[++cnt]=n1,c[cnt]=n2;
		return boo;
	}
	ll t1,t2;
	if (x!=n) t1=judge(x+1,y,n1*2,n2*2+a[x+1][y]);
	if (y!=m) t2=judge(x,y+1,n1*2+1,n2*2+a[x][y+1]);
	return t1&&t2;
}
void check(ll x){
//	printf("%d ",x);
	++cnt;
	rep(i,1,n*m){
		if ((1<<(i-1))&x) a[(i-1)/m][(i-1)%m+1]=1;
		else a[(i-1)/m][(i-1)%m+1]=0;
	}
	puts("");
	rep(i,1,n){
		rep(j,1,m) printf("%d ",a[i][j]); puts("");
	}
	cnt=0; if (judge(1,1,0,0)) ++ans;
}
void dfs(ll now,ll tmp){
	if (now==10){
		check(tmp);
		return;
	}
	dfs(now+1,tmp|(1<<(now-1)));
	dfs(now+1,tmp);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(); m=read();
	dfs(1,0);
	printf("%d",ans);
}
