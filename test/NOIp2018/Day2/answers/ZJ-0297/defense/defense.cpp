#include<vector>
#include<queue>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 5005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,a,b,ans,x,y,tot=1,aha[N],head[N],f[N][3],p[N],vis[N<<1],flag[N]; char ty[5];
struct edge{ ll fr,to,nxt; }e[N<<1];
void add(ll u,ll v) { e[++tot]=(edge){u,v,head[u]}; head[u]=tot; }
/*void dfs(ll u,ll last){
	f[u][1]=p[u]; ll sum=0,son=0; flag[u]=1;
	for (ll i=head[u],v=e[i].to;i;i=e[i].nxt,v=e[i].to){
		if (v==last||vis[i]||vis[i^1]) continue; dfs(v,u); ++son;
		f[u][1]+=min(min(f[v][1],f[v][0]),f[v][2]);
		sum+=min(f[v][0],f[v][1]);
	}
	for (ll i=head[u],v=e[i].to;i;i=e[i].nxt,v=e[i].to){
		if (v==last) continue;
		f[u][0]=min(f[u][0],sum-min(f[v][0],f[v][1])+f[v][1]);
	}
	if (!son) f[u][2]=0;
}*/
void check(){
	rep(u,1,n) for (ll i=head[u],v=e[i].to;i;i=e[i].nxt,v=e[i].to){
		if (aha[u]+aha[v]<=0) return;
	}
	if (aha[a]!=x||aha[b]!=y) return;
	ll tmp=0;
	rep(i,1,n) if (aha[i]) tmp+=p[i];
	ans=min(ans,tmp);
}
void work(ll now){
	if (now==n+1){
		check();
		return;
	}
	aha[now]=0; work(now+1);
	aha[now]=1; work(now+1);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(); m=read(); scanf("%s",ty);
	rep(i,1,n) p[i]=read();
	rep(i,2,n){
		ll u=read(),v=read();
		add(u,v); add(v,u);
	}
	while (m--){
		ll a=read(),x=read(),b=read(),y=read();
		ans=100000000; work(1);
		if (ans==100000000) puts("-1");
		else printf("%d\n",ans);
	}
/*	while (m--){
		ll a=read(),x=read(),b=read(),y=read();
		memset(vis,0,sizeof vis);
		memset(flag,0,sizeof flag);
		memset(f,7,sizeof f);
		ll ans=0;
		for (ll i=head[a];i;i=e[i].nxt) vis[i]=1,lin[e[i].to]=1;
		for (ll i=head[b];i;i=e[i].nxt) vis[i]=1,lin[e[i].to]=2;
		rep(i,1,n) if (!lin[i]) dfs(i,-1);
		rep(i,1,n){
			
		}
	}*/
}
/*
5 3 C3
2 4 1 3 9
1 5
5 2
5 3
3 4
1 0 3 0
2 1 3 1
1 0 5 0

*/
