#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=200005;
long long f[N][2];
int Next[N],vet[N],head[N],val[N];
int n,m,edge,Q,a1,a2,b1,b2;
char st[10];
inline long long min(long long a,long long b){return (a<b)?a:b;}
void read(int &a){
	a=0;char ch=getchar();int f=0;
	while (ch<'0'||ch>'9') f|=(ch=='-'),ch=getchar();
	while (ch>='0'&&ch<='9') a=(a<<3)+(a<<1)+ch-'0',ch=getchar();
	if (f) a=-a;
}
inline void add(int u,int v){
	Next[++edge]=head[u];
	head[u]=edge;
	vet[edge]=v;
}
void dfs(int u,int father){
	long long sss=0,sum=0;
	for (int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if (v==father) continue;
		dfs(v,u);
		sss+=min(f[v][0],f[v][1]);
		sum+=f[v][0];
	}
	f[u][1]=sum;
	f[u][0]=sss+val[u];
	if ((u==a1&&b1==1)||(u==a2&&b2==1)) f[u][1]=100000000000ll;
	if ((u==a1&&b1==0)||(u==a2&&b2==0)) f[u][0]=100000000000ll;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n);read(Q);scanf("%s",st+1);
	for (int i=1;i<=n;++i) read(val[i]);
	for (int i=1;i<n;++i){
		int u,v;
		read(u);read(v);
		add(u,v);add(v,u);
	}
	while (Q--){
		memset(f,0,sizeof(f));
		read(a1);read(b1);read(a2);read(b2);
		dfs(1,0);
		long long ans=min(f[1][1],f[1][0]);
		if (ans>=100000000000ll) puts("-1");
			else printf("%lld\n",ans);
	}
	return 0;
}
