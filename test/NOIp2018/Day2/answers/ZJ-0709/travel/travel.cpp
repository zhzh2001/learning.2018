#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;
vector<int> V[5005];
int ans[10005];
bool vis[5005];
int n,m,cnt=0;
void read(int &a){
	a=0;char ch=getchar();int f=0;
	while (ch<'0'||ch>'9') f|=(ch=='-'),ch=getchar();
	while (ch>='0'&&ch<='9') a=(a<<3)+(a<<1)+ch-'0',ch=getchar();
	if (f) a=-a;
}
void dfs(int u){
	if (!vis[u]) ans[++cnt]=u;
	vis[u]=1;
	int s=V[u].size();
	for (int i=0;i<s;++i)
	if (!vis[V[u][i]]) dfs(V[u][i]);
}
void ddd(int now){
	vis[now]=1;ans[++cnt]=now;
	int s=V[now].size();
	for (int i=0;i<s;++i){
		int v=V[now][i];
		if (!vis[v])
			ddd(v);
	}	
}
void DDD(int now,int sumtop){
	vis[now]=1;ans[++cnt]=now;
	int s=V[now].size();
	for (int i=0;i<s;++i){
		int v=V[now][i];
		if (!vis[v]){
			if (v>sumtop) return;
			DDD(v,sumtop);
		}
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);read(m);
	for (int i=1;i<=m;++i){
		int u,v;
		scanf("%d%d",&u,&v);
		V[u].push_back(v);
		V[v].push_back(u);
	}
	if (m==n-1){
		for (int i=1;i<=n;++i) sort(V[i].begin(),V[i].end());
		dfs(1);
		for (int i=1;i<n;++i) printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
		return 0;
	}
	bool flag=1;
	for (int i=1;i<=n;++i) if (V[i].size()>2) flag=0;
	if (flag){
		sort(V[1].begin(),V[1].end());
		ans[++cnt]=1;
		vis[1]=1;
		DDD(V[1][0],V[1][1]);
		ddd(V[1][1]);
		for (int i=1;i<n;++i) printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
		return 0;
	}
	for (int i=1;i<n;++i) printf("%d ",i);
	printf("%d\n",n);
	return 0;
}
