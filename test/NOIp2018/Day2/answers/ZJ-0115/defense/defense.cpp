#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline void judge(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
}
struct edge{
	int s,t,nxt;
}e[200005];
int last[100005],e_cnt;
void ins(int a,int b){
	e[e_cnt]=(edge){a,b,last[a]};
	last[a]=e_cnt++;
}
int p[5005];
int w[5005];
long long dp[5005][2];
int fa[5005];
void dfs(int x){
	dp[x][0]=0; dp[x][1]=p[x];
	for(int i=last[x];i!=-1;i=e[i].nxt)if(e[i].t!=fa[x]){
		fa[e[i].t]=x;
		dfs(e[i].t);
		dp[x][0]+=dp[e[i].t][1];
		dp[x][1]+=min(dp[e[i].t][0],dp[e[i].t][1]);
	}
	if(w[x]!=-1)dp[x][w[x]^1]=1e18;
}
int main(){
	judge();
	int n,m;
	scanf("%d%d",&n,&m);
	char c=getchar();
	while(c!='\n')c=getchar();
	for(int i=1;i<=n;i++)scanf("%d",&p[i]);
	memset(last,-1,sizeof(last));
	for(int i=1;i<n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		ins(u,v);
		ins(v,u);
	}
	for(int i=1;i<=m;i++){
		int a,b,c,d;
		scanf("%d%d%d%d",&a,&b,&c,&d);
		memset(w,-1,sizeof(w));
		w[a]=b;
		w[c]=d;
		dfs(1);
		long long res=min(dp[1][0],dp[1][1]);
		if(res<1e18)printf("%lld\n",res);
		else puts("-1");
	}
	return 0;
}
