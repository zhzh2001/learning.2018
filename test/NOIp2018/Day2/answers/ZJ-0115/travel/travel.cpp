#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
using namespace std;
inline void judge(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
}
vector<int> e[5005];
bool inst[5005],vis[5005];
int fa[5005],st[5005];
int C[5005],tot;
bool inC[5005];
void dfs(int x){
	vis[x]=1;
	inst[x]=1;
	st[++st[0]]=x;
	for(int i=0;i<e[x].size();i++)if(e[x][i]!=fa[x]){
		if(inst[e[x][i]]){
			int k;
			for(k=st[0];st[k]!=e[x][i];k--);
			for(;k<=st[0];k++)C[++tot]=st[k],inC[st[k]]=1;
		}else if(!vis[e[x][i]]){
			fa[e[x][i]]=x;
			dfs(e[x][i]);
		}
	}
	st[0]--;
	inst[x]=0;
}
vector<int> ans;
vector<int> res;
int u,v;
void dfs2(int x){
	res.push_back(x);
	vis[x]=1;
	for(int i=0;i<e[x].size();i++)
		if(!vis[e[x][i]]&&!((x==u&&e[x][i]==v)||(x==v&&e[x][i]==u)))dfs2(e[x][i]);
}
int main(){
	judge();
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		e[u].push_back(v);
		e[v].push_back(u);
	}
	for(int i=1;i<=n;i++)sort(e[i].begin(),e[i].end());
	dfs(1);
	memset(vis,0,sizeof(vis));
	if(!tot){
		dfs2(1);
		ans=res;
	}
	for(int i=1;i<=tot;i++){
		u=C[i]; v=C[i%tot+1];
		memset(vis,0,sizeof(vis));
		res.clear();
		dfs2(1);
		if(i==1)ans=res;
		else for(int i=0;i<res.size();i++){
			if(res[i]<ans[i]){ans=res; break;}
			else if(res[i]>ans[i])break;
		}
		//printf("%d\n",i);
		//for(int i=0;i<ans.size();i++)printf("%d ",ans[i]); puts("");
	}
	for(int i=0;i<ans.size();i++)printf("%d ",ans[i]); puts("");
	return 0;
}
