#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline void judge(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}
const int mod=1e9+7;
int fac2[1000005];
inline int pow(int x,int y){
	int z=1;
	while(y){
		if(y&1)z=1ll*z*x%mod;
		x=1ll*x*x%mod;
		y>>=1;
	}
	return z;
}
int main(){
	judge();
	int n,m;
	scanf("%d%d",&n,&m);
	if(n>m)swap(n,m);
	if(n==1){printf("%d\n",pow(2,m)); return 0;}
	if(n<=3){printf("%d\n",1ll*pow(3,m-n)*(n==2?12:112)%mod); return 0;}
	fac2[0]=fac2[1]=1;
	for(int i=2;i<=m;i++)fac2[i]=1ll*fac2[i-2]*i%mod;
	int ans=1ll*pow(4,n-1)*pow(3,m-n)%mod*2%mod*fac2[n-1]%mod;
	//printf("%d\n",ans);
	ans+=1ll*2*4*pow(6,n-3)%mod*pow(3,m-n+1)%mod*fac2[n-1]%mod;
	printf("%d\n",ans);
	return 0;
}
