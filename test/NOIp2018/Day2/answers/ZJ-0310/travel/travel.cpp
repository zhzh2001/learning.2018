#include<cstdio>
#include<iostream>
#include<algorithm>
#include<queue>
#include<cstring>
using namespace std;
#define maxn 5010
#define LL long long

int n,m;
vector<int> G[maxn];
int u,v;
int ans[maxn],cnt;
bool vis[maxn],in[maxn];
queue<int> huan;
int from[maxn];

struct Node{
	int x,y;
}e[maxn];
int num,x,y;
int now[maxn];

void dfs(int u){
	if(vis[u]) return;
	now[++cnt]=u;vis[u]=true;
	
	int s[u+1],t=0;
	for(unsigned int i=0;i<G[u].size();i++){
		v=G[u][i];
		if(vis[v]) continue;
		s[++t]=v;
	}
	sort(s+1,s+t+1);
	for(int i=1;i<=t;i++) dfs(s[i]);
}

void work1(){
	dfs(1);
	for(int i=1;i<n;i++) printf("%d ",now[i]);
	printf("%d\n",now[n]);
}

void dfs1(int u){
	if(vis[u]) return;
	now[++cnt]=u;vis[u]=true;
	
	int s[u+1],t=0;
	for(unsigned int i=0;i<G[u].size();i++){
		v=G[u][i];
		if(vis[v]) continue;
		if((u==x&&v==y)||(u==y&&v==x)) continue;
		s[++t]=v;
	}
	sort(s+1,s+t+1);
	for(int i=1;i<=t;i++) dfs1(s[i]);
}

void work2(){
	for(int i=1;i<=n;i++)
		if(from[i]==1) huan.push(i),in[i]=true;
	while(!huan.empty()){
		u=huan.front();huan.pop();
		for(unsigned int i=0;i<G[u].size();i++){
			v=G[u][i];
			if(in[v]) continue;
			from[v]--;
			if(from[v]==1) huan.push(v),in[v]=true;
		}
	}
	for(int u=1;u<=n;u++)if(!in[u]){
		for(unsigned int i=0;i<G[u].size();i++){
			v=G[u][i];
			if(!in[v]&&u<v){
				num++;
				e[num].x=u;e[num].y=v;
			}
		}
	}
	memset(ans,0x3f,sizeof(ans));
	for(int i=1;i<=num;i++){
		x=e[i].x;y=e[i].y;
		memset(now,0,sizeof(now));
		memset(vis,0,sizeof(vis));cnt=0;		
		dfs1(1);
		bool flag=true;
		for(int i=1;i<=n;i++)
			if(now[i]<ans[i]){
				break;
			}else if(now[i]>ans[i]){
				flag=false;
				break;
			}
		if(flag){
			for(int i=1;i<=n;i++) ans[i]=now[i];
		}
	}
	for(int i=1;i<n;i++) printf("%d ",ans[i]);
	printf("%d\n",ans[n]);
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		G[u].push_back(v);from[u]++;
		G[v].push_back(u);from[v]++;
	}
	if(m==n-1){
		work1();
	}else{
		work2();
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
