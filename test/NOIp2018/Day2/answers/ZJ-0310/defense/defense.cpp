#include<cstdio>
#include<iostream>
#include<algorithm>
#include<vector>
#include<cstring>
using namespace std;
#define maxn 100010
#define LL long long
#define inf 0x3f3f3f3f3f3f3f3f

int n,m;
char ch[5];
int p[maxn];

LL dp[maxn][3];
vector<int> G[maxn];
int f[maxn];
int u,v;
int a,x,b,y;
int deg[maxn];

void dfs(int u){
	int v;
	for(unsigned int i=0;i<G[u].size();i++){
		v=G[u][i];
		if(v==f[u]) continue;
		f[v]=u;
		dfs(v);
	}
}
inline LL min(LL x,LL y){
	return x<y?x:y;
}

void work(int u){
	int v;
	if((u==a&&x==1)||(u==b&&y==1)){
		for(unsigned int i=0;i<G[u].size();i++){
			v=G[u][i];
			if(v==f[u]) continue;
			work(v);
			dp[u][1]+=min(dp[v][1],min(dp[v][0],dp[v][2]));
		}
		dp[u][1]+=p[u];
		dp[u][0]=dp[u][2]=inf;
	}else if((u==a&&x==0)||(u==b&&y==0)){
		LL now=inf;
		for(unsigned int i=0;i<G[u].size();i++){
			v=G[u][i];
			if(v==f[u]) continue;
			work(v);
			dp[u][0]+=dp[v][2];
			now=min(now,dp[v][1]-dp[v][2]);
		}
		dp[u][2]=min(dp[u][2],dp[u][0]+now);
		dp[u][1]=inf;
	}else{
		LL now=inf;
		for(unsigned int i=0;i<G[u].size();i++){
			v=G[u][i];
			if(v==f[u]) continue;
			work(v);
			dp[u][0]+=dp[v][2];
			dp[u][1]=min(dp[u][0],min(dp[v][1],dp[v][2]));
			now=min(now,dp[v][1]-dp[v][2]);
		}
		dp[u][2]=min(dp[u][2],dp[u][0]+now);
		dp[u][1]+=p[u];
	}
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	
	scanf("%d%d %s",&n,&m,ch);
	for(int i=1;i<=n;i++) scanf("%d",p+i);
	for(int i=1;i<n;i++){
		scanf("%d%d",&u,&v);
		G[u].push_back(v);
		G[v].push_back(u);
		deg[u]++;deg[v]++;
	}
	dfs(1);
	for(int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if(x==0&&y==0){
			bool flag=false;
			for(unsigned int i=0;i<G[a].size();i++)
				if(G[u][i]==b){
					flag=true;
					break;
				}
			if(flag){
				if(deg[a]==1||deg[b]==1){
					puts("-1");
					goto out;
				}
			}
		}
		memset(dp,0,sizeof(dp));
		work(u);
		printf("%lld\n",min(dp[1][1],dp[1][2]));
		out:;
	}
	
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
