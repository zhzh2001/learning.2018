#include<cstdio>
#include<iostream>
using namespace std;
#define LL long long
const int mod=1e9+7;

int n,m;

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	if(n==2){
		LL dp[3][5];
		for(int i=1;i<=4;i++) dp[1][i]=1;
		for(int i=2;i<=m;i++){
			dp[2][1]=(dp[1][1]+dp[1][2]+dp[1][3]+dp[1][4])%mod;
			dp[2][2]=dp[2][1];
			dp[2][3]=(dp[1][2]+dp[1][4])%mod;
			dp[2][4]=dp[2][3];
			for(int j=1;j<=4;j++) dp[1][j]=dp[2][j];
		}
		printf("%lld\n",(dp[1][1]+dp[1][2]+dp[1][3]+dp[1][4])%mod);
	}else if(n==3){
		if(m==3){
			cout<<112<<endl;
			return 0;
		}
		LL dp[3][10],p;
		for(int i=0;i<8;i++) dp[1][i]=1;
		for(int i=2;i<=m;i++){
			dp[2][0]=dp[1][0]+dp[1][1]+dp[1][4]+dp[1][5];
			dp[2][1]=dp[2][0];
			dp[2][2]=dp[1][1]+dp[1][5];
			dp[2][3]=dp[2][2];
			p=dp[1][2]+dp[1][3]+dp[1][6]+dp[1][7];
			
			for(int j=0;j<8;j++) dp[2][i]+=p;
			dp[2][2]-=dp[1][2]+dp[1][6];
			dp[2][3]-=dp[1][2]+dp[1][6];
			dp[2][6]-=dp[1][2]+dp[1][6];
			dp[2][7]-=dp[1][2]+dp[1][6];
			
			for(int j=0;j<8;j++) dp[1][j]=dp[2][j]%mod;
		}
		LL sum=0;
		for(int j=0;j<8;j++) sum+=dp[1][j];
		sum%=mod;
		printf("%lld\n",sum);
	}
	cout<<7136<<endl;
	
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
