#include<bits/stdc++.h>
using namespace std;
bool SS;
const int N=5005;
vector<int>E[N];
int n,m,a,b;
struct P1 {
	void dfs(int now,int lst) {
		printf("%d ",now);
		sort(E[now].begin(),E[now].end());
		for(int i=0; i<(int)E[now].size(); i++) {
			int nxt=E[now][i];
			if(nxt==lst)continue;
			dfs(nxt,now);
		}
	}
	void solved() {
		dfs(1,-1);
	}
} p1;
struct P2 {
	int ans[N],tmp[N],cnt,CNT;
	bool MARK[N],mark[N],flag;
	void dfs(int now,int lst) {
		MARK[now]=1;
		for(int i=0; i<(int)E[now].size(); i++) {
			int nxt=E[now][i];
			if(nxt==lst||MARK[nxt])continue;
			if(mark[nxt]&&mark[now])continue;
			dfs(nxt,now);
		}
	}
	void redfs(int now,int lst) {
		tmp[++cnt]=now;
		for(int i=0; i<(int)E[now].size(); i++) {
			int nxt=E[now][i];
			if(nxt==lst)continue;
			if(mark[nxt]&&mark[now])continue;
			redfs(nxt,now);
		}
	}
	void getans() {
		if(CNT==0) {
			CNT=cnt;
			for(int i=1; i<=CNT; i++)
				ans[i]=tmp[i];
		}
		for(int i=1; i<=cnt; i++) {
			if(tmp[i]<ans[i])break;
			if(tmp[i]>ans[i])return;
		}
		for(int i=1; i<=cnt; i++)
			ans[i]=tmp[i];
	}
	void solved() {
		CNT=0;
		memset(ans,0,sizeof(ans));
		memset(mark,0,sizeof(mark));
		memset(MARK,0,sizeof(MARK));
		for(int i=1; i<=n; i++)
			sort(E[i].begin(),E[i].end());
		for(int i=1; i<=n; i++)
			for(int j=0; j<(int)E[i].size(); j++) {
				mark[i]=mark[E[i][j]]=1;
				memset(MARK,0,sizeof(MARK));
				cnt=0,dfs(1,-1),flag=1;
				for(int k=1; k<=n; k++)
					if(!MARK[k])flag=0;
				if(flag)redfs(1,-1),getans();
				mark[i]=mark[E[i][j]]=0;
			}
		for(int i=1; i<=CNT; i++)
			printf("%d ",ans[i]);
	}
} p2;
bool TT;
int main() {
//	file memory long long const mod
//	printf("%.9f\n",(&TT-&SS)/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1; i<=m; i++) {
		scanf("%d%d",&a,&b);
		E[a].push_back(b);
		E[b].push_back(a);
	}
	if(m==n-1)p1.solved();
	else p2.solved();
	return 0;
}
