#include<bits/stdc++.h>
using namespace std;
bool SS;
const int P=1e9+7;
int n,m,S[305];
struct P1 {
	int dp[1000005][10];
	int cnt,dis[10][10];
	bool check(int s1,int s2) {
		int A[10],B[10];
		memset(A,0,sizeof(A));
		memset(B,0,sizeof(B));
		for(int i=0; i<n; i++) {
			if((1<<i)&s1)A[i]=1;
			if((1<<i)&s2)B[i]=1;
		}
		for(int i=0; i<n-1; i++)
			if(A[i]>B[i+1])return false;
		return true;
	}
	void Init() {
		memset(dis,0,sizeof(dis));
		for(int i=0; i<(1<<n); i++)
			S[++cnt]=i;
		for(int i=1; i<=cnt; i++)
			for(int j=1; j<=cnt; j++)
				if(check(S[i],S[j]))dis[i][j]=1;
	}
	void solved() {
		cnt=0,Init();
		memset(dp,0,sizeof(dp));
		for(int i=1; i<=cnt; i++)
			dp[0][i]=1;
		for(int i=1; i<m; i++)
			for(int j=1; j<=cnt; j++)
				for(int k=1; k<=cnt; k++)
					if(dis[j][k])
						dp[i][j]=(dp[i][j]+dp[i-1][k])%P;
		int ans=0;
		for(int i=1; i<=cnt; i++)
			ans=(ans+dp[m-1][i])%P;
		printf("%d\n",ans);
	}
} p1;
struct P2 {
	void solved() {
		if(n==1&&m==1)puts("2");
		else if(n==1&&m==2)
			puts("4");
		else if(n==2&&m==1)
			puts("4");
		else if(n==2&&m==2)
			puts("12");
		else if(n==1&&m==3)
			puts("8");
		else if(n==3&&m==1)
			puts("8");
		else if(n==2&&m==3)
			puts("36");
		else if(n==3&&m==2)
			puts("36");
		else if(n==3&&m==3)
			puts("112");
	}
} p2;
bool TT;
int main() {
//	file memory long long const mod
//	printf("%.9f\n",(&TT-&SS)/1024.0/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==2)p1.solved();
	else if(n<=3&&m<=3)
		p2.solved();
	else p1.solved();
	return 0;
}
