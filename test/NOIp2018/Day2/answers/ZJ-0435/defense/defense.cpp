#include<bits/stdc++.h>
using namespace std;
bool SS;
const int N=100005;
int n,m,a,b,val[N];
int x,y;
vector<int>E[N];
char Key[10];
struct P1 {
	bool mark[15];
	bool check() {
		for(int i=1; i<=n; i++)
			for(int j=0; j<(int)E[i].size(); i++)
				if(mark[i]==0&&mark[E[i][j]]==0)
					return false;
		return true;
	}
	void getans(int A,int X,int B,int Y) {
		int ans=2e9;
		for(int i=1; i<(1<<n); i++) {
			int res=0;
			if(((1<<(A-1))&i)&&X==0)continue;
			if(((1<<(B-1))&i)&&Y==0)continue;
			if(((1<<(A-1))&i)==0&&X==1)continue;
			if(((1<<(B-1))&i)==0&&Y==1)continue;
			memset(mark,0,sizeof(mark));
			for(int j=1; j<=n; j++)
				if((1<<(j-1))&i)mark[j]=1,res+=val[j];
			if(check())ans=min(ans,res);
		}
		if(ans==2e9)ans=-1;
		printf("%d\n",ans);
	}
	void solved() {
		for(int i=1; i<=m; i++) {
			scanf("%d%d",&a,&x);
			scanf("%d%d",&b,&y);
			getans(a,x,b,y);
		}
	}
} p1;
struct P2 {
	long long dp[N];
	void getans(int A,int X,int B,int Y) {
		if(abs(A-B)==1&&X==0&&Y==0) {
			puts("-1");
			return;
		}
		long long ans=0;
		memset(dp,63,sizeof(dp));
		dp[1]=val[1],dp[2]=val[2];
		if(X==1)ans+=val[A];
		if(Y==1)ans+=val[B];
		if((1==A&&X==1)||(1==B&&Y==1))dp[1]=0;
		if((2==A&&X==1)||(2==B&&Y==1))dp[2]=0;
		for(int i=3; i<=n; i++) {
			if(i==A&&X!=0) {
				if((X==0&&i-1==A)||(Y==0&&i-1==B))dp[i]=dp[i];
				else dp[i]=min(dp[i],dp[i-1]);
				if((X==0&&i-2==A)||(Y==0&&i-2==B))dp[i]=dp[i];
				else dp[i]=min(dp[i],dp[i-2]);
				continue;
			} else if(i==A&&X==0) {
				if((X==0&&i-1==A)||(Y==0&&i-1==B))dp[i]=dp[i];
				else dp[i]=min(dp[i],dp[i-1]);	
			}
			if(i==B&&Y!=0) {
				if((X==0&&i-1==A)||(Y==0&&i-1==B))dp[i]=dp[i];
				else dp[i]=min(dp[i],dp[i-1]);
				if((X==0&&i-2==A)||(Y==0&&i-2==B))dp[i]=dp[i];
				else dp[i]=min(dp[i],dp[i-2]);
				continue;
			} else if(i==B&&Y==0) {
				if((X==0&&i-1==A)||(Y==0&&i-1==B))dp[i]=dp[i];
				else dp[i]=min(dp[i],dp[i-1]);	
			}
			if((X==0&&i-1==A)||(Y==0&&i-1==B))dp[i]=dp[i];
			else dp[i]=min(dp[i],dp[i-1]+val[i]);
			if((X==0&&i-2==A)||(Y==0&&i-2==B))dp[i]=dp[i];
			else dp[i]=min(dp[i],dp[i-2]+val[i]);
		}
		long long res=1e15;
		if((X==0&&n-1==A)||(Y==0&&n-1==B))
			res=res;
		else res=min(res,dp[n-1]+ans);
		if((X==0&&n==A)||(Y==0&&n==B))
			res=res;
		else res=min(res,dp[n]+ans);
		printf("%lld\n",res);
	}
	void solved() {
		for(int i=1; i<=m; i++) {
			scanf("%d%d",&a,&x);
			scanf("%d%d",&b,&y);
			getans(a,x,b,y);
		}
	}
} p2;
bool TT;
int main() {
//	file memory long long const mod
//	printf("%.9f\n",(&TT-&SS)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,Key);
	for(int i=1; i<=n; i++)
		scanf("%d",&val[i]);
	for(int i=2; i<=n; i++) {
		scanf("%d%d",&a,&b);
		E[a].push_back(b);
		E[b].push_back(a);
	}
	if(n<=10)p1.solved();
	else if(Key[0]=='A')
		p2.solved();
	else p2.solved();
	return 0;
}
