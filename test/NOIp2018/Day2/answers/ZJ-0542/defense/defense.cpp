#include<iostream>
#include<cstdio>
using namespace std;
#define M 100100
#define ll long long
ll f[M][3],dp[M][3],g[M][3];
int ff[M];
int hed[M],vet[M<<1],Next[M<<1];
int p[M];
char ch[10];
int num;
void add(int u,int v){
	++num;
	vet[num]=v;
	Next[num]=hed[u];
	hed[u]=num;
}
void dfs(int x,int fa){
	ff[x]=fa;
	f[x][1]=p[x];
	f[x][0]=0;
	for (int i=hed[x];i!=-1;i=Next[i]){
		int v=vet[i];
		if (v==fa) continue;
		dfs(v,x);
		f[x][1]+=min(f[v][0],f[v][1]);
		f[x][0]+=f[v][1];
	}
	//cout<<x<<" "<<f[x][0]<<" "<<f[x][1]<<endl;
}
void work(int x){
	int son=x;
	for (int i=ff[x];i!=-1;i=ff[i]){
		if (dp[i][1]!=1ll<<60){
		 	dp[i][1]-=min(g[son][0],g[son][1]);
			if (min(dp[son][0],dp[son][1])==1ll<<60)
				dp[i][1]=1ll<<60;
			else dp[i][1]+=min(dp[son][0],dp[son][1]);
	  }
	  if (dp[i][0]!=1ll<<60){
			dp[i][0]-=g[son][1];
	  	if (dp[son][1]==1ll<<60) dp[i][0]=1ll<<60;
			else dp[i][0]+=dp[son][1];
		}
		son=i;
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	scanf("%s",ch);
	for (int i=1;i<=n;++i) 	scanf("%d",&p[i]);
	num=0;
	for (int i=1;i<=n;++i) hed[i]=-1;
	for (int i=1;i<n;++i){
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);
		add(v,u);
	}
	dfs(1,-1);
	ll ma=1;
	for (int i=1;i<=10;++i) ma=ma*10;
	while (m--){
	  int a,x,b,y;
	  for (int i=1;i<=n;++i) dp[i][0]=g[i][0]=f[i][0],dp[i][1]=g[i][1]=f[i][1];
	  scanf("%d%d%d%d",&a,&x,&b,&y);
	  if (x==0) dp[a][1]=1ll<<60;
	  else dp[a][0]=1ll<<60;
		work(a);
	  for (int i=1;i<=n;++i) g[i][0]=dp[i][0],g[i][1]=dp[i][1];
	  if (y==0) dp[b][1]=1ll<<60;
	  else dp[b][0]=1ll<<60;
	  work(b);
	  ll ans=min(dp[1][0],dp[1][1]);
	  if (ans>ma) ans=(ll)-1;
	  printf("%lld\n",ans);
	}
	return 0;
}
