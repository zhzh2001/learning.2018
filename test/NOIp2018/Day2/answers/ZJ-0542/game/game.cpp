#include<iostream>
#include<cstdio>
using namespace std;
#define M 1000000007
#define ll long long
ll mi(int x,int y){
	if (y==0) return 1ll;
	ll num=1;
	ll t=x;
	while (y){
		if (y&1) num=num*t%M;
		t=t*t%M;
		y>>=1;
	}
	return num;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	ll ans=0;
	if (n==1) ans=(ll)mi(2,m);
	if (n==2) ans=(ll)4*mi(3,m-1);
	if (n==3&&m==3) ans=112;
	if (n==5&&m==5) ans=7136;
	printf("%lld\n",ans);
	return 0;
}
