#include<iostream>
#include<cstdio>
#include<algorithm>
#define M 5100
using namespace std;
int TT;
int AT,T,num;
bool flag[M];
int a[M],u[M],v[M],st[M],ans[M];
int hed[M],vet[M<<1],val[M<<1],Next[M<<1];
void add(int u,int v){
	++num;
	vet[num]=v;
	Next[num]=hed[u];
	hed[u]=num;
}
void dfs(int x,int fa){
	++TT;
	if (TT>=9000000) return;
	flag[x]=true;
	++AT;
	a[AT]=x;
	int ha1=T;
	for (int i=hed[x];i!=-1;i=Next[i]){
		int v=vet[i];
		if (v==fa||flag[v]==true) continue;
		++T;
		st[T]=v;
	}
	int ha2=T;
	sort(st+ha1+1,st+ha2+1);
	for (int i=ha1+1;i<=ha2;++i) dfs(st[i],x);
	T=ha1;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;++i) scanf("%d%d",&u[i],&v[i]);
	if (m==n-1){
		for (int i=1;i<=n;++i) hed[i]=-1;
		num=0;
		for (int i=1;i<=m;++i){
			add(u[i],v[i]);
			add(v[i],u[i]);
		}
		AT=0;T=0;
		dfs(1,-1);
		for (int i=1;i<AT;++i) printf("%d ",a[i]);
		printf("%d\n",a[AT]);
	}else{
		TT=0;
		for (int i=1;i<=n;++i) ans[i]=n+1;
		for (int cas=1;cas<=n/2;++cas){
			for (int i=1;i<=n;++i) hed[i]=-1,flag[i]=false;
			num=0;
			for (int i=1;i<=m;++i)
			if (i!=cas){
				add(u[i],v[i]);
				add(v[i],u[i]);
			}
			AT=0;T=0;
			dfs(1,-1);
			if (TT>=9000000) break;
			/*cout<<AT<<"::::";
			for (int i=1;i<=n;++i) cout<<a[i]<<" ";
			cout<<endl;*/
			if (AT<n) continue;
			bool flag=false;
			for (int i=1;i<=n;++i)
			if (a[i]<ans[i]){
				flag=true;
				break;
			}else if (a[i]>ans[i]) break;
	    if (flag){
	    	for (int i=1;i<=n;++i) ans[i]=a[i];
			}
		}
		for (int cas=n;cas>=n/2+1;--cas){
			for (int i=1;i<=n;++i) hed[i]=-1,flag[i]=false;
			num=0;
			for (int i=1;i<=m;++i)
			if (i!=cas){
				add(u[i],v[i]);
				add(v[i],u[i]);
			}
			AT=0;T=0;
			dfs(1,-1);
			if (TT>=9000000) break;
			/*cout<<AT<<"::::";
			for (int i=1;i<=n;++i) cout<<a[i]<<" ";
			cout<<endl;*/
			if (AT<n) continue;
			bool flag=false;
			for (int i=1;i<=n;++i)
			if (a[i]<ans[i]){
				flag=true;
				break;
			}else if (a[i]>ans[i]) break;
	    if (flag){
	    	for (int i=1;i<=n;++i) ans[i]=a[i];
			}
		}
		for (int i=1;i<n;++i) printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}
	return 0;
}
