#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0; char ch=getchar(); bool pos=1;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
const int N=5005;
bool vvv[N][N];
int n,m,ans[N],mn[N],vis[N],nedge,son[N],ed[N<<1],nextt[N<<1],tot,s[N],t[N];
vector<int> v[N];
void aedge(int a,int b){
	nextt[++nedge]=son[a]; son[a]=nedge; ed[nedge]=b;
}
void dfs(int p){ 
	if(vis[p])return; else vis[p]=1;
	ans[++tot]=p;
	for(int i=son[p];i;i=nextt[i])if(!vvv[p][ed[i]]){
		dfs(ed[i]);
	}
}
bool Mn(){
	for(int i=1;i<=n;i++)if(ans[i]!=mn[i])return ans[i]<mn[i];
	return 0;
}
int main(){
	freopen("travel.in","r",stdin); freopen("travel.out","w",stdout);
	n=m=5000;
	if(m==n-1){
		for(int i=1;i<=m;i++){
			int s=read(),t=read();
			v[s].push_back(t); v[t].push_back(s);
		}
		for(int i=1;i<=n;i++)sort(v[i].begin(),v[i].end());
		dfs(1);
		swap(mn,ans);
	}else{
		for(int i=1;i<=n;i++){
			s[i]=i<n?i+1:n; t[i]=rand()%i+1;
			v[s[i]].push_back(t[i]); v[t[i]].push_back(s[i]);
		}	
		for(int i=1;i<=n;i++)sort(v[i].begin(),v[i].end());
		for(int i=1;i<=n;i++){
			reverse(v[i].begin(),v[i].end());
			for(unsigned j=0;j<v[i].size();j++)aedge(i,v[i][j]);
		}
		int flag=0;
		for(int i=1;i<=m;i++){
			vvv[s[i]][t[i]]=vvv[t[i]][s[i]]=1; 
			memset(vis,0,sizeof(vis)); tot=0;
			dfs(1);
			vvv[s[i]][t[i]]=vvv[t[i]][s[i]]=0;
			if(tot<n)continue;
			//(int j=0;j<n;j++)cout<<ans[j]<<" "; puts("");
			if(flag==0){swap(mn,ans); flag=1;} else if(Mn())swap(mn,ans);
		}
	}
	for(int i=1;i<=n;i++){
		printf("%d ",mn[i]);
	}puts("");
}

