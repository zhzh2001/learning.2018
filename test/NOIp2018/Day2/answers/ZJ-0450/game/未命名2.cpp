#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0; char ch=getchar(); bool pos=1;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
const int N=20;
int n,m,ans;
char a[N][N];
string f[N][N],g[N][N];
bool check(){
	for(int i=n;i;i--){
		for(int j=m;j;j--){
			if(i==n)f[i][j]=g[i][j]=a[i][j]+f[i][j+1];
			else if(j==m)f[i][j]=g[i][j]=a[i][j]+f[i+1][j];
			else {
				f[i][j]=a[i][j]+max(f[i+1][j],f[i][j+1]);
				g[i][j]=a[i][j]+min(g[i+1][j],g[i][j+1]);
			}
				if(i<n&&j>1){if(f[i][j]>g[i+1][j-1])return 0;}
		}
	}
	return 1;
}
void dfs(int x,int y){
	if(y>m){
		dfs(x+1,1); return;
	}
	if(x>n){
		ans+=check(); return;
	}
	a[x][y]='1';
	dfs(x,y+1);
	a[x][y]='0';
	dfs(x,y+1);
}
int main(){
	n=3; m=5;
	dfs(1,1);
	cout<<ans<<endl;
}

