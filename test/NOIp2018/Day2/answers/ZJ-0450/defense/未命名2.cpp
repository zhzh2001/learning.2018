#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0; char ch=getchar(); bool pos=1;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
const int mod=1000000007,N=10;
int n,m,f[N][N],g[N][N],q[N];
#define add(a,b) a=((a+b)>=mod)?(a+b-mod):a+b
void bao(int l,int r){
	memset(g,0,sizeof(g)); 
	for(int i=1;i<n;i++){
		for(register int j=0;j<=n;j++)if(f[i][j]){
			int tot=0;
			if(j>1&&j<r){
				q[tot++]=j;
			}
			if(i>1){
				if(i>2&&i-1<r)q[tot++]=i-1;
				if(i<r)q[tot++]=i;
			}
			q[tot]=1; 
			if(tot==3||(tot==2&&q[0]!=q[1])){
				continue;
			}
			for(int k=l;k<=r;k++)if(i==1||k==i-1){
				add(g[q[0]][k],f[i][j]);
			}
			if(i==1)add(g[q[0]][0],f[i][j]);
		}
	}
	swap(f,g);
	//cout<<l<<" wzp "<<r<<endl;
	//for(int i=0;i<=n;i++)for(int j=0;j<=n;j++)if(f[i][j])cout<<f[i][j]<<" "<<i<<" "<<j<<endl;
}
int main(){
	//freopen("game.in","r",stdin); freopen("game.out","w",stdout);
	cin>>n>>m;
	if(n>m)swap(n,m);
	if(n==1){
		ll ans=1; for(int i=1;i<=m;i++)ans=ans*2%mod; cout<<ans<<endl; return 0;
	}
	f[1][0]=1;
	for(int i=m;i>m-n+1;i--)bao(n-(m-i),n);
	for(int i=1;i<=m-n+1;i++)bao(1,n);
	for(int i=n-1;i;i--)bao(1,i);
	int ans=0;
	for(int i=0;i<=n;i++)for(int j=0;j<=n;j++)add(ans,f[i][j]);
	cout<<ans<<endl;
}

