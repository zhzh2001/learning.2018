#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define PI pair<ll,ll>
#define mp make_pair
inline ll read(){
	ll x=0; char ch=getchar(); bool pos=1;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
const int N=100005;
const ll inf=1e17;
ll f[N],g[N];
int n,m,nedge,P[N],fa[N],son[N],ed[N<<1],nextt[N<<1];
char ch[10];
void aedge(int a,int b){
	nextt[++nedge]=son[a]; son[a]=nedge; ed[nedge]=b;
}
void dfs(int p){
	for(int i=son[p];i;i=nextt[i])if(ed[i]!=fa[p]){
		fa[ed[i]]=p;
		dfs(ed[i]);
		f[p]+=min(f[ed[i]],g[ed[i]]); g[p]+=f[ed[i]];
	}
	f[p]+=P[p];
}
void bao(int p,PI t){
	if(p>1){
		PI zs;
		zs.first=min(f[p]+t.first,g[p]+t.second)-min(f[p],g[p]);
		zs.second=f[p]+t.first-f[p];
		f[p]+=t.first; g[p]+=t.second;
		bao(fa[p],zs);
	}else{
		f[p]+=t.first; g[p]+=t.second;
	}
}
int main(){
	freopen("defense.in","r",stdin); freopen("my.out","w",stdout);
	n=read(); m=read(); scanf("%s",ch);
	for(int i=1;i<=n;i++)P[i]=read();
	for(int i=1;i<n;i++){
		int s=read(),t=read();
		aedge(s,t); aedge(t,s);
	}
	dfs(1);
	for(int i=1;i<=m;i++){
		int a=read(),x=read(),b=read(),y=read();
		PI t=mp(0,0);
		if(x)t.second=inf; else t.first=inf;
		bao(a,t); 
		t=mp(0,0);
		if(y)t.second=inf; else t.first=inf;
		bao(b,t);
		if(min(f[1],g[1])>=inf/2){
			puts("-1");
		}else {
			write(min(f[1],g[1])); puts("");	
		}
		t=mp(0,0);
		if(y)t.second=-inf; else t.first=-inf;
		bao(b,t); 
		t=mp(0,0);
		if(x)t.second=-inf; else t.first=-inf;
		bao(a,t);
	}
}

