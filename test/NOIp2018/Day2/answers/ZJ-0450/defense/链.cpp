#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define PI pair<ll,ll>
#define mp make_pair
inline ll read(){
	ll x=0; char ch=getchar(); bool pos=1;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
const int N=100005;
const ll inf=1e17;
ll f[N],g[N];
int n,m,nedge,P[N],fa[N],son[N],ed[N<<1],nextt[N<<1];
char ch[10];
void aedge(int a,int b){
	nextt[++nedge]=son[a]; son[a]=nedge; ed[nedge]=b;
}
void dfs(int p){
	for(int i=son[p];i;i=nextt[i])if(ed[i]!=fa[p]){
		fa[ed[i]]=p;
		dfs(ed[i]);
		f[p]+=min(f[ed[i]],g[ed[i]]); g[p]+=f[ed[i]];
	}
	f[p]+=P[p];
}
void bao(int p,PI t){
	if(p>1){
		PI zs;
		zs.first=min(f[p]+t.first,g[p]+t.second)-min(f[p],g[p]);
		zs.second=f[p]+t.first-f[p];
		f[p]+=t.first; g[p]+=t.second;
		bao(fa[p],zs);
	}else{
		f[p]+=t.first; g[p]+=t.second;
	}
}
ll tree[N<<2][2][2];
void push_up(int nod){
	for(int i=0;i<2;i++){
		for(int j=0;j<2;j++){
			tree[nod][i][j]=inf;
			for(int k=0;k<2;k++){
				for(int l=0;l<2;l++)if(k|l)
				tree[nod][i][j]=min(tree[nod][i][j],tree[nod<<1][i][k]+tree[nod<<1|1][l][j]);
			}
		}
	}
}
void insert(int l,int r,int i,int nod){
	if(l==r){
		tree[nod][0][0]=0; tree[nod][1][1]=P[i];  tree[nod][0][1]=tree[nod][1][0]=inf; return;
	}
	int mid=(l+r)>>1;
	if(i<=mid)insert(l,mid,i,nod<<1); else insert(mid+1,r,i,nod<<1|1);
	push_up(nod);
}
int main(){
	freopen("defense.in","r",stdin); freopen("defense.out","w",stdout);
	n=read(); m=read(); scanf("%s",ch);
	for(int i=1;i<=n;i++)P[i]=read();
	for(int i=1;i<n;i++){
		int s=read(),t=read();
		aedge(s,t); aedge(t,s);
	}
	if(ch[0]=='A'){
		for(int i=1;i<=n;i++)insert(1,n,i,1); 
		for(int i=1;i<=m;i++){
			int a=read(),x=read(),b=read(),y=read(),sa=0,sb=0;
			if(x){sa=P[a]; P[a]=0;}
			else P[a]+=inf;
			insert(1,n,a,1);
			if(y){sb=P[b]; P[b]=0;}
			else P[b]+=inf;
			insert(1,n,b,1);
			ll ans=inf;
			for(int i=0;i<2;i++)for(int j=0;j<2;j++)ans=min(ans,tree[1][i][j]);
			if(ans>=inf/2)puts("-1"); else{
				write(ans+sa+sb); puts("");
			}
			if(x)P[a]=sa; else P[a]-=inf;
			if(y)P[b]=sb; else P[b]-=inf;
			insert(1,n,a,1); insert(1,n,b,1);
		}
		return 0;
	}
	dfs(1);
	for(int i=1;i<=m;i++){
		int a=read(),x=read(),b=read(),y=read();
		PI t=mp(0,0);
		if(x)t.second=inf; else t.first=inf;
		bao(a,t); 
		t=mp(0,0);
		if(y)t.second=inf; else t.first=inf;
		bao(b,t);
		if(min(f[1],g[1])>=inf/2){
			puts("-1");
		}else {
			write(min(f[1],g[1])); puts("");	
		}
		t=mp(0,0);
		if(y)t.second=-inf; else t.first=-inf;
		bao(b,t); 
		t=mp(0,0);
		if(x)t.second=-inf; else t.first=-inf;
		bao(a,t);
	}
}
/*
5 5 A1
1 5 3 2 4
1 2
2 3
3 4
4 5

*/
