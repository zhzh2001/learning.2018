#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0; char ch=getchar(); bool pos=1;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
int n,m;
int main(){
	cin>>n>>m; int mod=1e9+7;
	ll ans=4;
	for(int i=1;i<m;i++)ans=ans*3%mod;
	cout<<ans<<endl;
}

