#include<bits/stdc++.h>
using namespace std;
const int N=2e5+5;
int Head[N],Next[N],vet[N];
int n,m,u,v,edgenum,k;
int ans[N],s[5005][5005],top[N],t[N];
bool vis[N];
struct edge{ int u,v; } E[N];
void addedge(int u, int v){
	edgenum++;
	vet[edgenum]=v;
	Next[edgenum]=Head[u];
	Head[u]=edgenum;
}
namespace S1{  //60pts
	void dfs(int u){
		vis[u]=1;
		ans[++k]=u; top[u]=0;
		for (int e=Head[u]; e; e=Next[e]){
			int v=vet[e];
			if (vis[v]) continue;
			t[++top[u]]=v;
		}
		sort(t+1,t+1+top[u]);
		for (int i=1; i<=top[u]; i++) s[u][i]=t[i];
		for (int i=1; i<=top[u]; i++) dfs(s[u][i]);
	}
	void solve(){
		dfs(1);
		for (int i=1; i<=n; i++) printf("%d ",ans[i]);
	}
}
namespace S2{  //28~40pts
	int ind[N],top[N],t[N];
	int ans[N],c[N];
	int k;
	void dfs(int u, int x, int y){
		vis[u]=1;
		c[++k]=u; top[u]=0;
		for (int e=Head[u]; e; e=Next[e]){
			int v=vet[e];
			if (vis[v] || (v==x && u==y) || (v==y && u==x)) continue;
			t[++top[u]]=v;
		}
		sort(t+1,t+1+top[u]);
		for (int i=1; i<=top[u]; i++) s[u][i]=t[i];
		for (int i=1; i<=top[u]; i++) dfs(s[u][i],x,y);
	}
	void dfs1(int u, int x, int y){
		vis[u]=1;
		for (int e=Head[u]; e; e=Next[e]){
			int v=vet[e];
			if (vis[v] || (v==x && u==y) || (v==y && u==x)) continue;
			dfs1(v,x,y);
		}
	}
	void solve1(){
		memset(ans,0x3f,sizeof(ans));
		for (int i=1; i<=m; i++){
			memset(vis,0,sizeof(vis));
			dfs1(1,E[i].u,E[i].v); bool boom=0;
			for (int j=1; j<=n; j++)
				if (!vis[j]){ boom=1; break; }
			if (boom) continue;
			memset(vis,0,sizeof(vis));
			memset(top,0,sizeof(top));
			k=0;
			dfs(1,E[i].u,E[i].v);
			for (int j=1; j<=n; j++){
				if (c[j]<ans[j]){
					for (int k=1; k<=n; k++) ans[k]=c[k];
					break;
				}
				if (c[j]>ans[j]) break;
			}
				
		}
		for (int i=1; i<=n; i++) printf("%d ",ans[i]);// puts("");
		/*
		for (int i=1; i<=n; i++){
			bool flag=0;
			for (int j=1; j<=n; j++)
				if (ans[j]==i){ flag=1; break; }
			if (!flag){
				printf("%d\n",i);
			}
		}
		*/
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=m; i++){
		scanf("%d%d",&u,&v);
		addedge(u,v); addedge(v,u);
		E[i]=(edge){u,v};
	}		
	if (m==n-1) S1::solve();
	else S2::solve1(); 
	return 0;
}
/*
6 5
1 3
2 3
3 4
2 5
4 6

6 6
1 3
3 4
3 2
4 5
2 5
4 6

8 8
1 3
1 6
3 2
2 5
5 4
4 7
7 8
8 6
*/
