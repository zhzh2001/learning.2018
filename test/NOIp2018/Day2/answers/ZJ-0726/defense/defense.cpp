#include<bits/stdc++.h>
using namespace std;
const int N=3e5+5;
int Head[N],Next[N<<1],vet[N<<1];
int p[N],dp[N][2];
int n,m,u,v,edgenum,a,x,b,y;
bool tag[N][2];
map<pair<int,int>,int> Map;
void addedge(int u, int v){
	edgenum++;
	vet[edgenum]=v;
	Next[edgenum]=Head[u];
	Head[u]=edgenum;
}
void dfs(int u, int f){
	if (!tag[u][0]) dp[u][0]=0;
	if (!tag[u][1]) dp[u][1]=p[u];
	for (int e=Head[u]; e; e=Next[e]){
		int v=vet[e];
		if (v==f) continue;
		dfs(v,u);
		if (!tag[u][0] && !tag[v][1]) dp[u][0]+=dp[v][1];
		if (!tag[u][1]){
			if (!tag[v][0] && !tag[v][1]) dp[u][1]+=min(dp[v][0],dp[v][1]);
			if (tag[v][1]) dp[u][1]+=dp[v][0];
			else if (tag[v][0]) dp[u][1]+=dp[v][1];
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	char ch=getchar();
	while (ch!='\n') ch=getchar();
	for (int i=1; i<=n; i++) scanf("%d",&p[i]);
	for (int i=1; i<n; i++){
		scanf("%d%d",&u,&v);
		addedge(u,v); addedge(v,u);
		Map[make_pair(u,v)]=Map[make_pair(v,u)]=1;
	}		
	for (int i=1; i<=m; i++){
		memset(dp,0,sizeof(dp));
		memset(tag,0,sizeof(tag));
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if (x==0 && y==0){
			if (Map[make_pair(a,b)] || Map[make_pair(b,a)]){
				puts("-1");
				continue;
			}
		}
		tag[a][1-x]=1; tag[b][1-y]=1;
		if (x==0){
			for (int e=Head[a]; e; e=Next[e]){
				int v=vet[e];
				tag[v][x]=1;
			}
		}
		if (y==0){
			for (int e=Head[b]; e; e=Next[e]){
				int v=vet[e];
				tag[v][y]=1;
			}
		}
		dfs(1,0);
		if (tag[1][0]) dp[1][0]=1e9;
		if (tag[1][1]) dp[1][1]=1e9;
		int ans=min(dp[1][0],dp[1][1]);
		if (ans==1e9) puts("-1");
		else printf("%d\n",ans);
	}
	return 0;
}
/*
5 3 C3
2 4 1 3 9
1 5
5 2
5 3
3 4
1 0 3 0
2 1 3 1
1 0 5 0

10 10 C3
57306 99217 65626 23866 84701 6623 7241 88154 33959 17847
2 1
3 1
4 1
5 2
6 3
7 5
8 7
9 6
10 7
4 1 9 1
3 1 2 1
3 1 4 1
3 0 10 0
6 0 9 0
3 1 7 1
7 0 10 1
5 1 1 1
10 1 4 0
1 0 4 0
*/
