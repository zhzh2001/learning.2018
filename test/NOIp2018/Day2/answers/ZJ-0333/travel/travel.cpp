#include<bits/stdc++.h>
using namespace std;
const int MAXN=5010;
vector<int>e[MAXN];
int f[MAXN],a[MAXN],b[MAXN];
bool vis[MAXN];
int n,m,x,y,t;
pair<int,int>circle;
bool p;
void dfs(int now,int fa){
	printf("%d ",now);
	int len=e[now].size();
	for (int i=0;i<len;i++) {
		int u=e[now][i];
		if (u==fa) continue;
		dfs(u,now);
	}
}
void dfs1(int now,int fa)
{
	int len=e[now].size();
	f[now]=fa;
	vis[now]=1;
	for (int i=0;i<len;i++) {
		int u=e[now][i];
		if (u==fa) continue;
		if (vis[u]&&!circle.first) {
			circle.first=now; circle.second=u;
		}
		else if (!vis[u]) dfs1(u,now);
	}
}
void pan1(int now,int fa,int x,int y)
{
	int len=e[now].size();
	a[++t]=now;
	for (int i=0;i<len;i++)
	{
		int u=e[now][i];
		if (u==fa) continue;
		if ((u==x&&now==y)||(u==y&&now==x)) continue;
		pan1(u,now,x,y);
	}
}
void find()
{
	if (!p) {
		for (int i=1;i<=n;i++) b[i]=a[i];
		p=1;
	}
	else {
		bool p1=0;
		for (int i=1;i<=n;i++) if (b[i]>a[i]) {
			p1=1; break;
		}
		else if (b[i]<a[i]) break;
		if (p1){
			for (int i=1;i<=n;i++) b[i]=a[i];
		}
	}
}
void solve1()
{
	dfs1(1,0);
	t=0;
	p=0;
	pan1(1,0,circle.first,circle.second);
	find();
	while (circle.first!=circle.second) {
		t=0;
		pan1(1,0,circle.first,f[circle.first]);
		find();
		circle.first=f[circle.first];
	}
	for (int i=1;i<=n;i++) printf("%d ",b[i]);
}
void solve2()
{   
    dfs(1,0);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++) {
		scanf("%d%d",&x,&y); 
		e[x].push_back(y); e[y].push_back(x);
	}
	for (int i=1;i<=n;i++) sort(e[i].begin(),e[i].end());
	if (m==n-1) 
	{
		solve2();
	}
	else solve1();
	fclose(stdin);
	fclose(stdout);
}
