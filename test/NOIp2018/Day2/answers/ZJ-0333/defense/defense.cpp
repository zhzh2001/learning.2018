#include<bits/stdc++.h>
#define ll long long 
using namespace std;
const int MAXN=1e5+10;
const ll inf=1e16;
int n,m;
char type[MAXN];
ll f[MAXN][2];
vector<int>e[MAXN];
map<int,int>mp[MAXN];
ll p[MAXN];
int x,xx,y,yy;
void find(int now,int fa)
{
	int len=e[now].size();
	f[now][0]=0; f[now][1]=0;
	for (int i=0;i<len;i++) {
		int u=e[now][i];
		if (u==fa) continue;
		find(u,now);
		f[now][1]+=min(f[u][1],f[u][0]);
		f[now][0]+=f[u][1];
		f[now][0]=min(f[now][0],inf);
	}
	f[now][1]+=p[now];
	if (now==x) {
		f[now][1-xx]=inf;
	}
	else if (now==y) f[now][1-yy]=inf;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,&type);
	for (int i=1;i<=n;i++) scanf("%lld",&p[i]);
	for (int i=1;i<n;i++) {
		scanf("%d%d",&x,&y);
		e[x].push_back(y); e[y].push_back(x);
		mp[x][y]=1; mp[y][x]=1;
	}
    for (int i=1;i<=m;i++) {
    	scanf("%d%d%d%d",&x,&xx,&y,&yy);
    	if (xx==0&&yy==0) {
    		if (mp[x][y]==1){
    			printf("%d\n",-1); continue;
			}
		}
    	find(1,0);
    	printf("%lld\n",min(f[1][1],f[1][0]));
 	}
 	fclose(stdin);
 	fclose(stdout);
}
