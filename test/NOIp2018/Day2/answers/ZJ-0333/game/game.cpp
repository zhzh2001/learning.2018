#define ll long long 
#include<bits/stdc++.h>
using namespace std;
const ll mod=1e9+7;
const int MAXM=1e6+10;
const int dx[2]={0,1};
const int dy[2]={1,0};
int f[9][MAXM];
int n,m,a[5][5];
ll ans;
bool s(int x1,int y1,int x2,int y2)
{
	if (x1>n||x2>n||y1>m||y2>m) return 0;
	if (a[x1][y1]<a[x2][y2]) return 1; else if (a[x1][y1]>a[x2][y2]) return 0;
    for (int i=0;i<=1;i++)
     for (int j=0;j<=1;j++)
      if (s(x1+dx[i],y1+dy[i],x2+dx[j],y2+dy[j])) return 1;
    return 0;  
}
bool pan(int x)
{
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++) {
			a[i][j]=(x&1); x>>=1;
		}
	}
	for (int i=1;i<=n;i++) 
	 for (int j=1;j<=m;j++)
	  if (j+1<=m&&i+1<=n) 
	   if (s(i+1,j,i,j+1)) return 0;
	return 1;   
}
void find(int x,int y)
{
	for (int i=0;i<(1<<(x*y));i++) 
	if (pan(i)) {
		ans++;
	}
	printf("%d\n",ans);
}
void fc(){
	fclose(stdin); fclose(stdout);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n<=3&&m<=3) {
		find(n,m); fc(); return 0;
	}
	ans=1;
	for (int i=1;i<=n;i++)
	{
		for (int j=1;j<=m;j++) 
		if (i-1>=1&&j+1<=m) f[i][j]=f[i-1][j+1]+1; else f[i][j]=1;
 	}
 	for (int i=1;i<=n;i++) ans=(ans*(ll)(f[i][1]+1))%mod;
 	for (int i=2;i<=m;i++) ans=(ans*(ll)(f[n][i]+1))%mod;
 	printf("%lld\n",ans);
 	fc();
}
