#include<cstdio>
#define LL long long
const int mod=1e9+7;
const int ans[5][5]=
	{{2,4,8,16,32},
	 {4,12,36,108,324},
	 {8,36,112,336,1008},
	 {16,108,336,912,2688},
	 {32,324,1008,2688,7136}};
int n,m;
LL pow(LL a,LL b)
{
	LL ret=1;
	for(;b;b>>=1)
	{
		if (b&1) ret=ret*a%mod;
		a=a*a%mod;
	}
	return ret;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n<=5&&m<=5)
	{
		printf("%d\n",ans[n-1][m-1]);
		fclose(stdin),fclose(stdout);
		return 0;
	}
	printf("%lld",pow(n==1?2:3,m-n)*(LL)ans[n-1][n-1]%mod);
	fclose(stdin),fclose(stdout);
	return 0;
}
