#include<cstdio>
#include<vector>
#include<algorithm>
#define LL long long
using namespace std;
const int N=100050,rt=1;
const LL INF=1LL<<50;
int n,m,col[N],w[N];
int fa[N],c[N];
LL dp[N][2],r[N][2];
vector<int> e[N];
int read()
{
	char ch=getchar();
	int x=0,t=1;
	while (ch!='-'&&(ch>'9'||ch<'0')) ch=getchar();
	if (ch=='-') t=-1,ch=getchar();
	while ((ch>='0'&&ch<='9')) x=x*10+ch-48,ch=getchar();
	return x*t;
}
void add(int u,int v)
{
	e[u].push_back(v);
}
void dfs(int o,int fa)
{
	if (!c[o])
	{
		dp[o][0]=r[o][0];
		dp[o][1]=r[o][1];
		return;
	}
	dp[o][0]=0,dp[o][1]=w[o];
	for(int i=0;i<(int)e[o].size();i++)
	{
		int to=e[o][i];
		if (to==fa) continue;
		dfs(to,o);
		dp[o][0]+=dp[to][1];
		dp[o][1]+=min(dp[to][1],dp[to][0]);
	}
	if (col[o]!=-1) dp[o][col[o]^1]=INF;
}
void calc(int o,int pre)
{
	r[o][0]=0,r[o][1]=w[o];
	for(int i=0;i<(int)e[o].size();i++)
	{
		int to=e[o][i];
		if (to==pre) continue;
		fa[to]=o;
		calc(to,o);
		r[o][0]+=r[to][1];
		r[o][1]+=min(r[to][1],r[to][0]);
	}
}
void up(int k,int x)
{
	for(;k!=rt;k=fa[k]) c[k]=x;
	c[rt]=x;
}
bool check(int u,int v)
{
	if (e[u].empty()) return 0;
	return *lower_bound(e[u].begin(),e[u].end(),v)==v;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),read();
	for(int i=1;i<=n;i++) col[i]=-1,w[i]=read();
	for(int i=1;i<n;i++)
	{
		int u=read(),v=read();
		add(u,v),add(v,u);
	}
	for(int i=1;i<=n;i++)
		sort(e[i].begin(),e[i].end());
	calc(rt,rt);
	while (m--)
	{
		int x=read();
		col[x]=read();
		int y=read();
		col[y]=read();
		if (!col[x]&&!col[y]&&check(x,y))
		{
			printf("%d\n",-1);
			col[x]=col[y]=-1;
			continue;
		}
		up(x,1),up(y,1);
		dfs(rt,rt);
		LL ans=min(dp[rt][0],dp[rt][1]);
		if (col[rt]!=-1) ans=dp[rt][col[rt]];
		printf("%lld\n",ans>=INF?-1:ans);
		col[x]=col[y]=-1;
		up(x,0),up(y,0);
	}
	fclose(stdin),fclose(stdout);
	return 0;
}
