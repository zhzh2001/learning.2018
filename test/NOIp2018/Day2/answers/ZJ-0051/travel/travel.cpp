#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;
const int N=5050;
int fa[N],w[N],ans[N],idx=0,n,m;
struct node{int u,v;}t[N];
struct edge{int to,id;};
vector<edge> e[N];
void add(int u,int v,int id)
{
	e[u].push_back((edge){v,id});
}
bool cmp(edge a,edge b){return a.to<b.to;}
int find(int x){return fa[x]==x?x:fa[x]=find(fa[x]);}
void unite(int u,int v){fa[find(u)]=find(v);}
void print()
{
//	for(int i=1;i<=idx;i++) printf("%d ",ans[i]);
	for(int i=1;i<=idx;i++) printf("%d%c",ans[i],i==idx?'\n':' ');
}
void dfs(int o,int fa,int id)
{
	w[++idx]=o;
	for(int i=0;i<(int)e[o].size();i++)
	{
		edge nxt=e[o][i];
		if (nxt.to==fa) continue;
		if (nxt.id==id) continue;
		dfs(nxt.to,o,id);
	}
}
void check()
{
	for(int i=1;i<=n;i++)
	{
		if (w[i]>ans[i]) return;
		if (w[i]<ans[i]) break; 
	}
	for(int i=1;i<=n;i++) ans[i]=w[i];
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v,i),add(v,u,i);
		t[i]=(node){u,v};
	}
 	for(int i=1;i<=n;i++) 
		sort(e[i].begin(),e[i].end(),cmp);
	if (m==n-1)
	{
		dfs(1,1,0);
		for(int i=1;i<=n;i++) ans[i]=w[i];
		print();
		fclose(stdin),fclose(stdout);
		return 0;
	}
	for(int i=1;i<=n;i++) ans[i]=n<<1;
	for(int k=1;k<=m;k++)
	{
		for(int i=1;i<=n;i++) fa[i]=i;
		for(int i=1;i<=m;i++)
			if (i!=k) unite(t[i].u,t[i].v);
		int pre=find(n),flag=1;
		for(int i=1;i<=n&&flag;i++)
			if (find(i)!=pre) flag=0;
		if (!flag) continue;
		idx=0,dfs(1,1,k);
		check();
	}
	print();
	fclose(stdin),fclose(stdout);
	return 0;
}
