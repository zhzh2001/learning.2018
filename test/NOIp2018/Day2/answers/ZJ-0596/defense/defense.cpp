#include<bits/stdc++.h>
using namespace std;
const int N=100010;
int n,m,type,p[N],x,y,a,b;
int len,lin[N];
struct nc{int y,nxt;}e[N<<1];
bool v[N],ok,ojbk;
long long sum,ans;
inline int read()
{
	int N=0,C=0;char tf=getchar();
	for(;!isdigit(tf);tf=getchar())C|=tf=='-';
	for(;isdigit(tf);tf=getchar())N=(N<<1)+(N<<3)+(tf^48);
	return C?-N:N;
}
inline void ins(int x,int y)
{
	e[++len].nxt=lin[x];
	lin[x]=len;
	e[len].y=y;
}
void dfs(int x,int f)
{
	for(int i=lin[x];i;i=e[i].nxt)
	{
		int y=e[i].y;
		if(!v[x]&&!v[y]){ojbk=0;return;}
		if(y!=f)dfs(y,x);
		if(!ojbk)return;
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read()+1,type=read();
	for(int i=1;i<=n;++i)p[i]=read();
	for(int i=1;i<n;++i)x=read(),y=read(),ins(x,y),ins(y,x);
	while(--m)
	{
		ans=0x7f7f7f7f;
		a=read(),x=read(),b=read(),y=read();
		for(int i=1;i<(1<<n);++i)
		{
			memset(v,0,sizeof(v));
			ok=1,sum=0;
			for(int j=0;j<20;++j)
			{
				if(a==j+1)
					if(x^((i>>j)&1)){ok=0;break;}
				if(b==j+1)
					if(y^((i>>j)&1)){ok=0;break;}
				if((i>>j)&1)v[j+1]=1,sum+=p[j+1];
			}
			if(!ok)continue;
			ojbk=1;
			dfs(1,0);
			if(ojbk)ans=min(ans,sum);
		}
		if(ans==0x7f7f7f7f)puts("-1");
		else printf("%lld\n",ans);
	}
	return 0;
}
