#include<bits/stdc++.h>
using namespace std;
const int N=5010;
int n,m,x,y,ans[N],c;
int len,lin[N];
struct nc{int y,nxt;}e[N<<1];
bool v[N],fff;
priority_queue<int,vector<int>,greater<int> >q[N];
inline int read()
{
	int N=0,C=0;char tf=getchar();
	for(;!isdigit(tf);tf=getchar())C|=tf=='-';
	for(;isdigit(tf);tf=getchar())N=(N<<1)+(N<<3)+(tf^48);
	return C?-N:N;
}
inline void ins(int x,int y)
{
	e[++len].nxt=lin[x];
	lin[x]=len;
	e[len].y=y;
}
void dfs1(int x)
{
	for(int i=lin[x];i;i=e[i].nxt)
	{
		int y=e[i].y;
		if(!v[y])q[x].push(y),v[y]=1;
	}
	while(!q[x].empty())
	{
		int xx=q[x].top();q[x].pop();
		ans[++c]=xx,dfs1(xx);
	}
}
inline void work1()
{
	for(int i=1;i<=m;++i)
	{
		x=read(),y=read();
		ins(x,y),ins(y,x);
	}
	v[1]=1,ans[c=1]=1,dfs1(1);
	for(int i=1;i<=c;++i)printf("%d ",ans[i]);
}
void dfs2(int x)
{
	v[x]=1;
	ans[++c]=x;
	for(int i=lin[x];i;i=e[i].nxt)
	{
		int yy=e[i].y;
		if(!v[yy])
		{
			if(yy<=y)dfs2(yy);
			else fff=1;
			break;
		}
	}
}
void dfs3(int x)
{
	v[x]=1;
	ans[++c]=x;
	for(int i=lin[x];i;i=e[i].nxt)
	{
		int yy=e[i].y;
		if(!v[yy])
		{
			dfs3(yy);
			break;
		}
	}
}
inline void work2()
{
	for(int i=1;i<=m;++i)
	{
		x=read(),y=read();
		ins(x,y),ins(y,x);
	}
	v[1]=1,ans[c=1]=1;
	for(int i=lin[1];i;i=e[i].nxt)y=x,x=e[i].y;
	if(x>y)swap(x,y);
	dfs2(x);
	if(fff)dfs3(y);
	for(int i=1;i<=c;++i)printf("%d ",ans[i]);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	m<n?work1():work2();
	return 0;
}
