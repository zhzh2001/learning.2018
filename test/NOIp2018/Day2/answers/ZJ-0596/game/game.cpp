#include<bits/stdc++.h>
using namespace std;
const int p=1e9+7;
const int N=10;
const int M=1000010;
int n,m,a[N][M],ans;
inline int read()
{
	int N=0,C=0;char tf=getchar();
	for(;!isdigit(tf);tf=getchar())C|=tf=='-';
	for(;isdigit(tf);tf=getchar())N=(N<<1)+(N<<3)+(tf^48);
	return C?-N:N;
}
void dfs(int x,int y)
{
	if(x==n+1&&y==1)
	{
		bool fff=1;
		for(int i=1;i<n;++i)
			for(int j=2;j<=m;++j)
				if(a[i][j]&&!a[i+1][j-1]){fff=0;break;}
		if(fff)ans=ans+1==p?0:ans+1;
		return;
	}
	int nx=x,ny=y+1;
	if(ny>m)ny=1,++nx;
	a[x][y]=0,dfs(nx,ny);
	a[x][y]=1,dfs(nx,ny);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	dfs(1,1);
	printf("%d\n",ans);
	return 0;
}
