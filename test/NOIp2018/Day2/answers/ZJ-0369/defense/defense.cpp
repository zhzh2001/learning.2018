#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m,tot,u,v,ou,ov,ff,a[110000],p[210000],ne[210000],he[110000];
long long f[110000][2];
int read(){
	int x=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9'){
		if(c=='-')f=-f;
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		x=x*10+c-48;
		c=getchar();
	}
	return x*f;
}
void adg(int u,int v){
	tot++;
	p[tot]=v;
	ne[tot]=he[u];
	he[u]=tot;
}
void dfs(int k,int fa){
	long long x=0,y=a[k];
	for(int i=he[k];i;i=ne[i])
		if(p[i]!=fa){
			dfs(p[i],k);
			if(f[p[i]][1]==-1)x=-1;
			else if(x>-1)x+=f[p[i]][1];
			if(f[p[i]][1]==-1)y+=f[p[i]][0];
			else if(f[p[i]][0]==-1)y+=f[p[i]][1];
				else y+=min(f[p[i]][0],f[p[i]][1]);
		}
	if(f[k][0]>-1)f[k][0]=x;
	if(f[k][1]>-1)f[k][1]=y;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();
	m=read();
	u=read();
//	char c=getchar();
//	c=getchar();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<n;i++){
		u=read();
		v=read();
		adg(u,v);
		adg(v,u);
	}
	for(int i=1;i<=m;i++){
		memset(f,0,sizeof(f));
		u=read();
		ou=read();
		v=read();
		ov=read();//printf("%d %d:",u,v);
		if(ou+ov==0){
			ff=0;
			for(int i=he[u];i;i=ne[i])
				if(p[i]==v){
					ff=1;
					break;
				}
			if(ff){
				puts("-1");
				continue;
			}
		}
		f[u][1-ou]=-1;
		f[v][1-ov]=-1;
		dfs(1,0);
		if(f[1][1]==-1)printf("%lld\n",f[1][0]);
		else if(f[1][0]==-1)printf("%lld\n",f[1][1]);
			else printf("%lld\n",min(f[1][1],f[1][0]));
	}
	return 0;
}
