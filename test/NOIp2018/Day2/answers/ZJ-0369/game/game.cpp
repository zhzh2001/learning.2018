#include<cstdio>
#include<algorithm>
using namespace std;
int mo=1000000007,n,m;
long long f[1100000][9][3];
int read(){
	int x=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9'){
		if(c=='-')f=-f;
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		x=x*10+c-48;
		c=getchar();
	}
	return x*f;
}
long long pwr(long long x,int y){
	long long z=1;
	while(y){
		if(y&1)z=z*x%mo;
		x=x*x%mo;
		y>>=1;
	}
	return z;
}
int iii(int x){
	if(x&1)return 1;
	else return -1;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();
	m=read();
	if(n>m)swap(n,m);
	f[1][1][0]=1;
	f[1][1][1]=1;
/*	for(int i=1;i<=m;i++)
		for(int j=1;j<=n;j++)
			if(i+j>2){
				if(i==1){f[i][j][1]=(f[i][j-1][1]+f[i][j-1][0])%mo;
					f[i][j][0]=f[i][j][1]=(f[i][j-1][1]+f[i][j-1][0])%mo;
//				printf("%d %d %lld %lld!\n",i,j,f[i][j][0],f[i][j][1]);
				}
				else if(j==n){
					f[i][j][0]=f[i][j][1]=(f[i][j-1][1]+f[i][j-1][0])%mo;
				}
				else if(j==1){
					f[i][j][0]=(f[i-1][j+1][1]+f[i-1][j+1][0])%mo*((f[i-1][n][0]+f[i-1][n][1])%mo)%mo*pwr(f[i-1][j+1][0]+f[i-1][j+1][2],mo-2)%mo;
					f[i][j][1]=f[i-1][j+1][1]*((f[i-1][n][0]+f[i-1][n][1])%mo)%mo*pwr(f[i-1][j+1][0]+f[i-1][j+1][2],mo-2)%mo;
			//		printf("%lld\n",pwr(f[i-1][j+1][0]*f[i-1][j+1][2],mo-2));
				}
				else{
					f[i][j][0]=(f[i-1][j+1][1]+f[i-1][j+1][0])%mo*((f[i][j-1][0]+f[i][j-1][1])%mo)%mo*pwr(f[i-1][j+1][0]+f[i-1][j+1][2],mo-2)%mo;
					f[i][j][1]=f[i-1][j+1][1]*((f[i][j-1][0]+f[i][j-1][1])%mo)%mo*pwr(f[i-1][j+1][0]+f[i-1][j+1][2],mo-2)%mo;
				}
				printf("%d %d %lld %lld\n",i,j,f[i][j][0],f[i][j][1]);
			}*/
/*
	long long ni=pwr(2,mo-2);
	for(int i=2;i<=n;i++)
		f[1][i][0]=f[1][i][1]=(f[1][i-1][0]+f[1][i-1][1])%mo;
	for(int i=2;i<=m;i++)
		f[i][1][0]=f[i][1][1]=(f[i-1][1][0]+f[i-1][1][1])%mo;
	for(int i=2;i<=m;i++)
		for(int j=2;j<=n;j++)
			f[i][j][0]=f[i][j][1]=(f[i-1][j][0]*f[i][j-1][0]%mo+f[i-1][j][1]*f[i][j-1][0]%mo+f[i-1][j][1]*f[i][j-1][1]%mo)%mo*ni%mo;
*/

	if(n==1){
		printf("%lld",pwr(2,m));
		return 0;
	}
	if(n==2){
		f[1][1][0]=1;
		f[1][1][1]=1;
		f[1][2][0]=2;
		f[1][2][1]=2;
		for(int i=2;i<=m;i++)
			for(int j=1;j<=n;j++)
				if(j==1){
					f[i][j][0]=(f[i-1][j+1][0]+f[i-1][j+1][1])%mo;
					f[i][j][1]=f[i-1][j+1][1];
				}
				else{
					f[i][j][0]=f[i][j][1]=(f[i][j-1][0]+f[i][j-1][1])%mo;
				}
		printf("%lld",(f[m][n][0]+f[m][n][1])%mo);
		return 0;
	}
	for(int i=2;i<=n;i++)
		f[1][i][0]=f[1][i][1]=(f[1][i-1][0]+f[1][i-1][1])%mo;
	for(int i=2;i<=m;i++)
		f[i][1][0]=f[i][1][1]=(f[i-1][1][0]+f[i-1][1][1])%mo;
	for(int i=2;i<=m;i++)
		for(int j=2;j<=n;j++){
			f[i][j][0]=f[i][j][1]=(f[i-1][n][0]+f[i-1][n][1])%mo*pwr(2,j-2)%mo;
			for(int k=1;k<=j;k++){
				f[i][j][1]-=f[i-1][k+1][0]*f[i][k][1]*iii(j-k+1)%mo;
				if(f[i][j][1]<0)f[i][j][1]+=mo;
				if(f[i][j][1]>mo)f[i][j][1]-=mo;
				if(k<j)f[i][j][0]-=f[i-1][k+1][0]*f[i][k][1]*iii(j-k)%mo;
				if(f[i][j][0]<0)f[i][j][0]+=mo;
				if(f[i][j][0]>mo)f[i][j][0]-=mo;
			}
		}
	printf("%lld",(f[m][n][0]+f[m][n][1])%mo);
	return 0;
}
