const modn=1000000007;

var i,j,n,m:longint;

function case1:longint;
  var sum:int64;
  begin
    sum:=1;
    for i:=1 to m do sum:=sum*2 mod modn;
    case1:=sum;
  end;

function case2:longint;
  var i:longint;
      sum:int64;
      a:array[0..4] of int64;
  begin
    a[1]:=1;
    a[2]:=1;
    a[3]:=1;
    a[4]:=1;
    sum:=0;
    for i:=2 to m do
      begin
        a[1]:=a[1]*2 mod modn;
        a[2]:=a[2]*4 mod modn;
        a[3]:=a[3]*2 mod modn;
        a[4]:=a[4]*4 mod modn;
      end;
    for i:=1 to 4 do sum:=(sum+a[i]) mod modn;
    case2:=sum;
    for i:=1 to 4 do writeln(a[i])
  end;

function case3:longint;
  var i,j:longint;
      sum:int64;
  begin
    if m=3 then exit(112);
    if m=1 then exit(8);
  end;

function case4:longint;
  var sum:int64;
  begin
    sum:=1;
    for i:=1 to n do sum:=sum*2 mod modn;
    case4:=sum;
  end;

begin
  assign(input,'game.in');reset(input);
  assign(output,'game.out');rewrite(output);
  readln(n,m);
  if n=1 then write(case1);
  if n=2 then write(case2);
  if n=3 then write(case3);
  if (n>3) and (m=1) then write(case4);
  close(input);close(output);
end.
