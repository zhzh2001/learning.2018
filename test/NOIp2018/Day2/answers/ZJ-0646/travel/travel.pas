var i,j,len,n,m,u,v:longint;
    heap,de:array[0..5050] of longint;
    gt,head,next:array[0..10000] of longint;
    hash:array[0..5000] of boolean;
    f:array[0..5000] of boolean;

procedure add(x:longint);
  var son,temp:longint;
  begin
    inc(len);heap[len]:=x;
    son:=len;
    while (son>1) and (heap[son]<heap[son div 2]) do
      begin
        temp:=heap[son];
        heap[son]:=heap[son div 2];
        heap[son div 2]:=temp;
        son:=son div 2;
      end;
  end;

function delete:longint;
  var fa,temp,son:longint;
  begin
    delete:=heap[1];
    heap[1]:=heap[len];
    dec(len);
    fa:=1;
    while ((fa*2<=len) or (fa*2+1<=len)) and ((heap[fa]>heap[fa*2]) or (heap[fa]>heap[fa*2+1])) do
      begin
        if (fa*2+1<=len) and (heap[fa]>heap[fa*2+1]) then son:=fa*2+1 else son:=fa*2;
        temp:=heap[fa];heap[fa]:=heap[son];heap[son]:=temp;
        fa:=son;
      end;
  end;

procedure tp;
  var i,j,v:longint;
  begin
    v:=heap[1];f[v]:=true;
    write(delete,' ');
    i:=head[v];
    while i<>0 do
      begin
        if f[gt[i]]=false then
          begin
            add(gt[i]);
            f[gt[i]]:=true;
          end;
        i:=next[i];
      end;
    if len>0 then tp;
  end;

begin
  assign(input,'travel.in');reset(input);
  assign(output,'travel.out');rewrite(output);
  readln(n,m);
  fillchar(hash,sizeof(hash),false);
  fillchar(f,sizeof(f),false);
  for i:=1 to m do
    begin
      readln(u,v);
      inc(de[u]);inc(de[v]);
      hash[u]:=true;
      hash[v]:=true;
      gt[i*2]:=v;
      next[i*2]:=head[u];
      head[u]:=i*2;
      gt[i*2-1]:=u;
      next[i*2-1]:=head[v];
      head[v]:=i*2-1;
    end;
  for i:=1 to 5000 do
    if hash[i]=true then
      begin
        add(i);
        tp;
        break;
      end;
  close(input);close(output);
end.
