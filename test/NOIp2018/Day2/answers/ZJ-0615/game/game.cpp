#include <cstdio>
#include <algorithm>

using namespace std;

const int p=1000000007;

int m,n;

inline int power(int x,int y)
{
	int s=1;
	for (int i=30;i>=0;i--)
	{
		s=1LL*s*s%p;
		if (y&(1<<i))
			s=1LL*s*x%p;
	}
	return s;
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m)
		swap(n,m);
	if (n==1)
		printf("%d",power(2,m));
	if (n==2)
		printf("%d",(int) (power(3,m-1)*4LL%p));
	if (n==3)
		printf("112");
	if (n==5)
		printf("7136");
	return 0;
}
