#include <cstdio>

inline int comb(int x1,int x2,int x3,int x4,int x5)
{
	return x1*16+x2*8+x3*4+x4*2+x5;
}

int main()
{
	int b[10];
	int t=0;
	for (int i=0;i<512;i++)
	{
		for (int j=1;j<=9;j++) if (i&(1<<(j-1))) b[j]=1; else b[j]=0;
		if (! (((comb(b[1],b[2],b[3],b[6],b[9])<=comb(b[1],b[2],b[5],b[6],b[9])) &&
			(comb(b[1],b[2],b[5],b[6],b[9])<=comb(b[1],b[2],b[5],b[8],b[9])) &&
			(comb(b[1],b[2],b[5],b[8],b[9])<=comb(b[1],b[4],b[5],b[6],b[9])) &&
			(comb(b[1],b[4],b[5],b[6],b[9])<=comb(b[1],b[4],b[5],b[8],b[9])) &&
			(comb(b[1],b[4],b[5],b[8],b[9])<=comb(b[1],b[4],b[7],b[8],b[9])))) &&
			(b[2]<=b[4]) && (b[3]<=b[5]) && (b[5]<=b[7]) && (b[6]<=b[8]))
			{ t++; printf("%d\n",t);
			for (int j=1;j<=3;j++) { for (int k=1;k<=3;k++)
				printf("%d ",b[(j-1)*3+k]); puts(""); } }
	}
	return 0;
}
