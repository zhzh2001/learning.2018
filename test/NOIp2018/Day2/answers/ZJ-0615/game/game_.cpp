#include <cstdio>
#include <algorithm>

using namespace std;

const int p=1000000007;

int f[10][300][10];
int i,j,k,l,m,n,o,s,x,y,z;

int main()
{
	scanf("%d%d",&n,&m);
	for (j=0;j<(1<<n);j++)
		f[1][j][n]=1;
	for (i=1;i<m;i++)
		for (j=0;j<(1<<n);j++)
			for (k=1;k<=n;k++)
				if (f[i][j][k])
					for (l=0;l<(1<<k);l++)
					{
						x=((l&(1<<(k-1)))<<(n-k))|(l&((1<<(k-1))-1))|((j>>k)<<(k-1)),y=k,z=1;
						for (o=1;o<n;o++)
						{
							if ((x&(1<<(o-1))) && (! (j&(1<<o))))
								z=0;
							if ((! (x&(1<<(o-1)))) && (j&(1<<o)))
								if ((o==1) || (i==1))
									y=min(y,o+1);
								else
									z=0;
						}
						printf("%d %d %d %d %d %d %d\n",i,j,k,f[i][j][k],x,y,z);
						if (z)
							f[i+1][x][y]=(f[i+1][x][y]+f[i][j][k])%p;
					}
	for (j=0;j<(1<<n);j++)
		for (k=1;k<=n;k++)
			if (f[m][j][k])
				s=(s+f[m][j][k])%p;
	printf("%d",s);
	return 0;
}
