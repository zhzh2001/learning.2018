#include <cstdio>
#include <algorithm>

using namespace std;

const int inf=1000000000;

int edge[300000],next[300000],first[300000];
int f[2][300000],g[300000];
int i,m,n,x,x1,x2,y,y1,y2,sum_edge;

inline void addedge(int x,int y)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],first[x]=sum_edge;
	return;
}

inline void dfs(int x,int y)
{
	for (int i=first[x];i!=0;i=next[i])
		if (edge[i]!=y)
			dfs(edge[i],x);
	f[0][x]=0,f[1][x]=g[x];
	for (int i=first[x];i!=0;i=next[i])
		if (edge[i]!=y)
		{
			f[0][x]=f[0][x]+f[1][edge[i]];
			f[1][x]=f[1][x]+min(f[0][edge[i]],f[1][edge[i]]);
		}
	if (x==x1)
		f[y1^1][x]=inf;
	if (x==x2)
		f[y2^1][x]=inf;
	return;
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	getchar(),getchar(),getchar();
	for (i=1;i<=n;i++)
		scanf("%d",&g[i]);
	for (i=1;i<n;i++)
	{
		scanf("%d%d",&x,&y);
		addedge(x,y),addedge(y,x);
	}
	for (i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&x1,&y1,&x2,&y2);
		dfs(1,0);
		if (min(f[0][1],f[1][1])>=inf)
			printf("-1\n");
		else
			printf("%d\n",min(f[0][1],f[1][1]));
	}
	return 0;
}
