#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

int edge[30000],next[30000],first[30000];
int b[30000],c[30000],d[30000],e[30000],q[30000];
int i,k,m,n,o,r,t,x,y,sum_edge;

inline void addedge(int x,int y)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],first[x]=sum_edge;
	return;
}

inline void dfs1(int x,int y)
{
	vector <int> V;
	vector <int> :: iterator it;
	V.clear();
	k++,d[k]=x;
	for (int i=first[x];i!=0;i=next[i])
		if (edge[i]!=y)
			V.push_back(edge[i]);
	sort(V.begin(),V.end());
	for (it=V.begin();it!=V.end();it++)
		dfs1(*it,x);
	return;
}

inline void subtask1()
{
	for (i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y);
		addedge(x,y),addedge(y,x);
	}
	dfs1(1,0);
	for (i=1;i<n;i++)
		printf("%d ",d[i]);
	printf("%d\n",d[n]);
	return;
}

inline void dfs3(int x,int y)
{
	o++,q[o]=x,b[x]=1;
	for (int i=first[x];i!=0;i=next[i])
		if (edge[i]!=y)
			if (! b[edge[i]])
				dfs3(edge[i],x);
			else
				for (int j=o;q[j]!=edge[i];j--)
					c[q[j]]=1;
	o--,b[x]=0;
	return;
}

inline void dfs2(int x,int y,int z)
{
	vector <int> V;
	vector <int> :: iterator it,it_;
	V.clear();
	k++,d[k]=x,e[x]=1;
	if (c[x])
		t++;
	for (int i=first[x];i!=0;i=next[i])
		if ((edge[i]!=y) && (! e[edge[i]]))
			V.push_back(edge[i]);
	sort(V.begin(),V.end());
	for (it=V.begin();it!=V.end();it++)
	{
		if (e[*it])
			continue;
		it_=it,it_++;
		if (it_!=V.end())
			dfs2(*it,x,*it_);
		else
			if ((c[*it]) && ((*it)>z) && (! r))
				r=1;
			else
				dfs2(*it,x,z);
	}
	return;
}

inline void subtask2()
{
	for (i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y);
		addedge(x,y),addedge(y,x);
	}
	dfs3(1,0);
	dfs2(1,0,n+1);
	for (i=1;i<n;i++)
		printf("%d ",d[i]);
	printf("%d\n",d[n]);
	return;
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m==n-1)
		subtask1();
	else
		subtask2();
	return 0;
}
