#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define C getchar()-48
inline ll read()
{
	ll s=0,r=1;
	char c=C;
	for(;c<0||c>9;c=C) if(c==-3) r=-1;
	for(;c>=0&&c<=9;c=C) s=(s<<3)+(s<<1)+c;
	return s*r;
}
const int N=100010;
int n,m,p;
int a[N];
char s[20];
int link[N],e[N<<1],nxt[N<<1],top;
inline void into(int xx,int yy)
{
	e[++top]=yy,nxt[top]=link[xx],link[xx]=top;
}
ll ans=0;
inline void dfs(int x,int fa,int zt)
{
	if(zt) ans+=a[x];
	for(int i=link[x];i;i=nxt[i])
	{
		int u=e[i];
		if(u==fa) continue;
		dfs(u,x,zt^1);
	}
}
void work1()
{
	dfs(1,0,1);
	for(int i=1;i<=m;i++) printf("%lld\n",ans);
	exit(0);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();scanf("%s",s+1);
	for(int i=1;i<=n;i++) a[i]=read();
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read();
		if(x>y) swap(x,y);
		if(x+1==y) p++;
		into(x,y);into(y,x);
	}
	if(s[2]=='1') work1();
	return 0;
}
