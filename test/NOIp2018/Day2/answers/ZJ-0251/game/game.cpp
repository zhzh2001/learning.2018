#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define C getchar()-48
inline ll read()
{
	ll s=0,r=1;
	char c=C;
	for(;c<0||c>9;c=C) if(c==-3) r=-1;
	for(;c>=0&&c<=9;c=C) s=(s<<3)+(s<<1)+c;
	return s*r;
}
const ll p=1e9+7;
ll n,m;
inline ll ksm(ll a,ll b)
{
	ll ans=1;
	while(b)
	{
		if(b&1) ans=(ans*a)%p;
		a=(a*a)%p;
		b>>=1;
	}
	return ans;
}
void work1()
{
	if(n==1) cout<<ksm(2,m);
	else     cout<<ksm(2,n);
	exit(0);
}
void work2()
{
	if(n==2) cout<<1ll*4*ksm(3,m-1);
	else     cout<<1ll*4*ksm(3,n-1);
	exit(0);
}
void work3()
{
	cout<<112;
	exit(0);
}
void worked()
{
	exit(0);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read(); 
	if(n==1||m==1) work1();
	else if(n==2||m==2) work2();
	else if(n==3&&m==3) work3();
	else worked();
	return 0;
}
