#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define C getchar()-48
inline ll read()
{
	ll s=0,r=1;
	char c=C;
	for(;c<0||c>9;c=C) if(c==-3) r=-1;
	for(;c>=0&&c<=9;c=C) s=(s<<3)+(s<<1)+c;
	return s*r;
}
const int N=5010;
int n,m;
int vis[N];
int ans[N],topd,w[N];
int j[N],topj;
int a[N][N];
int c[N];
int flag[N];
int p=0;
inline void dfs(int x,int fa)
{    
	if(!vis[x])vis[x]=1,ans[++topd]=x,w[x]=topd,c[x]++;
	if(p==0)
	{	
	    int id=0;
		for(int i=1;i<=n;i++)
		if(a[x][i]&&i!=fa&&vis[i])
		{id=i;break;}
		if(id)
		{   
			int st=w[id]+1;topd--;
			
			while(st<=topd&&ans[st]<x) st++;	
			for(int o=st;o<=topd;o++) vis[ans[o]]=0,flag[ans[o]]=1;
			vis[x]=0;topd=st-1;
		    p=1;
			return;
		}
	}
	for(int i=1;i<=n;i++)
	if(a[x][i])
	{
		if(flag[x]&&c[x]<2) {flag[x]=0;return;}
		int u=i;
		if(vis[i]) continue;
		dfs(u,x);
	}
	if(flag[x]) {flag[x]=0;return;}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++)
	{
		int x=read(),y=read();
		a[x][y]=a[y][x]=1;
	}
	if(m==n-1) p=1;
	dfs(1,0);
	for(int i=1;i<=n;i++) printf("%d ",ans[i]);
	return 0;
}
