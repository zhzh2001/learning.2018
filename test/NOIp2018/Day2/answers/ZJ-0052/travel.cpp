#include <bits/stdc++.h>
using namespace std;
const int N=5005;
int mp[N][N];
int vis[N],ans[N],out[N],n,m,tot,ls;
int dfn[N],low[N],bl[N],size[N],cnt,ti;
int sta[N<<1],top,insta[N],numa,numb,flag=1,fff=1;
void dfs(int x)
{
	vis[x]=1;
//	if(x==5) cout<<1;
	for(int i=1;i<=n;i++)
	{
		if(mp[x][i]&&!vis[i])
		{
			ans[++tot]=i;
			dfs(i);
		}
	}
}
void tarjan(int x,int fa)
{
	dfn[x]=low[x]=++ti;
	sta[++top]=x;insta[x]=1;
	for(int i=1;i<=n;i++)
	{
		if(i==fa) continue;
		if(mp[x][i])
		{
			if(!dfn[i])
			{
				tarjan(i,x);
				low[x]=min(low[x],low[i]);
			}
			else if(insta[i])
				low[x]=min(low[x],dfn[i]);
		}
	}
	if(dfn[x]==low[x])
	{
		int now;
		++cnt;
		while(now=sta[top--])
		{
			insta[now]=0;
			bl[now]=cnt;
			size[cnt]++;
			if(now==x) break;
		}
	}
}
void dfs2(int x)
{
	vis[x]=1;
	if(flag&&size[bl[x]]>1)
	{
		for(int i=1;i<=n;i++)
		{
			if(mp[x][i]&&bl[x]==bl[i])
			{
				if(!numa) numa=i;
				else numb=i;
				if(numa&&numb) break;
			}
		}
		flag=0;
	}
	for(int i=1;i<=n;i++)
	{
		if(mp[x][i]&&!vis[i])
		{
			if(bl[x]==bl[i])
			{
				if(i>numb)
				{
					if(out[x]-2>0)
					{
						if(x==numb) ans[++tot]=i,fff=0;
						continue ;
					}
					else
					{
						
						return ;
					}
						
				}
				else
				{
					ans[++tot]=i;
					dfs2(i);
				}
			}
			else
			{
				ans[++tot]=i;
				dfs2(i);
			}
		}
	}
}
int x,y;
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y);
		mp[x][y]=mp[y][x]=1;
		out[x]++;out[y]++;
	}
	if(m==n-1)
	{
		ans[++tot]=1;
		dfs(1);
		for(int i=1;i<=tot;i++)
			printf("%d ",ans[i]);
	}
	else
	{
		for(int i=1;i<=n;i++)
			if(!dfn[i]) tarjan(i,0);
//		for(int i=1;i<=n;i++)
//			printf("%d ",bl[i]);
//		puts("");
		ans[++tot]=1;
		dfs2(1);
//		printf("$%d\n",tot);
//		printf("%d %d\n",numa,numb);
//		if(ans[tot]!=numb) ans[++tot]=numb;  
//		vis[numb]=0;
//		for(int i=1;i<=tot;i++)
//			printf("%d ",ans[i]);
//		puts("");
//	for(int i=1;i<=n;i++)
//		printf("%d ",vis[i]);
//	puts("");
	
		if(fff) dfs(numb);
		for(int i=1;i<=tot;i++)
			printf("%d ",ans[i]);
	}
	return 0;
}
