#include <bits/stdc++.h>


#define OPEN(file) \
	freopen(#file".in", "r", stdin), \
	freopen(#file".out", "w", stdout)


int read(void)
{
	int x = 0, c = getchar();
	for (; !isdigit(c); c = getchar());
	for (;  isdigit(c); c = getchar()) {
		x = (x << 3) + (x << 1) + c - 48;
	}
	return x;
}


std::vector<int> edge[10086];
int n, m, cnt, ans[10086], cnt2, ans2[10086], u[10086], v[10086], father[10086];
bool vis[10086], rng[10086];


void dfs(int x, int f)
{
	ans[++cnt] = x;
	for (int i = 0, _i = edge[x].size(); i < _i; ++i) {
		int y = edge[x][i];
		if (y == f) {
			continue;
		}
		dfs(y, x);
	}
	return;
}


void dfsRing(int x, int f)
{
	if (father[x]) {
		while (x != 0) {
			rng[x] = true, x = father[x];
		}
		while (f != 0 && !rng[f]) {
			rng[f] = true, f = father[f];
		}
		f = father[f];
		while (f != 0) {
			rng[f] = false, f = father[f];
		}
		return;
	}
	father[x] = f;
	for (int i = 0, _i = edge[x].size(); i < _i; ++i) {
		int y = edge[x][i];
		if (y == f) {
			continue;
		}
		dfsRing(y, x);
	}
	return;
}


void dfs2(int x, int f, int v, int u)
{
	ans2[++cnt2] = x;
	for (int i = 0, _i = edge[x].size(); i < _i; ++i) {
		int y = edge[x][i];
		if (y == f || (x == u && y == v) || (x == v && y == u)) {
			continue;
		}
		dfs2(y, x, v, u);
	}
	return;
}


int main(void)
{
	OPEN(travel);
	n = read(), m = read();
	for (int i = 1; i <= m; ++i) {
		u[i] = read(), v[i] = read();
		edge[u[i]].push_back(v[i]);
		edge[v[i]].push_back(u[i]);
	}
	for (int i = 1; i <= n; ++i) {
		std::sort(edge[i].begin(), edge[i].end());
	}
	if (m == n - 1) {
		dfs(1, -1);
	} else {
		memset(ans, 0x3f, sizeof ans);
		dfsRing(1, 0);
		for (int i = 1; i <= m; ++i) {
			if (rng[u[i]] && rng[v[i]]) {
				cnt2 = 0;
				dfs2(1, -1, u[i], v[i]);
				if (cnt2 == n) {
					for (int j = 1; j <= n; ++j) {
						if (ans2[j] > ans[j]) {
							break;
						} else if (ans2[j] < ans[j]) {
							memcpy(ans, ans2, sizeof ans);
						}
					}
				}
			}
		}
	}
	for (int i = 1; i <= n; ++i) {
		printf("%d ", ans[i]);
	}
	return 0;
}
