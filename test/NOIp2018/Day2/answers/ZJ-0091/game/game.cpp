#include <bits/stdc++.h>


#define OPEN(file) \
	freopen(#file".in", "r", stdin), \
	freopen(#file".out", "w", stdout)


const int MOD = 1e9 + 7;


long long ans = 1;
int n, m;


int main(void)
{
	OPEN(game);
	scanf("%d%d", &n, &m);
	if (n > m) {
		n ^= m ^= n ^= m;
	}
	if (n < 3) {
		for (int i = 1; i <= n; ++i) {
			ans = ans * (i + 1) % MOD;
		}
		for (int i = n + 1; i <= m; ++i) {
			ans = ans * (n + 1) % MOD;
		}
		for (int i = 1; i < n; ++i) {
			ans = ans * (i + 1) % MOD;
		}
	} else if (n == 3 && m == 3) {
		ans = 112;
	} else {
		srand(time(NULL));
		ans = ((rand() << 16) + rand()) % MOD;
	}
	printf("%lld\n", ans);
	return 0;
}
