#include <bits/stdc++.h>


#define OPEN(file) \
	freopen(#file".in", "r", stdin), \
	freopen(#file".out", "w", stdout)
	
	
int n, m, cost[100086], u[100086], v[100086], st[100086];
	

namespace P16
{
	int solve(int a, int x, int b, int y)
	{
		int ans = 0x3f3f3f3f;
		for (int q = 0; q < (1 << n); ++q) {
			for (int i = 0; i < n; ++i) {
				st[i + 1] = (q & (1 << i)) > 0;
			}
			if (st[a] != x || st[b] != y) {
				continue;
			}
			bool flag = false;
			for (int i = 1; i < n; ++i) {
				if (st[u[i]] == 0 && st[v[i]] == 0) {
					flag = true;
					break;
				}
			}
			if (flag) {
				continue;
			}
			int tmp = 0;
			for (int i = 1; i <= n; ++i) {
				tmp += st[i] * cost[i];
			}
			ans = std::min(ans, tmp);
		}
		return ans == 0x3f3f3f3f ? -1 : ans;
	}
}


int main(void)
{
	OPEN(defense);
	scanf("%d%d%*s", &n, &m);
	for (int i = 1; i <= n; ++i) {
		scanf("%d", &cost[i]);
	}
	for (int i = 1; i < n; ++i) {
		scanf("%d%d", &u[i], &v[i]);
	}
	while (m--) {
		int a, x, b, y;
		scanf("%d%d%d%d", &a, &x, &b, &y);
		printf("%d\n", P16::solve(a, x, b, y));
	}
	return 0;
}
