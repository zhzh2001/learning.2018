#include<bits/stdc++.h>
#define ll long long
#define mod 1000000007
using namespace std;
void fff(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}
ll read(){
	ll u=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') f=-1;else f=1;c=getchar();}
	while(c>='0'&&c<='9') u=(u<<1)+(u<<3)+c-'0',c=getchar();
	return u*f;
}
void write(ll x){
	int le=0,p[30];
	if(x<0) x=-x;
	if(x==0) putchar('0');
	while(x) p[++le]=x%10,x/=10;
	while(le) putchar('0'+p[le--]);
}
int n,m,mp[10][10],cnt,ans;
string an[100];

void dfss(int x,int y,string s2){
	if(x==n&&y==m){
		an[++cnt]=s2;
	}
	if(x<n) dfss(x+1,y,s2+(char)('0'+mp[x+1][y]));
	if(y<m) dfss(x,y+1,s2+(char)('0'+mp[x][y+1]));
}

bool check(){
	cnt=0;
	string s;
	s+='0'+mp[1][1];
	dfss(1,1,s);
	for(int i=2;i<=cnt;i++){
		if(an[i]<an[i-1]){
			return false;
		}
	}
	return true;
}

void dfs(int x,int y){
	if(y==m+1){
		if(check()){
			ans++;
		}
		return;
	}
	if(x==n+1){
		dfs(1,y+1);
		return;
	}
	for(int i=0;i<=1;i++){
		mp[x][y]=i;
		dfs(x+1,y);
	}
}

ll powmod(ll a,ll b){
	if(b==0) return 1;if(b==1) return a;
	ll t=powmod(a,b>>1);
	t=(t*t)%mod;
	if(b&1) t=(t*a)%mod;
	return t;
}

int main(){
	fff();
	cin>>n>>m;
	if(n>m) swap(n,m);
	if(n==1){
		cout<<m+1;
		return 0;
	}
	if(n==2){
		cout<<(4*powmod(3,m-1))%mod;
		return 0;
	}
	dfs(1,1);
	cout<<ans<<"\n";
}
