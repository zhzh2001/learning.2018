#include<bits/stdc++.h>
#define ll long long
using namespace std;
void fff(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
}
ll read(){
	ll u=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') f=-1;else f=1;c=getchar();}
	while(c>='0'&&c<='9') u=(u<<1)+(u<<3)+c-'0',c=getchar();
	return u*f;
}
void write(ll x){
	int le=0,p[30];
	if(x<0) x=-x,putchar('-');
	if(x==0) putchar('0');
	while(x) p[++le]=x%10,x/=10;
	while(le) putchar('0'+p[le--]);
}
ll n,m,cnt,v[100010],hed[100010],nex[200010],vt[200010];
string s;
ll fa[100010][20],dp[100010][2],dep[100010],f[100010][2];

void add(){
	ll x=read(),y=read();
	vt[++cnt]=y,nex[cnt]=hed[x],hed[x]=cnt;
	vt[++cnt]=x,nex[cnt]=hed[y],hed[y]=cnt;
}

void dfs(int u){
	dep[u]=dep[fa[u][0]]+1;
	for(int i=hed[u];i;i=nex[i])
		if(vt[i]!=fa[u][0]){
			fa[vt[i]][0]=u,dfs(vt[i]);
			dp[u][0]+=dp[vt[i]][1];
			dp[u][1]+=min(dp[vt[i]][0],dp[vt[i]][1]);
		}
	dp[u][1]+=v[u];
}

void dfs2(int u){
	if(u!=1){
		f[u][0]=dp[u][0]+f[fa[u][0]][1]-min(dp[u][0],dp[u][1]);
		f[u][1]=dp[u][1]+min(f[fa[u][0]][1]-min(dp[u][0],dp[u][1]),f[fa[u][0]][0]-dp[u][1]);
	}
	for(int i=hed[u];i;i=nex[i]) if(vt[i]!=fa[u][0]) dfs2(vt[i]);
}

ll lca(ll x,ll y){
	if(dep[x]>dep[y]) swap(x,y);
	for(int i=18;i>=0;i--) if(dep[fa[y][i]]>=dep[x]) y=fa[y][i];
	if(x==y) return x;
	for(int i=18;i>=0;i--) if(fa[x][i]!=fa[y][i]) x=fa[x][i],y=fa[y][i];
	return fa[x][0];
}

void solve(ll a,ll x,ll b,ll y){
	ll lc=lca(a,b),res0,res1;
	if(dep[a]>dep[b]) swap(a,b),swap(x,y);
	res0=f[lc][0],res1=f[lc][1];
	ll tmp[100010][2];
	if(y==0){
		tmp[b][1]=0x7fffffffffffff;
		tmp[b][0]=dp[b][0];
	}
	else{
		tmp[b][0]=0x7fffffffffffff;
		tmp[b][1]=dp[b][1];
	}
	ll ff=fa[b][0];
	while(ff!=lc){
		if(b==0) break;
		tmp[ff][0]=dp[ff][0]-dp[b][1]+tmp[b][1];
		tmp[ff][1]=dp[ff][1]-min(dp[b][1],dp[b][0])+min(tmp[b][1],tmp[b][0]);
		b=ff,ff=fa[b][0];
	}
	if(x==0&&lc==a) res0-=dp[b][1]-tmp[b][1],res1=res0;
	else res1-=min(dp[b][0],dp[b][1])-min(tmp[b][0],tmp[b][1]),res0-=dp[b][1]-tmp[b][1];
	if(lc!=a){
		if(x==0){
			tmp[a][1]=0x7fffffffffffff;
			tmp[a][0]=dp[a][0];
		}
		else{
			tmp[a][1]=dp[a][1];
			tmp[a][0]=0x7fffffffffffff;
		}
		ff=fa[a][0];
		while(ff!=lc){
			tmp[ff][0]=dp[ff][0]-dp[a][1]+tmp[a][1];
			tmp[ff][1]=dp[ff][1]-min(dp[a][1],dp[b][0])+min(tmp[a][1],tmp[a][0]);
			a=ff,ff=fa[a][0];
		}
		res1-=min(dp[a][0],dp[a][1])-min(tmp[a][0],tmp[a][1]);
		res0-=dp[a][1]-tmp[a][1];
	}
	else if(x==0) res1=0x7ffffffffffff;
	else res0=0x7fffffffffffff;
	write(min(res0,res1)),putchar('\n');
}

int main(){
	fff();
	n=read(),m=read();cin>>s;
	for(int i=1;i<=n;i++) v[i]=read();
	for(int i=1;i<n;i++) add();
	dfs(1);
	f[1][0]=dp[1][0],f[1][1]=dp[1][1];
	if(s[1]=='1') f[1][0]=0x7fffffffffffff;
	dfs2(1);
	for(int l=1;(1<<l)<=n;l++) for(int i=1;i<=n;i++) fa[i][l]=fa[fa[i][l-1]][l-1];
	for(int i=1;i<=m;i++){
		ll a=read(),x=read(),b=read(),y=read();
		if(s[1]=='1'){
			write(f[b][y]),putchar('\n');
			continue;
		}
		if((x|y)==0){
			for(int i=hed[a];i;i=nex[i]){
				if(vt[i]==b){
					write(-1);putchar('\n');
					goto aaa;
				}
			}
		}
		solve(a,x,b,y);
		aaa:;
	}
}
