#include<bits/stdc++.h>
#define ll long long
using namespace std;
void fff(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
}
ll read(){
	ll u=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') f=-1;else f=1;c=getchar();}
	while(c>='0'&&c<='9') u=(u<<1)+(u<<3)+c-'0',c=getchar();
	return u*f;
}
void write(ll x){
	int le=0,p[30];
	if(x<0) x=-x;
	if(x==0) putchar('0');
	while(x) p[++le]=x%10,x/=10;
	while(le) putchar('0'+p[le--]);
}
ll n,m,cnt,ans[5010];
vector<int> g[5010];
ll dfn[5010],low[5010],cct,ctc,ne[5010],sta[5010];
ll rou;

void tarjan(int u,int fa){
	dfn[u]=low[u]=++cnt;sta[++cct]=u;
	int lt=g[u].size();
	for(int i=0;i<lt;i++){
		if(!dfn[g[u][i]]){
			tarjan(g[u][i],u);
			low[u]=min(low[u],low[g[u][i]]);
		}
		else if(g[u][i]!=fa) low[u]=min(low[u],low[g[u][i]]);
	}
	if(dfn[u]==low[u]){
		int v=0,size=0;++ctc;
		while(u!=v){
			v=sta[cct--];
			ne[v]=ctc;size++;
		}
		if(size>1) rou=ctc;
	}
}
bool vi[5010],flag=true;

void dfs(int u,ll last){
	vi[u]=1;ans[++cnt]=u;
	sort(g[u].begin(),g[u].end());
	int lt=g[u].size();
	for(int i=0;i<lt;i++){
		if(!vi[g[u][i]]){
			if(flag){
				if(i==lt-1&&ne[u]==rou){
					if(g[u][i]<last) dfs(g[u][i],last);
					else{
						flag=false;
						return;
					}
				}
				else{
					if(ne[u]==rou) dfs(g[u][i],g[u][i+1]);
					else dfs(g[u][i],0x7fffffff);
				}
			}
			else{
				dfs(g[u][i],0x7fffffff);
			}
		}
	}
}

int main(){
	fff();
	n=read(),m=read();
	for(int i=1;i<=m;i++){
		ll x=read(),y=read();
		g[x].push_back(y),g[y].push_back(x);
	}
	cnt=0,tarjan(1,0);
	cnt=0,dfs(1,0x7fffffff);
	for(int i=1;i<=cnt;i++) write(ans[i]),putchar(' ');
	return 0;
}
