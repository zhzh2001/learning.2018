const p=1000000007;
var
  n,m,total:longint;
  ans:array[1..8,1..8]of integer=((2,4,8,16,32,64,128,256),(4,12,36,108,324,972,2916,8748),(8,36,112,336,1008,3024,9072,27216),(16,108,336,912,2688,8064,24192,36288),(32,324,1008,2688,7136,21312,0,0),(64,972,3024,8064,21312,0,0,0),(128,2916,9072,24192,0,0,0,0),(256,8748,27216,36288,0,0,0,0);
  function asdf(x,k:longint):qword;
  begin
    if x=0 then exit(1);
    if x=1 then exit(k);
    if odd(x) then asdf:=(sqr(asdf(x div 2,k))mod p*k)mod p
      else asdf:=sqr(asdf(x div 2,k))mod p;
  end;
begin
  assign(input,'game.in');
  assign(output,'game.out');
  reset(input);
  rewrite(output);

  readln(n,m);
  if (n<=8)and(m<=8) then writeln(ans[n,m])
  else
  if n<=3 then
  begin
    total:=ans[n,n];
    if n=1 then writeln(total*asdf(m-n,2) mod p)
      else writeln(total*asdf(m-n,3)mod p);
  end;

  close(input);
  close(output);
end.
