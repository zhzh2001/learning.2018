var
  ch,ch1:string;
  n,m,i,j,x1,x2,y1,y2,ans:longint;
  f,g,w:array[0..100000]of longint;
  function min(x,y:longint):longint;
  begin
    if x<y then exit(x)
      else exit(y);
  end;
begin
  assign(input,'defense.in');
  assign(output,'defense.out');
  reset(input);
  rewrite(output);

  readln(n,m,ch);
  for i:=1 to n do read(w[i]);
  readln;
  for i:=1 to n-1 do readln(ch1);
  if ch[3]='2' then
  begin
    for i:=1 to m do
    begin
      readln(x1,y1,x2,y2);
      if y1+y2=0 then writeln(-1);
    end;
  end;
  if ch[2]='A' then
  begin
    fillchar(f,sizeof(f),0);
    fillchar(g,sizeof(g),0);
    for i:=1 to m do
    begin
      readln(x1,y1,x2,y2);
      for j:=1 to n do
      begin
        f[j]:=min(g[j-1],f[j-1])+w[j];
        g[j]:=f[j-1];
        if x1=j then
          if y1=0 then f[j]:=999999999
            else g[j]:=999999999;
        if x2=j then
          if y2=0 then f[j]:=999999999
            else g[j]:=999999999;
      end;
      ans:=min(f[n],g[n]);
      if ans>=999999999 then ans:=-1;
      writeln(ans);
    end;
  end;

  close(input);
  close(output);
end.
