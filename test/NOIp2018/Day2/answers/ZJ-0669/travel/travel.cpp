#include<cstdio>
#include<cctype>
#include<queue>
#include<algorithm>
#define maxn 5050
using namespace std;
inline int read() {
	int x = 0;char c=getchar();
	while(!isdigit(c)) c=getchar();
	while(isdigit(c)) x=(x<<3)+(x<<1)+c-48,c=getchar();return x;
}
struct Edge{
	int from,to,next;
}e[maxn];
int n,m,cnt,vis[maxn],p[maxn],head[maxn];
inline int add(int u,int v) {
	e[++cnt].to = v;
	e[cnt].next=head[u];
	head[u]=cnt;
}

inline int cmp(int x,int y) {
	return x < y;
}

void dfs(int x,int fa) {
	if(p[x]) return;
	if(!p[x]) {
		printf("%d ",x);
		p[x]=1;
	}
	
	int a[20],cnt=0;
	for(int i=head[x];i;i=e[i].next) {
		int v=e[i].to;
		if(v==fa) continue;
		a[++cnt] = v;
	}
	
	sort(a+1,a+1+cnt,cmp);
	
	for(int i=1;i<=cnt;i++) {
		dfs(a[i],x);
	}
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++) {
		int x=read(),y=read();
		add(x,y);add(y,x);
	}
	dfs(1,-1);
	return 0;
}                   

