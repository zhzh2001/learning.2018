#include<cstdio>
#include<cctype>
#include<algorithm>
#include<cstring>
#define maxn 100000
#define inf 10000000
#define ll long long
using namespace std;
inline int read() {
	int x =0;char c=getchar();
	while(!isdigit(c)) c=getchar();
	while(isdigit(c)) x=(x<<3)+(x<<1)+c-48,c=getchar();return x;
}

struct Edge {
	int from,to,next;
}e[maxn];
int n,m,p[maxn],head[maxn],cnt,fa[maxn],vis[maxn];
int a,x,b,y,in[maxn];
ll ans = inf;
char c[5];
inline void add(int u,int v) { e[++cnt].to=v;e[cnt].next=head[u];e[cnt].from = u; head[u]=cnt; }

void doit(int x) {
	vis[x]=1;
	for(int i=head[x];i;i=e[i].next) {
		int v=e[i].to;
		vis[v]=1;
	}
}
inline ll cal(int s) {
	int cnt = 1;
	ll mon = 0;
	while(s) {
		if(s & 1) {
			mon += 1LL*p[cnt];
		}
		s>>=1;cnt++;
	}
	return mon;
}
int check(int s) {
	memset(vis,0,sizeof(vis));
	int cnt = 1;
	while(s) {
		if(s & 1) {
			doit(cnt);
		}
		s>>=1;cnt++;
	}
	for(int i=1;i<=n;i++) {
		if(!vis[i])  return false;
	}
	return true;
}
void work() {
	for(int i=1;i<(1<<n);i++) {
		if(x){i|=(1<<(a-1));}
		if(y){i|=(1<<(b-1));}
		if(!x) {if(i & (1<<(a-1))) continue;}
		if(!y) {if(i & (1<<(b-1))) continue;}
		if(check(i)) {
			ans = min(ans,cal(i));
		}
	}
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n = read(),m=read();scanf("%s",c);
	for(int i=1;i<=n;i++) p[i] = read();
	for(int i=1;i<n;i++) {
		int x=read(),y=read();
		add(x,y);add(y,x);
	}
	for(int i=1;i<=m;i++) {
		a=read(),x=read(),b=read(),y=read();
		work();
		if(ans == inf) {
			printf("-1\n");
			continue;
		}
		printf("%d\n",ans);
		ans = inf;
	}
	return 0;
}
