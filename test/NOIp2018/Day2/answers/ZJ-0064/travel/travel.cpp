#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#define rd read()
using namespace std;

const int N = 5e3 + 5;

int n, m, ans[N], num, vis[N], cir[N], inq[N];
int st[N], tp;
int low[N], dfn[N], cnt;
int tocir[N];

vector<int> to[N];
set<int> S;

int read() {
	int X = 0, p = 1; char c = getchar();
	for (; c > '9' || c < '0'; c = getchar())
		if (c == '-') p = -1;
	for (; c >= '0' && c <= '9'; c = getchar())
		X = X * 10 + c - '0';
	return X * p;
} 

void dfs1(int u, int fa) {
	ans[++num] = u;
	for (int i = 0, up = to[u].size(); i < up; ++i) {
		int nt =to[u][i];
		if (nt == fa) continue;
		dfs1(nt, u);
	} 
} 

void tarjan(int u, int pre) {
	dfn[u] = low[u] = ++cnt;
	st[++tp] = u;
	for (int i = 0, up = to[u].size(); i < up; ++i) {
		int nt = to[u][i];
		if (nt == pre) continue;
		if (!dfn[nt]) {
			tarjan(nt, u);
			low[u] = min(low[u], low[nt]);
			if (low[nt] <= dfn[u]) {
				cir[nt] = 1;
				for (; tp; ) {
					int z = st[tp--];
					cir[z] = 1;
					if (z == u) break;
				} 
			} 
		} else low[u] = min(low[u], dfn[nt]); 
	}
	if (st[tp] == u) tp--;
} 

int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	n = rd; m = rd;
	for (int i = 1; i <= m; ++i) {
		int u = rd, v = rd;
		to[u].push_back(v);
		to[v].push_back(u);
	} 
	for (int i = 1; i <= n; ++i) 
		sort(to[i].begin(), to[i].end());
	if (m == n - 1) {
		dfs1(1, 0);
		for (int i = 1; i <= n; ++i) 
			printf("%d ", ans[i]);
	}
} 
