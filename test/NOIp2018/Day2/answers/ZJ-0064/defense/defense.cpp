#include<cstdio>
#include<cstring>
#include<algorithm>
#define rd read()
#define ll long long
using namespace std;

const int N = 1e5 + 5;
const ll inf = 4557430888798830399LL;

int n, m, cost[N];
int tot, head[N], lim[N];
ll f[N][3], ans;
char op[10];

struct edge {
	int nxt, to;
}e[N << 1]; 

int read() {
	int X = 0, p = 1; char c = getchar();
	for (; c > '9' || c < '0'; c = getchar())
		if (c == '-') p = -1;
	for (; c >= '0' && c <= '9'; c = getchar())
		X = X * 10 + c - '0';
	return X * p;
} 

void add(int u, int v) {
	e[++tot].to = v;
	e[tot].nxt = head[u];
	head[u] = tot;
} 

void down(ll &A, ll B) {
	if (A > B) A = B;
} 

void dp(int u, int fa) {
	ll tmp = 0;
	for (int i = head[u]; i; i = e[i].nxt) {
		int nt = e[i].to;
		if (nt == fa) continue;
		dp(nt, u);
	} 
	for (int i = head[u]; i; i = e[i].nxt) {
		int nt = e[i].to;
		if (fa == nt) continue;
		tmp += f[nt][1];
		if (lim[nt] == 0) tmp = inf;
		if (tmp == inf) break;
	} 
	down(f[u][0], tmp);
	if (lim[u] != 0) {
		tmp = 0;
		for (int i = head[u]; i; i = e[i].nxt) {
			int nt = e[i].to;
			if (fa == nt) continue;
			tmp += min(f[nt][0], f[nt][1]);
		}
		f[u][1] = tmp + cost[u];
	}  
	if (lim[u] == 1) f[u][0] = inf;
	if (lim[u] == 0) f[u][1] = inf;
} 

int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w" ,stdout);
	n = rd; m = rd; scanf("%s", op);
	for (int i = 1; i <= n; ++i) 
		cost[i] = rd;
	for (int i = 1; i < n; ++i) {
		int u = rd, v = rd;
		add(u, v); add(v, u);
	} 
	memset(lim, -1, sizeof(lim));
	for (int i = 1; i <= m; ++i) {
		memset(f, 63, sizeof(f));
		int x = rd, a = rd, y = rd, b = rd;
		lim[x] = a, lim[y] = b;
		dp(1, 0);
		if (lim[1] == -1) printf("%lld\n", (ans = min(f[1][1], f[1][0])) >= inf ? -1 : ans);
		if (lim[1] == 0) printf("%lld\n", (ans = f[1][0]) >= inf ? -1 : ans);
		if (lim[1] == 1) printf("%lld\n", (ans = f[1][1]) >= inf ? -1: ans);
		lim[x] = -1; lim[y] = -1;
	} 
} 
