#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;

const ll mod = 1e9 + 7;
const int N = 1e6 + 5;

int n, m;
ll ans = 4;

ll fpow(ll a, ll b) {
	ll res = 1;
	for (; b; b >>= 1, a = a * a % mod) 
		if (b & 1) res = res * a % mod;
	return res;
} 

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n == 1) {
		printf("%lld\n", fpow(2, m)); 
	} 
	if (n == 2) {
		ans = ans * fpow(3, m - 1) % mod;
		printf("%lld\n", ans);
	} 
	if (n == 3) {
		if (m == 1) printf("%lld\n", fpow(2, n));
		if (m == 2) {
			ans = ans * fpow(3, n - 1) % mod;
			printf("%lld\n", ans);
		} 
		if (m == 3) puts("112");
	} 
} 
