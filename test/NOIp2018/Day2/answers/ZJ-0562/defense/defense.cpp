#include<bits/stdc++.h>
#define MAXN 110000
using namespace std;
int n,m,a,x,b,y;
char type[10];
int p[MAXN];
long long f[MAXN][2];
int hed[MAXN],ed[MAXN*2],nxt[MAXN*2],l = 0;
inline void Line(int u,int v)
{
	nxt[++l] = hed[u],hed[u] = l,ed[l] = v;
	nxt[++l] = hed[v],hed[v] = l,ed[l] = u;
}
inline int read()
{
	int s = 0,w = 1;char ch = getchar();
	while(ch<'0'||ch>'9'){if(ch == '-')w = -1;ch = getchar();}
	while(ch>='0'&&ch<='9'){s = (s << 1) + (s << 3) + ch - '0';ch = getchar();}
	return s * w;
}
inline void DP(int u,int fa)
{
	f[u][1] = p[u];
	for(int i = hed[u];i;i=nxt[i])
	{
		if(ed[i] == fa) continue;
		DP(ed[i],u);
		if(f[ed[i]][1] == -1 && f[ed[i]][0] == -1)
		{
			f[u][1] = f[u][0] = -1;
			return;
		}
		else if(f[ed[i]][1] == -1)
		{
			f[u][0] = -1;
			if(f[u][1] != -1) f[u][1] += f[ed[i]][0];
		}
		else if(f[ed[i]][0] == -1)
		{
			if(f[u][0] != -1) f[u][0] += f[ed[i]][1];
			if(f[u][1] != -1) f[u][1] += f[ed[i]][1];
		}
		else
		{
			if(f[u][0] != -1) f[u][0] += f[ed[i]][1];
			if(f[u][1] != -1) f[u][1] += min(f[ed[i]][0],f[ed[i]][1]);
		}
	}
	if(u == a) f[u][x^1] = -1;
	if(u == b) f[u][y^1] = -1;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n = read(),m = read(),cin >> type;
	for(int i = 1;i<=n;i++) p[i] = read();
	for(int i = 1;i<n;i++) Line(read(),read());
	while(m--)
	{
		
		memset(f,0,sizeof(f));
		a = read(),x = read(),b = read(),y = read();
		if(type[1] == '2' && x == 0 && y == 0)
		{
			printf("-1\n");
			continue;
		}
		DP(1,0);
		if(a == 1) f[1][x^1] = -1;
		if(b == 1) f[1][y^1] = -1;
		if(f[1][1] == -1)
		{
			if(f[1][0] == -1) printf("-1\n");
			else printf("%lld\n",f[1][0]);
		}
		else if(f[1][0] == -1) printf("%lld\n",f[1][1]);
		else printf("%lld\n",min(f[1][1],f[1][0]));
	}
	return 0;
}
