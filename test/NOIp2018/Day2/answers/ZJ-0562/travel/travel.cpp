#include<bits/stdc++.h>
#define MAXN 5100
#define INF 2147483647
using namespace std;
int n,m,S;
int M[MAXN][MAXN],Ml[MAXN];
int r[MAXN];
bool vis[MAXN],used[MAXN],Mp[MAXN][MAXN];
inline void Line(int u,int v){M[u][++Ml[u]] = v,M[v][++Ml[v]] = u,r[u]++,r[v]++,Mp[u][v] = Mp[v][u] = 1;}
inline void TreeD(int u)
{
	used[u] = 1;
	printf("%d ",u);
	sort(M[u]+1,M[u]+1+Ml[u]);
	for(int i = 1;i<=Ml[u];i++) if(!used[M[u][i]]&&Mp[u][M[u][i]]) TreeD(M[u][i]);
}
inline int read()
{
	int s = 0,w = 1;char ch = getchar();
	while(ch<'0'||ch>'9'){if(ch == '-')w = -1;ch = getchar();}
	while(ch>='0'&&ch<='9'){s = (s << 1) + (s << 3) + ch - '0';ch = getchar();}
	return s * w;
}
inline void TP()
{
	queue<int>Q;
	for(int i = 1;i<=n;i++) if(r[i] == 1) Q.push(i);
	while(!Q.empty())
	{
		int k = Q.front();
		Q.pop();
		vis[k] = 1;
		for(int i = 1;i<=Ml[k];i++)
		{
			r[M[k][i]]--;
			if(r[M[k][i]] == 1) Q.push(M[k][i]);
		}
	}
}
inline void HuanD(int u,int opt,bool tmp)
{
	used[u] = 1;
	if(vis[u])
	{
		for(int i= 1;i<=Ml[u];i++)
		{
			if(!used[M[u][i]]) HuanD(M[u][i],0,0);
		}
	}	
	else if(tmp == 0)
	{
		S = u;
		int l = 0,r = 0;
		for(int i = 1;i<=Ml[u];i++)
			if(!vis[M[u][i]])
			{
				if(!l) l = M[u][i];
				else r = M[u][i];
			}
		if(l < r) HuanD(l,r,1);
		else HuanD(r,l,1);
	}
	else if(u == opt)
	{
		Mp[u][S] = Mp[S][u] = 0;
		return;
	}
	else if(tmp)
	{
		for(int i = 1;i<=Ml[u];i++)
		{
			if(!used[M[u][i]] && !vis[M[u][i]])
			{
				if(M[u][i] > opt)
				{
					Mp[u][M[u][i]] = Mp[M[u][i]][u] = 0;
					return;
				}
				else HuanD(M[u][i],opt,1);
			}
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n = read(),m = read();
	for(int i = 1;i<=m;i++) Line(read(),read());
	if(m == n-1) TreeD(1);
	else 
	{
		TP();
		HuanD(1,0,0);
		memset(used,0,sizeof(used));
		TreeD(1);
	}
	return 0;
}
