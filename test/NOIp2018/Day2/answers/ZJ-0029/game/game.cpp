#include<bits/stdc++.h>
#define N 500005
#define Mod 1000000007
using namespace std;
int n,m,wr,flag,a[55][55],p[N],ans[N];
inline int ksm(int x,int y){
	int ans1=1;while (y){
		if (y&1) ans1=1ll*ans1*x%Mod;
		y>>=1;x=1ll*x*x%Mod;
	}return ans1;
}/*
void dfs(int x,int y,int dep){
	if (x>n||y>m) return;
	if (wr) return;
	p[dep]=a[x][y];
	if (x==n&&y==m){
		if (flag){
			for (int i=1;i<=dep;i++) ans[i]=p[i];
			flag=0;
			return;
		}
		for (int i=1;i<=dep;i++)
			if (ans[i]<p[i]){wr=1;return;}
		for (int i=1;i<=dep;i++) ans[i]=p[i];
		return;
	}
	dfs(x+1,y,dep+1);
	dfs(x,y+1,dep+1);
}
inline bool check(int x){
	int cnt=0;flag=1;wr=0;
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			if (x>>cnt&1) a[i][j]=1;
			else a[i][j]=0;
			cnt++;
		}
	}
	dfs(1,1,1);
	return wr^1;
}*/
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1||m==1){
		printf("%d\n",ksm(2,n*m));
		return 0;
	}
	if (n==2) return printf("%lld\n",1ll*4*ksm(3,m-1)%Mod),0;
	if (n==3){
		if (m==2) return printf("%d\n",36),0;
		if (m==3) return printf("%d\n",112),0;
		printf("%lld\n",1ll*7*ksm(4,m-1)%Mod);
		return 0;
	}
	puts("7136");
	return 0;
}
