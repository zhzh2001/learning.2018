#include<bits/stdc++.h>
#define inf 1000000000000
#define int long long
#define N 500005
using namespace std;
int n,Q,x,y,xx,yy,kk,p[N],dp[N][3],tmp[N],ckr[N],head[N],father[N];
struct Tree{int nxt,to;}e[N];
inline void link(int x,int y){e[++kk].nxt=head[x];e[kk].to=y;head[x]=kk;}
void dfs1(int u,int fa){
	if (ckr[u]==-1){
		dp[u][0]=0;dp[u][1]=p[u];
	}
	else if (ckr[u]==0){
		dp[u][0]=0;dp[u][1]=inf;
	}
	else dp[u][0]=inf,dp[u][1]=p[u];
	for (int i=head[u];i;i=e[i].nxt){
		int v=e[i].to;
		if (v==fa) continue;
		dfs1(v,u);
		dp[u][1]+=min(dp[v][1],dp[v][0]);
		dp[u][0]+=dp[v][1];
	}
}
void dfs(int u,int fa){
	for (int i=head[u];i;i=e[i].nxt){
		int v=e[i].to;
		if (v==fa) continue;
		father[v]=u;
	}
}
signed main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char s[5];
	scanf("%lld%lld%s",&n,&Q,s+1);
	if (n<=5000){
		for (int i=1;i<=n;i++) scanf("%lld",&p[i]);
		for (int i=1;i<n;i++){
			scanf("%lld%lld",&x,&y);
			link(x,y);link(y,x);
		}
		dfs(1,-1);
		for (int i=1;i<=n;i++) ckr[i]=-1;
		while (Q--){
			scanf("%lld%lld%lld%lld",&x,&y,&xx,&yy);
			if (father[x]==xx||father[xx]==x){puts("-1");continue;}
			ckr[x]=y;ckr[xx]=yy;
			dfs1(1,-1);
			ckr[x]=ckr[xx]=-1;
			int tmpp=min(dp[1][1],dp[1][0]);
			if (tmpp>=inf) puts("-1");
			else printf("%lld\n",tmpp);
		}
		return 0;
	}
	return 0;
}
