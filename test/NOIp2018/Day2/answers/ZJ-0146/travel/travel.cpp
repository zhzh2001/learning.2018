#include<bits/stdc++.h> //Ithea Myse Valgulious
namespace chtholly{
typedef long long ll;
#define re0 register int 
#define rel register ll 
#define rec register char
#define gc getchar
#define pc putchar
#define p32 pc(' ')
#define pl puts("")
inline int read() {
  int x=0,f=1;char c=gc();
  for (;!isdigit(c);c=gc()) f^=c=='-';
  for (;isdigit(c);c=gc()) x=x*10+(c^'0');
  return f?x:-x;
  }
template <typename mitsuha>
inline bool read(mitsuha &x) {
  x=0;int f=1;char c=gc();
  for (;!isdigit(c)&&~c;c=gc()) f^=c=='-';
  if (!~c) return 0;
  for (;isdigit(c);c=gc()) x=x*10+(c^'0');
  return x=f?x:-x,1;
  }
template <typename mitsuha> 
inline int write(mitsuha x) {
  if (!x) return 0&pc(48);
  if (x<0) x=-x,pc('-');
  int bit[20],i,p=0;
  for (;x;x/=10) bit[++p]=x%10;
  for (i=p;i;--i) pc(bit[i]+48);
  return 0;
  }
inline char fuhao() {
  char c=gc();
  for (;isspace(c);c=gc());
  return c;
  }
}using namespace chtholly;
using namespace std;
const int aoi=10038;
typedef int you[aoi];
vector<int> lj[aoi];

namespace stree{
  vector<int> path;
  void dfs(int u,int fa) {
    path.push_back(u);
    for (int i=0,v;i<lj[u].size();++i) {
      if ((v=lj[u][i])^fa) dfs(v,u);
    }
  }
  int main(int n) {
    dfs(1,0);
    for (int i=0;i<path.size();++i) {
      printf("%d ",path[i]);
    } pl;
    return 0;
  }
}/*一棵树是送的,只要一遍dfs即可.*/

namespace ctree{
  you cir,vis,stk,inc,fa; int hascir;
  /*表示当前圆上的所有点的编号,是否在环上,当前访问的累计值.*/
  void gcir(int u,int fa) {
    vis[u]=1,stk[++*stk]=u;
    for (int i=0,v;i<lj[u].size();++i) {
      if (!vis[v=lj[u][i]]) gcir(v,u);
      else if (v^fa) {
        if (hascir) return;
        cir[++*cir]=v;
        for (;stk[*stk]^v;--*stk) {
          cir[++*cir]=stk[*stk];
        }  
        hascir=1;
        return;
      }
    }
  }
  vector<int> path;
  int main(int n) {
    int i,u,v;
    gcir(1,0);
    for (i=1;i<=*cir;++i) inc[cir[i]]=1;
    memset(vis,0,sizeof vis);
    queue<int> q;
    for (i=1;i<=*cir;++i) 
      q.push(cir[i]),vis[cir[i]]=1;
    for (;!q.empty();) {
      u=q.front(),q.pop();
      for (i=0;i<lj[u].size();++i) {
        if (!inc[v=lj[u][i]]&&!vis[v]) {
          fa[v]=u,q.push(v),vis[v]=1;
        }
      }
    }
    int now=1;
    memset(vis,0,sizeof vis);
    for (;fa[now];now=fa[now]) path.push_back(now),vis[now]=1;
    /*现在我在环上.*/
    priority_queue<int,vector<int>,greater<int> > pq;
    pq.push(now),vis[now]=1;
    for (;!pq.empty();) {
      u=pq.top(),pq.pop();
      path.push_back(u);
      for (i=0;i<lj[u].size();++i) {
        if (!vis[v=lj[u][i]]) 
          pq.push(v),vis[v]=1;
      }
    }
    for (i=0;i<path.size();++i) printf("%d ",path[i]);
    return 0;
  }
}
/*
考虑基环树.
老道理先找到环.
接下来处理出以环为根节点,剩下所有点的父亲.
*/
void fileopen(string s) {
  freopen((s+".in").c_str(),"r",stdin);
  freopen((s+".out").c_str(),"w",stdout);
}

int main() {
  fileopen("travel");
	int i,n,m,u,v;
  read(n),read(m);
  for (i=1;i<=m;++i) {
    read(u),read(v);
    lj[u].push_back(v);
    lj[v].push_back(u);
  }
  for (i=1;i<=n;++i)
    sort(lj[i].begin(),lj[i].end()); 
  if (n^m) return stree::main(n); // 一棵树
  else return ctree::main(n); // 基环树
}
/*雅雅姐祝我这题信仰得高分.*/