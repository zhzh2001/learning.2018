//Suplex
#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
#define N 10000
using namespace std;
int n,m,cnt,tot,head[N],ans[N],vet[N+N],Next[N+N],num[N],q[5050][5050],id[N+N],res[N];
//int Time,color,top,col[N],dfn[N],low[N],stack[N],c[N];
int start,x1,x2,xx,d[N],Q[N],del[N],faa[N],in[N],flag[N],U[N],V[N];
bool fff,cut;

inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0' || ch>'9'){if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0' && ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x*f;
}

void add_edge(int u,int v,int ide)
{
	vet[++cnt]=v;Next[cnt]=head[u];id[cnt]=ide;head[u]=cnt;
}

void dfs(int u,int fa)
{
	ans[++tot]=u;
	num[u]=0;
	for(int i=head[u];i;i=Next[i])
		if(vet[i]!=fa) q[u][++num[u]]=vet[i];
	sort(q[u]+1,q[u]+1+num[u]);
	for(int i=1;i<=num[u];i++)
		dfs(q[u][i],u);
}

void solve1()
{
	for(int i=1;i<=m;i++)
		add_edge(U[i],V[i],i),add_edge(V[i],U[i],i);
	dfs(1,0);
	for(int i=1;i<tot;i++) printf("%d ",ans[i]);
	printf("%d\n",ans[tot]);
}

void dfs1(int u,int fa)
{
	flag[u]=1;
	for(int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if(v==fa) continue;
		if(!flag[v]) dfs1(v,u);
		else in[v]=1;
	}
}

void find(int u,int fa)
{
	if(start) return;
	faa[u]=fa;
	if(in[u]){start=u;return;}
	for(int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if(v==fa) continue;
		find(v,u);
	}
}

void dfs2(int u,int fa)
{
	if(cut) return;
	num[u]=0;
	for(int i=head[u];i;i=Next[i])
		if(vet[i]!=fa && in[vet[i]]) xx=i;
	if(vet[xx]<x2) dfs2(vet[xx],u);
	else{del[id[xx]]=1;cut=true;return;}
}

void solve2()
{
	for(int i=1;i<=m;i++) add_edge(U[i],V[i],i),add_edge(V[i],U[i],i);
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++) flag[j]=0;
		dfs1(i,0);
	}
	
	if(fff){
		find(1,0);
		int ct=0;
		for(int i=head[start];i;i=Next[i])
			if(in[vet[i]]) Q[++ct]=vet[i];
		x1=Q[1];x2=Q[2];
		if(x1>x2) swap(x1,x2);
		dfs2(x1,start);
		cnt=0;
		for(int i=1;i<=n;i++) head[i]=0;
		for(int i=1;i<=m;i++)
			if(!del[i])
				add_edge(U[i],V[i],i),add_edge(V[i],U[i],i);
		tot=0;
		dfs(1,0);
		for(int i=1;i<n;i++) printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
		return;
	}
	
	for(int i=1;i<=n;i++) res[i]=1e9;
	for(int i=1;i<=m;i++){
		if(!in[U[i]] || !in[V[i]]) continue;
		cnt=0;tot=0;
		for(int j=1;j<=n;j++) head[j]=0;
		for(int j=1;j<=m;j++)
			if(i!=j) add_edge(U[j],V[j],j),add_edge(V[j],U[j],j);
		dfs(1,0);
		bool check=false;
		for(int i=1;i<=n;i++){
			if(ans[i]>res[i]) break;
			if(ans[i]<res[i]){check=true;break;}
		}
		if(check)
			for(int i=1;i<=n;i++) res[i]=ans[i];
	}
	for(int i=1;i<n;i++) printf("%d ",res[i]);
	printf("%d\n",res[n]);
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	fff=true;
	for(int i=1;i<=m;i++){
		U[i]=read(),V[i]=read();
		d[U[i]]++;d[V[i]]++;
		if(d[U[i]]>2 || d[V[i]]>2) fff=false;
	}
	if(m==n-1) solve1();
	if(m==n) solve2();
	return 0;
}
