//Suplex
#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
using namespace std;
const int mod=1e9+7;
int n,m,ans,cnt,dx[5],dy[5],id[5],a[10][10];

struct leo{
	long long a,b;
}q[5000000];

void dfs1(int x,int y,long long val1,long long val2)
{
	if(x==n && y==m){
		q[++cnt].a=val1;
		q[cnt].b=val2;
		return;
	}
	for(int i=1;i<=2;i++){
		int xx=x+dx[i],yy=y+dy[i];
		if(xx>n || yy>m) continue;
		dfs1(xx,yy,(val1<<1)+a[x][y],(val2<<1)+id[i]);
	}
}

void dfs(int x,int y)
{
	if(x>n){
		cnt=0;
		dfs1(1,1,0,0);
		bool flag=true;
		for(int i=1;i<=cnt;i++){
			for(int j=1;j<=cnt;j++)
				if(q[i].b>q[j].b)
					if(q[i].a>q[j].a){flag=false;break;}
			if(!flag) break;
		}
		if(flag) ans++;
		return;
	}
	int xx=x,yy=y+1;
	if(yy>m) xx++,yy=1;
	a[x][y]=1;
	dfs(xx,yy);
	a[x][y]=0;
	dfs(xx,yy);
}

void solve1()
{
	dfs(1,1);
	printf("%d\n",ans);
}

void solve2()
{
	int ans=1;
	for(int i=1;i<=m;i++) ans=(ans+ans)%mod;
	printf("%d\n",ans);
}

void solve3()
{
	long long ans=1;
	for(int i=1;i<m;i++) ans=ans*3 % mod;
	for(int i=1;i<=(n*m-m-m+2);i++) ans=ans*2 % mod;
	printf("%lld\n",ans);
}

void solve4()
{
	if(m==1){puts("8");return;}
	if(m==2){puts("36");return;}
	if(m==3){puts("112");return;}
	long long ans=112;
	for(int i=4;i<=m;i++) ans=ans*3 % mod;
	printf("%lld\n",ans);
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	dx[1]=1;dx[2]=0;id[1]=0;
	dy[1]=0;dy[2]=1;id[2]=1;
	scanf("%d%d",&n,&m);
	if(n<=3 && m<=3){solve1();return 0;}
	if(n==1){solve2();return 0;}
	if(n==2){solve3();return 0;}
	if(n==3){solve4();return 0;}
	return 0;
}
