//Suplex
#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
#define N 100100
using namespace std;
const long long inf=1e15;
int n,m,cnt,A,B,X,Y,vet[N+N],Next[N+N],head[N],p[N];
long long ans,dp[N][2];
char type[15];

inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0' || ch>'9'){if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0' && ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x*f;
}

void add_edge(int u,int v)
{
	vet[++cnt]=v;Next[cnt]=head[u];head[u]=cnt;
}

void dfs(int u,int fa)
{
	dp[u][0]=0;
	dp[u][1]=p[u];
	if(u==A) dp[u][X^1]=inf;
	if(u==B) dp[u][Y^1]=inf;
	for(int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if(v==fa) continue;
		dfs(v,u);
		if(dp[u][0]<inf) dp[u][0]+=dp[v][1];
		if(dp[u][1]<inf) dp[u][1]+=min(dp[v][0],dp[v][1]);
	}
}

void solve1()
{
	while(m--){
		A=read();X=read();B=read();Y=read();
		dfs(1,0);
		ans=min(dp[1][0],dp[1][1]);
		if(ans>=inf) puts("-1");else printf("%lld\n",ans);
	}
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();
	scanf("%s",type);
	for(int i=1;i<=n;i++) p[i]=read();
	for(int i=1;i<n;i++){
		int u=read(),v=read();
		add_edge(u,v);add_edge(v,u);
	}
	solve1();
	return 0;
}
