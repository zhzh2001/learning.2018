#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define ma 1000000007
using namespace std;
inline void read(int &x)
{
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
}
inline void read(ll &x)
{
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
}
inline void write(int x)
{
	if (x>9)write(x/10);
	putchar(x%10+48);
}
inline void write(ll x)
{
	if (x>9)write(x/10);
	putchar(x%10+48);
}
int n,m,x,y,t,o=0;
int a[5010][5010],b[5010],c[5010],d[5010];
void dfs(int x)
{
	c[x]=1;
	write(x);putchar(32);
	for (int i=1;i<=n;i++)
	if (a[x][i]&&c[i]==0)
	dfs(i);
}
void dfs1(int x,int y)
{
	c[x]=1;
	d[++t]=x;
	for (int i=1;i<=n;i++)
	if (a[x][i])
	{
		if (c[i]==0)dfs1(i,x);
		else if (i!=y&&o==0)
		{
			int f=t;
			while (d[f]!=i)
			{
				if (d[f-1]<x)break;
				f--;
			}
			a[d[f-1]][d[f]]=0;
			a[d[f]][d[f-1]]=0;
			o=x;
			if (f==t)
			{
				t--;
				c[x]=0;
				break;
			}
			for (int j=f;j<t;j++)
			c[d[j]]=0;
			t=f;
			d[t]=x;
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);read(m);
	for (int i=1;i<=m;i++)
	{
		read(x);read(y);
		a[x][y]=1;
		a[y][x]=1;
	}
	if (m<n)dfs(1);
	if (m==n)
	{
		dfs1(1,0);
		for (int i=1;i<=n;i++)
		write(d[i]),putchar(32);
	}
	return 0;
}
