#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define ma 1000000007
using namespace std;
inline void read(int &x)
{
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
}
inline void read(ll &x)
{
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
}
inline void write(int x)
{
	if (x>9)write(x/10);
	putchar(x%10+48);
}
inline void write(ll x)
{
	if (x>9)write(x/10);
	putchar(x%10+48);
}
int n,m;
ll s,a[100][100];
ll fac(int x)
{
	ll t=1,y=2;
	while (x)
	{
		if (x&1)t=t*y%ma;
		y=y*y%ma;
		x>>=1;
	}
	return t;
}
int min(int x,int y)
{
	return x<y?x:y;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n);read(m);
	for (int i=1;i<=max(n,m);i++)a[1][i]=0,a[i][1]=0;
	a[2][2]=12;
	a[2][3]=a[3][2]=36;
	a[3][3]=112;
	ll f=n*m,o=min(n,m),l=1;
	for (int i=2;i<o;i++)
	{
		l=l+i;
		s=(s+fac(i-1)*fac(f-l)*(i-1))%ma;
	}
	for (int i=-1;i<n+m-2*o;i++)
	{
		l+=o;
		s=(s+fac(i+o)*fac(f-l)*(o-1))%ma;
	}
	for (int i=o-1;i>1;i--)
	{
		l+=i;
		s=(s+fac(n+m-1-i)*fac(f-l)*(i-1))%ma;
	}
	if (n<=3&m<=3)write(a[n][m]);if (n==5&&m==5)write(7136);
	else
	write((fac(f)-s+ma)%ma);
	return 0;
}
