#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define ma 1000000007
using namespace std;
inline void read(int &x)
{
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
}
inline void read(ll &x)
{
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
}
inline void write(int x)
{
	if (x>9)write(x/10);
	putchar(x%10+48);
}
inline void write(ll x)
{
	if (x>9)write(x/10);
	putchar(x%10+48);
}
struct Node{
	int x,y,z;
}b[100010];
int n,m,x,y,q,p,s=ma;
int a[100010],c[100010][3],d[100010],e[100010];
int min(int x,int y)
{
	return x<y?x:y;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n);read(m);
	char ch=getchar();
	while (ch<'A'||ch>'C')ch=getchar();
		read(x);
		for (int i=1;i<=n;i++)
		read(a[i]);
		for (int i=1;i<n;i++)
		{
			read(x);read(y);
			b[i].z=min(a[x],a[y]);
			b[i].x=x;
			b[i].y=y;
			c[x][++c[x][0]]=i;
			c[y][++c[y][0]]=i;
		}
		for (int i=1;i<=m;i++)
		{
			read(x);read(q);read(y);read(p);
			if (x>y)swap(x,y),swap(q,p);
			if (x==y-1&&q==0&&p==0)
			{
				printf("-1\n");
				continue;
			}
			if (n<=10)
			{
				memset(d,0,sizeof(0));
				memset(e,0,sizeof(0));
				s=ma;
				while (e[0]==0)
				{
					int j=n,t=0;
					while (e[j])j--;
					for (int l=j+1;l<=n;l++)
					e[l]=0;
					e[j]=1;
					if (e[x]==q&&e[y]==p)
					{
						for (int j=1;j<=n;j++)
						if(e[j])t+=a[j];
					}
					s=min(s,t);
				}
				write(s);
				printf("\n");
			}
			if (q)
			{
				d[c[x][1]]=1;
				d[c[x][2]]=1;
				s+=a[x];
			}
			else
			{
				s+=min(b[c[x][1]].z,b[c[x][2]].z);
				
			}
			if (p)
			{
				d[c[y][1]]=1;
				d[c[y][2]]=1;
				s+=a[y];
			}
			else
			{
				s+=min(b[c[y][1]].z,b[c[y][2]].z);
			}
			write(s);
			printf("\n");
		}
	return 0;
}
