#include<cstdio>
struct edge{int to,nxt;}e[105];
int tot,n,m,fir[15],p[15],a[10005],c[15];
void addedge(int u,int v) {e[++tot].to=v;e[tot].nxt=fir[u];fir[u]=tot;}
int ck()
{
	for(int i=1;i<=n;i++)
	for(int j=fir[i];j;j=e[j].nxt)
	if(!c[i]&&!c[e[j].to]) return 0;
	return 1;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	char tp[10];scanf("%s",tp);
	for(int i=1;i<n;i++)
	{
		int x,y;scanf("%d%d",&x,&y);
		addedge(x,y);addedge(y,x);
	}
	for(int i=0;i<(1<<n);i++)
	{
		for(int j=1;j<=n;j++) c[j]=i&(1<<(j-1));
		if(!ck()) {a[i]=-1;continue;}
		for(int j=1;j<=n;j++) a[i]+=c[j]*p[j];
	}
	for(;m--;)
	{
		int a,x,b,y;scanf("%d%d%d%d",&a,&x,&b,&y);
		int ans=-1;
		for(int i=0;i<(1<<n);i++)
		{
			if(p[i]==-1) continue;
			if((i&(1<<(a-1)))!=x||(i&(1<<(b-1)))!=y) continue;
			if(ans==-1||ans>p[i]) ans=p[i];
		}
		printf("%d\n",ans);
	}
	return 0;
}
