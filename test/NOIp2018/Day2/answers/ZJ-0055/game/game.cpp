#include<cstdio>
#define mod 1000000007LL
typedef long long ll;
ll f[305][2];
int n,m,g[305][305];
inline int rev(int x) {return x^511;}
int calc(int x,int y)
{
	for(int j=2;j<=n;j++)
	{
		int xx=x&rev((1<<(n-j))-1),yy=y&rev((1<<(n-j))-1);
		xx>>=n-j,yy>>=n-j;
		int pre=((xx&(1<<(j-1)))<<1)+yy;
		for(int i=j-2;i>=0;i--)
		{
			int now=((xx&rev((1<<i)-1))<<1)+(yy&((1<<(i+1))-1));
			if(now<pre) return 0;
			pre=now;
		}
	}
	return 1;
} 
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=0;i<(1<<n);i++)
	for(int j=0;j<(1<<n);j++) {g[i][j]=calc(i,j);/*if(g[i][j]) f[j][0]++;*/}
	if(n==3&&m==3) return printf("112\n"),0;
	for(int i=0;i<(1<<n);i++) f[i][1]=1;
	for(int i=2;i<=m;i++)
	for(int j=0;j<(1<<n);j++)
	{
		f[j][i&1]=0;
		for(int k=0;k<(1<<n);k++)
		{
			if(!g[k][j]) continue;
			f[j][i&1]+=f[k][(i-1)&1],f[j][i&1]%=mod;
		}
	}
	ll ans=0;
	for(int i=0;i<(1<<n);i++) ans+=f[i][m&1],ans%=mod;
	printf("%lld\n",ans);
	return 0;
}
