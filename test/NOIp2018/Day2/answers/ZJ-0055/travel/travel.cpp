#include<cstdio>
#include<algorithm>
#include<cstring>
#define N (5000+5)
using namespace std;
struct edge{int to,nxt,rev;}e[2*N];
struct aff{
	int u,v,rev;
	friend bool operator <(aff x,aff y) {return x.u<y.u||x.u==y.u&&x.v>y.v;}
}a[2*N];
int tot,cnt,num,tp,fir[N],ans[N],now[N],vis[N];
int ban[2*N],id[N],dfn[N],low[N],st[N];
void addedge(int u,int v,int r) {e[++tot].to=v;e[tot].nxt=fir[u];e[tot].rev=r;fir[u]=tot;}
void tarjan(int x,int E)
{
	dfn[x]=low[x]=++cnt;
	st[++tp]=x;
	for(int i=fir[x];i;i=e[i].nxt)
	{
		if(e[i].rev==E) continue;
		int v=e[i].to;
		if(!dfn[v])
		{
			tarjan(v,i);
			low[x]=min(low[x],low[v]);
		}
		else low[x]=min(low[x],dfn[v]);
	}
	if(dfn[x]==low[x])
	{
		int y;num++;
		while(y=st[tp--])
		{
			id[y]=num;
			if(x==y) break;
		}
	}
}
void dfs(int x)
{
	now[++tot]=x;
	vis[x]=1;
	for(int i=fir[x];i;i=e[i].nxt)
	if(!vis[e[i].to]&&!ban[i]) dfs(e[i].to);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n,m;scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&a[2*i-1].u,&a[2*i-1].v);
		a[2*i].u=a[2*i-1].v;a[2*i].v=a[2*i-1].u;
	}
	sort(a+1,a+2*m+1);
	if(m<n)
	{
		for(int i=1;i<=2*m;i++) addedge(a[i].u,a[i].v,0);
		tot=0;dfs(1);
		for(int i=1;i<=n;i++) printf("%d ",now[i]);
		return 0;
	}
	for(int i=1;i<2*m;i++)
	for(int j=i+1;j<=2*m;j++)
	if(a[i].u==a[j].v&&a[i].v==a[j].u) a[i].rev=j,a[j].rev=i;
	for(int i=1;i<=2*m;i++) addedge(a[i].u,a[i].v,a[i].rev);
	tarjan(1,0);ans[1]=0x3f3f3f3f;
	for(int i=1;i<=2*m;i++)
	{
		if(id[a[i].u]!=id[a[i].v]) continue;
		ban[i]=ban[e[i].rev]=1;
		memset(vis,0,sizeof(vis));
		tot=0;dfs(1);
		int fg=0;
		for(int j=1;j<=tot;j++)
		{
			if(now[j]<ans[j]) {fg=1;break;}
			if(now[j]>ans[j]) {fg=0;break;}
		}
		if(fg) for(int j=1;j<=n;j++) ans[j]=now[j];
		ban[i]=ban[e[i].rev]=0;
	}
	for(int i=1;i<=n;i++) printf("%d ",ans[i]);
	return 0;
}
