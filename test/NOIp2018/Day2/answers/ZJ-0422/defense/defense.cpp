#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define maxn 100005
#define inf 0x3f3f3f3f
using namespace std;
int n,m;
int head[maxn],tot=0;
struct Edge{
	int t,nxt;
}edge[maxn<<1];
int dp[maxn][2];
int cost[maxn];
int sta[200];
int ans=inf;
int sum=0;
int fa[maxn];
int gmin(int a,int b){
	return a<b?a:b;
}
void add(int st,int to){
	edge[tot].t=to;
	edge[tot].nxt=head[st];
	head[st]=tot;
	++tot;
	return;
}
void dfs(int np,int f){
	fa[np]=f;
	for(int i=head[np];i!=-1;i=edge[i].nxt){
		if(fa[edge[i].t]==-1){
			dfs(edge[i].t,np);
		}
	}
	return;
}
void dfs2(int np,int a,int x,int b,int y){
	if(sum>ans) return;
	if(np>n){
		for(int i=2;i<=n;++i){
			if(sta[i]==0 && sta[fa[i]]==0)
				return;
		} 
		ans=sum;return;
	}
	if(np==a){
		if(x==1){
			sta[np]=1;
			sum+=cost[np];
			dfs2(np+1,a,x,b,y);
			sum-=cost[np];
			sta[np]=0;
		}
		else{
			dfs2(np+1,a,x,b,y);
		}
		return;
	}
	if(np==b){
		if(y==1){
			sta[np]=1;
			sum+=cost[np];
			dfs2(np+1,a,x,b,y);
			sum-=cost[np];
			sta[np]=0;
		}
		else{
			dfs2(np+1,a,x,b,y);
		}
		return;
	}
	sta[np]=1;
	sum+=cost[np];
	dfs2(np+1,a,x,b,y);
	sum-=cost[np];
	sta[np]=0;
	dfs2(np+1,a,x,b,y);
	return;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d %d",&n,&m);
	char P[4];
	scanf("%s",P);
	for(int i=1;i<=n;++i){
		scanf("%d",cost+i);
	}
	memset(head,-1,sizeof(head));
	for(int i=1;i<n;++i){
		int a,b;
		scanf("%d %d",&a,&b);
		add(a,b);
		add(b,a);
	}
	if(P[0]=='A'){
		for(int i=1;i<=m;++i){
			memset(dp,0,sizeof(dp));
			int a,b,x,y;
			scanf("%d %d %d %d",&a,&x,&b,&y);
			if(a==b && x+y==1){
				printf("-1\n");
				continue;
			}
			if(a<b){
				swap(a,b);
				swap(x,y);
			}
			if(a==b+1){
				if(x==0 && y==0){
					printf("-1\n");
					continue;
				}
			}
			for(int i=1;i<=n;++i){
				if(i==a){
					if(x==1){
						dp[i][1]=gmin(dp[i-1][1],dp[i-1][0])+cost[i];
						dp[i][0]=inf;
					}
					else{
						dp[i][0]=dp[i-1][1];
						dp[i][1]=inf;
					}
				}
				else if(i==b){
					if(y==1){
						dp[i][1]=gmin(dp[i-1][1],dp[i-1][0])+cost[i];
						dp[i][0]=inf;
					}
					else{
						dp[i][0]=dp[i-1][1];
						dp[i][1]=inf;
					}
				}
				else{
					dp[i][1]=gmin(dp[i-1][1],dp[i-1][0])+cost[i];
					dp[i][0]=dp[i-1][1];
				}
			}
			printf("%d\n",gmin(dp[n][1],dp[n][0]));
		}
		return 0;
	}
	if(n<=100){
		int a,x,b,y;
		memset(sta,0,sizeof(sta));
		memset(fa,-1,sizeof(fa));
		dfs(1,0);
		for(int i=1;i<=m;++i){
			scanf("%d %d %d %d",&a,&x,&b,&y);
			if(fa[a]==b || fa[b]==a){
				if(x==0 && y==0){
					printf("-1\n");
					continue;
				} 
			}
			ans=inf;
			dfs2(1,a,x,b,y);
			printf("%d\n",ans);
		}
		return 0;
	}
	for(int i=1;i<=m;++i){
		printf("-1\n");
	}
	return 0;
}
