#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<algorithm>
#define maxn 1000006
using namespace std;
int k[8][maxn];
int n,m;
int ans=0;
int check(int a,int b){
	if(k[a][b-1]==0 && k[a-1][b]==1) return 0;
	return 1;
}
void dfs(int a,int b){
	//cout<<a<<' '<<b<<endl;
	if(a==n){
		++ans;
		return;
	}
	k[a][b]=1;
	if(a && b){
		if(!check(a,b)){
			return;
		}
	}
	int p,q;
	q=b+1;
	p=a+q/m;
	q%=m;
	dfs(p,q);
	k[a][b]=0;
	if(a && b){
		if(!check(a,b)){
			return;
		}
	}
	dfs(p,q);
	return;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d %d",&n,&m);
	memset(k,0,sizeof(k));
	dfs(0,0);
	printf("%d\n",ans);
	return 0;
}
