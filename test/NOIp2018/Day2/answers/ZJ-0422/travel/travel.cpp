#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define maxn 5005
using namespace std;
int n,m;
int ans[maxn],res[maxn],tot=0;
int book[maxn];
int to[maxn][maxn],cnt[maxn];
int typ[maxn],totyp=0;
int flag=0;
int ins[maxn];
int stk[maxn],top=0;
int t1,t2;
int gmin(int a,int b){
	return a<b?a:b;
}
void tarjan(int np,int fa){
	//cout<<np<<endl;
	stk[top]=np;
	ins[np]=1;
	book[np]=1;
	++top;
	for(int i=0;i<cnt[np];++i){
		if(flag) return;
		if(ins[to[np][i]] && to[np][i]!=fa){
			flag=to[np][i];
			return;
		}
		if(!book[to[np][i]]){
			tarjan(to[np][i],np);
		}
	}
	if(flag) return;
	ins[np]=0;
	--top;
	return;
}
void dfs(int np,int &tmp){
	//cout<<np<<endl;
	res[tot]=np;
	book[np]=1;
	++tot;
	if(tmp==0 && res[tot-1]>ans[tot-1]) return;
	if(tmp==0 && res[tot-1]<ans[tot-1]) tmp=1;
	for(int i=0;i<cnt[np];++i){
		//cout<<np<<'p'<<to[np][i]<<endl;
		if((np==t1 && to[np][i]==t2)|| (np==t2 && to[np][i]==t1)){
			continue;
		}
		if(!book[to[np][i]]){
			dfs(to[np][i],tmp);
		}
	}
	return;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travle.out","w",stdout);
	scanf("%d %d",&n,&m);
	memset(cnt,0,sizeof(cnt));
	for(int i=1;i<=m;++i){
		int a,b;
		scanf("%d %d",&a,&b);
		to[a][cnt[a]]=b;
		++cnt[a];
		to[b][cnt[b]]=a;
		++cnt[b];
	}
	for(int i=1;i<=n;++i){
		sort(to[i],to[i]+cnt[i]);
	}
	//for(int i=1;i<=n;++i){
	//	for(int j=0;j<cnt[i];++j){
	//		cout<<to[i][j]<<' ';
	//	}cout<<endl;
	//}
	if(n==m){
		memset(book,0,sizeof(book));
		tarjan(1,0);
		//cout<<flag<<endl;
		typ[totyp]=flag;
		++totyp;
		while(stk[top]!=flag){
			if(stk[top]!=0){
				typ[totyp]=stk[top];
				++totyp;
			}
			--top;
		}
		typ[totyp]=flag;
		//for(int i=0;i<=totyp;++i){
		//	cout<<typ[i]<<' ';
		//}cout<<endl;
		memset(ans,0x3f,sizeof(ans));
		for(int i=0;i<totyp;++i){
			t1=typ[i],t2=typ[i+1];
			//cout<<t1<<'p'<<t2<<endl;
			memset(book,0,sizeof(book));
			int p=0;
			tot=0;
			dfs(1,p);
			if(p==1 && tot==n){
				//cout<<t1<<'a'<<t2<<endl;
				for(int i=0;i<tot;++i){
					ans[i]=res[i];
				}
				//for(int i=0;i<tot;++i){
				//	cout<<ans[i]<<' ';
				//}cout<<endl;
			}
		}
	}
	else{
		t1=-1,t2=-1;
		memset(book,0,sizeof(book));
		memset(ans,0x3f,sizeof(ans));
		int p=1;
		dfs(1,p);
		for(int i=0;i<n;++i){
			ans[i]=res[i];
		}
	}
	for(int i=0;i<n;++i){
		printf("%d ",ans[i]);
	}
	printf("\n");
	return 0;
}
