#include<bits/stdc++.h>
using namespace std;

#define FOR(i,s,t) for(int i=(s),_t=(t); i<=_t; ++i)
#define DOR(i,s,t) for(int i=(s),_t=(t); i>=_t; --i)
#define EOR(i,x) for(int i=Head[x]; ~i; i=Nxt[i])

typedef long long ll;

const int M=(int)1e5+50;

int Head[M],E[M<<1],Nxt[M<<1],tol;
void Edge(int x,int y) {
	E[tol]=y;Nxt[tol]=Head[x];Head[x]=tol++;
}
ll val[M];
int n,m;
namespace P_1{
	int ff[M];
	ll dp[M][2];
	void dfs(int x,int fr) {
		dp[x][0]=0;
		dp[x][1]=val[x];
		EOR(i,x) if(E[i]!=fr) {
			int y=E[i];
			dfs(y,x);
			dp[x][0]+=dp[y][1];
			dp[x][1]+=min(dp[y][0],dp[y][1]);
		}
		if(ff[x]==0) {
			dp[x][1]=(ll)2e12;
		} else if(ff[x]==1) {
			dp[x][0]=(ll)2e12;
		}
	}
	void solve() {
		memset(ff,-1,sizeof(ff));
		FOR(i,1,m) {
			int x,a,y,b;
			scanf("%d%d%d%d",&x,&a,&y,&b);
			ff[x]=a;ff[y]=b;
			dfs(1,0);
			ll res=min(dp[1][0],dp[1][1]);
			if(res>=(ll)2e12) res=-1;
			printf("%lld\n",res);
			ff[x]=ff[y]=-1;
		}
	}
}
namespace P_2{
	ll A[M][2],B[M][2];
	void solve() {
		FOR(i,1,n) {
			A[i][0]=A[i-1][1];
			A[i][1]=min(A[i-1][0],A[i-1][1])+val[i];
		}
		DOR(i,n,1) {
			B[i][0]=B[i+1][1];
			B[i][1]=min(B[i+1][0],B[i+1][1])+val[i];
		}
		FOR(i,1,m) {
			int x,y,a,b;
			scanf("%d%d%d%d",&x,&a,&y,&b);
			if(x>y) {
				swap(x,y);
				swap(a,b);
			}
			if(!a && !b) puts("-1");
			else printf("%lld\n",A[x][a]+B[y][b]);
		}
	}
}
namespace P_3{
	ll A[M][2],B[M][2],C[M][2];
	void solve() {
		A[1][0]=(ll)2e12;A[1][1]=val[1];
		FOR(i,2,n) {
			A[i][0]=A[i-1][1];
			A[i][1]=min(A[i-1][0],A[i-1][1])+val[i];
		}
		DOR(i,n,1) {
			B[i][0]=B[i+1][1];
			B[i][1]=min(B[i+1][0],B[i+1][1])+val[i];
		}
		FOR(i,1,m) {
			int x,y,a,b;
			scanf("%d%d%d%d",&x,&a,&y,&b);
			ll res=B[y][b];
			if(b==1) {
				res+=min(A[y-1][0],A[y-1][1]);
			} else {
				res+=A[y-1][1];
			}
			if(res>=2e12) res=-1;
			printf("%lld\n",res);
		}
	}
}
namespace P_4{
	ll dp[M][2];
	int Fa[M];
	void dfs(int x,int fr) {
		Fa[x]=fr;
		dp[x][0]=0;
		dp[x][1]=val[x];
		EOR(i,x) if(E[i]!=fr) {
			int y=E[i];
			dfs(y,x);
			dp[x][0]+=dp[y][1];
			dp[x][1]+=min(dp[y][0],dp[y][1]);
		}
	}
	void solve() {
		dfs(1,0);
		FOR(i,1,m) {
			int x,a,y,b;
			scanf("%d%d%d%d",&x,&a,&y,&b);
			ll f0=dp[y][0],f1=dp[y][1];
			if(b==0) f1=(ll)2e12;
			else f0=(ll)2e12;
			ll aa,bb;
			while(Fa[y]!=x) {
				aa=dp[Fa[y]][0];
				bb=dp[Fa[y]][1];
				aa-=dp[y][1];
				bb-=min(dp[y][0],dp[y][1]);
				aa+=f1;
				bb+=min(f0,f1);
				f0=aa;
				f1=bb;
				y=Fa[y];
			}
			ll res=dp[1][1]-min(dp[y][1],dp[y][0])+min(f0,f1);
			if(res>=2e12) res=-1;
			printf("%lld\n",res);
		}
	}
}
char Type[10];
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	
	memset(Head,-1,sizeof(Head));
	scanf("%d%d%s",&n,&m,Type);
	FOR(i,1,n) {
		scanf("%lld",&val[i]);
	}
	FOR(i,2,n) {
		int x,y;
		scanf("%d%d",&x,&y);
		Edge(x,y);
		Edge(y,x);
	}
	if(n<=2000)
		P_1::solve();
	else if(Type[0]=='A' && Type[1]=='2') 
		P_2::solve();
	else if(Type[0]=='A' && Type[1]=='1')
		P_3::solve();
	else if(Type[0]=='B' && Type[1]=='1')
		P_4::solve();
	else 
		P_1::solve();
	return 0;
}
