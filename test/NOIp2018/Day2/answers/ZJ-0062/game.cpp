#include<bits/stdc++.h>
using namespace std;

#define FOR(i,s,t) for(int i=(s),_t=(t); i<=_t; ++i)
#define DOR(i,s,t) for(int i=(s),_t=(t); i>=_t; --i)

typedef long long ll;

const int Mod=(int)1e9+7;

int n,m;
namespace P_1{
	void solve() {
		ll res=1;
		FOR(i,1,m) {
			res=res*2%Mod;
		}
		printf("%lld\n",res);
	}
}
namespace P_2{
	struct Mat{
		ll a[300][300];
		Mat () {memset(a,0,sizeof(a));}
		Mat operator *(const Mat &s) const{
			Mat t;
			for(int i=0; i<(1<<n); ++i) {
				for(int j=0; j<(1<<n); ++j) if(a[i][j]) {
					for(int k=0; k<(1<<n); ++k) if(s.a[j][k]) {
						t.a[i][k]=(t.a[i][k]+a[i][j]*s.a[j][k])%Mod;
					}
				}
			}
			return t;
		}
	}f,g;
	
	int val[2][9];
	int mn;
	bool dfs(int x,int y,int s) {
		s=(s<<1)|val[x][y];
		if(x==1 && y==n-1) {
			if(mn==-1) mn=s;
			if(s>mn) return false;
			else {
				mn=s;
				return true;	
			}
		}
		if(x<1) {
			if(!dfs(x+1,y,s)) {
				return false;
			}
		}
		if(y<n-1) {
			if(!dfs(x,y+1,s)) {
				return false;
			}
		}
		return true;
	}
	void solve() {
		for(int i=0; i<(1<<n); ++i) {
			FOR(j,0,n-1) {
				if(i&(1<<j)) {
					val[0][j]=true;
				} else {
					val[0][j]=false;
				}
			}
			for(int j=0; j<(1<<n); ++j) {
				FOR(k,0,n-1) {
					if(j&(1<<k)) {
						val[1][k]=true;
					} else {
						val[1][k]=false;
					}
				}
				mn=-1;
				g.a[i][j]=dfs(0,0,0);
			}
		}
		FOR(i,0,(1<<n)-1) {
			f.a[0][i]=1;
		}
		for(--m; m; m>>=1,g=g*g) if(m&1) f=f*g;
		ll res=0;
		FOR(i,0,(1<<n)-1) {
			res=(res+f.a[0][i])%Mod;
		}
		printf("%lld\n",res);
	}
}
namespace P_3{
	void solve() {
		if(m==1) puts("8");
		else if(m==2) puts("36");
		else if(m==3) puts("112");
		else {
			ll res=112;
			FOR(i,4,m) {
				res=res*3%Mod;
			}
			printf("%lld\n",res);
		}
	}
}
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	if(n==1) 
		P_1::solve();
	else if(n==2) 
		P_2::solve();
	else if(n==3) 
		P_3::solve();
	else 
		P_2::solve();
	return 0;
}
