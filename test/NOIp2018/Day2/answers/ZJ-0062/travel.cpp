#include<bits/stdc++.h>
using namespace std;

#define FOR(i,s,t) for(int i=(s),_t=(t); i<=_t; ++i)
#define DOR(i,s,t) for(int i=(s),_t=(t); i>=_t; --i)

const int M=(int)5050;

struct edge{
	int x,y;
	bool operator <(const edge &s) const{
		return x<s.x;
	}
};
vector<edge> E[M];

int n,m;
namespace P_1{
	int Ans[M],tid;
	void dfs(int x,int fr) {
		Ans[++tid]=x;
		for(int i=0; i<(int)E[x].size(); ++i) {
			if(E[x][i].x!=fr) dfs(E[x][i].x,x);
		}
	}
	void solve() {
		dfs(1,0);
		FOR(i,1,n) {
			printf("%d",Ans[i]);
			if(i!=n) putchar(' ');
		}
		putchar('\n');
	}
}
namespace P_2{
	int del_id;
	int Ans[M];
	int Tmp[M],tid;
	int vis[M];
	void dfs(int x,int fr) {
		if(vis[x]==del_id) return ;
		vis[x]=del_id;
		Tmp[++tid]=x;
		for(int i=0; i<(int)E[x].size(); ++i) {
			if(E[x][i].x!=fr && E[x][i].y!=del_id) {
				dfs(E[x][i].x,x);
			}
		}
	}
	bool Check() {
		if(!Ans[1]) return true;
		FOR(i,1,n) {
			if(Ans[i]!=Tmp[i]) {
				return Ans[i]>Tmp[i];
			}
		}
		return false;
	}
	void solve() {
		FOR(i,1,m) {
			del_id=i;
			tid=0;
			dfs(1,0);
			if(tid==n) {
				if(Check()) {
					FOR(j,1,n) {
						Ans[j]=Tmp[j];
					}
				}
			}
		}
		FOR(i,1,n) {
			printf("%d",Ans[i]);
			if(i!=n) putchar(' ');
		}
		putchar('\n');
	}
}

int main() {
	freopen("travel.in","r",stdin); 
	freopen("travel.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	FOR(i,1,m) {
		int x,y;
		scanf("%d%d",&x,&y);
		E[x].push_back((edge){y,i});
		E[y].push_back((edge){x,i});
	}
	FOR(i,1,n) {
		sort(E[i].begin(),E[i].end());
	}
	if(m==n-1) 
		P_1::solve();
	else 
		P_2::solve();
	return 0;
}
