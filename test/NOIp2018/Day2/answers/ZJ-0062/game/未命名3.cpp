#include<bits/stdc++.h>
using namespace std;

#define FOR(i,s,t) for(int i=(s),_t=(t); i<=_t; ++i)
#define DOR(i,s,t) for(int i=(s),_t=(t); i>=_t; --i)

typedef long long ll;

const int Mod=(int)1e9+7;

int n,m;
int Id(int x,int y) {
	return (x-1)*m+y-1;
}
int val[10][10];
int mn;
bool dfs(int x,int y,int s) {
	s=(s<<1)|val[x][y];
	if(x==n && y==m) {
		if(mn==-1) mn=s;
		if(s>mn) return false;
		else {
			mn=s;
			return true;	
		}
	}
	if(x<n) {
		if(!dfs(x+1,y,s)) {
			return false;
		}
	}
	if(y<m) {
		if(!dfs(x,y+1,s)) {
			return false;
		}
	}
	return true;
}
int main() {
//	freopen("__.out","w",stdout);
	FOR(nn,1,6) {
		FOR(mm,1,nn) {
			n=nn,m=mm;
			int res=0;
			FOR(i,0,(1<<(Id(n,m)+1))-1) {
				FOR(j,1,n) {
					FOR(k,1,m) {
						if((1<<Id(j,k))&i) {
							val[j][k]=true;
						} else {
							val[j][k]=false;
						}
					}
				}
				mn=-1;
				if(!dfs(1,1,0)) {
		//			mn=-1;
		//			if(dfs(2,1,0)) {
		//				n=2;
		//				mn=-1;
		//				if(dfs(1,1,0)) {
		//					FOR(j,1,n+1) {
		//						FOR(k,1,m) {
		//							printf("%d",val[j][k]);
		//						}
		//						putchar('\n');
		//					}
		//					putchar('\n');
		//					++res;
		//				}
		//				n=3;
		//			}
				} else {
					++res;
				}
			}
			printf("%d ",res);
		}
		putchar('\n');
	}
	return 0;
}
