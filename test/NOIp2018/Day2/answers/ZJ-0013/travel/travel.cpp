#include<bits/stdc++.h>
using namespace std;
const int N=5002;
int n,m,rec[N],vis[N],tot=1;
vector<int> g[N];
void dfs(int u){
	if(tot==n){
		for(int i=1;i<=n;i++) printf("%d ",rec[i]);
		exit(0);
	}
	for(int i=0;i<g[u].size();i++){
		int v=g[u][i];
		if(!vis[v]){
			rec[++tot]=v; vis[v]=1; dfs(v);
		}
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int u,v; scanf("%d%d",&u,&v);
		g[u].push_back(v); g[v].push_back(u);
	}
	for(int i=1;i<=n;i++) sort(g[i].begin(),g[i].end());
	if(m==n-1){
		vis[1]=1; rec[1]=1; dfs(1);
	}
	for(int i=1;i<=n;i++) printf("%d ",i);
return 0;
}
