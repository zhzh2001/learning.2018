#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int maxn=100000+10;
ll n,m,head[maxn],to[maxn<<1],nxt[maxn<<1],tot;
ll val[maxn],dp[maxn][2],a,u,b,v,flag;char opt[5];

inline ll read(){
	register ll x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return (f==1)?x:-x;
}
inline void add(ll x,ll y){
	to[++tot]=y;
	nxt[tot]=head[x];
	head[x]=tot;
}

void dfs(ll x,ll fa){
	dp[x][1]=val[x];dp[x][0]=0;
	for(ll i=head[x],y;i;i=nxt[i]){
		y=to[i];
		if(y==fa) continue;
		dfs(y,x);
	}
	if(a==x&&u==1){
		for(ll i=head[x],y;i;i=nxt[i]){
			y=to[i];
			if(y==fa) continue;
			if(b==y&&v==1) dp[x][1]+=dp[y][1];
			else if(b==y&&v==0) dp[x][1]+=dp[y][0];
			else dp[x][1]+=min(dp[y][0],dp[y][1]);
		}
	}
	else if(a==x&&u==0){
		for(ll i=head[x],y;i;i=nxt[i]){
			y=to[i];
			if(y==fa) continue;
			if(b==y&&v==0) flag=1;
			else dp[x][0]+=dp[y][1];
		}
	}
	else if(b==x&&v==1){
		for(ll i=head[x],y;i;i=nxt[i]){
			y=to[i];
			if(y==fa) continue;
			if(a==y&&u==1) dp[x][1]+=dp[y][1];
			else if(a==y&&u==0) dp[x][1]+=dp[y][0];
			else dp[x][1]+=min(dp[y][0],dp[y][1]);
		}
	}
	else if(b==x&&v==0){
		for(ll i=head[x],y;i;i=nxt[i]){
			y=to[i];
			if(y==fa) continue;
			if(a==y&&u==0) flag=1;
			else dp[x][0]+=dp[y][1];
		}
	}
	else {
		for(ll i=head[x],y;i;i=nxt[i]){
			y=to[i];
			if(y==fa) continue;
			if(a==y) dp[x][1]+=dp[y][u];
			else if(b==y) dp[x][1]+=dp[y][v];
			else dp[x][1]+=min(dp[y][0],dp[y][1]);
			if(a!=y&&b!=y) dp[x][0]+=dp[y][1];
			else if(a==y&&u==1) dp[x][0]+=dp[y][1];
			else if(b==y&&v==1) dp[x][0]+=dp[y][1];
		}
	}
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();
	scanf("%s",opt);
	for(ll i=1;i<=n;i++)
		scanf("%d",&val[i]);
	ll x,y;
	for(ll i=1;i<n;i++){
		x=read(),y=read();
		add(x,y);add(y,x);
	}
	for(ll i=1;i<=m;i++){
		a=read(),u=read(),b=read(),v=read();
		flag=0;dfs(1,0);
		if(flag==1) printf("-1\n");
		else {
			if(a==1) printf("%lld\n",dp[1][u]);
			else if(b==1) printf("%lld\n",dp[1][v]);
			else printf("%lld\n",min(dp[1][0],dp[1][1]));
		}
		/*printf("\n");
		for(ll j=1;j<=n;j++)
			printf("dp[%lld][0]=%lld,dp[%lld][1]=%lld\n",j,dp[j][0],j,dp[j][1]);
		printf("\n");*/
	}
	return 0;
}
