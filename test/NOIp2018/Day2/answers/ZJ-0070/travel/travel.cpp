#include <bits/stdc++.h>
#define res register int
using namespace std;
const int maxn=10000+10;
int n,m,x[maxn],y[maxn],vis[maxn],a[maxn],b[maxn],cnt,nx,ny;

vector<int> G[maxn];

inline int read(){
	res x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return (f==1)?x:-x;
}

void dfs(res x){
	b[++cnt]=x;
	vis[x]=1;
	res y,siz=G[x].size();
	for(int i=0;i<siz;i++){
		y=G[x][i];
		if((x==nx&&y==ny)||(x==ny&&y==nx)) continue;
		if(vis[y]) continue;
		dfs(y);
	}
}

int check(){
	if(a[1]==0) return 1;
	for(res i=1;i<=n;i++)
		if(a[i]!=b[i]) return a[i]>b[i];
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	res i,j;
	for(i=1;i<=m;i++){
		x[i]=read(),y[i]=read();
		G[x[i]].push_back(y[i]);
		G[y[i]].push_back(x[i]);
	}
	for(i=1;i<=n;i++)
		sort(G[i].begin(),G[i].end());
	if(m==n-1){
		cnt=0;dfs(1);
		for(i=1;i<=n;i++)
			a[i]=b[i];
	}
	else {
		for(i=1;i<=m;i++){
			memset(vis,0,sizeof(vis));
			nx=x[i],ny=y[i];
			cnt=0;
			dfs(1);
			if(cnt==n&&check()){
				for(j=1;j<=n;j++)
					a[j]=b[j];
			}
		}
	}
	for(i=1;i<=n;i++)
		printf("%d ",a[i]);
	printf("\n");
	return 0;
}
