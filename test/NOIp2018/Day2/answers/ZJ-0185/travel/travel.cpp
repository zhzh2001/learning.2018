#include<bits/stdc++.h>
using namespace std;
#define M 5005
vector<int>E[M];
int n,m;
int fa[M];
int Rt;
int getfa(int v){return fa[v]==v?v:fa[v]=getfa(fa[v]);}
struct P1{//for tree
	int A[M],len;
	void dfs(int x,int f){
		A[++len]=x;
		for(int i=0;i<E[x].size();i++){
			int y=E[x][i];
			if(y==f)continue;
			dfs(y,x);	
		}
	}
	void solve(){
		dfs(1,0);
		for(int i=1;i<len;i++)
			printf("%d ",A[i]);
		printf("%d\n",A[len]);
	}	
}p1;
struct P2{//for circle
	int stk[M],top;
	bool in[M],flag;
	int pa,pb,len;
	int A[M],B[M];
	void dfs(int x,int f){
		stk[++top]=x;
		in[x]=1;
		for(int i=0;i<E[x].size()&&!flag;i++){
			int y=E[x][i];
			if(y==f)continue;
			if(in[y]){
				flag=1;
				break;	
			}
			dfs(y,x);
		}
		if(flag)return;
		top--;in[x]=0;
	}
	void redfs(int x,int f){
		B[++len]=x;
		for(int i=0;i<E[x].size();i++){
			int y=E[x][i];
			if(y==f||x==pa&&y==pb||x==pb&&y==pa)continue;
			redfs(y,x);	
		}
	}
	void Update(){
		for(int i=1;i<=n;i++)
			if(A[i]!=B[i]){
				if(A[i]==0)break;
				if(A[i]<B[i])return;
				if(B[i]<A[i])break;
			}
		for(int i=1;i<=n;i++)A[i]=B[i];
	}
	void solve(){
		top=0;
		dfs(Rt,0);
		for(int i=1;i<=top;i++){
			pa=stk[i];
			pb=stk[i-1?i-1:top];
			len=0;
			redfs(1,0);
			Update();	
		}
		for(int i=1;i<n;i++)
			printf("%d ",A[i]);
		printf("%d\n",A[n]);
	}
}p2;
int main(){//name memory long long * mod - 切分判断 最值计算 初值 
//	printf("%.2lf\n",(sizeof(p1)+sizeof(p2))/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int x,y;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)fa[i]=i;
	for(int i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		E[x].push_back(y);
		E[y].push_back(x);
		if(getfa(x)==getfa(y)){
			Rt=x;
			continue;
		}
		fa[fa[x]]=fa[y];
	}
	for(int i=1;i<=n;i++)
		sort(E[i].begin(),E[i].end());
	if(m==n-1)p1.solve();
	else p2.solve();
	return 0;
}
