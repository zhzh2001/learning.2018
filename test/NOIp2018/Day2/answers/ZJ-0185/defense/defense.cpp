#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define M 100005
char tp[10];
const LL Inf=1e16;
struct List{
	int to,nxt;
}lt[M<<1];
int Hd[M],e_tot;
int val[M],n,m;
void Add_edge(int u,int v){
	lt[++e_tot]=(List){v,Hd[u]};
	Hd[u]=e_tot;
}
struct P1{//n<=2000
	#define Mn 2005
	LL dp[Mn][2];
	int px,py,sx,sy;
	bool Lik[Mn][Mn];
	void dfs(int x,int f){
		LL sum1=0,sum0=0;
		dp[x][1]=val[x];
		dp[x][0]=0;
		for(int i=Hd[x];i;i=lt[i].nxt){
			int y=lt[i].to;
			if(y==f)continue;
			dfs(y,x);
			dp[x][1]+=min(dp[y][0],dp[y][1]);
			dp[x][0]+=dp[y][1];	
		}
		if(px==x){
			dp[x][!sx]=Inf;
		}else if(py==x){
			dp[x][!sy]=Inf;
		}	
	}
	void dfs_init(int x,int f){
		Lik[x][f]=Lik[f][x]=1;
		for(int i=Hd[x];i;i=lt[i].nxt){
			int y=lt[i].to;
			if(y==f)continue;
			dfs_init(y,x);
		}
	}
	void solve(){
		dfs_init(1,0);
		for(int i=1;i<=m;i++){
			scanf("%d%d%d%d",&px,&sx,&py,&sy);
			if(!sx&&!sy&&Lik[px][py])puts("-1");
			else{
				memset(dp,63,sizeof(dp));
				dfs(1,0);
				printf("%lld\n",min(dp[1][0],dp[1][1]));
			}
		}
	}
}p1;
int main(){//name memory long long * mod - 切分判断 最值计算 初值 
//	printf("%.2lf\n",(sizeof(p1))/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,tp+1);
	for(int i=1;i<=n;i++)
		scanf("%d",&val[i]);
	int x,y;
	for(int i=1;i<n;i++){
		scanf("%d%d",&x,&y);
		Add_edge(x,y);
		Add_edge(y,x);	
	}
	p1.solve();
	return 0;
}
