#include<bits/stdc++.h>
using namespace std;
#define LL long long
const int P=1e9+7;
int n,m;
LL Pow(int a,int b){
	LL ans=1,bse=a;
	while(b){
		if(b&1)ans=ans*bse%P;
		bse=bse*bse%P;
		b>>=1;
	}
	return ans;
}
struct P1{//n==2 || n==3
	void solve(){
		if(n==2){
			printf("%lld\n",Pow(3,m-1)*4%P);
		}else{
			if(m==1)puts("8");
			else if(m==2)puts("36");
			else printf("%lld\n",112*Pow(3,m-3)%P);
		}
	}	
}p1;
struct P2{//n<=3&&m<=3
	#define Mn 2005
	int C[Mn],ans,c0;
	char mp[10][10];
	void Add(int x){
		if(x==0){
			c0++;
			return;
		}
		while(x<=1500){
			C[x]++;
			x+=x&-x;
		}
	}
	int Sum(int x){
		int res=0;
		while(x){
			res+=C[x];
			x&=x-1;
		}
		return res;
	}
	struct Pth{
		string W;
		int S;
		bool operator<(const Pth &x)const{
			return W>x.W;	
		}
	}A[Mn];
	int cnt;
	void redfs(int x,int y,string w,int st){
		if(x==n&&y==m){
			A[++cnt]=(Pth){w,st};
			return;
		}
		if(y+1<=m)redfs(x,y+1,w+'R',(st<<1)|(mp[x][y+1]^48));
		if(x+1<=n)redfs(x+1,y,w+'D',(st<<1)|(mp[x+1][y]^48));
	}
	bool check(){
		cnt=c0=0;
		string s="";
		redfs(1,1,s,mp[1][1]^48);
		sort(A+1,A+1+cnt);
		memset(C,0,sizeof(C));
		for(int i=1;i<=cnt;i++){
			if(Sum(A[i].S)+c0!=i-1)return 0;
			Add(A[i].S);
		}
		return 1;
	}
	void dfs(int x,int y){
		if(x==n+1){
			ans+=check();
			return;
		}
		if(y+1<=m){
			mp[x][y]='1';
			dfs(x,y+1);
			mp[x][y]='0';
			dfs(x,y+1);
		}else{
			mp[x][y]='1';
			dfs(x+1,1);
			mp[x][y]='0';
			dfs(x+1,1);
		}
	}
	void solve(){
		dfs(1,1);
		printf("%d\n",ans);
	}
}p2; 
int main(){//name memory long long * mod - 切分判断 最值计算 初值 
//	printf("%.2lf\n",(sizeof(p2))/1024.0/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==2||n==3)p1.solve();
	else if(n<=3&&m<=3)p2.solve();
	return 0;
}
