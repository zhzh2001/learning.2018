#include<cstdio>
#include<cstring>
using namespace std;
typedef long long ll;
const int N=100005,inf=99999999;
template<class T>void read(T &x){
	int f=0;x=0;char ch=getchar();
	while(ch>'9'||ch<'0')f=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+(ch^48),ch=getchar();
	x=f? -x:x;
}
struct E{int nxt,to;}e[N<<1];
int n,m,w[N],head[N],ednum,op[N];char tp[10];ll f[N][2];bool dekinai;
inline void adde(int from,int to){
	e[++ednum].nxt=head[from];
	e[ednum].to=to;head[from]=ednum;
}
inline ll min(ll x,ll y){return x<y? x:y;}
void dp(int x,int fa){
	f[x][0]=0;f[x][1]=w[x];bool flag=0;
	for(int i=head[x];i;i=e[i].nxt){
		int y=e[i].to;
		if(y==fa)continue;
		dp(y,x);
		if(op[y]==0)flag=1;
		f[x][0]+=f[y][1];
		if(op[x]!=0)f[x][1]+=min(f[y][0],f[y][1]);
		else f[x][1]+=f[y][1];
	}
	if(flag&&op[x]==0)dekinai=1;
	if(op[x]==1||flag)f[x][0]=f[x][1];
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n);read(m);scanf("%s",tp);
	int i,x,y,opx,opy;
	memset(op,-1,sizeof(op));
	for(i=1;i<=n;i++)read(w[i]);
	for(i=1;i<n;i++){
		read(x);read(y);
		adde(x,y);adde(y,x);
	}
	for(i=1;i<=m;i++){
		read(x);read(opx);read(y);read(opy);
		op[x]=opx;op[y]=opy;dekinai=0;
		dp(1,0);
		if(dekinai)printf("-1\n");
		else if(op[1]==0)printf("%lld\n",f[1][0]);
		else printf("%lld\n",min(f[1][0],f[1][1]));
		op[x]=-1;op[y]=-1;
	}
	return 0;
}
