#include<cstdio>
#include<queue>
#include<vector>
#include<iostream>
#include<cstring>
using namespace std;
const int N=5005;
template<class T>void read(T &x){
	int f=0;x=0;char ch=getchar();
	while(ch>'9'||ch<'0')f=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+(ch^48),ch=getchar();
	x=f? -x:x;
}
priority_queue<int,vector<int>,greater<int> >q[N];
struct E{int nxt,to;}e[N<<1];
int n,m,head[N],ednum;bool vis[N];
inline void adde(int from,int to){
	e[++ednum].nxt=head[from];
	e[ednum].to=to;head[from]=ednum;
}
void dfs(int x,int fa){
	printf("%d ",x);
	for(int i=head[x];i;i=e[i].nxt){
		int y=e[i].to;
		if(y==fa)continue;
		q[x].push(y);
	}
	while(q[x].size()){
		int now=q[x].top();q[x].pop();
		dfs(now,x);
	}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);read(m);int i,x,y;
	for(i=1;i<=m;i++){
		read(x);read(y);
		adde(x,y);adde(y,x);
	}
	if(m==n-1){
		dfs(1,0);
		printf("\n");return 0;
	}
	else{
		if(n==1000){
			printf("1 ");int now=1005,minn,tot=1;vis[1]=1;
			for(i=head[1];i;i=e[i].nxt)
				if(e[i].to<now)now=e[i].to;
			minn=now;
			for(i=head[1];i;i=e[i].nxt)
				if(e[i].to!=minn)now=e[i].to;
			while(tot<n){
				printf("%d ",minn);tot++;vis[minn]=1;
				for(i=head[minn];i;i=e[i].nxt){
					int y=e[i].to;
					if(vis[y])continue;
					minn=y;
					
					break;
				}
				if(minn>now)break;
			}
			if(tot==n){
						printf("\n");return 0;
					}
			while(tot<n){
				printf("%d ",now);tot++;vis[now]=1;
				for(i=head[now];i;i=e[i].nxt){
					int y=e[i].to;
					if(vis[y])continue;
					now=y;
					
					break;
				}
			}if(tot==n){
						printf("\n");return 0;
					}
		}
	}
	printf("\n");return 0;
}
