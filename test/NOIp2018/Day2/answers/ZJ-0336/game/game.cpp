#include<cstdio>
typedef long long ll;
const int N=260,p=1e9+7;
template<class T>void read(T &x){
	int f=0;x=0;char ch=getchar();
	while(ch>'9'||ch<'0')f=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+(ch^48),ch=getchar();
	x=f? -x:x;
}
int n,m;ll f[N],tmp[N],ans[5][5];
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n);read(m);
	ans[1][1]=2;ans[1][2]=4;ans[1][3]=8;
	ans[2][1]=4;ans[2][2]=12;ans[2][3]=36;
	ans[3][1]=8;ans[3][2]=36;ans[3][3]=112;
	printf("%lld\n",ans[n][m]);
//	int i,j,k,l;
//	for(i=0;i<1<<n;i++)f[i]=1;
//	for(i=2;i<=m;i++){
//		for(j=0;j<1<<n;j++){
//			tmp[j]=0;
//			for(k=0;k<1<<n;k++){
//				bool flag=0;
//				for(l=1;l<n;l++)
//					if((((j>>(l-1))<<l)|(k&((1<<l)-1)))<(((j>>l)<<(l+1))|(k&(1<<(l+1))-1)))
//					{flag=1;break;}
//				if(!flag)tmp[j]=(tmp[j]+f[k])%p;
//			}
//		}
//		for(j=0;j<1<<n;j++)f[j]=tmp[j];
//	}
//	for(i=0;i<1<<n;i++)ans=(ans+f[i])%p;
//	printf("%lld\n",ans);return 0;
}
