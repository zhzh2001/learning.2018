#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=2e5+5;
const ll inf=1e12;
int n,m,K1,K2,V1,V2;
char ty[11];
int w[N];
int hd[N],nxt[N],to[N],tot;
ll dp[N][2];
void add(int a,int b) {
	to[++tot]=b,nxt[tot]=hd[a],hd[a]=tot;
}
void dfs(int u,int f) {
	dp[u][0]=0;
	dp[u][1]=w[u];
	if (K1==u) {
		dp[u][V1^1]=inf;
	}
	if (K2==u) {
		dp[u][V2^1]=inf;
	}
	for (int i=hd[u];i;i=nxt[i]) {
		if (to[i]==f) continue;
		dfs(to[i],u);
		dp[u][0]+=dp[to[i]][1];
		dp[u][1]+=min(dp[to[i]][0],dp[to[i]][1]);
	}
}
int main() {
freopen("defense.in","r",stdin);
freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,ty);
	for (int i=1;i<=n;++i) scanf("%d",w+i);
	for (int i=1,x,y;i<n;++i) {
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	while (m--) {
		scanf("%d%d%d%d",&K1,&V1,&K2,&V2);
		if (n<=2000&&m<=2000) {
			dfs(1,0);
			ll x=min(dp[1][0],dp[1][1]);
			printf("%lld\n",x>=inf?-1:x);
		}
	}
}
