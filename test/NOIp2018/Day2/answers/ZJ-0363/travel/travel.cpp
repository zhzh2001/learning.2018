#include<bits/stdc++.h>
using namespace std;
const int N=10005;
int n,m,vi[N];
vector<int> e[N];
void dfs(int u,int f) {
	printf("%d ",u);
	sort(e[u].begin(),e[u].end());
	for (int i=0;i<e[u].size();++i) {
		if (e[u][i]!=f) dfs(e[u][i],u);
	}
}
void solv2() {
	int p=1,u=e[1][0],v=e[1][1];
	if (u>v) swap(u,v);
	printf("1 %d ",u);
	while (1) {
		int t=e[u][0]==p?e[u][1]:e[u][0];
		if (t<v) {
			printf("%d ",t);
			p=u,u=t;
		}
		else {
			printf("%d ",v);
			p=1;
			while (v!=t) {
				int s=e[v][0]==p?e[v][1]:e[v][0];
				printf("%d ",s);
				p=v,v=s;
			}
			return;
		}
	}
}
int main() {
freopen("travel.in","r",stdin);
freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,x,y;i<=m;++i) {
		scanf("%d%d",&x,&y);
		e[x].push_back(y);
		e[y].push_back(x);
	}
	if (n==m-1) dfs(1,0);
	else solv2();
	puts("");
}
