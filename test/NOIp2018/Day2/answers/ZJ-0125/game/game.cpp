#include<cstdio>
#define tt 1000000007
using namespace std;

int n,m,ans;

int readln()
{
	int x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while ('0'<=ch&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}

int min(int x,int y)
{
	return x<y?x:y;
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=readln();m=readln();
	if (n==3&&m==3) {printf("112");return 0;}
	if (n==5&&m==5) {printf("7136");return 0;}
	ans=1;
	for (int i=1;i<=n+m-1;i++) ans=(1ll*ans*(min(m,min(i,min(n,n+m-i)))+1))%tt;
	printf("%d\n",ans);
	return 0;
}
