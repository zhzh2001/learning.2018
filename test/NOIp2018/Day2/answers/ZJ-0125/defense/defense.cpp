#include<cstdio>
#include<cstring>
#define LL long long
#define INF 10000000000
#define maxn 300005
using namespace std;

int n,m,tot,a,b,c,d;
LL lnk[maxn],f[maxn][3],val[maxn];
char s[15];
struct edge
{
	int nxt,y;
} e[maxn<<1];


LL readln()
{
	LL x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while ('0'<=ch&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}

void add(int x,int y)
{
	e[++tot].nxt=lnk[x];lnk[x]=tot;e[tot].y=y;
	e[++tot].nxt=lnk[y];lnk[y]=tot;e[tot].y=x;
}

LL min(LL x,LL y)
{
	return x<y?x:y;
}

void dfs(int x,int fa)
{
	f[x][0]=0ll;
	f[x][2]=INF;
	f[x][1]=val[x];
	LL rec=INF,ret=0ll;
	int fl=0;
	for (int i=lnk[x];i;i=e[i].nxt)
	{
		int y=e[i].y;
		if (y==fa) continue;
		dfs(y,x);
		f[x][1]+=min(min(f[y][0],f[y][2]),f[y][1]);
		if (f[x][1]>INF) f[x][1]=INF;
		f[x][0]+=f[y][2];if (f[x][0]>INF) f[x][0]=INF;
		if (f[y][1]<=f[y][2]) ret+=f[y][1],fl=1;
		else
		{
			if (f[y][1]-f[y][2]<rec) rec=f[y][1]-f[y][2];
			ret+=f[y][2];
		}
		if (ret>INF) ret=INF;
	}
	if (fl==0) f[x][2]=ret+rec; else f[x][2]=ret;
	if (x==a) {if (b==0ll) f[x][1]=INF; else f[x][2]=f[x][0]=INF;}
	else if (x==c) {if (d==0ll) f[x][1]=INF; else f[x][2]=f[x][0]=INF;}
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=readln();m=readln();scanf("%s",s);
	for (int i=1;i<=n;i++) val[i]=readln();
	for (int i=1;i<n;i++)
	{
		int x=readln(),y=readln();
		add(x,y);
	}
	while (m--)
	{
		a=readln();b=readln();c=readln();d=readln();
		dfs(1,0);
		LL tmp=min(f[1][1],f[1][2]);
		printf("%lld\n",tmp==INF?-1:tmp);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
