#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 5005
using namespace std;

int n,m,tot,cnt,reca,recb,tx,ty,cnt1,cnt2,tott;
int lnk[maxn],ans[maxn],pa[maxn],up[maxn],dep[maxn],q1[maxn],q2[maxn],q[maxn];
bool vis[maxn];
struct edge
{
	int nxt,y;
} e[maxn<<1];

int readln()
{
	int x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while ('0'<=ch&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}

void add(int x,int y)
{
	e[++tot].nxt=lnk[x];lnk[x]=tot;e[tot].y=y;
	e[++tot].nxt=lnk[y];lnk[y]=tot;e[tot].y=x;
}

int getfa(int x)
{
	return pa[x]==x?x:pa[x]=getfa(pa[x]);
}

void dfs(int x,int fa)
{
	up[x]=fa;
	for (int i=lnk[x];i;i=e[i].nxt)
	{
		int y=e[i].y;
		if (y==fa) continue;
		dep[y]=dep[x]+1;
		dfs(y,x);
	}
}

bool cmp(int x,int y)
{
	return x<y;
}

void solve(int x,int fa)
{
	int l=tott+1;
	if (vis[x]) return;
	vis[x]=true;
	ans[++cnt]=x;
	for (int i=lnk[x];i;i=e[i].nxt)
	{
		int y=e[i].y;
		if (y==fa||(x==reca&&y==recb)||(x==recb&&y==reca)) continue;
		q[++tott]=y;
	}
	if (l<=tott) sort(q+l,q+tott+1,cmp);
	for (int i=l;i<=tott;i++) solve(q[i],x);
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=readln();m=readln();tot=0;
	memset(lnk,0,sizeof(lnk));
	for (int i=1;i<=n;i++) pa[i]=i;
	for (int i=1;i<=m;i++)
	{
		int x=readln(),y=readln(),fx=getfa(x),fy=getfa(y);
		if (fx!=fy) pa[fx]=fy,add(x,y);
		else tx=x,ty=y;
	}
	cnt1=cnt2=0;
	q1[0]=q2[0]=0;
	dfs(1,0);
	if (m==n)
	{
		int xx=tx,yy=ty;
		if (dep[xx]<dep[yy]) swap(xx,yy);
		q1[++cnt1]=xx;q2[++cnt2]=yy;
		while (dep[xx]!=dep[yy]) xx=up[xx],q1[++cnt1]=xx;
		while (xx!=yy) xx=up[xx],yy=up[yy],q1[++cnt1]=xx,q2[++cnt2]=yy;
		cnt1--;cnt2--;
		if (q1[cnt1]<q2[cnt2])
		{
			while (cnt1&&q1[cnt1]<q2[cnt2]) cnt1--;
			if (cnt1==0)
			{
				int l=1;
				while (l<cnt2&&q2[l]<q2[cnt2]) l++;
				if (l==1) reca=q2[l],recb=q1[1];
					else reca=q2[l],recb=q2[l-1];
			}
			else reca=q1[cnt1],recb=q1[cnt1+1];
		}
		else
		{
			while (cnt2&&q2[cnt2]<q1[cnt1]) cnt2--;
			if (cnt2==0)
			{
				int l=1;
				while (l<cnt1&&q1[l]<q1[cnt1]) l++;
				if (l==1) reca=q1[l],recb=q2[1];
					else reca=q1[l],recb=q1[l-1];
			}
			else reca=q2[cnt2],recb=q2[cnt2+1];
		}
		add(tx,ty);
	}
	tott=0;cnt=0;solve(1,n);
	for (int i=1;i<=n;i++) printf("%d ",ans[i]);
	fclose(stdin);fclose(stdout);
	return 0;
}
