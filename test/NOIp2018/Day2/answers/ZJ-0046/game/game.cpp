#include <cstdio>
#include <cstring>
const int QAQ=1e9+10;
const int MAXN=1e6+10;
int n,m;
int dp[MAXN][10][10];
inline void mod(int &x)
{
	x-=(x>=QAQ ? QAQ:0);
}
inline int min(int a,int b)
{
	return a<b ? a:b;
}
inline int max(int a,int b)
{
	return a>b ? a:b;
}
inline long long pow(long long x,int n)
{
	long long ans=1;
	while (n)
	{
		if (n&1)
			ans=ans*x%QAQ;
		x=x*x;n>>=1;
	}
	return ans;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1)
	{
		printf("%lld\n",pow(2,m));
		return 0;
	}
	if (n==2)
	{
		printf("%lld\n",pow(3,n+m-3)*4%QAQ);
		return 0;
	}
	for (int i=0;i<=1;i++)
		for (int j=0;j<=2;j++)
			dp[2][j][i]=1;
	for (int i=2;i<m;i++)
	{
		for (int j=0;j<=min(i,n);j++)
			for (int k=0;k<=min(i-1,n);k++)
			{
				dp[i+1][0][j]+=dp[i][j][k],mod(dp[i+1][0][j]);
				dp[i+1][1][j]+=dp[i][j][k],mod(dp[i+1][1][j]);
				if (k+1>1 && k+1<n && k+1<i)
					dp[i+1][k+1][j]+=dp[i][j][k],mod(dp[i+1][k+1][j]);
				for (int t=min(n,i);t<=min(i+1,n);t++)
					dp[i+1][t][j]+=dp[i][j][k],mod(dp[i+1][t][j]);
			}
	}
	for (int i=m,t=1;t<n;i++,t++)
	{
		for (int j=0;j<=n;j++)
			for (int k=0;k<=n;k++)
			{
				dp[i+1][t][j]+=dp[i][j][k],mod(dp[i+1][t][j]);
				if (k+1<n && k+1>t)
					dp[i+1][k+1][j]+=dp[i][j][k],mod(dp[i+1][k+1][j]);
				dp[i+1][n][j]+=dp[i][j][k],mod(dp[i+1][n][j]);
			}
	}
	int ans=0;
	for (int i=0;i<=n;i++)
		for (int j=0;j<=n;j++)
			ans+=dp[m+n-1][i][j],mod(ans);
	printf("%d\n",ans);
}
