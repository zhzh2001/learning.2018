#include <cstdio>
#include <cstring>
const int MAXN=1e5+10;
const int inf=1e9;
int n,q,m,a,b,c,d;
int frst[MAXN],nxt[MAXN<<1],ed[MAXN<<1];
int cost[MAXN];
int f[MAXN][3];
inline int min(int a,int b)
{
	return a<b ? a:b;
}
inline void add(int a,int b)
{
	nxt[++m]=frst[a];
	frst[a]=m;
	ed[m]=b;
}
inline void dfs(int x,int fa)
{
	int cnt=0;
	f[x][0]=f[x][2]=0;
	f[x][1]=cost[x];
	for (int r=frst[x],y=ed[r];r;y=ed[r=nxt[r]])
	{
		cnt++;
		if (y!=fa)
			dfs(y,x);
		int sa=f[x][0],sb=f[x][1],sc=f[x][2];
		if ((y==a && b) ||  (y==c && d))
		{
			f[x][0]=sa+f[y][1];
			f[x][1]=sb+f[y][1];
			f[x][2]=sa+f[y][1];
		}
		else
		{
			if ((y==a && !b) || (y==c && !d))
			{
				f[x][0]=sa+f[y][2];
				f[x][1]=sb+f[y][0];
				f[x][2]=sc+f[y][2];
			}
			else
			{
				f[x][0]=sa+min(f[y][1],f[y][2]);
				f[x][1]=sb+min(f[y][0],f[y][1]);
				f[x][2]=min(sc+f[y][2],sa+f[y][1]);
			}
		}
	}
	if (!cnt)
	{
		f[x][0]=0;
		f[x][1]=cost[x];
		f[x][2]=inf;
	}
	if ((x==a && b) ||  (x==c && d))
		f[x][0]=f[x][2]=inf;
	if ((x==a && !b) || (x==c && !d))
		f[x][1]=inf;
}
inline void print(int x)
{
	if (x>=inf)
		printf("-1\n");
	else
		printf("%d\n",x);
}
inline void solve()
{
	dfs(1,0);
	if ((a==1 && b) || (c==1 && d))
		print(f[0][1]);
	else
		if ((a==1 && !b) || (c==1 && !d))
			print(f[0][2]);
		else
			print(min(f[0][2],f[0][1]));
}
int main()
{
	scanf("%d%d",&n,&q);
	char s[10];
	scanf("%s",s);
	for (int i=1;i<=n;i++)
		scanf("%d",&cost[i]);
	for (int i=1;i<n;i++)
		scanf("%d%d",&a,&b),add(a,b);
	for (int i=1;i<=q;i++)
		scanf("%d%d%d%d",&a,&b,&c,&d),solve();
}
