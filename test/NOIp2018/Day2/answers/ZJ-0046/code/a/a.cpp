#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>
using namespace std;
const int MAXN=5010;
int n,m,e0;
vector<int> s[MAXN];
bool vis[MAXN];
inline void dfs1(int x)
{
	printf("%d ",x);
	vis[x]=true;
	int n=s[x].size();
	for (int i=0;i<n;i++)
		if (!vis[s[x][i]])
			dfs1(s[x][i]);
}
inline void solve1()
{
	memset(vis,false,sizeof(vis));
	dfs1(1);
	printf("\n");
}
int a[MAXN],an,fa[MAXN];
bool circle[MAXN];
int c[MAXN],cn;
int que[MAXN];
inline bool dfs2(int x)
{
	vis[x]=true;
	int n=s[x].size(),cnt=0;
	for (int i=0;i<n;i++)
		if (s[x][i]==fa[x] && cnt==0)
			cnt++;
		else
		{
			if (vis[s[x][i]])
			{
				for (int y=x;y!=s[x][i];y=fa[y])
					circle[y]=true,c[++cn]=y;
				circle[s[x][i]]=true,c[++cn]=s[x][i];
				return true;
			}
			else
			{
				fa[s[x][i]]=x;
				if (dfs2(s[x][i]))
					return true;
			}
		}
	vis[x]=false;
	return false;
}
inline void solve(int x,int t)
{
	vis[x]=true;
	printf("%d ",x);
	bool flag=false;
	int nxt=que[t%cn+1];
	for (int i=0;i<s[x].size();i++)
	{
		if (circle[s[x][i]])
			continue;
		if (s[x][i]<nxt)
			dfs1(s[x][i]);
		else
		{
			a[++an]=s[x][i];
			solve(nxt,t%cn+1);
			flag=true;
			break;
		}
	}
	if (!flag)
	{
		if (a[an]<nxt)
			return;
	}
}
inline void dfs3(int x)
{
	vis[x]=true;
	if (circle[x])
	{
		int t;
		for (t=1;t<=cn;t++)
			if (c[t]==x)
				break;
		if (c[t%cn+1]<c[(t+cn-2)%cn+1])
			for (int i=0;i<cn;i++)
				que[i+1]=c[(t+i-1)%cn+1];
		else
			for (int i=0;i<cn;i++)
				que[i+1]=c[(t+cn-i-1)%cn+1];
		a[++an]=que[cn];
		for (int i=0;i<s[x].size();i++)
			if (!vis[s[x][i]])
				a[++an]=s[x][i];
		solve(x,1);
	}
	printf("%d ",x);
}
inline void solve2()
{
	memset(vis,false,sizeof(vis));
	memset(circle,false,sizeof(circle));
	fa[1]=0;
	dfs2(1);
	//for (int i=1;i<=n;i++) if (circle[i]) printf("%d ",i);
	memset(vis,false,sizeof(vis));
	dfs3(1);
}
int main()
{
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		int a,b;
		scanf("%d%d",&a,&b);
		s[a].push_back(b);
		s[b].push_back(a);
	}
	for (int i=1;i<=n;i++)
		sort(s[i].begin(),s[i].end());
	if (m==n-1)
		solve1();
	else
		solve2();
}
