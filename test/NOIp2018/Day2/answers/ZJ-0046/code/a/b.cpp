#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>
using namespace std;
const int MAXN=5010;
int n,m,e0;
vector<int> s[MAXN];
bool vis[MAXN];
int ans[MAXN],newans[MAXN],N;
inline void dfs(int x)
{
	newans[++N]=x;
	vis[x]=true;
	int n=s[x].size();
	for (int i=0;i<n;i++)
		if (!vis[s[x][i]])
			dfs(s[x][i]);
}
inline bool update()
{
	for (int i=1;i<=n;i++)
		if (ans[i]!=newans[i])
			return newans[i]<ans[i];
	return false;
}
inline void solve()
{
	for (int i=1;i<=n;i++)
		sort(s[i].begin(),s[i].end());
	N=0;
	memset(vis,false,sizeof(vis));
	dfs(1);
	if (N==n && update())
		memcpy(ans,newans,sizeof(ans));
}
int a[MAXN],b[MAXN];
int main()
{
	ans[1]=1000000;
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
		scanf("%d%d",&a[i],&b[i]);
	if (m==n-1)
	{
		for (int i=1;i<=m;i++)
			s[a[i]].push_back(b[i]),s[b[i]].push_back(a[i]);
		solve();
	}
	else
	{
		for (int i=1;i<=m;i++)
		{
			for (int j=1;j<=n;j++)
				s[j].clear();
			for (int k=1;k<=m;k++)
				if (i!=k)
					s[a[k]].push_back(b[k]),s[b[k]].push_back(a[k]);
			solve();
			//printf("%d-%d: ",a[i],b[i]);
			//for (int i=1;i<=n;i++) printf("%d ",newans[i]);
			//printf("\n");
		}
	}
	for (int i=1;i<n;i++)
		printf("%d ",ans[i]);
	printf("%d\n",ans[n]);
}
