#include <bits/stdc++.h>
#define res register int
#define N 5005
using namespace std;
set<int> son[N];
int n,m,tot,d[N];
inline int read(){
	res w=0,X=0;register char ch=0;
	while(!isdigit(ch)){w|=ch=='-';ch=getchar();}
	while(isdigit(ch)){X=(X<<1)+(X<<3)+(ch^48);ch=getchar();}
	return w?-X:X;
}
struct papa{
	int to,next;
}e[N<<1];
inline void add(res x,res y){
	e[++tot].to=y;
	e[tot].next=d[x];
	d[x]=tot;
}
namespace subtask1{
	void dfs(res now,res fa){
		for(res i=d[now];i;i=e[i].next){
			res x=e[i].to;
			if(x==fa) continue;
			son[now].insert(x);
			dfs(x,now);
		}
	}
	void print(res now){
		printf("%d ",now);
		set<int>::iterator it;
		for(it=son[now].begin();it!=son[now].end();it++)
		  print(*it);
	}
	void MAIN(){
		dfs(1,0);
		print(1);
	}
}
namespace subtask2{
	int tim=0,dfn[N],low[N],num,st[N],top=0,belong[N],use;
	bool vis[N];
	set<int> S;
	void tarjan(res now,res fa){
		dfn[now]=low[now]=++tim;
		st[++top]=now;
		for(res i=d[now];i;i=e[i].next){
			res x=e[i].to;
			if(x==fa) continue;
			if(!dfn[x]){
				tarjan(x,now);
				low[now]=min(low[now],low[x]);
			}
			else if(!belong[x])
				low[now]=min(low[now],dfn[x]);
		}
		if(dfn[now]==low[now]){
			if(st[top]==now){
				top--;
				return;
			}
			for(;st[top]!=now;top--)
			  belong[st[top]]=1;
			belong[st[top--]]=1;
		}
	}
	void dfs(res now,res fa){
		vis[now]=1;
		for(res i=d[now];i;i=e[i].next){
			res x=e[i].to;
			if(x==fa) continue;
			if(vis[x]){
				son[now].insert(x);
				continue;
			}
			son[now].insert(x);
			dfs(x,now);
		}
	}
	void print(res now){
		if(vis[now]) return;
		set<int>::iterator it,it1;
		printf("%d\n",now);
//		for(it=son[now].begin();it!=son[now].end();it++)
//		  printf("%d ",*it);
		vis[now]=1;
		if(!belong[now])
		  for(it=son[now].begin();it!=son[now].end();it++)
		     print(*it);
		else{
		  for(it=son[now].begin();it!=son[now].end();it++)
		    if(belong[*it])
		      S.insert(*it);
		  for(it=son[now].begin();it!=son[now].end();){
		  	if(use) print(*it),it++;
		  	else{
		  	   it1=S.begin();
		  	   while(vis[*it1]){
		  	   	S.erase(*it1);
		  	   	it1=S.begin();
			   }
			   if(it1==S.end()) use=1;
			   if(!son[now].count(*it1)) use=1;
			   print(*it);
			}
		  }
		}
	}
	void MAIN(){
		tot=0;
		tarjan(1,0);
		dfs(1,0);
		memset(vis,0,sizeof(vis));
		print(1);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(res i=1;i<=m;i++){
		res x=read(),y=read();
		add(x,y);
		add(y,x);
	}
	if(m==n-1){
		subtask1::MAIN();
		return 0;
	}
	else{
		subtask2::MAIN();
		return 0;
	}
}
