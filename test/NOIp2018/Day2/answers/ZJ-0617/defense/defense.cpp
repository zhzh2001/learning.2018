#include <bits/stdc++.h>
#define res register int
#define N 2005
#define inf 0x3f3f3f3f
using namespace std;
int n,m,tot,f[N][3],p[N],d[N],a,x,b,y,tag[N];//0 �Լ� 1 ���� 2 �ְ�
string op;
inline int read() {
	res w=0,X=0;
	register char ch=0;
	while(!isdigit(ch)) {
		w|=ch=='-';
		ch=getchar();
	}
	while(isdigit(ch)) {
		X=(X<<1)+(X<<3)+(ch^48);
		ch=getchar();
	}
	return w?-X:X;
}
struct papa {
	int to,next;
} e[N<<1];
inline void add(res x,res y) {
	e[++tot].to=y;
	e[tot].next=d[x];
	d[x]=tot;
}
void dfs(res now,res fa) {
	f[now][0]=0;
	f[now][1]=p[now];
	for(res i=d[now]; i; i=e[i].next) {
		res x=e[i].to;
		if(x==fa) continue;
		dfs(x,now);
		f[now][0]+=f[x][1];
		f[now][1]+=min(f[x][1],f[x][0]);
	}
	if(tag[now]==0) f[now][1]=inf;
	if(tag[now]==1) f[now][0]=inf;
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();
	cin>>op;
	for(res i=1; i<=n; i++)
		p[i]=read();
	for(res i=1; i<n; i++) {
		res x=read(),y=read();
		add(x,y);
		add(y,x);
	}
	for(res i=1; i<=m; i++) {
		a=read(),x=read(),b=read(),y=read();
		memset(tag,-1,sizeof(tag));
		memset(f,inf,sizeof(f));
		tag[a]=x;
		tag[b]=y;
		dfs(1,0);
		res ans=min(f[1][1],f[1][0]);
		printf("%d\n",ans>=inf?-1:ans);
	}
}
