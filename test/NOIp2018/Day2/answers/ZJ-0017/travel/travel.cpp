#include<bits/stdc++.h>
#define maxn 500005
//#define kcz
using namespace std;
inline int read()
{
	int lin = 0,f = 1;
	char x = getchar();
	while(x < '0' || x > '9')
	{
		if(x == '-') f = -1;
		x = getchar();
	}
	while(x >= '0' && x <= '9')
	{
		lin = (lin << 1) + (lin << 3) + x - '0';
		x = getchar();
	}
	return lin * f;
}
#define PII pair<int,int>
#define fir first
#define sec second
#define ma(a,b) make_pair(a,b)
#define inf 123123123
int u,v,n,m,us[maxn],fl,cut_a,cut_b;
int ans[maxn],ce[maxn],cnt;
vector <int> s[maxn],huan;
stack <int> sta;
int check(int a,int b)
{
	if(a == cut_a && b == cut_b) return 0;
	swap(a,b);
	if(a == cut_a && b == cut_b) return 0;
	return 1;
}
void add(int u,int v)
{
	s[u].push_back(v);
}
void dfs(int now,int fa)
{
	printf("%d ",now);
	for(int i = 0; i < s[now].size(); i++)
	{
		int nex = s[now][i];
		if(nex != fa)
		{
			dfs(nex,now);
		}
	}
}
void zhao(int now,int fa)
{
	if(fl) return;
	if(us[now])
	{
		int sto = now;
		fl = 1;
		while(sta.size())
		{
			int pos = sta.top();
			sta.pop();
			if(pos == now)
			{
				huan.push_back(pos);
				return;
			}
			huan.push_back(pos);
		}
	}
	us[now] = 1;
	sta.push(now);
	for(int i = 0; i < s[now].size(); i++)
	{
		int nex = s[now][i];
		if(nex != fa)
		{
			zhao(nex,now);
			if(fl) return;
		}
	}
	us[now] = 0;
	sta.pop();
}
int bi()
{
	if(ans[1] == 0) return 1;
	for(int i = 1; i <= n; i++)
	{
		if(ans[i] < ce[i]) return 0;
		if(ans[i] > ce[i]) return 1;
	}
	return 1;
}
void sol(int now,int fa)
{
	ce[++cnt] = now;
	for(int i = 0; i < s[now].size(); i++)
	{
		int nex = s[now][i];
		if(nex != fa && check(now,nex))
			sol(nex,now);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n = read(); m = read();
	for(int i = 1; i <= m; i++)
	{
		u = read(); v = read();
		add(u,v);
		add(v,u);
	}
	for(int i = 1; i <= n; i++)
	{
		sort(s[i].begin(),s[i].end());
//		for(int j = 0; j < s[i].size(); j++)
//			printf("%d ",s[i][j]);
//		puts("");
	}
	if(m == n - 1)
	{
		dfs(1,1);
		return 0;
	}
	zhao(1,1);
//	for(int i = 0; i < huan.size(); i++)
//	{
//		printf("%d ",huan[i]);
//	}
	huan.push_back(huan[0]);
	for(int i = 0; i < huan.size() - 1; i++)
	{
		cut_a = huan[i],cut_b = huan[i + 1];
		cnt = 0;
		sol(1,1);
		if(bi())
		{
			for(int j = 1; j <= cnt; j++) ans[j] = ce[j];
		}
	}
	
	for(int i = 1; i <= n; i++)
		printf("%d ",ans[i]);
	return 0;
}

