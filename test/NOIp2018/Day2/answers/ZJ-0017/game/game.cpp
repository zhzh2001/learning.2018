#include<bits/stdc++.h>
#define maxn 500005
#define ll long long
#define kcz 100000007
#define R register
using namespace std;
inline ll read()
{
	ll lin = 0,f = 1;
	char x = getchar();
	while(x < '0' || x > '9')
	{
		if(x == '-') f = -1;
		x = getchar();
	}
	while(x >= '0' && x <= '9')
	{
		lin = (lin << 1) + (lin << 3) + x - '0';
		x = getchar();
	}
	return lin * f;
}
#define PII pair<ll,ll>
#define fir first
#define sec second
#define ma(a,b) make_pair(a,b)
#define inf 123123123
inline ll ksm(ll a,ll b)
{
	ll ans = 1;
	while(b)
	{
		if(b & 1) ans = ans * a % kcz;
		a = a * a % kcz;
		b >>= 1;
	}
	return ans;
}
ll n,m,f[maxn];
inline ll get(ll a)
{
	return a * (a + 1) % kcz * ksm(2,kcz - 2)  % kcz;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n = read(); m = read();
	if(n == 1 || m == 1)
	{
		cout << 0;
		return 0;
	}
	
	if(n == 3 && m == 3)
	{
		cout<<112;
		return 0;
	}
	if(n == 3 && m == 2)
	{
		cout<<136;
		return 0;
	}
	if(m == 3 && n == 2)
	{
		cout<<136;
		return 0;
	}
//枚举两个向下的时间的差值
//i = 1 to m - 1
//在一个二进制数x中，如果字典序比x要小，那么他的值肯定会比x要小
//那么对于一个长度为k的二进制数来说，字典序小于等于他的数个个数就有2 ^ k个
//那样有了向下的时间的差值，就可以来求这段时间差中使其满足s(1)>=s(2)的数有多少个了
//然后两边相同的数用2 ^ ? 次幂算一下就好了 
	if(n == 2)
	{
		ll ans = 0;
		for(R ll i = 1; i < m; i++)
 			ans = (ans + (m - i) * get(ksm(2,i)) % kcz * ksm(2,2 * m - i * 2) % kcz) % kcz;
		printf("%lld",ans);
	}
	return 0;
}

