#include<bits/stdc++.h>
#define maxn 500005
#define ll long long
//#define kcz
using namespace std;
inline int read()
{
	int lin = 0,f = 1;
	char x = getchar();
	while(x < '0' || x > '9')
	{
		if(x == '-') f = -1;
		x = getchar();
	}
	while(x >= '0' && x <= '9')
	{
		lin = (lin << 1) + (lin << 3) + x - '0';
		x = getchar();
	}
	return lin * f;
}
#define PII pair<int,int>
#define fir first
#define sec second
#define ma(a,b) make_pair(a,b)
#define inf 123123123123123
int head[maxn],tot,n,q,u,v,val[maxn],a,b,x,y;
ll f[maxn][2],le[maxn][2],re[maxn][2];
char c[maxn];
struct st{
	int v,nex;
}s[maxn];
void add(int u,int v)
{
	s[++tot] = (st) { v,head[u] };
	head[u] = tot;
}
//f[i][0] <= f[i][1] 
//0代表不驻扎 
//1代表驻扎 
//x == 1时，此地必须驻扎，f[i][0] = inf 
//x == 0时，此地不能驻扎，f[i][1] = inf 
void dfs(int now,int fa)
{
	for(int i = head[now]; i; i = s[i].nex)
		if(s[i].v != fa)
		{
			dfs(s[i].v,now);
		}
	f[now][1] = val[now];
	f[now][0] = 0;
	for(int i = head[now]; i; i = s[i].nex)
		if(s[i].v != fa)
		{
			f[now][1] += min(f[s[i].v][0],f[s[i].v][1]);
			f[now][0] += f[s[i].v][1];
		}
	if(f[now][0] > inf) f[now][0] = inf;
	if(f[now][1] > inf) f[now][1] = inf;
	if(now == a || now == b)
	{
		if(now == a)
			f[now][x ^ 1] = inf;
		else
			f[now][y ^ 1] = inf;
	}
}
void sol()
{
	for(int i = 1; i <= q; i++)
	{
		a = read(); x = read();
		b = read(); y = read();
		for(int j = 1; j <= n; j++)
			f[j][0] = f[j][1] = inf;
		dfs(1,1);
		if(f[1][0] == inf && f[1][1] == inf)
		{
			printf("-1\n");
			continue;
		}
		printf("%lld\n",min(f[1][0],f[1][1]));
	}
}
int main(){
	freopen("1.in","r",stdin);
	freopen("lian.out","w",stdout);
	n = read(); q = read(); scanf("%s",c + 1);
	for(int i = 1; i <= n; i++)
		val[i] = read();
	for(int i = 1; i < n; i++)
	{
		u = read(); v = read();
		add(u,v);
		add(v,u);
	}
	if(c[1] == 'A')
	{

		if(c[2] == '1')
		{
			le[1][1] = val[1];
			le[1][0] = inf;
			for(int i = 2; i <= n; i++)
			{
				le[i][0] = le[i - 1][1];
				le[i][1] = min(le[i - 1][0],le[i - 1][1]) + val[i];
			}
			for(int i = n; i >= 1; i--)
			{
				re[i][0] = re[i + 1][1];
				re[i][1] = min(re[i + 1][0],re[i + 1][1]) + val[i];
			}
			re[1][0] = inf;
//			for(int i = 1; i <= n; i++)
//				printf("%lld %lld %lld %lld\n",le[i][0],le[i][1],re[i][0],re[i][1]);
			for(int i = 1; i <= q; i++)
			{
				a = read(); x = read();
				b = read(); y = read();
				if(a == b && x != y)
				{
					printf("-1\n");
					continue;
				}
				int t = 0;
				if(y == 1)
					t  = val[b];
				printf("%lld\n",le[b][y] + re[b][y] - t);
			}
		}
		else if(c[2] == '2')
		{
			for(int i = 1; i <= n; i++)
			{
				le[i][0] = le[i - 1][1];
				le[i][1] = min(le[i - 1][0],le[i - 1][1]) + val[i];
			}
			for(int i = n; i >= 1; i--)
			{
				re[i][0] = re[i + 1][1];
				re[i][1] = min(re[i + 1][0],re[i + 1][1]) + val[i];
			}
			for(int i = 1; i <= q; i++)
			{
				a = read(); x = read();
				b = read(); y = read();
				if(!x && !y)
				{
					printf("-1\n");
					continue;
				}
				if(a > b) swap(a,b);
				printf("%lld\n",le[a][x] + re[b][y]);
			}
			return 0;
		}
	}
	return 0;
}

