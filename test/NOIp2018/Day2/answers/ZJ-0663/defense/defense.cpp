#include <bits/stdc++.h>

using namespace std;

const int Maxn = 1e5+7;
const long long INF = 1e15;

int n, m;
long long ans, res;
int p[Maxn], vis[Maxn];
vector<int> e[Maxn];
string type;

void fill(int cur)
{
	if(cur > n)
	{
		res = min(res, ans);
		return;
	}
	if(vis[cur]) fill(cur+1);
		ans += p[cur];
		vis[cur-1]++;vis[cur]++;vis[cur+1]++;
		fill(cur+2);
		ans -= p[cur];
		vis[cur-1]--;vis[cur]--;vis[cur+1]--;
	if(!(cur-1 >= 1 && !vis[cur-1]))
	{
		fill(cur+1);
	}
}

int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	cin >> n >> m >> type;
	for(int i = 1 ; i<= n; ++i)
		cin >> p[i];
	for(int i = 1, u, v; i < n; ++i)
		{
			cin >> u >> v;
			e[u].push_back(v);
			e[v].push_back(u);
		}
	for(int i = 1, a, b,x, y; i<= m; ++i)
	{
		ans = 0;
		res = INF;
		memset(vis, 0, sizeof vis);
		cin >> a >> x >> b >> y;
		if(x) vis[a] = vis[a-1] = vis[a+1] = 1;
		if(y) vis[b] = vis[b-1] = vis[b+1] = 1;
		ans += 1ll*p[a]*x+1ll*p[b]*y;
		fill(1);
		cout << res << endl;
	}
	return 0;
}
