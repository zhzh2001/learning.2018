#include <queue>
#include <cstdio>
#include <iostream>

using namespace std;

const int MOD = 1e9+7;
const int Maxn = 12;
const int Maxm = 1e6+7; 

int n, m, ans;
char a[Maxn][Maxm];
string sss;
int mem[10][10];

inline void print()
{
	for(int i = 1; i <= n; ++i)
	{
		for(int j = 1; j <= m; ++j)
			putchar(a[i][j]);
		putchar('\n');
	}
	putchar('\n');
}

int check(int x, int y, string now)
{
	if(x == n && y == m)
	{
		//print();
		if(!sss.length()) sss = now;
		if(sss > now) return 0;
		sss = now;
		return 1;
	}
	if(y+1 <= m)
	{
		if(!check(x, y+1, now+a[x][y])) return 0;
	}
	if(x+1 <= n)
	{
		if(!check(x+1, y, now+a[x][y])) return 0;
	}
	return 1;
}

void brute(int x, int y)
{
	if(x > n)
	{
		sss = "";
		ans = (ans+check(1, 1, ""))%MOD;
		return;
	}
	a[x][y] = '0';
	if(y+1 <= m) brute(x, y+1);
	else brute(x+1, 1);
	a[x][y] = '1';
	if(y+1 <= m) brute(x, y+1);
	else brute(x+1, 1);
}

inline void init()
{
mem[1][1]=2;
mem[1][2]=4;
mem[1][3]=8;
mem[2][1]=4;
mem[2][2]=12;
mem[2][3]=36;
mem[3][1]=8;
mem[3][2]=36;
mem[3][3]=112;
mem[3][4]=336;
mem[1][4]=16;
mem[1][5]=32;
mem[1][6]=64;
mem[1][7]=128;
mem[1][8]=256;
mem[2][4]=108;
mem[2][5]=324;
mem[2][6]=972;
mem[2][7]=2916;
mem[2][8]=8748;
mem[4][1]=16;
mem[5][1]=32;
mem[6][1]=64;
mem[7][1]=128;
mem[8][1]=256;
mem[4][2]=108;
mem[5][2]=324;
mem[6][2]=972;
mem[7][2]=2916;
mem[8][2]=8748;

}

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	init();
	scanf("%d%d", &n, &m);
	if(mem[n][m] != 0) return printf("%d\n", mem[n][m])&0;
	brute(1, 1);
	printf("%d\n", ans);
	return 0;
}
