#include <cstdio>
#include <vector>
#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;

const int Maxn = 5e3+7;

int n, m;
int vis[Maxn], res[Maxn], tot, daliarr[Maxn];
int dfn[Maxn], low[Maxn], col[Maxn], circle, cnt_dfn, cnt_col;
int stk[Maxn], sz;
vector<int> e[Maxn];
int ppp, flag;
int du[Maxn];
int q[Maxn], head, tail;

void brute_dfs(int cur)
{
	vis[cur] = 1;
	res[++tot] = cur;
	for(vector<int>::iterator it = e[cur].begin(); it != e[cur].end(); ++it)
		if(!vis[*it]) brute_dfs(*it);
}

void tarjan(int cur, int fa)
{
	dfn[cur] = low[cur] = ++cnt_dfn;
	vis[cur] = 1;
	stk[++sz] = cur;
	for(vector<int>::iterator it = e[cur].begin(); it != e[cur].end(); ++it)
	{
		if(*it == fa) continue;
		if(!dfn[*it])
		{
			tarjan(*it, cur);
			low[cur] = min(low[cur], low[*it]);
		}
		if(vis[cur]) low[cur] = min(low[cur], dfn[*it]);
	}
	if(dfn[cur] == low[cur])
	{
		++cnt_col;
		int num = 0;
		do
		{
			num++;
			col[stk[sz]] = cnt_col;
			vis[stk[sz]] = 0;
		} while(stk[sz--] != cur);
		if(num > 1) circle = cnt_col;
	}
}

// flag --> 0 no circle, 1 has another choice, 2 finish
void fuck_dfs(int cur)
{
	if(cur != 1) du[cur]--;// 无向图,理论上每边两点 
	if(!vis[cur]) res[++tot] = cur;
	vis[cur] = 1;
	// 第一个进入环的点，是可以回溯回来的点 
	if(flag == 0 && col[cur] == circle)// in circle, get two choice
	{
		flag = 1; ppp = cur;
		// in q, the point ppp can reach
		for(vector<int>::iterator it = e[cur].begin(); it != e[cur].end(); ++it)
			if(!vis[*it]) q[tail++] = *it;
	}
	// in normal way // 正常下去呗 
	for(vector<int>::iterator it = e[cur].begin(); it != e[cur].end(); ++it)
		if(!vis[*it])
		{
			// 当且仅当flag == 1有机会,要去的点在环上(可以另一条路走到),cur只剩下这条路 
			if(flag == 1 && col[*it] == circle && du[cur] == 1) // 还可以走特殊 
			{
				// ppp有比*it小的点 
				// there's another choice, force to change the way
				while(head < tail && vis[q[head]]) head++;
				if(head < tail && q[head] < *it) fuck_dfs(ppp), flag = 2, du[cur]--; // only once
				else fuck_dfs(*it), du[cur]--;
			}
			else fuck_dfs(*it), du[cur]--;
		}
}

bool cmp(int *x, int *y)
{
	for(int i = 1; i <= n; ++i)
		if(x[i] != y[i]) return x[i] < y[i];
	return false;
}

inline void print()
{
	for(int i = 1; i < n; ++i)
		printf("%d ", res[i]);
	printf("%d\n", res[n]);
}

int main()
{
	
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i = 1, u, v; i <= m; ++i)
	{
		scanf("%d%d", &u, &v);
		e[u].push_back(v);
		e[v].push_back(u);
		du[u]++; du[v]++;
	}
	for(int i = 1; i <= n; ++i)
		sort(e[i].begin(), e[i].end());
		
	if(m == n-1) brute_dfs(1);
	else if(m == n)
	{
		tarjan(1, 0);
		fuck_dfs(1);
	}
	print();
	return 0;
}
