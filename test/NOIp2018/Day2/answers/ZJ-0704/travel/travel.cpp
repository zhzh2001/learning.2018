#include<bits/stdc++.h>
#define ll long long
#define db double
#define mp make_pair
#define pb push_back
#define pii pair<int,int>
#define A first
#define B second
#define lowbit(p) (p&(-(p)))
using namespace std;
void read(int &x){
	x=0; char c=getchar(); int p=1;
	for (;c<48;c=getchar())if (c=='-')p=-1;
	for (;c>47;c=getchar())x=(x<<1)+(x<<3)+(c^48);
	x*=p;
}
void read(ll &x){
	x=0; char c=getchar(); int p=1;
	for (;c<48;c=getchar())if (c=='-')p=-1;
	for (;c>47;c=getchar())x=(x<<1)+(x<<3)+(c^48);
	x*=p;
}
void Min(int &x,int y){
	if (x>y)x=y;
}
void Min(ll &x,ll y){
	if (x>y)x=y;
}
void Max(int &x,int y){
	if (x<y)x=y;
}
void Max(ll &x,ll y){
	if (x<y)x=y;
}
bool stttt;
#define M 5005
int a[M],tot,n,m;
struct P1{
	vector<int>to[M];
	void dfs(int f,int x){
		a[++tot]=x;
		sort(to[x].begin(),to[x].end());
		int i,sz=to[x].size();
		for (i=0;i<sz;i++)if (to[x][i]!=f){
			dfs(x,to[x][i]);
		}
	}
	void solve(){
		int i,x,y;
		for (i=1;i<=m;i++){
			read(x); read(y);
			to[x].pb(y);
			to[y].pb(x);
		}
		dfs(1,1);
		for (i=1;i<n;i++){
			printf("%d ",a[i]);
		}
		printf("%d\n",a[i]);
	}
}p1;

struct P2{
	vector<pii>to[M];
	bool mk[M];
	int res[M],vis[M],tim;
	void dfs(int f,int x){
		if (vis[x]==tim)return ;
		vis[x]=tim;
		
		a[++tot]=x;
		int i,sz=to[x].size();
		for (i=0;i<sz;i++)if (to[x][i].A!=f&&!mk[to[x][i].B]){
			dfs(x,to[x][i].A);
		}
	}
	bool chk(){
		for (int i=1;i<=n;i++){
			if (vis[i]!=tim)return 0;
		}
		for (int i=1;i<=n;i++){
			if (a[i]!=res[i])return a[i]<res[i];
		}
		return 0;
	}
	void solve(){
		int i,x,y,j;
		for (i=1;i<=m;i++){
			read(x); read(y);
			to[x].pb(mp(y,i));
			to[y].pb(mp(x,i));
		}
		for (i=1;i<=n;i++){		
			sort(to[i].begin(),to[i].end());
			res[i]=n;
		}
		for (i=1;i<=m;i++){
			mk[i]=1;
			tot=0;
			tim++;
			dfs(1,1);
			mk[i]=0;
			if (chk()){
//				for (j=1;j<=n;j++)printf("%d ",a[j]); printf("\n");
				for (j=1;j<=n;j++){
					res[j]=a[j];
				}
			}
		}
		for (i=1;i<n;i++){
			printf("%d ",res[i]);
		}
		printf("%d\n",res[i]);
	}
}p2;

bool edddd;
int main(){
//	printf("%.2f\n",(&edddd-&stttt)/1024.0/1024.0);
//	freopen("1.in","r",stdin);
//	freopen("1.out","w",stdout);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n); read(m);
	if (m==n-1){
		p1.solve();
		return 0;
	}
	if (m==n){
		p2.solve();
		return 0;
	}
	return 0;
}
