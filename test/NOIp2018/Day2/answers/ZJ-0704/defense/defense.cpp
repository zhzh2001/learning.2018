#include<bits/stdc++.h>
#define ll long long
#define db double
#define mp make_pair
#define pb push_back
#define pii pair<int,int>
#define A first
#define B second
#define lowbit(p) (p&(-(p)))
using namespace std;
void read(int &x){
	x=0; char c=getchar(); int p=1;
	for (;c<48;c=getchar())if (c=='-')p=-1;
	for (;c>47;c=getchar())x=(x<<1)+(x<<3)+(c^48);
	x*=p;
}
void read(ll &x){
	x=0; char c=getchar(); int p=1;
	for (;c<48;c=getchar())if (c=='-')p=-1;
	for (;c>47;c=getchar())x=(x<<1)+(x<<3)+(c^48);
	x*=p;
}
void Min(int &x,int y){
	if (x>y)x=y;
}
void Min(ll &x,ll y){
	if (x>y)x=y;
}
void Max(int &x,int y){
	if (x<y)x=y;
}
void Max(ll &x,ll y){
	if (x<y)x=y;
}
bool stttt;
#define M 100005
const ll inf=10000000000000000ll;
struct ed{
	int x,nx;
}e[M<<1];
ll c[M];
ll dp[M][2],f[M][2];
int fa[M];
int nx[M],ecnt=1,n,m;
void add(int x,int y){
	e[ecnt]=(ed){y,nx[x]};
	nx[x]=ecnt++;
}
struct P1{
	int xx,yy,op1,op2;
	void dfs(int f,int x){
		dp[x][0]=0;
		dp[x][1]=c[x];
		for (int i=nx[x];i;i=e[i].nx)if (e[i].x!=f){
			dfs(x,e[i].x);
			dp[x][0]+=dp[e[i].x][1];
			dp[x][1]+=min(dp[e[i].x][1],dp[e[i].x][0]);
		}
		if (x==xx){
			dp[x][!op1]=inf;
		}
		if (x==yy){
			dp[x][!op2]=inf;
		}
	}
	void solve(){
		int i,x,y;
		for (i=1;i<=n;i++)read(c[i]);
		for (i=1;i<n;i++){
			read(x); read(y);
			add(x,y);
			add(y,x);
		}
		for (i=1;i<=m;i++){
			read(xx); read(op1);
			read(yy); read(op2);
//			printf("%d %d %d %d\n",xx,yy,op1,op2);
//			printf("------------\n");
			dfs(1,1);
			if (dp[1][0]<inf||dp[1][1]<inf){
				printf("%lld\n",min(dp[1][0],dp[1][1]));
			}
			else {
				printf("-1\n");
			}
		}
	}
}p1;
struct P2{
	void dfs(int f,int x){
		fa[x]=f;
		dp[x][0]=0;
		dp[x][1]=c[x];
		for (int i=nx[x];i;i=e[i].nx)if (e[i].x!=f){
			dfs(x,e[i].x);
			dp[x][0]+=dp[e[i].x][1];
			dp[x][1]+=min(dp[e[i].x][1],dp[e[i].x][0]);
		}
	}
	void cal(int x,int op){
		f[x][op]=dp[x][op];
		f[x][!op]=inf;
		for (;fa[x];x=fa[x]){
			f[fa[x]][0]=dp[fa[x]][0];
			f[fa[x]][1]=dp[fa[x]][1];
			f[fa[x]][0]-=dp[x][1];
			f[fa[x]][1]-=min(dp[x][1],dp[x][0]);
			f[fa[x]][0]+=f[x][1];
			f[fa[x]][1]+=min(f[x][1],f[x][0]);
		}
	}
	void solve(){
		int i,x,y,xx,yy,op1,op2;
		for (i=1;i<=n;i++)read(c[i]);
		for (i=1;i<n;i++){
			read(x); read(y);
			add(x,y);
			add(y,x);
		}
		dfs(0,1);
		for (i=1;i<=m;i++){
			read(xx); read(op1);
			read(yy); read(op2);
			cal(yy,op2);
			if (f[1][1]<inf){
				printf("%lld\n",f[1][1]);
			}
			else {
				printf("-1\n");
			}
		}
	}
}p2;
struct P3{
	ll res[M];
	vector<pii>qu[M];
	void dfs(int f,int x){
		dp[x][0]=0;
		dp[x][1]=c[x];
		for (int i=nx[x];i;i=e[i].nx)if (e[i].x!=f){
			dfs(x,e[i].x);
			dp[x][0]+=dp[e[i].x][1];
			dp[x][1]+=min(dp[e[i].x][0],dp[e[i].x][1]);
		}
	}
	void getres(int fa,int x){
		int sz=qu[x].size(),i,to;
		for (i=0;i<sz;i++){
			res[qu[x][i].B]=f[x][qu[x][i].A];
		}
		ll l0=f[x][0],l1=f[x][1];
		for (i=nx[x];i;i=e[i].nx)if (e[i].x!=fa){
			to=e[i].x;
			
			f[x][0]-=dp[to][1];
			f[x][1]-=min(dp[to][0],dp[to][1]);
			
			f[to][0]=dp[to][0];
			f[to][1]=dp[to][1];
			f[to][0]+=f[x][1];
			f[to][1]+=min(f[x][1],f[x][0]);
			
			getres(x,e[i].x);
			f[x][0]=l0;
			f[x][1]=l1;
		}
	}
	
	void solve(){
		int i,x,y;
		for (i=1;i<=n;i++){
			read(c[i]);
		}
		for (i=1;i<n;i++){
			read(x); read(y);
			add(x,y);
			add(y,x);
		}
		dfs(1,1);
		dp[1][0]=inf;
		f[1][0]=dp[1][0];
		f[1][1]=dp[1][1];
		for (i=1;i<=m;i++){
			read(x); read(y);
			read(x); read(y);
			qu[x].pb(mp(y,i));
		}
		getres(1,1);
		for (i=1;i<=m;i++){
			if (res[i]<inf)printf("%lld\n",res[i]);
			else printf("-1\n");
		}
	}
}p3;
struct P4{
	ll l[M][2],r[M][2];
	void solve(){
		int i,x,y,op1,op2;
		for (i=1;i<=n;i++){
			read(c[i]);
		}
		for (i=1;i<n;i++){
			read(x); read(y);
		}
		for (i=1;i<=n;i++){
			l[i][0]=0+l[i-1][1];
			l[i][1]=c[i]+min(l[i-1][0],l[i-1][1]);
		}
		for (i=n;i>=1;i--){
			r[i][0]=0+r[i+1][1];
			r[i][1]=c[i]+min(r[i+1][0],r[i+1][1]);
		}
		for (i=1;i<=m;i++){
			read(x); read(op1);
			read(y); read(op2);
			if (x>y)swap(x,y),swap(op1,op2);
			if (op1==0&&op2==0){
				printf("-1\n");
				continue;
			}
			ll res=l[x][op1]+r[y][op2];
			printf("%lld\n",res);
		}
	}
}p4;
bool edddd;
int main(){
//	printf("%.2f\n",(&edddd-&stttt)/1024.0/1024.0);
//	freopen("3.in","r",stdin);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char op[5];
	read(n); read(m); scanf("%s",op);
	if (n<=2000&&m<=2000){
		p1.solve();
		return 0;
	}
	if (op[0]=='B'&&op[1]=='1'){
		p2.solve();
		return 0;
	}
	if (op[1]=='1'){
		p3.solve();
		return 0;
	}
	if (op[0]=='A'&&op[1]=='2'){
		p4.solve();
		return 0;
	}
	return 0;
}
