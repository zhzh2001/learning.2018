#include<cstdio>
#include<algorithm>
#define tt 1000000007
using namespace std;
typedef long long ll;
int N,M;
ll qsm(ll x,ll y){
	ll Sum=1;
	while (y){
		if (y&1) Sum=(Sum*x)%tt;
		x=(x*x)%tt;y>>=1;
	}
	return Sum;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&N,&M);
	if (N==1||M==1){printf("%lld\n",qsm(2,N==1?M:N));return 0;}
	if (N==2){if (M==2) puts("12");else printf("%lld\n",(8*qsm(3,M-2))%tt);return 0;}
	if (N==3){if (M==2) puts("12");else printf("%lld\n",(28*qsm(4,M-2))%tt);return 0;}
	return 0;
}
