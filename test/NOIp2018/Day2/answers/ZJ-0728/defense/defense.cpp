#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
inline int read(){
	int Res=0,f=1;char ch=getchar();
	while (ch>'9'||ch<'0') f=(ch=='-'?-f:f),ch=getchar();
	while (ch>='0'&&ch<='9') Res=Res*10+ch-'0',ch=getchar();
	return Res*f;
}
int N,M,V[100005];
char Ss[8];
ll F[100005][2];
int tot,lnk[100005],nxt[200005],son[200005];
void add(int x,int y){son[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;}
int boo[100005],Fa[100005];
void dfs_(int x,int fa){
	F[x][0]=0;F[x][1]=V[x];Fa[x]=fa;
	if (boo[x]==1) F[x][1]=1e12;
	if (boo[x]==2) F[x][0]=1e12;
	for (int i=lnk[x];i;i=nxt[i]){
		if (son[i]==fa) continue;
		dfs_(son[i],x);
		F[x][1]+=min(F[son[i]][1],F[son[i]][0]);
		F[x][0]+=F[son[i]][1];
	}
}
int S[100005],top;
ll Y[100005][2];
void Jump(int x,int t){
//	F[x][t^1]=1e12;
	S[top=0]=x;if (x==1){F[x][t^1]=1e12;return ;}
	for (int i=Fa[x];i;i=Fa[i]) S[++top]=i;
	for (int i=top;i;i--) 
		F[S[i]][1]-=min(F[S[i-1]][0],F[S[i-1]][1]),
		F[S[i]][0]-=F[S[i-1]][1];
	F[x][t^1]=1e12;
	for (int i=1;i<=top;i++)
		F[S[i]][1]+=min(F[S[i-1]][0],F[S[i-1]][1]),
		F[S[i]][0]+=F[S[i-1]][1];
}
void Rever(int x){
	F[x][0]=Y[x][0];F[x][1]=Y[x][1];
	if (x==1) return ;
	for (int i=Fa[x];i;i=Fa[i]) F[i][0]=Y[i][0],F[i][1]=Y[i][1];
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	N=read();M=read();scanf("%s",Ss);
	for (int i=1;i<=N;i++) V[i]=read();
	for (int i=1;i<N;i++){
		int x=read(),y=read();
		add(x,y);add(y,x);
	}
	if (N<=2000){
		for (int i=1;i<=M;i++){
			int a=read(),x=read(),b=read(),y=read();
			boo[a]=x+1;boo[b]=y+1;
			dfs_(1,0);
			if (min(F[1][0],F[1][1])>=1e12) puts("-1");
			else printf("%lld\n",min(F[1][0],F[1][1]));
			boo[a]=0;boo[b]=0;
		}
	}
	else{
		dfs_(1,0);
		memcpy(Y,F,sizeof F);
		for (int i=1;i<=M;i++){
			int a=read(),x=read(),b=read(),y=read();
			Jump(a,x);Jump(b,y);
			if (min(F[1][0],F[1][1])>=1e12) puts("-1");
			else printf("%lld\n",min(F[1][0],F[1][1]));
			Rever(a);Rever(b);
		}
	}
	return 0;
}
