#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){
	int Res=0,f=1;char ch=getchar();
	while (ch>'9'||ch<'0') f=(ch=='-'?-f:f),ch=getchar();
	while (ch>='0'&&ch<='9') Res=Res*10+ch-'0',ch=getchar();
	return Res*f;
}
int N,M,S[5005][5005],id[5005][5005],top[5005];
int tot,lnk[5005],son[10005],nxt[10005];
void add(int x,int y){son[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;}
namespace Tree{
	void dfs(int x,int fa){
		for (int i=lnk[x];i;i=nxt[i]) if (son[i]!=fa) S[x][++top[x]]=son[i];
		sort(S[x]+1,S[x]+1+top[x]);
		for (int i=1;i<=top[x];i++) printf(" %d",S[x][i]),dfs(S[x][i],x);
	}
	void Solve(){
		printf("1");
		dfs(1,0);
	}
};
namespace Tree_lop{
	int DFS,dfn[5005],low[5005],Stack[5005],T;
	bool vis[10005],lop[5005];
	void tarjan(int x,int fa){
		dfn[x]=low[x]=++DFS;Stack[++T]=x;
		for (int i=lnk[x];i;i=nxt[i]){
			if (son[i]==fa) continue;
			if (!dfn[son[i]]) tarjan(son[i],x),low[x]=min(low[x],low[son[i]]);
			else if (!lop[son[i]]) low[x]=min(low[x],dfn[son[i]]);
		}
		if (dfn[x]==low[x]){
			int F=x;lop[F]=1;
			while (Stack[T]!=x){
				int p=Stack[T];lop[p]=1;
				vis[id[F][p]]=vis[id[p][F]]=1;
				T--;F=p;
			}
			T--;
		}
	}
	int Sq[5005],Min[5005],len;
	void dfs(int x,int fa,int v){
		top[x]=0;
		for (int i=lnk[x];i;i=nxt[i]) if (son[i]!=fa&&i!=v&&i!=(v^1)) S[x][++top[x]]=son[i];
		sort(S[x]+1,S[x]+1+top[x]);
		for (int i=1;i<=top[x];i++) Sq[++len]=S[x][i],dfs(S[x][i],x,v);
	}
	void Try(int v){
		len=0;dfs(1,0,v);
		if (Min[1]==0) for (int i=1;i<N;i++) Min[i]=Sq[i];
		else{
			for (int i=1;i<N;i++){
				if (Min[i]<Sq[i]) break;
				if (Min[i]>Sq[i]){
					for (int j=1;j<N;j++) Min[j]=Sq[j];
					break;
				}
			}
		}
	}
	void Solve(){
		for (int i=1;i<=N;i++) if (!dfn[i]) tarjan(i,0);
		printf("1");
		for (int i=2;i<=tot;i+=2) if (vis[i]) Try(i);
		for (int i=1;i<N;i++) printf(" %d",Min[i]);
	}
};
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	N=read();M=read();tot=1;
	for (int i=1;i<=M;i++){
		int x=read(),y=read();
		add(x,y);id[x][y]=tot;
		add(y,x);id[y][x]=tot;
	}
	if (M==N-1) Tree::Solve();
	else Tree_lop::Solve();
	return 0;
}
