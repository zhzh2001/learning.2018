#include <bits/stdc++.h>

const int Extra=5,Max_N=1e5,INF=1e9+7;

int Read(){
	int x=0,f=0,ch=getchar();
	while (!isdigit(ch)) f^=!(ch^'-'),ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (f) x=-x; return x;
}

void Check_Judge(){
	freopen("defense.in","r",stdin),freopen("defense.out","w",stdout);
}

struct Edge{
	int to,next;
};

Edge E[2*Max_N+Extra];

int n,m,A,B,now_a,now_b,now_x,now_y,w[Max_N+Extra],fa[Max_N+Extra],first[Max_N+Extra],f[Max_N+Extra][2];

void DFS(int now,int now_fa){
	fa[now]=now_fa;
	for (int i=first[now]; i; i=E[i].next)
		if (E[i].to!=now_fa)
			DFS(E[i].to,now);
}

void Dp(int now){
	f[now][0]=0,f[now][1]=w[now];
	for (int i=first[now]; i; i=E[i].next)
		if (E[i].to!=fa[now]){
			Dp(E[i].to);
			f[now][0]+=f[E[i].to][1],f[now][1]+=std::min(f[E[i].to][0],f[E[i].to][1]);
		}
	if (now_a==now) f[now][now_x^1]=INF;
	if (now_b==now) f[now][now_y^1]=INF;
}

void Add(int i,int x,int y){
	E[i]=(Edge){y,first[x]},first[x]=i;
}

void Work1(){
	for (int i=1; i<=m; ++i){
		for (int i=1; i<=n; ++i) f[i][0]=f[i][1]=0;
		now_a=Read(),now_x=Read(),now_b=Read(),now_y=Read();
		if ((fa[now_a]==now_b||fa[now_b]==now_a)&&now_x+now_y==0)
			printf("-1\n");
		else{
			Dp(1);
			printf("%d\n",std::min(f[1][0],f[1][1]));
		}
	}
}

void Dp1(int now){
	f[now][0]=0,f[now][1]=w[now];
	for (int i=first[now]; i; i=E[i].next)
		if (E[i].to!=fa[now]){
			Dp(E[i].to);
			f[now][0]+=f[E[i].to][1],f[now][1]+=std::min(f[E[i].to][0],f[E[i].to][1]);
		}
}

void Work2(){
	Dp1(1);
	for (int i=1; i<=m; ++i){
		for (int i=1; i<=n; ++i) ff[i][0]=ff[i][1]=0;
		now_a=Read(),now_x=Read(),now_b=Read(),now_y=Read();
		Work();
	}
}

int main(){
	Check_Judge();
	n=Read(),m=Read(),A=getchar(),B=getchar();
	for (int i=1; i<=n; ++i) w[i]=Read();
	for (int i=2; i<=n; ++i){
		int x=Read(),y=Read();
		Add(2*i,x,y),Add(2*i+1,y,x);
	}
	DFS(1,1),Work();
	return 0;
}
