#include <bits/stdc++.h>

const int Mod=1e9+7;

void Check_Judge(){
	freopen("game.in","r",stdin),freopen("game.out","w",stdout);
}

int n,m,ans,f[1000005];

void Work1(){
	if (n==2)
		ans=12;
	else
		ans=112;
	for (int i=n; i<m; ++i) ans=(1LL*ans*3)%Mod;
	printf("%d",ans);
}

int main(){
	Check_Judge();
	scanf("%d%d",&n,&m);
	if (n>m) std::swap(n,m);
	if (n==1){
		if (m==1){
			printf("%d",2);
			return 0;
		}
		if (m==2){
			printf("%d",4);
			return 0;
		}
		if (m==3){
			printf("%d",8);
			return 0;
		}
	}
	if (n==2){
		if (m==1){
			printf("%d",4);
			return 0;
		}
		if (m==2){
			printf("%d",12);
			return 0;
		}
		if (m==3){
			printf("%d",36);
			return 0;
		}
	}
	if (n==3){
		if (m==1){
			printf("%d",8);
			return 0;
		}
		if (m==2){
			printf("%d",36);
			return 0;
		}
		if (m==3){
			printf("%d",112);
			return 0;
		}
	}
	if (n==1){
		ans=1;
		for (int i=1; i<=m; ++i) ans=ans*2%Mod;
		printf("%d",ans);
		return 0;
	}
	if (n==2||n==3) Work1();
	if (n==4){
		if (m==4) printf("%d\n",912);
		else{
			f[3]=112;
			for (int i=4; i<=m; ++i) f[i]=1LL*f[i-1]*3%Mod;
			printf("%d\n",(f[m]+f[m-1])%Mod*2%Mod);
		}
	}
	return 0;
}
