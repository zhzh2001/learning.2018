#include <bits/stdc++.h>

const int Extra=5,Max_N=5000;

int Read(){
	int x=0,f=0,ch=getchar();
	while (!isdigit(ch)) f^=!(ch^'-'),ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (f) x=-x; return x;
}

void Check_Judge(){
	freopen("travel.in","r",stdin),freopen("travel.out","w",stdout);
}

struct Edge{
	int to,next;
};

Edge E[2*Max_N+Extra];
int p_tot[Max_N+Extra],p[Max_N+Extra][Max_N+Extra];
int n,m,tot,now_x,now_y,vis[Max_N+Extra],s[Max_N+Extra],fa[Max_N+Extra],ans[Max_N+Extra],deep[Max_N+Extra],first[Max_N+Extra];

void Add(int i,int x,int y){
	E[i]=(Edge){y,first[x]},first[x]=i;
}


void DFS(int now,int now_fa){
	fa[now]=now_fa,deep[now]=deep[now_fa]+1;
	for (int i=first[now]; i; i=E[i].next)
		if (fa[E[i].to]==0)
			DFS(E[i].to,now);
}

void DFS_Ans(int now){
	++tot,s[tot]=now,vis[now]=0;
	for (int i=1; i<=p_tot[now]; ++i)
		if (vis[p[now][i]]&&(now_x!=now||now_y!=p[now][i])&&(now_y!=now||now_x!=p[now][i]))
			DFS_Ans(p[now][i]);
}

int Check(){
	if (ans[1]==0) return 1;
	for (int i=1; i<=n; ++i)
		if (ans[i]!=s[i])
			return s[i]<ans[i];
	return 0;
}

void Get_Ans(int x,int y){
	for (int i=1; i<=n; ++i) vis[i]=1;
	tot=0;
	now_x=x,now_y=y;
	DFS_Ans(1);
	if (Check())
		for (int i=1; i<=n; ++i) ans[i]=s[i];
}

void Work(int x,int y){
	Get_Ans(x,y);
	while (x!=y)
		if (deep[x]>deep[y])
			Get_Ans(x,fa[x]),x=fa[x];
		else
			Get_Ans(y,fa[y]),y=fa[y];
}

int main(){
	Check_Judge();
	n=Read(),m=Read();
	for (int i=1; i<=m; ++i){
		int x=Read(),y=Read();
		Add(2*i,x,y),Add(2*i+1,y,x);
	}
	DFS(1,1);
	for (int i=1; i<=n; ++i){
		for (int j=first[i]; j; j=E[j].next)
			++p_tot[i],p[i][p_tot[i]]=E[j].to;
		std::sort(p[i]+1,p[i]+1+p_tot[i]);
	}
	for (int i=1; i<=n; ++i)
		for (int j=first[i]; j; j=E[j].next)
			if (fa[i]!=E[j].to&&fa[E[j].to]!=i)
				Work(i,E[j].to);
	if (m==n-1)
		Get_Ans(0,0);
	for (int i=1; i<n; ++i) printf("%d ",ans[i]); printf("%d\n",ans[n]);
	return 0;
}
