#include<bits/stdc++.h>
#define ll long long
using namespace std;
const ll inf=1e10;
int n,m,now,c[100005],p[100005],ft[100005];
int cnt,vet[200005],Next[200005],head[200005];
ll ans,f[100005][2],g[100005][2],h[100005][2];
char type[105];
void add(int x,int y){
	vet[++cnt]=y;
	Next[cnt]=head[x];
	head[x]=cnt;
}
void dfs(int x,int fa){
	f[x][1]=p[x];
	f[x][0]=0;
	ft[x]=fa;
	for (int i=head[x]; i; i=Next[i]){
		int v=vet[i];
		if (v==fa) continue;
		dfs(v,x);
		f[x][1]=f[x][1]+min(f[v][0],f[v][1]);
		f[x][0]=f[x][0]+f[v][1];
	}
	if (c[x]==0) f[x][1]=inf;
	if (c[x]==1) f[x][0]=inf;
}
void calc(int x,int fa){
	g[x][1]=p[x];
	g[x][0]=0;
	for (int i=head[x]; i; i=Next[i]){
		int v=vet[i];
		if (v==fa) continue;
		calc(v,x);
		g[x][1]=g[x][1]+min(g[v][0],g[v][1]);
		g[x][0]=g[x][0]+g[v][1];
	}
	if (c[x]==0) g[x][1]=inf;
	if (c[x]==1) g[x][0]=inf;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,type);
	for (int i=1; i<=n; i++) scanf("%d",&p[i]);
	for (int i=1; i<n; i++){
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	for (int i=1; i<=n; i++) c[i]=2;
	if (n<=2000 && m<=2000){
		for (int i=1; i<=m; i++){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			c[a]=x;
			c[b]=y;
			dfs(1,0);
			c[a]=2;
			c[b]=2;
			ans=min(f[1][0],f[1][1]);
			if (ans<inf) printf("%lld\n",ans);
			else printf("-1\n");
		}
	}
	else
		if (type[0]=='B'){
			dfs(1,0);
			for (int i=1; i<=m; i++){
				int a,x,b,y;
				scanf("%d%d%d%d",&a,&x,&b,&y);
				c[a]=x;
				c[b]=y;
				now=b;
				g[b][0]=f[b][0];
				g[b][1]=f[b][1];
				if (y==1) f[b][0]=inf;
				else f[b][1]=inf;
				while (now!=1){
					g[ft[now]][0]=f[ft[now]][0];
					g[ft[now]][1]=f[ft[now]][1];
					f[ft[now]][0]=f[ft[now]][0]-g[now][1]+f[now][1];
					f[ft[now]][1]=f[ft[now]][1]-min(g[now][0],g[now][1])+min(f[now][0],f[now][1]);
					now=ft[now];
				}
				printf("%lld\n",f[1][1]);
				now=b;
				while (now!=0){
					f[now][0]=g[now][0];
					f[now][1]=g[now][1];
					now=ft[now];
				}
			}
		}
		else
			if (type[0]=='A' && type[1]=='2'){
				dfs(1,0);
				calc(n,0);
				for (int i=1; i<=m; i++){
					int a,x,b,y;
					scanf("%d%d%d%d",&a,&x,&b,&y);
					if (ft[b]==a) swap(a,b),swap(x,y);
					h[a][0]=f[a][0];
					h[a][1]=f[a][1];
					if (x==0) f[a][1]=inf;
					else f[a][0]=inf;
					if (y==0) ans=g[b-1][1]+f[a][1];
					else ans=p[b]+min(g[b-1][0],g[b-1][1])+min(f[a][0],f[a][1]);
					f[a][0]=h[a][0];
					f[a][1]=h[a][1];
					if (ans<inf) printf("%lld\n",ans);
					else printf("-1\n"); 
				}
			}
	fclose(stdin); fclose(stdout);
	return 0;
}
