#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int mod=1e9+7;
ll ans;
int n,m;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	if (n==1){
		ans=1;
		for (int i=1; i<=m; i++) ans=ans*2%mod;
	}
	if (n==2){
		ans=1;
		for (int i=1; i<=m-1; i++) ans=ans*3%mod;
		ans=ans*4%mod;
	}
	if (n==3){
		ans=112;
	}
	printf("%lld\n",ans);
	fclose(stdin); fclose(stdout);
	return 0;
}
