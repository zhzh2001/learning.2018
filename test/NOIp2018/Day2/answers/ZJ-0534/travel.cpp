#include<bits/stdc++.h>
using namespace std;
int s,idx,dfn[5005],low[5005],col[5005],sum[5005];
int n,m,ms,mf,top,q[5005],mat[5005][5005];
bool op,flag[5005],vis[5005];
void dfs(int x){
	q[++top]=x;
	flag[x]=1;
	for (int i=1; i<=n; i++)
		if (mat[x][i] && !flag[i]) dfs(i);
}
void tarjan(int x,int fa){
	dfn[x]=low[x]=++idx;
	vis[x]=1;
	q[++top]=x;
	for (int i=1; i<=n; i++)
		if (mat[x][i] && i!=fa){
			if (!dfn[i]){
				tarjan(i,x);
				low[x]=min(low[x],low[i]);
			}
			else low[x]=min(low[x],dfn[i]);
		}
	if (dfn[x]==low[x]){
		s++;
		while (q[top]!=x){
			col[q[top]]=s;
			vis[q[top]]=0;
			sum[s]++;
			top--;
		}
		col[x]=s;
		vis[x]=0;
		sum[s]++;
		top--;
	}
}
void solve(int x){
	q[++top]=x;
	flag[x]=1;
	if (op && vis[x]){
		mf=0;
		for (int i=1; i<=n; i++)
			if (mat[x][i] && !flag[i]){
				if (mf){
					ms=i;
					break;
				}
				if (vis[i]) mf=1;
			}
		for (int i=1; i<=n; i++)
			if (mat[x][i] && !flag[i]){
				if (i<ms) solve(i);
				else
					if (op){
						op=0;
						return;
					}
			}
		for (int i=1; i<=n; i++)
			if (mat[x][i] && !flag[i]) solve(i);
		return;
	}
	for (int i=1; i<=n; i++)
		if (mat[x][i] && !flag[i]) solve(i);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=m; i++){
		int x,y;
		scanf("%d%d",&x,&y);
		mat[x][y]=mat[y][x]=1;
	}
	if (m==n-1) dfs(1);
	else{
		tarjan(1,0);
		for (int i=1; i<=n; i++)
			if (sum[col[i]]>1) vis[i]=1;
		ms=n;
		op=1;
		solve(1);
	}
	for (int i=1; i<=top; i++) printf("%d%c",q[i],i==top?'\n':' ');
	fclose(stdin); fclose(stdout);
	return 0;
}
