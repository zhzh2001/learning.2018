#include<bits/stdc++.h>
#define M 1000000007
using namespace std;
int n,m,answer,las,a[101][101],flag,kk;
void make1(int x,int y,int p)
{
	if (flag) return;
	if (x>n) return;
	if (y>m) return;
	p=p*2+a[x][y];
	if ((x==n)&&(y==m))
	{
		if (p<las)
		{
			flag=true;
			return;
		}
		else las=p;
	}
	make1(x,y+1,p);
	make1(x+1,y,p);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int k=0;k<=(1<<n*m)-1;k++)
	{
		kk=k;flag=false;las=0;
		for (int i=1;i<=n;i++)
		{
			for (int j=1;j<=m;j++)
			{
				a[i][j]=kk%2;
				kk/=2;
			}
		}
		make1(1,1,0);
		if (!flag) answer=(answer+1)%M;
	}
	printf("%d",answer);
	return 0;
}
