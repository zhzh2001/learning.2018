#include<cmath>
#include<queue>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
priority_queue<int>Q[5010];
int a[5010],tot,n,m,du[5010],b[5010],tb;
bool d[5010][5010],vis[5010],f[5010];
void dfs(int u,int fa)
{
	while (!Q[u].empty())
	{
		int v=-Q[u].top(); Q[u].pop();
		if (v==fa) continue;
		a[++tot]=v;
		dfs(v,u);
	}
	return;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m==n-1)
	{
		for (int i=1;i<=m;i++)
		{
			int x,y;
			scanf("%d%d",&x,&y);
			d[x][y]=d[y][x]=1;
			Q[x].push(-y);
			Q[y].push(-x);
		}
		a[1]=1; tot=1;
		dfs(1,0);
		for (int i=1;i<=tot;i++) printf("%d ",a[i]);
	}
	else
	{
		for (int i=1;i<=m;i++)
		{
			int x,y;
			scanf("%d%d",&x,&y);
			d[x][y]=d[y][x]=1;
			du[x]++; du[y]++;
		}
		bool FLAG=1;
		for (int i=1;i<=n;i++)
			if (du[i]>2) {FLAG=0; break;}
		if (FLAG)
		{
			int FA=0,NOW=1,MIN=1000000000;
			for (int i=1;i<=n;i++)
				if (d[1][i] && i<=MIN) MIN=i;
			a[1]=1; FA=1; NOW=MIN; tot=1;
			for (int i=2;i<=n;i++)
			{
				a[++tot]=NOW;
				for (int j=1;j<=n;j++)
					if (d[NOW][j] && j!=FA)
					 {FA=NOW; NOW=j; break;}
			}
			for (int i=1;i<=tot;i++) printf("%d ",a[i]);
			return 0;
		}
		memset(vis,0,sizeof(vis));
		memset(f,0,sizeof(f));
		a[1]=1; tot=1; vis[1]=1;
		for (int i=1;i<=n;i++)
			if (d[1][i]) f[i]=1;
		for (int i=2;i<=n;i++)
		{
			int MIN=1000000000;
			for (int j=1;j<=n;j++)
				if (!vis[j] && f[j])
					MIN=min(MIN,j);
			vis[MIN]=1;
			a[++tot]=MIN;
			for (int i=1;i<=n;i++)
				if (d[MIN][i]) f[i]=1;
		}
		for (int i=1;i<=tot;i++) printf("%d ",a[i]);	
	}
	return 0;
}
