#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define mod 1000000007
using namespace std;
int n,m,pw2[2000010],pw3[2000010];
ll ans;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==3 && m==2) {puts("144"); return 0;}
	if (n==3 && m==1) {puts("8"); return 0;}
	pw2[0]=1; for (int i=1;i<=2000000;i++) pw2[i]=pw2[i-1]*2%mod;
	pw3[0]=1; for (int i=1;i<=2000000;i++) pw3[i]=pw3[i-1]*3%mod;
	if (m==2) swap(n,m);
	if (n==2)
	{
		ans=pw2[m*2];
		for (int i=2;i<=m;i++)
			ans=(ans-pw2[(m-i+1)*2]*pw3[i-2]%mod+mod)%mod;
		printf("%lld\n",ans);
		return 0;
	}
	if (n==3 && m==3) {puts("112"); return 0;}
	if (n==5 && m==5) {puts("7136");return 0;}
	return 0;
}
