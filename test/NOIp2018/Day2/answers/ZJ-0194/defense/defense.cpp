#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
using namespace std;
struct edge{int v,nxt;}e[200010];
int n,m,cnt,head[100010],ok[100010],p[100010];
ll dp[100010][3];
void add_edge(int u,int v)
{
	e[++cnt].v=v;e[cnt].nxt=head[u];head[u]=cnt;
	e[++cnt].v=u;e[cnt].nxt=head[v];head[v]=cnt;
}
void dfs(int u,int fa)
{
	dp[u][1]=p[u];
	if (ok[u]==1) dp[u][1]=-1;
	if (ok[u]==2) dp[u][0]=-1;
	for (int i=head[u];i;i=e[i].nxt)
	{
		int v=e[i].v; if (v==fa) continue;
		dfs(v,u);
		if (dp[u][1]!=-1)
		{
			if (dp[v][0]==-1 && dp[v][1]==-1) dp[u][1]=-1;
			else if (dp[v][0]==-1 && dp[v][1]!=-1) dp[u][1]+=dp[v][1];
			else if (dp[v][0]!=-1 && dp[v][1]==-1) dp[u][1]+=dp[v][0];
			else if (dp[v][0]!=-1 && dp[v][1]!=-1) dp[u][1]+=min(dp[v][1],dp[v][0]);
		}
		if (dp[u][0]!=-1)
		{
			if (dp[v][1]==-1) dp[u][0]=-1;
			else dp[u][0]+=dp[v][1];
		}
	}
	return;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char s[10];
	scanf("%d%d%s",&n,&m,s);
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		add_edge(x,y);
	}
	while (m--)
	{
		int X,x,Y,y;
		scanf("%d%d%d%d",&X,&x,&Y,&y);
		memset(ok,0,sizeof(ok));
		memset(dp,0,sizeof(dp));
		if (x) ok[X]=2; else ok[X]=1;
		if (y) ok[Y]=2; else ok[Y]=1;
		dfs(1,0);
		if (dp[1][0]==-1 && dp[1][1]==-1) puts("-1");
		else if (dp[1][0]==-1 && dp[1][1]!=-1) printf("%lld\n",dp[1][1]);
		else if (dp[1][0]!=-1 && dp[1][1]==-1) printf("%lld\n",dp[1][0]);
		else if (dp[1][0]!=-1 && dp[1][1]!=-1) printf("%lld\n",min(dp[1][0],dp[1][1]));
	}
	return 0;
}
