#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const LL kcz=1000000000+7;
const int MAXN=8;
const int MAXNN=1<<(2*MAXN);
LL f[2][MAXNN+10];
void upd(LL& a,LL b)
{
	a=(a+b)%kcz;
}
LL ffx[MAXNN+10];
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n;
	n=8;
	LL last=0;
	for(int m=1; m<=30; ++m) {
		memset(f,0,sizeof(f));
		int nn=1<<n;
		for(int S=0; S<nn; ++S) {
			f[1][S]=1;
		}
		for(int i=2; i<=m; ++i) {
			int ii=i%2;
			memset(f[ii],0,sizeof(LL)*(1+nn*nn));
			for(int j=0; j<nn; ++j) {
				for(int k=0; k<nn*nn; ++k) {
					if (f[ii^1][k]==0) continue;
					bool flag=false;
					for(int p=0; p<n-1; ++p) {
						if ((k&(1<<(p+1)))==0 && (j&(1<<p))>0) {
							flag=true;
							break;
						}
					}
					if (flag) continue;
					for(int p=0; p<n-1; ++p) {
						if ((k&(1<<(p+n)))>0 && ((k&(1<<(p+1)))==0)!=((j&(1<<p))==0)) {
							flag=true;
							break;
						}
					}
					if (flag) continue;
					int S=0;
					bool last;
					for(int p=0; p<n; ++p) {
						if ((k&(1<<(p+n)))>0 || (p>0 && last) || (p>0 && (((k&(1<<(p)))==0)==((j&(1<<(p-1)))==0)))) {
							last=true;
							S|=(1<<p);
						} else {
							last=false;
						}
					}
					upd(f[ii][j|(S<<n)],f[ii^1][k]);
				}
			}
		}
		LL ans=0;
		cerr<<m<<' ';
		for(int i=0; i<nn*nn; ++i) {
			upd(ans,f[n%2][i]);
			upd(ffx[i&(nn-1)],f[n%2][i]);
		}
		if (ans!=last) {
			cerr<<double(ans)/last<<" ";
			last=ans;
		}
		cerr<<ans<<endl;
	}
	return 0;
}

