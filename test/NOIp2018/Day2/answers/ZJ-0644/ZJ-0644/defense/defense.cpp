#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const int MAXN=2000;
struct Edge
{
	int to,next;
}e[2*MAXN+10];
int head[MAXN+10];
void addEdge(int a,int b)
{
	static int c=0;
	e[++c]=(Edge){b,head[a]};
	head[a]=c;
	e[++c]=(Edge){a,head[b]};
	head[b]=c;
}
LL p[MAXN+10];
const LL INF=LLONG_MAX/100;
char ty[10];
bool vis[MAXN+10];
LL f[MAXN+10][2];
int a,x,b,y;
LL dfs(int now,int fa,int typ)
{
	vis[now]=true;
	if (f[now][typ]!=-1) return f[now][typ];	
	LL res=0;
	if (typ==1) {
		res+=p[now];
	}
	for(int i=head[now]; i!=0; i=e[i].next) {
		int v=e[i].to;
		if (v==fa) continue;
		if (v==b) {
			if (y==0 && typ==0) return INF;
			continue;
		}
		if (typ==0) {
			res+=dfs(v,now,1);	
		} else {
			LL mn=min(dfs(v,now,0),dfs(v,now,1));
			res+=mn;
		}
		if (res>INF) return INF;
	}
	return f[now][typ]=res;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m;
	scanf("%d%d%s",&n,&m,ty);
	for(int i=1; i<=n; ++i) {
		scanf("%lld",&p[i]);
	}
	for(int i=1; i<n; ++i) {
		int a,b;
		scanf("%d%d",&a,&b);
		addEdge(a,b);
	}
	for(int i=1; i<=m; ++i) {
		memset(f,-1,sizeof(f));
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if (x==0 && y==0) {
			bool flag=false;
			for(int j=head[a]; j!=0; j=e[j].next) {
				if (e[j].to==b) {
					flag=true;
					break;
				}
			}
			if (flag) {
				puts("-1");
				continue;
			}
		}
		memset(vis,false,sizeof(vis));
		LL ans=dfs(a,0,x);	
		if (y==1) ans+=p[b];
		for(int j=head[b]; j!=0; j=e[j].next) {
			int v=e[j].to;
			if (!vis[v]) {
				if (y==0) {
					ans+=dfs(v,b,1);
				} else {
					LL mn=min(dfs(v,b,0),dfs(v,b,1));
					ans+=mn;
				}
			}
		}
		printf("%lld\n",ans);
	}
	return 0;
}

