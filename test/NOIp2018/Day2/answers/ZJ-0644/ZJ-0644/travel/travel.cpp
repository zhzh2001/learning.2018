#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const int MAXN=5000;
bool is_huan[MAXN+10];
bool b[MAXN+10];
int huan_top;
vector<int> e[MAXN+10];
void addEdge(int a,int b)
{
	e[a].push_back(b);
	e[b].push_back(a);
}
bool gflag=false;
int anslist[MAXN+10],cnt;
void dfs_huan(int,int);
void dfs_tree(int now,int fa)
{
	b[now]=false;
	anslist[++cnt]=now;
	for(size_t i=0; i<e[now].size(); ++i) {
		int v=e[now][i];
		if (v!=fa && b[v]) {
			if (is_huan[v] && gflag) {
				dfs_huan(v,now);
			} else {
				dfs_tree(v,now);
			}
		}
	}
}
void dfs_huan(int s,int s_fa)
{
	anslist[++cnt]=s;
	b[s]=false;
	for(size_t i=0; i<e[s].size(); ++i) {
		int v=e[s][i];
		if (!is_huan[v] || !gflag) {
			if (v!=s_fa)
				dfs_tree(v,s);
		}  else {
			int upb=e[s][i+1];
			if (upb==s_fa) upb=e[s][i+2];
			int now=v;
			int last=s;
			stack<int> st;
			while(now<upb) {
				st.push(now);
				anslist[++cnt]=now;
				b[now]=false;
				int nxt=-1;
				for(size_t j=0; j<e[now].size(); ++j) {
					int v=e[now][j];
					if (v==last) continue;
					if (is_huan[v]) {
						nxt=v;
						if (j+1<e[now].size()) {
							if (e[now][j+1]!=last) {
								upb=e[now][j+1];
							} else if (j+2<e[now].size()) {
								upb=e[now][j+2];
							}
						}
						break;
					} else {
						dfs_tree(v,now);
					}
				}
				last=now;
				now=nxt;
			}
			gflag=false;
			while(!st.empty()) {
				int now=st.top(); st.pop();
				for(size_t j=0; j<e[now].size(); ++j) {
					int v=e[now][j];
					if (!is_huan[v] && b[v]) {
						dfs_tree(v,now);
					}
				}
			}
		}
	}

}
bool find_huan(int now,int fa)
{
	b[now]=false;
	for(size_t i=0; i<e[now].size(); ++i) {
		int v=e[now][i];
		if (v!=fa) {
			if (!b[v]) {
				huan_top=v;
				is_huan[now]=true;
				return true;
			}
			if (find_huan(v,now)) {
				is_huan[now]=true;
				if (huan_top==now) return false;
				return true;
			}
		}
	}
	return false;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1; i<=m; ++i) {
		int a,b;
		scanf("%d%d",&a,&b);
		addEdge(a,b);
	}
	for(int i=1; i<=n; ++i) {
		sort(e[i].begin(),e[i].end());
	}
	memset(b,true,sizeof(b));
	if (m==n-1) {
		dfs_tree(1,0);
	} else {
		gflag=true;
		find_huan(1,0);
		memset(b,true,sizeof(b));
		if (is_huan[1]) {
			dfs_huan(1,0);
		} else {
			dfs_tree(1,0);
		}
	}
	for(int i=1; i<n; ++i) {
		printf("%d ",anslist[i]);
	}
	printf("%d\n",anslist[n]);
	return 0;
}

