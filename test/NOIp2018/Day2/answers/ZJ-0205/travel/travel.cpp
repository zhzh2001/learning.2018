#include <iostream>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <cstdio>
#include <vector>
#include <queue>
#include <map>
#include <set>
using namespace std;
int n,m,top=-1,cir[5011],vis[5011],ans[5011],len=0,cirst=0,cired=0,fat[5011];
int g[5011][5011],another;
void dfs1(int now)
{
	vis[now]=1;
	ans[++len]=now;
	for(int i=1;i<=n;i++)if(!vis[i]&&g[now][i]) dfs1(i);
}
void fincir(int now,int fa)
{
	vis[now]=1;
	for(int i=1;i<=n;i++)
	{
		if(!g[now][i]) continue;
		if(i==fa) continue;
		if(!vis[i])
		{
			fat[i]=now;fincir(i,now);
			if(cirst) return;
		} 
		else
		{
			cirst=i;cired=now;
			return;
		}
	}
}
void dfs2(int now)
{
	if(!vis[now])ans[++len]=now;
	vis[now]=1;
	if(now==cirst)
	{
		for(int i=n;i>=1;i--)
		{
			if(g[now][i]&&cir[i])
			{
				another=i;
				break;
			}
		}
		for(int i=1;i<=n;i++)
		{
			if(g[now][i])
			{
				g[now][i]=0;
				dfs2(i);
			} 
		} 
	}
	else if(cir[now]==1)
	{
		int mi;
		for(int i=1;i<=n;i++)if(g[now][i]){mi=i;break;}
		if(another==0||mi<another)for(int i=1;i<=n;i++)if(g[now][i]){g[now][i]=0;dfs2(i);}
		else
		{
			int j=now;
			while(j)
			{
				if(j==cirst) break;
				g[j][fat[j]]=0;
				j=fat[j];
			}
			g[cirst][another]=0;
			int tp=another;another=0;
			dfs2(tp);
		}
	}
	else
	{
		for(int i=1;i<=n;i++)
	    {
		    if(g[now][i])
		    {
			    g[now][i]=0;
			    dfs2(i);
		    } 
	    } 
	} 
	
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int i,j,from,to;
	scanf("%d%d",&n,&m);
	for(i=1;i<=m;i++)
	{
		scanf("%d%d",&from,&to);
		g[from][to]=g[to][from]=1;
	}
	if(m==n)
	{
		memset(vis,0,sizeof vis);
		fincir(1,0);j=cired;
		while(j)
		{
			cir[j]=1;
			if(j==cirst) break;
			j=fat[j];
		}
		memset(vis,0,sizeof vis);
		dfs2(1);
		for(i=1;i<n;i++) printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	} 
	else
	{
		dfs1(1);
		for(i=1;i<n;i++) printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}
	return 0;
}
