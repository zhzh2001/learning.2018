#include <iostream>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <cstdio>
#include <vector>
#include <queue>
#include <map>
#include <set>
using namespace std;
int n,m,top=-1,a,sx,b,sy;
long long dp[100011][2],p[100011],ans;
char tp[101];
long long minn(long long p1,long long p2){return p1<p2?p1:p2;}
struct edge
{
	int st,ed;
	edge* nest;
}e[200011],*v[100011];
void addedge(int st,int ed)
{
	e[++top].st=st;
	e[top].ed=ed;
	e[top].nest=v[st];
	v[st]=&e[top];
}
void dfs(int now,int fa)
{
	edge* ne;int flag=0;long long sum1=0,sum0=0,pls;
	for(ne=v[now];ne;ne=ne->nest)
	{
		if(ne->ed==fa) continue;
		flag=1;
		dfs(ne->ed,now);
		if(now==a)
		{
			if(sx==1)
			{
				pls=minn(dp[ne->ed][1],dp[ne->ed][0]);
				sum1+=pls;sum0+=1e8;
			}
			else{pls=dp[ne->ed][1];sum0+=pls;sum1+=1e8;} 
		}
		else if(now==b)
		{
			if(sy==1)
			{
				pls=minn(dp[ne->ed][1],dp[ne->ed][0]);
				sum1+=pls;sum0+=1e8;
			}
			else{pls=dp[ne->ed][1];sum0+=pls;sum1+=1e8;}
		}
		else
		{
			pls=minn(dp[ne->ed][1],dp[ne->ed][0]); sum1+=pls;
			pls=dp[ne->ed][1]; sum0+=pls;
		}
	}
	if(flag==0)
	{
		if(a==now&&sx==1||b==now&&sy==1||a!=now&&b!=now) dp[now][1]=p[now];
		if(a==now&&sx==0||b==now&&sy==0||a!=now&&b!=now) dp[now][0]=0;
	}
	else
	{
		dp[now][1]=minn(dp[now][1],sum1+p[now]);
		dp[now][0]=minn(dp[now][0],sum0);
	}
	//cout<<now<<" "<<dp[now][1]<<" "<<dp[now][0]<<endl;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int i,j,from,to;
	scanf("%d%d",&n,&m);
	scanf("%s",tp);
	for(i=1;i<=n;i++) scanf("%d",&p[i]);
	for(i=1;i<=n-1;i++)
	{
		scanf("%d%d",&from,&to);
		addedge(from,to);addedge(to,from);
	}
	for(i=1;i<=m;i++)
	{
		for(j=1;j<=n;j++) dp[j][0]=dp[j][1]=1e8;
		scanf("%d%d%d%d",&a,&sx,&b,&sy);
		dfs(1,0);
		if(a==1) ans=dp[1][sx];
		else if(b==1) ans=dp[1][sy];
		else ans=minn(dp[1][1],dp[1][0]);
		if(ans>1e7) printf("-1\n");
		else printf("%lld\n",ans);
	}
	return 0;
}
