#include <iostream>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <cstdio>
#include <vector>
#include <queue>
#include <map>
#include <set>
using namespace std;
int n,m,c=0;
long long dp[256][2],ans=0,mo=1e9+7;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int i,j,k,nowzt;
	scanf("%d%d",&n,&m);
	memset(dp,0,sizeof dp);
	for(i=0;i<(1<<n);i++)dp[i][c]=1;
	for(j=1;j<m;j++)
	{
		for(k=0;k<(1<<n);k++) dp[k][c^1]=0;
		for(i=0;i<(1<<n);i++)
		{
			//nowzt=(i>>1);nowzt|=(1<<(n-1));
			nowzt=((i>>1)|(1<<(n-1)));
			for(k=0;k<=nowzt;k++)
			{
				if((nowzt&k)==k)
				{
					//cout<<k<<" "<<i<<endl;
					dp[k][c^1]=(dp[i][c]+dp[k][c^1])%mo;
				} 
			}
		}
		c^=1;
	}
	ans=0;
	for(i=0;i<(1<<n);i++)
	{
		//cout<<i<<" "<<dp[i][c]<<endl;
		ans=(ans+dp[i][c])%mo;
	} 
	cout<<ans;
	//i=1;n=3;
	//cout<<((i>>1)|(1<<(n-1)));
	return 0;
}
