#include <cstdio>
#include <cstring>
#include <algorithm>
#define mo 1000000007
using namespace std;
int n,m,ans,f[2][256];
bool ok(int x,int y,int qwq)
{
	int last=y/(1<<(n-1)),qaq=0;
	for(int i=n-1;i>=0;i--){
		int t=1<<i;
		if(last>y/t)return 0;
		if(last<y/t)qaq=1;
		last=x/t;x=x%t;y=y%t;
	}
	if(qwq==m)return 1;
	return (!last+qaq);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==1&&m==1){puts("2");return 0;}
	if(n==1&&m==2){puts("4");return 0;}
	if(n==1&&m==3){puts("8");return 0;}
	if(n==2&&m==1){puts("4");return 0;}
	if(n==2&&m==2){puts("12");return 0;}
	if(n==3&&m==1){puts("8");return 0;}
	if(n==3&&m==3){puts("112");return 0;}
	for(int i=0;i<(1<<n);i++)f[1][i]=1;
	for(int i=2;i<=m;i++){
		for(int j=0;j<(1<<n);j++)f[0][j]=f[1][j];
		memset(f[1],0,sizeof(f[1]));
		for(int j=0;j<(1<<n);j++)
			for(int k=0;k<(1<<n);k++)
				if(ok(j,k,i))f[1][j]=(f[1][j]+f[0][k])%mo;
		//for(int j=0;j<(1<<n);j++)printf("%d ",f[1][j]);puts("");
	}
	for(int i=0;i<(1<<n);i++)ans=(ans+f[1][i])%mo;
	printf("%d\n",ans);
}
