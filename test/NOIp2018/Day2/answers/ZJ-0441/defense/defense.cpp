#include <cstdio>
#include <cstring>
#include <algorithm>
#define N 4006
#define inf 1000000000
#define Min(a,b) (a>b?b:a)
using namespace std;
struct arr{int ne,to;}e[N];
int n,m,x,xx,y,yy,cnt,ans,c[2],a[N],h[N],f[N][3];
void add(int x,int y)
{
	e[++cnt].ne=h[x];e[cnt].to=y;h[x]=cnt;
	e[++cnt].ne=h[y];e[cnt].to=x;h[y]=cnt;
}
bool wk(int w,int fa)
{
	int yes=1;
	for(int i=h[w];i;i=e[i].ne){
		int son=e[i].to;
		if(son!=fa){
			wk(son,w);yes=0;
			f[w][0]=Min(inf,f[w][0]+Min(f[son][0],f[son][1]));
			f[w][1]=Min(inf,f[w][1]+f[son][0]);
		}
	}
	f[w][0]=Min(inf,f[w][0]+a[w]);
	if(w==x&&xx==0||w==y&&yy==0)f[w][0]=inf;
	if(w==x&&xx==1||w==y&&yy==1)f[w][1]=inf;
	//printf("%d %d %d %d %d\n",w,fa,f[w][0],f[w][1]);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);scanf("%s",c);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	for(int i=1;i<n;i++)scanf("%d%d",&x,&y),add(x,y);
	for(int i=1;i<=m;i++){
		scanf("%d%d%d%d",&x,&xx,&y,&yy);
		for(int i=1;i<N;i++)f[i][0]=f[i][1]=0;
		wk(1,0);ans=Min(f[1][0],f[1][1]);
		if(ans<inf)printf("%d\n",ans);else puts("-1");
	}
}
