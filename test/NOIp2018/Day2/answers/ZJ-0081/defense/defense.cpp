#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

typedef long long ll;

const int maxn = 1e5 + 5;
const ll INF = 1e12;

int n, m, edgenum, u, v, a, b, x, y;
int p[maxn], head[maxn], must[maxn];
ll ans;
ll dp[maxn][2];
char type[5];

struct EDGE {
	int Next, vet;
} edge[maxn << 1];

inline int read() {
	int ans = 0;
	char ch = getchar(), last = ' ';
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	while (ch >= '0' && ch <= '9') ans = (ans << 1) + (ans << 3) + ch - '0', ch = getchar();
	return last == '-' ? -ans : ans;
}

void dfs(int now, int pre) {
	int nxt;
	dp[now][0] = 0;
	dp[now][1] = p[now];
	for (int e = head[now]; e != -1; e = edge[e].Next) {
		nxt = edge[e].vet;
		if (nxt != pre) {
			dfs(nxt, now);
			dp[now][0] += dp[nxt][1];
			dp[now][1] += min(dp[nxt][0], dp[nxt][1]);
		}
	}
	if (must[now] == 0) dp[now][1] = INF;
	if (must[now] == 1) dp[now][0] = INF;
}

inline void clear() {
	edgenum = 0;
	memset(head, -1, sizeof(head));
}

inline void add_edge(int u, int v) {
	edge[++edgenum] = (EDGE) {head[u], v};
	head[u] = edgenum;
}

int main () {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	n = read(), m = read(), scanf("%s", type);
	for (int i = 1; i <= n; ++i) p[i] = read();
	clear();
	for (int i = 1; i < n; ++i) {
		u = read(), v = read();
		add_edge(u, v), add_edge(v, u);
	}
//	if (n <= 2000 && m <= 2000) {
		memset(must, -1, sizeof(must));
		for (int i = 1; i <= m; ++i) {
			a = read(), x = read(), b = read(), y = read();
			must[a] = x, must[b] = y;
			dfs(1, 1);
			must[a] = must[b] = -1;
			ans = min(dp[1][0], dp[1][1]);
			if (ans >= INF) puts("-1");
			else printf("%lld\n", ans);
		}
		return 0;
//	}
//	return 0;
}
