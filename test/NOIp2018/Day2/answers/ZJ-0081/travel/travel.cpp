#include <stdio.h>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn = 5e3 + 5;
const int INF = 1e9;

int n, m, edgenum, num, findu, findv, k, x, y, tot, mn;
int belong[maxn], fa[maxn], dep[maxn], head[maxn], p[maxn], ans[maxn][maxn];
bool flag[maxn];

struct node {
	int u, v;
} circle[maxn], E[maxn];

struct EDGE {
	int Next, vet;
} edge[maxn << 1];

inline int read() {
	int ans = 0;
	char ch = getchar(), last = ' ';
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	while (ch >= '0' && ch <= '9') ans = (ans << 1) + (ans << 3) + ch - '0', ch = getchar();
	return last == '-' ? -ans : ans;
}

void dfs(int now, int pre) {
	int t = 0;
	int a[maxn];
	fa[now] = pre, dep[now] = dep[pre] + 1;
	ans[tot][++p[tot]] = now;
	for (register int e = head[now]; e != -1; e = edge[e].Next) {
		int nxt = edge[e].vet;
		if (nxt != pre) a[++t] = nxt;
	}
	sort(a + 1, a + 1 + t);
	for (register int i = 1; i <= t; ++i) dfs(a[i], now);
}

inline int LCA(int u, int v) {
	memset(flag, false, sizeof(flag));
	while (u != 1) {
		flag[u] = true;
		u = fa[u];
	}
	while (v != 1) {
		if (flag[v]) break;
		v = fa[v];
	}
	return v;
}

int find(int x) {
	if (x != belong[x]) belong[x] = find(belong[x]);
	return belong[x];
}

inline void clear() {
	edgenum = 0;
	memset(head, -1, sizeof(head));
}

inline void add_edge(int u, int v) {
	edge[++edgenum] = (EDGE) {head[u], v};
	head[u] = edgenum;
}
	
int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	n = read(), m = read();
	num = 0;
	clear();
	for (register int i = 1; i <= n; ++i) belong[i] = i;
	for (register int i = 1; i <= m; ++i) {
		E[i].u = read(), E[i].v = read();
		if (E[i].u < E[i].v) swap(E[i].u, E[i].v);
		findu = find(E[i].u), findv = find(E[i].v);
		if (findu != findv) {
			belong[findu] = findv;
			add_edge(E[i].u, E[i].v), add_edge(E[i].v, E[i].u);
		} else circle[++num] = E[i];
	}
	tot = 1;
	dfs(1, 1);
	if (m == n - 1) {
		for (register int i = 1; i < n; ++i) printf("%d ", ans[1][i]);
		printf("%d\n", ans[1][n]);
	}
	if (m == n) {
		k = LCA(circle[num].u, circle[num].v);
		x = circle[num].u, y = circle[num].v;
		while (x != k) {
			circle[++num] = (node) {x, fa[x]};
			x = fa[x];
		}
		while (y != k) {
			circle[++num] = (node) {y, fa[y]};
			y = fa[y];
		}
		for (register int i = 1; i <= num; ++i) {
			clear();
			if (circle[i].u < circle[i].v) swap(circle[i].u, circle[i].v);
			for (register int j = 1; j <= m; ++j)
				if ((E[j].u != circle[i].u || E[j].v != circle[i].v)) add_edge(E[j].u, E[j].v), add_edge(E[j].v, E[j].u);
			++tot;
			dfs(1, 1);
		}
		memset(flag, true, sizeof(flag));
		for (register int round = 1; round <= n; ++round) {
			mn = INF;
			for (register int i = 1; i <= tot; ++i)
				if (flag[i]) mn = min(mn, ans[i][round]);
			for (register int i = 1; i <= tot; ++i)
				if (ans[i][round] > mn) flag[i] = false;
		}
		for (register int i = 1; i <= tot; ++i)
			if (flag[i]) {
				for (register int j = 1; j < n; ++j) printf("%d ", ans[i][j]);
				printf("%d\n", ans[i][n]);
				break;
			}
	}
	return 0;
}
