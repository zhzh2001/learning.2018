#include <cstdio>
#include <algorithm>

using namespace std;

typedef long long ll;

const int MAX = 1e6 + 5;
const int MOD = 1e9 + 7;

int n, m;
ll dp[2][MAX][2];

inline ll ksm(int a, int b) {
	if (b == 0) return 1;
	ll tmp = ksm(a, b >> 1);
	if (b & 1) return tmp * tmp % MOD * a % MOD;
	return tmp * tmp % MOD;
}

inline void solve(int n, int m) {
	for (int i = 1; i <= m; ++i) dp[1][i][0] = dp[1][i][1] = ksm(2, i - 1);
	for (int i = 1; i <= 2; ++i) dp[i][1][0] = dp[i][1][1] = ksm(2, i - 1);
	for (int i = 2; i <= m; ++i) dp[2][i][0] = dp[2][i][1] = (ksm(2, 2 * i - 3) + (dp[2][i - 1][0] + dp[2][i - 1][1]) % MOD) % MOD;
}

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n > m) swap(n, m);
	if (n == 1) {
		printf("%lld\n", ksm(2, m));
		return 0;
	}
	if (n <= 2) {
		solve(n, m);
		printf("%lld\n", dp[n][m][0] + dp[n][m][1]);
		return 0;
	}
	if (n == 3) {
		if (m == 1) puts("8");
		if (m == 3) puts("112");
		return 0;
	}
	if (n == 5 && m == 5) puts("7136");
	return 0;
}
