#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int n,m,totedge=1,a,b,go[5009][5009],head[5009],vv[5009],st[5009],C,key,dfn[5009],low[5009],time,tot=0,col,on[5009];
bool vis[5009];
struct EDGE{int to,next;}path[10009];
inline int min(int a,int b){return a<b?a:b;}
inline void add(int a,int b)
{
	path[++totedge].next=head[a];
	path[totedge].to=b;
	head[a]=totedge;
}
void dfs(int cur)
{
	printf("%d ",cur);
	vis[cur]=true;
	for (int i=head[cur];i;i=path[i].next)
		if (!vis[path[i].to])
			go[cur][++go[cur][0]]=path[i].to;
	sort(go[cur]+1,go[cur]+1+go[cur][0]);
	for (int i=1;i<=go[cur][0];i++)
		if (!vis[go[cur][i]])
			dfs(go[cur][i]);
}
void tarjan(int cur,int from)
{
	dfn[cur]=low[cur]=++time;
	st[++st[0]]=cur;
	for (int i=head[cur];i;i=path[i].next)
		if (!dfn[path[i].to])
		{
			tarjan(path[i].to,i);
			low[cur]=min(low[cur],low[path[i].to]);
		}
		else if (i!=(from^1)) low[cur]=min(low[cur],dfn[path[i].to]);
	if (low[cur]==dfn[cur])
	{
		on[cur]=++col;
		tot=0;
		while (st[st[0]]!=cur)
		{
			on[st[st[0]]]=col;
			st[0]--;
			tot++;
		}
		if (tot>=1) key=col;
		st[0]--;
	}
}
void dfsW(int cur)
{
	printf("%d ",cur);
	vis[cur]=true;
	for (int i=head[cur];i;i=path[i].next)
		if (!vis[path[i].to])
			go[cur][++go[cur][0]]=path[i].to;
	sort(go[cur]+1,go[cur]+1+go[cur][0]);
	if (cur==C) C=n+1;
	if (on[cur]==key && C && go[cur][1]>C && go[cur][0]==1) return;
	if (on[cur]==key && !C) C=go[cur][2];
	for (int i=1;i<=go[cur][0];i++)
		if (!vis[go[cur][i]])
		{
			dfsW(go[cur][i]);
		}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d",&a,&b);
		add(a,b);
		add(b,a);
		vv[a]++;vv[b]++;
	}
	if (m==n-1) dfs(1); else
	{
		tarjan(1,-233);
		//for (int i=1;i<=n;i++) printf("%d ",on[i]);printf("\n");
		dfsW(1);
	}
	return 0;
}
