#include <cstdio>
using namespace std;
typedef long long LL;
const LL pp=1e9+7;
int n,m,min;
LL f[1000019][19];
inline LL ksm(LL a,LL b)
{
	LL ans=1;
	while (b)
	{
		if (b&1) ans=ans*a%pp;
		b>>=1;
		a=a*a%pp;
	}
	return ans;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==3 && m==1) printf("8\n");
	else if (n==3 && m==2) printf("36\n");
	else if (n==3 && m==3) printf("112\n");
	else if (n==2) printf("%lld\n",4*ksm(3,m-1)%pp);
	else if (n==5 && m==5) printf("7136\n");
	else if (n==1) printf("%lld\n",ksm(2,m));
	return 0;
}
