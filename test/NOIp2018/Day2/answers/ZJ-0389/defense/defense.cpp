#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
typedef long long LL;
const LL INF=1e18;
int n,m,head[100009],a,b,totedge,ta,tb,tx,ty;
char type[19];
bool halt;
struct EDGE{int next,to;}path[200009];
LL f[100009][2],p[100009],f1[100009][2],f2[100009][2];
inline void add(int a,int b)
{
	path[++totedge].next=head[a];
	path[totedge].to=b;
	head[a]=totedge;
}
void dfs(int cur,int fat)
{
	f[cur][0]=0;
	f[cur][1]=p[cur];
	for (int i=head[cur];i;i=path[i].next)
		if (path[i].to!=fat)
			dfs(path[i].to,cur);
	for (int i=head[cur];i;i=path[i].next)
		if (path[i].to!=fat)
		{
			f[cur][0]+=f[path[i].to][1];
			f[cur][1]+=min(f[path[i].to][0],f[path[i].to][1]);
		}
	if (cur==ta) f[cur][tx^1]=INF;
	if (cur==tb) f[cur][ty^1]=INF;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,type);
	for (int i=1;i<=n;i++) scanf("%lld",&p[i]);
	for (int i=1;i<n;i++)
	{
		scanf("%d%d",&a,&b);
		add(a,b);
		add(b,a);
	}
	//printf("%lld\n",min(f[1][0],f[1][1]));
	if ((n<=2000 && m<=2000) || type[0]=='B')
		while (m--)
		{
			scanf("%d%d%d%d",&ta,&tx,&tb,&ty);
			halt=false;
			if (tx==0 && ty==0)
				for (int i=head[ta];i;i=path[i].next)
					if (path[i].to==tb)
					{
						printf("-1\n");
						halt=true;
						break;
					}
			if (halt) continue;
			dfs(1,0);
			printf("%lld\n",min(f[1][0],f[1][1]));
		}
	else if (type[0]=='A')
	{
		for (int i=1;i<=n;i++)
		{
			f1[i][0]=f1[i-1][1];
			f1[i][1]=min(f1[i-1][0],f1[i-1][1])+p[i];
		}
		for (int i=n;i>=1;i--)
		{
			f2[i][0]=f2[i+1][1];
			f2[i][1]=min(f2[i+1][0],f2[i+1][1])+p[i];
		}
		while (m--)
		{
			scanf("%d%d%d%d",&ta,&tx,&tb,&ty);
			if (tx==0 && ty==0) printf("-1\n"); else{
			if (ta>tb) {swap(ta,tb);swap(tx,ty);}
			printf("%lld\n",f1[ta][tx]+f2[tb][ty]);}
		}
	}
	return 0;
}
