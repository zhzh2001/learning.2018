uses math;
var
  n,m,i,a,x,b,y,tot,kk,kkk:longint;
  ans:int64;
  pp,p,q,head,next,vet:array[0..1000000] of longint;
  ss:string;
  vis:array[0..1000000] of boolean;

procedure build(x,y:longint);
begin
  inc(tot);
  next[tot]:=head[x];head[x]:=tot;vet[tot]:=y;
end;

procedure dfs(u,v:longint);
begin
  if u=n then
    begin
      if v<ans then ans:=v;
      exit;
    end;
  if pp[u+1]=1 then
    begin
      if (q[u]=0) and (q[u-1]=0) then exit;
      if (q[u]=0) and (u=n-1) then exit;

      q[u+1]:=0;
      dfs(u+1,v);
      exit;
    end;
  if pp[u+1]=2 then
    begin
      q[u+1]:=1;
      dfs(u+1,v+p[u+1]);
      exit;
    end;
  if q[u]=1 then
    begin
      q[u+1]:=0;
      dfs(u+1,v);
      q[u+1]:=1;
      dfs(u+1,v+p[u+1]);
      exit;
    end;
  if u=n-1 then
    begin
      q[u+1]:=1;
      dfs(u+1,v+p[n]);
      exit;
    end;
  if q[u-1]=0 then
    begin
      q[u+1]:=1;
      dfs(u+1,v+p[u+1]);
    end
    else
    begin
      q[u+1]:=0;
      dfs(u+1,v);
      q[u+1]:=1;
      dfs(u+1,v+p[u+1]);
      exit;
    end;
end;

function dfs1(u,fa,v:longint):longint;
var
  i,vv,sum,k,k1:longint;
  flag:boolean;
begin
  i:=head[u];
  if (vet[i]=fa) and (next[i]=0) then exit(v);
  sum:=0;
  while i>0 do
    begin
      vv:=vet[i];
      if vv=fa then
        begin
          i:=next[i];
          continue;
        end;
      if (pp[vv]=1) then
        begin
          if (q[u]=0) and (q[fa]=0) then
            begin
              i:=next[i];
              continue;
            end;
          if (head[vv]=0) and (q[u]=0) then
            begin
              i:=next[i];
              continue;
            end;
          vis[vv]:=true;
          q[vv]:=0;
          k:=dfs1(vv,u,v);
          if k<>-1 then sum:=sum+k;
          vis[vv]:=false;
          i:=next[i];
          continue;
        end;
      if pp[vv]=2 then
        begin
          vis[vv]:=true;
          q[vv]:=1;
          k:=dfs1(vv,u,v+p[vv]);
          if k>-1 then sum:=sum+k;
          vis[vv]:=false;
          i:=next[i];
          continue;
        end;
      if q[u]=0 then
        begin
          if (q[fa]=0) or ((vet[head[vv]]=u) and (next[head[vv]]=0)) then
            begin
              q[vv]:=1;
              vis[vv]:=true;
              k:=dfs1(vv,u,v+p[vv]);
              if k>-1 then sum:=sum+k;
              vis[vv]:=false;
            end
            else
            begin
              q[vv]:=0;
              vis[vv]:=true;
              k:=dfs1(vv,u,v);
              q[vv]:=1;
              k1:=dfs1(vv,u,v+p[vv]);
              if k<>-1 then
                begin
                  if k1=-1 then sum:=sum+k
                    else sum:=sum+min(k,k1);
                end
                else
                if k1>-1 then sum:=sum+k1;
              vis[vv]:=false;
            end;
        end
        else
        begin
          q[vv]:=0;
          vis[vv]:=true;
          k:=dfs1(vv,u,v);
          q[vv]:=1;
          k1:=dfs1(vv,u,v+p[vv]);
          if k<>-1 then
                begin
                  if k1=-1 then sum:=sum+k
                    else
                    if k<k1 then sum:=sum+k
                      else sum:=sum+k1;
                end
                else
                if k1>-1 then sum:=sum+k1;
              vis[vv]:=false;
        end;
      i:=next[i];
    end;
  if sum=0 then exit(-1)
    else exit(sum);
end;

begin
assign(input,'defense.in');
  assign(output,'defense.out');
  reset(input);
  rewrite(output);

  read(n,m);readln(ss);
  for i:=1 to n do read(p[i]);
  for i:=1 to n-1 do
    begin
      readln(x,y);
      build(x,y);
      build(y,x);
    end;
  if ss[2]='A' then
    begin
      while m>0 do
        begin
          dec(m);
          fillchar(pp,sizeof(pp),0);
          readln(a,x,b,y);
          if x=1 then pp[a]:=2
            else pp[a]:=1;
          if y=1 then pp[b]:=2
            else pp[b]:=1;
          ans:=maxlongint;
          if pp[1]=0 then
          begin
            q[0]:=0;
            q[1]:=0;
            dfs(1,0);
            fillchar(q,sizeof(q),0);
            q[1]:=1;
            dfs(1,p[1]);
          end
          else
          if pp[1]=1 then
          begin
            q[0]:=0;
            q[1]:=0;
            dfs(1,0);
          end
          else
          begin
            q[0]:=0;q[1]:=1;
            dfs(1,p[1]);
          end;
          if ans=maxlongint then write(-1)
          else write(ans);
        end;
      close(input);
      close(output);
      exit;
    end;
  while m>0 do
    begin
      dec(m);
      fillchar(pp,sizeof(pp),0);
      fillchar(vis,sizeof(vis),0);
      readln(a,x,b,y);
      if x=1 then pp[a]:=2
        else pp[a]:=1;
      if y=1 then pp[b]:=2
        else pp[b]:=1;
      q[0]:=0;
      vis[1]:=true;
      if pp[1]=1 then
        begin
          q[1]:=0;
          writeln(dfs1(1,0,0));
        end
        else
        if pp[1]=2 then
        begin
          q[1]:=1;
          writeln(dfs1(1,0,p[1]));
        end
        else
        begin
         q[1]:=0;
         kk:=dfs1(1,0,0);
         q[1]:=1;
         kkk:=dfs1(1,0,p[1]);
         if kk=-1 then writeln(kkk)
           else
             if kkk=-1 then writeln(kk)
               else writeln(min(kk,kkk));
        end;
    end;

  close(input);
  close(output);
end.
