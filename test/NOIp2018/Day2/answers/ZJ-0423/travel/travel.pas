type
  arr=array[0..5000] of integer;
var
  n,m,i,x,y,top,u,v,tot:longint;
  vis:array[0..100000] of boolean;
  p,head,next,vet:array[0..100000] of longint;

procedure build(x,y:longint);
begin
  inc(tot);
  next[tot]:=head[x];head[x]:=tot;vet[tot]:=y;
end;

procedure qsort(l,r:longint;var a:arr);
var
  i,j,mid,t:longint;
begin
  i:=l;j:=r;mid:=a[(l+r) div 2];
  repeat
    while a[i]<mid do inc(i);
    while a[j]>mid do dec(j);
    if i<=j then
      begin
        t:=a[i];a[i]:=a[j];a[j]:=t;
        inc(i);dec(j);
      end;
  until i>j;
  if i<r then qsort(i,r,a);
  if l<j then qsort(l,j,a);
end;

procedure dfs(u:longint);
var
  a:arr;
  tot,i,v,j,t:longint;
begin
  write(u,' ');
  tot:=0;
  i:=head[u];
  while i<>0 do
    begin
      v:=vet[i];
      if not vis[v] then
        begin
          vis[v]:=true;
          inc(tot);
          a[tot]:=v;
        end;
      i:=next[i];
    end;
  qsort(1,tot,a);
  for i:=1 to tot do dfs(a[i]);
end;

begin
  assign(input,'travel.in');
  assign(output,'travel.out');
  reset(input);
  rewrite(output);

  readln(n,m);
  for i:=1 to m do
    begin
      readln(x,y);
      build(x,y);
      build(y,x);
    end;
  fillchar(vis,sizeof(vis),0);
  vis[1]:=true;
  dfs(1);

  close(input);
  close(output);
end.
