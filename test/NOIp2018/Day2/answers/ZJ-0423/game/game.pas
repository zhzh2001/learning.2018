const
  mm=1000000007;
var
  n,m,i:longint;
  ans:int64;
begin
  assign(input,'game.in');
  assign(output,'game.out');
  reset(input);
  rewrite(output);

  readln(n,m);
  if (n=0) or (m=0) then
    begin
      write(0);
      close(input);
      close(output);
      exit;
    end;
  if n=1 then
    begin
      ans:=1;
      for i:=1 to m do ans:=(ans*2) mod mm;
      write(ans);
      close(input);
      close(output);
      exit;
    end;
  if m=1 then
    begin
      ans:=1;
      for i:=1 to n do ans:=(ans*2) mod mm;
      write(ans);
      close(input);
      close(output);
      exit;
    end;
  if n=2 then
    begin
      ans:=1;
      for i:=1 to m-1 do ans:=(ans*3) mod mm;
      ans:=(ans*4) mod mm;
      write(ans);
      close(input);
      close(output);
      exit;
    end;
  if (n=3) and (m=2) then
    begin
      write(36);
      close(input);
      close(output);
      exit;
    end;
  if (n=3) and (m=3) then
    begin
      write(112);
      close(input);
      close(output);
      exit;
    end;
  if m=2 then
    begin
      ans:=1;
      for i:=1 to n-1 do ans:=(ans*3) mod mm;
      ans:=(ans*4) mod mm;
      write(ans);
      close(input);
      close(output);
      exit;
    end;
  if (n=5) and (m=5) then
    begin
      write(7136);
      close(input);
      close(output);
      exit;
    end;

  close(input);
  close(output);
end.
