#include<bits/stdc++.h>
#define M 5005
using namespace std;
int n,m;
int Deg[M];
vector<int>E[M];
void Add_edge(int u,int v){
	E[u].push_back(v);	
}
struct P60{
	int dfn;
	int Pos[M];
	void Dfs(int x,int f){
		++dfn;Pos[dfn]=x;
		for(int i=0;i<(int)E[x].size();i++){
			int y=E[x][i];
			if(y==f)continue;
			Dfs(y,x);
		}
	}
	void Solve(){
		dfn=0;
		Dfs(1,0);
		for(int i=1;i<n;i++)printf("%d ",Pos[i]);	
		printf("%d\n",Pos[n]);
	}
}p60;
struct P100{
	bool had;
	int cmp1,cmp2;
	int dfn;
	int Pos[M],Fa[M];
	bool Mark[M],Inl[M];
	priority_queue<int>q;
	void Dfst(int x,int f){
		++dfn;Pos[dfn]=x;Mark[x]=1;
		for(int i=0;i<(int)E[x].size();i++){
			int y=E[x][i];
			if(y==f||Inl[y])continue;
			Dfst(y,x);			
		}
	}
	queue<int>Q;
	int ls,lt;
	bool Vis[M];
	void Dfs_l(int x,int f){
		Vis[x]=1;Fa[x]=f;
		for(int i=0;i<(int)E[x].size();i++){
			int y=E[x][i];
			if(y==f)continue;
			if(Vis[y]){
				if(ls==-1)ls=x,lt=y;
				continue;
			}
			Dfs_l(y,x);
		}
	}
	void Getloop(){
		ls=lt=-1;
		Dfs_l(1,0);
		while(ls!=lt){Inl[ls]=1;ls=Fa[ls];}
		Inl[lt]=1;			
	}
	void Dfsl(int x,int f){
		Mark[x]=1;++dfn;Pos[dfn]=x;
		for(int i=0;i<(int)E[x].size();i++){
			int y=E[x][i];
			if(Mark[y])continue;
			if(!Inl[y])Dfst(y,x);
			else {
				if(!had&&cmp2<y){
					had=1;
					return;
				}else Dfsl(y,x);
			}
		}
	}
	void Sol(int st){
		had=0;
		cmp1=-1,cmp2=-1;
		for(int i=0;i<(int)E[st].size();i++){
			int y=E[st][i];
			if(Mark[y])continue;
			if(Inl[y]){
				if(cmp1==-1)cmp1=y;
				else cmp2=y;	
			}
		}
		for(int i=0;i<(int)E[st].size();i++){
			int y=E[st][i];
			if(Mark[y])continue;
			if(!Inl[y])Dfst(y,st);
			else{
				Dfsl(y,st);
			}
		}
//		while(!q.empty()){
//			int x=-q.top();q.pop();
//			if(!Inl[x])Dfst(x,0);	
//			else{
//				++dfn;Pos[dfn]=x;
//				for(int i=0;i<(int)E[x].size();i++){
//					int y=E[x][i];
//					if(Mark[y])continue;
//					Mark[y]=1;
//					q.push(-y);
//				}
//			}
//		}
	}
	void Dfs(int x,int f){
		Mark[x]=1;
		++dfn;Pos[dfn]=x;
		if(Inl[x]){
			Sol(x);
			return;
		}
		for(int i=0;i<(int)E[x].size();i++){
			int y=E[x][i];
			if(y==f)continue;
			Dfs(y,x);	
		}
	}	
	void Solve(){
		dfn=0;
		while(!Q.empty())Q.pop();
		while(!q.empty())q.pop();
		for(int i=1;i<=n;i++){
			Mark[i]=0;
			Inl[i]=0;
			Vis[i]=0;
		}
		Getloop();
//		for(int i=1;i<=n;i++)printf("%d ",Inl[i]);printf("?\n");
		Dfs(1,0);
		for(int i=1;i<n;i++)printf("%d ",Pos[i]);
		printf("%d\n",Pos[n]);	
	}
}p100;
int main(){
//	printf("%.4f\n",1.0*(sizeof())/1024/1024);
	freopen("travel.in","r",stdin);
//	freopen("travel4.in","r",stdin);
	freopen("travel.out","w",stdout);	
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int a,b;
		scanf("%d%d",&a,&b);
		Add_edge(a,b);
		Add_edge(b,a);	
		++Deg[a],++Deg[b];
	}
	for(int i=1;i<=n;i++)sort(E[i].begin(),E[i].end());
//	p60.Solve();
//	p100.Solve();
	if(m==n-1)p60.Solve();
	else p100.Solve();
	return 0;
}
