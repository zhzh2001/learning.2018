#include<bits/stdc++.h>
const int mod=1e9+7;
using namespace std;
int n,m;
struct Pn2{
	void Solve(){
		int mul=1;
		for(int i=1;i<m;i++)mul=1ll*mul*3%mod;
		mul=1ll*mul*4%mod;
		printf("%d\n",mul);	
	}
}pn2;
//struct Pshui{
//	int ans;
//	bool Mark[5][5];
//	bool Check(){
//		for(int i=1;i<n;i++)
//			for(int j=2;j<=m;j++)
//				if(Mark[i+1][j-1]<Mark[i][j])return 0;	
//		return 1;
//	}
//	void Dfs(int x,int y){
//		if(y==m+1){
//			Dfs(x+1,1);
//			return;	
//		}
//		if(x==n+1){
//			ans+=Check();
//			return;	
//		}
//		Mark[x][y]=1;
//		Dfs(x,y+1);
//		Mark[x][y]=0;
//		Dfs(x,y+1);
//	}
//	void Solve(){
//		ans=0;	
//		Dfs(1,1);
//		printf("%d\n",ans);
//	}
//}pshui;
struct P{
	void Solve(){
		int ans=1;
		int x=1,y=1;
		while(x!=n||y!=m){
			ans=1ll*ans*(min(x,m-y+1)+1)%mod;
			if(x!=n)++x;
			else ++y;	
		}
		ans=1ll*ans*2%mod;
		printf("%d\n",ans);
	}
}p;
int main(){
//	printf("%.4f\n",1.0*(sizeof())/1024/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);	
	scanf("%d%d",&n,&m);
	if(n==m){
		if(n==2){
			puts("12");
			return 0;	
		}
		if(n==3){
			puts("112");
			return 0;	
		}
		if(n==5){
			puts("7136");
			return 0;	
		}	
	}
	if(n<=2)pn2.Solve();
	else p.Solve();
//	pshui.Solve();
	return 0;
}
