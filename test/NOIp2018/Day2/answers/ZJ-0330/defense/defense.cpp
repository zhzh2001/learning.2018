#include<bits/stdc++.h>
#define M 100005
#define ll long long
using namespace std;
int n,m,etot;
int Head[M],Val[M];
char Str[10];
struct Edge{
	int to,nxt;
}E[M<<1];
void Add_edge(int u,int v){
	E[++etot]=(Edge){v,Head[u]};
	Head[u]=etot;	
}
struct PA3s{
	ll Dp[M][2],dp[M][2],F[M][2];
	void Solve(){
		for(int i=1;i<=n;i++){
			Dp[i][0]=Dp[i-1][1];
			Dp[i][1]=min(Dp[i-1][0],Dp[i-1][1])+Val[i];	
		}
		for(int i=n;i>=1;i--){
			dp[i][0]=dp[i+1][1];
			dp[i][1]=min(dp[i+1][0],dp[i+1][1])+Val[i];	
		}
		while(m--){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(a>b)swap(a,b),swap(x,y);
			F[a][x^1]=1e18;
			F[a][x]=0;
			for(int i=a+1;i<b;i++){
				F[i][0]=F[i-1][1];
				F[i][1]=min(F[i-1][0],F[i-1][1])+Val[i];
				if(F[i][1]>1e18)F[i][1]=1e18;	
			}
			ll ans=Dp[a][x]+dp[b][y];
			if(a+1==b){
				if(x==0&&y==0)puts("-1");
				else printf("%lld\n",ans);
			}else{
				ll tmp;
				if(y==0)tmp=F[b-1][1];
				else tmp=min(F[b-1][1],F[b-1][0]);
				if(ans+tmp>=1e18)puts("-1");
				else printf("%lld\n",ans+tmp);	
			}
		}
	}
}pA3s;
struct PA2{
	ll Dp[M][2],dp[M][2];
	void Solve(){
		for(int i=1;i<=n;i++){
			Dp[i][0]=Dp[i-1][1];
			Dp[i][1]=min(Dp[i-1][0],Dp[i-1][1])+Val[i];	
		}
		for(int i=n;i>=1;i--){
			dp[i][0]=dp[i+1][1];
			dp[i][1]=min(dp[i+1][0],dp[i+1][1])+Val[i];	
		}
		while(m--){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(a>b)swap(a,b),swap(x,y);
			ll ans=Dp[a][x]+dp[b][y];
			if(x==0&&y==0)puts("-1");
			else printf("%lld\n",ans);
		}
	}
}pA2;
struct PA1{
	ll Dp[M][2],dp[M][2];
	void Solve(){
		Dp[1][1]=Val[1];Dp[1][0]=1e18;
		for(int i=2;i<=n;i++){
			Dp[i][0]=Dp[i-1][1];
			Dp[i][1]=min(Dp[i-1][0],Dp[i-1][1])+Val[i];	
			if(Dp[i][1]>1e18)Dp[i][1]=1e18;
		}
		for(int i=n;i>=1;i--){
			dp[i][0]=dp[i+1][1];
			dp[i][1]=min(dp[i+1][0],dp[i+1][1])+Val[i];	
			if(dp[i][1]>1e18)dp[i][1]=1e18;
		}
		while(m--){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(a>b)swap(a,b),swap(x,y);
			ll ans;
			if(y==0){
				ans=Dp[b-1][1]+dp[b+1][1];	
			}else{
				ans=min(Dp[b-1][1],Dp[b-1][0])+min(dp[b+1][1],dp[b+1][0]);	
			}
			if(ans>=1e18)puts("-1");
			else printf("%lld\n",ans);
		}
	}
}pA1;
struct PC3s{
	int a,xs,b,ys;
	ll Dp[M][2];
	void Dfs(int x,int f){
		Dp[x][0]=Dp[x][1]=0;
		if(x==a)Dp[x][xs^1]=1e18;
		else if(x==b)Dp[x][ys^1]=1e18;
		for(int i=Head[x];i;i=E[i].nxt){
			int y=E[i].to;
			if(y==f)continue;
			Dfs(y,x);
			if(Dp[x][0]!=1e18)Dp[x][0]+=Dp[y][1];
			if(Dp[x][1]!=1e18)Dp[x][1]+=min(Dp[y][0],Dp[y][1]);
		}
		Dp[x][1]+=Val[x];
		if(Dp[x][0]>1e18)Dp[x][0]=1e18;
		if(Dp[x][1]>1e18)Dp[x][1]=1e18;
	}
	void Solve(){
		while(m--){
			scanf("%d%d%d%d",&a,&xs,&b,&ys);
			Dfs(1,0);
			ll ans=min(Dp[1][0],Dp[1][1]);
			if(ans>=1e18)puts("-1");
			else printf("%lld\n",ans);
		}
	}
}pC3s;
//struct PB{
//	int Fa[M];
//	ll Dp[M][2],F[M][2];
//	void Dfs(int x,int f){
//		Fa[x]=f;
//		Dp[x][0]=Dp[x][1]=0;
//		if(x==1)Dp[x][0]=1e18;
//		for(int i=Head[x];i;i=E[i].nxt){
//			int y=E[i].to;
//			if(y==f)continue;
//			Dfs(y,x);
//			if(x!=1)Dp[x][0]+=Dp[y][1];
//			Dp[x][1]+=min(Dp[y][0],Dp[y][1]);
//		}
//		Dp[x][1]+=Val[x];
//	}
//	void Solve(){
//		while(m--){
//			int a,x,b,y;
//			scanf("%d%d%d%d",&a,&x,&b,&y);
//			int t=b;
//			while(
//		}
//	}
//}pb;
int main(){
//	printf("%.4f\n",1.0*(sizeof())/1024/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);	
	scanf("%d%d%s",&n,&m,Str+1);
	for(int i=1;i<=n;i++)scanf("%d",&Val[i]);
	for(int i=1;i<n;i++){
		int a,b;
		scanf("%d%d",&a,&b);
		Add_edge(a,b);
		Add_edge(b,a);
	}
	if(Str[1]=='A'){
		if(Str[2]=='3')pA3s.Solve();
		else if(Str[2]=='1')pA1.Solve();
		else if(Str[2]=='2')pA2.Solve();
	}else pC3s.Solve();			
	return 0;
}
