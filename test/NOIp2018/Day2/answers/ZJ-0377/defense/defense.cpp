#include<bits/stdc++.h>
#define Fr(i,a,b) for(int i=(a);i<=(int)(b);++i)
#define Dr(i,a,b) for(int i=(a);i>=(int)(b);--i)
const int inf=0x3f3f3f3f;
typedef long long ll;
using namespace std;
#define debug(a,b) cerr<<#a<<"="<<a<<" "<<#b<<"="<<b<<endl
const int M=1e5+5;
struct Graph{
	struct node{int fr,to;}e[M<<1];
	int head[M],tot;
	void clear(){memset(head,-1,sizeof(head)),tot=0;}
	void add(int a,int b){e[++tot].to=b;e[tot].fr=head[a];head[a]=tot;}
	#define Ed(i,u) for(int i=G.head[u];~i;i=G.e[i].fr)
}G;
int n,m,u,v;
char op[5];
int A[M];
struct P10{
	bool vis[M];
	bool dfs(int u,int fa,bool f){
		bool s=f|vis[u];
		Ed(i,u){
			int v=G.e[i].to;
			if(v==fa)continue;
			if(!dfs(v,u,vis[u]))return false;
			s|=vis[v];
		}
		if(!s)return false;
		else return true;
	}
	void sol(){
		while(m--){
			int a,b,x,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			int p=(1<<n)-1,ans=inf;
			Fr(i,0,p){
				int res=0;
				memset(vis,0,sizeof(vis));
				Fr(j,0,n-1){
					if(!(i&(1<<j)))continue;
					vis[j+1]=1,res+=A[j+1];
				}
				if(!dfs(1,0,vis[1]))continue;
				bool fl=0;
				Fr(j,1,n)if((j==a&&vis[j]!=x)||(j==b&&vis[j]!=y))fl=1;
				if(fl)continue;
				ans=min(ans,res);
			}
			if(ans==inf)ans=-1;
			printf("%d\n",ans);
		}
	}
}p_10;
struct P100{
	bool vis[M];
	bool dfs(int u,int fa,bool f){
		bool s=f|vis[u];
		Ed(i,u){
			int v=G.e[i].to;
			if(v==fa)continue;
			if(!dfs(v,u,vis[u]))return false;
			s|=vis[v];
		}
		if(!s)return false;
		else return true;
	}
	void sol(){
		while(m--){
			int a,b,x,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			int ans=inf;
			srand(time(NULL));
			Fr(cas,1,n){
				int res=0;
				Fr(i,1,n)vis[i]=rand()&1;
				Fr(i,1,n)if(vis[i])res+=A[i];
				if(!dfs(1,0,vis[1]))continue;
				bool fl=0;
				Fr(j,1,n)if((j==a&&vis[j]!=x)||(j==b&&vis[j]!=y))fl=1;
				if(fl)continue;
				ans=min(ans,res);
			}
			if(ans==inf)ans=-1;
			printf("%d\n",ans);
		}
	}
}p_100;
int main(){//defense memory mod long long *
//	printf("%.2lf\n",(&mmm2-&mmm1)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m>>op,G.clear();
	Fr(i,1,n)scanf("%d",&A[i]);
	Fr(i,1,n-1){
		scanf("%d%d",&u,&v);
		G.add(u,v),G.add(v,u);
	}
	if(n<=20&&m<=20)p_10.sol();
	else p_100.sol();
	return 0;
}
