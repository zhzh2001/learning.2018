#include<bits/stdc++.h>
#define Fr(i,a,b) for(int i=(a);i<=(int)(b);++i)
#define Dr(i,a,b) for(int i=(a);i>=(int)(b);--i)
const int inf=0x3f3f3f3f;
typedef long long ll;
using namespace std;
#define debug(a,b) cerr<<#a<<"="<<a<<" "<<#b<<"="<<b<<endl
const int M=5005;
vector<int>G[M];
int n,m,u,v;
struct P60{
	int a[M],Index;
	void dfs(int u,int fa){
		a[++Index]=u;
		Fr(i,0,G[u].size()-1){
			int v=G[u][i];
			if(v==fa)continue;
			dfs(v,u);
		}
	}
	void sol(){
		Index=0;
		dfs(1,0);
		Fr(i,1,Index-1)printf("%d ",a[i]);
		printf("%d\n",a[Index]);
	}
}p_60;
int deg[M];
struct P100{
	int vis[M],a[M],Index;
	bool cir[M];
	void topo_sort(){
		queue<int>q;
		while(!q.empty())q.pop();
		Fr(i,1,n)if(deg[i]==1)q.push(i),cir[i]=1;
		while(!q.empty()){
			int u=q.front();q.pop();
			Fr(i,0,G[u].size()-1){
				int v=G[u][i];
				deg[v]--;
				if(deg[v]==1)q.push(v),cir[v]=1;
			}
		}
	}
	int id,fl;
	void dfs(int u,int fa){
		if(vis[u]==2)return ;
		vis[u]++;
		if(vis[u]==1)a[++Index]=u;
		if(!cir[u]&&id==-1){
			Fr(i,0,G[u].size()-1){
				int v=G[u][i];
				if(v==fa)continue;
				if(cir[v])continue;
				id=v;
			}
		}
		if(~id&&!fl&&!cir[u]){
			Fr(i,0,G[u].size()-1){
				int v=G[u][i];
				if(v==fa)continue;
				if(cir[v])continue;
				if(v>id){
					fl=1;
					dfs(fa,u);
					return;	
				}
			}
		}
		Fr(i,0,G[u].size()-1){
			int v=G[u][i];
			if(v==fa)continue;
			dfs(v,u);
		}
	}
	void sol(){
		fl=0,id=-1,memset(cir,0,sizeof(cir));
		Index=0,memset(vis,0,sizeof(vis));
		topo_sort();
		dfs(1,0);
		Fr(i,1,n-1)printf("%d ",a[i]);
		printf("%d\n",a[n]);
	}
}p_100;
int main(){//travel memory mod long long *
//	printf("%.2lf\n",(&mmm2-&mmm1)/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	Fr(i,1,m){
		scanf("%d%d",&u,&v);
		G[u].push_back(v),G[v].push_back(u);
		deg[u]++,deg[v]++;
	}
	Fr(i,1,n)sort(G[i].begin(),G[i].end());
	if(m==n-1)p_60.sol();
	else p_100.sol();
	return 0;
}
