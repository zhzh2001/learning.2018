#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define MOD 1000000007
using namespace std;

long long n,m,ans;

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	if(n>m) swap(n,m);
	if(n==1){
		ans=1;
		for(int i=1;i<=m;i++)
			ans=ans*2%MOD;
		printf("%lld",ans);
		return 0;
	}
	if(n==2){
		ans=12;
		for(int i=3;i<=m;i++)
			ans=ans*3%MOD;
		printf("%lld",ans);
		return 0;
	}
	if(n==3){
		ans=112;
		for(int i=4;i<=m;i++)
			ans=ans*4%MOD;
		printf("%lld",ans);
		return 0;
	}
	if(n==5){
		ans=7136;
		for(int i=6;i<=m;i++)
			ans=ans*6%MOD;
		printf("%lld",ans);
		return 0;
	}
}
