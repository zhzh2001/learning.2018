#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

int tot,ans[5008],res[5008],n,m,x[5008],y[5008],num;
int fa[5008],f[5002][5002],bo[5008],du[5008],flag;

void dfs(int u,int last){
	if(tot==n){
		if(num==n)
		for(int i=1;i<=n;i++)
			res[i]=ans[i];
		return;
	}
	for(int i=1;i<=n;i++)
		if(f[u][i]&&(flag||(i<=res[tot+1]))&&i!=last){
			if(i<res[tot+1]) flag=1;
			tot++;
			ans[tot]=i;
			if(!bo[i]) num++;
			bo[i]=1;
			
			dfs(i,u);
		}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	memset(du,0,sizeof(du));
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&x[i],&y[i]);
		f[x[i]][y[i]]=1;
		f[y[i]][x[i]]=1;
		du[x[i]]++,du[y[i]]++;
	}
	for(int i=1;i<=n;i++) res[i]=100000008;
	res[1]=1;
	if(m==n-1){
		tot=1;
		ans[1]=1;
		flag=0;
		dfs(1,0);
		for(int i=1;i<=n;i++)
			printf("%d ",res[i]);
	}
	if(m==n){
		for(int i=1;i<=n;i++){
			memset(ans,0,sizeof(ans));
			f[x[i]][y[i]]=0;
			f[y[i]][x[i]]=0;
			du[x[i]]--; du[y[i]]--;
			if(du[x[i]]<=0){
				f[x[i]][y[i]]=1;
				f[y[i]][x[i]]=1;
				du[x[i]]++; du[y[i]]++;
				continue;
			}
			if(du[y[i]]<=0){
				f[x[i]][y[i]]=1;
				f[y[i]][x[i]]=1;
				du[x[i]]++; du[y[i]]++;
				continue;
			}
			tot=1;
			ans[1]=1;
			flag=0;
			bo[1]=1; num=1;
			memset(bo,0,sizeof(bo));
			dfs(1,0);
			f[x[i]][y[i]]=1;
			f[y[i]][x[i]]=1;
			du[x[i]]++;du[y[i]]++;
		}
		for(int i=1;i<=n;i++)
			printf("%d ",res[i]);
	}
}
