#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<set>
#include<cmath>
#include<map>
#include<vector>
#include<cstdlib>
#include<string>
#define ll long long
#define pb push_back
#define mp make_pair
using namespace std;
typedef pair<int,int> pii; 
const int maxn=300000,mod=1e9+7;

string s;
int n,m,f[maxn][2],p[maxn],v[maxn],a,x,b,y;

vector<int> g[maxn];

void add(int u,int v){
	g[u].pb(v);
	g[v].pb(u);
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);cin>>s;
		
		for(int i=1;i<n;i++)scanf("%d",&p[i]);
		while(m--){
			memset(f,0x3f3f,sizeof(f));
			f[0][0]=f[0][1]=0;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(abs(a-b)==1)cout<<-1<<endl;
			else {
				for(int i=1;i<=n;i++){
					f[i][0]=f[i-1][1]+p[i];
					f[i][1]=min(f[i-1][1],f[i-1][0])+p[i];
					f[a][x]=f[b][y]=0x3f3f;
				}
				cout<<min(f[n][0],f[n][1])<<endl;
			}	
		}
	return 0;
	
}
