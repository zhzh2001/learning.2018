#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<set>
#include<cmath>
#include<map>
#include<vector>
#include<cstdlib>
#define ll long long
#define pb push_back
#define mp make_pair
using namespace std;
typedef pair<int,int> pii; 
const int maxn=18,mod=1e9+7;

int f[1<<maxn],n,m,sum[1<<maxn],T[maxn<<3],s[maxn],t=0,ans=0,po[9000000];

void rebuild(){
	T[0]=0;
	for(int i=1;i<(maxn<<3);i++)T[i]=T[i-1]+i;
	for(int i=0;i<(1<<maxn);i++){
		int x=i;
		while(x>0){
			if(x&1)sum[i]++;
			x/=2;
		}
	}
	po[0]=1;
	for(int i=1;i<m*n+100;i++)po[i]=(po[i-1]*2)%mod;
}

void subset(int cur){
	for(int i=1;i<( 1<< (n-1)*2 );i++){
		if(sum[i]==n-1)s[t++]=i;
	}
}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	rebuild();
	subset(0);
	memset(f,0,sizeof(f));
	if(n<=3&&m<=3){
		if(n==1&&m==1)ans=1;
		if(n==2&&m==1||m==2&&n==1)ans=4;
		if(n==2&&m==2)ans=12;
		if(m+n==5)ans=48;
		if(m+n==6)ans=112;
		cout<<ans<<endl;
		return 0;
	}
	for(int i=0;i<t;i++)
		for(int j=i+1;j<t;j++){
			int a=s[i],b=s[j];
			int x=a^b;
			ans=(ans+T[sum[a&x]]+po[m+n-2-a&x])%mod;
		}
	cout<<ans*4<<endl;
	return 0;
}
