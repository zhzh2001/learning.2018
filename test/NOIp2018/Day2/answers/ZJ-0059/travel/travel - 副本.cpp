#include<bits/stdc++.h>
using namespace std;
inline int read()
{
	char c=getchar(); int ret=0;
	while(c<48||c>57)c=getchar();
	while(c>=48&&c<=57)ret=ret*10+c-48,c=getchar();
	return ret;
}
#define N 100005
int h,f[N],u,v,n,m,c[N][2],tot,a[N],s[N];
set<int>S;

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=m;++i){
		u=read(); v=read();
		c[++tot][0]=v; c[tot][1]=a[u]; a[u]=tot;
		c[++tot][0]=u; c[tot][1]=a[v]; a[v]=tot;
	}
	S.insert(1);
	while(!S.empty()){
		u=*S.begin();
		S.erase(u);
		f[u]=1; s[++h]=u;
		for(int o=a[u]; o; o=c[o][1]){
			v=c[o][0];
			if(f[v])continue;
			S.insert(v);
		}
	}
	for(int i=1;i<=h;++i)printf("%d ",s[i]);
}
