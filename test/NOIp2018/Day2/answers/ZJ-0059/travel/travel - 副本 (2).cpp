#include<bits/stdc++.h>
using namespace std;
inline int read()
{
	char c=getchar(); int ret=0;
	while(c<48||c>57)c=getchar();
	while(c>=48&&c<=57)ret=ret*10+c-48,c=getchar();
	return ret;
}
#define N 100005
int fl,dfn[N],lo[N],ti,h,f[N],u,v,n,m,c[N][2],tot,a[N],s[N];
set<int>S;

void tarjan(int p, int pr)
{
	dfn[p]=lo[p]=++ti;
	for(int o=a[p]; o; o=c[o][1]){
		int v=c[o][0];
		if(v==pr)continue;
		if(dfn[v])lo[p]=min(lo[p],dfn[v]);
		else {
			tarjan(v,p);
			lo[p]=min(lo[p],lo[v]);
		}
	}
}

void dfs(int p)
{
	set<int>G;
	for(int o=a[p]; o; o=c[o][1]){
		int v=c[o][0];
		if(f[v])continue;
		G.insert(v);
		S.insert(v);
	}
	
	int g,k;
	for(set<int>::iterator it=G.begin();it!=G.end(); ++it){
		g=*it;
		if(f[g])continue;
		k=*S.begin();
		if(g==k){
			s[++h]=k;
			S.erase(k);
			f[k]=1;
			dfs(k);
		}else if(!fl||lo[g]>dfn[p]) {
			s[++h]=g;
			S.erase(g);
			f[g]=1;
			dfs(g);
		}else {
			fl=0;
			s[++h]=k;
			S.erase(k);
			f[k]=1;
			dfs(k);
		}
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=m;++i){
		u=read(); v=read();
		c[++tot][0]=v; c[tot][1]=a[u]; a[u]=tot;
		c[++tot][0]=u; c[tot][1]=a[v]; a[v]=tot;
	}
	
	tarjan(1,0);
	
	for(int i=1;i<=100;++i)if(dfn[i]>lo[i])cerr<<i<<endl;
	
	fl=1; f[1]=1; s[++h]=1;
	dfs(1);
	for(int i=1;i<=h;++i)printf("%d ",s[i]);
}
