#include<bits/stdc++.h>
using namespace std;
inline int read()
{
	char c=getchar(); int ret=0;
	while(c<48||c>57)c=getchar();
	while(c>=48&&c<=57)ret=ret*10+c-48,c=getchar();
	return ret;
}
#define N 10005
int an[N],I,d,uu,fl,dfn[N],lo[N],ti,h,f[N],u,v,n,m,c[N][2],tot,a[N],s[N];

void dfs(int p)
{
	s[++d]=p;
	set<int>G; 
	for(int o=a[p]; o; o=c[o][1]){
		if(o>>1 == I)continue;
		int v=c[o][0];
		if(f[v])continue;
		G.insert(v);
	}
	for(set<int>::iterator it=G.begin();it!=G.end(); ++it){
		int v=*it;
		if(f[v])continue;
		f[v]=1;
		dfs(v);
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(); m=read(); tot=1;
	for(int i=1;i<=m;++i){
		u=read(); v=read();
		c[++tot][0]=v; c[tot][1]=a[u]; a[u]=tot;
		c[++tot][0]=u; c[tot][1]=a[v]; a[v]=tot;
	}
	if(m==n-1){
		f[1]=1;
		dfs(1);
		for(int i=1;i<=d;++i)printf("%d ",s[i]);
		return 0;
	}
	for(I=1;I<=m;++I){
		memset(f,0,sizeof(f)); d=0; f[1]=1;
		dfs(1); if(d!=n)continue;
		if(!an[1])for(int i=1;i<=n;++i)an[i]=s[i];
		else {
			fl=1;
			for(int i=1;i<=n;++i)if(an[i]>s[i]){
				break;
			}else if(an[i]<s[i]){
				fl=0;
				break;
			}
			if(fl)for(int i=1;i<=n;++i)an[i]=s[i];
		}
	}
	for(int i=1;i<=n;++i)printf("%d ",an[i]);
}
