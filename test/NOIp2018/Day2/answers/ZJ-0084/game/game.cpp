#include<cstdio>

using namespace std;

#define rep(i,a,b) for (int i=(a);i<=(b);i++)
#define LL long long

const int tt=1000000007;

int n,m;

int qsm(int x,int y) {int w=x,s=1;while (y) {if (y&1) s=(LL)s*w%tt;w=(LL)w*w%tt;y>>=1;} return s;}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1||m==1) printf("%d\n",qsm(2,n+m-1));
	else if (n==2||m==2) printf("%lld\n",4ll*qsm(3,n+m-3)%tt);
	else puts("112");
	return 0;
}
