#include<cstdio>

using namespace std;

#define rep(i,a,b) for (int i=(a);i<=(b);i++)

const int N=5005;

int n,m,ct,fa[N],ans[N],nxt[N],len[N],son[N][N];
bool cc[N],ls[N],vis[N],bo[N][N];

void GET(int x,int y)
{
	if (cc[x]||cc[y]) return;
	for (;x;x=fa[x]) cc[x]=1;
	for (;y;y=fa[y]) if (cc[y]) break;else cc[y]=1;
	for (x=y,y=fa[y];y;y=fa[y]) cc[y]=0;
	for (int i=x,fl=1,lst=fa[x];i!=x||fl;lst=i,i=nxt[i],fl=0)
	  rep(j,1,len[i]) if (son[i][j]!=lst&&cc[son[i][j]])
		{nxt[i]=son[i][j];break;}
	int _=0;rep(i,1,len[x])
	  if (son[x][i]!=fa[x]&&son[x][i]>nxt[x])
	    {_=son[x][i];break;}
	for (int i=x,fl=1,lst=fa[x];i!=x||fl;lst=i,i=nxt[i],fl=0)
	{
	  if (nxt[i]==son[i][len[i]]||(lst==son[i][len[i]]&&nxt[i]==son[i][len[i]-1]))
		if (nxt[i]>_) {ct=i;break;}
	  rep(j,1,len[i]) if (son[i][j]!=lst&&son[i][j]>nxt[i]) {_=son[i][j];break;}
	}
}
void DFS(int x,int y)
{
	fa[x]=y;ls[x]=1;
	rep(i,1,len[x]) if (son[x][i]!=y)
	{
		if (!ls[son[x][i]]) DFS(son[x][i],x);
		else GET(son[x][i],x);
	}
}
void dfs(int x)
{
	if (vis[x]) return;vis[ans[++ans[0]]=x]=1;
	rep(i,1,len[x]) if (x!=ct||son[x][i]!=nxt[x]) dfs(son[x][i]);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	rep(i,1,m) {int x,y;scanf("%d%d",&x,&y);bo[x][y]=bo[y][x]=1;}
	rep(i,1,n) rep(j,1,n) if (bo[i][j]) son[i][++len[i]]=j;
	DFS(1,0);dfs(1);
	rep(i,1,n-1) printf("%d ",ans[i]);printf("%d\n",ans[n]);
	return 0;
}
