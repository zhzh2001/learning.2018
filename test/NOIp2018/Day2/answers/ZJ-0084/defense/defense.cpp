#include<algorithm>
#include<cstring>
#include<cstdio>

using namespace std;

#define rep(i,a,b) for (int i=(a);i<=(b);i++)
#define LL long long
#define rs (id<<1|1)
#define ls (id<<1)

const LL inf=1000000000000ll;
const int N=100005,M=200005;

struct node{
	LL dp[2][2];
	node(LL A=inf,LL B=inf) {dp[0][0]=A,dp[1][1]=B;dp[0][1]=dp[1][0]=inf;}
	LL MIN() {return min(min(dp[0][0],dp[0][1]),min(dp[1][0],dp[1][1]));}
	friend node operator +(const node &A,const node &B)
	{
		node C;
		C.dp[0][0]=min(A.dp[0][1]+B.dp[1][0],min(A.dp[0][1]+B.dp[0][0],A.dp[0][0]+B.dp[1][0]));
		C.dp[0][1]=min(A.dp[0][1]+B.dp[1][1],min(A.dp[0][1]+B.dp[0][1],A.dp[0][0]+B.dp[1][1]));
		C.dp[1][0]=min(A.dp[1][1]+B.dp[1][0],min(A.dp[1][1]+B.dp[0][0],A.dp[1][0]+B.dp[1][0]));
		C.dp[1][1]=min(A.dp[1][1]+B.dp[1][1],min(A.dp[1][1]+B.dp[0][1],A.dp[1][0]+B.dp[1][1]));
		return C;
	}
};

int tot,lnk[N],son[M],nxt[M];
LL ans,f[N][2],g[N][2];
int n,m,ty,a[N],fa[N];
node t[N<<2];

int rd()
{
	char ch=getchar();
	while (ch<'A'||ch>'C') ch=getchar();
	return ch=='A';
}
int read()
{
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
void add(int x,int y)
{
	tot++;son[tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;
	tot++;son[tot]=x;nxt[tot]=lnk[y];lnk[y]=tot;
}
void build(int id,int L,int R)
{
	if (L==R) {t[id]=node(0,a[L]);return;}
	int mid=(L+R)>>1;build(ls,L,mid);build(rs,mid+1,R);
	t[id]=t[ls]+t[rs];
}
node get(int id,int L,int R,int l,int r)
{
	if (l>r) return node(0,0);
	if (L==l&&R==r) return t[id];int mid=(L+R)>>1;
	if (r<=mid) return get(ls,L,mid,l,r);
	else if (l>mid) return get(rs,mid+1,R,l,r);
	else return get(ls,L,mid,l,mid)+get(rs,mid+1,R,mid+1,r);
}
void work1()
{
	build(1,1,n);while (m--)
	{
		int A=read(),X=read(),B=read(),Y=read();
		if (A>B) swap(A,B),swap(X,Y);
		if (A+1==B)
		{
			if (X==0&&Y==0) {puts("-1");continue;}
			node le=get(1,1,n,1,A-1),ri=get(1,1,n,B+1,n);
			if (X==0) ans+=min(le.dp[0][1],le.dp[1][1]);else ans+=le.MIN()+a[A];
			if (Y==0) ans+=min(ri.dp[1][0],ri.dp[1][1]);else ans+=ri.MIN()+a[B];
		}
		else
		{
			node le=get(1,1,n,1,A-1),mid=get(1,1,n,A+1,B-1),ri=get(1,1,n,B+1,n);
			if (X==0) ans+=min(le.dp[0][1],le.dp[1][1]);else ans+=le.MIN()+a[A];
			if (Y==0) ans+=min(ri.dp[1][0],ri.dp[1][1]);else ans+=ri.MIN()+a[B];
			if (X==0&&Y==0) ans+=mid.dp[1][1];
			else if (X==0) ans+=min(mid.dp[1][0],mid.dp[1][1]);
			else if (Y==0) ans+=min(mid.dp[0][1],mid.dp[1][1]);
			else ans+=mid.MIN();
		}
		printf("%lld\n",ans);ans=0;
	}
}
void dfs(int x,int dad)
{
	fa[x]=dad;for (int j=lnk[x];j;j=nxt[j]) if (son[j]!=dad) dfs(son[j],x);
	f[x][0]=0;f[x][1]=a[x];for (int j=lnk[x];j;j=nxt[j]) if (son[j]!=dad)
	  f[x][0]+=f[son[j]][1],f[x][1]+=min(f[son[j]][0],f[son[j]][1]);
}
void UP(int x)
{
	if (x==1) return;
	f[fa[x]][0]-=g[x][1];f[fa[x]][0]+=f[x][1];
	f[fa[x]][1]-=min(g[x][0],g[x][1]),f[fa[x]][1]+=min(f[x][0],f[x][1]);
	UP(fa[x]);
}
void work2()
{
	dfs(1,0);memcpy(g,f,sizeof(f));
	while (m--)
	{
		int A=read(),X=read(),B=read(),Y=read();
		f[A][X^1]=inf;UP(A);f[B][Y^1]=inf;UP(B);ans=min(f[1][0],f[1][1]);
		if (ans<inf) printf("%lld\n",ans);else puts("-1");
		for (int i=A;i;i=fa[i]) f[i][0]=g[i][0],f[i][1]=g[i][1];
		for (int i=B;i;i=fa[i]) f[i][0]=g[i][0],f[i][1]=g[i][1];
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();ty=rd();read();
	rep(i,1,n) a[i]=read();
	rep(i,2,n) add(read(),read());
	if ((n<=2000&&m<=2000)||!ty) work2();else work1();
	return 0;
}
