#include<bits/stdc++.h>
#define N 5010
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){ if(ch=='-') f=-1; ch=getchar(); }
	while(ch>='0'&&ch<='9'){ x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
int n,m;
int head[N],nex[N<<1],ver[N<<1],tot;
inline void add(int x,int y){
	ver[++tot]=y;nex[tot]=head[x];head[x]=tot;
}
set<int> s[N],st;
set<int>::iterator it;
bool v[N],ic[N];
int f[N];
inline void fc(int x,int y)
{
	int now=x;
	while(now!=y){
		ic[now]=1;now=f[now];
	}
	ic[y]=1;
}
inline void dfs(int x,int fa){
//	cout<<x<<" ";
	for(int i=head[x];i;i=nex[i]){
		int y=ver[i];
//		cout<<x<<" "<<y<<endl;
		s[x].insert(y);
		if(y==fa) continue;
		if(v[y]&&!ic[x]) {	fc(x,y);continue;	}
		if(v[y]) continue;
		v[y]=1;f[y]=x;dfs(y,x);
	} 
}
bool use[N];
inline void dfs1(int x){
//	if(use[x]) return;
	use[x]=1;
	printf("%d ",x);
	while(!s[x].empty()){
		if(ic[x]){
			while(!st.empty()&&use[*st.begin()]) st.erase(*st.begin());
			for(it=s[x].begin();it!=s[x].end();++it) 
			if(!use[*it])st.insert(*it);
			if(!st.empty()&&*s[x].begin()>*st.begin()){
			int xx=*st.begin();st.erase(st.begin());dfs1(xx);
		}
		int xx=*s[x].begin();
		s[x].erase(xx);
		if(use[xx]) continue;
		dfs1(xx);
		}
		else{
		int xx=*s[x].begin();
		s[x].erase(xx);
		if(use[xx]) continue;
		dfs1(xx);
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1,x,y,z;i<=m;++i){
		x=read();y=read(); add(x,y);add(y,x);
	}
	v[1]=1;
	dfs(1,0);
	memset(v,0,sizeof(v));
	v[1]=1;
	dfs1(1);
	return 0;
}
