#include<bits/stdc++.h>
#define N 100010
using namespace std;
typedef long long ll;
const int inf=1e9+7;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){ if(ch=='-') f=-1; ch=getchar(); }
	while(ch>='0'&&ch<='9'){ x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
int n,m;
char ch[3];
int head[N],nex[N<<1],ver[N<<1],tot;
inline void add(int x,int y){
	ver[++tot]=y;nex[tot]=head[x];head[x]=tot;
}
int p[N];
ll f1[N][2],f[N][2],op[N];
inline void dp(int x,int fa){
	f[x][1]=p[x];
	f[x][0]=0;
	if(op[x]==1) f[x][0]=inf;
	if(op[x]==-1) f[x][1]=inf;
	for(int i=head[x];i;i=nex[i]){
		int y=ver[i];
		if(y==fa) continue;
		dp(y,x);
		f[x][1]+=min(f[y][1],f[y][0]);
		f[x][0]+=f[y][1];
	}
}
inline void solve1(int a,int x,int b,int y){
	if(x==0&&y==0)
	for(int i=head[a];i;i=nex[i]){
		if(ver[i]==b){
			printf("-1\n");
			return;
		}
	}
	if(x==0) op[a]=-1;
	else op[a]=1;
	if(y==0) op[b]=-1;
	else op[b]=1;
	dp(1,0);
	if(op[1]==0) printf("%d\n",min(f[1][1],f[1][0]));
	else  printf("%d\n",(op[1]==1)?f[1][1]:f[1][0]);
	op[a]=0;op[b]=0;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",ch);
	for(int i=1;i<=n;++i) p[i]=read();
	for(int i=1,x,y;i<n;++i){
		x=read();y=read();add(x,y);add(y,x);
	}
	for(int i=1;i<=m;++i){
		int a=read(),x=read(),b=read(),y=read();
//		if(ch[0]=='A'){
//			solve1(a,x,b,y);
//		}
		if(n<=10000&&m<=10000||ch[0]=='B')solve1(a,x,b,y);
	}
	return 0;
}
