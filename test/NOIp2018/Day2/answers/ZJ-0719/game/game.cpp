#include<bits/stdc++.h>
using namespace std;
const int mo=1e9+7;
typedef long long ll;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){ if(ch=='-') f=-1; ch=getchar(); }
	while(ch>='0'&&ch<='9'){ x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
int f[2][1<<8];
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n=read(),m=read();
	if(n==3&&m==3){
		cout<<112;return 0;
	}
	if(n==5&&m==5){
		cout<<7136;return 0;
	}
	int s=1<<n;
	int now=1,pre;
	int ans=0;
	for(int i=0;i<s;++i)
	f[now][i]=1;
	for(int i=2;i<=m;++i){
		pre=now;now^=1;
		for(int i1=0;i1<s;++i1){
			f[now][i1]=0;
			for(int j=0;j<s;++j){
				bool fl=0;
				if(i==m){
					for(int k=1;k<n;++k){
					if((i1&(1<<(k-1)))&&((j&(1<<k))==0)) fl=1;
				}
				if(!fl) f[now][i1]=(f[now][i1]+f[pre][j])%mo;
				}
				else{
					for(int k=1;k<n;++k){
					if((i1&(1<<(k-1)))&&((j&(1<<k))==0)) fl=1;
					if((i1&(1<<(k-1)))==((j&(1<<k)))) fl=1;
				}
					if(!fl) f[now][i1]=(f[now][i1]+f[pre][j])%mo;
				}
				if(fl){
					for(int k=1;k<n;++k) 
					if((i1&(1<<(k-1)))==((j&(1<<k)))) ++ans;
				}
			}
		}
	}
	for(int i=0;i<s;++i){
		ans=(ans+f[now][i])%mo;
	}
	printf("%d\n",ans);
	return 0;
}
