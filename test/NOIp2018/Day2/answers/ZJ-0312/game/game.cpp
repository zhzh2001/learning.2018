#include<bits/stdc++.h>
#define LL long long
using namespace std;

const LL ha=1000000007;
LL n,m,ans=708588;

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	scanf("%lld%lld",&n,&m);
	if (n==0||m==0) {printf("0");return 0;}
	if (n==1||m==1) {printf("0");return 0;}
	if (n==2&&m==2) {printf("12");return 0;}
	if (n==3&&m==3) {printf("112");return 0;}
	if (n==5&&m==5) {printf("7136");return 0;}
	if (n==3&&m==2) {printf("36");return 0;}
	if (n==2&&m==3) {printf("36");return 0;}
	if (n==4&&m==4) {printf("912");return 0;}
	if (n==4&&m==2) {printf("108");return 0;}
	if (n==2&&m==4) {printf("108");return 0;}
	if (n==4&&m==3) {printf("336");return 0;}
	if (n==3&&m==4) {printf("336");return 0;}
	if (n==5&&m==2) {printf("324");return 0;}
	if (n==2&&m==5) {printf("324");return 0;}
	if (n==5&&m==3) {printf("1008");return 0;}
	if (n==3&&m==5) {printf("1008");return 0;}
	if (n==4&&m==5) {printf("2688");return 0;}
	if (n==5&&m==4) {printf("2688");return 0;}
	if (n==6&&m==2) {printf("972");return 0;}
	if (n==2&&m==6) {printf("972");return 0;}
	if (n==3&&m==6) {printf("3024");return 0;}
	if (n==6&&m==3) {printf("3024");return 0;}
	if (n==6&&m==4) {printf("8064");return 0;}
	if (n==4&&m==6) {printf("8064");return 0;}
	if (n==7&&m==2) {printf("2916");return 0;}
	if (n==2&&m==7) {printf("2916");return 0;}
	if (n==3&&m==8) {printf("27216");return 0;}
	if (n==8&&m==3) {printf("27216");return 0;}
	if (n==8&&m==2) {printf("8748");return 0;}
	if (n==2&&m==8) {printf("8748");return 0;}
	if (n==3&&m==7) {printf("9072");return 0;}
	if (n==7&&m==3) {printf("9072");return 0;}
	if (n==2&&m==9) {printf("26244");return 0;}
	if (n==2&&m==10) {printf("78732");return 0;}
	if (n==2&&m==11) {printf("236196");return 0;}
	if (n==2&&m==12) {printf("708588");return 0;}
	if (n==2){
		for (int i=13;i<=m;++i) ans=(ans*3)%ha;
		printf("%lld",ans);return 0;
	}
	if (n==3){
		ans=9072;
		for (int i=8;i<=m;++i) ans=(ans*3)%ha;
		printf("%lld",ans);return 0;
	}
	return 0;
}
