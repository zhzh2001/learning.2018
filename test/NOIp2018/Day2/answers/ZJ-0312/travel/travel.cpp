#include<bits/stdc++.h>
using namespace std;

const int N=5005;
int n,m;
int q[N];
int son[N][N];

int read(){
	int s=0,w=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') w=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') s=(s<<3)+(s<<1)+ch-'0',ch=getchar();
	return s*w;
}

void dfs2(int s,int fa){
	q[++q[0]]=s;
	for (int i=1;i<=son[s][0];++i){
		if (son[s][i]==fa) continue;
		dfs2(son[s][i],s);
	}
	return;
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	n=read();m=read();
	for (int i=1;i<=m;++i){
		int u=read(),v=read();
		son[u][++son[u][0]]=v;son[v][++son[v][0]]=u;
	}
	for (int i=1;i<=n;++i) sort(son[i]+1,son[i]+1+son[i][0]);
	if (m==n){
		return 0;
	}else dfs2(1,0);
	for (int i=1;i<=n;++i) printf("%d ",q[i]);
	return 0;
}
