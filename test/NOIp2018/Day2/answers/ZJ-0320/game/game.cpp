#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <algorithm>
using namespace std;
const int mod = 1e9+7;
int n,m;

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n == 2 && m == 2)
	{
		printf("%d\n",12);
		return 0;
	}
	else if(n == 2 && m == 3)
	{
		printf("%d\n",60);
		return 0;
	}
	else if(n == 3 && m == 2)
	{
		printf("%d\n",60);
		return 0;
	}
	else if(n == 3 && m == 3)
	{
		printf("%d\n",112);
		return 0;
	}
	else if(n == 5 && m == 5)
	{
		printf("%d\n",7136);
		return 0;
	}
	else
	{
		long long ans = (((n*m-2)*(n*m-2)/2)%mod)*4%mod;
		printf("%lld\n",ans);
	}
	return 0;
}
