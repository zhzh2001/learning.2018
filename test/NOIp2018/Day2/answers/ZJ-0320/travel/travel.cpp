#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <algorithm>
using namespace std;
const int maxn = 5050;
int n,m,cnt = 0,sum1;
int head[maxn],ans[maxn];
bool vis[maxn],vis2[maxn];
int num[maxn];

struct Edge
{
	int nxt,to;
}edge[maxn<<1];

void add(int x,int y)
{
	edge[++cnt].to = y;
	edge[cnt].nxt = head[x];
	head[x] = cnt;
}

void dfs(int x,int sum)
{	
	if(!vis[x])
		sum1++;
	ans[sum1] = min(ans[sum1],x);
	if(sum1 == n)
		return ;
	vis[x] = 1;
	int pos = 1000000;
	for(int j = 1; j <= num[x]; ++j)
	{
		int i;
		pos = 1000000;
		for(i = head[x]; i ; i = edge[i].nxt)
		{
			int y = edge[i].to;
			if(!vis[y])
				pos = min(y,pos);
		}
		vis2[i] = 1;
		if(pos == 1000000)
			return ;
		else
			dfs(pos,sum+1);
	}
	return ;
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i = 1; i <= m; ++i)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
		num[x]++;
		num[y]++;
	}
	memset(ans,127,sizeof(ans));
	dfs(1,1);
	for(int i = 1; i < n; ++i)
		printf("%d ",ans[i]);
	printf("%d\n",ans[n]);
	return 0;
}
