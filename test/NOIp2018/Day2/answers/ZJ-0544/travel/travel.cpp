#include <bits/stdc++.h>
using namespace std;

int n, m;
int u, v, IG[5009][5009], vis[5009];

void add(int x, int y)
{
	IG[x][++IG[x][0]]=y;
}

void ss(int x)
{
	printf("%d ", x);
	vis[x]=1;
	for (int i=1; i<=IG[x][0]; i++)
		if (!vis[IG[x][i]]) ss(IG[x][i]);
}

int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i=1; i<=m; i++)
		scanf("%d%d", &u, &v), add(u, v), add(v, u);
	for (int i=1; i<=n; i++)
		sort(IG[i]+1, IG[i]+IG[i][0]+1);
	if (m+1==n) ss(1);
	else for (int i=1; i<=50; i++) printf("IGNB!!!\n");
	return 0;
}
