#include<bits/stdc++.h>
#define debug(x) cerr<<"\tDEBUG: "<<#x<<" = "<<(x)<<endl
using namespace std;
const int maxn=1e5+5,inf=2e9;
int n,m,ax,bx,a,b,A[maxn];
vector<int>T[maxn];
char str[5];
struct P11{
	long long dp[2][maxn];
	void dfs(int f,int x){
		dp[0][x]=0;
		dp[1][x]=A[x];
		for(int i=0;i<T[x].size();i++){
			int y=T[x][i];
			if(y==f)continue;
			dfs(x,y);
			dp[1][x]+=min(dp[0][y],dp[1][y]);
			dp[0][x]+=dp[1][y];
		}
		if(x==a)dp[1-ax][x]=inf;
		else if(x==b)dp[1-bx][x]=inf;
	}
	void solve(){
		while(m--){
			scanf("%d%d%d%d",&a,&ax,&b,&bx);
			dfs(0,1);
			long long ans=min(dp[1][1],dp[0][1]);
			if(ans>=inf)ans=-1;
			printf("%lld\n",ans);
		}
	}
}p11;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,str+1);
	
	for(int i=1;i<=n;i++)
		scanf("%d",&A[i]);
	for(int i=1;i<n;i++){
		scanf("%d%d",&a,&b);
		T[a].push_back(b);
		T[b].push_back(a);
	}
	p11.solve();
	return 0;
}
