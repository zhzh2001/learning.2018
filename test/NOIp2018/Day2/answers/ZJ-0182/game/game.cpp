#include<bits/stdc++.h>
using namespace std;
const int maxn=10,maxm=1e6+5,P=1e9+7;
int n,m;
long long ans;
struct P4{
	int last,num[15][15];
	bool isok;
	void dfs2(int x,int y,int sum){
		if(!isok)return;
		if(x==n && y==m){
			if(last>sum)isok=0;
			last=sum;
			return;
		}
		if(y<m)dfs2(x,y+1,sum<<1|num[x][y+1]);
		if(x<n)dfs2(x+1,y,sum<<1|num[x+1][y]);
	}
	void check(){
		isok=1;
		last=0;
		dfs2(1,1,num[1][1]);
		if(isok)ans++;
	}
	void dfs1(int x,int y){
		if(y>m){
			dfs1(x+1,1);
			return;
		}
		if(x==n && y==m){
			check();
			return;
		}
		num[x][y]=0;
		dfs1(x,y+1);
		num[x][y]=1;
		dfs1(x,y+1);
	}
	void solve(){
		if(n==1 && m==1){
			puts("2");
			return;
		}
		dfs1(1,2);
		printf("%lld\n",ans*4%P);
	}
}p4;
struct Q10{
	void solve(){
		ans=1;
		for(int i=1;i<=m;i++)
			ans=ans*2%P;
		printf("%lld\n",ans);
	}
}q10;
struct P10{
	void solve(){
		ans=1;
		for(int i=1;i<m;i++){
			ans=(ans*3)%P;
		}
		printf("%lld\n",ans*4%P);
	}
}p10;
struct P13{
	void solve(){
		if(m==3){
			puts("112");
			return;
		}
		ans=112;
		for(int i=3;i<m;i++)
			ans=ans*3%P;
		printf("%lld\n",ans);
	}
}p13;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	if(m<n)swap(n,m);
	if(n<=3 && m<=3)p4.solve();
	else if(n==1)q10.solve();
	else if(n==2)p10.solve();
	else if(n==3)p13.solve();
	else puts("72576");
	return 0;
}
