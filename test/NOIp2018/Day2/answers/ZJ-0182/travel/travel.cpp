#include<bits/stdc++.h>
using namespace std;
const int maxn=5005;
int n,m,x,y;
vector<int>T[maxn];
struct P15{
	int cnt;
	void dfs(int f,int x){
		cnt++;
		printf("%d%c",x,cnt==n?'\n':' ');
		for(int i=0;i<T[x].size();i++)
			if(T[x][i]!=f)dfs(x,T[x][i]);
	}
	void solve(){
		cnt=0;
		for(int i=1;i<=m;i++){
			scanf("%d%d",&x,&y);
			T[x].push_back(y);
			T[y].push_back(x);
		}
		for(int i=1;i<=n;i++)
			sort(T[i].begin(),T[i].end());
		dfs(0,1);
	}
}p15;
struct P25{
	int dep[maxn],fa[maxn],ans[maxn],res[maxn],tot;
	int num[maxn],id;
	bool vis[maxn];
	int a,b;
	void dfs1(int f,int x){
		dep[x]=dep[f]+1;
		fa[x]=f;
		vis[x]=1;
		for(int i=0;i<T[x].size();i++){
			int y=T[x][i];
			if(y==f)continue;
			if(vis[y]){
				a=x;
				b=y;
				continue;
			}
			dfs1(x,y);
		}
	}
	void dfs2(int f,int x,int p){
		res[++tot]=x;
		vis[x]=1;
		if(x==p)return;
		for(int i=0;i<T[x].size();i++){
//		if(p==2)cout<<x<<endl;
			int y=T[x][i];
			if(y==f || vis[y])continue;
			dfs2(x,y,p);
		}
	}
	bool isok(){
		if(ans[1]==0)return true;
		for(int i=1;i<=n;i++)
			if(res[i]>ans[i])return false;
			else if(res[i]<ans[i])return true;
		return false;
	}
	void check(int x){
		memset(vis,0,sizeof(vis));
		tot=0;
		dfs2(0,1,x);
		if(tot!=n)return;
		if(isok()){
			for(int i=1;i<=n;i++)
				ans[i]=res[i];
		}
	}
	void solve(){
		for(int i=1;i<=m;i++){
			scanf("%d%d",&x,&y);
			T[x].push_back(y);
			T[y].push_back(x);
		}
		for(int i=1;i<=n;i++)
			sort(T[i].begin(),T[i].end());
		dfs1(0,1);
		if(dep[a]<dep[b])swap(a,b);
		while(a!=b){
			num[++id]=a;
			a=fa[a];
		}
		num[++id]=b;
		for(int i=1;i<=id;i++)
			check(num[i]);
		check(0);
		for(int i=1;i<=n;i++)
			printf("%d%c",ans[i],i==n?'\n':' ');
	}
}p25;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	if(m==n-1)p15.solve();
	else p25.solve();
}
