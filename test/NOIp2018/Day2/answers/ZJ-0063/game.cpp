#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <algorithm>
using namespace std;
typedef long long LL;
const int P = 1000000007;
int a[100][100], n, m, ans, temp, cnt, c[600], d[600];
int dp[1000005][4];
int rapidPow(int a, int b) {
	int ret = 1;
	while (b) {
		if (b & 1) ret = (LL)ret * a % P;
		a = (LL)a * a % P;
		b >>= 1;
	}
	return ret;
}
void dfs1(int x, int y, int k, int st) {
	if (x == n && y == m) {
		c[++cnt] = k;
		d[cnt] = st;
		return;
	}
	if (y < m) dfs1(x, y + 1, k << 1 | a[x][y + 1], st * 10 + 1);
	if (x < n) dfs1(x + 1, y, k << 1 | a[x + 1][y], st * 10);
}
void dfs(int x, int y) {
	if (x > n) {
		cnt = 0;
		dfs1(1, 1, a[1][1], 0);
		bool flag = true;
		for (int p = 1; p <= cnt; ++p)
			for (int l = 1; l <= cnt; ++l)
				if (d[p] > d[l] && c[p] > c[l]) {
					flag = false;
					break;
				}
		ans += flag;
		return;
	}
	a[x][y] = 0;
	if (y < m) dfs(x, y + 1);
	else dfs(x + 1, 1);
	a[x][y] = 1;
	if (y < m) dfs(x, y + 1);
	else dfs(x + 1, 1);
}
int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n == 1) printf("%d\n", rapidPow(2, m));
	else if (n <= 3 && m <= 3) {
		dfs(1, 1);
		cout << ans << endl;
	} else if (n == 2) {
		dp[1][0] = dp[1][1] = dp[1][2] = dp[1][3] = 1;
		for (int i = 1; i < m; ++i)
			for (int j = 0; j <= 3; ++j)
				for (int k = 0; k <= 3; ++k)
					if (((k >> 1) & 1) <= (j & 1))
						(dp[i + 1][k] += dp[i][j]) %= P;
		for (int i = 0; i <= 3; ++i)
			(ans += dp[m][i]) %= P;
		printf("%d\n", ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
