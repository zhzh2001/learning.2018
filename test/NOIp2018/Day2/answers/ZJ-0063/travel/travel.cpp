#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <algorithm>
using namespace std;
const int MAXN = 5005;
int a[MAXN], ans[MAXN], n, m, e[MAXN][2], top;
bool edge[MAXN][MAXN], vis[MAXN];
void dfs(int u) {
	a[++top] = u;
	vis[u] = true;
	for (int v = 1; v <= n; ++v)
		if (!vis[v] && edge[u][v])
			dfs(v);
}
int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; ++i) {
		scanf("%d%d", &e[i][0], &e[i][1]);
		edge[e[i][0]][e[i][1]] = true;
		edge[e[i][1]][e[i][0]] = true;
	}
	if (m == n - 1) {
		dfs(1);
		for (int i = 1; i < n; ++i) printf("%d ", a[i]);
		printf("%d\n", a[n]);
	} else {
		ans[1] = 1e9;
		for (int i = 1; i <= m; ++i) {
			edge[e[i][0]][e[i][1]] = false;
			edge[e[i][1]][e[i][0]] = false;
			top = 0;
			memset(vis, false, sizeof(vis));
			dfs(1);
			edge[e[i][0]][e[i][1]] = true;
			edge[e[i][1]][e[i][0]] = true;
			if (top < n) continue;
			for (int j = 1; j <= n; ++j)
				if (a[j] != ans[j]) {
					if (a[j] < ans[j]) memcpy(ans, a, sizeof(a));
					break;
				}
		}
		for (int i = 1; i < n; ++i) printf("%d ", ans[i]);
		printf("%d\n", ans[n]);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
