#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <algorithm>
using namespace std;
typedef long long LL;
const LL INF = 1e15;
const int MAXN = 100005;
struct edge {
	int to, nxt;
} edge[MAXN << 1];
int n, m, val[MAXN], fir[MAXN], ecnt, a, x, b, y;
LL dp[MAXN][2];
char type[10];
void addedge(int u, int v) {
	edge[++ecnt].to = v;
	edge[ecnt].nxt = fir[u];
	fir[u] = ecnt;
}
void dfs(int u, int fa) {
	dp[u][0] = 0;
	dp[u][1] = val[u];
	for (int e = fir[u]; e; e = edge[e].nxt) {
		int v = edge[e].to;
		if (v == fa) continue;
		dfs(v, u);
		dp[u][0] += dp[v][1];
		dp[u][1] += min(dp[v][0], dp[v][1]);
	}
	if (u == a) dp[u][x ^ 1] = INF;
	if (u == b) dp[u][y ^ 1] = INF;
	dp[u][0] = min(dp[u][0], INF);
	dp[u][1] = min(dp[u][1], INF);
}
int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d%s", &n, &m, type);
	for (int i = 1; i <= n; ++i)
		scanf("%d", &val[i]);
	for (int i = 1; i < n; ++i) {
		int u, v;
		scanf("%d%d", &u, &v);
		addedge(u, v);
		addedge(v, u);
	}
	for (int i = 1; i <= m; ++i) {
		scanf("%d%d%d%d", &a, &x, &b, &y);
		dfs(1, 0);
		LL ans = min(dp[1][0], dp[1][1]);
		if (ans == INF) puts("-1");
		else printf("%lld\n", ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
