#include<cstdio>
#include<cstring>
using namespace std;
int n,m,len,ans;char mp[100][100],q[100];
struct Path
{
	char w[100],s[100];
}P[100000];
void check()
{
	for(int i=1;i<=len;i++)
	{
		int x=1,y=1;
		for(int j=1;j<n+m;j++)
		{
			P[i].s[j]=mp[x][y];
			if(P[i].w[j]=='R') y++;
			else x++;
		}
		P[i].s[n+m-1]=mp[n][m];
	}
	for(int i=1;i<=len;i++)
		for(int j=1;j<=len;j++)
			if((strcmp(P[i].w+1,P[j].w+1)==1)&&(strcmp(P[i].s+1,P[j].s+1)==1))
				return;
	ans++;
}
void MP(int x,int y,int stp)
{
	if(x==n&&y==m)
	{
		len++;strcpy(P[len].w+1,q+1);
		return;
	}
	if(x!=n)
	{
		q[stp]='D';
		MP(x+1,y,stp+1);
	}
	if(y!=m)
	{
		q[stp]='R';
		MP(x,y+1,stp+1);
	}
}
void DFS(int x,int y)
{
	if(x>n){check();return;}
	mp[x][y]='1';
	DFS(y==m?x+1:x,y==m?1:y+1);
	if(mp[x-1][y+1]=='1') return;
	mp[x][y]='0';
	DFS(y==m?x+1:x,y==m?1:y+1);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	MP(1,1,1);
	DFS(1,1);
	printf("%d\n",ans);
	return 0;
}
