#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long LL;
const int maxn=100005,maxm=200005;
int n,m,tot,lnk[maxn],son[maxm],nxt[maxm],a,b,x,y;char typ[5];LL F[maxn][2],p[maxn],ans;
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9'){ret=ret*10+ch-'0';ch=getchar();}
	return ret*f;
}
inline void add_e(int x,int y){tot++;son[tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;}
void Solve(int now,int fa)
{
	for(int i=lnk[now];i;i=nxt[i])
		if(son[i]!=fa)
			Solve(son[i],now);
	if((now==a&&x==0)||(now==b&&y==0)) F[now][1]=100000000000LL;
	else
	{
		F[now][1]=p[now];
		for(int i=lnk[now];i;i=nxt[i])
			if(son[i]!=fa)
				F[now][1]+=min(F[son[i]][0],F[son[i]][1]);
	}
	if((now==a&&x==1)||(now==b&&y==1)) F[now][0]=100000000000LL;
	else
	{
		F[now][0]=0;
		for(int i=lnk[now];i;i=nxt[i])
			if(son[i]!=fa)
				F[now][0]+=F[son[i]][1];
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",typ);
	for(int i=1;i<=n;i++) p[i]=read();
	for(int i=1;i<n;i++)
	{
		int a=read(),b=read();
		add_e(a,b);add_e(b,a);
	}
	while(m--)
	{
		a=read();x=read();b=read();y=read();
		Solve(1,0);ans=min(F[1][0],F[1][1]);
		printf("%lld\n",ans<100000000000LL?ans:-1);
	}
	return 0;
}
