#include<cstdio>
#include<vector>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=5005,maxm=10005;
int n,m,deg[maxn],tep[maxn],que[maxn],len,ans[maxn],top,stk[maxn];bool cir[maxn],vis[maxn];
vector<int> To[maxn];
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9'){ret=ret*10+ch-'0';ch=getchar();}
	return ret*f;
}
inline void add_e(int x,int y){To[x].push_back(y);deg[x]++;}
void DFS(int now,int fa)
{
	if(!vis[now])ans[++len]=now;vis[now]=true;
	for(int i=1;i<=deg[now];i++)
		if(To[now][i]!=fa&&!cir[To[now][i]]&&!vis[To[now][i]])
			DFS(To[now][i],now);
}
inline void Toposort()
{
	memcpy(tep,deg,sizeof(deg));
	memset(cir,true,sizeof(cir));
	int hed=0,til=0;
	for(int i=1;i<=n;i++)
		if(tep[i]==1)
			{til++;que[til]=i;cir[i]=false;}
	while(hed!=til)
	{
		hed++;
		for(int i=1;i<=deg[que[hed]];i++)
		{
			if(cir[To[que[hed]][i]])
			{
				tep[To[que[hed]][i]]--;
				if(tep[To[que[hed]][i]]==1)
				{
					cir[To[que[hed]][i]]=false;
					til++;que[til]=To[que[hed]][i];
				}
			}
		}
	}
}
inline void SolveCircle(int rot)
{
	ans[++len]=rot;vis[rot]=true;
	bool flg=false;int p=rot,nx=0;
	for(int i=1;i<=deg[rot];i++)
	{
		if(!vis[To[rot][i]])
		{
			if(nx)
			{
				nx=To[rot][i];
				break;
			}
			nx=To[rot][i];
		}
	}
	stk[1]=rot;top=1;
	while(top)
	{
		for(int i=1;i<=deg[stk[top]];i++)
		{
			if(vis[To[stk[top]][i]]) continue;
			if(!cir[To[stk[top]][i]]) DFS(stk[top],0);
			else
			{
				if(!flg&&nx<To[stk[top]][i])
				{
					flg=true;
					while(top>1)
					{
						if(deg[stk[top]]>2) DFS(stk[top],0);
						top--;
					}
					top++;break;
				}
				stk[++top]=To[stk[top-1]][i];
				ans[++len]=stk[top];
				vis[stk[top]]=true;
				top++;break;
			}
		}
		top--;
	}
}
void DFS2(int now,int fa)
{
	ans[++len]=now;vis[now]=true;
	for(int i=1;i<=deg[now];i++)
	{
		if(To[now][i]!=fa)
		{
			if(!cir[To[now][i]]) DFS2(To[now][i],now);
			else SolveCircle(To[now][i]);
		}
	}
}
inline void Solve()
{
	Toposort();
	if(cir[1]) SolveCircle(1);
	else DFS2(1,0);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++) To[i].push_back(0);
	for(int i=1;i<=m;i++)
	{
		int a=read(),b=read();
		add_e(a,b);add_e(b,a);
	}
	for(int i=1;i<=n;i++) sort(To[i].begin(),To[i].end());
	if(m==n-1) DFS(1,0);
	else Solve();
	for(int i=1;i<=n;i++)
		printf("%d%c",ans[i],i==n?'\n':' ');
	return 0;
}
