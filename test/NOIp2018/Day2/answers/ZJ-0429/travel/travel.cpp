#include <bits/stdc++.h>
#define int long long
using namespace std;
int n,m,cnt,head[12000],a[12000],num,v[12000],b[12000];
struct node {
	int next,to;
} sxd[24000];
bool vis[12000];
int read() {
	register int x=0,f=1;
	register char ch=getchar();
	while(!(ch>='0' && ch<='9')) {
		if(ch=='-') f=-1;
		ch=getchar();
	}
	while(ch>='0' && ch<='9') {
		x=x*10+ch-'0';
		ch=getchar();	
	}
	return x*f;
}
void write(int x) {
	if(x>=10) write(x/10);
	register char ch=x%10+'0';
	putchar(ch);
}
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	head[u]=cnt;
}
set <int> q[5200];
void dfs(int at,int f) {
	a[num]=at;
	vis[at]=1;
	if(num==n) {
		for(register int i=1; i<n; i++) {
			write(a[i]);
			putchar(' ');
		}
		write(a[n]);
		exit(0);
	}
	q[at].clear();
	for(register int i=head[at]; i; i=sxd[i].next) {
		if(sxd[i].to!=f && !vis[sxd[i].to]) q[at].insert(sxd[i].to);
	}
	if(!q[at].size()) return;
	for(register set <int> :: iterator it=q[at].begin(); it!=q[at].end(); it++) {
		int x=(*it);
		if(x>n) continue;
		num++;
		dfs(x,at);
	}
}
void dfs1(int at,int f) {
	a[num]=at;
	vis[at]=1;
	if(num==n) {
		bool flag=true;
		for(register int i=1; i<=n; i++) {
			if(a[i]<b[i]) {
				flag=true;
				break;
			} else if(a[i]>b[i]) return;
		}
		if(flag) {
			for(int i=1; i<=n; i++) b[i]=a[i];
		}
	}
	q[at].clear();
	for(register int i=head[at]; i; i=sxd[i].next) {
		if(sxd[i].to!=f && !vis[sxd[i].to] && !v[i]) q[at].insert(sxd[i].to);
	}
	if(!q[at].size()) return;
	for(register set <int> :: iterator it=q[at].begin(); it!=q[at].end(); it++) {
		int x=(*it);
		if(x>n) continue;
		num++;
		dfs1(x,at);
	}
}
signed main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	for(register int i=1; i<=m; i++) {
		int x=read(),y=read();
		add(x,y);
		add(y,x);
	}
	num=1;
	if(m==n-1) dfs(1,0);
	else {
		for(register int i=1; i<=n; i++) b[i]=2147483647;
		for(register int i=1; i<=cnt/2; i++) {
			memset(v,0,sizeof v);
			memset(vis,0,sizeof vis);
			num=1;
			v[(i-1)*2+1]=v[(i-1)*2+2]=true;
			dfs1(1,0);
		}
		for(register int i=1; i<n; i++) {write(b[i]);putchar(' ');}
		write(b[n]);
	}
	return 0;
}
