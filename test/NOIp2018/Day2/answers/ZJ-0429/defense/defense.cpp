#include <bits/stdc++.h>
#define int long long
using namespace std;
int n,m,s[120000],head[120000],cnt;
string ch;
struct node {
	int next,to;
} sxd[240000];
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	head[u]=cnt;
}
map <pair <int,int>,bool> mp;
int f[120000],vis[120000],v[120000],ans;
void dfs(int at,int val) {
//	cout<<"dfs" << at << " " << val << '\n';
	if(v[at]==0 && vis[at]==1 && f[at-1]==0 && at!=1) return;
	if(val>ans) return;
	if(vis[at]) {
		f[at]=v[at];
		dfs(at+1,val+((v[at]==1)?s[at]:0));
		return;
	}
	if(at==n+1) {
		ans=min(ans,val);
		return;
	}
	f[at]=1;
	dfs(at+1,val+s[at]);
	if(f[at-1]==1) {
		f[at]=0;
		dfs(at+1,val);
	} 
}
int bianx[12000],biany[12000];
signed main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m>>ch;
	for(int i=1; i<=n; i++) cin>>s[i];
	for(register int i=1; i<n; i++) {
		int x,y;
		cin>>x>>y;
		bianx[i]=x;
		biany[i]=y;
		mp[make_pair(min(x,y),max(x,y))]=1;
		add(x,y);
		add(y,x);
	}
	if(ch[0]=='A') {
		for(register int i=1; i<=m; i++) {
			memset(f,0,sizeof f);
			memset(vis,0,sizeof vis);
			memset(v,0,sizeof v);
			ans=2147483647;
			int a,b,c,d;
			cin>>a>>b>>c>>d;
			if(mp[make_pair(min(a,c),max(a,c))]==1 && (!b) && (!d)) {
				puts("-1");
				continue;
			}
			vis[a]=vis[c]=true;
			v[a]=b;
			v[c]=d;
			dfs(1,0);
			cout<<ans<<'\n';
		}
	} else if(n<=10){
		for(int i=1;i<=m;i++) {
			int a,b,c,d;
			cin>>a>>b>>c>>d;
			if(mp[make_pair(min(a,c),max(a,c))]==1 && (!b) && (!d)) {
				puts("-1");
				continue;
			}
			int ans=2147483647;
			for(int j=0;j<(1<<n);j++) {
				int val=0;
				for(int k=1;k<=n;k++) {
					if(j & (1<<(k-1))) val+=s[k];	
				}
				if(  (j & (1<<(a-1) ) ==0) && b==1  ) continue;
				if(  (j & (1<<(c-1) ) ==0) && d==1  ) continue;
				if(  (j & (1<<(a-1) ) !=0) && b==0  ) continue;
				if(  (j & (1<<(c-1) ) !=0) && d==0  ) continue;
				for(int k=1;k<=n-1;k++) {
					int x=bianx[k],y=biany[k];
					if((j & (1<<(x-1)))==0 && (j & (1<<(y-1)))==0) goto L1;	
				}
				if(ans>val) {
					ans=val;
				}
				L1:;
			}
			if(ans>=2147483647) puts("-1");
			else cout<<ans<<'\n';
		}	
	} else {
		for(int i=1;i<=m;i++) puts("-1");	
	}
	return 0;
}
