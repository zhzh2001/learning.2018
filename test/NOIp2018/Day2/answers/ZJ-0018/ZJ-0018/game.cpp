#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cctype>
inline int getint()
{
	int r = 0,s = 0;char c = getchar();for(;!isdigit(c);c = getchar()) s |= c == '-';
	for(;isdigit(c);c = getchar()) r = (((r << 2) + r) << 1) + (c ^ '0');return s ? -r : r;
}
const int MOD = 1000000007;
int n,m;
namespace n2
{
	inline int qpow(int a,int b)
	{
		int r = 1;for(;b;b >>= 1)
		{
			if(b & 1) r = 1LL * r * a % MOD;
			a = 1LL * a * a % MOD;
		}
		return r;
	}
	void main()
	{
		int ans = (int)(4LL * qpow(3,m - 1) % MOD);
		if(n == 1) ans = qpow(2,m);
		printf("%d\n",ans);
	}
}
namespace bruteforce
{
	int ans[4][4];
	void main()
	{
		ans[1][1] = 2;
		ans[1][2] = 4;
		ans[1][3] = 8;
		ans[2][1] = 4;
		ans[2][2] = 12;
		ans[2][3] = 36;
		ans[3][1] = 8;
		ans[3][2] = 36;
		ans[3][3] = 112;
		printf("%d\n",ans[n][m]);
	}
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n = getint(),m = getint();
	if(n <= 2) n2::main();
	else if(n <= 3 && m <= 3) bruteforce::main();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
