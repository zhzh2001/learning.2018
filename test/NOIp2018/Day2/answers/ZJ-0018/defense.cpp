#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cctype>
typedef long long LL;
inline int getint()
{
	int r = 0,s = 0;char c = getchar();for(;!isdigit(c);c = getchar()) s |= c == '-';
	for(;isdigit(c);c = getchar()) r = (((r << 2) + r) << 1) + (c ^ '0');return s ? -r : r;
}
inline char getupper(){char c = getchar();while(!isupper(c)) c = getchar();return c;}
char typec;int typen;
int n,m;
const int N = 100010;
const LL inf = 0x3f3f3f3f3f3f3f3f;
int head[N],next[N << 1],to[N << 1],tot = 0;
inline void addedge(int u,int v)
{
	next[++tot] = head[u];to[tot] = v;head[u] = tot;
	next[++tot] = head[v];to[tot] = u;head[v] = tot;
}
int p[N];
namespace bruteforce
{
	int seq[N],a,x,b,y;
	LL ans;
	inline bool judge()
	{
		if(seq[a] != x || seq[b] != y) return 0;
		for(int u = 1;u <= n;u++)
		{
			for(int i = head[u],v = to[i];~i;v = to[i = next[i]])
			{
				if((!seq[u]) && (!seq[v])) return 0;
			}
		}
		return 1;
	}
	inline void dfs(int u)
	{
		if(u == n + 1)
		{
			if(judge())
			{
				//for(int i = 1;i <= n;i++) printf("%d %d\n",i,seq[i]);
				//puts("----");
				LL ret = 0;
				for(int i = 1;i <= n;i++) if(seq[i]) ret += p[i];
				ans = std::min(ans,ret);
			}
			return;
		}
		seq[u] = 0;dfs(u + 1);
		seq[u] = 1;dfs(u + 1);
	}
	void main()
	{
		for(int i = 1;i <= m;i++)
		{
			a = getint(),x = getint(),b = getint(),y = getint();
			ans = inf;
			dfs(1);
			printf("%lld\n",ans == inf ? -1 : ans);
		}
	}
}
namespace chain
{
	int a,x,b,y;
	LL f[N];
	void main()
	{
		for(int i = 1;i <= m;i++)
		{
			a = getint(),x = getint(),b = getint(),y = getint();
			f[0] = 0;f[1] = ((a == 1 && x == 0) || (b == 1 && y == 0)) ? inf : p[1];
			for(int i = 2;i <= n;i++)
			{
				f[i] = f[i - 1];
				if(!((a == i - 1 && x == 1) || (b == i - 1 && y == 1))) f[i] = std::min(f[i],f[i - 2] + p[i]);
				if((a == i && x == 0) || (b == i && y == 0)) f[i] = inf;
			}
			LL ans = std::min(f[n - 1],f[n]);
			printf("%lld\n",ans >= inf ? -1 : ans);
		}
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof(head));
	n = getint(),m = getint();
	typec = getupper();
	typen = getint();
	for(int i = 1;i <= n;i++) p[i] = getint();
	for(int i = 1;i < n;i++) addedge(getint(),getint());
	if(n <= 10 && m <= 10) bruteforce::main();
	else if(typec == 'A') chain::main();
	return 0;
}
