#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cctype>
#include <vector>
inline int getint()
{
	int r = 0,s = 0;char c = getchar();for(;!isdigit(c);c = getchar()) s |= c == '-';
	for(;isdigit(c);c = getchar()) r = (((r << 2) + r) << 1) + (c ^ '0');return s ? -r : r;
}
const int N = 5050;
int head[N],next[N << 1],to[N << 1],tot = 0;
inline void addedge(int u,int v)
{
	next[++tot] = head[u];to[tot] = v;head[u] = tot;
	next[++tot] = head[v];to[tot] = u;head[v] = tot;
}
int n,m;
namespace tree
{
	std::vector<int> V[N];
	int res[N],cnt = 0;
	inline void dfs(int u,int fa)
	{
		for(int i = head[u],v = to[i];~i;v = to[i = next[i]])
		{
			if(v == fa) continue;
			V[u].push_back(v);
		}
		std::sort(V[u].begin(),V[u].end());
		res[++cnt] = u;
		for(int i = 0,sz = V[u].size();i < sz;i++) dfs(V[u][i],u);
	}
	void main()
	{
		dfs(1,0);
		for(int i = 1;i <= n;i++) printf("%d%c",res[i]," \n"[i == n]);
	}
}
namespace graph
{
	int dep[N],fa[N];
	int cir[N],cc = 0,ctop;
	inline void dfs(int u)
	{
		//printf("# %d\n",u);
		//static int sha = 0;
		//if(++sha == 50) abort();
		for(int i = head[u],v = to[i];~i;v = to[i = next[i]])
		{
			if(v == fa[u]) continue;
			if(!dep[v])
			{
				dep[v] = dep[u] + 1;
				fa[v] = u;
				dfs(v);
			}
			else if(dep[v] < dep[u])
			{
				//printf("@@@ %d\n",v);
				cir[++cc] = v;
				int x = u;
				do
				{
					//printf("$ %d\n",x);
					//static int qwq = 0;
					//if(++qwq == 30) abort();
					cir[++cc] = x;
					x = fa[x];
				}while(x != v);
			}
		}
	}
	void main()
	{
		//abort();
		dfs(1);
		std::reverse(cir + 2,cir + cc + 1);
		//for(int i = 1;i <= n;i++) printf("%d %d\n",i,fa[i]);
		//for(int i = 1;i <= cc;i++) printf("@ %d\n",cir[i]);
		int l = 2,r = cc;
		if(cir[l] < cir[r])
		{
			while(cir[l] < cir[r]) fa[cir[l]] = cir[l - 1],l++;
			//printf("!%d %d\n",l,r);
			if(l != r)
			{
				fa[cir[r]] = cir[1];
				while(r != l)
				{
					fa[cir[r - 1]] = cir[r];
					r--;
				}
			}
		}
		else
		{
			while(cir[l] > cir[r]) fa[cir[r]] = r == cc ? cir[1] : cir[r + 1],r--;
			//printf("!%d %d\n",l,r);
			if(l != r)
			{
				fa[cir[l]] = cir[1];
				while(r != l)
				{
					fa[cir[l + 1]] = cir[l];
					l++;
				}
			}
		}
		//for(int i = 1;i <= n;i++) printf("%d %d\n",i,fa[i]);
		memset(head,-1,sizeof(head));
		tot = 0;
		for(int i = 1;i <= n;i++) if(fa[i]) addedge(i,fa[i]);
		tree::main();
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(head,-1,sizeof(head));
	n = getint();m = getint();
	for(int i = 1;i <= m;i++) addedge(getint(),getint());
	if(m == n - 1) tree::main();
	else graph::main();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
