#include<stdio.h>
#include<stdlib.h>
#define maxn 5010
#include<vector>
#include<algorithm>
#include<stack>
using namespace std;
int book[maxn<<1],fa[maxn],MAP[maxn][maxn],n,m,a[maxn],tot=0;
int ans[maxn];
struct edge{
	int u,v;
} e[maxn<<1];
int first[maxn],next[maxn<<1],cnt=0,used[maxn];
vector<int> son[maxn];
stack<int> s;
void add(int x,int y){
	e[++cnt].u=x;
	e[cnt].v=y;
	next[cnt]=first[x];
	first[x]=cnt;
}
bool cmp(int x,int y){
	return x>y;
}
void dfs(int step){
	int i=first[step];
	a[++tot]=step;
	while(!son[step].empty()) son[step].pop_back();
	while(i){
		if(e[i].v!=fa[step]&&!book[i]&&!used[e[i].v]){
			used[e[i].v]=1;
			fa[e[i].v]=step;
			son[step].push_back(e[i].v);
		}
		i=next[i];
	}
	sort(son[step].begin(),son[step].end(),cmp);
	while(!son[step].empty()){
		dfs(son[step].back());
		son[step].pop_back();
	}
	return;
}
int main(){
	int i,j,x,y,flag=0;
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	if(m==n-1){
		dfs(1);
		for(i=1;i<=n;i++)
			printf("%d ",a[i]);
	}
	else{
		for(i=1;i<=2*m;i+=2){
			tot=0;
			book[i]=book[i+1]=1;
			for(j=2;j<=n;j++)
				used[j]=0;
			used[1]=1;
			dfs(1);
			book[i]=book[i+1]=0;
			if(tot==n){
				if(flag){
					bool ok=true;
					for(j=1;j<=n;j++){
						if(a[j]<ans[j]) break;
						if(a[j]>ans[j]){
							ok=false;
							break;
						}
					}
					if(ok){
						for(j=1;j<=n;j++){
							ans[j]=a[j];
						}
					}
				}
				else{
					for(j=1;j<=n;j++)
						ans[j]=a[j];
					flag=1;
				}
			}
		}
		for(i=1;i<=n;i++)
			printf("%d ",ans[i]);
	}
	return 0;
}
