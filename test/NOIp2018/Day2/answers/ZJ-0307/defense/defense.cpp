#include<stdio.h>
#include<stdlib.h>
#define maxn 100010
#define ll long long
#define inf 0x3f3f3f3f
#include<algorithm>
using namespace std;
char type[2];
struct edge{
	int u,v;
} e[maxn<<1];
int first[maxn],next[maxn<<1],cnt=0,fa[maxn],book[maxn];
long long p[maxn],dp[maxn][2];
void add(int x,int y){
	e[++cnt].u=x;
	e[cnt].v=y;
	next[cnt]=first[x];
	first[x]=cnt;
}
void dfs(int step){
	int i=first[step];
	dp[step][0]=0;
	dp[step][1]=p[step];
	while(i){
		if(e[i].v!=fa[step]){
			fa[e[i].v]=step;
			dfs(e[i].v);
			dp[step][0]+=dp[e[i].v][1];
			dp[step][1]+=min(dp[e[i].v][0],dp[e[i].v][1]);
		}
		i=next[i];
	}
	if(book[step]>=0) dp[step][book[step]^1]=inf;
	return;
}
int main(){
	int i,j,n,m,x,y,a,b;
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%s",type);
	for(i=1;i<=n;i++)
		scanf("%lld",&p[i]);
	for(i=1;i<=n;i++)
		book[i]=-1;
	for(i=1;i<n;i++){
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	for(i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		book[a]=x;
		book[b]=y;
		dfs(1);
		ll ans=min(dp[1][0],dp[1][1]);
		if(ans<inf) printf("%lld\n",ans);
		else printf("-1\n");
		book[a]=book[b]=-1;
	}
	return 0;
}
