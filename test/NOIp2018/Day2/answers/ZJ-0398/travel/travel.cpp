#include<cstdio>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<set>
#include<vector>
#define INF (1<<29)
#define LL long long
#define unsigned long long
#define sk() (putchar(' '))
#define ek() (putchar('\n'))
#define MAXN 5005
#define sit set<int>::iterator
using namespace std;
int Read(){
	int X=0,flag=1;char ch=0;
	while (ch<'0'||ch>'9'){
		if (ch=='-') flag=-1;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9')
	  X=(X<<3)+(X<<1)+ch-48,ch=getchar();
	return X*flag;
}
void Write(int x){
	if (x<0) putchar('-'),x=-x;
	if (x>9) Write(x/10);
	putchar(x%10+48);
}
int n,m;
set<int> s[MAXN];
int ans[MAXN],tot;
bool vis[MAXN];
void dfs(int now){
	if (!vis[now]) ans[++tot]=now;
	vis[now]=1;
	for (sit it=s[now].begin();it!=s[now].end();it++){
		int tmp=*it;
		if (vis[tmp]) continue;
		dfs(tmp);
	}
}
int sign,dfn[MAXN],low[MAXN],st[MAXN],top;
bool f[MAXN];
bool ff;
vector<int> cir;
int sz;
void Tarjan(int now,int fa){
	dfn[now]=low[now]=++sign;
	st[++top]=now;
	for (sit it=s[now].begin();it!=s[now].end();it++){
		int tmp=*it;
		if (tmp==fa) continue;
		if (!dfn[tmp]){
			Tarjan(tmp,now);
			low[now]=min(low[now],low[tmp]);
		}
		else low[now]=min(low[now],dfn[tmp]);
	}
	if (ff) return;
	if (low[now]==dfn[now]){
		sz=0;
		cir.clear();
		while (st[top]!=now){
			cir.push_back(st[top]);
			top--;
			sz++;
		}
		cir.push_back(now);
		top--;sz++;
		if (sz>1){
			int tmp=cir.size();
			for (int i=0;i<tmp;i++)
			  f[cir[i]]=1;
			ff=1;
		}
	}
}
int wh;
int DFS(int now){
	if (wh) return wh;
	ans[++tot]=now;
	vis[now]=1;
	if (f[now]){
		wh=now;return wh;
	}
	for (sit it=s[now].begin();it!=s[now].end();it++){
		if (wh) return wh;
		int tmp=*it;
		if (vis[tmp]) continue;
		DFS(tmp);
	}
	if (wh) return wh;
	return 0;
}
void d(int now){
	if (!vis[now])ans[++tot]=now;
	vis[now]=1;
	for (sit it=s[now].begin();it!=s[now].end();it++){
		int tmp=*it;
		if (vis[tmp]||f[tmp]) continue;
		d(tmp);
	}
}
int mm;
bool ll=0;
void dl(int now){
	vis[now]=1;
	ans[++tot]=now;
	for (sit it=s[now].begin();it!=s[now].end();it++){
		int tmp=*it;
		if (vis[tmp]) continue;
		if (tmp>mm){
			ll=1;break;
		}
		dl(tmp);
	}
	if (ll) d(now);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=Read(),m=Read();
	for (int i=1;i<=m;i++){
		int x=Read(),y=Read();
		s[x].insert(y);s[y].insert(x);
    }
	if (m==n-1){
	dfs(1);
	for (int i=1;i<n;i++) Write(ans[i]),sk();
	Write(ans[n]);ek();
	}  //if it's a tree
	else{
		Tarjan(1,0);
		//for (int i=0;i<cir.size();i++)
		  //cout<<cir[i]<<' ';
		//cout<<endl;
		int bg=DFS(1);
		int a=INF,b=INF,c=INF;
		for (sit it=s[bg].begin();it!=s[bg].end();it++){
			if (!vis[*it]&&!f[*it]){
				a=*it;
				break;
			}
		}
		int vs=cir.size();
		int p;
		for (int i=0;i<vs;i++){
			if (cir[i]==bg){
				p=i;break;
			}
		}
		int l=p-1,r=p+1;
		if (l<0) l=vs-1;
		if (r>=vs) r=0;
		b=cir[l];c=cir[r];
		mm=min(a,min(b,c));
		if (mm==a){
			d(a);
			a=INF;
		}
		mm=min(a,min(b,c));
		if (mm==b){
			mm=min(a,c);
			dl(b);
			dfs(cir[p]);
		}
		else{
			mm=min(b,c);
			dl(c);
			dfs(cir[p]);
		}
		for (int i=1;i<n;i++) Write(ans[i]),sk();
		Write(ans[n]),ek();
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
