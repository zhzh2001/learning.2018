#include<cstdio>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstring>
#define INF (1<<29)
#define LL long long
#define unsigned long long
#define sk() (putchar(' '))
#define ek() (putchar('\n'))
#define Mod 1000000007
using namespace std;
int Read(){
	int X=0,flag=1;char ch=0;
	while (ch<'0'||ch>'9'){
		if (ch=='-') flag=-1;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9')
	  X=(X<<3)+(X<<1)+ch-48,ch=getchar();
	return X*flag;
}
void Write(int x){
	if (x<0) putchar('-'),x=-x;
	if (x>9) Write(x/10);
	putchar(x%10+48);
}
int n,m;
int ans;
int mp[10][10];
bool flag;
int mm;
void d(int x,int y,int num){
	num=num*2+mp[x][y];
	if (!flag) return;
	if (x==n&&y==m){
		if (num<=mm){
			mm=num;
		}
		else flag=0;
		return;
	}
	if (x+1<=n) d(x+1,y,num);
	if (!flag) return;
	if (y+1<=m) d(x,y+1,num);
}
bool check(){
	flag=1;
	mm=INF;
	d(1,1,0);
	return flag;
}
void dfs(int x,int y){
	if (y>m) x++,y=1;
	if (x>n){
		if (check()){
			ans++;
			/*cout<<ans<<":"<<endl;
			for (int i=1;i<=n;i++){
				for (int j=1;j<=m;j++)
				  cout<<mp[i][j]<<' ';
				cout<<endl;
			}*/
		}
		return;
	}
	mp[x][y]=0;
	dfs(x,y+1);
	mp[x][y]=1;
	dfs(x,y+1);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=Read(),m=Read();
	if (n<=4&&m<=4){
		dfs(1,1);
		Write(ans);ek();
	}
	else if (n==1){
		int ans=1;
		for (int i=1;i<=m;i++) ans=((LL)ans*2)%Mod;
		Write(ans);ek();
	}
	else if (n==2){
		int ans=1;
		for (int i=2;i<=m;i++) ans=((LL)ans*3)%Mod;
		ans=(LL)ans*4%Mod;
		Write(ans);ek();
	}
	else if (n==3){
		int ans=1;
		for (int i=4;i<=m;i++) ans=((LL)ans*3)%Mod;
		ans=((LL)ans*112)%Mod;
		Write(ans);ek();
	}
	else if (n==5&&m==5){
		Write(7136);ek();
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

