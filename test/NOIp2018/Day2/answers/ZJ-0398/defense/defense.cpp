#include<cstdio>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstring>
#define INF 1099511627776
#define LL long long
#define unsigned long long
#define sk() (putchar(' '))
#define ek() (putchar('\n'))
#define MAXN 100005
using namespace std;
int Read(){
	int X=0,flag=1;char ch=0;
	while (ch<'0'||ch>'9'){
		if (ch=='-') flag=-1;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9')
	  X=(X<<3)+(X<<1)+ch-48,ch=getchar();
	return X*flag;
}
void Write(LL x){
	if (x<0) putchar('-'),x=-x;
	if (x>9) Write(x/10);
	putchar(x%10+48);
}
int n,m;
char ty;
int op;
int edgenum,Edge[MAXN<<1],nxt[MAXN<<1],head[MAXN];
void addedge(int x,int y){
	Edge[++edgenum]=y;
	nxt[edgenum]=head[x];
	head[x]=edgenum;
}
LL w[MAXN];
int a,b,c,d;
LL dp[MAXN][2];
void dfs(int now,int fa){
	dp[now][1]=w[now];
	if (a==now&&b==0||c==now&&d==0) dp[now][1]=INF;
	if (a==now&&b==1||c==now&&d==1) dp[now][0]=INF;
	for (int i=head[now];i;i=nxt[i]){
		int tmp=Edge[i];
		if (tmp==fa) continue;
		dfs(tmp,now);
		if (dp[now][0]<INF) dp[now][0]+=dp[tmp][1];
		if (dp[now][1]<INF) dp[now][1]+=min(dp[tmp][1],dp[tmp][0]);
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=Read(),m=Read();
	ty=getchar();
	while (ty!='A'&&ty!='B'&&ty!='C') ty=getchar();
	op=Read();
	for (int i=1;i<=n;i++) w[i]=Read();
	for (int i=1;i<n;i++){
		int x=Read(),y=Read();
		addedge(x,y);addedge(y,x);
	}
	for (int i=1;i<=m;i++){
		a=Read(),b=Read(),c=Read(),d=Read();
		memset(dp,0,sizeof(dp));
		dfs(1,0);
		LL ans=min(dp[1][0],dp[1][1]);
		if (ans>=INF) Write(-1);
		else Write(ans);
		ek();
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

