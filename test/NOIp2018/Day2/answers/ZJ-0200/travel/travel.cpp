#include<cstdio>
#include<cmath>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#include<queue>
#define N 6000
using namespace std;
int n,m,u,v,tot,last[N];
bool vis[N];
struct B{
	int x;
	bool operator < (const B b)const{return x>b.x;}
}now;
struct A{
	priority_queue<B> q;
}p[N];
void add_edge(int x,int y){
 	now.x=y;
 	p[x].q.push(now);
}
void search(int now,int fa){
	printf("%d ",now);
	while (!p[now].q.empty()){
		B to=p[now].q.top();
		p[now].q.pop();
		if (to.x!=fa) search(to.x,now);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		add_edge(u,v);
		add_edge(v,u);
	}
	search(1,0);
	return 0;
}
