#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<iostream>
#define N 300100
using namespace std;
int n,m,u,v,a,b,x,y,last[N],tot,cost[N];
long long f[N][2],ans;
char s[5];
struct A{
	int to,pre;
}p[N*2];
void add_edge(int x,int y){
	p[++tot].to=y;
	p[tot].pre=last[x];
	last[x]=tot;
}
void search(int now,int fa){
	f[now][0]=0;
	f[now][1]=cost[now];
	for (int i=last[now];i;i=p[i].pre){
		int to=p[i].to;
		if (to!=fa){
			search(to,now);
			f[now][1]+=min(f[to][1],f[to][0]);
			f[now][0]+=f[to][1];
		}
	}
	if (now==a)
		if (x==1) f[now][0]=1e11;
		else f[now][1]=1e11;
	if (now==b){
		if (y==1) f[now][0]=1e11;
		else f[now][1]=1e11;
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	if (n<=2000&&m<=2000){
		for (int i=1;i<=n;i++) scanf("%d",&cost[i]);
		for (int i=1;i<n;i++){
			scanf("%d%d",&u,&v);
			add_edge(u,v);
			add_edge(v,u);
		} 
		search(1,0);
		for (int i=1;i<=m;i++){
			scanf("%d%d%d%d",&a,&x,&b,&y);
			search(1,0);
			if (a!=1&&b!=1) ans=min(f[1][0],f[1][1]);
			else if (a==1){
					if (x==1) ans=f[1][1];
					else ans=f[1][0];
				}
				else if (b==1){
					if (y==1) ans=f[1][1];
					else ans=f[1][0];
				}
				if (ans<1e11) printf("%lld\n",ans);
				else printf("-1\n");
		}
	}
	return 0;
} 
