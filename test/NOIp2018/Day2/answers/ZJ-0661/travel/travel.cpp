#include<cstdio>
#include<cctype>
#include<algorithm>
#include<vector>
using namespace std;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int getint(){
	char ch=gc(); int res=0,ff=1;
	while(!isdigit(ch)) ch=='-'?ff=-1:0, ch=gc();
	while(isdigit(ch)) res=(res<<3)+(res<<1)+ch-'0', ch=gc();
	return res*ff;
}
const int maxn=5005,maxe=maxn*2;
struct data{ int x,y; } Es[maxe];
int n,m,ans[maxn],b[maxn];
int fir[maxn],nxt[maxe],son[maxe],tot=1;
void add(int x,int y){
	son[++tot]=y; nxt[tot]=fir[x]; fir[x]=tot;
}
bool isban[maxe];
vector<int> V[maxn];
void dfs(int x,int pre){
	b[++b[0]]=x;
	for(int j=0;j<V[x].size();j++)
		if(V[x][j]!=pre) dfs(V[x][j],x);
}
bool Check(){
	for(int i=1;i<=n;i++){
		if(ans[i]<b[i]) return false;
		if(ans[i]>b[i]) return true;
	}
	return false;
}
int fa[maxn];
int getfa(int x){ return fa[x]==x?x:fa[x]=getfa(fa[x]); }
void merge(int x,int y){
	x=getfa(x); y=getfa(y);
	if(x!=y) fa[x]=y;
}
void Solve(){ 
	for(int i=1;i<=n;i++) V[i].clear(), fa[i]=i;
	for(int i=1;i<=n;i++)
		for(int j=fir[i];j;j=nxt[j])
			if(!isban[j>>1]) V[son[j]].push_back(i), merge(son[j],i);
	for(int i=2;i<=n;i++) if(getfa(i)!=getfa(i-1)) return;
	b[0]=0; dfs(1,1);
	if(Check()) for(int i=1;i<=n;i++) ans[i]=b[i];
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=getint(); m=getint();
	for(int i=1;i<=m;i++){
		Es[i].x=getint(), Es[i].y=getint();
		add(Es[i].x,Es[i].y); add(Es[i].y,Es[i].x);
	}
	ans[1]=1e9;
	if(m==n-1) Solve(); else{
		for(int i=1;i<=m;i++)
			isban[i]=true, Solve(), isban[i]=false;
	}
	for(int i=1;i<=n-1;i++) printf("%d ",ans[i]); printf("%d\n",ans[n]);
	return 0;
}
