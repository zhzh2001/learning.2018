#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
typedef long long LL;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int getint(){
	char ch=gc(); int res=0,ff=1;
	while(!isdigit(ch)) ch=='-'?ff=-1:0, ch=gc();
	while(isdigit(ch)) res=(res<<3)+(res<<1)+ch-'0', ch=gc();
	return res*ff;
}
const int maxn=100005, maxe=maxn*2;
int n,m,a[maxn];
int fir[maxn],nxt[maxe],son[maxe],tot;
void add(int x,int y){
	son[++tot]=y; nxt[tot]=fir[x]; fir[x]=tot;
}
LL f[maxn][2],INF=1e13;
bool isban[maxn][2];
void dfsb(int x,int pre){
	f[x][0]=0; f[x][1]=a[x];
	for(int j=fir[x];j;j=nxt[j])
		if(son[j]!=pre){
			dfsb(son[j],x);
			f[x][0]+=f[son[j]][1]; f[x][0]=min(f[x][0],INF);
			f[x][1]+=min(f[son[j]][0],f[son[j]][1]); f[x][1]=min(f[x][1],INF);
		}
	if(isban[x][1]) f[x][1]=INF;
	if(isban[x][0]) f[x][0]=INF;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char st[10]; scanf("%d%d%s",&n,&m,st);
	for(int i=1;i<=n;i++) a[i]=getint();
	for(int i=1;i<=n-1;i++){
		int x=getint(),y=getint();
		add(x,y); add(y,x);
	}
	while(m--){
		int _a=getint(),x=getint()^1,_b=getint(),y=getint()^1;
		isban[_a][x]=true; isban[_b][y]=true;
		for(int i=1;i<=n;i++) f[i][0]=f[i][1]=INF; 
		dfsb(1,1);
		LL res=min(f[1][0],f[1][1]);
		if(res<INF) printf("%lld\n",res); else puts("-1");
		isban[_a][x]=false; isban[_b][y]=false;
	}
	return 0;
}
