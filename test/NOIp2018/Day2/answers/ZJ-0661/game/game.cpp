#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long LL;
const int maxn=1000005,P=1e9+7;
int n,m,sw[maxn][13],ss[maxn][13],tot,len;
bool Cmp(int a[],int b[]){ //a < b
	for(int i=1;i<=len;i++){
		if(a[i]<b[i]) return true;
		if(a[i]>b[i]) return false;
	}
	return false;
}
bool pd;
int b[20][20],noww[20],nows[20];
void dfs(int x,int y,int stp){
	if(!pd) return;
	if(stp==len+1){
		for(int i=1;i<=tot&&pd;i++)
			if(Cmp(noww,sw[i])&&Cmp(nows,ss[i])) pd=false;
		tot++;
		for(int i=1;i<=len;i++)
			sw[tot][i]=noww[i], ss[tot][i]=nows[i]; 
		return;
	}
	if(y<m){
		noww[stp]='R'; nows[stp]=b[x][y+1];
		dfs(x,y+1,stp+1);
	}
	if(x<n){
		noww[stp]='D'; nows[stp]=b[x+1][y];
		dfs(x+1,y,stp+1);
	}

}
void Make(int s){
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			if((i==1&&j==1)||(i==n&&j==m)) continue;
			b[i][j]=s&1; s>>=1;
		}
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==1||m==1){
		LL ans=1;
		for(int i=1;i<=n*m;i++) ans=ans*2%P;
		printf("%lld\n",ans);
		return 0;
	}
	if(n==2){
		LL ans=4;
		for(int i=1;i<=m-1;i++) ans=ans*3%P;
		printf("%lld\n",ans);
		return 0;
	}
	LL ans=0; len=n+m-2;
	for(int s=0;s<=(1<<(n*m-2))-1;s++){
		pd=true; tot=0;
		Make(s);
		dfs(1,1,1);
		if(pd) ans+=4;
	}
	printf("%lld\n",ans);
	return 0;
}
