#include<bits/stdc++.h>
#define ll long long
#define fr(i,x,y) for(int i=(x);i<=(y);i++)
#define rf(i,x,y) for(int i=(x);i>=(y);i--)
using namespace std;
const int N=5001;
const int M=N<<1;
int n,m;
vector<int> a[N];
int d[N],L,b[N];
int p[N],tt,mn[N];
int nox,noy;

void read(int &x){
	char ch=getchar();x=0;
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+(ch^48);
}

void add(int x,int y){
	a[x][0]++;
	a[x].push_back(y);
}

void dfs(int x,int fa){
	p[++tt]=x;
	int siz=a[x][0];
	fr(i,1,siz){
	 	int u=a[x][i];
	 	if (u==fa) continue;
	 	if (x==nox&&u==noy) continue;
	 	if (x==noy&&u==nox) continue;
	 	dfs(u,x);
	}
}

int find_circle(int x,int fa){
	b[x]=1;
	int siz=a[x][0];
	fr(i,1,siz){
		int u=a[x][i];
		if (u==fa) continue;
		if (b[u]){
			d[L]=u;
			d[++L]=x;
			return 1;
		}
		if (find_circle(u,x)){
			d[++L]=x;
			if (d[0]==x) return 0;
			 else return 1;
		}
	}
	return 0;
}

int pd(){
	fr(i,1,n)
	 if (p[i]!=mn[i]) return p[i]<mn[i];
	return 0;
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);read(m);
	int x,y;
	fr(i,1,n) a[i].push_back(0);
	fr(i,1,m){
		read(x);read(y);
		add(x,y);add(y,x);
	}
	//predfs(1,0);
	fr(i,1,n) sort(a[i].begin()+1,a[i].end());
	if (m==n-1){
		dfs(1,0);
		fr(i,1,n) printf("%d ",p[i]);
	}else{
		find_circle(1,0);
		mn[1]=n+1;
		fr(i,1,L){
			nox=d[i-1];noy=d[i];
			tt=0;
			dfs(1,0);
			if (pd()) fr(j,1,n) mn[j]=p[j];
		}
		fr(i,1,n) printf("%d ",mn[i]);
	}
	return 0;
}
