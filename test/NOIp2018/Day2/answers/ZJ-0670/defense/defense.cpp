#include<bits/stdc++.h>
#define ll long long
#define fr(i,x,y) for(int i=(x);i<=(y);i++)
#define rf(i,x,y) for(int i=(x);i>=(y);i--)
using namespace std;
const int N=100002;
const int M=N<<1;
int n,m;
char tp[5];
int a[N];
ll f[N][2],g[N][2];
ll h[N][2];

void read(int &x){
	char ch=getchar();x=0;
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+(ch^48);
}

namespace caseA23{
	void solve(){
		fr(i,1,n) read(a[i]);
		memset(f,0x3f,sizeof f);
		memset(g,0x3f,sizeof g);
		f[0][1]=0;g[n+1][1]=0;
		fr(i,1,n){
			f[i][1]=min(f[i-1][0],f[i-1][1])+a[i];
			f[i][0]=f[i-1][1];
		}
		rf(i,n,1){
			g[i][1]=min(g[i+1][0],g[i+1][1])+a[i];
			g[i][0]=g[i+1][1];
		}
		int c,d,x,y;
		fr(i,2,n) read(x),read(y);
		fr(i,1,m){
			read(c);read(x);read(d);read(y);
			if (c>d) swap(c,d),swap(x,y);
			if (d-c==1){
				if (y==0&&x==0) printf("-1\n");
				 else printf("%lld\n",f[c][x]+g[d][y]);
			}else{
				h[c][0]=h[c][1]=1e18;
				if (x) h[c][1]=a[c];
				 else h[c][0]=0;
				fr(j,c+1,d){
					h[j][1]=min(h[j-1][0],h[j-1][1])+a[j];
					h[j][0]=h[j-1][1];
				}
				printf("%lld\n",h[d][y]);
			}
		}
	}
}

namespace caseA1{
	void solve(){
		memset(f,0x3f,sizeof f);
		memset(g,0x3f,sizeof g);
		fr(i,1,n) read(a[i]);
		f[1][1]=a[1];g[n+1][1]=0;
		fr(i,2,n){
			f[i][1]=min(f[i-1][0],f[i-1][1])+a[i];
			f[i][0]=f[i-1][1];
		}
		rf(i,n,1){
			g[i][1]=min(g[i+1][0],g[i+1][1])+a[i];
			g[i][0]=g[i+1][1];
		}
		int c,d,x,y;
		fr(i,2,n) read(x),read(y);
		fr(i,1,m){
			read(c);read(x);read(d);read(y);
			ll ans=f[d][y];
			if (y) ans+=min(g[d+1][0],g[d+1][1]);
			 else ans+=g[d+1][1];
			printf("%lld\n",ans);
		}
	}
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n);read(m);
	scanf("%s",tp);
	if (tp[0]=='A'&&tp[1]!='1') caseA23::solve();
	if (tp[0]=='A'&&tp[1]=='1') caseA1::solve();
	return 0;
}

/*
5 3 A2
1 3 2 4 3
1 2 2 3 3 4 4 5
2 0 3 0
2 1 3 0
2 1 3 1
*/
