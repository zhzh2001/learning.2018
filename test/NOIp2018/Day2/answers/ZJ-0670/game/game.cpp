#include<bits/stdc++.h>
#define ll long long
#define fr(i,x,y) for(int i=(x);i<=(y);i++)
#define rf(i,x,y) for(int i=(x);i>=(y);i--)
#define frl(i,x,y) for(int i=(x);i<(y);i++)
using namespace std;
const int N=1000001;
const int M=9;
const int p=1e9+7;
int n,m;
int v[N],tot;
int a[M][M];

void read(int &x){
	char ch=getchar();x=0;
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+(ch^48);
}

void Add(int &x,int y){
	x+=y;
	while(x>=p) x-=p;
}

ll qpow(ll a,int n){
	ll ans=1;
	for(ll sum=a;n;n>>=1,sum=sum*sum%p) if (n&1) ans=ans*sum%p;
	return ans;
}

int pd(){
	fr(i,2,tot)
	 if (v[i]>=v[i-1]) 233;
	  else return 0;
	return 1;
}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n);read(m);
	if (n==1) printf("%lld\n",qpow(2,m));
	 else if (n==2) printf("%lld\n",4*qpow(3,n-1)%p);
	  else{
	  	int all=(1<<(n*m))-1,ans=0,s=0;
	  	fr(o,0,all){
	  		frl(i,0,n*m)
	  		 if (o&(1<<i)) a[i/m+1][i%m+1]=1;
	  		  else a[i/m+1][i%m+1]=0;
	  		int z=n+m-2;tot=0;
	  		memset(v,0,sizeof v);
	  		frl(i,0,1<<z){
	  			int s=0;
	  			frl(j,0,z) if (i&(1<<j)) s++;
	  			if (s!=n-1) continue;
	  			tot++;
	  			int x=1,y=1;
	  			frl(j,0,z){
	  				if (i&(1<<j)) x++;
	  				 else y++;
	  				v[tot]=(v[tot]<<1)|a[x][y];
	  			}
	  			//assert(x==n&&y==m);
	  		}
	  		assert(tot==6);
	  		if (pd()) ans++;
	  		 else s++;
	  	}
	  	cout<<ans<<endl;
	  }
	return 0;
}
