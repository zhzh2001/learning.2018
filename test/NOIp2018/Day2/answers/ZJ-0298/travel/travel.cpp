#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<queue>
#define il inline
using namespace std;
const int N=5005;
il int read(){
	int x=0,f=1;char c=getchar();
	for(;!isdigit(c);c=getchar()) if(c=='-') f=-1;
	for(;isdigit(c);c=getchar()) x=(x+(x<<2)<<1)+c-48;
	return x*f;
}
int n,m,k,fl,d[N],dp[N],v[N],u[N],ans[N],g[N][N],g1[N][N],c[N],ct;
il void dfs(int x){
	ans[++k]=x;v[x]=1;
	for(int i=1;i<=n;++i) if(!v[i]){
		if(g[x][i]) dfs(i);
		else if(fl&&g1[x][i]) fl=0,dfs(i);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1,x,y;i<=m;++i)
	x=read(),y=read(),++d[x],++d[y],g[x][y]=g[y][x]=1;
	queue<int> q;q.push(1);u[1]=1;
	while(!q.empty()){
		int x=q.front();q.pop();
		for(int i=1;i<=n;++i) if(g[x][i])
		if(!u[i]) dp[i]=dp[x]+1,q.push(i),u[i]=1;
	}
	if(m==n){
		fl=1;
		memset(u,0,sizeof(u));
		for(int i=1;i<=n;++i) if(d[i]==1) q.push(i),u[i]=1;
		while(!q.empty()){
			int x=q.front();q.pop();
			for(int i=1;i<=n;++i) if(g[x][i])
			if(!u[i]) if(--d[i]==0) q.push(i),u[i]=1;
		}
		for(int i=1;i<=n;++i) if(!u[i]) c[++ct]=i;
		int pos=c[1];
		for(int i=2;i<=ct;++i) if(dp[pos]>dp[c[i]]) pos=c[i];
		for(int i=1;i<=ct;++i) if(dp[c[i]]==dp[pos]+1){
			for(int j=1;j<=ct;++j)
			if(!g[c[j]][c[i]]) g1[c[j]][c[i]]=1;
		}
	}
	dfs(1);
	for(int i=1;i<=n;++i) printf("%d ",ans[i]);
	return 0;
}
