#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#define il inline
using namespace std;
typedef long long ll;
const int mo=1e9+7;
il ll qpow(ll a,ll b){ll res=1;for(;b;a=a*a%mo,b>>=1) if(b&1) res=res*a%mo;return res;}
int n,m;ll ans=1;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);if(n>m) swap(n,m);
	for(int i=2;i<=n;++i) ans=ans*i%mo*i%mo;
	ans=ans*qpow(n+1,m-n+1)%mo;
	if(n==3) ans=(ans-16*(m-1)%mo*(m-2)%mo+mo)%mo;
	printf("%lld",ans);
	return 0;
}
