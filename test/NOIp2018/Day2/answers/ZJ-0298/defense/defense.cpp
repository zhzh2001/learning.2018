#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#define il inline
using namespace std;
typedef long long ll;
const int N=1e5+5;
#define getchar() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<20,stdin),p1==p2)?EOF:*p1++)
char buf[1<<21],*p1=buf,*p2=buf;
il int read(){
	int x=0,f=1;char c=getchar();
	for(;!isdigit(c);c=getchar()) if(c=='-') f=-1;
	for(;isdigit(c);c=getchar()) x=(x+(x<<2)<<1)+c-48;
	return x*f;
}
il int cread(){
	char c=getchar();
	for(;!isalpha(c);c=getchar());
	return c-64;
}
int n,m,o1,o2,k,h[N],p[N],l1,l2,l3,l4;ll f[N][2],ans;
struct Edge{int to,nxt;}a[N<<1];
il void add(int x,int y){a[++k].to=y,a[k].nxt=h[x],h[x]=k;}
il void dfs(int x,int fa){
	f[x][0]=0;f[x][1]=p[x];
	for(int i=h[x];i;i=a[i].nxt)
	if(a[i].to^fa) dfs(a[i].to,x);
	for(int i=h[x];i;i=a[i].nxt){
		int y=a[i].to;
		if(y==fa) continue;
		f[x][0]+=f[y][1];
		f[x][1]+=min(f[y][1],f[y][0]);
	}
	if(x==l1&&l2==0) f[x][1]=1e15;
	if(x==l3&&l4==0) f[x][1]=1e15;
	if(x==l1&&l2==1) f[x][0]=f[x][1];
	if(x==l3&&l4==1) f[x][0]=f[x][1];
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();o1=cread(),o2=read();
	for(int i=1;i<=n;++i) p[i]=read();
	for(int i=1,x,y;i<n;++i) x=read(),y=read(),add(x,y),add(y,x);
	while(m--){
		l1=read(),l2=read(),l3=read(),l4=read();
		dfs(1,0);
		ans=min(f[1][0],f[1][1]);
		if(ans>=1e15) puts("-1");
		else printf("%lld\n",ans);
	}
	return 0;
}
