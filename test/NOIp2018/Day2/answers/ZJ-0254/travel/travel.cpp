#include<bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0,f=0;char ch=getchar();
  for(;ch<'0'||ch>'9';ch=getchar())
    if(ch=='-') f=1;
  for(;ch>='0'&&ch<='9';ch=getchar())
    x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const int N = 5005;
int n, m;
vector <int> v[N];
int vis[N], dep[N], fx, fy, fa[N], cnt;
namespace tree {
  inline void Dfs(int x, int fa) {
    vis[x] = 1;
    cnt++;
    if (cnt != n) printf("%d ",x);
    else printf("%d", x);
    int m = v[x].size();
    for (int i = 0; i < m; i++)
      if (v[x][i] != fa&&!vis[v[x][i]]) Dfs(v[x][i], x);
  }
}
inline void dfs(int x) {
  vis[x] = 1;
  int m = v[x].size();
  for (int i = 0; i < m; i++) {
    int y = v[x][i];
    if (y == fa[x]) continue; 
    if (vis[y]) {
      fx = x; fy = y;
      continue;
    }
    dep[y] = dep[x]+1;
    fa[y] = x;
    dfs(y);
  }
}
int a[N], tx, ty;
namespace yy{
  int vis[N];
  inline void Dfs(int x) {
    cnt++;
    vis[x] = 1; 
    if (cnt != n) printf("%d ", x);
    else printf("%d", x);
    int m = v[x].size();
    for (int i = 0; i < m; i++)
      if (!vis[v[x][i]]) {
        if (x == tx && v[x][i] == ty) continue;
        Dfs(v[x][i]);
      }
  }
}
inline void solve() {
  if (dep[fx] < dep[fy]) swap(fx, fy);
  int tnt = fx;
//  cout << dep[fx] << " "<<dep[fy] <<endl;
//  cout << fx <<" "<< fy <<endl;
  while (dep[fx] != dep[fy]) {
    fx = fa[fx];
    a[++a[0]] = fx;
  }
  fx = tnt;
  for (int i = a[0]-1; i; i--)
    if (fx < a[i]) {
      tx = a[i+1];
      ty = a[i];
      break;
    }
//  for (int i = 1; i <= a[0]; i++) cout << a[i] <<" ";
//  puts("");
//  cout << tx <<" "<< ty << endl;
  if (!tx) {
    using namespace tree;
    memset(vis, 0, sizeof vis);
    Dfs(1, 0);
    return;
  } else {
    using namespace yy;
    Dfs(1);
  }
}
int main() {
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  n = read(); m = read();
  for (int i = 1; i <= m; i++) {
    int x = read(), y = read();
    v[x].push_back(y);
    v[y].push_back(x);
  } 
  for (int i = 1; i <= m; i++)
    sort(v[i].begin(), v[i].end());
  if (m == n-1) {
    using namespace tree;
    Dfs(1, 0);
    return 0;
  }
  dfs(1);
  solve();
	return 0;
}
