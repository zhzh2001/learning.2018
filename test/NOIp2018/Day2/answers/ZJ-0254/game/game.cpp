#include<bits/stdc++.h>
using namespace std;
const int mod = 1e9+7;
int n, m, ans;
namespace baoli29 {
  int a[5][5], p[30];
  inline void Dfs(int x, int y, int sum) {
    if (x == n && y == m) {
      p[++p[0]] = sum;
      return;
    }
    if (y < m) Dfs(x, y+1, sum*2+a[x][y+1]);
    if (x < n) Dfs(x+1, y, sum*2+a[x+1][y]);
  }
  inline void dfs(int x, int y) {
    if (y == m+1) x++, y = 1;
    if (x == n+1) {
      p[0] = 0;
      Dfs(1, 1, a[1][1]);
      int f = 1;
      for (int i = 2; i <= p[0]; i++)
        if (p[i] < p[i-1]) {f = 0; break;}
      if (f) ans++;
      return;
    }
    a[x][y] = 0;
    if (y == m || x == 1 || a[x][y] == a[x-1][y+1]) dfs(x, y+1);
    a[x][y] = 1;
    dfs(x, y+1);
  }
}
inline int ksm(int x, int y) {
  int sum = 1;
  while (y) {
    if (y&1) sum = 1ll*sum*x%mod;
    y >>= 1;
    x = 1ll*x*x%mod;
  }
  return sum;
}
int main() {
  freopen("game.in","r",stdin);
  freopen("game.out","w",stdout);
  scanf("%d%d", &n, &m);
  if (n == 2) {
    ans = 4ll*ksm(3, m-1)%mod;
    printf("%d\n", ans);
  }else if (n <= 8 &&m <= 8) {
    using namespace baoli29;
    dfs(1, 1);
    printf("%d\n", ans);
  }else {
//    for (int i = 2; i <= n; i++)
  }
  return 0;
}
