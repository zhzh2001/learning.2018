#include<bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0,f=0;char ch=getchar();
  for(;ch<'0'||ch>'9';ch=getchar())
    if(ch=='-') f=1;
  for(;ch>='0'&&ch<='9';ch=getchar())
    x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const int N = 100005;
int head[N], cnt;
struct edge {
  int nxt, to;
}e[N*2];
inline void insert(int u, int v) {
  e[++cnt] = (edge) {head[u], v}; head[u] = cnt;
  e[++cnt] = (edge) {head[v], u}; head[v] = cnt;
}
const long long oo = 100000000000ll;
int n, m, X, xz, Y, yz, a[N], T, Fa[N];
long long f[N][2];
inline void dfs(int x, int fa) {
  f[x][0] = 0;
  f[x][1] = a[x];
  if (x == X) f[x][xz^1] = oo;
  if (x == Y) f[x][yz^1] = oo;
  for (int i = head[x]; i; i = e[i].nxt)
    if (e[i].to != fa) {
      if (T == 1) Fa[e[i].to] = x;
      dfs(e[i].to, x);
      f[x][1] += min(f[e[i].to][0], f[e[i].to][1]);
      f[x][0] += f[e[i].to][1];
    }
//  cout << x<<" "<<f[x][0] <<" "<<f[x][1]<<endl;
}
namespace papapa {
  inline void pa(int x, int y) {
    long long t0, t1, t2, t3;
    int X = x;
    t2 = f[x][0], t3 = f[x][1];
    while (x != 1) {
      t0 = f[Fa[x]][0]-f[x][1];
      t1 = f[Fa[x]][1]-min(f[x][0], f[x][1]);
      if (X == x) {
        if (y == 0) t3 = oo;
        else t2 = oo;
      }
      t0 += t3;
      t1 += min(t2, t3);
      t2 = t0;
      t3 = t1;
      x = Fa[x];
    }
    printf("%lld\n", t1);
  }
}
int main() {
//  freopen("defense.in","r",stdin);
//  freopen("defense.out","w",stdout);
  n = read(); m = read();
  char type[5]; scanf("%s",type);
  for (int i = 1; i <= n; i++) a[i] = read();
  for (int i = 1; i < n; i++) {
    int x = read(), y = read();
    insert(x, y);
  }
  if (n <= 3000 && m <= 3000) {
    for (int i = 1; i <= m; i++) {
      X = read(), xz = read();
      Y = read(), yz = read();
      dfs(1, 0);
      if (min(f[1][0], f[1][1]) >= oo) puts("-1");
      else printf("%lld\n", min(f[1][0], f[1][1]));
    }
  }else if (type[0] == 'B') {
     T = 1;dfs(1, 0);
    using namespace papapa;
    for (int i = 1; i <= m; i++) {
      X = read(), xz = read();
      Y = read(), yz = read();  
      pa(Y, yz);
    }
  }
  return 0;
}
