#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;

int n,m,cnt,a,x,b,y;
char ch,pd1,pd2;
int pdd[100005],head[500005];
long long p[100005];
long long f[100005][2];
struct node{
	int to,nxt;
}g[500005];
void addedge(int x,int y)
{
	g[++cnt]=(node){y,head[x]};
	head[x]=cnt;
}
void dfs(int u,int fa)
{
	if (pdd[u]==1) 
		f[u][0]=0;
	if (pdd[u]==2)
		f[u][1]=0;
	if (pdd[u]==0) f[u][0]=0,f[u][1]=0;
	long long sum1=0,sum2=0x3f3f3f3f,pd=0,sum3=0,pd2=0;
	for (int i=head[u];i>0;i=g[i].nxt)
	{
		int v=g[i].to;
		if (v==fa) continue;
		pd2=1;
		dfs(v,u);
		if (f[v][0]<f[v][1]) sum1+=f[v][0];else
		{
			sum1+=f[v][1];
			pd=1;
		}
		if (f[v][1]<sum2)
		{
			sum2=f[v][1];
			sum3=f[v][0];
		}
	}
	if (pd2==0) sum2=0;
	if (f[u][0]==0) 
	{
		if (pd==1)
		f[u][0]=sum1;else f[u][0]=sum1-sum3+sum2;
	}
	if (f[u][1]==0) f[u][1]=sum1+p[u];
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d %d",&n,&m);
	ch=getchar();
	while (ch!='A'  && ch!='B' && ch!='C') ch=getchar();
	pd1=ch;
	while (ch!='1' && ch!='2' && ch!='3') ch=getchar();
	pd2=ch;
	for (int i=1;i<=n;i++)
	scanf("%d",&p[i]);
	for (int i=1;i<=n-1;i++)
	{
		scanf("%d %d",&x,&y);
		addedge(x,y);
		addedge(y,x);
	}
	for (int i=1;i<=m;i++)
	{
		memset(f,0x3f,sizeof(f));
		scanf("%d %d %d %d",&a,&x,&b,&y);
		if (a==b && x!=y)
		{
			printf("-1\n");
			continue;
		}
		pdd[a]=x+1;pdd[b]=y+1;
		dfs(1,0);
		pdd[a]=0;pdd[b]=0;
		long long ans=min((long long)f[1][0],(long long)f[1][1]);
		if (ans>=0x3f3f3f3f) printf("-1\n");else printf("%lld\n",ans);
	}
	return 0;
}

