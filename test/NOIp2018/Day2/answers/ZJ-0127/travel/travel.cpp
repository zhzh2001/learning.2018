#include <cstdio>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int n,m,x,y,cnt,pd,tot;
vector <int> p[5005];
int vis[500005],head[500005];
struct node{
	int to,nxt;
}g[500005];
void addedge(int x,int y)
{
	g[++cnt]=(node){y,head[x]};
	head[x]=cnt;
}
void dfs(int u,int fa)
{
	vis[u]=1;
	if (pd==0) 
	{
		printf("%d",u);
		pd=1;
	}else printf(" %d",u);
	for (int i=head[u];i>0;i=g[i].nxt)
	{
		int v=g[i].to;
		if (vis[v]==1) continue;
		p[u].push_back(v);
	}
	sort(p[u].begin(),p[u].end());
	for (int i=0;i<p[u].size();i++)
	if (vis[p[u][i]]==0)
	dfs(p[u][i],u);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d %d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		scanf("%d %d",&x,&y);
		addedge(x,y);
		addedge(y,x);
	}
	dfs(1,0);
	return 0;
}
