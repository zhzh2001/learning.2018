#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int read(){
	int x=0;char c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
	return x;
}
const int N=5e3+3;
struct E{
	int v,l;
}edge[N<<1|1];
int n,m,cnt,head[N],k[N][N];
bool b[N];
inline void add(int u,int v){
	edge[cnt].v=v;
	edge[cnt].l=head[u];
	head[u]=cnt++;
}
bool cmp(int a,int b){return a<b;}
void dfs_pf1(int u){
	printf("%d ",u);b[u]=1;
	for(int i=head[u];~i;i=edge[i].l){
		int v=edge[i].v;
		if(!b[v])k[u][++k[u][0]]=v;
	}
	sort(k[u]+1,k[u]+1+k[u][0],cmp);
	for(int i=1;i<=k[u][0];i++)dfs_pf1(k[u][i]);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(head,-1,sizeof(head));
	n=read();m=read();
	while(m--){
		int u=read(),v=read();
		add(u,v);add(v,u);
	}
	dfs_pf1(1);
	return 0;
}
