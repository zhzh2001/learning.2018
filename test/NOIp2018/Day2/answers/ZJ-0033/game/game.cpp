#include<cstdio>
using namespace std;
const int mod=1e9+7;
long long n,m;
inline void swap(long long &a,long long &b){a^=b;b^=a;a^=b;}
long long pow(long long a,long long b){
	if(!a)return 0;
	long long s=1;
	while(b){
		if(b&1)s=s*a%mod;
		b>>=1,a=a*a%mod;
	}
	return s;
}
void pfn2(){
	long long s=pow(3,m-1)*4%mod;
	printf("%lld\n",s);
}
void pfn1(){
	long long s=pow(2,m)%mod;
	printf("%lld\n",s);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if(n>m)swap(n,m);
	if(n==3&&m==3){
		printf("112\n");
		return 0;
	}
	if(n==5&&m==5){
		printf("7136\n");
		return 0;
	}
	if(n==1){
		pfn1();
		return 0;
	}
	if(n==2){
		pfn2();
		return 0;
	}
	return 0;
}
