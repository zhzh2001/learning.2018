#include<cstdio>
#include<cstring>
using namespace std;
int read(){
	int x=0;char c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
	return x;
}
inline long long min(long long a,long long b){return a<b?a:b;}
const int N=1e5+5;
int n,m,p[N],cnt,head[N],ti[N];
struct Tr{
	int v,l;
}edge[N<<1|1];
inline void add(int u,int v){
	edge[cnt].v=v;
	edge[cnt].l=head[u];
	head[u]=cnt++;
}
long long f[N][2];
bool flag,b[N];
void dfs(int u){
	b[u]=1;f[u][1]=p[u];
	if(!ti[u]){
		for(int i=head[u];~i;i=edge[i].l){
			int v=edge[i].v;
			if(!ti[v]){flag=0;return;}
			if(b[v])continue;
			f[v][0]=-1;
			dfs(v);f[u][0]+=f[v][1];
		}
		return;
	}
	for(int i=head[u];~i;i=edge[i].l){
		int v=edge[i].v;
		if(b[v])continue;
		dfs(v);if(!flag)return;
		if(!ti[v]){
			f[u][1]+=f[v][0];
			f[u][0]=-1;
		}
		else if(f[v][0]==-1)f[u][1]+=f[v][1];
		else f[u][1]+=min(f[v][1],f[v][0]);
		if(f[u][0]!=-1)f[u][0]+=f[v][1];
	}
	if(ti[u]==1)f[u][0]=-1;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char pd[5];
	memset(head,-1,sizeof(head));
	n=read();m=read();scanf("%s",pd);
	for(int i=1;i<=n;i++)p[i]=read();
	for(int i=1;i<n;i++){
		int u=read(),v=read();
		add(u,v);add(v,u);
	}
	memset(ti,-1,sizeof(ti));
	while(m--){
		int a=read(),x=read(),b0=read(),y=read();
		ti[a]=x,ti[b0]=y;
		flag=1;
		memset(f,0,sizeof(f));
		memset(b,0,sizeof(b));
		dfs(1);
		if(flag){
			if(ti[1]!=-1)printf("%lld\n",f[1][ti[1]]);
			else if(f[1][0]==-1)printf("%lld\n",f[1][1]);
			else printf("%lld\n",min(f[1][0],f[1][1]));
		}
		else printf("-1\n");
		ti[a]=-1,ti[b0]=-1;
	}
	return 0;
}
