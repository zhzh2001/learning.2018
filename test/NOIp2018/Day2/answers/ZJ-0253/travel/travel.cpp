#include<cstdio>
#include<vector>
#include<algorithm>
#define N 5010
using namespace std;
int n,m,k,t,a[N],b[N],fir[N],nex[N<<1],nod[N<<1],vis[N];
vector<int> vec[N];
void add(int x,int y){nod[++k]=y,nex[k]=fir[x],fir[x]=k;}
void DFS(int u,int f,int x){
	if(vis[u]) return;
	vis[u]=1,x?b[++t]=u:a[++t]=u;
	for(int i=fir[u];i;i=nex[i])
		if((i>>1)!=x&&nod[i]!=f) vec[u].push_back(nod[i]);
	sort(vec[u].begin(),vec[u].end());
	for(int i=0,_=vec[u].size();i<_;i++) DFS(vec[u][i],u,x);
	vec[u].clear(),vis[u]=0;
}
void chec(){
	if(t<n) return;
	for(int i=1;i<=n;i++)
		if(b[i]<a[i]) break;else if(b[i]>a[i]) return;
	for(int i=1;i<=n;i++) a[i]=b[i];
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m),k=1,a[1]=n;
	for(int i=1,x,y;i<=m;i++)
		scanf("%d%d",&x,&y),add(x,y),add(y,x);
	if(m==n-1) DFS(1,0,0);
	else for(int i=1;i<=m;i++) t=0,DFS(1,0,i),chec();
	for(int i=1;i<=n;i++) printf("%d ",a[i]);
	return 0;
}
