#include<cstdio>
#define M 1000000007
const int f[9]={0,2,12,112,912,7136,56768,453504,0};
int n,m;
int quic(int y){
	int ret=1,x=3;
	while(y) y&1?ret=1ll*ret*x%M:0,x=1ll*x*x%M,y>>=1;
	return ret;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n>m){int t=n;n=m,m=t;}
	printf("%lld",1ll*f[n]*quic(m-n)%M);
	return 0;
}
