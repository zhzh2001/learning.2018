#include<cstdio>
#include<algorithm>
#define N 100010
#define INF 1000000000000ll
using namespace std;
int n,m,k,u1,u2,x1,x2,o,a[N],fir[N],nex[N<<1],nod[N<<1];
long long _,f[N][2],g[N][2];
int read(){
	int ret=0,c=getchar();
	while(c<48||57<c) c=getchar();
	while(47<c&&c<58) ret=ret*10+c-48,c=getchar();
	return ret;
}
void add(int x,int y){nod[++k]=y,nex[k]=fir[x],fir[x]=k;}
void DFS(int u,int fa){
	f[u][0]=0,f[u][1]=a[u];
	for(int i=fir[u],v;i;i=nex[i]) if((v=nod[i])!=fa)
		DFS(v,u),f[u][0]=min(f[u][0]+f[v][1],INF),
		f[u][1]=min(f[u][1]+min(f[v][0],f[v][1]),INF);
	if(u==u1) f[u][!x1]=INF;
	if(u==u2) f[u][!x2]=INF;
}
void DFS1(int u,int fa){
	f[u][0]=0,f[u][1]=a[u];
	if(u==1){
		f[u][0]=INF;
		for(int i=fir[u],v;i;i=nex[i])
			DFS1(v=nod[i],u),f[u][1]+=min(f[v][0],f[v][1]);
	}
	else for(int i=fir[u],v;i;i=nex[i]) if((v=nod[i])!=fa)
		DFS1(v,u),f[u][0]+=f[v][1],f[u][1]+=min(f[v][0],f[v][1]);
}
void DFS2(int u,int fa){
	if(u==1) g[u][0]=INF,g[u][1]=0;
	else{
		long long xx=f[fa][0]-f[u][1],yy=f[fa][1]-min(f[u][0],f[u][1]);
		g[u][0]=g[fa][1]+yy,g[u][1]=min(g[fa][0]+xx,g[fa][1]+yy);
	}
	for(int i=fir[u],v;i;i=nex[i]) if((v=nod[i])!=fa) DFS2(v,u);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),o=read();
	for(int i=1;i<=n;i++) a[i]=read();
	for(int i=1,x,y;i<n;i++) x=read(),y=read(),add(x,y),add(y,x);
	if(n<=2000&&m<=2000){
		for(int i=1;i<=m;i++)
			u1=read(),x1=read(),u2=read(),x2=read(),
			DFS(1,0),_=min(f[1][0],f[1][1]),printf("%lld\n",_<INF?_:-1);
	}
	else if(o==1){
		DFS1(1,0),DFS2(1,0);
		for(int i=1;i<=m;i++)
			read(),read(),u1=read(),x1=read(),
			printf("%lld\n",f[u1][x1]+g[u1][x1]);
	}
	else{
		for(int i=1;i<=n;i++)
			f[i][0]=f[i-1][1],f[i][1]=a[i]+min(f[i-1][0],f[i-1][1]);
		for(int i=n;i;i--)
			g[i][0]=g[i+1][1],g[i][1]=a[i]+min(g[i+1][0],g[i+1][1]);
		for(int i=1,_;i<=m;i++){
			u1=read(),x1=read(),u2=read(),x2=read();
			if(u1>u2) _=u1,u1=u2,u2=_,_=x1,x1=x2,x2=_;
			if(!x1&&!x2) puts("-1");
			else printf("%lld\n",f[u1][x1]+g[u2][x2]);
		}
	}
	return 0;
}
