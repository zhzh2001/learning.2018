// game
#include <cstdio>
#include <cstring>
#include <algorithm>

#ifdef DEBUG_MD
	#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#else
	#define debug(format, ...) 0
#endif

const int MAXS = (1 << 8) + 10;
const int MOD = 1e9 + 7;

int N, M, DP[2][MAXS];

inline void add(int &x, int d) { (x += d) >= MOD ? x -= MOD : 0; }

int main() {
#ifndef LOCAL 
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
#endif
	scanf("%d%d", &N, &M);
	int mlmt = 1 << N, *pre = DP[0], *nxt = DP[1];
	for (int mask = 0; mask < mlmt; ++mask) pre[mask] = 1;
	for (int i = 1; i < M; i++) {
		memset(nxt, 0, sizeof(int) * (mlmt + 5));
		debug("i=%d\n", i);
		for (int mask = 0; mask < mlmt; ++mask) {
			int s0 = 0;
			for (int j = 0; j + 1 < N; j++)
				if (mask >> j & 1) s0 |= 1 << j + 1;
			debug("  mask=%d, s0=%d\n", mask, s0);
			add(nxt[mask], pre[s0]);
			debug("  pre(%d) -> nxt(%d)\n", s0, mask);
			for (int tlmt = (mlmt - 1) ^ s0, t0 = tlmt; t0; t0 = t0 - 1 & tlmt) {
				add(nxt[mask], pre[t0 | s0]);
				debug("  pre(%d) -> nxt(%d)\n", t0 | s0, mask);
			}
		}
		std::swap(nxt, pre);
	}

	int ans = 0;
	for (int mask = 0; mask < mlmt; ++mask) add(ans, pre[mask]);

	printf("%d\n", ans);
#ifndef LOCAL
	fclose(stdin); fclose(stdout);
#endif
	return 0;
}
