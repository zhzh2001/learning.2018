// travel
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

#ifdef DEBUG_MD
	#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#else
	#define debug(format, ...) 0
#endif
#define pb push_back
#define forto(_) for (std::vector<int>::iterator v = G[_].begin(); v != G[_].end(); ++v)

const int MAXN = 5010;

typedef int Arr[MAXN];

int N, M, tota, totlp, inlp, to1, to2;
Arr vis, Ans, lp, on_lp, fa;
std::vector<int> G[MAXN];

void dfs1(int u);
void dfs2(int u);
bool find_lp(int u);
bool dfs_lp(int u, int md = 0);

int main() {
#ifndef LOCAL
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
#endif
	scanf("%d%d", &N, &M);
	for (int i = 0, x, y; i < M; i++) {
		scanf("%d%d", &x, &y);
		G[x].pb(y), G[y].pb(x);
	}

	for (int i = 1; i <= N; i++) std::sort(G[i].begin(), G[i].end());
	if (M + 1 == N) {
		dfs1(1);
		for (int i = 0; i < tota; i++) printf("%d ", Ans[i]);
	}
	else {
		fa[1] = 0;
		find_lp(1);
		debug("find_lp: inlp=%d\n", inlp);
#ifdef DEBUG_MD
		for (int i = 1; i <= N; i++) debug(" %d on_lp? %d\n", i, on_lp[i]);
#endif

		memset(vis, 0, sizeof vis);
		to1 = -1, to2 = 10000000;
		lp[totlp++] = inlp, vis[inlp] = 1;
		debug(" fa[inlp]=%d\n", fa[inlp]);
		forto(inlp) {
			if (*v == fa[inlp]) continue;
			if (to1 == -1) to1 = *v;
			else {
				to2 = *v;
				debug(" to1=%d, to2=%d\n", to1, to2);
				dfs_lp(to1);
				to1 = to2, to2 = 10000000;
			}
		}
		if (!vis[to1]) dfs_lp(to1);
		dfs2(1);

		debug(" vis[89]? %d\n", vis[89]);
		for (int i = 0; i < tota; i++) {
			if (Ans[i] == inlp) {
				for (int j = 0; j < totlp; j++) printf("%d ", lp[j]);
				continue;
			}
			printf("%d ", Ans[i]);
		}
	}

	putchar('\n');
#ifndef LOCAL
	fclose(stdin); fclose(stdout);
#endif
	return 0;
}

void dfs1(int u) {
	vis[u] = 1, Ans[tota++] = u;
	forto(u) {
		if (vis[*v]) continue;
		dfs1(*v);
	}
}

bool find_lp(int u) {
	debug(" find loop: u: %d, fa: %d\n", u, fa[u]);
	vis[u] = 1;
	forto(u) {
		if (*v == fa[u]) continue;
		if (vis[*v]) {
			inlp = *v;
			for (int uu = u; ; uu = fa[uu]) {
				on_lp[uu] = 1;
				if (uu == *v) break;
			}
			return true;
		}
		fa[*v] = u;
		if (find_lp(*v)) return true;
	}
	return false;
}

bool dfs_lp(int u, int md) {
	vis[u] = 1, lp[totlp++] = u;
	if (md) {
		forto(u) {
			if (vis[*v] || on_lp[*v]) continue;
			dfs_lp(*v, 1);
		}
		return false;
	}
	forto(u) {
		if (vis[*v]) continue;
		if (*v > to2 || !dfs_lp(*v)) {
			for (int lst = on_lp[*v] ? *v : -1; v != G[u].end(); ++v) {
				if (vis[*v]) continue;
				if (!on_lp[*v]) {
					if (~lst) dfs_lp(lst, 1);
					dfs_lp(*v, 1), lst = -1;
				}
				else lst = *v;
			}
			return false;
		}
	}
	return true;
}

void dfs2(int u) {
	vis[u] = 1, Ans[tota++] = u;
	forto(u) {
		if (*v == inlp) {
			Ans[tota++] = *v;
			continue;
		}
		if (vis[*v]) continue;
		dfs2(*v);
	}
}
