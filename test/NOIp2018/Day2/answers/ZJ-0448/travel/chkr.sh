#!/bin/bash

fname='travel'
g++ $fname.cpp -DLOCAL -Wall -fsanitize=address
for i in 1 2 3 4
do
	echo "Running on test $i..."
	time ./a.out < $fname$i.in > ret
	if diff ret $fname$i.ans; then
		echo " Accept"
	else
		echo " Wrong Answer"
	fi
done
