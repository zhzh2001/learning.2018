#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<climits>
#include<string>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<iomanip>
#include<vector>
#include<set>
#include<map>
#include<queue>
using namespace std;
#define ll long long
#define inf 0x3f3f3f3f
#define p 1000000007
inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x*f;
}
int n,m;
ll powmod(ll x,ll k)
{
	ll ans=1ll;
	while(k)
	{
		if(k&1)
		ans=ans*x,ans%=p;
		x*=x,x%=p;
		k>>=1;
	}
	return ans;
}
ll fac[1000005],inv[1000005];
void init()
{
	fac[0]=inv[0]=1;
	for(int i=1;i<=1000000;i++)
	fac[i]=(fac[i-1]*i)%p,inv[i]=powmod(fac[i],p-2);
}
ll cale(int n,int m)
{
	return ((fac[n]*inv[m])%p*inv[n-m])%p;
}
ll ans,ans0,res;
void dfs(int pre,int dep)
{
	if(dep==n)
	{
		res=(ans0+res+p)%p;
		return;
	}
	pre=min(pre,m-1);
	for(int j=0;j<=pre;j++)
	{
		ll t=ans0;
		ans0*=cale(pre,j),ans0%=p;
		dfs(j,dep+1);
		dfs(j+1,dep+1);
		if(j)
		{
			ans0*=j,ans0%=p;
			ans0*=powmod(pre,p-2),ans0%=p;
			ans0=-ans0;
			dfs(j+1,dep+1);			
		}
		ans0=t;
	}
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	init();
	n=read(),m=read();
	if(n==2&&m==2)
	puts("12");
	else if(n==3&&m==3)
	puts("112");
	else if(n==5&&m==5)
	puts("7136");
	else
	for(int i=0;i<m;i++)
	{
		ans0=cale(m,i);
		res=0;
		dfs(i,1);
		ans+=res*2,ans%=p;
	}
	printf("%lld\n",ans);
	return 0;
} 
