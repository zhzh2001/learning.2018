#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<climits>
#include<string>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<iomanip>
#include<vector>
#include<set>
#include<map>
#include<queue>
using namespace std;
#define inf 0x3f3f3f3f
inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x*f;
}
int n,m;
struct edge
{
	int to,nt;
}e[5005<<1];
int head[5005],tot;
inline void add(int x,int y)
{
	e[++tot].to=y;
	e[tot].nt=head[x];
	head[x]=tot;
}
int vis[5005][2];
int hd;
int que[5005];
int ans[5005]={0,inf};
bool cmp(int a[],int b[])
{
	for(int i=1;i<=n;i++)
	if(a[i]>b[i])
	return 1;
	else if(a[i]<b[i])
	return 0;
	return 0;
}
void cpy(int a[],int b[])
{
	for(int i=1;i<=n;i++)
	b[i]=a[i];
}
inline void dfs(int x)
{
	if(hd>=n)
	{
		if(cmp(ans,que))
		cpy(que,ans);
		return;
	}
	int pt=inf;
	for(register int i=head[x];i;i=e[i].nt)
	{
		int y=e[i].to;
 		if(!vis[y][0]&&y!=vis[x][1])
 		pt=min(pt,y);
	}
	if(pt==inf)
	{
		if(x!=1&&vis[x][1])
		dfs(vis[x][1]);	
		else
		return;	
	}
	else
	{
		if(x!=1&&vis[x][1]) dfs(vis[x][1]);
		que[++hd]=pt;
		if(!cmp(ans,que))
		return;
		vis[pt][0]=1,vis[pt][1]=x;
		dfs(pt);
		vis[pt][0]=vis[pt][1]=0;
		hd--;
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(register int i=1,x,y;i<=m;i++)
	{
		x=read(),y=read();
		add(x,y);add(y,x);
	}
	que[++hd]=1;
	dfs(1);
	for(register int i=1;i<=n;i++)
	printf("%d ",ans[i]);
	puts("");
	return 0;
} 
