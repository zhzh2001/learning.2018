#include<bits/stdc++.h>
#define ll long long
#define Mod 1000000007
using namespace std;
int n,m;

int dx[]={1,0};
int dy[]={0,1};
struct P1{
	int A[10][10];
	struct node{
		int x,y,now,v;
		bool operator < (const node &_)const{return now<_.now;}
	}G[100005],G1[100005];
	bool Judge(){
		int L=1,R=0,n1=0;
		G[++R]=(node){1,1,0,A[1][1]};
		while(L<=R){
			node now=G[L++];
			for(int i=0;i<2;i++){
				int X=now.x+dx[i],Y=now.y+dy[i];
				if(X>n||Y>m)continue;
				node nxt=(node){X,Y,now.now*2+i,now.v*2+A[X][Y]};
				if(X==n&&Y==m){G1[++n1]=nxt;continue;}
				G[++R]=nxt;
			}
		}
		sort(G1+1,G1+1+n1);
		for(int i=1;i<n1;i++)if(G1[i].v<G1[i+1].v)return 0;
//		for(int i=1;i<=n;i++){
//			for(int j=1;j<=m;j++)cerr<<A[i][j];
//			cerr<<endl;
//		}
//		cerr<<endl;
		return 1;
	}
	void solve(){
		int ans=0;
		for(int i=0;i<(1<<(n*m));i++){
			int id=0;
			for(int j=1;j<=n;j++)
				for(int k=1;k<=m;k++)A[j][k]=(i&(1<<id))>0,id++;
			if(Judge())ans++;
		}
		printf("%d\n",ans);
	}
}P1;

struct P2{
	int f1(int x,int y){
		int res=1;
		while(y){
			if(y&1)res=(ll)res*x%Mod;
			x=(ll)x*x%Mod;y>>=1;
		}return res;
	}
	void solve(){
		printf("%lld\n",(ll)4*f1(3,m-1)%Mod);
	}
}P2;

struct P3{
	int f1(int x,int y){
		int res=1;
		while(y){
			if(y&1)res=(ll)res*x%Mod;
			x=(ll)x*x%Mod;y>>=1;
		}return res;
	}
	void solve(){
		if(m==1)puts("8");
		else if(m==2)puts("36");
		else printf("%lld\n",(ll)112*f1(3,m-3)%Mod);
	}
}P3;


int Ans[20][20]={
{0,0,0,0,0,0,0},
{0,2,4,6,8,16,32},
{0,2,4,12,36,108,324},
{0,8,36,112,336,1008},
{0,16,108,336,912,2688},
{0,32,324,1008,2688,7136}
};
struct P4{
	void solve(){
		if(n<=5&&m<=5)printf("%d\n",Ans[n][m]);
	}
}P4;

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n>m)swap(n,m);
	if(n<=3&&m<=3)P1.solve();
	else if(n==2)P2.solve();
	else if(n==3)P3.solve();
	else P4.solve();
	return 0;
}
