#include<bits/stdc++.h>
#define M 5005
using namespace std;
bool ppp1;
int head[M],tot;
struct Edge{int nxt,to;}G[M*2];
void addedge(int x,int y){
	G[tot]=(Edge){head[x],y};
	head[x]=tot++;
}
int n,m;
int A[M],T;
int B[M][M],cnt[M],C[M];

struct P1{
	void Get(int x,int fa1){
		A[++T]=x;
		int n1=0;
		for(int i=head[x];i!=-1;i=G[i].nxt){
			int y=G[i].to;
			if(y==fa1)continue;
			B[x][++n1]=y;
		}
		for(int i=0;i<=n;i++)cnt[i]=0;
		for(int i=1;i<=n1;i++)cnt[B[x][i]]++;
		for(int i=1;i<=n;i++)cnt[i]+=cnt[i-1];
		for(int i=1;i<=n1;i++)C[cnt[B[x][i]]--]=B[x][i];
		for(int i=1;i<=n1;i++)B[x][i]=C[i];
		for(int i=1;i<=n1;i++)Get(B[x][i],x);
	}
	void solve(){
		Get(1,0);
		for(int i=1;i<=n;i++)printf("%d ",A[i]);
		puts("");
	}
}P1;

struct P2{
	int H[M],n1,vis[M];
	bool Geth(int x,int fa1){
		vis[x]=1;
		for(int i=head[x];i!=-1;i=G[i].nxt){
			int y=G[i].to;
			if(y==fa1)continue;
			if(vis[y]){
				H[++n1]=y;
				return 1;
			}
			int p=Geth(y,x);
			if(p){
				H[++n1]=y;
				if(H[1]==x)return 0;
				return 1;
			}
		}
		return 0;
	}
	int X,Y,Ans[M];
	void Get(int x,int fa1){
		A[++T]=x;
		int n2=0;
		for(int i=head[x];i!=-1;i=G[i].nxt){
			int y=G[i].to;
			if(y==fa1)continue;
			if(X==x&&Y==y)continue;
			if(X==y&&Y==x)continue;
			B[x][++n2]=y;
		}
		if(n2<=500){
			sort(B[x]+1,B[x]+1+n2);
		}else{
			for(int i=0;i<=n;i++)cnt[i]=0;
			for(int i=1;i<=n2;i++)cnt[B[x][i]]++;
			for(int i=1;i<=n;i++)cnt[i]+=cnt[i-1];
			for(int i=1;i<=n2;i++)C[cnt[B[x][i]]--]=B[x][i];
			for(int i=1;i<=n2;i++)B[x][i]=C[i];
		}
		for(int i=1;i<=n2;i++)Get(B[x][i],x);
	}
	bool Judge(){
		if(!Ans[1])return 1;
		for(int i=1;i<=n;i++)
			if(Ans[i]<A[i])return 0;
			else if(Ans[i]>A[i])return 1;
		return 0;
	}
	void solve(){
		Geth(1,0);
		for(int i=1;i<=n1;i++){
			X=H[i],Y=H[i+1];
			if(i==n1)Y=H[1];
			T=0;
			Get(1,0);
			if(Judge()){
				for(int j=1;j<=n;j++)Ans[j]=A[j];
			}
		}
		for(int i=1;i<=n;i++)printf("%d ",Ans[i]);
		puts("");
	}
}P2;
bool ppp2;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
//	printf("%.2lf\n",(&ppp2-&ppp1)/1024.0/1024);
	memset(head,-1,sizeof head);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		addedge(x,y);
		addedge(y,x);
	}
	if(m==n-1)P1.solve();
	else P2.solve();
	return 0;
}
