#include<bits/stdc++.h>
#define ll long long
#define M 100005
using namespace std;
bool ppp1;
int head[M],tot;
struct Edge{int nxt,to;}G[M*2];
void addedge(int x,int y){
	G[tot]=(Edge){head[x],y};
	head[x]=tot++;
}
int n,m,V[M];
struct ASK{int x,p1,y,p2;}Ask[M];
char op[5];

struct P1{
	int X,P1,Y,P2;
	ll dp[M][2];
	void f1(int x,int fa1){
		dp[x][0]=0;
		dp[x][1]=V[x];
		if(X==x)dp[x][!P1]=1e15;
		if(Y==x)dp[x][!P2]=1e15;
		for(int i=head[x];i!=-1;i=G[i].nxt){
			int y=G[i].to;
			if(y==fa1)continue;
			f1(y,x);
			if(dp[x][0]!=1e15)dp[x][0]+=dp[y][1];
			if(dp[y][1]==1e15)dp[x][0]=1e15;
			if(dp[x][1]!=1e15)dp[x][1]+=min(dp[y][0],dp[y][1]);
			if(min(dp[y][0],dp[y][1])==1e15)dp[x][1]=1e15;
		}
	}
	void solve(){
		for(int i=1;i<=m;i++){
			X=Ask[i].x,P1=Ask[i].p1;
			Y=Ask[i].y,P2=Ask[i].p2;
			f1(1,0);
			ll ans=min(dp[1][0],dp[1][1]);
			if(ans==1e15)puts("-1");
			else printf("%lld\n",ans);
		}
	}	
}P1;

struct node{
	ll dis[2][2];
	node(){
		dis[0][0]=1e15;
		dis[0][1]=1e15;
		dis[1][0]=1e15;
		dis[1][1]=1e15;
	}
	void Init(){
		dis[0][0]=1e15;
		dis[0][1]=1e15;
		dis[1][0]=1e15;
		dis[1][1]=1e15;
	}
};
struct Tree{
	node tree[M<<2];
	void Up(node &p,node l,node r){
		for(int i=0;i<2;i++)
			for(int j=0;j<2;j++){
				ll mn=min((ll)1e15,l.dis[i][1]+r.dis[1][j]);
				mn=min(mn,l.dis[i][0]+r.dis[1][j]);
				mn=min(mn,l.dis[i][1]+r.dis[0][j]);
				p.dis[i][j]=mn;
			}
	}
	void Build(int L,int R,int p){
		if(L==R){
			tree[p].dis[0][0]=0;
			tree[p].dis[1][1]=V[L];
			return;
		}
		int mid=(L+R)>>1;
		Build(L,mid,p<<1);
		Build(mid+1,R,p<<1|1);
		Up(tree[p],tree[p<<1],tree[p<<1|1]);
	}
	node Query(int L,int R,int l,int r,int p){
		if(L==l&&R==r)return tree[p];
		int mid=(L+R)>>1;
		if(mid>=r)return Query(L,mid,l,r,p<<1);
		else if(mid<l)return Query(mid+1,R,l,r,p<<1|1);
		else{
			node l1=Query(L,mid,l,mid,p<<1),r1=Query(mid+1,R,mid+1,r,p<<1|1),res;
			Up(res,l1,r1);
			return res;
		}
	}
}tree;
struct P2{
	void solve(){
		tree.Build(1,n,1);
		for(int i=1;i<=m;i++){
			int X=Ask[i].x,P1=Ask[i].p1;
			int Y=Ask[i].y,P2=Ask[i].p2;
			if(X>Y)swap(X,Y),swap(P1,P2);
			node ans;
//			cerr<<X<<" "<<Y<<" "<<P1<<" "<<P2<<endl;
			if(X==1){
				node res;
				ans.dis[P1][P1]=((P1==1)?V[X]:0);
				if(X+1<=Y-1){
					res=tree.Query(1,n,X+1,Y-1,1);
					tree.Up(ans,ans,res);
				}
				res.Init();
				res.dis[P2][P2]=((P2==1)?V[Y]:0);
				tree.Up(ans,ans,res);
				if(Y!=n){
					res=tree.Query(1,n,Y+1,n,1);
					tree.Up(ans,ans,res);
				}
				ll mn=min(ans.dis[0][0],ans.dis[0][1]);
				mn=min(mn,min(ans.dis[1][0],ans.dis[1][1]));
				if(mn==1e15)puts("-1");
				else printf("%lld\n",mn);
			}else{
				ans=tree.Query(1,n,1,X-1,1);
				node res;
				res.dis[P1][P1]=((P1==1)?V[X]:0);
				tree.Up(ans,ans,res);
				if(X+1<=Y-1){
					res=tree.Query(1,n,X+1,Y-1,1);
					tree.Up(ans,ans,res);
				}
				res.Init();
				res.dis[P2][P2]=((P2==1)?V[Y]:0);
				tree.Up(ans,ans,res);
				if(Y!=n){
					res=tree.Query(1,n,Y+1,n,1);
					tree.Up(ans,ans,res);
				}
				ll mn=min(ans.dis[0][0],ans.dis[0][1]);
				mn=min(mn,min(ans.dis[1][0],ans.dis[1][1]));
				if(mn==1e15)puts("-1");
				else printf("%lld\n",mn);
			}
		}
	}
}P2;
bool ppp2;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
//	printf("%.2lf\n",(&ppp2-&ppp1)/1024.0/1024);
	memset(head,-1,sizeof head);
	scanf("%d%d%s",&n,&m,op);
	for(int i=1;i<=n;i++)scanf("%d",&V[i]);
	for(int i=1;i<n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		addedge(x,y);
		addedge(y,x);
	}
	for(int i=1;i<=m;i++){
		int x,p1,y,p2;
		scanf("%d%d%d%d",&x,&p1,&y,&p2);
		Ask[i]=(ASK){x,p1,y,p2};
	}
	if(n<=1000)P1.solve();
	else if(op[0]=='A')P2.solve();
	return 0;
}
