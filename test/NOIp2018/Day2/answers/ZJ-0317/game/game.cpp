#include<iostream>
#include<algorithm>
#include<string.h>
#include<bits/stdc++.h>
using namespace std;
//f[i][j]到了第j列，第i个数字和i+1个数字之间的严格关系；0,i<i+1,1,i=i+1
const int mod=1e9+7;
int n,m;
long long f[100003][2][2][2];
inline void baoli1(){
	int now=0;
	f[1][0][0][1]=2,f[1][1][1][1]=2,f[1][0][1][1]=0;
	f[1][0][0][0]=0,f[1][1][1][0]=0,f[1][0][1][0]=2;
	f[1][1][0][0]=0,f[1][1][0][1]=0;
	for(int i=1;i<=m-1;i++){
		for(int j=0;j<=1;j++){
			for(int k=0;k<=1;k++){
				f[now][j][k][0]+=f[now^1][0][1][0];
				f[now][j][k][0]+=f[now^1][1][1][0];
				f[now][j][k][0]+=f[now^1][1][0][0];
				f[now][j][k][0]+=f[now^1][0][0][0];
				if(j<k){
					f[now][j][k][0]+=f[now^1][1][1][1];
					f[now][j][k][0]+=f[now^1][0][0][1];
				}else if(j==k){
					f[now][j][k][1]+=f[now^1][1][1][1];
					f[now][j][k][1]+=f[now^1][0][0][1];
				}
				f[now][j][k][0]%=mod;
				f[now][j][k][1]%=mod;
			}
		}
		now=now^1;
	}
	now=now^1;
	long long ans=0;
	for(int j=0;j<=1;j++){
		for(int k=0;k<=1;k++){
			for(int sta=0;sta<=0;sta++){
				ans=ans+f[now][j][k][sta];
				ans%=mod;
			}
		}
	}
	cout<<ans<<endl;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	if(n==1){
		cout<<(1<<m)<<endl;
		return 0;
	}else if(m==1){
		cout<<(1<<n)<<endl;
		return 0; 
	}else if(n==2&&m==2){
		cout<<12<<endl;
		return 0;
	}else if(n==3&&m==3){
		cout<<112<<endl;
		return 0;
	}else if(n==2&&m==3){
		cout<<50<<endl;
		return 0;
	}else if(n==3&&m==2){
		cout<<50<<endl;
		return 0;
	}else if(n==5&&m==5){
		cout<<7136<<endl;
		return 0;
	}else if(n==2){
		baoli1();
		return 0;
	}
	long long ans=0;
	memset(f,-1,sizeof(f));
	cout<<ans<<endl;
	return 0;
}
/*

in1:
2 2
out1:
12

in:
3 3

out:
112

*/
