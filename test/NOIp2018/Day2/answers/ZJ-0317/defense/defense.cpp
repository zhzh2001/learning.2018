#include<iostream>
#include<algorithm>
#include<bits/stdc++.h>
using namespace std;
long long n,m;
char opt[10];
long long p[200003];
long long f1[200003][2],f2[200003][2];
inline void baoli1(){
	for(int i=1;i<=n;i++){
		cin>>p[i];
	}
	for(int i=1;i<=n-1;i++){
		int u,v;
		cin>>u>>v;
	}
	f1[0][0]=0,f1[0][1]=1e15;
	for(int i=1;i<=n;i++){
		f1[i][0]=f1[i-1][1];
		f1[i][1]=min(p[i]+f1[i-1][0],p[i]+f1[i-1][1]);
	}
	f2[n+1][0]=0,f2[n+1][1]=1e15;
	for(int i=n;i>=1;i--){
		f2[i][0]=f2[i+1][1];
		f2[i][1]=min(p[i]+f2[i+1][0],p[i]+f2[i+1][1]);
	}
	for(int i=1;i<=m;i++){
		int a,x,b,y;
		cin>>a>>x>>b>>y;
		if(a>b){
			swap(x,y),swap(a,b);
		}
		long long ans=1e15;
		if(x==0&&y==1){
			ans=min(f1[x-1][1]+f2[y][1],f1[x-1][0]+f2[y][1]);
		}else if(x==1&&y==1){
			ans=min(f1[x][1]+f2[y][1],ans);
		}else if(x==1&&y==0){
			ans=min(f1[x][1]+f2[y+1][1],f1[x][1]+f2[y+1][0]);
		}else{
			ans=min(f1[x-1][1]+f2[y+1][1],ans);
		}
		cout<<ans<<endl;
	}
}
long long f[200003][2];
inline void baoli2(){
	for(int i=1;i<=n;i++){
		cin>>p[i];
	}
	for(int i=1;i<=n-1;i++){
		int u,v;
		cin>>u>>v;
	}
	f[0][0]=0,f[0][1]=1e15;
	for(int i=1;i<=m;i++){
		int a,x,b,y;
		cin>>a>>x>>b>>y;
		for(int j=1;j<=n;j++){
			f[j][0]=f[j-1][1];
			f[j][1]=min(p[j]+f[j-1][0],p[j]+f[j-1][1]);
			if(j==a){
				if(x==0){
					f[j][1]=1e15;
				}else{
					f[j][0]=1e15;
				}
			}else if(j==b){
				if(y==0){
					f[j][1]=1e15;
				}else{
					f[j][0]=1e15;
				}
			}
		}
		cout<<min(f[n][1],f[n][0])<<endl; 
	}
}
long long ff[100003][2];
inline void baoli3(){
	for(int i=1;i<=n;i++){
		cin>>p[i];
	}
	for(int i=1;i<=n-1;i++){
		int u,v;
		cin>>u>>v;
	}
	f1[1][1]=p[1],f1[1][0]=1e15;
	for(int i=2;i<=n;i++){
		f1[i][0]=f1[i-1][1];
		f1[i][1]=min(p[i]+f1[i-1][0],p[i]+f1[i-1][1]);
	}
	f2[n+1][0]=0,f2[n+1][1]=1e15;
	for(int i=n;i>=1;i--){
		f2[i][0]=f2[i+1][1];
		f2[i][1]=min(p[i]+f2[i+1][0],p[i]+f2[i+1][1]);
	}
	for(int i=1;i<=m;i++){
		int a,x,b,y;
		cin>>a>>x>>b>>y;
		long long ans=1e15;
		if(y==1){
			ans=min(ans,f1[b-1][1]+f2[b][1]);
			ans=min(ans,f1[b-1][0]+f2[b][1]);
			ans=min(ans,f1[b][1]+f2[b+1][0]);
			ans=min(ans,f1[b][1]+f2[b+1][1]);
		}else{
			ans=min(ans,f1[b-1][1]+f1[b+1][1]);
			ans=min(ans,f1[b-1][1]+f1[b+1][0]);
			ans=min(ans,f1[b-1][0]+f1[b+1][1]);
		}
		cout<<ans<<endl;
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m>>opt;
	if(opt[0]=='A'&&opt[1]=='2'){
		baoli1();
	}else if(n<=2000&&m<=2000&&opt[0]=='A'){
		baoli2();
	}else if(opt[0]=='A'&&opt[1]=='1'){
		baoli3();
	}
	return 0;
}
/*
3 3 A3
1 1 1
1 1 1 1
2 0 3 1
1 1 3 1


*/ 
