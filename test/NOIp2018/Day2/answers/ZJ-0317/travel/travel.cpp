#include<iostream>
#include<algorithm>
#include<bits/stdc++.h>
using namespace std;
int n,m;
int head[5003],to[11003],nxt[11003],tot;
inline void add(int u,int v){
	to[++tot]=v,nxt[tot]=head[u],head[u]=tot;
}
int son[5003][5003];
int cnt,ans[5003];
void dfs(int x,int fa){
	ans[++cnt]=x;
	for(int i=head[x];i;i=nxt[i]){
		if(to[i]!=fa){
			son[x][++son[x][0]]=to[i];
		}
	}
	sort(son[x]+1,son[x]+son[x][0]+1);
	for(int i=1;i<=son[x][0];i++){
		dfs(son[x][i],x);
	}
}
inline void solve1(){
	for(int i=1;i<=n-1;i++){
		int u,v;
		cin>>u>>v;
		add(u,v),add(v,u);
	}
	dfs(1,0);
	for(int i=1;i<=cnt;i++){
		cout<<ans[i]<<" ";
	}
}
int tim;
int mp[5003][5003];
int vis[5003],inq[5003];
void dfs2(int x,int fa){
	for(int i=head[x];i;i=nxt[i]){
		if(vis[to[i]]!=tim&&to[i]!=fa){
			vis[to[i]]=tim;
			dfs2(to[i],x);
		}
	}
}
int ans2[5003],cnt2;
void dfs3(int x,int fa){
	vis[x]=tim;
	ans2[++cnt2]=x;
	son[x][0]=0;
	for(register int i=1;i<=n;++i){
		if(mp[x][i]&&i!=fa){
			son[x][++son[x][0]]=i;
		}
	}
	sort(son[x]+1,son[x]+son[x][0]+1);
	for(int i=1;i<=son[x][0];i++){
		dfs3(son[x][i],x);
	}
}
void solve2(){
	for(int i=1;i<=n;i++){
		int u,v;
		cin>>u>>v;
		mp[u][v]=1,mp[v][u]=1;
		add(u,v),add(v,u);
	}
	for(register int i=1;i<=n;++i){
		++tim;
		dfs2(i,0);
		if(vis[i]==tim){
			inq[i]=1;
		}
	}
	cnt=n;
	for(register int i=1;i<=n;++i){
		ans[i]=1000000;
	}
	for(register int i=1;i<=n;++i){
		for(register int j=i+1;j<=n;++j){
			if(inq[i]&&inq[j]&&mp[i][j]){
				mp[i][j]=0,mp[j][i]=0;
				++tim;
				cnt2=0;
				dfs3(1,0);
				bool yes=false;
				for(int k=1;k<=n;k++){
					if(ans[k]<ans2[k]){
						break;
					}else if(ans2[k]<ans[k]){
						yes=true;
						break;
					}
				}
				if(yes){
					for(int i=1;i<=n;i++){
						ans[i]=ans2[i];
					}
				}
				mp[i][j]=1,mp[j][i]=1;
			}
		}
	}
	for(int i=1;i<=n;i++){
		cout<<ans[i]<<" ";
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	if(m==n-1){
		solve1();
	}else{
		solve2();
	}
	return 0;
}
/*

in:
6 6
1 3
2 3
2 5
3 4
4 5
4 6

out:
1 3 2 5 4 6

*/
