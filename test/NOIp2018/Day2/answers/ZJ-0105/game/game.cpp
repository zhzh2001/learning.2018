#include<cstdio>
#include<cstring>
using namespace std;
int n,m,tot,ans,c[105],b[205][205],a[105][105];
void Dfs(int x,int y,int z){
	if (z==n+m-1){++tot;for (int i=1;i<z;++i) b[tot][i]=c[i];b[tot][z]=a[n][m];return;}
	c[z]=a[x][y];
	if (y<m) Dfs(x,y+1,z+1);
	if (x<n) Dfs(x+1,y,z+1);
}
bool ck(int x,int y){
	for (int i=1;i<n+m;++i) if (b[x][i]<b[y][i])return 1;
	return 0;
}
bool check(){
	tot=0;
	memset(b,0,sizeof(b));
	memset(c,0,sizeof(c));
	Dfs(1,1,1);
	for (int i=1;i<=tot;++i)
	for (int j=i+1;j<=tot;++j)if (ck(i,j)) return 0;
	return 1;
}
void dfs(int x,int y){
	if (x>n){ans+=check();return;}
	a[x][y]=0;dfs(x+(y==m),y%m+1);
	a[x][y]=1;dfs(x+(y==m),y%m+1);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==3&&m==3){printf("%d\n",112);return 0;}
	dfs(1,1);
	printf("%d\n",ans);
	return 0;
}
