#include<cstdio>
#include<cctype>
#include<vector>
#include<queue>
#include<algorithm>
using namespace std;
const int maxn=5005,maxe=10005;
int n,e,tot,te,ans,F,fa[maxn],st[maxn],f[maxn],d[maxn],lw[maxn],an[maxn],lk[maxn],son[maxe],nt[maxe];
bool vis[maxn],g[maxn][maxn],ff[maxn];
priority_queue<int,vector<int>,greater<int> >hp;
inline int read(){
	int x=0;bool f=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-')f=0;ch=getchar();}
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-48,ch=getchar();
	if (f) return x;return -x;
}
void add(int x,int y){nt[++tot]=lk[x];son[lk[x]=tot]=y;}
void tarjan(int x,int y){
	d[x]=lw[x]=++te;st[++st[0]]=x;
	for (int i=lk[x];i;i=nt[i])if (!d[son[i]]){
		tarjan(son[i],x);lw[x]=min(lw[x],lw[son[i]]);
	}else if (son[i]!=y) lw[x]=min(lw[x],d[son[i]]);
	if (d[x]==lw[x]){
		do fa[st[st[0]]]=x;while(st[st[0]--]!=x);
	}
}
void dfs(int x,int y){
	vector<int>b;vis[an[++ans]=x]=1;
	for (int i=lk[x];i;i=nt[i])if(!vis[son[i]]){
		b.push_back(son[i]);
		if (fa[son[i]]==F) hp.push(son[i]);
	}
	sort(b.begin(),b.end());
	for (int i=0,r=b.size();i<r;++i) if (!vis[b[i]]){
		if (fa[b[i]]!=F) dfs(b[i],x);else{
			while (vis[hp.top()]||ff[hp.top()])ff[hp.top()]=0,hp.pop();
			int z=hp.top();
			if (g[z][x]) hp.pop(),dfs(z,x);else ff[b[i]]=1;
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();e=read();
	for (int i=1;i<=e;++i){
		int x=read(),y=read();g[x][y]=g[y][x]=1;
		add(x,y);add(y,x);
	}
	for (int i=1;i<=n;++i) if (!d[i]) tarjan(i,i);
	for (int i=1;i<=n;++i) f[fa[i]]++;
	for (int i=1;i<=n;++i) if (f[i]>1) F=i;
	dfs(1,0);
	for (int i=1;i<ans;++i) printf("%d ",an[i]);
	printf("%d\n",an[ans]);
	return 0;
}
