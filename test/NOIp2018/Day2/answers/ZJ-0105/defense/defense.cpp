#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0;bool f=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-')f=0;ch=getchar();}
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-48,ch=getchar();
	if (f) return x;return -x;
}
const int maxn=2005,maxe=4005;
int n,m,A,X,B,Y,tot,INF=2e9,p[maxn],f[maxn][2],lk[maxn],nt[maxe],son[maxe];
char T,TT;
void add(int x,int y){nt[++tot]=lk[x];son[lk[x]=tot]=y;}
void dfs(int x,int y){
	int mi=2e9,k=2e9;bool fg=1,fl=0,flg=0;
	for (int i=lk[x];i;i=nt[i])
	if (son[i]!=y){
		dfs(son[i],x);
		if (son[i]==B){
			fl=1;
			if (Y) f[x][0]+=f[B][1],f[x][1]+=f[B][1],fg=0;
			else f[x][1]+=f[B][0];
		}
		else{
			if (!nt[lk[son[i]]]) flg=1,f[x][0]+=p[son[i]],fg=0;else{
				int z=min(f[son[i]][0],f[son[i]][1]);
				if (z==f[son[i]][1])fg=0;f[x][1]+=z;
				f[x][0]+=z;if (f[son[i]][1]<mi) mi=f[son[i]][1],k=f[son[i]][0];
			}
		}
	}
	if (x==B) f[x][!Y]=INF;
	if (fl&&!Y) f[x][0]=INF;
	if (fg) f[x][0]+=mi-k;
	f[x][1]+=p[x];
}
int get(){
	memset(f,0,sizeof(f));
	A=read(),X=read(),B=read(),Y=read();
	if (!X&&!Y)for (int i=lk[A];i;i=nt[i]) if (son[i]==B) return -1;
	dfs(A,0);return f[A][X];
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();T=getchar();TT=getchar();
	for (int i=1;i<=n;++i) p[i]=read();
	for (int i=1;i<n;++i){
		int x=read(),y=read();
		add(x,y);add(y,x);
	}
	while(m--) printf("%d\n",get());
	return 0;
}
