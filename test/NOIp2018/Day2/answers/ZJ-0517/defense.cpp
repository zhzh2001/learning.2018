#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXN=300005;
const ll INF2=0x3f3f3f3f;
ll f[MAXN][2],F[MAXN][2],ans,r;
vector<int> e[MAXN];
ll p[MAXN],fa[MAXN],g[MAXN],INF=INF2*100;
char st[10];
inline int read()
{
	int f=1,x=0; char c=getchar();
	while (c<'0'||c>'9') { if (c=='-') f=-1; c=getchar(); }
	while (c>='0'&&c<='9') { x=(x<<3)+(x<<1)+c-'0'; c=getchar(); }
	return x*f;
}
void tree_dp(int x,int father)
{
	fa[x]=father;
	for (int i=0;i<e[x].size();i++) 
	    if (e[x][i]!=father) tree_dp(e[x][i],x);
	if (f[x][0]<INF) f[x][0]=0;
	if (f[x][1]<INF) f[x][1]=p[x];
	for (int i=0;i<e[x].size();i++)
	    if (e[x][i]!=father)
	    {
	    	int to=e[x][i];
	    	f[x][0]+=f[to][1];
	    	f[x][1]+=min(f[to][1],f[to][0]);
		}
}

void up(int x,int y)
{
	while (x!=0)
	{
		g[++r]=x;
		F[x][0]=f[x][0];
		F[x][1]=f[x][1];
		x=fa[x];
	}
	while (y!=0)
	{
		g[++r]=y;
		F[y][0]=f[y][0];
		F[y][1]=f[y][1];
		y=fa[y];
	}
}
void change(int x)
{
	x=fa[x];
	if (x==0) return;
	if (f[x][0]<INF) f[x][0]=0;
	if (f[x][1]<INF) f[x][1]=p[x];
	for (int i=0;i<e[x].size();i++)
	    if (e[x][i]!=fa[x])
	    {
	    	int to=e[x][i];
	    	f[x][0]+=f[to][1];
	    	f[x][1]+=min(f[to][1],f[to][0]);
		}
	change(x);
}
void down()
{
	for (int i=1;i<=r;i++) 
	    f[g[i]][0]=F[g[i]][0],f[g[i]][1]=F[g[i]][1];
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n=read(),m=read(); scanf("%s",st);
	for (int i=1;i<=n;i++) p[i]=read();
	for (int i=1;i<n;i++)
	{
		int u=read(),v=read();
		e[u].push_back(v);
		e[v].push_back(u);
	}
	tree_dp(1,0);
	for (int i=1;i<=m;i++)
	{
		int a=read(),x=read(),b=read(),y=read();
	    
	    r=0;
	    up(a,b); 
	    f[a][1-x]=f[b][1-y]=INF;
	    change(a); change(b);
		ll ans=min(f[1][1],f[1][0]);
	    down();
	    
		if (ans>=INF) printf("-1\n");
		else printf("%lld\n",ans);
	}
	return 0;
}
