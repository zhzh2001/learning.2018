#include<bits/stdc++.h>
using namespace std;
const int MAXN=30005;
int num=0,t=0,n,m,k,A,B,q[MAXN],ans[MAXN];
vector<int> e[MAXN];
bool vis[MAXN];
stack<int> V;
inline int read()
{
	int f=1,x=0; char c=getchar();
	while (c<'0'||c>'9') { if (c=='-') f=-1; c=getchar(); }
	while (c>='0'&&c<='9') { x=(x<<3)+(x<<1)+c-'0'; c=getchar(); }
	return x*f;
}
void treesort()
{
	for (int i=1;i<=n;i++)
	    sort(e[i].begin(),e[i].end());
}
void dfs(int x,int father)
{
	vis[x]=1; 
	V.push(x);
	for (int i=0;i<e[x].size();i++)
	    if (t==0&&e[x][i]!=father)
	    {
	    	if (vis[e[x][i]]==1)
	    	{
	    		while (!V.empty()&&V.top()!=e[x][i]) q[++t]=V.top(),V.pop();
	    		q[++t]=V.top();
	    		return;
			}
			dfs(e[x][i],x);
		}
	V.pop();
}
void dfs2(int x,int father)
{
	ans[++num]=x;
	for (int i=0;i<e[x].size();i++)
	    if (e[x][i]!=father) dfs2(e[x][i],x);
}
void dfs3(int x,int father)
{
	if (k==1) return;
	num++;
	if (ans[num]>x) k=2;
	if (k==2) ans[num]=x;
	else if (ans[num]<x) { k=1; return; }
	
	for (int i=0;i<e[x].size();i++)
	    if (k!=1&&e[x][i]!=father)
	    {
	    	int to=e[x][i];
	    	if ((x==A&&to==B)||(x==B&&to==A)) continue;
	    	dfs3(to,x);
		}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=m;i++) 
	{
		int u=read(),v=read();
		e[u].push_back(v);
		e[v].push_back(u);
	}
	treesort();
	if (m==n-1) dfs2(1,0);
	else 
	{
		dfs(1,0);
		q[t+1]=q[1];
		for (int i=1;i<=t/2;i++) swap(q[i],q[t-i+1]);
		memset(ans,10000,sizeof ans);
		for (int i=1;i<=t;i++) 
		{
			num=0,k=0;
		    A=q[i],B=q[i+1];
			dfs3(1,0);
		}
	}
	for (int j=1;j<n;j++) printf("%d ",ans[j]);
	printf("%d\n",ans[n]);
	return 0;
}
