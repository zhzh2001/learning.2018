#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int mods=1e9+7;
const int MAXN=1000050;
ll f[6][MAXN];
inline int read()
{
	int f=1,x=0; char c=getchar();
	while (c<'0'||c>'9') { if (c=='-') f=-1; c=getchar(); }
	while (c>='0'&&c<='9') { x=(x<<3)+(x<<1)+c-'0'; c=getchar(); }
	return x*f;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n=read(),m=read();
	if (n>m) swap(n,m);
	f[1][1]=2,f[2][2]=12,f[3][3]=112,f[4][4]=912,f[5][5]=7136;
	for (int i=2;i<=m;i++) f[1][i]=(f[1][i-1]<<1)%mods;
	for (int i=3;i<=m;i++) f[2][i]=(f[2][i-1]*3)%mods;
	for (int i=4;i<=m;i++) f[3][i]=(f[3][i-1]*3)%mods;
	printf("%d\n",f[n][m]);
	return 0;
}
