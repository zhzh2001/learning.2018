var
  k,n,m,i,x,y:longint;
  visit:array[-1..10000]of boolean;
  n1,line:array[-1..10000]of longint;
  a:array[-1..1000,-1..1000]of longint;
  map:array[-1..1000,-1..1000]of boolean;
function pd(xx,yy:longint):boolean;
var
  i:longint;
begin
  if map[xx,line[yy-1]] or map[xx,line[yy]]  then begin
    if map[xx,line[yy-1]] then k:=line[yy-1] else k:=line[yy];
    exit(true);
  end;
  exit(false);
end;
procedure dfs(x,sum:longint);
var
  i:longint;
begin
  if sum=n then begin
    for i:=1 to n-1 do write(line[i],' ');
    writeln(x);
    halt;
  end;
  visit[x]:=true;
  line[sum]:=x;
  for i:=1 to n do
    if visit[i]=false then
      if pd(i,sum) then begin
        dfs(i,sum+1);
      end;
end;
begin
  assign(input,'travel.in');reset(input);
  assign(output,'travel.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do begin
    readln(x,y);
    map[x,y]:=true;
    map[y,x]:=true;
  end;
  fillchar(visit,sizeof(visit),false);
  visit[1]:=true;
  line[1]:=1;
  dfs(1,1);
  for i:=1 to n do write(line[i],' ');
  close(input);close(output);
end.