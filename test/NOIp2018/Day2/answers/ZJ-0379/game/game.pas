var
  i,ans,n,m:longint;
  a:array[1..4,1..5]of longint;
begin
  assign(input,'game.in');reset(input);
  assign(output,'game.out');rewrite(output);
  readln(n,m);
  if (n<=3) and (m<=3) then begin
    a[1,1]:=2; a[1,2]:=4; a[1,3]:=8;
    a[2,1]:=4; a[2,2]:=12; a[2,3]:=36;
    a[3,1]:=8; a[3,2]:=36; a[3,3]:=112;
    writeln(a[n,m]);
    close(input);close(output);
    halt;
  end;
  if (m=n) and (n=5) then begin
    writeln(7136);
    close(input);close(output);
    halt;
  end;
  if n=1 then begin
    ans:=1;
    for i:=1 to m do
      ans:=(ans*2) mod 1000000007;
    writeln(ans);
    close(input);close(output);
    halt;
  end;
  if n=2 then begin
    ans:=1;
    for i:=1 to m do
      ans:=(ans*3) mod 1000000007;
    writeln(ans);
    close(input);close(output);
    halt;
  end;
  writeln(1);
  close(input);close(output);
end.

