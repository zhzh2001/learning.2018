#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int mod=1e9+7;
int n,m,cnt,ans;
const int N=100;
int f[N*2],a[N][N];
pair<int,int> p[100000];
inline void dfs1(int x,int y,int lu,int val)
{
	if (x==n&&y==m)
	{
		p[++cnt]=make_pair(lu,val);
		return;
	}
	if (x!=n)
		dfs1(x+1,y,lu*2,val*2+a[x+1][y]);
	if (y!=m)
		dfs1(x,y+1,lu*2+1,val*2+a[x][y+1]);
}
inline bool check()
{
	int fx=1,fy=1;
	for (int i=1;i<=n+m;++i)
	{
		int gx=fx,gy=fy;
		for (int j=1;gx>=0&&gy<=m;++j)
		{
			a[gx][gy]=(j<=f[i]?1:0);
			--gx;
			++gy;
		}
		if (fx==n)
			++gy;
		else
			++gx;
	}
	cnt=0;
	dfs1(1,1,0,0);
	sort(p+1,p+1+n);
	for (int i=1;i<cnt;++i)
		if (p[i].second<p[i+1].second)
			return 0;
	return 1;
}
inline void dfs(int x)
{
	if (x==n+m)
	{
		if (check())
			++ans;
	}
	for (int i=0;i<=min(min(x,n+m-x),min(n,m));++i)
	{
		f[x]=i;
		dfs(x+1);
	}
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	dfs(1);
	cout<<ans;
	return 0;
}