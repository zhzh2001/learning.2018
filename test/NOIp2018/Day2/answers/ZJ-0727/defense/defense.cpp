#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define int long long
const ll inf=5e15;
const int N=100005;
int n,m,x,xx,y,yy;
int fa[N];
ll a[N],f[N],g[N],h[N],pf[N],pg[N],ph[N],ans;
int la[N],to[N*2],pr[N*2],cnt;
int px,py,pxx,pyy;
char c[10];
inline void read(ll &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
// inline void read(int &x)
// {
// 	char c=getchar();
// 	while (c>'9'||c<'0')
// 		c=getchar();
// 	x=0;
// 	while (c>='0'&&c<='9')
// 	{
// 		x=x*10+c-'0';
// 		c=getchar();
// 	}
// }
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
inline void update(int x)
{
	if (!x)
		return;
	f[x]=a[x];
	g[x]=0;
	for (int i=la[x];i;i=pr[i])
		if (fa[x]!=to[i])
		{
			f[x]+=min(f[to[i]],g[to[i]]);
			g[x]+=f[to[i]];
		}
	update(fa[x]);
}
inline void dfs(int x)
{
	f[x]=a[x];
	g[x]=0;
	for (int i=la[x];i;i=pr[i])
		if (fa[x]!=to[i])
		{
			fa[to[i]]=x;
			dfs(to[i]);
			f[x]+=min(f[to[i]],g[to[i]]);
			g[x]+=f[to[i]];
		}
}
inline void clear(int x)
{
	while (x!=0)
	{
		f[x]=pf[x];
		g[x]=pg[x];
		// h[x]=ph[x];
		x=fa[x];
	}
}
signed main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n);
	read(m);
	scanf("%s",c);
	for (int i=1;i<=n;++i)
		read(a[i]);
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		add(x,y);
		add(y,x);
	}
	dfs(1);
	for (int i=1;i<=n;++i)
	{
		pf[i]=f[i];
		pg[i]=g[i];
	}
	while (m--)
	{
		read(x);
		read(xx);
		read(y);
		read(yy);
		// dfs(1);
		// ans=min(f[1],g[1]);
		// if (ans>inf/2)
		// 	ans=-1;
		// printf("%d\n",ans);
		if (xx==0)
			a[x]+=inf;
		else
			a[x]-=inf;
		update(x);
		if (yy==0)
			a[y]+=inf;
		else
			a[y]-=inf;
		update(y);
		dfs(1);
		ans=min(f[1],g[1])+inf*(xx+yy);
		if (ans>inf)
			ans=-1;
		printf("%d\n",ans);
		clear(x);
		clear(y);
		if (xx)
			a[x]+=inf;
		else
			a[x]-=inf;
		if (yy)
			a[y]+=inf;
		else
			a[y]-=inf;
	}
	return 0;
}