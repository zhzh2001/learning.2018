#include<bits/stdc++.h>
using namespace std;
const int N=5005;
vector<int> e[N];
int n,m,x,y,vis[N];
int st[N],top,cir[N],fidn_cir_complete,in[N];//first_cir,q[10];
int p,q,f[N],g[N],si;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void dfs(int x)
{
	vis[x]=1;
	printf("%d ",x);
	for (unsigned i=0;i<e[x].size();++i)
		if (!vis[e[x][i]])
			dfs(e[x][i]);
}
inline void clear()
{
	for (int i=1;i<=n;++i)
		vis[i]=0;
	top=0;
}
inline bool check()
{
	if (top!=n)
		return 0;
	if (g[1]==0)
		return 1;
	for (int i=1;i<=n;++i)
	{
		if (f[i]<g[i])
			return 1;
		if (f[i]>g[i])
			return 0;
	}
	return 0;
}
inline void update()
{
	if (check())
	{
		for (int i=1;i<=n;++i)
			g[i]=f[i];
	}
}
inline void dfs_1(int x)
{
	vis[x]=1;
	f[++top]=x;
	for (unsigned i=0;i<e[x].size();++i)
		if (!vis[e[x][i]]&&((x!=p||e[x][i]!=q)&&(x!=q||e[x][i]!=p)))
			dfs_1(e[x][i]);
}
inline void find_cir(int x,int y)
{
	if (fidn_cir_complete)
		return;
	in[x]=1;
	st[++top]=x;
	for (unsigned i=0;i<e[x].size();++i)
		if (e[x][i]!=y)
		{
			if (in[e[x][i]])
			{
				for (int j=top;st[j+1]!=e[x][i];--j)
					cir[++si]=st[j];
				fidn_cir_complete=1;
				break;
			}
			else
				find_cir(e[x][i],x);
		}
	--top;
	in[x]=0;
}
// inline void dfs_2(int x)
// {
// 	printf("%d ",x);
// 	vis[x]=1;
// 	for (unsigned i=0;i<e[x].size();++i)
// 		if (!vis[e[x][i]]&&!cir[e[x][i]])
// 			dfs_2(e[x][i]);
// 	for (unsigned i=0;i<e[x].size();++i)
// 		if (!vis[e[x][i]])
// 			dfs_2(e[x][i]);
// }
// inline void dfs_3(int x)
// {
// 	if (!vis[x])
// 		printf("%d ",x);
// 	vis[x]=1;
// 	for (unsigned i=0;i<e[x].size();++i)
// 		if (!vis[e[x][i]]&&!cir[e[x][i]])
// 			dfs_3(e[x][i]);
// }
// inline void work(int x)
// {
// 	dfs_2(q[2]);
// 	dfs_3(x);
// }
// inline void dfs_1(int x)
// {
	// vis[x]=1;
	// printf("%d ",x);
	// if (!first_cir&&cir[x])
	// {
	// 	first_cir=x;
	// 	for (unsigned i=0;i<e[x].size();++i)
	// 		if (cir[e[x][i]])
	// 			q[++q[0]]=e[x][i];
	// 	for (unsigned i=0;i<e[x].size();++i)
	// 		if (!vis[e[x][i]])
	// 			dfs_1(e[x][i]);
	// }
// 	else
// 	{
// 		if (!cir[x]||x==q[2])
// 		{
// 			for (unsigned i=0;i<e[x].size();++i)
// 				if (!vis[e[x][i]])
// 					dfs_1(e[x][i]);
// 		}
// 		else
// 		{
// 			for (unsigned i=0;i<e[x].size();++i)
// 				if (!vis[e[x][i]])
// 				{
// 					if (e[x][i]>q[2])
// 					{
// 						work(x);
// 						return;
// 					}
// 					else
// 					{
// 						if (!cir[e[x][i]])
// 							dfs_1(e[x][i]);
// 					}
// 				}
// 			for (unsigned i=0;i<e[x].size();++i)
// 				if (!vis[e[x][i]])
// 				{
// 					if (e[x][i]>q[2])
// 					{
// 						work(x);
// 						return;
// 					}
// 					else
// 					{
// 						dfs_1(e[x][i]);
// 					}
// 				}
// 		}
// 	}
// }
// inline void dfs_1(int x)
// {
// 	vis[x]=1;
// 	printf("%d ",x);
// 	if (!first_cir&&cir[x])
// 	{
// 		first_cir=x;
// 		for (unsigned i=0;i<e[x].size();++i)
// 			if (cir[e[x][i]])
// 				q[++q[0]]=e[x][i];
// 		for (unsigned i=0;i<e[x].size();++i)
// 			if (!vis[e[x][i]])
// 				dfs_1(e[x][i]);
// 	}
// 	else
// 	{
// 		if (!cir[x])
// 		{
// 			for (unsigned i=0;i<e[x].size();++i)
// 		}
// 	}
// }
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		e[x].push_back(y);
		e[y].push_back(x);
	}
	for (int i=1;i<=n;++i)
		sort(e[i].begin(),e[i].end());
	if (m==n-1)
		dfs(1);
	else
	{
		find_cir(x,0);
		top=0;
		for (int i=1;i<=si;++i)
		{
			p=cir[i];
			if (i==si)
				q=cir[1];
			else
				q=cir[i+1];
			dfs_1(1);
			update();
			clear();
		}
		for (int i=1;i<=n;++i)
			printf("%d ",g[i]);
	}
	return 0;
}