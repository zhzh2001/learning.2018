#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<vector>
#include<map>
#include<set>
#include<queue>
#include<string.h>
#include<string>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<ctype.h>
#define M 5005
using namespace std;
vector<int>edge[M];
int n,m;
struct P60{
	void dfs(int now,int las){
		printf("%d ",now);
		for(int i=0;i<edge[now].size();i++){
			int to=edge[now][i];
			if(to==las)continue;
			dfs(to,now);
		}
	}
	void solve(){
		for(int i=1;i<=n;i++)sort(edge[i].begin(),edge[i].end());
		dfs(1,0);
	}
}p60;
struct P40{
	int ans[M],sz;
	int ans1[M],sz1;
	int fa[M];
	bool mark[M],mark1[M];
	struct node{
		int x,y;
	}dui[M];
	int cnt;
	void dfs(int now,int las){
		mark[now]=1;
		fa[now]=las;
//		printf("%d\n",now);
		for(int i=0;i<edge[now].size();i++){
			int to=edge[now][i];
			if(to==las)continue;
			if(mark[to]&&!mark1[to]&&!mark1[now]){
				int t=now;
				mark1[t]=1;
				dui[++cnt]=(node){t,to};
//				printf("(%d %d)\n",t,to);
				while(t!=to){
//					printf("->>(%d)\n",t);
					dui[++cnt]=(node){t,fa[t]};
					t=fa[t];
					mark1[t]=1;
				}
			}
			if(mark[to])continue;
			dfs(to,now);
		}
	}
	void rdfs(int now,int las,int check){
		if(!check){
			printf("%d ",now);
			if(!mark1[now]){
				for(int i=0;i<edge[now].size();i++){
					int to=edge[now][i];
					if(to==las)continue;
					rdfs(to,now,check);
				}
			}
			else {
				for(int i=1;i<=cnt;i++){
					check=i,sz1=0;
					for(int k=0;k<edge[now].size();k++){
						int to=edge[now][k];
						if(to==las)continue;
						if(to==dui[check].x&&now==dui[check].y)continue;
						if(to==dui[check].y&&now==dui[check].x)continue;
						rdfs(to,now,check);
					}
					sz=sz1;
					bool ok=1;
					for(int j=1;j<=sz;j++){
						if(ans[j]>ans1[j])break;
						if(ans[j]<ans1[j]){
							ok=0;
							break;
						}
					}
					if(ok)for(int j=1;j<=sz;j++)ans[j]=ans1[j];
				}
			}
		}
		else {
			ans1[++sz1]=now;
			for(int k=0;k<edge[now].size();k++){
				int to=edge[now][k];
				if(to==las)continue;
				if(to==dui[check].x&&now==dui[check].y)continue;
				if(to==dui[check].y&&now==dui[check].x)continue;
				rdfs(to,now,check);
			}
		}
	}
	void solve(){
		memset(mark,0,sizeof(mark));
		memset(mark1,0,sizeof(mark1));
		cnt=0,sz=0;
		for(int i=1;i<=n;i++)ans[i]=n+1;
		for(int i=1;i<=n;i++)sort(edge[i].begin(),edge[i].end());
		dfs(1,-1);
		rdfs(1,-1,0);
		for(int i=1;i<=sz;i++)printf("%d ",ans[i]);
	}
}p40;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		edge[x].push_back(y);
		edge[y].push_back(x);
	}
//	p40.solve();
	if(m==n-1)p60.solve();
	else p40.solve();
	return 0;
}
