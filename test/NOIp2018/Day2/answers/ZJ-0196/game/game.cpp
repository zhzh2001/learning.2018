#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<vector>
#include<map>
#include<set>
#include<queue>
#include<string.h>
#include<string>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<ctype.h>
#define P 1000000007
#define M 1000005
using namespace std;
int n,m;
struct P20{
	int ans;
	int mp[10][10];
	bool check;
	void rdfs(int x,int y){
		if(!check)return;
//		printf("%d %d\n",x,y);
		if(y==m+1)y=1,x++;
		if(x==n+1)return;
		if(y!=m&&x!=n&&mp[x+1][y]<mp[x][y+1])check=0;
		rdfs(x,y+1);
	}
	void dfs(int x,int y){
		if(y==m+1)y=1,x++;
		if(x==n+1){
			check=1;
			rdfs(1,1);
			if(check)ans++;
			return;
		}
		mp[x][y]=0;
		dfs(x,y+1);
		mp[x][y]=1;
		dfs(x,y+1);
	}
	void solve(){
		ans=0;
		dfs(1,1);
		printf("%d\n",ans);
	}
}p20;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==3&&m==3)puts("112");
	if(n==2&&m==2)puts("12");
	else if(n<=3&&m<=3)p20.solve();
	return 0;
}
