#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<vector>
#include<map>
#include<set>
#include<queue>
#include<string.h>
#include<string>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<ctype.h>
#define M 100005
using namespace std;
char c[M];
int n,m;
vector<int>edge[M];
int mark[M],v[M];
int ans1;
void Min(int &a,int b){
	if(a==-1)a=b;
	else if(b!=-1){
		if(a>b)a=b;
	}
}
int dfs(int now,int las,int ok){
	if(mark[now]==0){
		if(!ok){
			return -1;
		}
		int re=0;
		for(int i=0;i<edge[now].size();i++){
			int to=edge[now][i];
			if(to==las)continue;
			int x=dfs(to,now,0);
			if(x==-1)return -1;
			re+=x;
		}
		return re;
	}
	else if(mark[now]==1){
		int re=v[now];
		for(int i=0;i<edge[now].size();i++){
			int to=edge[now][i];
			if(to==las)continue;
			int x=dfs(to,now,1);
			if(x==-1)return -1;
			re+=x;
		}
		return re;
	}
	else {
		int re=0;
		if(ok){
			for(int i=0;i<edge[now].size();i++){
				int to=edge[now][i];
				if(to==las)continue;
				int x=dfs(to,now,0);
				if(x==-1){
					re=-1;
					break;
				}
				re+=x;
			}
		}
		if(!ok)re=-1;
		int tmp=v[now];
		for(int i=0;i<edge[now].size();i++){
			int to=edge[now][i];
			if(to==las)continue;
			int x=dfs(to,now,1);
			if(x==-1){
				tmp=-1;
				break;
			}
			tmp+=x;
		}
		Min(re,tmp);
		return re;
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,c);
	memset(mark,-1,sizeof(mark));
	for(int i=1;i<=n;i++)scanf("%d",&v[i]);
	for(int i=1;i<n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		edge[x].push_back(y);
		edge[y].push_back(x);
	}
	for(int i=1;i<=m;i++){
		int a,x,b,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		mark[a]=x,mark[b]=y;
		int ans=-1;
		Min(ans,dfs(1,0,0));
//		printf("(%d)\n",edge[1][0]);
		Min(ans,dfs(edge[1][0],0,0));
		printf("%d\n",ans);
		mark[a]=-1,mark[b]=-1;
	}
	return 0;
}
