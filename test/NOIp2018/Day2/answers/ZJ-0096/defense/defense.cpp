#include<iostream>
#include<cstdio>
using namespace std;
struct tree{
	int to,nxt; 
}edge[200006];
int n,m,cost[100006],point[100006],u,v,cnt,t2,a,b,x,y,succ;
const int inf=0x7fffffff;
long long f[100006][2];
char t1;
inline void dfs(int now,int fa)
{
	int u;
	f[now][1]=cost[now];
	f[now][0]=0;
	if (!succ) return;
	for (int i=point[now];i;i=edge[i].nxt)
	{
		u=edge[i].to;
		if (u!=fa)
		{
			dfs(u,now);
			f[now][0]+=f[u][1];
			f[now][1]+=f[u][0]>f[u][1]?f[u][1]:f[u][0];
			
		}
	}
	if (t2!=2)
	{
		bool wr=false;
		if (a==now) 
		{f[now][x^1]=inf;wr=true;}
		if (b==now) {f[now][y^1]=inf;wr=true;}
		if (wr&&(fa==a||fa==b)) 
		{
			if (x==0&&y==0) 
			{
				printf("-1\n");
				succ=false;
				return;
			}
		}
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d %d %c%d",&n,&m,&t1,&t2);
	//printf("%d",t2);
	for (int i=1;i<=n;++i)
	{
		scanf("%d",&cost[i]);
	}
	for (int i=1;i<n;++i)
	{
		scanf("%d%d",&u,&v);
		cnt++;
		edge[cnt].nxt=point[u];
		point[u]=cnt;
		edge[cnt].to=v;
		cnt++;
		edge[cnt].nxt=point[v];
		point[v]=cnt;
		edge[cnt].to=u;
	}
	if (t2==2)
	{
		for (int i=1;i<=m;i++)
		{
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if (x==0&&y==0)
			{
				printf("-1\n");
				continue;
			}
			dfs(a,b);
			dfs(b,a);
			printf("%d\n",f[a][x]+f[b][y]);
		}
	}
	else
	{
		for (int i=1;i<=m;i++)
		{
			scanf("%d%d%d%d",&a,&x,&b,&y);
			succ=true;
			dfs(1,0);
			
			if (succ) printf("%d\n",f[1][0]>f[1][1]?f[1][1]:f[1][0]);
		}
	}
	return 0;
}
