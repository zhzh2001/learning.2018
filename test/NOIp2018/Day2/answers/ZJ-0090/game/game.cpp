#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define mod 1000000007
inline int ksm(int a,int b)
{
	int res=1;
	for(;b;b>>=1,a=1ll*a*a%mod) if(b&1) res=1ll*res*a%mod;
	return res;
}
inline void Add(int &x,int y){x=x+y>=mod?x+y-mod:x+y;}
int f[2][9][9];
int n,m;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==3&&m==3){puts("112");return 0;}
	if(n==3&&m==2){puts("36");return 0;}
	if(n==5&&m==5){puts("7136");return 0;}
	if(n==1||m==1){printf("%d",ksm(2,max(n,m)));return 0;}
	if(n==2){int ans=4ll*ksm(3,m-1)%mod;printf("%d",ans);return 0;}
	f[1][1][0]=1;f[1][0][0]=1;
	for(int i=1;i<n+m-1;i++)
	{
		int now=i&1,nxt=now^1;
		memset(f[nxt],0,sizeof(f[nxt]));
		int liml=1;if(i>=m) liml=i-m+1;
		int limr=min(i,n);
		int len=limr-liml+1;
		int lstl=1;if(i-1>=m) lstl=i-m+1;
		int lstr=min(i-1,n);
		int lstlen=max(0,lstr-lstl+1);
		int nxtl=1;if(i+1>=m) nxtl=i-m+2;
		int nxtr=min(i+1,n);
		int nxtlen=nxtr-nxtl+1;
		for(int j=0;j<=len;j++)
		{
			for(int k=0;k<=lstlen;k++)
			{
				for(int l=0;l<=nxtlen;l++)
				{
					bool flag=0;
					if(l==0||l==nxtlen)flag=1;
					else
					{
						if(i<n&&(l==1||l==nxtlen-1)) flag=1;
					//	if(i>m&&)
						else if(l>=k-1&&l<=len-k-1) flag=1;
						else flag=0;
					}
				//	cout<<i<<' '<<j<<' '<<k<<' '<<l<<' '<<flag<<endl;
					if(flag) Add(f[nxt][l][j],f[now][j][k]);
				}
			}
		}
	}
	int ans=0;
	for(int i=0;i<=n;i++) for(int j=0;j<=n;j++) Add(ans,f[(n+m-1)&1][i][j]);
	printf("%d",ans);
	return 0;
}
