#include<iterator>
#include<vector>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=5010;
int u[maxn],v[maxn],ans[maxn];
vector<int> ver[maxn];
int n,m;
namespace solve1
{
	int tot=0;
	inline void dfs(int x,int fat)
	{
		ans[++tot]=x;
		for(int i=0;i<(int)ver[x].size();i++)
		{
			int y=ver[x][i];if(y==fat) continue;
			dfs(y,x);
		}
	}
	inline void solve()
	{
		for(int i=1;i<=m;i++)
		{
			ver[u[i]].push_back(v[i]);
			ver[v[i]].push_back(u[i]);
		}
		for(int i=1;i<=n;i++) sort(ver[i].begin(),ver[i].end());
		dfs(1,0);
	}
}
namespace solve2
{
	bool vis[maxn];int tmp[maxn],tot;
	int del;
	inline void dfs(int x,int fat)
	{
		tmp[++tot]=x;vis[x]=1;
		for(int i=0;i<(int)ver[x].size();i++)
		{
			int y=ver[x][i];if(y==fat||vis[y]) continue;
			if(x==u[del]&&y==v[del]) continue;
			if(x==v[del]&&y==u[del]) continue;
			dfs(y,x);
		}
	}
	inline void solve()
	{
		for(int i=1;i<=m;i++)
		{
			ver[u[i]].push_back(v[i]);
			ver[v[i]].push_back(u[i]);
		}
		for(int i=1;i<=n;i++) sort(ver[i].begin(),ver[i].end());
		for(int i=1;i<=m;i++)
		{
			del=i;memset(vis,0,sizeof(vis));
			tot=0;dfs(1,0);
			if(tot<n) continue;
			bool flag=0;
			if(ans[1]==0) flag=1;else for(int j=1;j<=n;j++) if(tmp[j]!=ans[j]){flag=tmp[j]<ans[j];break;}
			if(flag) for(int j=1;j<=n;j++) ans[j]=tmp[j];
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++) scanf("%d%d",u+i,v+i);
	if(m==n-1) solve1::solve();
	else solve2::solve();
	for(int i=1;i<=n;i++) printf("%d ",ans[i]);
	return 0;
}
