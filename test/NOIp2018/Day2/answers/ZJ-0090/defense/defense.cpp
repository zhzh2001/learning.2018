#include<algorithm>
#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
using namespace std;
#define int long long
const int maxn=100100;
int head[maxn],nxt[maxn<<1],ver[maxn<<1],tot;
int n,m;
inline void addedge(int a,int b)
{
	nxt[++tot]=head[a];ver[tot]=b;head[a]=tot;
	nxt[++tot]=head[b];ver[tot]=a;head[b]=tot;
}
int p[maxn],f[maxn][2],fa[maxn];
inline void dfs(int x,int fat)
{
	f[x][0]=0;f[x][1]=p[x];fa[x]=fat;
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;
		dfs(y,x);
		f[x][0]+=f[y][1];f[x][1]+=min(f[y][0],f[y][1]);
	}
}
inline void update(int x,int opt)
{
	if(x==0) return;
	if(opt==0||f[x][0]<1e16) f[x][0]=0;if(opt==0||f[x][1]<1e16) f[x][1]=p[x];
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fa[x]) continue;
		f[x][0]+=f[y][1];f[x][1]+=min(f[y][0],f[y][1]);
	}
	update(fa[x],opt);
}
string opt;
#undef int
int main()
#define int long long
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m>>opt;
	for(int i=1;i<=n;i++) scanf("%lld",p+i);
	for(int i=1,a,b;i<n;i++) scanf("%lld%lld",&a,&b),addedge(a,b);
	dfs(1,0);
	while(m--)
	{
		int a1,p1,a2,p2;
		scanf("%lld%lld%lld%lld",&a1,&p1,&a2,&p2);
		int t1=f[a1][p1^1];f[a1][p1^1]=1e16;
		update(fa[a1],1);
		int t2=f[a2][p2^1];f[a2][p2^1]=1e16;
		update(fa[a2],1);
		if(min(f[1][0],f[1][1])>=1e16)puts("-1");else printf("%lld\n",min(f[1][0],f[1][1]));
		f[a2][p2^1]=t2;
		update(fa[a2],0);
		f[a1][p1^1]=t1;
		update(fa[a1],0);
	}
}
