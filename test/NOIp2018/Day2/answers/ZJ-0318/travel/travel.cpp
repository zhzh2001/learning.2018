#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
#include <vector>
int read(){
	register int x = 0;
	register int f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 10005
int n, m, k, a[N], p[N], vis[N], x, y;
std :: vector<int> e[N];
void dfs(int u){
	vis[u] = 1, a[++k] = u;
	for (register int i = 0, v; i < e[u].size(); ++i){
		v = e[u][i];
		if (vis[v]) continue;
		if (x == u && y == v) continue;
		if (x == v && y == u) continue;
		dfs(v);
	}
}
bool check(){
	for (register int i = 1; i <= n; ++i){
		if (a[i] < p[i]) return 1;
		if (a[i] > p[i]) return 0;
	}
	return 0;
}
int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	n = read(), m = read();
	for (register int i = 1, u, v; i <= m; ++i)
		u = read(), v = read(), e[u].push_back(v), e[v].push_back(u);
	for (register int i = 1; i <= n; ++i) std :: sort(e[i].begin(), e[i].end());
	if (m == n - 1){
		dfs(1);
		for (register int i = 1; i < n; ++i) printf("%d ", a[i]); printf("%d\n", a[n]);
		return 0;
	}
	for (register int i = 1; i <= n; ++i) p[i] = n + 1;
	for (x = 1; x <= n; ++x)
		for (register int i = 0; i < e[x].size(); ++i){
			for (register int j = 1; j <= n; ++j) vis[j] = 0;
			k = 0, y = e[x][i], dfs(1);
			if (k == n && check()) for (register int j = 1; j <= n; ++j) p[j] = a[j];
		}
	for (register int i = 1; i < n; ++i) printf("%d ", p[i]); printf("%d\n", p[n]);
	return 0;
}
