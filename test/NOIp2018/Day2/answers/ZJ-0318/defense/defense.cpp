#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register int f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
#define INF 10000000000000000ll
char Data_Type[5];
int n, m, a[N], U, X, V, Y;
int edge, to[N << 1], pr[N << 1], hd[N];
long long dp[N][2], ans;
void addedge(int u, int v){
	to[++edge] = v, pr[edge] = hd[u], hd[u] = edge;
}
void dfs(int u, int fa = 0){
	dp[u][0] = 0, dp[u][1] = a[u];
	for (register int i = hd[u], v; i; i = pr[i])
		if ((v = to[i]) != fa){
			dfs(v, u);
			dp[u][0] = std :: min(dp[u][0] + dp[v][1], INF);
			dp[u][1] = std :: min(dp[u][1] + std :: min(dp[v][0], dp[v][1]), INF);
		}
	if (U == u) dp[u][X ^ 1] = INF;
	if (V == u) dp[u][Y ^ 1] = INF;
}
int main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w" ,stdout);
	n = read(), m = read(), read();
	for (register int i = 1; i <= n; ++i) a[i] = read();
	for (register int i = 1, u, v; i < n; ++i)
		u = read(), v = read(), addedge(u, v), addedge(v, u); 
	for (register int i = 1; i <= m; ++i){
		U = read(), X = read(), V = read(), Y = read();
		dfs(1), ans = std :: min(dp[1][0], dp[1][1]);
		if (ans == INF) ans = -1;
		printf("%lld\n", ans);
	}
}
