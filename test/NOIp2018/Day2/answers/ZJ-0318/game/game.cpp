#include <cstdio>
#define P 1000000007
int n, m, p, dp[200005][300];
int check(int x, int y){
	for (register int i = 1; i < n; ++i)
		if (((y >> (i - 1)) & 1) > ((x >> i) & 1)) return 0;
	return 1;
}
int main(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m), p = 1 << n;
	if (n == 1) return printf("%d", 1 << m), 0;
	if (m == 1) return printf("%d", p), 0;
	if (n == 2 && m == 2) return printf("12"), 0;
	if (n == 2 && m == 3 || n == 3 && m == 2) return printf("36"), 0;
	if (n == 3 && m == 3) return printf("112"), 0;
	for (register int i = 0; i < p; ++i) dp[0][i] = 1;
	for (register int i = 1; i < m; ++i) 
		for (register int j = 0; j < p; ++j)
			for (register int k = 0; k < p; ++k)
				if (check(k, j)) (dp[i][j] += dp[i - 1][k]) %= P;
	int ans = 0;
	for (register int i = 0; i < p; ++i) (ans += dp[m - 1][i]) %= P;
	printf("%d\n", ans);
}
