#include<cstdio>
int tot,n,m,fa[4001],dp[4001][2],ui,vi,p[4001],nex[8001],fir[8001],sto[8001],ai,xi,bi,yi,ans;
char s[11];
void addbian(int aa,int bb)
{
	tot++;
	nex[tot]=fir[aa];
	fir[aa]=tot;
	sto[tot]=bb;
}
int getmin(int aa,int bb)
{
	if(aa<bb)return(aa);
	else return(bb);
}
void dfs(int x)
{
	int aa=fir[x];
	dp[x][0]=0;
	dp[x][1]=p[x];
	if(x==ai)
	{
		dp[x][1-xi]=dp[x][1-xi]+200000001;
	}
	if(x==bi)
	{
		dp[x][1-yi]=dp[x][1-yi]+200000001;
	}
	while(aa!=0)
	{
		if(sto[aa]!=fa[x])
		{
			fa[sto[aa]]=x;
			dfs(sto[aa]);
			dp[x][1]=dp[x][1]+getmin(dp[sto[aa]][0],dp[sto[aa]][1]);
			dp[x][0]=dp[x][0]+dp[sto[aa]][1];
		}
		aa=nex[aa];
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%s",s+1);
	for(int i=1;i<=n;i++)scanf("%d",&p[i]);
	for(int i=2;i<=n;i++)
	{
		scanf("%d%d",&ui,&vi);
		addbian(ui,vi);
		addbian(vi,ui);
	}
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&ai,&xi,&bi,&yi);
		fa[1]=1;
		dfs(1);
		ans=getmin(dp[1][0],dp[1][1]);
		if(ans>200000000)printf("-1\n");
		else printf("%d\n",ans);
	}
}
