#include<cstdio>
int wi,n,m,ui,vi,ru[10001],w,nw,cnt,ans[10001],tot,nex[10001],sto[10001],fir[10001],ri[10001],hd,tl,st[10001];
bool p[10001],q[10001];
void addbian(int aa,int bb)
{
	tot++;
	nex[tot]=fir[aa];
	fir[aa]=tot;
	sto[tot]=bb;
}
void dfs(int x)
{
	cnt++;
	ans[cnt]=x;
	p[x]=false;
	for(int i=1;i<=ru[x];i++)
	{
		w=n+1;
		nw=fir[x];
		while(nw!=0)
		{
			if(p[sto[nw]])
			{
				if(sto[nw]<w)w=sto[nw];
			}
			nw=nex[nw];
		}
		ru[w]--;
		dfs(w);
	}
}
void dfs1(int x,int pre)
{
	cnt++;
	ans[cnt]=x;                                                                                             
	p[x]=false;
	for(int i=1;i<=ru[x];i++)
	{
		w=n+1;
		wi=n+1;
		nw=fir[x];
		while(nw!=0)
		{
			if(p[sto[nw]])
			{
				if(sto[nw]<w)
				{
					wi=w;
					w=sto[nw];
				}
				else
				{
					if(wi>sto[nw])wi=sto[nw];
				}
			}
			nw=nex[nw];
		}
		if(w>n)break;
		if(q[x]==false)
		{
			ru[w]--;
			dfs1(w,n+1);
		}
		else
		{
			if(wi>n)
			{
				if(q[w])
				{
					if(pre<w)
					{
						for(int i=1;i<=n;i++)q[i]=false;
						return;
					}
				}
			}
			ru[w]--;
			if(q[w])
			{
				if(wi>n)dfs1(w,pre);
				else dfs1(w,wi);
			}
			else dfs1(w,n+1);
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)ru[i]=0;
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&ui,&vi);
		addbian(ui,vi);
		addbian(vi,ui);
		ru[ui]++;
		ru[vi]++;
	}
	for(int i=1;i<=n;i++)p[i]=true;
	cnt=0;
	if(n>m)dfs(1);
	else
	{
		for(int i=1;i<=n;i++)ri[i]=ru[i];
		hd=1;
		tl=0;
		for(int i=1;i<=n;i++)q[i]=true;
		for(int i=1;i<=n;i++)if(ri[i]==1)
		{
			tl++;
			st[tl]=i;
			q[i]=false;
		}
		while(hd<=tl)
		{
			int aa=fir[st[hd]];
			while(aa!=0)
			{
				if(ri[sto[aa]]>1)
				{
					ri[sto[aa]]--;
					if(ri[sto[aa]]==1)
					{
						q[sto[aa]]=false;
						tl++;
						st[tl]=sto[aa];
					}
				}
				aa=nex[aa];
			}
			hd++;
		}
		dfs1(1,n+1);
	}
	for(int i=1;i<=n;i++)printf("%d ",ans[i]);
}
