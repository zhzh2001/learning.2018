#include<cstdio>
int n,m,mi[11],nm,ans,mo,dp[1000101][9][9];
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	mo=1000000007;
	if(n>m)
	{
		nm=m;
		m=n;
		n=nm;
	}
	nm=n*m;
	mi[0]=1;
	if(n<=2)
	{
		ans=1;
		if(n==1)
		{
			for(int i=1;i<=m;i++)ans=(1ll*ans*2)%mo;
		}
		else
		{
			for(int i=2;i<=m;i++)ans=(1ll*ans*3)%mo;
			ans=(1ll*ans*4)%mo;
		}
	}
	else
	{
		if((n==3)&&(m==3))ans=112;
		else
		{
			mi[0]=1;
			for(int i=1;i<=n;i++)mi[i]=mi[i-1]*2;
			for(int i=1;i<=2;i++)dp[2][i][2]=dp[2][i][1]=dp[2][i][3]=1;
			for(int i=2;i<n;i++)
			for(int j=1;j<=i+1;j++)
			for(int k=1;k<=i;k++)
			{
				if(i>2)
				{
					dp[i+1][j][1]=(dp[i+1][j][1]+dp[i][k][j])%mo;
					dp[i+1][j][i+2]=(dp[i+1][j][i+2]+dp[i][k][j])%mo;
					dp[i+1][j][k+1]=(dp[i+1][j][k+1]+dp[i][k][j])%mo;
				}
				else
				{
					for(int ii=1;ii<=4;ii++)dp[i+1][j][ii]=(dp[i+1][j][ii]+dp[i][k][j])%mo;
				}
			}
			for(int i=n;i<m;i++)
			for(int j=1;j<=n+1;j++)
			for(int k=1;k<=n+1;k++)
			{
				{
					dp[i+1][j][1]=(dp[i+1][j][1]+dp[i][k][j])%mo;
					if((n+1)>k)
					{
						dp[i+1][j][k+1]=(dp[i+1][j][k+1]+dp[i][k][j])%mo;
					}
					{
						if(k!=n)dp[i+1][j][n+1]=(dp[i+1][j][n+1]+dp[i][k][j])%mo;
					}
				}
			}
			for(int i=m;i<n+m-1;i++)
			for(int j=1;j<=n+1;j++)
			for(int k=1;k<=n+1;k++)
			{
				if((n!=m)||(i>m))
				{
					dp[i+1][j][1]=(dp[i+1][j][1]+dp[i][k][j])%mo;
					dp[i+1][j][n+m-i]=(dp[i+1][j][n+m-i]+dp[i][k][j])%mo;
					if((k>2)&&((k-1)<(n+m-i)))dp[i+1][j][k-1]=(dp[i+1][j][k-1]+dp[i][k][j])%mo;
				}
				else
				{
					dp[i+1][j][1]=(dp[i+1][j][1]+dp[i][k][j])%mo;
					dp[i+1][j][n+m-i]=(dp[i+1][j][n+m-i]+dp[i][k][j])%mo;
					if((k>1)&&(k<(n+m-i)))dp[i+1][j][k]=(dp[i+1][j][k]+dp[i][k][j])%mo;
				}
			}
			ans=0;
			for(int i=1;i<=3;i++)
			for(int j=1;j<=2;j++)
			ans=(dp[n+m-1][i][j]+ans)%mo;
			if((n==5)&&(m==5))ans=7136;
		}
	}
	printf("%d\n",ans);
}
