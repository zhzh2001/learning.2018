#include <cstdio>
#include <cstring>
#include <algorithm>

using std::sort;

int n, m, i, j, edn = 0, edgn, a, b, p, q, h[5005], v[5005] = {0}, ans[5005], x[5005];

struct Edge{
	int fr, to;
	bool operator< (Edge a) const{
		if (fr < a.fr) return 1;
		if (fr > a.fr) return 0;
		return to < a.to;
	};
}ed[10005], edg[10005];
void added(int fr, int to){
	ed[edn].fr = fr;
	ed[edn].to = to;
	++edn;
}void dfsa(int p){
	if (v[p]) return;
	v[p] = 1; printf("%d ", p);
	for (int i = h[p]; i < h[p + 1]; ++i) dfsa(ed[i].to);
	return;
}void dfsb(int p){
	if (v[p]) return;
	v[p] = 1; x[q++] = p;
	for (int i = h[p]; i < h[p + 1]; ++i) dfsb(edg[i].to);
	return;
}void cmp(){
	int i;
	for (i = 0; i < n; ++i){
		if (ans[i] < x[i]) return;
		if (ans[i] > x[i]) i = n;
	}for (i = 0; i < n; ++i) ans[i] = x[i];
	return;
}

int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (m == n - 1){
		for (i = 0; i < m; ++i){
			scanf("%d%d", &a, &b);
			added(a, b);
			added(b, a);
		}sort(ed, ed + edn);
		added(0, 0); --edn;
		for (i = 1, p = 0; i <= edn; ++i){
			h[i] = p;
			while (ed[p].fr == i) ++p;
		}dfsa(1);
	}else{
		for (i = 0; i < m; ++i){
			scanf("%d%d", &a, &b);
			added(a, b);
			added(b, a);
		}edgn = edn - 2;
		ans[0] = 2;
		for (i = 0; i < m; ++i){
			memset(v, 0, sizeof(v));
			q = 0;
			for (j = 0; j < i; ++j){
				edg[j + j] = ed[j + j];
				edg[j + j + 1] = ed[j + j + 1];
			}for (j = i + 1; j < m; ++j){
				edg[j + j - 2] = ed[j + j];
				edg[j + j - 1] = ed[j + j + 1];
			}edg[edgn].fr = edg[edgn].to = 0;
			sort(edg, edg + edgn);
			for (j = 1, p = 0; j <= edgn; ++j){
				h[j] = p; while (edg[p].fr == j) ++p;
			}dfsb(1);
			if (q == n) cmp();
		}for (i = 0; i < n; ++i) printf("%d ", ans[i]);
	}fclose(stdin);
	fclose(stdout);
	return 0;
}
