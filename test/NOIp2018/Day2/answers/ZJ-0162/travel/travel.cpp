#include<bits/stdc++.h>
#define f1(i,x) for(int i=1;i<=x;++i)
#define f2(i,x,y) for(int i=x;i<=y;++i)
#define f0(i,x) for(int i=0;i<=x;i++)
using namespace std;
inline int read(){
	int x=0,tmp=1;char c=getchar();
	while(c<'0'||c>'9') { if(c=='-') tmp=-1; c=getchar(); }
	while(c>='0'&&c<='9') { x=x*10+c-'0'; c=getchar(); } 
	return x*tmp; 
}
int n,m;
int from[10005],tot,in[5005];
struct arr{
	int to,nxt;
}f[10005];
void add(int x,int y){
	tot++;
	f[tot].nxt=from[x];
	f[tot].to=y;
	from[x]=tot;
}
bool vis[5005],contac[5005],nothuan[5005],fff=0;
void bfs2(int x){
	if(vis[x]||x==5005) return ;
	printf("%d ",x);
	//cout<<x<<"?";
	vis[x]=1;
	int mi=5005;
	for(int i=from[x];i;i=f[i].nxt){
		int to=f[i].to;
		if(!vis[to]){
			mi=min(to,mi);
		}
	}
	if(mi!=5005){
		bfs2(mi);
		vis[mi]=1;
		while(mi!=5005){
			mi=5005;
			for(int i=from[x];i;i=f[i].nxt){
				int to=f[i].to;
				if(!vis[to]){
					if(nothuan[to])
					mi=min(to,mi);
				}
			}
			if(mi!=5005) bfs2(mi);
		}
	}
	return ;
}
void hbfs(int x,int y){
	if(vis[x]||x==5005) return ;
	printf("%d ",x);
	//cout<<x<<"?"<<y;
	vis[x]=1;
	int mi=5005,nxthuan=5005;
	for(int i=from[x];i;i=f[i].nxt){
		int to=f[i].to;
		if(!vis[to]){
			mi=min(to,mi);
			if((!nothuan[to])&&(nxthuan=5005))
			nxthuan=to;
		}
	}
	if(mi!=5005){
		if(fff==1)
		bfs2(mi);
		else {
			if(nothuan[mi]) bfs2(mi);
			else hbfs(min(mi,y),max(mi,y)); 
		}
		vis[mi]=1;
		while(mi!=5005){
			mi=5005;
			for(int i=from[x];i;i=f[i].nxt){
				int to=f[i].to;
				if(!vis[to]){
					if(nothuan[to])
					mi=min(to,mi);
				}
			}
			if(mi!=5005) {
				if(fff==1)
				bfs2(mi);
				else {
					if(nothuan[mi]) bfs2(mi);
					else hbfs(min(mi,y),max(mi,y)); 
				}		
			}
		}
	}
	if(nxthuan>y){
		fff=1;
		if(!vis[y])
		bfs2(y);
		if(!vis[nxthuan])
		bfs2(nxthuan);
	}
	else hbfs(nxthuan,y);
}
void bfs(int x){
	if(vis[x]||x==5005) return ;
	printf("%d ",x);
	//cout<<x<<"?";
	vis[x]=1;
	int mi=5005,nxthuan=5005,nxthuan2=5005;
	for(int i=from[x];i;i=f[i].nxt){
		int to=f[i].to;
		if(!vis[to]){
			mi=min(to,mi);
			if((!nothuan[to])&&(nxthuan==5005))
			nxthuan=to;
			if((!nothuan[to])&&(nxthuan!=5005)&&(to!=nxthuan))
			nxthuan2=to;
		}
	}
	if(mi!=5005){
		if(nothuan[mi]||nxthuan2==5005)
		bfs(mi);
		else hbfs(min(nxthuan,nxthuan2),max(nxthuan,nxthuan2));
		vis[mi]=1;
		while(mi!=5005){
			mi=5005;
			for(int i=from[x];i;i=f[i].nxt){
				int to=f[i].to;
				if(!vis[to]){
					mi=min(to,mi);
				}
			}
			if(mi!=5005) {
				if(nothuan[mi]||nxthuan2==5005)
				bfs(mi);
				else hbfs(min(nxthuan,nxthuan2),max(nxthuan,nxthuan2));
			}
		}
	}
	if(nxthuan!=5005&&nxthuan2==5005) bfs(nxthuan);
	if(nxthuan!=5005&&nxthuan2!=5005) hbfs(min(nxthuan,nxthuan2),max(nxthuan,nxthuan2));
	return ;
}
queue<int> q;
int main(){
    freopen("travel.in","r",stdin);
    freopen("travel.out","w",stdout);
	n=read(),m=read();
	f1(i,m){
		int x=read(),y=read();
		in[x]++,in[y]++;
		add(x,y);
		add(y,x);
	} 
	f1(i,n) if(in[i]==1) q.push(i);
	while(!q.empty()){
		int p=q.front();
		nothuan[p]=1;
		q.pop();
		for(int i=from[p];i;i=f[i].nxt){
			int to=f[i].to;
			in[to]--;
			if(in[to]==1) q.push(to);
		}
	}
	//f1(i,n) cout<<nothuan[i]<<endl;
	bfs(1);
	return 0;
}
