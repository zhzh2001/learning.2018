#include <cstdio>
#include <vector>
#include <algorithm>
const int N = 5005;
int n, m, ans[N], idx, banx, bany;
std::vector<int> g[N];
void addedge(int x, int y) { g[x].push_back(y); g[y].push_back(x); } 
namespace p1 {
	void dfs(int x, int f = 0) {
		ans[++idx] = x;
		for (std::vector<int>::iterator it = g[x].begin(); it != g[x].end(); ++it) if (*it != f && !(x == banx && *it == bany) && !(x == bany && *it == banx)) dfs(*it, x);
	}
	void start() {
		dfs(1);
		for (int i = 1; i <= n; i++) std::printf("%d%c", ans[i], " \n"[i == n]);
	}
}
namespace p2 {
	int ans[N], stack[N], top, cycle[N], size, vis[N];
	void dfs(int x, int f = 0) {
		stack[++top] = x; vis[x] = 1;
		for (std::vector<int>::iterator it = g[x].begin(); it != g[x].end(); ++it) if (*it != f) {
			if (!vis[*it]) dfs(*it, x);
			else if (!size) {
				int v;
				do {
					v = stack[top--];
					cycle[++size] = v;	
				} while (v != *it);
			}
		}
		--top;
	}
	void check() {
		int flag = 0;
		for (int i = 1; i <= n; i++) 
			if (::ans[i] > ans[i]) break;
			else if (::ans[i] < ans[i]) { flag = 1; break; }
		if (flag) 
			for (int i = 1; i <= n; i++) ans[i] = ::ans[i]; 
	}
	void start() {
		dfs(1);
		for (int i = 1; i <= n; i++) ans[i] = n + 1;
		for (int i = 1; i <= size; i++) {
			banx = cycle[i], bany = cycle[i % size + 1];
			idx = 0; p1::dfs(1);
			check();
		}
		for (int i = 1; i <= n; i++) std::printf("%d%c", ans[i], " \n"[i == n]);
	}
}
int main() {
	std::freopen("travel.in", "r", stdin);
	std::freopen("travel.out", "w", stdout);
	std::scanf("%d%d", &n, &m);
	for (int i = 1, x, y; i <= m; i++) std::scanf("%d%d", &x, &y), addedge(x, y);
	for (int i = 1; i <= n; i++) std::sort(g[i].begin(), g[i].end());
	if (m == n - 1) p1::start();
	else p2::start();
	std::fclose(stdin); std::fclose(stdout);
	return 0;
}
