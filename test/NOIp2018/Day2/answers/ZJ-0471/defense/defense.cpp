#include <cstdio>
#include <algorithm>
const int N = 100005;
int n, m, v[N], tot, head[N], f[N];
char typ[5];
typedef long long LL;
LL dp[N][2];
struct edge { int to, nxt; } e[N << 1];
void addedge(int x, int y) {
	e[++tot] = (edge) { y, head[x] }; head[x] = tot;
	e[++tot] = (edge) { x, head[y] }; head[y] = tot;
}
const LL inf = 0x3f3f3f3f3f3fLL;
namespace p1 {
	int a, x, b, y;
	void dfs(int x, int f = 0) {
		dp[x][1] = v[x];
		dp[x][0] = 0;
		for (int i = head[x]; i; i = e[i].nxt)
			if (e[i].to != f) {
				::f[e[i].to] = x;
				dfs(e[i].to, x);
				dp[x][0] += dp[e[i].to][1];
				dp[x][1] += std::min(dp[e[i].to][0], dp[e[i].to][1]);
			}
		if (x == p1::a) dp[x][!p1::x] = inf;
		if (x == p1::b) dp[x][!p1::y] = inf;
	}
	void start() {
		for (int i = 1; i <= m; i++) {
			std::scanf("%d%d%d%d", &a, &x, &b, &y);
			dfs(1);
			LL ans = std::min(dp[1][0], dp[1][1]);
			if (ans >= inf) std::puts("-1");
			else std::printf("%lld\n", ans);
		}
	}
}
namespace p2 {
	void start() {
	}
}
namespace p3 {
	LL _dp[N][2], __dp[N][2];
	int a, x, b, y;
	void update(int x) {
		__dp[f[x]][0] = _dp[f[x]][0] -= dp[x][1];
		__dp[f[x]][0] = _dp[f[x]][0] += _dp[x][1];
		__dp[f[x]][1] = _dp[f[x]][1] -= std::min(dp[x][0], dp[x][1]);
		__dp[f[x]][1] = _dp[f[x]][1] += std::min(_dp[x][0], _dp[x][1]);
		if (f[f[x]]) update(f[x]);
	}
	void update2(int x) {
		__dp[f[x]][0] -= _dp[x][1];
		__dp[f[x]][0] += __dp[x][1];
		__dp[f[x]][1] -= std::min(_dp[x][0], _dp[x][1]);
		__dp[f[x]][1] += std::min(__dp[x][0], __dp[x][1]); 
		if (f[f[x]]) update2(f[x]);
	}
	void clear() {
		for (int i = a; i; i = f[i]) __dp[i][0] = _dp[i][0] = dp[i][0], __dp[i][1] = _dp[i][1] = dp[i][1];
		for (int i = b; i; i = f[i]) __dp[i][0] = _dp[i][0] = dp[i][0], __dp[i][1] = _dp[i][1] = dp[i][1];
	}
	void query() {
		__dp[a][!x] = _dp[a][!x] = inf;
		update(a);
		__dp[b][!y] = inf;
		update2(b);
		LL ans = std::min(__dp[1][0], __dp[1][1]);
		if (ans >= inf) std::puts("-1");
		else std::printf("%lld\n", ans);
		clear();
	}
	void start() {
		p1::dfs(1);
		for (int i = 1; i <= n; i++) _dp[i][0] = dp[i][0], _dp[i][1] = dp[i][1], __dp[i][0] = dp[i][0], __dp[i][1] = dp[i][1];
		for (int i = 1; i <= m; i++) std::scanf("%d%d%d%d", &a, &x, &b, &y), query();
	}
}
int main() {
	std::freopen("defense.in", "r", stdin);
	std::freopen("defense.out", "w", stdout);
	std::scanf("%d%d%s", &n, &m, typ);
	for (int i = 1; i <= n; i++) std::scanf("%d", &v[i]);
	for (int i = 1, x, y; i < n; i++) std::scanf("%d%d", &x, &y), addedge(x, y);
	if (n <= 2000 && m <= 2000) p1::start();
	else p3::start();
	std::fclose(stdin); std::fclose(stdout);
	return 0;
}
