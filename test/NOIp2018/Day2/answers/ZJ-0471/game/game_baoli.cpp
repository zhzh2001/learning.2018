#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
int n, m, map[5][5], flag = 1, ans;
std::string now; 
std::vector<std::pair<std::string, std::string> > res;
void dfs(int x, int y, std::string w, std::string s) {
	if (x == n - 1 && y == m - 1) return res.push_back(std::make_pair(w, s));
	if (x < n - 1) dfs(x + 1, y, w + "D", s + char(map[x + 1][y] + '0'));
	if (y < m - 1) dfs(x, y + 1, w + "R", s + char(map[x][y + 1] + '0'));
}
void check() {
	res.clear();
	dfs(0, 0, "", std::string() + char(map[0][0] + '0'));
	if (map[0][0] & map[0][1] & map[0][2] & map[1][0] & map[1][1] & map[2][0] & map[2][1] & map[2][2]) {
		for (int i = 0; i < res.size(); ++i)
			std::cout << res[i].first << " " << res[i].second << std::endl;;
	}
	for (int i = 0; i < res.size(); ++i)
		for (int j = i + 1; j < res.size(); ++j)
			if (res[i].second < res[j].second) return;
	std::printf("---- %d\n", ++ans);
	for (int i = 0; i < n; i++, std::puts(""))
		for (int j = 0; j < m; j++)
			std::printf("%c", map[i][j] + '0');
}
void dfs(int x, int y) {
	if (x == n) return check();
	map[x][y] = 1;
	dfs(x + (y == m - 1), (y + 1) % m);
	map[x][y] = 0;
	dfs(x + (y == m - 1), (y + 1) % m);
}
int main() {
	std::scanf("%d%d", &n, &m);
	dfs(0, 0);
	std::printf("%d\n", ans);
	return 0;
}
