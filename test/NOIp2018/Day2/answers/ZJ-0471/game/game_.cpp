#include <iostream>
#include <cstdio>
int n, m;
typedef long long LL;
bool check(int i, int j) {
	if ((~i) << 1 & j) return 0;
	else return 1;
}
bool check2(int i, int j, int k) {
	int mask = (i & j >> 1 | ~i & ~(j >> 1)) & 1 << n - 1;
	return (j & mask >> 1) == (k >> 1 & mask >> 1);
}
LL dp[2][1 << 8][1 << 8];
const int mod = 1000000007;
int main() {
	std::cin >> n >> m;
	if (m == 1) std::cout << (1 << n) << std::endl;
	else {
		for (int i = 0; i < 1 << n; i++)
			for (int j = 0; j < 1 << n; j++)
				if (check(i, j)) dp[0][i][j] = 1;
		for (int t = 1; t < m - 1; t++) {
			int now = t & 1, lst = !t;
			for (int i = 0; i < 1 << n; i++)
				for (int j = 0; j < 1 << n; j++) if (dp[lst][i][j])
					for (int k = 0; k < 1 << n; k++) {
						if (check(j, k) && check2(i, j, k)) dp[now][j][k] += dp[lst][i][j];
					}
			for (int i = 0; i < 1 << n; i++)
				for (int j = 0; j < 1 << n; j++) 
					dp[now][i][j] %= mod;
		}
		LL ans = 0;
		for (int i = 0; i < 1 << n; i++)
			for (int j = 0; j < 1 << n; j++)
				ans += dp[m & 1][i][j];
		std::cout << ans % mod << std::endl;
	}
	return 0;
}
