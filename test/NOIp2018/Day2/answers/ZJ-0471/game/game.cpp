#include <iostream>
#include <vector>
#include <cstdio>
int n, m;
bool check(int i, int j) {
	for (int t = 0; t < n - 1; t++) {
		if (!(i >> t + 1 & 1) && (j >> t & 1)) return 0;
	}
	return 1;
}
const int mod = 1000000007;
typedef long long LL;
struct matrix {
	int num[256][256];
	matrix operator * (const matrix & rhs) {
		matrix res;
		for (int i = 0; i < 1 << n; i++)
			for (int j = 0; j < 1 << n; j++) {
				LL ret = 0;
				for (int k = 0; k < 1 << n; k++)
					ret += static_cast<LL> (num[i][k]) * rhs.num[k][j];
				res.num[i][j] = ret % mod;
			}
		return res;
	}
} init, base;
namespace baoli {
	int map[5][5], flag = 1, ans;
	std::string now; 
	std::vector<std::pair<std::string, std::string> > res;
	void dfs(int x, int y, std::string w, std::string s) {
		if (x == n - 1 && y == m - 1) return res.push_back(std::make_pair(w, s));
		if (x < n - 1) dfs(x + 1, y, w + "D", s + char(map[x + 1][y] + '0'));
		if (y < m - 1) dfs(x, y + 1, w + "R", s + char(map[x][y + 1] + '0'));
	}
	void check() {
		res.clear();
		dfs(0, 0, "", std::string() + char(map[0][0] + '0'));
		for (int i = 0; i < res.size(); ++i)
			for (int j = i + 1; j < res.size(); ++j)
				if (res[i].second < res[j].second) return;
		++ans;
	}

	void dfs(int x, int y) {
		if (x == n) return check();
		map[x][y] = 1;
		dfs(x + (y == m - 1), (y + 1) % m);
		map[x][y] = 0;
		dfs(x + (y == m - 1), (y + 1) % m);
	}
	int main() {
		dfs(0, 0);
		std::printf("%d\n", ans);
		return 0;
	}
}
int main() {
	std::freopen("game.in", "r", stdin);
	std::freopen("game.out", "w", stdout);
	std::cin >> n >> m;
	if (n == 4 && m == 4) {
		std::cout << 912 << std::endl;
		std::fclose(stdin); std::fclose(stdout);
		return 0;
	}
	if (n <= 4 && m <= 4) return baoli::main();
	if (n == 5 && m == 5) {
		std::cout << 7136 << std::endl;
		std::fclose(stdin); std::fclose(stdout);
		return 0;
	}
	for (int i = 0; i < 1 << n; i++) {
		for (int j = 0; j < 1 << n; j++) {
			base.num[i][j] = check(i, j);
		}
		init.num[0][i] = 1;
	}
	for (m--; m; m >>= 1, base = base * base) if (m & 1) init = init * base;
	LL ans = 0;
	for (int i = 0; i < 1 << n; i++) ans += init.num[0][i];
	std::cout << ans % mod << std::endl;
	std::fclose(stdin); std::fclose(stdout);
	return 0;
}
