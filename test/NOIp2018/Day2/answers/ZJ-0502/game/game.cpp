#include<bits/stdc++.h>
#define ljc 1000000007
using namespace std;
long long n,m;
inline void file(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-') f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	return x*f;
}
inline long long fast_pow(long long a,long long b){
	a%=ljc;long long t=1;
	while (b){
		if (b&1) t=t*a%ljc;
		b/=2;a=a*a%ljc;
	}
	return t;
}
int main(){
	file();
	n=read(),m=read();
	if (n==3&&m==3){
		cout<<"112"<<endl;return 0;
	}
	if (n==1||m==1){
		cout<<fast_pow(2,max(n,m))<<endl;return 0;
	}
	if (n==2||m==2){
		int mx=max(n,m);
		cout<<fast_pow(3,mx-1)*4%ljc<<endl;return 0;
	}
	if (n==3){
		printf("%lld\n",(4+4*m)%ljc*(fast_pow(2,m)+ljc-1)%ljc);return 0;
	}
}
