#include<bits/stdc++.h>
using namespace std;
inline void file(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
}
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	while (!isdigit(ch)){if (ch=='-') f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	return x*f;
}
inline char gc(){
	char ch=getchar();
	while (!isalpha(ch)){ch=getchar();}
	return ch;
}
struct edge{
	long long to,nxt;
}g[300001];
long long n,m,a[100004],fat[100004],dp[100004][2],dpnew[100004][2],head[100004],tot,whtf,xz[100004];
char wht;
bool flag=0;
inline void made(long long from,long long to){
	g[++tot].to=to;g[tot].nxt=head[from];head[from]=tot;
}
inline long long getans(){
	if (flag) return -1;
	if (xz[1]==0){
		return min(dp[1][0],dp[1][1]);
	}else if (xz[1]==1){
		return dp[1][1];
	}else return dp[1][0];
}
void dfs(long long u,long long fa){
	fat[u]=fa;
	if (xz[u]==1) dp[u][0]=99999999999,dp[u][1]=a[u];
	if (xz[u]==0) dp[u][1]=a[u];
	if (xz[u]==-1) dp[u][1]=99999999999;
	for (int i=head[u];i;i=g[i].nxt){
		long long v=g[i].to;
		if (v==fa) continue;
		dfs(v,u);
		if (xz[u]==0){
			dp[u][0]+=dp[v][1];
			dp[u][1]+=min(dp[v][1],dp[v][0]);
		}else if (xz[u]==1){
			dp[u][1]+=min(dp[v][1],dp[v][0]);
		}else{
			if (xz[v]==-1){
				flag=1;
			}
			dp[u][0]+=dp[v][1];
		}
		if (flag==1) return;
	}
}
void check(long long u,long long v){
	if (u==0){
		return;
	}
	if (xz[u]==0){
		dpnew[u][0]-=dp[v][1];
		dpnew[u][1]-=min(dp[v][1],dp[v][0]);
		dpnew[u][0]+=dpnew[v][1];
		dpnew[u][1]+=min(dpnew[v][1],dpnew[v][0]);
	}else if (xz[u]==1){
		dpnew[u][1]-=min(dp[v][1],dp[v][0]);
		dpnew[u][1]+=min(dpnew[v][1],dpnew[v][0]);
	}
	check(fat[u],u);
	if (u==1){
		printf("%lld\n",dpnew[1][1]);
	}
	dpnew[u][1]=dp[u][1];
	dpnew[u][0]=dp[u][0];
}
int main(){
	file();
	n=read(),m=read();wht=gc();whtf=read();tot=0;
	for (int i=1;i<=n;i++){
		a[i]=read();
	}
	for (int i=1;i<n;i++){
		long long x=read(),y=read();
		made(x,y);made(y,x);
	}
	memset(xz,0,sizeof(xz));
	if (n<=2000){
		while (m--){
			for (int i=1;i<=n;i++) dp[i][0]=dp[i][1]=0;flag=0;
			long long x=read(),xx=read(),y=read(),yy=read();
			if (xx==1){
				xz[x]=1;
			}else{
				xz[x]=-1;
			}
			if (yy==1){
				xz[y]=1;
			}else{
				xz[y]=-1;
			}
			dfs(1,0);
			xz[x]=0;xz[y]=0;
			printf("%lld\n",getans());
		}
	}else{
		if (wht=='B'&&whtf==1){
			xz[1]=1;
			dfs(1,0);
			for (int i=1;i<=n;i++){
				dpnew[i][0]=dp[i][0];dpnew[i][1]=dp[i][1];
			}
			while (m--){
				//for (int i=1;i<=n;i++) dp[i][0]=dp[i][1]=0;
				flag=0;
				long long x=read(),xx=read(),y=read(),yy=read();
				if (yy==1){
					xz[y]=1;dpnew[y][0]=9999999999;
				}else{
					xz[y]=-1;dpnew[y][1]=9999999999;
				}
				check(fat[y],y);
				xz[y]=0;dpnew[y][1]=dp[y][1];dpnew[y][0]=dp[y][0];
			}
		}
	}
	return 0;
}
