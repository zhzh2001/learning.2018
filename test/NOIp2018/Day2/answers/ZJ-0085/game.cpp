#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int Mod=1e9+7;
int n,m;
long long ans,d;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	if (n==1) ans=2,d=2;
	if (n==2) ans=12,d=3;
	if (n==3) ans=112,d=3;
	for (int i=n+1;i<=m;i++)
		ans=ans*d%Mod;
	printf("%lld\n",ans);
	return 0;
}
