#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int n,m,type,val[100010],d[100010];
int nxt[200010],head[200010],vet[200010],cnt,fa[100010];
long long dp[100010][2],f[100010][2],g[100010][2],k[100010][2];
int a,b,x,y;
int read()
{
	int ans=0;
	char ch=getchar();
	while (ch<'0' || ch>'9') 
	{
		if (ch=='A') ans=1;
		if (ch=='B') ans=2;
		if (ch=='C') ans=3;
		ch=getchar();
	}
	while (ch>='0' && ch<='9')
	{
		ans=ans*10+ch-'0';
		ch=getchar();
	}
	return ans;
}
void modify(int u,int D)
{
	d[u]=D;
	if (D==0)
	{
		for (int i=head[u];i;i=nxt[i])
		{
			int v=vet[i];
			d[v]=1;
		}
	}
	if (D==-1)
	{
		for (int i=head[u];i;i=nxt[i])
		{
			int v=vet[i];
			d[v]=-1;
		}
	}
}
void addedge(int u,int v)
{
	nxt[++cnt]=head[u];vet[cnt]=v;head[u]=cnt;
}
void DFS(int u,int f)
{
	fa[u]=f;
	for (int i=head[u];i;i=nxt[i])
	{
		int v=vet[i];
		if (v!=f) DFS(v,u);
	}
}
void dfs(int u,int f)
{
	dp[u][0]=0;dp[u][1]=val[u];
	for (int i=head[u];i;i=nxt[i])
	{
		int v=vet[i];
		if (v!=f)
		{
			dfs(v,u);
			if (d[u]!=1) dp[u][0]+=dp[v][1];
			if (d[u]!=0) 
			{	
				if (d[v]==0) dp[u][1]+=dp[v][0];
				if (d[v]==1) dp[u][1]+=dp[v][1];
				if (d[v]==-1) dp[u][1]+=min(dp[v][0],dp[v][1]);
			}
		}
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),type=read();
	for (int i=1;i<=n;i++) val[i]=read();
	for (int i=1;i<n;i++)
	{
		int u=read(),v=read();
		addedge(u,v);addedge(v,u);
	}
	memset(d,-1,sizeof(d));
	DFS(1,0);
	if ((type==12 || type==11))
	{
		for (int i=1;i<=n;i++) 
		{
			f[i][0]=f[i-1][1];
			f[i][1]=min(f[i-1][0],f[i-1][1])+val[i];
		}
		for (int i=n;i>=1;i--)
		{
			g[i][0]=g[i+1][1];
			g[i][1]=min(g[i+1][0],g[i+1][1])+val[i];
		}
		k[1][1]=val[1];
		for (int i=2;i<=n;i++)
		{
			k[i][0]=k[i-1][1];
			if (i==2) k[i][1]=k[i-1][1]; else k[i][1]=min(k[i-1][0],k[i-1][0]);
			k[i][1]+=val[i];
		}
		while (m--)
		{
			a=read(),x=read(),b=read(),y=read();
			if ((fa[a]==b || fa[b]==a) && (x+y)==0)
			{
				printf("-1\n");
				continue;
			}
			if (a>b) swap(a,b);
			if (type==12) printf("%lld\n",f[a][x]+g[b][y]);
			else if (type==11) printf("%lld\n",k[b][y]+((y==0)?g[b+1][1]:min(g[b+1][1],g[b+1][0])));
		}
	}
	else
	{
		while (m--)
		{
			a=read(),x=read(),b=read(),y=read();
			if ((fa[a]==b || fa[b]==a) && (x+y==0)) 
			{
				printf("-1\n");
				continue;
			}
			modify(a,x);
			modify(b,y);
			dfs(1,0);
			if (d[1]==1) printf("%lld\n",dp[1][1]);
			if (d[1]==0) printf("%lld\n",dp[1][0]);
			if (d[1]==-1) printf("%lld\n",min(dp[1][0],dp[1][1]));
			modify(a,-1);
			modify(b,-1);
		}
	}
	return 0;
}
