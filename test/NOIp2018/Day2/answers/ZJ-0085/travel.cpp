#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int n,m,edge[5050][5050],cnt[5050],ans[5050],vis[5050],tot,u,v;
int Cnt,delu[5050],delv[5050],fa[5050],st,ed,Ans[5050];
bool Del[5050][5050];
int read()
{
	int ans=0;
	char ch=getchar();
	while (ch<'0' || ch>'9') ch=getchar();
	while (ch>='0' && ch<='9')
	{
		ans=ans*10+ch-'0';
		ch=getchar();
	}
	return ans;
}
int check()
{
	for (int i=1;i<=n;i++)
		if (ans[i]<Ans[i]) return 1;
		else if (ans[i]>Ans[i]) return 0;
	return 0;
}
void dfs(int u,int f)
{
	if (!vis[u])
	{
		vis[u]=1;
		ans[++tot]=u;
	}
	for (int i=1;i<=cnt[u];i++)
	{
		int v=edge[u][i];
		if (vis[v] || Del[u][v]) continue;
		dfs(v,u);
	}
}
void DFS(int u,int f)
{
	if (st || ed) return;
	if (!vis[u]) 
	{
		fa[u]=f;
	}
	else
	{
		st=u,ed=f;
		return;
	}
	vis[u]=1;
	for (int i=1;i<=cnt[u];i++)
	{
		int v=edge[u][i];
		if (v==f) continue;
		DFS(v,u);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=m;i++)
	{
		u=read(),v=read();
		edge[u][++cnt[u]]=v;
		edge[v][++cnt[v]]=u;
	}
	for (int i=1;i<=n;i++)
	{
		sort(edge[i]+1,edge[i]+cnt[i]+1);
	}
	if (n==m+1)
	{
		dfs(1,0);
		for (int i=1;i<tot;i++) printf("%d ",ans[i]);
		printf("%d\n",ans[tot]);
	}
	else
	{
		DFS(1,0);
		while (ed!=st)
		{
			delu[++Cnt]=fa[ed],delv[Cnt]=ed;
			ed=fa[ed];
		}
		for (int i=1;i<=n;i++) Ans[i]=n;
		for (int i=1;i<=Cnt;i++)
		{
			tot=0;
			Del[delu[i-1]][delv[i-1]]=0;
			Del[delv[i-1]][delu[i-1]]=0;
			Del[delu[i]][delv[i]]=1;
			Del[delv[i]][delu[i]]=1;
			for (int j=1;j<=n;j++) vis[j]=0;
			dfs(1,0);
			if (check())
			{
				for (int j=1;j<=n;j++)
					Ans[j]=ans[j];
			}
		}
		for (int i=1;i<n;i++) printf("%d ",Ans[i]);
		printf("%d\n",Ans[n]);
	}
	return 0;
}
