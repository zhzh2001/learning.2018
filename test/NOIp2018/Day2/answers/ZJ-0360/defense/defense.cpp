#include<bits/stdc++.h>
using namespace std;
struct node{
	int to,next;
}E[100005];
int f[100005][2],n,m,q,w,e,r,head[100005],tot,a[100005];
char s[100];
void add(int x,int y)
{
	E[++tot].to=y;
	E[tot].next=head[x];
	head[x]=tot;
}
void dfs(int t,int fa)
{
//	bool bx=1;
	int q0=0,q1=0;
	for (int i=head[t];i;i=E[i].next)
		if (E[i].to!=fa)
		{
//			bx=0;
			dfs(E[i].to,t);
			q0+=min(f[E[i].to][0],f[E[i].to][1]);
			q1+=f[E[i].to][1];
		}
//	if (bx)
	if (q0>q1)
		q0=q1;
	{
		if (t==q)
		{
			if (w==0)
				f[t][0]=q1,f[t][1]=1e9;
			else
				f[t][1]=q0+a[t],f[t][0]=1e9;
		}
		else
		if (t==e)
		{
			if (r==0)
				f[t][0]=q1,f[t][1]=1e9;
			else
				f[t][1]=q0+a[t],f[t][0]=1e9;
		}
		else
			f[t][0]=q1,f[t][1]=q0+a[t];
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%s",s+1);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	for (int i=1;i<n;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	if (n<=2000&&m<=2000)
	{
		while (m--)
		{
			scanf("%d%d%d%d",&q,&w,&e,&r);
			memset(f,0,sizeof(f));
			dfs(1,0);
			if (min(f[1][0],f[1][1])>=1e9)
				printf("-1\n");
			else
				printf("%d\n",min(f[1][0],f[1][1]));
		}
	}
	return 0;
}
