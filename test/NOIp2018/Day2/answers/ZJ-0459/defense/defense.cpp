#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <vector>
using namespace std;
typedef long long LL;
const int MAXN = 10000 + 10;


int n, m;
int a[MAXN];
int f[MAXN][2];
bool vis[MAXN];
vector <int> vec[MAXN];
int rec[MAXN], cnt, dgr[MAXN], pos[MAXN];

void DFS(int u, int fa)
{
	vis[u] = true;
		rec[++cnt] = u; pos[u] = cnt;
	for (int i = 0; i < vec[u].size(); i ++)
	{
		int v = vec[u][i];
		if (v == fa || vis[v]) continue;
		DFS(v, u);
	}
}

int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	string tmp;
	scanf("%d%d", &n, &m);
	cin >> tmp;
	for (int i = 1; i <= n; i ++) scanf("%d", &a[i]);
	for (int i = 1; i <= n - 1; i ++)
	{
		int u, v; scanf("%d%d", &u, &v);
		dgr[u] ++, dgr[v] ++;
		vec[u].push_back(v);
		vec[v].push_back(u);
	}
	int root = 0;
	for (int i = 1; i <= n; i++)
	{
		if (dgr[i] == 1) 
		{
			root = i;
			break;
		}
	}
	DFS(root, 0);
//	printf("%d\n", root);
//	for (int i = 1; i <= n; i ++) printf("%d ", rec[i]);
	while (m --)
	{
		int A, x, b, y;
		scanf("%d%d%d%d", &A, &x, &b, &y);
		if (pos[A] > pos[b])
		{
			swap(A, b);
		}
		if (n <= 2 || (n == 3 && )) 
		{
			printf("-1\n");
			continue;
		}
		memset(f, 0x3f, sizeof f);
		f[1][0] = 0; f[1][1] = a[rec[1]];
		for (int i = 1; i <= n; i ++)
		{
			if ((i == A && x == 0) || (i == b && y == 0) || (i != A && i != b)) f[i][0] = min(f[i][0], f[i - 1][1]);
			if ((i == A && x == 1) || (i == b && y == 1) || (i != A && i != b)) f[i][1] = min(f[i][1], f[i - 1][0] + a[rec[i]]);
		}
		printf("%d\n", min(f[n][0], f[n][1]));
	}
	return 0;
}
