#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <vector>
using namespace std;
typedef long long LL;
const int MAXN = 1000000 + 10;
const LL MOD = 1e9 + 7;

int n, m;
LL f[2][(1 << 8) + 1], ans;

bool is1(int state, int x)
{
	return state & (1 << (x - 1));
}
void DFS(int i, int x, int state, int nex)
{
	if (x == n + 1)
	{
		int nex1 = nex, nex2 = nex;
		if (is1(nex, n)) nex2 = nex ^ (1 << (n - 1));
		else nex1 = nex | (1 << (n - 1));
		(f[(i + 1) % 2][nex1] += f[i % 2][state]) %= MOD;
		(f[(i + 1) % 2][nex2] += f[i % 2][state]) %= MOD;
//		printf("%d --> %d %d %d\n", state, nex, nex1, nex2);
		return ;
	}
	if (is1(state, x)) 
	{
		if (is1(nex, x - 1)) DFS(i, x + 1, state, nex ^ (1 << (x - 2))), DFS(i, x + 1, state, nex);
		else DFS(i, x + 1, state, nex | (1 << (x - 2))), DFS(i, x + 1, state, nex);
	}
	else
	{
		if (is1(nex, x - 1)) DFS(i, x + 1, state, nex ^ (1 << (x - 2)));
		else DFS(i, x + 1, state, nex);
	} 
}

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int state = 0; state < (1 << n); state ++) f[1][state] = 1;
	for (int i = 1; i < m; i ++)
	{
		for (int state = 0; state < (1 << n); state ++)
		{
//			printf("f[%d][%d] = %lld\n", i, state, f[i % 2][state]);
			DFS(i, 2, state, state);
		}
		for (int state = 0; state < (1 << n); state ++) f[i % 2][state] = 0;
	}
//	for (int state = 0; state < (1 << n); state ++) printf("f[%d][%d] = %lld\n", m, state, f[m % 2][state]);
	for (int state = 0; state < (1 << n); state ++) (ans += f[m % 2][state]) %= MOD;
	printf("%lld\n", ans);
	return 0;
}
