#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <vector>
using namespace std;
typedef long long LL;
const int MAXN = 5000 + 10;

int n, m;
bool is[MAXN];
int order[MAXN];
int m1, m2;
vector <int> vec[MAXN];
bool vis[MAXN];
int circle[MAXN], cnt, top;

void DFS(int u, int fa)
{
	printf("%d ", u);
	vis[u] = true;
	for (int i = 0; i < vec[u].size(); i ++)
	{
		int v = vec[u][i];
		if (v == fa || vis[v] || ((u == m1 && v == m2) || (u == m2 && v == m1))) continue;
//		printf("%d-->%d\n", u, v);
		DFS(v, u);
	}
}

pair <int, int> stk[MAXN << 2];
void find(int u, int fa)
{
	vis[u] = true;
	if (cnt != 0) return;
	for (int i = 0; i < vec[u].size(); i ++)
	{
		int v = vec[u][i];
		if (v == fa) continue;
		stk[++top] = make_pair(u, v);
		if (vis[v])
		{
			while (top > 0)
			{
				circle[++cnt] = stk[top].first;
				top --;
				if (circle[cnt] == v) break;
			}
			return ;
		}
		find(v, u);
		stk[--top];
	}
}

int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; i ++)
	{
		int u, v; scanf("%d%d", &u, &v);
		vec[u].push_back(v);
		vec[v].push_back(u);
//		addedge(u, v);
//		addedge(v, u);
	}
	for (int i = 1; i <= n; i ++) sort(vec[i].begin(), vec[i].end());
	if (m == n - 1)
		DFS(1, 0);
	else 
	{
		find(1, 0);	
//		for (int i = 1; i <= cnt; i ++) printf("%d -", circle[i]);
//		printf("\n");
		for (int i = 1; i <= cnt; i ++) is[circle[i]] = true;
		order[1] = circle[cnt];
		int l = cnt - 1, r = 1;
		int tmp = 1;
		if (circle[l] < circle[r])
		{
			while (circle[l - 1] < circle[r] && l != r) l --;
			if (l == 2) m1 = circle[1], m2 = circle[cnt];
			else m1 = circle[l], m2 = circle[l - 1];
		}
		if (circle[l] > circle[r])
		{
			while (circle[l] > circle[r + 1] && l != r)  r ++;
			if (r == cnt - 2) m1 = circle[cnt - 1], m2 = circle[cnt];
			else m1 = circle[r], m2 = circle[r + 1];
		}
//		printf("(%d %d)\n", m1, m2);
//		printf("------------------\n");
		memset(vis, false, sizeof vis);
		DFS(1, 0);
	}	
	return 0;
}
