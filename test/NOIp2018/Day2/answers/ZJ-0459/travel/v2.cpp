#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <vector>
using namespace std;
typedef long long LL;
const int MAXN = 5000 + 10;

int n, m;
bool is[MAXN];
bool mark[MAXN][MAXN];
bool flag;
int order[MAXN];
int m1, m2;
vector <int> vec[MAXN];
bool vis[MAXN];
int circle[MAXN], cnt, top;

void DFS(int u, int fa)
{
	printf("%d ", u);
	vis[u] = true;
	for (int i = 0; i < vec[u].size(); i ++)
	{
		int v = vec[u][i];
		if (v == fa || vis[v] || (flag && mark[u][v])) continue;
		printf("%d --> %d\n", u, v);
		if (mark[u][v]) 
		{
			flag = true;
			return;		
		}
		DFS(v, u);
	}
}

pair <int, int> stk[MAXN << 2];
void find(int u, int fa)
{
	vis[u] = true;
	if (cnt != 0) return;
	for (int i = 0; i < vec[u].size(); i ++)
	{
		int v = vec[u][i];
		if (v == fa) continue;
		stk[++top] = make_pair(u, v);
		if (vis[v])
		{
			while (top > 0)
			{
				circle[++cnt] = stk[top].first;
				top --;
				if (circle[cnt] == v) break;
			}
			return ;
		}
		find(v, u);
		stk[--top];
	}
}

int main()
{
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; i ++)
	{
		int u, v; scanf("%d%d", &u, &v);
		vec[u].push_back(v);
		vec[v].push_back(u);
//		addedge(u, v);
//		addedge(v, u);
	}
	if (m == n - 1)
	{
		for (int i = 1; i <= n; i ++) sort(vec[i].begin(), vec[i].end());
		DFS(1, 0);
	} 
	else 
	{
		find(1, 0);	
//		for (int i = 1; i <= cnt; i ++) printf("%d -", circle[i]);
		/*
int l = cnt - 1, r = 1;
		if (circle[l] < circle[r])
		{
			while (circle[l - 1] < circle[r] && l != r) l --;
			vec[circle[l]].push_back(circle[1]);
		}
		if (circle[l] > circle[r])
		{
			while (circle[l] > circle[r + 1] && l != r) r ++;
			vec[circle[r]].push_back(circle[cnt - 1]);
		}
		*/
//		/*
		for (int i = 1; i < cnt - 1; i ++)
			vec[circle[i]].push_back(circle[cnt - 1]), mark[circle[i]][circle[cnt - 1]] = true;
		for (int i = 2; i <= cnt - 1; i ++)
			vec[circle[i]].push_back(circle[1]), mark[circle[i]][circle[1]] = true;
//			*/
//		vec[circle[1]].push_back(circle[cnt - 1]);
//		vec[circle[cnt - 1]].push_back(circle[1]);
//		printf("(%d, %d)\n", circle[1], circle[cnt - 1]);
		for (int i = 1; i <= n; i ++) sort(vec[i].begin(), vec[i].end());
		for (int i = 1; i <= n; i ++)
		{
//			for (int j = 0; j < vec[i].size(); j ++) printf("-->%d\n", i, vec[i][j]);
		}
		memset(vis, false, sizeof vis);
		DFS(1, 0);
	}	
	return 0;
}
/*
11 11
1 3
3 2
2 7
3 6
6 5
2 4
4 8
8 9
4 5
8 10
5 11
*/
