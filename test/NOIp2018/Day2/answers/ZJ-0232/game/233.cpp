#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
const int mo=1e9+7;
int n,m,f[10001][50],g[10001][50];
int main() {
    //freopen("game.in","r",stdin);
    //freopen("game.out","w",stdout);
    scanf("%d%d",&n,&m);
    for (int i=0;i<(1<<n);i++) f[i][n]=1;
    for (int i=2;i<=m;i++) {
        for (int j=0;j<(1<<n);j++)
            for (int k=1;k<=n;k++) g[j][k]=0;
        for (int j=0;j<(1<<n);j++) {            
            for (int k=1;k<=n;k++) {
                if (f[j][k]==0) continue;
                for (int k2=0;k2<(1<<n);k2++) {
                    bool judge=1;
                    for (int k3=1;k3<n;k3++) {
                        bool j1=(k2&(1<<(k3-1)));
                        bool j2=(j&(1<<k3));
                        if (j1>j2) {
                            judge=0;
                            break;
                        }
                        if (j1==j2&&k3>k) {
                            judge=0;
                            break;
                        }
                    }
                    if (!judge) continue;
                    int max0=1;
                    for (int k3=1;k3<n;k3++) {
                        bool j1=(k2&(1<<(k3-1)));
                        bool j2=(j&(1<<k3));
                        if (j1!=j2) break;
                        max0=k3+1;
                        if (k3+1>=k) break;
                    }
                    max0=min(max0,k);
                    g[k2][max0]=(g[k2][max0]+f[j][k])%mo;
                }
            }
        }
        for (int j=0;j<(1<<n);j++)
            for (int k=1;k<=n;k++) f[j][k]=g[j][k];
    }
    int ans=0;
    for (int i=0;i<(1<<n);i++)
        for (int j=1;j<=n;j++) ans=(ans+f[i][j])%mo;
    printf("%d\n",ans);
    return 0;
}
