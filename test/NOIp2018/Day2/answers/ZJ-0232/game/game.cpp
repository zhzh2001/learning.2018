#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
const int mo=1e9+7;
int n,m,d[100][100];
int f[10001][50],g[10001][50];
void Solve() {
    for (int i=0;i<(1<<n);i++) f[i][n]=1;
    for (int i=2;i<=m;i++) {
        for (int j=0;j<(1<<n);j++)
            for (int k=1;k<=n;k++) g[j][k]=0;
        for (int j=0;j<(1<<n);j++) {            
            for (int k=1;k<=n;k++) {
                if (f[j][k]==0) continue;
                for (int k2=0;k2<(1<<n);k2++) {
                    bool judge=1;
                    for (int k3=1;k3<n;k3++) {
                        bool j1=(k2&(1<<(k3-1)));
                        bool j2=(j&(1<<k3));
                        if (j1>j2) {
                            judge=0;
                            break;
                        }
                        if (j1==j2&&k3>k) {
                            judge=0;
                            break;
                        }
                    }
                    if (!judge) continue;
                    int max0=1;
                    for (int k3=1;k3<n;k3++) {
                        bool j1=(k2&(1<<(k3-1)));
                        bool j2=(j&(1<<k3));
                        if (j1!=j2) break;
                        max0=k3+1;
                        if (k3+1>=k) break;
                    }
                    max0=min(max0,k);
                    g[k2][max0]=(g[k2][max0]+f[j][k])%mo;
                }
            }
        }
        for (int j=0;j<(1<<n);j++)
            for (int k=1;k<=n;k++) f[j][k]=g[j][k];
    }
    int ans=0;
    for (int i=0;i<(1<<n);i++)
        for (int j=1;j<=n;j++) ans=(ans+f[i][j])%mo;
    printf("%d\n",ans);
}
int main() {
    freopen("game.in","r",stdin);
    freopen("game.out","w",stdout);
    int ans=0;
    scanf("%d%d",&n,&m);
    if (n==1) {
        int now=1;
        for (int i=1;i<=m;i++) now=now*2%mo;
        cout<<now<<endl;
        return 0;
    }
    if (n==2) {
        int now=4;
        for (int i=1;i<=m-1;i++) now=(now*2%mo+now)%mo;
        cout<<now<<endl;
        return 0;
    }
    if (n*m>20) {
        Solve();
        return 0;
    }
    for (int i=0;i<(1<<(n*m));i++) {
        bool judge=1;
        for (int j=1;j<=n;j++)
            for (int k=1;k<=m;k++) {
                int loc=(j-1)*m+k;
                if (i&(1<<(loc-1))) d[j][k]=1;
                else d[j][k]=0;
            }
        for (int j=1;j<n;j++) {
            for (int k=2;k<=m;k++) {
                if (d[j][k]<d[j+1][k-1]) {
                    judge=0;
                    break;
                }
                if (d[j][k]==d[j+1][k-1]) {
                    for (int p1=j+k+1;p1<=n+m;p1++) {
                        int lst=-1;
                        for (int p2=j+1;p2<=n;p2++) {
                            int p3=p1-p2;
                            if (p3<k||p3>m) continue;
                            if (lst==-1) lst=d[p2][p3];
                            if (lst!=d[p2][p3]) {
                                judge=0;
                                break;
                            }
                        }
                    }
                }
            }
            if (!judge) break;
        }
        ans+=judge;
    }
    cout<<ans<<endl;
    return 0;
}
