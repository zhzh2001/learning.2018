#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
const int M=1e5;
int n,m,head[M+10],ff[M+10],pa[M+10],fa[M+10],vx;
int ans[M+10],pp[M+10],v[M+10],tot=0;
struct edges {
    int next,num,loc;
}edge[M+10];
struct data {
    int u,v,num;
}e[M+10],ed[M+10];
void Add(int u,int v,int num) {
    edge[++tot].next=head[u];
    edge[tot].num=v;
    edge[tot].loc=num;
    head[u]=tot;
}
bool cmp(data p,data q) {
    return p.v>q.v;
}
void Dfs(int x,int fat) {
    ans[++vx]=x;
    for (int i=head[x];i!=-1;i=edge[i].next) {
        int kx=edge[i].num;
        if (kx==fat) continue;
        fa[kx]=x;
        pa[kx]=edge[i].loc;
        Dfs(kx,x);
    }
}
void Solve1() {
    memset(head,-1,sizeof(head));
    tot=0;
    for (int i=1;i<=m;i++) {
        int u,v;
        scanf("%d%d",&u,&v);
        e[i*2-1].u=u;
        e[i*2-1].v=v;
        e[i*2].u=v;
        e[i*2].v=u;
    }
    sort(e+1,e+m*2+1,cmp);
    for (int i=1;i<=m*2;i++)
        Add(e[i].u,e[i].v,233);
    vx=0;
    Dfs(1,-1);
    for (int i=1;i<=n;i++) printf("%d ",ans[i]);
    printf("\n");
}
int Getf(int x) {
    if (ff[x]==x) return x;
    ff[x]=Getf(ff[x]);
    return ff[x];
}
void Solve2() {
    for (int i=1;i<=n;i++) ff[i]=i;
    for (int i=1;i<=m;i++)
        scanf("%d%d",&ed[i].u,&ed[i].v);
    int nowk=0;
    for (int i=1;i<=m;i++) {
        int fu=Getf(ed[i].u),fv=Getf(ed[i].v);
        if (fu==fv) nowk=i;
        else ff[fu]=fv;
    }
    memset(head,-1,sizeof(head));
    tot=0;
    for (int i=1;i<=m;i++)
        if (i!=nowk) {
            Add(ed[i].u,ed[i].v,i);
            Add(ed[i].v,ed[i].u,i);
        }
    vx=0;
    Dfs(ed[nowk].u,-1);
    int px=ed[nowk].v;
    int cnt=0;
    while (px!=ed[nowk].u) {
        v[++cnt]=pa[px];
        px=fa[px];
    }
    v[++cnt]=nowk;
    for (int i=1;i<=n;i++) pp[i]=n+1-i;
    for (int i=1;i<=m;i++) {
        e[i*2-1].u=ed[i].u;
        e[i*2-1].v=ed[i].v;
        e[i*2].u=ed[i].v;
        e[i*2].v=ed[i].u;
        e[i*2-1].num=e[i*2].num=i;
    }
    sort(e+1,e+m*2+1,cmp);
    for (int i=1;i<=cnt;i++) {
        for (int j=1;j<=n;j++) head[j]=-1;
        tot=0;
        for (int j=1;j<=m*2;j++) {
            if (e[j].num==v[i]) continue;
            Add(e[j].u,e[j].v,j);
        }
        vx=0;
        Dfs(1,-1);
        bool judge=0;
        for (int j=1;j<=n;j++) {
            if (ans[j]>pp[j]) break;
            if (ans[j]<pp[j]) {
                judge=1;
                break;
            }
        }
        if (judge) for (int j=1;j<=n;j++) pp[j]=ans[j];
    }
    for (int i=1;i<=n;i++) printf("%d ",pp[i]);
    printf("\n");
}
int main() {
    freopen("travel.in","r",stdin);
    freopen("travel.out","w",stdout);
    scanf("%d%d",&n,&m);
    if (m==n-1) Solve1();
    else Solve2();
    return 0;
}
