#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
typedef long long LL;
const int N=1e6;
const LL INF=1e13;
int n,m,head[N+10],p[N+10],tot=0;
char s[100];
LL f[N+10][2];
struct data {
    int next,num;
}edge[N+10];
void Add(int u,int v) {
    edge[++tot].next=head[u];
    edge[tot].num=v;
    head[u]=tot;
}
void Dfs(int x,int fat,int ax,int v1,int bx,int v2) {
    for (int i=head[x];i!=-1;i=edge[i].next) {
        int kx=edge[i].num;
        if (kx==fat) continue;
        Dfs(kx,x,ax,v1,bx,v2);
    }
    f[x][0]=f[x][1]=0;
    for (int i=head[x];i!=-1;i=edge[i].next) {
        int kx=edge[i].num;
        if (kx==fat) continue;
        f[x][0]+=f[kx][1];
        if (f[x][0]>INF) f[x][0]=INF;
    }
    if (x==ax&&v1==1) f[x][0]=INF;
    if (x==bx&&v2==1) f[x][0]=INF;
    LL minx=INF;
    for (int i=head[x];i!=-1;i=edge[i].next) {
        int kx=edge[i].num;
        if (kx==fat) continue;
        f[x][1]+=min(f[kx][0],f[kx][1]);
        if (f[x][1]>=INF) f[x][1]=INF;
    }
    f[x][1]+=p[x];
    if (f[x][1]>=INF) f[x][1]=INF;
    if (x==ax&&v1==0) f[x][1]=INF;
    if (x==bx&&v2==0) f[x][1]=INF;
}
int main() {
    freopen("defense.in","r",stdin);
    freopen("defense.out","w",stdout);
    memset(head,-1,sizeof(head));
    scanf("%d%d%s",&n,&m,s);
    for (int i=1;i<=n;i++) scanf("%d",&p[i]);
    for (int i=1;i<n;i++) {
        int u,v;
        scanf("%d%d",&u,&v);
        Add(u,v);
        Add(v,u);
    }
    for (int i=1;i<=m;i++) {
        int ax,x,bx,y;
        scanf("%d%d%d%d",&ax,&x,&bx,&y);
        for (int j=1;j<=n;j++) f[j][0]=f[j][1]=0;
        Dfs(1,-1,ax,x,bx,y);
        LL ans=min(f[1][0],f[1][1]);
        if (ans==INF) printf("-1\n");
        else printf("%lld\n",ans);
    }
    return 0;
}
