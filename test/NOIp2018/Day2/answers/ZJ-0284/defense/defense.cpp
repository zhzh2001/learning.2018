#include<bits/stdc++.h>
using namespace std;
const int N=1e6+5;
const long long inf=1e17;
int n,m,v[N];long long sum[N],f[N][2],g[N][2];
char typ[15];int cnt,head[N],to[N<<1],nxt[N<<1];
void add(int u,int v){nxt[++cnt]=head[u],to[cnt]=v,head[u]=cnt;}
int read(){
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(;isdigit(ch);ch=getchar())x=(x<<3)+(x<<1)+ch-48;
	return x*f;
}
int get(int l,int r){
	if(l>=r)return 0;
	g[l][0]=0,g[l][1]=v[l];
	for(int i=l+1;i<=r;i++)
	g[i][1]=min(g[i-1][0],g[i-1][1])+v[i],g[i][0]=g[i-1][1];
	return min(g[r][0],g[r][1]);
}
void dfs(int x,int fat){
	int sons=0;g[x][0]=g[x][1]=1e17;
	for(int i=head[x];i;i=nxt[i])
	if(to[i]!=fat){
		sons++,dfs(to[i],x);
		g[x][0]=min(g[x][0],g[to[i]][1]);
		g[x][1]=min(min(g[to[i]][0],g[to[i]][1])+v[i],g[x][1]);
	}
	if(!sons)g[x][0]=1e17,g[x][1]=v[x];
	else if(x==1)g[x][0]=1e17;
}
void work(int x,int notreach,int fat){
	int sons=0;g[x][0]=0,g[x][1]=1e17;
	for(int i=head[x];i;i=nxt[i])
	if(to[i]!=fat&&to[i]!=notreach){
		sons++;work(to[i],notreach,x);
		g[x][0]+=g[to[i]][1];
		g[x][1]=min(min(g[to[i]][0],g[to[i]][1])+v[i],g[x][1]);	
	}
	if(!sons)g[x][0]=1e17,g[x][1]=v[x];
}
void do_all(int x,int fat,int dan,int b){
	int sons=0;g[x][0]=0,g[x][1]=1e17;
	for(int i=head[x];i;i=nxt[i])
	if(to[i]!=fat){
		sons++;do_all(to[i],x,dan,b);
		g[x][0]+=g[to[i]][1];
		g[x][1]=min(min(g[to[i]][0],g[to[i]][1])+v[i],g[x][1]);	
	}
	if(!sons)g[x][0]=1e17,g[x][1]=v[x];
	else if(x==dan)g[x][!b]=1e17;
}
int main(){
	freopen("defense.in","r",stdin);freopen("defense.out","w",stdout);
	n=read(),m=read();scanf("%s",typ);
	for(int i=1;i<=n;i++)v[i]=read();
	for(int i=1,x,y;i<n;i++)x=read(),y=read(),add(x,y),add(y,x);
	f[1][1]=v[1],f[1][0]=0;
	for(int i=2;i<=n;i++)
	f[i][1]=min(f[i-1][0],f[i-1][1])+v[i],f[i][0]=f[i-1][1];
	if(typ[0]=='A'){//line
		for(int i=1;i<=n;i++)sum[i]=sum[i-2]+v[i];
		for(int i=1,a,b,x,y;i<=m;i++){
			x=read(),a=read(),y=read(),b=read();
			if(x>y)swap(x,y),swap(a,b);
			long long ans=a*v[x]+b*v[y];int l1=x-1,r1=x+1,l2=y-1,r2=y+1;
			if(!a)ans+=v[l1--]+v[r1++];
			if(!b)ans+=v[l2--]+v[r2++];
			if(!a&&!b)
				if(x+2==y)ans-=v[x+1];
			if(!a&&y==x+1)ans-=v[y];
			if(!b&&y==x+1)ans-=v[x];
			ans+=get(1,l1)+get(r1,l2)+get(r2,n);
			printf("%lld\n",ans);
		}
		return 0;
	}
	if(typ[1]=='1'){//x=1 a=1
		for(int i=1,a,b,x,y;i<=m;i++){
			x=read(),a=read(),y=read(),b=read();
			dfs(y,0);printf("%lld\n",g[y][b]);
		}
	}
	if(typ[1]=='2'){//near
		for(int i=1,a,b,x,y;i<=m;i++){
			x=read(),a=read(),y=read(),b=read();
			if(!a&&!b)puts("-1");
			else{
				long long ans=0;
				work(x,y,0),ans+=g[x][a]%inf;
				work(y,x,0),ans+=g[y][b]%inf;
				printf("%lld\n",ans);
			}
		}
		return 0;
	}
	for(int i=1,a,b,x,y;i<=m;i++){
		x=read(),a=read(),y=read(),b=read();
		long long ans=0;
		do_all(x,0,y,b);ans+=g[x][a];
		do_all(y,0,x,a);ans+=g[y][b];
		printf("%lld\n",ans<1e17?ans:-1);
	}
	return 0;
}
