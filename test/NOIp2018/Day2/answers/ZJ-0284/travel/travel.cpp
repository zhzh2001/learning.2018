#include<bits/stdc++.h>
using namespace std;
const int N=5005;
int n,m,vis[N],dfn[N],low[N],tim,fa[N],s=0,t=0;vector<int>G[N];
int read(){
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(;isdigit(ch);ch=getchar())x=(x<<3)+(x<<1)+ch-48;
	return x*f;
}
void go(int x,int f){
	printf("%d ",x);
	if(!G[x].size())return;
	for(int i=0;i<G[x].size();i++)
	if(G[x][i]!=f)go(G[x][i],x);
}
void do_tree(){
	go(1,0);
	puts("");
}
bool all_two(){
	for(int i=1;i<=n;i++)if(G[i].size()>2)return 0;
	return 1;
}
void go_ahead(int x){
	vis[x]=1;printf("%d ",x);
	for(int i=0;i<G[x].size();i++)
	if(!vis[G[x][i]])go_ahead(G[x][i]);
}
void huan(int x,int maxn){
	vis[x]=1;printf("%d ",x);
	for(int i=0;i<G[x].size();i++)
	if(!vis[G[x][i]]){
		if(G[x][i]<maxn)huan(G[x][i],maxn);
		else {go_ahead(maxn);return;}
	}
}
void bigg(){
	vis[1]=1;printf("%d ",1);
	huan(min(G[1][0],G[1][1]),max(G[1][0],G[1][1]));
}
void one_side(int x){
	dfn[x]=++tim;
	if(vis[x]&&!s)s=x;vis[x]++;
	for(int i=0;i<G[x].size();i++)
	if(!dfn[G[x][i]])fa[G[x][i]]=x,one_side(G[x][i]);
	low[x]=++tim;
}
void zhaoazhao(int x,int f){
	if(vis[x])return;printf("%d ",x);vis[x]=1;
	for(int i=0;i<G[x].size();i++)
	if(f!=G[x][i]){
		if(G[x][i]==s)swap(s,t);
		if(G[x][i]!=t)zhaoazhao(G[x][i],x);
	}
}
void find_circle(int x){
	fa[x]=0;one_side(x);t=s;
	for(int i=0;i<G[t].size();i++)
	if(fa[t]!=G[t][i]){memset(vis,0,sizeof(vis));fa[G[t][i]]=0;one_side(G[t][i]);break;}
//	if(s==1||t==1)
//	else 
	memset(vis,0,sizeof(vis));vis[0]=1;
	zhaoazhao(1,0),zhaoazhao(t,0);
}
int main(){
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1,x,y;i<=m;i++)x=read(),y=read(),G[x].push_back(y),G[y].push_back(x);
	for(int i=1;i<=n;i++)sort(G[i].begin(),G[i].end());
	if(m==n-1){do_tree();return 0;}
	if(all_two()){bigg();return 0;}
	int wai;for(int i=1;i<=n;i++)if(G[i].size()==1){wai=i;break;}
	find_circle(wai);
	return 0;
}
