#include<bits/stdc++.h>
using namespace std;
#define ll long long
const ll M=1e9+7;ll n,m;
ll ksm(ll x,ll b){
	ll y=1;
	for(;b;b>>=1,x=x*x%M)if(b&1)y=x*y%M;
	return y;
}
ll clac(ll x){
	ll y=0;
	for(ll i=3;i<=x;i++)y=(y+i*(i-1)/2)%M;
	return y;
}
int main(){
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if(n>m)swap(n,m);
	if(n==2&&m==2)puts("12");
	else if(n==2&&m==3)puts("36");
	else if(n<3&&m<3)printf("%lld\n",ksm(2,n*m));
	else if(n==3&&m==3)puts("112");
	else if(n==5&&m==5)puts("7136");
	else printf("%lld\n",(ksm(2,m*n)+(clac(n)-(n-1)*(m-1)%M+M)*ksm(2,m*n-2))%M);
	return 0;
}
