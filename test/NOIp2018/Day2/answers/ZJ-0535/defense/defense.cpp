#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdlib>
using namespace std;
int N,Q;
char TYPE[3];
struct node{
	int c,v;
}	cost[100005];
int road[100005][2];
int mark[100005];
int x,x1,y,y1;
int cmp(node a,node b){
	return a.c<b.c;
}
void work(){
	scanf("%d%d%d%d",&x,&x1,&y,&y1);
	memset(mark,0,sizeof(mark));
	if(x==1&&x1==0||x==N&&x1==0||y==1&&y1==0||y==N&&y1==0){
		cout<<"-1"<<endl;
		return;
	}
	int ans=cost[x].c*x1+cost[y].c*y1;
	mark[x]=1;mark[y]=1;
	sort(cost+1,cost+N+1,cmp);
	for(int i=1;i<=N;i++){
		int z=cost[i].v;
		if(mark[z-1]==0&&mark[z+1]==0)ans+=cost[i].c,mark[z]=1;
	}
	printf("%d\n",ans);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&N,&Q);scanf("%s",TYPE+1);
	for(int i=1;i<=N;i++)scanf("%d",&cost[i].c);
	for(int i=1;i<N;i++)scanf("%d%d",&road[i][0],&road[i][1]);
	while(Q--)work();
	fclose(stdin);fclose(stdout);
	return 0;
}
