#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#define LL long long
using namespace std;
const LL oo=1000000000+7;
int N,M;
LL DP[2][1000005][2][2][2];
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&N,&M);
	DP[1][1][0][0][0]=DP[1][1][0][0][1]=1;
	for(int i=2;i<=M;i++){
		DP[1][i][0][0][0]=DP[1][i][0][0][1]=DP[1][i-1][0][0][1]*2%oo;
	}
	for(int i=0;i<=1;i++)for(int j=0;j<=1;j++)DP[0][0][i][j][1]=DP[0][0][i][j][0]=0;
	for(int i=0;i<=1;i++)
		for(int j=0;j<=1;j++)
			for(int k=0;k<=1;k++)
				for(int l=0;l<=1;l++)DP[i][0][j][k][l]=0;
	for(int i=2;i<=N;i++){
		int x=(i&1);
		for(int j=1;j<=M;j++)
			for(int k=0;k<=1;k++){
				DP[x][j][0][0][k]=(DP[!x][j][0][0][0]+DP[!x][j][0][1][0]+DP[!x][j][1][1][0]+DP[x][j-1][0][0][0]+DP[x][j-1][0][1][0]+DP[x][j-1][1][1][0])%oo;
				DP[x][j][0][1][k]=(DP[!x][j][0][0][0]+DP[!x][j][0][1][0]+DP[!x][j][1][1][0]+DP[x][j-1][0][0][1]+DP[x][j-1][0][1][1]+DP[x][j-1][1][1][1])%oo;
				DP[x][j][1][1][k]=(DP[!x][j][0][0][1]+DP[!x][j][0][1][1]+DP[!x][j][1][1][1]+DP[x][j-1][0][0][1]+DP[x][j-1][0][1][1]+DP[x][j-1][1][1][1])%oo;
			}
	}
	int x=(N&1);
	printf("%lld",(DP[x][M][0][0][0]+DP[x][M][0][1][0]+DP[x][M][1][1][0]+DP[x][M][0][0][1]+DP[x][M][0][1][1]+DP[x][M][1][1][1])%oo);
	fclose(stdin);fclose(stdout);
	return 0;
}
