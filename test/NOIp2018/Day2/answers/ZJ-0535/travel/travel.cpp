#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdlib>
using namespace std;
int N,M;
int flag;
int Head[5005];
struct ARForest{
	int Next,to;
}	E[10005];
int tot=0;
int Map[5005][5005];
int G2R_Result[5005],Temp[5005],mark[5005],g=0;
void add(int x,int y){
	tot++;
	E[tot].Next=Head[x];
	E[tot].to=y;
	Head[x]=tot;
}
void Dfs1(int x,int pre){
	int y;
	for(int i=Head[x];i!=-1;i=E[i].Next){
		y=E[i].to;
		if(y==pre)continue;
		Map[x][++Map[x][0]]=y;
		Dfs1(y,x);
	}
}
void Dfs2(int x,int pre){
	G2R_Result[++g]=x;	
	int y;
	for(int i=1;i<=Map[x][0];i++){
		y=Map[x][i];
		if(y==pre)continue;
		Dfs2(y,x);
	}
}
void Work1(){
	for(int i=1;i<=N;i++)Map[i][0]=0;
	Dfs1(1,-1);
	for(int i=1;i<=N;i++)if(Map[i][0])sort(Map[i]+1,Map[i]+Map[i][0]+1);
	Dfs2(1,-1);
	for(int i=1;i<N;i++)printf("%d ",G2R_Result[i]);printf("%d\n",G2R_Result[N]);
}
void Dfs3(int x,int pre,int a,int b){
	int y;mark[x]=1;
	for(int i=Head[x];i!=-1;i=E[i].Next){
		y=E[i].to;
		if(y==pre)continue;
		if(x==a&&y==b||x==b&&y==a)continue;
		if(mark[y]==1&&y!=pre)continue;
		Map[x][++Map[x][0]]=y;
		Dfs3(y,x,a,b);
	}
}
int Dfs4(int x,int pre){
	Temp[++g]=x;
	if(Temp[g]<G2R_Result[g])flag=2;
	if(Temp[g]>G2R_Result[g]&&flag!=2)return flag=0;
	int y,p;
	for(int i=1;i<=Map[x][0];i++){
		y=Map[x][i];
		if(y==pre)continue;
		p=Dfs4(y,x);
		if(p==0)return 0;
	}
	return flag;
}
int Work2(int x,int y){
	for(int i=1;i<=N;i++)Map[i][0]=mark[i]=0,Temp[i]=100005;g=0;
	Dfs3(1,-1,x,y);//for(int i=1;i<=N;i++){cout<<i<<":";for(int j=1;j<=Map[i][0];j++)printf("%d ",Map[i][j]);printf("\n");}
	for(int i=1;i<=N;i++)if(Map[i][0])sort(Map[i]+1,Map[i]+Map[i][0]+1);
	int k=Dfs4(1,-1);//for(int i=1;i<=N;i++)cout<<Temp[i]<<' ';cout<<endl;
	return k;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&N,&M);
	for(int i=1;i<=N;i++)Head[i]=-1;
	for(int i=1;i<=N;i++)G2R_Result[i]=100005;
	int x,y;
	for(int i=1;i<=M;i++){
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	if(M==N-1)Work1();else{
		for(int i=1;i<=N;i++)
			for(int j=Head[i];j!=-1;j=E[j].Next){
				flag=1;
				if(Work2(i,E[j].to)==2){
					for(int i=1;i<=N;i++)G2R_Result[i]=Temp[i];
				}				
			}
		for(int i=1;i<N;i++)printf("%d ",G2R_Result[i]);printf("%d\n",G2R_Result[N]);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
