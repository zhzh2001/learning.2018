#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
const int N=5005;
int n,m,ans[N],a[N][N],v[N+N],Next[N+N],head[N],e,fa[N],num,F,ansh[N],numh,deep[N],Q[2][N],BB[N],fl[N],vis[N];
void add(int a,int b){
	v[++e]=b; Next[e]=head[a]; head[a]=e;
}
void dfs(int u){
	for (int i=head[u];i;i=Next[i])
	if (v[i]!=fa[u]){ fa[v[i]]=u; a[u][++a[u][0]]=v[i]; deep[v[i]]=deep[u]+1; dfs(v[i]); }
	sort(a[u]+1,a[u]+a[u][0]+1);
}
void dfs_1(int u){
	ans[++num]=u;
	for (int i=1;i<=a[u][0];i++) dfs_1(a[u][i]);
}
void solve1(){
	dfs_1(1);
	for (int i=1;i<=num;i++) printf("%d ",ans[i]);
}
void dfs_2(int u){
	if (u==F) {
		for (int i=1;i<=numh;i++) ans[++num]=ansh[i];
		return ;
	}
	ans[++num]=u;
	for (int i=1;i<=a[u][0];i++) dfs_2(a[u][i]);
}
void dfs_3(int u){
	ansh[++numh]=u;
	for (int i=1;i<=a[u][0];i++) dfs_3(a[u][i]);
}
/*void solve2(){
	int x,y; scanf("%d%d",&x,&y);
	if (deep[x]<deep[y]) swap(x,y);
	while (deep[x]>deep[y]) Q[0][++Q[0][0]]=x,,fl[x]=1,x=fa[x];
	while (x!=y) Q[0][++Q[0][0]]=x,fl[x]=1,x=fa[x],Q[1][++Q[1][0]]=y,fl[y]=1,y=fa[y];
	ansh[++numh]=x;
	for (int i=1;i<=a[x][0];i++){
		if (fl[a[x][i]]) break;
		else vis[a[x][j]]=1,dfs_3(a[x][j]);
	}
	if (Q[1][Q[1][0]]>Q[0][Q[0][0]]){
		for (int i=Q[0][0];i>=1;i--)
			BB[++BB[0]]=Q[0][i];
		for (int i=1;i<=Q[0][0];i++)
			BB[++BB[0]]=Q[1][i];
	} else {
		for (int i=Q[0][0];i>=1;i--)
			BB[++BB[0]]=Q[0][i];
		for (int i=1;i<=Q[0][0];i++)
			BB[++BB[0]]=Q[1][i];
	}
	int GG=BB[BB[0]],flag=0;
	for (int i=1;i<=BB[0];i++){
		if (BB[i]>GG) {
			int u=BB[i-1];
			for (int j=1;j<=a[u][0];j++)
				if (a[u][j]<GG && !fl[a[u][j]]) vis[a[u][j]]=1,dfs_3(a[u][j]);
			for (int j=i-2;j>=1;j--){
				int u=BB[j];
				for (int j=1;j<=a[u][0];j++)
				if (!vis[a[u][j]] && !fl[a[u][j]]) dfs_3(a[u][j]);
			}
			for (int j=BB[i];j>=i-2;j--){
				int u=BB[j];
				for (int j=1;j<=a[u][0];j++)
				if (!vis[a[u][j]] && !fl[a[u][j]]) dfs_3(a[u][j]);
			}
			flag=1; break;
		} else {
			int u=BB[i-1];
			for (int j=1;j<=a[u][0];j++)
				if (a[u][j]<BB[i] && !fl[a[u][j]]) vis[a[u][j]]=1,dfs_3(a[u][j]);
			
		}
	}
	if (!flag){
		for (int j=BB[BB[0]];j>=1;j--){
				int u=BB[j];
				for (int j=1;j<=a[u][0];j++)
				if (!vis[a[u][j]] && !fl[a[u][j]]) dfs_3(a[u][j]);
			}
	}
	dfs_2(1);
	for (int i=1;i<=num;i++) printf("%d ",ans[i]);
}*/
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,x,y;i<n;i++) {
		scanf("%d%d",&x,&y);
		add(x,y); add(y,x);
	}
	dfs(1);
	if (m==n-1) solve1();
	//else solve2();
	return 0;
}
