#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
const int N=5005;
int v[N+N],Next[N+N],head[N],c[N],e,a,b,x,y;
long long dp[N][3];
void add(int a,int b){
	v[++e]=b; Next[e]=head[a]; head[a]=e;
}
void dfs(int u,int f){
	long long Min=(long long)1e09,fl=0; dp[u][1]=c[u],dp[u][3]=0;
	for (int i=head[u];i;i=Next[i])
	if (v[i]!=f){
		dfs(v[i],u); dp[u][1]=dp[u][1]+min(dp[v[i]][2],min(dp[v[i]][0],dp[v[i]][1])); dp[u][2]=dp[u][2]+dp[v[i]][0];
		if (dp[v[i]][1]<=dp[v[i]][0]) dp[u][0]+=dp[v[i]][1],fl=1;
		else dp[u][0]+=dp[v[i]][0];
	}
	if (u==a){
		if (x==1) dp[u][0]=dp[u][2]=(long long)1e09;
		if (x==0) dp[u][1]=(long long)1e09;
	}
	if (u==b){
		if (y==1) dp[u][0]=dp[u][2]=(long long)1e09;
		if (y==0) dp[u][1]=(long long)1e09;
	}
	dp[u][1]=min(dp[u][1],(long long)1e09);
	dp[u][3]=min(dp[u][3],(long long)1e09);
	if (fl) return ;
	for (int i=head[u];i;i=Next[i])
	if (v[i]!=f){
		Min=min(Min,dp[u][0]-dp[v[i]][0]+dp[v[i]][1]);
	}
	dp[u][0]=Min;
	if (u==a){
		if (x==1) dp[u][0]=dp[u][2]=1e09;
		if (x==0) dp[u][1]=1e09;
	}
	if (u==b){
		if (y==1) dp[u][0]=dp[u][2]=1e09;
		if (y==0) dp[u][1]=1e09;
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m; char ch[5]; scanf("%d%d%s",&n,&m,ch);
	for (int i=1;i<=n;i++) scanf("%d",&c[i]);
	for (int i=1;i<n;i++){
		scanf("%d%d",&a,&b);
		add(a,b); add(b,a);
	}
	for (int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		int fl=0;
		for (int i=head[a];i;i=Next[i])
			if (v[i]==b && x==0 && y==0) fl=1;
		if (fl){
			puts("-1"); continue;
		}
		dfs(1,0);
		printf("%lld\n",min(dp[1][0],dp[1][1]));
	}
	return 0;
}
