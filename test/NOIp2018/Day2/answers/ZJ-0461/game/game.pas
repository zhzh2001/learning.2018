var
n,m,i:longint;
s,t,p:int64;
begin
assign(input,'game.in'); reset(input);
assign(output,'game.out'); rewrite(output);
readln(n,m);
s:=1;
for i:=1 to m+n do s:=s*2;
t:=1;
for i:=1 to m do t:=t*2;
p:=1;
for i:=1 to n-2 do p:=p*2;
writeln(((s-t)*p) mod 1000000007);
close(input);
close(output);
end.