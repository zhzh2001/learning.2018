#include <cstdio>
#include <algorithm>
#define ll long long
using namespace std;const ll inf=1e11,N=1e5+5;
int n,m,p[N],a,X,b,Y,H[N],to[N<<1],nxt[N<<1],tot;ll dp[N][2];
void Add(int u,int v){nxt[++tot]=H[u],to[tot]=v,H[u]=tot,nxt[++tot]=H[v],to[tot]=u,H[v]=tot;}
void dfs(int x,int fa)
{
	int v;dp[x][0]=0;dp[x][1]=p[x];
	for (int i=H[x];i;i=nxt[i])
	if (to[i]!=fa)
	{
		dfs(to[i],x);v=to[i];
		dp[x][1]+=min(dp[v][0],dp[v][1]);
		dp[x][0]+=dp[v][1];
	}
	if (a==x) 
	{
		if (!X) dp[x][1]=inf;
		else dp[x][0]=inf;
	}
	if (b==x) 
	{
		if (!Y) dp[x][1]=inf;
		else dp[x][0]=inf;
	}
}
int main()
{
	freopen("defense.in","r",stdin);freopen("defense.out","w",stdout);
	int i;scanf("%d%d",&n,&m);char c1=getchar(),c2=getchar(),c3=getchar();
	for(i=1;i<=n;i++)scanf("%d",&p[i]);int u,v;for(i=1;i<n;i++)scanf("%d%d",&u,&v),Add(u,v);
	for(i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&a,&X,&b,&Y);dfs(1,0);ll ans=min(dp[1][0],dp[1][1]);
		if (ans<inf) printf("%lld\n",ans);else puts("-1");
	}return 0;
}
