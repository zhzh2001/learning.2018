#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;const int N=5005;
vector<int>a[N];int n,m,ans[N],o;
bool vis[N];int t1,t2,pa[N],tmp[N],l;
void dfs(int x,int fa)
{
	sort(a[x].begin(),a[x].end());printf("%d ",x);
	for(int i=0;i<a[x].size();i++)if(a[x][i]!=fa)dfs(a[x][i],x);
}
void find(int x,int fa)
{
	vis[x]=1;sort(a[x].begin(),a[x].end());pa[x]=fa;
	for(int i=0;i<a[x].size();i++)if(a[x][i]!=fa)
	{
		if(vis[a[x][i]])t1=x,t2=a[x][i];else find(a[x][i],x);
	}
}
bool f;
void dfs2(int x,int fa)
{
	vis[x]=1;ans[++o]=x;if (x==t1) f=1;if (vis[t2]) f=0;
	for(int i=0;i<a[x].size();i++)if(!vis[a[x][i]])
	{
		if (f&&a[x][i]>t2) {f=0;return;}
		if (x==t1&&a[x][i]==t2) f=0;dfs2(a[x][i],x);
	}
}
int main()
{
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	int i,j,x,y;scanf("%d%d",&n,&m);for(i=1;i<=m;i++)scanf("%d%d",&x,&y),a[x].push_back(y),a[y].push_back(x);
	if(m==n-1)dfs(1,0);else{find(1,0);for(i=1;i<=n;i++)vis[i]=0;dfs2(1,0);for(i=1;i<=o;i++)printf("%d ",ans[i]);}return 0;
}
