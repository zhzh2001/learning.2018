#include <bits/stdc++.h>
using namespace std;
const int N = 5555, M = 11111;
int  n, m, u, v, edgenum, Head[N], Next[M], vet[M], seq[N], len, now[N][N], perm[N], sub, cnt[N];
bool vis[N], f[N], flag;
inline void add(int u, int v) {
	Next[++edgenum] = Head[u];
	Head[u] = edgenum;
	vet[edgenum] = v;
}
/*void Tarjan(int u, int fa) {
	dfn[u] = low[u] = ++TIME;
	vis[u] = 1, stk[++top] = u;
	for (int e=Head[u]; e; e=Next[e]) {
		int v=vet[e];
		if (v == fa) continue;
		if (!vis[v]) Tarjan(v, u), low[u] = min(low[u], low[v]);
		else low[u] = min(low[u], dfn[v]);
	} if (low[u] == dfn[u]) {
		if (stk[top] == u) top--;
		else do inCycle[stk[top]] = 1;
			while (stk[top--] ^ u);
	}
}*/
void dfs(int u) {
	seq[++len] = u;
	vis[u] = 1;
	for (int i=1; now[u][i]; i++)
		if (!vis[now[u][i]])
			dfs(now[u][i]);
}
void dfs2(int u) {
	vis[u] = 1;
	while (1) {
		int pos = lower_bound(now[u]+1, now[u]+1+cnt[u], perm[sub+1]) - now[u];
		if (now[u][pos] ^ perm[sub+1]) break;
		++sub, dfs2(now[u][pos]);
	}
}
void getPerm(int t) {
	if (t > n) {
		for (int i=1; i<=n; i++)
			vis[i] = 0;
		sub = 1, dfs2(1);
		if (sub == n) {
			for (int i=1; i<=n; i++)
				printf ("%d ", perm[i]);
			flag = 1;
		} return ;
	}
	for (int i=2; i<=n; i++)
		if (!f[i]) {
			f[i] = 1, perm[t] = i, 
			getPerm(t+1), f[i] = 0;
			if (flag) return ;
		}
}
int main() {
	freopen ("travel.in", "r", stdin);
	freopen ("travel.out", "w", stdout);
	scanf ("%d%d", &n, &m);
	for (int i=1; i<=m; i++)
		scanf ("%d%d", &u, &v), 
		add(u, v), add(v, u);
	for (int i=1; i<=n; i++) {
		int *nowu = now[i];
		for (int e=Head[i]; e; e=Next[e])
			nowu[++cnt[i]] = vet[e];
		sort(nowu+1, nowu+1+cnt[i]);
	}
	if (m == n-1) {
		dfs(1);
		for (int i=1; i<=n; i++)
			printf ("%d ", seq[i]);
		return 0;
	}
	if (n <= 20)
		perm[1] = 1, getPerm(2);
	else {
		int nxt;
		for (int i=1; ; ) {
			vis[i] = 1;
			printf ("%d ", i);
			if (vis[now[i][1]] && vis[now[i][2]])
				return 0;
			if (vis[now[i][1]])
				nxt = now[i][2];
			else nxt = now[i][1];
			if (now[1][2] < nxt) break;
			i = nxt;
		}
		for (int i=now[1][2]; i; ) {
			vis[i] = 1;
			printf ("%d ", i);
			if (vis[now[i][1]] && vis[now[i][2]])
				return 0;
			if (vis[now[i][1]])
				nxt = now[i][2];
			else nxt = now[i][1];
			i = nxt;
		}
	}
	return 0;
}
