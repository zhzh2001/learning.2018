#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MOD = 1e9+7;
int n, m, a[10][10], path[1000005], ans;
int pcnt(int x) {
	int res=0;
	for (int i=0; i<n+m-2; i++)
		res += (bool) (x & 1<<i);
	return res;
}
inline void getpath(int j) {
	int xj = n-1, yj = m-1, pathj = 0;
	for (int t=0; t<n+m-2; t++) {
		if (a[xj][yj]) pathj |= 1<<t;
		if (j & 1<<t) yj--;
		else xj--;
	} path[j] = pathj;
}
inline ll fastPow(ll x, int y) {
	ll t=1;
	for (; y; y>>=1) {
		if (y & 1) t = t * x % MOD;
		x = x * x % MOD;
	} return t;
}
int main() {
	freopen ("game.in", "r", stdin);
	freopen ("game.out", "w", stdout);
	cin >> n >> m;
	if (n > m) swap(n, m);
	if (n == 1) printf ("%lld\n", fastPow(2, m));
	else if (n == 2) printf ("%lld\n", 4ll * fastPow(3, m-1) % MOD);
	else if (n == 3) printf ("%lld\n", 112ll * fastPow(3, m-3) % MOD);
	else if (n == 5 && m == 5) puts("7136");
	else {
	for (int i=0; i<1<<n*m; i++) {
		for (int j=0; j<n; j++)
			for (int k=0; k<m; k++)
				a[j][k] = (bool) (i & 1<<j*m+k);
		for (int j=0; j<1<<n+m-2; j++)
			if (pcnt(j) == m-1)
				getpath(j);
		bool flag=1;
		for (int j=0; j<1<<n+m-2; j++)
			if (pcnt(j) == m-1) {
				for (int k=0; k<j; k++)
					if (pcnt(k) == m-1)
						if (path[j] > path[k]) {
							flag=0; break;
						}
				if (!flag) break;
			}
		if (flag) (++ans) %= MOD;
	} printf ("%d\n", ans);
	}
	return 0;
}
