#include <cstdio>
#include <iostream>
#include <map>
using namespace std;
typedef pair <int, int> pii;
typedef long long ll;
const int N = 1e5+5, M = 2e5+5;
const ll INF = 1ll << 60;
int n, m, p[N], u, v, edgenum, Head[N], Next[M], vet[M];
map <pii, bool> Map; char Type[233];
inline int read() {
	int s=0, w=1;
	char ch=getchar();
	while (ch<'0' || ch>'9') {
		if (ch == '-') w = -w;
		ch=getchar();
	}
	while (ch>='0' && ch<='9')
		s = s*10 + ch-'0', ch=getchar();
	return s*w;
}
inline void add(int u, int v) {
	Next[++edgenum] = Head[u];
	Head[u] = edgenum;
	vet[edgenum] = v;
}
namespace task1 {
	int a, x, b, y, father[N], dep[N];
	ll dp[N][2], f[N][2];
	void dfs1(int u, int fa) {
		dep[u] = dep[fa] + 1;
		father[u] = fa;
		for (int e=Head[u]; e; e=Next[e]) {
			int v=vet[e];
			if (v == fa) continue;
			dfs1(v, u);
			dp[u][0] += dp[v][1], 
			dp[u][1] += min(dp[v][0], dp[v][1]);
		} dp[u][1] += p[u];
		f[u][0] = dp[u][0], f[u][1] = dp[u][1];
	}
	void dfs2(int u) {
		int fu = father[u];
		if (!fu) return ;
		ll ffu0 = 0, ffu1 = 0;
		for (int e=Head[fu]; e; e=Next[e]) {
			int v=vet[e];
			if (v == father[fu]) continue;
			ffu0 += f[v][1], 
			ffu1 += min(f[v][0], f[v][1]);
		} ffu1 += p[fu];
		if (f[fu][0] ^ INF) f[fu][0] = ffu0;
		if (f[fu][1] ^ INF) f[fu][1] = ffu1;
		dfs2(fu);
	}
	inline void solve() {
		dfs1(1, 0);
		while (m--) {
			scanf ("%d%d%d%d", &a, &x, &b, &y);
			if (Map[(pii) {a, b}] && !x && !y) {
				puts("-1"); continue;}
			f[a][x^1] = INF, f[b][y^1] = INF;
			if (dep[a] >= dep[b])
				dfs2(a), dfs2(b);
			else dfs2(b), dfs2(a);
			printf ("%lld\n", min(f[1][0], f[1][1]));
			for (; a; a=father[a])
				f[a][0] = dp[a][0], f[a][1] = dp[a][1];
			for (; b; b=father[b])
				f[b][0] = dp[b][0], f[b][1] = dp[b][1];
		}
	}
}
int main() {
	freopen ("defense.in", "r", stdin);
	freopen ("defense.out", "w", stdout);
	n = read(), m = read(), scanf ("%s", Type);
	for (int i=1; i<=n; i++)
		p[i] = read();
	for (int i=1; i<n; i++)
		u = read(), v = read(), add(u, v), add(v, u), 
		Map[(pii) {u, v}] = Map[(pii) {v, u}] = 1;
//	if (n <= 1000 && m <= 1000 || Type[0] == 'B')
		task1:: solve();
	return 0;
}
