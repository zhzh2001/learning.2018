#include<bits/stdc++.h>
using namespace std;

#define lg long long
#define mp make_pair
#define pb push_back
#define pii pair<int,int>
#define ft first
#define sd second
#define db double
template<class T>
void read(T& d){
	int f=1;char ch=getchar();
	for(d=0;ch>'9'||ch<'0';ch=getchar())if(ch=='-')f*=-1;
	for(;ch<='9'&&ch>='0';ch=getchar())d=d*10+ch-'0';d*=f;
}
/******************************************************/
char TP[3];
int n,m;
namespace S2{
	lg p[2005];
	int fr[5005],nex[5005],vi[5005],tot=0;
	lg dp[2][2005];
	void add(int x,int y){
		nex[++tot]=fr[x];fr[x]=tot;vi[tot]=y;
	}
	void dfs(int x,int fa){
		dp[1][x]=p[x];
		for(int i=fr[x];i;i=nex[i])if(vi[i]!=fa){
			dfs(vi[i],x);
			dp[1][x]+=min(dp[1][vi[i]],dp[0][vi[i]]);
			dp[0][x]+=dp[1][vi[i]];
		}
	}
	lg get(){
		for(int i=0;i<=n;++i)dp[0][i]=dp[1][i]=0;
		dfs(1,1);
		return min(dp[1][1],dp[0][1]);
	}
	void main(){
		for(int i=1;i<=n;++i){
			read(p[i]);
		}
		for(int i=1,x,y;i<n;++i){
			read(x);read(y);
			add(x,y);add(y,x);
		}
		for(int i=1,x1,t1,x2,t2;i<=m;++i){
			read(x1);read(t1);read(x2);read(t2);
			lg H1=p[x1];
			lg H2=p[x2];
			lg R=0;
			if(t1)p[x1]=0,R+=H1;else p[x1]=0x3f3f3f3f3f3f3f3f;
			if(t2)p[x2]=0,R+=H2;else p[x2]=0x3f3f3f3f3f3f3f3f;
			R+=get();
			if(R>=0x3f3f3f3f3f3f3f3f)puts("-1");
			else printf("%lld\n",R);
			p[x1]=H1;p[x2]=H2;
		}
	}
}
#define MN 200055
namespace S1{
	lg p[MN];
	lg dp[6][MN];
	void main(){
		for(int i=1;i<=n;++i)read(p[i]);
		for(int i=1,x,y;i<n;++i){
			read(x);read(y);
		}
		dp[1][0]=0;
		for(int i=1;i<=n;++i){
			dp[0][i]=dp[1][i-1];
			dp[1][i]=min(dp[1][i-1],dp[0][i-1])+p[i];
		}
		dp[2][1]=0x3f3f3f3f3f3f3f3f;
		dp[3][1]=0;
		for(int i=2;i<=n;++i){
			dp[2][i]=dp[3][i-1];
			dp[3][i]=min(dp[3][i-1],dp[2][i-1])+p[i];
		}
		dp[5][n+1]=0;
		for(int i=n;i;--i){
			dp[4][i]=dp[5][i+1];
			dp[5][i]=min(dp[5][i+1],dp[4][i+1])+p[i];
		}
		for(int i=1,x1,x2,t1,t2;i<=m;++i){
			read(x1);read(t1);read(x2);read(t2);
			if(abs(x2-x1)==1&&t1==0&&t2==0){
				puts("-1");continue;
			}
			if(TP[1]=='1'){
				if(t2==1)printf("%lld\n",dp[3][x2]+dp[5][x2]-p[x2]+p[1]);
				else printf("%lld\n",dp[3][x2-1]+dp[5][x2+1]+p[1]);
			}else{
				if(x1>x2)swap(x1,x2),swap(t1,t2);
				if(t2&&t1)printf("%lld\n",dp[1][x1]+dp[5][x2]);
				if(!t2&&t1)printf("%lld\n",dp[1][x1]+dp[5][x2+1]);
				if(!t1&&t2)printf("%lld\n",dp[1][x1-1]+dp[5][x2]);
			}
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n);read(m);scanf("%s",TP);
	if(TP[0]=='A')S1::main();
	else S2::main();
	return 0;
}
