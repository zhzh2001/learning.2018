#include<bits/stdc++.h>
using namespace std;

#define lg long long
#define mp make_pair
#define pb push_back
#define pii pair<int,int>
#define ft first
#define sd second
#define db double
template<class T>
void read(T& d){
	int f=1;char ch=getchar();
	for(d=0;ch>'9'||ch<'0';ch=getchar())if(ch=='-')f*=-1;
	for(;ch<='9'&&ch>='0';ch=getchar())d=d*10+ch-'0';d*=f;
}
/******************************************************/
#define Md 1000000007
#define MN 1000005
int m,n;
int P2[MN],P3[MN];
lg Get3(int m){
	lg res=P3[m-3]*64%Md,h=4;
	for(int i=3;i<m;++i){
		res+=h*h*2*P3[m-i]%Md;
		res%=Md;
	}
	res+=h*h*3;
	res%=Md;
	return res;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n);read(m);
	P2[0]=P3[0]=1;
	for(int i=1;i<=m;++i){
		P2[i]=P2[i-1]*2%Md;
		P3[i]=P3[i-1]*3%Md;
	}
	if(m<n)swap(n,m);
	if(n==1){
		lg res=1;
		for(int i=1;i<=m;++i)res=res*2%Md;
		printf("%lld\n",res);
	}
	if(n==2){
		lg res=1;
		for(int i=1;i<m;++i)res=res*3%Md;
		res=res*4%Md;
		printf("%lld\n",res);
	}
	if(n==3){
		printf("%lld",Get3(m));
	}
	return 0;
}
