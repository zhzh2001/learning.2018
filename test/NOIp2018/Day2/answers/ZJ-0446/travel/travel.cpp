#include<bits/stdc++.h>
using namespace std;

#define lg long long
#define mp make_pair
#define pb push_back
#define pii pair<int,int>
#define ft first
#define sd second
#define db double
template<class T>
void read(T& d){
	int f=1;char ch=getchar();
	for(d=0;ch>'9'||ch<'0';ch=getchar())if(ch=='-')f*=-1;
	for(;ch<='9'&&ch>='0';ch=getchar())d=d*10+ch-'0';d*=f;
}
/******************************************************/
#define MN 5005

int n,m;
vector<pii> tr[MN];
int dfn[MN],dfc=0;
int cl[MN];
int ans[MN];
void add(int x,int y,int id){
	tr[x].pb(mp(y,id));
	tr[y].pb(mp(x,id));
}
void ckans(){
	for(int i=1;i<=n;++i){
		if(dfn[i]>ans[i])return;
		if(dfn[i]<ans[i]){
			for(int j=1;j<=n;++j){
				ans[j]=dfn[j];
			}
			return;
		}
	}
}
void dfs(int x,int id){
	dfn[++dfc]=x;cl[x]=id;
	for(unsigned i=0;i<tr[x].size();++i){
		if(cl[tr[x][i].ft]!=id&&tr[x][i].sd!=id){
			dfs(tr[x][i].ft,id);
		}
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);read(m);
	for(int i=1,x,y;i<=m;++i){
		read(x);read(y);
		add(x,y,i);add(y,x,i);
	}
	for(int i=1;i<=n;++i)sort(tr[i].begin(),tr[i].end());
	if(m==n-1){
		dfs(1,n);
		for(int i=1;i<=n;++i){
			printf("%d ",dfn[i]);
		}
	}else{
		ans[1]=n+1;
		for(int i=1;i<=m;++i){
			dfc=0;
			dfs(1,i);
			if(dfc==n)ckans();
		}
		for(int i=1;i<=n;++i)printf("%d ",ans[i]);
	}
	return 0;
}
