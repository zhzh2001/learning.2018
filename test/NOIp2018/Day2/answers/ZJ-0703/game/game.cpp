#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 1000005
using namespace std;
typedef long long LL;
const LL p=1e9+7;
int n,m,ans,a[8][8]; LL f[N][2];
LL ksm(int b){
	LL a=2,ret=1;
	for (;b;a=a*a%p,b>>=1)
		if (b&1) ret=ret*a%p;
	return ret;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1||m==1) return printf("%lld\n",ksm(n*m)),0;
	if (n==2){
		ans=(p+ksm(2*m)-ksm(m+1))%p;
		return printf("%d\n",ans),0;
	}
	if (n==3&&m==3) return puts("112"),0;
	if (n==3&&m==2) return puts("40"),0;
	return 0;
}
