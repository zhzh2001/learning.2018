#include<cctype>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 100005
#define F inline
using namespace std;
typedef long long LL;
struct edge{ int nxt,to; }ed[N<<1];
int n,m,k,h[N],p[N],fa[N],dep[N];
LL f[N][2],g[N][2];
char ss[N];
#define add(x,y) ed[++k]=(edge){h[x],y},h[x]=k
F void dd1(){
	LL s1[N],s2[N]; s1[0]=s2[0]=0;
	for (int i=1;i<=n;i++){
		s1[i]=s1[i-1],s2[i]=s2[i-1];
		if (i&1) s1[i]+=p[i];
		else s2[i]+=p[i];
	}
	while (m--){
		
	}
}
void dfs(int x){
	dep[x]=dep[fa[x]]+1,f[x][0]=0,f[x][1]=p[x];
	for (int i=h[x],v;i;i=ed[i].nxt){
		if ((v=ed[i].to)==fa[x]) continue;
		fa[v]=x,dfs(v),f[x][0]+=f[v][1];
		f[x][1]+=min(f[v][0],f[v][1]);
	}
}
F void cal(int x,int y){
	int ff=fa[x]; g[ff][0]=0,g[ff][1]=p[ff];
	for (int i=h[ff],v;i;i=ed[i].nxt){
		if ((v=ed[i].to)==fa[ff]) continue;
		if (v==x||v==y)
			g[ff][0]+=g[v][1],g[ff][1]+=min(g[v][0],g[v][1]);
		else g[ff][0]+=f[v][1],g[ff][1]+=min(f[v][0],f[v][1]);
	}
}
F void calc(int x,int a,int y,int b){
	g[x][a]=f[x][a],g[x][a^1]=1e9;
	g[y][b]=f[y][b],g[y][b^1]=1e9;
	if (dep[x]<dep[y]) swap(x,y);
	for (;dep[x]!=dep[y];x=fa[x]) cal(x,0);
	for (;x!=y;x=fa[x],y=fa[y]) cal(x,y),cal(y,x);
	for (;x!=1;x=fa[x]) cal(x,0);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,ss);
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1,x,y;i<n;i++)
		scanf("%d%d",&x,&y),add(x,y),add(y,x);
	for (dfs(1);m;m--){
		int a,x,b,y; scanf("%d%d%d%d",&x,&a,&y,&b);
		bool ff=false; if (dep[x]<dep[y]) swap(a,b),swap(x,y);
		for (int i=h[x];i;i=ed[i].nxt)
			if (ed[i].to==y){ ff=true; break; }
		if (ff&&!a&&!b){ puts("-1"); continue; }
		if (x==y&&a!=b){ puts("-1"); continue; }
		calc(x,a,y,b);
		if (y==1) printf("%lld\n",g[1][b]);
		else printf("%lld\n",min(g[1][0],g[1][1]));
	}
	return 0;
}
