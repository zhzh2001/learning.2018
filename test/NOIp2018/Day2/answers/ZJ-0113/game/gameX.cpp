#include <cstdio>

int n, m;
int mm[100][100];
int last;
int s[100000];
int ans;

void find(int x, int y, int t)
{
	t = t * 2 + mm[x][y];
	if (x == n && y == m)
	{
		last++;
		s[last] = t;
		return;
	}
	if (y < m)
	{
		find(x, y + 1, t);
	}
	if (x < n)
	{
		find(x + 1, y, t);
	}
}

void solve()
{
	last = 0;
	find(1, 1, 0);
	/*dd
	if (mm[2][3] == 0 && mm[3][3] == 0)
	{
		puts("##");
		for (int i = 1; i <= n; i++)
		{
			for (int j = 1; j <= m; j++)
			{
				printf("%d", mm[i][j]);
			}
			puts("");
		}
		for (int i = 1; i <= last; i++)
		{
			printf("%d\n", s[i]);
		}
		puts("##");
	}
	*/
	for (int i = 1; i < last; i++)
	{
		if (s[i] > s[i + 1])
		{
			return;
		}
	}
	ans++;
	puts("");
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
		{
			printf("%d", mm[i][j]);
		}
		puts("");
	}
}

void dfs(int x, int y)
{
	if (x == n + 1 && y == 1)
	{
		solve();
		return;
	}
	int nx = x;
	int ny = y + 1;
	if (ny == m + 1)
	{
		nx++;
		ny = 1;
	}
	mm[x][y] = 0;
	dfs(nx, ny);
	mm[x][y] = 1;
	dfs(nx, ny);
}

int main()
{
	scanf("%d%d", &n, &m);
	ans = 0;
	dfs(1, 1);
	printf("%d\n", ans);
	return 0;
}
