#include <cstdio>
#include <algorithm>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

const int M = 1000000007;
int n, m;

int pow_mod(int x, int p, int M)
{
	int ans = 1;
	int tmp = x;
	while (p)
	{
		if (p & 1)
		{
			ans = (long long)ans * tmp % M;
		}
		tmp = (long long)tmp * tmp % M;
		p >>= 1;
	}
	return ans;
}

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	n = read();
	m = read();
	if (n > m)
	{
		std::swap(n, m);
	}
	if (n == 1)
	{
		printf("%d\n", pow_mod(2, m, M));
		return 0;
	}
	if (n == 2)
	{
		int ans = (long long)12 * pow_mod(3, m - 2, M) % M;
		printf("%d\n", ans);
		return 0;
	}
	if (n == 3)
	{
		int ans = (long long)112 * pow_mod(3, m - 3, M) % M;
		printf("%d\n", ans);
		return 0;
	}
	if (n == 4)
	{
		if (m == 4)
		{
			printf("%d\n", 912);
			return 0;
		}
		int ans = (long long)2688 * pow_mod(3, m - 5, M) % M;
		printf("%d\n", ans);
		return 0;
	}
	return 0;
}
