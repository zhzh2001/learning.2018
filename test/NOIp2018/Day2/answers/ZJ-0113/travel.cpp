#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

const int _n = 5000 + 10;
int n, m;
std::vector<int> vec[_n];
int vis[_n];
int top;
int stack[_n];
int last;
int vv[_n];
int UU, VV;
int edgenum;
int vet[2 * _n], nextx[2 * _n], head[2 * _n];
int times;
int ans[_n], tmp[_n];

void add(int u, int v)
{
	edgenum++;
	vet[edgenum] = v;
	nextx[edgenum] = head[u];
	head[u] = edgenum;
}

bool dfs(int u, int father)
{
	vis[u] = 1;
	top++;
	stack[top] = u;
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (v != father)
		{
			if (vis[v])
			{
				last++;
				vv[last] = v;
				for (int j = top; stack[j] != v; j--)
				{
					last++;
					vv[last] = stack[j];
				}
				return true;
			}
			if (dfs(v, u))
			{
				return true;
			}
		}
	}
	top--;
	return false;
}

void solve(int u, int father)
{
	times++;
	tmp[times] = u;
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (v != father && ! (u == UU && v == VV) && ! (u == VV && v == UU))
		{
			solve(v, u);
		}
	}
}

bool check()
{
	for (int i = 1; i <= n; i++)
	{
		if (tmp[i] < ans[i])
		{
			return true;
		}
		if (tmp[i] > ans[i])
		{
			return false;
		}
	}
	return false;
}

int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	n = read();
	m = read();
	for (int i = 1; i <= m; i++)
	{
		int u, v;
		u = read();
		v = read();
		vec[u].push_back(v);
		vec[v].push_back(u);
	}
	for (int i = 1; i <= n; i++)
	{
		std::sort(vec[i].begin(), vec[i].end());
		for (int j = (int)vec[i].size() - 1; j >= 0; j--)
		{
			add(i, vec[i][j]);
		}
	}
	if (m == n)
	{
		memset(vis, 0, sizeof(vis));
		dfs(1, 0);
		int flag = -1;
		for (int i = 1; i <= last; i++)
		{
			UU = vv[i];
			VV = vv[i % last + 1];
			times = 0;
			solve(1, 0);
			if (flag == -1 || check())
			{
				flag = 1;
				for (int j = 1; j <= n; j++)
				{
					ans[j] = tmp[j];
				}
			}
		}
	}
	else
	{
		UU = -1;
		VV = -1;
		times = 0;
		solve(1, 0);
		for (int i = 1; i <= n; i++)
		{
			ans[i] = tmp[i];
		}
	}
	for (int i = 1; i <= n; i++)
	{
		printf("%d ", ans[i]);
	}
	puts("");
	return 0;
}
