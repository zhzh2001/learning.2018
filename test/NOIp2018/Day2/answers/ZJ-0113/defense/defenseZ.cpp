#include <cstdio>
#include <algorithm>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

const long long INF = (long long)1000000000 * 1000000000;
const int _n = 100000 + 10;
int n, m;
char cas[100];
int edgenum;
int vet[2 * _n], nextx[2 * _n], head[_n];
int deep[_n];
int cost[_n];
int fa[_n];
long long f[_n][2], g[_n][2];
int lim[_n];

void add(int u, int v)
{
	edgenum++;
	vet[edgenum] = v;
	nextx[edgenum] = head[u];
	head[u] = edgenum;
}

void dfs(int u, int father)
{
	fa[u] = father;
	f[u][0] = 0;
	f[u][1] = cost[u];
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (v != father)
		{
			deep[v] = deep[u] + 1;
			dfs(v, u);
			f[u][0] = std::min(INF, f[u][0] + f[v][1]);
			f[u][1] = std::min(INF, f[u][1] + std::min(f[v][0], f[v][1]));
		}
	}
	if (lim[u] != -1)
	{
		f[u][lim[u] ^ 1] = INF;
	}
}

void dfs1(int u, int father)
{
	f[u][0] = 0;
	f[u][1] = cost[u];
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (v != father)
		{
			dfs1(v, u);
			f[u][0] = std::min(INF, f[u][0] + f[v][1]);
			f[u][1] = std::min(INF, f[u][1] + std::min(f[v][0], f[v][1]));
		}
	}
	if (u == 1)
	{
		f[u][0] = INF;
	}
}

void dfs2(int u, int father)
{
	long long sum0 = g[u][1];
	long long sum1 = cost[u] + std::min(g[u][0], g[u][1]);
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (v != father)
		{
			sum0 = std::min(INF, sum0 + f[v][1]);
			sum1 = std::min(INF, sum1 + std::min(f[v][0], f[v][1]));
		}
	}
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (v != father)
		{
			g[v][0] = sum0 - f[v][1];
			g[v][1] = sum1 - std::min(f[v][0], f[v][1]);
			if (u == 1)
			{
				g[v][0] = INF;
			}
			dfs2(v, u);
		}
	}
}

void solve1()
{
	dfs1(1, 0);
	dfs2(1, 0);
	while (m--)
	{
		int vx, cx, vy, cy;
		vx = read();
		cx = read();
		vy = read();
		cy = read();
		long long ans;
		if (cy == 0)
		{
			ans = std::min(INF, f[vy][0] + g[vy][1]);
		}
		else
		{
			ans = std::min(INF, f[vy][1] + std::min(g[vy][0], g[vy][1]));
		}
		printf("%lld\n", ans == INF ? (long long)-1 : ans);
	}
}

void dfs1_2(int u, int father)
{
	f[u][0] = 0;
	f[u][1] = cost[u];
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (v != father)
		{
			deep[v] = deep[u] + 1;
			dfs1_2(v, u);
			f[u][0] = std::min(INF, f[u][0] + f[v][1]);
			f[u][1] = std::min(INF, f[u][1] + std::min(f[v][0], f[v][1]));
		}
	}
}

void dfs2_2(int u, int father)
{
	long long sum0 = g[u][1];
	long long sum1 = cost[u] + std::min(g[u][0], g[u][1]);
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (v != father)
		{
			sum0 = std::min(INF, sum0 + f[v][1]);
			sum1 = std::min(INF, sum1 + std::min(f[v][0], f[v][1]));
		}
	}
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (v != father)
		{
			g[v][0] = sum0 - f[v][1];
			g[v][1] = sum1 - std::min(f[v][0], f[v][1]);
			dfs2_2(v, u);
		}
	}
}

void solve2()
{
	dfs1_2(1, 0);
	dfs2_2(1, 0);
	while (m--)
	{
		int vx, cx, vy, cy;
		vx = read();
		cx = read();
		vy = read();
		cy = read();
		if (deep[vx] < deep[vy])
		{
			std::swap(vx, vy);
			std::swap(cx, cy);
		}
		printf("%lld\n", (cx == 0 && cy == 0) ? (long long)-1 : f[vx][cx] + g[vx][cy]);
	}
}

void change(int u)
{
	int v;
	long long pre0 = f[u][0];
	long long pre1 = f[u][1];
	f[u][lim[u] ^ 1] = INF;
	while (u != 1)
	{
		v = u;
		u = fa[u];
		long long now0 = f[u][0];
		long long now1 = f[u][1];
		f[u][0] = f[u][0] - pre1;
		f[u][1] = f[u][1] - std::min(pre0, pre1);
		f[u][0] = std::min(INF, f[u][0] + f[v][1]);
		f[u][1] = std::min(INF, f[u][1] + std::min(f[v][0], f[v][1]));
		pre0 = now0;
		pre1 = now1;
		if (lim[u] != -1)
		{
			f[u][lim[u] ^ 1] = INF;
		}
	}
}

void pred(int u)
{
	f[u][0] = g[u][0];
	f[u][1] = g[u][1];
	while (u != 1)
	{
		u = fa[u];
		f[u][0] = g[u][0];
		f[u][1] = g[u][1];
	}
}

void solve3()
{
	for (int i = 1; i <= n; i++)
	{
		lim[i] = -1;
	}
	dfs(1, 0);
	for (int i = 1; i <= n; i++)
	{
		g[i][0] = f[i][0];
		g[i][1] = f[i][1];
	}
	while (m--)
	{
		int vx, cx, vy, cy;
		vx = read();
		cx = read();
		vy = read();
		cy = read();
		lim[vx] = cx;
		lim[vy] = vy;
		change(vx);
		change(vy);
		long long ans = std::min(f[1][0], f[1][1]);
		printf("%lld\n", ans == INF ? (long long)-1 : ans);
		lim[cx] = -1;
		lim[vy] = -1;
		pred(vx);
		pred(vy);
	}
}

int main()
{
	n = read();
	m = read();
	scanf("%s", cas + 1);
	for (int i = 1; i <= n; i++)
	{
		cost[i] = read();
	}
	for (int i = 1; i < n; i++)
	{
		int u, v;
		u = read();
		v = read();
		add(u, v);
		add(v, u);
	}
	if (n > 2000 && cas[2] == '1')
	{
		solve1();
		return 0;
	}
	if (n > 2000 && cas[2] == '2')
	{
		solve2();
		return 0;
	}
	if (cas[1] == 'B')
	{
		puts("X");
		solve3();
		return 0;
	}
	for (int i = 1; i <= n; i++)
	{
		lim[i] = -1;
	}
	while (m--)
	{
		int vx, cx, vy, cy;
		vx = read();
		cx = read();
		vy = read();
		cy = read();
		lim[vx] = cx;
		lim[vy] = cy;
		dfs(1, 0);
		long long ans = std::min(f[1][0], f[1][1]);
		printf("%lld\n", ans == INF ? (long long)-1 : ans);
		lim[vx] = -1;
		lim[vy] = -1;
	}
	return 0;
}

