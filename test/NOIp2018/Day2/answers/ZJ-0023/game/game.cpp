#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
using namespace std;
#define mo 1000000007;
#define ll long long
ll f[2000000][20],ans;
int n,m;
int save1[10],save2[10];
bool b[5000][5000];

bool pd(int x,int y){
	int u=x;
	int v=y;
	u>>=1;
	int tto=1;
	while (tto<n){
		if ((u%2==0)&&(v%2==1)) return false;
		u/=2;v/=2;
		tto++;
	}
	return 1;
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int j=0;j<(1<<n);j++)f[1][j]=1;
	memset(b,0,sizeof(b));
	for (int i=0;i<(1<<n);i++)
		for (int j=0;j<(1<<n);j++)
			b[i][j]=pd(i,j);
	/*for (int i=0;i<(1<<n);i++){
		for (int j=0;j<(1<<n);j++) printf("%d ",b[i][j]);
		cout<<endl;
	}*/
	for (int i=2;i<=m;i++)
		for (int j=0;j<(1<<n);j++){
			for (int k=0;k<(1<<n);k++)
				if (b[j][k]) {
					f[i][j]=(f[i][j]+f[i-1][k])%mo;
				}
		}
	ans=0;
	for (int i=0;i<(1<<n);i++)
		ans=(ans+f[m][i])%mo;
	printf("%lld",ans);
	return 0;
}
