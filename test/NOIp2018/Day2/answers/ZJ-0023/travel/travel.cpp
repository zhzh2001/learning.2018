#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
using namespace std;
#define N 20000
queue<int>q;
int n,m,tot;
int u,v;
struct Edge{
	int to,next;
}edge[N];

struct dd{
	int num,dep;
}c[N];
int head[N],depth[N],ss;
bool vis[N];

void add(int u,int v){
	tot++;	
	edge[tot].next=head[u];edge[tot].to=v;
	head[u]=tot;
}

bool cmp(dd x,dd y){
	if (x.dep<y.dep) return 1;
	else if (x.dep==y.dep) return x.num<y.num;
}

void dfs(int x){
	int y=0;
	for (int i=head[x];i;i=edge[i].next)
		if (!vis[edge[i].to])y++;
	while (y--){
		int minn=1e9;
		for (int i=head[x];i;i=edge[i].next){
			int pp=edge[i].to;
			if (!vis[pp]) minn=min(minn,pp);
		}
		for (int i=head[x];i;i=edge[i].next){
		 	if (minn==edge[i].to){
		 		vis[edge[i].to]=1;
		 		printf("%d ",edge[i].to);
		 		dfs(edge[i].to);
		 	}
		}
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	tot=0;
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		add(u,v);add(v,u);
	}
	if (n-1==m){
		vis[1]=1;depth[1]=1;
		printf("%d ",1);
		dfs(1);	
		return 0;
	}
	else {
		q.push(1);
	while (!q.empty()){
		int x=q.front();q.pop();
		for (int i=head[x];i;i=edge[i].next){
			int pp=edge[i].to;
			if (!vis[pp]){
				vis[pp]=1;
				depth[pp]=depth[x]+1;
				q.push(pp);
			}
		}
	}
	for (int i=1;i<=n;i++) {
		c[i].dep=depth[i];
		c[i].num=i;
	}
	int tt=0;
	int deep=0;
	while (tt<n){
			deep++;
			for (int i=1;i<=n;i++)
				if (depth[i]==deep){printf("%d ",i);tt++;}
		}
		return 0;	
	}
	return 0;
}
