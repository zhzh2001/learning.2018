#include <cstdio>
#include <queue>
#include <vector>
#include <functional>

int vis[5009];
int ans[5009];
int top;
std::priority_queue<int, std::vector<int>, std::greater<int> > que[5009];

void dfs(int pos, int n) {
	vis[pos] = true;
	ans[top++] = pos;
	if (top == n) return ;
	while(!que[pos].empty()) {
		int t = que[pos].top();
		que[pos].pop();
		if (!vis[t])
			dfs(t, n);
	}
	return ;
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	int n, m;
	scanf("%d %d", &n, &m);
	for (int i = 0; i < m; i++) {
		int x, y;
		scanf("%d %d", &x, &y);
		que[x].push(y);
		que[y].push(x);
	}
	top = 0;
	dfs(1, n);
	for (int i = 0; i < n; i++) {
		printf("%d ", ans[i]);
	}
	printf("\n");
	fclose(stdin);
	fclose(stdout);
	return 0;
}
