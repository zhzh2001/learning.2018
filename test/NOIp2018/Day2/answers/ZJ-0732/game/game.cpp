#include <cstdio>

const int ans[4][4] = {
	{0, 0, 0, 0}, 
	{0, 2, 4, 8}, 
	{0, 4, 12, 32}, 
	{0, 12, 32, 112}
};

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	int n, m;
	scanf("%d %d", &n, &m);
	printf("%d\n", ans[n][m]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
