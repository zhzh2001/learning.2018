#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int maxn=5010;
struct node{int to,next;}edge[maxn];
int n,m,head[maxn],tot=0;
bool vis[maxn];
int b[maxn*100],e[maxn][2],ans[maxn],s[maxn];
void add_edge(int x,int y)
{
	edge[++tot]=(node){y,head[x]};
	head[x]=tot;
}
void dfs(int x,int fa)
{
	if (vis[x]) return;
	vis[x]=1;
	printf("%d ",x);
	int l=b[0]+1;
	for (int i=head[x];i!=0;i=edge[i].next)
		if (edge[i].to!=fa) b[++b[0]]=edge[i].to;
	int r=b[0];
	sort(b+l,b+1+r);
	for (int i=l;i<=r;i++)
		dfs(b[i],x);
	return;
}

void work(int x,int fa,int xx,int yy)
{
	if (vis[x]) return;
	vis[x]=1;
	s[++s[0]]=x;
	int l=b[0]+1;
	for (int i=head[x];i!=0;i=edge[i].next)
	{
		int to=edge[i].to;
		if ((x==xx&&to==yy)||(x==yy&&to==xx)) continue; 
		if (to!=fa) b[++b[0]]=to;
	}
	int r=b[0];
	sort(b+l,b+1+r);
	for (int i=l;i<=r;i++)
		work(b[i],x,xx,yy);
	return;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,x,y;i<=m;i++)
	{
		scanf("%d%d",&x,&y);
		add_edge(x,y);
		add_edge(y,x);
		e[i][0]=x;
		e[i][1]=y;
	}
	vis[0]=1;
	b[0]=0;
	if (m==n-1) {dfs(1,0);return 0;}
	ans[1]=n+1;
	for (int i=1;i<=m;i++)
	{
		memset(vis,0,sizeof(vis));
		b[0]=0;
		s[0]=0;
		work(1,0,e[i][0],e[i][1]);
		if (s[0]==n){
			int flag=1;
			for (int i=1;i<=n&&flag==1;i++)
				if (s[i]<ans[i]) flag=2;
				else if (s[i]>ans[i]) flag=0;
			if (flag==2)
				for (int i=1;i<=n;i++) ans[i]=s[i];
		}
	}
	for (int i=1;i<=n;i++) printf("%d ",ans[i]);
}
	
