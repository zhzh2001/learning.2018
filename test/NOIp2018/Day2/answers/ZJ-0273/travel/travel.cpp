#include<iomanip>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<queue>
#include<cmath>
using namespace std;
int n,m,found,egsta[6000],eg,temp,way[10000];//egsta[i]=第i座城市的边开始 
struct edge{
	int sta;
	int ed;
};
edge rd[6000];
long long ans;
bool cmp(edge x,edge y){
	if(x.sta<y.sta){
		if(x.ed<y.ed){
			return true;
		}
		return false;
	}
	return false;
}
void search(int x,int z,int rdx){//x为起点，z为记忆化前路，rdx为现在走完的城市个数 
	if(rdx==n){
		found=1;
		way[rdx]=x;
		return;
	}
	for(int i=egsta[x];i<egsta[x+1];i++){
		search(rd[x].ed,x,rdx+1);
		if(found){
			way[rdx]=x;
			return;
		}
	}
	return;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&rd[i].sta,&rd[i].ed);
		if(rd[i].sta>rd[i].ed){
			temp=rd[i].sta;
			rd[i].sta=rd[i].ed;
			rd[i].ed=temp;
		}
	}
	sort(rd+1,rd+m+1,cmp);
	found=0;
	egsta[1]=1;
	eg=1;
	for(int i=2;i<=m;i++){
		if(rd[i].sta>rd[i-1].sta){
			eg++;
			egsta[eg]=i;
		}
	}
	for(int i=1;i<=n-1;i++){
		search(i,0,1);
		if(found){
			for(int i=1;i<=n;i++)printf("%d ",way[i]);
			fclose(stdin);fclose(stdout);
			return 0;
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
