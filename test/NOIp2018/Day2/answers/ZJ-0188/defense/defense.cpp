#include <bits/stdc++.h>
using namespace std;

#define FOR(i,n) for (int i=1;i<=n;i++)
#define REP(i,a,b) for (int i=a;i<=b;i++)
#define pb push_back

typedef long long ll;

const ll inf=1e18;
const int N=100000+10;
const int mo=1e9+7;

int n,m;
struct edge {
	int to,nxt;
} e[2*N];
int p[N];
ll f[N][3];
int tot;
int head[N];
int tag[N];
ll ans;
void insert(int x,int y) {
	e[++tot].to=y,e[tot].nxt=head[x],head[x]=tot;
	e[++tot].to=x,e[tot].nxt=head[y],head[y]=tot;
}
void dfs(int x,int fa) {
	int y;
	bool must=0;
	f[x][1]=p[x];
	f[x][2]=0;
	bool ok1=1,ok2=1,ok3=1;
	for (int i=head[x];i;i=e[i].nxt) {
		y=e[i].to;
		if (y!=fa) {
			dfs(y,x);
			f[x][2]+=f[y][1];
			if (tag[y]==1) {
				f[x][1]+=f[y][1];
			} else if (tag[y]==2) {
				f[x][1]+=f[y][2];
			} else {
				f[x][1]+=f[y][0];
			}
		}
	}
	if (must) f[x][2]=inf;
	f[x][0]=min(f[x][1],f[x][2]);
	if (tag[x]==1) {
		f[x][0]=f[x][1],f[x][2]=inf;
	} else if (tag[x]==2) {
		f[x][1]=inf;
		f[x][0]=f[x][2];
	}
	f[x][0]=min(abs(f[x][0]),inf);
	f[x][1]=min(abs(f[x][1]),inf);
	f[x][2]=min(abs(f[x][2]),inf);
}
char c[100];
int dp[N][3];
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d %d %s",&n,&m,c);
	FOR(i,n) scanf("%d",&p[i]);
	int x,y;
	FOR(i,n-1) {
		scanf("%d %d",&x,&y);
		insert(x,y);
	}
	int a,b;
	ll sum=0;
	FOR(i,n) sum+=p[i]; 
	if (n>=100000&&c[0]=='A') {
		
		FOR(i,n) {
			f[i][1]=f[i-1][0]+p[i];
			f[i][1]=min(f[i][1],inf);
			f[i][2]=f[i-1][1];
			f[i][2]=min(f[i][2],inf);
			f[i][0]=min(f[i][1],f[i][2]);
			
			//f[i][0]%=mo;
			//f[i][1]%=mo;
			//f[i][2]%=mo;
		}
		FOR(i,m) {
			scanf("%d %d %d %d",&x,&a,&y,&b);
			if (a==0) a=2;
			if (b==0) b=2;
			if (f[x][a]>sum||f[y][b]>sum) {
				printf("%d\n",-1);
			} else {
				ans=f[n][0];
				ans+=f[x][a]-f[x][0]+f[y][b]-f[y][0];
				printf("%d\n",-1);
			}
		}
		return 0;
	}
	FOR(i,m) {
		scanf("%d %d %d %d",&x,&a,&y,&b);
		if (a==0) a=2;
		if (b==0) b=2;
		memset(f,0,sizeof f);
		tag[x]=a,tag[y]=b;
		dfs(1,0);
		if (tag[1]==1) {
			ans=f[1][1];
		} else if (tag[1]==2) {
			ans=f[1][2];
		} else {
			ans=f[1][0];
		}
		tag[x]=tag[y]=0;
		if (ans>sum) {
			printf("%d\n",-1);
		} else {
			printf("%lld\n",ans);
		}
	}
	return 0;
}
