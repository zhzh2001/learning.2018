#include <bits/stdc++.h>
using namespace std;

#define FOR(i,n) for (int i=1;i<=n;i++)
#define REP(i,a,b) for (int i=a;i<=b;i++)
#define pb push_back

const int N=5000+10;

/*
给定N点M边的无向联通图，不含自环和重边
求字典序最小的访问完所有点的dfs序列

保证这个图要么是树要么是基环树

为什么要这样保证？
都是直接贪心地找编号最小的点dfs
对于树来说是对的，环树要找环，然后断开每个边算出多种方案再取最优的 
*/

int n,m;
struct edge {
	int to,id;
};
int tot;
vector<edge> v[N];
vector<int> s,t,e;
bool vis[N];
bool ban[N];
vector<int> res,ans;
bool cmp(edge a,edge b) {
	return a.to<b.to;
}
void insert(int x,int y) {
	++tot;
	//cout<<tot<<" "<<x<<" "<<y<<endl;
	v[x].pb(edge{y,tot});
	v[y].pb(edge{x,tot});
}
void dfs(int x) {
	if (vis[x]) return;
	res.pb(x);
	vis[x]=1;
	int y;
	for (int i=0;i<(int)v[x].size();i++) {
		if (ban[v[x][i].id]) continue;
		y=v[x][i].to;
		if (!vis[y]) {
			dfs(y);
		}
	}
}
bool flag;
void find(int x,int fa) {
	if (flag) return;
	if (vis[x]) {
		flag=1;
		while (!s.empty()) {
			e.pb(s.back());
			s.pop_back();
			if (t.back()==x) {
				break;
			}
			t.pop_back();
		}
		while (!s.empty()) s.pop_back();
		while (!t.empty()) t.pop_back();
		return;	
	}
	vis[x]=1;
	t.pb(x);
	int y;
	for (int i=0;i<(int)v[x].size();i++) {
		if (flag) break;
		y=v[x][i].to;
		if (y!=fa) {
			s.pb(v[x][i].id);
			find(y,x);
		}
	}
}
bool ok() {
	for (int i=0;i<n;i++) {
		if (res[i]>ans[i]) return 0;
		else if (res[i]<ans[i]) return 1;
	}
	return 0;
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d %d",&n,&m);
	int x,y;
	FOR(i,m) {
		scanf("%d %d",&x,&y);
		insert(x,y);
	}
	FOR(i,n) sort(v[i].begin(),v[i].end(),cmp);
	if (m==n-1) {
		dfs(1);
		ans=res;
		FOR(i,n) {
			if (i!=n) printf("%d ",ans[i-1]);
			else printf("%d",ans[i-1]);
		}
	} else {
		find(1,0);
		for (int i=0;i<(int)e.size();i++) {
			memset(vis,0,sizeof vis);
			ban[e[i]]=1;
			res.clear();
			dfs(1);
			//FOR(i,n) printf("%d ",res[i-1]);
			//cout<<endl;
			if (i==0) {
				ans=res;
			} else if (ok()) {
				ans=res;
			}
			ban[e[i]]=0;
		}
		FOR(i,n) {
			if (i!=n) printf("%d ",ans[i-1]);
			else printf("%d",ans[i-1]);
		}
	}
	return 0;
}
