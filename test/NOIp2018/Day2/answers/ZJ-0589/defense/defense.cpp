#include <cstdio>
#include <algorithm>
int h[100001], to[200001], nxt[200001], cnt;
inline void add_edge(const int& u, const int& v) {
	to[++cnt] = v; nxt[cnt] = h[u]; h[u] = cnt;
}
int n, m, p[100001], a, x, b, y, dp[100001][2];
char type[233];
int dp1[100001][2];
int f[100001];
bool flag[100001];
void dfs1(const int& u) {
	dp1[u][0] = 0; dp1[u][1] = p[u];
	for (register int i = h[u]; i; i = nxt[i]) if (to[i] != f[u]) {
		f[to[i]] = u; dfs1(to[i]);
		dp1[u][0] += dp1[to[i]][1];
		dp1[u][1] += std::min(dp1[to[i]][0], dp1[to[i]][1]);
	}
}
void dfs(const int& u, const int& fa) {
	if (!flag[u]) {
		dp[u][0] = dp1[u][0], dp[u][1] = dp1[u][1];
		return;
	}
	flag[u] = 0;
	dp[u][0] = 0;
	dp[u][1] = p[u];
	for (register int i = h[u]; i; i = nxt[i]) if (to[i] != fa) {
		dfs(to[i], u);
		if (dp[to[i]][0] == -1 && dp[to[i]][1] == -1) {
			dp[u][0] = dp[u][1] = -1;
			return;
		}
		if (dp[to[i]][1] == -1) dp[u][0] = -1;
		if (dp[u][0] != -1) dp[u][0] += dp[to[i]][1];
		if (dp[u][1] == -1) continue;
		if (dp[to[i]][0] == -1) dp[u][1] += dp[to[i]][1];
		else if (dp[to[i]][1] == -1) dp[u][1] += dp[to[i]][0];
		else dp[u][1] += std::min(dp[to[i]][0], dp[to[i]][1]);
	}
	if (u == a) dp[u][!x] = -1;
	if (u == b) dp[u][!y] = -1;
}
void work1() {
	for (register int i = 0; i < m; ++i) {
		scanf("%d%d%d%d", &a, &x, &b, &y);
		dfs(1, 0);
		if ((~dp[1][0]) && (~dp[1][1])) printf("%d\n", std::min(dp[1][0], dp[1][1]));
		else if (dp[1][0] + dp[1][1] == -2) puts("-1");
		else printf("%d\n", (~dp[1][0]) ? dp[1][0] : dp[1][1]);
	}
}
void work2() {
	puts("work2()");
	dfs1(1);
	for (register int i = 0; i < m; ++i) {
		scanf("%d%d%d%d", &a, &x, &b, &y);
		for (register int i = a; i; i = f[i]) flag[i] = 1;
		for (register int i = b; i; i = f[i]) flag[i] = 1;
		dfs(1, 0);
		if ((~dp[1][0]) && (~dp[1][1])) printf("%d\n", std::min(dp[1][0], dp[1][1]));
		else if (dp[1][0] + dp[1][1] == -2) puts("-1");
		else printf("%d\n", (~dp[1][0]) ? dp[1][0] : dp[1][1]);
	}
}
int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d%s", &n, &m, type);
	for (register int i = 1; i <= n; ++i) scanf("%d", &p[i]);
	for (register int i = 1; i < n; ++i) {
		int u, v;
		scanf("%d%d", &u, &v);
		add_edge(u, v); add_edge(v, u);
	}
	if (n <= 2000) work1();
	else if (type[0] == 'B') work2();
	else return fclose(stdin), fclose(stdout), 233;
	return fclose(stdin), fclose(stdout), 0;
}
