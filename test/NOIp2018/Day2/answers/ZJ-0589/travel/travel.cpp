#include <cstdio>
#include <vector>
#include <algorithm>
#include <cassert>
struct _ {
	int u, v;
	_(const int& u = 0, const int& v = 0) : u(u), v(v) {}
} e[10001];
struct __ {
	bool operator()(const _& a, const _& b) {
		return a.v < b.v;
	}
} cmp;
int h[5001], to[10001], nxt[10001], cnt;
inline void add_edge(const int& u, const int& v) {
	to[++cnt] = v; nxt[cnt] = h[u]; h[u] = cnt;
}
int n, m;
std::vector<int> ans;
void dfs(const int& u, const int& fa) {
	ans.push_back(u);
	for (register int i = h[u]; i; i = nxt[i]) if (to[i] != fa)
		dfs(to[i], u);
}
void work1() {
	int u, v;
	for (register int i = 0; i < m; ++i) {
		scanf("%d%d", &u, &v);
		e[i << 1] = _(u, v);
		e[(i << 1) | 1] = _(v, u);
	}
	std::sort(e, e + (m << 1), cmp);
	for (register int i = n << 1; i--; )
		add_edge(e[i].u, e[i].v);
	dfs(1, 0);
}
int vis[5001], rt, u1, u2;
void dfs1(const int& u, const int& fa) {
	vis[u] = 1;
	for (register int i = h[u]; i; i = nxt[i]) if (to[i] != fa) {
		if (!rt && vis[to[i]]) rt = to[i], vis[u] = 2, u2 = u;
		else if (!vis[to[i]]) dfs1(to[i], u);
		if (vis[to[i]] == 2 && to[i] != rt) {
			vis[u] = 2;
			if (!u1 && u == rt) u1 = to[i];
		}
	}
}
bool vis2[5001];
int clk;
void dfs2(const int& u, const int& fa, const int& mx) {
	++clk;
	vis2[u] = 1; ans.push_back(u);
	for (register int i = h[u]; i; i = nxt[i]) if (to[i] != fa && !vis2[to[i]]) {
		if (mx && vis[u] == 2 && vis[to[i]] == 2 && !vis2[u2] && !nxt[i] && to[i] > mx) return;
		if (u == rt && to[i] == u1) dfs2(to[i], u, u2);
		else dfs2(to[i], u, mx ? (nxt[i] ? to[nxt[i]] : mx) : 0);
	}
}
void work2() {
	int u, v;
	for (register int i = 0; i < m; ++i) {
		scanf("%d%d", &u, &v);
		e[i << 1] = _(u, v);
		e[(i << 1) | 1] = _(v, u);
	}
	std::sort(e, e + (m << 1), cmp);
	for (register int i = n << 1; i--; )
		add_edge(e[i].u, e[i].v);
	dfs1(1, 0);
	dfs2(1, 0, 0);
	assert(clk == n);
}
int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (m == n - 1) work1();
	else work2();
	printf("%d", ans[0]);
	for (register int i = 1; i < n; ++i) printf(" %d", ans[i]);
	putchar('\n');
	return fclose(stdin), fclose(stdout), 0;
}
