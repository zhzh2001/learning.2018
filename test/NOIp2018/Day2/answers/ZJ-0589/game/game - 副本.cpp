#define wcytql
#include <cstdio>
#include <cstring>
#include <algorithm>
#define _for(i, a, b) for (register int i = a; i < b; ++i)
const int Mod = 1000000007;
inline void sadd(int& s, const int& a) {s = (s + a) % Mod;}
int n;
void print(int x) {if (x >> 1) print(x >> 1); putchar((x & 1) + '0');}
int m, dp[2][1 << 8][9];
int t1[1 << 8][(1 << 8) * 9][2], t2[1 << 8][1 << 8], cnt[1 << 8];
inline bool check(const int& S, const int& S1, const int& j) {
	_for(k, 1, n) if ((k <= j && (((S >> k) & 1)) ^ ((S1 >> (k - 1)) & 1)) || (k > j && (((S >> k) & 1) > ((S1 >> (k - 1)) & 1))))
		return 0;
	return 1;
}
inline int count(const int& S, const int& S1) {
	for (register int k = n - 1; k--; ) if (((S >> (k + 1)) & 1) == ((S1 >> k) & 1)) return k;
	return 0;
}
int main() {
//	freopen("game.in", "r", stdin);
//	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m); int U = 1 << n;
	_for(i, 0, U) _for(j, 0, U) _for(k, 0, n) if (check(i, j, k)) {
		t1[i][cnt[i]][0] = j; t1[i][cnt[i]][1] = k; ++cnt[i];
	}
	_for(i, 0, U) _for(j, 0, U) t2[i][j] = count(i, j);
	_for(S, 0, U) dp[0][S][0] = 1;
	bool opt = 0;
	_for(i, 1, m) {
		opt ^= 1;
		memset(dp[opt], 0, sizeof(dp[opt]));
		_for(S, 0, U) {
			_for(k, 0, cnt[S]) {
				int S1 = t1[S][k][0], j = t1[S][k][1], _j = std::max(j, t2[S][S1]);
            	sadd(dp[opt][S][_j], dp[opt ^ 1][S1][j]);
			}
		}
	}
	int ans = 0;
	_for(S, 0, U) _for(j, 0, n) sadd(ans, dp[opt][S][j]);
	printf("%d\n", ans);
	return fclose(stdin), fclose(stdout), 0;
}
