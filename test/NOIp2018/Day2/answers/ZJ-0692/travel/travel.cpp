#include<bits/stdc++.h>
#define F_IO(_) freopen(#_".in","r",stdin);freopen(#_".out","w",stdout)
using namespace std;
#define REP(i,j,k) for(int i=(j),i##_END_=(k);i<=i##_END_;i++)
#define DREP(i,j,k) for(int i=(j),i##_END_=(k);i>=i##_END_;i--)
#define LREP(i,x) for(int i=head[x];i!=-1;i=nxt[i])
typedef long long lll;
template<typename T,typename V>
inline bool tomin(T &x,V y){if(x>y){x=y;return true;}return false;}
template<typename T,typename V>
inline bool tomax(T &x,V y){if(x<y){x=y;return true;}return false;}
/****************header****************/
const int M=5005;
int n,m,a,b,stk[M],ord[M],ans[M],b0,b1;
vector<int>edge[M],cir,res;
bool mark[M],markc[M];
void dfs0(int x){
	if(x!=1)putchar(' ');
	printf("%d",x);
	mark[x]=true;
	REP(i,0,edge[x].size()-1){
		int t=edge[x][i];
		if(mark[t])continue;
		dfs0(t);
	}
}
void dfs1(int x,int f){
	stk[++stk[0]]=x;
	mark[x]=true;
	REP(i,0,edge[x].size()-1){
		int t=edge[x][i];
		if(t==f)continue;
		if(mark[t]){
			if(cir.size()>0)continue;
			int k=stk[0],tmp;
			do{
				tmp=stk[k];
				cir.push_back(tmp);
				--k;
			}while(k>=1&&t!=tmp);
		}else dfs1(t,x);
	}
	--stk[0];
}
void dfs3(int x){
	res.push_back(x);
	mark[x]=true;
	REP(i,0,edge[x].size()-1){
		int t=edge[x][i];
		if(mark[t])continue;
		if((x==b0&&t==b1)||(t==b0&&x==b1)){
			continue;
		}
		dfs3(t);
	}
}
bool cmp(vector<int> &a,vector<int> &b){
//	assert(a.size()==b.size());
	REP(i,0,a.size()-1){
		if(a[i]!=b[i])return a[i]<b[i];
	}
	return false;
}
void solve_cir(){
	dfs1(1,-1);
	REP(i,0,cir.size()-1)markc[cir[i]]=true;
	vector<int>ans;
	b0=cir[0];b1=*cir.rbegin();
	memset(mark,0,sizeof(mark));
	dfs3(1);
	swap(ans,res);
	res.clear();
	REP(i,1,cir.size()-1){
		b0=cir[i-1];b1=cir[i];
		memset(mark,0,sizeof(mark));
		dfs3(1);
		if(!cmp(ans,res))swap(ans,res);
		res.clear();
	}
	bool first=true;
	REP(i,0,ans.size()-1){
		if(first)first=false;
		else putchar(' ');
		printf("%d",ans[i]);
	}
}
int main(){
F_IO(travel);
	scanf("%d%d",&n,&m);
	REP(i,1,m){
		scanf("%d%d",&a,&b);
		edge[a].push_back(b);
		edge[b].push_back(a);
	}
	REP(i,1,n)sort(edge[i].begin(),edge[i].end());
	if(m==n-1)dfs0(1);
	else {
		solve_cir();
	}
	putchar('\n');
	return 0;
}
//cerr<<"\tMemory: "<<(&END_DEF-&BEG_DEF)/1024.0/1024.0<<"MB"<<endl;
