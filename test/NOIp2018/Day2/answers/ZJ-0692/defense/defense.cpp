#include<bits/stdc++.h>
#define F_IO(_) freopen(#_".in","r",stdin);freopen(#_".out","w",stdout)
using namespace std;
#define REP(i,j,k) for(int i=(j),i##_END_=(k);i<=i##_END_;i++)
#define DREP(i,j,k) for(int i=(j),i##_END_=(k);i>=i##_END_;i--)
#define LREP(i,x) for(int i=head[x];i!=-1;i=nxt[i])
typedef long long lll;
template<typename T,typename V>
inline bool tomin(T &x,V y){if(x>y){x=y;return true;}return false;}
template<typename T,typename V>
inline bool tomax(T &x,V y){if(x<y){x=y;return true;}return false;}
/****************header****************/
const int M=2000,INF=0x3f3f3f3f;
int n,m,A[M],dp[M][2],fa[M];
int mark[M];
vector<int>edge[M];
void dfs0(int x,int f){
	fa[x]=f;
	REP(i,0,edge[x].size()-1){
		int t=edge[x][i];
		if(t==f)continue;
		dfs0(t,x);
	}
}
void dfs1(int x,int f){
	dp[x][0]=0;//buqu
	dp[x][1]=0;//qu
	REP(i,0,edge[x].size()-1){
		int t=edge[x][i];
		if(t==f)continue;
		dfs1(t,x);
		dp[x][0]+=dp[t][1];
		dp[x][1]+=min(dp[t][0],dp[t][1]);
	}
	dp[x][1]+=A[x];
	if(mark[x]==1)dp[x][0]=dp[x][1];
	if(mark[x]==0)dp[x][1]=dp[x][0];
}
void solve(){
	dfs1(1,0);
	printf("%d\n",min(dp[1][0],dp[1][1]));
}
int main(){
F_IO(defense);
	scanf("%d%d",&n,&m);
	char str[20];
	scanf("%s",str);
	REP(i,1,n)scanf("%d",&A[i]);
	REP(i,1,n-1){
		int u,v;
		scanf("%d%d",&u,&v);
		edge[u].push_back(v);
		edge[v].push_back(u);
	}
	dfs0(1,0);
	memset(mark,-1,sizeof(mark));
	REP(cas,1,m){
		int x,y,inx,iny;
		scanf("%d%d%d%d",&x,&inx,&y,&iny);
		if(inx==0){
			mark[x]=0;
			REP(i,0,edge[x].size()){
				if(mark[edge[x][i]]==-1)mark[edge[x][i]]=1;
				else if(mark[edge[x][i]]==0){
					printf("-1\n");
					goto FAIL;
				}
			}
		}
		else mark[x]=1;
		if(iny==0){
			mark[y]=0;
			REP(i,0,edge[y].size()){
				if(mark[edge[y][i]]==-1)mark[edge[y][i]]=1;
				else if(mark[edge[y][i]]==0){
					printf("-1\n");
					goto FAIL;
				}
			}
		}
		else{
		       if(mark[y]==-1)mark[y]=1;
			else if(mark[y]==0){
				printf("-1\n");
				goto FAIL;
			}
		}
		solve();
FAIL:;
		mark[x]=-1;
		mark[y]=-1;
		REP(i,0,edge[x].size())
			mark[edge[x][i]]=-1;
		REP(i,0,edge[y].size())
			mark[edge[y][i]]=-1;
	}
	return 0;
}
//cerr<<"\tMemory: "<<(&END_DEF-&BEG_DEF)/1024.0/1024.0<<"MB"<<endl;
