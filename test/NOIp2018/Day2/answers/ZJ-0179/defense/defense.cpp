#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define oo (0x3f3f3f3f*0x3f3f3f3fLL)
#define L (k<<1)
#define R (L+1)
#define mid ((l+r)>>1)
struct ji{
	int nex,to;
}edge[200001];
struct ji2{
	ll a[5];
}o1,o2,o3,o4,ft[1000001];
ll E,n,m,x,y,p1,p2,ans,a[100001],f[100001][2],head[100001];
char s[11];
void add(int x,int y){
	edge[E].nex=head[x];
	edge[E].to=y;
	head[x]=E++;
}
ll jia(ll x,ll y){
	if ((x==oo)||(y==oo))return oo;
	return x+y;
}
void dfs(int k,int fa){
	f[k][0]=0;
	f[k][1]=a[k];
	for(int i=head[k];i!=-1;i=edge[i].nex)
		if (edge[i].to!=fa){
			dfs(edge[i].to,k);
			f[k][0]=jia(f[k][0],f[edge[i].to][1]);
			f[k][1]=jia(f[k][1],min(f[edge[i].to][0],f[edge[i].to][1]));
		}
	if (k==x)f[k][p1^1]=oo;
	if (k==y)f[k][p2^1]=oo;
}
ji2 up(ji2 o1,ji2 o2){
	ji2 o3;
	o3.a[0]=min(min(jia(o1.a[0],o2.a[2]),jia(o1.a[1],o2.a[0])),jia(o1.a[1],o2.a[2]));
	o3.a[1]=min(min(jia(o1.a[0],o2.a[3]),jia(o1.a[1],o2.a[1])),jia(o1.a[1],o2.a[3]));
	o3.a[2]=min(min(jia(o1.a[2],o2.a[2]),jia(o1.a[3],o2.a[0])),jia(o1.a[3],o2.a[2]));
	o3.a[3]=min(min(jia(o1.a[2],o2.a[3]),jia(o1.a[3],o2.a[1])),jia(o1.a[3],o2.a[3]));
	return o3;
}
void build(int k,int l,int r){
	if (l==r){
		ft[k].a[0]=0;
		ft[k].a[1]=ft[k].a[2]=oo;
		ft[k].a[3]=a[l];
		return;
	}
	build(L,l,mid);
	build(R,mid+1,r);
	ft[k]=up(ft[L],ft[R]);
}
ji2 query(int k,int l,int r,int x,int y){
	if ((x<=l)&&(r<=y))return ft[k];
	if (mid+1>y)return query(L,l,mid,x,y);
	else
		if (mid<x)return query(R,mid+1,r,x,y);
		else return up(query(L,l,mid,x,y),query(R,mid+1,r,x,y));
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof(head));
	scanf("%lld%lld%s",&n,&m,s);
	for(int i=1;i<=n;i++)scanf("%lld",&a[i]);
	for(int i=1;i<n;i++){
		scanf("%lld%lld",&x,&y);
		add(x,y);
		add(y,x);
	}
	o4.a[3]=oo;
	if (s[0]=='A')build(1,1,n);
	for(int i=1;i<=m;i++){
		scanf("%lld%lld%lld%lld",&x,&p1,&y,&p2);
		if (x>y){
			swap(x,y);
			swap(p1,p2);
		}
		if ((n<=2000)&&(m<=2000)){
			dfs(1,0);
			if (min(f[1][0],f[1][1])==oo)printf("-1\n");
			else printf("%lld\n",min(f[1][0],f[1][1]));
			continue;
		}
		if (s[0]=='A'){
			o1=o2=o3=o4;
			if (x>1)o1=query(1,1,n,1,x-1);
			if (x<y-1)o2=query(1,1,n,x+1,y-1);
			if (y<n)o3=query(1,1,n,y+1,n);
			o1.a[4]=o1.a[0];
			o1.a[0]=min(o1.a[1],o1.a[3]);
			o1.a[1]=min(min(o1.a[4],o1.a[0]),o1.a[2]);
			o3.a[4]=o3.a[0];
			o3.a[0]=min(o3.a[2],o3.a[3]);
			o3.a[1]=min(min(o3.a[4],o3.a[0]),o3.a[1]);
			ans=jia(a[x]*p1+a[y]*p2,jia(o1.a[p1],o3.a[p2]));
			if ((!p1)&&(!p2))ans=jia(ans,o2.a[3]);
			if ((!p1)&&(p2))ans=jia(ans,min(o2.a[2],o2.a[3]));
			if ((p1)&&(!p2))ans=jia(ans,min(o2.a[1],o2.a[3]));
			if ((p1)&&(p2))ans=jia(ans,min(min(o2.a[0],o2.a[1]),min(o2.a[2],o2.a[3])));
			if (ans==oo)ans=-1;
			printf("%lld\n",ans);
			continue;
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
