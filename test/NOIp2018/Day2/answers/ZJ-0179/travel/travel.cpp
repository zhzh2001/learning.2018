#include<bits/stdc++.h>
using namespace std;
int n,m,x,y,s[10001],ans[10001];
bool flag,vis[5001];
vector<int>v[5001];
bool dfs(int k,int fa){
	if (vis[k])return 1;
	vis[s[++s[0]]=k]=1;
	for(int i=0;i<v[k].size();i++)
		if ((v[k][i])&&(v[k][i]!=fa)&&(dfs(v[k][i],k)))return 1;
	return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		v[x].push_back(y);
		v[y].push_back(x);
	}
	for(int i=1;i<=n;i++)sort(v[i].begin(),v[i].end());
	if (!dfs(1,0)){
		for(int i=1;i<=n;i++)printf("%d ",s[i]);
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	x=y=0;
	for(int i=1;i<=n;i++)ans[i]=n;
	for(int i=1;i<=n;i++)
		for(int j=0;j<v[i].size();j++){
			memset(vis,0,sizeof(vis));
			s[0]=0;
			swap(v[i][j],x);
			y=lower_bound(v[x].begin(),v[x].end(),i)-v[x].begin();
			v[x][y]=0;
			if ((!dfs(1,0))&&(s[0]==n)){
				flag=0;
				for(int k=1;k<=n;k++)
					if (s[k]!=ans[k]){
						flag=(ans[k]>s[k]);
						break;
					}
				if (flag)
					for(int k=1;k<=n;k++)ans[k]=s[k];
			}
			v[x][y]=i;
			swap(x,v[i][j]);
		}
	for(int i=1;i<=n;i++)printf("%d ",ans[i]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
