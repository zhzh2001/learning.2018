#include<bits/stdc++.h>
using namespace std;
const int mod=(1e9)+7;
int n,m,mm,al,dp[1000010][10],t,ans;
int tot,f[1000],a[500][500];
bool check(int t1,int t2){
	for (int i=0;i<n;i++){
		if ((t2&1)>(t1&1)) return 0;
		t1>>=1; t2>>=1;
	}
	return 1;
}
inline int get(int t1,int t2){
	t1=t1%(1<<(n-2)); t2=t2%(1<<(n-2)); return (t1&t2);
}
inline int jia(int t1,int t2){
	return (t1+t2>=mod)?t1+t2-mod:t1+t2;
}
void dfs(int x,int y,int now){
	if (x==n&&y==n){
		tot++; f[tot]=now; return;
	}
	if (x!=n) dfs(x+1,y,(now<<1)|a[x][y]);
	if (y!=n) dfs(x,y+1,(now<<1)|a[x][y]);
}
void dfs2(int x){
	if (x>n){
		tot=0; bool flag=1;
		dfs(1,1,0); int maxx=f[tot];
		for (int i=tot-1;i>0;i--){
			if (f[i]<maxx){
				flag=0; break;
			}
			maxx=max(maxx,f[i]);
		}
		if (flag) ans++;
		return;
	}
	for (int i=0;i<al;i++){
		int t=i;
		for (int j=1;j<=m;j++) a[x][j]=t&1,t>>=1;
		dfs2(x+1);
	}
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==2){
		al=(1<<(n-1));
		for (int i=0;i<al;i++) dp[1][i]=2;
		for (int i=1;i<m;i++)
			for (int j=0;j<al;j++)
				if (dp[i][j]>0)
					for (int k=0;k<al;k++)
						if (check(j,k)){
							t=(get(j,k))<<1;
							dp[i+1][t|1]=jia(dp[i+1][t|1],dp[i][j]);
							dp[i+1][t]=jia(dp[i+1][t],dp[i][j]);
						}
		for (int i=0;i<al;i++) ans=jia(ans,dp[m][i]);
	}else{
		al=(1<<m); dfs2(1);
	}
	printf("%d\n",ans);
	return 0;
}
