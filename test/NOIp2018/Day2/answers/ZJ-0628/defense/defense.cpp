#include<bits/stdc++.h>
using namespace std;
const int N=100010;
int n,m,u,v,x,y,a,b,dp[N][2],f[N][2],val[N];
int num,vet[N<<1],nex[N<<1],hea[N];
char s[5];
void read(int &x){
	int tmp=1; char ch=getchar(); x=0;
	while (ch<'0'||ch>'9'){if (ch=='-') tmp=-1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48; ch=getchar();}
	x*=tmp;
}
void add(int u,int v){
	vet[++num]=v; nex[num]=hea[u]; hea[u]=num;
}
void dfs(int u,int fa){
	int limit=-1,now;
	bool flag;
	if (u==a) limit=x;
	if (u==b) limit=y;
	if (limit!=0){
		now=0; flag=1;
		for (int i=hea[u];i;i=nex[i])
			if (vet[i]!=fa){
				dfs(vet[i],u);
				if (dp[vet[i]][0]==-1&&dp[vet[i]][1]==-1){
					flag=0; break;
				}
				if (dp[vet[i]][0]==-1) now+=dp[vet[i]][1];
				else if (dp[vet[i]][1]==-1) now+=dp[vet[i]][0];
				else now+=min(dp[vet[i]][0],dp[vet[i]][1]);
			}
		if (flag) dp[u][1]=now+val[u];
	}
	if (limit!=1){
		now=0; flag=1;
		for (int i=hea[u];i;i=nex[i])
			if (vet[i]!=fa){
				dfs(vet[i],u);
				if (dp[vet[i]][1]==-1){
					flag=0; break;
				}
				now+=dp[vet[i]][1];
			}
		if (flag) dp[u][0]=now;
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for (int i=1;i<=n;i++) read(val[i]);
	for (int i=1;i<n;i++) read(u),read(v),add(u,v),add(v,u);
	if (n<=2000){
		while (m--){
			memset(dp,-1,sizeof(dp));
			read(a); read(x); read(b); read(y);
			dfs(1,0);
			if (dp[1][0]==-1) printf("%d\n",dp[1][1]);
			else if (dp[1][1]==-1) printf("%d\n",dp[1][0]);
			else printf("%d\n",min(dp[1][0],dp[1][1]));
		}
	}else{
		memset(dp,-1,sizeof(dp)); b=a=n+10; dfs(1,0);
		for (int i=1;i<=n;i++) f[i][0]=dp[i][0],f[i][1]=dp[i][1];
		memset(dp,-1,sizeof(dp)); dfs(n,0);
		while (m--){
			read(a); read(x); read(b); read(y);
			if (a>b) swap(a,b),swap(x,y);
			if (x==0&&y==0) printf("-1\n");
			else printf("%d\n",f[a][x]+dp[b][y]);
		}
	}
	return 0;
}
