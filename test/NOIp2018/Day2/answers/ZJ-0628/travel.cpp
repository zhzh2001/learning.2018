#include<bits/stdc++.h>
using namespace std;
int n,m,x,y,jin,st,u,v,f[5010],tot[5010],to[5010][5010],ans[5010];
int num,vet[10010],nex[10010],hea[5010];
bool flag[5010],boo1,boo2;
void add1(int u,int v){
	vet[++num]=v; nex[num]=hea[u]; hea[u]=num;
}
void dfs1(int u,int fa){
	flag[u]=1;
	for (int i=hea[u];i;i=nex[i])
		if (vet[i]!=fa){
			if (flag[vet[i]]){
				st=vet[i]; boo1=1; boo2=1; f[++f[0]]=u; return;
			}
			dfs1(vet[i],u);
			if (boo1){
				if (boo2) f[++f[0]]=u;
				boo2=boo2&&(u!=st); return;
			}
		}
}
void dfs2(int u,int fa){
	ans[++ans[0]]=u;
	for (int i=hea[u];i;i=nex[i])
		if (i!=jin&&(i^1)!=jin)
			if (vet[i]!=fa) to[u][++tot[u]]=vet[i];
	sort(to[u]+1,to[u]+tot[u]+1);
	for (int i=1;i<=tot[u];i++) dfs2(to[u][i],u);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m); num=1;
	for (int i=1;i<=m;i++) scanf("%d%d",&u,&v),add1(u,v),add1(v,u);
	if (m==n){
		dfs1(1,0);
		if (f[1]<f[f[0]-1]){
			x=2;
			while (f[x]<f[f[0]-1]) x++;
			y=f[x-1]; x=f[x];
		}else{
			x=f[0]-2;
			while (f[x]<f[1]) x--;
			y=f[x+1]; x=f[x];
		}
		for (int i=hea[x];i;i=nex[i])
			if (vet[i]==y){
				jin=i; break;
			}
	}
	dfs2(1,0);
	for (int i=1;i<ans[0];i++) printf("%d ",ans[i]);
	printf("%d\n",ans[ans[0]]);
	return 0;
}
