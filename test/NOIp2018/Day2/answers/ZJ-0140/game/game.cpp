#include<bits/stdc++.h>
using namespace std;
const int Mod = 1e9+7;
int n, m;
long long dp[1000002][4];
int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	dp[1][0] = dp[1][1] = dp[1][2] = dp[1][3] = 1;
	scanf("%d %d", &n, &m);
	for(int l = 2; l <= m; l++){
		for(int j = 0; j <= 3; j++){
			for(int i = 0; i <= 3; i++){
				if(j & 1){
					if(!(i & 2)){
						continue;
					}
				}
				(dp[l][j] += dp[l-1][i]) %= Mod;
			}
		}
	}
	long long ans = 0;
	for(int i = 0; i <= 3; i++){
		(ans += dp[m][i]) %= Mod;
	}
	printf("%lld", ans);
	return 0;
}
