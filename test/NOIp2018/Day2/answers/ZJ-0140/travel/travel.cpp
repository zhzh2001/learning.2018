#include<bits/stdc++.h>
using namespace std;
const int Maxn = 5010;
int s, e, n, m, f[Maxn], dfn[Maxn], size, jihuan[Maxn];
bool vis[Maxn], ins[Maxn], used[Maxn];
vector<int> g[5002];
void dfs(int x, int fa)
{
	printf("%d ", x);
	vis[x] = 1;
	for(int i = 0; i < g[x].size(); i++){
		int y = g[x][i];
		if(!vis[y]){
			dfs(y, x);
		}
	}
}
int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d %d", &n, &m);
	for(int i = 1; i <= m; i++){
		int x, y;
		scanf("%d %d", &x, &y);
		g[x].push_back(y);
		g[y].push_back(x);
	}
	for(int i = 1; i <= n; i++){
		sort(g[i].begin(), g[i].end());
	}
	dfs(1, 0); 
	return 0;
}
