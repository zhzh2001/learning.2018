#include<bits/stdc++.h>
using namespace std;
const int Maxn = 1e5+10;
int n, m, c[100002], p[100002];
long long dp[100002][4];
string que;
bool flag = 0;
int head[Maxn], g[2*Maxn], nxt[2*Maxn], cnt;
void addedge(int x, int y)
{
	cnt++;
	g[cnt] = y;
	nxt[cnt] = head[x];
	head[x] = cnt;
}
void dfs(int x, int fa)
{
	for(int i = head[x]; i; i = nxt[i]){
		if(g[i] != fa){
			dfs(g[i], x);
			if(c[g[i]] == 1){
				dp[x][1] += dp[g[i]][1];
				dp[x][2] += dp[g[i]][1];
			}
			if(c[g[i]] == 0){
				dp[x][1] += dp[g[i]][2];
				if(c[x] == 0){
					flag = 1;
					c[x] = 1;
				}
			}
			if(c[g[i]] == -1){
				dp[x][1] += min(dp[g[i]][1], dp[g[i]][2]);
				dp[x][2] += dp[g[i]][1];
			}
		}				
	}
	dp[x][1] += p[x];
	printf("%d %lld %lld\n", x, dp[x][1], dp[x][2]);
}
int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d %d", &n, &m);
	cin>>que;
	for(int i = 1; i <= n; i++){
		scanf("%d", &p[i]);
	}
	for(int i = 1; i < n; i++){
		int x, y;
		scanf("%d %d", &x, &y);
		addedge(x, y);
		addedge(y, x);
	}
	for(int i = 1; i <= m; i++){
		memset(c, -1, sizeof(c));
		memset(dp, 0, sizeof(dp));
		flag = 0;
		int x, y;
	 	int b1, b2;
	 	scanf("%d %d %d %d", &x, &b1, &y, &b2);
	 	c[x] = b1, c[y] = b2;
	 	dfs(1, 0);
	 	if(flag == 1) {
	 		printf("-1\n");
	 		continue;
		 }
	 	if(c[1] == 1) printf("%lld\n", dp[1][1]);
	 	if(c[1] == 0) printf("%lld\n", dp[1][2]);
	 	if(c[1] == -1) printf("%lld\n", min(dp[1][1], dp[1][2]));
	}
	return 0;
}
