#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int MAXN=100005;
const long long MAXM=10000000000000000;
typedef long long LL;
struct Ed{
	int tot,lnk[MAXN],nxt[MAXN<<1],son[MAXN<<1];
	void Add(int x,int y){nxt[++tot]=lnk[x];lnk[x]=tot;son[tot]=y;}
}E;
int n,m,Dep[MAXN],U[MAXN],a[MAXN];
char ch[10];
LL f[MAXN][2],g[MAXN][2];
int read(){
	int ret=0;char ch=getchar();bool f=1;
	for(;ch<'0'||'9'<ch;ch=getchar()) f^=!(ch^'-');
	for(;'0'<=ch&&ch<='9';ch=getchar()) ret=ret*10+ch-48;
	return f?ret:-ret;
}
void DFS(int x,int fa){
	U[x]=fa;Dep[x]=Dep[fa]+1;
	for(int j=E.lnk[x];j;j=E.nxt[j])
	if(E.son[j]!=fa){
		DFS(E.son[j],x);
		if(f[x][1]<MAXM) f[x][1]+=min(f[E.son[j]][0],f[E.son[j]][1]);
		if(f[x][0]<MAXM) f[x][0]+=f[E.son[j]][1];
	}
	if(f[x][1]<MAXM) f[x][1]+=a[x];
}
//LL Wok(int x,int fx,int y,int fy){
//	int fa;
//	g[x][fx]=f[x][fx];g[y][fy]=f[y][fy];
//	g[x][fx^1]=g[y][fy^1]=MAXM;
//	while(U[x]!=U[y]){
//		if(Dep[x]<Dep[y]) swap(x,y);
//		fa=U[x];
//		g[fa][1]=min(g[x][0],g[x][1]);
//		g[fa][0]=g[x][1];
//		for(int j=E.lnk[fa];j;j=E.nxt[j])
//		if(E.son[j]!=U[fa]&&E.son[j]!=x){
//			g[fa][1]+=min(f[E.son[j]][0],f[E.son[j]][1]);
//			g[fa][0]+=f[E.son[j]][1];
//		}
//		g[fa][1]+=a[fa];
//		x=fa;
//	}
//	if(x!=1){
//		fa=U[x];
//		g[fa][1]=min(g[x][0],g[x][1])+min(g[y][0],g[y][1]);
//		g[fa][0]=g[x][1]+g[y][1];
//		for(int j=E.lnk[fa];j;j=E.nxt[j])
//		if(E.son[j]!=U[fa]&&E.son[j]!=x&&E.son[j]!=y){
//			g[fa][1]+=min(f[E.son[j]][0],f[E.son[j]][1]);
//			g[fa][0]+=f[E.son[j]][1];
//		}
//		x=fa;
//	}
//	while(x!=1){
//		fa=U[x];
//		g[fa][1]=min(g[x][0],g[x][1]);
//		g[fa][0]=g[x][1];
//		for(int j=E.lnk[fa];j;j=E.nxt[j])
//		if(E.son[j]!=U[fa]&&E.son[j]!=x){
//			g[fa][1]+=min(f[E.son[j]][0],f[E.son[j]][1]);
//			g[fa][0]+=f[E.son[j]][1];
//		}
//		g[fa][1]+=a[fa];
//		x=fa;
//	}
//	return min(g[1][0],g[1][1]);
//}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();scanf("%s",ch);
	for(int i=1;i<=n;i++) a[i]=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();E.Add(x,y),E.Add(y,x);
	}
	for(int i=1;i<=m;i++){
		memset(f,0,sizeof(f));
		int x=read(),fx=read(),y=read(),fy=read();
		if(fx==0&&fy==0){
			bool tt=0;
			for(int j=E.lnk[x];j;j=E.nxt[j]) if(E.son[j]==y){tt=1;break;}
			if(tt){printf("-1\n");continue;} 
		}
//		printf("%lld\n",Wok(x,fx,y,fy));
		f[x][fx^1]=MAXM,f[y][fy^1]=MAXM;DFS(1,0);
		printf("%lld\n",min(f[1][0],f[1][1]));
	}
	return 0;
}
