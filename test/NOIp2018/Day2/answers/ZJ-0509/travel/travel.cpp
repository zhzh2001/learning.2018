#include<cstdio>
#include<algorithm>
using namespace std;
const int MAXN=5005;
int n,m,hd,tl,qu_[MAXN],q_Top,q[MAXN],Ans[MAXN],mp[MAXN][MAXN];bool vis[MAXN],W[MAXN][MAXN];
struct Ed{
	int tot,lnk[MAXN],nxt[MAXN<<1],son[MAXN<<1],F[MAXN];
	void Add(int x,int y){nxt[++tot]=lnk[x];F[tot]=x;son[tot]=y;lnk[x]=tot;}
}E;
void DFS(int x){
	vis[x]=1;q[++q_Top]=x;
	for(int j=E.lnk[x];j;j=E.nxt[j])
	if(!vis[E.son[j]]&&!W[x][E.son[j]]) DFS(E.son[j]);
}
bool BFS(int x){
	for(int i=1;i<=n;i++) vis[i]=0;
	hd=0;qu_[tl=1]=x;vis[x]=1;
	while(hd!=tl){
		x=qu_[++hd];
		for(int j=E.lnk[x];j;j=E.nxt[j])
		if(!vis[E.son[j]]&&!W[x][E.son[j]]) vis[E.son[j]]=1,qu_[++tl]=E.son[j];
	}
	for(int i=1;i<=n;i++) if(!vis[i]) return 0;
	return 1;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1,x,y;i<=m;i++){
		scanf("%d%d",&x,&y);
		mp[x][++mp[x][0]]=y;
		mp[y][++mp[y][0]]=x;
	}
	for(int i=1;i<=n;i++){
		sort(mp[i]+1,mp[i]+1+mp[i][0]);
		for(int j=mp[i][0];j;j--) E.Add(i,mp[i][j]);
	}
	if(n==m){
		for(int j=1;j<=E.tot;j++){
			W[E.F[j]][E.son[j]]=W[E.son[j]][E.F[j]]=1;
			if(BFS(1)){
				for(int i=1;i<=n;i++) vis[i]=0;
				q_Top=0;DFS(1);
				bool tt=0;
				for(int i=1;i<=n;i++)
				if(Ans[i]<q[i]){tt=0;break;}else
				if(Ans[i]>q[i]){tt=1;break;}
				if(tt||Ans[1]==0)
				for(int i=1;i<=n;i++) Ans[i]=q[i];
			}
			W[E.F[j]][E.son[j]]=W[E.son[j]][E.F[j]]=0;
		}
	}else{
		DFS(1);
		for(int i=1;i<=n;i++) Ans[i]=q[i];
	}
	for(int i=1;i<=n;i++){
		if(i==n) printf("%d\n",Ans[i]);
		else printf("%d ",Ans[i]);
	}
	return 0;
}
