#include <cstdio>
#include <cstdlib>
#include <set>
#include <algorithm>
#include <vector>
using namespace std;
#define rep(x,y,z) for (int x=y;x<=z;x++)
#define per(x,y,z) for (int x=y;x>=z;x--)
typedef long long LL;
void read(int &x){x=0;char ch=getchar();while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}}
void read(int &x,int &y){read(x);read(y);}
void read(int &x,int &y,int &z){read(x);read(y);read(z);}
const int maxn=5555;
int n,m,ans[maxn],tot,circle[maxn],ctot,degree[maxn];
bool ifincircle[maxn],ifvisited[maxn],ifvisit[maxn];
vector <int> vv[maxn];
// struct GRAPH
// {
	// int to[maxn*2],nxt[maxn*2],cnt,first[maxn];
	// void init(){cnt=0;}
	// void addedge(int u,int v){to[++cnt]=v;nxt[cnt]=first[u];first[u]=cnt;}
	// void addedge2(int u,int v){addedge(u,v);addedge(v,u);}
	// void dfs(int fa,int x)
	// {
		// for (int q=first[x];q!=0;q=nxt[q])
		// {
			// if (to[q]==fa) continue;
			// 
		// }
	// }
// }graph;
void dfs(int x,int fa)
{
	ans[++tot]=x;
	for (vector <int>::iterator it=vv[x].begin();it!=vv[x].end();it++)
	{
		if (*it==fa) continue;
		dfs(*it,x);
	}
}
bool ccfgood,ccfdone;
int target1;
void dfs2(int x,int fa)
{
	if (ccfdone) return;
	ifvisited[x]=true;
	for (vector <int>::iterator it=vv[x].begin();it!=vv[x].end();it++)
	{
		if (ccfdone) return;
		if (*it==fa) continue;
		if (ifvisited[*it]) {ccfgood=true;target1=*it;circle[++ctot]=x;return;}
		dfs2(*it,x);
		if (ccfgood) 
		{
			if (ccfdone) return;
			circle[++ctot]=x;
			if (target1==x) {ccfdone=true;}
			return;
		}
	}
}
bool used;
void dfs3(int x,int fa,int temp)
{
	ans[++tot]=x;
	ifvisit[x]=true;
	if (ifincircle[x]) 
	{
		for (vector <int>::iterator it=vv[x].begin();it!=vv[x].end();it++)
		{
			if (*it==fa) continue;
			if (ifvisit[*it]) continue;
			it++;
			int tt=temp;
			if (it!=vv[x].end()) tt=*it;
			it--;
			if (ifincircle[*it]&&temp<*it&&!used) {used=true;continue;}
			dfs3(*it,x,tt);
		}

	}
	else for (vector <int>::iterator it=vv[x].begin();it!=vv[x].end();it++)
	{
		if (*it==fa) continue;
		if (ifvisit[*it]) continue;
		dfs3(*it,x,0x3f3f3f3f);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n,m);
	tot=0;ctot=0;ccfdone=0;ccfgood=0;
	int x,y;
	rep(i,1,m)
	{
		read(x,y);
		vv[x].push_back(y);
		vv[y].push_back(x);
		degree[x]++;
		degree[y]++;
	}
	rep(i,1,n) sort(vv[i].begin(),vv[i].end());
	// graph.init();
	if (m==n-1)
	{
		dfs(1,0);
		printf("%d",ans[1]);
		rep(i,2,n) printf(" %d",ans[i]);
		putchar('\n');
		// rep(i,1,m) {read(x,y);graph.addedge2(x,y);}
		// graph.dfs();
	}
	else
	{
		ccfgood=false;
		dfs2(1,0);
		rep(i,1,ctot) ifincircle[circle[i]]=true;
// putchar('\n');
// rep(i,1,ctot) printf("%d ",circle[i]);
// putchar('\n');
		used=false;
		dfs3(1,0,0x3f3f3f3f);
		printf("%d",ans[1]);
		rep(i,2,n) printf(" %d",ans[i]);
		putchar('\n');
	}
	return 0;
}