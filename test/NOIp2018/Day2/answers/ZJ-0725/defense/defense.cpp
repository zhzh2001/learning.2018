#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;
#define rep(x,y,z) for (int x=y;x<=z;x++)
#define per(x,y,z) for (int x=y;x>=z;x--)
typedef long long LL;
void read(int &x){x=0;char ch=getchar();while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}}
void read(int &x,int &y){read(x);read(y);}
void read(int &x,int &y,int &z){read(x);read(y);read(z);}
int n,m;
int main()
{
	freopen("defence.in","r",stdin);
	freopen("defence.out","w",stdout);
	read(n,m);

	printf("%d\n",n+m);
	return 0;
}