#include<bits/stdc++.h>
#define LL long long
using namespace std;
const int M=1e5+5;
const LL INF=1e18;
bool cur1;
int n,m,a,qx,b,qy;
LL A[M],dp[M][2];
vector<int>E[M];
char ty[5];
void dfs(int x,int f){
	dp[x][0]=0,dp[x][1]=A[x];
	for(int i=0;i<(int)E[x].size();i++){
		int y=E[x][i];
		if(y==f)continue;
		dfs(y,x);
		dp[x][0]+=dp[y][1];
		dp[x][1]+=min(dp[y][0],dp[y][1]);
	}
	if(x==a)dp[a][qx^1]=INF;
	if(x==b)dp[b][qy^1]=INF;
}
bool cur2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,ty);
	for(int i=1;i<=n;i++)
		scanf("%lld",&A[i]);
	for(int i=1;i<n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		E[x].push_back(y);
		E[y].push_back(x);
	}
	for(int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&qx,&b,&qy);
		dfs(a,0);
		if(dp[a][qx]>=INF)puts("-1");
		else printf("%lld\n",dp[a][qx]);
	}
	return 0;
}
