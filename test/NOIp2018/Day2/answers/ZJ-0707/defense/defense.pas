var
        t:char;
        i:longint;
        n,m,a,x,b,y,top,u,v,p,j:int64;
        head,fa,deep,cost:array[1..100005]of int64;
        next,go:array[1..200005]of int64;
        f,f2:array[1..100005,0..1]of int64;
procedure add(u,v:int64);
begin
        inc(top);
        next[top]:=head[u];
        head[u]:=top;
        go[top]:=v;
end;
function min(a,b:int64):int64;
begin
        if a<b then exit(a);
        exit(b);
end;
procedure dfs(u,dep:int64);
var
        i,v:int64;
begin
        deep[u]:=dep;
        i:=head[u];
        f[u,1]:=cost[u];
        while i>0 do
        begin
                v:=go[i];
                if v=fa[u] then
                begin
                        i:=next[i];
                        continue;
                end;
                fa[v]:=u;
                dfs(v,dep+1);
                f[u,0]:=f[u,0]+f[v,1];
                f[u,1]:=f[u,1]+min(f[v,0],f[v,1]);
                i:=next[i];
        end;
end;
begin
        assign(input,'defense.in');reset(input);
        assign(output,'defense.out');rewrite(output);
        readln(n,m);
        for i:=1 to n do
        read(cost[i]);
        for i:=1 to n-1 do
        begin
                read(u,v);
                add(u,v);
                add(v,u);
        end;
        dfs(1,0);
        for i:=1 to m do
        begin
                read(a,x,b,y);
                p:=a;
                while p>1 do
                begin
                        p:=fa[p];
                        f2[p,0]:=f[p,0];
                        f2[p,1]:=f[p,1];
                end;
                p:=b;
                while p>1 do
                begin
                        p:=fa[p];
                        f2[p,0]:=f[p,0];
                        f2[p,1]:=f[p,1];
                end;
                if (x=0)and(y=0)and((fa[a]=b)or(fa[b]=a)) then
                begin
                        writeln(-1);
                        continue;
                end;
                f2[a,1-x]:=maxlongint;
                f2[a,x]:=f[a,x];
                f2[b,1-y]:=maxlongint;
                f2[b,y]:=f[b,y];
                while deep[a]<>deep[b] do
                begin
                        if deep[a]>=deep[b] then
                        begin
                                f2[fa[a],0]:=f2[fa[a],0]-f[a,1]+f2[a,1];
                                f2[fa[a],1]:=f2[fa[a],1]-min(f[a,0],f[a,1])+min(f2[a,0],f2[a,1]);
                                a:=fa[a];
                        end
                        else
                        begin
                                f2[fa[b],0]:=f2[fa[b],0]-f[b,1]+f2[b,1];
                                f2[fa[b],1]:=f2[fa[b],1]-min(f[b,0],f[b,1])+min(f2[b,0],f2[b,1]);
                                b:=fa[b];
                        end;
                end;
                if a=b then b:=1;
                while (a>1)or(b>1) do
                begin
                        if deep[a]>=deep[b] then
                        begin
                                f2[fa[a],0]:=f2[fa[a],0]-f[a,1]+f2[a,1];
                                f2[fa[a],1]:=f2[fa[a],1]-min(f[a,0],f[a,1])+min(f2[a,0],f2[a,1]);
                                a:=fa[a];
                        end
                        else
                        begin
                                f2[fa[b],0]:=f2[fa[b],0]-f[b,1]+f2[b,1];
                                f2[fa[b],1]:=f2[fa[b],1]-min(f[b,0],f[b,1])+min(f2[b,0],f2[b,1]);
                                b:=fa[b];
                        end;
                end;
                writeln(min(f2[1,0],f2[1,1]));
        end;
        close(input);
        close(output);
end.
