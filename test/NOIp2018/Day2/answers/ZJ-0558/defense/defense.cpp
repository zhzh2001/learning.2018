#include <bits/stdc++.h>
using namespace std;
int n,m,a,b,x,y,MIN,P[11];
char A,B,C;
bool f[11][11],xxx[11];
void dfs(int Step,int Cost)
{
	if (Step>n)
	{
		bool OK=true;
		for (int i=1;i<=n;i++)
		    for (int j=1;j<=n;j++)
		        if (f[i][j])
		            if ((!xxx[i])&&(!xxx[j]))
		            	OK=false;
		if ((OK)&&(xxx[a]==x)&&(xxx[b]==y))
		    MIN=min(MIN,Cost);
		return;
	}
	xxx[Step]=true;
	dfs(Step+1,Cost+P[Step]);
	xxx[Step]=false;
	dfs(Step+1,Cost);
	return;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%c%c%c",&n,&m,&A,&B,&C);
	for (int i=1;i<=n;i++)
	    scanf("%d",&P[i]);
	for (int i=1;i<n;i++)
	{
		scanf("%d%d",&x,&y);
		f[x][y]=true;
		f[y][x]=true;
	}
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if ((f[a][b])&&(!x)&&(!y))
		{
			printf("-1\n");
			continue;
		}
		MIN=1<<30;
		dfs(1,0);
		printf("%d\n",MIN);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
