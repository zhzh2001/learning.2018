const inf=5005;
var num,fa:array[1..5000]of longint;
    flag:array[1..5000]of boolean;
    edge:array[1..5000,1..5000]of longint;
    n,m,i,u,v:longint;
procedure add(u,v:longint);
begin
  inc(num[u]);
  edge[u,num[u]]:=v;
end;
procedure dfs(u,sum:longint);
var min,v:longint;
begin
  if sum>n then exit;
  if u=0 then exit;
  min:=inf;
  for i:=1 to num[u] do
  begin
    v:=edge[u,i];
    if (flag[v])and(v<min) then min:=v;
  end;
  if min=inf then dfs(fa[u],sum)
    else
    begin
      flag[min]:=false;
      fa[min]:=u;
      write(min,' ');
      dfs(min,sum+1);
    end;
end;
begin
  assign(input,'travel.in');reset(input);
  assign(output,'travel.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
  begin
    readln(u,v);
    add(u,v);
    add(v,u);
  end;
  write(1,' ');
  for i:=2 to n do flag[i]:=true;
  dfs(1,1);
  close(input);
  close(output);
end.