#include<bits/stdc++.h>
const int mod=1e9+7,N=10;
using namespace std;
int n,m,dp[1<<N][2],now;
void dfs(int k,int p,int res)
{
	if(k>n)
	{
		dp[res][now]=(dp[res][now]+dp[p][1-now])%mod;
		cout<<res<<endl;
		return;
	}
	if(k==1)
	{
		dfs(k+1,p,res);
		res=(1<<(n-1));
		dfs(k+1,p,res);
		return;
	}
	int t=p>>(n-k+1)&1;
	if(t==1)
	{
		dfs(k+1,p,res);
		res+=(1<<(n-k));
		dfs(k+1,p,res);
	}else dfs(k+1,p,res);
}
int main()
{
	scanf("%d%d",&n,&m);
	for(int i=0;i<(1<<n);i++)
	{
		cout<<i<<endl;
		dfs(1,i,0);
		cout<<endl;
	}
	/*for(int i=0;i<(1<<n);i++)dp[i][0]=1;
	now=0;
	for(int i=1;i<m;i++)
	{
		now=1-now;
		for(int j=0;j<(1<<n);j++)dfs(1,j,0);
	}
	int ans=0;
	for(int i=0;i<(1<<n);i++)
		ans=(ans+dp[i][now])%mod;
	printf("%d\n",ans);*/
	return 0;
}
