#include<bits/stdc++.h>
using namespace std;
const int N=3e5+5;
char type[10];
int head[N*2],tot,n,m,a[N],A,B,X,Y,f[N];
long long dp[N][2];
struct node
{
	int vet,nxt;
}edge[N*2];
void add(int u,int v)
{
	edge[++tot].vet=v;
	edge[tot].nxt=head[u];
	head[u]=tot;
}
int read()
{
	int x=0,f=1;char s=getchar();
	while(s>'9'||s<'0'){if(s=='-')f=-1;s=getchar();}
	while(s<='9'&&s>='0'){x=x*10+s-'0';s=getchar();}
	return x*f;
}
void dfs(int u,int fa)
{
	bool flag1=0,flag2=0;
	int cnt=0;
	for(int i=head[u];i;i=edge[i].nxt)
	{
		int v=edge[i].vet;
		if(v!=fa)
		{
			cnt++;
			dfs(v,u);
			if(f[v]==1){dp[u][1]+=dp[v][0];f[u]=2;}else
			if(f[v]==2){dp[u][0]+=dp[v][1];f[u]=1;}else
			{
				if(f[u]!=2)dp[u][1]+=dp[v][0];
				if(f[u]!=1)dp[u][0]+=dp[v][1];
			}
		}
	}
	dp[u][1]+=a[u];
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();
	scanf("%s",type);
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++)
	{
		int u=read(),v=read();
		add(u,v);add(v,u);
	}
	while(m--)
	{
		A=read(),X=read(),B=read(),Y=read();
		f[A]=X+1;f[B]=Y+1;
		memset(dp,0,sizeof(dp));
		dfs(1,0);
		long long ans;
		if(f[1]==1)ans=dp[1][0];else
			if(f[1]==2)ans=dp[1][1];else
				ans=min(dp[1][1],dp[1][0]);
		if(!ans)puts("-1");else printf("%lld\n",ans);
		f[A]=0,f[B]=0;
	}
	return 0;
}
