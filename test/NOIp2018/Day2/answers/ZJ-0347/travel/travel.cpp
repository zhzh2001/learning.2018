#include<bits/stdc++.h>
using namespace std;
const int N=5005;
int n,m,dist[N][N],top,a[N],flag,head[N*2],tot,vis[N];
int t,f[N],k=0,tt,ff=0;
int read()
{
	int x=0,f=1;char s=getchar();
	while(s>'9'||s<'0'){if(s=='-')f=-1;s=getchar();}
	while(s<='9'&&s>='0'){x=x*10+s-'0';s=getchar();}
	return x*f;
}
struct node
{
	int vet,nxt;
}edge[N*2];
void add(int u,int v)
{
	edge[++tot].vet=v;
	edge[tot].nxt=head[u];
	head[u]=tot;
}
int cmp(int x,int y)
{
	return x<y;
}
void dfs(int u,int fa)
{
	a[++top]=u;
	for(int i=1;i<=n;i++)
		if(dist[u][i]&&fa!=i)dfs(i,u);
}
void Dfs(int u,int fa)
{
	vis[u]=1;
	for(int i=head[u];i;i=edge[i].vet)
	{
		if(!flag)return;
		int v=edge[i].vet;
		if(fa!=v)
		{
			if(vis[v])
			{
				flag=0;
				t=v;
				return;
			}
			Dfs(v,u);
		}
	}
}
void dfs1(int u,int fa,int top)
{
	for(int i=head[u];i;i=edge[i].nxt)
	{
		if(!flag)break;
		int v=edge[i].vet;
		if(v!=fa)
		{
			if(v==top){f[u]=1;flag=0;return;}
			dfs1(v,u,top);
		}
	}
	if(!flag)f[u]=1;
}
void dfs2(int u,int fa)
{
	if(!vis[u])a[++top]=u;
	vis[u]=1;
	for(int i=1;i<=n;i++)
		if(dist[u][i]&&i!=fa&&!vis[i])
		{
			if(ff){dfs2(i,fa);continue;}
			if(f[u]&&f[i]&&!k)
			{
				for(int j=1;j<=n;j++)
				{
					if(j!=i&&j!=fa&&f[j]&&dist[u][j])
					{
						k=j;tt=u;
						break;
					}
				}
				dfs2(i,fa);
			}else
			if(f[u]&&f[i])
			{
				if(k<i)
				{
					ff=1;
					return;
				}
				if(!vis[i])dfs2(i,u);
			}else dfs2(i,fa);
		}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++)
	{
		int u=read(),v=read();
		add(u,v);add(v,u);
		dist[u][v]=dist[v][u]=1;
	}
	if(m==n-1)dfs(1,0);else
	{
		flag=1;
		Dfs(1,0);
		flag=1;
		dfs1(t,0,t);
		for(int i=1;i<=n;i++)vis[i]=0;
		dfs2(1,0);
	}
	for(int i=1;i<n;i++)printf("%d ",a[i]);
	printf("%d\n",a[n]);
	return 0;
}
