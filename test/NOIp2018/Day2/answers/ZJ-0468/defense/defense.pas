var
    w,head:array[1..100000]of longint;
    vet,next:array[1..200000]of longint;
    f:array[1..100000,0..1]of int64;
    n,q,x,y,a,b,i,wa,wb,tot:longint;
    ch1,ch2,ch3:char;
function min(a,b:int64):int64;
begin
    if a<b then exit(a) else exit(b);
end;
procedure add(x,y:longint);
begin
    inc(tot);
    next[tot]:=head[x];
    vet[tot]:=y;
    head[x]:=tot;
end;
procedure dfs(u,father:longint);
var
    i,v,mi:longint;
    flag:boolean;
begin
    f[u,0]:=w[u];
    i:=head[u]; flag:=false;
    while i<>0 do
    begin
        v:=vet[i];
        if v<>father then
        begin
            dfs(v,u);
            f[u,0]:=f[u,0]+min(f[v,0],f[v,1]);
            f[u,1]:=f[u,1]+f[v,0];
        end;
        i:=next[i];
    end;
end;
begin
    assign(input,'defense.in');reset(input);
    assign(output,'defense.out');rewrite(output);
    readln(n,q,ch1,ch2,ch3);
    for i:=1 to n-1 do
        read(w[i]);
    readln(w[n]);
    for i:=1 to n-1 do
    begin
        readln(x,y);
        add(x,y); add(y,x);
    end;
    while q>0 do
    begin
        fillchar(f,sizeof(f),0);
        readln(a,x,b,y);
        if ch3='2' then writeln(-1) else
        begin
            wa:=w[a]; wb:=w[b];
            if x=0 then w[a]:=maxlongint else f[a,1]:=maxlongint;
            if y=0 then w[b]:=maxlongint else f[b,1]:=maxlongint;
            dfs(1,0);
            if min(f[1,0],f[1,1])>=maxlongint then writeln(-1)
                else writeln(min(f[1,0],f[1,1]));
                w[a]:=wa; w[b]:=wb;
        end;
        dec(q);
    end;
    close(input);
    close(output);
end.