var
    next,vet:array[1..10000]of longint;
    flag:array[1..5000,1..5000]of boolean;
    head:array[1..5000]of longint;
    vis:array[1..5000]of boolean;
    i,n,m,x,y,tot:longint;
procedure add(x,y:longint);
begin
    inc(tot);
    next[tot]:=head[x];
    vet[tot]:=y;
    head[x]:=tot;
end;
procedure dfs(u:longint);
var
    i,v:longint;
begin
    write(u,' ');
    vis[u]:=true;
    i:=head[u];
    while i<>0 do
    begin
        v:=vet[i];
        if not vis[v] then
            flag[u,v]:=true;
        i:=next[i];
    end;
    for i:=1 to n do
        if (flag[u,i])and(not vis[i]) then dfs(i);
end;
begin
    assign(input,'travel.in');reset(input);
    assign(output,'travel.out');rewrite(output);
    read(n,m);
    for i:=1 to m do
    begin
        read(x,y);
        add(x,y); add(y,x);
    end;
    dfs(1);
    writeln;
    close(input);
    close(output);
end.