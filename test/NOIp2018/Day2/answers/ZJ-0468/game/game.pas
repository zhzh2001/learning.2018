var
    map:array[0..20,0..20]of integer;
    b:array[1..100000]of string;
    a:array[1..100000]of longint;
    n,m,tot,t:longint;
    oo,ans:int64;
function check:boolean;
var
    i,j:longint;
begin
    check:=true;
    for i:=1 to tot-1 do
         for j:=i+1 to tot do
            if (b[i]>b[j])and(a[i]>a[j]) then exit(false);
end;
procedure baoli(x,y,s:longint;p:string);
begin
    if (x=n)and(y=m) then
    begin
        inc(tot);
        a[tot]:=s; b[tot]:=p;
        exit;
    end;
    if x+1<=n then baoli(x+1,y,s*2+map[x+1,y],p+'R');
    if y+1<=m then baoli(x,y+1,s*2+map[x,y+1],p+'D');
end;
procedure dfs(x,y:longint);
var
    i,j:longint;
begin
    if x>n then
    begin
        tot:=0;
        baoli(1,1,map[1,1],'');
        if check then inc(ans);
        exit;
    end;
    i:=x; j:=y+1;
    if j>m then begin j:=1; inc(i); end;
    map[x,y]:=0; dfs(i,j);
    map[x,y]:=1; dfs(i,j);
end;
function power(n,m:int64):int64;
begin
    if m=0 then exit(1);
    if m mod 2=0 then exit(sqr(power(n,m div 2))mod oo)
    else exit(sqr(power(n,m div 2))mod oo*n mod oo);
end;
begin
    assign(input,'game.in');reset(input);
    assign(output,'game.out');rewrite(output);
    oo:=1000000007;
    fillchar(map,sizeof(map),255);
    read(n,m);
    if n>m then begin t:=n; n:=m; m:=t; end;
    if n=2 then writeln(4*power(3,m-1)mod oo) else
    if n=3 then writeln(112*power(3,m-3)mod oo) else
    if (n=4)and(m=4) then writeln(912) else
    if (n=5)and(m=5) then writeln(7136) else
    if (n=4)and(m=5) then writeln(2688) else
    begin
        dfs(1,1);
        writeln(ans);
    end;
    close(input);
    close(output);
end.