#include<bits/stdc++.h>
using namespace std;
#define fo(i,j,k) for(int i=(j);i<=(k);i++)
#define fd(i,j,k) for(int i=(j);i>=(k);i--)
#define fe(x,i) for(int i=head[x];i!=-1;i=mp[i].next)
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
struct node
{
	int from,to,next;
}mp[500001];
int n,m,tot;
int head[100001];
int v[100001];
int used[2001],vis[2001];
long long ans=0;
char type[100];
int tag[5001];
int cnt=0;
long long dp[2001][2];
void read(int &x)
{
	char c=getchar();int b=1;
	for(;c<'0'||c>'9';c=getchar()) if (c=='-') b=-1;
	for(x=0;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	x*=b;
}
void addedge(int x,int y)
{
	mp[++tot].from=x;mp[tot].to=y;mp[tot].next=head[x];head[x]=tot;
	mp[++tot].from=y;mp[tot].to=x;mp[tot].next=head[y];head[y]=tot;
}
void dfs(int x,int fa)
{
	vis[x]=1;
	int flag=0;
	fe(x,i)
	{
		if (tag[i]==0||mp[i].to==fa) continue;
		dfs(mp[i].to,x);
		dp[x][0]+=dp[mp[i].to][1];
		dp[x][1]+=min(dp[mp[i].to][0],dp[mp[i].to][1]);
		flag=1;
	}
	if (!flag) dp[x][0]=0,dp[x][1]=v[x];
	else dp[x][1]+=v[x];
	if (fa==0)
	ans+=min(dp[x][0],dp[x][1]);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof(head));
	read(n);read(m);scanf("%s",type);
	fo(i,1,n)
	read(v[i]);
	fo(i,1,n)
	if (n%2==1)
	ans+=v[i];
	fo(i,1,n-1)
	{
		int x,y;
		read(x);read(y);
		addedge(x,y);
	}
	if (n<=2000&&m<=2000)
	{
		fo(i,1,m)
		{
			cnt=0;
			ans=0;
			memset(tag,0,sizeof(tag));
			memset(used,0,sizeof(used));
			memset(vis,0,sizeof(vis));
			memset(dp,0,sizeof(dp));
			int a,b,x,y;
			read(a);read(x);read(b);read(y);
			if (x==0) x=-1;
			if (y==0) y=-1; 
			used[a]=x;used[b]=y;
			if (x==1) ans+=v[a];
			if (y==1) ans+=v[b];
			int flag=0;
			fo(i,1,n-1)
			{
				int u=mp[i*2-1].from,t=mp[i*2-1].to;
				if (used[u]==-1&&used[t]==-1)
				{
					flag=1;
					break;
				}
				if (used[u]==1||used[t]==1) continue;
				if (used[u]==-1) used[t]=1,ans+=v[t];
				if (used[t]==-1) used[u]=1,ans+=v[u];
			}
			if (flag) 
			{
				used[a]=used[b]=0;
				printf("-1\n");
				continue;
			}
			fo(i,1,n-1)
			{
				int u=mp[i*2-1].from,t=mp[i*2-1].to;
				if (used[u]==0&&used[t]==0) tag[i*2-1]=tag[i*2]=1,cnt++;
			}
			if (cnt==0)
			{
				printf("%lld\n",ans);	
				continue;
			}
			fo(i,1,n)
			if (!vis[i])
			dfs(i,0);
			used[a]=used[b]=0;	
			printf("%lld\n",ans);
		}
		return 0;
	}
	fo(i,1,m)
	{
		int a,b,x,y;
		read(a);read(x);read(b);read(y);
		if (x==0&&y==0) printf("%lld\n",ans);
		else printf("%lld\n",ans-v[x]-v[y]+v[x+1]+v[y+1]);
	}
	return 0;
}
