#include<bits/stdc++.h>
using namespace std;
#define fo(i,j,k) for(int i=(j);i<=(k);i++)
#define fd(i,j,k) for(int i=(j);i>=(k);i--)
#define fe(x,i) for(int i=head[x];i!=-1;i=mp[i].next)
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
void read(int &x)
{
	char c=getchar();int b=1;
	for(;c<'0'||c>'9';c=getchar()) if (c=='-') b=-1;
	for(x=0;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	x*=b;
}
vector<int> mp[10001];
int n,m,tot,cnt,top,flag=0,gg=0,t2,t1,t3,t4,minn=1e+9;
int head[10001],vis[10001],ans[10001];
int sta[10001],incir[10001];
void dfs(int x,int fa)
{
	ans[++cnt]=x;
	fo(i,0,(int)mp[x].size()-1)
	{
		if (mp[x][i]==fa) continue;
		dfs(mp[x][i],x);
	}
}
void dfs2(int x,int fa)
{
	if (flag) return;
	if (vis[x])
	{
		while (sta[top]!=x)
		{
			incir[sta[top]]=1;
			top--;
		}
		incir[x]=1;
		top--;
		flag=x;
		return;
	}
	sta[++top]=x;
	vis[x]=1;
	fo(i,0,(int)mp[x].size()-1)
	{
		if (mp[x][i]==fa) continue;
		dfs2(mp[x][i],x);
		if (flag) return;
	}
	top--;
	if (flag) return;
}
void solve(int x,int fa)
{
	ans[++cnt]=x;
	vis[x]=1;
	if (x==t3) return;
	fo(i,0,(int)mp[x].size()-1)
	{
		if (mp[x][i]==fa||vis[mp[x][i]]) continue;
		solve(mp[x][i],x);
	}
}
void dfs3(int x,int fa)
{
	if (mp[x].size()>=3&&x!=flag&&t3==0)
	{
		int fss=0;
		fo(i,0,(int)mp[x].size()-1)
		if (mp[x][i]!=fa)
		{
			if (incir[mp[x][i]]) fss=1;
			break;
		} 
		if (fss)
		{
			t1=x;
			fo(i,0,(int)mp[x].size()-1)
			if (!incir[mp[x][i]])
			{
				minn=mp[x][i];	
				break;
			}	
		}
	} 
	fo(i,0,(int)mp[x].size()-1)
	{
		if (incir[mp[x][i]]==0||mp[x][i]==fa||mp[x][i]==flag) continue;
		if (mp[x][i]>minn&&t3==0) t3=x;
		dfs3(mp[x][i],x);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(head,-1,sizeof(head));
	read(n);read(m);
	fo(i,1,m)
	{
		int x,y;
		read(x);read(y);
		mp[x].push_back(y);
		mp[y].push_back(x);
	}
	if (m==n-1)
	{
		fo(i,1,n)
		sort(mp[i].begin(),mp[i].end());
		dfs(1,0);
		fo(i,1,cnt-1)
		printf("%d ",ans[i]);
		printf("%d\n",ans[cnt]);
		return 0;
	}
	if (m==n)
	{
		fo(i,1,n)
		sort(mp[i].begin(),mp[i].end());
		dfs2(1,0);
		int ggg=0;
		fo(i,0,(int)mp[flag].size()-1)
		{
			if (incir[mp[flag][i]]) ggg++;
			if (ggg==2)
			{
				minn=mp[flag][i];
				break;
			}
		}
		t1=-1;
		dfs3(flag,0);
		memset(vis,0,sizeof(vis));
		solve(1,0);
		fo(i,1,cnt-1)
		printf("%d ",ans[i]);
		printf("%d\n",ans[cnt]);
		return 0;
	}
	return 0;
}
