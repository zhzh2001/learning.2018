#include<bits/stdc++.h>
using namespace std;
#define fo(i,j,k) for(int i=(j);i<=(k);i++)
#define fd(i,j,k) for(int i=(j);i>=(k);i--)
#define fe(x,i) for(int i=head[x];i!=-1;i=mp[i].next)
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
int n,m;
long long ans=0;
int used[10][10];
int pre=0,flag=0;
long long dp[2][256];
#define mo (1000000007)
void read(int &x)
{
	char c=getchar();int b=1;
	for(;c<'0'||c>'9';c=getchar()) if (c=='-') b=-1;
	for(x=0;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	x*=b;
}
void dfs(int x,int y,int cnt)
{
	if (x==n&&y==m)
	{
		if (pre<cnt&&pre!=-1) flag=0;
		pre=cnt;
		return;
	}
	if (x<n) dfs(x+1,y,cnt*2+used[x][y]);
	if (!flag) return;
	if (y<m) dfs(x,y+1,cnt*2+used[x][y]);
	if (!flag) return;
}
void check()
{
	pre=-1;
	flag=1;
	dfs(1,1,0);
	ans+=flag;
}
void solve(int x,int y)
{
	if (y==m+1) x++,y=1;
	if (x==n+1)
	{
		check();
		return;
	}
	used[x][y]=1;
	solve(x,y+1);
	used[x][y]=0;
	solve(x,y+1);
}
long long poww(long long a,long long b)
{
	long long c=1;
	while (b)
	{
		if (b&1) c=a*c%mo;
		a=a*a%mo;
		b/=2;
	}
	return c;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n);read(m);
	if (n<=5&&m<=4)
	{
		solve(1,1);
		printf("%lld\n",ans);
		return 0;
	}
	if (n==3)
	{
		if (m==1) printf("8\n");
		if (m==2) printf("36\n");
		if (m==3) printf("112\n");
		long long ans=112;
		ans=1ll*ans*poww(3,m-3);
		printf("%lld\n",ans);
		return 0;
	}
	int s=(1<<n)-1;
	fo(i,0,s)
	dp[1][i]=1;
	int tag=0;
	fo(t,2,m)
	{
		fo(i,0,s) dp[tag][i]=0;
		dp[tag][0]=(dp[tag^1][0]+dp[tag^1][1]+dp[tag^1][2]+dp[tag^1][3])%mo;
		dp[tag][1]=(dp[tag^1][0]+dp[tag^1][1]+dp[tag^1][2]+dp[tag^1][3])%mo;
		dp[tag][2]=(dp[tag^1][1]+dp[tag^1][3])%mo;
		dp[tag][3]=(dp[tag^1][1]+dp[tag^1][3])%mo;
		tag^=1;
	}
	fo(i,0,s)
	(ans+=dp[tag^1][i])%=mo;
	printf("%lld\n",ans);
	return 0;
}
