#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=5e3+10;
int f[N][N],g[N],ff[N],a[N],n,m,fl,c[N],b[N][N];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void dfs(int t,int x)
{
	printf("%d ",t);g[t]=1;
	for (int i=1;i<=n;i++)
		if (f[t][i]&&g[i]==0) b[t][++c[t]]=i;
	for (int i=1;i<=c[t];i++)
		if (g[b[t][i]]==0)
		{
			int s=b[t][i];
			if (fl==0&&ff[t]&&ff[s]&&s>x&&i==c[t]) {fl=1;return;}
			else
			{
				if (i==c[t]) dfs(s,x);
				else dfs(s,b[t][i+1]);
			}
		}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=m;i++)
	{
		int x=read(),y=read();
		f[x][y]=1;f[y][x]=1;
		ff[x]++;ff[y]++;
	}
	int l=1,r=0;
	for (int i=1;i<=n;i++)
		if (ff[i]==1) a[++r]=i;
	while (l<=r)
	{
		for (int i=1;i<=n;i++)
			if (f[a[l]][i]&&ff[i])
			{
				ff[i]--;ff[a[l]]--;
				if (ff[i]==1) a[++r]=i;
			}
		l++;
	}
	if (r<n) fl=0; else fl=1;
	dfs(1,n+1);putchar('\n');
	return 0;
}
