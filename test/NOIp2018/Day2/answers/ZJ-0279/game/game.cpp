#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long ll;
const int N=1e6+10,mod=1e9+7;
int f[10][10],g[N][4];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int main()
{
//	freopen("game.in","r",stdin);
//	freopen("game.out","w",stdout);
	int n=read(),m=read();
	if (n==1)
	{
		int ans=1;
		for (int i=1;i<=m;i++)
			ans=(ll)ans*2%mod;
		printf("%d\n",ans);
		return 0;
	}
	if (n==2)
	{
		g[1][0]=g[1][1]=g[1][2]=g[1][3]=1;
		for (int i=2;i<=m;i++)
		{
			g[i][1]=g[i][0]=(g[i-1][0]+g[i-1][2])%mod;
			g[i][3]=g[i][2]=((ll)g[i-1][0]+g[i-1][1]+g[i-1][2]+g[i-1][3])%mod;
		}
		printf("%lld\n",((ll)g[m][0]+g[m][1]+g[m][2]+g[m][3])%mod);
		return 0;
	}
	f[1][1]=2;f[1][2]=4;f[1][3]=8;f[1][4]=16;f[1][5]=32;f[1][6]=64;f[1][7]=128;f[1][8]=256;
	f[2][1]=4;f[2][2]=12;f[2][3]=36;f[2][4]=108;f[2][5]=324;f[2][6]=972;f[2][7]=2916;f[2][8]=8748;
	f[3][1]=8;f[3][2]=36;f[3][3]=112;f[3][4]=336;f[3][5]=1008;f[3][6]=3024;f[3][7]=9072;f[3][8]=27216;
	f[4][1]=16;f[4][2]=108;f[4][3]=336;f[4][4]=912;f[4][5]=2688;f[4][6]=8064;f[4][7]=24192;f[4][8]=72576;
	f[5][1]=32;f[5][2]=324;f[5][3]=1008;f[5][4]=2688;f[5][5]=7136;f[5][6]=21312;f[5][7]=63936;f[5][8]=123552;
	f[6][1]=64;f[6][2]=972;f[6][3]=3024;f[6][4]=8064;f[6][5]=21312;f[6][6]=56768;f[6][7]=109632;f[6][8]=133632;
	f[7][1]=128;f[7][2]=2916;f[7][3]=9072;f[7][4]=24192;f[7][5]=63936;f[7][6]=109632;f[7][7]=118784;f[7][8]=307200;
	f[8][1]=256;f[8][2]=8748;f[8][3]=27216;f[8][4]=72576;f[8][5]=123552;f[8][6]=133632;f[8][7]=307200;f[8][8]=0;
	printf("%d\n",f[n][m]);
	return 0;
}
