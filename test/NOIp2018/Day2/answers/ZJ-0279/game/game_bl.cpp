#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=1e6+10;
int n,m,ans,a[10][10],la,flag;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void dfs(int x,int y,int z)
{
	if (flag) return;
	if (x==n&&y==m)
	{
		if (z<la) flag=1;
		else la=z;
		return;
	}
	if (y<m) dfs(x,y+1,z*10+a[x][y]);
	if (x<n) dfs(x+1,y,z*10+a[x][y]);
}
int ck()
{
	flag=0;la=0;
	dfs(1,1,0);
	return flag;
}
void yl(int x,int y)
{
	if (x>n)
	{
		if (!ck()) ans++;
/*		{
			for (int i=1;i<=n;i++)
				for (int j=1;j<=n;j++) printf("%d ",a[i][j]);
			puts("");puts("");
		}*/
		return;
	}
	a[x][y]=1;if (y==m) yl(x+1,1); else yl(x,y+1);
	if (a[x-1][y+1]==0)
	{
		a[x][y]=0;
		if (y==m) yl(x+1,1); else yl(x,y+1);
	}
	a[x][y]=0;
}
int main()
{
//	freopen("travel.in","r",stdin);
//	freopen("travel.out","w",stdout);
	for (n=8;n<=8;n++)
	{
		for (m=8;m<=8;m++)
		{
			ans=0;yl(1,1);
			printf("%d ",ans);
		}
		puts("");
	}
	return 0;
}
