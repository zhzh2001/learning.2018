#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long ll;
const int N=1e5+10;
int x,y,xx,yy,n,m,hed[N],fa[N],v[N];ll g[N],f[N];
int nxt[N*2],to[N*2],cnt;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void add(int x,int y){to[++cnt]=y;nxt[cnt]=hed[x];hed[x]=cnt;}
void dfs(int t)
{
	for (int i=hed[t];i;i=nxt[i])
		if (to[i]!=fa[t])
			fa[to[i]]=t,dfs(to[i]);
}
void dp(int t)
{
	for (int i=hed[t];i;i=nxt[i])
		if (to[i]!=fa[t]) dp(to[i]);
	f[t]=0;
	for (int i=hed[t];i;i=nxt[i])
		if (to[i]!=fa[t]) f[t]+=min(f[to[i]],g[to[i]]);
	g[t]=f[t];int fl=0;ll minn=1e18;
	if (x==t&&xx==1||y==t&&yy==1) {f[t]+=v[t];return;}
	for (int i=hed[t];i;i=nxt[i])
		if (to[i]!=fa[t])
		{
			if (f[to[i]]<=g[to[i]]) fl=1;
			else minn=min(minn,f[to[i]]-g[to[i]]);
		}
	if (!fl)
	{
		if (x==t&&xx==0||y==t&&yy==0) f[t]+=minn;
		else f[t]+=min(minn,(ll)v[t]);
	}
//	printf("%d %lld %lld\n",t,f[t],g[t]);
}
int main()
{
	freopen("defence.in","r",stdin);
	freopen("defence.out","w",stdout);
	n=read(),m=read();int T=read();
	for (int i=1;i<=n;i++) v[i]=read();
	for (int i=1;i<n;i++)
	{
		int x=read(),y=read();
		add(x,y),add(y,x);
	}
	dfs(1);
	while (m--)
	{
		x=read(),xx=read(),y=read(),yy=read();int fl=0;
		for (int i=hed[x];i;i=nxt[i])
			if (to[i]==y) {fl=1;break;}
		if (fl&&yy==0&&xx==0||x==y&&xx!=yy) {puts("-1");continue;}
		memset(f,127,sizeof f);memset(g,0,sizeof g);
		dp(1);printf("%lld\n",f[1]);
	}
	return 0;
}
