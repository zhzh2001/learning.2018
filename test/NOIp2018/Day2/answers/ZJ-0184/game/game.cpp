#include<bits/stdc++.h>
using namespace std;
int Mod=1e9+7;
int n,m;
struct P20 {
	char Map[15][15];
	int Ans,Id;
	string Str[105];
	void Get(int x,int y,string S) {
		S+=Map[x][y];
		if(x==n-1&&y==m-1) {
			Str[++Id]=S;
			return;
		}
		if(y!=m-1)Get(x,y+1,S);
		if(x!=n-1)Get(x+1,y,S);
	}
	void Dfs(int x,int y) {
		if(y==m)x++,y=0;
		if(x==n) {
			Id=0;
			string S="";
			Get(0,0,S);
			bool Check=true;
			for(int i=1; i<Id; i++)if(Str[i]>Str[i+1])Check=false;
			Ans+=Check;
			return;
		}
		for(int i=0; i<=1; i++) {
			Map[x][y]=i+'0';
			Dfs(x,y+1);
		}
	}
	void Solve() {
		Dfs(0,0);
		printf("%d\n",Ans);
	}
} P20;
struct P50 {
	long long Ans[1000005];
	void Solve() {
		Ans[1]=4;
		for(int i=2; i<=m; i++)(Ans[i]=Ans[i-1]*3)%=Mod;
		printf("%lld\n",Ans[m]);
	}
} P50;
struct P65 {
	long long Ans[3][1000005];
	void Solve() {
		Ans[0][1]=4;
		Ans[1][1]=8,Ans[1][2]=36;
		for(int i=2; i<=m; i++)(Ans[0][i]=Ans[0][i-1]*3)%=Mod;
		for(int i=3; i<=m; i++)(Ans[1][i]=Ans[0][i]*3+Ans[0][i-2])%=Mod;
		printf("%lld\n",Ans[1][m]);
	}
} P65;
struct P0 {
	long long Ans[1000005];
	void Solve() {
		Ans[1]=2;
		for(int i=2;i<=m;i++)(Ans[i]=Ans[i-1]*2)%=Mod;
		printf("%lld\n",Ans[m]); 
	}
} P0;
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n>m)swap(n,m);
	if(n<=3&&m<=3)P20.Solve();
	else if(n==1)P0.Solve();
	else if(n==2)P50.Solve();
	else if(n==3)P65.Solve();
	return 0;
}
