#include<bits/stdc++.h>
using namespace std;
int n,m,pa,pb,fx,fy;
char Flag[15];
int A[2005];
vector<int>Edge[2005];
int Dp[2005][5];//0代表取 1代表不取
void Dfs(int Now,int From,int f) {
	if(Now==pa&&f!=fx) {
		Dp[Now][f]=1e9;
		return;
	}
	if(Now==pb&&f!=fy) {
		Dp[Now][f]=1e9;
		return;
	}
	if(f) { //取
		Dp[Now][f]=A[Now];
		for(register int i=0; i<(int)Edge[Now].size(); i++) {
			int Nxt=Edge[Now][i];
			if(Nxt==From)continue;
			Dfs(Nxt,Now,0),Dfs(Nxt,Now,1);
			Dp[Now][f]+=min(Dp[Nxt][1],Dp[Nxt][0]);
		}
	} else {
		Dp[Now][f]=0;
		for(register int i=0; i<(int)Edge[Now].size(); i++) {
			int Nxt=Edge[Now][i];
			if(Nxt==From)continue;
			Dfs(Nxt,Now,1);
			Dp[Now][f]+=Dp[Nxt][1];
		}
	}
}
bool Check(){
	for(int i=0;i<(int)Edge[pa].size();i++){
		int Nxt=Edge[pa][i];
		if(Nxt==pb)return true;
	}
	return false;
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,Flag);
	for(int i=1; i<=n; i++)scanf("%d",&A[i]);
	for(int i=1,u,v; i<n; i++) {
		scanf("%d%d",&u,&v);
		Edge[u].push_back(v);
		Edge[v].push_back(u);
	}
	while(m--) {
		scanf("%d%d%d%d",&pa,&fx,&pb,&fy);
		if((!fx&&!fy&&Check())||pa==pb)puts("-1");
		else {
			Dfs(1,0,0),Dfs(1,0,1);
			printf("%d\n",min(Dp[1][0],Dp[1][1]));
		}
	}
	return 0;
}
