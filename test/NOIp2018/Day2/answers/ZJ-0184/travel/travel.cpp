#include<bits/stdc++.h>
using namespace std;
#define Maxn 5005
int n,m;
vector<int>Edge[Maxn+5];
struct P60 {
	int Ans[Maxn+5],Id;
	void Dfs(int Now,int From) {
		Ans[++Id]=Now;
		for(int i=0; i<(int)Edge[Now].size(); i++) {
			int Nxt=Edge[Now][i];
			if(Nxt==From)continue;
			Dfs(Nxt,Now);
		}
	}
	void Solve() {
		Dfs(1,0);
		for(int i=1; i<n; i++)printf("%d ",Ans[i]);
		printf("%d\n",Ans[n]);
	}
} P60;
struct P0 {
	int Ans[Maxn+5],Id;
	int Stack[Maxn+5];
	bool Vis[Maxn+5];//标记环
	bool Dfs(int Now,int From) {
		Stack[++Id]=Now;
		if(Vis[Now])return true;
		Vis[Now]=1;
		for(int i=0; i<(int)Edge[Now].size(); i++) {
			int Nxt=Edge[Now][i];
			if(Nxt==From)continue;
			if(Dfs(Nxt,Now))return true;
		}
		Stack[Id--]=0;
		return false;
	}
	int Way,Flag;//记录环的分叉
	bool Mark[Maxn+5];//标记是否访问过
	void Redfs(int Now,int From) {
//		printf("%d %d\n",Now,From);
		Ans[++Id]=Now;
		Mark[Now]=1;
		if(Vis[Now]) {
			if(Flag==0) { //第一次进入环
				Flag=1;
				bool Check=0;
				for(int i=0; i<(int)Edge[Now].size(); i++) {
					int Nxt=Edge[Now][i];
					if(Nxt==From)continue;
					if(Vis[Nxt]) {
						if(!Check)Check=1;//要走的点
						else Way=Nxt;//分叉记录
					}
				}
				for(int i=0; i<(int)Edge[Now].size(); i++) {
					int Nxt=Edge[Now][i];
					if(Nxt==From||Mark[Nxt])continue;
					Redfs(Nxt,Now);//遍历
				}
			} else {
				int Go,Max=0;
				for(int i=0; i<(int)Edge[Now].size(); i++) {
					int Nxt=Edge[Now][i];
					if(Nxt==From||Mark[Nxt])continue;
					if(Vis[Nxt])Go=Nxt;
					else Max=max(Max,Nxt);
				}
				if((Go>Max)&&Go>Way&&Flag==1) {
					//如果不再走此环,则必须将其非环子树遍历
					for(int i=0; i<(int)Edge[Now].size(); i++) {
						int Nxt=Edge[Now][i];
						if(Nxt==From||Vis[Nxt]||Mark[Nxt])continue;
						Redfs(Nxt,Now);
					}
					Flag=2;
				} else {
					//如果走此环,则正常遍历
					for(int i=0; i<(int)Edge[Now].size(); i++) {
						int Nxt=Edge[Now][i];
						if(Nxt==From||Mark[Nxt])continue;
						Redfs(Nxt,Now);
					}
				}
			}
		} else {
			for(int i=0; i<(int)Edge[Now].size(); i++) {
				int Nxt=Edge[Now][i];
				if(Nxt==From)continue;
				Redfs(Nxt,Now);
			}//不是环上的点就正常操作
		}
	}
	void Solve() {
		Dfs(1,0);
		memset(Vis,0,sizeof(Vis));
		int u=Stack[Id--],v;
		do {
			v=Stack[Id--];
			Vis[v]=1;
		} while(v!=u);
		Id=0;
		Redfs(1,0);
		for(int i=1; i<n; i++)printf("%d ",Ans[i]);
		printf("%d\n",Ans[n]);
	}
} P0;
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1,u,v; i<=m; i++) {
		scanf("%d%d",&u,&v);
		Edge[u].push_back(v);
		Edge[v].push_back(u);
	}
	for(int i=1; i<=n; i++)sort(Edge[i].begin(),Edge[i].end());
	if(m==n-1)P60.Solve();
	else P0.Solve();
	return 0;
}
