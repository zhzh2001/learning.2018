#include<bits/stdc++.h>
#define MAXN 5010
using namespace std;

struct edge{
	int to, Next;
}Right[MAXN<<1];
int Begin[MAXN], dfn[MAXN], low[MAXN], st[MAXN], co[MAXN], si[MAXN], num, tot, col, top, Num;
int que[MAXN], n, m, x[MAXN], y[MAXN], ans[MAXN], bian[MAXN], TOT[MAXN], Plan[MAXN][MAXN];
bool Map[MAXN][MAXN], vis[MAXN], Vis[MAXN];

inline void add_edge(int x, int y){
	Right[++tot].to=y;
	Right[tot].Next=Begin[x];
	Begin[x]=tot;
}
namespace P60{
	
	int cnt=0;
	void dfs(int u, int fa){
		if (!vis[u]) que[++cnt]=u, vis[u]=1;
		for (int i=1; i<=n; i++){
			if (Map[u][i] && i!=fa){
				dfs(i, u);
			}
		}
	}
	void main(){
		dfs(1, 0);
		for (int i=1; i<n; i++) printf("%d ", que[i]);
		printf("%d\n", que[n]);
	}
}

namespace P40{
	int cnt=0;
	void Tarjan(int u, int fa){
		dfn[u]=low[u]=++num;
		st[++top]=u;
		for (int i=Begin[u]; i; i=Right[i].Next){
			int v=Right[i].to;
			if (v==fa) continue;
			if (!dfn[v]){
				Tarjan(v, u);
				low[u]=min(low[u], low[v]);
			}
			else if (!co[v]){
				low[u]=min(low[u], dfn[v]);
			}
		}
		if (dfn[u]==low[u]) {
			co[u]=++col;
			si[col]=1;
			while (st[top]!=u) {
				co[st[top]]=col;
				++si[col];
				--top;
			}
			--top;
		}
	}
	void dfs(int u, int fa){
		if (!vis[u]) que[++cnt]=u, vis[u]=1;
		for (int i=1; i<=TOT[u]; i++){
			if (Plan[u][i] && fa!=Plan[u][i]) {
				dfs(Plan[u][i], u);
			}
		}
	}
	void main(){
		Tarjan(1, 0);
		int huan=0;
		for (int i=1; i<=col; i++) 
			if (si[i]>1) {
				huan=i;
				break;
			}
		for (int i=1; i<=n; i++)
			if (co[i]==huan) Vis[i]=1;
		for (int i=1; i<=m; i++){
			if (Vis[x[i]] && Vis[y[i]]) bian[++Num]=i;
		}
		for (int i=1; i<=m; i++){
			Plan[x[i]][++TOT[x[i]]]=y[i];
			Plan[y[i]][++TOT[y[i]]]=x[i];
		}
		for (int i=1; i<=n; i++)
			sort(Plan[i]+1, Plan[i]+1+TOT[i]);
		memset(ans, 0x3f3f3f, sizeof ans);
		for (int i=1; i<=Num; i++){
			cnt=0;
			for (int j=1; j<=TOT[x[bian[i]]]; j++)
				if (Plan[x[bian[i]]][j]==y[bian[i]]) Plan[x[bian[i]]][j]=0;
			for (int j=1; j<=TOT[y[bian[i]]]; j++)
				if (Plan[y[bian[i]]][j]==x[bian[i]]) Plan[y[bian[i]]][j]=0;
			memset(que, 0, sizeof que);
			memset(vis, 0, sizeof vis);
			dfs(1, -1);
			int flag=1;
			for (int j=1; j<=n; j++){
				if (que[j]>ans[j]) {
					flag=0;
					break;
				}
				else if (que[j]<ans[j]) break;
			}	
			if (flag) for (int j=1; j<=n; j++) ans[j]=que[j];
			for (int j=1; j<=TOT[x[bian[i]]]; j++)
				if (Plan[x[bian[i]]][j]==0) Plan[x[bian[i]]][j]=y[bian[i]];
			for (int j=1; j<=TOT[y[bian[i]]]; j++)
				if (Plan[y[bian[i]]][j]==0) Plan[y[bian[i]]][j]=x[bian[i]];
		}
		for (int i=1; i<n; i++) printf("%d ", ans[i]);
		printf("%d\n", ans[n]);
	}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d", &n, &m);
	for (int i=1; i<=m; i++) {
		scanf("%d%d", &x[i], &y[i]);
		Map[x[i]][y[i]]=Map[y[i]][x[i]]=1;
		add_edge(x[i], y[i]);
		add_edge(y[i], x[i]);
	}
	if (m==n-1) P60::main();
		else P40::main();
	return 0;
}

/*

6 6
1 3
2 3
2 5
3 4 
4 5
4 6

*/
