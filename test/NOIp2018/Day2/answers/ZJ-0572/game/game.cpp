#include<bits/stdc++.h>
#define MAXN 5010
#define LL long long
#define mod 1000000007
using namespace std;

int cnt, vis[MAXN], Plan[MAXN][MAXN], n, m, now[MAXN], Map[MAXN][MAXN], ans;

void dfs(int x, int y, int s){
	if (x==n && y==m) {
		++cnt;
		for (int i=1; i<=s; i++) Plan[cnt][i]=vis[i];
		return; 
	}
	if (x>n || y>m) return;
	vis[s+1]=2;
	dfs(x, y+1, s+1);
	vis[s+1]=0;
	vis[s+1]=1;
	dfs(x+1, y, s+1);
	vis[s+1]=0;
}

void solve(){
	for (int i=1; i<=n; i++){
		int x=now[i];
		for (int j=1; j<=m; j++) Map[i][j]=x%2, x>>=1;
	}
	bool flag=0;
	for (int i=1; i<=cnt; i++)
	{
		for (int j=i+1; j<=cnt; j++) {
			int x=1, y=1;
			int Sum1=Map[1][1];
			for (int k=1; k<=n+m-2; k++) {
				Sum1<<=1;
				if (Plan[i][k]==1) y++;
					else x++;
				Sum1+=Map[x][y];
			}
			x=1, y=1;
			int Sum2=Map[1][1];
			for (int k=1; k<=n+m-2; k++){
				Sum2<<=1;
				if (Plan[j][k]==1) y++;
					else x++;
				Sum2+=Map[x][y];
			}
			if (Sum1<=Sum2 ) flag=1;
			else {
				flag=0;
				break;	
			}
		}
		if (!flag) break;
	}
	if (flag) ans++;
}
void DFS(int x){
	if (x>n) {
		solve();
		return;
	}
	for (int i=0; i<(1<<m); i++){
		now[x]=i;
		DFS(x+1);
		now[x]=0;
	}
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d", &n, &m);
	if (n==1){
		printf("0\n");
		return 0;
	}
	else if (n==2){
		if (m<=1) {
			printf("0\n");
			return 0;
		}
		else {
			LL sum=12;
			for (int i=3; i<=m; i++) sum=((LL)sum*4)%mod;
			printf("%lld\n", sum);
			return 0;
		}
	}
	else if (n==3){
		if (m<=1){
			printf("0\n");
			return 0;
		}
		else {
			if (m==2) {
				printf("24\n");
				return 0;
			}
			if (m==3) {
				printf("112\n");
				return 0;
			}
			if (m>=4){
				LL sum=496;
				for (int i=5; i<=m; i++) sum=((LL)sum*8)%mod;
				printf("%lld\n", sum);
				return 0;
			}
		}
	}
	dfs(1, 1, 0);
	DFS(1);
	printf("%d\n", ans);
	return 0;
}
