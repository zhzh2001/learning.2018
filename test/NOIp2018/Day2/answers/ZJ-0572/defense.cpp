#include<bits/stdc++.h>
#define LL long long
using namespace std;

int n, m;
char st[3];
namespace P16{
	const int MAXN=12;
	int val[MAXN], x[MAXN], y[MAXN];
	int a, b, flag1, flag2, ans;
	bool vis[MAXN];
	
	void check(){
		bool flag=1;
		int sum=0;
		for (int i=1; i<=n; i++)
			if (vis[i]) sum+=val[i];
		for (int i=1; i<n; i++) {
			if (!vis[x[i]] && !vis[y[i]]) {
				flag=0;
				break;
			}
		}
		if (!flag) return;
		if (flag1==vis[a] && flag2==vis[b]) {
			ans=min(ans, sum);
			for (int i=1; i<=n; i++) printf("%d ", vis[i]);
			cout<<endl;
		}
	}
	void dfs(int k){
		if (k>n){
			check();
			return;
		}
		vis[k]=1;
		dfs(k+1);
		vis[k]=0;
		dfs(k+1);
	}
	void main(){
		for (int i=1; i<=n; i++) scanf("%d", &val[i]);
		for (int i=1; i<n; i++){
			scanf("%d%d", &x[i], &y[i]);
		}
		for (int i=1; i<=m; i++){
			scanf("%d%d%d%d", &a, &flag1, &b, &flag2);
			ans=1000000000;
			dfs(1);
			if (ans==1000000000) printf("-1\n");
				else printf("%d\n", ans);
		}
	}
}
int val[100010], FLAG[100010];
LL dp[100010][2];

namespace PP16{
	const LL Inf=50000000000;
	void solve(){
		if (FLAG[1]==0) dp[1][0]=0, dp[1][1]=val[1];
		if (FLAG[1]==1) dp[1][0]=0, dp[1][1]=Inf; 
		if (FLAG[1]==2) dp[1][0]=Inf, dp[1][1]=val[1];
		for (int i=2; i<=n; i++){
			if (FLAG[i]==0) {
				dp[i][0]=min(dp[i-1][1], Inf);
				dp[i][1]=min(dp[i-1][0], dp[i-1][1])+val[i];
			}
			if (FLAG[i]==1) {
				dp[i][0]=dp[i-1][1];
				dp[i][1]=Inf;
			}
			if (FLAG[i]==2) {
				dp[i][0]=Inf;
				dp[i][1]=min(dp[i-1][0], dp[i-1][1])+val[i];
			}
		}
		if (dp[n][0]>=Inf && dp[n][1]>=Inf) printf("-1\n");
			else printf("%lld\n", min(dp[n][0], dp[n][1]));
	}
	void main(){
		for (int i=1; i<=n; i++) scanf("%d", &val[i]);
		for (int i=1; i<n; i++){
			int x, y;
			scanf("%d%d", &x, &y);
		}
		for (int i=1; i<=m; i++){
			int a, flag1, b, flag2;
			scanf("%d%d%d%d", &a, &flag1, &b, &flag2);
			memset(dp, 0, sizeof dp);
			FLAG[a]=flag1+1, FLAG[b]=flag2+1;
			solve();
			FLAG[a]=0, FLAG[b]=0;
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s", &n, &m, st+1);
	if (n<=10 && m<=10) P16::main();
	else if (st[1]=='A') PP16::main();
	return 0;
}

/*

11 2 A1
5 2 1 4 2 3 7 4 9 5 1
1 2
2 3
3 4
4 5
5 6
6 7
7 8
8 9
9 10
10 11
1 0 2 1
4 1 8 1
*/
