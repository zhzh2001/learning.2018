#include<bits/stdc++.h>
using namespace std;
const int M=1e9+7;
int n,m,i,j,k,s,x,y,a[5][5],t,ans,K;
string w1,w2,s1,s2;
bool fl;
int pw(int x,int y){
	int z=1;
	for (;y;y>>=1,x=1ll*x*x%M)
		if (y&1) z=1ll*z*x%M;
	return z;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1){
		printf("%d",pw(2,m));
		return 0;
	}
	if (n==2){
		printf("%lld",4ll*pw(3,m-1)%M);
		return 0;
	}
	if (n==3){
		if (m==1){
			puts("8");
			return 0;
		}
		if (m==2){
			puts("36");
			return 0;
		}
		printf("%lld",112ll*pw(3,m-3)%M);
		return 0;
	}
	K=n*m;
	for (s=0;s<1<<K;s++){
		for (i=0;i<n;i++)
			for (j=0;j<m;j++)
				if (s&(1<<i*m+j)) a[i][j]=1;
				else a[i][j]=0;
		fl=1;
		for (i=0;i<1<<n+m-2 && fl;i++){
			w1.clear();
			for (j=0;j<n+m-2;j++)
				if (i&(1<<j)) w1+='D';
				else w1+='R';
			for (j=0;j<n+m-2 && fl;j++)
				if (w1[j]=='R'){
					w2=w1;w2[j]='D';
					for (t=0;t<1<<n+m-3-j && fl;t++){
						for (k=j+1;k<n+m-2;k++)
							if (t&(1<<k-j-1)) w2[k]='D';
							else w2[k]='R';
						s1.clear();s2.clear();
						x=0;y=0;
						for (k=0;k<n+m-2;k++){
							if (w1[k]=='D') x++;
							else y++;
							if (x>=n || y>=m) break;
							s1+=(a[x][y]|48);
						}
						if (x>=n || y>=m) continue;
						x=0;y=0;
						for (k=0;k<n+m-2;k++){
							if (w2[k]=='D') x++;
							else y++;
							if (x>=n || y>=m) break;
							s2+=(a[x][y]|48);
						}
						if (x>=n || y>=m) continue;
						if (s1>s2) fl=0;
					}
				}
		}
		ans+=fl;
	}
	printf("%d",ans);
	return 0;
}
