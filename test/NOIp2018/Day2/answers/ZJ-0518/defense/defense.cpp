#include<bits/stdc++.h>
using namespace std;
const int N=2002;
struct node{
	int to,ne;
}e[N<<1];
int f[N][2],a,x,b,y,i,p[N],n,m,h[N],tot,ans;
char s[4];
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
void wri(int x){if(x<0)putchar('-'),x=-x;if(x>=10)wri(x/10);putchar(x%10|48);}
void wln(int x){wri(x);puts("");}
void add(int x,int y){
	e[++tot]=(node){y,h[x]};
	h[x]=tot;
}
void dfs(int u,int fa){
	f[u][0]=0;f[u][1]=p[u];
	for (int i=h[u],v;i;i=e[i].ne)
		if ((v=e[i].to)!=fa){
			dfs(v,u);
			f[u][0]+=f[v][1];
			f[u][1]+=min(f[v][0],f[v][1]);
		}
	if (a==u) f[u][x^1]=1e9;
	if (b==u) f[u][y^1]=1e9;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=rd(),m=rd();scanf("%s",s);
	for (i=1;i<=n;i++) p[i]=rd();
	for (i=1;i<n;i++) x=rd(),y=rd(),add(x,y),add(y,x);
	for (;m--;){
		a=rd(),x=rd(),b=rd(),y=rd();
		dfs(1,0);
		ans=min(f[1][0],f[1][1]);
		wln(ans<1e9?ans:-1);
	}
	return 0;
}
