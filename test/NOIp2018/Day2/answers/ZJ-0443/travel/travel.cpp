#include <bits/stdc++.h>
using namespace std;

template <typename T>
inline void read(T &f) {
	f = 0; T fu = 1; char c = getchar();
	while(c < '0' || c > '9') { if(c == '-') fu = -1; c = getchar(); }
	while(c >= '0' && c <= '9') { f = (f << 3) + (f << 1) + (c & 15); c = getchar(); }
	f *= fu;
}

const int N = 5005;

struct Edge {
	int u, v, next;
}G[N << 1];

queue <int> q;
vector <int> t[N];
int ans[N], used[N], in[N], iscycle[N], len;
int head[N];
int n, m, tot;

inline void addedge(int u, int v) {
	G[++tot] = (Edge) {u, v, head[u]}, head[u] = tot;
	G[++tot] = (Edge) {v, u, head[v]}, head[v] = tot;
	in[v]++; in[u]++;
}

void dfs(int u, int f) {
	ans[++len] = u; used[u] = 1;
	for(register int i = head[u]; i; i = G[i].next) {
		int v = G[i].v;
		if(v == f || used[v]) continue;
		t[u].push_back(v);
	}
	sort(t[u].begin(), t[u].end());
	int len = t[u].size();
	for(register int i = 0; i < len; i++) dfs(t[u][i], u);
}

bool jp;

void dfs2(int u, int f, int now) {
	used[u] = 1; ans[++len] = u;
	int sum = 0, cnt = 0;
	for(register int i = head[u]; i; i = G[i].next) {
		int v = G[i].v;
		if(v == f || used[v]) continue;
		if(iscycle[v]) {
			cnt++;
			sum += v;
		}
		t[u].push_back(v);
	}
	sort(t[u].begin(), t[u].end());
	int len = t[u].size();
	if(!len) return;
	for(register int i = 0; i < len - 1; i++) {
		int v = t[u][i];
		if(used[v]) continue;
		if(iscycle[v]) {
			dfs2(v, u, t[u][i + 1]);
		} else {
			dfs2(v, u, now);
		}
	}
	int v = t[u][len - 1];
	if(used[v]) return;
	if(iscycle[v] && now != -1 && v > now && !jp) { jp = 1; return; }
	dfs2(v, u, now);
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	read(n); read(m);
	for(register int i = 1; i <= m; i++) {
		int a, b; read(a); read(b);
		addedge(a, b);
	}
	if(m == n - 1) {
		dfs(1, 0);
		for(register int i = 1; i <= n; i++) printf("%d%c", ans[i], i == n ? '\n' : ' ');
		return 0;
	} else {
		for(register int i = 1; i <= n; i++) if(in[i] == 1) q.push(i), iscycle[i] = 0; else iscycle[i] = 1;
		while(!q.empty()) {
			int u = q.front(); q.pop();
			for(register int i = head[u]; i; i = G[i].next) {
				int v = G[i].v;
				if(!iscycle[v]) continue;
				in[v]--; if(in[v] == 1) {
					q.push(v);
					iscycle[v] = 0;
				}
			}
		}
		dfs2(1, 0, -1);
		for(register int i = 1; i <= n; i++) printf("%d%c", ans[i], i == n ? '\n' : ' ');
	}
	return 0;
}
