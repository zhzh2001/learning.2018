#include <bits/stdc++.h>
using namespace std;

template <typename T>
inline void read(T &f) {
	f = 0; T fu = 1; char c = getchar();
	while(c < '0' || c > '9') { if(c == '-') fu = -1; c = getchar(); }
	while(c >= '0' && c <= '9') { f = (f << 3) + (f << 1) + (c & 15); c = getchar(); }
	f *= fu;
}

const int N = 1e6 + 50, P = 1e9 + 7;

inline int mul(int x, int y) { return (int)(1ll * x * y % P); }

int f[N][9][9];
int n, m;

int fpow(int x, int y) {
	int ans = 1;
	while(y) {
		if(y & 1) ans = mul(ans, x);
		y >>= 1; x = mul(x, x);
	}
	return ans;
}

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	read(n); read(m);
	if(n <= 3 && m <= 3) {
		if(n == 1 && m == 1) cout << 2 << endl;
		if(n == 1 && m == 2) cout << 4 << endl;
		if(n == 1 && m == 3) cout << 8 << endl;
		if(n == 2 && m == 1) cout << 4 << endl;
		if(n == 2 && m == 2) cout << 12 << endl;
		if(n == 2 && m == 3) cout << 36 << endl;
		if(n == 3 && m == 1) cout << 8 << endl;
		if(n == 3 && m == 2) cout << 36 << endl;
		if(n == 3 && m == 3) cout << 112 << endl;
		return 0;
	}
	if(n > m) swap(n, m);
	if(n == 1) {
		cout << fpow(2, m) << endl;
		return 0;
	}
	if(n == 2) {
		cout << mul(4, fpow(3, m - 1)) << endl;
		return 0;
	}
	f[1][0][0] = f[1][1][0] = 1;
	for(register int i = 2; i <= m + n - 1; i++) {
		int sum = i + 1;
		int l = max(1, sum - m), r = min(n, sum - 1);
		int ll = max(1, sum - m - 1), rr = min(n, sum - 2);
		for(register int j = l; j < r; j++) {
			for(register int k = ll - 1; k <= rr; k++) {
				f[i][j][k] += f[i - 1][k][j - 1];
				if(f[i][j][k] >= P) f[i][j][k] -= P;
				if(sum - 2 == 2 || sum == n + m - 1) f[i][j][k] += f[i - 1][k][j];
				if(f[i][j][k] >= P) f[i][j][k] -= P;
			}
		}
		for(register int j = 0; j <= n; j++) {
			for(register int k = 0; k <= n; k++) {
				f[i][l - 1][k] += f[i - 1][k][j];
				f[i][r][k] += f[i - 1][k][j];
				if(f[i][l - 1][k] >= P) f[i][l - 1][k] -= P;
				if(f[i][r][k] >= P) f[i][r][k] -= P;
			}
		}
	}
	int ans = 0;
	for(register int i = 0; i <= n; i++) {
		for(register int j = 0; j <= n; j++) {
			ans = (ans + f[n + m - 1][j][i]) % P;
		}
	}
	cout << ans << endl;
	return 0;
}
