#include <bits/stdc++.h>
using namespace std;

template <typename T>
inline void read(T &f) {
	f = 0; T fu = 1; char c = getchar();
	while(c < '0' || c > '9') { if(c == '-') fu = -1; c = getchar(); }
	while(c >= '0' && c <= '9') { f = (f << 3) + (f << 1) + (c & 15); c = getchar(); }
	f *= fu;
}

typedef long long ll;
const int N = 1e5 + 5;

struct Edge {
	int u, v, next;
}G[N << 1];

int head[N], w[N];
char tp;
int n, m, tpp, tot;

inline void addedge(int u, int v) {
	G[++tot] = (Edge) {u, v, head[u]}, head[u] = tot;
	G[++tot] = (Edge) {v, u, head[v]}, head[v] = tot;
}

namespace wxw1 {
	const ll INF = 0x7fffffff;
	
	ll f[2005][2][2];
	bool used[2005][2][2];
	
	void dfs(int u, int fa) {
		f[u][1][1] = w[u]; f[u][0][0] = f[u][0][1] = 0;
		bool ok = 0;
		ll minn = INF;
		for(register int i = head[u]; i; i = G[i].next) {
			int v = G[i].v;
			if(v == fa) continue;
			dfs(v, u);
			f[u][0][0] += f[v][0][1];
			if(f[v][1][1] <= f[v][0][1]) {
				ok = 1;
				f[u][0][1] += f[v][1][1];
			} else {
				f[u][0][1] += f[v][0][1];
				minn = min(minn, f[v][1][1] - f[v][0][1]);
			}
			f[u][1][1] += min(f[v][1][1], min(f[v][0][1], f[v][0][0]));
		}
		if(!ok) f[u][0][1] += minn;
		for(register int j = 0; j <= 1; j++) {
			for(register int k = 0; k <= 1; k++) {
				if(used[u][j][k]) f[u][j][k] = INF;
//				printf("f[%d][%d][%d] = %lld\n", u, j, k, f[u][j][k]);
			}
		}
	}
	
	void main() {
		for(register int i = 1; i <= m; i++) {
			int a, b, c, d;
			read(a); read(b); read(c); read(d);
			if(b == 0) used[a][1][1] = 1;
			else used[a][0][1] = used[a][0][0] = 1;
			if(d == 0) used[c][1][1] = 1;
			else used[c][0][1] = used[c][0][0] = 1;
			dfs(1, 0);
			ll ans = min(f[1][1][1], f[1][0][1]);
			if(ans >= INF) printf("-1\n");
			else printf("%lld\n", ans);
			if(b == 0) used[a][1][1] = 0;
			else used[a][0][1] = used[a][0][0] = 0;
			if(d == 0) used[c][1][1] = 0;
			else used[c][0][1] = used[c][0][0] = 0;
		}
	}
}

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	cin >> n >> m >> tp >> tpp;
	for(register int i = 1; i <= n; i++) read(w[i]);
	for(register int i = 1; i < n; i++) {
		int a, b; read(a); read(b);
		addedge(a, b);
	}
	if(n <= 2000 && m <= 2000) wxw1 :: main();
	else {
		
	}
	return 0;
}
