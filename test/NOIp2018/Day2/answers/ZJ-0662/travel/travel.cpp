#include <bits/stdc++.h>
using namespace std;
#define N 5005
int n,m;
vector<int>E[N];
int In[N];
struct P60{
	int tot,ans[N];
	void dfs(int x,int f){
		ans[++tot]=x;
		sort(E[x].begin(),E[x].end());
		for(int i=0;i<(int)E[x].size();i++){
			int v=E[x][i];
			if(v==f)continue;
			dfs(v,x);
		}
	}
	void solve(){
		dfs(1,0);
		for(int i=1;i<=tot;i++)printf("%d ",ans[i]);
	}
}A60;
struct A40{
	int Q[N],cnt[N],ans[N],key[N],tot;
	bool cir[N],mark[N][N],vis[N][N];
	void topu(){
		int l=0,r=-1;
		for(int i=1;i<=n;i++)
			if(In[i]==1)Q[++r]=i;
		while(l<=r){
			int v=Q[l++];
			cir[v]=1;
			for(int i=0;i<(int)E[v].size();i++){
				int z=E[v][i];
				In[z]--;
				if(In[z]==1)Q[++r]=z;
			}
		}
	}
	void dfs(int x,int f){
		key[++tot]=x;
		for(int i=0;i<(int)E[x].size();i++){
			int v=E[x][i];
			if(v==f||mark[x][v]||mark[v][x])continue;
			cnt[v]=x;
		}
		for(int i=1;i<=n;i++)if(cnt[i]==x)dfs(i,x);
		for(int i=0;i<(int)E[x].size();i++){
			int v=E[x][i];
			if(v==f||mark[x][v]||mark[v][x])continue;
			cnt[v]=0;
		}
	}
	bool Cmp(){
		for(int i=1;i<=n;i++)
			if(ans[i]!=key[i])return ans[i]>key[i];
		return false;
	}
	void solve(){
		topu();
		for(int i=1;i<=n;i++)ans[i]=n;
		for(int i=1;i<=n;i++){
			if(cir[i])continue;
			for(int j=0;j<(int)E[i].size();j++){
				int v=E[i][j];
				if(cir[v])continue;
				if(vis[i][v]||vis[v][i])continue;
				vis[i][v]=vis[v][i]=1;
				mark[i][v]=mark[v][i]=1;
				tot=0;
				dfs(1,0);
				if(Cmp())for(int k=1;k<=n;k++)ans[k]=key[k];
				mark[i][v]=mark[v][i]=0;
			}
		}
		for(int i=1;i<=n;i++)printf("%d ",ans[i]);
	}
}A40;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d %d",&n,&m);
	for(int i=1;i<=m;i++){
		int a,b;
		scanf("%d %d",&a,&b);
		E[a].push_back(b);
		E[b].push_back(a);
		In[a]++,In[b]++;
	}
	if(m==n-1)A60.solve();
	else A40.solve();
	return 0;
}
