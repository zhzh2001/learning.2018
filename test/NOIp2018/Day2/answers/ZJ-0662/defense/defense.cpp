#include <bits/stdc++.h>
using namespace std;
#define M 100005
#define ll long long
const ll inf=1e17;
int n,m,val[M];
char str[10];
vector<int>E[M];
struct P16{
	struct node {
		int a,x,b,y;
		ll ans;
	}Q[15];
	void deal(int x){
		ll ret=0;
		bool mark[15];
		memset(mark,0,sizeof(mark));
		for(int i=0;i<n;i++)
			if(x&(1<<i)){
				ret+=val[i+1];
				mark[i+1]=1;
			}
		for(int i=1;i<=n;i++)
			for(int j=0;j<(int)E[i].size();j++)
				if(mark[i]==0&&mark[E[i][j]]==0)return;
		for(int i=1;i<=m;i++)
			if(mark[Q[i].a]==Q[i].x&&mark[Q[i].b]==Q[i].y)
				Q[i].ans=min(Q[i].ans,ret);
	}
	void solve(){
		for(int i=1;i<=m;i++){
			scanf("%d %d %d %d",&Q[i].a,&Q[i].x,&Q[i].b,&Q[i].y);
			Q[i].ans=inf;
		}	
		for(int i=1;i<(1<<n);i++)deal(i);
		for(int i=1;i<=m;i++){
			if(Q[i].ans>=inf)puts("-1");
			else printf("%lld\n",Q[i].ans);
		}
	}
}A16;
struct P55{
	ll dp[1005][2];
	bool mark[1005],vis[1005];
	void dfs(int x,int f){
		dp[x][0]=0;
		dp[x][1]=val[x];
		for(int i=0;i<(int)E[x].size();i++){
			int v=E[x][i];
			if(v==f)continue;
			dfs(v,x);
			dp[x][0]+=dp[v][1];
			dp[x][1]+=min(dp[v][1],dp[v][0]);	
		}
		if(mark[x]){
			if(vis[x])dp[x][0]=dp[x][1];
			else dp[x][1]=inf;
		}
	}
	void solve(){
		for(int i=1;i<=m;i++){
			int a,b,x,y;
			scanf("%d %d %d %d",&a,&x,&b,&y);
			mark[a]=mark[b]=1;	
			vis[a]=x,vis[b]=y;
			dfs(1,0);
			mark[a]=mark[b]=0;
			ll ret=min(dp[1][0],dp[1][1]);
			if(ret>=inf)puts("-1");
			else printf("%lld\n",ret);
		}
	}
}A55;
struct P8{
	ll dp[100005][2],tmp[100005][2];
	int fa[100005];
	void dfs(int x,int f){
		fa[x]=f;
		dp[x][0]=0;
		dp[x][1]=val[x];
		for(int i=0;i<(int)E[x].size();i++){
			int v=E[x][i];
			if(v==f)continue;
			dfs(v,x);
			dp[x][0]+=dp[v][1];
			dp[x][1]+=min(dp[v][1],dp[v][0]);	
		}
	}
	void solve(){
		dfs(1,0);
		for(int i=1;i<=m;i++){
			int a,b,x,y;
			scanf("%d %d %d %d",&a,&x,&b,&y);
			tmp[a][0]=tmp[a][1]=dp[a][1];
			while(fa[a]){
				tmp[fa[a]][0]=dp[fa[a]][1]-dp[a][0]+tmp[a][0];
				tmp[fa[a]][1]=dp[fa[a]][1]-min(dp[a][1],dp[a][0])+
				min(tmp[a][1],tmp[a][0]);
				a=fa[a];
			}
			ll ret=min(tmp[1][0],tmp[1][1]);
			if(ret>=inf)puts("-1");
			else printf("%lld\n",ret);
		}
	}
}A8;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d %d %s",&n,&m,str);
	for(int i=1;i<=n;i++)scanf("%d",&val[i]);
	for(int i=1;i<n;i++){
		int a,b;
		scanf("%d %d",&a,&b);
		E[a].push_back(b);
		E[b].push_back(a);
	}
	if(n<=10&&m<=10)A16.solve();
	else if(n<=5000&&m<=5000)A55.solve();
	else A8.solve();
	return 0;
}
