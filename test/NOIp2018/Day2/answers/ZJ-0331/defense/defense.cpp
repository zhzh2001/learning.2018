#include<cstdio>
#include<cstring>

const long long inf=1000000000000000000ll;
long long f[100001][2];
int n,p[100001],a,x,b,y,fa[100001],hd[100001],cnt,m,X;
char s[10];

struct node
{
	int to,next;
}e[200001];

long long getmin(long long a,long long b) {return a<b?a:b;}

void addedge(int x,int y)
{
	e[++cnt].next=hd[x];
	hd[x]=cnt;
	e[cnt].to=y;
}

void dfs(int x)
{
	f[x][0]=0; f[x][1]=p[x];
	for (int i=hd[x]; i; i=e[i].next)
		if (e[i].to!=fa[x]) 
		{
			fa[e[i].to]=x,dfs(e[i].to);
			f[x][0]+=f[e[i].to][1];
			f[x][1]+=getmin(f[e[i].to][0],f[e[i].to][1]);
		}
	if (a==x) f[x][X^1]=inf;
	if (b==x) f[x][y^1]=inf;
	f[x][0]=getmin(f[x][0],inf);
	f[x][1]=getmin(f[x][1],inf);
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m); scanf("%s",s); cnt=0;
	memset(hd,0,sizeof(hd));
	for (int i=1; i<=n; i++) scanf("%d",&p[i]);
	for (int i=1; i<n; i++) scanf("%d%d",&x,&y),addedge(x,y),addedge(y,x);
	while (m--)
	{
		scanf("%d%d%d%d",&a,&X,&b,&y);
		dfs(1); long long ans=getmin(f[1][0],f[1][1]);
		printf("%lld\n",(ans==inf)?(-1):(ans));
	}
	return 0;
}
