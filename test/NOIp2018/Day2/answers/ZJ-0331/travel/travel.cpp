#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;

bool bo[5001],Bo;
int n,m,hd[5001],cnt,x,y,ans[5001],c[5001],cn,la[5001],I,J,nw[5001],E[5001][5001];
vector<int> v[5001];
vector<int>::iterator it;

struct node
{
	int to,next;
}e[10001];

void addedge(int x,int y)
{
	e[++cnt].next=hd[x];
	hd[x]=cnt;
	e[cnt].to=y;
	E[x][y]=cnt;
}

void dfs(int x)
{
	if (bo[x])
	{
		c[++cn]=x; y=x;
		while (la[x]!=y) x=la[x],c[++cn]=x; 
		return;
	}
	bo[x]=1;
	for (int i=hd[x]; i; i=e[i].next)
		if (e[i].to!=la[x]) 
		{
			la[e[i].to]=x; dfs(e[i].to);
			if (cn) return;
		}
}

void Dfs(int x)
{
	nw[++cnt]=x;
	for (int i=hd[x]; i; i=e[i].next)
		if (e[i].to!=la[x]&&i!=I&&i!=J) la[e[i].to]=x,Dfs(e[i].to); 
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m); cnt=0;
	memset(hd,0,sizeof(hd));
	for (int i=1; i<=n; i++) v[i].clear();
	for (int i=1; i<=m; i++) scanf("%d%d",&x,&y),v[x].push_back(y),v[y].push_back(x);
	for (int i=1; i<=n; i++)
	{
		sort(v[i].begin(),v[i].end());
		for (it=v[i].end(); it!=v[i].begin(); ) it--,addedge(i,(*it));
	}
	memset(bo,0,sizeof(bo));
	memset(la,0,sizeof(la));
	cn=0; dfs(1);
	for (int i=1; i<=n; i++) ans[i]=n;
	if (!cn)
	{
		la[1]=cnt=I=J=0; Dfs(1);
		for (int i=1; i<=n; i++) ans[i]=nw[i];
	}
	for (int i=1; i<=cn; i++)
	{
		I=E[c[i]][c[i%cn+1]];
		J=E[c[i%cn+1]][c[i]];
		cnt=0; la[1]=0; Dfs(1); Bo=0;
		for (int i=1; i<=n; i++)
			if (nw[i]<ans[i]) {Bo=1; break;} else
			if (nw[i]>ans[i]) {Bo=0; break;}
		if (Bo)
		{
			for (int i=1; i<=n; i++) ans[i]=nw[i];
		}
	}
	for (int i=1; i<=n; i++) printf("%d ",ans[i]); printf("\n");
	return 0;
}
