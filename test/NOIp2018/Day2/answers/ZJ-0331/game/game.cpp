#include<cstdio>
#include<cstring>

int n,m,ans,sum,s,a[10][10];
const int mod=1000000007;

int quickmi(int a,int b)
{
	int t=1;
	while (b)
	{
		if (b&1) t=1ll*t*a%mod;
		a=1ll*a*a%mod;
		b>>=1;
	}
	return t;
}

bool check()
{
	for (int i=1; i<n; i++)
		for (int j=1; j<m; j++)
		{
			if (a[i+1][j]<a[i][j+1]) return 0;
			if (a[i+1][j]==a[i][j+1])
			{
				for (int k=i+2; k<=n; k++)
					for (int l=j+1; l<m; l++)
						if (a[k][l]!=a[k-1][l+1]) return 0;
			}
		}
	return 1;
}

void dfs(int x,int y)
{
	if (x>n) {if (check()) ans++; return;}
	if (y>m) {dfs(x+1,1); return;}
	a[x][y]=0; dfs(x,y+1);
	a[x][y]=1; dfs(x,y+1);
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) ans=n,n=m,m=ans;
	if (n<=3)
	{
		if (n==1) {printf("%d\n",quickmi(2,m)); return 0;}
		if (n==2) {printf("%lld\n",4ll*quickmi(3,m-1)%mod); return 0;}
		ans=2; sum=s=0;
		sum=(sum+2ll*quickmi(4,n-2)*quickmi(3,m-n)%mod*quickmi(2,n-1))%mod;
		if (n!=m) sum=(sum+8ll*quickmi(3,m-n-1)%mod*quickmi(2,n-1))%mod;
		else sum=(sum+12)%mod;
		if (n!=m) sum=(sum+4ll*quickmi(3,m-n-1)*quickmi(2,n-1))%mod;
		else sum=(sum+6)%mod;
		if (n==m) sum=(sum+6)%mod; else
		{
			for (int i=4; i<m; i++)
				sum=(sum+4ll*quickmi(3,m-i)*quickmi(2,n-1))%mod;
			sum=(sum+24)%mod;
		}
		printf("%lld\n",2ll*sum%mod);
	} else
	{
		ans=0; dfs(1,1);
		printf("%d\n",ans);
	}
	return 0; 
}
