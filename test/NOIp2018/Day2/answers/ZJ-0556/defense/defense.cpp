#include<bits/stdc++.h>
using namespace std;
#define N 100003
#define LL long long

int n,m,nedge,a,b,x,y;
int head[N],p[N];
char s[10];
struct Edge{
	int to,next;
}edge[2*N];
LL f[N][2];

void addedge(int a,int b){
	edge[nedge].to=b;
	edge[nedge].next=head[a];
	head[a]=nedge++;	
}

void dfs(int u,int fa){
	if (u==a){
		if (x==0) f[u][0]=0;
		  else f[u][1]=p[u];
	}
	else if (u==b){
		if (y==0) f[u][0]=0;
		  else f[u][1]=p[u];	
	}
	else{
		f[u][0]=0;
		f[u][1]=p[u];
	}
	for (int i=head[u];i!=-1;i=edge[i].next){
		int v=edge[i].to;
		if (v==fa) continue;
		dfs(v,u);
		if (f[v][1]==-1) f[u][0]=-1;
		if (f[u][0]!=-1 && f[v][1]!=-1) f[u][0]=(f[u][0]+f[v][1]);
		if (f[u][1]!=-1){
			if (f[v][1]==-1) f[u][1]+=f[v][0];
			  else if (f[v][0]==-1) f[u][1]+=f[v][1];
			    else f[u][1]+=min(f[v][0],f[v][1]);	
		}
	}
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s+1);
	for (int i=1;i<=n;i++)
	  scanf("%d",&p[i]);
	memset(head,-1,sizeof(head));
	for (int i=1;i<n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		addedge(u,v);
		addedge(v,u);
	}
	for (int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		memset(f,-1,sizeof(f));
		bool flag=1;	
		if (x==0 && y==0){
			for (int i=head[a];i!=-1;i=edge[i].next){
				int v=edge[i].to;
				if (v==b){
					flag=0;
					break;	
				}
			}
		}
		if (flag==0){
			printf("-1\n");
			continue;	
		}
		dfs(1,0);
		if (1==a){
			if (x==0) printf("%lld\n",f[1][0]);
			  else printf("%lld\n",f[1][1]);	
		}
		else if (1==b){
			if (y==0) printf("%lld\n",f[1][0]);
			  else printf("%lld\n",f[1][1]);	
		}
		else{
			if (f[1][0]==-1) printf("%lld\n",f[1][1]);
			  else if (f[1][1]==-1) printf("%lld\n",f[1][0]);
			    else printf("%lld\n",min(f[1][0],f[1][1]));
		}
	}
	return 0;	
}
