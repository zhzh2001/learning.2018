#include<bits/stdc++.h>
using namespace std;
#define N 5003

int n,m,nedge,tot,scc,tin,tim,nedge1,tot1;
int head[N],Ans[N],g[N],f[N];
struct Edge{
	int to,next;
}edge[2*N];
bool vis[N];

void addedge(int a,int b){
	edge[nedge].to=b;
	edge[nedge].next=head[a];
	head[a]=nedge++;	
}

void solve1(int u,int fa){
	Ans[++tot]=u;
	vector<int>vec;
	for (int i=head[u];i!=-1;i=edge[i].next){
		int v=edge[i].to;
		if (v==fa) continue;
		vec.push_back(v);
	}
	sort(vec.begin(),vec.end());
	for (int i=0;i<vec.size();i++)
	  solve1(vec[i],u);	
}	

bool check(){
	for (int i=1;i<=n;i++){
	  	if (g[i]<Ans[i]) return 1;
		if (g[i]>Ans[i]) return 0;
	}
	return 0;
}

void dfs(int u,int fa,int step){
	if (!vis[u]){
		vis[u]=1;
		step++;
		g[step]=u;
	}
	if (step==n){
		if (check()){
			for (int i=1;i<=n;i++)
			  Ans[i]=g[i];
		}
		return;	
	}
	for (int i=head[u];i!=-1;i=edge[i].next){
		int v=edge[i].to;
		if (v==fa || vis[v]) continue;
		dfs(v,u,step);
	}
}

void solve2(){
	memset(vis,0,sizeof(vis));
	dfs(1,0,0);	
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(head,-1,sizeof(head));
	for (int i=1;i<=m;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		addedge(u,v);
		addedge(v,u);	
	}
	tot=0;
	if (m==n-1){
		solve1(1,0);
		for (int i=1;i<=n;i++)
	  	  printf("%d ",Ans[i]);
	}
	else{
		for (int i=1;i<=n;i++)
		  printf("%d ",i);
	}
	return 0;
}
