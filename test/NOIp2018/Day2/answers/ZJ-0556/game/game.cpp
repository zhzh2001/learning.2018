#include<bits/stdc++.h>
using namespace std;
#define mod 1000000007

int n,m,ans;
int f[9][1000003][2];
int g[10][10]={
	{2,4,8},{4,12,36},{8,36,112}
};

int calc(int a,int b){
	int res=1,base=a;
	while (b){
		if (b&1) res=1ll*res*base%mod;
		base=1ll*base*base%mod;
		b>>=1;
	}
	return res;
}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n<=3 && m<=3){
		printf("%d\n",g[n-1][m-1]);	
		return 0;
	}
	if (n==2){
		ans=1ll*4*calc(3,m-1)%mod;
		printf("%d\n",ans);
		return 0;	
	}
	memset(f,0,sizeof(f));
	for (int i=1;i<=m;i++)
	  f[1][i][0]=f[1][i][1]=1;
	for (int i=2;i<=n;i++){
		for (int j=1;j<m;j++){
			f[i][j][0]=(f[i][j][0]+f[i-1][j+1][0])%mod;
			f[i][j][1]=(f[i][j][1]+f[i-1][j+1][0])%mod;
			f[i][j][1]=(f[i][j][1]+f[i-1][j+1][1])%mod;
		}
		f[i][m][0]=f[i][m][1]=1;
	}
	ans=1;
	for (int i=n;i<=n;i++){
		int sum=1;
		for (int j=1;j<=m;j++){
		  	sum=sum*((f[i][j][0]+f[i][j][1])%mod)%mod;
		  }
	  	ans=1ll*ans*sum%mod;
	}
	printf("%d\n",ans);
	return 0;	
}
