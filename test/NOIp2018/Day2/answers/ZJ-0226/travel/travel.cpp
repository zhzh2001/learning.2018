#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<queue>
using namespace std;
const int N=5e3+5;
int re()
{
	int all=0,key=1;char a=getchar();
	while(a<'0'||a>'9'){if(a=='-')key=-1;a=getchar();}
	while(a<='9'&&a>='0'){all=(all<<3)+(all<<1)+a-'0';a=getchar();}
	return all*key;
}
struct edge{
	int next,to,value;
}e[N<<1];
int n,m,first[N],tot,dfn[N],low[N],num,c[N],cnt,cir,gein=-1;
int anw[N],step,siz[N];
int minn=1e9,maxn=-1;
int death1=-1,death2=-1;
bool bridge[N<<1];
vector<int> Q[N];
void add(int x,int y)
{
	e[++tot].to=y;
	e[tot].next=first[x];
	first[x]=tot;
}
void print()
{
	int i;
	for(i=1;i<=n;i++)
		printf("%d ",anw[i]);
}
void dfs(int last,int fath)
{
	anw[++step]=last;
	if(step==n){
		print();
		exit(0);
	}
	int i,to,sum=0;
	priority_queue< int , vector < int > , greater < int > > P;
	while(!P.empty()) P.pop();
	for(i=first[last];i;i=e[i].next)
	{
		to=e[i].to;
		if(to==fath) continue;
		P.push(to);
	}
	while(!P.empty())
	{
		to=P.top();
		dfs(to,last);
		P.pop();
	}
}
int find(int x){
	if(x&1) return x+1;
	return x-1;
}
void tarjan(int x,int in_edge)
{
	dfn[x]=low[x]=++num;
	int to,i;
	for(i=first[x];i;i=e[i].next)
	{
		to=e[i].to;
		if(!dfn[to])
		{
			tarjan(to,i);
			low[x]=min(low[x],low[to]);
			if(low[to]>dfn[x]) bridge[i]=bridge[find(i)]=true;
		}
		else if(in_edge!=find(i)) low[x]=min(low[x],dfn[to]);
	}
}
void dfs1(int x)
{
	int to,i;
	c[x]=cnt;
	siz[cnt]++;
	if(siz[cnt]>1) cir=cnt;
	Q[cnt].push_back(x);
	for(i=first[x];i;i=e[i].next){
		to=e[i].to;
		if(c[to]||bridge[i]) continue;
		dfs1(to);
	}
}
void dfs2(int x,int fath)
{
	int i,to;
	if(c[x]==cir){
		gein=x;
		return;
	}
	for(i=first[x];i;i=e[i].next)
	{
		to=e[i].to;
		if(to==fath) continue;
		dfs2(to,x);
		if(gein!=-1) return; 
	}
}
void find_cut(int x,int fath)
{
	int i,to;
	if(x==maxn)
	{
		for(i=first[x];i;i=e[i].next)
		{
			to=e[i].to;
			if(to==fath||c[to]!=cir) continue;
			death1=i;
			death2=find(i);
			break;
		}
		return;
	}
	for(i=first[x];i;i=e[i].next)
	{
		to=e[i].to;
		if(to==fath||c[to]!=cir) continue;
		if(to>maxn){
			death1=i;
			death2=find(i);
			return;
		}
		find_cut(to,x);
		if(death1!=-1) return;
	}
}
void DFS(int x,int fath){
	anw[++step]=x;
	if(step==n)
	{
		print();
		exit(0);
	}
	int to,i;
	priority_queue< int , vector < int > , greater < int > > P;
	while(!P.empty()) P.pop();
	for(i=first[x];i;i=e[i].next)
	{
		if(i==death1||i==death2) continue;
		to=e[i].to;
		if(to==fath) continue;
		P.push(to);
	}
	while(!P.empty())
	{
		to=P.top();
		DFS(to,x);
		P.pop();
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int i,x,y,to;
	n=re();m=re();
	for(i=1;i<=m;i++){
		x=re();y=re();
		add(x,y);
		add(y,x);
	}
	if(m==n-1)
	{
		dfs(1,0);
		return 0;
	}
	else if(m==n)
	{
		for(i=1;i<=n;i++)
			if(!dfn[i]) tarjan(i,0);
		for(i=1;i<=n;i++)
			if(!c[i]) cnt++,dfs1(i);
		if(c[1]==cir)
			gein=1;
		else
			dfs2(1,0);
		for(i=first[gein];i;i=e[i].next){
			to=e[i].to;
			if(c[to]!=cir) continue;
			maxn=max(maxn,to);
			minn=min(minn,to);
		}
		//cout<<-1<<endl;
		//cout<<gein<<endl;
		find_cut(minn,gein);
		DFS(1,0);
	}
}
