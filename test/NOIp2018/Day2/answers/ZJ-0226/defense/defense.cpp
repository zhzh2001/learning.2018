#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=1e5+5;
int f[N][2],g[N][2];
int w[N],minn;
int re()
{
	int all=0,key=1;char a=getchar();
	while(a<'0'||a>'9'){if(a=='-')key=-1;a=getchar();}
	while(a<='9'&&a>='0'){all=(all<<3)+(all<<1)+a-'0';a=getchar();}
	return all*key;
}
int n,a,x,b,y;
bool cho[N];
void dfs(int now,int tot)
{
	if(now==n+1) minn=min(minn,tot);
	if(tot>=minn) return;
	if(now==a)
	{
		if(x==1)
		{
			cho[now]=true;
			dfs(now+1,tot+w[now]);
			cho[now]=false;
		}
		if(x==0)
		{
			if(cho[now-1]==false) return;
			dfs(now+1,tot);
		}
	}
	if(now==b)
	{
		if(x==1)
		{
			cho[now]=true;
			dfs(now+1,tot+w[now]);
			cho[now]=false;
		}
		if(x==0)
		{
			if(cho[now-1]==false) return;
			dfs(now+1,tot+w[now]);
		}
	}
	if(now!=a&&now!=b)
	{
		if(cho[now-1]==false)
		{
			cho[now]=true;
			dfs(now+1,tot+w[now]);
			cho[now]=false;
		}
		if(cho[now-1]==true)
		{
			cho[now]=true;
			dfs(now+1,tot+w[now]);
			cho[now]=false;
			dfs(now+1,tot);
		}
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int m,i;
	n=re();m=re();string typ;
	cin>>typ;
	for(i=1;i<=n;i++)
		w[i]=re();
	for(i=1;i<=n-1;i++){
		x=re();y=re();
	}
	for(i=1;i<=m;i++)
	{
		memset(cho,false,sizeof(cho));
		minn=1e9;
		a=re();x=re();b=re();y=re();
		if((a==b+1||b==a+1)&&x==0&&y==0){
			printf("-1\n");
			continue;
		}
		dfs(1,0);
		if(minn==1e9) printf("-1\n");
		else printf("%d\n",minn);
	}
	return 0;
}
