#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
const int mod=1e9+7,N=1e6+500;
int anw=0;
int tre[N],two[N];
int n,m;
int re()
{
	int all=0,key=1;char a=getchar();
	while(a<'0'||a>'9'){if(a=='-')key=-1;a=getchar();}
	while(a<='9'&&a>='0'){all=(all<<3)+(all<<1)+a-'0';a=getchar();}
	return all*key;
}
int init()
{
	int i;
	tre[0]=1;two[0]=1;
	tre[1]=3;two[1]=2;
	for(i=2;i<=n+m;i++)
	{
		tre[i]=tre[i-1]*3;
		tre[i]%=mod;
		two[i]=tre[i-1]*2;
		two[i]%=mod;
	}
}
int pre[N];
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int i;	
	n=re();m=re();
	if(n<=3&&m<=3)
	{
		if(n==1&&m==1) printf("0\n");
		if(n==1&&m==2) printf("0\n");
		if(n==1&&m==3) printf("0\n");
		if(n==2&&m==1) printf("0\n");
		if(n==2&&m==2) printf("12\n");
		if(n==2&&m==3) printf("24\n");
		if(n==3&&m==1) printf("0\n");
		if(n==3&&m==2) printf("84\n");
		if(n==3&&m==3) printf("112\n");
		return 0;
	}
	if(n==2)
	{
		init();
		for(i=2;i<=m;i++)
		{
			pre[i]=(ll)pre[i]+two[i]*tre[m-i]%mod;
			pre[i]%=mod;
		}
		int key=m;
		for(i=m;i>=2;i--)
		{
			anw=(ll)anw+((pre[key]-pre[1])%mod+mod)%mod;
			anw%=mod;
			key--;
		}
		printf("%d\n",anw);
		return 0;
	}
	return 0;
}
