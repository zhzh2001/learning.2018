#include<bits/stdc++.h>
  using namespace std;

#define Abigail inline void
typedef long long LL;

const int N=5000;

int n,m;
struct side{
  int y,next,c;
}e[N*2+9];
int lin[N+9],top=1;

void ins(int x,int y){
  e[++top].y=y;
  e[top].next=lin[x];
  lin[x]=top;
}

priority_queue<int,vector<int>,greater<int> >d[N+9];
int ord[N+9],num,tmp[N+9],use[N+9];

void dfs(int k){
  tmp[++num]=k;use[k]=1;
  for (int i=lin[k];i;i=e[i].next)
    if (!use[e[i].y]&&!e[i].c) d[k].push(e[i].y);
  for (;!d[k].empty();d[k].pop())
    dfs(d[k].top());
}

Abigail into(){
  scanf("%d%d",&n,&m);
  int x,y;
  for (int i=1;i<=m;++i){
  	scanf("%d%d",&x,&y);
  	ins(x,y);ins(y,x);
  }
}

Abigail work(){
  if (n-1==m){
    dfs(1);
    for (int i=1;i<=n;++i)
      ord[i]=tmp[i];
    return;
  }
  bool flag;
  for (int i=1;i<=n;++i)
    ord[i]=n+1;
  for (int i=2;i<=top;i+=2){
  	e[i].c=e[i^1].c=1;
  	num=0;flag=0;
  	for (int i=1;i<=n;++i)
  	  use[i]=0;
  	dfs(1);
  	e[i].c=e[i^1].c=0;
  	if (num<n) continue;
  	for (int j=1;j<=n;++j)
  	  if (tmp[j]^ord[j]){
	    flag=tmp[j]<ord[j];
		break;
	  }
	if (!flag) continue;
	for (int j=1;j<=n;++j)
	  ord[j]=tmp[j];
  }
}

Abigail outo(){
  for (int i=1;i<n;++i)
    printf("%d ",ord[i]);
  printf("%d\n",ord[n]);
}

int main(){
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  into();
  work();
  outo();
  return 0;
}
