#include<bits/stdc++.h>
  using namespace std;

#define Abigail inline void
typedef long long LL;

const int N=100000;
const LL INF=(1LL<<50)-1LL;

int read(){
  int x=0;
  char c=getchar();
  for (;c<'0'||c>'9';c=getchar());
  for (;c<='9'&&c>='0';c=getchar()) x=x*10+c-'0';
  return x;
}

struct side{
  int y,next;
}e[N*2+9];
int lin[N+9],top;

void ins(int x,int y){
  e[++top].y=y;
  e[top].next=lin[x];
  lin[x]=top;
}

int n,m,fa[N+9];

void dfs_start(int k,int dad){
  fa[k]=dad;
  for (int i=lin[k];i;i=e[i].next)
    if (e[i].y^dad) dfs_start(e[i].y,k);
}

LL v[N+9],f[N+9][2];
int x,a,y,b;

void dfs(int k){
  f[k][1]=v[k];
  for (int i=lin[k];i;i=e[i].next)
    if (e[i].y^fa[k]){
      dfs(e[i].y);
      f[k][0]+=f[e[i].y][1];
      f[k][1]+=min(f[e[i].y][0],f[e[i].y][1]);
	}
  if (k==a) f[k][x^1]=INF;
  if (k==b) f[k][y^1]=INF;
}

Abigail into(){
  n=read();m=read();
  v[1]=read();
  for (int i=1;i<=n;++i)
    v[i]=LL(read());
  int x,y;
  for (int i=1;i<n;++i){
  	x=read();y=read();
  	ins(x,y);ins(y,x);
  }
}

Abigail getans(){
  dfs_start(1,0);
  for (int i=1;i<=m;++i){
    a=read();x=read();b=read();y=read();
    for (int j=1;j<=n;++j)
      f[j][0]=f[j][1]=0;
	dfs(1);
    if ((fa[a]==b||fa[b]==a)&&x==0&&y==0) printf("-1\n");
    else printf("%lld\n",min(f[1][0],f[1][1]));
  }
}

int main(){
  freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout);
  into();
  getans();
  return 0;
}
