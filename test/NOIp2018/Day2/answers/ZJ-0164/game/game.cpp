#include<bits/stdc++.h>
  using namespace std;

#define Abigail inline void
typedef long long LL;

const int S=1<<8,mod=1000000007;

int f[2][S],now=1,old=0;
int n,m,s,ans;

bool check(int s1,int s2){
  for (int i=0;i<n-1;++i)
    for (int j=i+1;j<n;++j)
      if ((s2>>i&1)>(s1>>j&1)) return 0;
  return 1;
}

Abigail into(){
  scanf("%d%d",&n,&m);
  s=1<<n;
}

Abigail work(){
  for (int i=0;i<s;++i)
    f[old][i]=1;
  for (int i=2;i<=m;++i){
    for (int j=0;j<s;++j)
      for (int k=0;k<s;++k)
        if (check(j,k)){
          f[now][k]+=f[old][j];
          if (f[now][k]>=mod) f[now][k]-=mod;
		}
	now^=1;old^=1;
	for (int j=0;j<s;++j)
	  f[now][j]=0;
  }
  for (int i=0;i<s;++i){
    ans+=f[old][i];
    if (ans>=mod) ans-=mod;
  }
}

Abigail outo(){
  printf("%d\n",ans);
}

int main(){
  freopen("game.in","r",stdin);
  freopen("game.out","w",stdout);
  into();
  work();
  outo();
  return 0;
}
