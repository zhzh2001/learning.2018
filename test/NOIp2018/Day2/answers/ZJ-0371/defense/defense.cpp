#include <bits/stdc++.h>
using namespace std;
const long long INF = 10000000050;
char s[10]; int p[100001];
long long dp[100001][2];
bool mark[100001];
int head[100001], vet[200000], nxt[200000], Enum = 0;
void ins(int u, int v){
	vet[Enum] = v; nxt[Enum] = head[u];
	head[u] = Enum; Enum ++;
}
void DP(int k){
	if(dp[k][1] != INF) dp[k][1] = p[k];
	for(int e = head[k]; e != -1; e = nxt[e]){
		int v = vet[e];
		if(mark[v]) continue;
		mark[v] = true; DP(v);
		if(dp[k][0] != INF) dp[k][0] += dp[v][1];
		if(dp[k][1] != INF)
			dp[k][1] += min(dp[v][0], dp[v][1]);
	}
	if(dp[k][0] > INF) dp[k][0] = INF;
	if(dp[k][1] > INF) dp[k][1] = INF;
}
int main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	int a, x, b, y, n, m, u, v;
	scanf(" %d %d %s ", &n, &m, s);
	memset(head, 255, sizeof head);
	for(int i = 1; i <= n; i ++)
		scanf("%d", &p[i]);
	for(int i = 1; i < n; i ++){
		scanf("%d%d", &u, &v);
		ins(u, v); ins(v, u);
	}
	while(m --){
		scanf("%d%d%d%d", &a, &x, &b, &y);
		memset(dp, 0, sizeof dp);
		dp[a][1 - x] = dp[b][1 - y] = INF;
		memset(mark, false, sizeof mark);
		mark[1] = true; DP(1);
		long long ans = min(dp[1][0], dp[1][1]);
		if(ans == INF) printf("-1\n");
		else printf("%lld\n", ans);
	}
	return 0;
}
