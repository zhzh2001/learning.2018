#include <bits/stdc++.h>
using namespace std;
int c[5000], mn[5000], n, sz;
bool mark[5001], mat[5001][5001];
void dfs(int k){
	c[sz ++] = k;
	for(int i = 1; i <= n; i ++)
		if(!mark[i] && mat[i][k]){
			mark[i] = true; dfs(i);
		}
}
void print(int*a){
	for(int i = 0; i < n; i ++){
		if(i) printf(" ");
		printf("%d", a[i]);
	}
	printf("\n");
}
void change(int*a, int*ans){
	if(a[n - 1] == 0) return;
	for(int i = 0; i < n; i ++){
		if(a[i] < ans[i]){
			for(int j = i; j < n; j ++)
				ans[j] = a[j];
			return;
		}
		else if(a[i] > ans[i]) return;
	}
}
int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	int m, u, v;
	scanf("%d%d", &n, &m);
	memset(mat, false, sizeof mat);
	for(int i = 0; i < m; i ++){
		scanf("%d%d", &u, &v);
		mat[u][v] = mat[v][u] = true;
	}
	if(m < n){
		memset(mark, false, sizeof mark);
		mark[1] = true; sz = 0;
		dfs(1); print(c);
	}
	else{
		for(int i = 0; i < n; i ++) mn[i] = n + 1;
		for(int i = 1; i <= n; i ++)
			for(int j = 1; j < i; j ++)
				if(mat[i][j]){
					mat[i][j] = mat[j][i] = false;
					memset(mark, false, sizeof mark);
					memset(c, 0, sizeof c); sz = 0;
					mark[1] = true; dfs(1);
					change(c, mn);
					mat[i][j] = mat[j][i] = true;
				}
		print(mn);
	}
	return 0;
}
