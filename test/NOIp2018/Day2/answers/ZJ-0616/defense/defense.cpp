#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define N 100010 
#define INF 1000000000000

struct bian
{
	int x,y;
} a[N*2];

int n,m,p,fir[N],nex[N],fa[N],aa,bb,xx,yy;
LL f[N][2],val[N];
string s;

void dfs(int i)
{
	for (int j=fir[i];j;j=nex[j])
	{
		if (a[j].y!=fa[i])
		{
			fa[a[j].y]=i;
			dfs(a[j].y);
		}
	}
}

void sxdp(int i)
{
	if ((!nex[fir[i]]) && i!=1)
	{
		f[i][0]=0;
		f[i][1]=val[i];
		return;
	}
	for (int j=fir[i];j;j=nex[j])
	{
		if (a[j].y==fa[i]) continue;
		sxdp(a[j].y);
	}
	int cs=-1;
	if (i==aa || i==bb)
	{
		cs=(i==aa)?xx:yy;
	}
	LL cigema=0;
	if (cs!=0)
	{
		cigema=val[i];
		for (int j=fir[i];j;j=nex[j])
		{
			if (a[j].y==fa[i]) continue;
			cigema+=min(f[a[j].y][0],f[a[j].y][1]);
		}
		f[i][1]=min(f[i][1],cigema);
	}
	if (cs!=1)
	{
		cigema=0;
		for (int j=fir[i];j;j=nex[j])
		{
			if (a[j].y==fa[i]) continue;
			cigema+=f[a[j].y][1];
		}
		f[i][0]=min(f[i][0],cigema);
	}
	return;
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m;
	cin>>s;
	//cout<<s;
	for (int i=1;i<=n;++i) scanf("%lld",val+i);
	for (int i=1;i<=n-1;++i)
	{
		++p;
		scanf("%d%d",&a[p].x,&a[p].y);
		nex[p]=fir[a[p].x];
		fir[a[p].x]=p;
		++p;
		a[p].x=a[p-1].y;
		a[p].y=a[p-1].x;
		nex[p]=fir[a[p].x];
		fir[a[p].x]=p;
	}
	fa[1]=1;
	dfs(1);
	//for (int i=1;i<=n;++i) cout<<fa[i]<<' ';
	while (m--)
	{
		for (int i=1;i<=n;++i) f[i][0]=f[i][1]=(LL)INF;
		scanf("%d%d%d%d",&aa,&xx,&bb,&yy);
		if (fa[aa]==bb || fa[bb]==aa)
		{
			if (xx==0 && yy==0) 
			{
				printf("-1\n");
				continue;
			}
		}
		sxdp(1);
		//for (int i=1;i<=n;++i) printf("%lld %lld\n",f[i][0],f[i][1]);
		LL ans=min(f[1][0],f[1][1]);
		if (ans>=(LL)INF)
		{
			printf("-1\n");
		}
		else
		{
			printf("%lld\n",ans);
		}
	}
	return 0;
}
