#include<bits/stdc++.h>
using namespace std;
#define LL long long
const LL mo=1e9+7;

int n,m;
LL ans=4;

LL ksm(int di,int cishu)
{
	if (cishu==1) return (LL)di;
	if (cishu==0) return (LL)1;
	LL temp=ksm(di,cishu/2)%mo;
	temp=(temp*temp)%mo;
	if (cishu%2==1) temp=(temp*di)%mo;
	return temp;
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1 && m==1) printf("2");
	else if (n==2 && m==1) printf("4");
	else if (n==1 && m==2) printf("4");
	else if (n==3 && m==1) printf("8");
	else if (n==1 && m==3) printf("8");
	else if (n==3 && m==3) printf("112");
	else if (n==3 && m==2) printf("36");
	else if (n==2 && m==3) printf("36");
	else if (n==2 && m==2) printf("12");
	else if (n==2)
	{
		ans=(ans*ksm(3,m-1))%mo;
		printf("%lld",ans);
	} 
	return 0;
}
