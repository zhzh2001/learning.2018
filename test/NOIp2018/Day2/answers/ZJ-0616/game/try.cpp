#include<bits/stdc++.h>
using namespace std;
#define LL long long

int a[10][10],n=2,m,ans;

bool check()
{
	LL now=0,last=0;
	for (int i=1;i<=m;++i)
	{
		now=0;
		for (int j=1;j<=i;++j) now=now*10+a[1][j];
		for (int j=i;j<=m;++j) now=now*10+a[2][j];
		if (i!=1 && now>last) return 0;
		last=now;
	}
	return 1;
}

void dfs(int i,int j)
{
	if (i==n && j==m)
	{
		a[i][j]=0;
		if (check()) ++ans;
		a[i][j]=1;
		if (check()) ++ans;
		return;
	}
	a[i][j]=0;
	if (j==m) dfs(i+1,1);
	else dfs(i,j+1);
	a[i][j]=1;
	if (j==m) dfs(i+1,1);
	else dfs(i,j+1);
}

int main()
{
	for (int i=1;i<=15;++i)
	{
		ans=0;
		m=i;
		dfs(1,1);
		printf("n=2 m=%d %d\n",m,ans);
	}
}
