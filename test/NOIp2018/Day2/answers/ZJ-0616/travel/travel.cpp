#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define N 5010

struct bian
{
	int x,y;
} a[N*2];

int n,m,fir[N],nex[N],fa[N],p,wg1,wg2,ans[N],ansp,vis[N],ylzxd;
bool tu,wgused;

void dfs(int i)
{
	for (int j=fir[i];j;j=nex[j])
	{
		if (a[j].y!=fa[i])
		{
			if (fa[a[j].y])
			{
				wg1=i;
				wg2=a[j].y;
			}
			else
			{
				fa[a[j].y]=i;
				dfs(a[j].y);
			}
		}
	}
}

void dfs2(int i)
{
	if (!vis[i])
	{
		ans[++ansp]=i;
	}
	vis[i]=1;
	int ch[N],po=0;
	for (int j=fir[i];j;j=nex[j])
	{
		if (a[j].y==fa[j] || vis[a[j].y]) continue;
		ch[++po]=a[j].y;
	}
	sort(ch+1,ch+po+1);
	for (int j=1;j<=po;++j)
	{
		if (!vis[j]) dfs2(ch[j]);
	}
}

void dfswg(int i)
{
	if (!vis[i])
	{
		ans[++ansp]=i;
	}
	vis[i]=1;
	int ch[N],po=0;
	for (int j=fir[i];j;j=nex[j])
	{
		if (a[j].y==fa[i] || vis[a[j].y]) continue;
		ch[++po]=a[j].y;
	}
	sort(ch+1,ch+po+1);
	int fq=fa[i];
	while ((!vis[fq]) && (po>0) && (fq!=1) && (fq<ch[1]))
	{
		vis[fq]=1;
		ans[++ansp]=fq;
		fq=fa[fq];
	}
	dfs2(i);
	if (po==0)
	{
		while((!vis[fq]) && (fq!=1) && (fq<ylzxd))
		{
			vis[fq]=1;
			ans[++ansp]=fq;
			fq=fa[fq];
		}
	}
}

void dfs1(int i)
{
	if (!vis[i])
	{
		ans[++ansp]=i;
	}
	vis[i]=1;
	int lygwg=0;
	if ((i==wg1 || i==wg2) && !wgused)
	{
		lygwg=(i==wg1)?wg2:wg1;
	}
	int ch[N],po=0;
	for (int j=fir[i];j;j=nex[j])
	{
		if (a[j].y==fa[j] || vis[a[j].y]) continue;
		ch[++po]=a[j].y;
	}
	sort(ch+1,ch+po+1);
	for (int j=1;j<=po;++j)
	{
		if (ch[j]!=lygwg)  {if (!vis[ch[j]]) dfs1(ch[j]);}
		else if ((!wgused) && !vis[ch[j]]) 
		{
			wgused=1;
			ylzxd=(j==po)?(n+1):(ch[j+1]);
			dfswg(ch[j]);
		}
	}
}

void dfstu(int i)
{
	vis[1]=1;
	ans[++ansp]=1;
	int kaishi=a[fir[i]].y,beitai=a[nex[fir[i]]].y;
	if (kaishi>beitai) swap(kaishi,beitai);
	while (kaishi<beitai && ansp!=n)
	{
		ans[++ansp]=kaishi;
		vis[kaishi]=1;
		kaishi=(vis[a[fir[kaishi]].y]==1)?(a[nex[fir[kaishi]]].y):(a[fir[kaishi]].y);
	}
	while (ansp!=n)
	{
		ans[++ansp]=beitai;
		vis[beitai]=1;
		beitai=(vis[a[fir[beitai]].y]==1)?(a[nex[fir[beitai]]].y):(a[fir[beitai]].y);
	}
}

bool huan()
{
	for (int i=1;i<=n;++i)
	{
		if (nex[nex[fir[i]]]!=0) return 0;
	}
	return 1;
}

/*void bldfstu(int i)
{
	if (!vis[i])
	{
		vis[i]=1;
	}
	vis
}*/

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==m) tu=1;
	for (int i=1;i<=m;++i)
	{
		++p;
		scanf("%d%d",&a[p].x,&a[p].y);
		nex[p]=fir[a[p].x];
		fir[a[p].x]=p;
		++p;
		a[p].x=a[p-1].y;
		a[p].y=a[p-1].x;
		nex[p]=fir[a[p].x];
		fir[a[p].x]=p;
	}
	if (tu && huan())
	{
		dfstu(1);
		for (int i=1;i<=n;++i) printf("%d ",ans[i]);
		return 0;
	}
	else if (tu)
	{
		//bldfstu(1);
	}
	fa[1]=1;
	dfs(1);
	dfs1(1);
	for (int i=1;i<=n;++i) 
	{
		printf("%d ",ans[i]);
	}
	return 0;
}
