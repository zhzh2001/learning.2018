#include <bits/stdc++.h>
using namespace std;
int n,m;
struct hpac{
	int w;
	vector<int> g;
}a[100010];
char s[100];
long long dp[100010][2];
void dfs(int u,int fa){
	if(a[u].g.size()==1&&fa){
		dp[u][0]=max(dp[u][0],(long long)0),dp[u][1]=max(dp[u][1],(long long)a[u].w);
		return ;
	}
	for(int i=0;i<a[u].g.size();i++){
		int v=a[u].g[i];
		if(v==fa) continue;
		dfs(v,u);
		dp[u][1]+=min(dp[v][0],dp[v][1]);
		dp[u][0]+=dp[v][1];
	}
	dp[u][1]+=a[u].w;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m;
	cin>>s;
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i].w);
	for(int i=1;i<n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		a[u].g.push_back(v);
		a[v].g.push_back(u);
	}
	while(m--){
		int a,x,b,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		memset(dp,0,sizeof(dp));
		dp[a][x^1]=1e10;
		dp[b][y^1]=1e10;
		dfs(1,0);
		printf("%lld\n",min(dp[1][1],dp[1][0])>=1e10?-1:min(dp[1][1],dp[1][0]));
	}
	return 0;
}
