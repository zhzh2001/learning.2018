#include <bits/stdc++.h>
using namespace std;
int n,m,vis[5010],ans[5010],tmp,u[5010],v[5010],huant,huanw,ck,Tmp[5010];
vector<int> g[5010];
void gfs(int u){
	vis[u]=1;
	ans[++tmp]=u;
	for(int i=0;i<g[u].size();i++){
		int v=g[u][i];
		if(vis[v])
			continue ;
		gfs(v);
	}
}
int check(){
	for(int k=1;k<=n;k++){
		if(ans[k]<Tmp[k]) return 1;
		if(Tmp[k]<ans[k]) return 0;
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	memset(Tmp,127,sizeof(Tmp));
	for(int i=1;i<=m;i++){
		scanf("%d%d",&u[i],&v[i]); 
		g[u[i]].push_back(v[i]);
		g[v[i]].push_back(u[i]);
	}
	if(m==n-1){
		for(int i=1;i<=n;i++)
			sort(g[i].begin(),g[i].end());
		gfs(1);
		for(int i=1;i<=n;i++)
			printf("%d ",ans[i]);
		return 0;
	}
	for(int I=1;I<=m;I++){
		for(int j=1;j<=n;j++)
			g[j].clear();
		for(int j=1;j<=m;j++)
			if(j!=I){
				g[u[j]].push_back(v[j]);
				g[v[j]].push_back(u[j]);
			}
		for(int i=1;i<=n;i++)
			sort(g[i].begin(),g[i].end());
		tmp=0;
		memset(ans,0,sizeof(ans));
		memset(vis,0,sizeof(vis));
		gfs(1);
		if(tmp!=n) continue;
		if(check()) 
			for(int j=1;j<=n;j++)
				Tmp[j]=ans[j];
	}
	for(int j=1;j<=n;j++)
		printf("%d ",Tmp[j]);
	return 0;
}
