#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define P 1000000007
int n,m,r;
bool ch[100],sk[100],a[10][10];
bool dfs(int x,int y){
	sk[++r]=a[x][y];
	if(x+1<=n)if(!dfs(x+1,y))return 0;
	if(y+1<=m)if(!dfs(x,y+1))return 0;;
	if(x==n&&y==m){
		for(int i=1;i<=r;++i){
			if(sk[i]>ch[i]){
				--r;
				return 0;
			}
			else if(sk[i]<ch[i]){
				for(int j=1;j<=r;++j)ch[j]=sk[j];
				--r;
				return 1;
			}
		}
	}
	--r;
	return 1;
}
void solve1(){
	int len=(1<<(n*m))-1,ans=0;
	for(int w=0;w<=len;++w){
		for(int i=1;i<=n;++i)for(int j=1;j<=m;++j){
			if(1<<((i-1)*m+j-1)&w)a[i][j]=1;
			else a[i][j]=0;
		}
		r=0;
		for(int i=1;i<=n*m;++i)ch[i]=1;
		if(dfs(1,1))ans++;
	}
	printf("%d",ans);
}
ll ans=4;
void solve2(){
	for(int i=1;i<m;++i)ans=ans*3%P;
	printf("%lld",ans);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d %d",&n,&m);
	if(n<=4&&m<=4)solve1();
	else  solve2();
	return 0;
}
