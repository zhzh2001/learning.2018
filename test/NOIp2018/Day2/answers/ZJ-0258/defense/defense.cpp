#include<bits/stdc++.h>
using namespace std;
#define M 100005
#define ll long long
char dr[1004];
int a[M];
vector<int>d[M];
int p1,p2,p3,p4;
ll dp[2][M];
void dfs(int x,int f){
	ll sum1=0,sum2=0;
	dp[1][x]=1e9,dp[0][x]=1e9;
	for(int i=0;i<(int)d[x].size();++i){
		int v=d[x][i];
		if(v==f)continue;
		dfs(v,x);
		sum1+=min(dp[0][v],dp[1][v]);
		sum2+=dp[1][v];
	}
	dp[1][x]=a[x]+sum1;
	dp[0][x]=sum2;
	if(x==p1)dp[1-p2][x]=1e9;
	if(x==p3)dp[1-p4][x]=1e9;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m;
	scanf("%d %d",&n,&m);
	scanf("%s",dr+1);
	for(int i=1;i<=n;++i)scanf("%d",&a[i]);
	for(int i=1,a,b;i<n;++i){
		scanf("%d %d",&a,&b);
		d[a].push_back(b);
		d[b].push_back(a);
	}
	for(int i=1;i<=m;++i){
		scanf("%d %d %d %d",&p1,&p2,&p3,&p4);
		dfs(1,0);
		ll ans=min(dp[1][1],dp[0][1]);
		if(ans>=1e9)ans=-1;
		printf("%lld\n",ans);
	}
	return 0;
}
