#include<bits/stdc++.h>
using namespace std;
#define M 5005
vector<int>d[M];
int dg[M],sk[M],r,fa[M],vs[M],n,m,mk[M],cnt=0,q[M],ans;
void bs(int x){
	if(mk[x]==cnt)return;
	mk[x]=cnt;
	if(!vs[x])++ans;
	for(int i=0;i<(int)d[x].size();++i){
		int  v=d[x][i];
		if(!vs[v]&&mk[v]!=cnt)bs(v);
	}
}
bool ck(int x){
	while(x)bs(x),x=fa[x];
	return (n-ans)==r;
}
void dfs(int x,int f){
	sk[++r]=x,fa[x]=f,vs[x]=1;
	if(r==n)return;
	int mi=1e9,dq=x,id=x,nw=0;
	while(dq)q[++nw]=dq,dq=fa[dq];
	ans=0,++cnt;
	for(int i=nw;i>=1;--i)if(ck(q[i])){
		for(int j=1;j<=i;++j)for(int k=0;k<(int)d[q[j]].size();++k){
			int v=d[q[j]][k];
			if(vs[v])continue;
			if(v<mi)mi=v,id=q[j];
			break;
		}
		break;
	}
	dfs(mi,id);
}
void dfs1(int x,int f){
	sk[++r]=x;
	for(int i=0;i<(int)d[x].size();++i){
		int v=d[x][i];
		if(v==f)continue;
		dfs1(v,x);
	}
}
int main(){
	freopen("travel.in","r",stdin);
 	freopen("travel.out","w",stdout);
	scanf("%d %d",&n,&m);
	for(int i=1,a,b;i<=m;++i){
		scanf("%d %d",&a,&b);
		d[a].push_back(b);
		d[b].push_back(a);
		dg[a]++,dg[b]++;
	}
	for(int i=1;i<=n;++i)sort(d[i].begin(),d[i].end());
	if(m==n-1)dfs1(1,0);
	else dfs(1,0);
	for(int i=1;i<n;++i)printf("%d ",sk[i]);
	printf("%d",sk[n]);
	return 0;
}
