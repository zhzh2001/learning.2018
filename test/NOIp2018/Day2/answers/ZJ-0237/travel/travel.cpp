#include <bits/stdc++.h>

using namespace std;

bool bmm1;

const int SIZE = 5000 + 5;

int n, m;
vector<int> vec[SIZE];

struct P_MEqNM1 {
	int flg;

	void dfs(int u, int fa) {
		if (flg) printf(" %d", u);
		else     printf("%d",  u), flg = 1;
		int len = vec[u].size();
		for (int i = 0; i < len; ++i) {
			int v = vec[u][i];
			if (v == fa) continue;
			dfs(v, u);
		}
		return;
	}

	void solve(void) {
		flg = 0;
		for (int i = 1; i <= n; ++i) sort(vec[i].begin(), vec[i].end());
		dfs(1, 0), putchar('\n');
		return;
	}
} pMEqNM1;

struct P_MEqN {
	int stk[SIZE], scnt, hasFound;
	int mk[SIZE];

	void dfs1(int u, int fa) {
//		printf("dfs1(%d, %d)\n", u, fa);
		stk[++scnt] = u, mk[u] = 1;
		int flg = 0, len = vec[u].size();
		for (int i = 0; i < len; ++i) {
			int v = vec[u][i];
//			printf("v = %d\n", v);
			if (v == fa) {
				if (flg) {
					stk[++scnt] = v, hasFound = 1;
					return;
				} else {
					flg = 1;
					continue;
				}
			}
			if (mk[v]) {
				stk[++scnt] = v, hasFound = 1;
				return;
			}
			dfs1(v, u);
			if (hasFound) return;
		}
		--scnt, mk[u] = 0;
		return;
	}

	int used, top, topSon;
	int vis[SIZE], isfst;

	void dfs2(int u, int fa) {
		if (vis[u]) return;
		if (isfst) printf("%d", u), isfst = 0;
		else       printf(" %d", u);
		if (top == -1 && mk[u]) {
			top = u;
			int ind = 0;
			for (; vec[u][ind] == fa || !mk[vec[u][ind]]; ++ind);
//			int nxt = vec[u][ind];
			++ind;
			for (; vec[u][ind] == fa || !mk[vec[u][ind]]; ++ind);
			topSon = vec[u][ind];
			int len = vec[u].size();
			for (int i = 0; i < len; ++i) {
				int v = vec[u][i];
//				printf("v = %d\n", v);
				if (v == fa) continue;
				dfs2(v, u);
			}
//			dfs2(nxt, u);
			return;
		}
		int len = vec[u].size();
		int nxt = 0;
		for (; nxt < len && (vec[u][nxt] == fa || !mk[vec[u][nxt]]); ++nxt);
		vis[u] = 1;
		if (mk[u] && vec[u][nxt] > topSon && !used) {
			used = 1;
/*			for (int i = 0; i < len; ++i) {
				int v = vec[u][i];
				if (v == fa || i == nxt) continue;
				dfs2(v, u);
			}
			dfs2(topSon, top);
*/			return;
		}
		for (int i = 0; i < len; ++i) {
			int v = vec[u][i];
			if (v == fa) continue;
			dfs2(v, u);
		}
//		dfs2(nxt, u);
		return;
	}

	void solve(void) {
		for (int i = 1; i <= n; ++i) sort(vec[i].begin(), vec[i].end());
		memset(mk, 0, sizeof mk), scnt = hasFound = 0;
		dfs1(1, 0);
		int ind;
		for (ind = scnt - 1; stk[ind] != stk[scnt]; --ind);
/*		for (int i = ind; i <= scnt; ++i) printf("%d ", stk[i]);
		putchar('\n');
*/		for (int i = ind - 1; i; --i) mk[stk[i]] = 0;
		isfst = 1, memset(vis, 0, sizeof vis), top = -1, used = 0;
		dfs2(1, 0), putchar('\n');
		return;
	}
} pMEqN;

/*
 * Todo:
 *   * [x] 文件 IO
 *   * [x] 内存
 *   * [x] 初始化
 *   * [x] 下标未越界
*/

bool bmm2;

int main(void) {
//	printf("%f\n", (&bmm2 - &bmm1) / 1024.0 / 1024.0);
	freopen("travel.in",  "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; ++i) {
		int ui, vi;
		scanf("%d%d", &ui, &vi);
		vec[ui].push_back(vi), vec[vi].push_back(ui);
	}
	if (m == n - 1) pMEqNM1.solve();
	else pMEqN.solve();
	return 0;
}

