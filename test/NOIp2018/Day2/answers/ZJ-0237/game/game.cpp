#include <bits/stdc++.h>

using namespace std;

bool bmm1;

const int SZN = 8 + 1;
const int MOD = 1e9 + 7;

int mod(int num) {
	if (num >= MOD) num -= MOD;
	return num;
}

int n, m;

struct P20 {
	int tmp[10][10];
	int flg, ans, isok;

	void dfs(int x, int y, int num) {
//		printf("dfs(%d, %d, %d)\n", x, y, num);
		if (x >  n || y >  m) return;
		if (x == n && y == m) {
			if (flg && ans > num) isok = 0;
			flg = 1, ans = num;
			return;
		}
		dfs(x, y + 1, (num << 1) | tmp[x][y + 1]);
		if (!isok) return;
		dfs(x + 1, y, (num << 1) | tmp[x + 1][y]);
		return;
	}

	int solve(void) {
		int nPm = n * m;
		int res = 0;
		for (int i = 0; i < (1 << nPm); ++i) {
			for (int j = 0; j < n; ++j)
				for (int k = 0; k < m; ++k)
					tmp[j + 1][k + 1] = !(!(i & (1 << (j * m + k))));
			isok = 1;
			flg = 0, dfs(1, 1, tmp[1][1]);
			res += isok;
//			if (isok) printf("i = %d, isok = %d\n", i, isok);
		}
		return res;
	}
} p20;

struct P50 {
	int dp[2][5];

	int solve(void) {
/*		for (int i = 0; i < (1 << n); ++i) dp[1][i] = 1;
		for (int i = 2; i <= m; ++i) {
			int sth1 = i & 1, sth2 = sth1 ^ 1;
			for (int j = 0; j < (1 << n); ++j) {
				dp[sth1][j] = 0;
				for (int k = 0; k < (1 << n); ++k) {
					int isok = 1;
					for (int l = 0; l < n - 1; ++l)
						if ((!(!(k & (1 << l)))) < (!(!(j & (1 << (l + 1)))))) {
							isok = 0;
							break;
						}
					if (isok) dp[sth1][j] = mod(dp[sth1][j] + dp[sth2][k]);
				}
//				printf("dp[%d][%d] = %d\n", i, j, dp[sth1][j]);
			}
		}
		int ans = 0;
		for (int i = 0; i < (1 << n); ++i) ans = mod(ans + dp[m & 1][i]);
*/		int ans = 4;
		for (int i = 1; i < m; ++i) ans = mod(ans + mod(ans + ans));
		return ans;
	}
} p50;

struct P65 {
	int solve(void) {
		int ans = 112;
		for (int i = 3; i < m; ++i) ans = mod(ans + mod(ans + ans));
		return ans;
	}
} p65;

struct P_NP1 {
	int solve(void) {
		int ans = 1;
		for (int i = 1; i <= m; ++i) ans = mod(ans << 1);
		return ans;
	}
} pNP1;

/*
 * Todo:
 *   * [x] 文件 IO
 *   * [x] 内存
 *   * [x] 初始化
 *   * [x] 下标未越界
*/

bool bmm2;

int main(void) {
//	printf("%f\n", (&bmm2 - &bmm1) / 1024.0 / 1024.0);
	freopen("game.in",  "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n == 1) {
		printf("%d\n", pNP1.solve());
		return 0;
	}
	if (n == 2) {
		printf("%d\n", p50.solve());
		return 0;
	}
	if (n == 3 && m > 3) {
		printf("%d\n", p65.solve());
		return 0;
	}
	printf("%d\n", p20.solve());
	return 0;
}

