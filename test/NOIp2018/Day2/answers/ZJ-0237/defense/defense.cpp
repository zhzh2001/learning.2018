#include <bits/stdc++.h>

using namespace std;

bool bmm1;

typedef long long ll;

const int SIZE = 300000 + 5;
const int P5SZ = 2000   + 5;
const ll  INF  = 1ll << 40;

struct Edge {
	int v, head;
};

int  ecnt;
int  head[SIZE];
Edge edge[SIZE << 1];

void addEdge(int u, int v) {
	edge[++ecnt] = (Edge) {v, head[u]};
	head[u]      = ecnt;
	return;
}

void addEdges(int u, int v) {
	addEdge(u, v);
	addEdge(v, u);
	return;
}

int n, m;
int pi[SIZE];

struct P55 {
	ll dp[2][SIZE];
	int mk[SIZE];

	P55(void) {
		memset(mk, -1, sizeof mk);
		return;
	}

	void dfs(int u, int fa) {
//		printf("dfs(%d, %d)\n", u, fa);
		dp[1][u] = pi[u], dp[0][u] = 0;
		if (~mk[u]) {
			if (mk[u] == 0) dp[1][u] = INF;
			else            dp[0][u] = INF;
		}
		for (int i = head[u]; ~i; i = edge[i].head) {
			int v = edge[i].v;
			if (v == fa) continue;
			dfs(v, u);
			dp[1][u] += min(dp[0][v], dp[1][v]);
			dp[0][u] += dp[1][v];
		}
//		printf("dp[%d][%d] = %lld, dp[%d][%d] = %lld\n", 0, u, dp[0][u], 1, u, dp[1][u]);
		return;
	}

	ll solve(int a, int x, int b, int y) {
		mk[a] = x, mk[b] = y;
		dfs(1, 0);
		mk[a] = mk[b] = -1;
		return min(dp[0][1], dp[1][1]);
	}
} p55;

char ty[5];

/*
 * Todo:
 *   * [x] 文件 IO
 *   * [x] 内存
 *   * [x] 初始化
 *   * [x] 下标未越界
*/

bool bmm2;

int main(void) {
//	printf("%f\n", (&bmm2 - &bmm1) / 1024.0 / 1024.0);
	freopen("defense.in",  "r", stdin);
	freopen("defense.out", "w", stdout);
	memset(head, -1, sizeof head);
	scanf("%d%d%s", &n, &m, ty);
	for (int i = 1; i <= n; ++i) scanf("%d", &pi[i]);
	for (int i = 1; i < n; ++i) {
		int ui, vi;
		scanf("%d%d", &ui, &vi);
		addEdges(ui, vi);
	}
	while (m--) {
		int ai, xi, bi, yi;
		scanf("%d%d%d%d", &ai, &xi, &bi, &yi);
		ll ans = p55.solve(ai, xi, bi, yi);
		if (ans >= INF) puts("-1");
		else printf("%lld\n", ans);
	}
	return 0;
}

