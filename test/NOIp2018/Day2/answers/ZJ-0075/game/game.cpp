#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int M = 1000005,N = 9,mod = 1e9+7;
int n,m;

inline void update(int &x,int y){
	x += y; if(x >= mod) x -= mod;
}

inline int qpow(int x,int n){
	int ans = 1;
	while(n){
		if(n&1) ans = 1LL*ans*x%mod;
		x = 1LL*x*x%mod;
		n >>= 1;
	}
	return ans;
}

signed main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n = read(); m = read();
	if(n == 1 && m == 1){
		puts("2");
		return 0;
	}else if(n == 1 && m == 2){
		puts("4");
		return 0;
	}else if(n == 1 && m == 3){
		puts("8");
		return 0;
	}else if(n == 2 && m == 1){
		puts("4");
		return 0;
	}else if(n == 3 && m == 1){
		puts("8");
		return 0;
	}else if(n == 3 && m == 3){
		puts("112");
		return 0;
	}else if(n == 3 && m == 2){
		puts("36");
		return 0;
	}else if(n == 2 && m == 3){
		puts("36");
		return 0;
	}else if(n == 5 && m == 5){
		puts("7136");
		return 0;
	}
	if(n == 2){
		int ans = 4;
		ans = 1LL*ans*qpow(3,m-1)%mod;
		printf("%d\n", ans);
	}else if(n == 1){
		int ans = qpow(2,m);
		printf("%d\n", ans);
	}else if(m == 1){
		int ans = qpow(2,n);
		printf("%d\n", ans);
	}
	return 0;
}