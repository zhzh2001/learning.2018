#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 5005;
int n,m;
vector<int> to[N];
int ans[N],Cnt;
int Ans[N];
int fa[N];
bool vis[N];
bool cir[N];
int tops,downs;
bool mp[N][N];

void dfs(int x,int fa){
	ans[++Cnt] = x;
	sort(to[x].begin(),to[x].end());
	for(int i = 0;i < to[x].size();++i){
		int y = to[x][i];
		if(y != fa){
			dfs(y,x);
		}
	}
}

void dfs1(int x,int f){
	sort(to[x].begin(),to[x].end());
	vis[x] = true;
	fa[x] = f;
	for(int i = 0;i < to[x].size();++i){
		int y = to[x][i];
		if(y != f){
			if(vis[y]){
				tops = x;
				downs = y;
			}else{
				dfs1(y,x);
			}
		}
	}
}

void dfs3(int x){
	vis[x] = true;
	Ans[++Cnt] = x;
	for(int i = 0;i < to[x].size();++i){
		int y = to[x][i];
		if(!vis[y] && !mp[x][y])
			dfs3(y);
	}
}

inline bool check(){
	for(int i = 1;i <= n;++i)
		if(Ans[i] < ans[i])
			return true;
		else if(Ans[i] > ans[i])
			return false;
	return false;
}

signed main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n = read(); m = read();
	for(int i = 1;i <= m;++i){
		int a = read(),b = read();
		to[a].push_back(b);
		to[b].push_back(a);
	}
	if(m == n-1){
		dfs(1,1);
		for(int i = 1;i < n;++i)
			printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}else{
		dfs1(1,1);
		int ps = downs;
		cir[ps] = true;
		while(ps != tops){
			ps = fa[ps];
			cir[ps] = true;
		}
		ps = downs;
		memset(vis,0,sizeof vis);
		mp[tops][downs] = true;
		mp[downs][tops] = true;
		dfs3(1);
		for(int i = 1;i <= n;++i)
			ans[i] = Ans[i];
		mp[tops][downs] = false;
		mp[downs][tops] = false;
		while(ps != tops){
			mp[ps][fa[ps]] = mp[fa[ps]][ps] = true;
			Cnt = 0;
			memset(vis,0,sizeof vis);
			dfs3(1);
			mp[ps][fa[ps]] = mp[fa[ps]][ps] = false;
			if(check()){
				for(int i = 1;i <= n;++i)
					ans[i] = Ans[i];
			}
			ps = fa[ps];
		}
		for(int i = 1;i < n;++i)
			printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}
	return 0;
}