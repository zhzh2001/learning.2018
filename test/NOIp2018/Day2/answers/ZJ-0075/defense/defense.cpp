#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
typedef long long ll;

const int N = 100005;
int n,m;
char s[5];
int a[N],fa[N];
int to[N*2],nxt[N*2],head[N],cnt;
bool pos[N],bmon[N];
ll f[2][N];
bool isleaf[N];

inline void insert(int x,int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}

inline ll Min(ll x,ll y){
	if(x < y) return x; return y;
}

void dfs2(int x,int f){
	fa[x] = f;
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != f){
			isleaf[x] = true;
			dfs2(to[i],x);
		}
}

void dfs(int x){
	f[1][x] = a[x];
	f[0][x] = 0;
	ll Mi = 1e18;
	bool flag = false;
	for(int i = head[x];i;i = nxt[i]){
		if(to[i] == fa[x]) continue;
		int y = to[i];
		dfs(y);
		f[1][x] += Min(f[1][y],f[0][y]);
		f[0][x] += f[1][y];
	}
	if(pos[x]){
		if(bmon[x]){
			f[0][x] = 1e11;
		}else{
			f[1][x] = 1e11;
		}
	}
}

signed main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n = read(); m = read(); scanf("%s",s+1);
	for(int i = 1;i <= n;++i) a[i] = read();
	for(int i = 1;i < n;++i){
		int x = read(),y = read();
		insert(x,y); insert(y,x);
	}
	dfs2(1,1);
	if(s[2] == '2'){
		while(m--){
			int a1 = read(),b1 = read(),a2 = read(),b2 = read();
			puts("-1");
		}
		return 0;
	}else{
		while(m--){
			int a1 = read(),b1 = read(),a2 = read(),b2 = read();
			if(fa[a1] == a2 || fa[a2] == fa[a1]){
				if(b1 == 0 && b2 == 0){
					puts("-1");
					continue;
				}
			}
			pos[a1] = true; bmon[a1] = b1;
			pos[a2] = true; bmon[a2] = b2;
			dfs(1);
			ll Mi = min(f[1][1],f[0][1]);
			if(Mi > 1e10){
				puts("-1");
			}else printf("%lld\n", Mi);
			pos[a1] = false;
			pos[a2] = false;
			for(int i = 1;i <= n;++i)
				f[0][i] = f[1][i] = 0;
		}
	}
	
	return 0;
}