#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
using namespace std;
const int N=5005;
struct node{
	int v,p;
};
int cmp(node a,node b){return a.v<b.v;}
vector<node> G[N];
int a[N],b[N],cnt,n,m,Q[N];
bool vis[N];
inline void add(int u,int v,int k){
	node t;t.v=v;t.p=k;
	G[u].push_back(t);
	t.v=u;
	G[v].push_back(t);
}
void work1(int u,int fa){
	int l=G[u].size(),i;
	a[++cnt]=u;
	for(i=0;i<l;i++)if(G[u][i].v!=fa)work1(G[u][i].v,u);
}
inline void Main1(){
	cnt=0;
	work1(1,0);
	for(int i=1;i<=n;i++)printf("%d ",a[i]);
}
inline bool bfs(int k){
	int h=-1,r=0,i,l,u,v;
	memset(vis,0,sizeof(vis));
	Q[0]=1;vis[1]=1;
	while(h<r){
		u=Q[++h];l=G[u].size();
		for(i=0;i<l;i++)if(G[u][i].p!=k&&!vis[v=G[u][i].v]){
			vis[v]=1;
			Q[++r]=v;
		}
	}
	for(i=1;i<=n;i++)if(!vis[i])return 0;
	return 1;
}
void work2(int u,int fa,int k){
	int l=G[u].size(),i;
	b[++cnt]=u;
	for(i=0;i<l;i++)if(G[u][i].p!=k&&G[u][i].v!=fa)work2(G[u][i].v,u,k);
}
inline void Main2(){
	int i,j;
	bool ok;
	for(i=1;i<=n;i++)a[i]=n-i+1;
	for(i=0;i<m;i++){
		if(!bfs(i))continue;
		cnt=0;
		work2(1,0,i);
		ok=1;
		for(j=1;j<=n;j++)if(b[j]<a[j])break;
		else if(a[j]<b[j]){
			ok=0;
			break;
		}
		if(ok)memcpy(a,b,sizeof(a));
	}
	for(i=1;i<=n;i++)printf("%d ",a[i]);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int i,u,v;
	scanf("%d%d",&n,&m);
	for(i=0;i<m;i++){
		scanf("%d%d",&u,&v);
		add(u,v,i);
	}
	for(i=1;i<=n;i++)sort(G[i].begin(),G[i].end(),cmp);
	if(m==n-1)Main1();
	else Main2();
	return 0;
}
