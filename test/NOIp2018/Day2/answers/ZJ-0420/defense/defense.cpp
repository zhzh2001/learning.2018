#include<cstdio>
#include<cstring>
const int N=100050;
typedef long long LL;
int G[N],to[N<<1],nxt[N<<1],sz=-1,p[N],n,m,a,b,x,y;
LL f[N][2];
bool can[N][2];
char s[3];
LL num[N<<2][4];
inline LL Min(LL a,LL b){return a<b?a:b;}
inline void add(int u,int v){
	to[++sz]=v;nxt[sz]=G[u];G[u]=sz;
	to[++sz]=u;nxt[sz]=G[v];G[v]=sz;
}
void dfs(int u,int fa){
	int i,v;
	can[u][0]=can[u][1]=1;
	if(u==a)can[u][x^1]=0;
	if(u==b)can[u][y^1]=0;
	f[u][0]=0;f[u][1]=p[u];
	for(i=G[u];i!=-1;i=nxt[i])if((v=to[i])!=fa){
		dfs(v,u);
		if(can[u][0])if(!can[v][1])can[u][0]=0;
		else f[u][0]+=f[v][1];
		if(can[u][1])if(!can[v][0])if(!can[v][1])can[u][1]=0;
		else f[u][1]+=f[v][1];
		else if(!can[v][1])f[u][1]+=f[v][0];
		else f[u][1]+=Min(f[v][0],f[v][1]);
	}
	//printf("%d:%d %lld %d %lld\n",u,can[u][0],f[u][0],can[u][1],f[u][1]);
}
void build(int o,int L,int R){
	if(L==R){num[o][0]=0;num[o][1]=num[o][2]=num[o][3]=p[L];}
	else{
		int lc=o<<1,rc=lc|1,M=(L+R)>>1;
		build(lc,L,M);build(rc,M+1,R);
		num[o][0]=Min(Min(num[lc][0]+num[rc][1],num[lc][2]+num[rc][0]),num[lc][2]+num[rc][1]);
		num[o][1]=Min(Min(num[lc][1]+num[rc][1],num[lc][3]+num[rc][0]),num[lc][3]+num[rc][1]);
		num[o][2]=Min(Min(num[lc][0]+num[rc][3],num[lc][2]+num[rc][2]),num[lc][2]+num[rc][3]);
		num[o][3]=Min(Min(num[lc][1]+num[rc][3],num[lc][3]+num[rc][2]),num[lc][3]+num[rc][3]);
	}
}
LL ask(int o,int L,int R,int x,int y,int p){
	if(x<=L&&y>=R)return num[o][p];
	else{
		int lc=o<<1,rc=lc|1,M=(L+R)>>1;
		if(x<=M)if(y>M){
			if(p==0)return Min(Min(ask(lc,L,M,x,y,0)+ask(rc,M+1,R,x,y,1),ask(lc,L,M,x,y,2)+ask(rc,M+1,R,x,y,0)),ask(lc,L,M,x,y,2)+ask(rc,M+1,R,x,y,1));
			if(p==1)return Min(Min(ask(lc,L,M,x,y,1)+ask(rc,M+1,R,x,y,1),ask(lc,L,M,x,y,3)+ask(rc,M+1,R,x,y,0)),ask(lc,L,M,x,y,3)+ask(rc,M+1,R,x,y,1));
			if(p==2)return Min(Min(ask(lc,L,M,x,y,0)+ask(rc,M+1,R,x,y,3),ask(lc,L,M,x,y,2)+ask(rc,M+1,R,x,y,2)),ask(lc,L,M,x,y,2)+ask(rc,M+1,R,x,y,3));
			if(p==3)return Min(Min(ask(lc,L,M,x,y,1)+ask(rc,M+1,R,x,y,3),ask(lc,L,M,x,y,3)+ask(rc,M+1,R,x,y,2)),ask(lc,L,M,x,y,3)+ask(rc,M+1,R,x,y,3));
		}else return ask(lc,L,M,x,y,p);
		else return ask(rc,M+1,R,x,y,p);
	}
}
inline void Main1(){
	int t;
	LL ans;
	build(1,1,n);
	while(m--){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if(a>b){
			t=a;a=b;b=t;
			t=x;x=y;y=t;
		}
		if(!x&&!y&&b-a==1)puts("-1");
		else{
			if(a>1)ans=(x==1?Min(Min(ask(1,1,n,1,a-1,0),ask(1,1,n,1,a-1,1)),Min(ask(1,1,n,1,a-1,2),ask(1,1,n,1,a-1,3))):Min(ask(1,1,n,1,a-1,2),ask(1,1,n,1,a-1,3)));
			if(x==1)if(y==1)ans+=Min(Min(ask(1,1,n,a+1,b-1,0),ask(1,1,n,a+1,b-1,1)),Min(ask(1,1,n,a+1,b-1,2),ask(1,1,n,a+1,b-1,3)));
			else ans+=Min(ask(1,1,n,a+1,b-1,2),ask(1,1,n,a+1,b-1,3));
			else if(y==1)ans+=Min(ask(1,1,n,a+1,b-1,0),ask(1,1,n,a+1,b-1,1));
			else ans+=ask(1,1,n,a+1,b-1,3);
			if(b<n)ans+=(y==1?Min(Min(ask(1,1,n,b+1,n,0),ask(1,1,n,b+1,n,1)),Min(ask(1,1,n,b+1,n,2),ask(1,1,n,b+1,n,3))):Min(ask(1,1,n,b+1,n,0),ask(1,1,n,b+1,n,1)));
			if(x==1)ans+=p[x];
			if(y==1)ans+=p[y];
			printf("%lld\n",ans);
		}
	}
}
inline void Main2(){
	int u,v;
	memset(G,-1,sizeof(G));
	for(int i=1;i<n;i++){
		scanf("%d%d",&u,&v);
		add(u,v);
	}
	while(m--){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		dfs(1,0);
		if(!can[1][0])if(!can[1][1])puts("-1");
		else printf("%lld\n",f[1][1]);
		else if(!can[1][1])printf("%lld\n",f[1][0]);
		else printf("%lld\n",Min(f[1][0],f[1][1]));
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int i,u,v;
	scanf("%d%d%s",&n,&m,s);
	for(i=1;i<=n;i++)scanf("%d",&p[i]);
	if(n>=5000&&m>=5000&&s[0]=='A')Main1();
	else Main2();
	return 0;
}
