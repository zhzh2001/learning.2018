#include <bits/stdc++.h>
using namespace std;
const int mod = 1000000007;
int n, m;
inline int add(int x, int y) {
	x += y; return x < mod ? x : x - mod;
}
inline int dec(int x, int y) {
	x -= y; return x < 0 ? x + mod : x;
}
inline int mul(int x, int y) {
	return 1ll * x * y % mod;
}
inline int fpow(int x, int y) {
	int ans = 1;
	while (y) {
		if (y & 1) ans = mul(ans, x);
		y >>= 1; x = mul(x, x);
	}
	return ans;
}
int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n > m) swap(n, m);
	if (n == 1) printf("%d", fpow(2, m));
	else if (n == 2) printf("%d", mul(4, fpow(3, m - 1)));
	else if (n == 3) printf("%d", mul(112, fpow(3, m - 3)));
	else if (n == 4) {
		if (m == 4) printf("912");
		else printf("%d", mul(2688, fpow(3, m - 5)));
	} else if (n == 5) {
		if (m == 5) printf("7136");
		else printf("%d", mul(7136, fpow(3, m - 5)));
	}
	fclose(stdin);
	fclose(stdout);
}
