#include <bits/stdc++.h>
using namespace std;
const int maxn = 5000;
int n, m;
int a[maxn + 10][maxn + 10], cnt[maxn + 10];
int egl[maxn + 10], egr[maxn + 10];
int banl, banr;
int b[maxn + 10], bcnt, ans[maxn + 10];
bool vis[maxn + 10];

inline bool valid(int l, int r) {
	return (l != banl || r != banr) && (l != banr || r != banl);
}

void dfs(int p) {
	b[++bcnt] = p; vis[p] = 1;
	for (int i = 1; i <= cnt[p]; ++i) {
		int e = a[p][i];
		if (!vis[e] && valid(p, e)) dfs(e);
	}
}

void solve() {
	bcnt = 0;
	memset(vis, 0, sizeof vis);
	dfs(1);
	if (bcnt == n) {
		for (int i = 1; i <= n; ++i)
			if (b[i] > ans[i]) return;
			else if (b[i] < ans[i]) break;
		for (int i = 1; i <= n; ++i) ans[i] = b[i];
	}
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	ans[1] = 1e9;
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; ++i) {
		int l, r; scanf("%d%d", &l, &r);
		a[l][++cnt[l]] = r;
		a[r][++cnt[r]] = l;
		egl[i] = l; egr[i] = r;
	}
	for (int i = 1; i <= n; ++i)
		sort(a[i] + 1, a[i] + cnt[i] + 1);
	if (m == n) {
		for (int i = 1; i <= m; ++i) {
			banl = egl[i]; banr = egr[i];
			solve();
		}
	} else solve();
	for (int i = 1; i <= n; ++i) printf("%d ", ans[i]);
	fclose(stdin);
	fclose(stdout);
}
