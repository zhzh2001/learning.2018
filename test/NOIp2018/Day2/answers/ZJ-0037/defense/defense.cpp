#include <bits/stdc++.h>
#define FOR(a,b,c) for(int a=(b);a<=(c);a++)
#define N 120000
#define ll long long
using namespace std;
void read(int &x){
	int ss=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0'&&c<='9')ss=ss*10+c-'0',c=getchar();
	x=ss*f;
}
struct qu{
	int a,x,b,y;
	void scan(){read(a),read(x),read(b),read(y);}
}Q[N];
int n,m,val[N],head[N],ver[2*N],Next[2*N],tot;
void add(int x,int y){
	ver[tot]=y;
	Next[tot]=head[x];
	head[x]=tot++;
}
struct P1_11{
	ll dp[N][2];
	int Fa[N];
	void dfs(int x,int f){
		Fa[x]=f;
		for(int e=head[x];~e;e=Next[e]){
			int v=ver[e];if(v==f)continue;
			dfs(v,x);
		}
	}
	void redfs(int x,int f,int p){
		dp[x][1]=val[x];
		for(int e=head[x];~e;e=Next[e]){
			int v=ver[e];if(v==f)continue;
			redfs(v,x,p);
			if(v==Fa[Q[p].b]&&Q[p].y==0){
				dp[x][1]+=dp[v][1];
				dp[x][0]+=dp[v][1];
			}
			else if(v==Q[p].b){
				if(Q[p].y==0)dp[x][1]+=dp[v][0];
				else{
					dp[x][1]+=dp[v][1];
					dp[x][0]+=dp[v][1];
				}
			}
			else{
				dp[x][1]+=min(dp[v][0],dp[v][1]);
				dp[x][0]+=dp[v][1];
			}
		}
	}
	void solve(){
		FOR(i,1,m){
			memset(dp,0,sizeof(dp));
			int fl=0;
			for(int e=head[Q[i].a];~e;e=Next[e])
				if(ver[e]==Q[i].b){fl=1;break;}
			if(fl&&Q[i].x==0&&Q[i].y==0){
				printf("-1\n");continue;
			}
			dfs(Q[i].a,0);
			redfs(Q[i].a,0,i);
			printf("%lld\n",dp[Q[i].a][Q[i].x]);
		}
	}
}p1_11;
struct P14_16{
	ll dp1[N][2],dp2[N][2];
	int Fa1[N],Fa2[N],dee1[N],dee2[N];
	void dfs(int x,int f){
		Fa2[x]=f;dee2[x]=dee2[f]+1;
		for(int e=head[x];~e;e=Next[e]){
			int v=ver[e];if(v==f)continue;
			dfs(v,x);
		}
	}
	void redfs(int x,int f){
		dp2[x][1]=val[x];
		for(int e=head[x];~e;e=Next[e]){
			int v=ver[e];if(v==f)continue;
			redfs(v,x);
			dp2[x][1]+=min(dp2[v][0],dp2[v][1]);
			dp2[x][0]+=dp2[v][1];
		}
	}
	void solve(){
		dfs(1,0),redfs(1,0);
		memcpy(dp1,dp2,sizeof(dp1));
		memcpy(Fa1,Fa2,sizeof(Fa1));
		memcpy(dee1,dee2,sizeof(dee1));
		memset(dp2,0,sizeof(dp2));
		memset(Fa2,0,sizeof(Fa2));
		memset(dee2,0,sizeof(dee2));
		dfs(n,0),redfs(n,0);
		FOR(i,1,m){
			if(Q[i].x==0&&Q[i].y==0){
				printf("-1\n");continue;
			}
			if(dee1[Q[i].a]>dee1[Q[i].b])
				swap(Q[i].a,Q[i].b),swap(Q[i].x,Q[i].y);
			printf("%lld\n",dp2[Q[i].a][Q[i].x]+dp1[Q[i].b][Q[i].y]);
		}
	}
}p14_16;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof(head));
	char s[5];
	read(n),read(m);
	scanf("%s",s);
	FOR(i,1,n)read(val[i]);
	FOR(i,1,n-1){
		int x,y;read(x),read(y);
		add(x,y),add(y,x);
	}
	FOR(i,1,m)Q[i].scan();
	if(n<=2000&&m<=2000)p1_11.solve();
	else if(s[0]=='A'&&s[1]=='2')p14_16.solve();
	return 0;
}
