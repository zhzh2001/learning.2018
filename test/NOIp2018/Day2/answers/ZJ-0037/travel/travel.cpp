#include <bits/stdc++.h>
#define FOR(a,b,c) for(int a=(b);a<=(c);a++)
#define N 5200
using namespace std;
void read(int &x){
	int ss=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0'&&c<='9')ss=ss*10+c-'0',c=getchar();
	x=ss*f;
}
void print(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)print(x/10);
	putchar(x%10+'0');
}
int n,m;
vector<int> E[N];
struct P50{
	int tot,ans[N],vis[N];
	void dfs(int x){
		vis[x]=1;ans[++tot]=x;
		FOR(i,0,(int)E[x].size()-1){
			int v=E[x][i];if(vis[v])continue;
			dfs(v);
		}
	}
	void solve(){
		tot=0;memset(vis,0,sizeof(vis));
		dfs(1);
		FOR(i,1,n-1)print(ans[i]),putchar(' ');
		print(ans[n]),puts("");
	}
}p50;
struct P100{
	int vis[N],ans[N],now[N],sta[N],top,hh[N],tmp;
	int fl1,tot;
	int weix,weiy;
	void dfs_h(int x,int f){
		if(vis[x]){
			int y;
			do{
				y=sta[top--];
				hh[++tmp]=y;
			}while(y!=x);
			fl1=1;
			return;
		}
		sta[++top]=x;vis[x]=1;
		FOR(i,0,(int)E[x].size()-1){
			int v=E[x][i];if(v==f)continue;
			dfs_h(v,x);
			if(fl1)return;
		}
		top--;
	}
	void redfs(int x,int f){
		now[++tot]=x;
		FOR(i,0,(int)E[x].size()-1){
			int v=E[x][i];if(v==f)continue;
			if(x==weix&&v==weiy)continue;
			if(x==weiy&&v==weix)continue;
			redfs(v,x);
		}
	}
	void work(){
		if(ans[1]==-1){
			memcpy(ans,now,sizeof(ans));
			return;
		}
		FOR(i,1,n){
			if(ans[i]>now[i]){
				memcpy(ans,now,sizeof(ans));
				break;
			}
			else if(ans[i]<now[i])break;
		}
	}
	void solve(){
		memset(ans,-1,sizeof(ans));
		dfs_h(1,-1);
		tot=0,weix=hh[1],weiy=hh[tmp];
		redfs(1,-1),work();
		FOR(i,1,tmp-1){
			tot=0,weix=hh[i],weiy=hh[i+1];
			redfs(1,-1),work();
		}
		FOR(i,1,n-1)print(ans[i]),putchar(' ');
		print(ans[n]),putchar('\n');
	}
}p100;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n),read(m);
	FOR(i,1,m){
		int x,y;read(x),read(y);
		E[x].push_back(y);
		E[y].push_back(x);
	}
	FOR(i,1,n)sort(E[i].begin(),E[i].end());
	if(m==n-1)p50.solve();
	else p100.solve();
	return 0;
}
