#include<iostream>
#include<cstdio>
#include<cstring>

int n,m;
struct edge{
	int to,nxt;
}E[300000];
int H[200000],tot;
void add_edge(int a,int b){
	E[++tot]=(edge){b,H[a]};H[a]=tot;
	E[++tot]=(edge){a,H[b]};H[b]=tot;
}

long long dp0[200000],dp1[200000];
int u,v;
int p[200000];

int a,x,b,y;
bool lim[200000];
int limval[200000];

void DP(int now,int fa){
	dp0[now]=0;
	dp1[now]=p[now];
	for (int i=H[now];i;i=E[i].nxt){
		if (E[i].to==fa) continue;
		DP(E[i].to,now);
		dp0[now]+=dp1[E[i].to];
		dp1[now]+=std::min(dp0[E[i].to],dp1[E[i].to]);
	}
	if (lim[now]){
		if (limval[now]==1) dp0[now]=1LL<<42;
		else dp1[now]=1LL<<42;
	}
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%*s",&n,&m);
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++){
		scanf("%d%d",&u,&v);
		add_edge(u,v);
	}
	for (int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		lim[a]=lim[b]=true;
		limval[a]=x;
		limval[b]=y;
		for (int j=1;j<=n;j++) dp0[j]=dp1[j]=1LL<<42;
		DP(1,0);
		lim[a]=lim[b]=false;
		limval[a]=limval[b]=0;
		long long ans=std::min(dp0[1],dp1[1]);
		if (ans>=1LL<<42) puts("-1");
		else printf("%lld\n",ans);
	}
}
