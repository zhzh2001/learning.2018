#include<iostream>
#include<cstdio>
#include<algorithm>
#include<vector>

int n,m;
int u,v;

struct edge{
	int to,nxt;
}E[20000];
int H[10000],tot;
void add_edge(int a,int b){
	E[++tot]=(edge){b,H[a]};H[a]=tot;
	E[++tot]=(edge){a,H[b]};H[b]=tot;
}

bool vis[10000];
std::vector<int> son[10000];
std::pair<int,int> Ban;

inline void Pre(int now,int fa){
	for (int i=H[now];i;i=E[i].nxt){
		if (E[i].to==fa) continue;
		if ((now==Ban.first&&E[i].to==Ban.second)||(now==Ban.second&&E[i].to==Ban.first)) continue;
		son[now].push_back(E[i].to);
		Pre(E[i].to,now);
	}
	std::sort(son[now].begin(),son[now].end());
}

inline void solve(int now){
	printf("%d ",now);
	for (std::vector<int>::iterator it=son[now].begin();it!=son[now].end();it++) solve(*it);
}

std::vector<int> Save,Ans;

inline void solvesave(int now){
	Save.push_back(now);
	for (std::vector<int>::iterator it=son[now].begin();it!=son[now].end();it++) solvesave(*it);
}

int stk[10000],top;
bool instk[10000];
int dfn[10000],low[10000],dfncnt;
int col[10000],colcnt;
int size[10000];

std::vector<int> V[10000];

inline void Tarjan(int now,int fa){
	dfn[now]=low[now]=++dfncnt;
	instk[now]=true;
	stk[++top]=now;
	for (int i=H[now];i;i=E[i].nxt){
		if (E[i].to==fa) continue;
		if (!dfn[E[i].to]){
			Tarjan(E[i].to,now);
			low[now]=std::min(low[now],low[E[i].to]);
		}
		else if (instk[E[i].to]) low[now]=std::min(low[now],dfn[E[i].to]);
	}
	if (dfn[now]==low[now]){
		colcnt++;
		do{
			instk[stk[top]]=false;
			col[stk[top]]=colcnt;
			size[colcnt]++;
			V[colcnt].push_back(stk[top]);
		}while (stk[top--]!=now);
	}
}

bool oncir[10000];
int cir;

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		add_edge(u,v);
	}
	if (m==n-1){
		Pre(1,0);
		solve(1);
	}
	else{
		for (register int i=1;i<=n;i++) if (!dfn[i]) Tarjan(i,0);
		for (register int i=1;i<=n;i++) if (size[col[i]]>1) oncir[i]=true,cir=col[i];
		for (register int i=0;i<V[cir].size()-1;i++){
			Ban=std::make_pair(V[cir][i],V[cir][i+1]);
			for (register int j=1;j<=n;j++) son[j].clear();
			Pre(1,0);
			Save.clear();
			solvesave(1);
			if (!Ans.size()||Save<Ans) Ans=Save;
		}
		Ban=std::make_pair(V[cir][0],V[cir][V[cir].size()-1]);
		for (register int j=1;j<=n;j++) son[j].clear();
		Pre(1,0);
		Save.clear();
		solvesave(1);
		if (!Ans.size()||Save<Ans) Ans=Save;
		for (int i=0;i<Ans.size();i++) printf("%d ",Ans[i]);
	}
}
