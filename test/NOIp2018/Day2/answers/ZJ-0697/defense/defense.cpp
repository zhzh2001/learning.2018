#include<cstdio>
#include<cstring>
#define N 300005
int n,m,INF;
char s[30];
int lnk[N],nxt[N],son[N],tot,A,X,B,Y,f[2005][2],a[N];
long long g[N][2],h[N][2];
void add(int x,int y){nxt[++tot]=lnk[x];lnk[x]=tot;son[tot]=y;}
int min(const int x,const int y){if (x<y)return x;else return y;}
long long min(const long long x,const long long y){if (x<y)return x;else return y;}
void dfs(int x,int fa){//printf("%d %d %d %d %d\n",x,X,A,Y,B);
	for (int i=lnk[x];i;i=nxt[i])if (fa!=son[i])dfs(son[i],x);
	if ((x==A&&X==1)||(x==B&&Y==1)){
		f[x][1]=a[x];
		for (int i=lnk[x];i;i=nxt[i])if (fa!=son[i])f[x][1]+=min(f[son[i]][0],f[son[i]][1]);
		if (f[x][1]>INF)f[x][1]=INF;
		return;
	}
	if ((x==A&&X==0)||(x==B&&Y==0)){
		f[x][0]=0;
		for (int i=lnk[x];i;i=nxt[i])if (fa!=son[i])f[x][0]+=f[son[i]][1];
		if (f[x][1]>INF)f[x][1]=INF;
		return;
	}
		f[x][1]=a[x];
		for (int i=lnk[x];i;i=nxt[i])if (fa!=son[i])f[x][1]+=min(f[son[i]][0],f[son[i]][1]);
		if (f[x][1]>INF)f[x][1]=INF;
		f[x][0]=0;
		for (int i=lnk[x];i;i=nxt[i])if (fa!=son[i])f[x][0]+=f[son[i]][1];
		if (f[x][1]>INF)f[x][1]=INF;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s+1);
	for (int i=1;i<=n;i++)scanf("%d",&a[i]);
	for (int i=1;i<n;i++){
		int x,y; scanf("%d%d",&x,&y);
		add(x,y); add(y,x);
	}
	if (n<=2000&&m<=2000){
		for (int i=1;i<=m;i++){
			scanf("%d%d%d%d",&A,&X,&B,&Y);
			memset(f,63,sizeof(f)); INF=f[0][0];
			dfs(1,0);
			if (min(f[1][1],f[1][0])<INF)printf("%d\n",min(f[1][1],f[1][0]));
			else printf("-1\n");
		}
		return 0;
	}
	if (s[2]=='1'){
		g[1][0]=1LL<<60;g[1][1]=a[1];
		for (int i=2;i<=n;i++)g[i][0]=g[i-1][1],g[i][1]=min(g[i-1][0],g[i-1][1])+a[i];
		for (int i=n;i>=2;i--)h[i][0]=h[i+1][1],h[i][1]=min(h[i+1][0],h[i+1][1])+a[i];
		h[1][0]=1LL<<60;h[1][1]=a[1]+min(h[2][0],h[2][1]);
		for (int i=1;i<=m;i++){
			scanf("%d%d%d%d",&A,&X,&B,&Y);
			if (B==1&&Y==0){puts("-1");continue;}
			if (B==1&&Y==1){printf("%lld\n",min(h[1][0],h[1][1]));continue;}
			if (Y==0)printf("%lld\n",g[B-1][1]+h[B+1][1]);
			else printf("%lld\n",a[B]+min(g[B-1][0],g[B-1][1])+min(h[B+1][0],h[B+1][1]));
		}
		return 0;
	}
	for (int i=1;i<=n;i++)g[i][0]=g[i-1][1],g[i][1]=min(g[i-1][0],g[i-1][1])+a[i];
	for (int i=n;i>=1;i--)h[i][0]=h[i+1][1],h[i][1]=min(h[i+1][0],h[i+1][1])+a[i];
	for (int i=1;i<=m;i++){
		scanf("%d%d%d%d",&A,&X,&B,&Y);
		if (A>B){int t=A;A=B;B=t;t=X;X=Y;Y=t;}
		if (!X&&!Y){puts("-1");continue;}
		if (X==0&&Y==1)printf("%lld\n",g[A-1][1]+a[B]+min(h[B+1][0],h[B+1][1]));
		if (X==1&&Y==0)printf("%lld\n",min(g[A-1][0],g[A-1][1])+a[A]+h[B+1][1]);
		if (X==1&&Y==1)printf("%lld\n",min(g[A-1][0],g[A-1][1])+min(h[B+1][0],h[B+1][1])+a[A]+a[B]);
	}
}
