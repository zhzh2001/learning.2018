#include<vector>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 100005
using namespace std;
int tot,n,m,lnk[N],son[N],nxt[N],ps,p[N],ans[N],stk[N],top,ins[N],cle[N],cs,bx,by;
int ls[N],rs[N],val[N],fd;
void add(int x,int y){nxt[++tot]=lnk[x];lnk[x]=tot;son[tot]=y;}
void dfs1(int x,int fa){
	printf("%d ",x); vector<int> a; a.clear();
	for (int i=lnk[x];i;i=nxt[i])if (fa!=son[i])
		a.push_back(son[i]);
	sort(a.begin(),a.end());
	for (int i=0;i<a.size();i++)dfs1(a[i],x);
}
void dfs(int x,int fa){
	if (fd)return;
	ins[x]=1; stk[++top]=x;
	for (int i=lnk[x];i;i=nxt[i])if (fa!=son[i]){
		if (fd)return;
		if (!ins[son[i]])dfs(son[i],x);
		else {
			int o=top;
			do{
				cle[++cs]=stk[o--];
			}while (stk[o]!=son[i]);
			cle[++cs]=son[i];
			fd=1;
		}
		if (fd)return;
	}
	ins[x]=0; top--;
}
void check(int x,int fa){
	p[++ps]=x; bool bo=((x==bx)||(x==by));
	for (int i=ls[x];i<=rs[x];i++)if (fa!=val[i]&&((!bo)||((val[i]!=bx)&&val[i]!=by))){
		check(val[i],x);
	}
}
void ud(){
	for (int i=1;i<=n;i++)if (p[i]<ans[i]){
		memcpy(ans+1,p+1,sizeof(int)*n);
		return;
	}else if (p[i]>ans[i]){return;}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		int x,y; scanf("%d%d",&x,&y);
		add(x,y); add(y,x);
	}
	if (m==n-1){dfs1(1,0);return 0;}
	dfs(1,0); int now=1;
	//for (int i=1;i<=cs;i++)printf("%d ",cle[i]);puts("");
	for (int i=1;i<=n;i++){
		ls[i]=now;
		for (int j=lnk[i];j;j=nxt[j])val[now++]=son[j];
		rs[i]=now-1; sort(val+ls[i],val+rs[i]+1);
	}
	ans[1]=2333333;
	for (int i=1;i<cs;i++){
		bx=cle[i];by=cle[i+1];ps=0;check(1,0);ud();
	} bx=cle[cs];by=cle[1];ps=0;check(1,0);ud();
	for (int i=1;i<=n;i++)printf("%d ",ans[i]);
}
