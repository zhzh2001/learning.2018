#include<bits/stdc++.h>
using namespace std;
const int maxn=100000;
struct node{
	int x,y;
}e[maxn];
int x,y,z,n,m,head[maxn],f[maxn],next1[maxn],to[maxn],a[maxn],w[maxn],t1,o,vis[maxn],w1,w2,top,tot;
vector<int> p[maxn];
int read()
{  int x=0,fl=1;  char ch=getchar();
   while (ch<'0' || ch>'9')  {  if (ch=='-') fl=-1; ch=getchar();}
   while (ch>='0' && ch<='9')  { x=x*10+ch-'0'; ch=getchar();}
   return x*fl;
}
void add(int x,int y)
{  next1[++tot]=head[x];
   head[x]=tot;
   to[tot]=y;
}
void dfs(int x,int fa)
{ a[++top]=x; 
  for (int i=head[x];i;i=next1[i])
    {  int v=to[i];
       if (v==fa)  continue;
       p[x].push_back(v);
	}
   sort(p[x].begin(),p[x].end());
   int len=p[x].size();
   for (int i=0;i<len;i++)
    {  dfs(p[x][i],x); }
}
void dfs1(int x,int fa)
{ //cout<<x<<' '<<fa<<endl;
  for (int i=head[x];i;i=next1[i])
    {  int v=to[i];
       if (v==fa)  continue;
       if (vis[v]==1) {int x1=x; while (x1!=v) w[++t1]=x1,vis[x1]=2,x1=f[x1];vis[v]=2;}
              else if (!vis[v]) { vis[v]=1; f[v]=x; dfs1(v,x);}
    }
}
int main()
{ freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout); 
  n=read();  m=read();
  for (int i=1;i<=m;i++)
  {  x=read(); y=read();
     e[i].x=x; e[i].y=y;  
     add(x,y); add(y,x);
  }
  if (m==n-1)
  {  dfs(1,1);
    for (int i=1;i<=n-1;i++)  printf("%d ",a[i]); 
    printf("%d",a[n]);
  }
  if (m==n)
  {  vis[1]=1;dfs1(1,1);
     if (w[1]>w[t1]) { o=t1; while (w[o-1]<w[1] && o>2) o--; w1=w[o]; w2=w[o-1];}
                else { o=1; while (w[o+1]<w[t1] && o+1<t1) o++; w1=w[o]; w2=w[o+1];}
     // cout<<t1<<' '<<w1<<' '<<w2<<endl;
	 memset(next1,0,sizeof(next1));
	 memset(to,0,sizeof(to));
	 memset(head,0,sizeof(head)); tot=0;
	 for (int i=1;i<=m;i++)
	 { if ((e[i].x==w1 && e[i].y==w2) || (e[i].x==w2 && e[i].y==w1)) continue;
	   add(e[i].x,e[i].y);
	   add(e[i].y,e[i].x); 
	 }   
	 dfs(1,1);
	 for (int i=1;i<=n-1;i++)  printf("%d ",a[i]); 
    printf("%d",a[n]);           
  }
  return 0;
}
