#include<bits/stdc++.h>
using namespace std;
const int mod=1e9+7;
int n,m,a[100][100],w[1000100],tot1,mx,fl;
long long ans;
int read()
{  int x=0,fl=1;  char ch=getchar();
   while (ch<'0' || ch>'9')  {  if (ch=='-') fl=-1; ch=getchar();}
   while (ch>='0' && ch<='9')  { x=x*10+ch-'0'; ch=getchar();}
   return x*fl;
}
void dfs1(int x,int y,int z)
{ if (x==n && y==m) {  if (z<mx)  fl=0; mx=z;}
  if (y<m)  dfs1(x,y+1,z*2+a[x][y+1]);
  if (x<n) dfs1(x+1,y,z*2+a[x+1][y]);
}
void deal()
{ fl=1;
   mx=0; 
  dfs1(1,1,0);
  ans+=fl;
  
}
void dfs(int x,int y)
{ if (x==n+1) { deal();  return; }
  a[x][y]=1;
  if (y==m)  dfs(x+1,1);  else dfs(x,y+1);
  a[x][y]=0;
  if (y==m)  dfs(x+1,1);  else dfs(x,y+1);
}
int main()
{ freopen("game.in","r",stdin);
  freopen("game.out","w",stdout); 
  n=read(); m=read();
  w[0]=1;
  for (int i=1;i<=1000000;i++)  w[i]=w[i-1]*2%mod;
  if (n<=3 && m<=3)
  { ans=0;
    dfs(1,1);
  cout<<ans<<endl;}
  else
  { if (n>m) swap(n,m);
  if (n==1)  { ans=1;  for (int i=1;i<=m;i++) ans=ans*2%mod; cout<<ans<<endl;}
  if (n==2){ ans=4; for (int i=1;i<=m-1;i++) ans=ans*3%mod; cout<<ans<<endl;}
  if (n==3){ans=112;
            for (int i=1;i<=m-3;i++) ans=ans*3%mod;
	        cout<<ans<<endl;  }
  if (n==4 && m==4)  cout<<912<<endl;
  if (n==4 && m==5)  cout<<2688<<endl;
  if (n==4 && m==6)  cout<<8064<<endl;
  if (n==4 && m==7)  cout<<24192<<endl;
  if (n==5 && m==5)  cout<<7136<<endl;
  }
  return 0;
}
/*
3 5
 1008;
 
*/
