#include<bits/stdc++.h>
using namespace std;
const int maxn=100000;
int x,y,z,n,m,tot,zhi1,zhi2,xx,xx1,c[maxn],head[maxn],next1[maxn],to[maxn];
long long f[maxn][2];
char s[100];
bool w[2001][2001];
int read()
{  int x=0,fl=1;  char ch=getchar();
   while (ch<'0' || ch>'9')  {  if (ch=='-') fl=-1; ch=getchar();}
   while (ch>='0' && ch<='9')  { x=x*10+ch-'0'; ch=getchar();}
   return x*fl;
}
void add(int x,int y)
{  next1[++tot]=head[x];
   head[x]=tot;
   to[tot]=y;
}
void dfs(int x,int fa)
{ for (int i=head[x];i;i=next1[i])
   { int v=to[i];
     if (v!=fa)  dfs(v,x);
   }
  if (x==xx || x==xx1)
   { if (x==xx1){  swap(xx,xx1); swap(zhi1,zhi2);}
     //cout<<"qwq"<<x<<' '<<zhi1<<endl;
     f[x][1-zhi1]=1e12;
     if (zhi1==0) 
     {long long u=0; 
	   for (int i=head[x];i;i=next1[i])
	   { int v=to[i];
	     if (v!=fa)  { u+=f[v][1]; }
	   }
	   f[x][zhi1]=u;
	 }
	 else
	 { long long u=0; 
	   for (int i=head[x];i;i=next1[i])
	   { int v=to[i];
	     if (v!=fa)  { u+=min(f[v][1],f[v][0]); }
	   }
	   f[x][zhi1]=u+c[x]; 
	 }
     }
   else
   {  long long  u=0,u1=0;
     for (int  i=head[x];i;i=next1[i])
	   { int v=to[i];
	     if (v!=fa)  { u+=min(f[v][1],f[v][0]); u1+=f[v][1]; }
	   }
	   f[x][1]=u+c[x]; f[x][0]=u1;
   }
  //cout<<x<<' '<<f[x][1]<<' '<<f[x][0]<<endl;
}
int main()
{ freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout); 
  n=read(); m=read();  cin>>s;
  for (int i=1;i<=n;i++)  cin>>c[i];
  for (int i=1;i<=n-1;i++)
  { x=read(); y=read();w[x][y]=w[y][x]=1; add(x,y); add(y,x);}
  for (int o=1;o<=m;o++)
  {  x=read(); zhi1=read(); y=read(); zhi2=read();
  xx=x; xx1=y;
     if (w[x][y]==1 && zhi1==0 && zhi2==0)  cout<<-1<<endl;
     else 
	 {dfs(1,1);
	 if (xx==1)  cout<<f[1][zhi1]<<endl;
	 else
	 if (xx1==1)  cout<<f[1][zhi2]<<endl;
	 else cout<<min(f[1][0],f[1][1])<<endl;}
  } 
  return 0;
}
/*
5 3 C3
2 4 1 3 9
1 5
5 2
5 3
3 4
1 0 3 0
2 1 3 1
1 0 5 0
*/
