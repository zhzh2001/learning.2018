#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#define N 100005
#define M 100005
#define oo 1061109567
using namespace std;
template <class T>
inline void Rd(T &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c='0';}
	do{
		res=(res<<3)+(res<<1)+(c^48);
	}while(c=getchar(),c>=48);
	res*=k;
}
template <class T>
inline void Pt(T res){
	if(res<0){
		putchar('-');
		res=-res;
	}
	if(res>=10)Pt(res/10);
	putchar(res%10+48);
}
int n,m,p[N];
int head[N],edgecnt;
struct edge{
	int v,nxt;	
}e[N];
struct node{
	int a,x,b,y;
}q[M];
struct P1{
	int dp[2005][2];
	int mark[2005];
	void dfs(int x,int f){
		bool ff=1;
		int son[2005],tot=0;
		for(int i=head[x];~i;i=e[i].nxt){
			int v=e[i].v;
			if(v!=f){
				ff=0;
				son[++tot]=v;
				dfs(v,x);
			}
		}
		if(mark[x]){
			
			if(mark[x]-1){
				if(ff)dp[x][1]=p[x];
				else{
					int tmp=0;
					for(int i=1;i<=tot;i++)tmp+=min(dp[son[i]][0],dp[son[i]][1]);
					dp[x][1]=min(dp[x][1],tmp+p[x]);
				}
			}else{
				if(ff)dp[x][0]=0;
				else{
					int tmp=0;
					for(int i=1;i<=tot;i++)tmp+=dp[son[i]][1];
					dp[x][0]=min(dp[x][0],tmp);
				}
			}
		}else if(ff){
			dp[x][0]=0;
			dp[x][1]=p[x];
		}else{
			int tmp=0;
			for(int i=1;i<=tot;i++)tmp+=dp[son[i]][1];
			dp[x][0]=min(dp[x][0],tmp);
			tmp=0;
			for(int i=1;i<=tot;i++)tmp+=min(dp[son[i]][0],dp[son[i]][1]);
			dp[x][1]=min(dp[x][1],tmp+p[x]);
		}
	}
	void solve(){
		for(int i=1;i<=m;i++){
			int a=q[i].a,x=q[i].x,b=q[i].b,y=q[i].y;
			memset(dp,63,sizeof(dp));
			memset(mark,0,sizeof(mark));
			mark[a]=x+1,mark[b]=y+1;
			dfs(1,0);
			int ans=min(dp[1][0],dp[1][1]);
			if(ans==oo)puts("-1");
			else Pt(ans),putchar('\n');
		}
	}
}P1;
struct P2{
	void solve(){
		
	}
}P2;
void add_edge(int u,int v){
	e[++edgecnt]=(edge){v,head[u]};head[u]=edgecnt;
	e[++edgecnt]=(edge){u,head[v]};head[v]=edgecnt;
}
char s[10];
//512M 2s
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof(head));
	Rd(n);Rd(m);scanf("%s",s);
	for(int i=1;i<=n;i++)Rd(p[i]);
	int u,v;
	for(int i=1;i<n;i++){
		Rd(u);Rd(v);
		add_edge(u,v);
	}
	int a,x,b,y;
	for(int i=1;i<=m;i++){
		Rd(a),Rd(x),Rd(b),Rd(y);
		q[i]=(node){a,x,b,y};
	}
	if(n<=2000&&m<=2000)P1.solve();
	else P2.solve();
	return 0;
}
