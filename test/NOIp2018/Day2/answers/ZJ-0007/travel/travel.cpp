#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#define N 5005
#define M 5005
using namespace std;
template <class T>
inline void Rd(T &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c='0';}
	do{
		res=(res<<3)+(res<<1)+(c^48);
	}while(c=getchar(),c>=48);
	res*=k;
}
template <class T>
inline void Pt(T res){
	if(res<0){
		putchar('-');
		res=-res;
	}
	if(res>=10)Pt(res/10);
	putchar(res%10+48);
}
int n,m;
int head[N],edgecnt;
struct edge{
	int v,nxt;
}e[M];
struct node{
	int u,v;
}edg[M];
struct P1{
	void dfs(int x,int f){
		Pt(x),putchar(' ');
		int son[N],tot=0;
		for(int i=head[x];~i;i=e[i].nxt){
			int v=e[i].v;
			if(v!=f)son[++tot]=v;
		}
		sort(son+1,son+tot+1);
		for(int i=1;i<=tot;i++)dfs(son[i],x);
	}
	void solve(){
		dfs(1,0);
		putchar('\n');
	}
}P1;
void add_edge(int u,int v){
	e[++edgecnt]=(edge){v,head[u]};head[u]=edgecnt;
	e[++edgecnt]=(edge){u,head[v]};head[v]=edgecnt;
}
struct P2{
	int x1,x2,t;
	int fa[N],ans[N],tmp[N];
	bool mark[N],mk[M];
	void dfs(int x,int f){
		if(x1||x2)return;
		fa[x]=f;
		mark[x]=1;
		for(int i=head[x];~i;i=e[i].nxt){
			if(x1||x2)return;
			int v=e[i].v;
			if(v==f)continue;
			if(mark[v]){x1=x,x2=v;return;}
			dfs(v,x);
		}
	}
	void DFS(int x,int f){
		tmp[++t]=x;
		int son[N],tot=0;
		for(int i=head[x];~i;i=e[i].nxt){
			int v=e[i].v;
			if(v!=f)son[++tot]=v;
		}
		sort(son+1,son+tot+1);
		for(int i=1;i<=tot;i++)DFS(son[i],x);
	}
	void solve(){
		memset(mark,0,sizeof(mark));
		x1=x2=0;
		dfs(1,0);
		memset(mark,0,sizeof(mark));
		for(int i=x1;i!=x2;i=fa[i])mark[i]=1;
		mark[x2]=1;
		memset(mk,0,sizeof(mk));
		for(int i=1;i<=m;i++){
			int u=edg[i].u,v=edg[i].v;
			if(mark[u]&&mark[v])mk[i]=1;
		}
		for(int i=1;i<=n;i++)
		ans[i]=1e9;
		for(int i=1;i<=m;i++)
		if(mk[i]){
			memset(head,-1,sizeof(head));
			edgecnt=0;
			for(int j=1;j<=m;j++)
			if(j!=i){
				int u=edg[j].u,v=edg[j].v;
				add_edge(u,v);
			}
			t=0;
			DFS(1,0);
			bool f=0;
			for(int j=1;j<=n;j++)
			if(ans[j]<tmp[j])break;
			else if(ans[j]>tmp[j]){f=1;break;}
			if(f)for(int j=1;j<=n;j++)ans[j]=tmp[j];
		}
		for(int i=1;i<=n;i++)
		if(i<n)Pt(ans[i]),putchar(' ');
		else Pt(ans[i]),putchar('\n');
	}
}P2;
//512M 1s
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(head,-1,sizeof(head));
	Rd(n);Rd(m);
	int u,v;
	for(int i=1;i<=m;i++){
		Rd(u);Rd(v);
		add_edge(u,v);
		edg[i]=(node){u,v};
	}
	if(m==n-1)P1.solve();
	else P2.solve();
	return 0;
}
