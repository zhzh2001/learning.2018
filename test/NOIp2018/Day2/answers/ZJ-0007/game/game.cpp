#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#define M 1000005
#define ll long long
#define P 1000000007
using namespace std;
template <class T>
inline void Rd(T &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c='0';}
	do{
		res=(res<<3)+(res<<1)+(c^48);
	}while(c=getchar(),c>=48);
	res*=k;
}
template <class T>
inline void Pt(T res){
	if(res<0){
		putchar('-');
		res=-res;
	}
	if(res>=10)Pt(res/10);
	putchar(res%10+48);
}
int n,m;
struct P1{
	int p;
	int dp[2][8];
	bool mark[8][8];
	void solve(){
		memset(mark,0,sizeof(mark));
		p=1<<n;
		for(int i=0;i<p;i++)
		for(int j=0;j<p;j++){
			bool f=1;
			int r=i,l=j;
			for(int k=1;k<n;k++){
				int R=r%2;
				l/=2;
				int D=l%2;
				r/=2;
				if(D<R)f=0;
			}
			mark[i][j]=f;
		}
		int cur=0;
		for(int i=0;i<p;i++)dp[cur][i]=1;
		for(int i=1;i<m;i++){
			cur=!cur;
			memset(dp[cur],0,sizeof(dp[cur]));
			for(int j=0;j<p;j++)
			for(int k=0;k<p;k++)
			if(mark[k][j])dp[cur][k]=(dp[cur][k]+dp[!cur][j])%P;
		}
		int ans=0;
		for(int i=0;i<p;i++)
		ans=(ans+dp[cur][i])%P;
		Pt(ans),putchar('\n');
	}
}P1;
struct P2{
	void solve(){
		if(m==1)puts("8");
		else if(m==2)puts("36");
		else puts("112");
	}
}P2;
struct P3{
	void solve(){
		
	}
}P3;
//512M 1s
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	Rd(n);Rd(m);
	if(n<=2)P1.solve();
	else if(m<=3)P2.solve();
	else P1.solve();
	return 0;
}
