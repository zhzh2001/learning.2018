#include<bits/stdc++.h>
#define fo(i,j,k) for(int i=(j);i<=(k);i++)
#define fd(i,j,k) for(int i=(j);i>=(k);i--)
#define LL long long
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
using namespace std;
const int N=10008;
int n,m,ans,tot,head[N<<1],a[N],T,u,v,x1,x2,pd1,pd2,sum1,sum2;
char c1,c2;
struct edge{int nxt,toto;}G[N<<1];
void read(int& x){
	int bb=1;char ch=getchar();
	for(;ch<'0' || ch>'9';ch=getchar()) if (ch=='-') bb=-1;
	for(x=0;ch>='0' && ch<='9';x=x*10+ch-'0',ch=getchar());
	x=x*bb;
}
void add(int u,int v){
	G[++tot].nxt=head[u];head[u]=tot;G[tot].toto=v;
}
int make(int l,int r){
	int sm1=0,sm2=0;
	fo(i,l,r)
		if (i%2==0) sm1+=a[i];else sm2+=a[i];
	return min(sm1,sm2);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n);read(T);c1=getchar();c2=getchar();
	fo(i,1,n) read(a[i]);
	fo(i,1,n-1){
		read(u);read(v);
		add(u,v);add(v,u);
	}
	if (c1=='A'){
		if (c2=='1'){
			ans=a[1];
			while(T--){
				read(x1);read(pd1);read(x2);read(pd2);
				if (x1>x2) swap(x1,x2),swap(pd1,pd2);
				if (pd2==0){
					printf("%d\n",a[1]+make(2,n));
					continue;
				}
				if (pd2==1){
					printf("%d\n",a[1]+a[x2]+make(2,x2-1)+make(x2+1,n));continue;
				}
			}
			return 0;
		}
			ans=a[1];
			while(T--){
				read(x1);read(pd1);read(x2);read(pd2);
				if (x1>x2) swap(x1,x2),swap(pd1,pd2);
				if (pd1==0 && pd2==0){
					ans=a[x2+1]+a[x1-1];
					if (x2-x1==2) ans+=a[x2-1];
					printf("%d\n",ans+make(1,x1-2)+make(x2+2,n));
					continue;
				}
				if (pd1!=0 && pd2!=0){
					if (x1==x2) ans=a[x1]; else ans=a[x1]+a[x2];
					ans+=make(1,x1-1)+make(x2+1,n);
					ans+=make(x1+2,x2-2);
					printf("%d\n",ans);
					continue;
				}
				if (pd1!=0){
					ans=a[x1];
					ans+=make(1,x1-1)+make(x1+1,n);
					printf("%d\n",ans);
					continue;
				}
				    ans=a[x2];
					ans+=make(1,x2-1)+make(x2+1,n);
					printf("%d\n",ans);
		    }
			return 0;
		}
	return 0;
}
