#include<bits/stdc++.h>
#define fo(i,j,k) for(int i=(j);i<=(k);i++)
#define fd(i,j,k) for(int i=(j);i>=(k);i--)
#define LL long long
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
using namespace std;
const int mo=1000000007;
long long ans,f[2][257],n,m,nn;
LL ksm(LL x,LL y){
	LL ss=1;
	for(;y;y>>=1){
		if (y&1) ss=(1ll*ss*x)%mo; 
		x=(1ll*x*x)%mo;
	}
	return ss;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%lld %lld",&n,&m);nn=(1<<n);
	if (n==1){
		printf("%d",ksm(2,m));
		return 0;
	}
	if (n==3){
		if (m==1) printf("8\n");
		if (m==2) printf("24\n");
		if (m==3) printf("112\n");
		return 0;
	}
	fo(i,0,nn-1) f[1][i]=1;
	int cur=1;
	fo(i,2,m){
		cur^=1;
		memset(f[cur],0,sizeof(f[cur]));
		fo(j,0,nn-1)
		 fo(k,0,nn-1){
          if (!(k>>1&(j^((1<<n)-1)))) 
		   f[cur][k]=(f[cur][k]+f[cur^1][j])%mo;
	     }
	}
	fo(i,0,nn-1) ans=(ans+f[cur][i])%mo;
	printf("%lld\n",ans);
	return 0;
}
