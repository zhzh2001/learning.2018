#include<bits/stdc++.h>
#define fo(i,j,k) for(int i=(j);i<=(k);i++)
#define fd(i,j,k) for(int i=(j);i>=(k);i--)
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
using namespace std;
const int N=5008;
vector <int> V[N];
int b[N],n,m,u,v,t;
int ff[N],cnt,f[N],VV[N][N],flag,c[N<<1],used[N];
struct edge{int u,v;}a[N];
void dfs1(int x,int fa){
	t++;b[t]=x;
	sort(V[x].begin(),V[x].end());
	int lenn=V[x].size()-1;
	fo(i,0,lenn) 
       if (fa!=V[x][i])
		dfs1(V[x][i],x);
}
void dfs2(int x){
	t++;b[t]=x;
	sort(V[x].begin(),V[x].end());
	int lenn=V[x].size()-1;
	fo(i,0,lenn) 
		dfs2(V[x][i]);
}
void bfs(int x,int fa1,int fa2){
	ff[x]=1;ff[fa1]=1;
	int xx=fa1;
	while(f[xx]!=-1){
		ff[f[xx]]=1;xx=f[xx];
	}
	xx=fa2;
	while(f[xx]!=-1){
		ff[f[xx]]=1;xx=f[xx];
	}
}
void make(int x){
	int l=1,r=1,lenn=0;c[l]=x;used[x]=1;
	while(l<=r){
	int x=c[l];
	lenn=V[x].size()-1;
	fo(i,0,lenn){
		int y=V[x][i];
		if (used[y]==0) {
		 if (f[y]!=-1){bfs(x,y,f[y]);return ;}
		 f[y]=x;used[y]=1;r++;c[r]=y;
	  }
	}
	l++;
  }
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(b,0,sizeof(b));t=0;
	memset(f,-1,sizeof(f));flag=true;
	scanf("%d %d",&n,&m);
	fo(i,1,m){
		scanf("%d",&u);scanf("%d",&v);
		a[i].u=u;a[i].v=v;
		V[u].push_back(v);V[v].push_back(u);
	}
	if (m==n-1){
		dfs1(1,0);
		fo(i,1,t-1) printf("%d ",b[i]);
		printf("%d\n",b[t]);
	}else {
		dfs2(1);
		fo(i,1,t-1) printf("%d ",b[i]);
		printf("%d\n",b[t]);
	}
	return 0;
}
