#include <iostream>
#include <cstdio>
#include <cctype>
#define il inline
#define rg register
#define vd void
#define mn 100005
#define ll long long
#define rep(i,x,y) for(rg int i=x;i<=y;++i)
using namespace std;
il ll Min(ll a,ll b){return a<b?a:b;}
il int rd(){char c; int f=1;
	while(!isdigit(c=getchar())&&c!='-');
	c=='-'?c=getchar(),f=-1:0; int x=c^48;
	while(isdigit(c=getchar())) x=((x+(x<<2))<<1)+(c^48);
	return x*f;
}
vd rt(ll x){x<0?putchar('-'),x=-x:0,putchar((x>=10?rt(x/10),x%10:x)+48);}
int n,m,ty,u,v,U,V,A,B,cnt,p[mn],h[mn];
ll dp[3][mn];
char ch;
struct E{int to,nxt;}e[mn<<1];
il vd Add(int u,int v){e[++cnt]=(E){v,h[u]},h[u]=cnt;}
il vd Get(){while((ch=getchar())!='A'&&ch!='B'&&ch!='C');}
vd Dfs(int u,int fa){
	dp[0][u]=0,dp[1][u]=p[u];
	if(u==U) dp[A][u]=1e13;
	if(u==V) dp[B][V]=1e13;
	for(rg int i=h[u];i;i=e[i].nxt){int v=e[i].to;
		if(v!=fa) Dfs(v,u),dp[0][u]+=dp[1][v],dp[1][u]+=Min(dp[0][v],dp[1][v]);
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=rd(),m=rd(),Get(),ty=rd();
	rep(i,1,n) p[i]=rd();
	rep(i,2,n) u=rd(),v=rd(),Add(u,v),Add(v,u);
	while(m--){
		U=rd(),A=rd(),V=rd(),B=rd(),A^=1,B^=1;
		Dfs(1,0);
		ll ans=Min(dp[0][1],dp[1][1]);
		if(ans<1e13) rt(ans); else rt(-1);
		putchar('\n');
	}
	return 0;
}//long long!!!
