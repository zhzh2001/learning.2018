#include <iostream>
#include <cstdio>
#include <cctype>
#define il inline
#define rg register
#define vd void
#define p 1000000007
#define rep(i,x,y) for(rg int i=x;i<=y;++i)
using namespace std;
int n,m;
il int Pow(int a,int b){int ans=1; for(;b;b&1?ans=1ll*ans*a%p:0,a=1ll*a*a%p,b>>=1); return ans;}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	/*dp[0][1][1]=4,dp[1][1][1]=2;
	rep(i,2,n){
		dp[0][i][1]=2*dp[0][i-1][1],
		dp[1][i][1]=dp[0][i-1][1]+dp[1][i-1][1]*3;
	}
	rep(i,2,m){
		dp[0][1][i]=2*dp[0][1][i-1],
		dp[1][1][i]=dp[0][1][i-1]+dp[1][1][i-1]*3;
	}
	rep(i,2,n) rep(j,2,m){
		dp[0][i][j]=
	}*/
	if(n==1) return printf("%d",Pow(2,m)),0;
	if(m==1) return printf("%d",Pow(2,n)),0;
	if(n==2) return printf("%lld",4ll*Pow(3,m-1)%p),0;
	if(m==2) return printf("%lld",4ll*Pow(3,n-1)%p),0;
	if(n==3&&m==3) return puts("112"),0;
	if(n==5&&m==5) return puts("7136"),0;
	printf("1944");
	return 0;
}
