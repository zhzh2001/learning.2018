#include <iostream>
#include <cstdio>
#include <cctype>
#include <vector>
#include <algorithm>
#include <cstring>
#define il inline
#define rg register
#define vd void
#define mn 5005
using namespace std;
il int Min(int a,int b){return a<b?a:b;}
il int rd(){char c; int f=1;
	while(!isdigit(c=getchar())&&c!='-');
	c=='-'?c=getchar(),f=-1:0; int x=c^48;
	while(isdigit(c=getchar())) x=((x+(x<<2))<<1)+(c^48);
	return x*f;
}
vd rt(int x){x<0?putchar('-'),x=-x:0,putchar((x>=10?rt(x/10),x%10:x)+48);}
int n,m,u,v,cnt,s,f,h[mn],c[mn<<1],vis[mn],ans[mn],Ans[mn];
struct E{int to,nxt;}e[mn<<1];
vector <int> V[mn];
il vd Add(int u,int v){e[++cnt]=(E){v,h[u]},h[u]=cnt;}
vd Dfs(int u,int fa){
	rt(u),putchar(' ');
	for(rg int i=h[u];i;i=e[i].nxt){int v=e[i].to;
		if(v!=fa) V[u].push_back(v);
	}
	sort(V[u].begin(),V[u].end());
	for(rg int i=0;i<V[u].size();++i) Dfs(V[u][i],u);
}
il vd Tree(){Dfs(1,0);}
/*vd Tarjan(int u){
	st[++tp]=u,vis[u]=1,dfn[u]=low[u]=++dfc;
	for(rg int i=h[u];i;i=e[i].nxt){int v=e[i].to;
		if(!dfn[v]) Tarjan(v),low[u]=Min(low[u],low[v]);
		else if(vis[v]) low[u]=Min(low[u],dfn[v]);
	}
	if(dfn[u]==low[u]) for(++cc;st[tp+1]!=u;--tp)
		co[st[tp]]=cc,vis[st[tp]]=0;
}*/
/*vd ddfs(int u,int fa){
	for(rg int i=h[u];i;i=e[i].nxt){int v=e[i].to;
		if(v!=fa) V[u].push_back(v),Dfs(v,u);
	}
}
il int Check(){
	memset(vis,0,sizeof(vis));
	for()
}*/
vd Cfs(int u,int fa){
	//rt(u),putchar(' ');
	if(f) return;
	ans[++s]=u,vis[u]=1;
	for(rg int i=h[u];i;i=e[i].nxt) if(!c[i]) {int v=e[i].to;
		if(v!=fa){
			if(vis[v]){f=1; return;}
			V[u].push_back(v);
		}
	}
	sort(V[u].begin(),V[u].end());
	for(rg int i=0;i<V[u].size();++i) Cfs(V[u][i],u);
}
il int Check(){
	for(rg int i=1;i<=n;++i) if(Ans[i]!=ans[i]) return ans[i]<Ans[i];
	return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=rd(),m=rd();
	for(rg int i=1;i<=m;++i) u=rd(),v=rd(),Add(u,v),Add(v,u);
	if(m==n-1) return Tree(),0;
	//for(rg int i=1;i<=n;++i) if(!dfn[i]) Tarjan(i);
	Ans[1]=1e9;
	for(rg int i=1;i<=cnt;i+=2){
		for(rg int j=1;j<=n;++j) V[j].clear();
		f=0,memset(vis,0,sizeof(vis)),c[i]=c[i+1]=1,s=0,Cfs(1,0),c[i]=c[i+1]=0;
		if(s==n&&Check()) for(rg int j=1;j<=n;++j) Ans[j]=ans[j];
		//if(Check()) Cfs()
	}
	for(rg int i=1;i<=n;++i) rt(Ans[i]),putchar(' ');
	return 0;
}
