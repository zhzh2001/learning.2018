#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cctype>
#include<cmath>
#include<cstring>
#define LL long long
#define N (1000005)
using namespace std;
int choose[11][11];
const int P=1000000007;
template <typename T> void read (T &t){
	t=0;
	char ch=getchar();
	bool fl=0;
	while (!isdigit(ch)){
		if (ch=='-') fl=1;
		ch=getchar();
	}
	do{
		t=t*10+ch-'0';
		ch=getchar();
	}while (isdigit(ch));
	if (fl) t=-t;
}
int n,m,ans;
void out(){
	for (int i=1;i<n;i++){
		for (int j=1;j<m;j++){
			if (choose[i+1][j]<choose[i][j+1]) return;
		}
	}
	for (int i=2;i<n;i++){
		for (int j=2;j<m;j++){
			if (choose[i-1][j]==choose[i][j-1]&&choose[i+1][j]>choose[i][j+1]){
				return;
			}
		}
	}
	ans++;
}
void dfs(int x,int y){
	if (x>n){
		out();
		return;
	}
	choose[x][y]=1;
	if (y==m) dfs(x+1,1);
	else dfs(x,y+1);
	choose[x][y]=0;
	if (y==m) dfs(x+1,1);
	else dfs(x,y+1);
}
int ksm(int a,int b){
	int ret=1;
	while (b>1){
		if (b&1) ret=1ll*ret*a%P;
		a=1ll*a*a%P;
		b>>=1;
	}
	return 1ll*ret*a%P;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n),read(m);
	if (n<=3&&m<=3){
		dfs(1,1);
		printf("%d",ans);
		return 0;
	}
	if (n==2){
		printf("%lld",1ll*ksm(3,m-1)*4%P);
		return 0;
	}
	return 0;
}

