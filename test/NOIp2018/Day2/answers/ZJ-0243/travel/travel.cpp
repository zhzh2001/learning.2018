#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cctype>
#include<cmath>
#include<cstring>
#include<vector>
#define LL long long
#define N (5005)
using namespace std;
int n,m,tot,x,y,h,t,now;
int son[N<<1],nxt[N<<1],head[N],fa[N],l[N],r[N],q[N];
vector <int>chi[N];
bool vis[N];
template <typename T> void read (T &t){
	t=0;
	char ch=getchar();
	bool fl=0;
	while (!isdigit(ch)){
		if (ch=='-') fl=1;
		ch=getchar();
	}
	do{
		t=t*10+ch-'0';
		ch=getchar();
	}while (isdigit(ch));
	if (fl) t=-t;
}
inline void add(int x,int y){
	son[++tot]=y,nxt[tot]=head[x],head[x]=tot;
}
void dfs(int u){
	printf("%d ",u);
	for (int p=head[u];p;p=nxt[p]){
		int v=son[p];
		if (v!=fa[u]){
			fa[v]=u;
			chi[u].push_back(v);
		}
	}
	sort(chi[u].begin(),chi[u].end());
	for (int i=0;i<chi[u].size();i++){
		dfs(chi[u][i]);
	}
}
void get(int u){
	vis[u]=1;
	q[++t]=u;
	if (vis[l[u]]&&vis[r[u]]) return;
	if (!vis[l[u]]) get(l[u]);
	else get(r[u]);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n),read(m);
	for (int i=1;i<=m;i++){
		read(x),read(y);
		add(x,y);
		add(y,x);
	}
	if (m==n-1){
		dfs(1);
		return 0; 
	}
	for (int i=1;i<=n;i++){
		for (int p=head[i];p;p=nxt[p]){
			int v=son[p];
			if (!l[i]) l[i]=v;
			else r[i]=v;
		}
	}
	q[++t]=1;
	vis[1]=1;
	if (l[1]<r[1]){
		get(l[1]);
		int en=0;
		for (int i=1;i<=t;i++){
			if (r[1]<=q[i]){
				en=i-1;
				break;
			}
		}
		for (int i=1;i<=en;i++) printf("%d ",q[i]);
		for (int i=t;i>en;i--) printf("%d ",q[i]);
	}
	else{
		get(r[1]);
		int en=0;
		for (int i=1;i<=t;i++){
			if (l[1]<=q[i]){
				en=i-1;
				break;
			}
		}
		for (int i=1;i<=en;i++) printf("%d ",q[i]);
		for (int i=t;i>en;i--) printf("%d ",q[i]);
	}
	return 0;
}

