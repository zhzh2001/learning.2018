#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cctype>
#include<cmath>
#include<cstring>
#define LL long long
#define N (100005)
using namespace std;
int n,m,tot,op1,x,op2,y,t;
int a[N],son[N<<1],nxt[N<<1],head[N],fa[N],dep[N],change[N],be[N][2];
int f[N][2];
char c[20];
template <typename T> void read (T &t){
	t=0;
	char ch=getchar();
	bool fl=0;
	while (!isdigit(ch)){
		if (ch=='-') fl=1;
		ch=getchar();
	}
	do{
		t=t*10+ch-'0';
		ch=getchar();
	}while (isdigit(ch));
	if (fl) t=-t;
}
inline void add(int x,int y){
	son[++tot]=y,nxt[tot]=head[x],head[x]=tot;
}
void dfs(int u,int fa){
	f[u][0]=0;
	f[u][1]=a[u];
	for (int p=head[u];p;p=nxt[p]){
		int v=son[p];
		if (v!=fa){
			dep[v]=dep[u]+1;
			dfs(v,u);
		}
	}
	for (int p=head[u];p;p=nxt[p]){
		int v=son[p];
		if (v!=fa){
			f[u][0]+=f[v][1];
			f[u][1]+=min(f[v][1],f[v][0]);
		}
	}
	if (u==x){
		if (op1) f[u][0]=1e9;
		else f[u][1]=1e9;
	}
	if (u==y){
		if (op2) f[u][0]=1e9;
		else f[u][1]=1e9;
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n),read(m);
	scanf("%s",c+1);
	for (int i=1;i<=n;i++) read(a[i]);
	for (int i=1;i<n;i++){
		read(x),read(y);
		add(x,y);
		add(y,x);
	}
	if (n<=2000&&m<=2000){
		while (m--){
			read(x),read(op1),read(y),read(op2);
			dfs(1,0);
			int ans=min(f[1][1],f[1][0]);
			if (ans>=1e9) puts("-1");
			else printf("%d\n",ans);	
		}
		return 0;
	}
	return 0;
}

