#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
const int M=10,mo=1e9+7;
int n,m,nn,i,ax,ay,ans;
int st,ed,f[M];
void work(int x){
	int lst=st,led=ed,sum=0,i;
	if (x<m) st=m-x; else st=0;
	if (x>nn+1-m) ed=nn+1-x; else ed=m;
	for (i=lst;i<=led;i++) sum=(sum+f[i])%mo;
	int lt=max(lst,st+1),rt=min(led-2,ed-1);
	for (i=lt;i<=rt;i++) f[i]=f[i+1];
	for (i=st;i<lt;i++) f[i]=sum;
	for (i=rt+1;i<=ed;i++) f[i]=sum;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);nn=n+m-1;
	if (m>n) n^=m,m^=n,n^=m;
	st=m;ed=m;f[m]=1;
	for (i=0;i<m;i++) f[i]=0;
	for (i=1;i<=nn;i+=2) work(i);
	for (i=st;i<=ed;i++) ax=(ax+f[i])%mo;
	st=m;ed=m;f[m]=1;
	for (i=0;i<m;i++) f[i]=0;
	for (i=2;i<=nn;i+=2)
		work(i);
	for (i=st;i<=ed;i++) ay=(ay+f[i])%mo;
	ans=(ll)ax*ay%mo;
	printf("%d",ans);
}
