#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
const int N=1e5+5,M=2*N;
const ll inf=1e16;
int n,m,i,a[N];
int et,he[N],fa[N];
struct edge{int l,to;}e[M];
char sc[5];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(ll x){if (x>9) write(x/10);putchar(x%10+48);}
int dn,d[N];
void dfs(int x){
	for (int i=he[x];i;i=e[i].l){
		int y=e[i].to;if (y==fa[x]) continue;
		fa[y]=x;dfs(y);
	}
	d[++dn]=x;
}
ll f0[N],f1[N];
void solve(){
	int px=read(),kx=read(),py=read(),ky=read();
	for (i=1;i<=n;i++) f0[i]=0,f1[i]=a[i];
	if (kx) f0[px]=inf; else f1[px]=inf;
	if (ky) f0[py]=inf; else f1[py]=inf;
	for (int now=1;now<=n;now++){
		int x=d[now],y=fa[x];
		f0[y]+=f1[x];
		f1[y]+=min(f0[x],f1[x]);
	}
	ll ans=min(f0[1],f1[1]);
	if (ans>=inf) puts("-1");
	else write(ans),putchar('\n');
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",sc);
	for (i=1;i<=n;i++) a[i]=read();
	for (i=1;i<n;i++){
		int x=read(),y=read();
		e[++et].l=he[x];he[x]=et;e[et].to=y;
		e[++et].l=he[y];he[y]=et;e[et].to=x;
	}
	dfs(1);
	for (;m--;) solve();
}
