#include<cstdio>
using namespace std;
const int N=5005,M=2*N;
int n,m,i,ans[N];
int bt,hb[N],et,he[N];
struct edge{int l,to;}b[M],e[M];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int px,py,dn,d[N],flag;
void dfs(int x,int fa){
	d[++dn]=x;
	if (!flag){
		if (x>ans[dn]){flag=2;return;}
		if (x<ans[dn]) flag=1;
	}
	for (int i=he[x];i;i=e[i].l){
		int y=e[i].to;
		if (y==fa||px==x&&py==y||px==y&&py==x) continue;
		dfs(y,x);if (flag==2) return;
	}
}
void work(){
	dn=0;flag=0;dfs(1,0);
	if (flag==1&&dn==n)
		for (int i=1;i<=n;i++) ans[i]=d[i];
}
void solve(){
	if (m==n-1){work();return;}
	for (i=1;i<=m;i++){
		px=b[2*i-1].to;py=b[2*i].to;
		work();
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for (i=1;i<=m;i++){
		int x=read(),y=read();
		b[++bt].l=hb[x];hb[x]=bt;b[bt].to=y;
		b[++bt].l=hb[y];hb[y]=bt;b[bt].to=x;
	}
	for (int x=n;x;x--)
		for (i=hb[x];i;i=b[i].l)
			e[++et].l=he[b[i].to],
			he[b[i].to]=et,e[et].to=x;
	ans[1]=n+1;solve();
	for (i=1;i<=n;i++) write(ans[i]),putchar(' ');
}
