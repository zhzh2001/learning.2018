#include <bits/stdc++.h>
using namespace std;

inline int read(){ static int x; scanf("%d",&x); return x; }
#define FO(i,a,b) for (int i=a;i<=b;++i)
#define FD(i,a,b) for (int i=a;i>=b;--i)
#define fi first
#define se second

#define MO 1000000007
int n,m,a[15][15],ans;
vector<pair<int,int> > V;

void gen(int x,int y,int s1,int s2){
	if (x==n&&y==m){ V.push_back(make_pair(s1,s2)); }
	if (x!=n) gen(x+1,y,s1*2,s2*2+a[x+1][y]);
	if (y!=m) gen(x,y+1,s1*2+1,s2*2+a[x][y+1]);
}
void ck(){
	int mxx=0;
	V.clear();gen(1,1,0,0);
	sort(V.begin(),V.end());
	mxx=V[0].se;
	for (int i=0;i<V.size();++i){
		if (V[i].se>mxx) return ;
		mxx=V[i].se;
	}
//	FO(i,1,n){FO(j,1,m) putchar(a[i][j]+48);puts(""); }puts("");
	ans++;
}
void sc(int x,int y){
	if (x==n+1) { ck(); return ; }
	a[x][y]=1;if (y==m) sc(x+1,1);else sc(x,y+1);
	a[x][y]=0;if (y==m) sc(x+1,1);else sc(x,y+1);
}

int main(void){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();m=read();
	if (n<=3&&m<=3) { sc(1,1); printf("%d\n",ans); return 0 ;}
	if (n==1){
		int xx=2;
		FO(i,2,m) xx=xx*2%MO;
		printf("%d\n",xx);
	}
	if (n==2){
		int xx=4;
		FO(i,2,m) xx=xx*3%MO;
		printf("%d\n",xx);
	}
	if (n==3){
		if (m==1) return puts("8")*0;
		if (m==2) return puts("36")*0;
		if (m==3) return puts("112")*0;
		int xx=112;
		FO(i,4,m) xx=xx*3%MO;
		printf("%d\n",xx);
	}
	if (n==4){
		if (m==1) return puts("16")*0;
		if (m==2) return puts("108")*0;
		if (m==3) return puts("336")*0;
		if (m==4) return puts("912")*0;
		if (m==5) return puts("2688")*0;
		int xx=2688;
		FO(i,6,m) xx=xx*3%MO;
		printf("%d\n",xx);
	}
	if (n==5){
		if (m==1) return puts("32")*0;
		if (m==2) return puts("324")*0;
		if (m==3) return puts("1008")*0;
		if (m==4) return puts("2688")*0;
		if (m==5) return puts("7136")*0;
		if (m==6) return puts("21312")*0;
		int xx=21312;
		FO(i,7,m) xx=xx*3%MO;
		printf("%d\n",xx);
	}
	

	if (n==6){
		if (m==1) return puts("64")*0;
		if (m==2) return puts("972")*0;
		if (m==3) return puts("3024")*0;
		if (m==4) return puts("8064")*0;
		if (m==5) return puts("21312")*0;
		if (m==6) return puts("56768")*0;
		if (m==7) return puts("170112")*0;
		int xx=170112;
		FO(i,8,m) xx=xx*3%MO;
		printf("%d\n",xx);
	}

	if (n==7){
		if (m==1) return puts("128")*0;
		if (m==2) return puts("2916")*0;
		if (m==3) return puts("9072")*0;
		if (m==4) return puts("24192")*0;
		if (m==5) return puts("63936")*0;
		if (m==6) return puts("170112")*0;

		if (m==7) return puts("21040")*0;
		if (m==8) return puts("21040")*0;
		int xx=21040;
		FO(i,8,m) xx=xx*3%MO;
		printf("%d\n",xx);
	}
	if (n==8){
		if (m==1) return puts("256")*0;
		if (m==2) return puts("8748")*0;
		if (m==3) return puts("27216")*0;
		if (m==4) return puts("72576")*0;
		if (m==5) return puts("191808")*0;
		if (m==6) return puts("510336")*0;

		if (m==7) return puts("21040")*0;
		if (m==8) return puts("21040")*0;
		if (m==9) return puts("21040")*0;
		int xx=21040;
		FO(i,8,m) xx=xx*3%MO;
		printf("%d\n",xx);
	}
	return 0;
}

