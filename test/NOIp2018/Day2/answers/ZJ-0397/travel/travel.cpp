#include <bits/stdc++.h>
using namespace std;

inline int read(){ static int x; scanf("%d",&x); return x; }
#define FO(i,a,b) for (int i=a;i<=b;++i)
#define FD(i,a,b) for (int i=a;i>=b;--i)

#define N 100005
int n,m,x,y,found,ok[N],head[N],fall[N],nxt[N],ff[N],GLB,GLB1,cir[N],cnt;
vector<int> V[5005];

inline void ae(int x,int y){ ++cnt;nxt[cnt]=head[x];fall[cnt]=y;head[x]=cnt; }

void dfs(int x,int fa){
	printf("%d ",x);
	ok[x]=1;int xx=V[x].size();
	for (int i=0;i<xx;++i){
		int to=V[x][i];
		if (!ok[to]) dfs(to,x);
	}
}
stack<int> S;
void dfs1(int x,int fa){
	ok[x]=1;S.push(x);
	int xx=V[x].size();
	for (int i=0;i<xx;++i){
		int to=V[x][i];
		if (ok[to]&&to!=fa&&!found){
			found=1;
			int xxx=x;
			while (S.top()!=to){
				xxx=S.top();cir[xxx]=1;S.pop();
			}
			S.pop();cir[to]=1;
		}
		if (!ok[to]) dfs1(to,x);
	}
}
void dfs2(int x){
	printf("%d ",x);
	int xx=V[x].size();
	ok[x]=1;int dh1=-1,dh2=-1;
	if (cir[x]&&GLB==-1&&GLB1==-1)for (int i=0;i<xx;++i){
		int to=V[x][i];
		if (!ok[to]&&cir[to]){
			if (dh1==-1) dh1=to;else dh2=to;
		}
		if (dh1!=-1&&dh2!=-1){ GLB=dh1;GLB1=dh2;break;}
	}
	for (int i=0;i<xx;++i){
		int to=V[x][i];
		if (cir[x]&&cir[to]){
			if (GLB!=-1&&!ok[GLB]&&GLB<to) dfs2(GLB);
			if (GLB1!=-1&&!ok[GLB1]&&GLB1<to) dfs2(GLB1);
		}
		if (!ok[to]) dfs2(to);
	}
//	if (dh1!=-1&&dh2!=-1){
//		GLB=dh2;dfs2(dh1);
//	}else if (dh1!=-1){
//		if (GLB!=-1&&GLB<dh1) dh1=GLB,GLB=-1;
//		dfs2(dh1);
//	}
}

int main(void){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	FO(i,1,m){ x=read();y=read();V[x].push_back(y);V[y].push_back(x); }
	FO(i,1,n) sort(V[i].begin(),V[i].end());
	if (m==n-1){
		dfs(1,0);return 0;
	}else{
		dfs1(1,0);
		FO(i,1,n) ok[i]=0;
		GLB=GLB1=-1;dfs2(1);
	}
	return 0;
}

