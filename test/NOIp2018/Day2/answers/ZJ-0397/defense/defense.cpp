#include <bits/stdc++.h>
using namespace std;

inline int read(){ static int x; scanf("%d",&x); return x; }
#define FO(i,a,b) for (int i=a;i<=b;++i)
#define FD(i,a,b) for (int i=a;i>=b;--i)

typedef long long LL;
#define N 400005
LL f[2][N],p[N];
int n,m,x,y,aa,bb,xx,yy,nxt[N],head[N],fall[N],cnt;
inline void ae(int x,int y){ ++cnt;nxt[cnt]=head[x];fall[cnt]=y;head[x]=cnt; }

char str[1005];

void dfs(int x,int fa){
	LL ans1,ans2;ans1=ans2=0;
	for (int i=head[x];i;i=nxt[i]){
		int to=fall[i];
		if (to==fa) continue;
		dfs(to,x);ans1+=f[1][to];ans2+=min(f[0][to],f[1][to]);
	}
	f[0][x]=ans1;
	f[1][x]=ans2+p[x];
	if (x==aa) f[1^xx][x]=1e17;
	if (x==bb) f[1^yy][x]=1e17;
}

int main(void){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();
	scanf("%s",str);
	FO(i,1,n) p[i]=read();p[0]=p[n+1]=1e14;
	FO(i,1,n-1) { x=read();y=read();ae(x,y);ae(y,x); }
	if ((str[0]=='A')&&n>2000){
		LL odd,even;odd=even=0;
		FO(i,1,m){
			if (i&1) odd+=p[i];else even+=p[i];
		}
		FO(i,1,m){
			xx=read();aa=read();
			yy=read();bb=read();
			if (aa==0&&bb==0&&abs(xx-yy)==1){
				puts("-1");continue;
			}
			if (aa>bb) swap(aa,bb),swap(xx,yy);
			if (xx%2==yy%2){
				if (aa==0&&bb==0){
					if (xx%2) printf("%lld\n",even);
					else printf("%lld\n",odd);
				}else if (aa==0){
					if (xx%2) printf("%lld\n",even+p[yy]);
					else printf("%lld\n",odd+p[yy]);
				}else{
					if (xx%2) printf("%lld\n",odd);
					else printf("%lld\n",even);
				}
			}else{
				if (aa==0&&bb==0){
					LL ans=1e17;
					if (yy%2) ans=min(ans,odd-p[yy]+min(p[yy-1],p[yy+1])),ans=min(ans,even-p[xx]+min(p[xx-1],p[xx+1]));
					else ans=min(ans,even-p[yy]+min(p[yy-1],p[yy+1])),ans=min(ans,odd-p[xx]+min(p[xx-1],p[xx+1]));
					printf("%lld\n",ans);
				}else if (aa==0){
					if (yy%2) printf("%lld\n",odd);
					else printf("%lld\n",even);
				}else{
					LL ans=1e15;
					if (yy%2) ans=min(ans,even+p[yy]),ans=min(ans,odd+p[xx]);
					else ans=min(ans,odd+p[yy]),ans=min(ans,even+p[xx]);
					printf("%lld\n",ans);
				}
			}
		}
		return 0;
	}
	FO(i,1,m){
		aa=read();xx=read();
		bb=read();yy=read();
		dfs(1,0);
		printf("%lld\n",min(f[0][1],f[1][1])>1e15?-1:min(f[0][1],f[1][1]));
	}
	return 0;
}

