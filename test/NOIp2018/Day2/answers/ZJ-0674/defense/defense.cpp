#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#define FOR(i,a,b) for(int i=(a),i##_END_=(b);i<=i##_END_;i++)
#define ROF(i,a,b) for(int i=(a),i##_END_=(b);i>=i##_END_;i--)
template<typename T>void Rd(T &x) {
	static char c;x=0;
	while((c=getchar())<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while((c=getchar())>47);
}
#define N 100005
#define S 18
int A[N];
int head[N],Nxt[N<<1],To[N<<1],cntE=-1;
inline void add_edge(const int from,const int to) {
	Nxt[++cntE]=head[from];
	To[cntE]=to;
	head[from]=cntE;
}
int dp[N][2];
int dep[N],fa[N][S];
void Build(int x,int f) {
	int y;
	fa[x][0]=f;
	FOR(i,1,S-1)fa[x][i]=fa[fa[x][i-1]][i-1];
	dep[x]=dep[f]+1;
	for(int i=head[x]; ~i; i=Nxt[i]) {
		if((y=To[i])!=f) {
			Build(y,x);
			dp[x][0]+=dp[y][1];
			dp[x][1]+=std::min(dp[y][0],dp[y][1]);
		}
	}
	dp[x][1]+=A[x];
}
int ans[N][2];
int a,b,x,y,lca;
int LCA(int x,int y) {
	if(dep[x]<dep[y])x^=y^=x^=y;
	int stp=dep[x]-dep[y];
	FOR(i,0,S-1)if(stp&(1<<i))x=fa[x][i];
	if(x==y)return x;
	ROF(i,S-1,0)if(fa[x][i]!=fa[y][i])
		x=fa[x][i],y=fa[y][i];
	return fa[x][0];
}
void dfs(int now,int f) {
	if(a==now) {
		ans[a][x]=dp[a][x];
		ans[a][!x]=0x3f3f3f3f;
		return;
	}
	int nxt;
	if(b==now) {
		if(lca==b) {
			for(int i=head[now]; ~i; i=Nxt[i])if((nxt=To[i])!=f) {
				dfs(nxt,now);
				ans[now][0]+=ans[nxt][1];
				ans[now][1]+=std::min(ans[nxt][0],ans[nxt][1]);
			}
			ans[b][1]+=A[now];
		} else ans[b][y]=dp[b][y];
		ans[b][!y]=0x3f3f3f3f;
		return;
	}
	if(dep[now]>=a&&dep[now]>=b&&LCA(a,now)!=a&&LCA(b,now)!=b) {
		ans[now][0]=dp[now][0];
		ans[now][1]=dp[now][1];
		return;
	}
	for(int i=head[now]; ~i; i=Nxt[i])if((nxt=To[i])!=f) {
		dfs(nxt,now);
		ans[now][0]+=ans[nxt][1];
		ans[now][1]+=std::min(ans[nxt][0],ans[nxt][1]);
	}
	ans[now][1]+=A[now];
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof head);
	char op[5];
	int n,m;
	Rd(n),Rd(m),scanf("%s",op);
	FOR(i,1,n)Rd(A[i]);
	FOR(i,1,n-1) {
		Rd(x),Rd(y);
		add_edge(x,y);
		add_edge(y,x);
	}
	Build(1,0);
	FOR(i,1,m) {
		Rd(a),Rd(x),Rd(b),Rd(y);
		if((fa[a][0]==b||fa[b][0]==a)&&!x&&!y) {
			puts("-1");
			continue;
		}
		if(dep[a]<dep[b]) {
			a^=b^=a^=b;
			x^=y^=x^=y;
		}
		lca=LCA(a,b);
		memset(ans,0,sizeof ans);
		dfs(1,0);
		printf("%d\n",std::min(ans[1][0],ans[1][1]));
	}
	return 0;
}

