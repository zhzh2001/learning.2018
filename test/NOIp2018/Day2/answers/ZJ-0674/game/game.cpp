#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#define FOR(i,a,b) for(int i=(a),i##_END_=(b);i<=i##_END_;i++)
#define ROF(i,a,b) for(int i=(a),i##_END_=(b);i>=i##_END_;i--)
template<typename T,typename _>inline void Max(T &x,_ y) {x=x>y?x:y;}
template<typename T,typename _>inline void Min(T &x,_ y) {x=x<y?x:y;}
template<typename T>void Rd(T &x) {
	static char c;x=0;
	while((c=getchar())<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while((c=getchar())>47);
}
#define P 1000000007
int p2[1000005];
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	p2[0]=1;
	for(int i=1;i<=1000000;i++)p2[i]=p2[i-1]*2%P;
	int n,m;
	Rd(n),Rd(m);
	if(n==1)printf("%d\n",p2[m]);
	else if(m==1)printf("%d\n",p2[n]);
	else if(n==2&&m==2)puts("12");
	else if(n==2&&m==3)puts("36");
	else if(n==3&&m==2)puts("36");
	else if(n==3&&m==3)puts("112");
	else if(n==5&&m==5)puts("7136");
	else {
		if(n<m)n^=m^=n^=m;
		int ans=1;
		FOR(i,1,n+m-1) {
			int y=std::min(i,n+m-i);
			int x=(m<y?m:y)+1;
			ans=1ll*ans*x%P;
		}
		printf("%d\n",ans);
	}
	return 0;
}

