#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#define FOR(i,a,b) for(int i=(a),i##_END_=(b);i<=i##_END_;i++)
#define ROF(i,a,b) for(int i=(a),i##_END_=(b);i>=i##_END_;i--)
template<typename T,typename _>inline void Max(T &x,_ y) {x=x>y?x:y;}
template<typename T,typename _>inline void Min(T &x,_ y) {x=x<y?x:y;}
template<typename T>void Rd(T &x) {
	static char c;x=0;
	while((c=getchar())<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while((c=getchar())>47);
}
#include<vector>
#include<queue>
#define N 5005
bool vis[N];
std::vector<int>edge[N];
namespace P1 {
	void dfs(int x) {
		int y;
		FOR(i,0,edge[x].size()-1) {
			if(!vis[y=edge[x][i]]) {
				printf(" %d",y);
				vis[y]=1;
				dfs(y);
			}
		}
	}
}
namespace P2 {
	int nowcnt,cnt;
	int A[N][N];
	int boomx,boomy;
	void dfs(int x) {
		int y;
		FOR(i,0,edge[x].size()-1) {
			if(!vis[y=edge[x][i]]&&
			!(x==boomx&&y==boomy)&&
			!(y==boomx&&x==boomy)) {
				vis[y]=1;
				A[nowcnt][++cnt]=y;
				dfs(y);
			}
		}
	}
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n,m,x,y;
	Rd(n),Rd(m);
	FOR(i,1,m) {
		Rd(x),Rd(y);
		edge[x].push_back(y);
		edge[y].push_back(x);
	}
	FOR(i,1,n)sort(edge[i].begin(),edge[i].end());
	putchar(49);
	vis[1]=1;
	if(m==n-1)P1::dfs(1);
	else {
		using namespace P2;
		FOR(x,1,n) {
			boomx=x;
			FOR(i,0,edge[x].size()-1) {
				boomy=edge[x][i];
				nowcnt++;
				cnt=0;
				memset(vis,0,sizeof vis);
				vis[1]=1;
				dfs(1);
				if(cnt<n-1)nowcnt--;
			}
		}
		int pos=1;
		FOR(i,2,nowcnt) {
			FOR(j,1,n-1)if(A[i][j]<A[pos][j]) {
				pos=i;
				break;
			} else if(A[i][j]>A[pos][j])break;
		}
		FOR(i,1,n-1)printf(" %d",A[pos][i]);
	}
	putchar(10);
	return 0;
}

