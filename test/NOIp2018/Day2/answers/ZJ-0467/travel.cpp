#include<cstdio>
#include<algorithm>
int read(){
	int x=0,k=0;char c=getchar();
	while ((c>'9'||c<'0')&&c!='-') c=getchar();
	if (c=='-') k=1,c=getchar();
	while (c<='9'&&c>='0') x=(x<<1)+(x<<3)+(c^48),c=getchar();
	return x;
}
#define read read()
int n,m;
int c[5005];
int head[5005],tot;
int ans[5005];
struct node{
	int to,next;
}l[10005];
struct node1{
	int x,y;
}QWQ[10005];
void add(int x,int y){
	l[++tot].next=head[x];
	l[tot].to=y;
	head[x]=tot;
}
void swap(int &a,int &b){
	int k=a;
	a=b;
	b=k;
}
int vis[5005];
int b[5005];
int k=0;
int xx,yy;
int cmp(node1 a,node1 b){return (a.x==b.x)?(a.y>b.y):(a.x<b.x);}
void dfs_1(int x,int f){
	printf("%d ",x);
	for (int i=head[x];i;i=l[i].next){
		if (l[i].to!=f) dfs_1(l[i].to,x);
	}
}
void pd(){
	if (ans[1]==0){
		for (int i=1;i<=n;i++)
			ans[i]=b[i];
		return;
	}
	for (int i=1;i<=n;i++)
		if (ans[i]<b[i]) return;else
			if (ans[i]>b[i]) break;
	for (int i=1;i<=n;i++)
		ans[i]=b[i];
}
void dfs_2(int x,int f){
	b[++tot]=x;
	if (tot==n){
		pd();
		return;
	}
	for (int i=head[x];i;i=l[i].next){
		if ((x==xx&&l[i].to==yy)||(x==yy&&l[i].to==xx))
			continue;
		if (l[i].to!=f) dfs_2(l[i].to,x);
		if (tot==n) return;
	}
}
void dfs(int x,int f){
	if (k) return;
	vis[x]=1;
	for (int i=head[x];i;i=l[i].next){
		if (l[i].to==f) continue;
		if (vis[l[i].to]){
			k=1;
			xx=x;
			yy=l[i].to;
			b[x]=1;
			c[++tot]=yy;
			return;
		}
		dfs(l[i].to,x);
		if (k){
			if (c[1]!=c[tot]||tot==1) c[++tot]=l[i].to;
			return;
		}
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read;m=read;
	for (int i=1;i<=m;i++){
		int x=read,y=read;
		QWQ[++tot].x=x,QWQ[tot].y=y;
		QWQ[++tot].x=y,QWQ[tot].y=x;
	}
	std::sort(QWQ+1,QWQ+1+tot,cmp);
	int p=tot;
	tot=0;
	for (int i=1;i<=p;i++)
		add(QWQ[i].x,QWQ[i].y);
	if (m==n-1)
		dfs_1(1,0);
	else{
		tot=0;
		dfs(1,0);
		for (int i=1;i<tot;i++){
			xx=c[i];yy=c[i+1];
			tot=0;
			dfs_2(1,0);
		}
		for (int i=1;i<=n;i++)
			printf("%d ",ans[i]);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
