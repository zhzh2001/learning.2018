#include<cstdio>
typedef long long ll;
int a[100][100];
int n,m;
ll b[10000005];
int tot;
const ll mod=1e9+7;
void dfs1(int x,int y,ll num){
	if (x==n&&y==m){
		b[++tot]=num;
		return;
	}
	if (x<n)
		dfs1(x+1,y,num<<1|a[x][y]);
	if (y<m)
		dfs1(x,y+1,num<<1|a[x][y]);
}
ll ans;
void pd(){
	tot=0;
	dfs1(1,1,0);
	for (int i=1;i<tot;i++)	if (b[i]<b[i+1]) return;
	ans++;
}
void dfs(int x,int y){
	if (y>m){
		dfs(x+1,1);
		return;
	}
	if (x>n){
		pd();
		return;
	}
	a[x][y]=1;
	dfs(x,y+1);
	a[x][y]=0;
	dfs(x,y+1);
}
void work2(){
	m--;
	ans=4;
	for (int i=1;i<=m;i++)
		ans=(ans*3)%mod;
}
void work1(){
	ans=1;
	for (int i=1;i<=m;i++)
		ans=(ans+ans)%mod;
}
void work3(){
	ans=112;
	if (m==1) ans=8;
	if (m==2) ans=36;
	m-=3;
	for (int i=1;i<=m;i++)
		ans=(ans*3)%mod;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) {n^=m;m^=n;n^=m;}
	if (n==2) work2();else
	if (n==1) work1();else
	if (n==3) work3();else
	dfs(1,1);
	printf("%lld",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
