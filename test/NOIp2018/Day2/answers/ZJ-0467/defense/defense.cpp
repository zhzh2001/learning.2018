#include<cstdio>
#define max(a,b) (((a)>(b))?(a):(b))
#define min(a,b) (((a)<(b))?(a):(b))
typedef long long ll;
const ll inf=1000000000000;
char s[5];
int n,m;
ll c[100005];
int head[100005],tot;
ll f[100005][2];
ll ff[1000005][2];
struct node{
	int next,to;
}l[200005];
int read(){
	int x=0,k=0;char c=getchar();
	while ((c>'9'||c<'0')&&c!='-') c=getchar();
	if (c=='-') k=1,c=getchar();
	while (c<='9'&&c>='0') x=(x<<1)+(x<<3)+(c^48),c=getchar();
	return x;
}
#define read read()
void add(int x,int y){
	l[++tot].next=head[x];
	l[tot].to=y;
	head[x]=tot;
}
int x,xx,y,yy;
int k;
void dfs(int u,int fa){
	if (u==x||u==y) k++;
	f[u][0]=0;
	f[u][1]=c[u];
	for (int i=head[u];i;i=l[i].next){
		int v=l[i].to;
		if (v==fa) continue;
		if (k<2) dfs(v,u);else{
			f[v][0]=ff[v][0];
			f[v][1]=ff[v][1];
		}
		f[u][0]+=f[v][1];
		f[u][1]+=min(f[v][1],f[v][0]);
	}
	if (u==x) f[u][xx^1]=inf;
	if (u==y) f[u][yy^1]=inf;
	return;
}
void build(int u,int fa){
	ff[u][0]=0;
	ff[u][1]=c[u];
	for (int i=head[u];i;i=l[i].next){
		int v=l[i].to;
		if (v==fa) continue;
		build(v,u);
		ff[u][0]+=ff[v][1];
		ff[u][1]+=min(ff[v][1],ff[v][0]);
	}
	return;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read,m=read;
	scanf("%s",s+1);
	for (int i=1;i<=n;i++)
		c[i]=read;
	for (int i=1;i<n;i++){
		int x,y;
		x=read;y=read;
		add(x,y);
		add(y,x);
	}
	build(1,0);
	for (int i=1;i<=m;i++){
		x=read,xx=read;y=read,yy=read;
		k=0;
		if (i==1) k=-10;
		dfs(1,0);
		ll ans=min(f[1][0],f[1][1]);
		if (ans>=inf) puts("-1");else printf("%lld\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
