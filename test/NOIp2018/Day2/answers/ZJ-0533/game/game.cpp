//$1 orz
#ifndef _GLIBCXX_NO_ASSERT
#include <cassert>
#endif
#include <cctype>
#include <cerrno>
#include <cfloat>
#include <ciso646>
#include <climits>
#include <clocale>
#include <cmath>
#include <csetjmp>
#include <csignal>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#if __cplusplus >= 201103L
#include <ccomplex>
#include <cfenv>
#include <cinttypes>
#include <cstdalign>
#include <cstdbool>
#include <cstdint>
#include <ctgmath>
#include <cwchar>
#include <cwctype>
#endif

// C++
#include <algorithm>
#include <bitset>
#include <complex>
#include <deque>
#include <exception>
#include <fstream>
#include <functional>
#include <iomanip>
#include <ios>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <iterator>
#include <limits>
#include <list>
#include <locale>
#include <map>
#include <memory>
#include <new>
#include <numeric>
#include <ostream>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <streambuf>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>
#include <vector>

#if __cplusplus >= 201103L
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <forward_list>
#include <future>
#include <initializer_list>
#include <mutex>
#include <random>
#include <ratio>
#include <regex>
#include <scoped_allocator>
#include <system_error>
#include <thread>
#include <tuple>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#endif
using namespace std;
const int MOD=1e9+7;
int has[256][256],a[9],b[9],l1[10],l2[10],flag,n,m,f[2][256],ans;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	for(int i=0;i<=255;i++)
	{
		for(int j=0;j<=255;j++)
		{
			for(int k=0;k<=7;k++) if(i&(1<<(7-k))) a[k]=1; else a[k]=0;
			for(int k=0;k<=7;k++) if(j&(1<<(7-k))) b[k]=1; else b[k]=0;
			//for(int k=0;k<=7;k++) printf("%d",a[k]); puts("");
			//for(int k=0;k<=7;k++) printf("%d",b[k]); puts(""); puts("");
			flag=1;
			for(int k1=0;k1<=7&&flag==1;k1++)
			{
				memset(l1,0,sizeof(l1));
				memset(l2,0,sizeof(l2));
				for(int x=0;x<=k1;x++) l1[x]=a[x];
				for(int x=k1;x<=7;x++) l1[x+1]=b[x];
				for(int k2=0;k2<k1;k2++)
				{
					for(int x=0;x<=k2;x++) l2[x]=a[x];
					for(int x=k2;x<=7;x++) l2[x+1]=b[x];
					//if(i==2&&j==2) {for(int k=0;k<=8;k++) printf("%d",l1[k]); puts("");}
					//if(i==2&&j==2) {for(int k=0;k<=8;k++) printf("%d",l2[k]); puts("");}
					for(int x=0;x<=8;x++) if(l1[x]<l2[x]) {flag=0; break;}
				}
			}
			has[i][j]=flag;
		}
	}
	for(int i=0;i<8;i++) {for(int j=0;j<8;j++) printf("%d ",has[i][j]); puts("");}
	scanf("%d%d",&n,&m);
	for(int i=0;i<(1<<n);i++) f[0][i]=1;
	for(int i=2;i<=m;i++)
	{
		for(int j=0;j<(1<<n);j++)
		for(int k=0;k<(1<<n);k++) if(has[j][k]) f[1][j]=(f[1][j]+f[0][k])%MOD;
		swap(f[0],f[1]);
		memset(f[1],0,sizeof(f[1]));
	}
	for(int i=0;i<(1<<n);i++) ans+=f[0][i];
	printf("%d",ans);
	return 0;
}
