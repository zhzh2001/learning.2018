#ifndef _GLIBCXX_NO_ASSERT
#include <cassert>
#endif
#include <cctype>
#include <cerrno>
#include <cfloat>
#include <ciso646>
#include <climits>
#include <clocale>
#include <cmath>
#include <csetjmp>
#include <csignal>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#if __cplusplus >= 201103L
#include <ccomplex>
#include <cfenv>
#include <cinttypes>
#include <cstdalign>
#include <cstdbool>
#include <cstdint>
#include <ctgmath>
#include <cwchar>
#include <cwctype>
#endif

// C++
#include <algorithm>
#include <bitset>
#include <complex>
#include <deque>
#include <exception>
#include <fstream>
#include <functional>
#include <iomanip>
#include <ios>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <iterator>
#include <limits>
#include <list>
#include <locale>
#include <map>
#include <memory>
#include <new>
#include <numeric>
#include <ostream>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <streambuf>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>
#include <vector>

#if __cplusplus >= 201103L
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <forward_list>
#include <future>
#include <initializer_list>
#include <mutex>
#include <random>
#include <ratio>
#include <regex>
#include <scoped_allocator>
#include <system_error>
#include <thread>
#include <tuple>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#endif
using namespace std;
int n,m;
vector<int>g[5009];
int first[5009],vis[5009],ans_now[5009],ans[5009],Num,Nxt,s,e,f,fa[5009];
priority_queue<int,vector<int>,greater<int> > q[5009],q_now[5009];
pair<int,int> canc;
void AddEdge(int u,int v)
{
	g[u].push_back(v);
	q[u].push(v);
}
void dfs1(int now,int start,int fa)
{
	Num++;
	ans_now[Num]=now;
	while(!q[now].empty())
	{
		Nxt=q[now].top();
		q[now].pop();
		if(Nxt==fa) continue;
		dfs1(Nxt,start,now);
	}
	return;
}
void dfs2(int now,int start,int Num)
{
	//printf("%d ",Num);
	ans_now[Num]=now;
	vis[now]=1;
	//printf("%d ",now);
	if(Num==n)
	{
		//for(int i=1;i<=n;i++) printf("%d ",ans_now[i]); puts("");
		f=0;
		for(int i=1;i<=n;i++) if(ans_now[i]<ans[i]) {f=1; break;} else if(ans_now[i]>ans[i]) break;
		if(f) for(int i=1;i<=n;i++) ans[i]=ans_now[i];
	}
	q_now[now]=q[now];
	while(!q_now[now].empty())
	{
		Nxt=q_now[now].top();
		q_now[now].pop();
		if((Nxt==canc.first&&now==canc.second)||(Nxt==canc.second&&now==canc.first)) continue;
		//if(Nxt==fa) continue;
		if(!vis[Nxt]) dfs2(Nxt,start,Num+1);
		//else printf("%d ",Nxt);
	}
	ans_now[Num]=INT_MAX;
	vis[now]=0;
	return;
	/*Num++;
	//printf("%d ",Num);
	ans_now[Num]=now;
	vis[now]=1;
	if(Num==n)
	{
		//for(int i=1;i<=n;i++) printf("%d ",ans_now[i]); puts("");
		f=0;
		for(int i=1;i<=n;i++) if(ans_now[i]<ans[i]) {f=1; break;} else if(ans_now[i]>ans[i]) break;
		if(f) for(int i=1;i<=n;i++) ans[i]=ans_now[i];
		return;
	}
	q_now[now]=q[now];
	while(!q_now[now].empty())
	{
		Nxt=q_now[now].top();
		q_now[now].pop();
		if((Nxt==canc.first&&now==canc.second)||(Nxt==canc.second&&now==canc.first)) continue;
		if(vis[Nxt]) continue;
		dfs2(Nxt,start);
	}
	vis[now]=0;
	return;*/
}
void solve_tree()
{
	//for(int i=1;i<=n;i++) ans[i]=INT_MAX;//memset(ans,63,sizeof(ans));
	dfs1(1,1,0);
	for(int i=1; i<=Num; i++) printf("%d ",ans_now[i]);
	puts("");
}
void solve_graph()
{
	memset(ans,63,sizeof(ans));
	/*for(int i=1;i<=n;i++)
	{
		for(int j=0;j<g[i].size();j++)
		{
			canc.first=i,canc.second=g[i][j];
			Num=0;
			memset(ans_now,63,sizeof(ans_now));
			dfs2(1,1);
		}
	}*/
	dfs2(1,1,1);
	for(int i=1; i<=n; i++) printf("%d ",ans[i]);
	puts("");
}
int main()
{
	freopen("travel.in","r+",stdin);
	freopen("travel.out","w+",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1; i<=m; i++) scanf("%d%d",&s,&e),AddEdge(s,e),AddEdge(e,s);
	if(m==n-1) solve_tree();
	else solve_graph();
	return 0;
}
