//$1 orz
#ifndef _GLIBCXX_NO_ASSERT
#include <cassert>
#endif
#include <cctype>
#include <cerrno>
#include <cfloat>
#include <ciso646>
#include <climits>
#include <clocale>
#include <cmath>
#include <csetjmp>
#include <csignal>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#if __cplusplus >= 201103L
#include <ccomplex>
#include <cfenv>
#include <cinttypes>
#include <cstdalign>
#include <cstdbool>
#include <cstdint>
#include <ctgmath>
#include <cwchar>
#include <cwctype>
#endif

// C++
#include <algorithm>
#include <bitset>
#include <complex>
#include <deque>
#include <exception>
#include <fstream>
#include <functional>
#include <iomanip>
#include <ios>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <iterator>
#include <limits>
#include <list>
#include <locale>
#include <map>
#include <memory>
#include <new>
#include <numeric>
#include <ostream>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <streambuf>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>
#include <vector>

#if __cplusplus >= 201103L
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <forward_list>
#include <future>
#include <initializer_list>
#include <mutex>
#include <random>
#include <ratio>
#include <regex>
#include <scoped_allocator>
#include <system_error>
#include <thread>
#include <tuple>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#endif
using namespace std;
#define int long long
int n,x,y,a,b,m,p[100009],f[100009][2];
string s;
signed main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	cin>>s;
	for(int i=1;i<=n;i++) scanf("%d",&p[i]);
	for(int i=1;i<n;i++) scanf("%d%d",&x,&y);
	while(m--)
	{
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if((a==b+1||b==a+1)&&(x==0&&y==0)) puts("-1");
		f[1][0]=0,f[1][1]=p[1];
		for(int i=2;i<=n;i++)
		{
			f[i][0]=f[i-1][1];
			f[i][1]=min(f[i-1][1],f[i-1][0])+p[i];
			if(i==a) f[i][x^1]=INT_MAX;
			else if(i==b) f[i][y^1]=INT_MAX;
		}
		int ans;
		if(a!=n&&b!=n) ans=min(f[n][0],f[n][1]);
		else if(a==n) ans=min(f[n-1][0],f[n-1][1])+(x^1)*p[n];
		else if(b==n) ans=min(f[n-1][0],f[n-1][1])+(y^1)*p[n];
		if(ans>=INT_MAX) puts("-1");
		else printf("%d\n",ans);
	}
	return 0;
}
