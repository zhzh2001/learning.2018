#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>

using namespace std;

int rd() {
	int x = 0, f = 1; char c = getchar();
	while (c > '9' || c < '0') f = c == '-' ? -1 : 1, c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x * f;
}

const int inf = 0x7f7f7f7f;
const int N = 1e5 + 10;
int p[N], g[N], u[N], v[N], f[N][2];
int n, m;
char op[10];

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	n = rd(), m = rd(); scanf("%s", op);
	for (int i = 1; i <= n; i ++) p[i] = rd(), g[i] = -1;
	for (int i = 1; i < n; i ++) u[i] = rd(), v[i] = rd();
	if (op[0] == 'A') {
		memset(f, 0x7f, sizeof f);
		f[0][0] = f[0][1] = 0;
		for (int i = 1; i <= m; i ++) {
			int a = rd(), x = rd(), b = rd(), y = rd();
			if (abs(a - b) == 1 && !x && !y) {
				puts("-1");
				continue;
			}
			g[a] = x, g[b] = y;
			for (int i = 1; i <= n; i ++) {
				if (g[i] == 1) {
					f[i][1] = min(f[i - 1][0] + p[i], f[i - 1][1] + p[i]);
				}
				else if (g[i] == 0) {
					f[i][0] = f[i - 1][1];
				}
				else {
					f[i][0] = f[i - 1][1];
					f[i][1] = min(f[i - 1][0] + p[i], f[i - 1][1] + p[i]);
				}
			}
			g[a] = -1, g[b] = -1;
		}
		printf("%d\n", min(f[n][0], f[n][1]));
	}
	return 0;
}
