#include <cstdio>
#include <vector>
#include <queue>
#include <algorithm>

using namespace std;

int rd() {
	int x = 0, f = 1; char c = getchar();
	while (c > '9' || c < '0') f = c == '-' ? -1 : 1, c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x * f;
}

void wt(int x) {
	if (x > 9) wt(x / 10);
	putchar(x % 10 + 48);
}

const int N = 5010;
vector <int> e[N], ee;
queue <int> q;
int n, m, tot, ans[N], app[N];
bool vis[N], inq[N];

void bfs() {
	q.push(1);
	inq[1] = vis[1] = 1;
	while (!q.empty()) {
		int u = q.front(); q.pop();
		inq[u] = 0;
		int ed = e[u].size();
		ans[++tot] = u;
		for (int i = 0; i < ed; i ++) {
			int v = e[u][i];
			if (vis[v]) continue;
			vis[v] = 1;
			if (!inq[v]) q.push(v), inq[v] = 1;
//			else 
		}
	}
}

void dfs(int u) {
	vis[u] = 1;
	ans[++tot] = u;
	int ed = e[u].size();
	for (int i = 0; i < ed; i ++) {
		int v = e[u][i];
		if (!vis[v]) dfs(v);
	}
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	n = rd(), m = rd();
	for (int i = 1; i <= m; i ++) { 
		int u = rd(), v = rd();
		e[u].push_back(v);
		e[v].push_back(u);
	}
	for (int i = 1; i <= n; i ++) sort(e[i].begin(), e[i].end());
	if (m == n - 1) dfs(1);
	else bfs();
	for (int i = 1; i <= n; i ++) wt(ans[i]), putchar(32);
	putchar(10);
	return 0;
}
