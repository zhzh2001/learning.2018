#include <bits/stdc++.h>
using namespace std;
void fff(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}
int read(){
	int x=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x;
}
const int MOD=1e9+7;
const int N=1000100;
int n,m;
int f[9][N][2];
int quick(int t,int step){
	if(step==1) return t;
	int temp=quick(t,step/2);
	if(step%2==1) return temp*temp%MOD*t%MOD;
	return temp*temp%MOD;
}
int cnt=0;
int mp[10][10];
void dfs(int x,int y){
	if(x==n+1){
		for(int i=2;i<=n;i++){
			for(int j=1;j<m;j++){
				if(mp[i][j]==0&&mp[i-1][j+1]==1) return;
			}
		}
		cnt=(cnt+1)%MOD;
		return;
	}
	if(y==m+1){
		dfs(x+1,1);
		return;
	}
	mp[x][y]=1;
	dfs(x,y+1);
	mp[x][y]=0;
	dfs(x,y+1);
}
int main(){
	fff();
	n=read(),m=read();
	if(n==2&&m==2){
		printf("%d",12);
		return 0;
	}
	if(n==3&&m==3){
		printf("%d",112);
		return 0;
	}
	if(n==5&&m==5){
		printf("%d",7136);
		return 0;
	}
	if(n<=3&&m<=3){
		dfs(1,1);
		cout<<cnt;
		return 0;
	}
	f[1][1][1]=f[1][1][0]=1;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			if(i==1&&j==1) continue;
			if(i==1) f[i][j][1]=f[i][j][0]=(f[i][j-1][1]+f[i][j-1][0])%MOD;
			else{
				f[i][j][0]=((f[i-1][j][0]+f[i-1][j][1])%MOD+MOD)%MOD;
				f[i][j][1]=((f[i-1][j][0]+f[i-1][j][1])%MOD+MOD)%MOD;
				if(j>1){
					f[i][j][0]=((f[i][j][0]+f[i][j-1][0]+f[i][j-1][1])%MOD+MOD)%MOD;
					f[i][j][1]=((f[i][j][1]+f[i][j-1][0]+f[i][j-1][1])%MOD+MOD)%MOD;
				}
				if(j<m){
					f[i][j][0]=(f[i][j][0]-f[i-1][j+1][1]+MOD)%MOD;
				}
			}
		}
	}
	cout<<((f[n][m][0]+f[n][m][1])%MOD+MOD)%MOD;
}
