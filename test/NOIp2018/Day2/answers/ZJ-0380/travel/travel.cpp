#include <bits/stdc++.h>
using namespace std;
void fff(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
}
int read(){
	int x=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x;
}
const int N=5050;
vector<int> G[N];
bool flag=false;
bool rround[N];
int n,m;
void init(){
	n=read(),m=read();
	int u,v;
	for(int i=1;i<=m;i++){
		u=read(),v=read();
		G[u].push_back(v);
		G[v].push_back(u);
	}
	for(int i=1;i<=n;i++){
		sort(G[i].begin(),G[i].end());
	}
}
bool visited[N];
int sta[N],top=0;
void dfs(int u){
	visited[u]=true;
	printf("%d ",u);
	int siz=G[u].size(),v;
	for(int i=0;i<siz;i++){
		v=G[u][i];
		if(!visited[v]){
			dfs(v);
		}
	}
}
void dfs1(int u,int minn){
	visited[u]=true;
	printf("%d ",u);
	int siz=G[u].size(),v;
	for(int i=0;i<siz;i++){
		v=G[u][i];
		if(rround[v]&&v>minn) continue;
		if(!visited[v]){
			int temp=G[u][i+1];
			dfs1(v,v==minn?(i==siz-1?9999999:temp):minn);
			if(minn==v) minn=temp;
		}
	}
}

void dfs2(int u,int fa){
	visited[u]=true;
	sta[++top]=u;
	int siz=G[u].size(),v;
	for(int i=0;i<siz;i++){
		v=G[u][i];
		if(flag) return;
		if(!visited[v]){
			dfs2(v,u);
			continue;
		}
		if(visited[v]&&v!=fa){
			int maxx=0,pos;
			for(int j=1;j<=top;j++){
				rround[sta[j]]=true;
			}
		}
	}
	top--;
}
int main(){
	fff();
	init();
	if(m==n-1){
		dfs(1);
	}else{
		dfs2(1,-1);
		memset(visited,false,sizeof(visited));
		int temp=G[1][0];
		dfs1(1,temp);
	}
}
