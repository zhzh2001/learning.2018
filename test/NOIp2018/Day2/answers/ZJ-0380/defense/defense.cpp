#include <bits/stdc++.h>
using namespace std;
void fff(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
}
int read(){
	int x=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x;
}
const int N=100010;
struct Edge{
	int nxt,to;
}edge[N<<1];
int head[N],tot=0;
void add(int u,int v){
	edge[++tot].nxt=head[u];
	edge[tot].to=v;
	head[u]=tot;
}
int a[N],f[N][2];
const int INF=0x3f3f3f;
int n,m;
char ch[3];
bool visited[N];
int A,B,X,Y;
int check(int u){
	if(u==A) return X;
	if(u==B) return Y;
	return -1;
}
bool flag=false;
void dfs(int u){
	visited[u]=true;
	for(int i=head[u];i;i=edge[i].nxt){
		int v=edge[i].to;
		if(!visited[v]){
			if(check(u)==0&&check(v)==0){
				flag=true;
				return;
			}
			if(flag) return;
			dfs(v);
			if(check(v)==-1){
				f[u][0]+=f[v][1];
				f[u][1]+=min(f[v][0],f[v][1]==INF?0:f[v][1]);
			}else if(check(v)==1){
				f[u][0]+=f[v][1]==INF?0:f[v][1];
				f[u][1]+=f[v][1]==INF?0:f[v][1];
			}else{
				f[u][0]=INF;
				f[u][1]+=min(f[v][0],f[v][1]);
			}
		}
	}
	if(check(u)==0)f[u][1]=INF;
	else f[u][1]+=a[u];
	
}
int main(){
	fff();
	n=read();
	m=read();
	cin>>ch;
	for(int i=1;i<=n;i++){
		a[i]=read();
	}
	for(int i=1;i<n;i++){
		int u=read(),v=read();
		add(u,v);
		add(v,u);
	}
	for(int i=1;i<=m;i++){
		A=read(),X=read(),B=read(),Y=read();
		flag=false;
		memset(f,0,sizeof(f));
		memset(visited,false,sizeof(visited));
		dfs(1);
		if(flag) printf("%d\n",-1);
		else printf("%d\n",min(f[1][0],f[1][1]));
	}
}
