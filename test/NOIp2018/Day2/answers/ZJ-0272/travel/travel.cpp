#include <bits/stdc++.h>
using namespace std;
const int maxn=5005;
int n,m,he_[maxn],ne_[maxn<<1],to_[maxn<<1],tot_,temp_[maxn][maxn];
int fa[maxn],c[maxn],tot,cid[maxn],minn[maxn];
bool vis[maxn];
int ng;
inline void add_edge(int u,int v)
{
    ne_[tot_]=he_[u];
    to_[tot_]=v;
    he_[u]=tot_++;
}
void dfs_tree(int u)
{
    int i,cnt=0;
    printf("%d ",u);
    for(i=he_[u];i!=-1;i=ne_[i])
        if(to_[i]!=fa[u])
            fa[to_[i]]=u,temp_[u][cnt++]=to_[i];
    if(!cnt) return;
    sort(temp_[u],temp_[u]+cnt);
    for(i=0;i<cnt;i++)
        dfs_tree(temp_[u][i]);
}
void find_cir(int u)
{
    int i,j;
    vis[u]=1;
    for(i=he_[u];i!=-1;i=ne_[i])
        if(to_[i]!=fa[u])
        {
            if(vis[to_[i]] && tot==0)
            {
                tot=0;
                for(j=u;j!=to_[i];j=fa[j])
                    cid[j]=tot,c[tot++]=j;
                cid[to_[i]]=tot,c[tot++]=to_[i];
            }
            else if(!vis[to_[i]])
                fa[to_[i]]=u,find_cir(to_[i]);
        }
}
void dfs_cir(int u,int pre)
{
    int i,cnt=0;
    minn[u]=0x7fffffff;
    if(vis[u]) return;
    printf("%d ",u); vis[u]=1;
    for(i=he_[u];i!=-1;i=ne_[i])
        if(to_[i]!=pre && !vis[to_[i]])
        {
            temp_[u][cnt++]=to_[i];
            if(cid[to_[i]]==-1)
                ng++;
        }
    sort(temp_[u],temp_[u]+cnt);
    if(!cnt) return;
    for(i=0;i<cnt;i++)
    {
        minn[u]=(i==cnt-1)?(pre==-1?0x7fffffff:minn[pre]):temp_[u][i+1];
        if(cid[temp_[u][i]]==-1)
            ng--,dfs_tree(temp_[u][i]);
        else if(cid[u]!=-1 && i==cnt-1 && pre!=-1 && minn[pre]<temp_[u][i] && (!vis[c[tot-2]] || !vis[c[0]]))
            return;
        else if(cid[u]!=-1 && i==cnt-1 && ng==0 && !vis[c[tot-2]] && (vis[c[0]] || c[0]>c[tot-2]))
            dfs_cir(c[tot-2],c[tot-1]);
        else if(cid[u]!=-1 && i==cnt-1 && ng==0 && !vis[c[0]] && (vis[c[tot-2]] || c[0]<c[tot-2]))
            dfs_cir(c[0],c[tot-1]);
        else dfs_cir(temp_[u][i],u);
    }
}
int main()
{
    int i,u,v;
    freopen("travel.in","r",stdin); freopen("travel.out","w",stdout);
    scanf("%d%d",&n,&m);
    tot_=0;
    for(i=1;i<=n;i++) he_[i]=-1,cid[i]=-1;
    for(i=0;i<m;i++)
    {
        scanf("%d%d",&u,&v);
        add_edge(u,v);
        add_edge(v,u);
    }
    if(m==n-1)
        fa[1]=-1,dfs_tree(1);
    else
    {
        fa[1]=-1,tot=0,find_cir(1);
        for(i=1;i<=n;i++) vis[i]=0;
        ng=0,dfs_cir(1,-1);
    }
    printf("\n");
    fclose(stdin); fclose(stdout);
    return 0;
}
