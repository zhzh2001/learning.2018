#include <bits/stdc++.h>
using namespace std;
const int maxn=100005;
int n,fa[maxn],dep[maxn];
long long f[maxn][2],val[maxn],g[maxn][2];
vector<int> G[maxn];
void dfs(int u)
{
    vector<int>::iterator it;
    f[u][0]=0,f[u][1]=val[u];
    for(it=G[u].begin();it!=G[u].end();++it)
        if(*it!=fa[u])
        {
            fa[*it]=u,dep[*it]=dep[u]+1,dfs(*it);
            f[u][0]+=f[*it][1];
            f[u][1]+=min(f[*it][0],f[*it][1]);
        }
}
inline void check_inf(long long &x) { x=min(x,0x1fffffffffffffffll); }
int main()
{
    int i,m,u,v,x,y;
    freopen("defense.in","r",stdin); freopen("defense.out","w",stdout);
    scanf("%d%d%*s",&n,&m);
    for(i=1;i<=n;i++) scanf("%lld",&val[i]);
    for(i=1;i<n;i++)
    {
        scanf("%d%d",&u,&v);
        G[u].push_back(v);
        G[v].push_back(u);
    }
    fa[1]=-1,dep[1]=0,dfs(1);
    for(i=0;i<m;i++)
    {
        scanf("%d%d%d%d",&u,&x,&v,&y);
        if(!x && !y && (fa[u]==v || fa[v]==u))
        {
            printf("-1\n");
            continue;
        }
        if(dep[u]<dep[v]) swap(u,v),swap(x,y);
        g[u][0]=f[u][0],g[u][1]=f[u][1],g[u][x^1]=0x1fffffffffffffffll;
        for(;dep[u]>dep[v];u=fa[u])
        {
            g[fa[u]][0]=f[fa[u]][0]-f[u][1]+g[u][1];
            check_inf(g[fa[u]][0]);
            g[fa[u]][1]=f[fa[u]][1]-min(f[u][0],f[u][1])+min(g[u][0],g[u][1]);
            check_inf(g[fa[u]][1]);
        }
        if(u!=v)
        {
            g[v][0]=f[v][0],g[v][1]=f[v][1],g[v][y^1]=0x1fffffffffffffffll;
            for(;fa[u]!=fa[v];u=fa[u],v=fa[v])
            {
                g[fa[u]][0]=f[fa[u]][0]-f[u][1]+g[u][1];
                check_inf(g[fa[u]][0]);
                g[fa[u]][1]=f[fa[u]][1]-min(f[u][0],f[u][1])+min(g[u][0],g[u][1]);
                check_inf(g[fa[u]][1]);
                g[fa[v]][0]=f[fa[v]][0]-f[v][1]+g[v][1];
                check_inf(g[fa[v]][0]);
                g[fa[v]][1]=f[fa[v]][1]-min(f[v][0],f[v][1])+min(g[v][0],g[v][1]);
                check_inf(g[fa[v]][1]);
            }
            g[fa[u]][0]=f[fa[u]][0]-f[u][1]-f[v][1]+g[u][1]+g[v][1];
            check_inf(g[fa[u]][0]);
            g[fa[u]][1]=f[fa[u]][1]-min(f[u][0],f[u][1])+min(g[u][0],g[u][1])-min(f[v][0],f[v][1])+min(g[v][0],g[v][1]);
            check_inf(g[fa[u]][1]);
            u=fa[u];
        }
        else g[v][y^1]=0x1fffffffffffffffll;
        for(;u!=1;u=fa[u])
        {
            g[fa[u]][0]=f[fa[u]][0]-f[u][1]+g[u][1];
            check_inf(g[fa[u]][0]);
            g[fa[u]][1]=f[fa[u]][1]-min(f[u][0],f[u][1])+min(g[u][0],g[u][1]);
            check_inf(g[fa[u]][1]);
        }
        printf("%lld\n",min(g[1][0],g[1][1]));
    }
    fclose(stdin); fclose(stdout);
    return 0;
}
