#include <bits/stdc++.h>
using namespace std;
const int maxm=1000005;
const int maxn=10;
const int kcz=1000000007;
int n,m,ans;
bool ma[maxn][maxm],pre[maxm+maxn],cur[maxm+maxn];
bool check(int x,int y)
{
    if(y>=m) return true;
    if(x>=n) return true;
    cur[x+y]=ma[x][y];
    if(x==n-1 && y==m-1)
    {
        int i;
        for(i=0;i<=x+y;i++)
            if(cur[i]<pre[i])
                return false;
        for(i=0;i<=x+y;i++)
            pre[i]=cur[i];
        return true;
    }
    if(!check(x,y+1)) return false;
    if(!check(x+1,y)) return false;
    return true;
}
void dfs(int x,int y)
{
    if(y>=m) y=0,x++;
    if(x>=n)
    {
        int i;
        for(i=0;i<=n+m-2;i++)
            pre[i]=0;
        if(check(0,0)) ans++;
        return;
    }
    ma[x][y]=0; dfs(x,y+1);
    ma[x][y]=1; dfs(x,y+1);
}
inline long long qpow(long long a,long long k)
{
    long long res=1;
    while(k)
    {
        if(k&1) (res*=a)%=kcz;
        k>>=1;
        (a*=a)%=kcz;
    }
    return res;
}
inline void solve()
{
    if(n==1)
    {
        printf("%lld\n",qpow(2,m));
        return;
    }
    if(n==2)
    {
        printf("%lld\n",4*qpow(3,m-1)%kcz);
        return;
    }
    if(n==3 && m==3)
    {
        printf("112\n");
        return;
    }
    ans=0;
    dfs(0,0);
    printf("%d\n",ans);
}
int main()
{
    freopen("game.in","r",stdin); freopen("game.out","w",stdout);
    scanf("%d%d",&n,&m);
    if(n>m) swap(n,m);
    solve();
    fclose(stdin); fclose(stdout);
    return 0;
}
