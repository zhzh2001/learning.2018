#include<cstdio>
#include<cstring>
#include<algorithm>
using std::min;
typedef long long ll;
const int N=1e5+5;
int n,m;
char s[N];
int w[N];
int v[N<<1],first[N],next[N<<1];
namespace solver1
{
	const ll inf=0x3f3f3f3f3f3f3f3f;
	ll dp[N][2];
	int a[N];
	void dfs(int now,int father)
	{
		register int go;
		dp[now][0]=0;dp[now][1]=w[now];
		for(go=first[now];go;go=next[go])
			if(v[go]!=father)
			{
				dfs(v[go],now);
				if((dp[now][0]+=dp[v[go]][1])>=inf)
					dp[now][0]=inf;
				if((dp[now][1]+=min(dp[v[go]][0],dp[v[go]][1]))>=inf)
					dp[now][1]=inf;
			}
		if(a[now]==0)
			dp[now][1]=inf;
		if(a[now]==1)
			dp[now][0]=inf;
		return;
	}
	void main()
	{
		int x,y;
		while(m--)
		{
			memset(a+1,-1,sizeof(int)*n);
			scanf("%d%d",&x,&y);a[x]=y;
			scanf("%d%d",&x,&y);a[x]=y;
			dfs(1,0);
			if(min(dp[1][0],dp[1][1])==inf)
				puts("-1");
			else
				printf("%lld\n",min(dp[1][0],dp[1][1]));
		}
		return;
	}
}
namespace solver2
{
	ll res;
	ll dp[N][2];
	int q_v[N],q_first[N],q_next[N];
	ll ans[N];
	void dfs(int now,int father)
	{
		register int go;
		dp[now][0]=0;dp[now][1]=w[now];
		for(go=first[now];go;go=next[go])
			if(v[go]!=father)
			{
				dfs(v[go],now);
				dp[now][0]+=dp[v[go]][1];
				dp[now][1]+=min(dp[v[go]][0],dp[v[go]][1]);
			}
		return;
	}
	void trans(int now,int father)
	{
		register int go;
		for(go=q_first[now];go;go=q_next[go])
			ans[go]=res+dp[now][q_v[go]];
		for(go=first[now];go;go=next[go])
			if(v[go]!=father)
			{
				dp[now][0]-=dp[v[go]][1];
				dp[now][1]-=min(dp[v[go]][0],dp[v[go]][1]);
				dp[v[go]][0]+=dp[now][1];
				dp[v[go]][1]+=min(dp[now][0],dp[now][1]);
				trans(v[go],now);
				dp[v[go]][1]-=min(dp[now][0],dp[now][1]);
				dp[v[go]][0]-=dp[now][1];
				dp[now][1]+=min(dp[v[go]][0],dp[v[go]][1]);
				dp[now][0]+=dp[v[go]][1];
			}
		return;
	}
	void main()
	{
		int x;
		register int i,go;
		res=w[1];
		for(go=first[1];go;go=next[go])
			dfs(v[go],1),res+=min(dp[v[go]][0],dp[v[go]][1]);
		for(i=1;i<=m;i++)
		{
			scanf("%*d%*d%d%d",&x,&q_v[i]);
			q_next[i]=q_first[x];q_first[x]=i;
		}
		for(go=first[1];go;go=next[go])
		{
			res-=min(dp[v[go]][0],dp[v[go]][1]);
			trans(v[go],1);
			res+=min(dp[v[go]][0],dp[v[go]][1]);
		}
		for(i=1;i<=m;i++)
			printf("%lld\n",ans[i]);
		return;
	}
}
namespace solver3
{
	ll dp[N][2];
	struct cell
	{
		int u;
		int b1,b2;
	}q_v[N];
	int q_first[N],q_next[N];
	ll ans[N];
	void dfs(int now,int father)
	{
		register int go;
		dp[now][0]=0;dp[now][1]=w[now];
		for(go=first[now];go;go=next[go])
			if(v[go]!=father)
			{
				dfs(v[go],now);
				dp[now][0]+=dp[v[go]][1];
				dp[now][1]+=min(dp[v[go]][0],dp[v[go]][1]);
			}
		return;
	}
	void trans(int now,int father)
	{
		register int go;
		for(go=q_first[now];go;go=q_next[go])
			if(q_v[go].b1==0&&q_v[go].b2==0)
				ans[go]=-1;
			else if(q_v[go].b1==0&&q_v[go].b2==1)
				ans[go]=dp[now][0];
			else if(q_v[go].b1==1&&q_v[go].b2==0)
				ans[go]=dp[now][1]-min(dp[q_v[go].u][0],dp[q_v[go].u][1])+dp[q_v[go].u][0];
			else
				ans[go]=dp[now][1]-min(dp[q_v[go].u][0],dp[q_v[go].u][1])+dp[q_v[go].u][1];
		for(go=first[now];go;go=next[go])
			if(v[go]!=father)
			{
				dp[now][0]-=dp[v[go]][1];
				dp[now][1]-=min(dp[v[go]][0],dp[v[go]][1]);
				dp[v[go]][0]+=dp[now][1];
				dp[v[go]][1]+=min(dp[now][0],dp[now][1]);
				trans(v[go],now);
				dp[v[go]][1]-=min(dp[now][0],dp[now][1]);
				dp[v[go]][0]-=dp[now][1];
				dp[now][1]+=min(dp[v[go]][0],dp[v[go]][1]);
				dp[now][0]+=dp[v[go]][1];
			}
		return;
	}
	void main()
	{
		int x;
		register int i;
		for(i=1;i<=m;i++)
		{
			scanf("%d%d%d%d",&q_v[i].u,&q_v[i].b2,&x,&q_v[i].b1);
			q_next[i]=q_first[x];q_first[x]=i;
		}
		dfs(1,0);trans(1,0);
		for(i=1;i<=m;i++)
			printf("%lld\n",ans[i]);
		return;
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	register int i;
	scanf("%d%d",&n,&m);
	scanf("%s",s);
	for(i=1;i<=n;i++)
		scanf("%d",&w[i]);
	for(i=1;i<=n-1;i++)
	{
		scanf("%d%d",&v[n-1+i],&v[i]);
		next[i]=first[v[n-1+i]];first[v[n-1+i]]=i;
		next[n-1+i]=first[v[i]];first[v[i]]=n-1+i;
	}
	if(n<=2000&&m<=2000)
		solver1::main();
	else if(s[1]=='1')
		solver2::main();
	else if(s[1]=='2')
		solver3::main();
	return 0;
}
