#include<cstdio>
#include<cstring>
#include<algorithm>
using std::sort;
const int N=1e5+5,M=1e5+5;
int n,m,idx,tot;
struct cell
{
	int u,v;
	cell(int _u=0,int _v=0)
	{
		u=_u;v=_v;
		return;
	}
}e[M];
int deg[N];
cell pool[N<<3];
int ptr;
cell *g[N];
int size[N];
bool vis[M],book[M];
int best[N],cur[N];
void dfs(int now)
{
	register int i;
	cur[++idx]=now;vis[now]=1;
	for(i=0;i<size[now];i++)
		if(!book[g[now][i].u]&&!vis[g[now][i].v])
			dfs(g[now][i].v);
	return;
}
inline void update()
{
	register int i;
	for(i=1;i<=n;i++)
		if(best[i]==0||best[i]>cur[i])
		{
			memcpy(best+1,cur+1,sizeof(int)*n);
			break;
		}
		else if(best[i]<cur[i])
			break;
	return;
}
inline bool cmp(cell a,cell b)
{
	return a.v<b.v;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	register int i;
	scanf("%d%d",&n,&m);
	for(i=1;i<=m;i++)
	{
		scanf("%d%d",&e[i].u,&e[i].v);
		deg[e[i].u]++;deg[e[i].v]++;
	}
	for(i=1;i<=n;i++)
		g[i]=pool+ptr,ptr+=deg[i];
	for(i=1;i<=m;i++)
	{
		g[e[i].u][size[e[i].u]++]=cell(i,e[i].v);
		g[e[i].v][size[e[i].v]++]=cell(i,e[i].u);
	}
	for(i=1;i<=n;i++)
		sort(g[i],g[i]+size[i],cmp);
	if(m==n-1)
	{
		idx=0;memset(vis+1,0,sizeof(bool)*n);
		dfs(1);
		update();
	}
	else
		for(i=1;i<=m;i++)
		{
			book[i]=1;
			idx=0;memset(vis+1,0,sizeof(bool)*n);
			dfs(1);
			if(idx==n)
				update();
			book[i]=0;
		}
	for(i=1;i<=n;i++)
		printf("%d ",best[i]);
	putchar('\n');
	return 0;
}
