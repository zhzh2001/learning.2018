#include<cstdio>
#include<cstring>
const int mod=1e9+7;
inline int add(int a,int b)
{
	return (a+=b)>=mod?a-mod:a;
}
inline int sub(int a,int b)
{
	return (a-=b)<0?a+mod:a;
}
inline int mul(int a,int b)
{
	return (long long)a*b%mod;
}
inline int qpow(int a,int b)
{
	int res=1;
	for(;b;a=mul(a,a),b>>=1)
		if(b&1)
			res=mul(res,a);
	return res;
}
int n,m;
namespace solver1
{
	const int N=10;
	int ans;
	int a[N][N];
	int last,flag;
	void dfs2(int x,int y,int w)
	{
		if(x==n&&y==m)
		{
			if(w<last)
				flag=0;
			last=w;
			return;
		}
		if(y<m)
			dfs2(x,y+1,w<<1|a[x][y+1]);
		if(x<n)
			dfs2(x+1,y,w<<1|a[x+1][y]);
		return;
	}
	inline bool check()
	{
		flag=1;last=0;
		dfs2(1,1,a[1][1]);
	//	if(flag)
	//	{
	//		for(int i=1;i<=n;i++)
	//		{
	//			for(int j=1;j<=m;j++)
	//				printf("%d ",a[i][j]);
	//			putchar('\n');
	//		}
	//		putchar('\n');
	//	}
		return flag;
	}
	void dfs(int x,int y)
	{
		if(x==n+1)
		{
			if(check())
				ans++;
			return;
		}
		a[x][y]=0;
		if(y==m)
			dfs(x+1,1);
		else
			dfs(x,y+1);
		a[x][y]=1;
		if(y==m)
			dfs(x+1,1);
		else
			dfs(x,y+1);
		return;
	}
	void main()
	{
		dfs(1,1);
		printf("%d\n",ans);
		return;
	}
}
namespace solver2
{
	void main()
	{
		printf("%d\n",mul(4,qpow(3,m-1)));
		return;
	}
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	register int i;
	scanf("%d%d",&n,&m);
	if(n<=3&&m<=3)
		solver1::main();
	else if(n==2)
		solver2::main();
	return 0;
}
