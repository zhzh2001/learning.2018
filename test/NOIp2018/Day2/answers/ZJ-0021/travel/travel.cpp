#include<cstdio>
#include<algorithm>
#define maxn 5005
using namespace std;
int b[maxn],c[maxn],nox,noy;
int n,m,tot,num,s,t,dep[maxn],fa[maxn],F[maxn],ans[maxn],lnk[maxn],son[maxn<<1],net[maxn<<1];
struct chj{
	int x,y,z;
	bool operator <(const chj b)const{return z<b.z||(z==b.z&&min(x,y)<min(b.x,b.y));}
}a[maxn];
inline char nc(){
	static char buf[100000],*L=buf,*R=buf;
	return L==R&&(R=(L=buf)+fread(buf,1,100000,stdin),L==R)?EOF:*L++;
}
inline int read(){
	int ret=0,f=1;char ch=nc();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=nc();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=nc();
	return ret*f;
}
inline int abs_(int x){return  x>0?x:-x;}
inline int get(int x){return x==fa[x]?x:fa[x]=get(fa[x]);}
inline void add(int x,int y){net[++tot]=lnk[x];son[tot]=y;lnk[x]=tot;}
inline bool check(int x,int y){
	if (m==n-1) return 1;
	if (x==nox&&y==noy) return 0;
	if (x==noy&&y==nox) return 0;
	return 1;
}
void DFS(int x,int fa){
	ans[++num]=x;dep[x]=dep[fa]+1;F[x]=fa;
	int top=0,que[5000];
	for (int j=lnk[x];j;j=net[j])
	if (son[j]!=fa&&check(x,son[j])) que[++top]=son[j];
	if (top){
		sort(que+1,que+top+1);
		for (int i=1;i<=top;i++) DFS(que[i],x);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=m;i++){
		a[i].x=read(),a[i].y=read();
		a[i].z=abs_(a[i].x-a[i].y);
	}
	for (int i=1;i<=n;i++) fa[i]=i;
	for (int i=1;i<=m;i++){
		int fx=get(a[i].x),fy=get(a[i].y);
		if (fx==fy){s=a[i].x,t=a[i].y;continue;}
		fa[fx]=fy;
		add(a[i].x,a[i].y);
		add(a[i].y,a[i].x);
	}
	DFS(1,0);
	if (m==n-1){
		for (int i=1;i<n;i++) printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}else{
		add(s,t);add(t,s);
		while (s!=t){
			if (dep[s]>dep[t]) b[++b[0]]=s,s=F[s];
			else c[++c[0]]=t,t=F[t];
		}
		for (int i=1;i<=b[0]/2;i++) swap(b[i],b[b[0]-i+1]);
		for (int i=1;i<=c[0];i++) b[++b[0]]=c[i];
		int L=1,R=b[0];
		if (b[L]<b[R]){
			while (b[L]<b[R]) L++;
			nox=b[L-1],noy=b[L];
		}else{
			while (b[L]>b[R]) R--;
			nox=b[R],noy=b[R+1];
		}
		num=0;
		DFS(1,0);
		for (int i=1;i<num;i++) printf("%d ",ans[i]);
		printf("%d\n",ans[num]);
	}
    return 0;
}
