#include<cstdio>
#include<algorithm>
#define maxn 100005
#define LL long long
using namespace std;
LL INF=(LL)1<<60,f[maxn][2],g[maxn][2];
int n,Q,ty,tot,nox,noy,a[maxn],F[maxn],lnk[maxn],son[maxn<<1],net[maxn<<1];
inline char nc(){
	static char buf[100000],*L=buf,*R=buf;
	return L==R&&(R=(L=buf)+fread(buf,1,100000,stdin),L==R)?EOF:*L++;
}
inline int read(){
	int ret=0,f=1;char ch=nc();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=nc();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=nc();
	return ret*f;
}
inline void add(int x,int y){net[++tot]=lnk[x];son[tot]=y;lnk[x]=tot;}
void DFS(int x,int fa){
	F[x]=fa;f[x][0]=0;f[x][1]=a[x];
	for (int j=lnk[x];j;j=net[j])
	if (son[j]!=fa){
		DFS(son[j],x);
		f[x][0]+=f[son[j]][1];
		f[x][1]+=min(f[son[j]][1],f[son[j]][0]);
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();Q=read();ty=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<n;i++){
		int x=read(),y=read();
		add(x,y);add(y,x);
	}
	DFS(1,0);
	for (int i=1;i<=n;i++) g[i][0]=f[i][0],g[i][1]=f[i][1];
	while (Q--){
		int s=read(),x=read();nox=s;
		LL ls0=g[s][0],ls1=g[s][1];g[s][x^1]=INF;
		while (s!=1){
			int fa=F[s];
			LL x=g[fa][0],y=g[fa][1];
			g[fa][0]+=g[s][1]-ls1;
			g[fa][1]+=min(g[s][0],g[s][1])-min(ls0,ls1);
			ls0=x;ls1=y;s=fa;
		}
		s=read(),x=read();noy=s;
		ls0=g[s][0],ls1=g[s][1];g[s][x^1]=INF;
		while (s!=1){
			int fa=F[s];
			LL x=g[fa][0],y=g[fa][1];
			g[fa][0]+=g[s][1]-ls1;
			g[fa][1]+=min(g[s][0],g[s][1])-min(ls0,ls1);
			ls0=x;ls1=y;s=fa;
		}
		if (min(g[1][0],g[1][1])<INF) printf("%lld\n",min(g[1][0],g[1][1]));
		else printf("-1\n");
		while (nox!=0) g[nox][0]=f[nox][0],g[nox][1]=f[nox][1],nox=F[nox];
		while (noy!=0) g[noy][0]=f[noy][0],g[noy][1]=f[noy][1],noy=F[noy];
	}
	return 0;
}
