#include<cstdio>
#define tt 1000000007
#define LL long long
using namespace std;
int n,m;
inline int qsm(int a,int b){
	int w=a,s=1;
	while (b){
		if (b&1) s=(LL)s*w%tt;
		w=(LL)w*w%tt;
		b=b>>=1;
	}
	return s;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==3&&m==2){printf("36\n");return 0;}
	if (n==3&&m==3){printf("112\n");return 0;}
	if (n==5&&m==5){printf("7136\n");return 0;}
	if (n==1) printf("%d\n",qsm(2,m));else
	if (m==1) printf("%d\n",qsm(2,n));else
	if (n==2) printf("%lld\n",(LL)4*qsm(3,m-1)%tt);else
	if (n==3) printf("%lld\n",(LL)28*qsm(4,m-2)%tt);
	return 0;
}
