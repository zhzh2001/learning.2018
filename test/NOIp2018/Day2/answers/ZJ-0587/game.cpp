#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <string>
#include <queue>
#include <map>
#include <set>
#include <bitset>
#include <vector>
#define P 1000000007
#define M 200010

using namespace std;

typedef long long ll;

int n, m;

ll powmod(ll x, ll y = P - 2, ll p = P) {
	ll ans = 1;
	while(y) {
		if(y & 1) ans = ans * x  % p;
		x = x * x % p;
		y >>= 1;
	}
	return ans;
}

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if(n > m) swap(n, m);
	if(n == 2) {
		printf("%lld\n", (powmod(3, m - 1) * 4 % P + P) % P);
		return 0;
	} else if(n == 1) {
		printf("%lld\n", (powmod(2, m) % P + P) % P);
		return 0;
	} else if(n == 3) {
		printf("%lld\n", (powmod(3, m - 3) * 112ll % P + P) % P);
		return 0;
	} else if(n == 4) {
		if(n == 4 && m == 4) puts("912"); else printf("%lld\n", (powmod(3, m - 5) * 2688ll % P + P) % P);
		return 0;
	}
	return 0;
}
