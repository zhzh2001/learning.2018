#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <string>
#include <queue>
#include <map>
#include <set>
#include <bitset>
#include <vector>
#define M 800010

using namespace std;

typedef long long ll;

const ll Inf = 1e13;

int Head[M], Next[M], Go[M], Val[M], Cnt = 0, n, m, ax, bx, ay, by;

ll F[M][2];

void addedge(int x, int y) {
	Go[++Cnt] = y;
	Next[Cnt] = Head[x];
	Head[x] = Cnt;
}

void DFS(int x, int y) {
	for(int T = Head[x]; T; T = Next[T]) if(Go[T] != y) {
		DFS(Go[T], x);
	}
	F[x][0] = 0;
	F[x][1] = Val[x];
	if(!(x == ax && bx == 1) && !(x == ay && by == 1)) {
		for(int T = Head[x]; T; T = Next[T]) if(Go[T] != y) F[x][0] += F[Go[T]][1];
	} else F[x][0] = Inf;
	if(!(x == ax && bx == 0) && !(x == ay && by == 0)) {
		for(int T = Head[x]; T; T = Next[T]) if(Go[T] != y) F[x][1] += min(F[Go[T]][0], F[Go[T]][1]);
	} else F[x][1] = Inf;
	
}

ll Solve() {
	memset(F, 63, sizeof F);
	scanf("%d%d%d%d", &ax, &bx, &ay, &by);
	DFS(1, 0);
	if(F[1][0] >= Inf && F[1][1] >= Inf) return -1;
	return min(F[1][0], F[1][1]);
}

char chh[7];

struct node {
	ll X[2][2];
	node() {memset(X, 63, sizeof X);}
	node operator + (const node& A) {
		node Ans;
		for(int i = 0; i < 2; i++)
			for(int j = 0; j < 2; j++)
				if(i || j)
					for(int k = 0; k < 2; k++)
						for(int l = 0; l < 2; l++)
							Ans.X[k][l] = min(Ans.X[k][l], X[k][i] + A.X[j][l]);
		return Ans;
	}
};

struct SolveA {
	node F[M];
	
	void build(int x, int L, int R) {
		if(L == R) {
			F[x].X[0][0] = 0;
			F[x].X[1][1] = Val[L];
			F[x].X[0][1] = Inf;
			F[x].X[1][0] = Inf;
			return;
		}
		int md = (L + R) / 2;
		build(x * 2, L, md);
		build(x * 2 + 1, md + 1, R);
		F[x] = F[x * 2] + F[x * 2 + 1];
	}
	
	node calc(int x, int L, int R, int l, int r, int a, int b) {
		if(l == L && r == R) return F[x];
		int md = (L + R) / 2;
		node o;
		if(l <= md && r > md) o = calc(x * 2, L, md, l, min(md, r), a, b) + calc(x * 2 + 1, md + 1, R, max(md + 1, l), r, a, b);
		else if(l <= md) o = calc(x * 2, L, md, l, min(md, r), a, b);
		else o = calc(x * 2 + 1, md + 1, R, max(md + 1, l), r, a, b);
		return o;
	}
	
	ll calc(int l, int r, int a, int b) {
		node tmp = calc(1, 1, n, l, r, a, b);
		ll ans = Inf;
		for(int i = 0; i < 2; i++)
			for(int j = 0; j < 2; j++)
				if(i != 1 - a && j != 1 - b)
					ans = min(ans, tmp.X[i][j]);
		return ans;
	}
	
	void SubA() {
		build(1, 1, n);
		for(int i = 1; i <= m; i++) {
			int a, b, c, d;
			scanf("%d%d%d%d", &a, &b, &c, &d);
			if(a > c) swap(a, c), swap(b, d);
			ll ans = 0;
			if(b == 1) ans -= Val[a];
			if(d == 1) ans -= Val[c];
			ans += calc(1, a, -1, b) + calc(a, c, b, d) + calc(c, n, d, -1);
			if(ans >= Inf) puts("-1"); else printf("%lld\n", ans);
		}
	}
} solveA;

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d%s", &n, &m, chh);
	for(int i = 1; i <= n; i++) scanf("%d", &Val[i]);
	for(int i = 1; i < n; i++) {
		int a, b;
		scanf("%d%d", &a, &b);
		addedge(a, b);
		addedge(b, a);
	}
	if(n > 2000 && chh[0] == 'A') {
		solveA.SubA();
		return 0;
	}
	for(int i = 1; i <= m; i++) printf("%lld\n", Solve());
	return 0;
}
