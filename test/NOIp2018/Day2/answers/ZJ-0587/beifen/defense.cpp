#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <string>
#include <queue>
#include <map>
#include <set>
#include <bitset>
#include <vector>
#define M 200010

using namespace std;

typedef long long ll;

const ll Inf = 1e13;

int Head[M], Next[M], Go[M], Val[M], Cnt = 0, n, m, ax, bx, ay, by;

ll F[M][2];

void addedge(int x, int y) {
	Go[++Cnt] = y;
	Next[Cnt] = Head[x];
	Head[x] = Cnt;
}

void DFS(int x, int y) {
	for(int T = Head[x]; T; T = Next[T]) if(Go[T] != y) {
		DFS(Go[T], x);
	}
	F[x][0] = 0;
	F[x][1] = Val[x];
	if(!(x == ax && bx == 1) && !(x == ay && by == 1)) {
		for(int T = Head[x]; T; T = Next[T]) if(Go[T] != y) F[x][0] += F[Go[T]][1];
	} else F[x][0] = Inf;
	if(!(x == ax && bx == 0) && !(x == ay && by == 0)) {
		for(int T = Head[x]; T; T = Next[T]) if(Go[T] != y) F[x][1] += min(F[Go[T]][0], F[Go[T]][1]);
	} else F[x][1] = Inf;
	
}

ll Solve() {
	memset(F, 63, sizeof F);
	scanf("%d%d%d%d", &ax, &bx, &ay, &by);
	DFS(1, 0);
	if(F[1][0] >= Inf && F[1][1] >= Inf) return -1;
	return min(F[1][0], F[1][1]);
}

char chh[7];

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d%s", &n, &m, chh);
	for(int i = 1; i <= n; i++) scanf("%d", &Val[i]);
	for(int i = 1; i < n; i++) {
		int a, b;
		scanf("%d%d", &a, &b);
		addedge(a, b);
		addedge(b, a);
	}
	for(int i = 1; i <= m; i++) printf("%lld\n", Solve());
	return 0;
}
