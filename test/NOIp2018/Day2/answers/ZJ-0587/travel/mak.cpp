#include <bits/stdc++.h>

using namespace std;

int main() {
	freopen("travel.in", "w", stdout);
	int n = 5000, m = 5000;
	printf("%d %d\n", n, m);
	int tmp[5010];
	for(int i = 1; i <= n; i++) tmp[i] = i;
	random_shuffle(tmp + 1, tmp + n + 1);
	for(int i = 1; i < n; i++) printf("%d %d\n", tmp[i], tmp[i + 1]);
	printf("%d %d\n", tmp[1], tmp[n]);
	return 0;
}
