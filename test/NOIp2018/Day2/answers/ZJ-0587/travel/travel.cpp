#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <string>
#include <queue>
#include <map>
#include <set>
#include <bitset>
#include <vector>

#define M 200010

using namespace std;

int Go[5010][5010], GoT[5010];

int FF[5010], NwP[5010], ea, eb, n, m;

int getf(int x) {return FF[x] == x ? x : FF[x] = getf(FF[x]);}

int Ans[5010], anst = 0, NwAns[5010], nwanst = 0;

vector <int> Vtmp;

void addedge(int x, int y) {
	Go[x][GoT[x]++] = y;
}

void Do(int x, int f, int ex, int ey) {
	NwAns[nwanst++] = x;
	for(int i = 0; i < GoT[x]; i++) if(Go[x][i] != f) {
		int g = Go[x][i];
		if(x == ex && g == ey || x == ey && g == ex) continue;
		Do(g, x, ex, ey);
	}
}

void Do(int ex, int ey) {
	nwanst = 0;
	for(int i = 1; i <= n; i++) NwP[i] = 0;
	Do(1, 0, ex, ey);
}

int flag = 0;

void DFSx(int x, int f) {
	Vtmp.push_back(x);
	if(x == eb) {
		flag = 1;
		return;
	}
	for(int i = 0; i < GoT[x]; i++) if(Go[x][i] != f) {
		DFSx(Go[x][i], x);
		if(flag) return;
	}
	Vtmp.pop_back();
}

void cmin() {
	if(anst == 0) {
		memcpy(Ans, NwAns, sizeof NwAns);
		anst = nwanst;
		return;
	}
	for(int i = 0; i < anst; i++)
		if(Ans[i] < NwAns[i]) return;
		else if(Ans[i] > NwAns[i]) {
			memcpy(Ans, NwAns, sizeof NwAns);
			anst = nwanst;
			return;
		}
	return;
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	anst = 0;
	memset(Ans, 0, sizeof Ans);
	if(m == n - 1) {
		for(int i = 1; i <= m; i++) {
			int x, y;
			scanf("%d%d", &x, &y);
			addedge(x, y);
			addedge(y, x);
		}
		for(int i = 1; i <= n; i++) sort(Go[i], Go[i] + GoT[i]);
		Do(-1, -1);
		cmin();
		for(int i = 0; i < anst; i++) printf("%d ", Ans[i]);
	} else {
		for(int i = 1; i <= n; i++) FF[i] = i;
		for(int i = 1; i <= m; i++) {
			int x, y;
			scanf("%d%d", &x, &y);
			if(getf(x) == getf(y)) {
				ea = x;
				eb = y;
			} else {
				FF[getf(x)] = getf(y);
				addedge(x, y);
				addedge(y, x);
			}
		}
		Vtmp.clear();
		DFSx(ea, 0);
		addedge(ea, eb);
		addedge(eb, ea);
		for(int i = 1; i <= n; i++) sort(Go[i], Go[i] + GoT[i]);
		Vtmp.push_back(Vtmp[0]);
		for(int i = 0; i + 1 < Vtmp.size(); i++) {
			Do(Vtmp[i], Vtmp[i + 1]);
			cmin();
		}
		for(int i = 0; i < anst; i++) printf("%d ", Ans[i]);
	}
	return 0;
}
