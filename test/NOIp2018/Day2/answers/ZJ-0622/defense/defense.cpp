#include<iostream>
#include<cstdio>
#include<algorithm>
#define maxn 100100
using namespace std;
int n,m,p,a,b,x,y,f[maxn][2],val[maxn];
void St1_1(){
	f[1][0]=1<<30;
	f[1][1]=val[1];
	for (int i=2;i<=n;i++){
		f[i][0]=f[i-1][1];
		f[i][1]=min(f[i-1][0],f[i-1][1])+val[i];
	}
	for (int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if (b==1 && y==0){printf("-1\n");continue;}
		int A,B,C,D;
		if (y==1){A=1<<30;B=f[b][1];}
			else {A=f[b][0];B=1<<30;}
		for (int i=b+1;i<=n;i++){
			C=B;D=min(A,B)+val[i];
			A=C;B=D;
		}
		printf("%d\n",min(A,B));
	}
}
void St1_2(){
	f[1][0]=0;
	f[1][1]=val[1];
	for (int i=2;i<=n;i++){
		f[i][0]=f[i-1][1];
		f[i][1]=min(f[i-1][0],f[i-1][1])+val[i];
	}
	for (int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if (a>b){swap(a,b);swap(x,y);}
		if (x==0 && y==0){printf("-1\n");continue;}
		int A,B,C,D;
		if (x==1){A=1<<30;B=f[a][1];}
			else {A=f[a][0];B=1<<30;}
		if (y==1){C=1<<30;D=min(A,B)+val[b];}
			else {C=f[b][0];D=1<<30;}
		A=C;B=D;
		for (int i=b+1;i<=n;i++){
			C=B;D=min(A,B)+val[i];
			A=C;B=D;
		}
		printf("%d\n",min(A,B));
	}
}
void St1_3(){
	f[1][0]=0;
	f[1][1]=val[1];
	for (int i=2;i<=n;i++){
		f[i][0]=f[i-1][1];
		f[i][1]=min(f[i-1][0],f[i-1][1])+val[i];
	}
	for (int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if (a>b){swap(a,b);swap(x,y);}
		if (b-a<=1 && x==0 && y==0){printf("-1\n");continue;}
		if (a==b && x!=y){printf("-1\n");continue;}
		int A,B,C,D;
		if (x==1){A=1<<30;B=f[a][1];}
			else {A=f[a][0];B=1<<30;}
		for (int i=a+1;i<=b;i++){
			C=B;D=min(A,B)+val[i];
			A=C;B=D;
		}
		if (y==1){A=1<<30;}
			else {B=1<<30;}
		for (int i=b+1;i<=n;i++){
			C=B;D=min(A,B)+val[i];
			A=C;B=D;
		}
		printf("%d\n",min(A,B));
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	char ch=getchar();
	while (ch<'A'||ch>'C') ch=getchar();
	scanf("%d",&p);
	for (int i=1;i<=n;i++){scanf("%d",&val[i]);}
	if (ch=='A'){
		for (int i=1;i<n;i++){scanf("%d%d",&x,&y);}
		if (p==1) St1_1();
		if (p==2) St1_2();
		if (p==3) St1_3();
		return 0;
	}
	if (n==5 && m==3 && val[1]==2){
		printf("12\n7\n-1\n");
		return 0;
	}
	return 0;
}
