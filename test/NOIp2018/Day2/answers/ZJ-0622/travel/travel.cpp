#include<iostream>
#include<cstdio>
#include<cstring>
#define maxn 5200
using namespace std;
int n,m,cnt,tot=0,x,y,Last[maxn],P[maxn],Ans[maxn];
bool B[maxn][maxn];
struct recEdge{int pre,to;} E[maxn];
inline void read(int &x){
	char ch=getchar();int sym;
	for(;(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if (ch=='-'){sym=-1;ch=getchar();}else sym=1;
	for(x=0;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;
	x*=sym;
}
inline void addEdge(int u,int v){
	E[++tot].pre=Last[u];Last[u]=tot;
	E[tot].to=v;
}
inline bool cmp(){
	if (Ans[1]==0) return 1;
	for (int i=1;i<=n;i++){
		if (P[i]<Ans[i]) return 1;
		if (P[i]>Ans[i]) return 0;
	}
	return 0;
}
void getAns(int u,int fa){
	P[++cnt]=u;
	if (cnt>n) return;
	for (int ed=Last[u];ed;ed=E[ed].pre){
		if (E[ed].to==fa || (!B[u][E[ed].to])) continue;
		getAns(E[ed].to,u);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);read(m);
	for (int i=1;i<=m;i++){
		read(x);read(y);
		B[x][y]=1;
		B[y][x]=1;
	}
	memset(Last,0,sizeof(Last));
	for (int i=1;i<=n;i++){
		for (int j=n;j;j--){
			if (B[i][j]) addEdge(i,j);
		}
	}
	if (m==n-1){
		cnt=0;
		getAns(1,0);
		for (int i=1;i<=n;i++){
			printf("%d ",P[i]);
		}
	} else {
		memset(Ans,0,sizeof(Ans));
		for (int i=1;i<=n;i++){
			for (int ed=Last[i];ed;ed=E[ed].pre){
				if (E[ed].to<i) continue;
				cnt=0;
				B[i][E[ed].to]=B[E[ed].to][i]=0;
				getAns(1,0);
				if (cnt==n){
					if (cmp()){
						for (int j=1;j<=n;j++) Ans[j]=P[j];
					}
				}
				B[i][E[ed].to]=B[E[ed].to][i]=1;
			}
		}
		for (int i=1;i<=n;i++){
			printf("%d ",Ans[i]);
		}
	}
	return 0;
}
