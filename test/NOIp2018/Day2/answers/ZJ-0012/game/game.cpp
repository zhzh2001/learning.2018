#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=9,M=1e6+10;
const ll mod=1e9+7;

pair<string,string>tax[100010];
int n,m,nm,tnum;
ll ans;
bool vis[10][10];

void ddfs(int px,int py,string s1,string s2)
{
	if(px==n&&py==m)tax[++tnum]=make_pair(s2,s1);
	if(px<n)ddfs(px+1,py,s1+(char)(vis[px+1][py]+'0'),s2+'D');
	if(py<m)ddfs(px,py+1,s1+(char)(vis[px][py+1]+'0'),s2+'R');
}
bool cant()
{
	string tmp;
	tnum=0;
	tmp=(char)('0'+vis[1][1]);
	ddfs(1,1,"",tmp);
	sort(tax+1,tax+tnum+1);
	tmp=tax[1].second;
	for(int i=2;i<=tnum;i++)
	{
		if(tax[i].second>tmp)return 1;
		tmp=min(tmp,tax[i].second);
	}
	return 0;
}
void dfs(int px,int py)
{
	if(px>n)
	{
		for(int i=1;i<=n;i++)
		{
			for(int j=1;j<=m;j++)
			{
				if(i==n||j==m)continue;
				if(vis[i][j+1]>vis[i+1][j])return;
			}
		}
		if(!cant())ans++;
		return;
	}
	vis[px][py]=1;if(py<m)dfs(px,py+1);else dfs(px+1,1);
	vis[px][py]=0;if(py<m)dfs(px,py+1);else dfs(px+1,1);
}
void solve1()
{
	ans=0,dfs(1,1);
	printf("%lld\n",ans);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	nm=n*m;
	if(nm<=17)return solve1(),0;
	if(n==2)
	{
		ans=4;
		for(int i=2;i<=m;i++)
			ans=ans*3%mod;
		printf("%lld\n",ans);
		return 0;
	}
	if(n==3)
	{
		if(m<=2)return solve1(),0;
		ans=112;
		for(int i=4;i<=m;i++)
			ans=ans*3%mod;
		printf("%lld\n",ans);
		return 0;
	}
	if(n==4)
	{
		if(m<=4)return solve1(),0;
		ans=2688;
		for(int i=6;i<=m;i++)
			ans=ans*3%mod;
		printf("%lld\n",ans);
		return 0;
	}
	solve1();
	return 0;
}
