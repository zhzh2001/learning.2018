#include<bits/stdc++.h>
using namespace std;
const int N=5050;

int n,m,stk[N],snum,huan[N],hnum,ans[N],res[N],rnum;
vector<int>mp[N];
bool vis[N],cant[N][N];

void dfs1(int pos,int f)
{
	if(pos==1)printf("%d",pos);
	else printf(" %d",pos);
	for(int i=0;i<mp[pos].size();i++)
	{
		if(mp[pos][i]==f)continue;
		dfs1(mp[pos][i],pos);
	}
}

void solve1()
{
	for(int i=1;i<=n;i++)
		sort(mp[i].begin(),mp[i].end());
	dfs1(1,0),puts("");
}
void get_huan(int bg)
{
	int ps;
	hnum=0;
	for(int i=snum;i>=1;i--)
	{
		if(stk[i]==bg)
		{
			ps=i;
			break;
		}
	}
	for(int i=ps;i<=snum;i++)
		huan[++hnum]=stk[i];
}
void dfs2(int pos,int f)
{
	vis[pos]=1,stk[++snum]=pos;
	for(int i=0;i<mp[pos].size();i++)
	{
		if(mp[pos][i]==f)continue;
		if(vis[mp[pos][i]])
		{
			get_huan(mp[pos][i]);
			return;
		}
	}
	if(hnum)return;
	for(int i=0;i<mp[pos].size();i++)
	{
		if(mp[pos][i]==f)continue;
		dfs2(mp[pos][i],pos);
		if(hnum)return;
	}
	--snum;
	return;
}
void dfs3(int pos,int f)
{
	res[++rnum]=pos;
	for(int i=0;i<mp[pos].size();i++)
	{
		if(mp[pos][i]==f)continue;
		if(cant[pos][mp[pos][i]])continue;
		dfs3(mp[pos][i],pos);
	}
}
void get_ans()
{
	bool flag=0;
	for(int i=1;i<=n;i++)
	{
		if(res[i]!=ans[i])
		{
			if(res[i]<ans[i])
				flag=1;
			break;
		}
	}
	if(flag)
	{
		for(int i=1;i<=n;i++)
			ans[i]=res[i];
	}	
	return;
}
void solve2()
{
	snum=0;
	memset(vis,0,sizeof vis);
	for(int i=1;i<=n;i++)
		sort(mp[i].begin(),mp[i].end());
	dfs2(1,0);
	memset(cant,0,sizeof cant);
	memset(ans,63,sizeof ans);
	for(int i=1;i<hnum;i++)
	{
		cant[huan[i]][huan[i+1]]=1;
		cant[huan[i+1]][huan[i]]=1;
		rnum=0,dfs3(1,0),get_ans();
		cant[huan[i]][huan[i+1]]=0;
		cant[huan[i+1]][huan[i]]=0;
	}
	cant[huan[hnum]][huan[1]]=cant[huan[1]][huan[hnum]]=1;
	rnum=0,dfs3(1,0),get_ans();
	for(int i=1;i<=n;i++)
	{
		if(i==1)printf("%d",ans[i]);
		else printf(" %d",ans[i]);
	}
	puts("");
	return;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int u,v;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&u,&v);
		mp[u].push_back(v),
		mp[v].push_back(u);
	}
	if(m==n-1)solve1();
	else solve2();
	return 0;
}
