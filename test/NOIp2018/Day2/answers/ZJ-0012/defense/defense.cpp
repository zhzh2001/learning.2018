#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=1e5+10;
const ll inf=1e12;

char TP[10];
int n,m,a[N],shd[2010];
ll f[2010][2];
ll dp[N][2],dpp[N][2],dp0[N][2],dp1[N][2];
bool line[2010][2010];
vector<int>mp[N];

void dfs(int pos,int fa)
{
	int v;ll tp0=0,tp1=0;
	for(int i=0;i<mp[pos].size();i++)
		if(mp[pos][i]!=fa)dfs(mp[pos][i],pos);
	for(int i=0;i<mp[pos].size();i++)
	{
		v=mp[pos][i];
		if(v==fa)continue;
		tp0+=f[v][1],tp1+=min(f[v][0],f[v][1]);
	}
	f[pos][0]=tp0,f[pos][1]=tp1+a[pos];
	if(shd[pos]==1)f[pos][1]=inf;
	if(shd[pos]==2)f[pos][0]=inf;
	return;
}
void solve1()
{
	int aa,xx,bb,yy;ll res;
	while(m--)
	{
		scanf("%d%d%d%d",&aa,&xx,&bb,&yy);
		if(xx==0)shd[aa]=1;//cant
		else shd[aa]=2;//must
		if(yy==0)shd[bb]=1;
		else shd[bb]=2;
		memset(f,0,sizeof f),dfs(1,0),res=inf;
		if(shd[1]!=2)res=min(res,f[1][0]);
		if(shd[1]!=1)res=min(res,f[1][1]);
		shd[aa]=shd[bb]=0;
		(res>=inf)?puts("-1"):printf("%lld\n",res);
	}
}
int main()
{
	int u,v;
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m),scanf("%s",TP);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	for(int i=1;i<n;i++)
	{
		scanf("%d%d",&u,&v);
		mp[u].push_back(v),
		mp[v].push_back(u);
		if(n<=2010)line[u][v]=line[v][u]=1;
	}
	if(n<=2010&&m<=2010)return solve1(),0;
	else
	{
		dp[1][0]=0,dp[1][1]=a[1];
		dp0[1][0]=0,dp0[1][1]=inf;
		dp1[1][0]=inf,dp1[1][1]=a[1];
		for(int i=2;i<=n;i++)
		{
			dp[i][0]=dp[i-1][1],dp[i][1]=min(dp[i-1][0],dp[i-1][1])+a[i];
			dp0[i][0]=dp0[i-1][1],dp0[i][1]=min(dp0[i-1][0],dp0[i-1][1])+a[i];
			dp1[i][0]=dp1[i-1][1],dp1[i][1]=min(dp1[i-1][0],dp1[i-1][1])+a[i];
		}
		dpp[n][0]=0,dpp[n][1]=a[n];
		for(int i=n-1;i>=1;i--)
			dpp[i][0]=dpp[i+1][1],dpp[i][1]=min(dpp[i+1][0],dpp[i+1][1])+a[i];
		dp[0][1]=inf,dp[n+1][1]=inf,dpp[0][1]=inf,dpp[n+1][1]=inf;
		dp0[0][1]=inf,dp0[n+1][1]=inf,dp1[0][1]=inf,dp1[n+1][1]=inf;
		int aa,xx,bb,yy;ll res;
		while(m--)
		{
			scanf("%d%d%d%d",&aa,&xx,&bb,&yy);
			if(aa>bb)swap(aa,bb),swap(xx,yy);
			if(aa==bb-1)
			{
				if(xx==1&&yy==1)
					res=dp[aa][1]+dpp[bb][1];
				if(xx==1&&yy==0)
					res=dp[aa][1]+dpp[bb][0];
				if(xx==0&&yy==1)
					res=dp[aa][0]+dpp[bb][1];
				if(xx==0&&yy==0)
					res=dp[aa-1][1]+dpp[bb+1][1];
				(res>=inf)?puts("-1"):printf("%lld\n",res);
				continue;
			}
			if(xx==1&&yy==1)
				res=min(dp1[bb-1][1],dp1[bb-1][0])+dpp[bb][1];
			if(xx==1&&yy==0)
				res=min(dp1[bb-1][1]+dpp[bb][0],dp1[bb-1][0]+dpp[bb+1][1]);
			if(xx==0&&yy==1)
				res=min(dp0[bb-1][1],dp0[bb-1][0])+dpp[bb][1];
			if(xx==0&&yy==0)
				res=min(dp0[bb-1][1]+dpp[bb][0],dp0[bb-1][0]+dpp[bb+1][1]);
			(res>=inf)?puts("-1"):printf("%lld\n",res);
		}
	}
	return 0;
}
