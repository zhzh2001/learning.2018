#include<bits/stdc++.h>
#define maxn 5005
#define res register int 
using namespace std;
vector<int> g[maxn];
int n, m, low[maxn], dfn[maxn], a[maxn];
int cnt, bridgex[maxn], bridgey[maxn], ans[maxn];
int vis[maxn];
inline int read()
{
	int s = 0, f = 1; char ch = getchar();
	while (!isdigit(ch)) { if (ch == '-') f = -1; ch = getchar(); }
	while (isdigit(ch)) s = (s << 1) + (s << 3) + ch - '0', ch = getchar();
	return s * f;
}


void dfs(int x, int f)
{
	printf("%d ", x);
	for (res i = 0; i < g[x].size(); i++)
	{
		int y = g[x][i];
		if (y == f) continue;
		dfs(y, x);
	}
}

void tarjan(int x, int f)
{
	low[x] = dfn[x] = ++dfn[0];
	for (res i = 0, y; i < g[x].size(); i++)
	    if (!dfn[y = g[x][i]])
	    {
	    	tarjan(y, x);
	    	low[x] = min(low[x], low[y]);
	    	
	    	if (dfn[x] >= low[y]) 
	    	{
	    		cnt++;
	    		bridgex[cnt] = x;
	    		bridgey[cnt] = y;
			} 
		} else 
		if (y != f)
		    low[x] = min(low[x], dfn[y]);
}

void search(int x, int f, int v, int u)
{
	a[++a[0]] = x;
	for (res i = 0; i < g[x].size(); i++)
	{
		int y = g[x][i];
		if (y == f || (x == v && y == u) || (x == u && y == v)) 
		    continue;
		search(y, x, v, u);
	}
}

int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	n = read(); m = read();
	for (res i = 1; i <= m; i++)
	{
		int x = read(), y = read();
		g[x].push_back(y); g[y].push_back(x);
	}
	for (res x = 1; x <= n; x++)
	    if (g[x].size())
	        sort(g[x].begin(), g[x].end());
	if (m == n - 1) dfs(1, 0); else 
	{
		int pp[4] = {0};
		for (res i = 1; i <= n; i++)
		    if (!dfn[i]) tarjan(i, 0);
		for (res i = 1; i <= cnt; i++)
		    vis[bridgex[i]]++, vis[bridgey[i]]++;
		for (res i = 1; i <= n; i++)
		    if (vis[i] == 1)
		        pp[++pp[0]] = i;
		bridgex[++cnt] = pp[1]; bridgey[cnt] = pp[2];
		for (res i = 1; i <= n; i++)
		    ans[i] = n + 1;
		for (res i = 1; i <= cnt; i++)
		{
			bool flag = 0;
			a[0] = 0;
			search(1, 0, bridgex[i], bridgey[i]);
			for (res i = 1; i <= n; i++)
			{
				if (a[i] == ans[i]) continue;
				if (a[i] < ans[i]) 
				{
					flag = 1;
					break;
				}
				if (a[i] > ans[i])
				{
					flag = 0; 
					break;
				}
			}
			if (flag) for (res i = 1; i <= n; i++)
			    ans[i] = a[i];
		}
		for (res i = 1; i <= n; i++)
		    printf("%d ", ans[i]);
	}
}
