#include<bits/stdc++.h>
#define ll long long 
#define res register ll
#define maxn 100005
using namespace std;
const ll inf = 15000000000000;
ll ver[maxn<<1], nex[maxn<<1], head[maxn];
ll dp[maxn][2], a[maxn], p[maxn], n, m;
char type[10];
inline ll read()
{
	ll s = 0, f = 1; char ch = getchar();
	while (!isdigit(ch)) { if (ch == '-') f = -1; ch = getchar(); }
	while (isdigit(ch)) s = (s << 1) + (s << 3) + ch - '0', ch = getchar();
	return s * f;
}

void add_edge(ll x, ll y)
{
	ver[++ver[0]] = y; nex[ver[0]] = head[x]; head[x] = ver[0];
}

inline ll min(ll x, ll y)
{
	return x < y ? x : y;
}

void dfs(ll x, ll f)
{
	if (ver[head[x]] == f && nex[head[x]] == 0) 
	{
		if (a[x] == 0) dp[x][0] = 0;
		if (a[x] == 1) dp[x][1] = p[x];
		if (a[x] == 2) 
		{
			dp[x][1] = p[x];
			dp[x][0] = 0;
		}
		return;
	}
	for (res i = head[x]; i; i = nex[i])
	{
		ll y = ver[i];
		if (y == f) continue;
		dfs(y, x);
	}
	
	if (a[x] == 0 || a[x] == 2)
	{
		ll sum = 0;
		for (res i = head[x]; i; i = nex[i])
	    {
		    ll y = ver[i];
		    if (y == f) continue;
		    sum += dp[y][1];
	    }
	    if (sum < inf) dp[x][0] = sum; else 
	        dp[x][0] = inf;
	}
	
	if (a[x] == 1 || a[x] == 2)
	{
		ll sum = 0;
		for (res i = head[x]; i; i = nex[i])
		{
			ll y = ver[i];
			if (y == f) continue;
			sum += min(dp[y][1], dp[y][0]);
		}
		if (sum < inf) dp[x][1] = sum + p[x]; else 
		    dp[x][1] = inf;
	}
}

int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	n = read(); m = read(); scanf("%s", &type);
	for (res i = 1; i <= n; i++)
		p[i] = read();
	for (res i = 1; i < n; i++)
	{
		ll x = read(), y = read();
		add_edge(x, y); add_edge(y, x);
	}
	for (res i = 1; i <= n; i++) a[i] = 2;
	for (res i = 1; i <= m; i++)
	{
		int x = read(), px = read();
		int y = read(), py = read();
		a[x] = px; a[y] = py;
		for (res i = 1; i <= n; i++)
		    dp[i][1] = dp[i][0] = inf;
		dfs(1, 0);
		a[x] = 2; a[y] = 2;
		if (dp[1][1] == inf && dp[1][0] == inf)
		    printf("-1\n"); else 
		printf("%lld\n", min(dp[1][1], dp[1][0]));
	}
}
