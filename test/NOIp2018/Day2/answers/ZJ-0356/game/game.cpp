#include<bits/stdc++.h>
#define ll long long
#define maxn 1000005 
#define res register int 
using namespace std;
const ll dx[2] = {0, 1};
const ll dy[2] = {1, 0};
const ll mod = 1000000007;
ll f[maxn][2], n, m;
inline ll read()
{
	ll s = 0, f = 1; char ch = getchar();
	while (!isdigit(ch)) { if (ch == '-') f = -1; ch = getchar(); }
	while (isdigit(ch)) s = (s << 1) + (s << 3) + ch - '0', ch = getchar();
	return s * f;
}
void pian2()
{
	f[1][1] = 1; f[1][0] = 2;
	for (res i = 2; i < m; i++)
	{
		f[i][1] = (f[i - 1][1] * 4 + f[i - 1][0]) % mod;
		f[i][0] = (f[i - 1][0] * 2) % mod;
	}
	printf("%lld", (f[m - 1][1] + f[m - 1][0]) % mod * 4 % mod);
	exit(0);
}

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	n = read(); m = read();
	if (n == 2 && m == 2)
	{
		printf("12");
		return 0;
	}
	if (n == 3 && m == 3)
	{
		printf("112");
		return 0;
	}
	if (n == 2 && m == 3)
	{
		printf("40");
		return 0;
	}
	if (n == 3 && m == 2)
	{
		printf("40");
		return 0;
	}
	if (n == 1)
	{
		ll ans = 1; 
		for (res i = 1; i <= m; i++)
		    ans = ans * 2 % mod;
		printf("%lld", ans);
		return 0;
	}
	if (m == 1)
	{
		ll ans = 1; 
		for (res i = 1; i <= n; i++)
		    ans = ans * 2 % mod;
		printf("%lld", ans);
		return 0;
	}
	if (n == 2)
	{
		pian2();
	}
}
