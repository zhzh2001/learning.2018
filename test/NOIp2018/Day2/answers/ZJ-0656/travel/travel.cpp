#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const int N=5008;
int n,m,pin,pin2,rgl;
int pbg[N],pree[N],ringon[N],ringnd[N];
bool slt[N],ringer[N];
int ans[N],ansf[N];

struct EDGE
{
	int st,ed,next;
}
e[2*N];

void edgein()
{
	for (int i=1;i<=m;i++)
	{
		int stt,edd;
		scanf("%d%d",&stt,&edd);
		if (pbg[stt]==0) pbg[stt]=2*i-1;
		else e[pree[stt]].next=2*i-1;
		pree[stt]=2*i-1; e[2*i-1].st=stt; e[2*i-1].ed=edd; 
		if (pbg[edd]==0) pbg[edd]=2*i;
		else e[pree[edd]].next=2*i;
		pree[edd]=2*i; e[2*i].st=edd; e[2*i].ed=stt; 
	}
}

void treedom(int nd,int fa,int nd1,int nd2)
{
	pin++; ans[pin]=nd; slt[nd]=true;
	int ee=pbg[nd],minix=n+1,emin;
	bool blin=true;
	while (blin)
	{
		blin=false;
		while (ee!=0)
		{
			if (e[ee].ed<minix&&!slt[e[ee].ed]&&!(e[ee].st==nd1&&e[ee].ed==nd2||e[ee].ed==nd1&&e[ee].st==nd2))
			{
				minix=e[ee].ed; emin=ee;
				blin=true; 
			}
			ee=e[ee].next;
		}
		if (blin) 
		{
			treedom(e[emin].ed,nd,nd1,nd2);
			ee=pbg[nd]; minix=n+1;
		}
	}
}

void sol1()
{
	treedom(1,0,-1,-1);
	for (int i=1;i<=n;i++) printf("%d ",ans[i]);
}

bool ring_finder(int nd,int fa)
{
	if (ringer[nd]) 
	{
		int pin3=0,tmp2=ringon[pin2];
		pin2++;
		while (ringon[pin2]!=nd) {pin2--; pin3++; ringnd[pin3]=ringon[pin2];}
		ringnd[pin3+1]=tmp2;
		rgl=pin3;
		return true;
	}
	pin2++; ringon[pin2]=nd; 
	ringer[nd]=true;
	int ee=pbg[nd],minix=n+1,emin;
	while (ee!=0)
	{
		if (e[ee].ed!=fa)
		{
			if (ring_finder(e[ee].ed,nd)) return true;
		}
		ee=e[ee].next;
	}
	ringon[pin2]=0; pin2--; 
	return false;
}

void sol2()
{
	ring_finder(1,0);
	ansf[1]=n;
	for (int i=1;i<=rgl;i++)
	{
		pin2=0; pin=0; 
		memset(ans,0,sizeof(ans)); 
		memset(slt,false,sizeof(slt)); 
		treedom(1,0,ringnd[i],ringnd[i+1]);
		int pin4=1;
		while (ans[pin4]==ansf[pin4]&&pin4<=n) pin4++;
		if (pin4<=n&&ansf[pin4]>ans[pin4])
		{
			for (int i=1;i<=n;i++) ansf[i]=ans[i];
		}
	}
	for (int i=1;i<=n;i++) printf("%d ",ansf[i]);
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	edgein();
	if (m==n-1) sol1();
	else sol2();
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
