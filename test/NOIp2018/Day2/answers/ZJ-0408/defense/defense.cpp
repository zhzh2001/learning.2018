#include<iostream>
#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;
vector<int> a[5010];
bool vis[5010],mvis[5010],mnvis[5010],b[5010][5010],zhu[5010];
int w[5010];
int n,m,ans=10000000;
void nbdfs(int x,int sum)
{
	cout<<x<<' '<<sum<<endl;
	if(x==(n+1))
	{
		for(int i=1;i<=n;i++) 
			if(!vis[i]&&!zhu[i]) return;
		ans=min(ans,sum);
		return;
	}
	if(!mnvis[x])
	{
		vis[x]=1;for(int i=0;i<a[x].size();i++) zhu[a[x][i]]=1;
		nbdfs(x+1,sum+w[x]);
		for(int i=0;i<a[x].size();i++) zhu[a[x][i]]=0;
	}
	if(!mvis[x])
	{vis[x]=0;nbdfs(x+1,sum);}
	return;
}
int main()
{
	int t,tx,ty,x,y;
	char p;
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d %c%d",&n,&m,&p,&t);
	for(int i=1;i<=n;i++)
		scanf("%d",&w[i]);
	for(int i=1;i<=n-1;i++)
	{
		scanf("%d%d",&x,&y);
		a[x].push_back(y);
		a[y].push_back(x);
		b[x][y]=b[y][x]=1;
	}
	for(int i=1;i<=m;i++)
	{	
		ans=100000000;
		memset(vis,0,sizeof(vis));
		memset(zhu,0,sizeof(zhu));
		scanf("%d%d%d%d",&x,&tx,&y,&ty);
		if(tx==1) mvis[x]=1;
		if(ty==1) mvis[y]=1;
		if(tx==0) mnvis[x]=1;
		if(ty==0) mnvis[y]=1;
		if(b[x][y]&&!tx&&!ty) {printf("-1\n");continue;}
		nbdfs(1,0);
		printf("%d\n",ans);
		mvis[x]=mvis[y]=mnvis[x]=mnvis[y]=0;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
