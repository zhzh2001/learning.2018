#include<iostream>
#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;
vector<int> a[5010];
int b[5010],c[5010];
bool vis[5010];
int n,m,ans,num=0;
bool cmp(int x,int y)
{
	if(x<y) return true;else return false;}
void nbdfs(int x)
{
	b[++num]=x;
	vis[x]=true;
	memset(c,0,sizeof(c));
	for(int i=1;i<=a[x].size();i++) c[i]=a[x][i-1];
	int sum=a[x].size();
	stable_sort(c+1,c+1+sum,cmp);
	for(int i=0;i<=sum-1;i++)
		a[x][i]=c[i+1];
	for(int i=0;i<sum;i++)
		if(!vis[a[x][i]])
			nbdfs(a[x][i]);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int x,y;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y);
		a[x].push_back(y);
		a[y].push_back(x);
	}
//	if(m==n-1)
		nbdfs(1);
	for(int i=1;i<=num-1;i++)
		printf("%d ",b[i]);
	printf("%d\n",b[num]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
