#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
int edgenum,vet[300005],nex[300005],head[300005];
long long f[300005][2],g[300005][2],p[300005];
int n,m,a,b,c,d,ans;
void addedge(int u,int v)
{
	edgenum++;
	vet[edgenum]=v;
	nex[edgenum]=head[u];
	head[u]=edgenum;
}
void dfs(int u,int pre)
{
	int now=-1;
	if (u==a) now=b;
	if (u==c) now=d;
	if (now==1)
	{
		for (int i=head[u];i!=0;i=nex[i])
		{
			int v=vet[i];
			if (v==pre) continue;
			dfs(v,u);
			f[u][1]=min(1000000000000000000ll,(f[u][1]+f[v][0]));
		}
		f[u][0]=f[u][1];
	}
	else if (now==0)
	{
		f[u][1]=1000000000000000000ll;
		for (int i=head[u];i!=0;i=nex[i])
		{
			int v=vet[i];
			if (v==pre) continue;
			dfs(v,u);
			f[u][0]=min(1000000000000000000ll,(f[u][0]+f[v][1]));
		}
	}
	else
	{
		for (int i=head[u];i!=0;i=nex[i])
		{
			int v=vet[i];
			if (v==pre) continue;
			dfs(v,u);
			f[u][0]=min(1000000000000000000ll,(f[u][0]+f[v][1]));
			f[u][1]=min(1000000000000000000ll,(f[u][1]+f[v][0]));
		}
		f[u][0]=min(f[u][1],f[u][0]);
	}
	
}
void calca()
{
	while (m--)
	{
		scanf("%d%d%d%d",&a,&b,&c,&d);
		for (int i=1;i<=n;i++)
			f[i][0]=0,f[i][1]=p[i];
		ans=0;
		dfs(1,0);
		if (min(f[1][0],f[1][1])==1000000000000000000ll) printf("-1\n");
		else printf("%lld\n",min(f[1][0],f[1][1]));
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char typ[10];
	scanf("%d%d%s",&n,&m,&typ);
	for (int i=1;i<=n;i++)
		scanf("%lld",&p[i]);
	for (int i=1;i<n;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		addedge(x,y);
		addedge(y,x);
	}
	if (n<=2000 && m<=2000) calca();
	else if (typ[0]=='A')
	{
		if (typ[1]=='1')
		{
			f[2][0]=p[1];
			f[2][1]=p[1]+p[2];
			for (int i=3;i<=n;i++)
			{
				f[i][0]=f[i-1][1];
				f[i][1]=min(f[i-1][0],f[i-1][1])+p[i];
			}
			g[n][0]=0;
			g[n][1]=p[n];
			for (int i=n-1;i>=2;i--)
			{
				g[i][0]=g[i+1][1];
				g[i][1]=min(g[i+1][0],g[i+1][1])+p[i];
			}
			while (m--)
			{
				scanf("%d%d%d%d",&a,&b,&c,&d);
				if (d==0)
				{
					if (c==2) printf("-1\n");
					else printf("%d\n",f[c-1][1]+g[c+1][1]);
				}
				else printf("%d\n",min(f[c-1][1],f[c-1][0])+min(g[c+1][1],g[c+1][0])+p[c]);
			}
		}
	}
	return 0;
}
