#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
const int mod=1000000007;
int a[15][15],n,m,ans,len,sta,num[1005];
int flag[1005][1005],f[1000005][6],g[1000005];
void dfs2(int x,int y,int s)
{
	if (x==n && y==m)
	{
		len++,num[len]=s;
		return;
	}
	if (y<m) dfs2(x,y+1,(s+s+a[x][y+1]));
	if (x<n) dfs2(x+1,y,(s+s+a[x+1][y]));
}
bool check()
{
	len=0;
	dfs2(1,1,a[1][1]);
	for (int i=1;i<len;i++)
		if (num[i]>num[i+1])  return false;
	return true;
}
void dfs(int x)
{
	if (x>n*m)
	{
		if (check()) ans++;
		return;
	}
	a[(x-1)/m+1][(x-1)%m+1]=1;
	dfs(x+1);
	a[(x-1)/m+1][(x-1)%m+1]=0;
	dfs(x+1);
}
bool calc(int x,int y)
{
	for (int i=1;i<n;i++)
	{
		int t1=((x&(1<<i))>0),t2=((y&(1<<(i-1)))>0);
		if (t1<t2) return false;
	}
	return true;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n<=3 && m<=3)
	{
		dfs(1);
		printf("%d\n",ans);
	}
	else if (n==2)
	{
		sta=(1<<n);
		for (int i=0;i<sta;i++)
			f[1][i]=1;
		for (int i=0;i<sta;i++)
			for (int j=0;j<sta;j++)
				flag[i][j]=calc(i,j);
		for (int i=2;i<=m;i++)
		{
			for (int j=0;j<sta;j++)
				for (int k=0;k<sta;k++)
					f[i][j]=(f[i][j]+f[i-1][k]*flag[k][j])%mod;
		}
		for (int i=0;i<sta;i++)
			ans=(ans+f[m][i])%mod;
		printf("%d\n",ans);
	}
	return 0;
}
