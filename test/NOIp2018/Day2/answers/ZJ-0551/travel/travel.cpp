#include<bits/stdc++.h>
using namespace std;
const int N=5005;
long long read()
{
	long long x=0,f=1;
	char ch=getchar();
	while(('0'>ch)||(ch>'9'))
	{
		if(ch=='-')
		f=-1;
		ch=getchar();
	}
	while(('0'<=ch)&&(ch<='9'))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
int n,m;
int head[N],ver[N*2],nxt[N*2],tot;
int deg[N];
bool v[N];
int sta[N],top;
queue<int>q;
void add(int x,int y)
{
	deg[x]++;
	ver[++tot]=y;
	nxt[tot]=head[x];
	head[x]=tot;
}
struct data
{
	int x,y;
};
data edge[N*2];
bool cmp(data p,data q)
{
	if(p.x!=q.x)
	return p.x<q.x;
	else
	return p.y>q.y;
}
void topo()
{
	for(int i=1;i<=n;i++)
	if(deg[i]==1)
	{
		q.push(i);
	}
	while(q.size())
	{
		int x=q.front();
		q.pop();
		for(int i=head[x];i;i=nxt[i])
		{
			int y=ver[i];
			if(deg[y]!=1)
			{
				deg[y]--;
				if(deg[y]==1)
				q.push(y);
			}
		}
	}
}
void dfs(int x)
{
	v[x]=1;
	printf("%d ",x);
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];
		if(!v[y])
		{
			dfs(y);
		}
	}
}
void bfs(int st)
{
	q.push(st);
	bool flag=0;
	int a[N],sum,nnxt;
	while(q.size())
	{
		int x=q.front();
		q.pop();
		v[x]=1;
		flag=0;
		nnxt=0;
		sum=0;
		printf("%d ",x);
		for(int i=head[x];i;i=nxt[i])
		{
			int y=ver[i];
			if(!v[y])
			{
				if(!flag)
				{
					if(y>sta[top]&&top)
					{
						while(top&&deg[sta[top]]!=2)
						{
							dfs(sta[top]);
							top--;
						}
						return;
					}
					if(deg[y]!=2)
					{
						dfs(y);
					}
					else
					{
						nnxt=y;
						flag=1;
					}
				}
				else
				{
					a[++sum]=y;
				}
			}
		}
		if(nnxt!=0)
		{
			q.push(nnxt);
			while(sum)
			{
				if(sta[top]>a[sum])
				{
					sta[++top]=a[sum];
				}
				sum--;
			}
		}
	}
}
void nbfs(int st)
{
	while(q.size())
	{
		q.pop();
	}
	q.push(st);
	bool vv[N];
	memset(vv,0,sizeof(vv));
	while(q.size())
	{
		int x=q.front();
		q.pop();
		vv[x]=1;
		if(!v[x])
		{
			v[x]=1;
			printf("%d ",x);
		}
		int nnxt=0;
		for(int i=head[x];i;i=nxt[i])
		{
			int y=ver[i];
			if(deg[y]==2&&!vv[y])
			nnxt=y;
			else
			{
				if(!v[y])
				{
					dfs(y);
				}
			}
		}
		if(nnxt)
		{
			q.push(nnxt);
		}
	}
}
void stdfs(int x)
{
	if(deg[x]==2)
	{
		bfs(x);
		int sec=0;
		for(int i=head[x];i;i=nxt[i])
		{
			int y=ver[i];
			if(deg[y]==2)
			{
				sec=y;
			}
		}
		nbfs(sec);
	}
	else
	{
		v[x]=1;
		printf("%d ",x);
		for(int i=head[x];i;i=nxt[i])
		{
			int y=ver[i];
			if(!v[y])
			{
				stdfs(y);
			}
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();
	m=read();
	int x,y;
	for(int i=1;i<=m;i++)
	{
		x=read();
		y=read();
		edge[i*2-1].x=x;
		edge[i*2-1].y=y;
		edge[i*2].x=y;
		edge[i*2].y=x;
	}
	sort(edge+1,edge+m*2+1,cmp);
	for(int i=1;i<=2*m;i++)
	{
		add(edge[i].x,edge[i].y);
	}
	topo();
	sta[0]=1e9;
	stdfs(1);
	return 0;
}
