#include<bits/stdc++.h>
using namespace std;
const int N=100005;
long long read()
{
	long long x=0,f=1;
	char ch=getchar();
	while(('0'>ch)||(ch>'9'))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(('0'<=ch)&&(ch<='9'))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
int n,m;
char ch[10];
long long p[N];
long long dp[N][2];
long long fdp[N][2];
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();
	m=read();
	scanf("%s",ch+1);
	for(int i=1; i<=n; i++)
	{
		p[i]=read();
	}
	if(ch[1]=='A')
	{
		if(ch[2]=='1')
		{
			int a,b,x,y;
			for(int i=1; i<n; i++)
			{
				x=read();
				y=read();
			}
			dp[1][1]=p[1];
			dp[1][0]=1e15;
			for(int i=2; i<=n; i++)
			{
				dp[i][0]=dp[i-1][1];
				dp[i][1]=min(dp[i-1][0],dp[i][1]);
				if(i>2)
				{
					dp[i][1]=min(dp[i][1],min(dp[i-2][1],dp[i-2][0]));
				}
				dp[i][1]+=p[i];
			}
			for(int i=n; i>=2; i++)
			{
				fdp[i][0]=fdp[i+1][1];
				fdp[i][1]=min(fdp[i+1][1],fdp[i+1][0]);
				if(i<n-2)
				{
					fdp[i][1]=min(fdp[i][1],min(fdp[i+2][1],fdp[i+2][0]));
				}
				fdp[i][1]+=p[i];
			}
			while(m--)
			{
				a=read();
				x=read();
				b=read();
				y=read();
				long long ans1,ans2;
				if(y==1)
				{
					ans1=min(dp[b-2][0],dp[b-2][1]);
					ans2=min(fdp[b+1][0],fdp[b+1][1]);
					if(b<n-1)
					{
						ans2=min(ans2,min(dp[b+2][0],fdp[b+2][1]));
					}
					printf("%lld\n",ans1+ans2);
				}
				else
				{
					ans1=min(dp[b-1][1]+fdp[b+1][0],min(dp[b-1][1]+fdp[b+1][1],dp[b-1][0]+fdp[b+1][1]));
					printf("%lld\n",ans1);
				}
			}
		}
		else if(ch[2]=='2')
		{
			int a,b,x,y;
			for(int i=1; i<n; i++)
			{
				x=read();
				y=read();
			}
			for(int i=1; i<=n; i++)
			{
				dp[i][0]=dp[i-1][1];
				dp[i][1]=min(dp[i-1][0],dp[i][1]);
				if(i>3)
				{
					dp[i][1]=min(dp[i][1],min(dp[i-2][1],dp[i-2][0]));
				}
				dp[i][1]+=p[i];
			}
			for(int i=n; i>=1; i++)
			{
				fdp[i][0]=fdp[i+1][1];
				fdp[i][1]=min(fdp[i+1][1],fdp[i+1][0]);
				if(i<n-2)
				{
					fdp[i][1]=min(fdp[i][1],min(fdp[i+2][1],fdp[i+2][0]));
				}
				fdp[i][1]+=p[i];
			}
			while(m--)
			{
				a=read();
				x=read();
				b=read();
				y=read();
				if(a>b)
				swap(a,b);
				long long ans1,ans2;
				if(x==0&&y==0)
				{
					if((a+b==3)||(a+b==2*n-1))
					printf("-1\n");
					else
					printf("%lld\n",dp[a-1][1]+fdp[b+1][1]);
				}
				else
				if(x==1&&y==0)
				{
					printf("%lld\n",min(dp[a-2][1],dp[a-2][0])+min(dp[b+1][1],dp[b+1][0]));
				}
				else
				if(x==0&&y==1)
				{
					printf("%lld\n",min(dp[a-1][1],dp[a-1][0])+min(dp[b+2][1],dp[b+2][0]));
				}
				else
				{
					printf("%lld\n",min(dp[a-2][1],dp[a-2][0])+min(fdp[b+2][1],fdp[b+2][0]));
				}
			}
		}
		else
		{

		}
	}
}
