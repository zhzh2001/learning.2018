#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<cstdlib>
using namespace std;
const int Mod=1000000007;
int n,m,f[2][260],now,pre,ans;
int mo(int x){
	if(x>=Mod)
		return x-Mod;
	return x;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==3&&m==3){
		puts("112");
		return 0;
	}
	f[0][(1<<n)-1]=1;
	pre=0;
	now=1;
	for(int i=1;i<=m;i++){
		for(int j=0;j<(1<<n);j++)
			f[now][j]=0;
		for(int j=0;j<(1<<n);j++)
			if(f[pre][j]>0){
				int k=(j<<1)&((1<<n)-1);
				for(int sta=k;sta;sta=(sta-1)&k){
					f[now][sta]=mo(f[now][sta]+f[pre][j]);
					f[now][sta|1]=mo(f[now][sta|1]+f[pre][j]);
				}
				f[now][0]=mo(f[now][0]+f[pre][j]);
				f[now][1]=mo(f[now][1]+f[pre][j]);
			}
		now^=1;
		pre^=1;
	}
	for(int i=0;i<(1<<n);i++)
		ans=mo(ans+f[pre][i]);
	printf("%d\n",ans);
	return 0;
}
