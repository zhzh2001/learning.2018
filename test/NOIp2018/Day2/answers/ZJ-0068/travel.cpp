#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<vector>
using namespace std;
int n,m,cnt,now[5005],ans[5005],x[5005],y[5005],X,Y;
bool vis[5005];
vector<int>G[5005];
void dfs(int u){
	vis[u]=true;
	now[++cnt]=u;
	for(vector<int>::iterator it=G[u].begin();it!=G[u].end();it++){
		int v=*it;
		if(!vis[v]&&(u!=X||v!=Y)&&(u!=Y||v!=X))
			dfs(v);
	}
}
bool check(){
	for(int i=1;i<=n;i++)
		if(now[i]<ans[i])
			return true;
		else
		if(now[i]>ans[i])
			return false;
	return false;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&x[i],&y[i]);
		G[x[i]].push_back(y[i]);
		G[y[i]].push_back(x[i]);
	}
	for(int i=1;i<=n;i++)
		sort(G[i].begin(),G[i].end());
	if(m==n-1){
		cnt=0;
		for(int i=1;i<=n;i++)
			vis[i]=false;
		dfs(1);
		for(int i=1;i<n;i++)
			printf("%d ",now[i]);
		printf("%d\n",now[n]);
	}
	else{
		for(int i=1;i<=n;i++)
			ans[i]=n+1;
		for(int i=1;i<=m;i++){
			X=x[i];
			Y=y[i];
			cnt=0;
			for(int j=1;j<=n;j++)
				vis[j]=false;
			dfs(1);
			if(cnt==n&&check())
				for(int j=1;j<=n;j++)
					ans[j]=now[j];
		}
		for(int i=1;i<n;i++)
			printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}
	return 0;
}
