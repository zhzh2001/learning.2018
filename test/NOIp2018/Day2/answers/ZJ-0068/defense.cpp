#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
using namespace std;
int read(){
	int ans=0;
	char ch=getchar();
	while(ch<'0'||ch>'9')
		ch=getchar();
	while(ch<='9'&&ch>='0'){
		ans=ans*10+ch-'0';
		ch=getchar();
	}
	return ans;
}
const long long INF=1ll<<40;
int n,m,en,w[100005],vet[200005],Next[200005],head[100005],fa[100005],A,B,X,Y;
long long f[100005][2],g[100005][2];
char Type[3];
void addedge(int u,int v){
	Next[++en]=head[u];
	head[u]=en;
	vet[en]=v;
}
void dfs1(int u){
	f[u][0]=0;
	f[u][1]=w[u];
	for(int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if(v!=fa[u]){
			fa[v]=u;
			dfs1(v);
			f[u][0]+=f[v][1];
			f[u][1]+=min(f[v][0],f[v][1]);
		}
	}
	if(A==u)
		f[u][X^1]=INF;
	if(B==u)
		f[u][Y^1]=INF;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();
	m=read();
	scanf("%s",Type);
	for(int i=1;i<=n;i++)
		w[i]=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addedge(x,y);
		addedge(y,x);
	}
	if(n<=2000&&m<=2000){
		dfs1(1);
		while(m--){
			A=read();
			X=read();
			B=read();
			Y=read();
			if(X==0&&Y==0&&(fa[B]==A||fa[A]==B))
				puts("-1");
			else{
				dfs1(1);
				printf("%lld\n",min(f[1][0],f[1][1]));
			}
		}
	}
	else
	if(Type[0]=='A'&&Type[1]=='1'){
		f[1][1]=w[1];
		f[1][0]=INF;
		for(int i=2;i<=n;i++){
			f[i][0]=f[i-1][1];
			f[i][1]=min(f[i-1][0],f[i-1][1])+w[i];
		}
		g[n][1]=w[n];
		g[n][0]=0;
		for(int i=n-1;i>1;i--){
			g[i][0]=g[i+1][1];
			g[i][1]=min(g[i+1][0],g[i+1][1])+w[i];
		}
		while(m--){
			A=read();
			X=read();
			B=read();
			Y=read();
			if(Y==0)
				printf("%lld\n",g[B][0]+f[B-1][1]);
			else
				printf("%lld\n",g[B][1]+min(f[B-1][0],f[B-1][1]));
		}
	}
	else
	if(Type[0]=='A'&&Type[1]=='2'){
		f[1][1]=w[1];
		f[1][0]=0;
		for(int i=2;i<=n;i++){
			f[i][0]=f[i-1][1];
			f[i][1]=min(f[i-1][0],f[i-1][1])+w[i];
		}
		g[n][1]=w[n];
		g[n][0]=0;
		for(int i=n-1;i>=1;i--){
			g[i][0]=g[i+1][1];
			g[i][1]=min(g[i+1][0],g[i+1][1])+w[i];
		}
		while(m--){
			A=read();
			X=read();
			B=read();
			Y=read();
			if(A>B){
				swap(A,B);
				swap(X,Y);
			}
			if(X==0&&Y==0)
				puts("-1");
			else
				printf("%lld\n",f[A][X]+g[B][Y]);
		}
	}
	return 0;
}
