#include<bits/stdc++.h>
#define M 5005
using namespace std;
bool mm1;
int n,m;
struct P1{
	int ct;
	vector<int>G[M];
	void dfs(int x,int f){
		ct++;
		printf("%d",x);
		if(ct!=n)printf(" ");
		for(int i=0;i<(int)G[x].size();i++){
			int u=G[x][i];
			if(u==f)continue;
			dfs(u,x);	
		}
	}
	void solve(){
		ct=0;
		for(int i=1;i<=n;i++)G[i].clear();
		for(int i=1,a,b;i<=m;i++){
			scanf("%d%d",&a,&b);
			G[a].push_back(b);
			G[b].push_back(a);
		}
		for(int i=1;i<=n;i++)sort(G[i].begin(),G[i].end());
		dfs(1,0);
	}
}p1;
struct P2{
	struct node{
		int to,id;
		bool operator < (const node& res) const{//!!!!
			return to<res.to;
		}
	};
	vector<node>G[M];
	int fa[M],S,T,edg[M];
	int Q[M],qcnt;
	int R[M],rcnt;
	int Ans[M];
	int getfa(int x){return fa[x]==x?x:fa[x]=getfa(fa[x]);}
	void dfs(int x,int f){
		fa[x]=f;
		for(int i=0;i<(int)G[x].size();i++){
			int u=G[x][i].to,id=G[x][i].id;
			if(u==f)continue;
			edg[u]=id;
			dfs(u,x);
		}
	}
	void redfs(int x,int f,int lim){
		R[++rcnt]=x;
		for(int i=0;i<(int)G[x].size();i++){
			int u=G[x][i].to,id=G[x][i].id;
			if(u==f||id==lim)continue;
			redfs(u,x,lim);
		}
	}
	void solve(){
		Ans[1]=2e9;
		for(int i=1;i<=n;i++){fa[i]=i;G[i].clear();}
		for(int i=1,a,b;i<=m;i++){
			scanf("%d%d",&a,&b);
			if(getfa(a)!=getfa(b)){
				fa[getfa(a)]=getfa(b);
				G[a].push_back((node){b,i});
				G[b].push_back((node){a,i});
			}
			else {S=a;T=b;}
		}
		dfs(S,0);
		qcnt=0;
		int x=T;
		while(x!=S){
			Q[++qcnt]=edg[x];
			x=fa[x];	
		}
		G[S].push_back((node){T,m+1});
		G[T].push_back((node){S,m+1});
		for(int i=1;i<=n;i++)sort(G[i].begin(),G[i].end());
		Q[++qcnt]=m+1;
		for(int i=1;i<=qcnt;i++){
			rcnt=0;
			redfs(1,0,Q[i]);
			bool fl=0;
			for(int j=1;j<=n;j++){
				if(Ans[j]>R[j]){fl=1;break;}
				if(Ans[j]<R[j]){fl=0;break;}
			}
			if(fl){
				for(int j=1;j<=n;j++)Ans[j]=R[j];
			}
		}
		for(int i=1;i<=n;i++){
			printf("%d",Ans[i]);
			if(i!=n)printf(" ");
		}
	}
}p2;
bool mm2;
int main(){//file memory mod data 
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
//	printf("%lf",(&mm2-&mm1)/1024.0/1024);
	cin>>n>>m;
	if(m==n-1)p1.solve();
	else p2.solve();
	return 0;	
}
