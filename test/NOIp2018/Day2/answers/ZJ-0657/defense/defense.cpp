#include<bits/stdc++.h>
#define M 100005
#define LL long long
#define clr(x,y) memset(x,y,sizeof(x)) 
using namespace std;
int h[M],tot;
struct edge{
	int nxt,to;	
}G[M<<1];
void Add(int a,int b){
	G[++tot]=(edge){h[a],b};
	h[a]=tot;	
}
int n,m,P[M]; 
char S[15];
struct P44{
	int dp[2][2005],a,ax,b,bx;//是否驻扎军队，花费 
	void dfs(int x,int f){
		bool fl=0;
		for(int i=h[x];i;i=G[i].nxt){
			int u=G[i].to;
			if(u==f)continue;
			dfs(u,x);
			dp[0][x]+=dp[1][u];
			if(min(dp[0][u],dp[1][u])==1e9){dp[1][x]=1e9;fl=1;}
			if(!fl)dp[1][x]+=min(dp[0][u],dp[1][u]);
		}
		if(!fl)dp[1][x]+=P[x];
		if(a==x)dp[!ax][x]=1e9;
		if(b==x)dp[!bx][x]=1e9;
	}
	void solve(){
		for(int i=1;i<=m;i++){
			clr(dp,0);
			scanf("%d%d%d%d",&a,&ax,&b,&bx);
			dfs(1,0); 
			if(min(dp[0][1],dp[1][1])==1e9)puts("-1");
			else printf("%d\n",min(dp[0][1],dp[1][1]));
		}
	}
}p44;
int main(){//file memory mod data 
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,S+1);
	for(int i=1;i<=n;i++)scanf("%d",&P[i]);
	for(int i=1,a,b;i<n;i++){
		scanf("%d%d",&a,&b);
		Add(a,b);Add(b,a);
	}
	p44.solve(); 
	return 0;
}
