#include <fstream>
#include <vector>
#include <algorithm>
#include <cstring>

const int N = 5003;
std::vector <int> adj[N];
bool vis[N];

int ban_u, ban_v;
inline bool banned(const int u, const int v)
{
	return (u == ban_u && v == ban_v) || (u == ban_v && v == ban_u);
}

std::vector<int> ans0, ans1;
bool is_better()
{
	const int size = ans0.size();
	for (int i = 0; i < size; ++i)
		if (ans1[i] != ans0[i])
			return ans1[i] < ans0[i];
	return false;
}

int cnt_circle;
int circle[N];
int in_stack[N];
bool find_circle(const int u, const int dep, const int fr)
{
	vis[u] = 1;
	in_stack[dep] = u;
	const std::vector <int> & adj_u = adj[u];
	for (int i = adj_u.size() - 1; i >= 0; --i)
	{
		const int v = adj_u[i];
		if (v == fr)
			continue;
		if (vis[v])
		{
			circle[0] = u;
			do
			{
				++cnt_circle;
				circle[cnt_circle] = in_stack[dep - cnt_circle];
			}
			while (circle[cnt_circle] != v);
			circle[++cnt_circle] = u;
			return true;
		}
		else
			if (find_circle(v, dep + 1, u))
				return true;
	}
	return false;
}
void dfs(const int u, const int fr)
{
	ans1.push_back(u);
	const std::vector <int> & adj_u = adj[u];
	const int size = adj_u.size();
	for (int i = 0; i < size; ++i)
	{
		const int v = adj_u[i];
		if (!banned(u, v) && v != fr)
			dfs(v, u);
	}
}

int main()
{
	std::ifstream cin("travel.in");
	std::ofstream cout("travel.out");
	int n, m;
	cin >> n >> m;
	for (int i = 0; i < m; ++i)
	{
		int u, v;
		cin >> u >> v;
		adj[u].push_back(v);
		adj[v].push_back(u);
	}
	for (int i = 1; i <= n; ++i)
		std::sort(adj[i].begin(), adj[i].end());
	if (n == m)
	{
		find_circle(1, 0, 0);
		for (int i = 0; i < cnt_circle; ++i)
		{
			ban_u = circle[i];
			ban_v = circle[i + 1];
			dfs(1, 0);
			if (ans0.size() == 0 || is_better())
			{
				ans0.swap(ans1);
				ans1.clear();
			}
		}
	}
	else
	{
		dfs(1, 0);
		ans0.swap(ans1);
	}
	for (int i = 0; i < n; ++i)
		cout << ans0[i] << ' ';
	cout << std::endl;
	return 0;
}
