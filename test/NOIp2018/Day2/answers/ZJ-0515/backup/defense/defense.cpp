#include <fstream>
#include <algorithm>
#include <set>
using std::min;
typedef long long ll;
const ll INF = 2e11;

const int N = 100005;
int p[N];

struct edge
{
	int v, nxt;
	edge() {}
	edge(const int _v, const int _nxt) : v(_v), nxt(_nxt) {}
} e[N * 2];
int cnt_e, head[N];
inline void add_edge(const int u, const int v)
{
	e[++cnt_e] = edge(v, head[u]);
	head[u] = cnt_e;
	e[++cnt_e] = edge(u, head[v]);
	head[v] = cnt_e;
}

ll ans[N][2];
int a, x, b, y;
int fa[N], dep[N];
void dfs(const int u, const int fr, const int d)
{
	fa[u] = fr;
	dep[u] = d;
	ll ans[2] = {0, p[u]};
	for (int it = head[u]; it != 0; it = e[it].nxt)
	{
		const int v = e[it].v;
		if (v != fr)
		{
			dfs(v, u, d + 1);
			ans[1] += min(::ans[v][0], ::ans[v][1]);
			ans[0] += ::ans[v][1];
		}
	}
	::ans[u][0] = ans[0];
	::ans[u][1] = ans[1];
}

namespace JUMP
{
	struct ans
	{
		int u;
		ll a0, a1;
		ans() {}
		ans(const int _u, const ll _0, const ll _1)
			: u(_u), a0(_0), a1(_1) {}
	};
	void jump(ans &orig)
	{
		int &u = orig.u;
		ll &a0 = orig.a0, &a1 = orig.a1;
		const ll f = fa[u];
		ll _0 = ::ans[f][0] - ::ans[u][1] + a1;
		ll _1 = ::ans[f][1] - min(::ans[u][0], ::ans[u][1]) + min(a0, a1);
		u = f;
		a0 = _0;
		a1 = _1;
	}
}
ll solve()
{
	JUMP::ans aa, bb;
	{
		ll ans_a[2] = {ans[a][0], ans[a][1]};
		ans_a[1 - x] = INF;
		aa = JUMP::ans(a, ans_a[0], ans_a[1]);
		ll ans_b[2] = {ans[b][0], ans[b][1]};
		ans_b[1 - y] = INF;
		bb = JUMP::ans(b, ans_b[0], ans_b[1]);
	}
	using JUMP::jump;
	while (dep[aa.u] > dep[bb.u])
		jump(aa);
	while (dep[bb.u] > dep[aa.u])
		jump(bb);

	while (aa.u != bb.u)
	{
		jump(aa);
		jump(bb);
	}
	aa.a0 += bb.a0 - ans[bb.u][0];
	aa.a1 += bb.a1 - ans[bb.u][1];
	while (aa.u != 1)
		jump(aa);
	return min(aa.a0, aa.a1);
}

std::set <int> adj[N];

int main()
{
	std::ifstream cin("defense.in");
	std::ofstream cout("defense.out");
	int n, m;
	cin >> n >> m;
	char type_1, type_2;
	cin >> std::ws >> type_1 >> type_2;
	for (int i = 1; i <= n; ++i)
		cin >> p[i];
	for (int i = 1; i < n; ++i)
	{
		int u, v;
		cin >> u >> v;
		adj[u].insert(v);
		adj[v].insert(u);
		add_edge(u, v);
	}
	dfs(1, 1, 0);
	for (int i = 1; i <= m; ++i)
	{
		cin >> a >> x >> b >> y;
		if (x == 0 && y == 0 && adj[a].count(b) == 1)
			cout << "-1";
		else
			cout << solve();
		cout << std::endl;
	}
	return 0;
}
