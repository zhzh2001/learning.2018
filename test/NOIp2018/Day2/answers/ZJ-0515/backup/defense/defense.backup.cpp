#include <fstream>
#include <algorithm>
#include <set>
using std::min;
typedef long long ll;
const ll INF = 2e11;

const int N = 100005;
int p[N];

struct edge
{
	int v, nxt;
	edge() {}
	edge(const int _v, const int _nxt) : v(_v), nxt(_nxt) {}
} e[N * 2];
int cnt_e, head[N];
inline void add_edge(const int u, const int v)
{
	e[++cnt_e] = edge(v, head[u]);
	head[u] = cnt_e;
	e[++cnt_e] = edge(u, head[v]);
	head[v] = cnt_e;
}

//ll ans[N][2];
int a, x, b, y;
struct __ans
{
	ll a0, a1;
	__ans(const ll _0, const ll _1) : a0(_0), a1(_1) {}
};
__ans dfs(const int u, const int fr)
{
	ll ans[2] = {0, p[u]};
	for (int it = head[u]; it != 0; it = e[it].nxt)
	{
		const int v = e[it].v;
		if (v != fr)
		{
			const __ans tmp = dfs(v, u);
			ans[1] += min(tmp.a0, tmp.a1);
			ans[0] += tmp.a1;
		}
	}
	if (u == a)
		ans[1 - x] = INF;
	if (u == b)
		ans[1 - y] = INF;
	return __ans(ans[0], ans[1]);
}

ll solve()
{
	const __ans tmp = dfs(1, 0);
	return min(tmp.a0, tmp.a1);
}

std::set <int> adj[N];

int main()
{
	std::ifstream cin("defense.in");
	std::ofstream cout("defense.out");
	int n, m;
	cin >> n >> m;
	char type_1, type_2;
	cin >> std::ws >> type_1 >> type_2;
	for (int i = 1; i <= n; ++i)
		cin >> p[i];
	for (int i = 1; i < n; ++i)
	{
		int u, v;
		cin >> u >> v;
		adj[u].insert(v);
		adj[v].insert(u);
		add_edge(u, v);
	}
	for (int i = 1; i <= m; ++i)
	{
		cin >> a >> x >> b >> y;
		if (x == 0 && y == 0 && adj[a].count(b) == 1)
			cout << "-1";
		else
			cout << solve();
		cout << std::endl;
	}
	return 0;
}
