#include <fstream>
const int MOD = 1000000007;
int main()
{
	std::ifstream cin("game.in");
	std::ofstream cout("game.out");
	int n, m;
	cin >> n >> m;
	long long ans = 2;
	int n1 = 0, m1 = 0, n2 = 0, m2 = 0;
	for (int i = 1; i < n + m - 1; ++i)
	{
		++m1;
		++n2;
		if (m1 >= m)
		{
			--m1;
			++n1;
		}
		if (n2 >= n)
		{
			--n2;
			++m2;
		}
		ans = ans * (m1 - m2 + 2) % MOD;
	}
	cout << ans << std::endl;
	return 0;
}
/*
爆零了。
再见了。
*/
