#include <bits/stdc++.h>
using namespace std;

int n, m, a[1010][1010], Ans, PP, val[2020];

bool Check(int x, int y, int lim) {
  for(int i = 0; i < n + m - 1; i++)
    val[i] = -1;
  for(int i = x; i < n; i++)
    for(int j = y; j < m; j++) {
      if(x + y >= lim) break;
      if(!~val[i + j])
	val[i + j] = a[i][j];
      else if(val[i + j] != a[i][j])
	return 0;
    }
  return 1;
}

void Dfs(int x) {
  bool flg = 1;
  for(int i = 1; i < m && i < x && flg; i++) {
    if(a[0][i] == a[1][i - 1]) {
      flg &= Check(1, i, x);
    }
  }
  for(int i = 2; i < n && i < x && flg; i++) {
    if(a[i][0] == a[i - 1][1]) {
      flg &= Check(i, 1, x);
    }
  }
  if(!flg) return;
  if(x == n + m - 1) {
    Ans ++;
    return;
  }
  vector<pair<int, int> > tmp;
  for(int i = 0; i < n && i <= x; i++) {
    int j = x - i;
    if(j >= m) continue;
    tmp.push_back(make_pair(i, j));
    a[i][j] = 1;
  }
  Dfs(x + 1);
  for(int i = 0; i < tmp.size(); i++) {
    a[tmp[i].first][tmp[i].second] = 0;
    Dfs(x + 1);
  }
}

int main(void) {
  putchar('{');
  for(n = 1; n <= 8; n++) {
    putchar('{');
    for(m = 1; m <= 8; m++) {
      Ans = 0;
      Dfs(0);
      printf("%d", Ans);
      putchar(m < 8 ? ',' : '}');
    }
    putchar(n < 8 ? ',' : '}');
  }
  return 0;
}
