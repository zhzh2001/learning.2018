#include <bits/stdc++.h>
using namespace std;

const int N = 10555;

pair<int, int> st[N];
int top, vis[N], tot, fi[N], ne[N], la[N], to[N], In[N], Now[N];
int Cnt, n, m, Ans[N], e[N][2], ban[N], dep[N];
vector<pair<int, int> > g[N];

void Add(int x, int y) {
  to[++tot] = y;
  !fi[x] ? fi[x] = tot : ne[la[x]] = tot;
  la[x] = tot;
}

void GetLoop(int x, int fa, int eid) {
  dep[x] = dep[fa] + 1;
  if(top >= 0)
    st[++top] = make_pair(x, eid);
  vis[x] = 1;
  for(int i = 0; i < g[x].size(); i++) {
    int v = g[x][i].first;
    if(v == fa) continue;
    if(vis[v]) {
      if(dep[v] > dep[x]) continue;
      In[g[x][i].second] = 1;
      while(st[top].first != v) {
	In[st[top--].second] = 1;
      }
      continue;
    }
    GetLoop(v, x, g[x][i].second);
  }
  --top;
}

void Solve(int x) {
  vis[x] = 1;
  Now[++Cnt] = x;
  for(int i = fi[x]; i; i = ne[i]) {
    int v = to[i];
    if(vis[v] || ban[i]) continue;
    Solve(v);
  }
}

void Upd(int *a) {
  if(!Ans[1]) {
    for(int i = 1; i <= n; i++)
      Ans[i] = a[i];
  } else {
    bool flg = 0;
    for(int i = 1; i <= n; i++) {
      if(a[i] != Ans[i]) {
	flg = a[i] < Ans[i];
	break;
      }
    }
    if(flg) {
      for(int i = 1; i <= n; i++)
	Ans[i] = a[i];
    }
  }
}

int main(void) {
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);
  scanf("%d%d", &n, &m);
  tot = 1;
  for(int i = 1; i <= m; i++) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].push_back(make_pair(v, i));
    g[v].push_back(make_pair(u, i));
  }
  for(int i = 1; i <= n; i++) {
    sort(g[i].begin(), g[i].end());
    for(int j = 0; j < g[i].size(); j++) {
      int v = g[i][j].first, id = g[i][j].second;
      Add(i, v);
      e[id][!e[id][0] ? 0 : 1] = tot;
    }
  }
  if(m == n - 1) {
    Cnt = 0;
    memset(vis, 0, sizeof vis);
    Solve(1);
    
    Upd(Now);
  } else {
    memset(vis, 0, sizeof vis);
    GetLoop(1, 0, 0);
    for(int i = 1; i <= m; i++) {
      if(!In[i]) continue;
      
      ban[e[i][0]] = ban[e[i][1]] = 1;
      Cnt = 0;
      memset(vis, 0, sizeof vis);
      Solve(1);
      ban[e[i][0]] = ban[e[i][1]] = 0;

      Upd(Now);
    }
  }
  for(int i = 1; i <= n; i++)
    printf("%d ", Ans[i]);
  puts("");
  return 0;
}
