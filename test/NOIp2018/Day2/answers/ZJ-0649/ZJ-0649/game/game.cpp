#include <bits/stdc++.h>
using namespace std;

const int MOD = 1e9 + 7, N = 1111111;

namespace {
  inline int Add(int x, int y) { return (x += y) >= MOD ? x - MOD : x; }
  inline int Sub(int x, int y) { return (x -= y) < 0 ? x + MOD : x; }
  inline int Mul(int x, int y) { return 1LL * x * y % MOD; }
}

int n, m;

namespace S__ {

  void Rmain(void) {
    int Ans = 1;
    for(int i = 1; i <= m; i++)
      Ans = Mul(Ans, 2);
    printf("%d\n", Ans);
  }
  
}

namespace X__ {
  
  void Rmain(void) {
    int Ans = 1;
    for(int i = 1; i < m; i++)
      Ans = Mul(Ans, 3);
    Ans = Mul(Ans, 4);
    printf("%d\n", Ans);
  }
  
}

namespace Y__ {

  int pw[N];
  
  int Calc(int x) {
    if(x == 1) return 6;
    else return Mul(16, pw[x - 2]);
  }
  
  void Rmain(void) {
    pw[0] = 1;
    for(int i = 1; i <= m; i++)
      pw[i] = Mul(pw[i - 1], 3);
    int Ans = 6;
    for(int i = 1; i < m; i++) {
      Ans = Add(Ans, Mul(2 + (i > 1), Calc(m - i)));
    }
    Ans = Mul(Ans, 2);
    printf("%d\n", Ans);
  }
  
}

namespace Z__ {

  const int Ans[8][8] = {{2,4,8,16,32,64,128,256},{4,12,36,108,324,972,2916,8748},{8,36,112,336,1008,3024,9072,27216},{16,108,336,912,2688,8064,24192,72576},{32,324,1008,2688,7136,21312,63936,191808},{64,972,3024,8064,21312,56768,170112,510336},{128,2916,9072,24192,63936,170112,453504,1360128},{256,8748,27216,72576,191808,510336,1360128,3626752}};
  
  void Rmain(void) {
    if(n > 8 || m > 8) cout << 19260817 << endl;
    else cout << Ans[n - 1][m - 1] << endl;
  }
  
}

int main(void) {
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);
  scanf("%d%d", &n, &m);
  if(n > m) swap(n, m);
  if(n == 1) {
    S__::Rmain();
    return 0;
  }
  if(n == 2) {
    X__::Rmain();
    return 0;
  }
  if(n == 3) {
    Y__::Rmain();
    return 0;
  }
  Z__::Rmain();
  return 0;
}
