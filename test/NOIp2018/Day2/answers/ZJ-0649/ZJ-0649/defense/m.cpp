#include <bits/stdc++.h>
using namespace std;

const int MOD = 1e5;

int main(void) {
  freopen("defense.in", "w", stdout);
  srand(clock());
  int n = 1000, m = 1000;
  printf("%d %d B1\n", n, m);
  for(int i = 1; i <= n; i++)
    printf("%d ", rand() % MOD + 1);
  puts("");
  for(int i = 1; i < n; i++)
    printf("%d %d\n", i + 1, rand() % i + 1);
  for(int i = 1; i <= m; i++) {
    printf("%d %d %d %d\n", 1, 1, rand() % n + 1, rand() % 2);
  }
}
