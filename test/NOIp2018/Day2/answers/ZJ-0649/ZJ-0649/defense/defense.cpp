#include <bits/stdc++.h>
using namespace std;

const int N = 101010;
const long long INF = 1e12;

long long f[N][2];
int n, m, dep[N], fa[N], p[N];
char type[10];
vector<int> g[N];
pair<int, int> BAN[2];

void Dfs(int x) {
  dep[x] = dep[fa[x]] + 1;
  f[x][0] = 0;
  f[x][1] = p[x];
  for(int i = 0; i < g[x].size(); i++) {
    int v = g[x][i];
    if(v == fa[x]) continue;
    fa[v] = x;
    Dfs(v);
    f[x][0] += f[v][1];
    f[x][1] += min(f[v][0], f[v][1]);
  }
  for(int i = 0; i < 2; i++)
    if(BAN[i].first == x)
      f[x][BAN[i].second ^ 1] = INF;
}

namespace Y__ {

  void Solve(int u, int x, int v, int y) {
    BAN[0] = make_pair(u, x);
    BAN[1] = make_pair(v, y);
    Dfs(1);
    long long Ans = min(f[1][0], f[1][1]);
    if(Ans >= 1e12)
      Ans = -1;
    printf("%lld\n", Ans); 
  }
  
}

namespace X__ {

  long long pre[N][2];
  
  void Solve(int u, int x, int y) {
    int pu = u;
    pre[u][0] = f[u][0];
    pre[u][1] = f[u][1];
    f[u][!x] = INF;
    while(u != 1) {
      int fu = fa[u];
      pre[fu][0] = f[fu][0];
      pre[fu][1] = f[fu][1];
      f[fu][0] -= pre[u][1];
      f[fu][0] += f[u][1];
      f[fu][1] -= min(pre[u][0], pre[u][1]);
      f[fu][1] += min(f[u][0], f[u][1]);
      u = fu;
    }
    printf("%lld\n", f[u][y] >= 1e12 ? -1 : f[u][y]);
    u = pu;
    f[u][0] = pre[u][0];
    f[u][1] = pre[u][1];
    while(u != 1) {
      int fu = fa[u];
      f[fu][0] = pre[fu][0];
      f[fu][1] = pre[fu][1];
      u = fu;
    }
  }
  
}

int main(void) {
  freopen("defense.in", "r", stdin);
  freopen("defense.out", "w", stdout);
  scanf("%d%d%s", &n, &m, type);
  for(int i = 1; i <= n; i++) {
    scanf("%d", &p[i]);
  }
  for(int i = 1; i < n; i++) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].push_back(v);
    g[v].push_back(u);
  }
  Dfs(1);
  while(m--) {
    int u, x, v, y;
    scanf("%d%d%d%d", &u, &x, &v, &y);
    if(type[0] == 'B') X__::Solve(v, y, x);
    else Y__::Solve(u, x, v, y);
  }
  return 0;
}
