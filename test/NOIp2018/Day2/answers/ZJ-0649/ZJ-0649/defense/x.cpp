#include <bits/stdc++.h>
using namespace std;

const int N = 101010;
const long long INF = 1e12;

long long f[N][2];
int n, m, dep[N], fa[N], p[N];
char type[10];
vector<int> g[N];
set<pair<int, int> > st;
pair<int, int> BAN[2];

void Dfs(int x) {
  dep[x] = dep[fa[x]] + 1;
  f[x][0] = 0;
  f[x][1] = p[x];
  for(int i = 0; i < g[x].size(); i++) {
    int v = g[x][i];
    if(v == fa[x]) continue;
    fa[v] = x;
    Dfs(v);
    f[x][0] += f[v][1];
    f[x][1] += min(f[v][0], f[v][1]);
  }
  for(int i = 0; i < 2; i++)
    if(BAN[i].first == x)
      f[x][BAN[i].second ^ 1] = INF;
}

namespace Y__ {

  void Solve(int u, int x, int v, int y) {
    BAN[0] = make_pair(u, x);
    BAN[1] = make_pair(v, y);
    Dfs(1);
    long long Ans = min(f[1][0], f[1][1]);
    if(Ans >= 1e12) Ans = -1;
    printf("%lld\n", Ans); 
  }
  
}

int main(void) {
  freopen("defense.in", "r", stdin);
  freopen("defense.ans", "w", stdout);
  scanf("%d%d%s", &n, &m, type);
  for(int i = 1; i <= n; i++) {
    scanf("%d", &p[i]);
  }
  for(int i = 1; i < n; i++) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].push_back(v);
    g[v].push_back(u);
  }
  Dfs(1);
  while(m--) {
    int u, x, v, y;
    scanf("%d%d%d%d", &u, &x, &v, &y);
    Y__::Solve(u, x, v, y);
  }
}
