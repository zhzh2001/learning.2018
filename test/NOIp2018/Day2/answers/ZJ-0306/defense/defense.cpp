#include<bits/stdc++.h>
using namespace std;
const int N=1e5+7;
bool used[N];
int n,m,a,x,b,y,tot,f[N];
long long dp[N][2],val[N];
char s[N];
struct edge{int a,b,c;}edg[N<<1];
inline int read()
{
	int num=0; char g=getchar();
	while(g<48||57<g) g=getchar();
	while(47<g&&g<58) num=(num<<1)+(num<<3)+g-48,g=getchar();
	return num;
}
inline long long Min(long long a,long long b) {if(a<b) return a; return b;}
inline void addedge(int a,int b) {tot++,edg[tot].a=a,edg[tot].b=b,edg[tot].c=f[a],f[a]=tot;}
inline void find(int u)
{
	bool ch[2]={false,false};
	used[u]=true,dp[u][0]=dp[u][1]=0;
	for(int i=f[u];i>0;i=edg[i].c)
		if(!used[edg[i].b])
		{
			find(edg[i].b);
			if(dp[edg[i].b][1]==-1) ch[0]=true;
			if(dp[edg[i].b][0]==-1&&dp[edg[i].b][1]==-1) ch[1]=true;
			if(dp[edg[i].b][0]==-1) dp[u][1]+=dp[edg[i].b][1];
			else if(dp[edg[i].b][1]==-1) dp[u][1]+=dp[edg[i].b][0];
			else dp[u][1]+=Min(dp[edg[i].b][0],dp[edg[i].b][1]);
			dp[u][0]+=dp[edg[i].b][1];
		}
	dp[u][1]+=val[u];
	if(ch[0]) dp[u][0]=-1;
	if(ch[1]) dp[u][1]=-1;
	if(u==a) dp[u][x^1]=-1;
	if(u==b) dp[u][y^1]=-1;
//	cout<<u<<' '<<dp[u][0]<<' '<<dp[u][1]<<'x'<<endl;
	used[u]=false;
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(); scanf("%s",s+1);
	for(int i=1;i<=n;i++) val[i]=read();
	for(int i=1;i<n;i++) a=read(),b=read(),addedge(a,b),addedge(b,a);
	if(n<=2000&&m<=2000)
	{
		for(int i=1;i<=m;i++)
		{
			a=read(),x=read(),b=read(),y=read();
			find(1);
			if(dp[1][0]==-1&&dp[1][1]==-1) putchar('-'),putchar('1'),putchar('\n');
			else if(dp[1][0]==-1) printf("%d\n",dp[1][1]);
			else if(dp[1][1]==-1) printf("%d\n",dp[1][0]);
			else printf("%d\n",Min(dp[1][0],dp[1][1]));
		}
	}
	return 0;
}
