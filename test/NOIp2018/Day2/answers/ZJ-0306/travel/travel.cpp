#include<bits/stdc++.h>
using namespace std;
const int N=5e3+7;
bool flag[N],used[N],res=false,getit;
int n,m,e,p,tot,siz,opt,fs,fq,f[N],fa[N],kk[N],que[N],ans[N],dep[N];
struct edge{int a,b,c;}edg[N<<1],ed[N<<1];
inline int read()
{
	int num=0; char g=getchar();
	while(g<48||57<g) g=getchar();
	while(47<g&&g<58) num=(num<<1)+(num<<3)+g-48,g=getchar();
	return num;
}
inline int find(int u) {if(f[u]==u) return f[u]; return f[u]=find(f[u]);}
inline void addedge(int a,int b) {tot++,ed[tot].a=a,ed[tot].b=b,ed[tot].c=f[a],f[a]=tot;}
bool cmp(edge a,edge b) {return a.b<b.b;}
inline void search(int u)
{
	used[u]=true,ans[++siz]=u;
	for(int i=f[u];i>0;i=ed[i].c)
	{
		if(!used[ed[i].b])
		{
			if(u==e&&ed[i].b==p) {fq=u,fs=ed[i].b; continue;}
			if(u==p&&ed[i].b==e) {fq=u,fs=ed[i].b; continue;}
			dep[ed[i].b]=dep[u]+1,fa[ed[i].b]=u,search(ed[i].b);
		}
	}
}
inline void research(int u)
{
	que[++siz]=u,used[u]=true;
	for(int i=f[u];i>0;i=ed[i].c)
	{
		if(!used[ed[i].b])
		{
			if(u==e&&ed[i].b==p) res=true;
			if(u==p&&ed[i].b==e) res=true;
			if(ed[i].b==fa[u]) continue;
			if(ed[i].b==kk[opt]) continue;
			research(ed[i].b);
			if(u==e&&ed[i].b==p) res=false;
			if(u==p&&ed[i].b==e) res=false;
		}
	}
	if(res&&(!used[fa[u]])&&dep[fa[u]]>=opt) research(fa[u]);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=n;i++) f[i]=i;
	for(int i=1;i<=m;i++)
	{
		int a=read(),b=read(),s=find(a),t=find(b);
		if(s==t) e=a,p=b; f[s]=f[t];
		tot++,edg[tot].a=a,edg[tot].b=b,tot++,edg[tot].a=b,edg[tot].b=a;
	}
	sort(edg+1,edg+tot+1,cmp),memset(f,0,sizeof(f)),tot=0;
	for(int i=(m<<1);i>0;i--) addedge(edg[i].a,edg[i].b);
	dep[1]=1,search(1);
	if(m==n-1)
	{
		for(int x=1;x<=n;x++) printf("%d ",ans[x]);
		return 0;
	}
	fs=e,fq=p;
	int u=fs;
	while(dep[u]) kk[dep[u]]=u,u=fa[u];
	for(opt=1;opt<=dep[fs];opt++)
	{
		for(int i=1;i<=n;i++) que[i]=n;
		memset(used,0,sizeof(used));
		siz=0,research(1),getit=false;
		for(int x=1;x<=n;x++)
		{
			if(ans[x]>que[x])
			{
				getit=true; break;
			}
			if(ans[x]<que[x])
			{
				getit=false; break;
			}
		}
		if(getit)
		{
			for(int x=1;x<=n;x++) ans[x]=que[x];
		}
		
	}
	fs=p,fq=e;
	u=fs;
//	cout<<fs<<' '<<fq<<endl;
	while(dep[u]) kk[dep[u]]=u,u=fa[u];
	for(opt=1;opt<=dep[fs];opt++)
	{
		for(int i=1;i<=n;i++) que[i]=n;
		memset(used,0,sizeof(used));
		siz=0,research(1),getit=false;
		for(int x=1;x<=n;x++)
		{
			if(ans[x]>que[x])
			{
				getit=true; break;
			}
			if(ans[x]<que[x])
			{
				getit=false; break;
			}
		}
		if(getit)
		{
			for(int x=1;x<=n;x++) ans[x]=que[x];
		}
	}
	for(int x=1;x<=n;x++) printf("%d ",ans[x]);
	return 0;
}
//8 8
//1 4
//3 4
//2 3
//3 6
//4 5
//5 7
//5 8
//1 2
