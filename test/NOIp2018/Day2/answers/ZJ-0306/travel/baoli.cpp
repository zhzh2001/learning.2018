#include<bits/stdc++.h>
using namespace std;
const int N=5e3+7;
bool flag[N],used[N],res=false,getit;
int n,m,e,p,tot,siz,opt,fs,fq,f[N],fa[N],que[N],ans[N],dep[N],ss[N];
struct edge{int a,b,c;}edg[N<<1],ed[N<<1];
inline int read()
{
	int num=0; char g=getchar();
	while(g<48||57<g) g=getchar();
	while(47<g&&g<58) num=(num<<1)+(num<<3)+g-48,g=getchar();
	return num;
}
inline void ffind(int u)
{
	que[++siz]=u,used[u]=true;
	for(int i=1;i<=m;i++)
	{
		if(ed[i].a==u)
		{
			if(!used[ed[i].b]) ffind(ed[i].b);
		}
		if(ed[i].b==u)
		{
			if(!used[ed[i].a]) ffind(ed[i].a);
		}
	}
}
inline void find(int u)
{
	if(u==m+1)
	{
		for(int i=1;i<=n;i++) ed[i]=edg[ss[i]];
		memset(used,0,sizeof(used));
		siz=0,ffind(1);
		for(int x=1;x<=n;x++)
		{
			if(ans[x]>que[x])
			{
				getit=true; break;
			}
			if(ans[x]<que[x])
			{
				getit=false; break;
			}
		}
		if(getit)
		{
			for(int x=1;x<=n;x++) ans[x]=que[x];
		}
		return;
	}
	for(int x=1;x<=m;x++)
	{
		if(!used[x])
		{
			ss[u]=x,used[x]=true;
			find(u+1);
			used[x]=false;
		}
	}
}
int main()
{
//	freopen("b.txt","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++)
	{
		int a=read(),b=read(); ans[i]=n;
		tot++,edg[tot].a=a,edg[tot].b=b;
	}
	find(1);
	for(int i=1;i<=n;i++) cout<<ans[i]<<' ';
	return 0;
}
//5 5
//1 4
//4 5
//5 3
//3 2
//1 2
