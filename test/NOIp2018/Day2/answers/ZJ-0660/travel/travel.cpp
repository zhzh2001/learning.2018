#include<cstdio>
#include<algorithm>
using namespace std;
const int N=5005,INF=0x3f3f3f3f;
int n,m,a[N][N],c[N],f[N][15],fat[N],s1,s2,sz[N],st,d[N];
bool vis[N];
inline void read(int &x)
{
	x=0; char c=getchar();
	for (; c<'0' || c>'9'; c=getchar());
	for (; c>='0' && c<='9'; x=(x<<3)+(x<<1)+c-'0',c=getchar());
}
inline void dfs(int x,int fa)
{
	//if (vis[x]) return;
	//if (x==st) dfs2(st,x,INF);
	register int i;
	int tot=0;
	printf("%d ",x); //vis[x]=1;
	//printf("%d\n",a[x][0]);
	for (i=1; i<=a[x][0]; ++i)
	{
	  if (a[x][i]==fa || a[x][i]==x) continue;
	  c[++tot]=a[x][i];
	}
	//if (tot==0) return;
	sort(c+1,c+1+tot);
	//unique(c+1,c+1+tot);
	//if (x!=1) return;
	for (i=1; i<=tot; ++i)
	  //printf("%d ",c[i]);
	  dfs(c[i],x);
}
/*
inline int getf(int x)
{
	return fat[x]==x?x:fat[x]=getf(fat[x]);
}
inline void dfs1(int x,int fa) 
{
	register int i,x1,x2;
	for (i=1; i<=a[x][0]; ++i)
	{
		if (a[x][i]==fa) continue;
		x1=getf(x),x2=getf(a[x][i]);
		if (x1!=x2)
		{
			if (sz[x1]>sz[x2]) swap(x1,x2);
			fa[x1]=x2; sz[x2]+=sz[x1];
			f[a[x][i]][0]=x; d[a[x][i]]=d[x]+1;
		}
		else s1=x,s2=a[x][i];
	}
}
inline int lca(int x,int y)
{
	if (d[x]<d[y]) swap(x,y);
	register int i;
	for (i=15; ~i; --i) if (d[f[x][i]]>=d[y]) x=f[x][i];
	if (x==y) return x;
	for (i=15; ~i; --i)
	  if (f[x][i] && f[y][i] && f[x][i]!=f[y][i]) x=f[x][i],y=f[y][i];
	return f[x][0];
}
inline void dfs2(int x,int fa,int las)
{
	register int i;
	int tot=0;
	for (i=1; i<=a[x][0]; ++i)
	{
	  if (a[x][i]==fa || a[x][i]==x) continue;
	  c[++tot]=a[x][i];
	}
	//if (tot==0) return;
	sort(c+1,c+1+tot);
	//unique(c+1,c+1+tot);
	//if (x!=1) return;
	for (i=1; i<=tot; ++i)
	{
	  if (c[i]>las) dfs1(las,x);
	  //printf("%d ",c[i]);
	  dfs2(c[i],x,);
	}
}
*/
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n),read(m); register int i,x,y;
	for (i=1; i<=m; ++i)
	{
		read(x),read(y);
		a[x][++a[x][0]]=y;
		a[y][++a[y][0]]=x;
	}
	dfs(1,0);
	/*if (m==n-1) dfs(1,0);
	else { 
		for (i=1; i<=n; ++i) fat[i]=i,sz[i]=1;
		d[1]=1;
		dfs1(1,0);
		register int j;
		for (j=1; j<=15; ++j)
		  for (i=1; i<=n; ++i)
		    f[i][j]=f[f[i][j-1]][j-1];
		st=lca(s1,s2);
		dfs1(1,0);
	}*/
	return 0;
}
