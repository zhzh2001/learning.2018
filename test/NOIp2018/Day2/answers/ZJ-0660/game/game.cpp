#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=300,mo=1e9+7;
int n,m;
ll f[2][N],ans;
inline void read(int &x)
{
	x=0; char c=getchar();
	for (; c<'0' || c>'9'; c=getchar());
	for (; c>='0' && c<='9'; x=(x<<3)+(x<<1)+c-'0',c=getchar());
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n),read(m);
	register int i,j,k,l;
	for (i=0; i<(1<<n); ++i)
	  f[m&1][i]=1;
	bool bj;
	for (i=m-1; i; --i)
	{
		for (j=0; j<(1<<n); ++j)
		  for (k=0; k<(1<<n); ++k)
		  {
		  	bj=0;
		    for (l=1; l<n; ++l)
		      if (((j>>l)&1)<((k>>(l-1))&1))
		      {
		        bj=1; break;
		      }
		    if (bj) continue;
		    (f[i&1][j]+=f[1-(i&1)][k])%=mo;
		  }
	}
	ans=0;
	for (i=0; i<(1<<n); ++i)
	  (ans+=f[1][i])%=mo;
	printf("%lld\n",ans);
	return 0;
}
