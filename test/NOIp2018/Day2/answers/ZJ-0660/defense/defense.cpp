#include<cstdio>
#include<cstring>
#define ll long long
using namespace std;
const int N=1e5+5;
int n,m,nxt[N*2],to[N*2],hea[N],tot,p[N],a,b,x,y;
char s[5];
ll ans,f[N][2];
bool bj;
inline void read(int &x)
{
	x=0; char c=getchar();
	for (; c<'0' || c>'9'; c=getchar());
	for (; c>='0' && c<='9'; x=(x<<3)+(x<<1)+c-'0',c=getchar());
}
inline void add(int x,int y)
{
	to[++tot]=y; nxt[tot]=hea[x]; hea[x]=tot;
}
inline int min(int x,int y)
{
	if (x==-1) return y;
	if (y==-1) return x;
	return x<y?x:y;
}
inline void doing(int rt,int fa)
{
	
	register int i;
	for (i=hea[rt]; i; i=nxt[i])
	{
		if (to[i]==fa) continue;
		doing(to[i],rt);
		if ((a==rt && x==1) || (b==rt && y==1))
		{
			f[rt][1]+=min(f[to[i]][0],f[to[i]][1]);
		}
		else if ((a==rt && x==0) || (b==rt && y==0))
		{
			if (f[to[i]][1]==-1) {
				bj=1; return;
			}
			f[rt][0]+=f[to[i]][1];
		}
	}
	if ((a==rt && x==1) || (b==rt && y==1)) f[rt][1]+=p[rt],f[rt][0]=-1;
	if ((a==rt && x==0) || (b==rt && y==0)) f[rt][1]=-1;
/*	if (rt==a)
	{
		if (x==1)
		{
			if (f[fa][0]!=-1 && f[fa][1]!=-1)
			  f[rt][1]=min(f[fa][0],f[fa][1])+p[rt];
			else if (f[fa][0]==-1)
				f[rt][1]=f[fa][1]+p[rt];
			else f[rt][1]=f[fa][0]+p[rt];
			f[rt][0]=-1;
		}
		else {
			if (f[fa][0]!=-1 && f[fa][1]!=-1)
			  f[rt][0]=min(f[fa][0],f[fa][1]);
			else if (f[fa][0]==-1)
				f[rt][0]=f[fa][1];
			else {
				bj=1; return;
			}
			f[rt][1]=-1;
		}
	}
	else if (rt==b)
	{
		if (y==1)
		{
			if (f[fa][0]!=-1 && f[fa][1]!=-1)
			  f[rt][1]=min(f[fa][0],f[fa][1])+p[rt];
			else if (f[fa][0]==-1)
				f[rt][1]=f[fa][1]+p[rt];
			else f[rt][1]=f[fa][0]+p[rt];
			f[rt][0]=-1;
		}
		else {
			if (f[fa][0]!=-1 && f[fa][1]!=-1)
			f[rt][0]=min(f[fa][0],f[fa][1]);
			else if (f[fa][0]==-1)
				f[rt][0]=f[fa][1];
			else {
				bj=1; return;
			}
			f[rt][1]=-1;
		}
	}
	*/
	if (bj) return;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n),read(m); scanf("%s",s);
	register int i;
	for (i=1; i<=n; ++i) read(p[i]);
	for (i=1; i<n; ++i)
	{
		read(x); read(y);
		add(x,y); add(y,x);
	}
	for (i=1; i<=m; ++i)
	{
		memset(f,0,sizeof(f));
		read(a),read(x),read(b),read(y);
		bj=0;
		doing(1,0);
		ans=min(f[1][0],f[1][1]);
		if (bj) printf("-1\n");
		else printf("%lld\n",ans);
	}
	return 0;
}
