#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<iostream>
#include<vector>
#define mp make_pair
#define pb push_back
#define fi first
#define se second
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
#define per(i,b,a) for(int i=(b);i>=(a);--i)

inline int read(void){
	int x=0,f=1;char c=getchar();
	for (;!isdigit(c);c=getchar()) f=c=='-'?-1:1;
	for (;isdigit(c);c=getchar()) x=(x<<3)+(x<<1)+c-'0';
	return x * f;
}

using namespace std;

const int MAXN = 1e4 + 100;

struct Edges{
	int to,nxt;
}E[MAXN<<1];
int head[MAXN],cnt = 0,tot,vis[MAXN],n,m,seq[MAXN],seq2[MAXN];
int dfn[MAXN],low[MAXN],isstk[MAXN],top = 0,col[MAXN],stk[MAXN],dfsclk = 0,sum = 0,ssum = 0;
vector<int>vec;

vector<int>lt[MAXN];
inline void addedge(int u,int v){
	E[++cnt].to = v; E[cnt].nxt = head[u]; head[u] = cnt;
	lt[u].push_back(v);
}
pair<int,int>ed[MAXN];

void dfs1(int u){
	seq[++tot] = u;
	vis[u] = 1;
	sort(lt[u].begin(),lt[u].end());
	for (int i = 0; i < (int)lt[u].size(); ++i) if (!vis[lt[u][i]]) dfs1(lt[u][i]);
}

int nu,nv;

int xy(){
	for (int i = 1; i <= tot; ++i) {
		if (seq[i] < seq2[i]) return 1;
		else if (seq[i] > seq2[i]) return 0;
	}
	return 0;
}

void dfs2(int u){
	//printf("Root%d\n",u);
	seq[++tot] = u;
	vis[u] = 1;
	for (int i = 0; i < (int)lt[u].size(); ++i) {
		int v = lt[u][i];
		if ((v == nv && u == nu) || (v == nu && u == nv)) {
		//	puts("fak?");
			continue;
		}
		if (!vis[lt[u][i]]) dfs2(lt[u][i]);
	}
}

inline void tarjan(int u,int ff){
	isstk[u] = 1; stk[++top] = u;
	dfn[u] = low[u] = ++dfsclk;
	for (int i = 0; i < (int)lt[u].size(); ++i) {
		int v = lt[u][i];
		if (v == ff) continue;
		if (!dfn[v]) {
			tarjan(v,u);
			low[u] = min(low[u],low[v]);
		}
		else if (isstk[v]) low[u] = min(low[u],dfn[v]);
	}
	if (low[u] == dfn[u]) {
		++sum;
		int v = stk[top];
		while(u != v) {
			isstk[v] = 0;
			col[v] = sum;
			--top;
			v = stk[top];
		}
		col[u] = sum;
		isstk[u] = 0;
		top--;
	}
}

signed main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n = read(),m = read();
	for (int i = 1; i <= m; ++i){
		int u = read(),v = read();
		addedge(u,v);
		addedge(v,u);
	}
	rep(i,1,n) sort(lt[i].begin(),lt[i].end());
	if (m == n - 1) {
		dfs1(1);
		for (int i = 1; i <= tot; ++i) printf("%d%c",seq[i],i==tot?'\n':' ');
	}
	else {
		seq2[1] = n+1;
		tarjan(1,0);
		for (int i = 1; i <= n; ++i)
			for (int j = 0; j < (int)lt[i].size(); ++j) {
				//printf("%d %d\n",i,lt[i][j]);
				if (col[i] == col[lt[i][j]] && i < lt[i][j]) ed[++ssum] = mp(i,lt[i][j]);
			}
		//puts("-------")	;
//		for (int i = 1; i <= ssum; ++i)
//			printf("%d %d\n",ed[i].fi,ed[i].se);
		for (int i = 1; i <= ssum; ++i) {
			nu = ed[i].fi,nv = ed[i].se;
//			printf("%d %d\n",ed[i].fi,ed[i].se);
//			if (nu == 2 && nv == 5) puts("fak");
			tot = 0;
			memset(vis,0,sizeof(vis));
			dfs2(1);
			if (xy()) rep(j,1,tot) seq2[j] = seq[j];
//			rep(j,1,tot) printf("%d%c",seq2[j],j==tot?'\n':' ');
		}
		for (int i = 1; i <= tot; ++i) printf("%d%c",seq2[i],i==tot?'\n':' ');
	}
	return 0;
}
