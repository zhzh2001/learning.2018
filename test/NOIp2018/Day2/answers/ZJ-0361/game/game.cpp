#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<iostream>
#define mp make_pair
#define pb push_back
#define fi first
#define se second
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
#define per(i,b,a) for(int i=(b);i>=(a);--i)
#define int long long
inline int read(void){
	int x=0,f=1;char c=getchar();
	for (;!isdigit(c);c=getchar()) f=c=='-'?-1:1;
	for (;isdigit(c);c=getchar()) x=(x<<3)+(x<<1)+c-'0';
	return x * f;
}
inline int quickpow(int m,int n,int p){
	int b = 1;
	while(n) {if (n & 1) b = b * m % p; n >>= 1; m = m * m % p;}
	return b;
}
inline int getinv(int x,int p){return quickpow(x,p-2,p);}
using namespace std;

const int MAXN = 5e6 + 100;
const int Mod = 1e9 + 7;
int fac[MAXN],ifac[MAXN],f[MAXN][9],n,m;
int a[4][4];

inline void init(void){
	fac[0] = ifac[0] = 1;
	for (int i = 1; i < MAXN; ++i) fac[i] = 1LL * fac[i-1] * i % Mod;
	ifac[MAXN-1] = getinv(fac[MAXN-1],Mod);
	for (int i = MAXN - 2; i; --i) ifac[i] = 1LL *ifac[i+1] * (i+1) % Mod;
}

inline int C(int n,int m){
	return 1LL * fac[n] * ifac[m] % Mod * ifac[n-m] % Mod;
}


signed main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	init();
	n = read(),m = read();
	if (n == 2) {
		int p = 1,ans = 0;
		for (int i = 1; i <= m - 1; ++i,p = Mod - p) 
		ans += 1LL * C(m-1,i) * quickpow(2,n*m-2*i,Mod) % Mod * p % Mod;
		ans = (quickpow(2,n*m,Mod) - ans + Mod) % Mod;
		printf("%lld\n",(ans % Mod + Mod) % Mod);
	}
	else if (n == 1) 
		printf("%lld\n",quickpow(2,m,Mod));
	else if (m == 1)
		printf("%lld\n",quickpow(2,n,Mod));
	else if (n == 3 && m == 3) {
		puts("112");
	}
	else if (n == 5 && m == 5) {
		puts("7136");
	}
	return 0; 
}
