#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<iostream>
#define mp make_pair
#define pb push_back
#define fi first
#define se second
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
#define per(i,b,a) for(int i=(b);i>=(a);--i)

inline int read(void){
	int x=0,f=1;char c=getchar();
	for (;!isdigit(c);c=getchar()) f=c=='-'?-1:1;
	for (;isdigit(c);c=getchar()) x=(x<<3)+(x<<1)+c-'0';
	return x * f;
}

using namespace std;

const int MAXN = 1e5 + 100;
struct Edges{
	int to,nxt;
}E[MAXN<<1];
int n,m,val[MAXN],head[MAXN],cnt = 0,fa[MAXN];
inline void addedge(int u,int v){
	E[++cnt].to = v; E[cnt].nxt = head[u]; head[u] = cnt;
}
inline int getf(int x){return fa[x] == x ? x : fa[x] = getf(fa[x]);}
char type[3];
int f[MAXN];


signed main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n = read(), m = read();
	scanf("%s",type);
	rep(i,1,n) val[i] = read();
	rep(i,1,n-1) {
		int u = read(),v = read();
		addedge(u,v); addedge(v,u);
	}
	if (n <= 10) {
		for (int op = 1; op <= m; ++op) {
			int a = read(),x = read(),b = read(),y = read();
			int res = 0x3F3F3F3F;
			for (int i = 0; i < (1<<n); ++i) {
				memset(f,0,sizeof(f));
				memset(fa,0,sizeof(fa));
				int ret = 0;
				for (int j = 1; j <= n; ++j) {
					if (j == a || j == b) {
						if (j == a) f[j] = x;
						if (j == b) f[j] = y;
						continue;	
					}
					if (i >> (j-1) & 1) f[j] = 1;
				}
				rep(j,1,n) ret += (val[j] * f[j]);
				for (int j = 1; j <= n; ++j) {
					if (!f[j]) continue;
					fa[j] = 1;
					for (int k = head[j]; k; k = E[k].nxt) {
						fa[E[k].to] = 1;
					}
				}
				int isok = 1;
				rep(j,1,n) isok &= (fa[j]);
				if (isok) {
					res = min(res,ret);
				}
			}
			printf("%d\n",res == 0x3F3F3F3F ? -1 : res);
		}
	}
	return 0;
}
