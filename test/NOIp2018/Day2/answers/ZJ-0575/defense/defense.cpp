#include <cstdio>
#include <cctype>
#include <algorithm>
using namespace std;

char tc(){static char tr[10000],*A=tr,*B=tr;return A==B&&(B=(A=tr)+fread(tr,1,10000,stdin),A==B)?EOF:*A++;}
void read(long long &x){char c;for(;!isdigit(c=tc()););for(x=c-48;isdigit(c=tc());x=(x<<1)+(x<<3)+c-48);}

const long long V=1e5+5;
long long N,Q,x,y,a,b,Dep[V],F[V][2],G[V][2],A[V];
long long head[V],nxt[V<<1],to[V<<1],Cnt;
#define Add(x,y) (nxt[++Cnt]=head[x],to[Cnt]=y,head[x]=Cnt)

void dfs(long long x,long long fa){
	Dep[x]=Dep[fa]+1,F[x][1]=A[x];
	for(long long i=head[x];i;i=nxt[i])if(to[i]^fa){
		dfs(to[i],x);
		F[x][0]+=F[to[i]][1];
		F[x][1]+=min(F[to[i]][1],F[to[i]][0]);
	}
}

void redfs(long long now,long long fa){
	if((now==a&&!x)||(now==b&&!y))G[now][1]=1e9;else G[now][1]=A[now];
	if((now==a&&x)||(now==b&&y))G[now][0]=1e9;
	for(long long i=head[now];i;i=nxt[i])if(to[i]^fa){
		redfs(to[i],now);
		if(G[now][0]!=1e9){if((now==a&&x)||(now==b&&y)||G[to[i]][1]==1e9)G[now][0]=1e9;else G[now][0]+=G[to[i]][1];}
		if(G[now][1]!=1e9){if((now==a&&!x)||(now==b&&!y)||(G[to[i]][1]==1e9&&G[to[i]][0]==1e9))G[now][1]=1e9;else G[now][1]+=min(G[to[i]][0],G[to[i]][1]);}
	}
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(N),read(Q),tc(),tc();
	for(long long i=1;i<=N;i++)read(A[i]);
	for(long long i=1;i<N;i++)read(x),read(y),Add(x,y),Add(y,x);
	dfs(1,0);
	for(long long i=1;i<=Q;i++){
		read(a),read(x),read(b),read(y);
		for(long long i=1;i<=N;i++)G[i][0]=G[i][1]=0;
		redfs(1,0);
		if(min(G[1][0],G[1][1])==1e9)puts("-1");
		else printf("%lld\n",min(G[1][0],G[1][1]));
	}
	return 0;
}
