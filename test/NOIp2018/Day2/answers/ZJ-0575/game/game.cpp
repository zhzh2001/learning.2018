#include <cstdio>
#include <cctype>
#include <string>
#include <iostream>
#include <algorithm>
using namespace std;

char tc(){static char tr[10000],*A=tr,*B=tr;return A==B&&(B=(A=tr)+fread(tr,1,10000,stdin),A==B)?EOF:*A++;}
void read(long long &x){char c;for(;!isdigit(c=tc()););for(x=c-48;isdigit(c=tc());x=(x<<1)+(x<<3)+c-48);}

const long long Mod=1e9+7;
long long N,M,F[10][1000005][2];

long long Ksm(long long x,long long y){
	long long res=1;for(;y;y>>=1,x=x*x%Mod)if(y&1)res=res*x%Mod;return res;
}

long long Cnt;
struct Node{long long x;string s;}G[1000005];
void dfs(long long x,long long y,long long v,string s,long long val){
	if(x==N&&y==M){G[++Cnt]=(Node){v,s};return ;}
	if(x<N)dfs(x+1,y,v+(val&(1ll<<(x*N+y-1))),s+'D',val);
	if(y<M)dfs(x,y+1,v+(val&(1ll<<((x-1)*N+y))),s+'R',val);
}

long long Cmp(Node x,Node y){return x.s+y.s<y.s+x.s;}
bool check(long long x){
	string s="";
	Cnt=0,dfs(1,1,0,s,x);
	sort(G+1,G+Cnt+1,Cmp);
	for(int i=2;i<=Cnt;i++)if(G[i].x>G[i-1].x)return 0;
	return 1;
}

long long Calc(void){
	long long Ans=0;
	for(long long i=0;i<1<<(N*M);i++)
		Ans+=check(i);
	return Ans;
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(N),read(M);
	if(N==3&&M==3)printf("%d\n",112);
	else printf("%lld",Calc());
	return 0;
}
