#include <queue>
#include <cstdio>
#include <cctype>
#include <vector>
#include <algorithm>
using namespace std;

char tc(){static char tr[10000],*A=tr,*B=tr;return A==B&&(B=(A=tr)+fread(tr,1,10000,stdin),A==B)?EOF:*A++;}
void read(int &x){char c;for(;!isdigit(c=tc()););for(x=c-48;isdigit(c=tc());x=(x<<1)+(x<<3)+c-48);}

const int V=1e4+5;
int N,M,x,y,vis[V],Gi[V],fvis[V],fAns[V],Ans[V],Cnt,gvis[V],tim;
vector<int>A[V];

void dfs(int x){
	printf("%d ",x),vis[x]=1;
	for(int i=0;i<A[x].size();i++)if(!vis[A[x][i]])dfs(A[x][i]);
}

void Topo(void){
	queue<int>Q;while(!Q.empty())Q.pop();
	for(int i=1;i<=N;i++)if(Gi[i]==1)Q.push(i),vis[i]=1;
	while(!Q.empty()){
		int front=Q.front();Q.pop();
		for(int i=0;i<A[front].size();i++)if(!vis[A[front][i]])
			if((--Gi[A[front][i]])==1)vis[A[front][i]]=1,Q.push(A[front][i]);
	}
}

void gref(int x,int gx,int gy){
	fAns[++Cnt]=x,gvis[x]=tim;
	for(int i=0;i<A[x].size();i++){
		if(tim!=gvis[A[x][i]]&&((x!=gy||A[x][i]!=gx)&&(x!=gx||A[x][i]!=gy)))gref(A[x][i],gx,gy);
	}
}

void Uta(void){
	for(int i=1;i<=N;i++){
		if(fAns[i]<Ans[i])break;
		if(fAns[i]>Ans[i])return ;
	}
	for(int i=1;i<=N;i++)Ans[i]=fAns[i];
	return ;
}

void fdfs(int x,int fa){
	fvis[x]=1;for(int i=0;i<A[x].size();i++)if(!vis[A[x][i]]&&fa!=A[x][i]){
		if(!fvis[A[x][i]]){Cnt=0,tim++,gref(1,x,A[x][i]),Uta(),fdfs(A[x][i],x);break;}
		if(fvis[A[x][i]]){Cnt=0,tim++,gref(1,x,A[x][i]),Uta();break;}
	}return ;
}

void Solve(void){
	Topo();
	for(int i=1;i<=N;i++)Ans[i]=N-i+1;
	for(int i=1;i<=N;i++)if(!vis[i]){fdfs(i,0);break;}
	for(int i=1;i<=N;i++)printf("%d ",Ans[i]);
}


void Work(void){return (void)(dfs(1));}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(N),read(M);
	for(int i=1;i<=M;i++)read(x),read(y),A[x].push_back(y),A[y].push_back(x),Gi[x]++,Gi[y]++;
	for(int i=1;i<=N;i++)sort(A[i].begin(),A[i].end());
	if(N==M)Solve();else Work();
	return 0;
}
