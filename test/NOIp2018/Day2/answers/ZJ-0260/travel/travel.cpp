#include<bits/stdc++.h>
#define N 5005
#define SZ(_) (int)_.size()
#define pb push_back
using namespace std;
bool emm1;
template<class T>inline void chkmin(T &x,T y){if(x>y)x=y;}
template<class T>inline void chkmax(T &x,T y){if(x<y)x=y;}
template<class T>inline void Rd(T &x){
	x=0;char c=getchar();bool f=0;
	while(c<'0'||c>'9'){if(c=='-')f^=1;c=getchar();}
	while(c>='0'&&c<='9'){x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	if(f)x=-x;
}
int n,m,U,V,sz,p,stk[N];
vector<int>E[N];
int fa[N];
int ans[N],res[N];
int get(int x){return x==fa[x]?x:get(fa[x]);}
void dfs(int x,int f){
	res[++p]=x;
	for(int i=0;i<SZ(E[x]);i++){
		int v=E[x][i];if(v==f||(x==U&&v==V)||(v==U&&x==V))continue;
		dfs(v,x);
	}
}

bool Get_lop(int x,int f){
	stk[++sz]=x;
	if(x==V)return 1;
	for(int i=0;i<SZ(E[x]);i++){
		int v=E[x][i];if(v==f)continue;
		if(Get_lop(v,x))return 1;
	}
	sz--;
	return 0;
}

bool emm2;
int main(){
//	cout<<"The memory is "<<(&emm2-&emm1)/1024.0/1024.0<<endl;

	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	Rd(n),Rd(m);
	for(int i=1;i<=n;i++)fa[i]=i;
	for(int i=1,u,v;i<=m;i++){
		Rd(u),Rd(v);
		E[u].pb(v);E[v].pb(u);
		int fu=get(u),fv=get(v);
		if(fu==fv)U=u,V=v;
		else fa[fv]=fu;
	}
	for(int i=1;i<=n;i++)sort(E[i].begin(),E[i].end());
	if(m==n-1){
		dfs(1,-1);
		for(int i=1;i<=n;i++)ans[i]=res[i];
	}
	else {
		Get_lop(U,-1);
		dfs(1,-1);
		for(int i=1;i<=n;i++)ans[i]=res[i];
		for(int i=1;i<sz;i++){
			U=stk[i],V=stk[i+1];
			p=0;
			dfs(1,-1);
			bool flg=0;
			for(int i=1;i<=n;i++){
				if(ans[i]>res[i]){
					flg=1;
					break;
				}
				else if(ans[i]<res[i])break;
			}
			if(flg)
				for(int i=1;i<=n;i++)ans[i]=res[i];
		}
	}
	for(int i=1;i<=n;i++){
		printf("%d",ans[i]);
		if(i<n)putchar(' ');
	}
	puts("");
	return 0;
}
