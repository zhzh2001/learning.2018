#include<bits/stdc++.h>
#define N 100005
#define Inf 0x3f3f3f3f
#define INF 0x3f3f3f3f3f3f3f3f
#define LL long long
using namespace std;
bool emm1;
template<class T>inline void chkmin(T &x,T y){if(x>y)x=y;}
template<class T>inline void chkmax(T &x,T y){if(x<y)x=y;}
template<class T>inline void Rd(T &x){
	x=0;char c=getchar();bool f=0;
	while(c<'0'||c>'9'){if(c=='-')f^=1;c=getchar();}
	while(c>='0'&&c<='9'){x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	if(f)x=-x;
}
int n,m;
char Tp[3];
int hd[N],nt[N<<1],to[N<<1],ct;
int p[N];
void Add(int x,int y){nt[++ct]=hd[x];hd[x]=ct;to[ct]=y;}

LL dp[N][2];
int a,x,b,y;
struct Pn2_44{
	void dfs(int x,int f){
		dp[x][1]+=p[x];
		for(int i=hd[x];i;i=nt[i]){
			int v=to[i];if(v==f)continue;
			dfs(v,x);
			dp[x][1]+=min(dp[v][0],dp[v][1]);
			dp[x][0]+=dp[v][1];
		}
	}
	void solve(){
		while(m--){
			memset(dp,0,sizeof(dp));
			Rd(a),Rd(x),Rd(b),Rd(y);
			if(x==0&&y==0){
				bool flg=0;
				for(int i=hd[a];i;i=nt[i])
					if(to[i]==b){
						flg=1;
						break;
					}
				if(flg){
					puts("-1");
					continue;
				}
			}
			else if(x!=y&&a==b){
				puts("-1");
				continue;
			}
			dp[a][!x]=INF;
			dp[b][!y]=INF;
			dfs(1,-1);
			printf("%lld\n",min(dp[1][1],dp[1][0]));
		}
	}
}p44;

bool emm2;
int main(){
//	cout<<"The memory is "<<(&emm2-&emm1)/1024.0/1024.0<<endl;

	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	Rd(n),Rd(m);scanf("%s",Tp);
	for(int i=1;i<=n;i++)Rd(p[i]);
	for(int i=1,a,b;i<n;i++){
		Rd(a),Rd(b);
		Add(a,b);Add(b,a);
	}
	p44.solve();
	return 0;
}
