#include<bits/stdc++.h>
#define N 10
#define M 1000005
#define P 1000000007
#define LL long long
using namespace std;
bool emm1;
template<class T>inline void chkmin(T &x,T y){if(x>y)x=y;}
template<class T>inline void chkmax(T &x,T y){if(x<y)x=y;}
template<class T>inline void Rd(T &x){
	x=0;char c=getchar();bool f=0;
	while(c<'0'||c>'9'){if(c=='-')f^=1;c=getchar();}
	while(c>='0'&&c<='9'){x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	if(f)x=-x;
}
int n,m;
LL Fast(LL x,LL d){
	LL res=1;
	while(d){
		if(d&1)res=res*x%P;
		x=x*x%P;
		d>>=1;
	}
	return res;
}

int fx[]={1,0};
int fy[]={0,1};

struct Pn_m_3{
	int Id(int x,int y){return x*m+y;}
	bool mark[100];
	int res[100000],p;
	void dfs(int x,int y,int val){
		val=(val<<1)+mark[Id(x,y)];
		if(x==n-1&&y==m-1){
			res[++p]=val;
			return ;
		}
		for(int i=0;i<2;i++){
			int nx=x+fx[i],ny=y+fy[i];
			if(nx<0||nx>=n||ny<0||ny>=m)continue;
			dfs(nx,ny,val);
		}
	}
	bool chk(){
		p=0;
		dfs(0,0,0);
		for(int i=1;i<p;i++)if(res[i+1]>res[i])return 0;
		return 1;
	}
	void solve(){
		int ans=0;
		for(int i=0;i<(1<<(n*m));i++){
			for(int j=0;j<n*m;j++)
				if((1<<j)&i)mark[j]=1;
				else mark[j]=0;
			ans+=chk();
		}
		cout<<ans<<endl;
	}
}p3;

bool emm2;
int main(){
//	cout<<"The memory is "<<(&emm2-&emm1)/1024.0/1024.0<<endl;

	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	Rd(n),Rd(m);
	if(n<=3&&m<=3)p3.solve();
	else if(n==1)printf("%lld\n",Fast(2,m));
	else if(n==2)printf("%lld\n",Fast(3,m-1)*4%P);
	else p3.solve();
	return 0;
}
