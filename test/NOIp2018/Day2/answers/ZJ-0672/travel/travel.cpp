#include<bits/stdc++.h>
using namespace std;
int n,m;
int ans[5009],lenans;
int head[5009],to[10009],nt[10009],bh;
int zhan[5009],top=1;
bool mark[5009];
int ds[5009];
int read()
{
	int fs=1,tot=0;
	char ch;
	while((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if(ch=='-') ch=getchar(),fs=-1;
	while(ch>='0'&&ch<='9') tot=tot*10+ch-'0',ch=getchar();
	return tot*fs;	
}
void add(int u,int v)
{
	bh++;
	nt[bh]=head[u];
	head[u]=bh;
	to[bh]=v;
}
void insert(int k)
{
	zhan[top++]=k;
	int now=top-1;
	while(now>1&&zhan[now]<zhan[now>>1])
	{
		swap(zhan[now],zhan[now>>1]);
		now>>=1;
	}
	return;
}
int pop()
{
	int fin=zhan[1];
	swap(zhan[1],zhan[top-1]);
	top--;
	int now=1;
	while((now<<1)<top)
	{
		if((now<<1|1)>=top||zhan[now<<1]<zhan[now<<1|1]) 
		{
			if(zhan[now]>zhan[now<<1]) swap(zhan[now],zhan[now<<1]),now<<=1;
			else break;	
		}
		else
		{
			if(zhan[now]>zhan[now<<1|1]) swap(zhan[now],zhan[now<<1|1]),now=now<<1|1;
			else break;	
		}
	}
	return fin;
}
void dfs(int now,int fa)
{
	printf("%d ",now);
	mark[now]=1;
	int num[5009],lennum=0;
	for(int i=head[now];i;i=nt[i])
	{
		if(to[i]!=fa&&!mark[to[i]]) num[++lennum]=to[i];
	}
	sort(num+1,num+lennum+1);
	for(int i=1;i<=lennum;i++)
	{
		if(!mark[num[i]]) dfs(num[i],now);
	}
}
int main ()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++)
	{
		int u=read(),v=read();
		add(u,v);
		add(v,u);	
	}
	dfs(1,0);
	return 0;	
}
/*
6 5
1 3
2 3
2 5
3 4
4 6
*/
/*
6 6
1 3
2 3
2 5
3 4
4 5
4 6
*/
