#include<cstdio>
#include<algorithm>
#include<map>
using namespace std;
long long tree[210000][2],tree1[210000][2];
int n,m;
int p[210000];
map<int,int>o[210000];
int addnum,vel[210000],ne[210000],head[210000];
void add(int u,int v)
{
	addnum++,vel[addnum]=v,ne[addnum]=head[u],head[u]=addnum;
}
void dfs(int u,int fa,int a,int x,int b,int y)
{
	tree[u][0]=0;
	tree[u][1]=p[u];
	for (int e=head[u];e;e=ne[e])
	{
		int v=vel[e];
		if (v==fa) continue;
		dfs(v,u,a,x,b,y);
		tree1[u][0]=tree[u][0],tree[u][0]=1e18;
		tree1[u][1]=tree[u][1],tree[u][1]=1e18;
		tree[u][0]=min(tree[u][0],tree1[u][0]+tree[v][1]);
		tree[u][1]=min(tree[u][1],tree1[u][1]+min(tree[v][0],tree[v][1])); 
	}
	if (u==a) tree[u][1-x]=1e18;
	if (u==b) tree[u][1-y]=1e18;
 } 
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	char dc;
	for (int i=1;i<=3;i++) dc=getchar();
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y),add(y,x); 
		o[y][x]=o[x][y]=1;
	}
	//dfs(1,-1);
	while (m--)
	{
		int a,b,x,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if (!x&&!y&&o[a][b])
		{
			printf("-1\n");
			continue;
		}
		dfs(1,-1,a,x,b,y);
		printf("%lld\n",min(tree[1][0],tree[1][1]));
	}
 } 
