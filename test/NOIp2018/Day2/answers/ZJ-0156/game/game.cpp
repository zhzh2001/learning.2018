#include<cstdio>
#include<algorithm>
using namespace std;
int b[15][15];
int ans;
const int MOD=1e9+7;
int check(int pos,int xi,int yi,int xj,int yj,int dx,int dy,int n,int m)
{
	if (b[xi][yi]<b[xj][yj]) return 1;
	if (b[xi][yi]>b[xj][yj]) return 0;
	if (pos>n+m-2) return 1;
	if (dx&(1<<(n+m-2-pos))) xi++; else yi++;
	if (dy&(1<<(n+m-2-pos))) xj++; else yj++;
	return check(pos+1,xi,yi,xj,yj,dx,dy,n,m);
}
void dfs(int x,int y,int n,int m)
{
	if (x>n)
	{
		int flag=1;
		for (int i=0;i<(1<<(n+m-2));i++)
			for (int j=i+1;j<(1<<(n+m-2));j++)
			{
				int numi=0,numj=0;
				for(int w=1;w<=n+m-2;w++)
					if (i&(1<<(w-1))) numi++;
				for (int w=1;w<=n+m-2;w++)
					if (j&(1<<(w-1))) numj++;
				if (numi!=n-1||numj!=n-1) continue;
				flag&=check(1,1,1,1,1,i,j,n,m); 
			} 
		ans+=flag;
		return;
	}
	b[x][y]=1; dfs(x+(y==m),y==m?1:y+1,n,m);
	b[x][y]=0; dfs(x+(y==m),y==m?1:y+1,n,m);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout); 
	int n,m;
	scanf("%d%d",&n,&m);
	if (n<=3&&m<=3)
	{
		dfs(1,1,n,m);
		printf("%d\n",ans);
		return 0;
	}
	if (n==2)
	{
		int num=1;
		for (int i=1;i<m;i++) num=1ll*num*3%MOD;
		num=1ll*num*4%MOD;
		printf("%d\n",num);
		return 0;
	}
}
