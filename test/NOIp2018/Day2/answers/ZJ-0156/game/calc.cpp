#include<cstdio>
#include<algorithm>
using namespace std;
int b[15][15];
int ans;
const int MOD=1e9+7;
int check(int pos,int xi,int yi,int xj,int yj,int dx,int dy,int n,int m)
{
	if (b[xi][yi]<b[xj][yj]) return 1;
	if (b[xi][yi]>b[xj][yj]) return 0;
	if (pos>n+m-2) return 1;
	if (dx&(1<<(n+m-2-pos))) xi++; else yi++;
	if (dy&(1<<(n+m-2-pos))) xj++; else yj++;
	return check(pos+1,xi,yi,xj,yj,dx,dy,n,m);
}
void dfs(int x,int y,int n,int m)
{
	if (x>n)
	{
		int flag=1;
		for (int i=0;i<(1<<(n+m-2));i++)
			for (int j=i+1;j<(1<<(n+m-2));j++)
			{
				int numi=0,numj=0;
				for(int w=1;w<=n+m-2;w++)
					if (i&(1<<(w-1))) numi++;
				for (int w=1;w<=n+m-2;w++)
					if (j&(1<<(w-1))) numj++;
				if (numi!=n-1||numj!=n-1) continue;
			//	printf("cas=%d %d %d %d\n",i,j,n,m);
				flag&=check(1,1,1,1,1,i,j,n,m);
				//printf("cas=%d %d %d\n",n,m,flag); 
			} 
		//	printf("%d\n",flag);
		ans+=flag;
		return;
	}
	b[x][y]=1; dfs(x+(y==m),y==m?1:y+1,n,m);
	b[x][y]=0; dfs(x+(y==m),y==m?1:y+1,n,m);
}
int main()
{
	for (int i=1;i<=4;i++)
	{
		for (int j=1;j<=4;j++)
		{
			ans=0;
			dfs(1,1,i,j);
			printf("%d ",ans);
		}
		printf("\n");
	}
}
