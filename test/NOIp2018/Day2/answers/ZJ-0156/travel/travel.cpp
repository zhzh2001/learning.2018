#include<cstdio>
#include<algorithm>
#include<vector>
using namespace std;
vector<int>o[210000];
int n,m,num;
int b[210000],c[210000],vis[210000];
int x[210000],y[210000];
void dfs(int u,int d)
{
	vis[u]=1;
	b[++num]=u;
	for (int i=0;i<(int)o[u].size();i++)
	{
		int v=o[u][i];
		if (vis[v]) continue;
		if ((u==x[d]&&v==y[d])||(u==y[d]&&v==x[d])) continue;
		dfs(v,d);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++) 
	{
		scanf("%d%d",&x[i],&y[i]);
		o[x[i]].push_back(y[i]);
		o[y[i]].push_back(x[i]);
	}
	for (int i=1;i<=n;i++)
	sort(o[i].begin(),o[i].end());
	if (m==n-1){
		dfs(1,0);
		for (int i=1;i<=n;i++) printf("%d%c",b[i],i==n?'\n':' ');
		return 0;
	}
	if (m==n)
	{
		for (int i=1;i<=n;i++) c[i]=1e9;
		for (int d=1;d<=m;d++)
		{
			for (int i=1;i<=n;i++) vis[i]=0;
			num=0;
			dfs(1,d);
			int flag=1;
			for (int i=1;i<=n;i++)
				if (!b[i])
				{
					flag=0;
					break;
				}
			for (int i=1;i<=n;i++)
			if (b[i]>c[i])
			{
				flag=0;
				break;
			} else if (b[i]<c[i]) break;
			if (flag)
			for (int i=1;i<=n;i++) c[i]=b[i];
		}
		for (int i=1;i<=n;i++) printf("%d%c",c[i],i==n?'\n':' ');
		return 0;
	}
}
