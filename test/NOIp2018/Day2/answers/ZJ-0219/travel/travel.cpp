#include<bits/stdc++.h>
using namespace std;
const int N=5010;
//bool mem1;
struct edge{
	int nxt,t,id;
}e[N<<1];
int head[N],edge_cnt;
void add_edge(int x,int y,int z){
	e[edge_cnt]=(edge){head[x],y,z};
	head[x]=edge_cnt++;
}
int n,m;
struct Edge{
	int x,y;
}E[N];
struct P1{
	int B[N],tot,h,Ans[N];
	void dfs(int x,int f){
		Ans[++h]=x;
		int i,lb=tot+1;
		for (i=head[x];~i;i=e[i].nxt){
			int to=e[i].t;
			if (to==f){
				continue;
			}
			B[++tot]=to;
		}
		int rb=tot;
		sort(B+lb,B+rb+1);
		for (i=lb;i<=rb;i++){
			dfs(B[i],x);
		}
	}
	void solve(){
		dfs(1,0);
		int i;
		for (i=1;i<=n;i++){
			printf("%d%c",Ans[i]," \n"[i==n]);
		}
	}
}Tree;
struct P2{
	int dfn[N],low[N],Tar_cnt,stk[N],stk_top,loop[N],loop_cnt;
	bool in_loop[N];
	void Tarjan(int x,int f){
		dfn[x]=low[x]=++Tar_cnt;
		stk[++stk_top]=x;
		int i;
		for (i=head[x];~i;i=e[i].nxt){
			if ((i>>1)==f){
				continue;
			}
			int to=e[i].t;
			if (dfn[to]==0){
				Tarjan(to,i>>1);
				low[x]=min(low[x],low[to]);
			}else{
				low[x]=min(low[x],dfn[to]);
			}
		}
		if (dfn[x]==low[x]){
			if (stk[stk_top]==x){
				stk_top--;
			}else{
				int y;
				do{
					y=stk[stk_top--];
					in_loop[y]=1;
					loop[++loop_cnt]=y;
				}while (x!=y);
				reverse(loop+1,loop+loop_cnt+1);
			}
		}
	}
	int Ban,B[N],h,now[N],Ans[N],tot;
	void dfs(int x,int f){
		now[++h]=x;
		int i,lb=tot+1;
		for (i=head[x];~i;i=e[i].nxt){
			int to=e[i].t;
			if (to==f || e[i].id==Ban){
				continue;
			}
			B[++tot]=to;
		}
		int rb=tot;
		sort(B+lb,B+rb+1);
		for (i=lb;i<=rb;i++){
			dfs(B[i],x);
		}
	}
	bool check(){
		if (Ans[1]==0){
			return 1;
		}
		int i;
		for (i=1;i<=n;i++){
			if (Ans[i]<now[i]){
				return 0;
			}else if (Ans[i]>now[i]){
				return 1;
			}
		}
		return 0;
	}
	void Cmp(){
		if (check()){
			memcpy(Ans+1,now+1,sizeof(int)*1*n);
		}
	}
	void solve(){
		Tarjan(1,-1);
		int i;
		for (i=1;i<=n;i++){
			int x=E[i].x,y=E[i].y;
			if (in_loop[x] && in_loop[y]){
				Ban=i;
				h=tot=0;
				dfs(1,0);
				Cmp();
			}
		}
		for (i=1;i<=h;i++){
			printf("%d%c",Ans[i]," \n"[i==h]);
		}
	}
}JHTree;
//bool mem2;
int main(){
//	cerr<<(&mem2-&mem1)/1024.0/1024<<endl;
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(head+1,-1,sizeof(int)*1*n);
	int i;
	for (i=1;i<=m;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		add_edge(x,y,i);
		add_edge(y,x,i);
		E[i]=(Edge){x,y};
	}
	if (m==n-1){
		Tree.solve();
	}else{
		JHTree.solve();
	}
	return 0;
}
