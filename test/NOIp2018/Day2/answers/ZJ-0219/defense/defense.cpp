#include<bits/stdc++.h>
using namespace std;
const int N=100010,K=17,INF=0x3f3f3f3f;
const long long INFL=0x3f3f3f3f3f3f3f3f;
//bool mem1;
struct edge{
	int nxt,t;
}e[N<<1];
int head[N],edge_cnt;
void add_edge(int x,int y){
	e[edge_cnt]=(edge){head[x],y};
	head[x]=edge_cnt++;
}
int n,m,A[N];
char Tp[5];
struct P1{
	int dp[N][2],lim[N];
	//0 自己建了   1 自己没建 儿子建了
	void dfs(int x,int f){
		dp[x][0]=A[x];
		dp[x][1]=0;
		int i;
		for (i=head[x];~i;i=e[i].nxt){
			int to=e[i].t;
			if (to==f){
				continue;
			}
			dfs(to,x);
			dp[x][0]+=min(dp[to][0],dp[to][1]);
			dp[x][1]+=dp[to][0];
			
			dp[x][0]=min(dp[x][0],INF);
			dp[x][1]=min(dp[x][1],INF);
		}
		if (lim[x]==0){
			dp[x][0]=INF;
		}else if (lim[x]==1){
			dp[x][1]=INF;
		}
	}
	void solve(){
		int i;
		memset(lim+1,-1,sizeof(int)*1*n);
		for (i=1;i<=m;i++){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			lim[a]=x,lim[b]=y;
			dfs(1,0);
			int Ans=min(dp[1][0],dp[1][1]);
			if (Ans==INF){
				printf("-1\n");
			}else{
				printf("%d\n",Ans);
			}
			lim[a]=lim[b]=-1;
		}
	}
}P44;
struct node{
	long long s[2][2];
	void init(long long x,long long y,long long z,long long w){
		s[0][0]=x;
		s[0][1]=y;
		s[1][0]=z;
		s[1][1]=w;
	}
	long long val(){
		long long res=INFL;
		int i,j;
		for (i=0;i<2;i++){
			for (j=0;j<2;j++){
				res=min(res,s[i][j]);
			}
		}
		return res;
	}
//	void debug(){
//		cerr<<s[0][0]<<" "<<s[0][1]<<" "<<s[1][0]<<" "<<s[1][1]<<endl;
//	}
};
void Merge(node &c,node a,node b){
	int i,j;
	for (i=0;i<2;i++){
		for (j=0;j<2;j++){
			c.s[i][j]=INFL;
			c.s[i][j]=min(c.s[i][j],a.s[i][0]+b.s[0][j]);
			c.s[i][j]=min(c.s[i][j],a.s[i][0]+b.s[1][j]);
			c.s[i][j]=min(c.s[i][j],a.s[i][1]+b.s[0][j]);
		}
	}
}
struct Segment_Tree{
	#define Lc (p<<1)
	#define Rc (p<<1|1)
	node G[N<<2];
	void build(int l,int r,int p){
		if (l==r){
			G[p].init(A[l],INFL,INFL,0);
			return;
		}
		int mid=(l+r)>>1;
		build(l,mid,Lc);
		build(mid+1,r,Rc);
		Merge(G[p],G[Lc],G[Rc]);
	}
	node Query(int l,int r,int p,int pl,int pr){
		if (l==pl && r==pr){
			return G[p];
		}
		int mid=(l+r)>>1;
		if (pr<=mid){
			return Query(l,mid,Lc,pl,pr);
		}else if (pl>mid){
			return Query(mid+1,r,Rc,pl,pr);
		}else{
			node res;
			Merge(res,Query(l,mid,Lc,pl,mid),Query(mid+1,r,Rc,mid+1,pr));
			return res;
		}
	}
	#undef Lc
	#undef Rc
}T;
struct P2{
//	dp[x][0]+=min(dp[to][0],dp[to][1]);
//	dp[x][1]+=dp[to][0];
	void solve(){
		T.build(1,n,1);
		int i;
		for (i=1;i<=m;i++){
			int x,a,y,b;
			scanf("%d%d%d%d",&x,&a,&y,&b);
			if (x>y){
				swap(x,y);
				swap(a,b);
			}
			node L,R;
			a^=1,b^=1;
			node t;
			t=T.Query(1,n,1,1,x);
			if (a==0){
				L.init(t.s[0][0],INFL,t.s[1][0],INFL);
			}else{
				L.init(INFL,t.s[0][1],INFL,t.s[1][1]);
			}
			t=T.Query(1,n,1,y,n);
			if (b==0){
				R.init(t.s[0][0],t.s[0][1],INFL,INFL);
			}else{
				R.init(INFL,INFL,t.s[1][0],t.s[1][1]);
			}
			node Ans;
			if (x<y-1){
				Merge(Ans,L,T.Query(1,n,1,x+1,y-1));
				Merge(Ans,Ans,R);
			}else{
				Merge(Ans,L,R);
			}
			long long Mn=Ans.val();
			if (Mn==INFL){
				printf("-1\n");
			}else{
				printf("%lld\n",Mn);
			}
		}
	}
}P68;
//struct P3{
//	int dp[N][2];
//	void dfs_pre(int x,int f){
//		fa[x]=f;
//		dp[x][0]=A[x];
//		dp[x][1]=0;
//		int i;
//		for (i=head[x];~i;i=e[i].nxt){
//			int to=e[i].t;
//			if (to==f){
//				continue;
//			}
//			dfs_pre(to,x);
//			dp[x][0]+=min(dp[to][0],dp[to][1]);
//			dp[x][1]+=dp[to][0];
//		}
//	}
//	void Deal(int x,int y){
//		memcpy(now[x],dp[x],sizeof(now[x]));
//		memcpy(now[y],dp[y],sizeof(now[y]));
//		if (lim[x]==0){
//			now[x][0]=INF;
//		}else if (lim[x]==1){
//			now[x][1]=INF;
//		}
//		if (lim[y]==0){
//			dp[y][0]=INF;
//		}else if (lim[y]==1){
//			dp[y][1]=INF;
//		}
//		if (dep[x]<dep[y]){
//			swap(x,y);
//		}
//		while (dep[x]>dep[y]){
//			Up(fa[x],x);
//			x=fa[x];
//		}
//		while (x!=y){
//			Up(fa[x],x);
//			Up(fa[y],y);
//			x=fa[x];
//			y=fa[y];
//		}
//		while (x!=1){
//			Up(fa[x],x);
//			x=fa[x];
//		}
//	}
//	void solve(){
//		dfs_pre(1,0);
//		int i;
//		memset(lim+1,-1,sizeof(int)*1*n);
//		for (i=1;i<=m;i++){
//			int a,x,b,y;
//			scanf("%d%d%d%d",&a,&x,&b,&y);
//			Nowtim=i;
//			lim[a]=x,lim[b]=y;
//			Deal(x,y);
//			int Ans=min(now[1][0],now[1][1]);
//			if (Ans==INF){
//				printf("-1\n");
//			}else{
//				printf("%d\n",Ans);
//			}
//			lim[a]=lim[b]=-1;
//		}
//	}
//}P76;
//bool mem2;
int main(){
//	cerr<<(&mem2-&mem1)/1024.0/1024<<endl;
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,Tp);
	int i;
	for (i=1;i<=n;i++){
		scanf("%d",&A[i]);
	}
	memset(head+1,-1,sizeof(int)*1*n);
	for (i=1;i<n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		add_edge(x,y);
		add_edge(y,x);
	}
	if (n<=2000 && m<=2000){
		P44.solve();
	}else if (Tp[0]=='A'){
		P68.solve();
	}
//	else if (Tp[0]=='B'){
//		
//	}
	return 0;
}
