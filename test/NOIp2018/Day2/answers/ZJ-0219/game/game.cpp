#include<bits/stdc++.h>
using namespace std;
const int N=10,M=1000010,mo=1000000007;
int n,m;
int Pow(int x,int y){
	int res=1;
	while (y>0){
		if ((y&1)==1){
			res=1LL*res*x%mo;
		}
		y>>=1;
		x=1LL*x*x%mo;
	}
	return res;
}
struct P0{
	int A[N][N],Mx[N][N],Mn[N][N],Ans;
	bool check(){
		memset(Mx,0,sizeof(Mx));
		int i,j;
		for (i=n;i>=1;i--){
			for (j=m;j>=1;j--){
				if (i<n && j<m && Mn[i+1][j]<Mx[i][j+1]){
					return 0;
				}
				Mx[i][j]=max(Mx[i+1][j],Mx[i][j+1])+(A[i][j]<<(n+m-i-j));
				if (i==n && j==m){
					Mn[i][j]=0;
				}else if (i==n){
					Mn[i][j]=Mn[i][j+1];
				}else if (j==m){
					Mn[i][j]=Mn[i+1][j];
				}else{
					Mn[i][j]=min(Mn[i+1][j],Mn[i][j+1]);
				}
				Mn[i][j]+=(A[i][j]<<(n+m-i-j));
			}
		}
		return 1;
	}
	void dfs(int x,int y){
		if (y==m+1){
			dfs(x+1,1);
			return;
		}
		if (x==n+1){
			Ans+=check();
			return;
		}
		A[x][y]=0;
		dfs(x,y+1);
		A[x][y]=1;
		dfs(x,y+1);
	}
	void solve(){
		Ans=0;
		dfs(1,1);
		printf("%d\n",Ans);
	}
}P20;
struct P2{
	void solve(){
		int Ans=4LL*Pow(3,m-1)%mo;
		printf("%d\n",Ans);
	}
}P50;
struct P3{
	void solve(){
		if (m<=3){
			P20.solve();
		}else{
			int Ans=112LL*Pow(3,m-3)%mo;
			printf("%d\n",Ans);
		}
	}
}P65;
struct P4{
	void solve(){
		if (n==5 && m==5){
			printf("7136\n");
			return;
		}
		if (n==1){
			printf("%d\n",Pow(2,m));
			return;
		}
		int now=1,sum=4,tt=28,i;
		for (i=3;i<=n;i++){
			now+=2;
			sum=1LL*sum*tt%mo;
			tt-=4;
		}
		if (now>m){
			printf("0\n");
			return;
		}
		int Ans=1LL*sum*Pow(3,m-now)%mo;
		printf("%d\n",Ans);
	}
}P00;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m){
		swap(n,m);
	}
	if (n<=3 && m<=3){
		P20.solve();
	}else if (n==2){
		P50.solve();
	}else if (n==3){
		P65.solve();
	}else if (n*m<=24){
		P20.solve();
	}else{
		P00.solve();
	}
	return 0;
}
//struct P1{
//	int dp[N][2][2];//下面是否大于中间，中间是否大于右边 
//	void solve(){
////		dp[1][0][0]=2;
////		dp[2][1][1]=2;
//		dp[2][0][0]=4;
//		int i;
//		for (i=3;i<=n;i++){
//			dp[i][0][0]=(0LL+dp[i-1][0][0]*2)%mo;
//			dp[i][1][0]=(0LL+dp[i-1][0][0]+dp[i-1][1][0]*2)%mo;
//			dp[i][0][1]=(0LL+dp[i-1][0][0]+dp[i-1][0][1]*2)%mo;
//			dp[i][1][1]=(0LL+dp[i-1][0][1]*2+dp[i-1][1][0]*2+dp[i-1][1][1]*8LL)%mo;
//		}
//		cerr<<dp[n][0][0]<<" "<<dp[n][0][1]<<" "<<dp[n][1][0]<<" "<<dp[n][1][1]<<endl;
//		int Ans=(0LL+dp[n][0][0]+dp[n][1][0]+dp[n][0][1]+dp[n][1][1])%mo*Pow(2,n-1)%mo;
//		Ans=(Ans+2LL*Pow(2,n*m-3))%mo;
//		printf("%d\n",Ans);
//	}
//}Peq;
