#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int P=1e9+7;
inline int read()
{
	char ch=getchar(); int ans=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch<='9'&&ch>='0') ans=ans*10+ch-48,ch=getchar();
	return ans;
}
const int N=2005;
char st[N];
int num,vet[N<<1],nex[N<<1],head[N];
int g[N][N],fa[N],c[N],f[N],dp[N][2];
inline int chkmin(int x,int y)
{
	if (x==-1) return y;
	if (y==-1) return x;
	return (x<y)?x:y;
}
inline void add(int u,int v)
{
	num++;
	vet[num]=v; nex[num]=head[u]; head[u]=num;
}
void dfsT(int u,int pre)
{
	fa[u]=pre; 
	for (int i=head[u]; i; i=nex[i])
	{
		int v=vet[i];
		if (v==pre) continue;
		dfsT(v,u);
	}
}
void dfs(int u)
{
	int s=0; int a[2005];
	for (int i=head[u]; i; i=nex[i])
	{
		int v=vet[i];
		if (v!=fa[u]) a[++s]=v,dfs(v); 
	}
	dp[u][0]=0; dp[u][1]=c[u];
	if (f[u]==1) dp[u][1]=-1;
	if (f[u]==2) dp[u][0]=-1;
	for (int i=1; i<=s; i++)
		if (dp[u][0]!=-1&&dp[a[i]][1]!=-1) dp[u][0]+=dp[a[i]][1];
		else{dp[u][0]=-1; break;}
	for (int i=1; i<=s; i++)
		if (dp[u][1]!=-1&&(dp[a[i]][0]!=-1||dp[a[i]][1]!=-1)) dp[u][1]+=chkmin(dp[a[i]][0],dp[a[i]][1]);
		else{dp[u][1]=-1; break;}
	//printf("dp %d %d %d\n",u,dp[u][0],dp[u][1]);
}
int main()
{
	freopen("defense.in","r",stdin); freopen("defense.out","w",stdout);
	int n=read(),m=read();
	scanf("%s",st+1);
	for (int i=1; i<=n; i++) c[i]=read();
	for (int i=1; i<n; i++)
	{
		int u=read(),v=read();
		g[u][v]=g[v][u]=1;
		add(u,v); add(v,u);
	}
	dfsT(1,0);
	for (int i=1; i<=m; i++)
	{
		int x=read(),a=read(),y=read(),b=read();
		if (a==0&&b==0&&g[x][y]){puts("-1"); continue;}
		f[x]=a+1; f[y]=b+1;
		dfs(1); 
		int ans=-1;
		if (dp[1][0]!=-1) ans=chkmin(ans,dp[1][0]);
		if (dp[1][1]!=-1) ans=chkmin(ans,dp[1][1]);
		printf("%d\n",ans);
		f[x]=f[y]=0;
	}
	return 0;
}
