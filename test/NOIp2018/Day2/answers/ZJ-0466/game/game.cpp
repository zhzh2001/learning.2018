#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int P=1e9+7;
inline int read()
{
	char ch=getchar(); int ans=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch<='9'&&ch>='0') ans=ans*10+ch-48,ch=getchar();
	return ans;
}
int n,m,ans,num,f[15][15],g[15],b[15],s[15][15],a[15][15],f2[1000005],f3[1000005];
inline void incP(int &x,int y){x=x+y; x=(x>=P)?(x-P):x;}
inline bool check(int x,int y,int f[],int g[])
{
	for (int i=1; i<=x; i++)
	{
		if (f[i]>g[i]) return 1;
		if (f[i]<g[i]) return 0; 
	}
	return 0;
}
void dfs1(int x,int y)
{
	if (x>n)
	{
		for (int i=1; i<=num; i++)
		{
			int x=1,y=1;
			for (int j=1; j<=g[i]; j++)
			{
				if (f[i][j]==2) y++;
				else x++;
				s[i][j]=a[x][y]; 
			}
		}
		for (int i=1; i<=num; i++)
			for (int j=i+1; j<=num; j++)
			{
				if (!(check(g[i],g[j],f[i],f[j])^check(g[i],g[j],s[i],s[j]))) return;
			}
		ans++;
		return;
	}
	int tx=x,ty=y+1;
	if (ty==m+1) tx++,ty=1;
	a[x][y]=1; dfs1(tx,ty);
	a[x][y]=0,dfs1(tx,ty);
}
void dfs2(int x,int y,int s)
{
	if (x==n&&y==m)
	{
		num++; g[num]=s;
		for (int i=1; i<=s; i++) f[num][i]=b[i];
		return; 
	}
	if (y!=m) b[s]=2,dfs2(x,y+1,s+1);
	if (x!=n) b[s]=1,dfs2(x+1,y,s+1);
}
int main()
{
	freopen("game.in","r",stdin); freopen("game.out","w",stdout);
	n=read(),m=read();
	if (n<=3&&m<=3)
	{
		dfs2(1,1,1);
		dfs1(1,1);
		printf("%d\n",ans);
		return 0;
	}
	f2[0]=f3[0]=1; 
	for (int i=1; i<=m; i++) f2[i]=(long long)f2[i-1]*2%P,f3[i]=(long long)f3[i-1]*3%P;
	ans=f2[m-1];
	for (int i=2; i<=m; i++) incP(ans,(long long)f2[i-2]*f3[m-i]%P);
	printf("%d\n",(long long)ans*4%P);
	return 0;
}
