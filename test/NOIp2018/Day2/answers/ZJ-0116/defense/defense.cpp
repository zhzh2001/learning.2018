#include <cstdio>
#include <algorithm>
#define N 100005
#define INF 0x3f3f3f3f

using namespace std;
typedef long long LL;
LL dp[N][2], p[N];
char ch, ch1;

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	int n, m, u, v, a, x, b, y;
	scanf("%d%d", &n, &m);
	while (ch < 'A' || ch > 'C') ch = getchar();
	ch1 = getchar();
	for (int i = 1; i <= n; ++i) {
		scanf("%d", &p[i]);
	}
	for (int i = 1; i < n; ++i) {
		scanf("%d%d", &u, &v);
	}
//	if (ch == 'A') {
		for (int i = 1; i <= m; ++i) {
			scanf("%d%d%d%d", &a, &x, &b, &y);
			dp[0][1] = 0;
			dp[0][0] = 0;
			for (int j = 1; j <= n; ++j) {
				dp[j][0] = dp[j - 1][1];
				dp[j][1] = min(dp[j - 1][1], dp[j - 1][0]) + p[j];
				if (j == a) dp[j][x ^ 1] = INF;
				if (j == b) dp[j][y ^ 1] = INF;
			}
			LL ans = min(dp[n][0], dp[n][1]);
			if (ans >= INF) puts("-1");
			else printf("%lld\n", ans);
		}
//	}
	fclose(stdin); fclose(stdout);
	return 0;
}
