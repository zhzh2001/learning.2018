#include <cstdio>
#include <vector>
#include <algorithm>
#include <cstring>
#define N 5010

using namespace std;
vector <int> e[N];
int ans[N], cnt, vis[N], used[N], l, r, back, mini;

inline void dfs(int x, int fa) {
	ans[++cnt] = x;
	vis[x] = 1;
	vector<int> :: iterator it;
	for (it = e[x].begin(); it != e[x].end(); it++) {
		if ((*it) == fa) continue;
		if (vis[*it]) {
			l = x;
			r = *it;
			continue;
		}
		dfs(*it, x);
	}
}

inline void dfs1(int x, int fa) {
	if (vis[x]) {
		vis[fa] = 1;
		return;
	}
	vector<int> :: iterator it;
	for (it = e[x].begin(); it != e[x].end(); it++) {
		if (x == l && (*it) == r || (*it) == fa) continue;
		dfs1(*it, x);
		if (vis[x]) {
			vis[fa] = 1;
			return;
		}
	}
}

inline void dfs2(int x, int h) {
	used[x] = 1;
	ans[++cnt] = x;
	vector<int> :: iterator it;
	if (vis[x] && h) {
		int o = 0;
//		printf("%d\n", x);
		for (it = e[x].begin(); it != e[x].end(); it++) {
//			printf("%d   ", *it);
			if (!used[*it]) {
				if (!o) o = *it;
				else {
					mini = *it;
					break;
				}
			}
		}
		dfs2(o, 0);
	}
	for (it = e[x].begin(); it != e[x].end(); it++) {
		if (back && !h && (*it) > mini) {
			back = 0;
			dfs2(mini, 0);
		}
		if (!used[*it]) dfs2(*it, h);
	}
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	int n, m, u, v;
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; ++i) {
		scanf("%d%d", &u, &v);
		e[u].push_back(v);
		e[v].push_back(u);
	}
	for (int i = 1; i <= n; ++i) {
		sort(e[i].begin(), e[i].end());
	}
	if (m == n - 1) {
		dfs(1, 1);
	}
	if (m == n) {
		dfs(1, 1);
		memset(vis, 0, sizeof vis);
		vis[r] = 1;
		dfs1(l, l);
		cnt = 0;
		back = 1;
		dfs2(1, 1);
//		printf("%d %d\n", mini, back);
//		printf("%d\n\n", vis[3]);
	}
	for (int i = 1; i < cnt; ++i) {
		printf("%d ", ans[i]);
	}
	printf("%d\n", ans[cnt]);
	fclose(stdin); fclose(stdout);
	return 0;
}
