#include <cstdio>
#include <algorithm>
#define MOD 1000000007

using namespace std;
typedef long long LL;
const int d[3][3] = {{2, 4, 8}, {4, 12, 36}, {8, 36, 112}};
int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	int n, m;
	scanf("%d%d", &n, &m);
	if (n <= 2) {
		LL ans = 4;
		for (int i = 0; i <= n + m - 4; ++i) {
			ans = ans * 3 % MOD;
		}
		printf("%lld\n", ans);
	}
	else {
		if (n <= 3 && m <= 3) printf("%d\n", d[n - 1][m - 1]);
		else printf("%d\n", 7136);
	}
	fclose(stdin); fclose(stdout);
}
