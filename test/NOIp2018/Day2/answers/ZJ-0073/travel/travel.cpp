#include<bits/stdc++.h>
using namespace std;
const int N=5005;
vector<int>G[N];
int n,m;

void Bo() {
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
}

namespace Solver1 {
  int res[N],tot;
  void Dfs(int o,int fa) {
    res[++tot]=o;
    for(int i=0;i<(int)G[o].size();i++) {
      int to=G[o][i];
      if(to==fa) continue;
      Dfs(to,o);
    }
  }
  void main() {
    for(int i=1;i<=n;i++) {
      sort(G[i].begin(),G[i].end());
    }
    Dfs(1,1);
    for(int i=1;i<=n;i++) {
      printf("%d ",res[i]);
    }
    puts("");
    return ;
  }
}

namespace Solver2 {
  int ring[N],r,vis[N],fa[N],res[N],ans[N];
  int tot,a,b;
  int Getring(int o,int ff) {
    fa[o]=ff;vis[o]=1;
    for(int i=0;i<(int)G[o].size();i++) {
      int to=G[o][i];
      if(to==ff) continue;
      if(vis[to]) {
	for(int nw=o;nw!=to;nw=fa[nw]) {
	  ring[++r]=nw;
	}
	ring[++r]=to;
	return 1;
      }
      else {
	if(Getring(to,o)) return 1;
      }
    }
    return 0;
  }

  void Dfs(int o,int ff) {
    ans[++tot]=o;
    for(int i=0;i<(int)G[o].size();i++) {
      int to=G[o][i];
      if(to==ff) continue;
      if((to==a&&o==b)||(to==b&&o==a)) continue;
      Dfs(to,o);
    }
  }

  void Min() {
    bool f=0;
    for(int i=1;i<=n;i++) {
      if(ans[i]==res[i]) continue;
      if(ans[i]<res[i]) f=1;
      break;
    }
    if(f) {
      for(int i=1;i<=n;i++) res[i]=ans[i];
    }
  }
  
  void main() {
    Getring(1,1);
    for(int i=1;i<=n;i++) {
      sort(G[i].begin(),G[i].end());
    }
    //for(int i=1;i<=r;i++) printf("%d%c",ring[i]," \n"[i==r]);
    a=ring[r],b=ring[1];
    tot=0;
    Dfs(1,1);
    for(int i=1;i<=n;i++) res[i]=ans[i];
    for(int i=1;i<r;i++) {
      a=ring[i],b=ring[i+1];
      tot=0;
      Dfs(1,1);
      Min();
    }
    for(int i=1;i<=n;i++) {
      printf("%d ",res[i]);
    }
    puts("");
    return ;
  }
}

int main() {
  Bo();
  scanf("%d%d",&n,&m);
  for(int i=1,u,v;i<=m;i++) {
    scanf("%d%d",&u,&v);
    G[u].push_back(v);G[v].push_back(u);
  }
  if(m==n-1) {
    Solver1::main();
    return 0;
  }
  else Solver2::main();
  return 0;
}
