#include<bits/stdc++.h>
using namespace std;
const int N=1e5+500;
typedef long long ll;
const ll inf=1e15;
int n,m,tot,tp;
char s[4];
struct edge {
  int to,nxt;
}E[N<<2];
int head[N],val[N];
void Bo() {
  freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout);
}

void Addedge(int u,int v) {
  E[++tot].to=v;E[tot].nxt=head[u];head[u]=tot;
  E[++tot].to=u;E[tot].nxt=head[v];head[v]=tot;
}

namespace Solver1 {
  ll f[N][2];
  int nt[N],ms[N];

  void Dp(int o,int fa) {
    if(nt[o]) f[o][0]=0,f[o][1]=inf;
    else if(ms[o]) f[o][0]=inf,f[o][1]=val[o];
    else {
      f[o][0]=0;f[o][1]=val[o];
    }
    for(int i=head[o];~i;i=E[i].nxt) {
      int to=E[i].to;
      if(to==fa) continue;
      Dp(to,o);
      if(nt[o]) f[o][0]+=f[to][1];
      else if(ms[o]) f[o][1]+=min(f[to][0],f[to][1]);
      else {
	f[o][0]+=f[to][1];
	f[o][1]+=min(f[to][0],f[to][1]);
      }
    }
  }
  
  void Solve(int a,int b,int x,int y) {
    Dp(1,1);
    ll ans=min(f[1][0],f[1][1]);
    /*
    for(int i=1;i<=n;i++) {
      printf("%lld %lld\n",f[i][0]==inf?-1:f[i][0],f[i][1]==inf?-1:f[i][1]);
    }
    */
    if(a==b&&a==1&&x==y) {
      ans=f[1][x];
    } 
    printf("%lld\n",ans>=inf?-1:ans);
  }
  void main() {
    for(int i=1,a,b,x,y;i<=m;i++) {
      scanf("%d%d%d%d",&a,&x,&b,&y);
      if(a==b&&x!=y) {
	puts("-1");continue;
      }
      if(x==1) ms[a]=1;
      else nt[a]=1;
      if(y==1) ms[b]=1;
      else nt[b]=1;
      Solve(a,b,x,y);
      nt[a]=nt[b]=ms[a]=ms[b]=0;
    }
  }
}
/*
namespace Solver2 {
  int fa[N],wi[N];
  ll f[N][2],t[N][2];
  void preDp(int o,int ff) {
    f[o][0]=0;f[o][1]=val[o];fa[o]=ff;
    for(int i=head[o];~i;i=E[i].nxt) {
      int to=E[i].to;
      if(to==ff) continue;
      preDp(to,o);
      f[o][0]+=f[to][1];
      f[o][1]+=min(f[to][0],f[to][1]);
      if(f[to][0]<f[to][1]) wi[to]=0;
      else wi[to]=1;
    }
  }
  void Solve(int b,int y) {
    int nw=b;
    for(it )
    if(y==1) {
      t[fa[nw]][1]-=t[nw][wi[nw]];
      t[fa[nw]][1]+=t[nw][1];
      nw=fa[nw];
      while(nw!=1) {
	t[fa[nw]][0]-=f[nw][1];
	t[fa[nw]][0]+=t[nw][1];
	t[fa[nw]][1]-=f[nw][wi[nw]];
	t[fa[nw]][1]+=min(t[nw][0],t[nw][1]);
	nw=fa[nw];
      }
      ll ans=t[1][1];
      printf("%lld\n",ans>=inf?-1:ans);
    }
    else {
      t[fa[nw]][1]-=t[nw][wi[nw]];
      t[fa[nw]][1]+=t[nw][0];
      t[fa[nw]][0]=inf;
      nw=fa[nw];
      while(nw!=1) {
	t[fa[nw]][0]-=f[nw][1];
	t[fa[nw]][0]+=t[nw][1];
	t[fa[nw]][1]-=f[nw][wi[nw]];
	t[fa[nw]][1]+=min(t[nw][0],t[nw][1]);
	nw=fa[nw];
      }
      ll ans=t[1][1];
      printf("%lld\n",ans>=inf?-1:ans);
    }
  }
  void main() {
    preDp(1,1);
    for(int i=1,a,b,x,y;i<=m;i++) {
      scanf("%d%d%d%d",&a,&x,&b,&y);
      if(b==1&&y==1) {
	ll ans=f[1][1];
	printf("%lld\n",ans>=inf?-1:ans);
      }
      else if(b==1&&y==0) {
	puts("-1");
	continue;
      }
      else Solve(b,y);
    }
  }
}
*/

int main() {
  Bo();
  memset(head,-1,sizeof head);
  scanf("%d%d%s",&n,&m,s);
  for(int i=1;i<=n;i++) {
    scanf("%d",&val[i]);
  }
  for(int i=1,u,v;i<n;i++) {
    scanf("%d%d",&u,&v);
    Addedge(u,v);
  }
  //cerr<<"Read!"<<endl;
  Solver1::main();
  return 0;
}
