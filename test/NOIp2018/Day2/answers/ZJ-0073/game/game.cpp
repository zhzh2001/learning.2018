#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int Md=1e9+7;
int n,m;
void Bo() {
  freopen("game.in","r",stdin);
  freopen("game.out","w",stdout);
}

inline int Mul(const int &x,const int &y) {return (ll)x*y%Md;} 
inline int Add(const int &x,const int &y) {return (x+y)>=Md?(x+y-Md):(x+y);}
inline int Sub(const int &x,const int &y) {return (x-y)<0?(x-y+Md):(x-y);}
int Powe(int x,int y) {
  int res=1;
  while(y) {
    if(y&1) res=Mul(res,x);
    x=Mul(x,x);
    y>>=1;
  }
  return res;
}

namespace Solver1 {
  int mp[10][10];
  bool Min(vector<int>A,vector<int>B) {
    for(int i=0;i<A.size();i++) {
      if(A[i]==B[i]) continue;
      return A[i]<B[i];
    }
    return 0;
  } 
  struct path {
    vector<int>s1,s2;
    bool operator < (const path &rhs) const {
      return Min(s1,rhs.s1);
    }
  }p[100];
  int tot=0,ans=0;
  vector<int>p1,p2;
  void Getmp(int st) {
    for(int i=0;i<n;i++) {
      for(int j=0;j<m;j++) {
	int nw=(i*m+j);
	mp[i][j]=(1<<nw&st)>0?1:0;
      }
    }
  }
  void Dfs(int x,int y) {
    if(x==n-1&&y==m-1) {
      p[++tot]=(path){p1,p2};
      return ;
    }
    if(y+1<m) {
      p1.push_back(1);p2.push_back(mp[x][y+1]);
      Dfs(x,y+1);
      p1.pop_back();p2.pop_back();
    }
    if(x+1<n) {
      p1.push_back(0);p2.push_back(mp[x+1][y]);
      Dfs(x+1,y);
      p1.pop_back();p2.pop_back();
    }
    return ;
  }

  int Check() {
    sort(p+1,p+1+tot);
    for(int i=1;i<=tot;i++) {
      for(int j=i+1;j<=tot;j++) {
	if(Min(p[i].s2,p[j].s2)) return 0;
      }
    }
    return 1;
  }
  
  void main() {
    int state=(1<<(n*m));
    for(int i=0;i<state;i++) {
      //cerr<<i<<endl;
      Getmp(i);
      tot=0;
      p2.push_back(mp[0][0]);
      Dfs(0,0);
      //cerr<<"Dfs!"<<endl;
      p2.pop_back();
      if(Check()) ans++;
    }
    printf("%d\n",ans);
  }
}

namespace Solver2 {
  void main() {
    int ans=4;
    ans=Mul(ans,Powe(3,m-1));
    printf("%d\n",ans);
  }
}

namespace Solver3 {
  void main() {
    if(m==1) puts("8");
    else if(m==2) puts("36");
    else if(m==3) puts("112");
    else {
      int ans=112;
      ans=Mul(ans,Powe(3,m-3));
      printf("%d\n",ans);
    }
  }
}

int main() {
  Bo();
  scanf("%d%d",&n,&m);
  if(n<=3&&m<=3) {
    Solver1::main();
    return 0;
  }
  if(n==2) {
    Solver2::main();
    return 0;
  }
  if(n==3) {
    Solver3::main();
    return 0;
  }
  return 0;
} 
