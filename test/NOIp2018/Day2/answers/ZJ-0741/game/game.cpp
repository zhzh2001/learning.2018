#include<bits/stdc++.h>
using namespace std;
const int mod=1e9+7;

int n,m,f[2][257],ans;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==1){
		printf("%d\n",pow(2,m));
	}
	if(n==3){
		if(m==1){
			printf("8\n");
		}
		else if(m==2){
			printf("24\n");
		}
		else printf("112\n");
	}
	if(n==2){
		for(int i=0;i<(1<<n);i++){
			f[1][i]=1;
		}
		int cur=1;
		for(int i=2;i<=m;i++){
			cur^=1;
			memset(f[cur],0,sizeof f[cur]);
			for(int j=0;j<(1<<n);j++){
				for(int k=0;k<(1<<n);k++){
					if(!(k>>1&(j^((1<<n)-1))))f[cur][k]=(f[cur][k]+f[cur^1][j])%mod;
				}
			}
		}
		for(int i=0;i<1<<n;i++){
			ans=(ans+f[cur][i])%mod;
		}
		printf("%d\n",ans);
	}
	return 0;
}
