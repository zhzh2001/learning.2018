#include<bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
const int N=3e5+5,M=6e5+5;
struct edge{
	int v,nxt;
}e[M];
int head[N],tot,t[N];
int n,m,cost[N],f[N][2];
char s[5];
void addedge(int u,int v){
	e[++tot]=(edge){v,head[u]};
	head[u]=tot;
	e[++tot]=(edge){u,head[v]};
	head[v]=tot;
}
void dfs(int x,int fa){
	for(int i=head[x];i;i=e[i].nxt){
		if(e[i].v==fa)continue;
		dfs(e[i].v,x);
	}
	if(t[x]==1){
		f[x][0]=INF;
		f[x][1]=cost[x];
		for(int i=head[x];i;i=e[i].nxt){
			if(e[i].v==fa)continue;
			int y=min(f[e[i].v][1],f[e[i].v][0]);
			if(y==INF)f[x][1]=INF;
			else f[x][1]+=y;
		}
	}
	else if(t[x]==0){
		f[x][1]=INF;
		f[x][0]=0;
		for(int i=head[x];i;i=e[i].nxt){
			if(e[i].v==fa)continue;
			if(f[e[i].v][1]==INF)f[x][0]=INF;
			else f[x][0]+=f[e[i].v][1];
		}
	}
	else{
		f[x][1]=cost[x];
		f[x][0]=0;
		for(int i=head[x];i;i=e[i].nxt){
			if(e[i].v==fa)continue;
			if(f[e[i].v][1]==INF)f[x][0]=INF;
			else f[x][0]+=f[e[i].v][1];
			int y=min(f[e[i].v][1],f[e[i].v][0]);
			if(y==INF)f[x][1]=INF;
			else f[x][1]+=y;
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for(int i=1;i<=n;i++){
		scanf("%d",&cost[i]);
	}
	for(int i=1;i<n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		addedge(u,v);
	}
	int a,b,c,d;
	memset(t,-1,sizeof t);
	for(int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&b,&c,&d);
		t[a]=b,t[c]=d;
		dfs(1,0);
		t[a]=t[c]=-1;
		int ans=min(f[1][0],f[1][1]);
		if(ans==INF)puts("-1");
		else printf("%d\n",ans);
	}
}
