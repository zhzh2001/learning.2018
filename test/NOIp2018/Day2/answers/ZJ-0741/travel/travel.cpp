#include<bits/stdc++.h>
using namespace std;
const int M=1e4+5,N=5e3+5;

struct edge{
	int v,nxt;
}e[M];
int head[N],tot,n,m;
void addedge(int u,int v){
	e[++tot]=(edge){v,head[u]};
	head[u]=tot;
	e[++tot]=(edge){u,head[v]};
	head[v]=tot;
}
namespace task1{
	vector<int>son[N],ans;
	void dfs1(int x,int fa){
		for(int i=head[x];i;i=e[i].nxt){
			if(e[i].v==fa)continue;
			dfs1(e[i].v,x);
			son[x].push_back(e[i].v);
		}
		sort(son[x].begin(),son[x].end());
	}
	void dfs2(int x){
		int len=son[x].size();
		ans.push_back(x);
		for(int i=0;i<len;i++){
			dfs2(son[x][i]);
		}
	}
	void main(){
		dfs1(1,0);
		dfs2(1);
		int len=ans.size();
		for(int i=0;i<len;i++){
			printf("%d ",ans[i]);
		}
	}
}
namespace task2{
	int s[N],top,S;
	bool use[N],flag,vis[N];
	vector<int>son[N],ans,g;
	void dfs(int x,int fa){
		if(use[x]==1){
			flag=1,S=x;
			return;
		}
		use[x]=1;
		s[++top]=x;
		for(int i=head[x];i;i=e[i].nxt){
			if(e[i].v==fa)continue;
			dfs(e[i].v,x);
			if(flag)return;
		}
		use[x]=0;
		top--;
	}
	void dfs1(int x){
		vis[x]=1;
		for(int i=head[x];i;i=e[i].nxt){
			if(vis[e[i].v])continue;
			dfs1(e[i].v);
			son[x].push_back(e[i].v);
		}
		sort(son[x].begin(),son[x].end());
	}
	void dfs2(int x){
		int len=son[x].size();
		g.push_back(x);
		for(int i=0;i<len;i++){
			dfs2(son[x][i]);
		}
	}
	void dfs3(int x){
		int len=son[x].size();
		if(x==S)
		for(int k=0;k<ans.size();k++){
			g.push_back(ans[k]);
		}
		else g.push_back(x);
		for(int i=0;i<len;i++){
			dfs3(son[x][i]);
		}
	}
	void main(){
		dfs(1,0);
		int p;
 		for(int i=1;i<=top;i++){
			if(s[i]==S){
				p=i;
				break;
			}
			use[s[i]]=0;
		}
		p++;
		ans.push_back(S);
		vis[S]=1;
		if(s[p]<s[top]){
			bool kk=0;
			for(int i=p;i<=top;i++){
				if(s[i]>s[top]){
					kk=1;
				}
				else{
					vis[s[i]]=1;
					ans.push_back(s[i]);
					for(int j=head[s[i]];j;j=e[j].nxt){
						if(use[e[j].v])continue;
						dfs1(e[j].v);
						g.clear();
						dfs2(e[j].v);
						for(int k=0;k<g.size();k++){
							ans.push_back(g[k]);
						}
					}
				}
				if(kk){
					dfs1(s[top]);
					g.clear();
					dfs2(s[top]);
					for(int k=0;k<g.size();k++){
						ans.push_back(g[k]);
					}
					break;
				}
			}
		}
		else{
			bool kk=0;
			for(int i=top;i>=p;i--){
				if(s[i]>s[p]){
					kk=1;
				}
				else{
					vis[s[i]]=1;
					ans.push_back(s[i]);
					for(int j=head[s[i]];j;j=e[j].nxt){
						if(use[e[j].v])continue;
						dfs1(e[j].v);
						g.clear();
						dfs2(e[j].v);
						for(int k=0;k<g.size();k++){
							ans.push_back(g[k]);
						}
					}
				}
				if(kk){
					dfs1(s[p]);
					g.clear();
					dfs2(s[p]);
					for(int k=0;k<g.size();k++){
						ans.push_back(g[k]);
					}
					break;
				}
			}
		}
		g.clear();
		vis[S]=0;
		dfs1(1);
		dfs3(1);
		for(int i=0;i<g.size();i++){
			printf("%d ",g[i]);
		}
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		addedge(u,v);
	}
	if(n==m-1)task1::main();
	else task2::main();
	return 0;
}
