#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<vector>
#include<cctype>
#include<iomanip>
#include<map>
#include<stack>
#include<cstring>
#include<string>
#define N 5010
using namespace std;
int n,m,id=0,a[N],vis[N];
struct edge{
	int to,next;
}e[N];
int ct=1,head[N];
int re(){
	int x=0,t=1;char c=getchar();
	for(;!isdigit(c);c=getchar())if(c=='-')t=-1;
	for(;isdigit(c);c=getchar())x=x*10+c-'0';
	return x*t;
}
void add(int x,int y){
	e[ct].to=y;
	e[ct].next=head[x];
	head[x]=ct++;
	e[ct].to=x;
	e[ct].next=head[y];
	head[y]=ct++;
}
void dfs(int x){
	a[++id]=x;
	vis[x]=1;
	int tmp[N],tid=0;
	for(int i=head[x];i;i=e[i].next)
		if(!vis[e[i].to])tmp[++tid]=e[i].to;
	sort(tmp+1,tmp+tid+1);
	for(int i=1;i<=tid;i++)
		dfs(tmp[i]);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=re();m=re();
	if(m==n-1){
		for(int i=1;i<=m;i++){
			int x=re(),y=re();
			add(x,y);
		}
		dfs(1);
		for(int i=1;i<=n;i++)
			printf("%d ",a[i]);
	}
	return 0;
}
