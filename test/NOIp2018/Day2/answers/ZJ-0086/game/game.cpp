#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<vector>
#include<cctype>
#include<iomanip>
#include<map>
#include<stack>
#include<cstring>
#include<string>
#define N 5010
using namespace std;
int n,m,a[6][6],ans=0;
int re(){
	int x=0,t=1;char c=getchar();
	for(;!isdigit(c);c=getchar())if(c=='-')t=-1;
	for(;isdigit(c);c=getchar())x=x*10+c-'0';
	return x*t;
}
bool judge()
{
	for(int i=0;i<n;i++)
		for(int j=0;j<m;j++)
		{
			if(i && j<m-1)
				if(a[i][j]>a[i-1][j+1])return 0;
		}
	return 1;
}
void dfs(int k)
{
	if(k==m*n)
	{
		if(judge())ans++;
		return;
	}
	a[k/m][k%m]=0;
	dfs(k+1);
	a[k/m][k%m]=1;
	dfs(k+1);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=re();m=re();
	dfs(0);
	printf("%d\n",ans);
	return 0;
}
