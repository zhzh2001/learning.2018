#include<bits/stdc++.h>
using namespace std;
int tag[100005],s[100005],cost[100005],son[100005],l[100005],ans,s1;
int head[100005],next[200005],edge[200005],nodenum=0,n,m,fa[100005];
char ch[5];
void add(int x,int y)
{
	nodenum++;
	next[nodenum]=head[x];
	head[x]=nodenum;
    edge[nodenum]=y;
}
void dfs(int u,int pre,int grand)
{
	tag[u]=1-tag[pre];
	s[u]=s[grand]+cost[u];
	if(son[u]==0 || son[son[u]]==0)
	{
		l[tag[u]]=s[u];
	}
	if(son[u]==0) 	return;
	dfs(son[u],u,pre);
}
void dfs2(int u,int is,int pre)
{
	if(tag[u]!=-1)
	{
		if(tag[pre]==tag[u] && tag[u]==0)
		{
			ans=-1;return;
		}
		if(tag[u]==is)
			ans=ans+tag[u]*cost[u];
		else
		{
			ans=ans+tag[u]*cost[u];
			is=tag[u];
		}
	}
	else
		ans=ans+is*cost[u];
	for(int i=head[u];i;i=next[i])
	{
		int v=edge[i];
		if(v==pre)	continue;
		dfs2(v,1-is,u);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int x,y,a,b,n,ans1,ans2;
	scanf("%d%d%s",&n,&m,&ch);
	if(ch[0]=='A')
	{
		for(int i=1;i<=n;i++)
		{
			scanf("%d",&cost[i]);
			fa[i]=0;
		}
		for(int i=1;i<n;i++)
		{
			scanf("%d%d",&x,&y);
			fa[y]=x;son[x]=y;
		}
		for(int i=1;i<=n;i++)
			if(fa[i]==0)
				dfs(i,0,0);
		for(int i=1;i<=m;i++)
		{
			scanf("%d%d%d%d",&x,&a,&y,&b);
			if(a==b)
				if(son[x]==y || son[y]==x || x==y)
				{
					printf("%d\n",-1);
					continue;
				}
			if(a==0)
			{
				swap(a,b);swap(x,y);
			}
			int ans1=l[1],ans2=l[0]-cost[son[x]]-cost[fa[x]]+cost[x];
			if(b==1)
			{
				if(tag[x]!=tag[y]) ans1+=cost[y];
				if(tag[x]==tag[y]) ans2+=cost[y];
			}
			else
			{
				if(tag[x]!=tag[y])
				{
					if((son[x]!=y && fa[x]!=y) || (son[y]!=x && fa[y]!=x)) 
						ans2=ans2-cost[y]+min(cost[fa[y]],cost[son[y]]);
				}
				if(tag[x]==tag[y])
					ans1=ans1-cost[y]+min(cost[fa[y]],cost[son[y]]);
			}
			printf("%d\n",min(ans1,ans2));
		}
	}
	else
	{
		for(int i=1;i<=n;i++)
		{
			scanf("%d",&cost[i]);
			tag[i]=-1;
		}
		tag[0]=-1;
		for(int i=1;i<n;i++)
		{
			scanf("%d%d",&x,&y);
			add(x,y);add(y,x);
		}
		for(int i=1;i<=m;i++)
		{
			scanf("%d%d%d%d",&x,&a,&y,&b);
			tag[x]=a;tag[y]=b;
			s1=1e9+7,ans=0;
			if(tag[1]==-1)
			{
				dfs2(1,0,0);
				s1=ans;ans=0;
				dfs2(1,1,0);
			}
			else
				dfs2(1,tag[1],0);
			s1=min(s1,ans);tag[x]=-1;tag[y]=-1;
			printf("%d\n",s1);
		}
	}
}
