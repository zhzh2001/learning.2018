#include<bits/stdc++.h>
using namespace std;
int n,m,ed[5005][5005],insta[5005],vis[5005],T=0,sta[5005],is[5005],son2,tag;
int ans[5005],num[5005],len=0,dad;
void dfs(int u,int pre)
{
	if(vis[u]==1)	return;
	ans[++len]=u;
	vis[u]=1;
	for(int i=1;i<=num[u];i++)
	{
		if(ed[u][i]!=pre)
			dfs(ed[u][i],u);
	}
}
void find(int u,int pre)
{
	if(insta[u]==1)
	{
		int tail=T;
		while(sta[tail]!=u)
		{
			is[sta[tail]]=1;
			tail--;
		}
		is[u]=1;
		for(int i=1;i<=num[u];i++)
			if(is[ed[u][i]]==1)
				son2=ed[u][i];
		dad=u;
		return;
	}
	insta[u]=1;T++;sta[T]=u;
	for(int i=1;i<=num[u];i++)
	{
		if(ed[u][i]!=pre)
			find(ed[u][i],u);
	}
	insta[u]=0;T--;
}
void dfs2(int u,int pre,int son2)
{
	ans[++len]=u;
	vis[u]=1;
	for(int i=1;i<=num[u];i++)
	{
		if(ed[u][i]==pre)	continue;
		if(is[ed[u][i]]==0 || (is[ed[u][i]]==1 && ed[u][i]<son2))
			dfs2(ed[u][i],u,son2);
		else 
			return;
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		ed[x][++num[x]]=y;
		ed[y][++num[y]]=x;
	}
	for(int i=1;i<=n;i++)
		sort(ed[i]+1,ed[i]+num[i]+1);
	if(m==n-1)
	{
		dfs(1,0);

	}
	else
	{
		find(1,0);
		dfs2(1,0,son2);
		dfs(son2,dad);
	}
	for(int i=1;i<len;i++)
		printf("%d ",ans[i]);	
	printf("%d\n",ans[len]);
}

