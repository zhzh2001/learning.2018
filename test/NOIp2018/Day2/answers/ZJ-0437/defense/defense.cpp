#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define min(x, y) ((x) < (y) ? (x) : (y))
#define infty 0x3f3f3f3f
#define N 100005
inline int get() {
	int num = 0; char ch = getchar();
	for ( ; ch < '0' || ch > '9'; ch = getchar()) ;
	for ( ; ch >= '0' && ch <= '9'; ch = getchar())
		num = (num << 3) + (num << 1) + (ch ^ '0');
	return num;
}
struct edgeType {
	int to, nxt;
} edge[N << 1];
int head[N];
inline void addEdge(int from, int to) {
	static int cnt = 0;
	edge[++cnt] = (edgeType){to, head[from]};
	head[from] = cnt;
}
int a, x, b, y;
int d[N][2], c[N];
inline bool can(int u) {
	return  !((u == a && !x) || (u == b && !y));
}
inline bool cant(int u) {
	return !((u == a && x) || (u == b && y));
}
inline void solve(int u, int p) {
	d[u][1] = can(u) ? c[u] : infty;
	d[u][0] = cant(u) ? 0 : infty;
	for (int i = head[u]; i; i = edge[i].nxt) {
		int v = edge[i].to;
		if (v == p) continue;
		solve(v, u);
		if (can(u))	 d[u][1] += min(d[v][0], d[v][1]);
		if (cant(u)) d[u][0] += d[v][1];
	}
	if (d[u][1] > infty) d[u][1] = infty;
	if (d[u][0] > infty) d[u][0] = infty;
}
int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	int n = get(), m = get();
	char type[3]; scanf("%s", type + 1);
	for (int i = 1; i <= n; i++)
		scanf("%d", &c[i]);
	for (int i = 1; i < n; i++) {
		int u = get(), v = get();
		addEdge(u, v);
		addEdge(v, u);
	}
	for (int i = 1; i <= m; i++) {
		a = get(), x = get(), b = get(), y = get();
		solve(1, 0);
		int ans = min(d[1][0], d[1][1]);
		printf("%d\n", ans >= infty ? -1 : ans);
	}
	return 0;
}
