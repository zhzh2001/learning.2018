#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <vector>
#define N 5005
inline int get() {
	int num = 0; char ch = getchar();
	for ( ; ch < '0' || ch > '9'; ch = getchar()) ;
	for ( ; ch >= '0' && ch <= '9'; ch = getchar())
		num = (num << 3) + (num << 1) + (ch ^ '0');
	return num;
}
inline void put(int num) {
	if (num < 10) { putchar(num + '0'); return; }
	put(num / 10); putchar(num % 10 + '0');
}
int n, m;
struct algo1 {
	std::vector< int > edge[N];
	int sequence[N], idx;
	algo1() {}
	inline void init() {
		for (int i = 1; i <= m; i++) {
			int u = get(), v = get();
			edge[u].push_back(v);
			edge[v].push_back(u);
		}
		for (int i = 1; i <= n; i++) 
			std::sort(edge[i].begin(), edge[i].end());
	}
	inline void dfs(int u, int p) {
		sequence[++idx] = u;
		for (unsigned int i = 0; i < edge[u].size(); i++) {
			int v = edge[u][i];
			if (v == p) continue;
			dfs(v, u);
		}
	}
	inline void solve() {
		dfs(1, 0);
	}
	inline void print() {
		for (int i = 1; i < n; i++)
			put(sequence[i]), putchar(' ');
		put(sequence[n]); 
	}
};
int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	n = get(), m = get();
		algo1 *solution = new algo1();
		solution->init(), solution->solve(), solution->print();
	return 0;
}
