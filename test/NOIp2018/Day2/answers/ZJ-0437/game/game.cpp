#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
int map[10][10];
int n, m, ans;
inline bool judge() {
	for (int i = 1; i <= n + m - 1; i++) {
		bool flag = false; int x, y;
		if (i <= n) x = 1, y = i;
		else x = i - n, y = m;
		while (x <= n && y >= 1) {
			if (map[x][y] == 0 && flag) return false;
			if (map[x][y]) flag = true; 
			x++, y--;
		}
	}
	return true;
}
inline void solve(int x, int y) {
	if (x > n) {
		if (judge()) {
			ans++;
		}
		return;
	}
	map[x][y] = 0;
	y < m ? solve(x, y + 1) : solve(x + 1, 1);
	map[x][y] = 1;
	y < m ? solve(x, y + 1) : solve(x + 1, 1);
}
int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	solve(1, 1);
	printf("%d\n", ans);
	return 0;
}
