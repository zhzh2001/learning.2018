#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int s[5001],n,m,f[5001][5001],a,b,r[5001],pk;
bool vis[5001],cir[5001],p[5001];
inline int read(){
	char c=getchar();
	int a=0;
	for(;c<'0'||c>'9';) c=getchar();
	for(;c>='0'&&c<='9';) a=(a<<1)+(a<<3)+int(c)-48,c=getchar();
	return a;
}
inline void check(int x,int &p){
	int minn=0x7fffffff,nxt;
	for(int i=1;i<=n;i++){
		if(cir[i]&&f[i][0]>2&i!=x){
			p=i; break;
		}
	}
	for(int i=1;i<=f[p][0];i++){
		if(cir[f[p][i]]==0){
			p=f[p][i]; break;
		}
	}
	for(int i=1;i<=f[x][0];i++)
	  if(!vis[i]){
	  	minn=f[x][i]; break;
	  }
	nxt=f[x][3];
	for(;nxt>minn&&vis[minn]==0;){
		vis[minn]=1;
		s[++s[0]]=minn;
		bool flag=0;
		for(int i=1;i<=f[minn][0];i++) if(!vis[f[minn][i]]){
			minn=f[minn][i]; flag=1; break;
		}
		if(flag==0) break;
	}
	if(vis[minn]==0){
		for(;vis[nxt]==0;){
			vis[nxt]=1;
			s[++s[0]]=nxt;
			bool flag=0;
			for(int i=1;i<=f[nxt][0];i++) if(!vis[f[nxt][i]]){
				nxt=f[nxt][i]; flag=1; break;
			}
			if(flag==0) break;
		}
	}
	return;
}
void search(int x){
	if(x==0) return;
	vis[x]=1; s[++s[0]]=x;
	int p;
	if(cir[x]) check(x,p),search(p);
	for(int i=1;i<=f[x][0];i++){
		if(!vis[f[x][i]]) search(f[x][i]);
	}
	return;
}
inline int topo(int x,int fa){
	p[x]=1;
	for(int i=1;i<=f[x][0];i++){
		if(f[x][i]==fa) continue;
		if(p[f[x][i]]==1){
			cir[x]=1; pk=f[x][i]; return 1;
		}
		int t=topo(f[x][i],x);
		if(t==2) return 2;
		if(t==1){
			if(x==pk){
				cir[x]=1; return 2;
			}
			cir[x]=1; return 1;
		}
	}
	return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=m;i++){
		a=read(); b=read();
		f[a][++f[a][0]]=b;
		f[b][++f[b][0]]=a;
		r[a]++;
		r[b]++;
	}
	for(int i=1;i<=n;i++) sort(f[i]+1,f[i]+int(f[i][0])+1);
	if(m==n){
		topo(1,-1);
		search(1);
		for(int i=1;i<=n;i++) printf("%d ",s[i]);
		printf("\n");
	}
	else{
		search(1);
		for(int i=1;i<=n;i++) printf("%d ",s[i]);
		printf("\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
