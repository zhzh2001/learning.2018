#include<cstdio>
#include<algorithm>
using namespace std;
#define N 256
int mo=1e9+7;
int n,m,ok[N][N][N];
int b[N][N],t1[N],t2[N],tmp,c[N][N];
int pd(int p1,int p2){
	tmp=p1;for(int i=0;i<n;i++)t1[i]=tmp&1,tmp>>=1;
	tmp=p2;for(int i=0;i<n;i++)t2[i]=tmp&1,tmp>>=1;
	for(int i=0;i<n-1;i++)if(t1[i+1]<t2[i])return 0;
	c[p1][p2]=n;
	for(int i=0;i<n-1;i++)if(t1[i+1]==t2[i]){c[p1][p2]=i+1;break;}
	for(int i=0;i<=n;i++){
		ok[p1][p2][i]=1;
		for(int j=0;j<n-1;j++)if(i<=j)if(t1[j+1]!=t2[j])ok[p1][p2][i]=0;
	}
	return 1;
}
int f[N][N],g[N][N];
inline void up(int &x,int y){x+=y;if(x>=mo)x-=mo;}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=0;i<(1<<n);i++)for(int j=0;j<(1<<n);j++)b[i][j]=pd(i,j);
	for(int i=0;i<(1<<n);i++)f[i][n]=1;
	
	for(int i=2;i<=m;i++){
		for(int j=0;j<(1<<n);j++)for(int k=0;k<=n;k++)g[j][k]=0;
		for(int j=0;j<(1<<n);j++)for(int k=0;k<=n;k++)if(f[j][k]){
			for(int jj=0;jj<(1<<n);jj++)if(b[j][jj]){
				if(ok[j][jj][k])up(g[jj][min(k,c[j][jj])],f[j][k]);
			}
		}
		for(int j=0;j<(1<<n);j++)for(int k=0;k<=n;k++)f[j][k]=g[j][k];
	}
	int ans=0;
	for(int j=0;j<(1<<n);j++)for(int k=0;k<=n;k++)up(ans,g[j][k]);
	printf("%d\n",ans);
}
