#include<cstdio>
using namespace std;
#define N 256
int mo=1e9+7;
int n,m;
int a[N][N],ans=0;
void dfs(int x,int y){
	if(x==n){ans++;return;}
	if(y==n){dfs(x+1,0);return;}
	a[x][y]=1;
	int fl=1;if(x>0&&y+1<n&&a[x-1][y+1]==0)fl=0;
	if(fl)dfs(x,y+1);
	fl=1;
	a[x][y]=0;if(x>1&&y+1<n&&y>0&&a[x-1][y+1]==1&&a[x-2][y]==a[x-1][y-1])fl=0;
	if(fl)dfs(x,y+1);
}
int main(){
	freopen("game3.in","r",stdin);
	//freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	dfs(0,0);printf("%d\n",ans);
}
