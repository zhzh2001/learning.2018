#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define LL long long
#define N 100010
LL inf=1e12;
struct edge{int to,nxt;}e[N<<1]; int hed[N],cnt=0;
void add(int x,int y){e[++cnt].nxt=hed[x];hed[x]=cnt;e[cnt].to=y;}
int p[N],fa[N],n,m;  char op1,op2;
LL s[N][2],ans; int X=0,XX,Y=0,YY;
void dfs(int x){
	for(int i=hed[x];i;i=e[i].nxt)if(e[i].to!=fa[x])fa[e[i].to]=x,dfs(e[i].to);
	s[x][1]=p[x];
	for(int i=hed[x];i;i=e[i].nxt)if(e[i].to!=fa[x])
		s[x][1]+=min(s[e[i].to][0],s[e[i].to][1]);
	s[x][0]=0;
	for(int i=hed[x];i;i=e[i].nxt)if(e[i].to!=fa[x])s[x][0]+=s[e[i].to][1];
	if(x==X){if(XX)s[x][0]=inf;else s[x][1]=inf;}
	if(x==Y){if(YY)s[x][0]=inf;else s[x][1]=inf;}
}
void cas1(){
	while(m--){
		X=read(),XX=read(),Y=read(),YY=read();
		if(X>Y)swap(X,Y),swap(XX,YY);
		dfs(1); ans=min(s[1][0],s[1][1]);
		if(ans>1e10)puts("-1");else printf("%lld\n",ans);
	}
}
LL ss[N][2];
#include<vector>
vector<int>TM;

void DEL(int X,int XX){
	//printf("DEL %d %d\n",X,XX);
	LL t0,t1,p0=s[X][0],p1=s[X][1];
	if(XX)s[X][0]=inf;else s[X][1]=inf;
	TM.push_back(X);
	for(int j=X,i;j>1;j=fa[j]){
		i=fa[j];
		t0=s[i][0]+s[j][1]-p1,t1=s[i][1]+min(s[j][0],s[j][1])-min(p0,p1);
		//printf("%d %lld %lld %lld %lld\n",i,s[i][0],s[i][1],t0,t1);
		if(t0!=s[i][0]||t1!=s[i][1])TM.push_back(i);else break;
		p0=s[i][0],p1=s[i][1]; s[i][0]=t0,s[i][1]=t1;
	}
}
void cas3(){
	dfs(1);
	for(int i=1;i<=n;i++)ss[i][0]=s[i][0],ss[i][1]=s[i][1];
	while(m--){
		X=read(),XX=read(),Y=read(),YY=read();
		DEL(X,XX),DEL(Y,YY);
		ans=min(s[1][0],s[1][1]);
		if(ans>1e10)puts("-1");else printf("%lld\n",ans);
		for(int i=0,x;i<TM.size();i++)x=TM[i],s[x][0]=ss[x][0],s[x][1]=ss[x][1];
		TM.clear();
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),op1=getchar(),op2=getchar();
	for(int i=1;i<=n;i++)p[i]=read();
	for(int i=1,x,y;i<n;i++)x=read(),y=read(),add(x,y),add(y,x);
	if(n<=2000&&m<=2000){cas1();return 0;}
	cas3();return 0;
}
