#include<cstdio>
#include<vector>
#include<algorithm>
#include<queue>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define N 5010
int n,m;
vector<int>e[N];
void dfs(int x,int fa){
	printf("%d ",x);
	for(int i=0,y;i<e[x].size();i++){
		y=e[x][i];if(y!=fa)dfs(y,x);
	}
}
int fa[N],vis[N],dep[N];
void DFS(int x){
	for(int i=0,y;i<e[x].size();i++){
		y=e[x][i];if(y==fa[x])continue;
		if(fa[y]!=-1&&dep[y]<dep[x]){
			for(int j=x;j!=fa[y];j=fa[j])vis[j]=1;
		}
		else if(fa[y]==-1)dep[y]=dep[x]+1,fa[y]=x,DFS(y);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1,x,y;i<=m;i++){
		x=read(),y=read(),e[x].push_back(y),e[y].push_back(x);
	}
	for(int i=1;i<=n;i++)sort(e[i].begin(),e[i].end());
	if(m==n-1){dfs(1,0);return 0;}
}
