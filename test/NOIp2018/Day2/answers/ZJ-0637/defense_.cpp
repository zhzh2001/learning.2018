#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define LL long long
#define N 100010
LL inf=1e12;
struct edge{int to,nxt;}e[N<<1]; int hed[N],cnt=0;
void add(int x,int y){e[++cnt].nxt=hed[x];hed[x]=cnt;e[cnt].to=y;}
int p[N],fa[N],n,m;  char op1,op2;
LL s[N][2],ans; int X=0,XX,Y=0,YY;
void dfs(int x){
	for(int i=hed[x];i;i=e[i].nxt)if(e[i].to!=fa[x])fa[e[i].to]=x,dfs(e[i].to);
	s[x][1]=p[x];
	for(int i=hed[x];i;i=e[i].nxt)if(e[i].to!=fa[x])
		s[x][1]+=min(s[e[i].to][0],s[e[i].to][1]);
	s[x][0]=0;
	for(int i=hed[x];i;i=e[i].nxt)if(e[i].to!=fa[x])s[x][0]+=s[e[i].to][1];
	if(x==X){if(XX)s[x][0]=inf;else s[x][1]=inf;}
	if(x==Y){if(YY)s[x][0]=inf;else s[x][1]=inf;}
}
void cas1(){
	while(m--){
		X=read(),XX=read(),Y=read(),YY=read();
		if(X>Y)swap(X,Y),swap(XX,YY);
		dfs(1); ans=min(s[1][0],s[1][1]);
		if(ans>1e10)puts("-1");else printf("%lld\n",ans);
	}
}
int main(){
	freopen("defense3.in","r",stdin);
	freopen("defense.ans","w",stdout);
	n=read(),m=read(),op1=getchar(),op2=getchar();
	for(int i=1;i<=n;i++)p[i]=read();
	for(int i=1,x,y;i<n;i++)x=read(),y=read(),add(x,y),add(y,x);
	if(n<=2000&&m<=2000){cas1();return 0;}
}
