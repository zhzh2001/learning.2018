var
  n,m,i,x,y:longint;
  a,b,l,r,sg,xg:array[0..10010] of longint;
  zg,hn:array[0..10010] of boolean;
  procedure wc1(s:string);
    begin
      assign(input,s+'.in');
      assign(output,s+'.out');
      reset(input);
      rewrite(output);
    end;
  procedure wc2;
    begin
      close(input);
      close(output);
    end;
  procedure jh(var x,y:longint);
    var
      t:longint;
    begin
      t:=x;
      x:=y;
      y:=t;
    end;
  procedure kp(l,r:longint);
    var
      zja,zjb,i,j:longint;
    begin
      i:=l;
      j:=r;
      zja:=a[(l+r) div 2];
      zjb:=b[(l+r) div 2];
      repeat
        while (a[i]<zja)or((a[i]=zja)and(b[i]<zjb)) do inc(i);
        while (a[j]>zja)or((a[j]=zja)and(b[j]>zjb)) do dec(j);
        if i<=j then
          begin
            jh(a[i],a[j]);
            jh(b[i],b[j]);
            inc(i);
            dec(j);
          end;
      until i>j;
      if i<r then kp(i,r);
      if l<j then kp(l,j);
    end;
  procedure zh(x:longint);
    var
      i:longint;
    begin
      if zg[x] then
        begin
          zg[0]:=true;
          y:=x;
          exit;
        end;
      zg[x]:=true;
      for i:=l[x] to r[x] do
        begin
          if b[i]=sg[x] then continue;
          sg[b[i]]:=x;
          xg[x]:=b[i];
          zh(b[i]);
          if zg[0] then exit;
        end;
      zg[x]:=false;
    end;
  procedure ss(x,qq,em:longint);
    var
      i:longint;
    begin
      zg[x]:=true;
      write(x,' ');
      if not hn[x] then
        begin
          for i:=l[x] to r[x] do
            if b[i]<>qq then ss(b[i],x,0);
        end
      else
        begin
          if not hn[qq] then
            begin
              for i:=l[x] to r[x] do
                if not zg[b[i]] then
                  begin
                    if (b[i]=xg[x])and(not zg[sg[x]]) then ss(b[i],x,sg[x])
                    else
                      if (b[i]=sg[x])and(not zg[xg[x]]) then ss(b[i],x,xg[x])
                      else ss(b[i],x,n+1);
                  end;
            end
          else
            begin
              if (zg[sg[x]])and(zg[xg[x]]) then
                begin
                  for i:=l[x] to r[x] do
                    if not zg[b[i]] then ss(b[i],x,0);
                  exit;
                end;
              if (zg[sg[x]])and(em<xg[x]) then
                begin
                  for i:=l[x] to r[x] do
                    if (b[i]<>xg[x])and(not zg[b[i]]) then ss(b[i],x,0);
                  exit;
                end;
              if (zg[xg[x]])and(em<sg[x]) then
                begin
                  for i:=l[x] to r[x] do
                    if( b[i]<>sg[x])and(not zg[b[i]]) then ss(b[i],x,0);
                  exit;
                end;
              for i:=l[x] to r[x] do
                begin
                  if zg[b[i]] then continue;
                  if (b[i]=sg[x])or(b[i]=xg[x]) then ss(b[i],x,em)
                  else ss(b[i],x,0);
                end;
            end;
        end;
    end;
begin
  wc1('travel');
  readln(n,m);
  for i:=1 to m do
    begin
      readln(x,y);
      a[i*2-1]:=x;
      b[i*2-1]:=y;
      a[i*2]:=y;
      b[i*2]:=x;
    end;
  m:=m*2;
  kp(1,m);
//for i:=1 to m do writeln(a[i],' ',b[i]);
  for i:=1 to n do r[i]:=-1;
  for i:=1 to m do r[a[i]]:=i;
  for i:=m downto 1 do l[a[i]]:=i;
//for i:=1 to n do writeln(l[i],' ',r[i]);
  if n*2=m then zh(1);
  hn[y]:=true;
//writeln(y);
  while not hn[xg[y]] do
    begin
      y:=xg[y];
      hn[y]:=true;
//writeln(y);
    end;
  fillchar(zg,sizeof(zg),0);
  ss(1,0,0);
  wc2;
end.
