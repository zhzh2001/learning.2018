const
  MS=1000000007;
var
  n,m,da,i:int64;
  j,k:longint;
     procedure wc1(s:string);
    begin
      assign(input,s+'.in');
      assign(output,s+'.out');
      reset(input);
      rewrite(output);
    end;
  procedure wc2;
    begin
      close(input);
      close(output);
    end;

  function cm(x,y:int64):int64;
    var
      jg:int64;
    begin
      jg:=1;
      while y>0 do
        begin
          if y mod 2=1 then jg:=jg*x mod MS;
          x:=x*x mod MS;
          y:=y div 2;
        end;
      exit(jg);
    end;
begin
wc1('game');
  readln(n,m);
  if m<n then
    begin
      m:=n+m;
      n:=m-n;
      m:=m-n;
    end;
  if n=1 then
    begin
      writeln(cm(2,m));
      halt;
    end;
  if n=2 then
    begin
      writeln((4*cm(3,m-1)) mod MS);
      halt;
    end;
  da:=4*cm(2,n-1) mod MS;
  da:=da*cm(2,2*n-4) mod MS;
  da:=da*cm(3,n+m-3-n+1-n+2) mod MS;
  i:=da;
  da:=1;
  k:=4;
  for j:=4 to m do
    begin
      if k<=n then da:=da*5 mod MS
      else da:=da*4 mod MS;
      inc(k);
    end;
  if m>n then da:=da*3 mod MS;
  da:=da*cm(2,n-2);
  i:=i+da;
  da:=1;
  k:=4;
  for j:=4 to m do
    begin
      if k<=n then da:=da*5 mod MS
      else da:=da*4 mod MS;
      inc(k);
    end;
  da:=da*(cm(2,n-2));
  da:=da+i;
  writeln(da);
  wc2;
end.
