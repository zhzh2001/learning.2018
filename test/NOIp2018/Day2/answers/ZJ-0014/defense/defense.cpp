#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 100010
#define min(x,y) ((x)<(y) ? (x) : (y))

struct Edge {
	int to,ne;
} e[N<<1];

char s[10];
int d[N][2],a[N],g[N],x,y,n,m,cnt,xa,ya,xb,yb;

void dfs(int u,int fa) {
	d[u][0]=0;d[u][1]=a[u];
	for(int i=g[u];i!=-1;i=e[i].ne) {
		int v=e[i].to;
		if(v==fa) continue;
		dfs(v,u);
		d[u][1]+=min(d[v][0],d[v][1]);
		d[u][0]+=d[v][1];
	}
	if(xa==u) {
		if(ya) d[u][0]=1e9;
		else   d[u][1]=1e9;
	}
	if(xb==u) {
		if(yb) d[u][0]=1e9;
		else   d[u][1]=1e9;
	}
}

int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for(int i=1;i<=n;i++) {
		scanf("%d",&a[i]);
	}
	cnt=0;memset(g,-1,sizeof g);
	for(int i=1;i<n;i++) {
		scanf("%d%d",&x,&y);
		e[cnt]=(Edge){y,g[x]};
		g[x]=cnt++;
		e[cnt]=(Edge){x,g[y]};
		g[y]=cnt++;
	}
	memset(d,0,sizeof d);
	for(int i=1;i<=m;i++) {
		scanf("%d%d%d%d",&xa,&ya,&xb,&yb);
		dfs(1,0);
		int acht=min(d[1][0],d[1][1]);
		if(acht<1e9) printf("%d\n",acht);
		else printf("-1\n");
	}
	return 0;
}
