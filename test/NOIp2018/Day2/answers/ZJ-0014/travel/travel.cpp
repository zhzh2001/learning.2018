#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<queue>
#define N 5010
using namespace std;

vector<int> g[N];
//priority_queue<int,vector<int>,greater<int> > q;
int n,m,cnt,Index,From,To,ans[N],acht[N],f[N],b[N];
bool vis[N];

void dfs(int u,int fa) {
	ans[++cnt]=u;
	for(unsigned int i=0;i<g[u].size();i++) {
		int v=g[u][i];
		if(v==fa || (u==From && v==To) || (u==To && v==From)) continue;
		dfs(v,u);
	}
}

void solve1() {
	cnt=0;
	From=-1;To=-1;
	dfs(1,0);
	for(int i=1;i<n;i++) printf("%d ",ans[i]);
	printf("%d\n",ans[n]);
}

void dfsc(int u,int fa) {
	vis[u]=true;
	f[u]=fa;
	for(unsigned int i=0;i<g[u].size();i++) {
		int v=g[u][i];
		if(v==fa) continue;
		if(vis[v]) {
			int j=u;
			b[++Index]=u;
			while(j!=v) {
				j=f[j];
				b[++Index]=j;
			}
		}
		if(Index) return;
		dfsc(v,u);
		if(Index) return;
	}
}

void Min() {
	bool flag=false;
	for(int i=1;i<=n;i++) {
		if(acht[i]>ans[i]) {
			flag=true;break;
		}
		if(acht[i]<ans[i]) {
			return;
		}
	}
	if(flag) {
		memcpy(acht,ans,sizeof ans);
	}
}

void solve2() {
	memset(vis,0,sizeof vis);
	memset(b,0,sizeof b);
	memset(f,0,sizeof f);
	Index=0;
	dfsc(1,0);
	memset(acht,63,sizeof acht);
	for(int i=1;i<Index;i++) {
		From=b[i];
		To=b[i+1];
		cnt=0;
		dfs(1,0);
		Min();
	}
	From=b[Index];
	To=b[1];
	dfs(1,0);
	Min();
	for(int i=1;i<n;i++) {
		printf("%d ",acht[i]);
	}
	printf("%d\n",acht[n]);
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	int x,y;
	for(int i=1;i<=m;i++) {
		scanf("%d%d",&x,&y);
		g[x].push_back(y);
		g[y].push_back(x);
	}
	for(int i=1;i<=n;i++) {
		sort(g[i].begin(),g[i].end());
	}
	if(m==n-1) {
		solve1();
	} else if(m==n) {
		solve2();
	}
	return 0;
}
