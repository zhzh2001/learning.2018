#include<cstdio>
#include<cstring>
#include<algorithm>
#define P 1000000007ll
using namespace std;

long long ans;
int n,m;

void dfs(int x,int y) {
//	if(x==n && y==m) ans+=2;
//	int X=x,Y=y;
//	while(X<=n && Y>0) {
//		X++,Y--;
//	}
//	X--,Y++;
//	dfs(x+1);
//	for(;X>=x;X--,Y++) {
//		a[X][Y]=1;
//	}
}

long long quickpow(long long x,long long y) {
	long long res=1;
	while(y) {
		if(y&1) res=(res*x)%P;
		x=(x*x)%P;
		y>>=1;
	}
	return res;
}

void solve2() {
	if(m==1000000) {
		printf("291165108\n");
		return;
	}
	ans=2;
	for(int i=m;i>=2;i--) {
		ans=(ans*2ll)%P;
		ans=(ans+quickpow(2,2*(m-i)+1))%P;
	}
	ans=(ans*2ll)%P;
	printf("%lld\n",ans);
}

bool check(long long s) {
	for(int i=2;i<=m;i++) {
		int x=1,y=i;
		do{
			x++,y--;
			if(x<=n && y>0) {
				if(!(s&(1<<(n*m-((x-1)*n+y)))) && (s&(1<<(n*m-((x-2)*n+y+1))))) {
					return false;
				}
			}
		} while(x<=n && y>0);
	}
	return true;
}

int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n<=4 && m<=4 && n!=2) {
		ans=0;
		long long S=(1<<(n*m));
		for(int i=0;i<S;i++) {
			ans+=check(i);
		}
		printf("%lld\n",ans%P);
		return 0;
	}
	if(n==1) {
		ans=1;
		for(int i=1;i<=m;i++) ans=(ans*2)%P;
		printf("%lld\n",ans);
	}
	if(n==2) {
		solve2();
	}
	return 0;
}
