#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define rep(i,a,b) for(register int i=(a);i<(b);++i)
#define Rep(i,a,b) for(register int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=(b);--i)
#define gc getchar
#define pc putchar
inline ll read(){
	ll x = 0;int f = 1;char c = gc();
	for(;!isdigit(c);c=gc()) if(c=='-') f = -1;
	for(;isdigit(c);c=gc()) x = (x << 1) + (x << 3) + (c ^ '0');
	return f * x;
}
inline void write(ll x){if(x < 0){pc('-');x = -x;}if(x>=10) write(x/10);pc(x%10+'0');}
inline void writeln(ll x){write(x),pc('\n');}
inline void wri(ll x){write(x),pc(' ');}
const int mod = 1e9+7;
vector<int> edge[100005];
ll p[100005],h[100005],g[100005],f[100005],ff[100005],gg[100005];
int a,x,b,y;
int n,m;
void dfs(int u,int fa){
	f[u] = p[u];g[u] = 0;
	for(unsigned i=0;i<edge[u].size();++i){
		int v=edge[u][i];
		if(v==fa) continue;
		dfs(v,u);
		f[u] += min(f[v],g[v]);
		g[u] += f[v];
	}
	if(a==u && x==1 || b==u && y==1){
		g[u] = 1ll << 50; 
	}//这个地方必须
	else
	if(a==u && x==0 || b==u && y==0){
		f[u] = 1ll << 50;
	}//这个地方不能选 
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char type[23];
	n = read(),m = read();scanf("%s",type);
	if(type[0] == 'A' && type[1] == 2){
		Rep(i,1,n) p[i] = read();
		rep(i,1,n) read(),read();
		f[1] = p[1];g[1] = 0;
		Rep(i,2,n){
			f[i] = p[i] + min(f[i-1],g[i-1]);
			g[i] = f[i-1];
		}
		ff[n] = p[n];gg[1] = 0;
		Dep(i,n-1,1){
			ff[i] = p[i] + min(ff[i+1],gg[i+1]);
			gg[i] = gg[i+1];
		}
		while(m--){
			int a = read(),x = read(),b = read(),y = read();
			if(x&&y) writeln(f[a]+ff[b]); else
			if(x==1&&y==0) writeln(f[a]+gg[b]); else
			if(x==0&&y==1) writeln(g[a]+ff[b]); else
			puts("-1");
		}
		return 0;
	}
	Rep(i,1,n) p[i] = read();
	rep(i,1,n){
		int a = read(),b = read();
		edge[a] . push_back(b);
		edge[b] . push_back(a);
	}
	while(m--){
		a=read(),x=read();b=read(),y=read();
		dfs(1,0);
		ll ans = min(g[1],f[1]);
		if(ans >= (1ll<<50)){
			puts("-1"); 
		} else{
			writeln(ans);
		}
	}
	return 0;
}
