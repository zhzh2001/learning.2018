#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define rep(i,a,b) for(register int i=(a);i<(b);++i)
#define Rep(i,a,b) for(register int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=(b);--i)
#define gc getchar
#define pc putchar
inline ll read(){
	ll x = 0;int f = 1;char c = gc();
	for(;!isdigit(c);c=gc()) if(c=='-') f = -1;
	for(;isdigit(c);c=gc()) x = (x << 1) + (x << 3) + (c ^ '0');
	return f * x;
}
inline void write(ll x){if(x < 0){pc('-');x = -x;}if(x>=10) write(x/10);pc(x%10+'0');}
inline void writeln(ll x){write(x),pc('\n');}
inline void wri(ll x){write(x),pc(' ');}
#define fi first
#define se second
const int maxn = 5233;
vector<int> edge[maxn];
int fa[maxn],ans[maxn],f[maxn],n,m;
int c[maxn];
bool vis[maxn];
pair<int,int> limit;
bool findc(int u){
	vis[u] = true;
	for(unsigned i=0;i<edge[u].size();++i){
		int v=edge[u][i];
		if(v==fa[u]) continue;
		fa[v] = u;
		if(!vis[v]){
			if(findc(v)) return true;
		} else{
			int x = u;
			for(;;){
				c[++c[0]] = x;
				x = fa[x];
				if(x == fa[v]) break;
			}
			return true;
		}
	}
	return false;
}
void solve(int u,int fa){
	f[++f[0]] = u;
	for(unsigned i=0;i<edge[u].size();++i){
		int v=edge[u][i];
		if(v==fa) continue;
		if(u==limit.fi && v==limit.se) continue;
		if(u==limit.se && v==limit.fi) continue;
		solve(v,u);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n = read(),m = read();
	Rep(i,1,m){
		int a = read(),b = read();
		edge[a] . push_back(b);
		edge[b] . push_back(a);
	}
	Rep(i,1,n) ans[i] = n+1;
	Rep(i,1,n) sort(edge[i].begin(),edge[i].end());
	if(m==n){
		memset(vis,false,sizeof(vis));
		fa[1] = 0;
		findc(1);//找到环并存在c数组中，c[0]表示环长度 
		for(int i=1;i<=c[0];++i){
			int j=i%c[0]+1;
			limit = make_pair(c[i],c[j]);
			f[0] = 0;
			solve(1,0);
			bool flag = false;
			Rep(i,1,n){
				if(f[i] < ans[i]){flag = true;break;}
				if(f[i] > ans[i])break;
			}
			if(flag) Rep(i,1,n) ans[i] = f[i];
		}
	} else{
		f[0] = 0;
		solve(1,0);
		Rep(i,1,n) ans[i] = f[i];
	}
	Rep(i,1,n) if(i!=n)wri(ans[i]); else write(ans[i]);
	return 0;
}
