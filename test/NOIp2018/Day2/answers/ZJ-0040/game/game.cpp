#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define rep(i,a,b) for(register int i=(a);i<(b);++i)
#define Rep(i,a,b) for(register int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=(b);--i)
#define gc getchar
#define pc putchar
inline ll read(){
	ll x = 0;int f = 1;char c = gc();
	for(;!isdigit(c);c=gc()) if(c=='-') f = -1;
	for(;isdigit(c);c=gc()) x = (x << 1) + (x << 3) + (c ^ '0');
	return f * x;
}
inline void write(ll x){if(x < 0){pc('-');x = -x;}if(x>=10) write(x/10);pc(x%10+'0');}
inline void writeln(ll x){write(x),pc('\n');}
inline void wri(ll x){write(x),pc(' ');}
#define fi first
#define se second
pair<int,int> pos[233],last[233];
int a[233][233];
int n,m;
bool check(){
	bool FLAG = true;
	rep(i,0,1<<(n+m-2)){
		pos[0] = make_pair(0,0);
		bool flag = false;
		Dep(W,n+m-3,0){
			int t=n+m-3-W;
			pos[t+1] = pos[t];
			if(i >> W & 1) pos[t+1] . se++;
					  else pos[t+1] . fi++;//优先向下走 
			if(pos[t+1].fi >= n){
				flag = true;
				break;
			}
			if(pos[t+1].se >= m){
				flag = true;
				break;
			}
		}
		//puts("");
		if(flag) continue;
		if(!FLAG){
			rep(i,0,n+m-1){
				if(a[pos[i].fi][pos[i].se] > a[last[i].fi][last[i].se]) return false;
				if(a[pos[i].fi][pos[i].se] < a[last[i].fi][last[i].se]) break;
			}
		}
		FLAG = false;
		rep(i,0,n+m-1) last[i] = pos[i];
	}
	return true;
}
const int mod = 1e9+7;
inline int qpow(int a,int b){
	int ans = 1;
	for(;b;b>>=1,a=1ll*a*a%mod)
		if(b&1) ans=1ll*ans*a%mod;
	return ans;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
//	freopen("db.txt","w",stdout);
	n = read(),m = read();
	if(n==1){
		writeln(qpow(2,m));
		return 0;
	} else
	if(n==2){
		writeln(4ll*qpow(3,m-1)%mod);
		return 0;
	} else
	if(n==3 && m>=4){
		writeln(336ll*qpow(3,m-4)%mod);
		return 0;
	}
	int ans = 0;
	rep(t,0,(1<<(n*m))){
		rep(i,0,n)
			rep(j,0,m)
				a[i][j] = t >> (i*m+j) & 1;
		if(!(a[0][0]==1&&a[n-1][m-1]==1)) continue;
		bool flag = false;
		rep(i,1,n){
			rep(j,0,m-1){
				if(a[i][j] < a[i-1][j+1]){
					flag = true;
					break;
				}
			}
		}
		if(flag) continue;
		if(check()){
			ans++;
		}
	}
	writeln(4*ans);
	return 0;
}
