#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

int n,m;
int head[5011],to[10011],nxt[10011],tot;
int ans[5011],cnt;
vector<int> son[5011];
int fa[5011],tfa[5011];
struct edge
{
	int x,y;
}e[5011];
int cir[5011],top;
int banit;

ll read()
{
   ll x=0,f=1;char c=getchar();
   while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
   while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
   return x*f;
}

void write(ll x)
{
	 if (x<0) putchar('-'),x=-x;
	 if (x>=10) write(x/10);
	 putchar(x%10+'0');
}

void writeln(ll x){write(x);puts("");}
void writern(ll x){write(x);putchar(' ');}

void add(int fr,int tt)
{
	to[++tot]=tt;nxt[tot]=head[fr];head[fr]=tot;
	to[++tot]=fr;nxt[tot]=head[tt];head[tt]=tot;
}

void dfs(int x,int p)
{
	for (int i=head[x];i;i=nxt[i])
		if (to[i]!=p)
		{
			son[x].push_back(to[i]);
			dfs(to[i],x);
		}
}

void work1(int x)
{
	ans[++cnt]=x;
	for (unsigned int i=0;i<son[x].size();i++)
		work1(son[x][i]);
}

int findf(int x)
{
	return x==fa[x]?x:fa[x]=findf(fa[x]);
}

void dfs2(int x)
{
	if (x==e[cir[1]].x)
	{
		for (int i=head[x];i;i=nxt[i])
			if (to[i]!=fa[x] && to[i]!=e[cir[1]].y)
			{
				fa[to[i]]=x;tfa[to[i]]=(i+1)>>1;
				dfs2(to[i]);
			}
	}
	else if (x==e[cir[1]].y)
	{
		for (int i=head[x];i;i=nxt[i])
			if (to[i]!=fa[x] && to[i]!=e[cir[1]].x)
			{
				fa[to[i]]=x;tfa[to[i]]=(i+1)>>1;
				dfs2(to[i]);
			}
	}
	else
	{
		for (int i=head[x];i;i=nxt[i])
			if (to[i]!=fa[x])
			{
				fa[to[i]]=x;tfa[to[i]]=(i+1)>>1;
				dfs2(to[i]);
			}
	}
}

bool vis[5011];

void work2()
{
	int xx=e[cir[1]].x,yy=e[cir[1]].y,po;
	vis[xx]=1;
	while (fa[xx]) {xx=fa[xx];vis[xx]=1;}
	if (vis[yy]==1) po=yy;
	while (fa[yy])
	{
		yy=fa[yy];
		if (vis[yy]==1) {po=yy;break;}
	}
	xx=e[cir[1]].x,yy=e[cir[1]].y;
	while (xx!=po)
	{
		cir[++top]=tfa[xx];xx=fa[xx];
	}
	while (yy!=po)
	{
		cir[++top]=tfa[yy];yy=fa[yy];
	}
}

int tmpans[5011],tmpcnt;

void dfs3(int x,int p)
{
	if (x==e[banit].x)
	{
		for (int i=head[x];i;i=nxt[i])
			if (to[i]!=p&&to[i]!=e[banit].y)
			{
				son[x].push_back(to[i]);
				dfs3(to[i],x);
			}
	}
	else if (x==e[banit].y)
	{
		for (int i=head[x];i;i=nxt[i])
			if (to[i]!=p&&to[i]!=e[banit].x)
			{
				son[x].push_back(to[i]);
				dfs3(to[i],x);
			}
	}
	else
	{
		for (int i=head[x];i;i=nxt[i])
			if (to[i]!=p)
			{
				son[x].push_back(to[i]);
				dfs3(to[i],x);
			}
	}
}

void work3(int x)
{
	tmpans[++tmpcnt]=x;
	for (unsigned int i=0;i<son[x].size();i++)
		work3(son[x][i]);
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=n;i++) fa[i]=i;
	for (int i=1;i<=m;i++)
	{
		int xx=read(),yy=read();
		add(xx,yy);
		e[i]=(edge){xx,yy};
	}
	if (n==m+1)
	{
		dfs(1,0);
		for (int i=1;i<=n;i++)
			if (son[i].size()>1)
				sort(son[i].begin(),son[i].end());
		work1(1);
		for (int i=1;i<n;i++)
			writern(ans[i]);
		writeln(ans[n]);
		return 0;
	}
	for (int i=1;i<=m;i++)
	{
		int fx=findf(e[i].x),fy=findf(e[i].y);
		if (fx==fy)
		{
			cir[++top]=i;
			break;
		}
		fa[fx]=fy;
	}
	memset(fa,0,sizeof fa);
	for (int i=1;i<=n;i++) ans[i]=123456789;
	dfs2(1);
	work2();
	for (int ta=1;ta<=top;ta++)
	{
		banit=cir[ta];
		tmpcnt=0;
		for (int i=1;i<=n;i++) son[i].clear();
		dfs3(1,0);
		for (int i=1;i<=n;i++)
			if (son[i].size()>1)
				sort(son[i].begin(),son[i].end());
		work3(1);
		bool flag=1;
		for (int i=1;i<=n;i++)
		{
			if (ans[i]>tmpans[i])break;
			if (ans[i]<tmpans[i]){flag=0;break;}
		}
		if (flag)
		{
			for (int i=1;i<=n;i++) ans[i]=tmpans[i];
		}
	}
	for (int i=1;i<n;i++)
		writern(ans[i]);
	writeln(ans[n]);
	return 0;
}

