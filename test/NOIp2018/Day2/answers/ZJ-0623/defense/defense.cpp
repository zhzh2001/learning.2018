#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

char tp[5];
int n,m,aa,xx,bb,yy;
ll p[100011];
int head[100011],nxt[200011],to[200011],tot,fat[100011];
ll f[100011][2];
ll g[100011][2];

ll read()
{
   ll x=0,f=1;char c=getchar();
   while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
   while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
   return x*f;
}

void write(ll x)
{
	 if (x<0) putchar('-'),x=-x;
	 if (x>=10) write(x/10);
	 putchar(x%10+'0');
}

void writeln(ll x){write(x);puts("");}
void writern(ll x){write(x);putchar(' ');}

void add(int fr,int tt)
{
	to[++tot]=tt;nxt[tot]=head[fr];head[fr]=tot;
	to[++tot]=fr;nxt[tot]=head[tt];head[tt]=tot;
}

bool dp(int x,int pp)
{
	if (x==aa&&pp==bb&&xx==0&&yy==0)return 0;
	if (pp==aa&&x==bb&&xx==0&&yy==0)return 0;
	bool b1=0;
	for (int i=head[x];i;i=nxt[i])
		if (to[i]!=pp)
		{
			b1=1;fat[to[i]]=x;
			if (!dp(to[i],x)) return 0;
//			if (f[to[i]][1]>f[to[i]][0]) b2=0;
		}
	if (!b1)
	{
		if (x==aa)
		{
			if (xx==0)
			{
				f[x][0]=0;f[x][1]=1e13;
			}
			else
			{
				f[x][0]=1e15;f[x][1]=p[x];
			}
		}
		else if (x==bb)
		{
			if (yy==0)
			{
				f[x][0]=0;f[x][1]=1e13;
			}
			else
			{
				f[x][0]=1e15;f[x][1]=p[x];
			}
		}
		else
		{
			f[x][0]=0;f[x][1]=p[x];
		}
		return 1;
	}
	if ((x==aa&&xx==0)||(x==bb&&yy==0))
	{
		f[x][1]=1e14;
		for (int i=head[x];i;i=nxt[i])
			if (to[i]!=pp)
				f[x][0]+=f[to[i]][1];
		return 1;
	}
	if ((x==aa&&xx==1)||(x==bb&&yy==1))
	{
		f[x][0]=1e14;
		for (int i=head[x];i;i=nxt[i])
			if (to[i]!=pp)
				f[x][1]+=min(f[to[i]][1],f[to[i]][0]);
		f[x][1]+=p[x];
		return 1;
	}
	for (int i=head[x];i;i=nxt[i])
		if (to[i]!=pp)
		{
			f[x][0]+=f[to[i]][1];
			f[x][1]+=min(f[to[i]][1],f[to[i]][0]);
		}
	f[x][1]+=p[x];
	return 1;
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();scanf("%s",tp+1);
	for (int i=1;i<=n;i++)
		p[i]=read();
	for (int i=1;i<n;i++)
	{
		xx=read(),yy=read();
		add(xx,yy);
	}
	if (n<=2000)
	{
		for (int i=1;i<=m;i++)
		{
			aa=read(),xx=read(),bb=read(),yy=read();
			for (int i=1;i<=n;i++) f[i][0]=f[i][1]=0;
			if (!dp(1,0))
				puts("-1");
			else
				writeln(min(f[1][0],f[1][1]));
		}
		return 0;
	}
/*	if (tp[1]=='A'&&tp[2]=='1')
	{
		dp(1,0);
		for (int i=1;i<=m;i++)
		{
			aa=read(),xx=read(),bb=read(),yy=read();
			for (int j=1;j<=n;j++) g[j][0]=f[j][0],g[j][1]=f[j][1];
			g[fat[tmp]][1-yy]
			int tmp=bb;
		}
		return 0;
	}*/
	return 0;
}

