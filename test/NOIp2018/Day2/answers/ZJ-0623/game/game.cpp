#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const ll mod=1e9+7;
int n,m;

ll read()
{
   ll x=0,f=1;char c=getchar();
   while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
   while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
   return x*f;
}

void write(ll x)
{
	 if (x<0) putchar('-'),x=-x;
	 if (x>=10) write(x/10);
	 putchar(x%10+'0');
}

void writeln(ll x){write(x);puts("");}
void writern(ll x){write(x);putchar(' ');}

ll qpow(ll a,int b)
{
	ll r=1;
	while (b)
	{
		if (b&1) r=r*a%mod;
		a=a*a%mod;
		b>>=1;
	}
	return r;
}

int a[5][5],tot;

bool check()
{
	int ix,iy,jx,jy;
	for (int i=0;i<(1<<(n+m-2));i++)
	{
		for (int j=i+1;j<(1<<(n+m-2));j++)
		{
			ix=iy=jx=jy=1;
			for (int k=(1<<(n+m-3));k>=1;k>>=1)
			{
				if (i&k) iy++;
				else ix++;
				if (j&k) jy++;
				else jx++;
				if (iy>m||ix>n||jy>m||jx>n) break;
				if (a[ix][iy]>a[jx][jy])
					break;
				else if (a[ix][iy]<a[jx][jy])
					return 0;
			}
		}
	}
	return 1;
}

void dfs(int x,int y)
{
	if (x>n)
	{
		if (check())
		{
			tot++;
//			putchar('#');
//			writeln(tot);
//			for (int i=1;i<=n;i++)
//			{
//				for (int j=1;j<m;j++) writern(a[i][j]);
//				writeln(a[i][m]);
//			}
		}
		return;
	}
	int nx,ny;
	if (y==m)
	{
		nx=x+1;ny=1;
	}
	else nx=x,ny=y+1;
	dfs(nx,ny);
	a[x][y]=1;
	dfs(nx,ny);
	a[x][y]=0;
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if (n==1)
	{
		writeln(qpow(2,m));return 0;
	}
	if (m==1)
	{
		writeln(qpow(2,n));return 0;
	}
	if (n==2)
	{
		writeln(qpow(3,m-1)*4%mod);return 0;
	}
	if (n<=3 && m<=3)
	{
		dfs(1,1);
		writeln(tot);
	}
	return 0;
}

