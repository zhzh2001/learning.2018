#include <algorithm>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <vector>
using namespace std;
typedef long long LL;
char isd(char ch){
	return '0'<=ch&&ch<='9';
}
int read(){
	int x=0;
	char ch=getchar();
	while (!isd(ch))
		ch=getchar();
	while (isd(ch))
		x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return x;
}
const int N=4005;
struct Gragh{
	static const int N=100005,M=N*2;
	int cnt,y[M],nxt[M],fst[N];
	void clear(){
		cnt=1;
		memset(fst,0,sizeof fst);
	}
	void add(int a,int b){
		y[++cnt]=b,nxt[cnt]=fst[a],fst[a]=cnt;
	}
}g;
const LL INF=10000000000000LL;
const LL LIM=1000000000000LL;
char type[10];
int n,m,v[100005];
int A,X,B,Y;
LL dp[N][2];
void dfs(int x,int pre){
	dp[x][0]=0,dp[x][1]=v[x];
	for (int i=g.fst[x];i;i=g.nxt[i]){
		int y=g.y[i];
		if (y==pre)
			continue;
		dfs(y,x);
		dp[x][0]+=dp[y][1];
		dp[x][1]+=min(dp[y][0],dp[y][1]);
	}
	if (x==A)
		dp[x][X^1]=INF;
	if (x==B)
		dp[x][Y^1]=INF;
}
namespace pt1{
	const int N=100005;
	LL dp[N][2];
	struct Query{
		int b,y;
		LL ans;
	}q[N];
	vector <int> id[N];
	void update(int x,int y,int k){
		dp[x][0]+=dp[y][1]*k;
		dp[x][1]+=min(dp[y][0],dp[y][1])*k;
	}
	void dfs(int x,int pre){
		dp[x][0]=0,dp[x][1]=v[x];
		for (int i=g.fst[x];i;i=g.nxt[i]){
			int y=g.y[i];
			if (y==pre)
				continue;
			dfs(y,x);
			update(x,y,1);
		}
	}
	void dfs2(int x,int pre){
		for (int i=0;i<id[x].size();i++)
			q[id[x][i]].ans=dp[x][q[id[x][i]].y];
		for (int i=g.fst[x];i;i=g.nxt[i]){
			int y=g.y[i];
			if (y==pre)
				continue;
			update(x,y,-1);
			update(y,x,1);
			dfs2(y,x);
			update(y,x,-1);
			update(x,y,1);
		}
	}
	void solve(){
		for (int i=1;i<=n;i++)
			id[i].clear();
		for (int i=1;i<=m;i++){
			q[i].b=read();
			q[i].y=read();
			q[i].b=read();
			q[i].y=read();
			id[q[i].b].push_back(i);
		}
		dp[1][0]=INF;
		dfs2(1,0);
		for (int i=1;i<=m;i++){
			if (q[i].ans>=LIM)
				q[i].ans=-1;
			printf("%lld\n",q[i].ans);
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();
	scanf("%s",type);
	for (int i=1;i<=n;i++)
		v[i]=read();
	g.clear();
	for (int i=1;i<n;i++){
		int a=read(),b=read();
		g.add(a,b);
		g.add(b,a);
	}
	if (n>4000&&type[1]=='1'){
		pt1 :: solve();
		return 0;
	}
	while (m--){
		A=read(),X=read(),B=read(),Y=read();
		dfs(1,0);
		LL ans=min(dp[1][0],dp[1][1]);
		if (ans>=LIM)
			ans=-1;
		printf("%lld\n",ans);
	}
	return 0;
}
