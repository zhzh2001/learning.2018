#include <algorithm>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
using namespace std;
char isd(char ch){
	return '0'<=ch&&ch<='9';
}
int read(){
	int x=0;
	char ch=getchar();
	while (!isd(ch))
		ch=getchar();
	while (isd(ch))
		x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return x;
}
const int N=8,M=1000105,mod=1e9+7;
int n,m;
void Add(int &x,int y){
	if ((x+=y)>=mod)
		x-=mod;
}
int val[M];
int Pow(int x,int y){
	int ans=1;
	for (;y;y>>=1,x=1LL*x*x%mod)
		if (y&1)
			ans=1LL*ans*x%mod;
	return ans;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if (n>m)
		swap(n,m);
	if (n==1){
		int ans=1;
		for (int i=1;i<=m;i++)
			ans=2LL*ans%mod;
		printf("%d\n",ans);
		return 0;
	}
	if (n==2){
		int ans=4;
		for (int i=1;i<=m-1;i++)
			ans=3LL*ans%mod;
		printf("%d\n",ans);
		return 0;
	}
	int ans=0;
	/*
	0 0
	0
	*/
	ans=(4LL*Pow(4,n-2)%mod
			*Pow(3,m-n)%mod
			*Pow(2,n-1)+ans)%mod;
	/*
	0 0 0
	1 0
	0
	*/
	ans=(4LL*Pow(4,n-3)%mod
			*Pow(3,m-n)%mod
			*Pow(2,n-1)+ans)%mod;
	/*
	0 0 0
	1 1
	1
	*/
	for (int i=4;i<=n;i++)
		ans=(2LL*4LL*Pow(4,n-i)%mod
				*Pow(3,m-n)%mod
				*Pow(2,n-1)+ans)%mod;
	for (int i=n+1;i<=m;i++)
		ans=(2LL*3LL*Pow(3,m-i)%mod
				*Pow(2,n-1)+ans)%mod;
	ans=(2LL*2LL*Pow(2,n-2)+ans)%mod;
	/*
	0 0 0
	1 0
	1
	*/
	for (int i=4;i<=n;i++)
		ans=(2LL*4LL*Pow(4,n-i)%mod
				*Pow(3,m-n)%mod
				*Pow(2,n-1)+ans)%mod;
	if (m>n)
		ans=(2LL*4LL*Pow(3,m-n-1)%mod
					*Pow(2,n-1)%mod+ans)%mod;
	else
		ans=(2LL*3LL*Pow(2,n-1)%mod+ans)%mod;
	printf("%d\n",ans);
	return 0;
}
