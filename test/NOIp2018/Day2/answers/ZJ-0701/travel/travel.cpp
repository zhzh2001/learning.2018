#include <algorithm>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
using namespace std;
char isd(char ch){
	return '0'<=ch&&ch<='9';
}
int read(){
	int x=0;
	char ch=getchar();
	while (!isd(ch))
		ch=getchar();
	while (isd(ch))
		x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return x;
}
const int N=5005;
int n,m;
int Cir[N],cc,ct;
int vis[N];
int e[N][N],ec[N];
int dfs(int x,int pre){
	if (vis[x]){
		ct=x;
		return 1;
	}
	vis[x]=1;
	for (int i=1;i<=ec[x];i++){
		int y=e[x][i];
		if (y!=pre)
			if (dfs(y,x)){
				if (ct)
					Cir[++cc]=x;
				if (x==ct)
					ct=0;
				return 1;
			}
	}
	return 0;
}
void Getcir(){
	cc=ct=0;
	memset(vis,0,sizeof vis);
	dfs(1,0);
}
int px=0,py=0,flag;
int v[N],Time;
int id[N];
void solve(int x,int pre){
	Time++;
	if (!flag){
		if (v[Time]<x){
			flag=-1;
			return;
		}
		if (v[Time]>x)
			flag=1;
	}
	if (flag)
		v[Time]=x;
	if (x!=px&&x!=py){
		for (int i=1;i<=ec[x];i++)
			if (e[x][i]!=pre){
				solve(e[x][i],x);
				if (flag<0)
					return;
			}
	}
	else {
		for (int i=1;i<=ec[x];i++)
			if (e[x][i]!=pre&&e[x][i]!=px&&e[x][i]!=py){
				solve(e[x][i],x);
				if (flag<0)
					return;
			}
	}
}
void upd(){
	flag=Time=0;
	solve(1,0);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	memset(ec,0,sizeof ec);
	for (int i=1;i<=m;i++){
		int a=read(),b=read();
		e[a][++ec[a]]=b;
		e[b][++ec[b]]=a;
	}
	for (int i=1;i<=n;i++)
		sort(e[i]+1,e[i]+ec[i]+1);
	if (m==n)
		Getcir();
	for (int i=1;i<=n;i++)
		v[i]=n+1;
//	for (int i=1;i<=cc;i++)
//		printf("%d ",Cir[i]);puts("");
	if (m==n-1)
		upd();
	else {
		for (int i=1;i<=cc;i++)
			id[i]=i;
		unsigned a=34825,b=2384235;
		for (int i=1;i<=cc;i++){
			swap(id[a%cc+1],id[b%cc+1]);
			a^=b<<21;
			b^=a<<13;
			a^=b<<3;
			b^=a<<6;
		}
		for (int i=1;i<=cc;i++){
			px=Cir[id[i]];
			py=Cir[id[i]<cc?id[i]+1:1];
			upd();
		}
	}
	for (int i=1;i<=n;i++)
		printf("%d ",v[i]);
	return 0;
}
