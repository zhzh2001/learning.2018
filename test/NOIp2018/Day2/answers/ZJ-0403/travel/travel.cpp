#include<cstdio>
#include<algorithm>
#include<stack>
#include<vector>
using namespace std;
const int MAXN=5003;
int read()
{
	int x=0,w=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-')w=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x*w;
}
vector<int> g[MAXN];
int in[MAXN]={0},low[MAXN]={0},dfn[MAXN]={0},dep=0;
int len[MAXN]={0},siz[MAXN];
stack<int> S;
void Tarjan(int u,int fa)
{
	dfn[u]=++dep;
	low[u]=dfn[u];
	in[u]=1;
	S.push(u);
	for(int i=0;i<len[u];i++)
	{
		int v=g[u][i];
		if(v==fa)
			continue;
		if(!dfn[v])
		{
			Tarjan(v,u);
			low[u]=min(low[v],low[u]);
		}
		else
			if(in[v])
				low[u]=min(low[u],low[v]);
	}
	if(low[u]==dfn[u])
	{
		int s,num=0,t;
		do
		{
			s=S.top();
			S.pop();
			t=s;
			num++;
			in[s]=0;
		}while(s!=u);
		if(num==1)
			siz[t]=-1;
	}
}
int path[MAXN],cnt=0,vis[MAXN]={0};
void dfs(int num,int fa)
{
	int u=g[fa][num],nxt;
	vis[u]=1;
	path[++cnt]=u;
	for(int i=1;i<len[fa];i++)
		if(!vis[g[fa][i]])
		{
			nxt=i;
			break;
		}
	for(int i=0;i<len[u];i++)
	{
		int v=g[u][i];
		if(v==fa||vis[v]==1)
			continue;
		if(!siz[v]&&vis[v]!=2&&nxt<len[fa]&&v>g[fa][nxt])
		{
			vis[v]=2;
			continue;
		}	
		dfs(i,u);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n=read(),m=read(),u,v;
	for(int i=1;i<=m;i++)
	{
		u=read();
		v=read();
		g[u].push_back(v);
		g[v].push_back(u);		
	}
	for(int i=1;i<=n;i++)
	{
		len[i]=g[i].size();
		sort(g[i].begin(),g[i].end());
	}
	if(m==n)
		Tarjan(1,0);
	else
		for(int i=1;i<=n;i++)
			siz[i]=-1;
	g[0].push_back(1);
	len[0]=1;	
	dfs(0,0);
	for(int i=1;i<cnt;i++)
		printf("%d ",path[i]);
	printf("%d\n",path[cnt]);
	return 0;
}
