#include<cstdio>
#include<algorithm>
#include<cmath>
using namespace std;
const int MAXN=30003;
struct EDGE
{
	int to,next;
}e[MAXN*2];
int head[MAXN]={0},cnt=0;
void add(int u,int v)
{
	e[++cnt].to=v;
	e[cnt].next=head[u];
	head[u]=cnt;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int u,v,n,m,a,x,b,y,w[MAXN],vis[MAXN],ans;
	char s[3];
	scanf("%d%d%s",&n,&m,&s);
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&w[i]);
		vis[i]=-1;
	}
	for(int i=1;i<n;i++)
	{
		scanf("%d%d",&u,&v);
		add(u,v);
		add(v,u);
	}
	
		while(m--)
		{
			ans=0;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			vis[a]=x;
			vis[b]=y;
			if(abs(a-b)==1&&!x&&!y)
			{
				printf("-1\n");
				continue;
			}
			for(int i=1;i<n;i++)
			{
				if(vis[i]==-2)
					continue;
				if(vis[i]==-1&&vis[i+1]==-1)
				{
					if(w[i]<w[i+1])
					{
						ans+=w[i];
						vis[i]=-2;
					}
					else
					{
						ans+=w[i+1];
						vis[i+1]=-2;
					}
				}
				if(vis[i]==0)
				{
					ans+=w[i+1];
					vis[i+1]=-2;
				}
				if(vis[i]==1)
					ans+=w[i];
			}
			printf("%d\n",ans);
		}
	return 0;
}
