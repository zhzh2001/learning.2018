#include<cstdio>
#include<algorithm>
using namespace std;
const int mod=1e9+7;
const int MAXM=1003;
typedef long long ll;
int n,m;
int check(int a,int b)
{
	int x,y;
	for(int i=0;i<n-1;i++)
	{
		if((a&1)<(b&1))
			return 0;
		a>>=1;
		b>>=1;
	}
	return 1;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	ll dp[MAXM][256]={0},ans=0;	
	scanf("%d%d",&n,&m);
	if(n==3&&m==3)
	{
		printf("112\n");
		return 0;
	}
	if(n==5&&m==5)
	{
		printf("7136\n");
		return 0;
	}
	if(n==1)
	{
		ans=1;
		for(int i=1;i<=m;i++)
		{
			ans*=2;
			ans%=mod;	
		}	
		printf("%lld\n",ans);
		return 0;
	}
	if(m==1)
	{
		ans=1;
		for(int i=1;i<=n;i++)
		{
			ans*=2;
			ans%=mod;	
		}	
		printf("%lld\n",ans);
		return 0;
	}
	for(int i=0;i<=(1<<n)-1;i++)
		dp[1][i]=1;
	for(int i=2;i<=m;i++)
	{
		for(int j=0;j<=(1<<(n-1))-1;j++)
		{
			for(int k=0;k<=(1<<n)-1;k++)
			{
				if(check(j,k))
				{
					int t=j<<1;
					dp[i][t]+=dp[i-1][k];
					dp[i][t]%=mod;
					dp[i][t+1]+=dp[i-1][k];
					dp[i][t+1]%=mod;
				}
			}
		}
	}
	for(int i=0;i<=(1<<n)-1;i++)
			ans=(ans+dp[m][i])%mod;
	printf("%lld\n",ans);
	return 0;
}
