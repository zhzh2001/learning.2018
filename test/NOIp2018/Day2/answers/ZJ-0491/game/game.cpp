#include<bits/stdc++.h>
#define MOD 1000000007

using namespace std;
int t[100][100],n,m,ans;
void dfs(int x,int y){
	
	if(t[x-1][y+1]==0){
		if(x==n&&y==m){
			ans+=2;
			return;
		}
		t[x][y]=0;
		if(y==m){
			dfs(x+1,1);
		}else{
			dfs(x,y+1);
		}
		t[x][y]=1;
		if(y==m){
			dfs(x+1,1);
		}else{
			dfs(x,y+1);
		}
		t[x][y]=0;
	}else{
		t[x][y]=1;
		if(y==m){
			dfs(x+1,1);
		}else{
			dfs(x,y+1);
		}
	}
}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	dfs(1,1);
	printf("%d",ans);
}
