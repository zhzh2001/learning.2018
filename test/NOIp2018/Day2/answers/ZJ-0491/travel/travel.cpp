#include<bits/stdc++.h>
using namespace std;
struct typel{
	int x,y;
}edge[10001];
int n,m,x,y,cnt,head[10001],nxt[10001],to[10001],ans[5005],an[5005],M,X[5005],Y[5005],sum;
bool flag,t[5005];
void addedge(int u,int v){
	++cnt;
	to[cnt]=v;
	nxt[cnt]=head[u];
	head[u]=cnt;
}
bool cmp(typel x,typel y){
	if(x.x<y.x)return 1;
	else if(x.x==y.x&&x.y>y.y)return 1;
	return 0;
}

void dfs(int x,int from){
	if(sum==n){
		for(int i=1;i<=n;++i){
			if(ans[i]>an[i]){
				for(int i=1;i<=n;++i){
					ans[i]=an[i];
				}
				break;
			}
			if(ans[i]<an[i])break;
		}
		return;
	}
	for(int i=head[x];i;i=nxt[i]){
		int y=to[i];
		if(flag&&((X[M]==x&&Y[M]==y)||(X[M]==y&&Y[M]==x)))continue;
		if(t[y])continue;
		if(y==from)continue;
		t[y]=true;
		sum+=1;
		an[sum]=y;
		dfs(y,x);
		
	}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	ans[1]=n;
	an[1]=1;
	for(int i=1;i<=m;++i){
		scanf("%d%d",&x,&y);
		X[i]=x;
		Y[i]=y;
		edge[(i-1)*2+1].x=x;
		edge[(i-1)*2+1].y=y;
		edge[i*2].x=y;
		edge[i*2].y=x;
	}
	sort(edge+1,edge+m*2,cmp);
	for(int i=1;i<=m*2;++i){
		addedge(edge[i].x,edge[i].y);
	}
	if(m==n-1){
		flag=false;
		sum=1;
		dfs(1,0);
		
		for(int i=1;i<=n;++i){
			printf("%d ",ans[i]);
		}
		return 0;
	}else{
		flag=true;
		for(int i=1;i<=m;++i){
			M=i;
			sum=1;
			memset(t,false,sizeof(t));
			dfs(1,0);
		}
		for(int i=1;i<=n;++i){
			printf("%d ",ans[i]);
		}
		return 0;
	}
	
}
