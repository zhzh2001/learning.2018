#include<bits/stdc++.h>
typedef long long ll;
const int maxn = 100035;
const int maxm = 200035;

int n,m,p[maxn],tke[maxn];
ll f[maxn][3],ans,preans;
int edgeTot,head[maxn],nxt[maxm],edges[maxm];
char tpe[13];
bool fir[maxn];

int read()
{
	char ch = getchar();
	int num = 0, fl = 1;
	for (; !isdigit(ch); ch=getchar())
		if (ch=='-') fl = -1;
	for (; isdigit(ch); ch=getchar())
		num = (num<<1)+(num<<3)+ch-48;
	return num*fl;
}
void addedge(int u, int v)
{
	edges[++edgeTot] = v, nxt[edgeTot] = head[u], head[u] = edgeTot;
	edges[++edgeTot] = u, nxt[edgeTot] = head[v], head[v] = edgeTot;
}
inline ll min(ll a, ll b){return a<b?a:b;}
void dp(int x, int fa)
{
	if (tke[x]!=0) f[x][1] = p[x];
	if (tke[x]!=1) f[x][0] = 0;
	for (int i=head[x]; i!=-1; i=nxt[i])
	{
		int v = edges[i];
		if (v==fa) continue;
		dp(v, x);
		if (tke[x]!=0) f[x][1] += min(f[v][0], f[v][1]);
		if (tke[x]!=1) f[x][0] += f[v][1];
	}
	f[x][1] = min(f[x][1], f[0][0]);
	f[x][0] = min(f[x][0], f[0][0]);
//	printf("x:%d %lld %lld\n",x,f[x][0],f[x][1]);
}
void recAns(int x, int fa, bool chs)
{
	fir[x] = chs;
	for (int i=head[x]; i!=-1; i=nxt[i])
	{
		int v = edges[i];
		if (v==fa) continue;
		if (chs) recAns(v, x, f[v][1] < f[v][0]);
		else recAns(v, x, 1);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(tke, -1, sizeof tke);
	memset(head, -1, sizeof head);
	n = read(), m = read(), scanf("%s",tpe);
	for (int i=1; i<=n; i++) p[i] = read();
	for (int i=1; i<n; i++) addedge(read(), read());
	memset(f, 0x3f3f3f3f, sizeof f);
	dp(1, 1), preans = min(f[1][1], f[1][0]);
	recAns(1, 1, f[1][1] < f[1][0]);
	while (m--)
	{
		int a = read(), x = read(), b = read(), y = read();
		if (tpe[1]=='2'){
			if (!x&&!y){
				puts("-1");
				continue;
			}
		}
		tke[a] = x, tke[b] = y;
		if (fir[a]==x&&fir[b]==y) ans = preans;
		else{
			memset(f, 0x3f3f3f3f, sizeof f);
			dp(1, 1), ans = min(f[1][1], f[1][0]);
		}
		tke[a] = -1, tke[b] = -1;
		printf("%lld\n",ans>=f[0][0]?-1:ans);
	}
	return 0;
}
