#include<bits/stdc++.h>
#define MO 1000000007
typedef long long ll;

int n,m;

int qmi(int a, int b)
{
	int ret = 1;
	for (; b; b>>=1, a=1ll*a*a%MO)
		ret = 1ll*ret*a%MO;
	return ret;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1) printf("%d\n",qmi(2, m));
	else if (m==1) printf("%d\n",qmi(2, n));
	else{
		if (n==2&&m==2) puts("12");
		if (n==3&&m==3) puts("112");
		if (n==2&&m==3) puts("36");
		if (n==3&&m==2) puts("36");
		if (n==5&&m==5) puts("7136");
	}
	return 0;
}
