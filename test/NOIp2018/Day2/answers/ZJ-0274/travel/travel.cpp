#include<bits/stdc++.h>
const int maxn = 5035;
const int maxm = 10035;

bool vis[maxn];
int n,m,stk[maxn],cnt,t[maxn],tot;
int edgeTot,head[maxn],nxt[maxm],edges[maxm];

int read()
{
	char ch = getchar();
	int num = 0, fl = 1;
	for (; !isdigit(ch); ch=getchar())
		if (ch=='-') fl = -1;
	for (; isdigit(ch); ch=getchar())
		num = (num<<1)+(num<<3)+ch-48;
	return num*fl;
}
void addedge(int u, int v)
{
	edges[++edgeTot] = v, nxt[edgeTot] = head[u], head[u] = edgeTot;
	edges[++edgeTot] = u, nxt[edgeTot] = head[v], head[v] = edgeTot;
}
namespace subtask1
{
	void dfs(int x)
	{
		int l = cnt+1, r = cnt+1;
		vis[x] = 1, t[++tot] = x;
		for (int i=head[x]; i!=-1; i=nxt[i])
		{
			int v = edges[i];
			if (!vis[v]) stk[++cnt] = v;
		}
		r = cnt;
		if (l <= r){
			std::sort(stk+l, stk+cnt+1);
			for (int i=l; i<=r; i++)
				if (!vis[stk[i]]) dfs(stk[i]);
		}
	}
};
namespace subtask2
{
	int sv[maxn],sp[maxn],tp[maxn],stk[maxn];
	int chTot,pos,cnt,tot;
	bool fbd[maxn][maxn];
	void chkBet()
	{
		bool fnd = 0;
//		for (int i=1; i<=n; i++) printf("%d ",sv[i]);puts("");
		for (int i=1; i<=n&&!fnd; i++)
			if (sv[i] < t[i]) fnd = 1;
			else if (sv[i] > t[i]) break;
		if (fnd) for (int i=1; i<=n; i++) t[i] = sv[i];
	}
	bool fndCir(int x, int fa)
	{
		vis[x] = 1;
		for (int i=head[x]; i!=-1; i=nxt[i])
		{
			int v = edges[i];
			if (v==fa) continue;
			if (vis[v]||fndCir(v, x)){
				++chTot, sp[chTot] = x, tp[chTot] = v;
				if (vis[v]&&!pos) pos = x;
				return 1;
			}
		}
		vis[x] = 0;
		return 0;
	}
	void dfs(int x)
	{
		int l = cnt+1, r = cnt+1;
		vis[x] = 1, sv[++tot] = x;
		for (int i=head[x]; i!=-1; i=nxt[i])
		{
			int v = edges[i];
			if (!vis[v]&&!fbd[x][v]) stk[++cnt] = v;
		}
		r = cnt;
		if (l <= r){
			std::sort(stk+l, stk+cnt+1);
			for (int i=l; i<=r; i++)
				if (!vis[stk[i]]) dfs(stk[i]);
		}
	}
	void solve()
	{
		memset(fbd, 0, sizeof fbd);
		for (int i=1; i<=n; i++) t[i] = n+1;
		fndCir(1, 1);
		memset(vis, 0, sizeof vis);
//		printf("pos:%d\n",pos);
		chTot = 0, fndCir(pos, pos);
//		for (int i=1; i<=n; i++) if (vis[i]) printf("%d ",i);
//		for (int i=1; i<=chTot; i++) printf("#%d %d\n",s[i],t[i]);
		for (int i=1; i<=chTot; i++)
		{
			memset(vis, 0, sizeof vis);
			cnt = tot = 0;
			fbd[sp[i]][tp[i]] = 1, fbd[tp[i]][sp[i]] = 1;
//			for (int j=1; j<=n; j++, puts(""))
//				for (int k=1; k<=n; k++)
//					printf("%d ",fbd[j][k]);			
			dfs(1), chkBet();
			fbd[sp[i]][tp[i]] = 0, fbd[tp[i]][sp[i]] = 0;
		}
	}
};
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(head, -1, sizeof head);
	n = read(), m = read();
	for (int i=1; i<=m; i++) addedge(read(), read());
	if (m==n-1){
		//		SUBTASK::TREE
		subtask1::dfs(1);
		for (int i=1; i<=n; i++)
			printf("%d%c",t[i],i==n?'\n':' ');
		return 0;
	}
	subtask2::solve();
	for (int i=1; i<=n; i++)
		printf("%d%c",t[i],i==n?'\n':' ');
	return 0;
}
