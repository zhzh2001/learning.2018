#include<cstdio>
#include<algorithm>
#define LL long long
using namespace std;
const int N=5e5+5;
const LL inf=1e10;
struct edge{int to,nxt;}e[N<<2];
int head[N],a[N],q[N],fa[N];
char s[5];
LL f[2][N];
int n,Q,cnt;
int px,py,qx,qy;
int read()
{
	char ch=getchar(); int x=0;
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	return x;
}
void add(int x,int y){e[++cnt].nxt=head[x]; head[x]=cnt; e[cnt].to=y;}
void dfs(int u,int fa)
{
	LL sum=0,s=0;
	for(int i=head[u];i;i=e[i].nxt)
	if(e[i].to!=fa)
	{
	    dfs(e[i].to,u);
		sum+=f[1][e[i].to];
		s+=min(f[0][e[i].to],f[1][e[i].to]);
	}
	f[0][u]=sum;
	f[1][u]=min(s,sum)+a[u];
	f[py][px]=inf;
	f[qy][qx]=inf;
}
void pre(int u){for(int i=head[u];i;i=e[i].nxt)if(e[i].to!=fa[u]){fa[e[i].to]=u; pre(e[i].to);}}
void case1()
{
	for(int i=1,x;i<n;i++){x=read(); x=read();}
	while(Q--)
	{
		px=read(); py=read()^1; qx=read(); qy=read()^1;
		f[0][1]=0;
		f[1][1]=a[1];
		for(int i=2;i<=n;i++)
		{
			f[0][i]=f[1][i-1];
			f[1][i]=min(f[0][i-1],f[1][i-1])+a[i];
			f[py][px]=inf;
			f[qy][qx]=inf;
		}
		printf("%lld\n",min(f[0][n],f[1][n]));
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(); Q=read(); scanf("%s",s);
	for(int i=1;i<=n;i++)a[i]=read();
	if(s[0]=='A'){case1(); return 0;}
	for(int i=1,x,y;i<n;i++)
	{
		x=read(); y=read();
		add(x,y); add(y,x);
	}
	pre(1);
	while(Q--)
	{
	    px=read(); py=read()^1; qx=read(); qy=read()^1;
		if(py&&qy&&(fa[px]==qx||fa[qx]==px)){printf("-1\n"); continue;}
		dfs(1,0);
		printf("%lld\n",min(f[0][1],f[1][1]));
	}
	return 0;
}
