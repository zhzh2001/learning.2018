#include<cstdio>
#define LL long long
const LL mo=1e9+7;
const int N=5e3+5;
int a[N],b[N],p[N][N];
int n,m,ans=0,f;
int read()
{
	char ch=getchar(); int x=0;
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	return x;
}
int dfss(int x,int y,int st)
{
	b[++st]=p[x][y]; if(b[st]>a[st]&&f)return 0;
	if(b[st]<a[st])f=0;
	if(x==n&&y==m){for(int i=1;i<=st;i++)a[i]=b[i]; f=1;return 1;}
	if(x<n){if(!dfss(x+1,y,st))return 0;}
	if(y<m){if(!dfss(x,y+1,st))return 0;}
	return 1;
}
void dfs(int x,int y)
{
	if(y>m)
	{
	    y=1;
		if(++x>n)
		{
		    for(int i=n+m;i;i--)a[i]=4;f=1;
			ans+=dfss(1,1,0);
			return;
		}
	}
	p[x][y]=0; dfs(x,y+1);
	p[x][y]=1; dfs(x,y+1);
}
LL qmi(LL x,LL y){LL res=1; for(;y;y>>=1,x=x*x%mo)if(y&1)res=res*x%mo; return res;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	if(n==2){printf("%lld\n",qmi(3,m-1)*4%mo); return 0;}
	if(n==3){printf("%lld\n",qmi(3,m-3)*112%mo); return 0;}
	if(n==4)
	{
	    if(m==4){printf("912\n"); return 0;}
	    if(m==5){printf("%lld\n",qmi(3,m-5)*2688%mo); return 0;}
	}
	//scanf("%d%d",&n,&m);
	for(m=5;m<=20;m++)
	{
	    n=4;
		ans=0;
		dfs(1,1);
		printf("%d %lld\n",ans,qmi(3,m-5)*2688%mo);	
	}
}
/*
768 1000000007
2304 1000000007
6912 1000000007

912 1000000007
2688 1000000007
8064 1000000007
24192 1000000007


 912=2*2*2*2*57
 768=2*2*2*2*2*2*2*2*3
 112=2*2*2*2*7
7136=2*2*2*2*2*223

112 112
336 336
1008 1008
3024 3024
9072 9072
27216 27216
*/
