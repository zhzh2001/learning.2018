#include<cstdio>
#include<algorithm>
using namespace std;
const int N=10005;
struct edge{int to,nxt;}e[N<<2];
int head[N],f[N],a[N],q[N],bz[N],st[N];
int n,m,cnt,tot=0,t=0,top=0,tt,fl=0;
int read()
{
	char ch=getchar(); int x=0;
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	return x;
}
void add(int x,int y){e[++cnt].nxt=head[x]; head[x]=cnt; e[cnt].to=y;}
void dfs(int u)
{
	
	if(!f[u]){f[u]=1; if(++tot<n)printf("%d ",u);else printf("%d\n",u);}
	int l=t,r;
	for(int i=head[u];i;i=e[i].nxt)if(!f[e[i].to])a[++t]=e[i].to;
	if(l==t)return;
	r=t;
	sort(a+l+1,a+r+1);
	for(int i=l+1;i<=r;i++)dfs(a[i]);
}
bool dfss(int u,int f)
{
	q[++top]=u; bz[u]=1;
	for(int i=head[u];i;i=e[i].nxt)
	if(e[i].to!=f)
	{
		if(!bz[e[i].to]){if(dfss(e[i].to,u))return 1; continue;}
		st[tt=1]=e[i].to;
		while(q[top]!=e[i].to)st[++tt]=q[top],top--;
		return 1;
	}
	top--; 
	return 0;
}
void dfsss(int u,int lst)
{
	if(!f[u]){f[u]=1; if(++tot<n)printf("%d ",u);else printf("%d\n",u);}
	int l=t,r;
	if(u==st[1])fl=1;
	for(int i=head[u];i;i=e[i].nxt)if(!f[e[i].to])a[++t]=e[i].to;
	if(l==t)return;
	r=t;
	sort(a+l+1,a+r+1);
	for(int i=l+1;i<r;i++)dfsss(a[i],fl?a[i+1]:n+3);
	if(bz[a[r]]&&a[r]>lst){fl=0; return;}
	dfsss(a[r],lst);
	return;
}
int main()
{
	freopen("trave.in","r",stdin);
	freopen("trave.out","w",stdout);
	n=read(); m=read();
	for(int i=1,x,y;i<=m;i++)
	{
		x=read(); y=read();
		add(x,y); add(y,x);
	}
	if(m==n-1){dfs(1); return 0;}
	dfss(1,0);
	for(int i=1;i<=n;i++)bz[i]=0;
	for(int i=1;i<=tt;i++)bz[st[i]]=1;
	dfsss(1,n+3);
	return 0;
}
