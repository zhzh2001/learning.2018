#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<string>
using namespace std;
long long n,m,ans=0,maxn=1e18;
long long w[100100];
long long dp[100100][3];
long long a,x,b,y;
string type;
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(dp,0x3f,sizeof(dp));
	scanf("%lld%lld",&n,&m);
	cin>>type;
	for(int i=1;i<=n;i++)
		scanf("%lld",&w[i]);
	if(type=="A1"){
		dp[1][1]=w[1];
		dp[2][0]=w[1];
		dp[2][1]=w[1]+w[2];
		for(long long i=1;i<=m;i++){
			scanf("%lld%lld%lld%lld",&a,&x,&b,&y);
			for(long long j=3;j<=b;j++){
				dp[j][1]=min(dp[j-1][0],dp[j-1][1])+w[j];
				dp[j][0]=dp[j-1][1];
			}		
			dp[b][y]=maxn;	
			for(int j=b+1;j<=n;j++){
				dp[j][1]=min(dp[j-1][0],dp[j-1][1])+w[j];
				dp[j][0]=dp[j-1][1];
			}
			printf("%lld\n",min(dp[n][0],dp[n][1]));
		}
		return 0;
	}
	if(type=="A2"){
		dp[1][1]=w[1];
		dp[2][1]=w[2];
		dp[2][0]=w[1];
		for(long long i=1;i<=m;i++){
			scanf("%lld%lld%lld%lld",&a,&x,&b,&y);
			if(x==0 && y==0){
				printf("-1\n");
				continue;
			}
			if((a==1 && x==0) || (b==1 && y==0))dp[2][0]=maxn;
			if(a==1 || a==2)dp[a][x]=maxn;
			if(b==1 || b==2)dp[b][y]=maxn;
			for(long long j=3;j<=n;j++){
				dp[j][1]=min(dp[j-1][0],dp[j-1][1])+w[j];
				dp[j][0]=dp[j-1][1];				
				if(a==j)
					dp[a][x]=maxn;
				if(b==j)
					dp[b][y]=maxn;			
			}
			printf("%lld\n",min(dp[n][1],dp[n][0]));
		}
		return 0;
	}
	if(type=="A3"){
		dp[1][1]=w[1];
		dp[2][1]=w[2];
		dp[2][0]=w[1];
		for(long long i=1;i<=m;i++){
			scanf("%lld%lld%lld%lld",&a,&x,&b,&y);
			if(x==0 && y==0 && abs(a-b)==1){
				printf("-1\n");
				continue;
			}
			if((a==1 && x==0) || (b==1 && y==0))dp[2][0]=maxn;
			if(a==1 || a==2)dp[a][x]=maxn;
			if(b==1 || b==2)dp[b][y]=maxn;
			for(long long j=3;j<=n;j++){
				dp[j][1]=min(dp[j-1][0],dp[j-1][1])+w[j];
				dp[j][0]=dp[j-1][1];	
				if(a==j)
					dp[a][x]=0x3f;
				if(b==j)
					dp[b][y]=0x3f;			
			}
			printf("%lld\n",min(dp[n][1],dp[n][0]));
		}
		return 0;
	}
	if(x==0 && y==0 && abs(a-b)==1)
		printf("-1\n");
	fclose(stdin);fclose(stdout);
	return 0;
}
