#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar();	}
	while(ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar();	}
	return x*f;
}
const int MAXN=310000;
const ll INF=30000000001ll;
struct Node {
	int to,nxt;
} edge[MAXN<<1]; 
int N,M,head[MAXN],idx=0,pa[MAXN],a,x,b,y,vis[MAXN];
ll w[MAXN],dp[MAXN][2],dp2[MAXN][2]; char s[10];
inline void ade(const int&u,const int&v) {
	edge[++idx].to=v; edge[idx].nxt=head[u]; head[u]=idx;
}
void dfs(const int&fa,const int&u) {
	dp[u][0]=dp[u][1]=0; pa[u]=fa;
	for(int i=head[u];~i;i=edge[i].nxt) if(edge[i].to!=fa) {
		dfs(u,edge[i].to);
		dp[u][0]+=dp[edge[i].to][1];
		dp[u][1]+=min(dp[edge[i].to][0],dp[edge[i].to][1]);
	}
	dp[u][1]+=w[u];
	if(u==a) dp[u][x^1]=INF; else if(u==b) dp[u][y^1]=INF;
}
void work1() {
	for(int i=1;i<=M;++i) {
		a=read(),x=read(),b=read(),y=read();
		dfs(0,1);
		ll ans=min(dp[1][0],dp[1][1]);
		if(ans>=INF) puts("-1"); else printf("%lld\n",min(dp[1][0],dp[1][1]));
	}
}
void dfs2(const int&fa,const int&u) {
	if(u==0) return;
	dp2[u][0]=dp2[u][1]=0; vis[u]=1;
	for(int i=head[u];~i;i=edge[i].nxt) if(edge[i].to!=fa) {
		if(!vis[edge[i].to]) {
			dp2[u][0]+=dp[edge[i].to][1];
			dp2[u][1]+=min(dp[edge[i].to][0],dp[edge[i].to][1]);
		} else {
			dp2[u][0]+=dp2[edge[i].to][1];
			dp2[u][1]+=min(dp2[edge[i].to][0],dp2[edge[i].to][1]);
		}
	}
	dp2[u][1]+=w[u];
	if(u==a) dp2[a][x^1]=INF; else if(u==b) dp2[b][y^1]=INF;
	dfs2(pa[fa],fa);
	vis[u]=0;
}
void work2() {
	memset(vis,0,sizeof vis);
	a=b=x=y=0; dfs(0,1);
	for(int i=1;i<=M;++i) {
		a=read(),x=read(),b=read(),y=read();
		dfs2(pa[a],a); 
		dfs2(pa[b],b);
		ll ans=min(dp2[1][0],dp2[1][1]);
		if(ans>=INF) puts("-1"); else printf("%lld\n",ans);
	}
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof head);
	memset(pa,0,sizeof pa);
	N=read(),M=read(); scanf("%s",s);
	for(int i=1;i<=N;++i) w[i]=read();
	for(int i=1,u,v;i<N;++i) {
		u=read(),v=read();
		ade(u,v),ade(v,u);
	} 
	if(N<=5000) work1();
	else work2();

	return 0;
}

