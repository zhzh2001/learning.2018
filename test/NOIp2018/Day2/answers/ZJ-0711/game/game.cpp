#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar();	}
	while(ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar();	}
	return x*f;
}
const ll Pi=1e9+7;
inline ll fast_pow(ll a,ll k) {
	ll t=1;
	for(;k;k>>=1) {
		if(k&1) t=t*a%Pi;
		a=a*a%Pi;
	}
	return t;
}
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int N=read(),M=read();
	if(N==1) printf("%lld\n",fast_pow(2,M));
	else if(N==2) printf("%lld\n",4ll*fast_pow(3,M-1)%Pi);
	else if(N==3) {
		if(M==1) puts("8"); else if(M==2) puts("36"); else {
			ll t1=1,t2=1;
			for(int i=2;i<M;++i) t1=t1*min(i,N)%Pi,t2=t2*(ll)min(i+1,N+1)%Pi;
			printf("%lld\n",(32ll*t1%Pi+48ll*(t2-t1+Pi)%Pi)%Pi);
		}
	}

	return 0;
}

