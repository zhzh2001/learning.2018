#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;
inline int read() {
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar();	}
	while(ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar();	}
	return x*f;
}
const int MAXN=5100;
vector<int> G[MAXN];
#define rpg(ii,u) for(vector<int>::iterator ii=G[u].begin();ii!=G[u].end();++ii)
int N,M;
void dfs1(const int&fa,const int&u) {
	printf("%d ",u);
	rpg(ii,u) if((*ii)!=fa) 
		dfs1(u,(*ii));
}
void work1() {
	for(int i=1;i<=N;++i) sort(G[i].begin(),G[i].end());
	dfs1(0,1);
}
int inde[MAXN],IC[MAXN],
queue<int> Q1;
void work2() {
	memset(IC,0,sizeof IC);
	for(int i=1;i<=N;++i) if(inde[i]==1) Q1.push(i);
	while(!Q1.empty()) {
		int u=Q1.front(); Q1.pop();
		rpg(ii,u) if(inde[*ii]!=1) {
			--inde[*ii];
			if(inde[*ii]==1) Q1.push(*ii);
		}
	}
	for(int i=1;i<=N;++i) if(inde[i]>1) IC[i]=1;
	
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	N=read(),M=read();
	for(int i=1,u,v;i<=M;++i) {
		u=read(),v=read();
		G[u].push_back(v); G[v].push_back(u);
		++inde[u]; ++inde[v];
	}
	if(M==N-1) work1(); else work2();

	return 0;
}

