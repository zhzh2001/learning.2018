#include <bits/stdc++.h>
#define For(a,b,c) for(int a=b;a<=c;++a)
using namespace std;
const int N=5050;
int n,m;
struct _1 {
	int x,y;
	vector<int> V[N];
	void fly (int u,int fa) {
		printf("%d ",u);
		For(i,0,(int)V[u].size()-1)
		if (V[u][i]!=fa) fly(V[u][i],u);
	}
	void solve () {
		For(i,1,m)
			scanf("%d%d",&x,&y),
			V[x].push_back(y),
			V[y].push_back(x);
		For(i,1,n) sort(V[i].begin(),V[i].end());
		fly(1,-1);
	}
}P1;
struct _2 {
	int t,x,y,tp,w,e,f,A[N],S[N],ans[N],ANS[N];
	vector <int> V[N];
	bool B[N],fl;
	void run (int u,int fa) {
		B[u]=true;
		S[++tp]=u;
		For(i,0,(int)V[u].size()-1) {
			if (!B[V[u][i]]) run(V[u][i],u);
			else if (V[u][i]!=fa) {
				int v=V[u][i];
				do A[++t]=S[tp];
				while (S[tp--]!=v);
				fl=true;
			}
			if (fl) return;
		}
		--tp;
	}
	void fly (int u,int fa) {
		ans[++w]=u;
		For(i,0,(int)V[u].size()-1)
		if (V[u][i]!=fa) {
			int v=V[u][i];
			if (u==e&&v==f) continue;
			if (u==f&&v==e) continue;
			fly(v,u);
		}
	}
	bool boom () {
		For(i,1,n)
		if (ans[i]!=ANS[i]) return ans[i]<ANS[i];
		return false;
	}
	void solve () {
		fl=false;
		tp=t=0;
		memset(B,false,sizeof(B));
		For(i,1,n) ANS[i]=n+1;
		For(i,1,m)
			scanf("%d%d",&x,&y),
			V[x].push_back(y),
			V[y].push_back(x);
		For(i,1,n) sort(V[i].begin(),V[i].end());
		run(1,-1);
		/*DEBUG*/
	//	For(i,1,t) printf("%d ",A[i]);
		For(i,1,t) {
			e=A[i];
			f=A[i%t+1];
			w=0;
			fly(1,-1);
			if (boom()) memcpy(ANS,ans,sizeof(ANS));
		}
		For(i,1,n) printf("%d ",ANS[i]);
	}
}P2;
int main () {//mem
//	cerr<<(sizeof(P1)+sizeof(P2))/1048576.0<<endl;
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m==n-1) P1.solve();
	else P2.solve();
	return 0;
}
