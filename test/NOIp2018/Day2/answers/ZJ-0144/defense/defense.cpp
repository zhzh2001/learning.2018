#include <bits/stdc++.h>
#define For(a,b,c) for(int a=b;a<=c;++a)
#define LL long long
using namespace std;
const int N=101000;
const LL oo=10000000000000000;
int n,m;
char ty[5];
struct _1 {
	int a,b,x,y,hd[N],S[N],nt[N*2],to[N*2];
	LL F[2][N],ans;
	void fly (int u,int fa) {
		LL tt=0,s1=0;
		for(int i=hd[u];i;i=nt[i])
		if (to[i]!=fa) {
			int v=to[i];
			fly(v,u);
			tt+=min(F[0][v],F[1][v]);
			s1+=F[1][v];
			if (s1>oo) s1=oo;
			if (tt>oo) tt=oo;
		}
		F[0][u]=s1;
		F[1][u]=tt+S[u];
		if (u==a) F[1-x][u]=oo;
		if (u==b) F[1-y][u]=oo;
	}
	void solve () {
		memset(hd,0,sizeof(hd));
		For(i,1,n) scanf("%d",&S[i]);
		For(i,1,n-1)
			scanf("%d%d",&x,&y),
			nt[i*2-1]=hd[x],hd[x]=i*2-1,to[i*2-1]=y,
			nt[i*2]=hd[y],hd[y]=i*2,to[i*2]=x;
		while (m--) {
			scanf("%d%d%d%d",&a,&x,&b,&y);
			fly(1,-1);
			ans=min(F[0][1],F[1][1]);
			if (ans>=oo/10) puts("-1");
			else printf("%lld\n",ans);
		}
	}
}P1;
struct _A {
	int a,b,x,y,S[N];
	LL F[2][N],ans;
	void solve () {
		For(i,1,n) scanf("%d",&S[i]);
		For(i,1,n-1) scanf("%*d%*d");
		while (m--) {
			scanf("%d%d%d%d",&a,&x,&b,&y);
			For(i,1,n) {
				F[0][i]=F[1][i-1];
				F[1][i]=min(F[0][i-1],F[1][i-1])+1LL*S[i];
				if (i==a) F[1-x][i]=oo;
				if (i==b) F[1-y][i]=oo;
			}
			ans=min(F[0][n],F[1][n]);
			if (ans>=oo/10) puts("-1");
			else printf("%lld\n",ans);
		}
	}
}PA;
struct _B {
	int a,b,x,y,hd[N],S[N],fa[N],nt[N*2],to[N*2];
	LL F[2][N],TT[N],S1[N],F0,F1,tt,s1;
	void fly (int u) {
		TT[u]=0,S1[u]=0;
		for(int i=hd[u];i;i=nt[i])
		if (to[i]!=fa[u]) {
			int v=to[i];
			fa[v]=u;
			fly(v);
			TT[u]+=min(F[0][v],F[1][v]);
			S1[u]+=F[1][v];
			if (S1[u]>oo) S1[u]=oo;
			if (TT[u]>oo) TT[u]=oo;
		}
		F[0][u]=S1[u];
		F[1][u]=TT[u]+S[u];
	}
	void run (int u) {
		while (u!=1) {
			tt=TT[fa[u]]-min(F[0][u],F[1][u])+min(F0,F1);
			s1=S1[fa[u]]-F[1][u]+F1;
			u=fa[u];
			F0=s1;
			F1=tt+S[u];
		}
		if (F1>=oo/10) puts("-1");
		else printf("%lld\n",F1);
	}
	void solve () {
		memset(hd,0,sizeof(hd));
		fa[1]=0;
		For(i,1,n) scanf("%d",&S[i]);
		For(i,1,n-1)
			scanf("%d%d",&x,&y),
			nt[i*2-1]=hd[x],hd[x]=i*2-1,to[i*2-1]=y,
			nt[i*2]=hd[y],hd[y]=i*2,to[i*2]=x;
		fly(1);
		while (m--) {
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if (y==0) F0=F[0][b],F1=oo;
			else F0=oo,F1=F[1][b];
			run(b);
		}
	}
}PB;
int main () {//mem LL
//	cerr<<(sizeof(P1)+sizeof(PA)+sizeof(PB))/1048576.0<<endl;
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,ty);
	if (n<=2000&&m<=2000) P1.solve();
	else if (ty[0]=='A') PA.solve();
	else /*if (ty[0]=='B')*/ PB.solve();
	return 0;
}
