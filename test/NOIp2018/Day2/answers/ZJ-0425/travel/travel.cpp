#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;
int n,m,h,cu,cv,cc,cnt,p[5100],p1[5100],a[5100];
bool vis[5100],flag[5100],f,g;
int edgenum,vet[11000],Next[11000],Head[5100];
vector<int> vec[5100];
void addedge(int u,int v){
	vet[++edgenum]=v; Next[edgenum]=Head[u];
	Head[u]=edgenum;
}
void dfs1(int u,int fa){
	p[++cnt]=u; int tot=0; vec[u].clear();
	for (int e=Head[u];e;e=Next[e])
		if (vet[e]!=fa&&(u!=cu||vet[e]!=cv)&&(u!=cv||vet[e]!=cu)){
			tot++;
			vec[u].push_back(vet[e]);	
		}
	sort(vec[u].begin(),vec[u].end());
	for (int i=0;i<tot;i++) dfs1(vec[u][i],u);
}
void dfs2(int u,int fa){
	vis[u]=true;
	for (int e=Head[u];e;e=Next[e])
		if (vet[e]!=fa){
			if (vis[vet[e]]){
				f=true; g=true;
				a[++cc]=vet[e];
				flag[vet[e]]=true;
				return;
			}
			dfs2(vet[e],u);
			if (g){
				if (a[1]==vet[e]){
					f=false;
					h=u;
				}
				if (f) a[++cc]=vet[e],flag[vet[e]]=true;
				return;
			}
		}	
}
void work(){
	cnt=0;
	cu=a[1]; cv=a[2]; dfs1(1,0);
	for (int i=1;i<=n;i++) p1[i]=p[i];
	for (int i=2;i<=cc;i++){
		cu=a[i]; cv=a[i%cc+1];
		cnt=0;
		dfs1(1,0);
		for (int i=1;i<=n;i++){
			if (p[i]>p1[i]) break;
			if (p[i]<p1[i]){
				for (int j=1;j<=n;j++) p1[j]=p[j];
				break;
			}
		}
	}
		
		
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m); int u,v;
	for (int i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		addedge(u,v); addedge(v,u);
	}
	if (m==n-1){ cu=0; cv=0;
		cnt=0; dfs1(1,0);
		for (int i=1;i<n;i++) printf("%d ",p[i]);
		printf("%d\n",p[n]);
	} else{
		dfs2(1,0); work();
		for (int i=1;i<n;i++) printf("%d ",p1[i]);
		printf("%d\n",p1[n]);
	}
	return 0;
}

