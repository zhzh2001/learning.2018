#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
const ll INF=1000000000000000ll;
ll dp[110000][2],ans;
int n,m,a,b,x,y,p[110000];
int edgenum,vet[110000],Next[110000],Head[51000];
void addedge(int u,int v){
	vet[++edgenum]=v; Next[edgenum]=Head[u];
	Head[u]=edgenum;
}/*
void dfs(int u,int fa){
	dp[u][2]=p[u];
	dp[u][0]=0; dp[u][1]=INF;
	int v;
	for (int e=Head[u];e;e=Next[e]){
		v=vet[e];
		if (v!=fa){
			dfs(v,u);
			dp[u][2]+=min(dp[v][0],min(dp[v][1],dp[v][2]));
			dp[u][0]+=min(dp[v][1],dp[v][2]);
		}
	}
	for (int e=Head[u];e;e=Next[e]){
		v=vet[e];
		if (v!=fa) dp[u][1]=min(dp[u][1],dp[u][0]-min(dp[v][1],dp[v][2])+dp[v][2]);
	}
	if (dp[u][0]>INF) dp[u][0]=INF;
	if (dp[u][1]>INF) dp[u][1]=INF;
	if (a==u){
		if (!x) dp[u][2]=INF;
		else dp[u][0]=dp[u][1]=INF;
	}
	if (b==u){
		if (!y) dp[u][2]=INF;
		else dp[u][0]=dp[u][1]=INF;
	}
}*/
void dfs(int u,int fa){
	dp[u][0]=0;
	dp[u][1]=p[u];
	int v;
	for (int e=Head[u];e;e=Next[e]){
		v=vet[e];
		if (v!=fa){
			dfs(v,u);
			dp[u][1]+=min(dp[v][0],dp[v][1]);
			dp[u][0]+=dp[v][1];
		}
	}
	if (a==u){
		if (!x) dp[u][1]=INF;
		else dp[u][0]=INF;
	}
	if (b==u){
		if (!y) dp[u][1]=INF;
		else dp[u][0]=INF;
	}
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%*s",&n,&m); int u,v;
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++){
		scanf("%d%d",&u,&v);
		addedge(u,v); addedge(v,u);
	}
	while (m--){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		dfs(1,0); ans=min(dp[1][0],dp[1][1]);
		if (ans>=INF) puts("-1");
		else printf("%lld\n",ans);
	}
	return 0;
}

