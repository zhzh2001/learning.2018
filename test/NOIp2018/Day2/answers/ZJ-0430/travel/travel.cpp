#include <bits/stdc++.h>
using namespace std;
const int N=5009;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,m;
vector<int>son[N*2];
int vis[N*2],in[N*2],fa[N*2],cir[N*2],maxn=0;
bool cmp(int a,int b){return a<b;}
queue<int>q;
void cutleft(int x,int pre){
	if(x>maxn)return ;
	fa[x]=pre;
	for(int i=0;i<son[x].size();i++){
		if(!cir[son[x][i]]||fa[son[x][i]])continue;
		cutleft(son[x][i],x);
		return ;
	}
}
void cutright(int x,int pre){
	fa[x]=pre;
	for(int i=0;i<son[x].size();i++){
		if(!cir[son[x][i]]||fa[son[x][i]])continue;
		cutright(son[x][i],x);
	}
}
void cutcir(int x){
	int flag=0;
	for(int i=son[x].size()-1;i>=0;i--){
		if(cir[son[x][i]]&&flag==0){flag=son[x][i];}
		if(cir[son[x][i]]&&flag!=son[x][i]){cutleft(son[x][i],x);break;}
		maxn=min(son[x][i],maxn);
	}
	cutright(flag,x);
}
void get_pre(int x,int pre){
	if(cir[x]&&!cir[fa[x]])cutcir(x);
	for(int i=0;i<son[x].size();i++){
		if(son[x][i]==pre)continue;
		if(!fa[son[x][i]])fa[son[x][i]]=x;
		if(fa[son[x][i]]==x)get_pre(son[x][i],x);
	}
}

void dfs(int x,int pre){
	printf("%d ",x);
	for(int i=0;i<son[x].size();i++){
		if(son[x][i]==pre||fa[son[x][i]]!=x)continue;
		dfs(son[x][i],x);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		int u=read(),v=read();
		son[u].push_back(v);
		son[v].push_back(u);
		in[v]++;in[u]++;
	}
	for(int i=1;i<=n;i++){
		sort(son[i].begin(),son[i].end(),cmp);
		if(in[i]==1)q.push(i),vis[i]=1;
	}
	while(q.size()){
		int x=q.front();q.pop();
		for(int i=0;i<son[x].size();i++){
			int y=son[x][i];
			if(!vis[y]){
				in[y]--;
				if(in[y]==1){
					q.push(y);
					vis[y]=1;
				}
			}
		}
	}
	for(int i=1;i<=n;i++)
		if(!vis[i])cir[i]=1,cout<<i<<"  ";
	memset(vis,0,sizeof(vis));
	get_pre(1,1);
	dfs(1,1);
	return 0;
}

