#include <bits/stdc++.h>
using namespace std;
const int N=100009;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,m,type,s,a,b,x,y;
int p[N],f[N][2],fa[N];
int head[N],nxt[N],ver[N],tot=1;
void add(int u,int v){
	ver[++tot]=v;nxt[tot]=head[u];head[u]=tot;
	ver[++tot]=u;nxt[tot]=head[v];head[v]=tot;
}
char c;
void work1(){
	for(int i=1;i<=m;i++){
		int a,b,x,y;
		a=read();x=read();
		b=read();y=read();
		//cout<<1<<endl;
		if(a>b){
			swap(a,b);
			swap(x,y);
		}
		if(x==0&&y==0&&b==a+1){
			printf("-1\n");
			continue;
		}
		f[1][0]=0;f[1][1]=p[1];
		for(int j=2;j<=a;j++){
			f[j][0]=f[j-1][1];
			f[j][1]=min(f[j-1][0],f[j-1][1])+p[j];
		}
		f[a][1-x]=(1<<31-1);
		for(int j=a+1;j<=b;j++){
			f[j][0]=f[j-1][1];
			f[j][1]=min(f[j-1][0],f[j-1][1])+p[j];
		}
		f[b][1-y]=(1<<31-1);
		for(int j=b+1;j<=n;j++){
			f[j][0]=f[j-1][1];
			f[j][1]=min(f[j-1][0],f[j-1][1])+p[j];
		}
		printf("%d\n",min(f[n][0],f[n][1]));
	}
}
void dfs(int x,int pre){
	fa[x]=pre;
	for(int i=head[x];i;i=nxt[i]){
		if(ver[i]==pre)continue;
		dfs(ver[i],x);
	}
}
int dp(int xx,int t){
	if(xx==a&&t!=x)return 0x3f3f3f3f;
	if(xx==b&&t!=y)return 0x3f3f3f3f;
	if(f[xx][t]<0x3f3f3f3f)return f[xx][t];
	int ans=0;
	for(int i=head[xx];i;i=nxt[i]){
		if(ver[i]==fa[xx])continue;
		if(t==0)ans+=dp(ver[i],1);
		else ans+=min(dp(ver[i],1),dp(ver[i],0));
	}
	f[xx][t]=ans+t*p[xx];
	return f[xx][t];
}
void work2(){
	s=1;
	dfs(s,s);
	for(int i=1;i<=m;i++){
		memset(f,0x3f,sizeof(f));
		a=read();x=read();
		b=read();y=read();
		if(a==b&&x!=y){
			printf("-1\n");
			continue;
		}
		if(x==0&&y==0){
			int flag=0;
			for(int j=head[a];j;j=nxt[j]){
				if(ver[j]==b){
					flag=1;
					break;
				}
			}
			if(flag){
				printf("-1\n");
				continue;
			}
		}
		int ans;
		if(a==s)ans=dp(s,x);
		else if(b==s)ans=dp(s,y);
		else ans=min(dp(s,1),dp(s,0));
		printf("%d\n",ans);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();
	cin>>c>>type;
	for(int i=1;i<=n;i++)p[i]=read();
	for(int i=1;i<n;i++)add(read(),read());
	if(c=='A')work1();
	else work2();
	return 0;
}
 
