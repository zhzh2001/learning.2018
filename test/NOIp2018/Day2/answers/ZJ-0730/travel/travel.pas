var
  l,sa,ansa,b:array[1..1000]of longint;
  a:array[1..1000,1..1000]of longint;
  n,m,i,j,a1,a2:longint;
procedure dfs(x,s:longint);
var i:longint;
begin
  for i:=1 to s do
  begin
    if sa[i]>ansa[i] then exit;
    if sa[i]<ansa[i] then break;
  end;
  if s=n then
  begin
    ansa:=sa;
    exit;
  end;
  for i:=1 to l[x] do
  if b[a[x,i]]<>2 then
  begin
    if b[a[x,i]]=0 then
    begin
      inc(b[x]);
      sa[s+1]:=a[x,i];
      dfs(a[x,i],s+1);
      dec(b[x]);
    end else
    if b[a[x,i]]=1 then
    begin
      inc(b[x]);
      dfs(a[x,i],s);
      dec(b[x]);
    end;
  end;
end;
begin
  assign(input,'travel.in');reset(input);
  assign(output,'travel.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
  begin
    readln(a1,a2);
    inc(l[a1]);
    a[a1,l[a1]]:=a2;
    inc(l[a2]);
    a[a2,l[a2]]:=a1;
  end;
  ansa[1]:=maxlongint;sa[1]:=1;
  for i:=1 to n do b[i]:=0;
  dfs(1,1);
  for i:=1 to n-1 do write(ansa[i],' ');writeln(ansa[n]);
  close(input);close(output);
end.