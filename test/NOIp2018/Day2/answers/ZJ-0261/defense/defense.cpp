#include<bits/stdc++.h>
#define ll long long
#define M 100005
using namespace std;
bool llq;
vector<int> edge[M];
int n,Q,val[M],fa[M];
char str[5];
void dfs(int x,int f){
	fa[x]=f;
	for(int i=0;i<edge[x].size();++i){
		int y=edge[x][i];
		if (y==f) continue;
		dfs(y,x);
	}
}
struct sc44{
	ll dp[M][2];
	int mark[M];
	void dfs(int x,int f){
		dp[x][0]=0; dp[x][1]=val[x];
		for(int i=0;i<edge[x].size();++i){
			int y=edge[x][i];
			if (y==f) continue;
			dfs(y,x);
			if (dp[y][1]==1e18) dp[x][0]=1e18;
			else dp[x][0]+=dp[y][1];
			dp[x][1]+=min(dp[y][0],dp[y][1]);
		}
		if (mark[x]!=-1) dp[x][mark[x]^1]=1e18;
	}
	void solve(){
		memset(mark,-1,sizeof mark);
		while (Q--){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			mark[a]=x; mark[b]=y;
			if ((fa[a]==b||fa[b]==a)&&(x==0&&y==0)) printf("-1\n");
			else{ dfs(1,0); printf("%lld\n",min(dp[1][0],dp[1][1]));}
			mark[a]=-1; mark[b]=-1;
		}
	}
}p44;
bool LLQ;
int main(){
//	srand(time(NULL));
//	printf("%lf\n",(&LLQ-&llq)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&Q);
	scanf("%s",str);
	for(int i=1;i<=n;++i) scanf("%d",&val[i]);
	for(int i=1;i<n;++i){
		int u,v;
		scanf("%d%d",&u,&v);
		edge[u].push_back(v);
		edge[v].push_back(u);
	}
	dfs(1,0);
//	if (n<=2000)
		p44.solve();
	return 0;
}
