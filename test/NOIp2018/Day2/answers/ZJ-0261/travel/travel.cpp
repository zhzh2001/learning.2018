#include<bits/stdc++.h>
#define ll long long
#define M 5005
using namespace std;
bool llq;
vector<int> edge[M];
int n,m,fa[M],U,V,ans[M],a[M],dep[M],mark[M],K,dx,dy;
int getfa(int x){
	if (fa[x]!=x) fa[x]=getfa(fa[x]);
	return fa[x];
}
void dfs(int x,int f){
	fa[x]=f; dep[x]=dep[f]+1;
	ans[++K]=x;
	for(int i=0;i<edge[x].size();++i){
		int y=edge[x][i];
		if (y==f) continue;
		dfs(y,x);
	}
}
void LCA(int u,int v){
	if (dep[u]>dep[v]) swap(u,v);
	while (dep[v]>dep[u]){
		mark[v]=1; v=fa[v];
	}
	while (u!=v){
		mark[u]=1; u=fa[u];
		mark[v]=1; v=fa[v];
	}
}
void redfs(int x,int f){
	a[++K]=x;
	for(int i=0;i<edge[x].size();++i){
		int y=edge[x][i];
		if (y==f) continue;
		if ((x==dx&&y==dy)||(x==dy&&y==dx)) continue;
		redfs(y,x);
	}
}
bool CMP(){
	for(int i=1;i<=n;++i){
		if (a[i]<ans[i]) return true;
		if (a[i]>ans[i]) return false;
	}
	return false;
}
void calc(int u){
	while (mark[u]){
		dx=u; dy=fa[u];
		K=0;
		redfs(1,0);
		if (CMP())
			for(int i=1;i<=n;++i) ans[i]=a[i];
		u=fa[u];
	}
}
bool LLQ;
int main(){
//	srand(time(NULL));
//	printf("%lf\n",(&LLQ-&llq)/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;++i) fa[i]=i;
	for(int i=1;i<=m;++i){
		int u,v,x,y;
		scanf("%d%d",&u,&v);
		x=getfa(u); y=getfa(v);
		if (x!=y){
			fa[x]=y;
			edge[u].push_back(v); 
			edge[v].push_back(u);
		}else{
			U=u; V=v;
		}
	}
	for(int i=1;i<=n;++i)
		sort(edge[i].begin(),edge[i].end());
	dfs(1,0);
	if (m==n-1){
		for(int i=1;i<=n;++i) printf("%d ",ans[i]);
		printf("\n");
		return 0;
	}
	LCA(U,V);
	edge[U].push_back(V);
	sort(edge[U].begin(),edge[U].end());
	edge[V].push_back(U);
	sort(edge[V].begin(),edge[V].end());
	calc(U); calc(V);
	for(int i=1;i<=n;++i) printf("%d ",ans[i]);
	printf("\n");
	return 0;
}
