#include<bits/stdc++.h>
#define ll long long
using namespace std;
bool llq;
const ll mod=1e9+7;
int n,m;
ll modpow(ll x,ll y){
	ll res=1;
	while (y){
		if (y&1) res=res*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return res;
}
struct sc20{
	int pre[15],now[15],a[15][15];
	bool check(){
		for(int i=1;i<=n+m-1;++i){
			if (pre[i]<now[i]) return false;
			else if (pre[i]>now[i]) break;
		}
		for(int i=1;i<=n+m-1;++i) pre[i]=now[i];
		return true;
	}
	bool dfs(int x,int y,int p){
		now[p]=a[x][y];
		if (x==n&&y==m){
			if (check()) return true;
			return false;
		}
		if (x<n)
			if (!dfs(x+1,y,p+1)) return false;
		if (y<m)
			if (!dfs(x,y+1,p+1)) return false;
		return true;
	}
	void solve(){
		int ans=0;
		for(int i=0;i<(1<<n*m);++i){
			for(int x=1;x<=n;++x)
				for(int y=1;y<=m;++y) 
				a[x][y]=(i&(1<<((x-1)*m+y-1)));
			memset(pre,0x3f3f3f3f,sizeof pre);
			if (dfs(1,1,1)) ans++;
		}
		printf("%d\n",ans);
	}
}p20;
bool LLQ;
int main(){
//	srand(time(NULL));
//	printf("%lf\n",(&LLQ-&llq)/1024.0/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n<=3&&m<=3)
		p20.solve();
	else if (n==1) printf("%lld\n",modpow(2,m));
	else if (n==2) printf("%lld\n",modpow(3,m-1)*4%mod);
	return 0;
}
