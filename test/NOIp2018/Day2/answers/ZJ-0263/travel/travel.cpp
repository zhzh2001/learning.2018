#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;

inline void getint(int &num){
	register int ch, neg = 0;
	while(!isdigit(ch = getchar())) if(ch == '-') neg = 1;
	num = ch & 15;
	while(isdigit(ch = getchar())) num = num * 10 + (ch & 15);
	if(neg) num = -num;
}

int n, m, tope = 0, ans[5005], topans = 0, vis[5005];
int stk[5005], tops = 0, topcir = 0, cur[5005], topcur = 0;
vector<int> ed[5005];
struct Pair {int u, v;} oncir[5005], ban;

void dfs1(int u){
	ans[++topans] = u, vis[u] = 1; int siz = ed[u].size();
	for(register int i = 0; i < siz; i++)
		if(!vis[ed[u][i]]) dfs1(ed[u][i]);
}

bool find_cir(int u, int fa){
	stk[++tops] = u, vis[u] = 1; int siz = ed[u].size();
	for(register int i = 0; i < siz; i++){
		const int &v = ed[u][i];
		if(!vis[v]) {if(find_cir(v, u)) return 1;}
		else if(v != fa){
			oncir[++topcir] = (Pair){u, v};
			while(stk[tops] != v){
				oncir[++topcir] = (Pair){stk[tops], stk[tops - 1]};
				tops--;
			}
			return 1;
		}
	}
	return 0;
}

void dfs2(int u){
	cur[++topcur] = u, vis[u] = 1; int siz = ed[u].size();
	for(register int i = 0; i < siz; i++){
		const int &v = ed[u][i];
		if(!vis[v] && (u != ban.u || v != ban.v) && (u != ban.v || v != ban.u)) dfs2(v);
	}
}

inline void checkans(){
	bool flag = 0;
	for(register int i = 1; i <= n; i++)
		if(cur[i] < ans[i]) {flag = 1; break;}
		else if(cur[i] > ans[i]) {flag = 0; break;}
	if(flag) for(register int i = 1; i <= n; i++) ans[i] = cur[i];
}

int main(){
	freopen("travel.in", "r", stdin), freopen("travel.out", "w", stdout);
	getint(n), getint(m);
	for(register int i = 1; i <= m; i++){
		int u, v; getint(u), getint(v);
		ed[u].push_back(v), ed[v].push_back(u);
	}
	for(register int i = 1; i <= n; i++) sort(ed[i].begin(), ed[i].end());
	if(m == n - 1) dfs1(1);
	else if(m == n){
		find_cir(1, 0), ans[1] = 0x3f3f3f3f;
		for(register int i = 1; i <= topcir; i++){
			memset(vis, 0, sizeof(vis)), topcur = 0;
			ban = oncir[i], dfs2(1), checkans();
		}
	}
	for(register int i = 1; i < n; i++) printf("%d ", ans[i]);
	printf("%d\n", ans[n]);
	return 0;
}
