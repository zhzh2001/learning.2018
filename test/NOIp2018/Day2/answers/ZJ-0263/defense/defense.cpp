#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
using namespace std;
typedef long long ll;

inline void getint(int &num){
	register int ch, neg = 0;
	while(!isdigit(ch = getchar())) if(ch == '-') neg = 1;
	num = ch & 15;
	while(isdigit(ch = getchar())) num = num * 10 + (ch & 15);
	if(neg) num = -num;
}

int n, m, c[100005], tope = 0;
ll f[100005][2]; char tp[5];
struct Edge {int np; Edge *nxt;} E[200005], *V[100005];
bool vis[5005];

inline void addedge(const int &u, const int &v){
	E[++tope].np = v, E[tope].nxt = V[u], V[u] = E + tope;
}

void dfs(int u, int fa){
	vis[u] = 1, f[u][0] = 0, f[u][1] = c[u];
	for(register Edge *ne = V[u]; ne; ne = ne->nxt)
		if(!vis[ne->np]) dfs(ne->np, u), f[u][0] += f[ne->np][1], f[u][1] += min(f[ne->np][0], f[ne->np][1]);
}

int main(){
	freopen("defense.in", "r", stdin), freopen("defense.out", "w", stdout);
	getint(n), getint(m), scanf("%s", tp);
	for(register int i = 1; i <= n; i++) getint(c[i]);
	for(register int i = 1; i < n; i++){
		int u, v; getint(u), getint(v);
		addedge(u, v), addedge(v, u);
	}
	while(m--){
		ll ans = 0; memset(vis, 0, sizeof(vis));
		int a, x, b, y; getint(a), getint(x), getint(b), getint(y);
		if(x == 1) ans += c[a];
		else for(register Edge *ne = V[a]; ne; ne = ne->nxt)
				if(!vis[ne->np]) ans += c[ne->np], vis[ne->np] = 1;
		if(y == 1) {if(!vis[b]) ans += c[b];}
		else{
			if(vis[b]) {puts("-1"); continue;}
			for(register Edge *ne = V[b]; ne; ne = ne->nxt)
				if(!vis[ne->np]) ans += c[ne->np], vis[ne->np] = 1;
		}
		vis[a] = vis[b] = 1;
		for(register int i = 1; i <= n; i++)
			if(!vis[i]) dfs(i, 0), ans += min(f[i][0], f[i][1]);
		printf("%lld\n", ans);
	}
	return 0;
}
