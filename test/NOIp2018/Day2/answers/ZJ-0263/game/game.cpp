#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
#define MOD 1000000007
using namespace std;
typedef long long ll;

inline void getint(int &num){
	register int ch, neg = 0;
	while(!isdigit(ch = getchar())) if(ch == '-') neg = 1;
	num = ch & 15;
	while(isdigit(ch = getchar())) num = num * 10 + (ch & 15);
	if(neg) num = -num;
}

int n, m;
ll f[1000005][4];

inline ll fastpow(ll bas, ll ex){
	register ll res = 1; bas %= MOD;
	while(ex){
		if(ex & 1) res = res * bas % MOD;
		bas = bas * bas % MOD, ex >>= 1;
	}
	return res;
}

int main(){
	freopen("game.in", "r", stdin), freopen("game.out", "w", stdout);
	getint(n), getint(m);
	if(n == 2) printf("%lld\n", fastpow(3, m - 1) * 4 % MOD);
	else if(n == 3){
		ll ans = 4; int mx = m >> 1;
		f[1][0] = f[1][1] = f[1][2] = f[1][3] = 1;
		for(register int i = 2; i <= mx; i++){
			f[i][0] = f[i][1] = f[i][3] = (f[i - 1][0] + f[i - 1][2] + f[i - 1][3]) % MOD;
			f[i][2] = (f[i - 1][0] + f[i - 1][1] + f[i - 1][2] + f[i - 1][3]) % MOD;
		}
		if(m & 1) ans = ans * (f[mx][0] + f[mx][1] + f[mx][2] + f[mx][3]) % MOD;
		else ans = ans * (f[mx][1] + f[mx][2] + f[mx][3]) % MOD;
		
		mx = m + 1 >> 1, f[1][0] = f[1][1] = f[1][2] = 1, f[1][3] = 0;
		for(register int i = 2; i <= mx; i++){
			f[i][0] = f[i][1] = f[i][3] = (f[i - 1][0] + f[i - 1][2] + f[i - 1][3]) % MOD;
			f[i][2] = (f[i - 1][0] + f[i - 1][1] + f[i - 1][2] + f[i - 1][3]) % MOD;
		}
		if(m & 1) ans = ans * (f[mx][1] + f[mx][2] + f[mx][3]) % MOD;
		else ans = ans * (f[mx][0] + f[mx][1] + f[mx][2] + f[mx][3]) % MOD;
		printf("%lld\n", ans);
	}
	return 0;
}
