#include<bits/stdc++.h>
using namespace std;
#define M 5005
#define ll long long
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),!isdigit(c));
	do res=(res<<3)+(res<<1)+(c^48);
	while(c=getchar(),isdigit(c));	
}
bool OK[M][M];
int n,m;
struct ZF1{
	int ans[M],TP;
	void dfs(int x,int pre){
		ans[++TP]=x;
		for(int y=1;y<=n;y++){
			if(!OK[x][y]||y==pre)continue;
			dfs(y,x);
		}
	}
	void solve(){
		TP=0;
		dfs(1,-1);
		for(int i=1;i<=n;i++)printf("%d ",ans[i]);
		puts("");
	}
}zf1;
priority_queue<int>Q;
struct WATER{
	int Use[M],ans[M],TT,ffff;
	void Pfs(int x,int pre){
		if(ffff)return;
		ans[++TT]=x;Use[x]=1;//printf("x=%d\n",x);
		for(int y=1;y<=n;y++)if(!Use[y]&&OK[x][y])Q.push(-y);
		for(int y=1;y<=n;y++){
			if(ffff)return;
			if(!Use[y]&&OK[x][y]){
				//	printf("%d %d %d %d\n",x,y,OK[x][y],-Q.top());
				if(-Q.top()==y){
					Q.pop();
					Pfs(y,x);
				}
				else {//printf("%d\n",y);
					ffff=1;
					return;
				}
			}
		}
	}
	void Perfect(int x,int pre){
		ans[++TT]=x;
		for(int y=1;y<=n;y++){
			if(Use[y]||!OK[x][y]||y==pre)continue;
			Perfect(y,x);
		}
	}
	void solve(){
		TT=ffff=0;
		Pfs(1,-1);
		Perfect(-Q.top(),-1);
		for(int i=1;i<=n;i++)printf("%d ",ans[i]);
		puts("");
	}
}Water;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d %d",&n,&m);
	for(int i=1;i<=m;i++){
		int a,b;
		scanf("%d %d",&a,&b);
		OK[a][b]=OK[b][a]=1;	
	}
	if(m==n-1)zf1.solve();
	else Water.solve();
	return 0;
}
