#include<bits/stdc++.h>
using namespace std;
#define M 100005
#define ll long long
#define P 1000000007
#define Inf 2000000000
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),!isdigit(c));
	do res=(res<<3)+(res<<1)+(c^48);
	while(c=getchar(),isdigit(c));	
}
int n,m,val[M];char Type[5];
int head[M],tot;
struct node{
	int to,nxt;	
}edge[M<<1];
inline void Add(int a,int b){
	edge[tot].to=b;edge[tot].nxt=head[a];head[a]=tot++;
}
struct P48{
	#define N 2005
	int dp[2][N];int a,x,b,y;
	void Min(int &a,int b){
		if(a>b)a=b;
	}
	void dfs(int now,int pre){
//		if(a==now&&p!=x||b==now&&p!=y)return res;
//		if(head[x]==-1)res=1;
		int mi0=0,mi1=0;
		dp[0][now]=0;
		dp[1][now]=val[now];
		for(int i=head[now];~i;i=edge[i].nxt){
			int y=edge[i].to;
			if(y==pre)continue;
			dfs(y,now);
			if(mi1<Inf)mi1+=min(dp[0][y],dp[1][y]);
			if(mi0<Inf)mi0+=dp[1][y];
		}
		if(mi0>=Inf||now==a&&x==1||now==b&&y==1)dp[0][now]=Inf;
		else dp[0][now]=mi0;
		if(mi1>=Inf||now==a&&x==0||now==b&&y==0)dp[1][now]=Inf;
		else dp[1][now]+=mi1;
//		printf("dp[%d]=%d %d\n",now,dp[0][now],dp[1][now]);
	}
	void solve(){
		while(m--){
			Rd(a);Rd(x);Rd(b);Rd(y);
			for(int i=1;i<=n;i++)dp[0][i]=dp[1][i]=Inf;
			dfs(1,-1);
			if(dp[0][1]==Inf&&dp[1][1]==Inf)puts("-1");
			else if(dp[0][1]==Inf)printf("%d\n",dp[1][1]);
			else if(dp[1][1]==Inf)printf("%d\n",dp[0][1]);
			else printf("%d\n",min(dp[0][1],dp[1][1]));
		}
	}
}p44;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d %d %s",&n,&m,Type);
	for(int i=1;i<=n;i++)Rd(val[i]);
	memset(head,-1,sizeof(head));
	for(int i=1;i<n;i++){
		int x,y;
		Rd(x);Rd(y);
		Add(x,y);Add(y,x);
	}
	if(n<=2000)p44.solve();
	return 0;
}
