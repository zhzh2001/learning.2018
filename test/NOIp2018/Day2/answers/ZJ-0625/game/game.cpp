#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define P 1000000007
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),!isdigit(c));
	do res=(res<<3)+(res<<1)+(c^48);
	while(c=getchar(),isdigit(c));	
}
int ksm(int a,int b){
	int res=1;
	while(b){
		if(b&1)res=1ll*res*a%P;
		a=1ll*a*a%P;
		b>>=1;
	}
	return res;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	scanf("%d %d",&n,&m);
	int ans=1;
	if(n>m)swap(n,m);
	if(n==1&&m==1)puts("2");
	else if(n==1&&m==2)puts("4");
	else if(n==1&&m==3)puts("8");
	else if(n==2&&m==2)puts("12");
	else if(n==2&&m==3)puts("36");
	else if(n==3&&m==3)puts("112");
	else if(n==2){
		ans=4ll*ksm(3,m-n+1)%P;
		printf("%d\n",ans);
	}
	return 0;
}
