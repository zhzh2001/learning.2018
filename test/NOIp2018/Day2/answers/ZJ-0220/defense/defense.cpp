#include<bits/stdc++.h>
#define M 100005
#define ll long long
using namespace std;
bool F_lian;
int n,m,a,b,x,y;
char str[10];
int val[M],A[M],B[M];
vector<int>G[M];
void Add_edge(int a,int b){
	G[a].push_back(b);
	G[b].push_back(a);
}
struct P1{
	int mark[15];
	int res;
	bool check(){
		for(int i=1;i<n;++i){
			if(mark[A[i]]==1&&mark[B[i]]==1)return false;
		}
		return true;
	}
	void dfs(int x,int sum){
		if(res<=sum)return;
		if(mark[x]==2)sum+=val[x];
		if(x==n+1){
			if(check())res=min(res,sum);
			return;
		}
		int y=x+1;
		if(mark[y])dfs(y,sum);
		else{
			mark[y]=1;dfs(y,sum);
			mark[y]=2;dfs(y,sum);
			mark[y]=0;
		}
	}
	void solve(){
		for(int i=1;i<=m;++i){
			scanf("%d %d %d %d",&a,&x,&b,&y);
			memset(mark,0,sizeof(mark));
			mark[a]=x+1;mark[b]=y+1;res=1000000000;
			if(mark[1])dfs(1,0);
			else{
				mark[1]=1;dfs(1,0);
				mark[1]=2;dfs(1,0);
			}
			if(res==1000000000)puts("-1");
			else printf("%d\n",res);
		}
	}
}P1;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	F_lian=1;
	scanf("%d %d %s",&n,&m,str);
	for(int i=1;i<=n;++i)scanf("%d",&val[i]);
	for(int i=1;i<n;++i){
		scanf("%d %d",&A[i],&B[i]);
		if(A[i]>B[i])swap(A[i],B[i]);
		if(B[i]!=A[i]+1)F_lian=0;
		Add_edge(A[i],B[i]);
	}
	if(n<=10&&m<=10)P1.solve();
	return 0;
}
