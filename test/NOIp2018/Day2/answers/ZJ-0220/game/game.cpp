#include<bits/stdc++.h>
#define P 1000000007
#define ll long long
using namespace std;
int n,m;
struct P1{
	bool f;
	int cnt;
	int res,tmp;
	int A[15][15];
	void rdfs(int x,int y,int ans){
		if(x==n&&y==m){
			if(tmp>=ans)tmp=ans;
			else f=1;
			return;
		}
		if(x!=n)rdfs(x+1,y,ans*2+A[x+1][y]);
		if(y!=m)rdfs(x,y+1,ans*2+A[x][y+1]);
	}
	void dfs(int x,int y){
		if(x==n&&y==m){
			res+=2;
			if(A[1][1]==1){
				tmp=1000000000;f=0;
				rdfs(1,1,0);
				if(f)cnt++;
			}
			return;
		}
		A[x][y]=1;
		if(y!=m)dfs(x,y+1);
		else dfs(x+1,1);
		if(!A[x-1][y+1]){
			A[x][y]=0;
			if(y!=m)dfs(x,y+1);
			else dfs(x+1,1);
		}
	}
	void solve(){
		memset(A,0,sizeof(A));A[n][m]=1;
		res=0;cnt=0;
		dfs(1,1);
		printf("%d\n",res-cnt*4);
	}
}P1;
ll Calc(ll x,ll a){
	ll ans=1;
	while(a){
		if(a&1)ans=ans*x%P;
		x=x*x%P;
		a>>=1;
	}
	return ans;
}
struct P2{
	void solve(){
		ll res=1LL*2*2*Calc(3,m-1);
		printf("%lld\n",res);
	}
}P2;
struct P3{
	void solve(){
		ll res=1LL*16*7*Calc(3,m-3);
		printf("%lld\n",res);
	}
}P3;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d %d",&n,&m);
	if(n<=5&&m<=5)P1.solve();
	else if(n==2)P2.solve();
	else if(n==3)P3.solve();
	else P1.solve();
	return 0;
}
