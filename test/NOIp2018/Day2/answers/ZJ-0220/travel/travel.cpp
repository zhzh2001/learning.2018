#include<bits/stdc++.h>
#define M 5005
#define ll long long
using namespace std;
int n,m,a[M],b[M];
vector<int>G[M];
void Add_edge(int a,int b){
	G[a].push_back(b);
	G[b].push_back(a);
}
struct P1{
	int cnt;
	void dfs(int x,int f){
		cnt++;
		if(cnt!=n)printf("%d ",x);
		else printf("%d\n",x);
		for(int i=0;i<G[x].size();++i){
			int y=G[x][i];
			if(y==f)continue;
			dfs(y,x);
		}
	}
	void solve(){
		cnt=0;
		dfs(1,1);
	}
}P1;
struct P2{
	int cnt;
	int s,t;
	int fa[M],dep[M];
	int res[5][M];
	int getfa(int x){
		if(x==fa[x])return x;
		return fa[x]=getfa(fa[x]);
	}
	void dfs1(int x,int f){
		fa[x]=f;
		dep[x]=dep[f]+1;
		res[0][cnt++]=x;
		for(int i=0;i<G[x].size();++i){
			int y=G[x][i];
			if(y==f)continue;
			if((x==s&&y==t)||(x==t&&y==s))continue;
			dfs1(y,x);
		}
	}
	void dfs(int x,int f){
		res[1][cnt++]=x;
		for(int i=0;i<G[x].size();++i){
			int y=G[x][i];
			if(y==f)continue;
			if((x==s&&y==t)||(x==t&&y==s))continue;
			dfs(y,x);
		}
	}
	bool check(int a,int b){
		for(int i=0;i<n;++i){
			if(res[a][i]!=res[b][i])return res[a][i]<res[b][i];
		}
		return false;
	}
	void solve(){
		for(int i=1;i<=n;++i)fa[i]=i;
		for(int i=1;i<=m;++i){
			int p1=getfa(a[i]),p2=getfa(b[i]);
			if(p1!=p2)fa[p1]=p2;
			else{
				s=a[i];t=b[i];
			}
		}
		cnt=0;
		dfs1(1,1);
		
		int x=s,y=t;
		while(x!=y){
			cnt=0;
			if(dep[x]>dep[y]){
				s=x;t=fa[x];
				dfs(1,1);
				x=fa[x];
			}else{
				s=y;t=fa[y];
				dfs(1,1);
				y=fa[y];
			}
			if(check(1,0)){
				for(int i=0;i<n;++i)res[0][i]=res[1][i];
			}
		}
		
		for(int i=0;i<n-1;++i){
			printf("%d ",res[0][i]);
		}printf("%d\n",res[0][n-1]);
	}
}P2;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d %d",&n,&m);
	for(int i=1;i<=m;++i){
		scanf("%d %d",&a[i],&b[i]);
		Add_edge(a[i],b[i]);
	}
	for(int i=1;i<=n;++i){
		int k=G[i].size();
		if(k>=2)sort(G[i].begin(),G[i].end());
	}
	if(m==n-1)P1.solve();
	else P2.solve();
	return 0;
}
