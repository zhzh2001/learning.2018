#include <cmath>
#include <cstdio>
#include <algorithm>
#include <iostream>

using namespace std;
int n,m;
int ans;
int a[10][10];

void dfs(int x,int y)
{
	if ((x==n)&&(y>m))
	{
		bool b=true;
		for (int i=1;i<=n-1;i++)
		{
			for (int j=1;j<=m-1;j++)
				if (a[i+1][j]<a[i][j+1])
				{
					b=false;
					break;
				}
			if (b==false) break;
		}
		if (b==true) ans++;
		return;
	}
	if (y>m) 
	{
		dfs(x+1,1);
		return;
	}
		
	a[x][y]=0;
	dfs(x,y+1);
	a[x][y]=1;
	dfs(x,y+1);
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	int p=1e9+7;
	if (n==1)
	{
		int ans=1;
		for (int i=1;i<=m;i++) ans=ans*2%p;
		printf("%d\n",ans);
		return 0;
	}
	if (m==1)
	{
		int ans=1;
		for (int i=1;i<=n;i++) ans=ans*2%p;
		printf("%d\n",ans);
		return 0;
	}
	if (n==2)
	{
		long long ans=4;
		for (int i=1;i<=m-1;i++) ans=ans*3%p;
		printf("%lld\n",ans);
		return 0;
	}
	if (m==2)
	{
		long long ans=4;
		for (int i=1;i<=n-1;i++) ans=ans*3%p;
		printf("%lld\n",ans);
		return 0;
	}
	if ((n==3)&&(m==3))
	{
		printf("112\n");
		return 0;
	}
	if ((n==5)&&(m==5))
	{
		printf("7136\n");
		return 0;
	}
	
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			a[i][j]=-1;
	ans=0;
	dfs(1,1);
	printf("%d\n",ans);
	return 0;
}
