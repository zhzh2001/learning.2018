#include <cmath>
#include <cstdio>
#include <algorithm>
#include <iostream>

using namespace std;
int n,m,f[5005][5005],fa[5005],mark[5005];

void dfs(int n_ow)
{
	for (int i=1;i<=n;i++)
		if ((mark[i]==1)&&(f[n_ow][i]==1))
		{
			fa[i]=n_ow;
			mark[i]=0;
			dfs(i);
		}
}

void wrdfs(int n_ow)
{
	for (int i=1;i<=n;i++)
		if (fa[i]==n_ow)
		{
			printf(" %d",i);
			wrdfs(i);
		}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		f[x][y]=1;f[y][x]=1;
	}
	for (int i=1;i<=n;i++) mark[i]=1;
	for (int i=1;i<=n;i++) fa[i]=-1;
	mark[1]=0;
	dfs(1);
	printf("1");
	wrdfs(1);
	return 0;
}
