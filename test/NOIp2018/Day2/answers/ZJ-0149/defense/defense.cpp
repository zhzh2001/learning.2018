#include<bits/stdc++.h>
using namespace std;
#define M 100005
bool cur1;
int n,m,i,tot,u,V,p[M],h[M],v[M],nxt[M];
char t[3];
void add(int x,int y) {
	v[++tot]=y,nxt[tot]=h[x],h[x]=tot;
}
struct P1 {
	int a,b,i,fa[M];
	bool x,Y;
	long long dp[2][M];
	void dfs(int x,int f) {
		for(int i=h[x]; i!=-1; i=nxt[i]) {
			int y=v[i];
			if(y!=f)dfs(y,x),dp[0][x]+=dp[1][y],dp[1][x]+=y==b?dp[Y][b]:min(dp[1][y],dp[0][y]);
		}
		dp[1][x]+=p[x];
	}
	void rdfs(int x,int f) {
		for(int i=h[x]; i!=-1; i=nxt[i])if(v[i]!=f)fa[v[i]]=x,rdfs(v[i],x);
	}
	void solve() {
		rdfs(1,0);
		while(m--) {
			scanf("%d%d%d%d",&a,&x,&b,&Y);
			if(!x&&!Y&&(fa[a]==b||fa[b]==a))printf("-1\n");
			else memset(dp,0,sizeof(dp)),dfs(a,0),printf("%lld\n",dp[x][a]);
		}
	}
} p1;
//struct P2 {
//	bool now[2][M],st[M],X,Y;
//	long long dp[2][M],ans,a,b,sum,chang[M];
//	void dfs(int x) {
//		if(x==n)return;
//		dfs(x+1),now[0][x]=1,dp[0][x]=dp[1][x+1],dp[1][x]=p[x]+min(dp[0][x+1],dp[1][x+1]),now[1][x]=dp[1][x+1]<dp[0][x+1]?1:0;
//	}
//	void rdfs(int x,int v) {
//		st[x]=v;
//		if(x==n)return;
//		rdfs(x+1,now[v][x]);
//	}
//	void solve() {
//		dfs(1);
//		if(dp[0][1]<dp[1][1])ans=dp[0][1],rdfs(1,0);
//		else rdfs(1,1),ans=dp[1][1];
//		while(m--) {
//			scanf("%d%d%d%d",&a,&X,&b,&Y);
//			if((a==b+1||b==a+1)&&!X&&!Y)printf("-1\n");
//			else {
//				if(st[a]!=X);
//				if(st[b]!=Y);
//				printf("%lld\n",sum);
//			}
//		}
//	}
//} p2;
bool cur2;
int main() {
//	printf("%.lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,t),memset(h,-1,sizeof(h));
	for(i=1; i<=n; i++)scanf("%d",&p[i]);
	for(i=1; i<n; i++)scanf("%d%d",&u,&V),add(u,V),add(V,u);
	p1.solve();
	return 0;
}
