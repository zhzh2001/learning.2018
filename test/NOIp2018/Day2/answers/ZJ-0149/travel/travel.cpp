#include<bits/stdc++.h>
using namespace std;
#define M 5005
bool cur1;
int tot,n,m,i,u,v;
vector<int>a[M];
struct P1 {
	bool mark[M];
	int ans[M],x,si;
	void dfs(int x,int f) {
		sort(a[x].begin(),a[x].end()),ans[++si]=x;
		for(int i=0; i<a[x].size(); i++)
			if(a[x][i]!=f)dfs(a[x][i],x);
	}
	void solve() {
		dfs(1,0);
		for(i=1; i<=n; i++) {
			printf("%d",ans[i]);
			if(i!=n)printf(" ");
			else printf("\n");
		}
	}
} p1;
struct P2 {
	int dfn[M],cn,low[M],st,s[M],b[M],B,i,cnt,ans[M],si,fa,nxt;
	bool mark[M],mk[M],F;
	void tarjan(int x,int f) {
		if(cnt>1)return;
		int j;
		dfn[x]=low[x]=++cn,s[++st]=x;
		for(int i=0; i<a[x].size(); i++) {
			j=a[x][i];
			if(j!=f)
				if(!dfn[j])tarjan(j,x),low[x]=min(low[x],low[j]);
				else if(!b[j]&&dfn[j]<low[x])low[x]=dfn[j];
		}
		if(cnt>1)return;
		if(low[x]==dfn[x]) {
			B++,cnt=0;
			do j=s[st--],mark[j]=1,cnt++;
			while(j!=x);
			if(cnt<=1)mark[x]=0;
		}
	}
	void dfs(int x,int f) {
		sort(a[x].begin(),a[x].end()),ans[++si]=x,mk[x]=1;
		if(mark[x]&&!fa)fa=x;
		for(int i=0; i<a[x].size(); i++)
			if(!mk[a[x][i]]&&a[x][i]!=f) {
				if(!F&&mark[x]&&mark[a[x][i]]) {
					if(i+1<a[x].size())nxt=a[x][i+1];
					else if(nxt<a[x][i]) {
						F=1;
						return;
					}
				}
				dfs(a[x][i],x);
			}
	}
	void solve() {
		for(i=1; i<=n&&cnt<=1; i++)if(!dfn[i])tarjan(i,0);
		dfs(1,0);
		for(i=1; i<=n; i++) {
			printf("%d",ans[i]);
			if(i!=n)printf(" ");
			else printf("\n");
		}
	}
} p2;
bool cur2;
int main() {
//	printf("%.lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(i=1; i<=m; i++)scanf("%d%d",&u,&v),a[u].push_back(v),a[v].push_back(u);
	if(n==m+1)p1.solve();
	else p2.solve();
	return 0;
}
