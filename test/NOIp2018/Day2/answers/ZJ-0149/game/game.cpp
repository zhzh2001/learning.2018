#include<bits/stdc++.h>
using namespace std;
#define P 1000000007
bool cur1;
int n,m,i;
struct P1 {
	int a[16][16],i,j,ans,t[101],k[101],si;
	bool mark[16][16];
	struct node {
		int x,y;
	};
	vector<node>s[101];
	bool check() {
		for(i=0; i<si; i++) {
			t[i]=0;
			for(j=0; j<s[i].size(); j++)t[i]=t[i]*2+a[s[i][j].x][s[i][j].y];
		}
		for(i=0; i<si; i++)
			for(j=0; j<i; j++)
				if(t[i]<t[j])return 0;
		return 1;
	}
	void dfs(int x,int y) {
		if(x==n) {
			if(check())ans++;
			if(ans==P)ans=0;
			return;
		}
		if(y==m) {
			dfs(x+1,0);
			return;
		}
		a[x][y]=1,dfs(x,y+1);
		a[x][y]=0,dfs(x,y+1);
	}
	void redfs(int x,int y,int si,int c) {
		s[si].push_back((node) {
			x,y
		});
		if(c+3==n+m)return;
		if(k[c])redfs(x,y+1,si,c+1);
		else redfs(x+1,y,si,c+1);
	}
	void rdfs(int x,int y,int c) {
		if(c+3==n+m) {
			redfs(0,0,si,0),si++;
			return;
		}
		if(x)k[c]=1,rdfs(x-1,y,c+1);
		if(y)k[c]=0,rdfs(x,y-1,c+1);
	}
	void solve() {
		rdfs(m-1,n-1,0),dfs(0,0),printf("%d\n",ans);
	}
} p1;
int mul(int y,int x) {
	int ans=1;
	while(x) {
		if(x&1)ans=1ll*ans*y%P;
		y=1ll*y*y%P;
		x>>=1;
	}
	return ans;
}
struct P2 {
	int ans;
	void solve() {
		ans=1ll*mul(3,m-1)*4%P,printf("%d\n",ans);
	}
} p2;
struct P3 {
	int ans;
	void solve() {
		ans=mul(2,m),printf("%d\n",ans);
	}
} p3;
struct P4 {
	int ans;
	void solve() {
		if(m==1)printf("8\n");
		else if(m==2)printf("36\n");
		else {
			ans=1ll*112*mul(3,m-3)%P;
			printf("%d\n",ans);
		}
	}
} p4;
struct P5 {
	int ans;
	void solve() {
	}
} p5;
bool cur2;
int main() {
//	printf("%.lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m<n)swap(n,m);
	if(n==2)p2.solve();
	else if(n==1)p3.solve();
	else if(n==3)p4.solve();
	else p1.solve();
	return 0;
}
