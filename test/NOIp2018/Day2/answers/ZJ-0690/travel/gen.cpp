#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int maxn = 5000;

void Edge(int u, int v) {
  printf("%d %d\n", u + 1, v + 1);
}

int main(void) {
  int n = maxn, m = n;
  printf("%d %d\n", n, m);
  for (int i = 0; i < n; ++i) {
    Edge(i, (i + 1) % n);
  }
  // for (int i = 0; i < 2000; ++i) {
  //   Edge(i, (i + 1) % 2000);
  // }
  // for (int i = 2000; i < n; ++i) {
  //   Edge(0, i);
  // }
  return 0;
}
