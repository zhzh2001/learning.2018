#include <cstdio>
#include <cstring>
#include <algorithm>
// #include <vector>
using namespace std;

template<typename T> struct vector {
  T *a;
  int sz, cap;

  vector(void) : sz(0), cap(1) {
    a = new T[1];
  }

  ~vector(void) {
    delete[] a;
  }

  void Extend(void) {
    cap *= 2;
    T *b = new T[cap];
    memcpy(b, a, sz * sizeof(T));
    delete[] a;
    a = b;
  }

  inline void push_back(const T &x) {
    if (sz == cap) Extend();
    a[sz++] = x;
  }

  inline T &operator [] (int i) {
    return a[i];
  }

  inline T operator [] (int i) const {
    return a[i];
  }

  inline int size(void) const {
    return sz;
  }

  void clear(void) {
    sz = 0;
  }

  void erase(T *b) {
    int i = b - a;
    for (int j = i; j < sz - 1; ++j) {
      a[j] = a[j + 1];
    }
    --sz;
  }

  T *begin(void) {
    return a;
  }

  T *end(void) {
    return a + sz;
  }

  bool operator < (vector<T> &b) const {
    for (int i = 0; i < size() && i < b.size(); ++i) {
      if (a[i] < b[i]) return true;
      if (b[i] < a[i]) return false;
    }
    return size() < b.size();
  }

  void operator = (const vector<T> &b) {
    while (cap < b.size()) Extend();
    memcpy(a, b.a, b.size() * sizeof(T));
    sz = b.size();
  }
};

const int maxn = 5000;
int n, m;
vector<int> g[maxn];
vector<int> anst;

void DfsTree(int u, int p) {
  anst.push_back(u);
  for (int i = 0; i < g[u].size(); ++i) {
    int v = g[u][i];
    if (v == p) continue;
    DfsTree(v, u);
  }
}

void SolveTree(void) {
  anst.clear();
  DfsTree(0, -1);
}

void Print(vector<int> &a) {
  if (a.size() != n) throw;
  for (int i = 0; i < n; ++i) {
    printf("%d%c", a[i] + 1, " \n"[i + 1 == n]);
  }
}

bool vis[maxn];
int fa[maxn];
vector<int> cycle;

void GetCycle(int u, int v) {
  cycle.clear();
  cycle.push_back(u);
  while (u != v) {
    u = fa[u];
    cycle.push_back(u);
  }
}

bool DfsGraph(int u) {
  vis[u] = true;
  for (int i = 0; i < g[u].size(); ++i) {
    int v = g[u][i];
    if (v == fa[u]) continue;
    if (vis[v]) {
      GetCycle(u, v);
      return true;
    }
    fa[v] = u;
    if (DfsGraph(v)) {
      return true;
    }
  }
  return false;
}

void Remove(vector<int> &a, int x) {
  a.erase(find(a.begin(), a.end(), x));
}

void Insert(vector<int> &a, int x) {
  a.push_back(x);
  int i = a.size() - 1;
  while (i > 0 && x < a[i - 1]) {
    a[i] = a[i - 1];
    --i;
  }
  a[i] = x;
}

int main(void) {
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);
  scanf("%d%d", &n, &m);
  for (int i = 0; i < m; ++i) {
    int u, v;
    scanf("%d%d", &u, &v);
    --u; --v;
    g[u].push_back(v);
    g[v].push_back(u);
  }
  for (int i = 0; i < n; ++i) {
    sort(g[i].begin(), g[i].end());
  }
  if (m == n - 1) {
    SolveTree();
    Print(anst);
    return 0;
  }

  fa[0] = -1;
  DfsGraph(0);
  vector<int> ans;
  ans.push_back(n);
  for (int i = 0; i < cycle.size(); ++i) {
    int u = cycle[i], v = cycle[(i + 1) % cycle.size()];
    Remove(g[u], v);
    Remove(g[v], u);
    SolveTree();
    if (anst < ans) ans = anst;
    Insert(g[u], v);
    Insert(g[v], u);
  }
  Print(ans);

  return 0;
}
