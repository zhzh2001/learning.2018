#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

typedef long long li;

const int mod = 1e9 + 7;

inline int Add(int x) { return x >= mod ? x - mod : x; }
inline int Sub(int x) { return x < 0 ? x + mod : x; }
inline int Mul(int x, int y) { return (li)x * y % mod; }
void Add(int &x, int y) { x += y; if (x >= mod) x -= mod; }

int Pow(int x, int y) {
  int z = 1;
  for (; y; y >>= 1) {
    if (y & 1) z = Mul(z, x);
    x = Mul(x, x);
  }
  return z;
}

int n, m, cnt;
int a[50][500];
int mxlen[500];
int seq[500], seq2[500];

void Check(void) {
  memcpy(seq2, seq, sizeof seq);
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      if (seq2[i + j] > 0) {
        --seq2[i + j];
        a[i][j] = 0;
      } else {
        a[i][j] = 1;
      }
    }
  }
  for (int i = 1; i < n; ++i) {
    for (int j = 1; j < m; ++j) {
      if (a[i - 1][j] == a[i][j - 1]) {
        for (int i2 = i; i2 + 1 < n; ++i2) {
          for (int j2 = j; j2 + 1 < m; ++j2) {
            if (a[i2 + 1][j2] != a[i2][j2 + 1]) {
              return;
            }
          }
        }
      }
    }
  }
  ++cnt;
}

void Dfs(int i) {
  if (i == n + m - 1) {
    Check();
    return;
  }
  for (int k = 0; k <= mxlen[i]; ++k) {
    seq[i] = k;
    Dfs(i + 1);
  }
}

void Main(void) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      ++mxlen[i + j];
    }
  }
  Dfs(0);
  printf("%d\n", cnt);
}

int main(void) {
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);
  scanf("%d%d", &n, &m);
  if (n > m) swap(n, m);
  if (n == 1) {
    printf("%d\n", Pow(2, m));
  } else if (n == 2) {
    printf("%d\n", Mul(4, Pow(3, m - 1)));
  } else if (n == 3) {
    printf("%d\n", Mul(112, Pow(3, m - 3)));
  } else {
    Main();
  }
  return 0;
}
