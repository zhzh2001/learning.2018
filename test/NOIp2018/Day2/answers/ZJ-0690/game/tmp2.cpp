#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

typedef long long li;

const int mod = 1e9 + 7;

inline int Add(int x) { return x >= mod ? x - mod : x; }
inline int Sub(int x) { return x < 0 ? x + mod : x; }
inline int Mul(int x, int y) { return (li)x * y % mod; }
void Add(int &x, int y) { x += y; if (x >= mod) x -= mod; }

int Pow(int x, int y) {
  int z = 1;
  for (; y; y >>= 1) {
    if (y & 1) z = Mul(z, x);
    x = Mul(x, x);
  }
  return z;
}

int n, m;
int dp[2][1 << 8][8];

int main(void) {
  scanf("%d%d", &n, &m);
  int cur = 0, lst = 1;
  for (int i = 0; i < 1 << n; ++i) {
    dp[cur][i][n - 1] = 1;
  }
  for (int t = 1; t < m; ++t) {
    swap(cur, lst);
    for (int i = 0; i < 1 << n; ++i) {
      memset(dp[cur][i], 0, sizeof dp[cur][i]);
    }
    for (int s = 0; s < 1 << n; ++s) {
      for (int k = 0; k < n; ++k) {
        int nxt[8];
        memset(nxt, -1, sizeof nxt);
        for (int i = 1; i < n; ++i) {
          if (~s >> i & 1) {
            nxt[i - 1] = 0;
          } else if (i - 1 >= k) {
            nxt[i - 1] = 1;
          }
        }
        
      }
    }
  }
}
