#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

void Make(void) {
  FILE *f = fopen("in", "w");
  int n = 2000, m = 2000;
  fprintf(f, "%d %d A3\n", n, m);
  for (int i = 0; i < n; ++i) fprintf(f, "%d\n", rand() % 100000 + 1);
  for (int i = 0; i < n - 1; ++i) fprintf(f, "%d %d\n", i + 1, i + 2);
  for (int i = 0; i < m; ++i) {
    int u = rand() % n, v = rand() % (n - 1);
    if (v >= u) ++v;
    fprintf(f, "%d %d %d %d\n", u + 1, rand() & 1, v + 1, rand() & 1);
  }
  fclose(f);
}

void Run(void) {
  system("./main < in > out && ./brute < in > ans");
  if (system("diff -b out ans")) {
    exit(0);
  }
  puts("AC");
}

int main(void) {
  while (true) Make(), Run();
}
