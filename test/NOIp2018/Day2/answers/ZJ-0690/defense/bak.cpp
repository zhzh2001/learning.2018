#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long li;
const li infl = 1e18;

const int maxn = 2000;
int n, m, cond[maxn], p[maxn];
vector<int> g[maxn];
li dp[maxn][2];

void Dfs(int u, int fa) {
  dp[u][0] = dp[u][1] = 0;
  for (int i = 0; i < g[u].size(); ++i) {
    int v = g[u][i];
    if (v == fa) continue;
    Dfs(v, u);
    dp[u][0] += dp[v][1];
    dp[u][1] += min(dp[v][0], dp[v][1]);
  }
  dp[u][1] += p[u];
  if (dp[u][0] >= infl) dp[u][0] = infl;
  if (dp[u][1] >= infl) dp[u][1] = infl;
  if (cond[u] == 0) {
    dp[u][1] = infl;
  } else if (cond[u] == 1) {
    dp[u][0] = infl;
  }
}

int main(void) {
  scanf("%d%d%*s", &n, &m);
  for (int i = 0; i < n; ++i) {
    scanf("%d", p + i);
  }
  for (int i = 1; i < n; ++i) {
    int u, v;
    scanf("%d%d", &u, &v);
    --u; --v;
    g[u].push_back(v);
    g[v].push_back(u);
  }
  fill(cond, cond + n, -1);
  while (m--) {
    int u, x, v, y;
    scanf("%d%d%d%d", &u, &x, &v, &y);
    --u; --v;
    cond[u] = x;
    cond[v] = y;
    Dfs(0, -1);
    cond[u] = cond[v] = -1;
    li ans = min(dp[0][0], dp[0][1]);
    printf("%lld\n", ans == infl ? -1 : ans);
  }
  return 0;
}
