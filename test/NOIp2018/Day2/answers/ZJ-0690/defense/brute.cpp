#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long li;
const li infl = 1e18;

const int maxn = 1e5;
int n, m, cond[maxn], p[maxn];
vector<int> g[maxn];
li dp[maxn][2];

void Dfs(int u, int fa) {
  dp[u][0] = dp[u][1] = 0;
  for (int i = 0; i < g[u].size(); ++i) {
    int v = g[u][i];
    if (v == fa) continue;
    Dfs(v, u);
    dp[u][0] += dp[v][1];
    dp[u][1] += min(dp[v][0], dp[v][1]);
  }
  dp[u][1] += p[u];
  if (dp[u][0] >= infl) dp[u][0] = infl;
  if (dp[u][1] >= infl) dp[u][1] = infl;
  if (cond[u] == 0) {
    dp[u][1] = infl;
  } else if (cond[u] == 1) {
    dp[u][0] = infl;
  }
}

#define REP(i) for (int i = 0; i <= 1; ++i)

namespace line {
  struct Data {
    li a[2][2];

    Data(void) {}
    Data(int p) {
      REP(i) REP(j) a[i][j] = i * p;
      a[0][0] = infl;
    }

    Data operator + (const Data &b) const {
      Data c;
      REP(i) REP(j) c.a[i][j] = infl;
      REP(i) REP(j) REP(k) c.a[i][k] = min(c.a[i][k], a[i][j] + b.a[j][k]);
      return c;
    }

    void Print(void) {
      REP(i) REP(j) printf("%lld%c", a[i][j], " \n"[i && j]);
    }

    void Make(void) {
      REP(i) REP(j) a[i][j] = i == j ? 0 : infl;
    }
  } t[maxn << 1];

  void Init(void) {
    for (int i = 0; i < n; ++i) {
      t[n + i] = Data(p[i]);
    }
    for (int i = n - 1; i; --i) {
      t[i] = t[i << 1] + t[i << 1 | 1];
    }
  }

  Data Query(int l, int r) {
    Data rl, rr;
    rl.Make(), rr.Make();
    for (l += n, r += n; l < r; l >>= 1, r >>= 1) {
      if (l & 1) rl = rl + t[l++];
      if (r & 1) rr = t[--r] + rr;
    }
    return rl + rr;
  }

  li Query(int u, int x, int v, int y) {
    if (u > v) swap(u, v), swap(x, y);
    Data d1 = Query(0, u);
    Data d2 = Query(u, v);
    Data d3 = Query(v, n);
    return min(min(d1.a[0][x], d1.a[1][x]) + d2.a[x][y] + min(d3.a[y][0], d3.a[y][1]), infl);
  }
}

int main(void) {
  char type[4];
  scanf("%d%d%s", &n, &m, type);
  for (int i = 0; i < n; ++i) {
    scanf("%d", p + i);
  }
  for (int i = 1; i < n; ++i) {
    int u, v;
    scanf("%d%d", &u, &v);
    --u; --v;
    g[u].push_back(v);
    g[v].push_back(u);
  }
  fill(cond, cond + n, -1);
  while (m--) {
    int u, x, v, y;
    scanf("%d%d%d%d", &u, &x, &v, &y);
    li ans;
    --u; --v;
    cond[u] = x;
    cond[v] = y;
    Dfs(0, -1);
    cond[u] = cond[v] = -1;
    ans = min(dp[0][0], dp[0][1]);
    printf("%lld\n", ans == infl ? -1 : ans);
  }
  return 0;
}
