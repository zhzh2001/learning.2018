#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(;isdigit(ch);ch=getchar())x=(x<<1)+(x<<3)+ch-48;
	return x*f;
}const int N=5100;
int n,m,x,y,cnt,clo,a[N],vis[N],pos,b[N];
vector<int>G[N];bool fft=0;
void dfs(int x,int fa){
	a[++clo]=x;vis[x]=1;
	for(int i=0,to;i<G[x].size();i++){
		to=G[x][i];
		if(to==fa||vis[to])continue;
		dfs(to,x);
	}
}void pfs(int x,int fa){
	b[++clo]=x;vis[x]=1;
	for(int i=0,to;i<G[x].size();i++){
		to=G[x][i];
		if(to==fa||vis[to])continue;
		if(to==pos&&fft){
			fft=0;return;
		}pfs(to,x);
	}
}
inline void write(int x){
	if(x>9)write(x/10);putchar(x%10+48);
}
void solve2(){
	dfs(1,0);
	for(int i=1;i<=n;i++){
		pos=i;clo=0;fft=1;
		memset(vis,0,sizeof vis);
		pfs(1,0);
		if(clo==n){
			bool flg=0;
			for(int j=1;j<=n;j++)if(b[j]<a[j]){
				flg=1;break;
			}else if(b[j]>a[j])break;
			if(flg)
				for(int j=1;j<=n;j++)a[j]=b[j];
		}
	}
	for(int i=1;i<=n;i++){
		write(a[i]);
		if(i!=n)putchar(' ');
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		x=read();y=read();
		G[x].push_back(y);
		G[y].push_back(x);
	}
	if(m==n-1){
		for(int i=1;i<=n;i++)
			sort(G[i].begin(),G[i].end());
		dfs(1,0);
		for(int i=1;i<=n;i++){
			write(a[i]);
			if(i!=n)putchar(' ');
		}
		return 0;
	}else{
		for(int i=1;i<=n;i++)
			sort(G[i].begin(),G[i].end());
		solve2();return 0;
	}
	return 0;
}
