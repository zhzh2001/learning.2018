#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')f=-1;
	for(;isdigit(ch);ch=getchar())x=(x<<1)+(x<<3)+ch-48;
	return x*f;
}const int N=110000;
typedef long long ll;
const ll inf=0x3f3f3f3f3f;
int x,y,n,m,lim[N];char typ[10];
vector<int>G[N];
ll f[N][2],w[N];
void dfs(int x,int fa){
	ll tot=0,f1=w[x],f0=0;
	for(int i=0;i<G[x].size();i++){
		int to=G[x][i];if(to==fa)continue;
		dfs(to,x);tot++;
		if(lim[x]!=0){
			f1+=min(f[to][1],f[to][0]);
		}else f1=inf;
		if(lim[x]!=1)f0+=f[to][1];
		else f0=inf;
	}if(!tot){
		if(lim[x]!=-1){
			if(!lim[x])f[x][0]=0;
			else f[x][1]=w[x];
		}else f[x][1]=w[x],f[x][0]=0;
	}else{
		if(f1<inf)f[x][1]=f1;
		if(f0<inf)f[x][0]=f0;
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",typ);
	for(int i=1;i<=n;i++)w[i]=read();
	for(int i=1;i<n;i++){
		x=read();y=read();
		G[x].push_back(y);
		G[y].push_back(x);
	}for(int j=1;j<=n;j++)f[j][0]=f[j][1]=inf;
	memset(lim,-1,sizeof lim);ll ans;
	for(int i=1;i<=m;i++){
		ans=inf;
		for(int j=1;j<=n;j++)f[j][0]=f[j][1]=inf;
		memset(lim,-1,sizeof lim);
		x=read();lim[x]=read();
		y=read();lim[y]=read();
		dfs(1,0);ans=min(f[1][0],f[1][1]);
		printf("%lld\n",ans!=inf?ans:-1);
	}
	return 0;
}
