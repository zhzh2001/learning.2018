#include <bits/stdc++.h>
using namespace std;
#define ll long long
const int mo=1e9+7.3;

int qp(int a,int b)
{
	if(b==0) return 1;
	int t=qp(a,b/2);
	return b&1?1LL*t*t%mo*a%mo:1LL*t*t%mo;
}

int getbit(int x,int i)
{
	return (x>>(i-1))&1;
}

int main()
{
	ios::sync_with_stdio(0);
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	int n,m;
	cin>>n>>m;
	if(m==1)
	{
		cout<<qp(2,n);
		return 0;
	}
	if(n==2)
	{
		cout<<4LL*qp(3,m-1)%mo<<endl;
		return 0;
	}
	if(m==2)
	{
		cout<<4LL*qp(3,n-1)%mo<<endl;
		return 0;
	}
	if(n==1)
	{
		cout<<qp(2,m)<<endl;
		return 0;
	}
	if(n==3 || m==3)
	{
		cout<<112<<endl;
		return 0;
	}
	return 0;
}

