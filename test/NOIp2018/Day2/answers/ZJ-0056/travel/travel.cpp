#include <bits/stdc++.h>
using namespace std;
#define ll long long
int to[5007][5007];
int egnum[5007];
int n,m;

int nox,noy;

int ansrod[5007];
int trod[5007];
bool cmp()
{
	for(int i=1;i<=n;++i)
	{
		if(trod[i]<ansrod[i]) return 1;
		if(trod[i]>ansrod[i]) return 0;
	}
	return 0;
}

void dfs1(int x,int fa)
{
	//cout<<"???"<<endl;
	cout<<x<<" ";
	int chl[5007];
	int chlnum=0;
	for(int i=1;i<=egnum[x];++i)
	{
		if(to[x][i]!=fa)
		{
			chl[++chlnum]=to[x][i];
		}
	}
	if(chlnum) sort(chl+1,chl+chlnum+1);
	for(int i=1;i<=chlnum;++i)
	{
		dfs1(chl[i],x);
	}
}

int rod[5007],leth=0;
bool vis[5007]={0,1};
bool get=0;
int stn=0;
int sti;
void dfs2(int x,int fa)
{
	if(get) return;
	//cout<<x<<endl;
	rod[++leth]=x;
	for(int i=1;i<=egnum[x];++i)
	{
		if(to[x][i]!=fa)
		{
			if(vis[to[x][i]])
			{
				stn=to[x][i];
				get=1;
				for(int i=1;i<=leth;++i)
				{
					if(rod[i]==stn)
					{
						sti=i;
						break;
					}
				}
				return;
			}
			vis[to[x][i]]=1;
			dfs2(to[x][i],x);
			vis[to[x][i]]=0;
		}
	}
	if(get) return;
	--leth;
}

string ts="";
int l=-1;
/*
char *tc(int x)
{
	char ss[10]="";
	int lett=0;
	int tx=x;
	while(tx)
	{
		tx/=10;
		++lett;
	}
	for(int i=0;i<lett;++i)
	{
		tx=x;
		for(int j=0;j<lett-i-1;++j) tx/=10;
		ss[i]=tx%10+'0';
	}
	return ss;
}
*/
int tlen=0;

void dfs3(int x,int fa)
{
	//cout<<"???"<<endl;
	//cout<<x<<" ";
	trod[++tlen]=x;
	/*
	for(int i=1;i<=tlen;++i)
	{
		cout<<trod[i]<<" ";
	}cout<<endl;
	*/
	int chl[5007];
	int chlnum=0;
	for(int i=1;i<=egnum[x];++i)
	{
		if(to[x][i]==fa || (x==nox && to[x][i]==noy) || (x==noy && to[x][i]==nox))
		{
			//cout<<"con\n";
			continue;
		}
		else
		{
			chl[++chlnum]=to[x][i];
		}
	}
	if(chlnum) sort(chl+1,chl+chlnum+1);
	for(int i=1;i<=chlnum;++i)
	{
		dfs3(chl[i],x);
	}
}

int main()
{
	/*
	char ss[10];
	ss[0]='a';
	cout<<"b"+string(ss)+"c"<<endl;
	return 0;
	*/
	ios::sync_with_stdio(0);
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<=m;++i)
	{
		int a,b;
		cin>>a>>b;
		to[a][++egnum[a]]=b;
		to[b][++egnum[b]]=a;
	}
	if(m==n-1)
	{
		dfs1(1,0);
		return 0;
	}
	else
	{
		dfs2(1,0);
		rod[sti-1]=rod[leth];
		for(int i=n;i>=1;--i)
		{
			ansrod[n-i+1]=i;
		}
		//for(int i=1;i<=leth;++i){cout<<rod[i]<<" ";}cout<<endl;
		//cout<<sti<<endl;
		for(int i=sti-1;i<leth;++i)
		{
			nox=rod[i];
			noy=rod[i+1];
			//cout<<"no "<<nox<<" "<<noy<<endl;
			tlen=0;
			dfs3(1,0);
			if(cmp())
			{
				for(int i=1;i<=n;++i)
				{
					ansrod[i]=trod[i];
				}
			}
		}
		for(int i=1;i<=n;++i)
		{
			cout<<ansrod[i]<<" ";
		}
	}
	return 0;
}

