#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("game.in");
ofstream fout("game.out");
const int N=8,M=1000005,MOD=1e9+7;
int n,m,f[M][1<<3];
namespace bf
{
int ans,cc,W[M],P[M],cnt;
bool board[N][N],now,vis[1<<N][1<<N];
void walk(int x,int y,int w,int p)
{
	if(x==n&&y==m)
	{
		for(int i=1;i<=cc;i++)
			if((w>W[i]&&p>P[i])||(w<W[i]&&p<P[i]))
			{
				now=false;
				return;
			}
		W[++cc]=w;
		P[cc]=p;
		return;
	}
	if(x<n)
		walk(x+1,y,w*2,p*2+board[x+1][y]);
	if(y<m)
		walk(x,y+1,w*2+1,p*2+board[x][y+1]);
}
void dfs(int x,int y)
{
	if(x==n+1)
	{
		now=1;
		cc=0;
		walk(1,1,0,board[1][1]);
		ans+=now;
		if(now)
		{
			int pred=0,now=0;
			for(int i=1;i<=n;i++)
			{
				pred=pred*2+board[i][m-1];
				now=now*2+board[i][m];
			}
			vis[pred][now]++;
		}
	}
	else
	{
		board[x][y]=0;
		if(y<m)
			dfs(x,y+1);
		else
			dfs(x+1,1);
		board[x][y]=1;
		if(y<m)
			dfs(x,y+1);
		else
			dfs(x+1,1);
	}
}
};
int main()
{
	fin>>n>>m;
	if(n<=3&&m<=3)
	{
		bf::dfs(1,1);
		fout<<bf::ans<<endl;
	}
	else if(n<=2)
	{
		int t=m;
		m=n;
		bf::dfs(1,1);
		m=t;
		for(int i=0;i<1<<n;i++)
			f[1][i]=1;
		for(int i=2;i<=m;i++)
			for(int j=0;j<1<<n;j++)
				for(int k=0;k<1<<n;k++)
				{
					/*
					bool flag=true;
					int pred=0;
					for(int x=0;x<n;x++)
					{
						int now=((j&((1<<x+1)-1)))+((k&(((1<<n)-1)-(1<<x)+1))<<1);
						if(now<pred)
						{
							flag=false;
							break;
						}
						pred=now;
					}
					if(flag)
						f[i][j]=(f[i][j]+f[i-1][k])%MOD;
					*/
					if(bf::vis[k][j])
						f[i][j]=(f[i][j]+f[i-1][k])%MOD;
				}
		int ans=0;
		for(int i=0;i<1<<n;i++)
			ans=(ans+f[m][i])%MOD;
		fout<<ans<<endl;
	}
	else
	{
		bf::dfs(1,1);
		fout<<bf::ans<<endl;
	}
	return 0;
}
