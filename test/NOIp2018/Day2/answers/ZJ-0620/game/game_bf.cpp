#include<fstream>
using namespace std;
ifstream fin("game.in");
ofstream fout("game.ans");
const int N=15,M=233333;
int n,m,ans,cc,W[M],P[M],cnt;
bool board[N][N],now;
void walk(int x,int y,int w,int p)
{
	if(x==n&&y==m)
	{
		for(int i=1;i<=cc;i++)
			if((w>W[i]&&p>P[i])||(w<W[i]&&p<P[i]))
			{
				now=false;
				return;
			}
		W[++cc]=w;
		P[cc]=p;
		return;
	}
	if(x<n)
		walk(x+1,y,w*2,p*2+board[x+1][y]);
	if(y<m)
		walk(x,y+1,w*2+1,p*2+board[x][y+1]);
}
void dfs(int x,int y)
{
	if(x==n+1)
	{
		now=1;
		cc=0;
		walk(1,1,0,board[1][1]);
		ans+=now;
	}
	else
	{
		board[x][y]=0;
		if(y<m)
			dfs(x,y+1);
		else
			dfs(x+1,1);
		board[x][y]=1;
		if(y<m)
			dfs(x,y+1);
		else
			dfs(x+1,1);
	}
}
int main()
{
	fin>>n>>m;
	dfs(1,1);
	fout<<ans<<endl;
	return 0;
}
