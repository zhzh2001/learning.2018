#include<fstream>
#include<algorithm>
#include<vector>
using namespace std;
ifstream fin("travel.in");
ofstream fout("travel.out");
const int N=5005;
vector<int> mat[N];
int n,m,ans[N],cc,s[N],sp,cycle[N],cp,du,dv,Ans[N];
void dfs(int u,int fat)
{
	ans[++cc]=u;
	for(int i=0;i<mat[u].size();i++)
		if(mat[u][i]!=fat&&!((du==u&&dv==mat[u][i])||(du==mat[u][i]&&dv==u)))
			dfs(mat[u][i],u);
}
bool ins[N],vis[N];
void find_cycle(int u,int fat)
{
	s[++sp]=u;
	ins[u]=true;
	vis[u]=true;
	for(int i=0;i<mat[u].size();i++)
		if(mat[u][i]!=fat)
		{
			if(!ins[mat[u][i]])
			{
				if(!vis[mat[u][i]])
					find_cycle(mat[u][i],u);
			}
			else
			{
				for(;s[sp]!=mat[u][i];sp--)
					cycle[++cp]=s[sp];
				cycle[++cp]=mat[u][i];
			}
		}
	ins[u]=false;
	if(s[sp]==u)
		sp--;
}
void cut()
{
	cc=0;
	dfs(1,0);
	for(int i=1;i<=n;i++)
		if(ans[i]<Ans[i])
			break;
		else if(ans[i]>Ans[i])
			return;
	copy(ans+1,ans+n+1,Ans+1);
}
int main()
{
	fin>>n>>m;
	for(int i=1;i<=m;i++)
	{
		int u,v;
		fin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	for(int i=1;i<=n;i++)
		sort(mat[i].begin(),mat[i].end());
	if(m==n-1)
	{
		dfs(1,0);
		for(int i=1;i<=n;i++)
			fout<<ans[i]<<' ';
		fout<<endl;
	}
	else
	{
		find_cycle(1,0);
		Ans[1]=n;
		for(int i=1;i<cp;i++)
		{
			du=cycle[i];dv=cycle[i+1];
			cut();
		}
		du=cycle[cp];dv=cycle[1];
		cut();
		for(int i=1;i<=n;i++)
			fout<<Ans[i]<<' ';
		fout<<endl;
	}
	return 0;
}
