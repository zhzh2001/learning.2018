#include <cstdio>
#include <algorithm>
#include <vector>
#include <set>
using namespace std;

const int N = 50050;
int f[N], g[N], t1, t2, a[N], b[N], cnt, len, n, m, init[N], Q[N];
vector<int> V[N];
set<pair<int, int> > S;

void dfs(int u, int fa)
{
    g[++cnt] = u;
    for (int i = 0; i < V[u].size(); i++)
    {
        int v = V[u][i];
        if (v == fa) continue;
        if ((u == t1 && v == t2) || (u == t2 && v == t1)) continue;
        dfs(v, u);
    }
}

int main()
{
    freopen("travel.in", "r", stdin);
    freopen("travel.out", "w", stdout);
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= m; i++)
    {
        scanf("%d%d", &a[i], &b[i]);
        V[a[i]].push_back(b[i]);
        V[b[i]].push_back(a[i]);
        init[a[i]]++;
        init[b[i]]++;
    }
    for (int i = 1; i <= n; i++)
        sort(V[i].begin(), V[i].end());
    int H = 1, T = 0;
    for (int i = 1; i <= n; i++)
        if (init[i] == 1)
            Q[++T] = i;
    while (H <= T)
    {
        int u = Q[H++];
        for (int i = 0; i < V[u].size(); i++)
        {
            int v = V[u][i];
            S.insert(make_pair(u, v));
            S.insert(make_pair(v, u));
            init[v]--;
            if (init[v] == 1)
                Q[++T] = v;
        }
    }
    if (m == n)
    {
        for (int i = 1; i <= m; i++)
        {
            t1 = a[i];
            t2 = b[i];
            if (S.count(make_pair(t1, t2))) continue;
            cnt = 0;
            dfs(1, 0);
            if (cnt == n)
                if (len == 0)
                {
                    len = n;
                    for (int i = 1; i <= n; i++)
                        f[i] = g[i];
                } else
                {
                    int flg = 0;
                    for (int i = 1; i <= n; i++)
                        if (g[i] < f[i])
                        {
                            flg = 1;
                            break;
                        } else
                            if (f[i] < g[i])
                                break;
                    if (flg)
                    {
                        for (int i = 1; i <= n; i++)
                            f[i] = g[i];
                    }
                }
        }
    } else
    {
        cnt = 0;
        dfs(1, 0);
        len = n;
        for (int i = 1; i <= n; i++)
            f[i] = g[i];
    }
    for (int i = 1; i < n; i++)
        printf("%d ", f[i]);
    printf("%d\n", f[n]);
}
