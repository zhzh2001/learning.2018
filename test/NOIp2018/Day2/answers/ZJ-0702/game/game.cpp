#include <cstdio>
#include <algorithm>
using namespace std;

typedef long long ll;
const ll MOD = 1e9 + 7;
int n, m, ans, pre, a[100][100], b[1000];
bool ok;

void dfs2(int u)
{
    if (u > n + m - 2)
    {
        int t1 = 0, t2 = 0;
        for (int i = 1; i <= n + m - 2; i++)
            if (b[i] == 0)
                t1++;
            else
                t2++;
        if (t1 != n - 1 || t2 != m - 1)
            return ;
        int x = 1, y = 1;
        int t = 0;
        for (int i = 1; i <= n + m - 2; i++)
        {
            t |= (a[x][y] << (n + m - x - y));
            if (b[i] == 0)
                x++;
            else
                y++;
        }
        if (t < pre)
            ok = 0;
        pre = t;
        return ;
    }
    b[u] = 0;
    dfs2(u + 1);
    b[u] = 1;
    dfs2(u + 1);
}

void dfs(int u, int v)
{
    if (u > n)
    {
        pre = 0;
        ok = 1;
        dfs2(1);
        //ans += ok;
        ans += ok;
        return ;
    }
    if (v > m)
    {
        dfs(u + 1, 1);
        return ;
    }
    a[u][v] = 1;
    dfs(u, v + 1);
    a[u][v] = 0;
    dfs(u, v + 1);
}

ll pw(ll a, ll b)
{
    ll re = 1;
    while (b)
    {
        if (b & 1)
            re = re * a % MOD;
        a = a * a % MOD;
        b >>= 1;
    }
    return re;
}

int main()
{
    freopen("game.in", "r", stdin);
    freopen("game.out", "w", stdout);
    scanf("%d%d", &n, &m);
    if (n <= 3 && m <= 3)
    //if (1)
        dfs(1, 1);
    else
    {
        if (n == 1)
            ans = pw(2, m);
        if (n == 2)
            ans = pw(3, m - 1) * 4 % MOD;
        if (n == 3)
            ans = pw(3, m - 3) * 112 % MOD;
    }
    printf("%d\n", ans);
}
