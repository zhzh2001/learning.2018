#include <cstdio>
#include <algorithm>
using namespace std;
typedef long long ll;

const ll INF = 1e18;
const int N = 101000;
int a[N];
ll f[N][3];
int t1, t2, t3, t4;
char ch[100];

struct Edge
{
    int nxt, to;
} eg[N << 1];
int head[N], en, n, m;

void set_edge(int u, int v)
{
    eg[++en] = (Edge) {head[u], v};
    head[u] = en;
}

void dfs(int u, int fa)
{
    f[u][0] = 0;
    f[u][1] = a[u];
    f[u][2] = 0;
    ll t = INF;
    for (int e = head[u]; e; e = eg[e].nxt)
    {
        int v = eg[e].to;
        if (v == fa) continue;
        dfs(v, u);
        f[u][0] += f[v][0];
        t = min(t, f[v][1] - f[v][0]);
        f[u][1] += min(min(f[v][0], f[v][1]), f[v][2]);
        f[u][2] += min(f[v][0], f[v][1]);
    }
    f[u][0] += t;
    if (u == t1 && t2 == 0 || u == t3 && t4 == 0)
        f[u][1] = INF;
    if (u == t1 && t2 == 1 || u == t3 && t4 == 1)
        f[u][0] = f[u][2] = INF;
}

ll T[N << 2][3][3];

void merge(ll a[][3], ll b[][3], ll c[][3])
{
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            c[i][j] = INF;
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            for (int k = 0; k < 3; k++)
                for (int l = 0; l < 3; l++)
                    if (!(j == 0 && k == 2 || j == 2 && k == 0 || j == 2 && k == 2))
                        c[i][l] = min(c[i][l], a[i][j] + b[k][l]);
}

void updata(int u, int l, int r, int p, int ty)
{
    if (l == r)
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                T[u][i][j] = INF;
        if (ty == 0)
            T[u][2][2] = 0;
        if (ty == 1)
            T[u][1][1] = a[l];
        if (ty == 2)
        {
            T[u][1][1] = a[l];
            T[u][2][2] = 0;
        }
        return ;
    }
    int mid = l + r >> 1;
    if (p <= mid)
        updata(u << 1, l, mid, p, ty);
    else
        updata(u << 1 | 1, mid + 1, r, p, ty);
    merge(T[u << 1], T[u << 1 | 1], T[u]);
}

int main()
{
    freopen("defense.in", "r", stdin);
    freopen("defense.out", "w", stdout);
    scanf("%d%d", &n, &m);
    scanf("%s", ch);
    for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
    for (int i = 1; i < n; i++)
    {
        int u, v;
        scanf("%d%d", &u, &v);
        set_edge(u, v);
        set_edge(v, u);
    }
    if (n <= 2000 && m <= 2000)
    {
        while (m--)
        {
            scanf("%d%d%d%d", &t1, &t2, &t3, &t4);
            dfs(1, 0);
            ll ans = min(f[1][0], f[1][1]);
            if (ans >= INF / 2)
                puts("-1");
            else
                printf("%lld\n", ans);
        }
    } else
    {
        while (m--)
        {
            scanf("%d%d%d%d", &t1, &t2, &t3, &t4);
            updata(1, 1, n, t1, t2);
            updata(1, 1, n, t3, t4);
            ll ans = INF;
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                    ans = min(ans, T[1][i][j]);
            if (ans >= INF / 2)
                puts("-1");
            else
                printf("%lld\n", ans);
            updata(1, 1, n, t1, 2);
            updata(1, 1, n, t3, 2);
        }
    }
}
