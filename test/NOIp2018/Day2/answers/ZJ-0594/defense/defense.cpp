#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;

const int INF = 1e9;
const int N = 100007;

char Type[20];

int n, m, p[N], col[N];
int a, x, b, y;
int ecnt, head[N], vet[N << 1], nxt[N << 1];
int f[N][2];

inline void eadd(int u, int v){
	vet[++ecnt] = v;
	nxt[ecnt] = head[u];
	head[u] = ecnt;
}

void dfs(int u, int pre){
	if (col[u] != 1) f[u][0] = 0; else f[u][0] = INF;
	if (col[u] != 0) f[u][1] = p[u]; else f[u][1] = INF;
	bool check = true;
	for (int e = head[u]; e; e = nxt[e]){
		int v = vet[e];
		if (v != pre && col[v] == 0){
			check = false;
			break;
		}
	}
	if (!check) f[u][0] = INF;
	for (int e = head[u]; e; e = nxt[e]){
		int v = vet[e];
		if (v == pre) continue;
		dfs(v, u);
		if (f[u][0] < INF) f[u][0] += f[v][1];
		if (f[u][1] < INF) f[u][1] += min(f[v][0], f[v][1]);
	}
}

int main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d%s", &n, &m, Type);
	for (int i = 1; i <= n; ++i)
		scanf("%d", &p[i]);
	for (int i = 1; i < n; ++i){
		int u, v; scanf("%d%d", &u, &v);
		eadd(u, v); eadd(v, u);
	}
	memset(col, -1, sizeof(col));
	for (int i = 1; i <= m; ++i){
		scanf("%d%d%d%d", &a, &x, &b, &y);
		if (x == 0 && y == 0){
			bool check = false;
			for (int e = head[a]; e; e = nxt[e])
				if (vet[e] == b){
					check = true;
					break;
				}
			if (check){
				printf("-1\n");
				continue;
			}
		}
		col[a] = x;
		col[b] = y;
		dfs(1, 0);
		col[a] = -1;
		col[b] = -1;
		printf("%d\n", min(f[1][1], f[1][0]));
	}
	return 0;
}
