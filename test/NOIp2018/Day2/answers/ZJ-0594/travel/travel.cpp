#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <vector>
using namespace std;

const int N = 5007;
const int M = 5007;

vector<int> vet[N];
int n, m, ans[N], num;

inline void eadd(int u, int v){
	vet[u].push_back(v);
}

namespace Case1{
	void dfs(int u, int pre){
		ans[++num] = u;
		sort(vet[u].begin(), vet[u].end());
		for (int i = 0, Lim = vet[u].size(); i < Lim; ++i){
			if (vet[u][i] != pre)
				dfs(vet[u][i], u);
		}	
	}
}
namespace Case2{
	vector<int> circle;
	void dfs(int u, int pre){
		if (pre) circle.push_back(u);
		for (int i = 0, Lim = vet[u].size(); i < Lim; ++i)
			if (vet[u][i] != pre){
				if (vet[u][i] != 1)	dfs(vet[u][i], u);
				return;
			}
	}
	void doit(){
		circle.clear();
		dfs(1, 0);
		ans[num = 1] = 1;
		int l = 0, r = circle.size()-1;
		if (circle[l] < circle[r]){
			ans[++num] = circle[l];	++l;
			while (l <= r && circle[l] < circle[r])
				ans[++num] = circle[l++];
			while (l <= r) 
				ans[++num] = circle[r--];
		}else{
			ans[++num] = circle[r]; --r;
			while (l <= r && circle[l] > circle[r])
				ans[++num] = circle[r--];
			while (l <= r)
				ans[++num] = circle[r--];
		}
	}
}
int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; ++i){
		int u, v; scanf("%d%d", &u, &v);
		eadd(u, v); eadd(v, u);
	}
	if (m == n-1){
		Case1::dfs(1, 0);
		for (int i = 1; i < n; ++i)
			printf("%d ", ans[i]);
		printf("%d\n", ans[n]);
	} else{
		Case2::doit();
		for (int i = 1; i < n; ++i)
			printf("%d ", ans[i]);
		printf("%d\n", ans[n]);
	}
	return 0;
}
