#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
typedef long long ll;

const int p = 1e9 + 7;

int n, m, ans, tot, state[1000007], mat[20][20];
bool check;

inline int get(int sta, int pos){
	return (sta >> pos & 1);
}

inline int id(int x, int y){
	return (x-1) * m + y;
}

void dfs(int x, int y, int sta){
	if (x == n && y == m){
		for (int i = 1; i <= tot; ++i)
			if (state[i] > sta) check = false;
		state[++tot] = sta;
	}
	if (y < m) dfs(x, y + 1, sta | mat[x][y+1] << (n + m - 2 - (x + y - 2) - 1));
	if (x < n) dfs(x + 1, y, sta | mat[x+1][y] << (n + m - 2 - (x + y - 2) - 1));
}

int Qpow(int a, int k){
	int t = a, res = 1;
	while (k > 0){
		if (k % 2) res = (ll)res * t % p;
		t = (ll)t * t % p;
		k = k / 2;
	}
	return res;
}

int main(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n == 2){
		printf("%d\n", (ll)Qpow(3, m-1) * 4 % p);
		return 0;
	}
	for (int sta = 0; sta < (1 << (n * m)); ++sta){
		for (int i = 1; i <= n; ++i)
			for (int j = 1; j <= m; ++j)
				mat[i][j] = get(sta, id(i, j) - 1);
		
		tot = 0; check = true;
		dfs(1,1,0);
		if (check){
			++ans;
		}
	}
	printf("%d\n", ans);
	return 0;
}
