#include<bits/stdc++.h>
#define ll long long
using namespace std;
const ll mod=1e9+7;
ll n,m,cnt,pow2[1000010],a[1000010][2],opt,Ans;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	pow2[1]=1;
	for(int i=2;i<=1000000;i++)pow2[i]=pow2[i-1]*2ll%mod;
	scanf("%lld %lld",&n,&m);
	cnt=n+m-2;
	opt=1;
	a[1][1]=1;
	for(int i=2;i<=cnt;i++){
		ll ans=0;
		for(int j=2;j<i;j++)a[j][opt^1]=a[j-1][opt]+a[j][opt];
		opt^=1;
		a[1][opt]=1;a[i][opt]=1;
//		for(int j=1;j<=i;j++)cout<<a[j][opt]<<" ";
//		cout<<endl;
		for(int j=1;j<=i;j++)ans=(ans+a[j][opt]*pow2[j])%mod;
		Ans=ans;
//		cout<<endl;
//		cout<<ans<<endl;
//		cout<<endl;
	}
	cout<<(Ans+(cnt-2)/2)*4<<endl;
	return 0;
}
