#include<bits/stdc++.h>
using namespace std;
int m,n,x,y,ans[5010],cnt,nums[5010],tim;
vector<int>edge[5010];
bool vis[5010],Ans[5010];
int st[5010],s;
priority_queue<int>q;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d %d",&n,&m);
	for(int i=1;i<=n;i++)vis[i]=0;
	for(int i=1;i<=m;i++){
		scanf("%d %d",&x,&y);
		edge[x].push_back(y);
		edge[y].push_back(x);
	}
	edge[0].push_back(999999);
	for(int i=1;i<=n;i++)sort(edge[i].begin(),edge[i].end());
	if(m==n-1){
		st[++s]=1;vis[1]=1;ans[++cnt]=1;tim++;
		while(s){
			int x=st[s],numss=0;
			for(int i=nums[x];i<edge[x].size();i++){
				int v=edge[x][i];nums[x]=i;
				if(v==st[s-1])continue;
				if(vis[v])continue;
				else{
					ans[++cnt]=v;tim++;vis[v]=1;
					numss=v;nums[x]=i;break;
				}
			}
			if(nums[x]==edge[x].size()-1)s--;
			if(numss)st[++s]=numss;
		}
	}
	else{
		q.push(-1);vis[1]=1;
		while(q.size()){
			int x=q.top();q.pop();
			ans[++cnt]=-x;x*=-1;
			for(int i=0;i<edge[x].size();i++){
				if(!vis[edge[x][i]]){
					q.push(-edge[x][i]);
					vis[edge[x][i]]=1;
				}
			}
		}
	}
	printf("%d",ans[1]);
	for(int i=2;i<=n;i++)printf(" %d",ans[i]);
	return 0;
}
