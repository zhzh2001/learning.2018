#include<cstdio>
#include<iostream>
#include<cctype>
#include<cstring>
#include<map>
#define ll long long
#define inf 9223372036854775807>>1
using namespace std;

inline void kai(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
}

inline int read(){
	int ans=0,f=1;char chr=getchar();
	while(!isdigit(chr)){if(chr=='-') f=-1;chr=getchar();}
	while(isdigit(chr)) {ans=(ans<<3)+(ans<<1)+chr-48;chr=getchar();}
	return ans*f;
}
int n,m;
const int M=100005<<2;
int head[M],nxt[M],ver[M],tot,ans[M],vis[M],du[M],aa,bb;
inline void add(int x,int y){ver[++tot]=y;nxt[tot]=head[x];head[x]=tot;}
ll f[100005][2];
ll a[100005];
string opt;
map<int,int> ma[100005];
int xx,yy;
inline void swap(int &x,int &y){
	int t=x;
	x=y;
	y=t;
}
void dfs(int x,int fa){
	if(x==xx){
		if(aa==1)
			f[x][0]=inf;
		if(aa==0)
			f[x][1]=inf;
	}
	if(x==yy){
		if(bb==1)
			f[x][0]=inf;
		if(bb==0)
			f[x][1]=inf;
	}
	if(x!=1&&du[x]==1){
		f[x][0]+=0;
		f[x][1]+=a[x];
		return;
	}
	f[x][0]+=0;
	f[x][1]+=a[x];
	for(int i=head[x];i;i=nxt[i]){
		int y=ver[i];
		if(y==fa) continue;
		dfs(y,x);
		f[x][0]+=f[y][1];
		f[x][1]+=min(f[y][1],f[y][0]);
	}
}

inline void solve1(){
	dfs(1,0);
	while(m--){
		xx=read(),aa=read(),yy=read(),bb=read();
		if((aa==bb&&aa==0)&&ma[xx][yy]){
			puts("-1");
			continue;
		}else{
			if(xx>yy) swap(xx,yy),swap(aa,bb);
			ll k1,k2,k3;
			ll ans=aa*a[xx]+bb*a[yy];
			k1=k2=k3=0;
			if(aa==1&&bb==0){
				k1=min(f[xx-1][1],f[xx-1][0]);
				k2=f[yy-1][1]-max(f[xx+1][0],f[xx+1][1]);
				k3=min(f[n][0],f[n][1])-f[yy+1][1];
			}else if(aa==0&&bb==1){
				k1=f[xx-1][1];
				k2=min(f[yy-1][1],f[yy-1][0])-f[xx+1][1];
				k3=min(f[n][0],f[n][1])-max(f[yy+1][0],f[yy+1][1]);
			}else if(aa==bb&&bb==1){
				k1=min(f[xx-1][1],f[xx-1][0]);
				k2=min(f[yy-1][1],f[yy-1][0])-max(f[xx+1][0],f[xx+1][1]);
				k3=min(f[n][0],f[n][1])-max(f[yy+1][1],f[yy+1][0]);
			}else{
				k1=f[xx-1][1];
				k2=f[yy-1][1]-f[xx+1][1];
				k3=min(f[n][0],f[n][1])-f[yy+1][1];
			}
			ans+=k1+k2+k3;
			printf("%lld",ans);cout<<endl;
		}
		
	}
}

int main(){
	kai();
	n=read(),m=read();cin>>opt;
	for(int i=1;i<=n;i++){
		int x=read();
		a[i]=x*1ll;
	}
	for(int i=1;i<=n-1;i++){
		int x=read(),y=read();
		du[x]++,du[y]++;
		add(x,y),add(y,x);
		ma[x][y]=ma[y][x]=1;
	}
	if(opt[0]=='A'&&m>=100000){
		solve1();
		return 0;
	}
	while(m--){
		xx=read(),aa=read(),yy=read(),bb=read();
		if((aa==bb&&aa==0)&&ma[xx][yy]){
			printf("-1");cout<<endl;
			continue;
		}else{memset(f,0,sizeof(f));
			dfs(1,0);
			printf("%lld",min(f[1][0],f[1][1]));cout<<endl;
		}
	}
	return 0;
}
