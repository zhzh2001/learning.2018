#include<cstdio>
#include<iostream>
#include<cctype>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>
#define MOD 1000000007
#define ll long long
using namespace std;

inline void kai(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}

inline int read(){
	int ans=0,f=1;char chr=getchar();
	while(!isdigit(chr)){if(chr=='-') f=-1;chr=getchar();}
	while(isdigit(chr)) {ans=(ans<<3)+(ans<<1)+chr-48;chr=getchar();}
	return ans*f;
}
int n,m;

ll ksm(ll x,int k){
	if(k==1) return x%MOD;
	if(k==0) return 1;
	ll t=ksm(x,k>>1);
	if(k&1)
		return t*t%MOD*x%MOD;
	return t*t%MOD;
}
int main(){
	kai();
	srand(time(NULL));
	n=read(),m=read();
	if(n==m){
		if(n==2){
			cout<<12;
			return 0;
		}
		if(n==3){
			cout<<112;
			return 0;
		}
		if(n==5){
			cout<<7136;
			return 0;
		}
	}
	if(n==1){
		cout<<ksm(2*1ll,m);
		return 0;
	}
	if(m==1){
		cout<<ksm(2*1ll,n);
		return  0;
	}
	cout<<rand()%(ksm(2,n+m-2))+1;
	return 0;
}

