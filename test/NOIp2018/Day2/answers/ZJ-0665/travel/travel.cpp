#include<cstdio>
#include<iostream>
#include<cctype>
#include<cstring>
#include<set>
#include<map>
#include<algorithm>
using namespace std;
inline void kai(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
}
inline int read(){
	int ans=0,f=1;char chr=getchar();
	while(!isdigit(chr)){if(chr=='-') f=-1;chr=getchar();}
	while(isdigit(chr)) {ans=(ans<<3)+(ans<<1)+chr-48;chr=getchar();}
	return ans*f;
}
const int M=5005<<1;
int n,m;
int head[M],nxt[M],ver[M],tot,ans[M],vis[M];
inline void add(int x,int y){ver[++tot]=y;nxt[tot]=head[x];head[x]=tot;}
int cnt=0;
int fx,fy;
int fa[M];
int tux[M],tuy[M];
int fi(int x){
	if(fa[x]==x) return x;
	return fa[x]=fi(fa[x]);
}
int mp[M];
void dfs(int x,int fa){
//	cout<<x<<endl;
	int p[5005];
	mp[++cnt]=x;
	vis[x]=1;
	int k=0;
	for(int i=head[x];i;i=nxt[i])
			if(ver[i]!=fa&&(!(fx==x&&fy==ver[i]||fx==ver[i]&&fy==x)))p[++k]=ver[i];
	sort(p+1,p+k+1);
	for(int i=1;i<=k;i++)
		dfs(p[i],x);
	return;
}
void dfs1(int x,int fa){
	int p[5005];
	ans[++cnt]=x;
	vis[x]=1;
	int k=0;
	for(int i=head[x];i;i=nxt[i])
			if(ver[i]!=fa)p[++k]=ver[i];
	sort(p+1,p+k+1);
	for(int i=1;i<=k;i++)
		dfs1(p[i],x);
	return;
}
inline void solve1(){
	vis[0]=1;
	memset(vis,0,sizeof(vis));
	dfs1(1,0);
	for(int i=1;i<=n;i++)
		printf("%d ",ans[i]);
}
string s;
set<string> ss;
inline void solve2(){
	memset(ans,0x3f,sizeof(ans));
	for(int i=1;i<=n-1;i++){cnt=0;
		for(int j=1;j<=n;j++) fa[j]=j;
		for(int j=1;j<=m;j++){
			if(j==i) continue;
			int aa=fi(tux[j]),bb=fi(tuy[j]);
			if(aa!=bb) fa[aa]=bb;
		}
		fx=tux[i],fy=tuy[i];
		int kkk=0;
		for(int j=1;j<=n;j++) if(fa[j]==j) kkk++;
		if(kkk>1)  continue;
		dfs(1,0);
		int ff=0;
		for(int j=1;j<=n;j++){
			if(mp[j]<ans[j]) {ff=1;break;}
			if(mp[j]>ans[j]) break;
		}			
		if(ff){
			for(int j=1;j<=n;j++)
				ans[j]=mp[j];
		}
	}
	for(int i=1;i<=n;i++){
		printf("%d ",ans[i]);
	}
}
int main(){
	kai();
	n=read();m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		tux[i]=x,tuy[i]=y;
		add(x,y),add(y,x);
	}
	if(m==n-1){
		solve1();
		return 0;
	}else
		solve2();
	return 0;
}

