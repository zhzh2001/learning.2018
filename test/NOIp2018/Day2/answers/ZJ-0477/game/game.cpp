#include<cstdio>
#include<cstring>
#include<algorithm>
#define LL long long
using namespace std;
const int maxn=15,tt=1000000007;
int n,m;
inline char nc()
{
	static char buf[100000],*l,*r;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF; return *l++;
}
inline int Read()
{
	int res=0,f=1; char ch=nc(),cc=ch;
	while (ch<'0'||ch>'9') cc=ch,ch=nc();
	if (cc=='-') f=-1;
	while (ch>='0'&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}
int Qsm(int x,int b)
{
	int sum=1,w=x;
	for (; b>0; b>>=1,w=(LL)w*w%tt)
	 if (b&1) sum=(LL)sum*w%tt;
	return sum;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=Read(); m=Read();
	if (n>m) swap(n,m);
	if (n==1) printf("%d\n",Qsm(2,n));
	if (n==2) printf("%d\n",(LL)4*Qsm(3,m-1)%tt);
	if (n==3) printf("%d\n",(LL)112*Qsm(3,m-3)%tt);
	if (n==4)
	 {
	 	if (m==4) printf("912\n");
	 	if (m>=5) printf("%d\n",(LL)2688*Qsm(3,m-5)%tt);
	 }
	if (n==5)
	 {
	 	if (m==5) printf("7136\n");
	 }
	return 0;
}
