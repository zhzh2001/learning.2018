#include<cstdio>
#include<cstring>
#include<algorithm>
#define LL long long
using namespace std;
const int maxn=100005,maxm=200005;
const LL INF=(LL)1<<60;
int n,m,T,tot,a[maxn],nxt[maxm],son[maxm],lnk[maxn];
LL f[maxn][2];
bool vis[maxn][2];
inline char nc()
{
	static char buf[100000],*l,*r;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF; return *l++;
}
inline int Read()
{
	int res=0,f=1; char ch=nc(),cc=ch;
	while (ch<'0'||ch>'9') cc=ch,ch=nc();
	if (cc=='-') f=-1;
	while (ch>='0'&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}
void Add(int x,int y) {son[++tot]=y; nxt[tot]=lnk[x]; lnk[x]=tot;}
void Dfs(int x,int Fa)
{
	bool pd=0; LL t=0,s=0;
	for (int j=lnk[x]; j; j=nxt[j])
	 if (son[j]!=Fa)
	  {
	  	pd=1;
	  	Dfs(son[j],x);
	  	t+=f[son[j]][1]; s+=min(f[son[j]][0],f[son[j]][1]);
	  }
	if (!pd)
	 {
	 	if (!vis[x][1]) f[x][0]=0;
	 	if (!vis[x][0]) f[x][1]=a[x];
	 }
	 else
	  {
		if (!vis[x][0]) f[x][1]=s+a[x];
		if (!vis[x][1]) f[x][0]=t;
	  }
}
void Work()
{
	for (int i=1; i<=n; i++) f[i][0]=f[i][1]=INF;
	f[0][0]=f[0][1]=0;
	for (int i=1; i<=n; i++)
	 f[i][0]=f[i-1][1],
	  f[i][1]=min(f[i-1][1],f[i-1][0])+a[i];
	for (int i=1; i<=m; i++)
	{
		int x=Read(),p1=Read(),y=Read(),p2=Read();
		int t1,t2;
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=Read(); m=Read(); T=Read();
	for (int i=1; i<=n; i++) a[i]=Read();
	if (n>5000) {Work(); return 0;}
	for (int i=1,x,y; i<n; i++) x=Read(),y=Read(),Add(x,y),Add(y,x);
	for (int i=1; i<=m; i++)
	{
		for (int j=1; j<=n; j++) f[j][0]=f[j][1]=INF;
		int x=Read(),p1=Read(),y=Read(),p2=Read();
		vis[x][p1]=vis[y][p2]=1;
		Dfs(1,0);
		LL ans=min(f[1][0],f[1][1]);
		if (ans>=INF) printf("-1\n"); else printf("%lld\n",ans);
		vis[x][p1]=vis[y][p2]=0;
	}
	return 0; 
}
