#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=5005;
int n,m,T,p[maxn],ans[maxn];
bool f[maxn][maxn],vis[maxn];
struct Edge
{
	int x,y;
}a[maxn];
inline char nc()
{
	static char buf[100000],*l,*r;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF; return *l++;
}
inline int Read()
{
	int res=0,f=1; char ch=nc(),cc=ch;
	while (ch<'0'||ch>'9') cc=ch,ch=nc();
	if (cc=='-') f=-1;
	while (ch>='0'&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}
void Dfs(int x)
{
	vis[x]=1; printf("%d",x); T++; if (T!=n) putchar(32);
	for (int i=1; i<=n; i++)
	 if (!vis[i]&&f[x][i]) Dfs(i);
}
void Dfs2(int x)
{
	vis[x]=1; p[++T]=x;
	if (T==n)
	 {
	 	for (int i=1; i<=n; i++)
	 	 if (p[i]>ans[i]) return;
	 	  else if (p[i]<ans[i]) break;
	 	for (int i=1; i<=n; i++) ans[i]=p[i];
	 	return;
	 }
	for (int i=1; i<=n; i++)
	 if (!vis[i]&&f[x][i]) Dfs2(i);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=Read(); m=Read(); T=0;
	for (int i=1,x,y; i<=m; i++) x=Read(),y=Read(),a[i]=(Edge){x,y},f[x][y]=f[y][x]=1;
	if (m==n-1) Dfs(1);
	 else
	  {
	  	memset(ans,63,sizeof(ans));
	  	for (int i=1; i<=m; i++)
	  	{
	  		int x=a[i].x,y=a[i].y;
	  		f[x][y]=f[y][x]=0;
	  		for (int j=1; j<=n; j++) vis[j]=0;
	  		T=0; Dfs2(1);
	  		f[x][y]=f[y][x]=1;
		}
		for (int i=1; i<n; i++) printf("%d ",ans[i]); printf("%d",ans[n]);
	  }
	return 0;
}
