#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<=b;++i)
#define rpd(i,a,b) for(int i=a;i>=b;--i)
#define ll long long
using namespace std;

const int N=1000000+5;
const ll mod=1000000007LL;

int n,m;
ll pw(ll a,ll k){
	ll t=1LL;
	while(k){
		if(k&1)t*=a,t%=mod;
		a*=a;a%=mod;
		k>>=1;
	}
	return t;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
    scanf("%d%d",&n,&m);   
    if(n==3){
    	if(m==1){puts("8");return 0;}
    	if(m==2){puts("36");return 0;}
    	if(m==3){puts("112");return 0;}
	}
	if(m<n)swap(m,n);
	if(n==1){
		ll ans=pw(2LL,1LL*m);
		printf("%lld\n",ans);
		return 0;
	}
    if(n==2){
    	ll ans=pw(3LL,1LL*(m-1));
    	ans*=4LL;ans%=mod;
    	printf("%lld\n",ans);
    	return 0;
	}
	ll ans=pw(1LL*n,1LL*m);
	printf("%lld\n",ans);
	return 0;
}
