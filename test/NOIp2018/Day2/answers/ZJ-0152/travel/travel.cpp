#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<=b;++i)
#define rpd(i,a,b) for(int i=a;i>=b;--i)
using namespace std;

const int N=5000+5;

int n,m,tot;int s[N],f[N];bool g[N];
int st,ed,eds;
priority_queue<int,vector<int>,greater<int> >a[N],b[N];
void dfs(int x){
	s[++tot]=x;g[x]=1;
	while(!a[x].empty()){
		int k=a[x].top();a[x].pop();
		if(g[k])continue;
		dfs(k);
	}
}
void get_f(int x,int fa){
	f[x]=fa;
	while(!b[x].empty()){
		int k=b[x].top();b[x].pop();
		if(k==fa)continue;
		if(f[k]){ed=k;st=x;continue;}
		get_f(k,x);
	}
}
void dfs2(int x){
	if(g[x])return;s[++tot]=x;g[x]=1;
	while(!a[x].empty()){
		int k=a[x].top();a[x].pop();
		dfs2(k);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	rep(i,1,m){
		int u,v;scanf("%d%d",&u,&v);
		a[u].push(v);a[v].push(u);
		b[u].push(v);b[v].push(u);
	}
	rep(i,0,n)g[i]=0;
	if(m==n-1)dfs(1);
	if(m==n){
		rep(i,0,n)f[i]=0;
		get_f(1,0);eds=ed;
		while(ed!=st){
			if(ed>eds)a[f[ed]].push(eds);
		    ed=f[ed];
		}
		dfs2(1);
	}
	rep(i,1,n)printf("%d ",s[i]);
	return 0;
}
