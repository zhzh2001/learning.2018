#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#define N 100005
using namespace std;
int n,m,p[N],Head[N],tot,Next[N<<1],To[N<<1],a,b,x,y,vis[N],h[N],c[N];
long long f[N][2],ans;
char s[102];
void add(int x,int y){
	tot++;
	Next[tot]=Head[x];
	To[tot]=y;
	Head[x]=tot;
}
void dfs(int x,long long su){
	if (x>n){
		for (int i=1;i<=n;i++){
			for (int j=Head[i];j;j=Next[j]){
				if (h[i]==h[To[j]]&&h[i]==0) return;
			}
		}
		ans=min(ans,su);
		return;
	}
	if (vis[x]){
		dfs(x+1,su);
		return;
	}
	dfs(x+1,su);
	h[x]=1;
	dfs(x+1,su+p[x]);
	h[x]=0; 
}
void df(int x,int fa){
	if (vis[x]){
		if (h[x]==0) f[x][1]=1e12,f[x][0]=0;
		if (h[x]==1) f[x][0]=1e12,f[x][1]=p[x];
	}
	else{
		f[x][0]=0;
		f[x][1]=p[x];
	}
	int tot=0;
	for (int i=Head[x];i;i=Next[i]){
		int y=To[i];
		if (y!=fa){
			df(y,x);
			if (f[x][1]!=1e12) f[x][1]+=min(f[y][1],f[y][0]);
			c[++tot]=y;
		}
	}
	long long m=1e12,k=0;;
	for (int i=1;i<=tot;i++)
		if (f[c[i]][1]<m){
			m=f[c[i]][1];
			k=i;
		}
	if (f[x][0]!=1e12){
		f[x][0]=m;
		for (int i=1;i<=tot;i++)
			if (i!=k) f[x][0]+=min(f[c[i]][1],f[c[i]][0]);
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s+1);
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++){
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	while (m--){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if (x==0&&y==0){
			int t=1;
			for (int i=Head[a];i;i=Next[i]){
				if (b==To[i]){
					t=0;
					break;
				}
			}
			if (t==0){
				printf("-1\n");
				continue;
			}
		}
		
		for (int i=1;i<=n;i++) h[i]=0;
		h[a]=x; h[b]=y;
		vis[a]=vis[b]=1;
		ans=1e13;
		if (n<100) dfs(1,0); else{
			for (x=1;x<=n;x++){
				if (vis[x]==1){
					if (h[x]==0) f[x][1]=1e12,f[x][0]=0;
					if (h[x]==1) f[x][0]=1e12,f[x][1]=p[x];
				}else{
					f[x][0]=0;
					f[x][1]=p[x];
				}
				if (f[x][1]!=1e12) f[x][1]+=min(f[x-1][1],f[x-1][0]);
				if (f[x][0]!=1e12) f[x][0]+=f[x-1][1];
			}
			printf("%lld\n",min(f[n][1],f[n][0]));
			vis[a]=vis[b]=0;
			continue;
		}
		vis[a]=vis[b]=0;
		if (n<100) printf("%lld\n",ans+p[a]*x+p[b]*y); else printf("%lld\n",min(f[1][0],f[1][1]));
	}
	return 0;
}
