#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define N 5005
using namespace std;
int n,m,x[N],y[N],dfn[N],cnt,p[N],a[N][N],ans[N];
void dfs(int x){
	dfn[++cnt]=x;
	for (int i=1;i<=a[x][0];i++)
		if (p[a[x][i]]==0&&a[x][i]!=0){
			p[a[x][i]]=1;
			dfs(a[x][i]);
		}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d",&x[i],&y[i]);
		a[x[i]][0]++;
		a[x[i]][a[x[i]][0]]=y[i];
		a[y[i]][0]++;
		a[y[i]][a[y[i]][0]]=x[i];
	}
	for (int i=1;i<=n;i++){
		sort(a[i]+1,a[i]+1+a[i][0]);
	}
	if (n-1==m){
		p[1]=1;
		dfs(1);
		printf("%d",dfn[1]);
		for (int i=2;i<=n;i++) printf(" %d",dfn[i]);
		printf("\n");
		return 0;
	}
	if (n==m){
		for (int i=1;i<=n;i++) ans[i]=1e9;
		for (int i=1;i<=m;i++){
			for (int j=1;j<=a[x[i]][0];j++)
				if (a[x[i]][j]==y[i]){
					a[x[i]][j]=0;
					break;
				}
			for (int j=1;j<=a[y[i]][0];j++)
				if (a[y[i]][j]==x[i]){
					a[y[i]][j]=0;
					break;
				}
			for (int j=1;j<=n;j++) p[j]=0;
			p[1]=1;
			cnt=0;
			dfs(1);
			if (cnt==n){
				int t=0;
				for (int j=1;j<=n;j++)
					if (dfn[j]<ans[j]){
						t=1;
						break;
					} else if (dfn[j]>ans[j]) break;
				if (t){
					for (int j=1;j<=n;j++) ans[j]=dfn[j];
				}
			}
			for (int j=1;j<=a[x[i]][0];j++)
				if (a[x[i]][j]==0){
					a[x[i]][j]=y[i];
					break;
				}
			for (int j=1;j<=a[y[i]][0];j++)
				if (a[y[i]][j]==0){
					a[y[i]][j]=x[i];
					break;
				}
		}
		printf("%d",ans[1]);
		for (int i=2;i<=n;i++) printf(" %d",ans[i]);
		printf("\n");
		return 0;
	}
	return 0;
}
