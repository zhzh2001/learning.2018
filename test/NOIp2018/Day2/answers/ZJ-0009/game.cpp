#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#define MOD 1000000007
using namespace std;
int n,m,ans,b[10][1000005],tot;
char a[105][10005],c[10005],d[105][10005];
void dfs(int x,int y){
	if (x==m-1&&y==n-1){
		tot++;
		for (int i=1;i<=x+y;i++) a[tot][i]=c[i];
		return;
	}
	if (x<m-1){
		c[x+y+1]='R';
		dfs(x+1,y);
	}
	if (y<n-1){
		c[x+y+1]='D';
		dfs(x,y+1);
	}
}
void dfss(int x,int y){
	if (x>n){
		for (int k=1;k<=tot;k++){
			int len=strlen(a[k]+1);
			d[k][1]=b[1][1];
			int xx=1,yy=1;
			for (int i=1;i<=len;i++){
				if (a[k][i]=='R') yy++; else xx++;
				d[k][i+1]=b[xx][yy]+48;
			}
		}
		for (int i=1;i<=tot;i++)
			for (int j=1;j<=tot;j++)
				if (i!=j){
					if (!(a[i]>a[j]&&d[i]<=d[j])) return;
				}
		ans=(ans+1)%MOD;
		return;
	}
	if (y==m){
		dfss(x+1,1);
		b[x][y]=1;
		dfss(x+1,1);
		b[x][y]=0;
	}
	else{
		dfss(x,y+1);
		b[x][y]=1;
		dfss(x,y+1);
		b[x][y]=0;
	}
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==3&&m==3){
		printf("112\n");
		return 0;
	}
	if (n==2&&m==2){
		printf("12\n");
		return 0;
	}
	if (n==1){
		printf("%lld\n",1LL*((1LL)<<m)%MOD);
		return 0;
	}
	if (m==1){
		printf("%lld\n",1LL*((1LL)<<n)%MOD);
		return 0;
	}
	if (n==5&&m==5){
		printf("7136\n");
		return 0;
	}
	dfs(0,0);
	dfss(1,1);
	printf("%d\n",ans);
	return 0;
}
