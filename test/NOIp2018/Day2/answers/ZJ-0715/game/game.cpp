#include <bits/stdc++.h>
#define LL long long 
using namespace std;

  const LL mo=1e9+7;

  int n,m,ans,a[233][233];
  
  int val(int x,int y){
  	return(x<=n&&y<=m);
  }

  LL qpow(LL bas,int powe){
  	LL ret=1;
  	for (;powe;bas*=bas,bas%=mo){
  	  if (powe&1) ret*=bas,ret%=mo;
	  powe>>=1;	
	}
	return(ret);
  }

  void dfs(int po){
  	if (po==n+m){
  	  ans++;
	  return;	
	}
	
	for (int i=0;i<=min(po,n+m-po);i++){
	  int x,y;
	  if (po<=n) x=po,y=1;else x=n,y=po-n+1;
	  for (int k=1;k<=i;k++){
	  	a[x][y]=1;
	  	x--;y++;
	  }
	  for (int k=i+1;k<=min(po,n+m-po);k++){
	  	a[x][y]=0;
	  	x--;y++;
	  }
	  
	  int tpo=po-2,fl=1;
	  if (tpo<=n) x=tpo,y=1;else x=n,y=tpo-n+1;
	  for (int k=1;k<min(tpo,n+m-tpo);k++){
	  	if (a[x][y]==a[x-1][y+1]&&val(x+1,y+1)&&val(x,y+2)&&a[x+1][y+1]!=a[x][y+2])
	  	  fl=0;
	  	x--;y++;
	  }
	  if (fl)
	    dfs(po+1);
	}
  }

  int main(){
  	freopen("game.in","r",stdin);
  	freopen("game.out","w",stdout);
  	
  	scanf("%d%d",&n,&m);
  	if (n==1||m==1){
  	  printf("%lld\n",qpow(2,m)); 	
  	  return(0);
	}
	
	if (n<=8&&m<=8){
	  dfs(1);	
	  printf("%d\n",ans);
	}else
	if (n==2){
	  printf("%lld\n",qpow(3,m-1)*4%mo);	
	}
  }
  
  
