#include <bits/stdc++.h>
#define LL long long 
using namespace std;

  LL dp[100001][2];
  int nd[200001],des[200001],nxt[200001],que1,que2,que3,que4;
  int n,q,a[200001],cnt;

  void addedge(int x,int y){
  	nxt[++cnt]=nd[x];des[cnt]=y;nd[x]=cnt;
  }

  void dfs(int po,int fa=-1){
  	dp[po][0]=0;dp[po][1]=a[po];
  	for (int p=nd[po];p!=-1;p=nxt[p])
  	  if (des[p]!=fa){
  	    dfs(des[p],po);
  	    dp[po][0]+=dp[des[p]][1];
  	    dp[po][1]+=min(dp[des[p]][0],dp[des[p]][1]);
	  }
	if (po==que1) dp[po][1-que2]=1e17; 
	if (po==que3) dp[po][1-que4]=1e17;
  }

  int main(){
  	freopen("defense.in","r",stdin);
  	freopen("defense.out","w",stdout);
  	
  	scanf("%d%d%*s",&n,&q);
  	for (int i=1;i<=n;i++) scanf("%d",&a[i]),nd[i]=-1;
  	for (int i=1;i<n;i++){
  	  int t1,t2;
  	  scanf("%d%d",&t1,&t2);
      addedge(t1,t2);addedge(t2,t1);	
	}
	
	for (int i=1;i<=q;i++){
	  scanf("%d%d%d%d",&que1,&que2,&que3,&que4);
	  dfs(1);	
	  LL ans=min(dp[1][0],dp[1][1]);
	  if (ans>=1e17) printf("-1\n");else printf("%lld\n",ans);
	}
  }

