#include <bits/stdc++.h>
using namespace std;

  set <int> mp[10001];
  int lis[10001],cnt,bt[10001],n,m,ans[10001],sid[10001][2];

  void dfs(int po){
  	lis[++cnt]=po;bt[po]=1;
  	for (set <int> :: iterator it=mp[po].begin();it!=mp[po].end();it++)
  	  if (!bt[*it])
  	    dfs(*it);
  }

  void upd(){
  	if (cnt!=n) return;
  	int fl=0;
  	for (int i=1;i<=n;i++){
  	  if (lis[i]<ans[i]) {fl=1;break;}
  	  if (lis[i]>ans[i]) break;
    }
    if (fl)
      for (int i=1;i<=n;i++)
        ans[i]=lis[i];
  }

  int main(){
  	freopen("travel.in","r",stdin);
  	freopen("travel.out","w",stdout);
  	
  	scanf("%d%d",&n,&m);
  	for (int i=1;i<=m;i++){
  	  scanf("%d%d",&sid[i][0],&sid[i][1]);	
  	  mp[sid[i][0]].insert(sid[i][1]);
  	  mp[sid[i][1]].insert(sid[i][0]);
	}
	
  	if (m==n-1){
  	  for (int i=1;i<=n;i++) bt[i]=0;
  	  cnt=0;dfs(1);
	  for (int i=1;i<n;i++)		
	    printf("%d ",lis[i]);
	  printf("%d\n",lis[n]);
	}else{
	  for (int i=1;i<=n;i++) ans[i]=1e9;	
	  for (int i=1;i<=m;i++){
	  	mp[sid[i][0]].erase(sid[i][1]);
	  	mp[sid[i][1]].erase(sid[i][0]);
	  	for (int j=1;j<=n;j++) bt[j]=0;
	  	cnt=0;dfs(1);
	  	upd();
	  	mp[sid[i][0]].insert(sid[i][1]);
  	    mp[sid[i][1]].insert(sid[i][0]);
	  }
	  for (int i=1;i<n;i++)		
	    printf("%d ",ans[i]);	  
	  printf("%d\n",ans[n]);
	}
  }
