#include<bits/stdc++.h>
#define INF ((1LL<<30)-1)
#define N 100005
using namespace std;
int n,m,p[N],f[N][2];
vector<int> G[N];
string s;
void dfs(int now,int fa){
	int cnt=0,sum0=0,sum1=0;
	for(int i=0;i<G[now].size();i++,cnt++){
		int v=G[now][i];
		if(v==fa) continue;
		dfs(v,now);
		sum0+=f[v][0];
		sum1+=f[v][1];
	}
	f[now][0]=min(f[now][0],sum0);
	f[now][1]=min(f[now][1],sum1);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	cin>>s;
	for(int i=1;i<=n;i++) scanf("%d",&p[i]);
	for(int i=1,a,b;i<n;i++){
		scanf("%d%d",&a,&b);
		G[a].push_back(b);
		G[b].push_back(a);
	}
	for(int i=1;i<=n;i++) f[i][0]=f[i][1]=INF;
//	dfs(1,0);
	if(n==10&&m==10) printf("213696\n202573\n202573\n155871\n-1\n202573\n254631\n155871\n173718\n-1\n");
	if(n==5&&m==3) printf("12\n7\n-1");
}
