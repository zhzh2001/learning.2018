#include<bits/stdc++.h>
#define N 5005
using namespace std;
int n,m,flg,dep[N];
int vis[N],ord[N],tim;
int head[N],to[N<<1],nxt[N<<1],cnt;
inline void add_edge(int a,int b){
	nxt[++cnt]=head[a];
	to[cnt]=b;
	head[a]=cnt;
}
void dfs(int now){
	for(int i=head[now];i;i=nxt[i])
		if(dep[to[i]]>dep[now]+1)
			dep[to[i]]=dep[now]+1,dfs(to[i]);
}
void dfs1(int now,int f){
	set<int> s;
	set<int>::iterator it;
	for(int i=head[now];i;i=nxt[i])
		if(to[i]!=f) s.insert(to[i]);
	for(it=s.begin();it!=s.end();it++) ord[++tim]=*it,dfs1(*it,now);
}
void dfs2(int now,int f){
	set<int> s;
	set<int>::iterator it;
	for(int i=head[now];i;i=nxt[i])
		if(!vis[to[i]]&&dep[to[i]]>dep[now]&&to[i]!=f) s.insert(to[i]);
	for(it=s.begin();it!=s.end();it++){
		vis[*it]=1;
		ord[++tim]=*it;
		dfs2(*it,now);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1,a,b;i<=m;i++){
		scanf("%d%d",&a,&b);
		add_edge(a,b);
		add_edge(b,a);
	}
	dep[1]=1;for(int i=2;i<=n;i++) dep[i]=n+1;
	dfs(1);
	vis[1]=1;
	ord[++tim]=1;
	if(m==n-1) dfs1(1,0);
	else dfs2(1,0);
	for(int i=1;i<=n;i++) printf("%d ",ord[i]);
}
