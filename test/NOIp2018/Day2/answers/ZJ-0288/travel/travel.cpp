#include <cstdio>
#include <algorithm>
using namespace std;
#define N 100001
struct node
{
	int x,y;
}a[N];
int f1,f2;
int ans[N];
int tot,cnt;
int top,sec,ord,dep;
int fir[N],nex[N],got[N];
int dfn[N],low[N],ins[N],color[N],s[N];
inline int read()
{
	int x=0;
	char ch=getchar();
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
inline void write(int x)
{
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline int cmp(node x,node y)
{
	if (x.x==y.x) return x.y>y.y;
	return x.x<y.x;
}
inline void add_edge(int x,int y)
{
	nex[++tot]=fir[x];fir[x]=tot;got[tot]=y;
}
void dfs(int x,int fa)
{
	ans[++cnt]=x;
	for (int i=fir[x];i;i=nex[i])
	{
		int y=got[i];
		if ((x==f1 && y==f2) || (x==f2 && y==f1)) continue;
		if (y!=fa) dfs(y,x);
	}
}
void tarjan(int x,int fa)
{
	s[++dep]=x;
	ins[x]=true;
	low[x]=dfn[x]=++ord;
	for (int i=fir[x];i;i=nex[i])
	{
		int y=got[i];
		if (y==fa) continue;
		if (ins[y]) low[x]=min(low[x],dfn[y]);
		else tarjan(y,x),low[x]=min(low[x],low[y]);
	}
	if (low[x]==dfn[x])
	{
		int top;
		int num=0;
		loop:
			++num;
			top=s[dep--];
			ins[top]=false;
			color[top]=1;
		if (top!=x) goto loop;
		if (num==1) color[top]=0;
	}
}
void dfs3(int x,int fa,int k)
{
	if (x>=k)
	{
		f1=fa;
		f2=x;
		return;
	}
	for (int i=fir[x];i;i=nex[i])
	{
		int y=got[i];
		if (!color[y] || y==fa) continue;
		dfs3(y,x,k);
	}
}
void dfs2(int x,int fa)
{
	if (color[x])
	{
		top=x;
		int num=0,one=0;
		for (int i=fir[x];i;i=nex[i])
		{
			int y=got[i];
			if (y==fa) continue;
			++num;
			if (num==1) one=y;
			if (num==2) sec=y;
			if (num==2) break;
		}
		dfs3(one,x,sec);
		return;
	}
	for (int i=fir[x];i;i=nex[i])
	{
		int y=got[i];
		if (y!=fa) dfs2(y,x);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n=read();
	int m=read();
	if (m==n-1)
	{
		for (int i=1;i<=m;i++)
		{
			a[i].x=read();
			a[i].y=read();
			a[i+m].x=a[i].y;
			a[i+m].y=a[i].x;
		}
		sort(a+1,a+2*m+1,cmp);
		for (int i=1;i<=m*2;i++)
			add_edge(a[i].x,a[i].y);
		dfs(1,0);
		for (int i=1;i<=n;i++) write(ans[i]),putchar(' ');
	}
	if (n==m)
	{
		for (int i=1;i<=m;i++)
		{
			a[i].x=read();
			a[i].y=read();
			a[i+m].x=a[i].y;
			a[i+m].y=a[i].x;
		}
		sort(a+1,a+2*m+1,cmp);
		for (int i=1;i<=m*2;i++)
			add_edge(a[i].x,a[i].y);
		tarjan(1,0);
		for (int i=1;i<=n;i++) printf("%d ",color[i]);
		printf("\n");
		dfs2(1,0);
		printf("%d %d\n",f1,f2);
		dfs(1,0);
		for (int i=1;i<=n;i++) write(ans[i]),putchar(' ');
	}
	return 0;
}
