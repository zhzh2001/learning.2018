#include <cstdio>
#include <cstring>
using namespace std;
#define N 200001
#define MAX 1000000000
int tot;
int c[N];
int f[N],fa[N];
int tre[N][2];
int fir[N],nex[N],got[N];
inline int read()
{
	int x=0;
	char ch=getchar();
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
inline int min(int x,int y)
{
	return x<y?x:y;
}
inline void write(int x)
{
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void add_edge(int x,int y)
{
	nex[++tot]=fir[x];fir[x]=tot;got[tot]=y;
}
inline int tree_dp(int x,int fa,int k)
{
	if (f[x]==2 && k!=1) return MAX;
	if (f[x]==1 && k!=0) return MAX;
	if (tre[x][k]!=MAX) return tre[x][k]; 
	if (k) tre[x][k]=c[x];
	else tre[x][k]=0;
	for (int i=fir[x];i;i=nex[i])
	{
		int y=got[i];
		if (y==fa) continue;
		if (k) tre[x][k]+=min(tree_dp(y,x,0),tree_dp(y,x,1));
		else tre[x][k]+=tree_dp(y,x,1);  
	}
	return tre[x][k];
}
inline void dfs(int x,int father)
{
	fa[x]=father;
	for (int i=fir[x];i;i=nex[i])
	{
		int y=got[i];
		if (y==father) continue;
		dfs(y,x);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n=read();
	int m=read();
	char f1=getchar();
	char f2=getchar();
	for (int i=1;i<=n;i++) c[i]=read();
	for (int i=1;i<=n-1;i++)
	{
		int x=read();
		int y=read();
		add_edge(x,y);add_edge(y,x);
	}
	if (n<=2000)
	{			
		dfs(1,0);
		for (int i=1;i<=m;i++)
		{
			int a=read();
			int x=read();
			int b=read();
			int y=read();
			if ((x==0 && y==0) && (fa[a]==b || fa[b]==a)) 
			{
				printf("-1\n");continue;
			}
			f[a]=x+1;f[b]=y+1;
			for (int j=1;j<=n;j++) tre[j][0]=tre[j][1]=MAX;
			tree_dp(1,0,0);
			tree_dp(1,0,1);
			//if (tre[1][0]==-1) tre[1][0]=MAX;
			//if (tre[1][1]==-1) tre[1][1]=MAX;
			printf("%d\n",min(tre[1][0],tre[1][1]));
			//printf("%d %d\n",tre[1][0],tre[1][1]);
			f[a]=0;f[b]=0;
		}
	}
	return 0;
}