#include<bits/stdc++.h>
using namespace std;

#define MAX 50001

int n,m;
long long p[MAX];

int cnt,fst[MAX];
struct edge{
	int to,nxt;
}e[MAX];

void link(int u,int v){
	e[++cnt].nxt=fst[u];
	fst[u]=cnt;
	e[cnt].to=v;
}

long long dp[2][MAX];

int q[MAX];

long long dfs(int x,int lst,int opt){
	if(dp[opt][x])
		return dp[opt][x];
	long long ans=0;
	if(opt){
		for(int i=fst[x];i;i=e[i].nxt){
			int v=e[i].to;
			if(v!=lst){
				if(q[v]!=-1)
					ans+=dfs(v,x,q[v]);
				else
					ans+=min(dfs(v,x,0),dfs(v,x,1));
			}
		}
		
	}
	else{
		for(int i=fst[x];i;i=e[i].nxt){
			int v=e[i].to;
			if(v!=lst){
				if(q[v]==0)
					ans=2*0xffffffff;
				ans+=dfs(v,x,1);
			}
		}
	}
	return dp[opt][x]=ans+opt*p[x];
}

char s[5];

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for(int i=1;i<=n;++i)
		scanf("%d",&p[i]);
	for(int i=1;i<n;++i){
		int u,v;
		scanf("%d%d",&u,&v);
		link(u,v);
		link(v,u);
	}
	link(0,1);
	link(1,0);
	for(int i=1;i<=m;++i){
		int a,x,b,y;
		memset(dp,0,sizeof(dp));
		scanf("%d%d%d%d",&a,&x,&b,&y);
		for(int i=0;i<=n;++i)
			q[i]=-1;
		q[a]=x,q[b]=y;
		if(x==0 && y==0){
			int flag=0;
			for(int i=fst[a];i;i=e[i].nxt){
				if(e[i].to==b){
					printf("-1\n");
					flag=1;
					break;
				}
			}
			if(!flag){
				dfs(0,0,1);
				printf("%d\n",dp[1][0]);
			}
		}
		else{
			dfs(0,0,1);
			printf("%d\n",dp[1][0]);
		}
	}
}
/*
5 3 C3
2 4 1 3 9
1 5
5 2
5 3
3 4
1 0 3 0
2 1 3 1
1 1 5 0

*/
