#include<bits/stdc++.h>
using namespace std;

#define MAX 5001

int n,m;
int cnt,fst[MAX];
struct edge{
	int to,nxt;
}e[2*MAX];
void link(int u,int v){
	e[++cnt].nxt=fst[u];
	fst[u]=cnt;
	e[cnt].to=v;
}

int vis[MAX];

int s;

void dfs(int u,int lst){
	priority_queue<int,vector<int>,greater<int> >q;
	for(int i=fst[u];i;i=e[i].nxt){
		int v=e[i].to;
		if(v!=lst){
			q.push(v);
		}
	}
	while(!q.empty()){
		printf("%d ",q.top());
		dfs(q.top(),u);
		q.pop();
	}
	return;
}

int circle[MAX];
int tar;
vector<int>cir;

int check_circle(int pos,int lst){
	circle[pos]=1;
	for(int i=fst[pos];i;i=e[i].nxt){
		if(tar)
			break;
		int v=e[i].to;
		
		if(v!=lst){
			if(circle[v]){
				tar=v;
				cir.push_back(v);
				circle[pos]=2;
				return 0;
			}
			if(!check_circle(v,pos)){
				circle[pos]=2;
				cir.push_back(v);
				if(pos==tar)
					return 1;
				return 0;
			}
		}
		
	}
	return 1;
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;++i){
		int u,v;
		scanf("%d%d",&u,&v);
		link(u,v);
		link(v,u);
	}
	if(n-1==m){
		printf("1 ");
		dfs(1,0);
	}
	else{
		check_circle(1,0);
		int now[2],opt=0;
		check_circle(1,0);
		int head=cir.size()-2,tail=0;
		if(cir[head]<cir[tail])
			while(cir[head]<cir[tail]){
				head--;
				opt=1
			}
		else
			while(cir[head]<cir[tail]){
				tail++;
				opt=2;
			}
			int s,t;
		if(opt==1)
			s=head,t=head-1;
		else
			s=tail,t=tail+1;
		if(e[fst[s]].to==t)
			fst[s]=e[fst[s]].nxt;
		else{
			for(int i=fst[s];i;i=e[i].nxt){
				if(e[e[i].nxt].to==t)
					e[i].nxt=e[e[i].nxt].nxt;
			}
		}
		if(e[fst[t]].to==s)
			fst[t]=e[fst[t]].nxt;
		else{
			for(int i=fst[t];i;i=e[i].nxt){
				if(e[e[i].nxt].to==s)
					e[i].nxt=e[e[i].nxt].nxt;
			}
		}
		printf("1 ");
		dfs(1,0);
	}
}
