#include <bits/stdc++.h>

#define rep(i , l , r) for(int i = (l) , ___ = (r) ; i <= ___ ; ++i)
#define	per(i , r , l) for(int i = (r) , ___ = (l) ; i >= ___ ; --i)

using namespace std;

template<typename T>inline T read(T &f)
{
	f = 0 ; int x = 1 ; char c = getchar();
	while(!isdigit(c))
		x = c == '-' ? -1 : 1 , c = getchar();
	while(isdigit(c))
		(f *= 10) += c & 15 , c = getchar();
	return f *= x;
}

template<typename T>inline int chkmin(T &x , const T &y) { return x > y ? x = y , 1 : 0; }
template<typename T>inline int chkmax(T &x , const T &y) { return x < y ? x = y , 1 : 0; }

const int N = 100000 + 5;

int n , m , u , v;

struct Edge
{
	int to ; Edge *nxt;
} E[N<<1] , *pre[N];

void addedge(int u , int v)
{
	static int tot = 0;
	E[++tot] = (Edge){v , pre[u]} , pre[u] = E + tot;
	E[++tot] = (Edge){u , pre[v]} , pre[v] = E + tot;
}

char op[10];

int p[N] , x , y;

long long f[N][3];

// 0 bufang 1 fang 2 bufang bei fu gai

const int INF = 0x3f3f3f3f;

void dfs(int u , int fa)
{
	if(f[u][1] != INF) f[u][1] = p[u];
	long long mn = INF; int p = 0 , cnt = 0;
	for(Edge *i = pre[u] ; i ; i = i->nxt)
	{
		int v = i->to;
		if(v == fa) continue ;
		dfs(v , u);
		cnt++;
		if(chkmin(mn , f[v][1])) p = v;
		f[u][0] += f[v][2];
		if(f[u][0] >= INF) f[u][0] = INF;
		f[u][1] += min(f[v][0] , min(f[v][1] , f[v][2]));
	}
	if(cnt == 0)     
		f[u][2] = INF;
	else
	{
		f[u][2] += mn;
		for(Edge *i = pre[u] ; i ; i = i->nxt)
		{
			int v = i->to;
			if(v == fa || v == p) continue ;
			f[u][2] += min(f[v][1] , f[v][2]);
		}
	}
}

int main()
{
	freopen("defense2.in" , "r" , stdin);
	freopen("defense.out" , "w" , stdout);
	read(n) , read(m) , scanf("%s" , op);
	rep(i , 1 , n) read(p[i]);
	rep(i , 1 , n - 1)
	{
		read(u) , read(v);
		addedge(u , v);
	}
	int S = (1 << n) - 1;
	rep(i , 1 , m)
	{
		read(u) , read(x) , read(v) , read(y);
	}
	return 0;
}

