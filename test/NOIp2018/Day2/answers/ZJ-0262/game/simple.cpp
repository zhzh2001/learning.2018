#pragma GCC optimize("Ofast,-no-stack-protector")
#include <bits/stdc++.h>

#define rep(i , l , r) for(int i = (l) , ___ = (r) ; i <= ___ ; ++i)
#define	per(i , r , l) for(int i = (r) , ___ = (l) ; i >= ___ ; --i)

using namespace std;

template<typename T>inline T read(T &f)
{
	f = 0 ; int x = 1 ; char c = getchar();
	while(!isdigit(c))
		x = c == '-' ? -1 : 1 , c = getchar();
	while(isdigit(c))
		(f *= 10) += c & 15 , c = getchar();
	return f *= x;
}

template<typename T>inline int chkmin(T &x , const T &y) { return x > y ? x = y , 1 : 0; }
template<typename T>inline int chkmax(T &x , const T &y) { return x < y ? x = y , 1 : 0; }

const int KCZ = 1e9 + 7;

int n , m;

#define id(i , j) ((i - 1) * m + j)

namespace task0
{
	int A[10][10] , cnt , flag;
	
	string str[10000]; int v[10000];
	
	void dfs(int x , int y , string S , int now)
	{
		(now *= 2) += A[x][y];
		if(x == n && y == m)
		{
			// cout << now << ' ' << S << endl;
			rep(i , 1 , cnt)
			{
				if(str[i] < S)
				{
					if(now > v[i])
					{
						flag = 0;
					}
				}
				else
				{
					if(now < v[i])
					{
						flag = 0;
					}
				}
			}
			++cnt;
			str[cnt] = S , v[cnt] = now;
			return ;	
		}
		string ns = S;
		if(x + 1 <= n)
		{
			ns.push_back('D');
			dfs(x + 1 , y , ns , now);
		}
		if(y + 1 <= m)
		{
			ns = S; ns.push_back('R');
			dfs(x , y + 1 , ns , now);
		}
	}
	
	void main()
	{
		int S = (1 << (n * m)) - 1;
		int Ans = 0;
		rep(s , 0 , S)
		{
			rep(i , 1 , n)
			rep(j , 1 , m)
			{
				A[i][j] = (bool)(s & (1 << (id(i , j))));
			}
			flag = 1 , cnt = 0 , dfs(1 , 1 , "" , 0);
			if(flag) Ans++;
			// cout << endl;
		}
		cout << Ans << endl;
	}
}

typedef long long LL;

void add(int &x , const int &y) { x += y ; if(x >= KCZ) x -= KCZ; }

LL mp(LL x , LL y)
{
	LL res = 1;
	while(y)
	{
		if(y & 1) res = res * x % KCZ;
		x = x * x % KCZ;
		y >>= 1;
	}
	return res;
}

namespace task1
{
	int f[10] , g[10];
	void main()
	{
		rep(s , 0 , 3)
		{
			f[s] = 1;
		}
		rep(i , 2 , m)
		{
			add(g[1] , f[0]) , add(g[0] , f[0]);
			add(g[0] , f[2]) , add(g[2] , f[1]) , add(g[0] , f[1]) , add(g[3] , f[1]);
			add(g[0] , f[2]) , add(g[1] , f[2]);
			add(g[0] , f[3]) , add(g[1] , f[3]) , add(g[2] , f[3]) , add(g[3] , f[3]);
			swap(g , f);
			memset(g , 0 , sizeof g);
		}
		int Ans = 0;
		rep(s , 0 , 3) 
		{
			add(Ans , f[s]);
			// cout << f[s] << endl;
		}
		cout << Ans << endl;
	}	
}

int main()
{
	read(n) , read(m);
	task0::main();
	return 0;
}

