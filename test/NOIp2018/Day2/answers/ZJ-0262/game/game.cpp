#include <bits/stdc++.h>

#define rep(i , l , r) for(int i = (l) , ___ = (r) ; i <= ___ ; ++i)
#define	per(i , r , l) for(int i = (r) , ___ = (l) ; i >= ___ ; --i)

using namespace std;

template<typename T>inline T read(T &f)
{
	f = 0 ; int x = 1 ; char c = getchar();
	while(!isdigit(c))
		x = c == '-' ? -1 : 1 , c = getchar();
	while(isdigit(c))
		(f *= 10) += c & 15 , c = getchar();
	return f *= x;
}

template<typename T>inline int chkmin(T &x , const T &y) { return x > y ? x = y , 1 : 0; }
template<typename T>inline int chkmax(T &x , const T &y) { return x < y ? x = y , 1 : 0; }

const int KCZ = 1e9 + 7;

int n , m;

#define id(i , j) ((i - 1) * m + j)

namespace task0
{
	int A[5][5] , cnt , flag;
	
	string str[1000]; int v[1000];
	
	void dfs(int x , int y , string S , int now)
	{
		(now *= 2) += A[x][y];
		if(x == n && y == m)
		{
			// cout << now << ' ' << S << endl;
			rep(i , 1 , cnt)
			{
				if(str[i] < S)
				{
					if(now > v[i])
					{
						flag = 0;
					}
				}
				else
				{
					if(now < v[i])
					{
						flag = 0;
					}
				}
			}
			++cnt;
			str[cnt] = S , v[cnt] = now;
			return ;	
		}
		string ns = S;
		if(x + 1 <= n)
		{
			ns.push_back('D');
			dfs(x + 1 , y , ns , now);
		}
		if(y + 1 <= m)
		{
			ns = S; ns.push_back('R');
			dfs(x , y + 1 , ns , now);
		}
	}
	
	void main()
	{
		int S = (1 << (n * m)) - 1;
		int Ans = 0;
		rep(s , 0 , S)
		{
			rep(i , 1 , n)
			rep(j , 1 , m)
			{
				A[i][j] = (bool)(s & (1 << (id(i , j))));
			}
			flag = 1 , cnt = 0 , dfs(1 , 1 , "" , 0);
			if(flag) Ans++;
			// cout << endl;
		}
		cout << Ans << endl;
	}
}

typedef long long LL;

void add(int &x , const int &y) { x += y ; if(x >= KCZ) x -= KCZ; }

LL mp(LL x , LL y)
{
	LL res = 1;
	while(y)
	{
		if(y & 1) res = res * x % KCZ;
		x = x * x % KCZ;
		y >>= 1;
	}
	return res;
}

namespace task1
{
	void main()
	{
		LL v = 12;
		rep(i , 2 , m - 1)
		{
			v = v * 3 % KCZ;
		}
		cout << v << endl;
	}	
}

namespace task2
{
	void main()
	{
		LL v = 112;
		rep(i , 3 , m - 1)
		{
			v = v * 3 % KCZ;
		}
		cout << v << endl;
	}	
}

int main()
{
	freopen("game.in" , "r" , stdin);
	freopen("game.out" , "w" , stdout);
	read(n) , read(m);
	if(n > m) swap(n , m);
	if(n <= 3 && m <= 3) task0::main();
	else if(n == 1)
	{
		printf("%lld\n" , mp(2 , m));
		return 0;
	}
	else if(n <= 2) task1::main();
	else if(n <= 3) task2::main();
	else
	{
		if(n == 4 && m == 4) printf("%d\n" , 912);
		if(n == 4 && m == 5) printf("%d\n" , 2688);
		
		if(n == 5 && m == 5) printf("%d\n" , 7136);
	}
	return 0;
}

