#include <bits/stdc++.h>

#define rep(i , l , r) for(int i = (l) , ___ = (r) ; i <= ___ ; ++i)
#define	per(i , r , l) for(int i = (r) , ___ = (l) ; i >= ___ ; --i)

using namespace std;

template<typename T>inline T read(T &f)
{
	f = 0 ; int x = 1 ; char c = getchar();
	while(!isdigit(c))
		x = c == '-' ? -1 : 1 , c = getchar();
	while(isdigit(c))
		(f *= 10) += c & 15 , c = getchar();
	return f *= x;
}

template<typename T>inline int chkmin(T &x , const T &y) { return x > y ? x = y , 1 : 0; }
template<typename T>inline int chkmax(T &x , const T &y) { return x < y ? x = y , 1 : 0; }

const int N = 5000 + 5;

struct Edge
{
	int to; Edge *nxt;
} E[N<<1] , *pre[N];

int deg[N];

void addedge(int u , int v)
{
	static int tot = 0;
	E[++tot] = (Edge){v , pre[u]} , pre[u] = E + tot;
	E[++tot] = (Edge){u , pre[v]} , pre[v] = E + tot;
	deg[u]++ , deg[v]++;
}

int vis[N];

typedef vector<int>::iterator vit;

namespace task0
{
	void dfs(int u)
	{
		if(u == 1) printf("%d" , u);
		else printf(" %d" , u);
		vector<int> S;
		for(Edge *i = pre[u] ; i ; i = i->nxt)
		{
			int v = i->to;
			if(vis[v]) continue ;
			S.push_back(v);
		}
		sort(S.begin() , S.end());
		for(vit i = S.begin() ; i != S.end() ; ++i)
		{
			if(vis[*i]) continue ;
			vis[*i] = 1 , dfs(*i);
		}
	}
	void main()
	{
		vis[1] = 1 , dfs(1);
		puts("");
	}
}

int n , m;

namespace task1
{
	int dfn[N] , low[N] , in[N] , fa[N];
	
	int Ans[N] , tp[N] , cnt;
	
	int bu , bv;
	
	void dfs(int u)
	{
		tp[++cnt] = u;
		vector<int> S;
		for(Edge *i = pre[u] ; i ; i = i->nxt)
		{
			int v = i->to;
			if(vis[v]) continue ;
			if(min(u , v) == bu && max(u , v) == bv) continue ;
			S.push_back(v);
		}
		sort(S.begin() , S.end());
		for(vit i = S.begin() ; i != S.end() ; ++i)
		{
			if(vis[*i]) continue ;
			vis[*i] = 1 , dfs(*i);
		}
	}
	
	int comp()
	{
		if(Ans[1] == 0) return 1;
		rep(i , 1 , n)
		{
			if(Ans[i] == tp[i]) continue ;
			else return tp[i] < Ans[i];
		}
		return 0;
	}
	
	void find(int u , int fa)
	{
		static int dc = 0;
		dfn[u] = low[u] = ++dc;
		for(Edge *i = pre[u] ; i ; i = i->nxt)
		{
			int v = i->to;
			if(v == fa) continue;
			if(dfn[v])
			{
				if(chkmin(low[u] , dfn[v]))
				{
					in[v] = 1 , bu = u , bv = v;
					if(bu < bv) swap(bu , bv);
					rep(i , 1 , n) vis[i] = 0;
					cnt = 0 , vis[1] = 1 , dfs(1);
					if(comp()) swap(Ans , tp);
				}
			}
			else
			{
				find(v , u);
				if(chkmin(low[u] , low[v]))
				{
					bu = u , bv = v;
					if(bu > bv) swap(bu , bv);
					rep(i , 1 , n) vis[i] = 0;
					cnt = 0 , vis[1] = 1 , dfs(1);
					if(comp()) swap(Ans , tp);
				}
			}
		}
		if(low[u] < dfn[u]) in[u] = 1;
	}
	
	void print(int u)
	{
		printf("%d " , u);
		vis[u] = 1;
		for(Edge *i = pre[u] ; i ; i = i->nxt)
		{
			int v = i->to;
			if(vis[v] || !in[v]) continue;
			print(v);
		}
	}
	
	void print()
	{
		rep(i , 1 , n) if(in[i])
		{
			memset(vis , 0 , sizeof vis);
			print(i); break;
		}
	}
	
	void main()
	{
		find(1 , 0);
		rep(i , 1 , n - 1) printf("%d " , Ans[i]);
		printf("%d\n" , Ans[n]);
	}
}

int u , v;

int main()
{
	freopen("travel.in" , "r" , stdin);
	freopen("travel.out" , "w" , stdout);
	read(n) , read(m);
	rep(i , 1 , m)
	{
		read(u) , read(v);
		addedge(u , v);
	}
	if(m == n - 1) task0::main();
	else task1::main();
	return 0;
}

