#include<queue>
#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
int n,m,a,b,cnt=0,tmpl=0,to[5005],next[5005],head[5005],ans[5005],tmp[5005];
bool vis[5005];
queue<int> q;
int mymin(int x,int y)
{
	return x>y?y:x;
}
void add(int a,int b)
{
	cnt++;to[cnt]=b;next[cnt]=head[a];head[a]=cnt;
}
void dfs(int x)
{
	int minn=0x7fffffff;
	for(int i=head[x];i!=-1;i=next[i])
	{
		if(!vis[to[i]])
			minn=mymin(minn,to[i]);
	}
	if(minn!=0x7fffffff) 
	{
		printf("%d ",minn);
		vis[minn]=true;
		dfs(minn);
	}
	return;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	//ios::sync_with_stdio(false);
	memset(head,-1,sizeof(head));
	memset(ans,0x3f,sizeof(ans));
	cin>>n>>m;
	for(int i=1;i<=m;i++)
	{
		cin>>a>>b;
		add(a,b);
		add(b,a);
	}
	vis[1]=true;
	printf("1 ");
	dfs(1);
	dfs(1);
	return 0;
}
