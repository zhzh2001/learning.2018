#include <bits/stdc++.h>
#define pb push_back
using namespace std;

const int N = 5100;

int dfn[N], ans[N], Time, u[N], v[N], spu = -1, spv = -1;
bool vis[N];
vector<int> E[N];

void dfs(int rt)
{
	vis[rt] = 1;
	int s = E[rt].size();
	dfn[++Time] = rt;
	for (int i = 0; i < s; i++) if (!vis[E[rt][i]] && !((spu == rt && spv == E[rt][i]) || (spv == rt && spu == E[rt][i])))
		dfs(E[rt][i]);
}

int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	int n, m;
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; i++) scanf("%d%d", u + i, v + i);
	for (int i = 1; i <= m; i++) E[v[i]].pb(u[i]), E[u[i]].pb(v[i]);
	for (int i = 1; i <= n; i++) sort(E[i].begin(), E[i].end());
	if (m == n - 1)
	{
		dfs(1);
		for (int i = 1; i <= n; i++) printf("%d ", dfn[i]);
	}
	else
	{
		memset(ans, 0x3f, sizeof ans);
		for (int i = 1; i <= m; i++)
		{
			spu = u[i], spv = v[i];
			memset(vis, 0, sizeof vis);
			Time = 0;
			dfs(1);
			if (Time != n) continue;
			for (int j = 1; j <= n; j++)
			{
				if (dfn[j] > ans[j]) break;
				if (dfn[j] < ans[j])
				{
					for (int k = 1; k <= n; k++) ans[k] = dfn[k];
					break;
				}
			}
		}
		for (int i = 1; i <= n; i++) printf("%d ", ans[i]);
	}
	puts("");
	fclose(stdin);
	fclose(stdout);
	return 0;
}
