#include <bits/stdc++.h>
using namespace std;
typedef long long LL;


const LL INF = 1e13;

const int N = 100100;
int fst[N], nxt[N << 1], to[N << 1], a[N], E = 0;
LL  dp[N][2], idp[N][2];
void add(int u, int v) { nxt[++E] = fst[u], fst[u] = E, to[E] = v; }

int b, y;

void dfs(int rt, int fa)
{
	dp[rt][0] = 0, dp[rt][1] = a[rt];
	for (int i = fst[rt]; i != -1; i = nxt[i]) if (to[i] != fa)
	{
		dfs(to[i], rt);
		dp[rt][0] += dp[to[i]][1];
		dp[rt][1] += min(dp[to[i]][0], dp[to[i]][1]);
	}
	if (rt == b) dp[rt][y ^ 1] = INF;
}

int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	memset(fst, -1, sizeof fst);
	int n, m, u, v, x, t;
	char str[4];
	scanf("%d%d%s", &n, &m, str + 1);
	for (int i = 1; i <= n; i++) scanf("%d", a + i);
	for (int i = 1; i < n; i++) scanf("%d%d", &u, &v), add(u, v), add(v, u);
	if (str[1] == 'A' && str[2] == '1')
	{
		dp[1][1] = a[1]; dp[1][0] = INF;
		for (int i = 2; i <= n; i++)
		{
			dp[i][1] = min(dp[i - 1][0], dp[i - 1][1]) + a[i];
			dp[i][0] = dp[i - 1][1];
		}
		idp[n][0] = 0, idp[n][1] = a[n];
		for (int i = n - 1; i >= 1; i--)
		{
			idp[i][1] = min(idp[i + 1][0], idp[i + 1][1]) + a[i];
			idp[i][0] = idp[i + 1][1];
		}
		for (int i = 1; i <= m; i++)
		{
			scanf("%*d%*d%d%d", &b, &y);
			if (y) printf("%lld\n", min(dp[b - 1][0], dp[b - 1][1]) + min(idp[b + 1][0], idp[b + 1][1]) + a[b]);
			else printf("%lld\n", dp[b - 1][1] + idp[b + 1][1]);
		}
	}
	else for (int i = 1; i <= m; i++)
	{
		scanf("%d%d%d%d", &t, &x, &b, &y);
		dfs(t, -1);
		printf("%lld\n", dp[t][x] >= INF ? -1ll : dp[t][x]);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
