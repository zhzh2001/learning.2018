#include <bits/stdc++.h>
using namespace std;
typedef long long LL;

const LL MOD = 1e9 + 7, inv2 = 500000004;

LL Pow(LL x, LL i)
{
	LL ret = 1;
	for (; i; i >>= 1, x = x * x % MOD) if (i & 1) ret = ret * x % MOD;
	return ret;
}

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	int n, m;
	scanf("%d%d", &n, &m);
	if (n <= 3 && m <= 3)
	{
		if (n == 1 && m == 1) puts("2");
		if (n == 1 && m == 2) puts("4");
		if (n == 1 && m == 3) puts("8");
		if (n == 2 && m == 1) puts("2");
		if (n == 2 && m == 2) puts("12");
		if (n == 2 && m == 3) puts("36");
		if (n == 3 && m == 1) puts("8");
		if (n == 3 && m == 2) puts("36");
		if (n == 3 && m == 3) puts("112");
	}
	else if (n == 2)
	{
		printf("%lld\n", 4ll * Pow(3, m - 1) % MOD);
	}
	else if (n == 3)
	{
		printf("%lld\n", ((36ll * Pow(4, m - 2) % MOD - Pow(2, 2 * m - 1) * (m - 1) % MOD * (m - 2) % MOD * inv2 % MOD) % MOD + MOD) % MOD);
	}
	else if (n == 5 && m == 5) puts("7136");
	fclose(stdin);
	fclose(stdout);
	return 0;
}
