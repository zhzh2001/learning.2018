#include<bits/stdc++.h>
using namespace std;
bool mmm1;
const int M=1e5+5;
int n,m;
char str[5];
int head[M],to[M<<1],nxt[M<<1],tot;
void Add(int x,int y){
	to[++tot]=y;
	nxt[tot]=head[x];
	head[x]=tot;
}
#define LL long long
int c[M];
struct P50{
	LL dp[M][2];
	bool vis[M],kd[M];
	void dfs(int x,int f){
		dp[x][0]=0,dp[x][1]=c[x];
		for(int i=head[x];i;i=nxt[i]){
			int y=to[i];
			if(y==f)continue;
			dfs(y,x);
			dp[x][1]+=min(dp[y][0],dp[y][1]);
			dp[x][0]+=dp[y][1];
		}
		if(vis[x])dp[x][1-kd[x]]=2e12;
	}
	void solve(){
		for(int i=1;i<=m;i++){
			int a,b,x,y;
			for(int j=1;j<=n;j++)vis[j]=0;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			vis[a]=1,vis[b]=1;
			kd[a]=x,kd[b]=y;
			dfs(1,0);LL ans;
			if(vis[1])ans=dp[1][kd[1]];
			else ans=min(dp[1][0],dp[1][1]);
			printf("%lld\n",ans<1e12?ans:-1);
		}
	}
}p50;
bool mmm2;
int main(){//long long  fole memory  the most
//	printf("%lf  MB  \n",(&mmm2-&mmm1)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,str);
	for(int i=1;i<=n;i++)scanf("%d",&c[i]);
	for(int a,b,i=1;i<n;i++){
		scanf("%d%d",&a,&b);
		Add(a,b),Add(b,a);
	}
	p50.solve();
	return 0;
}
