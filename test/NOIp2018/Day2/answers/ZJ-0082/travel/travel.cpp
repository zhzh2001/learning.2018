#include<bits/stdc++.h>
using namespace std;
bool mmm1;
const int M=5005;
int n,m;
vector<int>edge[M];
struct P50{
	void dfs(int x,int f){
		printf("%d ",x);
		for(int i=0;i<(int)edge[x].size();i++){
			int y=edge[x][i];
			if(y==f)continue;
			dfs(y,x);
		}
	}
	void solve(){
		for(int i=1;i<=n;i++)
			sort(edge[i].begin(),edge[i].end());
		dfs(1,0);
	}
}p50;
struct P_50{
	bool mark[M],vis[M];
	int bx,ex;
	void dfs(int x,int f){
		vis[x]=1;
		for(int i=0;i<(int)edge[x].size();i++){
			int y=edge[x][i];
			if(y==f)continue;
			if(vis[y]){bx=x,ex=y;return;}
			dfs(y,x);
		}
	}
	int stk[M],top;
	bool flag;
	void redfs(int x,int f){
		stk[++top]=x;
		if(x==ex){flag=1;return;}
		for(int i=0;i<(int)edge[x].size();i++){
			int y=edge[x][i];
			if(y==f)continue;
			redfs(y,x);
			if(flag)return;
		}
		top--;
	}
	priority_queue<int>Q;
	void solve(){
		mark[1]=1;
		Q.push(-1);
		while(!Q.empty()){
			int x=-Q.top();Q.pop();
			printf("%d ",x);
			for(int i=0;i<(int)edge[x].size();i++){
				int y=edge[x][i];
				if(mark[y])continue;
				mark[y]=1;
				Q.push(-y);
			}
		}
	}
}p_50;
struct P15{
	int hav[M],tot;
	bool mark[M];
	void calc1(int x,int f,int k){
		printf("%d ",x);
		mark[x]=1;
		for(int i=0;i<(int)edge[x].size();i++){
			int y=edge[x][i];
			if(y==f)continue;
			if(mark[y])continue;
			if(y>k)return;
			calc1(y,x,k);
		}
	}
	void calc2(int x,int f){
		if(mark[x])return;
		mark[x]=1;
		printf("%d ",x);
		for(int i=0;i<(int)edge[x].size();i++){
			int y=edge[x][i];
			if(mark[y])continue;
			calc2(y,x);
		}
	}
	void solve(){
		tot=0;
		for(int i=0;i<(int)edge[1].size();i++)
			hav[++tot]=edge[1][i];
		printf("%d ",1);mark[1]=1;
		if(hav[1]<hav[2])calc1(hav[1],1,hav[2]),calc2(hav[2],1);
		else calc1(hav[2],1,hav[1]),calc2(hav[1],1);
	}
}p15;
int in[M];
bool check(){
	for(int i=1;i<=n;i++)if(in[i]!=2)return false;
	return true;
}
bool mmm2;
int main(){//long long  fole memory  the most
//	printf("%lf  MB  \n",(&mmm2-&mmm1)/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int a,b,i=1;i<=m;i++){
		scanf("%d%d",&a,&b);
		in[a]++,in[b]++;
		edge[a].push_back(b);
		edge[b].push_back(a);
	}
	if(m==n-1)p50.solve();
	else if(check())p15.solve();
	else p_50.solve();
	return 0;
}
