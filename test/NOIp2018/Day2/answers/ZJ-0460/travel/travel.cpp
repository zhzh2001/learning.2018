#include<bits/stdc++.h>
using namespace std;

const int N=5000+50;
const int M=5000+50;
// m<=n --> m==n or m==n-1
int n,m,ans[N],vis[N],cnt(0);
int dfn[N],low[N],siz[N],num(0);
int stk[N],tp(0),cir[N];
vector<int> g[N];


inline int read(){
	char ch=getchar();int x=0,f=1;
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-48;
	return x*f;
}

void tarjan(int x,int fa){// find circle
	low[x]=dfn[x]=++num,stk[++tp]=x,vis[x]=1;
	int tot=g[x].size();
	for(int i=0,y;i<tot;i++){
		if((y=g[x][i])==fa) continue;
		if(!dfn[y]) {tarjan(y,x);low[x]=min(low[x],low[y]);}
		else if(vis[y]) low[x]=min(low[x],dfn[y]);
	}
	if(low[x]==dfn[x]){
		siz[x]=1;
		while(stk[tp]!=x)
			vis[stk[tp]]=0,--tp,++siz[x];
		--tp,vis[x]=0;
	}
}

int flag(0),pos(0),z,p;
void dfs(int x){
	if(!vis[x]) ans[++cnt]=x;
	vis[x]=1;
	int tot=g[x].size();
	if(!cir[x]){
		for(int i=0,y;i<tot;i++)
			if(!vis[y=g[x][i]]) dfs(y);
		return;
	}
	for(int i=0,y;i<tot;i++){
		if(vis[y=g[x][i]]) continue;
		if(!cir[y]){
			dfs(y);
			continue;
		}
		if(pos>0&&cir[y]&&y>z){
			flag=1;
			return;
		}
		if(pos==0&&cir[y]){
			pos=x;
			for(int j=i+1;j<tot;j++)
				if(cir[g[x][j]]) {z=g[x][j];break;}
			dfs(y);
			if(flag) flag=0,pos=-1;
			continue;
		}
		dfs(y);
	}
	if(flag&&x!=pos) return;
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1,x,y;i<=m;i++){
		x=read(),y=read();
		g[x].push_back(y);
		g[y].push_back(x);
	}
	for(int i=1;i<=n;i++)
		sort(g[i].begin(),g[i].end());
	tarjan(1,0);
	for(int i=1;i<=n;i++)
		if(low[i]!=dfn[i]) cir[i]=1;
		else if(siz[i]>1) cir[i]=1;
	memset(vis,0x00,sizeof(vis));
	dfs(1);
	for(int i=1;i<=n;i++)
		printf("%d ",ans[i]); 
	return 0;
}
