#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
const ll N=1e5+5;
char typ[10];
ll n,m,head[N],cnt(0);
struct Edge{
	ll to,nxt;
	void add(ll u,ll v){
		to=v;nxt=head[u];head[u]=cnt++;
	}
} edge[N<<1];
ll p[N];
ll fa[N],dep[N];
ll f[N][3];//,g[N][3];
ll A,B,X,Y;

inline ll read(){
	char ch=getchar();ll x=0,f=1;
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-48;
	return x*f;
}

void dfs1(ll x){
	for(ll i=head[x],y;~i;i=edge[i].nxt){
		if((y=edge[i].to)==fa[x]) continue;
		fa[y]=x;
		dep[y]=dep[x]+1;
		dfs1(y);
	}
}

inline ll Min(ll x,ll y){
	if(x==-1||y==-1) return x+y+1;
	return (x<y)?x:y;
}

void dp(ll x){
	ll s1=0,s2=0;
	for(int i=head[x],y;~i;i=edge[i].nxt){
		if((y=edge[i].to)==fa[x]) continue;
		dp(y);
		if(f[y][1]==-1) s1=-1;
		if(~s1) s1+=f[y][1];
		s2+=f[y][2];
	}
	f[x][0]=s1;
	f[x][1]=s2+p[x];
	int t;
	if(x==A||x==B){
		if(x==A) t=X;
		else t=Y;
		f[x][t^1]=-1;
	}
	f[x][2]=Min(f[x][0],f[x][1]);
}

inline bool check(ll u,ll v){
	int dt=abs(dep[u]-dep[v]);
	if(dt!=1) return false;
	if(fa[u]==v||fa[v]==u) return true;
	return false;
}

void solve3(){
	dfs1(1);
	while(m--){
		A=read(),X=read(),B=read(),Y=read();
		if(X==0&&Y==0){
			if(check(A,B)){
				puts("-1");
				continue;
			}
		}
		memset(f,0x00,sizeof(f));
		dp(1);
		printf("%lld\n",f[1][2]);
	}
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	scanf("%s",typ);
	for(ll i=1;i<=n;i++) p[i]=read();
	memset(head,-1,sizeof(head));
	for(ll i=1,x,y;i<n;i++){
		x=read(),y=read();
		edge[cnt].add(x,y);
		edge[cnt].add(y,x);
	}
	solve3();
//	if(typ[1]=='3') solve3();
//	else if(typ[1]=='2') solve2();
//	else solve1();
	return 0;
}
