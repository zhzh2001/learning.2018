#include<bits/stdc++.h>
#define int long long
using namespace std;
const int P=1000000007;
int n,m;
int poww(int x,int y){
	int ans=1;
	for(int i=1;i<=y;i++)
		ans=(ans*x)%P;
	if(ans<4)
		ans+=P;
	return ans;
}
signed main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if(n==2||m==2)
		return printf("%lld",(poww(2,n*m)-4)%P),0;
	if(n==m&&n==3)
		return printf("112"),0;
	return 0;
}
