#include<bits/stdc++.h>
using namespace std;
long long n,m,ans,base=1e9+7,ma[4][4],cnt;
string s[100001];
inline long long up(long long x,long long y){
	long long temp=1,tmp=x;
	while(y){
		if(y&1)temp=temp*(tmp%base)%base;
		tmp=tmp*tmp%base;
		y>>=1;
	}
	return temp;
}
inline bool pd(string x,string y){
	int xx=1,xy=1,yx=1,yy=1;bool flag;
	for(int i=1;i<=n+m-2;++i){
		if(ma[xx][xy]==ma[yx][yy]){
			if(x[i-1]=='D')xx++; else xy++;
		if(y[i-1]=='D')yx++; else yy++;
		continue;
		}
		if(ma[xx][xy]>ma[yx][yy])return 0;
	    if(ma[xx][xy]<ma[yx][yy])return 1;
		if(x[i-1]=='D')xx++; else xy++;
		if(y[i-1]=='D')yx++; else yy++;
	}
	if(ma[xx][xy]==ma[yx][yy])return 1;
		if(ma[xx][xy]>ma[yx][yy])return 0;
	    if(ma[xx][xy]<ma[yx][yy])return 1;
	    
	return 1;
}
inline bool check(){
	for(int i=1;i<cnt;++i){
		for(int j=i+1;j<=cnt;++j){
			if(!pd(s[j],s[i]))return 0;
		}
		}
    return 1;
}
void mov(int x,int y,string tmp){
	if(x==n&&m==y){
		s[++cnt]=tmp;
		return;
	}
	if(x<n){
		mov(x+1,y,tmp+"D");
	}
	if(y<m){
		mov(x,y+1,tmp+"R");
	}
}
void dfs(int x,int y){
	if(x==n+1){
		if(check())ans++;
		return;
	}
	if(y==m){
		dfs(x+1,1);
	} else dfs(x,y+1);
	ma[x][y]=1;
	if(y==m){
		dfs(x+1,1);
	} else dfs(x,y+1);
	ma[x][y]=0;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if(n==1){
		printf("%lld\n",up(2,m));
		return 0;
	}
	if(m==1){
		printf("%lld\n",up(2,n));
		return 0;
	}
	if(n==2){
		printf("%lld",4*up(3,m-1)%base);
		return 0;
	}
	mov(1,1,"");
	sort(s+1,s+1+cnt);
	dfs(1,1);
	cout<<ans;
	return 0;
}
