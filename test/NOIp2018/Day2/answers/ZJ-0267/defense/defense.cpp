#include<bits/stdc++.h>
using namespace std;
int f[100001][3],n,m,cnt,ver[200001],head[200001],nxt[200001],p[200001],c1,c2,s1,s2;
string typ;
inline void add(int x,int y) {
	ver[++cnt]=y;
	nxt[cnt]=head[x];
	head[x]=cnt;
	ver[++cnt]=x;
	nxt[cnt]=head[y];
	head[y]=cnt;
}
inline int min(int x,int y) {
	return x<y?x:y;
}
inline void tree_dp(int x,int fa) {
	f[x][0]=f[x][1]=0;
	int minn=2e9+7,flag=0;
	f[x][2]=p[x];
	int fat=0;
	for(int i=head[x]; i; i=nxt[i]) {
		if(ver[i]==fa)continue;
		fat+=1;
		tree_dp(ver[i],x);
		if(ver[i]!=c1&&ver[i]!=c2){
			f[x][2]+=min(f[ver[i]][0],min(f[ver[i]][1],f[ver[i]][2]));
		if(f[ver[i]][2]<=f[ver[i]][1]) {
			f[x][1]+=f[ver[i]][2];
			flag=1;
			minn=0;
		} else {
			f[x][1]+=f[ver[i]][1];
			minn=min(minn,f[ver[i]][2]-f[ver[i]][1]);
		}
		f[x][0]+=f[ver[i]][1];
		}
		if(ver[i]==c1){
			if(s1){
				f[x][2]+=f[ver[i]][2];
				f[x][1]+=f[ver[i]][2];
				f[x][0]+=f[ver[i]][2];
			} else {
				f[x][2]+=min(f[ver[i]][1],f[ver[i]][0]);
				f[x][1]+=f[ver[i]][1];
				f[x][0]+=f[ver[i]][1];
			}
		}
		if(ver[i]==c2){
			if(s2){
				f[x][2]+=f[ver[i]][2];
				f[x][1]+=f[ver[i]][2];
				f[x][0]+=f[ver[i]][2];
			} else {
				f[x][2]+=min(f[ver[i]][1],f[ver[i]][0]);
				f[x][1]+=f[ver[i]][1];
				f[x][0]+=f[ver[i]][1];
			}
		}
	}
	
	if(!flag&&minn!=2e9+7)f[x][1]+=minn;
	if(!fat)f[x][1]=p[x];
	return;
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m>>typ;
	for(int i=1; i<=n; ++i) {
		scanf("%d",&p[i]);
	}
	for(int i=1; i<=n-1; ++i) {
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);
	}
	for(int i=1; i<=m; ++i) {
		scanf("%d%d%d%d",&c1,&s1,&c2,&s2);
		if(c1==c2&&s1!=s2) {
			printf("-1\n");
			continue;
		}
		int tar=0;
		for(int j=head[c1];j;j=nxt[j]){
			if(ver[j]==c2&&s1==0&&s2==0){
				tar=1;
				break;
			}
		}
		if(tar){
			printf("-1\n");
			continue;
		}
		memset(f,0,sizeof(f));
		tree_dp(1,1);
		if(1==c1){
			if(s1){
				printf("%d\n",f[1][2]);
			} else printf("%d\n",f[1][1]);
			continue;
		}
		if(1==c2){
			if(s2){
				printf("%d\n",f[1][2]);
			} else printf("%d\n",f[1][1]);
			continue;
		}
		printf("%d\n",min(f[1][1],f[1][2]));
}
return 0;
}
