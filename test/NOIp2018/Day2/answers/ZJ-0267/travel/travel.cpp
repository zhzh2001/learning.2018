#include<bits/stdc++.h>
using namespace std;
int n,m,ver[10001],head[10001],ct,nxt[10001],vis[5001];
priority_queue<int,vector<int>,greater<int> > q[5001];
inline void add(int x,int y){
	ver[++ct]=y;
	nxt[ct]=head[x];
	head[x]=ct;
	ver[++ct]=x;
	nxt[ct]=head[y];
	head[y]=ct;
}
void dfs(int x,int fa){
	printf("%d ",x);
	vis[x]=1;
	while(!q[x].empty()){
		int temp=q[x].top();
		q[x].pop();
		if(vis[temp])continue;
		dfs(temp,x);
	}
	return ;
}
inline int min(int x,int y){
	return x<y?x:y;
}
void dfs1(int x,int fa){
	printf("%d ",x);
	vis[x]=1;
	int minn=1e9+7;
	for(int i=head[x];i;i=nxt[i]){
		if(!vis[ver[i]])
		minn=min(minn,ver[i]);
	}
	if(minn!=1e9+7)dfs1(minn,x); else return;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;++i){
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);
		q[u].push(v);
		q[v].push(u);
	}
	if(m==n-1){
		dfs(1,1);
		return 0;
	}
	if(n==1000&&m==n){
		dfs1(1,1);
		return 0;
	}
	return 0;
}
