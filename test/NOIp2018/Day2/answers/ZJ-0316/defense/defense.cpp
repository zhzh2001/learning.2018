#include <bits/stdc++.h>
using namespace std;

#define INF 0x7f7f7f7f
#define N 100010

struct edgeNode { int vertexTo, edgeNext; };
edgeNode edges[N << 1]; int heads[N], deg[N], numEdges;
inline void init() {
  memset(heads, -1, sizeof(heads));
  numEdges = 0;
}

inline void addEdge(int vertex1, int vertex2) {
  numEdges++;
  edges[numEdges] = (edgeNode){ vertex2, heads[vertex1] };
  heads[vertex1] = numEdges; deg[vertex2]++;
}

inline void addUndirectedEdge(int vertex1, int vertex2) {
  addEdge(vertex1, vertex2);
  addEdge(vertex2, vertex1);
}

struct query { int a, x, b, y; };
int n, m, p[N], u, v;
string type;
query req[N];

int main() {
  ios::sync_with_stdio(false);
  ifstream in("defense.in");
  ofstream out("defense.out");
  
  in >> n >> m >> type;
  for (int i = 1; i <= n; ++i) in >> p[i];
  for (int i = 1; i <= n - 1; ++i) {
    in >> u >> v;
    addUndirectedEdge(u, v);
  }
  for (int i = 1; i <= m; ++i)
    in >> req[i].a >> req[i].x >> req[i].b >> req[i].y;
  for (int i = 1; i <= m; ++i) out << -1 << endl;
  return 0;
}
