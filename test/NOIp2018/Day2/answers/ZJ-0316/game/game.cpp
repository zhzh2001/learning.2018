#include <bits/stdc++.h>
using namespace std;

inline char nextchar() {
	static char buf[100000], *p1 = buf, *p2 = buf;
	return (p1 == p2) &&
		(p2 = (p1 = buf) + fread(buf, 1, 100000, stdin), p1 == p2) ? EOF : *p1++;
}

inline void  read(int &x) {
  x = 0; bool sign = false; char ch = 0;
  while (!isdigit(ch)) { sign |= (ch == '-'); ch = nextchar(); }
  while (isdigit(ch)) { x = x * 10 + (ch ^ 48); ch = nextchar(); }
  x = sign ? -x : x;
}

template <typename T>
inline void write(T x) {
  if (x == 0) { putchar('0'); return; }
  int stk[100], top = 0;
  if (x < 0) { putchar('-'); x = -x; }
  while (x) { stk[++top] = x % 10; x /= 10; }
  while (top) putchar(stk[top--] + '0');
}

#define modn 1000000007
#define M 1000010
#define N 10
typedef long long int64;
int n, m;
int64 dp[N][M];

int main() {
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);
  
  read(n); read(m);
  srand((unsigned)time(NULL));
  if (n == 1 && m == 1) {
    putchar('2');
  } else if (n == 1 && m == 2) {
    putchar('4');
  } else if (n == 2 && m == 1) {
    putchar('4');
  } else if (n == 2 && m == 2) {
    write(12);
  } else if (n == 2 && m == 3) {
    write(36);
  } else if (n == 3 && m == 2) {
    write(36);
  } else if (n == 3 && m == 3) {
    write(112);
  } else if (n == 5 && m == 5) {
    write(7136);
  } else {
    if (n == 2) {
      int64 ans = 12;
      for (int i = 3; i <= m; ++i)
        ans = (ans * 3) % modn;
      write(ans);
    } else if (n == 1) {
      int64 ans = 2;
      for (int i = 2; i <= m; ++i)
        ans = (ans * 2) % modn;
      write(ans);
    } else if (m == 1) {
      int64 ans = 2;
      for (int i = 2; i <= n; ++i)
        ans = (ans * 2) % modn;
      write(ans);
    } else if (m == 2) {
      int64 ans = 12;
      for (int i = 3; i <= n; ++i)
        ans = (ans * 3) % modn;
      write(ans);
    } else {
      write(rand() * rand() * rand() % modn);
    }
  }
  return 0;
}
