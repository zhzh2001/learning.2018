#include <bits/stdc++.h>
using namespace std;

inline char nextchar() {
	static char buf[100000], *p1 = buf, *p2 = buf;
	return (p1 == p2) &&
		(p2 = (p1 = buf) + fread(buf, 1, 100000, stdin), p1 == p2) ? EOF : *p1++;
}

inline void  read(int &x) {
  x = 0; bool sign = false; char ch = 0;
  while (!isdigit(ch)) { sign |= (ch == '-'); ch = nextchar(); }
  while (isdigit(ch)) { x = x * 10 + (ch ^ 48); ch = nextchar(); }
  x = sign ? -x : x;
}

inline void write(int x) {
  if (x == 0) { putchar('0'); return; }
  int stk[100], top = 0;
  if (x < 0) { putchar('-'); x = -x; }
  while (x) { stk[++top] = x % 10; x /= 10; }
  while (top) putchar(stk[top--] + '0');
}

#define N 5010
#define INF 0x7f7f7f7f

struct edgeNode { int vertexTo, edgeNext; };
edgeNode edges[N << 1]; int heads[N], deg[N], numEdges;
inline void init() {
  memset(heads, -1, sizeof(heads));
  numEdges = 0;
}

inline void addEdge(int vertex1, int vertex2) {
  numEdges++;
  edges[numEdges] = (edgeNode){ vertex2, heads[vertex1] };
  heads[vertex1] = numEdges; deg[vertex2]++;
}

inline void addUndirectedEdge(int vertex1, int vertex2) {
  addEdge(vertex1, vertex2);
  addEdge(vertex2, vertex1);
}

bool vis[N];
int rnk[N], cnt;

void dfs1(int x) {
  vis[x] = true;
  rnk[++cnt] = x;
  int num = 0;
  for (int i = heads[x]; i != -1; i = edges[i].edgeNext) {
    int y = edges[i].vertexTo;
    if (vis[y]) continue;
    num++;
  }
  int* node = (int*)malloc(num * sizeof(int));
  int idx = 0;
  for (int i = heads[x]; i != -1; i = edges[i].edgeNext) {
    int y = edges[i].vertexTo;
    if (vis[y]) continue;
    node[++idx] = y;
  }
  sort(node + 1, node + idx + 1);
  for (int i = 1; i <= idx; ++i) dfs1(node[i]);
}

int n, m, u, v;
int main() {
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);

  read(n); read(m);
  init();
  for (int i = 1; i <= m; ++i) {
    read(u); read(v);
    addUndirectedEdge(u, v);
  }
  dfs1(1);
  for (int i = 1; i <= cnt; ++i) write(rnk[i]), putchar(' ');
  return 0;
}
