#include<cstdio>
#include<iostream>
#define ll long long
using namespace std;

const int N=1e6+4;
char s[10];
int n,m;

namespace p1
{
	ll f[N][3],a[N];
	int cnt,to[N],nxt[N],he[N];
	
	inline void add(int u,int v)
	{
		to[++cnt]=v,nxt[cnt]=he[u],he[u]=cnt;
	}
	
	void dfs(int fa,int u,int aa,int x,int b,int y)
	{
		f[u][0]=f[u][1]=f[u][2]=0;
		for(int e=he[u];e;e=nxt[e])
		{
			int v=to[e];
			if(v!=fa) 
			{
				dfs(u,v,aa,x,b,y);
				f[u][1]+=f[v][2];
				f[u][0]+=f[v][2];
				f[u][2]+=min(min(f[v][0],f[v][1]),f[v][2]);
			}
		}
		f[u][2]+=a[u];
		if(!f[u][1]) f[u][1]=1e18;
		if(u==aa||u==b)
		{
			if(u==aa&&x||u==b&&y)  f[u][1]=f[u][0]=1e18;
			if(u==aa&&(!x)||u==b&&(!y)) f[u][2]=1e18;
		}
	}
	void main()
	{
		for(int i=1;i<=n;i++) scanf("%d",&a[i]);
		for(int i=1;i<n;i++) 
		{
			int u,v;
			scanf("%d%d",&u,&v);
			add(u,v),add(v,u);
		}
		while(m--)
		{
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(!x&&!y)
			{
				bool f=0;
				for(int i=he[a];i;i=nxt[i])
				{
					int v=to[i];
					if(v==b) 
					{
						f=1; break;
					}
				}
				if(f)  
				{
					puts("-1"); continue;
				}
			}
			dfs(0,1,a,x,b,y);
			printf("%d\n",min(f[1][2],f[1][1]));
		}
	}
}
int main()
{
	freopen("defense.in","r",stdin );
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s+1);
	if(n<=2000&&m<=2000) p1::main();
	fclose(stdin); fclose(stdout);
	return 0;
}
