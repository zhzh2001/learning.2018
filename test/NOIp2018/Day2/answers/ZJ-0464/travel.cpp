#include<cstdio>
#include<algorithm>
#include<iostream>
using namespace std;

const int N=1e5+5;
int n,m;

namespace p1
{
	int f[5005][5005],num[N],tot=0,ans[N];
	struct A{int u,v;}e[N];
	
	void dfs(int fa,int u)
	{
		ans[++tot]=u;
		for(int i=1;i<=num[u];i++)
		{
			int v=f[u][i];
			if(v!=fa) dfs(u,v);
		}
	}
	
	bool cmp(A i,A j)
	{
		return i.u<j.u||i.u==j.u&&i.v<j.v;
	}
	
	void main()
	{
		for(int i=1;i<=m;i++)
		{
			scanf("%d%d",&e[i].u,&e[i].v);
			e[i+m].u=e[i].v,e[i+m].v=e[i].u;
		}
		sort(e+1,e+m+m+1,cmp);
		for(int i=1;i<=(m<<1);i++) 
			f[e[i].u][++num[e[i].u]]=e[i].v;
		dfs(0,1);
		for(int i=1;i<=n;i++)
			printf("%d%c",ans[i],i==n?'\n':' ');
	}
}

namespace p2
{
	int f[5005][5005],num[N],tot=0,ans[N],b1[N],b2[N],c[N];
	int cnt,to[N],nxt[N],he[N],st[N],top=0,toa;
	bool fl[N],ff;
	struct A{int u,v;}e[N];
	
	bool cmp(A i,A j)
	{
		return i.u<j.u||i.u==j.u&&i.v<j.v;
	}

	void dfs(int fa,int u,int jj1,int jj2)
	{
		c[++tot]=u;
		for(int i=1;i<=num[u];i++)
		{
			int v=f[u][i];
			if(v!=fa&&(!(u==jj1&&v==jj2||u==jj2&&v==jj1))) dfs(u,v,jj1,jj2);
		}
	}
	
	inline void add(int u,int v)
	{
		to[++cnt]=v,nxt[cnt]=he[u],he[u]=cnt;
	}
	
	void tar(int fa,int u)
	{
		st[++top]=u; fl[u]=1;
		for(int e=he[u];e;e=nxt[e])
		{
			int v=to[e];
			if(v!=fa) 
			{
				if(!fl[v]) tar(u,v);
					else
					{
						ff=1;
						int lst=v;
						while(top&&st[top]!=v)
							b1[++toa]=lst,lst=st[top],b2[toa]=st[top--];
						b1[++toa]=lst,b2[toa]=v;
					}
				if(ff) break;
			}
		}
		top--;
	}
	void main()
	{
		for(int i=1;i<=m;i++)
		{
			scanf("%d%d",&e[i].u,&e[i].v);
			e[i+m].u=e[i].v,e[i+m].v=e[i].u;
			add(e[i].u,e[i].v),add(e[i].v,e[i].u);
		}
		tar(0,1);
		sort(e+1,e+m+m+1,cmp);
		for(int i=1;i<=(m<<1);i++) 
			f[e[i].u][++num[e[i].u]]=e[i].v;
		for(int i=1;i<=toa;i++) 
		{
			tot=0;
			dfs(0,1,b1[i],b2[i]);
			if(!ans[1]) for(int i=1;i<=n;i++) ans[i]=c[i];
				else 
				{
					bool fff=0;
					for(int i=1;i<=n;i++)
						if(c[i]<ans[i]) 
						{
							fff=1; break;
						}else if(c[i]>ans[i]) break;
					if(fff)
						for(int i=1;i<=n;i++) ans[i]=c[i];
				}
		}
		for(int i=1;i<=n;i++)
			printf("%d%c",ans[i],i==n?'\n':' ');
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m<n) p1::main();
		else p2::main();
	fclose(stdin); fclose(stdout);
	return 0;
}
