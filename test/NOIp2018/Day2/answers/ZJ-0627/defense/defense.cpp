#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
int n,m,x1,x2,y1,y2,minn1,minn2,minn3,ans,f[100010],a[100010],b[2010][2010],c[2010][2010];
char ch,ch1,ch2;
inline void read(int &x)
{
	x=0; ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
}
int abs(int x)
{
	if (x<0) return -x; else return x;
}

void ss1(int x,int y)
{
	if (y>=minn1) return;
	if (x==1)
	{
		minn1=min(minn1,y); return;
	}
	if (f[x]==0)
	{
		f[x-1]=1; ss1(x-1,y+a[x-1]); return;
	}
	if (f[x]==1)
	{
		f[x-1]=1; ss1(x-1,y+a[x-1]);
		f[x-1]=0; ss1(x-1,y); 
	}
}
void ss2(int x,int y)
{
	if (x==x2) return;
	if (y>=minn2) return;
	if (x==x2-1)
	{
		if (f[x2]==1&&f[x-1]==1) minn2=min(minn2,y);
		else minn2=min(minn2,y+a[x2-1]);
		return;
	}
	if (f[x-1]==0)
	{
		f[x]=1; ss2(x+1,y+a[x]); return;
	}
	f[x]=0; ss2(x+1,y);
	f[x]=1; ss2(x+1,y+a[x]);
}
void ss3(int x,int y)
{
	if (y>=minn3) return;
	if (x==n)
	{
		minn3=min(minn3,y); return;
	}
	if (f[x]==0)
	{
		f[x+1]=1; ss3(x+1,y+a[x+1]); return;
	}
	f[x+1]=0; ss3(x+1,y);
	f[x+1]=1; ss3(x+1,y+a[x+1]);
}

void sss1(int x,int y)
{
	if (y>=minn1) return;
	for (int i=1;i<=b[x][0];i++)
		if (f[b[x][i]]<0)
		{
			if (f[x]==0)
			{
				f[b[x][i]]=1; sss1(b[x][i],y+a[b[x][i]]); continue;
			}
			f[b[x][i]]=0; sss1(b[x][i],y);
			f[b[x][i]]=1; sss1(b[x][i],y+a[b[x][i]]);
		}
	if (x==x1) minn1=min(minn1,y);
}
void sss2(int x,int y)
{
	if (y>=minn2) return;
	if (c[x][x2])
	{
		if (f[x2]) minn2=min(minn2,y); else minn2=min(minn2,y+a[x]);
		return;
	}
	for (int i=1;i<=b[x][0];i++)
		if (f[b[x][i]]<0)
		{
			if (f[x]==0)
			{
				f[b[x][i]]=1; sss2(b[x][i],y+a[b[x][i]]); continue;
			}
			f[b[x][i]]=0; sss2(b[x][i],y);
			f[b[x][i]]=1; sss2(b[x][i],y+a[b[x][i]]);
		}
}
void sss3(int x,int y)
{
	if (y>=minn3) return;
	for (int i=1;i<=b[x][0];i++)
		if (f[b[x][i]]<0)
		{
			if (f[x]==0)
			{
				f[b[x][i]]=1; sss3(b[x][i],y+a[b[x][i]]); continue;
			}
			f[b[x][i]]=0; sss3(b[x][i],y);
			f[b[x][i]]=1; sss3(b[x][i],y+a[b[x][i]]);
		}
	if (x==x2) minn3=min(minn3,y);
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n); read(m);
	while (ch<'A'||ch>'C') ch=getchar();
	ch1=ch; ch2=getchar();
	for (int i=1;i<=n;i++) read(a[i]);
	if (ch1=='A')
	{
		for (int i=1;i<n;i++)
		{
			int x;
			read(x); read(x);
		}
		while (m--)
		{
			read(x1); read(y1); read(x2); read(y2);
			if (ch2=='1'&&y1==0&&y2==0)
			{
				printf("-1\n"); continue;
			}
		//	for (int i=1;i<=n;i++) f[i]=0;
			f[x1]=y1; f[x2]=y2;
			ans=y1*a[x1]+y2*a[x2];
			if (x1>x2)
			{
				int t=x1; x1=x2; x2=t;
			}
			minn1=1e9; minn2=1e9; minn3=1e9;
			ss1(x1,0);
			if (x1+1==x2) minn2=0; else ss2(x1+1,0);
			ss3(x2,0);
			printf("%d\n",ans+minn1+minn2+minn3);
		}
		return 0;
	}
	for (int i=1;i<n;i++)
	{
		int x,y; read(x); read(y);
		b[x][0]++; b[x][b[x][0]]=y;
		b[y][0]++; b[y][b[y][0]]=x;
		c[x][y]=1;
	}
	while (m--)
	{
		read(x1); read(y1); read(x2); read(y2);
		if (x1>x2)
		{
			int t=x1; x1=x2; x2=t;
			t=y1; y1=y2; y2=t;
		}
		if (x2-x1==1&&y1==0&&y2==0)
		{
			printf("-1\n"); continue;
		}
		for (int i=1;i<=n;i++) f[i]=-1;
		f[x1]=y1; f[x2]=y2;
		minn1=1e9; minn2=1e9; minn3=1e9;
		sss1(x1,0);
		if (x1+1==x2) minn2=0; else sss2(x1,0);
		sss3(x2,0);
		printf("%d\n",y1*a[x1]+y2*a[x2]+minn1+minn2+minn3);
		return 0;
	}
	return 0;
}
