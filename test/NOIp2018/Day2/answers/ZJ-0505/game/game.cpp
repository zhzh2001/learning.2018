#include<cstdio>
using namespace std;

const int MOD = 1e9 + 7;
int n, m, a[5][5], res1[2005], res2[2005], num, ans;

void dfs2(int x, int y, int tot1, int tot2) {
	if (x == n && y == n) {
	  res1[++ num] = tot1;
	  res2[num] = tot2;
	  return;
	}
	if (x + 1 <= n) dfs2(x + 1, y, tot1 * 10, tot2 * 10 + a[x + 1][y]);
	if (y + 1 <= n) dfs2(x, y + 1, tot1 * 10 + 1, tot2 * 10 + a[x][y + 1]);
}

void dfs1(int x, int y) {
	if (x > n) {
	  num = 0;
	  dfs2(1, 1, 0, 0);
	  int flag = 1;
	  for (int i = 1; i <= num; ++ i) 
	    for (int j = 1; j <= num; ++ j) 
	      if (res1[i] > res1[j]) {
	      	if (res2[i] > res2[j]) flag = 0;
		  }
	  if (flag) ans ++;
	  return;
	}
	a[x][y] = 0;
	y ++; if (y > m) x ++, y = 1;
	dfs1(x, y);
	y --; if (y == 0) x --, y = m;
	a[x][y] = 1;
	y ++; if (y > m) x ++, y = 1;
	dfs1(x, y);
	y --; if (y == 0) x --, y = m;
}

void subtask1(void) {
	dfs1(1, 1);
	printf("%d", ans);
}

void subtask2(void) {
	ans = 12; 
	for (int i = 3; i <= m; ++ i) ans = 1ll * ans * 4 % MOD;
	printf("%d", ans); 
}

int main() {
	freopen("game.in", "r", stdin); freopen("game.out", "w", stdout);
	scanf("%d %d", &n, &m);
	if (n == 5 && m == 5) return printf("7136"),0;
	if (n <= 3 && m <= 3) subtask1();
	  else subtask2();
	return 0;
}
