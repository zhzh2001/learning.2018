#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;

const int maxn = 100005;
int n, m, val[maxn], pos[maxn], num;
ll ans, dp[maxn][2], f[maxn][2];
char s[10];
int nxt[maxn * 2], hed[maxn * 2], cnt, to[maxn * 2];

int read(void) {
	char c; while (c = getchar(), c < '0' || c > '9'); int x = c - '0';
	while (c = getchar(), c >= '0' && c <= '9') x = x * 10 + c - '0'; return x;
}

void add(int x, int y) {
	nxt[++ cnt] = hed[x]; hed[x] = cnt; to[cnt] = y;
}

void treedp(int u, int pre) {
	int flag = 0;
	for (int i = hed[u]; i ; i = nxt[i]) {
	  int v = to[i];
	    if (v == pre) continue;
	  flag = 1; treedp(v, u);
	  dp[u][1] += min(dp[v][1], dp[v][0]);
	  dp[u][0] += dp[v][1];
	}
	dp[u][1] += val[u];
 	if (!flag) {
	   dp[u][0] = max(dp[u][0], (ll)0);
	   dp[u][1] = max(dp[u][1], (ll)val[u]);
	 }
}

int main() {
	freopen("defense.in", "r", stdin); freopen("defense.out", "w", stdout);
	n = read(); m = read(); scanf("%s", s + 1);
	  for (int i = 1; i <= n; ++ i) val[i] = read();
	  for (int i = 1; i < n; ++ i) {
	  	int x = read(), y = read();
	  	add(x, y); add(y, x);
	  }
	  while (m --) {
	  	int x = read(), bj1 = read(), y = read(), bj2 = read();
	  	for (int i = 1; i <= n; ++ i) dp[i][0] = dp[i][1] = 0;
	  	if ( !bj1 && !bj2) {
	  		int flag = 0;
	  		for (int i = hed[x]; i ; i = nxt[i]) 
	  	      if (to[i] == y) {
	  	  	      puts("-1"); flag = 1; break;
			    } 
			if (flag) continue;
		  }
		dp[x][bj1 ^ 1] = dp[y][bj2 ^ 1] = 1e18;
		treedp(1, 0);
		printf("%lld\n", min(dp[1][0], dp[1][1]));
	  }
	return 0;
}
