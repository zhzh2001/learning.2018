#include<cstdio>
using namespace std;

const int maxn = 5005;
int n, m, w[maxn][maxn], ans[maxn], tot, can[maxn], vis[maxn];

int read(void) {
	char c; while (c = getchar(), c < '0' || c > '9'); int x = c - '0';
	while (c = getchar(), c >= '0' && c <= '9') x = x * 10 + c - '0'; return x;
}

void dfs1(int u) {
	for (int i = 1; i <= n; ++ i) 
	  if (!vis[i] && w[u][i]) { 
	  	vis[i] = 1; ans[++ tot] = i;
	  	dfs1(i);
	  } 
}

void subtask1(void) {
	vis[1] = 1; ans[++ tot] = 1;
	dfs1(1);
}

void dfs(int dep, int u) {
	for (int i = 1; i <= n; ++ i) {
	  if (vis[i] <= 2 && w[u][i]) {
	  	int add = 0;
	  	if (!vis[i]) {
	  		ans[dep] = i; add = 1;
		  }
	  	vis[i] ++;
	  	dfs(dep + add, i);
	  	vis[i] --;
	  }
	}
}

void subtask2(void) {
	vis[1] = 1; ans[1] = 1;
	dfs(2, 1);
}

int main() {
	freopen("travel.in", "r", stdin); freopen("travel.out", "w", stdout);
	n = read(); m = read();
	  for (int i = 1; i <= m; ++ i) {
	  	int x = read(), y = read();
	  	w[x][y] = w[y][x] = 1;
	  }
	  if (m == n - 1) subtask1();
	    else subtask2();
	  for (int i = 1; i <= n; ++ i) printf("%d ", ans[i]);
	return 0;
}
