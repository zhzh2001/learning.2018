#include<stdio.h>
#include<map>
#include<cstdio>
#include<queue>
#include<algorithm>
#include<iostream>
#include<complex>
#include<vector>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1e6+5,Mod=1e9+7; int n,m,f[N];
inline int Read(){
	char c=getchar(); int t=0;
	for (;c>57||c<48;c=getchar());
	for (;c>47&&c<58;c=getchar()) t=t*10+c-48; return t;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=Read(),m=Read();
	if (n==1){
		f[1]=2;
		for (int i=2;i<=m;i++) f[i]=f[i-1]*2%Mod;
		printf("%d\n",f[m]);
	}
	if (n==2){f[1]=4;
		for (int i=2;i<=m;i++) f[i]=1ll*f[i-1]*3ll%Mod;
		printf("%d\n",f[m]);
	}
	if (n==3){
		f[1]=8;
		f[2]=36;
		f[3]=112;
		for (int i=4;i<=m;i++) f[i]=1ll*f[i-1]*3ll%Mod;
		printf("%d\n",f[m]);
	}
	if (n==4){
		f[1]=16;
		f[2]=108;
		f[3]=336;
		f[4]=912;
		f[5]=2688;
		for (int i=6;i<=m;i++) f[i]=1ll*f[i-1]*3ll%Mod;
		printf("%d\n",f[m]);
	}
	if (n==5){
		f[1]=32;
		f[2]=324;
		f[3]=1008;
		f[4]=2688;
		f[5]=7136;
		printf("%d\n",f[m]);
	}
}	
