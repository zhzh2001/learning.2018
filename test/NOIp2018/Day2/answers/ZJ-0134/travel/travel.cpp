#include<stdio.h>
#include<map>
#include<cstdio>
#include<queue>
#include<algorithm>
#include<iostream>
#include<complex>
#include<vector>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=5005;
int flag,tot,vis[N],g[N],cant[N],fa[N],a[N],n,m,ll[N];
vector<int> Map[N];
inline int Read(){
	char c=getchar(); int t=0;
	for (;c>57||c<48;c=getchar());
	for (;c>47&&c<58;c=getchar()) t=t*10+c-48; return t;
}
int Dfs(int p){//printf("Dfs:%d\n",p);
	if (!flag) return 0;
	if (!vis[p]) vis[p]=1,g[++tot]=p;
	else{flag=0; int x=1;
		for (;x<=tot&&g[x]!=p;x++);
	//	for (int i=x;i<=tot;i++) printf("%d ",g[i]); printf("+++++++++++++++++++++++++++\n");
		if (g[++x]<g[tot]){//printf("This");
			for (;x<=tot&&g[x]<g[tot];x++);//printf("x=======%d\n",g[x]);
			if (x<=tot) cant[g[x-1]]=g[x],cant[g[x]]=g[x-1];
			else cant[g[tot]]=p,cant[p]=g[tot];
		}
		else{int x2=tot;
			for (;x2>=x&&g[x2]<g[x];x2--);
			if (x2>=x) cant[g[x2+1]]=g[x2],cant[g[x2]]=g[x2+1];
			else cant[g[x]]=p,cant[p]=g[x];
		}
//		printf("__________________%d\n",cant[2]);
//		printf("Work");
		return 0;
	}
	int num=Map[p].size();
	for (int i=0;i<num;i++){
	//	printf("%d %d\n",p,Map[p][i]);
		if (Map[p][i]!=fa[p])
	//		printf("in"),
			fa[Map[p][i]]=p,Dfs(Map[p][i]);
	}
	tot--;
}
int Work(int p,int F){int num=Map[p].size(); a[++tot]=p;
//	printf("%d %d %d\n",p,F,tot);
	for (int i=0;i<num;i++){int poi=Map[p][i];
	//	printf("in:%d====>%d\n",p,poi);
		if (poi!=F&&poi!=cant[p]) Work(poi,p);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=Read(),m=Read(); flag=1;
	for (int i=1;i<=m;i++){
		int x=Read(),y=Read();
		Map[x].push_back(y),Map[y].push_back(x);
	}
	for (int i=1;i<=n;i++){int num=Map[i].size();
		for (int j=0;j<num;j++) ll[j]=Map[i][j];
		sort(ll,ll+num); Map[i].clear();
		for (int j=0;j<num;j++) Map[i].push_back(ll[j]);
	}
	tot=0;
	if (n==m) Dfs(1);// printf("sd"); 
	tot=0,Work(1,0);
	for (int i=1;i<n;i++) printf("%d ",a[i]);
	printf("%d\n",a[n]); return 0;
}	
