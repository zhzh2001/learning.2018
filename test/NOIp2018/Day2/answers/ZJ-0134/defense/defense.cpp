#include<stdio.h>
#include<map>
#include<cstdio>
#include<queue>
#include<algorithm>
#include<iostream>
#include<complex>
#include<vector>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1e5+5,MAXN=100000000;
struct T{int next,to;} edge[N<<1];
int head[N],nedge,f[N][2],n,m,T,a[N],Ans,tot,num,vis[N],cho[N]; char c;
inline int Read(){
	char c=getchar(); int t=0;
	for (;c>57||c<48;c=getchar());
	for (;c>47&&c<58;c=getchar()) t=t*10+c-48; return t;
}
inline int addline(int x,int y){
	edge[++nedge].next=head[x],edge[nedge].to=y,head[x]=nedge;
}
int Dfs(int p,int F){
	for (int i=head[p];i;i=NX)
		if (O!=F) Dfs(O,p);
		
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=Read(),T=Read();
	c=getchar(); printf("%c\n",c); m=Read();
	for (int i=1;i<=n;i++) a[i]=Read();
	for (int i=1;i<n;i++){
		int x=Read(),y=Read();
		addline(x,y),addline(y,x);
	}
	for (int i=1;i<=T;i++){Ans=MAXN;
		int xx=Read(),xxx=Read(),yy=Read(),yyy=Read();
		for (int j=0;j<1<<n;j++){tot=num=0;
			for (int k=1;k<=n;k++) vis[k]=0;
			for (int k=1;k<=n;k++) cho[k]=((1<<(k-1))&j)>0;
		//	for (int k=1;k<=n;k++) printf("%d ",cho[k]); printf("\n");
			for (int k=1;k<=n;k++) num+=a[k]*cho[k];
			for (int k=1;k<=n;k++)
				if (cho[k]&&(!vis[k])) vis[k]=1,tot++;
			for (int k=1;k<=n;k++)
				if (cho[k])
					for (int l=head[k];l;l=edge[l].next)
						if (!vis[edge[l].to]) tot++,vis[edge[l].to]=1;
		//	printf("%d\n",tot);
			if (tot==n&&cho[xx]==xxx&&cho[yy]==yyy) Ans=min(Ans,num);//printf("%d\n",num);
		}
		if (Ans==MAXN) printf("-1\n");
		else printf("%d\n",Ans);
	}
}
