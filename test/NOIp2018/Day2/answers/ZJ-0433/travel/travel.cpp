#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

int n, m, map[5001][5001], f[5001], b[5001], cnt;

void dfs(int x)
{
	f[++cnt] = x;
	if (cnt == n) return;
	b[x] = 1;
	for (int i = 1; i <= n; i++)
		if (!b[i] && map[x][i]) dfs (i);
	b[x] = 0;
}

int main()
{
	freopen ("travel.in", "r", stdin);
	freopen ("travel.out", "w", stdout);
	
	scanf ("%d%d", &n, &m);
	
	memset (f, 0x3f, sizeof (f));
	for (int i = 1; i <= m; i++)
	{
		int a1, b1;
		scanf ("%d%d", &a1, &b1);
		map[a1][b1] = 1;
		map[b1][a1] = 1;
	}
	
	dfs (1);
	
	for (int i = 1; i <= n; i++) printf ("%d ", f[i]);
	
	return 0;
}
