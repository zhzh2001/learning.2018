#include <bits/stdc++.h>
using namespace std;
#define Rint register int
typedef long long ll;
const ll MOD=1e9+7;
inline ll read(){
	ll x=0,w=1;char c=getchar();
	while(!isdigit(c)){
		if(c=='-')w=-1;
		c=getchar();
	}
	while(isdigit(c)){
		x=(x<<1)+(x<<3)+c-'0';
		c=getchar();
	}
	return x*w;
}
ll n,m;
ll f[9][100000];
ll pw2(ll u){
	ll res=1,x=2;
	while(u){
		if(u&1)res*=x,res%=MOD;
		x=(x<<1)%MOD;
		u>>=1;
	}
	return res;
}
ll f2[2000010][4];
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if(n==1){
		printf("%lld\n",pw2(m));
		return 0;
	}
	if(m==1){
		printf("%lld\n",pw2(n));
		return 0;
	}
	if(n==3&&m==3){
		puts("112");
		return 0;
	}else if(n==5&&m==5){
		puts("7136");
		return 0;
	}
	if(n==2){
		f2[1][0]=f2[1][1]=f2[1][2]=f2[1][3]=1;
		for (Rint i=2;i<=m;i++){
			f2[i][0]=f2[i][1]=(f2[i-1][0]+f2[i-1][2])%MOD;
			f2[i][2]=f2[i][3]=(f2[i-1][0]+f2[i-1][1]+f2[i-1][2]+f2[i-1][3])%MOD;
		}
		printf("%lld\n",(f2[m][0]+f2[m][1]+f2[m][2]+f2[m][3])%MOD);
		return 0;
	}
	for (Rint i=0;i<m;i++)
		f[n][i]=2;
	for (Rint i=1;i<=m;i++)
		f[n][i]+=f[n][i-1];
	if(n==1)goto ed;
	for (Rint i=n-1;i>=2;i--){
		f[i][0]=f[i+1][m];
		f[i][1]=f[i][0]+f[i+1][m];
		for (Rint j=2;j<=m;j++)
			f[i][j]=f[i+1][m]-f[i+1][j-2]+f[i][j-1];
	}
	f[1][1]=f[2][m]<<1;
	for (Rint i=2;i<=m;i++){
		f[1][i]=(f[2][m]-f[2][i-2])<<1;
		f[1][i]+=f[1][i-1];
	}
	ed:
	printf("%lld\n",f[1][m]);
	return 0;
}
