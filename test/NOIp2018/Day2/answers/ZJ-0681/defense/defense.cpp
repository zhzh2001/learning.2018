#include <bits/stdc++.h>
using namespace std;
#define Rint register int
typedef long long ll;
const ll INF=0x3f3f3f3f3f3f3f3f;
inline ll read(){
	ll x=0,w=1;char c=getchar();
	while(!isdigit(c)){
		if(c=='-')w=-1;
		c=getchar();
	}
	while(isdigit(c)){
		x=(x<<1)+(x<<3)+c-'0';
		c=getchar();
	}
	return x*w;
}
ll hd[100010],eds[200010],nxt[200010],et=0;
ll val[100010];
inline void ad(ll u,ll v){
	eds[++et]=v;
	nxt[et]=hd[u];
	hd[u]=et;
}
ll n,m;
string gt(){
	string a,b;
	cin>>a;
	b=a[0]+a[1];
	return b;
}
string tp;
ll cp[102400];
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),tp=gt();
	ll t1,t2;
	for (Rint i=1;i<=n;i++)
		val[i]=read();
	for (Rint i=1;i<n;i++){
		t1=read(),t2=read();
		ad(t1,t2);
		ad(t2,t1);
	}
	if(n<=10){
		for (Rint i=0;i<(1<<n);i++){
			for (Rint j=1;j<=n;j++)
				if(i&(1<<(j-1))){
					cp[i]+=val[j];
				}else{
					bool cd=false;
					for (Rint k=hd[j];k;k=nxt[k])
						cd|=(i&(1<<(eds[k]-1)));
					if(!cd){
						cp[i]=INF;
						goto ed;
					}
				}
			ed:
				while(false);
		}
		ll t1,t2,t3,t4;
		
		for (Rint j=1;j<=m;j++){
			ll minn=INF;
			t1=read(),t2=read(),t3=read(),t4=read();
			for (Rint i=0;i<(1<<n);i++)
				if(((i&(1<<(t1-1)))>0)==t2&&((i&(1<<(t3-1)))>0)==t4)
					minn=min(minn,cp[i]);
			printf("%lld\n",minn==INF?-1:minn);
		}
		
	}
	return 0;
}
