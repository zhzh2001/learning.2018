var
  n,m,i,j,k,t,x,y,first,q,num,ff,ttt:longint;
  a:array[1..5000,1..5000] of longint;
  ans,p,vis,tt,fa,f,h,b:array[1..5000] of longint;
    procedure qsort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=b[(l+r) div 2];
         repeat
           while b[i]<x do
            inc(i);
           while x<b[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=b[i];
                b[i]:=b[j];
                b[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           qsort(l,j);
         if i<r then
           qsort(i,r);
      end;
procedure dfs1(x:longint);
  var i,j,l:longint;
  begin
    inc(k);
    ans[k]:=x;
    for i:=1 to n do
      if a[x,i]=1
        then
          if vis[i]=0
            then
              begin
                vis[i]:=1;
                dfs1(i);
              end
            else
              if (first=i)and(h[x]=0)
                then
                  begin
                    k:=k+1;
                    ans[k]:=first;
                    for j:=2 to num do
                      b[j-1]:=f[j];
                    ff:=num-1;
                    for j:=2 to num do
                      begin
                        for l:=1 to n do
                          if (a[f[j],l]=1)and(vis[l]=0)
                            then
                              begin
                                inc(ff);
                                b[ff]:=l;
                              end;
                      end;
                    qsort(1,ff);
                    for j:=1 to ff do
                      if h[b[j]]=1
                        then
                          begin
                            inc(k);
                            ans[k]:=b[j];
                          end
                        else dfs1(b[j]);
                    for j:=num+1 to q do
                      b[j-num]:=f[j];
                    ff:=q-num; 
                    for j:=num+1 to q-1 do
                      begin
                        for l:=1 to n do
                          if (a[f[j],l]=1)and(vis[l]=0)
                            then
                              begin
                                inc(ff);
                                b[ff]:=l;
                              end;
                      end;
                    qsort(1,ff);
                    for j:=1 to ff do
                      if h[b[j]]=1
                        then
                          begin
                            inc(k);
                            ans[k]:=b[j];
                          end
                        else dfs1(b[j]);
                  end;
  end;
function dfs(x:longint):longint;
  var i,j,t:longint;
  begin
    dfs:=0;
    tt[x]:=1;
    for i:=1 to n do
      if a[x,i]=1
        then
          if tt[i]=0
            then
              begin
                fa[i]:=x;
                t:=dfs(i);
                if t>0
                  then
                    begin
                      inc(q);
                      p[q]:=x;
                      h[x]:=1;
                      vis[x]:=1;
                      if x=first
                        then exit(-1)
                        else exit(t);
                    end
                  else
                    if t<0
                      then exit(-1);
              end
            else
              if fa[x]<>i
                then
                  begin
                    first:=i;
                    h[x]:=1;
                    q:=1;
                    p[1]:=x;
                    vis[x]:=1;
                    exit(i);
                  end;
  end;
begin
  assign(input,'travel.in');
  reset(input);
  assign(output,'travel.out');
  rewrite(output);  
  first:=0;
  readln(n,m);
  for i:=1 to m do
    begin
      readln(x,y);
      a[x,y]:=1;
      a[y,x]:=1;
    end;
  if m=n-1
    then
      begin
        k:=0;
        for i:=1 to n do
          vis[i]:=0;
        vis[1]:=1;
        dfs1(1);
        for i:=1 to n do
          write(ans[i],' ');
      end
    else
      begin
        for i:=1 to n do
          vis[i]:=0;
        for i:=1 to n do
          tt[i]:=0;
        dfs(1);
        x:=p[1];
        y:=p[q-1];
        f[1]:=first;
        if x>y
          then
            begin
              ttt:=1;
              k:=q-2;
              f[2]:=y;
              while (k>0)and(p[k]<x) do
                begin
                  f[q-k+1]:=p[k];
                  dec(k);
                end;
              num:=k;
              for i:=1 to k do
                f[q-k+i]:=p[i];
            end
          else
            begin
              ttt:=2;
              k:=2;
              f[2]:=x;
              while (k<=q)and(p[k]<y) do
                begin
                  f[k+1]:=p[k];
                  inc(k);
                end;
              num:=k;
              for i:=q-1 downto k do
                f[k+q-i]:=p[i];
            end;
        k:=0;
        vis[1]:=1;
        ans[1]:=1;
        dfs1(1);
        for i:=1 to n do
          write(ans[i],' ');
      end;
  close(input);
  close(output);
end.
