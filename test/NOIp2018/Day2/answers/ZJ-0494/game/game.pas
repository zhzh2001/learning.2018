var
  n,m,i,j:longint;
  ans:int64;
begin
  assign(input,'game.in');
  reset(input);
  assign(output,'game.out');
  rewrite(output);
  readln(n,m);
  ans:=2;
  for i:=1 to n do
    ans:=(ans*6) mod 100000007;
  writeln(ans);
  close(input);
  close(output);
end.