#include<bits/stdc++.h>
#define FOR(i,a,b) for(int i=(a),i##_end_=(b);i<=i##_end_;++i)
#define DOR(i,a,b) for(int i=(a),i##_end_=(b);i>=i##_end_;--i)
#define SFOR(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;++i)
#define SDOR(i,a,b) for(int i=(a)-1,i##_end_=(b);i>=i##_end_;--i)
#define debug(x) cerr<<#x<<"="<<x<<endl
#define bug() cerr<<"Surprise MotherFucker"<<endl
#define fi first
#define se second
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int P=1e9+7;
#define M 1000086
ll fact[M];
ll fast(ll a,int b){
	ll res=1,x=a;
	while(b){
		if(b&1)res=res*x%P;
		b>>=1;
		x=x*x%P;
	}
	return res;
}
void Init(){
	fact[0]=1;
	SFOR(i,1,M)fact[i]=fact[i-1]*i%P;
}
ll C(int n,int m){
	return fact[n]*fast(fact[m],P-2)%P*fast(fact[n-m],P-2);
}
int n,m;
struct P1{
	void solve(){
		printf("%lld\n",fast(2,m));
	}
}p1;
struct P2{
	void solve(){
		ll ans=0;
		FOR(i,0,m-1){
			ans=(ans+C(m-1,i)*fast(2,i)%P)%P;
		}
		cout<<ans*4%P<<endl;
	}
}p2;
struct P3{
	int pos[1024][7];
	bool Ok[1024];
	void solve(){
		int t=n*m;
		ll ans=0;
		int len=n+m-2;
		SFOR(s,0,1<<len){
			Ok[s]=true;
			int nx=0,ny=0;
			SFOR(j,0,len){
				if(1<<j&s)ny++;
				else nx++;
				if(nx>=n||ny>=m){
					Ok[s]=false;
					break;
				}
				pos[s][j]=nx*m+ny;
			}
		}
		SFOR(s,0,1<<t){
			bool f=true;
			SFOR(j,0,1<<len){
				if(!Ok[j])continue;
				int tmp1=0;
				SFOR(a,0,len)tmp1|=((1<<pos[j][a]&s)>0)<<a;
				SFOR(k,0,j){
					if(!Ok[k])continue;
					int tmp2=0;
					SFOR(b,0,len)tmp2|=((1<<pos[k][b]&s)>0)<<b;
					if(tmp1>tmp2){
						f=false;
						break;
					}
				}
				if(!f)break;
			}
			ans+=f;
		}
		cout<<ans<<endl;
	}
}p3;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	Init();
	cin>>n>>m;
	if(n==1)p1.solve();
	else if(n==2)p2.solve();
	else if(m<=3)p3.solve();
	return 0;
}
