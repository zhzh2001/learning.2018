#include<bits/stdc++.h>
#define FOR(i,a,b) for(int i=(a),i##_end_=(b);i<=i##_end_;++i)
#define DOR(i,a,b) for(int i=(a),i##_end_=(b);i>=i##_end_;--i)
#define SFOR(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;++i)
#define SDOR(i,a,b) for(int i=(a)-1,i##_end_=(b);i>=i##_end_;--i)
#define debug(x) cerr<<#x<<"="<<x<<endl
#define bug() cerr<<"Surprise MotherFucker"<<endl
#define fi first
#define se second
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
#define M 5050
vector<int>edge[M];
int n,m;
struct P60{
	int ans[M],tot;
	void dfs(int x,int f){
		ans[++tot]=x;
		SFOR(i,0,edge[x].size()){
			int y=edge[x][i];
			if(y==f)continue;
			dfs(y,x);
		}
	}
	void solve(){
		tot=0;
		FOR(i,1,n)sort(edge[i].begin(),edge[i].end());
		dfs(1,0);
		FOR(i,1,n)printf("%d%c",ans[i],i==n?'\n':' ');
//		puts("");
	}
}p60;
int dfn[M],low[M],T;
int id[M],size[M],S;
int Stack[M],top;
void Tarjan(int x,int f){
	dfn[x]=low[x]=++T;
	Stack[++top]=x;
	SFOR(i,0,edge[x].size()){
		int y=edge[x][i];
		if(y==f)continue;
		if(!dfn[y]){
			Tarjan(y,x);
			low[x]=min(low[x],low[y]);
		}else low[x]=min(low[x],dfn[y]);
	}
	if(dfn[x]==low[x]){
		++S;
		do id[Stack[top]]=S,++size[S];
		while(Stack[top--]!=x);
	}
}
bool Inloop[M];
bool vis[M];
int deg[M];
struct P100{
	int ans[M],tot,st;
	int A[M],len;
	bool flag;
	int tmp[M];
	void pre1(int x,int f){
		if(st)return;
		ans[++tot]=x;
		SFOR(i,0,edge[x].size()){
			int y=edge[x][i];
			if(y==f)continue;
			if(Inloop[y]){
				st=y;
				return;
			}
			pre1(y,x);
		}
	}
	void Find_loop(int x,int f){
		if(!(x==1&&flag))A[++len]=x;
		vis[x]=1;
		SFOR(i,0,edge[x].size()){
			int y=edge[x][i];
			if(y==f)continue;
			if(!Inloop[y]||vis[y])continue;
			Find_loop(y,x);
		}
	}
	void dfs(int x,int f){
		vis[x]=1;
		ans[++tot]=x;
		SFOR(i,0,edge[x].size()){
			int y=edge[x][i];
			if(vis[y]||Inloop[y])continue;
			dfs(y,x);
		}
	}
	bool check(){
		if(ans[1]==0)return true;
		FOR(i,1,n){
			if(ans[i]>tmp[i])return true;
			if(ans[i]<tmp[i])return false;
		}
		return false;
	}
	void solve(){
		tot=0;
		FOR(i,1,n)sort(edge[i].begin(),edge[i].end());
		Tarjan(1,0);
		FOR(i,1,n)if(size[id[i]]>1)Inloop[i]=1;
		bool p12=true;
		FOR(i,1,n)if(deg[i]!=2)p12=false;
		if(p12){
			Find_loop(1,0);
			if(A[2]>A[len])reverse(A+1,A+len+1);
			FOR(t,2,len){
				int tp=0;
				FOR(i,1,t)tmp[++tp]=A[i];
				DOR(i,len,t+1)tmp[++tp]=A[i];
				if(check())FOR(i,1,n)ans[i]=tmp[i];
			}
		}
		FOR(i,1,n)printf("%d%c",ans[i],i==n?'\n':' ');
	}
}p100;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	FOR(i,1,m){
		int a,b;
		scanf("%d%d",&a,&b);
		edge[a].push_back(b);
		edge[b].push_back(a);
		++deg[a],++deg[b];
	}
	if(m==n-1)p60.solve();
	else p100.solve();
	return 0;
}
