#include<bits/stdc++.h>
#define FOR(i,a,b) for(int i=(a),i##_end_=(b);i<=i##_end_;++i)
#define DOR(i,a,b) for(int i=(a),i##_end_=(b);i>=i##_end_;--i)
#define SFOR(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;++i)
#define SDOR(i,a,b) for(int i=(a)-1,i##_end_=(b);i>=i##_end_;--i)
#define debug(x) cerr<<#x<<"="<<x<<endl
#define bug() cerr<<"Surprise MotherFucker"<<endl
#define fi first
#define se second
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
#define M 100086
int n,m;
char str[10];
int val[M];
struct Node{
	int to,nxt;
}G[M<<1];
int head[M],Tot;
void add_edge(int a,int b){
	G[Tot]=(Node){b,head[a]};
	head[a]=Tot++;
}
const ll INF=1e15;
struct P44{
	int Fa[2050],A[2050],tot;
	bool mark[2050];
	ll dp[2055][2];
	ll DP[2055][2];
	void DFS(int x,int f){
		Fa[x]=f;
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(y==f)continue;
			DFS(y,x);
		}
	}
	void dfs(int x,int f){
		dp[x][0]=0,dp[x][1]=val[x];
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(mark[y]||y==f)continue;
			dfs(y,x);
			dp[x][0]+=dp[y][1];
			dp[x][1]+=min(dp[y][0],dp[y][1]);
		}
	}
	void solve(){
		while(m--){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			DFS(a,0);
			if(Fa[b]==a&&!x&&!y){
				puts("-1");
				continue;
			}
			tot=0;
			memset(mark,0,sizeof(mark));
			for(int i=b;i;i=Fa[i]){
				A[++tot]=i;
				mark[i]=1;
			}
			FOR(i,1,tot)dfs(A[i],0);
			dp[b][!y]=INF;
//			debug(dp[a][0]);
			FOR(i,1,tot){
				DP[i][0]=DP[i-1][1]+dp[A[i]][0];
				DP[i][1]=min(DP[i-1][1],DP[i-1][0])+dp[A[i]][1];
			}
			printf("%lld\n",DP[tot][x]);
		}
	}
}p44;
struct P52{
	ll Ldp[M][2],Rdp[M][2];
	int Abs(int x){
		return x>0?x:-x;
	}
	void solve(){
		FOR(i,1,n){
			Ldp[i][0]=Ldp[i-1][1];
			Ldp[i][1]=min(Ldp[i-1][1],Ldp[i-1][0])+val[i];
		}
		DOR(i,n,1){
			Rdp[i][0]=Rdp[i+1][1];
			Rdp[i][1]=min(Rdp[i+1][1],Rdp[i+1][0])+val[i];
		}
		while(m--){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(!x&&!y){
				puts("-1");
				continue;
			}
			if(a>b)swap(a,b),swap(x,y);
			printf("%lld\n",Ldp[a][x]+Rdp[b][y]);
		}
	}
}p52;
struct P60{
	ll Ldp[M][2],Rdp[M][2];
	void solve(){
		Ldp[1][1]=val[1];
		Ldp[1][0]=INF;
		FOR(i,2,n){
			Ldp[i][0]=Ldp[i-1][1];
			Ldp[i][1]=min(Ldp[i-1][1],Ldp[i-1][0])+val[i];
		}
		DOR(i,n,1){
			Rdp[i][0]=Rdp[i+1][1];
			Rdp[i][1]=min(Rdp[i+1][1],Rdp[i+1][0])+val[i];
		}
		while(m--){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(b==2&&!x&&!y){
				puts("-1");
				continue;
			}
			if(y)printf("%lld\n",Rdp[b][1]+min(Ldp[b-1][0],Ldp[b-1][1]));
			else printf("%lld\n",Rdp[b][0]+Ldp[b-1][1]);
		}
	}
}p60;
int main(){
	memset(head,-1,sizeof(head));
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,str);
	FOR(i,1,n)scanf("%d",&val[i]);
	FOR(i,2,n){
		int a,b;
		scanf("%d%d",&a,&b);
		add_edge(a,b);
		add_edge(b,a);
	}
	if((n<=2000&&m<=2000)||str[0]=='B')p44.solve();
	else if(str[0]=='A'&&str[1]=='2')p52.solve();
	else if(str[0]=='A'&&str[1]=='1')p60.solve();
	return 0;
}
