#include<bits/stdc++.h>
#define res register int
#define ll long long
#define inf 0x3f3f3f3f
#define N 100050
using namespace std;
int n,m,p;
int f[N][2],a[N],head[N*2],fa[N],tot,q[N];
char c;
struct data{
	int to,nxt;
}edge[N*2];

inline void dfs(int root,int father)
{
	fa[root]=father;
	q[father]++;
	for(res i=head[root];i;i=edge[i].nxt)
	{
		int y=edge[i].to;
		if(father!=y) 
		dfs(y,root);
	}
}

inline void add(int x,int y)
{
	edge[++tot].to=y;
	edge[tot].nxt=head[x];
	head[x]=tot;
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%c",&c);
	scanf("%c",&c);
	scanf("%d",&p);
		for(res i=1;i<=n;i++)
		scanf("%d",&a[i]);
		for(res i=1;i<=n-1;i++)
		{
			int l,r;
			scanf("%d%d",&l,&r);
			add(l,r);
			add(r,l);
		}
	dfs(1,1);
	for(res i=1;i<=m;i++)
	{
		int x,y,l,r;
		scanf("%d%d%d%d",&x,&y,&l,&r);
		memset(f,inf,sizeof(f));
		f[0][0]=f[0][1]=0;
		if(fa[x]==l||fa[l]==x&&!y&&!r)
		printf("-1\n");
		else 
		{
			for(res i=1;i<=n;i++)
			{
			  if(i==x&&y==1)
			  f[i][1]=min(f[i-1][0],f[i-1][1])+a[i],f[i][0]=inf;
			   else if(i==x&&y==0)
			  f[i][0]=f[i-1][1],f[i][1]=inf;
			  else  if(i==l&&r==1)
			  f[i][1]=min(f[i-1][0],f[i-1][1])+a[i],f[i][0]=inf;
			   else if(i==l&&r==0)
			  f[i][0]=f[i-1][1],f[i][1]=inf;
			  else 
			  f[i][0]=f[i-1][1],f[i][1]=min(f[i-1][0],f[i-1][1])+a[i];
			  
		    }
		    if(f[n][0]>=inf&&f[n][1]>=inf)
		    printf("-1\n");
		    else 
		    printf("%d\n",min(f[n][1],f[n][0]));
		}
	}
}
		
