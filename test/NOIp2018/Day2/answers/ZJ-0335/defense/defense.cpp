#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define inf 0x3f3f3f3f
#define xep(i,x) for(int i=head[x];i;i=nxt[i])

using namespace std;

const int N=100010;
int n,m,cnt=0,ans=inf,res=0;
int w[N],vis[N],dp[N][2],tag[N];
int head[N],nxt[N<<1],to[N<<1];
int X[N],Y[N];
char opt[2];

inline void add(int x,int y){
	to[++cnt]=y;nxt[cnt]=head[x];
	head[x]=cnt;
}

inline bool check(){
	for(int i=1;i<=n;i++)
		xep(j,i){
			bool fg=0;
			if(vis[to[j]]==1||vis[i]==1) fg=1;
			if(fg==0) return false;
		}
	return true;
}

inline void dfs(int x){
	if(x>=n+1){
		if(check()){
			res=0;
			for(int i=1;i<=n;i++)
				if(vis[i]==1) res+=w[i];
			ans=min(ans,res);
		}
		return ;
	} 
	if(vis[x]==0){
		vis[x]=1;dfs(x+1);
		vis[x]=0;dfs(x+1);
	}
	else dfs(x+1);
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,opt);
	for(int i=1;i<=n;i++)
		scanf("%d",&w[i]);
	for(int i=1;i<n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		X[i]=x;Y[i]=y;
		add(x,y);add(y,x);
	}
	if(n<=30){
		while(m--){
			int x,a,y,b;
			bool fg=0;
			memset(vis,0,sizeof(vis));
			scanf("%d%d%d%d",&x,&a,&y,&b);
			for(int i=1;i<n;i++)
				if((X[i]==x&&Y[i]==y)||(X[i]==y&&Y[i]==x))
					if(a==0&&b==0){
						puts("-1");fg=1;
						break;
					}
			if(fg==0){
			if(a==1) vis[x]=a;else vis[x]=2;
			if(b==1) vis[y]=b;else vis[y]=2;
			dfs(1);
			printf("%d\n",ans);
		}
		}
	}
	if(opt[0]=='A'){
		while(m--){
			memset(dp,0,sizeof(dp));
			int x,a,y,b;
			scanf("%d%d%d%d",&x,&a,&y,&b);
			if(abs(x-y)==1&&a==0&&b==0) puts("-1");
			else {
				for(int i=1;i<=n;i++)
					if(i!=x&&i!=y) 
						dp[i][0]=dp[i-1][1],dp[i][1]=min(dp[i-1][0],dp[i-1][1])+w[i];
					else if(i==x)
						if(a==1) dp[i][1]=min(dp[i-1][0],dp[i-1][1])+w[i],dp[i][0]=inf;
						else dp[i][0]=dp[i-1][1],dp[i][1]=inf;
					else if(i==y)
						if(b==1) dp[i][1]=min(dp[i-1][0],dp[i-1][1])+w[i],dp[i][0]=inf;
						else dp[i][0]=dp[i-1][1],dp[i][1]=inf;
				printf("%d\n",min(dp[n][0],dp[n][1]));
			}
		}
	}
	return 0;
}
