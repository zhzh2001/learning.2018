#include<cstdio>
#include<cstring>
#include<cmath>
#define ll long long

using namespace std;

const int MOD=1e9+7;
int n,m;
int a[10][10];
ll ans=0;

inline ll fastpow(int x,int k){
	ll res=1;
	while(k){
		if(k&1) x*=x,x%=MOD;
		k>>=1,res*=x,res%=MOD;
	}
	return res;
}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==2){
		for(int i=1;i<=m-1;i++){
			ans=fastpow(2,2*m);
			ll temp=0;
			for(int i=1;i<=m-1;i++){
				temp+=fastpow(2,i-1);temp%=MOD;
				temp+=fastpow(2,2*(m-1-i));temp%=MOD;
			}
			temp*=2;temp%=MOD;
			ans=(ans+MOD-temp)%MOD;
		}
		printf("%lld\n",ans);
	}
	else if(n<=3&&m<=3){
		if(n==3&&m==3) puts("112");
		else if(n==3&&m==2)   puts("144");
		else if(n==1||m==1) puts("0");
	}
	return 0;
}
