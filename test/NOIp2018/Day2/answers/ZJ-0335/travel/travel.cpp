#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<cmath>
#define xep(i,x) for(int i=head[x];i;i=nxt[i])

using namespace std;

const int N=5010;
int n,m,cnt=0,top=0;
int head[N],nxt[N<<1],to[N<<1];
int vis[N],ans[N],stk[N];

inline void add(int x,int y){
	to[++cnt]=y;nxt[cnt]=head[x];
	head[x]=cnt;
}

inline void dfs(int x,int ff){
	ans[++cnt]=x;vis[x]=1;
	int num=0,son[N];
	xep(i,x){
		int v=to[i];
		if(v==ff||vis[v]==1) continue;
		son[++num]=v;
	}
	sort(son+1,son+num+1);
	for(int i=1;i<=num;i++)
		dfs(son[i],x);
}

inline void spfa(int x,int ff){
	vis[x]++;
	xep(i,x){
		int v=to[i];
		if(v==ff) continue;
		dfs(v,x);
	}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);add(y,x);
	}
	if(n==m+1){
		cnt=0; 
		dfs(1,0);
		for(int i=1;i<=cnt;i++)
			printf("%d ",ans[i]);
		puts("");
	}
	else{//有环的情况 
		memset(vis,0,sizeof(vis));
		spfa(1,0);
//		for(int i=1;i<=n;i++) printf("%d ",vis[i]);
	}
	return 0;
}
