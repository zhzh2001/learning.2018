#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const int N=4e5+5;
struct node{
	int to,next;
}a[N];
int l = 0;
int head[N],col[N],val[N],de[N];
int ans[3][N],all[3];
int x,y,tx,ty;
void add(int x,int y){
	a[++l].to=y;a[l].next=head[x];head[x]=l;
}

char c[3];
int re(){
	char c=getchar();int all=0,pd=1;
	for(;c>'9'||c<'0';c=getchar()) if(c=='-') pd=-1;
	while(c>='0'&&c<='9') all=all*10+c-'0',c=getchar();return all*pd;
}
void dfs(int x,int fa){
	int t = col[x] ^ 1;
	for(int i=head[x];i;i=a[i].next){
		int to = a[i].to;
		if(to == fa) continue;
		col[to] = t;
		dfs(to,x);
	}
}

void dfs1(int x,int fa){
	de[x] = de[fa] + 1;
	for(int i=head[x];i;i=a[i].next){
		int to = a[i].to;
		if(to == fa) continue;
		dfs1(to,x);
	}
}

int work(int id){
	if(ans[id][x] == 1 && tx == 0) return 1e9;
	if(ans[id][y] == 1 && ty == 0) return 1e9;
	int tot = 0;
	if(tx == 1 && ans[id][tx] == 0) tot += val[tx];
	if(ty == 1 && ans[id][ty] == 0) tot += val[ty];
	return tot + all[id];

}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n=re(),m=re();
	scanf("%s",c);
	for(int i=1;i<=n;i++) val[i]=re();
	for(int i=1;i<n;i++){
		int x=re(),y=re();
		add(x,y);add(y,x);
	}
	dfs1(1,0);
	memset(col,-1,sizeof(col));
	col[1] = 0;dfs(1,0);
	for(int i=1;i<=n;i++) ans[0][i] = col[i];
	for(int i=1;i<=n;i++) 
		if(col[i]) all[0] += val[i];
	memset(col,-1,sizeof(col));
	col[1] = 1;dfs(1,0);
	for(int i=1;i<=n;i++) ans[1][i] = col[i];
	for(int i=1;i<=n;i++) 
		if(col[i]) all[1] += val[i];
	for(int i=1;i<=m;i++){
		x=re(),tx=re();
		y=re(),ty=re();
		int res = 1e9;
		res = min(work(0),work(1));
		if(de[x] == de[y]+1 ||de[x] == de[y] - 1){
			if(tx == 0 && ty == 0) {printf("-1\n");continue;}
		}
		printf("%d\n",res);
	}
	return 0;
}
