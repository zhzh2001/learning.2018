#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
const ll N=105;
const ll MOD = 1e9+7;
ll qpow[N];
ll re(){
	char c=getchar();ll all=0,pd=1;
	for(;c>'9'||c<'0';c=getchar()) if(c=='-') pd=-1;
	while(c>='0'&&c<='9') all=all*10+c-'0',c=getchar();return all*pd;
}

ll fpow(ll x,ll k){
	ll w = 1;
	for(ll i=k;i;i>>=1,x=x*x%MOD)
		if(i&1) w = w*x%MOD;
	return w;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	ll n=re(),m=re();
	qpow[0] = 1;
	for(ll i=1;i<=n+1;i++) qpow[i] = qpow[i-1] * 2;
	if(n==2){
		if(m == 1) {printf("%lld",qpow[n]);return 0;}
	}if(n==2){
		if(m == 2) {printf("%d",12);return 0;}
	}if(n==2){
		if(m == 3) {printf("%d",36);return 0;}
	}if(n==3){
		if(m == 1) {printf("%d",8);return 0;}
	}if(n==3){
		if(m == 2) {printf("%d",36);return 0;}
	}if(n==3){
		if(m == 3) {printf("%d",112);return 0;}
	}if(n==5){
		if(m == 5) {printf("%d",7136);return 0;}
	}
	if(n == 2){
		printf("%lld",4*fpow(3,m-1)%MOD);
	}else{
		printf("0\n");
	}
	return 0;
} 
