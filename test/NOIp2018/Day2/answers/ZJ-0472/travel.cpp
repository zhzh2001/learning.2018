#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=2e5+5;
struct node{
	int to,next;
}a[N];
int f[5005][5005],head[N],b[N],sta[N],q1[N],q2[N],num[N],fin[N],ans[5005][5005];
int l,now,vis[N];
void add(int x,int y){
	a[++l].to=y;a[l].next=head[x];head[x]=l;
}
int re(){
	char c=getchar();int all=0,pd=1;
	for(;c>'9'||c<'0';c=getchar()) if(c=='-') pd=-1;
	while(c>='0'&&c<='9') all=all*10+c-'0',c=getchar();return all*pd;
}

void dfs(int x){
	if(b[x]) return;
	b[x] = 1;
	printf("%d ",x);
	int tot = 0;
	for(int i=head[x];i;i=a[i].next){
		int to = a[i].to;
		f[x][++tot] = to;
	}sort(f[x]+1,f[x]+1+tot);
	for(int i=1;i<=tot;i++){
		int to = f[x][i];
		dfs(to);
	}
}

void dfs2(int x){
	if(b[x]) return;
	b[x] = 1;
	ans[now][++num[now]] = x;
	int tot = 0;
	for(int i=head[x];i;i=a[i].next){
		int to = a[i].to;
		f[x][++tot] = to;
	}sort(f[x]+1,f[x]+1+tot);
	for(int i=1;i<=tot;i++){
		int to = f[x][i];
		dfs2(to);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n=re(),m=re();
	for(int i=1;i<=m;i++){
		int x=re(),y=re();
		q1[i]=x,q2[i] = y;
		add(x,y);add(y,x);
	}
	if(m == n-1){
		dfs(1);
	}else{
		for(int i=1;i<=n;i++) fin[i] = 1e9;
		for(int i=1;i<=m;i++){
			now = i;
			memset(head,0,sizeof(head));
			memset(b,0,sizeof(b));l = 0;
			for(int j=1;j<=m;j++){
				if(i==j) continue;
				int x=q1[j],y=q2[j];
				add(x,y);add(y,x);
			}
			dfs2(1);
			if(num[now]!=n) vis[now] = 1;;
		}
		for(int i=1;i<=m;i++){
			if(vis[i]) continue;
			for(int j=1;j<=n;j++){
				if(fin[j] < ans[i][j]) break;
				else if(fin[j] > ans[i][j]){
					for(int t=1;t<=n;t++)
						fin[t] = ans[i][t];
					break;
				}
			}
		}
		for(int i=1;i<=n;i++) printf("%d ",fin[i]);
	}
	return 0;
}
