#include <cstdio>
#include <cctype>
#define ll long long
using namespace std;
const int mod = 1e9 + 7;
int n, m, a[2005][2005];
ll ans = 0;
#define gc ch = getchar()
inline int re(){
	char ch = getchar(); int x = 0, f = 1;
	for (; !isdigit(ch); gc) if (ch == '-') f = -1;
	for (; isdigit(ch); gc) x = x * 10 + ch - '0';
	return x * f;
}
#undef gc
int ksm(int x, int y){
	int res = 1;
	while (y){
		if (y & 1) res = (ll) res * x % mod;
		y >>= 1;
		x = (ll) x * x % mod;
	}
	return res;
}
bool pd(){
	for (int i = 0; i < n - 1; i++)
		for (int j = 0; j < m - 1; j++) 
			if (a[i][j + 1] > a[i + 1][j]) return 0; 
	return 1;
}
void dfs(int x, int y){
	if (y == n - 1 && x == n - 1){
		a[x][y] = 0;
		if (pd()) ++ans; 
		a[x][y] = 1;
		if (pd()) ++ans;
		return;
	}
	if (y == n){dfs(x + 1, 0); return;}
	a[x][y] = 0;
	dfs(x, y + 1);
	a[x][y] = 1;
	dfs(x, y + 1);
}
int main(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	n = re(); m = re();
	if (n == 1)	printf("%d\n", ksm(2, m));
	else if (n == 2){
		if (m == 1) puts("4");
		else if (m == 2) puts("12");
		else if (m == 3) puts("");
	}
	else if (n == 3){
		if (m == 1) puts("8");
		if (m == 3) puts("112");
	}
	else if (m == 1) printf("%d\n", ksm(2, n));
	else if (n == 5 && m == 5) puts("7136");
	else{
		dfs(0, 0);
		printf("%d\n", ans);
	}
	return 0;
}
