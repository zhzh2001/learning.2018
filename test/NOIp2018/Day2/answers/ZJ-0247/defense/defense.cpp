#include <cstdio>
#include <cstring>
#include <iostream>
#define N 100005
#define ll long long
using namespace std;
struct node{
	int nxt, to;
}e[N << 1];
int n, m, p[N], a, x, b, y, len, cnt = 0, head[N], cur[N], sum[N];
char s[3];
long long ans, ans2, ans3;
#define gc ch = getchar()
inline int re(){
	char ch = getchar(); int x = 0, f = 1;
	for (; !isdigit(ch); gc) if (ch == '-') f = -1;
	for (; isdigit(ch); gc) x = x * 10 + ch - '0';
	return x * f;
}
#undef gc
inline void add(int x, int y){
	e[++cnt].to = y;
	e[cnt].nxt = head[x];
	head[x] = cnt;
}
void dfs(int now, int num, int fa){
	sum[now] = num; ans2 += num * p[now];
	for (int i = head[now]; i; i = e[i].nxt){
		int v = e[i].to;
		if (v == fa) continue;
		dfs(v, 1 - num, now);
	}
}
bool count(int now, int to, int fa, int dep){
	if (now == to){len = dep - 1; return 1;}
	for (int i = head[now]; i; i = e[i].nxt){
		int v = e[i].to;
		if (v == fa) continue;
		bool fl = count(v, to, now, dep + 1);
		if (fl){cur[dep] = now; return 1;}
	}
	return 0;
}
void dfs2(int now, int fa, int num){
	if (now == a || now == b) return;
	if (sum[now] != num){ans -= sum[now] * p[now]; ans += num * p[now];}
	for (int i = head[now]; i; i = e[i].nxt){
		int v = e[i].to;
		if (v == fa) continue;
		dfs2(v, now, 1 - num);
	}
}
bool nextt(int a, int b){
	for (int i = head[a]; i; i = e[i].nxt)
		if (e[i].to == b) return 1;
	return 0;
}
int main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	n = re(); m = re(); cin >> s;
	memset(cur, 0, sizeof cur);
	for (int i = 1; i <= n; i++) p[i] = re();
	for (int i = 1; i < n; i++){
		int u = re(), v = re();
		add(u, v); add(v, u);
	} 
	while (m--){
		a = re(), x = re(), b = re(), y = re();
		if (nextt(a, b) && !x && !y) puts("-1");
		else{
			len = 0;
			count(a, b, 0, 0);
			ans = 0; ans2 = 0, ans3 = 100000000;
			dfs(a, x, 0);
			if (len & 1) printf("%lld\n", ans2);
			else{
				if (x != y) printf("%lld\n", ans2);
				else{
					if (x == 1) ans = ans2 + y * p[b];
					else ans = ans2 - y * p[b];
					dfs2(cur[1], 0, 1);
					ans3 = min(ans3, ans);
					if (x == 1) ans = ans2 + y * p[b];
					else ans = ans2 - y * p[b];
					dfs2(cur[1], 0, 0);
					printf("%lld\n", min(ans3, ans));
				}
			}
		}
	}
	return 0;
}
