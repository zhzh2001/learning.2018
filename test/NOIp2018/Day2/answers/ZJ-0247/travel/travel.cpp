#include <cstdio>
#include <set>
#include <cctype>
#include <cstring>
#include <algorithm>
#define N 5005
using namespace std;
struct node{
	int nxt, to;
}e[N << 1];
int n, m, cnt = 0, minn = 2147483647, head[N], ans[N], anss[N];
bool viss[N];
set<int> s;
#define gc ch = getchar()
inline int re(){
	char ch = getchar(); int x = 0, f = 1;
	for (; !isdigit(ch); gc) if (ch == '-') f = -1;
	for (; isdigit(ch); gc) x = x * 10 + ch - '0';
	return x * f;
}
#undef gc
inline void add(int x, int y){
	e[++cnt].to = y;
	e[cnt].nxt = head[x];
	head[x] = cnt;
}
void dfs(int now, int fa){
	int cntv = 0, vis[N];
	ans[++ans[0]] = now; viss[now] = 1;
	for (int i = head[now]; i; i = e[i].nxt){
		int v = e[i].to;
		if (v == fa || viss[v]) continue;
		vis[++cntv] = v;
	}
	sort(vis + 1, vis + 1 + cntv);
	for (int i = 1; i <= cntv; i++) dfs(vis[i], now);
}
void pd(){
	for (int i = 1; i <= n; i++)
		if (ans[i] > anss[i]) return;
		else if (ans[i] < anss[i]) break;
	for (int i = 1; i <= n; i++) anss[i] = ans[i];
}
void bl(int now){
	viss[now] = 1; ans[++ans[0]] = now;
	if (ans[0] == n){pd(); --ans[0]; return;}
	for (int i = head[now]; i; i = e[i].nxt){
		int v = e[i].to;
		if (viss[v]) continue;
		bl(v);
	}
}
int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	n = re(); m = re();
	for (int i = 1; i <= m; i++){
		int u = re(), v = re();
		add(u, v); add(v, u); minn = min(minn, min(u, v));
	}
	if (m == n - 1) dfs(minn, 0);
	else{
		for (int i = 1; i <= n; i++) anss[i] = 2147483647;
		memset(viss, 0, sizeof viss);
		bl(minn);
		for (int i = 1; i <= n; i++) printf("%d ", anss[i]);
		return 0;
	}
	for (int i = 1; i <= ans[0]; i++) printf("%d ", ans[i]);
	return 0;
}
