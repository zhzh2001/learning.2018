#include<cstring>
#include<cmath>
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<queue>
#include<vector>

#define ll long long
#define mod 1000000007
#define N 1000007

#define fo(i,x,y) for(int i=x;i<=y;i++)
#define fd(i,x,y) for(int i=x;i>=y;i--)
#define fb(i,x) for(int i=x;i!=-1;i=nxt[i])
using namespace std;
template<typename T>
inline void read(T &x)
{
	int f=1;x=0;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	x=x*f;
}

int n,m;
ll mi[N<<1],f[N];

void solve_1()
{
	if (n>m) swap(n,m);
	if (n==1&&m==1) cout<<2<<endl;
	if (n==1&&m==2) cout<<4<<endl;
	if (n==1&&m==3) cout<<8<<endl;
	if (n==2&&m==2) cout<<12<<endl;
	if (n==2&&m==3) cout<<40<<endl;
	if (n==3&&m==3) cout<<112<<endl;
}
void solve_2()
{
	ll ans=4;
	f[1]=3;
	fo(i,2,m-1)
		f[i]=(f[i-1]*2+mi[2*(i-1)])%mod;
	ans=ans*f[m-1]%mod;
	printf("%lld\n",ans);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	mi[0]=1;fo(i,1,2000000) mi[i]=mi[i-1]*2%mod;
	read(n),read(m);
	if (n<=3&&m<=3) solve_1();
	else if (n==2) solve_2();	
}
