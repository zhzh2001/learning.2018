#include<cstring>
#include<cmath>
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<queue>
#include<vector>

#define ll long long
#define inf 2000000007ll
#define N 100007

#define fo(i,x,y) for(int i=x;i<=y;i++)
#define fd(i,x,y) for(int i=x;i>=y;i--)
#define fb(i,x) for(int i=x;i!=-1;i=nxt[i])
using namespace std;
template<typename T>
inline void read(T &x)
{
	int f=1;x=0;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	x=x*f;
}

int n,m;
int p[N];
char ch[10];
int cnt,hed[N],rea[N<<1],nxt[N<<1];
bool boo[2007][2007];
ll f[2007][2];
int x1,g1,x2,g2;

void add(int u,int v)
{
	if (n<=2000) boo[u][v]=true;
	nxt[++cnt]=hed[u];
	hed[u]=cnt;
	rea[cnt]=v;
}
void dfs(int u,int fa)
{
	int q=-1;
	if (u==x1)
	{
		if (g1==1) q=1;
		else q=0;
	}
	if (u==x2)
	{
		if (g2==1) q=1;
		else q=0;
	}
	if (q==0) f[u][1]=inf;
	if (q==1) f[u][0]=inf;
	
	fb(i,hed[u])
	{
		if (rea[i]==fa) continue;
		dfs(rea[i],u); 
	}
	if (q!=0)
	{
		ll res=0;
		fb(i,hed[u])
		{
			int v=rea[i];
			if (v==fa) continue;
			res+=min(f[v][0],f[v][1]);
		}
		f[u][1]=p[u]+res;
		if (f[u][1]>inf) f[u][1]=inf;
	}
	if (q!=1)
	{
		ll res=0;
		fb(i,hed[u])
		{
			int v=rea[i];
			if (v==fa) continue;
			res+=f[v][1];
		}
		f[u][0]=res;
		if (f[u][0]>inf) f[u][0]=inf;
	}
}
void solve_1()
{
	while(m--)
	{
		read(x1),read(g1),read(x2),read(g2);
		if (boo[x1][x2]&&!g1&&!g2)
		{
			printf("-1\n");
			continue;
		}
		else
		{
			memset(f,0,sizeof(f));
			dfs(1,0);
			printf("%lld\n",min(f[1][0],f[1][1]));
		}
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	
	memset(hed,-1,sizeof(hed));
	read(n),read(m);scanf("%s",ch+1);
	fo(i,1,n)read(p[i]);
	int x,y;
	fo(i,1,n-1)
	{
		read(x),read(y);
		add(x,y),add(y,x);
	}
	if (n<=2000&&m<=2000) solve_1();
}
