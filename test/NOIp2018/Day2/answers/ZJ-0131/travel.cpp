#include<cstring>
#include<cmath>
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<queue>
#include<vector>

#define N 10007

#define fo(i,x,y) for(int i=x;i<=y;i++)
#define fd(i,x,y) for(int i=x;i>=y;i--)
#define fb(i,x) for(int i=x;i!=-1;i=nxt[i])
using namespace std;
template<typename T>
inline void read(T &x)
{
	int f=1;x=0;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	x=x*f;
}

int n,m;
int ans[N],tx,du[N],hb;
bool boo[N],huan[N],rt,vis[N];
int cnt,hed[N],rea[N<<1],nxt[N<<1];
vector<int>ve[N];
queue<int>q;
int ge;
priority_queue<int,vector<int>,greater<int> >qu[N];

void add(int u,int v)
{
	nxt[++cnt]=hed[u];
	hed[u]=cnt;
	rea[cnt]=v;
}
void solve1(int u,int f)
{
	ans[++tx]=u;
	fb(i,hed[u])
	{
		if (rea[i]==f) continue;
		ve[u].push_back(rea[i]);
	}
	sort(ve[u].begin(),ve[u].end());
	fo(i,0,(int)ve[u].size()-1)
		solve1(ve[u][i],u);
}
void dfs(int u,int f)
{
	if (vis[u]) return;
	vis[u]=true;
	ans[++tx]=u;
	if (!huan[u]||rt)
	{
		fb(i,hed[u])
		{
			if (rea[i]==f) continue;
			ve[u].push_back(rea[i]);
		}
		sort(ve[u].begin(),ve[u].end());
		fo(i,0,(int)ve[u].size()-1)
			dfs(ve[u][i],u);
	}
	else
	{
		if (!huan[f])
		{
			int r1=0,r2=0;
			fb(i,hed[u])
			{
				if (rea[i]==f) continue;
				if (huan[rea[i]])
				{
					if (r1) r2=rea[i];
					else r1=rea[i];
				}
				else qu[u].push(rea[i]);
			}
			if (r1>r2) swap(r1,r2);
			hb=r2;
			while(!qu[u].empty())
			{
				if (r1)
				{
					if (r1<qu[u].top()) dfs(r1,u),r1=0;
					else dfs(qu[u].top(),u),qu[u].pop();
				}
				else if (r2)
				{
					rt=true;
					if (r2<qu[u].top()) dfs(r2,u),r2=0;
					else dfs(qu[u].top(),u),qu[u].pop();
				}
				else dfs(qu[u].top(),u),qu[u].pop();
			}
			if (r1) dfs(r1,u);
			if (r2) rt=true,dfs(r2,u);
		}
		else
		{
			int nx=0;++ge;
			fb(i,hed[u])
			{
				if (rea[i]==f) continue;
				if (huan[rea[i]]) nx=rea[i];
				else qu[u].push(rea[i]);
			}
			if (nx>hb)
			{
				while(!qu[u].empty())
					dfs(qu[u].top(),u),qu[u].pop();
			}
			else
			{
				while(!qu[u].empty())
				{
					if (nx)
					{
						if (qu[u].top()>nx) dfs(nx,u),nx=0;
						else dfs(qu[u].top(),u),qu[u].pop();
					}
					else dfs(qu[u].top(),u),qu[u].pop();
				}
				if (nx) dfs(nx,u);
			}
		}
	}
}
void solve2()
{
	fo(i,1,n)huan[i]=true;
	fo(i,1,n)if(du[i]==1)q.push(i);
	while(!q.empty())
	{
		int u=q.front();q.pop();
		huan[u]=false;
		fb(i,hed[u])
		{
			du[rea[i]]--;
			if (du[rea[i]]==1) q.push(rea[i]);
		}
	}
	dfs(1,0);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	memset(hed,-1,sizeof(hed));
	read(n),read(m);
	int x,y;
	fo(i,1,m)
	{
		read(x),read(y);
		add(x,y),add(y,x);
		du[x]++;
		du[y]++;
	}
	
	if (m==n-1) solve1(1,0);
	else solve2();
	
	
	fo(i,1,n)printf("%d ",ans[i]);
}
