#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

const int N=5010;
int n,m;
int G[N][N];
int head[N],nxt[N<<1],fr[N<<1],to[N<<1],lst=1;
inline void adde(int x,int y) {
  nxt[++lst]=head[x]; fr[lst]=x; to[lst]=y; head[x]=lst;
}
int ans[N],seq[N],ind=0;
int cnt=0;
int vis[N],col=0;
int ban[2];
inline void dfs(int u) {
  seq[++ind]=u; vis[u]=col; ++cnt;
  for(int i=head[u];i;i=nxt[i])
    if(vis[to[i]]!=col) {
      if(i==ban[0] || i==ban[1]) continue;
      int v=to[i];
      dfs(v);
    }
}
inline void Solve() {
  for(int i=1;i<=n;i++)
    for(register int j=i+1;j<=n;j++)
      if(G[i][j]) {
	cnt=0; ind=0; ++col;
	ban[0]=G[i][j]; ban[1]=G[j][i];
	dfs(1);
	if(cnt==n) {
	  bool fl=0;
	  for(int k=1;k<=n;k++)
	    if(seq[k]<ans[k]) {
	      fl=1; break;
	    } else if(seq[k]>ans[k]) {
	      fl=0; break;
	    }
	  if(fl) for(int k=1;k<=n;k++) ans[k]=seq[k];
	}
      }
}
int main() {
#ifdef LOCAL
#else
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
#endif
  scanf("%d %d",&n,&m);
  for(int i=1;i<=n;i++) ans[i]=n;
  for(int i=1;i<=m;i++) {
    int x,y; scanf("%d %d",&x,&y);
    G[x][y]=G[y][x]=1;
  }
  for(int i=1;i<=n;i++)
    for(int j=n;j>=1;j--)
      if(G[i][j]) {
	adde(i,j);
	G[i][j]=lst;
      }
  if(m==n-1) {
    ++col;dfs(1);
    for(int i=1;i<=n;i++) ans[i]=seq[i];
  } else {
    Solve();
  }
  for(int i=1;i<=n;i++) printf("%d ",ans[i]); puts("");
  return 0;
}
/*
6 5
1 3
2 3
2 5
3 4
4 6
*/
