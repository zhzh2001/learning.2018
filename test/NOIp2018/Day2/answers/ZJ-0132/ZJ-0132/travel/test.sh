#!bin/bash
for i in $(seq 1 4); do
    ./travel < travel$i.in > my.out
    if diff -b my.out travel$i.ans; then
	echo AC
    else
	echo WA
    fi
done
