#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=100010;
const ll INF=1e16;

char T[20];
int n,m;
ll a[N];
int head[N],nxt[N<<1],to[N<<1],lst=0;
inline void adde(int x,int y) {
  nxt[++lst]=head[x]; to[lst]=y; head[x]=lst;
}
int fa[N],dep[N];
void Build(int u) {
  for(int i=head[u];i;i=nxt[i])
    if(to[i]!=fa[u]) {
      int v=to[i];
      dep[v]=dep[u]+1;
      fa[v]=u;
      Build(v);
    }
}
namespace Brute {
  ll f[N][2];
  int ban[2],sta[2];
  void DP(int u) {
    f[u][0]=f[u][1]=0;
    for(int i=head[u];i;i=nxt[i])
      if(to[i]!=fa[u]) DP(to[i]);
    for(int i=head[u];i;i=nxt[i])
      if(to[i]!=fa[u]) {
	int v=to[i];
	f[u][1]+=min(f[v][0],f[v][1]);
	f[u][0]+=f[v][1];
      }
    f[u][1]+=a[u];
    for(int i=0;i<2;i++)
      if(u==ban[i]) {
	if(sta[i]==1) {
	  f[u][0]=f[u][2]=INF;
	} else {
	  f[u][1]=INF;
	}
      }
  }
  void main() {
    for(int cas=1;cas<=m;cas++) {
      bool fl=1;
      for(int i=0;i<2;i++) scanf("%d%d",&ban[i],&sta[i]);
      if(!sta[0] && !sta[1])
	for(int i=head[ban[0]];i;i=nxt[i])
	  if(to[i]==ban[1]) {puts("-1"); fl=0; break;}
      if(!fl) continue;
      DP(1);
      /*
      for(int i=1;i<=n;i++)
	printf("%d:{%lld,%lld,%lld}\n",i,f[i][0],f[i][1],f[i][2]);
      //*/
      printf("%lld\n",min(f[1][1],f[1][0]));
    }
  }
}
namespace Lian {

  namespace S2 {
    ll f[N][2],g[N][2];
    void main() {
      for(int i=1;i<=n;i++) {
	f[i][0]=f[i-1][1];
	f[i][1]=min(f[i-1][0],f[i-1][1])+a[i];
      }
      for(int i=n;i>=1;i--) {
	g[i][0]=g[i+1][1];
	g[i][1]=min(g[i+1][0],g[i+1][1])+a[i];
      }
      for(int cas=1;cas<=m;cas++) {
	int a,b,x,y; scanf("%d%d%d%d",&a,&x,&b,&y);
	if(!x && !y) {puts("-1"); continue;}
	if(a<b)
	  printf("%lld\n",f[a][x]+g[b][y]);
	else printf("%lld\n",f[b][y]+g[a][x]);
      }
    }
  }
  namespace S1 {
    ll f[N][2],g[N][2];
    void main() {
      f[1][1]=a[1]; f[1][0]=INF;
      for(int i=2;i<=n;i++) {
	f[i][0]=f[i-1][1];
	f[i][1]=min(f[i-1][0],f[i-1][1])+a[i];
      }
      for(int i=n;i>=1;i--) {
	g[i][0]=g[i+1][1];
	g[i][1]=min(g[i+1][0],g[i+1][1])+a[i];
      }
      g[1][0]=INF;
      for(int cas=1;cas<=m;cas++) {
	int a,b,x,y; scanf("%d%d%d%d",&a,&x,&b,&y);
	if(y) {
	  printf("%lld\n",min(f[b-1][0],f[b-1][1])+g[b][y]);
	} else {
	  printf("%lld\n",f[b-1][1]+g[b][y]);
	}
      }
    }
  }
  void main() {
    if(T[2]=='1') S1::main();
    else if(T[2]=='2') S2::main();
  }
}
int main() {
#ifdef LOCAL
#else
  freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout);
#endif
  scanf("%d%d%s",&n,&m,T+1);
  for(int i=1;i<=n;i++) scanf("%lld",&a[i]);
  for(int i=1;i<n;i++) {
    int x,y; scanf("%d%d",&x,&y);
    adde(x,y); adde(y,x);
  }
  dep[1]=1; Build(1);
  //fprintf(stderr,"Arr");
  if(n<=2000) {Brute::main();}
  else if(T[1]=='A') {Lian::main();}
  return 0;
}
/*
5 3 A1
3 2 5 4 1
1 2
2 3
3 4
4 5

  5 3 C3
  2 4 1 3 9
  1 5
  5 2
  5 3
  3 4
  1 0 3 0
  2 1 3 1
  1 0 5 0
*/
