#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=100010;
const ll INF=1e16;

char T[20];
int n,m;
ll a[N];
int head[N],nxt[N<<1],to[N<<1],lst=0;
inline void adde(int x,int y) {
  nxt[++lst]=head[x]; to[lst]=y; head[x]=lst;
}
int fa[N],dep[N];
void Build(int u) {
  for(int i=head[u];i;i=nxt[i])
    if(to[i]!=fa[u]) {
      int v=to[i];
      dep[v]=dep[u]+1;
      fa[v]=u;
      Build(v);
    }
}
namespace Brute {
  ll f[N][3];
  int ban[2],sta[2];
  void DP(int u) {
    f[u][0]=f[u][1]=f[u][2]=0;
    for(int i=head[u];i;i=nxt[i])
      if(to[i]!=fa[u]) DP(to[i]);
    for(int i=head[u];i;i=nxt[i])
      if(to[i]!=fa[u]) {
	int v=to[i];
	f[u][1]+=min(f[v][0],min(f[v][1],f[v][2]));
	f[u][2]+=f[v][0];
      }
    f[u][0]=INF;
    for(int i=head[u];i;i=nxt[i])
      if(to[i]!=fa[u]) {
	int v=to[i];
	f[u][0]=min(f[u][0],f[u][1]-min(f[v][0],min(f[v][1],f[v][2]))+f[v][1]);
      }
    f[u][1]+=a[u];
    for(int i=0;i<2;i++)
      if(u==ban[i]) {
	if(sta[i]==1) {
	  f[u][0]=f[u][2]=INF;
	} else {
	  f[u][1]=INF;
	}
      }
  }
  void main() {
    for(int cas=1;cas<=m;cas++) {
      bool fl=1;
      for(int i=0;i<2;i++) scanf("%d%d",&ban[i],&sta[i]);
      if(!sta[0] && !sta[1])
	for(int i=head[ban[0]];i;i=nxt[i])
	  if(to[i]==ban[1]) {puts("-1"); fl=0; break;}
      if(!fl) continue;
      DP(1);
      /*
      for(int i=1;i<=n;i++)
	printf("{%lld,%lld,%lld}\n",f[i][0],f[i][1],f[i][2]);
      */
      printf("%lld\n",min(f[1][1],f[1][0]));
    }
  }
}
int main() {
#ifdef LOCAL
#else
  freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout);
#endif
  scanf("%d%d%s",&n,&m,T+1);
  for(int i=1;i<=n;i++) scanf("%lld",&a[i]);
  for(int i=1;i<n;i++) {
    int x,y; scanf("%d%d",&x,&y);
    adde(x,y); adde(y,x);
  }
  dep[1]=1; Build(1);
  if(n<=2000) {Brute::main();}
  return 0;
}
/*
5 3 C3
2 4 1 3 9
1 5
5 2
5 3
3 4
1 0 3 0
2 1 3 1
1 0 5 0
*/
