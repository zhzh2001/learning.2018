#!bin/bash
for i in $(seq 1 2); do
    ./defense < defense$i.in > my.out
    if diff -b my.out defense$i.ans; then
	echo AC
    else
	echo WA
    fi
done
