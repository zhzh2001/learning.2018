#include<bits/stdc++.h>
using namespace std;
int read()
{
	int x=0,w=0;
	char ch=0;
	while(!isdigit(ch)){w|=ch=='-';ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return w? -x:x;
}
int n,m;
vector<int> v[5010];
bool vis[5010];
int cnt,visit_th[5010];
void dfs(int x,int fa=0)
{
	visit_th[++cnt]=x;
	vis[x]=1;
	for(int i=0;i<v[x].size();++i)
	{
		int to=v[x][i];
		if(fa==to) continue;
		if(vis[to]) continue;
		dfs(to,x);
	}
}
int father[5010],depth[5010];
int now_goto=10000,nex_goto=10000;
void dfs_depth(int x,int fa=0)
{
	depth[x]=depth[fa]+1;
	vis[x]=1;
	// cerr<<x<<endl;
	for(int i=0;i<v[x].size();++i)
	{
		int to=v[x][i];
		if(to==fa) continue;
		if(depth[to]) continue;
		if(vis[to]) continue;
		dfs_depth(to,x);
	}
}
queue<int> q;
struct edge{
	int to,next;
}e[20010];
int len=1,linkk[20010];
void insert(int u,int v)
{
	e[++len].to=v,e[len].next=linkk[u],linkk[u]=len;
	e[++len].to=u,e[len].next=linkk[v],linkk[v]=len;
}
// bool is_o[5010];
// bool find(int from,int x)
// {
// 	vis[x]=1;
// 	for(int i=0;i<v[x].size();++i)
// 	{
// 		int to=v[x][i];
// 		if(to==from) return is_o[to]=1;
// 		if(vis[to]) continue;
// 		if(find(from,to)==1) return is_o[to]=1;
// 	}
// 	return is_o[x]=0;
// }
// void spfa(int st)
// {
// 	int dis[5010],cc[5010];
// 	memset(cc,0,sizeof(cc));
// 	memset(dis,128,sizeof(dis));
// 	q.push(st),vis[st]=1;dis[st]=0;
// 	while(q.size())
// 	{
// 		int x=q.front(); q.pop(); vis[x]=0;
// 		for(int i=0;i<v[x].size();++i)
// 		{
// 			int to=e[i].to;
// 			if(dis[to]<dis[x]+1)
// 			{
// 				if(++cc[to]>=n) {memset(vis,0,sizeof(vis));find(to,to);return;}
// 				if(vis[to]) continue;
// 				vis[to]=1;
// 				q.push(to);
// 			}
// 		}
// 	}
// }
// void getans(int x=1,int side=0)
// {
	// vis[x]=1;
	// if(side==0) visit_th[++cnt]=x;
	// for(int i=0;i<v[x].size();++i)
	// {
	// 	int to=v[x][i];
	// 	if(vis[to]) continue;
	// 	if(side==1)
	// 	if(!is_o[x]&&is_o[to]) getans(to,1);
	// 	if(is_o[x]&&is_o[to]) getans(to,side);
	// 	if(is_o[x]&&!is_o[to]) getans(to,side);
	// 	if(!is_o[x]&&!is_o[to]) getans(to,0);
	// }
// }
void dfs2(int x,int fa=0)
{
	vis[x]=1;
	visit_th[++cnt]=x;
	for(int i=linkk[x],j=0;i&&j<v[x].size();++j,i=e[i].next)
	{
		int to=v[x][j];
		if(vis[to]) continue;
		if(to==fa) continue;
		if(e[i].to==0) continue;
		dfs2(to,x);
	}
}
int answer[5010];
void dfs3(int x=1)
{
	vis[x]=1;
	if(v[x].size()==2)
	{
		if(!vis[v[x][0]]&&now_goto==10000) now_goto=v[x][0];
		if(!vis[v[x][1]]&&nex_goto==10000) nex_goto=v[x][1];
		if(now_goto<nex_goto)
		{
			vis[now_goto]=1;
			visit_th[++cnt]=now_goto;
			now_goto=10000;
			dfs3(v[x][0]);
		}
		else
		{
			vis[nex_goto]=1;
			visit_th[++cnt]=nex_goto;
			nex_goto=10000;
			dfs3(v[x][1]);
		}
	}
	else if(!vis[v[x][0]])
	{
		if(!vis[v[x][0]]&&now_goto==10000)
		{
			now_goto=v[x][0];
			if(now_goto<nex_goto)
			{
				vis[now_goto]=1;
				visit_th[++cnt]=now_goto;
				now_goto=10000;
				dfs3(v[x][0]);
			}
			else
			{
				vis[nex_goto]=1;
				visit_th[++cnt]=nex_goto;
				nex_goto=10000;
				dfs3(v[x][1]);
			}
		}
		else if(!vis[v[x][0]]&&nex_goto==10000)
		{
			nex_goto=v[x][0];
			if(now_goto<nex_goto)
			{
				vis[now_goto]=1;
				visit_th[++cnt]=now_goto;
				now_goto=10000;
				dfs3(v[x][0]);
			}
			else
			{
				vis[nex_goto]=1;
				visit_th[++cnt]=nex_goto;
				nex_goto=10000;
				dfs3(v[x][1]);
			}
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;++i)
	{
		int U=read(),V=read();
		v[U].push_back(V);
		v[V].push_back(U);
		insert(U,V);
	}
	for(int i=1;i<=n;++i) if(v[i].size()) sort(v[i].begin(),v[i].end());
	if(m==n-1)
	{
		dfs(1);
		for(int i=1;i<=n;++i) printf("%d ",visit_th[i]);
	}
	else
	{
		int k;
		for(int i=1;i<=n;++i)
			for(int j=linkk[i];j;j=e[j].next)
			{
				cnt=0;memset(visit_th,0,sizeof(visit_th));
				memset(vis,0,sizeof(vis));
				k=e[i].to;
				e[i].to=e[i^1].to=0;
				dfs2(1);
				e[i].to=e[i^1].to=k;
				if(cnt<n){continue;}
				if(answer[1]==0) memcpy(answer,visit_th,sizeof(visit_th));
				else for(int l=1;l<=n;++l)
					if(answer[l]>visit_th[l]){memcpy(answer,visit_th,sizeof(visit_th));break;}
			}
		for(int i=1;i<=n;++i) printf("%d ",answer[i]);
	}
	return 0;
}