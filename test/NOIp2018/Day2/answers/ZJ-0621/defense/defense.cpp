#include<bits/stdc++.h>
using namespace std;
int read()
{
	int x=0,w=0;
	char ch=0;
	while(!isdigit(ch)){w|=ch=='-';ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return w? -x:x;
}
struct edge{
	int to,next;
}e[600010];
int linkk[600010],len;
void insert(int u,int v)
{
	e[++len].next=linkk[u],linkk[u]=len,e[len].to=v;
	e[++len].next=linkk[v],linkk[v]=len,e[len].to=u;
}
int n,m;
char type[4];
int p[300010];
int lim[300010];
long long mincost,tag;
bool vis[300010];
void dfs(int x,int fa=0,int c=0,long long cost=0)
{
	if(c>=2) return;
	if(x==n)
	{
		if(c==1)
		{
			if(lim[x]==-1) tag=-1;
			else cost+=p[x],mincost=min(mincost,cost),tag=0;
		}
		else
		{
			if(lim[x]==1) cost+=p[x],mincost=min(mincost,cost),tag=0;
			else mincost=min(mincost,cost),tag=0;
		}
		return;
	}
	vis[x]=1;
	for(int i=linkk[x];i;i=e[i].next)
	{
		int to=e[i].to;
		if(fa==to) continue;
		if(vis[to]) continue;
		if(!lim[x]) dfs(to,x,c+1,cost),dfs(to,x,0,cost+p[x]);
		else if(lim[x]==1) dfs(to,x,0,cost+p[x]);
		else dfs(to,x,c+1,cost);
	}
}
int a[20];
bool dfssss(int x,int c=0)
{
	vis[x]=1;
	if(c>1&&!a[x]) return 0;
	for(int i=linkk[x];i;i=e[i].next)
	{
		int to=e[i].to;
		if(vis[to]) continue;
		if(a[x]) 
			if(dfssss(to,0)) continue;
			else return 0;
		else 
			if(dfssss(to,c+1)) continue; 
			else return 0;
	}
	return 1;
}
void dfss(int k)
{
	if(k>n)
	{
		for(int i=1;i<=n;++i)
			if(lim[i]==1&&!a[k]) return;
			else if(lim[i]==-1&&a[k]) return;
		int c=0; long long cost=0;
		if(dfssss(1))
			mincost=min(mincost,cost);
		return ;
	}
	a[k]=1;
	dfs(k+1);
	a[k]=0;
	dfs(k+1);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(); scanf("%s",type);
	for(int i=1;i<=n;++i) p[i]=read();
	for(int i=1;i<n;++i)
	{
		int u=read(),v=read();
		insert(u,v);
	}
	if(type[0]=='A')
	{
		while(m--)
		{
			mincost=20000000000000ll;tag=0;
			memset(vis,0,sizeof(vis));
			int a=read(),x=read(),b=read(),y=read();
			if(x==1) lim[a]=1;
			else lim[a]=-1;
			if(y==1) lim[b]=1;
			else lim[b]=-1;
			dfs(1);
			lim[a]=lim[b]=0;
			if(tag==-1&&mincost==20000000000000ll){puts("-1");}
			else printf("%lld\n",mincost);
		}
	}
	else if(n<=10&&m<=10)
	{
		while(m--)
		{
			mincost=20000000000000ll; tag=0;
			memset(vis,0,sizeof(vis));
			int a=read(),x=read(),b=read(),y=read();
			if(x==1) lim[a]=1;
			else lim[a]=-1;
			if(y==1) lim[b]=1;
			else lim[b]=-1;
			dfss(1);
			lim[a]=lim[b]=0;
			if(mincost==20000000000000ll){puts("-1");}
			else printf("%lld\n",mincost);
		}
	}
	else
	{
		cout<<"QAQ"<<endl;
	}
	return 0;
}