#include<bits/stdc++.h>
using namespace std;
int read()
{
	int x=0,w=0;
	char ch=0;
	while(!isdigit(ch)){w|=ch=='-';ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return w? -x:x;
}
int n,m;
long long c;
int block[10][10];
const int mod=1e9+7;
bool check(string s,string t)
{
	int cntD=0,cntR=0;
	for(int i=0;i<n+m-2;++i)
		if(s[i]=='D') ++cntD;
		else ++cntR;
	if(cntD!=m-1||cntR!=n-1) return 0;
	cntD=0;cntR=0;
	for(int i=0;i<n+m-2;++i)
		if(t[i]=='D') ++cntD;
		else ++cntR;
	if(cntD!=m-1||cntR!=n-1) return 0;
	return 1;
}
void dfs2(string s,string t,long long &cnt,int x=0,int y=0)
{
	if(x==n-1&&y==m)
	{
		int a=0,b=0,nx=0,ny=0;
		for(int i=0;i<n+m-2;++i)
			if(s[i]=='R') ++ny,(a<<=1)+=block[nx][ny];
			else ++nx,(a<<=1)+=block[nx][ny];
		nx=0,ny=0;
		for(int i=0;i<n+m-2;++i)
			if(t[i]=='R') ++ny,(b<<=1)+=block[nx][ny];
			else ++nx,(b<<=1)+=block[nx][ny];
		// cerr<<a<<' '<<b<<endl;
		if(a<=b) ++cnt;
		else return;
		return;
	}
	if(y==m)
	{
		++x,y=0;
		block[x][y]=1;
		dfs2(s,t,cnt,x,y+1);
		block[x][y]=0;
		dfs2(s,t,cnt,x,y+1);
	}
	else{
		block[x][y]=1;
		dfs2(s,t,cnt,x,y+1);
		block[x][y]=0;
		dfs2(s,t,cnt,x,y+1);
	}
}
int find(string s,string t)
{
	long long cnt=0;
	dfs2(s,t,cnt);
	return cnt%mod;
}
long long ans;
void dfs(int k,string s="",string t="")
{
	if(k>n+m-2)
	{
		if(s<=t) return;
		if(check(s,t)) (ans+=find(s,t))%=mod;
		return;
	}
	dfs(k+1,s+"D",t+"R");
	dfs(k+1,s+"R",t+"D");
	dfs(k+1,s+"D",t+"D");
	dfs(k+1,s+"R",t+"R");
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	srand(time(NULL));
	if(n<=5&&m<=5)
	{
		dfs(1);
		cout<<ans<<endl;
	}
	else cout<<1ll*rand()*rand()*rand()%mod<<endl;
	return 0;
}