#include<iostream>
#include<cstdlib>
#include<cstring>
#include<cstdio>
#include<cctype>
#include<algorithm>
#include<queue>
#define N 5005
#define INF 0x3f3f3f3f

using namespace std;

inline int read(void){
	char ch;int x=0,f=1;
	while(!isdigit(ch=getchar()) && ch!='-');
	if (ch=='-') f=-1,ch=getchar(); x=ch-'0';
	while (isdigit(ch=getchar())) x=x*10+ch-'0';
	return x*f;
}

struct edge{
	int to; 
	edge* next;
	edge(int tox,edge* nx){
		to=tox,next=nx;
	}
};

int n,m;
edge* yead[N];
edge* head[N];
bool dis[N],vis[N];
int fa[N];

void dfs(int x){
	printf("%d ",x);
	vector<int> sn;
	for (edge* i=head[x];i;i=i->next)
		sn.push_back(i->to);
	sort(sn.begin(),sn.end());
	for (int i=0;i<sn.size();i++)
		dfs(sn[i]);
}

int main(void){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for (int x,y,i=1;i<=m;i++){
		x=read(),y=read();
		yead[x]=new edge(y,yead[x]);
		yead[y]=new edge(x,yead[y]);
	} 
	memset(dis,0,sizeof dis);
	memset(vis,0,sizeof vis);
	memset(fa,0x3f,sizeof fa);
	vis[1]=true;dis[1]=true;
	for (edge* i=yead[1];i!=NULL;i=i->next)
		dis[i->to]=true,fa[i->to]=1;
	for (int g=1;g<n;g++){
		int id=INF;
		for (int i=1;i<=n;i++){
			if (!vis[i] && dis[i])
				id=min(id,i);
		}vis[id]=true;
		for (edge* i=yead[id];i!=NULL;i=i->next){
			if (!vis[i->to]) dis[i->to]=true,fa[i->to]=id;
			//if (i->to==67) cout<<"&"<<fa[i->to]<<"^"<<id<<endl;
		}
	}
	//for (int i=2;i<=n;i++) printf("%d:%d\n",i,fa[i]); 
	for (int i=2;i<=n;i++)
		head[fa[i]]=new edge(i,head[fa[i]]);
	dfs(1);
	return 0;
}
