#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <queue>
#include <vector>
using namespace std;
#define inf (1LL<<60)
#define ll long long
#define N 200005
char type[10];
int num,to[N],pre[N],Next[N],head[N];
int a[N],flag[N];
int n,m;
ll mn;
inline void add(int x,int y)
{
	++num;
	pre[num]=x;
	to[num]=y;
	Next[num]=head[x];
	head[x]=num;
}
int check()
{
	for (int i=1;i<=num;i+=2) if ((!flag[pre[i]]) && (!flag[to[i]])) return 0;
	return 1;
}
void dfs(int k,ll cost)
{
	if (k>n)
	{
		if (check()) mn=min(mn,cost);
		return;
	}
	if (flag[k]!=-1) {dfs(k+1,cost+flag[k]*a[k]);return;}
	flag[k]=0;dfs(k+1,cost);
	flag[k]=1;dfs(k+1,cost+a[k]);
	flag[k]=-1;
}
void solve()
{
	int aa,x,bb,y;
	scanf("%d%d%d%d",&aa,&x,&bb,&y);
	for (int i=1;i<=n;++i) flag[i]=-1;
	flag[aa]=x;flag[bb]=y;
	mn=inf;
	dfs(1,0LL);
	if (mn==inf) mn=-1;
	printf("%lld\n",mn);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int x,y,Q;
	cin>>n>>Q;scanf("%s",type);
	for (int i=1;i<=n;++i) scanf("%d",&a[i]);
	for (int i=1;i<n;++i)
	{
		scanf("%d%d",&x,&y);
		add(x,y);add(y,x);
	}
	while (Q--) solve();
	fclose(stdin);fclose(stdout);
	return 0;
}
