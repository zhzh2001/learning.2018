#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <queue>
#include <vector>
using namespace std;
#define p 1000000007
#define ll long long
int s[205];
int a[10][10];
int n,m,K;
ll tot=0LL;
void dfss(int x,int y,int sta,int ss)
{
	if (x==n && y==m)
	{
		//cout<<"st="<<sta<<' '<<ss<<endl;
		s[sta]=ss;
		return;
	}
	if (x<n) dfss(x+1,y,sta<<1,ss<<1|a[x][y]);
	if (y<m) dfss(x,y+1,sta<<1|1,ss<<1|a[x][y]);
}
int check()
{
	K=0;
	memset(s,-1,sizeof(s));
	dfss(1,1,0,0);
	int pre=-1;
	for (int i=0;i<(1<<(n+m-2))-1;++i)
	if (s[i]!=-1)
	{
		if (s[i]>pre && pre!=-1) return 0;
		pre=s[i];
	}
	/*for (int i=1;i<=n;++i)
	{
		//for (int j=1;j<=m;++j) cout<<a[i][j]<<' ';cout<<endl;
	}
	cout<<endl;*/
	return 1;
}
void dfs(int x,int y)
{
	if (x>n)
	{
		tot+=check();
		return;
	}
	int nx=x,ny=y+1;
	if (ny>m) nx+=1,ny=1;
	a[x][y]=0;
	dfs(nx,ny);
	a[x][y]=1;
	dfs(nx,ny);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	tot=0;
	if (m>4)
	{
		ll t=1LL;
		if (n==2){
		for (int i=1;i<m;++i) t=t*3%p;}else
		{
			t=28LL;
			for (int i=4;i<=m;++i) t=t*3%p;
		}
		cout<<(t*4)%p<<endl;
	}else
	{
	//system("pause");
	dfs(1,1);
	cout<<(tot)%p<<endl;
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
