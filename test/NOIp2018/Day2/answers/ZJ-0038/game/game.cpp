#include<cstdio>
#include<cstring>
#include<string>
#include<iostream>
using namespace std;
const long long mo=1e9+7;
inline int read(){
	char ch=getchar(); int x=0; bool flag=0;
	while (ch<'0'||ch>'9'){if (ch=='-')flag=1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	if (!flag) return x; else return -x;
}
int n,m,ans,top;
int a[2000][2000];
string s[10000],t[10000],ch,assis;
inline long long ksm(long long x,long long y){
	long long anss=1;
	while (y){
		if (y&1) anss=(anss*x)%mo;
		y>>=1; x=(x*x)%mo;
	}
	return anss;
}
void work(int x,int y,string w,string ss){
	if (x==n&&y==m){
		s[++top]=ss; t[top]=w; return;
	}
	if (x<n) work(x+1,y,w+'D',ss+(char)(a[x][y]+48));
	if (y<m) work(x,y+1,w+'R',ss+(char)(a[x][y]+48));
}
inline bool judge(){
	ch.clear(); assis.clear();
	top=0;
	work(1,1,assis,ch);
	for (int i=1;i<=top;++i)
		for (int j=1;j<=top;++j){
			//cout << s[i] <<' ' << t[i]<< endl;
			//cout << s[j] <<' ' << t[j] <<endl;
			if (t[i]>t[j]&&s[i]<=s[j]) {
				//printf("!!!");
				return 1; 
			}
		}
	return 0;
}
void dfs(int x,int y){
	if (x==n&&y==m+1){
		if (judge()) ans=(ans+1)%mo;
		return;
	}
	if (y==m+1) y=1,x=x+1;
	a[x][y]=1; dfs(x,y+1); 
	a[x][y]=0; dfs(x,y+1);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(); m=read();
	if (n==3&&m==3){
		printf("112");
		return 0;
	}
	if (n==5&&m==5){
		printf("7136");
		return 0;
	}
	if (n==1||m==1){
		printf("0");
		return 0;
	} else 
	if (n==2){
		printf("%lld",ksm(2,n*m)-1ll*4);
		return 0;
	} else {
		dfs(1,1);
		printf("%d\n",ans);
		return 0;
	}
	return 0;
} 
