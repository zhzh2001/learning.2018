#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
#include<string>
using namespace std;
const int MAXM=10005;
int n,m,x,y,top,lx,ly,len;
int nxt[MAXM],fir[MAXM],get[MAXM];
int sp[MAXM];
priority_queue <int ,vector<int>,greater<int> >q;
int edge[5001][5001],size[50001],fa[50001];
int key[50001][5];
string s,t;
bool vis[MAXM],flag,inQ[MAXM];
int p[MAXM],ans[MAXM];
inline int read(){
	char ch=getchar(); int x=0; bool flag=0;
	while (ch<'0'||ch>'9'){if (ch=='-')flag=1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	if (!flag) return x; else return -x;
}
inline void add(int x,int y){get[++top]=y; nxt[top]=fir[x]; fir[x]=top;}
bool comp(const int&x,const int &y){return x<y;}
void dfs(int x,int fa){
	if (vis[x]) return; vis[x]=1; printf("%d ",x);
	for (int i=1;i<=size[x];++i)
	dfs(edge[x][i],x);
}
void find(int x,int f){
	//printf("x=%d\n",x);
	if (flag) return; vis[x]=1; fa[x]=f; 
	for (int i=fir[x];i;i=nxt[i]){
		int v=get[i];
		if (flag) return;
		if (v==f) continue;
		if (vis[v]){
			//printf("x=%d v=%d flag=%d \n",x,v,flag);
			key[++len][1]=x; key[len][2]=v; int now=x;
			while (now!=v){
				key[++len][1]=now; key[len][2]=fa[now];
				now=fa[now];
			}
			flag=1;
		}
		find(v,x);
	}
}
void dfs1(int x,int fa,int l,int r){
	if (vis[x]) return; vis[x]=1; p[++len]=x;
	for (int i=1;i<=size[x];++i){
		if ((edge[x][i]==l&&x==r)||(x==l&&edge[x][i]==r)) continue;
		dfs1(edge[x][i],x,l,r);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(); m=read();
	for (int i=1;i<=m;++i){
		x=read(); y=read();
		add(x,y); add(y,x);
	}
	for (int i=1;i<=n;++i){
		for (int j=fir[i];j;j=nxt[j]){
			int v=get[j];
			edge[i][++size[i]]=v;
		}	
		sort(edge[i]+1,edge[i]+1+size[i],comp);
	}
	if (m==n-1){
		dfs(1,0);
		return 0;
	} else 
	if (m==n){ flag=0;
		memset(vis,0,sizeof(vis));
		find(1,0);
		t.clear();
		memset(vis,0,sizeof(vis)); len=0;
		dfs1(1,0,key[1][1],key[1][2]);
		for (int i=1;i<=n;++i) ans[i]=p[i];
		memset(vis,0,sizeof(vis));
		for (int i=2;i<=len;++i){
			t.clear();memset(vis,0,sizeof(vis)); len=0; memset(p,0,sizeof(p));
			dfs1(1,0,key[i][1],key[i][2]);
			for (int j=1;j<=n;++j){
				if (p[j]<ans[j]){
					for (int k=1;k<=n;++k) ans[k]=p[k];
					break;
				}
				if (p[j]>ans[j]) break;
			}
		}
		for (int i=1;i<=n;++i) printf("%d ",ans[i]);
		return 0;
	}
	return 0;
}
