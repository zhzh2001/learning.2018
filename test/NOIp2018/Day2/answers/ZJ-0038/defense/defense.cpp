#include<cstdio>
#include<cstring>
#include<string>
#include<iostream>
using namespace std;
const int MAXN=100005;
const int MAXM=200010;
struct node{
	int x,id,sp;
}q[MAXM];
int n,m,cnt;
int val[MAXM],top,a,b,c,d;
int fir[MAXM],nxt[MAXM],get[MAXM],fa[MAXM],son[MAXM];
int judge[MAXM],anss,ans;
bool flag;
int f[MAXM],g[MAXM];
string type;
inline int read(){
	char ch=getchar(); int x=0; bool flag=0;
	while (ch<'0'||ch>'9'){if (ch=='-')flag=1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	if (!flag) return x; else return -x;
}
inline int max(int x,int y){if (x>y) return x; else return y;}
inline int min(int x,int y){if (x<y) return x; else return y;}
void dfs(int x,int f){
	fa[x]=f; son[f]=x;
	for (int i=fir[x];i;i=nxt[i]){
		int v=get[i];
		if (v==f) continue;
		dfs(v,x);
	}
}
inline void add(int x,int y){get[++top]=y; nxt[top]=fir[x]; fir[x]=top;}
inline int abs(int x){if (x<0) return -x; else return x;}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(); m=read(); cin >>type;
	for (int i=1;i<=n;++i) val[i]=read();
	for (int i=1,x,y;i<n;++i){
		x=read(); y=read();  if (abs(x-y)!=1) flag=1;
		add(x,y); add(y,x);
	}
	if (!flag){
		for (int l=1;l<=m;++l){
			memset(judge,0,sizeof(judge)); ans=20000000; top=0; memset(q,0,sizeof(q)); 
			a=read(); b=read(); c=read(); d=read();
			if (b==0) judge[a]=-1; else judge[a]=1;
			if (d==0) judge[c]=-1; else judge[c]=1;
			dfs(1,0); int cur=1;
			for (int i=1;i<=n;++i){
				q[i].x=val[cur]; q[i].id=cur; 
				q[i].sp=judge[cur];
				cur=son[cur];
			}
			for (int i=1;i<=n;++i){
				f[i]=g[i]=200000000;
			}
			f[0]=0; g[0]=0;
			for (int i=1;i<=n;++i){
				if (q[i].sp!=-1) f[i]=min(g[i-1]+q[i].x,f[i-1]+q[i].x);
				if (q[i].sp==1) g[i]=min(g[i-1]+q[i].x,f[i-1]+q[i].x); else 
				if (q[i].sp==-1) g[i]=f[i-1]; else
				if (q[i].sp==0) g[i]=min(g[i-1]+q[i].x,f[i-1]);
			}
			if (f[n]>=200000000&&g[n]>=200000000) printf("-1\n"); else 
			printf("%d\n",min(f[n],g[n]));
		}
		return 0;
	}
	return 0;
}
