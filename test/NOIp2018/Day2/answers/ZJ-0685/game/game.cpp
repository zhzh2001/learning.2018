#include <bits/stdc++.h>
using namespace std;
#define ll long long
const ll N=1e6+7,P=1000000007;

#define C getchar()-48
ll read()
{
  ll s=0,t=1,k=C;
  for (;k<0||9<k;k=C) if (k==-3) t=-1;
  for (;0<=k&&k<=9;k=C) s=(s<<1)+(s<<3)+k;
  return s*t;
}

  ll n,m;
void in()
{
  cin>>n>>m;
}

  ll L,f;
  ll a[9][9];
void che(ll x,ll y,ll z)
{
  if (!f) return;
  if (x==n&&y==m)
    {
      if (z<L) f=0;
      L=z;
      return;
    }
  if (y<m) che(x,y+1,z<<1|a[x][y+1]);
  if (x<n) che(x+1,y,z<<1|a[x+1][y]);
}

  ll A;
void dfs(ll x,ll y)
{
  if (x*m-m+y>n*m)
    {
      f=1;
      L=0;
      che(1,1,0);
      A+=f;
      return;
    }
  for (ll i=2;~--i;)
    a[x][y]=i,
    dfs(x+y/m,y%m+1);
}

void w2()
{
  int a,b,c;
  if (n==1) a=2,c=2,b=m-1;
  if (n==2) a=3,c=4,b=m-1;
  if (n==3) a=3,c=112,b=m-3;
  for (;b;b>>=1,a=a*a%P)
    if (b&1) c=c*a%P;
  A=c;
}

void sol()
{
  if (n*m<=20) dfs(1,1);
  else
  if (n<4) w2();
  else
  A=rand();
}

void out()
{
  cout<<A;
}

int main()
{
  freopen("game.in","r",stdin);
  freopen("game.out","w",stdout);
  in();
  sol();
  out();
  fclose(stdin);
  fclose(stdout);
  exit(0);
}
