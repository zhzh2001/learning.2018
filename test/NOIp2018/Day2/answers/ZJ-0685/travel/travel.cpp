#include <bits/stdc++.h>
using namespace std;
const int N=5007;

#define C getchar()-48
int read()
{
  int s=0,t=1,k=C;
  for (;k<0||9<k;k=C) if (k==-3) t=-1;
  for (;0<=k&&k<=9;k=C) s=(s<<1)+(s<<3)+k;
  return s*t;
}

  int t[N];
  int e[N][N];
void ins()
{
  int x=read(),y=read();
  e[x][++t[x]]=y;
  e[y][++t[y]]=x;
}

  int n,m;
void in()
{
  cin>>n>>m;
  for (int i=0;++i<=m;)
    ins();
}

  int A[N],B[N];
  int Z;

void dfs(int k,int F,int U,int V)
{
  B[++Z]=k;
  for (int i=0;++i<=t[k];)
    {
      if (e[k][i]==F)
        continue;
      if (k==U&&e[k][i]==V)
        continue;
      if (k==V&&e[k][i]==U)
        continue;
      dfs(e[k][i],k,U,V);
    }
}

void ud()
{
  int f=1;
  for (int i=0;++i<=n;)
    {
      if (A[i]<B[i]&&f) return;
      if (A[i]>B[i]) f=0;
      A[i]=B[i];
    }
}

  int g=1,Q;
  int z[N],v[N],q[N];
void fin(int k,int F)
{
  if (!g) return;
  z[++Z]=k;
  if (v[k])
    {
      g=0;
      do{
        q[++Q]=z[Z];
        --Z;
      }while (z[Z]^k);
      return;
    }
  v[k]=1;
  for (int i=0;++i<=t[k];)
    {
      if (e[k][i]==F)
        continue;
      fin(e[k][i],k);
    }
  --Z;
}

void wor()
{
  Z=0;
  fin(1,0);
  q[Q+1]=q[1];
  for (int i=0;++i<=Q;)
    Z=0,dfs(1,0,q[i],q[i+1]),ud();
}

void sol()
{
  memset(A,10,sizeof A);
  for (int i=0;++i<=n;)
    sort(e[i]+1,e[i]+t[i]+1);
  if (n^m)
    dfs(1,0,0,0),ud();
  else
    wor();
}

void out()
{
  for (int i=0;++i<=n;)
    printf("%d ",A[i]);
}

int main()
{
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  in();
  sol();
  out();
  fclose(stdin);
  fclose(stdout);
  exit(0);
}
