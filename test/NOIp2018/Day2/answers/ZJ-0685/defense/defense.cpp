#include <bits/stdc++.h>
using namespace std;
#define ll long long
const ll N=2e5+7;

#define C getchar()-48
ll read()
{
  ll s=0,t=1,k=C;
  for (;k<0||9<k;k=C) if (k==-3) t=-1;
  for (;0<=k&&k<=9;k=C) s=(s<<1)+(s<<3)+k;
  return s*t;
}

  ll t;
  ll h[N];
  struct hh
    {
      ll n,r;
    }e[N];
void ins()
{
  ll x=read(),y=read();
  e[++t]=(hh){h[x],y};h[x]=t;
  e[++t]=(hh){h[y],x};h[y]=t;
}

  ll n,m;
  ll v[N];
void in()
{
  cin>>n>>m;
  read();
  for (ll i=0;++i<=n;)
    v[i]=read();
  for (ll i=0;++i<n;)
    ins();
}

  ll f[N];
#define P(i) e[i].r
void dfs(ll k,ll F)
{
  f[k]=F;
  for (ll i=h[k];i;i=e[i].n)
    if (P(i)^F) dfs(P(i),k);
}

  ll a,b,x,y;
  ll g[N][2];
void dp(ll k,ll F)
{
  ll S=0,M=P(h[k]);
  for (ll i=h[k];i;i=e[i].n)
    if (P(i)^F)
      {
        dp(P(i),k);
        S+=g[P(i)][0];
        if ( (g[M][1]-g[M][0]) > (g[P(i)][1]-g[P(i)][0]) )
          M=P(i);
      }
  g[k][1]=S+v[k];
  if (a==k) g[k][x^1]=1e17;
  if (b==k) g[k][y^1]=1e17;
  g[k][0]=S-g[M][0]+g[M][1];
  if (a==k) g[k][x^1]=1e17;
  if (b==k) g[k][y^1]=1e17;
  g[k][0]=min(g[k][1],g[k][0]);
}

void wor()
{
  memset(g,0,sizeof g);
  dp(1,0);
  if (a==1) printf("%lld\n",g[1][x]);
  else
  if (b==1) printf("%lld\n",g[1][y]);
  else
  printf("%lld\n",g[1][0]);
}

  ll A;
void sol()
{
  dfs(1,0);
  while (m--)
    {
      a=read(),x=read(),
      b=read(),y=read();
      if (x+y==0&&(f[a]==b||f[b]==a))
        puts("-1");
      else
        wor();
    }
}

int main()
{
  freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout);
  in();
  sol();
  fclose(stdin);
  fclose(stdout);
  exit(0);
}
