#include <cstdio>
using namespace std;
int n,m;
int u[2010],v[2010],p[2010];
int a,x,b,y;
long long ans,anst;
char tp[10];
bool mark[2010];
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d %d %s",&n,&m,tp);
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++) scanf("%d %d",&u[i],&v[i]);
	if (n<=10)
	{
		while (m--){
			scanf("%d %d %d %d",&a,&x,&b,&y);
			ans=-1;
			int d=(1<<n);
			for (int i=1;i<d;i++)
			{
				int tmp=i,pos=0;
				while (pos<n) {mark[++pos]=(tmp&1),tmp>>=1;}
				bool flag=1;
				for (int j=1;j<n;j++)
					if (!(mark[u[j]] || mark[v[j]])) {flag=0; break;}
				if (mark[a]!=x || mark[b]!=y) flag=0;
				if (!flag) continue;
				anst=0;
				//for (int j=1;j<=n;j++) if (mark[j]) printf("1 "); else printf("0 ");
				//puts("");
 				for (int j=1;j<=n;j++) if (mark[j]) anst+=p[j];
				if (ans==-1 || (ans!=-1 && anst<ans)) ans=anst;
			}
			if (ans==-1) puts("-1");
			else printf("%lld\n",ans);
		}
	}
	
	return 0;
}
