#include <bits/stdc++.h>

using namespace std;

typedef long long LL;
typedef pair<int, int> PII;

#define fi first
#define se second
#define mp make_pair

const int N = 100010;
const LL INF = 0x3f3f3f3f3f3f3f3f;

int n, m;
int tot = -1, head[N];
struct Edge {
    int p, nxt;
    Edge(int p = 0, int nxt = 0) : p(p), nxt(nxt) {}
} edge[N * 2];
inline void Add(int u, int v) {
    edge[++tot] = Edge(v, head[u]);
    head[u] = tot;
    return;
}

int a, x, b, y;
int p[N];

LL f[N][2];

void DFS(int u, int fa) {
    f[u][0] = 0;
    f[u][1] = p[u];
    for (int i = head[u]; ~i; i = edge[i].nxt) {
        int v = edge[i].p;
        if (v == fa) continue;
        DFS(v, u);
        f[u][0] = min(f[u][0] + f[v][1], INF);
        f[u][1] = min(f[u][1] + min(f[v][0], f[v][1]), INF);
    }
    if (u == a) f[u][x ^ 1] = INF;
    if (u == b) f[u][y ^ 1] = INF;
    return;
}

namespace ST {
    struct Info {
        LL f[2][2];
        Info(int x = 0) {
            memset(f, 0x3f, sizeof(f));
            f[0][0] = 0;
            f[1][1] = x;
            return;
        }
    };
    Info operator + (Info a, Info b) {
        Info c;
        memset(c.f, 0x3f, sizeof(c.f));
        for (int i = 0; i < 2; ++i)
            for (int j = 0; j < 2; ++j)
                c.f[i][j] = min(a.f[i][0] + b.f[1][j], min(a.f[i][1] + b.f[0][j], a.f[i][1] + b.f[1][j]));
        return c;
    }
    struct Node {
        Info info;
    } p[N * 4];
#define lc k << 1
#define rc k << 1 | 1
    void Build(int k, int l, int r) {
        if (l == r) {
            p[k].info = Info(::p[l]);
            return;
        }
        int m = (l + r) / 2;
        Build(lc, l, m);
        Build(rc, m + 1, r);
        p[k].info = p[lc].info + p[rc].info;
        return;
    }
    Info Query(int k, int l, int r, int L, int R) {
        if (L <= l && r <= R) return p[k].info;
        int m = (l + r) / 2;
        if (L <= m) {
            if (m < R) return Query(lc, l, m, L, R) + Query(rc, m + 1, r, L, R);
            return Query(lc, l, m, L, R);
        }
        return Query(rc, m + 1, r, L, R);
    }
#undef lc
#undef rc
    
}

int main() {
    freopen("defense.in", "r", stdin);
    freopen("defense.out", "w", stdout);
    char tp[10];
    scanf("%d%d%s", &n, &m, tp);
    for (int i = 1; i <= n; ++i)
        scanf("%d", &p[i]);
    memset(head, -1, sizeof(head));
    for (int i = 1; i < n; ++i) {
        int u, v;
        scanf("%d%d", &u, &v);
        Add(u, v);
        Add(v, u);
    }
    if (n <= 2000 && m <= 2000) {
        while (m--) {
            scanf("%d%d%d%d", &a, &x, &b, &y);
            DFS(1, 0);
            LL res = min(f[1][0], f[1][1]);
            if (res < INF) printf("%lld\n", res);
            else puts("-1");
        }
        return 0;
    }
    if (tp[0] == 'A') {
        ST::Build(1, 1, n);
        while (m--) {
            int a, x, b, y;
            scanf("%d%d%d%d", &a, &x, &b, &y);
            if (a > b) {
                swap(a, b);
                swap(x, y);
            }
            if (a + 1 == b && !x && !y) {
                puts("-1");
                continue;
            }
            LL res = 0;
            if (x) res += p[a];
            if (y) res += p[b];
            ST::Info t;
            if (a > 1) {
                t = ST::Query(1, 1, n, 1, a - 1);
                LL ret = INF;
                for (int i = 0; i < 2; ++i)
                    for (int j = 0; j < 2; ++j)
                        if (j || x) ret = min(ret, t.f[i][j]);
                res += ret;
            }
            if (a + 1 < b) {
                t = ST::Query(1, 1, n, a + 1, b - 1);
                LL ret = INF;
                for (int i = 0; i < 2; ++i)
                    for (int j = 0; j < 2; ++j)
                        if ((x || i) && (j || y)) ret = min(ret, t.f[i][j]);
                res += ret;
            }
            if (b < n) {
                t = ST::Query(1, 1, n, b + 1, n);
                LL ret = INF;
                for (int i = 0; i < 2; ++i)
                    for (int j = 0; j < 2; ++j)
                        if (y || i) ret = min(ret, t.f[i][j]);
                res += ret;
            }
            printf("%lld\n", res);
        }
    }
    return 0;
}
