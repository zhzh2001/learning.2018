#include <bits/stdc++.h>

using namespace std;

#define fi first
#define se second
#define mp make_pair

typedef pair<int, int> PII;

const int N = 5005;

map<PII, int> hmp;

int n, m;
int tot = -1, head[N];
struct Edge {
    int p, nxt;
    Edge(int p = 0, int nxt = 0) : p(p), nxt(nxt) {}
} edge[N * 2];
inline void Add(int u, int v) {
    edge[++tot] = Edge(v, head[u]);
    head[u] = tot;
    return;
}

PII w[N * 2];
int k1, k2;
int a[N], b[N];

void DFS(int u, int fa) {
    a[++*a] = u;
    for (int i = head[u]; ~i; i = edge[i].nxt) {
        int v = edge[i].p;
        if (v == fa || i == k1 || i == k2) continue;
        DFS(v, u);
    }
    return;
}

int l, r;
PII t[N];
int v[N];

bool DFS(int u, int fa, int dep) {
    v[u] = dep;
    for (int i = head[u]; ~i; i = edge[i].nxt) {
        int v = edge[i].p;
        if (v == fa) continue;
        t[dep] = mp(u, v);
        if (::v[v]) {
            l = ::v[v];
            r = dep;
            return 1;
        }
        if (DFS(v, u, dep + 1)) return 1;
    }
    return 0;
}

int main() {
    freopen("travel.in", "r", stdin);
    freopen("travel.out", "w", stdout);
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= m; ++i) {
        int u, v;
        scanf("%d%d", &u, &v);
        w[i * 2 - 1] = mp(u, v);
        w[i * 2] = mp(v, u);
    }
    sort(w + 1, w + m * 2 + 1);
    memset(head, -1, sizeof(head));
    for (int i = m * 2; i; --i) {
        Add(w[i].se, w[i].fi);
        hmp[mp(w[i].se, w[i].fi)] = tot;
    }
    if (m == n - 1) {
        k1 = k2 = -1;
        *a = 0;
        DFS(1, 0);
        for (int i = 1; i <= n; ++i)
            printf("%d ", a[i]);
        puts("");
        return 0;
    }
    DFS(1, 0, 1);
    b[1] = n + 1;
    for (int j = l; j <= r; ++j) {
        k1 = hmp[::t[j]];
        k2 = hmp[mp(::t[j].se, ::t[j].fi)];
        *a = 0;
        DFS(1, 0);
        for (int i = 1; i <= n; ++i)
            if (a[i] < b[i]) {
                for (int k = 1; k <= n; ++k)
                    b[k] = a[k];
                break;
            } else if (a[i] > b[i]) break;
    }
    for (int i = 1; i <= n; ++i)
        printf("%d ", b[i]);
    puts("");
    return 0;
}
