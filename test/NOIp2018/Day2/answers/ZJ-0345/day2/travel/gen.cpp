#include <bits/stdc++.h>

using namespace std;

int main() {
    srand(time(0));
    for (int i = 1; i <= 100; ++i)
        srand(time(0) * clock() + rand());
    int n = 5000, m = n;
    printf("%d %d\n", n, m);
    for (int i = 1; i < n; ++i)
        printf("%d %d\n", i, i + 1);
    printf("%d %d\n", n, 1);
    return 0;
}
