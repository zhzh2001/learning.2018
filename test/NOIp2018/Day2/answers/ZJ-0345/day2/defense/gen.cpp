#include <bits/stdc++.h>

using namespace std;

int main() {
    srand(time(0));
    for (int i = 1; i <= 100; ++i)
        srand(time(0) * clock() + rand());
    int n = 2000, m = 2000;
    printf("%d %d A3\n", n, m);
    for (int i = 1; i <= n; ++i)
        printf("%d ", rand() % 10000000 + 1);
    for (int i = 1; i < n; ++i)
        printf("%d %d\n", i, i + 1);
    while (m--) {
        int a = rand() % n + 1, x = rand() & 1, b = rand() % n + 1, y = rand() & 1;
        do b = rand() % n + 1;
        while (a == b);
        printf("%d %d %d %d\n", a, x, b, y);
    }
    cerr << rand() << endl;
    return 0;
}
