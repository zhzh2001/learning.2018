#include <bits/stdc++.h>

using namespace std;

const int N = 10;

int n, m;
int cnt = 0;

int ch[N][N];

void DFS(int x, int y) {
    if (y == m) {
        DFS(x + 1, 0);
        return;
    }
    if (x == n) {
        ++cnt;
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j)
                if (i && j + 1 < m && ch[i][j] < ch[i - 1][j + 1]) {
                    --cnt;
                    return;
                }
        return;
    }
    ch[x][y] = 0;
    DFS(x, y + 1);
    ch[x][y] = 1;
    DFS(x, y + 1);
    return;
}

int main() {
    scanf("%d%d", &n, &m);
    DFS(0, 0);
    printf("%d\n", cnt);
    return 0;
}
