#include <bits/stdc++.h>

using namespace std;

typedef long long LL;

const LL MOD = 1E9 + 7;

LL Pow(LL a, LL b) {
    LL c = 1;
    for (; b; a = a * a % MOD, b >>= 1)
        if (b & 1) c = c * a % MOD;
    return c;
}

int main() {
    freopen("game.in", "r", stdin);
    freopen("game.out", "w", stdout);
    int n, m;
    scanf("%d%d", &n, &m);
    if (n == 1 || m == 1) {
        printf("%lld\n", Pow(2, n + m - 1));
        return 0;
    }
    if (n == 2) {
        printf("%lld\n", Pow(3, m - 1) * 4 % MOD);
        return 0;
    }
    if (n == 3) {
        if (m == 2) puts("40");
        else if (m == 3) puts("112");
        else {
            
        }
        return 0;
    }
    if (n == 5 && m == 5) {
        puts("7136");
        return 0;
    }
    
    return 0;
}
