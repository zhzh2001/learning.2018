#include<bits/stdc++.h>
using namespace std;
const int mod=1e9+7;

int n, m, f[10][266], g[10][266], hisam[266][266], lodif[266][266];

inline void U(int &x,int y){
  x+=y; if (x>=mod) x-=mod;
}

int realmain(int n,int m){
  for (int s=0;s<(1<<n);++s)
    for (int t=0;t<(1<<n);++t){
      for (int i=0;i<n-1;++i)
	if ((s>>i&1)<(t>>i+1&1))
	  hisam[s][t]=-1;
      if (hisam[s][t]==-1) continue;
      hisam[s][t]=0; lodif[s][t]=233;
      for (int i=0;i<n-1;++i)
	if ((s>>i&1)^(t>>i+1&1))
	  lodif[s][t]=min(lodif[s][t],i);
	else
	  hisam[s][t]=max(hisam[s][t],i);
    }
  for (int i=0;i<(1<<n);++i) f[0][i]=1;
  for (m--;m--;){
    for (int i=0;i<=n;++i)
      for (int j=0;j<(1<<n);++j)
	g[i][j]=0;
    for (int i=0;i<=n;++i)
      for (int j=0;j<(1<<n);++j)
	if (f[i][j])
	  for (int k=0;k<(1<<n);++k)
	    if (hisam[j][k]!=-1&&lodif[j][k]>=i)
	      U(g[max(i,hisam[j][k])][k],f[i][j]);
    for (int i=0;i<=n;++i)
      for (int j=0;j<(1<<n);++j)
	f[i][j]=g[i][j];
  }
  int ans=0;
  for (int i=0;i<=n;++i)
    for (int j=0;j<(1<<n);++j)
      U(ans,f[i][j]);
  return ans;
}

int main(){
  freopen("game.in","r",stdin); freopen("game.out","w",stdout);
  cin>>n>>m;
  if (m<=100||n<=2){
    printf("%d\n",realmain(n,m));
    return 0;
  }
  int ans=realmain(n,100); m-=100;
  for (;m--;) ans=ans*3ll%mod;
  cout<<ans<<endl;
}
