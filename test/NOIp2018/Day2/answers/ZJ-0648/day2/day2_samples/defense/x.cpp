#include<bits/stdc++.h>
#define ll long long
using namespace std;

int n, m, a[101000]; string s;
ll bas;
struct R{
  int to, nex;
}r[202000];
int hea[101000], cnr, sz[101000], bs[101000];
int fa[101000], tp[101000], bot[101000], dfn[101000], dft, pos[101000];
const ll inf=1e17;


int rt[101000], cnt;
struct TR{
  int lc, rc; ll val[2][2];
  void out(){
    printf(" %lld %lld  %lld %lld\n",val[0][0],val[0][1],val[1][0],val[1][1]);
  }
}tr[404000];

inline ll Max(ll a,ll b,ll c){
  return max(a,max(b,c));
}
void U(int x){
  for (int i=0;i<=1;++i)
    for (int j=0;j<=1;++j)
      tr[x].val[i][j]=Max(
			  tr[tr[x].lc].val[i][0]+tr[tr[x].rc].val[0][j],
			  tr[tr[x].lc].val[i][0]+tr[tr[x].rc].val[1][j],
			  tr[tr[x].lc].val[i][1]+tr[tr[x].rc].val[0][j]
			  );
			  
}

void build(int &x,int l,int r){
  x=++cnt;
  if (l==r) return;
  int mid=l+r>>1;
  build(tr[x].lc,l,mid);
  build(tr[x].rc,mid+1,r);
}
void gai(int x,int l,int r,int p,ll v0,ll v1){
  if (l==r){
    tr[x].val[0][0]+=v0;
    tr[x].val[1][1]+=v1;
    tr[x].val[0][1]=tr[x].val[1][0]=-inf;
    return;
  }
  int mid=l+r>>1;
  if (p<=mid) gai(tr[x].lc,l,mid,p,v0,v1);
  else gai(tr[x].rc,mid+1,r,p,v0,v1);
  U(x);
}

ll org[101000][2];
void chkup(int x){
  if (x==1) return;
  ll t0=max(tr[rt[x]].val[0][0],tr[rt[x]].val[0][1]);
  ll t1=max(tr[rt[x]].val[1][0],tr[rt[x]].val[1][1]);
  int t=tp[fa[x]];
  gai(rt[t],dfn[t],dfn[bot[t]],dfn[fa[x]],max(t0,t1)-org[x][0],t0-org[x][1]);
  org[x][0]=max(t0,t1); org[x][1]=t0;
  //printf(" [%d] %lld %lld\n",x,t0,t1);
}

void cg(int x,ll v){
  gai(rt[tp[x]],dfn[tp[x]],dfn[bot[tp[x]]],dfn[x],0,v);
  for (x=tp[x];x!=1;){
    chkup(x); x=tp[fa[x]];
  }
}

void gaobs(int x,int fff){
  sz[x]=1; fa[x]=fff;
  for (int i=hea[x];i;i=r[i].nex){
    int y=r[i].to; if (y==fff) continue;
    gaobs(y,x); sz[x]+=sz[y];
    if (sz[y]>=sz[bs[x]]) bs[x]=y;
  }
}
void pourshit(int x,int fff,int ttt){
  tp[x]=ttt; dfn[x]=++dft; pos[dft]=x;
  bot[tp[x]]=x;
  if (bs[x]) pourshit(bs[x],x,ttt);
  else build(rt[tp[x]],dfn[tp[x]],dfn[x]);
  for (int i=hea[x];i;i=r[i].nex){
    int y=r[i].to; if (y==fa[x]||y==bs[x]) continue;
    pourshit(y,x,y);
  }
}

void getans(){
  //tr[rt[1]].out(); tr[rt[2]].out();
  ll ans=bas-max(
	     max(tr[rt[1]].val[0][0],tr[rt[1]].val[0][1]),
	     max(tr[rt[1]].val[1][0],tr[rt[1]].val[1][1])
	     );
  if (abs(ans)>inf/10) puts("-1");
  else printf("%lld\n",ans);
}

int main(){
  //freopen("defense.in","r",stdin); freopen("defense.out","w",stdout);
  cin>>n>>m>>s;
  for (int i=1;i<=n;++i) scanf("%d",&a[i]), bas+=a[i];
  int u, v;
  for (int i=1;i<n;++i){
    scanf("%d%d",&u,&v);
    r[++cnr]=(R){v,hea[u]}; hea[u]=cnr;
    r[++cnr]=(R){u,hea[v]}; hea[v]=cnr;
  }
  gaobs(1,0);
  pourshit(1,0,1);
  
  for (int i=1;i<=n;++i) cg(i,a[i]);
  //getans();// return 0;

  for (int tu, tv;m--;){
    scanf("%d%d%d%d",&u,&tu,&v,&tv);
    cg(u,tu==1? -inf: inf); if (tu==0) bas+=inf;
    cg(v,tv==1? -inf: inf); if (tv==0) bas+=inf;
    getans();
    cg(v,tv==1? inf: -inf); if (tu==0) bas-=inf;
    cg(u,tu==1? inf: -inf); if (tv==0) bas-=inf;
    //getans();
  }
}
