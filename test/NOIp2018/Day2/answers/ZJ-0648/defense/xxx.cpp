#include<bits/stdc++.h>
#define ll long long
using namespace std;

int n, m; ll a[101000], f[101000][2], bas; string s;
struct R{
  int to, nex;
}r[202000];
int hea[101000], cnr;
const ll inf=1e17;

void dfs(int x,int fff){
  f[x][0]=0; f[x][1]=a[x];
  for (int i=hea[x];i;i=r[i].nex){
    int y=r[i].to; if (y==fff) continue;
    dfs(y,x);
    f[x][0]+=max(f[y][0],f[y][1]);
    f[x][1]+=f[y][0];
  }
}

void getans(){
  dfs(1,0);
  ll ans=bas-max(f[1][0],f[1][1]);
  if (abs(ans)>inf/10) puts("-1");
  else printf("%lld\n",ans);
}

void cg(int x,ll v){
  a[x]+=v;
}

int main(){
  cin>>n>>m>>s;
  for (int i=1;i<=n;++i) scanf("%lld",&a[i]), bas+=a[i];
  int u, v;
  for (int i=1;i<n;++i){
    scanf("%d%d",&u,&v);
    r[++cnr]=(R){v,hea[u]}; hea[u]=cnr;
    r[++cnr]=(R){u,hea[v]}; hea[v]=cnr;
  }
  
  for (int tu, tv;m--;){
    scanf("%d%d%d%d",&u,&tu,&v,&tv);
    cg(u,tu==1? -inf: inf); if (tu==0) bas+=inf;
    cg(v,tv==1? -inf: inf); if (tv==0) bas+=inf;
    getans();
    cg(v,tv==1? inf: -inf); if (tu==0) bas-=inf;
    cg(u,tu==1? inf: -inf); if (tv==0) bas-=inf;
  }
}
