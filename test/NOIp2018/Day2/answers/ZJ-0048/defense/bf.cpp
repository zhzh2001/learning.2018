#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
typedef pair<int,int> pa;
const LL INF=1e18;
LL f[500010][2],ans;
char tp[555];
int tmp[500010],ts,A,B,X,Y,n,p[500010],nxt[500100],hed[500010],too[500010],nedge,m,x,y;
void ae(int x,int y){
	nxt[++nedge]=hed[x];
	hed[x]=nedge;
	too[nedge]=y;
}
void dfs(int x,int l){
	for (int i=hed[x];i;i=nxt[i]){
		int y=too[i];
		if (y==l) continue;
		dfs(y,x);
	}
	ts=0;
	f[x][0]=0;
	f[x][1]=p[x];
	for (int i=hed[x];i;i=nxt[i]){
		int y=too[i];
		if (y==l) continue;
		f[x][1]+=min(f[y][1],f[y][0]);
		f[x][0]+=f[y][1];
	}
	if (x==A) f[x][X^1]=INF;
	if (x==B) f[x][Y^1]=INF;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("bf.out","w",stdout);
	cin>>n>>m;
	scanf("%s",tp);
	FOR(i,1,n) scanf("%d",&p[i]);
	FOR(i,2,n){
		scanf("%d%d",&x,&y);
		ae(x,y),ae(y,x);
	}
	while (m--){
		scanf("%d%d%d%d",&A,&X,&B,&Y);
		dfs(1,0);
		ans=min(f[1][0],f[1][1]);
		if (ans<INF) printf("%lld\n",ans);
		else puts("-1");
	}
	return 0;
}
