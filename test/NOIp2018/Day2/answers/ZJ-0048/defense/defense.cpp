#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
typedef pair<int,int> pa;
const LL INF=1e18;
LL f[500010][2],ans;
char tp[555];
int tmp[500010],ts,A,B,X,Y,n,p[500010],nxt[500100],hed[500010],too[500010],nedge,m,x,y;
void ae(int x,int y){
	nxt[++nedge]=hed[x];
	hed[x]=nedge;
	too[nedge]=y;
}
void dfs(int x,int l){
	for (int i=hed[x];i;i=nxt[i]){
		int y=too[i];
		if (y==l) continue;
		dfs(y,x);
	}
	ts=0;
	f[x][0]=0;
	f[x][1]=p[x];
	for (int i=hed[x];i;i=nxt[i]){
		int y=too[i];
		if (y==l) continue;
		f[x][1]+=min(f[y][1],f[y][0]);
		f[x][0]+=f[y][1];
	}
	if (x==A) f[x][X^1]=INF;
	if (x==B) f[x][Y^1]=INF;
}

struct node{
	LL p[2][2];
	node(){memset(p,0,sizeof(p));}
}T[500010];

node mer(node A,node B){
	node C;
	FOR(i,0,1)
		FOR(j,0,1)
			C.p[i][j]=INF;
	FOR(k,0,1)
	FOR(l,0,1){
	if (k==0 && l==0) continue;
	FOR(i,0,1)
		FOR(j,0,1)
			C.p[i][j]=min(C.p[i][j],A.p[i][k]+B.p[l][j]);
	}
	return C;
}

void build(int l,int r,int rt){
	if (l==r){
		T[rt].p[0][1]=T[rt].p[1][0]=INF;
		T[rt].p[0][0]=0;
		T[rt].p[1][1]=p[l];
		return;
	}
	int m=l+r>>1;
	build(l,m,rt<<1);
	build(m+1,r,rt<<1|1);
	T[rt]=mer(T[rt<<1],T[rt<<1|1]);
}

node que(int l,int r,int rt,int L,int R){
	if (l>=L && r<=R) return T[rt];
	int m=l+r>>1;
	if (R<=m) return que(l,m,rt<<1,L,R);
	else if (L>m) return que(m+1,r,rt<<1|1,L,R);
	else return mer(que(l,m,rt<<1,L,R),que(m+1,r,rt<<1|1,L,R));
}
void SP(){
	build(1,n,1);
	while (m--){
		scanf("%d%d%d%d",&A,&X,&B,&Y);
		if (A>B) swap(A,B),swap(X,Y);
		node T1;
		T1.p[0][0]=T1.p[1][1]=T1.p[0][1]=T1.p[1][0]=INF;
		T1.p[X][X]=p[A]*X;
		node T2;
		T2.p[0][0]=T2.p[1][1]=T2.p[0][1]=T2.p[1][0]=INF;
		T2.p[Y][Y]=p[B]*Y;
		if (A>1) T1=mer(que(1,n,1,1,A-1),T1);
		if (B<n) T2=mer(T2,que(1,n,1,B+1,n));
		if (A+1<=B-1) T1=mer(T1,que(1,n,1,A+1,B-1));
		T1=mer(T1,T2);
		ans=min(min(T1.p[0][0],T1.p[0][1]),min(T1.p[1][0],T1.p[1][1]));
		if (ans<INF) printf("%lld\n",ans);
		else puts("-1");
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m;
	scanf("%s",tp);
	FOR(i,1,n) scanf("%d",&p[i]);
	FOR(i,2,n){
		scanf("%d%d",&x,&y);
		ae(x,y),ae(y,x);
	}
	if (n>2000){SP();return 0;}
	while (m--){
		scanf("%d%d%d%d",&A,&X,&B,&Y);
		dfs(1,0);
		ans=min(f[1][0],f[1][1]);
		if (ans<INF) printf("%lld\n",ans);
		else puts("-1");
	}
	return 0;
}
