#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
typedef pair<int,int> pa;
pa e[100010];
vector<int> v[100010];
int n,m,A,B,es,hed[500010],fa[500010],x[500010],y[500010],ans[500010],tmp[500010],ts,too[500010],nxt[500010],nedge;
int getf(int x){
	return fa[x]==x?x:fa[x]=getf(fa[x]);
}
void ae(int x,int y){
	nxt[++nedge]=hed[x];
	hed[x]=nedge;
	too[nedge]=y;
}
void ck(){
	FOR(i,1,n)
		if (tmp[i]<ans[i]){break;}
		else if (tmp[i]>ans[i]) return;
	FOR(i,1,n) ans[i]=tmp[i];
}
void dfs(int x,int l){
	tmp[++ts]=x;
	for (int i=hed[x];i;i=nxt[i]){
		int y=too[i];
		if (y==l) continue;
		if (x==A && y==B || x==B && y==A) continue;
		dfs(y,x);
	}
}
void DFS(int x,int l){
	fa[x]=l;
	if (x==B){
		int t=x;
		while (1){
			if (!fa[t]) break;
			e[++es]=mp(t,fa[t]);
			t=fa[t];
		}
	}
	for (int i=hed[x];i;i=nxt[i]){
		int y=too[i];
		if (y==l) continue;
		if (x==A && y==B || x==B && y==A) continue;
		DFS(y,x);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	FOR(i,1,n) fa[i]=i;
	FOR(i,1,m){
		scanf("%d%d",&x[i],&y[i]);
		v[x[i]].pb(y[i]);
		v[y[i]].pb(x[i]);
		int X=getf(x[i]),Y=getf(y[i]);
		if (X==Y){
			A=x[i],B=y[i];
		}
		else fa[X]=Y;
	}
	FOR(i,1,n){
		sort(v[i].begin(),v[i].end());
		reverse(v[i].begin(),v[i].end());
		for (int j=0;j<v[i].size();++j)
			ae(i,v[i][j]);
	}
	e[++es]=mp(A,B);
	memset(fa,0,sizeof(fa));
	if (m==n){
		DFS(A,0);
	}
	FOR(i,1,n) ans[i]=n-i+1;
	if (m==n-1){
		ts=0;
		dfs(1,0);
		ck();
	}
	else{
		FOR(i,1,es){
			A=e[i].fi,B=e[i].se;
			ts=0;
			dfs(1,0);
			ck();
		}
	}
	FOR(i,1,n-1) printf("%d ",ans[i]);
	printf("%d\n",ans[n]);
	return 0;
}
