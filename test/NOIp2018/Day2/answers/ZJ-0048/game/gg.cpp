#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
typedef pair<int,int> pa;
const int MO=1e9+7;
LL ans;
int n,m,c[666],s1,s2,s3,s4,f[100][260][260];
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	
	f[n+m-2][0][1<<(n-1)]=1;
	f[n+m-2][0][0]=1;
	FORD(i,n+m-1,0)
		FOR(s1,0,(1<<n)-1) //same
		FOR(s2,0,(1<<n)-1) //01
		{
			//f[i+1][s1][s2]
			if (!f[i+1][s1][s2])  continue;
			int l=0,r=n-1;
			//i-t>=0 i-t<=m-1
			r=min(r,i);
			l=max(l,i-m+1);
			FOR(j,l,r-1){
				//j j+1
				if ((s1>>(j+1))&1) c[j+1]=1;
				else c[j+1]=0;
			}
			FOR(j,0,(1<<n)-1){
				bool gg=0;
				FOR(k,0,l-1)
					if ((j>>k)&1) gg=1;
				FOR(k,r+1,n-1)
					if ((j>>k)&1) gg=1;
				FOR(k,l+1,r)
					if (c[k]){
						if (!((j>>k)&1) || ((j>>(k-1))&1) ){
							gg=1;
							break;
						} 
					}
				FOR(k,l+1,r)
					if ( ((j>>k)&1) < ((j>>(k-1))&1) ) gg=1;
				if (gg) continue;
				s4=j;
				s3=0;
				FOR(k,l,r) if ( !((s1>>k)&1) && !((s1>>(k+1))&1) && ((s2>>k)&1)==((s2>>(k+1))&1) ) s3+=0;
				else s3+=1<<k;
				(f[i][s3][s4]+=f[i+1][s1][s2])%=MO;
			}
		}
	FOR(j,0,1)
	FOR(k,0,1)
	(ans+=f[0][j][k])%=MO;
	cout<<ans<<endl;
	return 0;
}
