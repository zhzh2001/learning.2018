#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
typedef pair<int,int> pa;
const int MO=1e9+7;
LL ans;
int n,m;
LL pw(LL x,LL y){
	LL t=1;
	for (;y;y>>=1){
		if (y&1) t=t*x%MO;
		x=x*x%MO;
	}
	return t;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	if (n==1) ans=pw(2,m);
	if (n==2) ans=pw(3,m-1)*4%MO;
	if (n==3){
		if (m==1) ans=8;
		else if (m==2) ans=36;
		else if (m==3) ans=112;
		else ans=112ll*pw(3,m-3)%MO;
	}
	ans=(ans%MO+MO)%MO;
	cout<<ans<<endl;
	return 0;
}
