#include<stdio.h>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#define N 5005
using namespace std;
int tot,head[N],vet[N<<1],Next[N<<1];
void add(int x,int y){
	tot++;
	vet[tot]=y;
	Next[tot]=head[x];
	head[x]=tot;
}
bool vis[N];
int dep[N],n,m,ans[N],cnt,cut,flag,tmp[N];
void dfs(int u,int fa){
	vis[u]=true;
	flag++;
	dep[u]=dep[fa]+1;
	for(int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if(vis[v]||i==cut||i==cut-1)continue;
		dfs(v,u);
	}
}
int edgetot,Head[N];
struct edge{int next,vet;}e[N];
void addedge(int x,int y){
	edgetot++;
	e[edgetot].vet=y;
	e[edgetot].next=Head[x];
	Head[x]=edgetot;
}
void dp(int u){
	tmp[++cnt]=u;
	for(int i=Head[u];i;i=e[i].next){
		int v=e[i].vet;
		dp(v);
	}
}
int x[N],y[N],z[N],r[N];
bool cmp(int i,int j){return x[i]==x[j] ? y[i]>y[j]:x[i]<x[j];}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&x[i],&y[i]);
		add(x[i],y[i]);
		add(y[i],x[i]);
		r[i]=i;
	}
	for(int i=1;i<=n;i++)ans[i]=1e9;
	if(m==n-1){
		dfs(1,0);
		for(int i=1;i<=m;i++)
			if(dep[x[i]]>dep[y[i]])
				swap(x[i],y[i]);
		sort(r+1,r+m+1,cmp);
		for(int o=1;o<=m;o++){
			int i=r[o];
			addedge(x[i],y[i]);
		}
		dp(1);
		for(int i=1;i<=cnt;i++)
			printf("%d ",tmp[i]);
	}else{
		for(int i=1;i<=m;i++){
			flag=cnt=0;cut=i*2;
			memset(vis,false,sizeof(vis));
			dfs(1,0);
			if(flag==n){
				edgetot=0; 
				memset(Head,0,sizeof(Head));
				for(int j=1;j<=m;j++){
					if(dep[x[j]]>dep[y[j]])swap(x[j],y[j]);
					r[j]=j;
				}
				sort(r+1,r+m+1,cmp);
				for(int o=1;o<=m;o++)
					if(r[o]!=i)
						addedge(x[r[o]],y[r[o]]);
				dp(1);
				bool f=false;
				for(int i=1;i<=cnt;i++)
					if(ans[i]!=tmp[i]){
						f=ans[i]>tmp[i];
						break;
					}
				if(f)for(int i=1;i<=cnt;i++)
					ans[i]=tmp[i];
			}
		}
		for(int i=1;i<=n;i++)
			printf("%d ",ans[i]);
	}
	return 0;
}
