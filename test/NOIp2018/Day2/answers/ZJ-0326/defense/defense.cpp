#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#define inf 1e16
#define N 100006
#define ll long long
using namespace std;
int tot,head[N],vet[N<<1],Next[N<<1];
void add(int x,int y){
	tot++;
	vet[tot]=y;
	Next[tot]=head[x];
	head[x]=tot;
}
ll f[N][2];
int m,xa,pa,xb,pb,n,a[N],x[N],y[N];char p[3];
void dfs(int u,int fa){
	if(u==xa){
		if(pa==1){
			f[u][1]=a[u];f[u][0]=inf;
			for(int i=head[u];i;i=Next[i]){
				int v=vet[i];
				if(v==fa)continue;
				dfs(v,u);
				f[u][1]+=min(f[v][1],f[v][0]);
			}
		}
		if(pa==0){
			f[u][0]=0;f[u][1]=inf;
			for(int i=head[u];i;i=Next[i]){
				int v=vet[i];
				if(v==fa)continue;
				dfs(v,u);
				f[u][0]+=f[v][1];
			}
		}return;
	}
	if(u==xb){
		if(pb==1){
			f[u][1]=a[u];f[u][0]=inf;
			for(int i=head[u];i;i=Next[i]){
				int v=vet[i];
				if(v==fa)continue;
				dfs(v,u);
				f[u][1]+=min(f[v][1],f[v][0]);
			}
		}
		if(pb==0){
			f[u][0]=0;f[u][1]=inf;
			for(int i=head[u];i;i=Next[i]){
				int v=vet[i];
				if(v==fa)continue;
				dfs(v,u);
				f[u][0]+=f[v][1];
			}	
		}return;
	}
	
	f[u][1]=a[u];f[u][0]=0;
	for(int i=head[u];i;i=Next[i]){
		int v=vet[i];
		if(v==fa)continue;
		dfs(v,u);
		f[u][1]+=min(f[v][1],f[v][0]);
		f[u][0]+=f[v][1];
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,p);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	for(int i=1;i<n;i++){
		scanf("%d%d",&x[i],&y[i]);
		add(x[i],y[i]);add(y[i],x[i]);
	}
	dfs(1,0);
	if(n<=2000&&m<=2000){
		for(int i=1;i<=m;i++){
			scanf("%d%d%d%d",&xa,&pa,&xb,&pb);
			dfs(1,0);
			ll ans=min(f[1][0],f[1][1]);
			if(ans>=inf)ans=-1;
			printf("%lld\n",ans);
		}
	}
	return 0;
}
