#include<stdio.h>
#include<string.h>
#include<math.h>
#include<ctype.h>
#include<time.h>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#include<stack>
#include<set>
#include<bitset>
#include<map>
#include<string>

using namespace std;

#define bug(x) cerr<<#x<<'='<<x<<' ' 
#define debug(x) cerr<<#x<<'='<<x<<'\n' 
#define For(i,a,b) for(int i=a;i<=b;++i)
#define Ror(i,a,b) for(int i=b;a<=i;--i)

typedef long long ll;

template<class T>void rd(T&x){
	char c,f=1;x=0;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^'0');
	while(c=getchar(),isdigit(c));
	x*=f;
}

template<class T>void pf(T x){
	static int top=0,stk[100];
	if(!x)putchar('0');
	if(x<0)putchar('-'),x=-x;
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]+'0');
}
template<class T>void pt(T x){pf(x);putchar(' ');}
template<class T>void ptn(T x){pf(x);putchar('\n');}
template<class T>void Max(T&x,T y){if(x<y)x=y;}
template<class T>void Min(T&x,T y){if(x>y)x=y;}

const int M=2005,INF=2e8;

int dp[M][2],p[M];

bool vis[M][2];

char str[100];

struct E{int t,nxt;}G[M<<1];

int h[M],E_tot;

void Add(int x,int y){
	G[++E_tot]=(E){y,h[x]};h[x]=E_tot;
	G[++E_tot]=(E){x,h[y]};h[y]=E_tot;
}

char Ty[100];

int sx,sy,tx,ty,n,m,fa[M],sz[M];

void rfs(int x,int f){
	fa[x]=f;
	sz[x]=1;
	for(int i=h[x];i;i=G[i].nxt){
		int y=G[i].t;
		if(y==f)continue;
		rfs(y,x);
		sz[x]+=sz[y];
	}
}

int dfs(int x,int f){
	if(vis[x][f])return dp[x][f];
	vis[x][f]=1;
	if(x==sx&&f!=tx)dp[x][f]=INF;
	else if(x==sy&&f!=ty)dp[x][f]=INF;
	else if(sz[x]==1){
		if(f)dp[x][f]=p[x];
		else dp[x][f]=0;
	}
	else if(f){//Ҫ���� 
		int res=0;
		for(int i=h[x];i;i=G[i].nxt){
			int y=G[i].t;
			if(y==fa[x])continue;
			res+=min(dfs(y,0),dfs(y,1));
		}
		dp[x][f]=res+p[x];
	}else{
		int res=0,dx=INF;
		for(int i=h[x];i;i=G[i].nxt){
			int y=G[i].t;
			if(y==fa[x])continue;
			res+=dfs(y,1);
		}
		dp[x][f]=res;
	}
	return dp[x][f];
	
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	rd(n);rd(m);scanf("%s",Ty);
	For(i,1,n)rd(p[i]);
	For(i,1,n-1){
		int x,y;
		rd(x);rd(y);
		Add(x,y);
	}
	
	rfs(1,0);
	
	while(m--){
		rd(sx);rd(tx);rd(sy);rd(ty);
		For(i,1,n)dp[i][1]=dp[i][0]=INF;
		memset(vis,0,sizeof(vis));
		int ans=min(dfs(1,0),dfs(1,1));
		if(ans<INF)ptn(ans);
		else ptn(-1);
	}
	return 0;
}
