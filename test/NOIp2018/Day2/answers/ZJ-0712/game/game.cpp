#include<stdio.h>
#include<string.h>
#include<math.h>
#include<ctype.h>
#include<time.h>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#include<stack>
#include<set>
#include<bitset>
#include<map>
#include<string>

using namespace std;

#define bug(x) cerr<<#x<<'='<<x<<' ' 
#define debug(x) cerr<<#x<<'='<<x<<'\n' 
#define For(i,a,b) for(int i=a;i<=b;++i)
#define Ror(i,a,b) for(int i=b;a<=i;--i)

typedef long long ll;

template<class T>void rd(T&x){
	char c,f=1;x=0;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^'0');
	while(c=getchar(),isdigit(c));
	x*=f;
}

template<class T>void pf(T x){
	static int top=0,stk[100];
	if(!x)putchar('0');
	if(x<0)putchar('-'),x=-x;
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]+'0');
}
template<class T>void pt(T x){pf(x);putchar(' ');}
template<class T>void ptn(T x){pf(x);putchar('\n');}
template<class T>void Max(T&x,T y){if(x<y)x=y;}
template<class T>void Min(T&x,T y){if(x>y)x=y;}

const int M=1000005,P=1e9+7;

const int N=10005;

int m,n;

struct SHUI{
	string W[N];
	string S[N],R[25];
	bool v[25][25];
	int cnt;
	void dfs(int x,int y,string w,string s){
		if(x==n-1&&y==m-1){
			cnt++;
			W[cnt]=w;
			S[cnt]=s;
			return;
		}
		if(x<n-1)dfs(x+1,y,w+"D",s+R[v[x+1][y]]);
		if(y<m-1)dfs(x,y+1,w+"R",s+R[v[x][y+1]]);
	}
	void work(){
		int mask=(1<<(n*m))-1;
		R[0]="0";
		R[1]="1";
		int ans=0;
		For(s,0,mask){
			cnt=0;
			For(i,0,n*m-1){
				int x=i/3,y=i%3;
				if(s&(1<<i))v[x][y]=1;
				else v[x][y]=0;
			}
			dfs(0,0,"",R[v[0][0]]);
			bool f=1;
			For(i,1,cnt)For(j,1,cnt)if(W[i]>W[j]&&S[i]>S[j])f=0;
			ans+=f;
		}
		ptn(ans);
	}	
}P1;

struct P2{
	void work(){
		if(m==1)ptn(4);
		else if(m==2)ptn(12);
		else if(m==3)ptn(36);
		else {
			m-=3;
			int ans=36;
			while(m--)ans=1ll*ans*4%P;
			ptn(ans);
		}
	}
}P2;


void teae(){
	int x,stk[100],top=0;
	rd(x);
	while(x)stk[++top]=x%2,x/=2;
	while(top)putchar(stk[top--]+'0');
	putchar('\n');
}



struct P3{
	void work(){
		if(m==1)ptn(8);
		else if(m==2)ptn(24);
		else if(m==3)ptn(112);
		else if(m==4)ptn(496);
		else if(m==5)ptn(3968);
		else if(m==6)ptn(31744);
		else {
			int ans=3968;
			m-=5;
			while(m--)ans=1ll*ans*8%P;
			ptn(ans);
		}
	}
}P3;

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	rd(n);rd(m);
	if(n==2)P2.work();
	else if(n==3)P3.work();
	else P1.work();
	return 0;
}
