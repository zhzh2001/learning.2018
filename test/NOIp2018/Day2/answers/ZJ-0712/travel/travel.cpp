#include<stdio.h>
#include<string.h>
#include<math.h>
#include<ctype.h>
#include<time.h>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#include<stack>
#include<set>
#include<bitset>
#include<map>
#include<string>

using namespace std;

#define bug(x) cerr<<#x<<'='<<x<<' ' 
#define debug(x) cerr<<#x<<'='<<x<<'\n' 
#define For(i,a,b) for(int i=a;i<=b;++i)
#define Ror(i,a,b) for(int i=b;a<=i;--i)

typedef long long ll;

template<class T>void rd(T&x){
	char c,f=1;x=0;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^'0');
	while(c=getchar(),isdigit(c));
	x*=f;
}

template<class T>void pf(T x){
	static int top=0,stk[100];
	if(!x)putchar('0');
	if(x<0)putchar('-'),x=-x;
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]+'0');
}
template<class T>void pt(T x){pf(x);putchar(' ');}
template<class T>void ptn(T x){pf(x);putchar('\n');}
template<class T>void Max(T&x,T y){if(x<y)x=y;}
template<class T>void Min(T&x,T y){if(x>y)x=y;}

const int M=5005;

struct node{
	int x,y,id;
	bool operator<(const node&A)const{
		return y>A.y;
	}
}A[M];

struct E{int t,nxt;}G[M<<1];

int E_tot,h[M],n,m,num,cnt,res[M],ans[M];

bool vis[M];

void Add(int x,int y){
	G[++E_tot]=(E){y,h[x]};h[x]=E_tot;
}

void init(int fb=0){
	cnt=0;
	E_tot=0;
	memset(h,0,sizeof(h));
	memset(vis,0,sizeof(vis));
	For(i,1,num){
		if(A[i].id==fb)continue;
		Add(A[i].x,A[i].y);
	}
}

void dfs(int x){
	if(vis[x])return;
	vis[x]=1;
	res[++cnt]=x;
	for(int i=h[x];i;i=G[i].nxt){
		int y=G[i].t;
		dfs(y);
	}
}

bool check(){
	For(i,1,n)if(ans[i]!=res[i])return res[i]<ans[i];
	return 0;
}

void solve(){
	bool f=0;
	For(s,1,m){
		init(s);
		dfs(1);
		if(cnt==n){
			if(!f){For(i,1,n)ans[i]=res[i];f=1;}
			else if(check())For(i,1,n)ans[i]=res[i];
		}
	}
	For(i,1,n)pt(ans[i]);
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	rd(n);rd(m);
	num=0;
	For(i,1,m){
		int x,y;
		rd(x);rd(y);
		A[++num]=(node){x,y,i};
		A[++num]=(node){y,x,i};
	}
	sort(A+1,A+1+num);
	init();
	if(n-1==m){
		dfs(1);
		For(i,1,n)pt(res[i]);
	}
	else solve();
	return 0;
}
