#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N = 1e5 + 10;
int n, m, val[N];
int tot, fi[N], a[N << 1], ne[N << 1];
char ty[5];
inline void Add(int x, int y) {
	a[++tot] = y; ne[tot] = fi[x]; fi[x] = tot;
}
namespace BF {
	int ff[N], f[N][2], de[N];
	inline void Dp(int x) {
		f[x][0] = 0;
		f[x][1] = val[x];
		for (int i = fi[x]; i; i = ne[i]) if (a[i] != ff[x]) {
			f[x][0] += f[a[i]][1];
			f[x][1] += min(f[a[i]][0], f[a[i]][1]);
		}
		for (int i = 0; i < 2; ++i) f[x][i] = min(f[x][i], (int)1e15);
	}
	inline void Dfs(int x, int fa) {
		ff[x] = fa; de[x] = de[fa] + 1;
		for (int i = fi[x]; i; i = ne[i]) if (a[i] != fa) Dfs(a[i], x);
		Dp(x);
	}
	inline void ReDp(int x) {
		while (x) Dp(x), x = ff[x];
	}
	inline void Force(int x, int y) {
		f[x][y ^ 1] = 1e15;
	}
	inline void Solve() {
		Dfs(1, 0);
		for (int id = 1, a, b, x, y; id <= m; ++id) {
			scanf("%lld%lld%lld%lld", &a, &x, &b, &y);
			if (de[a] < de[b]) swap(a, b), swap(x, y);
			Force(a, x); ReDp(ff[a]);
			Force(b, y); ReDp(ff[b]);
			int ans = min(f[1][0], f[1][1]);
			printf("%lld\n", ans >= 1e15 ? -1 : ans);
			ReDp(a); ReDp(b);
		}
	}
}
namespace Lian {
	const int xb = N - 5;
	struct Node {
		int f[2][2];
		inline Node(int val = 0) {
			f[0][1] = 1e15;
			f[1][0] = 1e15;
			f[1][1] = val;
			f[0][0] = 0;
		}
	} f[N << 1];
	inline Node Merge(Node u, Node v) {
		Node ans;
		for (int i = 0; i < 2; ++i)
			for (int j = 0; j < 2; ++j)
				ans.f[i][j] = min((int)1e15, min(u.f[i][1] + v.f[1][j], min(u.f[i][0] + v.f[1][j], u.f[i][1] + v.f[0][j])));
		return ans;
	}
	inline Node Ask(int x, int y) {
		Node lans, rans;
		for (int l = xb + x - 1, r = xb + y + 1; l ^ r ^ 1; l >>= 1, r >>= 1) {
			if (~l & 1) lans = Merge(lans, f[l ^ 1]);
			if ( r & 1) rans = Merge(f[r ^ 1], rans);
		}
		return Merge(lans, rans);
	}
	inline Node Force(int val, int x) {
		if (x == 1) {
			Node ans = Node(val);
			ans.f[0][0] = 1e15;
			return ans;
		} else {
			return Node(1e15);
		}
	}
	inline void Solve() {
		for (int i = 1; i <= n; ++i) f[xb + i] = Node(val[i]);
		for (int i = xb; i; --i) f[i] = Merge(f[i << 1], f[i << 1 | 1]);
		for (int id = 1, a, x, b, y; id <= m; ++id) {
			scanf("%lld%lld%lld%lld", &a, &x, &b, &y);
			if (a > b) swap(a, b), swap(x, y);
			Node ans = Merge(Ask(1, a - 1), Force(val[a], x));
			ans = Merge(ans, Ask(a + 1, b - 1));
			ans = Merge(ans, Force(val[b], y));
			ans = Merge(ans, Ask(b + 1, n));
			int tmp = min(min(ans.f[0][0], ans.f[1][1]), min(ans.f[0][1], ans.f[1][0]));
			printf("%lld\n", tmp >= 1e15 ? -1 : tmp);
		}
	}
}
signed main() {
	scanf("%lld%lld%s", &n, &m, ty);
	for (int i = 1; i <= n; ++i) scanf("%lld", val + i);
	for (int i = 1, x, y; i < n; ++i) {
		scanf("%lld%lld", &x, &y);
		Add(x, y); Add(y, x);
	}
	if (ty[0] == 'A') Lian::Solve();
	else BF::Solve();
	return 0;
}
