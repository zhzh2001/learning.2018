#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N = 1e5 + 10;
int n, m, val[N];
int tot, fi[N], a[N << 1], ne[N << 1];
char ty[5];
inline void Add(int x, int y) {
	a[++tot] = y; ne[tot] = fi[x]; fi[x] = tot;
}
namespace BF {
	int ff[N], f[N][2], de[N];
	inline void Dp(int x) {
		f[x][0] = 0;
		f[x][1] = val[x];
		for (int i = fi[x]; i; i = ne[i]) if (a[i] != ff[x]) {
			f[x][0] += f[a[i]][1];
			f[x][1] += min(f[a[i]][0], f[a[i]][1]);
		}
		for (int i = 0; i < 2; ++i) f[x][i] = min(f[x][i], (int)1e15);
	}
	inline void Dfs(int x, int fa) {
		ff[x] = fa; de[x] = de[fa] + 1;
		for (int i = fi[x]; i; i = ne[i]) if (a[i] != fa) Dfs(a[i], x);
		Dp(x);
	}
	inline void ReDp(int x) {
		while (x) Dp(x), x = ff[x];
	}
	inline void Force(int x, int y) {
		f[x][y ^ 1] = 1e15;
	}
	inline void Solve() {
		Dfs(1, 0);
		for (int id = 1, a, b, x, y; id <= m; ++id) {
			scanf("%lld%lld%lld%lld", &a, &x, &b, &y);
			if (de[a] < de[b]) swap(a, b), swap(x, y);
			Force(a, x); ReDp(ff[a]);
			Force(b, y); ReDp(ff[b]);
			int ans = min(f[1][0], f[1][1]);
			printf("%lld\n", ans >= 1e15 ? -1 : ans);
			ReDp(a); ReDp(b);
		}
	}
}
namespace Lian {
	inline void Solve() {
		BF::Solve();
	}
}
signed main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%lld%lld%s", &n, &m, ty);
	for (int i = 1; i <= n; ++i) scanf("%lld", val + i);
	for (int i = 1, x, y; i < n; ++i) {
		scanf("%lld%lld", &x, &y);
		Add(x, y); Add(y, x);
	}
	if (ty[0] == 'A') Lian::Solve();
	else BF::Solve();
	return 0;
}
