#include<bits/stdc++.h>
using namespace std;
const int N = 5050;
int n, m, banx, bany, dclk, dfx[N], ff[N], de[N], ans[N];
vector<int> g[N], cir;
inline void GetCircle(int x, int fa) {
	de[x] = de[fa] + 1; ff[x] = fa;
	for (vector<int>::iterator it = g[x].begin(); it != g[x].end(); ++it)
		if (*it != fa) {
			if (de[*it]) {
				if (de[x] > de[*it]) {
					for (int v = x; v != *it; v = ff[v]) cir.push_back(v);
					cir.push_back(*it);
				}
			} else {
				GetCircle(*it, x);
			}
		}
}
inline void Dfs(int x, int fa) {
	dfx[++dclk] = x;
	for (vector<int>::iterator it = g[x].begin(); it != g[x].end(); ++it)
		if (*it != fa && !((x == banx && *it == bany) || (x == bany && *it == banx)))
			Dfs(*it, x);
}
inline bool IsLess(int *a, int *b) {
	for (int i = 1; i <= n; ++i) if (a[i] != b[i]) return a[i] < b[i];
	return 0;
}
int main() {
	scanf("%d%d", &n, &m);
	for (int i = 1, x, y; i <= m; ++i) {
		scanf("%d%d", &x, &y);
		g[x].push_back(y);
		g[y].push_back(x);
	}
	for (int i = 1; i <= n; ++i) sort(g[i].begin(), g[i].end());
	if (m == n - 1) {
		dclk = 0; Dfs(1, 0);
		for (int i = 1; i <= n; ++i) printf("%d ", dfx[i]);
		puts("");
	} else {
		GetCircle(1, 0);
		cir.push_back(cir.front());
		ans[1] = 1e9;
		for (int i = 1; i < (int)cir.size(); ++i) {
			banx = cir[i - 1]; bany = cir[i];
			dclk = 0; Dfs(1, 0);
			if (IsLess(dfx, ans)) memcpy(ans + 1, dfx + 1, n * sizeof *ans);
		}
		for (int i = 1; i <= n; ++i) printf("%d ", ans[i]);
		puts("");
	}
	return 0;
}
