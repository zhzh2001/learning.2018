#include<bits/stdc++.h>
using namespace std;
const int N = 5050;
int n, m, banx, bany, dclk, dfx[N], ff[N], de[N], ans[N];
int tot, fi[N], a[N << 1], la[N];
vector<int> g[N], cir;
inline void GetCircle(int x, int fa) {
	de[x] = de[fa] + 1; ff[x] = fa;
	for (int i = fi[x]; i <= la[x]; ++i) if (a[i] != fa) {
		if (de[a[i]]) {
			if (de[x] > de[a[i]]) {
				for (int v = x; v != a[i]; v = ff[v]) cir.push_back(v);
				cir.push_back(a[i]);
			}
		} else {
			GetCircle(a[i], x);
		}
	}
}
inline void Dfs(int x, int fa) {
	dfx[++dclk] = x;
	for (int i = fi[x]; i <= la[x]; ++i)
		if (a[i] != fa && !((x == banx && a[i] == bany) || (x == bany && a[i] == banx)))
			Dfs(a[i], x);
}
inline bool IsLess(int *a, int *b) {
	for (int i = 1; i <= n; ++i) if (a[i] != b[i]) return a[i] < b[i];
	return 0;
}
int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1, x, y; i <= m; ++i) {
		scanf("%d%d", &x, &y);
		g[x].push_back(y);
		g[y].push_back(x);
	}
	for (int i = 1; i <= n; ++i) {
		sort(g[i].begin(), g[i].end());
		fi[i] = tot + 1;
		for (vector<int>::iterator it = g[i].begin(); it != g[i].end(); ++it)
			a[++tot] = *it;
		la[i] = tot;
	}
	if (m == n - 1) {
		dclk = 0; Dfs(1, 0);
		for (int i = 1; i <= n; ++i) printf("%d ", dfx[i]);
		puts("");
	} else {
		GetCircle(1, 0);
		cir.push_back(cir.front());
		ans[1] = 1e9;
		for (int i = 1; i < (int)cir.size(); ++i) {
			banx = cir[i - 1]; bany = cir[i];
			dclk = 0; Dfs(1, 0);
			if (IsLess(dfx, ans)) memcpy(ans + 1, dfx + 1, n * sizeof *ans);
		}
		for (int i = 1; i <= n; ++i) printf("%d ", ans[i]);
		puts("");
	}
	return 0;
}
