#include<bits/stdc++.h>
using namespace std;
const int p = 1e9 + 7;
int n, m;
inline int Qmi(int x, int y = p - 2) {
	int ans = 1;
	for (; y; y >>= 1, x = 1ll * x * x % p) if (y & 1) ans = 1ll * ans * x % p;
	return ans;
}
int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n > m) swap(n, m);
	if (n == 1) {
		printf("%d\n", Qmi(2, m));
		return 0;
	}
	if (n == 2) {
		printf("%lld\n", 4ll * Qmi(3, m - 1) % p);
		return 0;
	}
	if (n == 3) {
		printf("%d\n", 112);
		return 0;
	}
	return 0;
}
