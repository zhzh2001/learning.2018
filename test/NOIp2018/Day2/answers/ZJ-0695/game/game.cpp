#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<cmath>
#define rep(x,a,b) for(int x=(a);x<=(b);x++)
#define drp(x,a,b) for(int x=(a);x>=(b);x--)
#define cross(i,x) for(i=hd[x];i;i=nx[i])
#define ll long long
#define maxn 10005
#define mod 1000000007
using namespace std;
inline ll rd()
{
	ll x=0,f=1;
	char ch=getchar();
	for(;!isdigit(ch)&&ch!=EOF&&ch!='-';ch=getchar());
	if (ch=='-') ch=getchar(),f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
void write(ll x){if (x>=10) write(x/10),putchar(x%10+'0');else putchar(x+'0');}
inline void wrt(ll x,char ch){if (x<0) x=-x,putchar('-');write(x);if (ch) putchar(ch);}
int q[maxn],t,n,m;
int dp[1000005];
int ma[5][5],ans;
bool flag;
void dfs(int x,int y,int s)
{
	s=s<<1|ma[x][y];
	if (x==n&&y==m) {
		rep(i,1,t)
			if (s>q[t]) {flag=0;return ;}
			q[++t]=s;
		return ;
	}
	if (x<n) dfs(x+1,y,s);
	if (y<m) dfs(x,y+1,s);
}
inline void check()
{
	flag=1;t=0;dfs(1,1,0);
}
void dfs1(int x,int y)
{
	if (x>n) {check();if (flag) ans++;return ;}
	ma[x][y]=1;
	if (y==m) dfs1(x+1,1);else dfs1(x,y+1);
	ma[x][y]=0;
	if (y==m) dfs1(x+1,1);else dfs1(x,y+1);
}
inline int Mod(int x)
{
	return x>=mod?x-mod:x;
}
inline int qpow(int x,int k)
{
	int ans=1;
	for(;k;k>>=1,x=1ll*x*x%mod) if (k&1) ans=1ll*ans*x;
	return ans;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=rd(),m=rd();
	if (n<=3&&m<=3){
		dfs1(1,1);
		wrt(ans,'\n');
	}
	else {
		dp[1]=4;
		int pp=qpow(2,n-1);dp[2]=6;
		rep(i,3,m)
			pp=1ll*pp*qpow(2,n)%mod,dp[i]=Mod(dp[i-1]+pp);
		wrt(dp[m]*2,'\n');
	}
}
