#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<cmath>
#define rep(x,a,b) for(int x=(a);x<=(b);x++)
#define drp(x,a,b) for(int x=(a);x>=(b);x--)
#define cross(i,x) for(int i=hd[x];i;i=nx[i])
#define ll long long
#define maxn 5005
using namespace std;
inline ll rd()
{
	ll x=0,f=1;
	char ch=getchar();
	for(;!isdigit(ch)&&ch!=EOF&&ch!='-';ch=getchar());
	if (ch=='-') ch=getchar(),f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
void write(ll x){if (x>=10) write(x/10),putchar(x%10+'0');else putchar(x+'0');}
inline void wrt(ll x,char ch){if (x<0) x=-x,putchar('-');write(x);if (ch) putchar(ch);}
int n,m,tp;
bool vis[maxn];
int Ans[maxn],tot;
int d[maxn];
int G[maxn][maxn];
int q[maxn],head,tail;
int dis[maxn];
bool f[maxn];
int pre[maxn];
inline void dfs(int u,int fa)
{
	Ans[++tot]=u;f[u]=1;
	rep(i,1,G[u][0]) if (G[u][i]!=fa&&vis[G[u][i]])
		dfs(G[u][i],u);
}
inline void torp()
{
	rep(i,1,n)
		if (d[i]==1) q[++tail]=i,vis[i]=1,dis[i]=1;
	while(head<tail){
		int tt=q[++head];
		rep(i,1,G[tt][0]) if (!vis[G[tt][i]]){
			int v=G[tt][i];
			d[v]--;
			if (d[v]==1) q[++tail]=v,vis[v]=1,dis[v]=dis[tt]+1;
		}
	}
}
inline void back(int p)
{
	while(p!=tp){
		if (G[p][0]>=3){
			rep(i,1,G[p][0])
				if (vis[G[p][i]]&&!f[G[p][i]]) dfs(G[p][i],p);
		}
		p=pre[p];
	}
}
inline void gt(int u)
{
	int x=u;
	while(dis[x]){
		Ans[++tot]=x;f[x]=1;
		rep(i,1,G[x][0])
			if (dis[G[x][i]]>dis[x]||dis[G[x][i]]==0) {x=G[x][i];break;}
	}
	Ans[++tot]=x,f[x]=1;tp=x;
	int mx=0,se=0;
	rep(i,1,G[x][0])
		if (!vis[G[x][i]]){
			if (mx==0) mx=G[x][i];
			else if (se==0) se=G[x][i];	
		}
	rep(i,1,G[x][0]){
		if (vis[G[x][i]]&&!f[G[x][i]]) dfs(G[x][i],x);
		if (G[x][i]==mx) break;
	}
	pre[mx]=pre[se]=tp;
	x=mx;
	Ans[++tot]=x,f[x]=1;
	bool flag=1;
	while(flag){
		flag=0;
		if (G[x][0]>=3){
			rep(i,1,G[x][0]){
				if (!f[G[x][i]]&&!vis[G[x][i]]) break;
				if (vis[G[x][i]]) dfs(G[x][i],x);
			}
		}
		rep(i,1,G[x][0])
			if (!f[G[x][i]]&&G[x][i]<se&&!vis[G[x][i]])
				{pre[G[x][i]]=x,x=G[x][i],Ans[++tot]=x,f[x]=1,flag=1;break;}
	}
	back(x);x=tp;
	rep(i,1,G[x][0]){
		if (vis[G[x][i]]&&!f[G[x][i]]) dfs(G[x][i],x);
		if (G[x][i]==se) break;
	}
	x=se;
	Ans[++tot]=x;f[x]=1,flag=1;
	while(flag){
		flag=0;
		if (G[x][0]>=3){
			rep(i,1,G[x][0]){
				if (!f[G[x][i]]&&!vis[G[x][i]]) break;
				if (vis[G[x][i]]) dfs(G[x][i],x);
			}
		}
		rep(i,1,G[x][0])
			if (!f[G[x][i]]&&!vis[G[x][i]])
				{pre[G[x][i]]=x,x=G[x][i],Ans[++tot]=x,f[x]=1,flag=1;break;}
	}
	back(x);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=rd(),m=rd();
	rep(i,1,m){
		int x=rd(),y=rd();
		G[x][++G[x][0]]=y,G[y][++G[y][0]]=x;
		d[x]++,d[y]++;
	}
	rep(i,1,n)
		sort(G[i]+1,G[i]+1+G[i][0]);
	if (m==n-1){
		rep(i,1,n) vis[i]=1;
		dfs(1,1);
		rep(i,1,tot) wrt(Ans[i],' ');
	}
	else {
		torp();
		if (vis[1]==1){
			wrt(1,' ');
			rep(i,1,G[1][0])
				if (dis[G[1][i]]>dis[1]||dis[G[1][i]]==0) gt(1);
				else dfs(G[1][i],1);
			rep(i,1,tot)
				if (Ans[i]!=1) wrt(Ans[i],' ');
			putchar('\n');
		}
		else {
			tp=1;gt(1);
			rep(i,1,tot) wrt(Ans[i],' ');
		}
	}
}
