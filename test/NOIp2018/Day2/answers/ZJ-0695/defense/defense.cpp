#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<cmath>
#define rep(x,a,b) for(int x=(a);x<=(b);x++)
#define drp(x,a,b) for(int x=(a);x>=(b);x--)
#define cross(i,x) for(int i=hd[x];i;i=nx[i])
#define ll long long
#define inf 1666666666666
#define maxn 100005
using namespace std;
inline ll rd()
{
	ll x=0,f=1;
	char ch=getchar();
	for(;!isdigit(ch)&&ch!=EOF&&ch!='-';ch=getchar());
	if (ch=='-') ch=getchar(),f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
void write(ll x){if (x>=10) write(x/10),putchar(x%10+'0');else putchar(x+'0');}
inline void wrt(ll x,char ch){if (x<0) x=-x,putchar('-');write(x);if (ch) putchar(ch);}
int n,m;
char s[20];
int yq[maxn],p[maxn];
ll dp[maxn][2];
int nx[maxn<<1],to[maxn<<1],hd[maxn],cnt;
inline void add(int u,int v){nx[++cnt]=hd[u],to[cnt]=v,hd[u]=cnt;}
inline void DP1(int u,int fa)
{
	dp[u][1]=0;
	cross(i,u)
		if (to[i]!=fa)
			dp[u][1]+=min(dp[to[i]][1],dp[to[i]][0]);
	dp[u][1]+=p[u];
}
inline void DP0(int u,int fa)
{
	dp[u][0]=0;
	cross(i,u)
		if (to[i]!=fa)
			dp[u][0]+=dp[to[i]][1];
}
inline void dfs(int u,int fa)
{
	cross(i,u)
		if (to[i]!=fa) dfs(to[i],u);
	if (yq[u]!=-1){
		dp[u][yq[u]^1]=inf;
		if (yq[u]==1) DP1(u,fa);
		if (yq[u]==0) DP0(u,fa);
	}
	else DP1(u,fa),DP0(u,fa);
	//printf("%d %lld %lld\n",u,dp[u][0],dp[u][1]);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=rd(),m=rd();scanf("%s",s+1);
	rep(i,1,n) p[i]=rd();
	rep(i,1,n-1){
		int x=rd(),y=rd();
		add(x,y);add(y,x);
	}
	memset(yq,-1,sizeof yq);
	int x,y,qx,qy;
	rep(i,1,m){
		yq[x]=yq[y]=-1;
		x=rd(),qx=rd(),y=rd(),qy=rd();
		yq[x]=qx,yq[y]=qy;
		dfs(1,1);
		//rep(i,1,n)
		//	printf("%d %d\n",dp[i][0],dp[i][1]);
		if (dp[1][1]>=inf&&dp[1][0]>=inf) wrt(-1,'\n');
		else wrt(min(dp[1][1],dp[1][0]),'\n');
	}
}
