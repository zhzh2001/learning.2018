#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
int n,m,Ecnt;
ll f[100005][2],p[100005];
bool BAN0[100005],BAN1[100005];
char s[10];
struct Edge{int nxt,to;}E[200010];int head[100005];
void add(int x,int y){
	E[++Ecnt].to=y;
	E[Ecnt].nxt=head[x];
	head[x]=Ecnt;
}
void dp(int pre,int u){
	f[u][1]=f[u][0]=10000000001LL;
	ll t0=0LL,t1=0LL;
	for (int i=head[u];i;i=E[i].nxt){
		int j=E[i].to;
		if (j==pre) continue;
		dp(u,j);
		if (!BAN1[u]){
			t1+=min(f[j][0],f[j][1]);
		}
		if (!BAN0[u]){
			t0+=f[j][1];
		}
	}
	if (!BAN0[u]) f[u][0]=t0;
	if (!BAN1[u]) f[u][1]=t1+p[u];
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d %s",&n,&m,s);
	if (n>5000){
		for (int i=1;i<=m;i++) puts("-1");
		return 0;
	}
	for (int i=1;i<=n;i++) scanf("%lld",&p[i]);
	int x,y;Ecnt=0;
	for (int i=1;i<n;i++){
		scanf("%d%d",&x,&y);
		add(x,y),add(y,x);
	}
	int X1,X2;
	for (int i=1;i<=m;i++){
		scanf("%d%d%d%d",&x,&X1,&y,&X2);
		if (X1) BAN0[x]=1; else BAN1[x]=1;
		if (X2) BAN0[y]=1; else BAN1[y]=1;
		dp(0,1);
		if (min(f[1][0],f[1][1])<=10000000000LL)
			printf("%lld\n",min(f[1][0],f[1][1]));
		 else puts("-1");
		if (X1) BAN0[x]=0; else BAN1[x]=0;
		if (X2) BAN0[y]=0; else BAN1[y]=0;
	}
	return 0;
}
