#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
int read(){
	int f=1,x=0;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-48;ch=getchar();}
	return x*f;
}
int n,m,cnt,Ecnt,tmp,a[5005],b[50007],ans[5005];
int x[5005],y[5005];
bool vis[5005];
struct Edge{int nxt,to;}E[10010];int head[5005];
void add(int x,int y){
	E[++Ecnt].to=y;
	E[Ecnt].nxt=head[x];
	head[x]=Ecnt;
}
void compar(){
	if (cnt<n) return;
	if (!ans[1]){
		for (int i=1;i<=n;i++) ans[i]=a[i];
		return;
	}
	for (int i=1;i<=n;i++){
		if (ans[i]>a[i]){
			for (int j=1;j<=n;j++) ans[j]=a[j];
			return;
		}
		if (a[i]>ans[i]) return;
	}
}
void dfs(int pre,int u){
	a[++cnt]=u;int start=tmp+1;
	for (int i=head[u];i;i=E[i].nxt){
		if (E[i].to==pre) continue;
		b[++tmp]=E[i].to;
	}
	if (start>tmp) return;
	int last=tmp;
	sort(b+start,b+1+last);
	for (int i=start;i<=last;i++) dfs(u,b[i]);
}
bool huan(int pre,int u){
	vis[u]=1;
	for (int i=head[u];i;i=E[i].nxt){
		if (E[i].to==pre) continue;
		if (vis[E[i].to]) return 1;
		if (huan(u,E[i].to)) return 1;
	}
	return 0;
}
int duan(int pre,int u){
	int num=0;
	for (int i=head[u];i;i=E[i].nxt){
		if (E[i].to==pre) continue;
		num+=duan(u,E[i].to);
	}
	return num+1;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=m;i++) x[i]=read(),y[i]=read();
	if (m<n){
		Ecnt=0;cnt=0;tmp=0;
		for (int i=1;i<=m;i++) add(x[i],y[i]),add(y[i],x[i]);
		dfs(0,1);
		for (int i=1;i<n;i++) printf("%d ",a[i]);
		printf("%d\n",a[n]);
	} else{
		for (int j=1;j<=m;j++){
			Ecnt=0;
			for (int i=1;i<=n;i++) head[i]=vis[i]=0;
			for (int i=1;i<=m;i++)
				if (i!=j) add(x[i],y[i]),add(y[i],x[i]);
			if (huan(0,1) || duan(0,1)<n) continue;
			tmp=cnt=0;
			dfs(0,1);
			compar();
		}
		for (int i=1;i<n;i++) printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}
	return 0;
}
