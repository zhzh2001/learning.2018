#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
const int mod=1000000007;
int n,m,ans;
int dfs(int y,int past){
	if (y>m) return 1;
	int tt=0;
	for (int i=0;i<(1<<n);i++){
		bool ok=1;
		if (y>1){
			for (int j=1;j<n;j++)
				if (!((1<<j)&past) && ((1<<(j-1))&i)){
					ok=0;
					break;
				}
		}
		if (ok) tt=(tt+dfs(y+1,i))%mod;
	}
	return tt;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1){
		int x=1;
		for (int i=1;i<=m;i++) x=(x<<1)%mod;
		printf("%d\n",x);
		return 0;
	}
	if (m==1){
		int x=1;
		for (int i=1;i<=n;i++) x=(x<<1)%mod;
		printf("%d\n",x);
		return 0;
	}
	if (n==2){
		if (m==2){cout<<12<<endl;return 0;}
		ll x=12LL;
		for (int i=3;i<=m;i++) x=x*3LL%mod;
		printf("%lld\n",x);
		return 0;
	}
	if (n==3){
		if (m==2){cout<<36<<endl;return 0;}
		if (m==3){cout<<112<<endl;return 0;}
		if (m>3){
			ll x=112LL;
			for (int i=4;i<=m;i++) x=(x-4LL*(i-1)+mod)*4LL%mod;
			printf("%lld\n",x);
		}
	}
	if (n==5 && m==5){
		cout<<7136<<endl;
		return 0;
	}
	cout<<(1<<10)<<endl;
	return 0;
}
