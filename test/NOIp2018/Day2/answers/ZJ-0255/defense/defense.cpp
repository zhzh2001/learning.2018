#include<bits/stdc++.h>
#define ll long long
#define inf 1000000000000000000ll
#define dn(x,y) (x>(y)?x=(y):0)
#define N 100009
using namespace std;

int n,m,tot,fst[N],pnt[N<<1],nxt[N<<1],fa[N][17],c[N],d[N];
ll f[N][2],h[2],g[2];
struct matrix{ ll p[2][2]; }a[N][17],b[N];
matrix operator *(matrix a,matrix b){
	matrix c; memset(c.p,60,sizeof(c.p));
	int i,j,k;
	for (i=0; i<2; i++)
		for (j=0; j<2; j++)
			for (k=0; k<2; k++) c.p[i][k]=min(c.p[i][k],a.p[i][j]+b.p[j][k]);
	return c;	
}
void add(int x,int y){
	pnt[++tot]=y; nxt[tot]=fst[x]; fst[x]=tot;
}
void dfs(int x){
	int i,y; f[x][1]=c[x];
	for (i=1; i<17; i++) fa[x][i]=fa[fa[x][i-1]][i-1];
	for (i=fst[x]; i; i=nxt[i]){
		y=pnt[i];
		//cerr<<x<<' '<<y<<'\n';
		if (y!=fa[x][0]){
			fa[y][0]=x; d[y]=d[x]+1; dfs(y);
			f[x][0]+=f[y][1]; f[x][1]+=min(f[y][0],f[y][1]);
		}
	}
	for (i=fst[x]; i; i=nxt[i]){
		y=pnt[i];
		if (y!=fa[x][0]){
			a[y][0].p[1][1]=a[y][0].p[0][1]=f[x][1]-min(f[y][0],f[y][1]);
			a[y][0].p[1][0]=f[x][0]-f[y][1];
		}
	}
}
void dfs2(int x){
	int i,y;
	for (i=1; i<17; i++)
		a[x][i]=a[x][i-1]*a[fa[x][i-1]][i-1];
	if (x>1) b[x]=a[x][0]*b[fa[x][0]];
	for (i=fst[x]; i; i=nxt[i]){
		y=pnt[i];
		//cerr<<x<<"->"<<y<<'\n';
		if (y!=fa[x][0]) dfs2(y);
	}
}
int go(int x,int tmp){
	for (int i=0; i<17; i++) if (tmp>>i&1) x=fa[x][i];
	return x;
}	
int lca(int x,int y){
	if (d[x]<d[y]) swap(x,y);
	int i,tmp=d[x]-d[y]; x=go(x,tmp);
	for (i=16; i>=0; i--) if (fa[x][i]!=fa[y][i]){
		x=fa[x][i]; y=fa[y][i];
	}
	return x==y?x:fa[x][0];
}
ll solve(int &y,int v,int len,int u){
	matrix ans;
	memset(ans.p,0,sizeof(ans.p));
	ans.p[0][v]=f[y][v]; ans.p[0][v^1]=inf;
	for (int i=0; i<17; i++) if (len>>i&1){
		ans=ans*a[y][i];
		y=fa[y][i];	
	}
	return ans.p[0][u];
	//printf("%lld\n",ans.p[0][u]);
}
int main(){
	freopen("defense.in","r",stdin); freopen("defense.out","w",stdout);
	char ch[10];
	scanf("%d%d%s",&n,&m,ch);
	int i,j,k,x,y,z,u,v,p,q; ll ans;
	for (i=1; i<=n; i++) scanf("%d",&c[i]);
	for (i=1; i<n; i++){
		scanf("%d%d",&x,&y); add(x,y); add(y,x);
	}
	//cerr<<"ok";
	memset(a,60,sizeof(a));
	dfs(1); dfs2(1);
	//cerr<<"ok";
	while (m--){
		scanf("%d%d%d%d",&x,&u,&y,&v);
		if (x==y) continue;
		if (d[x]>d[y]){
			swap(x,y); swap(u,v);	
		}
		if (x==fa[y][0] && !u && !v){ puts("-1"); continue; }
		z=lca(x,y);
		//cerr<<z<<' '<<b[z].p[u][0]<<'\n';
		if (z==x){ printf("%lld\n",solve(y,v,d[y]-d[z],u)+min(b[z].p[u][0],b[z].p[u][1]+c[1])); continue; }
		p=x; q=y;
		g[0]=solve(p,u,d[x]-d[z]-1,0); g[1]=solve(x,u,d[x]-d[z]-1,1);
		h[0]=solve(q,v,d[y]-d[z]-1,0); h[1]=solve(y,v,d[y]-d[z]-1,1);
		//cerr<<g[0]<<' '<<g[1]<<' '<<h[0]<<' '<<h[1]<<'\n';
		ans=inf;
		for (i=0; i<2; i++)
			for (j=0; j<2; j++)
				for (k=0; k<2; k++){
					if (!i && !k) continue;
					if (!j && !k) continue;
					if (!k) dn(ans,h[i]+g[j]+f[z][0]-f[x][1]-f[y][1]+min(b[z].p[k][0],b[z].p[k][1]+c[1]));
					else dn(ans,h[i]+g[j]+f[z][1]-min(f[x][0],f[x][1])-min(f[y][0],f[y][1])+min(b[z].p[k][0],b[z].p[k][1]+c[1]));	
				}
		printf("%lld\n",ans);
	}
	return 0;
}

