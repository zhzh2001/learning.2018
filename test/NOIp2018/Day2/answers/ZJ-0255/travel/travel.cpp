#include<bits/stdc++.h>
#define vi vector<int>
#define pb push_back
#define N 5009
using namespace std;

int n,m,cnt,tot,tp,A,B,q[N],ans[N];
int fst[N],pnt[N<<1],nxt[N<<1],id[N],f[N],h[N],fa[N],d[N];
bool vis[N],ban[N<<1]; vi e[N];
void add(int x,int y){
	//cerr<<x<<' '<<y<<'\n';
	pnt[++tot]=y; nxt[tot]=fst[x]; fst[x]=tot;
}
void dfs(int x){
	int i,y; vis[x]=1; q[++tp]=x;
	//cerr<<x<<'\n';
	for (i=fst[x]; i; i=nxt[i]){
		y=pnt[i];
		//cerr<<x<<"->"<<y<<'\n';
		if (x==A && y==B || x==B && y==A) continue;
		if (!vis[y]){
			fa[y]=x; d[y]=d[x]+1; id[y]=i; dfs(y);
		} else if (y!=fa[x] && d[y]>d[x]){
			cnt=d[y]; int now=cnt;
			for (; y!=x; y=fa[y]) h[now--]=y; h[1]=x;
		}
	}
}
void check(){
	int i;
	//for (i=1; i<=n; i++) cerr<<q[i]<<' ';cerr<<'\n';
	for (i=1; i<=n; i++) if (q[i]!=ans[i]){
		if (q[i]>ans[i]) return; else break;	
	}
	memcpy(ans,q,sizeof(q));
}
void opt(){ for (int i=1; i<=n; i++) printf("%d%c",ans[i],i<n?' ':'\n'); exit(0); }
int main(){
	freopen("travel.in","r",stdin); freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	int i,x,y;
	for (i=1; i<=m; i++){
		scanf("%d%d",&x,&y);
		e[x].pb(y); e[y].pb(x);
	}
	for (i=1; i<=n; i++) sort(e[i].begin(),e[i].end());
	tot=1;
	for (i=1; i<=n; i++)
		for (int j=e[i].size()-1; j>=0; j--) add(i,e[i][j]);
	ans[1]=n+1; 
	dfs(1); check();
	if (m==n-1) opt();
	for (i=1; i<=n; i++) f[i]=id[i];
	for (i=2; i<=cnt; i++){
		//x=f[h[i]];
		//cerr<<' '<<h[i]<<' '<<h[i-1]<<' '<<x<<'\n';
		//cerr<<pnt[x]<<' '<<pnt[x^1]<<'\n';
		//ban[x]=ban[x^1]=1;
		A=h[i]; B=h[i-1];
		memset(vis,0,sizeof(vis)); memset(fa,0,sizeof(fa));
		tp=0; dfs(1); check();
		//ban[x]=ban[x^1]=0;
	}
	//cerr<<"ok\n";
	opt();
	return 0;
}

