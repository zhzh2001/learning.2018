#include<bits/stdc++.h>
#define ll long long
#define mod 1000000007
#define mpr make_pair
#define fi first
#define se second
#define pii pair<int,int>
using namespace std;

int m,n,dp[2][10009][9];
set<pii > s[2];
set<pii >:: iterator it;
int gao(int x){
	return x<n?(1<<x+1):0;
}
int main(){
	freopen("game.in","r",stdin); freopen("game.out","w",stdout);
	scanf("%d%d",&m,&n); if (m<n) swap(m,n);
	if (n==1){
		int ans=1; while (m--) ans=(ans<<1)%mod;
		printf("%d\n",ans); return 0;
	}/*
	if (n==2){
		if (m==2) puts("12"); else if (m==3) puts("36");
		return 0;
	}
	if (n==3 && m==3){ puts("112"); return 0; }*/
	s[0].insert(mpr((1<<n+1)-1,0)); dp[0][(1<<n+1)-1][0]=1;
	int i,j,k=0,last,now=0;
	for (i=1; i<=m+n-1; i++){
		int l=max(0,i-m),r=min(i,n);
		last=now; now^=1;
		s[now].clear();
		for (it=s[last].begin(); it!=s[last].end(); it++){
			pii x=*it;
			int &tmp=dp[last][x.fi][x.se];
			for (j=l; j<=r; j++) if ((x.fi>>j&1) || j==l || j==r){
				pii y;
				if (i==1) y=mpr(x.fi,j);
				else y=mpr(x.fi&(k|gao(x.se)),j);
				dp[now][y.fi][y.se]=(dp[now][y.fi][y.se]+tmp)%mod;
				s[now].insert(y);
			}
			tmp=0;
		}
		for (j=k=0; j<=l+1; j++) k|=1<<j;
		for (j=r+1; j<=n; j++) k|=1<<j;
		//cerr<<now<<'\n';
		//for (it=s[now].begin(); it!=s[now].end(); it++){
		//	pii o=*it;
			//printf("%d %d %d\n",o.fi,o.se,dp[now][o.fi][o.se]);
		//}
	}
	int ans=0;
	for (it=s[now].begin(); it!=s[now].end(); it++) ans=(ans+dp[now][(*it).fi][(*it).se])%mod;
	printf("%d\n",ans);
	return 0;
}

