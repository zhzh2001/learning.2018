#include<bits/stdc++.h>
#define For(i,a,b) for(int i=(a),i##_=(b);i<=i##_;++i)
#define Ror(i,a,b) for(int i=(a),i##_=(b);i>=i##_;--i)
#define LL long long
#define P 1000000007
#define M 1000005
using namespace std;
bool cur1;
int n,m;
template<class T>inline void Max(T &x,T y){
	if(y>x)x=y;
}
template<class T>inline void Min(T &x,T y){
	if(y<x)x=y;
}
inline void Add(int &x,int y){
	x+=y;
	if(x>=P)x-=P;
}
bool cur2; 
struct P0{
	void solve(){
		int ans=1;
		For(i,1,n)ans=2LL*ans%P;
		printf("%d\n",ans);
	}
}P1;
struct P30{
	void solve(){
		int ans=4;
		For(i,1,m-1)ans=3LL*ans%P;
		printf("%d\n",ans);		
	}
}P2;
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==1)P1.solve();
	else if(n==2)P2.solve();
	return 0;
}
