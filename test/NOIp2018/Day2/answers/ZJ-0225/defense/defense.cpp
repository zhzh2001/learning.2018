#include<bits/stdc++.h>
#define For(i,a,b) for(int i=(a),i##_=(b);i<=i##_;++i)
#define Ror(i,a,b) for(int i=(a),i##_=(b);i>=i##_;--i)
#define Dor(i,x) for(int i=head[x];i;i=nxt[i])
#define LL long long
#define M 100005
using namespace std;
bool cur1;
char op[10];
int n,m;
int A[M];
int head[M],nxt[M<<1],to[M<<1],tot;
template<class T>inline void Max(T &x,T y){
	if(y>x)x=y;
}
template<class T>inline void Min(T &x,T y){
	if(y<x||x==-1)x=y;
}
template<class T>inline void read(T &res){
	res=0;char p;
	while(p=getchar(),p<48);
	do res=(res<<1)+(res<<3)+(p^48);
	while(p=getchar(),p>47);
}
inline void Add(int x,int y){
	nxt[++tot]=head[x];
	head[x]=tot;
	to[tot]=y;
}
struct P44{
	LL dp[M][2];
	int x1,a1,x2,a2;
	void dfs(int x,int f){
		dp[x][0]=dp[x][1]=-1;
		LL sum1=0,sum2=0;
		Dor(i,x){
			int y=to[i];
			if(y!=f){
				dfs(y,x);
				if(~dp[y][1]&&~sum1)sum1+=dp[y][1];
				else if(~sum1)sum1=-1;
				if(~sum2){
					LL tmp=dp[y][0];
					if(~dp[y][1])Min(tmp,dp[y][1]);
					if(tmp==-1)sum2=-1;
					else sum2+=tmp;
				}
			}
		}
		if(~sum1)dp[x][0]=sum1;
		if(~sum2)dp[x][1]=sum2+A[x];
		if(x==x1)dp[x][1^a1]=-1;
		else if(x==x2)dp[x][1^a2]=-1;
	}
	void solve(){
		For(i,1,m){
			read(x1),read(a1),read(x2),read(a2);
			dfs(1,0);
			LL ans=dp[1][0];
			if(~dp[1][1])Min(ans,dp[1][1]);
			printf("%lld\n",ans);
		}
	}
}P1;
struct P52{
	LL dp[M][2],Dp[M][2];
	void solve(){
		dp[1][1]=A[1];
		dp[1][0]=-1;
		For(i,2,n){
			dp[i][0]=dp[i-1][1];
			LL tmp=dp[i-1][1];
			if(~dp[i-1][0])Min(tmp,dp[i-1][0]);
			dp[i][1]=tmp+A[i];
		}
		Ror(i,n,2){
			Dp[i][0]=Dp[i+1][1];
			Dp[i][1]=min(Dp[i+1][1],Dp[i+1][0])+A[i];
		}
		int a,b,x,y;
		For(i,1,m){
			read(a),read(x),read(b),read(y);
			LL ans=Dp[b][y]+dp[b-1][1];
			if(y&&~dp[b-1][0])Min(ans,Dp[b][y]+dp[b-1][0]);
			printf("%lld\n",ans);
		}
	}
}P2;
struct P64{
	LL dp[M][2],Dp[M][2];
	void solve(){
		For(i,1,n){
			dp[i][0]=dp[i-1][1];
			dp[i][1]=min(dp[i-1][1],dp[i-1][0])+A[i];
		}
		Ror(i,n,1){
			Dp[i][0]=Dp[i+1][1];
			Dp[i][1]=min(Dp[i+1][1],Dp[i+1][0])+A[i];
		}
		int a,b,x,y;
		For(i,1,m){
			read(a),read(x),read(b),read(y);
			if(!x&&!y){puts("-1");continue;}
			if(a>b)swap(a,b),swap(x,y);
			printf("%lld\n",dp[a][x]+Dp[b][y]);
		}
	}
}P3;
bool cur2; 
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n),read(m);
	scanf("%s",op);
	For(i,1,n)read(A[i]);
	int x,y;
	For(i,1,n-1){
		read(x),read(y);
		Add(x,y),Add(y,x);
	}
	if(n<=2000&&m<=2000)P1.solve();
	else if(op[0]=='A'){
		if(op[1]=='1')P2.solve();
		else if(op[1]=='2')P3.solve();
	}
	return 0;
}
