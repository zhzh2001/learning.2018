#include<bits/stdc++.h>
#define For(i,a,b) for(int i=(a),i##_=(b);i<=i##_;++i)
#define Ror(i,a,b) for(int i=(a),i##_=(b);i>=i##_;--i)
#define pb push_back
#define M 5005
using namespace std;
bool cur1;
int n,m,id;
int mk[M];
struct node{
	int x,id;
	bool operator<(const node &a)const{
		return x<a.x;
	}
};
vector<node>E[M];
template<class T>inline void Max(T &x,T y){
	if(y>x)x=y;
}
template<class T>inline void Min(T &x,T y){
	if(y<x)x=y;
}
template<class T>inline void read(T &res){
	res=0;char p;
	while(p=getchar(),p<48);
	do res=(res<<1)+(res<<3)+(p^48);
	while(p=getchar(),p>47);
}
struct P60{
	void dfs(int x,int f){
		printf("%d ",x);
		For(i,0,E[x].size()-1){
			int y=E[x][i].x;
			if(y!=f)dfs(y,x);
		}
	}
	void solve(){
		dfs(1,0);
		puts("");
	}
}P1;
struct P100{
	int Ans[M],Tmp[M];
	int cnt;
	void Chk(){
		bool p=1;
		if(Ans[1]){
			For(i,1,n){
				if(Ans[i]>Tmp[i]){
					p=0;
					break; 
				}else if(Ans[i]<Tmp[i])break;
			}
		}else p=0;
		if(!p)For(i,1,n)Ans[i]=Tmp[i]; 
	}
	void dfs(int x,int f){
		if(mk[x]==id)return;
		mk[x]=id;
		Tmp[++cnt]=x;
		For(i,0,E[x].size()-1){
			int y=E[x][i].x;
			if(y!=f&&E[x][i].id!=id)dfs(y,x);
		}
	}
	void solve(){
		Ans[1]=0;
		For(i,1,m){
			id=i;
			cnt=0;
			dfs(1,0);
			if(cnt==n)Chk();
		}
		For(i,1,n)printf("%d ",Ans[i]);puts("");
	}
}P2;
bool cur2; 
int main(){
//	printf("%lf\n",(&cur2-&cur1)/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n),read(m);
	int x,y;
	For(i,1,m){
		read(x),read(y);
		E[x].pb((node){y,i}),E[y].pb((node){x,i});
	}
	For(i,1,n)sort(E[i].begin(),E[i].end());
	if(m==n-1)P1.solve();
	else P2.solve();
	return 0;
}
