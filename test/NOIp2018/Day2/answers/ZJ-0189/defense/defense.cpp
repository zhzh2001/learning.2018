#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<string>
#include<cctype>
#include<ctime>
#include<cstdlib>
#define re register
#define rep(x,a,b) for (re int x=(int)(a);x<=(int)(b);++x)
#define drp(x,a,b) for (re int x=(int)(a);x>=(int)(b);--x)
#define LL long long
#define inf 0x3f3f3f3f
#define N 5010
using namespace std;
int n,m;
char ch[10];
int To[N<<1],Nxt[N<<1],Head[N],Cnt;
inline void add(int u,int v){
	To[++Cnt]=v;
	Nxt[Cnt]=Head[u];
	Head[u]=Cnt;
}
int f[N][2],ans,boo[N][N],a,b,x,y,p[N];
bool vis[N][2];
inline void dfs(int u,int fa,int bo){
	if (u==a&&x!=bo) {
		f[u][bo]=inf;
		return;
	}
	if (u==b&&y!=bo) {
		f[u][bo]=inf;
		return;
	}
//	if (vis[u][bo]) return;
//	vis[u][bo]=1;
	for (int i=Head[u];i;i=Nxt[i]){
		int v=To[i];
		if (v==fa) continue;
		dfs(v,u,1);
		if (bo==1) dfs(v,u,0);
		if (v==a||v==b){
			if (v==a) {
				if (bo==1) f[u][bo]+=f[v][x];
				if (bo==0&&x==0) f[u][bo]=inf;
			}else{
				if (bo==1) f[u][bo]+=f[v][y];
				if (bo==0&&y==0) f[u][bo]=inf;			
			}
		}
		else{
			if (bo==1) f[u][bo]=f[u][bo]+min(f[v][0],f[v][1]);
				else f[u][bo]+=f[v][1];			
		}

	}
}
inline void SubTask1(){
	rep(i,1,m){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		//Ca[a]=x;Ca[b]=y;
		if (boo[a][b]&&(x==0)&&(y==0)) {
			printf("-1\n");return;
		}
		memset(f,0,sizeof(f));
		memset(vis,0,sizeof(vis));
		rep(i,1,n) f[i][1]=p[i];
		dfs(1,0,0);
		int t=f[1][0];
		memset(f,0,sizeof(f));
		rep(i,1,n) f[i][1]=p[i];
		f[1][0]=t;
		dfs(1,0,1);
		if (a==1) ans=f[1][x];
		else if (b==1) ans=f[1][y];
			else ans=min(f[1][1],f[1][0]);
		printf("%d\n",ans);
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%s",ch);
	rep(i,1,n) scanf("%d",&p[i]);
	memset(boo,0,sizeof(boo));
	rep(i,1,n-1){
		scanf("%d%d",&x,&y);
		boo[x][y]=boo[y][x]=1;
		add(x,y);add(y,x);
	}
	if (ch[1]=='2') {
		rep(i,1,m) printf("-1\n");
		return 0;
	}else if ((n<=10)&&(m<=10)) {
		SubTask1();
		return 0;
	}
	return 0;
}

