#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<cctype>
inline int getint(){
	register int x = 0;
	register char ch = getchar();
	while(!isdigit(ch))ch = getchar();
	for(;isdigit(ch);ch = getchar())x = (((x << 2) + x) << 1) + (ch ^ 48);
	return x;
}
#define MAXN 100010
typedef long long lnt;
int n, m, p[MAXN], deep[MAXN];
int head[MAXN], to[MAXN << 1], next[MAXN << 1], tot = 0;
inline void $(int u, int v){
	next[tot] = head[u], to[tot] = v, head[u] = tot++;
	next[tot] = head[v], to[tot] = u, head[v] = tot++;
}
const lnt INF = 20000000000LL;
lnt f[MAXN], g[MAXN], ans;
int a, u, b, v, fa[MAXN];
void dp(int x){
	for(int i = head[x];~i;i = next[i]){
		if(to[i] == fa[x])continue;
		fa[to[i]] = x;
		deep[to[i]] = deep[x] + 1;
		dp(to[i]);
		g[x] += f[to[i]];
		f[x] += std::min(f[to[i]], g[to[i]]);
	}
	f[x] += p[x];
}
void clear(int x){
	if(!fa[x])return;
	clear(fa[x]);
	f[fa[x]] -= std::min(f[x], g[x]);
	g[fa[x]] -= f[x];
}
void redp(int x){
	if(!fa[x])return;
	f[fa[x]] += std::min(f[x], g[x]);
	g[fa[x]] += f[x];
	redp(fa[x]);
}
int main(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	n = getint(); m = getint(); getint();
	memset(head, -1, sizeof(head));
	for(int i = 1;i <= n;i++)p[i] = getint();
	for(int i = 1;i < n;i++)$(getint(), getint());
	dp(1);
	for(int i = 1;i <= m;i++){
		a = getint(); u = getint(); b = getint(); v = getint();
		if(deep[a] < deep[b])std::swap(a, b), std::swap(u, v);
		lnt bk1 = f[a], bk2 = g[a], bk3 = f[b], bk4 = g[b];
		clear(a); (u ? g[a] : f[a]) = INF; redp(a);
		clear(b); (v ? g[b] : f[b]) = INF; redp(b);
		ans = std::min(f[1], g[1]);
		clear(a); f[a] = bk1, g[a] = bk2; redp(a);
		clear(b); f[b] = bk3, g[b] = bk4; redp(b);
		printf("%lld\n", ans >= INF ? -1 : ans);
	}
	return 0;
}
