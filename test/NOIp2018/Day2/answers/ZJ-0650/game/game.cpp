#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define MOD 1000000007
inline void up(int &x, int y){
	if((x += y) >= MOD)x -= MOD;
}
int n, m, v[100][100];
int a[10000], b[10000], id[10000], top;
void dfs(int x, int y, int n1, int n2){
	if(x>=n||y>=m)return;
	n2 = n2 * 2 + v[x][y];
	if(x == n-1 && y == m-1){
		++top;
		a[top] = n1;
		b[top] = n2;
		return;
	}
	dfs(x+1,y,n1*2,n2);
	dfs(x,y+1,n1*2+1,n2);
}
inline int cmp(int x, int y){return a[x]<a[y];}
int main(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if(n == 1){
		int ans = 1;
		while(m--)ans = 1LL * ans * 2 % MOD;
		printf("%d\n", ans);
		return 0;
	}
	if(n == 2){
		int ans = 4;
		m--;
		while(m--)ans = 1LL * ans * 3 % MOD;
		printf("%d\n", ans);
		return 0;
	}
	int ans = 0;
	for(int i = 0;i < (1 << (n * m));i++){
		for(int j = 0;j < n;j++){
			for(int k = 0;k < m;k++){
				v[j][k] = (i >> (j * m + k)) & 1;
			}
		}
		top=0;
		dfs(0,0,0,0);
		for(int i=1;i<=top;i++)id[i]=i;
		std::sort(id+1,id+top+1,cmp);
		for(int i=2;i<=top;i++){
			if(b[id[i]]>b[id[i-1]])goto fail;
		}
		ans++;
		continue;
		fail:;
	}
	printf("%d\n", ans);
	return 0;
}
