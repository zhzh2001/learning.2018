#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<queue>
#define MAXN 5010
#define MAXM 5010
int n, m, u[MAXN], v[MAXN], vis[MAXN];
int head[MAXN], to[MAXM << 1], next[MAXM << 1], tot = 0;
inline void $(int u, int v){
	next[tot] = head[u]; to[tot] = v; head[u] = tot++;
	next[tot] = head[v]; to[tot] = u; head[v] = tot++;
}
int fa[MAXN], deep[MAXN], cir[MAXN], inc[MAXN], cnt = 0;
void dfs(int x){
	vis[x] = 1;
	for(int i = head[x];~i;i = next[i]){
		if(to[i] == fa[x])continue;
		if(vis[to[i]]){
			if(deep[to[i]] > deep[x])continue;
			for(int y = x;y != fa[to[i]];y = fa[y])inc[cir[++cnt] = y] = 1;	
			continue;
		}
		fa[to[i]] = x;
		deep[to[i]] = deep[x] + 1;
		dfs(to[i]);	
	}	
}
std::priority_queue<int> go[MAXN];
int ans[MAXN], tmp[MAXN], clk;
inline void update(){
	for(int i = 1;i <= n;i++){
		if(tmp[i] > ans[i])return;
		if(tmp[i] < ans[i])break;
	}
	memcpy(ans, tmp, sizeof(ans));
}
void dp(int x){
	vis[x] = 1;
	tmp[++clk] = x;
	while(go[x].size()){
		int y = -go[x].top();
		go[x].pop();
		if(vis[y])continue;
		dp(y);
	}
}
int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	memset(head, -1, sizeof(head));
	for(int i = 1;i <= m;i++){
		scanf("%d%d", &u[i], &v[i]);
		$(u[i], v[i]);
		go[u[i]].push(-v[i]);
		go[v[i]].push(-u[i]);
	}
	dfs(1);
	memset(vis, 0, sizeof(vis));
	dp(1);
	memcpy(ans, tmp, sizeof(ans));
	for(int i = 1;i <= m;i++){
		if(!inc[u[i]] || !inc[v[i]])continue;
		for(int j = 1;j <= m;j++){
			if(j == i)continue;
			go[u[j]].push(-v[j]);
			go[v[j]].push(-u[j]);
		}
		memset(vis, 0, sizeof(vis));
		clk = 0;
		dp(1);
		update();
	}
	for(int i = 1;i <= n;i++)printf("%d%c", ans[i], " \n"[i == n]);
}
