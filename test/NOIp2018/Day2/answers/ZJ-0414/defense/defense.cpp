#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
int n,m;
const int qw=200005;
int a,b;
int nxt[qw],adj[qw],go[qw];
int ecnt=1;
void add(int u,int v){
	nxt[++ecnt]=adj[u];adj[u]=ecnt;go[ecnt]=v;
	nxt[++ecnt]=adj[v];adj[v]=ecnt;go[ecnt]=u;
}
int bo[qw];
int s[qw];
const int inf=10000000;
int dp[1000005][2];
void work(){
	
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char tp[100];
	scanf("%d%d",&n,&m);
	scanf("%s",tp);
	for(int i=1;i<=n;i++)scanf("%d",&s[i]);
	for(int i=1;i<n;i++){
		scanf("%d%d",&a,&b);
		add(a,b);
	}
	int x,y,x1,y1;
	for(int i=1;i<=n;i++)bo[i]=-1;
	bool jg;
	for(int i=1;i<=m;i++){
		scanf("%d%d%d%d",&x,&x1,&y,&y1);
		bo[x]=x1;bo[y]=y1;
	//	ans=inf;
		jg=1;
		for(int i=1;i<=n;i++){dp[i][0]=inf;dp[i][1]=inf;}
		if(bo[1]==-1){
			dp[1][0]=0;
			dp[1][1]=s[1];
		}
		else if(bo[1]==0){
			dp[1][0]=0;
			dp[1][1]=inf;
		}
		else if(bo[1]==1){
			dp[1][1]=s[1];
			dp[1][0]=inf;
		}
		for(int i=2;i<=n;i++){
			if(bo[i]==-1){
				if(bo[i-1]==-1){
					dp[i][0]=dp[i-1][1];
					dp[i][1]=min(dp[i-1][1],dp[i-1][0])+s[i];
				}
				else if(bo[i-1]==0){
					dp[i][1]=dp[i-1][0]+s[i];
					dp[i][0]=inf;
				}
				else if(bo[i-1]==1){
					dp[i][0]=dp[i-1][1];
					dp[i][1]=dp[i-1][1]+s[i];
				}
			}
			else if(bo[i]==0){
				if(bo[i-1]==0){
					jg=false;
					break;
				}
				dp[i][0]=dp[i-1][1];
			}
			else if(bo[i]==1){
				if(bo[i-1]==-1){
					dp[i][1]=min(dp[i-1][0],dp[i-1][1])+s[i];
				}
				else if(bo[i-1]==0){
					dp[i][1]=dp[i-1][0]+s[i];
				}
				else if(bo[i-1]==1){
					dp[i][1]=dp[i-1][1]+s[i];
				}
			}
		}
		if(jg==false)printf("-1\n");
		else {
			if(bo[n]==-1){
				printf("%d\n",min(dp[n][0],dp[n][1]));
			}
			else if(bo[n]==0)printf("%d\n",dp[n][0]);
			else if(bo[n]==1)printf("%d\n",dp[n][1]);
		}
	}
	return 0;
}
