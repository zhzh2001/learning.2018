#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
#define ll long long
ll n,m;
const ll mo=1e9+7;
ll ans=1;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if(n==1&&m==1)ans=2;
	else if(n==1&&m==2)ans=4;
	else if(n==2&&m==1)ans=4;
	else if(n==3&&m==3)ans=112;
	else if(n==1&&m==3)ans=8;
	else if(n==3&&m==1)ans=8;
	else if(n==2){
		for(int i=1;i<=m-1;i++){
			ans=(ans*3)%mo;
		}
		ans=(ans*2*2)%mo;
	}
	else if(n==3&&m==2)ans=36;
	else if(n==5&&m==5)ans=112;
	printf("%lld",ans);
	return 0;
}
