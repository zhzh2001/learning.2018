#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
#define ll long long
ll n,m;
ll ans=1;
ll dp[6][1000005][2];
const ll mo=1e9+7;
int main(){
	freopen("game.in","r",stdin);
	
	scanf("%lld%lld",&n,&m);
//	for(int i=1;i<=n;i++)dp[i][0][0]=dp[i][0][1]=1;
	//for(int i=0;i<=m;i++)dp[0][i][0]=dp[0][i][1]=1;
	dp[1][1][0]=1;
	dp[1][1][1]=1;
	bool bo;
	int j;
	for(int he=2;he<=n+m;he++){
		bo=true;
		for(int i=n;i>=1;i--){
			j=he-i;
			if(i>=he)break;
			if(he==2)break;
			if(bo){
				dp[i][j][0]=dp[i-1][j][0];
				dp[i][j][1]=dp[i-1][j][1];
				bo=false;
				continue;
			}
			dp[i][j][0]=(dp[i-1][j][1]*dp[i][j-1][1]+
			             dp[i-1][j][0]*(dp[i][j-1][1]+dp[i][j-1][0]))
			             *(dp[i+1][j-1][0]+dp[i+1][j-1][1])%mo;
			dp[i][j][1]=(dp[i-1][j][1]*dp[i][j-1][1]+
			             dp[i-1][j][0]*(dp[i][j-1][1]+dp[i][j-1][0]))
			             *dp[i+1][j-1][1]%mo;
		}
	}
	ll ans=dp[n][m][1]+dp[n][m][0];
	ans=ans%mo;
	printf("%lld",ans);
	return 0;
}
