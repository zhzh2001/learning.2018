#include <bits/stdc++.h>
using namespace std;
const int N=200005;
const int Inf=1e9;
int n,q,a[N],u[N],v[N],X,y,C1,C2,ans,p[N];
char s[1000];
void dfs(int x,int sum) {
//	printf("%d ",x);
	if (x==n+1) {
		for (int i=1; i<n; i++)
			if (a[u[i]]==1 && a[v[i]]==1) return;
		if (a[X]!=C1) return;
		if (a[y]!=C2) return;
		ans=min(ans,sum);
		return;
	}
	a[x]=1;
	dfs(x+1,sum);
	a[x]=0;
	dfs(x+1,sum+p[x]);
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("check.out","w",stdout);
	scanf("%d%d",&n,&q); scanf("%s",s);
	for (int i=1; i<=n; i++) scanf("%d",&p[i]);
	for (int i=1; i<n; i++) scanf("%d%d",&u[i],&v[i]);
	while (q--) {
		ans=Inf;
		scanf("%d%d%d%d",&X,&C1,&y,&C2);
		C1=1-C1,C2=1-C2;
		dfs(1,0);
		if (ans==Inf) ans=-1;
		printf("%d\n",ans);
	}
	return 0;
}
