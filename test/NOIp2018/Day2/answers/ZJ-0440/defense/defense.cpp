#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;
typedef long long ll;
const ll Inf=1e16; //INF INF INF
const ll N=200005;
ll dep[N],fa[N],a[N],Down[N][2],Up[N][2],vet[N],n,q;
ll nxt[N],head[N],edgenum,f[N][22],g[N][22][2][2];
char s[1000];
void Add(ll u,ll v) {
	vet[++edgenum]=v; nxt[edgenum]=head[u]; head[u]=edgenum;
}
void dfs1(ll x,ll f) {
	fa[x]=f,dep[x]=dep[f]+1;
	Down[x][0]=a[x];
	for (ll e=head[x]; e!=0; e=nxt[e]) {
		ll v=vet[e]; if (v==f) continue;
		dfs1(v,x);
		Down[x][0]+=min(Down[v][0],Down[v][1]);
		Down[x][1]+=Down[v][0];
	}
}
void dfs2(ll x,ll f) {
	for (ll e=head[x]; e!=0; e=nxt[e]) {
		ll v=vet[e]; if (v==f) continue;
		ll B1=Down[x][0]-min(Down[v][0],Down[v][1])-a[x];
		ll B2=Down[x][1]-Down[v][0];
		Up[v][0]=min(B1+Up[x][0],B2+Up[x][1])+a[v];
		Up[v][1]=B1+Up[x][0];
		dfs2(v,x);
	}
}
void RMQ() {
	for (ll i=1; i<=n; i++) f[i][0]=fa[i];
	for (ll i=1; i<=n; i++) {
		g[i][0][0][0]=Down[fa[i]][0]-min(Down[i][0],Down[i][1]);
		g[i][0][1][0]=Down[fa[i]][0]-min(Down[i][0],Down[i][1]);
		g[i][0][0][1]=Down[fa[i]][1]-Down[i][0];
		g[i][0][1][1]=Inf;
	}
	for (ll j=1; j<=20; j++)
		for (ll i=1; i<=n; i++)
			f[i][j]=f[f[i][j-1]][j-1];
	for (ll j=1; j<=20; j++)
		for (ll i=1; i<=n; i++)
			for (ll x=0; x<=1; x++)
				for (ll y=0; y<=1; y++) {
					ll k=f[i][j-1];
					g[i][j][x][y]=min(g[i][j-1][x][0]+g[k][j-1][0][y],
									  g[i][j-1][x][1]+g[k][j-1][1][y]);
				}

}
ll calc(ll x,ll y,ll C1,ll C2) {
	ll ans0=0,ans1=0,fir=true;
	for (ll i=20; i>=0; i--)
		if (dep[f[x][i]]>=dep[y]) {
			if (fir) {
				fir=false;
				ans0=g[x][i][C1][0],ans1=g[x][i][C1][1];
			} else {
				ll t0=ans0,t1=ans1;
				ans0=min(t0+g[x][i][0][0],t1+g[x][i][1][0]);
				ans1=min(t0+g[x][i][0][1],t1+g[x][i][1][1]);
			}
			x=f[x][i];
		}
	if (C2==0) return ans0; else return ans1;
}
ll LCA(ll x,ll y) {
	if (dep[x]<dep[y]) swap(x,y);
	for (ll i=20; i>=0; i--) 
		if (dep[f[x][i]]>=dep[y]) x=f[x][i];
	if (x==y) return x;
	for (ll i=20; i>=0; i--)
		if (f[x][i]!=f[y][i]) x=f[x][i],y=f[y][i];
	return f[x][0];
}

int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%lld%lld",&n,&q); scanf("%s",s);
	for (ll i=1; i<=n; i++) scanf("%lld",&a[i]);
	for (ll i=1; i<n; i++) {
		ll u,v; scanf("%lld%lld",&u,&v);
		Add(u,v),Add(v,u);
	}
	dfs1(1,0),Up[1][0]=a[1],dfs2(1,0); RMQ();
//	printf("%lld\n",g[4][1][0][0]);
	while (q--) {
		ll x,y,C1,C2; scanf("%lld%lld%lld%lld",&x,&C1,&y,&C2);
		if (dep[x]<dep[y]) swap(x,y),swap(C1,C2);
		ll lca=LCA(x,y); C1=1-C1,C2=1-C2;
		if (lca==y) {
			ll ans=Down[x][C1]+Up[y][C2]+calc(x,y,C1,C2);
			if (C2==0) ans-=a[y];
			if (ans>=Inf) ans=-1; printf("%lld\n",ans);
		} else {
			ll anss=Inf;
			for (ll C=0; C<=1; C++) {
				ll ans=Down[x][C1]+Down[y][C2]+calc(x,lca,C1,C);
				ans=ans+calc(y,lca,C2,C)+Up[lca][C]-Down[lca][C];
				if (C==0) ans-=a[lca];
				anss=min(anss,ans);
			}
			if (anss>=Inf) anss=-1; printf("%lld\n",anss);
		}
	}
	return 0;
}
/*
5 3 C3
1 2 3 4 5
1 2
1 5
2 3 
2 4
*/
