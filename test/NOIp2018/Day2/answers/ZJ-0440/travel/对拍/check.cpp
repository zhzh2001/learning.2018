#include <bits/stdc++.h>
using namespace std;
const int Inf=1e9;
int M[1005][1005],Stack[1005],a[1005],n,m,vis[1005],ans[1005];
void dfs(int x) {
	if (x==n+1) {
		int top=0; 
		Stack[++top]=a[1];
		for (int i=2; i<=n; i++) {
			while (top>0 && !M[Stack[top]][a[i]]) {
				top--;
			}
			if (top==0) return;
			Stack[++top]=a[i];
		}
		for (int i=1; i<=n; i++) {
			if (ans[i]<a[i]) return;
			if (a[i]<ans[i]) break;
		}
		for (int i=1; i<=n; i++) ans[i]=a[i];
	}
	for (int i=1; i<=n; i++) 
		if (!vis[i]) {
			vis[i]=true; a[x]=i;
			dfs(x+1);
			vis[i]=false;
		}
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("check.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=m; i++) {
		int u,v; scanf("%d%d",&u,&v);
		M[u][v]=M[v][u]=true;
	}
	for (int i=1; i<=n; i++) ans[i]=Inf;
	dfs(1);
	for (int i=1; i<=n; i++) printf("%d ",ans[i]);
	return 0;
}
