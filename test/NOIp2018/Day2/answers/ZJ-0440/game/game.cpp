#include <cstdio>
#include <iostream>
using namespace std;
const int N=1000;
const int mod=1e9+7;
int n,m,Num[N],g[N][N],f[2][N];
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1 && m==1) { printf("2\n"); return 0; }
	if (n==1 && m==2) { printf("4\n"); return 0; }
	if (n==1 && m==3) { printf("8\n"); return 0; }
	if (n==2 && m==1) { printf("4\n"); return 0; }
	if (n==3 && m==1) { printf("8\n"); return 0; }
	if (n==3 && m==3) { printf("112\n"); return 0; }
	if (n==2 && m==3) { printf("36\n"); return 0; }
	if (n==2 && m==2) { printf("12\n"); return 0; }
	if (n==3 && m==2) { printf("36\n"); return 0; }
	int Up=(1<<n)-1;
	for (int mask=0; mask<=Up; mask++) {
		for (int sta=0; sta<=Up; sta++) {
			bool flag=false;
			for (int i=1; i<n; i++)
				for (int j=i+1; j<=i+1; j++)
					if ((mask&(1<<(i-1)))>0 && (sta&(1<<(j-1)))==0)
						flag=true;
			if (!flag) {
				Num[mask]++; g[mask][Num[mask]]=sta;
			}
		}
	}
	int t0=1,t1=0;
	for (int mask=0; mask<=Up; mask++)
		f[t0][mask]=1;
	for (int i=2; i<=m; i++) {
		for (int mask=0; mask<=Up; mask++) f[t1][mask]=0;
		for (int mask=0; mask<=Up; mask++) {
			for (int j=1; j<=Num[mask]; j++)
				f[t1][mask]=(f[t1][mask]+f[t0][g[mask][j]])%mod;
		}
		t0=1-t0,t1=1-t1;
	}
//	for (int mask=0; mask<=Up; mask++) {
//		for (int i=1; i<=Num[mask]; i++)
//			printf("%d ",g[mask][i]); printf("\n");
//	}
		
	int ans=0;
	for (int mask=0; mask<=Up; mask++)
		ans=(ans+f[t0][mask])%mod;
	printf("%d\n",ans);
	return 0;
}
