#include <cstdio>
#include <iostream>
using namespace std;
const int N=400;
const int mod=1e9+7;
int n,m,Num[N],g[N][N],f[N][N];
int main() {
	//freopen("game.in","r",stdin);
	//freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	int Up=(1<<n)-1;
	for (int mask=0; mask<=Up; mask++) {
		for (int sta=0; sta<=Up; sta++) {
			bool flag=false;
			for (int i=1; i<n; i++)
				for (int j=i+1; j<=n; j++)
					if ((mask&(1<<(i-1)))>0 && (sta&(1<<(j-1)))==0)
						flag=true;
			if (!flag) {
				Num[mask]++; g[mask][Num[mask]]=sta;
			}
		}
	}
	int t0=1,t1=0;
	for (int mask=0; mask<=Up; mask++)
		f[t0][mask]=1;
	for (int i=2; i<=m; i++) {
		for (int mask=0; mask<=Up; mask++) f[t1][mask]=0;
		for (int mask=0; mask<=Up; mask++) {
			for (int j=1; j<=Num[mask]; j++)
				f[t1][mask]=(f[t1][mask]+f[t0][g[mask][j]])%mod;
		}
		t0=1-t0,t1=1-t1;
	}
//	for (int i=1; i<=m; i++)
	//	for (int mask=0; mask<=Up; mask++)
		
	int ans=0;
	for (int mask=0; mask<=Up; mask++)
		ans=(ans+f[t0][mask])%mod;
	printf("%d\n",ans);
	return 0;
}
