#include<bits/stdc++.h>

using namespace std;

const int mod=1000000007;
int n,m,now,f[2][10][300],g1[10],g2[10],h1[10],h2[10];

inline void upd(int &x,int y){x=(x+y)%mod;}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m<n) swap(n,m);
	now=0;
	f[now][0][0]=f[now][1][0]=1;
	for (int i=1;i<n+m-1;++i){
		now^=1;
		memset(f[now],0,sizeof f[now]);
		if (i<n){
			int len=i;
			for (int j=0;j<=len;++j)
				for (int k=0;k<(1<<len);++k){
					if (!f[now^1][j][k]) continue;
					for (int l=1;l<=len;++l) g1[l]=k>>(l-1)&1;
					for (int l=1;l<=len;++l) g2[l]=l>j;
					int S=0;
					for (int l=1;l<=len+1;++l){
						h1[l]=h2[l]=0;
						if (l<=len){
							h1[l]|=g1[l];
							h2[l]=g1[l];
						}
						if (l>1) h1[l]|=g1[l-1];
						if (l<=len&&l>1){
							h1[l]|=g2[l-1]==g2[l];
						}
						S|=h1[l]<<(l-1);
					}
					h2[0]=h2[len+1]=0;
					for (int l=0;l<=len+1;++l)
						if (!h2[l]) upd(f[now][l][S],f[now^1][j][k]);
				}
			continue;
		}
		if (n+m-1-i<n){
			int len=n+m-1-i+1;
			for (int j=0;j<=n;++j)
				for (int k=0;k<1<<n;++k){
					if (!f[now^1][j][k]) continue;
					for (int l=1;l<=n;++l) g1[l]=k>>(l-1)&1;
					for (int l=1;l<=n;++l) g2[l]=l>j;
					int S=0;
					h2[n-len+1]=0;
					for (int l=n-len+2;l<=n;++l){
						h1[l]=g1[l]|g1[l-1]|(g2[l-1]==g2[l]);
						h2[l]=g1[l];
						S|=h1[l]<<(l-1);
					}
					h2[n]=0;
					for (int l=n-len+1;l<=n;++l)
						if (!h2[l]) upd(f[now][l][S],f[now^1][j][k]);
				}
			continue;
		}
		for (int j=0;j<=n;++j)
			for (int k=0;k<1<<n;++k){
				if (!f[now^1][j][k]) continue;
				for (int l=1;l<=n;++l) g1[l]=k>>(l-1)&1;
				for (int l=1;l<=n;++l) g2[l]=l>j;
				int S=0;
				for (int l=1;l<=n;++l){
					h1[l]=g1[l];
					h2[l]=g1[l];
					if (l>1){
						h1[l]|=g1[l-1]|(g2[l-1]==g2[l]);
					}
					S|=h1[l]<<(l-1);
				}
				h2[0]=h2[n]=0;
				for (int l=0;l<=n;++l)
					if (!h2[l]) upd(f[now][l][S],f[now^1][j][k]);
			}
	}
	int ans=0;
	upd(ans,f[now][n-1][1<<(n-1)]);
	upd(ans,f[now][n][1<<(n-1)]);
	upd(ans,f[now][n-1][0]);
	upd(ans,f[now][n][0]);
	printf("%d\n",ans);
	
	return 0;
}
			
