#include<bits/stdc++.h>

using namespace std;

#define LL long long
#define INF 0x3f3f3f3f3f3f3f3fLL
#define N 300000

LL n,m,p,a[N],x,y,xx,yy,tot,head[N],f[N][2];
char c;
struct edge{LL v,nxt;}e[N];

void add(LL x,LL y){
	e[++tot].v=y; e[tot].nxt=head[x]; head[x]=tot;
}

void dfs(LL u,LL fa){
	f[u][0]=0; f[u][1]=a[u];
	for (LL i=head[u],v;i;i=e[i].nxt)
		if ((v=e[i].v)!=fa){
			dfs(v,u);
			f[u][0]+=f[v][1];
			f[u][1]+=min(f[v][0],f[v][1]);
			f[u][0]=min(f[u][0],INF);
			f[u][1]=min(f[u][1],INF);
		}
	if (u==x){
		if (xx==0) f[u][1]=INF;
		else f[u][0]=INF;
	}
	if (u==y){
		if (yy==0) f[u][1]=INF;
		else f[u][0]=INF;
	}
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%lld%lld %c%lld",&n,&m,&c,&p);
	for (LL i=1;i<=n;++i)
		scanf("%lld",a+i);
	for (LL i=1;i<n;++i){
		scanf("%lld%lld",&x,&y);
		add(x,y); add(y,x);
	}
	while (m--){
		scanf("%lld%lld%lld%lld",&x,&xx,&y,&yy);
		dfs(1,0);
		LL ans=min(f[1][0],f[1][1]);
		printf("%lld\n",ans>=INF?-1:ans);
	}
	
	return 0;
}
			
