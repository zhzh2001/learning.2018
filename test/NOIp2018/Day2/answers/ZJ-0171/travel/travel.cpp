#include<bits/stdc++.h>

using namespace std;

#define N 6000
#define ff first
#define ss second
#define P(x,y) make_pair(x,y)

int n,m,x,y,xb,f[N],g[N];
bool vis[N];
vector<pair<int,int> > e[N];

void dfs(int u,int j){
	g[++xb]=u;
	vis[u]=1;
	int m=e[u].size();
	for (int i=0;i<m;++i)
		if (!vis[e[u][i].ff]&&e[u][i].ss!=j) dfs(e[u][i].ff,j);
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;++i){
		scanf("%d%d",&x,&y);
		e[x].push_back(P(y,i));
		e[y].push_back(P(x,i));
	}
	for (int i=1;i<=n;++i) sort(e[i].begin(),e[i].end());
	if (m<n){
		dfs(1,0);
		for (int i=1;i<=n;++i) f[i]=g[i];
	}
	else{
		for (int i=1;i<=n;++i) f[i]=n;
		for (int i=1;i<=m;++i){
			memset(vis,0,sizeof vis); xb=0;
			dfs(1,i);
			if (xb==n){
				bool fl=0;
				for (int j=1;j<=n;++j){
					if (!fl&&g[j]>f[j]) break;
					if (g[j]<f[j]) fl=1;
					if (fl) f[j]=g[j];
				}
			}
		}
	}
	for (int i=1;i<=n;++i) printf(i==n?"%d\n":"%d ",f[i]);
	
	return 0;
}
			
