#include<bits/stdc++.h>
#define N 100003
using namespace std;
typedef long long LL;

char s[5];
LL n, m, a[N], f[N], sta[N], fst[N], dp[N][3];
struct edge{ LL x, nx; }e[N << 1];

void dfs(LL x, LL fa)
{
	dp[x][0] = 0, dp[x][1] = a[x];
	for(LL i = fst[x]; i; i = e[i].nx)
		if(e[i].x != fa)
		{
			dfs(e[i].x, x);
			dp[x][0] += dp[e[i].x][1];
			dp[x][1] += min(dp[e[i].x][0], dp[e[i].x][1]);
		}
	if(sta[x] == 0) dp[x][1] = 2e9;
	if(sta[x] == 1) dp[x][0] = 2e9;
}

void dfsb(int x)
{
	for(int i = fst[x]; i; i = e[i].nx)
		if(e[i].x != f[x])
			f[e[i].x] = x, dfsb(e[i].x);
}

int get(int x)
{
	if(f[x] == 1) return x;
	return f[x] = get(f[x]);
}

int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	memset(sta, -1, sizeof(sta));
	scanf("%lld%lld%s", &n, &m, s);
	for(LL i = 1; i <= n; i ++)
		scanf("%lld", &a[i]);
	for(LL i = 1, x, y, l = 0; i < n; i ++)
	{
		scanf("%lld%lld", &x, &y);
		e[++ l].x = y, e[l].nx = fst[x], fst[x] = l;
		e[++ l].x = x, e[l].nx = fst[y], fst[y] = l;
	}
	if(s[0] == 'B')
	{
		dfs(1, 0);
		dfsb(1);
		for(LL i = 1, x, xx, y, yy; i <= m; i ++)
		{
			scanf("%lld%lld%lld%lld", &x, &xx, &y, &yy);
			sta[x] = xx, sta[y] = yy;
			dp[1][1] -= min(dp[get(y)][0], dp[f[y]][1]);
			dfs(f[y], 1);
			dp[x][1] += min(dp[f[y]][0], dp[f[y]][1]);
			if(dp[1][0] < 2e9 || dp[1][1] < 2e9)
				printf("%lld\n", min(dp[1][0], dp[1][1]));
			else puts("-1");
			sta[x] = sta[y] = -1;
		}
	}
	else
	{
		for(LL i = 1, x, xx, y, yy; i <= m; i ++)
		{
			scanf("%lld%lld%lld%lld", &x, &xx, &y, &yy);
			sta[x] = xx, sta[y] = yy;
			dfs(1, 0);
			if(dp[1][0] < 2e9 || dp[1][1] < 2e9)
				printf("%lld\n", min(dp[1][0], dp[1][1]));
			else puts("-1");
			sta[x] = sta[y] = -1;
		}
	}
	return 0;
}
