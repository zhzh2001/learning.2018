#include<bits/stdc++.h>
using namespace std;
typedef long long LL;

const LL p = 1e9 + 7;
LL n, m;

LL ksm(LL x, LL y)
{
	LL ans = 1;
	for(; y; y >>= 1, (x *= x) %= p)
		if(y & 1) (ans *= x) %= p;
	return ans;
}

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%lld%lld", &n, &m);
	if(n > m) swap(n, m);
	if(n == 1) printf("%lld", ksm(2, m));
	else if(n == 2) printf("%lld", 4 * ksm(3, m - 1) % p);
	else if(n == 3 && m == 3) printf("%lld", 112);
	else if(n == 5 && m == 5) printf("%lld", 7136);
	return 0;
}
