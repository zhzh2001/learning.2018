#include<bits/stdc++.h>
#define N 5003
using namespace std;

bool h[N << 1];
int n, m, l = 1, p, s, dfn, a[N], t[N], f[N], hi[N], fa[N], ans[N], fst[N];
struct edge{ int x, nx; }e[N << 1], nx[N];

void dfs(int x, int fa)
{
	a[++ dfn] = x;
	int tot = 0, fs = 0;
	for(int i = fst[x]; i; i = e[i].nx)
		if(e[i].x != fa && !h[i]) t[++ tot] = e[i].x;
	sort(t + 1, t + tot + 1);
	for(int i = tot; i >= 1; i --)
		nx[++ s].x = t[i], nx[s].nx = fs, fs = s;
	for(int i = fs; i; i = nx[i].nx)
		dfs(nx[i].x, x);
}

void dfs1(int x)
{
	hi[x] = hi[e[fa[x] ^ 1].x] + 1;
	for(int i = fst[x]; i; i = e[i].nx)
		if((i ^ 1) != fa[x] && !h[i])
			fa[e[i].x] = i, dfs1(e[i].x);
}

int get(int x)
{
	if(!f[x]) return x;
	return f[x] = get(f[x]);
}

void work(int i)
{
	h[p] = h[p ^ 1] = 0;
	p = fa[i];
	h[p] = h[p ^ 1] = 1;
	s = dfn = 0; dfs(1, 0);
	for(int j = 1; j <= n; j ++)
		if(a[j] > ans[j]) break;
		else if(a[j] < ans[j])
		{
			for(int t = 1; t <= n; t ++)
				ans[t] = a[t];
				break;
		}
}

int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i = 1, x, y; i <= m; i ++)
	{
		scanf("%d%d", &x, &y);
		e[++ l].x = y, e[l].nx = fst[x], fst[x] = l;
		e[++ l].x = x, e[l].nx = fst[y], fst[y] = l;
	}
	if(m == n - 1)
	{
		dfs(1, 0);
		for(int i = 1; i <= n; i ++)
			printf("%d ", a[i]);
	}
	else
	{
		for(int i = 2; i <= l; i += 2)
		{
			int x = get(e[i].x), y = get(e[i ^ 1].x);
			if(x == y){ p = i; break; }
			f[x] = y;
		}
		int x = e[p].x, y = e[p ^ 1].x;
		h[p] = h[p ^ 1] = 1;
		dfs(1, 0);
		for(int i = 1; i <= n; i ++) ans[i] = a[i];
		dfs1(1);
		if(hi[x] < hi[y]) swap(x, y);
		int i, j;
		for(i = x; hi[i] > hi[y]; i = e[fa[i] ^ 1].x)
			work(i);
		for(j = y; i != j; i = e[fa[i] ^ 1].x, j = e[fa[j] ^ 1].x)
			work(i), work(j);
		for(int i = 1; i <= n; i ++)
			printf("%d ", ans[i]);
	}
	return 0;
}
