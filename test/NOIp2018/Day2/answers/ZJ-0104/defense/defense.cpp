#include <bits/stdc++.h>

typedef int INT;
#define int long long

const int N = 100000 + 10;
const int INF = 2000000000;
#define max(x, y) ((x) > (y) ? (x) : (y))
#define min(x, y) ((x) < (y) ? (x) : (y))

int n, m, c, C;

int va[N], dep[N];

int dp[N][2];

int t[N];

int head[N], cnt;

struct node { int to, nxt; } e[N << 1];

int ans;

int read()
{
	int sum = 0;
	char c = getchar(), f = c;
	while(c < '0' || c > '9') f = c, c = getchar();
	while(c >= '0' && c <= '9') sum = sum * 10 + c - '0', c = getchar();
	if(f == '-') sum *= -1;
	return sum;
}

char Get()
{
	char c = getchar();
	while(c < 'A' || c > 'C') c = getchar();
	return c;
}

void Add(int x, int y)
{
	e[++cnt] = (node) { y, head[x] }; head[x] = cnt;
	e[++cnt] = (node) { x, head[y] }; head[y] = cnt;
}

void Init(int u, int f)
{
	dp[u][0] = 0;
	dp[u][1] = va[u];
	for(int i = head[u]; i; i = e[i].nxt)
	{
		int v = e[i].to;
		if(v == f) continue;
		dep[v] = dep[u] + 1;
		Init(v, u);
		dp[u][0] = dp[u][0] + dp[v][1];
		dp[u][1] = dp[u][1] + min(dp[v][0], dp[v][1]);
	}
}

void DFS(int u, int s)
{
//	printf("%lld %lld\n", u, s);
	if(u == n + 1) { ans = min(ans, s); return ; }
	bool mrk = 0;
	for(int i = head[u]; i; i = e[i].nxt)
		if(t[e[i].to] == 0) { mrk = 1; break; }
	if(t[u] == -1 || t[u] == 1)
	{
		t[u] = 1;
		DFS(u + 1, s + va[u]);
		t[u] = -1;
	}
	if(mrk) return ;
	if(t[u] == -1 || t[u] == 0)
	{
		t[u] = 0;
		DFS(u + 1, s);
		t[u] = -1;
	}
}

void Solve0()
{
	for(int i = 1; i <= m; i++)
	{
		ans = INF;
		memset(t, -1, sizeof(t));
		int a = read();
		int x = read();
		int b = read();
		int y = read(); 
		t[a] = x;
		t[b] = y;
		DFS(1, 0);
		if(ans == INF) ans = -1;
		printf("%lld\n", ans);
	}
}

INT main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	n = read(), m = read();
	c = Get(), C = read();
	for(int i = 1; i <= n; i++) va[i] = read();
	for(int i = 1; i < n; i++)
	{
		int x = read();
		int y = read();
		Add(x, y);
	}
	dep[1] = 1;
	Init(1, 0);
	if(n <= 2000 && m <= 2000) { Solve0(); return 0; }
	for(int i = 1; i <= m; i++) printf("-1\n");
	return 0;
}
