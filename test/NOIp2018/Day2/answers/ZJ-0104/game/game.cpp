#include <bits/stdc++.h>

typedef int INT;
#define int long long

const int N = 0 + 10;
const int M = 1000000007;

int n, m;

int ans;

int read()
{
	int sum = 0;
	char c = getchar(), f = c;
	while(c < '0' || c > '9') f = c, c = getchar();
	while(c >= '0' && c <= '9') sum = sum * 10 + c - '0', c = getchar();
	if(f == '-') sum *= -1;
	return sum;
}

int pw(int a, int p)
{
	int res = 1;
	while(p)
	{
		if(p % 2) res = res * a % M;
		a = a * a % M;
		p /= 2;
	}
	return res;
}

int Wrk(int a, int b)
{
	if(a > b) std::swap(a, b);
	if(a == 1) return pw(2, b);
	return (Wrk(a - 1, a) * pw(a + 1, b - a + 1)) % M;
}

INT main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	n = read(), m = read();
	ans = Wrk(n, m);
	printf("%lld\n", ans);
	return 0;
}
