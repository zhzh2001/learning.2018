#include <bits/stdc++.h>

typedef int INT;

const int N = 5000 + 10;
const int INF = 10000000;
#define max(x, y) ((x) > (y) ? (x) : (y))
#define min(x, y) ((x) < (y) ? (x) : (y))

int n, m;

bool used[N];

int H[N], rt;

int mn, mrk, flg;

int head[N], cnt;

struct node { int to, nxt; } E[N << 1];

std::priority_queue <int> e[N];

int read()
{
	int sum = 0;
	char c = getchar(), f = c;
	while(c < '0' || c > '9') f = c, c = getchar();
	while(c >= '0' && c <= '9') sum = sum * 10 + c - '0', c = getchar();
	if(f == '-') sum *= -1;
	return sum;
}

void Add(int x, int y)
{
	e[x].push(-y);
	e[y].push(-x);
}

void Addedge(int x, int y)
{
	E[++cnt] = (node) { y, head[x] }; head[x] = cnt;
	E[++cnt] = (node) { x, head[y] }; head[y] = cnt;
}

void DFS(int u)
{
	used[u] = 1;
	while(e[u].empty() == 0)
	{
		int v = -e[u].top(); e[u].pop();
		if(used[v]) continue;
		printf(" %d", v);
		DFS(v);
	}
}

void Fnd(int u, int f)
{
	used[u] = 1;
	for(int i = head[u]; i; i = E[i].nxt)
	{
		int v = E[i].to;
		if(v == f) continue;
		if(used[v]) { H[u] = 1; rt = v; return ; }
		Fnd(v, u);
		if(rt == v) { rt = 0; return ; }
		if(rt) { H[u] = 1; return ; }
	}
}

void Wrk(int u, bool d)
{
	used[u] = 1;
	while(e[u].empty() == 0)
	{
		int v = -e[u].top(); e[u].pop();
		if(used[v]) continue;
		if(H[u] == 1 && d == 0 && mn != INF) mrk = 1, rt = u, mn = -e[u].top();
//		printf("\n>>> %d %d %d %d %d\n", u, v, mn, d, rt);
		if(H[v] == 1 && e[u].empty() && mrk && d && v > mn) { flg = 1; return ; }
		printf(" %d", v);
		bool D = d & e[u].empty();
		if(u == rt) D = 1;
		Wrk(v, D);
		if(u == rt && flg) mrk = 0, rt = 0, mn = INF;
		if(u == rt) mrk = 0, rt = 0;
	}
}

void Solve1()
{
	for(int i = 1; i <= m; i++)
	{
		int x = read();
		int y = read();
		Add(x, y);
	}
	printf("1");
	DFS(1);
}

void Solve2()
{
	for(int i = 1; i <= m; i++)
	{
		int x = read();
		int y = read();
		Add(x, y);
		Addedge(x, y);
	}
//	for(int i = head[17]; i; i = E[i].nxt) printf("%d\n", E[i].to);
	printf("1");
	memset(used, 0, sizeof(used)); Fnd(1, 0);
//	for(int i = 1; i <= n; i++)
//		if(H[i]) printf("*** %d\n", i);
	memset(used, 0, sizeof(used)); Wrk(1, 0);
}

INT main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	n = read(), m = read();
	if(n == m + 1) { Solve1(); return 0; }
	if(n == m) { Solve2(); return 0; }
	return 0;
}
