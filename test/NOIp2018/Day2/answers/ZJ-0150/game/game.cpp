#include<cstdio>
#include<iostream>
using namespace std;
const long long MOD=1e9+7;
int n,m,a[10][10];
long long ans=0;
bool check()
{
	for(int i=2;i<=m;i++)
	{
		if(a[1][i]>a[2][i-1]||a[2][i]>a[3][i-1])return false;
		if(a[1][i]==a[2][i-1])
			for(int j=i+1;j<=m;j++) if(a[2][j]!=a[3][j-1])return false;
	}
	return true;
}
void dfs(int x,int y)
{
	if(x>n)x=1,y++;
	if(y>m)
	{
		if(check())ans=(ans+1)%MOD;
		return;
	}
	a[x][y]=0;
	dfs(x+1,y);
	a[x][y]=1;
	dfs(x+1,y);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==2)
	{
		long long x=3ll;
		ans=1;
		for(int p=m-1;p;p>>=1,x=x*x%MOD)
			if(p&1)ans=ans*x;
		printf("%lld",1ll*ans*4%MOD);
		return 0;
	}
	if(n*m<10)
	{
		dfs(1,1);
		cout<<ans<<endl;
		return 0;
	}
	return 0;
}
