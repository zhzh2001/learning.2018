#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
const long long inf=10000000007;
const int N=100008;
struct Edge
{
	int nxt,to;
}edge[N];
long long f[N][2],g[N][2],val[N],ans;
int n,m;
bool tag[N];
int vis[N],cnt,head[N];
char type[10];
void add(int x,int y)
{
	edge[++cnt]=(Edge){head[x],y};
	head[x]=cnt;
}
void check()
{
	long long sum=0;
	memset(tag,false,sizeof(tag));
	for(int i=1;i<=n;i++)
		if(vis[i])
		{
			sum+=val[i];
			for(int p=head[i];p;p=edge[p].nxt)tag[edge[p].to]=true;
		}
	for(int i=1;i<=n;i++)
		if(!tag[i]&&!vis[i])return;
	ans=min(ans,sum);
}
void dfs(int x,int x1,int y1,int x2,int y2)
{
	if(x==x1||x==x2)dfs(x+1,x1,y1,x2,y2);
	if(x>n)
	{
		check();
		return;
	}
	vis[x]=0;
	dfs(x+1,x1,y1,x2,y2);
	vis[x]=1;
	dfs(x+1,x1,y1,x2,y2);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	cin>>type;
	for(int i=1;i<=n;i++)scanf("%lld",&val[i]);
	for(int i=1;i<n;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y); add(y,x);
	}
	if(type[0]=='A')
	{
		if(n<=2)
		{
			while(m--)
			{
				int x1,y1,x2,y2;
				scanf("%d%d%d%d",&x1,&y1,&x2,&y2);
				if(x1>x2)swap(x1,x2),swap(y1,y2);
				f[-1][0]=inf; f[-1][1]=inf;
				f[-2][0]=inf; f[-2][1]=inf;
				for(int i=1;i<=n;i++)
				if(i!=x1&&i!=x2)
				{
					f[i][0]=f[i-1][1];
					f[i][1]=min(f[i-2][0],f[i-2][1])+val[i];
				}
				else
					if(i==x1)
					{
						if(y1)f[i][1]=min(f[i-2][0],f[i-2][1])+val[i],f[i][0]=inf;
						else f[i][1]=inf,f[i][0]=f[i-1][1];
					}
					else
					{
						if(y2)f[i][1]=min(f[i-2][0],f[i-2][1])+val[i],f[i][0]=inf;
						else f[i][1]=inf,f[i][0]=f[i-1][1];
					}
				if(x2==n)printf("%lld\n",f[n][y2]); else printf("%lld\n",min(f[n][0],f[n][1]));
			}
		}
		else
		if(type[1]=='1')
		{
			f[1][1]=val[1]; f[1][0]=inf;
			f[2][1]=val[2]+val[1]; f[2][0]=val[1];
			for(int i=3;i<=n;i++)
				f[i][0]=f[i-1][1],f[i][1]=min(f[i-2][0],f[i-2][1])+val[i];
			g[n][1]=val[n]; g[n][0]=inf;
			g[n-1][1]=val[n-1]; g[n-1][0]=val[n];
			for(int i=n-2;i>=1;i--)
				g[i][0]=g[i+1][1],g[i][1]=min(g[i+2][0],g[i+2][1])+val[i];
			while(m--)
			{
				int x1,y1,x2,y2;
				scanf("%d%d%d%d",&x1,&y1,&x2,&y2);
				if(x1>x2)swap(x1,x2),swap(y1,y2);
				if(y2==1)printf("%lld\n",f[x2][y2]+g[x2][y2]-val[x2]);
				else printf("%lld\n",min(f[x2-1][1]+min(g[x2+1][0],g[x2+1][1]),g[x2+1][1]+min(f[x2-1][0],f[x2-1][1])));
			}
		}
		else
		{
			f[1][1]=val[1]; f[1][0]=inf;
			f[2][1]=val[2]; f[2][0]=val[1];
			for(int i=3;i<=n;i++)
				f[i][0]=f[i-1][1],f[i][1]=min(f[i-2][0],f[i-2][1])+val[i];
			g[n][1]=val[n]; g[n][0]=inf;
			g[n-1][1]=val[n-1]; g[n-1][0]=val[n];
			for(int i=n-2;i>=1;i--)
				g[i][0]=g[i+1][1],g[i][1]=min(g[i+2][0],g[i+2][1])+val[i];
			while(m--)
			{
				int x1,y1,x2,y2;
				scanf("%d%d%d%d",&x1,&y1,&x2,&y2);
				if(x1>x2)swap(x1,x2),swap(y1,y2);
				if(y1==y2&&y1==0)puts("-1");
				else printf("%lld\n",f[x1][y1]+g[x2][y2]);
			}
		}
	}
	if(type[0]=='C')
	{
		while(m--)
		{
			int x1,y1,x2,y2;
		scanf("%d%d%d%d",&x1,&y1,&x2,&y2);
		if(x1>x2)swap(x1,x2),swap(y1,y2);
		ans=inf;
		vis[x1]=y1; vis[x2]=y2;
		dfs(1,x1,y1,x2,y2);
		if(ans==inf)puts("-1");
		else printf("%lld\n",ans);
		}
	}
	return 0;
}
