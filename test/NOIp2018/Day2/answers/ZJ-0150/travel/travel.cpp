#include<cstdio>
#include<iostream>
#include<algorithm>
#include<vector>
#include<cstring>
using namespace std;
vector<int> g[5008];
int n,m,rt,x1,x2;
bool vis[5008],loop[5008];
void dfs1(int x,int fa)
{
	printf("%d ",x);
	sort(g[x].begin(),g[x].end());
	for(int i=0;i<g[x].size();i++)
		if(g[x][i]!=fa)dfs1(g[x][i],x);
}
int dfs(int x,int fa)
{
	vis[x]=true;
	for(int i=0;i<g[x].size();i++)
	{
		if(g[x][i]==fa)continue;
		if(vis[g[x][i]])
		{
			loop[x]=true;
			rt=g[x][i];
			return 1;
		}
		int tmp=0;
		if(tmp=dfs(g[x][i],x))
		{
			if(tmp==1)loop[x]=true;
			if(x==rt)tmp=2;
			return tmp;
		}
	}
	return 0;
}
void dfs3(int x,int fa,int flag)
{
	if(vis[x])return;
	vis[x]=true;
	printf("%d ",x);
	sort(g[x].begin(),g[x].end());
	for(int i=0;i<g[x].size();i++)
	{
		if(g[x][i]==fa)continue;
		if(!loop[g[x][i]])dfs1(g[x][i],x);
		else if(flag==x1&&g[x][i]<=x2||flag!=x1)dfs3(g[x][i],x,flag);
	}
}
void solve_loop(int x,int fa)
{
	printf("%d ",x);
	vis[x]=true;
	x1=0; x2=0;
	sort(g[x].begin(),g[x].end());
	for(int i=0;i<g[x].size();i++)
	{
		if(g[x][i]==fa)continue;
		if(loop[g[x][i]])
			if(!x1)x1=g[x][i]; else x2=g[x][i];
	}
	for(int i=0;i<g[x].size();i++)
	{
		if(g[x][i]==fa)continue;
		if(!loop[g[x][i]])dfs1(g[x][i],x);
		else dfs3(g[x][i],x,g[x][i]);
	}
}
void dfs2(int x,int fa)
{
	printf("%d ",x);
	vis[x]=true;
	sort(g[x].begin(),g[x].end());
	for(int i=0;i<g[x].size();i++)
		if(g[x][i]!=fa)
			if(!loop[g[x][i]])dfs2(g[x][i],x); else solve_loop(g[x][i],x);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		g[x].push_back(y);
		g[y].push_back(x);
	}
	if(m==n-1)
	{
		dfs1(1,0);
		return 0;
	}
	else
	{
		dfs(1,0);
		memset(vis,false,sizeof(vis));
		dfs2(1,0);
		return 0;
	}
	return 0;
}
