#include<cstdio>
#include<algorithm>
#include<cstring>
#define N 100005
int n,m,head[N],cnt=0,a,b,x,y,p[N];
long long f[2][N],pre[2][N],suf[2][N];
char tp[4];
struct edge{
	int to,nxt;
}e[N<<1];
void dfs(int now,int pre=0){
	f[1][now]=p[now],f[0][now]=0;
	for(int i=head[now];i;i=e[i].nxt)
	if(e[i].to!=pre){
		dfs(e[i].to,now);
		f[1][now]+=std::min(f[0][e[i].to],f[1][e[i].to]);
		f[0][now]+=f[1][e[i].to];
	}
	if(now==a)f[x^1][now]=1e15;
	if(now==b)f[y^1][now]=1e15;
	if(f[0][now]>1e15)f[0][now]=1e15;
	if(f[1][now]>1e15)f[1][now]=1e15;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,tp);
	for(int i=1;i<=n;++i)scanf("%d",p+i);
	if(n<=2000&&m<=2000){
		for(int i=1;i<n;++i){
			int u,v;scanf("%d%d",&u,&v);
			e[++cnt]=(edge){v,head[u]};head[u]=cnt;
			e[++cnt]=(edge){u,head[v]};head[v]=cnt;
		}
		while(m--){
			scanf("%d%d%d%d",&a,&x,&b,&y);
			memset(f,0,sizeof f);
			dfs(1);
			if(f[0][1]>=1e15&&f[1][1]>=1e15)
			puts("-1");else
			printf("%lld\n",std::min(f[0][1],f[1][1]));
		}
		return 0;
	}else
	if(*tp=='A'&&tp[2]=='1'){
		for(int i=1;i<n;++i){
			int u,v;scanf("%d%d",&u,&v);
			e[++cnt]=(edge){v,head[u]};head[u]=cnt;
			e[++cnt]=(edge){u,head[v]};head[v]=cnt;
		}
		pre[1][1]=p[1],pre[0][1]=1e15;
		suf[1][n]=p[n],suf[0][n]=0;
		for(int i=2;i<=n;++i){
			pre[1][i]=p[i]+std::min(pre[1][i-1],pre[0][i-1]);
			pre[0][i]=p[i]+pre[1][i-1];
			if(pre[1][i]>1e15)pre[1][i]=1e15;
			if(pre[0][i]>1e15)pre[0][i]=1e15;
		}
		for(int i=n-1;i;--i){
			suf[1][i]=p[i]+std::min(suf[1][i+1],suf[0][i+1]);
			suf[0][i]=p[i]+suf[1][i-1];
			if(suf[1][i]>1e15)suf[1][i]=1e15;
			if(suf[0][i]>1e15)suf[0][i]=1e15;
		}
		while(m--){
			int b,y;
			scanf("%*d%*d%d%d",&b,&y);
			if(y==1){
				long long ans=p[b]+std::min(pre[0][b-1],pre[1][b-1])+std::min(suf[0][b+1],suf[1][b+1]);
				if(ans<1e15)
				printf("%lld\n",ans);else puts("-1");
			}else{
				long long ans=pre[1][b-1]+suf[1][b+1];
				if(ans<1e15)
				printf("%lld\n",ans);else puts("-1");
			}
		}
	}else
	if(*tp=='A'&&tp[2]=='2'){
		for(int i=1;i<n;++i){
			int u,v;scanf("%d%d",&u,&v);
			e[++cnt]=(edge){v,head[u]};head[u]=cnt;
			e[++cnt]=(edge){u,head[v]};head[v]=cnt;
		}
		pre[1][1]=p[1],pre[0][1]=0;
		suf[1][n]=p[n],suf[0][n]=0;
		for(int i=2;i<=n;++i){
			pre[1][i]=p[i]+std::min(pre[1][i-1],pre[0][i-1]);
			pre[0][i]=p[i]+pre[1][i-1];
			if(pre[1][i]>1e15)pre[1][i]=1e15;
			if(pre[0][i]>1e15)pre[0][i]=1e15;
		}
		for(int i=n-1;i;--i){
			suf[1][i]=p[i]+std::min(suf[1][i+1],suf[0][i+1]);
			suf[0][i]=p[i]+suf[1][i-1];
			if(suf[1][i]>1e15)suf[1][i]=1e15;
			if(suf[0][i]>1e15)suf[0][i]=1e15;
		}
		while(m--){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(a>b)a^=b^=a^=b,x^=y^=x^=y;
			if(x==0&&y==0)puts("-1");else
			if(x==1&&y==1){
				long long ans=p[a]+p[b]+std::min(pre[0][a-1],pre[1][a-1])+std::min(suf[0][b+1],suf[1][b+1]);
				if(ans<1e15)
				printf("%lld\n",ans);else puts("-1");
			}else
			if(x==0&&y==1){
				long long ans=p[b]+pre[1][a-1]+std::min(suf[0][b+1],suf[1][b+1]);
				if(ans<1e15)
				printf("%lld\n",ans);else puts("-1");
			}else
			if(x==1&&y==0){
				long long ans=p[a]+pre[1][b+1]+std::min(pre[0][a-1],pre[1][a-1]);
				if(ans<1e15)
				printf("%lld\n",ans);else puts("-1");
			}
		}
	}
	return 0;
}
