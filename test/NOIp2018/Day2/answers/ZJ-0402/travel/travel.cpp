//skip1978 AK IOI
#include<cstdio>
#include<cstring>
#include<cctype>
#include<algorithm>
#define N 5050
struct istream{
	inline istream&operator>>(int&d){
		static int c;
		while(!isdigit(c=getchar()));
		for(d=0;isdigit(c);c=getchar())
		d=(d<<3)+(d<<1)+(c^'0');
		return*this;
	}
}cin;
int G[N][N],cnt[N];
int n,m,vis[N],uu,vv,ans[N],out[N],totans;
struct edge{
	int u,v;
}e[N];
void dfs(int now){
	vis[now]=1;
	ans[++totans]=now;
	for(register int i=1,sz=cnt[now];i<=sz;++i){
		register const int v=G[now][i];
		if(now==uu&&v==vv||now==vv&&v==uu)continue;
		if(!vis[v])dfs(v);
	}
}
inline bool check(){
	for(int i=1;i<=n;++i)
	if(ans[i]!=out[i])return ans[i]<out[i];
	return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	if(m==n-1){
		while(m--){
			int u,v;
			cin>>u>>v;
			G[u][++cnt[u]]=v;
			G[v][++cnt[v]]=u;
		}
		for(int i=1;i<=n;++i)
		std::sort(G[i]+1,G[i]+cnt[i]+1),vis[i]=0;
		dfs(1);
		for(int i=1;i<=n;++i)printf("%d ",ans[i]);
		return 0;
	}else{
		for(int i=1;i<=m;++i){
			int&u=e[i].u,&v=e[i].v;
			cin>>u>>v;
			G[u][++cnt[u]]=v;
			G[v][++cnt[v]]=u;
		}
		bool ok=0;
		for(int i=1;i<=n;++i)
		std::sort(G[i]+1,G[i]+cnt[i]+1);
		for(int i=1;i<=m;++i){
			uu=e[i].u,vv=e[i].v;
			totans=0;
			memset(vis,0,sizeof vis);
			dfs(1);
			if(totans==n&&(!ok||check())){
				ok=1;
				for(int j=1;j<=n;++j)out[j]=ans[j];
			}
		}
		for(int i=1;i<=n;++i)printf("%d ",out[i]);
	}
	return 0;
}
