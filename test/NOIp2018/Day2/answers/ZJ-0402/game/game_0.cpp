#include<cstdio>
#include<cstring>
int dp[2][256];
int n,m,C,DC,U;
bool bigger[256][256];
inline void upd(int&x,int y){if((x+=y)>=1000000007)x-=1000000007;}
int main(){
	scanf("%d%d",&n,&m);
	C=1<<(n-1),U=(1<<n)-1;
	for(int i=0;i<=U;++i)dp[0][i]=1;
/*	for(int i=0;i<DC;++i)
	for(int j=0;j<DC;++j)
	for(int k=0;k<n-2;++k)
	if((i>>k&1)>=(j>>k&1))bigger[i][j]=1;
*/	int cur=0;
	for(int t=m-1;t;--t){
		cur^=1;
		memset(dp[cur],0,sizeof*dp);
		for(int i=0;i<C;++i){
			const int _0=i<<1,_1=_0|1;
			for(int j=i;;j=(j-1)&i){
				int add=dp[cur^1][j]+dp[cur^1][j|C];
				upd(dp[cur][_0],add),upd(dp[cur][_1],add);
				if(!j)break;
			}
		}
	}
	int ans=0;
	for(int i=0;i<=U;++i)upd(ans,dp[cur][i]);
	printf("%d\n",ans);
	return 0;
}
/*
3 3

112
*/
