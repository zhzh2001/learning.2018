#include<cstdio>
#include<cstring>
#include<vector>
#include<utility>
#include<algorithm>
#include<functional>
int dp[2][256];
int a[6][6];
std::vector<std::pair<int,int> >v;
int n,m,C,DC,U;
inline void upd(int&x,int y){if((x+=y)>=1000000007)x-=1000000007;}
inline int popcount(int a){
	int ret=0;
	for(int i=0;i<20;++i)
	if(a>>i&1)++ret;
	return ret;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m>3){
		C=1<<(n-1),U=(1<<n)-1;
		for(int i=0;i<=U;++i)dp[0][i]=1;
	/*	for(int i=0;i<DC;++i)
		for(int j=0;j<DC;++j)
		for(int k=0;k<n-2;++k)
		if((i>>k&1)>=(j>>k&1))bigger[i][j]=1;
	*/	int cur=0;
		for(int t=m-1;t;--t){
			cur^=1;
			memset(dp[cur],0,sizeof*dp);
			for(int i=0;i<C;++i){
				const int _0=i<<1,_1=_0|1;
				for(int j=i;;j=(j-1)&i){
					int add=dp[cur^1][j]+dp[cur^1][j|C];
					upd(dp[cur][_0],add),upd(dp[cur][_1],add);
					if(!j)break;
				}
			}
		}
		int ans=0;
		for(int i=0;i<=U;++i)upd(ans,dp[cur][i]);
		printf("%d\n",ans);
	}else{
		int ans=0;
		int*dp=*::dp;
		int N=n*m,p=n+m-2;
		for(int i=0;i<1<<N;++i){
			for(int j=0;j<N;++j)
			a[j/m][j%m]=i>>j&1;
			v.clear();
			for(int j=0;j<1<<p;++j)
			if(popcount(j)==m-1){
				int x=0,y=0,s=0;
				for(int k=0;k<p;++k){
					s|=a[x][y]<<k;
					if(j>>k&1)++y;else ++x;
				}
				v.push_back(std::make_pair(j,s));
			}
			std::sort(v.begin(),v.end(),std::greater<std::pair<int,int> >());
			bool ok=1;
			for(int i=1;i<v.size();++i)
			for(int j=0;j<i;++j)
			if(v[i].second>v[j].second)ok=0;
			ans+=ok;
			/*if(ok){
				for(int i=0;i<n;++i,putchar('\n'))
				for(int j=0;j<m;++j)
				printf("%d ",a[i][j]);
				putchar('\n');
				int zt=0;
				for(int j=0;j<n;++j)zt|=a[j][0]<<j;
				++dp[zt];
			}*/
		}
		printf("%d\n",ans);
	}
	return 0;
}
