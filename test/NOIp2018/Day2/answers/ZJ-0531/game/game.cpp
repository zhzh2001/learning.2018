#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const long long TT = 1e9 + 7;
const int p[2][2] = {{0, 1}, {1, 0}};
int n, m, a[5][5], cnt = 0, pp = 0;

inline int read()
{
	char ch = getchar();
	int ret = 0, f = 1;
	while (ch > '9' || ch < '0')
	{
		if (ch == '-')
			f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9')
		ret = ret * 10 + ch - '0', ch = getchar();
	return ret * f;
}

bool cmp(int s, int g)
{
	for (int i = 0; i < n + m - 2; ++i)
	{
		if ((s & (1 << i)) != (g & (1 << i)))
			return (s & (1 << i)) < (g & (1 << i));
	}
	return false;
}

bool wlk(int s, int g)
{
	int xl = 1, xr = 1, yl = 1, yr = 1;
	for (int i = 0; i < n + m - 2; ++i)
	{
		if (s & (1 << i))
			xl += p[1][0], yl += p[1][1];
		else
			xl += p[0][0], yl += p[0][1];
		
		if (g & (1 << i))
			xr += p[1][0], yr += p[1][1];
		else
			xr += p[0][0], yr += p[0][1];
		if (a[xl][yl] != a[xr][yr])
			return a[xl][yl] < a[xr][yr];
	}
	return true;
}

int check()
{
	for (int s = 0; s <= (1 << (n + m - 2)) - 1; ++s)
	{
		int op = s, ti = 0;
		while (op)
		{
			ti++;
			op -= op & -op;
		}
		if (ti != n - 1)
			continue;
		for (int g = 0; g <= (1 << (n + m - 2)) - 1; ++g)
		{
			op = g, ti = 0;
			while (op)
			{
				ti++;
				op -= op & -op;
			}
			if (ti != n - 1)
				continue;
			for (int i = 0; i <= n + m - 2; ++i)
			{
				if (cmp(s, g))
				{
					if (!wlk(s, g))
						return false;
				}
			}
		}
	}
	cnt++;
	return true;
}

int DFS(int x, int y)
{
	if (x > n)
		return check();
	if (y > m)
		return DFS(x + 1, 1);
	int res = 0;
	a[x][y] = 0;
	res += DFS(x, y + 1);
	a[x][y] = 1;
	res += DFS(x, y + 1);
	return res;
}

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	n = read(), m = read();
	if (n == 1)
	{
		long long now = 1;
		for (int i = 1; i <= m; ++i)
		{
			now = (now << 1) % TT;
		}
		printf("%lld\n", now);
		return 0;
	}
	else if (n * m <= 14)
		printf("%d\n", DFS(1, 1));
	else
	{
		long long now = 1, p = n + 1;
		for (int i = 1; i < m; ++i)
		{
			now = (now * p) % TT;
		}
		now = (now << 2) % TT;
		printf("%lld", now);
	}
	return 0;
}
