#include <cstdio>
#include <cstring>
#include <algorithm>
#define maxn 100005
#define maxe (maxn << 1)

using namespace std;

const long long INF = (long long)100000000001;
long long f[maxn][2], a[maxn];
int n, m, vis[maxn];
int lnk[maxn], son[maxe], nxt[maxe], tot = 1;

inline int read()
{
	char ch = getchar();
	int ret = 0, f = 1;
	while (ch > '9' || ch < '0')
	{
		if (ch == '-')
			f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9')
		ret = ret * 10 + ch - '0', ch = getchar();
	return ret * f;
}

void adde(int x, int y)
{
	son[++tot] = y, nxt[tot] = lnk[x], lnk[x] = tot;
}

void DFS(int now, int pre)
{
	f[now][1] = a[now], f[now][0] = 0;
	for (int j = lnk[now]; j; j = nxt[j])
	{
		if (son[j] == pre)
			continue;
		DFS(son[j], now);
		f[now][0] += f[son[j]][1];
		f[now][1] += min(f[son[j]][0], f[son[j]][1]);
	}
	if (vis[now] < 0)
		f[now][0] = INF;
	if (vis[now] > 0)
		f[now][1] = INF;
}

int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	n = read(), m = read(), read();
	for (int i = 1; i <= n; ++i)
		a[i] = read();
	for (int i = 1; i < n; ++i)
	{
		int x = read(), y = read();
		adde(x, y), adde(y, x);
	}
	while (m--)
	{
		int x = read(), a = read();
		int y = read(), b = read();
		bool flg = true;
		if (a + b == 0)
		{
			for (int j = lnk[x]; j; j = nxt[j])
			{
				if (son[j] == y)
					flg = false;
			}
		}
		if (flg)
		{
			vis[x] = a ? -1 : 1;
			vis[y] = b ? -1 : 1;
			DFS(1, -1);
			printf("%lld\n", min(f[1][0], f[1][1]));
		}
		else
			printf("%d\n", -1);
		vis[x] = vis[y] = 0;
	}
	return 0;
}
