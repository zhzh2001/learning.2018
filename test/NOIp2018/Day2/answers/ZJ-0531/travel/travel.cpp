#include <cstdio>
#include <cstring>
#include <algorithm>
#define maxn 5005
#define maxe (maxn << 1)

using namespace std;

int n, m, stck[maxn], top, cir = 1, in[maxn], cnt;
int ans[maxn], to[maxn][maxn], s[maxn], idx = 0;
int lnk[maxn], nxt[maxe], son[maxe], tot = 1;
bool vis[maxn], flg[maxn];
int num = 0;

inline int read()
{
	char ch = getchar();
	int ret = 0, f = 1;
	while (ch > '9' || ch < '0')
	{
		if (ch == '-')
			f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9')
		ret = ret * 10 + ch - '0', ch = getchar();
	return ret * f;
}

void adde(int x, int y)
{
	son[++tot] = y, nxt[tot] = lnk[x], lnk[x] = tot;
}

void Trump()
{
	memset(flg, 1, sizeof(flg));
	for (int i = 1; i <= n; ++i)
		flg[i] = true;
	top = 0;
	for (int i = 1; i <= n; ++i)
	{
		if (in[i] == 1)
			stck[++top] = i;
	}
	while (top)
	{
		int now = stck[top--];
		flg[now] = 0, cnt--;
		for (int j = lnk[now]; j; j = nxt[j])
		{
			in[son[j]]--;
			if (in[son[j]] == 1)
				stck[++top] = son[j];
		}
	}
}

void DFS(int now, int lst)
{
	if (!vis[now])
	{
		ans[++idx] = now;
		if (flg[now])
			cnt--;
	}
	to[now][0] = 0, vis[now] = 1;
	for (int j = lnk[now]; j; j = nxt[j])
	{
		if (!vis[son[j]])
			to[now][++ to[now][0]] = son[j];
	}
	sort(to[now] + 1, to[now] + 1 + to[now][0]);
	if (s[now] == 0)
		s[now] = 1;
	for (; s[now] <= to[now][0]; ++s[now])
	{
		if (s[now] == to[now][0])
		{
			if (to[now][s[now]] > lst && flg[to[now][s[now]]])
			{
				if (cir < 0 && cnt > 0 && num > 1)
				{
					if (flg[now])
						cir++;
					else
						num--;
					return;
				}
			}
			DFS(to[now][s[now]], lst);
		}
		else
			DFS(to[now][s[now]], to[now][s[now] + 1]);
	}
}

int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	n = read(), m = read();
	memset(s, 0, sizeof(s));
	for (int i = 1; i <= m; ++i)
	{
		int x = read(), y = read();
		adde(x, y), adde(y, x);
		in[x]++, in[y]++;
	}
//	for (int i = 1; i <= n; ++i)
//		fa[i] = i;
	if (n == m)
	{
		cir = -1, cnt = n;
		Trump();
		for (int i = 1; i <= n; ++i)
		{
			if (flg[i])
				continue;
				for (int j = lnk[i]; j; j = nxt[j])
			if (flg[son[j]])
			{
				num++;
				break;
			}
		}
	}
	DFS(1, n + 1);
	for (int i = 1; i <= n; ++i)
		printf("%d ", ans[i]);
	return 0;
}
