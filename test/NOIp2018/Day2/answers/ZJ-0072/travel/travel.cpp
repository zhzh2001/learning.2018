#include<bits/stdc++.h>
using namespace std;
const int N=5005;
int n,m,x,y,cnt=0,num=0,t;
int loop[N],ans[N],fa[N],vis[N];
bool flag=false,c[N];
vector<int> edge[N];
void dfs1(int u,int pa) {
    ans[++num]=u;
    vis[u]=num; fa[u]=pa;
    sort(edge[u].begin(),edge[u].end());
    for (int i=0,sz=edge[u].size();i<sz;i++) {
        int v=edge[u][i];
        if (v==pa || c[v]) continue;
        if (vis[v]) {
            for (int pos=u;pos!=v;pos=fa[pos]) {
                loop[++cnt]=pos;
                c[pos]=true;
            }
            t=vis[v];
            flag=true;
        }
        else dfs1(v,u);
    }
}
int main() {
    freopen("travel.in","r",stdin);
    freopen("travel.out","w",stdout);
    scanf("%d%d",&n,&m);
    for (int i=1;i<=m;i++) {
        scanf("%d%d",&x,&y);
        edge[x].push_back(y);
        edge[y].push_back(x);
    }
    dfs1(1,0);
    if (!flag) {
        for (int i=1;i<=num;i++) {
            printf("%d ",ans[i]);
        }
    } else {
        if (loop[1]<loop[cnt]) {
            int k=1;
            while (loop[k]<loop[cnt]) k++; k--;
            for (int i=1;i<=k;i++) {
                ans[++t]=loop[i];
            }
            for (int i=cnt;i>k;i--) {
                ans[++t]=loop[i];
            }
        } else {
            int k=cnt;
            while (loop[k]<loop[1]) k--; k++;
            for (int i=cnt;i>=k;i--) {
                ans[++t]=loop[i];
            }
            for (int i=1;i<k;i++) {
                ans[++t]=loop[i];
            }
        }
        for (int i=1;i<=num;i++) {
            printf("%d ",ans[i]);
        }
    }
    return 0;
}