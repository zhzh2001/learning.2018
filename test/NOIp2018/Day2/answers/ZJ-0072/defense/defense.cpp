#include<bits/stdc++.h>
using namespace std;
const int N=1e5+7,inf=1e9+7;
char type[N];
int n,m,p[N],dp[1005][1005];
int a,b,x,y,dep[N];
vector<int> edge[N];
inline int min(int x,int y) {return x<y?x:y;}
void dfs(int u,int fa) {
    dep[u]=dep[fa]+1;
    dp[u][0]=0; dp[u][1]=p[u];
    for (int i=0,sz=edge[u].size();i<sz;i++) {
        int v=edge[u][i];
        if (v==fa) continue;
        if (i==0) {
            dp[u][0]=dp[v][1];
            dp[u][1]=min(dp[v][1],dp[v][0])+p[u];
        }
        else {
            dp[u][0]=min(dp[u][0],dp[v][1]);
            dp[u][1]=min(dp[u][1],min(dp[v][1],dp[v][0])+p[u]);
        }
    }
}
int main() {
    freopen("defense.in","r",stdin);
    freopen("defense.out","w",stdout);
    scanf("%d%d%s",&n,&m,&type);
    for (int i=1;i<=n;i++) scanf("%d",&p[i]);
    for (int i=1;i<n;i++) {
        scanf("%d%d",&x,&y);
        edge[x].push_back(y);
        edge[y].push_back(x);
    }
    dfs(1,0);
    for (int i=1;i<=m;i++) {
        scanf("%d%d%d%d",&a,&x,&b,&y);
        if (a==1&&x==1) printf("%d\n",dp[1][1]);
        else printf("%d\n",min(dp[1][0],dp[1][1]));
    }
    return 0;
}
