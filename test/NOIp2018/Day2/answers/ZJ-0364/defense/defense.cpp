#include<bits/stdc++.h>
using namespace std;
int read()
{
	char ch=getchar();int ff=1,x=0;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff; 
}
void write(int aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
int n,m,mx;
int p[100005];
int tot,head[100005],nx[200005],to[200005];
int faa[100005],f[100005][2],g[100005][2];
int vis[100005],dep[100005];
char s[105];
void jia(int aa,int bb)
{
	tot++;
	nx[tot]=head[aa];
	to[tot]=bb;
	head[aa]=tot;
	return;
}
void dfs(int rt,int fa)
{
	faa[rt]=fa;f[rt][1]=p[rt];
	dep[rt]=dep[fa]+1;
	for(int i=head[rt];i;i=nx[i])
	{
		int yy=to[i];
		if(yy==fa) continue;
		dfs(yy,rt);
		f[rt][0]+=f[yy][1];
		f[rt][1]+=min(f[yy][1],f[yy][0]);
	}
	return;
}
void up(int rt)
{
	vis[rt]=1;
	g[rt][1]=p[rt];g[rt][0]=0;
	for(int i=head[rt];i;i=nx[i])
	{
		int yy=to[i];
		if(yy==faa[rt]) continue;
		if(vis[yy]) g[rt][0]+=g[yy][1],g[rt][1]+=min(g[yy][1],g[yy][0]);
		else g[rt][0]+=f[yy][1],g[rt][1]+=min(f[yy][1],f[yy][0]);
//		if(rt==5) cout<<yy<<" "<<g[yy][1]<<" "<<vis[yy]<<endl;
	}
	if(g[rt][0]==f[rt][0]&&g[rt][1]==f[rt][1]) return;
	if(rt!=faa[rt]) up(faa[rt]);
	return;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();
	scanf("%s",s);
	for(int i=1;i<=n;++i) p[i]=read(),mx+=p[i];
	for(int i=1;i<n;++i)
	{
		int x=read(),y=read();
		jia(x,y);jia(y,x);
	}
	dfs(1,1);
	while(m--)
	{
		int a=read(),x=read(),b=read(),y=read();
		if(dep[a]<dep[b]) swap(a,b),swap(x,y);
		if(x==0&&y==0&&faa[a]==b) {puts("-1");continue;}
		vis[a]=1;
		g[a][x]=f[a][x];
		g[a][x^1]=mx+1;
		if(a!=faa[a]) up(faa[a]);
		if(!vis[b]) g[b][y]=f[b][y];
		vis[b]=1;
		g[b][y^1]=mx+1;
		if(b!=faa[b]) up(faa[b]);//if(m==1) cout<<"  "<<g[3][1]<<endl;
		int ans;
		if(vis[1]) ans=min(g[1][0],g[1][1]);
		else ans=min(f[1][0],f[1][1]);
		if(ans>=mx+1) puts("-1");
		else write(ans),puts("");
		while(vis[a]) vis[a]=0,a=faa[a];
		while(vis[b]) vis[b]=0,b=faa[b];
	}
	return 0;
}
