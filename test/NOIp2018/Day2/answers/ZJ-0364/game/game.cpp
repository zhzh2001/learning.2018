#include<bits/stdc++.h>
using namespace std;
long long read()
{
	char ch=getchar();long long ff=1,x=0;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff; 
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
const long long mod=1e9+7;
long long n,m,ans,sum;
long long mp[9][9],bj[100005];
string s[10005];
void tj(long long x,long long y,string tmp,long long bs)
{
	tmp+=(mp[x][y]?'1':'0');
	if(x==n&&y==m) {s[++sum]=tmp;return;}
	if(y<m) tj(x,y+1,tmp,bs+1);
	if(x<n) tj(x+1,y,tmp,bs+1);
}
bool cmp(long long aa,long long bb)
{
	return s[aa]<s[bb];
}
long long ok()
{
	sum=0;
	tj(1,1,"",0);
	for(long long i=1;i<=sum;++i) bj[i]=i;
	sort(bj+1,bj+sum+1,cmp);
	for(long long i=1;i<=sum;++i) if(bj[i]!=i) return 0;
	return 1;
}
void dfs(long long x,long long y)
{
	if(y==m+1) x++,y=1;
	if(x==n+1) 
	{
		if(ok()) 
		{
			ans++;
		}
		if(ans>=mod) ans-=mod;
		return;
	}
	if(x==1||y==m||mp[x-1][y+1]==0) 
	{
		mp[x][y]=0;
		dfs(x,y+1);
	}
	mp[x][y]=1;
	dfs(x,y+1);
	return;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if(n>m) swap(n,m);
	if(n==1) 
	{
		ans=1;
		for(long long i=1;i<=m;++i) ans=(ans*2)%mod;
		write(ans);
		return 0;
	}
	if(n==2) 
	{
		ans=4;
		for(long long i=1;i<m;++i) ans=(ans*3)%mod;
		write(ans);
		return 0;
	}
	dfs(1,1);
	write(ans);
	return 0;
}
