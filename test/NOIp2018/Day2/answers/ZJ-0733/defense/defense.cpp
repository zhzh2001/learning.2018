#include <bits/stdc++.h>
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define per(i,r,l) for(int i=r;i>=l;i--)
#define N 100005
int n,m;char tp[5];
int cost[N],deg[N],e[2005][2005];
int must[N];
long long dp[N][2];
void DP(int fr,int x){
	
	rep(i,0,1){
		if(must[x]+i==1){
			dp[x][i]=1e18;
			continue;
		}
		long long tmp;
		if(i==0) tmp=0;
		else tmp=cost[x];
		
		rep(j,1,deg[x]){
			int y=e[x][j];
			if(y==fr) continue;
			DP(x,y);
			if(i==0) tmp+=dp[y][1];
			else tmp+=std::min(dp[y][0],dp[y][1]);
		}
		dp[x][i]=tmp;
	}
}
void bf(){
	rep(i,1,n) scanf("%d",&cost[i]);
	memset(deg,0,sizeof(deg));
	rep(i,1,n-1){
		int u,v;
		scanf("%d%d",&u,&v);
		e[u][++deg[u]]=v;
		e[v][++deg[v]]=u;
	}
	while(m--){
		int a,x,b,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		if(x==0&&y==0){
			bool goudai=0;
			rep(i,1,deg[a]) if(e[a][i]==b){
				goudai=1;break;
			}
			if(goudai){
				puts("-1");continue;
			}
		}
		rep(i,1,n) must[i]=-1;
		must[a]=x;must[b]=y;
		DP(0,1);
		printf("%d\n",std::min(dp[1][0],dp[1][1]));
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	
	scanf("%d%d%s",&n,&m,tp);
	if(n<=2000) bf();
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
