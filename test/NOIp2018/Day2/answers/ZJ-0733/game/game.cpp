//Actually we could do it by complexity of O(log)?
#include <bits/stdc++.h>
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define per(i,r,l) for(int i=r;i>=l;i--)
#define M 1000000007
#define N 1000005
int n,m;
long long ans,s;
long long p2[N],p3[N],p4[N];
inline long long P(int b,int t){
	if(t<=0) return 1;
	if(b==2) return p2[t];
	if(b==3) return p3[t];
	return p4[t];
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	p2[0]=p3[0]=p4[0]=1;
	rep(i,1,1000000){
		p2[i]=p2[i-1]*2%M;
		p3[i]=p3[i-1]*3%M;
		p4[i]=p4[i-1]*4%M;
	}
	
	
	scanf("%d%d",&n,&m);
	if(n>m) std::swap(n,m);
	
	if(n==1){
		ans=1;
		ans=ans*P(2,m)%M;
		printf("%d\n",(int)ans);
		return 0;
	}
	
	if(n==2){
		ans=2;
		ans=ans*P(3,m-1)%M;
		ans=ans*2%M;
		printf("%d\n",(int)ans);
		return 0;
	}
	
	//assume n>=3
	
	ans=0;
	
	s=2*2;
	s=s*P(4,n-2)%M;//rep(i,3,n) s=s*4%M;
	s=s*P(3,m-n)%M;//rep(i,n+1,m) s=s*3%M;
	s=s*P(2,n+m-1-m)%M;//rep(i,m+1,n+m-1) s=s*2%M;
	ans=(ans+s)%M;
	
	s=2*1*2;
	if(n>3){
		s=s*5;
	}//adapt 4
	
	s=s*P(4,n-4)%M;//rep(i,5,n) s=s*4%M;
	rep(i,n+1,m) if(i==4) s=s*4%M;else s=s*3%M;//rima
	rep(i,m+1,n+m-1) if(i==4) s=s*3%M;else s=s*2%M;//rima
	ans=(ans+s)%M;
	
	rep(i,4,m){
		//assume m>=4
		s=2*1*1;
		{
			//rep(j,4,i-1) s=s*1;
			if(i<=n) s=s*4;else s=s*3;
			if(i+1>n){
				if(i==m) s=s*3;
				else s=s*4;
			}
			else{
				if(i==m) s=s*4;
				else s=s*5;
			}
			//adapt i+1
			s=s*P(4,n-i-1)%M;//rep(j,i+2,n) s=s*4%M;
			s=s*P(3,m-std::max(n+1,i+2)+1)%M;//rep(j,std::max(n+1,i+2),m) s=s*3%M;
			s=s*P(2,n+m-1-std::max(m+1,i+2)+1)%M;//rep(j,std::max(m+1,i+2),n+m-1) s=s*2%M;
		}
		ans=(ans+s)%M;
	//	printf("%d\n",s);
	}
	s=2*1*1;
	//rep(i,4,m) s=s*1;
	s=s*3;
	s=s*P(2,n+m-1-m-1)%M;//rep(i,m+2,n+m-1) s=s*2;
	ans=(ans+s)%M;
	
	std::swap(n,m);
	//////////////////////////////////////////////////!!!!!
	rep(i,4,m){
		//assume m>=4
		//assume n>m?
		s=2*1*1;
		{
			//rep(j,4,i-1) s=s*1;
			s=s*4;//if(i<=n) s=s*4;else s=s*3;
			if(i+1>n){
				if(i==m) s=s*3;
				else s=s*4;
			}
			else{
				if(i==m) s=s*4;
				else s=s*5;
			}
			//adapt i+1
			s=s*P(4,m-i-1)%M;//rep(j,i+2,m) s=s*4%M;
			s=s*P(3,n-std::max(m+1,i+2)+1)%M;//rep(j,std::max(m+1,i+2),n) s=s*3%M;
			s=s*P(2,n+m-1-std::max(n+1,i+2)+1)%M;//rep(j,std::max(n+1,i+2),n+m-1) s=s*2%M;
		}
		ans=(ans+s)%M;
	//	printf("%d\n",s);
	}
	s=2*1*1;
	//rep(i,4,m) s=s*1;
	if(n>m) s=s*4;else s=s*3;
	s=s*P(3,n-m-1)%M;//rep(i,m+2,n) s=s*3%M;
	s=s*P(2,n+m-1-std::max(m+2,n+1)+1)%M;//rep(i,std::max(m+2,n+1),n+m-1) s=s*2%M;
	ans=(ans+s)%M;
	
	printf("%d\n",(int)ans);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
