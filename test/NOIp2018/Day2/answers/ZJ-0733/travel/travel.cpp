#include <bits/stdc++.h>
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define N 5005
int n,m;
int deg[N],e[N][N];
int best[N],p[N],cnt;
int b1,b2,vis[N];
void dfs(int x){
	p[++cnt]=x;
	vis[x]=1;
	rep(i,1,deg[x]){
		int y=e[x][i];
		if(vis[y]) continue;
		if(x+y==b1&&x*y==b2) continue;
		dfs(y);
	}
}
void work(){
	memset(vis,0,sizeof(vis));
	cnt=0;
	dfs(1);
	if(cnt<n) return;
	rep(i,1,n){
		if(best[i]<p[i]) break;
		if(best[i]>p[i]){
			rep(j,1,n) best[j]=p[j];
			break;
		}
	}
	
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	rep(i,1,n) best[i]=n-i+1;
	memset(deg,0,sizeof(deg));
	rep(i,1,m){
		int x,y;
		scanf("%d%d",&x,&y);
		e[x][++deg[x]]=y;
		e[y][++deg[y]]=x;
	}
	rep(i,1,n) std::sort(e[i]+1,e[i]+deg[i]+1);
	b1=b2=0;
	if(m==n-1) work();
	else{
		rep(x,1,n) rep(i,1,deg[x]){
			int y=e[x][i];
			if(x<y){
				b1=x+y;b2=x*y;work();
			}
		}
	}
	rep(i,1,n) printf(i<n?"%d ":"%d\n",best[i]);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
