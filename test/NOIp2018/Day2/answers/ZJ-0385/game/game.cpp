#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

const int Mod=1e9+7;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}

int Lim;
struct Matrix{
	int v[1<<8][1<<8];
	Matrix(){
		memset(v,0,sizeof(v));
	}
	void Print(){
		Rep(i,0,Lim){
			Rep(j,0,Lim) printf("%d ",v[i][j]);puts("");
		}
	}
};

inline Matrix operator * (Matrix A,Matrix B){
	Matrix Res;
	Rep(i,0,Lim) Rep(j,0,Lim) Rep(k,0,Lim){
		Add(Res.v[i][j],1ll*A.v[i][k]*B.v[k][j]%Mod);
	}
	return Res;
}

int n,m;

inline int One(int val){
	if (val>0) return 1;else return 0;
}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();Lim=(1<<n)-1;
	if (m==3){
		if (n==1){puts("8");return 0;}
		if (n==2){puts("36");return 0;}
		if (n==3){puts("112");return 0;}
		int mul=1;
		Rep(i,1,n-3) mul=1ll*mul*3%Mod;
		int Ans=1ll*112*mul%Mod;
		printf("%d\n",Ans);
		return 0;
	}
	Matrix Base,Tra;
	Rep(i,0,Lim) Base.v[0][i]=1;
	Rep(a,0,Lim) Rep(b,0,Lim){
		bool flag=true;
		Rep(k,1,n-1) if (One(b&(1<<k-1))>One(a&(1<<k))) flag=false;
		if (flag) Add(Tra.v[a][b],1);
	}
//	Tra.Print();
	m=m-1;
	for (int i=1;i<=m;i*=2,Tra=Tra*Tra){
		if (i&m) Base=Base*Tra;
//		Base.Print();
	}
	int Ans=0;
	Rep(i,0,Lim) Add(Ans,Base.v[0][i]);
	printf("%d\n",Ans);
}
