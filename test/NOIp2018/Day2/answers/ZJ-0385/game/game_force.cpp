#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

const int Mod=1e9+7;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}

int Lim;
struct Matrix{
	int v[1<<8][1<<8];
	Matrix(){
		memset(v,0,sizeof(v));
	}
	void Print(){
		Rep(i,0,Lim){
			Rep(j,0,Lim) printf("%d ",v[i][j]);puts("");
		}
	}
};

inline Matrix operator * (Matrix A,Matrix B){
	Matrix Res;
	Rep(i,0,Lim) Rep(j,0,Lim) Rep(k,0,Lim){
		Add(Res.v[i][j],1ll*A.v[i][k]*B.v[k][j]%Mod);
	}
	return Res;
}

int n,m,f[10][512];

int main(){
//	freopen("game1.in","r",stdin);
	n=read(),m=read();Lim=(1<<n)-1;
	Rep(a,0,Lim) f[1][a]=1;
	Rep(i,2,m){
		Rep(a,0,Lim) Rep(b,0,Lim){
			bool flag=true;
			Rep(k,1,n-1) if (((b>>k-1)&1)>((a>>k)&1)) flag=false;
			if (flag) Add(f[i][b],f[i-1][a]);
		}
	}
	Rep(a,0,Lim) printf("%d ",f[m][a]);puts("");
}
