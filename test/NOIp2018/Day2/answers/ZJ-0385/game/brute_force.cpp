#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<string>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

const int Mod=1e9+7;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}

int Lim;
struct Matrix{
	int v[1<<8][1<<8];
	Matrix(){
		memset(v,0,sizeof(v));
	}
	void Print(){
		Rep(i,0,Lim){
			Rep(j,0,Lim) printf("%d ",v[i][j]);puts("");
		}
	}
};

inline Matrix operator * (Matrix A,Matrix B){
	Matrix Res;
	Rep(i,0,Lim) Rep(j,0,Lim) Rep(k,0,Lim){
		Add(Res.v[i][j],1ll*A.v[i][k]*B.v[k][j]%Mod);
	}
	return Res;
}

int n,m,f[10][512];
int Dat[15][15],Ans;

vector<string>V;

void find(int x,int y,string s){
	s+=Dat[x][y]+48;
	if (x==n && y==m){
		V.push_back(s);return;
	}
	if (x<=n) find(x+1,y,s);
	if (y<=m) find(x,y+1,s);
}
bool Spec(){
	bool flag=true;
	Rep(i,1,n-1) Rep(j,2,n){
		if (Dat[i][j]==1 && Dat[i+1][j-1]==0) flag=false;
	}
	return flag;
}
void check(){
	V.clear();
	string s="";
	find(1,1,s);
	bool flag=true;
	Rep(i,0,V.size()-2){
		if (V[i]<V[i+1]){
			flag=false;break;
		}
	}
	if (flag){
		Ans++;
	}
}

void Dfs(int x,int y){
	if (x>n){
		check();return;
	}
	if (y>m){
		Dfs(x+1,1);return;
	}
	Dat[x][y]=0;Dfs(x,y+1);
	Dat[x][y]=1;Dfs(x,y+1);
}

int main(){
//	freopen("game1.in","r",stdin);
	n=read(),m=read();
	Dfs(1,1);
	printf("Ans=%d\n",Ans);
}
