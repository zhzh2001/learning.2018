#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

const int N=100005;

int n,m,Vis[N],Ans[N],ans[N];
int Sta[N],Oncir[N],Ins[N],top;

vector<int>V[N];

void Dfs(int u,int fa){
	Ins[Sta[++top]=u]=true;
	Vis[Ans[++*Ans]=u]=true;
	sort(V[u].begin(),V[u].end());
	Rep(i,0,V[u].size()-1){
		int v=V[u][i];
		if (!Vis[v]) Dfs(v,u);else if (v!=fa && Ins[v]){
			while (Sta[top]!=v){
				Oncir[Sta[top]]=true;Ins[Sta[top]]=false;top--;
			}
			Oncir[Sta[top]]=true;Ins[Sta[top]]=false;top--;
		}
	}
	top--;
}
int Spot;
void Find(int u,int fa){
	Vis[u]=true; 
	ans[++*ans]=u;
	if (Spot==u){
		Rep(i,0,V[u].size()-1){
			int v=V[u][i];
			if (!Oncir[v] && !Vis[v]) Find(v,u);
		}
		Spot=0;return;
	}
	Rep(i,0,V[u].size()-1){
		int v=V[u][i];
		if (!Vis[v]) Find(v,u);
	}
}

bool Compare(int *A,int *B){
	Rep(i,1,*A) if (A[i]!=B[i]){
		return A[i]<B[i];
	}
	return false;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	Rep(i,1,m){
		int x=read(),y=read();
		V[x].push_back(y),V[y].push_back(x);
	}
	Dfs(1,0);
//	Rep(i,1,n) printf("%d\n",Oncir[i]);
	Rep(i,1,n) if (Oncir[i]){
		Spot=i;
		*ans=0;
		memset(Vis,0,sizeof(Vis));
		Find(1,0);
		if (*ans!=n) continue;
		if (Compare(ans,Ans)){
			Rep(j,1,n) Ans[j]=ans[j];
		}
	}
	Rep(i,1,n) printf("%d",Ans[i]),putchar(i==n?'\n':' ');
	/*
	int u=64;
	Rep(i,0,V[u].size()-1) printf("v=%d\n",V[u][i]);
	printf("Oncir=%d\n",Oncir[u]);
	*/
}
