#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<string>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

const int N=100005;
const ll INF=1e18+7;

ll f[N][2];
int A,X,B,Y,Val[N];

int n,m;
int lines,front[N];
struct Edge{
	int next,to;
}E[N*2];
inline void Addline(int u,int v){
	E[++lines]=(Edge){front[u],v};front[u]=lines;
}

void Dp(int u,int fa){
	f[u][0]=0;
	f[u][1]=Val[u];
	ll Delta=INF,size=0;
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=fa){
			Dp(v,u);
			f[u][1]+=min(f[v][0],f[v][1]);
			f[u][0]+=f[v][1];
		}
	}
	if (u==A){
		f[u][X^1]=INF;
	}
	if (u==B){
		f[u][Y^1]=INF;
	}
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),read();
	Rep(i,1,n) Val[i]=read();
	Rep(i,1,n-1){
		int x=read(),y=read();
		Addline(x,y),Addline(y,x);
	}
	Rep(i,1,m){
		A=read(),X=read(),B=read(),Y=read();
		Dp(1,0);
		ll res=min(f[1][0],f[1][1]);
		printf("%d\n",res<INF?res:-1);
	}
}
