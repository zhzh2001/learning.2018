#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=5005,maxe=maxn<<1;
int n,m,X,Y,U[maxn],V[maxn],len;
int e,lnk[maxn],nxt[maxe],son[maxe];
bool vis[maxn];
struct ltl{
	int u,v;
	bool operator <(ltl b)const{return v>b.v;}
}g[maxe];
struct ltl2{
	int a[maxn];
	bool operator <(ltl2 b)const{
		for (int i=1;i<=n;i++) if(a[i]^b.a[i]) return a[i]<b.a[i];
		return 0;
	}
}ans,now;
int read(){
	int ret=0;bool f=0;char ch=getchar();
	while(ch>'9'||ch<'0') f^=ch=='-',ch=getchar();
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();
	return f?-ret:ret;
}
void add_e(int x,int y){son[++e]=y;nxt[e]=lnk[x];lnk[x]=e;}
void DFS(int x){
	vis[now.a[++len]=x]=1;
	for (int i=lnk[x];i;i=nxt[i]){
		if(vis[son[i]]) continue;
		if(x==X&&son[i]==Y) continue;
		if(x==Y&&son[i]==X) continue;
		DFS(son[i]);
	}
}
void work(){
	len=0;
	memset(vis,0,sizeof vis);
	DFS(1);
	if(len==n) ans=min(ans,now);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for (int i=0;i<m;i++){
		U[i]=read(),V[i]=read();
		g[i<<1]=(ltl){U[i],V[i]};
		g[i<<1|1]=(ltl){V[i],U[i]};
	}
	sort(g,g+(m<<1));
	ans.a[1]=n+1;
	for (int i=0;i<(m<<1);i++) add_e(g[i].u,g[i].v);
	if(m==n) for (int i=0;i<m;i++) X=U[i],Y=V[i],work();
	else work();
	for (int i=1;i<=n;i++) printf("%d",ans.a[i]),putchar(i==n?'\n':' ');
	return 0;
}
