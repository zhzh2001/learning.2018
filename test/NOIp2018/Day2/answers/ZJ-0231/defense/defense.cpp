#include<cstdio>
#include<algorithm>
#define check(x) ((x)>=INF?-1:(x))
using namespace std;
const int maxn=100005,maxe=maxn<<1;
int n,m,w[maxn],fa[maxn],D[maxn];
int e,son[maxe],nxt[maxe],lnk[maxn];
long long F[maxn][2],f[maxn][2],INF=(long long)1<<40;
int read(){
	int ret=0;bool f=0;char ch=getchar();
	while(ch>'9'||ch<'0') f^=ch=='-',ch=getchar();
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();
	return f?-ret:ret;
}
void add_e(int x,int y){son[++e]=y;nxt[e]=lnk[x];lnk[x]=e;}
void DFS(int x){
	for (int i=lnk[x];i;i=nxt[i]) if(son[i]^fa[x]){
		fa[son[i]]=x;D[son[i]]=D[x]+1;
		DFS(son[i]);
		F[x][0]+=F[son[i]][1];
		F[x][1]+=min(F[son[i]][0],F[son[i]][1]);
	}
	F[x][1]+=w[x];
}
void work(int&x){
	f[fa[x]][0]=F[fa[x]][0]+f[x][1]-F[x][1];
	f[fa[x]][1]=F[fa[x]][1]+min(f[x][0],f[x][1])-min(F[x][0],F[x][1]);
	x=fa[x];
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();read();
	for (int i=1;i<=n;i++) w[i]=read();
	for (int i=1,x,y;i<n;i++){
		x=read(),y=read();
		add_e(x,y),add_e(y,x);
	}
	DFS(1);
	while(m--){
		int a=read(),x=read(),b=read(),y=read();
		if(a==b&&(x^y)){printf("-1\n");continue;}
		f[a][x]=F[a][x],f[a][x^1]=INF;
		f[b][y]=F[b][y],f[b][y^1]=INF;
		while(1){
			if(fa[a]==fa[b]){
				f[fa[a]][0]=F[fa[a]][0]+f[a][1]-F[a][1]+f[b][1]-F[b][1];
				f[fa[a]][1]=F[fa[a]][1]+min(f[a][0],f[a][1])-min(F[a][0],F[a][1])+min(f[b][0],f[b][1])-min(F[b][0],F[b][1]);
				a=fa[a];
				break;
			}
			if(D[a]<D[b]) swap(a,b);
			if(fa[a]==b){
				if(f[b][0]^INF) f[b][0]=F[b][0]+f[a][1]-F[a][1];
				if(f[b][1]^INF) f[b][1]=F[b][1]+min(f[a][0],f[a][1])-min(F[a][0],F[a][1]);
				a=fa[a];
				break;
			}
			work(a);
		}
		while(a^1) work(a);
		printf("%lld\n",check(min(f[1][0],f[1][1])));
	}
}
