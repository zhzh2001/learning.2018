#include<bits/stdc++.h>
using namespace std;
const int maxn = 5000/.9;
int n,m;
struct graph {
  struct edge{int u,v; bool operator<(const edge &a)const{return u<a.u||(u==a.u&&v<a.v);}};
  edge es[maxn*2];
  int beg[maxn];
  int ec;
  void aa(int x,int y) {
    ++ec; es[ec]=(edge){x,y};
  }
  void ae(int x,int y) {
    aa(x,y); aa(y,x);
  }
  void init() {
    sort(es+1,es+ec+1);
    for(int i=1;i<=ec;++i) {
      if(es[i].u!=es[i-1].u) beg[es[i].u]=i;
    }
  }
}g;
int lop[maxn], lopc;
int fa[maxn];
int vis[maxn];
int tme=3;
void Dfs(int x) {
  static bool ok=false;
  vis[x]=true;
  for(int i=g.beg[x]; g.es[i].u==x; ++i) {
    int y=g.es[i].v;
    if(y==fa[x]) continue;
    if(vis[y]) { // back edge
      int z=x;
      while(z!=y) {
        lop[++lopc]=z;
        z=fa[z];
      }
      lop[++lopc]=y;
      ok=true;
      return;
    }
    fa[y]=x; Dfs(y);
    if(ok) return;
  }
}
void Lop() {
  Dfs(1);
}
int ans[maxn];
int dfa[maxn], dfc;
int P,Q;
void dfs(int x) {
  dfa[++dfc]=x;
  vis[x]=tme;
  for(int i=g.beg[x]; g.es[i].u==x; ++i) {
    int y=g.es[i].v;
    if((x==P&&y==Q) || (x==Q&&y==P)) continue;
    if(vis[y]==tme) continue;
    dfs(y);
  }
}
void work(int p,int q) {
  ++tme;
  dfc=0;
  P=p,Q=q;
  dfs(1);
  bool good=false;
  for(int i=1;i<=dfc;++i) {
    if(dfa[i]<ans[i]) {good=true;break;}
    else if(dfa[i]>ans[i]) {good=false; break;}
  }
  if(good) memcpy(ans,dfa,sizeof(*ans)*(n+3));
}
int main() {
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  cin.sync_with_stdio(false);
  cin.tie(0);
  cin>>n>>m;
  for(int i=1;i<=m;++i) {
    int u,v; cin>>u>>v;
    g.ae(u,v);
  }
  g.init();
  ans[1]=n+233;
  if(m>=n) {
    Lop();
    for(int i=1,j=2;i<=lopc;++i,++j) {
      work(lop[i],lop[j]);
    }
    work(lop[1],lop[lopc]);
  } else {
    work(-1,-1);
  }
  for(int i=1;i<=n;++i) {
    (i==1?cout:(cout<<' '))<<ans[i];
  }
  cout<<endl;
  return 0;
}
