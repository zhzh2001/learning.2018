#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll inf = 0x3f3f3f3f3f3f3f3fll;
const int maxn = 100000/.9;
int n,m;
string type;
int p[maxn];
struct graph {
  int e;
  int beg[maxn], nxt[maxn*2], to[maxn*2];
  void aa(int x,int y) {
    ++e; nxt[e]=beg[x]; to[e]=y; beg[x]=e;
  }
  void ae(int x,int y) {
    aa(x,y); aa(y,x);
  }
} g;
struct Solver {
  virtual void init()=0;
  virtual ll qry(int a,int x,int b,int y)=0;
};
namespace BF{
  static const int maxn = 2000/.9;
  int must[maxn];
  ll f[maxn], ff[maxn];
  int vis[maxn], tme;
  #define Min(a,b) (((a)<(b))?(a):(b))
  void dfs(int x) {
    vis[x]=tme;
    f[x]=p[x]; ff[x]=0;
    for(int i=g.beg[x];i;i=g.nxt[i]) {
      int y=g.to[i];
      if(vis[y]==tme) continue;
      dfs(y);
      ff[x]+=f[y];
      f[x]+=Min(f[y],ff[y]);
    }
    if(must[x]>0) ff[x]=inf;
    else if(must[x]<0) f[x]=inf;
  }
  struct Bf:public Solver {
    virtual void init() {
      memset(must,0,sizeof must);
      memset(f,0,sizeof f);
      memset(ff,0,sizeof ff);
    }
    virtual ll qry(int a,int x,int b,int y) {
      must[a]=2*x-1; must[b]=2*y-1;
      ++tme;
      dfs(1);
      ll ans=min(f[1],ff[1]);
      must[a]=0; must[b]=0;
      return ans;
    }
  };
}using BF::Bf;
struct Undone:public Solver {
  virtual void init() {}
  virtual ll qry(int,int,int,int) {return -1;}
};
namespace LIAN {
  const int sz=16-1;
  struct data {
    typedef ll (rt)[2];
    ll z[2][2];
    rt &operator[](int x){return z[x];}
    const rt &operator[](int x)const{return z[x];}
    ll ga()const {
      return min(Min(z[0][0],z[0][1]),Min(z[1][0],z[1][1]));
    }
  };
  data operator+(const data &a, const data &b) {
    data t;
    t[0][0]=Min(a[0][0]+b[1][0],a[0][1]+b[0][0]);
    t[0][1]=Min(a[0][0]+b[1][1],a[0][1]+b[0][1]);
    t[1][0]=Min(a[1][0]+b[1][0],a[1][1]+b[0][0]);
    t[1][1]=Min(a[1][0]+b[1][1],a[1][1]+b[0][1]);
    t[0][0]=Min(t[0][0],        a[0][1]+b[1][0]);
    t[0][1]=Min(t[0][1],        a[0][1]+b[1][1]);
    t[1][0]=Min(t[1][0],        a[1][1]+b[1][0]);
    t[1][1]=Min(t[0][1],        a[1][1]+b[1][1]);
    return t;
  }
  void ADD(data &t, const data &a, const data &b) {
    t[0][0]=Min(a[0][0]+b[1][0],a[0][1]+b[0][0]);
    t[0][1]=Min(a[0][0]+b[1][1],a[0][1]+b[0][1]);
    t[1][0]=Min(a[1][0]+b[1][0],a[1][1]+b[0][0]);
    t[1][1]=Min(a[1][0]+b[1][1],a[1][1]+b[0][1]);
    t[0][0]=Min(t[0][0],        a[0][1]+b[1][0]);
    t[0][1]=Min(t[0][1],        a[0][1]+b[1][1]);
    t[1][0]=Min(t[1][0],        a[1][1]+b[1][0]);
    t[1][1]=Min(t[1][1],        a[1][1]+b[1][1]);
    t[0][0]=Min(t[0][0], inf);
    t[0][1]=Min(t[0][1], inf);
    t[1][0]=Min(t[1][0], inf);
    t[1][1]=Min(t[1][1], inf);
  }
  data t[sz*2];
  inline void maintain(int x) {
    while(x>0) {
      ADD(t[x],t[x<<1],t[(x<<1)|1]);
      x>>=1;
    }
  }
  inline void touch(int x) {
    t[sz+x].z[0][0]=0; t[sz+x].z[1][1]=p[x]; t[sz+x].z[0][1]=t[sz+x].z[1][0]=inf;
    maintain((sz+x)>>1);
  }
  inline void make(int x,int c) {
    if(c) {
      t[sz+x].z[0][0]=inf; t[sz+x].z[1][1]=p[x]; t[sz+x].z[0][1]=t[sz+x].z[1][0]=inf;
    } else {
      t[sz+x].z[0][0]=0  ; t[sz+x].z[1][1]=inf ; t[sz+x].z[0][1]=t[sz+x].z[1][0]=inf;
    }
    maintain((sz+x)>>1);
  }
  struct Lian:public Solver {
    virtual void init() {
      for(int i=1;i<=n;++i) {
        touch(i);
      }
    }
    virtual ll qry(int a,int x,int b,int y) {
      make(a,x); make(b,y);
      ll ans=t[1].ga();
      touch(a); touch(b);
      return ans;
    }
  };
}using LIAN::Lian;
int main() {
  freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout);
  cin.sync_with_stdio(false); cin.tie(0);
  cin>>n>>m>>type;
  for(int i=1;i<=n;++i) {
    cin>>p[i];
  }
  for(int i=1;i<n;++i) {
    int x,y;cin>>x>>y;
    g.ae(x,y);
  }
  Solver* s;
  if(n<=2000) {
    s=new Bf();
  } else if(type[0]=='A') {
    s=new Lian();
  } else {
    s=new Undone();
  }
  s->init();
  for(int i=1;i<=m;++i) {
    int a,x,b,y;
    cin>>a>>x>>b>>y;
    ll cc=s->qry(a,x,b,y);
    if(cc>=inf/2) cc=-1;
    cout<<cc<<'\n';
  }
  return 0;
}
