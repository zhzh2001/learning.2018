#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int mo = 1e9+7;
const int maxn = 1e6/.9;
int fac[maxn*2];
int ifac[maxn*2];
int modpow(ll a, ll b, int p=mo) {
  ll c=1;
  while(b) {
    if(b&1) (c*=a)%=p;
    (a*=a)%=p;
    b>>=1;
  }
  return c;
}
int niy(int x) {
  return modpow(x,mo-2);
}
void Init() {
  int z=maxn*2-5;
  fac[0]=1;
  for(int i=1;i<=z;++i) {
    fac[i]=fac[i-1]*(ll)i%mo;
  }
  ifac[z]=niy(fac[z]);
  for(int i=z-1;i>=0;--i) {
    ifac[i]=ifac[i+1]*ll(i+1)%mo;
  }
}
ll ga[10][10];
ll C(int x,int y) {
  return fac[x]*ifac[y]%mo*ifac[x-y]%mo;
}
int zjt(int s,int h) {
//  if(s==0) ++s,--h;
//  if(h==0) return 1;
  ll ans=0;
  for(int i=0;i<h;++i) {
    ans+=ga[h-1][i] * C(h-i+s,s);
  }
  return ans%mo;
}
int n,m;

int main() {
  freopen("game.in","r",stdin);
  freopen("game.out","w",stdout);
  cin.sync_with_stdio(false); cin.tie(0);
  cin>>n>>m; if(n>m) swap(n,m);
  Init();
  ga[0][0]=1;
  for(int i=1;i<=9;++i) {
    ga[i][0]=ga[i-1][0];
    for(int j=1;j<=i;++j) {
      ga[i][j]=ga[i][j-1]+ga[i-1][j];
    }
  }
  ll sum=0;
  int rs=n+m-1;
  for(int i=1;i<rs-1;++i) {
    int k=min(min(i,rs-i-1)+1,n);
    ll ans=modpow(2,i);
    ll sig=0;
    int p=max(0,m-1-i);
    int q=max(0,n-1-i);
    for(int j=1;j<k;++j) {
      sig+=zjt(p,k-j);
      sig+=zjt(q,j);
    }
    (sum+=sig%mo*ans)%=mo;
  }
  cout<<sum+modpow(2,rs)<<endl;
  return 0;
}



