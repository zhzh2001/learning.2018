#include<bits/stdc++.h>
using namespace std;
int n=3,m=3;
int g[10][10];
bool sm(int (&a)[5], int (&b)[5]) {
  for(int j=0;j<5;++j) {
    if(a[j]<b[j]) return true;
    if(a[j]>b[j]) return false;
  }
  return true;
}
vector<vector<int> > v;
vector<int> now;
void dfs(int x,int y) {
  now.push_back(g[x][y]);
  if(x+1==n && y+1==m) {
    v.push_back(now);
    goto end;
  }
  if(x+1<n) dfs(x+1,y);
  if(y+1<m) dfs(x,y+1);
end:
  now.pop_back();
}
bool good() {
  v.clear();
  dfs(0,0);
  for(int i=1;i<v.size(); ++i) {
    if(v[i-1]<v[i]) return false;
  }
  return true;
}
vector<int> f[23333];
int Tot=0;
void Print() {
  ++Tot;
  //cout<<Tot<<endl;for(int i=0;i<n;++i){for(int j=0;j<m;++j)cout<<g[i][j];cout<<endl;}cout<<endl;
}
void Dfs(int x) {
  if(x>n+m-2) {
    if(good()) {
      Print();
    }
    return;
  }
  for(int i=0;i<=f[x].size(); ++i) {
    Dfs(x+1);
    if(i<f[x].size()) {
      g[f[x][i]][x-f[x][i]]=1;
    }
  }
  for(int i=0;i<f[x].size(); ++i) {
      g[f[x][i]][x-f[x][i]]=0;
  }
}
int main(int argc, char**argv) {
n=atoi(argv[1]); m=atoi(argv[2]);
  for(int j=0;j<m;++j) {
    for(int i=0;i<n;++i) {
      f[i+j].push_back(i);
    }
  }
  Dfs(0);
  cout<<Tot<<endl;
}
