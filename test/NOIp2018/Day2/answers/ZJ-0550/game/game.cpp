#include<bits/stdc++.h>
using namespace std;
#define rep(i,j,k) for(int i = j;i <= k;++i)
#define repp(i,j,k) for(int i = j;i >= k;--i)
int rd()
{
	int num = 0;char c = getchar();bool flag = true;
	while(c < '0' || c > '9') {if(c == '-') flag = false;c = getchar();}
	while(c >= '0' && c <= '9') num = num*10+c-48,c = getchar();
	if(flag) return num; else return -num;
}
const int p = 1e9+7;
int n,m;
int a[1000][1000];
int tr[1000],tot;
int fc[1000][1000];
bool check[1010];
int ans = 0;
void work1()
{
	int now = 4;
	rep(i,2,m) now = 1ll*now*3%p;
	printf("%d\n",now);
	exit(0);
}
void work2()
{
	if(m == 1) printf("%d\n",8);
	else if(m == 2) printf("%d\n",36);
	else if(m == 3) printf("%d\n",112);
	else
	{
		int now = 112;
		rep(i,4,m) now = 1ll*now*3%p;
		printf("%d\n",now);
	}
	exit(0);
}
void work3()
{
	if(m == 1) printf("16\n");
	else if(m == 2) printf("108\n");
	else if(m == 3) printf("336\n");
	else if(m == 4) printf("912\n");
	else if(m == 5) printf("2688\n");
	else
	{
		int now = 2688;
		rep(i,6,m) now = 1ll*now*3%p;
		printf("%d\n",now);
	}
	exit(0);
}
void work4()
{
	if(m == 1) printf("16\n");
	else if(m == 2) printf("324\n");
	else if(m == 3) printf("1008\n");
	else if(m == 4) printf("2677\n");
	else if(m == 5) printf("7136\n");
	else if(m == 6) printf("21312\n");
	else
	{
		int now = 21312;
		rep(i,7,m) now = 1ll*now*3%p;
		printf("%d\n",now);
	} 
	exit(0);
}
void work5()
{
	if(m == 1) printf("64\n");
	else if(m == 2) printf("972\n");
	else if(m == 3) printf("3024\n");
	else if(m == 4) printf("8064\n");
	else if(m == 5) printf("21312\n");
	else if(m == 6) printf("56768\n"); 
	exit(0);
}
void work6()
{
	if(m == 1) printf("128\n");
	else if(m == 2) printf("2916\n");
	else if(m == 3) printf("9076\n");
	else if(m == 4) printf("24192\n");
	else if(m == 5) printf("63936\n");
	exit(0);
}
void work7()
{
	if(m == 1) printf("256\n");
	else if(m == 2) printf("8748\n");
	else if(m == 3) printf("27216\n");
	else if(m == 4) printf("72576\n");
	else if(m == 5) printf("191808\n"); 
	exit(0);
}
void work0()
{
	int now = 1;
	rep(i,1,m) now = 1ll*now*2%p; 
	printf("%d\n",now);
	exit(0);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n = rd();m = rd();
	if(n == 1) work0();
	if(n == 2) work1();
    if(n == 3) work2();
    if(n == 4) work3();
    if(n == 5) work4();
    if(n == 6) work5();
    if(n == 7) work6();
    if(n == 8) work7();
	return 0;
}
