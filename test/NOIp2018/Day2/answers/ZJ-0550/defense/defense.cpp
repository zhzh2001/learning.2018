#include<bits/stdc++.h>
using namespace std;
#define rep(i,j,k) for(int i = j;i <= k;++i)
#define repp(i,j,k) for(int i = j;i >= k;--i)
#define ll long long
int rd()
{
	int num = 0;char c = getchar();bool flag = true;
	while(c < '0' || c > '9') {if(c == '-') flag = false;c = getchar();}
	while(c >= '0' && c <= '9') num = num*10+c-48,c = getchar();
	if(flag) return num; else return -num;
}
const int INF = 1e9;
int n,m;
int linkk[100100],t;
ll f[100100],g[100100];
int v[100100];
int spe[100100];
struct node{int n,y;}e[202000];
void insert(int x,int y)
{
	e[++t].y = y;e[t].n = linkk[x];linkk[x] = t;
	e[++t].y = x;e[t].n = linkk[y];linkk[y] = t;
}
void init()
{
	n = rd();m = rd();v[1] = rd();
	rep(i,1,n) v[i] = rd();
	rep(i,1,n-1)
	{
		int x = rd(),y = rd();
		insert(x,y);
	}
}
void dfs(int x,int fa)
{
	for(int i = linkk[x];i;i = e[i].n)
	    if(e[i].y != fa) dfs(e[i].y,x);
    ll sigma1 = 0,sigma2 = 0;
    for(int i = linkk[x];i;i = e[i].n)
        if(e[i].y != fa)
            sigma1 += min(g[e[i].y],f[e[i].y]),
            sigma2 += f[e[i].y];
    f[x] = sigma1 + v[x];
    g[x] = sigma2;
    if(spe[x] == 0) f[x] = INF;
    if(spe[x] == 1) g[x] = INF;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	init();
	rep(i,1,n) spe[i] = -1;
	rep(i,1,m)
	{
		int x = rd(),y = rd();
		spe[x] = y;
		int X = rd(),Y = rd();
		spe[X] = Y;
		bool ck = true;
		for(int i = linkk[x];i;i = e[i].n)
		    if(spe[e[i].y] != -1 && spe[e[i].y] == 0 && spe[x] == 0) {ck=false;break;}
		if(ck)
		{
			dfs(1,0);
			printf("%lld\n",min(g[1],f[1]));
		}
	    else printf("-1\n");
	    spe[x] = spe[X] = -1;
	}
	return 0;
}
