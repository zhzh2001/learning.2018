#include<stdio.h>
#include<bits/stdc++.h>
using namespace std;
#define inf 19999999999999999ll
#define int long long

struct edge{
	int t,nt;
}e[200003];
int cnt=0,head[100003];
int n,m,f[100003][2],cs[100003];
int x,xi,y,yi;

void add(int x,int y){
	e[++cnt].t=y;
	e[cnt].nt=head[x];
	head[x]=cnt;
}

void dfs(int u,int fa){
	int v;
	f[u][0]=0ll;
	f[u][1]=cs[u];
	for(int i=head[u];i;i=e[i].nt){
		v=e[i].t;
		if(v==fa) continue;
		dfs(v,u);
		f[u][1]+=min(f[v][0],f[v][1]);
		f[u][0]+=f[v][1];
	}
	if(u==x) f[u][xi^1ll]=inf;
	if(u==y) f[u][yi^1ll]=inf;
}

signed main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	string s;
	scanf("%lld%lld",&n,&m);
	cin>>s;
	for(int i=1ll;i<=n;i++) scanf("%lld",&cs[i]);
	for(int i=1ll;i<n;i++){
		scanf("%lld%lld",&x,&y);
		add(x,y),add(y,x);
	}
	int ans;
	for(int i=1ll;i<=m;i++){
		scanf("%lld%lld%lld%lld",&x,&xi,&y,&yi);
		memset(f,0x7fll,sizeof(f));
		dfs(1ll,-1ll);
		ans=min(f[1][0],f[1][1]);
		if(ans>=inf) ans=-1ll;
		printf("%lld\n",ans);
	}
	return 0;
}

