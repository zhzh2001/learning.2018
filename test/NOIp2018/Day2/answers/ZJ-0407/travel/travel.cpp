#include<stdio.h>
#include<bits/stdc++.h>
using namespace std;

struct edge{
	int t,nt;
}e[10004];
int cnt=0,head[5004];

void add(int x,int y){
	e[++cnt].t=y;
	e[cnt].nt=head[x];
	head[x]=cnt;
}

int n,m,lim;
bool vis[5004];
priority_queue< int,vector<int>,greater<int> > q[5004];
bool inh[5004],inz[5004],jout;
int z[5004],len=0;

void ddfs(int p,int fa){
	z[++len]=p;
	inz[p]=1;
	int v;
	for(int i=head[p];i;i=e[i].nt){
		v=e[i].t;
		if(v!=fa){
			if(inz[v]){
				lim=p;
				while(z[len]!=v){
					inh[z[len]]=1;
					len--;
				}
				inh[v]=1;
				jout=1;
				lim=max(p,z[len+1]);
				return ;
			}
			ddfs(v,p);
			if(jout) return;
			while(z[len]!=p) len--;
			
		}
	}
}

void dfs(int p,int fa){
	printf("%d ",p);
	vis[p]=true;
	int v;
	while(!q[p].empty()){
		v=q[p].top(),q[p].pop();
		if(n==m){
			if(inh[p]&&inh[v]&&v>=lim){
				lim=1999999999;
				continue;
			}
		}
		if(v==fa) continue;
		if(!vis[v]) dfs(v,p);
	}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	int x,y;
	for(int i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		q[x].push(y);
		q[y].push(x);
		add(x,y),add(y,x);
	}
	if(n==m) ddfs(1,-1);
	dfs(1,-1);
	return 0;
}

