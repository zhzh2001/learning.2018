#include<bits/stdc++.h>
#define ll long long
#define INF 1008208820
using namespace std;
struct node
{
	int to,next;
}edge[200010];
int n,m,f[100005][2],a[100005],num=0,head[100005],mp[100005],qa,qb,qx,qy;
char ch1,ch2;
inline ll read()
{
	ll f=1,x=0;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
void add_edge(int x,int y)
{
	edge[++num].to=y;
	edge[num].next=head[x];
	head[x]=num;
}
void dp(int x,int fa)
{
	int ans1=0,ans2=0;
	for (int i=head[x];i;i=edge[i].next)
	if (edge[i].to!=fa)
	{
	    dp(edge[i].to,x);
	    ans1+=f[edge[i].to][1];
	    ans2+=min(f[edge[i].to][0],f[edge[i].to][1]);
	}
	if (f[x][0]!=INF) f[x][0]=ans1;
	if (f[x][1]!=INF) f[x][1]=ans2+a[x];
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
    n=read(),m=read();ch1=getchar();ch2=getchar();
//    printf("%d@%d@%c@%c@",n,m,ch1,ch2);
    for (int i=1;i<=n;i++) a[i]=read();
    for (int i=1;i<n;i++)
    {
    	int x=read(),y=read();
    	add_edge(x,y);
    	add_edge(y,x);
    }
    	while (m--)
        {
    		qa=read(),qx=read(),qb=read(),qy=read();
    		memset(f,0,sizeof(f));
    		if (qx==1) f[qa][0]=INF; else f[qa][1]=INF;
    		if (qy==1) f[qb][0]=INF; else f[qb][1]=INF;
    		dp(1,0);
    		if (min(f[1][0],f[1][1])>=INF) printf("-1\n"); else
    		printf("%d\n",min(f[1][0],f[1][1]));
    	}
	return 0;
}
