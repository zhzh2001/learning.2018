#include<bits/stdc++.h>
#define ll long long
using namespace std;
int n,m,mp[5005][5005],a[5005];
bool flag[5005];
inline ll read()
{
	ll f=1,x=0;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
void dfs(int x)
{
	flag[x]=0;
	printf("%d ",x);
	for (int i=1;i<=n;i++)
	if (flag[i]&&mp[x][i]) dfs(i);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
    n=read(),m=read();
    for (int i=1;i<=m;i++) 
    {
    	int x=read(),y=read();
    	mp[x][y]=mp[y][x]=1;
    }
    for (int i=1;i<=n;i++) flag[i]=1;
    dfs(1);
	return 0;
}
