#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0)	{x=-x;putchar('-');}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
const int N=10005;
int n,m,q[N],top,cnt;
vector<int> e[N];
bool gg[5005][5005];
inline void Dfs(int x,int fa)
{
	q[++top]=x;if(top>n){return;}
	vector<int> vec;
	for(int i=0;i<e[x].size();++i)	if(e[x][i]!=fa&&!gg[e[x][i]][x])	vec.push_back(e[x][i]);
	if(!vec.size())	return;
	for(int i=0;i<vec.size();++i)
		Dfs(vec[i],x);
}
int ans[N],ex[N],ey[N];
inline bool pd()
{
	For(i,1,n)	if(ans[i]>q[i])	return 1;else	if(ans[i]<q[i])	return 0;
	return 0;
}
inline void main2()
{
	int top_ans=0;
	For(i,1,m)
	{
		gg[ex[i]][ey[i]]=gg[ey[i]][ex[i]]=1;
		top=0;
		Dfs(1,1);
		gg[ex[i]][ey[i]]=gg[ey[i]][ex[i]]=0;
		if(top!=n)	continue;
		if(top_ans==0||pd())	swap(ans,q),top_ans=n;
	}
	For(i,1,n)	write_p(ans[i]);
}
#define pb push_back
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();
	m=read();
	For(i,1,m)	
	{
		int x=read(),y=read();
		ex[i]=x;ey[i]=y;
		e[x].pb(y);e[y].pb(x);
	}
	For(i,1,n)	sort(e[i].begin(),e[i].end());
	if(m==n-1)	{Dfs(1,1);For(i,1,n)	write_p(q[i]);}
	else	main2();
		
}
