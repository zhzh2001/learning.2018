#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0)	{x=-x;putchar('-');}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}


int n,m;
ll mo=1e9+7;
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2,x=x*x%mo)	if(y&1)	sum=sum*x%mo;return sum;}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();m=read();
	if(n>m)	swap(n,m);
	if(n==1){writeln(ksm(2,m));return 0;}
	if(n==2)
	{
		writeln(4*ksm(3,m-1)%mo);
		return 0;
	}
	if(n==3)
	{
		ll ans=0;
		if(m==1)	ans=8;if(m==2)	ans=36;if(m==3)	ans=112;
		if(m>3)	ans=112*ksm(3,m-3)%mo;
		writeln(ans);
		return 0;
	}
	if(n==4)
	{
		ll ans=0;
		if(m==4)	ans=912;if(m==5)	ans=2688;
		if(m>5)	ans=1LL*2688*ksm(3,m-5)%mo;
		writeln(ans);
		return 0;
	}
	if(n==5)
	{
		ll ans=0;
		if(m==5)	ans=7136;
		if(m==6)	ans=21312;
		if(m>6)	ans=1LL*21312*ksm(3,m-6)%mo;
		writeln(ans);
		return 0;
	}
	if(n==6)
	{
		ll ans=0;
		if(m==6)	ans=56768;
		writeln(ans);
	}
}
