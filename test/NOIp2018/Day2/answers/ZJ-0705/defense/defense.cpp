#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define y1 wobaodanle
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0)	{x=-x;putchar('-');}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
const int N=2e5+5;
ll dp[N][2],v[N];
int n,m;
int poi[N*2],nxt[N*2],head[N],cnt,ned[N],Fa[N];
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;}
inline void Dfs(int x,int fa)//1->不能放  2->必须放
{
	dp[x][0]=0;dp[x][1]=v[x];
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		Dfs(poi[i],x);
		dp[x][0]+=dp[poi[i]][1];
		dp[x][1]+=min(dp[poi[i]][1],dp[poi[i]][0]);
	}
	if(ned[x]==1)	dp[x][1]=1e15;
	if(ned[x]==2)	dp[x][0]=1e15;
}
inline void P_Dfs(int x,int fa)
{
	Fa[x]=fa;
	for(int i=head[x];i;i=nxt[i])	if(poi[i]!=fa)	P_Dfs(poi[i],x);
}
char opt[5];
namespace Sub
{
	const int N=1e5+5;
	ll f[N][2],g[N][2];
	inline void Main()
	{
		f[1][1]=v[1];if(opt[2]=='1')	f[1][0]=1e15;
		For(i,2,n)	f[i][1]=min(f[i-1][0],f[i-1][1])+v[i],f[i][0]=f[i-1][1];
		Dow(i,1,n)	g[i][1]=min(g[i+1][0],g[i+1][1])+v[i],g[i][0]=g[i+1][1];
		if(opt[2]=='1')	g[1][0]=1e15;
		For(i,1,m)
		{
			int x1=read(),y1=read(),x2=read(),y2=read();
			if(opt[2]=='1')
			{
				ll ans=0;
				if(y2==1)	ans=min(f[x2-1][1],f[x2-1][0])+min(g[x2+1][0],g[x2+1][1])+v[x2];
				if(y2==0)	ans=f[x2-1][1]+g[x2+1][1];
				writeln(ans);
			}else
			{
				if(x1>x2)	swap(x1,x2),swap(y1,y2);
				if(y1+y2==0)	{puts("-1");continue;}
				ll ans=0;
				if(y1==0)	ans+=f[x1-1][1];else	ans+=min(f[x1-1][1],f[x1-1][0])+v[x1];
				if(y2==0)	ans+=g[x2+1][1];else	ans+=min(g[x2+1][1],g[x2+1][0])+v[x2];
				writeln(ans);
			}
		}
	}
}
namespace Sub2
{
	ll chg[N],chg_val[N][2];
	inline void Main()
	{	
		P_Dfs(1,1);
		ned[1]=2;
		Dfs(1,1);
		For(i,1,m)
		{
			read();read();
			int x=read(),y=read();
			int top=0;
			top=1;chg[top]=x;
			chg_val[1][1]=dp[x][1],chg_val[1][0]=dp[x][0];
			if(y==0)	dp[x][1]=1e15;if(y==1)	dp[x][0]=1e15;
			while(1)
			{
				int tmp=Fa[x];
				chg[++top]=tmp;
				chg_val[top][1]=dp[tmp][1];
				chg_val[top][0]=dp[tmp][0];
				dp[tmp][0]-=chg_val[top-1][1];	dp[tmp][0]+=dp[x][1];
				dp[tmp][1]-=min(chg_val[top-1][1],chg_val[top-1][0]);
				dp[tmp][1]+=min(dp[x][0],dp[x][1]);
				x=tmp;
				if(tmp==1)	break;
			}
			writeln(dp[1][1]);
			For(i,1,top)	dp[chg[i]][0]=chg_val[i][0],dp[chg[i]][1]=chg_val[i][1];
		}
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",opt+1);
	For(i,1,n)	v[i]=read();
	For(i,1,n-1)	add(read(),read());
	if(n>2000)
	{
		if(opt[1]=='A')	{Sub::Main();return 0;}
		if(opt[1]=='B')	{Sub2::Main();return 0;}
		if(opt[1]=='C')	{Sub2::Main();return 0;}
	}
	P_Dfs(1,1);
	For(i,1,m)
	{
		int x=read(),y=read();
		ned[x]=y+1;
		int x1=read(),y1=read();
		ned[x1]=y1+1;
		if(x==Fa[x1]||x1==Fa[x])	if(y1+y==0){ned[x]=ned[x1]=0;puts("-1");continue;}
		Dfs(1,1);
		writeln(min(dp[1][0],dp[1][1]));
		ned[x]=ned[x1]=0;
	}
}
