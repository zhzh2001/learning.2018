#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;
const int maxn=1000006,tt=1000000007;
int n,m,tem,S;
struct data{
	int x,y;
	bool operator <(const data&b)const{return y<b.y;}
}a[maxn];
int id(int x,int y){return (x-1)*m+y;}
int power(int x,int y){
	int sum=1,w=x;
	while(y){
		if(y&1)sum=(LL)sum*w%tt;
		w=(LL)w*w%tt;y>>=1;
	}return sum;
}
bool check(int x,int y){if(x<1||x>n||y<1||y>m)return 0;return 1;}
void dfs(int x,int y,int X,int Y){
	if(x==n&&y==m){a[++tem].x=X;a[tem].y=Y;return;}
	int t=id(x,y),p=S>>(t-1)&1,t1=n+m-(x+y);X+=p<<(t1-1);
	if(check(x+1,y))dfs(x+1,y,X,Y);
	if(check(x,y+1))dfs(x,y+1,X,Y+(1<<(t1-1)));
}
int main(){
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==1)printf("%d",(LL)power(2,m));else
	if(n==2)printf("%d",(LL)4*power(3,m-1)%tt);else
	if(n==3){
		if(m==1)printf("%d",8);else
		if(m==2)printf("%d",36);else printf("%d",(LL)112*power(3,m-3)%tt);
	}else{
		int ans=0;
		for(S=0;S<1<<(n*m);S++){
			tem=0;dfs(1,1,0,0);
			sort(a+1,a+1+tem);bool check=1;
			for(int i=2;i<=tem;i++) if(a[i].x>a[i-1].x){check=0;break;}
			ans+=check;
		}printf("%d",ans);
	}return 0;
}
