#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=5006;
inline char nc(){static char buf[100000],*i=buf,*j=buf;return i==j&&(j=(i=buf)+fread(buf,1,100000,stdin),i==j)?EOF:*i++;}
inline int _read(){char ch=nc();int sum=0;while(!(ch>='0'&&ch<='9'))ch=nc();
while(ch>='0'&&ch<='9')sum=sum*10+ch-48,ch=nc();return sum;}
int n,m,e,tot,tem,X,Y,b[maxn],ans[maxn],lnk[maxn],son[maxn*2],nxt[maxn*2];
bool vis[maxn];
struct data{
	int x,y;
	bool operator <(const data&b)const{return y>b.y;}
}a[maxn*2];
void add(int x,int y){nxt[++tot]=lnk[x];son[tot]=y;lnk[x]=tot;}
void dfs(int x){
	b[++tem]=x;vis[x]=1;
	for(int j=lnk[x];j;j=nxt[j]) if(!vis[son[j]]&&!((x==X&&son[j]==Y)||(x==Y&&son[j]==X)))dfs(son[j]);
}
int main(){
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	n=_read();e=_read();
	for(int i=1;i<=e;i++)a[++m].x=_read(),a[m].y=_read(),++m,a[m]=a[m-1],swap(a[m].x,a[m].y);
	sort(a+1,a+1+m);
	if(e==n-1){
		for(int i=1;i<=m;i++)add(a[i].x,a[i].y);
		memset(vis,0,sizeof(vis));
		dfs(1);for(int i=1;i<=n;i++)ans[i]=b[i];
	}else{
		for(int i=1;i<=n;i++)ans[i]=n;
		for(int i=1;i<=m;i++)add(a[i].x,a[i].y);
		for(int i=1;i<=m;i++){
			X=a[i].x;Y=a[i].y;for(int j=0;j<=n;j++)vis[j]=0;
			tem=0;dfs(1);
			if(tem<n)continue;
			bool check=0;
			for(int j=1;j<=n;j++) if(b[j]!=ans[j]){check=b[j]<ans[j];break;}
			if(check)for(int j=1;j<=n;j++)ans[j]=b[j];
		}
	}for(int i=1;i<=n;i++)printf("%d ",ans[i]);
	return 0;
}
