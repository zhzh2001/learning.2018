#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;
const int maxn=2006;
inline char nc(){static char buf[100000],*i=buf,*j=buf;return i==j&&(j=(i=buf)+fread(buf,1,100000,stdin),i==j)?EOF:*i++;}
inline int _read(){char ch=nc();int sum=0;while(!(ch>='0'&&ch<='9'))ch=nc();
while(ch>='0'&&ch<='9')sum=sum*10+ch-48,ch=nc();return sum;}
int n,m,tot,A,B,X,Y,INF,a[maxn],lnk[maxn],son[maxn*2],nxt[maxn*2];
LL f[maxn][2];
void add(int x,int y){nxt[++tot]=lnk[x];son[tot]=y;lnk[x]=tot;}
void dfs(int x,int fa){
	f[x][0]=0;f[x][1]=a[x];
	for(int j=lnk[x];j;j=nxt[j]) if(son[j]!=fa){
		dfs(son[j],x);
		if(f[son[j]][1]<INF)f[x][0]+=f[son[j]][1];else f[x][0]=INF;
		if(f[son[j]][1]<INF||f[son[j]][0]<INF)f[x][1]+=min(f[son[j]][1],f[son[j]][0]);else f[x][1]=INF;
	}
	if(x==A)f[x][X^1]=INF;if(x==B)f[x][Y^1]=INF;
}
int main(){
	freopen("defense.in","r",stdin);freopen("defense.out","w",stdout);
	n=_read();m=_read();_read();
	for(int i=1;i<=n;i++)a[i]=_read();
	for(int i=1,x,y;i<n;i++)x=_read(),y=_read(),add(x,y),add(y,x);
	memset(f,63,sizeof(f));INF=f[0][0];
	while(m--){
		A=_read();X=_read();B=_read();Y=_read();
		dfs(1,0);LL t=min(f[1][0],f[1][1]);
		if(t>=INF)printf("%d\n",-1);else printf("%lld\n",t);
	}return 0;
}
