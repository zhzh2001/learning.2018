#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline void ju() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
}
const int N = 5050;
int n, m;
bool a[N][N], vis[N];
inline void dfs(int u) {
	vis[u] = 1; printf("%d ", u);
	for(int i = 1; i <= n; ++i)
		if(a[u][i]&&!vis[i])
			dfs(i);
}
int main() {
	ju();
	scanf("%d%d", &n, &m);
	for(int i = 1; i <= m; ++i) {
		int x, y; scanf("%d%d", &x, &y);
		a[x][y] = a[y][x] = 1;
	}
	dfs(1);
	puts("");
	return 0;
}
