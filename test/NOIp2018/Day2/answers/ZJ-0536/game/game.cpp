#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline void ju() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
}
const int mo = 1e9+7;
int n, m;
const int a[3][3] = {{2, 4, 8},{4, 12, 36},{8, 36, 112}};
int main() {
	ju();
	scanf("%d%d", &n, &m);
	if(n==2) {
		ll ans = 4;
		for(int i = 1; i <= m-1; ++i) ans = ans*3%mo;
		printf("%lld\n", ans);
		return 0;
	}
	if(n<=3&&m<=3) {
		printf("%d\n", a[n-1][m-1]);
		return 0;
	}
	if(n==1) {
		ll ans = 2;
		for(int i = 1; i <= m-1; ++i) ans = ans*2%mo;
		printf("%lld\n", ans);
		return 0;
	}
	if(m==1) {
		ll ans = 2;
		for(int i = 1; i <= n-1; ++i) ans = ans*2%mo;
		printf("%lld\n", ans);
		return 0;
	}
	return 0;
}
