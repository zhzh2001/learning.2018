#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline void ju() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
}
const int N = 202000, INF = 1e9+7;
int n, m, cnt, p[N], d[N][2];
int fst[N], nxt[N], to[N];
char c;
bool vis[N];
inline void add(int x, int y) {
	nxt[++cnt] = fst[x]; fst[x] = cnt; to[cnt] = y;
}
inline void dfs(int u) {
	vis[u] = 1; int v;
	for(int i = fst[u]; i; i = nxt[i]) {
		v = to[i];
		if(!vis[v]) {
			dfs(v);
			if(d[u][0]<INF) d[u][0] += d[v][1];
			if(d[u][1]<INF) d[u][1] += min(d[v][0], d[v][1]);
		}
	}
	d[u][1] += p[u];
}
int main() {
	ju();
	scanf("%d%d", &n, &m);
	c = getchar(); c = getchar(); c = getchar();
	for(int i = 1; i <= n; ++i) scanf("%d", &p[i]);
	for(int i = 1; i < n; ++i) {
		int x, y; scanf("%d%d", &x, &y);
		add(x, y); add(y, x);
	}
	for(int i = 1; i <= m; ++i) {
		memset(d, 0, sizeof d);
		memset(vis, 0, sizeof vis);
		int t1, x, t2, y; scanf("%d%d%d%d", &t1, &x, &t2, &y);
		d[t1][1-x] = d[t2][1-y] = INF;
		dfs(1);
		int ans = min(d[1][0], d[1][1]);
		if(ans >= INF) puts("-1");
		else printf("%d\n", ans);
	}
	return 0;
}
