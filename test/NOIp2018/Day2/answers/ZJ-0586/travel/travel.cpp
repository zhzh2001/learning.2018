#include <bits/stdc++.h>
using namespace std;
#define rep(i,l,r) for(int i=l;i<=r;++i)
#define per(i,r,l) for(int i=r;i>=l;--i)
typedef long long ll;
inline int rd(){
	char c=getchar();int x=0,f=1;//ע��long long!!!!!!!!!!!!!!!!!!! 
	for(;c<'0'||c>'9';c=getchar())
		if(c=='-') f=-1;
	for(;c>='0'&&c<='9';c=getchar())
		x=x*10+c-'0';
	return x*f;
}
const int N=10009,M=10009;
int n,m;


int len,fst[N],to[M],nxt[M];
void add(int u,int v){
	to[++len]=v;nxt[len]=fst[u];fst[u]=len;
}

bool vis[N],cir[M],TG;
int f[N],tg[N];
void get(int fa){
	vis[fa]=1;
	for(int i=fst[fa];i;i=nxt[i]){
		int son=to[i];
		if(son!=f[fa]){
			if(vis[son]){
				if(!TG){
					TG=1;
					cir[i]=1;
					tg[fa]++;
					tg[f[son]]--;
				}
			}
			else{
				f[son]=fa;
				get(son);
				tg[fa]+=tg[son];
			}
		}
	}
	if(tg[fa])for(int i=fst[fa];i;i=nxt[i]){
		int son=to[i];
		if(f[son]==fa&&tg[son]) cir[i]=1;
	}
}

bool cant[M];
int a[N],no,ans[N];
void dfs(int fa,int anc){
	a[++no]=fa;
	priority_queue<int>q;
	while(!q.empty()) q.pop();
	for(int i=fst[fa];i;i=nxt[i])if(!cant[i]){
		int son=to[i];
		if(son!=anc) q.push(-son);
	}
	while(!q.empty()){
		dfs(-q.top(),fa);
		q.pop();
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=rd();m=rd();
	len=1;
	rep(i,1,m){
		int u=rd(),v=rd();
		add(u,v);add(v,u);
	}
	
	if(m==n-1){
		dfs(1,0);
		rep(i,1,n) printf("%d ",a[i]);
		return 0;
	}
	
	get(1);
	rep(i,1,n) ans[i]=n;
	per(o,len,2)if(cir[o]){
		cant[o]=cant[o^1]=1;
		no=0;
		dfs(1,0);
		rep(i,1,n) if(a[i]!=ans[i]){
			if(a[i]<ans[i]) rep(j,i,n) ans[j]=a[j];
			break;
		}
		cant[o]=cant[o^1]=0;
	}
	rep(i,1,n) printf("%d ",ans[i]);
	return 0;
}
