#include <bits/stdc++.h>
using namespace std;
#define rep(i,l,r) for(int i=l;i<=r;++i)
#define per(i,r,l) for(int i=r;i>=l;--i)
typedef long long ll;
inline int rd(){
	char c=getchar();int x=0,f=1; 
	for(;c<'0'||c>'9';c=getchar())
		if(c=='-') f=-1;
	for(;c>='0'&&c<='9';c=getchar())
		x=x*10+c-'0';
	return x*f;
}
const int N=100005,M=200005;
int n,m,a,b,x,y;
vector<int>s[N];
ll dp[N][2],p[N];
bool can[N][2];
char tp[10];
void dfs(int fa,int anc){
	dp[fa][0]=0;
	dp[fa][1]=p[fa];
	can[fa][1]=can[fa][0]=1;
	per(i,s[fa].size()-1,0){
		int son=s[fa][i];
		if(son!=anc){
			dfs(son,fa);
			if(!can[son][0]&&!can[son][1]){
				can[fa][0]=can[fa][1]=0;
				break;
			}
			if(!can[son][0]){
				dp[fa][0]+=dp[son][1];
				dp[fa][1]+=dp[son][1];
			}
			else if(!can[son][1]){
				can[fa][0]=0;
				dp[fa][1]+=dp[son][0];
			}
			else{
				dp[fa][0]+=dp[son][1];
				dp[fa][1]+=min(dp[son][0],dp[son][1]);
			}
		}
	}
	if(fa==b) can[fa][y^1]=0;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=rd(); m=rd(); scanf("%s",tp);
	rep(i,1,n) p[i]=rd();
	rep(i,1,n-1){
		int u=rd(),v=rd();
		s[u].push_back(v);
		s[v].push_back(u);
	}
	rep(i,1,m){
		a=rd(),x=rd(),b=rd(),y=rd();
		dfs(a,0);
		if(!can[a][x]) dp[a][x]=-1;
		printf("%lld\n",dp[a][x]);
	}
	return 0;
}
