#include<bits/stdc++.h>
#define N 5000
#define add(x,y) (e[++ee].nxt=lnk[x],e[lnk[x]=ee].to=y)
#define swap(x,y) (x^=y^=x^=y)
using namespace std;
int n,m,ee,cur,lnk[N+5],vis[N+5],data[N+5]; 
struct edge
{
	int to,nxt;
}e[(N<<1)+5];
class Class_TreeSolver
{
	public:
		inline void Solve(int x=1,int lst=0)
		{
			register int i,H=cur+1,T;
			for(printf("%d ",x),i=lnk[x];i;i=e[i].nxt) e[i].to^lst&&(data[++cur]=e[i].to);
			for(T=cur,sort(data+H,data+T+1),i=H;i<=T;++i) Solve(data[i],x);
		}
}TreeSolver;
inline void Begin_Circle(int,int);
class Class_CircleTreeSolver
{
	private:
		int Top,used[N+5],StackH[N+5],StackT[N+5],StackPos[N+5];
		class Class_CircleChecker
		{
			private:
				int Found,fa[N+5],In[N+5],vis[N+5],Depth[N+5];
			public:
				Class_CircleChecker() {Found=0;}
				inline void Init(int x=1,int lst=0)
				{
					register int i,y;
					for(vis[x]=1,i=lnk[x];i;i=e[i].nxt)
					{
						if(!(e[i].to^lst)) continue;
						if(vis[y=e[i].to]) 
						{
							if(Depth[x]<Depth[y]) swap(x,y);
							while(Depth[x]>Depth[y]) In[x]=1,x=fa[x];
							while(x^y) In[x]=In[y]=1,x=fa[x],y=fa[y];
							return (void)(In[x]=Found=1);
						}
						if(Depth[e[i].to]=Depth[fa[e[i].to]=x]+1,Init(e[i].to,x),Found) return;
					}
					vis[x]=0;
				}
				inline bool InCircle(int x) {return In[x];}
		}C;
		inline void dfs(int x,int lst)
		{
			if(used[x]) return;
			if(C.InCircle(x)) return Begin_Circle(x,lst);
			register int i,H=cur+1,T;
			for(used[x]=1,printf("%d ",x),i=lnk[x];i;i=e[i].nxt) !used[e[i].to]&&(data[++cur]=e[i].to);
			for(T=cur,sort(data+H,data+T+1),i=H;i<=T;++i) dfs(data[i],x);
		}
		inline void dfs_Circle(int x,int lst,int Begin,int OtherSide)
		{
			if(used[x]) return;
			register int i,H=cur+1,T;
			for(used[x]=1,printf("%d ",x),i=lnk[x];i;i=e[i].nxt) !used[e[i].to]&&(data[++cur]=e[i].to);
			for(T=cur,sort(data+H,data+T+1),i=H;i<T;++i) 
			{
				if(C.InCircle(data[i])) StackH[++Top]=i+1,StackT[Top]=T,StackPos[Top]=x,dfs_Circle(data[i],x,Begin,OtherSide),--Top;
				else dfs(data[i],x);
			}
			if(C.InCircle(data[T]))
			{
				if(OtherSide&&data[StackH[Top]]<data[i]&&!used[OtherSide]) 
				{
					while(Top>1) 
					{
						for(i=StackH[Top];i<=StackT[Top];++i) dfs(data[i],StackPos[Top]);
						--Top;
					}
					while(StackH[1]<=StackT[1]&&data[StackH[1]]<OtherSide) dfs(data[StackH[1]++],Begin);
					dfs_Circle(OtherSide,x,Begin,0);
				}
				else dfs_Circle(data[i],x,Begin,OtherSide);
			}
			else dfs(data[i],x);
		}
		inline void Begin_Circle(int x,int lst)
		{
			register int i,H=cur+1,T,s1=0,s2=0;
			for(used[x]=1,printf("%d ",x),i=lnk[x];i;i=e[i].nxt) if(!used[e[i].to]&&C.InCircle(data[++cur]=e[i].to)) s1?s2=e[i].to:s1=e[i].to;
			for(T=cur,sort(data+H,data+T+1),i=H;i<=T;++i) 
			{
				if(C.InCircle(data[i])) StackH[++Top]=i+1,StackT[Top]=T,StackPos[Top]=x,dfs_Circle(data[i],x,x,data[i]^s1?s1:s2);
				else dfs(data[i],x);
			}
		}
	public:
		inline void Solve() {C.Init(),used[0]=1,dfs(1,0);}
}CircleTreeSolver;
int main()
{
	freopen("travel.in","r",stdin),freopen("travel.out","w",stdout);
	register int i,j,k,x,y;
	for(scanf("%d%d",&n,&m),i=1;i<=m;++i) scanf("%d%d",&x,&y),add(x,y),add(y,x);
	if(m==n-1) TreeSolver.Solve();else CircleTreeSolver.Solve();
	return 0;
}
