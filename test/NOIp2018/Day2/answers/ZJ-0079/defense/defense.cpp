#include<bits/stdc++.h>
#define N 100000
#define add(x,y) (e[++ee].nxt=lnk[x],e[lnk[x]=ee].to=y)
#define swap(x,y) (x^=y^=x^=y)
#define min(x,y) ((x)<(y)?(x):(y))
using namespace std;
int n,ee,lnk[N+5],Cost[N+5];
struct edge
{
	int to,nxt;
}e[(N<<1)+5];
class Class_FIO
{
	private:
		#define Fsize 100000
		#define tc() (A==B&&(B=(A=Fin)+fread(Fin,1,Fsize,stdin),A==B)?EOF:*A++)
		#define pc(ch) (void)(FoutSize<Fsize?Fout[FoutSize++]=ch:(fwrite(Fout,1,Fsize,stdout),Fout[(FoutSize=0)++]=ch))
		int Top,FoutSize;char ch,*A,*B,Fin[Fsize],Fout[Fsize],Stack[Fsize];
	public:
		Class_FIO() {A=B=Fin;}
		inline void read(int &x) {x=0;while(!isdigit(ch=tc()));while(x=(x<<3)+(x<<1)+(ch&15),isdigit(ch=tc()));}
		inline void readc(char &x) {while(isspace(x=tc()));}
		inline void writeln(int x) {if(!x) return pc('0'),pc('\n');while(x) Stack[++Top]=x%10+48,x/=10;while(Top) pc(Stack[Top--]);pc('\n');}
		inline void writeNoAnswer() {pc('-'),pc('1'),pc('\n');}
		inline void clear() {fwrite(Fout,1,FoutSize,stdout),FoutSize=0;}
}F;
class Class_TreeDP
{
	private:
		int f[N+5][2],g[N+5];
		inline void DP(int x)
		{
			for(register int i=lnk[x];i;i=e[i].nxt) e[i].to^fa[x]&&(DP(e[i].to),f[x][0]+=f[e[i].to][1],f[x][1]+=g[e[i].to]);
			g[x]=min(f[x][0],f[x][1]);
		}
	public:
		int fa[N+5];
		inline void Init(int x=1) {for(register int i=lnk[x];i;i=e[i].nxt) e[i].to^fa[x]&&(fa[e[i].to]=x,Init(e[i].to),0);}	
		inline void Solve(int x,int sx,int y,int sy)
		{
			register int i;
			for(i=1;i<=n;++i) f[i][0]=0,f[i][1]=Cost[i];
			f[x][sx^1]=f[y][sy^1]=1e9,DP(1),F.writeln(g[1]);
		}
}T;
class Class_UpdateDP
{
	private:
		#define F5(x) (g[x]=min(f[x][0],f[x][1]))
		int f[N+5][2],g[N+5],Depth[N+5];
		inline void Del(int x) {x&&(Del(fa[x]),f[fa[x]][0]-=f[x][1],f[fa[x]][1]-=g[x],F5(fa[x]),0);}
		inline void Add(int x) {x&&(f[fa[x]][0]+=f[x][1],f[fa[x]][1]+=g[x],F5(fa[x]),Add(fa[x]),0);}
	public:
		int fa[N+5];
		inline void Init(int x=1) 
		{
			f[x][0]=0,f[x][1]=Cost[x];
			for(register int i=lnk[x];i;i=e[i].nxt) 
				e[i].to^fa[x]&&(Depth[e[i].to]=Depth[fa[e[i].to]=x]+1,Init(e[i].to),f[x][0]+=f[e[i].to][1],f[x][1]+=g[e[i].to]);
			F5(x);
		}	
		inline void Solve(int x,int sx,int y,int sy)
		{
			if(Depth[x]<Depth[y]) swap(x,y),swap(sx,sy);
			register int fx=f[x][sx^1],fy=f[y][sy^1];
			Del(x),f[x][sx^1]=1e9,F5(x),Add(x),Del(y),f[y][sy^1]=1e9,F5(y),Add(y),F.writeln(g[1]),
			Del(x),f[x][sx^1]=fx,F5(x),Add(x),Del(y),f[y][sy^1]=fy,F5(y),Add(y);
		}
}U;
class Class_LineDP
{
	private:
		int Front[N+5][2],Back[N+5][2];
	public:
		inline void Init()
		{
			register int i;
			for(i=1;i<=n;++i) Front[i][0]=Front[i-1][1],Front[i][1]=min(Front[i-1][0],Front[i-1][1])+Cost[i];
			for(i=n;i;--i) Back[i][0]=Back[i+1][1],Back[i][1]=min(Back[i+1][0],Back[i+1][1])+Cost[i];
		}
		inline void Solve(int x,int sx,int y,int sy)
		{
			if(x>y) swap(x,y),swap(sx,sy);
			F.writeln(Front[x][sx]+Back[y][sy]+(sy?min(Front[y-1][0],Front[y-1][1]):Front[y-1][1])-Front[x][sx]);
		}
};
int main()
{
	freopen("defense.in","r",stdin),freopen("defense.out","w",stdout);
	register int i,Q,x,y,sx,sy,TypeY,ans;register char TypeX;
	for(F.read(n),F.read(Q),F.readc(TypeX),F.read(TypeY),i=1;i<=n;++i) F.read(Cost[i]);
	for(i=1;i<n;++i) F.read(x),F.read(y),add(x,y),add(y,x);
	for(U.Init();Q;--Q)
	{
		F.read(x),F.read(sx),F.read(y),F.read(sy);
		if(!sx&&!sy&&(U.fa[x]==y||U.fa[y]==x)) {F.writeNoAnswer();continue;}
		U.Solve(x,sx,y,sy);
	}
	return F.clear(),0;
}
