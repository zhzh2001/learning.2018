#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
struct nob{
	int to,jump;
}a[10005];
int n,m,jump[5005],tot=1,son[5005][5005],fa[5005],deep[5005];
int f[5005][20];
void add(int x,int y){
	tot++;
	a[tot].to=y;
	a[tot].jump=jump[x];
	jump[x]=tot;
}
void Build(int pos){
	for (int i=jump[pos]; i; i=a[i].jump){
		if (a[i].to==fa[pos]) continue ;
		son[pos][0]++;
		son[pos][son[pos][0]]=a[i].to;
		fa[a[i].to]=pos;
		Build(a[i].to);
	}
}
void dfs(int pos){
	if (pos!=1) printf(" %d",pos);
	else printf("%d",pos);
	sort(son[pos]+1,son[pos]+1+son[pos][0]);
	for (int i=1; i<=son[pos][0]; i++)
		dfs(son[pos][i]);
}
int pa,pb,pp,jud[5005],ppa,ppb;
void Bfs(int pos){
	int line[5005]={0},head=0,tail=1;
	jud[1]=1;
	line[1]=1;
	while (head<tail){
		head++;
		pos=line[head];
		for (int i=jump[pos]; i; i=a[i].jump){
			if (a[i].to==f[pos][0]) continue ;
			if (!deep[a[i].to]){
				deep[a[i].to]=deep[pos]+1;
				f[a[i].to][0]=pos;
				tail++;
				line[tail]=a[i].to;
				son[pos][0]++;
				son[pos][son[pos][0]]=a[i].to;
			}
			else{
				pa=pos;
				pb=a[i].to;
				ppa=pa;
				ppb=pb;
			}
		}
		son[pos][son[pos][0]+1]=1;
	}
}
int Lca(int x,int y){
	if (deep[x]<deep[y]) swap(x,y);
	int le=deep[x]-deep[y];
	for (int i=0; i<=15; i++)
		if (le&(1<<i)) x=f[x][i];
	if (x==y) return x;
	for (int i=15; i>0; i++)
		if (f[x][i]!=f[y][i]){
			x=f[x][i];
			y=f[y][i];
		}
	pa=x;
	pb=y;
	return f[x][0];
}
int k=1,prea,preb;
void Dfs(int pos){
	if (!jud[pos]){
		if (pos==1) printf("1");
		else printf(" %d",pos);
	}
	jud[pos]=1;
	if (pos==pp){
		for (int i=1; i<=son[pos][0]; i++){
			if (son[pos][i]!=pa && son[pos][i]!=pb) Dfs(son[pos][i]);
			if (!prea && son[pos][i]==pb){
				swap(pa,pb);
				swap(ppa,ppb);
			}
			prea=pa;preb=i+1;
//			if (prea<preb){
				printf(" %d",prea);
				jud[prea]=1;
				for (int i=1; i<=son[prea][0]; i++){
					if (son[prea][i]<preb){
						if (Lca(son[prea][i],ppa)!=son[prea][i]) Dfs(son[pos][i]);
						
					}
					
				}
//			}
		}
	}
	for (int i=1; i<=son[pos][0]; i++)
		Dfs(son[pos][i]);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,x,y; i<=m; i++){
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	if (m==n-1){
		Build(1);
		dfs(1);
		return 0;
	}
	else{
		printf("1");
		for (int i=2; i<=n; i++)
			printf(" %d",i);
	}
	return 0;
}
