#include <cstdio>
#include <cmath>
#include <cstring>
#include <algorithm>
using namespace std;

typedef long long ll;
const int MAXN = 1e5 + 10;
const ll INF = 1e14;
int n, m, sum;
char ch, c1;
int last[MAXN];
ll v[MAXN];
struct Edge {
	int link, to;
} edge[MAXN << 1];

inline void read1(int &x) {
	char ch = getchar(); x = 0;
	for (; ch < 48 || ch > 57; ch = getchar());
	for (; ch >= 48 && ch <= 57; ch = getchar()) x = (x << 3) + (x << 1) + (ch ^ 48);
	return;
}

inline void read2(ll &x) {
	char ch = getchar(); x = 0;
	for (; ch < 48 || ch > 57; ch = getchar());
	for (; ch >= 48 && ch <= 57; ch = getchar()) x = (x << 3) + (x << 1) + (ch ^ 48);
	return;
}

inline void addedge(int u, int v) {
	edge[++sum] = (Edge) { v, last[u] }, last[u] = sum;
	edge[++sum] = (Edge) { u, last[v] }, last[v] = sum;
}

namespace DP {
	int N[MAXN], M[MAXN];
	ll dp[MAXN][2];
	
	void dfs(int x, int fa) {
		ll L0 = 0, L1 = 0;
		for (register int i = last[x]; i; i = edge[i].to) {
			int y = edge[i].link;
			if (y == fa) continue; dfs(y, x);
			L0 += dp[y][1], L1 += min(dp[y][0], dp[y][1]);
		}
		if (!L0 && !L1) {
			if (N[x]) {
				dp[x][0] = 0, dp[x][1] = INF;
			} else if (M[x]) {
				dp[x][0] = INF, dp[x][1] = v[x];
			} else {
				dp[x][0] = 0, dp[x][1] = v[x];
			}
		} else {
			if (N[x]) {
				dp[x][0] = L0, dp[x][1] = INF;
			} else if (M[x]) {
				dp[x][0] = INF, dp[x][1] = v[x] + L1;
			} else {
				dp[x][0] = L0, dp[x][1] = v[x] + L1;
			}
		}
	}
	
	inline void doit() {
		while (m--) {
			int a, b, x, y; read1(a), read1(x), read1(b), read1(y);
			if (x == 0) N[a] = 1;
			else M[a] = 1;
			if (y == 0) N[b] = 1;
			else M[b] = 1;
			dfs(1, 0);
			ll ans = min(dp[1][0], dp[1][1]);
			if (ans >= INF) puts("-1");
			else printf("%lld\n", ans);
			N[a] = N[b] = M[a] = M[b] = 0;
		}
	}
};

int main() {
	freopen("defense.in", "r", stdin); freopen("defense.out", "w", stdout);
	read1(n), read1(m), ch = getchar();
	for (; ch < 'A' || ch > 'C'; ch = getchar());
	c1 = getchar();
	for (register int i = 1; i <= n; ++i) read2(v[i]);
	for (register int i = 1; i < n; ++i) {
		int u, v; read1(u), read1(v);
		addedge(u, v);
	}
	if (n <= 2000 && m <= 2000) DP::doit();
	fclose(stdin); fclose(stdout);
	return 0;
}
