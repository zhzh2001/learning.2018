#include <cstdio>
#include <cmath>
#include <cstring>
#include <algorithm>
using namespace std;

const int MAXN = 5010, MAXM = 5010;
int n, m, sum, ff;
int last[MAXN], in[MAXN];
struct Edge {
	int link, to;
} edge[MAXM << 1];

inline void read(int &x) {
	char ch = getchar(); x = 0;
	for (; ch < 48 || ch > 57; ch = getchar());
	for (; ch >= 48 && ch <= 57; ch = getchar()) x = (x << 3) + (x << 1) + (ch ^ 48);
	return;
}

inline void addedge(int u, int v) {
	edge[++sum] = (Edge) { v, last[u] }, last[u] = sum;
	edge[++sum] = (Edge) { u, last[v] }, last[v] = sum;
}

namespace tree {
	inline void dfs(int x, int fa) {
		printf("%d ", x);
		int b[in[x] + 5], cnt = 0;
		for (int i = last[x]; i; i = edge[i].to) b[++cnt] = edge[i].link;
		sort(b + 1, b + cnt + 1);
		for (register int i = 1; i <= cnt; ++i)
			if (b[i] != fa) dfs(b[i], x);
		return;
	}
	
	inline void doit() {
		dfs(1, 0);
		puts("");
		return;
	}
};

namespace more {
	int flag, cnt, cx, cy;
	int f[MAXN], b[MAXN], vis[MAXN];
	
	void dfs(int x, int fa) {
		f[x] = fa, vis[x] = 1;
		for (register int i = last[x]; i; i = edge[i].to) {
			int y = edge[i].link;
			if (y == fa) continue;
			if (vis[y]) {
				b[++cnt] = y;
				while (x != y) b[++cnt] = x, x = f[x];
				flag = 1;
			} else dfs(y, x);
			if (flag) return;
		}
		return;
	}
	
	inline void dfs1(int x, int fa) {
		printf("%d ", x);
		int b[in[x] + 5], cnt = 0;
		for (int i = last[x]; i; i = edge[i].to) b[++cnt] = edge[i].link;
		sort(b + 1, b + cnt + 1);
		for (register int i = 1; i <= cnt; ++i)
			if (b[i] != fa && ((x != cx || b[i] != cy) && (x != cy || b[i] != cx))) dfs1(b[i], x);
		return;
	}
	
	inline void doit() {
		flag = 0;
		for (register int i = 1; i <= n; ++i) vis[i] = 0;
		dfs(1, 0);
		int f1 = 0, f2 = 0, p = 2, q = cnt;
		while (p < q && (!f1 || !f2)) {
			if (b[p] < b[q]) ++p, f1 = 1;
			else --q, f2 = 1;
		}
		if (cnt == 2) dfs1(1, 0);
		else {
			if (f1 && f2) {
				if (b[p - 1] < b[q + 1]) cx = b[p - 1], cy = b[p];
				else cx = b[q], cy = b[q + 1];
			} else if (f1) {
				cx = b[1], cy = b[cnt];
			} else {
				cx = b[1], cy = b[2];
			}
			dfs1(1, 0);
		}
		puts("");
		return;
	}
};

int main() {
	freopen("travel.in", "r", stdin); freopen("travel.out", "w", stdout);
	read(n), read(m);
	for (register int i = 1; i <= m; ++i) {
		int u, v; read(u), read(v);
		addedge(u, v);
		++in[u], ++in[v];
	}
	ff = 1;
	for (register int i = 1; i <= n; ++i)
		if (in[i] > 2) ff = 0;
	if (m == n - 1) tree::doit();
	else more::doit();
	fclose(stdin); fclose(stdout);
	return 0;
}
