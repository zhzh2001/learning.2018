#include<iostream>
#include<cstdio>
#define ls (o<<1)
#define rs ((o<<1)|1)
using namespace std;
struct TREE{
	int to,nxt;
}t[505000];
int last[505000],cnt;long long p[100500],f[100500][2],g[100500][2];
string type;
void build(int u,int v){
	t[++cnt].to=v;
	t[cnt].nxt=last[u];
	last[u]=cnt;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int i,j,k,m,n,u,v,a,b,x,y;
	cin>>n>>m>>type;
	for(i=1;i<=n;i++){
		scanf("%lld",&p[i]);
	} 
	for(i=1;i<=n-1;i++){
		scanf("%d%d",&u,&v);
		build(u,v);build(v,u);
	}
	if(type[0]=='A'){
		f[1][1]=p[1];f[1][0]=1e18;
		for(i=2;i<=n;i++){
			f[i][0]=f[i-1][1];
			f[i][1]=min(f[i-1][1],f[i-1][0])+p[i];
		}
		for(j=1;j<=m;j++){
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(a>b){
				int tmp=a;a=b;b=tmp;
				tmp=x;x=y;y=tmp;
			}
			else if(a==b){
				if(x!=y){
					printf("-1\n");
					continue;
				}
			}
			g[a][x]=f[a][x];
			g[a][1-x]=1e18;
			for(i=a+1;i<=b;i++){
				g[i][0]=g[i-1][1];
				g[i][1]=min(g[i-1][1],g[i-1][0])+p[i];
			}
			g[b][y]=g[b][y];
			g[b][1-y]=1e18;
			for(i=b+1;i<=n;i++){
				g[i][0]=g[i-1][1];
				g[i][1]=min(g[i-1][1],g[i-1][0])+p[i];
			}
			long long ans=min(g[n][0],g[n][1]);
			if(ans<1e17) printf("%lld\n",ans);
			else printf("-1\n");
		}
		fclose(stdin);fclose(stdout);
		return 0;
	}
	for(i=1;i<=m;i++){
		printf("-1\n");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
