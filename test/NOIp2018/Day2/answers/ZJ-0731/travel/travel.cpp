#include<iostream>
#include<cstdio>
#include<vector>
#include<queue>
#include<stack>
using namespace std;
struct TU{
	int to,nxt;bool use;
}t[20005];
int last[5005],f[5005],cnt;bool vis[5005],apr[5005];
void build(int u,int v){
	t[++cnt].to=v;
	t[cnt].nxt=last[u];
	t[cnt].use=1;
	last[u]=cnt;
}
stack<int> st;
int p[20005],lt=0;
void dfs(int x){
	p[++lt]=x;
	priority_queue<int> q;
	int e=last[x];
	while(e){
		if(t[e].use==0){
			e=t[e].nxt;
			continue;
		}
		int v=t[e].to;
		if(!vis[v]){
			vis[v]=1;
			q.push(-v);
		}
		e=t[e].nxt;
	}
	while(!q.empty()){
		int u=-q.top();q.pop();
		dfs(u);
	}
}
bool fic(int fa,int x,int dd){
	if(x==dd){
		int mmd=0;
		while(!st.empty()){
			mmd=max(mmd,st.top());
			st.pop();
		}
		int e=last[mmd];
		int qe=0,xd=1e9;
		while(e){
			if(xd>t[e].to){
				xd=t[e].to;
				qe=(e+1)/2;
			}
			e=t[e].nxt;
		}
		t[qe*2-1].use=t[qe*2].use=0;
		return 1;
	}
	int e=last[x];
	while(e){
		int v=t[e].to;
		if(v!=fa){
			st.push(v);
			if(fic(x,v,dd)) return 1;
			st.pop();
		}
		e=t[e].nxt;
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int i,j,k,m,n,u,v;
	scanf("%d%d",&n,&m);
	for(i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		build(u,v);build(v,u);
		if(apr[u]&&apr[v]){
			fic(v,u,v);
		}
		apr[u]=apr[v]=1;
		
	}
	vis[1]=1;
	dfs(1);
	for(i=1;i<=n;i++){
		printf("%d ",p[i]);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
