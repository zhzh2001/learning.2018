#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
typedef long long LL;
const int maxn=8,maxm=1000000,maxp=1000,maxl=1000,MOD=1e9+7;

int n,m,fac[maxm+5],INV[maxm+5],pw[maxm+5],ans;
int pic[maxn+5][maxm+5],len,tot,now[maxl+5];
struct data {int a[maxl+5];} s[maxp+5];

#define Eoln(x) ((x)==10||(x)==13||(x)==EOF)
inline char readc(){
	static char buf[100000],*l=buf,*r=buf;
	return (l==r&&(r=(l=buf)+fread(buf,1,100000,stdin),l==r))?EOF:*l++;
}
template<typename T> inline int readi(T &x){
	T tot=0;char ch=readc(),lst='+';
	while (!isdigit(ch)) {if (ch==EOF) return EOF;lst=ch;ch=readc();}
	while (isdigit(ch)) tot=(tot<<3)+(tot<<1)+(ch^48),ch=readc();
	return (lst=='-'?x=-tot:x=tot),Eoln(ch);
}
inline void Make(){
	fac[0]=INV[0]=INV[1]=1;for (int i=2;i<=m;i++) INV[i]=(LL)(MOD-MOD/i)*INV[MOD%i]%MOD;
	for (int i=1;i<=m;i++) fac[i]=(LL)fac[i-1]*i%MOD,INV[i]=(LL)INV[i-1]*INV[i]%MOD;
	pw[0]=1;for (int i=1;i<=m;i++) pw[i]=((LL)pw[i-1]<<1)%MOD;
}
#define C(x,y) ((LL)fac[x]*INV[y]%MOD*INV[(x)-(y)]%MOD)
inline void AMOD(int &x,int y) {if ((x+=y)>=MOD) x-=MOD;}
void getp(int x,int y,int L=1){
	if (x==n&&y==m) {tot++;for (int i=1;i<=len;i++) s[tot].a[i]=now[i];return;}
	if (x<n) now[L]=0,getp(x+1,y,L+1);if (y<m) now[L]=1,getp(x,y+1,L+1);
}
inline bool cmp(const data &a,const data &b){
	for (int i=1;i<=len;i++)
		if (a.a[i]<b.a[i]) return true;
		else if (a.a[i]>b.a[i]) return false;
	return false;
}
bool Travel(int i,int j,int A,int B,int C,int D,int L=1){
	if (L>len) return true;if (pic[A][B]>pic[C][D]) return false;if (pic[A][B]<pic[C][D]) return true;
	if (s[i].a[L]==0&&s[j].a[L]==0) return Travel(i,j,A+1,B,C+1,D,L+1);
	if (s[i].a[L]==0&&s[j].a[L]==1) return Travel(i,j,A+1,B,C,D+1,L+1);
	if (s[i].a[L]==1&&s[j].a[L]==0) return Travel(i,j,A,B+1,C+1,D,L+1);
	if (s[i].a[L]==1&&s[j].a[L]==1) return Travel(i,j,A,B+1,C,D+1,L+1);
}
inline bool check(){
	for (int i=1;i<tot;i++)
		for (int j=i+1;j<=tot;j++)
			if (!Travel(j,i,1,1,1,1)) return false;
	return true;
}
void Dfs(int x,int y){
	if (x>n) {ans+=check();return;}
	pic[x][y]=0;Dfs(x+(y==m?1:0),y==m?1:y+1);
	pic[x][y]=1;Dfs(x+(y==m?1:0),y==m?1:y+1);
}
int main(){
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	readi(n);readi(m);Make();len=n+m-2;
	if (n==2){
		for (int i=0;i<m;i++) AMOD(ans,C(m-1,i)*pw[i]%MOD);
		ans=((LL)ans<<2)%MOD;printf("%d\n",ans);
	} else getp(1,1),sort(s+1,s+1+tot,cmp),Dfs(1,1),printf("%d\n",ans);
	return 0;
}
