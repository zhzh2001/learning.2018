#include<cstdio>
#include<cctype>
using namespace std;
typedef long long LL;
const int maxn=100000;const LL INF=9e18;

int n,m,a[maxn+5],ti,vis[maxn+5];LL f[maxn+5][2],g[maxn+5][2],h[maxn+5][2];
int E,lnk[maxn+5],son[(maxn<<1)+5],nxt[(maxn<<1)+5],fa[maxn+5];

#define Eoln(x) ((x)==10||(x)==13||(x)==EOF)
inline char readc(){
	static char buf[100000],*l=buf,*r=buf;
	return (l==r&&(r=(l=buf)+fread(buf,1,100000,stdin),l==r))?EOF:*l++;
}
template<typename T> inline int readi(T &x){
	T tot=0;char ch=readc(),lst='+';
	while (!isdigit(ch)) {if (ch==EOF) return EOF;lst=ch;ch=readc();}
	while (isdigit(ch)) tot=(tot<<3)+(tot<<1)+(ch^48),ch=readc();
	return (lst=='-'?x=-tot:x=tot),Eoln(ch);
}
#define Add(x,y) (son[++E]=(y),nxt[E]=lnk[x],lnk[x]=E)
#define min(x,y) ((x)<(y)?(x):(y))
void DP(int x,int pre=0){
	fa[x]=pre;f[x][1]+=a[x];
	for (int j=lnk[x];j;j=nxt[j])
		if (son[j]!=pre){
			DP(son[j],x);f[x][0]+=f[son[j]][1];
			f[x][1]+=min(f[son[j]][0],f[son[j]][1]);
		}
}
void UpdateA(int x){
	vis[x]=ti;if (x==1) return;g[fa[x]][0]=f[fa[x]][0];g[fa[x]][1]=f[fa[x]][1];
	if (g[x][1]==INF) g[fa[x]][0]=INF; else g[fa[x]][0]+=g[x][1]-f[x][1];
	if (min(g[x][0],g[x][1])==INF) g[fa[x]][1]=INF; else g[fa[x]][1]+=min(g[x][0],g[x][1])-min(f[x][0],f[x][1]);
	UpdateA(fa[x]);
}
#define G(x,y) (vis[x]<ti?f[x][y]:g[x][y])
void UpdateB(int x){
	if (x==1) return;h[fa[x]][0]=G(fa[x],0);h[fa[x]][1]=G(fa[x],1);
	if (h[x][1]==INF) h[fa[x]][0]=INF; else h[fa[x]][0]+=h[x][1]-G(x,1);
	if (min(h[x][0],h[x][1])==INF) h[fa[x]][1]=INF; else h[fa[x]][1]+=min(h[x][0],h[x][1])-min(G(x,0),G(x,1));
	UpdateB(fa[x]);
}
int main(){
	freopen("defense.in","r",stdin);freopen("defense.out","w",stdout);
	readi(n);readi(m);int orz;readi(orz);for (int i=1;i<=n;i++) readi(a[i]);
	for (int i=1,x,y;i<n;i++) readi(x),readi(y),Add(x,y),Add(y,x);DP(1);
	for (int i=1;i<=m;i++){
		int x,a,y,b;readi(x);readi(a);readi(y);readi(b);ti++;
		g[x][a]=f[x][a];g[x][a^1]=INF;UpdateA(x);
	    h[y][b]=G(y,b);h[y][b^1]=INF;UpdateB(y);
		LL ans=min(h[1][0],h[1][1]);ans<INF?printf("%lld\n",ans):puts("-1");
	}return 0;
}
