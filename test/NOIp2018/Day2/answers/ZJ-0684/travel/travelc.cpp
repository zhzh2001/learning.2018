#include <algorithm>
#include <cstdio>
#include <iostream>
#include <vector>
#include <queue>
using namespace std;
#define ll long long
int n,m;
vector <int> lt[6000];
bool vd[6000];
int ro[6000];
int cnt;
int dfs(int o){
	vd[o] = 1;
	ro[++cnt] = o;
	priority_queue <int,vector <int> , greater<int> > q;
	while (!q.empty()) q.pop();
	for (int i = 0; i < lt[o].size(); i++)
	if (!vd[lt[o][i]]) q.push(lt[o][i]);
	while (!q.empty()){
		int v = q.top(); q.pop();
		dfs(v);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i = 1; i <= m; i++){
		int x,y;
		scanf("%d%d",&x,&y);
		lt[x].push_back(y);
		lt[y].push_back(x);
	}
	if (n == m+1){
		for (int i = 1; i <= n; i++) vd[i] = 0;
		dfs(1);
		for (int i = 1; i <= n; i++) printf("%d ",ro[i]);
		printf("\n");
	}
}
