#include <algorithm>
#include <cstdio>
#include <iostream>
#include <vector>
#include <queue>
#include <stack>
using namespace std;
#define ll long long
int n,m;
vector <int> lt[6000];
bool vd[6000];
int ro[6000];
int bstro[6000];
int inr[6000];
int dfn[6000];
int lw[6000];
int istk[6000];
int bv,bu;
int cnt;
struct edge{
	int u,v;
	edge(){
	}
	edge(int u , int v){
		this->u = u;
		this->v = v;
	}
}e[6000];
stack <int> s;
int ctt = 0;
int dfs(int o){
	vd[o] = 1;
	ro[++cnt] = o;
	priority_queue <int,vector <int> , greater<int> > q;
	while (!q.empty()) q.pop();
	for (int i = 0; i < lt[o].size(); i++)
	if (!vd[lt[o][i]]) q.push(lt[o][i]);
	while (!q.empty()){
		int v = q.top(); q.pop();
		dfs(v);
	}
}
int dfs2(int o){
	vd[o] = 1;
	ro[++cnt] = o;
	priority_queue <int,vector <int> , greater<int> > q;
	while (!q.empty()) q.pop();
	if (o == bv){
		for (int i = 0; i < lt[o].size(); i++)
		if ((!vd[lt[o][i]])&&(lt[o][i] != bu)) q.push(lt[o][i]);
	}
	else if (o == bu){
		for (int i = 0; i < lt[o].size(); i++)
		if ((!vd[lt[o][i]])&&(lt[o][i] != bv)) q.push(lt[o][i]);
	}
	else
	for (int i = 0; i < lt[o].size(); i++)
	if (!vd[lt[o][i]]) q.push(lt[o][i]);
	while (!q.empty()){
		int v = q.top(); q.pop();
		dfs2(v);
	}
}
void tarjan(int o,int fa){
	dfn[o] = ++ctt;
	lw[o] = dfn[o];
	istk[o] = 1;
	s.push(o);
	for (int i = 0; i < lt[o].size(); i++)
	if (!dfn[lt[o][i]]){
		tarjan(lt[o][i],o);
		lw[o] = min(lw[o],lw[lt[o][i]]);
	}
	else if (istk[lt[o][i]] && (lt[o][i] != fa))
	lw[o] = min(lw[o],dfn[lt[o][i]]);
//	cout<<dfn[o]<<" "<<lw[o]<<endl;
	if (lw[o] == dfn[o]){
		if (s.top() == o){
			s.pop();
			istk[o] = 0;
			return;
		}
		while (s.top() != o){
			inr[s.top()] = 1;
			istk[s.top()] = 0;
			s.pop();
		}
		inr[s.top()] = 1;
		istk[s.top()] = 0;
		s.pop();
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	int tttr = 0;
	for (int i = 1; i <= m; i++){
		int x,y;
		scanf("%d%d",&x,&y);
		lt[x].push_back(y);
		lt[y].push_back(x);
		e[++tttr] = edge(x,y);
	}
	if (n == m+1){
		cnt = 0;
		for (int i = 1; i <= n; i++) vd[i] = 0;
		dfs(1);
		for (int i = 1; i <= n; i++) printf("%d ",ro[i]);
		printf("\n");
	}
	else{
		for (int i = 1; i <= n; i++) bstro[i] = n;
		tarjan(1,0);
		for (int i = 1; i <= n; i++)
		if (inr[e[i].u]&&inr[e[i].v]){
			cnt = 0;
			for (int j = 1; j <= n; j++) ro[j] = 0;
			for (int j = 1; j <= n; j++) vd[j] = 0;
			bv = e[i].v; bu = e[i].u;
			dfs2(1);
			bool flag = 1;
			for (int j = 1; j <= n; j++)
			if (ro[j] < bstro[j]) break;
			else if (ro[j] > bstro[j]){
				flag = false;
				break;
			}
			if (flag){
				for (int j = 1; j <= n; j++) bstro[j] = ro[j];
			}
		}
		for (int j = 1; j <= n; j++) printf("%d ",bstro[j]);
		printf("\n");
	}
}
