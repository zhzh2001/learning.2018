#include <cstdio>
#include <cstring>
#include <algorithm>
using std::swap;

typedef long long ll;
const int kMod = 1e9 + 7;
const int kMaxN = 1e6 + 5;

inline ll pow(ll x, int n) {
  ll ret = 1;
  while (n) {
    if (n & 1) ret = ret * x % kMod;
    x = x * x % kMod;
    n >>= 1;
  }
  return ret;
}

int n, m;
ll f[kMaxN][4], ans;

namespace Small {
  int ans;
  int num[1 << 5];
  bool matrix[5][5];
  void DFS(int i, int j, int id, int tmp) {
    tmp <<= 1; tmp += matrix[i][j];
    if (i == n && j == m) {
      num[id] = tmp;
      return;
    }
    if (i + 1 <= n)
      DFS(i + 1, j, (id << 1) + 1, tmp);
    if (j + 1 <= m)
      DFS(i, j + 1, id << 1, tmp);
  }
  void Check() {
    memset(num, -1, sizeof num);
    DFS(1, 1, 0, 0);
    for (int i = (1 << 5) - 1; i; i--) {
      for (int j = (1 << 5) - 1; j > i; j--) {
        if (num[i] != -1 && num[j] != -1 && num[i] < num[j])
          return;
      }
    }
    ans++;
  }
  void Make(int i, int j) {
    if (i > n) {
      Check();
      return;
    }
    int ni, nj;
    if (j == m)
      ni = i + 1, nj = 1;
    else
      ni = i, nj = j + 1;
    matrix[i][j] = 0;
    Make(ni, nj);
    matrix[i][j] = 1;
    Make(ni, nj);
  }
  void Solve() {
    Make(1, 1);
    printf("%d\n", ans);
  }
}

int main() {
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);
  scanf("%d%d", &n, &m);
  if (n <= 3 && m <= 3) {
    Small::Solve();
  } else {
    if (n > m)
      swap(n, m);
    if (n == 1) {
      ans = pow(2, m);
    } else if (n == 2) {
      ans = 4ll * pow(3, m - 1) % kMod;
    } else if (n == 3) {
      f[3][0] = 2; f[3][1] = 4;
      f[3][2] = 6; f[3][3] = 12;
      for (int i = 4; i <= m; i++) {
        for (int j = 0; j < 4; j++) {
          if (j & 1) {
            (f[i][(j >> 1) + 2] += f[i - 1][j] * 3) %= kMod;
          } else {
            (f[i][(j >> 1)] += f[i - 1][j]) %= kMod;
            (f[i][(j >> 1) + 2] += f[i - 1][j] * 3) %= kMod;          
          }
        }
      }
      ans = (f[m][1] + f[m][3]) * 4 % kMod + (f[m][0] + f[m][2]) * 6 % kMod;
      ans %= kMod;
    }
    printf("%lld\n", ans % kMod);
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}
