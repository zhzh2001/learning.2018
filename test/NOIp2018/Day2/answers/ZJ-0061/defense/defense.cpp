#include <cstdio>
#include <vector>
#include <cassert>
#include <cstring>
using std::vector;
typedef vector<int> vec;

// long long

inline int min(const int lhs, const int rhs) {
//  assert(lhs == -1 && rhs == -1);
  if (lhs == -1)
    return rhs;
  if (rhs == -1)
    return lhs;
  return lhs < rhs ? lhs : rhs;
}

const int kMaxN = 100000 + 5;

int n, m;
int p[kMaxN];
vec dst[kMaxN];
int f[kMaxN][2];

int fa[kMaxN];
void GetFa(const int u) {
  for (vec::iterator it = dst[u].begin(); it != dst[u].end(); it++) {
    if (*it == fa[u])
      continue;
    fa[*it] = u;
    GetFa(*it);
  }
}

int a, b, la, lb, x, y;
bool change[kMaxN];
void Change(int x) {
  while (fa[x] != x) {
    change[x] = 1;
    x = fa[x];
  }
}
bool Calc(const int u) {
  for (vec::iterator it = dst[u].begin(); it != dst[u].end(); it++) {
    if (*it == fa[u] || !change[*it])
      continue;
    if (Calc(*it))
      return 1;
  }
  f[u][0] = 0;
  f[u][1] = p[u];
  for (vec::iterator it = dst[u].begin(); it != dst[u].end(); it++) {
    if (*it == fa[u])
      continue;
    if (f[u][0] != -1) {
      if (f[*it][1] == -1)
        f[u][0] = -1;
      else
        f[u][0] += f[*it][1];
    }
    f[u][1] += min(f[*it][0], f[*it][1]);
  }  
  if (u == a)
    f[u][x ^ 1] = -1;
  else if (u == b)
    f[u][y ^ 1] = -1;
  if (f[u][0] == -1 && f[u][1] == -1)
    return 1;
  return 0;
}

char rubbish[100];

int main() {
  freopen("defense.in", "r", stdin);
  freopen("defense.out", "w", stdout);
  scanf("%d%d%s", &n, &m, rubbish);
  for (int i = 1; i <= n; i++)
    scanf("%d", &p[i]);
  for (int i = 1, u, v; i < n; i++) {
    scanf("%d%d", &u, &v);
    dst[u].push_back(v);
    dst[v].push_back(u);
  }
  
  GetFa(1);
  for (int i = 1; i <= n; i++)
    change[i] = 1;
  Calc(1);
  
  for (int i = 1; i <= m; i++) {
    la = a; lb = b;
    scanf("%d%d%d%d", &a, &x, &b, &y);
    memset(change, 0, sizeof change);
    Change(la);
    Change(lb);
    Change(a);
    Change(b);
    if (Calc(1))
      printf("-1\n");
    else
      printf("%d\n", min(f[1][0], f[1][1]));
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}
