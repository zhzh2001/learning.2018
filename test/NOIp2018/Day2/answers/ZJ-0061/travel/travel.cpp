#include <set>
#include <queue>
#include <cstdio>
#include <vector>
#include <cstring>
using std::set;
using std::queue;
using std::vector;

typedef set<int> se;
typedef vector<int> vec;
const int kMaxN = 5000 + 5;

vec ans, tmp;
int n, m;
int u[kMaxN], v[kMaxN];
se dst[kMaxN];

namespace Tree {
  void DFS(const int u, const int fa, vec &tmp) {
    tmp.push_back(u);
    for (se::iterator it = dst[u].begin(); it != dst[u].end(); it++) {
      if (*it != fa)
        DFS(*it, u, tmp);
    }
  }
  void Solve(vec &tmp) {
    DFS(1, 0, tmp);
  }
}

namespace Circle {
  queue<int> q;
  int degree[kMaxN];
  bool out_circle[kMaxN];
  bool cmp(vec &a, vec &b) {
    for (int i = 0; i < n; i++) {
      if (a[i] > b[i])
        return 1;
      if (a[i] < b[i])
        return 0;
    }
    return 0;
  }
  void Solve() {
    for (int i = 1; i <= n; i++) {
      degree[i] = dst[i].size();
      if (degree[i] == 1) {
        q.push(i);
        out_circle[i] = 1;
      }
    }
    while (!q.empty()) {
      const int now = q.front(); q.pop();
      for (se::iterator it = dst[now].begin(); it != dst[now].end(); it++) {
        if (!out_circle[*it]) {
          degree[*it]--;
          if (degree[*it] == 1) {
            q.push(*it);
            out_circle[*it] = 1;
          }
        }
      }
    }
    ans.push_back(n + 1);
    for (int i = 1; i <= m; i++) {
      if (out_circle[u[i]] || out_circle[v[i]])
        continue;
      tmp.clear();
      dst[u[i]].erase(v[i]);
      dst[v[i]].erase(u[i]);
      Tree::Solve(tmp);
      dst[u[i]].insert(v[i]);
      dst[v[i]].insert(u[i]);
      if (cmp(ans, tmp)) {
        ans.clear();
        for (int i = 0; i < n; i++)
          ans.push_back(tmp[i]);
      }
    }
  }
}

int main() {
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= m; i++) {
    scanf("%d%d", &u[i], &v[i]);
    dst[u[i]].insert(v[i]);
    dst[v[i]].insert(u[i]);
  }
  if (m == n - 1)
    Tree::Solve(ans);
  else
    Circle::Solve();
  for (int i = 0; i < n - 1; i++)
    printf("%d ", ans[i]);
  printf("%d\n", ans[n - 1]);
  fclose(stdin);
  fclose(stdout);
  return 0;
}
