#include <cstdio>
#include <cstring>
#include <algorithm>
const int MAXN = 1e5 + 10;
typedef long long LL;
const LL INF = 1e12;

struct Edge {
  int v, next;
}edge[MAXN << 1];
int head[MAXN], tail, n, m;
char typ[5];
int p[MAXN];
void insert(int u, int v) {
  edge[++tail] = (Edge) {v, head[u]}; head[u] = tail;
}
namespace solver1 {

  int a, x, b, y;
  LL f[MAXN][2];
  void dfs(int u, int fa) {
    f[u][0] = 0;
    f[u][1] = p[u];
    
    for (int i = head[u]; i; i = edge[i].next) {
      int v = edge[i].v;
      if (v == fa) continue;
      dfs(v, u);
      f[u][0] += f[v][1];
      f[u][1] += std::min(f[v][0], f[v][1]);
    }

    if (a == u) {
      f[u][x ^ 1] = INF;
    }
    if (b == u) {
      f[u][y ^ 1] = INF;
    }
       
    
  }
  void main() {
    for (int i = 1; i <= m; i++) {
      scanf("%d%d%d%d", &a, &x, &b, &y);
      
      dfs(1, 0);
      long long ans = std::min(f[1][0], f[1][1]);
      if (ans >= INF) {
        puts("-1");
      } else {
        printf("%lld\n", ans);
      }
    }
  }
}
namespace solver2 {
  LL f[MAXN][2];
  void dfs(int u, int fa) {
    f[u][0] = 0;
    f[u][1] = p[u];
    
    for (int i = head[u]; i; i = edge[i].next) {
      int v = edge[i].v;
      if (v == fa) continue;
      dfs(v, u);
      f[u][0] += f[v][1];
      f[u][1] += std::min(f[v][0], f[v][1]);
    }
  }
  LL rhs[MAXN][2];
  int pa[MAXN];
  void dfs2(int u, int fa) {
    pa[u] = fa;
    for (int i = head[u]; i; i = edge[i].next) {
      int v = edge[i].v;
      if (v == fa) continue;
      rhs[v][0] = f[u][0] - f[v][1];
      rhs[v][1] = f[u][1] - std::min(f[v][0], f[v][1]);
      dfs2(v, u);
    }
  }
  void main() {
    dfs(1, 0);
    dfs2(1, 0);
    for (int i = 1; i <= m; i++) {
      int a, x, b, y;
      scanf("%d%d%d%d", &a, &x, &b, &y);
      LL g[2];
      g[0] = f[b][0];
      g[1] = f[b][1];
      g[y ^ 1] = INF;
      int cur = b;
      while(cur != 1) {
        LL t = std::min(g[0], g[1]);
        g[0] = g[1] + rhs[cur][0];
        g[1] = t + rhs[cur][1];
        cur = pa[cur];
      }
      printf("%lld\n", g[1] >= INF ? -1 : g[1]);
    }
  }
}
int main() {
#ifndef LOCAL
  freopen("defense.in", "r", stdin);
  freopen("defense.out", "w", stdout);
#endif
  scanf("%d%d%s", &n, &m, typ);
  for (int i = 1; i <= n; i++) {
    scanf("%d", p + i);
  }

  for (int i = 1; i < n; i++) {
    int u, v;
    scanf("%d%d", &u, &v);
    insert(u, v);
    insert(v, u);
  }

  
  if (n <= 2000 && m <= 2000) {
    solver1::main();
  } else if (typ[0] == 'B') {
    solver2::main();
  }
}
