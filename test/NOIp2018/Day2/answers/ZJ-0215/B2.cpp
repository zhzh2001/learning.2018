#include <cstdio>
#include <cstring>
#include <algorithm>
const int MOD = 1e9 + 7;


int n, m;
namespace {
  inline int add(int x) { return x >= MOD ? x - MOD : x;}
  inline int sub(int x) { return x < 0 ? x + MOD : x; }
  inline int mul(int x, int y) { return 1ll * x * y % MOD; }
}
namespace solver1 {
  void main() {
    if (n == 1) {
      int ans = 1;
      for (int j = 1; j <= m; j++) ans = mul(ans, 2);
      printf("%d\n", ans);
      return;
    }
    
    if (n == 2) {
      int ans = 4;
      for (int i = 1; i < m; i++) ans = mul(ans, 3);
      printf("%d\n", ans);
      return;
    }

    if (n == 3) {
      int ans = 112;
      for (int i = 1; i <= m - 3; i++) ans = mul(ans, 3);
      printf("%d\n", ans);
    }

  }
}
namespace solver2 {
  int a[10][10];
  int ans;
  int last;
  bool dfs(int x, int y, int val) {
    if (x == n && y == m) {
      bool ans = val >= last;
      last = val;
      return ans;
    }
    if (x + 1 <= n) if (!dfs(x + 1, y, val << 1 | a[x][y])) return 0;
    if (y + 1 <= m) if (!dfs(x, y + 1, val << 1 | a[x][y])) return 0;
    return 1;
  }
  bool check() {
    last = -1;
    return dfs(1, 1, 0);
  }
  void dfs(int x) {
    if (x == n + m) {

      ans += check() * 4;
      return;
    }
    for (int i = 1; i <= n; i++) {
      int j = x - i;
      if (j >= 1 && j <= m) {
        a[i][j] = 0;
      }
    }
    dfs(x + 1);

    for (int i = 1; i <= n; i++) {
      int j = x - i;
      if (j >= 1 && j <= m) {
        a[i][j] = 1;
        dfs(x + 1);
      }
    }

  }
  void main() {
    dfs(3);
    printf("%d\n", ans);
  }
}
namespace solver3 {
  const int ans4[] = {912, 2688, 8064, 24192, 72576};
  const int ans5[] = {7136, 21312, 63936, 191808};
  const int ans6[] = {56768, 170112, 510336};
  const int ans7[] = {453504, 1360128};
  const int ans8[] = {666};
  void main() {

    if (n == 4) {
      if (m <= 8) printf("%d\n", ans4[m - 4]);
      else {
        int ans = ans4[8 - 4];
        for (int i = 1; i <= m - 8; i++) ans = mul(ans, 3);
        printf("%d\n", ans);
      }
    }
    if (n == 5) {
      if (m <= 8) printf("%d\n", ans5[m - 5]);
      else {
        int ans = ans5[8 - 5];
        for (int i = 1; i <= m - 8; i++) ans = mul(ans, 3);
        printf("%d\n", ans);
      }

    }
    if (n == 6) {
      if (m <= 8) printf("%d\n", ans6[m - 6]);
      else {
        int ans = ans6[8 - 6];
        for (int i = 1; i <= m - 8; i++) ans = mul(ans, 3);
        printf("%d\n", ans);
      }
    }
    if (n == 7) {
      if (m <= 8) printf("%d\n", ans7[m - 7]);
      else {
        int ans = ans7[8 - 7];
        for (int i = 1; i <= m - 8; i++) ans = mul(ans, 3);
        printf("%d\n", ans);
      }
    }
    if (n == 8) {
      printf("%d\n", ans8[m - 8]);
    }

  }
}
int main() {
#ifndef LOCAL
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);
#endif
  scanf("%d%d", &n, &m);
  if (n > m) std::swap(n, m);


   solver2::main();
}
