#!/bin/bash

A=travel
B=game
C=defense
for i in $A $B $C; do
    g++ $i/$i.cpp
    for ((j = 1; ; j++)); do
        cp down/$i/$i$j.in $i.in 2> /dev/null
        if [ $? -ne 0 ]; then break; fi
        echo problem $i sample $j
        time ./a.out
        diff $i.out down/$i/$i$j.ans -Z
        if [ $? -ne 0 ]; then
            echo "wa"
        else
            echo "ok"
        fi
        echo ""
        rm $i.in $i.out
    done
    echo ""
done
diff A.cpp $A/$A.cpp
diff B.cpp $B/$B.cpp
diff C.cpp $C/$C.cpp
        
