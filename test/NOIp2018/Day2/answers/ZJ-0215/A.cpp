#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
const int MAXN = 5000 + 10;

int n, m;
std::vector <int> g[MAXN];
int gu[MAXN], gv[MAXN];
namespace solver1 {
  int xu, xv;
  int seq[MAXN], ans[MAXN], cnt;
  bool visit[MAXN];
  void dfs(int u, int fa) {
    seq[++cnt] = u;
    visit[u] = 1;
    for (int i = 0; i < (int) g[u].size(); i++) {
      int v = g[u][i];
      if (v == fa ||
          (xu == u && xv == v) ||
          (xv == u && xu == v) || visit[v]) continue;
      dfs(v, u);
    }
  }

  void check() {
    bool flag = 0;
    for (int i = 1; i <= n; i++) {
      if (ans[i] != seq[i]) {
        flag = seq[i] < ans[i];
        break;
      }
    }

    if (flag) {
      std::copy(seq + 1, seq + n + 1, ans + 1);
    }
  }
  void main() {
    for (int i = 1; i <= n; i++) {
      std::sort(g[i].begin(), g[i].end());
    }

    if (m == n - 1) {
      xu = xv = -1;
      dfs(1, 0);
      for (int i = 1; i <= n; i++) {
        printf("%d ", seq[i]);
      }
      printf("\n");

    } else {
      
      ans[1] = n + 1;
      for (int i = 1; i <= m; i++) {
        cnt = 0;
        xu = gu[i], xv = gv[i];
        std::fill(visit + 1, visit + n + 1, 0);
        dfs(1, 0);
        if (cnt == n) check();
      }
      for (int i = 1; i <= n; i++) {
        printf("%d ", ans[i]);
      }
      printf("\n");
    }
      
  }
}
int main() {
#ifndef LOCAL
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);
#endif

  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; i++) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].push_back(v);
    g[v].push_back(u);
    gu[i] = u;
    gv[i] = v;
  }
  solver1::main();
}
