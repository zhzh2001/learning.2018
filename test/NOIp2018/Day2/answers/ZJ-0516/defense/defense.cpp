//Scrooge
#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#define N 300005
#define ll long long
using namespace std;
int n,m,cnt,a,b,x,y,u,v,first[N],Next[N*2],to[N*2];
ll dp[N][2],w[N];
char TYPE[5];
int read()
{
	int res=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9')
	{
		if (ch=='-') f=-1;
		ch=getchar();
	}
	while ('0'<=ch&&ch<='9')
	{
		res=res*10+ch-'0';
		ch=getchar();
	}
	return res*f;
}
void add_edge(int x,int y)
{
	Next[++cnt]=first[x];
	first[x]=cnt;
	to[cnt]=y;
}
void dfs(int u,int fa)
{
	int Sum=0;
	bool flag1=false,flag2=false;//0表示不取u，1表示取u 
	dp[u][0]=0;
	dp[u][1]=w[u];
	for (int i=first[u];i;i=Next[i])
	{
		int v=to[i];
		if (v==fa) continue;
		dfs(v,u);
		if (dp[v][1]==1ll<<59) flag1=true;
		if ((dp[v][1]==1ll<<59)
		&& (dp[v][0]==1ll<<59)) flag2=true;
		dp[u][0]+=dp[v][1];
		dp[u][1]+=min(dp[v][0],dp[v][1]);
	}
	if (flag1) dp[u][0]=1ll<<59;
	if (flag2) dp[u][1]=1ll<<59;
	if (u==a) dp[u][1-x]=1ll<<59;
	if (u==b) dp[u][1-y]=1ll<<59;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();
	scanf("%s",TYPE+1);
	for (int i=1;i<=n;i++) w[i]=read();
	for (int i=1;i<n;i++) 
	{
		u=read(),v=read();
		add_edge(u,v);
		add_edge(v,u);
	}
	while (m--)
	{
		a=read(),x=read(),b=read(),y=read();
		dfs(1,-1);
		ll ans=min(dp[1][0],dp[1][1]);
		if (ans==1ll<<59) printf("-1\n");
		else printf("%lld\n",ans);
//		printf("\n\n");
	}
	return 0;
}
