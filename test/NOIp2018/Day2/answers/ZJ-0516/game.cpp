//Scrooge
#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#define N 15
#define mo 1000000007
using namespace std;
int n,m,ans;
int a[N][N],w1[N*2],w2[N*2],pick1[N*2],pick2[N*2];
int t[5][10];
bool flag;
void check(int x,int y,int X,int Y)
{
	if (!flag) return;
	if (x+y==n+m)
	{
		bool flag1=false;
		for (int i=1;i<n+m-1;i++)
		if (w1[i]>w2[i]) 
		{
			flag1=true;
			break;
		}
		else if (w1[i]<w2[i])
		{
			flag1=false;
			break;
		}
		if (flag1)
		{
			for (int i=1;i<n+m-1;i++)
			if (pick1[i]>pick2[i])
			{
				flag=false;
				break;
			}
			else if (pick1[i]<pick2[i]) break;
		}
		return;
	}
	for (int i=1;i<=4;i++)
	if ((x+t[i][1]<=n)&&(y+t[i][2]<=m)
	&& (X+t[i][3]<=n)&&(Y+t[i][4]<=m))
	{
		w1[x+y-1]=t[i][5];
		w2[x+y-1]=t[i][6];
		pick1[x+y-1]=a[x+t[i][1]][y+t[i][2]];
		pick2[x+y-1]=a[X+t[i][3]][Y+t[i][4]];
		check(x+t[i][1],y+t[i][2],X+t[i][3],Y+t[i][4]);
	}
}
void dfs(int x,int y)
{
	if (x>n)
	{
		flag=true;
		check(1,1,1,1);
		if (flag) 
		{
			ans++;
		}
		return;
	}
	a[x][y]=1;
	if (y==m) dfs(x+1,1);
	else dfs(x,y+1);
	a[x][y]=0;
	if (y==m) dfs(x+1,1);
	else dfs(x,y+1);	
}
int ksm(int a,int b)
{
	int res=1;
	while (b)
	{
		if (b%2) res=(1ll*res*a)%mo;
		a=(1ll*a*a)%mo;
		b=b>>1;
	}
	return res;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	if (n==1)
	{
		printf("%d\n",ksm(2,m));
	}
	if (n==2)
	{
		ans=(1ll*4*ksm(3,m-1))%mo;
		printf("%d\n",ans);
	}
	if (n==3)
	{
		if (m==1) ans=8;
		else if (m==2) ans=36;
		else ans=(1ll*112*ksm(3,m-3))%mo;
		printf("%d\n",ans);
	}
	if (n==4)
	{
		if (m==1) ans=16;
		else if (m==2) ans=108;
		else if (m==3) ans=336;
		else if (m==4) ans=912;
		else ans=(1ll*2688*ksm(3,m-5))%mo;
		printf("%d\n",ans);
	}
	if (n==5)
	{
		if (m==1) ans=32;
		else if (m==2) ans=324;
		else if (m==3) ans=1008;
		else if (m==4) ans=2688;
		else if (m==5) ans=7136;
		printf("%d\n",ans);
	}
	if (n>5) 
	{
		t[1][1]=0;t[1][2]=1;t[1][3]=0;t[1][4]=1;t[1][5]=1;t[1][6]=1;
		t[2][1]=1;t[2][2]=0;t[2][3]=0;t[2][4]=1;t[2][5]=0;t[2][6]=1;
		t[3][1]=0;t[3][2]=1;t[3][3]=1;t[3][4]=0;t[3][5]=1;t[3][6]=0;
		t[4][1]=1;t[4][2]=0;t[4][3]=1;t[4][4]=0;t[4][5]=0;t[4][6]=0;
		ans=0;
		dfs(1,1);
		printf("%d\n",ans);
	}
	return 0;
}

