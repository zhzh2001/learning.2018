//Scrooge
#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#define N 5005
using namespace std;
int n,m,cnt,ans[N],u,v;
bool map[N][N],visit[N];
void dfs(int u)
{
	visit[u]=true;
	ans[++cnt]=u;
	for (int i=1;i<=n;i++)
	if ((!visit[i])
	&& (map[u][i])) dfs(i); 
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(map,false,sizeof(map));
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d",&u,&v);
		map[u][v]=true;
		map[v][u]=true;
	}
	dfs(1);
	for (int i=1;i<n;i++) printf("%d ",ans[i]);
	printf("%d\n",ans[n]);
	return 0;
}
