#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdio>
using namespace std;
const int MAXN=5010;
const int INF=99999999;
struct edge{
  int u,v,nex;
};
edge e[MAXN<<1];
int head[MAXN],cnt,viss[MAXN];
bool vis[MAXN];
int pre[MAXN];
int n,m;
int ans[MAXN],ccans[MAXN];

inline void add(int u,int v){
  e[++cnt].u=u;
  e[cnt].v=v;
  e[cnt].nex=head[u];
  head[u]=cnt;
}

void dfs1(int now,int step){
  if(viss[now]==3){
    return;
  }
  if(viss[now]==1){
    ccans[step]=now;
  }
  if(step==n){
    bool pd=false;
    for(int i=1;i<=n;i++){
      if(ans[i]<ccans[i]){
	break;
      }
      if(ans[i]>ccans[i]){
	pd=true;
      }
    }
    if(pd==true){
      for(int i=1;i<=n;i++){
	ans[i]=ccans[i];
      }
    }
    return;
  }
  for(int i=head[now];i;i=e[i].nex){
    if(viss[e[i].v]==0){
      viss[e[i].v]++;
      dfs1(e[i].v,step+1);
      viss[e[i].v]--;
    }
    else if(viss[e[i].v]==1){
      viss[e[i].v]++;
      dfs1(e[i].v,step);
      viss[e[i].v]--;
    }
  }
  return;
}

void dfs(int now,int step){
  if(step==n+1){
    return;
  }
  bool pd=false;
  ans[step]=now;
  vis[now]=true;
  int zuixiao=INF;
  if(step==n){
    return;
  }
  for(int i=head[now];i;i=e[i].nex){
    if(!vis[e[i].v]){
      pd=true;
      zuixiao=min(zuixiao,e[i].v);
    }
  }
  while(pd==false){
    now=pre[now];
    zuixiao=INF;
    for(int i=head[now];i;i=e[i].nex){
      if(!vis[e[i].v]){
	pd=true;
	zuixiao=min(zuixiao,e[i].v);
      }
    }
  }
  pre[zuixiao]=now;
  dfs(zuixiao,step+1);
}

int main(){
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  memset(vis,false,sizeof(vis));
  scanf("%d %d",&n,&m);
  for(int i=1;i<=m;i++){
    int x,y;
    scanf("%d %d",&x,&y);
    add(x,y);
    add(y,x);
  }
  if(m==n-1){
    dfs(1,1);
  }
  else{
    for(int i=1;i<=n;i++){
      ans[i]=INF;
    }
    viss[1]++;
    dfs1(1,1);
  }
  for(int i=1;i<=n;i++){
    printf("%d ",ans[i]);
  }
}
