#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
const int MAXN=100000;
const long long INF=9999999999;
int n,m;
char op[4];
int money[MAXN];
long long dp[MAXN][3];
int a,xx,b,yy;

struct edge{
  int u,v,nex;
};
edge e[MAXN<<1];
int head[MAXN];
int cnt;

inline void add(int u,int v){
  e[++cnt].u=u;
  e[cnt].v=v;
  e[cnt].nex=head[u];
  head[u]=cnt;
}

long long dfs(int now,int lx,int fa){
  if((now==a&&lx!=xx)||(now==b&&lx!=yy)){
    return INF;
  }
  if(dp[now][lx]!=INF){
    return dp[now][lx];
  }
  bool pd=false;
  dp[now][lx]=0;
  for(int i=head[now];i;i=e[i].nex){
    if(e[i].v!=fa){
      if(lx==1){
	pd=true;
	dp[now][lx]+=min(dfs(e[i].v,1,now),dfs(e[i].v,0,now));
      }
      else if(lx==0){
	pd=true;
	dp[now][lx]+=dfs(e[i].v,1,now);
      }
    }
  }
  if(lx==1){
    dp[now][lx]+=money[now];
  }
  if(pd==false){
    if(lx==1){
      dp[now][lx]=money[now];
    }
    else{
      dp[now][lx]=0;
    }
  }
  return dp[now][lx];
}

int main(){
  freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout);
  cnt=0;
  scanf("%d %d",&n,&m);
  scanf("%s",op);
  for(int i=1;i<=n;i++){
    scanf("%d",&money[i]);
  }
  for(int i=1;i<n;i++){
    int x,y;
    scanf("%d %d",&x,&y);
    add(x,y);
    add(y,x);
  }
  while(m--){
    scanf("%d %d %d %d",&a,&xx,&b,&yy);
    for(int i=1;i<=n;i++){
      dp[i][0]=INF;
      dp[i][1]=INF;
    }
    long long ans=min(dfs(1,0,0),dfs(1,1,0));
    if(ans>=INF){
      puts("-1");
    }
    else{
      printf("%lld\n",ans);
    }
  }
  return 0;
}
