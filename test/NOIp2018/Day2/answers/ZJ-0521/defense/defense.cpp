#include<cstdio>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x) for(int y,i=head[x];i;i=E[i].nxt)

using namespace std;

const int N=100035;

char opt[15];
long long f[N][2][2];
int p[N],n,m,OA,b,OB,a,ans;

int cnt,head[N];
struct Edge { int to,nxt; }E[N<<1];
void Link(int u,int v) {
	E[++cnt]=(Edge){v,head[u]},head[u]=cnt;
	E[++cnt]=(Edge){u,head[v]},head[v]=cnt;
}

void dp(int x,int fa) {
	int sum=0,flg=0;
	
	f[x][1][1]=p[x];
	f[x][0][0]=0,f[x][0][1]=3e8;
	
	Rep (i,x) if ((y=E[i].to)!=fa) {
		dp(y,x),flg=1;
		f[x][0][0]+=f[y][0][1];
		f[x][1][1]+=min(f[y][0][0],min(f[y][0][1],f[y][1][1]));
		sum+=min(f[y][0][1],f[y][1][1]);
	}
	Rep (i,x) if ((y=E[i].to)!=fa)
		f[x][0][1]=min(f[x][0][1],sum-min(f[y][0][1],f[y][1][1])+f[y][1][1]);
	
	if (!flg) f[x][0][1]=3e8;
	if ((x==a && OA==1) || (x==b && OB==1)) f[x][0][0]=f[x][0][1]=3e8;
	if ((x==a && OA==0) || (x==b && OB==0)) f[x][1][1]=3e8;
	
	if (f[x][0][0]>3e8) f[x][0][0]=3e8;
	if (f[x][0][1]>3e8) f[x][0][1]=3e8;
	if (f[x][1][1]>3e8) f[x][1][1]=3e8;
	
	return;
}

int main() {
	
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	
	scanf("%d%d",&n,&m); scanf("%s",opt);
	For (i,1,n) scanf("%d",&p[i]);
	For (i,1,n-1) {
		int x,y;
		scanf("%d%d",&x,&y);
		Link(x,y);
	}
	
	For (i,1,m) {
		scanf("%d%d%d%d",&a,&OA,&b,&OB);
		For (l,1,n) For (j,0,1) For (k,0,1)
			f[l][j][k]=3e8;
	
		dp(1,-1);
		
		ans=min(f[1][0][1],f[1][1][1]);
		if (ans==3e8) puts("-1");
		else printf("%d\n",ans);
	}
	
	return 0;
}
