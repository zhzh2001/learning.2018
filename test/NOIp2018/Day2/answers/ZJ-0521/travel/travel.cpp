#include<cstdio>
#include<vector>
#include<cstdlib>
#include<utility>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x) for(int y,i=head[x];i;i=E[i].nxt)

using namespace std;

const int N=5035;

vector<int>V[N];
pair<int,int>c[N],stk[N];
int n,m,k,X,Y,cir,top,ins[N],P[N],Ans[N];

void dfs(int x,int fa) {
	P[++k]=x;
	int y,sz=V[x].size();
	for (int i=0;i<sz;i++) if (V[x][i]!=fa) {
		if ((x==X && V[x][i]==Y) || (x==Y && V[x][i]==X))
			continue;
		y=V[x][i];
		dfs(V[x][i],x);
	}
	return;
}
bool findcir(int x,int fa) {
	ins[x]=1;
	int sz=V[x].size();
	for (int i=0;i<sz;i++) if (V[x][i]!=fa) {
		if (ins[ V[x][i] ]) {
			c[++cir]=make_pair(x,V[x][i]);
			while (stk[top+1].first!=V[x][i]) {
				c[++cir]=stk[top];
				top--;
			}
			return 1;
		}
		else {
			stk[++top]=make_pair(x,V[x][i]);
			if (findcir(V[x][i],x)) return 1;
			top--;
		}
	}
	ins[x]=0;
	return 0;
}

bool chk() {
	For (i,1,n) {
		if (P[i]<Ans[i]) return true;
		if (P[i]>Ans[i]) return false;
	}
	return false;
}

int main() {
	
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	For (i,1,m) {
		int x,y;
		scanf("%d%d",&x,&y);
		V[x].push_back(y);
		V[y].push_back(x);
	}
	For (i,1,n) sort(V[i].begin(),V[i].end());
	
	if (m==n-1) {
		X=-1,Y=-1,k=0,dfs(1,-1);
		For (i,1,n-1) printf("%d ",P[i]);
		printf("%d\n",P[n]);
	}
	else {
		findcir(1,-1);
		For (i,1,cir) {
			X=c[i].first,Y=c[i].second;
			k=0,dfs(1,-1);
			if (i==1 || chk()) {
				For (i,1,n) Ans[i]=P[i];
			}
		}
		For (i,1,n-1) printf("%d ",Ans[i]);
		printf("%d\n",Ans[n]);
	}
	
	return 0;
}
/*
6 5
1 3
2 3
2 5
3 4
4 6

1 3 2 5 4 6
*/
