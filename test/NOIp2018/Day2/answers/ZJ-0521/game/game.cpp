#include<cstdio>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)

using namespace std;

int top,n,m,k,a[10][10],stk[26],P[100005][26],ans;

void pat(int x,int y) {
	stk[++top]=a[x][y];
	if (x==n && y==m) {
		k++;
		For (i,1,top) P[k][i]=stk[i];
	}
	if (y!=m) pat(x,y+1);
	if (x!=n) pat(x+1,y);
	top--;
	return;
}
bool bijiao(int x,int y) {
	For (i,1,n+m-1) if (P[x][i]>P[y][i]) return 1;
	return 0;
}

void chk() {
	k=0;
	pat(1,1);
	For (i,1,k) For (j,i+1,k)
		if (bijiao(i,j)) {
			return;
		}
	/*
	For (i,1,n) {
		For (j,1,m) putchar(a[i][j]+48);
		puts("");
	}
	puts("-----------");
	*/
	ans++; return;
}

void dfs(int x,int y) {
	a[x][y]=0;
	if (x==n && y==m) {
		chk();
	}
	else {
		if (y==m) dfs(x+1,1);
		else dfs(x,y+1);
	}
	
	a[x][y]=1;
	if (x==n && y==m) {
		chk();
	}
	else {
		if (y==m) dfs(x+1,1);
		else dfs(x,y+1);
	}
	
	a[x][y]=0;
}

int main() {
	
	int mod=1e9+7;
	
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
//	freopen("test.out","w",stdout);
	
	scanf("%d %d",&n,&m);
	if (n==1) {
		ans=1;
		For (i,1,m) ans=ans*2%mod;
		printf("%d\n",ans);
		return 0;
	}
	if (m==1) {
		ans=1;
		For (i,1,n) ans=ans*2%mod;
		
		printf("%d\n",ans);
		return 0;
	}
	if (n==2) {
		ans=1;
		For (i,1,m-1)
			ans=((ans+ans)%mod+ans)%mod;
		ans=(((ans+ans)%mod+ans)%mod+ans)%mod;
		printf("%d\n",ans);
		return 0;
	}
	if (n==3 && m==3) {
		puts("112"); return 0;
	}
	return 0;
}
