#include<bits/stdc++.h>
using namespace std;
const int N=1e5+100,inf=1e9;
vector<int> head[N];
int n,m,costa[N][2],w[N],x1,f1,x2,f2;
bool vis[N];
int read()
{
	int x;
	scanf("%d",&x);
	return x;
}
void dfs(int x,int fa)
{
	costa[x][1]=w[x];
	for(int i=0;i<head[x].size();++i)
	{
		int y=head[x][i];
		if(y==fa) continue;
		dfs(y,x);
		costa[x][1]+=min(costa[y][1],costa[y][0]);
		costa[x][0]+=costa[y][1];
	}
	if(x==x1)
		costa[x][!f1]=inf;
	if(x==x2)
		costa[x][!f2]=inf;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();
	char ch;
	ch=getchar();
	while(!('0'<=ch&&ch<='9')) ch=getchar();
	for(int i=1;i<=n;++i)
		w[i]=read();
	for(int i=1;i<n;++i)
	{
		int x,y;
		x=read();y=read();
		head[x].push_back(y);
		head[y].push_back(x);
	}
	for(int i=1;i<=m;++i)
	{
		scanf("%d%d%d%d",&x1,&f1,&x2,&f2);
		memset(costa,0,sizeof(costa));
		dfs(1,0);
		int ans=min(costa[1][1],costa[1][0]);
		if(ans>=inf)
			printf("%d\n",-1);
		else
			printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
