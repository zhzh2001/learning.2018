#include<bits/stdc++.h>
using namespace std;
const int N=5e3+100;
vector<int> head[N];
int n,m,cnt,cot,ct,ans[N],cir[N],aans[N];
bool vis[N];
int read()
{
	int x;
	scanf("%d",&x);
	return x;
}
void dfs(int x,int fa)
{
	vector<int> t;
	ans[++cnt]=x;
	for(int i=0;i<head[x].size();++i)
	{
		int y=head[x][i];
		if(y==fa) continue;
		if(m==n&&(x==cir[ct]&&y==cir[ct+1])||(x==cir[ct+1]&&y==cir[ct]))
			continue;
		t.push_back(y);
	}
	sort(t.begin(),t.end());
	for(int i=0;i<t.size();++i)
		dfs(t[i],x);
}
bool dfs1(int x,int fa)
{
	if(vis[x]) return true;
	vis[x]=true;
	for(int i=0;i<head[x].size();++i)
	{
		int y=head[x][i];
		if(y==fa) continue;
		if(dfs1(y,x))
		{
			cir[++cot]=y;
			if(x==cir[1])
				return false;
			else
				return true;
		}
	}
	return false;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;++i)
	{
		int x,y;
		x=read();y=read();
		head[x].push_back(y);
		head[y].push_back(x);
	}
	if(m==n-1)
	{
		dfs(1,0);
		for(int i=1;i<n;++i)
			printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
	}
	else
	{
		dfs1(1,0);
		cir[cot+1]=cir[1];
		aans[1]=n+1;
		for(int i=1;i<=cot;++i)
		{
			ct=i;
			cnt=0;
			dfs(1,0);
			bool la=false;
			for(int j=1;j<=n;++j)
			{
				if(aans[j]==ans[j]) continue;
				if(aans[j]>ans[j])
					la=true;
				else
					la=false;
				break;
			}
			if(la)
				for(int i=1;i<=n;++i)
					aans[i]=ans[i];
		}
		for(int i=1;i<n;++i)
			printf("%d ",aans[i]);
		printf("%d\n",aans[n]);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
