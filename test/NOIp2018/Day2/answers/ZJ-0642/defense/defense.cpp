#include<bits/stdc++.h>
#define ll long long
#define FO(i,a,b) for (int i=a;i<=b;i++)
using namespace std;

const ll maxn=100010,inf=1000000000000LL;
struct edg{
	ll v,next;
}g[maxn<<1];
ll n,m,xx,yy,tot,op1,op2,loop,head[maxn],v[maxn],f[maxn][3];
#define go(x) for (int i=head[x];i;i=g[i].next)
char s[20];
void add(ll u,ll v){
	g[++tot].v=v;g[tot].next=head[u];head[u]=tot;
}
void dfs(ll x,ll fa){
	ll ban=3;
	f[x][0]=0;f[x][1]=v[x];
	if (xx==x) f[x][op1^1]=inf,ban=op1^1;
	if (yy==x) f[x][op2^1]=inf,ban=op2^1;
	ll del=inf,top=0;
	go(x){
		ll v=g[i].v;
		if (v==fa) continue;
		top=1;
		dfs(v,x);
		if (ban!=0) f[x][0]+=f[v][1];
		if (ban!=1) f[x][1]+=min(f[v][0],f[v][1]);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	ll uu,vv,sum=0;
	scanf("%lld%lld",&n,&m);
	scanf("%s",&s);
	FO(i,1,n) scanf("%lld",&v[i]),sum+=v[i];
	FO(i,1,n-1) scanf("%lld%lld",&uu,&vv),add(uu,vv),add(vv,uu);
	FO(i,1,m){
		loop=i;
		scanf("%lld%lld%lld%lld",&xx,&op1,&yy,&op2);
		dfs(1,0);
		ll res=3;
		if (xx==1) res=op1^1;
		if (yy==1) res=op2^1; 
		ll ans=inf; 
		if (res!=0) ans=min(ans,f[1][0]);
		if (res!=1) ans=min(ans,f[1][1]);
		(ans<=sum)?printf("%lld\n",ans):printf("-1\n");
	}
}
