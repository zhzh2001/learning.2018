#include<bits/stdc++.h>
#define FO(i,a,b) for (int i=a;i<=b;i++)
using namespace std;

const int maxn=5010,inf=0x3f3f3f3f;
struct edg{
	int v,next;
}g[maxn<<1];
int n,m,spe,ban,step,root,tar,ind,tot,xxx,bbb,finished,cnt,head[maxn],uu[maxn],vv[maxn],
	edge[maxn],vis[maxn],isok[maxn],p[maxn],nxt[maxn],a[maxn][maxn];
#define go(x) for (int i=head[x];i;i=g[i].next)
void add(int u,int v){
	g[++tot].v=v;g[tot].next=head[u];head[u]=tot;
}
void dfs1(int x,int fa){
	if (vis[x]){
		root=x;ban=1;return;
	}
	vis[x]=1;
	go(x){
		int v=g[i].v;
		if (v==fa) continue;
		dfs1(v,x);
		if (ban) return;
	}
}
void dfs2(int x,int fa,int d){
	if (vis[x]){
		ind=d-1;ban=1;
		return;
	}
	vis[x]=1;
	go(x){
		int v=g[i].v;
		if (v==fa) continue;
		p[++d]=v;isok[v]=1;dfs2(v,x,d+1);if (ban) return ;isok[v]=0;
	}
}
void dfs3(int x,int fa){
	printf("%d ",x);cnt++;
	int res=0,ee=0;
	if (x==root){
		int pos=0;
		go(x){
			int v=g[i].v;
			if (v==fa) continue;
			nxt[++pos]=v;
		}
		if (edge[x]) {
			FO(i,1,n){
				if (!a[x][i]||i==fa||isok[i]) continue;
				if (vis[i]) continue;
				if (i>nxt[1]||i>nxt[2]) continue;
				dfs3(i,x),vis[i]=1;
			}
		}
		if (nxt[1]>nxt[2]) swap(nxt[1],nxt[2]);
		dfs3(nxt[1],x);step=1;
		if (!bbb){
			if (edge[x]) {
				FO(i,1,n){
					if (!a[x][i]||i==fa||isok[i]) continue;
					if (vis[i]) continue;
					if (i>nxt[2]) continue;
					dfs3(i,x),vis[i]=1;
				}
			}
			dfs3(nxt[2],x);
		}
		if (edge[x]) {
				FO(i,1,n){
					if (!a[x][i]||i==fa||isok[i]) continue;
					if (vis[i]) continue;
					dfs3(i,x),vis[i]=1;
				}
			}
		return;
	}
	
	if (isok[x]){
		if (xxx==x) return;
		FO(i,1,n){
			if (!a[x][i]||i==fa||!isok[i]) continue;
			res=i;
		}
		if (res==root) {xxx=x;res=inf;nxt[2]=inf;ee=1;bbb=1;}
		if (res==xxx) {res=inf;ee=1;}
		if (edge[x]){
			if (!step){
				FO(i,1,n){
					if (!a[x][i]||i==fa||isok[i]) continue;
					if (vis[i]) continue;
					if (i>res||i>nxt[2]) break;
					dfs3(i,x),vis[i]=1;
				}
			}
			else{
				FO(i,1,n){
					if (!a[x][i]||i==fa||isok[i]) continue;
					if (vis[i]) continue;
					if (i>res) break;
					dfs3(i,x),vis[i]=1;
				}
			}
		}
		if (ee) return;
		if (!step){
			if (res<nxt[2]) dfs3(res,x);
			else {xxx=x;}
		}
		else dfs3(res,x);
		if (edge[x]) {
			FO(i,1,n){
				if (!a[x][i]||i==fa||isok[i]) continue;
				if (vis[i]) continue;
				dfs3(i,x),vis[i]=1;
			}
		}
		return;
	}
	FO(i,1,n){
		if (!a[x][i]||i==fa) continue;
		dfs3(i,x);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	FO(i,1,m){
		scanf("%d%d",&uu[i],&vv[i]);
		add(uu[i],vv[i]);add(vv[i],uu[i]);
		a[uu[i]][vv[i]]=a[vv[i]][uu[i]]=1;
	}
	spe=(n==m);
	ban=0;dfs1(1,0);
	if (spe){
		memset(vis,0,sizeof vis);
		p[1]=root;
		ban=0;
		dfs2(root,0,1);
		FO(i,1,m){
			if (isok[uu[i]]&&isok[vv[i]]) continue;
			if (isok[vv[i]]) swap(uu[i],vv[i]);
			if (isok[uu[i]]) edge[uu[i]]=1;
		}
	}
	memset(vis,0,sizeof vis);
	step=0;dfs3(1,0);
}
