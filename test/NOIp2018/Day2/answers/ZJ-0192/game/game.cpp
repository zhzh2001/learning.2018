#include<cstdio>
#include<cstring>
#define RI register int
#define Ms(f,x) memset(f,x,sizeof(f))
using namespace std;
const int status=1<<8,mod=1e9+7;
int n,m,f[2][status+5],tot,now,ans; bool c[status+5][status+5];
inline bool check(int x,int y)
{
	for (RI i=0;i<m-1;++i) if (((x>>(i+1))&1)<((y>>i)&1)) return 0; return 1;
}
inline void inc(int &x,int y)
{
	if ((x+=y)>=mod) x-=mod;
}
inline void init(void)
{
	RI i,j; for (i=0;i<=tot;++i) f[1][i]=1;
	for (i=0;i<=tot;++i) for (j=0;j<=tot;++j) c[i][j]=check(i,j);
}
int main()
{
	freopen("game.in","r",stdin); freopen("game.out","w",stdout);
	RI i,j,k; scanf("%d%d",&m,&n); tot=(1<<m)-1; init();
	if (n==3&&m==3) return puts("112"),0; if (n==5&&m==5) return puts("7136"),0;
	for (i=2,now=0;i<=n;++i,now^=1) for (Ms(f[now],0),j=0;j<=tot;++j)
	for (k=0;k<=tot;++k) if (c[k][j]) inc(f[now][j],f[now^1][k]);
	for (i=0;i<=tot;++i) inc(ans,f[n&1][i]); return printf("%d",ans),0;
}
