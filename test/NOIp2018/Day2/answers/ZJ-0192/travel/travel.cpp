#include<cstdio>
#include<vector>
#include<cstring>
#include<algorithm>
#define RI register int
#define pb push_back
using namespace std;
const int N=5005;
vector <int> v[N]; int n,m,x,y;
class Tree_Solver
{
	private:
		int s[N],idx;
		inline void DFS(int now,int fa)
		{
			s[++idx]=now; int lim=v[now].size();
			for (RI i=0;i<lim;++i) if (v[now][i]!=fa) DFS(v[now][i],now);
		}
	public:
		inline void solve(void)
		{
			DFS(1,0); for (RI i=1;i<=n;++i) printf("%d%c",s[i],i^n?' ':'\n');
		}
}Case1;
class Circle_Tree_Solver
{
	private:
		int ans[N],s[N],idx,l[N],r[N],pre[N],L,R,cnt; bool vis[N],flag;
		inline void paint(int now)
		{
			while (pre[now]) l[++cnt]=pre[now],r[cnt]=now,now=pre[now];
		}
		inline void find(int now,int fa)
		{
			if (flag) return; vis[now]=1; int lim=v[now].size();
			for (RI i=0;i<lim;++i)
			{
				int to=v[now][i]; if (to==fa) continue;
				if (vis[to]) return (void)(l[++cnt]=fa,r[cnt]=now,flag=1,paint(fa));
				if (flag) return; pre[to]=now; find(to,now); if (flag) return;
			}
		}
		inline void DFS(int now,int fa)
		{
			s[++idx]=now; int lim=v[now].size();
			for (RI i=0;i<lim;++i)
			{
				int to=v[now][i]; if (to==fa) continue;
				if ((now==L&&to==R)||(now==R&&to==L)) continue; DFS(to,now);
			}
		}
		inline bool check(void)
		{
			if (!ans[1]) return 1; for (RI i=1;i<=n;++i)
			{
				if (s[i]>ans[i]) return 0;
				if (s[i]<ans[i]) return 1;
			}
			return 0;
		}
	public:
		inline void solve(void)
		{
			RI i; for (find(1,0),i=1;i<=cnt;++i)
			{
				L=l[i]; R=r[i]; idx=0;
				DFS(1,0); if (check()) memcpy(ans,s,sizeof(ans));
			}
			for (i=1;i<=n;++i) printf("%d%c",ans[i],i^n?' ':'\n');
		}
}Case2;
int main()
{
	freopen("travel.in","r",stdin); freopen("travel.out","w",stdout);
	RI i; for (scanf("%d%d",&n,&m),i=1;i<=m;++i) scanf("%d%d",&x,&y),v[x].pb(y),v[y].pb(x);
	for (i=1;i<=n;++i) sort(v[i].begin(),v[i].end()); return (m^n?Case1.solve():Case2.solve()),0;
}
