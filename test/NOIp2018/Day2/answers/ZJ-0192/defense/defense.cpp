#include<cstdio>
#include<cctype>
#define RI register int
#define add(x,y) e[++cnt]=(edge){y,head[x]},head[x]=cnt
using namespace std;
const int N=100005;
const long long INF=1e12;
struct edge
{
	int to,nxt;
}e[N<<1]; int head[N],p[N],cnt,n,m,x,y,a,b,opt2; long long f[N][2],ans; char opt1;
class FileInputOutput
{
	private:
		#define S 1<<21
		#define tc() (A==B&&(B=(A=Fin)+fread(Fin,1,S,stdin),A==B)?EOF:*A++)
		char Fin[S],*A,*B;
	public:
		FileInputOutput() { A=B=Fin; }
		inline void gc(char &ch)
		{
			while (!isalpha(ch=tc()));
		}
		inline void read(int &x)
		{
			x=0; char ch; while (!isdigit(ch=tc()));
			while (x=(x<<3)+(x<<1)+(ch&15),isdigit(ch=tc()));
		}
		#undef S
		#undef tc
}F;
inline long long min(long long a,long long b)
{
	return a<b?a:b;
}
#define to e[i].to
inline void DFS(int now,int fa)
{
	f[now][0]=0; f[now][1]=p[now];
	for (RI i=head[now];i;i=e[i].nxt) if (to!=fa)
	{
		DFS(to,now); f[now][0]+=f[to][1];
		f[now][1]+=min(f[to][0],f[to][1]);
	}
	if (a==now) f[now][x^1]=INF;
	if (b==now) f[now][y^1]=INF;
	f[now][0]=min(f[now][0],INF); f[now][1]=min(f[now][1],INF);
}
#undef to
int main()
{
	freopen("defense.in","r",stdin); freopen("defense.out","w",stdout);
	RI i; for (F.read(n),F.read(m),F.gc(opt1),F.read(opt2),i=1;i<=n;++i) F.read(p[i]);
	for (i=1;i<n;++i) F.read(x),F.read(y),add(x,y),add(y,x);
	while (m--)
	{
		F.read(a); F.read(x); F.read(b); F.read(y);
		DFS(1,0); ans=min(f[1][0],f[1][1]); printf("%lld\n",ans!=INF?ans:-1);
	}
	return 0;
}
