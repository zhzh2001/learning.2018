/*program by mangoyang*/
#include<bits/stdc++.h>
#define inf (0x7f7f7f7f)
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
typedef long long ll;
using namespace std;
template <class T>
inline void read(T &x){
	int ch = 0, f = 0; x = 0;
	for(; !isdigit(ch); ch = getchar()) if(ch == '-') f = 1;
	for(; isdigit(ch); ch = getchar()) x = x * 10 + ch - 48;
	if(f) x = -x;
}
const int mod = 1e9+7;
int n, m, tot, tot2, error;
namespace bf{
	char a[20][20]; string s1, s2;
	vector<string> c, d;
	inline void dfs2(int x, int y, string s1, string s2){
		if(x == n && y == m){
			c.push_back(s1), d.push_back(s2);
			return;
		}
		if(x < n) dfs2(x + 1, y, s1 + "D", s2 + a[x+1][y]);
		if(y < m) dfs2(x, y + 1, s1 + "R", s2 + a[x][y+1]);
	}
	inline void check(){
		c.clear(), d.clear(); 
		s1.clear(), s2.clear(), dfs2(1, 1, s1, s2 + a[1][1]);
		for(int i = 0; i < c.size(); i++)
			for(int j = i + 1; j < c.size(); j++){
				if(c[i] > c[j] && d[i] > d[j]) return;
				if(c[j] > c[i] && d[j] > d[i]) return;
			}
		tot++;
	}
	inline void dfs(int x, int y){
		if(x == n + 1) return (void) (check());
		int nx = (y == m) ? x + 1 : x , ny = (y == m) ? 1 : y + 1;
		a[x][y] = '1', dfs(nx, ny), a[x][y] = '0', dfs(nx, ny);
	}
}
namespace gao{
	inline void up(int &x, int y){ 
		x = (x + y >= mod) ? (x + y - mod) : (x + y); 
	}
	int f[2][10][513];
	inline void solve(){
		f[0][n][(1<<n)-1] = 1;
		for(int i = 1, o = 1; i <= m; i++, o ^= 1){
			for(int s = 0; s < (1 << n); s++) f[o][0][s] = f[o^1][n][s];
			for(int j = 0; j <= n; j++)
				for(int s = 0; s < (1 << n); s++) f[o^1][j][s] = 0;
			for(int j = 0; j <= n; j++)
				for(int s = 0; s < (1 << n); s++) if(f[o][j][s]){
					if(j == n - 1 || ((1 << j + 1) & s)){ 
						up(f[o][j+1][s|(1<<j)], f[o][j][s]);
						up(f[o][j+1][(s|(1<<j))^(1<<j)], f[o][j][s]);
					}
					else up(f[o][j+1][(s|(1<<j))^(1<<j)], f[o][j][s]);			
				}
		}
		int ans = 0;
		for(int i = 0; i < (1 << n); i++) up(ans, f[m&1][n][i]);
		cout << ans;
	}	
}
using namespace gao;
int main(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	read(n), read(m);
	if(n <= 3 && m <= 3){
		bf::dfs(1, 1), cout << tot << endl;
	}
	else if(n == 2) solve(); 
	else bf::dfs(1, 1), cout << tot << endl;
	return 0;
}
