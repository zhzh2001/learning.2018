/*program by mangoyang*/
#include<bits/stdc++.h>
#define inf (0x7f7f7f7f)
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
typedef long long ll;
using namespace std;
template <class T>
inline void read(T &x){
	int ch = 0, f = 0; x = 0;
	for(; !isdigit(ch); ch = getchar()) if(ch == '-') f = 1;
	for(; isdigit(ch); ch = getchar()) x = x * 10 + ch - 48;
	if(f) x = -x;
}
const int N = 100005;
vector<int> g[N];
int ans[N], inc[N], rans[N], vis[N], c[N], tot, n, m, len, root, bu, bv;
inline int gaoc(int u, int fa){
	if(vis[u]) return root = u, 1;
	vis[u] = 1;
	for(int i = 0; i < g[u].size(); i++){
		int v = g[u][i];
		if(v == fa) continue;
		int tmp = gaoc(v, u);
		if(!tmp) continue;
		if(tmp == 1){
			c[++tot] = v, inc[v] = 1;
			if(u == root) return 2; else return 1;
		}
		return 2;
	}
	return 0;
}
inline void gao(int u, int fa){
	ans[++len] = u;
	for(int i = 0; i < g[u].size(); i++){
		int v = g[u][i];
		if(bu == u && bv == v) continue;
		if(bv == u && bu == v) continue;
		if(v != fa) gao(v, u);
	}
}
inline void check(){
	if(!rans[1]){
		for(int i = 1; i <= n; i++) rans[i] = ans[i];
		return;
	}
	for(int i = 1; i <= n; i++){
		if(rans[i] < ans[i]) return;
		if(ans[i] < rans[i]){
			for(int i = 1; i <= n; i++) rans[i] = ans[i];
			return;
		}
	}
}
int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	read(n), read(m);
	for(int i = 1, x, y; i <= m; i++){
		read(x), read(y);
		g[x].push_back(y), g[y].push_back(x);
	}
	for(int i = 1; i <= n; i++) sort(g[i].begin(), g[i].end());
	if(m == n - 1){
		gao(1, 0);
		for(int i = 1; i <= n; i++) printf("%d ", ans[i]);
		return 0;
	}
	gaoc(1, 0);
	for(int i = 1; i < tot; i++)
		bu = c[i], bv = c[i+1], len = 0, gao(1, 0), check();
	bu = c[tot], bv = c[1], len = 0, gao(1, 0), check();
	for(int i = 1; i <= n; i++) printf("%d ", rans[i]);
	return 0;
}
