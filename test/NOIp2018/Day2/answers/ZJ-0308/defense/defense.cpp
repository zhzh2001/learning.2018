/*program by mangoyang*/
#include<bits/stdc++.h>
#define inf ((ll)(1e13))
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
typedef long long ll;
using namespace std;
template <class T>
inline void read(T &x){
	int ch = 0, f = 0; x = 0;
	for(; !isdigit(ch); ch = getchar()) if(ch == '-') f = 1;
	for(; isdigit(ch); ch = getchar()) x = x * 10 + ch - 48;
	if(f) x = -x;
}
#define int ll
const int N = 200005;
char str[10];
int val[N], n, m;
namespace bf{
	vector<int> g[N];
	int f[N][2], ban[N];
	inline void dfs(int u, int fa){
		f[u][0] = 0, f[u][1] = val[u];
		for(int i = 0; i < g[u].size(); i++){
			int v = g[u][i];
			if(v == fa) continue;
			dfs(v, u);
			f[u][0] += f[v][1];
			f[u][1] += min(f[v][1], f[v][0]);
		}
		if(~ban[u]) f[u][ban[u]] = inf;
		if(f[u][0] > inf) f[u][0] = inf;
		if(f[u][1] > inf) f[u][1] = inf;
	}
	inline void solve(){
		for(int i = 1, x, y; i < n; i++){
			read(x), read(y);
			g[x].push_back(y), g[y].push_back(x);
		}
		memset(ban, -1, sizeof(ban));
		for(int i = 1, a, b, x, y; i <= m; i++){
			read(a), read(x), read(b), read(y);
			ban[a] = x ^ 1, ban[b] = y ^ 1;
			dfs(1, 0);
			int ans = min(f[1][0], f[1][1]);
			if(ans == inf) puts("-1"); else printf("%lld\n", ans);
			ban[a] = ban[b] = -1;
		}
	}
}
namespace one{
	vector<int> g[N];
	int f[N][2], ans[N][2];
	inline void dfs(int u, int fa){
		f[u][0] = 0, f[u][1] = val[u];
		for(int i = 0; i < g[u].size(); i++){
			int v = g[u][i];
			if(v == fa) continue;
			dfs(v, u);
			f[u][0] += f[v][1];
			f[u][1] += min(f[v][1], f[v][0]);
		}
		if(u == 1) f[u][0] = inf;
	}
	inline void dfs2(int u, int fa){
		ans[u][0] = f[u][0], ans[u][1] = f[u][1];
		for(int i = 0; i < g[u].size(); i++){
			int v = g[u][i];
			if(v == fa) continue;
			int tmp0 = f[u][0], tmp1 = f[u][1];
			if(f[u][0] != inf) f[u][0] -= f[v][1];
			if(f[u][1] != inf) f[u][1] -= min(f[v][0], f[v][1]);
			f[v][0] += f[u][1];
			f[v][1] += min(f[u][0], f[u][1]);
			if(f[v][0] > inf) f[v][0] = inf;
			if(f[v][1] > inf) f[v][1] = inf;
			dfs2(v, u), f[u][0] = tmp0, f[u][1] = tmp1;
		}
	}
	inline void solve(){
		for(int i = 1, x, y; i < n; i++){
			read(x), read(y);
			g[x].push_back(y), g[y].push_back(x);
		}
		dfs(1, 0), dfs2(1, 0);
		for(int i = 1, a, b, x, y; i <= m; i++){
			read(a), read(x), read(b), read(y);
			if(ans[b][y] == inf) puts("-1"); else printf("%lld\n", ans[b][y]);
		}
	}
}
namespace A2{
	int f[N][2], g[N][2];
	inline void solve(){
		for(int i = 1, x, y; i < n; i++) read(x), read(y);
		memset(f, 33, sizeof(f));
		memset(g, 33, sizeof(g));
		f[0][1] = g[n+1][1] = f[0][0] = g[n+1][0] = 0;
		for(int i = 1; i <= n; i++){
			f[i][0] = f[i-1][1];
			f[i][1] = min(f[i-1][0], f[i-1][1]) + val[i];
			f[i][1] = min(f[i][1], inf);
			f[i][0] = min(f[i][0], inf);
		}
		for(int i = n; i >= 1; i--){
			g[i][0] = g[i+1][1];
			g[i][1] = min(g[i+1][0], g[i+1][1]) + val[i];
			g[i][1] = min(g[i][1], inf);
			g[i][0] = min(g[i][0], inf);
		}
		for(int i = 1, a, b, x, y; i <= m; i++){
			read(a), read(x), read(b), read(y);
			if(a > b) swap(a, b);
			if(!x && !y) puts("-1"); else printf("%lld\n", f[a][x] + g[b][y]);
		}
	}
}
signed main(){	
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	read(n), read(m), scanf("%s", str);
	for(int i = 1; i <= n; i++) read(val[i]);
	if(n <= 2000 && m <= 2000) bf::solve();
	else if(str[1] == '1') one::solve();
	else if(str[0] == 'A' && str[1] == '2') A2::solve();
	else bf::solve();
	return 0;
}
