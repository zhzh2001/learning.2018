#include<bits/stdc++.h>
#define N 100005
using namespace std;
bool cur1;
int n,m;
char TYPE[10];
int p[N];
int fa[N];
int head[N],id;
struct edge{
	int to,nxt;
}E[N<<1];
void add_edge(int a,int b){
	E[++id]=(edge){b,head[a]};
	head[a]=id;
}
void dfs_init(int x,int f){
	for(int i=head[x];i;i=E[i].nxt){
		int v=E[i].to;
		if(v==f)continue;
		fa[v]=x;
		dfs_init(v,x);
	}
}
#define M 2005
struct P44{
	long long mn1[M],mn2[M];
	bool mark[M],ned[M];
	void dfs(int x,int f){
		mn1[x]=p[x];mn2[x]=0;
		if(mark[x]){
			if(ned[x])mn2[x]=1e13;
			else mn1[x]=1e13;
		}
		for(int i=head[x];i;i=E[i].nxt){
			int v=E[i].to;
			if(v==f)continue;
			dfs(v,x);
			mn1[x]+=min(mn1[v],mn2[v]);
			mn2[x]+=mn1[v];
		}
	}
	void solve(){
		for(int i=1;i<=m;i++){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(x==0&&y==0&&(fa[a]==b||fa[b]==a)){
				puts("-1");continue;
			}
			mark[a]=1;mark[b]=1;
			ned[a]=x;ned[b]=y;
			dfs(1,-1);
			mark[a]=0;mark[b]=0;
			ned[a]=0;ned[b]=0;
			printf("%lld\n",min(mn1[1],mn2[1]));
		}
	}
}p44;
#undef M
struct Plink{
	long long mn1[N],mn2[N];
	void solve(){
		for(int i=n;i>=1;i--){
			mn1[i]=p[i];mn2[i]=0;
			mn1[i]+=min(mn1[i],mn2[i]);
			mn2[i]+=mn1[i+1];
		}
		for(int i=1;i<=m;i++){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(a>b)swap(a,b),swap(x,y);
			if(a+1==b&&x==0&&y==0){puts("-1");continue;}
			printf("%lld\n",min(mn1[1],mn2[1]));
		}
	}
}plink;
bool cur2;
int main(){
//	printf("%lf MB\n",(&cur2-&cur1)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,TYPE+1);
	for(int i=1;i<=n;i++)scanf("%d",&p[i]);
	for(int i=1,a,b;i<n;i++){
		scanf("%d%d",&a,&b);
		add_edge(a,b);
		add_edge(b,a);
	}
	dfs_init(1,-1);
	if(n<=2000&&m<=2000)p44.solve();
	else if(TYPE[1]=='A')plink.solve();
	return 0;
}
