#include<bits/stdc++.h>
#define p 1000000007
using namespace std;
bool cur1;
int n,m;
bool DEBUG;
struct P20{
	int que[1050],cnt;
	int mp[20][20];
	void init(){
		for(int i=0;i<(1<<(n+m-2));i++){
			int ct=0,h=i;
			while(h)h^=(h&(-h)),ct++;
			if(ct==m-1)que[++cnt]=i;
		}
	}
	bool is_ok(int x,int y){
		int X1=1,X2=1,Y1=1,Y2=1;
		for(int i=n+m-2;i>=1;i--){
			if(x&(1<<(i-1)))Y1++;
			else X1++;
			if(y&(1<<(i-1)))Y2++;
			else X2++;
			if(mp[X1][Y1]>mp[X2][Y2])return false;
			if(mp[X1][Y1]<mp[X2][Y2])return true;
		}
		return true;
	}
	bool check(int X){
		for(int i=1;i<=n*m;i++){
			int x=(i+m-1)/m,y=(i-1)%m+1;
			if(X&(1<<(i-1)))mp[x][y]=1;
			else mp[x][y]=0;
		}
		for(int i=1;i<cnt;i++){
			for(int j=i+1;j<=cnt;j++){
				if(!is_ok(que[i],que[j]))return false;
			}
		}
		return true;
	}
	void solve(){
		init();
		int ans=0;
		for(int i=0;i<(1<<(n*m));i++){
			if(i==504)DEBUG=1;
			if(check(i))ans++;
			if(i==504)DEBUG=0;
		}
		printf("%d\n",ans);
	}
}p20;
struct P100{
	void solve(){
		
	}
}p100;
bool cur2;
int main(){
//	printf("%lf MB\n",(&cur2-&cur1)/1024.0/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	p20.solve();
	return 0;
}
