#include <bits/stdc++.h>

typedef long long ll;

const int N = 300100, M = N * 2;
const ll INF = 0x3f3f3f3f3f3f3f3fll;

char type[6];

int n, q, E = 0;
int w[N];
int to[M], first[N], next[M];
int p[N];
int A, X, B, Y;
ll f[N], g[N];

inline void down(ll &x, const ll y) {x > y ? x = y : 0;}

inline void addedge(int u, int v) {
	to[++E] = v; next[E] = first[u]; first[u] = E;
	to[++E] = u; next[E] = first[v]; first[v] = E;
}

void dfs(int x) {
	int i, y;
	for (i = first[x]; i; i = next[i])
		if ((y = to[i]) != p[x]) {
			p[y] = x; dfs(y);
		}
}

void Dfs(int x) {
	int i, y; f[x] = w[x]; g[x] = 0;
	for (i = first[x]; i; i = next[i])
		if (p[y = to[i]] == x) {
			Dfs(y);
			if (f[y] < INF || g[y] < INF) f[x] += std::min(f[y], g[y]);
			else f[x] = INF;
			if (f[y] < INF) g[x] += f[y];
			else g[x] = INF;
		}
	if (x == A) (X ? g[x] : f[x]) = INF;
	if (x == B) (Y ? g[x] : f[x]) = INF;
}

namespace Chain {
	#define segc int M = L + R - 1 >> 1, lc = id << 1, rc = lc | 1

	struct node {ll v[2][2];} x[N * 4];

	int w[N];

	void update(node &ret, const node l, const node r) {
		int i, j;
		for (i = 0; i < 2; ++i)
			for (j = 0; j < 2; ++j) {
				ret.v[i][j] = INF;
				down(ret.v[i][j], l.v[i][1] + r.v[1][j]);
				down(ret.v[i][j], l.v[i][1] + r.v[0][j]);
				down(ret.v[i][j], l.v[i][0] + r.v[1][j]);
			}
	}

	void build(int id, int L, int R) {
		if (L == R) {x[id].v[0][0] = 0; x[id].v[1][1] = w[L]; x[id].v[0][1] = x[id].v[1][0] = INF; return;}
		segc; build(lc, L, M); build(rc, M + 1, R);
		update(x[id], x[lc], x[rc]);
	}

	node range(int id, int L, int R, int ql, int qr) {
		if (ql <= L && R <= qr) return x[id];
		segc; node ret, l, r;
		if (qr <= M) return range(lc, L, M, ql, qr);
		if (ql > M) return range(rc, M + 1, R, ql, qr);
		l = range(lc, L, M, ql, M); r = range(rc, M + 1, R, M + 1, qr);
		return update(ret, l, r), ret;
	}

	int main() {
		int i; ll ans, cur; node t;
		for (i = 1; i <= n; ++i) scanf("%d", w + i);
		for (i = 1; i < n; ++i) scanf("%*d%*d");
		build(1, 1, n);
		for (; q; --q) {
			scanf("%d%d%d%d", &A, &X, &B, &Y);
			if (A > B) std::swap(A, B), std::swap(X, Y);
			if (!(X || Y) && A + 1 == B) {puts("-1"); continue;}
			ans = (X ? w[A] : 0) + (Y ? w[B] : 0);
			if (A != 1) {
				t = range(1, 1, n, 1, A - 1);
				cur = std::min(t.v[0][1], t.v[1][1]);
				if (X) down(cur, std::min(t.v[0][0], t.v[1][0]));
				ans += cur;
			}
			if (A + 1 != B) {
				t = range(1, 1, n, A + 1, B - 1);
				cur = t.v[1][1];
				if (X) down(cur, t.v[0][1]);
				if (Y) down(cur, t.v[1][0]);
				if (X && Y) down(cur, t.v[0][0]);
				ans += cur;
			}
			if (B != n) {
				t = range(1, 1, n, B + 1, n);
				cur = std::min(t.v[1][0], t.v[1][1]);
				if (Y) down(cur, std::min(t.v[0][0], t.v[0][1]));
				ans += cur;
			}
			printf("%lld\n", ans);
		}
		return 0;
	}
}

int main() {
	int i, u, v;
#ifndef FY
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
#endif
	scanf("%d%d%s", &n, &q, type);
	if (*type == 65) return Chain::main();
	for (i = 1; i <= n; ++i) scanf("%d", w + i);
	for (i = 1; i < n; ++i) scanf("%d%d", &u, &v), addedge(u, v);
	dfs(1);
	for (; q; --q) {
		scanf("%d%d%d%d", &A, &X, &B, &Y);
		if (!(X || Y) && (p[A] == B || p[B] == A)) {puts("-1"); continue;}
		Dfs(1);
		printf("%lld\n", std::min(f[1], g[1]));
	}
	return 0;
}
