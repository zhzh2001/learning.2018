#include <bits/stdc++.h>

typedef std::vector <int> vector;

const int N = 5100, M = N * 2;

int V, E;
vector G[N];
int p[N], best[N];
int cnt = 0, o[N], id[N];
int cyt = 0, cyc[N];
bool is_cyc[N], cut = false;
int top = 0, back[N];

bool better(int n, int *a, int *b) {
	for (int i = 1; i <= n; ++i) if (a[i] != b[i]) return a[i] < b[i];
	return false;
}

void dfs(int x) {
	int i, j, y, deg = G[x].size();
	o[++cnt] = x; id[x] = cnt;
	for (i = 0; i < deg; ++i)
		if (!id[y = G[x][i]])
			p[y] = x, dfs(y);
		else if (y != p[x] && id[x] < id[y]) { // cycle
			for (j = y; j != x; j = p[j]) cyc[++cyt] = j, is_cyc[j] = true;
			cyc[++cyt] = x; is_cyc[x] = true;
		}
}

void dfs2(int x) {
	int i, y, deg = G[x].size(); bool fy;
	o[++cnt] = x; id[x] = cnt;
	for (i = 0; i < deg; ++i)
		if (!id[y = G[x][i]]) {
			fy = !cut && is_cyc[x] && is_cyc[y];
			if (fy && i + 1 < deg) back[++top] = G[x][i + 1];
			if (fy && y > back[top]) {cut = true; continue;}
			p[y] = x, dfs2(y);
			if (fy && i + 1 < deg) --top;
		}
}

int main() {
	int i, u, v;
#ifndef FY
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
#endif
	scanf("%d%d", &V, &E);
	for (i = 0; i < E; ++i) scanf("%d%d", &u, &v), G[u].push_back(v), G[v].push_back(u);
	for (i = 1; i <= V; ++i) std::sort(G[i].begin(), G[i].end());
	dfs(1); memcpy(best + 1, o + 1, V << 2);
	if (cyt) {
		assert(cyt >= 3); back[0] = INT_MAX;
		cnt = 0; memset(id + 1, 0, V << 2); dfs2(1);
		if (better(V, o, best)) memcpy(best + 1, o + 1, V << 2);
	}
	for (i = 1; i <= V; ++i) printf("%d%c", best[i], i == V ? 10 : 32);
	return 0;
}
