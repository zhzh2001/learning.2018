#include <bits/stdc++.h>
#define M2 260

typedef long long ll;
const ll mod = 1000000007;

int R, C;

int dp[2][M2][M2], (*cur)[M2] = *dp, (*nxt)[M2] = dp[1]; 

inline void add(int &x, const int y) {x = (x + y >= mod ? x + y - mod : x + y);}

ll PowerMod(ll a, int n, ll c = 1) {for (; n; n >>= 1, a = a * a % mod) if (n & 1) c = c * a % mod; return c;}

int main() {
	int i, j, k, l, c, d, G, mask, fy = 0; ll ans = 0;
#ifndef FY
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
#endif
	scanf("%d%d", &C, &R);
	if (C > R) std::swap(C, R); // ensure C <= R
	if (C == 1) return printf("%lld\n", PowerMod(2, R)), 0;
	if (C == 2) return printf("%lld\n", PowerMod(3, R - 1, 4)), 0;
	nxt[0][0] = 1; mask = (1 << C - 1) - 1;
	for (i = 2; i <= R + C - 2; ++i) {
		std::swap(cur, nxt);
		if (C + 4 <= i) {
			fy = mask - 3;
			for (j = fy; j <= mask; ++j)
				for (k = fy; k <= mask; ++k)
					nxt[j][k] = 0;
		} else memset(nxt, 0, sizeof *dp);
		for (j = fy; j <= mask; ++j)
			for (k = fy; k <= mask; ++k)
				if (cur[j][k]) {
					d = std::max(0, i - R);
					c = std::min(i, C);
					for (l = d; l <= c; ++l)
						if (l == d || !(j >> l - 1 & 1)) {
							G = ((1 << c) - 2) & ~(1 << l);
							add(nxt[k][(k | k << 1 | G) & mask], cur[j][k]);
						}
				}
	}
	for (j = fy; j <= mask; ++j)
		for (k = fy; k <= mask; ++k)
			ans += nxt[j][k];
	printf("%lld\n", ans * 4 % mod);
	return 0;
}
