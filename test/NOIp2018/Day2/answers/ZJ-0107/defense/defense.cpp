#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=100005;
ll f[N][2];
int g[N],vis[N];
int head[N],nxt[2*N],tail[2*N],t;
inline void addto(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
const ll oo=1e14;
void dfs(int k,int fa)
{
//	cout<<k<<endl;
	f[k][0]=0;
	f[k][1]=g[k];
	for(int i=head[k];i;i=nxt[i]){
		int v=tail[i];
		if(v==fa)continue;
		dfs(v,k);
		if(!vis[v]){
			f[k][0]+=f[v][1];
			f[k][1]+=min(f[v][1],f[v][0]);
		}else if(vis[v]==1){
			f[k][0]+=f[v][1];
			f[k][1]+=f[v][1];
		}else if(vis[v]==2){
			f[k][0]+=oo;
			f[k][1]+=f[v][0];
		}
	}
}
int n,m;string s;
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();cin>>s;
	for(int i=1;i<=n;i++)
		g[i]=read();
	for(int i=1;i<n;i++){
		int x,y;
		x=read(),y=read();
		addto(x,y);
		addto(y,x);
	}
	for(int i=1;i<=m;i++){
		int x,y,a,b;
		a=read(),x=read(),b=read(),y=read();
		vis[a]=2-x;
		vis[b]=2-y;
		dfs(1,1);
		ll ans=0;
//		for(int i=1;i<=n;i++)
//			cout<<f[i][0]<<' '<<f[i][1]<<endl;
//		cout<<endl;
		if(vis[1]==0)ans=min(f[1][0],f[1][1]);
		else if(vis[1]==1)ans=f[1][1];
		else ans=f[1][0];
		if(ans>=oo)puts("-1");
		else writeln(ans);
		vis[a]=0;vis[b]=0;
	}
	return 0;
}
/*
10 10 C3
57306 99217 65626 23866 84701 6623 7241 88154 33959 17847
2 1
3 1
4 1
5 2
6 3
7 5
8 7
9 6
10 7
4 1 9 1
3 1 2 1
3 1 4 1
3 0 10 0
6 0 9 0
3 1 7 1
7 0 10 1
5 1 1 1
10 1 4 0
1 0 4 0

*/

