#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
int n,m;
const int N=5005;
int head[N],nxt[2*N],tail[2*N],t=1;
int inh[N];
bool bb;
int vist[2*N],vis[N],huan;
inline void addto(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
void dfsh(int k,int fa)
{
	vis[k]=true;
	for(int i=head[k];i;i=nxt[i]){
		if(bb)break;
		if(vist[i])continue;
		int v=tail[i];
		vist[i]=1;vist[i^1]=1;
		if(vis[v]){
			huan=v;
			inh[k]=true;
			bb=true;
			return;
		}
		dfsh(v,k);
	}
	if(huan==k){
		inh[k]=true;
		bb=false;
		return;
	}else if(bb){
		inh[k]=true;
		return;		
	}
}
#define pa pair<int,int> 
#define mk make_pair
#define fi first
#define se second
int inq[N];
bool ab=false;
void dfs(int k)
{
	inq[k]=false;
	priority_queue<pa,vector<pa>,greater<pa> >q;
	for(int i=head[k];i;i=nxt[i]){
		if(vist[i])continue;
		int v=tail[i];
		if(vis[v])continue;
		inq[v]=true;
		q.push(mk(v,i));
	}
	while(!q.empty()){
		pa tmp=q.top();
		q.pop();
		int v=tmp.fi;
		if(q.empty()&&inh[v]&&!ab){
			bool abc=true;
			for(int i=1;i<v;i++)
				if(inq[i]&&inh[i])
					abc=false;
			if(!abc)ab=true;
			if(!abc)continue;
		}
		write(v),putchar(' ');
		vis[v]=true;
		int i=tmp.se;
		vist[i]=true,vist[i^1]=true;
		dfs(v);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		int x,y;
		x=read(),y=read();
		addto(x,y);
		addto(y,x);
	}
	if(m==n){
		dfsh(1,1);
		memset(vist,0,sizeof(vist));
		memset(vis,0,sizeof(vis));
		write(1),putchar(' ');
		dfs(1);
	}else{
		write(1),putchar(' ');
		dfs(1);
	}
}
