#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=8;
const int M=(1<<N)-1;
int n,m,l;
ll a[M][M];
const int mod=1e9+7;
inline ll qp(ll x,ll y)
{
	ll ret=1;
	while(y){
		if(y&1)ret=ret*x%mod;
		x=x*x%mod;
		y/=2;
	}
	return ret;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	ll ans=0;
	n=read();m=read();if(n>m)swap(n,m);
	a[1][1]=2;a[2][2]=12;a[3][3]=112;
	if(n<2)ans=a[n][n]*qp(2,m-n)%mod;
	else ans=a[n][n]*qp(3,m-n)%mod;
	writeln(ans);
}
