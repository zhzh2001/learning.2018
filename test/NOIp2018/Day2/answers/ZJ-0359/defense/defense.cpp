#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=100005;
const ll inf=(1ll<<40);
#define P pair<ll,ll>
#define fir first
#define sec second
ll f[N][2];
char s[10];
int n,m,tp,p[N],a,b,A,B,head[N],cnt,u,v,fa[N];
P Ans;
struct node{int to,next;}num[N<<1];
void add(int x,int y)
{num[++cnt].to=y;num[cnt].next=head[x];head[x]=cnt;}
int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while ('0'<=ch&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*f;
}
void dp(int x)
{
	f[x][0]=f[x][1]=0;
	for (int i=head[x];i;i=num[i].next)
	  if (num[i].to!=fa[x])
	  {
	  	  fa[num[i].to]=x;
	  	  dp(num[i].to);
	  	  f[x][0]+=f[num[i].to][1];f[x][0]=min(f[x][0],inf);
	  	  f[x][1]+=min(f[num[i].to][0],f[num[i].to][1]);f[x][1]=min(f[x][1],inf);
	  }
	f[x][1]+=p[x];f[x][1]=min(f[x][1],inf);
	if (x==a) f[x][A^1]=inf;
	if (x==b) f[x][B^1]=inf;
}
void update(int x,int son,P pre)
{
   P now=P(f[x][0],f[x][1]);
   now.fir-=f[son][1];now.sec-=min(f[son][0],f[son][1]);
   now.fir+=pre.sec;now.sec+=min(pre.fir,pre.sec);
   if (x!=1) update(fa[x],x,now);else Ans=now;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	
	scanf("%d%d%s",&n,&m,s);
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++) scanf("%d%d",&u,&v),add(u,v),add(v,u);
	if (n<=2000&&m<=2000)
	{
		while (m--)
		{
			scanf("%d%d%d%d",&a,&A,&b,&B);
			dp(1);
			ll ans=min(f[1][1],f[1][0]);
			if (ans==inf) puts("-1");else printf("%lld\n",ans);
		}
		return 0;	
	}
	if (s[0]=='B')
	{
		dp(1);
		while (m--)
		{
			scanf("%d%d%d%d",&a,&A,&b,&B);
			if (B==0) update(fa[b],b,P(f[b][0],inf));else update(fa[b],b,P(inf,f[b][1]));
			if (Ans.sec==inf) puts("-1");else printf("%lld\n",Ans.sec);
		}
		return 0;
	}
	puts("good luck");
	return 0;
}
