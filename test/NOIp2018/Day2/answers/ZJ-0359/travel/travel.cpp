#include<bits/stdc++.h>
using namespace std;
const int N=5005;
vector<int> vec[N];
int tot,top,ans[N],Ans[N],n,m,u,v,dep[N],Fa[N],ban1,ban2,q[N];
void dfs(int x,int fa)
{
	ans[++tot]=x;
    for (int i=0;i<vec[x].size();i++)
      if (vec[x][i]!=fa){
      	if (vec[x][i]==ban1&&x==ban2||vec[x][i]==ban2&&x==ban1) continue;
	  	dfs(vec[x][i],x);
	  }
}
void find_cir(int x,int fa)
{
	for (int i=0;i<vec[x].size();i++)
	  if (vec[x][i]!=fa)
	  {
	  	  if (!dep[vec[x][i]]) dep[vec[x][i]]=dep[x]+1,Fa[vec[x][i]]=x,find_cir(vec[x][i],x);
	  	  else if (dep[vec[x][i]]<dep[x]){
	  	  	  int v=x;
	  	  	  while (v!=vec[x][i]) q[++top]=v,v=Fa[v];
	  	  	  q[++top]=vec[x][i];
	  	      return;	
		  }
	  }
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++) scanf("%d%d",&u,&v),vec[u].push_back(v),vec[v].push_back(u);
	for (int i=1;i<=n;i++) sort(vec[i].begin(),vec[i].end());
	if (m==n-1)
	{
		dfs(1,-1); 
		for (int i=1;i<n;i++) printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
		return 0;
	}else{
		for (int i=1;i<=n;i++) Ans[i]=n+1;
		dep[1]=1;find_cir(1,-1);
		for (int i=1;i<=top;i++)
		{
			ban1=q[i];ban2=q[i%top+1];
			tot=0;dfs(1,-1);
			for (int j=1;j<=n;j++)
			  if (ans[j]<Ans[j]){memcpy(Ans,ans,sizeof(ans));break;}
			  else if (ans[j]>Ans[j]) break;
		}
		for (int i=1;i<n;i++) printf("%d ",Ans[i]);
		printf("%d\n",Ans[n]);
	}
	return 0;
}
