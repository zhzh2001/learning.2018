#include<bits/stdc++.h>
#define P pair<int,int>
#define fir first
#define sec second
using namespace std;
typedef long long ll;
const int mod=1e9+7;
const int N=1000010;
int n,m,ans,x,y,f[2][2],now,top,lst;
P st[N],q[N];
int ksm(int x,int y)
{
	int res=1;
	while (y)
	{
		if (y&1) res=(ll)res*x%mod;
		x=(ll)x*x%mod;y>>=1;
	}
	return res;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++) st[++top]=P(i,1);
	for (int i=2;i<=m;i++) st[++top]=P(n,i);
	if (n==1) return printf("%d\n",ksm(2,m)),0;
	if (n==2) return printf("%d\n",(ll)4*ksm(3,m-1)%mod),0;
	f[0][0]=1;
	for (int i=1;i<=top;i++){
		x=st[i].fir,y=st[i].sec;
		lst=now;now^=1;memset(f[now],0,sizeof(f[now]));
		int _x=x,_y=y,tot=0;
		while (_x>=1&&_y<=m) q[++tot]=P(_x--,_y++);
		if (tot==1) f[now][0]=(ll)f[lst][0]*2%mod,f[now][1]=(ll)f[lst][1]*2%mod;
		if (tot==2) f[now][0]=(ll)f[lst][0]*2%mod,f[now][1]=((ll)f[lst][0]+(ll)f[lst][1]*2%mod)%mod;
		if (tot==3)
		{
			f[now][0]=(ll)f[lst][0]*3%mod;
			f[now][1]=((ll)f[lst][0]+f[lst][1]*3%mod)%mod;
		}
	}
	printf("%d\n",((ll)f[now][0]+f[now][1])%mod);
	return 0;
}
