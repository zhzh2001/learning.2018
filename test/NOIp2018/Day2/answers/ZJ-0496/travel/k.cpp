#include<cstring>
#include<cstdio>
#include<cmath>
#include<cassert>
#include<iostream>
#include<algorithm>
#include<vector>
#include<queue>

using namespace std;

#define Oops() cout<<"!!!!!!!"<<endl
#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>"<<endl
#define Divher() cout<<"<<<<<<"<<endl

template<typename T> bool GetMax(T &a,T b)
{
	return (a<b)?(a=b,true):false;
}

template<typename T> bool GetMin(T &a,T b)
{
	return (a>b)?(a=b,true):false;
}

/*
	 Great
 */

const int N=5200;

int n,m;
vector<int> G[N];

namespace Tree
{

	void Song(int u,int fa=0)
	{
		printf("%d ",u);
		for(int i=0,v;i<(int)G[u].size();++i)if((v=G[u][i])!=fa)
			Song(v,u);
	}

}

int cnt[N];
bool vis[N];

vector<int> ans,nw;

void Dfs(int u)
{
	if(!cnt[u])
		return;
	--cnt[u];
	if(!vis[u])
	{
		nw.push_back(u);
		vis[u]=true;
	}
	if(cnt[u])
		return;
	for(int i=0;i<(int)G[u].size();++i)
		Dfs(G[u][i]);
}

int main()
{
	scanf("%d%d",&n,&m);
	for(int i=1,u,v;i<=m;++i)
	{
		scanf("%d%d",&u,&v);
		G[u].push_back(v);
		G[v].push_back(u);
	}
	for(int i=1;i<=n;++i)
		sort(G[i].begin(),G[i].end());
	if(m+1==n)
	{
		Tree::Song(1);
		return 0;
	}
	if(n==1)
	{
		puts("1");
		return 0;
	}
	ans.push_back(n+1);
	for(int i=2;i<=n+1;++i)
	{
		for(int j=1;j<=n;++j)
		{
			cnt[j]=1;
			vis[j]=false;
		}
		cnt[i]=2;
		nw.clear();
		Dfs(1);
		if(nw.size()==n)
			GetMin(ans,nw);
	}
	for(int i=0;i<n;++i)
		printf("%d ",ans[i]);
	return 0;
}
