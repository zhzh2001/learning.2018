#include<cstring>
#include<cstdio>
#include<cmath>
#include<cassert>
#include<iostream>
#include<algorithm>
#include<set>

using namespace std;

#define Oops() cout<<"!!!!!!!"<<endl
#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>"<<endl
#define Divher() cout<<"<<<<<<"<<endl

template<typename T> bool GetMax(T &a,T b)
{
	return (a<b)?(a=b,true):false;
}

template<typename T> bool GetMin(T &a,T b)
{
	return (a>b)?(a=b,true):false;
}

/*
	 Great
 */

#define int long long

const int N=1e5+10;
const int INF=0x3f3f3f3f3f3f3f3f;

int n,m,p[N];
int fir[N],nxt[N<<1],to[N<<1],tote;

set<int> G[N];

void Adde(int u,int v)
{
	to[++tote]=v;
	nxt[tote]=fir[u];
	fir[u]=tote;
	to[++tote]=u;
	nxt[tote]=fir[v];
	fir[v]=tote;
	G[u].insert(v);
	G[v].insert(u);
}

int st[N],dp[N][2],res[N][2];

void Song(int u,int fa=0)
{
	int &x=dp[u][0],&y=dp[u][1];
	x=y=0;
	for(int i=fir[u],v;i;i=nxt[i])if((v=to[i])!=fa)
	{
		x+=dp[v][1];
		y+=min(dp[v][0],dp[v][1]);
	}
	y+=p[u];
}

void Dfs(int u,int fa=0)
{
	for(int i=fir[u],v;i;i=nxt[i])if((v=to[i])!=fa)
		Dfs(v,u);
	Song(u,fa);
	if(~st[u])
		dp[u][!st[u]]=INF;
}

namespace Solver1
{

	void Solve()
	{
		for(int i=1,a,x,b,y;i<=m;++i)
		{
			scanf("%lld%lld%lld%lld",&a,&x,&b,&y);
			if(x==0&&y==0&&G[a].count(b))
			{
				puts("-1");
				continue;
			}
			st[a]=x;
			st[b]=y;
			Dfs(1);
			printf("%lld\n",min(dp[1][0],dp[1][1]));
			st[a]=-1;
			st[b]=-1;
		}
	}

}

namespace Solver2
{

	void Dfs2(int u,int fa=0)
	{
		res[u][0]=dp[u][0];
		res[u][1]=dp[u][1];
		for(int i=fir[u],v;i;i=nxt[i])if((v=to[i])!=fa)
		{
			Song(u,v);
			Song(v);
			Dfs2(v,u);
			Song(v,u);
			Song(u);
		}
	}

	void Solve()
	{
		Dfs(1);
		Dfs2(1);
		for(int i=1,a,x,b,y;i<=m;++i)
		{
			scanf("%lld%lld%lld%lld",&a,&x,&b,&y);
			if(x==0&&y==0&&G[a].count(b))
			{
				puts("-1");
				continue;
			}
			printf("%lld\n",max(res[a][x],res[b][y]));
		}
	}

}

signed main()
{
	scanf("%lld%lld%*s",&n,&m);
	for(int i=1;i<=n;++i)
		scanf("%lld",p+i);
	for(int i=1,u,v;i<n;++i)
	{
		scanf("%lld%lld",&u,&v);
		Adde(u,v);
	}
	memset(st,-1,sizeof st);
	if(n<=2000&&m<=2000&&0)
		Solver1::Solve();
	else
		Solver2::Solve();
	return 0;
}
