#include<cstring>
#include<cstdio>
#include<cmath>
#include<cassert>
#include<iostream>
#include<algorithm>
#include<vector>
#include<queue>

using namespace std;

#define Oops() cout<<"!!!!!!!"<<endl
#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>"<<endl
#define Divher() cout<<"<<<<<<"<<endl

template<typename T> bool GetMax(T &a,T b)
{
	return (a<b)?(a=b,true):false;
}

template<typename T> bool GetMin(T &a,T b)
{
	return (a>b)?(a=b,true):false;
}

/*
	 Great
 */

const int N=5200;

int n,m;
vector<int> G[N];

namespace Tree
{

	void Song(int u,int fa=0)
	{
		printf("%d ",u);
		for(int i=0,v;i<(int)G[u].size();++i)if((v=G[u][i])!=fa)
			Song(v,u);
	}

}

int from[N],to[N];
bool vis[N];

vector<int> ans,nw;

int ban;

void Dfs(int u)
{
	vis[u]=true;
	nw.push_back(u);
	for(int i=0,v;i<(int)G[u].size();++i)if(!vis[v=G[u][i]])
	{
		if((u==from[ban]&&v==to[ban])||(v==from[ban]&&u==to[ban]))
			continue;
		Dfs(v);
	}
}

int main()
{
	scanf("%d%d",&n,&m);
	for(int i=1,u,v;i<=m;++i)
	{
		scanf("%d%d",&u,&v);
		G[u].push_back(v);
		G[v].push_back(u);
		from[i]=u;
		to[i]=v;
	}
	for(int i=1;i<=n;++i)
		sort(G[i].begin(),G[i].end());
	if(m+1==n)
	{
		Tree::Song(1);
		return 0;
	}
	if(n==1)
	{
		puts("1");
		return 0;
	}
	ans.push_back(n+1);
	for(ban=1;ban<=m;++ban)
	{
		memset(vis,0,sizeof vis);
		nw.clear();
		Dfs(1);
		if(nw.size()==n)
			GetMin(ans,nw);
	}
	for(int i=0;i<n;++i)
		printf("%d ",ans[i]);
	return 0;
}
