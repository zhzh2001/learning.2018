#include<bits/stdc++.h>
#include<vector>
using namespace std;
int n,m,kk,a[100005],b[100005],x,aa,y,bb;
char ch;
vector<int>s[100005];
long long ans,total;
inline void readl(int &x){
	char c=getchar();
	int f=1;
	x=0;
	while((c>'9' || c<'0')&& c!='-')
		c=getchar();
	if(c=='-')
		c=getchar();
	while(c>='0' && c<='9')
		x=x*10+c-48,c=getchar();
	x*=f;
}
inline void dfs(int place,int dd,long long tot,int sum){
	if(dd==0){
		if(place<1){
			total=min(total,tot);
			return;
		}
		if(b[place+1]==0){
			b[place]=1;
			dfs(place-1,0,tot+a[place],1);
			return;
		}
		if(sum==2){
			b[place]=0;
			dfs(place-1,0,tot,0);
			return;
		}
		for(int i=0;i<=1;++i){
			b[place]=i;
			dfs(place-1,0,tot+i*a[place],max(sum-1+i*2,0));
		}
	}
	if(dd==1){
		if(place>n){
			total=min(total,tot);
			return;
		}
		if(b[place-1]==0){
			b[place]=1;
			dfs(place+1,1,tot+a[place],1);
			return;
		}
		if(sum==2){
			b[place]=0;
			dfs(place+1,1,tot,0);
			return;
		}
		for(int i=0;i<=1;++i){
			b[place]=i;
			dfs(place+1,1,tot+i*a[place],max(sum-1+i*2,0));
		}
	}
}
inline void dfss(int place,int dd,long long tot,int sum){
	if(dd==0){
		if(place<1){
			total=min(total,tot);
			return;
		}
		if(place==1){
			b[place]=1;
			dfs(place-1,0,tot+a[place],1);
			return;
		}
		if(place==2){
			if(b[3]==1){
				b[2]=0;
				dfs(place-1,0,tot,0);
				return;
			}
			if(b[3]==0){
				b[2]=1;
				dfs(1,0,tot+a[2],1);
				return;
			}
		}
		if(b[place+1]==0){
			b[place]=1;
			dfs(place-1,0,tot+a[place],1);
			return;
		}
		if(sum==2){
			b[place]=0;
			dfs(place-1,0,tot,0);
			return;
		}
		for(int i=0;i<=1;++i){
			b[place]=i;
			dfs(place-1,0,tot+i*a[place],max(sum-1+i*2,0));
		}
	}
	if(dd==1){
		if(place>n){
			total=min(total,tot);
			return;
		}
		if(b[place-1]==0){
			b[place]=1;
			dfs(place+1,1,tot+a[place],1);
			return;
		}
		if(sum==2){
			b[place]=0;
			dfs(place+1,1,tot,0);
			return;
		}
		for(int i=0;i<=1;++i){
			b[place]=i;
			dfs(place+1,1,tot+i*a[place],max(sum-1+i*2,0));
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	readl(n);
	readl(m);
	while(ch<'A' || ch>'C')
		ch=getchar();
	readl(kk);
	for(int i=1;i<=n;++i)
		readl(a[i]);
	int ll,rr;
	for(int i=1;i<n;++i){
		readl(ll);
		readl(rr);
		s[ll].push_back(rr);
		s[rr].push_back(ll);
	}
	if(ch=='A'){
		if(kk==2 || kk==3){
			for(int kkk=1;kkk<=m;++kkk){
				readl(aa);
				readl(x);
				readl(bb);
				readl(y);
				if(x==0 && y==0){
					puts("-1");
					continue;
				}
				ans=0;
				total=1e9;
				memset(b,0,sizeof(b));
				if(x==0)
					dfs(min(aa,bb)-1,0,0,0);
				else
					b[min(aa,bb)]=1,dfs(min(aa,bb)-1,0,0,1);
				ans+=total;
				total=1e9;
				if(y==0)
					dfs(max(aa,bb)+1,1,0,0);
				else
					b[max(aa,bb)]=1,dfs(max(aa,bb)+1,1,0,1);
				ans+=total;
				if(x==1)
					ans+=a[aa];
				if(y==1)
					ans+=a[bb];
				printf("%lld\n",ans);
			}
		}
		if(kk==1){
			for(int kkk=1;kkk<=m;++kkk){
				readl(aa);
				readl(x);
				readl(bb);
				readl(y);
				ans=0;
				memset(b,0,sizeof(b));
				b[1]=1;
				total=1e9;
				if(y==1)
					b[bb]=1;
				if(y==0)
					dfss(bb+1,1,0,0);
				else
					dfss(bb+1,1,0,1);
				ans+=total;
				if(bb==2){
					ans+=a[1];
					ans+=a[bb]*y;
					printf("%lld\n",ans);
					continue;
				}
				if(bb==3 && y==0){
					ans+=a[2]+a[1];
					ans+=a[bb]*y;
					printf("%lld\n",ans);
					continue;
				}
				total=1e9;
				if(y==0)
					dfss(bb-1,0,0,0);
				else
					dfss(bb-1,0,0,1);
				ans+=total;
				ans+=a[bb]*y;
				printf("%lld\n",ans);
			}
		}
	}
	return 0;
}
