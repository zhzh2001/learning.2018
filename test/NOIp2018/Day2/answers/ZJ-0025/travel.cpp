#include<bits/stdc++.h>
#include<queue>
#include<vector>
using namespace std;
int n,m,_hash[5005],root[5005],now;
priority_queue<int,vector<int>,greater<int> >q[5005];
vector<int>s[5005];
inline void readl(int &x){
	char c=getchar();
	int f=1;
	x=0;
	while((c>'9' || c<'0')&& c!='-')
		c=getchar();
	if(c=='-')
		c=getchar();
	while(c>='0' && c<='9')
		x=x*10+c-48,c=getchar();
	x*=f;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	readl(n);
	readl(m);
	int ll,rr;
	for(int i=1;i<=m;++i){
		readl(ll);
		readl(rr);
		s[ll].push_back(rr);
		s[rr].push_back(ll);
	}
	root[1]=1;
	_hash[1]=1;
	for(int i=0;i<s[1].size();++i)
		q[1].push(s[1][i]);
	now=1;
	printf("1");
	while(!q[root[now]].empty() && now>=1){
		int head=q[root[now]].top();
		_hash[head]=1;
		q[root[now]].pop();
		printf(" %d",head);
		for(int i=0;i<s[head].size();++i)
			if(!_hash[s[head][i]])
				q[head].push(s[head][i]);
		if(!q[head].empty())
			root[++now]=head;
		else
			while(q[root[now]].empty() && now>=1)
				--now;
	}
	return 0;
}
