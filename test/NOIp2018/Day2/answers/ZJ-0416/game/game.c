#include <stdio.h>
#define MOD 1000000007

long int pw(int a,int p);
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
    int n,m;
    scanf("%d %d",&n,&m);
    if(n==2){
        printf("%ld",((4*pw(3,m-1))%MOD));
    }else if(n==1){
    	printf("%ld",pw(2,m)%MOD);
	}
	return 0;
}

long int pw(int a,int p){
	long int ans=1;
	int i=0;
	for(i=0;i<p;i++){
		ans=(ans*a)%MOD;
	}
	return ans;
}
