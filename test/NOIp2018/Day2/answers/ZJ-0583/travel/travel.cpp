#include<bits/stdc++.h>
#define rep(q,a,b) for(int q(a),q##_end_(b);q<=q##_end_;++q)
#define dep(q,a,b) for(int q(a),q##_end_(b);q>=q##_end_;--q)
#define mem(a,b) memset(a,b,sizeof a )
using namespace std;
void in(int &r) {
	char c;
	r=0;
	while(c=getchar(),!isdigit(c));
	do r=(r<<1)+(r<<3)+(c^48);
	while(c=getchar(),isdigit(c));
}
const int mn=5005;
int n,m;
vector<int> edge[mn];
namespace Tree {
	void ans_dfs(int f,int x) {
		printf("%d ",x);
		rep(q,0,(int)edge[x].size()-1)if(edge[x][q]!=f)ans_dfs(x,edge[x][q]);
	}
	int main() {
		ans_dfs(0,1);
		return 0;
	}
}
bool mark[mn],is_fu[mn];
namespace fu_tree {
	int at,isok;
	int unab,unat;
	void find_dfs(int f,int x) {
		if(mark[x]) {
			isok=1,at=x;
			return;
		}
		mark[x]=1;
		rep(q,0,(int)edge[x].size()-1)if(edge[x][q]!=f) {
			find_dfs(x,edge[x][q]);
			if(isok) {
				is_fu[x]=1;
				if(x==at)isok=0;
				return;
			}
		}
		mark[x]=0;
	}

	int rt;
	void find_fu(int f,int x) {
		if(is_fu[x]) {
			rt=x;
			return;
		}
		rep(q,0,(int)edge[x].size()-1)if(edge[x][q]!=f)find_fu(x,edge[x][q]);
	}
	bool fi;
	int cmp_val;
	void get_fu(int f,int x) {
		if(fi) {
			rep(q,0,(int)edge[x].size()-1)if(is_fu[edge[x][q]]) {
				cmp_val=edge[x][q];
			}
			fi=0;
			rep(q,0,(int)edge[x].size()-1)if(is_fu[edge[x][q]]) {
				get_fu(x,edge[x][q]);
				return;
			}
			
			return;
		}
		rep(q,0,(int)edge[x].size()-1)if(edge[x][q]!=f&is_fu[edge[x][q]]) {
			if(edge[x][q]>=cmp_val)unab=x,unat=edge[x][q];
			else get_fu(x,edge[x][q]);
		}
	}
	void solve(int f,int x) {
		printf("%d ",x);
		rep(q,0,(int)edge[x].size()-1)if(edge[x][q]!=f&&(x!=unat||edge[x][q]!=unab)&&(x!=unab||edge[x][q]!=unat))solve(x,edge[x][q]);
	}
	int main() {
		isok=0,fi=1;
		find_dfs(0,1);
		find_fu(0,1);
		get_fu(0,rt);
		solve(0,1);
		return 0;
	}
}
int main() {
    freopen("travel.in","r",stdin);
    freopen("travel.out","w",stdout);
	int a,b;
	in(n),in(m);
	rep(q,1,m)in(a),in(b),edge[a].push_back(b),edge[b].push_back(a);
	rep(q,1,n)sort(edge[q].begin(),edge[q].end());
	if(m==n-1)Tree::main();
	else fu_tree::main();
	return 0;
}
