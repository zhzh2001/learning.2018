#include<bits/stdc++.h>
#define rep(q,a,b) for(int q(a),q##_end_(b);q<=q##_end_;++q)
#define dep(q,a,b) for(int q(a),q##_end_(b);q>=q##_end_;--q)
#define mem(a,b) memset(a,b,sizeof a )
using namespace std;
void in(int &r) {
	char c;
	r=0;
	while(c=getchar(),!isdigit(c));
	do r=(r<<1)+(r<<3)+(c^48);
	while(c=getchar(),isdigit(c));
}
int dp[2][9][9][3],n,m;
const int INF=1e9+7;
int main() {
    freopen("game.in","r",stdin);
    freopen("game.out","w",stdout);
	cin>>n>>m;
	if(n==1) {
		long long mul=1;
		rep(q,1,m)mul=mul*2%INF;
		printf("%lld\n",mul);
	} else if(n==2) {
		if(m==1)puts("4");
		else {
			long long now=1;
			rep(q,1,m-1)now=now*3%INF;
			printf("%lld\n",now*4%INF);
		}
	} else if(n==3&&m==1)puts("8");
	else if(n==3&&m==2)puts("36");
	else if(n==3&&m==3)puts("112");
	else {
		bool ok=1;
		dp[ok][0][0][0]=1;
		dp[ok][1][0][0]=1;
		rep(q,1,m-1) {
			ok=!ok;
			mem(dp[ok],0);
			rep(w,0,min(n,q)){
			    rep(e,0,min(q,n)){
				    rep(r,0,3){
					    dp[ok][w][e][r]=dp[!ok][w][e][r];
					}
				}
			}
		}
		cout<<dp[ok][1][0][0]+dp[ok][0][0][0]<<endl;
	}
	return 0;
}
