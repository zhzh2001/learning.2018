#include<bits/stdc++.h>
#define rep(q,a,b) for(int q(a),q##_end_(b);q<=q##_end_;++q)
#define dep(q,a,b) for(int q(a),q##_end_(b);q>=q##_end_;--q)
#define mem(a,b) memset(a,b,sizeof a )
using namespace std;
void in(int &r) {
	char c;
	r=0;
	while(c=getchar(),!isdigit(c));
	do r=(r<<1)+(r<<3)+(c^48);
	while(c=getchar(),isdigit(c));
}
const int mn=100005;
#define link(a,b) link_edge(a,b),link_edge(b,a)
#define link_edge(a,b) to[++cnt1]=b,ne[cnt1]=head[a],head[a]=cnt1
#define travel(x) for(int q(head[x]);q;q=ne[q])
int n,m,head[mn],ne[mn<<1],to[mn<<1],cnt1;
string ty;
int val[mn];
namespace p44 {
	int a,a_ty,b,b_ty;
	long long dp[2005][2];
	const long long INF=1e15;
	void dfs(int f,int x) {
		dp[x][0]=0,dp[x][1]=val[x];
		travel(x)if(to[q]!=f) {
			dfs(x,to[q]);
			dp[x][0]+=dp[to[q]][1];
			dp[x][1]+=min(dp[to[q]][1],dp[to[q]][0]);
		}
		if(x==a) {
			if(a_ty==1)dp[x][0]=INF;
			else dp[x][1]=INF;
		}
		if(x==b) {
			if(b_ty==1)dp[x][0]=INF;
			else dp[x][1]=INF;
		}
	}
	int main() {
		rep(q,1,m) {
			in(a),in(a_ty),in(b),in(b_ty);
			dfs(0,1);
			long long v=min(dp[1][0],dp[1][1]);
			if(v>=INF)puts("-1");
			else printf("%lld\n",v);
		}
		return 0;
	}
}
//namespace p24 {
//	long long now[mn][19][2][2];
//	long long Min(long long a,long long b,long long c) {
//		return min(c,min(a,b));
//	}
//	int a,b,a_ty,b_ty;
//	void init() {
//		rep(q,1,n) {
//			now[q][0][1][1]=val[q];
//			now[q][0][0][1]=1e15;
//			now[q][0][1][0]=1e15;
//		}
//		rep(q,1,18) {
//			rep(w,1,n-(1<<q)+1) {
//				now[w][q][1][1]=Min(now[w][q-1][1][1]+now[w+(1<<q)][q-1][0][1],now[w][q-1][1][1]+now[w+(1<<q)][q-1][1][1],now[w][q-1][1][0]+now[w+(1<<q)][q-1][1][1]);
//				now[w][q][0][1]=Min(now[w][q-1][0][1]+now[w+(1<<q)][q-1][0][1],now[w][q-1][0][1]+now[w+(1<<q)][q-1][1][1],now[w][q-1][0][0]+now[w+(1<<q)][q-1][1][1]);
//				now[w][q][1][0]=Min(now[w][q-1][1][1]+now[w+(1<<q)][q-1][0][0],now[w][q-1][1][1]+now[w+(1<<q)][q-1][1][0],now[w][q-1][1][0]+now[w+(1<<q)][q-1][1][0]);
//			    now[w][q][0][0]=Min(now[w][q-1][0][1]+now[w+(1<<q)][q-1][1][0],now[w][q-1][0][1]+now[w+(1<<q)][q-1][0][0],now[w][q-1][0][0]+now[w+(1<<q)][q-1][1][0]);
//			}
//		}
//	}
//	long long solve(int l,int r,int lm,int lim){
//	    if(l>r)return 0;
//	    if(lm==-1&&rm==1){
//		    
//		}
//	}
//	int main() {
//		init();
//		rep(q,1,m){
//		    in(a),in(a_ty),in(b),in(b_ty);
//		    if(a>b)swap(a,b),swap(a_ty,b_ty);
//		    if(b==a+1&&a_ty==0&&b_ty==0)puts("-1");
//		    else{
//			     long long ans=0;
//			     if(a_ty==1)ans+=solve(1,a-1,-1,-1)+val[a];
//			     else ans+=solve(1,a-1,-1,1);
//			     if(b_ty==1)ans+=solve(b+1,n,-1,-1)+val[b];
//			     else ans+=solve(b+1,n,1,-1);
//			     if(a_ty==1&&b_ty==1)ans+=solve(a+1,b-1,-1,-1);
//			     else if(a_ty==1&&b_ty==0)ans+=solve(a+1,b-1,-1,1);
//			     else if(b_ty==0)ans+=solve(a+1,b-1,1,1);
//			     else ans+=solve(a+1,b-1,1,1);
//			}
//		}
//		return 0;
//	}
//}
int main() {
    freopen("defense.in","r",stdin);
    freopen("defense.out","w",stdout);
	cin>>n>>m>>ty;
	rep(q,1,n)in(val[q]);
	int a,b;
	rep(q,2,n) {
		in(a),in(b),link(a,b);
	}
//	if(n<=2000)
	p44::main();
//	else if(ty[0]=='A')p24::main();
	return 0;
}
