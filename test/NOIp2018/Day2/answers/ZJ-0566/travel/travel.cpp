#include<cstdio>
#include<vector>
#include<algorithm>

using namespace std;

int n,m;

int h,t;
int top;
int s[5005],ins[5005];

int banu,banv;

vector<int> to[5001];

void add(int u, int v)
{
	++to[u][0];
	to[u].push_back(v);
	++to[v][0];
	to[v].push_back(u);
}

void dfsa(int u, int f)
{
	printf("%d ",u);
	for(int i=1; i<=to[u][0]; ++i)
		if(to[u][i]!=f) dfsa(to[u][i],u);
}

void dfsc(int u, int f)
{
	s[++top]=u;
	ins[u]=top;
	for(int i=1; i<=to[u][0]; ++i)
		if(to[u][i]!=f)
		{
			if(ins[to[u][i]])
			{
				s[++top]=to[u][i];
				h=ins[to[u][i]];
				t=top;
				return;
			}
			else
			{
				dfsc(to[u][i],u);
				if(t) return;
			}
		}
	--top;
	ins[u]=0;
}

int c,d;

struct node{
	int x[5001];
}ans[5001];

inline bool operator < (const node &a, const node &b)
{
	for(int i=1; i<=n; ++i)
		if(a.x[i]<b.x[i]) return true;
		else if(a.x[i]>b.x[i]) return false;
	return false;
}

void dfsb(int u, int f)
{
	ans[c].x[++d]=u;
	for(int i=1; i<=to[u][0]; ++i)
	{
		if(to[u][i]!=f)
		{
			if(!(((to[u][i]==banu)&&(u==banv))||((to[u][i]==banv)&&(u==banu))))
			{
				dfsb(to[u][i],u);
			}
		}
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1; i<=n; ++i) to[i].push_back(0);
	int a,b;
	for(int i=1; i<=m; ++i)
	{
		scanf("%d%d",&a,&b);
		add(a,b);
	}
	for(int i=1; i<=n; ++i) sort(to[i].begin()+1,to[i].end());
	if(m==n-1) dfsa(1,0);
	else
	{
		dfsc(1,0);
		for(int i=h; i<t; ++i)
		{
			banu=s[i];
			banv=s[i+1];
			++c;
			d=0;
			dfsb(1,0);
		}
		node an=ans[1];
		for(int i=2; i<=t-h; ++i)
			if(ans[i]<an) an=ans[i];
		for(int i=1; i<=n; ++i)
			printf("%d ",an.x[i]);
	}
	return 0;
}
