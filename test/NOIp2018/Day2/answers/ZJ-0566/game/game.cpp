#include<cstdio>
#include<cstring>
#include<algorithm>

#define MOD 1000000007LL

using namespace std;

int n,m;
int t,g;
long long f[2][257];
int a=0, b=1;
long long ans;

void dfs(int w, int x)
{
	if(x==n)
	{
		f[b][g]+=f[a][w];
		return;
	}
	if(w&(1<<x)) dfs(w,x+1);
	else
	{
		dfs(w,x+1);
		dfs(w|(1<<x),x+1);
	}
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==1)
	{
		ans=1;
		for(int i=1; i<=m; ++i) ans=ans*2%MOD;
		printf("%lld",ans);
		return 0;
	}
	t=1<<n;
	for(int i=0; i<t; ++i) f[0][i]=1;
	for(g=0; g<t; ++g)
	{
		f[b][g]=0;
		dfs(g>>1,0);
		f[b][g]%=MOD;
	}
	b=0, a=1;
	for(int i=2; i<m; ++i)
	{
		for(g=0; g<t; ++g)
		{
			f[b][g]=f[a][g>>1]+f[a][(g>>1)|(1<<(n-1))];
			if(!(g&(1<<(n-1)))) f[b][g]+=f[a][(g>>1)|(1<<(n-2))]+f[a][(g>>1)|(3<<(n-2))];
			f[b][g]%=MOD;
		}
		if(a==0) a=1,b=0;
		else b=1, a=0;
	}
	for(int i=0; i<t; ++i) ans+=f[a][i];
	ans%=MOD;
	printf("%lld",ans);
	return 0;
}
