program travel;
var
 a:array[0..5050,0..5050]of boolean;
 f:array[0..5050]of boolean;
 order:array[0..5050]of integer;
 n,m,i,cnt,u,v:longint;
procedure dfs(x:longint);
var
 i:longint;
 begin
  inc(cnt);
  order[cnt]:=x;
  for i:=1 to n do
   if f[i] and a[x,i] then
    begin
     f[i]:=false;
     dfs(i);
    end;
 end;
begin
 assign(input,'travel.in');reset(input);
 assign(output,'travel.out');rewrite(output);
 readln(n,m);
 fillchar(a,sizeof(a),false);
 fillchar(f,sizeof(f),true);
 for i:=1 to m do
  begin
   read(u,v);
   a[u,v]:=true;
   a[v,u]:=true;
  end;
 f[1]:=false;
 dfs(1);
 for i:=1 to n do write(order[i],' ');
 close(input);
 close(output);
end.
