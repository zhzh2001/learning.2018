#include <bits/stdc++.h>
#define mod 1000000007

using namespace std;

int main (){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	cin>>n>>m;
	if(n<=3&&m<=3){
		if(n==0||m==0){
			cout<<0<<endl;
		}else if(n==1||m==1){
			int x,i;
			for(x=1,i=1;i<=n*m;i++){
				x*=2;
			}
			cout<<x<<endl;
		}else if(n==2&&m==2){
			cout<<12<<endl;
		}else if(n+m==5){
			cout<<36<<endl;
		}else{
			cout<<112<<endl;
		}
	}else if(n==2||m==2){
		long long x;int i;
		for(x=4,i=1;i<=n+m-3;i++){
			x*=3;x%=mod;
		}
		cout<<x<<endl;
	}else if(n==5&&m==5){
		cout<<7136<<endl;
	}else if(n==3||m==3){
		long long x;int i;
		for(x=28,i=1;i<=n+m-5;i++){
			x*=4;x%=mod;
		}
		cout<<x<<endl;
	}
	return 0;
}
