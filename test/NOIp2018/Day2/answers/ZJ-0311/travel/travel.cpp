#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define rep(i,a,b) for (int i=a;i<=b;i++)
#define per(i,a,b) for (int i=a;i>=b;i--)
#define Rep(i,a,b) for (int i=a;i<b;i++)
#define Per(i,a,b) for (int i=a;i>b;i--)

void read(int&x){
	x=0;char c=getchar();
	while (!isdigit(c))c=getchar();
	while (isdigit(c))x=x*10+c-'0',c=getchar();
}

#define maxn 5005
vector<int>E[maxn];
int n,m;
int cir[maxn];
bool vis[maxn];
int p[maxn];
int ans[maxn],res[maxn];
int findcircle(int x,int f){
	ans[++ans[0]]=x;vis[x]=true;
	Rep(i,0,E[x].size())if (E[x][i]!=f){
		if (!vis[E[x][i]]){p[E[x][i]]=x;findcircle(E[x][i],x);}
		else{
			if (cir[0]>0)continue;
			int y=E[x][i];
			for (int k=x;k!=y;k=p[k])cir[++cir[0]]=k;
			cir[++cir[0]]=y;
		}
	}
}
int dx,dy;

bool check(int x,int y){
	if ((x==dx&&y==dy)||(x==dy&&y==dx))return false;return true;
}
int flag=1;
void findans(int x,int f){
	res[++res[0]]=x;
	if (flag==0){
		if (res[res[0]]>ans[res[0]]){flag=-1;return;}
		else if (res[res[0]]<ans[res[0]])flag=1;
	}
	Rep(i,0,E[x].size())if (E[x][i]!=f&&check(x,E[x][i])){
		findans(E[x][i],x);if (flag==-1)return;
	}
}
void check(){
	bool flag=true;
	rep(i,1,n)if (res[i]!=ans[i]){
		if (res[i]<ans[i])flag=false;break;
	}
	if (!flag)rep(i,1,n)ans[i]=res[i];
}
int main(){
	// int x;read(x);cout<<x<<endl;
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);read(m);
	rep(i,1,m){
		int x,y;read(x);read(y);
		E[x].pb(y);E[y].pb(x);
	}
	rep(i,1,n)sort(E[i].begin(),E[i].end());
	findcircle(1,0);
	if (cir[0]>2){
		rep(i,1,cir[0]){
			dx=cir[i];dy=cir[i%cir[0]+1];res[0]=0;
			flag=0;findans(1,0);// check();
			if (flag==1)rep(k,1,n)ans[k]=res[k];
		}
	}
	rep(i,1,n){
		printf("%d",ans[i]);
		if (i==n)puts("");else putchar(' ');
	}
	return 0;
}
