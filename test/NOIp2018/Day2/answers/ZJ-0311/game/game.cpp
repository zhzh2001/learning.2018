#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define rep(i,a,b) for (int i=a;i<=b;i++)
#define per(i,a,b) for (int i=a;i>=b;i--)
#define Rep(i,a,b) for (int i=a;i<b;i++)
#define Per(i,a,b) for (int i=a;i>b;i--)

void read(int&x){
	x=0;char c=getchar();
	while (!isdigit(c))c=getchar();
	while (isdigit(c))x=x*10+c-'0',c=getchar();
}
/*
#define mod 1000000007
int n,m;
int power(int a,int b){
	int s=1;for (int w=a;b;b>>=1,w=1ll*w*w%mod)if (b&1)s=1ll*s*w%mod;return s;
}
#define maxn 1000005
int c[10];

int p[10][10];
int ans[maxn][3];

void solve(){
	memset(p,0,sizeof(p));
	rep(i,2,n+1){
		rep(j,1,c[i])p[j][i+1-j]=1;
		rep(j,c[i]+1,i)p[i][i+1-j]=0;
	}
	
	rep(i,1,n+1){
		rep(j,1,n+1)printf("%d ",p[i][j]);
		puts("");
	}
	puts("");
	rep(i,1,n+2)rep(j,1,n+2)if (i>n||j>n){
		if (p[i][j]==1)return;
	}
	
	rep(i,1,n-1)rep(j,1,min(n,m)-1)
		if (p[i+1][j]!=p[i][j+1]){
			if (i>=3&&j>=3)return;
			if (i>1&&j>1&&p[i-1][j]==p[i][j-1])return;
		}
	if (m<=n+1)return power(2,m-2);
	else if (p[3][n-1]!=p[2][n])return ans[][0];
	else if (p[2][n]!=p[1][n+1])return ans[][1];
	else return ans[][2];
	rep(i,1,n+1){
		rep(j,1,n+1)printf("%d ",p[i][j]);
		puts("");
	}
	puts("");
}
void work(){
	
}
void dfs(int x){
	if (x>n+1){
		solve();return;
	}
	rep(i,0,x){
		c[x]=i;dfs(x+1);
	}
}
int work(int x){}
*/

#define maxn 1000005
#define mod 1000000007
int f[maxn][10];int n,m;
void add(int&x,int y){(x+=y)%=mod;}
int dfs(int x,int del){
	// printf("%d %d\n",x,del);
	if (f[x][del]!=0)return f[x][del];
	if (x==2)return 2;if (x==3){
		if (del==2)return 1;else return 3;
	}
	int res=0;
	Rep(i,1,n){
		int l=i,r=x-i;
		if (l>=3&&r>=4)continue;if (l>=n||r>m)continue;
		if (del!=0&&l!=del-1)continue;
		//if (l==1||r==2)add(res,dfs(x-1,del-1));
		//else add(res,dfs(x-1,del));
		if (l==1||r==2)add(res,dfs(x-2,0));else add(res,dfs(x-2,l));
		
		// printf("%d %d %d\n",x,del,res);
	}
	if (!del)add(res,dfs(x-2,0)),add(res,dfs(x-2,0));
	f[x][del]=res;
	// printf("%d %d %d\n",x,del,res);
	return res;
}

int zx[1000030];
int main(){
	// int x;read(x);cout<<x<<endl;
	// freopen("game.in","r",stdin);
	// freopen("game.out","w",stdout);
	// read(n);read(m);
	// work();
	// dfs(2);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n);read(m);
	if (n==2&&m==2){printf("%d\n",12);return 0;}
	int ans1=2ll*dfs(n+m-2,0)%mod;
	int ans2=2ll*dfs(n+m-3,0)%mod;
	int l=n-1,r=m;
	if (l<3||r<4){
		// cerr<<"find"<<endl;
		if (l==1||r==2)add(ans2,dfs(n+m-3,0));
		else add(ans2,dfs(n+m-3,l));
	}
	// printf("%d %d\n",ans1,ans2);
	printf("%lld\n",1ll*ans1*ans2%mod);
	/*
	int zxx=0;
	rep(i,2,m)a[i]=3;
	rep(i,m+1,m+n-2)a[i]=2;
	rep(i,3,m+n-2)a[i]=1ll*a[i]*a[i-1]%mod;
	int zxxx=1;
	rep(i,2,m+n-2)add(zxx,a[i])*/
	return 0;
}
