#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define rep(i,a,b) for (int i=a;i<=b;i++)
#define per(i,a,b) for (int i=a;i>=b;i--)
#define Rep(i,a,b) for (int i=a;i<b;i++)
#define Per(i,a,b) for (int i=a;i>b;i--)

void read(int&x){
	x=0;char c=getchar();
	while (!isdigit(c))c=getchar();
	while (isdigit(c))x=x*10+c-'0',c=getchar();
}

#define maxn 2005
int sign[maxn],p[maxn];
int n,m;
vector<int>E[maxn];
#define INF 1e9+7
pi dfs(int x,int f){
	pi res=mp(p[x],0);
	Rep(i,0,E[x].size())if (E[x][i]!=f){
		pi ans=dfs(E[x][i],x);
		res.fi+=min(ans.fi,ans.se);res.se+=ans.fi;
	}
	if (sign[x]==0)res.fi=INF;
	if (sign[x]==1)res.se=INF;
	return res;
}
int main(){
	// int x;read(x);cout<<x<<endl;
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n);read(m);getchar();getchar();getchar();
	rep(i,1,n)read(p[i]);
	Rep(i,1,n){
		int x,y;read(x);read(y);
		// printf("%d %d\n",x,y);
		E[x].pb(y);E[y].pb(x);
	}
	rep(i,1,n)sign[i]=-1;
	int res=0;
	rep(i,1,m){
		int a,b,x,y;read(a);read(x);read(b);read(y);
		sign[a]=x;sign[b]=y;
		if (a==b&&x!=y)printf("%d\n",-1);
		else{
			pi ans=dfs(1,0);
			if (ans.fi>=INF&&ans.se>=INF)printf("%d\n",-1);
			else printf("%d\n",min(ans.fi,ans.se));
		}
		sign[a]=sign[b]=-1;
	}
	return 0;
}
