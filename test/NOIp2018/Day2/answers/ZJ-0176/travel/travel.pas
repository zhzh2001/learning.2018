var
  f:array[0..5001,0..5001] of longint;
  flag:array[0..5005] of boolean;
  n,m,i,ans,len,u,v,min:longint;
  num,a:array[0..5005] of longint;

procedure qsort(l,r,k:longint);
 var p,q,mid,t:longint;
 begin
   if l>=r then exit;
   p:=l;q:=r;mid:=f[k,(l+r) div 2];
   while p<=q do
    begin
     while (f[k,p]<mid) and (p<r) do inc(p);
     while (f[k,q]>mid) and (q>l) do dec(q);
     if p<=q then
      begin
       t:=f[k,p];
       f[k,p]:=f[k,q];
       f[k,q]:=t;
       inc(p);dec(q);
      end;
    end;
   qsort(l,q,k);qsort(p,r,k);
 end;

procedure dfs(k:longint);
 var i:longint;
 begin
   if ans>n then exit;
   a[ans]:=k;flag[k]:=true;
   qsort(1,num[k],k);
   for i:=1 to num[k] do
    if not flag[f[k,i]] then
     begin
       inc(ans);
       dfs(f[k,i]);
     end;
 end;


begin
  assign(input,'travel.in');
  reset(input);
  assign(output,'travel.out');
  rewrite(output);
  readln(n,m);
  min:=maxlongint;
  for i:=1 to m do
   begin
    readln(u,v);
    inc(num[u]);
    f[u,num[u]]:=v;
    inc(num[v]);
    f[v,num[v]]:=u;
    if u<min then min:=u;
    if v<min then min:=v;
   end;
  ans:=1;
  dfs(min);
  for i:=1 to n do write(a[i],' ');
  close(input);
  close(output);
end.