#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define inf 10000000000000000LL
using namespace std;
struct T
{
	ll a[2][2];
}O,v[N*4];
ll n,m,sz[N],top[N],Bson[N],dep[N],st[N],ord[N],cnt,a[N],fa[N];
ll f[N][2],g[N][2],h[N][2];
ll to[N<<1],nxt[N<<1],fst[N],l;
void link(ll x,ll y)
{
	to[++l]=y;nxt[l]=fst[x];fst[x]=l;
	to[++l]=x;nxt[l]=fst[y];fst[y]=l;
}
T operator+(T x,T y)
{
	T z;
	for (ll i=0;i<2;i++)
	for (ll j=0;j<2;j++)
		z.a[i][j]=min(x.a[i][0]+y.a[1][j],min(x.a[i][1]+y.a[0][j],x.a[i][1]+y.a[1][j]));
	return z;
}
T inv(T a)
{
	swap(a.a[0][1],a.a[1][0]);
	return a;
}
void build(ll k,ll l,ll r)
{
	if (l==r)
	{
		v[k]=O;
		v[k].a[0][0]=h[l][0];
		v[k].a[1][1]=h[l][1];
		return;
	}
	ll mid=l+r>>1;
	build(k<<1,l,mid);
	build(k<<1|1,mid+1,r);
	v[k]=v[k<<1]+v[k<<1|1];
}
T qry(ll k,ll l,ll r,ll x,ll y)
{
	if (x<=l&&r<=y) return v[k];
	ll mid=l+r>>1;
	if (x<=mid&&y>mid) return qry(k<<1,l,mid,x,y)+qry(k<<1|1,mid+1,r,x,y);
	if (x>mid) return qry(k<<1|1,mid+1,r,x,y);
	return qry(k<<1,l,mid,x,y);
}
void dfs(ll x)
{
	sz[x]=1;f[x][1]=a[x];
	dep[x]=dep[fa[x]]+1;
	for (ll i=fst[x];i;i=nxt[i])
		if (to[i]!=fa[x])
		{
			fa[to[i]]=x;
			dfs(to[i]);
			sz[x]+=sz[to[i]];
			if (sz[to[i]]>sz[Bson[x]])Bson[x]=to[i];
			f[x][0]+=f[to[i]][1];
			f[x][1]+=min(f[to[i]][0],f[to[i]][1]);
		}
	
}
void dfs(ll x,ll y)
{
//	cout<<x<<"--  "<<f[x][0]<<' '<<f[x][1]<<' '<<g[x][0]<<' '<<g[x][1]<<endl;
	top[x]=y;st[x]=++cnt;ord[cnt]=x;
	if (Bson[x])
	{
		g[Bson[x]][0]=g[x][1]+f[x][0]-f[Bson[x]][1];
		//cout<<g[x][1]+f[x][0]-f[to[i]][1]+a[fa[x]]<<endl;
		g[Bson[x]][1]=min(g[x][1],g[x][0])+f[x][1]-min(f[Bson[x]][0],f[Bson[x]][1]);
		dfs(Bson[x],y);
	}
	h[st[x]][1]=a[x];
	for (ll i=fst[x];i;i=nxt[i])
		if (!top[to[i]])
		{
			g[to[i]][0]=g[x][1]+f[x][0]-f[to[i]][1];
			//cout<<g[x][1]+f[x][0]-f[to[i]][1]+a[fa[x]]<<endl;
			g[to[i]][1]=min(g[x][1],g[x][0])+f[x][1]-min(f[to[i]][0],f[to[i]][1]);
			dfs(to[i],to[i]);
			h[st[x]][0]+=f[to[i]][1];
			h[st[x]][1]+=min(f[to[i]][0],f[to[i]][1]);
		}
}
ll lca(ll x,ll y)
{
	for (;top[x]!=top[y];x=fa[top[x]])
		if (dep[top[x]]<dep[top[y]]) swap(x,y);
	return dep[x]<dep[y]?x:y;
}
T Get(T k,ll x,ll y)
{
	T tmp=k;
	
	if (top[x]==top[y])
	{
		if (st[y]+1<=st[x]-1)
			tmp=tmp+inv(qry(1,1,n,st[y]+1,st[x]-1));
		return tmp;
	}
	else
	{
		ll j;
		for (j=x;top[fa[j]]!=top[y];j=top[fa[j]])
		{
			if (j==top[j])
			{
				T t;
				t.a[0][1]=t.a[1][0]=inf;
				t.a[0][0]=f[fa[j]][0]-f[j][1];/**/
				t.a[1][1]=f[fa[j]][1]-min(f[j][0],f[j][1]);
				tmp=tmp+t;
			}
			if (top[fa[j]]!=fa[j])
				tmp=tmp+inv(qry(1,1,n,st[top[fa[j]]],st[fa[j]]-1));
		}
		if (fa[j]==y)
		{
			
		}
		else
		{
			{
				T t;
				t.a[0][1]=t.a[1][0]=inf;
				t.a[0][0]=f[fa[j]][0]-f[j][1];
				t.a[1][1]=f[fa[j]][1]-min(f[j][0],f[j][1]);
				tmp=tmp+t;
			}
			
			if (st[y]+1<=st[fa[j]]-1)
				tmp=tmp+inv(qry(1,1,n,st[y]+1,st[fa[j]]-1));
		}
	}
	return tmp;
}
ll jump(ll x,ll y)
{
	for (;dep[x]-dep[top[x]]<y;)
	{
		y-=dep[x]-dep[top[x]]+1;
		x=fa[top[x]];
	}
	return ord[st[x]-y];
}
int main()
{
	freopen("defense2.in","r",stdin);
	//freopen("defense.out","w",stdout);
	O.a[0][0]=O.a[1][1]=O.a[1][0]=O.a[0][1]=inf;
	char s[10];
	scanf("%lld%lld%s",&n,&m,s);
	for (ll i=1;i<=n;i++)
		scanf("%lld",&a[i]);
	
	for (ll i=1;i<n;i++)
	{
		ll x,y;
		scanf("%lld%lld",&x,&y);
		link(x,y);
	}
	dfs(1);
	dfs(1,1);
	build(1,1,n);
	for (ll i=1;i<=m;i++)
	{
		ll x,y,X,Y;ll Ans;
		scanf("%lld%lld%lld%lld",&x,&X,&y,&Y);
		ll t=lca(x,y);
		if (t==x) swap(x,y),swap(X,Y);
		if (t==y)
		{
			T tmp=O,tmp1=O;
			
			tmp.a[X][X]=0;
			tmp1.a[Y][Y]=0;
			if (top[x]==top[y])
			{
				if (st[y]+1<=st[x]-1)
					tmp=tmp+inv(qry(1,1,n,st[y]+1,st[x]-1));
				tmp=tmp+tmp1;
				Ans=tmp.a[X][Y]+f[x][X]+g[Bson[y]][Y];
			}
			else
			{
				ll j;
				for (j=x;top[fa[j]]!=top[y];j=top[fa[j]])
				{
					if (j==top[j])
					{
						T t;
						t.a[0][1]=t.a[1][0]=inf;
						t.a[0][0]=f[fa[j]][0]-f[j][1];
						t.a[1][1]=f[fa[j]][1]-min(f[j][0],f[j][1]);
						tmp=tmp+t;
					}
					if (top[fa[j]]!=fa[j])
						tmp=tmp+inv(qry(1,1,n,st[top[fa[j]]],st[fa[j]]-1));
				}
				if (fa[j]==y)
				{
					tmp=tmp+tmp1;
					Ans=tmp.a[X][Y]+f[x][X]+g[j][Y];
				}
				else
				{
					{
						T t;
						t.a[0][1]=t.a[1][0]=inf;
						t.a[0][0]=f[fa[j]][0]-f[j][1];
						t.a[1][1]=f[fa[j]][1]-min(f[j][0],f[j][1]);
						tmp=tmp+t;
					}
					
					if (st[y]+1<=st[fa[j]]-1)
						tmp=tmp+inv(qry(1,1,n,st[y]+1,st[fa[j]]-1));
					tmp=tmp+tmp1;
					Ans=tmp.a[X][Y]+f[x][X]+g[Bson[y]][Y];
				}
			}
			printf("%lld\n",Ans>=inf?-1LL:Ans);
			continue;
		}
		T tmp=O,tmp1=O;
		
		tmp.a[X][X]=0;
		tmp1.a[Y][Y]=0;
		T XX=Get(tmp,x,t);
		T YY=inv(Get(tmp1,y,t));
		T tmp2=O;
		ll xt=jump(x,-dep[t]+dep[x]-1);
		ll yt=jump(y,-dep[t]+dep[y]-1);
		tmp2.a[0][0]=g[t][1]+f[t][0]-f[xt][1]-f[yt][1];
		tmp2.a[1][1]=min(g[t][1],g[t][0])+f[t][1]-min(f[xt][1],f[xt][0])-min(f[yt][1],f[yt][0]);
		tmp=XX+tmp2+YY;
		Ans=tmp.a[X][Y]+f[x][X]+f[y][Y];
		printf("%lld\n",Ans>=inf?-1LL:Ans);
	}
	
}
