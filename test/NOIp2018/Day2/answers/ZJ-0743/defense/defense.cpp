#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 10000000000000000LL
using namespace std;
ll n,m,a[N],fa[N];
ll f[N][2],g[N];
ll to[N<<1],nxt[N<<1],fst[N],l;
void link(ll x,ll y)
{
	to[++l]=y;nxt[l]=fst[x];fst[x]=l;
	to[++l]=x;nxt[l]=fst[y];fst[y]=l;
}
void dfs(ll x)
{
	f[x][1]=a[x];
	f[x][0]=0;
	for (ll i=fst[x];i;i=nxt[i])
		if (to[i]!=fa[x])
		{
			fa[to[i]]=x;
			dfs(to[i]);
			f[x][0]+=f[to[i]][1];
			f[x][1]+=min(f[to[i]][0],f[to[i]][1]);
		}
	if (g[x]) f[x][2-g[x]]=inf;
	//cout<<x<<' '<<f[x][0]<<' '<<f[x][1]<<endl;
}
int main()
{
	
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	
	char s[10];
	scanf("%lld%lld%s",&n,&m,s);
	for (ll i=1;i<=n;i++)
		scanf("%lld",&a[i]);
	
	for (ll i=1;i<n;i++)
	{
		ll x,y;
		scanf("%lld%lld",&x,&y);
		link(x,y);
	}
	for (ll i=1;i<=m;i++)
	{
		ll x,y,X,Y;ll Ans;
		scanf("%lld%lld%lld%lld",&x,&X,&y,&Y);
		g[x]=1+X;
		g[y]=1+Y;
		dfs(1);
		g[x]=g[y]=0;
		printf("%lld\n",min(f[1][0],f[1][1])>=inf?-1:min(f[1][0],f[1][1]));
	}
	
}
