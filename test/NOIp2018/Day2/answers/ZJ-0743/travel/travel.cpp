#include<bits/stdc++.h>
#define N 200005
using namespace std;
int n,m,a[N],b[N],cnt,vis[N],Bx,By,x[N],y[N];
vector<int> v[N];
void dfs(int x)
{
	b[++cnt]=x;
	vis[x]=1;
	for (int i=0;i<v[x].size();i++)
	{
		int to=v[x][i];
		if ((!vis[to])&&(Bx!=x||By!=to)&&(By!=x||Bx!=to))
			dfs(to);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d",&x[i],&y[i]);
		v[x[i]].push_back(y[i]);
		v[y[i]].push_back(x[i]);
	}
	for (int i=1;i<=n;i++)
		sort(v[i].begin(),v[i].end());
	if (m==n-1)
	{
		dfs(1);
		for (int i=1;i<=n;i++)
			a[i]=b[i];
	}
	else
	{
		for (int i=1;i<=m;i++)
		{
			cnt=0;
			for (int j=0;j<=n;j++)
				vis[j]=0;
			Bx=x[i];By=y[i];
			dfs(1);
			if (cnt!=n) continue;
			
			int flag=0;
			for (int j=1;j<=n;j++)
				if (a[j]!=b[j])
				{
					flag=a[j]>b[j];
					break;
				}
			if (flag||a[1]==0)
				for (int j=1;j<=n;j++)
					a[j]=b[j];
		}
	}
	for (int i=1;i<=n;i++)
		printf(i==n?"%d\n":"%d ",a[i]);
}
