#include <bits/stdc++.h>
using namespace std;

const int INF = 1e9;
const int N = 3e5;
int n, m;
vector <int> bi[N];
int pi[N];
char st[10];
struct data 
{
	int d[2];
	data () {for (int i = 0; i < 2; ++ i) d[i] = INF;}
};
void chkmin(int &t, int d)
{
	t = min(t, d);
}
data add_to(data a, data b)
{
	data c;
	for (int i = 0; i < 2; ++ i)
		for (int j = 0; j < 2; ++ j)
			if (i || j) chkmin(c.d[j], a.d[i] + b.d[j]);
	return c;
}
data init(int id, int t)
{
	data c;
	if (t == -1 || t == 1) c.d[1] = pi[id];
	if (t == -1 || t == 0) c.d[0] = 0;
	return c;
}
int A, X, B, Y;
data dfs(int t, int f = 0)
{
	data w;
	if (t == A)
		w = init(t, X);
	else if (t == B)
		w = init(t, Y);
	else 
		w = init(t, -1);
	for (int i = 0; i < bi[t].size(); ++ i)
		if (bi[t][i] != f)
			w = add_to(dfs(bi[t][i], t), w);
	return w;
}

struct pack
{
	int d[2][2];
	pack () {for (int i = 0; i < 2; ++ i) for (int j = 0; j < 2; ++ j) d[i][j] = INF;}
};
int lc[N], rc[N], lb[N], rb[N];
pack val[N];
pack add(pack a, pack b)
{
	pack c;
	for (int i = 0; i < 2; ++ i)
		for (int j = 0; j < 2; ++ j)
			for (int k = 0; k < 2; ++ k) if (j || k)
				for (int l = 0; l < 2; ++ l)
					c.d[i][l] = min(c.d[i][l], a.d[i][j] + b.d[k][l]);		
	return c;
}
int build(int l, int r)
{
	static int tot = 0;
	int t = ++ tot;
	lb[t] = l; rb[t] = r;
	if (l < r) 
	{
		lc[t] = build(l, (l + r) / 2);
		rc[t] = build((l + r) / 2 + 1, r);
		val[t] = add(val[lc[t]], val[rc[t]]);
	}
	else 
	{
		val[t].d[0][0] = 0;
		val[t].d[1][1] = pi[l];
	}
	return t;
}
pack query(int t, int l, int r)
{
	if (l <= lb[t] && rb[t] <= r)
		return val[t];
	if (r <= rb[lc[t]]) return query(lc[t], l, r);
	if (l >= lb[rc[t]]) return query(rc[t], l, r);
	return add(query(lc[t], l, r), query(rc[t], l, r));
}
int main()
{
	freopen("defense.in", "r", stdin);
	scanf("%d%d%s", &n, &m, st);
	for (int i = 1; i <= n; ++ i) scanf("%d", &pi[i]);
	for (int i = 1; i < n; ++ i)
	{
		int a, b;
		scanf("%d%d", &a, &b);
		bi[a].push_back(b);
		bi[b].push_back(a);
	}
	if (st[0] == 'A') 
	{
		build(1, n);
		while (m --)
		{
			scanf("%d%d%d%d", &A, &X, &B, &Y);
			if (A > B) swap(A, B), swap(X, Y);
			pack w;
			w.d[1][1] = 0;
			if (1 != A) w = add(w, query(1, 1, A - 1));
			{
				pack v;
				if (X == 0) v.d[0][0] = 0;
				if (X == 1) v.d[1][1] = pi[A];
				w = add(w, v);
			}
			if (A != B - 1) w = add(w, query(1, A + 1, B - 1));
			{
				pack v;
				if (Y == 0) v.d[0][0] = 0;
				if (Y == 1) v.d[1][1] = pi[B];
				w = add(w, v);
			}
			if (B != n) w = add(w, query(1, B + 1, n));
			int res = min(w.d[1][0], w.d[1][1]);
			if (res >= INF) res = -1;
			printf("%d\n", res);
		}
	}
}
