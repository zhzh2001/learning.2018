#include <bits/stdc++.h>
using namespace std;

#define LL long long 
const LL INF = 1e15;
const LL N = 3e5;
LL n, m;
vector <LL> bi[N];
LL pi[N];
char st[10];
struct data 
{
	LL d[2];
	data () {for (LL i = 0; i < 2; ++ i) d[i] = INF;}
};
void chkmin(LL &t, LL d)
{
	t = min(t, d);
}
data add_to(data a, data b)
{
	data c;
	for (LL i = 0; i < 2; ++ i)
		for (LL j = 0; j < 2; ++ j)
			if (i || j) chkmin(c.d[j], a.d[i] + b.d[j]);
	return c;
}
data init(LL id, LL t)
{
	data c;
	if (t == -1 || t == 1) c.d[1] = pi[id];
	if (t == -1 || t == 0) c.d[0] = 0;
	return c;
}
LL A, X, B, Y;
data dfs(LL t, LL f = 0)
{
	data w;
	if (t == A)
		w = init(t, X);
	else if (t == B)
		w = init(t, Y);
	else 
		w = init(t, -1);
	for (LL i = 0; i < bi[t].size(); ++ i)
		if (bi[t][i] != f)
			w = add_to(dfs(bi[t][i], t), w);
	return w;
}

struct pack
{
	LL d[2][2];
	pack () {for (LL i = 0; i < 2; ++ i) for (LL j = 0; j < 2; ++ j) d[i][j] = INF;}
};
LL lc[N], rc[N], lb[N], rb[N];
pack val[N];
pack add(pack a, pack b)
{
	pack c;
	for (LL i = 0; i < 2; ++ i)
		for (LL j = 0; j < 2; ++ j)
			for (LL k = 0; k < 2; ++ k) if (j || k)
				for (LL l = 0; l < 2; ++ l)
					c.d[i][l] = min(c.d[i][l], a.d[i][j] + b.d[k][l]);		
	return c;
}
LL build(LL l, LL r)
{
	static LL tot = 0;
	LL t = ++ tot;
	lb[t] = l; rb[t] = r;
	if (l < r) 
	{
		lc[t] = build(l, (l + r) / 2);
		rc[t] = build((l + r) / 2 + 1, r);
		val[t] = add(val[lc[t]], val[rc[t]]);
	}
	else 
	{
		val[t].d[0][0] = 0;
		val[t].d[1][1] = pi[l];
	}
	return t;
}
pack query(LL t, LL l, LL r)
{
	if (l <= lb[t] && rb[t] <= r)
		return val[t];
	if (r <= rb[lc[t]]) return query(lc[t], l, r);
	if (l >= lb[rc[t]]) return query(rc[t], l, r);
	return add(query(lc[t], l, r), query(rc[t], l, r));
}
int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%lld%lld%s", &n, &m, st);
	for (LL i = 1; i <= n; ++ i) scanf("%lld", &pi[i]);
	for (LL i = 1; i < n; ++ i)
	{
		LL a, b;
		scanf("%lld%lld", &a, &b);
		bi[a].push_back(b);
		bi[b].push_back(a);
	}
	if (n <= 2000)
	{
		while (m --)
		{
			scanf("%lld%lld%lld%lld", &A, &X, &B, &Y);
			data w = dfs(1);
			LL res = min(w.d[0], w.d[1]);
			if (res >= INF) res = -1;
			printf("%lld\n", res);
		}
	}
	else if (st[0] == 'A') 
	{
		build(1, n);
		while (m --)
		{
			scanf("%lld%lld%lld%lld", &A, &X, &B, &Y);
			if (A > B) swap(A, B), swap(X, Y);
			pack w;
			w.d[1][1] = 0;
			if (1 != A) w = add(w, query(1, 1, A - 1));
			{
				pack v;
				if (X == 0) v.d[0][0] = 0;
				if (X == 1) v.d[1][1] = pi[A];
				w = add(w, v);
			}
			if (A != B - 1) w = add(w, query(1, A + 1, B - 1));
			{
				pack v;
				if (Y == 0) v.d[0][0] = 0;
				if (Y == 1) v.d[1][1] = pi[B];
				w = add(w, v);
			}
			if (B != n) w = add(w, query(1, B + 1, n));
			LL res = min(w.d[1][0], w.d[1][1]);
			if (res >= INF) res = -1;
			printf("%lld\n", res);
		}
	}
}
