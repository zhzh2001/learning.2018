#include <bits/stdc++.h>
using namespace std;

const int MOD = 1e9 + 7;
const int N = 2000;
int n, m;
map <pair <int, int>, int> F, G;
int res;
void ADD(int &t, int d)
{
	t += d; if (t >= MOD) t -= MOD;
}
int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	F[make_pair(0, 0)] = 1;
	for (int i = 0; i < m; ++ i)
	{
		for (int j = 0; j < n; ++ j)
		{
			G.clear();
			for (map <pair <int, int>, int>::iterator it = F.begin(); it != F.end(); ++ it)
			{
				int p = it->first.first, q = it->first.second, v = it->second;
				for (int k = 0; k < 2; ++ k)
				{
					int p_1 = (p >> (j - 1)) & 1, p0 = (p >> j) & 1;
					int q_1 = (q >> (j - 1)) & 1, q0 = (q >> j) & 1;
					if (i && (j != n - 1) && bool((p >> (j + 1)) & 1) < k) continue;
					if (q0 && (k != bool((p >> (j + 1)) & 1))) 
						continue;
					int np = (p ^ (p & (1 << j))) | (k << j);
					int nq = q | (i && j && (j < n - 1) ? (((p_1 == p0) | q_1) << j) : 0);
					// cerr << i << " " << j << " " << p << " " << q << " " << np << " " << nq << " " << v << endl;
					ADD(G[make_pair(np, nq)], v);
				}
			}
			swap(F, G);
			// cerr << i << " " << j << " " << F.size() << endl;
		}
	}
	for (map <pair <int, int>, int>::iterator it = F.begin(); it != F.end(); ++ it)
		ADD(res, it->second);
	printf("%d\n", res);
}
