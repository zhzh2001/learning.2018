#include <bits/stdc++.h>
using namespace std;

const int N = 50000;
int n, m;
vector <pair <int, int> > bi[N];
int vis[N], res[N], now[N], cnt, sum, ban;
void dfs(int t, int lst[], int &cnt)
{
	vis[t] = 1; lst[++ cnt] = t;
	for (int i = 0; i < bi[t].size(); ++ i)
	{
		int p = bi[t][i].first, q = bi[t][i].second;
		if (q != ban && !vis[p])
			dfs(p, lst, cnt);
	}
}
int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; ++ i)	
	{
		int a, b;
		scanf("%d%d", &a, &b);
		bi[a].push_back(make_pair(b, i));
		bi[b].push_back(make_pair(a, i));
	}
	for (int i = 1; i <= n; ++ i)
		sort(bi[i].begin(), bi[i].end());
	if (n != m)
		dfs(1, res, sum);
	else 
	{
		res[1] = 1e9;
		for (int i = 1; i <= m; ++ i)
		{
			ban = i; cnt = 0;
			for (int j = 1; j <= n; ++ j) vis[j] = 0;
			dfs(1, now, cnt);
			if (cnt == n)
			{
				int ok;
				for (int j = 1; j <= n; ++ j)
					if (now[j] != res[j])
					{
						ok = now[j] < res[j];
						break;
					}
				if (ok)
				{
					for (int j = 1; j <= n; ++ j)
						res[j] = now[j];
				}
			}
		}
	}
	for (int i = 1; i <= n; ++ i)
		printf("%d ", res[i]);
	puts("");
}
