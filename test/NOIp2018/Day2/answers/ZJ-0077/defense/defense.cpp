#include<cstdio>
#include<algorithm>
#define maxn 100005
#define INF 1000000000
using namespace std;
int read(){
	char c=getchar();
	bool sgn=0;
	int x=0;
	while((c<'0'||c>'9')&&c!='-')c=getchar();
	if(c=='-')sgn=1,c=getchar();
	while(c>='0'&&c<='9'){
		x=(x<<3)+(x<<1)+c-'0';
		c=getchar();
	}
	return x;
}
void write(int x){
	if(x<0)putchar('-'),write(-x);
	else{
		if(x>=10)write(x/10);
		putchar(x%10+'0');
	}
}
struct edge{
	int to,next;
};
edge e[maxn<<1];
int head[maxn],cnte;
void add(int u,int v){
	e[++cnte].to=v;
	e[cnte].next=head[u];
	head[u]=cnte;
}
char ty[5];
int p[maxn],n,m,dp[maxn][2],dep[maxn];
void dfs(int u,int last){
	dep[u]=dep[last]+1;
	for(int i=head[u];i;i=e[i].next){
		int v=e[i].to;
		if(v==last)continue;
		dfs(v,u);
		dp[u][0]+=dp[v][1];
		dp[u][1]+=dp[v][0]+p[u];
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();scanf("%s",ty);
	for(int i=1;i<=n;i++){
		p[i]=read();
	}
	if(*ty=='A'){
		for(int i=1;i<n;i++){
			int u=read(),v=read();
		}
		for(int i=1;i<=m;i++){
			int a=read(),x=read(),b=read(),y=read();
			for(int i=1;i<=n;i++)dp[i][0]=dp[i][1]=INF;
			dp[0][0]=dp[0][1]=0;
			for(int i=1;i<=n;i++){
				if(!(i==a&&x)&&!(i==b&&y))
					dp[i][0]=dp[i-1][1];
				if(!(i==a&&!x)&&!(i==b&&!y))
					dp[i][1]=min(dp[i-1][1],dp[i-1][0])+p[i];
			}
			write(min(dp[n][0],dp[n][1])>=INF?-1:min(dp[n][0],dp[n][1]));
			putchar('\n');
		}
		return 0;
	}
	else if(ty[1]=='2'){
		for(int i=1;i<n;i++){
			int u=read(),v=read();
			add(u,v);
			add(v,u);
		}
		dfs(1,0);
		for(int i=1;i<=m;i++){
			int a=read(),x=read(),b=read(),y=read();
			if(dep[a]<dep[b])swap(a,b),swap(x,y);
			if(x&&y){
				if(dep[a]&1)write(min(dp[1][1]+p[b],dp[1][0]+p[a]));
				else write(min(dp[1][0]+p[b],dp[1][1]+p[a]));
			}
			putchar('\n');
		}
		return 0;
	}
	return 0;
}
