#include<cstdio>
#include<vector>
#include<algorithm>
#define maxn 10002
using namespace std;
int read(){
	char c=getchar();
	bool sgn=0;
	int x=0;
	while((c<'0'||c>'9')&&c!='-')c=getchar();
	if(c=='-')sgn=1,c=getchar();
	while(c>='0'&&c<='9'){
		x=(x<<3)+(x<<1)+c-'0';
		c=getchar();
	}
	return x;
}
void write(int x){
	if(x<0)putchar('-'),write(-x);
	else{
		if(x>=10)write(x/10);
		putchar(x%10+'0');
	}
}
vector<int> g[maxn];
int cyc[maxn],cntcyc,oncyc[maxn],n,m,ans[maxn],cntans;
bool vis[maxn];
bool getcyc(int u,int last){
	vis[u]=1;
	for(int i=0;i<int(g[u].size());i++){
		int v=g[u][i];
		if(vis[v]){
			if(v!=last){
				cyc[++cntcyc]=v;
				oncyc[v]=cntcyc;
				return 1;
			}
			continue;
		}
		if(getcyc(v,u)){
			cyc[++cntcyc]=v;
			oncyc[v]=cntcyc;
			if(oncyc[u])return 0;
			return 1;
		}
	}
	return 0;
}
void dfs(int,int);
void solve(int u,int last){
//printf("solve %d %d\n",u,last);
	int i=oncyc[u]+1,j=oncyc[u]-1;
	int flag1=0,flag2=0;
	while(1){
		if(i>cntcyc)i=1;
		if(j<1)j=cntcyc;
//printf("%d:%d %d:%d\n",i,cyc[i],j,cyc[j]);
		if((cyc[i]<cyc[j]&&flag2<2)||(flag2==2&&flag1==1)){
			if(flag1!=1)flag2++;
			flag1=1;
			ans[++cntans]=cyc[i];
			for(int k=0;k<int(g[cyc[i]].size());k++){
				int v=g[cyc[i]][k];
				if(oncyc[v]||v==last)continue;
				dfs(v,cyc[i]);
			}
			if(i==j)break;
			i++;
		}
		else if((cyc[i]>cyc[j]&&flag2<2)||(flag2==2&&flag1==2)){
			if(flag1!=2)flag2++;
			flag1=2;
			ans[++cntans]=cyc[j];
			for(int k=0;k<int(g[cyc[j]].size());k++){
				int v=g[cyc[j]][k];
				if(oncyc[v]||v==last)continue;
				dfs(v,cyc[j]);
			}
			if(i==j)break;
			j--;
		}
		else{
			ans[++cntans]=cyc[i];
			for(int k=0;k<int(g[cyc[i]].size());k++){
				int v=g[cyc[i]][k];
				if(oncyc[v]||v==last)continue;
				dfs(v,cyc[i]);
			}
			break;
		}
	}
}
void dfs(int u,int last){
	ans[++cntans]=u;
	if(oncyc[u]){
		solve(u,last);
		return;
	}
	for(int i=0;i<int(g[u].size());i++){
		int v=g[u][i];
		if(v==last)continue;
		dfs(v,u);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++){
		int u=read(),v=read();
		g[u].push_back(v);
		g[v].push_back(u);
	}
	for(int i=1;i<=n;i++)sort(g[i].begin(),g[i].end());
	getcyc(1,0);
	dfs(1,0);
	for(int i=1;i<=cntans;i++){
		write(ans[i]),putchar(' ');
	}
	return 0;
}
