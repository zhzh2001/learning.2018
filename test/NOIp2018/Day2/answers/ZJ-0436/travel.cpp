#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
int n,m,ans[20000],cnt=0;
bool b[20000],mp[5001][5001];

void dfs(int k){
	for (int i=1;i<=n;i++)
		if (mp[k][i]&&b[i]){
			b[i]=0;
			cnt++;ans[cnt]=i;
			dfs(i);
		}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(b,1,sizeof(b));
	memset(mp,0,sizeof(mp));
	cin>>n>>m;
	int x,y;
	for (int i=1;i<=m;i++){
		cin>>x>>y;
		mp[x][y]=1;
		mp[y][x]=1;
	}
	ans[1]=1;cnt=1;b[1]=0;
	dfs(1);
	for (int i=1;i<=n;i++)
		cout<<ans[i]<<' ';
	return 0;	
}
