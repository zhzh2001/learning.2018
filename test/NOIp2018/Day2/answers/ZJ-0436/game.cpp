#include <iostream>
#include <cstdio>
using namespace std;
const long long mod=1e9+7;
long long n,m,mp[100][100],sum=0;
long long a[100][100],b[100][100];

long long pow(long long a,long long b){
	if (b==0) return 1;
	long long ans=pow(a,b/2)*pow(a,b/2)%mod;
	if (b%2==1) ans=ans*a%mod;
	return ans;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	mp[1][1]=0;
	mp[1][2]=0;
	mp[2][1]=0;
	mp[2][2]=12;
	mp[2][3]=36;
	mp[3][2]=36;
	mp[3][3]=112;
	cin>>n>>m;
	if (n<=3&&m<=3)
	cout<<mp[n][m];
	else 
	if (n==2) cout<<(long long)4*pow(3,max(n,m)-1)%mod;
	else cout<<(long long)7*pow(4,max(n,m)-1)%mod;
	return 0;
}
