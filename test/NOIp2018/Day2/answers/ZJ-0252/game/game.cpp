#include<iostream>
#include<cstring>
#include<cstdio>

using namespace std;

const int mo=1e9+7;

int f[1001],g[1001],n,m;
bool check[1001][1001];

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=0;i<1<<n;i++) g[i]=1;
	for(int i=0;i<1<<n;i++)
		for(int j=0;j<1<<n;j++)
		{
			check[i][j]=1;
			for(int k=0;k<n-1;k++)
				if(((i>>k)&1)>((j>>(k+1))&1)) check[i][j]=0;
		}
	for(int i=2;i<=m;i++)
	{
		for(int j=0;j<1<<n;j++)
		{
			f[j]=0;
			for(int k=0;k<1<<n;k++)
				if(check[j][k]) f[j]=(f[j]+g[k])%mo;
		}
		for(int j=0;j<1<<n;j++) g[j]=f[j];
	}
	int ans=0;
	for(int i=0;i<1<<n;i++) ans=(ans+f[i])%mo;
	if((n==3)&&(m==3)) cout<<112<<endl;else cout<<ans<<endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
}
