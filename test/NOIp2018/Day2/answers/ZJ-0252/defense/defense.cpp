#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdio>
#define next dfkjgfd
#define int long long

using namespace std;

const int inf=1e15;

int n,m,f[100001][2],to[500001],next[500001],g[100001][2];
int first[500001],e1=0,fa[100001],a[100001],a1,b1,x,y;
char ch[100];

void insert(int o,int p)
{
	to[e1]=p;next[e1]=first[o];first[o]=e1++;
}

void dfs1(int o,int p)
{
	fa[o]=p;int yu1=0,yu0=0;
	for(int i=first[o];i!=-1;i=next[i])
		if(to[i]!=p)
		{
			dfs1(to[i],o);
			yu1+=min(f[to[i]][0],f[to[i]][1]);
			yu0+=f[to[i]][1];
		}
	if(((o==x)&&(a1==1))||((o==y)&&(b1==1))) f[o][0]=inf;else f[o][0]=yu0;
	if(((o==x)&&(a1==0))||((o==y)&&(b1==0))) f[o][1]=inf;else f[o][1]=yu1+a[o];
}

signed main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(first,-1,sizeof(first));
	scanf("%lld%lld%s",&n,&m,ch);
	for(int i=1;i<=n;i++) 
	{
		scanf("%lld",&a[i]);
		f[i][0]=f[i][1]=0;
	}
	for(int i=1;i<n;i++)
	{
		int x,y;scanf("%lld%lld",&x,&y);
		insert(x,y);insert(y,x);
	}
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&x,&a1,&y,&b1);
		if((a1==0)&&(b1==0)&&((fa[x]==y)||(fa[y]==x))) 
		{
			puts("-1");
			continue;
		}
		for(int j=1;j<=n;j++) f[j][0]=f[j][1]=0;
		dfs1(1,0);
		printf("%lld\n",min(f[1][0],f[1][1]));
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
