#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int MAX_N=5+1e5;
struct Node{
	int to,nxt,poi; bool mark;
}edge[MAX_N*2];
int ring[MAX_N],top_ring=0,n,m;
int head[MAX_N],top_edge=-1;
void add_edge(int x,int y){
	++top_edge;
	edge[top_edge]=(Node){y,head[x],top_edge^1,false};
	head[x]=top_edge;
}
pair<int,int> a[MAX_N];
int pos[MAX_N];
void init(){
	scanf("%d%d",&n,&m);
	memset(head,-1,sizeof(head));
	for(int i=1;i<=m;++i){
		int u,v; scanf("%d%d",&u,&v);
		add_edge(u,v),add_edge(v,u);
	}
	for(int i=1;i<=n;++i){
		int top=0;
		for(int j=head[i];j!=-1;j=edge[j].nxt){
			++top;
			a[top]=make_pair(edge[j].to,edge[j].poi);
			pos[top]=j;
		}
		sort(a+1,a+top+1);
		for(int i=1;i<=top;++i){
			edge[pos[i]].to=a[i].first;
			edge[pos[i]].poi=a[i].second;
			edge[a[i].second].poi=pos[i];
		}
	}
//	exit(0);
/*	for(int i=1;i<=n;++i){
		for(int j=head[i];j!=-1;j=edge[j].nxt)
			printf("(%d %d)",edge[j].to,edge[edge[j].poi].to);
		puts("");
	}*/
}
pair<int,int> stk[MAX_N];
int top_stk=0;
bool mark[MAX_N];
int ans[MAX_N],g[MAX_N],top_g;
bool judge(){
	for(int i=1;i<=n;++i){
		if(g[i]<ans[i]) return true;
		if(g[i]>ans[i]) return false;
	}
	return false;
}
void opera(){
	if(judge()){
		for(int i=1;i<=n;++i)
			ans[i]=g[i];
	}
}
void tarjan(int x,int pre,int id){
	if(mark[x]){
		if(top_ring==0){
			for(int i=top_stk;stk[i].first!=x;--i){
				ring[++top_ring]=stk[i].second;
			}
			ring[++top_ring]=id;
		}
		return;
	}
	mark[x]=true,stk[++top_stk]=make_pair(x,id);
	for(int j=head[x];j!=-1;j=edge[j].nxt){
		int y=edge[j].to;
		if(y!=pre) tarjan(y,x,j);
	}
	--top_stk,mark[x]=false;
}
void solve(int x){
	mark[x]=true,g[++top_g]=x;
	for(int j=head[x];j!=-1;j=edge[j].nxt){
		int y=edge[j].to;
		if(!mark[y]&&!edge[j].mark)
			solve(y);
	}
}
void work(){
	for(int i=1;i<=n;++i) mark[i]=false;
	top_g=0,solve(1),opera();
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	init();
	tarjan(1,0,0);
	for(int i=1;i<=n;++i) ans[i]=n;
	if(m<n) work();
	else{
//		for(int i=1;i<=top_ring;++i)
//			printf("[%d %d]",edge[ring[i]].to,edge[edge[ring[i]].poi].to);
		for(int i=1;i<=top_ring;++i){
			edge[ring[i]].mark=true;
			edge[edge[ring[i]].poi].mark=true;
			work();
			edge[ring[i]].mark=false;
			edge[edge[ring[i]].poi].mark=false;
		}
	}
	for(int i=1;i<=n;++i) printf("%d ",ans[i]);
	return 0;
}

