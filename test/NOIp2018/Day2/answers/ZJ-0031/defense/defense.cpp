#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
const int MAX_N=5+1e5;
struct Edge{
	int to,nxt;
}edge[MAX_N*2];
int head[MAX_N],top_edge=-1;
void add_edge(int x,int y){
	edge[++top_edge]=(Edge){y,head[x]};
	head[x]=top_edge;
}
int g[MAX_N];
ll f[MAX_N][2],p[MAX_N];
bool mark;
void dfs(int x,int pre){
	f[x][0]=0,f[x][1]=p[x];
	for(int j=head[x];j!=-1;j=edge[j].nxt){
		int y=edge[j].to;
		if(y!=pre){
			dfs(y,x);
			if(g[y]==0){
				if(g[x]==0) mark=true;
				g[x]=1;
				f[x][1]+=f[y][0];
			}else if(g[y]==1){
				f[x][0]+=f[y][1];
				f[x][1]+=f[y][1];
			}else{
				f[x][0]+=f[y][1];
				f[x][1]+=min(f[y][0],f[y][1]);
			}
		}
	}
}
char s[15];
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof(head));
	memset(g,-1,sizeof(g));
	int n,m; scanf("%d%d%s",&n,&m,s);
	for(int i=1;i<=n;++i) scanf("%lld",&p[i]);
	for(int i=1;i<n;++i){
		int u,v; scanf("%d%d",&u,&v);
		add_edge(u,v),add_edge(v,u);
	}
	for(int i=1;i<=m;++i){
		int a,x,b,y; scanf("%d%d%d%d",&a,&x,&b,&y);
		for(int j=1;j<=n;++j) g[j]=-1;
		g[a]=x,g[b]=y,mark=false;
		dfs(1,0);
		if(mark) puts("-1");
		else{
			if(g[1]==0) printf("%lld\n",f[1][0]);
			else if(g[1]==1) printf("%lld\n",f[1][1]);
			else printf("%lld\n",min(f[1][0],f[1][1]));
		}
	}
	return 0;
}
