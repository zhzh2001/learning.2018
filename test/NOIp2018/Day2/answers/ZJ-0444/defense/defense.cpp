#include<bits/stdc++.h>
using namespace std;
#define maxn 1000010
#define LL long long
#define INF 1e18
struct edge
{
	int y,next;
}e[maxn*2];
int linkk[maxn],len=0;
int N,M;
char ch[10];
int val[maxn];
LL f[maxn],g[maxn];
LL cf[maxn],cg[maxn];
int deep[maxn];
int a,wa,b,wb;
bool flag;
int read() {
	int s=1,w=0;
	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())
		if(ch=='-') s=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())
		w=(w<<3)+(w<<1)+ch-48;
	return s*w;
}
void insert(int xx,int yy) {
	e[++len].next=linkk[xx];
	e[len].y=yy;
	linkk[xx]=len;
}
void init() {
	N=read();M=read();scanf("%s",ch);
	for(int i=1;i<=N;++i) val[i]=read();
	for(int i=1;i<N;++i) {
		int s=read(),t=read();
		insert(s,t);
		insert(t,s);
	}
}
void DP(int x,int fa) {
	for(int i=linkk[x];i;i=e[i].next) {
		int y=e[i].y;
		if(y==fa) continue;
		deep[y]=deep[x]+1;
		DP(y,x);
		if(g[x]>=0) {
			if(f[y]<0) g[x]=-1;
			else g[x]+=f[y];
		}
		if(f[x]>=0) {
			if(f[y]<0&&g[y]<0) f[x]=-1;
			else if(g[y]<0) f[x]+=f[y];
			else if(f[y]<0) f[x]+=g[y];
			else f[x]+=min(f[y],g[y]);
		}
	}
	if(f[x]>=0) f[x]+=val[x];
}
void work1() {
	for(int i=1;i<=M;++i) {
		for(int j=1;j<=N;++j) 
			f[j]=g[j]=0;
		a=read(); wa=read();
		b=read(); wb=read();
		if(wa) g[a]=-1;
		else f[a]=-1;
		if(wb) g[b]=-1;
		else f[b]=-1;
		DP(1,0);
		if(f[1]<0&&g[1]<0) puts("-1");
		else if(f[1]<0) printf("%lld\n",g[1]);
		else if(g[1]<0) printf("%lld\n",f[1]);
		else printf("%lld\n",min(f[1],g[1]));
	}
}
void dfs(int x,int now) {
	if(x==b) flag=1;
	for(int i=linkk[x];i;i=e[i].next) {
		int y=e[i].y;
		if(deep[y]>=deep[x]) continue;
		cf[y]=f[y]; cg[y]=g[y];
		if(!now) {
			cg[y]=-1;
			cf[y]-=min(f[x],g[x])-cg[x];
		}
		else if(now==1) {
			cg[y]=g[y];
			cf[y]-=min(f[x],g[x])-cf[x];
		}
		else {
			if(cg[x]<0&&cf[x]<0) cf[y]=cg[y]=-1;
			else if(cf[x]<0) { cg[y]=-1; cf[y]-=min(f[x],g[x])-cg[x]; }
			else if(cg[x]<0) { cf[y]-=min(f[x],g[x])-cf[x]; cg[y]-=f[x]-cf[x]; }
			else {
				cf[y]=cf[y]-min(f[x],g[x])+min(cf[x],cg[x]);
				cg[y]=cg[y]-f[x]+cf[x];
			}
		}
		dfs(y,2);
	}
}
void work2() {
	deep[1]=1;
	DP(1,0);
	for(int i=1;i<=M;++i) {
		flag=0;
		a=read(); wa=read();
		b=read(); wb=read();
		if(a==b&&wa!=wb) { puts("-1"); continue; }
		if(deep[a]<deep[b]) { swap(a,b); swap(wa,wb); }
		if(wa) { cf[a]=f[a]; cg[a]=-1; }
		else { cf[a]=-1; cg[a]=g[a]; }
		dfs(a,wa);
		if(wb) { 
			cg[b]=-1;
			if(!flag) cf[b]=f[b];
		}
		else {
			cf[b]=-1;
			if(!flag) cg[b]=g[b];
		}
		dfs(b,wb);
		if(cf[1]<0&&cg[1]<0) puts("-1");
		else if(cf[1]<0) printf("%lld\n",cg[1]);
		else if(cg[1]<0) printf("%lld\n",cf[1]);
		else printf("%lld\n",min(cf[1],cg[1]));
	}
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	init();
	if(N<=5000) work1();
	else work2();
	return 0;
}
