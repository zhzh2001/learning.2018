#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define mod 1000000007
int a,b;
LL f[10][10];
LL ans;
LL power(int x,int p) {
	LL sum=1;
	while(p) {
		if(p&1) sum=sum*x%mod;
		x=x*x%mod;
		p>>=1;
	}
	return sum;
}
void work() {
	ans=3;
	LL x=1;
	for(int i=1;i<=b-2;++i) {
		x=x*4%mod;
		ans=(ans+2*x%mod)%mod;
	}
	ans=ans*4%mod;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&a,&b);
	f[2][2]=12;f[2][3]=f[3][2]=44;f[3][3]=112;f[5][5]=7136;
	if(a==5&&b==5) ans=f[5][5];
	else if(a==1) ans=power(2,b); 
	else if(b==1) ans=power(2,a);
	else if(a<=3&&b<=3) ans=f[a][b];
	else if(a==2) work();
	printf("%lld\n",ans);
	return 0;
}
