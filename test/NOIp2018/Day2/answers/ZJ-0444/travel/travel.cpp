#include<bits/stdc++.h>
using namespace std;
#define maxn 5010
int N,M;
bool e[maxn][maxn];
int ans[maxn],k=0;
int dfn[maxn],low[maxn];
int cnt=0;
int s[maxn],top=0;
int a[maxn];
bool vis[maxn];
int flag=0;
int read()
{
	int s=1,w=0;
	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())
		if(ch=='-') s=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())
		w=(w<<3)+(w<<1)+ch-48;
	return s*w;
}

void init()
{
	N=read();M=read();
	for(int i=1;i<=M;++i) {
		int s=read(),t=read();
		e[s][t]=e[t][s]=1;
	}
}
void DFS(int x,int fa) {
	ans[++k]=x;
	for(int i=1;i<=N;++i) {
		if(i==fa||!e[x][i]) continue;
		DFS(i,x);
	}
}
void work1() {
	DFS(1,0);
	for(int i=1;i<N;++i)
		printf("%d ",ans[i]);
	printf("%d\n",ans[N]);
}
void tarjan(int x,int fa) {
	dfn[x]=low[x]=++cnt;
	s[++top]=x;
	for(int i=1;i<=N;++i) {
		if(i==fa||!e[x][i]) continue;
		if(!dfn[i]) {
			tarjan(i,x);
			low[x]=min(low[x],low[i]);
		}
		else low[x]=min(low[x],dfn[i]);
	}
	if(dfn[x]<=low[x]) {
		if(s[top]!=x) {
			while(s[top]!=x) 
				a[++k]=s[top--];
			a[++k]=x;
		}
		if(s[top]==x) top--;
	}
}
void dfs(int x,int fa) {
	if(x==flag) { flag=0; return; }
	vis[x]=1;ans[++k]=x;
	for(int i=1;i<=N;++i) {
		if(vis[i]||!e[x][i]) continue;
		dfs(i,x);
	}
}
void work2() {
	tarjan(1,0);
	int j=k-1,i=1;
	while(i<j) {
		if(a[i]<a[j]) {
			if(j!=k-1) { flag=a[j]; break; }
			else i++;
		}
		else {
			if(i!=1) { flag=a[i]; break; }
			else j--;
		}
	}
	k=0;
	dfs(1,0);
	for(int i=1;i<N;++i) 
		printf("%d ",ans[i]);
	printf("%d\n",ans[N]);
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	init();
	if(M==N-1) work1();
	else work2();
	return 0;
}
