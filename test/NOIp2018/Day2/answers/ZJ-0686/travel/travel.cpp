#include <bits/stdc++.h>
using namespace std;
int to[10005],last[10005],Next[10005],n,m,x,y,edge,tot,son[5005],s[5005],top,cnt,h,dfn[5005],low[5005],f[5005],ff=1,u,vis[5005];
void add(int x,int y){
	to[++edge]=y;
	Next[edge]=last[x];
	last[x]=edge;
}
void dfs(int x,int fa){
	for (int i=last[x];i;i=Next[i])
	if (to[i]!=fa){
		dfs(to[i],x);
		son[x]++;
	}
}
void dfs1(int x,int fa){
	printf("%d ",x);
	int a[son[x]+5],cnt=0;
	for (int i=last[x];i;i=Next[i])
	if (to[i]!=fa) a[++cnt]=to[i];
	sort(a+1,a+cnt+1);
	for (int i=1;i<=cnt;i++) 
	dfs1(a[i],x);
}
void dfs2(int x,int fa){
	s[++top]=x,dfn[x]=low[x]=++cnt;
	vis[x]=1;
	for (int i=last[x];i;i=Next[i])
	if (to[i]!=fa){
		if (!dfn[to[i]]){
			dfs2(to[i],x);
			low[x]=min(low[x],low[to[i]]);
		}else if (vis[to[i]])
			low[x]=min(low[x],dfn[to[i]]);
		son[x]++;
	}
	int k=0;
	if (low[x]==dfn[x]){
		int u;
		tot++;
		do{
			u=s[top--];
			vis[u]=0;
			k++;
			f[u]=tot;
		}while (u!=x);
		if (k>1) h=tot;
	}
}
void dfs3(int x,int fa,int ss){
	printf("%d ",x);
	dfn[x]=1;
	int a[son[x]+5],cnt=0;
	for (int i=last[x];i;i=Next[i])
		if (to[i]!=fa && dfn[to[i]]==0){
			a[++cnt]=to[i];
		}
	sort(a+1,a+cnt+1);
	
	a[cnt+1]=5005;
	for (int i=1;i<=cnt;i++){
		if (f[a[i]]==h && ff && a[i]>ss){
			ff=0;
			return;
		}
		if (f[x]==h && i-cnt==0) dfs3(a[i],x,min(a[i+1],ss));
		else dfs3(a[i],x,a[i+1]);
	}
		
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	if (m==n-1){
		dfs(1,0);
		dfs1(1,0);
	}else{
		dfs2(1,0);
		memset(dfn,0,sizeof(dfn));
		dfs3(1,0,5005);
	}
}
