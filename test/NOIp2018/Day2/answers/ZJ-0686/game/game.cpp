#include <bits/stdc++.h>
const int mu=1000000007;
int n,m,flag,a[200][200],dp[2][200],ans=0;
using namespace std;
int ksm(int x,int y){
	int ans=1,now=x;
	while (y){
		if (y%2) ans=1ll*ans*now%mu;
		now=1ll*now*now%mu;
		y/=2;
	}
	return ans;
}
bool pf(int x,int y){
	int wx=0,wy=0,ax[10],ay[10];
	memset(ax,0,sizeof(ax));
	memset(ay,0,sizeof(ay));
	while (x>0) ax[++wx]=x%2,x/=2;
	while (y>0) ay[++wy]=y%2,y/=2;
	for (int i=max(wx,wy);i;i--) if (ax[i]<ay[i]) return false;
	return true;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d %d",&n,&m);
	if (n==1){
		printf("%d",ksm(2,m));
	}if (n==2){
		printf("%d",ksm(3,m-1)*1ll*4%mu);
	}else if (n==3 && m==3) printf("%d",112);
	else
	{
		int k=ksm(2,n)-1;
		for (int i=0;i<=k;i++)
			for (int j=0;j<=i;j++) if (pf(i,j)) a[i][++a[i][0]]=j;
		for (int i=0;i<=k;i++) dp[0][i]=1;
		for (int i=2;i<=m;i++){
			flag=1-flag;
			memset(dp[flag],0,sizeof(dp[flag]));
			for (int j=0;j<=k/2;j++){
				for (int h=1;h<=a[j][0];h++)
					dp[flag][j]=dp[flag][j+k/2+1]=(1ll*dp[flag][j]+dp[1-flag][a[j][h]*2]+dp[1-flag][a[j][h]*2+1])%mu;
			}	
		}
		for (int j=0;j<=k;j++) ans=(ans+dp[flag][j])%mu;
		printf("%d",ans);
	}
}
