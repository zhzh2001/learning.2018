#include <bits/stdc++.h>
const long long INF=100000000001;
const int N=100005;
long long dp[2][N];
using namespace std;
int to[N<<1],last[N],edge,Next[N<<1],xx,yy,a,b,v[N],n,m,x,y;
char s[5];
void add(int x,int y){
	to[++edge]=y;
	Next[edge]=last[x];
	last[x]=edge;
}
void dfs(int x,int fa){
	for (int i=last[x];i;i=Next[i])
	if (to[i]!=fa){
		dfs(to[i],x);
		dp[0][x]+=dp[1][to[i]];
		dp[1][x]+=min(dp[1][to[i]],dp[0][to[i]]);
	}
	dp[1][x]+=v[x];
	if (x==xx) dp[1-a][x]=INF;
	if (x==yy) dp[1-b][x]=INF;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d %d ",&n,&m);
	gets(s);
	for (int i=1;i<=n;i++) scanf("%d",&v[i]);
	for (int i=1;i<n;i++){
		scanf("%d %d",&x,&y);
		add(x,y);
		add(y,x);
	}
	for (int i=1;i<=m;i++){
		scanf("%d %d %d %d",&xx,&a,&yy,&b);
		memset(dp,0,sizeof(dp));
		dfs(1,0);
		long long ans=min(dp[0][1],dp[1][1]);
		if (ans>=INF) printf("-1\n");
		else printf("%lld\n",ans);
	}
	return 0;
}
