#include <bits/stdc++.h>
using namespace std;

int n, m;
int G[20][20];
string a, b;
pair <string, string> c[2000000];
int top;
bool ok;

inline void Dfs(int s, int t) {
  if (!ok) return;
  if (s == n && t == m) {
    c[++ top] = make_pair(a, b);
    for (int i = 1; i < top; i ++) {
      if (a < c[i].first && b < c[i].second) ok = 0;
      if (c[i].first < a && c[i].second < b) ok = 0;
    }
    return;
  }
  string c = a, d = b;
  if (s < n) {
    a = c + 'D';
    b = d + (char)('0' + G[s + 1][t]);
    Dfs(s + 1, t);
  }
  if (t < m) {
    a = c + 'R';
    b = d + (char)('0' + G[s][t + 1]);
    Dfs(s, t + 1);
  }
  a = c; b = d;
}

int ans = 0;

inline void check() {
  top = 0;
  ok = 1;
  Dfs(1, 1);
  if (!ok) return;
  ans ++;
  if (G[1][1] == 1 && G[n][m] == 1) {
    for (int i = 1; i <= n; i ++) {
      for (int j = 1; j <= m; j ++) cout << G[i][j];
      puts("");
    }
    puts("");
  }
}

int main() {
  scanf("%d%d", &n, &m);
  int tot = 1 << (n * m);
  for (int s = 0; s < tot; s ++) {
    int k = 0;
    for (int i = 1; i <= n; i ++)
      for (int j = 1; j <= m; j ++) {
	G[i][j] = (s >> k) & 1; k ++;
      }
    check();
  }
  cout << ans << endl;
  return 0;
}
