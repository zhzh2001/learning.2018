#include <bits/stdc++.h>
using namespace std;

const int N = 5050;

int n, m;
int ans[N], cur[N], top;
vector <int> G[N];

inline void Dfs(int x, int f) {
  int y;
  ans[++ top] = x;
  for (size_t i = 0; i < G[x].size(); i ++) {
    y = G[x][i];
    if (y == f) continue;
    Dfs(y, x);
  }
}

int vis[N];
int bidx, bidy, fa[N], there, fin;
bool bad, ok;

inline void fd(int x, int f) {
  int y;
  fa[x] = f;
  vis[x] = 1;
  ans[++ top] = x;
  for (size_t i = 0; i < G[x].size(); i ++) {
    y = G[x][i];
    if (y == f) continue;
    if (vis[y] == 1) there = x, fin = y;
    else if (!vis[y]) fd(y, x);
  }
  vis[x] = 2;
}

inline void sfd(int x, int f) {
  if (!ok && bad) return;
  cur[++ top] = x;
  if (cur[top] > ans[top]) bad = 1;
  if (cur[top] < ans[top] && !bad) ok = 1;
  for (size_t i = 0; i < G[x].size(); i ++) {
    int y = G[x][i];
    if (y == f) continue;
    if (x == bidx && y == bidy) continue;
    if (x == bidy && y == bidx) continue;
    sfd(y, x);
  }
}

inline void doit() {
  fd(1, 0);
  for (int i = there; i != fin; i = fa[i]) {
    top = 0;
    bad = ok = 0;
    bidx = i;
    bidy = fa[i];
    sfd(1, 0);
    if (ok) memcpy(ans, cur, sizeof ans);
  }
}

int main() {
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);
  scanf("%d%d", &n, &m);
  for (int i = 1, x, y; i <= m; i ++) {
    scanf("%d%d", &x, &y);
    G[x].push_back(y);
    G[y].push_back(x);
  }
  for (int i = 1; i <= n; i ++)
    sort(G[i].begin(), G[i].end());
  if (m == n - 1) Dfs(1, 0);
  else doit();
  for (int i = 1; i <= n; i ++) printf("%d ", ans[i]);
  puts("");
  return 0;
}
