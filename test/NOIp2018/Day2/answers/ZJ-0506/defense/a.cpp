#include <bits/stdc++.h>
using namespace std;

typedef long long LL;
const int N = 1e5 + 50;

struct edge {
  int nxt, to;
} e[N << 1];
int fir[N], cnt = 1;

inline void addedge(int x, int y) {
  e[++ cnt] = (edge) { fir[x], y }; fir[x] = cnt;
}

int c[N];
char s[10];
int n, m;

namespace brute {
  int val[N];
  LL dp[N][2];
  inline void Dfs(int x, int f) {
    for (int i = fir[x]; i; i = e[i].nxt)
      if (e[i].to != f) Dfs(e[i].to, x);
    dp[x][0] = dp[x][1] = 0;
    for (int i = fir[x]; i; i = e[i].nxt)
      if (e[i].to != f) {
	int y = e[i].to;
	dp[x][0] += dp[y][1];
	dp[x][1] += min(dp[y][0], dp[y][1]);
      }
    dp[x][1] += c[x];
    if (val[x] == 0) dp[x][1] = 1e18;
    else if (val[x] == 1) dp[x][0] = 1e18;
  }
  inline void solve() {
    int x, y, a, b;
    memset(val, -1, sizeof val);
    for (; m; m --) {
      scanf("%d%d%d%d", &x, &a, &y, &b);
      val[x] = a;
      val[y] = b;
      Dfs(1, 0);
      val[x] = val[y] = -1;
      LL res = min(dp[1][0], dp[1][1]);
      if (res >= 1e18) res = -1;
      printf("%lld\n", res);
    }
  }
}

namespace AC {
  LL dp[N][2], rdp[N][2];
  int fa[N][17], dep[N];
  LL val[N][17][2][2];
  inline void Dfs(int x, int f) {
    for (int i = fir[x]; i; i = e[i].nxt)
      if (e[i].to != f) Dfs(e[i].to, x);
    dp[x][0] = dp[x][1] = 0;
    for (int i = fir[x]; i; i = e[i].nxt)
      if (e[i].to != f) {
	int y = e[i].to;
	dp[x][0] += dp[y][1];
	dp[x][1] += min(dp[y][0], dp[y][1]);
      }
    dp[x][1] += c[x];
  }
  inline void RDfs(int x, int f) {
    for (int i = fir[x]; i; i = e[i].nxt)
      if (e[i].to != f) {
	int y = e[i].to;
	LL v1 = rdp[x][1] + (dp[x][1] - min(dp[y][1], dp[y][0])) - c[x];
	LL v0 = rdp[x][0] + (dp[x][0] - dp[y][1]);
	rdp[y][1] = min(v1, v0) + c[y];
	rdp[y][0] = v1;
	RDfs(y, x);
      }
  }
  inline void Build(int x, int f) {
    dep[x] = dep[f] + 1;
    fa[x][0] = f;
    val[x][0][0][0] = dp[x][0];
    val[x][0][1][1] = dp[x][1];
    for (int i = 1, j = f, k = x; i < 17; i ++) {
      for (int a = 0; a < 2; a ++)
	for (int b = 0; b < 2; b ++)
	  for (int c = 0; c < 2; c ++)
	    if (b || c)
	      for (int d = 0; d < 2; d ++) {
		LL t = val[x][i - 1][a][b] + val[j][i - 1][c][d];
		if (c == 0) t -= dp[k][1];
		else t -= min(dp[k][0], dp[k][1]);
		val[x][i][a][d] = min(val[x][i][a][d], t);
	      }
      j = fa[x][i] = fa[j][i - 1];
      k = fa[k][i - 1];
    }
    for (int i = fir[x]; i; i = e[i].nxt)
      if (e[i].to != f) Build(e[i].to, x);
  }
  inline void Init() {
    Dfs(1, 0);
    Build(1, 0);
    RDfs(1, 0);
  }
  inline int LCA(int x, int y) {
    if (dep[x] < dep[y]) swap(x, y);
    for (int i = 16; dep[x] != dep[y]; i --)
      if (dep[fa[x][i]] >= dep[y]) x = fa[x][i];
    if (x == y) return x;
    for (int i = 16; fa[x][0] != fa[y][0]; i --)
      if (fa[x][i] != fa[y][i]) {
	x = fa[x][i]; y = fa[y][i];
      }
    return fa[x][0];
  }
  LL res[2][2], swp[2][2], tab[2], tb[2];
  inline int jmpt(int x, int d) {
    for (int i = 16; ~ i; i --)
      if (dep[fa[x][i]] >= d) x = fa[x][i];
    return x;
  }
  inline int calc(int x, int bidf) {
    if (dep[x] == dep[bidf] + 1) {
      memset(res, 0x3f, sizeof res);
      res[0][0] = dp[x][0];
      res[1][1] = dp[x][1];
      return x;
    }
    memset(res, 0, sizeof res);
    bool first = 1;
    for (int i = 16, k; ~ i; i --)
      if (dep[fa[x][i]] > dep[bidf]) {
	if (first) {
	  memcpy(res, val[x][i], sizeof res);
	  k = x;
	  x = fa[x][i];
	  k = jmpt(k, dep[x] + 1);
	  first = 0;
	  continue;
	}
	memset(swp, 0x3f, sizeof swp);
	for (int a = 0; a < 2; a ++)
	  for (int b = 0; b < 2; b ++)
	    for (int c = 0; c < 2; c ++)
	      if (b || c)
		for (int d = 0; d < 2; d ++) {
		  LL t = res[a][b] + val[x][i][c][d];
		  if (c == 0) t -= dp[k][1];
		  else t -= min(dp[k][0], dp[k][1]);
		  swp[a][d] = min(swp[a][d], t);
		}
	x = fa[x][i]; k = fa[k][i];
	memcpy(res, swp, sizeof res);
      }
    return x;
  }
  inline void solve() {
    memset(val, 0x3f, sizeof val);
    Init();
    int x, y, a, b;
    for (; m; m --) {
      scanf("%d%d%d%d", &x, &a, &y, &b);
      if (dep[x] + 1 == dep[y] && a + b == 0) {
	puts("-1");
	continue;
      }
      int bro = LCA(x, y);
      if (bro == y) swap(x, y);
      LL ans = 1e18;
      if (x == bro) {
	int p = calc(y, x);
	tab[0] = min(res[0][0], res[1][0]);
	tab[1] = min(res[0][1], res[1][1]);
	ans = min(ans, tab[0] + rdp[p][0]);
	ans = min(ans, tab[1] + rdp[p][1] - c[p]);
      } else {
	int p = calc(x, bro);
	tab[0] = min(res[0][0], res[1][0]);
	tab[1] = min(res[0][1], res[1][1]);
	int q = calc(y, bro);
	tb[0] = min(res[0][0], res[1][0]);
	tb[1] = min(res[0][1], res[1][1]);
	int dos = c[bro];
	for (int a = 0; a < 2; a ++)
	  for (int b = 0; b < 2; b ++)
	    for (int c = 0; c < 2; c ++)
	      if (c || (a && b)) {
		LL v = rdp[bro][c] + dp[bro][c] + tab[a] + tb[b];
		if (c) v -= dos + min(dp[p][0], dp[p][1]) + min(dp[q][0], dp[q][1]);
		else v -= dp[p][1] + dp[q][1];
		ans = min(ans, v);
	      }
      }
      printf("%lld\n", ans);
    }
  }
}

inline void file() {
  freopen("defense.in", "r", stdin);
  freopen("defense.out", "w", stdout);
}

int main() {
  file();
  scanf("%d%d", &n, &m);
  scanf("%s", s);
  for (int i = 1; i <= n; i ++) scanf("%d", &c[i]);
  for (int i = 1, x, y; i < n; i ++) {
    scanf("%d%d", &x, &y);
    addedge(x, y); addedge(y, x);
  }
  if (n <= 2000 && m <= 2000) brute :: solve();
  else
    AC :: solve();
  return 0;
}
