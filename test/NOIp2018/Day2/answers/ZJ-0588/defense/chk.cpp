#include<bits/stdc++.h>
using namespace std;
bool _begin;
#define N 100005
#define inf 1000000000
#define LL long long
int n,m,A[N];
char str[10];
int H[N],totedge;
struct edge{int to,n;}e[N<<1];
#define link(a,b) e[++totedge]=(edge){H[a],b},H[a]=totedge
namespace P55{
	#define N55 2005
	int dp[N55][2];
	int p1,c1,p2,c2;
	void chk(int &x,int y){
		if(x==inf)return;
		if(y==inf)x=inf;
		else x+=y;
	}
	void qry(int x){
		if(x==p1)dp[x][!c1]=inf;
		else if(x==p2)dp[x][!c2]=inf;
	}
	void dfs(int f,int x){
		dp[x][1]=dp[x][0]=0;
		qry(x);
		for(int i=H[x];i;i=e[i].to){
			int y=e[i].n;
			if(y==f)continue;
			dfs(x,y);
			int mi=min(dp[y][0],dp[y][1]);
			chk(dp[x][1],mi);
			chk(dp[x][0],dp[y][1]);
		}
		chk(dp[x][1],A[x]);
	}
	void solve(){
		while(m--){
			scanf("%d%d%d%d",&p1,&c1,&p2,&c2);
			dfs(0,1);
			int ans=min(dp[1][0],dp[1][1]);
			if(ans==inf)ans=-1;
			printf("%d\n",ans);
		}
	}
}
namespace P2{
	int dep[N];
	LL D[N][2],U[N][2];
	void dfs(int f,int x){
		D[x][0]=D[x][1]=0;
		for(int i=H[x];i;i=e[i].to){
			int y=e[i].n;
			if(y==f)continue;
			dep[y]=dep[x]+1;
			dfs(x,y);
			D[x][0]+=D[y][1];
			D[x][1]+=min(D[y][1],D[y][0]);
		}
		D[x][1]+=A[x];
	}
	void redfs(int f,int x){
		for(int i=H[x];i;i=e[i].to){
			int y=e[i].n;
			if(y==f)continue;
			U[y][0]=D[x][0]-D[y][1];
			U[y][1]=D[x][1]-min(D[y][0],D[y][1]);
			redfs(x,y);
		}
	}
	void solve(){
		dfs(0,1);
		while(m--){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(dep[a]<dep[b])swap(a,b);//a low
			if(x||y)printf("%lld\n",D[a][x]+U[a][y]);
			else puts("-1");
		}
	}
}
bool _end;
int main(){//file LL mod memset ....
//	printf("%lf\n",(&_end-&_begin)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("chk.out","w",stdout);
	cin>>n>>m>>str;
	for(int i=1;i<=n;i++)scanf("%d",&A[i]);
	for(int i=1;i<n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		link(u,v),link(v,u);
	}
	P55::solve();
	return 0;
}
