#include<bits/stdc++.h>
using namespace std;
bool _begin;
#define M 100005
int n,m;
const int dx[]={0,1},dy[]={1,0},mod=1e9+7;
const char ch[]={"RD"};
namespace P20{
	char mp[10][10];
	int cnt;
	string s[20],w[20];
	void dfs(int x,int y,string s1,string w1){
		if(x==n-1&&y==m-1)s[++cnt]=s1,w[cnt]=w1;
		for(int i=0;i<2;i++){
			int a=x+dx[i],b=y+dy[i];
			if(0<=a&&a<n&&0<=b&&b<m)
				dfs(a,b,s1+mp[a][b],w1+ch[i]);
		}
	}
	bool chk(){
		for(int i=1;i<cnt;i++)
			for(int j=i+1;j<=cnt;j++){
				if(w[i]>w[j]&&s[i]>s[j])return 0;
				if(w[i]<w[j]&&s[i]<s[j])return 0;
			}
		return 1;
	}
	void solve(){
		int tot=n*m,ans=0;
		for(int i=0;i<(1<<tot);i++){
			for(int j=0;j<tot;j++)
				mp[j/m][j%m]=((i&1<<j)>0)+'0';
			cnt=0;
			string tmp;
			tmp=mp[0][0];
			dfs(0,0,tmp,"");
			if(chk())ans++;
		}
		cout<<ans<<endl;
	}
}
namespace P2{
	int Pow(int x,int b){
		int res=1;
		while(b){
			if(b&1)res=1ll*res*x%mod;
			x=1ll*x*x%mod;
			b>>=1;
		}
		return res;
	}
	void solve(){
		if(n==1)cout<<Pow(2,m)<<endl;
		else cout<<4ll*Pow(3,m-1)%mod<<endl;
	}
}
bool _end;
int main(){//file LL mod memset ....
//	printf("%lf\n",(&_end-&_begin)/1024.0/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	if(n<=3&&m<=3)P20::solve();
	else if(n<=2)P2::solve();
	
//	P20::solve();
//	P2::solve();
	return 0;
}
