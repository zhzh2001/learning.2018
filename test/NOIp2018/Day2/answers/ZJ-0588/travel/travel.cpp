#include<bits/stdc++.h>
using namespace std;
bool _begin;
#define N 5005
int n,m;
struct edge{
	int to,id;
	bool operator<(const edge&A)const{
		return to<A.to;
	}
};
vector<edge>e[N];
namespace P1{
	void dfs(int f,int x){
		printf("%d ",x);
		for(int i=0;i<e[x].size();i++){
			int y=e[x][i].to;
			if(y==f)continue;
			dfs(x,y);
		}
	}
	void solve(){
		for(int i=1;i<=n;i++)sort(e[i].begin(),e[i].end());
		dfs(0,1);
		puts("");
	}
}
namespace P2{
	int vis[N];
	bool loop;
	int stk[N],top,Ans[N];
	int broken;
	void dfs(int f,int x){
		if(vis[x]==*vis){
			loop=1;
			return;
		}
		vis[x]=*vis;
		stk[++top]=x;
		for(int i=0;i<e[x].size();i++){
			int y=e[x][i].to;
			if(y==f||e[x][i].id==broken)continue;
			dfs(x,y);
		}
	}
	bool cmp(){
		for(int i=1;i<=n;i++)
			if(stk[i]!=Ans[i])
				return stk[i]<Ans[i];
		return 0;
	}
	void solve(){
		for(int i=1;i<=n;i++)sort(e[i].begin(),e[i].end());
		bool ans=0;
		for(int i=1;i<=m;i++){
			loop=0;
			++*vis;
			broken=i;
			top=0;
			dfs(0,1);
			for(int j=1;j<=n;j++)
				if(vis[j]!=*vis){
					loop=1;break;
				}
			if(loop)continue;
			if(!ans||cmp())memcpy(Ans,stk,sizeof Ans),ans=1;
		}
		for(int i=1;i<=n;i++)printf("%d ",Ans[i]);
		puts("");
	}
}
bool _end;
int main(){//file LL mod memset ....
//	printf("%lf\n",(&_end-&_begin)/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<=m;i++){
		int a,b;
		scanf("%d%d",&a,&b);
		e[a].push_back((edge){b,i});
		e[b].push_back((edge){a,i});
	}
	if(m==n-1)P1::solve();
	else P2::solve();
	return 0;
}
