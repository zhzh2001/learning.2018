var
nxt,v,head,du,vis:Array[0..10010] of longint;
que:Array[0..100010] of longint;
n,m,x,y,enum,tail,i:longint;
procedure addedge(x,y:longint);
begin
    inc(enum);
    v[enum]:=y;
    nxt[enum]:=head[x];
    head[x]:=enum;
end;
procedure qsort(l,r:longint);
var i,j,t,mid:longint;
begin
    i:=l; j:=r;
    mid:=que[(l+r) div 2];
    while i<=j do
    begin
        while que[i]<mid do inc(i);
        while que[j]>mid do dec(j);
        if i<=j then
        begin
            t:=que[i]; que[i]:=que[j]; que[j]:=t;
            inc(i); dec(j);
        end;
    end;
    if l<j then qsort(l,j);
    if i<r then qsort(i,r);
end;
procedure dfs(t,fa:longint);
var i,vv:longint;
begin
    if vis[t]=1 then write(t,' ');
    i:=head[t];
    while i<>0 do
    begin
        vv:=v[i];
        if (vv=fa)and(m=n-1) then begin i:=nxt[i]; continue; end
        else begin
        inc(tail);
        que[tail]:=vv;
        end;
        i:=nxt[i];
    end;
    qsort(tail-du[t]+2,tail);
    for i:=tail-du[t]+2 to tail do
    if vis[que[i]]<du[que[i]] then
    begin
        inc(vis[que[i]]);
        dfs(que[i],t);
        inc(vis[que[i]]);
    end;
end;
begin
assign(input,'travel.in');
assign(output,'travel.out');
reset(input);
rewrite(output);
    readln(n,m);
    for i:=1 to m do
    begin
        readln(x,y);
        addedge(x,y);
        addedge(y,x);
        inc(du[x]); inc(du[y]);
    end;
    vis[1]:=1; inc(du[1]);
    dfs(1,0);
close(input);
close(output);
end.
