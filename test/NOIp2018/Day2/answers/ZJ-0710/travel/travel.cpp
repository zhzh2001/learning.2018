#include<bits/stdc++.h>

using namespace std;
typedef long long LL;
const int N = 1e5 + 7;

int n, m;
int head[N], cnt = 0;

struct edge {
	int next, to;
}e[2 * N];

void add(int u, int v) { 
	e[++cnt].to = v; e[cnt].next = head[u], head[u] = cnt;
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	
	cin >> n >> m;
	for (int i = 1; i <= m; ++ i) {
		int x, y;
		cin >> x >> y;
		add(x, y); add(y, x);
	}
	for (int i = 1; i <= n; ++ i) cout << i << " ";
	return 0;
}
