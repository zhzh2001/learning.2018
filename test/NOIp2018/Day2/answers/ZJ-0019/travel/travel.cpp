#include <bits/stdc++.h>
using namespace std;

template <class T> inline
void read(T &x) {
	int f = 1; x = 0;
	char ch = getchar();
	while (ch < '0' || ch > '9') {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;
	
}

const int N = 5000 + 5;

int n, m, ban, num;
int a[N], A[N], vis[N];
vector <int> v[N], w[N];
map<int, int> mp[N];

void dfs(int x) {
	a[++num] = x;
	vis[x] = 1;
	for (int i = 0; i < v[x].size(); i++) {
		int y = v[x][i];
		if (vis[y] || ban == w[x][i]) continue;
		dfs(y);
	}
}

void compare() {
	if (num != n) return;
	for (int i = 1; i <= n; i++) {
		if (a[i] == A[i]) continue;
		if (a[i] < A[i]) {
			for (int j = 1; j <= n; j++) A[j] = a[j];
			return;
		} 
		if (a[i] > A[i]) return;
	}
}

void solve(int k) {
	ban = k;
	for (int i = 1; i <= n; i++) vis[i] = 0;
	num = 0;
	dfs(1);
	compare();
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	read(n), read(m);
	for (int i = 1, x, y; i <= m; i++) {
		read(x), read(y);
		v[x].push_back(y);
		v[y].push_back(x);
		mp[x][y] = mp[y][x] = i;
	}
	for (int i = 1; i <= n; i++) sort(v[i].begin(), v[i].end());
	for (int i = 1; i <= n; i++) {
		for (int j = 0; j < v[i].size(); j++) {
			w[i].push_back(mp[i][v[i][j]]);
		}
	}
	/*
	for (int i = 1; i <= n; i++) {
		for (int j = 0; j < v[i].size(); j++) {
			printf("%d %d %d\n", i, v[i][j], w[i][j]);
		}
	}
	*/
	for (int i = 1; i <= n; i++) A[i] = n;
	if (m == n - 1) solve(0);
	else {
		for (int i = 1; i <= m; i++) solve(i);
	}
	for (int i = 1; i < n; i++) printf("%d ", A[i]); printf("%d\n", A[n]);
	return 0;
}
