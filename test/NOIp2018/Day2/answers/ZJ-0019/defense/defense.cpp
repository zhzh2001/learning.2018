#include <bits/stdc++.h>
#define LL long long
#define INF (1LL << 60)
#define lc (x << 1)
#define rc (x << 1 | 1)
using namespace std;

template <class T> inline
void read(T &x) {
	int f = 1; x = 0;
	char ch = getchar();
	while (ch < '0' || ch > '9') {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;	
}

const int N = 100000 + 5;

int n, m;
int ff[N];
LL p[N], f[N][2], F[N][2], g[N];
vector <int> v[N], tag;

void dfs(int x, int fa) {
	ff[x] = fa;
	for (int i = 0; i < v[x].size(); i++) {
		int y = v[x][i];
		if (y == fa) continue;
		dfs(y, x);
	}
	f[x][1] = p[x];
	f[x][0] = 0;
	for (int i = 0; i < v[x].size(); i++) {
		int y = v[x][i];
		if (y == fa) continue;
		f[x][1] += min(f[y][1], f[y][0]);
		f[x][0] += f[y][1];
	}
	if (g[x] == 0) f[x][1] = INF;
	if (g[x] == 1) f[x][0] = INF;
	f[x][1] = min(f[x][1], INF);
	f[x][0] = min(f[x][0], INF);
}	

struct node {
	LL a[2][2];
} R[N << 2];

node makenode(int x, int c) {
	node v;
	v.a[1][1] = p[x];
	v.a[0][0] = 0;
	v.a[1][0] = v.a[0][1] = INF;
	if (c == 1) v.a[0][0] = INF;
	if (c == 0) v.a[1][1] = INF;
	return v;
}

node merge(node A, node B) {
	node v;
	for (int i = 0; i < 2; i++) 
		for (int j = 0; j < 2; j++) {
			v.a[i][j] = INF;
			for (int k = 0; k < 2; k++) 
				for (int w = 0; w < 2; w++) {
					if (k + w == 0) continue;
					v.a[i][j] = min(v.a[i][j], A.a[i][k] + B.a[w][j]);
				}
		}
	return v;
}

void build(int x, int l, int r) {
	if (l == r) {R[x] = makenode(l, -1); return;}
	int m = (l + r) >> 1;
	build(lc, l, m);
	build(rc, m + 1, r);
	R[x] = merge(R[lc], R[rc]);
}

node query(int x, int l, int r, int s, int t) {
	if (s <= l && r <= t) return R[x];
	int m = (l + r) >> 1; node ans;
	if (s <= m) ans = query(lc, l, m, s, t);
	if (m < t) {
		if (s <= m) ans = merge(ans, query(rc, m + 1, r, s, t));
		else ans = query(rc, m + 1, r, s, t);
	}
	return ans;
}

void modify(int x, int c) {
	if (c == 0) f[x][1] = INF;
	if (c == 1) f[x][0] = INF;
	tag.push_back(x);
	while (ff[x]) {
		int y = ff[x];
		tag.push_back(y);
		f[y][0] -= F[x][1];
		f[y][0] += f[x][1];
		
		f[y][1] -= min(F[x][1], F[x][0]);
		f[y][1] += min(f[x][1], f[x][0]);
		x = y;
	}
}

char s[10];

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	read(n), read(m); scanf("%s", s + 1);
	for (int i = 1; i <= n; i++) read(p[i]);
	for (int i = 1; i <= n; i++) g[i] = -1;
	for (int i = 1, x, y; i < n; i++) {
		read(x), read(y);
		v[x].push_back(y);
		v[y].push_back(x);
	}
	if (n <= 2000 && m <= 2000) {	
		for (int i = 1, x, y, a, b; i <= m; i++) {
			read(x), read(a);
			read(y), read(b);
			g[x] = a;
			g[y] = b;
			dfs(1, 0);
			g[x] = -1;
			g[y] = -1;
			LL ans = min(f[1][0], f[1][1]);
			if (ans == INF) printf("-1\n");
			else printf("%lld\n", ans);
		}
	} else if (s[1] == 'A') {
		build(1, 1, n);
		for (int i = 1, x, y, a, b; i <= m; i++) {
			read(x), read(a);
			read(y), read(b);
			if (x > y) {
				swap(x, y);
				swap(a, b);
			}
			node ans;
			if (x != 1) ans = merge(query(1, 1, n, 1, x - 1), makenode(x, a));
			else ans = makenode(x, a);
			if (x + 1 <= y - 1) ans = merge(ans, query(1, 1, n, x + 1, y - 1));
			ans = merge(ans, makenode(y, b));
			if (y + 1 <= n) ans = merge(ans, query(1, 1, n, y + 1, n));
			LL tmp = INF;
			for (int i = 0; i <= 1; i++) 
				for (int j = 0; j <= 1; j++) 
					tmp = min(tmp, ans.a[i][j]);
			if (tmp == INF) printf("-1\n");
			else printf("%lld\n", tmp);
		}
	} else {
		dfs(1, 0);
		for (int i = 1; i <= n; i++) F[i][0] = f[i][0], F[i][1] = f[i][1];
		for (int i = 1, x, y, a, b; i <= m; i++) {
			read(x), read(a);
			read(y), read(b);
			if (x != 1) modify(x, a);
			if (y != 1) modify(y, b);
			LL ans = f[1][1];
			if (ans == INF) printf("-1\n");
			else printf("%lld\n", ans);
			for (int i = 0; i < tag.size(); i++) {
				int x = tag[i];
				f[i][0] = F[i][0];
				f[i][1] = F[i][1];
			}
			tag.clear();
		}
	}
	return 0;
}
