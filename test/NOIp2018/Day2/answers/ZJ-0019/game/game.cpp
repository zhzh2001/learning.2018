#include <bits/stdc++.h>
using namespace std;

template <class T> inline
void read(T &x) {
	int f = 1; x = 0;
	char ch = getchar();
	while (ch < '0' || ch > '9') {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;
	
}

const int mo = 1000000007;

int n, m, ans;

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	read(n), read(m);
	if (n == 1) {
		ans = 1;
		for (int i = 1; i <= m; i++) ans = 1LL * ans * 2 % mo;
	
	}
	if (n == 2) {
		ans = 4;
		for (int i = 1; i < m; i++) ans = 1LL * ans * 3 % mo;
	}
	if (n == 3) {
		ans = 112;
	}
	printf("%d\n", ans);
	return 0;
}
