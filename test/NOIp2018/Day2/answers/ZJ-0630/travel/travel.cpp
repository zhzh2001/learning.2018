#include<bits/stdc++.h>
#define N 5050
using namespace std;
struct Edge{
	int to,next;
}e[N*2];
int h[N],cnt=0,n,m,a[N],f1,f2;
bool vis[N];
vector<int>g[N];
void dfs(int x){
	printf("%d ",x);vis[x]=1;
	for (int i=0;i<g[x].size();i++)
	    if(!vis[g[x][i]])dfs(g[x][i]);
}
void dfs0(int x){
	a[++cnt]=x;vis[x]=1;
	for (int i=0;i<g[x].size();i++)
	    if(!vis[g[x][i]]&&(!(x==f1&&g[x][i]==f2||x==f2&&g[x][i]==f1)))dfs0(g[x][i]);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		e[++cnt].to=v;e[cnt].next=h[u];h[u]=cnt;
		e[++cnt].to=u;e[cnt].next=h[v];h[v]=cnt;
	}
	for (int i=1;i<=n;i++){
		cnt=0;
		for (int j=h[i];j;j=e[j].next)a[++cnt]=e[j].to;
		sort(a+1,a+cnt+1);
		for (int j=1;j<=cnt;j++)g[i].push_back(a[j]);
	}
	if(n-1==m){
		dfs(1);
	    printf("\n");
	    return 0;
	}
	int ans[N];
	for (int i=1;i<=n;i++)ans[i]=N;
	for (int i=1;i<=n;i++)
		for (int j=0;j<g[i].size();j++)
		    if(i<g[i][j]){
		    	cnt=0;f1=i;f2=g[i][j];
				memset(vis,0,sizeof(vis));memset(a,0,sizeof(a));
		    	dfs0(1);
		    	if(cnt<n)continue;
		    	bool f=0;
		    	for (int i=1;i<=n;i++){
		    		if(a[i]<ans[i]){f=1;break;}
		    		if(a[i]>ans[i])break;
				}
		    	if(f==1)
		    	    for (int i=1;i<=n;i++)ans[i]=a[i];
			}
	for (int i=1;i<=n;i++)printf("%d ",ans[i]);
	printf("\n");
	return 0;
}
