#include<bits/stdc++.h>
#define N 100010
#define ll long long
using namespace std;
struct Edge{
	int to,next;
}e[N*2];
int n,m,p[N],h[N],cnt,sod[20],a,x,b,y;
ll ans;
bool f(){
	for (int i=1;i<=n;i++)
		if(!sod[i])
		    for (int j=h[i];j;j=e[j].next)
		        if(!sod[e[j].to])return 0;
	return 1;
}
void dfs(int t){
	if(t>n){
		if (f()&&sod[a]==x&&sod[b]==y){
			ll sum=0;
			for (int i=1;i<=n;i++)
			    if(sod[i])sum+=p[i];
			ans=min(ans,sum);
		}
		return;
	}
	sod[t]=1;
	dfs(t+1);
	sod[t]=0;
	dfs(t+1);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d %d ",&n,&m);
	char c1,c2;
	scanf("%c%c",&c1,&c2);
	for (int i=1;i<=n;i++)scanf("%d",&p[i]);
	for (int i=1;i<n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		e[++cnt].to=v;e[cnt].next=h[u];h[u]=cnt;
		e[++cnt].to=u;e[cnt].next=h[v];h[v]=cnt;
	}
	while(m--){
		scanf("%d%d%d%d",&a,&x,&b,&y);ans=(ll)N*N;
		if(c2=='2'&&!x&&!y){printf("-1\n");continue;}
		dfs(1);
		if(ans==(ll)N*N)ans=-1;
		printf("%lld\n",ans);
	}
	return 0;
}
