#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int M=(int)1e5+5;
const long long inf=(long long)1e13+7;
template<class T>void Rd(T &res){
	res=0;static char p;
	while(p=getchar(),p<'0');
	do{
		res=(res*10)+(p^48);
	}while(p=getchar(),p>='0');
}
template<class T>void Ps(T x){
	static int stk[64],sz;sz=0;
	while(x)stk[sz++]=x%10,x/=10;
	if(sz==0)putchar('0');
	else while(sz--)putchar(stk[sz]^48);
	putchar('\n');
}
template<class T>void Min(T &x,T y){
	if(y==-1)return;
	if(x>y||x==-1)x=y;
}
struct W{
	int to,nx;
}Lis[M*2];
int Head[M],tot;
void eAdd(int x,int y){
	Lis[++tot]=(W){y,Head[x]};
	Head[x]=tot;
}
int n,m,p[M];
char S[15];
struct SHUI{
	long long dp[2005][2];
	int aa,xx,bb,yy,ff;
	void dfs(int x,int f){
		dp[x][0]=0;
		dp[x][1]=p[x];
		for(int i=Head[x],to;i&&(to=Lis[i].to,1);i=Lis[i].nx)
			if(to!=f){
				dfs(to,x);
				if(dp[to][1]!=-1&&dp[x][0]!=-1)dp[x][0]+=dp[to][1];
				else dp[x][0]=-1;
				dp[x][1]+=dp[to][0];
			}
		if(aa==x){
			if(xx)dp[x][0]=dp[x][2]=-1;
			else dp[x][1]=-1;
		}
		if(bb==x){
			if(yy)dp[x][0]=dp[x][2]=-1;
			else dp[x][1]=-1;
		}
		Min(dp[x][0],dp[x][1]);
		if(dp[x][0]==-1)ff=true;
	}
	void calc(){
		dfs(1,1);
	}
	void solve(){
		for(int i=1;i<=m;i++){
			Rd(aa),Rd(xx),Rd(bb),Rd(yy);
			ff=false;
			calc();
			if(ff)puts("-1");
			else if(dp[1][0]==-1){
				if(dp[1][1]==-1)puts("-1");
				else Ps(dp[1][1]);
			}else {
				if(dp[1][1]==-1)Ps(dp[1][0]);
				else Ps(min(dp[1][0],dp[1][1]));
			}
		}
	}
}P44;
int v[2][2];
struct IHUI{
	struct SEGMENT{
		long long val[M*4][2][2];
		void up(int p){
			int ls=p<<1,rs=p<<1|1;
			val[p][0][0]=min(val[ls][0][0]+val[rs][1][0],val[ls][0][1]+val[rs][0][0]);
			val[p][0][1]=min(val[ls][0][0]+val[rs][1][1],val[ls][0][1]+val[rs][0][1]);
			val[p][1][0]=min(val[ls][1][0]+val[rs][1][0],val[ls][1][1]+val[rs][0][0]);
			val[p][1][1]=min(val[ls][1][0]+val[rs][1][1],val[ls][1][1]+val[rs][0][1]);
			Min(val[p][0][1],val[p][1][1]);
			Min(val[p][1][0],val[p][1][1]);
			Min(val[p][0][0],min(val[p][0][1],val[p][1][0]));
		}
		void build(int L,int R,int pp){
			if(L==R){
				val[pp][0][0]=0;
				val[pp][1][1]=val[pp][0][1]=val[pp][1][0]=p[L];
				return;
			}int mid=(L+R)>>1;
			build(L,mid,pp<<1),build(mid+1,R,pp<<1|1);
			up(pp);
		}
		void query(int L,int R,int p,int l,int r){
			if(L==l&&R==r){
				if(v[0][0]==-1){
					v[0][0]=val[p][0][0];
					v[0][1]=val[p][0][1];
					v[1][0]=val[p][1][0];
					v[1][1]=val[p][1][1];
				}else {
					static int tmpv[2][2];
					tmpv[0][0]=min(v[0][0]+val[p][1][0],v[0][1]+val[p][0][0]);
					tmpv[0][1]=min(v[0][0]+val[p][1][1],v[0][1]+val[p][0][1]);
					tmpv[1][0]=min(v[1][0]+val[p][1][0],v[1][1]+val[p][0][0]);
					tmpv[1][1]=min(v[1][0]+val[p][1][1],v[1][1]+val[p][0][1]);
					Min(tmpv[0][1],tmpv[1][1]);
					Min(tmpv[1][0],tmpv[1][1]);
					Min(tmpv[0][0],min(tmpv[0][1],tmpv[1][0]));
					v[0][0]=tmpv[0][0];
					v[0][1]=tmpv[0][1];
					v[1][0]=tmpv[1][0];
					v[1][1]=tmpv[1][1];
				}
				return;
			}int mid=(L+R)>>1;
			if(mid>=r)query(L,mid,p<<1,l,r);
			else if(mid<l)query(mid+1,R,p<<1|1,l,r);
			else query(L,mid,p<<1,l,mid),query(mid+1,R,p<<1|1,mid+1,r);
		}
	}T;
	void solve(){
		T.build(1,n,1);
		for(int i=1,a,b,x,y;i<=m;i++){
			Rd(a),Rd(x),Rd(b),Rd(y);
			if(a>b)swap(a,b),swap(x,y);
			if(a+1==b){
				if(x==0&&y==0)puts("-1");
				else {
					static long long ans;
					ans=0;
					if(a>1){
						memset(v,-1,sizeof(v));
						T.query(1,n,1,1,a-1);
						ans+=v[0][!x];
					}
					if(x)ans+=p[a];
					if(b<n){
						memset(v,-1,sizeof(v));
						T.query(1,n,1,b+1,n);
						ans+=v[!y][0];
					}
					if(y)ans+=p[b];
					Ps(ans);
				}
			}else {
				static long long ans;
				ans=0;
				if(a>1){
					memset(v,-1,sizeof(v));
					T.query(1,n,1,1,a-1);
					ans+=v[0][!x];
				}
				if(x)ans+=p[a];
				if(a+1<b){
					memset(v,-1,sizeof(v));
					T.query(1,n,1,a+1,b-1);
					ans+=v[!x][!y];
				}
				if(b<n){
					memset(v,-1,sizeof(v));
					T.query(1,n,1,b+1,n);
					ans+=v[!y][0];
				}
				if(y)ans+=p[b];
				Ps(ans);
			}
		}
	}
}P24;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	Rd(n),Rd(m),scanf("%s",S);
	for(int i=1;i<=n;i++)Rd(p[i]);
	for(int i=1,x,y;i<n;i++)Rd(x),Rd(y),eAdd(x,y),eAdd(y,x);
//	if(0);
	if(n<=2000&&m<=2000)P44.solve();
	else if(S[0]=='A')P24.solve();
	
	return 0;
}
