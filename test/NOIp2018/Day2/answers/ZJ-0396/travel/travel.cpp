#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
const int M=5005;
int n,m,mark[M],ans[M],sz;
vector<int>eg[M];
struct SHUI{
	void dfs(int x){
		mark[x]=1;
		ans[sz++]=x;
		for(int i=0,up=eg[x].size();i<up;i++)
			if(!mark[eg[x][i]])
				dfs(eg[x][i]);
	}
	void solve(){
		dfs(1);
		for(int i=0;i<n-1;i++)printf("%d ",ans[i]);
		printf("%d\n",ans[n-1]);
	}
}P60;
struct IHUS{
	int fa[M],mark[M],TIM,imp1,imp2;
	void dfs_pre(int x){
		mark[x]=1;
		for(int i=0,up=eg[x].size();i<up;i++)
			if(eg[x][i]==fa[x]);
			else if(!mark[eg[x][i]])fa[eg[x][i]]=x,dfs_pre(eg[x][i]);
			else if(imp1==0)imp1=x,imp2=eg[x][i];
	}
	int cl[M],csz;
	int tmp[M],tsz;
	bool cmp(){
		for(int i=0;i<n;i++)
			if(tmp[i]!=ans[i]){
				if(tmp[i]>ans[i])return false;
				if(tmp[i]<ans[i])return true;
			}
		return false;
	}
	int a,b;
	void dfs(int x){
		mark[x]=TIM;
		tmp[tsz++]=x;
		for(int i=0,aa,bb,up=eg[x].size();i<up;i++)
			if(mark[eg[x][i]]!=TIM){
				aa=x,bb=eg[x][i];
				if(aa>bb)swap(aa,bb);
				if(aa==a&&bb==b);
				else dfs(eg[x][i]);
			}
	}
	void calc(){
		TIM+=2;
		tsz=0;
		dfs(1);
	}
	void solve(){
		fa[1]=1;dfs_pre(1);
		csz=0;
		cl[csz++]=imp1;
		for(int x=imp1;x!=imp2;x=fa[x])cl[csz++]=fa[x];
		a=cl[csz-1],b=cl[0];
		if(a>b)swap(a,b);
		calc();
		for(int i=0;i<tsz;i++)ans[i]=tmp[i];
		for(int i=0;i<csz-1;i++){
			a=cl[i],b=cl[i+1];
			if(a>b)swap(a,b);
			calc();
			if(cmp())for(int j=0;j<n;j++)ans[j]=tmp[j];
		}
		for(int i=0;i<n-1;i++)printf("%d ",ans[i]);
		printf("%d\n",ans[n-1]);
	}
}PPP;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d %d",&n,&m);
	for(int i=1,x,y;i<=m;i++){
		scanf("%d %d",&x,&y);
		eg[x].push_back(y);
		eg[y].push_back(x);
	}
	for(int i=1;i<=n;i++)sort(eg[i].begin(),eg[i].end());
	if(n-1==m)P60.solve();
	else PPP.solve();
	return 0;
}
