#include<bits/stdc++.h>
using namespace std;

typedef long long s64;
#define rep(i,l,r) for(int i=l;i<=r;++i)
const int D=1e9+7;

int main()
{
	freopen("game.in","r",stdin);
	int n,m;
	cin>>n>>m;
	if(n>m)swap(n,m);
	s64 ans=1;
	rep(i,1,n-1)(ans*=i+1)%=D;
	ans=ans*ans%D;
	rep(i,n,m)(ans*=n+1)%=D;
	cout<<ans;
}
