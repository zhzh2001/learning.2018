#include<bits/stdc++.h>
using namespace std;

typedef long long s64;
#define rep(i,l,r) for(int i=l;i<=r;++i)
const int D=1e9+7;
s64 f[2][8][1<<8];

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	cin>>n>>m;
	if(n>m)swap(n,m);
	f[1][0][0]=1;
	rep(i,1,n-1)
	{
		swap(f[0],f[1]);
		int u=(1<<i)-1;
		rep(k,0,i)
		rep(j,0,u)f[1][k][j]=0;
		rep(kj,0,i)
		rep(j,0,u)
		if(f[0][kj][j])
		{
			rep(k,0,i)
			if(!(k&&(j&(1<<(k-1)))))
			{
				int j1=(j|(j<<1))&u;
				rep(t,1,kj-1)j1|=1<<t;
				rep(t,kj+1,i-1-1)j1|=1<<t;
				(f[1][k][j1]+=f[0][kj][j])%=D;
			}
		}
	}
	rep(tmp,n,m)
	{
		swap(f[0],f[1]);
		int u=(1<<n)-1;
		rep(k,0,n)
		rep(j,0,u)f[1][k][j]=0;
		rep(kj,0,n)
		rep(j,0,u)
		if(f[0][kj][j])
		{
			rep(k,0,n)
			if(!(k&&k<n&&(j&(1<<(k-1)))))
			{
				int j1=(j|(j<<1))&u;
				rep(t,1,kj-1)j1|=1<<t;
				rep(t,kj+1,n-1)j1|=1<<t;
				(f[1][k][j1]+=f[0][kj][j])%=D;
			}
		}
	}
	rep(i,1,n-1)
	{
		swap(f[0],f[1]);
		int u=(1<<n)-1;
		rep(k,0,n)
		rep(j,0,u)f[1][k][j]=0;
		rep(kj,0,n)
		rep(j,0,u)
		if(f[0][kj][j])
		{
			rep(k,i,n)
			if(!(k>i&&k<n&&(j&(1<<(k-1)))))
			{
				int j1=(j|(j<<1))&u;
				rep(t,(i-1)+1,kj-1)j1|=1<<t;
				rep(t,kj+1,n-1)j1|=1<<t;
				(f[1][k][j1]+=f[0][kj][j])%=D;
			}
		}
	}
	s64 ans=0;
	int u=(1<<n)-1;
	rep(k,0,n)
	rep(j,0,u)ans+=f[1][k][j];
	cout<<ans%D;
}
