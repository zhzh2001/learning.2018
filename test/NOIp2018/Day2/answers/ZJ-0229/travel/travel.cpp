#include<bits/stdc++.h>
using namespace std;

#define rep(i,l,r) for(int i=l;i<=r;++i)
#define rep0(i,l,r) for(int i=l;i<int(r);++i)
const int N=5000+5;
int n;
vector<int>lk[N];
int fa[N];bool vis[N];
int q[N],tot,ansq[N];
void dfs2(int x,int fr)
{
	q[++tot]=x;
	rep0(i,0,lk[x].size())
	{int y=lk[x][i];
	if(y&&y!=fr)dfs2(y,x);
	}
}
void check(int x,int y)
{
	int ix=lower_bound(lk[x].begin(),lk[x].end(),y)-lk[x].begin(),iy=lower_bound(lk[y].begin(),lk[y].end(),x)-lk[y].begin();
	lk[x][ix]=0;
	lk[y][iy]=0;
	tot=0;
	dfs2(1,0);
	lk[x][ix]=y;
	lk[y][iy]=x;
	int i=1;
	while(i<=n&&ansq[i]==q[i])++i;
	if(q[i]>ansq[i])return ;
	rep(i,1,n)ansq[i]=q[i];
}
int q1[N],t1;
void dfs1(int x,int fr)
{
	fa[x]=fr;vis[x]=1;
	rep0(i,0,lk[x].size())
	{int y=lk[x][i];
	if(y!=fr&&!t1)
	{
		if(vis[y])
		{
			for(int i=x;i!=y;i=fa[i])q1[++t1]=i;
			q1[++t1]=y;
			rep(i,1,n)ansq[i]=n;
			check(y,x);
			rep(i,1,t1-1)check(q1[i],q1[i+1]);
			continue;
		}
		dfs1(y,x);
	}}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int m;
	cin>>n>>m;
	rep(i,1,m)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		lk[x].push_back(y);
		lk[y].push_back(x);
	}
	rep(x,1,n)sort(lk[x].begin(),lk[x].end());
	if(m==n)
	{
		dfs1(1,0);
		rep(i,1,n)printf("%d%c",ansq[i]," \n"[i==n]);
	}
	else
	{
		dfs2(1,0);
		rep(i,1,n)printf("%d%c",q[i]," \n"[i==n]);
	}
}
