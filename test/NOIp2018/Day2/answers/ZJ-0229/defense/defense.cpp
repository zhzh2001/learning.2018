#include<bits/stdc++.h>
using namespace std;

template <typename T> void chmax(T &x,const T &y)
{
	if(x<y)x=y;
}
typedef long long s64;
#define gc (c=getchar())
int read()
{
	char c;
	while(gc<'0');
	int x=c-'0';
	while(gc>='0')x=x*10+c-'0';
	return x;
}
#define rep(i,l,r) for(int i=l;i<=r;++i)
#define rep0(i,l,r) for(int i=l;i<int(r);++i)
const int N=1e5+5;
const s64 inf=1e18;
struct Dp
{
	s64 f[2][2];
	Dp () 
	{
		rep(i,0,1)
		rep(j,0,1)f[i][j]=0;
	}
};
Dp operator +(const Dp &a,const Dp &b)
{
	Dp ans;
	rep(i,0,1)
	rep(j,0,1)
	{
		s64 now=-inf;
		rep(k1,0,1)
		rep(k2,0,1)
		if(!(k1&&k2))chmax(now,a.f[i][k1]+b.f[k2][j]);
		ans.f[i][j]=now;
	}
	return ans;
}
struct Seg
{
	Dp *a;int n,d;
	s64 operator () (int x)
	{
		return max(a[1].f[x][0],a[1].f[x][1]);
	}
	void init(int _n)
	{
		n=_n;
		for(d=1;d<n;d<<=1);d-=1;
		a=new Dp [n+d+5];
	}
	void add(int i,s64 f[2])
	{
		i+=d;
		a[i].f[0][0]+=f[0];
		a[i].f[1][1]+=f[1];
		a[i].f[0][1]=a[i].f[1][0]=-inf;
		while(i>>=1)a[i]=a[i*2]+a[i*2+1];
	}
};
Seg seg[N];
vector<int>lk[N];
int p[N];
int fa[N],sz[N],son[N],top[N],deep[N];
void dfs(int x,int fr)
{
	fa[x]=fr;
	sz[x]=1;
	rep0(i,0,lk[x].size())
	{
		int y=lk[x][i];
		if(y==fr)continue;
		dfs(y,x);
		sz[x]+=sz[y];
		if(sz[y]>sz[son[x]])son[x]=y;
	}
}
void add(int x,s64 f[2])
{
	while(x)
	{
		int y=fa[top[x]];
		s64 f0[2]={max(seg[top[x]](0),seg[top[x]](1)),seg[top[x]](0)};
		seg[top[x]].add(deep[x],f);
		f[0]=max(seg[top[x]](0),seg[top[x]](1))-f0[0];
		f[1]=seg[top[x]](0)-f0[1];
		x=y;
	}	
}
void dfs2(int x,int fr)
{
	rep0(i,0,lk[x].size())
	{
		int y=lk[x][i];
		if(y==fr)continue;
		dfs2(y,x);
	}
	
	if(x==son[fa[x]])return ;
	int k=0;
	for(int y=x;y;y=son[y]){deep[y]=++k;top[y]=x;}
	seg[x].init(k);

		for(int y=x;y;y=son[y])
		{
			s64 f[2]={0,p[y]};
			rep0(i,0,lk[y].size())
			{
				int z=lk[y][i];
			if(z!=fa[y]&&z!=son[y])
			{
				f[0]+=max(seg[z](1),seg[z](0));
				f[1]+=seg[z](0);
			}
			}	
			seg[x].add(deep[y],f);
		}
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m;char type[5];
	scanf("%d%d%s",&n,&m,type);
	s64 sum=0;
	rep(i,1,n)sum+=(p[i]=read());
	rep(i,1,n-1)
	{
		int x=read(),y=read();
		lk[x].push_back(y);
		lk[y].push_back(x);
	}
	dfs(1,0);
	dfs2(1,0);
//	cout<<max(seg[1](0),seg[1](1))<<endl;
	static const s64 inf=1e15;
	while(m--)
	{
		int a=read(),x=read(),b=read(),y=read();
		x^=1;y^=1;
		s64 f[2];
		f[x]=0;f[x^1]=-inf;
		add(a,f);
		f[y]=0;f[y^1]=-inf;
		add(b,f);
		
		s64 mx=max(seg[1](0),seg[1](1));
		if(mx<0)puts("-1");
		else printf("%lld\n",sum-mx);
		
		f[y]=0;f[y^1]=inf;
		add(b,f);
		f[x]=0;f[x^1]=inf;
		add(a,f);
	}
}
