#include<bits/stdc++.h>
using namespace std;

template <typename T> void chmax(T &x,const T &y)
{
	if(x<y)x=y;
}
typedef long long s64;
#define rep(i,l,r) for(int i=l;i<=r;++i)
#define rep0(i,l,r) for(int i=l;i<int(r);++i)
const int N=1e5+5;
const s64 inf=1e18;
vector<int>lk[N];
int p[N],fa[N];
s64 f[N][2];bool mark[N][2];
void up(int x)
{
	if(!mark[x][0])f[x][0]=0;
	if(!mark[x][1])f[x][1]=p[x];
	rep0(i,0,lk[x].size())
	{
		int y=lk[x][i];
		if(y==fa[x])continue;
		f[x][0]+=max(f[y][0],f[y][1]);
		f[x][1]+=f[y][0];
	}
}
void dfs(int x,int fr)
{
	fa[x]=fr;
	rep0(i,0,lk[x].size())
	{
		int y=lk[x][i];
		if(y==fr)continue;
		dfs(y,x);
	}
	up(x);
}
void add(int x)
{
	while((x=fa[x]))up(x);
}

int main()
{
	freopen("1.in","r",stdin);
	freopen("1.out","w",stdout);
	int n,m;char type[5];
	scanf("%d%d%s",&n,&m,type);
	s64 sum=0;
	rep(i,1,n){scanf("%d",p+i);sum+=p[i];}
	rep(i,1,n-1)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		lk[x].push_back(y);
		lk[y].push_back(x);
	}
	dfs(1,0);
	while(m--)
	{
		int a,x,b,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		x^=1;y^=1;
		s64 tmp1=f[a][x^1];
		f[a][x^1]=-inf;mark[a][x^1]=1;
		add(a);
		s64 tmp2=f[b][y^1];mark[b][y^1]=1;
		f[b][y^1]=-inf;
		add(b);
		mark[a][x^1]=mark[b][y^1]=0;
		
		s64 mx=max(f[1][0],f[1][1]);
		if(mx<0)puts("-1");
		else printf("%lld\n",sum-mx);
		
		f[b][y^1]=tmp2;
		add(b);
		f[a][x^1]=tmp1;
		add(a);
	}
}
