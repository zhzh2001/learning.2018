#include<bits/stdc++.h>
#define MAXN 5005
using namespace std;
int read(){
	char c;int x;while(c=getchar(),c<'0'||c>'9');x=c-'0';
	while(c=getchar(),c>='0'&&c<='9') x=x*10+c-'0';return x;
}
void print(int x){
	if(x/10) print(x/10);
	putchar(x%10+'0');
}
int n,m,cnt,head[MAXN<<1],nxt[MAXN<<1],go[MAXN<<1];
int sta[MAXN],top,out[MAXN];
void add(int x,int y){
	go[cnt]=y;nxt[cnt]=head[x];head[x]=cnt;cnt++;
	go[cnt]=x;nxt[cnt]=head[y];head[y]=cnt;cnt++;
}
vector<int> q[MAXN];
namespace uck{
	int vis[MAXN];
	void solve(int x){
		vis[x]=1;sta[++top]=x;int l=q[x].size();
		for(int i=0;i<l;i++){
			int a=q[x][i];
			if(vis[a]) continue;
			solve(a);
		}
	}
}
namespace ccf{
	int sta[MAXN][MAXN],top[MAXN],vis[MAXN];
	void solve(int x,int p,int p1,int p2){
		vis[x]=1;sta[p][++top[p]]=x;int l=q[x].size();
		for(int i=0;i<l;i++){
			int a=q[x][i];
			if(vis[a]) continue;
			if((x==p1&&a==p2)||(x==p2&&a==p1)) continue;
			solve(a,p,p1,p2);
		}
	}
	int cp(int x,int y){
		if(top[x]<n) return 1;
		if(top[y]<n) return 0;
		for(int i=1;i<=n;i++)
		  if(sta[x][i]!=sta[y][i]) return sta[y][i]<sta[x][i];
		return 0;
	}
	void compareit(){
		int l=1,r=2;
		while(r<=n){
			if(cp(l,r)) l=r;
			r++;			
		}
		for(int i=1;i<=n;i++) out[i]=sta[l][i];
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	memset(head,-1,sizeof(head));
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		add(x,y);q[x].push_back(y);q[y].push_back(x);
	}
	for(int i=1;i<=n;i++)
	 sort(q[i].begin(),q[i].end());
	if(m==n-1){
		for(int i=1;i<=n;i++) uck::vis[i]=0;
		uck::solve(1);
		for(int i=1;i<=top;i++) print(sta[i]),putchar(' ');
	}
	else{
		int p=1;
		for(int i=0;i<cnt;i+=2,p++){
			for(int j=1;j<=n;j++) ccf::vis[j]=0;
			ccf::solve(1,p,go[i],go[i^1]);
		}
		ccf::compareit();
		for(int i=1;i<=n;i++) print(out[i]),putchar(' ');
	}
	return 0;
}
