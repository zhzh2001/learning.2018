#include<bits/stdc++.h>
#define MAXN 100005
#define ll long long
using namespace std;
int read(){
	char c;int x;while(c=getchar(),c<'0'||c>'9');x=c-'0';
	while(c=getchar(),c>='0'&&c<='9') x=x*10+c-'0';return x;
}
void print(int x){
	if(x/10) print(x/10);
	putchar(x%10+'0');
}
int n,m,cnt,p[MAXN],vis[MAXN];
int head[MAXN<<1],nxt[MAXN<<1],go[MAXN<<1];
ll f[MAXN][2],ans,inf;
string s;
void add(int x,int y){
	go[cnt]=y;nxt[cnt]=head[x];head[x]=cnt;cnt++;
	go[cnt]=x;nxt[cnt]=head[y];head[y]=cnt;cnt++;
}
void dp(int x,int fa){
	f[x][1]=p[x];
	for(int i=head[x];i!=-1;i=nxt[i]){
		int to=go[i];
		if(to==fa) continue;dp(to,x);
		f[x][1]+=min(f[to][0],f[to][1]);
		f[x][0]+=f[to][1];
	}
	if(vis[x]==1) f[x][0]=inf;
	if(vis[x]==-1) f[x][1]=inf;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();cin>>s;
	memset(head,-1,sizeof(head));
	for(int i=1;i<=n;i++) p[i]=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		add(x,y);
	}
	inf=1e18;memset(f,0,sizeof(f));
	for(int i=1;i<=m;i++){
		int x=read(),y=read(),u=read(),v=read();
		vis[x]=y?1:-1;vis[u]=v?1:-1;
		dp(1,0);ans=0;
		ans=min(f[1][0],f[1][1]);
		if(ans>=inf) puts("-1");
		else printf("%lld\n",ans);
		memset(f,0,sizeof(f));
		memset(vis,0,sizeof(vis));
	}		
	return 0;
}
