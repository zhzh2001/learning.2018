#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for(int i=(a),i_##END_=(b);i<=i_##END_;++i)
#define REP(i,a,b) for(int i=(a),i_##END_=(b);i>=i_##END_;--i)
#define M 100005
#define N
#define fi first
#define se second
typedef long long ll;
typedef unsigned long long ull;
typedef double db;
typedef pair<int,int>Pa;
void Rd(int &x){
	x=0;char c;
	while(c=getchar(),c<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),c>47);
}
struct edge{
	int to,nxt;
	edge(int _to=0,int _nxt=0):to(_to),nxt(_nxt){}
}G[M<<1];
int head[M];
int tol;
void addedge(int a,int b){
	G[tol]=edge(b,head[a]);
	head[a]=tol++;
}
int n,m;
int A[M];
namespace P1{
	int Mark[M];
	int dp[M][2];
	void dfs(int x,int f){
		dp[x][1]=dp[x][0]=0;
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(y==f)continue;
			dfs(y,x);
			if(!~dp[y][0]||(!Mark[x]&&!Mark[y])){
				dp[x][0]=-1;
				return;
			}
			if(!Mark[y]){
				dp[x][0]=-1;
				dp[x][1]+=dp[y][0];
			}else if(Mark[y]==1){
				if(~dp[x][0])dp[x][0]+=dp[y][1];
				dp[x][1]+=dp[y][1];
			}else {
				if(~dp[x][0])dp[x][0]+=dp[y][1];
				dp[x][1]+=min(dp[y][0],dp[y][1]);
			}
		}
		dp[x][1]+=A[x];
		if(!~dp[x][0]||(Mark[x]==1))dp[x][0]=1e9;
		else if(Mark[x]==0)dp[x][1]=1e9;
	}
	void Solve(){
		int a,b,x,y;
		while(m--){
			Rd(a);Rd(x);Rd(b);Rd(y);
			FOR(i,1,n)Mark[i]=-1;
			Mark[a]=x;
			Mark[b]=y;
			dfs(1,0);
			printf("%d\n",min(dp[1][0],dp[1][1]));
		}
	}
}
namespace P2{
	int Mark[M];
	ll dp[M][2];
	ll Dp[M][2];
	int Fa[M];
	void dfs1(int x,int f){
		Fa[x]=f;
		Dp[x][1]=A[x];
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(y==f)continue;
			dfs1(y,x);
			Dp[x][0]+=Dp[y][1];
			Dp[x][1]+=min(Dp[y][1],Dp[y][0]);
		}
	}
	int T;
	int Need[M];
	void dfs(int x,int f){
		dp[x][1]=dp[x][0]=0;
		for(int i=head[x];~i;i=G[i].nxt){
			int y=G[i].to;
			if(y==f)continue;
			if(Need[y]==T){
				dfs(y,x);
				if(!~dp[y][0]||(!Mark[x]&&!Mark[y])){
					dp[x][0]=-1;
					return;
				}
				if(!Mark[y]){
					dp[x][0]=-1;
					dp[x][1]+=dp[y][0];
				}else if(Mark[y]==1){
					if(~dp[x][0])dp[x][0]+=dp[y][1];
					dp[x][1]+=dp[y][1];
				}else {
					if(~dp[x][0])dp[x][0]+=dp[y][1];
					dp[x][1]+=min(dp[y][0],dp[y][1]);
				}
			}else {
				if(~dp[x][0])dp[x][0]+=Dp[x][1];
				dp[x][1]+=min(Dp[x][0],Dp[x][1]);
			}
		}
		dp[x][1]+=A[x];
		if(!~dp[x][0]||(Mark[x]==1))dp[x][0]=1e18;
		else if(Mark[x]==0)dp[x][1]=1e18;
	}
	void Solve(){
		int a,b,x,y;
		dfs1(1,0);
		FOR(i,1,n)Mark[i]=-1;
		while(m--){
			Rd(a);Rd(x);Rd(b);Rd(y);
			Mark[a]=x;
			Mark[b]=y;
			int pos=a;
			T++;
			while(pos!=1){
				Need[pos]=T;
				pos=Fa[pos];
			}
			pos=b;
			while(pos!=1){
				Need[pos]=T;
				pos=Fa[pos];
			}
			dfs(1,0);
			printf("%lld\n",min(dp[1][0],dp[1][1]));
			Mark[a]=-1;
			Mark[b]=-1;
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char type[10];
	Rd(n);Rd(m);
	scanf("%s",type);
	FOR(i,1,n)head[i]=-1;
	FOR(i,1,n)Rd(A[i]);
	int a,b;
	FOR(i,1,n-1){
		Rd(a);Rd(b);
		addedge(a,b);
		addedge(b,a);
	}
	if(n<=2000&&m<=2000)P1::Solve();
	else P2::Solve();
	return 0;
}
