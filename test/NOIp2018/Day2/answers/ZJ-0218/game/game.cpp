#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for(int i=(a),i_##END_=(b);i<=i_##END_;++i)
#define REP(i,a,b) for(int i=(a),i_##END_=(b);i>=i_##END_;--i)
#define M 1000005
#define N
#define fi first
#define se second
typedef long long ll;
typedef unsigned long long ull;
typedef double db;
typedef pair<int,int>Pa;
void Rd(int &x){
	x=0;char c;
	while(c=getchar(),c<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),c>47);
}
const int md=1e9+7;
int pow_mod(int x,int k){
	int res=1;
	while(k){
		if(k&1)res=(ll)res*x%md;
		x=(ll)x*x%md;
		k>>=1;
	}
	return res;
}
int Fac[M],Inv[M];
void Init(){
	Fac[0]=Fac[1]=1;
	Inv[0]=Inv[1]=1;
	FOR(i,2,M-1){
		Fac[i]=(ll)Fac[i-1]*i%md;
		Inv[i]=(ll)(md-md/i)*Inv[md%i]%md;
	}
	FOR(i,1,M-1)Inv[i]=(ll)Inv[i]*Inv[i-1]%md;
}
int C(int x,int y){
	if(x<y)return 0;
	return (ll)Fac[x]*Inv[y]%md*Inv[x-y]%md;
}
int n,m;
namespace P2{
	void Solve(){
		printf("%lld\n",(ll)4*pow_mod(3,m-1)%md);
	}
}
namespace P3{
	void Solve(){
		printf("%d\n",pow_mod(2,m*n));
	}
}
namespace P4{
	void Solve(){
		if(m==1)printf("%d\n",8);
		else if(m==2)printf("%d\n",36);
		else if(m==3)printf("%d\n",112);
		else printf("%lld\n",(ll)112*pow_mod(3,m-3)%md);
	}
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	Rd(n);Rd(m);
	if(n>m)swap(n,m);
	if(n==2)P2::Solve();
	else if(n==1)P3::Solve();
	else P4::Solve();
	return 0;
}
