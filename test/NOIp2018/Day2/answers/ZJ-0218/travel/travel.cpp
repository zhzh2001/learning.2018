#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for(int i=(a),i_##END_=(b);i<=i_##END_;++i)
#define REP(i,a,b) for(int i=(a),i_##END_=(b);i>=i_##END_;--i)
#define M 5005
#define N
#define fi first
#define se second
typedef long long ll;
typedef unsigned long long ull;
typedef double db;
typedef pair<int,int>Pa;
void Rd(int &x){
	x=0;char c;
	while(c=getchar(),c<48);
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),c>47);
}
int n;
int m;
vector<int>G[M];
namespace P1{
	bool Mark[M];
	int Ans[M];
	int cnt;
	void dfs(int x){
		Ans[++cnt]=x;
		Mark[x]=1;
		FOR(i,0,G[x].size()-1){
			int y=G[x][i];
			if(Mark[y])continue;
			dfs(y);
		}
	}
	void Solve(){
		dfs(1);
		FOR(i,1,n){
			printf("%d",Ans[i]);
			if(i<n)printf(" ");
		}
		puts("");
	}
}
namespace P2{
	int Mark[M];
	int A[M];
	int cnt;
	int Ans[M];
	int T;
	int nx,ny;
	void dfs(int x){
		A[++cnt]=x;
		Mark[x]=T;
		FOR(i,0,G[x].size()-1){
			int y=G[x][i];
			if(Mark[y]==T||(y==ny&&x==nx)||(y==nx&&x==ny))continue;
			dfs(y);
		}
	}
	int tot;
	int Cir[M];
	int Dep[M];
	int Now[M];
	void dfs1(int x,int f){
		Dep[x]=Dep[f]+1;
		Now[Dep[x]]=x;
		FOR(i,0,G[x].size()-1){
			int y=G[x][i];
			if(y==f)continue;
			if(!Dep[y])dfs1(y,x);
			else {
				FOR(i,Dep[y],Dep[x])Cir[++tot]=Now[i];
			}
		}
	}
	bool Judge(){
		if(!Ans[1])return 1;
		FOR(i,1,n){
			if(Ans[i]<A[i])return 0;
			if(Ans[i]>A[i])return 1;
		}
		return 0;
	}
	void Solve(){
		dfs1(1,0);
		Cir[tot+1]=Cir[1];
		FOR(i,1,tot){
			nx=Cir[i],ny=Cir[i+1];
			T=i;
			cnt=0;
			dfs(1);
			if(Judge()){
				FOR(j,1,n)Ans[j]=A[j];
			}
		}
		FOR(i,1,n){
			printf("%d",Ans[i]);
			if(i<n)printf(" ");
		}
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	Rd(n);Rd(m);
	int a,b;
	FOR(i,1,m){
		Rd(a);Rd(b);
		G[a].push_back(b);
		G[b].push_back(a);
	}
	FOR(i,1,n)sort(G[i].begin(),G[i].end());
	if(m==n-1)P1::Solve();
	else P2::Solve();
	return 0;
}
