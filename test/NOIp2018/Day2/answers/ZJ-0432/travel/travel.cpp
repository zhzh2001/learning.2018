#include<iostream>
#include<cstdio>
#include<algorithm>
#include<vector>
#define N 5555
using namespace std;
int n,m;
vector<int> v[N];
int pos[N],cnt=0;
int ans[N];
int dfn[N],sett=0,low[N];
int heap[N],size=0;
int lib[N],libal=0;
bool ban[N][N];
bool check() {
	for(int i=1; i<=n; i++) {
		if(ans[i]<pos[i])return false;
		if(pos[i]<ans[i])return true;
		}
	return false;
	}
void dfs(int s,int fa) {
	pos[++cnt]=s;
	for(int i=0; i<v[s].size(); i++) {
		int to=v[s][i];
		if(to==fa||ban[s][to])continue;
		dfs(to,s);
		}
	}
void tarjan(int x,int fa) {
	dfn[x]=low[x]=++sett;
	heap[++size]=x;
	int p=size;
	for(int i=0; i<v[x].size(); i++) {
		int to=v[x][i];
		if(to==fa)continue;
		if(!dfn[to])tarjan(to,x),low[x]=min(low[x],low[to]);
		else low[x]=min(low[x],dfn[to]);
		}
	if(low[x]>=dfn[x]) {
		//cout<<"***"<<dfn[x]<<" "<<p<<" "<<size<<endl;
		if(size<=p) {
			size=p-1;
			return;
			}
		for(int i=p; i<=size; i++) {
			lib[++libal]=heap[i];
			}
		size=p-1;
		return;
		}
	}
int main() {
	//freopen("lbi.txt","w",stdout);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1; i<=m; i++) {
		int a,b;
		scanf("%d%d",&a,&b);
		v[a].push_back(b);
		v[b].push_back(a);
		}
	for(int i=1; i<=n; i++)sort(v[i].begin(),v[i].end());
	if(m==n-1) {
		cnt=0;
		dfs(1,0);
		for(int i=1; i<=n; i++)printf("%d ",pos[i]);
		}
	else {
		for(int i=1; i<=n; i++)ans[i]=n;
		tarjan(1,0);
//		for(int i=1; i<=libal; i++)cout<<lib[i]<<" ";
//		cout<<endl;
		for(int i=1; i<=libal; i++) {
	//		cout<<lib[i]<<" "<<lib[(i%libal)+1]<<endl;
			ban[lib[i]][lib[(i%libal)+1]]=ban[lib[(i%libal)+1]][lib[i]]=true;
			cnt=0;
			dfs(1,0);
//			for(int r=1; r<=n; r++)cout<<pos[r]<<" ";
//			cout<<endl;
			if(check())for(int i=1; i<=n; i++)ans[i]=pos[i];
			ban[lib[i]][lib[(i%libal)+1]]=ban[lib[(i%libal)+1]][lib[i]]=false;
			}
		for(int i=1; i<=n; i++)printf("%d ",ans[i]);
		}
	}


