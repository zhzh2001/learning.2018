#include<iostream>
#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
int n,m;
const ll mod=1e9+7;
ll qpow(ll a,ll b) {
	ll ans=1;
	for(; b; b>>=1,a=a*a%mod)if(b&1)ans=ans*a%mod;
	return ans;
	}
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if(n>m)swap(n,m);
	if(n==1) {
		cout<<qpow(2,m)%mod;
		return 0;
		}
	if(n==2) {
		cout<<4*qpow(3,m-1)%mod;
		return 0;
		}
	if(n==3) {
		cout<<7*qpow(4,m-1)%mod;
		return 0;
		}
	else {
		cout<<((n-1)*3+1)*qpow(n+1,m-1)%mod;
		return 0;
		}
	}

