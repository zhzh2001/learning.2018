#include<bits/stdc++.h>
using namespace std;
const long long mo=1000000007;
int c2[1000005],c3[1000005],c4[1000005];
int n,m,ans,ans1,ans2,ans3;
int ksm(int x,int y)
{
	int z=1;
	while(y)
	{
		if(y&1)z=1ll*z*x%mo;
		x=1ll*x*x%mo; y=y>>1;
	}
	return z;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n>m)swap(n,m);
	c2[0]=c3[0]=c4[0]=1;
	for(int i=1;i<=m;i++)
	{
		c2[i]=c2[i-1]*2ll%mo;
		c3[i]=c3[i-1]*3ll%mo;
		c4[i]=c4[i-1]*4ll%mo;
	}
	int n2=ksm(2,mo-2),n3=ksm(3,mo-2),n4=ksm(4,mo-2);
	if(n==1)
	{
		ans=ksm(2,m);
		printf("%d\n",ans); return 0;
	}
	if(n==2)
	{
		ans=4;
		for(int i=2;i<=m;i++)ans=ans*3%mo;
		printf("%d\n",ans); return 0;
	}
	ans1=4;
	ans1=1ll*ans1*c4[n-2]%mo;
	ans1=1ll*ans1*c3[m-n]%mo;
	ans1=1ll*ans1*c2[n-1]%mo;
	ans=(ans+ans1)%mo;
	ans2=4;
	ans2=1ll*ans2*c4[n-3]%mo;
	ans2=1ll*ans2*c3[m-n]%mo;
	ans2=1ll*ans2*c2[n-1]%mo;
	if(n>=4)ans2=5ll*ans2*n4%mo;else
	if(m>=4)ans2=4ll*ans2*n3%mo;else
	ans2=3ll*ans2*n2%mo;
	ans=(ans+ans2)%mo;
	for(int i=4;i<=n;i++)
	{
		ans3=8;
		ans3=1ll*ans3*c4[n-i]%mo;
		ans3=1ll*ans3*c3[m-n]%mo*c2[n-1]%mo;
		if(n>=i+1)ans3=5ll*ans3*n4%mo;else
		if(m>=i+1)ans3=4ll*ans3*n3%mo;else
		ans3=3ll*ans3*n2%mo;
		ans=(ans+2ll*ans3)%mo;
	}
	for(int i=n+1;i<=m;i++)
	{
		ans3=6;
		ans3=1ll*ans3*c3[m-i]%mo;
		ans3=1ll*ans3*c2[n-1]%mo;
		if(m>=i+1)ans3=4ll*ans3*n3%mo;else
		ans3=3ll*ans3*n2%mo;
		ans=(ans+ans3)%mo;
	}
	ans3=2;
	ans3=1ll*ans3*c3[m-n]%mo;
	ans3=1ll*ans3*c2[n-1]%mo;
	if(m>=n+1)ans3=4ll*ans3*n3%mo;else
	ans3=3ll*ans3*n2%mo;
	ans=(ans+ans3)%mo;
	ans3=6ll*c2[n-2]%mo;
	ans=(ans+ans3)%mo;
	printf("%d\n",ans);
}
