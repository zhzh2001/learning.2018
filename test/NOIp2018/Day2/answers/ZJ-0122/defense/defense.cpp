#include <iostream>
#include <algorithm>
#include <cstring>
#include <cstdio>
#include <cstdlib>
using namespace std;

#define LL long long

struct edge{int Next,to;};
edge G[200100];
int head[100100];
LL Value[100010];
string TYPE;
int N,M,cnt=2;
bool flag=true;
int a,x,b,y;

inline void Add_Edge(int u,int v){
	G[cnt].to=v;
	G[cnt].Next=head[u];
	head[u]=cnt++;
	return;
}

LL Ans=2147483647LL;
bool Choose[10000];

bool Judge(){
	for(int i=2;i<cnt;i+=2){
		int u=G[i].to,v=G[i^1].to;
		if(!Choose[u] && !Choose[v]) return false;
	}	
	return true;
}

void DFS(int pos,LL Val){
	if(pos>=N+1){
		if(Judge()) Ans=min(Ans,Val);
		return;
	}
	if(pos==a){
		if(x==1){Choose[pos]=true;DFS(pos+1,Val+Value[pos]);Choose[pos]=false;}
		else if(x==0) DFS(pos+1,Val);
	}
	else if(pos==b){
		if(y==1){Choose[pos]=true;DFS(pos+1,Val+Value[pos]);Choose[pos]=false;}
		else if(y==0) DFS(pos+1,Val);
	}
	else{
		Choose[pos]=true;
		DFS(pos+1,Val+Value[pos]);
		Choose[pos]=false;
		DFS(pos+1,Val);
	}
	return;
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&N,&M);
	cin>>TYPE;
	for(int i=1;i<=N;++i)
		scanf("%lld",&Value[i]);
	for(int i=1;i<=N-1;++i){
		int u,v;
		scanf("%d%d",&u,&v);
		Add_Edge(u,v);
		Add_Edge(v,u);
		if(u!=v+1 && v!=u+1) flag=false;
	}
	for(int i=1;i<=M;++i){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		Ans=2147483647LL;
		DFS(1,0);
		if(Ans==2147483647LL) printf("-1\n");
		else printf("%lld\n",Ans);
	}
	
	return 0;
}

