#include <iostream>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <vector>
using namespace std;

struct edge{int Next,to;};
edge G[10010];
int head[5010],DFN[5010],LOW[5010],EDCC_ID[5010],EDCC_Count[5010];
int N,M,cnt=2,Index=0,EDCC_Index=0;
vector<int> Ans,Res;
bool vis[5010],Bridge[5010];

inline void Add_Edge(int u,int v){
	G[cnt].to=v;
	G[cnt].Next=head[u];
	head[u]=cnt++;
	return;
}

void Tarjan(int u,int InEdge){
	DFN[u]=LOW[u]=++Index;
	for(int i=head[u];i;i=G[i].Next){
		int v=G[i].to;
		if(!DFN[v]){
			Tarjan(v,i);
			LOW[u]=min(LOW[u],LOW[v]);
			if(LOW[v]>DFN[u]) Bridge[i]=Bridge[i^1]=true;
		}
		if((i^1)!=InEdge) LOW[u]=min(LOW[u],DFN[v]);
	}
	return;
}

void DFS_EDCC(int u){
	vis[u]=true;
	EDCC_ID[u]=EDCC_Index;
	++EDCC_Count[EDCC_Index];
	for(int i=head[u];i;i=G[i].Next){
		int v=G[i].to;
		if(vis[v]) continue;
		if(EDCC_ID[v] || Bridge[i]) continue;
		DFS_EDCC(v);
	}
	return;
}

void DFSA(int u,int fa){
	vis[u]=true;
	Ans.push_back(u);
	vector<int> Vec;
	for(int i=head[u];i;i=G[i].Next){
		int v=G[i].to;
		if(v==fa) continue;
		if(vis[v]) continue;
		Vec.push_back(v);
	}
	sort(Vec.begin(),Vec.end());
	for(int i=0;i<(int)Vec.size();++i){
		int v=Vec[i];
		DFSA(v,u);
	}
	return;
}

void DFSB(int u,int fa,int E){
	Res.push_back(u);
	vector<int> Vec;
	for(int i=head[u];i;i=G[i].Next){
		if(i==E || (i^1)==E) continue;
		int v=G[i].to;
		if(v==fa) continue;
		Vec.push_back(v);
	}
	sort(Vec.begin(),Vec.end());
	for(int i=0;i<(int)Vec.size();++i){
		int v=Vec[i];
		DFSB(v,u,E);
	}
	return;
}

void Solve1(){
	DFSA(1,0);
	for(int i=0;i<(int)Ans.size();++i){
		printf("%d",Ans[i]);
		if(i!=(int)Ans.size()-1) printf(" ");
	}
	printf("\n");
	return;
}

void Solve2(){
	Ans.push_back(99999999);
	for(int i=1;i<=N;++i)
		if(!DFN[i]) Tarjan(i,0);
	for(int i=1;i<=N;++i)
		if(!EDCC_ID[i]){++EDCC_Index;DFS_EDCC(i);}
	for(int i=2;i<cnt;i+=2){
		int u=G[i].to,v=G[i^1].to;
		if(EDCC_ID[u]==EDCC_ID[v]){
			Res.clear();
			DFSB(1,0,i);
			if(Res<Ans) Ans=Res;
		}
	}
	for(int i=0;i<(int)Ans.size();++i){
		printf("%d",Ans[i]);
		if(i!=(int)Ans.size()-1) printf(" ");
	}
	printf("\n");
	return;
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	scanf("%d%d",&N,&M);
	for(int i=1;i<=M;++i){
		int u,v;
		scanf("%d%d",&u,&v);
		Add_Edge(u,v);
		Add_Edge(v,u);
	}
	if(M==N-1) Solve1();
	else Solve2();

	return 0;
}
