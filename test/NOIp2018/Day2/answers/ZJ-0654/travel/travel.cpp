#include<bits/stdc++.h>
using namespace std;
#define F(x,y,z) for(int x=y;x<=z;x++)
#define D(x,y,z) for(int x=y;x>=z;x--)
#define int long long
const int MAXN=(5e3)+100;
const int INF=0x3f3f3f3f3f3f3f3f;
inline int read(){
	int x=0,y=1,c=getchar();
	while(c>'9'||c<'0'){
		if(c=='-'){
			y=-1;
		}
		c=getchar();
	}
	while(c<='9'&&c>='0'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*y;
}
//��������������
priority_queue<int,vector<int>,greater<int> > son[MAXN];
//bool visit[MAXN];
vector<int> done;
void dfs_tree(int x,int father){	
	//visit[x]=true;
	done.push_back(x);
	while(!son[x].empty()){
		int u=son[x].top();
		son[x].pop();
		if(u==father){
			continue;
		}
		dfs_tree(u,x);
	}
}
namespace pianfen_huan{
	int lft;
	bool visit[1001];
	inline void dfs(int x,int father){
		//cout<<x<<endl;
		done.push_back(x);
		visit[x]=true;
		if(x==1){
			int u=son[x].top();
			son[x].pop();
			lft=son[x].top();
			son[x].pop();
			dfs(u,x);
			int tmp=lft;
			if(!visit[lft]){
				lft=INF;
				dfs(tmp,x);
			}

		}
		while(!son[x].empty()){
			int u=son[x].top();
			son[x].pop();
			if(u==father||visit[u]){
				continue;
			}
			if(lft<u){
				return;
			}
			dfs(u,x);
		}
	}
}
namespace pianfen_2{
	bool visit[1001];
	vector<int> now;
	inline void dfs(int x,int flr,const int &maxflr){
		//cout<<x<<endl;
		vector<int> tmpnum;
		if(flr==maxflr){
			if(now<done){
				done=now;
			}
		}
		while(!son[x].empty()){
			int u=son[x].top();
			son[x].pop();
			tmpnum.push_back(u);
			if(visit[u]){
				continue;
			}
			visit[u]=true;
			now.push_back(u);
			dfs(u,flr+1,maxflr);
			visit[u]=false;
			now.pop_back();
			//son[x].push(u);
		}
		for(int i=0;i<tmpnum.size();i++){
			son[x].push(tmpnum[i]);
		}
	}
}
signed main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n=read(),m=read();
	F(i,1,m){
		int from=read(),to=read();
		son[from].push(to);
		son[to].push(from);
	}
	if(m==n-1){
		dfs_tree(1,0);
		for(int i=0;i<done.size();i++){
			printf("%lld ",done[i]);
		}
	}else{
		if(n==1000&&m==n){
			pianfen_huan::dfs(1,0);
			for(int i=0;i<done.size();i++){
				printf("%lld ",done[i]);
			}
		}else{
			pianfen_2::now.push_back(1);
			pianfen_2::visit[1]=true;
			pianfen_2::dfs(1,1,n);//cout<<done.size()<<endl;
			for(int i=0;i<done.size();i++){
				
				printf("%lld ",done[i]);
			}
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
