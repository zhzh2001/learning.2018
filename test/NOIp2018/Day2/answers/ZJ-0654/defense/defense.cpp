#include<bits/stdc++.h>
using namespace std;
#define F(x,y,z) for(int x=y;x<=z;x++)
#define D(x,y,z) for(int x=y;x>=z;x--)
#define ini(x,y) memset(x,y,sizeof(x))
#define int long long
const int MAXN=(1e5)+100;
const int INF=0x3f3f3f3f3f3f3f3f;
const int MOD=1e9+7;
inline int read(){
	int x=0,y=1,c=getchar();
	while(c>'9'||c<'0'){
		if(c=='-'){
			y=-1;
		}
		c=getchar();
	}
	while(c<='9'&&c>='0'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*y;
}
//xhk AK IOI,CTSC,NOI,ZJOI,NOIp
int cost[MAXN],from,isfrom,to,isto;
vector<int> son[MAXN]; 
int f[MAXN][2];
inline  void dfs(int x,int father){
	if(x!=1&&son[x].size()==1){
		f[x][0]=0;
		f[x][1]=cost[x];
		f[from][!isfrom]=INF;
		f[to][!isto]=INF;
	}
	if(f[x][0]!=INF||f[x][1]!=INF){
		f[from][!isfrom]=INF;
		f[to][!isto]=INF;
		return;
	}
	f[x][0]=0;
	f[x][1]=cost[x];
	f[from][!isfrom]=INF;
	f[to][!isto]=INF;
	for(int i=0;i<son[x].size();i++){
		int u=son[x][i];
		if(u==father){
			continue;
		}
		dfs(u,x);
		f[from][!isfrom]=INF;
		f[to][!isto]=INF;
		f[x][0]+=f[u][1];
		f[x][0]=min(f[x][0],INF);
		f[x][1]+=min(f[u][0],f[u][1]);
		f[x][1]=min(f[x][1],INF);
	}
}
map<pair<int,int>,bool> check;
signed main(){
	freopen("defense.in��","r",stdin);
	freopen("defense.out","w",stdout);
	int n=read(),m=read();
	string ceshitype;
	cin>>ceshitype;
	F(i,1,n){
		cost[i]=read();
	} 
	F(i,1,n-1){
		int from=read(),to=read();
		check[make_pair(from,to)]=true;
		check[make_pair(to,from)]=true;
		son[from].push_back(to);
		son[to].push_back(from);
	}
	F(i,1,m){
		from=read(),isfrom=read(),to=read(),isto=read();
		if(check[make_pair(from,to)]&&isfrom==0&&isto==0){
			printf("-1\n"); 
		}else{
			ini(f,0x3f);
			dfs(1,0);
			printf("%lld\n",min(f[1][1],f[1][0]));
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
} 
