#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;
LL n,m,mod,ans,l,r,L,R,t;
char ch;
void read(LL &digit)
{
	digit=0;
	ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9')
	{
		digit=digit*10+ch-'0';
		ch=getchar();
	}
}
void wri(LL x)
{
	if (x>9) wri(x/10);
	putchar('0'+x%10);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	mod=1000000007;
	read(n);read(m);
	if (n>m) {t=n;n=m;m=t;}
	if (n==3&&m==3) wri(112);
	else if (n==1)
	{
		ans=1;
		for (LL i=1;i<=m;i++) ans=ans*2%mod;
		wri(ans);
	}
	else if (n==2)
	{
		ans=4;
		for (LL i=1;i<m;i++) ans=ans*3%mod;
		wri(ans);
	}
	return 0;
}
