#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
struct node{int s[5010];} son[5010];
int n,m,x[5010],y[5010],len[5010],ans[5010],tmp[5010],top,banx,bany,flag,Now,bo[5010];
char ch;
void read(int &digit)
{
	digit=0;
	ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9')
	{
		digit=digit*10+ch-'0';
		ch=getchar();
	}
}
void wri(int x)
{
	if (x>9) wri(x/10);
	putchar('0'+x%10);
}
void dfs(int x,int f)
{
	wri(x);putchar(' ');
	for (int i=1;i<=len[x];i++)
	if (son[x].s[i]!=f) dfs(son[x].s[i],x);
}
void dfs2(int x,int f)
{
	if (flag==1) return;
	bo[x]=Now;
	top++;
	tmp[top]=x;
	int y;
	for (int i=1;i<=len[x];i++)
	{
		y=son[x].s[i];
		if (y!=f)
		{
			if (x==banx&&y==bany) continue;
			if (x==bany&&y==banx) continue;
			if (bo[y]==Now) {flag=1;return;}
			dfs2(y,x);
		}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);read(m);
	memset(len,0,sizeof(len));
	for (int i=1;i<=m;i++)
	{
		read(x[i]);read(y[i]);
		len[x[i]]++;
		son[x[i]].s[len[x[i]]]=y[i];
		len[y[i]]++;
		son[y[i]].s[len[y[i]]]=x[i];
	}
	for (int i=1;i<=n;i++)
	sort(son[i].s+1,son[i].s+len[i]+1);
	if (m==n-1) dfs(1,0);
	else
	{
		for (int i=1;i<=n;i++) ans[i]=n+1;
		memset(bo,0,sizeof(bo));
		for (int i=1;i<=m;i++)
		{
			Now=i;
			banx=x[i];bany=y[i];
			top=0;
			flag=0;
			dfs2(1,0);
			if (flag==1||top<n) continue;
			flag=0;
			for (int j=1;j<=n;j++)
			{
				if (tmp[j]<ans[j]) {flag=1;break;}
				if (tmp[j]>ans[j]) break;
			}
			if (flag==1) for (int j=1;j<=n;j++) ans[j]=tmp[j];
		}
		for (int i=1;i<=n;i++) {wri(ans[i]);putchar(' ');}
	}
	return 0;
}
