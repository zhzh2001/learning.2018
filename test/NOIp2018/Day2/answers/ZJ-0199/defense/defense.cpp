#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;
LL n,m,x,y,X,Y,ty2,a,b,f[100010][3],g[100010][3],w[100010],oo,ans,fa[100010],Now,Father;
LL k,first[200010],A[200010],last[200010],ne[200010];
char ch,ty1;
void read(LL &digit)
{
	digit=0;
	ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9')
	{
		digit=digit*10+ch-'0';
		ch=getchar();
	}
}
void wri(LL x)
{
	if (x>9) wri(x/10);
	putchar('0'+x%10);
}
void dfs(LL x,LL F)
{
	LL y;
	f[x][1]=w[x];
	f[x][0]=0;
	for (LL i=first[x];i;i=ne[i])
	{
		y=A[i];
		if (y!=F)
		{
			dfs(y,x);
			f[x][1]=f[x][1]+f[y][2];
			f[x][0]=f[x][0]+f[y][1];
		}
	}
	if (x==a) f[x][X^1]=oo;
	if (x==b) f[x][Y^1]=oo;
	f[x][0]=min(oo,f[x][0]);
	f[x][1]=min(oo,f[x][1]);
	f[x][2]=min(f[x][0],f[x][1]);
}
void dfs2(LL x,LL F)
{
	fa[x]=F;
	LL y;
	f[x][1]=w[x];
	f[x][0]=0;
	for (LL i=first[x];i;i=ne[i])
	{
		y=A[i];
		if (y!=F)
		{
			dfs2(y,x);
			f[x][1]=f[x][1]+f[y][2];
			f[x][0]=f[x][0]+f[y][1];
		}
	}
	f[x][0]=min(oo,f[x][0]);
	f[x][1]=min(oo,f[x][1]);
	f[x][2]=min(f[x][0],f[x][1]);
}
void add(LL x,LL y)
{
	k++;
	A[k]=y;
	if (first[x]==0) first[x]=k;
	else ne[last[x]]=k;
	last[x]=k;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	oo=10000000000;
	read(n);read(m);
	ch=getchar();
	while(ch<'A'||ch>'C') ch=getchar();
	ty1=ch;
	read(ty2);
	for (LL i=1;i<=n;i++) read(w[i]);
	for (LL i=1;i<n;i++)
	{
		read(x);read(y);
		add(x,y);add(y,x);
	}
	if (n<=2000&&m<=2000)
	{
		for (LL i=1;i<=m;i++)
		{
			memset(f,0,sizeof(f));
			read(a);read(X);read(b);read(Y);
			dfs(1,0);
			ans=f[1][2];
			if (ans<oo) wri(ans);
			else {putchar('-');putchar('1');}
			putchar('\n');
		}
	}
	else
	{
		memset(f,0,sizeof(f));
		dfs2(1,0);
		for (int i=1;i<=n;i++)
			for (int j=0;j<=2;j++) g[i][j]=f[i][j];
		for (int i=1;i<=m;i++)
		{
			read(a);read(X);read(b);read(Y);
			g[a][X^1]=oo;
			g[a][2]=min(g[a][1],g[a][0]);
			Now=a;
			while(fa[Now]!=0)
			{
				Father=fa[Now];
				g[Father][1]=g[Father][1]-f[Now][2]+g[Now][2];
				g[Father][0]=g[Father][0]-f[Now][1]+g[Now][1];
				g[Father][0]=min(oo,g[Father][0]);
				g[Father][1]=min(oo,g[Father][1]);
				g[Father][2]=min(g[Father][0],g[Father][1]);
				Now=Father;
			}
			g[b][Y^1]=oo;
			g[b][2]=min(g[b][1],g[b][0]);
			Now=b;
			while(fa[Now]!=0)
			{
				Father=fa[Now];
				g[Father][1]=g[Father][1]-f[Now][2]+g[Now][2];
				g[Father][0]=g[Father][0]-f[Now][1]+g[Now][1];
				g[Father][0]=min(oo,g[Father][0]);
				g[Father][1]=min(oo,g[Father][1]);
				g[Father][2]=min(g[Father][0],g[Father][1]);
				Now=Father;
			}
			ans=g[1][2];
			if (ans<oo) wri(ans);
			else {putchar('-');putchar('1');}
			putchar('\n');
			Now=a;
			while(Now!=0)
			{
				for (int j=0;j<=2;j++) g[Now][j]=f[Now][j];
				Now=fa[Now];
			}
			Now=b;
			while(Now!=0)
			{
				for (int j=0;j<=2;j++) g[Now][j]=f[Now][j];
				Now=fa[Now];
			}
		}
	}
	return 0;
}
