#include<iostream>
#include<cstdio>
#include<vector>
#include<algorithm>
#include<memory.h>
using namespace std;
struct node
{
	int to,id;
	node(){};
	node(int x,int y):to(x),id(y){};
	bool operator <(node x) const
	{
		return to<x.to;
	}
};
int n,m,b[5005],c[5005],ans[5005],t;
vector<node> a[5005];
vector<int> d[5005];
void dfs(int x)
{
	cout<<x<<' ';
	b[x]=1;
	for (int i=0;i<d[x].size();i++)
	{
		if (b[d[x][i]]==0) dfs(d[x][i]);
	}
}
void fun1()
{
	int x,y;
	for (int i=1;i<=m;i++)
	{
		scanf("%d %d",&x,&y);
		d[x].push_back(y);
		d[y].push_back(x);
	}
	for (int i=1;i<=n;i++)
	{
		if (d[i].size()==0) continue;
		sort(d[i].begin(),d[i].end());
	}
	dfs(1);
}
void dfs(int x,int y)
{
	b[x]=1;
	c[++t]=x;
	for (int i=0;i<a[x].size();i++)
	{
		if (b[a[x][i].to]==0&&a[x][i].id!=y) dfs(a[x][i].to,y); 
	}
}
void fun2()
{
	int x,y;
	for (int i=1;i<=m;i++)
	{
		scanf("%d %d",&x,&y);
		a[x].push_back(node(y,i));
		a[y].push_back(node(x,i));
	}
	for (int i=1;i<=n;i++)
	{
		if (a[i].size()==0) continue;
		sort(a[i].begin(),a[i].end());
	}
	for (int i=1;i<=n;i++) ans[i]=n;
	for (int i=1;i<=m;i++)
	{
		memset(b,0,sizeof(b));
		t=0;
		dfs(1,i);
		if (t!=n) continue;
		int flag=0;
		for (int j=1;j<=n;j++)
		{
			if (c[j]>ans[j]) break;
			if (c[j]<ans[j]) 
			{
				flag=j;
				break;
			}
		}
		if (flag!=0) 
		{
			for (int j=flag;j<=n;j++)
			{
				ans[j]=c[j];
			}
		}
	}
	for (int i=1;i<=n;i++) cout<<ans[i]<<' ';
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	if (m==n-1) fun1();
	else fun2();
}
