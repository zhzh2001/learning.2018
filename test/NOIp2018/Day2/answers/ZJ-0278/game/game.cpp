#include<iostream>
#include<cstdio>
using namespace std;
#define MOD 1000000007
long long pow(long long x,int y)
{
	long long ans=1;
	while (y>0)
	{
		if (y&1) ans=ans*x%MOD;
		x=x*x%MOD;
		y/=2;
	}
	return ans;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	cin>>n>>m;
	if (n==1) 
	{
		cout<<pow(2,m);
		return 0;
	}
	if (m==1)
	{
		cout<<pow(2,n);
		return 0;
	}
	if (n==2)
	{
		cout<<pow(3,m-1)*4%MOD;
		return 0;
	}
	if (m==2)
	{
		cout<<pow(3,n-1)*4%MOD;
		return 0;
	}
	if (n==3&&m==3) 
	{
		cout<<112;
		return 0;
	}
}
