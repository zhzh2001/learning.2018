#include <bits/stdc++.h>
using namespace std;
#define rll register ll
#define IL inline
#define rep(i,h,t) for (ll i=h;i<=t;i++)
#define dep(i,t,h) for (ll i=t;i>=h;i--)
#define ll long long
#define me(x) memset(x,0,sizeof(x))
#define mid ((h+t)>>1)
#define mid2 ((h+t+1)>>1)
char ss[1<<24],*A=ss,*B=ss;
IL char gc()
{
  return A==B&&(B=(A=ss)+fread(ss,1,1<<24,stdin),A==B)?EOF:*A++;
}
template<class T>void read(T &x)
{
  rll f=1,c; while (c=gc(),c<48||c>57) if (c=='-') f=-1; x=(c^48);
  while (c=gc(),c>47&&c<58) x=(x<<3)+(x<<1)+(c^48); x*=f;
}
char sr[1<<24],z[20];ll Z,C=-1;
template<class T>void wer(T x)
{
  if (x<0) sr[++C]='-',x=-x;
  while (z[++Z]=x%10+48,x/=10);
  while (sr[++C]=z[Z],--Z);
}
IL void wer1()
{
  sr[++C]=' ';
}
IL void wer2()
{
  sr[++C]='\n';
}
template<class T>IL void maxa(T &x,T y)
{
  if (x<y) x=y;
}
template<class T>IL void mina(T &x,T y)
{
  if (x>y) x=y;
}
template<class T>IL T MAX(T x,T y)
{
  return x>y?x:y;
}
template<class T>IL T MIN(T x,T y)
{
  return x<y?x:y;
}
const ll mo=1e9+7;
ll p[100],n,m,cnt;
ll f[20][20];
ll ans[1000000][20];
void dfs(ll x,ll y,ll z)
{
  p[z]=f[x][y];
  if (x==n&&y==m)
  {
    cnt++;
    rep(i,1,z)
      ans[cnt][i]=p[i];
    return;
  }
  if (x<=n) dfs(x+1,y,z+1);
  if (y<=m) dfs(x,y+1,z+1);
}
IL ll ksm(ll x,ll y)
{
  if (y==0) return(1);
  if (y==1) return(x);
  ll ans=ksm(x,y/2);
  ans=(ans*ans)%mo;
  if (y%2==1) ans=(ans*x)%mo;
  return ans;
}
int main()
{
  freopen("game.in","r",stdin);
  freopen("game.out","w",stdout);
  read(n); read(m);
  if (n<=4&&m<=4)
  {
    ll x=(1<<(n*m))-1; ll ans2=0;
    ll cnt3=0;
    rep(i,0,x)
    {  
    rep(i1,1,n)
      rep(j1,1,m)
      {
        ll t=(i1-1)*m+j1;
        if ((i>>(t-1))&1) f[i1][j1]=1; else f[i1][j1]=0;
      }
    cnt=0;
    dfs(1,1,1);
    bool tt=0;
    rep(i,1,cnt-1)
      rep(j,1,n+m-1)
      {
        if (ans[i][j]<ans[i+1][j]) tt=1;
        if (ans[i][j]>ans[i+1][j]) break;
      }
    if (!tt)
    { 
    ans2++;
    }
  } 
  wer(ans2); wer2();
  } else
  {
  if (m<n) swap(n,m);
  if (n==2)
  {
    ll ans=4*ksm(3,m-1)%mo;
    wer(ans);
  } else
  if (n==3)
  {
    ll ans=112*ksm(3,m-3)%mo;
    wer(ans);
  } else
  if (n==4)
  {
    ll ans=2688*ksm(3,m-5)%mo;
    wer(ans);
  }
  }
  fwrite(sr,1,C+1,stdout);
  return 0;
}
