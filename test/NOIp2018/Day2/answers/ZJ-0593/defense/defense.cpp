#include <bits/stdc++.h>
using namespace std;
#define rll register ll
#define IL inline
#define rep(i,h,t) for (ll i=h;i<=t;i++)
#define dep(i,t,h) for (ll i=t;i>=h;i--)
#define ll long long
#define me(x) memset(x,0,sizeof(x))
#define mid ((h+t)>>1)
#define mid2 ((h+t+1)>>1)
char ss[1<<24],*A=ss,*B=ss;
IL char gc()
{
  return A==B&&(B=(A=ss)+fread(ss,1,1<<24,stdin),A==B)?EOF:*A++;
}
template<class T>void read(T &x)
{
  rll f=1,c; while (c=gc(),c<48||c>57) if (c=='-') f=-1; x=(c^48);
  while (c=gc(),c>47&&c<58) x=(x<<3)+(x<<1)+(c^48); x*=f;
}
char sr[1<<24],z[20];ll Z,C=-1;
template<class T>void wer(T x)
{
  if (x<0) sr[++C]='-',x=-x;
  while (z[++Z]=x%10+48,x/=10);
  while (sr[++C]=z[Z],--Z);
}
IL void wer1()
{
  sr[++C]=' ';
}
IL void wer2()
{
  sr[++C]='\n';
}
template<class T>IL void maxa(T &x,T y)
{
  if (x<y) x=y;
}
template<class T>IL void mina(T &x,T y)
{
  if (x>y) x=y;
}
template<class T>IL T MAX(T x,T y)
{
  return x>y?x:y;
}
template<class T>IL T MIN(T x,T y)
{
  return x<y?x:y;
}
const ll INF=1e12;
const ll N=2e5;
char cc[10];
ll a[N];
ll l,head[N],n,m;
struct re{
  ll a,b;
}e[N*2];
void arr(ll x,ll y)
{
  e[++l].a=head[x];
  e[l].b=y;
  head[x]=l;
}
ll x11,x22,x3,x4;
ll f[N][2];
void dfs(ll x,ll y)
{
  f[x][1]=a[x]; f[x][0]=0;
  for (rll u=head[x];u;u=e[u].a)
  {
    ll v=e[u].b;
    if (v!=y)
    {
      dfs(v,x);
      f[x][1]+=MIN(f[v][0],f[v][1]);
      mina(f[x][1],INF);
      f[x][0]+=f[v][1];
      mina(f[x][0],INF);
    }
  }
  if (x11==x)
  {
    if (x22==0) f[x][1]=INF; else f[x][0]=INF;
  }
  if (x3==x)
  {
    if (x4==0) f[x][1]=INF; else f[x][0]=INF;
  }
}
int main()
{
  freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout);
  ios::sync_with_stdio(false);
  cin>>n>>m; cin>>cc;
  rep(i,1,n) cin>>a[i];
  rep(i,1,n-1)
  {
    ll x,y;
    cin>>x>>y;
    arr(x,y); arr(y,x);
  }
  rep(i,1,m)
  {
    cin>>x11>>x22>>x3>>x4;
    dfs(1,0);
    ll ans=MIN(f[1][0],f[1][1]);
    if (ans<INF) wer(ans); else wer(-1);
    wer2(); 
  }
  fwrite(sr,1,C+1,stdout);
  return 0;
}
