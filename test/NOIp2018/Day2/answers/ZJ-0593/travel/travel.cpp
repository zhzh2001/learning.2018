#include <bits/stdc++.h>
using namespace std;
#define rint register int
#define IL inline
#define rep(i,h,t) for (int i=h;i<=t;i++)
#define dep(i,t,h) for (int i=t;i>=h;i--)
#define ll long long
#define me(x) memset(x,0,sizeof(x))
#define mid ((h+t)>>1)
#define mid2 ((h+t+1)>>1)
char ss[1<<24],*A=ss,*B=ss;
IL char gc()
{
  return A==B&&(B=(A=ss)+fread(ss,1,1<<24,stdin),A==B)?EOF:*A++;
}
template<class T>void read(T &x)
{
  rint f=1,c; while (c=gc(),c<48||c>57) if (c=='-') f=-1; x=(c^48);
  while (c=gc(),c>47&&c<58) x=(x<<3)+(x<<1)+(c^48); x*=f;
}
char sr[1<<24],z[20];int Z,C=-1;
template<class T>void wer(T x)
{
  if (x<0) sr[++C]='-',x=-x;
  while (z[++Z]=x%10+48,x/=10);
  while (sr[++C]=z[Z],--Z);
}
IL void wer1()
{
  sr[++C]=' ';
}
IL void wer2()
{
  sr[++C]='\n';
}
template<class T>IL void maxa(T &x,T y)
{
  if (x<y) x=y;
}
template<class T>IL void mina(T &x,T y)
{
  if (x>y) x=y;
}
template<class T>IL T MAX(T x,T y)
{
  return x>y?x:y;
}
template<class T>IL T MIN(T x,T y)
{
  return x<y?x:y;
}
const int N=1e4+10;
int n,m,l,jl1,jl2,head[N];
struct re{
  int a,b,from;
}e[N*2];
int ans[N],now[N],cnt2;
bool vis[N];
IL void arr(int x,int y)
{
  e[++l].a=head[x];
  e[l].b=y;
  e[l].from=x;
  head[x]=l;
}
void dfs(int x,int y,int x1,int y1)
{
  vector<int> ve;
  vis[x]=1;
  now[++cnt2]=x;
  int cnt=0;
  for (rint u=head[x];u;u=e[u].a)
  {
    int v=e[u].b;
    if (v!=y&&!(x1==x&&y1==v)&&!(x1==v&&y1==x)&&!vis[v]) ve.push_back(v);
  }
  sort(ve.begin(),ve.end());
  int l=(int)(ve.size())-1;
  rep(i,0,l) dfs(ve[i],x,x1,y1);
}
int main()
{
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  read(n); read(m);
  rep(i,1,m)
  {
    int x,y;
    read(x); read(y);
    arr(x,y); arr(y,x);
  }
  rep(i,1,n) ans[i]=n;
  if (n==m)
  {
    rep(i,1,l)
    if (i%2==1)
    {
      me(now); me(vis);
      jl1=e[i].from; jl2=e[i].b; cnt2=0;
      dfs(1,0,jl1,jl2);
      bool tt2=0;
      rep(j,1,n) if (!now[j]) tt2=1;
      if (tt2) continue;
      bool tt=0;
      rep(i,1,n)
      {
        if (ans[i]<now[i]) break;
        if (ans[i]>now[i])
        {
          tt=1;
          break;
        }
      }
      if (tt) rep(i,1,n) ans[i]=now[i];
    }
  } else
  {
    dfs(1,0,0,0);
    rep(i,1,n) ans[i]=now[i];
  }
  rep(i,1,n) wer(ans[i]),wer1();
  fwrite(sr,1,C+1,stdout);
  return 0;
}
