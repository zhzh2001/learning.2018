#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
ll dp[100010][2];
ll dp2[100010][2];
int p[100010],fa[100010],a,b,x,y,n,m,dep[100010];
char opt[10];
struct edge{
	int v,next;
}e[200010];
int head[100010],tot;
void addedge(int u,int v){
	e[++tot].v=v;
	e[tot].next=head[u];
	head[u]=tot;
}
void dfs(int now,int f){
	fa[now]=f;
	dep[now]=dep[fa[now]]+1;
	dp[now][0]=0;
	dp[now][1]=p[now];
	for(int i=head[now];i;i=e[i].next){
		if(e[i].v==f)continue;
		dfs(e[i].v,now);
		dp[now][0]+=dp[e[i].v][1];
		dp[now][1]+=min(dp[e[i].v][0],dp[e[i].v][1]);
	}
	if(a==now)dp[now][x^1]=100000000000000ll;
	if(b==now)dp[now][y^1]=100000000000000ll;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,opt);
	for(int i=1;i<=n;i++)scanf("%d",p+i);
	for(int i=1,u,v;i<n;i++)scanf("%d%d",&u,&v),addedge(u,v),addedge(v,u);
	if(n<=2000&&m<=2000){
		for(int i=1;i<=m;i++){
			scanf("%d%d%d%d",&a,&x,&b,&y);
			dfs(1,0);
			ll ans=min(dp[1][0],dp[1][1]);
			if(ans<=10000000000ll)printf("%lld\n",ans);
			else puts("-1");
		}
		fclose(stdin);
		fclose(stdout);
		return 0;
	}	
	dfs(1,0);
	for(int i=1;i<=m;i++){
		scanf("%d%d%d%d",&a,&x,&b,&y);
		dp2[b][y]=dp[b][y];
		dp2[b][y^1]=100000000000000ll;
		for(int la=b,k=fa[b];k;k=fa[la=k]){
			dp2[k][0]=dp[k][0];
			dp2[k][1]=dp[k][1];
			dp2[k][0]=dp2[k][0]-dp[la][1]+dp2[la][1];
			dp2[k][1]=dp2[k][1]-min(dp[la][1],dp[la][0])+min(dp2[la][1],dp2[la][0]);
		}
		ll ans=dp2[1][1];
		if(ans<=10000000000ll)printf("%lld\n",ans);
		else puts("-1");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
