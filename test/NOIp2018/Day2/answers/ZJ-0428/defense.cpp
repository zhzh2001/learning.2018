#include<bits/stdc++.h>
#define rep(i,a,b)	for(int i=(a);i<=(b);++i)
#define rev(i,a,b)	for(int i=(a);i>=(b);--i)
using namespace std;
typedef long long LL;
void read(int &a){
	a=0;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')	ch=getchar();
	int f=1;if (ch=='-')	ch=getchar(),f=-1;
	while (ch>='0'&&ch<='9')	a=a*10+ch-48,ch=getchar();
	a=a*f;
}
void read(LL &a){
	a=0;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')	ch=getchar();
	LL f=1;if (ch=='-')	ch=getchar(),f=-1;
	while (ch>='0'&&ch<='9')	a=a*10+ch-48,ch=getchar();
	a=a*f;
}
void write(int a){
	if (!a)	return;
	if (a<0)	putchar('-'),write(-a);
	else write(a/10),putchar(a%10+48);
}
void write(LL a){
	if (!a)	return;
	if (a<0)	putchar('-'),write(-a);
	else write(a/10),putchar(a%10+48);
}
const int INF=2e9+7,N=3e5+5;
struct str1{
	int y,nxt;
}g[N<<1];
int h[N],w[N],dp[N][3];
int n,m;
bool f[11];int a[11],G=0;
void addedge(int x,int y){
	g[++G].nxt=h[x],g[G].y=y,h[x]=G;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	read(n),read(m);
	char ch=getchar(),C;while (ch<'A'||ch>'C')	ch=getchar();C=getchar();
	rep(i,1,n)	read(w[i]);
	rep(i,1,n-1)	{int x,y;read(x),read(y),addedge(x,y),addedge(y,x);	}
	if (n<=10){
		a[0]=1;rep(i,1,n)	a[i]=a[i-1]*2;
		int ans,sum,x1,y1,x2,y2;bool b;
		rep(i,1,m){
			read(x1),read(x2),read(y1),read(y2);
			ans=INF;
			rep(j,1,a[n]-1){
				if (x2!=((a[x1-1]&j)>0)||y2!=((a[y1-1]&j)>0))	continue;
				sum=0,b=true;rep(k,1,n)	sum+=a[k-1]&j?w[k]:0;
				if (j==a[n]-1)
					sum=sum+1,sum=sum-1;
				if (sum>ans)	continue;
				memset(f,false,sizeof(f));
				rep(k,1,n)
					for(int q=h[k];q;q=g[q].nxt)
						if (!(((a[k-1]&j)>0)||((a[g[q].y-1]&j)>0)))	{
							b=false; break;
						}
				if (b)	ans=sum;						
			}
			write(ans==INF?-1:ans),puts("");
		}
		return 0;
	}
	if (ch=='A'){
		int x1,y1,x2,y2;
		rep(i,1,m){
			read(x1),read(x2),read(y1),read(y2);
			rep(j,0,n)		dp[j][0]=dp[j][1]=INF;
			if (x1!=1&&y1!=1)	dp[1][0]=0,dp[1][1]=w[1];
			else if (x1==1)	{	if (x2)	dp[1][0]=INF,dp[1][1]=w[1];else dp[1][1]=INF,dp[1][0]=0;		}
			else if (y1==1)	{	if (y2)	dp[1][0]=INF,dp[1][1]=w[1];else dp[1][1]=INF,dp[1][0]=0;		}
			rep(j,2,n){
				dp[j][0]=min(dp[j][0],dp[j-1][1]),dp[j][1]=min(dp[j][1],min(dp[j-1][0],dp[j-1][1])+w[j]);
				if (x1==j)	{if (x2)	dp[j][0]=INF;else dp[j][1]=INF;}
				if (y1==j)	{if (y2)	dp[j][0]=INF;else dp[j][1]=INF;}
			}			
			write(min(dp[n][0],dp[n][1])==INF?-1:min(dp[n][0],dp[n][1])),puts("");
		}
		return 0;
	}
}
