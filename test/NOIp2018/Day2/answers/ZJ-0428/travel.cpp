#include<bits/stdc++.h>
#define rep(i,a,b)	for(int i=(a);i<=(b);++i)
#define rev(i,a,b)	for(int i=(a);i>=(b);--i)
using namespace std;
typedef long long LL;
void read(int &a){
	a=0;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')	ch=getchar();
	int f=1;if (ch=='-')	ch=getchar(),f=-1;
	while (ch>='0'&&ch<='9')	a=a*10+ch-48,ch=getchar();
	a=a*f;
}
void read(LL &a){
	a=0;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')	ch=getchar();
	LL f=1;if (ch=='-')	ch=getchar(),f=-1;
	while (ch>='0'&&ch<='9')	a=a*10+ch-48,ch=getchar();
	a=a*f;
}
void write(int a){
	if (!a)	return;
	if (a<0)	putchar('-'),write(-a);
	else write(a/10),putchar(a%10+48);
}
void write(LL a){
	if (!a)	return;
	if (a<0)	putchar('-'),write(-a);
	else write(a/10),putchar(a%10+48);
}
const int N=5e3+5;
struct str1{
	int x,y;
}a[N<<1];
int s[N],ans[N],g[N];
int n,m,l;
bool f[N];
bool cmp(str1 a,str1 b){
	return a.x<b.x||a.x==b.x&&a.y<b.y;
}
void dfs(int x,int q){
	f[x]=false;g[++l]=x;
	rep(i,s[x],2*m){
		if (a[i].x!=x)	break;	if (!f[a[i].y]||((x==a[q].x&&a[i].y==a[q].y)||(a[i].y==a[q].x&&x==a[q].y)))	continue;
		dfs(a[i].y,q);
	}
}
bool check(){
	rep(i,1,n)	if (ans[i]<g[i])	return false;
	else if (ans[i]>g[i])	return true;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n),read(m);
	rep(i,1,m)	read(a[2*i-1].x),read(a[2*i-1].y),a[2*i].x=a[2*i-1].y,a[2*i].y=a[2*i-1].x;
	sort(a+1,a+2*m+1,cmp);ans[1]=n;
	rev(i,2*m,1)	s[a[i].x]=i;
	rep(i,m<n?2*n:1,2*n){
		if (a[i].x<a[i].y)	continue;
		memset(f,true,sizeof(f));l=0;
		dfs(1,i);
		if (check()&&l==n)	rep(j,1,n)	ans[j]=g[j];	
	}
	rep(i,1,n-1)	write(ans[i]),putchar(' ');
	write(ans[n]),puts("");
	return 0;
}
