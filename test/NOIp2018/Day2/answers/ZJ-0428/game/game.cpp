#include<bits/stdc++.h>
#define rep(i,a,b)	for(int i=(a);i<=(b);++i)
#define rev(i,a,b)	for(int i=(a);i>=(b);--i)
using namespace std;
typedef long long LL;
void read(int &a){
	a=0;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')	ch=getchar();
	int f=1;if (ch=='-')	ch=getchar(),f=-1;
	while (ch>='0'&&ch<='9')	a=a*10+ch-48,ch=getchar();
	a=a*f;
}
void read(LL &a){
	a=0;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')	ch=getchar();
	LL f=1;if (ch=='-')	ch=getchar(),f=-1;
	while (ch>='0'&&ch<='9')	a=a*10+ch-48,ch=getchar();
	a=a*f;
}
void write(int a){
	if (!a)	return;
	if (a<0)	putchar('-'),write(-a);
	else write(a/10),putchar(a%10+48);
}
void write(LL a){
	if (!a)	return;
	if (a<0)	putchar('-'),write(-a);
	else write(a/10),putchar(a%10+48);
}
const int mod=1e9+7;
int n,m;
bool c[105][105],b[105][105];
int a[105][105],ans=0;
bool check(){
	rep(i,1,n)	c[i][m+1]=b[i][m+1]=true;
	rep(i,1,m)	c[n+1][i]=b[n+1][i]=true;
	c[n][m]=b[n][m]=true;
	rev(i,n,1)	rev(j,m,1)
		if (!(i==n&&j==m))
		c[i][j]=c[i+1][j]&&c[i][j+1]&&(a[i][j]==0),b[i][j]=b[i+1][j]&&b[i][j+1]&&(a[i][j]==1);
	rep(i,2,n)	rep(j,1,m-1){
		if (a[i][j]<a[i-1][j+1])	return false;
		if (a[i][j]==a[i-1][j+1])	if (!(c[i][j+1]||b[i][j+1]&&b[i+1][j]))	return false;
	}
	return true;
}
void dfs(int x,int y){
	if (x>n){
		if (check())	ans++;
		return;
	}
	a[x][y]=1;
	if (y<m)	dfs(x,y+1);	else dfs(x+1,1);
	a[x][y]=0;
	if (y<m)	dfs(x,y+1);	else dfs(x+1,1);
}
LL ppw(int x,int y){
	if (y==0)	return 1;
	LL ans=ppw(x,y/2);ans=ans*ans%mod;
	if (y&1)	ans=3*ans%mod;
	return ans;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read(n),read(m);
	if (n==3&&m==3){
		puts("112");	return 0;
	}
	if (n==2){
		write((LL)4*ppw(3,m-1)%(LL)mod),puts("");	return 0;
	}
	if (n*m<=25){
		dfs(1,1);
		write(ans),puts("");	return 0;
	}
	return 0;
}
