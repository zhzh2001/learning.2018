#include<bits/stdc++.h>
using namespace std;
typedef long long LL;
const int N=100100;
const LL INF=1e16;
int n,m;
#define F(i) for(int i=1;i<=n;i++)
int p[N];
namespace tle{
	vector<int> es[N];
	inline void ae(int u,int v){
		es[u].push_back(v);
	}
#define Fs(x) for(vector<int>::iterator it=es[x].begin();it!=es[x].end();it++)
	int a,qa,b,qb;
	LL dp[N][2];
	bool ok[N][2];
	LL dfs(int x,int qd,int fa){
		if(x==a&&qd!=qa)return INF;
		if(x==b&&qd!=qb)return INF;
		LL &ans=dp[x][qd];
		bool &done=ok[x][qd];
		if(done)return ans;
		done=true,ans=qd?p[x]:0;
		Fs(x){
			if(*it==fa)continue;
			if(qd==0){
				ans+=dfs(*it,1,x);
			}else{//qd==1
				ans+=min(dfs(*it,0,x),dfs(*it,1,x));
			}
		}
		return ans;
	}
	void main(){
		for(int i=0;i<n-1;i++){
			int u,v;
			scanf("%d%d",&u,&v);
			ae(u,v);
			ae(v,u);
		}
		for(int i=0;i<m;i++){
//			cerr<<i<<endl;
			scanf("%d%d%d%d",&a,&qa,&b,&qb);
			F(i)ok[i][0]=ok[i][1]=false;
			LL ans=min(dfs(1,0,-1),dfs(1,1,-1));
			if(ans>=INF)ans=-1;
			printf("%lld\n",ans);
		}
	}
}

namespace wa{
	struct Res{
		LL qaq[2][2];
		int lp,rp;
		Res(){
#define FOLD(i,s) {int i=0;s i=1;s}
			FOLD(i,{
				FOLD(j,{
					qaq[i][j]=INF;
				});
			});
			lp=rp=-1;
		};
		Res(int _lp,int _rp){
			lp=_lp,rp=_rp;
			qaq[0][1]=rp;
			qaq[1][0]=lp;
			qaq[1][1]=lp+rp;
			qaq[0][0]=INF;
		}
		inline Res operator +(const Res o)const{
			if(lp==-1)return o;
			if(o.lp==-1)return *this;
			Res ans;
			FOLD(i,{
				FOLD(j,{
					ans.qaq[i][j]=min(
						qaq[i][0]+o.qaq[0][j],
						qaq[i][1]+o.qaq[1][j]-rp
					);
				});
			});
			ans.lp=lp;
			ans.rp=o.rp;
			return ans;
		}
		LL val(){
			LL ans=INF;
			FOLD(i,{
				FOLD(j,{
					ans=min(ans,qaq[i][j]);
				});
			});
			return ans;
		}
	};
	struct SegTree{
		Res sum[N*4];
		int size;
#define ls (id<<1)
#define rs ((id<<1)|1)
#define h(x) ((x)+size-1)
		void build(){
			for(size=1;size<n-1;)size<<=1;
			for(int i=1;i<=n-1;i++)sum[h(i)]=Res(p[i],p[i+1]);
			for(int id=h(0);id>=1;id--)sum[id]=sum[ls]+sum[rs];
		}
		Res _query(int l,int r,int ll,int rr,int id){
			if(l<=ll&&rr<=r)return sum[id];
			if(r<ll||rr<l)return Res();
			int mid=(ll+rr)/2;
			return _query(l,r,ll,mid,ls)+_query(l,r,mid+1,rr,rs);
		}
		LL query(int l,int r){
			if(l>r)return 0;
			return _query(l,r,1,size,1).val();
		}
	}tr;
	void main(){
		for(int i=0;i<n-1;i++)
			scanf("%*d%*d");
		p[0]=p[n+1]=0;
		tr.build();
		for(int i=0;i<m;i++){
			int a,b,x,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(a>b){
				swap(a,b);
				swap(x,y);
			}
			LL ans;
			if(a==b&&x!=y)ans=-1;
			else if(abs(a-b)==1&&x==0&&y==0)ans=-1;
			else{
				ans=0;
				if(x==1){ans+=p[a];}else{ans+=p[a-1]+p[a+1];}
				if(y==1){ans+=p[b];}else{ans+=p[b-1]+p[b+1];}
				if(b-a==2&&x==0&&y==0)ans-=p[a+1];
				if(b-a==1){
					if(x==1&&y==0)ans-=p[a];
					if(x==0&&y==1)ans-=p[b];
				}
				int al=x==1?a-2:a-3,ar=x==1?a+1:a+2;
				int bl=y==1?b-2:b-3,br=y==1?b+1:b+2;
				ans+=tr.query(1,al);
				ans+=tr.query(ar,bl);
				ans+=tr.query(br,n-1);
			}
			printf("%lld\n",ans);
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char t1,t2;
	scanf("%d%d %c%c",&n,&m,&t1,&t2);
	F(i)scanf("%d",&p[i]);
	if(t1=='A'&&n>200){
		wa::main();
	}else{
		tle::main();
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
