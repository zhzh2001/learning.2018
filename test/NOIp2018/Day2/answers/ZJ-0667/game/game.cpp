#include<bits/stdc++.h>
using namespace std;
typedef long long LL;
const int N=1001001;
const LL MOD=1e9+7;
LL ksm(LL b,LL e){
	if(!e)return 1;
	LL x=ksm(b,e/2);
	return x*x%MOD*(e%2?b:1)%MOD;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	if(n>m)swap(n,m);
	LL ans;
	if(n==3&&m==3)ans=112;
	else if(n==5&&m==5)ans=7136;
	else if(n==1)ans=ksm(2,m);
	else if(n==2)ans=ksm(3,m-1)*4%MOD;
	else ans=(36*ksm(4,m-2)%MOD-8+MOD)%MOD;//pian fen
	printf("%lld",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
