#include<bits/stdc++.h>
using namespace std;
typedef long long LL;
const int N=5050;
int n,m;
#define F(i) for(int i=1;i<=n;i++)
vector<int> es[N];
inline void ae(int u,int v){
	es[u].push_back(v);
}
#define Fs(x) for(vector<int>::iterator it=es[x].begin();it!=es[x].end();it++)
struct E{
	int u,v;
}e[N*2];
int banU,banV;
inline void setBan(int u,int v){
	banU=min(u,v);
	banV=max(u,v);
}
inline bool baned(int u,int v){
	return min(u,v)==banU&&max(u,v)==banV;
}
int ans[N],now[N];
void init(){
	F(i)sort(es[i].begin(),es[i].end());
	F(i)ans[i]=n+1-i;
}
int vis;
void dfs(int x,int fa){
//	printf("dfs %d\n",x);
	now[++vis]=x;
	if(vis>n)return;
	Fs(x){
		if(*it==fa||baned(x,*it))continue;
		dfs(*it,x);
		if(vis>n)return;
	}
}
bool better(){
	F(i)if(now[i]!=ans[i])return now[i]<ans[i];
	return false;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=0;i<m;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		ae(u,v);
		ae(v,u);
		e[i]=(E){u,v};
	}
	init();
	if(m==n-1){
		dfs(1,-1);
		F(i)ans[i]=now[i];
	}else for(int i=0;i<m;i++){
		setBan(e[i].u,e[i].v);
		vis=0;
		dfs(1,-1);
		if(vis==n&&better()){
			F(i)ans[i]=now[i];
		}
	}
	F(i)printf("%d%c",ans[i],i==n?'\n':' ');
	fclose(stdin);
	fclose(stdout);
	return 0;
}
