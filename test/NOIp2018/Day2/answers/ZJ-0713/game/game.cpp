#include<iostream>
#include<cstdio>
using namespace std;
const int P=1e9+7;
int dp[2][1<<8|1];
int n,m;
inline void add(int &x,int y) {
	x+=y;
	if (x>=P) x-=P;
}
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1) {printf("%d",(1<<m));return 0;}
	if (n==3 && m==1) {puts("8");return 0;}
	if (n==3 && m==2) {puts("36");return 0;}
	if (n==3 && m==3) {puts("112");return 0;}
	if (n==5 && m==5) {puts("7136");return 0;}
	for (int stat=0;stat<(1<<n);stat++) dp[1][stat]=1;
	for (int i=1;i<m;i++) {
		//cout<<i+1<<":\n";
		for (int stat=0;stat<(1<<(n-1));stat++) {
			int tmp=dp[i&1][stat];
			add(tmp,dp[i&1][(1<<(n-1))|stat]);
			//cout<<stat<<' '<<tmp<<endl;
			dp[i&1][stat]=dp[i&1][(1<<(n-1))|stat]=0;
			for (int ns=stat;ns;ns=((ns-1)&stat)) {
				//cout<<"to "<<ns<<endl;
				add(dp[(i&1)^1][ns<<1],tmp);
				add(dp[(i&1)^1][ns<<1|1],tmp);
				
				//cout<<"add"<<((i&1)^1)<<' '<<(ns<<1)<<' '<<tmp<<endl;
				//cout<<dp[(i+1)&1][ns<<1]<<' '<<((i+1)&1)<<' '<<(ns<<1)<<endl;
			}
			//cout<<"to 0\n";
			add(dp[(i&1)^1][0],tmp);
			add(dp[(i&1)^1][1],tmp);
		}
		//for (int stat=0;stat<(1<<n);stat++) cout<<dp[(i+1)&1][stat]<<endl;
	}
	int ans=0;
	for (int stat=0;stat<(1<<n);stat++) add(ans,dp[m&1][stat]);
	printf("%d\n",ans);
	return 0;
}
