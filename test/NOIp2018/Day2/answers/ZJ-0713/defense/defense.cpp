#include<bits/stdc++.h>
using namespace std;
typedef long long LL;
const int MAXN=100005;
int head[MAXN],vet[MAXN*2],nxt[MAXN*2],num;
LL dp[MAXN][2],md1[MAXN][2],md2[MAXN][2];
int cost[MAXN],fa[MAXN];
int xx,a,yy,b,rt;
int n,q;
char str[10];
inline void addedge(int x,int y) {
	num++;vet[num]=y;nxt[num]=head[x];head[x]=num;
	num++;vet[num]=x;nxt[num]=head[y];head[y]=num;
}
void dfs1(int x,int ff) {
	dp[x][0]=dp[x][1]=0;
	for (int e=head[x];e;e=nxt[e]) {
		int v=vet[e];
		if (v==ff) continue;
		dfs1(v,x);
		dp[x][0]+=dp[v][1];
		dp[x][1]+=min(dp[v][0],dp[v][1]);
	}
	dp[x][1]+=cost[x];
	if (x==yy) dp[x][b^1]=1e16;
}
void dfs2(int x) {
	dp[x][0]=dp[x][1]=0;
	for (int e=head[x];e;e=nxt[e]) {
		int v=vet[e];
		if (v==fa[x]) continue;
		fa[v]=x;
		dfs2(v);
		dp[x][0]+=dp[v][1];
		dp[x][1]+=min(dp[v][0],dp[v][1]);
	}
	dp[x][1]+=cost[x];
}
inline bool NoSolution() {
	if (a+b==0) {
		bool flag=false;
		for (int e=head[xx];e;e=nxt[e]) {
			if (vet[e]==yy) {flag=true;break;}
		}
		return flag;
	}
	return false;
}
inline void update1() {
	int x=xx;
	md1[x][0]=dp[x][0];
	md1[x][1]=dp[x][1];
	dp[x][a^1]=1e16;
	while (x!=rt) {
		int v=fa[x];
		md1[v][0]=dp[v][0];
		md1[v][1]=dp[v][1];
		dp[v][0]+=dp[x][1]-md1[x][1];
		dp[v][1]+=min(dp[x][1],dp[x][0])-min(md1[x][0],md1[x][1]);
		x=v;
	}
}
inline void update2() {
	int x=yy;
	md2[x][0]=dp[x][0];
	md2[x][1]=dp[x][1];
	dp[x][b^1]=1e16;
	while (x!=rt) {
		int v=fa[x];
		md2[v][0]=dp[v][0];
		md2[v][1]=dp[v][1];
		dp[v][0]+=dp[x][1]-md2[x][1];
		dp[v][1]+=min(dp[x][1],dp[x][0])-min(md2[x][0],md2[x][1]);
		x=v;
	}
}
inline void bk() {
	int x=yy;
	while (x!=0) dp[x][0]=md2[x][0],dp[x][1]=md2[x][1],x=fa[x];
	x=xx;
	while (x!=0) dp[x][0]=md1[x][0],dp[x][1]=md1[x][1],x=fa[x];
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&q);scanf("%s",str);
	for (int i=1;i<=n;i++) scanf("%d",&cost[i]);
	for (int i=1;i<n;i++) {
		int x,y;scanf("%d%d",&x,&y);
		addedge(x,y);
	}
	if (q*n<1e8) {
		while (q--) {
			scanf("%d%d%d%d",&xx,&a,&yy,&b);
			if (NoSolution()) {puts("-1");continue;}
			dfs1(xx,0);
			printf("%lld\n",dp[xx][a]);
		}
	}
	else {
		srand(time(NULL));rt=rand()%n+1;
		if (str[0]=='B') rt=1;
		dfs2(rt);
		while (q--) {
			scanf("%d%d%d%d",&xx,&a,&yy,&b);
			if (NoSolution()) {puts("-1");continue;}
			update1();update2();
			if (xx==rt) printf("%lld\n",dp[rt][a]);
			else if (yy==rt) printf("%lld\n",dp[rt][b]);
			else printf("%lld\n",min(dp[rt][0],dp[rt][1]));
			bk();
		}
	}
	return 0;
}
