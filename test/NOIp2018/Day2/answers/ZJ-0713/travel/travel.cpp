#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int MAXN=5005;
int head[MAXN],vet[MAXN*2],nxt[MAXN*2],num;
int ans[MAXN],ss[MAXN],nn,fb;
int son[MAXN][MAXN];
bool vis[MAXN];
int n,m;
inline void addedge(int x,int y) {
	num++;vet[num]=y;nxt[num]=head[x];head[x]=num;
	num++;vet[num]=x;nxt[num]=head[y];head[y]=num;
}
inline void check() {
	/*
	cout<<fb<<":\n";
	for (int i=1;i<=nn;i++) cout<<ss[i]<<' ';cout<<endl;
	for (int i=1;i<=n;i++) cout<<ans[i]<<' ';cout<<endl;
	cout<<"len="<<nn<<endl;*/
	
	if (nn<n) return;
	if (ans[1]==0) {
		for (int i=1;i<=n;i++) ans[i]=ss[i];
		return;
	}
	bool flag=false;
	for (int i=1;i<=n;i++) {
		if (ss[i]<ans[i]) {
			flag=true;
			break;
		}
		else if (ss[i]>ans[i]) break;
	}
	if (flag) for (int i=1;i<=n;i++) ans[i]=ss[i];
	//for (int i=1;i<=n;i++) cout<<ans[i]<<' ';cout<<endl;
}
void dfs(int x) {
	vis[x]=1;son[x][0]=0;
	ss[++nn]=x;
	for (int e=head[x];e!=-1;e=nxt[e]) {
		int v=vet[e];
		if (vis[v] || (e>>1)==fb) continue; 
		son[x][++son[x][0]]=v;
	}
	sort(son[x]+1,son[x]+son[x][0]+1);
	for (int i=1;i<=son[x][0];i++) {
		if (!vis[son[x][i]]) dfs(son[x][i]);
	}
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(head,-1,sizeof(head));num=-1;
	for (int i=1;i<=m;i++) {
		int x,y;scanf("%d%d",&x,&y);
		addedge(x,y);
	}
	if (m==n-1) {
		fb=-1;nn=0;
		dfs(1);
		for (int i=1;i<=n;i++) printf("%d ",ss[i]);
		return 0;
	}
	else {
		for (int i=0;i<m;i++) {
			fb=i;nn=0;
			memset(vis,0,sizeof(vis));
			dfs(1);
			check();
		}
		for (int i=1;i<=n;i++) printf("%d ",ans[i]);
	}
	return 0;
}
