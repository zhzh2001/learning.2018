#include<bits/stdc++.h>
using namespace std;
const int maxn=300010;
int n,m,p[maxn],x,y,tot,head[maxn],u,v,b[maxn];
long long ans1,ans2,ans;
bool flag[2010][2010];
string s;
struct atree
{
	int y,next;
}
a[maxn<<1];
inline int read()
{
	int num=0,flag=1;
	char c=getchar();
	for (;c<'0'||c>'9';c=getchar())
	if (c=='-') flag=-1;
	for (;c>='0'&&c<='9';c=getchar())
	num=(num<<3)+(num<<1)+c-48;
	return num*flag;
}
void add(int x,int y)
{
	a[++tot].y=y;
	a[tot].next=head[x];
	head[x]=tot;
}
void init()
{
	n=read();
	m=read();
	getline(cin,s);
	for (int i=1;i<=n;++i)
	p[i]=read();
	for (int i=1;i<n;++i)
	{
		x=read();
		y=read();
		add(x,y);
		add(y,x);
	}
}
void dfs1(int x,int fa)
{
	for (int i=head[x];i;i=a[i].next)
	{
		int y=a[i].y;
		flag[x][y]=true;
		flag[y][x]=true;
		if (y==fa) continue;
		dfs1(y,x);
	}
}
void dfs2(int x,int fa)
{
	for (int i=head[x];i;i=a[i].next)
	{
		int y=a[i].y;
		if (y==fa) continue;
		if (b[x]==0&&b[y]==0) b[x]=1;
		if (b[y]==-1)
		{
			if (b[x]==1) b[y]=0;
			else b[y]=1;
		}
		dfs2(y,x);
	}
}
void work()
{
	dfs1(1,0);
	for (int i=1;i<=m;++i)
	{
		u=read();
		x=read();
		v=read();
		y=read();
		if (flag[u][v]&&x==0&&y==0)
		{
			printf("-1\n");
			continue;
		}
		memset(b,-1,sizeof(b));
		b[u]=x;
		b[v]=y;
		dfs2(u,0);
		ans1=0;
		for (int j=1;j<=n;++j)
		if (b[j]==1) ans1+=p[j];
		memset(b,-1,sizeof(b));
		b[u]=x;
		b[v]=y;
		dfs2(v,0);
		ans2=0;
		for (int j=1;j<=n;++j)
		if (b[j]==1) ans2+=p[j];
		ans=min(ans1,ans2);
		printf("%lld\n",ans);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	init();
	work();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
