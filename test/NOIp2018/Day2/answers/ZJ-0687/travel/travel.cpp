#include<bits/stdc++.h>
using namespace std;
const int maxn=5010;
int n,m,x,y,tot,head[maxn],minn=maxn,maxx;
bool b[maxn],flag=true;
struct atree
{
	int y,next;
}
a[maxn<<1];
inline int read()
{
	int num=0,flag=1;
	char c=getchar();
	for (;c<'0'||c>'9';c=getchar())
	if (c=='-') flag=-1;
	for (;c>='0'&&c<='9';c=getchar())
	num=(num<<3)+(num<<1)+c-48;
	return num*flag;
}
void add(int x,int y)
{
	a[++tot].y=y;
	a[tot].next=head[x];
	head[x]=tot;
}
void init()
{
	n=read();
	m=read();
	for (int i=1;i<=m;++i)
	{
		x=read();
		y=read();
		add(x,y);
		add(y,x);
	}
}
void dfs1(int x,int fa)
{
	printf("%d ",x);
	int t=0,b[maxn]={};
	for (int i=head[x];i;i=a[i].next)
	{
		int y=a[i].y;
		if (y==fa) continue;
		b[++t]=y;
	}
	sort(b+1,b+t+1);
	for (int i=1;i<=t;++i)
	dfs1(b[i],x);
}
void work1()
{
	dfs1(1,0);
	printf("\n");
}
void dfs2(int x)
{
	printf("%d ",x);
	b[x]=true;
	for (int i=head[x];i;i=a[i].next)
	{
		int y=a[i].y;
		if (b[y]) continue;
		if (flag&&maxx<y)
		{
			flag=false;
			dfs2(maxx);
		}
		else dfs2(y);
	}
}
void work2()
{
	for (int i=head[1];i;i=a[i].next)
	{
		int y=a[i].y;
		minn=min(minn,y);
		maxx=max(maxx,y);
	}
	printf("1 ");
	b[1]=true;
	dfs2(minn);
	printf("\n");
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	init();
	if (m==n-1) work1();
	else work2();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
