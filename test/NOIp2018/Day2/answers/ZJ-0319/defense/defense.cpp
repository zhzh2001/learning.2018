#include<bits/stdc++.h>
using namespace std;
int e[200000];
int v1[200000];
int f1[200000][3];
int h[200000];
int n,t,m;
int a[200000];
int read()
{
	char ch=getchar();
	int x=0;
	int f=1;
	while(ch>'9'||ch<'0') 
	{
		if(ch=='-') f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		x=x*10-48+ch;
		ch=getchar();
	}
	return x*f;
}
void add(int x,int y)
{
	t++;
	v1[t]=h[x];
	h[x]=t;
	e[t]=y;
}
int dpp(int x,int z,int fa)
{
	if(f1[x][z]>-1) return f1[x][z];
	int v=0;
	if(z==1)
	{
		v+=a[x];
		for(int i=h[x];i;i=v1[i])
			if(e[i]!=fa)
			{
				int k1=dpp(e[i],0,x);
				int k2=dpp(e[i],1,x);
				v+=k1>k2?k2:k1;
			}
	}
	if(z==0)
	{
		for(int i=h[x];i;i=v1[i])
			if(e[i]!=fa)
			{
				int s1=dpp(e[i],1,x);
				v+=s1;
			}
	}
	f1[x][z]=v;
	return v;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();
	char ch=getchar();
	memset(f1,-1,sizeof(f1));
	while(ch<'A'||ch>'C') ch=getchar();
	int tt=getchar()-48;
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	for(int i=1;i<=n-1;i++)
	{
		int x=read();
		int y=read();
		add(x,y);
		add(y,x);
	}
	for(int i=1;i<=m;i++)
	{
		int x1,y1,x2,y2;
		x1=read();y1=read();
		x2=read();y2=read();
		memset(f1,-1,sizeof(f1));
		f1[x2][1-y2]=214748364;
		dpp(x1,y1,0);
		if(f1[x1][y1]>=214748364) printf("-1\n");
		else printf("%d\n",f1[x1][y1]);
	}
}
