#include<cstdio>
#include<vector>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define FOR(i,x,y) for(int i=(x),i##_END=(y);i<=i##_END;++i)
#define DOR(i,x,y) for(int i=(x),i##_END=(y);i>=i##_END;--i)

inline bool chk_mi(int &x,const int &y){return y<x?x=y,true:false;}
inline bool chk_mx(int &x,const int &y){return x<y?x=y,true:false;}

const int M=300005;
vector<int>E[M];

int n,m;
int V[M];
namespace P44{
	const int N=2005;
	const int INF=1000000000;
	int mark[N];
	int dp[N][3];//0:自己不取,取自己,需要父亲 
	void dfs(int x,int f){
		dp[x][0]=0,dp[x][1]=V[x];
		FOR(i,0,E[x].size()-1){
			int y=E[x][i];
			if(y==f) continue;
			dfs(y,x);
			dp[x][0]=dp[x][0]+dp[y][1];
			dp[x][1]=dp[x][1]+min(dp[y][0],dp[y][1]);
		}
		if(mark[x]==1)dp[x][0]=INF;
		if(mark[x]==0)dp[x][1]=INF;
	}
	void solve(){
		FOR(i,1,n)mark[i]=2;
		while(m--){
			int x,y,a,b;
			scanf("%d%d%d%d",&x,&a,&y,&b);
			mark[x]=a,mark[y]=b;
			dfs(1,0);
			mark[x]=2,mark[y]=2;
			int ans=min(dp[1][0],dp[1][1]);
			if(ans>=INF)puts("-1");
			else printf("%d\n",ans);
		}
	}
}
namespace P24{
	const int INF=1000000000;
	struct node{
		int a00,a01,a11,a10;
		node(){a00=a01=a11=a10=INF;}
		node operator +(const node &_){
			node tmp;
			chk_mi(tmp.a00,a00+_.a10);
			chk_mi(tmp.a00,a01+_.a10);
			chk_mi(tmp.a00,a01+_.a00);
			chk_mi(tmp.a11,a10+_.a11);
			chk_mi(tmp.a11,a11+_.a11);
			chk_mi(tmp.a11,a11+_.a01);
			chk_mi(tmp.a10,a10+_.a10);
			chk_mi(tmp.a10,a11+_.a10);
			chk_mi(tmp.a10,a11+_.a00);
			chk_mi(tmp.a01,a00+_.a11);
			chk_mi(tmp.a01,a01+_.a11);
			chk_mi(tmp.a01,a01+_.a01);
			return tmp;
		}
	}tree[M<<2];
	#define lson l,mid,p<<1
	#define rson mid+1,r,p<<1|1
	void build(int l,int r,int p){
		if(l==r){
			tree[p].a00=0;
			tree[p].a11=V[l];
			return;
		}
		int mid=(l+r)>>1;
		build(lson),build(rson);
		tree[p]=tree[p<<1]+tree[p<<1|1];
	}
	node query(int L,int R,int l,int r,int p){
		if(L==l&&R==r)return tree[p];
		int mid=(l+r)>>1;
		if(mid>=R)return query(L,R,lson);
		else if(mid<L)return query(L,R,rson);
		else return query(L,mid,lson)+query(mid+1,R,rson);
	}
	void solve(){
		build(1,n,1);
		while(m--){
			int x,y,a,b;
			scanf("%d%d%d%d",&x,&a,&y,&b);
			if(x>y)swap(x,y),swap(a,b);
			node A=query(x,y,1,n,1);
			node P=A;
			A.a00=A.a01=A.a10=A.a11=INF;
			if(!a&&!b)A.a00=P.a00;
			if(!a&&b)A.a01=P.a01;
			if(a&&!b)A.a10=P.a10;
			if(a&&b)A.a11=P.a11;
			if(x>1){
				node B=query(1,x-1,1,n,1);
				A=B+A;
			}
			if(y<n){
				node B=query(y+1,n,1,n,1);
				A=A+B;
			}
			int ans=min(min(A.a00,A.a01),min(A.a10,A.a11));
			if(ans==INF)puts("-1");
			else printf("%d\n",ans);
		}
	}
}
char op[10];
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	scanf("%s",op);
	FOR(i,1,n)scanf("%d",&V[i]);
	FOR(i,2,n){
		int a,b;
		scanf("%d%d",&a,&b);
		E[a].push_back(b);
		E[b].push_back(a);
	}
	if(n<=2000&&m<=2000)P44::solve();
	else if(op[0]=='A')P24::solve();
	
	return 0;
}
