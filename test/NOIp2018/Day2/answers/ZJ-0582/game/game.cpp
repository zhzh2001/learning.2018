#include<cstdio>
#include<cstring>
#include<iostream>
#include<string>
#define FOR(i,x,y) for(int i=(x),i##_END=(y);i<=i##_END;++i)
#define DOR(i,x,y) for(int i=(x),i##_END=(y);i>=i##_END;--i)
using namespace std;

typedef long long LL;
int n,m;
const int P=1000000007;
namespace P20{
	int A[10][10];
	void solve(){
		int tot=n*m;
		int ans=0;
		FOR(i,0,(1<<tot)-1){
			FOR(j,0,n-1)FOR(k,0,m-1){
				int id=j*m+k;
				A[j][k]=(i&(1<<id))>0;
			}
			string mi="";
			bool f=true,fi=true;
			FOR(j,0,(1<<(n+m-2))-1){
				int a=0,b=0;
				string s="";
				s=A[a][b]?'1':'0';
				DOR(k,n+m-3,0){
					if(j&(1<<k))b++;
					else a++;
					s+=A[a][b]?'1':'0';
				}
				if(a!=n-1||b!=m-1) continue;
				if(fi)mi=s,fi=false;
				if(mi<s){
					f=false;break;
				}else mi=s;
			}
			if(f)ans++;
		}
		printf("%d\n",ans);
	}
}

namespace P50{
	void solve(){
		int ans=4;
		FOR(i,1,m-1)ans=(LL)ans*3%P;
		printf("%d\n",ans);
	}
}
namespace P65{
	void solve(){
		int ans=112;
		FOR(i,1,m-3)ans=(LL)ans*3%P;
		printf("%d\n",ans);
	}
}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	
	if(n<=3&&m<=3)P20::solve();
	else if(n==2)P50::solve();
	else if(n==3)P65::solve();
	return 0;
}
