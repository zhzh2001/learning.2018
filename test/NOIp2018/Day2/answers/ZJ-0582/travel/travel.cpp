#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define FOR(i,x,y) for(int i=(x),i##_END=(y);i<=i##_END;++i)
#define DOR(i,x,y) for(int i=(x),i##_END=(y);i>=i##_END;--i)

inline bool chk_mi(int &x,const int &y){return y<x?x=y,true:false;}
inline bool chk_mx(int &x,const int &y){return x<y?x=y,true:false;}

const int M=5005;
int n,m;
vector<int>E[M];
namespace P60{
	int A[M][M];
	void dfs(int x,int f){
		A[x][0]=0;
		FOR(j,0,E[x].size()-1){
			int y=E[x][j];
			if(y==f) continue;		
			dfs(y,x);
			FOR(i,1,A[y][0])A[x][++A[x][0]]=A[y][i];
		}
		A[x][0]++;
		DOR(i,A[x][0],2)A[x][i]=A[x][i-1];
		A[x][1]=x;
	}
	void solve(){
		FOR(i,1,n) sort(E[i].begin(),E[i].end()); 
		dfs(1,0);
		FOR(i,1,A[1][0])printf("%d%c",A[1][i]," \n"[i==n]);
	}
}
namespace P68{
	int A[M],pre[M];
	bool ok;
	void dfs(int x,int i,int f){
		if(!pre[x])pre[x]=f;
		if(i==n){ok=true;return;}
		FOR(j,0,E[x].size()-1){
			int y=E[x][j];
			if(y!=A[i+1])continue;
			A[i+1]=y;
			dfs(y,i+1,x);
		}
		if(pre[x])dfs(pre[x],i,0);
	}
	void solve(){
		FOR(i,1,n)A[i]=i;
		do{
			FOR(i,1,n)pre[A[i]]=0;
			ok=false;
			dfs(A[1],1,0);
			if(ok)break;
		}while(next_permutation(A+1,A+n+1));
		FOR(i,1,n)printf("%d ",A[i]);puts("");
	}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	FOR(i,1,m){
		int a,b;
		scanf("%d%d",&a,&b);
		E[a].push_back(b);
		E[b].push_back(a);
	}
	if(m==n-1)P60::solve();
	else if(n<=10)P68::solve();
	
	return 0;
}
