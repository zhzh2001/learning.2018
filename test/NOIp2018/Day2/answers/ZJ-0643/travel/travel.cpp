#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;
const int N=5002;
vector<int>a[N];
int i,j,k,n,m,ft[N],x[N],ans[N];
void dfs1(int u,int ft)
{
	printf("%d ",u);
	for(int i=0;i<a[u].size();++i)
		if(a[u][i]!=ft)dfs1(a[u][i],u);
}
void dfs2(register int u)
{
	if(k==n)
	{
		for(i=1;i<n&&ans[i]==x[i];++i);
		if(x[i]<ans[i])for(i=0;++i<n;ans[i]=x[i]);
		return;
	}
	for(register int i=0;i<a[u].size();++i)if(!ft[a[u][i]])
		ft[a[u][i]]=u,dfs2(x[k++]=a[u][i]),--k,ft[a[u][i]]=0,i=n;
	if(ft[u]>0)dfs2(ft[u]);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(k=1;k<=m;++k)
	{
		scanf("%d%d",&i,&j);
		a[i].push_back(j);
		a[j].push_back(i);
	}
	for(i=1;i<=n;++i)
		sort(a[i].begin(),a[i].end());
	if(m<n)dfs1(1,0);else
	{
		for(i=1;i<n;ans[i++]=n);
		ft[1]=-1,dfs2(k=1),printf("1");
		for(i=0;++i<n;printf(" %d",ans[i]));
	}
}
