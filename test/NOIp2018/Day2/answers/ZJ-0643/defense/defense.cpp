#include<cstdio>
#include<cstring>
#include<algorithm>
const int N=10003;
char s[8];
int i,j,n,m,t,x1,y1,x2,y2,a[N],f[N][2],en[N*2],fa[N*2],nxt[N*2];
void dfs(int u,int ft)
{
	for(int i=fa[u];i;i=nxt[i])if(en[i]!=ft)
	{
		dfs(en[i],u);
		if(*f[u]<1e9)*f[u]+=f[en[i]][1];
		if(f[u][1]<1e9)f[u][1]+=std::min(*f[en[i]],f[en[i]][1]);
	}
	f[u][1]+=a[u];
	if(u==x1)y1?*f[u]=1e9:f[u][1]=1e9;else
	if(u==x2)y2?*f[u]=1e9:f[u][1]=1e9;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for(i=1;i<=n;++i)scanf("%d",a+i);
	for(;++t<n+n-1;)
	{
		scanf("%d%d",&i,&j);
		en[t]=j,nxt[t]=fa[i],fa[i]=t;
		en[++t]=i,nxt[t]=fa[j],fa[j]=t;
	}
	for(;m--;printf("%d\n",t<1e9?t:-1))
	{
		scanf("%d%d%d%d",&x1,&y1,&x2,&y2);
		memset(f,0,sizeof f),dfs(1,0);
		t=std::min(*f[1],f[1][1]);
	}
}
