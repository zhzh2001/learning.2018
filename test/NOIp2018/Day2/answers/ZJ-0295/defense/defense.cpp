#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
struct edge
{
	int eto,nxt;
};
int fst[100002],p[100002],n,m;
char c1,c2;
edge e[200004];
void addedge(int x,int y,int i)
{
	e[i].eto=y;
	e[i].nxt=fst[x];
	fst[x]=i;
}
void solve_A()
{
	int a,b,x,y;
	scanf("%d%d%d%d",&a,&x,&b,&y);
	if ((a==b+1 || b==a+1) && x==0 && y==0)
	    printf("-1\n");
	else 
	{
		int dp[100002][3];
		memset(dp,-1,sizeof(dp));
		dp[1][0]=0;
		dp[1][1]=p[1];
		dp[a][1-x]=-2;
		dp[b][1-x]=-2;
		for (int i=2;i<=n;i++)
		{ 
			if (dp[i][1]!=-2)
			{   dp[i][1]=(1<<30)-1;
				if (dp[i-1][0]>=0) dp[i][1]=min(dp[i][1],dp[i-1][0]);
				if (dp[i-1][1]>=0) dp[i][1]=min(dp[i][1],dp[i-1][1]);
				if (dp[i-1][2]>=0) dp[i][1]=min(dp[i][1],dp[i-1][2]);
				dp[i][1]+=p[i];
			}
			if (dp[i][0]!=-2)
			{
				if (dp[i-1][1]>=0) dp[i][0]=dp[i-1][1];
				if (dp[i][0]>=0) dp[i][2]=dp[i][0];
			}
		} 
		int ans;
		if (dp[n][1]<0) ans=dp[n][0];
		 else if (dp[n][0]<0) ans=dp[n][1];
		  else ans=min(dp[n][0],dp[n][1]);
	    printf("%d\n",ans);
	}
}
void solve_it()
{
	if (c1='A') solve_A();
	else ;
}
int main()
{   freopen("defense.in","r",stdin);
    freopen("defense.out","w",stdout);
	scanf("%d%d%c%c%c",&n,&m,&c1,&c1,&c2);
	for (int i=1;i<=n;i++)
	  scanf("%d",&p[i]);
	for (int i=0;i<n-1;i++)
	{ int x,y;
	  scanf("%d%d",&x,&y);
	  addedge(x,y,2*i);
	  addedge(y,x,2*i+1);
	}
	for (int i=1;i<=m;i++)
	  solve_it();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
