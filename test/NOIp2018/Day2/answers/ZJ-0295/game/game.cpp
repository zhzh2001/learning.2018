#include<cstdio>
#include<cstring>
bool a[12][12]={{1,0,0,1,0,0,0,0,0,0,0,0},{1,0,0,1,0,0,0,0,0,0,0,0},
               {0,0,0,0,0,0,1,0,0,1,0,0},{0,1,1,0,1,1,0,0,0,0,0,0},
			   {0,1,1,0,1,1,0,0,0,0,0,0},{0,0,0,0,0,0,0,1,1,0,1,1},
			   {1,0,0,1,0,0,0,0,0,0,0,0},{1,0,0,1,0,0,0,0,0,0,0,0},
               {0,0,0,0,0,0,1,0,0,1,0,0},{0,1,1,0,1,1,0,0,0,0,0,0},
			   {0,1,1,0,1,1,0,0,0,0,0,0},{0,0,0,0,0,0,0,1,1,0,1,1}};
bool b[12][12]={{1,1,0,1,1,0,0,0,0,0,0,0},{0,0,0,0,0,0,1,1,0,1,1,0},
               {0,0,0,0,0,0,1,1,0,1,1,0},{0,0,1,0,0,1,0,0,0,0,0,0},
			   {0,0,0,0,0,0,0,0,1,0,0,1},{0,0,0,0,0,0,0,0,1,0,0,1},
			   {1,1,0,1,1,0,0,0,0,0,0,0},{0,0,0,0,0,0,1,1,0,1,1,0},
               {0,0,0,0,0,0,1,1,0,1,1,0},{0,0,1,0,0,1,0,0,0,0,0,0},
			   {0,0,0,0,0,0,0,0,1,0,0,1},{0,0,0,0,0,0,0,0,1,0,0,1}};
int nxt[200],fst[12][12],ans[8][1000001][12],odr[200];
void mfind()
{  int fn=0;
   for (int i=0;i<=11;i++)
    for (int j=0;j<=11;j++)
     for (int k=0;k<=11;k++)
      if (a[i][k]&&b[j][k]) 
      { odr[fn]=k;
        nxt[fn]=fst[i][j];
      	fst[i][j]=fn;
      	fn++;
	  }
}
int main()
{   int n,m;
    int ep=1e9+7;
    freopen("game.in","r",stdin);
    freopen("game.out","w",stdout);
    scanf("%d%d",&n,&m);
    if ((n==3)&(m==3)) printf("112\n");
    else {
    memset(ans,0,sizeof(ans));
    memset(fst,-1,sizeof(fst));
    mfind();
    for (int i=0;i<=11;i++)
     ans[0][0][i]=1;
    for (int i=1;i<=n-2;i++)
     for (int j=0;j<=11;j++)
      for (int k=0;k<=11;k++)
       if (b[j][k]) ans[i][0][k]=(ans[i][0][k]+ans[i-1][0][j])%ep;
    for (int i=1;i<=m-2;i++)
     for (int j=0;j<=11;j++)
      for (int k=0;k<=11;k++)
       if (a[j][k]) ans[0][i][k]=(ans[0][i-1][j]+ans[0][i][k])%ep;
    for (int i=1;i<=n-2;i++)
     for (int j=1;j<=m-2;j++)
      for (int k=0;k<=11;k++)
       for (int h=0;h<=11;h++)
        for (int t=fst[k][h];t!=-1;t=nxt[t])
         ans[i][j][odr[t]]=(ans[i][j][odr[t]]+(ans[i][j-1][k]%ep*(ans[i-1][j][h]%ep))%ep)%ep;
	int s=0;
	for (int i=0;i<=11;i++)
	 s=(s+ans[n-2][m-2][i])%ep;
	printf("%d\n",s);}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
