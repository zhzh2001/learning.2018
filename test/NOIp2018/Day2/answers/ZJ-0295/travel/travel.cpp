#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
using namespace std;
struct edge
{
	int eto,nxt;
};
int ans[5002],bns[5002],fst[5002],n,m,cur=0;
bool vis[5002],abled[5002],flag3=false;
edge e[10004];
void addedge(int x,int y,int i)
{
	e[i].eto=y;
	e[i].nxt=fst[x];
	fst[x]=i;
}
bool mcmp(int x[],int y[])
{
	for (int i=0;i<=n-2;i++)
	  if (y[i]<x[i]) return true;
	    else if (y[i]>x[i]) return false;
    return false;
}
void dfs(int x)
{   
    bns[cur]=x;
    vis[x]=1;
    cur++;
	int tdfs[5001],nt=-1;
	for (int t=fst[x];t!=-1;t=e[t].nxt)
	{   
		if (abled[t]) {
		nt++;  tdfs[nt]=e[t].eto;}
	}
	sort(tdfs,tdfs+nt+1);
	for (int i=0;i<=nt;i++)
	 if (!vis[tdfs[i]])  dfs(tdfs[i]);
}
int main()
{   
    freopen("travel.in","r",stdin);
    freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(fst,-1,sizeof(fst));
	memset(vis,0,sizeof(vis));
	for (int i=0;i<m;i++)
	  { int x,y;
	  	scanf("%d%d",&x,&y);
	  	addedge(x,y,2*i);
	  	addedge(y,x,2*i+1);
	  }
	memset(abled,1,sizeof(abled));
	if (m==n-1) dfs(1);
	else 
	for (int i=1;i<m;i++)
	{
		abled[2*i]=false;
		abled[2*i+1]=false;
		dfs(1);
	    memset(vis,0,sizeof(vis));
	    if (cur==n && (mcmp(ans,bns)||!flag3))
	     { 
		   for (int j=0;j<=n-1;j++) ans[j]=bns[j];
	       flag3=true;
	     }
		abled[2*i]=true;
		abled[2*i+1]=true;
		cur=0;
    }
    if (m==n-1) for (int i=0;i<=n-1;i++) ans[i]=bns[i];
	for (int i=0;i<=n-2;i++)
	 printf("%d ",ans[i]);
	printf("%d\n",ans[n-1]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
