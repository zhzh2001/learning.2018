# include <bits/stdc++.h>
using namespace std;
const int MAXN=1e5+10;
int f[MAXN][2],tag[MAXN];
int head[MAXN],size[MAXN],tot=0,p[MAXN];
int INF,n,m;
char type[100];
struct rec{ int pre,to;}a[MAXN*2];
void adde(int u,int v)
{
	a[++tot].pre=head[u];
	a[tot].to=v;
	head[u]=tot;
}
void dfs1(int u,int fa)
{
	size[u]=1;
	for (int i=head[u];i;i=a[i].pre){
		int v=a[i].to; if (v==fa) continue;
		dfs1(v,u);
		size[u]+=size[v];
	}
}
void TreeDp(int u,int fa)
{
	if (size[u]==1) { 
		 if (tag[u]==-1) f[u][0]=0,f[u][1]=p[u];
		 else if (tag[u]==1) f[u][0]=INF,f[u][1]=p[u];
		 else if (tag[u]==0) f[u][0]=0,f[u][1]=INF;
//		 printf("f[%d][0]= %d f[%d][1]=%d\n",u,f[u][0],u,f[u][1]);
		 return;
	}
	bool flag=false;
	int ret=0;
	for (int i=head[u];i;i=a[i].pre){
		int v=a[i].to; if (v==fa) continue;
		if (tag[v]==0) flag=true;
		ret+=min(f[v][0],f[v][1]);
	}
	for (int i=head[u];i;i=a[i].pre) {
		int v=a[i].to; if (v==fa) continue;
		if (tag[u]==-1) {
			TreeDp(v,u);
			if (!flag) f[u][0]=min(f[u][0],f[v][1]);
			else f[u][0]=INF;
			f[u][1]=min(f[u][1],min(f[v][0]+p[u],f[v][1]+p[u]));	
		} else if (tag[u]==1) {
			TreeDp(v,u);
			f[u][0]=INF;
			f[u][1]=min(f[u][1],min(f[v][0]+p[u],f[v][1]+p[u]));
		} else if (tag[u]==0) {
			TreeDp(v,u);
			f[u][0]=min(f[u][0],f[v][1]);
			f[u][1]=INF;
		}
	}
//	printf("f[%d][0]= %d f[%d][1]=%d\n",u,f[u][0],u,f[u][1]);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m>>type;
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++) {
		int u,v;
		scanf("%d%d",&u,&v);
		adde(u,v); adde(v,u);
	}
	dfs1(1,-1);
	while (m--) {
		memset(tag,-1,sizeof(tag));
		memset(f,0x3f,sizeof(f));
		INF=f[0][0];
		int u,v,xu,xv;
		scanf("%d%d%d%d",&u,&xu,&v,&xv);
		tag[u]=xu; tag[v]=xv;
		TreeDp(1,-1);
		int ans=min(f[1][0],f[1][1]);
		if (ans==INF) puts("-1");
		else printf("%d\n",ans);
	}
	return 0;
}