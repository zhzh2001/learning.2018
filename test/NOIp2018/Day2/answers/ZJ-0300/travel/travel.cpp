# include <bits/stdc++.h>
using namespace std;
const int MAXN=5005;
int head[MAXN],n,m,tot=0,ans[MAXN];
int eu,ev;
struct rec{ int pre,to; }a[MAXN*2];
bool vis[MAXN];
vector<int>E[MAXN];
vector<pair<int,int> >All;
void adde(int u,int v)
{
	a[++tot].pre=head[u];
	a[tot].to=v;
	head[u]=tot;
}
void dfs0(int u)
{
	vis[u]=true;
	for (int i=0;i<E[u].size();i++) {
		int v=E[u][i];
		if (vis[v]) continue;
		if (u==eu&&v==ev) continue;
		if (u==ev&&v==eu) continue;
		dfs0(v);
	}
}
bool check()
{
	memset(vis,false,sizeof(vis));
	dfs0(1);
	for (int i=1;i<=n;i++)
	 if (vis[i]==false) return false;
	return true; 
}
void dfs1(int u)
{
	ans[++ans[0]]=u; vis[u]=true;
	vector<int>tmp;
	for (int i=head[u];i;i=a[i].pre){
		int v=a[i].to; if (vis[v]) continue;
		tmp.push_back(v);
	}
	if (tmp.size()==0) return;
	sort(tmp.begin(),tmp.end());
	for (int i=0;i<tmp.size();i++)
	 dfs1(tmp[i]);
}
int aa[MAXN];
void dfs2(int u)
{
	aa[++aa[0]]=u; vis[u]=true;
	vector<int>tmp;
	for (int i=0;i<E[u].size();i++) {
		int v=E[u][i];
		if (vis[v]) continue;
		if (u==eu&&v==ev) continue;
		if (u==ev&&v==eu) continue;
		tmp.push_back(v);
	}
	if (tmp.size()==0) return;
	sort(tmp.begin(),tmp.end());
	for (int i=0;i<tmp.size();i++)
	 dfs2(tmp[i]);
}
void work1()
{

	memset(ans,0,sizeof(ans));
	memset(vis,false,sizeof(vis));
	dfs1(1);
	for (int i=1;i<=ans[0];i++) printf("%d ",ans[i]);
	putchar('\n');
}
void update(int *a)
{
	for (int i=1;i<=n;i++) 
		if (ans[i]>a[i]) break;
		else if (ans[i]<a[i]) return;
	for (int i=1;i<=n;i++)
	 ans[i]=a[i];	
}
void work2()
{
	memset(ans,0x3f,sizeof(ans));
	for (int i=0;i<All.size();i++) {
		eu=All[i].first; ev=All[i].second;
		if (!check()) continue;
		memset(aa,0,sizeof(aa));
		memset(vis,false,sizeof(vis));
		dfs2(1);
		update(aa);
	}
	for (int i=1;i<=n;i++)
	 printf("%d ",ans[i]);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	int u,v;
	for (int i=1;i<=m;i++) {
		scanf("%d%d",&u,&v);
		adde(u,v); adde(v,u);
		E[u].push_back(v);
		E[v].push_back(u);
		All.push_back(make_pair(u,v));
	}
	if (m==n-1) work1();
	else work2();
	return 0;
}