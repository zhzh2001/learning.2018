#include<bits/stdc++.h>
using namespace std;

ifstream fin("travel.in");
ofstream fout("travel.out");

const int N=5010;
int cnt,a[N*2],b[N*2],g[N*2],d[N*2],p[N*2],q[N*2],n,m;
int vis[N][N],vit[N];
void dfs(int x){
	p[++cnt]=x;vit[x]=1;
	for(int i=d[x];i<d[x+1];i++){
		int u=g[i];
		if(!vit[u]&&!vis[x][u])dfs(u);
	}
	vit[x]=0;
}

int main(){
	fin>>n>>m;
	for(int i=1;i<=m;i++)fin>>a[i]>>b[i],d[a[i]]++,d[b[i]]++;
	for(int i=1;i<=n;i++)d[i]+=d[i-1];d[n+1]=d[n]+1;
	for(int i=1;i<=m;i++){
		int u=a[i],v=b[i];
		g[d[u]--]=v;g[d[v]--]=u;
	}
	for(int i=1;i<=n;i++)++d[i];
	for(int i=1;i<=n;i++)sort(g+d[i],g+d[i+1]);
	if(m==n-1){
		cnt=0;
		dfs(1);
		for(int i=1;i<=cnt;i++)
			fout<<p[i]<<(i==cnt?'\n':' ');
		return 0;
	}else{
		for(int i=1;i<=n;i++)q[i]=n;
		for(int i=1;i<=m;i++){
			vis[a[i]][b[i]]=vis[b[i]][a[i]]=1;
			cnt=0;dfs(1);
			if(cnt==n){
				int flag=1;
				for(int i=1;i<=n;i++)
					if(p[i]!=q[i]){
						if(p[i]>q[i])flag=0;
						break;
					}
				if(flag)for(int i=1;i<=n;i++)q[i]=p[i];
			}
			vis[a[i]][b[i]]=vis[b[i]][a[i]]=0;
		}
		for(int i=1;i<=n;i++)
			fout<<q[i]<<(i==n?'\n':' ');
		return 0;
	}
	return 0;
}

