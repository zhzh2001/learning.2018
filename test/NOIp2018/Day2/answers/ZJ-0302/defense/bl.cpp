#include<bits/stdc++.h>
using namespace std;

const int N=100100;
typedef long long ll;
const ll inf=1e18;
ll dp[N][2];

int n,m,flag[N],a[N];
vector<int>g[N];

void dfs(int x,int fa=-1){
	dp[x][0]=0;dp[x][1]=a[x];
	if(flag[x]==1)dp[x][0]=inf;
	if(flag[x]==-1)dp[x][1]=inf;
	for(int i=0;i<(int)g[x].size();i++){
		int u=g[x][i];
		if(u!=fa){
			dfs(u,x);
			dp[x][1]+=min(dp[u][0],dp[u][1]);
			dp[x][0]+=dp[u][1];
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.ans","w",stdout);
	scanf("%d%d%*s",&n,&m);
	for(int i=1;i<=n;i++)scanf("%d",a+i);
	for(int i=1;i<n;i++){
		int x,y;scanf("%d%d",&x,&y);
		g[x].push_back(y);g[y].push_back(x);
	}
	while(m--){
		int u,v,x,y;scanf("%d%d%d%d",&u,&x,&v,&y);
		if(x)flag[u]=1;else flag[u]=-1;
		if(y)flag[v]=1;else flag[v]=-1;
		dfs(1);
		ll ans=min(dp[1][0],dp[1][1]);
		if(ans>=inf)ans=-1;
		printf("%lld\n",ans);
		flag[u]=flag[v]=0;
		
	}
}
