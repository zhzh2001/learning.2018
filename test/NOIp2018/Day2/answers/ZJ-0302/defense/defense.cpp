#include<bits/stdc++.h>
using namespace std;

ifstream fin("defense.in");
ofstream fout("defense.out");

const int N=300100;
typedef long long ll;
const ll inf=10000000000000000ll;
ll a[N],f[N][2],g[N][2];
inline ll min(const ll&a,const ll&b){
	return a<b?a:b;
}
struct mat{
	ll a[2][2];
	mat(){memset(a,0x3f,sizeof a);}
	mat operator * (const mat&b)const{
		mat c;
		for(int i=0;i<2;i++)
			for(int j=0;j<2;j++)
				for(int k=0;k<2;k++)
					c.a[i][k]=min(c.a[i][k],a[i][j]+b.a[j][k]);
		return c;
	}
}an[N*4];

int G[N],ne[N*2],to[N*2],dp[N],fa[N],sz[N],tp[N],hc[N],dfn[N],ed[N],dft[N],clk,cnt,lc[N*4],rc[N*4],rt[N],n,m,xb;
void dfs(int x){
	for(int i=G[x];~i;i=ne[i]){
		int u=to[i];
		if(u!=fa[x]){
			fa[u]=x;dp[u]=dp[x]+1;
			dfs(u);sz[x]+=sz[u];
			if(sz[u]>sz[hc[x]])hc[x]=u;
		}
	}
	if(!sz[x])sz[x]=1;
}

void dfs2(int x,int y){
	tp[x]=y;dfn[x]=++clk;dft[clk]=x;
	if(hc[x])dfs2(hc[x],y);else ed[y]=dfn[x];
	for(int i=G[x];~i;i=ne[i])
		if(!tp[to[i]])dfs2(to[i],to[i]);
}


void bt(int&o,int l,int r){
	o=++cnt;
	if(l==r){
		int x=dft[l];
		an[o].a[0][0]=inf;
		an[o].a[0][1]=g[x][0];
		an[o].a[1][0]=an[o].a[1][1]=a[x]+g[x][1];
		return;
	}
	int mid=(l+r)/2;
	bt(lc[o],l,mid);bt(rc[o],mid+1,r);
	an[o]=an[lc[o]]*an[rc[o]];
}


void mdf(int o,int l,int r,int x){
	if(l==r){
		x=dft[x];
		an[o].a[0][0]=inf;
		an[o].a[0][1]=g[x][0];
		an[o].a[1][0]=an[o].a[1][1]=a[x]+g[x][1];
		return;
	}
	int mid=(l+r)/2;
	if(x<=mid)mdf(lc[o],l,mid,x);
	else mdf(rc[o],mid+1,r,x);
	an[o]=an[lc[o]]*an[rc[o]];
}

void upd(int x){
	for(;x;x=fa[tp[x]]){
		int i=tp[x];mdf(rt[i],dfn[i],ed[i],dfn[x]);
		g[fa[i]][0]-=f[i][1];
		g[fa[i]][1]-=min(f[i][0],f[i][1]);
		f[i][0]=min(an[rt[i]].a[0][0],an[rt[i]].a[0][1]);
		f[i][1]=min(an[rt[i]].a[1][0],an[rt[i]].a[1][1]);
		g[fa[i]][0]+=f[i][1];
		g[fa[i]][1]+=min(f[i][0],f[i][1]);
	}
}
void add(int x,int y){ne[xb]=G[x];to[xb]=y;G[x]=xb++;}
string s;
int main(){
	fin>>n>>m>>s;memset(G,-1,sizeof G);
	for(int i=1;i<=n;i++)fin>>a[i];
	for(int i=1;i<n;i++){
		int x,y;fin>>x>>y;
		add(x,y);add(y,x);
	}
	dfs(1);dfs2(1,1);
	for(int I=n;I;I--){
		int i=dft[I];
		if(tp[i]==i){
			bt(rt[i],dfn[i],ed[i]);
			f[i][0]=min(an[rt[i]].a[0][0],an[rt[i]].a[0][1]);
			f[i][1]=min(an[rt[i]].a[1][0],an[rt[i]].a[1][1]);
			g[fa[i]][0]+=f[i][1];
			g[fa[i]][1]+=min(f[i][0],f[i][1]);
		}
	}
	while(m--){
		int u,v,x,y;fin>>u>>x>>v>>y;
		ll s=a[u],t=a[v];
		ll tmp=0;
		if(x==0){a[u]=inf;}
		else{a[u]=0;tmp+=s;}
		upd(u);
		if(y==0){a[v]=inf;}
		else{a[v]=0;tmp+=t;}
		upd(v);
		tmp+=g[0][1];
		if(tmp>=inf){
			fout<<-1<<'\n';
		}else fout<<tmp<<'\n';
		a[u]=s;upd(u);
		a[v]=t;upd(v);
	}
	return 0;
}

