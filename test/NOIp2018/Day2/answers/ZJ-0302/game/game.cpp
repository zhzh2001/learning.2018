#include<bits/stdc++.h>
using namespace std;

ifstream fin("game.in");
ofstream fout("game.out");
const int mod=1000000007;
typedef long long ll;
const int N=10;
const int M=600;
ll ret[M][M];
int ans,n,B[M],m,p[M],q[M],r[N][1<<N],cnt,tmp[N],tmq[N];
struct mat{
	int a[M][M];
	void sqr(){
		memset(ret,0,sizeof ret);
		for(int i=1;i<=cnt;i++)
			for(int j=1;j<=cnt;j++)if(a[i][j]){
				register int k1=1,k2=2,k3=3,k4=4;register ll t=a[i][j];
				register int*u=a[j];register ll*r=ret[i];
				for(;k1<=cnt;){
					(r[k1]+=t*u[k1])%=mod;k1+=4;
					(r[k2]+=t*u[k2])%=mod;k2+=4;
					(r[k3]+=t*u[k3])%=mod;k3+=4;
					(r[k4]+=t*u[k4])%=mod;k4+=4;
				}
			}
		for(int i=1;i<=cnt;i++)
			for(int j=1;j<=cnt;j++)
				a[i][j]=ret[i][j]%mod;
	}
}A;
ll pmt[M];
void mul(mat&A,int*B){
	memset(pmt,0,sizeof pmt);
	for(int i=1;i<=cnt;i++)
		for(int j=1;j<=cnt;j++)
			pmt[i]=(pmt[i]+(ll)A.a[i][j]*B[j])%mod;
	for(int i=1;i<=cnt;i++)B[i]=pmt[i]%mod;
}
int main(){
	fin>>n>>m;
	if(m==1){
		fout<<(1<<n)<<endl;
		return 0;
	}
	for(int i=0;i<n;i++){
		for(int j=0;j<(1<<(i+1));j++){
			p[++cnt]=i;q[cnt]=j;
			r[i][j]=cnt;
		}
	}
	for(int i=1;i<=cnt;i++)
		for(int j=1;j<=cnt;j++)
			if(p[i]<=p[j]){
				int flag=1;
				for(int k=0;k<n;k++)
					tmp[k]=!(q[i]>>k&1);
				for(int k=0;k<n;k++)
					tmq[k]=!(q[j]>>k&1);
				for(int k=0;k+1<n;k++)if(tmq[k+1]<tmp[k])flag=0;
				if(p[i]!=p[j]&&tmq[p[j]]!=tmp[p[j]-1])flag=0;
				for(int k=p[j];k+1<n;k++)if(tmq[k+1]==tmp[k])flag=0;
				for(int k=p[i]-1;k>0;k--)if(tmp[k-1]!=tmq[k])flag=0;
				A.a[i][j]=flag;
			}
	m-=2;
	for(int i=1;i<=cnt;i++)B[i]=1;
	for(;m;m/=2,A.sqr())if(m&1)mul(A,B);
	for(int i=0;i<(1<<n);i++)
		for(int j=1;j<=cnt;j++){
			int flag=1;
			for(int k=0;k<n;k++)
				tmp[k]=!(i>>k&1);
			for(int k=0;k<n;k++)
				tmq[k]=!(q[j]>>k&1);
			for(int k=0;k+1<n;k++)if(tmq[k+1]<tmp[k])flag=0;
			if(p[j]&&tmq[p[j]]!=tmp[p[j]-1])flag=0;
			for(int k=p[j];k+1<n;k++)if(tmq[k+1]==tmp[k])flag=0;
			if(flag)ans=(ans+B[j])%mod;
		}
	fout<<(ans+mod)%mod<<endl;
	return 0;
}

