#include<bits/stdc++.h>
using namespace std;
const int N=5003;
int n,m,*a[N],x[N],y[N],b[N],ans[N]={INT_MAX},s,t,_ans[N];
bool f[N];
char g[N]={3};//1:available,2:used,0:unavailable
bool cmp(int a,int b){
	return a<b;
}
void dfs(int x){
	f[x]=1;
	ans[s++]=x;
	for(int k=1;k<=b[x];k++)
	if(!f[a[x][k]])
	dfs(a[x][k]);
}
void copy(){
	bool co=0;
	int i=0;
	for(;i<n;i++)
	if(ans[i]>_ans[i])
	{
		co=1;
		break;
	}
	if(co)
	for(;i<n;i++)
	ans[i]=_ans[i];
}
void search(){
	for(int start=1;s<n;)
	{
		ans[s++]=start;
		g[start]|=2;
		for(int k=1;k<=b[start];k++)
		g[a[start][k]]|=1;
		int i=1;
		for(;i<=n;i++)
		if(g[i]==1)
		{
			start=i;
			break;
		}
	}
}
void tansearch(int x,int times){
	_ans[times]=x;
	if(times==n-1)
	{
		copy();
		return;
	}
	f[x]=1;
	for(int k=1;k<=b[x];k++)
	if(!f[a[x][k]])
	tansearch(a[x][k],times+1);
	f[x]=0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	scanf("%d%d",x+i,y+i),b[x[i]]++,b[y[i]]++;
	for(int i=1;i<=n;i++)
	a[i]=new int[b[i]+1],a[i][0]=0;
	for(int i=1;i<=m;i++)
	a[x[i]][++a[x[i]][0]]=y[i],
	a[y[i]][++a[y[i]][0]]=x[i];
	for(int i=1;i<=n;i++)
	sort(a[i]+1,a[i]+b[i]+1,cmp);
	if(m==n-1)
	dfs(1);
	else
	search();
	printf("1");
	for(int i=1;i<n;i++)
	printf(" %d",ans[i]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
