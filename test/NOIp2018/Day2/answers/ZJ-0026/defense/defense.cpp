#include<bits/stdc++.h>
//#define debug
using namespace std;
const int N=100003;
const long long BIG=0xffffffffff;
int *a[N],b[N],x[N],y[N],n,m,w[N],t[N]={1},tt;
long long g[N],ng[N];
char type[3];
bool f[N];
struct ques{
	int a,x,b,y;
}q;
struct node{
	long long g,ng;
	node(){g=ng=0;}
}s[N];
void dfs(int k){
	node s[100];
	f[k]=1;
	int son=0;
	for(int i=1;i<=b[k];i++)
	if(!f[a[k][i]])
	{
		dfs(a[k][i]);
		s[++son].g=g[a[k][i]];
		s[son].ng=ng[a[k][i]];
	}
	for(int i=1;i<=son;i++)
	g[k]+=min(s[i].g,s[i].ng),
	ng[k]+=s[i].g;
	g[k]+=w[k];
}
void bfs(){
	for(int j=0;j<n;j++)
	{
		int k=t[j];
		f[k]=1;
		for(int i=1;i<=b[k];i++)
		if(!f[a[k][i]])
		t[++tt]=a[k][i];
	}
	for(int i=1;i<=n;i++)
	for(int j=n-1;~j;j--)
	{
		int k=t[j];
		int son=0;
		for(int i=1;i<=b[k];i++)
		if(!f[a[k][i]])
		{
			s[++son].g=g[a[k][i]];
			s[son].ng=ng[a[k][i]];
		}
		for(int i=1;i<=son;i++)
		g[k]+=min(s[i].g,s[i].ng),
		ng[k]+=s[i].g;
		g[k]+=w[k];
		f[k]=0;
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,type);
	for(int i=1;i<=n;i++)
	scanf("%d",w+i);
	for(int i=1;i<n;i++)
	scanf("%d%d",x+i,y+i),b[x[i]]++,b[y[i]]++;
	for(int i=1;i<=n;i++)
	a[i]=new int[b[i]+1],a[i][0]=0;
	for(int i=1;i<n;i++)
	a[x[i]][++a[x[i]][0]]=y[i],
	a[y[i]][++a[y[i]][0]]=x[i];
#ifdef debug
	for(int i=1;i<=n;i++,printf("\n"))
	for(int k=1;k<=b[i];k++)
	printf("%d\t",a[i][k]);
	cout<<BIG<<endl;
#endif
	for(int i=1;i<=m;i++)
	{
		bool bo=0;
		for(int i=1;i<=n;i++)f[i]=g[i]=ng[i]=0;
		scanf("%d%d%d%d",&q.a,&q.x,&q.b,&q.y);
//		for(int k=1;k<=b[q.a];k++)
//		if(q.b==a[q.a][k])
//		{
//			printf("-1\n");
//			bo=1;
//			break;
//		}
		if(bo==1)continue;
		if(q.x==1)ng[q.a]=BIG;
		else g[q.a]=BIG;
		if(q.y==1)ng[q.b]=BIG;
		else g[q.b]=BIG;
		dfs(1);
//		bfs();
		long long ans=min(g[1],ng[1]);
		printf("%lld\n",ans>=BIG?-1:ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
