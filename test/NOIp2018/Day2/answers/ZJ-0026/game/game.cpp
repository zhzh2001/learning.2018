#include<bits/stdc++.h>
using namespace std;
const int N=9,M=1000001,MOD=1000000007;
int n,m,ans;
char a[N][M],che[N][M];
char check(int x,int y){
	if(che[x][y])return che[x][y]&1;
	if(x+1==n||y+1==m)return che[x][y]=3;
	if(a[x+1][y]==a[x][y+1])
	return che[x][y]=check(x+1,y)&check(x,y+1);
	else if(a[x+1][y]>a[x][y+1])return che[x][y]=3;
	else return che[x][y]=2;
}
void force(int x,int y){
	if(x==n)
	{
		if(check(0,0))ans++;
		return;
	}
	a[x][y]=0;
	if(y+1==m)force(x+1,0);
	else force(x,y+1);
	a[x][y]=1;
	if(y+1==m)force(x+1,0);
	else force(x,y+1);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==1||m==1)
	{
		int times=max(n,m);
		int x=1;
		for(int i=1;i<=times;i++)
		x<<=1,x%=MOD;
		printf("%d",x);
	}
	force(0,0);
	printf("%d",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
