#include<bits/stdc++.h>
using namespace std;
int tot,n,m,num,now,flag,cnt,g[5001],ans[50001],b[50001],e[100001],head[100001],nxt[100001];
void build(int t,int k){
	tot++;
	e[tot]=k;
	nxt[tot]=head[t];head[t]=tot;
}
void dfs(int x,int fa){
	//printf("%d %d\n",x,fa);
	int i;
	vector<int>aa;
	b[++cnt]=x;
	for(i=head[x];i;i=nxt[i]){
		if((i+1)/2==now||e[i]==fa)continue;
		aa.push_back(e[i]);
	}
	sort(aa.begin(),aa.end());
	int sum=aa.size();
	for(i=0;i<sum;i++)dfs(aa[i],x);
}
void check(){
	int i;
	//for(i=1;i<=n;i++)printf("%d ",b[i]);
	//puts("");
	if(!flag){
		flag=1;
		for(i=1;i<=n;i++)ans[i]=b[i];
		return;
	}
	int now=0;
	for(i=1;i<=n;i++)
	 if(b[i]<ans[i]){
	 	now=1;break;
	 }
	  else if(b[i]>ans[i])break;
	if(now)
	 for(i=1;i<=n;i++)ans[i]=b[i];
}
void dfs1(int x,int fa){
	int i;
	num++;g[x]=1;
	for(i=head[x];i;i=nxt[i]){
		if((i+1)/2==now||g[e[i]])continue;
		dfs1(e[i],x);
	}
}
int main(){
freopen("travel.in","r",stdin);
freopen("travel.out","w",stdout);	
	int i,t,k;
	scanf("%d%d",&n,&m);
	if(m==n-1){
		for(i=1;i<n;i++){
			scanf("%d%d",&t,&k);
			build(t,k);build(k,t);
		}
		dfs(1,0);
		for(i=1;i<=n;i++)printf("%d ",b[i]);
		return 0;
	}
	for(i=1;i<=n;i++){
		scanf("%d%d",&t,&k);
		build(t,k);build(k,t);
}
    for(i=1;i<=n;i++){
    	now=i;cnt=num=0;
    	memset(g,0,sizeof(g));
    	dfs1(1,0);
    	if(num<n)continue;
    	//printf("%d\n",i);
    	dfs(1,0);
    	//puts("-1");
    	check();
}
    for(i=1;i<=n;i++)printf("%d ",ans[i]);
   return 0;
}
/*
6 5
1 3
2 3
2 5
3 4
4 6

6 6
1 3
2 3
2 5
3 4
4 5
4 6
*/
