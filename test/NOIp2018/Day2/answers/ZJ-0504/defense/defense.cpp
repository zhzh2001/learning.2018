#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll tot,n,m,A,X,B,Y,e[300001],head[300001],nxt[300001],f[2001][3],p[100001];
void build(ll t,ll k){
	tot++;
	e[tot]=k;
	nxt[tot]=head[t];head[t]=tot;
}
void dfs(ll x,ll fa){
	ll i,sum=0,flag=0,sum1=1e9;
	f[x][2]=p[x];
	for(i=head[x];i;i=nxt[i]){
		if(e[i]==fa)continue;
		dfs(e[i],x);
		f[x][2]+=min(f[e[i]][0],f[e[i]][2]);
		f[x][0]+=f[e[i]][2];
		/*if(f[e[i]][2]<=f[e[i]][1]){
			flag=1;sum+=f[e[i]][2];
		}
		 else{
		 	sum+=f[e[i]][1];
		 	sum1=min(sum1,f[e[i]][2]-f[e[i]][1]);
		 }*/
	}
	/*if(flag)f[x][1]=sum;
	 else f[x][1]=sum+sum1;*/
	if(x==A){
		if(X==1)f[x][0]=1e12;
		 else f[x][2]=1e12;
	}
	if(x==B){
		if(Y==1)f[x][0]=1e12;
		 else f[x][2]=1e12;
	}
	//printf("%lld %lld %lld %lld %lld\n",x,fa,f[x][0],f[x][1],f[x][2]);
	f[x][0]=min(f[x][0],(ll)1e12);
	f[x][2]=min(f[x][2],(ll)1e12);
}
int main(){
freopen("defense.in","r",stdin);
freopen("defense.out","w",stdout);	
	ll i,t,k;
	char s[10];
	scanf("%lld %lld %s",&n,&m,&s);
	if(n<=2000&&m<=2000){
		for(i=1;i<=n;i++)scanf("%lld",&p[i]);
		for(i=1;i<n;i++){
			scanf("%lld%lld",&t,&k);
			build(t,k);build(k,t);
		}
		while(m--){
			scanf("%lld%lld%lld%lld",&A,&X,&B,&Y);
			memset(f,0,sizeof(f));
			dfs(1,0);
			if(min(f[1][0],f[1][2])>=1e9)puts("-1");
		 else printf("%lld\n",min(f[1][0],f[1][2]));
		}
	}
	return 0;
}

/*
5 3 C3
2 4 1 3 9
1 5
5 2
5 3
3 4
1 0 3 0
2 1 3 1
1 0 5 0
*/
