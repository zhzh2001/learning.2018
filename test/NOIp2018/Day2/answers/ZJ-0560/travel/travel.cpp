#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m;
//solve1
int ans1[5010],len1=0;
int e[5010][5010],ss[5010];
//solve2
int In[5010],q[5010],he=1,ta=0;
bool c[5010][5010],vis[5010];
int tlen,tans[5010];

int read() {
	int tmp=0;char c=getchar(); while(c<'0'||c>'9')	c=getchar();
	while(c>='0'&&c<='9')	tmp=(tmp<<1)+(tmp<<3)+(c^48),c=getchar();
	return tmp;
}
int Min(int x,int y) { return x<y?	x:y; }
void dfs1(int u,int fa) {
	ans1[++len1]=u; for(int i=1;i<=ss[u];i++)	if(e[u][i]!=fa)	dfs1(e[u][i],u);
}
void solve1() {
	for(int i=1,x,y;i<=m;i++)	x=read(),y=read(),e[x][++ss[x]]=y,e[y][++ss[y]]=x;
	for(int i=1;i<=n;i++)	sort(e[i]+1,e[i]+ss[i]+1);
	dfs1(1,0);
	printf("%d",ans1[1]); for(int i=2;i<=len1;i++)	printf(" %d",ans1[i]);
	printf("\n");
}
void Find_circle() {
	for(int i=1;i<=n;i++)	if(In[i]==1)	q[++ta]=i,vis[i]=1;
	while(he<=ta) {
		int u=q[he++];
		for(int i=1;i<=ss[u];i++) {
			c[u][i]=1,--In[e[u][i]];
			if(In[e[u][i]]==1&&!vis[e[u][i]])	vis[e[u][i]]=1,q[++ta]=e[u][i];
		}
	}
}
void dfs2(int u,int fa,int x,int y) {
	tans[++tlen]=u;
	for(int i=1;i<=ss[u];i++) {
		if(fa==e[u][i]||(u==x&&e[u][i]==y)||(u==y&&e[u][i]==x))	continue;
		dfs2(e[u][i],u,x,y);
	}
}
bool judge() {
	for(int i=1;i<=n;i++) {
		if(ans1[i]<tans[i])	return 0; if(ans1[i]>tans[i])	return 1;
	}
	return 0;
}
void solve2() {
	for(int i=1;i<=n;i++)	ans1[i]=0x3f3f3f3f;
	for(int i=1,x,y;i<=m;i++)
		x=read(),y=read(),e[x][++ss[x]]=y,e[y][++ss[y]]=x,++In[x],++In[y];
	for(int i=1;i<=n;i++)	sort(e[i]+1,e[i]+ss[i]+1);
	Find_circle();
	for(int i=1;i<=n;i++) {
		if(vis[i])	continue;
		for(int j=1;j<=ss[i];j++)
			if(!vis[e[i][j]]) {
				if(i>e[i][j])	continue;
				tlen=0,dfs2(1,0,i,e[i][j]);
				if(judge())	for(int k=1;k<=n;k++)	ans1[k]=tans[k];
			}
	}
	printf("%d",ans1[1]); for(int i=2;i<=n;i++)	printf(" %d",ans1[i]);
	printf("\n");
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read(); memset(ss,0,sizeof(ss));
	if(m==n-1)	solve1(); else	solve2();
	fclose(stdin);fclose(stdout);
	return 0;
}
