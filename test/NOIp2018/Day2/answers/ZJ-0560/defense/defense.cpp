#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
const int N=100010;
const ll INF=10000000000;
int n,m,used[N];
int cnt=0,hed[N],to[N+N],nxt[N+N];
ll w[N],f[N][2];
char opt[5];

int read() {
	int tmp=0;char c=getchar(); while(c<'0'||c>'9')	c=getchar();
	while(c>='0'&&c<='9')	tmp=(tmp<<1)+(tmp<<3)+(c^48),c=getchar();
	return tmp;
}
ll Min(ll x,ll y) { return x<y?	x:y; }
void add(int x,int y) { to[++cnt]=y,nxt[cnt]=hed[x],hed[x]=cnt; }
void dfs(int u,int fa) {
	f[u][1]=w[u];
	if(used[u]==1)	f[u][0]=INF; else if(used[u]==0)	f[u][1]=INF;
	for(int i=hed[u];i;i=nxt[i]) {
		if(to[i]==fa)	continue;
		dfs(to[i],u),f[u][0]+=f[to[i]][1],f[u][1]+=Min(f[to[i]][1],f[to[i]][0]);
	}
}
void solve1() {
	memset(used,-1,sizeof(used));
	for(;m;--m) {
		int x,v1,y,v2; x=read(),v1=read(),y=read(),v2=read();
		used[x]=v1,used[y]=v2,memset(f,0,sizeof(f));
		dfs(1,0); ll tans=0;
		if(used[1]==0)	tans=f[1][0];
		else if(used[1]==1)	tans=f[1][1];
		else	tans=Min(f[1][0],f[1][1]);
		if(tans>=INF)	printf("-1\n");
		else	printf("%lld\n",tans);
		used[x]=-1,used[y]=-1;
	}
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read(),scanf("%s",opt); for(int i=1;i<=n;i++)	w[i]=(ll)read();
	for(int i=1,x,y;i<n;i++)	scanf("%d%d",&x,&y),add(x,y),add(y,x);
	if(n<=2000&&m<=2000) { solve1();return 0; }
	
	fclose(stdin);fclose(stdout);
	return 0;
}
