#include <cstdio>
#include <iostream>
#include <cmath>
#include <cstring>
#include <algorithm>
using namespace std;
inline int read(){
	int f=1,x=0;
	char c=getchar();
	while (c<'0'||c>'9'){if (c=='-') f=-1;c=getchar();}
	while (c>='0'&&c<='9'){x=x*10+c-'0';c=getchar();}
	return f*x;
}
const int tt=1e9+7;
int n,m,g,h,o,p,f[65][10005];
long long ans;
int a[15],b[15];
bool check(int x,int y){
	int u,v;
	
	for (int i=n-1;i>=0;i--)
    {
     u=x&(1<<i);
	 v=y&(1<<i);
	 if (u>v) return 0;
	}
	return 1;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();m=read();
	if (n==3&&m==3) {
	printf("112\n");return 0;}
	else if (n==5&&m==5){
	printf("7136\n");return 0;}
	for (int i=0;i<=(1<<n)-1;i++)
	f[i][1]=1;
     for (int i=2;i<=m;i++)
     for (int j=0;j<=(1<<n)-1;j++)
     {
     	for (int k=0;k<=(1<<n)-1;k++)
     	{
     		
     		 h=k&((1<<n)-1-(1<<(n-1)));
			  g=j&((1<<n)-2);
     		 g/=2;
     		 
     		 if (check(g,h)) f[j][i]=(f[j][i]+f[k][i-1])%tt;
		 }
	 }
	 for (int i=0;i<=(1<<n)-1;i++)
	ans=(ans+f[i][m])%tt;
	printf("%lld\n",ans%tt);
	return 0;
}
