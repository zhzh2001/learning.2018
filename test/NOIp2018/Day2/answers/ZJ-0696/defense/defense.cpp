#include <cstdio>
#include <iostream>
#include <cmath>
#include <cstring>
#include <algorithm>
using namespace std;
inline int read(){
	int f=1,x=0;
	char c=getchar();
	while (c<'0'||c>'9'){if (c=='-') f=-1;c=getchar();}
	while (c>='0'&&c<='9'){x=x*10+c-'0';c=getchar();}
	return f*x;
}
const int maxn=1e6+5;
int n,m,g,h,o,p,cnt;
int s[maxn],nxt[maxn],w[maxn],a[maxn];
char c,c1,ans;
int vis[maxn];
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m>>c>>c1;
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<n;i++)
	{
		int x=read(),y=read();
	}
	for (int i=1;i<=m;i++){
		memset(vis,0,sizeof(vis));
		int x=read(),y=read(),xx=read(),yy=read();
		if (x<y) {swap(x,xx),swap(y,yy);}
		ans=0;
		if (x==xx-1&&y==0&&yy==0) printf("-1\n");
		else {
			for (int j=x;j>=1;j--)
			if (j%2==x%2) vis[j]=y,ans=ans+(y*a[j]);
			for (int j=xx;j<=n;j++)
			if (j%2==xx%2) vis[j]=yy,ans=ans+(yy*a[j]);
			if (x!=xx-1)
			{
				if (y==0) vis[x+1] =1;
				if (yy=0) vis[xx-1]=1;
			int sum=0,sum1=0;
				for (int j=x+2;j<=xx-2;j++)
				if (j%2) sum+=a[j];
				else sum1+=a[j];
			    ans=min(sum,sum1);
			}
			printf("%d\n",ans);
		} 
	}
	
	return 0;
}
