#include <cstdio>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstring>
using namespace std;
inline int read() {
	int f=1,x=0;
	char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9') {
		x=x*10+c-'0';
		c=getchar();
	}
	return f*x;
}
const int maxn=1e5+5;
int n,m,g,t1,w1,h[maxn*10];
int lnk[maxn],nxt[maxn],s[maxn],cnt;
int vis[5005],b[5005],v[maxn];
int a[5005];
int c[5005][5005];
void adde(int x,int y) {
	s[++cnt]=y;
	nxt[cnt]=lnk[x];
	lnk[x]=cnt;
}
bool cmp(int x,int y) {
	if (x>y) return 1;
	else return 0;
}
void dfs(int x) {
	h[++t1]=x;
	if (t1==n) {
		for (int i=1; i<=n; i++)  printf("%d ",h[i]);
		printf("\n");
		return;
	}
	int num=0;
	for (int i=lnk[x]; i!=-1; i=nxt[i])
		if (!vis[s[i]]) {
			a[++num]=s[i];
			vis[s[i]]=1;
		}
	sort(a+1,a+num+1);
	for (int i=1; i<=num; i++)
		c[x][i]=a[i];
	for (int i=1; i<=num; i++)
		dfs(c[x][i]);
}
void dfs1(int x) {
	h[++t1]=x;

	if (t1==n) {
		int flag=0;
		for (int i=1; i<=n; i++)  if (h[i]<b[i]) {
				flag=1;
				break;
			} else if (h[i]>b[i]) break;
		if (flag==1) {
			for (int i=1; i<=n; i++) b[i]=h[i];
		}
		return;
	}
	int num=0;
	for (int i=lnk[x]; i!=-1; i=nxt[i])
		if (!vis[s[i]]&&!v[i]) {
			a[++num]=s[i];
			vis[s[i]]=1;
		}
	sort(a+1,a+num+1);
	for (int i=1; i<=num; i++)
		c[x][i]=a[i];
	for (int i=1; i<=num; i++)
		dfs1(c[x][i]);
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(lnk,-1,sizeof(lnk));
	n=read();
	m=read();
	for (int i=1; i<=m; i++) {
		int x=read(),y=read();
		adde(x,y);
		adde(y,x);
	}
	if (m==n-1) {
		vis[1]=1,dfs(1);
	} else {
		for (int i=1; i<=n; i++) b[i]=6000;

		for (int i=1; i<=n; i++) {
			v[i*2]=1;
			v[i*2-1]=1;
			memset(vis,0,sizeof(vis));
			vis[1]=1;
			t1=0;
			dfs1(1);
			v[i*2]=0;
			v[i*2-1]=0;
		}

		for (int i=1; i<=n; i++)
			printf("%d ",b[i]);
			printf("\n");
	}
	return 0;
}
