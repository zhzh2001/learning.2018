#include<bits/stdc++.h>
using namespace std;
const int kcz=1e9+7;
#define ll long long
int fastmi(int x,int y){
	ll ans=1,now=x;
	while (y){
		if (y&1) ans=ans*now%kcz;
		now=now*now%kcz;
		y>>=1;
	}
	return ans;
}

int read(){
	int n;
	scanf("%d",&n);
	return n;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	int n=read(),m=read();
	if (n>m) swap(n,m);
	if (n==1)
	{
		printf("%d\n",fastmi(2,m));
		return 0;
	}
	if (n==2)
	{
		printf("%d\n",4ll*fastmi(3,m-1)%kcz);
		return 0;
	}
	if ((n==3)&&(m==3))
	{
		printf("112");
		return 0;
	}
	if (n==3)
	{
		printf("%d",(36ll*fastmi(4,m-2)-fastmi(2,m+2)+kcz)%kcz);
		return 0;
	}
}
	
	
