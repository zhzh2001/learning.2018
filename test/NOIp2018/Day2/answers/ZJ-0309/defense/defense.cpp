#include<cstdio>
#include<algorithm>
#include<cstring>
#define ll long long
using namespace std;
struct edge{int u,v,next;}E[10100];
const int inf=1<<30;
ll dp[2010][2],p[10100];
int n,m,x,y,z,a,b,va,vb,t,head[10100],fa[10100];
char type[10];
inline int read()
{
  int cnt=0,f=1;char ch=getchar();
  while (ch<'0'||ch>'9'){if (ch=='-')f=-1;ch=getchar();}
  while (ch>='0'&&ch<='9'){cnt=cnt*10+ch-48;ch=getchar();}
  return cnt*f;
}
inline ll min(ll a,ll b){return a<b?a:b;}
void build(int x,int y)
{
   for (int i=head[x];i!=0;i=E[i].next)
    if (E[i].v!=y)
    {
      fa[E[i].v]=x;
      build(E[i].v,x);
	}
}
void dfs(int x,int y)
{
   dp[x][1]=p[x];
   dp[x][0]=0;
   for (int i=head[x];i!=0;i=E[i].next)
    if (E[i].v!=y)
    {
	  dfs(E[i].v,x);
	  if (x!=a&&x!=b)
      {
      dp[x][1]+=min(dp[E[i].v][0],dp[E[i].v][1]);
      dp[x][0]+=dp[E[i].v][1];
      }
      else
      {
        if (x==a) 
		{
		 if (va==1) dp[x][1]+=min(dp[E[i].v][0],dp[E[i].v][1]);
		 else dp[x][0]+=dp[E[i].v][1];
	    }
		if (x==b) 
		{
		 if (vb==1) dp[x][1]+=min(dp[E[i].v][0],dp[E[i].v][1]);
		 else dp[x][0]+=dp[E[i].v][1];
	    }	
	  }
	}
   if (x==a) dp[x][!va]=inf;
   if (x==b) dp[x][!vb]=inf;
}
int main()
{
   freopen("defense.in","r",stdin);
   freopen("defense.out","w",stdout);
   n=read();m=read();scanf("%s",type);
   for (int i=1;i<=n;i++)p[i]=read();
   for (int i=1;i<n;i++)
    {
      x=read();y=read();
      E[i].u=x;E[i].v=y;E[i].next=head[x];head[x]=i;t=i+n;
      E[t].u=y;E[t].v=x;E[t].next=head[y];head[y]=t;
	}
   build(1,0);
   for (int i=1;i<=m;i++)
    {
      a=read();va=read();b=read();vb=read();	
      if ((fa[a]==b&&!va&&!vb)||(fa[b]==a&&!va&&!vb)) {puts("-1");continue;}
	  dfs(1,0);
	  printf("%lld\n",min(dp[1][0],dp[1][1]));
	}
   fclose(stdin);
   fclose(stdout);
   return 0;
}
