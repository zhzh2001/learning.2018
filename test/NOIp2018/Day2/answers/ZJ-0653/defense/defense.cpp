#include<bits/stdc++.h>
using namespace std;
#define N 100010
#define ll long long
ll n,m;
char s[100];
ll va[N],dp[N][2],a,xx,b,yy,head[N],to[N<<1],nxt[N<<1],u,v,cnt;
void add(ll u,ll v)
{
	nxt[++cnt]=head[u];
	head[u]=cnt;
	to[cnt]=v;
}
void dfs(ll x,ll fa)
{
	dp[x][0]=0;
	dp[x][1]=va[x];
	for (ll i=head[x];i;i=nxt[i])
		if (to[i]!=fa) dfs(to[i],x);
	for (ll i=head[x];i;i=nxt[i])
	if (to[i]!=fa)
	{
		dp[x][0]+=dp[to[i]][1];
		dp[x][1]+=min(dp[to[i]][1],dp[to[i]][0]);
	}
	if (x==a) dp[x][xx^1]=1e14;
	if (x==b) dp[x][yy^1]=1e14;
	dp[x][0]=min(dp[x][0],(ll)1e14);
	dp[x][1]=min(dp[x][1],(ll)1e14);
	return;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%lld%lld %s",&n,&m,s);
	for (ll i=1;i<=n;i++) scanf("%lld",&va[i]);
	for (ll i=1;i<n;i++)
	{
		scanf("%lld%lld",&u,&v);
		add(u,v);
		add(v,u);
	}	
	for (ll i=1;i<=m;i++)
	{
		scanf("%lld%lld%lld%lld",&a,&xx,&b,&yy);
		bool ft=0;
		for (ll i=head[a];i;i=nxt[i])
			if (to[i]==b && xx==0 && yy==0)
			{
				ft=1;
				printf("-1\n");
			}
		if (ft) continue;
		dfs(1,0);
		printf("%lld\n",min(dp[1][0],dp[1][1]));
	}
}
