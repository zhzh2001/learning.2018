#include<bits/stdc++.h>
#define N 5101
using namespace std;
int n,m,head[N],to[N<<1],nxt[N<<1],cnt,u,v,tar,du[N],ft,t[N],tt,fat[N];
bool f[N],ff[N];
stack<int> s;
vector <int> e[N];
void add(int u,int v)
{
	nxt[++cnt]=head[u];
	head[u]=cnt;
	to[cnt]=v;
}
void gloop(int x,int fa)
{
	ff[x]=1;
	fat[x]=fa;
	for (int i=head[x];i;i=nxt[i])
		if (to[i]!=fa)
		{
			if (ff[to[i]])
			{
				int t=x;
				while (x!=to[i])
				{
					f[x]=1;
					x=fat[x];
				}
				f[to[i]]=1;
				ft=1;
				return;
			}
			else gloop(to[i],x);
			if (ft) return;
		}
}
void dfs(int x,int fa)
{
	if (ft==1 && x>tar && f[x]) 
	{
		ft=2;
		return;
	}
	if (x==tar) ft=2;
	printf("%d ",x);
	ff[x]=1;
	for (int i=0;i<e[x].size();i++)
		if (e[x][i]!=fa && !ff[e[x][i]])
		{
			if (f[e[x][i]] && f[x] && !ft) ft=1;
			if (f[x] && f[e[x][i]] && i!=e[x].size()-1) tar=e[x][i+1];
			dfs(e[x][i],x);
		}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d",&u,&v);
		e[u].push_back(v);
		e[v].push_back(u);
		du[u]++;du[v]++;
		add(u,v);
		add(v,u);
	}
	for (int i=1;i<=n;i++) 
	{
		for (int j=0;j<e[i].size();j++) t[j]=e[i][j];
		int ll=e[i].size();
		sort(t,t+ll);
		e[i].clear();
		for (int j=0;j<ll;j++) e[i].push_back(t[j]);
	}
	gloop(1,0);
	tar=1e8;
	memset(ff,0,sizeof(ff));
	ft=0;
	dfs(1,0);
	printf("\n");
}
