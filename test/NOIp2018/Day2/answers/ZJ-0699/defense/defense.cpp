#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define ll long long
using namespace std;
const ll INF=(1ll<<60);
const int N=100005;
struct edge{
	int to,next;
}e[N*2];
int head[N],tot,n,Q;
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
}
int p1,v1,p2,v2;
ll f[N][2],v[N];
void bf(int x,int fa){
	f[x][0]=0; f[x][1]=v[x];
	if (x==p1) f[x][1-v1]=INF;
	if (x==p2) f[x][1-v2]=INF;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa){
			int to=e[i].to; bf(to,x);
			ll lv0=f[x][0],lv1=f[x][1];
			f[x][0]=min(INF,lv0+f[to][1]);
			f[x][1]=min(INF,lv1+min(f[to][0],f[to][1]));
		}
}
inline int read(){
	int x=0;
	#define gc getchar
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
struct node{
	ll a[2][2];
	node operator +(const node &b)const{
		node ans;
		For(v1,0,1) For(v2,0,1) ans.a[v1][v2]=INF;
		For(v1,0,1) For(v2,0,1) For(v3,0,1) For(v4,0,1)
			if (v2||v3) ans.a[v1][v4]=min(ans.a[v1][v4],a[v1][v2]+b.a[v3][v4]);
		return ans;
	}
}t[N*4];
void build(int k,int l,int r){
	if (l==r){
		t[k].a[0][1]=t[k].a[1][0]=INF;
		t[k].a[0][0]=0; t[k].a[1][1]=v[l];
		return;
	}
	int mid=(l+r)/2;
	build(k*2,l,mid);
	build(k*2+1,mid+1,r);
	t[k]=t[k*2]+t[k*2+1];
}
node ask(int k,int l,int r,int x,int y){
	if (l==x&&r==y) return t[k];
	int mid=(l+r)/2;
	if (y<=mid) return ask(k*2,l,mid,x,y);
	if (x>mid) return ask(k*2+1,mid+1,r,x,y);
	return ask(k*2,l,mid,x,mid)+ask(k*2+1,mid+1,r,mid+1,y);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(); Q=read(); read();
	For(i,1,n) v[i]=read();
	bool chain=1;
	For(i,1,n-1){
		int x,y;
		scanf("%d%d",&x,&y);
		if (x-y!=1&&x-y!=-1) chain=0;
		add(x,y); add(y,x);
	}
	if (n<=2000){
		while (Q--){
			p1=read(); v1=read(); p2=read(); v2=read();
			bf(1,0);
			ll ans=min(f[1][0],f[1][1]);
			printf("%lld\n",ans>1ll<<50?-1:ans);
		}
		return 0;
	}
	if (chain){
		build(1,1,n);
		while (Q--){
			p1=read(); v1=read(); p2=read(); v2=read();
			if (p1>p2) swap(p1,p2),swap(v1,v2);
			ll ans=v1*v[p1]+v2*v[p2];
			if (p1!=1){
				node tmp=ask(1,1,n,1,p1-1);
				ll tmpv=INF;
				For(v3,0,1) For(v4,0,1) if (v4||v1)
					tmpv=min(tmpv,tmp.a[v3][v4]);
				ans+=tmpv;
			}
			if (p1+1!=p2){
				node tmp=ask(1,1,n,p1+1,p2-1);
				ll tmpv=INF;
				For(v3,0,1) For(v4,0,1)
					if ((v3||v1)&&(v4||v2))
						tmpv=min(tmpv,tmp.a[v3][v4]);
				ans+=tmpv;
			}
			else if (v1==0&&v2==0) ans=INF;
			if (p2!=n){
				node tmp=ask(1,1,n,p2+1,n);
				ll tmpv=INF;
				For(v3,0,1) For(v4,0,1) if (v3||v2)
					tmpv=min(tmpv,tmp.a[v3][v4]);
				ans+=tmpv;
			}
			printf("%lld\n",ans>1ll<<50?-1:ans);
		}
	}
}
/*
10 10 233
42 43 23 19 24 49 31 21 23 25
1 2
2 3
3 4
4 5
5 6
6 7
7 8
8 9
9 10
3 0 4 1
8 1 5 1
8 0 3 0
8 0 2 0
3 1 6 1
4 1 2 1
6 0 7 1
4 1 8 0
9 0 3 0
6 0 2 1
*/
/*
140
161
140
143
177
140
140
140
157
140
*/
