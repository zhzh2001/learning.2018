#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define pii pair<int,int>
using namespace std;
const int mo=1000000007;
int n,m;
int power(int x,int y){
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	if (n==1) printf("%d\n",power(2,m));
	else if (n==2) printf("%d\n",4ll*power(3,m-1)%mo);
	else if (n==3) printf("%d\n",112ll*power(3,m-3)%mo);
}
