#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
const int N=5005;
vector<int> e[N];
int n,m,ans[N],Ans[N];
int vis[N],T,fl,sum,pre;
int banx,bany,sz[N];
int x[N],y[N];
void check(int x,int fa){
	if (!fl) return;
	sum++; ans[++*ans]=x; vis[x]=T;
	for (int i=0;i<sz[x];i++){
		int to=e[x][i];
		if (to==fa||(to==banx&&x==bany)||(x==banx&&to==bany)) continue;
		if (vis[to]==T){
			fl=0; return;
		}
		check(to,x);
	}
}
bool check_ok(){
	sum=0; fl=1; pre=0; ++T;
	check(1,0);
	return fl&(sum==n);
}
bool cmp(){
	For(i,1,n)
		if (ans[i]!=Ans[i])
			return ans[i]<Ans[i];
	return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	For(i,1,m){
		scanf("%d%d",&x[i],&y[i]);
		e[x[i]].push_back(y[i]);
		e[y[i]].push_back(x[i]);
	}
	For(i,1,n){
		sz[i]=e[i].size();
		sort(e[i].begin(),e[i].end());
	}
	if (m+1==n){
		banx=-1,bany=-1;*ans=0;
		check_ok();
		memcpy(Ans,ans,sizeof(ans));
	}
	else{
		Ans[1]=1e9;
		For(i,1,m){
			banx=x[i]; bany=y[i]; *ans=0;
			if (check_ok()&&cmp())
				memcpy(Ans,ans,sizeof(ans));
		}
	}
	For(i,1,n-1)
		printf("%d ",Ans[i]);
	printf("%d\n",Ans[n]);
	return 0;
}
