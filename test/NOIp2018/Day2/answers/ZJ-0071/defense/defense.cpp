#include<cstdio>
#include<cstring>
#include<algorithm>
#define INF 10000000001
#define N 100010
#define ll long long
#define min(a,b) ((a)<(b)?(a):(b))

ll dp[N][2];
bool vis[N];
int p[N],tot,head[N],n,m,T;
char ch,c;

struct Edge {
	int nxt,to;
} e[N<<1];

void add(int u,int v) {
	e[++tot].nxt=head[u];
	e[tot].to=v;
	head[u]=tot;
}

void dfs(int u,int z,int x) {
	vis[u]=1;
	dp[u][1]=p[u];
	for (int i=head[u]; i; i=e[i].nxt) {
		int v=e[i].to;
		if (vis[v]) continue;
		dfs(v,z,x);
		if (v==z) {
			dp[u][1]+=dp[v][x];
			if (x==0) dp[u][0]=INF;
			else dp[u][0]+=dp[v][1];
		} else {
			dp[u][0]+=dp[v][1];
			dp[u][1]+=min(dp[v][0],dp[v][1]);
		}
	}
}

int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%c%c%d",&n,&m,&ch,&c,&T);
	for (int i=1; i<=n; i++) scanf("%d",&p[i]);
	for (int i=1; i<n; i++) {
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);
		add(v,u);
	}
	for (int i=1; i<=m; i++) {
		int a,x,b,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		memset(vis,0,sizeof(vis));
		memset(dp,0,sizeof(dp));
		dfs(a,b,y);
		if (dp[a][x]>=INF) printf("-1\n"); else printf("%lld\n",dp[a][x]);
	}
	return 0;
}
