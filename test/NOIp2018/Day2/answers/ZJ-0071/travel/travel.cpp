#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 5010

int tot,head[N],u[N],v[N],n,m,id[N],cnt,p[N],x,y,fath[N],c[N];
bool vis[N];

struct Edge {
	int nxt,to;
} e[N<<1],q[N<<1];

bool cmp(int x,int y) {
	return u[x]>u[y]||u[x]==u[y]&&v[x]>v[y];
}

void add(int u,int v) {
	e[++tot].nxt=head[u];
	e[tot].to=v;
	head[u]=tot;
}

void adid(int u,int v) {
	q[++cnt].nxt=p[u];
	q[cnt].to=v;
	p[u]=cnt;
}

void dfs(int u) {
	vis[u]=1;
	for (int i=head[u]; i; i=e[i].nxt) {
		int v=e[i].to;
		if (vis[v]) continue;
		printf(" %d",v);
		dfs(v);
	}
}

void dffs(int u,int fa) {
	vis[u]=1;
	for (int i=p[u]; i; i=q[i].nxt) {
		int v=q[i].to;
		if (v!=fa&&vis[v]) {
			x=v; y=u;
			continue;
		} else if (vis[v]) continue;
		fath[v]=u;
		dffs(v,u);
	}
	
}

int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=m; i++) {
		scanf("%d%d",&u[i],&v[i]);
		if (u[i]<v[i]) std::swap(u[i],v[i]);
		id[i]=i;
	}
	std::sort(id+1,id+m+1,cmp);
	if (m==n-1) {
		for (int i=1; i<=m; i++) add(u[id[i]],v[id[i]]),add(v[id[i]],u[id[i]]);
		printf("1");
		dfs(1);
	} else {
		for (int i=1; i<=m; i++) adid(u[id[i]],v[id[i]]),adid(v[id[i]],u[id[i]]);
		dffs(1,0);
		int sum=0;
		while (x!=y) {
			c[++sum]=x;
			x=fath[x];
		}
		c[++sum]=x;
		if (c[1]<c[sum-1]) {
			int z=sum;
			for (int i=2; i<=sum-1; i++) {
				if (c[i]>c[sum-1]) {
					z=i;
					break;
				}
			}
			x=c[z]; y=c[z-1];
		} else {
			int z=0;
			c[0]=c[sum];
			for (int i=sum-2; i>=1; i--) {
				if (c[i]>c[1]) {
					z=i;
					break;
				}
			}
			x=c[z]; y=c[z+1];
		}
		if (x<y) std::swap(x,y);
		for (int i=1; i<=m; i++) {
			if (!(u[id[i]]==x&&v[id[i]]==y))
				add(u[id[i]],v[id[i]]),add(v[id[i]],u[id[i]]);
		}
		printf("1");
		memset(vis,0,sizeof(vis));
		dfs(1);
	}
	return 0;
}
