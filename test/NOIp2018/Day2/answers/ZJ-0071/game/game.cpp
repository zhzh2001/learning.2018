#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define MOD 1000000007

int f[10][10],n,m;

ll p(ll x,int y) {
	ll z;
	for (z=1; y; x=x*x%MOD,y>>=1) if (y&1) z=z*x%MOD;
	return z;
}

int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) std::swap(n,m);
	if (n<=3&&m<=3) {
		f[1][1]=2;
		f[1][2]=4;
		f[1][3]=8;
		f[2][2]=12;
		f[2][3]=36;
		f[3][3]=112;
		printf("%d\n",f[n][m]);
	} else if (n==1) {
		printf("%lld\n",p((ll)2,m));
	} else if (n==2) {
		printf("%lld\n",p((ll)3,m-1)*4%MOD);
	}
	return 0;
}
