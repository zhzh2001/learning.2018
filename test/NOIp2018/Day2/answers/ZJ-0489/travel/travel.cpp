#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline GG read() {
	GG x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}
inline void write(GG x) {
	if(x<0) putchar('-'), x = -x;
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(GG x) {
	write(x); puts("");
}


const int N = 5011; 
struct edge{
	int to, pre; 
}e[N*2];
struct node{
	int x, y; 
}a[2*N];
int n, m;
int s[3]; 
int head[N], cnt, vis[N];
bool flag = 0;   

inline void add(int x, int y) {
	e[++cnt].to = y; 
	e[cnt].pre = head[x]; 
	head[x] = cnt; 
}

void dfs(int u) {
	write(u); putchar(' '); vis[u] = 1; 
	for(int i=head[u]; i; i=e[i].pre) {
		int v = e[i].to; 
		if(!vis[v]) dfs(v); 
	}
}

inline bool cmp(node a, node b) {
	if(a.x != b.x) return a.x < b.x; 
	return a.y > b.y; 
}

inline void work_1() {
	For(i, m+1, 2*m) a[i].x = a[i-m].y, a[i].y = a[i-m].x; 
	sort(a+1, a+2*m+1, cmp); 
	For(i, 1, 2*m) add(a[i].x, a[i].y); 
	dfs(1); 
	exit(0); 
}

void color(int u) {
	if(vis[u]) return; 
	write(u); putchar(' '); vis[u] = 1; 
	for(int i=head[u]; i; i=e[i].pre) {
		int v = e[i].to; 
		if(!vis[v]) {
			if(v>s[2] && !flag) {
				flag = 1; 
				color(s[2]); 
			} 
			else color(v); 
		} 
	}
	
}

int main() {
	freopen("travel.in", "r", stdin); 
	freopen("travel.out", "w", stdout); 
	n = read(); m = read(); 
	For(i, 1, m) {
		a[i].x = read(); a[i].y = read(); 
	}
	if(m==n-1) work_1(); 
	
	
	
	For(i, m+1, 2*m) a[i].x = a[i-m].y, a[i].y = a[i-m].x; 
	sort(a+1, a+2*m+1, cmp); 
	For(i, 1, 2*m) add(a[i].x, a[i].y);
	int num = 0; 
	for(int i=head[1]; i; i=e[i].pre) {
			s[++num] = e[i].to; 
		}
	if(s[1] > s[2]) swap(s[1], s[2]); 
	color(1); 
}



/*

5 5
1 2
1 3
2 4
5 3
4 5



*/




