#include <bits/stdc++.h>
#define LL long long
#define GG long long 
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline GG read() {
	GG x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}
inline void write(GG x) {
	if(x<0) putchar('-'), x = -x;
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(GG x) {
	write(x); puts("");
}

const int N = 300011; 
const LL INF = 1e15; 
struct edge{
	int to, pre; 
}e[N*2];
int n, Q, cnt; 
char ch[10]; 
int head[N]; 
LL dp[2][N], val[N]; 
LL ans; 

inline void add(int x, int y) {
	e[++cnt].to = y; 
	e[cnt].pre = head[x]; 
	head[x] = cnt; 
}

void work_1() {
	while(Q) {
		int a = read(), x = read(), b = read(), y = read(); 
		if(abs(a-b)==1 && x==0 && y==0) {
			puts("-1"); 
			continue; 
		}
		For(i, 1, n) dp[0][i] = 0, dp[1][i] = 0;    //
		For(i, 1, n) {
			if(i==a) {
				if(x==1) dp[1][i] = min(dp[0][i-1], dp[1][i-1])+val[i], dp[0][i] = INF; 
				else dp[1][i] = INF, dp[0][i] = dp[1][i-1]; 
				continue; 
			}
			if(i==b) {
				if(y==1) dp[1][i] = min(dp[0][i-1], dp[1][i-1])+val[i], dp[0][i] = INF; 
				else dp[1][i] = INF, dp[0][i] = dp[1][i-1]; 
				continue; 
			}
			dp[1][i] = min(dp[0][i-1], dp[1][i-1])+val[i]; 
			dp[0][i] = dp[1][i-1];
		}
		writeln(min(dp[0][n], dp[1][n])); 
	}
}

inline void work_2() {
	while(Q--) {
		int a = read(), x = read(), b = read(), y = read(); 
		if(x==0 && y==0) puts("-1"); 
	}
}

int main() {
	freopen("defense.in", "r", stdin); 
	freopen("defense.out", "w", stdout); 
	n = read(); Q = read(); scanf("%s", ch+1); 
	For(i, 1, n) val[i] = read(); 
	For(i, 1, n-1) {
		int x = read(), y = read(); 
		add(x, y); add(y, x); 
	} 
	if(n<=5000 && ch[1]=='A') work_1(); 
	if(ch[2]==2) work_2(); 
}




/*

10 10 A1
30 10 20 40 70 80 40 100 110 120
1 2
2 3
3 4 
4 5
5 6
6 7
7 8
8 9
9 10
1 0 10 1



*/




