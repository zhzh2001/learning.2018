#include<bits/stdc++.h>
#define LL long long
const int N=10;
const int M=1000010;
const int MOD=1000000007;
using namespace std;

int a[N][N],n,m;
LL ans[M];

void SOLVEA(){
	a[1][1]=2;
	a[1][2]=4;
	a[1][3]=8;
	a[1][4]=16;
	a[1][5]=32;
	a[2][1]=4;
	a[2][2]=12;
	a[2][3]=36;
	a[2][4]=108;
	a[2][5]=324;
	a[3][1]=8;
	a[3][2]=36;
	a[3][3]=112;
	a[3][4]=336;
	a[3][5]=1008;
	a[4][1]=16;
	a[4][2]=108;
	a[4][3]=336;
	a[4][4]=912;
	a[4][5]=2688;
	a[5][1]=32;
	a[5][2]=324;
	a[5][3]=1008;
	a[5][4]=2688;
	a[5][5]=7136;
	printf("%d\n",a[n][m]);
}

LL ksm(LL x,LL y){
	LL res=1;
	while(y){
		if(y%2==1)res=(res*x)%MOD;
		x=(x*x)%MOD;
		y/=2;
	}
	return res;
}

void OvO(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}

int main(){
	OvO();
	scanf("%d%d",&n,&m);
	if(n<=5&&m<=5){
		SOLVEA();
		return 0;
	}
	if(n==1||m==1){
		if(m==1)swap(n,m);
		printf("%lld\n",ksm(2,m));
		return 0;
	}
	if(n==2||m==2){
		ans[1]=4;
		if(m==2)swap(n,m);
		for(int i=2;i<=m;i++)ans[i]=(ans[i-1]*3)%MOD;
		printf("%lld\n",ans[m]);
		return 0;
	}
	if(n==3||m==3){
		ans[1]=8;ans[2]=36;ans[3]=112;
		if(m==3)swap(n,m);
		for(int i=4;i<=m;i++)ans[i]=(ans[i-1]*3)%MOD;
		printf("%lld\n",ans[m]);
		return 0;
	}
	if(n==4||m==4){
		ans[1]=16;ans[2]=108;ans[3]=336;ans[4]=912;
		ans[5]=2688;
		if(m==4)swap(n,m);
		for(int i=6;i<=m;i++)ans[i]=(ans[i-1]*3)%MOD;
		printf("%lld\n",ans[m]);
		return 0;
	}
	return 0;
}
