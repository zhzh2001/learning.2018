#include<bits/stdc++.h>
#define LL long long
const int N=5010;
const int M=10010;
using namespace std;

int n,m,U,V,flag,Pos;
int edge[M],lst[N],nxt[M],t=0;
int son[N][N],Ans[N],vis[N],dad[N],Flag[N],Las;

void ADD(int x,int y){
	edge[++t]=y;nxt[t]=lst[x];lst[x]=t;
}

void READ(){
	int u,v;
	t=0;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		ADD(u,v);ADD(v,u);
	}
}

void SEARCH(int x,int fa){
	Ans[++Pos]=x;
	int cnt=0;
	for(int r=lst[x];r;r=nxt[r]){
		if(edge[r]==fa)continue;
		son[x][++cnt]=edge[r];
	}
	sort(son[x]+1,son[x]+cnt+1);
	for(int i=1;i<=cnt;i++){
		SEARCH(son[x][i],x);
	}
}

void SOLVEA(){
	Pos=0;
	SEARCH(1,0);
	for(int i=1;i<=Pos;i++)printf("%d ",Ans[i]);
}

void BUILD(int x,int fa){
	if(flag)return;
	vis[x]=1;dad[x]=fa;
	for(int r=lst[x];r;r=nxt[r]){
		if(edge[r]==fa)continue;
		if(vis[edge[r]]&&U!=-1){
			U=edge[r];
			V=x;
			flag=1;
			return;
		}
		BUILD(edge[r],x);
		if(flag)return;
	}
}

void SEARCHB(int x,int fa){
	if(Flag[x]&&dad[x]==U&&V<x&&flag==0){
		Las=x;
		flag=1;
		SEARCHB(V,fa);
	}
	if(vis[x])return;
	vis[x]=1;
	Ans[++Pos]=x;
	int cnt=0;
	for(int r=lst[x];r;r=nxt[r]){
		if(edge[r]==fa)continue;
		if(vis[edge[r]])continue;	
		if(x==U&&edge[r]==V)continue;
		son[x][++cnt]=edge[r];
	}
	sort(son[x]+1,son[x]+cnt+1);
	if(Flag[x]==1&&flag==1){
		if(son[x][cnt]==dad[x]&&dad[x]>Las)cnt--;
	}
	for(int i=1;i<=cnt;i++){
		if(x==U&&son[x][i]>V){
			flag=1;
			Las=son[x][i];
			SEARCHB(V,x);
		}
		SEARCHB(son[x][i],x);
	}
}

void SOLVEB(){
	Pos=flag=0;
	BUILD(1,0);
	int pos=V;
	while(pos!=U){
		Flag[pos]=1;
		pos=dad[pos];
	}
	flag=0;
	for(int i=1;i<=n;i++)vis[i]=0;
	SEARCHB(1,0);
	for(int i=1;i<=Pos;i++)printf("%d ",Ans[i]);
}

void OvO(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
}

int main(){
	OvO();
	READ();
	if(m==n-1){
		SOLVEA();
		return 0;
	}
	if(m==n){
		SOLVEB();
		return 0;
	}
}
