#include<bits/stdc++.h>
#define LL long long
const int N=100010;
const int M=200010;
const LL INF=1e14;
using namespace std;

string s;
int n,m,can[N][2];
LL ans,f[N][2],a[N];
int edge[M],lst[N],nxt[M],t=0;

void ADD(int x,int y){
	edge[++t]=y;nxt[t]=lst[x];lst[x]=t;
}

void READ(){
	int u,v;
	cin>>n>>m>>s;
	for(int i=1;i<=n;i++)scanf("%lld",&a[i]);
	for(int i=1;i<=n-1;i++){
		scanf("%d%d",&u,&v);
		ADD(u,v);ADD(v,u);
	}
}

void CHK(int x,int fa){
	int cnt=0;
	LL num;
	f[x][1]=a[x];f[x][0]=0;
	for(int r=lst[x];r;r=nxt[r]){
		if(edge[r]==fa)continue;
		cnt++;
		CHK(edge[r],x);
		if(can[edge[r]][1]==0)can[x][0]=0;
		if(can[edge[r]][1])f[x][0]+=f[edge[r]][1];
		num=INF;
		if(can[edge[r]][0])num=min(num,f[edge[r]][0]);
		if(can[edge[r]][1])num=min(num,f[edge[r]][1]);
		f[x][1]+=num;
	}
	if(cnt==0){
		if(can[x][1])f[x][1]=a[x];
		if(can[x][0])f[x][0]=0;
		return;
	}
}

void SOLVE(){
	int A,pdA,B,pdB;
	scanf("%d%d%d%d",&A,&pdA,&B,&pdB);
	can[A][pdA^1]=0;can[B][pdB^1]=0;
	CHK(1,0);
	for(int i=1;i<=n;i++){
		if(can[i][0]==0&&can[i][1]==0){
			printf("-1\n");
			return;
		}
	}
	ans=INF;
	if(can[1][1])ans=f[1][1];
	if(can[1][0])ans=min(ans,f[1][0]);
	printf("%lld\n",ans);
}

void OvO(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
}

int main(){
	OvO();
	READ();
	while(m--){
		for(int i=1;i<=n;i++){
			for(int j=0;j<=1;j++){
				can[i][j]=1;
				f[i][j]=0;
			}
		}
		SOLVE();
	}
	return 0;
}
