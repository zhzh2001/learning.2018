#include<bits/stdc++.h>
#define ll long long
#define C getchar()
#define Violet inline
#define N 5050
using namespace std;
Violet int read()
{
	int x=0,l=1;char c=C;
	for (;c<'0'||c>'9';c=C) if (c=='-') l=-1;
	for (;c>='0'&&c<='9';c=C) x=x*10+c-48;
	return x*l;
}

struct Vio {int next,y;}e[N<<1];

int n,m;
int b[N];
int p[N];
int v[N];
int lin[N],L;

priority_queue<int>q,s[N];

Violet void fnp(int x,int y) { e[++L]=(Vio){lin[x],y},lin[x]=L; }

Violet void dfs(int x,int par)
{
	for (int i=lin[x],y;i,y=e[i].y;i=e[i].next)
		if (y!=par)
			{
				s[x].push(-y);
				dfs(y,x);
			}
}

Violet void prt(int x)
{
	printf("%d ",x);
	while (!s[x].empty())
		{
			int y=-s[x].top();
			s[x].pop();
			prt(y);
		}
}

Violet void work()
{
	q.push(-1);
	b[1]=1;
	while (!q.empty())
		{
			int x=-q.top(),y;
			q.pop();
			s[p[x]].push(-x);
			v[x]=1;
			for (int i=lin[x];i,y=e[i].y;i=e[i].next)
				if (!v[y])
					{
						p[y]=x;
						if (!b[y]) q.push(-y),b[y]=1;
					}
		}
}

int main()
{
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=m;i++)
		{
			int x=read(),y=read();
			fnp(x,y);fnp(y,x);
		}
	if (m==n-1)
		{
			dfs(1,0);
			prt(1);
		}
	else
		{
			work();
			prt(1);
		}
	return 0;
}
