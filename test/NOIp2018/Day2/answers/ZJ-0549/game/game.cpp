#include<bits/stdc++.h>
#define ll long long
#define C getchar()
#define Violet inline
#define mo 1000000007
using namespace std;
Violet int read()
{
	int x=0,l=1;char c=C;
	for (;c<'0'||c>'9';c=C) if (c=='-') l=-1;
	for (;c>='0'&&c<='9';c=C) x=x*10+c-48;
	return x*l;
}

int n,m,t;
int p[1000005];
int a[1005][1005];
ll ans;

Violet ll poww(ll a,ll b)
{
	ll sum=1;
	while (b)
		{
			if (b&1) sum=sum*a%mo;
			b>>=1;
			a=a*a%mo;
		}
	return sum;
}

Violet void dfsp(int x,int y,int sum)
{
	if (x>n||y>m) return;
	if (x==n&&y==m) { p[++t]=sum;return; }
	dfsp(x+1,y,sum*10+a[x+1][y]);
	dfsp(x,y+1,sum*10+a[x][y+1]);
}

Violet void opp()
{
	t=0;
	dfsp(1,1,a[1][1]);
	ans++;
	for (int i=1;i<t;i++)
		if (p[i]<p[i+1])
			{ ans--;break; }
	if (ans>=mo) ans-=mo;
}

Violet void dfs(int x,int y)
{
	if (x>n) { opp();return;}
	a[x][y]=0;
	dfs(x+(y==m),y%m+1);
	a[x][y]=1;
	dfs(x+(y==m),y%m+1);
}

Violet void work2()
{
	ans=0;
	dfs(1,1);
	printf("%lld\n",ans);
}

int main()
{
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	n=read();m=read();
	if (n==2) printf("%lld\n",poww(3ll,1ll*m-1)*4%mo); else
	if (m==1||n==1) printf("%lld\n",poww(2ll,1ll*max(n,m))); else
	if (m==2) printf("%lld\n",poww(3ll,1ll*n-1)*4%mo); else
	if (n==3&&m>=3) printf("%lld\n",poww(3ll,1ll*m-3)*112%mo); else
	if (n==4&&m>=5) printf("%lld\n",poww(3ll,1ll*m-5)*2688%mo); else
		work2();
	return 0;
}
