#include<bits/stdc++.h>
#define maxn 100005
using namespace std;
inline int read(){
	int x=0,c;while(!isdigit(c=getchar()));
	while(x=x*10+c-'0',isdigit(c=getchar()));
	return x;
}
char ty[10];
int n,m,cnt,last[maxn];
struct edge{
	int to,next;
}e[maxn<<1];
inline void insert(int x,int y){
	e[++cnt].to=y;e[cnt].next=last[x];last[x]=cnt;
}
int p[maxn],f[maxn][2],F[maxn];
void dfs(int x,int fa){
	f[x][1]=p[x];
	for(int j=last[x];j;j=e[j].next){
		int y=e[j].to;if(y==fa)continue;
		F[y]=x;dfs(y,x);f[x][0]+=f[y][1];f[x][1]+=min(f[y][1],f[y][0]);
	}
}
int vis[maxn];
inline void print(int x){
	if(x>=1e9)printf("-1\n");else printf("%d\n",x);
}
namespace work{
	#define INF 1e9
	void Dfs(int x,int fa){
		f[x][1]=p[x];f[x][0]=0;
		for(int j=last[x];j;j=e[j].next){
			int y=e[j].to;if(y==fa)continue;
			Dfs(y,x);if(vis[y]==0)f[x][0]=INF;else f[x][0]+=f[y][1];
			if(vis[y]!=-1)f[x][1]+=f[y][vis[y]];else f[x][1]+=min(f[y][0],f[y][1]);
		}
	}
	void Main(){
		memset(vis,255,sizeof(vis));
		while(m--){
			int a=read(),x=read(),b=read(),y=read();
			vis[a]=x;vis[b]=y;Dfs(1,0);
			if(vis[1]!=-1)print(f[1][vis[1]]);else print(min(f[1][0],f[1][1]));
			vis[a]=-1;vis[b]=-1;
		}
	}	
}
namespace work2{
	int g[maxn][2];
	void Main(){
		memset(vis,255,sizeof(vis));
		while(m--){
			int a=read(),x=read(),b=read(),y=read(),A=a,B=b;vis[a]=x;vis[b]=y;
			g[a][1]=f[a][1];g[a][0]=f[a][0];g[b][1]=f[b][1];g[b][0]=f[b][0];g[a][x^1]=1e9;g[b][y^1]=1e9;
			while(a){
				int X=F[a];
				g[X][0]=f[X][0]-f[a][1];g[X][1]=f[X][1]-min(f[a][1],f[a][0]);
				g[X][0]+=g[a][1];g[X][1]+=min(g[a][0],g[a][1]);a=F[a];
			}
			while(b){
				int X=F[b];
				g[X][0]=f[X][0]-f[b][1];g[X][1]=f[X][1]-min(f[b][1],f[b][0]);
				g[X][0]+=g[b][1];g[X][1]+=min(g[b][0],g[b][1]);b=F[b];
			}
			if(vis[1]!=-1)print(g[1][vis[1]]);else print(min(g[1][0],g[1][1]));
			vis[A]=-1;vis[B]=-1;
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",ty+1);
	for(int i=1;i<=n;i++)p[i]=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		insert(x,y);insert(y,x);
	}
	dfs(1,0);
	if(n<=2000&&m<=2000)work::Main();
	else work2::Main();
	return 0;
}
