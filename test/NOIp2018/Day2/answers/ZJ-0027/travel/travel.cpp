#include<bits/stdc++.h>
#define maxn 5005
using namespace std;
inline int read(){
	int x=0,c;while(!isdigit(c=getchar()));
	while(x=x*10+c-'0',isdigit(c=getchar()));
	return x;
}
struct edge{
	int to,next;
}e[maxn<<1];
int last[maxn],n,m,cnt=1;
inline void insert(int x,int y){
	e[++cnt].to=y;e[cnt].next=last[x];last[x]=cnt;
}
int ans[maxn],tot,vis[maxn],tmp,s[maxn];
int Do,F,b[maxn];
vector<int>E[maxn];
void dfs(int x,int fa){
	tmp=0;ans[++tot]=x;
	for(int j=last[x];j;j=e[j].next){
		int y=e[j].to;if(y==fa||vis[j]||b[y])continue;
		E[x].push_back(y);b[y]=1;
	}
	sort(E[x].begin(),E[x].end());
	for(int i=0;i<E[x].size();i++)dfs(E[x][i],x);
}
void work(){
	dfs(1,0);
	for(int i=1;i<=tot;i++)printf("%d ",ans[i]);
}
int fa[maxn],dep[maxn];
void Dfs(int x){
	for(int j=last[x];j;j=e[j].next){
		int y=e[j].to;if(y==fa[x])continue;
		if(vis[y]){tmp=j;break;}else vis[y]=1,dep[y]=dep[x]+1,fa[y]=x,Dfs(y);
		if(tmp)break;
	}
}
int Lca,Min;
void dfs(int x){
	for(int j=last[x];j;j=e[j].next)if(j!=tmp&&(j^1)!=tmp){
		int y=e[j].to;if(y==fa[x]||!vis[y])continue;
		if(y>E[Lca][F+1]){Do=j;break;}else dfs(y);
	}
}
int res[maxn];
inline bool check(){
	for(int i=1;i<=n;i++)if(res[i]!=ans[i])return res[i]>ans[i];	
}
void work2(){
//	vis[1]=1;Dfs(1);int x=e[tmp].to,y=e[tmp^1].to;memset(vis,0,sizeof(vis));vis[x]=vis[y]=1;
	for(int i=1;i<=n;i++)res[i]=n;
	for(int i=2;i<=cnt;i+=2){
		vis[i]=vis[i^1]=1;
		for(int i=1;i<=n;i++)E[i].clear();
		memset(b,0,sizeof(b));
		tot=0;dfs(1,0);
		if(tot==n&&check())for(int i=1;i<=n;i++)res[i]=ans[i];
		vis[i]=vis[i^1]=0;
	}
	/*while(x!=y){
		if(dep[x]<dep[y])swap(x,y);
		x=fa[x];vis[x]=1;
	}Lca=x;x=e[tmp].to;y=e[tmp^1].to;
	while(fa[x]!=Lca)x=fa[x];while(fa[y]!=Lca)y=fa[y];
	Min=min(x,y);
	for(int j=last[Lca];j;j=e[j].next){
		int y=e[j].to;if(y==fa[Lca])continue;
		E[Lca].push_back(y);
	}
	sort(E[Lca].begin(),E[Lca].end());
	for(int i=0;i<E[Lca].size();i++)if(E[x][i]==Min)F=i;
	if(F!=E[Lca].size()-1)dfs(Min);E[Lca].clear();
	dfs(1,0);*/
	for(int i=1;i<=n;i++)printf("%d ",res[i]);
}
int main(){
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		insert(x,y);insert(y,x);
	}
	if(m==n-1)work();
	else work2();
	return 0;
}
