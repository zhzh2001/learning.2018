#include<bits/stdc++.h>
#define maxn 1000005
#define Mod 1000000007
#define ll long long
using namespace std;
int n,m,a[10][10],ans;
inline int read(){
	int x=0,c;while(!isdigit(c=getchar()));
	while(x=x*10+c-'0',isdigit(c=getchar()));
	return x;
}
inline bool check(int x){
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(x&(1<<((i-1)*m+j-2)))a[i][j]=1;else a[i][j]=0;
	for(int i=1;i<n;i++)
		for(int j=1;j<m;j++)if(a[i+1][j]<a[i][j+1])return 0;
	return 1;
}
void print(){
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++)printf("%d ",a[i][j]);
		printf("\n");
	}printf("%d\n",ans);
}
int ksm(int x,int y){
	int res=1;
	while(y){
		if(y&1)res=1ll*res*x%Mod;
		x=1ll*x*x%Mod;y>>=1;
	}return res;
}
int main(){
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	n=read(),m=read();
	if(n==1){printf("%d\n",ksm(2,m));return 0;}
	if(n==2){printf("%d\n",1ll*ksm(3,m-1)*4%Mod);return 0;}
	if(n==3&&m==3){printf("112\n");return 0;}
	if(m==2){printf("%d\n",1ll*ksm(3,n-1)*4%Mod);return 0;}
	if(m==1){printf("%d\n",1ll*ksm(2,n));return 0;}
//	for(int i=0;i<(1<<(n*m));i++)
//	if(check(i))print(),ans++/;
//	printf("%d\n",ans);
	return 0;
}
