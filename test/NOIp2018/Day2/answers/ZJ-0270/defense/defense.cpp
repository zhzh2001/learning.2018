#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=2005;
const int inf=0x3fffffff;
int n,m,x,y,X,Y,a[N],head[N],cnt,f1[N],f2[N];
bool vv[N][N];
struct edge{int to,nt;}e[N*2];
void add(int u,int v){
	e[++cnt].to=v;
	e[cnt].nt=head[u];
	head[u]=cnt;
}
void dfs(int u,int fa){
	f1[u]=a[u];f2[u]=0;bool f=0;
	for(int i=head[u];i;i=e[i].nt)
		if(e[i].to!=fa){
			dfs(e[i].to,u);
			f1[u]+=min(f1[e[i].to],f2[e[i].to]);
			f2[u]+=f1[e[i].to];
		}
	if(X==u){
		if(x==1)f2[u]=inf;
		else f1[u]=inf;
	}else if(Y==u){
		if(y==1)f2[u]=inf;
		else f1[u]=inf;
	}
}
char str[100];
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",str+1);
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++){
		x=read();y=read();
		add(x,y);add(y,x);
		vv[x][y]=vv[y][x]=1;
	}
	for(int i=1;i<=m;i++){
		X=read();x=read();
		Y=read();y=read();
		if(x==0&&y==0&&vv[X][Y]){
			puts("-1");
			continue;
		}
		dfs(1,0);
		cout<<min(f1[1],f2[1])<<'\n';
	}
	return 0;
}
