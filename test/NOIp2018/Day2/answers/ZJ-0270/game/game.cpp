#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int P=1e9+7;
int n,m,ans=1;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();m=read();
	if(m<n)swap(n,m);
	if(n==3&&m==3)ans=112;
	else if(n<3) for(int i=1;i<=m;i++)
		if(i<n)ans=ans*(i+1)%P*(i+1)%P;
			else ans=ans*(n+1)%P;
	cout<<ans;
	return 0;
}
