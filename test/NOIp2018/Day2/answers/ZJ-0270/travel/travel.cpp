#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=5005;
struct edge{int to,nt;}e[N*2];
int n,m,cnt,head[N],x,y,tot,rt,a[N];
bool ol[N],vis[N],fff;
void add(int u,int v){
	e[++cnt].to=v;
	e[cnt].nt=head[u];
	head[u]=cnt;
}
void dfs3(int u){
	if(vis[u])return;
	vis[u]=1;
	cout<<u<<' ';
	vector<int> v;
	for(int i=head[u];i;i=e[i].nt)
		if(!vis[e[i].to])v.push_back(e[i].to);
	sort(v.begin(),v.end());
	for(int i=0;i<v.size();i++)dfs3(v[i]);
}
void dfs2(int u){
	if(vis[u])return;
	vis[u]=1;
	cout<<u<<' ';
	vector<int> v;
	for(int i=head[u];i;i=e[i].nt)
		if(!vis[e[i].to])v.push_back(e[i].to);
	sort(v.begin(),v.end());
	for(int i=0;i<v.size();i++)
		if(ol[v[i]]){
			for(int j=v.size()-1;j>i;j--)a[++tot]=v[j];
			while(tot&&vis[a[tot]])tot--;
			if(a[tot]<v[i])for(;tot;tot--)dfs3(a[tot]);
			else dfs2(v[i]);
			break;
		}else dfs3(v[i]);
}
void dfs1(int u){
	if(vis[u])return;
	vis[u]=1;
	cout<<u<<' ';
	vector<int> v;
	for(int i=head[u];i;i=e[i].nt)
		if(!vis[e[i].to])v.push_back(e[i].to);
	sort(v.begin(),v.end());
	for(int i=0;i<v.size();i++)
		if(ol[v[i]])dfs2(v[i]);
		else dfs1(v[i]);
}
void init(int u,int fa){
	if(fff)return;
	if(ol[u])fff=1,rt=u;
	ol[u]=1;
	for(int i=head[u];i;i=e[i].nt)
		if(e[i].to!=fa)init(e[i].to,u);
	if(!fff)ol[u]=0;
}
void init2(int u,int fa){
	if(!ol[u])return;
	if(u==rt)return;
	ol[u]=0;
	for(int i=head[u];i;i=e[i].nt)
		if(e[i].to!=fa)init2(e[i].to,u);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		x=read();y=read();
		add(x,y);add(y,x);
	}
	if(m==n){
		init(1,0);
		if(rt!=1)init2(1,0);
		if(ol[1])dfs2(1);
			else dfs1(1);
		for(;tot;tot--)dfs1(a[tot]);
	}
	else dfs1(1);
	return 0;
}
