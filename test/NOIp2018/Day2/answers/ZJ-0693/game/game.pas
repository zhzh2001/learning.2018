const mo=1000000007;
var n,m,i,j,k:longint;
    ans:int64;
    f:array[0..100001,0..128] of longint;
function check(x,y:longint):boolean;
var i:longint;
begin
  for i:=1 to n-1 do
    if ((x>>i) and 1)<((y>>i) and 1) then
      exit(false);
  exit(true);
end;
begin
assign(input,'game.in');reset(input);
assign(output,'game.out');rewrite(output);
  read(n,m);
  if (n=3) and (m=3) then
    begin
      writeln(112);
      close(inpuT);
      close(output);
      halt;
    end;

  for i:=0 to (1<<n)-1 do
    f[1,i]:=1;
  for i:=2 to m do
    for j:=0 to (1<<n)-1 do
      for k:=0 to (1<<n)-1 do
        if check(j,k) then
          f[i,j]:=(f[i,j]+f[i-1,k]) mod mo;
  for i:=0 to (1<<n)-1 do
    ans:=(ans+f[m,i]) mod mo;
  writeln(ans);
close(input);
close(output);
end.
