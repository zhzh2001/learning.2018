type node=array[0..5001] of longint;
var tot,n,m,i,j,x,y,t,w,v:longint;
    q,ans,du,lst:node;
    flag:boolean;
    f:array[0..5001,0..5001] of longint;
    vis:array[0..100001] of boolean;
procedure dfs(x:longint);
var i:longint;
begin
  inc(tot);
  ans[tot]:=x;
  for i:=1 to f[x,0] do
    if not vis[f[x,i]] then
      begin
        vis[f[x,i]]:=true;
        dfs(f[x,i]);
      end;
end;
function min(x,y:node):node;
var i:longint;
begin
  for i:=1 to n do
    if x[i]<y[i] then
      exit(x)
    else
    if x[i]>y[i] then
      exit(y);
  exit(x);
end;
procedure sort(l,r:longint);
var i,j,x,y:longint;
begin
  i:=l;j:=r;x:=f[t,(l+r) div 2];
  repeat
    while f[t,i]<x do
      inc(i);
    while x<f[t,j] do
      dec(j);
    if i<=j then
      begin
        y:=f[t,i];f[t,i]:=f[t,j];f[t,j]:=y;
        inc(i);dec(j);
      end;
  until i>j;
  if l<j then
    sort(l,j);
  if i<r then
    sort(i,r);
end;
begin
assign(input,'travel.in');reset(input);
assign(output,'travel.out');rewrite(output);
  read(n,m);
  for i:=1 to m do
    begin
      read(x,y);
      inc(f[x,0]);inc(f[y,0]);
      f[x,f[x,0]]:=y;
      f[y,f[y,0]]:=x;
      inc(du[x]);inc(du[y]);
    end;
  for t:=1 to n do
    sort(1,f[t,0]);
  if m=n-1 then
    begin
      vis[1]:=true;
      dfs(1);
      for i:=1 to n do
        write(ans[i],' ');
    end
  else
    begin
      for i:=1 to n do
        if f[i,0]<>2 then
          flag:=true;
      if not flag then
        begin
          vis[1]:=true;
          dfs(1);
          for i:=1 to n do
            write(ans[i],' ');
        end
      else
      begin
      t:=1;w:=1;
      vis[1]:=true;
      q[1]:=1;
      while t<=w do
        begin
          x:=q[t];
          inc(t);
          for i:=1 to f[x,0] do
            begin
              v:=f[x,i];
              if not vis[v] then
                begin
                  vis[v]:=true;
                  inc(w);
                  q[w]:=v;
                end;
            end;
        end;
      for i:=1 to n do
        write(q[i],' ');
      end;
    end;
  writeln;
close(input);
close(output);
end.
