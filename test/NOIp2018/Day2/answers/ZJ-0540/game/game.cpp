#include<bits/stdc++.h>
using namespace std;
#define mod 1000000007
int read()
{
	int x=0,f=1;
	char c=getchar();
	while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();}
	while (c>='0'&&c<='9') {x=x*10+c-'0'; c=getchar();}
	return x*f;
}
int n,m;
int i,j,k,l;
int p;
unsigned int f[1000010][260];
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();
	m=read();
//	cout<<n<<endl;
	p=(1<<n)-1;
//	cout<<p<<endl;
//	memset(f,0,sizeof(f));
	for (i=0; i<=p; i++) f[1][i]=1;
	int a,b,c,d;
	bool flag;
	for (i=2; i<=m; i++)
		for (j=0; j<=p; j++)
			for (k=0; k<=p; k++) 
			{
				flag=true;
				for (l=1; l<=n-1; l++)
				{
					c=1<<l;
					d=1<<(l-1);
					a=(j%c)/d;
					c=1<<(l+1);
					d=1<<l;
					b=(k%c)/d;
					if (a>b)
					{
						flag=false;
						break;
					}
				}
				if (flag) (f[i][j]+=f[i-1][k])%=mod;
			}
	long long ans=0;
	for (i=0; i<=p; i++) (ans+=f[m][i])%=mod;
	cout<<ans<<endl;
	return 0;
}
