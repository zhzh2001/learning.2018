#include<bits/stdc++.h>
using namespace std;
#define mod 1000000007
int read()
{
	int x=0,f=1;
	char c=getchar();
	while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();}
	while (c>='0'&&c<='9') {x=x*10+c-'0'; c=getchar();}
	return x*f;
}
int n,m;
string s;
int f[100010][2];
int ans[100010][2];
int fa[100010];
vector <int> child[100010];
int i,j,k;
int tot=0;
int p[100010];
int head[100010],to[200010],nxt[200010];
int dep[100010];
void add(int x,int y)
{
	tot++;
	nxt[tot]=head[x];
	to[tot]=y;
	head[x]=tot;
}
int dfs(int u,int ff)
{
	int i;
	f[u][1]+=p[u];
	for (i=head[u]; i; i=nxt[i])
	{
		int v=to[i];
		if (v==ff) continue;
		dep[v]=dep[u]+1;
		dfs(v,u);
		f[u][0]+=f[v][1];
		f[u][1]+=min(f[v][1],f[v][0]);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m>>s;
//	cout<<s<<endl;
	int u,v;
	for (i=1; i<=n; i++) p[i]=read();
	for (i=1; i<n; i++)
	{
		u=read();
		v=read();
		add(u,v);
		add(v,u);
	}
	dfs(1,-1);
	int x,y,a,b;
	while (m--)
	{
		x=read();
		a=read();
		y=read();
		b=read();
		if (dep[y]<dep[x]) swap(x,y),swap(a,b);
		if (fa[y]==x&&(a==0&&b==0))
		{
			cout<<-1<<endl;
			continue;
		}
		long long ans=0;
		ans=min(f[1][0]+(dep[x]-dep[1]%2==0?((a==0)?0:f[x][1]-f[x][0]):((a==0)?f[x][0]-f[x][1]:0))+(dep[y]-dep[1]%2==0?((b==0)?0:f[y][1]-f[y][0]):((b==0)?f[y][0]-f[y][1]:0)),
		f[1][1]+(dep[x]-dep[1]%2==0?((a==1)?0:f[x][0]-f[x][1]):((a==1)?f[x][1]-f[x][0]:0))+(dep[y]-dep[1]%2==0?((b==1)?0:f[y][0]-f[y][1]):((b==1)?f[y][1]-f[y][0]:0)));
		cout<<ans<<endl;
	}
}
