#include<bits/stdc++.h>
using namespace std;
int read()
{
	int x=0,f=1;
	char c=getchar();
	while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();}
	while (c>='0'&&c<='9') {x=x*10+c-'0'; c=getchar();}
	return x*f;
}
struct edge{
	int id;
	bool operator < (const edge &a) const{
		return id>a.id;
	}
};
priority_queue <edge> q[5010];
int n,m;
int i,j,k;
int u,v;
int tot=0;
int head[5010],to[10010],nxt[10010];
int ins[5010];
int size=0;
int isr[5010];
int id=0;
int w[5010];
bool vis[5010];
int sum=0,cnt;
int ans[5010];
void add(int x,int y)
{
	tot++;
	nxt[tot]=head[x];
	to[tot]=y;
	head[x]=tot;
}
void dfs1(int u)
{
	sum++;
	ans[sum]=u;
	vis[u]=true;
	if (sum==n) return;
	while (!q[u].empty())
	{
		edge v=q[u].top();
		q[u].pop();
		if (vis[v.id]) continue;
		dfs1(v.id);
	}
}
stack <int> s;
vector <edge> g;
void get_ring(int u,int fa)
{
	int i;
	vis[u]=true;
	for (i=head[u]; i; i=nxt[i])
	{
		int v=to[i];
		if (v==fa) continue;
		if (ins[v])
		{
//			cout<<1<<endl;
			int now;
			do{
				now=s.top();
				s.pop();
//				g.push_back(now);
				isr[now]=1;
				ins[now]=0;
				size++;
			}while (now!=v);
//			cout<<1<<endl;
		}
		if (vis[v]) continue;
		s.push(v);
		ins[v]=1;
		get_ring(v,u);
		s.pop();
		ins[v]=0;
	}
}
void special(int u)
{
	int s1=0,s2=0;
	vis[u]=true;
//	cout<<u<<endl;
	sum++;
	ans[sum]=u;
	while (!q[u].empty())
	{
		edge v=q[u].top();
		q[u].pop();
		g.push_back(v);
		if (isr[v.id]&&s1==0) s1=v.id;
		else if (isr[v.id]&&s1!=0) s2=v.id;
	}
//	cout<<s1<<" "<<s2<<endl;
	for (int i=0; i<g.size(); i++) q[u].push(g[i]);
	bool flag=false;
	while (!q[u].empty())
	{
		edge v=q[u].top();
		q[u].pop();
		if (vis[v.id]) continue;
//		cout<<v.id<<endl;
		if (!isr[v.id])
		{
//			cout<<"*******"<<endl;
			dfs1(v.id);
		}
		else if (v.id==s1)
		{
			vis[v.id]=1;
			sum++;
			ans[sum]=s1;
			while (!q[v.id].empty())
			{
				edge vv=q[v.id].top();
//				cout<<vv.id<<endl;
				q[v.id].pop();
				if (vis[vv.id]) continue;
				if (q[v.id].empty()&&vv.id>s2&&isr[vv.id])
				{
					flag=true;
//					cout<<sum<<endl;
					dfs1(s2);
				}
				else
				{
					dfs1(vv.id);
				}
			}
		}
		else if (v.id==s2)
		{
			if (flag) continue;
			else dfs1(s2);
		}
	}
}
void dfs2(int u)
{
	sum++;
	ans[sum]=u;
	vis[u]=true;
	if (sum==n) return;
	while (!q[u].empty())
	{
		edge v=q[u].top();
		q[u].pop();
		if (vis[v.id]) continue;
		if (isr[v.id]) special(v.id);
		else dfs2(v.id);
	}
//	cout<<"*********"<<endl;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();
	m=read();
	if (m==n-1)
	{
		memset(vis,false,sizeof(vis));
		for (i=1; i<=m; i++)
		{
			u=read();
			v=read();
			edge now;
			now.id=v;
			q[u].push(now);
			now.id=u;
			q[v].push(now);
//			add(u,v);
//			add(v,u);
		}
		dfs1(1);
//		cout<<1<<endl;
		for (i=1; i<n; i++) printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
		return 0;
	}
//	cout<<1<<endl;
	if (m==n)
	{
		memset(vis,false,sizeof(vis));
		for (i=1; i<=m; i++)
		{
			u=read();
			v=read();
			edge now;
			now.id=v;
			q[u].push(now);
			now.id=u;
			q[v].push(now);
			add(u,v);
			add(v,u);
		}
//		cout<<1<<endl;
//		cnt=0;
		sum=0;
		get_ring(1,-1);
//		cout<<1<<endl;
		memset(vis,false,sizeof(vis));
		dfs2(1);
//		for (i=1; i<=n; i++) if (isr[i]) cout<<i<<endl;
//		sum=0;
//		dfs2(1);
		for (i=1; i<n; i++) printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
		return 0;
	}
}
