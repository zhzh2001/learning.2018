#include <bits/stdc++.h>
#define range(i,c,o) for(register int i=(c);i<(o);++i)
using namespace std;

// QIO BEGIN
inline unsigned getu()
{
	char c; unsigned r=0;
	for(;!isdigit(c)&&c!=EOF;c=getchar());
	for(;isdigit(c);c=getchar()) r=(r<<3)+(r<<1)+c-'0';
	return r;
}
// QIO END

static int n,m;
vector<int> edg[5005];

namespace tree
{
	void DFS(const int&x,const int&f)
	{
		if(x!=1) putchar(' ');
		printf("%d",x);
		range(i,0,edg[x].size())
		{
			int y=edg[x][i]; if(y!=f) DFS(y,x);
		}
	}
	
	inline bool judge() {return n==m+1;}
	int solve() {return DFS(1,0),putchar('\n'),0;}
}

namespace loop
{
	bool inl[5005];
	int top,cnt,pre[5005],nxt[5005],stk[5005],lop[5005],ans[5005];
	void tarjan(const int&x,const int&f)
	{
		pre[stk[top++]=x]=f;
		range(i,0,edg[x].size())
		{
			int y=edg[x][i];
			if(y==f) continue;
			if(!pre[y]) tarjan(y,x);
			else if(!cnt)
			{
				int i=top-1; for(;stk[i]!=y;--i);
				range(j,i,top) inl[lop[cnt++]=stk[j]]=1;
			}
		}
		--top;
	}
	void DFS(const int&x,const int&f)
	{
		stk[top++]=x;
		range(i,0,edg[x].size())
		{
			int y=edg[x][i]; if(y!=f&&(!inl[x]||!inl[y])) DFS(y,x);
		}
	}
	inline void update()
	{
		range(i,0,top)
		{
			if(stk[i]<ans[i]) return (void)memcpy(ans,stk,sizeof ans);
			if(stk[i]>ans[i]) return;
		}
	}
	
	int solve()
	{
		memset(pre,0,sizeof pre),memset(nxt,0,sizeof nxt);
		memset(inl,0,sizeof inl),top=cnt=0,tarjan(1,0);
		for(int i=lop[0];pre[i];i=pre[i]) nxt[pre[i]]=i;
		putchar('1');
		for(int i=nxt[1];!inl[i];i=nxt[i]) printf(" %d",i);
		memset(inl,0,sizeof inl),ans[0]=n;
		range(i,0,cnt-1)
		{
			inl[lop[i]]=inl[lop[i+1]]=1;
			top=0,DFS(lop[0],pre[lop[0]]),update();
			inl[lop[i]]=inl[lop[i+1]]=0;
		}
		inl[lop[0]]=inl[lop[cnt-1]]=1;
		top=0,DFS(lop[0],pre[lop[0]]),update();
		inl[lop[0]]=inl[lop[cnt-1]]=0;
		range(i,0,top) printf(" %d",ans[i]);
		return putchar('\n'),0;
	}
}

int main()
{
	freopen("travel.in" ,"r",stdin );
	freopen("travel.out","w",stdout);
	n=getu(),m=getu();
	range(i,0,m)
	{
		int u=getu(),v=getu();
		edg[u].push_back(v);
		edg[v].push_back(u);
	}
	range(i,1,n+1) sort(edg[i].begin(),edg[i].end());
	if(tree::judge()) return tree::solve();
	return loop::solve();
}
