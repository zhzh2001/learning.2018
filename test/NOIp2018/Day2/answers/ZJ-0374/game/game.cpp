#include <bits/stdc++.h>
#define range(i,c,o) for(register int i=(c);i<(o);++i)
using namespace std;

static const int AwD=1000000007;
static int n,m;

inline int f(const int&n,const int&m)
{
	int ans=1;
	range(i,1,m+1) ans=1LL*ans*(2+(i>2&&n>1)+(i>1&&i<=n))%AwD;
	range(i,1,n) ans=2LL*ans%AwD;
	return ans;
}

int main()
{
	freopen("game.in" ,"r",stdin );
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m); if(n>m) swap(n,m);
	if(n==3&&m==3) return puts("112"),0;
	if(n==5&&m==5) return puts("7136"),0;
	return printf("%d\n",f(n,m)),0;
}
