#include<cstdio>
#include<algorithm>
#include<cstring>
#include<vector>
using namespace std;
const int N=5e3+10;
int m,n,t,l;
vector<int> e[N];
int a[N],len,tmp[N],len1;
int dfn[N],low[N],timer,cnt,color[N],v[N];
int s[N],top,key;
struct node{
	int ex,ey;
	node(int ex=0,int ey=0):ex(ex),ey(ey){}
}no[N<<1];int mp[N][N];

template<typename T>
inline void re(T &N)
{
	char c; while((c=getchar())< '0'||c> '9');
	N=c-'0';while((c=getchar())>='0'&&c<='9')N=N*10+c-'0';
} 
inline bool judge(int a[],int b[],int lena,int lenb)
{
	for(int i=1;i<=min(lena,lenb);i++)
	{
		if(a[i]<b[i])return 1;
		if(a[i]>b[i])return 0;
	}
	if(lena<lenb)return 1;
	return 0;
}
void dfs(int x,int p)
{
	a[++len]=x;
	for(int i=0;i<(int)e[x].size();i++)
	{
		if(e[x][i]==p||(x==no[l].ex&&e[x][i]==no[l].ey)||(e[x][i]==no[l].ex&&x==no[l].ey))continue;
		dfs(e[x][i],x);
	}
}
void tarjan(int x,int p)
{
	dfn[x]=low[x]=++timer;
	v[x]=1;s[++t]=x;
	for(int i=0;i< (int)e[x].size();i++)
	{
		if(e[x][i]==p)continue;
		if(!dfn[e[x][i]])
		{
			tarjan(e[x][i],x);
			low[x]=min(low[x],low[e[x][i]]);
		}else if(v[e[x][i]])low[x]=min(low[x],dfn[e[x][i]]);
	}
	if(low[x]==dfn[x])
	{
		cnt++;int sum=0;
		while(s[t]!=x){color[s[t]]=cnt;v[s[t--]]=0;sum++;}
		color[s[t]]=cnt;v[s[t--]]=0;sum++;
		if(sum>1)key=cnt;
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
//	freopen("data.txt","r",stdin);
	re(n),re(m);
	for(int i=1,x,y;i<=m;i++)
	{
		re(x),re(y);
		e[x].push_back(y);
		e[y].push_back(x); 
	}
	for(int i=1;i<=n;i++)sort(e[i].begin(),e[i].end());
	if(m==n-1)
	{
		dfs(1,0);
		for(int i=1;i<=len;i++)printf("%d ",a[i]);
		puts("");
	}
	else
	{
		len1=1;
		tmp[1]=1e9;
		tarjan(1,0);cnt=0;
		for(int i=1;i<=n;i++)
			for(int j=0;j< (int)e[i].size();j++)
				if(color[i]==color[e[i][j]]&&!mp[i][e[i][j]])
					no[++cnt]=node(i,e[i][j]),mp[i][e[i][j]]=mp[e[i][j]][i]=1;
		for(l=1;l<=cnt;l++)
		{
			len=0;
			dfs(1,0);
			if(judge(a,tmp,len,len1))
				memcpy(tmp,a,sizeof(a)),len1=len;
		}
		for(int i=1;i<=len1;i++)printf("%d ",tmp[i]);
		puts("");
	}
	return 0;
}
