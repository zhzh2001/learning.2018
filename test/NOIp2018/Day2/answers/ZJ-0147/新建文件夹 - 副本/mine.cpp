#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int m,n,t,sum;
int a[6][6],q1[25],q2[25],len,ok,key;

template<typename T>
inline void re(T &N)
{
	char c; while((c=getchar())< '0'||c> '9');
	N=c-'0';while((c=getchar())>='0'&&c<='9')N=N*10+c-'0';
}
inline bool judge()
{
	for(int i=1;i<=len;i++)
		if(q1[i]>q2[i])return 0;
	return 1;
}
void dfs(int x1,int y1,int x2,int y2,int flag)
{
	if(x1>n||x2>n||y1>m||y2>m)return;
	if(x1==n&&y1==m)
	{
		if(flag)
		{
			if(!judge())ok=1,key=0;
		}
		return;
	}
	if(flag)
	{
		x1++;x2++;
		q1[++len]=a[x1][y1];q2[len]=a[x2][y2];
		dfs(x1,y1,x2,y2,flag);
		if(ok)return;
		x2--;y2++;
		q1[len]=a[x1][y1];q2[len]=a[x2][y2];
		dfs(x1,y1,x2,y2,flag);
		if(ok)return;
		x1--;y1++;
		q1[len]=a[x1][y1];q2[len]=a[x2][y2];
		dfs(x1,y1,x2,y2,flag);
		if(ok)return;
		y2--;x2++;
		q1[len]=a[x1][y1];q2[len]=a[x2][y2];
		dfs(x1,y1,x2,y2,flag);
		if(ok)return;
		len--;
	}
	else
	{
		x1++;x2++;
		q1[++len]=a[x1][y1];q2[len]=a[x2][y2];
		dfs(x1,y1,x2,y2,flag);
		if(ok)return;
		x2--;y2++;
		x1--;y1++;
		q1[len]=a[x1][y1];q2[len]=a[x2][y2];
		dfs(x1,y1,x2,y2,flag);
		if(ok)return;
		y2--;x2++;
		q1[len]=a[x1][y1];q2[len]=a[x2][y2];
		dfs(x1,y1,x2,y2,1);
		if(ok)return;
		len--;
	}
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	re(n),re(m);
	if(n<=3&&m<=3)
	{
		if(m==3&&n==3){puts("112");return 0;}
		for(int i=0;i< 1ll<<(m*n);i++)
		{
			len=ok=0;key=1;
			for(int x=1;x<=n;x++)
				for(int y=1;y<=m;y++)
					a[x][y]=((i&(1<<((x-1)*m+y-1)))>0);
			dfs(1,1,1,1,0);
			sum+=key;
		}
		printf("%d\n",sum);
	}
}
