#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=5e3+10;
int m,n,t,p[N],v[N],r;
struct edge{
	int to,nxt;
}e[N<<1];int head[N];
struct node{
	int a[N<<2],len;
}tmp,ans;

template<typename T>
inline void re(T &N)
{
	char c; while((c=getchar())< '0'||c> '9');
	N=c-'0';while((c=getchar())>='0'&&c<='9')N=N*10+c-'0';
}
inline void add(int x,int y)
{
	e[++t].to=y;e[t].nxt=head[x];head[x]=t;
	e[++t].to=x;e[t].nxt=head[y];head[y]=t;
}
inline node cmp(node a,node b)
{
	for(int i=1;i<=min(a.len,b.len);i++)
	{
		if(a.a[i]<b.a[i])return a;
		if(a.a[i]>b.a[i])return b;
	}
	if(a.len<b.len)return a;
	else return b;
}
void dfs(int x)
{
	if(x==r)
	{
		int flag=1;
		for(int i=1;i<=n;i++)flag&=v[i];
		if(flag)
		{
			ans=cmp(ans,tmp);
			return;
		}
	}
	for(int i=head[x];i;i=e[i].nxt)
	{
		int to=e[i].to;
		if(p[x]==to)
		{
			tmp.a[++tmp.len]=to;
			dfs(to);
			tmp.len--;
			continue;
		}
		if(v[to])continue;
		tmp.a[++tmp.len]=to;
		v[to]=1;p[to]=x;
		dfs(to);
		tmp.len--;
		v[to]=0;p[to]=0;
	}
}
int main()
{
	freopen("data.txt","r",stdin);
	re(n);re(m);
	for(int i=1,x,y;i<=m;i++)
	{
		re(x),re(y);
		add(x,y);
	}ans.a[1]=1e9;
	for(int i=1;i<=n;i++)
	{
		memset(v,0,sizeof(v));tmp.len=0;
		r=i;v[i]=1;tmp.a[++tmp.len]=i;
		dfs(i);
	}
	for(int i=1;i<=ans.len;i++)printf("%d ",ans.a[i]);
	puts("");
}
