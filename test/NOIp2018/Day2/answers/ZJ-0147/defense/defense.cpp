#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int N=2e3+10,M=1e5+10;
typedef long long ll;
char c[3];
struct edge{
	int to,nxt;
}e[N<<1];int head[N];
int m,n,t,a[2],b[2];
ll f[N][2],w[M];
int v[N];

template<typename T>
inline void re(T &N)
{
	char c; int f=1;
	while((c=getchar())< '0'||c> '9')if(c=='-')f=-1;N=c-'0';
	while((c=getchar())>='0'&&c<='9')N=N*10+c-'0';N*=f;
}
inline void add(int x,int y)
{
	e[++t].to=y;e[t].nxt=head[x];head[x]=t;
	e[++t].to=x;e[t].nxt=head[y];head[y]=t;
}
void dfs(int x,int p)
{
	f[x][1]=w[x];f[x][0]=0;
	if(x==a[0])
	{
		if(a[1]==1)f[x][0]=1e17;
		else f[x][1]=1e17;
		for(int i=head[x];i;i=e[i].nxt)
		{
			int to=e[i].to;
			if(to==p)continue;
			dfs(to,x);
			if(a[1]==1)f[x][1]+=min(f[to][1],f[to][0]);
			else f[x][0]+=f[to][1];
		}
	}
	else if(x==b[0])
	{
		if(b[1]==1)f[x][0]=1e17;
		else f[x][1]=1e17;
		for(int i=head[x];i;i=e[i].nxt)
		{
			int to=e[i].to;
			if(to==p)continue;
			dfs(to,x);
			if(b[1]==1)f[x][1]+=min(f[to][1],f[to][0]);
			else f[x][0]+=f[to][1];
		}
	}
	else
	{
		for(int i=head[x];i;i=e[i].nxt)
		{
			int to=e[i].to;
			if(to==p)continue;
			dfs(to,x);
			f[x][1]+=min(f[to][1],f[to][0]);
			f[x][0]+=f[to][1];
		}
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
//	freopen("data.txt","r",stdin);
	re(n),re(m);scanf("%s",c+1);
	for(int i=1;i<=n;i++)re(w[i]);
	for(int i=1,x,y;i< n;i++)
	{
		re(x),re(y);
		add(x,y);
	}
	if(n<=2000)
	{
		for(int i=1;i<=m;i++)
		{
			re(a[0]),re(a[1]),re(b[0]),re(b[1]);
			dfs(1,0);
			printf("%lld\n",min(f[1][0],f[1][1])>1e14?-1:min(f[1][0],f[1][1]));
		}
	}
	else if(c[1]=='A')
	{
		dfs(1,0);ll sum=min(f[1][0],f[1][1]);
		int la=f[1][0]<f[1][1]?0:1;v[1]=la;
		for(int i=2;i<=n;i++)
		{
			v[i]=(f[i-1][la]-(la?w[i]:0)==f[i][1])?1:0;
			la=v[i];
		}
		for(int i=1;i<=m;i++)
		{
			re(a[0]),re(a[1]),re(b[0]),re(b[1]);
			if(v[a[0]]==a[1]&&v[b[0]]==b[1])printf("%lld\n",min(f[1][0],f[1][1]));
			else
			{
				dfs(1,0);
				printf("%lld\n",min(f[1][0],f[1][1])>1e14?-1:min(f[1][0],f[1][1]));
			}
		}
	}
	return 0;
}
