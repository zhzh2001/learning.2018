bool Memory_test_begin;
#include<bits/stdc++.h>
using namespace std;
#define FOR(a,b,c) for(int a=(b),a##_end__=(c);a<a##_end__;++a)
#define INF 0x3f3f3f3f3f3f3f3fLL
template<class T>inline bool chkmin(T&a,T const&b){return b<a?a=b,true:false;}
template<class T>inline bool chkmax(T&a,T const&b){return a<b?a=b,true:false;}
const int M=100005;
struct edge{
	int to;
	edge*next;
}*G[M],Pool[M<<1],*allc=Pool;
int p[M];
char S[10];
int n,m;
void add_edge(int u,int v){
	*allc=(edge){v,G[u]};
	G[u]=allc++;
}
namespace P_0{
	long long f[M],g[M];
	bool t0[M],t1[M];
	void dfs(int u,int fa){
		f[u]=p[u];
		g[u]=0;
		for(edge*e=G[u];e;e=e->next){
			if(e->to==fa) continue;
			dfs(e->to,u);
			g[u]+=f[e->to];
			f[u]+=min(f[e->to],g[e->to]);
			if(g[u]>INF) g[u]=INF;
			if(f[u]>INF) f[u]=INF;
		}
		if(t0[u]) f[u]=INF;
		if(t1[u]) g[u]=INF;
	}
	int main(){
		while(m--){
			int a,b,x,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(x==0) t0[a]=true;
			else t1[a]=true;
			if(y==0) t0[b]=true;
			else t1[b]=true;
			dfs(1,-1);
			long long ans=min(f[1],g[1]);
			if(ans==INF) ans=-1;
			printf("%lld\n",ans);
			t0[a]=t1[a]=t0[b]=t1[b]=false;
		}
		return 0;
	}
}
namespace P_1{
	long long f[M],g[M];
	void dfs(int u,int fa){
		f[u]=p[u];
		for(edge*e=G[u];e;e=e->next){
			if(e->to==fa) continue;
			dfs(e->to,u);
			g[u]+=f[e->to];
			f[u]+=min(f[e->to],g[e->to]);
		}
	}
	void re_dfs(int u,int fa){
		for(edge*e=G[u];e;e=e->next){
			int v=e->to;
			if(v==fa) continue;
			long long _f=f[u]-min(f[v],g[v]);
			long long _g=g[u]-f[v];
			g[v]+=_f;
			f[v]+=min(_f,_g);
			re_dfs(v,u);
		}
	}
	int main(){
		dfs(1,-1);
		g[1]=INF;
		re_dfs(1,-1);
		while(m--){
			int a,b,x,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(y==1) printf("%lld\n",f[b]);
			else printf("%lld\n",g[b]);
		}
	}
}
namespace P_2{
	long long f[M],g[M],A[M],B[M];
	int dep[M];
	void dfs(int u,int fa){
		f[u]=p[u];
		for(edge*e=G[u];e;e=e->next){
			if(e->to==fa) continue;
			dep[e->to]=dep[u]+1;
			dfs(e->to,u);
			g[u]+=f[e->to];
			f[u]+=min(f[e->to],g[e->to]);
		}
	}
	void re_dfs(int u,int fa){
		for(edge*e=G[u];e;e=e->next){
			int v=e->to;
			if(v==fa) continue;
			long long _f=f[u]-min(f[v],g[v]);
			long long _g=g[u]-f[v];
			g[v]+=_f;
			f[v]+=min(_f,_g);
			re_dfs(v,u);
		}
	}
	int main(){
		dfs(1,-1);
		FOR(u,1,n+1){
			A[u]=f[u];
			B[u]=g[u];
		}
		re_dfs(1,-1);
		while(m--){
			int a,b,x,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(x==0 and y==0){
				puts("-1");
				continue;
			}
			if(dep[b]<dep[a]) swap(x,y),swap(a,b);
			long long  _f=f[a]-min(A[b],B[b]);
			long long  _g=g[a]-A[b];
			long long ans=0;
			if(x==1) ans+=_f;
			else ans+=_g;
			if(y==1) ans+=A[b];
			else ans+=B[b];
			printf("%lld\n",ans);
		}
	}
}
bool Memory_test_end;
int main(){
	//printf("%lf\n",1.0*((&Memory_test_end)-(&Memory_test_begin))/1024/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d %s",&n,&m,S);
	FOR(i,1,n+1) scanf("%d",p+i);
	FOR(i,1,n){
		int u,v;
		scanf("%d%d",&u,&v);
		add_edge(u,v);
		add_edge(v,u);
	}
	if(S[1]=='1') P_1::main();
	else if(S[1]=='2') P_2::main();
	else P_0::main();
	return 0;
}
