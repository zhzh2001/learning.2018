bool Memory_test_begin;
#include<bits/stdc++.h>
using namespace std;
#define FOR(a,b,c) for(int a=(b),a##_end__=(c);a<a##_end__;++a)
template<class T>inline bool chkmin(T&a,T const&b){return b<a?a=b,true:false;}
template<class T>inline bool chkmax(T&a,T const&b){return a<b?a=b,true:false;}
const int M=5005;
vector<int>E[M];
int n,m,e;
namespace P_0{
	int ans[M];
	int sz;
	void dfs(int u,int fa){
		ans[sz++]=u;
		FOR(i,0,E[u].size()){
			int v=E[u][i];
			if(v==fa) continue;
			dfs(v,u);
		}
	}
	int main(){
		FOR(u,1,n+1) sort(E[u].begin(),E[u].end());
		dfs(1,-1);
		FOR(i,0,n) printf("%d ",ans[i]);
		puts("");
		return 0;
	}
}
namespace P_1{
	int ans[M],p[M],ring[M],Fa[M];
	bool vis[M],ban[M][M];
	int t,sz;
	bool Tarjan(int u,int fa){
		vis[u]=true;
		FOR(i,0,E[u].size()){
			int v=E[u][i];
			if(v==fa) continue;
			if(vis[v]){
				for(int w=u;w!=v;w=Fa[w]) ring[t++]=w;
				ring[t++]=v;
				return true;
			}else{
				Fa[v]=u;
				if(Tarjan(v,u)) return true;
			}
		}
		return false;
	}
	void dfs(int u,int fa){
		p[sz++]=u;
		FOR(i,0,E[u].size()){
			int v=E[u][i];
			if(ban[u][v] or v==fa) continue;
			dfs(v,u);
		}
	}
	bool cmp(){
		FOR(i,0,m) if(ans[i]!=p[i]) return p[i]<ans[i];
		return false;
	}
	int main(){
		FOR(u,1,n+1) sort(E[u].begin(),E[u].end());
		Tarjan(1,-1);
		ans[0]=-1;
		FOR(i,0,t){
			ban[ring[i]][ring[(i+1)%t]]=true;
			ban[ring[(i+1)%t]][ring[i]]=true;
			sz=0,dfs(1,-1);
			if(ans[0]<0 or cmp()) memcpy(ans,p,sizeof(ans));
			ban[ring[i]][ring[(i+1)%t]]=false;
			ban[ring[(i+1)%t]][ring[i]]=false;
		}
		FOR(i,0,n) printf("%d ",ans[i]);
		puts("");
	}
}
bool Memory_test_end;
int main(){
	//printf("%lf\n",1.0*((&Memory_test_end)-(&Memory_test_begin))/1024/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	FOR(i,0,m){
		int u,v;
		scanf("%d%d",&u,&v);
		E[u].push_back(v);
		E[v].push_back(u);
	}
	if(n==m) P_1::main();
	else P_0::main();
	return 0;
}
