bool Memory_test_begin;
#include<bits/stdc++.h>
using namespace std;
#define FOR(a,b,c) for(int a=(b),a##_end__=(c);a<a##_end__;++a)
#define Mod 1000000007
template<class T>inline bool chkmin(T&a,T const&b){return b<a?a=b,true:false;}
template<class T>inline bool chkmax(T&a,T const&b){return a<b?a=b,true:false;}
int n,m;
long long Pow(long long x,long long k){
	long long res=1;
	for(;k;k>>=1,x=(x*x)%Mod)
		if(k&1) res=(res*x)%Mod;
	return res;
}
namespace P_0{
	int main(){
		if(m<n) swap(n,m);
		if(n==1) printf("%lld\n",Pow(2,m));
		else if(n==2){
			printf("%lld\n",4LL*Pow(3,m-1)%Mod);
		}else puts("112");
	}
}
namespace P_1{
	int main(){
		printf("%lld\n",4LL*Pow(3,m-1)%Mod);
	}
}
/*
namespace P_2{
	const int M=1000015;
	int dp[M][5],q[M][5];
	int dfs(int i){
		if(i==m+3) return 1;
		int S;
		if(i<=2) S=0;
		else S=((q[i-2][0]==q[i-2][1])<<1)|(q[i-1][0]==q[i-1][1]);
		if(~dp[i][S]) return dp[i][S];
		int l=max(0,i-m);
		int r=min(n-1,i-1);
		int res=0;
		FOR(j,l-1,r+1){
			FOR(k,l,j+1) q[i][k]=0;
			FOR(k,j+1,r) q[i][k]=1;
			bool flag=true;
			if(i>3 and i!=m+2 and q[i-2][0]==q[i-2][1] and q[i][1]!=q[i][2]) flag=false;
			if(flag) res=(res+dfs(i+1))%Mod;
		}
		return dp[i][S]=res;
	}
	int main(){
		memset(dp,-1,sizeof(dp));
		printf("%d\n",dfs(1));
		return 0;
	}
}*/
bool Memory_test_end;
int main(){
	//printf("%lf\n",1.0*((&Memory_test_end)-(&Memory_test_begin))/1024/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m<n) swap(n,m);
	if(n<=3 and m<=3) P_0::main();
	else if(n==2) P_1::main();
	//else if(n==3) P_2::main();
	return 0;
}
