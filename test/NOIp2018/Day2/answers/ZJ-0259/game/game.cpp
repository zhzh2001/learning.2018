#include<bits/stdc++.h>
#define LL long long
#define FOR(x,l,r) for(int x=l,x##_=r;x<=x##_;x++)
#define FR(x,l,r) for(int x=l,x##_=r;x<x##_;x++)
#define DOR(x,r,l) for(int x=r,x##_=l;x>=x##_;x--)
using namespace std;
bool mem1;
const int N=100005;
const LL P=1e9+7;
int n,m;
struct P20{
	int A[5][5],lst[25];
	bool check(){
		FOR(i,1,n+m-2)lst[i]=0;
//		printf("%d\n",n+m-2);
		FR(i,0,1<<(n+m-2)){
			int ct=0,tot=0;
			int x=0,y=0;
			FR(j,0,n+m-2)if((1<<j)&i)ct++;
			if(ct!=n-1)continue;
//			printf("ct:%d %d\n",i,ct);
			DOR(j,n+m-2-1,0){
				if((1<<j)&i)x++;
				else y++;
				tot++;
//				printf("%d %d %d %d %d\n",x,y,A[x][y],tot,lst[tot]);
				if(A[x][y]<lst[tot])return 0;
				lst[tot]=A[x][y];
			}
			printf("P:");FOR(j,1,tot)printf("%d ",lst[j]);puts("");
		}
		FR(i,0,n){FR(j,0,m)printf("%d ",A[i][j]);puts("");}puts("");
		return 1;
	}
	void sl(){
		int ans=0;
		FR(i,0,1<<(n*m)){
			FR(j,0,n*m){
				int y=j%m,x=j/m;
				A[x][y]=(((1<<j)&i)?1:0);
			}
			printf("%d:\n",i);
			ans+=check();
		}
		printf("%d\n",ans);
	}
	void sol(){
		LL ans=4;
		FR(i,1,m)ans=(ans*3)%P;
		printf("%lld\n",ans);
	}
	void sl0(){
		LL ans=36;
		FOR(i,1,m-2)ans=(ans*4)%P;
		ans-=32LL*(m-2);
		ans=(ans%P+P)%P;
		printf("%lld\n",ans);
	}
	void s0(){
		LL ans=1,cnt=2;
		FR(i,1,min(n,m))ans=(ans*(i+1)*(i+1))%P,cnt*=4;
		FOR(i,1,m-min(n,m)+1)ans=(ans*(min(n,m)+1))%P;
		ans-=cnt*(m-min(n,m)+1);
		ans=(ans%P+P)%P;
		printf("%lld\n",ans);
	}
}p20;
bool mem2;
int main(){
//	printf("%lf\n",(&mem2-&mem1)/1024.0/1024);
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==2)p20.sol();
	else if(n==3)p20.sl0();
	else if(n==5&&m==5)puts("7136");
	else p20.s0();
//	p20.sl();
	
	return 0;
}
