#include<bits/stdc++.h>
#define LL long long
#define FOR(x,l,r) for(int x=l,x##_=r;x<=x##_;x++)
#define FR(x,l,r) for(int x=l,x##_=r;x<x##_;x++)
#define DOR(x,r,l) for(int x=r,x##_=l;x>=x##_;x--)
using namespace std;
bool mem1;
const int N=100005;
int A[N],L[N],R[N],tot;
int p[N];
vector<int>edge[N];
int x,y,a,b;
int n,m;
int mark[N][2];
int T;
struct P44{
	LL dp[N][2];
	void dfs(int x,int f){
		L[x]=++tot;
		LL ct1=0,ct2=0;
		FR(i,0,edge[x].size()){
			int k=edge[x][i];
			if(k==f)continue;
			dfs(k,x);
			ct1+=dp[k][1];
			ct2+=min(dp[k][1],dp[k][0]);
		}
		if(mark[x][0]!=T)dp[x][0]=ct1;
		else dp[x][0]=1e16;
		if(mark[x][1]!=T)dp[x][1]=ct2+p[x];
		else dp[x][1]=1e16;
	}
	void sl(){
		FOR(i,1,m){
			scanf("%d%d%d%d",&x,&y,&a,&b);
			T++;
			mark[x][!y]=T,mark[a][!b]=T;
			dfs(1,0);
			LL ans=min(dp[1][0],dp[1][1]);
			if(ans>=1e16)ans=-1;
			printf("%lld\n",ans);
		}
	}
}p44;
char str[5];
bool mem2;
int main(){
//	printf("%lf\n",(&mem2-&mem1)/1024.0/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int x,y;
	scanf("%d%d%s",&n,&m,str);
	FOR(i,1,n)scanf("%d",&p[i]);
	FR(i,1,n){
		scanf("%d%d",&x,&y);
		edge[x].push_back(y);
		edge[y].push_back(x);
	}
	p44.sl();
	return 0;
}
