#include<bits/stdc++.h>
#define LL long long
#define FOR(x,l,r) for(int x=l,x##_=r;x<=x##_;x++)
#define FR(x,l,r) for(int x=l,x##_=r;x<x##_;x++)
#define DOR(x,r,l) for(int x=r,x##_=l;x>=x##_;x--)
using namespace std;
bool mem1;
const int N=5005;
vector<int>edge[N];
int n,m,stk[N<<1],tot;
bool mark[N];
struct P60{
	void dfs(int x,int f){
		stk[++tot]=x;
		FR(i,0,edge[x].size()){
			int k=edge[x][i];
			if(k==f)continue;
			dfs(k,x);
		}
	}
	void sl(){
		dfs(1,0);
		FOR(i,1,tot)printf("%d ",stk[i]);puts("");
	}
}p60;
int IN[N],ans[N],top,ID[N];
queue<int>Q;
vector<int>F[N];
bool vis[N],vis1[N];
priority_queue<int>q;
struct P40{
	void dfs(int x,int f){
		if(vis1[x])return;
		vis1[x]=1;
		stk[++tot]=x;
		ID[x]=tot;
		FR(i,0,edge[x].size()){
			int k=edge[x][i];
			if(k==f||mark[k])continue;
			dfs(k,x);
		}
	}
	void TP(){
		FOR(i,1,n)if(IN[i]==1)Q.push(i),mark[i]=1;
		while(!Q.empty()){
			int x=Q.front();Q.pop();
			FR(i,0,edge[x].size()){
				int k=edge[x][i];
				if(mark[k])continue;
				IN[k]--;
				if(IN[k]==1)mark[k]=1,Q.push(k);
			}
		}
		FOR(i,1,n)if(!mark[i]){dfs(i,0);break;}
	}
	void dfs_edge(int x,int f,int fa){
//		printf("%d ",x);
		F[fa].push_back(x);
		FR(i,0,edge[x].size()){
			int k=edge[x][i];
			if(k==f||!mark[k])continue;
			dfs_edge(k,x,fa);
		}
	}
	int I(int x){
		if(x<1)return x+tot;
		if(x>tot)return x-tot;
	}
	void get_ans(){
		memset(vis,0,sizeof(vis));
		FOR(i,1,tot)stk[i+tot]=stk[i];
		DOR(i,1,tot){
			int x=stk[i];
			while(-q.top()<x){
				int k=-q.top();q.pop();
				FR(j,0,F[k].size()){
					ans[++top]=F[k][j];
				}
			}
			ans[++top]=x;
		}
	}
	void sl(){
		TP();
		FOR(i,1,tot){
			int x=stk[i];
			F[x].push_back(x);
			FR(j,0,edge[x].size()){
				int k=edge[x][j];
				if(!mark[k])continue;
				dfs_edge(k,x,k);
			}
		}
		get_ans();
		FOR(i,1,top)printf("%d ",ans[top]);
	}
}p40;
bool mem2;
int main(){
//	printf("%lf\n",(&mem2-&mem1)/1024.0/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int x,y;
	scanf("%d%d",&n,&m);
	FOR(i,1,m){
		scanf("%d%d",&x,&y);
		edge[x].push_back(y);
		edge[y].push_back(x);
		IN[x]++,IN[y]++;
	}
	FOR(i,1,n)sort(edge[i].begin(),edge[i].end());
	if(n-1==m)p60.sl();
	else p40.sl();
	return 0;
}
