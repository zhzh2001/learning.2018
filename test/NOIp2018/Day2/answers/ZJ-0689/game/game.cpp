#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
using namespace std;

typedef long long LL;
const int MAXN=9, MAXM=1000011;
const LL Md=1000000007;
int n, m;
LL ans;

namespace Solve1 {
  int tot, lim, ans;
  int a[MAXN][MAXN];

  int check() {
    int tot2=n+m-2, lim2=1<<tot2;
    for(int i=0;i<lim2;++i) {
      for(int j=i+1;j<lim2;++j) {
	int p=0, q=0, x=1, y=1;
	//printf("Go %d: ", i);
	for(int k=tot2-1;k>=0;--k) {
	  //printf("(%d,%d) ", x, y);
	  if(x>n || y>m) {
	    p=-1;
	    break;
	  }
	  p+=(a[x][y]<<(k+1));
	  if((i>>k)&1) ++x;
	  else ++y;
	}
	if(p==-1) continue;
	//printf("(%d,%d)\n", x, y);
	if(x>n || y>m) continue;
	else p+=a[x][y];
	x=y=1;
	for(int k=tot2-1;k>=0;--k) {
	  if(x>n || y>m) {
	    q=-1;
	    break;
	  }
	  q+=(a[x][y]<<(k+1));
	  if((j>>k)&1) ++x;
	  else ++y;
	}
	if(q==-1) continue;
	if(x>n || y>m) continue;
	else q+=a[x][y];
	//printf("%d %d ; %d %d\n", i, p, j, q);
	if(i>j && p>q) return 0;
	if(j>i && q>p) return 0;
      }
    }
    return 1;
  }
  
  void main() {
    if(n==0 || m==0) {
      puts("1");
      return;
    }
    tot=n*m;
    lim=1<<tot;
    ans=0;
    for(int i=0;i<lim;++i) {
      for(int j=0;j<tot;++j) {
	a[j/m+1][j%m+1]=(i>>j)&1;
      }
      ans+=check();
    }
    printf("%lld\n", (LL)ans%Md);
  }
}

namespace Solve2 {
  LL Powe(LL x, LL y) {
    LL res=1;
    while(y) {
      if(y&1) res=res*x%Md;
      x=x*x%Md;
      y>>=1;
    }
    return res;
  }
  
  void main() {
    LL ans=1;
    if(n==0 || m==0) {
      puts("1");
      return;
    }
    if(n==1) {
      printf("%lld\n", Powe(2ll, m));
      return;
    }
    if(n==2) {
      if(m==1) puts("4");
      else if(m==2) puts("12");
      else printf("%lld\n", 12ll*Powe(3ll, m-2)%Md);
      return;
    }
    if(n==3) {
      if(m==1) puts("8");
      else if(m==2) puts("36");
      else if(m==3) puts("112");
      else printf("%lld\n", 112ll*Powe(3ll, m-3)%Md);
      return;
    }
    if(n==4) {
      if(m==4) puts("912");
      else if(m==5) puts("2688");
    }
    if(n==5) {
      if(m==5) puts("7136");
    }
  }
}

int main() {
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);
  scanf("%d%d", &n, &m);
  if(n>m) swap(n, m);
  if(n<=3 && m<=3) {
    Solve1::main();
  } else {
    Solve2::main();
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}
