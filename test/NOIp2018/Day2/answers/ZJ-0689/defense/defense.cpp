#include <bits/stdc++.h>
using namespace std;

typedef long long LL;
const int MAXN=100011;
struct edge {
  int x, y, nxt;
} E[MAXN<<1];
int head[MAXN], ord[MAXN], p[MAXN], father[MAXN];
LL dp[MAXN][2], adp[MAXN][2];
int n, m, tot;
char type[4];

void AddEdge(int x, int y) {
  E[++tot]=(edge){x, y, head[x]};
  head[x]=tot;
}

void PreDFS(int x, int fa) {
  father[x]=fa;
  for(int i=head[x];~i;i=E[i].nxt) {
    int to=E[i].y;
    if(to!=fa) {
      PreDFS(to, x);
    }
  }
}

void DFS(int x, int fa) {
  dp[x][1]=p[x];
  for(int i=head[x];~i;i=E[i].nxt) {
    int to=E[i].y;
    if(to!=fa) {
      DFS(to, x);
      LL mn=0x7f7f7f7f7f7f7f7f;
      if(dp[to][0]!=-1) {
	mn=min(mn, dp[to][0]);
      }
      if(dp[to][1]!=-1) {
	mn=min(mn, dp[to][1]);
	if(dp[x][0]!=-1) dp[x][0]+=dp[to][1];
      } else {
	dp[x][0]=-1;
      }
      if(mn<0x7f7f7f7f7f7f7f7f) {
	if(dp[x][1]!=-1) dp[x][1]+=mn;
      } else {
	dp[x][1]=-1;
      }
    }
  }
  if(ord[x]&1) dp[x][1]=-1;
  if(ord[x]&2) dp[x][0]=-1;
}

void SubDFS(int x) {
  if(x==0) return;
  adp[x][0]=adp[x][1]=0;
  adp[x][1]=p[x];
  for(int i=head[x];~i;i=E[i].nxt) {
    int to=E[i].y;
    if(to!=father[x]) {
      LL mn=0x7f7f7f7f7f7f7f7f;
      if(adp[to][0]!=-1) {
	mn=min(mn, adp[to][0]);
      }
      if(adp[to][1]!=-1) {
	mn=min(mn, adp[to][1]);
	if(adp[x][0]!=-1) adp[x][0]+=adp[to][1];
      } else {
	adp[x][0]=-1;
      }
      if(mn<0x7f7f7f7f7f7f7f7f) {
	if(adp[x][1]!=-1) adp[x][1]+=mn;
      } else {
	adp[x][1]=-1;
      }
    }
  }
  if(ord[x]&1) adp[x][1]=-1;
  if(ord[x]&2) adp[x][0]=-1;
  SubDFS(father[x]);
}

void SubSolve(int a, int b, int x, int y) {
  for(int i=head[father[a]];~i;i=E[i].nxt) {
    int to=E[i].y;
    if(to!=father[father[a]]) {
      adp[to][0]=dp[to][0];
      adp[to][1]=dp[to][1];
    }
  }
  for(int i=head[father[b]];~i;i=E[i].nxt) {
    int to=E[i].y;
    if(to!=father[father[b]]) {
      adp[to][0]=dp[to][0];
      adp[to][1]=dp[to][1];
    }
  }
  adp[a][0]=dp[a][0]; adp[a][1]=dp[a][1];
  adp[b][0]=dp[b][0]; adp[b][1]=dp[b][1];
  adp[a][1^x]=-1; adp[b][1^y]=-1;
  SubDFS(father[a]);
  SubDFS(father[b]);
  if(adp[1][0]==-1) {
    printf("%lld\n", adp[1][1]);
  } else if(adp[1][1]==-1) { 
    printf("%lld\n", adp[1][0]);
  } else {
    printf("%lld\n", min(adp[1][0], adp[1][1]));
  }
}

int main() {
  freopen("defense.in", "r", stdin);
  freopen("defense.out", "w", stdout);
  memset(head, -1, sizeof head);
  memset(ord, 0, sizeof ord);
  scanf("%d%d%s", &n, &m, type);
  for(int i=1;i<=n;++i) {
    scanf("%d", &p[i]);
  }
  for(int i=1, x, y;i<n;++i) {
    scanf("%d%d", &x, &y);
    AddEdge(x, y);
    AddEdge(y, x);
  }
  if(type[0]=='B') {
    PreDFS(1, 0);
    DFS(1, 0);
  }
  while(m--) {
    int x, a, y, b;
    scanf("%d%d%d%d", &a, &x, &b, &y);
    ord[a]|=(1<<x);
    ord[b]|=(1<<y);
    if(type[0]=='B') {
      SubSolve(a, b, x, y);
      ord[a]=ord[b]=0;
      continue;
    }
    memset(dp, 0, sizeof dp);
    DFS(1, 0);
    ord[a]=ord[b]=0;
    if(dp[1][0]==-1) {
      printf("%lld\n", dp[1][1]);
    } else if(dp[1][1]==-1) { 
      printf("%lld\n", dp[1][0]);
    } else {
      printf("%lld\n", min(dp[1][0], dp[1][1]));
    }
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}
