#include <bits/stdc++.h>
using namespace std;

const int MAXN=10011;
vector<int> G[MAXN];
int ans[MAXN], now[MAXN], vis[MAXN];
pair<int, int> E[MAXN];
int n, m, top, clk, forbx, forby;

void DFS(int x, int fa) {
  vis[x]=clk;
  now[++top]=x;
  for(int i=0;i<(int)G[x].size();++i) {
    int to=G[x][i];
    if(x==forbx && to==forby) continue;
    if(to==forbx && x==forby) continue;
    if(to!=fa && vis[to]!=clk) {
      DFS(to, x);
    }
  }
}

void Copy() {
  for(int i=1;i<=n;++i) {
    ans[i]=now[i];
  }
}

void Update() {
  if(top<n) return;
  for(int i=1;i<=n;++i) {
    if(ans[i]!=now[i]) {
      if(ans[i]<now[i]) return;
      else Copy();
      return;
    }
  }
}
 
int main() {
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);
  scanf("%d%d", &n, &m);
  for(int i=1, x, y;i<=m;++i) {
    scanf("%d%d", &x, &y);
    G[x].push_back(y);
    G[y].push_back(x);
    E[i]=make_pair(x, y);
  }
  for(int i=1;i<=n;++i) {
    ans[i]=n+1;
    sort(G[i].begin(), G[i].end());
  }
  if(m==n-1) {
    top=0;
    ++clk;
    DFS(1, 0);
    Update();
  } else {
    for(int i=1;i<=m;++i) {
      forbx=E[i].first;
      forby=E[i].second;
      ++clk;
      top=0;
      DFS(1, 0);
      Update();
    }
  }
  for(int i=1;i<=n;++i) {
    printf("%d", ans[i]);
    putchar(i!=n ? ' ' : '\n');
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}
