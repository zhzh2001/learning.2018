#include<bits/stdc++.h>
#define ll long long
using namespace std;

string ss;
struct line{
	int to,next;
};  line lx[200020];
ll inf,dp[100010][2];
int lnum,head[100010],pay[100010];
int smark[100010],n,m;
inline void addl(int x,int y){
	lnum++; lx[lnum].to=y; lx[lnum].next=head[x];
	head[x]=lnum;
}
inline void dfs(int k,int fro){
	int t=head[k];
	bool flag=false;
	dp[k][0]=0; dp[k][1]=pay[k];
	while(t!=0){
		int b=lx[t].to;
		if(b!=fro){
			dfs(b,k);
			dp[k][0]+=dp[b][1];
			dp[k][1]+=min(dp[b][0],dp[b][1]);
		}
		t=lx[t].next;
	}
	if(smark[k]==0)dp[k][1]=inf;
	if(smark[k]==1)dp[k][0]=inf;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	inf=(1<<28); inf=inf*inf;
	scanf("%d%d",&n,&m); cin>>ss;
	if(true){
		lnum=0;
		for(int i=1;i<=n;++i){
			scanf("%d",&pay[i]);
		}
		for(int i=1;i<=n-1;++i){
			int x,y;
			scanf("%d%d",&x,&y);
			addl(x,y); addl(y,x);
		}
		for(int i=1;i<=m;++i){
			int a,b,x,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			for(int j=1;j<=n;++j)smark[j]=-1;
			int t=head[a]; 
			bool flag1=true;
			while(t!=0){
				int c=lx[t].to;
				if(c==b){
					flag1=false; break;
				}
				t=lx[t].next;
			}
			if(x==0&&y==0&&flag1==false){
				printf("-1\n"); continue;
			}
			smark[a]=x; smark[b]=y;
			dfs(1,0);
			printf("%lld\n",min(dp[1][0],dp[1][1]));
		}
	}
	return 0;
}

