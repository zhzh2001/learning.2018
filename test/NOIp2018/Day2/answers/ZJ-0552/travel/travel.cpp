#include<bits/stdc++.h>
using namespace std;
const int N=5005;
vector<int> vec[N],circle;
int n,m,fa[N],vis[N],dep[N],a[N],ans[N],x,y,len;
inline bool check(int u,int v)
{
	return (u==x&&v==y)||(u==y&&v==x);
}
inline bool judge()
{
	for(int i=1;i<=n;i++)
		if(a[i]<ans[i])return 1;
		else if(a[i]>ans[i])return 0;
	return 0;
}
void dfs(int u,int f)
{
	fa[u]=f;vis[u]=1;dep[u]=dep[f]+1;
	for(int i=0;i<vec[u].size();i++)if(vec[u][i]!=f)
		if(!vis[vec[u][i]])dfs(vec[u][i],u);
		else if(dep[vec[u][i]]<dep[u])
		{
			int x=u;
			while(x!=vec[u][i])circle.push_back(x),x=fa[x];
			circle.push_back(vec[u][i]);
		}
}
void get(int u,int f)
{
	a[++len]=u;
	for(int i=0;i<vec[u].size();i++)if(vec[u][i]!=f&&!check(u,vec[u][i]))get(vec[u][i],u);
}
inline void solve()
{
	len=0;get(1,0);
	for(int i=1;i<=n;i++)printf("%d ",a[i]);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		int x,y;scanf("%d%d",&x,&y);
		vec[x].push_back(y);
		vec[y].push_back(x);
	}
	for(int i=1;i<=n;i++)sort(vec[i].begin(),vec[i].end());
	dfs(1,0);
	if(!circle.size())return solve(),0;
	ans[1]=n+1;
	for(int i=0;i<circle.size();i++)
	{
		x=circle[i];y=circle[(i+1)%circle.size()];
		len=0;get(1,0);
		if(judge())for(int j=1;j<=n;j++)ans[j]=a[j];
	}
	for(int i=1;i<=n;i++)printf("%d ",ans[i]);puts("");
	return 0;
}
