#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=1e5+5;
const ll INF=1e18;
struct E{int to,next;}edge[N<<1];
int n,m,head[N],tot,p[N],c[N];
ll f[N],g[N];
char s[10];
set<pair<int,int> > st;
inline ll mnn(ll a,ll b){return a<b?a:b;}
void dfs(int u,int fa)
{
	f[u]=g[u]=INF;
	for(int i=head[u];i!=-1;i=edge[i].next)if(edge[i].to!=fa)dfs(edge[i].to,u);
	if(c[u]!=0)
	{
		f[u]=p[u];
		for(int i=head[u];i!=-1;i=edge[i].next)if(edge[i].to!=fa)f[u]+=mnn(f[edge[i].to],g[edge[i].to]),f[u]=mnn(f[u],INF);
	}
	if(c[u]!=1)
	{
		g[u]=0;
		for(int i=head[u];i!=-1;i=edge[i].next)if(edge[i].to!=fa)g[u]+=f[edge[i].to],g[u]=mnn(g[u],INF);
	}
}
inline void solve(int a,int x,int b,int y)
{
	for(int i=1;i<=n;i++)c[i]=-1;
	if(!x)
	{
		c[a]=0;
		for(int i=head[a];i!=-1;i=edge[i].next)c[edge[i].to]=1;
	}
	else c[a]=1;
	if(!y)
	{
		c[b]=0;
		for(int i=head[b];i!=-1;i=edge[i].next)c[edge[i].to]=1;
	}
	else c[b]=1;
	dfs(1,0);
	printf("%lld\n",mnn(f[1],g[1]));
}
struct node{ll a[4];}t[N<<2];
inline void clear(node&a){for(int i=0;i<4;i++)a.a[i]=0;}
inline ll get(node a){return mnn(mnn(a.a[0],a.a[1]),mnn(a.a[2],a.a[3]));}
inline node merge(node a,node b)
{
	node c;clear(c);
	c.a[0]=mnn(a.a[0]+b.a[2],mnn(a.a[1]+b.a[0],a.a[1]+b.a[2]));
	c.a[1]=mnn(a.a[0]+b.a[3],mnn(a.a[1]+b.a[1],a.a[1]+b.a[3]));
	c.a[2]=mnn(a.a[2]+b.a[2],mnn(a.a[3]+b.a[0],a.a[3]+b.a[2]));
	c.a[3]=mnn(a.a[2]+b.a[3],mnn(a.a[3]+b.a[1],a.a[3]+b.a[3]));
	return c;
}
void build(int u,int l,int r)
{
	if(l==r)
	{
		clear(t[u]);
		t[u].a[3]=p[l];
		t[u].a[1]=t[u].a[2]=INF;
		return;
	}
	int mid=(l+r)>>1;
	build(u<<1,l,mid);
	build(u<<1|1,mid+1,r);
	t[u]=merge(t[u<<1],t[u<<1|1]);
}
node query(int u,int l,int r,int left,int right)
{
	if(left<=l&&right>=r)return t[u];
	int mid=(l+r)>>1;
	if(right<=mid)return query(u<<1,l,mid,left,right);
	if(left>mid)return query(u<<1|1,mid+1,r,left,right);
	return merge(query(u<<1,l,mid,left,right),query(u<<1|1,mid+1,r,left,right));
}
inline int work(int x){if(c[x]==-1)return c[x]=1,p[x];return 0;}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense2.out","w",stdout);
	memset(c,-1,sizeof(c));
	memset(head,-1,sizeof(head));
	scanf("%d%d%s",&n,&m,s);
	for(int i=1;i<=n;i++)scanf("%d",&p[i]);
	for(int i=1;i<n;i++)
	{
		int x,y;scanf("%d%d",&x,&y);
		st.insert(make_pair(x,y));st.insert(make_pair(y,x));
		edge[++tot]=(E){y,head[x]};head[x]=tot;
		edge[++tot]=(E){x,head[y]};head[y]=tot;
	}
	build(1,1,n);
	while(m--)
	{
		int a,b,x,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);if(a>b)swap(a,b),swap(x,y);
		set<pair<int,int> >::iterator it=st.lower_bound(make_pair(a,b));
		if(x==0&&y==0&&it!=st.end()&&(*it)==make_pair(a,b)){puts("-1");continue;}
		if(n<=2000){solve(a,x,b,y);continue;}
		int pp[4];
		pp[0]=x?a-1:a-2;
		pp[1]=x?a+1:a+2;
		pp[2]=y?b-1:b-2;
		pp[3]=y?b+1:b+2;
		ll ans=0;
		if(pp[0]>=1)ans+=get(query(1,1,n,1,pp[0]));
		if(pp[1]<=pp[2])ans+=get(query(1,1,n,pp[1],pp[2]));
		if(pp[3]<=n)ans+=get(query(1,1,n,pp[3],n));
		if(x)ans+=work(a);else ans+=work(a-1)+work(a+1);
		if(y)ans+=work(b);else ans+=work(b-1)+work(b+1);
		for(int i=a-1;i<=a+1;i++)c[i]=-1;
		for(int i=b-1;i<=b+1;i++)c[i]=-1;
		printf("%lld\n",ans);
	}
	return 0;
}
