#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int M=50,mod=1e9+7;
struct mat
{
	ll a[M][M];
}a,b;
inline mat operator *(mat a,mat b)
{
	mat c;
	for(int i=0;i<M;i++)
		for(int j=0;j<M;j++)
		{
			c.a[i][j]=0;
			for(int k=0;k<M;k++)c.a[i][j]=(c.a[i][j]+a.a[i][k]*b.a[k][j])%mod;
		}
	return c;
}
int n,m,f[2][1<<8][8];
vector<int> vec[1<<8];
inline bool check(int S,int T)
{
	S&=(1<<(n-1))-1;T>>=1;
	return (S&T)==T;
}
inline int get(int S,int x){return (S>>x)&1;}
inline int id(int x,int y){return x*n+y;}
inline void solve()
{
	memset(a.a,0,sizeof(a.a));
	int N=1<<n;
	for(int j=0;j<N;j++)
		for(int k=0;k<n;k++)
			for(int p=0;p<N;p++)if(check(j,p))
			{
				int flag=1,q=k;
				for(int l=1;l<=k;l++)if(get(j,l-1)!=get(p,l)){flag=0;break;}
				if(!flag)continue;
				for(int l=n-1;l;l--)if(get(j,l-1)==get(p,l)){if(l-1>q)q=l-1;break;}
				a.a[id(j,k)][id(p,q)]++;
			}
	memcpy(b.a,a.a,sizeof(a.a));
	memset(a.a,0,sizeof(a.a));
	for(int i=0;i<M;i++)a.a[i][i]=1;
	m--;
	while(m)
	{
		if(m&1)a=a*b;
		b=b*b;
		m>>=1;
	}
	ll sum=0;
	for(int i=0;i<N;i++)
		for(int j=0;j<M;j++)sum=(sum+a.a[id(i,0)][j])%mod;
	printf("%lld\n",sum);
}
int main()
{
	//freopen("game.in","r",stdin);
	//freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n<=3)return solve(),0;
	int N=1<<n;
	for(int i=0;i<N;i++)f[0][i][0]=1;
	int now=0,lst=0;
	for(int i=1;i<m;i++)
	{
		now^=1;
		memset(f[now],0,sizeof(f[now]));
		for(int j=0;j<N;j++)
			for(int k=0;k<n;k++)if(f[lst][j][k])
				for(int x=0;x<vec[j].size();x++)
				{
					int p=vec[j][x],flag=1,q=k;
					for(int l=1;l<=k;l++)if(get(j,l-1)!=get(p,l)){flag=0;break;}
					if(!flag)continue;
					for(int l=n-1;l;l--)if(get(j,l-1)==get(p,l)){if(l-1>q)q=l-1;break;}
					f[now][p][q]=(f[now][p][q]+f[lst][j][k])%mod;
				}
		lst=now;
	}
	int sum=0;
	for(int i=0;i<N;i++)
		for(int j=0;j<n;j++)sum=(sum+f[now][i][j])%mod;
	printf("%d\n",sum);
	return 0;
}
