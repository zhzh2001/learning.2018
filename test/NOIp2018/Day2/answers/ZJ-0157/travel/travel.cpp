#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<vector>
using namespace std;
const int N=6010;
int n,m,tot,cnt,X[N],Y[N],A[N],ans[N],Stack[N],top,vis[N],p[N];
vector<pair<int,int> >E[N];
void dfs(int u,int fa,int P)
{
	A[++cnt]=u;
	for (int i=0;i<(int)E[u].size();i++)
	{
		int v=E[u][i].first;
		if (v==fa||E[u][i].second==P) continue;
		dfs(v,u,P);
	}
}
void DFS(int u,int fa)
{
	if (vis[u]==1)
	{
		int i;
		do
		{
			i=Stack[top--];
			p[i]=1;
			vis[i]=2;
		}while (i!=u);
		return;
	}
	vis[u]=1;
	Stack[++top]=u;
	for (int i=0;i<(int)E[u].size();i++)
	{
		int v=E[u][i].first;
		if (E[u][i].second==fa) continue;
		if (vis[v]!=2) DFS(v,E[u][i].second);
	}
	vis[u]=2;
	top--;
}
bool check()
{
	for (int i=1;i<=n;i++)
	if (A[i]!=ans[i])
	{
		if (A[i]<ans[i]) return 1;
			else return 0;
	}
	return 0;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d",&X[i],&Y[i]);
		E[X[i]].push_back(make_pair(Y[i],i));
		E[Y[i]].push_back(make_pair(X[i],i));
	}
	for (int i=1;i<=n;i++)
		sort(E[i].begin(),E[i].end());
	if (m==n-1)
	{
		cnt=0;
		dfs(1,0,0);
		for (int i=1;i<=n;i++) printf("%d%c",A[i]," \n"[i==n]);
		return 0;
	}
	DFS(1,0);
	for (int i=1;i<=n;i++) ans[i]=n+1;
	for (int i=1;i<=m;i++)
	if (p[X[i]]&&p[Y[i]])
	{
		cnt=0;
		dfs(1,0,i);
		if (check())
		{
			for (int i=1;i<=n;i++) ans[i]=A[i];
		}
	}
	for (int i=1;i<=n;i++) printf("%d%c",ans[i]," \n"[i==n]);
	return 0;
}
