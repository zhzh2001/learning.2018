#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
#define mo 1000000007
using namespace std;
int n,m,A[100000],p[10][10],ans,cnt,vis[100000];
void solve(int x,int y,int S,int sta)
{
	if (x==n&&y==m)
	{
		vis[S]=cnt;
		A[S]=sta;
		return;
	}
	if (x+1<=n) solve(x+1,y,S<<1,sta<<1|p[x][y]);
	if (y+1<=m) solve(x,y+1,S<<1|1,sta<<1|p[x][y]);
}
bool check()
{
	cnt++;
	solve(1,1,0,0);
	int Max=0;
	for (int i=0;i<(1<<(n+m-2));i++)
	if (vis[i]==cnt)
	{
		if (Max>A[i])
		{
			return 0;
		}
		Max=max(Max,A[i]);
	}
	return 1;

}
void dfs(int x,int y)
{
	if (x>n)
	{
		if (check())
		{
			ans++;
		}
		return;
	}
	int X=x,Y=y;
	p[X][Y]=1;
	y++;
	if (y>m) x++,y=1;
	dfs(x,y);
	p[X][Y]=0;
	dfs(x,y);
}
ll mi(ll x,int y)
{
	ll ans=1;
	while (y)
	{
		if (y&1) ans=ans*x%mo;
		x=x*x%mo;
		y>>=1;
	}
	return ans;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==5&&m==5)
	{
		puts("7136");
		return 0;
	}
	if (m<=6)
	{
		dfs(1,1);
		printf("%d\n",ans);
		return 0;
	}
	if (n==1)
	{
		printf("%lld\n",mi(2,m));
		return 0;
	}
	if (n==2)
	{
		printf("%lld\n",mi(3,m-1)*4%mo);
		return 0;
	}
	printf("%lld\n",mi(3,m-3)*112%mo);
	return 0;
}
