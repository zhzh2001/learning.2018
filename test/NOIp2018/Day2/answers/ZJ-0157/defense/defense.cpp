#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
const int N=200100;
int n,m,tot,Next[N*2],head[N],tree[N*2],a[N],flag[N],father[N],dep[N];
ll f[N][2],last[N][2];
char T[10];
void add(int x,int y)
{
	tot++;
	Next[tot]=head[x];
	head[x]=tot;
	tree[tot]=y;
}
void dfs(int u,int fa)
{
	if (flag[u]==1) f[u][0]=1LL<<60;
	if (flag[u]==0) f[u][1]=1LL<<60;
		else f[u][1]=a[u];
	for (int i=head[u];i;i=Next[i])
	{
		int v=tree[i];
		if (v==fa) continue;
		dfs(v,u);
		if (flag[u]!=1) f[u][0]=min(f[u][0]+f[v][1],1LL<<60);
		if (flag[u]!=0) f[u][1]=min(f[u][1]+min(f[v][0],f[v][1]),1LL<<60);
	}
}
void DFS(int u,int fa,int depth)
{
	father[u]=fa;
	dep[u]=depth;
	for (int i=head[u];i;i=Next[i])
	{
		int v=tree[i];
		if (v==fa) continue;
		DFS(v,u,depth+1);
	}
}
void solve(int u,int x,int v,int y)
{
	int lastu=u,lastv=v;
	last[u][0]=f[u][0];last[u][1]=f[u][1];
	last[v][0]=f[v][0];last[v][1]=f[v][1];
	flag[u]=x;flag[v]=y;
	while (u!=v)
	{
		if (dep[u]<dep[v]) swap(u,v);
		int fa=father[u];
		if (v!=fa)
		{
			last[fa][0]=f[fa][0];last[fa][1]=f[fa][1];
		}
		if (flag[u]==1) f[u][0]=1LL<<60;
		if (flag[u]==0) f[u][1]=1LL<<60;
		if (flag[fa]!=1) f[fa][0]=min(f[fa][0]+f[u][1]-last[u][1],1LL<<60);
		if (flag[fa]!=0) f[fa][1]=min(f[fa][1]+min(f[u][0],f[u][1])-min(last[u][0],last[u][1]),1LL<<60);
		f[u][0]=last[u][0];f[u][1]=last[u][1];
		u=fa;
	}
	while (u!=1)
	{
		int fa=father[u];
		last[fa][0]=f[fa][0];last[fa][1]=f[fa][1];
		if (flag[u]==1) f[u][0]=1LL<<60;
		if (flag[u]==0) f[u][1]=1LL<<60;
		if (flag[fa]!=1) f[fa][0]=min(f[fa][0]+f[u][1]-last[u][1],1LL<<60);
		if (flag[fa]!=0) f[fa][1]=min(f[fa][1]+min(f[u][0],f[u][1])-min(last[u][0],last[u][1]),1LL<<60);
		f[u][0]=last[u][0];f[u][1]=last[u][1];
		u=fa;
	}
	if (flag[u]==1) f[u][0]=1LL<<60;
	if (flag[u]==0) f[u][1]=1LL<<60;
	ll ans=min(f[1][0],f[1][1]);
	f[u][0]=last[u][0];f[u][1]=last[u][1];
	flag[lastu]=flag[lastv]=-1;
	if (ans==1LL<<60) puts("-1");
		else printf("%lld\n",ans);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,T);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<n;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);add(y,x);
	}
	for (int i=1;i<=n;i++) flag[i]=-1;
	if (T[0]=='B')
	{
		DFS(1,0,1);
		dfs(1,0);
		while (m--)
		{
			int x,X,y,Y;
			scanf("%d%d%d%d",&x,&X,&y,&Y);
			solve(x,X,y,Y);
		}
		return 0;
	}
	while (m--)
	{
		int x,y,X,Y;
		scanf("%d%d%d%d",&x,&X,&y,&Y);
		flag[x]=X;flag[y]=Y;
		for (int i=1;i<=n;i++) f[i][0]=f[i][1]=0;
		dfs(1,0);
		ll ans=min(f[1][0],f[1][1]);
		if (ans==1LL<<60) puts("-1");
			else printf("%lld\n",ans);
		flag[x]=-1;flag[y]=-1;
	}
	return 0;
}
