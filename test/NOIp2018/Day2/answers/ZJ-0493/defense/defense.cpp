#include<bits/stdc++.h>
#define ll long long
using namespace std;

char getch()
{
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}

void read(ll &x)
{
	x=0;
	char c=getch();
	while(c<'0'||c>'9') c=getch();
	while(c>='0'&&c<='9')
	{
		x=(x<<1)+(x<<3)+(c-'0');
		c=getch();
	}
}

ll n,m,num;
char s;
ll f[100001];
ll dp[100001][3];

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%lld %lld %c%lld",&n,&m,&s,&num);
	for(ll i=1;i<=n;++i) read(f[i]);
	for(ll i=1,x,y;i<n;++i) read(x),read(y);
	for(ll t=1;t<=m;++t)
	{
		ll a,x,b,y;
		read(a);read(x);read(b);read(y);
		if(abs(a-b)==1&&x==0&&y==0)
		{
			puts("-1");
			continue;
		}
		memset(dp,0x3f,sizeof(dp));
		dp[0][0]=dp[0][1]=0;
		for(ll i=1;i<=n;++i)
		{
			dp[i][0]=dp[i-1][1];
			dp[i][1]=min(dp[i-1][0],dp[i-1][1])+f[i];
			if(i==a)
			{
				if(x==0) dp[i][1]=0x3f3f3f3f;
				else 
				{
					dp[i][0]=0x3f3f3f3f;
					if(b==a+1&&y==1) dp[i][0]=dp[i-1][1]+f[i];
				}
			}
			if(i==b)
			{
				if(y==0) dp[i][1]=0x3f3f3f3f;
				else 
				{
					dp[i][0]=0x3f3f3f3f;
					if(a==b+1&&x==1) dp[i][0]=dp[i-1][1]+f[i];
				}
			}
		}
		printf("%lld\n",min(dp[n][0],dp[n][1]));
	}
	return 0;
}


