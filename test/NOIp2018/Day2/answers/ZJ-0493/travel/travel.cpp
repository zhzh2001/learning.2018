#include<bits/stdc++.h>
using namespace std;

char getch()
{
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}

void read(int &x)
{
	x=0;
	char c=getch();
	while(c<'0'||c>'9') c=getch();
	while(c>='0'&&c<='9')
	{
		x=(x<<1)+(x<<3)+(c-'0');
		c=getch();
	}
}

int n,m,cnt;
int head[5003],ins[5003],dfn[5003],low[5003],color[5003],vis[5003];
struct node
{
	int u,nxt,v;
}ed[10001];
stack<int> s;
priority_queue<int,vector<int>,greater<int> > e[5003],sd[5003];

void add(int u,int v)
{
	ed[++cnt].u=u;
	ed[cnt].v=v;
	ed[cnt].nxt=head[u];
	head[u]=cnt;
}

void tarjan(int u,int f)
{
	dfn[u]=low[u]=++cnt;
	s.push(u);ins[u]=1;
	for(int i=head[u];i;i=ed[i].nxt)
	{
		int v=ed[i].v;
		if(!dfn[v])
		{
			tarjan(v,u);
			low[u]=min(low[u],low[v]);
		}
		else if(ins[v]&&v!=f) low[u]=min(low[u],dfn[v]);
	}
	if(dfn[u]==low[u])
	{
		int v;
		do
		{
			v=s.top();s.pop();
			ins[v]=0;
			sd[u].push(v);
			color[v]=u;
		}while(v!=u);
	}
}

void dfs2(int u)
{
	vis[u]=1;
	printf("%d ",u);
	while(!sd[u].empty())
	{
		if(sd[u].top()==u)
		{
			sd[u].pop();
			continue;
		}
		printf("%d ",sd[u].top());
		sd[u].pop();
	}
	while(!e[u].empty())
	{
		int v=e[u].top();e[u].pop();
		if(vis[v]) continue;
		dfs2(v);
	}
}

void prefix1()
{
	for(int i=1,x,y;i<=m;++i)
	{
		read(x);read(y);
		add(x,y);add(y,x);
	}
	cnt=0;
	tarjan(1,0);
	m<<=1;
	for(int i=1;i<=m;++i)
	{
		int u=color[ed[i].u],v=color[ed[i].v];
		if(u!=v)
		{
			e[u].push(v);
			e[v].push(u);
		}
	}
	dfs2(1);
}

void dfs1(int u)
{
	printf("%d ",u);
	while(!e[u].empty())
	{
		int v=e[u].top();e[u].pop();
		if(vis[v]) continue;
		dfs1(v);
	}
}

void prefix2()
{
	for(int i=1,x,y;i<=m;++i)
	{
		read(x);read(y);
		e[x].push(y);
		e[y].push(x);
	}
	dfs1(1);
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n);read(m);
	if(m==n) prefix1();
	else prefix2();
	return 0;
}
//如果是一张图就缩点
//对新建的树dfs； 
