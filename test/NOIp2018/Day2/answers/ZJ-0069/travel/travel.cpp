#include <bits/stdc++.h>
using namespace std;
#define M 5005
#define LL long long
int n,m;
struct P60{
	void dfs(int f,int now){
		printf("%d ",now);
		for(int i=0;i<edge[now].size();i++){
			int to=edge[now][i];
			if(to==f)continue;
			dfs(now,to);
		}
	}
	vector<int>edge[M];
	void solve(){
		for(int a,b,i=1;i<=m;i++){
			scanf("%d%d",&a,&b);
			edge[a].push_back(b);
			edge[b].push_back(a);
		}
		for(int i=1;i<=n;i++)
			sort(edge[i].begin(),edge[i].end());
		dfs(-1,1);
	}
}p60;
struct P{
	int mark[M],in[M];
	vector<int>edge[M];
	int Q[M],vis[M];
	void Find(){
		int L=0,R=-1;
		for(int i=1;i<=n;i++)
			if(in[i]==1)Q[++R]=i;
		while(L<=R){
			int now=Q[L++];
			vis[now]=1;
			for(int i=0;i<(int)edge[now].size();i++){
				int to=edge[now][i];
				in[to]--;
				if(in[to]==1)Q[++R]=to;
			}
		}
	}
	int Px[M],len,fa[M];
	void dfs(int f,int now){
		fa[now]=f;
		for(int j=0;j<(int)edge[now].size();j++){
			int to=edge[now][j];
			if(to==f)continue;
			dfs(now,to);
		}
	}
	void Print(int f,int now){
		printf("%d ",now);
		for(int i=0;i<edge[now].size();i++){
			int to=edge[now][i];
			if(to==f)continue;
			Print(now,to);
		}
	}
	void FIND(){
		int st=0;
		for(int i=1;!st&&i<=n;i++)
			if(vis[i]){
				for(int j=0;j<(int)edge[i].size();j++){
					int to=edge[i][j];
					if(vis[to])continue;
					dfs(i,to);	
				}
			}
	}
	priority_queue<int>q;
	void solve(){
		for(int a,b,i=1;i<=m;i++){
			scanf("%d%d",&a,&b);
			edge[a].push_back(b);
			edge[b].push_back(a);
			in[a]++,in[b]++;
		}
		while(!q.empty()){
			int now=-q.top();q.pop();
			if(vis[now]){
				printf("%d ",now);
				for(int i=0;i<(int)edge[now].size();i++){
					int to=edge[now][i];
					if(!mark[to]){
						mark[to]=1;
						q.push(-to);
					}
				}
			}else Print(fa[now],now);
		}
	}
}p;
int main(){
	//文件名 数组 内存
	// LL 题目 极限数据
	// MOd(1e9+7)
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout); 
	scanf("%d%d",&n,&m);
	if(m==n-1)p60.solve();
	else p.solve();
	return 0;
}
