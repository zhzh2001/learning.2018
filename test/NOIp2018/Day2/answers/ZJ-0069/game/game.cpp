#include <bits/stdc++.h>
using namespace std;
#define M 100005
#define LL long long
int n,m;
const int Mod=1e9+7;
struct P50{
	void solve(){
		int Ans=12LL*(m-1)%Mod;
		printf("%d\n",Ans);
	}
}p50;
struct P30{
	int Map[10][10],len;
	string S[M];
	void Make(int x,int y,string s){
		s+='0'+Map[x][y];
		if(x==n&&m==y)
			S[++len]=s;
		if(x<n)Make(x+1,y,s);
		if(y<m)Make(x,y+1,s);
	}
	bool check(){len=0;
		Make(1,1,"");
		for(int i=1;i<=len;i++)
			for(int j=i+1;j<=len;j++)
				if(S[j]>S[i])return 0;
		return 1;
	}
	void solve(){
		int tt=n*m,Ans=0;
		for(int i=0;i<1<<tt;i++){
			int x=1,y=1;
			for(int j=0;j<tt;j++){
				if((1<<j)&i)Map[x][y]=1;
				else Map[x][y]=0;
				y++;
				if(y>m)y=1,x++;
			}
			Ans+=check();
		}
		printf("%d\n",Ans);
	}
}p30;
struct P{
	void solve(){
		int Ans=112;
		for(int i=4;i<=m;i++)
			Ans=3LL*Ans%Mod;
		printf("%d\n",Ans);
	}
}p;
int main(){
	//文件名 数组 内存
	// LL 题目 极限数据
	// MOd(1e9+7)
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout); 
	scanf("%d%d",&n,&m);
	if(n<=3&&m<=3)p30.solve();
	else if(n==2)p50.solve();
	else p.solve();
	return 0;
}
