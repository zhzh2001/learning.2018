#include <bits/stdc++.h>
using namespace std;
#define M 100005
#define LL long long
int n,m,Cost[M];
struct Edge{
	int to,nx;
}edge[M<<1];
int h[M],tt;
#define DEG(i,st) for(int i=h[st];i;i=edge[i].nx)
void link(int a,int b){
	edge[++tt].to=b;
	edge[tt].nx=h[a];
	h[a]=tt;
}
char S[M];
template<class T>void tomin(T &x,T y){
	if(x>y)x=y;
}
struct P50{
	LL dp[M][2];
	int a,b,x,y;
	void dfs(int f,int now){
		if(now==a)dp[now][x]=x*Cost[now];
		else if(now==b)dp[now][y]=y*Cost[now];
		else dp[now][0]=0,dp[now][1]=Cost[now];
		DEG(i,now){
			int to=edge[i].to;
			if(to==f)continue;
			dfs(now,to);
			dp[now][0]+=dp[to][1];
			dp[now][1]+=min(dp[to][1],dp[to][0]);
		}
	}
	void solve(){
		while(m--){
			scanf("%d%d%d%d",&a,&x,&b,&y);
			for(int i=1;i<=n;i++)
				dp[i][0]=dp[i][1]=1e9;
			dfs(-1,1);
			LL Ans=min(dp[1][0],dp[1][1]);
			if(Ans>=1e9)Ans=-1;
			printf("%lld\n",Ans);
		}
	}
}p50;
struct PL{
	struct node{
		int l,r;
		LL lo,ro,ls,rs;// 00 01 10 11
		void Print(){
			printf("00=%lld 01=%lld 10=%lld 11=%lld\n",lo,ro,ls,rs);
		}
	}tree[M<<2];
	#define fa tree[p]
	#define lson tree[p<<1]
	#define rson tree[p<<1|1]
	node Up(node l,node r){
		node res;
		res.l=l.l,res.r=r.r;
		res.lo=min(l.ro+min(r.lo,r.ls),l.lo+r.ls);
		res.ro=min(r.ro+l.ro,r.rs+min(l.lo,l.ro));
		res.ls=min(l.ls+r.ls,l.rs+min(r.lo,r.ls));
		res.rs=min(l.ls+r.rs,l.rs+min(r.ro,r.rs));
		return res;
	}
	void build(int L,int R,int p){
		fa.l=L,fa.r=R;
		if(L==R){
			fa.ro=fa.ls=1e11;
			fa.lo=0;fa.rs=Cost[L];
			return;
		}
		int mid=L+R>>1;
		build(L,mid,p<<1);
		build(mid+1,R,p<<1|1);
		fa=Up(lson,rson);
	}
	node Query(int L,int R,int p){
		if(fa.l==L&&fa.r==R)return fa;
		int mid=fa.l+fa.r>>1;
		if(R<=mid)return Query(L,R,p<<1);
		else if(L>mid)return Query(L,R,p<<1|1);
		else return Up(Query(L,mid,p<<1),Query(mid+1,R,p<<1|1));
	}
	#undef fa
	#undef lson
	#undef rson
	void Change(int a,int x,node &A){
		if(!x)A.lo=0,A.ro=A.ls=A.rs=1e11;
		else A.rs=Cost[a],A.lo=A.ls=A.ro=1e11;
		A.l=a,A.r=a;
	}
	void solve(){
		build(1,n,1);
		while(m--){
			int a,b,x,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(a>b)swap(a,b),swap(x,y);
			if(a==b-1&&!x&&!y){puts("-1");continue;}
			node A,B;
			Change(a,x,A);
			Change(b,y,B);
			if(a!=1)A=Up(Query(1,a-1,1),A);
			if(a+1<=b-1)B=Up(Query(a+1,b-1,1),B);
			A=Up(A,B);
			if(b!=n)A=Up(A,Query(b+1,n,1));
			LL Ans=min(min(A.ls,A.lo),min(A.rs,A.ro));
			printf("%lld\n",Ans);
		}
	}
}pl;
struct P{
	LL dp[M][2];
	int a,b,x,y,fa[M];
	void dfs(int f,int now){fa[now]=f;
		dp[now][0]=0,dp[now][1]=Cost[now];
		DEG(i,now){
			int to=edge[i].to;
			if(to==f)continue;
			dfs(now,to);
			dp[now][0]+=dp[to][1];
			dp[now][1]+=min(dp[to][1],dp[to][0]);
		}
	}
	struct Node{
		int x;
		LL a,b;
	}Q[M];
	int len;
	LL Up(){
		if(fa[a]==b||fa[b]==a)
			if(!x&&!y)return -1;
		len=0;
		Q[++len]=(Node){a,dp[a][0],dp[a][1]};
		int last=a;
		for(int i=fa[a];i;i=fa[i]){
			Q[++len]=(Node){i,dp[i][0],dp[i][1]};
			dp[i][0]-=Q[len-1].b;
			dp[i][1]-=min(Q[len-1].a,Q[len-1].b);
			last=i;
		}
		Q[++len]=(Node){b,dp[b][0],dp[b][1]};
		last=b;
		for(int i=fa[b];i;i=fa[i]){
			Q[++len]=(Node){i,dp[i][0],dp[i][1]};
			dp[i][0]-=Q[len-1].b;
			dp[i][1]-=min(Q[len-1].b,Q[len-1].a);
			last=i;
		}
		if(x)dp[a][0]=1e11;
		else dp[a][1]=1e11;
		if(y)dp[b][0]=1e11;
		else dp[b][1]=1e11;
		last=a;
		for(int i=fa[a];i;i=fa[i])
			dp[i][0]+=dp[last][1],
			dp[i][1]+=min(dp[last][1],dp[last][0]),
			last=i;
		last=b;
		for(int i=fa[b];i;i=fa[i])
			dp[i][0]+=dp[last][1],
			dp[i][1]+=min(dp[last][1],dp[last][0]),
			last=i;
		LL Ans=min(dp[1][0],dp[1][1]);
		for(int i=1;i<=len;i++){
			int id=Q[i].x;
			dp[id][0]=Q[i].a;
			dp[id][1]=Q[i].b;
		}
		return Ans;
	}
	void solve(){
		for(int i=1;i<=n;i++)dp[i][0]=dp[i][1]=1e11;
		dfs(0,1);
		while(m--){
			scanf("%d%d%d%d",&a,&x,&b,&y);
			printf("%lld\n",Up());
		}
	}
}p;
int main(){
	//文件名 数组 内存
	// LL 题目 极限数据
	// MOd(1e9+7)
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout); 
	scanf("%d%d%s",&n,&m,S);
	for(int i=1;i<=n;i++)
		scanf("%d",&Cost[i]);
	for(int a,b,i=1;i<n;i++){
		scanf("%d%d",&a,&b);
		link(a,b);
		link(b,a);
	}
	if(n<=1000&&m<=1000)p50.solve();
	else if(S[0]=='A')pl.solve();
	else p.solve();
	return 0;
}
