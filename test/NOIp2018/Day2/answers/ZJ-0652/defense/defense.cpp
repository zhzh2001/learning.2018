#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
int cost[5010];
struct Edge
{
	int to;
	int nxt;
} edge[5010];
int head[5010],cnt=0;
void add(int x,int y)
{
	edge[++cnt]=(Edge){y,head[x]};
	head[x]=cnt;
}
int fa[5010];
bool vis[5010]={false},dis[5010]={false};
void find(int x)
{
	vis[x]=true;
	bool t=true;
	for (int i=head[x];i;i=edge[i].nxt)
	{
		int v=edge[i].to;
		if (vis[v]==false)
		{
			fa[v]=x;
			t=false;
			find(v);
		}
	}
	if (t==true) dis[x]=true;
}
int f[5010][3],xx,yy,zz,uu,k[5010];
void dfs(int x)
{
	if (x==0) return;
	int u=fa[x];
	if (k[x]!=1) if (f[u][2]<f[x][1]+cost[u]) f[u][2]=f[x][1]+cost[u];
	if (k[x]!=0) if (f[u][1]<f[x][2]) f[u][1]=f[x][2];
	dfs(u);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int a,b;
	string s;
	cin >> a >> b >> s;
	for (int i=1;i<=a;i++)
	scanf("%d",&cost[i]);
	for (int i=1;i<=a-1;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	fa[1]=0;
	find(1);
	for (int i=1;i<=b;i++)
	{
		scanf("%d%d%d%d",&xx,&yy,&zz,&uu);
		f[0][1]=f[0][2]=0;
		for (int j=1;j<=a;j++)
		{
			f[j][1]=0;
			f[j][2]=0;
			k[j]=-1;
		}
		k[xx]=yy; k[zz]=uu;
		for (int j=1;j<=a;j++)
		if (dis[j]==true) 
		{
		    f[j][1]=0;
		    f[j][2]=cost[j];
		    dfs(j);
		}
		//for (int i=1;i<=a;i++)
		//cout << f[0][1] << " " << f[0][2] << endl;
		if (f[0][1]==0 && f[0][2]==0) cout <<-1; else
		if (f[0][1]==0) cout << f[0][2]; else
		if (f[0][2]==0) cout << f[0][1]; else
	    if (f[0][1]>f[0][2]) cout << f[0][2]; else cout << f[0][1];
		cout << endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
