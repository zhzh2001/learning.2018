#include<iostream>
#include<cstdio>
#include<queue>
#include<cstring>
using namespace std;
int a,b,edge[5010][5010]={false};
int w[5010]={0};
bool v[5010]={false},vis[5010],temp=false;
int ans[5010],sum=0;
int min=999999;
priority_queue<int ,vector<int> , greater<int> > q;
void find(int x)
{
	v[x]=true;
	vis[x]=false;
	for (int i=1;i<=a;i++)
	if (edge[x][i]==true && vis[i]==true) if (w[i]-1==1) find(i);
}
void dfs(int x)
{
	if (vis[x]==false) return;
	vis[x]=false;
	ans[++sum]=x;
	if (temp==false || v[x]==true)
	{
	    for (int i=1;i<=a;i++)
	    if (edge[x][i]==true && vis[i]==true) dfs(i); 
	}
	if (temp==true)
	{
		//int u=q.top();
		bool t=true;
		for (int i=1;i<=a;i++)
		if (edge[x][i]==true && vis[i]==true) 
		q.push(i);
		int i=1;
		while (i<=a)
		{
		    if (edge[x][i]==true && vis[i]==true) 
	     	{
		    	int u=q.top();
		    	if (i>q.top())
		    	{
		    		temp=false;
	    			dfs(u);
		    	}
		    	if (u==i) if (!q.empty())q.pop();
		    	dfs(i);
	    	} 
	    	i++;
	    }
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin >> a >> b;
	for (int i=1;i<=b;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		edge[x][y]=edge[y][x]=true;
		w[x]++; w[y]++;
	}
	memset(vis,true,sizeof(vis));
	for (int i=2;i<=a;i++)
	if (w[i]==1) find(i);
    memset(vis,true,sizeof(vis));
    if (b==a) temp=true;
    //for (int i=1;i<=a;i++)
    //{
    //	for (int j=1;j<=a;j++)
    //	cout << edge[i][j] << " ";
    //	cout << endl;
	//}
    dfs(1);
    for (int i=1;i<=sum;i++)
    cout << ans[i] << " ";
    fclose(stdin);
	fclose(stdout);
	return 0;
}
