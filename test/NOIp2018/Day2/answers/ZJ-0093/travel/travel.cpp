#include <bits/stdc++.h>
using namespace std;
#define rep(i,j,k) for(i=j;i<=k;++i)
#define dow(i,j,k) for(i=j;i>=k;--i)
const int N=5e3+10;
vector<int>G[N];
int n,m,p1,p2,ans[N],pr[N];
bool vis[N],op[N];
struct eg{
	int u,v;
}e[N<<1];
void dfs(int u,int fa){
	int i;pr[++pr[0]]=u;if(vis[u])return;vis[u]=1;
	rep(i,0,((int)G[u].size()-1)){
		int v=G[u][i];
		if(u==p1 && v==p2)continue;
		if(u==p2 && v==p1)continue;
		if(v!=fa)dfs(v,u);
	}
}
int main(){
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);int i,k,j;
	rep(i,1,m){
		int x,y;scanf("%d%d",&x,&y);G[x].push_back(y);G[y].push_back(x);e[i]=(eg){x,y};
	}
	rep(i,1,n)sort(G[i].begin(),G[i].end());
	if(m==n-1){
		dfs(1,0);rep(i,1,n)printf("%d ",pr[i]);fclose(stdin);fclose(stdout);
		return 0;
	}
	rep(i,1,m){
		p1=e[i].u;p2=e[i].v;pr[0]=0;memset(vis,0,sizeof(vis));
		dfs(1,0);
		rep(j,1,n){
			if(ans[j]==0 || ans[j]>pr[j]){
				rep(k,1,n)ans[k]=pr[k];break;
			}
			if(ans[j]<pr[j])break;
		}
	}
	rep(i,1,n)printf("%d ",ans[i]);
	fclose(stdin);fclose(stdout);return 0;
}
