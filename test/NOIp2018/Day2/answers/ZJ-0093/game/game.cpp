#include <bits/stdc++.h>
using namespace std;
#define rep(i,j,k) for(i=j;i<=k;++i)
#define dow(i,j,k) for(i=j;i>=k;--i)
#define LL long long
const LL md=1e9+7;
const int M=1e6+10;
int n,m;
LL f1[(1<<9)+10],f2[(1<<9)+10],pw;
bool jg(int x,int y){
	int i=2,cnt=1;
	while(cnt<=n-1){
		if(((x&i)==0) && (y&(i>>1)))return 0;
		i<<=1;++cnt;
	}
	return 1;
}
void upd(int u){
	int i;
	rep(i,0,((1<<n)-1))
		if(jg(i,u))
			f2[u]+=f1[i],f2[u]%=md;
}
int main(){freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);int i;
	if(n==3){
		if(m==3)printf("112");
		else if(m==2)printf("36");
		else if(m==1)printf("7");
		fclose(stdin);fclose(stdout);
		return 0;
	}
	rep(i,0,((1<<n)-1))f1[i]=1;--m;
	while(m--){
		rep(i,0,((1<<n)-1))f2[i]=0;
		rep(i,0,((1<<n)-1))upd(i);
		rep(i,0,((1<<n)-1))f1[i]=f2[i];
	}
	LL ans=0;
	rep(i,0,((1<<n)-1))ans+=f1[i],ans%=md;
	pw=1;int cnt=n+m-1;
	while(cnt--)pw*=2,pw%=md;
	printf("%lld",(ans+0)%md);
	fclose(stdin);fclose(stdout);
	return 0;
}
