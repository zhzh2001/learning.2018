#include<cstdio>
#include<algorithm>
const int N=101000;
using namespace std;
int n,m,w[N],ord[N];
char s[10];
int f[N][2];
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for(int i=1;i<=n;i++)
		scanf("%d",&w[i]);
	int a,b,x,y;
	if(s[0]=='A'){
		for(int i=1;i<n;i++) scanf("%d%d",&x,&y);
		while(m--){
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if(x==0&&y==0&&(a==b+1||a==b-1)){
				puts("-1");
				continue;
			}
			ord[a]=x+1,ord[b]=y+1;
			f[1][0]=0; f[1][1]=w[1];
			if(ord[1]==1) f[1][1]=2e9;
			if(ord[1]==2) f[1][0]=2e9;
			for(int i=2;i<=n;i++){
				f[i][0]=f[i-1][1];
				f[i][1]=min(f[i-1][0],f[i-1][1])+w[i];
				if(ord[i]==1) f[i][1]=2e9;
				if(ord[i]==2) f[i][0]=2e9;
			}
			printf("%d\n",min(f[n][1],f[n][0]));
			ord[a]=0,ord[b]=0;
		}
		return 0;
	}
	return 0;
}
