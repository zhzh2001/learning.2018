#include<cstdio>
#include<vector>
#include<algorithm>
const int N=5100;
using namespace std;
vector<int>e[N];
int n,m,cir[N],fa[N],flg,fir,firnxt,vis[N];
void dfs1(int u,int ff){
	if(flg) return;
	if(fa[u]){
		cir[u]=1;
		fir=u;
		int v=ff;
		while(v!=u) cir[v]=1,v=fa[v]; 
		flg=1;
		return;
	}
	fa[u]=ff; 
	for(int i=0;i<e[u].size();i++)
		if(e[u][i]!=ff) dfs1(e[u][i],u);
}
void dfs2(int u,int ff){
	if(vis[u]) return;
	sort(e[u].begin(),e[u].end());
	vis[u]=1;
	printf("%d ",u);
	for(int i=0;i<e[u].size();i++)
		if(e[u][i]!=ff) dfs2(e[u][i],u);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1,x,y;i<=m;i++){
		scanf("%d%d",&x,&y);
		e[x].push_back(y);
		e[y].push_back(x);
	}
	dfs1(1,0);
	dfs2(1,0);
	return 0;
}
