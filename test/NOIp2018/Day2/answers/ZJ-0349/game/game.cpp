#include<cstdio>
const int p=1e9+7;
using namespace std;
int n,m,f[10][1010000],ans;
inline int check(const int &l,const int &r){
	for(int i=1;i<n;i++)
		if((r&(1<<i-1))&&!(l&(1<<i))) return 0;
	return 1;
}
inline int md(int x){
	if(x>0) return x%p;
	return x%p+p;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==3&&m==3) return printf("%d\n",112),0;
	for(int i=0;i<(1<<n);i++) f[i][1]=1;
		for(int i=1;i<m;i++)
			for(int j=0;j<(1<<n);j++){
				if(!f[j][i]) continue;
				for(int k=0;k<(1<<n);k++)
					if(check(j,k)){
						f[k][i+1]+=f[j][i];
						if(f[k][i+1]>=p) f[k][i+1]-=p;
					}
			}
		for(int i=0;i<(1<<n);i++){
			ans+=f[i][m];
			if(ans>=p) ans-=p;
		}
	printf("%d\n",ans);
	return 0;
}
