#include <cstdio>
#include <cstring>
#include <algorithm>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
#define per(i, a, b) for(int i = (a); i >= (b); i--)
#define N 1005
#define ll long long
using namespace std;

int n, m, u, v, a, b, x, y, cp;
int p[20], ans;
int from[50], to[50];
char str[10];
bool lk[20][20], vis[20];

void check()
{
	rep(i, 1, cp)
	{
		if(!vis[from[i]] && !vis[to[i]])
			return;
	}
	int sum = 0;
	rep(i, 1, n) if(vis[i])
		sum += p[i];
	if(sum < ans) ans = sum;
}

void dfs(int x)
{
	if(x == (n + 1))
	{
		check();
		return;
	}
	if(x == a || x == b) dfs(x + 1);
	else 
	{
		vis[x] = true;
		dfs(x + 1);
		vis[x] = false;
		dfs(x + 1);
	}
}

int solve()
{
	ans = (1<<30);
	dfs(1);
	if(ans == (1 << 30)) return -1;
	else return ans;
}

void init()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
}

int main()
{
	init();
	scanf("%d%d%s", &n, &m, str);
	rep(i, 1, n)
		scanf("%d", p + i);
	rep(i, 1, n - 1)
	{
		scanf("%d%d", &u, &v);
		lk[u][v] = lk[v][u] = true;
		from[++cp] = u; to[cp] = v;
		from[++cp] = v; to[cp] = u;
	}
	rep(i, 1, m)
	{
		scanf("%d%d%d%d", &a, &x, &b, &y);
		vis[a] = x; vis[b] = y;
		printf("%d\n", solve());
		vis[a] = vis[b] = 0;
	}
	return 0;
}
