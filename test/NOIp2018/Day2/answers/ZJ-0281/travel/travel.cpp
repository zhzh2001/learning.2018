#include <cstdio>
#include <cstring>
#include <algorithm>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
#define per(i, a, b) for(int i = (a); i >= (b); i--)
#define N 10010
using namespace std;

int n, m, u, v, cp, cnt;
int nxt[N], head[N], to[N], stk[N], fa[N];
bool vis[N], in_circle[N], flag, avail[N], mst[N];

void add(int u, int v)
{
	to[++cp] = v;
	nxt[cp] = head[u];
	head[u] = cp;
}

void find_circle(int u, int fa)
{
	stk[++cnt] = u;
	vis[u] = true;
	for(int i = head[u]; i != -1; i = nxt[i]) if(to[i] != fa)
	{
		if(flag) break;
		if(vis[to[i]])
		{
			while(stk[cnt] != to[i])
				in_circle[stk[cnt--]] = true;
			in_circle[to[i]] = true;
			flag = true;
			break;
		}
		find_circle(to[i], u);
	}
	cnt--;
}

void dfs(int u, int fa)
{
	printf("%d ", u);
	int l = cnt, r;
	for(int i = head[u]; i != -1; i = nxt[i])
	if(to[i] != fa) {
		stk[++cnt] = to[i];
	}
	sort(stk + l + 1, stk + cnt + 1);
	r = cnt;
	rep(i, l + 1, r) dfs(stk[i], u);
	cnt--;
}

void init()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
}

int main()
{
	init();
	cp = 1;
	memset(head, -1, sizeof(head));
	scanf("%d%d", &n, &m);
	rep(i, 1, m)
	{
		scanf("%d%d", &u, &v);
		add(u, v);
		add(v, u);
	}
	cnt = 0;
	memset(vis, 0, sizeof(vis));
	memset(stk, 0, sizeof(stk));
	dfs(1, -1);
	printf("\n");
	return 0;
}
