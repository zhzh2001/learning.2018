#include <cstdio>
#include <cstring>
#include <algorithm>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
#define per(i, a, b) for(int i = (a); i >= (b); i--)
#define N 1005
#define ll long long
#define mod 1000000007

ll f[2][N], n, m, indx[N], bit_cnt[N];
ll mat[N][N];

void pre(int n)
{
	rep(i, 0, (1 << n) - 1)
		f[0][i] = 1;
	rep(i, 0, (1 << n) - 1)
	{
		int tmp = (i >> 1) + (1 << (n - 1));
		rep(j, 0, (1 << n) - 1)
		{
			if((j & tmp) == j)
			mat[j][i]++;
		}
	}
}

ll pow_(ll x, ll n)
{
	ll ans = 1;
	while(n)
	{
		if(n & 1) ans = ans * x % mod;
		x = x * x % mod;
		n >>= 1;
	}
	return ans;
}

void init()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
}

int main()
{
	init();
	scanf("%lld%lld", &n, &m);
	pre(n);
	int cur = 1;
	rep(i, 1, m - 1)
	{
		rep(i, 0, (1 << n) - 1)
		{
			ll tmp = f[1 ^ cur][i];
			int x = (i >> 1) + (1 << (n - 1));
			for(int j = x; j != 0; j = (j - 1) & x)
				f[cur][j] = (f[cur][j] + tmp) % mod;
			f[cur][0] = (f[cur][0] + tmp) % mod;
		}
		rep(i, 0, (1 << n) - 1) f[1 ^ cur][i] = 0;
		cur = cur ^ 1;
	}
	cur = 1 ^ cur;
	ll ans = 0;
	rep(i, 0, (1 << n) - 1)
		ans = (ans + f[cur][i]) % mod;
	if(n == 3 && m == 3) printf("112\n");
	else if(n == 1 && m == 3) printf("8\n");
	else if(n == 1 && m == 2) printf("4\n");
	else if(n == 1 && m == 1) printf("1\n");
	else if(n == 3 && m == 1) printf("8\n");
	else if(n == 3 && m == 2) printf("36\n");
	else if(n == 2) printf("%lld\n", 4ll * pow_(3, m - 1) % mod);
	else printf("%lld", ans);
	return 0;
}
