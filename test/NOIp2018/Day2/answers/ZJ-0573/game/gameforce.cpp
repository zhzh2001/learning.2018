#include<bits/stdc++.h>
using namespace std;
int n,m,lst,flag,ans;
int a[10][10];
bool check(int x,int y){
	return x>=1&&x<=n&&y>=1&&y<=m;
}
void dfs(int x,int y,int sum){
	if(!flag) return;
	if(x==n&&y==m){
		if(sum<lst) flag=0;
		lst=sum;
		//if(ans==2) printf("%d\n",sum);
		return;
	}
	if(check(x,y+1)){
		dfs(x,y+1,(sum<<1)+a[x][y+1]);
	}
	if(check(x+1,y)){
		dfs(x+1,y,(sum<<1)+a[x+1][y]);
	}
}
int main(){
	int i,j,x,y,t;
	scanf("%d %d",&n,&m);
	ans=0;
	for(i=0;i<=(1<<n*m)-1;i++){
		for(j=1;j<=n*m;j++){
			x=(j-1)/m+1,y=(j-1)%m+1;
			t=((1<<(j-1))&i)>>(j-1);
			a[x][y]=t;
		}
		//for(j=1;j<=n;j++){
		//	for(int k=1;k<=m;k++) printf("%d ",a[j][k]);
		//	cout<<endl;
		//}
		lst=0,flag=1;
		dfs(1,1,a[1][1]);
		if(flag) ans++;//,printf("!\n");
	}
	printf("%d",ans);
	return 0;
}
