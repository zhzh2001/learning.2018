#include<bits/stdc++.h>
#define ll long long
using namespace std;
struct ed{
	int nxt,to;
}e[200005];
int h[100005],cnt;
int n,m,dx,tx,dy,ty;
int p[100005];
ll f[100005][2],inf;
char s[20];
void add(int from,int to){
	e[++cnt].nxt=h[from],e[cnt].to=to,h[from]=cnt;
}
void dfs(int x,int fa){
	int i,y;
	if(x==dy) f[x][1]=ty?p[x]:inf,f[x][0]=ty?inf:0;
	else f[x][1]=p[x],f[x][0]=0;
	for(i=h[x];i;i=e[i].nxt){
		y=e[i].to;
		if(y==fa) continue;
		dfs(y,x);
		if(x==dy){
			if(ty) f[x][1]+=min(f[y][0],f[y][1]);
			else f[x][0]+=f[y][1];
		}
		else{
			f[x][1]+=min(f[y][0],f[y][1]);
			f[x][0]+=f[y][1];
		}
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int i,x,y;
	scanf("%d %d",&n,&m);
	scanf("%s",s),inf=10000000000;
	for(i=1;i<=n;i++) scanf("%d",&p[i]);
	memset(h,0,sizeof(h)),cnt=0;
	for(i=1;i<n;i++){
		scanf("%d %d",&x,&y);
		add(x,y),add(y,x);
	}
	for(i=1;i<=m;i++){
		scanf("%d %d %d %d",&dx,&tx,&dy,&ty);
		dfs(dx,0);
		if(f[dx][tx]>=inf) printf("-1\n");
		else printf("%lld\n",f[dx][tx]);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*
5 3 C3
2 4 1 3 9
1 5
5 2
5 3
3 4
1 0 3 0
2 1 3 1
1 0 5 0

*/
