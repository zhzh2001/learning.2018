#include<bits/stdc++.h>
using namespace std;
struct ed{
	int nxt,to;
}e[10005];
int h[5005],cnt;
int n,m,tot,dx,dy;
int a[5005],b[5005];
bool ee[5005][5005],v[5005];
int l[5005],q[5005];
void add(int from,int to){
	e[++cnt].nxt=h[from],e[cnt].to=to,h[from]=cnt;
}
void dfs(int x){
	int i,y;
	q[++tot]=x,v[x]=1;
	for(i=h[x];i;i=e[i].nxt){
		y=e[i].to;
		if(v[y]||(x==dx&&y==dy)||(x==dy&&y==dx)) continue;
		dfs(y);
	}
	v[x]=0;
}
bool cmp(){
	int i;
	for(i=1;i<=n;i++){
		if(q[i]<l[i]) return true;
		if(l[i]<q[i]) return false;
	}
	return false;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int i,j;
	scanf("%d %d",&n,&m);
	memset(h,0,sizeof(h)),cnt=0;
	memset(ee,0,sizeof(ee)),memset(v,0,sizeof(v));
	for(i=1;i<=m;i++){
		scanf("%d %d",&a[i],&b[i]);
		ee[a[i]][b[i]]=ee[b[i]][a[i]]=1;
	}
	for(i=1;i<=n;i++) for(j=n;j>=1;j--) if(ee[i][j]) add(i,j);
	if(m==(n-1)){
		tot=dx=dy=0;
		dfs(1);
		for(i=1;i<=n;i++) printf("%d ",q[i]);
		return 0;
	}
	l[1]=n+1;
	for(i=1;i<=m;i++){
		dx=a[i],dy=b[i],tot=0;
		dfs(1);
		if(tot<n) continue;
		if(cmp()) memcpy(l,q,sizeof(q));
	}
	for(i=1;i<=n;i++) printf("%d ",l[i]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*
6 5
1 3
2 3
2 5
3 4
4 6

6 6
1 3
2 3
2 5
3 4
4 5
4 6

*/
