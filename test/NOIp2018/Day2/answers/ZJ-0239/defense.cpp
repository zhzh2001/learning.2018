#include<cstdio>
#include<cstring>

inline int getint()
{
	register int i1,i2;
	for(i2=getchar();i2<'0'||i2>'9';i2=getchar());
	i1=(i2-'0');
	for(i2=getchar();i2>='0'&&i2<='9';i2=getchar())i1=((i1<<3)+(i1<<1)+i2-'0');
	return i1;
}

int n,m,typea,type1,u,v,x,y,i,j,a[100005],first[100005],nxt[200005],e[200005],tope;

inline void link(int u0,int v0)
{
	++tope;
	e[tope]=v0;
	nxt[tope]=first[u0];
	first[u0]=tope;
	return;
}
int fa[100005],q[100005],lq,rq;
long long f0[100005],f1[100005],ans;
inline void work(int u0,int v0,int x0,int x1)
{
	int i1;
	long long i2;
	memset(fa,0,sizeof(fa));
	q[rq=1]=u0;
	for(lq=1;lq<=rq;++lq)
	{
		for(i1=first[q[lq]];i1;i1=nxt[i1])
		{
			if(e[i1]==fa[q[lq]])continue;
			fa[e[i1]]=q[lq];
			q[++rq]=e[i1];
		}
	}
	if((fa[v0]==u0)&&(!x1)&&(!x0))
	{
		ans=-1;
		return;
	}
	for(lq=rq;lq>=1;--lq)
	{
//		f2[q[lq]]=a[q[lq]];
		f1[q[lq]]=a[q[lq]];
		f0[q[lq]]=0;
		i2=98765432123456ll;
		for(i1=first[q[lq]];i1;i1=nxt[i1])
		{
			if(e[i1]==fa[q[lq]])continue;
			f1[q[lq]]+=f0[e[i1]];
			f0[q[lq]]+=f1[e[i1]];
		}
		if(lq==1)break;
		if(q[lq]==v0)
		{
			if(x1)f0[v0]=f1[v0];
			else f1[v0]=987654321234ll;
		}
		if(f1[q[lq]]<f0[q[lq]])f0[q[lq]]=f1[q[lq]];
	}
	if(x0)ans=f1[u0];
	else ans=f0[u0];
	if(ans>1e11)ans=-1;
	return;
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=getint();m=getint();
	for(typea=getchar();typea<'A'||typea>'Z';typea=getchar());
	type1=getchar();
	for(i=1;i<=n;++i)a[i]=getint();
	memset(first,0,sizeof(first));
	tope=0;
	for(i=1;i<n;++i)
	{
		u=getint();v=getint();
		link(u,v);link(v,u);
	}
	for(i=1;i<=m;++i)
	{
		u=getint();x=getint();v=getint();y=getint();
		work(u,v,x,y);
		printf("%lld\n",ans);
	}
	return 0;
}
