#include<cstdio>
#include<cstring>

inline int getint()
{
	register int i1,i2;
	for(i2=getchar();i2<'0'||i2>'9';i2=getchar());
	i1=(i2-'0');
	for(i2=getchar();i2>='0'&&i2<='9';i2=getchar())i1=((i1<<3)+(i1<<1)+i2-'0');
	return i1;
}

int n,m,i,j,u[10004],v[10004],lv[5003],rv[5003];
int tu[10004],tv[10004];

inline void sortuv(const int l0,const int r0)
{
	if(l0>=r0)return;
	int i1,i2,i3,i4=l0-1;
	sortuv(l0,i3=((l0+r0)>>1));
	sortuv(i2=(i3+1),r0);
	for(i1=l0;i1<=i3;++i1)
	{
		while((i2<=r0)&&((u[i2]<u[i1])||((u[i2]==u[i1])&&(v[i2]<v[i1]))))
		{
			++i4;
			tu[i4]=u[i2];
			tv[i4]=v[i2];
			++i2;
		}
		++i4;
		tu[i4]=u[i1];
		tv[i4]=v[i1];
	}
	for(i1=l0;i1<=i4;++i1)
	{
		u[i1]=tu[i1];
		v[i1]=tv[i1];
	}
	return;
}

int dfn[5003],dfnum,dfv[5003],ans[5003];

inline void dfs1(int v0)
{
	dfn[v0]=(++dfnum);
	ans[dfnum]=v0;
	int i1;
	for(i1=rv[v0-1]+1;i1<=rv[v0];++i1)if(!dfn[v[i1]])dfs1(v[i1]);
	return;
}

int fa[5003],cir[5003],hc,lc,banu,banv;
inline void findcirc(int v0)
{
	dfn[v0]=(++dfnum);
	int i1;
	for(i1=rv[v0-1]+1;i1<=rv[v0];++i1)
	{
		if(v[i1]==fa[v0])continue;
		if(dfn[v[i1]])
		{
			//if(hc&&(dfn[v[i1]]<dfn[v0]))puts("error1");
			if(dfn[v[i1]]<dfn[v0])
			{
				hc=v[i1];
				lc=v0;
			}
		}else
		{
			fa[v[i1]]=v0;
			findcirc(v[i1]);
		}
	}
	return;
}

inline void dfs2(int v0)
{
	dfn[v0]=(++dfnum);
	dfv[dfnum]=v0;
	int i1;
	for(i1=rv[v0-1]+1;i1<=rv[v0];++i1)
	{
		if((!dfn[v[i1]])&&(v0!=banu||v[i1]!=banv)&&(v0!=banv||v[i1]!=banu))dfs2(v[i1]);
	}
	return;
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=getint();m=getint();
	for(i=1;i<=m;++i)
	{
		u[i]=getint();v[i]=getint();
		u[i+m]=v[i];v[i+m]=u[i];
	}
	sortuv(1,m<<1);
	memset(lv,0,sizeof(lv));
	memset(rv,0,sizeof(rv));
	j=(m<<1);
	lv[n+1]=rv[n+1]=j+1;
	lv[0]=rv[0]=0;
	for(i=n;i>=1;--i)
	{
		while((j>0)&&(u[j]==i))--j;
		lv[i]=j+1;
		rv[i]=lv[i+1]-1;
	}
	if(m<n)
	{
		memset(dfn,0,sizeof(dfn));
		dfnum=0;
		dfs1(1);
	}else
	{
//		memset(dfn,0,sizeof(dfn));
//		dfnum=0;
//		dfs1(1);
		memset(dfn,0,sizeof(dfn));
		dfnum=0;
		memset(fa,0,sizeof(fa));
		hc=lc=0;
		findcirc(1);
		banu=lc;banv=hc;
		memset(dfn,0,sizeof(dfn));
		dfnum=0;
		dfs2(1);
		for(j=1;j<=n;++j)ans[j]=dfv[j];
		for(i=lc;i!=hc;i=fa[i])
		{
			banu=i;banv=fa[i];
			memset(dfn,0,sizeof(dfn));
			dfnum=0;
			dfs2(1);
			//for(j=1;j<=n;++j)printf(j<n?"%d ":"%d!\n",dfv[j]);
			for(j=1;(j<=n)&&(ans[j]==dfv[j]);++j);
			if((j<=n)&&(ans[j]>dfv[j]))
			{
				for(j=1;j<=n;++j)ans[j]=dfv[j];
			}
		}
	}
	for(i=1;i<n;++i)printf("%d ",ans[i]);
	printf("%d\n",ans[n]);
	return 0;
}
