#include<cstdio>
#include<cstring>

inline int getint()
{
	register int i1,i2;
	for(i2=getchar();i2<'0'||i2>'9';i2=getchar());
	i1=(i2-'0');
	for(i2=getchar();i2>='0'&&i2<='9';i2=getchar())i1=((i1<<3)+(i1<<1)+i2-'0');
	return i1;
}

const int moo=1000000007;
int n,n2,m,i,j,t1,t2;
long long ans;
int f[302][9],f2[302][9];
int s1[21],s2[21];
inline void work(int x0,int x1,int x2)
{
	int i1,i2,i3,i4;
	i2=x0;
	for(i1=1;i1<=n;++i1)
	{
		s1[i1]=(i2&1);
		i2>>=1;
	}
	for(i1=0;i1<n2;++i1)
	{
		if((i1^(x0<<1))&((1<<x1)-2))continue;
		i2=i1;
		for(i3=1;i3<=n;++i3)
		{
			s2[i3]=(i2&1);
			i2>>=1;
		}
//		for(i2=2;i2<=x1;++i2)if(s2[i2]!=s1[i2-1])break;
//		if(i2<=x1)continue;
		i4=x1;
		for(i2=x1+1;i2<=n;++i2)
		{
			if(s2[i2]==s1[i2-1])i4=i2-1;
			if(s2[i2]>s1[i2-1])break;
		}
		if(i2<=n)continue;
//		printf("%d %d->%d %d :%d\n",x0,x1,i1,i4,x2);
		f[i1][i4]=(f[i1][i4]+x2)%moo;
	}
	return;
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	n2=(1<<n);
	memset(f,0,sizeof(f));
	for(i=0;i<n2;++i)f[i][1]=1;
	for(j=2;j<=m;++j)
	{
		for(i=0;i<n2;++i)for(t1=1;t1<=n;++t1)f2[i][t1]=f[i][t1];
		memset(f,0,sizeof(f));
		for(i=0;i<n2;++i)
		{
			for(t1=1;t1<=n;++t1)if(f2[i][t1])work(i,t1,f2[i][t1]);
		}
	}
	ans=0;
	for(i=0;i<n2;++i)for(t1=1;t1<=n;++t1)ans+=f[i][t1];
	i=ans%moo;
	printf("%d\n",i);
	return 0;
}
