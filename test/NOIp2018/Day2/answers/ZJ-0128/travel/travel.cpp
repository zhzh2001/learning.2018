#include<bits/stdc++.h>
#define rep(i,x,y) for (int i=(x);i<=(y);i++)
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
const int N=10005;
int n,m,x,y,cnt=1,head[N],now[N],id[N],vis[N],fa[N],dep[N],q[N],top,ans[N],banx,bany,flag;
struct edge{int to,nxt;}e[N<<1]; vector<int> G[N];
void adde(int x,int y){e[++cnt].to=y; e[cnt].nxt=head[x]; head[x]=cnt;}
void find(int u,int par){
	vis[u]=1;
	for (int i=head[u],v;i;i=e[i].nxt)
		if (v=e[i].to,v!=par)
			if (!vis[v]){fa[v]=u; dep[v]=dep[u]+1; id[v]=i; find(v,u);}
			else if (dep[v]<dep[u]){
				q[++top]=i;
				for (int x=u;x!=v;x=fa[x]) q[++top]=id[x];
			}
}
void dfs(int u,int par){
	now[++*now]=u;
	rep (i,0,(int)G[u].size()-1){
		int v=G[u][i]; if (v==par) continue;
		if (!(u==banx&&v==bany||u==bany&&v==banx)) dfs(v,u);
	}
}
void solve1(){
	rep (u,1,n){
		for (int i=head[u];i;i=e[i].nxt) G[u].push_back(e[i].to);
		sort(G[u].begin(),G[u].end());
	}
	*now=0;
	dfs(1,0);
	rep (i,1,n) printf("%d%c",now[i]," \n"[i==n]);
	exit(0);
}
void get_best(int *f,int *g){
	if (!flag){rep (i,1,n) g[i]=f[i]; flag=1; return;}
	bool fy=0;
	rep (i,1,n) if (g[i]!=f[i]){if (f[i]<g[i]) fy=1; break;}
	if (fy) rep (i,1,n) g[i]=f[i];
}
void solve2(){
	find(1,0);//�һ������ 
	rep (u,1,n){
		for (int i=head[u];i;i=e[i].nxt) G[u].push_back(e[i].to);
		sort(G[u].begin(),G[u].end());
	}
	rep (i,1,top){
		banx=e[q[i]].to,bany=e[q[i]^1].to;
		*now=0;
		dfs(1,0);
		get_best(now,ans);
	}
	rep (i,1,n) printf("%d%c",ans[i]," \n"[i==n]);
	exit(0);
}
int main(){
	freopen("travel.in","r",stdin); freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	rep (i,1,m) scanf("%d%d",&x,&y),adde(x,y),adde(y,x);
	if (m==n-1) solve1();
	if (m==n) solve2();
	return 0;
}
