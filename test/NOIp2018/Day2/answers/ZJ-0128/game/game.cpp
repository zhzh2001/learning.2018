#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int mod=1e9+7;
int n,m;
int ksm(int x,int y){
	int s=1;
	for (;y;y>>=1,x=(ll)x*x%mod) if (y&1) s=(ll)s*x%mod;
	return s;
}
int main(){
	freopen("game.in","r",stdin); freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	if (n==1) return printf("%d\n",ksm(2,m)),0;
	if (n==2) return printf("%d\n",4ll*ksm(3,m-1)%mod);
	if (n==3) return printf("%d\n",112ll*ksm(3,m-3)%mod);
	return 0;
}
