#include<bits/stdc++.h>
#define rep(i,x,y) for (int i=(x);i<=(y);i++)
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
#define ll long long
using namespace std;
#define gc getchar
int read(){
	char ch=gc(); int x=0,op=1;
	for (;!isdigit(ch);ch=gc()) if (ch=='-') op=-1;
	for (;isdigit(ch);ch=gc()) x=(x<<1)+(x<<3)+ch-48;
	return x*op;
}
const int N=1e5+5;
const ll inf=1e12;
int n,m,cnt,head[N],a[N]; char typ[10]; ll f[N][2];
struct edge{int to,nxt;}e[N<<1];
void adde(int x,int y){e[++cnt].to=y; e[cnt].nxt=head[x]; head[x]=cnt;}
namespace brute{
	int x,tx,y,ty;
	void dp(int u,int par){
		f[u][0]=f[u][1]=0;
		for (int i=head[u],v;i;i=e[i].nxt)
			if (v=e[i].to,v!=par){
				dp(v,u);
				f[u][0]+=min(f[v][0],f[v][1]);
				f[u][1]+=f[v][0];
			}
		f[u][0]+=a[u];
		if (u==x)
			if (tx) f[u][1]=inf; else f[u][0]=inf;
		if (u==y)
			if (ty) f[u][1]=inf; else f[u][0]=inf;
		f[u][0]=min(f[u][0],inf); f[u][1]=min(f[u][1],inf);
	}
	void solve(){
		while (m--){
			x=read(),tx=read(),y=read(),ty=read();
			dp(1,0);
			ll ans=min(f[1][0],f[1][1]);
			if (ans==inf) puts("-1"); else printf("%lld\n",ans);
		}
		exit(0);
	}
}
namespace brute2{
	int x,tx,y,ty,fa[N],top,stk[N]; ll g[N][2],h[N][2];
	void dp(int u,int par){
		f[u][0]=f[u][1]=0;
		for (int i=head[u],v;i;i=e[i].nxt)
			if (v=e[i].to,v!=par){
				fa[v]=u; dp(v,u);
				f[u][0]+=min(f[v][0],f[v][1]);
				f[u][1]+=f[v][0];
			}
		f[u][0]+=a[u];
		f[u][0]=min(f[u][0],inf); f[u][1]=min(f[u][1],inf);
	}
	void modify(int x,int d,ll v){
		g[x][0]=f[x][0]; g[x][1]=f[x][1]; f[x][d]=v;
		while (fa[x]){
			int y=fa[x];
			g[y][0]=f[y][0]; g[y][1]=f[y][1]; //f[y][0]
			if (f[y][0]!=inf){
				if (min(g[x][0],g[x][1])<inf) f[y][0]-=min(g[x][0],g[x][1]);
				f[y][0]+=min(f[x][0],f[x][1]); f[y][0]=min(f[y][0],inf); //f[y][1]
			}
			if (f[y][1]!=inf){
				if (g[x][0]<inf) f[y][1]-=g[x][0]; f[y][1]+=f[x][0]; f[y][1]=min(f[y][1],inf);
			}
			x=fa[x];
		}
	}
	void get(int x){
		while (x){stk[++top]=x; h[x][0]=f[x][0],h[x][1]=f[x][1]; x=fa[x];}
	}
	void solve(){
		dp(1,0);
		while (m--){
			x=read(),tx=read(),y=read(),ty=read();
			top=0; get(x); get(y);
			modify(x,tx,inf); modify(y,ty,inf);
			ll ans=min(f[1][0],f[1][1]);
			if (ans>=inf) puts("-1"); else printf("%lld\n",ans);
			rep (i,1,top) f[stk[i]][0]=h[stk[i]][0],f[stk[i]][1]=h[stk[i]][1];
		}
		exit(0);
	}
}
int main(){
	freopen("defense.in","r",stdin); freopen("defense.out","w",stdout);
	n=read(),m=read(); scanf("%s",typ);
	rep (i,1,n) a[i]=read();
	rep (i,1,n-1){int x=read(),y=read(); adde(x,y),adde(y,x);}
	if (n<=2000) brute::solve();
	brute2::solve();
	return 0;
}
/*
10 1 C2
1 3 5 9 2 5 2 6 3 1
2 1
3 1
4 1
5 2
6 3
7 5
8 7
9 6
10 7
4 1 9 1
*/
