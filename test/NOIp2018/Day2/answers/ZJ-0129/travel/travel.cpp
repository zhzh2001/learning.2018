#include<bits/stdc++.h>
using namespace std;
const int N = 5050;

int n, m, E;
int u[N], v[N];
vector<int> vec[N];

namespace Subtask1 {

   void dfs(int x, int fa) {
      printf("%d ", x);
      for (int i = 0; i < vec[x].size(); ++i) {
	 int to = vec[x][i];
	 if (to == fa) continue;
	 dfs(to, x);
      }
   }
   
   void main() {
      dfs(1, 0);
      puts("");
   }
}

namespace Subtask2 {
   int len, ans[N], tmp[N], vis[N], ban[N];

   void dfs(int x) {
      vis[x] = 1;
      tmp[++len] = x;
      for (int i = 0; i < vec[x].size(); ++i) {
	 int to = vec[x][i];
	 if (vis[to] || ban[x] == to) continue;
	 dfs(to);
      }
   }

   int check() {
      if (len < n) return 0;
      for (int i = 1; i <= n; ++i) {
	 if (tmp[i] < ans[i]) return 1;
	 if (tmp[i] > ans[i]) return 0;
      }
      return 0;
   }
   
   void main() {
      for (int i = 1; i <= n; ++i)
	 ans[i] = n + 1;
      for (int i = 1; i <= m; ++i) {
	 len = 0;
	 ban[u[i]] = v[i];
	 ban[v[i]] = u[i];
	 memset(vis, 0, sizeof vis);
	 dfs(1);
	 ban[u[i]] = 0;
	 ban[v[i]] = 0;
	 if (check()) {
	    for (int j = 1; j <= n; ++j)
	       ans[j] = tmp[j];
	 }
      }
      for (int i = 1; i <= n; ++i)
	 printf("%d ", ans[i]);
      puts("");
   }
}

int main() {
   freopen("travel.in", "r", stdin);
   freopen("travel.out", "w", stdout);
   scanf("%d%d", &n, &m);
   for (int i = 1; i <= m; ++i) {
      int x, y;
      scanf("%d%d", &x, &y);
      u[i] = x;
      v[i] = y;
      vec[x].push_back(y);
      vec[y].push_back(x);
   }
   for (int i = 1; i <= n; ++i) {
      sort(vec[i].begin(), vec[i].end());
   }
   if (m == n - 1) Subtask1::main();
   else Subtask2::main();
   return 0;
}
