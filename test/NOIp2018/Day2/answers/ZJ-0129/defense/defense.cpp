#include<bits/stdc++.h>
#define LL long long
using namespace std;
const int N = 1e5 + 10;
const LL inf = 0x3f3f3f3f3f3f3f;

char type[10];
int n, m, E;
int qx, qy, qa, qb;
LL a[N], dp[N][2], f[N][2], g[N][2][2][20];
int fa[N], fir[N], nex[N << 1], arr[N << 1];

namespace init {

   inline void Add_Edge(int x, int y) {
      nex[++E] = fir[x];
      fir[x] = E; arr[E] = y;
   }
   
   void dfs(int x) {
      dp[x][0] = 0;
      dp[x][1] = a[x];
      for (int i = fir[x]; i; i = nex[i]) {
	 if (arr[i] == fa[x]) continue;
	 fa[arr[i]] = x;
	 dfs(arr[i]);
	 dp[x][0] += dp[arr[i]][1];
	 dp[x][1] += min(dp[arr[i]][0], dp[arr[i]][1]);
      }
   }
   
   void main() {
      scanf("%d%d%s", &n, &m, type + 1);
      for (int i = 1; i <= n; ++i) {
	 scanf("%lld", &a[i]);
      }
      for (int i = 1; i < n; ++i) {
	 int x, y;
	 scanf("%d%d", &x, &y);
	 Add_Edge(x, y);
	 Add_Edge(y, x);
      }
      dfs(1);
   }
}

namespace Subtask1 {

   void dfs(int x, int fa) {
      dp[x][0] = 0;
      dp[x][1] = a[x];
      for (int i = fir[x]; i; i = nex[i]) {
	 if (arr[i] == fa) continue;
	 dfs(arr[i], x);
	 dp[x][0] += dp[arr[i]][1];
	 dp[x][1] += min(dp[arr[i]][0], dp[arr[i]][1]);
      }
      if (qa == x) {
	 dp[x][qx ^ 1] = inf;
      }
      if (qb == x) {
	 dp[x][qy ^ 1] = inf;
      }
   }
   
   void Solve() {
      dfs(1, 0);
      LL ans = min(dp[1][0], dp[1][1]);
      printf("%lld\n", ans < inf ? ans : -1);
   }
}

namespace Subtask3 {
   void main() {
      f[qb][0] = dp[qb][0];
      f[qb][1] = dp[qb][1];
      f[qb][qy ^ 1] = inf;
      while (fa[qb]) {
	 int x = fa[qb];
	 f[x][0] = dp[x][0];
	 f[x][1] = dp[x][1];
	 f[x][0] -= dp[qb][1];
	 f[x][0] += f[qb][1];
	 f[x][1] -= min(dp[qb][0], dp[qb][1]);
	 f[x][1] += min(f[qb][0], f[qb][1]);
      }
      LL ans = f[1][1];
      printf("%lld\n", ans < inf ? ans : -1);
   }
}

namespace Subtask2 {

   LL mmin(LL x, LL y, LL z) {
      if (y > z) swap(y, z);
      return (x < y) ? x : y;
   }
   LL Solve(int a, int x, int b, int y) {
      LL f0 = 0, f1 = 0;
      if (!(x & 1)) f0 = inf;
      if (!(x & 2)) f1 = inf;
      for (int i = 18; i >= 0; --i) {
	 if (a - (1 << i) < b) continue;
	 LL g0 = inf, g1 = inf;
	 g0 = min(f0 + g[a][0][0][i], f1 + g[a][1][0][i]);
	 g1 = min(f0 + g[a][0][1][i], f1 + g[a][1][1][i]);
	 f0 = g0; f1 = g1;
      }
      if (!(y & 1)) f0 = inf;
      if (!(y & 2)) f1 = inf;
      return min(f0, f1);
   }
   
   void main() {
      for (int i = 1; i <= n; ++i) {
	 g[i][0][0][0] = 0;
	 g[i][1][1][0] = a[i];
	 for (int j = 1; j < 18; ++j) {
	    int ff = i - (1 << j - 1);
	    if (f <= 0) break;
	    LL t0 = g[i][0][0][j - 1];
	    LL t1 = g[i][0][1][j - 1];
	    LL t2 = g[i][1][0][j - 1];
	    LL t3 = g[i][1][1][j - 1];
	    g[i][0][0][j] = mmin(g[i][0][0][j - 1] + g[ff][1][0][j - 1], g[i][0][1][j - 1] + g[ff][0][0][j - 1], g[i][0][1][j - 1] + g[ff][1][0][j - 1]);
	    g[i][0][1][j] = mmin(g[i][0][0][j - 1] + g[ff][1][1][j - 1], g[i][0][1][j - 1] + g[ff][0][1][j - 1], g[i][0][1][j - 1] + g[ff][1][1][j - 1]);
	    g[i][1][0][j] = mmin(g[i][1][0][j - 1] + g[ff][1][0][j - 1], g[i][1][1][j - 1] + g[ff][0][0][j - 1], g[i][1][1][j - 1] + g[ff][1][0][j - 1]);
	    g[i][0][1][j] = mmin(g[i][1][0][j - 1] + g[ff][1][1][j - 1], g[i][1][1][j - 1] + g[ff][0][1][j - 1], g[i][1][1][j - 1] + g[ff][1][1][j - 1]);
	 }
      }
      while (m--) {
	 scanf("%d%d%d%d", &qa, &qx, &qb, &qy);
	 if (qa > qb) {
	    swap(qa, qb);
	    swap(qx, qy);
	 }
	 LL ans = Solve(n, 3, qb, qy) + Solve(qb, qy, qa, qx) + Solve(qa, qx, 0, 3);
	 printf("%lld\n", ans < inf ? ans : -1);
      }
   }
}


int main() {
   freopen("defense.in", "r", stdin);
   freopen("defense.out", "w", stdout);
   init::main();
   while (m--) {
      scanf("%d%d%d%d", &qa, &qx, &qb, &qy);
      if (n <= 2000) Subtask1::Solve();
      else if (type[1] == 'B') Subtask3::main();
   }
   return 0;
}
