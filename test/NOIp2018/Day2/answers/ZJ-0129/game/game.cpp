#include<bits/stdc++.h>
#define LL long long
using namespace std;
const int mod = 1e9 + 7;

int n, m;

LL Qpow(LL x, int p) {
   LL ans = 1;
   while (p) {
      if (p & 1) ans = ans * x % mod;
      x = x * x % mod;
      p >>= 1;
   }
   return ans;
}

int main() {
   freopen("game.in", "r", stdin);
   freopen("game.out", "w", stdout);
   cin >> n >> m;
   if (n > m) swap(n, m);
   if (n == 1) {
      cout << Qpow(2, m) << endl;
      return 0;
   }
   if (n == 2) {
      cout << 4 * Qpow(3, m - 1) % mod << endl;
      return 0;
   }
   if (n == 3) {
      cout << 112 * Qpow(3, m - 3) % mod << endl;
      return 0;
   }
}
