#include<bits/stdc++.h>
#define ll long long
#define pb push_back
#define db double
using namespace std;
bool mm1;
const int N=5010;
int n,m;
struct P1{
	vector<int>E[N];
	int ans[N],r;
	void addedge(int u,int v){
		E[u].pb(v);
		E[v].pb(u);	
	}
	void dfs(int x,int f){
		ans[++r]=x;
		for(int i=0;i<E[x].size();i++){
			int V=E[x][i];
			if(V==f) continue;
			dfs(V,x);
		}
	}
	void Do(){
		for(int i=2;i<=n;i++){
			int a,b;
			scanf("%d %d",&a,&b);
			addedge(a,b);
		}
		for(int i=1;i<=n;i++) sort(E[i].begin(),E[i].end());
		dfs(1,0);
		for(int i=1;i<=r;i++){
			printf("%d",ans[i]);
			if(i!=r) printf(" ");
			else printf("\n");
		}
	}
}Solve1;
struct P2{
	vector<int>E[N];
	int ans[N],r,Q1[N],q1;
	bool vis[N];
	void addedge(int u,int v){
		E[u].pb(v);
		E[v].pb(u);	
	}
	void dfs(int x,int f,int u,int v){
		Q1[++q1]=x;
		for(int i=0;i<E[x].size();i++){
			int V=E[x][i];
			if(V==f) continue;
			if(x==u&&V==v) continue;
			if(x==v&&V==u) continue;
			dfs(V,x,u,v);
		}
	}
	int cnt;
	void Mark(int x,int f,int u,int v){
		if(vis[x]) return ;
		vis[x]=1;
		cnt++;
		for(int i=0;i<E[x].size();i++){
			int V=E[x][i];
			if(V==f) continue;
			if(x==u&&V==v) continue;
			if(x==v&&V==u) continue;
			Mark(V,x,u,v);	
		}
	}
	void Check(){
		int f=0;
		if(!r){
			r=q1;
			for(int i=1;i<=n;i++) ans[i]=Q1[i];
		}
		else{
			for(int i=1;i<=n;i++){
				if(ans[i]>Q1[i]){
					f=1;
					break;	
				}
				if(ans[i]<Q1[i]){
					f=0;
					break;	
				}
			}
			if(f) for(int i=1;i<=n;i++) ans[i]=Q1[i];
		}
	}
	void Do(){
		for(int i=1;i<=m;i++){
			int a,b;
			scanf("%d %d",&a,&b);
			addedge(a,b);
		}
		for(int i=1;i<=n;i++) sort(E[i].begin(),E[i].end());
		for(int i=1;i<=n;i++){
			for(int j=0;j<E[i].size();j++){
				int V=E[i][j];
				if(i<=V) continue;
				q1=0;
				memset(vis,0,sizeof(vis));
				cnt=0;
				Mark(1,0,i,V);
//				printf("%d !%d %d\n",i,V,cnt);
				if(cnt!=n) continue;
				dfs(1,0,i,V);
				Check();
			}
		}
		for(int i=1;i<=r;i++){
			printf("%d",ans[i]);
			if(i!=r) printf(" ");
			else printf("\n");
		}
	}
}Solve2;
bool mm2;
int main(){	
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d %d",&n,&m);
	if(m==n-1) Solve1.Do();
	else 
	Solve2.Do();
}
/*
6 6
1 3
2 3
2 5
3 4
4 5
4 6
*/
