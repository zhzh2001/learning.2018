#include<bits/stdc++.h>
#define ll long long
#define pb push_back
#define ls p<<1
#define rs p<<1|1
#define tr tree[p].r
#define tl tree[p].l
using namespace std;
bool mm1;
int n,m;
char tp[10];
const int N1=2018;
struct P1{
	
	ll f[N1],g[N1],w[N1];
	vector<int>E[N1];
	void Min(ll &x,ll y){
		if(x==-2||y==-2) return ;
		if(x==-1||x>y) x=y;
	}
	void addedge(int u,int v){
		E[u].pb(v);
		E[v].pb(u);
	}
	void dfs(int x,int fa){
//		if(x==3) cout<<f[x]<<endl;
		if(f[x]!=-2) f[x]=w[x];
		if(g[x]!=-2) g[x]=0;
		for(int i=0;i<E[x].size();i++){
			int V=E[x][i];
			if(V==fa) continue;
			dfs(V,x);
			ll t1=-1;
			Min(t1,g[V]);
			Min(t1,f[V]);
			if(f[x]!=-2&&t1>=0) f[x]+=t1;
			else f[x]=-2;
			if(g[x]!=-2&&f[V]>=0) g[x]+=f[V];
			else g[x]=-2;
		}
		
	}
	void Do(){
		for(int i=1;i<=n;i++) scanf("%lld",&w[i]);
		for(int i=2;i<=n;i++){
			int a,b;
			scanf("%d %d",&a,&b);
			addedge(a,b);
		}
		for(int i=1;i<=m;i++){
			int a,b,c,d;
			scanf("%d %d %d %d",&a,&b,&c,&d);
			memset(f,-1,sizeof(f));
			memset(g,-1,sizeof(g));
//			cout<<b<<" "<<d<<endl;
			if(b) g[a]=-2;
			else f[a]=-2;
			if(d) g[c]=-2;
			else f[c]=-2;
			dfs(1,0);
			if(f[1]>=0&&g[1]>=0) printf("%lld\n",min(f[1],g[1]));
			else if(f[1]>=0) printf("%lld\n",f[1]);
			else if(g[1]>=0) printf("%lld\n",g[1]);
			else printf("-1\n");
		}
	}
}Solve1;
const int N=3e5+10;
void Min(ll &x,ll y){
	if(y==-1) return;
	if(x==-1||x>y) x=y;	
}
struct node{
	ll v[2][2];
	int l,r;
	node operator +(const node &a)const{
		node c;
		c.l=l,c.r=a.r;
		memset(c.v,-1,sizeof(c.v));
		for(int i=0;i<2;i++){
			for(int j=0;j<2;j++){
				for(int k=0;k<2;k++){
					for(int t=0;t<2;t++){
						if(t==0&&k==0) continue;
						if(~v[i][k]&&~a.v[t][j]) Min(c.v[i][j],v[i][k]+a.v[t][j]);
					}
				}
			}
		}
		return c;	
	}
};
ll w[N];
struct P2{
	node tree[N<<2];
	void Up(int p){
		tree[p]=tree[ls]+tree[rs];
	}
	void Build(int l,int r,int p){
		
		tl=l,tr=r;
		memset(tree[p].v,0,sizeof(tree[p].v));
		if(l==r){
			tree[p].v[0][0]=0;
			tree[p].v[1][1]=w[l];
			tree[p].v[0][1]=tree[p].v[1][0]=-1;
			return ;
		}
		int mid=(l+r)>>1;
		Build(l,mid,ls);
		Build(mid+1,r,rs);
		Up(p);
	}
	node Query(int l,int r,int p){
		
		if(tl==l&&tr==r){
			return tree[p];
		}
		int mid=(tl+tr)>>1;
		if(l>mid) return Query(l,r,rs);
		else if(r<=mid) return Query(l,r,ls);
		else return Query(l,mid,ls)+Query(mid+1,r,rs);
		
	}
	void Do(){
		Build(1,n,1);
//		printf("%lld\n",Query(1,1,1).v[0][0]);
		while(m--){
			int a,b,c,d;
			scanf("%d %d %d %d",&a,&b,&c,&d);
			if(a>=c){
				swap(b,d);
				swap(a,c);	
			}
			if(c==a+1&&!b&&!d) puts("-1");
			else{
				ll ans=0,ret=-1;
				for(int i=0;i<2;i++) for(int j=0;j<2;j++) if(a>1){
					if(!b&&!j) continue;
					Min(ret,Query(1,a-1,1).v[i][j]);
//					printf("%d %d %lld\n",i,j,ret);
				}
				if(~ret) ans+=ret;
				ret=-1;
				for(int i=0;i<2;i++) for(int j=0;j<2;j++) if(a+1<c){
					if(!b&&!i) continue;
					if(!d&&!j) continue;
//					printf("%d %d\n",a+1,c-1);
					Min(ret,Query(a+1,c-1,1).v[i][j]);
				}
//			printf("%d %d %d %d\n",a,b,c,d);
				if(~ret) ans+=ret;
				ret=-1;
				for(int i=0;i<2;i++) for(int j=0;j<2;j++) if(c<n){
					if(!d&&!i) continue;
//					printf("%d %d\n",i,j);
					Min(ret,Query(c+1,n,1).v[i][j]);
				}
				if(~ret) ans+=ret;
				printf("%lld\n",ans+w[a]*b+w[c]*d);
			}
		}
	}
}Solve2;
bool mm2;
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d %d %s",&n,&m,tp);
	if(n<=2000&&m<=2000) Solve1.Do();
	else{
		for(int i=1;i<=n;i++) scanf("%lld",&w[i]);
		for(int i=2;i<=n;i++){
			int a,b;
			scanf("%d %d",&a,&b);	
		}
		Solve2.Do();
	}
	return 0;	
}
/*
6 1 A1
8 3 5 9 10 6 
1 2
2 3
3 4
4 5
5 6
2 1 4 1
*/
