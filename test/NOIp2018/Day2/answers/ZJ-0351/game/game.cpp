#include<bits/stdc++.h>
#define ll long long
#define pb push_back
#define db double
using namespace std;
int n,m;
int rx[]={1,0};
int ry[]={0,1};
struct P1{
	int ok,c[10][10];
	string P,Q,p,q;
	int ID(int x,int y){
		return x*m+y;	
	}
	bool Judge(int x,int y){
		return x>=0&&y>=0&&x<n&&y<m;	
	}
	void DFS(int x,int y,int step){
		string t1=P,t2=Q;
		P=P+(char)(c[x][y]+'0');
		if(x==n-1&&y==m-1){
//			cout<<p<<" "<<P<<endl;
			if(p>P){
				if(q>Q)	ok=0;
			}
			P=t1;
			return ;
		}
		for(int i=0;i<2;i++){
			int nx=x+rx[i],ny=y+ry[i];
			Q=Q+(char)(i+'0');
			if(Judge(nx,ny)){
				DFS(nx,ny,step+1);
				if(!ok) return ;
			}
			Q=t2;
		}
		P=t1;
	}
	void dfs(int x,int y,int step){
		string t1=p,t2=q;
		p=p+(char)(c[x][y]+'0');
		if(x==n-1&&y==m-1){
			P="",Q="";
			DFS(0,0,1);
//			cout<<p<<endl;
			p=t1;
			return ;
		}
		for(int i=0;i<2;i++){
			int nx=x+rx[i],ny=y+ry[i];
			q=q+(char)(i+'0');
			if(Judge(nx,ny)){
				dfs(nx,ny,step+1);
			}
			q=t2;
		}
		p=t1;
	}	
	void Do(){
		int ans=0;
		for(int i=0;i<1<<(n*m);i++){
			for(int a=0;a<n;a++){
				for(int b=0;b<m;b++){
					if(i&(1<<ID(a,b))){
						c[a][b]=1;
					}
					else c[a][b]=0;
				}
			}
			ok=1;
			dfs(0,0,1);
			ans+=ok;
		}
		printf("%d\n",ans);
	}
}Solve1;
const int Mod=1e9+7;
struct P2{
	
	void Do(){
		ll Pw=4;
		for(int i=1;i<=m-1;i++){
			Pw=Pw*3%Mod;
		}
		printf("%lld\n",Pw);
	}
}Solve2;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d %d",&n,&m);
	if(n<=3&&m<=3) Solve1.Do();
	else if(n==2) Solve2.Do();
	return 0;
}
