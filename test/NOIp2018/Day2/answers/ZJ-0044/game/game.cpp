#include<bits/stdc++.h>
using namespace std;
#define REP(i,f,t) for(int i=(f),i##_end_=(t);i<=i##_end_;++i)
#define SREP(i,f,t) for(int i=(f),i##_end_=(t);i<i##_end_;++i)
#define DREP(i,f,t) for(int i=(f),i##_end_=(t);i>=i##_end_;--i)
#define ll long long
template<class T>inline bool chkmin(T&x,T y){return x>y?x=y,1:0;}
template<class T>inline bool chkmax(T&x,T y){return x<y?x=y,1:0;}
template<class T>inline void rd(T&x){
	x=0;char c;int f=1;
	while((c=getchar())<48)if(c=='-')f=-1;
	do x=(x<<1)+(x<<3)+(c^48);
	while((c=getchar())>47);
	x*=f;
}

const int N=10,M=1e5+2;
const int mod=1e9+7;

int n,m;
ll fac[M],inv[M];

ll Pow(ll a,ll b){
	ll x=1;
	while(b){
		if(b&1)x=x*a%mod;
		a=a*a%mod,b>>=1;
	}
	return x;
}

ll C(int n,int m){
	if(n<m or m<0) return 0;
	return fac[n]*inv[m]%mod*inv[n-m]%mod;
}

struct p1{
	void solve(){
		printf("%lld\n",Pow(2,m-1));
	}
}p1;

struct p2{
	void solve(){
		ll ans=0;
		REP(i,0,m-1) ans=(ans+C(m-1,i)*Pow(2,i)%mod);
		printf("%lld\n",ans*4%mod);
	}
}p2;

struct p3{
	
	bool mark[1<<11];
	int pos[1<<11][N];
	
	void solve(){
		int t=n*m;
		int len=n+m-2;
		ll ans=0;
		memset(mark,1,sizeof mark);
		SREP(s,0,1<<t){
			SREP(i,0,1<<len){
				int nx=0,ny=0;
				SREP(j,0,len){
					if(s&(1<<j)) ny++;
					else nx++;
					if(nx>n || ny>m){
						mark[s]=0;
						break;
					}
					pos[s][j]=nx*m+ny;
				}
			}
		}
		SREP(s,0,1<<t){
			bool f=1;
			SREP(j,0,1<<len){
				if(!mark[j])continue;
				int tmp1=0;
				SREP(a,0,len) tmp1|=(1<<pos[j][a]&s)>0;
				SREP(k,0,j){
					if(!mark[k])continue;
					int tmp2=0;
					SREP(b,0,len) tmp2|=(1<<pos[j][b]&s)>0;
					if(tmp1>tmp2){
						f=0;break;
					}
				}
				if(!f)break;
			}
			if(f)ans++;
		}
		printf("%lld\n",ans);
	}
}p3;

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	rd(n),rd(m);
	if(n>m)swap(n,m);
	
	fac[0]=fac[1]=1;
	REP(i,2,m) fac[i]=fac[i-1]*i%mod;
	inv[0]=inv[1]=1;
	REP(i,2,m) inv[i]=(mod-mod/i)*inv[mod%i]%mod;
	REP(i,2,m) inv[i]=inv[i-1]*inv[i]%mod;
	
//	printf("C=%lld  %lld %lld %lld\n",C(5,0),fac[5],inv[0],inv[5]);
	
	if(n==1)p1.solve();
	else if(n==2)p2.solve();
	else if(n==3)p3.solve();
	
	return 0;
}
