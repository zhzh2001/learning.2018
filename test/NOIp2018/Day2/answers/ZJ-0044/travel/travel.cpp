#include<bits/stdc++.h>
using namespace std;
#define REP(i,f,t) for(int i=(f),i##_end_=(t);i<=i##_end_;++i)
#define SREP(i,f,t) for(int i=(f),i##_end_=(t);i<i##_end_;++i)
#define DREP(i,f,t) for(int i=(f),i##_end_=(t);i>=i##_end_;--i)
#define ll long long
template<class T>inline bool chkmin(T&x,T y){return x>y?x=y,1:0;}
template<class T>inline bool chkmax(T&x,T y){return x<y?x=y,1:0;}
template<class T>inline void rd(T&x){
	x=0;char c;int f=1;
	while((c=getchar())<48)if(c=='-')f=-1;
	do x=(x<<1)+(x<<3)+(c^48);
	while((c=getchar())>47);
	x*=f;
}

const int N=5002;
bool mms;
int n,m;
vector<int>E[N];
int tim,sgID[N];

struct p60{
	
	
	void dfs(int x,int f){
		sgID[++tim]=x;
		SREP(i,0,E[x].size()) if(E[x][i]!=f) dfs(E[x][i],x);
	}
	
	void solve(){
		dfs(1,0);
		REP(i,1,tim) printf("%d%c",sgID[i]," \n"[i==tim]);
	}
}p1;

int deg[N];

struct p100{
	
	int dfn[N],low[N],tim;
	int stk[N],top;
	int vis[N];
	bool belong[N];
	int chain[N],len,id[N];
	
	void tarjan(int x,int f){
		dfn[x]=low[x]=++tim;
		stk[++top]=x;
		vis[x]=1;
		bool flag=1;
		SREP(i,0,E[x].size()){
			int y=E[x][i];
			if(y==f and flag){flag=0;continue;}
			if(!dfn[y]){
				tarjan(y,x);
				chkmin(low[x],low[y]);
			}
			else if(vis[y]) chkmin(low[x],dfn[y]);
		}
		
		if(dfn[x]==low[x]){
			if(stk[top]==x)top--;
			else {
				do{
					chain[++len]=stk[top];
					id[stk[top]]=len;
					vis[stk[top]]=0;
					belong[stk[top]]=1;
				}while(x!=stk[top--]);
			}
		}
	}
	
	bool check(){
		REP(i,1,n) if(deg[i]!=2) return 0;
		return 1;
	}
	
	void pre(int x,int f){
//		if(st)return;
		
	}
	
	void solve(){
		tarjan(1,0);
		
		reverse(chain+1,chain+1+len);

		tim=0;
		if(check()){
			sgID[++tim]=1;
			if(chain[2]>chain[len]) reverse(chain+2,chain+1+len);
			int L=2,R=len;
			while(chain[L]<chain[R]) sgID[++tim]=chain[L],++L;
			DREP(i,R,L) sgID[++tim]=chain[i];
		}
		
		else {
			
		}
		
//		printf("tim=%d\n",tim);
		REP(i,1,tim) printf("%d%c",sgID[i]," \n"[i==tim]);
	}
}p2;
bool mmt;
int main(){
//	printf("sz=%lf\n",1.0*(&mmt-&mms)/1024/1024);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);

	rd(n),rd(m);
	REP(i,1,m){
		int a,b;
		rd(a),rd(b);
		E[a].push_back(b);
		E[b].push_back(a);
		deg[a]++,deg[b]++;
	}
	REP(i,1,n) sort(E[i].begin(),E[i].end());
	
	if(m==n-1)p1.solve();
	else p2.solve();
	
	return 0;
}
