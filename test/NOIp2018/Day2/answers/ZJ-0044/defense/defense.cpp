#include<bits/stdc++.h>
using namespace std;
#define REP(i,f,t) for(int i=(f),i##_end_=(t);i<=i##_end_;++i)
#define SREP(i,f,t) for(int i=(f),i##_end_=(t);i<i##_end_;++i)
#define DREP(i,f,t) for(int i=(f),i##_end_=(t);i>=i##_end_;--i)
#define ll long long
template<class T>inline bool chkmin(T&x,T y) {
	return x>y?x=y,1:0;
}
template<class T>inline bool chkmax(T&x,T y) {
	return x<y?x=y,1:0;
}
template<class T>inline void rd(T&x) {
	x=0;
	char c;
	int f=1;
	while((c=getchar())<48)if(c=='-')f=-1;
	do x=(x<<1)+(x<<3)+(c^48);
	while((c=getchar())>47);
	x*=f;
}

const int N=1e5+2,INF=0x3f3f3f3f;
bool mms;
int n,m;
char op[4];
int p[N];

int qwq,head[N];
struct edge {
	int to,nxt;
} E[N<<1];
void addedge(int x,int y) {
	E[qwq]=(edge) {
		y,head[x]
	};
	head[x]=qwq++;
}
#define EREP(x) for(int i=head[x];~i;i=E[i].nxt)

map<int,bool>mp[N];

struct p44{
	int mark[N];
	int dp[N][2];
	
	void dfs(int x,int f) {
		if(~mark[x]) {
			if(mark[x]) dp[x][1]=p[x],dp[x][0]=INF;
			else dp[x][0]=0,dp[x][1]=INF;
		}
		else dp[x][1]=p[x],dp[x][0]=0;
		EREP(x){
			int y=E[i].to;
			if(y==f)continue;
			dfs(y,x);
			dp[x][0]+=dp[y][1];
			dp[x][1]+=min(dp[y][0],dp[y][1]);
		}
	//	printf("x=%d dp0=%d dp1=%d\n",x,dp[x][0],dp[x][1]);
	}
	
	void solve(){
	
		memset(mark,-1,sizeof mark);
		while(m--) {
			int a,b,x,y;
			rd(a),rd(x),rd(b),rd(y);
			mark[a]=x,mark[b]=y;
			if(mp[a][b] and !x and !y) puts("-1");
			else {
				dfs(1,0);
				int ans=INF;
				chkmin(ans,dp[1][0]);
				chkmin(ans,dp[1][1]);
				printf("%d\n",ans==INF?-1:ans);
			}
			mark[a]=mark[b]=-1;
		}
	}	
}p1;

struct pline{
	ll Ldp[N][2],Rdp[N][2];
	
	void solve(){
		
		Ldp[1][1]=p[1];
		Ldp[1][0]=0;
		
		REP(i,2,n){
			Ldp[i][0]=Ldp[i-1][1];
			Ldp[i][1]=min(Ldp[i-1][0],Ldp[i-1][1])+p[i];
		}
		
		Rdp[n][0]=0;
		Rdp[n][1]=p[n];
		
		DREP(i,n-1,1){
			Rdp[i][0]=Rdp[i+1][1];
			Rdp[i][1]=min(Rdp[i+1][0],Rdp[i+1][1])+p[i];
		}
		
		while(m--){
			int a,x,b,y;
			rd(a),rd(x),rd(b),rd(y);
			if(mp[a][b] and !x and !y)puts("-1");
			else {
				if(a>b)swap(a,b),swap(x,y);
				
				ll ans=Ldp[a][x]+Rdp[b][y];
				printf("%lld\n",ans);
			}
		}
	}
	
}p2;
bool mmt;
int main() {
//	printf("sz=%lf\n",1.0*(&mmt-&mms)/1024/1024);
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	rd(n),rd(m);
	scanf("%s",op);
	REP(i,1,n) rd(p[i]);
	
	memset(head,-1,sizeof head);
	SREP(i,1,n) {
		int a,b;
		rd(a),rd(b);
		addedge(a,b);
		addedge(b,a);
		mp[a][b]=1;
	}

	if(n<=2000 and m<=2000)p1.solve();
	else if(op[0]=='A')p2.solve();
	else p1.solve();

	return 0;
}
