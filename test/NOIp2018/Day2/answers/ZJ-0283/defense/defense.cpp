#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
char nc() {
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
bool fl;
void Read(int& x) {
	char c=nc();
	for(;c<'0'||c>'9';c=nc()) if(c=='B') fl=1;
	for(x=0;c>='0'&&c<='9';x=(x<<3)+(x<<1)+c-48,c=nc());
}
typedef long long ll;
const int N=100010;
const ll INF=1e12;
int sz[N],d[N],f[N],ff[N],p[N];
int k,n,m,x,y,Rt;
int X,Y;
int a[N],cnt;
ll dp[N][2],dp1[N][2];
int h[N],nx[N<<1],t[N<<1],num;
bool b[N][2];
void Add(int x,int y) {
	t[++num]=y;nx[num]=h[x];h[x]=num;
}
void Dfs1(int x,int y) {
	sz[x]=1;ff[x]=0;
	for(int i=h[x];i;i=nx[i])
		if(t[i]!=y) Dfs1(t[i],x),sz[x]+=sz[t[i]],ff[x]=max(ff[x],sz[t[i]]);
	ff[x]=max(ff[x],n-sz[x]);
	if(ff[x]<ff[Rt]) Rt=x;
}
void Dfs(int x,int y) {
	d[x]=d[y]+1;f[x]=y;dp[x][1]=p[x];
	for(int i=h[x];i;i=nx[i])
		if(t[i]!=y) {
			Dfs(t[i],x);
			dp[x][0]+=dp[t[i]][1];
			dp[x][1]+=min(dp[t[i]][0],dp[t[i]][1]);
		}
	dp1[x][0]=dp[x][0];dp1[x][1]=dp[x][1];
}
void Update(int x,int y) {
	while(d[x]>d[y]) {
		a[++cnt]=f[x];
		if(b[x][1]) b[f[x]][0]=1;else dp[f[x]][0]+=dp[x][1]-dp1[x][1];
		dp[f[x]][1]+=min(!b[x][0]?dp[x][0]:INF,!b[x][1]?dp[x][1]:INF)-min(dp1[x][0],dp1[x][1]);
		x=f[x];
	}
	if(x==y) {
		while(x!=Rt) {
			a[++cnt]=f[x];
			if(b[x][1]) b[f[x]][0]=1;else dp[f[x]][0]+=dp[x][1]-dp1[x][1];
			dp[f[x]][1]+=min(!b[x][0]?dp[x][0]:INF,!b[x][1]?dp[x][1]:INF)-min(dp1[x][0],dp1[x][1]);
			x=f[x];
		}
	} else {
		while(f[x]!=f[y]) {
			a[++cnt]=f[x];
			if(b[x][1]) b[f[x]][0]=1;else dp[f[x]][0]+=dp[x][1]-dp1[x][1];
			dp[f[x]][1]+=min(!b[x][0]?dp[x][0]:INF,!b[x][1]?dp[x][1]:INF)-min(dp1[x][0],dp1[x][1]);
			x=f[x];
			a[++cnt]=f[y];
			if(b[y][1]) b[f[y]][0]=1;else dp[f[y]][0]+=dp[y][1]-dp1[y][1];
			dp[f[y]][1]+=min(!b[y][0]?dp[y][0]:INF,!b[y][1]?dp[y][1]:INF)-min(dp1[y][0],dp1[y][1]);
			y=f[y];
		}
		a[++cnt]=f[x];
		if(b[x][1]||b[y][1]) b[f[x]][0]=1;else dp[f[x]][0]+=dp[x][1]-dp1[x][1]+dp[y][1]-dp1[y][1];
		dp[f[x]][1]+=min(!b[x][0]?dp[x][0]:INF,!b[x][1]?dp[x][1]:INF)-min(dp1[x][0],dp1[x][1])+min(!b[y][0]?dp[y][0]:INF,!b[y][1]?dp[y][1]:INF)-min(dp1[y][0],dp1[y][1]);
		x=f[x];
		while(x!=Rt) {
			a[++cnt]=f[x];
			if(b[x][1]) b[f[x]][0]=1;else dp[f[x]][0]+=dp[x][1]-dp1[x][1];
			dp[f[x]][1]+=min(!b[x][0]?dp[x][0]:INF,!b[x][1]?dp[x][1]:INF)-min(dp1[x][0],dp1[x][1]);
			x=f[x];
		}
	}
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	Read(n);Read(m);Read(x);
	for(int i=1;i<=n;i++) Read(p[i]);
	for(int i=1;i<n;i++) {
		Read(x);Read(y);Add(x,y);Add(y,x);
	}
	if(!fl) { ff[0]=n+1;Dfs1(1,0); }else Rt=1;
	Dfs(Rt,0);
	while(m--) {
		Read(x);Read(X);Read(y);Read(Y);
		if(d[x]<d[y]) swap(x,y),swap(X,Y);
		if(f[x]==y&&!X&&!Y) {
			puts("-1");
			continue;
		}
		b[x][X^1]=b[y][Y^1]=1;
		cnt=0;a[++cnt]=x;a[++cnt]=y;
		Update(x,y);
		printf("%lld\n",min(!b[Rt][0]?dp[Rt][0]:INF,!b[Rt][1]?dp[Rt][1]:INF));
		for(int i=1;i<=cnt;i++) dp[a[i]][0]=dp1[a[i]][0],dp[a[i]][1]=dp1[a[i]][1],b[a[i]][0]=b[a[i]][1]=0;
	}
	return 0;
}
