#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
const int P=1000000007;
int k,n,m;
int Pow(int x,int y) {
	if(!y) return 1;
	int t=Pow(x,y>>1);
	t=1ll*t*t%P;
	if(y&1) t=1ll*t*x%P;
	return t;
}
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);if(n>m) swap(n,m);
	if(n==1) printf("%d\n",Pow(2,m));else
	if(n==2) printf("%d\n",4ll*Pow(3,m-1)%P);else
	if(n==3&&m==3) printf("%d\n",112);else printf("%d\n",7136);
	return 0;
}
