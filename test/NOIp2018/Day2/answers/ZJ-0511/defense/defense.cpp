#include<bits/stdc++.h>
using namespace std;

char kd[3];
int n,m,a[100010];
vector<int> g[100100];

namespace subtask1
{
	int solve()
	{
		long long ans1=a[1],ans2=a[1];
		for(int i=2;i<=n;i++)
		{
			if(i&1) ans1+=a[i];
			else ans2+=a[i];
		}
		int aa,bb,xx,yy;
		for(int i=1;i<=m;i++)
		{
			scanf("%d%d%d%d",&aa,&xx,&bb,&yy);
			if(!((bb&1)^yy)) printf("%lld\n",ans1);
			else printf("%lld\n",ans2);
		}
	}
}

namespace subtask2
{
	int dp[2048],sta;
	
	int check()
	{
		int flag=1;
		for(int i=1;i<=n;i++)
		{
			for(int j=0;j<g[i].size();j++)
			{
				if(!(sta&(1<<(g[i][j]-1))|sta&(1<<(i-1)))) flag=0;
			}
		}
		return flag;
	}
	
	int solve()
	{
		for(sta=0;sta<(1<<n)-1;sta++)
		{
			if(check())
			{
				for(int i=0;i<n;i++)
				{
					if(sta&(1<<i)) dp[sta]+=a[i+1];
				}
			}
			else dp[sta]=0x3f3f3f3f;
		}
		int aa,xx,bb,yy;
		for(int i=1;i<=m;i++)
		{
			scanf("%d%d%d%d",&aa,&xx,&bb,&yy);
			int ans=0x3f3f3f3f;
			for(int sta=0;sta<(1<<n)-1;sta++)
			{
				int posa=(sta&(1<<(aa-1)));
				int posb=(sta&(1<<(bb-1)));
				if(posa==(xx<<(aa-1))&&posb==(yy<<(bb-1))) ans=min(ans,dp[sta]);
			}
			if(ans==0x3f3f3f3f) puts("-1");
			else printf("%d\n",ans);
		}
	}
}

namespace subtask3
{
	int solve()
	{
		long long ans1=0,ans2=0,ans22=0;
		long long sum1[100010],sum2[100010];
		for(int i=1;i<=n;i++)
		{
			if(i&1) ans1+=a[i],sum1[i]=sum1[i-1]+a[i];
			else ans2+=a[i],sum2[i]=sum2[i-1]+a[i];
		}
		int pos=n>>1;
		if(pos&1)
		{
			ans22=sum1[pos]+sum2[n]-sum2[pos];
		}
		else
		{
			ans22=sum2[pos]+sum1[n]-sum1[pos];
		}
		int aa,bb,xx,yy;
		for(int i=1;i<=m;i++)
		{
			scanf("%d%d%d%d",&aa,&xx,&bb,&yy);
			if(!(xx|yy)) puts("-1");
			if(xx&(!yy)) 
			{
				if(aa&1) printf("%lld\n",min(ans22,ans1));
				else printf("%lld\n",min(ans22,ans2));
			}
			if((!xx)&yy) 
			{
				if(bb&1) printf("%lld\n",min(ans22,ans1));
				else printf("%lld\n",min(ans22,ans2));
			}
			if(xx&yy)
			{
				if(aa&1)
				{
					long long ans3=min(sum1[aa],a[aa]+sum2[aa-1]);
					long long ans4=min(sum2[n]-sum2[aa],a[bb]+sum1[n]-sum1[aa]);
					printf("%lld\n",ans3+ans4);
				}
				else
				{
					long long ans3=min(sum2[aa],a[aa]+sum1[aa-1]);
					long long ans4=min(sum1[n]-sum1[aa],a[bb]+sum2[n]-sum2[aa]);
					printf("%lld\n",ans3+ans4);
				}
			}
		}
	}
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,kd);
	for(int i=1;i<=n;i++) scanf("%d",&a[i]);
	int from,to;
	for(int i=1;i<n;i++)
	{
		scanf("%d%d",&from,&to);
		g[from].push_back(to);
		g[to].push_back(from);
	}
	if(n<=10&&m<=10) subtask2::solve();
	else if(kd[0]=='A'&&kd[1]=='1') subtask1::solve();
		else if(kd[0]=='A'&&kd[1]=='2') subtask3::solve();
}
