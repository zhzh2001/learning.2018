#include<bits/stdc++.h>
using namespace std;

int n,m;
vector<int> g[100010];

namespace subtask1
{
	int a[100010],cnt=0;
	
	int dfs(int now,int fa)
	{
		a[++cnt]=now;
		for(int i=0;i<g[now].size();i++)
		{
			if(g[now][i]==fa) continue;
			dfs(g[now][i],now);
		}
	}
	
	int solve()
	{
		int from,to;
		for(int i=1;i<=m;i++)
		{
			scanf("%d%d",&from,&to);
			g[from].push_back(to);
			g[to].push_back(from);
		}
		for(int i=1;i<=n;i++) sort(g[i].begin(),g[i].end());
		dfs(1,0);
		for(int i=1;i<=n;i++)
		{
			printf("%d ",a[i]);
		}
	}
}

namespace subtask2
{
	int vis[5050],st[5050],top,h[5050],gg;
	int flag2=0,mi[2],cnt,a[5050],cnt2;
	
	int dfs1(int now,int fa)
	{
		if(flag2) return 0;
		int flag=0;
		vis[now]=1;
		st[++top]=now;
		int pos=-1;
		for(int i=0;i<g[now].size();i++)
		{
			if(g[now][i]==fa) continue;
			if(!vis[g[now][i]]) dfs1(g[now][i],now);
			else
			{
				pos=g[now][i];
				flag=1;
				break;
			} 
		}
		if(flag2) return 0;
		if(flag)
		{
			flag2=1;
			h[pos]=1;
			while(st[top]!=pos)
			{
				h[st[top]]=1;
				top--;
			}
			top--;
		}
	}
	
	int dfs2(int now,int fa)
	{
		if(flag2) return 0;
		if(h[now])
		{
			gg=now;
			for(int i=0;i<g[now].size();i++)
			{
				if(h[g[now][i]])
				{
					mi[++cnt]=g[now][i];
				}
			}
			flag2=1;
			if(mi[1]>mi[2]) swap(mi[1],mi[2]);
			return 0;
		}
		for(int i=0;i<=g[now].size();i++)
		{
			if(g[now][i]==fa) continue;
			dfs2(g[now][i],now);
		}
	}
	
	int dfs3(int now,int fa)
	{
		vis[now]=1;
		a[++cnt2]=now;
		for(int i=0;i<g[now].size();i++)
		{
			if(g[now][i]==fa||vis[g[now][i]]) continue;
			if(h[g[now][i]]&&g[now][i]!=gg&&g[now][i]>mi[2]&&(!flag2)) flag2=1;
			else dfs3(g[now][i],now);
		}
	}
	
	int solve()
	{
		memset(vis,0,sizeof(vis));
		int from,to;
		for(int i=1;i<=m;i++)
		{
			scanf("%d%d",&from,&to);
			g[from].push_back(to);
			g[to].push_back(from);
		}
		for(int i=1;i<=n;i++) sort(g[i].begin(),g[i].end());
		dfs1(1,0);
		flag2=0;
		dfs2(1,0);
		flag2=0;
		memset(vis,0,sizeof(vis));
		dfs3(1,0);
		for(int i=1;i<=n;i++) printf("%d ",a[i]);
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m==n-1) subtask1::solve();
	else subtask2::solve();
}
