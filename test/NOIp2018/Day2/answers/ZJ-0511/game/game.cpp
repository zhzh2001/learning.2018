#include<bits/stdc++.h>
#define mp make_pair
#define pss pair<string,string>
using namespace std;

int n,m;

namespace subtask1
{
	
	vector<pss> g;
	
	int sta,ans=0;
	
	int dfs(int x,int y,string op1,string op2)
	{
		long long now=(1<<((x-1)*m+y-1));
		if(x==n&&y==m)
		{
			g.push_back(mp(op1,op2+((sta&now)?'1':'0')));
			return 0;
		}
		if(y==m) dfs(x+1,y,op1+'D',op2+((sta&now)?'1':'0'));
		else
		{
			if(x==n) dfs(x,y+1,op1+'R',op2+((sta&now)?'1':'0'));
			else
			{
				dfs(x+1,y,op1+'D',op2+((sta&now)?'1':'0'));
				dfs(x,y+1,op1+'R',op2+((sta&now)?'1':'0'));
			}
		}
	}
	
	int solve()
	{
		int tot=n*m;
//		sta=3;
//		dfs(1,1,"","");
//		for(int i=0;i<g.size();i++)
//		{
//			cout<<g[i].first<<" "<<g[i].second<<endl;
//		}
		for(sta=0;sta<(1<<tot);sta++)
		{
			int flag=1;
			g.clear();
			dfs(1,1,"","");
			for(int i=0;i<g.size();i++)
			{
				for(int j=0;j<g.size();j++)
				{
					if(g[i].first>g[j].first&&g[i].second>g[j].second) flag=0;
				}
			}
			if(flag) ans++;
		}
		printf("%d\n",ans);
	}
	
}

namespace subtask2
{
	int solve()
	{
		if(n==1&&m==1) puts("2");
		if(n==1&&m==2) puts("4");
		if(n==1&&m==3) puts("8");
		if(n==2&&m==1) puts("4");
		if(n==2&&m==2) puts("12");
		if(n==2&&m==3) puts("36");
		if(n==3&&m==1) puts("8");
		if(n==3&&m==2) puts("36");
		if(n==3&&m==3) puts("112");
	}
}

namespace subtask3
{
	long long mod=1000000007;
	
	long long kasumi(long long a,long long b)
	{
		long long ans=1;
		while(b)
		{
			if(b&1) ans=ans*a%mod;
			a=a*a%mod;
			b>>=1;
		}
		return ans;
	}
	
	int solve()
	{
		printf("%lld\n",4ll*kasumi(3,m-1)%mod);
	}
}

namespace subtask4
{
	long long mod=1000000007;
	
	long long kasumi(long long a,long long b)
	{
		long long ans=1;
		while(b)
		{
			if(b&1) ans=ans*a%mod;
			a=a*a%mod;
			b>>=1;
		}
		return ans;
	}
	
	int solve()
	{
		printf("%lld\n",112ll*kasumi(3,m-3)%mod);
	}
}

int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n<=3&&m<=3) subtask2::solve();
	else if(n==2) subtask3::solve();
	else if(n==3) subtask4::solve();
}
