#include<cstdio>
using namespace std;
struct ab
{
	int f,t;
} t[50005];
int mp[505][505],mmp[50005],h,hh[50005],v[50005];
void make(int x,int y)
{
	t[++h].f=hh[x];
	t[h].t=y;
	hh[x]=h;
}
int min(int x,int y)
{
	return x>y?y:x;
}
int dfs(int x,int y)
{
	int ans=1e9,anss;
	for(int k=0;k<=1;k++)
	{
		if(k==0&&!mmp[y]) continue;
		if(mp[x][(k+1)%2]) continue;
		mmp[x]=k;
		anss=0;
		for(int i=hh[x];i;i=t[i].f)
		{
			int j=t[i].t;
			if(j==y) continue;
			anss+=dfs(j,x);
		}
		ans=min(ans,anss+k*v[x]);
	}
	return ans;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	char s[5];
	int n,m,x,y,a,b;
	scanf("%d%d%s",&n,&m,&s);
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&v[i]);
	}
	for(int i=1;i<n;i++)
	{
		scanf("%d%d",&x,&y);
		make(x,y);
		make(y,x);
	}
	mmp[0]=1;
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&a,&x,&b,&y);
		mp[a][x]=1;
		mp[b][y]=1;
		int ans=dfs(1,0);
		if(ans==1e9) printf("%d\n",-1);
		else printf("%d\n",ans);
		mp[a][x]=mp[b][y]=0;
	}
	return 0;
}
