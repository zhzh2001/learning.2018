#include<cstdio>
#define int long long
using namespace std;
const int mod=1e9+7;
int ans[4][4]={0,0,0,0,
               0,2,4,8,
               0,4,12,36,
               0,8,36,112};
int pow(int x,int y)
{
	int anss=1;
	while(y)
	{
		if(y&1) anss=anss*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return anss%mod;
}
signed main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	scanf("%lld%lld",&n,&m);
	if(n==1) printf("%lld\n",pow(2,m)%mod);
	else if(n==2) printf("%lld\n",pow(3,m-1)*4%mod);
	else
	{
		printf("%lld\n",ans[n][m]);
	}
	return 0;
}
