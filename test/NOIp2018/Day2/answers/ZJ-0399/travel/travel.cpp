#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;
vector<int>v[50005];
int mp[500005],flag,t,s[500005],h,ans[500005],mmp[5005][5005];
void dfs(int x,int y)
{
	printf("%d ",x);
	for(int i=0;i<v[x].size();i++)
	{
		int j=v[x][i];
		if(j==y) continue;
		dfs(j,x);
	}
}
void find(int x,int y)
{
	mp[x]=1;
	for(int i=0;i<v[x].size();i++)
	{
		int j=v[x][i];
		if(j==y) continue;
		if(mp[j])
		{
			flag=1;
			t=j;
			s[++h]=x;
			return;
		}
		find(j,x);
		if(flag)
		{
			if(x==t) flag=0;
			s[++h]=x;
			return;
		}
	}
}
void dfs1(int x,int y)
{
	if(flag==2) return;
	t++;
	if(flag==0&&ans[t]<x)
	{
		flag=2;
		return;
	}
	if(ans[t]>x) flag=1;
	ans[t]=x;
	for(int i=0;i<v[x].size();i++)
	{
		int j=v[x][i];
		if(j==y) continue;
		if(mmp[x][j]) continue;
		dfs1(j,x);
	}
}
void work(int n)
{
	find(1,0);
	for(int i=1;i<=n;i++)
	{
		ans[i]=n+1;
	}
	s[h+1]=s[1];
	for(int i=1;i<=h;i++)
	{
		mmp[s[i]][s[i+1]]=mmp[s[i+1]][s[i]]=1;
		t=0;
		flag=0;
		dfs1(1,0);
		mmp[s[i]][s[i+1]]=mmp[s[i+1]][s[i]]=0;
	}
	for(int i=1;i<=n;i++)
	{
		printf("%d ",ans[i]);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n,m,x,y;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y);
		v[x].push_back(y);
		v[y].push_back(x);
	}
	for(int i=1;i<=n;i++)
	{
		sort(v[i].begin(),v[i].end());
	}
	if(m==n-1) dfs(1,0);
	else work(n);
	return 0;
}
