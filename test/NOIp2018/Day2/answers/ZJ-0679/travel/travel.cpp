#include<bits/stdc++.h>
#define ll long long

using std::cin;
using std::cout;
using std::endl;

const int MAXN=5005;

int head[MAXN],degree[MAXN],v[MAXN],m,n;

struct Edge
{
	int to;
	int next;
}e[2*MAXN];

void pjy(int nv)
{
	int ne=head[nv];
	if(v[nv]==0)
		cout<<nv<<" ";
	v[nv]=1;
	while(ne!=-1)
	{
		if(v[e[ne].to]==1)
			degree[nv]--;
		ne=e[ne].next;
	}
	ne=head[nv];
	while(degree[nv]!=0)
	{
		int min=MAXN;
		while(ne!=-1)
		{
			if(v[e[ne].to]==0)
				pjy(e[ne].to);
			ne=e[ne].next;
		}
		degree[nv]--;
		/*for(int i=1;i<=n;i++)
		cout<<degree[i]<<" ";
	cout<<'\n';*/
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(head,-1,sizeof(head));
	memset(v,0,sizeof(v));
	memset(degree,0,sizeof(degree));
	cin>>n>>m;
	int min=50005;
	int e_num=1;
	for(int i=0;i<m;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		if(x<min)
			min=x;
		if(y<min)
			min=y;
		degree[x]++;
		degree[y]++;
		e[e_num].to=y;
		int tag=head[x];
		if(tag!=-1)
		{
			int last=0;
			int now=head[x];
			bool isBreak=false;
			while(now!=-1)
			{
				if(e[now].to>y)
				{
					if(last!=0)
						e[last].next=e_num;
					else
						head[x]=e_num;
					e[e_num].next=now;
					isBreak=true;
					break;
				}
				last=now;
				now=e[now].next;
			}
			if(!isBreak)
			{
				e[last].next=e_num;
				e[e_num].next=-1;
			}
		}
		else
		{
			e[e_num].next=head[x];
			head[x]=e_num;
		}
		e_num++;
		e[e_num].to=x;
		tag=head[y];
		if(tag!=-1)
		{
			int last=0;
			int now=head[y];
			bool isBreak=false;
			while(now!=-1)
			{
				if(e[now].to>x)
				{
					if(last!=0)
						e[last].next=e_num;
					else
						head[y]=e_num;
					e[e_num].next=now;
					isBreak=true;
					break;
				}
				last=now;
				now=e[now].next;
			}
			if(!isBreak)
			{
				e[last].next=e_num;
				e[e_num].next=-1;
			}
		}
		else
		{
			e[e_num].next=head[y];
			head[y]=e_num;
		}
		e_num++;
	}
	//for(int i=1;i<=n;i++)
	//	cout<<degree[i]<<" ";
	//cout<<'\n';
	pjy(min);
	return 0;
}
