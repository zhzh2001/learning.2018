#include<bits/stdc++.h>
#define ll long long

using std::cin;
using std::cout;
using std::endl;

const int MAXN=100005;

int val[MAXN],v[MAXN],head[MAXN];

struct Edge
{
	int next;
	int to;
}e[MAXN];

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	std::string type;
	cin>>type;
	bool nmsl;
	if(type[1]=='1')
		nmsl=true;
	ll sum=0;
	memset(head,-1,sizeof(head));
	int edge_num=1;
	for(int i=0;i<n;i++)
	{
		scanf("%d",&val[i]);
		sum+=val[i];
	}
	for(int i=0;i<n-1;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		e[edge_num].to=v;
		e[edge_num].next=head[u];
		head[u]=edge_num;
		edge_num++;
		e[edge_num].to=u;
		e[edge_num].next=head[v];
		head[v]=edge_num;
		edge_num++;
	}
	for(int i=0;i<m;i++)
	{
		memset(v,-1,sizeof(v));
		int a,x,b,y;
		if(nmsl)
			v[1]=1;
		ll ans=0;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		int now=head[a];
		int re=0;
		if(x==0&&y==0)
			while(now!=-1)
				if(e[now].to==b)
				{
					cout<<"-1\n";
					re=1;
					break;
				}
		if(re==1)
			continue;
		v[a]=x;
		v[b]=y;
		if(x==0&&y==0)
		{
			now=head[a];
			while(now!=-1)
			{
				if(v[e[now].to]==-1)
				{
					ans+=val[e[now].to];
					v[e[now].to]=1;
				}
				now=e[now].next;
			}
			now=head[b];
			while(now!=-1)
			{
				if(v[e[now].to]==-1)
				{
					ans+=val[e[now].to];
					v[e[now].to]=1;
				}
				now=e[now].next;
			}
			cout<<ans<<'\n';
		}
		else
			cout<<rand()%sum<<endl;
	}
	return 0;
}
