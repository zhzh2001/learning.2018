#include<cstdio>
#include<algorithm>
using namespace std;

struct Node{
	char path[30];
	int re[30];
}S[100010];
int tot,n,m,block[20][20],ans,id;
char s[100010];
pair<int,int> Ans[100010],Ans2[100010];
bool cmp(int a,int b){
	for(int i=1;i<=n+m-1;i++)
		if(S[a].re[i]>S[b].re[i])return 1;
		else if(S[a].re[i]<S[b].re[i]) return 0;
	return 0;
}

void dfs(int x,int y,int step){
	if(x==n&&y==m){
		tot++;
		for(int i=1;i<=n+m-2;i++)
			S[tot].path[i]=s[i];
		return ;
	}
	if(y<m){
		s[step]='R';
		dfs(x,y+1,step+1);
	}
	if(x<n){
		s[step]='D';
		dfs(x+1,y,step+1);
	}
}

int main(){
	scanf("%d%d",&n,&m);
	dfs(1,1,1);
	for(int sta=0;sta<(1<<(n*m));sta++){
		int id=0;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++){
				block[i][j]=(sta>>id)&1;
				id++;
			}
		bool flag=1;
		for(int i=1;i<=tot&&flag;i++){
			int cx=1,cy=1;
			S[i].re[1]=block[1][1];
			for(int j=1;j<=n+m-2;j++){
				if(S[i].path[j]=='R')cy++;
				else if(S[i].path[j]=='D')cx++;
				S[i].re[j+1]=block[cx][cy];
			}
			if(i>1){
				if(cmp(i-1,i))
					flag=0;
			}
		}
		if(flag){
			ans++;
			int sta2=0,sta3=0,sta4=0;
			for(int i=1;i<=n;i++)	
				sta2|=block[i][1]<<(i-1),sta3|=block[i][2]<<(i-1),sta4|=block[i][3]<<(i-1);
			Ans[ans]=(make_pair(sta2,sta3));
			Ans2[ans]=make_pair(sta3,sta4);
			/*for(int i=1;i<=n;i++){
				for(int j=1;j<=m;j++)
					printf("%d",block[i][j]);
				printf("\n");
			}
			printf("\n");*/
		}
	}
	printf("ans=%d\n",ans);
	sort(Ans+1,Ans+1+ans);
	int tn=unique(Ans+1,Ans+1+ans)-Ans-1;
	printf("tn1=%d\n",tn);
	for(int i=1;i<=tn;i++)
		printf("%d %d\n",Ans[i].first,Ans[i].second);
	printf("------\n");

	sort(Ans2+1,Ans2+1+ans);
	tn=unique(Ans2+1,Ans2+1+ans)-Ans2-1;
	printf("tn2=%d\n",tn);
	for(int i=1;i<=tn;i++)
		printf("%d %d\n",Ans2[i].first,Ans2[i].second);
}
				
	
