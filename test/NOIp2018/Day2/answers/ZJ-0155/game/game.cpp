#include<cstdio>
#include<algorithm>
using namespace std;
int ans[10][10]={{},{0,2,4,8,16,32,64,128},{0,4,12,36},{0,8,36,112},{0,16,108,336,912,2688,8064},{0,32,324,1008,2688,7136}};
int n,m;
#define MOD 1000000007
typedef long long ll;
ll Pow(ll a,ll b){
	ll res=1;
	while(b){
		if(b&1)res=res*a%MOD;
		a=a*a%MOD;
		b>>=1;
	}
	return res;
}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n<=3&&m<=3){
		printf("%d\n",ans[n][m]);
	}else if(n<=3){
		ll st=(n==2)?36:112;
		st=(st*Pow(3,m-3))%MOD;
		printf("%lld\n",st);
	}else{
		printf("%d\n",ans[n][m]);
	}
}
