#include<cstdio>
#include<algorithm>
#include<cstring>
#include<vector>
using namespace std;

int n,m,u,v,ans[100010],Ans[100010],atot,eu,ev,x,S[100010],top,dfsvis[100010],cir[100010],cirnum;
vector<int>vec[5010];

bool cmp(){
	for(int i=1;i<=n;i++)
		if(ans[i]<Ans[i])return 1;
		else if(ans[i]>Ans[i])return 0;
	return 0;
}

void dfs2(int u,int f){
	ans[++atot]=u;
	for(int i=0;i<vec[u].size();i++){
		int v=vec[u][i];
		if((v==eu&&u==ev)||(u==eu&&v==ev))continue;
		if(v!=f)
			dfs2(v,u);
	}
}

bool dfs(int u,int f){
	dfsvis[u]=1;
	S[++top]=u;
	for(int i=0;i<vec[u].size();i++){
		int v=vec[u][i];
		if(v==f)continue;
		if(dfsvis[v]){
			x=v;
			return 1;
		}
		if(dfs(v,u))return 1;
	}
	top--;
	return 0;
}
	

int main(){
		freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		vec[u].push_back(v);
		vec[v].push_back(u);
	}
	for(int i=1;i<=n;i++)
		sort(vec[i].begin(),vec[i].end());
	if(m==n-1){
		dfs2(1,1);
		for(int i=1;i<=n;i++)
			printf("%d%c",ans[i],(i==n)?'\n':' ');
	}else{
		dfs(1,1);
		bool flag=0;
		for(int i=1;i<=top;i++){
			if(S[i]==x)
				flag=1;
			if(flag)
				cir[++cirnum]=S[i];
		}
		cir[cirnum+1]=cir[1];
		memset(Ans,0x3f3f3f3f,sizeof Ans);
		for(int i=1;i<=cirnum;i++){
			eu=cir[i],ev=cir[i+1];
			atot=0;
			dfs2(1,1);
			if(cmp())
				for(int j=1;j<=n;j++)
					Ans[j]=ans[j];
		}
		for(int i=1;i<=n;i++)
			printf("%d%c",Ans[i],(i==n)?'\n':' ');
	}
	return 0;
}
		
