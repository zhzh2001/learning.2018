#include<cstdio>
#include<algorithm>
using namespace std;
#define N 100010
typedef long long ll;

int tot,Head[N],Next[N<<1],To[N<<1],c[N],n,m,u,v,x,y;
char t[10];
ll f[N][2],inf=1e11;

void addedge(int u,int v){
	tot++;
	Next[tot]=Head[u];
	Head[u]=tot;
	To[tot]=v;
}

void dfs(int a,int ff){
//	printf("%d %d\n",a,ff);
	f[a][0]=0,f[a][1]=c[a];
	if(a==u)
		f[a][1^x]=inf;
	if(a==v)
		f[a][1^y]=inf;
	for(int it=Head[a];it;it=Next[it]){
		int v=To[it];
		if(v!=ff){
			dfs(v,a);
			f[a][0]+=f[v][1];
			f[a][1]+=min(f[v][1],f[v][0]);
		}
	}
}

int main(){
	scanf("%d%d%s",&n,&m,t);
	for(int i=1;i<=n;i++)
		scanf("%d",&c[i]);
	for(int i=1;i<n;i++){
		scanf("%d%d",&u,&v);
		addedge(u,v);
		addedge(v,u);
	}	
	for(int i=1;i<=m;i++){
		scanf("%d%d%d%d",&u,&x,&v,&y);
		dfs(1,1);
		ll ans=min(f[1][0],f[1][1]);
		printf("%lld\n",(ans>=inf)?(ll)-1:ans);
	}
}
