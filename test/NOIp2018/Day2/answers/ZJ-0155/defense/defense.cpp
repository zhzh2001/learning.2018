#include<stdio.h>
#include<algorithm>
using namespace std;
typedef long long ll;
#define N 200010
#define M 20
int fa[N][M],dep[N],dfsi[N],dfso[N],c[N],Lg[N],n,m,u,x,v,y;
struct Node{ll a[4];}f[N][M];
int tot,Head[N],Next[N<<1],To[N<<1],ts;
char t[10];
ll inf=1e11;
ll ff[N][2],ffo[N][2];

void dfs(int u,int f){
	fa[u][0]=f;
	dep[u]=dep[f]+1;
	dfsi[u]=++ts;
	ff[u][1]=c[u];
	for(int it=Head[u];it;it=Next[it]){
		int v=To[it];
		if(v!=f){
			dfs(v,u);
			ff[u][0]+=ff[v][1];
			ff[u][1]+=min(ff[v][0],ff[v][1]);
		}
	}
	dfso[u]=ts;
}

void dfs2(int u,int f,ll o0,ll o1){
	ffo[u][0]=o1;
	ffo[u][1]=min(o0,o1)+c[u];
	o0=ffo[u][0]+ff[u][0];
	o1=ffo[u][1]+min(ff[u][0],ff[u][1]-c[u]);
	for(int it=Head[u];it;it=Next[it]){
		int v=To[it];
		if(v!=f)
			dfs2(v,u,o0-ff[v][1],o1-min(ff[v][0],ff[v][1]));
	}
}

int LCA(int u,int v){
	for(int i=Lg[n];i>=0;i--)
		if(dep[u]-(1<<i)>=dep[v])
			u=fa[u][i];
	if(u==v)return u;
	for(int i=Lg[n];i>=0;i--)
		if(fa[u][i]!=fa[v][i])
			u=fa[u][i],v=fa[v][i];
	return fa[u][0];
}

ll calc(int u,int x,int v,int y){
	bool flag=0;
	//Node res=(Node){0,inf,inf,0};
	Node res;
	res.a[0]=res.a[3]=0;
	res.a[1]=res.a[2]=inf;
	for(int i=Lg[n];i>=0;i--)
		if(dep[u]-(1<<i)>=dep[v]){
			if(!flag)
				res=f[u][i],flag=1;
			else{
				Node rres;
				for(int t1=0;t1<=1;t1++)
					for(int t2=0;t2<=1;t2++)
						rres.a[t1<<1|t2]=min(res.a[t1<<1]+f[u][i].a[t2],res.a[t1<<1|1]+f[u][i].a[2|t2]);
				res=rres;
			}
			u=fa[u][i];
		}
	//printf("calc(%d,%d,%d,%d)=%lld\n",u,x,v,y,res.a[x<<1|y]);
	return res.a[x<<1|y];
}
void addedge(int u,int v){
	tot++;
	Next[tot]=Head[u];
	Head[u]=tot;
	To[tot]=v;
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,t);
	for(int i=1;i<=n;i++)
		scanf("%d",&c[i]);
	for(int i=1;i<n;i++){
		scanf("%d%d",&u,&v);
		addedge(u,v);
		addedge(v,u);
	}
	Lg[1]=0;
	for(int i=2;i<=n;i++)
		Lg[i]=Lg[i>>1]+1;
	dfs(1,1);
	dfs2(1,1,0,0);
	for(int i=1;i<=n;i++){
		int f0=fa[i][0];
		ll o0=ff[f0][0]-ff[i][1];
		ll o1=ff[f0][1]-min(ff[i][0],ff[i][1]);
		f[i][0].a[0]=inf;
		f[i][0].a[1]=o1;
		f[i][0].a[2]=o0;
		f[i][0].a[3]=o1;
		//f[i][0]=(Node){inf,o1,o0,o1};
		//printf("f[%d][0]=%lld %lld\n",i,o0,o1);
	}
	for(int i=1;i<=Lg[n];i++)
		for(int j=1;j<=n;j++){
			fa[j][i]=fa[fa[j][i-1]][i-1];
			for(int t1=0;t1<=1;t1++)
				for(int t2=0;t2<=1;t2++)
					f[j][i].a[t1<<1|t2]=min(f[j][i-1].a[t1<<1]+f[fa[j][i-1]][i-1].a[t2],f[j][i-1].a[t1<<1|1]+f[fa[j][i-1]][i-1].a[2|t2]);
		}
	for(int i=1;i<=m;i++){
		scanf("%d%d%d%d",&u,&x,&v,&y);
		if(dep[u]<dep[v])swap(u,v),swap(x,y);
		ll ans;
		if(dfsi[u]>=dfsi[v]&&dfsi[u]<=dfso[v])		
			ans=ff[u][x]+ffo[v][y]+calc(u,x,v,y)-y*c[v];
		else{
			int lca=LCA(u,v);
			//printf("lca=%d\n",lca);
			//printf("%lld %lld\n",ff[u][x],ff[v][y]);
			ans=ff[u][x]+ff[v][y]+min(calc(u,x,lca,0)+calc(v,y,lca,0)-ff[lca][0]+ffo[lca][0],calc(u,x,lca,1)+calc(v,y,lca,1)-ff[lca][1]+ffo[lca][1]-c[lca]);
		}
		printf("%lld\n",ans>=inf?-1:ans);
	}
}
