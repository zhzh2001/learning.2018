#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define fi first
#define se second
#define pb push_back
typedef long long ll;
typedef unsigned long long ull;
struct E
{
	int to,nxt;
}e[200100];
int f1[100100],ne;
int n,m;
char tp[233];
int ff[100010];
ll p[100010];
const ll inf1=0x3f3f3f3f3f3f3f3f;
ll an[100010][2],an2[100010][2];
int vis[100010];
int dep[100010];
void addto(ll &x,ll y)
{
	if(x>=inf1)	return;
	if(y>=inf1)	{x=inf1;return;}
	x+=y;
}
void dfs(int u,int fa)
{
	ff[u]=fa;dep[u]=dep[fa]+1;
	for(int v,k=f1[u];k;k=e[k].nxt)
		if(e[k].to!=fa)
		{
			v=e[k].to;
			dfs(v,u);
			an[u][0]+=an[v][1];
			an[u][1]+=min(an[v][0],an[v][1]);
		}
	an[u][1]+=p[u];
}

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int i,a,x,b,y,u,v,l;ll t;
	scanf("%d%d%s",&n,&m,tp);
	for(i=1;i<=n;++i)
		scanf("%lld",&p[i]);
	for(i=1;i<n;++i)
	{
		scanf("%d%d",&x,&y);
		e[++ne].to=y;e[ne].nxt=f1[x];f1[x]=ne;
		e[++ne].to=x;e[ne].nxt=f1[y];f1[y]=ne;
	}
	dfs(1,0);
	memcpy(an2,an,sizeof(an2));
	while(m--)
	{
		scanf("%d%d%d%d",&a,&x,&b,&y);
		x=!x;y=!y;
		for(u=a,v=b;u!=v;u=ff[u])
			if(dep[u]<dep[v])
				swap(u,v);
		l=u;
		for(u=ff[a],v=a;v!=l;u=ff[u],v=ff[v])
		{
			an2[u][0]-=an[v][1];
			an2[u][1]-=min(an[v][0],an[v][1]);
		}
		for(u=ff[b],v=b;v!=l;u=ff[u],v=ff[v])
		{
			an2[u][0]-=an[v][1];
			an2[u][1]-=min(an[v][0],an[v][1]);
		}
		for(u=ff[l],v=l;u;u=ff[u],v=ff[v])
		{
			an2[u][0]-=an[v][1];
			an2[u][1]-=min(an[v][0],an[v][1]);
		}
		an2[a][x]=an2[b][y]=inf1;
		for(u=ff[a],v=a;v!=l;u=ff[u],v=ff[v])
		{
			addto(an2[u][0],an2[v][1]);
			addto(an2[u][1],min(an2[v][0],an2[v][1]));
		}
		for(u=ff[b],v=b;v!=l;u=ff[u],v=ff[v])
		{
			addto(an2[u][0],an2[v][1]);
			addto(an2[u][1],min(an2[v][0],an2[v][1]));
		}
		for(u=ff[l],v=l;u;u=ff[u],v=ff[v])
		{
			addto(an2[u][0],an2[v][1]);
			addto(an2[u][1],min(an2[v][0],an2[v][1]));
		}
		t=min(an2[1][0],an2[1][1]);
		printf("%lld\n",t>=inf1?-1:t);
		for(u=a;u;u=ff[u])
		{
			an2[u][0]=an[u][0];an2[u][1]=an[u][1];
		}
		for(u=b;u;u=ff[u])
		{
			an2[u][0]=an[u][0];an2[u][1]=an[u][1];
		}
	}
	return 0;
}
