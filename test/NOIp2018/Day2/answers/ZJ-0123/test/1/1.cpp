#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define fi first
#define se second
#define pb push_back
typedef long long ll;
typedef unsigned long long ull;
struct E
{
	int to,nxt;
}e[10010];
int ban[5010],ban1,ban2;
int f1[5010],ne;
int n,m;
int ans[5010];
namespace s1
{
	int state;
	E ex[10010];int ch[10010],nc;
	int ff[10010],len;
	void dfs(int u,int fa)
	{
		for(int k=f1[u];k;k=e[k].nxt)
			if(e[k].to!=fa&&e[k].to!=ban[u])
			{
				dfs(e[k].to,u);
				ff[e[k].to]=u;
			}
	}
	void dfs2(int u)
	{
		if(state==1)	ans[++len]=u;
		else if(state==-1)	return;
		else
		{
			++len;
			if(u<ans[len])
			{
				state=1;
				ans[len]=u;
			}
			else if(u>ans[len])
			{
				state=-1;
				return;
			}
		}
		for(int k=ch[u];k;k=ex[k].nxt)
		{
			dfs2(ex[k].to);
		}
	}
	void solve()
	{
		nc=0;
		memset(ch+1,0,sizeof(ch[0])*n);
		dfs(1,0);
		for(int i=n;i>=2;--i)
		{
			ex[++nc].to=i;ex[nc].nxt=ch[ff[i]];ch[ff[i]]=nc;
		}
		state=0;len=0;
		dfs2(1);
	}
}
int dep[5010],ff2[5010];
void dfs1(int u,int fa)
{
	dep[u]=dep[fa]+1;
	ff2[u]=fa;
	for(int k=f1[u];k;k=e[k].nxt)
		if(e[k].to!=fa&&e[k].to!=ban[u])
			dfs1(e[k].to,u);
}
int fa[5010];
int find(int x){return x==fa[x]?x:fa[x]=find(fa[x]);}
int main()
{
	int i,x,y,fx,fy;
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;++i)
		fa[i]=i;
	for(i=1;i<=n;++i)
		ans[i]=n+1;
	for(i=1;i<=m;++i)
	{
		scanf("%d%d",&x,&y);
		e[++ne].to=y;e[ne].nxt=f1[x];f1[x]=ne;
		e[++ne].to=x;e[ne].nxt=f1[y];f1[y]=ne;
		fx=find(x);fy=find(y);
		if(fx==fy)	{ban[x]=y;ban[y]=x;ban1=x;ban2=y;continue;}
		fa[fx]=fy;
	}
	if(m==n-1)
	{
		s1::solve();
		for(i=1;i<=n;++i)
			printf("%d ",ans[i]);
		puts("");
		return 0;
	}
	dfs1(1,0);
	s1::solve();ban[ban1]=ban[ban2]=0;
	for(x=ban1,y=ban2;x!=y;)
	{
		if(dep[x]<dep[y])	swap(x,y);
		ban[x]=ff2[x];ban[ff2[x]]=x;
		s1::solve();
		ban[x]=ban[ff2[x]]=0;
		x=ff2[x];
	}
	for(i=1;i<=n;++i)
		printf("%d ",ans[i]);
	puts("");
	return 0;
}
