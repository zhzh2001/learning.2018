#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define fi first
#define se second
#define pb push_back
typedef long long ll;
typedef unsigned long long ull;
struct E
{
	int to,nxt;
}e[200100];
int f1[100100],ne;
int n,m;
char tp[233];
int ff[100010];
ll p[100010];
const ll inf1=0x3f3f3f3f3f3f3f3f;
ll an[100010][2],an2[100010][2];
int vis[100010];
int dep[100010];
int tag[100010];
void addto(ll &x,ll y)
{
	if(x>=inf1)	return;
	if(y>=inf1)	{x=inf1;return;}
	x+=y;
}
void dfs(int u,int fa)
{
	an[u][0]=an[u][1]=0;
	ff[u]=fa;dep[u]=dep[fa]+1;
	for(int v,k=f1[u];k;k=e[k].nxt)
		if(e[k].to!=fa)
		{
			v=e[k].to;
			dfs(v,u);
			addto(an[u][0],an[v][1]);
			addto(an[u][1],min(an[v][0],an[v][1]));
		}
	addto(an[u][1],p[u]);
	if(tag[u]!=-1)	an[u][tag[u]]=inf1;
}

int main()
{
	int i,a,x,b,y,u,v,l;ll t;
	scanf("%d%d%s",&n,&m,tp);
	for(i=1;i<=n;++i)
		scanf("%lld",&p[i]);
	for(i=1;i<n;++i)
	{
		scanf("%d%d",&x,&y);
		e[++ne].to=y;e[ne].nxt=f1[x];f1[x]=ne;
		e[++ne].to=x;e[ne].nxt=f1[y];f1[y]=ne;
	}
	memcpy(an2,an,sizeof(an2));
	memset(tag,-1,sizeof(tag));
	while(m--)
	{
		scanf("%d%d%d%d",&a,&x,&b,&y);
		tag[a]=!x;tag[b]=!y;
		dfs(1,0);
		t=min(an[1][0],an[1][1]);
		printf("%lld\n",t>=inf1?-1:t);
		tag[a]=-1;tag[b]=-1;
	}
	return 0;
}
