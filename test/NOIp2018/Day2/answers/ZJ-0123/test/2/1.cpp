#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define fi first
#define se second
#define pb push_back
typedef long long ll;
typedef unsigned long long ull;
const ll md=1000000007;
ll n,m,ans=1;
bool vis[9][1000010];
void work(ll x,ll y)
{
	if(vis[x][y])	return;
	//printf("2t%lld %lld\n",x,y);
	ll num=0;
	for(;x>=1&&y<=m&&!vis[x][y];--x,++y)
	{
		vis[x][y]=1;
		++num;
	}
	printf("1t%lld %lld %lld\n",x,y,num);
	ans=ans*(num+1)%md;
}
int main()
{
	ll i;
	scanf("%lld%lld",&n,&m);
	for(i=1;i<=n;++i)
		work(i,1);
	for(i=1;i<=m;++i)
		work(n,i);
	printf("%lld\n",ans);
	return 0;
}
