#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define fi first
#define se second
#define pb push_back
typedef long long ll;
typedef unsigned long long ull;
const ll md=1000000007;
ll n,m,ans=1;
bool vis[9][1000010];
void work(ll x,ll y)
{
	if(vis[x][y])	return;
	//printf("2t%lld %lld\n",x,y);
	ll num=0;
	for(;x>=1&&y<=m&&!vis[x][y];--x,++y)
	{
		vis[x][y]=1;
		++num;
	}
	//printf("1t%lld %lld %lld\n",x,y,num);
	ans=ans*(num+1)%md;
}
int an1[]={0,2,4,8,16,32,64};
int an2[]={0,4,12,36,108,324};
int an3[]={0,8,36,112,336,1008};
int an4[]={0,16,108,336,912};
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	ll i;
	scanf("%lld%lld",&n,&m);
	if(n==1&&m<=6)
	{
		printf("%d\n",an1[m]);
		return 0;
	}
	else if(n==2&&m<=5)
	{
		printf("%d\n",an2[m]);
		return 0;
	}
	else if(n==3&&m<=5)
	{
		printf("%d\n",an3[m]);
		return 0;
	}
	else if(n==4&&m<=4)
	{
		printf("%d\n",an4[m]);
		return 0;
	}
	for(i=1;i<=n;++i)
		work(i,1);
	for(i=1;i<=m;++i)
		work(n,i);
	printf("%lld\n",ans);
	return 0;
}
