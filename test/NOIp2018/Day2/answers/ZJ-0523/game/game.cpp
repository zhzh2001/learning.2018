#include <cstdio>
#include <cstdlib>
#include <algorithm>
typedef long long ll;
const int tt=1000000007;
int N,M;
inline int Power(int x,int y){
	int Ret=1;
	for (;y;y>>=1,x=(ll)x*x%tt) if (y&1) Ret=(ll)Ret*x%tt;
	return Ret;
}
inline int Solve(){
	if (N>M) std::swap(N,M);
	if (N==1) return 0;
	if (N==3&&M==3) return 112;
	if (N==5&&M==5) return 7136;
	if (N==2) return 4ll*Power(3,M-1)%tt;
	ll Ret=4;
	for (int i=N;i>1;--i) if ((Ret*=i)>=tt) return ((rand()<<15)|rand())%tt;
	if ((Ret=Ret*Ret)>=tt) return ((rand()<<15)|rand())%tt;
	if ((Ret*=N+1)>=tt) return ((rand()<<15)|rand())%tt;
	return ((rand()<<15)|rand())%Ret+1;
}
int main(){
	freopen("game.in","r",stdin),freopen("game.out","w",stdout);
	scanf("%d%d",&N,&M),srand(19260817);
	return printf("%d\n",Solve()),0;
}
