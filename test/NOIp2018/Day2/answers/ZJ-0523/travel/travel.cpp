#include <cstdio>
#include <vector>
#include <cstring>
#include <algorithm>
using namespace std;
const int maxn=5005;
int N,M,A[maxn],cnt,fa[maxn],C[maxn],_x,_y,AlP;
vector<int> E[maxn]; bool fl;
typedef vector<int>::iterator it;
struct Edge{int x,y;}P[maxn];
inline void DFS(int x,int fa){
	A[++cnt]=x;
	for (it i=E[x].begin();i!=E[x].end();++i) if (fa^*i) DFS(*i,x);
}
inline void PFS(int x,int fa){
	C[++cnt]=x;
	for (it i=E[x].begin();i!=E[x].end();++i)
		if ((fa^*i)&&!((_x==x&&_y==*i)||(_x==*i&&_y==x))) PFS(*i,x);
}
int getf(int x){return fa[x]^x?fa[x]=getf(fa[x]):x;}
int que[maxn],hd,tl,k,pre[maxn];
bool vis[maxn];
inline void BFS(int St,int Ed){
	for (vis[que[tl=1]=St]=true;hd<tl;)
		for (it j=E[k=que[++hd]].begin();j!=E[k].end();++j)
			if (!((_x==k&&_y==*j)||(_x==*j&&_y==k))&&!vis[*j]) vis[que[++tl]=*j]=true,pre[*j]=k;
	for (int i=Ed;i^St;i=pre[i]) P[++AlP]=(Edge){pre[i],i};
}
int main(){
	freopen("travel.in","r",stdin),freopen("travel.out","w",stdout);
	scanf("%d%d",&N,&M);
	if (M<N){
		for (int i=M,x,y;i;--i) scanf("%d%d",&x,&y),E[x].push_back(y),E[y].push_back(x);
		for (int i=N;i;--i) sort(E[i].begin(),E[i].end());
		DFS(1,0);
		for (int i=1;i<N;++i) printf("%d ",A[i]);
		return printf("%d\n",A[N]),0;
	}
	for (int i=N;i;--i) A[fa[i]=i]=N;
	for (int i=M,x,y,fx,fy;i;--i){
		scanf("%d%d",&x,&y),E[x].push_back(y),E[y].push_back(x),fx=getf(x),fy=getf(y);
		if (fx^fy) fa[fy]=fx; else _x=x,_y=y;
	}
	for (int i=N;i;--i) sort(E[i].begin(),E[i].end());
	BFS(_x,_y),PFS(1,0),memcpy(A,C,(N+2)<<2);
	for (int i=AlP;i;--i){
		_x=P[i].x,_y=P[i].y,fl=true,cnt=0,PFS(1,0);
		for (int i=1;i<=N;++i) if (A[i]^C[i]){fl=C[i]<A[i]; break;}
		if (fl) memcpy(A,C,(N+2)<<2);
	}
	for (int i=1;i<N;++i) printf("%d ",A[i]);
	return printf("%d\n",A[N]),0;
}
