#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long ll;
const ll inf=10000000000000ll;
const int maxn=100005,maxe=200005;
int N,Q,A[maxn],lnk[maxn],nxt[maxe],son[maxe],tot;
char opt[5]; ll F[maxn][2],Ans,G[maxn][2];
inline void read(int &Res){
	char ch=getchar(); bool fl=false;
	for (Res=0;!isdigit(ch);ch=getchar());
	for (;isdigit(ch);ch=getchar()) Res=(Res<<3)+(Res<<1)+ch-48;
	if (fl) Res=-Res;
}
template<typename _Tp> inline ll Min(_Tp x,_Tp y){return x<y?x:y;}
template<typename _Tp> inline ll Max(_Tp x,_Tp y){return x>y?x:y;}
namespace Sub_A_1{
	ll L[maxn][2],R[maxn][2];
	inline void Solve(){
		for (int i=1,x,y;i<N;++i) read(x),read(y);
		L[1][1]=A[1],L[1][0]=inf;
		for (int i=2;i<=N;++i) L[i][0]=L[i-1][1],L[i][1]=Min(L[i-1][0],L[i-1][1])+A[i];
		R[N][0]=0,R[N][1]=A[N];
		for (int i=N-1;i>1;--i) R[i][0]=R[i+1][1],R[i][1]=Min(R[i+1][0],R[i+1][1])+A[i];
		for (int a,x,b,y;Q;--Q){
			read(a),read(x),read(b),read(y);
			if (y) Ans=Min(L[b-1][0],L[b-1][1])+Min(R[b+1][0],R[b+1][1])+A[b];
				else Ans=L[b-1][1]+R[b+1][1];
			printf("%lld\n",Ans<inf?Ans:-1);
		}
	}
}
namespace Sub_A_2{
	ll L[maxn][2],R[maxn][2];
	inline void Solve(){
		for (int i=1,x,y;i<N;++i) read(x),read(y);
		L[1][1]=A[1],L[1][0]=0;
		for (int i=2;i<=N;++i) L[i][0]=L[i-1][1],L[i][1]=Min(L[i-1][0],L[i-1][1])+A[i];
		R[N][0]=0,R[N][1]=A[N],R[N+1][1]=inf;
		for (int i=N-1;i;--i) R[i][0]=R[i+1][1],R[i][1]=Min(R[i+1][0],R[i+1][1])+A[i];
		for (int a,x,b,y;Q;--Q){
			read(a),read(x),read(b),read(y);
			if (!x&&!y){puts("-1"); continue;}
			if (a>b) swap(a,b),swap(x,y);
			Ans=(x?Min(L[a-1][0],L[a-1][1]):L[a-1][1])+(y?Min(R[b+1][0],R[b+1][1]):R[b+1][1])+(x?A[a]:0)+(y?A[b]:0);
			printf("%lld\n",Ans<inf?Ans:-1);
		}
	}
}
namespace Sub_B_1{
	int pre[maxn];
	void DFS(int x,int fa){
		ll Sum=0,Sum2=0; pre[x]=fa;
		for (int j=lnk[x];j;j=nxt[j]) if (fa^son[j]) DFS(son[j],x),Sum+=F[son[j]][1],Sum2+=Min(F[son[j]][1],F[son[j]][0]);
		if (F[x][0]<inf) F[x][0]=Sum;
		if (F[x][1]<inf) F[x][1]=Sum2+A[x];
	}
	inline void Solve(){
		DFS(1,0);
		for (int a,x,b,y;Q;--Q){
			read(a),read(x),read(b),read(y);
			if (y) G[b][0]=inf,G[b][1]=F[b][1];
				else G[b][1]=inf,G[b][0]=F[b][0];
			for (int fa=pre[b];b^1;b=fa,fa=pre[fa])
				G[fa][0]=F[fa][0],G[fa][1]=F[fa][1],G[fa][0]+=G[b][1]-F[b][1],G[fa][1]+=Min(G[b][1],G[b][0])-Min(F[b][1],F[b][0]);
			Ans=G[1][1],printf("%lld\n",Ans<inf?Ans:-1);
		}
	}
}
inline void Add(int x,int y){nxt[++tot]=lnk[x],son[lnk[x]=tot]=y;}
void DFS(int x,int fa){
	ll Sum=0,Sum2=0;
	for (int j=lnk[x];j;j=nxt[j]) if (fa^son[j]) DFS(son[j],x),Sum+=F[son[j]][1],Sum2+=Min(F[son[j]][1],F[son[j]][0]);
	if (F[x][0]<inf) F[x][0]=Sum;
	if (F[x][1]<inf) F[x][1]=Sum2+A[x];
}
int main(){
	freopen("defense.in","r",stdin),freopen("defense.out","w",stdout);
	read(N),read(Q),scanf("%s",opt);
	for (int i=1;i<=N;++i) read(A[i]);
	if ('A'==*opt){
		if (opt[1]=='1') return Sub_A_1::Solve(),0;
		if (opt[1]=='2') return Sub_A_2::Solve(),0;
	}
	for (int i=1,x,y;i<N;++i) read(x),read(y),Add(x,y),Add(y,x);
	if (opt[1]=='1') return Sub_B_1::Solve(),0;
	for (int a,x,b,y;Q;--Q) read(a),read(x),read(b),read(y),memset(F,0,(N+2)<<4),F[a][x^1]=F[b][y^1]=inf,DFS(1,0),Ans=Min(F[1][1],F[1][0]),printf("%lld\n",Ans<inf?Ans:-1);
	return 0;
}
