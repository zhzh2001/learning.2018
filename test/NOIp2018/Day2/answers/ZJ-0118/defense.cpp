#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
int n,m,To[200005],Next[200005],Head[200005],tot,f[200005][2],opt[100005],a,b,opta,optb,s[100005],val[100005];
char type[5],u,v;
int read(){
	int x=0,f=1; char ch=getchar();
	while (ch>'9'||ch<'0') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') {x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void add(int u,int v){
	To[++tot]=v;
	Next[tot]=Head[u];
	Head[u]=tot;
}
void dfs(int u,int fa){
	int F=0;
	f[u][1]=val[u];
	if (s[u]) f[u][opt[u]^1]=-1;
	for (int i=Head[u];i;i=Next[i]){
		int v=To[i];
		if (v==fa) continue;
		F=1;
		dfs(v,u);
		if (s[u]){
			if (opt[u]==0) f[u][0]+=f[v][1],f[u][1]=-1;
			if (opt[u]==1) f[u][1]=min(f[u][1]+f[v][0],f[u][1]+f[v][1]),f[u][0]=-1;
		}else{int FF=0;
			if (s[v]&&opt[v]==0); 
			else f[u][0]+=f[v][1],F=1; 
			int num=1<<30;
			if (f[v][0]!=-1) num=f[u][1]+f[v][0],FF=1;
			if (f[v][1]!=-1) num=min(num,f[u][1]+f[v][1]),FF=1;
			if (FF) f[u][1]=num;
		}
	}
	return;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(); m=read(); scanf("%s",type);
	for (int i=1;i<=n;i++) val[i]=read();
	for (int i=1;i<n;i++){
		u=read(); v=read();
		add(u,v); add(v,u);
	}
	for (int i=1;i<=m;i++){
		memset(f,0,sizeof(f));
		a=read(); opta=read(); b=read(); optb=read();
		s[a]=1; s[b]=1;
		opt[a]=opta; opt[b]=optb;
		dfs(1,0);
		int ans=1<<30;
		s[a]=0; s[b]=0; opt[a]=0; opt[b]=0;
		if (f[1][1]!=-1) ans=min(ans,f[1][1]);
		if (f[1][0]!=-1) ans=min(ans,f[1][0]);
		if (ans==1<<30) printf("-1\n");
		else printf("%d\n",ans);
	}
}
