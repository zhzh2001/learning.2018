#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
using namespace std;
int To[100005],Next[100005],Head[100005],tot,a[100005],vis[100005];
int Deep[100005],Father[100005],s[100005],n,m,u,v,f[100005],Ax,Ay,sx,sy;
int Cutx[100005],F,Cuty[100005],Cut[100005],ans[100005],mm,g,S[5005][5005];
void add(int u,int v){
	To[++tot]=v;
	Next[tot]=Head[u];
	Head[u]=tot;
}
bool cmp(int a,int b){
	return a<b;
}
void dfs(int u,int fa,int dep){
	vis[u]=1; Deep[u]=dep; Father[u]=fa;
	int cnt=0;
	a[++mm]=u;
	for (int i=Head[u];i;i=Next[i]){
		int v=To[i];
		if (v==fa) continue;
		if (Cut[u]&&Cut[v]) continue;
		s[++cnt]=v;
	}sort(s+1,s+cnt+1,cmp);
	for (int i=1;i<=cnt;i++) S[u][i]=s[i];
	for (int i=1;i<=cnt;i++){
		if (vis[S[u][i]]==0) dfs(S[u][i],u,dep+1);
	}
	return;
}
int find(int x){
	if (f[x]!=x) f[x]=find(f[x]);
	return f[x];
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++) f[i]=i;
	for (int i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		int X=find(u),Y=find(v);
		if (X==Y){
			Ax=u; Ay=v;
			F=1;
		}else{
			f[X]=Y;
			add(u,v); add(v,u);
		}
	}dfs(1,0,1);
	for (int i=1;i<=n;i++) ans[i]=a[i];
	add(Ax,Ay); add(Ay,Ax);
	g=1; Cutx[1]=Ax; Cuty[1]=Ay;
	int sx=Ax,sy=Ay;
	if (Deep[sx]<Deep[sy]) swap(sx,sy);
	while (Deep[sx]>Deep[sy]){
		Cutx[++g]=Father[sx]; Cuty[g]=sx;
		sx=Father[sx];
	}
	while (sx!=sy){
		Cutx[++g]=Father[sx]; Cuty[g]=sx;
		Cutx[++g]=Father[sy]; Cuty[g]=sy;
		sx=Father[sx]; sy=Father[sy];
	}
	if (F==1)
	for (int i=1;i<=g;i++){
		for (int j=1;j<=n;j++) vis[j]=0;
		Cut[Cutx[i]]=1; Cut[Cuty[i]]=1;
		mm=0;
		dfs(1,0,1);
		Cut[Cutx[i]]=0; Cut[Cuty[i]]=0;
		for (int j=1;j<=n;j++){
			if (a[j]>ans[j]) break;
			if (a[j]<ans[j]){
				for (int l=1;l<=n;l++){
					ans[l]=a[l];
				}
				break;
			}
		}
	}
	for (int i=1;i<n;i++) printf("%d ",ans[i]); printf("%d",ans[n]);
	return 0;
}
