#include <cstdio>
#include <cstring>
#include <algorithm>
#include <queue>
#include <vector>
#define N 5005
#define For(i,l,r) for(int i=l;i<=r;i++)
#define Dor(i,l,r) for(int i=l;i>=r;i--)
#define ll long long
using namespace std;

queue <int> q;
vector <int> to[N];
int n,m,vis[N],nxt[N];
void dfs(int x){	
	printf("%d ",x);
	vis[x]=1;				
	int minn=N;
	for(int i=0;i<to[x].size();i++){
		int v=to[x][i];
		if (!vis[v]){
			vis[v]=1;
			dfs(v);
		}
	}
}
priority_queue <int,vector<int>,greater<int> > Q;
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	For(i,1,m){
		int x,y;
		scanf("%d%d",&x,&y);
		to[x].push_back(y);
		to[y].push_back(x);
	}
	For(i,1,n){
		for(int j=0;j<to[i].size();j++){
			Q.push(to[i][j]);
		}
		for(int j=0;j<to[i].size();j++){
			to[i][j]=Q.top();
			Q.pop();
		}
	}
	dfs(1);
	printf("\n");
	fclose(stdin);fclose(stdout);
	return 0;
}
