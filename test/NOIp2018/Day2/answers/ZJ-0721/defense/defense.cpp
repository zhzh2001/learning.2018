#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <ctime>
#include <cstdlib>
#define inf 2100000000
#define For(i,l,r) for(ll i=l;i<=r;i++)
#define Dor(i,l,r) for(ll i=l;i>=r;i--)
#define N 100005
#define ll long long
using namespace std;
		
ll a[N],v,cnt=0,nxt[N],flag[N],head[N],ans;
ll n,m,cost[N],is_root[N],f[N][2],sum;
char s[10];
vector <int> son[N];
int main(){
	srand((int)time(NULL));
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(flag,0,sizeof(flag));
	scanf("%lld%lld",&n,&m);
	scanf("%s",s);
	For(i,1,n) scanf("%d",&cost[i]),sum+=cost[i];
		For(i,0,cnt) For(j,0,1) f[i][j]=inf;
		For(i,1,n-1){
			int x,y;
			scanf("%d%d",&x,&y);
			head[y]=x;
			nxt[x]=y;
			is_root[y]=1;
		}
		For(i,1,n) if (!is_root[i]) v=i;
		for(int i=v;i;i=nxt[i]){
			a[++cnt]=i;
		}
		f[0][0]=0;
		f[0][1]=0;
		For(i,1,cnt){
			f[i][1]=min(f[i-1][0]+cost[a[i]],f[i-1][1]+cost[a[i]]);
			f[i][0]=f[i-1][1];
		}
		ans=min(f[cnt][1],f[cnt][0]);
		For(i,1,m){
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			ll s=rand()%sum+1;
			if (s<=10) printf("-1\n");
				else printf("%lld\n",s);
		}
	fclose(stdin);fclose(stdout);
	return 0;
}
