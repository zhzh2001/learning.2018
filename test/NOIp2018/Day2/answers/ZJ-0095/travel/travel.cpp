#include <bits/stdc++.h>
#define rep(i, n) for (rint i = 1; i <= (n); i ++)
#define re0(i, n) for (rint i = 0; i < (int) n; i ++)
#define travel(i, u) for (rint i = head[u]; i; i = e[i].nxt)
#define rint register int
using namespace std;

template <class T> inline void read(T &x) {
  x = 0; char c = getchar(); int f = 0;
  for (; c < '0' || c > '9'; f |= c == '-', c = getchar());
  for (; c >= '0' && c <= '9'; x = x * 10 + c - '0', c = getchar());
  if (f) x = -x;
}
#define pb push_back
const int N = 5111;
const int M = N * 2;
struct E {
  int nxt, to;
}e[M];
int head[N], e_cnt = 0;
inline void add(int x, int y) {
  e[++ e_cnt] = (E) {head[x], y}; head[x] = e_cnt;
}
pair <int, int> r[M];
int ans_cnt = 0, a_cnt = 0, a[N], ans[N];
bool vis[N];
int n, m, bx, by;
vector <int> G[N];

inline void up(void) {
  if (a_cnt != n) return ;
  if (!ans_cnt) {
    ans_cnt = a_cnt;
    rep (i, a_cnt) ans[i] = a[i];
  } else {
    rep (i, n) if (a[i] != ans[i]) {
      if (a[i] < ans[i]) {
	rep (i, a_cnt) ans[i] = a[i];
      }
      else return ;
    }
  }
}

inline void dfs(int u, int fat) {
  a[++ a_cnt] = u; vis[u] = true;
  travel (i, u) {
    rint v = e[i].to;
    if (vis[v]) continue;
    if (u == bx && v == by || u == by && v == bx) continue;
    dfs(v, u);
  }
}

int main(void) {
  freopen("travel.in", "r", stdin);
  freopen("travel.out", "w", stdout);
  read(n); read(m);
  rep (i, m) {
    int x, y; read(x); read(y);
    if (x > y) swap(x, y);
    r[i] = make_pair(x, y);
  }
  sort(r + 1, r + m + 1);
  m = unique(r + 1, r + m + 1) - r - 1;
  rep (i, m) {
    int x = r[i].first, y = r[i].second;
    G[x].pb(y); G[y].pb(x);
  }
  rep (i, n) sort(G[i].begin(), G[i].end());
  rep (i, n) {
    for (rint k = (int) G[i].size() - 1; ~k; -- k)
      add(i, G[i][k]);
  }
  if (m == n - 1) {
    a_cnt = 0;
    memset(vis, 0, sizeof vis);
    dfs(1, 0);
    up();
  } else {
    rep (i, m) {
      bx = r[i].first; by = r[i].second;
      memset(vis, 0, sizeof vis);
      a_cnt = 0;
      dfs(1, 0);
      up();
    }
  }
  rep (i, n) cout << ans[i] << " "; cout << "\n";
}
