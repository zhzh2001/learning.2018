#include <bits/stdc++.h>
#define rep(i, n) for (rint i = 1; i <= (n); i ++)
#define re0(i, n) for (rint i = 0; i < (int) n; i ++)
#define travel(i, u) for (rint i = head[u]; i; i = e[i].nxt)
#define rint register int
using namespace std;

template <class T> inline void read(T &x) {
  x = 0; char c = getchar(); int f = 0;
  for (; c < '0' || c > '9'; f |= c == '-', c = getchar());
  for (; c >= '0' && c <= '9'; x = x * 10 + c - '0', c = getchar());
  if (f) x = -x;
}
const int mo = 1e9 + 7;
int n, m;

inline int mul(int x, int y) {
  return 1LL * x * y % mo;
}

inline int pw(int a, int k) {
  int ans = 1;
  for (; k; k >>= 1, a = mul(a, a))
    if (k & 1) ans = mul(ans, a);
  return ans;
}

int main(void) {
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);
  read(n); read(m);
  if (n > m) swap(n, m);
  if (n == 1) {
    int ans = pw(2, m);
    cout << ans << "\n";
  } else if (n == 2) {
    int ans = mul(12, pw(3, m - 2));
    cout << ans << "\n";
  } else if (n == 3) {
    int ans = mul(112, pw(3, m - 3));
    cout << ans << "\n";
  } else if (n == 4) {
    if (m == 4) {
      cout << "912\n";
    } else {
      int ans = mul(2688, pw(3, m - 5));
      cout << ans << "\n";
    }
  } else if (n == 5)  {
    if (m == 5) {
      cout << "7136\n";
    } else {
      int ans = mul(21312, pw(3, m - 6));
      cout << ans << "\n";
    }
  } else if (n == 6) {
    if (m == 6) {
      cout << "56768\n";
    } else  {
      int ans = mul(170112, pw(3, m - 7));
      cout<<ans<<"\n";
    }
  } else if (n == 7) {
    if (m == 7) {
      cout << "453504\n";
    } else if (m == 8) {
      cout << "\n";
    }
  } else if (n == 8) {
    if (m == 8) {
      cout << "\n";
    }
  } else {
    cout << "23336666\n";
  }
}
