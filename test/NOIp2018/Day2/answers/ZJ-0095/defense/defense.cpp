#include <bits/stdc++.h>
#define rep(i, n) for (rint i = 1; i <= (n); i ++)
#define re0(i, n) for (rint i = 0; i < (int) n; i ++)
#define travel(i, u) for (rint i = head[u]; i; i = e[i].nxt)
#define rint register int
using namespace std;

template <class T> inline void read(T &x) {
  x = 0; char c = getchar(); int f = 0;
  for (; c < '0' || c > '9'; f |= c == '-', c = getchar());
  for (; c >= '0' && c <= '9'; x = x * 10 + c - '0', c = getchar());
  if (f) x = -x;
}
#define int long long
char type[233]; int n, m;
namespace lian {
  const int N = 3e5 + 233;
  const int inf = 1e13;
  struct E {
    int nxt, to;
  }e[N << 2];
  int head[N], e_cnt = 0;
  int p[N], dp[N][2], col[N];
  struct node {
    int d[2][2];
    inline void init(void) {
      d[0][0] = d[0][1] = d[1][0] = d[1][1] = inf;
    }
    void ovo() {
      cout << d[0][0] << " " << d[0][1] << " " << d[1][0] << " " << d[1][1] << "\n";
    }
  };

#define mid (l + (r - l) / 2)
#define ls (rt << 1)
#define rs (rt << 1 | 1)
  namespace bit {
    node tr[N << 2];
    node make(node a, node b) {
      node ans; ans.init();
      re0 (k1, 2) re0 (k2, 2)
	ans.d[k1][k2] = min(min(ans.d[k1][k2], a.d[k1][1] + b.d[0][k2]),
			    min(a.d[k1][0] + b.d[1][k2], a.d[k1][1] + b.d[1][k2]));
      return ans;
    }
    inline void build(int l, int r, int rt) {
      if (l == r) {
	tr[rt].init();
	tr[rt].d[0][0] = 0; tr[rt].d[1][1] = p[l];
	return ;
      }
      build(l, mid, ls);
      build(mid + 1, r, rs);
      tr[rt] = make(tr[ls], tr[rs]);
    }
    node query(int ql, int qr, int l, int r, int rt) {
      if (ql == l && qr == r) {
	return tr[rt];
      }
      if (qr <= mid) return query(ql, qr, l, mid, ls);
      else if (ql > mid) return query(ql, qr, mid + 1, r, rs);
      else {
	node tmp = make(query(ql, mid, l, mid, ls),
			query(mid + 1, qr, mid + 1, r, rs));
	return tmp;
      }
    }
  }

  void main(void) {
    rep (i, n) read(p[i]);
    rep (i, n - 1) {
      int x, y; read(x); read(y);
    }
    bit::build(1, n, 1);
    rep (i, m) {
      int x, y, tx, ty; read(x); read(tx); read(y); read(ty);
      if (x > y) swap(x, y), swap(tx, ty);
      node a = bit::query(1, x, 1, n, 1);
      node b = bit::query(x, y, 1, n, 1);
      node c = bit::query(y, n, 1, n, 1);
      int ans = min(a.d[0][tx], a.d[1][tx]) 
	+ b.d[tx][ty] 
	+ min(c.d[ty][0], c.d[ty][1]) - p[x] * tx - p[y] * ty;
      if (ans > 1e11) cout << "-1\n";
      else cout << ans << "\n";
    }
  }
}

namespace bf {
  const int N = 3333;
  const int inf = 1e13;
  struct E {
    int nxt, to;
  }e[N << 2];
  int head[N], e_cnt = 0;
  int p[N], dp[N][2], col[N];

  inline void add(int x, int y) {
    e[++ e_cnt] = (E) {head[x], y}; head[x] = e_cnt;
  }

  inline void dfs(int u, int fat) {
    dp[u][0] = 0; dp[u][1] = p[u];
    travel (i, u) {
      int v = e[i].to;
      if (v != fat) {
	dfs(v, u);
	dp[u][0] += dp[v][1];
	dp[u][1] += min(dp[v][1], dp[v][0]);
      }
    }
    if (col[u] != -1) dp[u][col[u] ^ 1] = inf;
  }

  void main(void) {
    rep (i, n) read(p[i]);
    rep (i, n - 1) {
      int x, y; read(x); read(y);
      add(x, y); add(y, x);
    }
    rep (i, n) col[i] = -1;       
    rep (i, m) {
      int x, y;
      read(x); read(col[x]);
      read(y); read(col[y]);
      dfs(1, 0);
      // rep (k, n) cout << dp[k][1] << " " << dp[k][0] << "\n";
      int ans = min(dp[1][1], dp[1][0]);
      if (ans > 1e11) cout << "-1\n";
      else cout << ans << "\n";
      col[x] = col[y] = -1;
    }
  }

}

signed main(void) {
  freopen("defense.in", "r", stdin);
  freopen("defense.out", "w", stdout);
  read(n); read(m); scanf("%s", type + 1);
  if (type[1] == 'A') {
    lian::main();
  } else {
    bf::main();
  }
}
