#include <cstdio>
#include <cstring>
#include <algorithm>
using std :: sort;
inline void swap(int &x, int &y) {int tmp = x; x = y; y = tmp;}
inline void Maxed(int &x, int y) {if(y > x) x = y;}
const int INF = 1e9 + 7;

int n, m;
int first[5010], to[10010], nxt[10010], cnt = 0;
inline void addE(int u, int v){
	to[++cnt] = v;
	nxt[cnt] = first[u];
	first[u] = cnt;
}
int ans[5010], len = 0, son[50010], pl = 0;
int fa[5010], dep[5010], able, onc[5010], choice = INF, lca = 0;

void findfa(int u, int FA){
	dep[u] = dep[FA] + 1;
	fa[u] = FA;
	for(int p = first[u]; p; p = nxt[p]){
		int v = to[p];
		if(v == FA)
			continue;
		findfa(v, u);
	}
}

void dfs(int u){
	ans[++len] = u;
	
	int st = pl + 1;
	for(int p = first[u]; p; p = nxt[p]){
		int v = to[p];
		if(v == fa[u])
			continue;
		son[++pl] = v;
	}
	int ed = pl;
	sort(son + st, son + ed + 1);
	for(int i = st; i <= ed; ++i){
		int v = son[i];
		dfs(v);
	}//是树，则我的子树必须走完 
}
void Tree(){
	for(int i = 1; i <= m; ++i){
		int u, v;
		scanf("%d %d", &u, &v);
		addE(u, v);
		addE(v, u);
	}
	able = 0;
	findfa(1, 0);
	dfs(1);
	//exit
}

int vis[5010];
void Smartdfs(int u){
	ans[++len] = u;
	vis[u] = 1;
	int st = pl + 1;
	for(int p = first[u]; p; p = nxt[p]){
		int v = to[p];
		if(v == fa[u] || vis[v])
			continue;
		son[++pl] = v;
	}
	int ed = pl;
	sort(son + st, son + ed + 1);
	
	//what if back?
	for(int i = st; i <= ed; ++i){
		int v = son[i];
		if(able && onc[v] && choice < v and i == ed){
			able = 0;
			continue;
		}
		if(able && onc[u] && i != ed){
			if(choice == INF)
				choice = son[i+1];
			else
				Maxed(choice, son[i+1]);
		}
		Smartdfs(v);//走完小子树，到了环……比剩下子树还小？走了，必须走，因为这个点的子树必须搞定才能离开，仅仅环那一方可以交给合作伙伴
	}
}

int pa[5010];
int find(int u){
	return (u == pa[u]) ? u : (pa[u] = find(pa[u]));
}

void Circle(){
	int c, d;
	for(int i = 1; i <= n; ++i)
		pa[i] = i;
		
	for(int i = 1; i <= m; ++i){
		int u, v;
		scanf("%d %d", &u, &v);
		if(find(u) == find(v)){
			c = u, d = v;
			continue;
		}
		pa[find(u)] = find(v);
		addE(u, v);
		addE(v, u);
	}
	
	dep[0] = 0;
	findfa(1, 0);
	
	addE(c, d);
	addE(d, c);
	
	onc[c] = onc[d] = 1;
	while(dep[c] != dep[d]){
		if(dep[c] < dep[d])
			swap(c, d);
		c = fa[c];
		
		onc[c] = 1;
	}
	while(c != d)
		c = fa[c], d = fa[d], onc[c] = onc[d] = 1;
	lca = c;
	able = 1;
	Smartdfs(1);
}
void link(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
}
int main(){
	link();
	memset(onc, 0, sizeof(onc));
	scanf("%d %d", &n, &m);
	if(m == n-1)
		Tree();
	else
		Circle();
	for(int i = 1; i <= n; ++i)
		printf("%d", ans[i]), putchar((i == n) ? '\n' : ' ');
	return 0;
}
