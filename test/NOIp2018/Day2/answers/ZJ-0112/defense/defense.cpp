#include <cstdio>
#include <cctype>
#include <cstring>
inline int getint(){
	int ch = getchar(), res = 0;
	while(!isdigit(ch) and ch != EOF)
		ch = getchar();
	while(isdigit(ch))
		res = res * 10 + (ch - '0'), ch = getchar();
	return res;
}
#define re register
inline long long min(long long x, long long y) {return (x < y) ? x : y;}

long long ans = 0;
int n, m, P[100010];
int first[100010], to[200010], nxt[200010], cnt = 0;
inline void addE(int u, int v){
	to[++cnt] = v;
	nxt[cnt] = first[u];
	first[u] = cnt;
}
int need[100010], ill[100010];

int dep[100010], fa[100010];
void dfs(int u, int FA){
	dep[u] = dep[FA] + 1;
	fa[u] = FA;
	for(int p = first[u]; p; p = nxt[p]){
		int v = to[p];
		if(v == FA)
			continue;
		dfs(v, u);
	}
}

long long cost[100010][2];
const long long INF = (long long)(1e9 + 7) * (long long)(1e9 + 7);
void find(int u){
	for(int p = first[u]; p; p = nxt[p]){
		int v = to[p];
		if(v == fa[u])
			continue;
		find(v);
		cost[u][0] += cost[v][1];
		cost[u][1] += min(cost[v][0], cost[v][1]);
	}
	if(ill[u])
		cost[u][1] = INF;
	if(need[u])
		cost[u][0] = INF;
	
	cost[u][1] += P[u];
}

int a[3], s[3];
int Gate(){
	for(re int i = 1; i <= 2; ++i){
		a[i] = getint(), s[i] = getint();
		if(s[i])
			need[a[i]] = 1;
		else
			ill[a[i]] = 1;
	}
	
	if(ill[a[1]] && ill[a[2]])
		for(re int p = first[a[1]]; p; p = nxt[p])
			if(to[p] == a[2]){
				puts("-1");
				for(re int i = 1; i <= 2; ++i)
					need[ a[i] ] = ill[ a[i] ] = 0;				
				return 0;
			}
	
	memset(cost, 0, sizeof(cost));
	find(1);

	printf("%lld\n", min(cost[1][0], cost[1][1]));
	for(re int i = 1; i <= 2; ++i)
		need[ a[i] ] = ill[ a[i] ] = 0;
	return 0;
}

void link(){
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
}

int main(){
	link();
	n = getint(), m = getint();
	getchar(), getchar();//good
	for(re int i = 1; i <= n; ++i)
		P[i] = getint();
	for(re int i = 1; i <= n-1; ++i){
		int u = getint(), v = getint();
		addE(u, v);
		addE(v, u);
	}
	dfs(1, 0);
	while(m--)
		Gate();
	return 0;
}
