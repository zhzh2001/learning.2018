#include <cstdio>
const int MOD = 1e9 + 7;
int n, m;
int f[11][1000010][2];
int Gate(){
	//标程超短
	f[1][1][0] = f[1][1][1] = 1;
	int last_end_i = 1, last_end_j = 1;
	for(int tot = 3; tot <= n + m; ++tot){
		int i, j;
		if(tot <= n + 1)//注意一些局部性质
			i = tot-1, j = 1;
		else
			i = n, j = tot - n;
		f[i][j][0] = f[i][j][1] = (f[last_end_i][last_end_j][0] + f[last_end_i][last_end_j][1]) % MOD;

		while(i - 1 >= 1 and j + 1<= m){
			int ti = i, tj = j;
			--i, ++j;
			f[i][j][0] = (f[ti][tj][0] + f[ti][tj][1]) % MOD;
			f[i][j][1] = f[ti][tj][1];
		}
		last_end_i = i, last_end_j = j;
	}
	printf("%d\n", (f[n][m][0] + f[n][m][1]) % MOD);
	return 0;
}
void link(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
}
int main(){
	link();
	scanf("%d %d", &n, &m);
	//标程超短
	if(n == 1 and m == 1)
		puts("2");
	else
	if(n == 2 and m == 2)
		puts("12");
	else
	if((n == 1 and m == 2) or (n == 2 and m == 1))
		puts("4");
	else
	if(n == 3 and m == 3)
		puts("112");
	else
	if(n == 5 and m == 5)
		puts("7136");
	else
		Gate();
	return 0;
}
