#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstring>
#include<vector>
#include<queue>
#include<algorithm>
using namespace std;

#define ll long long
#define maxn (5000+10)
int read(){
	#define isnum (c>='0' && c<='9')
	int x=0,f=1; char c=getchar();
	for(;!isnum;c=getchar())if(c=='-')f=-1;
	for(; isnum;c=getchar())x=x*10+c-48;
	return x*f;
}

vector<int> a[maxn],st;
int vis[maxn],biaoji[maxn],deep[maxn];
int n,m,u,v;
int xuanze_f=0,xuanze_p=0,xuanze_x=0;

void tanxin(int x){
	if(vis[x])return;
	vis[x]=1;
	printf("%d ",x);
	for(int i=0;i<(int)a[x].size();i++)
		tanxin(a[x][i]);
}

bool tanxin2(int x){
	if(vis[x])return 0;
	vis[x]=1;
	printf("%d ",x);
	for(int i=0;i<(int)a[x].size();i++){
		int p=a[x][i]; if(vis[p])continue;
		if(!xuanze_f && biaoji[x] && biaoji[p]){
			if(xuanze_x>0 && p>xuanze_p){
				xuanze_f=1;
				return 1;
			}
			if(i!=(int)a[x].size()-1){
				xuanze_x=x;
				xuanze_p=a[x][i+1];
			}
		}
		int f=tanxin2(p);
		if(f && x!=xuanze_x)return 1;//递归 
	}
	return 0;
}

void bj(int x,int d){
	if(vis[x])return;
	vis[x]=1;
	deep[x]=d;
	st.push_back(x);
	
	for(int i=0;i<(int)a[x].size();i++){
		int p=a[x][i];
		bj(p,d+1);
		if(deep[p]<deep[x]-1){
			while(st.back()!=p){
				biaoji[st.back()]=1;
				st.pop_back();
			}
			biaoji[st.back()]=1;
			st.pop_back();
		}
	}
	
	if(!st.empty() && st.back()==x)st.pop_back();
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(); m=read();
	for(int i=0;i<m;i++){
		u=read(); v=read();
		a[u].push_back(v);
		a[v].push_back(u);
	}
	for(int i=1;i<=n;i++)
		sort(a[i].begin(),a[i].end());
	if(m==n-1)
	{tanxin(1);return 0;}//第一种情况 
	bj(1,1);
	for(int i=1;i<=n;i++)vis[i]=0;
	tanxin2(1);//第二种情况 
	
	return 0;
}
