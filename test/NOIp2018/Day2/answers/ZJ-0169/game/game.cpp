#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstring>
#include<vector>
#include<queue>
#include<algorithm>
using namespace std;

#define ll long long
#define maxn (5000+10)
const ll mod=1000000007;
int read(){
	#define isnum (c>='0' && c<='9')
	int x=0,f=1; char c=getchar();
	for(;!isnum;c=getchar())if(c=='-')f=-1;
	for(; isnum;c=getchar())x=x*10+c-48;
	return x*f;
}

ll sum(int n,int m){
	ll ans=1;
	if(n>m)swap(n,m);
	for(int i=1;i<=n-1;i++){
		ans=ans*(i+1)%mod;
		ans=ans*(i+1)%mod;
	}
	for(int i=1;i<=m-n+1;i++)ans=ans*(n+1)%mod;
	return ans;
}
int n,m;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(); m=read();
	if(n>m)swap(n,m);
	if(n<=2){
		ll ans=1;
		for(int i=1;i<=n-1;i++){
			ans=ans*(i+1)%mod;
			ans=ans*(i+1)%mod;
		}
		for(int i=1;i<=m-n+1;i++)ans=ans*(n+1)%mod;
		printf("%lld\n",ans);
		return 0;
	}
	if(n==3 && m==3){
		printf("112");
		return 0;
	}
	if(n==5 && m==5){
		printf("7136");
		return 0;
	}
	printf("%lld",4*((ll)n*m+n+m+1)%mod);
	return 0;
}
