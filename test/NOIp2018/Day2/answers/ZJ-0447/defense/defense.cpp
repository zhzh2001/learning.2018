#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

typedef long long ll;
const int N=1e5+10;
const ll inf=1ll<<50;
struct side{
	int to,nt;
}s[N<<1];
int n,m,w[N],num,h[N],a,b,w1,w2;
ll f[N][2];
char str[10];

inline void add(int x,int y){
	s[++num]=(side){y,h[x]},h[x]=num;
	s[++num]=(side){x,h[y]},h[y]=num;
}

inline void AMOD(ll &a,ll b){
	if (b>=inf) a=inf; else a+=b;
}

inline void so(int x,int fa){
	for (int i=h[x]; i; i=s[i].nt)
		if (s[i].to!=fa){
			so(s[i].to,x);
			AMOD(f[x][0],f[s[i].to][1]);
			AMOD(f[x][1],min(f[s[i].to][0],f[s[i].to][1]));
		}
	f[x][1]+=w[x];
	if (x==a) f[x][w1^1]=inf;
	if (x==b) f[x][w2^1]=inf;
	return;
}

int main(void){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,str);
	for (int i=1; i<=n; ++i) scanf("%d",&w[i]);
	for (int i=1,x,y; i<n; ++i) scanf("%d%d",&x,&y),add(x,y);
	while (m--){
		scanf("%d%d%d%d",&a,&w1,&b,&w2),memset(f,0,sizeof f);
		so(1,0),printf("%lld\n",min(f[1][0],f[1][1])>=inf?-1:min(f[1][0],f[1][1]));
	}
	return 0;
}
