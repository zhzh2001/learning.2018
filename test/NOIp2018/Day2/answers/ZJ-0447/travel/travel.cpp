#include <cstdio>
#include <queue>
using namespace std;

const int N=5010;
struct side{
	int to,nt;
}s[N<<1];
int n,m,num,h[N],ans[N],cnt,sk[N],top,c[N],len;
bool b[N],vis[N],bo[N],used[N];

inline void add(int x,int y){
	s[++num]=(side){y,h[x]},h[x]=num;
	s[++num]=(side){x,h[y]},h[y]=num;
}

inline int pos(int x){
	if (x==0) return len;
	if (x==len+1) return 1;
	return x;
}

inline void dfs(int x,int fa){
	if (used[x]) return;
	used[x]=1;
	if (!vis[x]) ans[++cnt]=x;
	priority_queue <int,vector<int>,greater<int> > que;
	for (int i=h[x]; i; i=s[i].nt)
		if (s[i].to!=fa&&!vis[s[i].to]) que.push(s[i].to);
	while (!que.empty()){
		int p=que.top(); que.pop();
		dfs(p,x);
	}
	return;
}

inline void work(int x,int k){
	used[k]=1,ans[++cnt]=x;
	int wz,op,st,from;
	for (int i=1; i<=len; ++i) if (c[i]==x) {wz=st=from=i;break;}
	if (c[pos(from-1)]<c[pos(from+1)]) op=-1; else op=1;
	
	while (c[pos(wz+op)]<c[pos(from-op)]){
		wz=pos(wz+op),ans[++cnt]=c[wz];
		int tmp=1e9,l=0,r=0;
		for (int i=h[c[wz]]; i; i=s[i].nt)
			if (!vis[s[i].to]){
				if (s[i].to<c[pos(wz+op)]) dfs(s[i].to,c[wz]),++l,used[s[i].to]=1;
				++r;
			}
		if (l<r) bo[wz]=1;
	}
	for (int i=wz; i!=from; i=pos(i-op))
		if (bo[i]) dfs(c[i],0);
	
	for (int i=h[x]; i; i=s[i].nt)
		if (s[i].to<c[pos(from-op)]) dfs(s[i].to,x),used[s[i].to]=1;
	
	while (1){
		st=pos(st-op);
		if (st==wz) break;
		ans[++cnt]=c[st];
		int tmp=1e9,l=0,r=0;
		for (int i=h[c[st]]; i; i=s[i].nt)
			if (!vis[s[i].to]){
				if (s[i].to<c[pos(st-op)]) dfs(s[i].to,c[st]),++l,used[s[i].to]=1;
				++r;
			}
		if (l<r) bo[st]=1;
	}
	for (int i=pos(st+op); i!=from; i=pos(i+op))
		if (bo[i]) dfs(c[i],0);
	
	dfs(x,k);
	
	return;
}

inline void so(int x,int fa){
	if (used[x]) return;
	used[x]=1;
	ans[++cnt]=x;
	priority_queue <int,vector<int>,greater<int> > que;
	for (int i=h[x]; i; i=s[i].nt)
		if (s[i].to!=fa) que.push(s[i].to);
	while (!que.empty()){
		int p=que.top(); que.pop();
		if (vis[p]){
			work(p,x);
			break;
		}
		so(p,x);
	}
	return;
}

inline bool find(int x,int fa){
	sk[++top]=x,b[x]=1;
	for (int i=h[x]; i; i=s[i].nt)
		if (s[i].to!=fa)
			if (!b[s[i].to]){
				if (find(s[i].to,x)) return 1;
			}
			else {
				while (1){
					int p=sk[top--];
					vis[p]=1,c[++len]=p;
					if (p==s[i].to) break;
				}
				return 1;
			}
	return --top,0; 
}

int main(void){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,x,y; i<=m; ++i) scanf("%d%d",&x,&y),add(x,y);
	if (m==n-1){
		so(1,0);
		for (int i=1; i<=n; ++i) printf("%d%c",ans[i]," \n"[i==n]);
		return 0;
	}
	find(1,0),so(1,0);
	for (int i=1; i<=n; ++i) printf("%d%c",ans[i]," \n"[i==n]);
	return 0;
}
