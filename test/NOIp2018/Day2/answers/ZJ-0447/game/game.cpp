#include <cstdio>
using namespace std;

const int mod=1e9+7;
int n,m;

inline int pow(int x,int y){
	int ret=1;
	for (;y;y>>=1,x=1ll*x*x%mod) if (y&1) ret=1ll*ret*x%mod;
	return ret;
}

int main(void){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==2) return printf("%d\n",4ll*pow(3,m-1)%mod),0;
	if (n==1) return printf("%d\n",pow(2,m)),0;
	if (m==1) return printf("%d\n",pow(2,n)),0;
	if (n==3&&m==2) return printf("%d\n",36),0;
	if (n==3&&m==3) return printf("%d\n",112),0;
	return 0;
}
