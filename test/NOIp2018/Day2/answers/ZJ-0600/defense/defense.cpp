#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=2001,M=4001;
const long long inf=1LL<<60;
int abs(int x){return x>0?x:-x;} 
int read(){int c,x=0;while(!isdigit(c=getchar()));while(x=(x<<3)+(x<<1)+c-'0',isdigit(c=getchar()));return x;}
char s[20];
int n,m,p[N];
int cnt,hed[N],nex[M],rec[M];
void add(int u,int v){nex[++cnt]=hed[u],rec[hed[u]=cnt]=v;}
int w,x,v,y;
int vis[N][2],can[N][2];
long long f[N][2];
long long DFS(int u,int k,int fa){
	if(vis[u][k])return f[u][k];can[u][k]=1;vis[u][k]=1;f[u][k]=k?p[u]:0;
	if(u==w&&k^x){can[u][k]=0;return inf;}
	if(u==v&&k^y){can[u][k]=0;return inf;}
	if(k==0)
	for(int j=hed[u];j;j=nex[j])if(rec[j]^fa)
	{
		DFS(rec[j],k^1,u);if(!can[rec[j]][k^1]){can[u][k]=0;break;}
		f[u][k]+=f[rec[j]][k^1];
	}
	if(k==1)
	for(int j=hed[u];j;j=nex[j])if(rec[j]^fa)
	{
		long long k1=DFS(rec[j],k^1,u),k2=DFS(rec[j],k,u);if(!can[rec[j]][k^1]&&!can[rec[j]][k]){can[u][k]=0;break;}
		f[u][k]+=min(k1,k2);
	}
	return can[u][k]?f[u][k]:inf;
}
void solve(){
	while(m--){
		w=read(),x=read(),v=read(),y=read();
		memset(vis,0,sizeof vis);
		long long ans=min(DFS(1,0,0),DFS(1,1,0));
		printf("%lld\n",ans==inf?-1:ans);
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();scanf("%s",s+1);
	for(int i=1;i<=n;++i) p[i]=read();
	for(int i=1;i<n;++i){
		int u=read(),v=read();
		add(u,v),add(v,u);
	}
	solve();return 0;
}
