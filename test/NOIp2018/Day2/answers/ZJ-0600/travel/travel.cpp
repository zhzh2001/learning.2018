#include<cstdio>
#include<cctype>
#include<vector>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=5001,M=10001;
char inchar(){static char c[100000],*x=c,*y=c;return x==y&&(y=(x=c)+fread(c,1,100000,stdin),x==y)?EOF:*x++;}
int read(){int c,x=0;while(!isdigit(c=inchar()));while(x=(x<<3)+(x<<1)+c-'0',isdigit(c=inchar()));return x;}
int n,m;
namespace M1{
	int cnt,vis[N],ans[N];
	vector<int> e[N];
	void DFS(int x){
		vis[x]=1,ans[++cnt]=x;
		for(int i=0;i<e[x].size();++i)
			if(!vis[e[x][i]]) DFS(e[x][i]);
	}
	void solve(){
		while(m--){
			int u=read(),v=read();
			e[u].push_back(v);
			e[v].push_back(u);
		}
		for(int i=1;i<=n;++i) sort(e[i].begin(),e[i].end());
		DFS(1);
		for(int i=1;i<=n;++i) if(i==1) printf("%d",ans[i]); else printf(" %d",ans[i]);
	}
}
namespace M2{
	int cnt,A,B,u[M],v[M],vis[N],now[N],ans[N];
	vector<int> e[N];
	void DFS(int x){
		vis[x]=1,now[++cnt]=x;
		for(int i=0;i<e[x].size();++i)
			if(x==A&&e[x][i]==B);
			else if(x==B&&e[x][i]==A);
			else if(!vis[e[x][i]]) DFS(e[x][i]);
	}
	void solve(){
		int yes=0;
		for(int i=1;i<=m;++i) u[i]=read(),v[i]=read();
		for(int i=1;i<=m;++i)
			e[u[i]].push_back(v[i]),
			e[v[i]].push_back(u[i]);
		for(int i=1;i<=n;++i) sort(e[i].begin(),e[i].end());
		for(int i=1;i<=m;++i){
			A=u[i],B=v[i];
			cnt=0;memset(vis,0,sizeof vis);DFS(1);
			if(cnt==n)
				if(!yes){yes=1;for(int j=1;j<=n;++j) ans[j]=now[j];}
				else
				for(int j=1;j<=n;++j)
					if(now[j]<ans[j]){
						for(int k=1;k<=n;++k) ans[k]=now[k]; break;
					}else if(now[j]>ans[j])break;
		}
		for(int i=1;i<=n;++i) if(i==1) printf("%d",ans[i]); else printf(" %d",ans[i]);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	if(m==n-1) M1::solve(); else M2::solve();
	return 0;
}
