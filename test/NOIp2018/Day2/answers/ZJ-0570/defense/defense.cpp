#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<cctype>
#define M 200010
#define ll long long
#define rt register int
#define r read()
#define l putchar('\n')
ll read(){
	ll x=0;char ch=getchar(),zf=1;
	while(!isdigit(ch)&&ch!='-')ch=getchar();
	if(ch=='-')ch=getchar(),zf=-1;
	while(isdigit(ch))x=x*10+ch-'0',ch=getchar();
	return x*zf;
}
void write(ll x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);l;}
using namespace std;
int i,j,k,m,n,x,y,z,cnt,t1,t2,v1,v2;
int F[M],L[M],N[M],a[M],v[M];
void add(int x,int y){
	a[++k]=y;
	if(!F[x])F[x]=k;
	else N[L[x]]=k;
	L[x]=k;
}
char opt[10];
ll f[100010][2];
void dfs(int x,int pre){
	for(rt i=F[x];i;i=N[i])if(a[i]!=pre){
		dfs(a[i],x);
		f[x][0]+=f[a[i]][1];
		f[x][1]+=min(f[a[i]][0],f[a[i]][1]);
	}
	f[x][1]+=v[x];
	if(t1==x)f[x][v1^1]=9999999999999999ll;
	if(t2==x)f[x][v2^1]=9999999999999999ll;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=r;m=r;
	scanf("%s",opt+1);
	for(rt i=1;i<=n;i++)v[i]=r;
	for(rt i=1;i<n;i++){
		x=r;y=r;
		add(x,y);
		add(y,x);
	}
	for(rt i=1;i<=m;i++){
		t1=r,v1=r,t2=r,v2=r;
		for(rt j=1;j<=n;j++)f[j][0]=f[j][1]=0;
		dfs(1,1);
		if(min(f[1][0],f[1][1])>=999999999999999)writeln(-1);else
		writeln(min(f[1][0],f[1][1]));
	}
	return 0;
}

