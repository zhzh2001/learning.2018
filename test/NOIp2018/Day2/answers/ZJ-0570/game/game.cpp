#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<cctype>
#define p 1000000007
#define ll long long
#define rt register int
#define r read()
#define l putchar('\n')
ll read(){
	ll x=0;char ch=getchar(),zf=1;
	while(!isdigit(ch)&&ch!='-')ch=getchar();
	if(ch=='-')ch=getchar(),zf=-1;
	while(isdigit(ch))x=x*10+ch-'0',ch=getchar();
	return x*zf;
}
void write(ll x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);l;}
using namespace std;
int i,j,k,m,n,x,y,z,cnt,sz;
ll ksm(ll x,ll y){
	if(!y)return 1;ll ew=1;
	while(y>1){
		if(y&1)y--,ew=x*ew%p;
		y>>=1,x=x*x%p;
	}return x*ew%p;
}
ll f[1000010];
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=r;m=r;
	if(n>m)swap(n,m);
	if(n==1)writeln(ksm(2,m));
	if(n==2)writeln(ksm(3,m-1)*4%p);
	if(n==3){
		f[1]=8;f[2]=36;
		for(rt i=3;i<=m;i++){
			f[i]=(f[i-1]*3+f[i-2])%p;
			if(i%4==3)f[i]-=4;
			if(i%4==0)f[i]-=12;
			if(i%4==1)f[i]+=4;
			if(i%4==2)f[i]+=12;
		}
		cout<<(f[m]%p+p)%p;
	}
	return 0;
}

