#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
int n,m;
string str;
int v[100010];
int vis[100010];
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	cin>>str;
	if(str[0]=='A')
	{
		for(int i=1;i<=n;i++)
		  scanf("%d",&v[i]);
		for(int i=1;i<n;i++)
		{
			int a,b;
			scanf("%d%d",&a,&b);
		}
		for(int j=1;j<=m;j++)
		{
			int a,b,c,d,flag=0;
			long long dp[100010][2];
			for(int i=1;i<=n;i++)
			  dp[i][0]=0x3f3f3f3f,dp[i][1]=0x3f3f3f3f;
			memset(vis,0,sizeof(vis));
			scanf("%d%d%d%d",&a,&b,&c,&d);
			vis[a]=b+1;
			vis[c]=d+1;
			dp[1][1]=v[1];
			dp[1][0]=0;
			for(int i=2;i<=n;i++)
			{
				if(vis[i]==2 && vis[i-1]==2)
				  dp[i][1]=dp[i-1][1]+v[i];
				else
				if(vis[i]==1 && vis[i-1]==2)
				  dp[i][0]=dp[i-1][1]+v[i];
			    else
			    if(vis[i]==1 && vis[i-1]==1)
			    {
			    	flag=1;
			    	break;
				}
				else
				if(vis[i]==2 && vis[i-1]==1)
				  dp[i][1]=dp[i-1][0]+v[i];
				else
				{
					dp[i][0]=dp[i-1][1];
					dp[i][1]=min(dp[i-1][0],dp[i-1][1])+v[i];
				}
			}
			if(flag)
			  printf("-1\n");
			else
			  printf("%lld\n",min(dp[n][1],dp[n][0]));
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
