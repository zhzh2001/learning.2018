#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <queue>
#include <vector>
using namespace std;

priority_queue <int> q;

int n,m;
int fa[5010];
struct node
{
	int nxt,to;
}ed[10100];
int las[5010],len;
bool mp[5010][5010];
int hx,hy;
int huan[5010];
bool hflag=0;
bool vis[5010];
int fid(int x)
{
	int f=fa[x];
	while(f!=fa[f])
	{
		f=fa[f];
	}
	fa[x]=f;
	return f;
}

void build(int x,int y)
{
	ed[++len].to=y;
	ed[len].nxt=las[x];
	las[x]=len;
	if(x < y)
	{
	  int fx=fid(x),fy=fid(y);
	  if(fx==fy)
		hx=x,hy=y;
	  else
	    fa[fx]=fa[fy];
	}
	
}





void dfs(int no,int f)
{
	printf("%d ",no);
	for(int i=las[no];i;i=ed[i].nxt)
	{
		if(ed[i].to!=f)
		  dfs(ed[i].to,no);
	}
}

void dfs2(int no,int f)
{
	fa[no]=f;
	for(int i=las[no];i;i=ed[i].nxt)
	{
		int to=ed[i].to;
		if(to!=f)
		  if( !(   (to==hy && no==hx) || (to==hx && no==hy)))
	        dfs2(to,no);
	}
}

void dfs4(int no,int f)
{
	if(vis[no])
	  return ;
	vis[no]=1;
	printf("%d ",no);
	for(int i=las[no];i;i=ed[i].nxt)
		  if(ed[i].to!=f)
		    dfs4(ed[i].to,no);
}

inline void dfsh(int no,int f)
{
	if(vis[no])
	  return;
	if(no > -1*q.top())
	{
		hflag=1;
		return;
	}
	vis[no]=1;
	printf("%d ",no);
    for(int i=las[no];i;i=ed[i].nxt)
    {
    	int to=ed[i].to;
    	if(to!=f && huan[to])
    	  dfsh(to,no);
    	if(to!=f && !huan[to])
    	  dfs4(to,no);
    	if(hflag)
    	  return ;
	}
}

inline void dfs3(int no,int f)
{
	if(vis[no])
	  return ;
	vis[no]=1;
	printf("%d ",no);
	if(huan[no] && !hflag)
	{
		for(int i=las[no];i;i=ed[i].nxt)
		{
			int to=ed[i].to;
			if(huan[to] && to!=f)
			  q.push(-1*to);
			else
			  dfs3(to,no);
		}
			int as=-1*q.top();
			q.pop();
			dfsh(as,no);
			as=-1*q.top();
			dfs3(as,no);
	}
	else
	{
		for(int i=las[no];i;i=ed[i].nxt)
		  if(ed[i].to!=f)
		    dfs3(ed[i].to,no);
	}
}


int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		int a,b;
		scanf("%d%d",&a,&b);
		mp[a][b]=1;
		mp[b][a]=1;
	}
	for(int i=1;i<=n;i++)
	  fa[i]=i;
	
	for(int i=1;i<=5000;i++)
		for(int j=5000;j>=1;j--)
			if(mp[i][j])
			  build(i,j);
	if(m==n-1)
	{
		printf("1 ");
		for(int i=las[1];i;i=ed[i].nxt)
			dfs(ed[i].to,1);
	}
	else
	{
		for(int i=1;i<=n;i++)
		  fa[i]=0;
		for(int i=las[1];i;i=ed[i].nxt)
		{
			int to=ed[i].to;
			if( !( (hx==1 && hy==to) || (hy==1 && hx==to) ))
			  dfs2(to,1);
		}
		int fx=hx;
		while(fx!=0)
		{
			huan[fx]=huan[fx]^1;
			fx=fa[fx];
		}
		int fy=hy;
		int ddd=0;
		while(fy!=0)
		{
			if(!ddd && huan[fy])
			  ddd=fy;
			huan[fy]=huan[fy]^1;
			fy=fa[fy];
		}
		huan[ddd]=huan[ddd]^1;
		printf("1 ");
		vis[1]=1;
		if(huan[1] && !hflag)
		{
			for(int i=las[1];i;i=ed[i].nxt)
			{
			    int to=ed[i].to;
				if(huan[to])
				  q.push(-to);
				else
				  dfs3(to,1);
            }
            int as=-1*q.top();
            q.pop();
			dfsh(as,1);
			as=-1*q.top();
			dfs3(as,1);
		}
		else
			for(int i=las[1];i;i=ed[i].nxt)
				dfs3(ed[i].to,1);
		//print();
		
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
