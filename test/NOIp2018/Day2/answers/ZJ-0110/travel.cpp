#include<cstdio>
#include<algorithm>
#include<vector>
#include<cstring>
using namespace std;
int pre[1000005],viss[1000005],num,sum[1000005],top_e,n,Top,m,cnt,top,flag=0,vis[1000005],stack[1000005],instack[1000005],last[1000005],a[1000005],stack_e[1000005],ans[1000005],ANS[1000005];
vector<int> vec[5005];
struct node{
	int to,next;
}e[1000005];
void add(int a,int b){
	e[++cnt].to=b;
	e[cnt].next=last[a];
	last[a]=cnt;
}
void dfs1(int x,int fa){
	if (flag) return;
	vis[x]=1;
	stack[++top]=x;
	for (int i=last[x]; i; i=e[i].next){
		if (flag) return;
		int V=e[i].to;
		if (V==fa) continue;
		if (!vis[V]) {
			stack_e[++top_e]=i;
			dfs1(V,x);
			top_e--;
		}
		else{
			Top=1;
			ans[Top]=i;
			while (stack[top]!=V) {
				top--;				
				ans[++Top]=stack_e[top];
			}
			flag=1;
			return;
		}
	}
	top--;
}
int check(){
	for (int i=1; i<=n; i++)
		if (ANS[i]<sum[i]) return 0;
		else if (ANS[i]>sum[i]) return 1;
	return 0;
}
void update(){
	for (int i=1; i<=n; i++) ANS[i]=sum[i];
}
void dfs(int x,int fa){
	sum[++num]=x;
	viss[x]=1;
	int cnt=0;
	for (int i=last[x]; i; i=e[i].next){
		int V=e[i].to;
		if (vis[i]) continue;
		if (V==fa) continue;
		a[++cnt]=V;
	}
	sort(a+1,a+cnt+1);
	for (int i=1; i<=cnt; i++) vec[x].push_back(a[i]);
	for (int i=0; i<cnt; i++) if (!viss[vec[x][i]]) dfs(vec[x][i],x);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=m; i++){
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
		pre[cnt-1]=cnt;
		pre[cnt]=cnt-1;
	}
	top=0;
	for (int i=1; i<=n; i++){
		for (int j=1; j<=n; j++) vis[j]=0;
		dfs1(i,0);
		if (flag) break;
	}
	for (int i=1; i<=n; i++) ANS[i]=1e9;
	memset(vis,0,sizeof(vis));
	memset(viss,0,sizeof(viss));
	dfs(1,0);
	update();
	for (int i=1; i<=Top; i++) {
		for (int j=1; j<=n; j++) vec[j].clear();
		for (int j=1; j<=n; j++) viss[j]=0;
		num=0;
		vis[ans[i]]=vis[pre[ans[i]]]=1;
		dfs(1,0);
		vis[ans[i]]=vis[pre[ans[i]]]=0;
		if (check()) update();
	}
	for (int i=1; i<=n; i++) printf("%d ",ANS[i]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
