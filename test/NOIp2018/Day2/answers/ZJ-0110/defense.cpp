#include<cstdio>
#include<algorithm>
using namespace std;
char cas[1000005];
int n,m,cnt,last[1000005],F[1000005][2],vis[1000005][2],a[1000005];
struct node{
	int to,next;
}e[1000005];
void add(int a,int b){
	e[++cnt].to=b;
	e[cnt].next=last[a];
	last[a]=cnt;
}
void dfs(int x,int fa){
	F[x][0]=0;
	F[x][1]=a[x];
	for (int i=last[x]; i; i=e[i].next){
		int V=e[i].to;
		if (V==fa) continue;
		dfs(V,x);
		int x0=F[x][0],x1=F[x][1];
		int y0=F[V][0],y1=F[V][1];
		F[x][0]=F[x][1]=1e9;
		F[x][0]=min(F[x][0],x0+y1);
		F[x][1]=min(F[x][1],x1+y0);
		F[x][1]=min(F[x][1],x1+y1);
	}
	if (vis[x][0]) F[x][0]=1e9;
	if (vis[x][1]) F[x][1]=1e9;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%s",cas);
	for (int i=1; i<=n; i++) scanf("%d",&a[i]);
	for (int i=1; i<n; i++){
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	while (m--){
		int a,x,b,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		vis[a][x^1]=1;
		vis[b][y^1]=1;
		dfs(1,0);
		vis[a][x^1]=0;
		vis[b][y^1]=0;
		int ans=min(F[1][0],F[1][1]);
		//printf("%d %d %d\n",F[2][0][0],F[2][0][1],F[2][1][1]);
	//	printf("%d %d %d\n",F[3][0][0],F[3][0][1],F[3][1][1]);
		if (ans==1e9) printf("-1\n");
		else printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
