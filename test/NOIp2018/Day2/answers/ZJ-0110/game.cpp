#include<cstdio>
#include<algorithm>
using namespace std;
const int mod=1e9+7;
int pow(int a,int b){
	int ans=1;
	while (b){
		if (b&1) ans=1ll*ans*a%mod;
		a=1ll*a*a%mod;
		b=b>>1;
	}
	return ans;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	if (n==1) printf("%d\n",pow(2,m));
	else if (n==2) printf("%lld\n",1ll*pow(3,m-1)*4%mod);
	else if (n==3){
		if (m==1) printf("%d\n",8);
		else if (m==2) printf("%d\n",-1);
		else printf("%d\n",112);
	}
	else printf("%d\n",7136);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
