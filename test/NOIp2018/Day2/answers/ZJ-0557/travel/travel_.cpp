#include <set>
#include <map>
#include <cmath>
#include <queue>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define fi first
#define se second
#define pii pair<int,int>
#define mp make_pair
typedef long long ll;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9'){f=ch=='-'?-f:f;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
	return x*f;
}
const int N=5010;
int fi[N*2],ne[N*2],la[N*2],a[N*2],b[N],tem[N],ans[N],tot;
int i,j,k,n,m,x,y,t,dfn[N],low[N],num,s[N],cir[N],cantx,canty;
priority_queue<int>q;
void add(int x,int y){
	k++;a[k]=y;
	if (fi[x]==0)fi[x]=k;else ne[la[x]]=k;
	la[x]=k;
}
bool ok(int x,int y){
	if (x==cantx&&y==canty)return 0;
	if (x==canty&&y==cantx)return 0;
	return 1;
}
void dfs(int x,int fa){
	tem[++tot]=x;
	vector<int>b;
	for (int i=fi[x];i;i=ne[i])if (a[i]!=fa&&ok(x,a[i]))b.push_back(a[i]);
	sort(b.begin(),b.end());
	for (int i=0;i<b.size();i++)dfs(b[i],x);
}
void tarjan(int x,int fa){
	dfn[x]=low[x]=++num;s[++s[0]]=x;
	for (int i=fi[x];i;i=ne[i])
		if (a[i]!=fa){
			if (!dfn[a[i]]){tarjan(a[i],x);low[x]=min(low[x],low[a[i]]);}
			else low[x]=min(low[x],dfn[a[i]]);
		}
	if (low[x]==dfn[x]){
		if (s[s[0]]==x){s[0]--;return;}
		while (s[s[0]]!=x)cir[++cir[0]]=s[s[0]],s[0]--;
		cir[++cir[0]]=x;s[0]--;
	}
}
bool check(){
	for (int i=1;i<=n;i++)if (ans[i]>tem[i])return 1;else if (ans[i]<tem[i])return 0;
	return 0;
}
int main(){
//	freopen("travel.in","r",stdin);
//	freopen("travel.out","w",stdout);
	n=read();m=read();
	if (m==n-1){
		for (i=1;i<=m;i++){
			x=read();y=read();add(x,y);add(y,x);
		}
		dfs(1,-1);
		for (i=1;i<=n;i++)printf("%d ",tem[i]);printf("\n");
	}
	else{
		for (i=1;i<=m;i++){
			x=read();y=read();add(x,y);add(y,x);
		}
		tarjan(1,-1);
//		for (i=1;i<=cir[0];i++)printf("%d ",cir[i]);printf("\n");
		ans[1]=n+1;
		for (i=1;i<=cir[0];i++){
			cantx=cir[i],canty=cir[i%cir[0]+1];
			tot=0;
			dfs(1,-1);
			if (check())memcpy(ans,tem,sizeof tem);
		}
		for (i=1;i<=n;i++)printf("%d ",ans[i]);printf("\n");
	}
	return 0;
}
