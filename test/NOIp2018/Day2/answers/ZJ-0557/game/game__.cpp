#include <bits/stdc++.h>
using namespace std;
const int mod=1000000007;
int i,j,k,n,m,x,y,t;
int f[9][300][300],p1[9],p2[9],p3[9];
bool ok(int x,int y,int t){
	for (int i=1;i<=n;i++)p1[i]=x&1,p2[i]=y&1,p3[i]=t&1,x>>=1,y>>=1,t>>=1;
	for (int i=n;i>1;i--)if (p2[i]<p3[i-1])return 0;
	if (x==-1)return 1;
	for (int i=2;i<n;i++)
		if (p1[i]==p2[i-1]){
			if (!p2[i]&&p2[i+1]&&!p3[i]&&!p3[i+1])return 0;
		}
	return 1;
}
int main(){
	scanf("%d%d",&n,&m);
	for (i=0;i<1<<n;i++)for (j=0;j<1<<n;j++)if (ok(-1,i,j))f[2][i][j]=1;
	for (i=3;i<=m;i++){
		for (int x1=0;x1<1<<n;x1++)
			for (int y1=0;y1<1<<n;y1++)
				if (f[i-1][x1][y1])
					for (int t1=0;t1<1<<n;t1++)
						if (ok(x1,y1,t1)){
							f[i][y1][t1]=(f[i][y1][t1]+f[i-1][x1][y1])%mod;
							printf("%d %d %d\n",x1,y1,t1);
						}
	}
	int ans=0;
	for (int i=0;i<1<<n;i++)for (int j=0;j<1<<n;j++)ans=(ans+f[n][i][j])%mod;
	printf("%d\n",ans);
	return 0;
}
