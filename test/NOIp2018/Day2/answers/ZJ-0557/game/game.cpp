//lyq i love you!
#include <set>
#include <map>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define fi first
#define se second
#define pii pair<int,int>
#define mp make_pair
typedef long long ll;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9'){f=ch=='-'?-f:f;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
	return x*f;
}
int i,j,k,n,m,x,y,t;
const int mod=1000000007;
int mi(int x,int y){
	if (y==0)return 1;
	int t=mi(x,y>>1);
	t=1ll*t*t%mod;
	return y&1?(1ll*x*t%mod):t;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1)printf("%d\n",mi(2,m));
	else if (n==2)printf("%lld\n",4ll*mi(3,n-1)%mod);
	else if (n==3){
		if (m==1)printf("8\n");
		else if (m==2)printf("36\n");
		else if (m==3)printf("112\n");
		else printf("%lld\n",112ll*mi(3,n-3)%mod);
	}
	else if (n==4){
		if (m==1)printf("16\n");
		else if (m==2)printf("108\n");
		else if (m==3)printf("336\n");
		else if (m==4)printf("912\n");
		else if (m==5)printf("2688\n");
		else printf("%lld\n",2688ll*mi(3,m-5)%mod);
	}
	return 0;
}
