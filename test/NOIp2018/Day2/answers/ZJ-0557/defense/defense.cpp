//lyq i love you!
#include <set>
#include <map>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define fi first
#define se second
#define pii pair<int,int>
#define mp make_pair
typedef long long ll;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9'){f=ch=='-'?-f:f;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
	return x*f;
}
const int N=100010;
int i,j,k,n,m,x,y,t,p[N];
int fi[N*2],ne[N*2],la[N*2],a[N*2],Fa[N],d[N];
long long f[N][2],g[N][2],v[N][2];
const ll inf=1e12;
char s[3];
void add(int x,int y){
	k++;a[k]=y;
	if (fi[x]==0)fi[x]=k;else ne[la[x]]=k;
	la[x]=k;
}
void dfs(int x,int fa){
	Fa[x]=fa;f[x][1]=v[x][1]+p[x];f[x][0]=v[x][0];d[x]++;
	for (int i=fi[x];i;i=ne[i])
		if (a[i]!=fa){
			d[a[i]]=d[x];
			dfs(a[i],x);
			f[x][0]+=f[a[i]][1];
			f[x][1]+=min(f[a[i]][0],f[a[i]][1]);
		}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();scanf("%s",s);
	for (i=1;i<=n;i++)p[i]=read();
	for (i=1;i<n;i++){
		x=read();y=read();
		add(x,y);add(y,x);
	}
	dfs(1,-1);
	while (m--){
		int s1,s2;
		x=read();s1=read();y=read();s2=read();
		v[x][1-s1]=inf;v[y][1-s2]=inf;
		dfs(1,-1);
		ll ans=min(f[1][0],f[1][1]);
		if (ans>=inf)ans=-1;
		v[x][1-s1]=0;v[y][1-s2]=0;
		printf("%lld\n",ans);
	}
}
