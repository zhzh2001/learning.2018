#include <bits/stdc++.h>
using namespace std;
#define ref(i,x,y)for(int i=x;i<=y;++i)
typedef long long LL;
const int mod=1e9+7;
int n,m,a[10][10];
int mi(int a,int b){
	int s=1;
	for(;b;b>>=1,a=(LL)a*a%mod)
		if(b&1)s=(LL)s*a%mod;
	return s;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	if(n==2){
		cout<<1LL*mi(3,m-1)*4%mod<<endl;
		return 0;
	}
	int ans=0;
	ref(I,0,(1<<(n*m))-1){
		ref(i,1,n)ref(j,1,m){
			int p=(i-1)*m+j-1;
			a[i][j]=I>>p&1;
		}
		bool flag=1;
		ref(i,2,n)ref(j,1,m-1)if(a[i][j]<a[i-1][j+1])flag=0;
		ref(i,2,n)ref(j,1,m-1)
			ref(ii,i+1,n)ref(jj,j+1,m-1){
				if(a[i][j]==a[i-1][j+1]&&a[ii][jj]>a[ii-1][jj+1])flag=0;
			}
		ans+=flag;
	}
	cout<<ans<<endl;
}
