#include <bits/stdc++.h>
using namespace std;
#define ref(i,x,y)for(int i=x;i<=y;++i)
#define eef(i,x)for(int i=head[x],y=e[i].to;i;i=e[i].next,y=e[i].to)
int read(){
	char c=getchar();int d=0,f=1;
	for(;c<'0'||c>'9';c=getchar())if(c=='0')f=-1;
	for(;c>='0'&&c<='9';d=d*10+c-48,c=getchar());
	return d*f;
}
const int N=10101;
int n,m,ans[N];
int cnt,head[N],Cnt;
struct edge{int to,next;}e[N],E[N];
bool cmp(edge a,edge b){return a.next>b.next;}
void add(int x,int y){
	E[++Cnt]=(edge){x,y};E[++Cnt]=(edge){y,x};
}
void addedge(int x,int y){
	e[++cnt]=(edge){y,head[x]};head[x]=cnt;
}
bool flag=0,mk[N];
int stk[N],top,Top;bool vis[N];
void dfs(int f,int x){
	vis[x]=1;
	eef(i,x)if(y!=f){
		stk[++top]=i;
		if(vis[y]){
			flag=1;
			Top=1;
			if(y!=1){
				while(e[stk[Top]].to!=y)Top++;
				Top++;
			}
			return;
		}
		dfs(x,y);
		if(flag)return;
		stk[top--]=0;
	}
}
int k,la,nw;
bool solve(int f,int x){
	++k;
	if(x<ans[k]||flag)ans[k]=x,flag=1;
	if(x>ans[k]&&!flag)return 0;
	eef(i,x)if(!mk[i]&&y!=f&&(x!=la||y!=nw)&&(y!=la||x!=nw)) if(!solve(x,y))return 0;
	return 1;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	ref(i,1,m)add(read(),read());
	sort(E+1,E+Cnt+1,cmp);
	ref(i,1,Cnt)addedge(E[i].to,E[i].next);
	ref(i,1,n)ans[i]=n;
	if(m==n-1)solve(0,1);else{
		dfs(0,1);
		la=e[stk[top]].to;
		ref(i,Top,top){
			nw=e[stk[i]].to;
			flag=0; k=0;
			solve(0,1);
			la=nw;
		}
	}
	ref(i,1,n)printf("%d%c",ans[i],i<n?' ':'\n');
}
