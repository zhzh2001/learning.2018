#include <bits/stdc++.h>
using namespace std;
#define ref(i,x,y)for(int i=x;i<=y;++i)
#define def(i,x,y)for(int i=x;i>=y;--i)
#define pb push_back
#define SZ(x) ((int)x.size())
#define test(x) (!x.empty())
int read(){
	char c=getchar();int d=0,f=1;
	for(;c<'0'||c>'9';c=getchar())if(c=='0')f=-1;
	for(;c>='0'&&c<='9';d=d*10+c-48,c=getchar());
	return d*f;
}
const int N=100010;
typedef long long LL;
const LL inf=1e18;
int n,m,tp,w[N];
vector<int> e[N];
void addedge(int x,int y){
	e[x].pb(y);e[y].pb(x);
}
int tmp,sz[N],Dep[N],F[N][17],p[N];
void dfs1(int f,int x){
	p[++p[0]]=x;
	sz[x]=1;
	ref(i,0,SZ(e[x])-1){
		int y=e[x][i];if(y==f)continue;
		if(Dep[y])continue;
		dfs1(x,y);sz[x]+=sz[y];
	}
}
void solve(int pre,int x,int d){
	tmp=d;
	p[0]=0;
	dfs1(0,x);
	int X=0,SZ=sz[x];
	ref(i,1,p[0])if(!X||sz[p[i]]>SZ/2&&sz[p[i]]<sz[X])X=p[i];
	Dep[X]=d;F[X][0]=pre;
	ref(i,0,SZ(e[X])-1){
		int y=e[X][i];
		if(Dep[y])continue;
		solve(X,y,d+1);
	}
}
int Lca(int x,int y){
	if(Dep[x]<Dep[y])swap(x,y);
	def(i,16,0)if(F[x][i]&&Dep[F[x][i]]>=Dep[y])x=F[x][i];
	if(x==y)return x;
	def(i,16,0)if(F[x][i]&&F[y][i]&&F[x][i]!=F[y][i])x=F[x][i],y=F[y][i];
	return F[x][0];
}
struct qint{int x,sx,y,sy,id;};
vector<qint> E[N];
LL dp2[N][2][2], res[N][2][2];
LL s1[N],s2[N]; int mktmp,mk[N];
LL f1[N][2],f2[N][2],F2[N][2];int fa[N];
void getdp0(LL&s,int x,int y){
	if(fa[y]==x)s=f1[y][0];
	else s=F2[x][0];
}
void getdp1(LL&s,int x,int y){
	if(fa[y]==x)s=f1[y][1];
	else s=F2[x][1];
}
void DFS(int f,int x){
	mk[x]=mktmp;
	p[0]=0;
	ref(i,0,SZ(e[x])-1){
		int y=e[x][i];
		if(y==f)continue;
		p[++p[0]]=y;
	}
	LL S1=0,S2=0;
	ref(i,1,p[0]){
		int y=p[i];
		LL d0,d1;getdp0(d0,x,y);getdp1(d1,x,y);
		S1+=(s1[i]=d1); S2+=(s2[i]=min(d0,d1));
	}
	LL x00=dp2[x][0][0];res[x][0][0]=x00+S1;
	LL x10=dp2[x][1][0];res[x][1][0]=x10+S2;
	LL x01=dp2[x][0][1];res[x][0][1]=x01+S1;
	LL x11=dp2[x][1][1];res[x][1][1]=x11+S2;
	ref(i,1,p[0]){
		int y=p[i];if(Dep[y]<tmp)continue;
		dp2[y][0][0]=x10+S2-s2[i];
		dp2[y][0][1]=x11+S2-s2[i];
		dp2[y][1][0]=min(dp2[y][0][0],x00+S1-s1[i])+w[y];
		dp2[y][1][1]=min(dp2[y][0][1],x01+S1-s1[i])+w[y];
	}
	ref(i,0,SZ(e[x])-1){
		int y=e[x][i];
		if(y==f)continue;
		if(Dep[y]<tmp)continue;
		DFS(x,y);
	}
}
LL ss1[N],ss2[N];
void dfsdp0(int f,int x){
	f1[x][1]=w[x];fa[x]=f;
	ref(i,0,SZ(e[x])-1){
		int y=e[x][i];if(y==f)continue;
		dfsdp0(x,y);
		f1[x][0]+=f1[y][1];f1[x][1]+=min(f1[y][0],f1[y][1]);
	}
}
void dfsdp1(int f,int x){
	p[0]=0;
	ref(i,0,SZ(e[x])-1){
		int y=e[x][i];if(y==f)continue;
		p[++p[0]]=y;
	}
	int S1=0,S2=0;
	ref(i,1,p[0]){
		int y=p[i];
		S1+=(s1[i]=f1[y][1]);S2+=(s2[i]=min(f1[y][0],f1[y][1]));
	}
	ref(i,1,p[0]){
		int y=p[i];
		F2[y][0]=f2[x][0]+S1-s1[i];
		F2[y][1]=f2[x][1]+S2-s2[i];
		f2[y][1]=min(F2[y][0],F2[y][1])+w[y];
		f2[y][0]=F2[y][1];
	}
	ref(i,0,SZ(e[x])-1){
		int y=e[x][i];if(y==f)continue;
		dfsdp1(x,y);
	}
}
LL Ans[N];
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();tp=read();
	ref(i,1,n)w[i]=read();
	ref(i,2,n)addedge(read(),read());
	solve(0,1,1);
	ref(j,1,16)ref(i,1,n)if(F[i][j-1]&&F[F[i][j-1]][j-1])
		F[i][j]=F[F[i][j-1]][j-1];
	ref(i,1,m){
		int x=read(),sx=read(),y=read(),sy=read();
		int L=Lca(x,y);E[L].pb((qint){x,sx,y,sy,i});
	}
	dfsdp0(0,1);
	f2[1][1]=w[1];
	dfsdp1(0,1);
	ref(i,1,n)if(test(E[i])){
		tmp=Dep[i];
		int S1=0,S2=0;
		ref(I,0,SZ(e[i])-1){
			int j=e[i][I];
			if(Dep[j]>Dep[i]){
				mktmp=j;
				dp2[j][0][0]=inf;dp2[j][1][0]=w[j];
				dp2[j][0][1]=0;dp2[j][1][1]=w[j];
				DFS(i,j);
			}
			LL d0,d1;getdp0(d0,i,j);getdp1(d1,i,j);
			S1+=(ss1[j]=d1);
			S2+=(ss2[j]=min(d0,d1));
		}
		ref(I,0,SZ(E[i])-1){
			qint q=E[i][I];
			int x=q.x,sx=q.sx,y=q.y,sy=q.sy,id=q.id;
			if(y==i)swap(x,y),swap(sx,sy);
			if(x==i){
				LL ans=res[y][sy][sx];
				if(sx==0){
					ans+=S1-ss1[mk[y]];
				}else{
					ans+=S2-ss2[mk[y]]+w[i];
				}
				Ans[id]=ans>=inf?-1:ans;
			}else{
				LL ans1=res[x][sx][1]+res[y][sy][1]+w[i]+S2-ss2[mk[x]]-ss2[mk[y]];
				LL ans2=res[x][sx][0]+res[y][sy][0]+S1-ss1[mk[x]]-ss1[mk[y]];
				LL ans=min(ans1,ans2);
				Ans[id]=ans>=inf?-1:ans;
			}
		}
	}
	ref(i,1,m)printf("%lld\n",Ans[i]);
}
