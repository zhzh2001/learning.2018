#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int INF=1000000000;
int num,Next[200010],vet[200010],head[100010],a[100010];
long long f[100005][2];
int flag[100005];
void add(int u,int v)
{
	Next[++num]=head[u];
	head[u]=num;
	vet[num]=v;
}
inline long long mins(long long a,long long b)
{
	if (a<b) return a;
	return b;
}
void dfs(int u,int fa)
{
	for (int i=head[u];i;i=Next[i])
	{
		int v=vet[i];
		if (v==fa) continue;
		dfs(v,u);
	}
	if (flag[u]==0) f[u][1]=INF;
	else
	{
		f[u][1]=a[u];
		for (int i=head[u];i;i=Next[i])
		{
			int v=vet[i];
			if (v==fa) continue;
			f[u][1]+=mins(f[v][1],f[v][0]);
		}
	}
	if (flag[u]==1)
	{
		f[u][0]=INF;
		return;
	}
	f[u][0]=0;
	for (int i=head[u];i;i=Next[i])
	{
		int v=vet[i];
		if (v==fa) continue;
		f[u][0]+=f[v][1];
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m;
	char ty[5];
	scanf("%d%d%s",&n,&m,ty);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<n;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);
		add(v,u);
	}
	memset(flag,-1,sizeof(flag));
	while (m--)
	{
		int a,x,b,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		flag[a]=x,flag[b]=y;
		dfs(1,0);
		long long ans=mins(f[1][1],f[1][0]);
		if (ans>=INF) puts("-1");
		else printf("%lld\n",ans);
		flag[a]=flag[b]=-1;
	}
	return 0;
}
