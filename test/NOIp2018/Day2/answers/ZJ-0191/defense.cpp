#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int INF=1000000000;
int num,Next[200010],vet[200010],head[100010],a[100010];
long long f[100005][2],pre[100010][2][2],suf[100010][2];
int flag[100005];
void add(int u,int v)
{
	Next[++num]=head[u];
	head[u]=num;
	vet[num]=v;
}
inline long long mins(long long a,long long b)
{
	if (a<b) return a;
	return b;
}
void dfs(int u,int fa)
{
	for (int i=head[u];i;i=Next[i])
	{
		int v=vet[i];
		if (v==fa) continue;
		dfs(v,u);
	}
	if (flag[u]==0) f[u][1]=INF;
	else
	{
		f[u][1]=a[u];
		for (int i=head[u];i;i=Next[i])
		{
			int v=vet[i];
			if (v==fa) continue;
			f[u][1]+=mins(f[v][1],f[v][0]);
			f[u][1]=mins(f[u][1],INF);
		}
	}
	if (flag[u]==1)
	{
		f[u][0]=INF;
		return;
	}
	f[u][0]=0;
	for (int i=head[u];i;i=Next[i])
	{
		int v=vet[i];
		if (v==fa) continue;
		f[u][0]+=f[v][1];
		f[u][0]=mins(f[u][0],INF);
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m;
	char ty[5];
	scanf("%d%d%s",&n,&m,ty);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<n;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);
		add(v,u);
	}
	if ((n>2000||m>2000)&&ty[0]=='A')
	{
		pre[1][0][0]=0,pre[1][1][1]=a[1];
		pre[1][0][1]=pre[1][1][0]=INF;
		for (int i=2;i<=n;i++)
		{
			pre[i][0][0]=pre[i-1][1][0],pre[i][0][1]=pre[i-1][1][1];
			pre[i][1][0]=mins(pre[i-1][1][0],pre[i-1][0][0])+a[i];
			pre[i][1][1]=mins(pre[i-1][1][1],pre[i-1][0][1])+a[i];
		}
		suf[n][0]=0,suf[n][1]=a[n];
		for (int i=n-1;i;i--)
		{
			suf[i][0]=suf[i+1][1];
			suf[i][1]=mins(suf[i+1][1],suf[i+1][0])+a[i];
		}
		while (m--)
		{
			int a,x,b,y;
			scanf("%d%d%d%d",&a,&x,&b,&y);
			if (ty[1]=='2')
			{
				if (x==0&&y==0)
				{
					puts("-1");
					continue;
				}
				if (a>b) swap(a,b),swap(x,y);

				else printf("%lld\n",mins(pre[a][x][0],pre[a][x][1])+suf[b][y]);
				continue;
			}				
			if (a>b) swap(a,b),swap(x,y);
			if (b==2&&x==0&&y==0) 
			{
				puts("-1");
				continue;
			}
			if (b==n)
			{
				printf("%lld\n",pre[b][y][x]);
				continue;
			}
			if (y==1) printf("%lld\n",pre[b][y][x]+mins(suf[b+1][0],suf[b+1][1]));
			else printf("%lld\n",pre[b][y][x]+suf[b+1][1]);
		}
		return 0;
	}
	memset(flag,-1,sizeof(flag));
	while (m--)
	{
		int a,x,b,y;
		scanf("%d%d%d%d",&a,&x,&b,&y);
		flag[a]=x,flag[b]=y;
		dfs(1,0);
		long long ans=mins(f[1][1],f[1][0]);
		if (ans>=INF) puts("-1");
		else printf("%lld\n",ans);
		flag[a]=flag[b]=-1;
	}
	return 0;
}
