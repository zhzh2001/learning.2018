#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int num,ti,cnt,huan[5005],head[5005],Next[10005],vet[10005],g[5005],ans[5005];
bool vis[5005],flag[5005];
int son[5005][5005];
struct node{
	int p,e;
}a[5005];
void add(int u,int v)
{
	Next[++num]=head[u];
	head[u]=num;
	vet[num]=v;
}
bool cmp(node x,node y)
{
	return (x.p<y.p);
}
void init(int u)
{
	vis[u]=1;
	int cc=0;
	for (int i=head[u];i;i=Next[i])
		a[++cc].p=vet[i],a[cc].e=i;
	sort(a+1,a+1+cc,cmp);
	son[u][0]=cc;
	for (int i=1;i<=cc;i++)
		son[u][i]=a[i].e;
	for (int i=head[u];i;i=Next[i])
	{
		int v=vet[i];
		if (!vis[v]) init(v);
	}
}
void dfs(int u,int fa)
{
	g[++ti]=u;
	for (int i=1;i<=son[u][0];i++)
	{
		int E=son[u][i],v=vet[E];
		if (flag[E>>1]||v==fa) continue;
		dfs(v,u);
	}
}
int dfss(int u,int pre)
{
	vis[u]=1;
	for (int i=head[u];i;i=Next[i])
	{
		int v=vet[i];
		if (v==pre) continue;
		if (vis[v])
		{
			huan[++cnt]=i>>1;
			return v;
		}
		int xx=dfss(v,u);
		if (xx&&v!=xx) 
		{
			huan[++cnt]=i>>1;
			return xx;
		}
	}
	return 0;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	num=1;
	for (int i=1;i<=m;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);
		add(v,u);
	}
	init(1);
	if (m==n-1)
	{
		ti=0;
		dfs(1,0);
		for (int i=1;i<n;i++) printf("%d ",g[i]);
		printf("%d\n",g[n]);
		return 0;
	}
	memset(vis,0,sizeof(vis));
	dfss(1,0);
	for (int i=1;i<=n;i++) ans[i]=n+1;
	for (int i=1;i<=cnt;i++)
	{
		ti=0;
		flag[huan[i]]=1;
		dfs(1,0);
		int Flag=0;
		for (int j=1;j<=n;j++)
		{
			if (g[j]!=ans[j]) 
			{
				if (g[j]<ans[j]) Flag=1;
				break;
			}
		}
		if (Flag)
			for (int j=1;j<=n;j++) ans[j]=g[j];
		flag[huan[i]]=0;
	}
	for (int i=1;i<n;i++) printf("%d ",ans[i]);
	printf("%d\n",ans[n]);
	return 0;
}
