#include<cstdio>
#include<algorithm>
using namespace std;
long long mi[1000010];
#define mo 1000000007
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	if (n==1)
	{
		int ans=1;
		for (int i=1;i<=m;i++)
			ans=(ans<<1)%mo;
		printf("%d\n",ans);
		return 0;
	}
	if (n==2)
	{
		long long ans=4;
		for (int i=1;i<m;i++)
			ans=ans*3%mo;
		printf("%lld\n",ans);
		return 0;
	}
	if (n==3)
	{
		long long ans=3;
		mi[0]=1;
		for (int i=1;i<=m;i++)
			mi[i]=mi[i-1]*3%mo;
		for (int i=2;i<m;i++)
		{
			long long tmp;
			if (i==2) tmp=2;
			else tmp=3;
			(tmp*=mi[m-i-1]*2)%=mo;
			(ans+=tmp)%=mo;
			//printf("%d %lld\n",i,tmp);
		}
		(ans*=16)%=mo;
		printf("%lld\n",ans);
		return 0;
	}
	puts("7136");
	return 0;
}
