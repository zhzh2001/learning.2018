#include<bits/stdc++.h>
using namespace std;
const int N=10050;
int tmp[N],ans[N],Head[N],tmpe[N],e[N],a[N][N],S,T,now,n,m,tot,nume;
bool flag[N*2],vis[N],FLAG;

struct Edge{
	int v,next;
}edge[N*2];
void addedge(int x,int y)
{
	edge[++tot]=(Edge){y,Head[x]};
	Head[x]=tot;
}
void dfs(int u,int fa)
{
	vis[u]=true;
	for (int i=Head[u];i;i=edge[i].next)
	{
		int v=edge[i].v;
		if (v==fa) continue;
		if (!vis[v]) dfs(v,u);
		else 
		{
			S=u;
			T=v;
			flag[i]=flag[i^1]=true;
			e[++nume]=i;
		}
	}
}
void find(int u,int fa,int k)
{
	if (u==T)
	{
		for (int i=1;i<k;i++) e[++nume]=tmpe[i];
		FLAG=true;
		return;
	}
	for (int i=Head[u];i;i=edge[i].next)
	{
		if (flag[i]) continue;
		int v=edge[i].v;
		if (v==fa) continue;
		tmpe[k]=i;
		find(v,u,k+1);
		if (FLAG) return;
	}
}
void update()
{
//	printf("now=%d\n",now);
//	for (int i=1;i<=now;i++) printf("%d ",tmp[i]); puts("");
	for (int i=1;i<=n;i++)
		if (ans[i]!=tmp[i])
		{
			if (ans[i]>tmp[i])
			{
				for (int j=1;j<=n;j++) ans[j]=tmp[j];
			}
			return;
		}
}
void DFS(int u,int fa)
{
	a[u][0]=0; tmp[++now]=u;
	for (int i=Head[u];i;i=edge[i].next)
	{
		if (flag[i]) continue;
		int v=edge[i].v;
		if (v==fa) continue;
		a[u][++a[u][0]]=v;
	}
//	printf("u=%d\n",u);
//	for (int i=1;i<=a[u][0];i++) printf("%d ",a[u][i]); puts("");
	sort(a[u]+1,a[u]+1+a[u][0]);
	for (int i=1;i<=a[u][0];i++)
		DFS(a[u][i],u);
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	tot=1;
	for (int i=1;i<=m;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		addedge(x,y); addedge(y,x);
	}
	ans[1]=n+1; now=0;
	dfs(1,0);
	if (m==n-1) 
	{
		now=0;
		DFS(1,0);
		update();
	}
	else
	{
		//assert(nume==2);
		nume--;
		find(S,0,1);
		DFS(1,0);
		update();
		for (int i=2;i<=nume;i++)
		{
			now=0;
			flag[e[i-1]]=flag[e[i-1]^1]=false;
			flag[e[i]]=flag[e[i]^1]=true;
			DFS(1,0);
			update();
		}
	}
	for (int i=1;i<=n;i++) printf("%d%c",ans[i]," \n"[i==n]);
	return 0;
}
