#include<bits/stdc++.h>
using namespace std;
const int N=200050;
typedef long long LL;
int n,m,p[N],Head[N],A,B,X,Y,tot;
char s[100];

struct Edge{
	int v,next;
}edge[N*2];
void addedge(int x,int y)
{
	edge[++tot]=(Edge){y,Head[x]};
	Head[x]=tot;
}
namespace task1{
	int f[2050][2];	
	void dfs(int u,int fa)
	{
		f[u][0]=0;
		f[u][1]=p[u];
		if (u==A)
		{
			f[u][X^1]=-1;
		}
		if (u==B)
		{
			f[u][Y^1]=-1;
		}
		for (int i=Head[u];i;i=edge[i].next)
		{
			int v=edge[i].v;
			if (v==fa) continue;
			dfs(v,u);
			if (f[u][0]!=-1)
			{
				if (f[v][1]==-1) f[u][0]=-1;
				else f[u][0]=f[u][0]+f[v][1];
			}
			if (f[u][1]!=-1)
			{
				if (f[v][0]==-1 && f[v][1]==-1) f[u][1]=-1;
				else if (f[v][0]==-1) f[u][1]=f[u][1]+f[v][1];
				else if (f[v][1]==-1) f[u][1]=f[u][1]+f[v][0];
				else f[u][1]=f[u][1]+min(f[v][0],f[v][1]);
			}
		}
	}
	void work()
	{
		dfs(1,0);
		if (f[1][0]==-1 && f[1][1]==-1) puts("-1");
		else if (f[1][0]==-1) printf("%d\n",f[1][1]);
		else if (f[1][1]==-1) printf("%d\n",f[1][0]);
		else printf("%d\n",min(f[1][0],f[1][1]));
	}
	void solve()
	{
		for (int i=1;i<=m;i++)
		{
			scanf("%d%d%d%d",&A,&X,&B,&Y);
			work();
		}
	}
};
namespace task2{
	LL f[20][N][4],ans[2];
	int Lg[N];
	LL query(int x,int y,int P,int Q)
	{
		int d=y-x+1,now=x;
		ans[0]=ans[1]=0LL;
		for (int i=Lg[n];i>=0;i--)
			if ((d>>i)&1)
			{
				ans[0]=f[i][now][P];
				ans[1]=f[i][now][P+2];
				d-=(1<<i);
				now+=(1<<i);
				break;
			}
		
		if (d)
		{
			for (int i=Lg[n];i>=0;i--)
				if ((d>>i)&1)
				{
					LL t0=ans[0],t1=ans[1];
					ans[0]=ans[1]=1LL<<60;
					for (int j=0;j<2;j++)
					{
						if (t0!=-1 && f[i][now][1+j*2]!=-1) ans[j]=min(ans[j],t0+f[i][now][1+j*2]);
						if (t1!=-1 && f[i][now][0+j*2]!=-1) ans[j]=min(ans[j],t1+f[i][now][0+j*2]);
						if (t1!=-1 && f[i][now][1+j*2]!=-1) ans[j]=min(ans[j],t1+f[i][now][1+j*2]);
					}
					now+=(1<<i);
				}
		}
		
		return ans[Q];
	}
	void solve()
	{
		Lg[0]=-1;
		memset(f,0x3f,sizeof(f));
		for (int i=1;i<=n;i++) Lg[i]=Lg[i>>1]+1;
		for (int i=1;i<=n;i++)
			f[0][i][0]=0,f[0][i][3]=p[i],f[0][i][2]=f[0][i][1]=-1;
		for (int i=1;i<n;i++)
			f[1][i][0]=-1,f[1][i][1]=p[i],f[1][i][2]=p[i+1],f[1][i][3]=p[i]+p[i+1];
		for (int i=2;i<=Lg[n];i++)
			for (int j=1;j<=n;j++)
			{
				if (j+(1<<i)-1>n) break;
				for (int k1=0;k1<2;k1++)
					for (int k4=0;k4<2;k4++)
					{
						int t=k1+k4*2;
						for (int k2=0;k2<2;k2++)
							for (int k3=k2^1;k3<2;k3++)
							{
								int t1=k1+k2*2,t2=k3+k4*2;
								if (f[i-1][j][t1]!=-1 && f[i-1][j+(1<<(i-1))][t2]!=-1)
									f[i][j][t]=min(f[i][j][t],f[i-1][j][t1]+f[i-1][j+(1<<(i-1))][t2]);
							}
					}
			}
		for (int cas=1;cas<=m;cas++)
		{
			scanf("%d%d%d%d",&A,&X,&B,&Y);
			if (abs(A-B)==1 && X==0 && Y==0) {puts("-1"); continue;}
			LL ANS=query(A,B,X,Y);
			//printf("ANS=%lld\n",ANS);
			if (A>1)
			{
				LL tmp=1LL<<60;
				for (int t1=0;t1<2;t1++)
					for (int t2=0;t2<2;t2++)
					{
						if (t2+X==0 || (A==2 && t1!=t2) || (A==3 && t1==0 && t2==0)) continue;
						tmp=min(tmp,query(1,A-1,t1,t2));
					}
				ANS+=tmp;
			}
			//printf("ANS=%lld\n",ANS);
			if (B<n)
			{
				LL tmp=1LL<<60;
				for (int t1=0;t1<2;t1++)
					for (int t2=0;t2<2;t2++)
					{
						if (t1+Y==0 || (B==n-1 && t1!=t2) || (B==n-2 && t1==0 && t2==0)) continue;
						//printf("t1=%d t2=%d -----------------tmp=%lld\n",t1,t2,query(B+1,n,t1,t2));
						tmp=min(tmp,query(B+1,n,t1,t2));
					}
				//printf("----------------tmp=%lld\n",tmp);
				ANS+=tmp;
			}
			printf("%lld\n",ANS);
		}
	}

};

int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		addedge(x,y); addedge(y,x);
	}
	if (n<=2000 && m<=2000) task1::solve();
	else if (s[0]=='A') task2::solve();


	return 0;
}
