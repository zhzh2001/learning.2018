#include<bits/stdc++.h>
using namespace std;
typedef long long LL;
const int MOD=1e9+7;
int a[5][5],n,m;

int ksm(int a,int b)
{
	int ret=1;
	for (;b;b>>=1,a=(LL)a*a%MOD)
		if (b&1) ret=(LL)ret*a%MOD;
	return ret;
}


int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=3;i++) a[1][i]=a[i][1]=ksm(2,i);
	a[2][2]=12; a[3][3]=112;
	a[2][3]=a[3][2]=36;
	if (n<=3 && m<=3) printf("%d\n",a[n][m]);
	else
	{
		if (n==2)
		{
			printf("%lld\n",4LL*ksm(3,m-1)%MOD);
			return 0;
		}



	}
	return 0;
}
