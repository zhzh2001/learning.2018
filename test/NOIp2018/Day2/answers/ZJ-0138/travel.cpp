#include<bits/stdc++.h>
#define N 5005
using namespace std;
void read(int&n){
	n=0;int k=1;char ch=getchar();
	while(!('0'<=ch&&ch<='9')){if(ch=='-')k=-1;ch=getchar();}
	while('0'<=ch&&ch<='9')n=n*10+ch-48,ch=getchar();
	n*=k;
}
int nxt[N<<1],head[N],to[N<<1],tot,w,p[N<<1],f[N],dp[N];
void add(int x,int y){
	nxt[++tot]=head[x],to[tot]=y,head[x]=tot;
	nxt[++tot]=head[y],to[tot]=x,head[y]=tot;
}
bool b[N];
int n,m,x,y,ans[N],q[N],l,xx,yy,X,Y,c[N],fa[N];
void dfs(int k,int x){
	int t=w;
	q[++l]=k;//if(q[l]>ans[l])return;
	//printf("%d ",k);
	for(int i=head[k];i;i=nxt[i])
		if(to[i]!=x&&!b[i>>1])p[++w]=-to[i];
	sort(p+1+t,p+1+w);
	for(int i=w;i>t;i--)dfs(-p[i],k);
}
void Dfs(int k,int x){
	for(int i=head[k];i;i=nxt[i])
		if(to[i]!=x)
			f[to[i]]=k,dp[to[i]]=dp[k]+1,
			fa[to[i]]=i>>1,Dfs(to[i],k);
}
int find(int k){
	return f[k]=f[k]==k?k:find(f[k]);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	read(n),read(m),tot=1;
	if(m==n-1){
		for(int i=1;i<=m;i++)
			read(x),read(y),add(x,y);
		l=0;dfs(1,0);
		for(int i=1;i<=n;i++)printf("%d ",q[i]);
		return 0;
	}
	for(int i=1;i<=n;i++)f[i]=i;
	for(int i=1;i<=m;i++){
		read(x),read(y);
		xx=find(x),yy=find(y);
		if(xx!=yy)add(x,y),f[xx]=yy;
		else {X=x,Y=y;continue;}
	}
	f[1]=0,Dfs(1,0);
	x=X,y=Y;
	while(dp[x]!=dp[y]){
		if(dp[x]<dp[y])swap(x,y);
		c[++l]=fa[x],x=f[x];
	}
	while(x!=y){
		c[++l]=fa[x],c[++l]=fa[y];
		x=f[x],y=f[y];
	}
	add(X,Y);c[++l]=tot>>1,tot=l;
	for(int i=1;i<=n;i++)ans[i]=1000000000;
	for(int i=1;i<=tot;i++){
		l=0;w=0;b[c[i]]=1;dfs(1,0);
		for(int j=1;j<=n;j++)
			if(q[j]<ans[j]){
				for(int k=1;k<=n;k++)ans[k]=q[k];
				break;
			}else if(q[j]>ans[j])break;
		b[c[i]]=0;
	}
	for(int i=1;i<=n;i++)printf("%d ",ans[i]);
	return 0;
}
