#include<bits/stdc++.h>
#define N 100005
using namespace std;
int nxt[N<<1],head[N],to[N<<1],tot;
void add(int x,int y){
	nxt[++tot]=head[x],to[tot]=y,head[x]=tot;
	nxt[++tot]=head[y],to[tot]=x,head[y]=tot;
}
int dp[N][2],x,y,xx,yy,n,m,a[N],X,Y; char s[23];
void dfs(int k,int x){
	for(int i=head[k];i;i=nxt[i])
	if(to[i]!=x)dfs(to[i],k);
	dp[k][0]=0,dp[k][1]=a[k];
	for(int i=head[k];i;i=nxt[i])
		if(to[i]!=x){
			dp[k][1]+=dp[to[i]][0];
			dp[k][0]+=dp[to[i]][1];
		}
	if(k==X)dp[k][xx^1]=500000000;
	if(k==Y)dp[k][yy^1]=500000000;
	dp[k][0]=min(dp[k][0],dp[k][1]);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	for(int i=1;i<n;i++)scanf("%d%d",&x,&y),add(x,y);
	while(m--){
		scanf("%d%d%d%d",&X,&xx,&Y,&yy);
		dfs(1,0);
		if(dp[1][0]>=500000000)puts("-1");else printf("%d\n",dp[1][0]);
	}
	return 0;
}
