#include <bits/stdc++.h>
#include <iostream>
#include <cmath>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
using namespace std;
const int mod=1000000007;
int n,m;
bool a[10][1000005];
long long ans=1,k;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	if(n==1){
		k=2;
		while(m){
			if(m&1)ans=(ans*k)%mod;
			m>>=1;
			k=(k*k)%mod;
		}
		cout<<ans<<endl;
		return 0;
	}
	if(m==1){
		k=2;
		while(n){
			if(n&1)ans=(ans*k)%mod;
			n>>=1;
			k=(k*k)%mod;
		}
		cout<<ans<<endl;
		return 0;
	}
	if(n==2){
		ans=4;k=3;
		m--;
		while(m){
			if(m&1)ans=(ans*k)%mod;
			m>>=1;
			k=(k*k)%mod;
		}
		cout<<ans<<endl;
		return 0;
	}
	if(m==2){
		ans=4;k=3;
		n--;
		while(n){
			if(n&1)ans=(ans*k)%mod;
			n>>=1;
			k=(k*k)%mod;
		}
		cout<<ans<<endl;
		return 0;
	}
	if(n==3&&m==3){
		cout<<112<<endl;
		return 0;
	}
	if(n==5&&m==5){
		cout<<7136<<endl;
		return 0;
	}
	return 0;
}
