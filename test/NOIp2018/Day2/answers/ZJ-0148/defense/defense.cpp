#include <bits/stdc++.h>
#include <iostream>
#include <cmath>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
using namespace std;
const long long inf=99999999999;
int n,m,u,v,t,a,b,x,y,tot=0,pre[100005],used[100005],w[100005],to[100005],head[100005],nxt[100005];
string type;
void add(int l,int r){
	tot++;
	to[tot]=r;
	nxt[tot]=head[l];
	head[l]=tot;
}
void build(int x){
	for(int i=head[x];i;i=nxt[i]){
		if(to[i]==pre[x])continue;
		pre[to[i]]=x;
		build(to[i]);
	}
}
long long dfs(int p,int q,long long d){
	if(q)d+=w[p];
	used[p]=1;
	for(int i=head[p];i;i=nxt[i]){
		if(used[to[i]])continue;
		if(to[i]==b){
			if(y==0&&q==0){
				d=inf;
				break;
			}
			if(y==1){
				d+=dfs(b,1,0);
				continue;
			}
			if(y==0){
				d+=dfs(b,0,0);
				continue;
			}
		}
		if(!q)d+=dfs(to[i],1,0);
		else d+=min(dfs(to[i],1,0),dfs(to[i],0,0));
	}
	return d;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m>>type;
	for(int i=1;i<=n;i++)cin>>w[i];
	for(int i=1;i<n;i++){
		cin>>u>>v;
		add(u,v);
		add(v,u);
	}
	build(1);
	while(m--){
		t=0;
		for(int i=1;i<=n;i++)used[i]=0;
		cin>>a>>x>>b>>y;
		if(x==0&&y==0){
			if(pre[a]==b||pre[b]==a){
				cout<<-1<<endl;
				t=-1;
			}
		}
		if(t==-1)continue;
		cout<<dfs(a,x,0)<<endl;
	}
	return 0;
}
