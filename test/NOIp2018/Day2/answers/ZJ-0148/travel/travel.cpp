#include <bits/stdc++.h>
#include <iostream>
#include <cmath>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
using namespace std;
int n,m,u,v,tot=0,p=0,c=1,to[5005],head[5005],nxt[5005],a[5005],b[5005],y[5005],used[5005];
void add(int x,int y){
	tot++;
	to[tot]=y;
	nxt[tot]=head[x];
	head[x]=tot;
}
bool cmp(int x,int y){return x<y;}
void dfs(int x,int q){
	if(!used[x]){a[++p]=x;used[x]=1;}
	if(p==n)return;
	for(int i=head[x];i;i=nxt[i]){
		if(used[to[i]])continue;
		b[++q]=to[i];
	}
	sort(b+1,b+1+q,cmp);
	for(int i=1;i<=q;i++)dfs(b[i],0);
}
void up(int x){
	if(x==1)return;
	if(y[x]<y[x>>1]){swap(y[x],y[x>>1]);up(x>>1);}
}
void insert(int x){
	used[x]=1;
	y[++c]=x;
	up(c);
}
void down(int x){
	int d=x<<1;
	if(d>c)return;
	if(d+1<=c&&y[d]>y[d+1])d++;
	if(y[x]>y[d]){swap(y[x],y[d]);down(d);}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<=m;i++){
		cin>>u>>v;
		add(u,v);
		add(v,u);
	}
	if(m==n-1)dfs(1,0);
	else{
		y[1]=used[1]=1;
		while(c){
			int d=y[1];
			a[++p]=d;
			swap(y[1],y[c]);
			c--;
			down(1);
			for(int i=head[d];i;i=nxt[i]){
				if(!used[to[i]])insert(to[i]);
			}
		}
	}
	for(int i=1;i<n;i++)cout<<a[i]<<" ";
	cout<<a[n]<<endl;
	return 0;
}
