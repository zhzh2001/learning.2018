#include<bits/stdc++.h>
using namespace std;

const int N=1e5+10;
int n,m,x,y;
struct link
{
	int top,fi[N],to[N],la[N],ne[N];
	void add(int x,int y)
	{
		top++,to[top]=y;
		if(fi[x]==0)fi[x]=top;else ne[la[x]]=top;
		la[x]=top;
	}
	void adde(int x,int y)
	{
		add(x,y);
		add(y,x);
	}
}L;
void dfs(int now,int f)
{
	vector<int> son;son.clear();
	for(int i=L.fi[now];i;i=L.ne[i])
		if(L.to[i]!=f)son.push_back(L.to[i]);
	sort(son.begin(),son.end());
	int len=son.size();
	printf("%d ",now);
	for(int i=0;i<len;i++)dfs(son[i],now);
}
int vis[N],cir[N],fd=0,other,ed=0;
void DFS(int now,int f)
{
	vis[now]=1;
	for(int i=L.fi[now];i;i=L.ne[i])
	if(L.to[i]!=f)
	{
		if(!vis[L.to[i]])DFS(L.to[i],now);
		else fd=1,other=L.to[i];
		if(fd)
		{
			cir[now]=1;
			if(now==other)
			{	
				fd=0,ed=1;
			}
			return;
		}
		if(ed)return;
	}
}
int C[N],top,be;
void fd_cir(int now,int f)
{
	C[++top]=now;
	for(int i=L.fi[now];i;i=L.ne[i])
	if(cir[L.to[i]]&&(i^1)!=f)
	{
		if(L.to[i]!=be)fd_cir(L.to[i],i);
		return;
	}
}
void out(int now,int f)
{
	printf("%d ",now);
	vector<int> son;son.clear();
	for(int i=L.fi[now];i;i=L.ne[i])
	if(!cir[L.to[i]]&&L.to[i]!=f)
		out(L.to[i],now);
}
void dfs2(int now,int f)
{	
	printf("%d ",now);
	if(cir[now])
	{
		be=now;
		fd_cir(now,0);
		int L=2,R=top;
		if(C[L]<C[R])
		{
			while(C[L]<C[R])out(C[L],0),L++;
			for(int i=R;i>=L;i--)out(C[i],0);
		}else
		{
			while(C[L]>C[R])out(C[R],0),R--;
			for(int i=L;i<=R;i++)out(C[i],0);
		}
		return;
	}
	vector<int> son;son.clear();
	for(int i=L.fi[now];i;i=L.ne[i])
	if(L.to[i]!=f)
		son.push_back(L.to[i]);
	int len=son.size();
	for(int i=0;i<len;i++)dfs2(son[i],now);
}
int main()
{
	L.top=1;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)scanf("%d%d",&x,&y),L.adde(x,y);
	if(m==n-1)
	{
		dfs(1,0);
		return 0;
	}
	DFS(1,0);
	//for(int i=1;i<=n;i++)cout<<cir[i]<<' ';
	if(cir[1])
	{
		be=1;
		//fd_cir(1,0);
		//for(int i=1;i<=top;i++)cout<<C[i]<<' ';
		//int L=2,R=top;
	
	}else
	{
		dfs2(1,0);
	}
	return 0;
}
