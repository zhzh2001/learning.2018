#include<bits/stdc++.h>
using namespace std;

const int N=1e4+10;
int n,m,x[N],y[N],vis[N],cir[N];
struct link
{
	int top,fi[N],to[N],la[N],ne[N];
	void add(int x,int y)
	{
		top++,to[top]=y;
		if(fi[x]==0)fi[x]=top;else ne[la[x]]=top;
		la[x]=top;
	}
	void adde(int x,int y)
	{
		add(x,y);
		add(y,x);
	}
	void clear()
	{
		top=0;
		memset(fi,0,sizeof fi);
		memset(ne,0,sizeof ne);
		memset(to,0,sizeof to);
		memset(la,0,sizeof la);
	}
}L;
int fd,other,ed;
void fd_cir(int now,int f)
{
	vis[now]=1;
	for(int i=L.fi[now];i;i=L.ne[i])
	if(L.to[i]!=f)
	{
		if(!vis[L.to[i]])fd_cir(L.to[i],now);
		else fd=1,other=L.to[i];
		if(fd)
		{
			cir[now]=1;
			if(now==other)
			{	
				fd=0,ed=1;
			}
			return;
		}
		if(ed)return;
	}
}
void dfs(int now,int f)
{
	vector<int> son;son.clear();
	for(int i=L.fi[now];i;i=L.ne[i])
		if(L.to[i]!=f)son.push_back(L.to[i]);
	sort(son.begin(),son.end());
	int len=son.size();
	printf("%d ",now);
	for(int i=0;i<len;i++)dfs(son[i],now);
}
int de,ans[5010][5010],top[5010],can[5010];
void DFS(int now,int f)
{
	vector<int> son;son.clear();
	for(int i=L.fi[now];i;i=L.ne[i])
		if(L.to[i]!=f)son.push_back(L.to[i]);
	sort(son.begin(),son.end());
	int len=son.size();
	//printf("%d ",now);
	ans[de][++top[de]]=now;
	for(int i=0;i<len;i++)
		DFS(son[i],now);
}
int lalala[N],TOPC,C[N];
void get_cir(int now,int f)
{
	C[++TOPC]=now;
	lalala[now]=1;
	for(int i=L.fi[now];i;i=L.ne[i])
	if(L.to[i]!=f&&!lalala[L.to[i]])
	{
		get_cir(L.to[i],now);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);

	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)scanf("%d%d",&x[i],&y[i]),L.adde(x[i],y[i]);
	if(m==n-1)
	{
		dfs(1,0);
		return 0;
	}
	fd_cir(1,0);//for(int i=1;i<=n;i++)cout<<cir[i]<<' ';
	int all=1;
	for(int i=1;i<=n;i++)all&=cir[i];
	if(all&&n>1000)
	{
		/*for(int i=1;i<=n;i++)
			printf("%d ",C[i]);*/
		get_cir(1,0);
		printf("%d ",1);
		int L=2,R=n;
		if(C[L]<C[R])
		{
			while(C[L]<C[R])printf("%d ",C[L]),L++;
			for(int i=R;i>=L;i--)printf("%d ",C[i]);
		}else
		{
			while(C[L]>C[R])printf("%d ",C[R]),R--;
			for(int i=L;i<=R;i++)printf("%d ",C[i]);
		}
		return 0;
	}
	for(de=1;de<=n;de++)
	if(cir[x[de]]&&cir[y[de]])
	{
		L.clear();
		for(int i=1;i<=n;i++)
			if(i!=de)L.adde(x[i],y[i]);
		DFS(1,0);
		can[de]=1;
	}
	for(int i=1;i<=n;i++)
	{
		int mn=n+1;
		for(int j=1;j<=n;j++)
		if(can[j])mn=min(mn,ans[j][i]);
		for(int j=1;j<=n;j++)if(ans[j][i]!=mn)can[j]=0;
	}
	for(int i=1;i<=n;i++)
	if(can[i])
	{
		for(int j=1;j<=n;j++)printf("%d",ans[i][j]),putchar(j!=n?' ':'\n');
		return 0;
	}
	return 0;
}
