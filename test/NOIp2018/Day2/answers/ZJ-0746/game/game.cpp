#include<bits/stdc++.h>
using namespace std;

const int ha=1e9+7;
int qpow(int a,int b)
{
	int re=1;
	while(b)
	{
		if(b&1)re=1ll*re*a%ha;
		a=1ll*a*a%ha;b>>=1;
	}
	return re;
}
int n,m,M;
int calc(int x,int y)
{
	if(x==1)return 2;
	if(x==2)return y==1?4:12;
	if(x==3)
	{
		if(y==1)return 8;
		if(y==2)return 36;
		if(y==3)return 112;
	}
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);

	scanf("%d%d",&n,&m);if(n>m)swap(n,m);
	M=m,m=min(m,n);
	int ans=calc(n,m);
	ans=1ll*ans*qpow(3,M-m)%ha;
	printf("%d",ans);
	return 0;
}
