#include<bits/stdc++.h>
using namespace std;

const int N=2e5+10;
int n,m,w[N],x,y,a,b;
struct link
{
	int top,fi[N],ne[N],la[N],to[N];
	void add(int x,int y)
	{
		top++,to[top]=y;
		if(fi[x]==0)fi[x]=top;else ne[la[x]]=top;
		la[x]=top;
	}
	void adde(int x,int y)
	{
		add(x,y);
		add(y,x);
	}
}L;
char s[100];
const long long INF=1e15;
long long dp[N][2];
//0 have
//1 nohave
void dfs(int now,int f)
{
	long long sum0=0,sum1=0;
	for(int i=L.fi[now];i;i=L.ne[i])
	if(L.to[i]!=f)
	{
		dfs(L.to[i],now);
		sum0+=min(dp[L.to[i]][0],dp[L.to[i]][1]);
		sum1+=dp[L.to[i]][0];
	}
	dp[now][0]=sum0+w[now];
	dp[now][1]=sum1;
	if(now==a&&x==0)dp[now][0]=INF;
	if(now==a&&x==1)dp[now][1]=INF;
	if(now==b&&y==0)dp[now][0]=INF;
	if(now==b&&y==1)dp[now][1]=INF;
	//cout<<now<<' '<<dp[now][0]<<' '<<dp[now][1]<<' '<<dp[now][2]<<endl;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);

	scanf("%d%d",&n,&m);
	scanf("%s",s);
	for(int i=1;i<=n;i++)scanf("%d",&w[i]);
	for(int i=1;i<n;i++)scanf("%d%d",&x,&y),L.adde(x,y);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&a,&x,&b,&y);
		dfs(1,0);
		long long ans=min(dp[1][0],dp[1][1]);
		if(ans<INF)printf("%lld\n",ans);else puts("-1");
	}
	return 0;
}
