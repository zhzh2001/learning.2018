#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=2005;
int i,j,k,n,m,q1,q2,q3,q4,u,v;
int f[N][4],a[N],b[N];
char s[10];
int main()
{
	scanf("%d%d%s",&n,&m,s);
	for (i=1;i<=n;++i)
		scanf("%d",&a[i]);
	for (i=1;i<n;++i)
		scanf("%d%d",&u,&v);
	f[1][2]=f[1][3]=1<<30;
	f[1][1]=a[1];
	for (i=2;i<=n;++i)
		{
			f[i][0]=f[i-1][2];
			f[i][1]=min(f[i-1][0],f[i-1][2])+a[i];
			f[i][2]=min(f[i-1][1],f[i-1][3]);
			f[i][3]=min(f[i-1][1],f[i-1][3])+a[i];
			printf("%d %d %d %d\n",f[i][0],f[i][1],f[i][2],f[i][3],f[i][4]);
		}
	printf("%d\n",min(min(f[n][2],f[n][3]),min(f[n][0],f[n][1])));
	for (i=1;i<=m;++i)
	{
		scanf("%d%d%d%d",&q1,&q2,&q3,&q4);
		memset(b,0xff,sizeof(b));
		memset(f,0xff,sizeof(f));
		if ((q1-q2)>=-1&&(q1-q2)<=1)
		{
			printf("-1");
			continue;
		}
		b[q1]=q2,b[q3]=q4;
		for (i=1;i<=n;++i)
			if (b[i]==-1)
			{
				f[i][0]=f[i-1][1];
				f[i][1]=min(f[i-1][0],f[i-1][1])+a[i];
			}
			else
			if (b[i]==1)
				f[i][0]=1<<30;
			else
				f[i][1]=1<<30;
		printf("%d\n",min(f[n][0],f[n][1]));
	}
	return 0;
}
