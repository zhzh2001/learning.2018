#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
int n,m,x,y,p1,p2,ans;
char s[10];
int syx[100005];
int p[100005],e1[100005],e2[100005],ok[100005];
void dfs(int k,int cost)
{
	if(cost>=ans) return;
	if(k>n) 
	{
		for(int i=1;i<n;i++) 
		if(ok[e1[i]]==0&&ok[e2[i]]==0) return;
		ans=cost;
		return;
	}
	if(syx[k]!=-1) 
	{
	    ok[k]=1;
	    dfs(k+1,cost+p[k]);
	    ok[k]=0;
	}
	if(syx[k]!=1) dfs(k+1,cost);
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	cin>>n>>m;
	scanf("%s",s+1);
	for(int i=1;i<=n;i++) scanf("%d",&p[i]);
	for(int i=1;i<n;i++) scanf("%d%d",&e1[i],&e2[i]);
	for(int i=1;i<=m;i++) 
	{
		scanf("%d%d%d%d",&x,&p1,&y,&p2);
		if(p1==1) syx[x]=1; else syx[x]=-1;
		if(p2==1) syx[y]=1; else syx[y]=-1;
		ans=1e9;
		dfs(1,0);
		syx[x]=0;
		syx[y]=0;
		if(ans==1e9) printf("-1\n"); else printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
