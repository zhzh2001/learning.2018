#include<bits/stdc++.h>
const int N=1111111,mo=1000000007;
int n,m,i,j,k;
inline int poww(int x,int y){
	int ans=1;
	for(;y;y>>=1,x=1ll*x*x%mo)if(y&1)ans=1ll*ans*x%mo;
	return ans;
}
int main(){
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n>m)std::swap(n,m);
	if(n==1){
		printf("%d\n",poww(2,m));
		return 0;
	}
	if(n==2){
		int ans=4ll*poww(3,m-1)%mo;
		printf("%d\n",ans);
		return 0;
	}
	if(n==3 && m==3){
		puts("112");
		return 0;
	}
	return 0;
}
