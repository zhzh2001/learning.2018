#include<bits/stdc++.h>
using namespace std;
const int N=50005;
int n,m,u,v,i,j;
bool vi[N];
struct edge{int to,next;}e[N<<1];
int h[N],xb=1;
bool b[N];
int ce[N],ccnt;
int d[N];
int ans[N],tmd[N],cn;
inline void getc(){
	static int q[N];int t=0,w=0,i,u;
	for(i=1;i<=n;++i)if(d[i]==1)q[++w]=i;
	for(;t<w;){
		u=q[++t];
		for(i=h[u];i;i=e[i].next)if(1==--d[e[i].to])q[++w]=e[i].to;
	}
	for(u=1;u<=n;++u)if(d[u]>1)
		for(i=h[u];i;i=e[i].next)if(d[e[i].to]>1)ce[++ccnt]=i>>1;
	sort(ce+1,ce+ccnt+1);ccnt=unique(ce+1,ce+ccnt+1)-ce-1;
}
vector<int>g[N];
void dfs(int x,int fa){
	tmd[++cn]=x;
	int z=-1;
	for(int i=h[x];i;i=e[i].next)if(b[i>>1])z=e[i].to;
	for(int i=0;i<g[x].size();++i)if(g[x][i]!=z && g[x][i]!=fa)dfs(g[x][i],x);
}
int main(){
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(i=1;i<=m;++i){
		scanf("%d%d",&u,&v);
		e[++xb]=(edge){v,h[u]};h[u]=xb;
		e[++xb]=(edge){u,h[v]};h[v]=xb;
		++d[u];++d[v];
		g[u].push_back(v);g[v].push_back(u);
	}
	for(i=1;i<=n;++i)sort(g[i].begin(),g[i].end());
	getc();
	if(m==n){
		memset(ans,10,sizeof ans);
		for(i=1;i<=ccnt;++i){
			b[ce[i]]=1;cn=0;
			dfs(1,0);
			for(j=1;tmd[j]==ans[j];++j);
			if(ans[j]>tmd[j])memcpy(ans+1,tmd+1,n<<2);
			b[ce[i]]=0;
		}
	}else{
		cn=0;
		dfs(1,0);
		memcpy(ans+1,tmd+1,n<<2);		
	}
	for(i=1;i<=n;++i)printf("%d ",ans[i]);putchar('\n');
	return 0;
}
