#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll Min(const ll&a,const ll&b){return a<b?a:b;}
const ll inf=1ll<<55;
const int N=100005;
int n,m,i;
string S;
int p[N],u,v,a,x,b,y;
vector<int>e[N];
namespace T1{
	ll f[N],g[N];
	void dfs(int u,int fa){
		f[u]=p[u];g[u]=0;
		for(int i=0;i<e[u].size();++i)if(e[u][i]!=fa){
			dfs(e[u][i],u);
			f[u]+=Min(f[e[u][i]],g[e[u][i]]);
			g[u]+=f[e[u][i]];
		}
		if(u==a){
			if(x==0)f[u]=inf;else g[u]=inf;
		}
		if(u==b){
			if(y==0)f[u]=inf;else g[u]=inf;
		}
	}
	inline void work(){
		while(m--){
			cin>>a>>x>>b>>y;
			dfs(1,0);
			ll ans=Min(f[1],g[1]);
			if(ans>=inf)ans=-1;
			cout<<ans<<'\n';
		}
	}
}
namespace T2{
	inline void work(){
	}
}
int main(){
//	freopen("defense1.in","r",stdin);
	ios::sync_with_stdio(0);cin.tie(0);
	cin>>n>>m>>S;
	for(i=1;i<=n;++i)cin>>p[i];
	for(i=1;i<n;++i){
		cin>>u>>v;
		e[u].push_back(v);
		e[v].push_back(u);
	}
	if(n<=3000 && m<=3000)T1::work();
		else T2::work();
	return 0;
}
