#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll Min(const ll&a,const ll&b){return a<b?a:b;}
const ll inf=1ll<<55;
const int N=100005;
int n,m,i;
string S;
int p[N],u,v,a,x,b,y;
vector<int>e[N];
namespace T1{
	ll f[N],g[N];
	void dfs(int u,int fa){
		f[u]=p[u];g[u]=0;
		for(int i=0;i<e[u].size();++i)if(e[u][i]!=fa){
			dfs(e[u][i],u);
			f[u]+=Min(f[e[u][i]],g[e[u][i]]);
			g[u]+=f[e[u][i]];
		}
		if(u==a){
			if(x==0)f[u]=inf;else g[u]=inf;
		}
		if(u==b){
			if(y==0)f[u]=inf;else g[u]=inf;
		}
	}
	inline void work(){
		while(m--){
			cin>>a>>x>>b>>y;
			dfs(1,0);
			ll ans=Min(f[1],g[1]);
			if(ans>=inf)ans=-1;
			cout<<ans<<'\n';
		}
	}
}
namespace T2{
	ll f[N],g[N];
	ll ff[N],gg[N];
	void dfs1(int x,int fa){
		for(int i=0;i<e[x].size();++i)if(e[x][i]!=fa){
			dfs1(e[x][i],x);
			f[x]+=min(f[e[x][i]]+p[e[x][i]],g[e[x][i]]);
			g[x]+=f[e[x][i]]+p[e[x][i]];
		}
	}
	void dfs2(int x,int fa){
		static int f1[N],f2[N],g1[N],g2[N];
		int i,d=e[x].size();
		f1[0]=f2[d+1]=g1[0]=g2[d+1]=0;
		for(i=0;i<d;++i){
			f1[i+1]=f2[i+1]=min(f[e[x][i]]+p[e[x][i]],g[e[x][i]]);
			g1[i+1]=g2[i+1]=f[e[x][i]]+p[e[x][i]];
		}
		for(i=1;i<=d;++i)f1[i]+=f1[i-1],g1[i]+=g1[i-1];
		for(i=d;i;--i)f2[i]+=f2[i+1],g2[i]==g2[i+1];
		for(i=0;i<d;++i){
			ff[e[x][i]]=min(f1[i]+f2[i+2]+ff[x]+p[x],g1[i]+g2[i+2]+gg[x]);
			gg[e[x][i]]=f1[i]+f2[i+2]+ff[x]+p[x];
		}
	}
	inline void work(){
		g[1]=gg[1]=inf;
		dfs1(1,0);
		dfs2(1,0);
		while(m--){
			cin>>a>>x>>b>>y;
			if(y==0)cout<<gg[b]+g[b]<<'\n';
				else cout<<ff[b]+f[b]+p[b]<<'\n';
		}
	}
}
int main(){
//	freopen("defense1.in","r",stdin);
	ios::sync_with_stdio(0);cin.tie(0);
	cin>>n>>m>>S;
	for(i=1;i<=n;++i)cin>>p[i];
	for(i=1;i<n;++i){
		cin>>u>>v;
		e[u].push_back(v);
		e[v].push_back(u);
	}
	T1::work();
	return 0;
}
