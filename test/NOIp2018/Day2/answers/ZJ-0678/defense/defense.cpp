#include<bits/stdc++.h>
#define N 100010
#define ll long long
using namespace std;
const ll inf=1e18;
int p[N];
vector<int>q[N];
int f1,g1,f2,g2;
ll dp[N][2],tmp[N][2],dp2[N][2];
void DFS(int x,int fa){
	ll sum1=0,sum2=0;
	for(int i=0;i<(int)q[x].size();i++){
		int to=q[x][i];
		if(to==fa)continue;
		DFS(to,x);
		sum1+=dp[to][1],sum2+=min(dp[to][0],dp[to][1]);
	}
	dp[x][0]=sum1,dp[x][1]=sum2+p[x];
	if(x==f1){
		if(g1)dp[x][0]=inf;
		else dp[x][1]=inf;
	}
	if(x==f2){
		if(g2)dp[x][0]=inf;
		else dp[x][1]=inf;
	}
}

int fa[N];
void DFS2(int x){
	ll sum1=0,sum2=0;
	for(int i=0;i<(int)q[x].size();i++){
		int to=q[x][i];
		if(to==fa[x])continue;
		fa[to]=x;
		DFS2(to);
		sum1+=dp[to][1],sum2+=min(dp[to][0],dp[to][1]);
	}
	dp[x][0]=sum1,dp[x][1]=sum2+p[x];
}
void DFS3(int x){
	ll sum1=0,sum2=0;
	for(int i=0;i<(int)q[x].size();i++){
		int to=q[x][i];
		if(to==fa[x])continue;
		fa[to]=x;
		DFS3(to);
		sum1+=dp2[to][1],sum2+=min(dp2[to][0],dp2[to][1]);
	}
	dp2[x][0]=sum1,dp2[x][1]=sum2+p[x];
	if(x==1)dp2[x][0]=inf;
}
void DFS4(int x){
	ll sum1=0,sum2=0;
	for(int i=0;i<(int)q[x].size();i++){
		int to=q[x][i];
		if(to==fa[x])continue;
		fa[to]=x;
		DFS4(to);
		sum1+=dp2[to][1],sum2+=min(dp2[to][0],dp2[to][1]);
	}
	dp2[x][0]=sum1,dp2[x][1]=sum2+p[x];
}
void work(int x,int y){
	ll now0=dp[x][0],now1=dp[x][1];
	if(y)now0=inf;
	else now1=inf;
	while(x!=1){
		ll g0=dp[fa[x]][0]+now1-dp[x][1];
		ll g1=dp[fa[x]][1]+min(now0,now1)-min(dp[x][0],dp[x][1]);
//		cout<<x<<" "<<dp[x][0]<<" "<<dp[x][1]<<" "<<now0<<" "<<now1<<endl;
		dp[x][0]=now0,dp[x][1]=now1;
		now0=g0,now1=g1;
		x=fa[x];
	}
//	cout<<x<<" "<<dp[x][0]<<" "<<dp[x][1]<<" "<<now0<<" "<<now1<<endl;
	dp[x][0]=now0,dp[x][1]=now1;
}
void work2(int x){
	while(x!=1){
		dp[x][0]=tmp[x][0],dp[x][1]=tmp[x][1];
		x=fa[x];
	}
	dp[x][0]=tmp[x][0],dp[x][1]=tmp[x][1];
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m,u,v;
	char tp[6];
	scanf("%d%d%s",&n,&m,tp);
	for(int i=1;i<=n;i++)scanf("%d",&p[i]);
	for(int i=1;i<n;i++){
		scanf("%d%d",&u,&v);
		q[u].push_back(v),q[v].push_back(u);
	}
	if(n<=2000&&m<=2000){
		for(int i=1;i<=m;i++){
			scanf("%d%d%d%d",&f1,&g1,&f2,&g2);
			DFS(1,-1);
			ll ans=min(dp[1][0],dp[1][1]);
			printf(ans>=inf?"-1\n":"%lld\n",ans);
		}
		return 0;
	}
	if(tp[0]=='B'){
		DFS2(1);
		for(int i=1;i<=n;i++)tmp[i][0]=dp[i][0],tmp[i][1]=dp[i][1];
//		for(int i=1;i<=n;i++)cout<<i<<" "<<dp[i][0]<<" "<<dp[i][1]<<endl;
		for(int i=1;i<=m;i++){
			scanf("%d%d%d%d",&f1,&g1,&f2,&g2);
			work(f1,g1);
			work(f2,g2);
			ll ans=min(dp[1][0],dp[1][1]);
			printf(ans>=inf?"-1\n":"%lld\n",ans);
			work2(f1),work2(f2);
		}
		return 0;
	}
	if(tp[0]=='A'&&tp[1]=='1'){
		memset(fa,0,sizeof fa);
		DFS2(1);
		dp[1][0]=inf;
		memset(fa,0,sizeof fa);
		DFS3(n);
//		for(int i=1;i<=n;i++)cout<<i<<" "<<dp[i][0]<<" "<<dp[i][1]<<endl;
//		cout<<endl;
//		for(int i=1;i<=n;i++)cout<<i<<" "<<dp2[i][0]<<" "<<dp2[i][1]<<endl;
//		cout<<endl;
		for(int i=1;i<=m;i++){
			scanf("%d%d%d%d",&f1,&g1,&f2,&g2);
			if(!g2){
				ll ans=dp[f2+1][1]+dp2[f2-1][1];
				printf(ans>=inf?"-1\n":"%lld\n",ans);
			}
			else {
				ll ans=min(dp[f2+1][0],dp[f2+1][1])+min(dp2[f2-1][0],dp2[f2-1][1])+p[f2];
				printf(ans>=inf?"-1\n":"%lld\n",ans);
			}
		}
		return 0;
	}
	if(tp[0]=='A'&&tp[1]=='2'){
		memset(fa,0,sizeof fa);
		DFS2(1);
		memset(fa,0,sizeof fa);
		DFS4(n);
		for(int i=1;i<=m;i++){
			scanf("%d%d%d%d",&f1,&g1,&f2,&g2);
			if(!g1&&!g2){
				printf("-1\n");
				continue;
			}
			if(f1>f2)swap(f1,f2),swap(g1,g2);
			ll ans=0;
			if(!g1)ans+=dp2[f1-1][1];
			else ans+=min(dp2[f1-1][0],dp2[f1-1][1])+p[f1];
			if(!g2)ans+=dp[f2+1][1];
			else ans+=min(dp[f2+1][0],dp[f2+1][1])+p[f2];
			printf(ans>=inf?"-1\n":"%lld\n",ans);
		}
		return 0;
	}
	return 0;
}
