#include<bits/stdc++.h>
#define N 5010
using namespace std;
int n,m,u,v;
vector<int>q[N];
int ans[N],cnt,tmp[N];
void DFS(int x,int fa){
	ans[++cnt]=x;
	for(int i=0;i<(int)q[x].size();i++){
		int to=q[x][i];
		if(to==fa)continue;
		DFS(to,x);
	}
}
int s[N],tot;
int fa[N];
bool vis[N],ok;
int h1[N],h2[N],f1,f2,f3,f4;
void DFS2(int x){
	vis[x]=1;
	for(int i=0;i<(int)q[x].size();i++){
		int to=q[x][i];
		if(to==fa[x])continue;
		if(vis[to]){
			if(ok)continue;
			int now=x;
			while(now!=to)s[++tot]=now,now=fa[now];
			s[++tot]=to;
			ok=1;
			continue;
		}
		fa[to]=x;
		DFS2(to);
	}
}
void DFS3(int x,int fa){
	tmp[++cnt]=x;
	for(int i=0;i<(int)q[x].size();i++){
		int to=q[x][i];
		if(to==fa)continue;
		if((f1==x&&f2==i)||(f3==x&&f4==i))continue;
		DFS3(to,x);
	}
}
bool judge(){
	if(!ans[1])return 1;
	for(int i=1;i<=n;i++){
		if(ans[i]<tmp[i])return 0;
		if(ans[i]>tmp[i])return 1;
	}
	return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		q[u].push_back(v),q[v].push_back(u);
	}
	for(int i=1;i<=n;i++)sort(q[i].begin(),q[i].end());
	if(m==n-1){
		DFS(1,-1);
		for(int i=1;i<=n;i++)printf("%d%c",ans[i],i==n?'\n':' ');
	}
	else {
		DFS2(1);
		for(int i=1;i<=tot;i++){
			int las=(i!=1?i-1:tot),nex=(i!=tot?i+1:1);
			for(int j=0;j<(int)q[s[i]].size();j++){
				int to=q[s[i]][j];
				if(to==s[las])h1[i]=j;
				if(to==s[nex])h2[i]=j;
			}
//			cout<<"!"<<s[i]<<" "<<s[las]<<" "<<h1[i]<<" "<<h2[i]<<endl;
		}
		for(int i=1;i<=tot;i++){
			int las=(i!=1?i-1:tot);
			f1=s[i],f2=h1[i],f3=s[las],f4=h2[las];
			cnt=0;
			DFS3(1,-1);
			if(judge()){
				for(int j=1;j<=n;j++)ans[j]=tmp[j];
			}
		}
		for(int i=1;i<=n;i++)printf("%d%c",ans[i],i==n?'\n':' ');
	}
	return 0;
}
