#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<=b;i++)
#define per(i,a,b) for(int i=a;i>=b;i--)
#define L(i,u) for(int i=head[u];i;i=nxt[i])
using namespace std;
inline void read(int &x){
	x=0;char c=getchar();int f=1;
	while(!isdigit(c)){if(c=='-')f=-1;c=getchar();}
	while(isdigit(c)){x=10*x+c-'0';c=getchar();}x*=f;
}
typedef long long ll;
const int N = 1020000, mo = 1e9+7;
int n,m;
int power(int a, int n){
	int res=1;
	while(n){
		if(n&1)res=1LL*res*a%mo;
		a=1LL*a*a%mo;n>>=1;
	}
	return res;
}
struct yz1r{
int n,m,a[15][15],v[17][18],res;
void print(){
	rep(i,1,n){rep(j,1,m)printf("%d",a[i][j]);puts("");}puts("");
}
inline void dfs(int x, int y){
	if(x>n){
		rep(i,1,n)memset(v[i],0,sizeof(v[i]));
		per(i,n,1)per(j,m,1)v[i][j]=a[i][j]+v[i][j+1];
		per(i,n,1)per(j,m,1)v[i][j]+=v[i+1][j];
		rep(i,1,n-1)rep(j,2,m)if(a[i][j]==a[i+1][j-1]){
			rep(x,i+1,n-1)rep(y,j+1,m)if(a[x][y]!=a[x+1][y-1])return;
		}
	//	if(a[i][j]==a[i+1][j-1]&&(v[i+1][j]-a[n][m]-a[i+1][j])!=0&&(v[i+1][j]+(a[n][m]==0)+(a[i+1][j]==0))!=(n-i)*(n-j+1)){return;}
		res++;return;
	}
	int nx=x,ny=y+1;if(ny>m)ny=1,nx++;
	a[x][y]=1;dfs(nx,ny);a[x][y]=0;
	if(!(x-1>=1&&y+1<=m&&a[x-1][y+1]==1)){a[x][y]=0;dfs(nx,ny);}
}
void solve(){
	dfs(1,1);cout<<res;
}
}yzr;
int main(){
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	read(n);read(m);
	if(n==1){
		cout<<power(2,m);return 0;
	}
	if(n==2){
		long long res=4LL*power(3,m-1)%mo;cout<<res;return 0;
	}
	if(n<=3&&m<=3){yzr.solve();return 0;}
	if(n==3){
		ll res=0;
		rep(x,1,m-1){
			if(x==m-1)res+=6LL*power(3,x-2)%mo;
			else {
				ll now=4;if(x>1)now*=2;
				now=now*power(3,max(x-2,0))%mo;
				res+=now;res%=mo;
			}
		}
		res+=6LL*power(3,m-2)%mo;
		res=(res%mo+mo)%mo;res=res*4%mo;cout<<res;
	}
	return 0;
}
