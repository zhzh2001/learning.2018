#include<bits/stdc++.h>
#define rep(i,a,b) for(register int i=a;i<=b;i++)
#define per(i,a,b) for(register int i=a;i>=b;i--)
#define SZ(x) ((int)x.size())
using namespace std;
inline void read(int &x){
	x=0;char c=getchar();int f=1;
	while(!isdigit(c)){if(c=='-')f=-1;c=getchar();}
	while(isdigit(c)){x=10*x+c-'0';c=getchar();}x*=f;
}
const int N = 10200;
int n,m,dfn[N],num,Fa[N],dot[N],tot,a,b,res[N],s[N],len,l[N];
vector<int>e[N];
inline int min(int a, int b){return a<b?a:b;}
inline int max(int a, int b){return a>b?a:b;}
inline void dfs(int u, int fa){
	s[++len]=u;
	rep(i,0,l[u]-1)if(e[u][i]!=fa&&(u+e[u][i]!=a+b||min(u,e[u][i])!=a))//(min(u,e[u][i])!=a||max(u,e[u][i])!=b))
		dfs(e[u][i],u);
}
void getcir(int u, int fa){
	dfn[u]=++num;Fa[u]=fa;
	rep(i,0,l[u]-1)if(e[u][i]!=fa)
		if(!dfn[e[u][i]])getcir(e[u][i],u);
		else if(dfn[e[u][i]]<dfn[u]){
			for(int v=u;;v=Fa[v]){dot[++tot]=v;if(v==e[u][i])break;}
		}
}
inline void upd(){
	bool flag=0;
	rep(i,1,n)if(s[i]!=res[i]){flag=s[i]<res[i];break;}
	if(flag)rep(i,1,n)res[i]=s[i];
}
int main(){
	freopen("travel.in","r",stdin);freopen("travel.out","w",stdout);
	read(n);read(m);rep(i,1,m){
		int u,v;read(u);read(v);e[u].push_back(v);e[v].push_back(u);
	}
	rep(i,1,n)sort(e[i].begin(),e[i].end());res[1]=1e9;
	rep(i,1,n)l[i]=SZ(e[i]);
	if(m==n-1){
		len=0;dfs(1,0);upd();
		rep(i,1,n)printf("%d ",res[i]);
		return 0;
	}
	getcir(1,0);res[1]=1e9;dot[tot+1]=dot[1];
	rep(i,1,tot){
		a=min(dot[i],dot[i+1]);b=max(dot[i],dot[i+1]);
		len=0;dfs(1,0);upd();
	}
	rep(i,1,n)printf("%d ",res[i]);
	return 0;
}
