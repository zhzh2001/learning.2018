#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<=b;i++)
#define per(i,a,b) for(int i=a;i>=b;i--)
#define L(i,u) for(int i=head[u];i;i=nxt[i])
using namespace std;
typedef long long ll;
inline void read(int &x){
	x=0;char c=getchar();int f=1;
	while(!isdigit(c)){if(c=='-')f=-1;c=getchar();}
	while(isdigit(c)){x=10*x+c-'0';c=getchar();}x*=f;
}
const int N = 202000;const ll inf = 1e13;
int n,q,w[N],head[N],nxt[N],to[N],edgenum;
void add(int u, int v){
	to[++edgenum]=v;nxt[edgenum]=head[u];head[u]=edgenum;
}
int v[N];bool gg;ll f[N][2];
inline void dfs(int u, int fa){
	f[u][0]=0;f[u][1]=w[u];
	L(i,u)if(to[i]!=fa){
		dfs(to[i],u);gg|=v[u]==0&&v[to[i]]==0;
		f[u][0]+=f[to[i]][1];f[u][1]+=min(f[to[i]][0],f[to[i]][1]);
	}
	if(v[u]==1)f[u][0]=inf;if(v[u]==0)f[u][1]=inf;
}

ll g[N<<2][2][2],s[2][2],t[2][2];
inline ll min(ll a, ll b){return a<b?a:b;}
void upd(int k){
	rep(a,0,1)rep(b,0,1){
		g[k][a][b]=inf;
		g[k][a][b]=min(g[k][a][b],g[k<<1][a][1]+g[k<<1][1][b]);
		g[k][a][b]=min(g[k][a][b],g[k<<1][a][1]+g[k<<1][0][b]);
		g[k][a][b]=min(g[k][a][b],g[k<<1][a][0]+g[k<<1][1][b]);
	}
}
void upd(ll s[2][2],ll t[2][2]){
	ll v[2][2];
	rep(a,0,1)rep(b,0,1){
		v[a][b]=inf;
		v[a][b]=min(v[a][b],s[a][1]+t[1][b]);
		v[a][b]=min(v[a][b],s[a][1]+t[0][b]);
		v[a][b]=min(v[a][b],s[a][0]+t[1][b]);
	}
	rep(a,0,1)rep(b,0,1)s[a][b]=v[a][b];
}
void build(int k, int l, int r){
	int mid=(l+r)>>1;rep(i,0,1)rep(j,0,1)g[k][i][j]=inf;
	if(l==r){g[k][0][0]=0;g[k][1][1]=w[l];return;}
	build(k<<1,l,mid);build(k<<1|1,mid+1,r);upd(k);
}
void query(int k, int l, int r, int L, int R, ll s[2][2]){
	if(l<=L&&r>=R){rep(i,0,1)rep(j,0,1)s[i][j]=g[k][i][j];return;}
	int mid=(L+R)>>1;
	if(r<=mid)query(k<<1,l,r,L,mid);
	else if(l>mid)query(k<<1|1,l,r,mid+1,R);
	else{
		query(k<<1,l,mid,L,mid,s);query(k<<1|1,mid+1,r,mid+1,R,t);
		upd(s,t);
	}
}
int main(){
	freopen("defense.in","r",stdin);freopen("defense.out","w",stdout);
	read(n);read(q);char tp[7];scanf("%s",tp);
	rep(i,1,n)read(w[i]);
	rep(i,1,n-1){int u,v;read(u);read(v);add(u,v);add(v,u);}
	if(n>2000){
		build(1,1,n);
		while(q--){
			int a,b,c,d;read(a);read(b);read(c);read(d);if(a>c){swap(a,c);swap(b,d);}
			if(a+1==c&&b==0&&d==0)puts("-1");
			else {
				ll res=(b*w[a])+(d*w[c]);
				if(1<=a-1){
					query(1,1,a-1,1,n,s);
					ll now=min(s[1][1],s[0][1]);
					if(b==1)now=min(now,min(s[1][0],s[0][0]));
					res+=now;
				}
				if(c+1<=n){
					query(1,c+1,n,1,n,s);
					ll now=min(s[1][0],s[1][1]);
					if(d==1)now=min(now,min(s[0][0],s[0][1]));
					res+=now;
				}
				if(a+1<=c-1){
					query(1,a+1,c-1,1,n,s);
					ll now=s[1][1];
					if(d==1)now=min(now,s[1][0]);
					if(b==1)now=min(now,s[0][1]);
					if(b==1&&d==1)now=min(now,s[0][0]);
				}
				printf("%lld\n",res);
			}
		}
		return 0;
	}
	
	while(q--){
		int a,b,c,d;read(a);read(b);read(c);read(d);
		rep(i,1,n)v[i]=-1;v[a]=b;v[c]=d;gg=0;
		dfs(1,0);
		if(gg)puts("-1");else printf("%lld\n",min(f[1][0],f[1][1]));
	}
	return 0;
}
