#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<=b;i++)
#define per(i,a,b) for(int i=a;i>=b;i--)
#define L(i,u) for(int i=head[u];i;i=nxt[i])
using namespace std;
typedef long long ll;
inline void read(int &x){
	x=0;char c=getchar();int f=1;
	while(!isdigit(c)){if(c=='-')f=-1;c=getchar();}
	while(isdigit(c)){x=10*x+c-'0';c=getchar();}x*=f;
}
const int N = 202000;const ll inf = 1e13;
int n,q,w[N],head[N],nxt[N],to[N],edgenum;
void add(int u, int v){
	to[++edgenum]=v;nxt[edgenum]=head[u];head[u]=edgenum;
}
int v[N];bool gg;ll f[N][2];
inline void dfs(int u, int fa){
	f[u][0]=0;f[u][1]=w[u];
	L(i,u)if(to[i]!=fa){
		dfs(to[i],u);gg|=v[u]==0&&v[to[i]]==0;
		f[u][0]+=f[to[i]][1];f[u][1]+=min(f[to[i]][0],f[to[i]][1]);
	}
	if(v[u]==1)f[u][0]=inf;if(v[u]==0)f[u][1]=inf;
}
int main(){
	freopen("defense2.in","r",stdin);freopen("defense.out","w",stdout);
	read(n);read(q);char tp[7];scanf("%s",tp);
	rep(i,1,n)read(w[i]);
	rep(i,1,n-1){int u,v;read(u);read(v);add(u,v);add(v,u);}
	while(q--){
		int a,b,c,d;read(a);read(b);read(c);read(d);
		rep(i,1,n)v[i]=-1;v[a]=b;v[c]=d;gg=0;
		dfs(1,0);
		if(gg)puts("-1");else printf("%lld\n",min(f[1][0],f[1][1]));
	}
	return 0;
}
