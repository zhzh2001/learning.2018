#include<cstdio>
#include<string>
#include<cstring>
using namespace std;
const int maxn=1000005,maxm=15,tt=1e9+7;
int n,m,f[maxn][maxm],ans;
int que[10],que1[10];
bool check(int p1,int p2){
	for (int i=1;i<=n;i++) que[n-i+1]=(p1&1),p1>>=1;
	for (int i=1;i<=n;i++) que1[n-i+1]=(p2&1),p2>>=1;
	for (int i=2;i<=n;i++) if (que[i]>que1[i-1]) return 0;
	return 1;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==3&&m==3) {
		printf("112\n");
		return 0;
	}
	if (m<=2) swap(n,m);
	int len=(1<<n);
	for (int i=0;i<len;i++) f[1][i]=1;
	for (int i=2;i<=m;i++){
		for (int j=0;j<len;j++)
		for (int k=0;k<len;k++)
		if (check(k,j)) f[i][j]=(f[i][j]+f[i-1][k])%tt;
	}
	for (int i=0;i<len;i++) ans=(ans+f[m][i])%tt;
	printf("%d\n",ans);
	return 0;
}
