#include<cstdio>
#include<string>
#include<cstring>
#define ll long long
using namespace std;
const int maxn=100005,maxm=3005;
int n,m,lnk[maxn],son[2*maxn],nxt[2*maxn],tot,p[maxn];
ll f[maxm][2];
bool pd[maxn],pd1[maxn],check=0;
char s[3];
void adde(int x,int y){
	son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot;
}
void dfs(int x,int fa){
	f[x][0]=f[x][1]=0ll;
	for (int j=lnk[x];j;j=nxt[j])
	if (son[j]!=fa) {
		dfs(son[j],x);
		if (f[son[j]][1]!=-1&&f[x][0]!=-1) f[x][0]+=f[son[j]][1];
		else f[x][0]=-1;
		if ((f[son[j]][0]!=-1||f[son[j]][1]!=-1)&&f[x][1]!=-1) {
			if (f[son[j]][0]!=-1&&(f[son[j]][1]==-1||f[son[j]][0]<f[son[j]][1]))
				f[x][1]+=f[son[j]][0];
			else f[x][1]+=f[son[j]][1];
		} else f[x][1]=-1;
	}
	if (pd[x]) f[x][0]=-1;
	if (pd1[x]) f[x][1]=-1;
	for (int j=lnk[x];j;j=nxt[j])
	if (son[j]!=fa&&pd1[son[j]]) f[x][0]=-1;
	if (f[x][1]!=-1) f[x][1]+=1ll*p[x];
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%s",s);
	for (int i=1;i<=n;i++) scanf("%d",&p[i]);
	for (int i=1;i<n;i++){
		int x,y; scanf("%d%d",&x,&y);
		adde(x,y); adde(y,x);
	}
	for (int i=1;i<=m;i++){
		int a,x,b,y; scanf("%d%d%d%d",&a,&x,&b,&y);
		if (x==1) pd[a]=1; else pd1[a]=1;
		if (y==1) pd[b]=1; else pd1[b]=1;
		memset(f,255,sizeof(f));
		dfs(1,0);
		if (f[1][0]==-1&&f[1][1]==-1) printf("-1\n");
		else {
			if (f[1][0]!=-1&&(f[1][1]==-1||f[1][0]<f[1][1])) printf("%d\n",f[1][0]);
			else printf("%lld\n",f[1][1]);
		}
		if (x==1) pd[a]=0; else pd1[a]=0;
		if (y==1) pd[b]=0; else pd1[b]=0;
	}
	return 0;
}
