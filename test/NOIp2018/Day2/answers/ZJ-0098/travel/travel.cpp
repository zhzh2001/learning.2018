#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=5005;
int n,m,n1,cnt,lnk[maxn],son[2*maxn],nxt[2*maxn],tot,que[maxn],fa[maxn],dep[maxn],id[maxn];
bool vis[maxn];
int ans[maxn];
struct dyt{
	int x,y,id;
	bool operator <(const dyt &b) const{
		return x<b.x||(x==b.x&&y>b.y);
	}
}a[2*maxn];
void adde(int x,int y){
	son[++tot]=y,nxt[tot]=lnk[x],lnk[x]=tot;
}
void adde1(int x,int y,int z){
	son[++tot]=y,id[tot]=z,nxt[tot]=lnk[x],lnk[x]=tot;
}
void dfs(int x){
	que[++cnt]=x;
	for (int j=lnk[x];j;j=nxt[j])
	if (!vis[son[j]]) {vis[son[j]]=1; dfs(son[j]);}
}
void DFS(int x,int idd){
	que[++cnt]=x;
	for (int j=lnk[x];j;j=nxt[j])
	if (!vis[son[j]]&&id[j]!=idd) {vis[son[j]]=1; DFS(son[j],idd);}
}
void compar_(){
	for (int i=1;i<=n;i++) {
		if (que[i]>ans[i]) return;
		else if (que[i]<ans[i]) break;
	}
	for (int i=1;i<=n;i++) ans[i]=que[i];
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m==n-1){
		for (int i=1;i<=m;i++){
			int x,y; scanf("%d%d",&x,&y);
			a[++n1].x=x,a[n1].y=y;
			a[++n1].x=y,a[n1].y=x;
		}
		sort(a+1,a+1+n1);
		for (int i=1;i<=n1;i++) adde(a[i].x,a[i].y);
		vis[1]=1; dfs(1);
		for (int i=1;i<=n;i++) printf("%d ",que[i]); printf("\n");
	} else {
		for (int i=1;i<=m;i++){
			int x,y; scanf("%d%d",&x,&y);
			a[++n1].x=x,a[n1].y=y,a[n1].id=i;
			a[++n1].x=y,a[n1].y=x,a[n1].id=i;
		}
		sort(a+1,a+1+n1);
		for (int i=1;i<=n1;i++) adde1(a[i].x,a[i].y,a[i].id);
		ans[1]=n+1;
		for (int i=1;i<=m;i++) {
			for (int j=1;j<=n;j++) vis[j]=0;
			cnt=0; vis[1]=1; DFS(1,i); bool check=0;
			for (int j=1;j<=n;j++) if (!vis[j]) {check=1; break;}
			if (!check) compar_();
		}
		for (int i=1;i<=n;i++) printf("%d ",ans[i]); printf("\n");
	}
	return 0;
}
