#include <bits/stdc++.h>
using namespace std;
#define N 5010
inline int read(){
	char ch=getchar(); int x=0;
	for (;ch>'9'||ch<'0';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x;
}
pair<int,int>Cut;
vector<int>G[N];
int S[N],top,ins[N],vis[N],tmp[N],ans[N],tot,Cir[N],len,n,m;
void dfs(int x,int f){
	printf("%d ",x); int t=G[x].size();
	for (int i=0;i<t;i++){
		int v=G[x][i]; if (v==f) continue;
		dfs(v,x);
	}
}
/*void dfs2(int x,int f){
	//printf("%d %d\n",x,f);
	if (len) return;
	S[++top]=x; ins[x]=1; vis[x]=1; int t=G[x].size();
	for (int i=0;i<t && !len;i++){
		int v=G[x][i];
		if (v==f) continue;
		if (vis[v]){
			if (len) return;
		//	printf("::%d %d\n",x,v);
		//	for (int i=1;i<=top;i++) printf("%d ",S[i]); puts("");
			int t=0;
			while (t!=v){
				t=S[top--];
			//	printf("%d\n",t);
				ins[t]=0;
				Cir[++len]=t;
			}
		//	puts("TT");
			return;
		}
		if (len) return;
		dfs2(v,x);
	}
	if (!ins[x]) top--;
}*/
void check(){
	if (tot!=n) return;
	if (ans[1]==0){memcpy(ans,tmp,sizeof(tmp)); return;}
	for (int i=1;i<=n;i++){
		if (tmp[i]>ans[i]) return;
		if (tmp[i]<ans[i]){
			memcpy(ans,tmp,sizeof(tmp));
			return;
		}
	}
}
void dfs3(int x,int f){
	tmp[++tot]=x; register int t=G[x].size(); vis[x]=1;
	for (register int i=0;i<t;i++){
		register int v=G[x][i];
		if (v==f) continue;
		if (vis[v]) continue;
		if ((v==Cut.first && x==Cut.second) || (v==Cut.second && x==Cut.first)) continue;
		dfs3(v,x);
	}
}
struct node{
	int u,v;
	node(){}
	node(int u,int v):u(u),v(v){}
}E[N<<1];
int main(){
	freopen("travel.in","r",stdin); freopen("travel.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=m;i++){
		int u=read(),v=read();
		G[u].push_back(v);
		G[v].push_back(u);
		E[i]=node(u,v);
	}
	for (int i=1;i<=m;i++)
		sort(G[i].begin(),G[i].end());
	if (m==n-1){
		dfs(1,0);
		puts("");
		return 0;
	}
/*	dfs2(1,0);
	puts("WTF");
	for (int i=1;i<=len;i++) printf("%d ",Cir[i]); puts("");
	for (int i=1;i<len;i++){
		Cut=make_pair(Cir[i],Cir[i+1]);
		memset(tmp,0,sizeof(tmp)); tot=0;
		dfs3(1,0);
		check();
	}
	Cut=make_pair(Cir[len],Cir[1]);
	memset(tmp,0,sizeof(tmp)); tot=0;
	dfs3(1,0);
	check();
	for (int i=1;i<=n;i++) printf("%d ",ans[i]); puts("");*/
	for (register int i=1;i<=m;i++){
		Cut=make_pair(E[i].u,E[i].v);
		memset(tmp,0,sizeof(tmp)); tot=0; memset(vis,0,sizeof(vis));
		dfs3(1,0);
		check();
	}
	for (int i=1;i<=n;i++) printf("%d ",ans[i]); puts("");
	return 0;
}
