#include <bits/stdc++.h>
using namespace std;
#define int long long
#define M 1000000007
int dp[5],g[5],n,m,a[5][5],T,G[64][64],ans;
bool getans(int x,int y,int P){
	if (P>T) return 0;
	if (x==n-1 && y==m-1){
		T=P;
		return 1;
	}
	int res=1;
	if (x!=n-1) res&=getans(x+1,y,P*2+a[x+1][y]);
	if (!res) return 0;
	if (y!=m-1) res&=getans(x,y+1,P*2+a[x][y+1]);
	return res;
}
bool check(int P){
	int tot=0;
	for (int i=0;i<n;i++)
		for (int j=0;j<m;j++){
			a[i][j]=(P>>tot)&1;
			tot++;
		}
	T=0;
	for (int i=0;i<n;i++) T=T*2+a[i][0];
	for (int j=1;j<m;j++) T=T*2+a[n-1][j];
	
	return getans(0,0,a[0][0]);
}
void getPP(int P){
	int tot=0;
	for (int i=0;i<n;i++)
		for (int j=0;j<m;j++){
			a[i][j]=(P>>tot)&1;
			tot++;
		}
	int P1=0,P2=0;
	for (int i=0;i<n;i++)
		for (int j=0;j<m-1;j++)
			P1=P1*2+a[i][j];
	for (int i=0;i<n;i++)
		for (int j=1;j<m;j++)
			P2=P2*2+a[i][j];
	G[P1][P2]++;
}
int getans(int P){
	int tot=0,res=1;
	for (int i=1;i<=m;i++){
		int t=(P>>tot)&1;
		if (!t){
			int tt=min(i,n);
			(res*=tt)%=M;
		}
		tot++;
	}
	for (int i=2;i<=n;i++){
		int t=(P>>tot)&1;
		if (!t){
			int tt=min(n-i+1,m);
			(res*=tt)%=M;
		}
		tot++;
	}
	printf("%lld:%lld\n",P,res);
	return res;
}
signed main(){
	freopen("game2.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if (n==1){
		int res=1;
		for (int i=1;i<=m;i++) (res*=2)%=M;
		printf("%lld\n",res);
		return 0;
	}
	if (n==2){
		dp[0]=dp[1]=dp[2]=dp[3]=1;
		for (int i=2;i<=m;i++){
			g[0]=((dp[0]+dp[1])%M+(dp[2]+dp[3])%M)%M;
			g[1]=(dp[2]+dp[3])%M;
			g[2]=g[0];
			g[3]=g[1];
			dp[0]=g[0]; dp[1]=g[1]; dp[2]=g[2]; dp[3]=g[3];
		}
		printf("%lld\n",(((dp[0]+dp[1])%M+dp[2])%M+dp[3])%M);
		return 0;
	}
	if (n<=3 && m<=3){
		int t=n*m; int ans=0;
		for (int i=0;i<(1<<t);i++) if (check(i)){
			getPP(i);
			if (i==448){puts("WWW");}
			for (int i=0;i<3;i++) {for (int j=0;j<3;j++) printf("%lld ",a[i][j]); puts("");}
			ans++;
			puts("");

		}
		int tot=0;
//		for (int i=0;i<64;i++) for (int j=0;j<64;j++) if (G[i][j]) printf("G[%lld][%lld]=%lld\n",i,j,G[i][j]),tot+=G[i][j];
		printf("%lld\n",ans);
	//	return 0;
	}
	if (n<=8 && m<=8){
		int t=n+m-1;
		for (int i=0;i<(1<<t);i++){
			(ans+=getans(i))%=M;
		}
		printf("%lld\n",ans);
		return 0;
	}
}
