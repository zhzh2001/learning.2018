#include <bits/stdc++.h>
using namespace std;
#define int long long
#define M 1000000007
int dp[6],g[6],n,m,a[6][6],T,G[64][64],c[64][64],tmp[64][64];
bool getans(int x,int y,int P){
	if (P>T) return 0;
	if (x==n-1 && y==m-1){
		T=P;
		return 1;
	}
	int res=1;
	if (x!=n-1) res&=getans(x+1,y,P*2+a[x+1][y]);
	if (!res) return 0;
	if (y!=m-1) res&=getans(x,y+1,P*2+a[x][y+1]);
	return res;
}
bool check(int P){
	int tot=0;
	for (int i=0;i<n;i++)
		for (int j=0;j<m;j++){
			a[i][j]=(P>>tot)&1;
			tot++;
		}
	T=0;
	for (int i=0;i<n;i++) T=T*2+a[i][0];
	for (int j=1;j<m;j++) T=T*2+a[n-1][j];
	
	return getans(0,0,a[0][0]);
}

signed main(){
	freopen("game.in","r",stdin); freopen("game.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if (n>m) swap(n,m);
	if (n==1){
		int res=1;
		for (int i=1;i<=m;i++) (res*=2)%=M;
		printf("%lld\n",res);
		return 0;
	}
	if (n==2){
		dp[0]=dp[1]=dp[2]=dp[3]=1;
		for (int i=2;i<=m;i++){
			g[0]=((dp[0]+dp[1])%M+(dp[2]+dp[3])%M)%M;
			g[1]=(dp[2]+dp[3])%M;
			g[2]=g[0];
			g[3]=g[1];
			dp[0]=g[0]; dp[1]=g[1]; dp[2]=g[2]; dp[3]=g[3];
		}
		printf("%lld\n",(((dp[0]+dp[1])%M+dp[2])%M+dp[3])%M);
		return 0;
	}
	if (n<=4 && m<=5){
		int t=n*m; int ans=0;
		for (int i=0;i<(1<<t);i++) if (check(i)) ans++;
		printf("%lld\n",ans);
		return 0;
	}
	if (n==3){
		int t=112;
		for (int i=4;i<=m;i++) (t*=3LL)%=M;
		printf("%lld\n",t);
		return 0;
	}
	if (n==4){
		int t=2688;
		for (int i=6;i<=m;i++) (t*=3LL)%=M;
		printf("%lld\n",t);
		return 0;
	}
	return 0;
}
