#include <bits/stdc++.h>
using namespace std;
#define int long long
#define N 100010
inline int read(){
	char ch=getchar(); int x=0;
	for (;ch>'9'||ch<'0';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x;
}
int ff[2010][2],he[N],ne[N<<1],e[N<<1],cnt,p[N],dp[N][2],S[N][2],fa[N],g[N][2];
char ss[6];
map<int,bool>mp[N];
void dfs(int x,int f,int a,int xx,int b,int yy){
	bool flag=0;
	ff[x][0]=ff[x][1]=0;
	for (int i=he[x];i;i=ne[i]){
		if (e[i]==f) continue;
		dfs(e[i],x,a,xx,b,yy);
		if (ff[e[i]][1]==-1) flag=1;
		ff[x][0]+=ff[e[i]][1];
		int tmp=1e18;
		if (ff[e[i]][0]!=-1) tmp=min(tmp,ff[e[i]][0]);
		if (ff[e[i]][1]!=-1) tmp=min(tmp,ff[e[i]][1]);
		ff[x][1]+=tmp;
	}
	ff[x][1]+=p[x];
	if (flag) ff[x][0]=1e18;
	if (x==a) ff[x][xx^1]=1e18;
	if (x==b) ff[x][yy^1]=1e18;
	ff[x][1]=min(ff[x][1],(int)1e18);
	ff[x][0]=min(ff[x][0],(int)1e18);
}
void add(int u,int v){ne[++cnt]=he[u];he[u]=cnt;e[cnt]=v;}
void dfs(int x,int f){
	fa[x]=f;
	dp[x][0]=dp[x][1]=0;
	for (int i=he[x];i;i=ne[i]){
		if (e[i]==f) continue;
		dfs(e[i],x);
		dp[x][0]+=dp[e[i]][1];
		dp[x][1]+=min(dp[e[i]][0],dp[e[i]][1]);
	}
	dp[x][1]+=p[x];
	for (int i=he[x];i;i=ne[i]){
		if (e[i]==f) continue;
		S[e[i]][0]=dp[x][0]-dp[e[i]][1];
		S[e[i]][1]=dp[x][1]-min(dp[e[i]][1],dp[e[i]][0]);
	}
}
void getans(int x,int p){
	g[x][p]=dp[x][p]; g[x][p^1]=1e18;
//	printf("%lld %lld\n",x,g[x][p]);
	int ne=x;
	for (x=fa[x];x;x=fa[x]){
		g[x][0]=S[ne][0]+g[ne][1];
		g[x][0]=min(g[x][0],(int)1e18);
		g[x][1]=S[ne][1]+min(g[ne][0],g[ne][1]);
		g[x][1]=min(g[x][1],(int)1e18);
		if (g[x][0]==dp[x][0] && g[x][1]==dp[x][1]){
			printf("%lld\n",dp[1][1]);
			return;
		}
	//	printf("ne=%lld:%lld %lld\n",ne,S[ne][0],S[ne][1]);
		ne=x;
	}
	printf("%lld\n",g[1][1]);
}
signed main(){
	freopen("defense.in","r",stdin); freopen("defense.out","w",stdout);
	int n=read(),m=read(); scanf("%s",ss);
	for (int i=1;i<=n;i++) scanf("%lld",&p[i]);
	for (int i=1;i<n;i++){
		int u=read(),v=read();
		add(u,v); add(v,u);
		mp[u][v]=1; mp[v][u]=1;
	}
	dfs(1,0);
//	for (int i=1;i<=n;i++) printf("%lld %lld\n",dp[i][0],dp[i][1]);
//	puts("WTF");
	while (m--){
		int a=read(),x=read(),b=read(),y=read();
		if (mp[a].count(b) && x==0 && y==0){puts("-1"); continue;}
	//	puts("WTF");
		if (n<=2000 && m<=2000){
			memset(ff,0,sizeof(ff));
			dfs(1,0,a,x,b,y); int ans=1e18;
			if (a==1) ans=ff[1][x]; else
			if (b==1) ans=ff[1][y]; else{
				if (ff[1][0]!=-1) ans=min(ans,ff[1][0]);
				if (ff[1][1]!=-1) ans=min(ans,ff[1][1]);
			}
	//		for (int i=1;i<=n;i++) printf("%lld %lld\n",ff[i][0],ff[i][1]);
			printf("%lld\n",ans);
			continue;
		}
		if (ss[0]=='B'){
			getans(b,y);
			continue;
		}
		if (ss[1]=='1') {getans(b,y); continue;}
		printf("%lld\n",min(dp[1][0],dp[1][1]));
	}
	return 0;
}
