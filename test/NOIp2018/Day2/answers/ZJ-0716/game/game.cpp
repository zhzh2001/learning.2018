#include <bits/stdc++.h>
using namespace std;
bool mmm1;


int n,m;
struct P20 {
	int ans;
	int A[200][200];
	struct node {
		int s,t;
		bool operator<(const node& x) const {
			return s<x.s;
		}
	} p[2005];
	int tot;
	void redfs(int x,int y,int u,int v) {
		if(x==n&&y==m) {
			p[++tot]=(node) {
				u,v
			};
			return;
		}
		if(x+1<=n) redfs(x+1,y,(u<<1)|0,(v<<1)|A[x+1][y]);
		if(y+1<=m) redfs(x,y+1,(u<<1)|1,(v<<1)|A[x][y+1]);
	}
	bool cdq(int L,int R) {
		if(L==R) return 0;
		int mid=(L+R)>>1;
		int mx=0;
		for(int i=mid+1; i<=R; ++i)
			mx=max(mx,p[i].t);
		for(int i=L; i<=mid; ++i)
			if(mx>p[i].t) return 1;
		if(cdq(L,mid)) return 1;
		if(cdq(mid+1,R)) return 1;
		return 0;
	}
	int chk() {
		tot=0;
		redfs(1,1,0,0);
		sort(p+1,p+1+tot);
		return !cdq(1,tot);
	}
	void dfs(int x,int y) {
		if(y==m+1) x++,y=1;
		if(x==n+1) {
			ans+=chk();
			return ;
		}
		A[x][y]=0;
		dfs(x,y+1);
		A[x][y]=1;
		dfs(x,y+1);
	}
	void solve() {
		ans=0;
		dfs(1,1);
		printf("%d\n",ans);
	}
} p20;
#define M 1000005
const int P=(int)1e9+7;
//MOD!!!!!!!!!!!!!!!
typedef long long ll;
struct P50 {
	ll F[M];
	void solve() {
		F[1]=4;
		for(int i=2; i<=m; ++i)
			F[i]=F[i-1]*3ll%P;
		printf("%lld\n",F[m]);
	}
} p50;

struct P65 {
	ll F[M];
	void solve() {
		F[1]=8;
		F[2]=36;
		F[3]=112;
		F[4]=336;
		for(int i=5; i<=m; ++i)
			F[i]=F[i-1]*3ll%P;
		printf("%lld\n",F[m]);
	}
} p65;

const int Table[][5]= {
	{2, 4, 8, 16, 32},
	{4, 12, 36, 108, 324},
	{8, 36, 112, 336, 1008},
	{16, 108, 336, 912, 2688},
	{32, 324, 1008, 2688, 7136}
};

struct Pun {
	ll F[M];
	void solve() {
		F[1]=16;
		F[2]=108;
		F[3]=336;
		F[4]=912;
		F[5]=2688;
		for(int i=6;i<=m;++i)
			F[i]=F[i-1]*3ll%P;
		printf("%lld\n",F[m]);
	}
} pun;

bool mmm2;
int main() {
//	cout<<(&mmm2-&mmm1)/1024.0/1024<<endl;
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n<=5 && m<=5) printf("%d\n",Table[n-1][m-1]);
	else if(n==2) p50.solve();
	else if(n==3) p65.solve();
	else if(n==4) pun.solve();
	else if(m==2||m==3||m==4) {
		swap(n,m);
		if(n==2) p50.solve();
		else if(n==3) p65.solve();
		else if(n==4) pun.solve();
	}
	else p20.solve();
	return 0;
}


