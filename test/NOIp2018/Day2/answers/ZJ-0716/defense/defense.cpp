#include <bits/stdc++.h>
using namespace std;
bool mmm1;
typedef long long ll;
#define M 100005
int n,m;
string str;
ll cost[M];

vector<int> E[M];

struct P50 {
	int mark[M];
	ll dp[M][2];
	void dfs(int x,int f) {
		if(mark[x]==-1) dp[x][1]=cost[x],dp[x][0]=0;
		else if(mark[x]==1) dp[x][1]=cost[x],dp[x][0]=(int)1e9;
		else dp[x][1]=(int)1e9,dp[x][0]=0;
		for(int i=0;i<(int)E[x].size();++i) {
			int y=E[x][i];
			if(y==f) continue;
			dfs(y,x);
			dp[x][1]+=min(dp[y][0],dp[y][1]);
			dp[x][0]+=dp[y][1];
		}
	}
	void solve() {
		memset(mark,-1,sizeof mark);
		for(int i=1;i<=m;++i) {
			int x,a,y,b;
			scanf("%d%d%d%d",&x,&a,&y,&b);
			mark[x]=a,mark[y]=b;
			dfs(1,0);
			mark[x]=mark[y]=-1;
			ll ans=min(dp[1][0],dp[1][1]);
			printf("%lld\n",ans<(int)1e9?ans:-1);
		}
	}
} p50;

struct Pun {
	int mark[M];
	ll dp[M][2];
	void dfs(int x,int f) {
		if(mark[x]==-1) dp[x][1]=cost[x],dp[x][0]=0;
		else if(mark[x]==1) dp[x][1]=cost[x],dp[x][0]=(int)1e9;
		else dp[x][1]=(int)1e9,dp[x][0]=0;
		for(int i=0;i<(int)E[x].size();++i) {
			int y=E[x][i];
			if(y==f) continue;
			dfs(y,x);
			dp[x][1]+=min(dp[y][0],dp[y][1]);
			dp[x][0]+=dp[y][1];
		}
	}
	void solve() {
		memset(mark,-1,sizeof mark);
		for(int i=1;i<=m;++i) {
			int x,a,y,b;
			scanf("%d%d%d%d",&x,&a,&y,&b);
			printf("%lld\n",min(dp[1][0],dp[1][1]));
		}
	}
} pun;

bool mmm2;
int main() {
//	cout<<(&mmm2-&mmm1)/1024.0/1024<<endl;
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d",&n,&m);
	cin>>str;
	for(int i=1;i<=n;++i) scanf("%lld",cost+i);
	for(int i=1;i<n;++i) {
		int a,b;
		scanf("%d%d",&a,&b);
		E[a].push_back(b);
		E[b].push_back(a);
	}
	if(n<=5000) p50.solve();
	else pun.solve();
	return 0;
}


