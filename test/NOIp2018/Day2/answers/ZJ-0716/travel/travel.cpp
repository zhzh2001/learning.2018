#include <bits/stdc++.h>
using namespace std;
bool mmm1;
#define M 5005
int n,m;
vector<int> E[M];
int tot;
int ans[M];
struct P50 {
	void dfs(int x,int f) {
		ans[++tot]=x;
		for(int i=0;i<(int)E[x].size();++i) {
			int y=E[x][i];
			if(y==f) continue;
			dfs(y,x);
		}
	}
	void solve() {
		tot=0;
		dfs(1,0);
		for(int i=1;i<=n;++i)
			printf("%d%c",ans[i]," \n"[i==n]);
	}
}p50;

int Fa[M];
int getfa(int x) {return Fa[x]==x?x:Fa[x]=getfa(Fa[x]);}

int X,Y;

struct P100 {
	int line[M];
	int A[M];
	int top;
	bool dfs(int x,int f) {
		line[++top]=x;
		if(x==Y) return 1;
		for(int i=0;i<(int)E[x].size();++i) {
			int y=E[x][i];
			if(y==f) continue;
			if(dfs(y,x)) return 1;
		}
		top--;
		return 0;
	}
	void redfs(int x,int f) {
		A[++tot]=x;
		for(int i=0;i<(int)E[x].size();++i) {
			int y=E[x][i];
			if(y==f||(X==x&&Y==y)||(X==y&&Y==x)) continue;
			redfs(y,x);
		}
	}
	bool cmp() {
		for(int i=1;i<=n;++i) 
			if(A[i]!=ans[i]) return A[i]<ans[i];
		return 0;
	}
	void solve() {
		top=tot=0;
		dfs(X,0);
		redfs(1,0);
		for(int i=1;i<=n;++i) ans[i]=A[i];
		E[X].push_back(Y);
		E[Y].push_back(X);
		sort(E[X].begin(),E[X].end());
		sort(E[Y].begin(),E[Y].end());
		for(int i=1;i<top;++i) {
			tot=0;
			X=line[i],Y=line[i+1];
			redfs(1,0);
			if(cmp()) for(int j=1;j<=n;++j) ans[j]=A[j];
		}
		for(int i=1;i<=n;++i)
			printf("%d%c",ans[i]," \n"[i==n]);
	}
} p100;

bool mmm2;
int main() {
//	cout<<(&mmm2-&mmm1)/1024.0/1024<<endl;
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;++i) Fa[i]=i;
	for(int i=1;i<=m;++i) {
		int a,b;
		scanf("%d%d",&a,&b);
		if(getfa(a)!=getfa(b)) {
			Fa[getfa(a)]=getfa(b);
			E[a].push_back(b);
			E[b].push_back(a);
		} else {
			X=a,Y=b;
		}
	}
	for(int i=1;i<=n;++i)
		sort(E[i].begin(),E[i].end());
	if(m==n-1) {
		p50.solve();
	} else {
		p100.solve();
	}
	return 0;
}


