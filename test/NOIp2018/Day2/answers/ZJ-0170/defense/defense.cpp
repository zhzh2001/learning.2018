#include <cstdio>
#include <vector>
#include <algorithm>
#include <cstring>
#define INF 100000000
using namespace std;
vector <int> v[100100];
int f[100100][3];
bool vis[100100];
int n,m,i,x,y,b,p,num,d,p1,p2;
int a[100100];char c;
int read(){
	int x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')
	    ch=getchar();
	while (ch>='0'&&ch<='9')
	    x=x*10+ch-48,
	    ch=getchar();
	return x;
}
int sc(int t,int p){
	vis[t]=1;
	if (f[t][p]!=-1) return f[t][p];
	int t1;
	if (p==0){
		t1=0;
		for (int i=0;i<v[t].size();i++)
		    if (!vis[v[t][i]])
		    	t1+=sc(v[t][i],1);
		f[t][p]=t1;	
	}
	else {
		t1=a[t];
		for (int i=0;i<v[t].size();i++)
			if (!vis[v[t][i]])
			t1+=min(sc(v[t][i],1),sc(v[t][i],0));	
		f[t][p]=t1;	
	}
	vis[t]=0;
	return f[t][p];
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read();m=read();
	while (c<'A'||c>'C')
	    c=getchar();	    
	num=read();
	for (i=1;i<=n;i++)
	    a[i]=read();
	for (i=1;i<n;i++){
		x=read();
		y=read();
		v[x].push_back(y);
		v[y].push_back(x);
	}
	while (m--){
		b=read();x=read();
		d=read();y=read();
		memset(f,255,sizeof(f));
		f[b][1-x]=INF;f[d][1-y]=INF;
		memset(vis,0,sizeof(vis));	
		p1=sc(1,0);
		memset(vis,0,sizeof(vis));	
		p2=sc(1,1);		
		p=min(p1,p2);
		
		printf("%d\n",p>=INF?-1:p);
	}
	return 0;
}
