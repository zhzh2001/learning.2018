#include <cstdio>
typedef long long ll;

const int MOD = 1000000007;

int n, m, ans, f[10][10];

inline int fpow(int a, int b) {
	int res = 1;
	for (; b; b >>= 1, a = (ll)a * a % MOD) if (b & 1) res = (ll)res * a % MOD;
	return res;
}

inline void init() {
	f[1][1] = 2;
	f[1][2] = 4;
	f[1][3] = 8;
	f[1][4] = 16;
	f[1][5] = 32;
	f[1][6] = 64;
	f[1][7] = 128;
	f[2][1] = 4;
	f[2][2] = 12;
	f[2][3] = 36;
	f[2][4] = 108;
	f[2][5] = 324;
	f[2][6] = 972;
	f[2][7] = 2916;
	f[3][1] = 8;
	f[3][2] = 36;
	f[3][3] = 112;
	f[3][4] = 336;
	f[3][5] = 1008;
	f[3][6] = 3024;
	f[3][7] = 9072;
	f[4][1] = 16;
	f[4][2] = 108;
	f[4][3] = 336;
	f[4][4] = 912;
	f[4][5] = 2688;
	f[4][6] = 8064;
	f[4][7] = 24192;
	f[5][1] = 32;
	f[5][2] = 324;
	f[5][3] = 1008;
	f[5][4] = 2688;
	f[5][5] = 7136;
}

inline int solve_1_to_4(int n, int m) {
	return f[n][m];
}

inline int solve_11_13(int n, int m) {
	return (ll)112 * fpow(3, m - 3) % MOD;
}

inline int solve_5_to_10(int n, int m) {
	return (ll)fpow(3, m - 1) * 4 % MOD;
}

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	init();
	scanf("%d%d", &n, &m);
	if (n == 2) ans = solve_5_to_10(n, m);
	else if (n <= 5 && m <= 5) ans = solve_1_to_4(n, m);
	else if (n == 3) ans = solve_11_13(n, m);
	else if (n == 4) ans = 2688 * fpow(3, m - 5);
	else { if (m < n) n ^= m ^= n ^= n; ans = f[n][m]; }
	printf("%d\n", ans);
	return 0;
}
