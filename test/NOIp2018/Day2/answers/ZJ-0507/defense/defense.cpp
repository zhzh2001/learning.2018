#include <cstdio>
typedef long long ll;

const int MAXN = 100005;
const ll INF = 0x3F3F3F3F3F3F3F3F;

int n, m, a, x, b, y, val[MAXN], fa[MAXN]; ll f[MAXN][2], g[2], h[2], sum[2];
char typ[10];

template <typename T>
inline void read(T &x) {
	int fl = 0, ch;
	while (ch = getchar(), ch < 48 || 57 < ch) fl ^= !(ch ^ 45); x = (ch & 15);
	while (ch = getchar(), 47 < ch && ch < 58) x = x * 10 + (ch & 15);
	if (fl) x = -x;
}

struct ed {
	ed *nxt;
	int to;
} pool[MAXN << 1], *p = pool, *lnk[MAXN];

inline void add_e(int u, int v) { *++p = (ed) { lnk[u], v }, lnk[u] = p; }

inline ll min(ll a, ll b) {
	if (a == -INF) return b;
	if (b == -INF) return a;
	return a < b ? a : b;
}

inline void dfs(int u, int fafa) {
	f[u][0] = 0, f[u][1] = val[u];
	if (a == u) f[u][x ^ 1] = -INF;
	if (b == u) f[u][y ^ 1] = -INF;
	for (ed *i = lnk[u]; i; i = i->nxt) if (i->to != fafa) {
		dfs(i->to, u);
		f[u][0] += f[i->to][1];
		f[u][1] += min(f[i->to][0], f[i->to][1]);
		f[u][0] = f[u][0] < 0 ? -INF : f[u][0];
		f[u][1] = f[u][1] < 0 ? -INF : f[u][1];
	}
}


inline void dfs2(int u) {
	f[u][0] = 0, f[u][1] = val[u];
	if (a == u) f[u][x ^ 1] = -INF;
	if (b == u) f[u][y ^ 1] = -INF;
	for (ed *i = lnk[u]; i; i = i->nxt) if (i->to != fa[u]) {
		dfs2(i->to);
		fa[i->to] = u;
		f[u][0] += f[i->to][1];
		f[u][1] += min(f[i->to][0], f[i->to][1]);
		f[u][0] = f[u][0] < 0 ? -INF : f[u][0];
		f[u][1] = f[u][1] < 0 ? -INF : f[u][1];
	}
}

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	read(n), read(m), scanf("%s", typ);
	for (int i = 1; i <= n; ++i) read(val[i]), sum[i & 1] += val[i];
	for (int i = 1, u, v; i < n; ++i) {
		read(u), read(v);
		add_e(u, v), add_e(v, u);
	}
	if (n <= 2000 && m <= 2000) {
		while (m--) {
			read(a), read(x), read(b), read(y);
			dfs(1, 1);
			if (f[1][0] == -INF && f[1][1] == -INF) puts("-1");
			else printf("%lld\n", min(f[1][0], f[1][1]));
		}
	} else if (typ[0] == 'A') {
		while (m--) {
			read(a), read(x), read(b), read(y);
			if (((a^x) & 1) ^ ((b^y) & 1)) puts("-1");
			else printf("%lld\n", sum[(a ^ x ^ 1) & 1]);
		}
	}
//	else if (typ[0] == 'B') {
//		dfs2(1);
//		while (m--) {
//			read(a), read(x), read(b), read(y); int u = fa[b], v = b;
//			h[0] = f[b][0], h[1] = f[b][1];
//			while (u) {
//				g[0] = f[u][0], g[1] = f[u][1];
//				g[0] -= h[1];
//				g[1] -= min(h[0], h[1]);
//				if (b == v) h[y ^ 1] = -INF;
//				g[0] = g[0] < 0 ? -INF : g[0];
//				g[1] = g[1] < 0 ? -INF : g[1];
//				h[0] = g[0], h[1] = g[1];
//				v = u;
//				u = fa[u];
//			}
//		}
//	}
	return 0;
}
