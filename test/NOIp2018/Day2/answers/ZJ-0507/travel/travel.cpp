#include <cstdio>
#include <vector>
#include <algorithm>

const int MAXN = 5005;

int n, m, nod = -1, qu, qv, flg, ans[MAXN], now[MAXN], uu[MAXN], vv[MAXN];
bool vis[MAXN], hav[MAXN];
std::vector <int> g[MAXN];

template <typename T>
inline void read(T &x) {
	int fl = 0, ch;
	while (ch = getchar(), ch < 48 || 57 < ch) fl ^= !(ch ^ 45); x = (ch & 15);
	while (ch = getchar(), 47 < ch && ch < 58) x = x * 10 + (ch & 15);
	if (fl) x = -x;
}

namespace A {
	void dfs(int u, int fa) {
		ans[++ans[0]] = u;
		for (std::vector <int>::iterator it = g[u].begin(); it != g[u].end(); ++it)
			if (*it != fa) dfs(*it, u);
	}
}

namespace B {
	void dfs(int u, int fa) {
		ans[++ans[0]] = u, vis[u] = 1;
		for (std::vector <int>::iterator it = g[u].begin(); it != g[u].end(); ++it)
			if (*it != fa) {
				if (vis[*it]) {
					hav[u] = 1;
					if (nod == -1) nod = *it;
				}
				else {
					dfs(*it, u);
					if (hav[*it] && *it != nod) {
						hav[u] = 1;
					}
				}
			}
	}
	void dfs2(int u, int fa) {
		now[++now[0]] = u;
		if (!flg && now[now[0]] < ans[now[0]]) flg = 1;
		else if (!flg && now[now[0]] > ans[now[0]]) {
			flg = -1;
			return;
		}
		for (std::vector <int>::iterator it = g[u].begin(); it != g[u].end(); ++it)
			if (*it != fa && (u != qu || *it != qv) && (u != qv || *it != qu)) dfs2(*it, u);
	}
	void solve() {
		dfs(1, 1);
		for (int i = 1; i <= m; ++i) {
			qu = uu[i], qv = vv[i];
			if (hav[qu] && hav[qv]) {
				flg = now[0] = 0;
				dfs2(1, 1);
				if (flg == 1) {
					for (int i = 1; i <= n; ++i) ans[i] = now[i];
				}
			}
		}
	}
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	read(n), read(m);
	for (int i = 1, u, v; i <= m; ++i) {
		read(u), read(v); uu[i] = u, vv[i] = v;
		g[u].push_back(v), g[v].push_back(u);
	}
	for (int i = 1; i <= n; ++i) std::sort(g[i].begin(), g[i].end());
	if (m == n - 1) A::dfs(1, 1);
	else B::solve();
	for (int i = 1; i <= n; ++i) if (i < n) printf("%d ", ans[i]); else printf("%d\n", ans[i]);
	return 0;
}
