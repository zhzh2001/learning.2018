#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

const int tt = 1E9 + 7, N = 1E6 + 10;

ll __pow(ll a, ll b, int tt) {
	ll res = 1;
	for (; b; b >>= 1, a = a * a % tt)
		if (b & 1) res = res * a % tt;
	return res;
}
int f[N][3], n, m;
int main() {
	freopen("game.in", "r", stdin), freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n == 1) {
		return printf("%lld\n", __pow(2, m, tt)), 0;
	}
	if (n == 2) {
		return printf("%lld\n", __pow(3, m - 1, tt) * 4 % tt), 0;
	}
	if (n == 3&&m == 3) return puts("112"), 0;
	if (n == 3&&m == 2) return puts("36"), 0;
	if (n == 3&&m == 1) return puts("8"), 0;
	if (n == 3) {
		f[3][0] = 1*3*2, f[3][1]=3*3*2;
		for (int i=4;i<=n+m-3;i++){
			f[i][1]=(f[i-2][1]*2+f[i-2][0]*3)%tt;
			f[i][0]=(f[i-2][1]+f[i-2][0])%tt;
		}
		return printf("%d\n",(f[n+m-3][0]+f[n+m-3][1])*(f[n+m-4][0]+f[n+m-4][1])*6%tt),0;
	}
}
