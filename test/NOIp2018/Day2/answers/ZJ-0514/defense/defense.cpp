#include <bits/stdc++.h>

#define pb push_back

using namespace std;

typedef long long ll;

const int N = 1E5 + 10;

vector <int> Vector[N];
int p[N], n, q;
char s[N];

namespace subtask1 {
	ll f[2005][2];
	
	int U, vu, V, vv;
	
	void dp(int u, int fa) {
		f[u][1] = p[u], f[u][0] = 0;
		if (U == u) f[u][vu ^ 1] = 1ll << 60;
		if (V == u) f[u][vv ^ 1] = 1ll << 60;
		for (vector <int> :: iterator it = Vector[u].begin(); it != Vector[u].end(); it++)
			if (*it != fa) {
				dp(*it, u);
				f[u][1] += min(f[*it][0], f[*it][1]);
				f[u][0] += f[*it][1];
			}
	}
	int main() {
		while (q--) {
			scanf("%d%d%d%d", &U, &vu, &V, &vv);
			dp(1, 0);
			printf("%d\n", min(f[1][0], f[1][1]) < 1ll << 60 ? min(f[1][0], f[1][1]) : -1);
		}
		return 0;
	} 
}

int main() {
	freopen("defense.in", "r", stdin), freopen("defense.out", "w", stdout);
	scanf("%d%d%s", &n, &q, s + 1);
	for (int i = 1; i <= n; i++)
		scanf("%d", p + i);
	for (int a, b, i = 1; i < n; i++)
		scanf("%d%d", &a, &b), Vector[a].pb(b), Vector[b].pb(a);
	return subtask1 :: main();
}
