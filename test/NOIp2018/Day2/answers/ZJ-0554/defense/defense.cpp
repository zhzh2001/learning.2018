#include <bits/stdc++.h>
using namespace std;

#define rep(i, s, t) for(int i = s; i <= t; ++i)
#define per(i, s, t) for(int i = t; i >= s; --i)
#define repo(i, s, t) for(int i = s; i < t; ++i)
#define szof(x) (int)(x).size()
#define debug(x) cerr << #x << " : " << x << endl

bool MMmark1;

const int MAXN = 100005;
const int INF = 1e9 + 5;

int n, m;
string type;
int p[MAXN];
int A[MAXN], B[MAXN];
int x[MAXN], f1[MAXN], y[MAXN], f2[MAXN];

struct Part1 {
	vector <int> g[MAXN];
	int mark[MAXN];
	int dp[MAXN][2];
	int rec(int x, int fa, bool f) {
		if (dp[x][f] != INF) return dp[x][f];
		if (mark[x] == 0 && !f) return dp[x][f] = -1;
		
		int res1 = p[x];
		bool x1 = false;
		repo (i, 0, szof(g[x])) {
			int t = g[x][i];
			if (t == fa) continue;
			res1 += rec(t, x, 1);
			if (rec(t, x, 1) == -1) x1 = true;
		}
		if (x1) res1 = -1;
		if (!f) return dp[x][f] = res1;
		
		int res2 = 0;
		bool x2 = false;
		repo (i, 0, szof(g[x])) {
			int t = g[x][i];
			if (t == fa) continue;
			res2 += rec(t, x, 0);
			if (rec(t, x, 0) == -1) x2 = true;
		}
		if (x2) res2 = -1;
		
		if (mark[x] == 0) return dp[x][f] = res2;
		if (mark[x] == 1) return dp[x][f] = res1;
		if (res2 == -1) return dp[x][f] = res1;
		else if (res1 == -1) return dp[x][f] = res2;
		else return dp[x][f] = min(res1, res2);
	}
	void main() {
		rep (i, 1, n - 1) {
			g[A[i]].push_back(B[i]);
			g[B[i]].push_back(A[i]);
		}
		memset(mark, -1, sizeof(mark));
		rep (i, 1, m) {
			mark[x[i - 1]] = -1;
			mark[y[i - 1]] = -1;
			mark[x[i]] = f1[i];
			mark[y[i]] = f2[i];
			
			rep (i, 1, n) dp[i][0] = dp[i][1] = INF;
			printf("%d\n", rec(1, 1, 1));
		}
	}
} part1;

bool MMmark2;

void Read() {
	cin >> n >> m >> type;
	rep (i, 1, n) scanf("%d", &p[i]);
	rep (i, 1, n - 1) scanf("%d %d", &A[i], &B[i]);
	rep (i, 1, m) scanf("%d %d %d %d", &x[i], &f1[i], &y[i], &f2[i]);
}
void Solve() {
	if (n <= 2000 && m <= 2000) {
		part1.main();
	}
}
int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	
	Read();
	Solve();
	
	cerr << ((&MMmark2 - &MMmark1) >> 20) << " MB" << endl;
	return 0;
}
