#include <bits/stdc++.h>
using namespace std;

#define rep(i, s, t) for(int i = s; i <= t; ++i)
#define per(i, s, t) for(int i = t; i >= s; --i)
#define repo(i, s, t) for(int i = s; i < t; ++i)
#define szof(x) (int)(x).size()
#define debug(x) cerr << #x << " : " << x << endl

bool MMmark1;

const int MAXN = 10, MAXM = 1000005;
const int MOD = 1e9 + 7;

void addp(int &x, int y) {
	x += y;
	x %= MOD;
}
void delp(int &x, int y) {
	x -= y;
	x += MOD;
	x %= MOD;
}

int n, m;

struct Part1 {
	static const int MAXN = 5;
	bool map[MAXN][MAXN];
	bool chk() {
		repo (i, 0, n) {
			repo (j, 0, m) {
//				cerr << map[i][j] << " ";
				if (i - 1 >= 0 && j + 1 < m && map[i][j] < map[i  - 1][j + 1]) return false;
				if (i <= n - 2  && j >= 2 && i - 1 >= 0 && j + 1 < m && map[i][j] <= map[i  - 1][j + 1]) return false;
				
			}
//			cerr << endl;
		}
		return true;
	}
	void main() {
		int S = 1 << (n * m);
		int res = 0;
		repo (s, 0, S) {
			repo (i, 0, n) {
				repo (j, 0, m) {
					int p = i * m + j;
					map[i][j] = s >> p & 1;
				}
			}
			if (chk()) res++;
		}
		cout << res << endl;
	}
} part1;

struct Part8_8 {
	static const int MAXN = 8, MAXS = 1 << MAXN;
	static const int MAXM = 10; 
	#define cut(S, p) (((S) >> (p)) & 1)
	int S;
	int dp[MAXM][MAXS];
	int trans[MAXS][MAXS], sqr[MAXN][MAXS][MAXS];
	void inittrans() {
		int dp[MAXN][2];
		repo (S1, 0, S) {
			repo (S2, 0, S) {
				dp[1][0] = 1, dp[1][1] = 1;
				if (cut(S2, 0) != 1) continue;
				repo (i, 1, n) {
					int x = cut(S1, i), y = cut(S2, i);
					if (x == 1 && y == 1) {
						dp[i + 1][0] = dp[i][0];
						dp[i + 1][1] = (dp[i][0] + dp[i][1]) % MOD;
					}
					else if (x == 1 && y == 0) dp[i + 1][0] = dp[i + 1][1] = 0;
					else if (x == 0 && y == 1) {
						dp[i + 1][0] = 0;
						dp[i + 1][1] = dp[i][0];
					}
					else if (x == 0 && y == 0 ) {
						dp[i + 1][0] = dp[i][0];
						dp[i + 1][1] = dp[i][1];
					}
				}
				trans[S1][S2] = (dp[n][0] + dp[n][1]) % MOD;
			}
		}
	}
	
	void initsqr() {
		int dp[MAXN][2];
		per (k, 1, n - 1) {
			repo (S1, 0, 1 << (k + 1)) repo (S2, 0, 1 << k) {
				if (cut(S2, 0) != 1) continue;
				dp[1][0] = 1, dp[1][1] = 1;
				repo (i, 1, k) {
					int x = cut(S1, i + 1), y = cut(S2, i);
					if (x == 0 && y == 0) {
						dp[i + 1][0] = dp[i][0];
						dp[i + 1][1] = dp[i][1];
					}
					else if (x == 0 && y == 1) {
						dp[i + 1][0] = 0;
						dp[i + 1][1] = dp[i][0];
					}
					else if (x == 1 && y == 0) {
						dp[i + 1][0] = dp[i + 1][1] = 0;
					}
					else {
						dp[i + 1][0] = dp[i][0];
						dp[i + 1][1] = (dp[i][0] + dp[i][1]) % MOD;
					}
				}
				sqr[k][S1 << (n - k - 1)][S2 << (n - k)] = (dp[k][0] + dp[k][1]) % MOD;
				debug(sqr[k][S1 << (n - k - 1)][S2 << (n - k)]);
			}
		}
	}
	void main() {
		debug(n);
		S = 1 << n;
		inittrans();
		initsqr();
		dp[0][1] = 1;
		int t = n + m - 1;
		rep (i, 1, t) {
			repo (S1, 0, S) repo (S2, 0, S) {
				if (i > m) {
					addp(dp[i][S2], 1ll * dp[i - 1][S1] * sqr[m + m - i][S1][S2] % MOD);
				}
				else addp(dp[i][S2], 1ll * dp[i - 1][S1] * trans[S1][S2] % MOD);
			}
			if (i < n) {
				repo (s, 0, S) {
					bool d = false;
					repo (j, i, n) if (cut(s, j)) d = true;
					if (d) dp[i][s] = 0;
				}
			}
//			cerr << i << " --------------" << endl;
//			repo (s, 0, S) {
//				printf("%d %d\n", s, dp[i][s]);
//			}
		}
		int res = 0;
		repo (s, 0, S) {
			addp(res, dp[t][s]);
		}
		rep (i, 1, t) {
			int now = 0;
			repo (s, 0, S) addp(now, dp[i][s]);
			debug(now);
		}
		cout << res << endl;
	}
	#undef cut
} part8_8;

bool MMmark2;

void Read() {
	scanf("%d %d", &n, &m);
}
void Solve() {
	if (0);
	else if (n <= 3 && m <= 3) part1.main();
	else part8_8.main();
}
int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	
	Read();
	Solve();
	
	cerr << ((&MMmark2 - &MMmark1) >> 20) << " MB" << endl;
	return 0;
}
