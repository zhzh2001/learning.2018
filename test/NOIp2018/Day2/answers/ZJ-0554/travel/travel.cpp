#include <bits/stdc++.h>
using namespace std;

#define rep(i, s, t) for(int i = s; i <= t; ++i)
#define per(i, s, t) for(int i = t; i >= s; --i)
#define repo(i, s, t) for(int i = s; i < t; ++i)
#define szof(x) (int)(x).size()
#define debug(x) cerr << #x << " : " << x << endl

bool MMmark1;

const int MAXN = 5005;
int n, m;
int A[MAXN], B[MAXN];
vector <int> g[MAXN];

struct Partn_1 {

	bool mark[MAXN];
	int res[MAXN], tot;
	void dfs(int x) {
		res[++tot] = x;
		mark[x] = true;
		repo (i, 0, szof(g[x])) {
			int t = g[x][i];
			if (mark[t]) continue;
			dfs(t);
		}
	}
	
	void main() {
		rep (i, 1, n) sort(g[i].begin(), g[i].end());
		dfs(1);
		rep (i, 1, tot) printf("%d%c", res[i], " \n"[i == n]);
	}
} partn_1;

struct Partn {
	int dx, dy;
	int res[MAXN];
	int now[MAXN], tot;
	bool mark[MAXN];
	bool can(int x, int y) {
		if (x == dx && y == dy) return false;
		if (x == dy && y == dx) return false;
		return true;
	}
	void dfs(int x) {
		now[++tot] = x;
		mark[x] = true;
		repo (i, 0, szof(g[x])) {
			int t =g[x][i];
			if (mark[t] || !can(x, t)) continue;
			dfs(t);
		}
	}
	void update() {
		bool f = false;
		rep (i, 1, tot) {
			if (now[i] > res[i]) break;
			if (now[i] < res[i]) {
				f = true;
				break;
			}
		}
		if (f) rep (i, 1, tot) res[i] = now[i];
	}
	
	void main() {
		rep (i, 1, n) sort(g[i].begin(), g[i].end());
		rep (i, 1, n) res[i] = n;
		rep (i, 1, m) {
			dx = A[i], dy = B[i];
			tot = 0;
			memset(mark, 0, sizeof(mark));
			if ((dx == 1 || dy == 1) && szof(g[1]) == 1) continue;
			else dfs(1);
			update();
		}
		rep (i, 1, n) printf("%d%c", res[i], " \n"[i == n]);
	}
} partn;

bool MMmark2;

void Read() {
	cin >> n >> m;
	rep (i, 1, m) {
		int x, y;
		scanf("%d %d", &x, &y);
		A[i] = x, B[i] = y;
		g[x].push_back(y);
		g[y].push_back(x);
	}
}
void Solve() {
	if (m == n - 1) partn_1.main();
	else partn.main();
}
int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	
	Read();
	Solve();
	
	cerr << ((&MMmark2 - &MMmark1) >> 20) << " MB" << endl;
	return 0;
}
