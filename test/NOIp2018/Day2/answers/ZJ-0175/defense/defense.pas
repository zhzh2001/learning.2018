var
    n,m,x,y,xx,yy,t,ax,ay,len,i:longint;
    space,cx,cy:char;
    edge,next,first,cost,fa,deep:array[0..200001] of longint;
    f,ff:array[0..100001,0..1] of longint;
    vis:array[0..100001] of boolean;

procedure add(x,y:longint);
begin
    inc(len);
    edge[len]:=y;next[len]:=first[x];first[x]:=len;
end;

function min(a,b:longint):longint;
begin
    if a<b then exit(a) else exit(b);
end;

procedure dfs(x:longint);
var i,y:longint;
begin
    i:=first[x];vis[x]:=true;
    while i>-1 do
        begin
            y:=edge[i];
            if vis[y]=true then
                begin
                    i:=next[i];
                    continue;
                end;
            fa[y]:=x;deep[y]:=deep[x]+1;
            dfs(y);
            if (x=xx) then
                begin
                    if (ax=0) then
                        begin
                            f[x,0]:=f[x,0]+f[y,1];
                            f[x,1]:=100000000;
                        end else
                            begin
                                f[x,1]:=f[x,1]+min(f[y,0],f[y,1]);
                                f[x,0]:=100000000;
                            end
                end else
                    if (x=yy) then
                        begin
                            if (ay=0) then
                                begin
                                    f[x,0]:=f[x,0]+f[y,1];
                                    f[x,1]:=100000000;
                                end else
                                    begin
                                        f[x,1]:=f[x,1]+min(f[y,0],f[y,1]);
                                        f[x,0]:=100000000;
                                    end;
                        end else
                            begin
                                f[x,0]:=f[x,0]+f[y,1];
                                f[x,1]:=f[x,1]+min(f[y,0],f[y,1]);
                            end;
            i:=next[i];
        end;
    if (x=xx) and (ax=0) or (x=yy) and (ay=0) then exit;
    f[x,1]:=f[x,1]+cost[x];
end;

procedure update(dx,dax,z:longint);
begin
    if (dx<>x) then
        dec(ff[dx,dax],z);
    if dx=1 then exit;
    if (z=1) then
        update(fa[dx],0,z) else
            update(fa[dx],1,z);
end;

begin
    assign(input,'defense.in');reset(input);
    assign(output,'defense.out');rewrite(output);
    readln(n,m,space,cx,cy);
    for i:=1 to n do
        read(cost[i]);
    fillchar(first,sizeof(first),255);
    for i:=1 to n-1 do
        begin
            readln(x,y);
            add(x,y);add(y,x);
        end;
    for i:=1 to m do
        begin
            readln(xx,ax,yy,ay);
            fillchar(f,sizeof(f),0);
            fillchar(vis,sizeof(vis),false);
            fillchar(fa,sizeof(fa),0);
            fillchar(deep,sizeof(deep),0);
            dfs(1);
            if ((fa[xx]=yy) or (fa[yy]=xx)) and (ax=0) and (ay=0) then
                 begin
                     writeln(-1);
                     continue;
                 end;
            if f[1,0]=0 then writeln(f[1,1]) else
                if f[1,1]=0 then writeln(f[1,0]) else
                    writeln(min(f[1,0],f[1,1]));
        end;
    close(input);close(output);
end.



