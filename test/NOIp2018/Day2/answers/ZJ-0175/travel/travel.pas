var
    n,m,i,j,len,w:longint;
    edge,next,first,q,ans,x,y:array[0..100000] of longint;
    f:array[0..5001,0..5001] of boolean;
    vis:array[0..10001] of boolean;
    flag:boolean;

procedure add(x,y:longint);
begin
    inc(len);
    edge[len]:=y;next[len]:=first[x];first[x]:=len;
end;

procedure dfs(x:longint);
var i,y:longint;
begin
    inc(w);q[w]:=x;
    i:=first[x];vis[x]:=true;
    while i>-1 do
        begin
            y:=edge[i];
            if (vis[y]=false) and f[x,y] then dfs(y);
            i:=next[i];
        end;
end;

begin
    assign(input,'travel.in');reset(input);
    assign(output,'travel.out');rewrite(output);
    readln(n,m);
    fillchar(first,sizeof(first),255);
    for i:=1 to m do
        begin
            readln(x[i],y[i]);
            f[x[i],y[i]]:=true;f[y[i],x[i]]:=true;
        end;
    for i:=1 to n do
        for j:=n downto 1 do
            if f[i,j] then
                add(i,j);
    if (m=n-1) then
        begin
            w:=0;
            dfs(1);
            for i:=1 to w do
                write(q[i],' ');
            writeln;
            close(input);close(output);
            halt;
        end;
    ans[1]:=maxlongint;
    for i:=1 to m do
        begin
            w:=0;
            f[x[i],y[i]]:=false;f[y[i],x[i]]:=false;
            for j:=1 to n do vis[j]:=false;
            dfs(1);
            if w=n then
                begin
                    flag:=false;
                    for j:=1 to n do
                        if q[j]<ans[j] then
                            begin
                                flag:=true;
                                break;
                            end else if q[j]>ans[j] then break;
                    if flag then
                        for j:=1 to n do ans[j]:=q[j];
                end;
            f[x[i],y[i]]:=true;f[y[i],x[i]]:=true;
        end;
    for i:=1 to n do write(ans[i],' ');
    writeln;
    close(input);close(output);
end.