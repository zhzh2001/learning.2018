const
    mo=1000000007;

var
    n,m,i,w:longint;ans:int64;
    q:array[0..1000001] of longint;
    a:array[0..9,0..1000001] of longint;

procedure get(x,y,v:longint);
begin
    if (x>n) then
        begin
            if (y=m) then
                begin
                    inc(w);q[w]:=v;
                end;
            exit;
        end;
    if y<m then get(x,y+1,v*2+a[x,y]);
    get(x+1,y,v*2+a[x,y]);
end;

procedure pd;
var i,j:longint;
begin
    w:=0;
    get(1,1,0);
    for i:=1 to w-1 do
        if q[i]>q[i+1] then exit;
    inc(ans);
    {for i:=1 to 3 do
        begin
            for j:=1 to 3 do
                write(a[i,j]);
            writeln;
        end;
    readln;    }
end;

procedure dfs(x,y:longint);
begin
    if (x>n) then
        begin
            pd;exit;
        end;
    if y=m then
        begin
            dfs(x+1,1);
            a[x,y]:=1;
            dfs(x+1,1);
            a[x,y]:=0;
            exit;
        end;
    if a[x-1,y+1]=0 then
        dfs(x,y+1);
    a[x,y]:=1;
    dfs(x,y+1);
    a[x,y]:=0;
end;

begin
    assign(input,'game.in');reset(input);
    assign(output,'game.out');rewrite(output);
    readln(n,m);
    if (n*m<=25) then
        begin
            dfs(1,1);
            writeln(ans);
            close(input);close(output);
            halt;
        end;
    if n=1 then
        begin
            ans:=1;
            for i:=1 to m do ans:=ans*2 mod mo;
            writeln(ans);
            close(input);close(output);
            halt;
        end;
    ans:=4;
    for i:=2 to m do
        ans:=ans*3 mod mo;
    writeln(ans);
    close(input);close(output);
end.
