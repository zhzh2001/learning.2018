#include <cstdio>
#include <algorithm>
using namespace std;
int n,m;
const int mod=1e9+7;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>m) swap(n,m);
	long long ans=1;
	for (int i=1;i<=n;i++)
		ans=ans*(i+1)%mod;
	for (int i=n+1;i<=m;i++) ans=ans*(n+1)%mod;
	for (int i=n+m;i>=m+2;i--) ans=ans*(n+m-i+2)%mod;
	printf("%lld\n",ans);
	return 0;
}
