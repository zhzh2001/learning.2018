#include <cstdio>
#include <vector>
#include <algorithm>
#include <cstring>
using namespace std;
vector<int> w[10010],W[10010];
int n,m,val[10010],cnt,Next[10010],head[10010],ans[10010],size,bel[10010],low[10010],dfn[10010],q[2][10010],vis[10010],Circle,First,stack[10010],top,tim,Head[5],tail[5],Cut,Size[10010],num;
bool Change,In_Circle;
void add(int a,int b)
{
	val[++cnt]=b;
	Next[cnt]=head[a];
	head[a]=cnt;
}
void dfs(int u,int fa)
{
	ans[++size]=u;
	for (int i=head[u];i;i=Next[i])
	{
		int v=val[i];
		if (v==fa) continue;
		w[u].push_back(v);
	}
	sort(w[u].begin(),w[u].end());
	for (int i=0;i<(int)w[u].size();i++) dfs(w[u][i],u);
}
void tarjan(int u,int fa)
{
	vis[u]=1;
	stack[++top]=u;
	low[u]=dfn[u]=++tim;
	for (int i=head[u];i;i=Next[i])
	{
		int v=val[i];
		if ((i^1)==fa) continue;
		if (!vis[v]) tarjan(v,i),low[u]=min(low[u],low[v]);
		else if (vis[v]==1) low[u]=min(low[u],dfn[v]);
	}
	if (low[u]==dfn[u])
	{
		Cut++;
		while (stack[top]!=u)
		{
			bel[stack[top]]=Cut;
			vis[stack[top]]=2;
			Size[Cut]++;
			top--;
		}
		bel[u]=Cut;
		vis[u]=2;
		Size[Cut]++;
		top--;
		if (Size[Cut]!=1) Circle=Cut;
	}
}
void dfs1(int u,int fa,int key)
{
	if (vis[u]) return;
	vis[u]=true;
	if (!In_Circle&&bel[u]==Circle)
	{
		First=u;
		In_Circle=true;
		int Key=0;
		for (int i=head[u];i;i=Next[i])
		{
			int v=val[i];
			if (v==fa) continue;
			if (bel[v]==Circle) dfs1(v,u,Key),Key++;
		}
	}
	else if (In_Circle)
	{
		q[key][++tail[key]]=u;
		for (int i=head[u];i;i=Next[i])
		{
			int v=val[i];
			if (v==fa) continue;
			if (bel[v]==Circle) dfs1(v,u,key);
		}
	}
	else
	{
		for (int i=head[u];i;i=Next[i])
		{
			int v=val[i];
			if (v==fa) continue;
			dfs1(v,u,0);
		}
	}
	vis[u]=false;
}
void dfs2(int u,int From,int key)
{
	if (vis[u]) return;
	vis[u]=true;
	if (key<=1) Head[key]++;
	ans[++size]=u;
	for (int i=head[u];i;i=Next[i])
	{
		int v=val[i];
		if (vis[v]) continue;
		w[u].push_back(v);
		if (bel[v]!=Circle) W[u].push_back(v);
	}
	sort(w[u].begin(),w[u].end());
	sort(W[u].begin(),W[u].end());
	W[u].push_back(1e9);
	int hd=0;
	if (bel[u]!=Circle)
	{
		for (int i=0;i<(int)w[u].size();i++)
		{
			if (w[u][i]==W[u][hd]) hd++;
			dfs2(w[u][i],W[u][hd],2);
		}
	}
	else if (First==u)
	{
		for (int i=0;i<(int)w[u].size();i++)
			if (w[u][i]==W[u][hd])
			{
				hd++;
				dfs2(w[u][i],W[u][hd],2);
			}
			else dfs2(w[u][i],W[u][hd],q[1][1]==w[u][i]),Change=true;
	}
	else
	{
		int Num=0;
		for (int i=0;i<(int)w[u].size();i++)
			if (bel[w[u][i]]!=Circle) Num++,num++;
		for (int i=0;i<(int)w[u].size();i++)
			if (bel[w[u][i]]==Circle)
			{
				int Tmp=W[u][hd];
				if (Tmp==1e9) Tmp=From;
				if (Num!=0)
				{
					dfs2(w[u][i],W[u][hd],key);
					continue;
				}
				int tmp=w[u][i];
				if (num==0&&!Change&&Head[key^1]<=tail[key^1]&&tmp>q[key^1][Head[key^1]]&&!vis[q[key^1][Head[key^1]]])
				{
					tmp=q[key^1][Head[key^1]];
				}
				if (tmp>From) tmp=From;
				if (tmp==w[u][i]) dfs2(w[u][i],Tmp,key);
				else if (tmp==From) return;
				else
				{
					Change=true;
					dfs2(tmp,1e9,key^1);
				}
			}
			else
			{
				hd++;Num--;num--;dfs2(w[u][i],W[u][hd],2);
			}
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	cnt=1;
	for (int i=1;i<=m;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);add(v,u);
	}
	if (m==n-1)
	{
		dfs(1,0);
		for (int i=1;i<=n;i++) printf("%d%c",ans[i],(i==n)?'\n':' ');
		return 0;
	}
	tarjan(1,0);
	memset(vis,0,sizeof(vis));
	dfs1(1,0,0);
	memset(vis,0,sizeof(vis));
	Head[0]=Head[1]=1;
	dfs2(1,1e9,2);
	for (int i=1;i<=n;i++) printf("%d%c",ans[i],(i==n)?'\n':' ');
	return 0;
}
