#include <cstdio>
#include <algorithm>
using namespace std;
int n,m,x1,y1,x2,y2,val[200010],cnt,Next[200010],head[200010],a[100010];
char str[20];
long long dp[100010][2];
void add(int a,int b)
{
	val[++cnt]=b;
	Next[cnt]=head[a];
	head[a]=cnt;
}
void dfs(int u,int fa)
{
	dp[u][0]=dp[u][1]=0;
	bool flag0=true,flag1=true;
	if (u==x1)
	{
		if (y1) flag0=false;
		else flag1=false;
	}
	if (u==x2)
	{
		if (y2) flag0=false;
		else flag1=false;
	}
	if (!flag0) dp[u][0]=-1e9;
	if (!flag1) dp[u][1]=-1e9;
	for (int i=head[u];i;i=Next[i])
	{
		int v=val[i];
		if (v==fa) continue;
		dfs(v,u);
		if (flag0)
		{
			if (dp[v][1]!=-1e9) dp[u][0]+=dp[v][1];
			else flag0=false,dp[u][0]=-1e9;
		}
		if (flag1)
		{
			if (dp[v][0]==-1e9&&dp[v][1]==-1e9)
			{
				flag1=false;
				dp[u][1]=-1e9;
			}
			else
			{
				long long tmp=(1LL<<50);
				if (dp[v][0]!=-1e9) tmp=dp[v][0];
				if (dp[v][1]!=-1e9) tmp=min(tmp,dp[v][1]);
				dp[u][1]+=tmp;
			}
		}
	}
	if (flag1) dp[u][1]+=a[u];
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%d%d%s",&n,&m,str);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<n;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		add(v,u);add(u,v);
	}
	if (n<=2000&&m<=2000)
	{
		while (m--)
		{
			scanf("%d%d%d%d",&x1,&y1,&x2,&y2);
			dfs(1,0);
			if (dp[1][0]==-1e9&&dp[1][1]==-1e9)
			{
				printf("-1\n");
			}
			else
			{
				long long ans=(1LL<<50);
				if (dp[1][0]!=-1e9) ans=dp[1][0];
				if (dp[1][1]!=-1e9) ans=min(ans,dp[1][1]);
				printf("%lld\n",ans);
			}
		}
	}
	return 0;
}
