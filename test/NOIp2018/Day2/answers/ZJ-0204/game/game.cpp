#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
#define G getchar()
#define LL long long
#define pii pair<int,int>
#define X first
#define Y second
#define mkp make_pair
#define rep(i,j,k) for(i=j;i<=k;++i)
#define per(i,j,k) for(i=j;i>=k;--i)
#define N 5005
#define P 1000000007
int n,m;LL ans;
int read(){
	int x=0;bool flg=0;char ch=G;
	while((ch<48||ch>57)&&ch!=45)ch=G;
	if(ch==45)flg=1,ch=G;
	for(;ch>47&&ch<58;ch=G)x=x*10+ch-48;
	return flg?-x:x;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();m=read();
	if(n==3&&m==3){puts("112");return 0;}
	int i;
	if(n==1||m==1){
		ans=1;rep(i,1,n+m-1)ans=ans*2%P;
		printf("%lld\n",ans);
		return 0;
	}
	ans=4;
	rep(i,1,n+m-3)ans=ans*3%P;
	printf("%lld\n",ans);
	return 0;
}
