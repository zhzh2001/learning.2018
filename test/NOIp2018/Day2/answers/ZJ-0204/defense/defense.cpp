#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
#define G getchar()
#define LL long long
#define pii pair<int,int>
#define X first
#define Y second
#define mkp make_pair
#define rep(i,j,k) for(i=j;i<=k;++i)
#define per(i,j,k) for(i=j;i>=k;--i)
#define N 100005
#define inf 10000000005LL
int n,m;LL w[N];char str[3];
int he[N],to[N],ne[N],tot;
LL f[2][N];
int read(){
	int x=0;bool flg=0;char ch=G;
	while((ch<48||ch>57)&&ch!=45)ch=G;
	if(ch==45)flg=1,ch=G;
	for(;ch>47&&ch<58;ch=G)x=x*10+ch-48;
	return flg?-x:x;
}
void add(int x,int y){
	to[++tot]=y;ne[tot]=he[x];he[x]=tot;
}
void DFS(int x,int e){
	int i,y;LL Min=inf;
	if(!he[x]){
		if(!f[1][x])f[1][x]=w[x];return;
	}
	for(i=he[x];i;i=ne[i])if(i!=e){
		DFS(y=to[i],i^1);
		f[0][x]+=f[1][y];
		f[1][x]+=min(f[0][y],f[1][y]);
	}
	f[0][x]=min(inf,f[0][x]);
	f[1][x]=min(inf,f[1][x]+w[x]);
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int i,x,y,a,b;
	scanf("%d%d%s",&n,&m,str);
	rep(i,1,n)w[i]=read();
	tot=1;rep(i,2,n){
		x=read();y=read();add(x,y);add(y,x);
	}
	while(m--){
		rep(i,1,n)f[0][i]=f[1][i]=0;
		a=read();x=read();b=read();y=read();
		for(i=he[a];i;i=ne[i])if(to[i]==b)break;
		if(i&&!x&&!y){puts("-1");continue;}
		if(!x)f[1][a]=inf;else f[0][a]=inf;
		if(!y)f[1][b]=inf;else f[0][b]=inf;
		DFS(1,0);
		printf("%lld\n",min(f[0][1],f[1][1]));
	}
	return 0;
}
