#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
#define G getchar()
#define LL long long
#define pii pair<int,int>
#define X first
#define Y second
#define mkp make_pair
#define rep(i,j,k) for(i=j;i<=k;++i)
#define per(i,j,k) for(i=j;i>=k;--i)
#define N 5005
int n,m;
vector<pii>he[N];int tot;
bool vis[N];
int tmp,ans[N],aa[N],ln;
int read(){
	int x=0;bool flg=0;char ch=G;
	while((ch<48||ch>57)&&ch!=45)ch=G;
	if(ch==45)flg=1,ch=G;
	for(;ch>47&&ch<58;ch=G)x=x*10+ch-48;
	return flg?-x:x;
}
void add(int x,int y){
	he[x].push_back(mkp(y,++tot));
}
void DFS1(int x){
	int y;
	vector<pii>::iterator ii;
	vis[x]=1;printf("%d ",x);
	for(ii=he[x].begin();ii!=he[x].end();++ii)if(!vis[y=ii->X]){
		DFS1(y);
	}
}
void DFS2(int x){
	int y;
	vector<pii>::iterator ii;
	vis[x]=1;aa[++ln]=x;
	for(ii=he[x].begin();ii!=he[x].end();++ii){
		if(!vis[y=ii->X]&&(ii->Y)>>1!=tmp){
			DFS2(y);
		}
	}
	
}
int cmp(){
	int i;
	rep(i,1,n)
		if(aa[i]<ans[i])return 1;
		else if(aa[i]>ans[i])return 0;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int i,x,y;
	n=read();m=read();tot=1;
	rep(i,1,m){
		x=read();y=read();add(x,y);add(y,x);
	}
	rep(i,1,n)sort(he[i].begin(),he[i].end());
	if(m==n-1){
		DFS1(1);puts("");return 0;
	}
	ans[1]=2;
	rep(tmp,1,n){
		memset(vis,0,sizeof vis);ln=0;DFS2(1);
		if(ln<n)continue;
		if(cmp()==1)memcpy(ans,aa,sizeof ans);
	}
	rep(i,1,n)printf("%d ",ans[i]);
	return 0;
}
