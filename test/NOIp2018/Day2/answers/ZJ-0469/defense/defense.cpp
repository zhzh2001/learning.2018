#include<cstdio>
#include<cstring>
#include<algorithm>
#define rint register int
using namespace std;
const int maxn=100010;
typedef long long ll;
const ll inf=1000000000000000ll;

struct Edge{
	int to,nex;
}edges[maxn<<1];
int head[maxn],cnt=0,n,m;
inline void addEdge(int u,int v){
	edges[++cnt].to=v;
	edges[cnt].nex=head[u];
	head[u]=cnt;
}
ll p[maxn],f[maxn][2][2];
bool ban[maxn][2];
void dfs(int u,int fa){
	int v;
	if(ban[u][0]) f[u][0][0]=f[u][0][1]=inf;
	else f[u][0][0]=f[u][0][1]=0;
	if(ban[u][1]) f[u][1][0]=f[u][1][1]=inf;
	else f[u][1][0]=f[u][1][1]=p[u];
	bool flag=true;
	for(rint i=head[u];i!=-1;i=edges[i].nex){
		v=edges[i].to;
		if(v==fa) continue;
		dfs(v,u);
		flag=false;
		if(!ban[u][0]){
			f[u][0][0]+=f[v][1][0];
			f[u][0][1]+=f[v][1][0];
		}
		if(!ban[u][1]){
			f[u][1][0]+=min(f[v][1][1],f[v][0][1]);
			f[u][1][1]+=min(f[v][1][1],f[v][0][1]);
		}
	}
	if(flag){
		f[u][0][0]=inf;
	}
//	printf("f[%d][0][0]=%lld\n",u,f[u][0][0]);
//	printf("f[%d][0][1]=%lld\n",u,f[u][0][1]);
//	printf("f[%d][1][0]=%lld\n",u,f[u][1][0]);
//	printf("f[%d][1][1]=%lld\n",u,f[u][1][1]);
}

int read(){
	int x=0;bool w=false;char c=0;
	while(c>'9'||c<'0') {w|=c=='-';c=getchar();}
	while(c>='0'&&c<='9') {x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	return w?-x:x;
}

ll lread(){
	ll x=0;bool w=false;char c=0;
	while(c>'9'||c<'0') {w|=c=='-';c=getchar();}
	while(c>='0'&&c<='9') {x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	return w?-x:x;
}
char typ[10];

void sp(){
	
}

int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	memset(head,-1,sizeof(head));
	n=read(),m=read();
	scanf("%s",typ+1);
	for(rint i=1;i<=n;i++) p[i]=lread();
	int u,v;
	for(rint i=1;i<n;i++){
		u=read(),v=read();
		addEdge(u,v);addEdge(v,u);
	}
	if(m>=10000){
		sp();
		return 0;
	}
	memset(ban,false,sizeof(ban));
	int a,x,b,y;
	for(rint i=1;i<=m;i++){
		a=read(),x=read(),b=read(),y=read();
		ban[a][1-x]=true;
		ban[b][1-y]=true;
		dfs(1,0);
		ll ans=min(f[1][0][0],f[1][1][0]);
		if(ans>=inf) ans=-1;
		printf("%lld\n",ans);
		ban[a][1-x]=false;
		ban[b][1-y]=false;
	}
	return 0;
}
