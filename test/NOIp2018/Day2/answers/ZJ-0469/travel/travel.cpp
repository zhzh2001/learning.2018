#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#define rint register int
using namespace std;
const int maxn=5170;
struct Edge{
	int to,nex;
}edges[maxn<<1];
int head[maxn],cnt=0,n,m;
inline void addEdge(int u,int v){
	edges[++cnt].to=v;
	edges[cnt].nex=head[u];
	head[u]=cnt;
}

int read(){
	int x=0;bool w=false;char c=0;
	while(c>'9'||c<'0') {w|=c=='-';c=getchar();}
	while(c>='0'&&c<='9') {x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	return w?-x:x;
}
int ut[maxn],vt[maxn],ind[maxn];
int sol[maxn],pt=0,tmp[maxn];
bool vis[maxn];
void dfs(int u,int fa,int ban){
	vis[u]=true;
	tmp[++pt]=u;
	int v;
	vector<int> vec;
	for(rint i=head[u];i!=-1;i=edges[i].nex){
		v=edges[i].to;
		if(vis[v]||(v==fa)) continue;
		if(((ut[ban]==u)&&(vt[ban]==v))||((ut[ban]==v)&&(vt[ban]==u))) continue;
		vec.push_back(v);
	}
	sort(vec.begin(),vec.end());
	int siz=vec.size();
	for(rint i=0;i<siz;i++){
		v=vec[i];
		dfs(v,u,ban);
	}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(head,-1,sizeof(head));
	n=read(),m=read();
	int u,v;
	for(rint i=1;i<=m;i++){
		u=read(),v=read();
		ut[i]=u,vt[i]=v;
		addEdge(u,v);addEdge(v,u);
		ind[u]++,ind[v]++;
	}
	if(n-1==m){
		dfs(1,0,0);
		for(rint i=1;i<=n;i++){
			printf("%d",tmp[i]);
			if(i<n) printf(" ");
		}
	}else{
		memset(sol,0x3f,sizeof(sol));
		for(rint i=1;i<=m;i++){
			if((ind[ut[i]]==1)||(ind[vt[i]]==1)) continue;
			memset(vis,false,sizeof(vis));
			pt=0;
			dfs(1,0,i);
			bool flag=false;
			for(rint i=1;i<=n;i++){
				if(tmp[i]>sol[i]) break;
				else if(tmp[i]<sol[i]){
					flag=true;
					break;
				}
			}
			if(flag) memcpy(sol,tmp,sizeof(tmp));
		}
		for(rint i=1;i<=n;i++){
			printf("%d",sol[i]);
			if(i<n) printf(" ");
		}
	}
	return 0;
}
