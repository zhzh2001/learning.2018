#include<cstdio>
#include<cstring>
#include<algorithm>
#define rint register int
using namespace std;
const int maxn=1000010;
typedef long long ll;
const ll mod=1e9+7;
int read(){
	int x=0;bool w=false;char c=0;
	while(c>'9'||c<'0') {w|=c=='-';c=getchar();}
	while(c>='0'&&c<='9') {x=(x<<1)+(x<<3)+(c^48);c=getchar();}
	return w?-x:x;
}
int n,m;
ll fsp(ll a,ll b){
	ll ret=1;
	while(b){
		if(b&1) ret=ret*a%mod;
		b>>=1;
		a=a*a%mod;
	}
	return ret;
}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read(),m=read();
	if(m==1){
		ll res=fsp(2ll,(ll)n);
		printf("%lld\n",res%mod);
		return 0;
	}
	if(n==1){
		ll res=fsp(2ll,(ll)m);
		printf("%lld",res%mod);
		return 0;
	}else if(n==2){
		ll res=fsp(3ll,(ll)m-1ll);
		res=res*4ll%mod;
		printf("%lld\n",res%mod);
		return 0;
	}else{
		if(n==3&&m==3) printf("112\n");
		else{
			
		}
	}
	return 0;
}
