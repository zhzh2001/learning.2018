#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

inline void Rd(int &res) {
	char c; res = 0;
	while (c = getchar(), c < '0');
	do {
		res = (res << 1) + (res << 3) + (c ^ 48);
	} while (c = getchar(), c >= '0');
}

const int M = 5005;

int n, m;
vector <int> G[M];

namespace Subtask1 {
	
	bool mark[M];
	int ans[M], sz;
	
	void dfs(int x, int f) {
		ans[++sz] = x;
		for (int i = 0; i < (int) G[x].size(); i++) {
			int to = G[x][i];
			if (to == f) continue;
			dfs(to, x);
		}
	}
	
	void solve() {
		for (int i = 1; i <= n; i++) sort(G[i].begin(), G[i].end());
		dfs(1, 0);
		for (int i = 1; i <= sz; i++) printf("%d%c", ans[i], " \n"[i == sz]);
	}
	
}

namespace Subtask2 {
	
	int val[M], sz, ans[M], ansz;
	
	void dfs(int x, int f) {
		if (x == 1) return;
		val[++sz] = x;
		for (int i = 0; i < (int) G[x].size(); i++) {
			int to = G[x][i];
			if (to == f) continue;
			dfs(to, x);
		}
	}
	
	void solve() {
		sort(G[1].begin(), G[1].end());
		val[++sz] = 1;
		dfs(G[1][0], 1);
		int l = 1;
		for (l = 1; l <= n; l++) {
			if (val[l] <= val[n]) ans[++ansz] = val[l];
			else break;
		}
		for (int i = n; i >= l; i--) ans[++ansz] = val[i];
		for (int i = 1; i <= n; i++) printf("%d%c", ans[i], " \n"[i == n]);
	}
	
}

int main() {
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	Rd(n), Rd(m);
	for (int i = 1; i <= m; i++) {
		int a, b;
		Rd(a), Rd(b);
		G[a].push_back(b);
		G[b].push_back(a);
	}
	if (m == n - 1) Subtask1::solve();
	else Subtask2::solve();
	return 0;
}
