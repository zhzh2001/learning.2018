#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long

using namespace std;

inline void Rd(int &res) {
	char c; res = 0;
	while (c = getchar(), c < '0');
	do {
		res = (res << 1) + (res << 3) + (c ^ 48);
	} while (c = getchar(), c >= '0');
}

const int M = (int) 1e5 + 5;
const ll INF = 1LL << 60;

int n, m, P[M], Head[M], tot;
struct Node {int to, nxt;} Edge[M << 1];
char type[5];

inline void Check(ll &a, ll b) {
	if (a < 0 || b < a) a = b;
}

inline void Addedge(int a, int b) {
	Edge[tot] = (Node) {b, Head[a]}; Head[a] = tot++;
	Edge[tot] = (Node) {a, Head[b]}; Head[b] = tot++;
}

ll chkmin(ll a, ll b) {
	if (a > b) swap(a, b);
	if (b < 0) return -INF;
	if (a < 0) return b;
	else return a;
}

namespace Subtask1 {
	
	bool must[M], ban[M];
	ll dp[2][M];
	
	void dfs(int x, int f) {
		dp[1][x] = P[x];
		for (int i = Head[x]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == f) continue;
			dfs(to, x);
			dp[0][x] += dp[1][to];
			dp[1][x] += chkmin(dp[0][to], dp[1][to]);
		}
		if (ban[x]) dp[1][x] = -INF;
		if (must[x]) dp[0][x] = -INF;
	}
	
	void solve() {
		while (m--) {
			int a, x, b, y;
			Rd(a), Rd(x), Rd(b), Rd(y);
			if (x) must[a] = true;
			else ban[a] = true;
			if (y) must[b] = true;
			else ban[b] = true;
			for (int i = 1; i <= n; i++) dp[0][i] = dp[1][i] = 0;
			dfs(1, 0);
			ll ans = chkmin(dp[0][1], dp[1][1]);
			printf("%lld\n", ans < 0 ? -1 : ans);
			if (x) must[a] = false;
			else ban[a] = false;
			if (y) must[b] = false;
			else ban[b] = false;
		}
	}
	
}

#define lson (p << 1)
#define rson (p << 1 | 1)

ll dp00[M << 2], dp01[M << 2], dp10[M << 2], dp11[M << 2];

namespace Subtask2 {	
	
	void up(int p) {
		dp00[p] = dp01[p] = dp10[p] = dp11[p] = -INF;
		if (dp00[lson] + dp10[rson] >= 0) Check(dp00[p], dp00[lson] + dp10[rson]);
		if (dp01[lson] + dp00[rson] >= 0) Check(dp00[p], dp01[lson] + dp00[rson]);
		if (dp01[lson] + dp10[rson] >= 0) Check(dp00[p], dp01[lson] + dp10[rson]);
		
		if (dp10[lson] + dp10[rson] >= 0) Check(dp10[p], dp10[lson] + dp10[rson]);
		if (dp11[lson] + dp00[rson] >= 0) Check(dp10[p], dp11[lson] + dp00[rson]);
		if (dp11[lson] + dp10[rson] >= 0) Check(dp10[p], dp11[lson] + dp10[rson]);
		
		if (dp00[lson] + dp11[rson] >= 0) Check(dp01[p], dp00[lson] + dp11[rson]);
		if (dp01[lson] + dp01[rson] >= 0) Check(dp01[p], dp01[lson] + dp01[rson]);
		if (dp01[lson] + dp11[rson] >= 0) Check(dp01[p], dp01[lson] + dp11[rson]);
		
		if (dp10[lson] + dp11[rson] >= 0) Check(dp11[p], dp10[lson] + dp11[rson]);
		if (dp11[lson] + dp01[rson] >= 0) Check(dp11[p], dp11[lson] + dp01[rson]);
		if (dp11[lson] + dp11[rson] >= 0) Check(dp11[p], dp11[lson] + dp11[rson]);
	}
	
	void build(int l, int r, int p) {
		dp00[p] = dp01[p] = dp10[p] = dp11[p] = -INF;
		if (l == r) {
			dp00[p] = 0, dp11[p] = P[l];
			return;
		}
		int mid = (l + r) >> 1;
		build(l, mid, lson);
		build(mid + 1, r, rson);
		up(p);
	}
	
	void update(int l, int r, int x, int ty, int p) {
		if (l == r) {
			dp00[p] = dp01[p] = dp10[p] = dp11[p] = -INF;
			if (ty == 1 || ty == -1) dp11[p] = P[l]; // must and recover
			if (ty == 0 || ty == -1) dp00[p] = 0; // ban and recover
			return;
		}
		int mid = (l + r) >> 1;
		if (x <= mid) update(l, mid, x, ty, lson);
		else update(mid + 1, r, x, ty, rson);
		up(p);
	}
	
	void solve() {
		build(1, n, 1);
		while (m--) {
			int a, x, b, y;
			Rd(a), Rd(x), Rd(b), Rd(y);
			update(1, n, a, x, 1);
			update(1, n, b, y, 1);
			ll ans = -INF;
			if (dp00[1] >= 0) Check(ans, dp00[1]);
			if (dp01[1] >= 0) Check(ans, dp01[1]);
			if (dp10[1] >= 0) Check(ans, dp10[1]);
			if (dp11[1] >= 0) Check(ans, dp11[1]);
			printf("%lld\n", ans < 0 ? -1 : ans);
			update(1, n, a, -1, 1);
			update(1, n, b, -1, 1);
		}
	}
	
}

namespace Subtask3 {
	
	int op[M];
	ll sumdp1[M], sumdp01[M];
	int id[M], dfscnt, top[M], fa[M], depth[M], sz[M], son[M], lst[M], rid[M];
	
	void up(int p) {
		dp00[p] = dp01[p] = dp10[p] = dp11[p] = -INF;
		if (dp00[lson] + dp10[rson] >= 0) Check(dp00[p], dp00[lson] + dp10[rson]);
		if (dp01[lson] + dp00[rson] >= 0) Check(dp00[p], dp01[lson] + dp00[rson]);
		if (dp01[lson] + dp10[rson] >= 0) Check(dp00[p], dp01[lson] + dp10[rson]);
		
		if (dp10[lson] + dp10[rson] >= 0) Check(dp10[p], dp10[lson] + dp10[rson]);
		if (dp11[lson] + dp00[rson] >= 0) Check(dp10[p], dp11[lson] + dp00[rson]);
		if (dp11[lson] + dp10[rson] >= 0) Check(dp10[p], dp11[lson] + dp10[rson]);
		
		if (dp00[lson] + dp11[rson] >= 0) Check(dp01[p], dp00[lson] + dp11[rson]);
		if (dp01[lson] + dp01[rson] >= 0) Check(dp01[p], dp01[lson] + dp01[rson]);
		if (dp01[lson] + dp11[rson] >= 0) Check(dp01[p], dp01[lson] + dp11[rson]);
		
		if (dp10[lson] + dp11[rson] >= 0) Check(dp11[p], dp10[lson] + dp11[rson]);
		if (dp11[lson] + dp01[rson] >= 0) Check(dp11[p], dp11[lson] + dp01[rson]);
		if (dp11[lson] + dp11[rson] >= 0) Check(dp11[p], dp11[lson] + dp11[rson]);
	}
	
	struct nnode {
		
		ll dp01, dp10, dp11, dp00;
		
	};
	
	nnode operator + (const nnode &A, const nnode &B) {
		nnode res;
		res.dp00 = res.dp01 = res.dp10 = res.dp11 = -INF;
		if (A.dp00 + B.dp10 >= 0) Check(res.dp00, A.dp00 + B.dp10);
		if (A.dp01 + B.dp00 >= 0) Check(res.dp00, A.dp01 + B.dp00);
		if (A.dp01 + B.dp10 >= 0) Check(res.dp00, A.dp01 + B.dp10);
		
		if (A.dp10 + B.dp10 >= 0) Check(res.dp10, A.dp10 + B.dp10);
		if (A.dp11 + B.dp00 >= 0) Check(res.dp10, A.dp11 + B.dp00);
		if (A.dp11 + B.dp10 >= 0) Check(res.dp10, A.dp11 + B.dp10);
		
		if (A.dp00 + B.dp11 >= 0) Check(res.dp01, A.dp00 + B.dp11);
		if (A.dp01 + B.dp01 >= 0) Check(res.dp01, A.dp01 + B.dp01);
		if (A.dp01 + B.dp11 >= 0) Check(res.dp01, A.dp01 + B.dp11);
		
		if (A.dp10 + B.dp11 >= 0) Check(res.dp11, A.dp10 + B.dp11);
		if (A.dp11 + B.dp01 >= 0) Check(res.dp11, A.dp11 + B.dp01);
		if (A.dp11 + B.dp11 >= 0) Check(res.dp11, A.dp11 + B.dp11);
		return res;
	}
	
	void dfs(int x, int f) {
		sz[x] = 1, son[x] = x, fa[x] = f;
		for (int i = Head[x]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == f) continue;
			depth[to] = depth[x] + 1;
			dfs(to, x);
			sz[x] += sz[to];
			if (son[x] == x || sz[to] > sz[son[x]]) son[x] = to;
		}
	}
	
	void rdfs(int x, int tp) {
		top[x] = tp, id[x] = ++dfscnt, lst[tp] = dfscnt, rid[dfscnt] = x;
		if (son[x] != x) rdfs(son[x], tp);
		for (int i = Head[x]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == fa[x] || to == son[x]) continue;
			rdfs(to, to);
		}
	}
	
	nnode query(int L, int R, int l, int r, int p) {
		if (L == l && r == R) return (nnode) {dp01[p], dp10[p], dp11[p], dp00[p]};
		int mid = (L + R) >> 1;
		if (r <= mid) return query(L, mid, l, r, lson);
		else if (l > mid) return query(mid + 1, R, l, r, rson);
		else return query(L, mid, l, mid, lson) + query(mid + 1, R, mid + 1, r, rson);
	}
	
	void modify(int l, int r, int x, int p) {
		if (l == r) {
			int pos = rid[x];
			dp00[p] = dp01[p] = dp10[p] = dp11[p] = -INF;
//			printf("l = %d r = %d pos = %d\n", l, r, pos);
			if (op[pos] != 1) dp00[p] = sumdp1[pos]; // not must
			if (op[pos] != 0) dp11[p] = P[pos] + sumdp01[pos]; // not ban
//			cout << dp00[p] << ' ' << dp01[p] << ' ' << dp10[p] << ' ' << dp11[p] << endl;
			return;
		}
		int mid = (l + r) >> 1;
		if (x <= mid) modify(l, mid, x, lson);
		else modify(mid + 1, r, x, rson);
		up(p);
//		printf("l = %d r = %d 00 = %lld 01 = %lld 10 = %lld 11 = %lld\n", l, r, dp00[p], dp01[p], dp10[p], dp11[p]);
	}
	
	void update(int l, int r, int x, int ty, int p) {
		if (l == r) {
			dp00[p] = dp01[p] = dp10[p] = dp11[p] = -INF;
			int pos = rid[x];
//			printf("pos = %d\n", pos);
			if (ty == 1 || ty == -1) dp11[p] = P[pos] + sumdp01[pos]; // must and recover
			if (ty == 0 || ty == -1) dp00[p] = sumdp1[pos]; // ban and recover
			return;
		}
		int mid = (l + r) >> 1;
		if (x <= mid) update(l, mid, x, ty, lson);
		else update(mid + 1, r, x, ty, rson);
		up(p);
	}
	
	void build(int x) {
		for (int i = Head[x]; ~i; i = Edge[i].nxt) {
			int to = Edge[i].to;
			if (to == fa[x]) continue;
			build(to);
			if (to != son[x]) {
				nnode tmp = query(1, n, id[to], lst[to], 1);
				sumdp1[x] += chkmin(tmp.dp11, tmp.dp10);
				sumdp01[x] += chkmin(chkmin(tmp.dp01, tmp.dp10), chkmin(tmp.dp00, tmp.dp11));
			}
		}
		modify(1, n, id[x], 1);
	}
	
	void upd(int x, int ty) {
		int start = x;
		while (1) {
//			printf("x = %d\n", x);
			int tp = top[x];
			nnode tmp = query(1, n, id[tp], lst[tp], 1);
			ll ori1 = chkmin(tmp.dp10, tmp.dp11);
			ll ori0 = chkmin(chkmin(tmp.dp01, tmp.dp10), chkmin(tmp.dp00, tmp.dp11));
//			printf("x = %d or1 = %lld ori0 = %lld\n", x, ori1, ori0);
//			puts("UPDATE!:");
			if (x == start) update(1, n, id[x], ty, 1);
			else modify(1, n, id[x], 1);
			tmp = query(1, n, id[tp], lst[tp], 1);
			ll now1 = chkmin(tmp.dp10, tmp.dp11);
//			cout << tmp.dp01 << ' ' << tmp.dp10 << ' ' << tmp.dp00 << ' ' << tmp.dp11 << endl;
			ll now0 = chkmin(chkmin(tmp.dp01, tmp.dp10), chkmin(tmp.dp00, tmp.dp11));
//			printf("x = %d now1 = %lld now0 = %lld\n", x, now1, now0);
			x = fa[top[x]];
//			printf("xx = %d\n", x);
			if (!x) break;
			sumdp1[x] += now1 - ori1;
			sumdp01[x] += now0 - ori0;
//			cout << "1 " << sumdp1[x] << ' ' << "0   " << sumdp01[x] << endl;
//			puts("");
		}
	}
	
	void solve() {
		dfs(1, 0);
		rdfs(1, 1);
//		for (int i = 1; i <= n; i++) printf("i = %d top = %d id = %d\n", i, top[i], id[i]);
		for (int i = 0; i < (M << 2); i++)
			dp00[i] = dp01[i] = dp10[i] = dp11[i] = -INF;
		memset(op, -1, sizeof(op));
		build(1);
//		for (int i = 1; i <= n; i++) printf("i = %d sumdp0 = %lld %lld\n", i, sumdp01[i], sumdp1[i]);
		
		while (m--) {
			int a, x, b, y;
			Rd(a), Rd(x), Rd(b), Rd(y);
			upd(a, x);
			op[a] = x;
			upd(b, y);
			op[b] = y;
//cout << id[1] << lst[1] << endl;
//			cout << query(1, n, id[1], lst[1], 0, 1) << ' ' << query(1, n, id[1], lst[1], 1, 1) << endl;
			nnode tmp = query(1, n, id[1], lst[1], 1);
			ll ans = chkmin(chkmin(tmp.dp01, tmp.dp10), chkmin(tmp.dp00, tmp.dp11));
			printf("%lld\n", ans < 0 ? -1 : ans);
			upd(a, -1);
			op[a] = -1;
			upd(b, -1);
			op[b] = -1;
		}
	}
	
}

int main() {
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	Rd(n), Rd(m);
	scanf("%s", type);
	memset(Head, -1, sizeof(Head));
	for (int i = 1; i <= n; i++) Rd(P[i]);
	for (int i = 1; i < n; i++) {
		int a, b;
		Rd(a), Rd(b);
		Addedge(a, b);
	}
	if (n <= 2000 && m <= 2000) Subtask1::solve();
	else if (type[0] == 'A') Subtask2::solve();
	else Subtask3::solve();
	return 0;
}

/*
5 6 A3
4 6 2 5 7
1 2
2 3
3 4
4 5
1 0 3 1
3 0 5 1
3 1 4 1
2 1 3 0
5 0 3 0
1 0 3 1

5 1 C3
2 4 1 3 9
1 5
5 2
5 3
3 4
2 1 3 1
*/
