#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>

#define ll long long

using namespace std;

inline void Rd(int &res) {
	char c; res = 0;
	while (c = getchar(), c < '0');
	do {
		res = (res << 1) + (res << 3) + (c ^ 48);
	} while (c = getchar(), c >= '0');
}

const int P = (int) 1e9 + 7;

int n, m;

namespace Subtask1 {
	
	int val[10][10], ans;
	vector <string> vec;
	
	void dfs(int x, int y, string str) {
		str += (val[x][y] + '0');
		if (x == n - 1 && y == m - 1) {
			vec.push_back(str);
			return;
		}
		if (y < m - 1) dfs(x, y + 1, str);
		if (x < n - 1) dfs(x + 1, y, str);
	}
	
	bool check() {
		vec.clear();
		dfs(0, 0, "");
		bool flag = true;
		for (int i = 0; i + 1 < (int) vec.size(); i++)
			flag &= (vec[i] <= vec[i + 1]);
		return flag;
	}
	
	void solve() {
		int t = (1 << (n * m));
		for (int st = 0; st < t; st++) {
			for (int i = 0; i < n; i++)
				for (int j = 0; j < m; j++) {
					int id = i * m + j;
					if (st >> id & 1) val[i][j] = 1;
					else val[i][j] = 0;
				}
			if (check()) ans++;
		}
		printf("%d\n", ans);
	}
	
}

int Pow(int x, int k) {
	int rs = 1;
	while (k) {
		if (k & 1) rs = (ll) rs * x % P;
		x = (ll) x * x % P;
		k >>= 1;
	}
	return rs;
}

namespace Subtask2 {
	
	void solve() {
		int ans = (ll) 4 * Pow(3, m - 1) % P;
		printf("%d\n", ans);
	}
	
}

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	Rd(n), Rd(m);
	if (n <= 3 && m <= 3) Subtask1::solve();
	else if (n == 2) Subtask2::solve();
	return 0;
}
