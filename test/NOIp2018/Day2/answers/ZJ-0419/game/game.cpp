#include<bits/stdc++.h>
using namespace std;

long long n,m,ans;

long long ksm(long long dishu,long long cishu)
{
    if (cishu==1) return dishu;
    if (cishu==0) return 1;
    long long qq;
    qq=(ksm(dishu,cishu/2)*ksm(dishu,cishu/2))%1000000007;
    if (cishu%2==1) qq=(qq*dishu)%1000000007;
    return qq;
}


int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	long long kk=min(n,m);
	long long kkk=max(m,n);
	n=kk;m=kkk;
	if (n==1) {
		ans=ksm(2,m);
	}
	if (n==2) {
		ans=ksm(3,m-1);
		ans=(ans*4)%1000000007;
	}
	if (n==3){
		if (m==1) ans=8;
		if (m==2) ans=36;
		if (m==3) ans=112;
	}
	if (n==5){
		 ans=7136;
	}
	cout<<ans;

}
