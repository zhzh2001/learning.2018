#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;
int pdd,j,wz,gs,s,t,pd,n,m,i,u,v,xb,xb2,xb3,bcc[100005],bccc[100005],stac[100005],ans[100005],sc2[100005],sc3[100005],lj[5005][5005],vv[100005],sc[100005],vv2[100005],vvv[100005],vv3[100005];
void dfs(int dq){
	vv[dq]=1;
	xb++;
	sc[xb]=dq;
	for (int i=1;i<=lj[dq][0];i++)
		if (vv[lj[dq][i]]==0){
			dfs(lj[dq][i]);
		}
}
void dfs3(int dq){
	vv2[dq]=1;
	xb2++;
	sc2[xb2]=dq;
	for (int i=1;i<=lj[dq][0];i++)
		if (vv2[lj[dq][i]]==0){
			if (((lj[dq][i]!=bcc[j])&&(lj[dq][i]!=bccc[j]))||((dq!=bcc[j])&&(dq!=bccc[j]))) dfs3(lj[dq][i]);
		}
}

void dfs2(int dq,int fa){
	wz++;
	stac[wz]=dq;
	vvv[dq]=1;
	for (int i=1;i<=lj[dq][0];i++){
		if ((vvv[lj[dq][i]]==1)&&(lj[dq][i]!=fa)&&(pdd==0)){
			pdd=1;
			gs++;
			bcc[gs]=dq;
			bccc[gs]=lj[dq][i];
			while (stac[wz]!=lj[dq][i]){
				gs++;
				bcc[gs]=stac[wz];
				bccc[gs]=stac[wz-1];
				wz--;
			}
			wz--;
		}
		if (vvv[lj[dq][i]]==0) dfs2(lj[dq][i],dq);
	}
	wz--;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		lj[u][0]++;
		lj[u][lj[u][0]]=v;
		lj[v][0]++;
		lj[v][lj[v][0]]=u;
	}
	for (i=1;i<=n;i++)
		sort(lj[i]+1,lj[i]+lj[i][0]+1);
	if (m==n){
		dfs2(1,1);
		for (i=1;i<=n;i++){
			ans[i]=n+1;
		}
		for (j=1;j<=gs;j++){
			for (i=1;i<=n;i++){
				sc2[i]=n+1;
				vv2[i]=0;
			}
			xb2=0;
			dfs3(1);
			pd=0;
			for (i=1;i<=n;i++)
				if (sc2[i]<ans[i]){
					pd=0;
					break;
				}
				else if (sc2[i]>ans[i]){
					pd=1;
					break;
				}
			if (pd==0)
				for (i=1;i<=n;i++)
					ans[i]=sc2[i];
				
		}
		for (i=1;i<=n;i++)
			printf("%d ",ans[i]);
	}
	else{
		dfs(1);
		for (i=1;i<=n;i++)
			printf("%d ",sc[i]);
	}
	return 0;
}
