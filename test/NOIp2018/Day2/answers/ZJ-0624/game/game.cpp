#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int rd(void){
	int res=0;char c;
	while(c=getchar(),c>'9'||c<'0');
	while(c>='0'&&c<='9') res=res*10+c-'0',c=getchar();
	return res;
}
const int N=10,mod=1E9+7;
int n,m,ans=0;
int a[N][N];
inline int ksm(int x,int y){return y==1?x:1ll*ksm(1ll*x*x%mod,y>>1)*((y&1)?x:1)%mod;}
inline int ck(int x,int y){
	if(x<=1||y<=1||y>=m) return 1;
	if(a[x-1][y]==a[x][y-1]&&a[x+1][y]!=a[x][y+1]) return 0;
	return 1;
}
inline void dfs(int x){
	if(x==n+m){
		ans+=2;
		return;
	}
	int lim=min(n+1,min(x,m+n+2-x));
	for(int k=1;k<=lim;k++){
		int tot=0,fl=1;
		for(int i=1;i<=n;i++){
			if(x-i>0&&x-i<=m){
				tot++;
				if(tot>=k) a[i][x-i]=1;
				else a[i][x-i]=0;
				if(!ck(i-1,x-i)){
					fl=0;
					break;
				}
			}
		}
		if(fl) dfs(x+1);
	}
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);	
	scanf("%d%d",&n,&m);
	if(n>m) swap(n,m);
	if(n==1){
		printf("%d\n",ksm(2,m));
		return 0;
	}else if(n==2){
		printf("%d\n",4ll*ksm(3,m-1)%mod);
		return 0;
	}else{
		dfs(2);
		printf("%d",ans);
	}
	return 0;
}
