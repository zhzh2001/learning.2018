#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int rd(void){
	int res=0;char c;
	while(c=getchar(),c>'9'||c<'0');
	while(c>='0'&&c<='9') res=res*10+c-'0',c=getchar();
	return res;
}
const int N=1E5+10;
const long long inf=100000000000000ll;
int n,m,sb;
int a[N];long long f[N][2];
int hd[N],nxt[N<<1],to[N<<1],cnt;
inline void init(int x,int y){
	cnt++;
	nxt[cnt]=hd[x];
	hd[x]=cnt;
	to[cnt]=y;
}
int gx,xt,gy,yt;
inline void dfs(int x,int pre){
	f[x][0]=0; f[x][1]=a[x];
	for(int i=hd[x];i;i=nxt[i]){
		int u=to[i];
		if(u!=pre){
			dfs(u,x);
			f[x][0]+=f[u][1];
			f[x][1]+=min(f[u][0],f[u][1]);
		}
	}
	if(x==gx){
		if(xt) f[x][0]=inf;
		else f[x][1]=inf;
	}
	if(x==gy){
		if(yt) f[x][0]=inf;
		else f[x][1]=inf;
	}
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=rd(),m=rd(),sb=rd();
	for(int i=1;i<=n;i++){
		scanf("%d",&a[i]);
	}
	for(int i=1;i<n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		init(x,y);
		init(y,x);
	}
//	dfs(1,1);
	while(m--){
		scanf("%d%d%d%d",&gx,&xt,&gy,&yt);
		memset(f,60,sizeof(f));
		dfs(1,1);
		long long ans=min(f[1][0],f[1][1]);
		if(ans>100000ll*n){
			puts("-1");
		}else printf("%lld\n",ans);
	}
	return 0;
}
