#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;
inline int rd(void){
	int res=0;char c;
	while(c=getchar(),c>'9'||c<'0');
	while(c>='0'&&c<='9') res=res*10+c-'0',c=getchar();
	return res;
}
const int N=5500;
int n,m;
vector<int>f[N];
int ans[N],tot=0,k[N];
int used[N],dep[N],fa[N];
int dx[N],dy[N];
int gx=0,gy=0;
inline void dfs(int x,int pre){
	ans[++tot]=x;
	int t=f[x].size();
	for(int i=0;i<t;i++){
		int u=f[x][i];
		if(u!=pre&&(x!=gx||u!=gy)&&(u!=gx||x!=gy)){
			dfs(u,x);
		}
	}
}
inline void find(int x,int pre){
	used[x]=1;dep[x]=dep[pre]+1,fa[x]=pre;
	int t=f[x].size();
	for(int i=0;i<t;i++){
		int u=f[x][i];
		if(u!=pre){
			if(used[u]){
				gx=x;gy=u;
			}else find(u,x);
		}
	}
}
inline void ck(void){
	if(!k[1]){
		for(int i=1;i<=n;i++){
			k[i]=ans[i];
		}
	}else{
		for(int i=1;i<=n;i++){
			if(k[i]!=ans[i]){
				if(k[i]<ans[i]) return;
				else break;
			}
		}
		for(int i=1;i<=n;i++){
			k[i]=ans[i];
		}
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		f[x].push_back(y);
		f[y].push_back(x);
	}
	for(int i=1;i<=n;i++){
		sort(f[i].begin(),f[i].end());
	}
	if(m==n-1){
		dfs(1,1);
		for(int i=1;i<=n;i++){
			printf("%d ",ans[i]);
		}
	}else{
		find(1,1);
		int sum=1;
		dx[1]=gx;
		dy[1]=gy;
		while(gx!=gy){
			if(dep[gx]<dep[gy]) swap(gx,gy);
			dx[++sum]=gx;
			gx=fa[gx];
			dy[sum]=gx;
		}
		for(int i=1;i<=sum;i++){
			gx=dx[i];
			gy=dy[i];
			tot=0;
			dfs(1,1);
			ck();
		}
		for(int i=1;i<=n;i++){
			printf("%d ",k[i]);
		}
	}
	return 0;
}
