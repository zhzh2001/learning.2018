//by Judge
#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int mod=1e9+7;
const int M=1e6+5;
int n,m; ll ans; bool f[11][M];
inline void solv1(){
	if(m==1) return (void)printf("4");
	ans=1;
	for(int i=2;i<=m;++i)
		ans*=12,ans%mod;
	printf("%lld\n",ans);
}
inline ll qpow(ll x,int p){
	ll res=1;
	while(p){
		if(p&1) res=res*x%mod;
		x=x*x%mod,p>>=1;
	} return res;
}
inline bool check(int x,int y){
	if(f[x][y]) return true;
	int dx=x-1,dy=y+1;
	if(dx<1||dy>m) return true;
	if(f[dx][dy]) return false;
	return true;
}
inline int get(int x,int y){
	return (n-x)*m+m-y;
}
inline void dfs(int x,int y){
	if(y>m) y=1,++x; if(x>n) return ;
	f[x][y]=1,dfs(x,y+1),f[x][y]=0;
	if(check(x,y)) dfs(x,y+1);
	else return (void)(ans=(ans-qpow(2ll,get(x,y))+mod)%mod);
}
inline void solv2(){
	if(n==1){
		printf("%lld\n",qpow(2ll,m));
		return ;
	} else if(n==3){
		if(m==1) printf("8\n");
		else if(m==2) printf("36\n");
		else if(m==3) printf("112\n");
		return ;
	}
	if(n==5&&m==5)
		return (void)printf("7136\n");
	ans=qpow(2,n*m),dfs(1,1);
	printf("%lld\n",ans);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	if(n==2){
		solv1();
		return 0;
	}
	solv2();
	return 0;
}
