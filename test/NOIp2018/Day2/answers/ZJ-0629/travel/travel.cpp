//by Judge
#include<bits/stdc++.h>
#define P pair<int,int>
using namespace std;
const int M=1e4+5;
#define getchar() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<21,stdin),p1==p2)?EOF:*p1++)
char buf[1<<21],*p1,*p2;
inline int read(){ int x=0,f=1; char c=getchar();
	for(;!isdigit(c);c=getchar()) if(c=='-') f=-1;
	for(;isdigit(c);c=getchar()) x=x*10+c-'0'; return x*f;
} int n,m,top,pat,vis[M],node;
vector<int> vec[M]; bool flag[M][M];
void dfs1(int u){
	vis[u]=1,printf("%d ",u);
	int mx=vec[u].size();
	for(int i=0;i<mx;++i)
		if(!vis[vec[u][i]])
			dfs1(vec[u][i]);
}
int end;
bool find_cir(int u,int fa){
	int mx=vec[u].size(); vis[u]=1;
	for(int i=0;i<mx;++i){ int v=vec[u][i];
		if(v==fa) continue;
		if(vis[v]){ end=v;
			return flag[u][v]=flag[v][u]=1;
		}
		if(find_cir(v,u)){
			flag[u][v]=flag[v][u]=1;
			if(end==u) return false;
			else return true;
		}
	} return false;
}
void dfs2(int u){
	vis[u]=1,printf("%d ",u);
	int mx=vec[u].size();
	for(int i=0;i<mx;++i){
		int v=vec[u][i];
		if(vis[v]) continue;
		if(flag[u][v]){
			if(!node){
				for(int j=i+1;j<mx;++j)
					if(flag[u][vec[u][j]])
						{ node=vec[u][j]; break; }
			} if(v<=node||vis[node]) dfs2(v);
		} else dfs2(v);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1,u,v;i<=m;++i){
		u=read(),v=read();
		vec[u].push_back(v);
		vec[v].push_back(u);
	}
	for(int i=1;i<=n;++i)
		sort(vec[i].begin(),vec[i].end());
	if(m==n-1){
		dfs1(1);
		printf("\n");
		return 0;
	} else{
		find_cir(1,0);
		memset(vis,0,sizeof(vis));
		dfs2(1),printf("\n");
		return 0;
	}
}
