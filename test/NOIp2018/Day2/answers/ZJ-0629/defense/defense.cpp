//by Judge
#include<bits/stdc++.h>
#define ll long long
using namespace std;
const ll inf=1e16;
const int M=1e5+5;
#define getchar() (p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<21,stdin),p1==p2)?EOF:*p1++)
char buf[1<<21],*p1,*p2;
inline int read(){ int x=0,f=1; char c=getchar();
	for(;!isdigit(c);c=getchar()) if(c=='-') f=-1;
	for(;isdigit(c);c=getchar()) x=x*10+c-'0'; return x*f;
} int n,m,a,b,x,y,pat,F[M],head[M]; ll P[M],f[M][3],dp[M][3];  // 1 self  2 son 
struct Edge{ int to,next;
	Edge(int v,int x):to(v),next(x){} Edge(){}
}e[M<<1];
inline void add(int u,int v){
	e[++pat]=Edge(v,head[u]),head[u]=pat;
	e[++pat]=Edge(u,head[v]),head[v]=pat;
}
inline void cmin(ll& a,ll b){
	if(a>b) a=b;
}
inline ll Min(ll a,ll b){
	return a<b?a:b;
}
#define v e[i].to
void prep(int u,int fa){
	for(int i=head[u];i;i=e[i].next)
		if(v^fa) F[v]=u,prep(v,u);
}
void dfs(int u){
	f[u][0]=0,f[u][1]=P[u];
	for(int i=head[u];i;i=e[i].next) if(v^F[u]){
		dfs(v),f[u][0]+=f[v][1];
		f[u][1]+=Min(f[v][0],f[v][1]);
	}
	if(u==a){
		if(x) f[u][0]=inf;
		else f[u][1]=inf;
	} else if(u==b){
		if(y) f[u][0]=inf;
		else f[u][1]=inf;
	}
}
#undef v
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),m=read();
	int type=read();
	for(int i=1;i<=n;++i)
		P[i]=read();
	for(int i=1,u,v;i<n;++i)
		u=read(),v=read(),add(u,v);
	prep(1,0);
	for(int i=1;i<=m;++i){
		a=read(),x=read(),b=read(),y=read();
		if((F[a]==b||F[b]==a)&&!x&&!y){
			printf("-1\n"); continue;
		} else if(a==b&&x!=y){
			printf("-1\n"); continue;
		} dfs(1);
		printf("%lld\n",Min(f[1][1],f[1][0]));
	} return 0;
}
