#include<cstdio>
#include<algorithm>
#define int long long
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0) x=-x,putchar('-');
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=1e5+5,INF=1e16;
struct edge{
	int link,next;
}e[N<<1];
bool mark[N];
int w[N],tot,head[N],dp[N][2],n,m,val[N];
char s[20];
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add_edge(u,v); add_edge(v,u);
}
inline void init(){
	n=read(); m=read(); scanf("%s",s+1);
	for (int i=1;i<=n;i++) w[i]=read();
	for (int i=1;i<n;i++){
		insert(read(),read());
	}
}
void dfs(int u,int fa){
	dp[u][0]=dp[u][1]=0;
	if (mark[u]){
		dp[u][val[u]^1]=INF;
	}
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u);
			dp[u][0]+=dp[v][1];
			dp[u][1]+=min(dp[v][1],dp[v][0]);
		}
	}
	dp[u][1]+=w[u];
}
inline void solve(){
//	dfs(1,0);
	for (int i=1;i<=m;i++){
		int x=read(),v1=read(),y=read(),v2=read();
		mark[x]=mark[y]=1; val[x]=v1; val[y]=v2;
		dfs(1,0);
		mark[x]=mark[y]=0;
		int ans=min(dp[1][0],dp[1][1]); val[x]=val[y]=0;
		if (ans>=INF) writeln(-1);
			else writeln(ans);
	}
}
signed main(){
	freopen("defense.in","r",stdin); freopen("defense.out","w",stdout);
	init(); solve();
	return 0;
}
