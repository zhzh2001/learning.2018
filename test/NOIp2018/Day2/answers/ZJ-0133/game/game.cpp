#include<cstdio>
#include<algorithm>
#define int long long
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0) x=-x,putchar('-');
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=20;
int n,m;

const int mod=1e9+7,M=1000005;
int ans,f[M];
inline void init(){
	n=read(); m=read();
}
int mat[N][N],pre;
int lowbit(int x){
	return x&(-x);
}
inline int get(int x){
	int cnt=0;
	for (;x;x-=lowbit(x)) cnt++;
	return cnt;
}
inline bool pd(){
	int t=0; pre=0;
	for (int i=0;i<(1<<(n+m-2));i++){
		int x=1,y=1;
		int cnt=get(i),sum=0;
		if (cnt!=m-1) continue; t++;
		for (int j=n+m-2;j;j--){
			if (i&(1<<j-1)){
				y++;
			}else{
				x++;
			}
			sum=sum*2+mat[x][y];
		}
		if (t>1&&sum>pre) return 0;
		pre=sum; 
	}
	return 1;
}
void dfs(int x,int y){
	if (x==n+1){
		if (pd()) {
	/*		for (int i=1;i<=3;i++){
				for (int j=1;j<=3;j++){
					write(mat[i][j]); putchar(' ');
				}
				puts("");
			}
			puts("");
			*/
			ans++;
		}
		return;
	}
	for (int i=0;i<2;i++){
		mat[x][y]=i;
		if (y==m) dfs(x+1,1);
			else dfs(x,y+1);
	}
}
inline void solve(){
	if (n>m) swap(n,m);
	if (n==1){
		ans=1;
		for (int i=1;i<=m;i++) ans=ans*2%mod;
	}
	if (n==2){
		ans=1;
		f[1]=4; f[2]=12;
		for (int i=3;i<=m;i++){
			f[i]=f[i-2]*9%mod;
		}
		ans=f[m];
	}
	if (n==3){
		f[1]=8; f[2]=36; f[3]=112;
		for (int i=4;i<=m;i++){
			f[i]=f[i-1]*3%mod;
		}
		ans=f[m];
	}
	if (n==4){
		f[1]=16; f[2]=108; f[3]=336; f[4]=912; f[5]=2688;
		for (int i=6;i<=m;i++){
			f[i]=f[i-1]*3%mod;
		}
		ans=f[m];
	}
	if (n>4){
		dfs(1,1);
	}
	writeln(ans);
}
signed main(){
	freopen("game.in","r",stdin); freopen("game.out","w",stdout);
	init(); solve();
	return 0;
}
