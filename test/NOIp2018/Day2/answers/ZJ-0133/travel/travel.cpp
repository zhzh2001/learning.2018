#include<cstdio>
#include<queue>
#include<vector>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0) x=-x,putchar('-');
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=5005;
struct edge{
	int link,next;
}e[N<<1];
int q[N],n,m,tot,head[N],mark[N<<1];
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add_edge(u,v); add_edge(v,u);
}
inline void init(){
	n=read(); m=read(); tot=1;
	for (int i=1;i<=m;i++){
		int u=read(),v=read();
		insert(u,v);
	}
}
priority_queue<int,vector<int>, greater<int> >heap[N];
void dfs(int u,int fa){
	q[++tot]=u;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (!mark[i]&&v!=fa){
			heap[u].push(v);
		}
	}
	while (!heap[u].empty()){
		int v=heap[u].top(); heap[u].pop();
		dfs(v,u);
	}
}
bool flag,vis[N],EX;
int ans[N],cir[N],pre,cnt;
void find_cir(int u,int fa){
	vis[u]=1;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			if (EX) return;
			if (vis[v]) {
				cir[++cnt]=i;
				pre=v; flag=1;
				return;
			}
			find_cir(v,u);
			if (flag) {
				cir[++cnt]=i;
				if (pre==u) flag=0,EX=1;
				return;
			}
		}
	}
}
inline bool cmp(){
	for (int i=1;i<=n;i++){
		if (q[i]!=ans[i]) return q[i]<ans[i];
	}
}
inline void solve(){
	if (m==n-1) {
		tot=0;
		dfs(1,0);
		for (int i=1;i<n;i++) write(q[i]),putchar(' ');
		writeln(q[n]);
	}else{
		find_cir(1,0);
		for (int i=1;i<=cnt;i++){
			mark[cir[i]]=mark[cir[i]^1]=1;
			tot=0;
			dfs(1,0);
			if (i==1||cmp()){
				for (int i=1;i<=tot;i++) ans[i]=q[i];
			}
			mark[cir[i]]=mark[cir[i]^1]=0;
		}
		for (int i=1;i<n;i++) write(ans[i]),putchar(' ');
		writeln(ans[n]);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	init(); solve();
	return 0;
}
