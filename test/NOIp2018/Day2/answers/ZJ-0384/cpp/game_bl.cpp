#include <cstdio>
int n,m,en;
int f[2][513];
bool b[513][513];
inline void init(){
	for (int i=0;i<en;i++)
		for (int j=0;j<en;j++){
			b[i][j]=true;
			for (int l=0;l<n-1;l++) if (((i>>(l+1))&1)<((j>>l)&1)) b[i][j]=false;
		}
}
int main(){
	freopen("game.in","r",stdin);
	//freopen("game2.out","w",stdout);
	scanf("%d%d",&n,&m);
	en=1<<n;init();
	int p=0;
	for (int i=0;i<en;i++) f[p][i]=1;
	for (int l=2;l<=m;l++){
		for (int i=0;i<en;i++)
			for (int j=0;j<en;j++) if (b[i][j]) f[p^1][j]=(f[p^1][j]+f[p][i])%1000000007;
		for (int i=0;i<en;i++) f[p][i]=0;
		p^=1;
	}
	int ans=0;
	for (int i=0;i<en;i++) ans=(ans+f[p][i])%1000000007;
	printf("%d\n",ans);
	return 0;
}
