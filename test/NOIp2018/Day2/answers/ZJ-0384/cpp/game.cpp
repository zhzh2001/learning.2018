#include <cstdio>
int n,m;
namespace do1{
	int f[2][513],en;
	bool b[513][513];
	inline void init(){
		for (int i=0;i<en;i++)
			for (int j=0;j<en;j++){
				b[i][j]=true;
				for (int l=0;l<n-1;l++) if (((i>>(l+1))&1)<((j>>l)&1)) b[i][j]=false;
			}
	}
	inline void doo(){
		en=1<<n;init();
		int p=0;
		for (int i=0;i<en;i++) f[p][i]=1;
		for (int l=2;l<=m;l++){
			for (int i=0;i<en;i++)
				for (int j=0;j<en;j++) if (b[i][j]) f[p^1][j]=(f[p^1][j]+f[p][i])%1000000007;
			for (int i=0;i<en;i++) f[p][i]=0;
			p^=1;
		}
		int ans=0;
		for (int i=0;i<en;i++) ans=(ans+f[p][i])%1000000007;
		printf("%d\n",ans);
	}
}
namespace do2{
	int f[2][65536+233],en;
	bool b[513][513];bool b2[33][33][33];
	inline void init(){
		for (int i=0;i<en;i++)
			for (int j=0;j<en;j++){
				b[i][j]=true;
				for (int l=0;l<n-1;l++) if (((i>>(l+1))&1)<((j>>l)&1)) b[i][j]=false;
			}
		for (int i=0;i<en;i++)
				for (int j=0;j<en;j++) if (b[i][j])
					for (int k=0;k<en;k++) if (b[j][k]){
						b2[i][j][k]=true;
						if ( (((j>>2)&1)!=((k>>1)&1)) && (((i>>1)&1)==((j)&1)) ) b2[i][j][k]=false;
					}
	}
	inline void doo(){
		en=1<<n;init();
		int p=0;
		for (int i=0;i<en;i++) for (int j=0;j<en;j++) if (b[i][j]) f[p][(i<<8)|j]=1;
		for (int l=3;l<=m;l++){
			for (int i=0;i<en;i++)
				for (int j=0;j<en;j++) if (b[i][j])
					for (int k=0;k<en;k++) if (b[j][k]){
						//bool pb=true;
						//for (int l=1;l<=n-2;l++) if ( (((j>>(l+1))&1)!=((k>>l)&1)) && (((i>>l)&1)==((j>>(l-1))&1)) ) pb=false;
						if (b2[i][j][k]) f[p^1][(j<<8)|k]=(f[p^1][(j<<8)|k]+f[p][(i<<8)|j])%1000000007;
					}
			for (int i=0;i<en;i++) for (int j=0;j<en;j++) f[p][(i<<8)|j]=0;
			p^=1;
		}
		int ans=0;
		for (int i=0;i<en;i++) for (int j=0;j<en;j++) ans=(ans+f[p][(i<<8)|j])%1000000007;
		printf("%d\n",ans);
	}
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==1 || n==2 || m==1 || m==2){
		do1::doo();
	}else{
		do2::doo();
	}
	return 0;
}
