#include <cstdio>
#include <vector>
#include <algorithm>
int n,m;
int x[10003],y[10003];
std::vector<int> e[10003];
int a[10003],ans[10003],cnt=0;
bool b[5003][5003];
void dfs(int x,int fa){
	a[++cnt]=x;if (cnt>n) return;
	for (int i=0;i<e[x].size();i++) if (e[x][i]!=fa && !b[x][e[x][i]]){
		dfs(e[x][i],x);if (cnt>n) return;
	}
}
inline bool pd(){
	for (int i=1;i<=n;i++){
		if (a[i]<ans[i]) return true;
		if (a[i]>ans[i]) return false;
	}
	return false;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d",&x[i],&y[i]);
		e[x[i]].push_back(y[i]);
		e[y[i]].push_back(x[i]);
	}
	for (int i=1;i<=n;i++) std::sort(e[i].begin(),e[i].end());
	if (m==n-1){
		cnt=0;dfs(1,0);
		for (int i=1;i<=n;i++) printf("%d ",a[i]);printf("\n");
	}else{
		for (int i=1;i<=n;i++) ans[i]=2333333;
		for (int i=1;i<=m;i++){
			b[x[i]][y[i]]=b[y[i]][x[i]]=true;
			cnt=0;dfs(1,0);
			b[x[i]][y[i]]=b[y[i]][x[i]]=false;
			if (cnt==n){
				if (pd()) {
					for (int i=1;i<=n;i++) ans[i]=a[i];
				}
			}
		}
		for (int i=1;i<=n;i++) printf("%d ",ans[i]);printf("\n");
	}
	return 0;
}
