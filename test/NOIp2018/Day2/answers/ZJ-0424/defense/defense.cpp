#include <bits/stdc++.h>
#define int long long
using namespace std;
const int maxn = 100010;
const int INF = (int)1e14;
struct node
{
	int ne,to;
}e[maxn << 1];
int n,m,A,X,B,Y,he[maxn],nume;
int a[maxn],dp[maxn][2],opt[maxn];
char ch[3];
inline void addside(int u,int v)
{
	e[++nume].ne = he[u];
	e[nume].to = v;
	he[u] = nume;
}
inline void dfs(int u,int pre)
{
	int res0 = 0, res1 = 0;
	for (int i = he[u];i; i = e[i].ne)
	{
		int v = e[i].to;
		if (v==pre) continue;
		dfs(v,u);
		res0 += dp[v][1]; res1 += min(dp[v][0],dp[v][1]);
		if (dp[v][0] < dp[v][1]) opt[v] = 0; else opt[v] = 1;
	}
	dp[u][0] = res0; dp[u][1] = res1 + a[u];
	if (A == u)
	{
		if (X==0) dp[u][1] = INF; else dp[u][0] = INF;
	}
	else if (B==u)
	{
		if (Y==0) dp[u][1] = INF; else dp[u][0] = INF;
	}
}
inline void dfs2(int u,int pre)
{
	
}
signed main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	char c = 0;
	while (!isalpha(c)) c = getchar();
	while (!isdigit(c)) c = getchar();
	for (int i = 1; i <= n; ++i) scanf("%lld",&a[i]);
	for (int i = 1; i < n; ++i)
	{
		int u,v; scanf("%lld%lld",&u,&v);
		addside(v,u); addside(u,v);
	}
	if (n <= 2000)
	{
		for (int i = 1; i <= m; ++i)
		{
			//int A,X,B,Y;
			scanf("%lld%lld%lld%lld",&A,&X,&B,&Y);	memset(dp,0,sizeof(dp));
			dfs(1,0);
			int ans = min(dp[1][0],dp[1][1]);
			if (ans >= INF) puts("-1"); else printf("%lld\n",ans);
		//	printf("%lld\n",(min(dp[1][0],dp[1][1])>=INF)?;
			//addside()
		}
	}
	else
	{
		dfs(1,0);
		for (int i = 1; i <= m; ++i)
		{
			scanf("%lld%lld%lld%lld",&A,&X,&B,&Y);
			if (opt[B]==Y) printf("%lld\n",dp[1][1]);
			else printf("%lld\n",dp[1][1] - dp[1][Y^1]+dp[1][Y]);
		}
	}
}
