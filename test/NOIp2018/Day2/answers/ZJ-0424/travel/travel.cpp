#include <bits/stdc++.h>
#define PB push_back
using namespace std;
const int maxn = 5010;
vector<int> G[maxn];
int n,m;
struct node
{
	int ne,to,dis;
}e[maxn << 1];
int he[maxn],nume;
inline void addside(int u,int v)
{
	e[++nume].ne = he[u];
	e[nume].to = v;
	he[u] = nume;
}
inline int read()
{
	int x = 0, f = 1;
	char ch = 0;
	for (;!isdigit(ch); ch = getchar()) if (ch == '-') f = -1;
	for (;isdigit(ch); ch = getchar()) x = x * 10 + ch - 48;
	return x * f;
}
int stk[maxn],nums,huan[maxn],numh,numt,dfn[maxn];
bool vis[maxn],pd;
int del1, del2;
inline void dfs(int u,int pre)
{
	if (pd) return;
	stk[++nums] = u; vis[u] = 1;
	for (int i = he[u];i&&!pd; i = e[i].ne)
	{
		int v = e[i].to;
		if (v==pre) continue;
		if (vis[v]&&!pd)
		{
			huan[++numh] = v; 
			//for (int i = 1; i <= nums; ++i) cerr<<stk[i]<<' ';cerr<<v<<endl;
			while (stk[nums] != v) huan[++numh] = stk[nums], nums--;
			pd = 1;
			return;
		}
		dfs(v,u);
	}
	 nums--;
}
inline void dfs2(int u,int pre)
{
	dfn[++numt] = u;
	sort(G[u].begin(), G[u].end());
	for (int i = 0; i < G[u].size(); ++i)
	{
		if ((u == del1&&G[u][i]==del2)||(u==del2&&G[u][i]==del1)) continue;
		if (G[u][i] == pre) continue;
		dfs2(G[u][i],u);
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n = read(); m = read();
	pd = 0;
	for (int i = 1;i <= m; ++i)
	{
		int u = read(), v = read();
		addside(u,v); addside(v,u);
		G[u].PB(v); G[v].PB(u);
	}
	if (n == m)
	{
		dfs(1,0);
		int L = 2, R = numh;
	//	for (int i = 1; i <= numh; ++i) cerr<<huan[i]<<' ';cerr<<endl;
		if (huan[L] < huan[R])
		{
			while (huan[L] < huan[R]) L++;
			del1 = huan[L]; del2 = huan[L-1];
		}
		else
		{
			while (huan[R] < huan[L]) R--;
			del1 = huan[R]; del2 = huan[R+1];
		}
	}
//	cerr<<del1<<'!'<<del2<<endl;
	dfs2(1,0);
	for (int i = 1; i <= n; ++i) printf("%d ",dfn[i]);
	fclose(stdin);
	fclose(stdout);
}
