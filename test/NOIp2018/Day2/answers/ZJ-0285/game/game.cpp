#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;

const int P=1e9+7;

inline int Pow(int x,int y){
  int ret=1;
  for(;y;y>>=1,x=1LL*x*x%P) if(y&1) ret=1LL*ret*x%P;
  return ret; 
}

inline int calc(int n,int m){
  if(n>m) swap(n,m);
  if(n==1) return 2LL*Pow(3,m-1)%P;
  return 1LL*Pow(4,n-1)*Pow(3,m-n)%P*Pow(2,n-1)%P;
}

int main(){
  freopen("game.in","r",stdin);
  freopen("game.out","w",stdout);
  int n,m; scanf("%d%d",&n,&m);
  if(n==1 || m==1) return printf("%d\n",Pow(2,n+m-1)),0;
  if(n==2 || m==2) return printf("%lld\n",4LL*Pow(3,n+m-3)%P),0;
  int ans=0;
  for(int i=2;i<=m;i++){
    int cur;
    cur=1LL*Pow(4,min(n-i,m-i))*Pow(3,min(abs(n-m),m-i))%P*Pow(2,n-1)%P;
    cur=1LL*cur*(min(i,min(n,m)))%P;
    ans=(ans+cur)%P;
  }
  if(m<n) ans=(ans+4LL*Pow(3,n-m-1)*Pow(2,m-1))%P;
  else ans=(ans+3LL*Pow(2,m-2))%P;
  swap(n,m);
  for(int i=4;i<=m;i++){
    int cur;
    cur=1LL*Pow(4,min(n-i,m-i))*Pow(3,min(abs(n-m),m-i))%P*Pow(2,n-1)%P;
    cur=1LL*cur*(min(i,min(n,m)))%P;
    ans=(ans+cur)%P;
  }
  if(m<n) ans=(ans+4LL*Pow(3,n-m-1)*Pow(2,m-1))%P;
  else ans=(ans+3LL*Pow(2,m-2))%P;
  ans=2LL*ans%P;
  printf("%d\n",ans);
  return 0;
}
