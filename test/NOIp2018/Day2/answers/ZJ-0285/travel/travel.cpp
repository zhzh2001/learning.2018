#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

const int N=10010;

inline char nc(){
  static char buf[100000],*p1=buf,*p2=buf;
  return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}

inline void read(int &x){
  char c=nc(); x=0;
  for(;c>'9'||c<'0';c=nc());for(;c>='0'&&c<='9';x=x*10+c-'0',c=nc());
}

int n,m,cnt,G[N],u[N],v[N],u1[N],v1[N],m1,ans[N],cur[N],Q[N],vis[N],t,anst,curt;
struct edge{
  int t,nx;
}E[N<<1];

inline void addedge(int x,int y){
  E[++cnt].t=y; E[cnt].nx=G[x]; G[x]=cnt;
}

inline bool btr(){
  if(curt!=n) return false;
  if(!anst) return true;
  for(int i=1;i<=n;i++)
    if(ans[i]!=cur[i]) return ans[i]>cur[i];
}

void dfs(int x,int f){
  cur[++curt]=x; vis[x]=1;
  for(int i=G[x];i;i=E[i].nx)
    if(E[i].t!=f && !vis[E[i].t]) dfs(E[i].t,x);
}

int main(){
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  read(n); read(m);
  for(int i=1;i<=m;i++) read(u[i]),read(v[i]);
  for(int i=1;i<=n;i++){
    t=0;
    for(int j=1;j<=m;j++){
      if(u[j]==i) Q[++t]=v[j];
      if(v[j]==i) Q[++t]=u[j];
    }
    sort(Q+1,Q+1+t);
    for(int j=t;j;j--){
      ++m1; u1[m1]=i; v1[m1]=Q[j];
    }
  }
  if(m==n-1){
    for(int i=1;i<=m1;i++) addedge(u1[i],v1[i]);
    dfs(1,0);
    for(int i=1;i<=n;i++) printf("%d ",cur[i]);
    return 0;
  }
  for(int k=1;k<=m;k++){
    cnt=curt=0;
    for(int i=1;i<=n;i++) G[i]=vis[i]=0;
    for(int i=1;i<=m1;i++){
      if(u1[i]==u[k] && v1[i]==v[k]) continue;
      if(u1[i]==v[k] && v1[i]==u[k]) continue;
      addedge(u1[i],v1[i]);
    }
    dfs(1,0);
    if(btr()){
      anst=curt; for(int i=1;i<=curt;i++) ans[i]=cur[i];
    }
  }
  for(int i=1;i<=n;i++) printf("%d ",ans[i]);
  return 0;
}
