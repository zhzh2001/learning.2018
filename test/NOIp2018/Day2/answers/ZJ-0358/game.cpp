#include <cstdio>
#include <algorithm>
#define ll long long
using namespace std;
const int Mod=1e9+7;
int n,m;
int f[1000005];
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n>m)swap(n,m);
	if(n==1){
		f[1]=2;
		for(int i=2;i<=m;i++)
			f[i]=(f[i-1]*2)%Mod;
		printf("%d\n",f[m]);
		return 0;
	}
	if(n==2){
		f[2]=12;
		for(int i=3;i<=m;i++)
			f[i]=((ll)f[i-1]*3)%Mod;
		printf("%d\n",f[m]);
		return 0;
	}
	if(n==3){
		f[3]=112;
		for(int i=4;i<=m;i++)
			f[i]=((ll)f[i-1]*3)%Mod;
		printf("%d\n",f[m]);
		return 0;
	}
	if(n==4){
		f[4]=912;f[5]=2688;
		for(int i=6;i<=m;i++)
			f[i]=((ll)f[i-1]*3)%Mod;
		printf("%d\n",f[m]);
		return 0;
	}
	if(n==5){
		if(m==5){
			puts("7136");
			return 0;
		}
	}
	return 0;
}
