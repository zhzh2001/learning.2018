#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#define pa pair<int,int>
using namespace std;
const int N=5005;
int n,m,top;
int a[N],Ans[N];
bool mark[N],vis[N];
vector<pa> vec[N];
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
struct edge{
	int to,next;
}e[N+N];
int cnt_edge,last[N];
inline void add_edge(int u,int v){
	e[++cnt_edge]=(edge){v,last[u]};last[u]=cnt_edge;
}
void check(){
	for(int i=1;i<=n;i++)
		if(Ans[i]>a[i]){
			memcpy(Ans,a,sizeof(a));
			return;
		}else if(Ans[i]<a[i])return;
}
void dfs(int u){
	a[++top]=u;vis[u]=1;
	for(int i=0;i<(int)vec[u].size();i++){
		pa v=vec[u][i];
		if(vis[v.first] || mark[v.second])continue;
		dfs(v.first);
	}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++){
		int u=read(),v=read();
		add_edge(u,v);
		add_edge(v,u);
	}
	for(int u=1;u<=n;u++){
		for(int i=last[u];i;i=e[i].next){
			int v=e[i].to;
			vec[u].push_back(make_pair(v,i));
		}
		sort(vec[u].begin(),vec[u].end());
	}
	for(int i=1;i<=n;i++)
		Ans[i]=n+1;
	if(m==n){
		for(int i=1;i<=m;i++){
			memset(vis,0,sizeof(vis));
			mark[i*2-1]=mark[i*2]=1;
			top=0;dfs(1);
			if(top==n)check();
			mark[i*2-1]=mark[i*2]=0;
		}
	}else{
		memset(vis,0,sizeof(vis));
		top=0;dfs(1);
		check();
	}
	for(int i=1;i<=n;i++)
		printf("%d%c",Ans[i],(i==n)?'\n':' ');
	return 0;
}
