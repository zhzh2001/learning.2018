#include <cstdio>
#include <cstring>
#include <algorithm>
#define ll long long
using namespace std;
const int N=1e5+5;
const ll inf=1e10;
int n,Q;
int U[N],V[N],w[N];
ll Ans,f[3][N],g[3][N];
char type[5];
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
struct edge{
	int to,next;
}e[N+N];
int cnt_edge,last[N];
inline void add_edge(int u,int v){
	e[++cnt_edge]=(edge){v,last[u]};last[u]=cnt_edge;
}
int col[N];
bool check(){
	for(int i=1;i<n;i++){
		if(col[U[i]] | col[V[i]])continue;
		return 0;
	}
	return 1;
}
void dfs(int u,ll val){
	if(val>=Ans)return;
	if(u>n){
		if(check())Ans=val;
		return;
	}
	if(col[u]>=0){
		dfs(u+1,val+((col[u]==1)?w[u]:0));
		return;
	}
	col[u]=1;
	dfs(u+1,val+w[u]);
	col[u]=0;
	dfs(u+1,val);
	col[u]=-1;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(),Q=read();scanf("%s",type+1);
	for(int i=1;i<=n;i++)w[i]=read();
	for(int i=1;i<n;i++){
		U[i]=read(),V[i]=read();
		add_edge(U[i],V[i]);
		add_edge(V[i],U[i]);
	}
	
	if(n<=10 && Q<=10){
		while(Q--){
			Ans=inf;
			memset(col,-1,sizeof(col));
			int a=read(),x=read(),b=read(),y=read();
			col[a]=x,col[b]=y;
			dfs(1,0);
			if(Ans==inf)puts("-1");
			else printf("%lld\n",Ans);
		}
		return 0;
	}
	
	if(type[1]=='A'){
		if(type[2]=='1'){
			puts("!");
			for(int i=0;i<=2;i++)
				for(int j=1;j<=n;j++)
					f[i][j]=inf;
			f[1][0]=inf;
			for(int i=1;i<=n;i++){
				f[0][i]=f[1][i-1];
				f[1][i]=min(f[0][i-1],f[1][i-1])+w[i];
				if(i==1)
					f[0][i]=inf;
			}
			g[1][n+1]=inf;
			for(int i=0;i<=2;i++)
				for(int j=1;j<=n;j++)
					g[i][j]=inf;
			for(int i=n;i>=1;i--){
				g[0][i]=g[1][i+1];
				g[1][i]=min(g[0][i+1],g[1][i+1])+w[i];
				if(i==1)
					g[0][i]=inf;
			}
			while(Q--){
				int a=read(),x=read(),b=read(),y=read();
				if(y==0){
					Ans=min(f[1][y-1]+min(g[0][y+1],g[1][y+1]),f[0][y-1]+g[1][y+1]);
					if(Ans>=inf)puts("-1");
					else printf("%lld\n",Ans);
				}else{
					Ans=min(f[0][y-1],f[1][y-1])+min(g[0][y+1],g[1][y+1])+w[y];
					if(Ans>=inf)puts("-1");
					else printf("%lld\n",Ans);
				}
			}
			return 0;
		}
		while(Q--){
			int a=read(),x=read(),b=read(),y=read();
			for(int i=0;i<=2;i++)
				for(int j=1;j<=n;j++)
					f[i][j]=inf;
			f[1][0]=inf;
			for(int i=1;i<=n;i++){
				f[0][i]=f[1][i-1];
				f[1][i]=min(f[0][i-1],f[1][i-1])+w[i];
				if(i==a){
					if(x==0)f[1][i]=inf;
					else f[0][i]=inf;
				}
				if(i==b){
					if(y==0)f[1][i]=inf;
					else f[0][i]=inf;
				}
			}
			Ans=min(f[1][n],f[0][n]);
			if(Ans>=inf)puts("-1");
			else printf("%lld\n",min(f[1][n],f[0][n]));
		}
		return 0;
	}
	return 0;
}
