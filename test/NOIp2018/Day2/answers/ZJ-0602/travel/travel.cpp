#include<iostream>
#include<cstdio>
#include<queue>

using namespace std;

const int maxn=5005;

int n,m,G[maxn][maxn];

bool visited[maxn];

struct node
{
	int index;
	friend bool operator < (node x,node y)
	{
		return x.index>y.index;
	}
};

priority_queue <node> q;

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	cin>>n>>m;
	for(int i=0;i<m;i++)
	{
		int u,v;
		cin>>u>>v;
		G[u][v]=1;
		G[v][u]=1;
	}
	node t;
	t.index=1;
	q.push(t);
	visited[1]=1;
	while(!q.empty())
	{
		node tmp=q.top();
		cout<<tmp.index<<" ";
		q.pop();
		for(int i=1;i<=n;i++)
		{
			if(G[tmp.index][i]!=0&&!visited[i])
			{
				node nn;
				nn.index=i;
				q.push(nn);
				visited[i]=1;
			}
		}
	}
	return 0;
}
