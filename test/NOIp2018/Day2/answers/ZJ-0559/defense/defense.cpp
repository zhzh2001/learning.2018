#include<cstdio>
#include<cctype>
#include<vector>
#include<cstring>
#include<climits>
inline int getint() {
	register char ch;
	while(!isdigit(ch=getchar()));
	register int x=ch^'0';
	while(isdigit(ch=getchar())) x=(((x<<2)+x)<<1)+(ch^'0');
	return x;
}
inline char getalpha() {
	register char ch;
	while(!isalpha(ch=getchar()));
	return ch;
}
typedef long long int64;
const int N=1e5+1;
int p[N],mark[N],par[N];
int64 f[N][3],sib[N][2];
std::vector<int> e[N];
inline void add_edge(const int &u,const int &v) {
	e[u].push_back(v);
	e[v].push_back(u);
}
inline void upd(int64 &a,const int64 &b) {
	if(a==LLONG_MAX) return;
	if(b==LLONG_MAX) {
		a=LLONG_MAX;
		return;
	}
	a+=b;
}
void dfs(const int &x,const int &par) {
	::par[x]=par;
	f[x][0]=0;
	f[x][1]=p[x];
	f[x][2]=LLONG_MAX;
	int64 tmp0=0,tmp1=0;
	for(unsigned i=0;i<e[x].size();i++) {
		const int &y=e[x][i];
		if(y==par) continue;
		sib[y][0]=tmp0;
		sib[y][1]=tmp1;
		dfs(y,x);
		upd(tmp0,std::min(f[y][0],f[y][1]));
		upd(tmp1,f[y][1]);
		upd(f[x][0],f[y][1]);
		upd(f[x][1],std::min(f[y][0],f[y][1]));
	}
	tmp0=tmp1=0;
	for(register unsigned i=e[x].size()-1;~i;i--) {
		const int &y=e[x][i];
		if(y==par) continue;
		upd(sib[y][0],tmp0);
		upd(sib[y][1],tmp1);
		upd(tmp0,std::min(f[y][0],f[y][1]));
		upd(tmp1,f[y][1]);
	}
	if(mark[x]==0) f[x][1]=LLONG_MAX;
	if(mark[x]==1) f[x][0]=LLONG_MAX;
}
void solve(int x) {
	std::swap(f[x][!mark[x]],f[x][2]);
	while(x!=1) {
		f[par[x]][0]=sib[x][1];
		upd(f[par[x]][0],f[x][1]);
		f[par[x]][1]=p[par[x]];
		upd(f[par[x]][1],sib[x][0]);
		upd(f[par[x]][1],std::min(f[x][0],f[x][1]));
		x=par[x];
		if(mark[x]!=-1) f[x][!mark[x]]=LLONG_MAX;
	}
}
int main() {
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	const int n=getint(),m=getint();
	const char t1=getalpha();
	const int t2=getint();
	for(register int i=1;i<=n;i++) {
		p[i]=getint();
	}
	for(register int i=1;i<n;i++) {
		add_edge(getint(),getint());
	}
	memset(mark,-1,sizeof mark);
	if(n<=2000&&m<=2000) {
		for(register int i=0;i<m;i++) {
			const int a=getint();
			mark[a]=getint();
			const int b=getint();
			mark[b]=getint();
			dfs(1,0);
			mark[a]=mark[b]=-1;
			const int64 ans=std::min(f[1][0],f[1][1]);
			printf("%lld\n",ans!=LLONG_MAX?ans:-1);
		}
		return 0;
	}
	dfs(1,0);
	if(t1=='B') {
		for(register int i=0;i<m;i++) {
			const int a=getint();
			mark[a]=getint();
			const int b=getint();
			mark[b]=getint();
			solve(a);
			solve(b);
			const int64 ans=std::min(f[1][0],f[1][1]);
			printf("%lld\n",ans!=LLONG_MAX?ans:-1);
			solve(a);
			solve(b);
			mark[a]=mark[b]=-1;
		}
		return 0;
	}
	return 0;
}
