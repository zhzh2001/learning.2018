#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
inline int getint() {
	register char ch;
	while(!isdigit(ch=getchar()));
	register int x=ch^'0';
	while(isdigit(ch=getchar())) x=(((x<<2)+x)<<1)+(ch^'0');
	return x;
}
const int N=8,mod=1e9+7;
inline int popcount(int x) {
	int ret=0;
	while(x) {
		x-=x&-x;
		ret++;
	}
	return ret;
}
int f[2][1<<N];
inline bool check(const int &s,const int &t,const int &n) {
	for(register int i=1;i<n;i++) {
		if(((s>>(i-1))&1)>((t>>i)&1)) return false;
	}
	return true;
}
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	const int n=getint(),m=getint();
	if(n<=3&&m<=3) {
		int ans=0;
		for(register int s=0;s<1<<(n*m);s++) {
			for(register int t=0,max=0;t<1<<(n+m-2);t++) {
				if(popcount(t)!=n-1) continue;
				int state=s&1;
				for(register int i=n+m-2,p=0;~i;i--) {
					if((t>>i)&1) {
						p+=m;
					} else {
						p++;
					}
					state=state<<1|((s>>p)&1);
				}
				if(max>state) goto Next;
				max=state;
			}
			ans++;
			Next:;
		}
		printf("%d\n",ans);
		return 0;
	}
	if(n<=2) {
		f[0][(1<<n)-1]=1;
		for(register int i=1;i<=m;i++) {
			const bool cur=i&1;
			memset(f[cur],0,sizeof f[cur]);
			for(register int i=0;i<1<<n;i++) {
				for(register int j=0;j<1<<n;j++) {
					if(check(i,j,n)) (f[cur][i]+=f[!cur][j])%=mod;
				}
			}
		}
		int ans=0;
		for(register int i=0;i<1<<n;i++) {
			(ans+=f[m&1][i])%=mod;
		}
		printf("%d\n",ans);
		return 0;
	}
	return 0;
}
