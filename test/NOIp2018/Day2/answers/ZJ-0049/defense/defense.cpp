#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define MAXN 10000000000000
#define LL long long
using namespace std;
char ty[12];
int roadto[600040],road[600040],wz[600040],p[600040],dad[600040];
int tot,n,A,A1,B,B1;
LL f[600040],g[600040];

void cnct(int x,int y)
{
  roadto[++tot]=wz[x],road[tot]=y,wz[x]=tot;
  roadto[++tot]=wz[y],road[tot]=x,wz[y]=tot;
}

void dfs(int x,int fr)
{
  int t=wz[x],pd=0;
  dad[x]=fr;
  while(t)
    {
      if(road[t]!=fr)
        {
		  dfs(road[t],x);
		}
	  t=roadto[t];
	}
  if((x==A && A1==0) || (x==B && B1==0))
    f[x]=MAXN;
  else
    {
	  f[x]=p[x];
	  t=wz[x];
	  while(t)
       {
      if(road[t]!=fr)
        {
          if(f[road[t]]<=g[road[t]])
            f[x]+=f[road[t]];
          else 
		  f[x]+=g[road[t]];
		}
 	  t=roadto[t];
	   }
	}
   if((x==A && A1==1) || (x==B && B1==1))
    g[x]=MAXN;
  else
    {
	  g[x]=0;
	  t=wz[x];
	  while(t)
       {
      if(road[t]!=fr)
        {
		  g[x]+=f[road[t]];
		}
 	  t=roadto[t];
	   }
	}
}

void gx(int x)
{
  int t;
  if(!x) return;
  if((x==A && A1==0) || (x==B && B1==0))
    f[x]=MAXN;
  else
    {
	  f[x]=p[x];
	  t=wz[x];
	  while(t)
       {
      if(road[t]!=dad[x])
        {
          if(f[road[t]]<=g[road[t]])
            f[x]+=f[road[t]];
          else 
		  f[x]+=g[road[t]];
		}
 	  t=roadto[t];
	   }
	}
   if((x==A && A1==1) || (x==B && B1==1))
    g[x]=MAXN;
  else
    {
	  g[x]=0;
	  t=wz[x];
	  while(t)
       {
      if(road[t]!=dad[x])
        {
		  g[x]+=f[road[t]];
		}
 	  t=roadto[t];
	   }
	}
  gx(dad[x]);
}

int main()
{
  int x,y,i,T,t1,t2;
  freopen("defense.in","r",stdin);
  freopen("defense.out","w",stdout);
  scanf("%d%d%s",&n,&T,&ty);
  for(i=1;i<=n;i++) scanf("%d",&p[i]);
  for(i=1;i<n;i++)
    {
	  scanf("%d%d",&x,&y);
	  cnct(x,y);
	}
  if(ty[0]!='B')
  while(T--)
    {
	  scanf("%d%d%d%d",&A,&A1,&B,&B1);
	  dfs(1,0);
	  if(f[1]>=MAXN && g[1]>=MAXN) printf("-1\n");
	  else printf("%lld\n",min(f[1],g[1]));
	}
  else
    {
	  dfs(1,0);
	  while(T--)
      {
	  scanf("%d%d%d%d",&A,&A1,&B,&B1);
	  gx(A),gx(B);
	  if(f[1]>=MAXN && g[1]>=MAXN) printf("-1\n");
	  else printf("%lld\n",min(f[1],g[1]));
	  t1=A,t2=B,A=A1=B=B1=0;
	  gx(t1),gx(t2);
	  }
	}
}
