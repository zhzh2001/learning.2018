#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
int tot,TT,n,m,banx,bany,fid,cirn,ed;
int roadto[105050],wz[105050],road[105050];
int f[5050][5050],b[5050],ans[5050],cir[5050];

void cnct(int x,int y)
{
  roadto[++tot]=wz[x],road[tot]=y,wz[x]=tot;
  roadto[++tot]=wz[y],road[tot]=x,wz[y]=tot;
}

bool ok(int x,int y)
{
  if((x==banx && y==bany) || (x==bany && y==banx)) return false;
  else return true;
}

void qwe(int x,int fr)
{
  int t=wz[x];
  while(t)
    {
      if((road[t]!=fr) && ok(x,road[t]))
        {
		  qwe(road[t],x);
		  f[x][++f[x][0]]=road[t];
		}
	  t=roadto[t];
	}
  sort(f[x]+1,f[x]+1+f[x][0]);
}

void ewq(int x)
{
  if(m==n-1)
  {
  --TT,printf("%d",x);
  if(TT) printf(" "); else printf("\n");
  }
  else b[++b[0]]=x;
  int i,t=f[x][0];
  for(i=1;i<=t;i++)
    {
	  ewq(f[x][i]);
	}
}

void dfs(int x,int fr)
{
  int t=wz[x];
  b[x]=1;
  while(t)
    {
      if(road[t]!=fr)
        {
          if(b[road[t]]) 
		  {cir[++cirn]=x,ed=road[t];
          return;}
		  dfs(road[t],x);
		  if(cirn)
		    {
			  if(ed) cir[++cirn]=x;
			  if(x==ed) ed=0;
			  return;
			}
		}
	  t=roadto[t];
	}
}

bool sma()
{
  int i;
  for(i=1;i<=n;i++)
    if(b[i]<ans[i]) return true;
    else if(b[i]>ans[i]) return false;
  return false;
}

int main()
{
  int i,j,x,y;
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  scanf("%d%d",&n,&m),TT=n;
  for(i=1;i<=m;i++)
    {
	  scanf("%d%d",&x,&y);
	  cnct(x,y);
	}
  if(m==n-1)
    {
	  qwe(1,0);
	  ewq(1);
	}
  else
    {
	  dfs(1,0),cir[cirn+1]=cir[1];
	  for(i=1;i<=cirn;i++) 
	    {
		 banx=cir[i],bany=cir[i+1];
         memset(b,0,sizeof(b));
         for(j=1;j<=n;j++) f[j][0]=0;
         ed=0,qwe(1,0);
         ewq(1);
         if(i==1 || sma())
           for(j=1;j<=n;j++) ans[j]=b[j];
		}
	  for(i=1;i<n;i++) printf("%d ",ans[i]);
	   printf("%d\n",ans[i]);
	}
}

