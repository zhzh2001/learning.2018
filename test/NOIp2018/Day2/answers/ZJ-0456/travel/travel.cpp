#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=10007;

struct Line{
	int u,v,next;
}e[N<<1];

int vis[N],head[N],b[N];
int flag1,flag2,pre[N],cnt;
int n,m;

void Ins(int u,int v){
	e[++cnt]=(Line){u,v,head[u]};
	head[u]=cnt;
}
void Insert(int u,int v){ Ins(u,v),Ins(v,u); }

void Pre_Dfs(int x,int last){
	vis[x]=1;pre[x]=last;
//	printf("%d %d\n",x,last);
	for (int i=head[x];i;i=e[i].next){
		int y=e[i].v;
		if (y==last) continue;
//		if (x==35) printf("%d %d  %d\n",x,y,vis[y]);
		if (vis[y]&&flag1==0) flag1=y,flag2=x;
		if (!vis[y]) Pre_Dfs(y,x);
	}
}
const int INF=10000;
int tot,res[N];
void Dfs_Tr(int x){
	vis[x]=1;
	res[++tot]=x;
	int Fl;
	do{	
		int p=INF;
		Fl=0;
		for(int i=head[x];i;i=e[i].next){
			int y=e[i].v;
			if (vis[y]) continue;
			Fl=1;
			p=min(p,y);
		}
		if (p!=INF) Dfs_Tr(p);
	}while (Fl);
}

int Sec,Secfl,Secvis;
int Third=0,Thirdfl=1;

int Check(int x){
	int ret=0;
	for (int i=head[x];i;i=e[i].next){
		int y=e[i].v;
		if (vis[y]==0&&b[y]==0) {
			if (ret==0) ret=y;
			else ret=min(ret,y);
		}
	}
	return ret;
}
void Dfs_Rd(int x){
//	printf("%d %d %d\n",x,Sec,Third);
	vis[x]=1;
	res[++tot]=x;
	int Fl,SecflLas=Secfl;
	do{
		Fl=0;
		int p=INF;
		for (int i=head[x];i;i=e[i].next){
			int y=e[i].v;
			if (vis[y]) continue;
			Fl++;
			p=min(p,y);
		}
	//	if (x==64) printf("%d %d  %d   Secvis=%d Secfl=%d  Third=%d\n",x,p,Sec,Secvis,Secfl,Third);

		if (b[p]&&Fl==1&&Secvis==0&&p>Third&&Thirdfl&&Third>0){
//			printf("P=%d Third=%d\n",p,Third);
			Third=0;
			Thirdfl=0;
			return;
		}
		if (b[x]&&b[p]&&Check(x)){ 
			Third=Check(x);
		}
		if (p==INF) continue;
		if (!b[x]||Secvis==1) Dfs_Rd(p);
		else {
			if (!b[p]){
				Dfs_Rd(p);
				continue;
			}
			if (!Secvis&&!Sec){
				for (int i=head[x];i;i=e[i].next){
					int y=e[i].v;
					if (vis[y]) continue;
					if (y!=p&&b[y]) Sec=y;
				}
				Dfs_Rd(p);
				continue;
			}
			if (Sec>p){
				if (b[p]&&Check(x)!=0){
	//				printf("FL1 X=%d P=%d Secfl=%d\n",x,p,Secfl);
					Secfl=0;
					Dfs_Rd(p);
					Secfl=SecflLas;
	//				printf("FL2 X=%d P=%d Secfl=%d\n",x,p,Secfl);
				}
				else Dfs_Rd(p);
			}
			else if (Secfl==1&&Secvis==0&&Sec&&!vis[Sec]) Dfs_Rd(Sec); 
			else Dfs_Rd(p);
		}
	}while (Fl);
//	printf("%d\n",x);
}

int main(){
	
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	n=read(),m=read();
	For(i,1,m){
		int u=read(),v=read();
		Insert(u,v);
	}
	if (m==n){	
		Pre_Dfs(1,0);
		int x=flag2;
		while (x!=flag1){ 
		//	printf("%d\n",x);
			b[x]=1;
			x=pre[x];
		}
		b[x]=1;
	//	For(i,1,n) if (b[i]) printf("%d ",i);printf("\n");
	}
	memset(vis,0,sizeof(vis));
	if (m==n-1){
		Dfs_Tr(1);
		For(i,1,n-1) printf("%d ",res[i]); printf("%d\n",res[n]);
		return 0;
	}
//	printf("PRE%d\n",pre[5]);
 //	For(i,1,n) if (b[i]) printf("%d ",i); printf("\n");
	Sec=Secvis=0;Secfl=1;
	Dfs_Rd(1);
	For(i,1,n-1) printf("%d ",res[i]); printf("%d\n",res[n]);
}
