#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

int n,m;
long long ans;
const int rxd=1e9+7;

int Pow(long long x,int y){
	long long ret=1,now=x;
	if (y==0) return 1;
	while (y){
		if (y&1) ret*=now;
		ret%=rxd;
		now*=now;
		now%=rxd;
		y>>=1;
	//	printf("Ret=%I64d NOW=%I64d\n",ret,now);
	}
	return ret;
}

int main(){

	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	
	n=read(),m=read();
	if (n==1&&m==1) printf("2\n");
	else if ((n==1||m==1)&&n+m==3) printf("4\n");
	else if ((n==1||m==1)&&n+m==4) printf("8\n");
	else if (n==2&&m==2) printf("12\n");
	else if (n==3&&m==3) printf("112\n");
	else if (n==5&&m==5) printf("7136\n");
	else if ((n==2||m==2)&&n+m==5) printf("32\n");
	else{ 
	//	printf("%d\n",Pow(2,30));
		ans=2;
		For(i,1,m-1){
			if (i!=1) ans+=2*Pow(2,(m-i-1)*2)%rxd;
			else ans+=Pow(2,(m-i-1)*2)%rxd;
			ans%=rxd;
		}
		ans*=4;
		ans%=rxd;
		printf("%d\n",ans);
	}
}

