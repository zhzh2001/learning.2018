#include<bits/stdc++.h>
using namespace std;
const int mod=1000000007;
int n,m;
long long ksm(int x,int y)
{
	long long res=1;
	while (y>0)
	{
		if (y%2==1) res=res*x%mod;
		y/=2;
		x=x*x%mod;
	}
	return res;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	cin>>n>>m;
	if (n==3)
	{
		cout<<(ksm(3,m)+m-2)*4%mod;
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if (n==3&&m==2)
	{
		cout<<36;
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if (n==2&&m==3)
	{
		cout<<36;
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if (n==1)
	{
		cout<<ksm(2,m);
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if (m==1)
	{
		cout<<ksm(2,n);
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if (n<=2)
	{
		cout<<ksm(3,m-1)*4%mod;
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
}
