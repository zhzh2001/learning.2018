#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <vector>
using namespace std;
const int MAXN = 5555;
int n, m;
int ans[MAXN], tmp[MAXN], dfn;
int From[MAXN], To[MAXN];
int F, T;
bool vis[MAXN];
namespace Graph
{
	vector<int> G[MAXN];
	void addEdge(int u, int v) {G[u].push_back(v);}
	bool cmp(const int & a, const int & b) {return a < b;}
	void init()
	{
		for (int i = 1; i <= n; ++i)
		{
			sort(G[i].begin(), G[i].end(), cmp);
		}
	}
	void dfs(int u, int fa)
	{
		if (vis[u])
		{
			tmp[1] = 2;
			return;
		}
		vis[u] = true;
		++dfn;
		tmp[dfn] = u;
		for (size_t i = 0, M = G[u].size(); i < M; ++i)
		{
			int v = G[u][i];
			if (v == fa)
				continue;
			if (u == F && v == T)
				continue;
			if (u == T && v == F)
				continue;
			dfs(v, u);
		}
	}
}

void Solve()
{
	memset(tmp, 0x3f, sizeof tmp);
	memset(vis, 0, sizeof vis);
	dfn = 0;
	Graph::dfs(1, 0);
	bool flag = false;
	for (int i = 1; i <= n; ++i)
	{
		if (tmp[i] < ans[i])
		{
			flag = true;
			break;
		}
		else if (tmp[i] > ans[i])
		{
			break;
		}
	}
	if (flag)
	{
		for (int i = 1; i <= n; ++i)
			ans[i] = tmp[i];
	}
}
int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	memset(ans, 0x3f, sizeof ans);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; ++i)
	{
		int u, v;
		scanf("%d%d", &u, &v);
		Graph::addEdge(u, v);
		Graph::addEdge(v, u);
		From[i] = u;
		To[i] = v;
	}
	Graph::init();
	if (m == n - 1)
		Solve();
	else
	{
		for (int i = 1; i <= m; ++i)
		{
			F = From[i];
			T = To[i];
			Solve();
		}	
	}
	printf("%d", ans[1]);
	for (int i = 2; i <= n; ++i)
		printf(" %d", ans[i]);
	putchar('\n');
	return 0;
}
