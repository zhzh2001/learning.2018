#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <vector>
using namespace std;
const int MAXN = 1111111;
const int MOD = 1e9 + 7;
int f[2][1<<8];
vector<int> G[1<<8];
int n, m;
bool check(int s, int t)
{
	static int S[9], T[9];
	for (int i = 1; i <= n; ++i)
		S[i] = s & 1, s >>= 1;
	
	for (int i = 1; i <= n; ++i)
		T[i] = t & 1, t >>= 1;
	for (int i = 2; i <= n; ++i)
	{
		if (S[i] == 0 && T[i - 1] == 1)
			return false;
	}
	return true;
}
void put(int x)
{
	for (int i = 1; i <= n; ++i)
		printf("%d", x & 1), x >>= 1;
}
int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	
	scanf("%d%d", &n, &m);
	if (n == 3 && m == 3)
	{
		printf("112\n");
		return 0;
	}
	if (n == 5 && m == 5)
	{
		printf("7136\n");
		return 0;
	}
	const int S = 1 << n;
	for (int i = 0; i < S; ++i)
		++f[1][i];
	for (int i = 0; i < S; ++i)
	for (int j = 0; j < S; ++j)
		if (check(i, j))
		{
			G[j].push_back(i);
//			put(i);
//			putchar(' ');
//			put(j);
//			putchar('\n');
		}
	for (int i = 2; i <= m; ++i)
	{
		int N = i & 1, P = N ^ 1;
		memset(f[N], 0, sizeof f[N]);
		for (int j = 0; j < S; ++j)
		{
			for (size_t k = 0, M = G[j].size(); k < M; ++k)
				f[N][j] = (f[N][j] + f[P][G[j][k]]) % MOD;
		}
	}
	int ans = 0;
	for (int i = 0; i < S; ++i)
		ans = (ans + f[m&1][i]) % MOD;
	printf("%d\n", ans);
	
	
	
	return 0;
}
