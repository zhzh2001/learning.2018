#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <vector>
using namespace std;
int G[5][5];
int n, m;
void print()
{
	for (int i = 1; i <= n; ++i)
	{
		for (int j = 1; j <= m; ++j)
		{
			printf("%d", G[i][j]);
		}
		putchar('\n');
	}
	putchar('\n');
}
string A[1111111];
string B[1111111];
int tot = 0;
void dfs(int x, int y, string a, string b)
{
	if (x > n) return;
	if (y > m) return;
	b = b + (G[x][y] == 0 ? '0' : '1');
	if (x == n && y == m)
	{
		++tot;
		A[tot] = a;
		B[tot] = b;
	}
	dfs(x + 1, y, a + "D", b);
	dfs(x, y + 1, a + "R", b);
}
bool check()
{
	tot = 0;
	dfs(1, 1, "", "");
	for (int i = 1; i <= tot; ++i)
	for (int j = 1; j <= tot; ++j)
	{
		if (A[i] > A[j] && B[i] > B[j])
			return false;
	}
	return true;
}
bool check2()
{
	for (int i = 1; i < m; ++i)
	{
		for (int j = 2; j <= n; ++j)
			if (G[j][i] == 0 && G[j - 1][i + 1] == 1)
				return false;
	}
	return true;
}
int main()
{
	scanf("%d%d", &n, &m);
	int P = n * m;
	int ans = 0;
	for (int i = 0; i < (1 << P); ++i)
	{
		int t = i;
		for (int j = 1; j <= P; ++j)
		{
			int row = (j - 1) / m + 1;
			int col = j - (row - 1) * m;
			G[row][col] = t & 1;
			t >>= 1;
		}
		if (check())
		{
//			print();
			++ans;
		}
		else
		{
//			if (check2())
//				print();
			
		}
	}
	printf("%d\n", ans);
	
	return 0;
}
