#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <vector>
using namespace std;
int n, m;
char C[1000];
vector<int> G[222222];
int a, x, b, y;
long long f[222222][2], P[222222], INF;
void update(int u, int t, int fa)
{
	if (t == 0)
	{
		f[u][0] = 0;
		for (size_t i = 0, M = G[u].size(); i < M; ++i)
		{
			int v = G[u][i];
			if (v == fa) continue;
			if (f[v][1] >= INF)
			{
				f[u][0] = INF;
				return;	
			}
			f[u][0] += f[v][1];
		}
	}
	else if (t == 1)
	{
		f[u][1] = P[u];
		for (size_t i = 0, M = G[u].size(); i < M; ++i)
		{
			int v = G[u][i];
			if (v == fa) continue;
			if (min(f[v][1], f[v][0]) >= INF)
			{
				f[u][1] = INF;
				return;	
			}
			f[u][1] += min(f[v][1], f[v][0]);
		}
	}
}
void dfs(int u, int fa)
{
	for (size_t i = 0, M = G[u].size(); i < M; ++i)
	{
		int v = G[u][i];
		if (v == fa) continue;
		dfs(v, u);
	}
	if (u == a)
		update(u, x, fa);
	else if (u == b)
		update(u, y, fa);
	else
		update(u, 0, fa), update(u, 1, fa);
}
int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d", &n, &m);
	scanf("%s", C);
	for (int i = 1; i <= n; ++i)
		scanf("%lld", &P[i]);
	for (int i = 1; i < n; ++i)
	{
		int u, v;
		scanf("%d%d", &u, &v);
		G[u].push_back(v);
		G[v].push_back(u);
//		Pass[u][v] = Pass[v][u] = true;
	}
	for (int i = 1; i <= n; ++i)
		sort(G[i].begin(), G[i].end());
	memset(f, 0x3f, sizeof f);
	INF = f[0][0];
	for (int i = 1; i <= m; ++i)
	{
		scanf("%d%d%d%d", &a, &x, &b, &y);
		if (x == 0 && y == 0)
		{
			vector<int>::iterator it = lower_bound(G[a].begin(), G[a].end(), b);
			if (it != G[a].end() && *it == b)
			{
				printf("-1\n");
				continue;
			}
		}
		dfs(1, 0);
		if (min(f[1][1], f[1][0]) == INF)
		{
			printf("-1\n");
			continue;	
		}
		printf("%lld\n", min(f[1][1], f[1][0]));
		memset(f, 0x3f, sizeof f);
	}
	return 0;
}
