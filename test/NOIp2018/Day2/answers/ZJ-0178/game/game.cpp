#include<iostream>
#include<cstdio>
#include<cstring>
#define ll long long
using namespace std;
const int mod = 1e9+7;
const int N = 10;
int n, m, ans;
int a[N][N];

int Pow(int x, int y)
{
	int ret = 1;
	while (y){
		if (y&1) ret = 1ll*ret*x%mod;
		x = 1ll*x*x%mod;
		y >>= 1;
	}
	return ret;
}

void Do0()
{
	ans = Pow(2, m);
	printf("%d\n", ans);
}

void Do1()
{
	ans = 4ll*Pow(3, m-1)%mod;
	printf("%d\n", ans);
}

bool check()
{
	for (int i = 1; i < n; ++ i)
		for (int j = 1; j < m; ++ j){
			if (a[i+1][j] < a[i][j+1]) return false;
			if (a[i+1][j] == a[i][j+1]){
				for (int k = i+1; k < n; ++ k)
					for (int o = j+1; o < m; ++ o)
						if (a[k+1][o] != a[k][o+1])
							return false;
			}
		}
	return true;
}

void Dfs(int x, int y)
{
	if (x == n+1){
		if (check()){
			++ ans;
			if (ans >= mod) ans -= mod;
		}
		return;
	}
	int nx = x, ny = y+1;
	if (ny > m){
		++ nx;
		ny = 1;
	}
	a[x][y] = 1;
	Dfs(nx, ny);
	a[x][y] = 0;
	Dfs(nx, ny);
}

void Do2()
{
	ans = 0;
	Dfs(1, 1);
	printf("%d\n", ans);
}

void Do3()
{
	if (n == 5 && m == 5) printf("7136\n");
}

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n > m) swap(n, m);
	if (n == 0 || m == 0){
		printf("0\n");
		return 0;
	}
	if (n == 1){
		Do0();
	}
	else if (n == 2){
		Do1();
	}
	else if (n*m <= 20){
		Do2();
	}
	else{
		Do3();
	}
	return 0;
}
