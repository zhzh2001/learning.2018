#include<iostream>
#include<cstdio>
#include<cstring>
#define ll long long
using namespace std;
const ll inf = 1e12+7;
const int N = 1e5+10;
int n, m, co[N], point[N], nowtp;
struct EDGE{
	int nxt, v;
}edge[N<<1];
char tp[2];
struct QRY{
	int a, x, b, y;
}qry[N];
ll f[N][2];

void add_edge(int u, int v, int e)
{
	edge[e] = (EDGE){point[u], v};
	point[u] = e;
}

void Dfs1(int u, int fa)
{
	f[u][0] = 0ll;
	f[u][1] = 1ll*co[u];
	for (int i = point[u]; i != -1; i = edge[i].nxt){
		int v = edge[i].v;
		if (v == fa) continue;
		Dfs1(v, u);
		f[u][0] += f[v][1];
		f[u][1] += min(f[v][0], f[v][1]);
	}
	if (f[u][0] > inf){
		f[u][0] = inf;
	}
	if (f[u][1] > inf){
		f[u][1] = inf;
	}
	if (qry[nowtp].a == u){
		f[u][1-qry[nowtp].x] = inf;
	}
	else if (qry[nowtp].b == u){
		f[u][1-qry[nowtp].y] = inf;
	}
}

void Do1()
{
	for (int i = 1; i <= m; ++ i){
		nowtp = i;
		Dfs1(1, 0);
		ll ans = min(f[1][0], f[1][1]);
		printf("%lld\n", ans == inf ? -1ll : ans);
	}
}

int main()
{
	freopen("defense.in", "r", stdin);
	freopen("defense.out", "w", stdout);
	scanf("%d%d%s", &n, &m, tp);
	for (int i = 1; i <= n; ++ i)
		scanf("%d", &co[i]);
	memset(point, -1, sizeof(point));
	for (int i = 0, ub = n-1; i < ub; ++ i){
		int x, y;
		scanf("%d%d", &x, &y);
		add_edge(x, y, i<<1);
		add_edge(y, x, i<<1^1);
	}
	for (int i = 1; i <= m; ++ i)
		scanf("%d%d%d%d", &qry[i].a, &qry[i].x, &qry[i].b, &qry[i].y);
	Do1();
	return 0;
}
