#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N = 10010;
const int M = 20010;
int n, m, point[N], dgr[N];
struct EDGE{
	int nxt, v;
}edge[M];
bool vis[N];
int loop[N], ln, banx, bany;

void add_edge(int u, int v, int e)
{
	edge[e] = (EDGE){point[u], v};
	point[u] = e;
}

void Dfs1(int u, int fa)
{
	printf("%d ", u);
	int son[dgr[u]+1], sn = 0;
	for (int i = point[u]; i != -1; i = edge[i].nxt){
		int v = edge[i].v;
		if (v == fa) continue;
		son[++ sn] = v;
	}
	sort(son+1, son+sn+1);
	for (int i = 1; i <= sn; ++ i)
		Dfs1(son[i], u);
}

void Do1()
{
	Dfs1(1, 0);
	puts("");
}

int Dfs2(int u, int fa)
{
	vis[u] = 1;
	for (int i = point[u]; i != -1; i = edge[i].nxt){
		int v = edge[i].v;
		if (v == fa) continue;
		if (vis[v]){
			loop[++ ln] = u;
			return v;
		}
		int tmp = Dfs2(v, u);
		if (tmp > 0){
			loop[++ ln] = u;
			if (u == tmp) return -1;
			else return tmp;
		}
		else if (tmp == -1){
			return -1;
		}
	}
	return 0;
}

void Dfs3(int u, int fa)
{
	printf("%d ", u);
	int son[dgr[u]+1], sn = 0;
	for (int i = point[u]; i != -1; i = edge[i].nxt){
		int v = edge[i].v;
		if (v == fa) continue;
		if (banx == u && bany == v) continue;
		if (banx == v && bany == u) continue;
		son[++ sn] = v;
	}
	sort(son+1, son+sn+1);
	for (int i = 1; i <= sn; ++ i)
		Dfs3(son[i], u);
}

void Do2()
{
	memset(vis, 0, sizeof(vis));
	ln = 0;
	Dfs2(1, 0);
	int le[2], xyy[2][N], xn, chs;
	
	le[0] = 2;
	while (loop[le[0]] < loop[ln-1] && le[0] < ln-1)
		++ le[0];
	-- le[0];
	xyy[0][xn = 1] = loop[ln];
	for (int i = 1; i <= le[0]; ++ i)
		xyy[0][++ xn] = loop[i];
	for (int i = ln-1; i > le[0]; -- i)
		xyy[0][++ xn] = loop[i];
	
	le[1] = ln-2;
	while (loop[le[1]] < loop[1] && le[1] > 1)
		-- le[1];
	++ le[1];
	xyy[1][xn = 1] = loop[ln];
	for (int i = ln-1; i >= le[1]; -- i)
		xyy[1][++ xn] = loop[i];
	for (int i = 1; i < le[1]; ++ i)
		xyy[1][++ xn] = loop[i];
	
	chs = 0;
	for (int i = 1; i <= ln; ++ i)
		if (xyy[0][i] != xyy[1][i]){
			chs = (xyy[0][i] < xyy[1][i] ? 0 : 1);
			break;
		}
	banx = loop[le[chs]];
	bany = loop[le[chs]+(chs ? -1 : 1)];
	Dfs3(1, 0);
	puts("");
}

int main()
{
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	scanf("%d%d", &n, &m);
	memset(point, -1, sizeof(point));
	memset(dgr, 0, sizeof(dgr));
	for (int i = 0; i < m; ++ i){
		int x, y;
		scanf("%d%d", &x, &y);
		++ dgr[x];
		++ dgr[y];
		add_edge(x, y, i<<1);
		add_edge(y, x, i<<1^1);
	}
	if (m == n-1){
		Do1();
	}
	else{
		Do2();
	}
	return 0;
}
