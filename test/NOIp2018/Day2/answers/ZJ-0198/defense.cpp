#include<bits/stdc++.h>
using namespace std;
#define MAXN 100005
int n,m,nedge,ans,x,y,nxt[MAXN*2],too[MAXN*2],head[MAXN],a[MAXN],f[MAXN];char ch,cc;
inline int rea()
{
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	int x=ch-'0';ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
inline void addedge(int x,int y)
{
	nxt[++nedge]=head[x],head[x]=nedge,too[nedge]=y;
	nxt[++nedge]=head[y],head[y]=nedge,too[nedge]=x;
}
inline void dfs(int x,int fa,int sum,int t,int tot)
{
	if (tot==n){
		if (sum<ans) ans=sum;
		return;
	}
	int k=head[x];
	while (k>0){
		int v=too[k];
		if (v!=fa){
			if (f[x]==0)
				if (t==1) dfs(v,x,sum+a[v],2,tot+1);
				else dfs(v,x,sum,1,tot+1),dfs(v,x,sum+a[v],2,tot+1);
			else if (f[x]==1) dfs(v,x,sum,1,tot+1);
			else if (f[x]==2) dfs(v,x,sum+a[v],2,tot+1);
		}
		k=nxt[k];
	}
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=rea(),m=rea(),ch=getchar();
	while (ch<'A'||ch>'C') ch=getchar();
	cc=getchar();
	for (int i=1;i<=n;i++) a[i]=rea();
	for (int i=1;i<n;i++) x=rea(),y=rea(),addedge(x,y);
	if (n<=10&&m<=10){
		for (int i=1;i<=m;i++){
			memset(f,0,sizeof(f)),ans=1e9;
			x=rea(),y=rea(),f[y]=x+1;
			x=rea(),y=rea(),f[y]=x+1;
			dfs(1,0,0,1,0),dfs(1,0,a[1],2,0);
			if (ans==1e9) printf("-1\n");else printf("%d\n",ans);
		}
	}else for (int i=1;i<=m;i++) printf("-1\n");
	return 0;
}
