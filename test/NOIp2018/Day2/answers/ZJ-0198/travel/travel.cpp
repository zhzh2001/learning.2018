#include<bits/stdc++.h>
using namespace std;
struct arr{
	int fi,se;
}a[10005];
int n,m,nedge,tot,x,y,nxt[10005],head[5005],too[10005];
inline int rea()
{
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	int x=ch-'0';ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
inline bool cmp(arr x,arr y)
{
	return x.fi<y.fi||x.fi==y.fi&&x.se>y.se;
}
inline void swp(int&x,int&y)
{
	int t=x;x=y,y=t;
}
inline void addedge(int x,int y)
{
	nxt[++nedge]=head[x],head[x]=nedge,too[nedge]=y;
}
inline void dfs(int x,int fa)
{
	tot++,printf("%d%c",x,(tot==n)?'\n':' ');
	int k=head[x];
	while (k>0){
		int v=too[k];
		if (v!=fa) dfs(v,x);
		k=nxt[k];
	}
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=rea(),m=rea();
	for (int i=1;i<=m;i++) a[i].fi=rea(),a[i].se=rea();
	if (m==n-1){
		for (int i=1;i<=m;i++) a[i+m].fi=a[i].se,a[i+m].se=a[i].fi;
		sort(a+1,a+2*m+1,cmp),m=m*2;
		for (int i=1;i<=m;i++) addedge(a[i].fi,a[i].se);
		dfs(1,0);
	}else{
		for (int i=1;i<=m;i++) addedge(a[i].fi,a[i].se),addedge(a[i].se,a[i].fi);
		printf("1");
		x=too[head[1]],y=too[nxt[head[1]]];
		if (x>y) swp(x,y);printf(" %d",x);
		int tot=2,fa=1;
		while (tot<n){
			int k=head[x];bool flag=true;
			while (k>0){
				int v=too[k];
				if (v>y){flag=false;break;}
				if (v!=fa){printf(" %d",v),tot++,fa=x,x=v;break;}
				k=nxt[k];
			}
			if (flag==false) break;
		}
		fa=1,printf(" %d",y),tot++;
		while (tot<n){
			int k=head[y];
			while (k>0){
				int v=too[k];
				if (v!=fa){printf(" %d",v),tot++,fa=y,y=v;break;}
				k=nxt[k];
			}
		}
		printf("\n");
	}
	return 0;
}
