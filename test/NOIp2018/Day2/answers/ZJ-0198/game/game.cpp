#include<bits/stdc++.h>
using namespace std;
#define MO (long long)1000000007
int n,m,ans,b[10],B[10],c[10],C[10],a[5][5];long long f[1000005];
inline bool Check()
{
	bool flag=false;
	for (int i=1;i<=n+m-2;i++){
		if (c[i]>C[i]) return false;
		if (c[i]<C[i]){flag=true;break;}
	}
	if (flag==false) return true;
	for (int i=1;i<=n+m-2;i++){
		if (b[i]<B[i]) return false;
		if (b[i]>B[i]) return true;
	}
	return true;
}
inline bool check()
{
	bool flag=false;
	for (int i=1;i<=n+m-2;i++){
		if (c[i]<C[i]) return false;
		if (c[i]>C[i]){flag=true;break;}
	}
	if (flag==false) return true;
	for (int i=1;i<=n+m-2;i++){
		if (b[i]>B[i]) return false;
		if (b[i]<B[i]) return true;
	}
	return true;
}
inline bool wrk2(int x,int y,int t)
{
	if (x==n&&y==m){
		if (check()==true||Check()==true) return true;
		else return false;
	}
	C[t]=1,B[t]=a[x][y];bool flag1=true,flag2=true;
	if (y<m) flag1=wrk2(x,y+1,t+1);
	C[t]=0;
	if (x<n) flag2=wrk2(x+1,y,t+1);
	B[t]=0;return flag1&flag2;
}
inline bool wrk1(int x,int y,int t)
{
	if (x==n&&y==m){
		if (wrk2(1,1,1)==true) return true;
		else return false;
	}
	c[t]=1,b[t]=a[x][y];bool flag1=true,flag2=true;
	if (y<m) flag1=wrk1(x,y+1,t+1);
	c[t]=0;
	if (x<n) flag2=wrk1(x+1,y,t+1);
	b[t]=0;return flag1&flag2;
}
inline void dfs(int x,int y)
{
	if (x>n){
		if (wrk1(1,1,1)==true) ans++;
		return;
	}
	a[x][y]=1;
	if (y<m) dfs(x,y+1);else dfs(x+1,1);
	a[x][y]=0;
	if (y<m) dfs(x,y+1);else dfs(x+1,1);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n<=3&&m<=3) dfs(1,1),printf("%d\n",ans);
	else if (n==2){
		f[1]=4LL;
		for (int i=2;i<=m;i++) f[i]=f[i-1]*3LL%MO;
		printf("%lld\n",f[m]);
	}else if (n==3){
		f[1]=8LL,f[2]=36LL,f[3]=112LL;
		for (int i=4;i<=m;i++) f[i]=f[i-1]*3LL%MO;
		printf("%lld\n",f[m]);
	}
	return 0;
}
