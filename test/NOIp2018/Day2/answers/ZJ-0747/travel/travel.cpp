#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<vector>
using namespace std;
const int maxn=5010;

struct Edge{
	int from,to;
};

bool vis[maxn];
int n,m,from,to;
vector<Edge> G[maxn];
vector<int> son[maxn];

void dfs1(int x,int fa){
	vis[x]=true;
	for (int i=0;i<G[x].size();++i){
		Edge &e=G[x][i];
		if (e.to!=fa && !vis[e.to]){
			dfs1(e.to,x);
			son[x].push_back(e.to);
		}
	}
}

void dfs2(int x,int fa){
  printf("%d ",x);
  vis[x]=true;
  for (int i=0;i<son[x].size();++i){
  	  if (!vis[son[x][i]]) dfs2(son[x][i],x);  	 
  }
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;++i){
		scanf("%d%d",&from,&to);
		G[from].push_back((Edge){from,to});
		G[to].push_back((Edge){to,from});
	}
  
	memset(vis,0,sizeof(vis));	
	dfs1(1,0);
	for (int i=1;i<=n;++i)
	  sort(son[i].begin(),son[i].end());
	/*for (int i=1;i<=n;++i){
	  for (int j=0;j<son[i].size();++j)
	    printf("%d ",son[i][j]);  
		puts("");
		
	}*/
	memset(vis,0,sizeof(vis));
	dfs2(1,0);
	
	return 0;
}
