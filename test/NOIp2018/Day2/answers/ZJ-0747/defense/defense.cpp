#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<vector>
using namespace std;
const int maxn=1e5+10;
typedef long long LL;
const LL INF=0x3f3f3f3f3f3f3f3f;
struct Edge{
	int to,nxt;
}edges[maxn<<1];

template<typename T> inline void chkmin(T &a,T b){if (a>b) a=b;}
int x,y,num1,num2,z,cnt=0,n,m;
int head[maxn],dep[maxn],fa[maxn],size[maxn];
LL ans[maxn][2],dp[maxn][2];
LL a[maxn];
char state[10];

void addedge(int from,int to){
	edges[++cnt]=(Edge){to,head[from]};
	head[from]=cnt;
}

void dfs1(int x,int f){
	fa[x]=f;
	dep[x]=dep[f]+1;
	size[x]=1;
	for (int i=head[x];i;i=edges[i].nxt){
		Edge &e=edges[i];
		if (e.to!=f) {
			dfs1(e.to,x);
			size[x]+=size[e.to];
		}
	}
}



void DP(int x,int f){
	int tmp=n+1;
	bool flag=false;
	dp[x][1]+=a[x];
	dp[x][0]=0;
	for (int i=head[x];i;i=edges[i].nxt ){
		Edge &e=edges[i];
		if (e.to!=f){
			DP(e.to,x);
            dp[x][1]+=min(dp[e.to][0],dp[e.to][1]);
            if ( dp[e.to][0]>dp[e.to][1] ) {
            	flag=true; dp[x][0]+=dp[e.to][1];
			} 
			else {
				dp[x][0]+=dp[e.to][0];
				if (dp[tmp][1] > dp[e.to][1] ) tmp=e.to;
			}
		}
	}
	if (!flag && size[x]!=1) dp[x][0]-=(dp[tmp][0]-dp[tmp][1]);
	
}

void dp2(int x,int f){
	bool flag=false;
	int tmp=n+1;
	ans[x][1]+=a[x];
	ans[x][0]=0;
	for (int i=head[x];i;i=edges[i].nxt ){
		Edge &e=edges[i];
		if (e.to!=f){
			dp2(e.to,x);
			if (e.to==x)   ans[e.to][num1^1]=INF;
			if (e.to==y)   ans[e.to][num2^1]=INF;  
			
            ans[x][1]+=min(ans[e.to][0],ans[e.to][1]);
            ans[x][1]+=a[x];
            if ( ans[e.to][0] > ans[e.to][1] ) {
            	flag=true; ans[x][0]+=ans[e.to][1];
			} 
			else {
				ans[x][0]+=ans[e.to][0];
				if ( ans[tmp][1] > ans[e.to][1] ) tmp=e.to;
			}
		}
	}
	if (!flag && size[x]!=1) ans[x][0]-=(ans[tmp][0]-ans[tmp][1]);
}


int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
    scanf("%d%d",&n,&m); scanf("%s",state);
    for (int i=1;i<=n;++i) scanf("%lld",&a[i]);
    dp[n+1][1]=INF;
    int from,to;
    for (int i=1;i<n;++i){
    	scanf("%d%d",&from,&to);
    	addedge(from,to);
    	addedge(to,from);
	}
	dfs1(1,1);
	
	DP(1,1);

	for (int i=1;i<=m;++i){
		scanf("%d%d%d%d",&x,&num1,&y,&num2);
		if (dep[x]<dep[y]) swap(x,y);
		if (!(num1 || num2) && fa[x]==y)  { puts("-1"); continue;}
	     memset(ans,0,sizeof(ans));
	     dp2(1,0);
	     printf("%lld\n",min(ans[1][0],ans[1][1]));
	}
	
	return 0;
}
