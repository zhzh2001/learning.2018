#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<vector>
using namespace std;
const int maxn=1e6+10;
typedef long long LL;
const LL mod=1e9+7;

int n,m;
LL dp[9][maxn][2];
template <typename T> inline void  add(T &a ,T b){a=(a+b)%mod;}

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i)
	   dp[i][1][0]=dp[i][1][1]=(1<<(i-1));
	for (int i=1;i<=n;++i)
	   dp[1][i][0]=dp[1][i][1]=(1<<(i-1));
	 
	for (int i=2;i<=n;++i)
	  for (int j=2;j<=m;++j){
	  	 LL tmp=( (dp[i][j-1][1]*((dp[i-1][j][0]+dp[i-1][j][1]))/(dp[i-1][j-1][0]+dp[i-1][j-1][1])%mod)%mod + dp[i][j-1][0]*dp[i-1][j][0]/(dp[i-1][j-1][0]+dp[i-1][j-1][1])%mod) %mod;  
	  	 add(dp[i][j][1],  tmp);
	  	 add(dp[i][j][0],  tmp);
	  }
	printf("%lld",(dp[n][m][0]+dp[n][m][1])%mod );
	return 0;
}
