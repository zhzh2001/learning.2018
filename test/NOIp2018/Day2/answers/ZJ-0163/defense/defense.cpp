#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;
long long p[100004];
long long f[100004][2];
vector<int> v[100004];
int xz[100004];
int dfs(int a,int b){
	f[a][1]=p[a];
	f[a][0]=0;
	for(int i=0;i<v[a].size();i++){
		if(v[a][i]==b)continue;
		dfs(v[a][i],a);
		f[a][1]+=f[v[a][i]][0];
		f[a][0]+=f[v[a][i]][1];
	}
	if(xz[a]==1){
		f[a][1]=1e14;
	}
	if(xz[a]==2){
		f[a][0]=1e14;
	}
	if(f[a][0]>f[a][1])f[a][0]=f[a][1];
}
inline long long min(long long a,long long b){
	return a>b?b:a;
}
int main(){
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	int n,m;char c;char d;
	cin>>n>>m>>c>>d;
	for(int i=1;i<=n;i++){
		scanf("%lld",p+i);
	}
	for(int i=1;i<n;i++){
		int g,h;
		scanf("%d%d",&g,&h);
		v[g].push_back(h);
		v[h].push_back(g);
	}
	for(int i=1;i<=m;i++){
		int t,q,r,s,fl=0;
		scanf("%d%d%d%d",&t,&q,&r,&s);
		if(q==0 && s==0)for(int i=0;i<v[t].size();i++){
			if(v[t][i]==r){
				cout<<-1<<endl;
				fl=1;break;
			}
		}
		if(fl)continue;
		xz[t]=q+1;xz[r]=s+1;
		dfs(1,1);
		printf("%lld\n",f[1][0]);
		xz[t]=xz[r]=0;
	}
}
