#include<bits/stdc++.h>
using namespace std;
int n,m,from,to,p[100010],num,head[100010],flag1,flag2,a,b;
int dp[100010][2];
string s;
struct xx
{
	int next,to;
}way[200010];
int read()
{
	int x=0;
	char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') {
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	return x;
}
void add(int from,int to)
{
	way[++num].next=head[from];
	way[num].to=to;
	head[from]=num;
}
void work(int x,int fa)
{
	dp[x][1]=p[x]; dp[x][0]=0;
	bool t1=0,t2=0;
	for(int i=head[x];i;i=way[i].next)
	{
		int y=way[i].to;
		if(y==fa) continue;
		work(y,x);
		if(!t1) {
			if(min(dp[y][0],dp[y][1])==0x3f3f3f3f)
			{
				t1=1;
				dp[x][1]=0x3f3f3f3f;
			}
			else dp[x][1]+=min(dp[y][0],dp[y][1]);
		}
		if(!t2) {
			if(dp[y][1]==0x3f3f3f3f) {t2=1; dp[x][0]=0x3f3f3f3f;}
			else dp[x][0]+=dp[y][1];
		}
	}
	if(x==a) dp[x][!flag1]=0x3f3f3f3f;
	if(x==b) dp[x][!flag2]=0x3f3f3f3f;
}
int main()
{
	freopen("defense.in","r",stdin);
	freopen("defense.out","w",stdout);
	n=read(); m=read();	cin>>s;
	if(n<=2000&&m<=2000)
	{
		for(int i=1;i<=n;i++) p[i]=read();
		for(int i=1;i<n;i++)
		{
			from=read(); to=read(); 
			add(from,to); add(to,from);
		}
		while(m--)
		{
			a=read(); flag1=read(); b=read(); flag2=read();
			work(1,0);
			if(min(dp[1][0],dp[1][1])==0x3f3f3f3f) printf("-1\n");
			else printf("%d\n",min(dp[1][0],dp[1][1]));
		}
	}

	fclose(stdin);
	fclose(stdout);
	return 0;
}
