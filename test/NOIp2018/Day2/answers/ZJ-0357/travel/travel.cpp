#include<bits/stdc++.h>
using namespace std;
int n,m,from,to,ans[5010],t,degree[5010],c[5010],k,p[5010],h;
vector<int> v[5010];
bool f[5010][5010],b[5010],flag[5010];
void dfs(int x,int fa)
{
	ans[++t]=x;
	for(int i=0;i<v[x].size();i++)
	{
		int y=v[x][i];
		if(y==fa) continue;
		dfs(y,x);
	}
}
queue<int> q;
void Dfs(int x)
{
	c[++k]=x;
	for(int i=0;i<v[x].size();++i)
	{
		int y=v[x][i];
		if(b[y]) continue;
		if(flag[y]) continue;
		flag[y]=1;
		Dfs(y);
		break;
	}
}
void top_sort()
{
	for(int i=1;i<=n;i++) {if(degree[i]==1) {q.push(i);b[i]=1;}}
	while(q.size()) {
		int x=q.front(); q.pop();
		for(int i=0;i<v[x].size();++i) {
			int y=v[x][i];
			if(b[y]) continue;
			degree[y]--;
			if(degree[y]==1) {q.push(y);b[y]=1;}
		}
	}
	for(int i=1;i<=n;i++) {if(b[i]==0) {flag[i]=1;Dfs(i);c[++k]=i;break;}}
}
void dfss(int x,int fa)
{
	p[++h]=x;
	for(int i=0;i<v[x].size();++i) {
		int y=v[x][i];
		if(y==fa) continue;
		if(f[x][y]) continue;
		dfss(y,x);
	}
}
void work(int x)
{
	if(x==1) {
		for(int i=1;i<=n;i++) ans[i]=p[i];
		return;
	}
	else{
		bool ff=0;
		for(int i=1;i<=n;++i)
		{
			if(ans[i]<p[i]) break;
			if(ans[i]>p[i]) {ff=1; break;}
		}
		if(ff==1) {
			for(int i=1;i<=n;i++) ans[i]=p[i];
		}
	} 
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;++i) {
		scanf("%d%d",&from,&to);
		degree[from]++; degree[to]++;
		v[from].push_back(to);
		v[to].push_back(from);
	}
	for(int i=1;i<=n;++i) sort(v[i].begin(),v[i].end());
	if(m==n-1) {
		dfs(1,0);
		for(int i=1;i<=n;i++) printf("%d ",ans[i]); 
	}	
	else{
		top_sort();
		for(int i=1;i<k;++i) {
			h=0;
			f[c[i]][c[i+1]]=f[c[i+1]][c[i]]=1; 
			dfss(1,0);
			f[c[i]][c[i+1]]=f[c[i+1]][c[i]]=0;
			work(i); 			
		}
		for(int i=1;i<=n;++i) printf("%d ",ans[i]); 
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
