#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("defense.in");
ofstream fout("defense.out");
const int N=100005,INF=1e9;
int p[N],head[N],v[N*2],nxt[N*2],e,a,x,b,y,dp[N][2];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int u,int fat)
{
	dp[u][0]=dp[u][1]=INF;
	if(a==u)
	{
		if(x)
			dp[u][1]=p[u];
		else
			dp[u][0]=0;
	}
	else if(b==u)
	{
		if(y)
			dp[u][1]=p[u];
		else
			dp[u][0]=0;
	}
	else
	{
		dp[u][1]=p[u];
		dp[u][0]=0;
	}
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			dfs(v[i],u);
			if(dp[v[i]][1]!=INF&&dp[u][0]!=INF)
				dp[u][0]+=dp[v[i]][1];
			else
				dp[u][0]=INF;
			if(min(dp[v[i]][0],dp[v[i]][1])!=INF&&dp[u][1]!=INF)
				dp[u][1]+=min(dp[v[i]][0],dp[v[i]][1]);
			else
				dp[u][1]=INF;
		}
}
int main()
{
	int n,m;
	string type;
	fin>>n>>m>>type;
	for(int i=1;i<=n;i++)
		fin>>p[i];
	for(int i=1;i<n;i++)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	while(m--)
	{
		fin>>a>>x>>b>>y;
		dfs(1,0);
		int ans=INF;
		if(a==1)
			ans=dp[1][x];
		else if(b==1)
			ans=dp[1][y];
		else
			ans=min(dp[1][0],dp[1][1]);
		if(ans==INF)
			fout<<-1<<'\n';
		else
			fout<<ans<<'\n';
	}
	return 0;
}
