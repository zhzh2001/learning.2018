#include<fstream>
#include<algorithm>
#include<cstdlib>
#include<set>
using namespace std;
ifstream fin("track.in");
ofstream fout("track.out");
const int N=50005;
int n,m,head[N],v[N*2],w[N*2],nxt[N*2],id[N*2],e;
inline void add_edge(int u,int v,int w,int id)
{
	::v[++e]=v;
	::w[e]=w;
	::id[e]=id;
	nxt[e]=head[u];
	head[u]=e;
}
int f[N],g[N],ans,chain[N],len[1005][1005],p[1005*1005];
pair<int,int> np[1005*1005];
void dfs(int u,int fat)
{
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			dfs(v[i],u);
			if(f[v[i]]+w[i]>=f[u])
			{
				g[u]=f[u];
				f[u]=f[v[i]]+w[i];
			}
			else if(f[v[i]]+w[i]>g[u])
				g[u]=f[v[i]]+w[i];
		}
	ans=max(ans,f[u]+g[u]);
}
bool f1,f2;
unsigned now[32],path[1005][1005][32];
bool check(int mid)
{
	if(f1)
	{
		int cnt=0;
		multiset<int> S;
		for(int i=n-1;i;i--)
			if(w[i]>=mid)
				cnt++;
			else
				S.insert(w[i]);
		while(S.size()>=2)
		{
			int mx=*S.rbegin();
			S.erase(--S.end());
			set<int>::iterator it=S.lower_bound(mid-mx);
			if(it==S.end())
				break;
			cnt++;
			S.erase(it);
		}
		return cnt>=m;
	}
	if(f2)
	{
		int cnt=0,sum=0;
		for(int i=1;i<n;i++)
		{
			sum+=chain[i];
			if(sum>=mid)
			{
				cnt++;
				sum=0;
			}
		}
		return cnt>=m;
	}
	int cc=0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			if(len[i][j]>=mid)
				np[++cc]=make_pair(i,j);
	int ans=0;
	for(int t=1;1ll*t*n*n*n/32<=2e6;t++)
	{
		for(int i=1;i<=cc;i++)
			p[i]=i;
		random_shuffle(p+1,p+cc+1);
		for(int i=0;i<=n/32;i++)
			now[i]=0;
		int cnt=0;
		for(int i=1;i<=cc;i++)
		{
			bool flag=true;
			for(int j=0;j<=n/32;j++)
				if(path[np[p[i]].first][np[p[i]].second][j]&now[j])
				{
					flag=false;
					break;
				}
			if(flag)
			{
				cnt++;
				for(int j=0;j<=n/32;j++)
					now[j]|=path[np[p[i]].first][np[p[i]].second][j];
			}
			ans=max(ans,cnt);
		}
	}
	return ans>=m;
}
void dfs2(int u,int fat,int dist,int rt)
{
	for(int i=0;i<=n/32;i++)
		path[rt][u][i]=now[i];
	len[rt][u]=dist;
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			now[id[i]/32]^=1<<(id[i]&31);
			dfs2(v[i],u,dist+w[i],rt);
			now[id[i]/32]^=1<<(id[i]&31);
		}
}
int main()
{
	srand(1e9+7);
	fin>>n>>m;
	f1=f2=true;
	int sum=0;
	for(int i=1;i<n;i++)
	{
		int u,v,w;
		fin>>u>>v>>w;
		sum+=w;
		if(v!=1)
			f1=false;
		if(u!=v+1)
			f2=false;
		add_edge(u,v,w,i);
		add_edge(v,u,w,i);
	}
	if(m==1)
	{
		dfs(1,0);
		fout<<ans<<endl;
		return 0;
	}
	if(f1)
	{
		for(int i=1;i<n;i++)
			w[i]=w[i*2];
		sort(w+1,w+n);
	}
	if(f2)
	{
		int cc=0;
		for(int i=1;i<n;i++)
			for(int j=head[i];j;j=nxt[j])
				if(v[j]==i+1)
					chain[++cc]=w[j];
	}
	if(!f1&&!f2)
		for(int i=1;i<=n;i++)
			dfs2(i,0,0,i);
	int l=0,r=sum/m,ans=0;
	while(l<=r)
	{
		int mid=(l+r)/2;
		if(check(mid))
		{
			l=mid+1;
			ans=mid;
		}
		else
			r=mid-1;
	}
	fout<<ans<<endl;
	return 0;
}
