#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("road.in");
ofstream fout("road.out");
const int N=100005;
int n,a[N];
pair<int,int> tree[N*4];
long long ans;
struct state
{
	int l,r,now;
}q[N];
void build(int id,int l,int r)
{
	if(l==r)
		tree[id]=make_pair(a[l],l);
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		tree[id]=min(tree[id*2],tree[id*2+1]);
	}
}
pair<int,int> query(int id,int l,int r,int L,int R)
{
	if(L<=l&&R>=r)
		return tree[id];
	int mid=(l+r)/2;
	if(R<=mid)
		return query(id*2,l,mid,L,R);
	if(L>mid)
		return query(id*2+1,mid+1,r,L,R);
	return min(query(id*2,l,mid,L,R),query(id*2+1,mid+1,r,L,R));
}
/*
void solve(int l,int r,int now)
{
	pair<int,int> mn=query(1,1,n,l,r);
	ans+=mn.first-now;
	if(mn.second>l)
		solve(l,mn.second-1,mn.first);
	if(mn.second<r)
		solve(mn.second+1,r,mn.first);
}
*/
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	build(1,1,n);
	int l=1,r=1;
	q[1].l=1;q[1].r=n;q[1].now=0;
	for(;l<=r;l++)
	{
		pair<int,int> mn=query(1,1,n,q[l].l,q[l].r);
		ans+=mn.first-q[l].now;
		if(mn.second>q[l].l)
		{
			q[++r].l=q[l].l;q[r].r=mn.second-1;q[r].now=mn.first;
		}
		if(mn.second<q[l].r)
		{
			q[++r].l=mn.second+1;q[r].r=q[l].r;q[r].now=mn.first;
		}
	}
	fout<<ans<<endl;
	return 0;
}
