#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;
const int N=3e5+7;
int Q,n,m,sn,sm,X,Y,mnx=1e9,mny=1e9,mxx,mxy;
struct node {
	int x,y;
}p[N];
//bool cmp(node a,node b) {return min(abs(a.x-midx),abs(a.y-midy))<min(abs(b.x-midx),abs(b.y-midy));}
bool cmp(node a,node b) {return min(min(a.x,n-a.x),min(a.y,m-a.y))>min(min(b.x,n-b.x),min(b.y,m-b.y));}
int main() {
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	scanf("%d%d%d",&n,&m,&Q);
	X=n,Y=m;
	for (int i=1;i<=n;i++) scanf("%d%d",&p[i].x,&p[i].y);
	sort(p+1,p+Q+1,cmp);
	for (int i=1,x,y,mn,s1,s2,s3,s4;i<=Q;i++) {
		x=p[i].x;y=p[i].y;
		if (x<=sn || x>=n || y<=sm || y>=m) continue;
		mn=1e9;
		s1=x-sn;s2=n-x;s3=y-sm;s4=m-y;
		mn=min(mn,min(s1,s2));mn=min(mn,min(s3,s4));
		if (s1==mn) sn=x;
		else if (s2==mn) n=x;
		else if (s3==mn) sm=y;
		else if (s4==mn) m=y;
	}
	printf("%d\n",(n-sn+m-sm)*2);
}
