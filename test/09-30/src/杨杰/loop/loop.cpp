#include <cstdio>
#include <cstring>
using namespace std;
const int N=5e5+7,P=1e9+7,H=2333;
char s[N];
int ad[N]={1},hash[N],f[N];
long long g[N];
int n;
void per() {
	for (int i=1;i<=n;i++) ad[i]=ad[i-1]*H%P;
	for (int i=1;i<=n;i++) hash[i]=1ll*hash[i-1]*H%P+s[i]-'a'+1;
}
int Fhash(int l,int r) {
	return (hash[r]-1ll*hash[l-1]*ad[r-l+1]%P+P)%P;
}
bool check(int l,int r) {
	if (l==r) return 1;
	for (int i=l+1,lx;i<=r;i++) {
		lx=l+r-i;
		if (Fhash(i,r)==Fhash(l,lx) && Fhash(l,i-1)==Fhash(lx+1,r)) return 0;
	}
	return 1;
}
int main() {
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	scanf("%s",s+1);
	n=strlen(s+1);
	per();
	g[0]=1;
	for (int i=1;i<=n;i++) {
		for (int j=0;j<i;j++) {
			if (f[i] && f[j]>f[i]-1) break;
			if (!g[j]) continue;
			if (check(j+1,i)) f[i]=f[j]+1,g[i]+=g[j];
		}
	}
	printf("%d\n%lld\n",f[n],g[n]);
}
