#include <cstdio>
#include <algorithm>
using namespace std;
const int N=3005,P=1e9+7,M=1e5+7;
int n,m,Q;
int mp[N][N],f[N][N];
int F[M<<1]={1};
void work1() {
	for (int i=1,x,y;i<=Q;i++) scanf("%d%d",&x,&y),mp[x][y]=-1;
	f[0][1]=1;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++) if (mp[i][j]!=-1){
			if (mp[i-1][j]!=-1) f[i][j]+=f[i-1][j];
			if (mp[i][j-1]!=-1) f[i][j]+=f[i][j-1];
			f[i][j]%=P;
		}
	printf("%d\n",f[n][m]);
}
int ksm(int a,int b) {
	int s=1;
	for (;b;a=1ll*a*a%P,b>>=1) if (b&1) s=1ll*s*a%P;
	return s;
}
int C(int m,int n) {
	if (m>n) return 0;
	return 1ll*F[n]*ksm(1ll*F[m]*F[n-m]%P,P-2)%P;
}
void work2() {
	int mx=n,my=0,ans=0;
	for (int i=1,x,y;i<=Q;i++) scanf("%d%d",&x,&y),mx=min(mx,x),my=max(my,y);
	for (int i=1;i<=2*n;i++) F[i]=1ll*F[i-1]*i%P;
	for (int i=1;i<mx;i++) {
		ans+=1ll*C(i-1,i+my-3)*C(n-i,n-i+m-my-1)%P;
		ans%=P;
	}
	printf("%d\n",ans);
}
int main() {
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	scanf("%d%d%d",&n,&m,&Q);
	if (n<=3000) work1();
	else work2();
	return 0;
}
