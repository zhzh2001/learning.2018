#include <fstream>
#include <iostream>

using namespace std;

const int maxn = 3005;
const int mod = 1e9 + 7;

int dp[maxn][maxn];
bool hav[maxn][maxn];

int main()
{
	ifstream fin("BOARD.in");
	ofstream fout("BOARD.out");
	int h, w, n;
	fin >> h >> w >> n;
	dp[1][1] = 1;
	for(int i = 1; i <= n; ++i)
	{
		int x, y;
		fin >> x >> y;
		hav[x][y] = true;
	}
	for(int i = 1; i <= h; ++i)
		for(int j = 1; j <= w; ++j)
			if((i != 1 || j != 1) && !hav[i][j])
				dp[i][j] = (dp[i-1][j] + dp[i][j-1]) % mod;
	fout << dp[h][w];
	return 0;
}
