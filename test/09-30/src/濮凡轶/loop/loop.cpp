#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>

using namespace std;

const int maxn = 500005;

int aa[30];
char c[maxn];

int main()
{
	gets(c);
	int len = strlen(c);
	for(int i = 0; i < len; ++i)
		aa[c[i] - 'a']++;
	int ans = 0;
	for(int i = 0; i < 26; ++i)
		ans = __gcd(ans, aa[i]);
	if(ans <= 1 || len % ans)
		puts("1");
	else
		printf("%d\n", len / ans / 2 + 1);
	printf("%d\n", ans * 2 - 1);
//	cout << len << endl;
	return 0;
}
