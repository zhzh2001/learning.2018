#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn = 300005;

int n, x, y;

struct Point { int x, y; bool xuan; } p[maxn];
inline bool cmp_x(Point a, Point b) { return a.x < b.x; }
inline bool cmp_y(Point a, Point b) { return a.y < b.y; }

inline Point P(int x, int y)
{
	Point ans;
	ans.x = x, ans.y = y;
	ans.xuan = false;
	return ans;
}

inline int getx()
{
	sort(p + 1, p + n + 1, cmp_x);
	int ansx, ansy, nowans = 0;
	for(int i = 1, j; i < n; ++i)
	{
		j = i + 1;
		while(p[j].xuan && j <= n)
			j++;
		if(j > n)
			break;
		if(p[i].x - p[j].x + 1 > nowans)
		{
			ansx = i;
			ansy = j;
			nowans = p[i].x + p[j].x + 1;
		}
	}
}

int main()
{
	scanf("%d%d%d", &x, &y, &n);
	for(int i = 1; i <= n; ++i)
	{
		scanf("%d%d", &p[i].x, &p[i].y);
		p[i].xuan = false;
	}
	p[++n] = P(0, 0);
	p[++n] = P(x, 0);
	p[++n] = P(0, y);
	p[++n] = P(x, y);
}
