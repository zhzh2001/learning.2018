#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn = 300005;

int n, x, y;

struct Point { int x, y, id; bool xuan; } p[maxn];
inline bool cmp_x(Point a, Point b) { return a.x < b.x; }
inline bool cmp_y(Point a, Point b) { return a.y < b.y; }
inline bool cmp_id(Point a, Point b) { return a.id < b.id; }

int xuanx[2];
int xuany[2];

inline Point P(int x, int y, int id)
{
	Point ans;
	ans.x = x, ans.y = y;
	ans.xuan = false;
	return ans;
}

inline int getx()
{
	sort(p + 1, p + n + 1, cmp_x);
	int ansx = 0, ansy = 0, nowans = 0;
	for(int i = 1, j; i < n; ++i)
	{
		j = i + 1;
		while(p[j].xuan && j <= n)
			j++;
		if(j > n)
			break;
		if(p[i].x - p[j].x > nowans)
		{
			ansx = i;
			ansy = j;
			nowans = p[i].x - p[j].x;
		}
	}
	p[ansx].xuan = p[ansy].xuan = true;
	xuanx[0] = p[ansx].id;
	xuanx[1] = p[ansy].id;
	return nowans;
}

inline int gety()
{
	sort(p + 1, p + n + 1, cmp_y);
	int ansx = 0, ansy = 0, nowans = 0;
	for(int i = 1, j; i < n; ++i)
	{
		j = i + 1;
		while(p[j].xuan && j <= n)
			j++;
		if(j > n)
			break;
		if(p[i].y - p[j].y > nowans)
		{
			ansx = i;
			ansy = j;
			nowans = p[i].y - p[j].y;
		}
	}
	p[ansx].xuan = p[ansy].xuan = true;
	xuany[0] = p[ansx].id;
	xuany[1] = p[ansy].id;
	return nowans;
}

inline int solve_ding_x(int id)
{
	sort(p + 1, p + n + 1, cmp_id);
	for(int i = 1; i <= n; ++i)
		p[i].xuan = false;
	p[id].xuan = true;
	xuany[0] = id;
	sort(p + 1, p + n + 1, cmp_y);
	int Id = 0;
	for(int i = 1; i <= n; ++i)
	{
		if(p[i].id = Id)
		{
			Id = i;
			break;
		}
	}
	int ans = 0;
	if(p[Id].y - p[Id-1].y > p[Id+1].y - p[Id].y)
	{
		xuany[1] = Id - 1;
		p[Id-1].xuan = true;
		ans = p[Id-1].x +
	}
	else
	{
		xuany[0] = Id + 1;
		p[Id+1].x = true;
	}
	gety()
}

int main()
{
	scanf("%d%d%d", &x, &y, &n);
	for(int i = 1; i <= n; ++i)
	{
		scanf("%d%d", &p[i].x, &p[i].y);
		p[i].id = i;
		p[i].xuan = false;
	}
	n++;
	p[n] = P(0, 0, n);
	n++;
	p[n] = P(x, 0, n);
	n++;
	p[n] = P(0, y, n);
	n++;
	p[n] = P(x, y, n);
	int ansx = getx();
	int ansy = gety();
	int ans = ansx + ansy;
	p[xuanx[1]].xuan = p[xuany[0]].xuan = p[xuany[1]].xuan = false;
	printf("%d\n", ans << 1);
	return 0;
}
