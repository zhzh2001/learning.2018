#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn = 300005;

int n, x, y;

int xx[maxn];
int yy[maxn];
struct Point{ int x, y; } p[maxn];
inline bool operator < (Point a, Point b)
{}

int main()
{
	scanf("%d%d%d", &x, &y, &n);
	for(int i = 1; i <= n; ++i)
	{
		scanf("%d%d", &xx[i], &yy[i]);
		p[i].x = x;
		p[i].y = y;
	}
	xx[++n] = 0;
	yy[n] = 0;
	p[n].x = 0;
	p[n].y = 0;
	xx[++n] = x;
	yy[n] = y;
	p[n].x = x;
	p[n].y = y;
	sort(xx + 1, xx + n + 1);
	sort(yy + 1, yy + n + 1);
	int ansx = 0, ansy = 0;
	for(int i = 1; i < n; ++i)
	{
		ansx = max(xx[i + 1] - xx[i], ansx);
		ansy = max(yy[i + 1] - yy[i], ansy);
	}
	if(n <= 100)
	{
		p[++n].x = 0;
		p[n].y = y;
		p[++n].x = x;
		p[n].y = 0;
		sort(p + 1, p + n + 1, cmp_x);
		for(int i = 1; i <= n; ++i)
		{
			for(int j = i + 2; j <= n; ++j)
			{
				vector<int> tmp;
				for(int k = i + 1; k <= j - 1; ++k)
					tmp.push_back(p[k].y);
				sort(tmp.begin(), tmp.end());
				for(int i = 0; i < tmp.size() - 1; ++i)
					ans = max(tmp[i+1] - tmp[i] + i - j, ans);
			}
		}
	}
	printf("%d\n", max(ansx + x, ansy + y) << 1);
	return 0;
}
