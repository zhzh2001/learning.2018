#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn = 300005;

int n, x, y;

struct Point { int x, y, id; bool xuan; } p[maxn];
inline bool cmp_x(Point a, Point b) { return a.x < b.x; }
inline bool cmp_y(Point a, Point b) { return a.y < b.y; }

inline Point P(int x, int y, int id)
{
	Point ans;
	ans.x = x, ans.y = y;
	ans.xuan = false;
	return ans;
}

inline int getx()
{
	sort(p + 1, p + n + 1, cmp_x);
	int ansx = 0, ansy = 0, nowans = 0;
	for(int i = 1, j; i < n; ++i)
	{
		j = i + 1;
		while(p[j].xuan && j <= n)
			j++;
		if(j > n)
			break;
		if(p[i].x - p[j].x > nowans)
		{
			ansx = i;
			ansy = j;
			nowans = p[i].x - p[j].x;
		}
	}
	p[ansx].xuan = p[ansy].xuan = true;
	return nowans;
}

inline int gety()
{
	sort(p + 1, p + n + 1, cmp_y);
	int ansx = 0, ansy = 0, nowans = 0;
	for(int i = 1, j; i < n; ++i)
	{
		j = i + 1;
		while(p[j].xuan && j <= n)
			j++;
		if(j > n)
			break;
		if(p[i].y - p[j].y > nowans)
		{
			ansx = i;
			ansy = j;
			nowans = p[i].y - p[j].y;
		}
	}
	p[ansx].xuan = p[ansy].xuan = true;
	return nowans;
}

int main()
{
	scanf("%d%d%d", &x, &y, &n);
	for(int i = 1; i <= n; ++i)
	{
		scanf("%d%d", &p[i].x, &p[i].y);
		p[i].id = i;
		p[i].xuan = false;
	}
	n++;
	p[n] = P(0, 0, n);
	n++;
	p[n] = P(x, 0, n);
	n++;
	p[n] = P(0, y, n);
	n++;
	p[n] = P(x, y, n);
	int ansx = getx();
	int ansy = gety();
	int ans =ansx + ansy;
	printf("%d\n", ans << 1);
	return 0;
}
