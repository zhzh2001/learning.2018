#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

const int maxn = 300005;

int n, x, y;

int xx[maxn];
int yy[maxn];
struct Point{ int x, y; } p[maxn];
inline bool operator < (Point a, Point b)
{ return a.x < b.x; }

int main()
{
	freopen("color.in", "r", stdin);
	freopen("color.out", "w", stdout);
	scanf("%d%d%d", &x, &y, &n);
	for(int i = 1; i <= n; ++i)
	{
		scanf("%d%d", &xx[i], &yy[i]);
		p[i].x = xx[i];
		p[i].y = yy[i];
	}
	xx[++n] = 0;
	yy[n] = 0;
	p[n].x = 0;
	p[n].y = 0;
	xx[++n] = x;
	yy[n] = y;
	p[n].x = x;
	p[n].y = y;
	sort(xx + 1, xx + n + 1);
	sort(yy + 1, yy + n + 1);
	int ansx = 0, ansy = 0;
	for(int i = 1; i < n; ++i)
	{
		ansx = max(xx[i + 1] - xx[i], ansx);
		ansy = max(yy[i + 1] - yy[i], ansy);
	}
	int ans = max(ansx + x, ansy + y);
	if(n <= 100)
	{
		p[++n].x = 0;
		p[n].y = y;
		p[++n].x = x;
		p[n].y = 0;
		sort(p + 1, p + n + 1);
		for(int i = 1; i <= n; ++i)
		{
			for(int j = i + 2; j <= n; ++j)
			{
//				cout << i << ' ' << p[i].x << ' ' << j << ' ' << p[j].x << endl;
				vector<int> tmp;
				tmp.clear();
				for(int k = i + 1; k <= j - 1; ++k)
					tmp.push_back(p[k].y);
				sort(tmp.begin(), tmp.end());
				for(int k = 0; k < tmp.size() - 1; ++k)
				{
					ans = max(tmp[k+1] - tmp[k] + p[j].x - p[i].x, ans);
//					printf("%d\n", tmp[k+1] - tmp[k] + p[j].x - p[i].x);
				}
			}
		}
	}
	printf("%d\n", ans << 1);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
