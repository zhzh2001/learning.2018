#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn = 300005;

struct Point { int x, y; bool xuan; } p[maxn];


bool xuan[maxn];

inline Point P(int x, int y)
{
	Point ans;
	ans.x = x, ans.y = y;
	ans.xuan = false;
	return ans;
}

int main()
{
	int n, x, y;
	scanf("%d%d%d", &x, &y, &n);
	for(int i = 1; i <= n; ++i)
	{
		scanf("%d%d", &p[i].x, &p[i].y);
		p[i].xuan = false;
	}
	p[++n] = P(0, 0);
	p[++n] = P(x, 0);
	p[++n] = P(0, y);
	p[++n] = P(x, y);
}
