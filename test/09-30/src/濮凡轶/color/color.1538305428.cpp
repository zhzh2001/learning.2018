#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn = 300005;

int n, x, y;

int xx[maxn];
int yy[maxn];

int main()
{
	scanf("%d%d%d", &x, &y, &n);
	for(int i = 1; i <= n; ++i)
		scanf("%d%d", &xx[i], &yy[i]);
	xx[++n] = 0;
	yy[n] = 0;
	xx[++n] = x;
	yy[n] = y;
	sort(xx + 1, xx + n + 1);
	sort(yy + 1, yy + n + 1);
	int ansx = 0, ansy = 0;
	for(int i = 1; i < n; ++i)
	{
		ansx = max(xx[i + 1] - xx[i], ansx);
		ansy = max(yy[i + 1] - yy[i], ansy);
	}
	printf("%d\n", max(ansx + x, ansy + y) << 1);
	return 0;
}
