#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define int long long
const int N=200005,md=1000000007;
int h,w,n,jc[N],invjc[N],f[N];
struct node{int x,y;}p[N];
inline bool cmp(node a,node b){return (a.x!=b.x)?(a.x<b.x):(a.y<b.y);}
inline int kpw(int x,int y){int re=1LL;while(y){if(y&1)re=re*x%md;x=x*x%md;y>>=1;}return re%md;}
inline int C(int n,int m){return 1LL*jc[n]%md*invjc[m]%md*invjc[n-m]%md;}
inline void Ad(int &x,int y){x=x+y;if(x<0)x+=md;if(x>md)x-=md;}
signed main()
{
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	int i,j; scanf("%lld%lld%lld",&h,&w,&n); jc[0]=invjc[0]=1LL;
	for(i=1;i<=h+w;i++)jc[i]=jc[i-1]*i%md,invjc[i]=kpw(jc[i],md-2)%md;
	for(i=1;i<=n;i++)scanf("%lld%lld",&p[i].x,&p[i].y); memset(f,0,sizeof f);
	sort(p+1,p+n+1,cmp); p[n+1].x=h; p[n+1].y=w;
	for(i=1;i<=n+1;i++)
	{
		Ad(f[i],C(p[i].x+p[i].y-2,p[i].x-1)%md);
		for(j=1;j<i;j++)
		{
			if(p[j].x<=p[i].x&&p[j].y<=p[i].y)Ad(f[i],-1LL*f[j]%md*C(p[i].x-p[j].x+p[i].y-p[j].y,p[i].x-p[j].x)%md);
		}
	}printf("%lld\n",f[n+1]%md);
}
