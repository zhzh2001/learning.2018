#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=3005;
int R,C,n,f[N][N];
struct point{int x,y;}p[N];
inline bool cmp(point a,point b){return (a.x!=b.x)?(a.x<b.x):(a.y<b.y);}
int main()
{
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	int i,j,k,t=0,re=0;; scanf("%d%d%d",&R,&C,&n);
	for(i=1;i<=n;i++){scanf("%d%d",&p[i].x,&p[i].y);}sort(p+1,p+n+1,cmp);
	if(n<=100)
	{
		for(i=1;i+3<=n;i++)
		{
			for(j=i+3;j<=n;j++)
			{
				if(p[j].x-p[i].x==0)continue; for(k=i+1;k<j-1;k++)re=max(re,2*(p[j].x-p[i].x)+2*(p[k+1].y-p[k].y));
			}
		}return printf("%d\n",re),0;
	}
	else
	{
		for(i=1;i<n;i++)
		{
			f[i][i+1]=p[i+1].y-p[i].y;
			for(j=i+2;j<=n;j++)
			{
				f[i][j]=max(f[i][j-1],p[j].y-p[j-1].y);
			}
		}
		for(i=1;i+3<=n;i++)
		{
			for(j=i+3;j<=n;j++)
			{
				if(p[j].x-p[i].x==0)continue; re=max(re,2*(p[j].x-p[i].x)+2*f[i+1][j-1]);
			}
		}return printf("%d\n",re),0;
	}
}
