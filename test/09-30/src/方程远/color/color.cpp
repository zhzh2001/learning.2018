#include<bits/stdc++.h>
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
struct Edge{
	int x,y;
}line[300001],cross[300001];
bool cmp(Edge x,Edge y){
	return x.x<y.x;
}
int X,Y,n,x,y,maxx,ans;
int main(){
	freopen("color.in","r",stdin);
	freopen("color.out","r",stdin);
	read(X);
	read(Y);
	read(n);
	for(int i=1;i<=n;i++){
		read(line[i].x);
		read(cross[i].x);
		line[i].y=cross[i].y=i;
	}
	line[n+1].x=X;
	cross[n+1].y=Y;
	sort(line,line+2+n,cmp);
	sort(cross,cross+2+n,cmp);
	for(int i=0;i<=n;i++)
		for(int j=i+1;j<=n+1;j++){
			maxx=0;
			x=line[i].y;
			y=line[j].y;
			for(int k=0;k<=n;k++){
				if(cross[k].y==x||cross[k].y==y)
					continue;
				int l=k+1;
				while(cross[l].y==x||cross[l].y==y)
					l++;
				maxx=max(cross[l].x-cross[k].x,maxx);
			}
			ans=max(ans,maxx*2+2*(line[j].x-line[i].x));
		}
	cout<<ans;
	return 0;
}
