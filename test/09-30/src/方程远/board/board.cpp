#include<bits/stdc++.h>
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
map<int,map<int,int> >mp,f;
int h,w,n,x,y;
int main(){
	freopen("board.in","r",stdin);
	freopen("board.out","r",stdin);
	read(h);
	read(w);
	read(n);
	for(int i=1;i<=n;i++){
		read(x);
		read(y);
		mp[x][y]=1;
	}
	f[1][1]=1;
	for(int i=1;i<=h;i++)
		for(int j=1;j<=w;j++){
			if(!mp[i-1][j])
				f[i][j]+=f[i-1][j];
			if(!mp[i][j-1])
				f[i][j]+=f[i][j-1];
		}
	cout<<f[h][w];
	return 0;
}
