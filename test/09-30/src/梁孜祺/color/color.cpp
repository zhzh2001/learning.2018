#include<bits/stdc++.h>
#define ll long long
#define N 300005
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
int h, w, n, hah[N], b[N], ans;
struct data {
  int x, y;
  bool operator <(const data b) const {
    return (x == b.x) ? (y < b.y) : (x < b.x);
  }
}a[N];
int main() {
  freopen("color.in","r",stdin);
  freopen("color.out","w",stdout);
  h = read();
  w = read();
  n = read();
  for (int i = 1; i <= n; i++) 
    a[i] = (data) {read(), read()}, hah[i] = a[i].x;
  sort(hah+1,hah+1+n);
  int tot = unique(hah+1,hah+1+n) - hah - 1;
  sort(a+1,a+1+n);
  for (int i = 1; i <= n; i++) 
    a[i].x = lower_bound(hah+1, hah+1+tot,a[i].x) - hah;
  for (int i = 1; i <= tot; i++)
    for (int j = tot; j > i; j--) {
      int sum = hah[j] - hah[i];
      if (sum < ans) continue;
      int tot = 0;
      for (int k = 1; k <= n; k++) {
        if (a[k].x <= i || a[k].x >= j) continue;
        b[++tot] = a[k].y;
      }
      b[++tot] = 0; b[++tot] = w;
      sort(b+1,b+1+tot);
      int mx = 0;
      for (int i = 2; i <= tot; i++)
        if (mx < b[i] - b[i-1]) mx = b[i] - b[i-1];
      ans = max(ans, sum * 2 + mx * 2);
    }
  printf("%d\n", ans);
  return 0;
}