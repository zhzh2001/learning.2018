#include<bits/stdc++.h>
#define ll long long
#define N 500005
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
char s[N];
int n, nxt[N], xh, nxt2[N];
inline void doit() {
  int sum = 0;
  for (int i = 1; i < n; i++)
    if (nxt[i] == 0 || i % (i - nxt[i]) != 0)
      if (nxt2[n - i] == 0 || (n - i) % (n - i - nxt2[n - i]) != 0) sum++;
  printf("%d\n", sum);
}
int main() {
  freopen("loop.in","r",stdin);
  freopen("loop.out","w",stdout);
  scanf("%s",s+1);
  n = strlen(s+1);
  nxt[1] = 0;
  int j = 0;
  for (int i = 2; i <= n; i++) {
    while (j && s[i] != s[j+1]) j = nxt[i];
    if (s[i] == s[j+1]) j++;
    nxt[i] = j;
  }
  for (int i = 1; i <= n / 2; i++) swap(s[i], s[n - i + 1]);
  j = 0;
  for (int i = 2; i <= n; i++) {
    while(j && s[i] != s[j+1]) j = nxt2[i];
    if (s[i] == s[j+1]) j++;
    nxt2[i] = j;
  }
  xh = n - nxt[n];
  if (xh == 1) {
    printf("%d\n", n);
    puts("1");
  }
  else if (n % xh != 0) {
    puts("1");
    puts("1");
  }else {
    puts("2");
    doit();
  }
  return 0;
}