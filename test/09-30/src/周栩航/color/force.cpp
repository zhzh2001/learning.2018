#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=2e5;
int n,mx,my;
struct node{int mx,mi,dif;}	tr[N*4];
struct point{int x,y;}	p[N];
map<int,int> vis;
int to[N],t[N],q[N],ans;
inline bool cmp(point x,point y){return x.x<y.x;}
int main()
{
	freopen("color.in","r",stdin);freopen("color.ans","w",stdout);
	mx=read();my=read();n=read();
	For(i,1,n)	p[i].x=read(),p[i].y=read(),t[i]=p[i].y;
	sort(t+1,t+n+1);
	sort(p+1,p+n+1,cmp);
	p[0].x=0;p[n+1].x=mx;
	For(i,0,n+1)
		For(j,i,n+1)
		{
			int lx=p[j].x-p[i].x;
			int top=0,ly=0;
			q[++top]=0;q[++top]=my;
			For(k,i+1,j-1)	q[++top]=p[k].y;
			sort(q+1,q+top+1);
			For(k,1,top-1)ly=max(ly,q[k+1]-q[k]);//cout<<i<<' '<<j<<' '<<q[k+1]-q[k]<<endl;
			ans=max(ans,ly+lx);		
		}
	writeln(1LL*ans*2);
}
