#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=3e5;
int n,mx,my;
struct node{int mx,mi,dif;}	tr[N*4];
struct point{int x,y;}	p[N];
map<int,int> vis;
int to[N],t[N];
inline bool cmp(point x,point y){return x.x<y.x;}
inline void Build(int x,int l,int r)
{
	tr[x].mi=0;
	tr[x].mx=0;tr[x].dif=0;
	if(l==r)
	{
		return;
	}
	int mid=l+r>>1;
	Build(x<<1,l,mid);Build(x<<1|1,mid+1,r);
}
inline void Up(int x)
{
	tr[x].dif=max(tr[x<<1].dif,tr[x<<1|1].dif);
	if(tr[x<<1].mx)	tr[x].dif=max(tr[x].dif,tr[x<<1|1].mi-tr[x<<1].mx);
	if(tr[x<<1].mi&&tr[x<<1|1].mi)	tr[x].mi=min(tr[x<<1].mi,tr[x<<1|1].mi);
	else	if(tr[x<<1].mi)	tr[x].mi=tr[x<<1].mi;else	tr[x].mi=tr[x<<1|1].mi;
	
	if(tr[x<<1].mx&&tr[x<<1|1].mx)	tr[x].mx=max(tr[x<<1].mx,tr[x<<1|1].mx);
	else	if(tr[x<<1].mx)	tr[x].mx=tr[x<<1].mx;else	tr[x].mx=tr[x<<1|1].mx;
}
inline void Insert(int x,int l,int r,int t)
{
	if(l==r)
	{
		tr[x].mi=tr[x].mx=to[t];
		return;
	}
	int mid=l+r>>1;
	if(t<=mid)	Insert(x<<1,l,mid,t);else	Insert(x<<1|1,mid+1,r,t);
	Up(x);
}
int main()
{
	freopen("color.in","r",stdin);freopen("color.out","w",stdout);
	mx=read();my=read();n=read();
	For(i,1,n)	p[i].x=read(),p[i].y=read(),t[i]=p[i].y;
	sort(t+1,t+n+1);
	int tot=0;
	For(i,1,n)	if(!vis[t[i]])	vis[t[i]]=++tot,to[tot]=t[i];
	For(i,1,n)	p[i].y=vis[p[i].y];
	sort(p+1,p+n+1,cmp);
	p[n+1].x=mx;
	int ans=0;
	For(i,0,n)
	{
		int sx=p[i].x;
		Build(1,1,n);
		int mx1=my,mx2=my,mxy=0;
		For(j,i,n)
		{
			if(j!=i)	mx1=min(mx1,to[p[j].y]),mx2=min(mx2,my-to[p[j].y]);
			if(j!=i)	Insert(1,1,n,p[j].y);//cout<<"For:"<<sx<<' '<<to[p[j].y]<<endl;
			mxy=max(max(mx1,mx2),tr[1].dif);
			ans=max(ans,(p[j+1].x-sx)+mxy);
//			cout<<i<<' '<<j<<' '<<(p[j+1].x-sx)+mxy<<endl;
		}
	}
	writeln(1LL*ans*2);
}
