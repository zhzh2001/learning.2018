#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

int h,w,n;
bool vis[3005][3005];
int main()
{
	freopen("board.in","w",stdout);
	srand(time(0));
	h=3000;w=2900;n=2000;
	cout<<h<<' '<<w<<' '<<n<<endl;
	vis[1][1]=vis[h][w]=1;
	
	For(i,1,n)
	{
		int x=rand()%h+1,y=rand()%w+1;
		while(vis[x][y])	 x=rand()%h+1,y=rand()%w+1;
		cout<<x<<' '<<y<<endl;
	}
}
