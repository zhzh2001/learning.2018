#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back
#define int ll

using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=3005;
int mo=1e9+7;
inline void Upd(ll &x,ll y)
{
	x=((x+y%mo)%mo+mo)%mo;
}
int h,w,n,vis[N][N],dp[N][N];
signed main()
{
	freopen("board.in","r",stdin);freopen("board.ans","w",stdout);
	h=read();w=read();n=read();
	For(i,1,n)
	{
		int x=read(),y=read();
		vis[x][y]=1;
	}
	dp[1][1]=1;
	For(i,1,h)	For(j,1,w)
	if(!vis[i][j])
	{
		Upd(dp[i+1][j],dp[i][j]);
		Upd(dp[i][j+1],dp[i][j]);
	}
	writeln(dp[h][w]);
}
