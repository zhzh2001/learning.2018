#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back
#define y1 zyyak

using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=200005;
ll fac[N],mo=1e9+7,rev[N],n,h,w,dp[5005][2],ans;
inline void Upd(ll &x,ll y)
{
	x=((x+y%mo)%mo+mo)%mo;
}
struct node{int x,y;}	p[N];
inline bool cmp(node x,node y){return x.x+x.y<y.x+y.y;}
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2,x=x*x%mo)	if(y&1)	sum=sum*x%mo;	return sum;}
inline ll C(ll n,ll m)
{
	if(n<m)	return 0;
	return fac[n]*rev[m]%mo*rev[n-m]%mo;
}
inline ll Get(int x1,int y1,int x2,int y2)
{
	ll tmp=x2-x1+y2-y1;
	return C(tmp,x2-x1);
}
int main()
{
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	h=read();w=read();n=read();
	For(i,1,n)
		p[i].x=read(),p[i].y=read();
	sort(p+1,p+n+1,cmp);
	fac[0]=1;
	For(i,1,200000)	fac[i]=fac[i-1]*i%mo;
	rev[200000]=ksm(fac[200000],mo-2);Dow(i,0,200000-1)	rev[i]=(rev[i+1]*(i+1))%mo;
	For(i,1,n)	dp[i][1]=Get(1,1,p[i].x,p[i].y);
	For(i,1,n)
		For(j,i+1,n)
		if(p[i].x<=p[j].x&&p[i].y<=p[j].y)
		{
			Upd(dp[j][1],dp[i][0]*Get(p[i].x,p[i].y,p[j].x,p[j].y)%mo);
			Upd(dp[j][0],dp[i][1]*Get(p[i].x,p[i].y,p[j].x,p[j].y)%mo);
		}
	For(i,1,n)
	{
		Upd(ans,dp[i][0]*Get(p[i].x,p[i].y,h,w)%mo*-1);
		Upd(ans,dp[i][1]*Get(p[i].x,p[i].y,h,w)%mo);
	}
	ll ANS=Get(1,1,h,w);
	Upd(ANS,ans*-1);
	writeln(ANS);
}
