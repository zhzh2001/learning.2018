#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=500055;
struct node{int l,r;}	line[N];
inline bool cmp(node x,node y){return x.l<y.l;}
char s[N];
vector<int> q[26];
int n,num,cnt[N],beg[N],end[N];
unsigned ll hsh[N],bas=29,ten[N];
inline unsigned ll Get(int l,int r){return hsh[r]-hsh[l-1]*ten[r-l+1];}
inline bool check(int l,int r)
{
	if(l==1)	return beg[r];else	return  end[l];
}
inline void Pre()
{
	For(i,2,n)
	{
		if(s[i]==s[1])
		{
			int len=i-1;
			int now=i;
			while(now+len-1<=n)
			{
				if(now+len-1<=n&&Get(now,now+len-1)==Get(1,len))	beg[now+len-1]=1;else	break;
				now=now+len;
			}
		}
	}
	
	reverse(s+1,s+n+1);
	hsh[0]=1;
	For(i,1,n)
		hsh[i]=hsh[i-1]*bas+s[i];
		
	For(i,2,n)
	{
		if(s[i]==s[1])
		{
			int len=i-1;
			int now=i;
			while(now+len-1<=n)
			{
				if(Get(now,now+len-1)==Get(1,len))	end[now+len-1]=1;else	break;
				now=now+len;
			}
		}
	}
	reverse(end+1,end+n+1);
}
int main()
{
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	scanf("\n%s",s+1);
	n=strlen(s+1);
	ten[0]=1;hsh[0]=1;
	For(i,1,n)
	{
		hsh[i]=hsh[i-1]*bas+s[i];
		ten[i]=ten[i-1]*bas;
	}
	For(i,1,n)	cnt[s[i]-'a'+1]++;
	sort(cnt+1,cnt+26+1);reverse(cnt+1,cnt+26+1);
	if(cnt[1]==n)
	{
		writeln(n);writeln(1);
		return 0;
	}
	Pre();
	if(!beg[n])
	{
		writeln(1);writeln(1);
		return 0;
	}
	int ans=0;
	For(i,1,n)
	{
		if(!check(1,i)&&!check(i+1,n))	ans++;
	}
	writeln(2);writeln(ans);
}
