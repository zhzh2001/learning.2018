#include<bits/stdc++.h>
using namespace std;
long long n,X,Y,cnt,tot,ans;
long long lx,ly,rx,ry;
long long val[300005];
struct node
{
	long long x,y;
}a[300005];
struct lisan
{
	long long zhi,id;
}y[300005];
priority_queue<long long>q,p;
set<long long>s;
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
bool cmp(lisan aa,lisan bb)
{
	return aa.zhi<bb.zhi;
}
bool cmpp(node aa,node bb)
{
	return aa.x<bb.x;
}
int main()
{
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	X=read(),Y=read(),n=read();
	if(n>3000)
	{
		rx=X;
		ry=Y;
		for(long long i=1;i<=n;++i) 
		{
			long long x=read(),y=read();
			if(x<=lx||x>=rx||y<=ly||y>=ry) continue;
			long long mnx=min(x-lx,rx-x),mny=min(y-ly,ry-y);
			if(mnx<mny) if(x-lx<=rx-x) lx=x;else rx=x;
			else if(y-ly<=ry-y) ly=y;else ry=y;
		}
		printf("%lld",(rx-lx+ry-ly)*2);
		return 0;
	}
	for(long long i=1;i<=n;++i)
	{
		a[i].x=read(),a[i].y=read();
		y[++cnt].zhi=a[i].y;
		y[cnt].id=i;
	}
	sort(y+1,y+cnt+1,cmp);
	y[0].zhi=-0x3f3f3f;
	for(long long i=1;i<=cnt;++i)
	{
		if(y[i].zhi!=y[i-1].zhi) val[++tot]=y[i].zhi;
		a[y[i].id].y=tot;
	}
	if(val[tot]!=Y) val[++tot]=Y;
	sort(a+1,a+n+1,cmpp);
	for(long long l=n;l>=0;--l)
	{
		while(!q.empty()) q.pop();
		while(!p.empty()) p.pop();
		s.clear();
		q.push(Y);
		s.insert(0);
		s.insert(tot);
		for(long long i=l+1;i<=n;++i)
		{
			ans=max(ans,a[i].x-a[l].x+q.top());
			set<long long>::iterator kk=s.lower_bound(a[i].y);
			if(*kk==a[i].y) continue;
			long long hihir=*kk;
			kk--;
			long long hihil=*kk;
			p.push(val[hihir]-val[hihil]);
			while(!q.empty()&&!p.empty()&&q.top()==p.top()) q.pop(),p.pop();
			q.push(val[a[i].y]-val[hihil]);
			q.push(val[hihir]-val[a[i].y]);
			s.insert(a[i].y);
		}
		ans=max(ans,X-a[l].x+q.top());
	}
	printf("%lld",ans*2);
	return 0;
}
