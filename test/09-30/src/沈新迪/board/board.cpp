#include<bits/stdc++.h>
using namespace std;
const long long mod=1e9+7;
long long H,W,n;
long long jc[200005],ny[200005];
long long f[3005];
struct node
{
	long long x,y;
}a[3005];
bool cmp(node aa,node bb)
{
	return aa.x==bb.x?aa.y<bb.y:aa.x<bb.x;
}
long long ksm(long long aa,long long bb)
{
	long long ans=1;
	while(bb)
	{
		if(bb&1) ans=(ans*aa)%mod;
		bb>>=1;
		aa=(aa*aa)%mod;
	}
	return ans;
}
long long C(long long aa,long long bb)
{
	if(aa>bb) return 0;
	return jc[bb]*ny[aa]%mod*ny[bb-aa]%mod;
}
int main()
{
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	scanf("%lld%lld%lld",&H,&W,&n);
	jc[0]=1;
	for(long long i=1;i<=H+W;++i) jc[i]=(jc[i-1]*i)%mod;
	ny[H+W]=ksm(jc[H+W],mod-2);
	for(long long i=H+W;i>=1;--i) ny[i-1]=(ny[i]*i)%mod;
	for(long long i=1;i<=n;++i) scanf("%lld%lld",&a[i].x,&a[i].y);
	a[++n].x=H;
	a[n].y=W;
	sort(a+1,a+n+1,cmp);
	for(long long i=1;i<=n;++i)
	{
		f[i]=C(a[i].x-1,a[i].x+a[i].y-2);
		for(long long j=i-1;j>=1;--j)
		if(a[j].y<=a[i].y) f[i]=(f[i]-(f[j]*C(a[i].x-a[j].x,a[i].x+a[i].y-a[j].x-a[j].y))%mod+mod)%mod;
	}
	printf("%lld",f[n]);
	return 0;
}
