#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 3010
#define MOD 1000000007
#define maxhw 200010
#define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
struct arr
{
	int x,y;
	friend bool operator < (arr x,arr y)
	{
		return x.x<y.x||x.x==y.x&&x.y<y.y;
	}
};
arr a[maxn];
int h,w,n,ans[maxn],tmp,jiec[maxhw],jiecni[maxhw],ni[maxhw];
void init(int n,int mod)
{
	ni[0]=0;
	ni[1]=1;
	for(int i=2;i<=n;i++)
		ni[i]=(-(mod/i)*ni[mod%i]%mod+mod)%mod;
}
int ccc(int x,int y)
{
	return jiec[x]*jiecni[y]%MOD*jiecni[x-y]%MOD;
}
signed main()
{
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	h=read();
	w=read();
	n=read();
	for(int i=1;i<=n;a[i++].y=read())
		a[i].x=read();
	sort(a+1,a+n+1);
	jiec[0]=1;
	for(int i=1;i<=h+w;i++)
		jiec[i]=jiec[i-1]*i%MOD;
	init(h+w,MOD);
	jiecni[0]=1;
	for(int i=1;i<=h+w;i++)
		jiecni[i]=jiecni[i-1]*ni[i]%MOD;
	for(int i=1;i<=n;i++)
	{
		ans[i]=ccc(a[i].x+a[i].y-2,a[i].x-1);
		for(int j=i-1;j;j--)
			if(a[j].y<=a[i].y) ans[i]=((ans[i]-ccc(a[i].x-a[j].x+a[i].y-a[j].y,a[i].x-a[j].x)*ans[j]%MOD)%MOD+MOD)%MOD;
	}
	for(int i=1;i<=n;i++)
		(tmp+=ans[i]*(ccc(h-a[i].x+w-a[i].y,h-a[i].x))%MOD)%=MOD;
	write(((ccc(h+w-2,h-1)-tmp)%MOD+MOD)%MOD);
	return 0;
}
