#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 500010
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
char s[maxn];
int l,mst,pr[maxn],vis[maxn],r,ans;
void pri(int x)
{
	for(int i=2;i<=(int)sqrt(x);i++)
		for(int j=i*i;!vis[i]&&j<=x;j+=i)
			vis[j]=1;
	for(int i=2;i<=x;i++)
		if(!vis[i]) pr[++r]=i;
}
int check()
{
	for(int i=1;i<=r;i++)
	{
		if(l/pr[i]<1||l%pr[i]!=0) continue;
		int f=1,oc=l/pr[i];
		for(int j=oc+1;j<=l;j++)
			if(s[j]!=s[(j-1)%oc+1])
			{
				f=0;
				break;
			}
		if(f) return pr[i];
	}
	return 0;
}
int check2()
{
	for(int i=2;i<=l;i++)
		if(s[i]!=s[i-1]) return 0;
	return 1;
}
signed main()
{
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	gets(s+1);
	l=strlen(s+1);
	// wln(l);
	pri(l);
	mst=check();
	if(mst==0)
	{
		wln(1);
		write(1);
		return 0;
	}
	if(check2())
	{
		wln(l);
		write(1);
		return 0;
	}
	wln(2);
	write(l-1);
	return 0;
}
