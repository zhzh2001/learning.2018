#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 3010
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
struct node
{
	int x,y;
	friend bool operator < (node a,node b)
	{
		return a.x<b.x||a.x==b.x&&a.y<b.y;
	}
};
node a[maxn];
int n,x,y,q[maxn],ans;
signed main()
{
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	x=read();
	y=read();
	n=read();
	for(int i=1;i<=n;a[i++].y=read())
		a[i].x=read();
	sort(a+1,a+n+1);
	a[0].y=0;
	a[n+1].y=y;
	for(int i=0;i<=n;i++)
		for(int j=i+1;j<=n+1;j++)
		{
			int miy=min(a[i].y,a[j].y);
			int mxy=max(a[i].y,a[j].y);
			int r=0;
			for(int k=1;k<=n;k++)
				if(a[k].y>miy&&a[k].y<mxy) q[++r]=a[k].x;
			int mxx=x-q[r];
			for(int k=1;k<=r;k++)
				mxx=max(mxx,q[k]-q[k-1]);
			ans=max(ans,mxx+mxy-miy<<1);
		}
	write(ans);
	return 0;
}
