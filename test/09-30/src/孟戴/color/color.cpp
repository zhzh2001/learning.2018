//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#define max(a,b) (a>b)?a:b
using namespace std;
ifstream fin("color.in");
ofstream fout("color.out");
bool a[103],b[103];
long long t[103];
struct fangbo{
	long long x,y;
}q[103];
long long x,y,n;
long long ans;
inline long long getans(){
	long long ret=0;
	for(int i=0;i<=x;i++){
		a[i]=1;
	}
	for(int i=0;i<=y;i++){
		b[i]=1;
	}
	for(int i=1;i<=n;i++){
		if(a[i]==1){
			for(int j=1;j<=q[i].x;j++){
				a[i]=0;
			}
		}else if(a[i]==2){
			for(int j=q[i].x+1;j<=x;j++){
				a[i]=0;
			}
		}else if(a[i]==3){
			for(int j=1;j<=q[i].y;j++){
				b[i]=0;
			}
		}else if(a[i]==4){
			for(int j=q[i].y+1;j<=y;j++){
				b[i]=0;
			}
		}
	}
	for(int i=1;i<=x;i++){
		if(a[i]==1){
			ret++;
		}
	}
	for(int i=1;i<=y;i++){
		if(b[i]==1){
			ret++;
		}
	}
	return ret*2;
}
long long dfs(int k){
	if(k>n){
		ans=max(getans(),ans);
		return 0;
	}
	t[k]=1,dfs(k+1),t[k]=2,dfs(k+1);
	t[k]=3,dfs(k+1),t[k]=4,dfs(k+1);
}
int main(){
	fin>>x>>y>>n;
	for(int i=1;i<=n;i++){
		fin>>q[i].x>>q[i].y;
	}
	dfs(1);
	fout<<ans<<endl;
	return 0;
}
/*

in:
10 10 4
1 6
4 1
6 9
9 4

out:
32

*/
