//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("board.in");
ofstream fout("board.out");
const long long mod=1000000007;
long long h,w,n;
long long fac[1000003],inv[1000003];
struct xy{
	long long x,y,ans;
}q[5003];
inline bool cmp(xy a,xy b){
	if(a.x==b.x){
		return a.y<b.y;
	}
	return a.x<b.x;
}
long long qpow(long long a,long long b){
	if(b==1){
		return a;
	}
	long long ret=qpow(a,b/2);
	ret=ret*ret%mod;
	if(b&1){
		ret=ret*a%mod;
	}
	return ret;
}
inline long long C(long long n,long long m){
	if(n==0){
		return 0;
	}
	if(m==0){
		return 1;
	}
	return fac[n]*inv[m]%mod*inv[n-m]%mod;
}
int main(){
	fin>>h>>w>>n;
	fac[0]=1;
	for(int i=1;i<=h+w;i++){
		fac[i]=fac[i-1]*i%mod;
	}
	inv[h+w]=qpow(fac[h+w],mod-2);
	for(int i=h+w-1;i>=0;i--){
		inv[i]=inv[i+1]*(i+1)%mod;
	}
	for(int i=1;i<=n;i++){
		q[i].ans=0;
		fin>>q[i].x>>q[i].y;
	}
	q[++n].x=h,q[n].y=w;
	sort(q+1,q+n+1,cmp);
	for(int i=1;i<=n;i++){
		q[i].ans=C(q[i].x+q[i].y-2,q[i].x-1);
		for(int j=1;j<=i-1;j++){
			if(q[j].x<=q[i].x&&q[j].y<=q[i].y){
				q[i].ans=(q[i].ans-q[j].ans*C(q[i].x+q[i].y-q[j].x-q[j].y,q[i].x-q[j].x)%mod+mod)%mod;
			}
		}
	}
	fout<<q[n].ans<<endl;
	return 0;
}
/*

in:
3 4 2
2 2
2 3

out:
2

*/
