#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
#define PI pair<int,int>
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){write(a); puts("");}
inline void wri(ll a){write(a); putchar(' ');}
inline ull rnd(){
	return ((ull)rand()<<30^rand())<<4|rand()%4;
}
const int N=5005;
char ch[N];
int n,ycl[N][N],f[N],g[N];
int main(){
	scanf("%s",ch+1);
	n=strlen(ch+1);
	for(int i=1;i<=n;i++){
		for(int j=i;j<=n;j++){
			ycl[i][j]=1;
			for(int k=1;k<=j-i;k++)if((j-i+1)%k==0){
				int flag=1;
				for(int l=i+k;l<=j;l++){
					if(ch[l]!=ch[l-k])flag=0;
				}
				if(flag){
					ycl[i][j]=0; break;
				}
			}
		}
	}
	for(int i=1;i<=n;i++){
		if(ycl[1][i]){f[i]=1; g[i]=1;}
		else{
			f[i]=1e9;
			for(int j=1;j<i;j++)if(ycl[j+1][i]){
				if(f[j]+1<f[i]){
					f[i]=f[j]+1; g[i]=g[j];
				}else if(f[j]+1==f[i])g[i]+=g[j];
			}
		}
	}
	cout<<f[n]<<endl<<g[n]<<endl;
}

