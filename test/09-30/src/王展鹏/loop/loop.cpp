#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
#define PI pair<int,int>
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){write(a); puts("");}
inline void wri(ll a){write(a); putchar(' ');}
inline ull rnd(){
	return ((ull)rand()<<30^rand())<<4|rand()%4;
}
const int N=500005;
char ch[N];
int n,ans,f[N],g[N];
void kmp(int *nxt){
	int p=nxt[0]=0;
	for(int i=1;i<n;i++){
		while(ch[p]!=ch[i]&&p)p=nxt[p-1];
		if(ch[p]==ch[i])p++;
		nxt[i]=p;
	}
}
int main(){
	freopen("loop.in","r",stdin); fropen("loop.out","w",stdout);
	scanf("%s",ch);
	n=strlen(ch);
	kmp(f);
	reverse(&ch[0],&ch[n]);
	kmp(g);
	if(f[n-1]==n-1){
		cout<<n<<endl<<1<<endl; return 0;
	}
	if(n%(n-f[n-1])){
		puts("1\n1"); return 0;
	}
	for(int i=0;i<n;i++)if((f[i]==0||(i+1)%(i+1-f[i]))&&(g[n-i-2]==0||(n-i-1)%(n-i-1-g[n-i-2])))ans++;
	cout<<2<<endl<<ans<<endl;
}

