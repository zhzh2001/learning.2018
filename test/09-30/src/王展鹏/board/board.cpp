#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
#define PI pair<int,int>
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){write(a); puts("");}
inline void wri(ll a){write(a); putchar(' ');}
inline ull rnd(){
	return ((ull)rand()<<30^rand())<<4|rand()%4;
}
const int N=200005,mod=1000000007;
const ll mod2=(ll)mod*mod;
ll ans,h,w,n,fac[N],dp[3005],ni[N];
pair<ll,ll> a[3005];
inline ll cal(int x,int y){
	return fac[x+y]*ni[x]%mod*ni[y]%mod;
}
inline ll ksm(ll a,int b){
	int ans=1;
	for(;b;b>>=1){
		if(b&1)ans=ans*a%mod;
		a=a*a%mod;
	}
	return ans;
}
int main(){
	freopen("board.in","r",stdin); freopen("board.out","w",stdout);
	h=read(); w=read(); n=read();
	for(int i=1;i<=n;i++){a[i].first=read(); a[i].second=read();}
	for(int i=fac[0]=1;i<N;i++)fac[i]=fac[i-1]*i%mod; ni[N-1]=ksm(fac[N-1],mod-2); for(int i=N-1;i;i--)ni[i-1]=ni[i]*i%mod;
	sort(&a[1],&a[n+1]);
	ans=cal(h-1,w-1);
	for(int i=1;i<=n;i++){
		dp[i]=cal(a[i].first-1,a[i].second-1);
		for(int j=1;j<i;j++)if(a[j].first<=a[i].first&&a[j].second<=a[i].second){
			dp[i]=dp[i]-dp[j]*cal(a[i].first-a[j].first,a[i].second-a[j].second); if(dp[i]<0)dp[i]+=mod2;
		}
		dp[i]%=mod;
		ans=ans-dp[i]*cal(h-a[i].first,w-a[i].second); if(ans<0)ans+=mod2;
	}
	ans%=mod;
	cout<<(ans+mod)%mod<<endl;
}

