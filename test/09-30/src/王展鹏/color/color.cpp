#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){write(a); puts("");}
inline void wri(ll a){write(a); putchar(' ');}
inline ull rnd(){
	return ((ull)rand()<<30^rand())<<4|rand()%4;
}
const int N=300005;
int h,w,n,ans,zs[N],tree[N<<2],lazy[N<<2],q1[N],q2[N];
struct PI{
	int x,y,id;
}a[N],b[N],c[N];
bool operator <(PI a,PI b){
	return a.x==b.x?a.y<b.y:a.x<b.x;
}
inline bool cmp(PI a,PI b){
	return a.y<b.y;
}
void insert(int l,int r,int i,int j,int x,int nod){
	if(l==i&&r==j){
		tree[nod]+=x; lazy[nod]+=x; return;
	}
	int mid=(l+r)>>1;
	if(j<=mid)insert(l,mid,i,j,x,nod<<1); else if(i>mid)insert(mid+1,r,i,j,x,nod<<1|1);
	else{
		insert(l,mid,i,mid,x,nod<<1); insert(mid+1,r,mid+1,j,x,nod<<1|1);
	}
	tree[nod]=min(tree[nod<<1],tree[nod<<1|1])+lazy[nod];
}
void build(int l,int r,int nod){
	if(l==r){
		tree[nod]=zs[l]; return;
	}
	int mid=(l+r)>>1;
	lazy[nod]=0;
	build(l,mid,nod<<1); build(mid+1,r,nod<<1|1);
	tree[nod]=min(tree[nod<<1],tree[nod<<1|1]);
}
void solve(int l,int r){
	if(l==r){
		ans=max(ans,max(max(a[l].y-b[l-1].y,b[r+1].y-a[l].y)+h,max(a[l].x,h-a[l].x)+(b[r+1].y-b[l-1].y))); 
		return;
	}
	int mid=(l+r)>>1;
	solve(l,mid); solve(mid+1,r); 
	//if(r==150000)cout<<l<<" "<<r<<" "<<a[150000].y<<endl;
	merge(&a[l],&a[mid+1],&a[mid+1],&a[r+1],&c[l]);
	for(int i=l;i<=r;i++)a[i]=c[i];
	int top1=0,top2=0;
	ans=max(ans,a[l].x+b[r+1].y-b[l-1].y);
	int L=b[l-1].y,R=b[r+1].y;
	for(int i=l;i<=r;i++)zs[i]=a[i].x; zs[l-1]=0;
	build(l-1,r,1);
	q1[0]=q2[0]=l-1;
	for(int i=l;i<=r;i++){
		if(a[i].id<=mid){
			while(top1&&a[q1[top1]].y<=a[i].y){
				insert(l-1,r,q1[top1-1],q1[top1]-1,-(a[q1[top1]].y-L),1); top1--;
			}
			q1[++top1]=i; //cout<<q1[top1-1]<<" "<<q1[top1]-1<<endl;
			insert(l-1,r,q1[top1-1],q1[top1]-1,a[q1[top1]].y-L,1);
		}else{
			while(top2&&a[q2[top2]].y>=a[i].y){
				insert(l-1,r,q2[top2-1],q2[top2]-1,-(R-a[q2[top2]].y),1); top2--;
			}
			q2[++top2]=i;
			insert(l-1,r,q2[top2-1],q2[top2]-1,R-a[q2[top2]].y,1);
		}
		if(R-L+h-tree[1]<=ans)return;
		ans=max(ans,R-L+(i==r?h:a[i+1].x)-tree[1]);
	}
}
int main(){
	freopen("color.in","r",stdin); freopen("color.out","w",stdout);
	h=read(); w=read(); n=read();
	for(int i=1;i<=n;i++){
		a[i].x=read(); a[i].y=read(); 
	}
	sort(&a[1],&a[n+1],cmp);
	for(int i=1;i<=n;i++)a[i].id=i; 
	a[0].y=0; a[n+1].y=w;
	for(int i=0;i<=n+1;i++)b[i]=a[i];
	solve(1,n);
	cout<<ans*2<<endl;
}

