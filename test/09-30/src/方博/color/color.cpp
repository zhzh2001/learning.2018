#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=105;
bool a[N],b[N];
int t[N];
int x,y,n;
struct xxx{
	int x,y;
}d[N];
int ans;
int getans()
{
	ll ret=0;
	for(int i=0;i<=x;i++)
		a[i]=1;
	for(int i=0;i<=y;i++)
		b[i]=1;
	for(int i=1;i<=n;i++){
		if(a[i]==1){
			for(int j=1;j<=d[i].x;j++)
				a[i]=0;
		}else if(a[i]==2){
			for(int j=d[i].x+1;j<=x;j++)
				a[i]=0;
		}else if(a[i]==3){
			for(int j=1;j<=d[i].y;j++)
				b[i]=0;
		}else if(a[i]==4){
			for(int j=d[i].y+1;j<=y;j++)
				b[i]=0;
		}
	}
	for(int i=1;i<=x;i++)
		if(a[i]==1)ret++;
	for(int i=1;i<=y;i++)
		if(b[i]==1)ret++;
	return ret*2;
}
int dfs(int k)
{
	if(k>n){
		ans=max(getans(),ans);
		return 0;
	}
	t[k]=1;
	dfs(k+1);
	t[k]=2;
	dfs(k+1);
	t[k]=3;
	dfs(k+1);
	t[k]=4;
	dfs(k+1);
}
int main()
{
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	x=read();y=read();n=read();
	for(int i=1;i<=n;i++)
		d[i].x=read(),d[i].y=read();
	dfs(1);
	writeln(ans);
}
