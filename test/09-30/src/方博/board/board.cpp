#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=100005;
struct xxx{
	ll x,y;
}a[N];
ll f[N];//表示（1,1）到第i个点的方案数
bool com(xxx a,xxx b){return (a.x+a.y)<(b.x+b.y);}
ll fac[N+N];
ll inv[N+N];
const ll mod=1e9+7;
ll qpow(ll a,ll b){
	ll ret=1;
	while(b){
		if(b&1)ret=ret*a%mod;
		a=a*a%mod;
		b>>=1;
	}
	return ret;
}
inline ll C(ll n,ll m){
	if(n==0)return 0;
	if(m==0)return 1;
	return fac[n]*inv[m]%mod*inv[n-m]%mod;
}
ll n,h,w;
int main()
{
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	h=read();w=read();n=read();
	fac[0]=1;
	for(int i=1;i<=h+w;i++)
		fac[i]=fac[i-1]*i%mod;
	inv[h+w]=qpow(fac[h+w],mod-2);
	for(int i=h+w-1;i>=0;i--)
		inv[i]=inv[i+1]*(i+1)%mod;
	for(int i=1;i<=n;i++)
		a[i].x=read(),a[i].y=read();
	sort(a+1,a+n+1,com);
	n++;
	a[n].x=h;a[n].y=w;
	for(int i=1;i<=n;i++){
		int tx=a[i].x,ty=a[i].y;
		f[i]=C(tx+ty-2,tx-1);
		for(int j=1;j<=i;j++)
			if(a[j].x<=a[i].x&&a[j].y<=a[i].y){
				ll x=a[i].x-a[j].x+1;
				ll y=a[i].y-a[j].y+1;
				f[i]=(f[i]-f[j]*C(x+y-2,x-1)%mod+mod)%mod;
			}
	}
	writeln(f[n]);
}
