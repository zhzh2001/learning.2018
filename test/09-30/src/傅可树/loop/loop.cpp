#include<algorithm>
#include<cstring>
#include<cstdio>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=5e5+5;
char s[N];
int n,nxt[N],nxt2[N];
inline void work() {
	int ans=0;
  	for (int i=1;i<n;i++) 
		if (!nxt[i]||i%(i-nxt[i])!=0){
			if (!nxt2[n-i]||(n-i)%(n-i-nxt2[n-i])!=0) ans++;
  		}
	writeln(ans);
}
inline void init(){
	scanf("%s",s+1); n=strlen(s+1);
}
inline void KMP(){
	nxt[1]=0; int j=0;
	for (int i=2;i<=n;i++){
    	while (j&&s[i]!=s[j+1]) j=nxt[i];
    	if (s[i]==s[j+1]) j++;
    	nxt[i]=j;
  	}
  	for (int i=1;i<=n/2;i++) swap(s[i],s[n-i+1]);
  	j=0;
  	for (int i=2;i<=n;i++) {
    	while(j&&s[i]!=s[j+1]) j=nxt2[i];
    	if (s[i]==s[j+1]) j++;
    	nxt2[i]=j;
  	}
}
inline void solve(){
  	KMP();
	int tmp=n-nxt[n];
	if (tmp==1){
		writeln(n);
	    puts("1");
	}else{
		if (n%tmp!=0) {
	    	puts("1"); puts("1");
	  	}else {
	    	puts("2"); work();
		}
	}
}
int main() {
  freopen("loop.in","r",stdin);
  freopen("loop.out","w",stdout);
  init(); solve();
  return 0;
}
