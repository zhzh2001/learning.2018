#include<cstdio>
#include<algorithm>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int H=2e5+5,N=3e3+5,mod=1e9+7;
int n,w,h;
struct node{
	int x,y;
}a[N];
inline void init(){
	h=read(); w=read(); n=read();
	for (int i=1;i<=n;i++) a[i].x=read(),a[i].y=read();
}
int fac[H];
inline int pow(int x,int k){
	int y=1;
	for (;k;k>>=1,x=x*x%mod) if (k&1) y=y*x%mod;
	return y;
}
inline int inv(int x){
	return pow(x,mod-2);
}
inline int C(int x,int y){
	if (!y) return 1;
	return fac[x]*inv(fac[y])%mod*inv(fac[x-y])%mod;
}
inline bool cmp(node A,node B){return A.x<B.x||A.x==B.x&&A.y<B.y;}
int ans,f[N];
inline void solve(){
	fac[0]=1;
	for (int i=1;i<H;i++) fac[i]=fac[i-1]*i%mod;
	sort(a+1,a+1+n,cmp);
	for (int i=1;i<=n;i++){
		for (int j=1;j<i;j++) {
			if (a[j].x<=a[i].x&&a[j].y<=a[i].y) {
				f[i]=(f[i]+f[j]*C(a[i].x+a[i].y-a[j].x-a[j].y,a[i].x-a[j].x)%mod)%mod;
			}
		}
		f[i]=(C(a[i].x+a[i].y-2,a[i].x-1)-f[i]+mod)%mod;
		ans+=f[i]*C(h+w-a[i].x-a[i].y,h-a[i].x)%mod; ans%=mod;
	}
	ans=(C(h+w-2,w-1)-ans+mod)%mod;
	writeln(ans);
}
signed main(){
	freopen("board.in","r",stdin); freopen("board.out","w",stdout);
	init(); solve();
	return 0;
}
