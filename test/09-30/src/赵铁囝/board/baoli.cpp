#include <iostream>
#include <cstdio>

using namespace std;

const int wzp=1e9+7;

int n,m,t,x,y,f[1005][1005];
bool vis[1005][1005];

int main(){
	ios::sync_with_stdio(false);
	cin>>n>>m>>t;
	for(int i=1;i<=t;i++){
		cin>>x>>y;
		vis[x][y]=true;
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			if(i==1&&j==1){
				f[i][j]=1;
				continue;
			}
			if(vis[i][j]){
				f[i][j]=0;
				continue;
			}
			f[i][j]=(f[i-1][j]+f[i][j-1])%wzp;
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			cout<<f[i][j]<<" ";
		}
		cout<<endl;
	}
	cout<<f[n][m]<<endl;
	return 0;
}
