#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>

#define ll long long
#define Max 3005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

const ll wzp=1e9+7;

struct Node{
	ll x,y;
	inline bool operator<(const Node&a)const{
		if(x!=a.x)return x<a.x;
		return y<a.y;
	}
}a[Max];

ll n,m,l,r,s,ans,size,wx[100005],wy[100005],fact[200005],idx[Max],idy[Max],num[Max],f[Max][Max],sum[Max][Max];
bool vis[100005],use[Max][Max];

inline ll pow(ll x,ll y){
	ll ans=1;
	while(y){
		if(y&1)ans=ans*x%wzp;
		x=x*x%wzp;
		y>>=1;
	}
	return ans;
}

inline ll calc(ll x,ll y){
	return fact[x+y]*pow(fact[x]*fact[y]%wzp,wzp-2)%wzp;
}

int main(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	fact[0]=1;
	for(ll i=1;i<=200000;i++)fact[i]=fact[i-1]*i%wzp;
	n=read();m=read();s=read();
	if(n<=2000&&m<=2000){
		for(int i=1;i<=s;i++){
			l=read();r=read();
			use[l][r]=true;
		}
		for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++){
				if(i==1&&j==1){
					f[i][j]=1;
					continue;
				}
				if(use[i][j]){
					f[i][j]=0;
					continue;
				}
				f[i][j]=(f[i-1][j]+f[i][j-1])%wzp;
			}
		}
		writeln(f[n][m]);
		return 0;
	}
	for(ll i=1;i<=s;i++){
		a[i].x=read();a[i].y=read();
		if(!vis[a[i].x]){
			vis[a[i].x]=true;
			num[++size]=a[i].x;
		}
	}
	if(!vis[1])num[++size]=1;
	if(!vis[n])num[++size]=n;
	sort(num+1,num+size+1);
	for(ll i=1;i<=size;i++){
		wx[num[i]]=i;
		idx[i]=num[i];
	}
	l=size;
	size=0;
	memset(vis,0,sizeof vis);
	for(ll i=1;i<=s;i++){
		if(!vis[a[i].y]){
			vis[a[i].y]=true;
			num[++size]=a[i].y;
		}
	}
	if(!vis[1])num[++size]=1;
	if(!vis[m])num[++size]=m;
	sort(num+1,num+size+1);
	for(ll i=1;i<=size;i++){
		wy[num[i]]=i;
		idy[i]=num[i];
	}
	r=size;
	for(ll i=1;i<=s;i++){
		use[wx[a[i].x]][wy[a[i].y]]=true;
	}
	for(ll i=1;i<=l;i++){
		for(ll j=1;j<=r;j++){
			if(use[i][j]){
				f[i][j]=calc(idx[i]-1,idy[j]-1);
			}else{
				f[i][j]=f[i-1][j-1]*calc(idx[i]-idx[i-1],idy[j]-idy[j-1])%wzp;
				f[i][j]=((f[i][j]+f[i-1][j]-f[i-1][j-1])%wzp+wzp)%wzp;
				f[i][j]=((f[i][j]+f[i][j-1]-f[i-1][j-1])%wzp+wzp)%wzp;
			}
			sum[i][j]=sum[i][j-1]+sum[i-1][j]-sum[i-1][j-1]+f[i][j];
//			cout<<f[i][j]<<" ";
		}
//		cout<<endl;
	}
	ans=calc(n-1,m-1);
	writeln(((ans-f[l][r])%wzp+wzp)%wzp);
	return 0;
}
