#include <algorithm>
#include <iostream>
#include <cstdio>

#define ll long long
#define Max 300005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

struct Node{
	int x,y;
	inline bool operator<(const Node&a)const{
		if(x!=a.x)return x<a.x;
		else return y<a.y;
	}
}a[Max];

inline bool cmp(Node x,Node y){
	return x.y<y.y;
}

int x,y,n,b,c,d,e,ans;

int main(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	x=read();y=read();n=read();
	b=0;c=x+1;d=0;e=y+1;
	for(int i=1;i<=n;i++){
		a[i].x=read();a[i].y=read();
	}
	sort(a+1,a+n+1);
	ans=max(ans,max(y*2+a[1].x*2,y*2+(x-a[n].x)*2));
	sort(a+1,a+n+1,cmp);
	ans=max(ans,max(x*2+a[1].y*2,x*2+(y-a[n].y)*2));
	writeln(ans);
	return 0;
}
