#include <iostream>
#include <cstdio>

using namespace std;

int n;

int main(){
	freopen("loop.in","w",stdout);
	n=500000;
	while(n--){
		if(n%2==0){
			putchar('a');
		}else{
			putchar('b');
		}
	}
	return 0;
}
