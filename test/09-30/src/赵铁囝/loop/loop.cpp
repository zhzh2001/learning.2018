#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>

#define ll long long
#define Max 500005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

int n,sum,size,f[Max],num[Max],num2[Max];
bool flag,vis[Max],vis2[Max];
char s[Max];

inline bool check(int x){
	for(int i=1,j=1;i<=n;i++,j=j%x+1){
//		cout<<i<<" "<<j<<" "<<x<<endl;
		if(s[i]!=s[j])return false;
	}
//	cout<<endl;
	return true;
}

inline void calc(int x,bool&flag){
	flag=false;
	for(int i=2;i<=n/2;i++){
		if(n%i==0){
			if(check(i)){
				flag=true;
				return;
			}
		}
	}
	return;
}

int main(){
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	scanf("%s",s+1);
	n=strlen(s+1);
	calc(n,flag);
	if(!flag){
		puts("1\n1");
	}else{
		flag=false;
		for(int i=2;i<=n;i++){
			if(s[i]!=s[i-1]){
				flag=true;
				break;
			}
		}
		if(!flag){
			printf("%d\n%d\n",n,1);
			return 0;
		}
		for(int i=1;i<n;i++){
			for(int j=1;j<=i/2;j++){
				if(i%j==0){
					if(vis[i-j]){
						if(num[i-j]!=j){
							continue;
						}else{
							flag=false;
							for(int k=1;k<=j;k++){
								if(s[i-j+k]!=s[i-j-j+k]){
									flag=true;
									break;
								}
							}
							if(!flag){
								num[i]=num[i-j];
								vis[i]=true;
								break;
							}
						}
					}
					if(i/j==2){
						flag=false;
						for(int k=1;k<=j;k++){
							if(s[k]!=s[k+j]){
								flag=true;
								break;
							}
						}
						if(!flag){
							vis[i]=true;
							num[i]=j;
							break;
						}
					}
				}
			}
//			cout<<vis[i]<<endl;
		}
		for(int i=1;i<=n/2;i++)swap(s[i],s[n-i+1]);
		for(int i=1;i<n;i++){
//			cout<<s[i]<<endl;
			for(int j=1;j<=i/2;j++){
				if(i%j==0){
					if(vis2[i-j]){
						if(num2[i-j]!=j){
							continue;
						}else{
							flag=false;
							for(int k=1;k<=j;k++){
								if(s[i-j+k]!=s[i-j-j+k]){
									flag=true;
									break;
								}
							}
							if(!flag){
								num2[i]=num2[i-j];
								vis2[i]=true;
								break;
							}
						}
					}
					if(i/j==2){
						flag=false;
						for(int k=1;k<=j;k++){
							if(s[k]!=s[k+j]){
								flag=true;
								break;
							}
						}
						if(!flag){
							vis2[i]=true;
							num2[i]=j;
							break;
						}
					}
				}
			}
		}
		for(int i=1;i<=(n-1)/2;i++)swap(vis2[i],vis2[n-i]),swap(num2[i],num2[n-i]);
		for(int i=1;i<n;i++){
//			cout<<vis2[i]<<" "<<num2[i]<<endl;
//			cout<<vis[i]<<" "<<vis2[i]<<endl;
			if(vis[i]||vis2[i])sum++;
		}
		puts("2");
		cout<<n-sum-1<<endl;
	}
	return 0;
}
