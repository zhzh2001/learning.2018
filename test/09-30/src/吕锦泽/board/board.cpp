#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define mod 1000000007
#define N 3005
#define M 200005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
struct data{ ll x,y; }a[N];
ll H,W,n,nn,inv[M],fac[M],num[N];
bool cmp(data x,data y){
	if (x.x==y.x) return x.y<y.y;
	else return x.x<y.x;
}
ll qpow(ll x,ll y){
	ll now=1;
	for (;y;y>>=1,x=x*x%mod) if (y&1) now=now*x%mod;
	return now;
}
ll C(ll n,ll m){
	return fac[n]*inv[n-m]%mod*inv[m]%mod;
}
ll get(ll x,ll y){
	if (x<y) swap(x,y);
	return C(x+y-2,y-1);
}
int main(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	H=read(); W=read(); n=read(); nn=max(H,W)<<1ll; inv[0]=fac[0]=1;
	rep(i,1,nn) fac[i]=fac[i-1]*i%mod;
	rep(i,1,nn) inv[i]=qpow(fac[i],mod-2);
	rep(i,1,n){ a[i].x=read(); a[i].y=read(); }
	a[++n]=(data){H,W};
	sort(a+1,a+1+n,cmp);
	rep(i,1,n){
		num[i]=get(a[i].x,a[i].y);
		rep(j,1,i-1) if (a[i].x>=a[j].x&&a[i].y>=a[j].y)
			num[i]=(num[i]-num[j]*get(a[i].x-a[j].x+1,a[i].y-a[j].y+1)%mod)%mod;
	}
	printf("%lld",(num[n]%mod+mod)%mod);
}
