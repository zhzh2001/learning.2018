#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define mod 1000000007
#define N 500005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
char s[N]; ll n,num,sum,nxt1[N],nxt2[N];
int main(){
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	scanf("%s",s+1); n=strlen(s+1);
	for (ll i=2,j=0;i<=n;++i){
		while (j&&s[i]!=s[j+1]) j=nxt1[j];
		if (s[i]==s[j+1]) ++j; nxt1[i]=j;
	}
	rep(i,1,n/2) swap(s[i],s[n-i+1]);
	for (ll i=2,j=0;i<=n;++i){
		while (j&&s[i]!=s[j+1]) j=nxt2[j];
		if (s[i]==s[j+1]) ++j; nxt2[i]=j;
	}
	num=n-nxt1[n];
	if (num==1) return printf("%d\n1",n)&0;
	if (n%num) return printf("1\n1")&0;
	puts("2"); rep(i,1,n-1) if ((!nxt1[i]||i%(i-nxt1[i]))&&(!nxt2[n-i]||(n-i)%(n-i-nxt2[n-i])!=0)) ++sum;
	printf("%lld\n",sum);
}
