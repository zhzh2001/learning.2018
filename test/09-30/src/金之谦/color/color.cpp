#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=3e5+10;
struct ppap{int x,y;}a[N];
inline bool cmp(ppap a,ppap b){return a.x==b.x?a.y<b.y:a.x<b.x;}
int X,Y,n,b[N],p[N],q[N];
int pre[N],nex[N];
signed main()
{
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	X=read();Y=read();n=read();
	for(int i=1;i<=n;i++){
		a[i].x=read();
		a[i].y=b[i]=read();
	}
	a[++n]=(ppap){0,0};b[n]=0;
	a[++n]=(ppap){X,Y};b[n]=Y;
	sort(a+1,a+n+1,cmp);
	sort(b+1,b+n+1);
	int cnt=unique(b+1,b+n+1)-b-1;
	p[1]=p[cnt]=1;
	int ans=0;
	for(int i=1;i<=n;i++){
		a[i].y=lower_bound(b+1,b+cnt+1,a[i].y)-b;
		int sum=0;
		for(int j=1;j<=cnt;j++)q[j]=p[j];
		for(int j=2;j<=cnt;j++){
			if(q[j-1])pre[j]=j-1;else pre[j]=pre[j-1];
		}
		for(int j=cnt-1;j;j--){
			if(q[j+1])nex[j]=j+1;else nex[j]=nex[j+1];
			sum=max(sum,b[nex[j]]-b[j]);
		}
		for(int j=1;j<i;j++){
			int t=a[j].y;
			if(a[j].x==a[i].x)continue;
			q[t]--;
			if(!q[t]){
				nex[pre[t]]=nex[t];
				pre[nex[t]]=pre[t];
				sum=max(sum,b[nex[t]]-b[pre[t]]);
			}
			ans=max(ans,2*(sum+a[i].x-a[j].x));
		}
		p[a[i].y]++;
	}
	writeln(ans);
	return 0;
}