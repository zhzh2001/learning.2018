#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int MOD=1e9+7;
const int N=3010,NN=3e5+10;
int jie[NN],inv[NN],H,W,n;
int f[N],px[N],py[N];
inline int mi(int a,int b){
	int x=1,y=a;
	for(;b;b>>=1,y=y*y%MOD)if(b&1)x=x*y%MOD;
	return x;
}
inline int C(int x,int y){return jie[x]*inv[y]%MOD*inv[x-y]%MOD;}
inline int dfs(int x){
	if(f[x]>-1)return f[x];
	f[x]=C(px[x]+py[x]-2,px[x]-1);
	for(int i=1;i<=n;i++)if(i!=x&&px[i]<=px[x]&&py[i]<=py[x]){
		int p=dfs(i);
		f[x]=(f[x]-p*C(px[x]-px[i]+py[x]-py[i],px[x]-px[i])%MOD+MOD)%MOD;
	}
	return f[x];
}
signed main()
{
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	H=read();W=read();n=read();
	memset(f,-1,sizeof f);
	
	jie[0]=1;
	for(int i=1;i<=H+W+1;i++)jie[i]=jie[i-1]*i%MOD;
	inv[H+W+1]=mi(jie[H+W+1],MOD-2);
	for(int i=H+W;i>=0;i--)inv[i]=inv[i+1]*(i+1)%MOD;
	
	for(int i=1;i<=n;i++)px[i]=read(),py[i]=read();
	px[n+1]=H;py[n+1]=W;
	writeln(dfs(n+1));
	return 0;
}
/*
f[i]:无障碍地走到每个障碍点的方案数
考虑容斥障碍点
*/
