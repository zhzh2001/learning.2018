#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=3010,MOD=1e9+7;
int H,W,n,f[N][N],b[N][N];
signed main()
{
	freopen("board.in","r",stdin);
	freopen("baoli.out","w",stdout);
	H=read();W=read();n=read();
	for(int i=1;i<=n;i++){
		int x=read(),y=read();
		b[x][y]=1;
	}
	f[1][1]=1;b[1][1]=1;
	for(int i=1;i<=H;i++)
		for(int j=1;j<=W;j++)if(!b[i][j]){
			f[i][j]=(f[i-1][j]+f[i][j-1])%MOD;
		}
	writeln(f[H][W]);
	return 0;
}