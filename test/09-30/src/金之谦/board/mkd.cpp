#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int main()
{
	freopen("board.in","w",stdout);
	srand(time(0));
	int H=rand()%3000+1,W=rand()%3000+1,n=rand()%3000+1;
	cout<<H<<' '<<W<<' '<<n<<endl;
	for(int i=1;i<=n;i++){
		int x=rand()%H+1,y=rand()%W+1;
		cout<<x<<" "<<y<<endl;
	}
	return 0;
}