#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=5e5+10;
char s[N];
int n,L,nex[N];
int main()
{
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	scanf("%s",s+1);n=strlen(s+1);
	int j=0;
	for(int i=2;i<=n;i++){
		while(j&&s[j+1]!=s[i])j=nex[j];
		if(s[j+1]==s[i])j++;
		nex[i]=j;
	}
	L=n-nex[n];
	if(n%L!=0||n==L){
		puts("1\n1");
		return 0;
	}
	if(L==1){
		writeln(n);puts("1");
		return 0;
	}
	puts("2");int ans=n-1;
	memset(nex,0,sizeof nex);
	j=0;
	for(int i=2;i<=L;i++){
		while(j&&s[j+1]!=s[i])j=nex[j];
		if(s[j+1]==s[i])j++;
		nex[i]=j;
		if(nex[i]&&i%(i-nex[i])==0)ans--;
	}
	reverse(s+1,s+L+1);
	memset(nex,0,sizeof nex);
	j=0;
	for(int i=2;i<=L;i++){
		while(j&&s[j+1]!=s[i])j=nex[j];
		if(s[j+1]==s[i])j++;
		nex[i]=j;
		if(nex[i]&&i%(i-nex[i])==0)ans--;
	}
	writeln(ans);
	return 0;
}
//ѭ����:n-border
