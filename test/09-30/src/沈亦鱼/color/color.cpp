#include<cstdio>
#include<algorithm>
using namespace std;
int h,w,n,ms,s,st,en,a[310000],b[310000],x[310000],y[310000],z[310000],l[310000],r[310000],d[310000];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void sor1(int l,int r){
	int i=l,j=r,xx=a[(l+r)>>1];
	while(i<=j){
		while(a[i]<xx)i++;
		while(xx<a[j])j--;
		if(i<=j){
			swap(a[i],a[j]);
			swap(b[i],b[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor1(l,j);
	if(i<r)sor1(i,r);
}
void sor2(int l,int r){
	int i=l,j=r,xx=y[(l+r)>>1];
	while(i<=j){
		while(y[i]<xx)i++;
		while(xx<y[j])j--;
		if(i<=j){
			swap(x[i],x[j]);
			swap(y[i],y[j]);
			swap(z[i],z[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor2(l,j);
	if(i<r)sor2(i,r);
}
int main(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	h=read();
	w=read();
	n=read();
	for(int i=1;i<=n;i++){
		x[i]=read();
		y[i]=read();
		z[i]=i;
		a[i]=x[i];
		b[i]=i;
	}
	sor1(1,n);
	sor2(1,n);
	s=0;
	for(int i=0;i<=n;i++){
		if(i<n){
			if(a[i]==a[i+1])continue;
		}
		st=0;
		en=n+1;
		ms=0;
		for(int j=1;j<=n;j++)
			if(x[j]>a[i]){
				ms=max(ms,y[j]-d[st]);
				r[st]=z[j];
				l[z[j]]=st;
				st=z[j];
				d[st]=y[j];
			}
		r[st]=en;
		l[en]=st;
		d[en]=w;
		s=max(s,ms+h-a[i]);
		for(int j=n;j>i;j--){
			ms=max(ms,d[r[b[j]]]-d[l[b[j]]]);
			s=max(s,a[j]-a[i]+ms);
			l[r[b[j]]]=l[b[j]];
			r[l[b[j]]]=r[b[j]];
		}
	}
	printf("%d",s*2);
	return 0;
}
