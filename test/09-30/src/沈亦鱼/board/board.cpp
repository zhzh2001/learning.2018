#include<cstdio>
using namespace std;
int mo=1000000007,h,w,t,n,u,tot,a[3100],b[3100],d[3100],p[4500000],ne[4500000],he[3100],r[3100];
long long fac[210000],ni[210000],ta[4500000],s[3100];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
int gcd(int x,int y){
	if(!y)return x;
	else return gcd(y,x%y);
}
inline long long pwr(long long a,int b){
	long long c=1;
	while(b>0){
		if(b&1)c=c*a%mo;
		a=a*a%mo;
		b>>=1;
	}
	return c;
}
inline long long getc(int a,int b){
	return a>=b?(long long)fac[a]*ni[b]%mo*ni[a-b]%mo:0;
}
void adg(int u,int v){
	tot++;
	p[tot]=v;
	ta[tot]=getc(a[v]-a[u]+b[v]-b[u],a[v]-a[u]);
	ne[tot]=he[u];
	he[u]=tot;
}
int main(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	fac[0]=1;
	for(int i=1;i<=200000;i++)
		fac[i]=(long long)fac[i-1]*i%mo;
	ni[200000]=pwr(fac[200000],mo-2);
	for(int i=199999;i>=0;i--)
		ni[i]=(long long)ni[i+1]*(i+1)%mo;
	h=read();
	w=read();
	n=read();
	a[1]=1;
	b[1]=1;
	for(int i=2;i<=n+1;i++){
		a[i]=read();
		b[i]=read();
	}
	a[n+2]=h;
	b[n+2]=w;
	for(int i=1;i<=n+2;i++)
		for(int j=1;j<=n+2;j++)
			if(i!=j&&a[i]<=a[j]&&b[i]<=b[j]){
				adg(i,j);
				r[j]++;//printf("%d\n",tot);
			}
	h=0;
	t=1;
	d[1]=1;
	s[1]=-1;
	while(h<t){
		h++;
		u=d[h];
		for(int i=he[u];i;i=ne[i]){
			r[p[i]]--;
			s[p[i]]=(s[p[i]]-s[u]*ta[i]%mo)%mo;//printf("%d %d\n",p[i],s[p[i]]);
			if(r[p[i]]==0){
				t++;
				d[t]=p[i];
			}
		}
	}
	if(s[n+2]<0)s[n+2]+=mo;
	printf("%lld",s[n+2]);
	return 0;
}
