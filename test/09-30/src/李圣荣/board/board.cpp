#include<bits/stdc++.h>
using namespace std;
#define int long long
const int mod=1e9+7;
int f[3005][3005],a[3005][3005];
int dp[500050],jc[500050],inv[500050];
int C(int x,int y){
//	cout<<x<<" "<<y<<endl;
	if(x<0||y<0) return 0;
	if(x==y) return 1;
	return jc[y]*inv[x]%mod*inv[y-x]%mod;
}
signed main(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	int h,w,n; cin>>h>>w>>n; int minx=1e9; int maxy=0;
	for(int i=1;i<=n;i++){
		int x,y; cin>>x>>y;
		minx=min(minx,x); maxy=max(maxy,y);
		a[x][y]=1;
	}
	//cout<<minx<<" "<<maxy<<endl;
	if(h<=3000&&w<=3000){
		f[1][1]=1;
		for(int i=1;i<=h;i++){
			for(int j=1;j<=w;j++){
				if(i==1&&j==1||a[i][j]) continue;
				f[i][j]=(f[i-1][j]*(a[i-1][j]!=1)+f[i][j-1]*(a[i][j-1]!=1))%mod;
			}
		}
		cout<<f[h][w]%mod;	 return 0;
	}else{
		jc[0]=1;
		for(int i=1;i<=max(h,w);i++) jc[i]=jc[i-1]*i%mod;
		inv[1]=1;
		for(int i=2;i<=max(h,w);++i)inv[i]=inv[mod%i]*(mod-mod/i)%mod;
		for(int i=1;i<minx;i++) dp[i]=C(i-1,i-1+maxy-1);//cout<<111<<endl;
		for(int i=minx;i<=h;i++) dp[i]=dp[minx-1];
		int ans=0;
		for(int i=1;i<=h;i++){
			ans+=dp[i]*C(h-i,h-i+w-maxy-1);
			ans%=mod;
		}
		cout<<ans<<endl;
	}
}
