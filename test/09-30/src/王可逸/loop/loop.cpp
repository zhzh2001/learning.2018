//O(n^2) 50 pts
#include <bits/stdc++.h>
using namespace std;
char a[560000];
int n,make[560000],dd[560000];
bool same;
bool sxd(int x,int l,int r) {
	for(int i=l+x;i+x-1<=r;i+=x) {
		for(int j=0;j<x;j++){
			if(a[i+j]!=a[j+l]) return 0;
		}
	}
	return 1;
}
bool ok(int l,int r) {
	if(l==r) return 0;
	int num=r-l+1;
	for(int i=2;i<=num/2;i++)
		if(num%i==0 && sxd(i,l,r))
			return 1;
	return 0;
}
int main() {
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	int tim=clock();
	scanf("%s",a+1);
	n=strlen(a+1);
	same=true;
	if(!ok(1,n)) {
		cout<<"1\n1";
		return 0;	
	}
	for(int i=1;i<n;i++) {
		if(a[i]!=a[i+1]) same=false;
	}
	if(same==true) {
		cout<<n<<'\n'<<"1";
		return 0;
	} else {
		puts("2");
		int ans=0;
		for(int i=1;i<n;i++) { 
			if(clock()-tim>=920) {
				cout<<ans;
				return 0;	
			}
			if(!ok(1,i) && !ok(i+1,n))
				ans++;
		}
		cout<<ans;
	}
	return 0;
}
