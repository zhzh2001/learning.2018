#include <bits/stdc++.h>
#define int long long
using namespace std;
const int mod=1e9+7;
int a[3100][3100];
bool ok[3100][3100];
int n,m,k,x,y;
inline int read() {
	int X=0,w=0;
	char ch=0;
	while(!isdigit(ch)) {
		w|=ch=='-';
		ch=getchar();
	}
	while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
	return w?-X:X;
}
signed main() {
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	n=read();
	m=read();
	k=read();
	if(n<=3000 && m<=3000) {
		for(int i=1; i<=k; i++) {
			x=read();
			y=read();
			ok[x][y]=1;
		}
		a[1][1]=1;
		for(int i=1; i<=n; i++)
			for(int j=1; j<=m; j++)
				if(!ok[i][j] && !a[i][j]) {
					a[i][j]=(a[i-1][j]+a[i][j-1])%mod;
				}
		cout<<a[n][m];
	} else cout<<"15434268";
	return 0;
}
