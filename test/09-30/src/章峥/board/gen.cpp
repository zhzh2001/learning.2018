#include<fstream>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("board.in");
const int n=3000;
bool vis[n+5][n+5];
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<' '<<n<<' '<<n<<endl;
	for(int i=1;i<=n;)
	{
		uniform_int_distribution<> d(1,n);
		int x=d(gen),y=d(gen);
		if(!vis[x][y])
		{
			fout<<x<<' '<<y<<endl;
			vis[x][y]=true;
			i++;
		}
	}
	return 0;
}
