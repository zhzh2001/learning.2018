#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("board.in");
ofstream fout("board.out");
const int N=3005,M=200000,MOD=1e9+7;
int h,w,n,fact[M+5],inv[M+5],f[N];
pair<int,int> p[N];
int qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}while(b/=2);
	return ans;
}
inline int c(int n,int m)
{
	return 1ll*fact[n]*inv[n-m]%MOD*inv[m]%MOD;
}
int dp(int i)
{
	if(~f[i])
		return f[i];
	f[i]=c(p[i].first+p[i].second-2,p[i].first-1);
	for(int j=1;j<=n;j++)
		if(j!=i&&p[j].first<=p[i].first&&p[j].second<=p[i].second)
			f[i]=(f[i]-1ll*dp(j)*c(p[i].first+p[i].second-p[j].first-p[j].second,p[i].first-p[j].first)%MOD+MOD)%MOD;
	return f[i];
}
int main()
{
	fact[0]=1;
	for(int i=1;i<=M;i++)
		fact[i]=1ll*fact[i-1]*i%MOD;
	inv[M]=qpow(fact[M],MOD-2);
	for(int i=M-1;i>=0;i--)
		inv[i]=1ll*inv[i+1]*(i+1)%MOD;
	fin>>h>>w>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>p[i].first>>p[i].second;
		if((p[i].first==1&&p[i].second==1)||(p[i].first==h&&p[i].second==w))
		{
			fout<<0<<endl;
			return 0;
		}
	}
	p[++n]=make_pair(h,w);
	fill(f+1,f+n+1,-1);
	fout<<dp(n)<<endl;
	return 0;
}
