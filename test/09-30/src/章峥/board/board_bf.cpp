#include<fstream>
using namespace std;
ifstream fin("board.in");
ofstream fout("board.ans");
const int N=5005,MOD=1e9+7;
int f[N][N];
int main()
{
	int h,w,n;
	fin>>h>>w>>n;
	for(int i=1;i<=n;i++)
	{
		int x,y;
		fin>>x>>y;
		f[x][y]=-1;
		if(x==1&&y==1)
		{
			fout<<0<<endl;
			return 0;
		}
	}
	f[1][1]=1;
	for(int i=1;i<=h;i++)
		for(int j=1;j<=w;j++)
			if(!f[i][j])
				f[i][j]=(f[i-1][j]+f[i][j-1])%MOD;
			else if(f[i][j]==-1)
				f[i][j]=0;
	fout<<f[h][w]<<endl;
	return 0;
}
