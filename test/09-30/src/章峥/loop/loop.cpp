#include<fstream>
#include<string>
using namespace std;
ifstream fin("loop.in");
ofstream fout("loop.out");
const int N=500005,B=29;
typedef long long hash_t;
hash_t h[N],p[N];
bool f[N],g[N];
int loop(int l,int r)
{
	for(int lp=1;lp<=r-l+1;lp++)
	{
		if((r-l+1)%lp)
			continue;
		bool flag=true;
		hash_t init=h[l+lp-1]-h[l-1]*p[lp];
		for(int i=l+lp*2-1;i<=r;i+=lp)
			if(h[i]-h[i-lp]*p[lp]!=init)
			{
				flag=false;
				break;
			}
		if(flag)
			return lp;
	}
	return -1;
}
int main()
{
	string s;
	fin>>s;
	if(s==string(s.length(),s[0]))
	{
		fout<<s.length()<<endl<<1<<endl;
		return 0;
	}
	int len=s.length();
	s=' '+s;
	p[0]=1;
	for(int i=1;i<=len;i++)
	{
		h[i]=h[i-1]*B+s[i]-'a';
		p[i]=p[i-1]*B;
	}
	int lp=loop(1,len);
	if(lp==len)
		fout<<1<<endl<<1<<endl;
	else
	{
		int ans=0;
		for(int i=2;i<=len;i+=2)
			if(!f[i]&&h[i/2]==h[i]-h[i/2]*p[i/2])
				for(int j=i;j<=len;j+=i/2)
				{
					if(h[j]-h[j-i/2]*p[i/2]!=h[i/2])
						break;
					f[j]=true;
				}
		for(int i=len-1;i>=2;i-=2)
		{
			int tl=len-i+1;
			if(!g[i]&&h[i+tl/2-1]-h[i-1]*p[tl/2]==h[len]-h[i+tl/2-1]*p[tl/2])
				for(int j=i;j>=1;j-=tl/2)
				{
					if(h[j+tl/2-1]-h[j-1]*p[tl/2]!=h[len]-h[i+tl/2-1]*p[tl/2])
						break;
					g[j]=true;
				}
		}
		for(int i=1;i<len;i++)
			ans+=!f[i]&&!g[i+1];
		fout<<2<<endl<<ans<<endl;
	}
	return 0;
}
