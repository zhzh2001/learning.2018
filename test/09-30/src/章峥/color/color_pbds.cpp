#include<fstream>
#include<algorithm>
#include<ext/pb_ds/tree_policy.hpp>
#include<ext/pb_ds/assoc_container.hpp>
using namespace std;
ifstream fin("color.in");
ofstream fout("color.out");
const int N=3005;
pair<int,int> p[N];
int main()
{
	int x,y,n;
	fin>>x>>y>>n;
	for(int i=1;i<=n;i++)
		fin>>p[i].first>>p[i].second;
	sort(p+1,p+n+1);
	p[0]=make_pair(0,0);
	p[++n]=make_pair(x,y);
	int ans=0;
	for(int i=0;i<=n;i++)
	{
		__gnu_pbds::tree<pair<int,int>,__gnu_pbds::null_type> point,len;
		point.insert(make_pair(0,-1));point.insert(make_pair(y,x+1));
		len.insert(make_pair(y,0));
		for(int j=i+1;j<=n;j++)
		{
			ans=max(ans,(p[j].first-p[i].first+len.rbegin()->first)*2);
			__gnu_pbds::tree<pair<int,int>,__gnu_pbds::null_type>::iterator it1=point.insert(make_pair(p[j].second,p[j].first)).first,it2=it1;
			--it1;++it2;
			len.erase(len.lower_bound(make_pair(it2->first-it1->first,-1e9)));
			len.insert(make_pair(p[j].second-it1->first,-j));len.insert(make_pair(it2->first-p[j].second,j));
		}
	}
	fout<<ans<<endl;
	return 0;
}
