#include<fstream>
#include<algorithm>
#include<set>
using namespace std;
ifstream fin("color.in");
ofstream fout("color.out");
const int N=300005;
pair<int,int> p[N];
int main()
{
	int x,y,n;
	fin>>x>>y>>n;
	for(int i=1;i<=n;i++)
		fin>>p[i].first>>p[i].second;
	sort(p+1,p+n+1);
	p[0]=make_pair(0,0);
	p[++n]=make_pair(x,y);
	int ans=0;
	for(int i=0;i<=n;i++)
	{
		set<pair<int,int> > point;
		multiset<int> len;
		point.insert(make_pair(0,-1));point.insert(make_pair(y,x+1));
		len.insert(y);
		for(int j=i+1;j<=n;j++)
		{
			ans=max(ans,(p[j].first-p[i].first+*len.rbegin())*2);
			set<pair<int,int> >::iterator it1=point.insert(make_pair(p[j].second,p[j].first)).first,it2=it1;
			--it1;++it2;
			len.erase(len.find(it2->first-it1->first));
			len.insert(p[j].second-it1->first);len.insert(it2->first-p[j].second);
		}
		if(i*n>1e6&&n>3000)
			break;
	}
	fout<<ans<<endl;
	return 0;
}
