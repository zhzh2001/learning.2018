#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("color.in");
ofstream fout("color.ans");
const int N=1005;
pair<int,int> p[N];
int x[N],y[N];
int main()
{
	int h,w,n;
	fin>>h>>w>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>p[i].first>>p[i].second;
		x[i]=p[i].first;
		y[i]=p[i].second;
	}
	sort(x+1,x+n+1);
	sort(y+1,y+n+1);
	x[0]=0;x[++n]=h;
	y[0]=0;y[n]=w;
	int ans=0;
	for(int xl=0;xl<=n;xl++)
		for(int xr=xl+1;xr<=n;xr++)
			for(int yl=0;yl<=n;yl++)
				for(int yr=yl+1;yr<=n;yr++)
				{
					bool flag=true;
					for(int i=1;i<n;i++)
						if(!(p[i].first<=x[xl]||p[i].first>=x[xr]||p[i].second<=y[yl]||p[i].second>=y[yr]))
						{
							flag=false;
							break;
						}
					if(flag)
						ans=max(ans,(x[xr]-x[xl]+y[yr]-y[yl])*2);
				}
	fout<<ans<<endl;
	return 0;
}
