#include<fstream>
#include<random>
#include<ctime>
#include<set>
using namespace std;
ofstream fout("color.in");
const int n=3000,m=233333;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<m<<' '<<m<<' '<<n<<endl;
	set<pair<int,int> > S;
	for(int i=1;i<=n;)
	{
		uniform_int_distribution<> d(0,m);
		int x=d(gen),y=d(gen);
		if(S.find(make_pair(x,y))==S.end())
		{
			S.insert(make_pair(x,y));
			fout<<x<<' '<<y<<endl;
			i++;
		}
	}
	return 0;
}
