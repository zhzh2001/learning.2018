#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
typedef pair<int,int> pii;
#define fi first
#define se second
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define gc getchar
#define debug(x) printf(#x" = %lld\n",x);
#define pc putchar
inline ll rd(){
	ll x=0,f=1;char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
inline int ran(){
	return rand() << 15 | rand();
}
int W,H,n;
int main(){
	freopen("board.in","w",stdout);
	srand(time(NULL));
	W = 2000,H = 2000,n = ran()%50;
	printf("%d %d %d\n",W,H,n);
	int x,y;
	Rep(i,1,n){
		x=ran()%W+1,y=ran()%H+1;
		if(x==1&&y==1||x==W&&y==H){
			i--;
			continue;
		}
		printf("%d %d\n",x,y);
	}
	return 0;
}
