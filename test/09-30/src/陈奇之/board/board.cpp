#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
typedef pair<int,int> pii;
#define fi first
#define se second
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define gc getchar
#define debug(x) printf(#x" = %lld\n",x);
#define pc putchar
inline ll rd(){
	ll x=0,f=1;char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 2e5+233;
const int mod = 1e9+7;
int fac[maxn],inv[maxn];
inline int qpow(int a,int b){
	int ans = 1;
	for(;b;b>>=1,a=1ll*a*a%mod)
		if(b&1) ans=1ll*ans*a%mod;
	return ans;
}
inline int C(int n,int m){
	return 1ll * fac[n] * inv[m] % mod * inv[n-m] % mod;
}
void init(int n){
	fac[0] = 1;
	Rep(i,1,n) fac[i] = 1ll * fac[i-1] * i % mod;
	inv[n] = qpow(fac[n],mod-2);
	Dep(i,n-1,0) inv[i] = 1ll * inv[i+1] * (i+1) % mod;
}
#define y1 __y1
inline int calc(int x1,int y1,int x2,int y2){
	int x = x2-x1,y = y2-y1;
	return C(x+y,x);
}
int H,W,n;
int f[maxn];
pii a[maxn];
int main(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	init(200000);
	H = rd(),W = rd(),n = rd();
	Rep(i,1,n){
		a[i] . fi = rd(),a[i] . se = rd();
	}
	sort(a+1,a+1+n);
	a[0] = pii(1,1);
	a[n+1] = pii(H,W);
	
	f[0] = mod-1;
	for(int i=1;i<=n+1;++i){
		f[i] = 0;
		for(int j=0;j<i;++j)if(a[i].fi >= a[j].fi && a[i].se >= a[j].se){
			f[i] = (f[i] - 1ll * calc(a[j].fi,a[j].se,a[i].fi,a[i].se) * f[j] % mod + mod) % mod;
		}
	}
	writeln(f[n+1]);
	return 0;
}
