#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
typedef pair<int,int> pii;
#define fi first
#define se second
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define gc getchar
#define debug(x) printf(#x" = %lld\n",x);
#define pc putchar
inline ll rd(){
	ll x=0,f=1;char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 2e5+233;
const int mod = 1e9+7;
int f[2005][2005];
bool mark[2005][2005];
int H,W,n;
int main(){
	freopen("board.in","r",stdin);
	freopen("bf.out","w",stdout);
	H = rd(),W = rd(),n = rd();
	Rep(i,1,n){
		int x=rd(),y=rd();
		mark[x][y] = true;
	}
	f[1][1] = 1;
	Rep(i,1,H){
		Rep(j,1,W){if(i==1&&j==1)continue;
			f[i][j] = (f[i-1][j] + f[i][j-1])% mod;
			if(mark[i][j]) f[i][j] = 0;
		}
	}
	writeln(f[H][W]);
	return 0;
}
