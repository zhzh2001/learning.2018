#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define gc getchar
#define debug(x) printf(#x" = %lld\n",x);
#define pc putchar
inline ll rd(){
	ll x=0,f=1;char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 5e5+233;
int nxts[maxn],nxtt[maxn],n;
char s[maxn],t[maxn];
bool pre[maxn],suf[maxn];
void kmp(char s[],int nxt[]){
	nxt[0] = 0;
	for(int i=1,k=0;i<=n;++i){
		while(k && s[i]!=s[k]) k=nxt[k-1];
		if(s[i]==s[k])++k;
		nxt[i]=k;
	}
}
int main(){
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	bool flag = true;
	scanf("%s",s);n = strlen(s);
	rep(i,0,n-1){
		if(s[i] != s[i+1]) flag = false;
	}
	if(flag){
		writeln(n);//必须全部分开 
		puts("1");
		return 0;
	}
	rep(i,0,n) t[i] = s[i];
	kmp(s,nxts);
	reverse(t,t+n);
	kmp(t,nxtt);
	reverse(nxtt,nxtt+n);
	memset(pre,false,sizeof(pre));
	memset(suf,false,sizeof(suf));
//	rep(i,0,n){
//		printf("pre[%d] = %d\n",i,nxts[i]);
//		printf("suf[%d] = %d\n",i,nxtt[i]);
//	}
	pre[0] = true;
	suf[n-1] = true;
	rep(i,0,n){
		if(nxts[i]==0 || (i+1) % ((i+1) - nxts[i]) != 0) pre[i] = true;
		if(nxtt[i]==0 || (n-i) % ((n-i) - nxtt[i]) != 0) suf[i] = true;
	}
//	rep(i,0,n){
//		printf("%d %d\n",pre[i],suf[i]);
//	}
//	
	if(pre[n-1]){
		puts("1");
		puts("1");
		return 0;
	}//整个串 
	puts("2");
	int ans = 0;
	rep(i,0,n-1){
		if(pre[i] && suf[i+1]) ans++;
	}
	writeln(ans);
	return 0;
}
