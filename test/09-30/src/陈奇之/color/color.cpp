#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
typedef pair<int,int> pii;
#define fi first
#define se second
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define gc getchar
#define debug(x) printf(#x" = %lld\n",x);
#define pc putchar
inline ll rd(){
	ll x=0,f=1;char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
inline void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
const int maxn = 3e5+233;
int T[maxn<<2],tag[maxn<<2];
int X,Y,n,x[maxn],y[maxn],h[maxn],L[maxn],R[maxn];
#define lson (o<<1)
#define rson (o<<1|1)
#define mid ((l+r)>>1)
inline void modify(int o,int l,int r,int x,int y,int v){//区间加
	if(l==x&&r==y){
		tag[o]+=v;
		T[o]+=v;
		return ;
	}
	if(y<=mid) modify(lson,l,mid,x,y,v); else
	if(mid+1<=x) modify(rson,mid+1,r,x,y,v); else
	modify(lson,l,mid,x,mid,v),modify(rson,mid+1,r,mid+1,y,v);	
	T[o] = max(T[lson],T[rson]) + tag[o];
}

inline void build(int o,int l,int r){
	T[o]=tag[o]=0;
	if(l==r) return;
	build(lson,l,mid);
	build(rson,mid+1,r);
}
#undef mid
#undef lson
#undef rson
//#define modify(l,r,v) modify(1,1,n,h[0],l,r,v)
inline void modify(int l,int r,int v){
//	printf("modify(%d %d %d)\n",l,r,v);
	modify(1,1,h[0],l,r,v);
}
int topl,topr,sl[maxn],sr[maxn];
inline int solve(){
	int mid = (X+0) / 2;
	h[0] = 0;
	Rep(i,1,n){
		h[++h[0]] = y[i];
	}
	h[++h[0]] = 0;
	h[++h[0]] = Y;
	sort(h+1,h+1+h[0]);
	h[0] = unique(h+1,h+1+h[0]) - h - 1;
	Rep(i,1,h[0]) L[i] = mid - 0,R[i] = X - mid; 
	Rep(i,1,n){
		y[i] = lower_bound(h+1,h+1+h[0],y[i]) - h;
		if(x[i] <= mid)
			L[y[i]] = min(L[y[i]],mid - x[i]);
		else 
			R[y[i]] = min(R[y[i]],x[i] - mid);
	}
//	Rep(i,1,h[0]){
//		printf("[%d(%d)] : {%d %d}\n",i,h[i],L[i],R[i]);
//	}
	int ans = 0;
	topl = topr = 0;
	//L[0] = R[0] = 0;
	//sl[++topl] = 0;
	//sr[++topr] = 0;
	build(1,1,h[0]);
	sl[0] = sr[0] = 1;
	sl[++topl] = 1,sr[++topr] = 1;
	modify(1,1,2*X-2*h[1]);
	
	Rep(i,2,h[0]){
		ans = max(ans,2 * h[i] + T[1]);
		int tmp = sl[topl];
		if(L[i] < L[sl[topl]]){
			while(topl && L[sl[topl]] > L[i]){
				int ml = sl[topl-1];
				int mr = sl[topl]-1;
				if(ml <= mr) modify(ml,mr,-2*(L[sl[topl]] - L[i]));
				topl--;
			}
		}
		if(tmp<=i-1) modify(tmp,i-1,-2*(mid-0-L[i]));
		sl[++topl] = i;
		
		tmp = sr[topr];
		if(R[i] < R[sr[topr]]){
			int tmp = sr[topr];
			while(topr && R[sr[topr]] > R[i]){
				int ml = sr[topr-1];
				int mr = sr[topr]-1;
				if(ml <= mr) modify(ml,mr,-2*(R[sr[topr]] - R[i]));
				topr--;
			}
		}
		if(tmp<=i-1) modify(tmp,i-1,-2*(X-mid-R[i]));
		sr[++topr] = i;
		modify(i,i,2*X-2*h[i]);
		//上面的修改不能修改i-1这个位置 
	}
	//还原
	Rep(i,1,n) y[i] = h[y[i]]; 
//	printf("ans = %d\n",ans);
	return ans;
}

int ans;
int main(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	X = rd(),Y = rd(),n = rd();
	Rep(i,1,n){
		x[i] = rd(),y[i] = rd();
	}
	ans = 2 * max(X,Y) + 2;
	ans = max(ans,solve());
	Rep(i,1,n){
		swap(x[i],y[i]);
	}swap(X,Y);
	ans = max(ans,solve());
	writeln(ans);
	return 0;
}
/*

*/
