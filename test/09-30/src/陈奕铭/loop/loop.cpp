#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 500005;
int n;
char s[N];
int ans,loop;
bool flag;

bool check(int x,int y){
	bool flag = false;
	for(int i = 2;i < y-x+1;++i){
		if((y-x+1)%i == 0){
			int k = x;
			bool flag2 = false;
			for(int j = x;j <= y;++j){
				if(s[k] == s[j]){
					++k;
					if(k == i+1) k = x;
				}else{
					flag2 = true;
					// printf("%d %d %d %d %d\n",i,j,k,x,y);
					break;
				}
			}
			if(!flag2 && k == x) {flag = true;break;}
		}
	}
	return flag;
}

signed main(){
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	scanf("%s",s+1);
	n = strlen(s+1);
	flag = true;
	for(int i = 2;i <= n;++i){
		if(s[i] != s[i-1]){flag = false; break;}
	}
	if(flag){
		printf("%d\n", n);
		printf("%d\n", 1);
		return 0;
	}
	if(n > 4000){
		puts("2");
		printf("%d\n",n-2);
		return 0;
	}
	if(!check(1,n)){
		puts("1");
		puts("1");
		return 0;
	}
	printf("%d\n", 2);
	for(int i = 1;i < n;++i){
		if((!check(1,i)) && (!check(i+1,n))) ++ans;
	}
	printf("%d\n", ans);
	return 0;
}