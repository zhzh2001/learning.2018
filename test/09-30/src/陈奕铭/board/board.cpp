#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005,M = 3005,mod = 1e9+7;
int H,W,n;
ll dp[M];
ll sum[N*2],rev[N*2];
struct node{
	ll x,y;
	friend bool operator <(node a,node b){
		if(a.x != b.x) return a.x < b.x;
		return a.y < b.y;
	}
}a[M];

inline ll qpow(ll x,ll n){
	ll ans = 1;
	while(n){
		if(n&1) ans = ans*x%mod;
		x = x*x%mod;
		n >>= 1;
	}
	return ans;
}
inline ll C(ll n,ll m){
	return sum[n]*(rev[m]*rev[n-m]%mod)%mod;
}

signed main(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	sum[0] = 1; for(ll i = 1;i <= 200000;++i) sum[i] = sum[i-1]*i%mod;
	rev[200000] = qpow(sum[200000],mod-2); for(ll i = 199999;i >= 0;--i) rev[i] = rev[i+1]*(i+1)%mod;
	H = read(); W = read(); n = read();
	for(int i = 1;i <= n;++i) a[i].x = read(),a[i].y = read();
	sort(a+1,a+n+1);
	a[n+1].x = H; a[n+1].y = W;
	for(int i = 1;i <= n+1;++i){
		ll sum = C(a[i].x+a[i].y-2,a[i].x-1);
		// printf("# %d %lld\n",i,sum);
		for(int j = 1;j < i;++j)
			if(a[j].y <= a[i].y)
				sum = (sum-dp[j]*C(a[i].x+a[i].y-a[j].x-a[j].y,a[i].x-a[j].x)%mod)%mod;
		dp[i] = (sum%mod+mod)%mod;
	}
	// for(int i = 1;i <= n;++i) printf("%d %d\n", a[i].x,a[i].y);
	// printf("@ %lld\n", C(1,0));
	// for(int i = 1;i <= n;++i) printf("%lld\n",dp[i]);
	printf("%lld\n", (dp[n+1]%mod+mod)%mod);
	return 0;
}