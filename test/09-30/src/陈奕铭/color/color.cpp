#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
const int N = 3005;
int X,Y,n;
int ans;
struct node{
	int x,y;
	friend bool operator<(node a,node b){
		if(a.y == b.y) return a.x < b.x;
		return a.y < b.y;
	}
}a[N];
multiset<int> S;

signed main(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	X = read(); Y = read(); n = read();
	for(int i = 1;i <= n;++i) a[i].x = read(),a[i].y = read();
	sort(a+1,a+n+1);
	a[0].y = 0;
	S.insert(0); S.insert(X);
	for(int i = 0;i <= n;++i){
		for(int j = i+1;j <= n;++j) S.insert(a[j].x);
		int Mx = 0;
		multiset<int> ::iterator it1 = S.begin(),it2 = S.begin();
		it2++;
		while(it1 != S.end()){
			max(Mx,*it2 - *it1);
			it1++; it2++;
		}
		ans = max(ans,Y-a[i].y+Mx);
		for(int j = n;j >= i+1;--j){
			S.erase(S.find(a[j].x));
			if(S.count(a[j].x) == 0){
				it1 = S.lower_bound(a[j].x);
				it2 = it1;
				it1--;
				Mx = max(Mx,*it2-*it1);
			}
			ans = max(ans,Mx+a[j].y-a[i].y);
		}
	}
	printf("%d\n", ans*2);
	return 0;
}
