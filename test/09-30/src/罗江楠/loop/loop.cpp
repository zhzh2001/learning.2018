#include <bits/stdc++.h>
#define N 500020
#define ll long long
#define mod 1000000007
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
char ch[N];
/*
int hsh[N];
ll fast_pow(ll x, ll y) {
  ll z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}
bool same(int l, int r, int x, int y) {
  if (y - x != r - l) return false;
  int hsh1 = (hsh[r] - hsh[l - 1] * fast_pow(19260817ll, r - l + 1) % mod + mod) % mod;
  int hsh2 = (hsh[y] - hsh[x - 1] * fast_pow(19260817ll, y - x + 1) % mod + mod) % mod;
  return hsh1 == hsh2;
}*/
int nxt[N], nxt2[N];
/*
void kmp(int start, int n) {
  for (int i = start + 1, j = start - 1; i <= n; ++ i){
    while (j >= start && ch[i] != ch[j + 1]) j = nxt[j];
    if(ch[i] == ch[j + 1]) ++ j; nxt[i] = j - (start - 1);
  }
}
*/
int main(int argc, char const *argv[]) {
  freopen("loop.in", "r", stdin);
  freopen("loop.out", "w", stdout);
  scanf("%s", ch + 1);
  int n = strlen(ch + 1);
  bool is_all_equals = true;
  for (int i = 2; i <= n; ++ i) {
    // hsh[i] = ch[i] - 'a' + 1;
    // hsh[i] = (hsh[i - 1] * 19260817ll % mod + hsh[i - 1]) % mod;
    is_all_equals &= ch[i] == ch[i - 1];
  }

  if (is_all_equals) {
    return printf("%d\n1\n", n), 0;
  }

  for (int i = 2, j = 0; i <= n; ++ i){
    while (j && ch[i] != ch[j + 1]) j = nxt[j];
    if (ch[i] == ch[j + 1]) ++ j; nxt[i] = j;
  }

  if (nxt[n] && nxt[n] % (n - nxt[n]) == 0) {
    puts("2");
    reverse(ch + 1, ch + n + 1);

    for (int i = 2, j = 0; i <= n; ++ i){
      while (j && ch[i] != ch[j + 1]) j = nxt2[j];
      if (ch[i] == ch[j + 1]) ++ j; nxt2[i] = j;
    }

    int ans = 0;
    for (int i = 1; i < n; ++ i) {
      // [1, i] [i + 1, n]
      bool left = nxt[i] && nxt[i] % (i - nxt[i]) == 0;
      bool right = nxt2[n-i] && nxt2[n-i] % ((n-i) - nxt2[n-i]) == 0;
      ans += !left && !right;
    }
    printf("%d\n", ans);
  } else {
    puts("1\n1");
  }


  // calc k
  /*
  int now = 1, k = 0;
  while (now < n) {
    kmp(now, n);
    for (int i = n; i > now; -- i) {
      printf("%d %d %d\n", now, i, nxt[i]);
      // 有循环节
      if (nxt[i] && nxt[i] % (i - (now - 1) - nxt[i]) == 0) {
        continue;
      }
      now = i;
      break;
    }
    ++ k;
  }

  printf("%d\n", k);

  // calc ans

  // for (int i = 1; i <= k; ++ i) {

  // }

  puts("1");
  */
  return 0;
}
/*
timber is akking
*/