#include <bits/stdc++.h>
#define N 200020
#define ll long long
#define mod 1000000007
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}


ll frac[N];
ll fast_pow(ll x, ll y) {
  ll z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}
ll inv(ll x) {
  return fast_pow(x, mod - 2);
}
// choose n from m
ll C(ll m, ll n) {
  return frac[m] * inv(frac[n]) % mod * inv(frac[m - n]) % mod;
}
ll R(ll h, ll w) {
  return C(h + w - 2, h - 1);
}

struct node {
  int x, y;
};
bool cmp(const node &a, const node &b) {
  return a.x == b.x ? a.y < b.y : a.x < b.x;
}
node a[3020];

int f[N];

int main(int argc, char const *argv[]) {
  freopen("board.in", "r", stdin);
  // freopen("board.out", "w", stdout);
  for (int i = frac[0] = 1; i <= 2e5; ++ i) {
    frac[i] = frac[i - 1] * i % mod;
  }
  // printf("%lld\n", R(99970, 100000) % mod * R(99900, 30) % mod);

  int h = read(), w = read(), n = read();

  a[0].x = 1;
  a[0].y = 1;

  for (int i = 1; i <= n; ++ i) {
    a[i].x = read();
    a[i].y = read();
  }

  sort(a, a + n + 1, cmp);
  int now = 0;
  f[1] = 1;

  for (int i = 1; i <= h; ++ i) {
    for (int j = 1; j <= w; ++ j) {
      if (a[now].x == i && a[now].y == j) {
        ++ now;
        if (i + j > 2) {
          f[j] = 0;
        }
      } else {
        f[j] = (f[j] + f[j - 1]) % mod;
      }
    }
  }

  printf("%d\n", f[w]);

  return 0;
}