#include <bits/stdc++.h>
#define N 100020
#define ll long long
#define mod 1000000007
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}


ll frac[N];
ll fast_pow(ll x, ll y) {
  ll z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}
ll inv(ll x) {
  return fast_pow(x, mod - 2);
}
// choose n from m
ll C(ll m, ll n) {
  return frac[m] * inv(frac[n]) % mod * inv(frac[m - n]) % mod;
}


// unique
int a[N<<1], topa;
int b[N<<1], topb;


bool mp[3020][3020];
int wlen[3020], hlen[3020];

int x[3020], y[3020];
int f[3020][3020];

int main(int argc, char const *argv[]) {
  // freopen("board.in", "r", stdin);
  // freopen("board.out", "w", stdout);
  for (int i = frac[0] = 1; i <= 1e5; ++ i) {
    frac[i] = frac[i - 1] * i % mod;
  }
  // printf("%d\n", C(5, 2));

  int h = read(), w = read(), n = read();


  a[++ topa] = 1; // a[++ topa] = h;
  b[++ topb] = 1; // b[++ topb] = w;

  for (int i = 1; i <= n; ++ i) {
    x[i] = read();
    y[i] = read();

    a[++ topa] = x[i];
    if (x[i] < h) {
      a[++ topa] = x[i] + 1;
    }

    b[++ topb] = y[i];
    if (y[i] < w) {
      b[++ topb] = y[i] + 1;
    }
  }

  sort(a + 1, a + topa + 1);
  sort(b + 1, b + topb + 1);
  int nh = unique(a + 1, a + topa + 1) - a - 1;
  int nw = unique(b + 1, b + topb + 1) - b - 1;

  for (int i = 1; i <= nh; ++ i) {
    hlen[i] = a[i + 1] - a[i];
  } hlen[nh] = h - a[nh] + 1;
  for (int i = 1; i <= nw; ++ i) {
    wlen[i] = b[i + 1] - b[i];
  } wlen[nw] = w - b[nw] + 1;

  /* test passing
  printf("%d %d\n", nh, nw);
  for (int i = 1; i <= nh; ++ i) {
    printf("%d ", hlen[i]);
  } puts("");
  for (int i = 1; i <= nw; ++ i) {
    printf("%d ", wlen[i]);
  } puts("");
  */

  for (int i = 1; i <= n; ++ i) {
    x[i] = lower_bound(a + 1, a + nh + 1, x[i]) - a;
    y[i] = lower_bound(b + 1, b + nw + 1, y[i]) - b;
    mp[x[i]][y[i]] = 1;
  }

  f[1][0] = 1;

  for (int i = 1; i <= nh; ++ i) {
    for (int j = 1; j <= nw; ++ j) {
      // f[i][j] = f[]

    }
  }
  return 0;
}