#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

const int maxn=110;
int ans,n,X,Y,m[maxn][maxn];
struct info{
	int x,y;
}a[maxn];

void judge(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
}

void dfs(int u,int l,int r){
	if (u==n+1){
		ans=max(ans,(l+r)*2);
		return;
	}
	dfs(u+1,l-a[u].x,r);
	dfs(u+1,a[u].x,r);
	dfs(u+1,l,r-a[u].y);
	dfs(u+1,l,a[u].y);
}

int main(){
	judge();
	scanf("%d%d%d",&X,&Y,&n);
	for (int i=1;i<=n;i++) scanf("%d%d",&a[i].x,&a[i].y);
	dfs(1,X,Y);
	printf("%d\n",ans);
	return 0;
}
