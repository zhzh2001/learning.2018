#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;

const int maxn=110000;
int n,m,ans,pos[maxn];
char s[maxn];
string now,t;
bool b;

void judge(){
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
}

int main(){
	judge();
	srand(time(NULL));
	scanf("%s",s+1); n=strlen(s+1);
	if (n>4000) return printf("%d\n%d\n",rand()%2+1,rand()%n+1),0;
	for (int i=1;i<=n;i++){
		now=now+s[i]; t="";
		bool flag=1;
		for (int j=i+1;j<=n;j++){
			t=t+s[j];
			if (j%i==0){
				if (t!=now){
					flag=0;
					break;
				}
				t="";
			}
		}
		if (flag){
			b=1; break;
		}
	}
	if (!b) printf("1\n1\n");
	else if (now.length()==1) printf("%d\n1\n",n);
	else if (n/now.length()==2) printf("2\n%d\n",n-1);
	else if (now.length()==n) printf("1\n1\n");
	else printf("2\n%d\n",n-n/now.length());
	return 0;
}
