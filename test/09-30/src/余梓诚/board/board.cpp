#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

const int maxn=4100,mo=1e9+7;
int a[maxn][maxn],n,m,s,t,ans;

void judge(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
}

void dfs(int x,int y){
	if (x<1||x>s||y<1||y>t||a[x][y]) return;
	if (x==s&&y==t) ans++;
	dfs(x+1,y),dfs(x,y+1);
}

int main(){
	judge();
	scanf("%d%d%d",&s,&t,&n);
	for (int i=1,x,y;i<=n;i++) scanf("%d%d",&x,&y),a[x][y]=1;
	dfs(1,1);
	printf("%d\n",ans);
	return 0;
}
