#include <bits/stdc++.h>
using namespace std;
int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
struct Point{int x,y;}p[100005];
bool cmp(Point a,Point b){
	return a.x==b.x?a.y<b.y:a.x<b.x;
}
bool cmp2(Point a,Point b){
	return a.y==b.y?a.x<b.x:a.y<b.y;
}
int X,Y,n,l,r,ans;
int main(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	X=read();Y=read();n=read();
	for(int i=1;i<=n;i++)
		p[i].x=read(),p[i].y=read();
	p[++n].x=0;p[n].y=0;
	p[++n].x=X;p[n].y=0;
	p[++n].x=0;p[n].y=Y;
	p[++n].x=X;p[n].y=Y;
	sort(p+1,p+n+1,cmp);
	for(int i=1;i<=n;i++){
		l=0;r=Y;
		for(int j=i+1;j<=n;j++){
			if(p[j].x==p[i].x)continue;
			if((r-l+X-p[i].x)*2<ans)break;
			ans=max(ans,(r-l+p[j].x-p[i].x)*2);
			if(p[j].y==p[i].y)break;
			if(p[j].y>p[i].y)r=min(r,p[j].y);
				else l=max(l,p[j].y);
		}
		l=0;r=Y;
		for(int j=i-1;j;j--){
			if(p[j].x==p[i].x)continue;
			if((r-l+p[i].x)*2<ans)break;
			ans=max(ans,(r-l+p[i].x-p[j].x)*2);
			if(p[j].y==p[i].y)break;
			if(p[j].y>p[i].y)r=min(r,p[j].y);
				else l=max(l,p[j].y);
		}
	}
	sort(p+1,p+n+1,cmp2);
	for(int i=1;i<=n;i++){
		l=0;r=X;
		for(int j=i+1;j<=n;j++){
			if(p[j].y==p[i].y)continue;
			if((r-l+Y-p[i].y)*2<ans)break;
			ans=max(ans,(r-l+p[j].y-p[i].y)*2);
			if(p[j].x==p[i].x)break;
			if(p[j].x>p[i].x)r=min(r,p[j].x);
				else l=max(l,p[j].x);
		}
		l=0;r=X;
		for(int j=i-1;j;j--){
			if(p[j].y==p[i].y)continue;
			if((r-l+p[i].y)*2<ans)break;
			ans=max(ans,(r-l+p[i].y-p[j].y)*2);
			if(p[j].x==p[i].x)break;
			if(p[j].x>p[i].x)r=min(r,p[j].x);
				else l=max(l,p[j].x);
		}
	}
	cout<<ans;
	return 0;
}
