#include <bits/stdc++.h>
#define int long long
using namespace std;
int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int P=1e9+7;
int h,w,n,f[3005][3005],x,y,mx,mi=1000000,ans;
/*
int fac[200005],ny[200005];
int ksm(int x,int y){
	int ans=1;
	for(;y;y>>=1,x=x*x%P)if(y&1)ans=ans*x%P;
	return ans;
}
int C(int x,int y){return fac[x]*ny[x-y]%P*ny[y]%P;}
*/
signed main(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	h=read();w=read();n=read();
	if(h<=3000&&w<=3000){
		for(int i=1;i<=n;i++){
			x=read();y=read();
			f[x][y]=-1;
		}
		for(int i=1;i<=h;i++)
			for(int j=1;j<=w;j++)
				if(i==1&&j==1)f[i][j]=1;
				else if(f[i][j]==0){
					f[i][j]=max(f[i-1][j],0ll)+max(f[i][j-1],0ll);
					if(f[i][j]>P)f[i][j]-=P;
				}
		cout<<f[h][w];
		return 0;
	}
	/*
	for(int i=1;i<=n;i++)
		mi=min(mi,read()),mx=max(mx,read());
	fac[0]=1;
	for(int i=1;i<=200000;i++)fac[i]=fac[i-1]*i%P;
	ny[200000]=ksm(fac[200000],P-2);
	for(int i=199999;i>=0;i--)ny[i]=ny[i+1]*(i+1)%P;
	for(int i=mi,j=mx;(i>0&&j<=w);j++,i--)
		ans=(ans+C(i+j-2,i-1)*C(h-i+w-j,h-i)%P)%P;
	cout<<ans;
	*/
	cout<<0;
	return 0;
}
