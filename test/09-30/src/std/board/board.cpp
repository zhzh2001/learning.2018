#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxM = 1e5 + 5, kMaxN = 3005, kMod = 1e9 + 7;

int h, w, n, fact[kMaxM << 1], rfact[kMaxM << 1];
ll ans[kMaxN];

inline ll QuickPow(ll x, ll y) {
  ll ret = 1;
  for (; y; y >>= 1, x = x * x % kMod)
    if (y & 1)
      ret = ret * x % kMod;
  return ret;
}

inline ll Combin(ll n, ll m) {
  return n < m ? 0 : (1LL * fact[n] * rfact[m] % kMod * rfact[n - m] % kMod);
}

struct Node {
  int x, y;
  inline bool operator<(const Node &other) const {
    return x + y < other.x + other.y;
  }
} a[kMaxN];

int main() {
  freopen("board.in", "r", stdin);
  freopen("board.out", "w", stdout);
  scanf("%d%d%d", &h, &w, &n);
  fact[0] = 1;
  for (int i = 1; i <= h + w; ++i)
    fact[i] = 1LL * fact[i - 1] * i % kMod;
  rfact[h + w] = QuickPow(fact[h + w], kMod - 2);
  for (int i = h + w - 1; i >= 0; --i)
    rfact[i] = 1LL * rfact[i + 1] * (i + 1) % kMod;
  for (int i = 1; i <= n; ++i)
    scanf("%d%d", &a[i].x, &a[i].y);
  a[n + 1].x = h, a[n + 1].y = w;
  sort(a + 1, a + n + 1);
  for (int i = 1; i <= n + 1; ++i) {
    ans[i] = Combin(a[i].x + a[i].y - 2, a[i].x - 1);
    for (int j = 1; j < i; ++j) {
      if (a[i].x < a[j].x || a[i].y < a[j].y)
        continue;
      ans[i] = (ans[i] - ans[j] * Combin(a[i].x - a[j].x + a[i].y - a[j].y, a[i].x - a[j].x) + kMod) % kMod;
    }
  }
  printf("%lld\n", (ans[n + 1] % kMod + kMod) % kMod);
  return 0;
}
