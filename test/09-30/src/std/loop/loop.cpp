#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pi;
typedef vector<int> vi;

const int kMaxL = 5e5 + 5;
char s[kMaxL];
int n, nxt[kMaxL], pre[kMaxL];

inline bool CheckNext(int pos) {
  if (!nxt[pos])
    return true;
  return (pos % (pos - nxt[pos]) != 0);
}
inline bool CheckPred(int pos) {
  if (!pre[pos])
    return true;
  return (pos % (pos - pre[pos]) != 0);
}

void InitFail(const char *s, int *fail) {
  for (int p = 0, i = 2; i <= n; ++i) {
    for (; p && s[i] != s[p + 1]; p = fail[p])
      ;
    if (s[i] == s[p + 1])
      ++p;
    fail[i] = p;
  }
}

int main() {
  freopen("loop.in", "r", stdin);
  freopen("loop.out", "w", stdout);
  scanf("%s", s + 1);
  n = strlen(s + 1);
  InitFail(s, nxt);
  int loop = 0;
  if (nxt[n] && n % (n - nxt[n]) == 0)
    loop = n - nxt[n];
  if (!loop)
    return printf("1\n1\n"), 0;
  if (loop == 1)
    return printf("%d\n1\n", n), 0;
  reverse(s + 1, s + n + 1);
  InitFail(s, pre); 
  int ans = 0;
  for (int i = 1; i < n; ++i)
    ans += (CheckNext(i) && CheckPred(n - i));
  return printf("2\n%d\n", ans), 0;
}
