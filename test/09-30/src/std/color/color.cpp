#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <vector>
#include <algorithm>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void ReadInt(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::ReadInt;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 3e5 + 5;

struct Node { 
  int x, y;
  inline Node(int _x = 0, int _y = 0) : x(_x), y(_y) {}
  inline bool operator<(const Node &other) const { return x < other.x; }
} a[kMaxN], stl[kMaxN], str[kMaxN];

struct SegTree {
  static const int kNode = kMaxN << 2;
  int mx[kNode], ladd[kNode];
  inline void Clear() {
    memset(mx, 0x00, sizeof mx);
    memset(ladd, 0x00, sizeof ladd);
  }
  inline void PushDown(int x) {
    if (ladd[x]) {
      mx[x << 1] += ladd[x], mx[x << 1 | 1] += ladd[x];
      ladd[x << 1] += ladd[x], ladd[x << 1 | 1] += ladd[x];
      ladd[x] = 0;
    }
  }
  void IAdd(int x, int l, int r, int ql, int qr, int val) {
    if (ql <= l && r <= qr) {
      mx[x] += val, ladd[x] += val;
      return;
    }
    int mid = (l + r) >> 1;
    PushDown(x);
    if (ql <= mid)
      IAdd(x << 1, l, mid, ql, qr, val);
    if (qr > mid)
      IAdd(x << 1 | 1, mid + 1, r, ql, qr, val);
    mx[x] = max(mx[x << 1], mx[x << 1 | 1]);
  }
  int IQMax(int x, int l, int r, int ql, int qr) {
    if (ql <= l && r <= qr)
      return mx[x];
    int mid = (l + r) >> 1;
    PushDown(x);
    if (qr <= mid)
      return IQMax(x << 1, l, mid, ql, qr);
    if (ql > mid)
      return IQMax(x << 1 | 1, mid + 1, r, ql, qr);
    return max(IQMax(x << 1, l, mid, ql, qr), IQMax(x << 1 | 1, mid + 1, r, ql, qr));
  }
} seg;

int X, Y, N, topl, topr, ans;

void Update() {
  seg.Clear();
  sort(a + 1, a + N + 1);
  topl = topr = 0;
  for (int i = 1; i < N; ++i) {
    int cur = i - 1;
    if (a[i].y <= Y / 2) {
      for (; topl && stl[topl].y < a[i].y; cur = stl[topl--].x - 1)
        seg.IAdd(1, 1, N, stl[topl].x, cur, stl[topl].y - a[i].y);
      if (cur != i - 1)
        stl[++topl] = Node(cur + 1, a[i].y);
    } else {
      for (; topr && str[topr].y > a[i].y; cur = str[topr--].x - 1)
        seg.IAdd(1, 1, N, str[topr].x, cur, a[i].y - str[topr].y);
      if (cur != i - 1)
        str[++topr] = Node(cur + 1, a[i].y);
    }
    stl[++topl] = Node(i, 0);
    str[++topr] = Node(i, Y);
    seg.IAdd(1, 1, N, i, i, Y - a[i].x);
    // ans = max(ans, seg.IQMax(1, 1, N, 1, i) + a[i + 1].x);
    ans = max(ans, seg.mx[1] + a[i + 1].x);
  }
}

int main() {
  freopen("color.in", "r", stdin);
  freopen("color.out", "w", stdout);
  ReadInt(X), ReadInt(Y), ReadInt(N);
  for (int i = 1; i <= N; ++i) {
    ReadInt(a[i].x), ReadInt(a[i].y);
  }
  a[++N] = Node(0, 0), a[++N] = Node(X, Y);
  Update();
  for (int i = 1; i <= N; ++i) {
    swap(a[i].x, a[i].y);
  }
  swap(X, Y);
	Update();
  printf("%d\n", ans << 1);
  return 0;
}
