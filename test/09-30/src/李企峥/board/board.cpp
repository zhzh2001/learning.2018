#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
struct D{
	int x,y;
}a[100100];
int f[100100];
ll zyy[200100],lqz[200100];
ll mo,n,h,w;
inline ll qpow(ll a,ll b)
{
	ll lzq=1;
	while(b)
	{
		if(b&1)lzq=lzq*a%mo;
		a=a*a%mo;
		b>>=1;
	}
	return lzq;
}
inline ll C(ll n,ll m)
{
	if(n==0)return 0;
	if(m==0)return 1;
	return zyy[n]*lqz[m]%mo*lqz[n-m]%mo;
}
bool cmp(D a,D b)
{
	return (a.x+a.y)<(b.x+b.y);
}
int main()
{
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	mo=1e9+7;
	h=read();w=read();n=read();
	zyy[0]=1;
	For(i,1,h+w)zyy[i]=zyy[i-1]*i%mo;
	lqz[h+w]=qpow(zyy[h+w],mo-2);
	Rep(i,0,h+w-1)lqz[i]=lqz[i+1]*(i+1)%mo;
	For(i,1,n)a[i].x=read(),a[i].y=read();
	sort(a+1,a+n+1,cmp);
	n++;
	a[n].x=h;a[n].y=w;
	For(i,1,n)
	{
		int tx=a[i].x,ty=a[i].y;
		f[i]=C(tx+ty-2,tx-1);
		For(j,1,i)
			if(a[j].x<=a[i].x&&a[j].y<=a[i].y)
			{
				int x=a[i].x-a[j].x+1,y=a[i].y-a[j].y+1;
				f[i]=(f[i]-f[j]*C(x+y-2,x-1)%mo+mo)%mo;
			}
	}
	cout<<f[n]<<endl;
	return 0;
}
