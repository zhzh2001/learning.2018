#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x); puts("");}
void setIO() {
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
}
const int N = 600000;
char s[N];
int n;
ll f[N], g[N];
bool check(int l, int r) {
	int len = r - l + 1;
	if(len == 1) return 1;
	for(int i = 1; i < len; ++i)
		if(len % i == 0) {
			bool fg = 1;
			for(int j = l + i; j <= r; ++j)
				if(s[j] != s[l + (j - l) % i])  {
					fg = 0;
					break;
				}
			if(fg) return 0;
		}
	return 1;
}
int main() {
	setIO(); 
	scanf("%s", s + 1);
	n = strlen(s + 1);
	g[0] = 1;
	for(int i = 1; i <= n; ++i) {
		f[i] = i;
		for(int j = 1; j <= i; ++j)
			if(f[j - 1] + 1 <= f[i] && check(j, i)) {
				if(f[j - 1] + 1 < f[i]) {
					f[i] = f[j - 1] + 1;
					g[i] = g[j - 1];
				} else if(f[j - 1] + 1 == f[i])
					g[i] += g[j - 1];
			}
	}
	writeln(f[n]);
	writeln(g[n]);
	return 0;
}
