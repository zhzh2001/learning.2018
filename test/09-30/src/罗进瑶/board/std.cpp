#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x); puts("");}
void setIO() {
	freopen("board.in","r",stdin);
	freopen("std.out","w",stdout);
}
const int N = 4100;
const ll p = 1e9 + 7;
int r, c, n;
bool vis[3100][3100];
int f[3100][3100]; 
namespace F1 {
	void main() {
		for(int i = 1; i <= n; ++i) {
			int x = read(), y = read();
			vis[x][y] = 1;
		}
		f[1][1] = 1;
		for(int i = 1; i <= r; ++i)
			for(int j = 1; j <= c; ++j) {
				if(i == 1 && j == 1 || vis[i][j]) continue;
				f[i][j] = (f[i - 1][j] + f[i][j - 1]) % p;
			}
		writeln(f[r][c]);		
	}
}
int b1[N], b2[N], tot1, tot2, x[N], y[N];
ll fac[205000], inv[205000];
ll ksm(ll x, ll y) {
	ll res = 1;
	for(; y; y >>= 1, x = x * x % p)
		if(y & 1)
			res = res * x % p;
	return res;
}
inline ll C(ll n, ll m) {
	if(n < m) return 0;
	return fac[n] * inv[m] % p * inv[n - m] % p;
}
namespace F2 {
	int rak[N];
	bool cmp(int l, int r) {
		if(x[l] == x[r]) return y[l] < y[r];
		return x[l] < x[r];
	}
	void main() {
		fac[0] = inv[0] = 1;
		for(int i = 1; i <= 200000; ++i) {
			fac[i] = fac[i - 1] * i % p;
			inv[i] = ksm(fac[i], p - 2);
		}
		for(int i = 1; i <= n; ++i) {
			x[i] = read(), y[i] = read();
			rak[i] = i;
		}
		sort(rak + 1, rak + 1 + n, cmp);
		int B = (1 << n);
		ll ans = 0;
		for(int S = 0; S < B; ++S) {
			ll res = 1;
			int cnt = 0, prex = 1, prey = 1;
			for(int i = 1;i <= n; ++i)
				if(S & 1 << i - 1) {
					++cnt;
					res *= C(x[rak[i]] - prex + y[rak[i]] - prey, y[rak[i]] - prey);
					res %= p;
					prex = x[rak[i]], prey = y[rak[i]];
				}
			res = res * C(r + c - prex - prey, r - prex) %p;
			if(cnt & 1) ans -= res;
			else ans += res;			
		}
		writeln(ans);
	}
}
namespace F3 {
	void main() {
		fac[0] = inv[0] = 1;
		for(int i = 1; i <= 200000; ++i) {
			fac[i] = fac[i - 1] * i % p;
			inv[i] = ksm(fac[i], p - 2);
		}
		for(int i = 1; i <= n; ++i) {
			x[i] = read(), y[i] = read();
			b1[++tot1] = x[i];
			b2[++tot2] = y[i];
		}
		sort(b1 + 1, b1 + 1 + tot1);
		sort(b2 + 1, b2 + 1 + tot2);
		tot1 = unique(b1 + 1, b1 + 1 + tot1) - b1 - 1;
		tot2 = unique(b2 + 1, b2 + 1 + tot2) - b2 - 1;
		for(int i = 1; i <= n; ++i) {
			x[i] = lower_bound(b1 + 1, b1 + 1 + tot1, x[i]) - b1;
			y[i] = lower_bound(b2 + 1, b2 + 1 + tot2, y[i]) - b2;
			vis[x[i]][y[i]] = 1;
		}
		ll ans = C(r + c - 2, r - 1);
		for(int i = 1; i <= tot2; ++i) f[1][i] = C(b1[1] - 1 + b2[i] - 1, b1[1] - 1);
		for(int i = 2; i <= tot1; ++i)
			for(int j = 1; j <= tot2; ++j)
				f[i][j] = (f[i - 1][j] + f[i][j - 1]) % p;
		for(int i = 1; i <= tot1; ++i) {
			ans -= f[i][tot2] * C(r - b1[i] + c - (b2[tot2] + 1), r - b1[i]) % p;
			ans %= p;
		}
		ans = (ans + p) % p;
		writeln(ans);
	}
}
int main() {
	setIO();
	r = read(), c = read(), n = read();
	F1::main();
	return 0;
}
