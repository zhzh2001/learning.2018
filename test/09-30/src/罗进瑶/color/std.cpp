#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x); puts("");}
void setIO() {
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
}
const int N = 400000;
ll r, c;
int n; 
ll x[N], y[N], ans;
void dfs(ll x1, ll y1, ll x2, ll y2, int cur) {
	if(2 * (y2 - y1 + x2 - x1)<= ans) return;
	if(x2 < x1 || y2 < y1) return;
	if(cur == n + 1) {
		ans = ((x2 - x1) + (y2 - y1)) * 2;
		return;
	}
	if(!(x[cur] >= x1 && x[cur] <= x2 && y[cur] >= y1 && y[cur] <= y2)) dfs(x1, y1, x2, y2, cur +1);
	else {
		dfs(x1, y1, x[cur],  y2, cur + 1);
		dfs(x1, y1, x2, y[cur], cur + 1);
		dfs(x[cur], y1, x2, y2, cur + 1);
		dfs(x1, y[cur], x2, y2, cur + 1); 
	}
}
int main() {
	setIO();
	r = read(), c = read(), n = read();
	for(int i = 1; i <= n; ++i) {
		x[i] = read(), y[i] = read();
	}
	dfs(0, 0, r, c, 1);
	writeln(ans);
	return 0;
}
