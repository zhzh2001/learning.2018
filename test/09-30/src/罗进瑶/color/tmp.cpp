#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x); puts("");}
void setIO() {
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
}
const int N = 400000;
ll r, c;
int n; 
ll ans;
struct T {
	ll x, y;
}a[N];
ll b1[N], tot1;
ll b2[N], tot2;
vector<int>v[N];
int f[N];
ll solve(ll p, ll q) {
	ll res = -1e18;
	for(int i = p; i <= q; ++i)
		for(int j = 0; j < v[i].size(); ++j)
			f[v[i][j]] = 1;
	int pre = 0;
	for(int i = 1; i <= tot2; ++i)
		if(f[i]) {
			res = max(res, b2[i] - b2[pre]);
			pre = i;			
		}
	res = max(res, c - b2[pre]);
	for(int i = p; i <= q; ++i)
		for(int j = 0; j < v[i].size(); ++j)
			f[v[i][j]] = 0;
	return res;
}
int main() {
	r = read(), c = read(), n = read();
	for(int i = 1; i <= n; ++i){
		a[i].x = read(), a[i].y = read();
		b1[++tot1] = a[i].x;
		b2[++tot2] = a[i].y;
	}
	sort(b1 + 1, b1 + tot1 + 1);
	sort(b2 + 1, b2 + tot2 + 1);
	for(int i = 1; i <= n; ++i) {
		a[i].x = lower_bound(b1 + 1, b1 + 1 + tot1, a[i].x) - b1;
		a[i].y = lower_bound(b2 + 1, b2 + 1 + tot2, a[i].y) - b2;
		v[a[i].x].push_back(a[i].y);
	}
	ll ans = 0;
	for(int i = 1; i < tot1; ++i)
		for(int j = i + 1; j <= tot1; ++j)
			ans = max(ans, 2 * (b1[a[j].x] - b1[a[i].x] + solve(i + 1, j - 1)));
	writeln(ans);
	return 0;
}








#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x); puts("");}
void setIO() {
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
}
const int N = 400000;
ll r, c;
int n; 
ll ans;
struct T {
	ll x, y;
}a[N];
ll b1[N], tot1;
ll b2[N], tot2;
vector<int>v[N];
int f[N];
ll solve(ll p, ll q) {
	ll res = -1e18;
	for(int i = p; i <= q; ++i)
		for(int j = 0; j < v[i].size(); ++j)
			f[v[i][j]] = 1;
	int pre = 0;
	for(int i = 1; i <= tot2; ++i)
		if(f[i]) {
			res = max(res, b2[i] - b2[pre]);
			pre = i;			
		}
	res = max(res, c - b2[pre]);
	for(int i = p; i <= q; ++i)
		for(int j = 0; j < v[i].size(); ++j)
			f[v[i][j]] = 0;
	return res;
}
int rt[N * 32], num, ls[N], rs[N * 32], sum[N * 32];
void change(int &o, int l, int r, int x){
	if(!o) o = ++num;
	if(l == r) {
		++sum[o];
		return;
	}
	int mid = (l + r) >> 1;
	if(x <= mid) change(ls[o], l, mid, x);
	else change(rs[o], mid + 1, r, x);
}

void up(int x, int y) {
	for(; x <= tot1; x += x & -x)
		change(rt[x], 1, tot2, y);
}
int main() {
	r = read(), c = read(), n = read();
	for(int i = 1; i <= n; ++i){
		a[i].x = read(), a[i].y = read();
		b1[++tot1] = a[i].x;
		b2[++tot2] = a[i].y;
	}
	sort(b1 + 1, b1 + tot1 + 1);
	sort(b2 + 1, b2 + tot2 + 1);
	for(int i = 1; i <= n; ++i) {
		a[i].x = lower_bound(b1 + 1, b1 + 1 + tot1, a[i].x) - b1;
		a[i].y = lower_bound(b2 + 1, b2 + 1 + tot2, a[i].y) - b2;
		v[a[i].x].push_back(a[i].y);
	}
	for(int i = 1; i <= tot1; ++i)
		for(int j = 0; j < v[i].size(); ++j)
			up(rt[i], v[i][j]);
	ll ans = 0;
	for(int i = 1; i < tot1; ++i)
		for(int j = i + 1; j <= tot1; ++j)
			ans = max(ans, 2 * (b1[a[j].x] - b1[a[i].x] + solve(i + 1, j - 1)));
	writeln(ans);
	return 0;
}
