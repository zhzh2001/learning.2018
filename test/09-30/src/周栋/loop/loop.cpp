#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
typedef int ll;
const ll N=5e5+10,P=998244353;
char a[N];
ll n,net[N],f[N];
int main(){
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	scanf("%s",a);
	n=strlen(a);
	bool flag=0;
	for (ll i=1;i<n;++i) if (a[i]!=a[i-1]) flag=1;
	if (!flag) return printf("%d\n1",n),0;
	for (ll t=2;t<n;++t){
		if (n%t!=0) continue;
		flag=1;
		ll v=0;
		for (ll i=0;i<t;++i) v=(v*26+a[i]-'a')%P;
		for (ll S=1;t*S<=n&&flag;++S){
			ll g=0;
			for (ll i=t*(S-1);i<t*S;++i)
				g=(g*26+a[i]-'a')%P;
			if (g!=v) flag=0;
		}
		if (flag==1){
			puts("2");
			cout<<n-n/t+1;
			return 0;
		}
	}
	puts("1\n1");
	return 0;
}
/*
	for (ll i=1,k=0;i<n;++i){
		while (k&&a[k]!=a[i]) k=net[k-1];
		if (a[k]==a[i]) ++k;
		net[i]=k;
	}
	for (ll i=0;i<n;++i) while (net[i]*2-1>i) net[i]=net[net[i]];
//	for (ll i=0;i<n;++i) f[i]=f[net[i]-1]+1;
	ll num=0;
	while (net[n-1]) net[n-1]=net[net[n-1]],++num;
	printf("%lld\n",num);
*/
//sxdakking
