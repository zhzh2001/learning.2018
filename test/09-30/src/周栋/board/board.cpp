#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return -1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll P=1e9+7,N=2e5+10;
ll fac[N],inv[N],H,W,n,f1[N],f2[N],ans;
struct node{ll x,y;}a[N];
bool sxd(node a,node b){return a.x==b.x?a.y<b.y:a.x<b.x;}
inline ll pow(ll x,ll y){
	ll ret=1;
	for(;y;y>>=1,x=x*x%P) if (y&1) ret=ret*x%P;
	return ret;
}
inline ll C(ll x,ll y){return fac[x]*inv[y]%P*inv[x-y]%P;}
int main(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	read(H),read(W),read(n);
	fac[0]=1;
	for (ll i=1;i<=H+W;++i) fac[i]=fac[i-1]*i%P;
	inv[H+W]=pow(fac[H+W],P-2);
	for (ll i=H+W-1;~i;--i) inv[i]=inv[i+1]*(i+1)%P;
	for (ll i=1;i<=n;++i) read(a[i].x),read(a[i].y);
	sort(a+1,a+1+n,sxd);
	for (ll i=1;i<=n;++i){
		f1[i]=C(a[i].x+a[i].y-2,a[i].x-1);
		f2[i]=C(H+W-a[i].x-a[i].y,H-a[i].x);
		for (ll j=1;j<i;++j){
			if (a[j].x<=a[i].x&&a[j].y<=a[i].y)
				f1[i]=(f1[i]-f1[j]*C(a[i].x+a[i].y-a[j].x-a[j].y,a[i].x-a[j].x)%P+P)%P;
/*			if (a[i].x<=a[j].x&&a[i].y<=a[j].y)
				f1[j]=(f1[j]-C(a[i].x+a[i].y-2,a[i].x-1)*C(a[j].x+a[j].y-2-(a[i].x+a[i].y-2),a[j].x-1-(a[i].x-1))%P+P)%P;*/
/*			if (a[i].x>=a[j].x&&a[i].y>=a[j].y)
				f2[j]=(f2[j]-C(a[i].x+a[i].y-a[j].x-a[j].y,a[i].x-a[j].x)*C(H+W-a[i].x-a[i].y,H-a[i].x)%P+P)%P;*/
		}
	}
	for (ll i=1;i<=n;++i) ans=(ans+f1[i]*f2[i]%P)%P;
	wr((C(H+W-2,H-1)-ans+P)%P);
	return 0;
}
//sxdakking
