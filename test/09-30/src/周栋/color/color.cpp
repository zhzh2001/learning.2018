#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return -1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=3e5+10;
ll x,y,n,ans;
struct node{
	ll x,y;
	bool operator <(const node &res)const{
		return x==res.x?y<res.y:x<res.x;
	}
}a[N];
int main(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	read(x),read(y),read(n);
	for (ll i=1;i<=n;++i) read(a[i].x),read(a[i].y);
	sort(a+1,a+1+n);
	for (ll i=1;i<=n;++i){
		ll U=0,D=y;
		for (ll j=i+1;j<=n;++j){
			ans=max(ans,(a[j].x-a[i].x+D-U)*2);
			if (a[j].y<U||a[j].y>D) continue;
			ll mid=(U+D)/2;
			if (a[j].y<mid) U=a[j].y;
			else D=a[j].y;
		}
	}
	wr(ans);
	return 0;
}
//sxdakking
