/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("0"),0;
#define mod 1000000007
#define N 500055
#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,f[N],fac[N],inv[N],h,w;
struct xx{
	int x,y;
}z[N+5];
bool cmp(xx a,xx b){
	return (a.x==b.x)?a.y<b.y:a.x<b.x;
}
int ksm(ll x,int k){
	int sum=1;
	while (k){
		if (k&1) sum=sum*x%mod;
		x=x*x%mod;k>>=1;
	}
	return sum;
}
int C(int n,int m){
	return (n<m||m<0)?0:1LL*fac[n]*inv[m]%mod*inv[n-m]%mod;
}
void init(int x){
	fac[0]=1;F(i,1,x) fac[i]=1LL*fac[i-1]*i%mod;
	inv[x]=ksm(fac[x],mod-2);D(i,x-1,0) inv[i]=1LL*inv[i+1]*(i+1)%mod;
}
inline void add(int &x,int k){x+=k;x-=(x>=mod)?mod:0;x+=(x<0)?mod:0;}
signed main(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	h=read();w=read();n=read();
	z[0].x=1;z[0].y=1;
	F(i,1,n){
		z[i].x=read();z[i].y=read();
		if ((z[i].x==1&&z[i].y==1)||(z[i].x==h&&z[i].y==w)) ret;
	}
	z[n+1].x=h;z[n+1].y=w;
	sort(z,z+n+2,cmp);
	n++;
	init(h+w+15);
	F(i,1,n){
		f[i]=C(z[i].x+z[i].y-2,z[i].x-1);
		if (i!=n) f[i]=(mod-f[i])%mod;
		F(j,1,i-1){
			if (i!=n) add(f[i],-1LL*f[j]*C(z[i].x+z[i].y-z[j].x-z[j].y,z[i].x-z[j].x)%mod);
			else add(f[i],1LL*f[j]*C(z[i].x+z[i].y-z[j].x-z[j].y,z[i].x-z[j].x)%mod);
		}
	}
	wrn(f[n]);
	return 0;
}
