/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
//#define mod 1000000007
#define N 400055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,wzp,ans,h1[N],h2[N],X,Y,l,r,t2,t1,z1[N],z2[N],x[N],y[N],zh[N],cnt,g[3005][3005],g2[3005][3005];
struct xx{
	int x,y;
}z[N];
/*struct tr{
	int l,r,min;
}t[N*4];
void build(int x,int l,int r,int p){
	t[x].l=l;t[x].r=r;
	if (l==r){
		if (p==0) t[x].min=h2[l];
		if (p==1) t[x].min=h1[l];
		return;
	}
	int mid=(l+r)>>1;
	build(x*2,l,mid,p);build(x*2+1,mid+1,r,p);
	t[x].min=min(t[x*2].min,t[x*2+1].min);
}
int query(int x,int l,int r){
	if (l>r) return wzp;
	if (t[x].l==l&&t[x].r==r) return t[x].min;
	int mid=(t[x].l+t[x].r)>>1;
	if (r<=mid) return query(x*2,l,r);
	else if (l>mid) return query(x*2+1,l,r);
	else return min(query(x*2,l,mid),query(x*2+1,mid+1,r));
}*/
map <int,int> p;
signed main(){
	freopen("color.in","r",stdin);freopen("color.out","w",stdout);
	X=read();Y=read();n=read();
	l=X/2;r=Y/2;
	F(i,1,n){
		x[i]=z[i].x=read();y[i]=z[i].y=read();
	}
	x[n+1]=X/2;y[n+1]=Y/2;x[n+2]=y[n+2]=0;x[n+3]=X;y[n+3]=Y;
	sort(x+1,x+n+4);sort(y+1,y+n+4);
	x[0]=y[0]=-1;
	int t1=0,t2=0;
	F(i,1,n+3){
		if (x[i]!=x[i-1]) z1[++t1]=x[i];
		p[x[i]]=t1;
	}
	F(i,1,n) z[i].x=p[z[i].x];l=p[l];
	t2=0;
	F(i,1,n+3){
		if (y[i]!=y[i-1]) z2[++t2]=y[i];
		p[y[i]]=t2;
	}
	F(i,1,n) z[i].y=p[z[i].y];r=p[r];
	ans=max(X,Y)*2+2;
	F(i,1,t2){
		h1[i]=X-z1[l];h2[i]=z1[l];
	}
	F(i,1,n){
		if (z[i].x>l) h1[z[i].y]=min(h1[z[i].y],z1[z[i].x]-z1[l]);
		if (z[i].x<=l) h2[z[i].y]=min(h2[z[i].y],z1[l]-z1[z[i].x]);
	}
	h1[1]=h1[t2]=X-z1[l];h2[1]=h2[t2]=z1[l];
	F(i,1,t2) F(j,1,t2) g[i][j]=X-z1[l],g2[i][j]=z1[l];
	F(i,1,t2){
		g[i][i]=h1[i];g2[i][i]=h2[i];
		F(j,i+1,t2){
			g[i][j]=min(g[i][j-1],h1[j]);
			g2[i][j]=min(g2[i][j-1],h2[j]);
		}
	}
	F(i,1,t2){
		F(j,i+1,t2){
			ans=max(ans,(z2[j]-z2[i])*2+g[i+1][j-1]*2+g2[i+1][j-1]*2);
		}
	}
	
	/*build(1,1,t2,0);
	zh[cnt=1]=1;
	F(i,2,t2){
		ans=max(ans,h1[zh[cnt]]*2+(y[i]-y[zh[cnt]])*2+query(1,zh[cnt]+1,i-1)*2);
		while (h1[i]<h1[zh[cnt]]&&cnt>0){
			cnt--;if (cnt>0) ans=max(ans,h1[zh[cnt]]*2+(y[i]-y[zh[cnt]])*2+query(1,zh[cnt]+1,i-1)*2);
		}
		zh[++cnt]=i;
	}
	while (cnt>0){
		ans=max(ans,h1[zh[cnt]]*2+(y[t2]-y[zh[cnt]])*2+query(1,zh[cnt]+1,t2-1)*2);
		cnt--;
	}
	build(1,1,t2,1);
	zh[cnt=1]=1;
	F(i,2,t2){
		ans=max(ans,h2[zh[cnt]]*2+(y[i]-y[zh[cnt]])*2+query(1,zh[cnt]+1,i-1)*2);
		while (h2[i]<h2[zh[cnt]]&&cnt>0){
			cnt--;if (cnt>0) ans=max(ans,h2[zh[cnt]]*2+(y[i]-y[zh[cnt]])*2+query(1,zh[cnt]+1,i-1)*2);
		}
		zh[++cnt]=i;		
	}
	while (cnt>0){
		ans=max(ans,h2[zh[cnt]]*2+(y[t2]-y[zh[cnt]])*2+query(1,zh[cnt]+1,t2-1)*2);
		cnt--;
	}*/
	F(i,1,t1){
		h1[i]=Y-z2[r];h2[i]=z2[r];
	}
	F(i,1,n){
		if (z[i].y>r) h1[z[i].x]=min(h1[z[i].x],z2[z[i].y]-z2[r]);
		if (z[i].y<=r) h2[z[i].x]=min(h2[z[i].x],z2[r]-z2[z[i].y]);
	}
	h1[1]=h1[t1]=Y-z2[r];h2[1]=h2[t1]=z2[r];
	F(i,1,t1) F(j,1,t2) g[i][j]=Y-z2[r],g2[i][j]=z2[r];
	F(i,1,t1){
		g[i][i]=h1[i];g2[i][i]=h2[i];
		F(j,i+1,t1){
			g[i][j]=min(g[i][j-1],h1[j]);
			g2[i][j]=min(g2[i][j-1],h2[j]);
		}
	}
	F(i,1,t1){
		F(j,i+1,t1){
			ans=max(ans,(z1[j]-z1[i])*2+g[i+1][j-1]*2+g2[i+1][j-1]*2);
		}
	}
	/*build(1,1,t1,0);
	zh[cnt=1]=1;
	F(i,2,t1){
		ans=max(ans,h1[zh[cnt]]*2+(x[i]-x[zh[cnt]])*2+query(1,zh[cnt]+1,i-1)*2);
		while (h1[i]<h1[zh[cnt]]&&cnt>0){
			cnt--;if (cnt>0) ans=max(ans,h1[zh[cnt]]*2+(x[i]-x[zh[cnt]])*2+query(1,zh[cnt]+1,i-1)*2);
		}
		zh[++cnt]=i;
	}
	while (cnt>0){
		ans=max(ans,h1[zh[cnt]]*2+(x[t1]-x[zh[cnt]])*2+query(1,zh[cnt]+1,t1-1)*2);
		cnt--;
	}
	build(1,1,t1,1);
	zh[cnt=1]=1;
	F(i,2,t1){
		ans=max(ans,h2[zh[cnt]]*2+(x[i]-x[zh[cnt]])*2+query(1,zh[cnt]+1,i-1)*2);
		while (h2[i]<h2[zh[cnt]]&&cnt>0){
			cnt--;if (cnt>0) ans=max(ans,h2[zh[cnt]]*2+(x[i]-x[zh[cnt]])*2+query(1,zh[cnt]+1,i-1)*2);
		}
		zh[++cnt]=i;
	}
	while (cnt>0){
		ans=max(ans,h2[zh[cnt]]*2+(x[t1]-x[zh[cnt]])*2+query(1,zh[cnt]+1,t1-1)*2);
		cnt--;
	}*/
	wrn(ans);
	return 0;
}
