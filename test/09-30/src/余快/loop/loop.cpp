/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
//#define mod 1000000007
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,nxt[N],t,nxt2[N],ans;
char str[N];
signed main(){
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	scanf("%s",str+1);
	n=strlen(str+1);
	nxt[1]=t=0;
	F(i,2,n){
		while (str[i]!=str[t+1]&&t) t=nxt[t];
		if (str[i]==str[t+1]) t++;
		nxt[i]=t;
	} 
	int num=n-nxt[n];
	if (n%num==0&&num!=n){
		if (num==1){
			wrn(n);wrn(1);return 0;
		}
		wrn(2);
		t=n+1;nxt2[n]=n+1;
		D(i,n-1,1){
			while (str[i]!=str[t-1]&&t<=n) t=nxt2[t];
			if (str[i]==str[t-1]) t--;
			nxt2[i]=t;
		}
		F(i,1,n-1){
			if ((i<=1||(i%(i-nxt[i])!=0||nxt[i]==0))&&(i>=n||((n-i)%(n-i-(n+1-nxt2[i+1]))!=0||(nxt2[i+1]==n+1)))) ans++;
//			wrn((n-i)%(n-i-(n+1-nxt2[i+1]))!=0);
		}
		wrn(ans);
	}
	else{
		wrn(1);wrn(1);
	}
	return 0;
}

