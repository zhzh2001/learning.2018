// timber is akking
#include <bits/stdc++.h>
#define N 200020
#define ll long long
#define mod 1000000007
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

ll frac[N];
ll fast_pow(ll x, ll y) {
  ll z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}
ll inv(ll x) {
  return fast_pow(x, mod - 2);
}
ll C(ll m, ll n) {
  return frac[m] * inv(frac[n]) % mod * inv(frac[m - n]) % mod;
}
ll R(ll h, ll w) {
  return C(h + w - 2, h - 1);
}
struct node {
  int x, y, id;
} a[3020];
bool cmp(const node &a, const node &b) {
  return a.x == b.x ? a.y < b.y : a.x < b.x;
}
bool rcmp(const node &a, const node &b) {
  return a.id < b.id;
}
int res[3020];
vector<int> son[3020], pts[3020];
int dep[3020], in[3020], fake[3020];
int main(int argc, char const *argv[]) {
  freopen("board.in", "r", stdin);
  freopen("board.out", "w", stdout);
  for (int i = frac[0] = 1; i <= 2e5; ++ i) {
    frac[i] = frac[i - 1] * i % mod;
  }
  // printf("%d\n", C(5, 2));

  int h = read(), w = read(), n = read();

  ll ans = R(h, w);
  // printf("%d\n", ans);

  for (int i = 1; i <= n; ++ i) {
    a[i].x = read();
    a[i].y = read();
    a[i].id = i;
  }
  sort(a + 1, a + n + 1, cmp);

  for (int i = 1; i <= n; ++ i) {
    son[0].push_back(i);
    ++ in[i];
    for (int j = 1; j < i; ++ j) {
      if (a[j].x <= a[i].x && a[j].y <= a[i].y) {
        son[j].push_back(i);
        ++ in[i];
      }
    }
  }

  queue<int> q;
  q.push(0);
  int mxdep = 0;
  while (!q.empty()) {
    int x = q.front(); q.pop();
    for (size_t i = 0; i < son[x].size(); ++ i) {
      if (!--in[son[x][i]]) {
        q.push(son[x][i]);
        dep[son[x][i]] = dep[x] + 1;
        pts[dep[son[x][i]]].push_back(son[x][i]);
        mxdep = max(mxdep, dep[son[x][i]]);
      }
    }
  }

  for (int i = 1; i <= mxdep; ++ i) {
    for (size_t j = 0; j < pts[i].size(); ++ j) {
      int now = pts[i][j];
      fake[now] = R(a[now].x, a[now].y);
      for (size_t k = 0; k < pts[i-1].size(); ++ k) {
        int last = pts[i-1][k];
        fake[now] = (fake[now] - R(a[now].x - a[last].x + 1,
            a[now].y - a[last].y + 1) * R(a[last].x, a[last].y) % mod + mod) % mod;
        // fake[now] = (fake[now] + R(a[now].x - a[last].x + 1,))
      }
    }
  }

  for (int i = 1; i <= n; ++ i) {
    res[a[i].id] = fake[i];
  }

  // for (int i = 1; i <= n; ++ i) {
  //   a[i].x = h - a[i].x + 1;
  //   a[i].y = w - a[i].y + 1;
  // }

  // work(res2, n);

  sort(a + 1, a + n + 1, rcmp);

  for (int i = 1; i <= n; ++ i) {
    // printf("%d %d\n", res1[i], R(h - a[i].x + 1, w - a[i].y + 1));
    // ans = (ans - res1[i] * res2[i] % mod + mod) % mod;
    ans = (ans - res[i] * R(h - a[i].x + 1, w - a[i].y + 1) % mod + mod) % mod;
  }

  printf("%d\n", ans);
  return 0;
}