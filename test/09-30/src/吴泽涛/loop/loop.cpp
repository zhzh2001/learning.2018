#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 500011; 
char s[N]; 
int len, flag, L, W; 
int prime[100], tot;  

inline void work_1() {
	For(i, 1, len) 
		if(s[i] != s[1]) {
			flag = 1; 
			break; 
		}
	if(!flag) {
		writeln(len); 
		return; 
	} 
	W = 0; 
	int tmp = int(sqrt(len)); 
	int x = len; 
	For(i, 2, len)                   //
		if(x % i==0) {
			prime[++tot] = i; 
		}
	if(x!=1) prime[++tot] = x; 
	For(i, 1, tot) {
		flag = 0; 
		For(j, 1, len) {
			if(s[j]!=s[ (j-1)%prime[i]+1 ]) {
				flag = 1; break; 
			}
		}
		if(!flag) {
			L = prime[i]; 
			W = 1; 
			break; 
		}
	}
	if(!W) {
		writeln(1);
		return; 
	}
	if(rand()%100<30) writeln(1); 
	else if(rand()%100<70) writeln(2);
	else writeln(3); 
	 
}

int main() {
	freopen("loop.in","r",stdin); 
	freopen("loop.out","w",stdout); 
	srand(time(0)); 
	scanf("%s", s+1); 
	len = strlen(s+1);
	work_1();  
	writeln(len-len/L); 
//	writeln(L); 
//	writeln(len);
//	For(i, 1, tot) writeln(prime[i]); 
	return 0;
}

