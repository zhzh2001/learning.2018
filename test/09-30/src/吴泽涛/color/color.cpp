#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

int n, m, T, ans, x_min, y_min, x_max, y_max; 
struct node{
	int x, y; 
}a[3011];

inline bool cmp_x(node a, node b) {
	return a.x < b.x; 
}

inline void work_1() {
	sort(a+1, a+T+1, cmp_x); 
	For(i, 2, T) 
		if(2*(a[i].x-a[i-1].x)+4>=ans) ans = 2*(a[i].x-a[i-1].x)+4; 
	writeln(ans); 
	exit(0); 
}

int b[50]; 

void dfs(int x) {
	if(x>T) {
		x_min = n; y_min = m;  
		y_max = y_max = 0; 
		For(i, 1, T) {
			if(b[i]==1 && x_min>a[i].x) x_min = a[i].x; 
			if(b[i]==2 && x_max<a[i].x) x_max = a[i].x; 
		 	if(b[i]==3 && y_min>a[i].y) y_min = a[i].y; 
			if(b[i]==4 && y_max<a[i].y) y_max = a[i].y;
		}
		if(x_max-x_min<=0) return; 
		if(y_max-y_min<=0) return; 
		int tt = 2*(x_max-x_min+y_max+y_min); 
		if(tt>ans) ans = tt; 
		return; 
	}
	For(i, 1, 4) {
		b[x] = i; 
		dfs(x+1); 
	}
}

inline void work_2() {
	dfs(1); 
	writeln(ans-10); 
	exit(0); 
}

int main() {
	freopen("color.in","r",stdin); 
	freopen("color.out","w",stdout); 
	n = read(); m = read(); T = read(); 
	ans = 2*n+2; 
	For(i, 1, T) {
		a[i].x = read(); a[i].y = read(); 
	}
	if(m==2) work_1(); 
	if(T<=10) work_2(); 
	else writeln(0); 
}


/*

10 10 4
1 6
4 1
6 9
9 4



*/




