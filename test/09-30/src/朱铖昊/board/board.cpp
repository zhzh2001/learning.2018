#include<bits/stdc++.h>
using namespace std;
#define ll long long
// #define int long long
const int mod=1e9+7;
const int N=3005,M=200005;
int fac[M],inv[M];
pair<int,int> a[N];
int f[N],n,h,w;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
inline int C(int x,int y)
{
	return x<y?0:(ll)fac[x]*inv[y]%mod*inv[x-y]%mod;
}
signed main()
{
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	read(h);
	read(w);
	read(n);
	for (int i=1;i<=n;++i)
	{
		read(a[i].first);
		read(a[i].second);
	}
	a[++n]=make_pair(1,1);
	sort(a+1,a+1+n);
	fac[0]=1;
	for (int i=1;i<=h+w;++i)
		fac[i]=(ll)fac[i-1]*i%mod;
	inv[h+w]=ksm(fac[h+w],mod-2);
	for (int i=h+w-1;i>=0;--i)
		inv[i]=(ll)inv[i+1]*(i+1)%mod;
	for (int i=n;i>=1;--i)
	{
		f[i]=C(h-a[i].first+w-a[i].second,h-a[i].first);
		for (int j=i+1;j<=n;++j)
			if (a[i].first<=a[j].first&&a[i].second<=a[j].second)
				f[i]=(f[i]-(ll)C(a[j].first-a[i].first+a[j].second-a[i].second,a[j].first-a[i].first)*
					  f[j]%mod+mod)%mod;
	}
	cout<<f[1];
	return 0;
}