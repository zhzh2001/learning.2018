#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define pi pair<int,int>
const int mod1=1e9+7,mod2=1e9+9;
const int mi=37;
const int N=500005;
char c[N];
pi ha[N],fac[N];
bool f[N],g[N];
int n;
inline bool same()
{
	for (int i=2;i<=n;++i)
		if (c[i]!=c[1])
			return false;
	return true;
}
// inline int ksm(int x,int y,int mod)
// {
// 	int ans=1;
// 	for (;y;y/=2,x=(ll)x*x%mod)
// 		if (y&1)
// 			ans=(ll)ans*x%mod;
// 	return ans;
// }
inline pi mul(pi x,int y)
{
	x.first=(ll)x.first*y%mod1;
	x.second=(ll)x.second*y%mod2;
	return x;
}
inline void add(pi &x,int y)
{
	(x.first+=y)%=mod1;
	(x.second+=y)%=mod2;
}
inline pi get(int l,int r)
{
	return (pi){(ha[r].first-(ll)ha[l-1].first*
				fac[r-l+1].first%mod1+mod1)%mod1,
				(ha[r].second-(ll)ha[l-1].second*
				fac[r-l+1].second%mod2+mod2)%mod2};
}
inline void work(int x)
{
	pi a=get(1,x);
	for (int i=2;i<=n/x;++i)
	{
		if (a!=get(x*i-x+1,x*i))
		{
			// for (int j=i;j<=n/x;++j)
			// 	f[j*x]=false;
			break;
		}
		else
			f[i*x]=false;
	}
	a=get(n-x+1,n);
	for (int i=2;i<=n/x;++i)
	{
		if (a!=get(n-x*i+1,n-x*i+x))
		{
			// for (int j=i;j<=n/x;++j)
			// 	f[n-j*x+1]=false;
			break;
		}
		else
			g[n-i*x+1]=false;
	}
}
int main()
{
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	scanf("%s",c+1);
	n=strlen(c+1);
	if (same())
	{
		cout<<n<<"\n1";
		return 0;
	}
	fac[0]=(pi){1,1};
	for (int i=1;i<=n;++i)
		fac[i]=mul(fac[i-1],mi);
	// int I1=ksm(mi,mod1-2),I2=ksm(mi,mod2-2);
	// inv[0]=(pi){1,1};
	// for (int i=1;i<=n;++i)
	// 	inv[i]=pi{(int)((ll)inv[i-1].first*I1%mod1),
	// 			  (int)((ll)inv[i-1].second*I2%mod2)};
	for (int i=1;i<=n;++i)
	{
		ha[i]=mul(ha[i-1],mi);
		add(ha[i],c[i]-'a'+1);
	}
	for (int i=1;i<=n;++i)
		f[i]=g[i]=true;
	for (int i=1;i<=n/2;++i)
		work(i);
	if (f[n])
		puts("1\n1");
	else
	{
		int ans=0;
		for (int i=1;i<n;++i)
			if (f[i]&g[i+1])
				++ans;
		cout<<"2\n"<<ans;
	}
	return 0;
}