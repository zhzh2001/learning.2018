#include<bits/stdc++.h>
using namespace std;
#define int long long
#define pi pair<int,int>
const int N=300005;
int h,w,n,ans;
pi a[N];
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
bool cmp(pi x,pi y)
{
	return x.second<y.second;
}
inline void bl()
{
	for (int i=1;i<=n;++i)
	{
		int u=0,d=w,bb=1;
		for (int j=i+1;j<=n;++j)
		{
			if (a[j].second==a[i].second)
			{
				for (int k=j+1;k<=n;++k)
				{
					if (a[k].second>a[i].second)
					{
						if (a[k].second<d)
						{
							ans=max(ans,2*(a[k].first-a[i].first+d-a[i].second));
							d=a[k].second;
						}
					}
					if (a[k].second<a[i].second)
					{
						if (a[k].second>u)
						{
							ans=max(ans,2*(a[k].first-a[i].first+a[i].second-u));
							u=a[k].second;
						}
					}
				}
				ans=max(ans,2*(h-a[i].first+max(a[i].second-u,d-a[i].second)));
				bb=0;
				break;
			}
			if (a[j].second>a[i].second)
			{
				if (a[j].second<d)
				{
					ans=max(ans,2*(a[j].first-a[i].first+d-u));
					d=a[j].second;
				}
			}
			if (a[j].second<a[i].second)
			{
				if (a[j].second>u)
				{
					ans=max(ans,2*(a[j].first-a[i].first+d-u));
					u=a[j].second;
				}
			}
		}
		if (bb)
			ans=max(ans,2*(h-a[i].first+d-u));
	}
}
signed main()
{
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	read(h);
	read(w);
	read(n);
	for (int i=1;i<=n;++i)
	{
		read(a[i].first);
		read(a[i].second);
	}
	sort(a+1,a+1+n,cmp);
	ans=2*(w-a[n].second+h);
	for (int i=1;i<=n;++i)
		ans=max(ans,2*(a[i].second-a[i-1].second+h));
	sort(a+1,a+1+n);
	ans=2*(h-a[n].first+w);
	for (int i=1;i<=n;++i)
		ans=max(ans,2*(a[i].first-a[i-1].first+w));
	//if (n<=3000)
		bl();
	cout<<ans;
	return 0;
}