#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;
inline char _gc(){
	static char buf[1 << 14], *p1 = buf, *p2 = buf;
	return (p1 == p2 && (p2 = (p1 = buf) + fread(buf, 1, 1 << 14, stdin), p1 == p2)) ? EOF : *p1++;
}
#define gc c = _gc()
int read(){
	int x = 0, f = 1; char gc;
	for(; !isdigit(c); gc) if(c == '-') f = -1;
	for(; isdigit(c); gc) x = x * 10 + c - '0';
	return x * f;
}
#undef gc
const int MAXN = 300000;
int X, Y, n;
int a[MAXN], x[MAXN], y[MAXN];
int solve(){
	int L = 0, R = Y;
	int U = 0, D = X;
	for(register int i = 0; i < n; i++) {
		int tmp = 1e9, k = -1;
		if(x[a[i]] - U <= tmp) {
			tmp = x[a[i]] - U;
			k = 0;
		}
		if(D - x[a[i]] <= tmp) {
			tmp = D - x[a[i]];
			k = 1;
		}
		if(y[a[i]] - L <= tmp) {
			tmp = y[a[i]] - L;
			k = 2;
		}
		if(R - y[a[i]] <= tmp) {
			tmp = R - y[a[i]];
			k = 3;
		}
		if(k == 0) U = max(U, x[a[i]]);
		if(k == 1) D = min(D, x[a[i]]);
		if(k == 2) L = max(L, y[a[i]]);
		if(k == 3) R = min(R, y[a[i]]);
	}
	return (R - L + D - U) * 2;
}
int main(){
	freopen("color.in", "r", stdin);
	freopen("color.out", "w", stdout);
	X = read(), Y = read(), n = read();
	for(register int i = 0; i < n; i++) x[i] = read(), y[i] = read();
	for(register int i = 0; i < n; i++) a[i] = i;
	int ans = solve();
	int num = 15000000 / n;
	for(register int i = 0; i < num; i++) {
		srand((signed)time(NULL));
		for(register int j = 0; j < n; j++) swap(a[j], a[rand() % n]);
		ans = max(ans, solve());
	}
	printf("%d", ans);
}
