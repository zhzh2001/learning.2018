#include <bits/stdc++.h>
using namespace std;
#define int long long
const int P = 1e9 + 7;
int n, m, num, x, y, f[5005][5005];
bool vis[5005][5005];
signed main(){
	freopen("board.in", "r", stdin);
	freopen("std.out", "w", stdout);
	cin >> n >> m >> num;
	while(num--) {
		cin >> x >> y;
		vis[x][y] = 1;
	}
	for(int i = 1; i <= n; i++) {
		if(vis[i][1]) break;
		f[i][1] = 1;
	}
	for(int i = 1; i <= m; i++) {
		if(vis[1][i]) break;
		f[1][i] = 1;
	}
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= m; j++) {
			if(i == 1 || j == 1) continue;
			if(vis[i][j]) continue;
			f[i][j] = (f[i - 1][j] + f[i][j - 1]) % P;
		}
	printf("%lld", f[n][m]);
}
