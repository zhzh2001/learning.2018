#include <bits/stdc++.h>
using namespace std;
bool vis[5005][5005];
int main(){
	srand((signed)time(NULL));
	freopen("board.in", "w", stdout);
	int n = 5000, m = 5000;
	cout << n << ' ' << m << ' ';
	int num = rand() % 3000 + 1;
	cout << num << endl;
	vis[1][1] = vis[5000][5000] = 1;
	while(num--) {
		int x = rand() % n + 1, y = rand() % m + 1;
		while(vis[x][y]) x = rand() % n + 1, y = rand() % m + 1;
		cout << x << ' ' << y << endl;
		vis[x][y] = 1;
	}
}
