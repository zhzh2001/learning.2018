#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
string s, lp1, lp2;
int main(){
	freopen("loop.in", "r", stdin);
	freopen("loop.out", "w", stdout);
	cin >> s;
	int len = s.size();
	bool f = 1;
	for(int i = 1; i < (int)s.size(); i++) f &= s[i] == s[0];
	if(f) return printf("%d\n1", len), 0;
	s = '#' + s;
	f = 0;
	for(int i = 1; i <= len / 2; i++) {
		lp1 += s[i];
		if(lp1 == s.substr(1 + i, i)) {
			f = 1;
			break;
		}
	}
	if(!f) return puts("1\n1"), 0;
	int num = 0;
	for(int i = 1; i <= len - (int)lp1.size(); i += lp1.size()) {
		if(s.substr(i, lp1.size()) != lp1) break;
		num++;
	}
	int ans = len - num * (int)lp1.size() + (int)lp1.size();
	if((s.substr(1, len / 2) == lp1)) return printf("2\n%d", ans - 1), 0;
	s.erase(1, num * lp1.size());
	len = s.size() - 1;
	f = 0;
	for(int i = 1; i <= len / 2; i++) {
		lp2 += s[i];
		if(s.substr(1, i) == s.substr(1 + i, i)) {
			f = 1;
			break;
		}
	}
	// if(lp2.size() == 1 && lp1.size() == 1) return puts("1\n1"), 0;
	if(f) printf("2\n%d", ans);
	else puts("1\n1");
}
