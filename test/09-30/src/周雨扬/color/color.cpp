#include<bits/stdc++.h>
using namespace std;
const int N=300050;
struct node{
	int x,y;
	bool operator <(const node &a)const{
		return x<a.x;
	}
}a[N];
int b[N],n,X,Y;
int tg[N*4],mn[N*4];
void build(int k,int l,int r){
	mn[k]=-b[l-1]; tg[k]=0;
	if (l==r) return;
	int mid=(l+r)/2;
	build(k*2,l,mid);
	build(k*2+1,mid+1,r);
}
void change(int k,int l,int r,int x,int y,int v){
	if (x>y) return;
	if (l==x&&r==y){
		tg[k]+=v; mn[k]+=v;
		return;
	}
	int mid=(l+r)/2;
	if (y<=mid) change(k*2,l,mid,x,y,v);
	else if (x>mid) change(k*2+1,mid+1,r,x,y,v);
	else{
		change(k*2,l,mid,x,mid,v);
		change(k*2+1,mid+1,r,mid+1,y,v);
	}
	mn[k]=max(mn[k*2],mn[k*2+1])+tg[k];
}
int ask(int k,int l,int r,int x,int y){
	if (x>y) return -1234567890;
	if (l==x&&r==y) return mn[k];
	int mid=(l+r)/2;
	if (y<=mid) return ask(k*2,l,mid,x,y)+tg[k];
	if (x>mid) return ask(k*2+1,mid+1,r,x,y)+tg[k];
	return max(ask(k*2,l,mid,x,mid),ask(k*2+1,mid+1,r,mid+1,y))+tg[k];
}
struct range{
	int l,r,v;
}q1[N*2],q2[N*2];
int ans;
void solve(){
	int mid=Y/2;
	sort(a+1,a+n+1);
	for (int i=1;i<=n;i++) b[i]=a[i].x;
	sort(b+1,b+n+1);
	*b=unique(b+1,b+n+1)-b-1;
	build(1,2,*b);
	int t1=1,t2=1;
	q1[1]=q2[1]=(range){1,1,1};
	for (int l=1,r;l<=n;l=r+1){
		int at=lower_bound(b+1,b+*b+1,a[l].x)-b;
		if (l!=1){
			q1[++t1]=(range){at-1,at,-mid};
			q2[++t2]=(range){at-1,at,-(Y-mid)};
			change(1,2,*b,at,at,Y);
		}
		ans=max(ans,a[l].x+ask(1,2,*b,2,at));
		for (r=l;r<=n&&a[l].x==a[r+1].x;r++);
		for (int j=l;j<=r;j++)
			if (a[j].y<=mid){
				int v=mid-a[j].y;
				for (;t1&&-q1[t1].v>=v;t1--)
					change(1,2,*b,q1[t1].l+1,q1[t1].r,q1[t1].v);
				q1[t1+1]=(range){q1[t1].r,at,-v};
				change(1,2,*b,q1[t1].r+1,at,v);
				++t1;
			}
			else{
				int v=a[j].y-mid;
				for (;t2&&-q2[t2].v>=v;t2--)
					change(1,2,*b,q2[t2].l+1,q2[t2].r,q2[t2].v);
				q2[t2+1]=(range){q2[t2].r,at,-v};
				change(1,2,*b,q2[t2].r+1,at,v);
				++t2;
			}
	}
}
const int LZH=1000000;
char WZP[LZH],*SSS=WZP,*TTT=WZP;
inline char gc(){
	if (SSS==TTT)
		TTT=(SSS=WZP)+fread(WZP,1,LZH,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
int main(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	X=read(); Y=read(); n=read();
	for (int i=1;i<=n;i++)
		a[i].x=read(),a[i].y=read();
	a[++n]=(node){0,0};
	a[++n]=(node){0,Y};
	a[++n]=(node){X,0};
	a[++n]=(node){X,Y};
	solve();
	swap(X,Y);
	for (int i=1;i<=n;i++)
		swap(a[i].x,a[i].y);
	solve();
	printf("%d\n",ans*2);
}
