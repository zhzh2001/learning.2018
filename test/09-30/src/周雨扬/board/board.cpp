#include<bits/stdc++.h>
using namespace std;
const int mo=1000000007;
const int N=200005;
int H,W,n,f[3005];
int fac[N],inv[N];
struct node{
	int x,y;
	bool operator <(const node &a)const{
		return (x<a.x)||(x==a.x&&y<a.y);
	}
}a[3005];
int path(node a,node b){
	int X=b.x-a.x,Y=b.y-a.y;
	if (X<0||Y<0) return 0;
	return 1ll*fac[X+Y]*inv[X]%mo*inv[Y]%mo;
}
int main(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	scanf("%d%d%d",&H,&W,&n);
	for (int i=1;i<=n;i++) scanf("%d%d",&a[i].x,&a[i].y);
	a[++n]=(node){1,1}; a[++n]=(node){H,W};
	sort(a+1,a+n+1);
	fac[0]=inv[0]=inv[1]=1;
	for (int i=2;i<N;i++) inv[i]=1ll*inv[mo%i]*(mo-mo/i)%mo;
	for (int i=1;i<N;i++){
		fac[i]=1ll*fac[i-1]*i%mo;
		inv[i]=1ll*inv[i-1]*inv[i]%mo;
	}
	for (int i=2;i<=n;i++)
		f[i]=path(a[1],a[i]);
	for (int i=2;i<=n;i++)
		for (int j=2;j<i;j++)
			f[i]=(f[i]+mo-1ll*f[j]*path(a[j],a[i])%mo)%mo;
	printf("%d\n",f[n]);
}
