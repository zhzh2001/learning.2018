#include<bits/stdc++.h>
using namespace std;
const int N=500005;
const int mo1=1000000007;
const int mo2=1000000009;
char s[N];
int n,P1[N],P2[N];
int hsh1[N],hsh2[N];
bool ok[N];
void hash_init(){
	P1[0]=P2[0]=1;
	for (int i=1;i<=n;i++){
		P1[i]=1ll*P1[i-1]*137%mo1;
		P2[i]=1ll*P2[i-1]*139%mo2;
	}
	for (int i=1;i<=n;i++){
		hsh1[i]=(137ll*hsh1[i-1]+s[i])%mo1;
		hsh2[i]=(139ll*hsh2[i-1]+s[i])%mo2;
	}
}
pair<int,int> gethash(int l,int r){
	return make_pair(
		(hsh1[r]+mo1-1ll*hsh1[l-1]*P1[r-l+1]%mo1)%mo1,
		(hsh2[r]+mo2-1ll*hsh2[l-1]*P2[r-l+1]%mo2)%mo2
	);
}
void check_allsame(){
	for (int i=2;i<=n;i++)
		if (s[i]!=s[1]) return;
	printf("%d 1",n);
	exit(0);
}
void check_loop(int len){
	for (int i=len+1;i<=n;i+=len)
		if (gethash(1,len)!=gethash(i,i+len-1)) return;
	for (int i=1;i<=n;i++){
		for (int j=i+1;j+i-1<=n;j+=i){
			if (gethash(1,i)!=gethash(j,j+i-1)) break;
			ok[j+i-1]=1;
		}
		for (int j=n-i;j-i+1>=1;j-=i){
			if (gethash(n-i+1,n)!=gethash(j-i+1,j)) break;
			ok[j-i]=1;
		}
	}
	printf("2 ");
	int ans=0;
	for (int i=1;i<n;i++)
		ans+=!ok[i];
	printf("%d",ans);
	exit(0);
}
int main(){
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	scanf("%s",s+1);
	n=strlen(s+1);
	check_allsame();
	hash_init();
	for (int i=2;i<n;i++)
		if (n%i==0) check_loop(i);
	puts("1 1"); 
}
