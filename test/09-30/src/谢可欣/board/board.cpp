#include <iostream>
#include <cstdio>
#define int long long
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
const int mod=1e9+7;
const int N=1e5+3;
int f[5000][5000],a[5000][5000];
int fac[100005],inv[100005];
inline int power(int x,int k){
	int ans=1;
	for(;k;k>>=1,x=1LL*x*x%mod)if(k&1)ans=1LL*ans*x%mod;
	return ans;
}
int C(int n,int m) {
	return fac[n]%mod*inv[m]%mod*inv[n-m]%mod;
}
signed main()
{
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	int n=read(),m=read(),k=read();
	if(n<=3000&&m<=3000){
		for(int i=1;i<=k;i++) {
			int x=read(),y=read();
			a[x][y]=1;
		}
		f[1][1]=1;
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=m;j++) {
				if(i==1&&j==1)continue;
				if(!a[i][j]){
					f[i][j]=(f[i-1][j]+f[i][j-1])%mod;
				}
			}
		}
		/*for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++)cout<<f[i][j]<<" ";puts("");
		}*/
		cout<<f[n][m]%mod<<endl;
	}
	else{
		fac[0]=1;
		for(int i=1; i<=N; i++)fac[i]=(fac[i-1]*i)%mod;
		inv[N]=power(fac[N],mod-2)%mod;
		for(int i=N-1; i>=1; i--)inv[i]=inv[i+1]*(i+1)%mod;
		int mx=0;
		int mn=100000000;
		for(int i=1; i<=k; i++) {
			int x=read(),y=read();
			mn=min(mn,x);
			mx=max(mx,y);
		}
		int ans=C(m+n-2,m-1);
		for(int i=mn; i<=n; i++) {
			int sum=(1+n-i+1)*(n-i+1)/2;
			ans=(ans-C(i+mx-2,i-1)*sum+mod)%mod;
		}
		cout<<ans%mod<<endl;
	}
}
/*
5 5 2
4 1
4 2
*/
