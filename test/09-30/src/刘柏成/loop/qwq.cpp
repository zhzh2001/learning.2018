#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	// freopen("loop.in","r",stdin);
	// freopen("loop.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]=' ';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
ll h[500005],pw[500005];
void work(char *s,bool *ans){
	int n=strlen(s+1);
	pw[0]=1;
	for(int i=1;i<=n;i++){
		h[i]=(h[i-1]*237+s[i]-'a'+1)%mod;
		pw[i]=(pw[i-1]*237)%mod;
	}
	for(int i=1;i<=n;i++)
		for(int j=i*2;j<=n;j+=i){
			//[1,j-i],[i+1,j]
			ll v=(h[j]-h[i]*pw[j-i])%mod;
			if (v<0)
				v+=mod;
			if (v==h[j-i])
				ans[j]=1;
			else break;
		}
}
char s[500005];
bool f1[500005],f2[500005];
int main(){
	srand(time(0));
	init();
	int n=500000;
	// printf("%d\n",n);
	for(int i=1;i<=n/2;i++)
		s[i*2-1]='a',s[i*2]='b';
	bool flag=1;
	for(int i=2;i<=n;i++)
		if (s[i]!=s[1]){
			flag=0;
			break;
		}
	if (flag){
		printf("%d\n1\n",n);
		return 0;
	}
	work(s,f1);
	if (!f1[n]){
		printf("1\n1\n");
		return 0;
	}
	reverse(s+1,s+n+1);
	work(s,f2);
	int ans=0;
	for(int i=1;i<n;i++)
		if (!f1[i] && !f2[n-i])
			ans++;
	printf("2\n%d\n",ans);
	// fprintf(stderr,"%.5f\n",1.0*clock()/CLOCKS_PER_SEC);
}