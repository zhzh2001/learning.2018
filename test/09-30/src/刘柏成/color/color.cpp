#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=3102;
struct point{
	int x,y;
	bool operator <(const point & rhs)const{
		return x<rhs.x;
	}
}a[maxn];
int nums[maxn],cur;
int cnt[maxn],lst[maxn],nxt[maxn];
void del(int x){
	nxt[lst[x]]=nxt[x];
	lst[nxt[x]]=lst[x];
}
int main(){
	init();
	int h=readint(),w=readint(),n=readint();
	for(int i=1;i<=n;i++)
		a[i].x=readint(),a[i].y=readint();
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)
		nums[++cur]=a[i].y;
	sort(nums+1,nums+cur+1);
	cur=unique(nums+1,nums+cur+1)-nums-1;
	nums[cur+1]=w;
	for(int i=1;i<=n;i++)
		a[i].y=lower_bound(nums+1,nums+cur+1,a[i].y)-nums;
	int ans=0;
	for(int i=0;i<=cur;i++)
		ans=max(ans,h*2+(nums[i+1]-nums[i])*2);
	for(int i=1;i<=n;i++){
		memset(lst,0,sizeof(lst));
		memset(nxt,0,sizeof(nxt));
		memset(cnt,0,sizeof(cnt));
		for(int j=i+1;j<=n;j++)
			cnt[a[j].y]++;
		int qwq=0,now=0;
		for(int j=1;j<=cur;j++)
			if (cnt[j]){
				lst[j]=qwq;
				nxt[qwq]=j;
				now=max(now,nums[j]-nums[qwq]);
				qwq=j;
			}
		lst[cur+1]=qwq;
		nxt[qwq]=cur+1;
		now=max(now,w-nums[qwq]);
		// printf("i=%d now=%d\n",i,now);
		ans=max(ans,(h-a[i].x)*2+now*2);
		for(int j=n;j>i;j--){
			cnt[a[j].y]--;
			if (!cnt[a[j].y]){
				now=max(now,nums[nxt[a[j].y]]-nums[lst[a[j].y]]);
				del(a[j].y);
			}
			// printf("i=%d j=%d now=%d\n",i,j,now);
			ans=max(ans,now*2+(a[j].x-a[i].x)*2);
		}
	}
	printf("%d",ans);
}