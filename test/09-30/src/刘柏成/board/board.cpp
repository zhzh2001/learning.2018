#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int mod=1000000007;
ll power(ll x,ll y){
	ll ans=1;
	while(y){
		if (y&1)
			ans=ans*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return ans;
}
ll fac[200005],invf[200005];
void init(int n){
	fac[0]=1;
	for(int i=1;i<=n;i++)
		fac[i]=fac[i-1]*i%mod;
	invf[n]=power(fac[n],mod-2);
	for(int i=n;i;i--)
		invf[i-1]=invf[i]*i%mod;
}
struct point{
	int x,y;
	bool operator <(const point& rhs)const{
		return x==rhs.x?y<rhs.y:x<rhs.x;
	}
}a[3002];
ll dp[3002];
ll c(int n,int m){
	if (n<m || m<0)
		return 0;
	return fac[n]*invf[m]%mod*invf[n-m]%mod;
}
int main(){
	init();
	int h=readint(),w=readint(),n=readint();
	init(h+w);
	for(int i=1;i<=n;i++)
		a[i].x=readint(),a[i].y=readint();
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++){
		dp[i]=-c(a[i].x+a[i].y-2,a[i].x-1);
		// printf("dp[%d]=%lld\n",i,dp[i]);
		for(int j=1;j<i;j++)
			if (a[j].y<=a[i].y)
				dp[i]=(dp[i]-dp[j]*c(a[i].x-a[j].x+a[i].y-a[j].y,a[i].x-a[j].x))%mod;
		// printf("dp[%d]=%lld\n",i,dp[i]);
	}
	ll ans=c(h+w-2,h-1);
	// printf("%lld\n",ans);
	for(int i=1;i<=n;i++)
		ans=(ans+dp[i]*c(h-a[i].x+w-a[i].y,h-a[i].x))%mod;
	printf("%lld",(ans%mod+mod)%mod);
	// fprintf(stderr,"%.5f\n",1.0*clock()/CLOCKS_PER_SEC);	
}