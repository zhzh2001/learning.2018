#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
#include <queue>
#include <set>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
struct st{int x,y;}a[3005];
bool operator<(st a,st b){
	return a.x!=b.x?a.x<b.x:a.y<b.y;
}
priority_queue<int>d,era;
inline void ext(){
	while(!era.empty()&&d.top()==era.top()){
		d.pop();
		era.pop();
	}
}
inline bool empty(){
	ext();
	return d.empty();
}
inline void pop(){
	ext();
	d.pop();
}
inline int top(){
	ext();
	return d.top();
}
inline void erase(int v){
	era.push(v);
}
inline void push(int v){
	d.push(v);
}
set<int>sp;
int X,Y,n,p1,p2,ans;
int main(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	X=read();
	Y=read();
	n=read();
	ans=0;
	for(int i=1;i<=n;i++){
		a[i].x=read();
		a[i].y=read();
	}
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++){
		while(!empty())pop();
		push(Y);
		sp.clear();
		sp.insert(0);
		sp.insert(Y);
		for(int j=i+1;j<=n;j++){
			ans=max(ans,top()+a[j].x-a[i].x);
			set<int>::iterator k=sp.insert(a[j].y).first;
			k--;
			p1=*k;
			k++;
			k++;
			p2=*k;
			erase(p2-p1);
			push(a[j].y-p1);
			push(p2-a[j].y);
		}
	}
	printf("%d\n",ans+ans);
	return 0;
}