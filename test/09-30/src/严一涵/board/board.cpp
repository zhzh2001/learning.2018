#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const LL md=1000000007;
const LL MC=200000;
struct st{LL x,y;}a[3005];
LL h,w,n,f[3005],fac[200003],invfac[200003];
inline LL cc(LL m,LL n){
	return fac[m]*invfac[n]%md*invfac[m-n]%md;
}
void exgcd(LL a,LL b,LL&x,LL&y){
	if(b==0){x=1;y=0;return;}
	exgcd(b,a%b,y,x);
	y-=a/b*x;
}
inline LL inv(LL v){
	LL x,y;
	exgcd(v,md,x,y);
	return (x%md+md)%md;
}
bool operator<(st a,st b){
	return a.x!=b.x?a.x<b.x:a.y<b.y;
}
int main(){
	freopen("board.in","r",stdin);
	freopen("board.out","w",stdout);
	h=read();
	w=read();
	n=read();
	for(int i=1;i<=n;i++){
		a[i].x=read();
		a[i].y=read();
	}
	n++;
	a[n].x=h;
	a[n].y=w;
	fac[0]=1;
	for(int i=1;i<=MC;i++)fac[i]=fac[i-1]*i%md;
	invfac[MC]=inv(fac[MC]);
	for(int i=MC-1;i>=0;i--)invfac[i]=invfac[i+1]*(i+1)%md;
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++){
		f[i]=cc(a[i].x+a[i].y-2,a[i].x-1);
		for(int j=1;j<i;j++){
			f[i]-=cc(a[i].x-a[j].x+a[i].y-a[j].y,a[i].x-a[j].x)*f[j];
			f[i]%=md;
		}
	}
	printf("%lld\n",(f[n]+md)%md);
	return 0;
}