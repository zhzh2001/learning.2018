#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const LL md1=1000000007,md2=998244353;
char s[500003];
LL v1[500003],v2[500003];
LL tj1[500003],tj2[500003];
LL q[500003],q2[500003];
LL n,m,ans;
int main(){
	freopen("loop.in","r",stdin);
	freopen("loop.out","w",stdout);
	gets(s+1);
	n=strlen(s+1);
	tj1[0]=1;
	tj2[0]=1;
	for(int i=1;i<=n;i++){
		v1[i]=v1[i-1]*26+s[i]-'a';
		v1[i]%=md1;
		v2[i]=v2[i-1]*26+s[i]-'a';
		v2[i]%=md2;
		tj1[i]=tj1[i-1]*26%md1;
		tj2[i]=tj2[i-1]*26%md2;
	}
	for(int i=1;i<=n/2;i++)for(int j=i+i;j<=n;j+=i)if(!q[j]){
		if(v1[j-i]==((v1[j]-v1[i]*tj1[j-i])%md1+md1)%md1
		&&v2[j-i]==((v2[j]-v2[i]*tj2[j-i])%md2+md2)%md2){
			q[j]=i;
		}
	}
	if(q[n]==0){
		printf("1\n1\n");
		return 0;
	}else if(q[n]==1){
		printf("%lld\n1\n",n);
		return 0;
	}
	for(int i=1;i<=n/2;i++)swap(s[i],s[n-i+1]);
	tj1[0]=1;
	tj2[0]=1;
	for(int i=1;i<=n;i++){
		v1[i]=v1[i-1]*26+s[i]-'a';
		v1[i]%=md1;
		v2[i]=v2[i-1]*26+s[i]-'a';
		v2[i]%=md2;
		tj1[i]=tj1[i-1]*26%md1;
		tj2[i]=tj2[i-1]*26%md2;
	}
	for(int i=1;i<=n/2;i++)for(int j=i+i;j<=n;j+=i)if(!q2[j]){
		if(v1[j-i]==((v1[j]-v1[i]*tj1[j-i])%md1+md1)%md1
		&&v2[j-i]==((v2[j]-v2[i]*tj2[j-i])%md2+md2)%md2){
			q2[j]=i;
		}
	}
	ans=0;
	for(int i=1;i<=n-1;i++)if(!q[i]&&!q2[n-i]){
		ans++;
	}
	printf("2\n%lld\n",ans);
	return 0;
}