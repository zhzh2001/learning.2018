#include<iostream>
#include<algorithm>
using namespace std;
const int N=100005;
int a[N],l[N],r[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	for(int i=1;i<=n;i++)
		cin>>l[i]>>r[i];
	long long sum=0,mn=l[1],mx=r[1];
	for(int i=2;i<=n;i++)
		if(i&1)
		{
			sum=a[i]-a[i-1]-sum;
			long long nmx=r[i]-sum,nmn=l[i]-sum;
//			cout<<nmn<<' '<<nmx<<endl;
			mx=min(mx,nmx);
			mn=max(mn,nmn);
		}
		else
		{
			sum=a[i]-a[i-1]-sum;
			long long nmn=sum-r[i],nmx=sum-l[i];
//			cout<<nmn<<' '<<nmx<<endl;
			mx=min(mx,nmx);
			mn=max(mn,nmn);
		}
	cout<<max(mx-mn+1,0ll)<<endl;
	return 0;
}
