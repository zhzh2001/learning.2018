#include <fstream>
#include <random>
#include <windows.h>
using namespace std;
ofstream fout("meet.in");
const int n = 2e4;
int main()
{
	minstd_rand gen(GetTickCount());
	fout << n << endl;
	for (int i = 1; i <= n; i++)
	{
		uniform_int_distribution<> d(-1e9, 1e9);
		fout << d(gen) << ' ' << d(gen) << endl;
	}
	return 0;
}