#include <fstream>
using namespace std;
ifstream fin("wei.in");
ofstream fout("wei.out");
const int N = 1e6, MOD = 1e9 + 7;
int fact[N + 5], inv[N + 5], p[N + 5];
int qpow(long long a, int b)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % MOD;
		a = a * a % MOD;
	} while (b /= 2);
	return ans;
}
int main()
{
	fact[0] = p[0] = 1;
	for (int i = 1; i <= N; i++)
	{
		fact[i] = 1ll * fact[i - 1] * i % MOD;
		p[i] = p[i - 1] * 2 % MOD;
	}
	inv[N] = qpow(fact[N], MOD - 2);
	for (int i = N - 1; i >= 0; i--)
		inv[i] = 1ll * inv[i + 1] * (i + 1) % MOD;
	int t;
	fin >> t;
	while (t--)
	{
		int a, b;
		fin >> a >> b;
		fout << 1ll * p[a - b] * fact[a] % MOD * inv[a - b] % MOD * inv[b] % MOD << endl;
	}
	return 0;
}