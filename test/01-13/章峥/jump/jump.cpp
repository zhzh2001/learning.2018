#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("jump.in");
ofstream fout("jump.out");
const int N = 300005;
long long x[N];
int f[N], g[N], ans[N];
int main()
{
	int n, k;
	long long m;
	fin >> n >> k >> m;
	for (int i = 1; i <= n; i++)
		fin >> x[i];
	for (int i = 1; i <= n; i++)
	{
		long long l = 1, r = max(x[i] - x[1], x[n] - x[i]);
		int lm, rm;
		while (l < r)
		{
			long long mid = (l + r) / 2;
			int l2 = 1, r2 = i;
			while (l2 < r2)
			{
				int mid2 = (l2 + r2) / 2;
				if (x[i] - x[mid2] <= mid)
					r2 = mid2;
				else
					l2 = mid2 + 1;
			}
			int lmost = r2;
			l2 = i;
			r2 = n;
			int rmost = 0;
			while (l2 <= r2)
			{
				int mid2 = (l2 + r2) / 2;
				if (x[mid2] - x[i] <= mid)
				{
					rmost = mid2;
					l2 = mid2 + 1;
				}
				else
					r2 = mid2 - 1;
			}
			if (rmost - lmost >= k)
			{
				r = mid;
				lm = lmost;
				rm = rmost;
			}
			else
				l = mid + 1;
		}
		if (x[rm] - x[i] > x[i] - x[lm])
			f[i] = rm;
		else
			f[i] = lm;
	}
	for (int i = 1; i <= n; i++)
		ans[i] = i;
	do
	{
		if (m & 1)
			for (int i = 1; i <= n; i++)
				ans[i] = f[ans[i]];
		for (int i = 1; i <= n; i++)
			g[i] = f[f[i]];
		copy(g + 1, g + n + 1, f + 1);
	} while (m /= 2);
	for (int i = 1; i <= n; i++)
		fout << ans[i] << ' ';
	fout << endl;
	return 0;
}