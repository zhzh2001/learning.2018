#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
const int mod=1000000007;
int a,b,t;
ll ans;
ll exp2(int x)
{
	ll a=1;ll y=2;
	while (x)
	{
		if (x&1) a=a*y%mod;
		y=y*2%mod;
		x>>=1;
	}
	return a;
}
int main()
{
	freopen("wei.in","r",stdin);freopen("wei.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d",&a,&b);
		if (a==b)
		{
			printf("1\n");continue;
		}else
		if (b==1)
		{
			printf("%lld\n",exp2(a));
		}else
		if (b==2)
		{
			printf("%lld\n",(a*exp2(a)/2)%mod);
		}
		else printf("%lld\n",(a*b)%mod);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
