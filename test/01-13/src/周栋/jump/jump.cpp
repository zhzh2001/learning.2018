#include <iostream>
#include <cstdio>
#define ll long long
using namespace std;
const ll N=0x3f3f3f3f3f3f3f3f;
ll a[300010],n,m,k,near[300010],x,l,r;
inline ll solve(ll now,ll res,ll las)
{
	if (res==0) return now;
	if (near[now]==las)
	{
		if (res&1) return las;
		else return now;
	}
	return solve(near[now],res-1,now);
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%lld%lld%lld",&n,&k,&m);
	for (int i=1;i<=n;i++)
		scanf("%lld",&a[i]);
	a[0]=-N;a[n+1]=N;
	for (int i=1;i<=n;i++)
	{
		x=k;l=i-1;r=i+1;
		while (x>0)
		{
			if (a[i]-a[l]>a[r]-a[i]) 
			{
				near[i]=r;
				r++;
				x--;
			}
			if (x<=0) break;
			if (a[i]-a[l]<a[r]-a[i])
			{
				near[i]=l;
				l--;
				x--;
			}
			if (x<=0) break;
			if (a[i]-a[l]==a[r]-a[i])
			{
				near[i]=l;
				l--;
				r++;
				x-=2;
			}
			if (x<=0) break;
		}
	}
	printf("%lld",solve(1,m,-1));
	for (int i=2;i<=n;i++)
		printf(" %lld",solve(i,m,-1));
	return 0;
}
