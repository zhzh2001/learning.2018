#include <iostream>
#include <cstdio>
#include <cstring>
#define ll long long
using namespace std;
const ll N=1000000007;
ll t,a,b,num[150000];
ll tot=0,s[150000];
bool prime[2000010];
ll pow(ll x,ll y)
{
	if (y==0) return 1;
	ll k=1;
	while (y)
	{
		if (y&1) k=k*x%N;
		y=y/2;
		x=x*x%N;
	}
	return k;
}
ll get(ll a,ll b)
{
	memset(num,0,sizeof(num));
	ll c,t,maxp=1;
	num[1]=a-b;
	for (ll i=a-b+1;i<=a;i++)
	{
		c=i,t=1;
		while (c!=1)
		{
			while (c%s[t]==0)
			{
				c/=s[t];
				num[t]++;
			}
			t++;
		}
		maxp=max(maxp,t);
	}
	for (ll i=2;i<=b;i++)
	{
		c=i,t=1;
		while (c!=1)
		{
			while (c%s[t]==0)
			{
				c/=s[t];
				num[t]--;
			}
			t++;
		}
		maxp=max(maxp,t);
	}
	ll k=1;
	for (ll i=1;i<=maxp;i++)
			k=k*pow(s[i],num[i])%N;
	return k;
}
int main()
{
	freopen("wei.in","r",stdin);
	freopen("wei.out","w",stdout);
	for (ll i=2;i*i<=2000000;i++)
		if (!prime[i])
			for (ll j=2;i*j<=2000000;j++)
				prime[i*j]=1;
	for (ll i=2;i<=2000000;i++)
		if (!prime[i]) s[++tot]=i;
	scanf("%lld",&t);
	while (t--)
	{
		scanf("%lld%lld",&a,&b);
		printf("%lld\n",get(a,b));
	}
	return 0;
}
