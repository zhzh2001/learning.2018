#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x>9)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
const int N=300005;
ll a[N],b[N],to[N];
ll la,lb;
int n,m,k;
map<int,int>mmp;
/*inline bool check(int t,int x)
{
	ll tmp=a[t]-(a[x]-a[t]);
	int now=x-t;
	ll tt=*lower_bound(b+1,b+lb+1,tmp);
	if(tt!=a[t]){
		now+=t-mmp[tt];
		if(tt==tmp)now--;
	}
	if(now<=k)return true;
	else return false;
}
inline int get(int t)
{
	int l=t+1,r=n;
	int ans=t;
	while(l<=r){
		int mid=(l+r)>>1;
		if(check(t,mid))ans=mid,l=mid+1;
		else r=mid-1;
	}
	if(ans==t)return t-k;
	else {
		ll tmp=a[t]-(a[ans]-a[t]);
		ll tt=*lower_bound(b+1,b+lb+1,tmp);
		if(tt!=tmp)return ans;
		else return mmp[tt];
	}
}*/
int use[N],s[N],f[N],ans[N],cir[N];
inline void getans(int k)
{
	int t=0;
	memset(use,0,sizeof(use));
	while(!use[k]&&!cir[k]){use[k]=true;s[++t]=k;k=to[k];}
	if(t>m){
		ans[s[1]]=s[1+m];
		for(int i=2;i<=t&&!ans[s[i]];i++)
			ans[s[i]]=to[ans[s[i-1]]];
	}else if(cir[k]){
		ans[s[1]]=k;
		for(int i=(m-t)%cir[k];i;i--)
			ans[s[1]]=to[ans[s[1]]];
		for(int i=2;i<=t&&!ans[s[i]];i++)
			ans[s[i]]=to[ans[s[i-1]]];
	}else{
		int x=t;
		while(s[x]!=k)x--;
		for(int i=x;i<=t;i++)
			cir[s[i]]=t-x+1;
//		cout<<x<<' '<<cir[s[x]]<<' '<<x+(m-x+1)%cir[s[x]]<<endl;
		ans[s[1]]=s[x+(m-x+1)%cir[s[x]]];
		for(int i=2;i<=t&&!ans[s[i]];i++)
			ans[s[i]]=to[ans[s[i-1]]];
	}
}
inline int get(int t)
{
	int l=t-1;
	int r=t+1;
	int x=1;
	while(x<k){
		if(l<1)r++;
		else if(r>n)l--;
		else if(a[t]-a[l]>a[r]-a[t])r++;
		else l--;
		x++;
	}
//	cout<<t<<'!'<<l<<' '<<r<<endl;
	if(a[t]-a[l+1]==a[r]-a[t])return l+1;
	if(l<1)return r;
	else if(r>n)return l;
	else if(a[t]-a[l]>a[r]-a[t])return r;
	else return l;
}
//inline void getans(int k)
//{
//	ans[k]=k;
//	for(int i=1;i<=m;i++)
//		ans[k]=to[ans[k]];
//}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();k=read();m=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	
	to[1]=k+1;to[n]=n-k;
	for(int i=2;i<n;i++)
		to[i]=get(i);

//	for(int i=1;i<=n;i++)if(!f[i]&&!ans[i])getans(i);
//	for(int i=1;i<=n;i++)if(!ans[i])getans(i);
	for(int i=1;i<=n;i++)
		getans(i);
	for(int i=1;i<=n;i++)
		write(ans[i]),putchar(' ');
	return 0;
}
