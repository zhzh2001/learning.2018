#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x>9)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
const int N=100005;
struct xxx{ll x,y,id;}a[N];
ll n,ans,tot;
int x,y;
inline ll ab(ll x){if(x<0)return -x;else return x;}
struct aaa{ll now,k,ans;}b[15];
bool cmpk(aaa a,aaa b){return a.k<b.k;}
bool cmpans(aaa a,aaa b){return a.ans<b.ans;}
int main()
{
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i].x=read();a[i].y=read();
		x+=a[i].x,y+=a[i].y;
	}
	x/=n;y/=n;
	int t=min(50LL,n);
	for(int i=1;i<=t;i++){
		b[i].k=max(ab(a[i].x-x),ab(a[i].y-y));
		b[i].now=i;
	}
	sort(b+1,b+t+1,cmpk);
	for(int i=min(50LL,n)+1;i<=n;i++){
		int tmp=max(ab(a[i].x-x),ab(a[i].y-y));
		for(int j=1;j<=t;j++)
			if(tmp<b[j].k){
				for(int k=t;k>=j+1;k--)
					b[k]=b[k-1];
				b[j].now=i;b[j].k=tmp;
				break;
			}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=t;j++)
			b[j].ans+=max(ab(a[i].x-a[b[j].now].x),ab(a[i].y-a[b[j].now].y));
	} 
	sort(b+1,b+t+1,cmpans);
	writeln(b[1].ans);
}
