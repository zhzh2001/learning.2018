//#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<stdio.h>
#include<fstream>
#include<map>
//#define cin fin
//#define cout fout
using namespace std;
ifstream fin("jump3.in");
ofstream fout("jump3.out");
map<long long,int>mp;
int n,k,m;
long long x[300003];
long long f[300003][93];
int main(){
	fin>>n>>k>>m;
	for(int i=1;i<=n;i++){
		fin>>x[i];
		mp[x[i]]=i;
	}
	sort(x+1,x+n+1);
	//f[1][0]=3,f[2][0]=3,f[3][0]=1,f[4][0]=3,f[5][0]=3;
	for(int i=1;i<=n;i++){
		long long l=0,r=1e18,mid;
		while(l<r-1){
			mid=(l+r)/2;
			long long it1=*lower_bound(x+1,x+n+1,x[i]-mid);
			it1=mp[it1];
			long long it2=*upper_bound(x+1,x+n+1,mid+x[i]);
			if(mid+x[i]>=x[n]){
				it2=n;
			}else{
				it2=mp[it2]-1;
			}
			if(it2-it1>k+1){
				r=mid;
			}else{
				l=mid;
			}
		}
		mid=l-1;
		long long it1=*lower_bound(x+1,x+n+1,x[i]-mid);
		it1=mp[it1];
		long long it2=*upper_bound(x+1,x+n+1,mid+x[i]);
		if(mid+x[i]>=x[n]){
			it2=n;
		}else{
			it2=mp[it2]-1;
		}
		while(it2-it1>=k+1){
			if(x[i]-x[it1]>x[it2]-x[i]){
				it1++;
			}else{
				it2--;
			}
		}
		if(x[i]-x[it1]==x[it2]-x[i]){
			f[i][0]=it1;
		}else{
			f[i][0]=it2;
		}
		if(i==it1){
			f[i][0]=it2;
		}
		if(i==it2){
			f[i][0]=it1;
		}
	}
	f[1][0]=1+k;
	f[n][0]=n-k;
	for(int i=1;i<=90;i++){
		for(int j=1;j<=n;j++){
			f[j][i]=f[f[j][i-1]][i-1];
		}
	}
	for(int i=1;i<=n;i++){
		int now=i;
		for(long long j=90;j>=0;j--){
			if(k&(1ll<<j)){
				now=f[now][j];
			}
		}
		fout<<now<<" ";
	}
	return 0;
}
/*

in:
5 2 4
1 2 4 7 10

out:
1 1 3 1 1

*/
