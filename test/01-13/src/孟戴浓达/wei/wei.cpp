//#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<stdio.h>
#include<fstream>
using namespace std;
ifstream fin("wei.in");
ofstream fout("wei.out");
const long long mod=1e9+7;
long long T,a,b;
long long f[5003][5003];
long long fac[1000003];
long long inv[1000003];
long long qpow(long long a,long long b){
	if(b==1){
		return a;
	}
	long long fanhui=qpow(a,b/2);
	fanhui=fanhui*fanhui%mod;
	if(b%2==1){
		fanhui=fanhui*a%mod;
	}
	return fanhui;
}
inline long long C(long long a,long long b){
	if(b==0||a==0){
		return 0;
	}
	if(a==b){
		return 1;
	}
	return fac[a]*inv[b]%mod*inv[a-b]%mod;
}
int main(){
	fin>>T;
	f[1][1]=2,f[1][2]=1;
	/*
	fac[0]=1;
	for(int i=1;i<=1000000;i++){
		fac[i]=fac[i-1]*i%mod;
	}
	inv[1000000]=qpow(fac[1000000],mod-2);
	for(int i=1000000-1;i>=1;i--){
		inv[i]=inv[i+1]*(i+1)%mod;
	}
	*/
	for(int i=2;i<=5000;i++){
		f[i][1]=qpow(2,i);
		for(int j=2;j<=5000;j++){
			f[i][j]=(f[i-1][j]*2+f[i-1][j-1])%mod;
		}
	}
	while(T--){
		fin>>a>>b;
		//cout<<C(a+1,b-1)+2*C(a+1,b)<<endl;
		fout<<f[a][b+1]<<endl;
	}
	return 0;
}
