//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("meet.in");
ofstream fout("meet.out");
struct node{
	long long a,b,id;
}p[200003];
int n,x,y;
long long lx[200003],ly[200003],rx[200003],ry[200003];
inline bool cmpx(const node& x,const node& y){
	return x.a<y.a;
}
inline bool cmpy(const node& x,const node & y){
	return x.b<y.b;
}
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>x>>y;
		p[i].a=x-y,p[i].b=x+y,p[i].id=i;
	}
	sort(p+1,p+n+1,cmpx);
	for(int i=1;i<=n;i++){
		lx[p[i].id]=lx[p[i-1].id]+(p[i].a-p[i-1].a)*(i-1);
	}
	for(int i=n;i>=1;i--){
		ly[p[i].id]=ly[p[i+1].id]+(p[i+1].a-p[i].a)*(n-i);
	}
	sort(p+1,p+n+1,cmpy);
	for(int i=1;i<=n;i++){
		rx[p[i].id]=rx[p[i-1].id]+(p[i].b-p[i-1].b)*(i-1);
	}
	for(int i=n;i>=1;i--){
		ry[p[i].id]=ry[p[i+1].id]+(p[i+1].b-p[i].b)*(n-i);
	}
	long long ans=9999999999999999;
	for(int i=1;i<=n;i++){
		ans=min(ans,lx[i]+ly[i]+rx[i]+ry[i]);
	}
	fout<<ans/2<<endl;
	return 0;
}
/*

in:
6
0 0
2 0
-5 -2
2 -2
-1 2
4 0

out:
15

*/
