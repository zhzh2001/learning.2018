#include <cstdio>
#include <cstring>
#define MOD 1000000007
int n,x,y;
long long f[5105][5105],k[5105];
int main()
{
	freopen("wei.in","r",stdin);
	freopen("wei.out","w",stdout);
	memset(f,0,sizeof(f));
	f[0][0]=1;
	for(int i=1;i<=5100;i++)
		for(int j=0;j<=i;j++)
			f[i][j]=(f[i-1][j]+f[i-1][j-1])%MOD;
	memset(k,0,sizeof(k));
	k[0]=1;
	for(int i=1;i<=5100;i++)
		k[i]=(k[i-1]<<1)%MOD;
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	{
		scanf("%d%d",&x,&y);
		long long t=(f[x][y]*k[x-y])%MOD;
		printf("%lld\n",t);
	}
}