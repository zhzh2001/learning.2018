#include <cstdio>
#include <algorithm>
using namespace std;
long long x[100005],y[100005],ans,temp,n;
long long solve(int x,int y,int nx,int ny)
{
	nx=abs(nx-x);
	ny=abs(ny-y);
	return max(nx,ny);
}
int main()
{
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	scanf("%lld",&n);
	for(int i=1;i<=n;i++)
		scanf("%lld%lld",x+i,y+i);
	ans=0x3f3f3f3f3f3f3f3f;
	for(int i=1;i<=n;i++)
	{
		temp=0;
		for(int j=1;j<=n;j++)
		{
			if(i==j) continue;
			temp+=solve(x[i],y[i],x[j],y[j]);
		}
		if(temp<ans) ans=temp;
	}
	printf("%lld",ans);
	return 0;
}