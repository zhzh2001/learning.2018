program meet;
 uses math;
 var
  x,y:array[0..100001] of longint;
  i,j,n:longint;
  s,smax:int64;
 begin
  assign(input,'meet.in');
  assign(output,'meet.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
   readln(x[i],y[i]);
  smax:=1008208820;
  for i:=1 to n do
   begin
    s:=0;
    for j:=1 to n do
     inc(s,max(abs(x[i]-x[j]),abs(y[i]-y[j])));
    smax:=min(s,smax);
   end;
  writeln(smax);
  close(input);
  close(output);
 end.
