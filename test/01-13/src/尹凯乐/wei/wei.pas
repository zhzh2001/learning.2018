program wei;
 var
  binv,fac:array[0..1000001] of int64;
  a,b,i1,i,n,t,mot:longint;
 operator **(x,y:int64)z:int64;
  begin
   z:=1;
   while y>0 do
    begin
     if y and 1=1 then z:=z*x mod mot;
     y:=y>>1;
     x:=x*x mod mot;
    end;
  end;
 begin
  assign(input,'wei.in');
  assign(output,'wei.out');
  reset(input);
  rewrite(output);
  mot:=1000000007;
  n:=1000000;
  readln(t);
  binv[0]:=1;
  binv[1]:=1;
  fac[0]:=1;
  for i:=2 to n do
   binv[i]:=(mot-mot div i)*binv[mot mod i] mod mot;
  for i:=1 to n do
   binv[i]:=binv[i]*binv[i-1] mod mot;
  {for i:=1 to 9 do
   write(binv[i],' ');
  writeln(binv[10]);}
  for i:=1 to n do
   fac[i]:=fac[i-1]*i mod mot;
  for i1:=1 to t do
   begin
    readln(a,b);
    writeln(fac[a]*binv[b] mod mot*binv[a-b] mod mot*(2**(a-b)) mod mot);
   end;
  close(input);
  close(output);
 end.
