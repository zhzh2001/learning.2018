// #include<bits/stdc++.h>
// using namespace std;
// #define ll long long
// inline void read(ll &x){
// 	x = 0;bool f = false; char ch = getchar();
// 	while(ch < '0' || ch > '9'){if(ch == '-')f = true;ch = getchar();}
// 	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
// 	if(f) x = -x;
// }

// const ll N = 300005;
// ll left[N],X[N],lhead,ltail,rhead,rtail,addL,delR;
// ll n,k,m;
// ll f[N],nxt[N];

// int main(){
// 	read(n); read(k); read(m);
// 	read(delR);
// 	for(ll i = 1;i < n;++i) read(X[i]);
// 	rtail = n-1; rhead = 1;
// 	ltail = 0; lhead = 1;
// 	for(ll i = 1;i <= n;++i){
// 		ll rL = rhead,rR = rtail;		//右边找到第几个
// 		ll Ans = rhead,Ansl = ltail;
// 		while(rL <= rR){								//要在里面找到第一个比它大的数
// 			ll rmid = (rL+rR)/2;			//现在二分到的位置
// 			ll o = rmid-rhead+1;			//现在是第几个
// 			ll sum = X[rmid]-delR;

// 			ll lL = 1,lR = ltail,ans = ltail;
// 			while(lL <= lR){
// 				ll lmid = (lL+lR)/2;
// 				if(X[lmid]+addL >= sum){
// 					ans = lmid;
// 					lR = lmid-1;
// 				}
// 				else lL = lmid+1;
// 			}
// 			Ansl = ans;
// 			o += ans-lhead+1;
// 			if(o <= k){
// 				rL = rmid+1;
// 				Ans = rmid;
// 			}
// 			else rR = rmid-1;
// 		}
// 		// Ans 当前right的位置，Ansl 当前left的位置
// 		ll o = k-(Ans-rhead+1)-(Ansl-lhead+1);		//之后还要加的数
// 		if(o == 0){
// 			if(Ans+1 <= rtail && X[Ansl] <= X[Ans+1]) nxt[i] = Ans+2;
// 			else nxt[i] = Ansl;
// 		}
// 		else nxt[i] = Ansl+o;
// 		addL += X[rhead]-delR;
// 		delR = X[rhead++];
// 	}
// 	for(int i = 1;i <= n;++i)
// 		printf("%lld\n", nxt[i]);
// 	return 0;
// }

#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){
	ll x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 5005;
ll n,m,k;
ll x[N],nxt[N],f[N],len[N];

int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n = read(); k = read(); m = read();
	for(int i = 1;i <= n;++i) x[i] = read();
	for(int i = 1;i <= n;++i){
		int l = i,r = i;
		for(int j = 1;j <= k;++j){
			if(l == 1) ++r;
			else if(r == n) --l;
			else if(x[i]-x[l-1] <= x[r+1]-x[i]) --l;
			else ++r;
		}
		if(x[i]-x[l] >= x[r]-x[i]) nxt[i] = l;
		else nxt[i] = r; 
	}
	// for(int i = 1;i <= n;++i) printf("%lld\n", nxt[i]);
	for(int i = 1;i <= n;++i){
		f[i] = i;
		while(nxt[nxt[f[i]]] != f[i]){
			f[i] = nxt[f[i]];
			++len[i];
		}
	}
	for(int i = 1;i <= n;++i){
		ll ans;
		if(len[i] > m){
			ans = i;
			for(int j = 1;j <= m;++j)
				ans = nxt[ans];
		}
		else{
			ll o = m-len[i];
			ans = f[i];
			if(o%2 == 1) ans = nxt[ans];
		}
		printf("%lld ",ans);
	}
	return 0;
}