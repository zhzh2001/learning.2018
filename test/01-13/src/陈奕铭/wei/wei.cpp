#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){
	ll x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const ll N = 1000005,mod = 1e9+7;
ll che[N],rev[N];
void exgcd(ll a,ll b,ll &x,ll &y){
	if(b == 0){
		x = 1;
		y = 0;
		return;
	}
	exgcd(b,a%b,x,y);
	ll t = x;
	x = y;
	y = t-a/b*y;
}

ll quick(ll n){
	ll ans = 1,a = 2;
	for(;n;n >>= 1,a = a*a%mod)
		if(n&1) ans = ans*a%mod;
	return ans;
}

ll C(ll a,ll b){ return (che[a]*rev[b])%mod*rev[a-b]%mod; }

int main(){
	freopen("wei.in","r",stdin);
	freopen("wei.out","w",stdout);
	che[0] = 1;
	for(ll i = 1;i <= 1000000;++i) che[i] = che[i-1]*i%mod;
	ll y;
	exgcd(che[1000000],mod,rev[1000000],y);
	rev[1000000] %= mod;
	rev[1000000] = (rev[1000000]+mod)%mod;
	for(ll i = 999999;i >= 0;--i) rev[i] = rev[i+1]*(i+1)%mod;
	rev[0] = 1;
	ll T = read();
	ll a,b;
	while(T--){
		a = read(); b = read();
		ll sum = quick(a-b);
		sum = sum*C(a,a-b)%mod;
		printf("%lld\n", sum);
	}
	return 0;
}
