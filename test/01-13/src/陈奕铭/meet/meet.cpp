#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){
	ll x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 3005;
ll x[N],y[N];
ll n;
ll sum = 1e18;

inline ll Max(ll a,ll b){ return a > b ? a : b; }
inline ll Min(ll a,ll b){ return a < b ? a : b; }
inline ll Abs(ll a){ return a > 0 ? a : -a; }

int main(){
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;++i){
		x[i] = read(); y[i] = read();
	}
	for(int i = 1;i <= n;++i){
		ll num = 0;
		for(int j = 1;j <= n;++j)
			num += Max(Abs(x[j]-x[i]),Abs(y[i]-y[j]));
		sum = Min(num,sum);
	}
	printf("%lld\n",sum);
	return 0;
}