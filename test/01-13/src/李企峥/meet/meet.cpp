#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll T,n,ans,sum,a[100100],b[100100],x;
bool f[100100]; 
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	srand(time(0));
	n=read();
	For(i,1,n)
	{
		a[i]=read();b[i]=read();
	}
	T=7e7;
	T/=n;
	if(T>n)T=n;
	ans=1e17;
	while(T--)
	{
		x=rand()%n+1;
		while(f[x]==true)x=rand()%n+1;
		f[x]=true;
		sum=0;
		For(i,1,n)
			sum+=max(abs(a[i]-a[x]),abs(b[i]-b[x]));
		if(sum<ans)ans=sum;
	}
	cout<<ans<<endl;
	return 0;
}

