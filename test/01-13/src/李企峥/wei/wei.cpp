#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define mo 1000000007
using namespace std;
struct D{
	ll x,y;
}a[5000];
bool f[5010];
ll ans,n,m,zs,T;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void fj(int x,int y)
{
	For(i,1,zs)
	{
		while(x%a[i].x==0)
		{
			x/=a[i].x;
			a[i].y+=y;
		}
		if(x==1)return;
	}
	if(x>1)ans*=x,ans%=mo;
	return;
}
ll ksm(D a)
{
	ll lzq=1,z=a.x;
	while(a.y>0)
	{
		if(a.y%2==1)lzq=lzq*z%mo;
		z=z*z%mo;
		a.y/=2;
	}
	return lzq;
}
int main()
{
	freopen("wei.in","r",stdin);
	freopen("wei.out","w",stdout);
	T=read();
	memset(f,true,sizeof(f));
	For(i,2,5000)
	{
		if(f[i]==false)continue;
		zs++;
		a[zs].x=i;
		a[zs].y=0;
		For(j,2,5000/i)f[i*j]=false;
	}
	while(T--)
	{
		ans=1;
		For(i,1,zs)a[i].y=0;
		n=read();m=read();
		a[1].y=n-m;
		if(m<n-m)m=n-m;
		For(i,m+1,n)fj(i,1);
		For(i,2,n-m)fj(i,-1);
		For(i,1,zs)ans=(ans*ksm(a[i]))%mo;
		cout<<ans<<endl;
	}
	return 0;
}  
