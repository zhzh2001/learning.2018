#include<cstdio>
#include<iostream>
using namespace std;
long long f[5001][5001];
long long p=1e9+7;
int main(){
	freopen("wei.in","r",stdin);
	freopen("wei.out","w",stdout);
	int t;
	scanf("%d",&t);
	f[0][0]=1;
	for(int i=1;i<=5000;i++){
		f[i][i]=1;
		f[i][0]=f[i-1][0]*2%p;
		for(int j=1;j<i;j++)f[i][j]=(f[i-1][j-1]+f[i-1][j]*2)%p;
	}
	int a,b;
	while(t--){
		scanf("%d%d",&a,&b);
		printf("%lld\n",f[a][b]);
	}
}
