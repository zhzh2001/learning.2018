#include<cstdio>
#include<iostream>
#include<cmath>
#include<algorithm>
using namespace std;
int n,i,j;
long long ans=2e18,sum,dx,dy;
struct Node{
	long long x,y;
}node[100001];
bool cmp(Node a,Node b){
	if(a.x!=b.x)return a.x<b.x;
	return a.y<b.y;
}
int main(){
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++)scanf("%lld%lld",&node[i].x,&node[i].y);
	sort(node+1,node+n+1,cmp);
	for(i=1;i<=n;i++){
		sum=0;
		for(j=1;j<=n;j++){
			dx=abs(node[i].x-node[j].x);dy=abs(node[i].y-node[j].y);
			sum+=max(dx,dy);
			if(sum>ans)break;
		}
		if(sum<ans)ans=sum;
	}
	printf("%lld",ans);
}
