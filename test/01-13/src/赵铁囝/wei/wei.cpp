#include<iostream>
#include<cstdio>
using namespace std;

int t,a,b;
long long x,y,ans,sum[10000000];
const long long mod=1e9+7;

long long pow(long long x,long long y){
	long long ans;
	if(y>2){
		if(y%2==0){
			ans=pow(x,y/2)%mod;
			ans=ans*ans%mod;
		}else{
			ans=pow(x,y/2)*pow(x,y/2+1)%mod;
		}
	}else{
		if(y==1)ans=x%mod;
		if(y==2)ans=x*x%mod;
	}
	return ans;
}

int main(){
	freopen("wei.in","r",stdin);
	freopen("wei.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>t;
	sum[0]=1;
	for(int i=1;i<=1000000;i++){
		sum[i]=sum[i-1]*i%mod;
	}
	for(int p=1;p<=t;p++){
		cin>>a>>b;
		x=1;y=1;
		x=pow(2,a-b)*sum[a]%mod;
		x=x*pow(sum[b],mod-2)%mod;
		y=sum[a-b];
		cout<<x*pow(y,mod-2)%mod<<endl;
	}
	return 0;
}
