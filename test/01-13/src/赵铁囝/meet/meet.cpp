#include<iostream>
#include<cstdio>
#include<cmath>
using namespace std;

int n,xl,xr,yl,yr,mid,maxx,maxy,minx,miny,xi,yi;
int x[100000],y[100000];
long long ans;

long long get(int q){
	long long sum=0;
	for(int i=1;i<=n;i++){
		sum+=max(abs(x[i]-x[q]),abs(y[i]-y[q]));
	}
	return sum;
}

int main(){
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
    ios::sync_with_stdio(false);
    ans=1e9;
    cin>>n;
    maxx=-1e9;maxy=-1e9;minx=1e9;miny=1e9;
    xl=-1e9;xr=1e9;mid=(xl+xr)/2;
    for(int i=1;i<=n;i++){
    	cin>>x[i]>>y[i];
    }
    for(int i=1;i<=n;i++){
    	ans=min(ans,get(i));
    }
    cout<<ans;
	return 0;
}
