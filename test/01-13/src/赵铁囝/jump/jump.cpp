#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
using namespace std;

int n,k,m,num,now;
int a[1000],f[1000][1000],b[1000],c[1000];

int dfs(int x,int y){
	if(f[x][y]!=0)return f[x][y];
	if(x==0)f[x][y]=dfs(x+1,y-1);
	if(x>0){
		if(a[x+1]-a[x]>=a[x]-a[x-1])f[x][y]=dfs(x-1,y-1);else f[x][y]=dfs(x+1,y-1);
	}
	return f[x][y];
}

int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","r",stdout);
    cin>>n>>k>>m;
    for(int i=1;i<=n;i++){
    	cin>>a[i];
    }
    for(int i=1;i<=n;i++){
        dfs(1,k+m);
    }
    for(int i=1;i<=n;i++){
    	cout<<f[i][k+m]<<" ";
    }
    return 0;
}
