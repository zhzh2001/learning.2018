#include<bits/stdc++.h>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define ll long long
#define N 100005
using namespace std;
int n;ll ans[N],sum[N];
struct data{ll x,y,jz,zq,id;}a[N];
inline bool cmp1(data a,data b){return a.jz<b.jz;}
inline bool cmp2(data a,data b){return a.zq<b.zq;}
int main(){
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n) scanf("%lld%lld",&a[i].x,&a[i].y),a[i].jz=a[i].x+a[i].y,a[i].zq=a[i].x-a[i].y,a[i].id=i;
	sort(a+1,a+1+n,cmp1);
	For(i,1,n) sum[i]=sum[i-1]+a[i].jz;
	For(i,1,n) ans[a[i].id]+=(a[i].jz*i-sum[i])+(sum[n]-sum[i]-a[i].jz*(n-i));
	sort(a+1,a+1+n,cmp2);
	For(i,1,n) sum[i]=sum[i-1]+a[i].zq;
	For(i,1,n) ans[a[i].id]+=(a[i].zq*i-sum[i])+(sum[n]-sum[i]-a[i].zq*(n-i));
	ll mn=1ll<<60;
	For(i,1,n) mn=min(mn,ans[i]);
	printf("%lld\n",mn/2);
	return 0;
}//jzqsb
