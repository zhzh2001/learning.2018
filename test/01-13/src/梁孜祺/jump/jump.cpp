#include<bits/stdc++.h>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define ll long long
#define N 300005
using namespace std;
int ans[N],n,k,fa[N],q[N],top;ll x[N],m;
bool vis[N];
int sb[N],pre[N],sol[N];
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%lld",&n,&k,&m);k++;
	For(i,1,n) scanf("%lld",&x[i]);
	int l=1,r=k;fa[1]=k;
	For(i,2,n){
		while(l<i&&r+1<=n&&x[r+1]-x[i]<x[i]-x[l]) r++,l++;
//		cout<<l<<" "<<r<<endl;
		if(x[i]-x[l]>=x[r]-x[i]) fa[i]=l;
		else fa[i]=r;
	}
//	For(i,1,n) cout<<fa[i]<<" ";cout<<endl;
	For(i,1,n) if(!vis[i]){
		int x=i;top=0;q[++top]=i;
		while(!vis[x]){
			vis[x]=1;x=fa[x];
			q[++top]=x;
		}
		int l=0;
//		For(j,1,top) cout<<q[j]<<" ";cout<<endl;
		For(j,1,top-1) if(q[j]==q[top]){l=j;break;}
		if(l){
			For(j,l,top-1) pre[q[j+1]]=q[j],sol[q[j]]=q[j+1];
//			For(j,l,top-1) cout<<pre[q[j]]<<" "<<sol[q[j]]<<endl;
			int sz=top-l,jb=m%sz;
			sb[q[l]]=q[l];
			For(j,1,jb) sb[q[l]]=sol[sb[q[l]]];
			For(j,l+1,top-1) sb[q[j]]=sol[sb[q[j-1]]];
			Rep(j,l-1,1){
				if(l-j>m) break;
				sb[q[j]]=pre[sb[q[j+1]]];
				pre[q[j]]=q[j-1];
			}
			For(j,1,l-1){
				if(l-j<=m) break;
				sb[q[j]]=q[j+m];
				pre[q[j]]=q[j-1];
			}
		}else{
			Rep(j,top-1,1){
				if(m<top-j) break;
				sb[q[j]]=pre[sb[q[j+1]]];
				pre[q[j]]=q[j-1];
			}
			For(j,1,top-1){
				if(m>=top-j) break;
				sb[q[j]]=q[j+m];
				pre[q[j]]=q[j-1];
			}
		}
	}
	For(i,1,n) printf("%d ",sb[i]);
	return 0;
}
