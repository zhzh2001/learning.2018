#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read()
{
	int k = 0, f = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9')
	{
		if (ch == '-')
			f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9')
	{
		k = k * 10 + ch - '0';
		ch = getchar();
	}
	return k * f;
}
inline void write(int x)
{
	if (x < 0)
		putchar('-'), x = -x;
	if (x > 9)
		write(x / 10);
	putchar(x % 10 + '0');
}
inline void writeln(int x)
{
	write(x);
	puts("");
}
int n, k, m, b[1000010][2], ans[1000010], a[1000010];
signed main()
{
	freopen("jump.in", "r", stdin);
	freopen("jump.out", "w", stdout);
	n = read();
	k = read();
	m = read();
	for (int i = 1; i <= n; i++)
		a[i] = read(), ans[i] = i;
	b[1][0] = k + 1;
	int l = 1, r = k + 1;
	for (int i = 1; i <= n; i++)
	{
		while (r < n && a[i] - a[l] > a[r + 1] - a[i])
			l++, r++;
		b[i][0] = (a[i] - a[l] < a[r] - a[i]) ? r : l;
	}
	for (int i = 0; (1ll << i) <= m; i++)
	{
		if (m & (1ll << i))
			for (int j = 1; j <= n; j++)
				ans[j] = b[ans[j]][i & 1];
		for (int j = 1; j <= n; j++)
			b[j][i & 1 ^ 1] = b[b[j][i & 1]][i & 1];
	}
	for (int i = 1; i <= n; i++)
		write(ans[i]), putchar(' ');
	return 0;
}
