#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD = 1e9 + 7;
inline int read()
{
	int k = 0, f = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9')
	{
		if (ch == '-')
			f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9')
	{
		k = k * 10 + ch - '0';
		ch = getchar();
	}
	return k * f;
}
inline void write(int x)
{
	if (x < 0)
		putchar('-'), x = -x;
	if (x > 9)
		write(x / 10);
	putchar(x % 10 + '0');
}
inline void writeln(int x)
{
	write(x);
	puts("");
}
inline int mi(int a, int b)
{
	int x = 1, y = a;
	while (b)
	{
		if (b & 1)
			x = x * y % MOD;
		y = y * y % MOD;
		b >>= 1;
	}
	return x;
}
int jie[1000010];
inline int C(int x, int y) { return jie[x] * mi(jie[y], MOD - 2) % MOD * mi(jie[x - y], MOD - 2) % MOD; }
signed main()
{
	freopen("wei.in", "r", stdin);
	freopen("wei.out", "w", stdout);
	jie[0] = 1;
	for (int i = 1; i <= 1000000; i++)
		jie[i] = jie[i - 1] * i % MOD;
	int T = read();
	while (T--)
	{
		int x = read(), y = read();
		writeln(C(x, y) * mi(2, x - y) % MOD);
	}
	return 0;
}
