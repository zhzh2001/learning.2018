#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read()
{
	int k = 0, f = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9')
	{
		if (ch == '-')
			f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9')
	{
		k = k * 10 + ch - '0';
		ch = getchar();
	}
	return k * f;
}
inline void write(int x)
{
	if (x < 0)
		putchar('-'), x = -x;
	if (x > 9)
		write(x / 10);
	putchar(x % 10 + '0');
}
inline void writeln(int x)
{
	write(x);
	puts("");
}
struct ppap
{
	int x, y;
} a[100010];
int x[100010], y[100010], sx[100010], sy[100010], n;
signed main()
{
	freopen("meet.in", "r", stdin);
	freopen("meet.out", "w", stdout);
	n = read();
	for (int i = 1; i <= n; i++)
	{
		int X = read(), Y = read();
		x[i] = a[i].x = X + Y;
		y[i] = a[i].y = X - Y;
	}
	sort(x + 1, x + n + 1);
	sort(y + 1, y + n + 1);
	for (int i = 1; i <= n; i++)
		sx[i] = sx[i - 1] + x[i], sy[i] = sy[i - 1] + y[i];
	int ans = 1e18, sum;
	for (int i = 1; i <= n; i++)
	{
		sum = 0;
		int p1 = lower_bound(x + 1, x + n + 1, a[i].x) - x, p2 = lower_bound(y + 1, y + n + 1, a[i].y) - y;
		sum += a[i].x * (p1 - 1) - sx[p1 - 1] + sx[n] - sx[p1] - a[i].x * (n - p1);
		sum += a[i].y * (p2 - 1) - sy[p2 - 1] + sy[n] - sy[p2] - a[i].y * (n - p2);
		ans = min(ans, sum);
	}
	writeln(ans / 2);
	return 0;
}
