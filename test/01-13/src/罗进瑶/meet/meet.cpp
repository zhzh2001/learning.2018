#include<iostream>
#include<cstdio>
#include<cstring>
#define INF 0x3f3f3f3f
const int maxn=100010;
int n,x[maxn],y[maxn],ans=0x3f3f3f3f;
using namespace std;
int abs(int a){
	return a>0?a:-a; 
}
inline int min(int a,int b){
	return a<b?a:b; 
}
int dist(int x1,int y1,int x2,int y2){
	if(abs(x1-x2)==abs(y1-y2))return abs(x1-x2);
	else return abs(x1-x2)+abs(y1-y2)-min(abs(x1-x2),abs(y1-y2));
}
int main()
{
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	cin>>n;
	for(int i=0;i<n;i++){

		cin>>x[i]>>y[i];
	}
/*	for(int i=0;i<n;i++)
	{
		for(int j=0;j<n;j++)printf("%d ",dist[i][j]);
		cout<<endl;
	}*/
	for(int i=0;i<n;i++){
		int t=0;
		for(int j=0;j<n;j++)if(i!=j)t+=dist(x[i],y[i],x[j],y[j]);
		if(t<ans)ans=t;
	}
	cout<<ans<<endl;
	return 0;
}
