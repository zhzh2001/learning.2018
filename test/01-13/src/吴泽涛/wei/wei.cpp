#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j;i<=k;i++)
#define Dow(i, j, k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std;

inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0' || ch>'9') { if(ch=='-') f = -1; ch=getchar(); }
	while(ch>='0' && ch<='9') { x = x * 10+ch-48; ch=getchar(); }
	return x * f;
}
void write(int x) {
	if(x<0) putchar('-'),x=-x;
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void writeln(int x) {
	write(x);
	putchar('\n');
}

const int mod = 1e9+7; 
const int N = 1000011, alpha = 1000000; 
int A,B,T,now,ans,sum; 
int a[N],f[N],t[40],w[N],D[N]; 

inline int ksm(int x) {
	int res = 1;
	Dow(i, now, 1) {
		res = 1ll*res*res%mod; 
		if(t[i]) res = 1ll*res*x%mod; 
	}
	return res;
}

int main() {
	freopen("wei.in","r",stdin); 
	freopen("wei.out","w",stdout); 
	int x = mod-2; 
	while(x) {
		t[++now] = x&1; 
		x/=2; 
	}
	a[0] = 2; 
	For(i, 1, alpha) a[i]=a[i-1]+2; 
	For(i, 0, alpha) D[i]=a[i]; 
	For(i, 1, alpha) a[i]=1ll*a[i]*a[i-1]%mod; 
	
	w[-1] = 1; 
	w[alpha] = ksm(a[alpha]);
	Dow(i, alpha-1, 0) 
		w[i] = 1ll*w[i+1]*D[i+1] %mod; 
	
	f[0] = 1; f[1] = 1;
	f[alpha] = 1;
	For(i, 2, alpha)  f[alpha] = 1ll*f[alpha]*i %mod; 
	f[alpha] = ksm(f[alpha]);
	Dow(i, alpha-1, 2) 
		f[i] = 1ll*f[i+1]*(i+1) %mod; 
	
	T = read(); 
	while(T--) {
		A = read(); B = read(); 
		if(A == B) {
			puts("1"); 
			continue; 
		}
		ans = 1ll*a[A-1]*w[B-1] %mod *f[A-B] %mod;  
		writeln(ans); 
	}
	return 0; 
}



