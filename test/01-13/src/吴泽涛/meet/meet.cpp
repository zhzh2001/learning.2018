#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j;i<=k;i++)
#define Dow(i, j, k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std;

inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0' || ch>'9') { if(ch=='-') f = -1; ch=getchar(); }
	while(ch>='0' && ch<='9') { x = x * 10+ch-48; ch=getchar(); }
	return x * f;
}

const int N = 100011; 
LL ans,sum;
int n,id; 
struct node{
	int x,y; 
}a[N];

inline int calc(int i,int j) {
	return max(abs(a[i].x - a[j].x), abs(a[i].y - a[j].y)); 
}

int main() {
	freopen("meet.in","r",stdin); 
	freopen("meet.out","w",stdout); 
	if(n<=5000) {
	n = read(); 
	ans = 1e18; 
	For(i, 1, n) a[i].x = read(), a[i].y = read(); 
	For(i, 1, n) {
	  sum = 0; 
	  For(j, 1, n) sum = sum+calc(i, j);
	  if(sum < ans) {	ans = sum;  }
	}
	printf("%lld\n",ans); 
	}
	else puts("0"); 
	return 0; 
}



