#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j;i<=k;i++)
#define Dow(i, j, k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std;

inline LL read() {
	LL x = 0, f = 1;
	char ch = getchar();
	while(ch<'0' || ch>'9') { if(ch=='-') f = -1; ch=getchar(); }
	while(ch>='0' && ch<='9') { x = x * 10+ch-48; ch=getchar(); }
	return x * f;
}
void write(LL x) {
	if(x<0) putchar('-'),x=-x;
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void writeln(LL x) {
	write(x);
	putchar('\n');
}

const int N = 300011; 
int st[65][N],t[65]; 
int n,K; 
LL m; 
LL s[N]; 

inline void OUTT() {
	int now = 0; 
	while(m) {
		t[now++] = m&1; 
		m/=2; 
	} 
	For(i, 1, n) {
		int ans = i; 
		For(j, 0, now-1) {
			if(t[ j ]) ans = st[j][ans]; 
		}
		write(ans); putchar(' '); 
	}
}

int main() {
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout); 
	n = read(); K = read(); m = read(); 
	For(i, 1, n) s[i] = read(); 
	st[0][1] = K+1; 
	int l = 1, r = K+1; 
	For(i, 2, n) {
		while(l < i&&r+1 <= n&& s[i]-s[l]>s[r+1]-s[i]) 
			l++, r++; 
		if(s[i]-s[l] >= s[r]-s[i]) st[0][i] = l; 
			else st[0][i] = r; 
	} 

	For(j, 1, 60) 
	  For(i, 1, n) 
	    st[j][i] = st[j-1][ st[j-1][i] ]; 
	OUTT();
	return 0; 
}



