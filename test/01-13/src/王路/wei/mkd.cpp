#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int Uniform(int l, int r) {
  return rand() % (r - l + 1) + l;
}

int main() {
  freopen("wei.in", "w", stdout);
  srand(GetTickCount());
  int T = 10000, limit = 5000;
  printf("%d\n", T);
  while (T--) {
    int a = Uniform(1, limit), b = Uniform(1, a);
    printf("%d %d\n", a, b);
  }
  return 0;
}