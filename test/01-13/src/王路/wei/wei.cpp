#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cassert>
using namespace std;

const int kMod = 1e9 + 7, kMaxN = 1000005;

long long fact[kMaxN], rfact[kMaxN];

inline long long QuickPow(long long x, long long y) {
  long long ret = 1;
  for (; y; y >>= 1, x = x * x % kMod) {
    if (y & 1)
      ret = ret * x % kMod;
  }
  return ret;
}

int Solve1(int T) {
  while (T--) {
    int a, b;
    scanf("%d%d", &a, &b);
    int offset = a - b;
    long long ans = 1;
    for (int i = 1; i <= offset; ++i) {
      ans = ans * (2LL * a - 2LL * (i - 1)) % kMod;
    }
    printf("%lld\n", ans * rfact[offset] % kMod);
  }
  return 0;
}

int Solve2(int T) {

  return 0;
}

int main() {
  freopen("wei.in", "r", stdin);
  freopen("wei.out", "w", stdout);
  int T;
  scanf("%d", &T);
  fact[0] = rfact[0] = 1;
  for (int i = 1; i < kMaxN; ++i)
    fact[i] = fact[i - 1] * i % kMod;
  rfact[kMaxN - 1] = QuickPow(fact[kMaxN - 1], kMod - 2);
  for (int i = kMaxN - 2; i >= 1; --i)
    rfact[i] = rfact[i + 1] * (i + 1) % kMod;
  if (T <= 1000)
    return Solve1(T);
  return Solve2(T);
}