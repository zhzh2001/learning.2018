#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

const int kMaxN = 100005;
const long long kInfL = 1LL << 62;
int n;

struct Node { 
  int x, y, id; 
} a[kMaxN];

inline bool cmp1(const Node &a, const Node &b) {
  return a.x < b.x;
}

inline bool cmp2(const Node &a, const Node &b) {
  return a.y < b.y;
}

int main() {
  freopen("meet.in", "r", stdin);
  freopen("meet.out", "w", stdout);
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) {
    scanf("%d%d", &a[i].x, &a[i].y);
  }
  if (n <= 3000) {
    long long ans = kInfL;
    for (int i = 1; i <= n; ++i) {
      long long res = 0;
      for (int j = 1; j <= n; ++j) {
        res = (res + (long long)max(abs(a[j].x - a[i].x), abs(a[j].y - a[i].y)));
      }
      ans = min(ans, res);
    }
    printf("%lld\n", ans);
    return 0;
  }
  
  return 0;
}