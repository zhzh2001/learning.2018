#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

namespace IO {
static const int SZ = 1 << 20;
static char buf[SZ], *S = buf, *T = buf;
inline char nc() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), SZ, stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int>
inline void Read(Int &x) {
  x = 0;
  char ch;
  Int flag = 1;
  for (ch = nc(); isspace(ch); ch = nc())
    ;
  if (ch == '-') {
    ch = nc();
    flag = -1;
  }
  for (; isdigit(ch); ch = nc())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // IO

using IO::Read;

const int kMaxN = 300005, kInf = 0x3f3f3f3f;
const long long kInfL = 1LL << 62;
int n, k, m, nxt[kMaxN], cyc[kMaxN];
long long x[kMaxN], off[kMaxN];
int vis[kMaxN];

int main() {
  freopen("jump.in", "r", stdin);
  freopen("jump.out", "w", stdout);
  Read(n), Read(k), Read(m);
  for (int i = 1; i <= n; ++i) {
    Read(x[i]);
    off[i] = x[i] - x[i - 1];
  }
  off[1] = off[n + 1] = kInfL;
  if (n <= 5000) {
    for (int i = 1; i <= n; ++i) {
      long long l = i, r = i + 1, ls = 0, rs = 0, cnt = 0, lastpos = l, first = 0;
      while (cnt < k) {
        long long c1 = ls + off[l], c2 = rs + off[r];
        if (c1 < c2) {
          ++cnt;
          ls = c1;
          lastpos = --l;
        } else if (c1 > c2) {
          ++cnt;
          rs = c2;
          lastpos = r++;
        } else if (c1 == c2) {
          ++cnt;
          rs = c2;
          if (!first)
            first = r;
          r++;
          lastpos = (l - 1 > 0) ? (first = 0, l - 1) : first;
        }
      }
      nxt[i] = lastpos;
    }
    for (int i = 1; i <= n; ++i) {
      int pcnt = 1, pos = nxt[i];
      while (pos != i && pcnt < m) {
        ++pcnt;
        pos = nxt[pos];
      }
      if (pcnt == m)
        printf("%d%c", pos, " \n"[i == n]);
      else {
        //printf("loop %d %d\n", i, pcnt);
        int need = m % pcnt;
        pcnt = 0, pos = i;
        while (pcnt < need) {
          ++pcnt;
          pos = nxt[pos];
        }
        printf("%d%c", pos, " \n"[i == n]);
      }
    }
  }
  return 0;
}