#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <math.h>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
    if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL a[300003],n,m,o,p,q;
int f[300003][63];
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();
	o=read();
	m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1,j=1;i<=n;i++){
		if(j+o<i)j=i-o;
		while(j+o<n&&a[i]-a[j]>a[j+o+1]-a[i])j++;
		if(a[j+o]-a[i]>a[i]-a[j])f[i][0]=j+o;else f[i][0]=j;
	}
	for(int j=1;j<=60;j++)for(int i=1;i<=n;i++)f[i][j]=f[f[i][j-1]][j-1];
	for(int i=1;i<=n;i++){
		p=m;q=i;
		for(int j=60;j>=0;j--)if((1ll<<j)<=p){p-=(1ll<<j);q=f[q][j];}
		printf("%lld ",q);
	}
	return 0;
}
