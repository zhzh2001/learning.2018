#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <math.h>
using namespace std;
typedef long long LL;
const int md=1000000007;
#define dd c=getchar()
inline int read(){int a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
    if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
int a,b,T,f[5003][5003],x,y;
int main(){
	freopen("wei.in","r",stdin);
	freopen("wei.out","w",stdout);
	f[0][0]=1;
	for(int i=1;i<=5000;i++){
		f[i][0]=1;
		for(int j=1;j<=5000;j++)f[i][j]=(f[i-1][j]+f[i-1][j-1]*2%md)%md;
	}
	T=read();
	while(T--){
		a=read();
		b=read();
		printf("%d\n",f[a][a-b]);
	}
	return 0;
}
