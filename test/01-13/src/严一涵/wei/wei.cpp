#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <math.h>
using namespace std;
typedef long long LL;
const LL md=1000000007;
#define dd c=getchar()
inline int read(){int a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
    if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL a,b,T,x,y;
LL v[1000003],v2[1000003];
LL exgcd(LL a,LL b,LL&x,LL&y){
	if(b==0){x=1;y=0;return a;}
	LL c=exgcd(b,a%b,y,x);
	y-=a/b*x;
}
LL e2p(LL n){
	if(n==0)return 1;
	if(n==1)return 2;
	LL ans=e2p(n/2);
	if(n%2==1)return ans*ans%md*2%md;else return ans*ans%md;
}
int main(){
	freopen("wei.in","r",stdin);
	freopen("wei.out","w",stdout);
	v[0]=1;v2[0]=1;
	for(int i=1;i<=1000000;i++){
		v[i]=v[i-1]*i%md;
		exgcd(v[i],md,v2[i],x);
	}
	T=read();
	while(T--){
		a=read();
		b=a-read();
		printf("%lld\n",(e2p(b)*v[a]%md*v2[a-b]%md*v2[b]%md+md)%md);
	}
	return 0;
}
