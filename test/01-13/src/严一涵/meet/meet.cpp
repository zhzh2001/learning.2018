#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <math.h>
using namespace std;
typedef long long LL;
const LL oo=1ll<<60;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
    if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL x[3003],y[3003],n,ans,ret;
int main(){
	freopen("meet.in","r",stdin);
	freopen("meet.out","w",stdout);
	n=read();ans=oo;
	for(int i=1;i<=n;i++){x[i]=read();y[i]=read();}
	for(int i=1;i<=n;i++){
		ret=0;
		for(int j=1;j<=n;j++)ret+=max(abs(x[i]-x[j]),abs(y[i]-y[j]));
		ans=min(ans,ret);
	}
	printf("%lld",ans);
	return 0;
}
