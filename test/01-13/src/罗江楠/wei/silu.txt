   |0	1	2	3	4
---+--------------------
0  |1
1  |2	1
2  |4	4	1
3  |8	12	6	1
4  |16	32	24	8	1

f(4, 0) = 8 * f(3, 0) / 4
f(4, 1) = 8 * f(3, 1) / 3
f(4, 2) = 8 * f(3, 2) / 2
f(4, 3) = 8 * f(3, 3) / 1

f(x, y) = 2xf(x - 1, y) / (x - y), x > y
        = 1,                       x = y

f(x, y) = 2 ^ (x - y) * x! / (x - y)! / y!

sin2x    = 2sinxcosx
cos(x+y) = cosxcosy-sinxsiny
cos(x-y) = cosxcosy+sinxsiny
