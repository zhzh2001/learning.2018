#include <bits/stdc++.h>
#define mod 1000000007
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
long long fast_pow(long long x, long long y) {
	long long c = 1;
	for (; y; y >>= 1, x = x * x % mod)
		if (y & 1) c = c * x % mod;
	return c;
}
long long inv(long long x) {
	return fast_pow(x, mod - 2);
}
long long frac[1000020];
long long calc(long long x, long long y) {
	long long fz = fast_pow(2, x - y) * frac[x] % mod;
	long long fm = frac[x - y] * frac[y] % mod;
	return fz * inv(fm) % mod;
}
int main(int argc, char const *argv[]) {
	freopen("wei.in", "r", stdin);
	freopen("wei.out", "w", stdout);
	frac[0] = 1;
	for (int i = 1; i <= 1000000; i++)
		frac[i] = frac[i - 1] * i % mod;
	int n = read();
	while (n --) {
		int x = read(), y = read();
		cout << calc(x, y) << endl;
	}
	return 0;
}