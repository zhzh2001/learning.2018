#include <bits/stdc++.h>
using namespace std;
int main(int argc, char const *argv[]) {
	srand(time(0));
	freopen("wei.in", "w", stdout);
	int n = 10000, m = 1000000;
	cout << n << endl;
	for (int i = 1; i <= n; i++) {
		int x = rand() * rand() % m + 1; // [1, m]
		int y = rand() * rand() % x + 1; // [1, x]
		cout << x << " " << y << endl;
	}
	return 0;
}