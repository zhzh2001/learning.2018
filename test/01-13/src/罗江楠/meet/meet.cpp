#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
struct point {
	long long x, y;
	int id;
} v[N];
bool cmpx(point a, point b) {
	return a.x == b.x ? a.y < b.y : a.x < b.x;
}
bool cmpy(point a, point b) {
	return a.y == b.y ? a.x < b.x : a.y < b.y;
}
long long a[N], b[N], l, r;
int main(int argc, char const *argv[]) {
	freopen("meet.in", "r", stdin);
	freopen("meet.out", "w", stdout);
	int n = read();
	for (int i = 1; i <= n; i++) {
		int a = read(), b = read();
		v[i].x = a - b;
		v[i].y = a + b;
		v[i].id = i;
	}

	l = r = 0;
	sort(v + 1, v + n + 1, cmpx);
	for (int i = 1; i <= n; i++)
		r += v[i].x - v[1].x;
	for (int i = 1; i <= n; i++) {
		if (i > 1 && v[i].x != v[i - 1].x) {
			l += (v[i].x - v[i - 1].x) * (i - 1);
			r -= (v[i].x - v[i - 1].x) * (n - i + 1);
		}
		// printf("a :: i = %4lld, l = %8lld, r = %8lld\n", i, l, r);
		a[v[i].id] = l + r;
	}

	l = r = 0;
	sort(v + 1, v + n + 1, cmpy);
	for (int i = 1; i <= n; i++)
		r += v[i].y - v[1].y;
	for (int i = 1; i <= n; i++) {
		if (i > 1 && v[i].y != v[i - 1].y) {
			l += (v[i].y - v[i - 1].y) * (i - 1);
			r -= (v[i].y - v[i - 1].y) * (n - i + 1);
		}
		// printf("b :: i = %4lld, l = %8lld, r = %8lld\n", i, l, r);
		b[v[i].id] = l + r;
	}

	long long ans = 1ll << 60;
	for (int i = 1; i <= n; i++)
		ans = min(ans, (a[i] + b[i]) / 2);

	cout << ans << endl;
	return 0;
}