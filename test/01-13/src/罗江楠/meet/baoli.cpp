#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
long long x[N], y[N], ans = 1ll << 60;
int main(int argc, char const *argv[]) {
	freopen("meet.in", "r", stdin);
	freopen("meet.ans", "w", stdout);
	int n = read();
	for (int i = 1; i <= n; i++) {
		x[i] = read();
		y[i] = read();
	}
	for (int i = 1; i <= n; i++) {
		long long sum = 0;
		for (int j = 1; j <= n; j++)
			sum += max(abs(x[i] - x[j]), abs(y[i] - y[j]));
		ans = min(ans, sum);
	}
	cout << ans << endl;
	return 0;
}