#include <bits/stdc++.h>
#define N 300020
using namespace std;
inline long long read() {
	long long x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int f[N], g[N];
long long x[N], b[N];
map <long long, int> mp;
int main(int argc, char const *argv[]) {
	freopen("jump.in", "r", stdin);
	freopen("jump.out", "w", stdout);
	int n = read(), k = read(), m = read();
	for (int i = 1; i <= n; i++) {
		x[i] = read();
		mp[x[i]] = i;
	}
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++)
			b[j] = abs(x[i] - x[j]);
		sort(b + 1, b + n + 1);
		long long misaka = b[k + 1];
		f[i] = mp[x[i] - misaka] ? mp[x[i] - misaka] : mp[x[i] + misaka];
	}
	for (int i = 1; i <= n; i++)
		g[i] = i;
	while (m --)
		for (int i = 1; i <= n; i++)
			g[i] = f[g[i]];
	for (int i = 1; i <= n; i++)
		printf("%d ", g[i]);
	puts("");
	return 0;
}