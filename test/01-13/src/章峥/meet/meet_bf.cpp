#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("meet.in");
ofstream fout("meet.ans");
const int N = 100005;
int x[N], y[N];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> x[i] >> y[i];
	long long ans = 1e18;
	for (int i = 1; i <= n; i++)
	{
		long long now = 0;
		for (int j = 1; j <= n; j++)
			now += max(abs(x[i] - x[j]), abs(y[i] - y[j]));
		ans = min(ans, now);
	}
	fout << ans << endl;
	return 0;
}