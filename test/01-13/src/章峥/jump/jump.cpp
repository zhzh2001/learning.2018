#include <cstdio>
#include <cctype>
#include <algorithm>
using namespace std;
FILE *fin = fopen("jump.in", "r"), *fout = fopen("jump.out", "w");
const int SZ = 1e6;
char buf[SZ], *p = buf, *pend = buf;
inline int nextchar()
{
	if (p == pend)
	{
		pend = (p = buf) + fread(buf, 1, SZ, fin);
		if (pend == buf)
			return EOF;
	}
	return *p++;
}
template <typename Int>
inline void read(Int &x)
{
	char c = nextchar();
	for (; isspace(c); c = nextchar())
		;
	x = 0;
	Int sign = 1;
	if (c == '-')
	{
		sign = -1;
		c = nextchar();
	}
	for (; isdigit(c); c = nextchar())
		x = x * 10 + c - '0';
	x *= sign;
}
inline void writechar(char c)
{
	if (p == pend)
	{
		fwrite(buf, 1, SZ, fout);
		p = buf;
	}
	*p++ = c;
}
inline void flush()
{
	fwrite(buf, 1, p - buf, fout);
}
int dig[20];
template <typename Int>
inline void writeln(Int x)
{
	if (x < 0)
	{
		writechar('-');
		x = -x;
	}
	int len = 0;
	do
		dig[++len] = x % 10;
	while (x /= 10);
	for (; len; len--)
		writechar(dig[len] + '0');
	writechar(' ');
}
const int N = 300005;
long long x[N];
int f[N], g[N], ans[N];
int main()
{
	int n, k;
	long long m;
	read(n);
	read(k);
	read(m);
	for (int i = 1; i <= n; i++)
		read(x[i]);
	f[1] = k + 1;
	int l = 1, r = k + 1;
	for (int i = 2; i <= n; i++)
	{
		while (r < n && x[i] - x[l] > x[r + 1] - x[i])
		{
			l++;
			r++;
		}
		if (x[r] - x[i] > x[i] - x[l])
			f[i] = r;
		else
			f[i] = l;
	}
	for (int i = 1; i <= n; i++)
		ans[i] = i;
	do
	{
		if (m & 1)
			for (int i = 1; i <= n; i++)
				ans[i] = f[ans[i]];
		for (int i = 1; i <= n; i++)
			g[i] = f[f[i]];
		copy(g + 1, g + n + 1, f + 1);
	} while (m /= 2);
	p = buf;
	pend = buf + SZ;
	for (int i = 1; i <= n; i++)
		writeln(ans[i]);
	writechar('\n');
	flush();
	return 0;
}