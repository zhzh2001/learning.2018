#include <fstream>
#include <random>
#include <algorithm>
using namespace std;
ifstream fin("jump.in");
ofstream fout("jump.ans");
const int N = 300005;
int nxt[N];
long long x[N];
pair<long long, int> t[N];
int main()
{
	int n, k, m;
	fin >> n >> k >> m;
	for (int i = 1; i <= n; i++)
		fin >> x[i];
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= n; j++)
			t[j] = make_pair(abs(x[i] - x[j]), j);
		nth_element(t + 1, t + k + 1, t + n + 1);
		nxt[i] = t[k + 1].second;
		if (t[k].first == t[k + 1].first)
			nxt[i] = t[k].second;
	}
	for (int i = 1; i <= n; i++)
	{
		int now = i;
		for (int j = 1; j <= m; j++)
			now = nxt[now];
		fout << now << ' ';
	}
	fout << endl;
	return 0;
}