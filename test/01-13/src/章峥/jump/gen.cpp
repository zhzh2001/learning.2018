#include <fstream>
#include <random>
#include <ctime>
#include <algorithm>
using namespace std;
ofstream fout("jump.in");
const int n = 1e4, k = 2333;
const long long m = 1e4;
long long a[n + 5];
int main()
{
	minstd_rand gen(time(NULL));
	fout << n << ' ' << k << ' ' << m << endl;
	for (int i = 1; i <= n; i++)
	{
		uniform_int_distribution<long long> d(-1e18, 1e18);
		a[i] = d(gen);
	}
	sort(a + 1, a + n + 1);
	for (int i = 1; i <= n; i++)
		fout << a[i] << ' ';
	fout << endl;
	return 0;
}