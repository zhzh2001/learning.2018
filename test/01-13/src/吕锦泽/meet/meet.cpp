#include<cstdio>
#include<algorithm>
#define ll long long
#define N 100005
#define inf (1ll<<50)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,ans=inf,l[N],r[N],u[N],d[N];
struct data{ ll x,y,id; }a[N];
bool cmp1(data x,data y) { return x.x<y.x; }
bool cmp2(data x,data y) { return x.y<y.y; }
int main(){
	freopen("meet.in","r",stdin); freopen("meet.out","w",stdout);
	n=read(); rep(i,1,n) { ll x=read(),y=read(); a[i]=(data){x+y,x-y,i}; }
	sort(a+1,a+1+n,cmp1);
	rep(i,1,n) l[a[i].id]=l[a[i-1].id]+(i-1)*(a[i].x-a[i-1].x);
	per(i,n,1) r[a[i].id]=r[a[i+1].id]+(n-i)*(a[i+1].x-a[i].x);
	sort(a+1,a+1+n,cmp2);
	rep(i,1,n) u[a[i].id]=u[a[i-1].id]+(i-1)*(a[i].y-a[i-1].y);
	per(i,n,1) d[a[i].id]=d[a[i+1].id]+(n-i)*(a[i+1].y-a[i].y);
	rep(i,1,n) ans=min(ans,l[i]+r[i]+u[i]+d[i]);
	printf("%lld",ans/2);
}
