#include<cstdio>
#define ll long long
#define N 1000005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll T,a,b,fac[N];
ll qpow(ll x,ll y) { ll num=1; for (;y;x=x*x%mod,y>>=1) if (y&1) num=num*x%mod; return num%mod; }
ll C(ll i,ll j) { return fac[i]*qpow(fac[j],mod-2)%mod*qpow(fac[i-j],mod-2)%mod; }
int main(){
	freopen("wei.in","r",stdin); freopen("wei.out","w",stdout);
	T=read(); fac[0]=1; rep(i,1,1000000) fac[i]=fac[i-1]*i%mod;
	while (T--) { a=read(),b=read(); printf("%lld\n",qpow(2,a-b)*C(a,b)%mod); }
}
