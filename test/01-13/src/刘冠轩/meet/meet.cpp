#include <cstdio>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstring>
#define LL long long
using namespace std;
int n,x[200000],y[200000],xx[200000],yy[200000];
int main(){
  freopen("meet.in","r",stdin);
  freopen("meet.out","w",stdout);
  scanf("%d",&n);
  for (int i=1;i<=n;i++)
    scanf("%d%d",&x[i],&y[i]);
  if (n<=3000){
    LL ans=0x7f7f7f7f7f7f7f,sum;
    for (int i=1;i<=n;i++){
      sum=0;
      for (int j=1;j<=n;j++)
      sum+=max(abs(x[i]-x[j]),abs(y[i]-y[j]));
      if (sum<ans) ans=sum;
    }
    cout<<ans;
  }else{
    sort(x+1,x+n+1);
    sort(y+1,y+n+1);
    int ans=0;
    for (int i=1;i<=n;i++)
      ans+=max(abs(x[n/2]-x[i]),abs(y[n/2]-y[i]));
    cout<<ans;
  }
  return 0;
}
