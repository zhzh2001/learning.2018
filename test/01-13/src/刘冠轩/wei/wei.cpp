#include <cstdio>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstring>
using namespace std;
long long f[5200][5200],e[10000];
int main(){
  freopen("wei.in","r",stdin);
  freopen("wei.out","w",stdout);
  f[0][0]=1;
  for (int i=1;i<=5150;i++){
    f[i][0]=1;
    for (int j=1;j<=i;j++)
      f[i][j]=(f[i-1][j-1]+f[i-1][j])%1000000007;
  }
  e[0]=1;
  for (int i=1;i<=6000;i++) e[i]=e[i-1]*2%1000000007;
  int t;cin>>t;
  while (t>0){
    int a,b;
    cin>>a>>b;
    cout<<f[a][b]*e[a-b]%1000000007<<endl;
    t--;
  }
}
