// by ��

#include <bits/stdc++.h>

#define For(i,N) for(int i=1;i<=N;++i)
#define cp const Vec&

using namespace std;

typedef double db;

int N,M;

struct Vec{
    int x,y;
    void IN(){scanf("%d%d",&x,&y);}
    Vec operator - (cp A){return {x-A.x,y-A.y};}
    db operator * (cp A){return x*A.x+y*A.y;}
    db operator ^ (cp A){return x*A.y-y*A.x;}
    db Len(){return sqrt(x*x+y*y);}
}P,Q,A[1005],B[1005];

inline void ck(Vec A,Vec b,Vec c,Vec Q){
    b=b-A-Q;c=c-A-Q;
    db r1=c.Len(),r2=b.Len(),ds=(b-c).Len();
    if(!ds)return;if(r1>r2)swap(r1,r2);
    if(c*(c-b)>=0 && b*(b-c)>=0)r1=fabs((b^c)/(b-c).Len());
    if(r1<=Q.Len() && r2>=Q.Len())puts("YES"),exit(0);
}

int main(){
	freopen("cc.in","r",stdin);
	freopen("cc.out","w",stdout);
    P.IN(); scanf("%d",&N); For(i,N)A[i].IN();
    Q.IN(); scanf("%d",&M); For(i,M)B[i].IN();
    For(i,N)For(j,M){
        ck(A[i]-P,B[j]-P,B[j%M+1]-P,Q-P);
        ck(B[j]-Q,A[i]-Q,A[i%N+1]-Q,P-Q);
    }
    puts("NO");
}
