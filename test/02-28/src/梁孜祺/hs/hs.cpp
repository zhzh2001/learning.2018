#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 100005
#define mod 998244353
using namespace std;
inline int Pow(int x,int y){
	int sum=1;
	while(y){
		if(y&1) sum=1ll*sum*x%mod;
		y>>=1;
		if(y) x=1ll*x*x%mod;
	}
	return sum;
}
int x[N],y[N],Q,n;
inline void s1(){
	while(Q--){
		int opt;scanf("%d",&opt);
		if(opt==1){
			int d,X,Y;scanf("%d%d%d",&d,&X,&Y);
			x[d]=X;y[d]=Y;
		}else{
			int X,Y;scanf("%d%d",&X,&Y);
			printf("%d\n",1ll*x[1]*Pow(y[1],mod-2)%mod);
		}
	}
}
int main(){
	freopen("hs.in","r",stdin);
	freopen("hs.out","w",stdout);
	scanf("%d%d",&n,&Q);
	For(i,1,n) scanf("%d%d",&x[i],&y[i]);
	if(n==1) s1();
	return 0;
}
