#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 105
#define pi 3.1415926535897932384626433832795
#define mod 998244353
using namespace std;
inline ll sqr(int x){return 1ll*x*x;}
int px,py,qx,qy,n,m,x[N],y[N],X[N],Y[N];
inline void solve(){
	
	exit(0);
}
int main(){
	scanf("%d%d%d",&px,&py,&n);
	For(i,1,n) scanf("%d%d",&x[i],&y[i]);
	scanf("%d%d%d",&qx,&qy,&m);
	For(i,1,m) scanf("%d%d",&X[i],&Y[i]);
	if(n==4&&m==4) solve();
	ll mx1=0,mx2=0;
	For(i,1,n) mx1=max(mx1,sqr(x[i]-px)+sqr(y[i]-py));
	For(i,1,m) mx2=max(mx2,sqr(X[i]-qx)+sqr(Y[i]-qy));
	if(mx1+mx2<sqr(px-qx)+sqr(py-qy)) return puts("NO")&0;
	else puts("YES");
	solve();
	return 0;
}
