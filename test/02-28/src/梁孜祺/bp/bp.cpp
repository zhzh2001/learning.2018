#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 5005
using namespace std;
int f[N][N],m,n,mod,ans,c[N][N],Pow[N];
inline void init(){
	c[0][0]=1;
	For(i,1,5000){
		c[i][0]=1;
		For(j,1,i) c[i][j]=(c[i-1][j-1]+c[i-1][j])%mod;
	}
	Pow[0]=1;For(i,1,N-5) Pow[i]=Pow[i-1]*2%mod;
}
int main(){
	freopen("bp.in","r",stdin);
	freopen("bp.out","w",stdout);
	scanf("%d%d%d",&m,&n,&mod);
	f[0][0]=1;init();
	For(i,1,m)
		For(j,1,n)
			For(k,0,j-1) f[i][j]=(f[i][j]+1ll*f[i-1][k]*c[n-k][j-k]%mod*Pow[k]%mod)%mod;
	int ans=0;
	For(i,0,n) ans=(ans+f[m][i])%mod;
	printf("%d\n",ans);
	return 0;
}
