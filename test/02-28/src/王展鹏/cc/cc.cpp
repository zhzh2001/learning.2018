#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
const int N=105;
const double oo=1e100,eps=1e-10,pi=2*asin(1);
struct point{
	double x,y;
	point(double _x=0,double _y=0):x(_x),y(_y){}
	point operator +(const point b){return point(x+b.x,y+b.y);}
	point operator -(const point b){return point(x-b.x,y-b.y);}
	point operator *(double d){return point(x*d,y*d);}
	friend point operator *(double d,const point x){return point(x.x*d,x.y*d);}
	point operator /(double d){return point(x/d,y/d);}
	friend point operator /(double d,const point x){return point(x.x/d,x.y/d);}
	point operator -(){return point(-x,-y);}
	bool operator ==(const point b){return fabs(x-b.x)<eps&&fabs(y-b.y)<eps;}
	bool operator !=(const point b){return !(*this==b);}
}x,y,a[N],b[N];
struct line{
	point x,y;
	line(){}
	line(point _x,point _y):x(_x),y(_y){}
};
inline point rotate(point x,double angle){  
	return point(x.x*cos(angle)-x.y*sin(angle),x.y*cos(angle)+x.x*sin(angle));
}
inline int on_segment(point p1,line l){
	if (min(l.x.x,l.y.x)-eps<=p1.x&&max(l.x.x,l.y.x)+eps>=p1.x&&
	min(l.x.y,l.y.y)-eps<=p1.y&&max(l.x.y,l.y.y)+eps>=p1.y)return 1;
	else return 0;
}
double mx1,mx2;
inline double dist(point a,point b){
	return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}
int main(){
	freopen("cc.in","r",stdin); freopen("cc.out","w",stdout);
	int u=read(),v=read(); x=(point){u,v};
	int n=read();
	for(int i=1;i<=n;i++){
		u=read(); v=read();
		a[i]=(point){u,v};
		mx1=max(mx1,dist(x,a[i]));
	}
	u=read(); v=read(); y=(point){u,v};
	n=read();
	for(int i=1;i<=n;i++){
		u=read(); v=read();
		b[i]=(point){u,v};
		mx2=max(mx2,dist(y,b[i]));
	}
	if(dist(x,y)<=mx1+mx2)puts("YES"); else puts("NO");
	return 0;
}


