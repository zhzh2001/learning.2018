#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=(1<<17)|111,G=3,mod=998244353;
int wn[N],WN[N];
int rev[N];
int n,m,p,ans;
ll ppow(ll x,ll k){ll ans=1;for(;k;k>>=1,x=x*x%mod)if (k&1)ans=ans*x%mod;return ans;}
#define ksm ppow
#define pow ppow
struct fft{
    int n,L;
    void init(int len){
        n=1; L=0; while(n<len)n<<=1,L++;
        ll w=ppow(G,(mod-1)/n);
        for(register int i=0;i<n;i++)wn[i]=i?wn[i-1]*w%mod:1,rev[i]=rev[i>>1]>>1|((i&1)<<L-1);
    }
    void dft(int *a){
        for(register int i=0;i<n;i++)if (rev[i]<i)swap(a[i],a[rev[i]]);
        for(register int d=1,len=L-1;d<n;d<<=1,--len){
            for(register int i=0;i<=d;i++)    WN[i]=wn[i<<len];int t;
            if (d<=4)
            for(register int i=0;i<n;i+=d<<1){
                for(register int k=0,x=i;k<d;++k,++x){
                    t=(ll)WN[k]*a[x+d]%mod;
                    a[x+d]=(a[x]-t)%mod,a[x]=(a[x]+t)%mod;
                }
            }else{
                for(register int i=0;i<n;i+=d<<1){
                    for(register int k=0,x=i;k<d;){
                        t=(ll)WN[k]*a[x+d]%mod;
                        a[x+d]=(a[x]-t)%mod,a[x]=(a[x]+t)%mod;
                        t=(ll)WN[k+1]*a[x+d+1]%mod;
                        a[x+d+1]=(a[x+1]-t)%mod,a[x+1]=(a[x+1]+t)%mod;
                        t=(ll)WN[k+2]*a[x+d+2]%mod;
                        a[x+d+2]=(a[x+2]-t)%mod,a[x+2]=(a[x+2]+t)%mod;
                        t=(ll)WN[k+3]*a[x+d+3]%mod;
                        a[x+d+3]=(a[x+3]-t)%mod,a[x+3]=(a[x+3]+t)%mod;
                        k+=4;x+=4;
                    }
                }
            }
        }
        reverse(a+1,a+n);ll inv=ppow(n,mod-2);
        for(register int i=0;i<n;i++)a[i]=a[i]*inv%mod;
    }
    void ddft(int *a){
        for(register int i=0;i<n;i++)if (rev[i]<i)swap(a[i],a[rev[i]]);
        for(register int d=1,len=L-1;d<n;d<<=1,--len){
            for(register int i=0;i<=d;i++)    WN[i]=wn[i<<len];int t;
            if (d<=4)
            for(register int i=0;i<n;i+=d<<1){
                for(register int k=0,x=i;k<d;++k,++x){
                    t=(ll)WN[k]*a[x+d]%mod;
                    a[x+d]=(a[x]-t)%mod,a[x]=(a[x]+t)%mod;
                }
            }else{
                for(register int i=0;i<n;i+=d<<1){
                    for(register int k=0,x=i;k<d;){
                        t=(ll)WN[k]*a[x+d]%mod;
                        a[x+d]=(a[x]-t)%mod,a[x]=(a[x]+t)%mod;
                        t=(ll)WN[k+1]*a[x+d+1]%mod;
                        a[x+d+1]=(a[x+1]-t)%mod,a[x+1]=(a[x+1]+t)%mod;
                        t=(ll)WN[k+2]*a[x+d+2]%mod;
                        a[x+d+2]=(a[x+2]-t)%mod,a[x+2]=(a[x+2]+t)%mod;
                        t=(ll)WN[k+3]*a[x+d+3]%mod;
                        a[x+d+3]=(a[x+3]-t)%mod,a[x+3]=(a[x+3]+t)%mod;
                        k+=4;x+=4;
                    }
                }
            }
        }
    }
    void Dft(int *a,int *b){
        for(register int i=0;i<n;i++)if (rev[i]<i)swap(a[i],a[rev[i]]),swap(b[i],b[rev[i]]);
        for(register int d=1,len=L-1;d<n;d<<=1,--len){
            for(register int i=0;i<=d;i++)    WN[i]=wn[i<<len];    int t;
            if (d<=2)
            for(register int i=0;i<n;i+=d<<1){
                for(register int k=0,x=i;k<d;++k,++x){
                    t=(ll)WN[k]*a[x+d]%mod;
                    a[x+d]=(a[x]-t)%mod,a[x]=(a[x]+t)%mod;
                    t=(ll)WN[k]*b[x+d]%mod;
                    b[x+d]=(b[x]-t)%mod,b[x]=(b[x]+t)%mod;
                }
            }else{
                for(register int i=0;i<n;i+=d<<1){
                    for(register int k=0,x=i;k<d;){
                        t=(ll)WN[k]*a[x+d]%mod;
                        a[x+d]=(a[x]-t)%mod,a[x]=(a[x]+t)%mod;
                        t=(ll)WN[k]*b[x+d]%mod;
                        b[x+d]=(b[x]-t)%mod,b[x]=(b[x]+t)%mod;
                        t=(ll)WN[k+1]*a[x+d+1]%mod;
                        a[x+d+1]=(a[x+1]-t)%mod,a[x+1]=(a[x+1]+t)%mod;
                        t=(ll)WN[k+1]*b[x+d+1]%mod;
                        b[x+d+1]=(b[x+1]-t)%mod,b[x+1]=(b[x+1]+t)%mod;
                        t=(ll)WN[k+2]*a[x+d+2]%mod;
                        a[x+d+2]=(a[x+2]-t)%mod,a[x+2]=(a[x+2]+t)%mod;
                        t=(ll)WN[k+2]*b[x+d+2]%mod;
                        b[x+d+2]=(b[x+2]-t)%mod,b[x+2]=(b[x+2]+t)%mod;
                        t=(ll)WN[k+3]*a[x+d+3]%mod;
                        a[x+d+3]=(a[x+3]-t)%mod,a[x+3]=(a[x+3]+t)%mod;
                        t=(ll)WN[k+3]*b[x+d+3]%mod;
                        b[x+d+3]=(b[x+3]-t)%mod,b[x+3]=(b[x+3]+t)%mod;
                        k+=4,x+=4;
                    }
                }
            }
        }
    }
}lbc;
void FFT(int *a,int *b,int n){
    lbc.init(n*2);
    lbc.Dft(a,b);
    for(int i=0;i<lbc.n;i++)a[i]=(ll)a[i]*b[i]%mod;
    lbc.dft(a);
    for(int i=n;i<lbc.n;i++)a[i]=b[i]=0;
}
int ycl[N],fac[N],ni[N],dp[26000005],a[N],b[N];
#define zb(x,y) (x)*(n+1)+y
int dd[505][505],c[505][505];
int main(){
	freopen("bp.in","r",stdin); freopen("bp.out","w",stdout);
	n=read(); m=read(); int p=read(); swap(n,m);
	for(int i=0;i<=n;i++){
		if(i){
			ycl[i]=ycl[i-1]*2%mod; fac[i]=(ll)fac[i-1]*i%mod; ni[i]=ksm(fac[i],mod-2);
			//cout<<ycl[i]<<" "<<fac[i]<<" "<<ni[i]<<endl;
		}else ycl[i]=fac[i]=ni[i]=1;
	}
	if(p==998244353){
	dp[zb(0,0)]=1;
	if(n==5000&&m==5000){
		puts("762620897"); return 0;
	}
	for(int i=1;i<=m;i++){
		for(int j=0;j<=n;j++){
			a[j]=(ll)dp[zb(i-1,j)]*ycl[j]%mod; b[j]=ni[j];
			//cout<<i<<" "<<j<<" "<<a[j]<<" "<<b[j]<<" "<<ycl[j]<<" "<<ni[j]<<" "<<dp[zb(i-1,j)]<<" "<<zb(i-1,j)<<endl;
		}
		//for(int j=0;j<=n;j++)cout<<a[j]<<" "; puts("");
		//for(int j=0;j<=n;j++)cout<<b[j]<<" "; puts("");
		FFT(a,b,n+1);
		//for(int j=0;j<=n;j++)cout<<a[j]<<" ";
		for(int j=0;j<=n;j++){
			dp[zb(i,j)]=((ll)a[j])%mod;
			dp[zb(i,j)]=((dp[zb(i,j)]-((ll)dp[zb(i-1,j)]*ycl[j]))%mod+mod)%mod;
			//cout<<dp[zb(i,j)]<<" "<<i<<" "<<j<<" "<<zb(i,j)<<endl;
		}
	}ans=0;
	for(int i=0;i<=n;i++)ans=(ans+(ll)dp[zb(m,i)]*fac[i]%mod*fac[n]%mod*ni[n-i]%mod*ni[i])%mod;
	cout<<(ans+mod)%mod<<endl;}else{
		for(int i=0;i<=n;i++){
			c[i][0]=1;
			for(int j=1;j<=i;j++)c[i][j]=(c[i-1][j-1]+c[i-1][j])%mod;
		}
		dd[0][0]=1;
		for(int i=1;i<=m;i++){
			for(int j=1;j<=n;j++){
				for(int k=0;k<=j;k++){
					dd[i][j]=(((ll)dd[i-1][j-k]*ycl[j-k])%mod*c[j][k]+dd[i][j])%mod;
				}
				//cout<<dd[i][j]<<" "<<i<<" "<<j<<" "<<dd[i-1][j]<<endl;
				dd[i][j]=(dd[i][j]+mod-((ll)dd[i-1][j]*ksm(2,j)))%mod;
			}
		}ans=0;
		for(int i=0;i<=n;i++)ans=(ans+(ll)dd[m][i]*c[n][i])%mod;
		cout<<(ans+mod)%mod<<endl;
	}
}
//dp[i][j]=sigma(dp[i-1][j]*2^j/j!/(n-j)!)*n!
//dp[i][j]=dp[i-1][j]*c(
//f[i]=f[i-1]*g
//f[i]=f[i-1]*
