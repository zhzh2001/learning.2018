#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int mod=998244353,N=100005,size=2;
int n,m,x[N],y[N],xx[N],yy[N];
inline int ksm(int a,int b){
	int t=1,y=a;
	while (b){
		if (b&1) t=(long long)t*y%mod;
		y=(long long)y*y%mod;
		b>>=1;
	}
	return t;
}
struct matrix{
    int a[2][2];
}ycl,tree[N<<2];
matrix cheng(matrix x,matrix y){
    matrix tmp;
    for(int i=0;i<size;i++)
     for(int j=0;j<size;j++){tmp.a[i][j]=0;
      for(int k=0;k<size;k++){
          tmp.a[i][j]=(tmp.a[i][j]+(long long)x.a[i][k]*y.a[k][j])%mod;
      }}
    return tmp;
}
matrix ask(int l,int r,int i,int j,int nod){
	if(l==i&&r==j)return tree[nod];
	int mid=(l+r)>>1;
	if(j<=mid)return ask(l,mid,i,j,nod<<1); else if(i>mid)return ask(mid+1,r,i,j,nod<<1|1);
	else return cheng(ask(l,mid,i,mid,nod<<1),ask(mid+1,r,mid+1,j,nod<<1|1));
}
void insert(int l,int r,int pos,int nod){
	if(l==r){
		tree[nod].a[0][0]=0; tree[nod].a[0][1]=(1-(ll)y[pos]*xx[pos])%mod;
		tree[nod].a[1][0]=1; tree[nod].a[1][1]=(ll)y[pos]*xx[pos]%mod; return;
	}
	int mid=(l+r)>>1;
	if(pos<=mid)insert(l,mid,pos,nod<<1); else insert(mid+1,r,pos,nod<<1|1);
	tree[nod]=cheng(tree[nod<<1],tree[nod<<1|1]);
}
void build(int l,int r,int nod){
	if(l==r){
		tree[nod].a[0][0]=0; tree[nod].a[0][1]=(1-(ll)y[l]*xx[l])%mod;
		tree[nod].a[1][0]=1; tree[nod].a[1][1]=(ll)y[l]*xx[l]%mod; return;
	}
	int mid=(l+r)>>1;
	build(l,mid,nod<<1); build(mid+1,r,nod<<1|1);
	tree[nod]=cheng(tree[nod<<1],tree[nod<<1|1]);
}
int main(){
	freopen("hs.in","r",stdin); freopen("hs.out","w",stdout);
	n=read(); m=read(); ycl.a[0][1]=1;
	for(int i=1;i<=n;i++){
		x[i]=read(); y[i]=read(); xx[i]=ksm(x[i],mod-2); yy[i]=ksm(y[i],mod-2);
	}
	build(1,n,1);
	for(int i=1;i<=m;i++){
		int op=read();
		if(op==1){
			int d=read(),a=read(),b=read();
			x[d]=a; y[d]=b;
			xx[d]=ksm(x[d],mod-2); yy[d]=ksm(y[d],mod-2);
			insert(1,n,d,1);
		}else{
			int l=read(),r=read();
			writeln(ksm((cheng(ycl,ask(1,n,l,r,1)).a[0][1]+mod)%mod,mod-2));
		}
	}
}
//dp[r+1]=1 dp[i]=dp[i+1]*x+dp[i-1]*(1-x)
//dp[l]=dp[l+1]*x dp[l+1]=dp[l]/x
//dp[l+1]=dp[l]*(1-x)+dp[l+2]*x
//dp[l]*x=dp[l]*(1-y)+dp[l+2]*y
//dp[l]*x=
//f[i-1] f[i]
//0 1-y[i]*xx[i]
//1 y[i]*xx[i]
//f[i+1]=((ll)(f[i]-f[i-1])*y[i]%mod*xx[i]+f[i-1])%mod;
