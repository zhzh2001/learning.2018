#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int mod=998244353,N=100005;
int n,m,x[N],y[N],f[N],xx[N],yy[N];
inline int ksm(int a,int b){
	int t=1,y=a;
	while (b){
		if (b&1) t=(long long)t*y%mod;
		y=(long long)y*y%mod;
		b>>=1;
	}
	return t;
}
int main(){
	n=read(); m=read();
	for(int i=1;i<=n;i++){
		x[i]=read(); y[i]=read(); xx[i]=ksm(x[i],mod-2); yy[i]=ksm(y[i],mod-2);
	}
	for(int i=1;i<=m;i++){
		int op=read();
		if(op==1){
			int d=read(),a=read(),b=read();
			x[d]=a; y[d]=b;
			xx[d]=ksm(x[d],mod-2); yy[d]=ksm(y[d],mod-2);
		}else{
			int l=read(),r=read();
			f[l]=1;
			for(int i=l;i<=r;i++){
				//g[i+1]=((ll)(g[i]-g[i-1])*y[i]%mod*xx[i]+g[i-1])%mod;
				f[i+1]=((ll)(f[i]-f[i-1])*y[i]%mod*xx[i]+f[i-1])%mod;
				//f[i+1]=(f[i]-(ll)f[i-1]*(y[i]-x[i])%mod*yy[i])%mod*y[i]%mod*xx[i]%mod;
				//f[i]=f[i+1]*(x[d]/y[d])+f[i-1]*(y[d]-x[d])/y[d];
			}
			//cout<<f[r+1]<<endl;
			writeln(ksm((f[r+1]+mod)%mod,mod-2));
		}
	}
}
//dp[r+1]=1 dp[i]=dp[i+1]*x+dp[i-1]*(1-x)
//dp[l]=dp[l+1]*x dp[l+1]=dp[l]/x
//dp[l+1]=dp[l]*(1-x)+dp[l+2]*x
//dp[l]*x=dp[l]*(1-y)+dp[l+2]*y
//dp[l]*x=
