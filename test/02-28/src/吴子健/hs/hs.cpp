#include <bits/stdc++.h>
#define LL long long
using namespace std;
inline int rd()
{
	int x = 0, vis = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') vis = -1;
		ch = getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		x = x*10+ch-48;
		ch = getchar();
	}
	return x * vis;
}
const LL mod=998244353;
const int MAXN=1e5+10;
// data
int N,Q;
struct fenshu
{
	LL fenzi,fenmu;
} f[MAXN],g[MAXN],win[MAXN];
fenshu operator *(fenshu x,fenshu y)
{
	x.fenzi=x.fenzi*y.fenzi%mod;
	x.fenmu=x.fenmu*y.fenmu%mod;
	return x;
}
LL tomod(LL x)
{
	return (x%mod+mod)%mod;
}
fenshu tolimit(fenshu x)
{
	LL a=x.fenzi,b=x.fenmu;
	x.fenzi=a,x.fenmu=tomod(b-a);
	return x;
}
fenshu add_one(fenshu x)
{
	x.fenzi=tomod(x.fenzi+x.fenmu);
	return x;
}
fenshu reverse_one(fenshu x)//将x变为1-x
{
	x.fenzi=tomod(x.fenmu-x.fenzi);
	return x;
}
void set_one(fenshu& x)
{
	x.fenzi=x.fenmu=1;
}
LL qpow(LL base,LL index)
{
	if(index==0)	return 1;
	LL ans=1;
	if(index&1)	ans=base;
	return ans*qpow(base*base%mod,index>>1)%mod;
}
void print_mod(fenshu x)
{
	printf("%lld\n",qpow(x.fenzi,mod)%mod*qpow(x.fenmu,mod-2)%mod);
}
// code
void query(int l,int r)
{
	set_one(f[l]),set_one(g[l]);
	for(int i=l+1; i<=r; ++i)
	{
		f[i]=f[i-1]*g[i-1]*win[i-1];
		g[i]=tolimit(reverse_one(win[i])*g[i-1]*win[i-1]);
		g[i]=add_one(g[i]);
		//printf("f[%d]=%d/%d g[%d]=%d/%d\n",i,f[i].fenzi,f[i].fenmu,i,g[i].fenzi,g[i].fenmu);
	}
	f[r+1]=f[r]*g[r]*win[r];
	print_mod(f[r+1]);
}
void work1()
{
	for(int i=1; i<=Q; ++i)
	{
		int flag=rd();
		if(flag==1)
		{
			int d=rd();
			win[d].fenzi=rd();
			win[d].fenmu=rd();
		}
		else
		{
			int l=rd(),r=rd();
			query(l,r);
		}
	}
}

//main
int main()
{
	freopen("hs.in","r",stdin);
	freopen("hs.out","w+",stdout);
	N=rd(),Q=rd();
	cout<<N<<Q<<endl;
	for(int i=1; i<=N; ++i)
	{
		win[i].fenzi=rd();
		win[i].fenmu=rd();
	}
	work1();
	//work2();//看看操作有没有1
	return 0;
}
