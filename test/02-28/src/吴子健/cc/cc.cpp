#include <bits/stdc++.h>
#include <cmath>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;
inline int rd()
{
	int x = 0, vis = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') vis = -1;
		ch = getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		x = x*10+ch-48;
		ch = getchar();
	}
	return x * vis;
}
const LL mod=998244353;
LL qpow(LL base,LL index)
{
	if(index==0)	return 1;
	LL ans=1;
	if(index&1)	ans=base;
	return ans*qpow(base*base%mod,index>>1)%mod;
}
struct pnt
{
	double x,y;
};
const double eps=1e-9;
const int INVALID=0x3f3f3f3f;
void solve_equation(double a,double b,double c,double&x1,double &x2)//ax^2+bx+c=0
{
	if(b*b+eps>4*a*c)
	{
		x1=(-b+sqrt(b*b-4*a*c))/a/2;
		x2=(-b-sqrt(b*b-4*a*c))/a/2;
	}
}
int solve_circle(double a,double b,double r1,double c,double d,double r2,pnt&x1,pnt&x2)//(a,b) r1  (c,d) r2
{
	double dis=sqrt((a-c)*(a-c)+(b-d)*(b-d));
	if(r1+r2<dis||abs(r1-r2)>dis)	return 0;
	/*
	r1^2=x^2-2ax+a^2+y^2-2by+b^2
	r2^2=x^2-2cx+c^2+y^2-2dy+d^2
	r2^2-r1^2=2(a-c)x+c^2-a^2+2(b-d)+(d^2-b^2)
	r2^2-r1^2+aa-cc+bb-dd=2(a-c)x+2(b-d)y

	m=(r2r2-r1r1+aa-cc+bb-dd)/2/(b-d)
	s=(a-c)/(b-d)
	-> y=m-sx

	r1r1=xx-2ax+aa+(m-sx-b)^2
	0	=xx-2ax+aa+(m-b)^2+ssxx-2(m-b)sx-r1r1
	0	=(1+ss)xx-2ax-2msx+2bsx+(m-b)^2-r1r1
	*/
	if(a==c&&b==d)
	{
		return 0;
	}
	if(b!=d)
	{
		double m=(r2*r2-r1*r1+a*a-c*c+b*b-d*d)/2/(b-d),
		       s=(a-c)/(b-d);
		//printf("m=%lf s=%lf\n",m,s);
		solve_equation(1+s*s,-2*a-2*m*s+2*b*s,(m-b)*(m-b)-r1*r1,x1.x,x2.x);
		x1.y=m-s*x1.x;
		x2.y=m-s*x2.x;
		//printf("(%lf,%lf)",x1.x,x1.y);
	}
	else
	{
		double m=(r2*r2-r1*r1+a*a-c*c+b*b-d*d)/2/(a-c);//此时m的值就是x的值
		x1.x=x2.x=m;
		solve_equation(1,-2*b,b*b+a*a+m*m-2*a*m-r1*r2,x1.y,x2.y);
		//printf("(%lf,%lf)\n",x1.x,x1.y);
	}
	return 1;
}
int N,M;
pnt midN,midM;
pnt a[1000],b[1000];
double dist(pnt x,pnt y)
{
	return sqrt((x.x-y.x)*(x.x-y.x)+(x.y-y.y)*(x.y-y.y));
}
//code

int main()
{
	freopen("cc.in","r+",stdin);
	freopen("cc.out","w+",stdout);
	//printf("%lf\n",atan2(1.0,0.0));
	midN.x=rd(),midN.y=rd();
	N=rd();
	for(int i=1; i<=N; ++i)
		a[i].x=rd(),a[i].y=rd();
	midM.x=rd(),midM.y=rd();
	M=rd();
	for(int i=1; i<=M; ++i)
		b[i].x=rd(),b[i].y=rd();
	bool flag=false;
	for(int i=1; i<=N; ++i)
		for(int j=1; j<=M; ++j)
		{
			pnt tp1,tp2;
			if(solve_circle(midN.x,midN.y,dist(a[i],midN),midM.x,midM.y,dist(b[j],midM),tp1,tp2))
			{
				double angle1=atan2(tp1.y,tp1.x)-atan2(a[i].y-midN.y,a[i].x-midN.x);
				double angle2=atan2(tp2.y,tp2.x)-atan2(a[i].y-midN.y,a[i].x-midN.x);
				double angle3=atan2(tp1.y,tp1.x)-atan2(b[j].y-midM.y,b[j].x-midM.x);
				double angle4=atan2(tp2.y,tp2.x)-atan2(b[j].y-midM.y,b[j].x-midM.x);
				//printf("%lf to %lf   %lf to %lf\n",angle1,angle2,angle3,angle4);
				if((abs(angle1)-abs(angle3))*(abs(angle2)-abs(angle4))<=eps)
				{
					printf("YES\n");
					exit(0);
				}
			}
		}
	puts("NO");
	return 0;
}
