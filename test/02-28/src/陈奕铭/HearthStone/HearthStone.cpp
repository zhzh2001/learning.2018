#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
#define ll long long
const int N = 100005,p = 998244353;
int x[N],y[N];
int xx[N],xy[N];
int n,q;

inline int qpow(int x,int n){
	int ans = 1;
	while(n){
		if(n&1) ans = 1LL*ans*x%p;
		x = 1LL*x*x%p;
		n >>= 1;
	}
	return ans;
}

int main(){
	freopen("HearthStone.in","r",stdin);
	freopen("HearthStone.out","w",stdout);
	n = read(); q = read();
	for(int i = 1;i <= n;++i){
		x[i] = read(); y[i] = read();
		int tmp = qpow(y[i],p-2);
		xx[i] = 1LL*x[i]*tmp%p;
		xy[i] = 1LL*(y[i]-x[i])*tmp%p;
	}
	if(n == 1){
		while(q--){
			int opt = read();
			if(opt == 1){
				int d = read(),a = read(),b = read();
				x[d] = a; y[d] = b;
				int tmp = qpow(y[d],p-2);
				xx[d] = 1LL*x[d]*tmp%p;
				xy[d] = 1LL*(y[d]-x[d])*tmp%p;
			}
			else{
				int l = read(); read();
				printf("%lld\n",1LL*x[l]*qpow(y[l],p-2)%p);
			}
		}
		return 0;
	}
	while(q--){
		int opt = read();
		if(opt == 1){
			int d = read(),a = read(),b = read();
			x[d] = a; y[d] = b;
			int tmp = qpow(y[d],p-2);
			xx[d] = 1LL*x[d]*tmp%p;
			xy[d] = 1LL*(y[d]-x[d])*tmp%p;
		}
		else{
			int l = read(),r = read();
			ll ans = xx[l];
			ll pos = 1;
			for(int i = l+1;i <= r;++i){
				ans = ans*xx[i]%p;
				for(int j = i;j >= l+1;--j){
					pos = 1;
					for(int k = i;k >= j;--k)
						pos = pos*xy[k]%p*xx[k-1]%p;
					pos = ((1-pos)%p+p)%p;
					ans = ans*qpow(pos,p-2)%p;
				}
			}
			printf("%lld\n", ans);
		}
	}
	return 0;
}