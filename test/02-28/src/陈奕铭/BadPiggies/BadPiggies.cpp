#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 5005;
int dp[N][N];
int f[N][N];
int n,m,p;
int bin[N];
int ans;

int main(){
	freopen("BadPiggies.in","r",stdin);
	freopen("BadPiggies.out","w",stdout);
	m = read(); n = read(); p = read();
	f[0][0] = 1;
	for(int i = 1;i <= n;++i){
		f[i][0] = 1;
		for(int j = 1;j <= i;++j)
			f[i][j] = (f[i-1][j]+f[i-1][j-1])%p;
	}
	bin[0] = 1;
	for(int i = 1;i <= n;++i) bin[i] = (bin[i-1]*2)%p;
	for(int j = 1;j <= n-m+1;++j) dp[1][j] = f[n][j];
	for(int i = 1;i < m;++i)
		for(int j = i;j <= n-m+i;++j)
			if(dp[i][j])
				for(int k = j+1;k <= n-m+i+1;++k)
					dp[i+1][k] = (dp[i+1][k]+1LL*dp[i][j]*bin[j]%p*f[n-j][k-j]%p)%p;
	for(int i = m;i <= n;++i)
		ans = (ans+dp[m][i])%p;
	printf("%d\n",ans);
	return 0;
}