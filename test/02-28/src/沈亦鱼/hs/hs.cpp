#include<cstdio>
int mo=998244353,n,m,ch,z,p,q;
long long a[110000],b[110000],x[360000],y[360000],sx[360000],sy[360000];
long long gcd(long long x,long long y){
	if(y==0)return x;
	else return gcd(y,x%y);
}
void pup(long long &x,long long &y,long long x1,long long y1,long long x2,long long y2){
	x=x1*x2%mo;
	y=y1*y2%mo;
	long long z=gcd(x,y);
	x/=z;
	y/=z;
	z=(y2-x2+mo)%mo;
	long long a=z*x2%mo,b=y2*y2%mo;
	z=(b-a+mo)%mo;
	x=x*b%mo;
	y=y*z%mo;
	z=gcd(x,y);
	x/=z;
	y/=z;
}
void buil(int k,int l,int r){
	if(l==r){
		x[k]=a[l];
		y[k]=b[l];
		return;
	}
	int mid=(l+r)/2;
	buil(k*2,l,mid);
	buil(k*2+1,mid+1,r);
	pup(x[k],y[k],x[k*2],y[k*2],x[k*2+1],y[k*2+1]);
}
void cne(int loc,int p,int q,int k,int l,int r){
	if(loc==l&&loc==r){
		x[k]=p;
		y[k]=q;
		return;
	}
	int mid=(l+r)/2;
	if(loc<=mid)cne(loc,p,q,k*2,l,mid);
	else cne(loc,p,q,k*2+1,mid+1,r);
	pup(x[k],y[k],x[k*2],y[k*2],x[k*2+1],y[k*2+1]);
}
void sea(int p,int q,int k,int l,int r){
	if(p==l&&q==r){
		sx[k]=x[k];
		sy[k]=y[k];
		return;
	}
	int mid=(l+r)/2;
	if(q>mid)if(p>mid){
			sea(p,q,k*2+1,mid+1,r);
			sx[k]=sx[k*2+1];
			sy[k]=sy[k*2+1];
		}
		else{
			sea(p,mid,k*2,l,mid);
			sea(mid+1,q,k*2+1,mid+1,r);
			pup(sx[k],sy[k],sx[k*2],sy[k*2],sx[k*2+1],sy[k*2+1]);
		}
	else{
		sea(p,q,k*2,l,mid);
		sx[k]=sx[k*2];
		sy[k]=sy[k*2];
	}
}
long long pwr(long long x,long long y){
	long long s=1;
	while(y){
		if(y&1)s=s*x%mo;
		y=y/2;
		x=x*x%mo;
	}
	return s;
}
long long che(int p,int q){
	sea(p,q,1,1,n);
	long long ans=sx[1]*pwr(sy[1],mo-2)%mo;
	return ans;
}
int main(){
	freopen("hs.in","r",stdin);
	freopen("hs.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		scanf("%d%d",&a[i],&b[i]);
	buil(1,1,n);
	for(int i=1;i<=m;i++){
		scanf("%d",&ch);
		if(ch==1){
			scanf("%d%d%d",&z,&p,&q);
			cne(z,p,q,1,1,n);
		}
		else{
			scanf("%d%d",&p,&q);
			printf("%lld\n",che(p,q));
		}
	}
	return 0;
}

/*
s=a+aab+aaabb..
abs=aab+aaabb..
(1-ab)s=a
s=a/(1-ab)
*/
