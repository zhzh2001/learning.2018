#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,m,mo,zs;
ll f[5000][5000],f2[5000][5000],a[10000],b[10000],c[2000][5000];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
ll ksm(ll x,ll y)
{
	if(y==0)return 1;
	ll ans=x,z=x;y--;
	while(y>0)
	{
		if(y%2==1)
		{
			ans*=z;
			ans%=mo;
		}
		y%=2;
		z=z*z;
		z%=mo;
	}
	return ans;
}
inline ll C(ll x,ll y)
{
	ll ans=1;
	For(i,1,zs)
	{
		ll jzq=c[i][x]-c[i][x-y]-c[i][y];
		ans*=ksm(b[i],jzq);	
		ans%=mo;
	}
	return ans;
}
int main()
{
	freopen("bp.in","r",stdin);
	freopen("bp.out","w",stdout);
	m=read();n=read();mo=read();
	memset(f,0,sizeof(f));
	memset(f2,0,sizeof(f2));
	For(i,1,n)a[i]=i;
	For(i,2,n)
	{
		if(a[i]==1)continue;
		zs++;
		b[zs]=i;
		For(j,1,n/i)
		{
			while(a[j*i]%i==0)
			{
				c[zs][j*i]++;
				a[j*i]/=i;
			}
		}
	}
	For(i,1,n)
		For(j,1,zs)
			c[j][i]+=c[j][i-1];
	/*For(i,1,n)
	{
		For(j,1,zs)
			cout<<"c["<<j<<"]["<<i<<"]="<<c[j][i]<<" ";
		cout<<endl;
	}*/
	/*For(i,1,m)
	{
		For(j,1,n-m+1)
		{
			For(k,min(1,i-1),n)
			{
				For(kk,i-2+k,n)
				{
					if(kk+j+m-i>n)break;
					if(kk<k)continue;
					ll jzq=0;
					For(kkk,0,kk)
					{
						jzq+=C(kk,kkk);
						jzq%=mo;
					}
					jzq*=C(n-kk,j);
					jzq%=mo;
					f2[j+kk][j]+=f1[kk][k]*jzq;
					f2[j+kk][j]%=mo;
				}
			}
		}
		For(j,1,n)For(k,1,n)f1[j][k]=f2[j][k],f2[j][k]=0;
	}*/
	ll jzq=n-m;
	f[0][jzq]=1;
	For(i,1,m)
	{
		For(j,0,jzq)
		{
			For(k,j,jzq)
			{
				if(f[i-1][k]==0)continue;
				ll lzq=0,fb=jzq-k+i-1;
				For(kk,0,fb)
				{
					lzq+=C(fb,kk);
					lzq%=mo;
				}
				lzq*=C(n-fb,j+1);
				lzq%=mo;
				f[i][k-j]+=f[i-1][k]*lzq;
				f[i][k-j]%=mo;
			}
		}
	}
	ll ans=0;
	For(j,0,jzq)
	{
		ans+=f[m][j];
		ans%=mo;
	}
	cout<<ans<<endl;
	return 0;
}

