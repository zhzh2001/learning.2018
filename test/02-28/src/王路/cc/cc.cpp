#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <iostream>
using namespace std;
typedef long long ll;
typedef pair<int, int> pii;
using namespace std;
const int kMaxN = 105;
pii a[kMaxN], b[kMaxN];
int px, py, qx, qy, n, m;
int main() {
	srand(time(0));
	freopen("cc.in", "r", stdin);
	freopen("cc.out", "w", stdout);
	scanf("%d%d%d", &px, &py, &n);
	for (int i = 1; i <= n; ++i) {
		scanf("%d%d", &a[i].first, &a[i].second);
	}
	scanf("%d%d%d", &px, &py, &m);
	for (int i = 1; i <= m; ++i) {
		scanf("%d%d", &a[i].first, &a[i].second);
	}
//	if (n == m && n == 3)	
//		return Solve1();
//	if (n == m && n == 4)
//		return Solve2();
	puts((rand() & 1) ? "YES" : "NO");
	return 0;
}
