#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;
typedef long long ll;
typedef pair<int, int> pii;
using namespace std;
const int kMaxN = 1005, kMaxM = 30005;

int m, n, p;
int f[kMaxN][kMaxN], C[105][105];
ll fact[kMaxM];

inline ll QuickPow(ll x, ll y) {
	ll res = 1;
	for (; y; x = x * x % p, y >>= 1)
		if (y & 1) res = res * x % p;
	return res;	
}

inline ll Comb(int x, int y) {
	if (p == 998244353)
		return fact[x] * QuickPow(fact[y], p - 2) % p * QuickPow(fact[x - y], p - 2) % p;
	else
		return C[x][y];
}

int main() {
	freopen("bp.in", "r", stdin);
	freopen("bp.out", "w", stdout);
	scanf("%d%d%d", &m, &n, &p);
	C[0][0] = 1;
	for (int i = 1; i <= 100; ++i) {
		C[i][0] = 1;
		for (int j = 1; j <= 100; ++j) {
			C[i][j] = (C[i - 1][j] + C[i - 1][j - 1]) % p;
		}
	}
	fact[0] = 1;
	for (int i = 1; i < kMaxN; ++i)
		fact[i] = ((ll)fact[i - 1] * i) % p;
	for (int i = 1; i <= n; ++i) {
		f[1][i] = Comb(n, i);
	}
	for (int i = 2; i <= m; ++i)
		for (int j = i; j <= n; ++j)
			for (int k = i - 1; k < j; ++k) {
				f[i][j] = (f[i][j] + f[i - 1][k] * Comb(n - k, j - k) % p * QuickPow(2, k) % p) % p;
			}
	int sum = 0;
	for (int i = 1; i <= n; ++i)
		sum = ((ll)sum + f[m][i]) % p;
	printf("%d\n", sum);
	return 0;
}
