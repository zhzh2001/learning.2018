#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;
typedef long long ll;
const int kMod = 998244353;
inline ll QuickPow(ll x, ll y) {
	ll res = 1;
	for (; y; x = x * x % kMod, y >>= 1)
		if (y & 1) res = res * x % kMod;
	return res;	
}
int n, q;
inline int Solve1() {
	int x, y;
	scanf("%d %d", &x, &y);
	ll rev = QuickPow(y, kMod - 2) * (ll)x % kMod;
	while (q--) {
		int opt;
		scanf("%d", &opt);	
		if (opt == 1) {
			int k, x, y;
			scanf("%d %d %d", &k, &x, &y);
			rev = QuickPow(y, kMod - 2) * (ll)x % kMod;
		} else {
			int l, r;
			scanf("%d %d", &l, &r);
			printf("%d\n", rev);
		}
	}
	return 0;
}
inline int Solve2() {
	ll rev[10];
	for (int i = 1; i <= n; ++i) {
		int x, y;
		scanf("%d %d", &x, &y);
		rev[i] = QuickPow(y, kMod - 2) * x % kMod;	
	}
	while (q--) {
		int opt;
		scanf("%d", &opt);
		if (opt == 1) {
			int k, x, y;
			scanf("%d %d %d", &k, &x, &y);
			rev[k] = QuickPow(y, kMod - 2) * (ll)x % kMod;
		} else {
			int l, r;
			scanf("%d %d", &l, &r);
			if (l == r)
				printf("%d\n", rev[l]);
			if (l + 1 == r) {
				printf("%d\n", (rev[l] * rev[l + 1] % kMod + rev[l] + (1 - rev[l + 1] + kMod) % kMod * rev[l] % kMod * rev[l + 1] % kMod) % kMod);
			} else if (l + 2 == r) {
				printf("%d\n", rev[l] * rev[l + 1] % kMod);
			}
		}
	}
	return 0;
}
int main() {
	freopen("hs.in", "r", stdin);
	freopen("hs.out", "w", stdout);	
	scanf("%d %d", &n, &q);
	if (n == 1) return Solve1();
	if (n <= 3 && q <= 10) return Solve2();
	return 0;
}
