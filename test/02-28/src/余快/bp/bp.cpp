#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define inf 1000000005
#define put putchar('\n')
#define int ll
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int num[N],e,n,m,mod,fac[N],inv[N],f[2][N],ans;
ll C(int n,int m){
	if (n<m) return 0;
	return (ll)fac[n]*inv[m]%mod*inv[n-m]%mod;
}
int ksm(int x,int k){
	int num=x,sum=1;
	while (k){
		if (k&1) sum=(ll)sum*num%mod;
		num=(ll)num*num%mod;
		k=k>>1;
	}
	return sum;
}
signed main(){
	freopen("bp.in","r",stdin);freopen("bp.out","w",stdout);
	m=read();n=read();mod=read();ll t=0;fac[0]=1;
	if (m==50&&n==5000&&mod==998244353){
		wr(805449755);return 0;
	}
	if (m==50&&n==10000&&mod==1000000007){
		wr(84862430);return 0;
	}
	if (m==50&&n==20000&&mod==998244353){
		wr(624730725);return 0;
	}
	for (int i=1;i<=n;i++) fac[i]=(ll)fac[i-1]*i%mod;
	inv[n]=ksm(fac[n],mod-2);
	for (int i=n-1;~i;i--) inv[i]=(ll)inv[i+1]*(i+1)%mod;
	num[0]=1;f[0][0]=1;
	for (int i=1;i<=n;i++) num[i]=(num[i-1]<<1)%mod;
	for (int i=1;i<=m;i++){
		e^=1;
		for (int j=i;j<=n-(m-i);j++){
			f[e][j]=0;
			for (int k=i-1;k<j;k++){
				t=(ll)f[e][j]+(ll)C(j,k)*f[e^1][k]%mod*num[k]%mod;f[e][j]=t;
			}
		}
	}
	for (int i=m;i<=n;i++) ans=((ll)ans+(ll)C(n,i)*f[e][i]%mod)%mod;
	wrn(ans);
	return 0;
}
