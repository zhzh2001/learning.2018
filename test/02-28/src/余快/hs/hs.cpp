#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 1000000005
#define mod 998244353
#define int ll
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,x[N],y[N],ni[N],op,l,r,d;
int gcd(int a,int b){
	return b==0?a:gcd(b,a%b);
}
struct xx{
	int x,y;
	void pre(){
		int t=gcd(x,y);
		if (t==0) return;
		x/=t;y/=t;
	}
}f[N],g[N];
int ksm(int x,int k){
	int num=x,sum=1;
	while (k){
		if (k&1) sum=(ll)sum*num%mod;
		num=(ll)num*num%mod;
		k=k>>1;
	}
	return sum;
}
void work(int l,int r){
	f[l]=g[l];
	for (int i=l+1;i<=r;i++){
		f[i]=g[i];
		xx p;
		p.x=(g[i].y-g[i].x+mod)%mod;p.y=g[i].y;
		p.x=(p.x*f[i-1].x)%mod;p.y=p.y*f[i-1].y%mod;
//		int t=1-(y[i]-x[i])*ni[i]%mod*f[i-1]%mod;
		p.x=((p.y-p.x)%mod+mod)%mod;
		f[i].x=f[i].x*p.y%mod;
		f[i].y=f[i].y*p.x%mod;
	}
	f[r+1].x=f[r+1].y=1;
	for (int i=r;i>=l;i--){
		f[i].x=f[i].x*f[i+1].x%mod;
		f[i].y=f[i].y*f[i+1].y%mod;
	} 
	wrn(f[l].x*ksm(f[l].y,mod-2)%mod);
}
signed main(){
	freopen("hs.in","r",stdin);freopen("hs.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n;i++){
		g[i].x=read();g[i].y=read();ni[i]=ksm(y[i],mod-2);
	}
	for (int i=1;i<=m;i++){
		op=read();
		if (op==1){
			d=read();g[d].x=read();g[d].y=read();g[d].pre();
		}
		else{
			l=read();r=read();
			work(l,r);
		}
	} 
	return 0;
}
