#include <fstream>
using namespace std;
ifstream fin("bp.in");
ofstream fout("bp.out");
const int N = 12;
int m, n, p, f[N + 1][1 << N];
inline void update(int &x, int y)
{
	x += y;
	if (x > p)
		x -= p;
}
int main()
{
	fin >> m >> n >> p;
	f[0][0] = 1;
	for (int i = 0; i < m; i++)
		for (int s = 0; s < 1 << n; s++)
			if (f[i][s])
				for (int ns = 0; ns < 1 << n; ns++)
					if ((s | ns) > s)
						update(f[i + 1][s | ns], f[i][s]);
	int ans = 0;
	for (int s = 0; s < 1 << n; s++)
		update(ans, f[m][s]);
	fout << ans << endl;
	return 0;
}