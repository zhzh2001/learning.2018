#include <fstream>
#include <cmath>
using namespace std;
ifstream fin("cc.in");
ofstream fout("cc.out");
const int N = 105;
const double eps = 1e-6, pi = acos(-1.);
struct point
{
	double x, y;
	bool operator==(const point &rhs) const
	{
		return fabs(x - rhs.x) < eps && fabs(y - rhs.y) < eps;
	}
	double dist(const point &rhs) const
	{
		return sqrt((x - rhs.x) * (x - rhs.x) + (y - rhs.y) * (y - rhs.y));
	}
} p[N], q[N], pd[N], qd[N], np[N], nq[N];
inline double stri(point tri[])
{
	double a = tri[1].dist(tri[2]), b = tri[2].dist(tri[3]), c = tri[3].dist(tri[1]), p = (a + b + c) / 2;
	return sqrt(p * (p - a) * (p - b) * (p - c));
}
inline double srect(point rect[])
{
	return rect[1].dist(rect[2]) * rect[2].dist(rect[3]);
}
int main()
{
	point po;
	int n;
	fin >> po.x >> po.y >> n;
	for (int i = 1; i <= n; i++)
	{
		fin >> p[i].x >> p[i].y;
		double d = po.dist(p[i]);
		pd[i].x = (p[i].x - po.x) / d;
		pd[i].y = (p[i].y - po.y) / d;
	}
	double ps = .0;
	if (n == 3)
		ps = stri(p);
	else if (n == 4)
		ps = srect(p);
	else
	{
		point tri[4];
		tri[1] = po;
		for (int i = 1; i <= n; i++)
		{
			tri[2] = p[i];
			tri[3] = p[i % n + 1];
			ps += stri(tri);
		}
	}
	point qo;
	int m;
	fin >> qo.x >> qo.y >> m;
	for (int i = 1; i <= m; i++)
	{
		fin >> q[i].x >> q[i].y;
		double d = qo.dist(q[i]);
		qd[i].x = (q[i].x - qo.x) / d;
		qd[i].y = (q[i].y - qo.y) / d;
	}
	double qs = .0;
	if (m == 3)
		qs = stri(q);
	else if (m == 4)
		qs = srect(q);
	else
	{
		point tri[4];
		tri[1] = qo;
		for (int i = 1; i <= m; i++)
		{
			tri[2] = q[i];
			tri[3] = q[i % m + 1];
			qs += stri(tri);
		}
	}
	double delta = pi * 2. / 5e5 * (n + m);
	for (double a = .0; a < pi * 2.; a += delta)
	{
		for (int i = 1; i <= n; i++)
		{
			if (po == p[i])
				np[i] = p[i];
			else
			{
				double ncos = pd[i].x * cos(a) - pd[i].y * sin(a), nsin = pd[i].y * cos(a) + pd[i].x * sin(a);
				np[i].x = po.x + ncos * po.dist(p[i]);
				np[i].y = po.y + nsin * po.dist(p[i]);
			}
		}
		for (int i = 1; i <= m; i++)
		{
			if (qo == q[i])
				nq[i] = q[i];
			else
			{
				double ncos = qd[i].x * cos(a) - qd[i].y * sin(a), nsin = qd[i].y * cos(a) + qd[i].x * sin(a);
				nq[i].x = qo.x + ncos * qo.dist(q[i]);
				nq[i].y = qo.y + nsin * qo.dist(q[i]);
			}
		}
		for (int i = 1; i <= n; i++)
		{
			double s = .0;
			point tri[4];
			tri[1] = np[i];
			for (int j = 1; j <= m; j++)
			{
				tri[2] = nq[j];
				tri[3] = nq[j % m + 1];
				s += stri(tri);
			}
			if (fabs(s - ps) < eps)
			{
				fout << "YES\n";
				return 0;
			}
		}
		for (int i = 1; i <= m; i++)
		{
			double s = .0;
			point tri[4];
			tri[1] = nq[i];
			for (int j = 1; j <= n; j++)
			{
				tri[2] = np[j];
				tri[3] = np[j % n + 1];
				s += stri(tri);
			}
			if (fabs(s - qs) < eps)
			{
				fout << "YES\n";
				return 0;
			}
		}
	}
	fout << "NO\n";
	return 0;
}