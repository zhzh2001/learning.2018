#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

int m,n,Mod;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
int f[2][1<<20],c[1<<20];
int main(){
	m=read(),n=read(),Mod=read();
	Rep(i,1,(1<<n)-1){
		c[i]=c[i-(i&-i)]+1;
	}
	f[0][0]=1;
	int now=1;
	Rep(t,1,m){
		now=t&1;int pre=now^1;
		memset(f[now],0,sizeof(f[now]));
		Rep(i,1,(1<<n)-1){
			for (int sub=(i-1)&i;true;sub=(sub-1)&i){
				Add(f[now][i],1ll*f[pre][sub]*(1<<c[sub])%Mod);
				if (sub==0) break;
			}
		}
		puts("start");
		Rep(i,0,(1<<n)-1){
			printf("i=%d c=%d f=%d\n",i,c[i],f[now][i]);
		}
	}
	int Ans=0;
	Rep(i,1,(1<<n)-1) Add(Ans,f[now][i]);
	printf("%d\n",Ans);
}
