#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=1005;

int m,n,Mod;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
int f[2][N],Pow2[N],C[N][N];
int main(){
	m=read(),n=read(),Mod=read();
	f[0][0]=1;
	int now=0;
	Pow2[0]=1;
	Rep(i,1,n) Pow2[i]=2ll*Pow2[i-1]%Mod;
	C[0][0]=1;
	Rep(i,1,n){
		C[i][0]=1;
		Rep(j,1,i) C[i][j]=(C[i-1][j-1]+C[i-1][j])%Mod;
	}
	Rep(t,1,m){
		now=t&1;int pre=now^1;
		memset(f[now],0,sizeof(f[now]));
		Rep(i,0,n) Rep(j,1,n) if (i+j<=n) Add(f[now][i+j],1ll*f[pre][i]*C[n-i][j]%Mod*Pow2[i]%Mod);
		Rep(i,0,n) printf("%d ",f[now][i]);puts("");
	}
	int Ans=0;
	Rep(i,0,n) Add(Ans,f[now][i]);
	printf("%d\n",Ans);
}
/*
c[n-i][j]
A[i]=A'[n-i]
f[i]*C[i][j]
*/
