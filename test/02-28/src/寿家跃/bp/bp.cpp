#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read() {
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9') {
		if (ch=='-') f=-1;
		ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

inline ll Pow(ll base,ll k,ll Mod) {
	base%=Mod;
	ll res=1;
	for (ll i=1; i<=k; i*=2,base=base*base%Mod) {
		if (i&k) res=res*base%Mod;
	}
	return res;
}

const int N=200005;
const int p1=998244353,p2=1004535809,p3=469762049;

int Mod;

inline void Add(int &num,int val) {
	if ((num+=val)>=Mod) num-=Mod;
}

const ll M_=1ll*p1*p2,inv1=Pow(p1,p2-2,p2),inv2=Pow(p2,p1-2,p1),inv3=Pow(M_%p3,p3-2,p3);

int rev[N],a[N],b[N];

struct NTT{
	int P,num;
	int w[2][N];
	void Pre(int p,int n) {
		P=p;
		num=n;
		int ng=Pow(3,(P-1)/num,P),ig=Pow(ng,P-2,P);
		w[1][0]=w[0][0]=1;
		for(int i=1;i<num;i++)
			w[1][i]=1LL*w[1][i-1]*ng%P,w[0][i]=1LL*w[0][i-1]*ig%P;
	}
	void FFT(int *a,int n,int r) {
		for (int i=1;i<n;i++){
			if (rev[i]>i) swap(a[i],a[rev[i]]);
		}
		for (int i=1;i<n;i<<=1){
			for (int j=0;j<n;j+=(i<<1)){
				for (int k=0;k<i;k++) {
					int x=a[j+k],y=1ll*a[j+k+i]*w[r][num/(i<<1)*k]%P;
					a[j+k]=(x+y)%P;
					a[j+k+i]=(x+P-y)%P;
				}
			}
		}
		if (!r) for (int i=0,inv=Pow(n,P-2,P);i<n;i++) a[i]=1ll*a[i]*inv%P;
	}
}ntt[3];

inline ll Mul(ll a,ll b,ll p) {
	a%=p;
	b%=p;
	return ((a*b-(ll)((ll)((long double)a/p*b+1e-3)*p))%p+p)%p;
}

inline int CRT(int a1,int a2,int a3) {
	ll A=(Mul(1ll*a1*p2%M_,inv2,M_)+Mul(1ll*a2*p1%M_,inv1,M_))%M_;
	ll K=(1ll*a3+p3-A%p3)*inv3%p3;
	return (K*(M_%Mod)+A)%Mod;
}

int m,n,len,A_[N],B_[N],Dat[3][N];

inline void Poly_Mul(int *A,int *B) {
	int L=0;
	while(!(len>>L&1)) L++;
	L--;
	Rep(i,0,len-1) rev[i]=(rev[i>>1]>>1)|((i&1)<<L);
	if (Mod==p1) {
		int k=0;
		Rep(i,0,len-1) A_[i]=A[i],B_[i]=B[i];
		ntt[k].FFT(A_,len,1);
		ntt[k].FFT(B_,len,1);
		Rep(i,0,len-1) Dat[k][i]=1ll*A_[i]*B_[i]%ntt[k].P;
		ntt[k].FFT(Dat[k],len,0);
		Rep(i,0,len-1) A[i]=Dat[k][i];
		return;
	}
	Rep(k,0,2) {
		Rep(i,0,len-1) A_[i]=A[i],B_[i]=B[i];
		ntt[k].FFT(A_,len,1);
		ntt[k].FFT(B_,len,1);
		Rep(i,0,len-1) Dat[k][i]=1ll*A_[i]*B_[i]%ntt[k].P;
		ntt[k].FFT(Dat[k],len,0);
	}
	/*
	Rep(k,0,2){
		Rep(i,0,len-1) printf("%d ",Dat[k][i]);puts("");
	}
	*/
	Rep(i,0,len-1) A[i]=CRT(Dat[0][i],Dat[1][i],Dat[2][i]);
}
int Pow2[N],Fac[N],Fav[N];
int main() {
	freopen("bp.in","r",stdin);
	freopen("bp.out","w",stdout);
	m=read(),n=read(),Mod=read();
	len=1;
	while (len<=n) len*=2;
	len*=2;
	ntt[0].Pre(p1,len);
	ntt[1].Pre(p2,len);
	ntt[2].Pre(p3,len);
	Pow2[0]=1;
	Rep(i,1,n) Pow2[i]=2ll*Pow2[i-1]%Mod;
	Fac[1]=1;
	Rep(i,2,n) Fac[i]=1ll*i*Fac[i-1]%Mod;
	Fav[n]=Pow(Fac[n],Mod-2,Mod);
	Dep(i,n-1,1) Fav[i]=1ll*Fav[i+1]*(i+1)%Mod;
	static int A[N],B[N],C[N];
	C[0]=1;
	Rep(t,1,m) {
		Rep(i,0,n) A[i]=1ll*C[i]*Pow2[i]%Mod*Fac[n-i]%Mod;
		Rep(i,n+1,len-1) A[i]=0;
		Rep(i,0,n) B[i]=Fav[i];
		Rep(i,n+1,len-1) B[i]=0;
		Poly_Mul(A,B);
		Rep(i,0,n) C[i]=A[i];
		Rep(i,0,n-1) C[i]=1ll*C[i]*Fav[n-i]%Mod;
	}
	int Ans=0;
	Rep(i,0,n) Add(Ans,C[i]);
	printf("%d\n",Ans);
}
/*
13 15 998244353
*/
