#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int Mod=998244353;
const int N=105;

inline int Pow(int base,int k){
	int res=1;
	for (int i=1;i<=k;i*=2,base=1ll*base*base%Mod){
		if (i&k) res=1ll*res*base%Mod;
	}
	return res;
}
inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
int n,q,w[N];
int Dat[N][N],res[N];

void Swap(int x,int y){
	Rep(i,1,n+1) swap(Dat[x][i],Dat[y][i]);
}
void Gauss(){
	Rep(i,1,n){
		bool b=false;
		Rep(j,i,n) if (Dat[j][i]!=0){
			b=true;Swap(i,j);break;
		}
		if (!b) continue;
		Rep(j,i+1,n){
			int val=1ll*Dat[j][i]*Pow(Dat[i][i],Mod-2)%Mod;
			Rep(k,i,n+1) Add(Dat[j][k],Mod-1ll*val*Dat[i][k]%Mod);
		}
	}
	memset(res,0,sizeof(res));
	Dep(i,n,1){
		if (Dat[i][i]) res[i]=1ll*Dat[i][n+1]*Pow(Dat[i][i],Mod-2)%Mod;
		Rep(j,1,i-1) Add(Dat[j][n+1],Mod-1ll*Dat[j][i]*res[i]%Mod);
	}
}
int main(){
	freopen("hs.in","r",stdin);
	freopen("hs.out","w",stdout);
	n=read(),q=read();
	Rep(i,1,n){
		int x=read(),y=read();
		w[i]=1ll*x*Pow(y,Mod-2)%Mod;
	}
	Rep(t,1,q){
		int opt=read();
		if (opt==1){
			int d=read(),x=read(),y=read();
			w[d]=1ll*x*Pow(y,Mod-2)%Mod;
		}
		if (opt==2){
			memset(Dat,0,sizeof(Dat));
			int x=read(),y=read();
			Rep(i,x,y){
				Add(Dat[i][i],1);
				if (i<y) Add(Dat[i][i+1],Mod-w[i]);else Add(Dat[i][n+1],w[i]);
				int w_=1-w[i];
				if (w_<0) w_+=Mod;
				if (i>x) Add(Dat[i][i-1],Mod-w_);
			}
			Gauss();
			printf("%d\n",res[x]);
		}
	}
}
