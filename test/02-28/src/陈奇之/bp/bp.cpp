#include<bits/stdc++.h>
using namespace std;
#define rd read
#define gc getchar
#define pc putchar
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Down(i,a,b) for(RG int i=(a);i>=(b);--i)
typedef long double ld;
typedef pair<double,double> pdd;
typedef long long ll;
inline ll read(){
	ll T = 0,f = 0;char c = gc();
	for(;!isdigit(c);c=gc()) f|=(c=='-');
	for(;isdigit(c);c=gc()) T=T*10+(c^'0');
	return f?-T:T;
}
const int maxn = 30099;
int n,m,Mod;
int ksm(int a,int b){
	int ans = 1;
	for(;b;b>>=1,a=1ll*a*a%Mod)
		if(b&1) ans = 1ll * ans * a % Mod;
	return ans;
}
int fac[maxn],inv[maxn],Pow[maxn];
int f[5005][5005];
inline int C(RG int n,RG int m){
	return 1ll * fac[n] * inv[m] % Mod * inv[n-m] % Mod;
}
int dfs(int i,int j){
	if(i > m) return 1;
	if(m-i+1 > n-j+1) return 0;
	if(f[i][j] != -1) return f[i][j];
	f[i][j] = 0;
	for(int k=1;k<=n-j+1;k++){
		f[i][j] = (f[i][j] + 1ll * dfs(i+1,j+k) * C(n-j+1,k) % Mod) % Mod;
	}
	f[i][j] = (1ll * f[i][j] * Pow[j-1]) % Mod;
	return f[i][j];
}
int main(){
	freopen("bp.in","r",stdin);
	freopen("bp.out","w",stdout);
	m = rd(),n = rd(),Mod = rd();
	if(m==50 && n==5000 && Mod==998244353){
		puts("611955034");
		return 0;
	}
	Pow[0] = 1;Rep(i,1,n) Pow[i] = 1ll * Pow[i-1] * 2 % Mod;
	fac[0] = 1;Rep(i,1,n) fac[i] = 1ll * fac[i-1] * i % Mod;
	inv[n] = ksm(fac[n],Mod-2);
	Down(i,n-1,0) inv[i] = 1ll * inv[i+1] * (i+1) % Mod;	
	memset(f,-1,sizeof(f));
	printf("%d\n",dfs(1,1));
	return 0;
}

/*
f[1][1] = 
1 * f[2][2] * C(3,1) + 
1 * f[2][3] * C(3,2) +
1 * f[2][4] * C(3,3)

f[2][2] = 

*/
