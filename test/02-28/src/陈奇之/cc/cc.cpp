#include<bits/stdc++.h>
using namespace std;
#define rd read
#define gc getchar
#define pc putchar
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
typedef long double ld;
typedef pair<double,double> pdd;
typedef long long ll;
const double eps = 1e-3;
const double pi = acos(-1.0);
inline ll read(){
	ll T = 0,f = 0;char c = gc();
	for(;!isdigit(c);c=gc()) f|=(c=='-');
	for(;isdigit(c);c=gc()) T=T*10+(c^'0');
	return f?-T:T;
}
#define x first
#define y second
double Cross(pdd a,pdd b,pdd c){
	return (a.x-c.x) * (b.y-c.y) - (b.x-c.x) * (a.y-c.y);
}
bool IsCross(pdd &a,pdd &b,pdd &c,pdd &d){
	double f1 = Cross(b,c,a),f2 = Cross(b,d,a);
	if(f1<-eps&&f2<-eps) return false;
	if(f1>eps &&f2> eps) return false;
	f1 = Cross(a,c,d),f2 = Cross(b,c,d);
	if(f1<-eps&&f2<-eps) return false;
	if(f1>eps &&f2> eps) return false;
	return true;
}
const int maxn = 105;
pdd p[maxn],q[maxn];
pdd pp[maxn],qq[maxn];
double px,py,qx,qy;
int n,m;
int main(){
	freopen("cc.in","r",stdin);
	freopen("cc.out","w",stdout);
	px = rd(),py = rd();n = rd();
	Rep(i,1,n)
		pp[i].x = rd(),pp[i].y = rd();
	qx = rd(),qy = rd();m = rd();
	Rep(i,1,m)
		qq[i].x = rd(),qq[i].y = rd();
	for(long double w = 0.001;w <= 2*pi;w += 0.001){
		long double _cos = cos(-w);
		long double _sin = sin(-w);
		Rep(i,1,n){
			long double x = pp[i].x - px;
			long double y = pp[i].y - py;
			p[i].x = _cos * x - _sin * y + px;
			p[i].y = _sin * x + _cos * y + py;
//			printf("%lf %lf\n",p[i].x,p[i].y);
		}
//		puts("");
		Rep(i,1,m){
			double x = qq[i].x - qx;
			double y = qq[i].y - qy;
			q[i].x = _cos * x - _sin * y + qx;
			q[i].y = _sin * x + _cos * y + qy;
//			printf("%lf %lf\n",q[i].x,q[i].y);
		}
//		puts("");
		p[n+1] = p[1],q[n+1] = q[1];
		Rep(i,1,n)Rep(j,1,m){
			if(IsCross(p[i],p[i+1],q[j],q[j+1])){
				puts("YES");
				return 0;
			}
		}
	}
	puts("NO");
	return 0;
}
/*
1 0
4
0 0
1 0
1 5
0 5
9 0
4
9 0
9 -5
10 -5
10 0
*/
