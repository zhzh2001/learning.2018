#include<bits/stdc++.h>
using namespace std;
#define rd read
#define gc getchar
#define pc putchar
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
typedef long long ll;
inline ll read(){
	ll T = 0,f = 1;char c = gc();
	for(;!isdigit(c);c=gc()) f&=(c=='-');
	for(;isdigit(c);c=gc()) T=T*10+(c^'0');
	return f?T:-T;
}
int main(){
	rep(i,1,1000)
		rep(j,1,1000){
			if(fabs((1.0 * i / j) - 0.3076923077) < 1e-5){
				printf("%d %d\n",i,j);
			}
		}
}
