#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=30005;
ll poi[N],ans;
ll n,m,mod;
ll f[N],g[N];
ll a[N];
inline ll qpow(ll x,ll y,ll p)
{
	ll ret=1;
	while(y){
		if(y&1)ret=ret*x;
		if(ret>1e9)ret%=p;
		x*=x;
		if(x>1e9)x%=p;
		y>>=1;
	}
	return ret%mod;
}
inline ll getC(ll n,ll m)
{
	return a[n]%mod*qpow(a[n-m],mod-2,mod)%mod*qpow(a[m],mod-2,mod)%mod;
}
int main()
{
	freopen("bp.in","r",stdin);
	freopen("bp.out","w",stdout);
	m=read();n=read();mod=read();
	a[0]=1;
	for(int i=1;i<=n;i++)
		a[i]=a[i-1]*i%mod;
	poi[0]=1;
	for(int i=1;i<=n;i++)
		poi[i]=poi[i-1]*2%mod;
	f[0]=1;
	for(int i=m;i>=1;i--){
		for(int j=n-i;j>=m-i;j--)
			for(int k=n-j;k>=1;k--)
//	cout<<getC(n-0,1)*poi[0]<<endl;
//	for(int i=1;i<=m;i++){
//		for(int j=i-1;j<=n;j++)
//			for(int k=1;k+j<=n;k++)
				g[j+k]=(g[j+k]+f[j]*getC(n-j,k)%mod*poi[j]%mod)%mod;
		for(int j=1;j<=n;j++)
			f[j]=g[j],g[j]=0;
//		for(int i=1;i<=n;i++)
//			cout<<f[i]<<' ';
//		cout<<endl;
	}
	for(int i=m;i<=n;i++)
		ans=f[i]+ans,ans%=mod;
	writeln(ans%mod);
}
