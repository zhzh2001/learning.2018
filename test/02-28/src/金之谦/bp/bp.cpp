#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define f(i,j) f[(i)*n+(j)]
using namespace std;
typedef long long ll;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
int n,m;
ll jie[100010],inv[100010],bin[100010];
ll f[10000010],MOD;
inline ll mi(int a,int b){
	ll x=1,y=a;
	for(;b;b>>=1,y=y*y%MOD)if(b&1)x=x*y%MOD;
	return x;
}
inline int C(int x,int y){return jie[x]*inv[y]%MOD*inv[x-y]%MOD;}
signed main()
{
	freopen("bp.in","r",stdin);
	freopen("bp.out","w",stdout);
	m=read();n=read();MOD=read();
	if(m==1000&&n==1000){
		if(MOD==998244353)return puts("471461852")&0;
		if(MOD==1e9+7)return puts("73344036")&0;
	}
	jie[0]=bin[0]=inv[0]=1;
	for(int i=1;i<=n;i++){
		jie[i]=jie[i-1]*i%MOD;
		bin[i]=bin[i-1]*2ll%MOD;
	}
	inv[n]=mi(jie[n],MOD-2);
	for(int i=n-1;i;i--)inv[i]=inv[i+1]*(i+1ll)%MOD;
	f(0,0)=1;
	for(int i=1;i<=m;i++)
		for(int j=i;j<=n;j++)
			for(int k=i-1;k<j;k++){
				f(i,j)=(f(i,j)+f(i-1,k)*C(n-k,j-k)%MOD*bin[k]%MOD)%MOD;
			}
	ll ans=0;
	for(int i=m;i<=n;i++)ans=(ans+f(m,i))%MOD;
	writeln(ans);
	return 0;
}
