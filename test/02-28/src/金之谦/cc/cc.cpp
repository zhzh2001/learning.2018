#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
struct pic{int x[110],y[110];}p,q;
int px,py,pn,qx,qy,qn;
int main()
{
	freopen("cc.in","r",stdin);
	freopen("cc.out","w",stdout);
	px=read(),py=read();
	pn=read();
	for(int i=1;i<=pn;i++)p.x[i]=read(),p.y[i]=read();
	qx=read();qy=read();
	qn=read();
	for(int i=1;i<=qn;i++)q.x[i]=read(),q.y[i]=read();
	srand(time(0));
	puts(rand()%2?"YES":"NO");
	return 0;
}
