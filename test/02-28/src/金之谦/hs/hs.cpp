#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=998244353;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,x[100010],y[100010],p[100010];
inline int mi(int a,int b){
	int x=1,y=a;
	for(;b;b>>=1,y=y*y%MOD)if(b&1)x=x*y%MOD;
	return x;
}
inline int gcd(int a,int b){return b?gcd(b,a%b):a;}
signed main()
{
	freopen("hs.in","r",stdin);
	freopen("hs.out","w",stdout);
	n=read();int q=read();
	for(int i=1;i<=n;i++){
		x[i]=read(),y[i]=read();
		p[i]=gcd(x[i],y[i]);
		x[i]/=p[i];y[i]/=p[i];
	}
	while(q--){
		int op=read();
		if(op==1){
			int k=read();int X=read(),Y=read();
			x[k]=X;y[k]=Y;p[k]=gcd(X,Y);
			x[k]/=p[k];y[k]/=p[k];
		}else{
			int l=read(),r=read();
			int ans=x[l],anss=y[l];
			for(int i=l+1;i<=r;i++){
				ans=ans*x[i];anss=anss*y[i]-1-(i-l-1)*2;
				if(i<r){
					int p=gcd(ans,anss);
					ans/=p;anss/=p;
				}
				ans%=MOD;anss%=MOD;
			}
			ans=ans*mi(anss,MOD-2)%MOD;
			writeln(ans);
		}
	}
	return 0;
}
