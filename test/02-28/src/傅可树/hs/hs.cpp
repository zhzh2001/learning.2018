#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
typedef long long ll;
const int mod=998244353,maxn=105;
int n,q;
inline ll pow(ll x,int k){
	ll y=1;
	while (k){
		if (k&1) y=y*x%mod;
		x=x*x%mod;
		k>>=1;
	}
	return y;
}
inline ll inv(ll x){
	return pow(x,mod-2);
}
inline ll get(ll x,ll y){
	return x*inv(y)%mod;
}
ll p[maxn][2];
inline void init(){
	scanf("%d%d",&n,&q);
	for (int i=1;i<=n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		p[i][1]=get(x,y);
		p[i][0]=get(y-x,y);
	}
}
ll a[maxn][maxn];
int N;
inline void build(int l,int r){
	memset(a,0,sizeof a);
	N=r-l+2;
	a[1][2]=p[l+1][0]; a[1][1]=-1; a[1][N]=-1; a[1][N+1]=-1;
	for (int i=2;i<N-1;i++){
		a[i][i-1]=p[l+i-2][1];
		a[i][i+1]=p[l+i][0];
		a[i][i]=mod-1;
		a[i][N]=-1; a[i][N+1]=-1;
	}
	a[N-1][N-2]=p[r-1][1]; a[N-1][N-1]=-1; a[N-1][N]=-1; a[N-1][N+1]=-1;
	a[N][N-1]=p[r][1]; a[N][N+1]=-1; a[N][N]=-1;
	a[N+1][1]=p[l][0]; a[N+1][N]=-1; a[N+1][N+1]=-1;
	N++;
}
inline void gauss(){
	for (int i=1;i<=N;i++){
		int pos=i;
		for (int j=i+1;j<=N;j++){
			if (abs(a[j][i])>abs(a[pos][i])){
				pos=j;
			}
		}
		for (int j=i;j<=N+1;j++){
			swap(a[pos][j],a[i][j]);
		}
		int res=inv(a[i][i]);
		for (int j=i+1;j<=N;j++){
			ll temp=a[j][i]*res%mod;
			for (int k=i;k<=N+1;k++){
				a[j][k]=((a[j][k]-temp*a[i][k])%mod+mod)%mod;
			}
		}
	}
	for (int i=N;i>=1;i--){
		a[i][N+1]=a[i][N+1]*inv(a[i][i])%mod;
		for (int j=1;j<i;j++){
			a[j][N+1]=((a[j][N+1]-a[i][N+1]*a[j][i])%mod+mod)%mod;
		}
	}
}
inline void solve(){
	for (int i=1;i<=q;i++){
		int opt,x,y,d;
		scanf("%d",&opt);
		if (opt==1){
			scanf("%d%d%d",&d,&x,&y);
			p[d][1]=get(x,y);
			p[d][0]=get(y-x,y);
		}else{
			scanf("%d%d",&x,&y);
			if (x==y){
				printf("%d\n",p[x][1]);
				continue;
			}
			build(x,y);
			gauss();
			printf("%lld\n",a[N-1][N+1]);
		}
	}
}
int main(){
	freopen("hs.in","r",stdin);
	freopen("hs.out","w",stdout);
	init();
	solve();
	return 0;
} 
