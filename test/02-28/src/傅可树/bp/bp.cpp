#include<cstdio>
#include<cstring>
using namespace std;
typedef long long ll;
const int maxn=505;
ll n,m;
ll p;
inline void init(){
	scanf("%lld%lld%lld",&m,&n,&p);
}
ll bin[maxn],C[maxn][maxn];
inline void prepare(){
	bin[0]=1;
	for (int i=1;i<=n;i++){
		bin[i]=bin[i-1]*2%p;
	}
	C[0][0]=1;
	for (int i=1;i<=n;i++){
		C[i][0]=1;
		for (int j=1;j<=i;j++){
			C[i][j]=(C[i-1][j]+C[i-1][j-1])%p;
		}
	}
}
ll dp[2][maxn];
ll cur;
inline void solve(){
	prepare();
	dp[0][0]=1;
	for (int i=1;i<=m;i++){
		memset(dp[cur^1],0,sizeof dp[cur^1]);
		for (int j=1;j<=n;j++){
			for (int k=0;k<j;k++){
				dp[cur^1][j]=(dp[cur^1][j]+dp[cur][k]*C[n-k][j-k]%p*bin[k]%p)%p;
			}
		}
		cur^=1;
	}
	ll ans=0;
	for (int i=1;i<=n;i++){
		(ans+=dp[cur][i])%=p;
	}
	printf("%lld\n",ans);
}
int main(){
	freopen("bp.in","r",stdin);
	freopen("bp.out","w",stdout);
	init();
	solve();
	return 0;
}
