#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
#define dd c=getchar()
typedef long long LL;
inline LL read(){
	LL a=0,b=1;
	char dd;
	while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
LL exgcd(LL a,LL b,LL&x,LL&y){
	if(b==0){
		x=1;
		y=0;
		return a;
	}
	LL ans=exgcd(b,a%b,y,x);
	y-=a/b*x;
	return ans;
}
LL inv(LL x,LL md){
	LL a,b;
	exgcd(x,md,a,b);
	return (a%md+md)%md;
}
LL n,m,md,f[503][503],fac[503],finv[503],ftwo[503],ans;
int main(){
	freopen("bp.in","r",stdin);
	freopen("bp.out","w",stdout);
	m=read();
	n=read();
	md=read();
	ftwo[0]=fac[0]=1;
	for(int i=1;i<=n;i++)ftwo[i]=ftwo[i-1]*2%md;
	for(int i=1;i<=n;i++)fac[i]=fac[i-1]*i%md;
	finv[n]=inv(fac[n],md);
	for(int i=n-1;i>=0;i--)finv[i]=finv[i+1]*(i+1)%md;
	f[0][0]=1;
	for(int i=1;i<=m;i++){
		for(int j=i;j<=n-m+i;j++){
			f[i][j]=0;
			for(int k=i-1;k<j;k++){
				(f[i][j]+=f[i-1][k]*ftwo[k]%md*fac[n-k]%md*finv[j-k]%md*finv[n-j]%md)%=md;
			}
			if(i==m)(ans+=f[i][j])%=md;
		}
	}
	printf("%lld\n",ans);
	return 0;
}
