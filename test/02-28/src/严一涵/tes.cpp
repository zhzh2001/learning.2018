#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
#define dd c=getchar()
const double PI=acos(-1);
inline int read(){
	int a=0,b=1;
	char dd;
	while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
struct point{
	double a,b;
	point(){a=b=0;}
	point(double x,double y){
		a=x;
		b=y;
	}
	void read(){
		a=::read();
		b=::read();
	}
	void move(double x,double y){
		a+=x;
		b+=y;
	}
	double arc(){
		if(a==0){
			if(b>0)return PI/2;
			else return PI/2*3;
		}
		double ans=atan(abs(b/a));
		if(a<0)ans=PI-ans;
		if(b<0)ans=2*PI-ans;
		return ans;
	}
	double dis(){
		return sqrt(a*a+b*b);
	}
	void arc(double x){
		double aa=arc(),bb=dis();
		if(x>2*PI)x-=2*PI;
		if(x<0)x+=2*PI;
		aa+=x;
		while(aa>2*PI)aa-=2*PI;
		while(aa<0)aa+=2*PI;
		if(aa==0){
			a=bb;
			b=0;
			return;
		}else if(aa==PI){
			a=-bb;
			b=0;
			return;
		}else if(aa==PI/2){
			a=0;
			b=bb;
			return;
		}else if(aa==PI/2*3){
			a=0;
			b=-bb;
			return;
		}
		a=b=1.0;
		if(aa>=PI){
			aa=2*PI-aa;
			b=-1;
		}
		b*=bb*sin(aa);
		a*=bb*cos(aa);
	}
};
int main(){
	point x(0,5);
	x.arc(2*PI-PI/2);
	
	printf("%.6lf %.6lf\n",x.a,x.b);
	return 0;
}
