#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
#define dd c=getchar()
const double PI=acos(-1);
inline int read(){
	int a=0,b=1;
	char dd;
	while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
struct point{
	double a,b;
	point(){a=b=0;}
	point(double x,double y){
		a=x;
		b=y;
	}
	void read(){
		a=::read();
		b=::read();
	}
	void move(double x,double y){
		a+=x;
		b+=y;
	}
	double arc(){
		if(a==0){
			if(b>0)return PI/2;
			else return PI/2*3;
		}
		double ans=atan(abs(b/a));
		if(a<0)ans=PI-ans;
		if(b<0)ans=2*PI-ans;
		return ans;
	}
	double dis(){
		return sqrt(a*a+b*b);
	}
	void arc(double x){
		double aa=arc(),bb=dis();
		aa+=x;
		if(aa>2*PI)aa-=2*PI;
		if(aa<0)aa+=2*PI;
		if(aa==0){
			a=bb;
			b=0;
			return;
		}else if(aa==PI){
			a=-bb;
			b=0;
			return;
		}else if(aa==PI/2){
			a=0;
			b=bb;
			return;
		}else if(aa==PI/2*3){
			a=0;
			b=-bb;
			return;
		}
		a=b=1.0;
		if(aa>=PI){
			aa=2*PI-aa;
			b=-1;
		}
		b*=bb*sin(aa);
		a*=bb*cos(aa);
	}
}A,B,ap[103],bp[103],cp[103],dp[103];
int an,bn;
bool ans=0;
bool check(point a,point b,point c,point d){
	b.move(-a.a,-a.b);
	c.move(-a.a,-a.b);
	d.move(-a.a,-a.b);
	a=point();
	double xx=2*PI-b.arc();
	b.arc(xx);
	c.arc(xx);
	d.arc(xx);
	if(!(c.b<=0&&d.b>=0||c.b<=0&&d.b>=0))return 0;
	double k=(c.b-d.b)/(c.a-d.a);
	double tb=c.b-k*c.a;
	double ax=-tb/k;
	return ax>=0&&ax<=b.a;
}
void solve(point&A,point*ap,int an,point&B,point*bp,int bn){
	for(int i=1;i<=an;i++)ap[i].move(-A.a,-A.b);
	for(int i=1;i<=bn;i++)bp[i].move(-A.a,-A.b);
	B.move(-A.a,-A.b);
	A=point();
	double xx=2*PI-B.arc();
	for(int i=1;i<=an;i++)ap[i].arc(xx);
	for(int i=1;i<=bn;i++){
		bp[i].move(-B.a,-B.b);
		bp[i].arc(xx);
		bp[i].move(B.a,B.b);
	}
	B.arc(xx);
	for(int qux=0;qux<360;qux++){
		xx=qux/180.0*PI;
		for(int i=1;i<=an;i++){
			cp[i]=ap[i];
			cp[i].arc(xx);
		}
		for(int i=1;i<=bn;i++){
			dp[i]=bp[i];
			dp[i].move(-B.a,-B.b);
			dp[i].arc(xx);
			dp[i].move(B.a,B.b);
		}
		for(int i=1;i<=an;i++){
			int ti=i-1;
			if(ti==0)ti=an;
			for(int j=1;j<=bn;j++){
				int tj=j-1;
				if(tj==0)tj=bn;
				if(check(cp[i],cp[ti],dp[j],dp[tj])){
					ans=1;
				}
			}
		}
	}
}
int main(){
	freopen("cc.in","r",stdin);
	freopen("cc.out","w",stdout);
	A.read();
	an=read();
	for(int i=1;i<=an;i++)ap[i].read();
	B.read();
	bn=read();
	for(int i=1;i<=bn;i++)bp[i].read();
	solve(A,ap,an,B,bp,bn);
	solve(B,bp,bn,A,ap,an);
	puts(ans?"YES":"NO");
	return 0;
}
