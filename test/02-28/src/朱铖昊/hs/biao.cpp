#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=105,mod=998244353;
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
int main()
{
	freopen("biao.out","w",stdout);
	for (int i=1;i<=10;++i,putchar('\n'))
		for (int j=1;j<=10;++j)
			cout<<(ll)i*ksm(j,mod-2)%mod<<' ';
		return 0;

}