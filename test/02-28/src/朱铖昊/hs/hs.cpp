#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=10005,mod=998244353;
//int f[N][N],g[N];
int f[N];
int a[N],b[N],q,n,x,y,opt,pos;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
inline int guass(int l,int r)
{
	/*memset(f,0,sizeof(f));
	memset(g,0,sizeof(g));
	g[1]=1;
	for (int i=1;i<=r-l+2;++i)
		f[i][i]=1;
	for (int i=l;i<=r;++i)
		f[i-l+2][i-l+1]=(mod-a[i])%mod;
	for (int i=l;i<r;++i)
		f[i-l+1][i-l+2]=(mod-(1+mod-a[i+1])%mod)%mod;
	int K=r-l+2;
	for (int i=1;i<=K;++i)
	{
		if (!f[i][i])
		{
			for (int j=i+1;j<=K;++j)
				if (f[j][i])
				{
					for (int k=1;k<=k;++k)
						swap(f[i][k],f[j][k]);
					swap(g[i],g[j]);
					break;
				}
		}
		if (!f[i][i])
			break;
		int inv=ksm(f[i][i],mod-2);
		for (int j=i;j<=K;++j)
			f[i][j]=(ll)f[i][j]*inv%mod;
		g[i]=(ll)g[i]*inv%mod;
		for (int j=i+1;j<=K;++j)
			if (f[j][i])
			{
				int p=f[j][i];
				for (int k=i;k<=K;++k)
					f[j][k]=((f[j][k]-(ll)f[i][k]*p%mod)%mod+mod)%mod;
				g[j]=((g[j]-(ll)g[i]*p%mod)%mod+mod)%mod;			
			}
	}
	return g[K]*ksm(f[K][K],mod-2);*/
	if (l==r)
		return a[l];
	f[r+1]=1;
	for (int i=r;i>=l;--i)
	{
		if (i>=r-1)
			f[i]=(ll)b[i]*f[i+1]%mod;
		else
			f[i]=(ll)b[i]*(((ll)f[i+1]+(ll)f[i+2]*(a[i+2]-1+mod)%mod+mod)%mod)%mod;
	}
	int k=(f[l]+(ll)f[l+1]*(a[l+1]-1+mod)%mod+mod)%mod;
	return ksm(k,mod-2);
}
int main()
{
	freopen("hs.in","r",stdin);
	freopen("hs.out","w",stdout);
	read(n);
	read(q);
	for (int i=1;i<=n;++i)
	{
		read(x);
		read(y);
		a[i]=(ll)x*ksm(y,mod-2)%mod;
		b[i]=ksm(a[i],mod-2);
	}
	while (q--)
	{
		read(opt);
		if (opt==1)
		{
			read(pos);
			read(x);
			read(y);
			a[pos]=(ll)x*ksm(y,mod-2)%mod;
			b[pos]=ksm(a[pos],mod-2);
		}
		else
		{
			read(x);
			read(y);
			cout<<guass(x,y)<<'\n';
		}
	}
	return 0;
}