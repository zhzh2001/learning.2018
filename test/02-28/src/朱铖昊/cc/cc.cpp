#include<bits/stdc++.h>
using namespace std;
const long double pi=acos(-1.0),inf=1e15,eps=1e-10;
struct point
{
	int rd()
	{
		char c=getchar();
		int x=0,y=1;
		while (c>'9'||c<'0')
		{
			if (c=='-')
				y=-1;
			c=getchar();
		}
		while (c>='0'&&c<='9')
		{
			x=x*10+c-'0';
			c=getchar();
		}
		return x*y;
	}
	long double x,y;
	void read()
	{
		x=rd();
		y=rd();
	}
	point zig_zag(const double &a)
	{
		return (point){cos(a)*x-sin(a)*y,cos(a)*y+sin(a)*x};
	}
	point operator-(const point &a)
	{
		return (point){x-a.x,y-a.y};
	}
	point operator+(const point &a)
	{
		return (point){x+a.x,y+a.y};
	}
};
struct line
{
	long double k,b,x1,x2;
	void make_line(const point &x,const point &y)
	{
		point c=x,d=y;
		if (c.x>d.x)
			swap(c,d);
		x1=c.x;
		x2=d.x;
		if (x2-x1<eps)
		{
			k=inf;
			b=0;
		}
		else
		{
			k=(d.y-c.y)/(d.x-c.x);
			b=c.y-k*c.x;
		}
	}
	int cross(const point &a)
	{
		if (k>=inf)
			return 0;
		if (a.x<x1||a.x>x2)
			return 0;
		long double p=b+k*a.x;
		return p>a.y;
	}
};
const int N=105;
struct graph
{
	int rd()
	{
		char c=getchar();
		int x=0,y=1;
		while (c>'9'||c<'0')
		{
			if (c=='-')
				y=-1;
			c=getchar();
		}
		while (c>='0'&&c<='9')
		{
			x=x*10+c-'0';
			c=getchar();
		}
		return x*y;
	}
	point o,a[N],b[N];
	line c[N];
	int n;
	void read()
	{
		o.read();
		n=rd();
		for (int i=1;i<=n;++i)
			a[i].read();
	}
	void zig_zag(const double &alpha)
	{
		for (int i=1;i<=n;++i)
			b[i]=(a[i]-o).zig_zag(alpha)+o;
		for (int i=1;i<n;++i)
			c[i].make_line(b[i],b[i+1]);
		c[n].make_line(b[1],b[n]);
	}
	bool in(const point &a)
	{
		int sum=0;
		for (int i=1;i<=n;++i)
			sum+=c[i].cross(a);
		return sum&1;
	}
}x,y;
int main()
{
	freopen("cc.in","r",stdin);
	freopen("cc.out","w",stdout);
	x.read();
	y.read();
	x.zig_zag(0);
	for (int i=0;i<4000;++i)
	{
		long double alpha=pi*(long double)i/2000.0;
		x.zig_zag(alpha);
		y.zig_zag(alpha);
		for (int j=1;j<=x.n;++j)
			if (y.in(x.b[j]))
			{
				// cout<<i<<' ';
				puts("YES");
				return 0;
			}
		for (int j=1;j<=y.n;++j)
			if (x.in(y.b[j]))
			{
				// cout<<i<<' ';
				puts("YES");
				return 0;
			}
	}
	puts("NO");
	return 0;
}