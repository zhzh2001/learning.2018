#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
// inline int ksm(int x,int y)
// {
// 	int ans=1;
// 	for (;y;y/=2,x=(ll)x*x%mod)
// 		if (y&1)
// 			ans=(ll)ans*x%mod;
// 	return ans;
// }
// inline void exgcd(int x,int y,int &a,int &b)
// {
// 	if (!y)
// 	{
// 		a=1;
// 		b=0;
// 	}
// }
const int N=5005;
int f[N][N],c[N][N],bits[N],n,m,mod;
int main()
{
	freopen("bp.in","r",stdin);
	freopen("bp.out","w",stdout);
	read(m);
	read(n);
	read(mod);
	// fac[1]=1;
	// for (int i=1;i<=n;++i)
	// 	fac[i]=(ll)fac[i-1]*i%mod;
	c[0][0]=1;
	for (int i=1;i<=n;++i)
	{
		c[i][0]=1;
		for (int j=1;j<=n;++j)
			c[i][j]=(c[i-1][j-1]+c[i-1][j])%mod;
	}
	bits[0]=1;
	for (int i=1;i<=n;++i)
		bits[i]=(ll)bits[i-1]*2%mod;
	f[0][0]=1;
	for (int i=1;i<=m;++i)
		for (int j=1;j<=n;++j)
			for (int k=0;k<j;++k)
				(f[i][j]+=(ll)f[i-1][k]*bits[k]%mod*c[n-k][j-k]%mod)%=mod;
	cout<<f[m][n];
	return 0;
}