#include <bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int sqr(int x) {
  return x * x;
}
// 那你很棒棒哦
int main(int argc, char const *argv[]) {
  freopen("cc.in", "r", stdin);
  freopen("cc.out", "w", stdout);
  int px1 = read(), py1 = read();
  int n = read(), mx1 = 0;
  for (int i = 1; i <= n; i++) {
    int x = read(), y = read();
    mx1 = max(mx1, sqr(x - px1) + sqr(y - py1));
  }
  int px2 = read(), py2 = read();
  int m = read(), mx2 = 0;
  for (int i = 1; i <= m; i++) {
    int x = read(), y = read();
    mx2 = max(mx2, sqr(x - px2) + sqr(y - py2));
  }
  if (px1 == px2 && py1 == py2)
    return puts("NO"), 0;
  (sqrt(mx1) + sqrt(mx2) > sqrt(sqr(px1 - px2) + sqr(py1 - py2)))
    ? puts("YES")
    : puts("NO");
  return 0;
}