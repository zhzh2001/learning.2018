#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
ll fast_pow(ll a, ll b, ll p) {
  ll c = 1 % p;
  for (; b; b >>= 1, a = a * a % p)
    if (b & 1) c = c * a % p;
  return c;
}
int x[N], y[N];
int main(int argc, char const *argv[]) {
  freopen("hs.in", "r", stdin);
  freopen("hs.out", "w", stdout);
  int n = read(), m = read();
  int p = 998244353;
  for (int i = 1; i <= n; i++) {
    x[i] = read();
    y[i] = read();
  }
  while (m --) {
    int op = read();
    if (op == 1) {
      int d = read();
      x[d] = read();
      y[d] = read();
    } else {
      int l = read(), r = read();
      int fz = 0, fm = 0;
      for (int i = l; i <= r; i++) {
        fz += x[i];
        fm += y[i];
      }
      printf("%lld\n", fz * fast_pow(fm, p - 2, p) % p);
    }
  }
  return 0;
}