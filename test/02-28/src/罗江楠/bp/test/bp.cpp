#include <bits/stdc++.h>
#define N 30020
#define ll long long
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
ll n, m, p;
/**
 * 快速幂 (mod p)
 * @author swwind
 */
ll fast_pow(ll a, ll b) {
  ll c = 1 % p;
  for (; b; b >>= 1, a = a * a % p)
    if (b & 1) c = c * a % p;
  return c;
}
/**
 * 求逆元 (mod p)
 * @author swwind
 */
ll inv(ll a) {
  return fast_pow(a, p - 2);
}
ll frac[N];
/**
 * 求组合数 (mod p)
 * @author swwind
 */
ll C(ll n, ll m) {
  return frac[n] * inv(frac[n - m]) % p * inv(frac[m]) % p;
}
ll f[2][N]; // 获得 n 个星的方案数
ll pw2[N];
int main(int argc, char const *argv[]) {
  // start preparing
  freopen("bp.in", "r", stdin);
  // freopen("bp.out", "w", stdout);
  m = read(); n = read(); p = read();
  // end preparing

  // start calculate frac and pw2.
  frac[0] = 1;
  for (int i = 1; i <= n; i++)
    frac[i] = frac[i - 1] * i % p;
  pw2[0] = 1;
  for (int i = 1; i <= n; i++)
    pw2[i] = pw2[i - 1] * 2 % p;
  // end calculate frac and pw2.

  // start n^3 dp
  int now = 0, pre = 1;
  f[now][0] = 1;
  while (m --) {
    swap(now, pre);
    memset(f[now], 0, sizeof f[now]);
    for (int i = 1; i <= n; i++) {
      for (int j = 0; j <= i - 1; j++) {
        f[now][i] = (f[now][i] + f[pre][j] * pw2[j] % p * C(n - j, n - i) % p) % p;
      }
    }
  }
  // end n^3 dp

  // start output
  ll ans = 0;
  for (int i = 1; i <= n; i++)
    ans = (ans + f[now][i]) % p;
  cout << ans << endl;
  // end output

  return 0;
}