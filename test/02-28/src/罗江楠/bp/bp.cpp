#include <bits/stdc++.h>
#define N 30020
#define ll long long
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
ll n, m, p;
/**
 * 快速幂 (mod p)
 * @author swwind
 */
ll fast_pow(ll a, ll b) {
  ll c = 1 % p;
  for (; b; b >>= 1, a = a * a % p)
    if (b & 1) c = c * a % p;
  return c;
}
/**
 * 求逆元 (mod p)
 * @author swwind
 */
ll _inv(ll a) {
  return fast_pow(a, p - 2);
}
ll frac[N];
ll inv[N];
ll f[N]; // 获得 n 个星的方案数
ll pw2[N];
ll c[N];
int main(int argc, char const *argv[]) {
  // start preparing
  freopen("bp.in", "r", stdin);
  freopen("bp.out", "w", stdout);
  m = read(); n = read(); p = read();
  // end preparing

  // start calculate frac and pw2.
  frac[0] = pw2[0] = inv[0] = 1;
  for (int i = 1; i <= n; i++) {
    frac[i] = frac[i - 1] * i % p;
    pw2[i] = pw2[i - 1] * 2 % p;
    inv[i] = _inv(frac[i]); // nlog
  }
  // end calculate frac and pw2.

  // start n^3 dp
  f[0] = frac[n];
  while (m --) {
    for (int i = n; i; i--) {
      f[i] = 0;
      for (int j = 0; j <= i - 1; j++) { // n^3
        // C(n - j, n - i) = frac[n - j] * inv[i - j] * inv[n - i]
        // C(n - j, i - j) = frac[n - j] * inv[n - i] * inv[i - j]
        f[i] = (f[i] + f[j] * inv[i - j] % p * inv[n - i] % p) % p;
      }
    }
    if (!m) break;
    for (int i = 1; i <= n; i++)
      f[i] = f[i] * pw2[i] % p * frac[n - i] % p; // n^2
    f[0] = 0;
  }
  // end n^3 dp

  // start output
  ll ans = 0;
  for (int i = 1; i <= n; i++)
    ans = (ans + f[i]) % p;
  cout << ans << endl;
  // end output

  return 0;
}
/*
let f[i][j] => 第 i 次，获得 j 个星的方案数
let ans[i] = \sum_{j=1}^{n}{f[i][j]}
let res[i] = \sum_{j=0}^{n-1}{f[i][j] * 2^j}

ans[i+1] = \sum_{j=0}^{n-1}{f[i][j] * (2^n - 2^j)}
ans[i+1] = (ans[i] - f[i][n]) * 2^n - res[i]

因为 f[i][n] = res[i]

ans[i+1] = (ans[i] - res[i]) * 2^n - res[i]
res[i+1] = ... WTF ... 菜啊。。。

*/