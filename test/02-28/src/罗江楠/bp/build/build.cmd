@echo off
g++ bp.cpp -o bp.exe -lm -std=c++11
if NOT errorlevel==0 (
  echo 编译错误
  pause && exit
)
for /L %%i in (1, 1, 2) do (
  copy bp%%i.in bp.in > nul
  bp.exe
  if NOT errorlevel 0 (
    color 5
    echo Runtime Error at test %%i
    pause > nul
  )
  fc bp.out bp%%i.out > nul
  if errorlevel 1 (
    color 4
    echo Wrong Answer at test %%i
    pause > nul
  )
  cls
)
color 2
echo Accepted
pause > nul

