#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define md ((l+r)>>1)
using namespace std;
const int N=100100,P=998244353;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline int ksm(int x,int y){
	int z=1;for(;y;y>>=1,x=1ll*x*x%P)
	if(y&1)z=1ll*z*x%P;return z;
}
int n,m,p[N],f[N];
int main(){
	freopen("hs.in","r",stdin);
	freopen("hs1.out","w",stdout);
	register int o,i,x,y,k;
	n=read();m=read();
	for(i=1;i<=n;i++){
		x=read();y=ksm(read(),P-2);
		p[i]=1ll*x*y%P;
	}
	for(;m--;){
		o=read();
		if(o==1){
			i=read();
			x=read();y=ksm(read(),P-2);
			p[i]=1ll*x*y%P;
		}else{
			x=read();y=read();
			f[x-1]=0;k=1;
			for(i=x;i<=y;i++){
				f[i]=1ll*(p[i]-1)*f[i-1]%P;
				f[i]=ksm(1+f[i],P-2);
				f[i]=1ll*f[i]*p[i]%P;
				k=1ll*k*f[i]%P;
			}printf("%d\n",k);
		}
	}
	return 0;
}
