#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define db double
using namespace std;
const int N=110;
const db pi=acos(-1),eps=1e-5;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
struct pt{db x,y;}nw;
pt operator+(pt i,pt j){return (pt){i.x+j.x,i.y+j.y};}
pt operator-(pt i,pt j){return (pt){i.x-j.x,i.y-j.y};}
db operator*(pt i,pt j){return i.x*j.y-i.y*j.x;}
db dis(pt x,pt y){
	x=x-y;
	return sqrt(x.x*x.x+x.y*x.y);
}
struct cqz{
	int n;
	pt o,q[N];
	void in(){
		o.x=read();o.y=read();
		n=read();
		for(int i=1;i<=n;i++)
		q[i].x=read(),q[i].y=read();
		q[n+1]=q[1];
	}
}a,b,c,d;
cqz mie(cqz x,db k){
	db c=cos(k),s=sin(k);
	cqz y;
	y.o=x.o;y.n=x.n;
	for(int i=1;i<=y.n+1;i++){
		nw=x.q[i]-x.o;
		y.q[i].x=nw.x*c-nw.y*s;
		y.q[i].y=nw.x*s+nw.y*c;
		y.q[i]=y.q[i]+y.o;
	}return y;
}
bool in(pt c,pt a,pt b){
	if(a.x>b.x)swap(a,b);
	if(c.x<a.x-eps||c.x>b.x+eps)return 0;
	if(a.y>b.y)swap(a,b);
	if(c.y<a.y-eps||c.y>b.y+eps)return 0;return 1;
}
db get(pt a,pt b,pt c,pt d){
	db s1,s2;pt e;
	s1=(c-a)*(b-a);
	s2=(b-a)*(d-a);
	s1=s1/(s1+s2);
	e.x=c.x+s1*(d.x-c.x);
	e.y=c.y+s1*(d.y-c.y);
	if(in(e,a,b)&&in(e,c,d))return -1;
	s1=min(dis(a,c),dis(a,d));
	s1=min(s1,dis(b,c));
	s1=min(s1,dis(b,d));
	return s1;
}
db solve(db jiao){
	c=mie(a,jiao);
	d=mie(b,jiao);
	db res=1e8;
	for(int i=c.n;i;i--)
	for(int j=d.n;j;j--)
	res=min(res,get(c.q[i],c.q[i+1],d.q[j],d.q[j+1]));
	return res;
}
db ran(){
	return rand()%10000/10000.0;
}
int cnt;
double now,res,to1,re1,to2,re2,T,to,re,dt;
int main(){
	freopen("cc.in","r",stdin);
	freopen("cc.out","w",stdout);
	srand(233*666);
	a.in();b.in();
	for(int ti=min(30000/a.n/b.n,1000);ti;ti--){
	T=1000;now=0;res=solve(now);
	while(T>1e-3){
		to1=now+pi*(T/1000);
		to2=now-pi*(T/1000);
		re1=solve(to1);
		if(re1<0)
		goto fuck;
		re2=solve(to2);
		if(re2<0)
		goto fuck;
		dt=res-re1;
		if(dt>0||exp(dt/T)>ran())now=to1,res=re1;
		dt=res-re2;
		if(dt>0||exp(dt/T)>ran())now=to2,res=re2;
		T*=0.965;cnt++;
	}
	//printf("%lf\n",now);
	}puts("NO");
	//printf("%d\n",cnt);
	return 0;fuck:puts("YES");
}
