#include<bits/stdc++.h>
#define db double
using namespace std;
const db pi=acos(-1);
db tmpx,tmpy,nxtdeg;
struct P{
	db x,y;
};
struct L{
	P x,y;
};
int sgn(db x){
	return (fabs(x)<1e-7?0:(x<0?-1:1));
}
db cha(P a,P b,P c){
	return (b.x-a.x)*(c.y-a.y)-(b.y-a.y)*(c.x-a.x);
}
bool online(L a,P b){
	return ((a.x.x-1e-5<=b.x&&b.x<=a.y.x+1e-5)||(a.y.x-1e-5<=b.x&&b.x<=a.x.x+1e-5))&&
		   ((a.x.y-1e-5<=b.y&&b.y<=a.y.y+1e-5)||(a.y.y-1e-5<=b.y&&b.y<=a.x.y+1e-5));
}
bool insert(L a,L b){
	int v1=sgn(cha(a.x,a.y,b.x));
	int v2=sgn(cha(a.x,a.y,b.y));
	if (v1*v2==1) return 0;
	if (!v1&&online(a,b.x)) return 1;
	if (!v2&&online(a,b.y)) return 1;
	int v3=sgn(cha(b.x,b.y,a.x));
	int v4=sgn(cha(b.x,b.y,a.y));
	if (v3*v4==1) return 0;
	if (!v3&&online(b,a.x)) return 1;
	if (!v4&&online(b,a.y)) return 1;
	return 1;
}
struct Graph{
	P a[105],O;
	int n;
	void init(P &a){
		double x,y;
		scanf("%lf%lf",&x,&y);
		a.x=x; a.y=y;
	}
	void init(){
		init(O);
		scanf("%d",&n);
		for (int i=1;i<=n;i++)
			init(a[i]);
	}
	void rotate(){
		for (int i=1;i<=n;i++){
			db x=a[i].x-O.x,y=a[i].y-O.y;
			a[i].x=O.x+x*tmpy-y*tmpx;
			a[i].y=O.y+x*tmpx+y*tmpy;
		}
	}
	L getline(int x){
		return (L){a[x],a[x%n+1]};
	}
}G1,G2;
bool check(){
	for (int i=1;i<=G1.n;i++)
		for (int j=1;j<=G2.n;j++)
			if (insert(G1.getline(i),G2.getline(j)))
				return 1;
	return 0;
}
int main(){
	freopen("cc.in","r",stdin);
	freopen("cc.out","w",stdout);
	G1.init(); G2.init();
	db mx=0;
	for (int i=1;i<=G1.n;i++){
		mx=max(mx,fabs(G1.a[i].x));
		mx=max(mx,fabs(G1.a[i].y));
	}
	for (int i=1;i<=G2.n;i++){
		mx=max(mx,fabs(G2.a[i].x));
		mx=max(mx,fabs(G2.a[i].y));
	}
	mx=max(mx,fabs(G1.O.x));
	mx=max(mx,fabs(G1.O.y));
	mx=max(mx,fabs(G2.O.x));
	mx=max(mx,fabs(G2.O.y));
	for (int i=1;i<=G1.n;i++)
		G1.a[i].x/=mx,G1.a[i].y/=mx;
	for (int i=1;i<=G2.n;i++)
		G2.a[i].x/=mx,G2.a[i].y/=mx;
	G1.O.x/=mx; G1.O.y/=mx;
	G2.O.x/=mx; G2.O.y/=mx;
	int times=12345678/G1.n/G2.n;
	times+=times&1;
	db deg=2*pi/times;
	tmpx=sin(deg);
	tmpy=cos(deg);
	for (int i=1;i<=times;i++){
		if (check()) return puts("YES"),0;
		G1.rotate(); G2.rotate();
	}
	puts("NO");
}
/*
1 0
4
0 0
1 0
1 5
0 5
9 0
4
9 0
9 -5
10 -5
10 0

0 0
3
1 0
2 -1
2 1
0 0
3
-1 0
-2 1
-2 -1
*/
