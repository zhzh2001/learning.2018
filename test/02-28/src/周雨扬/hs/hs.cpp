#include<bits/stdc++.h>
#define mo 998244353
#define N 135791
using namespace std;
int P[N],f[N];
int n,Q;
int power(int x,int y){
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
int main(){
	freopen("hs.in","r",stdin);
	freopen("hs.out","w",stdout);
	scanf("%d%d",&n,&Q);
	for (int i=1;i<=n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		P[i]=1ll*x*power(y,mo-2)%mo;
	}
	while (Q--){
		int fl,x,y,p;
		scanf("%d",&fl);
		if (fl==1){
			scanf("%d%d%d",&p,&x,&y);
			P[p]=1ll*x*power(y,mo-2)%mo;
		}
		else{
			scanf("%d%d",&x,&y);
			f[x]=1; f[x-1]=0;
			for (int i=x;i<=y;i++)
				f[i+1]=1ll*(f[i]-1ll*f[i-1]*(mo+1-P[i])%mo+mo)*power(P[i],mo-2)%mo;
			printf("%d\n",power(f[y+1],mo-2));
		}
	}
}
/*
3 7
1 3
1 2
2 3
2 1 1
2 1 2
2 1 3
1 2 2 3
2 1 1
2 1 2
2 1 3
*/
