#include<bits/stdc++.h>
#define N 135791
using namespace std;
int R[N],a[N],b[N],inv[N];
int n,m,mo,c,L,ans;
int power(int x,int y){
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
void FFT(int *a,int n,int l,int f){
	for (int i=1;i<n;i++)
		R[i]=(R[i>>1]>>1)|((i&1)<<(l-1));
	for (int i=0;i<n;i++)
		if (i<R[i]) swap(a[i],a[R[i]]);
	for (int d=1;d<n;d<<=1){
		int wn=power(3,(mo-1)/(d<<1));
		if (f==-1) wn=power(wn,mo-2);
		for (int i=0;i<n;i+=d<<1)
			for (int j=i,w=1;j<i+d;j++,w=1ll*w*wn%mo){
				int y=1ll*a[j+d]*w%mo;
				a[j+d]=(a[j]+mo-y)%mo;
				a[j]=(a[j]+y)%mo;
			}
	}
	if (f==-1){
		int v=power(n,mo-2);
		for (int i=0;i<n;i++)
			a[i]=1ll*a[i]*v%mo;
	}
}
void poly(int l){
	if (l==1){
		for (int i=1;i<=n;i++) a[i]=inv[i];
		return;
	}
	int mid=l/2;
	poly(mid);
	for (int i=1;i<=n;i++)
		b[i]=1ll*a[i]*power(2,i*mid)%mo;
	FFT(a,c,L,1); FFT(b,c,L,1);
	for (int i=0;i<c;i++)
		a[i]=1ll*a[i]*b[i]%mo,b[i]=0;
	if (l&1){
		for (int i=1;i<=n;i++)
			b[i]=1ll*inv[i]*power(2,i*(l-1))%mo;
		FFT(b,c,L,1);
		for (int i=0;i<c;i++)
			a[i]=1ll*a[i]*b[i]%mo,b[i]=0;
	}
	FFT(a,c,L,-1);
	a[0]=0;
	for (int i=n+1;i<c;i++)
		a[i]=0;
}
int main(){
	freopen("bp.in","r",stdin);
	freopen("cgt.out","w",stdout);
	scanf("%d%d%d",&m,&n,&mo);
	if (mo==998244353){
		c=1,L=0;
		for (;c<=3*n;c<<=1,L++);
		for (int i=1;i<=n;i++)
		 	inv[i]=power(i,mo-2);
		inv[0]=1;
		for (int i=1;i<=n;i++)
			inv[i]=1ll*inv[i]*inv[i-1]%mo;
		poly(m);
		inv[0]=1;
		for (int i=1;i<=n;i++)
			ans=(ans+1ll*a[i]*inv[n-i]%mo)%mo;
		for (int i=1;i<=n;i++)
			ans=1ll*ans*i%mo;
		printf("%d",ans);
	}
}
