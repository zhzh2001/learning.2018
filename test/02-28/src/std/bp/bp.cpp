#include <cstdio>
#include <cmath>
#include <cstring>
#include <algorithm>

using namespace std;

int fac[40000],inv[40000];
int b[40000],f[50][40000],g[50][40000],h[40000];
int i,j,k,n,p,s,t;
long long m;

inline int power(int u,int v)
{
	int s=1;
	for (int i=30;i>=0;i--)
	{
		s=1LL*s*s%p;
		if ((1<<i)&v)
			s=1LL*s*u%p;
	}
	return s;
}

inline void preC()
{
	fac[0]=1;
	for (i=1;i<=n;i++)
		fac[i]=1LL*i*fac[i-1]%p;
	inv[n]=power(fac[n],p-2);
	for (i=n-1;i>=0;i--)
		inv[i]=1LL*(i+1)*inv[i+1]%p;
	return;
}

inline int C(int u,int v)
{
	return 1LL*fac[u]*inv[v]%p*inv[u-v]%p;
}

const int q=33333;
const double pi=acos(-1);

struct complex
{
	double r,i;
	
	inline complex operator + (const complex t) const
	{
		return (complex) {r+t.r,i+t.i};
	}
	
	inline complex operator - (const complex t) const
	{
		return (complex) {r-t.r,i-t.i};
	}
	
	inline complex operator * (const complex t) const
	{
		return (complex) {r*t.r-i*t.i,r*t.i+i*t.r};
	}
};

complex W[80000][2],X[80000],Y[80000],Z[80000];
int R[80000],u[80000],v[80000],z[80000];
int u0[80000],u1[80000],v0[80000],v1[80000],z0[80000],z1[80000],z2[80000];
int w;

inline void preFFT()
{
	for (w=1;w<=n;w=w<<1);
	w=w<<1;
	for (int i=0;i<w;i++)
	{
		W[i][0]=(complex) {cos(2*pi*i/w),sin(2*pi*i/w)};
		W[i][1]=(complex) {cos(2*pi*i/w),-sin(2*pi*i/w)};
	}
	for (int i=0;i<w;i++)
		for (int j=i,k=1;k<w;j=j>>1,k=k<<1)
			R[i]=(R[i]<<1)|(j&1);
	return;
}

inline void FFT(complex *T,int t)
{
	for (int i=0;i<w;i++)
		if (i<R[i])
			swap(T[i],T[R[i]]);
	complex u,v;
	for (int i=1;i<w;i=i<<1)
		for (int j=0;j<w;j=j+(i<<1))
			for (int k=0;k<i;k++)
				u=T[j+k],v=W[w/(i<<1)*k][t]*T[i+j+k],T[j+k]=u+v,T[i+j+k]=u-v;
	if (t)
		for (int i=0;i<w;i++)
			T[i].r=T[i].r/w;
	return;
}

inline void turncomplex(complex *X,int *u)
{
	for (int i=0;i<w;i++)
		X[i].r=u[i],X[i].i=0;
	return;
}

inline void turnnum(int *u,complex *X)
{
	for (int i=0;i<w;i++)
		u[i]=(u[i]+((long long) (X[i].r+0.5))%p)%p;
	return;
}

inline void mainFFT(int *u,int *v,int *z)
{
	turncomplex(X,u);
	turncomplex(Y,v);
	FFT(X,0);FFT(Y,0);
	for (int i=0;i<w;i++)
		Z[i]=X[i]*Y[i];
	FFT(Z,1);
	turnnum(z,Z);
	return;
}

inline void solve(int m)
{
	if (b[m])
		return;
	t++,b[m]=t;
	for (int i=0;i<=n;i++)
		g[b[m]][i]=power(h[m],i);
	if (m==1)
	{
		for (int i=1;i<=n;i++)
			f[b[m]][i]=1;
		return;
	}
	int t1=m/2,t2=m-m/2;
	solve(t1),solve(t2);
	memset(u,0,sizeof(u));
	for (int i=0;i<=n;i++)
		u[i]=1LL*f[b[t1]][i]*g[b[t2]][i]%p*inv[i]%p;
	memset(u0,0,sizeof(u0));
	for (int i=0;i<=n;i++)
		u0[i]=u[i]%q;
	memset(u1,0,sizeof(u1));
	for (int i=0;i<=n;i++)
		u1[i]=u[i]/q;
	memset(v,0,sizeof(v));
	for (int i=0;i<=n;i++)
		v[i]=1LL*f[b[t2]][i]*inv[i]%p;
	memset(v0,0,sizeof(v0));
	for (int i=0;i<=n;i++)
		v0[i]=v[i]%q;
	memset(v1,0,sizeof(v1));
	for (int i=0;i<=n;i++)
		v1[i]=v[i]/q;
	memset(z0,0,sizeof(z0));
	mainFFT(u0,v0,z0);
	memset(z1,0,sizeof(z1));
	mainFFT(u1,v0,z1);
	mainFFT(u0,v1,z1);
	memset(z2,0,sizeof(z2));
	mainFFT(u1,v1,z2);
	memset(z,0,sizeof(z));
	for (int i=0;i<=n;i++)
		z[i]=(1LL*q*q*z2[i]+1LL*q*z1[i]+z0[i])%p;
	for (int i=0;i<=n;i++)
		f[b[m]][i]=1LL*z[i]*fac[i]%p;
	return;
}

int main()
{
	freopen("bp.in","r",stdin);
	freopen("bp.out","w",stdout);
	scanf("%d%d%d",&m,&n,&p);
	preC();
	preFFT();
	h[0]=1;
	for (i=1;i<=n;i++)
		h[i]=(h[i-1]+h[i-1])%p;
	solve(m);
	for (i=m;i<=n;i++)
		s=(s+1LL*f[b[m]][i]*C(n,i))%p;
	printf("%d",s);
	return 0;
}
