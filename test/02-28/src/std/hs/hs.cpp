#include <cstdio>

const int p=998244353;

inline int power(int x,int y)
{
	int s=1;
	for (int i=30;i>=0;i--)
	{
		s=1LL*s*s%p;
		if ((1<<i)&y)
			s=1LL*s*x%p;
	}
	return s;
}

struct treenode
{
	int L,R;
};

treenode tree[500000];
int i,m,n,t,x,y,z;

inline treenode merge(treenode l,treenode r)
{
	int t=power(p+1-1LL*r.L*l.R%p,p-2);
	return (treenode) {(l.L+1LL*(p+1-l.L)*r.L%p*(p+1-l.R)%p*t)%p,(r.R+1LL*(p+1-r.R)*l.R%p*(p+1-r.L)%p*t)%p};
}

inline void buildtree(int l,int r,int node)
{
	if (l==r)
	{
		scanf("%d%d",&x,&y);
		int t=power(y,p-2);
		tree[node].L=(p+1-1LL*x*t%p)%p;
		tree[node].R=1LL*x*t%p;
	}
	else
	{
		int mid=(l+r)>>1;
		buildtree(l,mid,node<<1);
		buildtree(mid+1,r,node<<1|1);
		tree[node]=merge(tree[node<<1],tree[node<<1|1]);
	}
	return;
}

inline void inserttree(int l,int r,int x,int y,int z,int node)
{
	if (l==r)
	{
		int t=power(z,p-2);
		tree[node].L=(p+1-1LL*y*t%p)%p;
		tree[node].R=1LL*y*t%p;
	}
	else
	{
		int mid=(l+r)>>1;
		if (x<=mid)
			inserttree(l,mid,x,y,z,node<<1);
		else
			inserttree(mid+1,r,x,y,z,node<<1|1);
		tree[node]=merge(tree[node<<1],tree[node<<1|1]);
	}
	return;
}

inline treenode asktree(int l,int r,int x,int y,int node)
{
	if ((l==x) && (r==y))
		return tree[node];
	int mid=(l+r)>>1;
	if (y<=mid)
		return asktree(l,mid,x,y,node<<1);
	if (x>mid)
		return asktree(mid+1,r,x,y,node<<1|1);
	return merge(asktree(l,mid,x,mid,node<<1),asktree(mid+1,r,mid+1,y,node<<1|1));
}

int main()
{
	freopen("hs.in","r",stdin);
	freopen("hs.out","w",stdout);
	scanf("%d%d",&n,&m);
	buildtree(1,n,1);
	for (i=1;i<=m;i++)
	{
		scanf("%d",&t);
		if (t==1)
		{
			scanf("%d%d%d",&x,&y,&z);
			inserttree(1,n,x,y,z,1);
		}
		else
		{
			scanf("%d%d",&x,&y);
			printf("%d\n",(p+1-asktree(1,n,x,y,1).L)%p);
		}
	}
	return 0;
}
