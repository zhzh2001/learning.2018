#include <cstdio>
#include <cmath>
#include <algorithm>

using namespace std;

const double eps=1e-12;

double pu[1200],pv[1200],qu[1200],qv[1200];
double u0,v0,u1,v1,u2,v2,r,k0,b0,k1,b1,ru,rv,tu,tv,px,py,qx,qy,vx,vy,vz,rx,ry,d,x;
int i,j,pz,qz;

int main()
{
	freopen("cc.in","r",stdin);
	freopen("cc.out","w",stdout);
	scanf("%lf%lf",&px,&py);
	scanf("%d",&pz);
	for (i=1;i<=pz;i++)
		scanf("%lf%lf",&pu[i],&pv[i]);
	pu[0]=pu[pz],pv[0]=pv[pz];
	scanf("%lf%lf",&qx,&qy);
	scanf("%d",&qz);
	for (i=1;i<=qz;i++)
		scanf("%lf%lf",&qu[i],&qv[i]);
	qu[0]=qu[qz],qv[0]=qv[qz];
	for (i=1;i<=pz;i++)
	{
		u0=qx+pu[i]-px,v0=qy+pv[i]-py;
		r=sqrt((px-qx)*(px-qx)+(py-qy)*(py-qy));
		for (j=1;j<=qz;j++)
		{
			ru=min(qu[j-1],qu[j]),rv=min(qv[j-1],qv[j]);
			tu=max(qu[j-1],qu[j]),tv=max(qv[j-1],qv[j]);
			if (qu[j]!=qu[j-1])
				k0=1.0*(qv[j]-qv[j-1])/(qu[j]-qu[j-1]),b0=qv[j]-k0*qu[j];
			else
				k0=1e7,b0=qu[j];
			if (((k0==1e7) && (b0==u0)) || (k0*u0+b0==v0))
			{
				vx=u0-qu[j],vy=v0-qv[j];
				vz=sqrt(vx*vx+vy*vy);
				vx=vx/vz*r,vy=vy/vz*r;
				if ((u2-ru>-eps) && (tu-u2>-eps))
					if ((v2-rv>-eps) && (tv-v2>-eps))
						return puts("YES"),0;
				vx=-vx,vy=-vy;
				u2=u0+vx,v2=v0+vy;
				if ((u2-ru>-eps) && (tu-u2>-eps))
					if ((v2-rv>-eps) && (tv-v2>-eps))
						return puts("YES"),0;
				continue;
			}
			if (k0)
			{
				if (k0==1e7)
					k1=0;
				else
					k1=-1/k0;
				b1=v0-k1*u0;
			}
			else
				k1=1e7,b1=v0;
			if (k0==1e7)
				u1=b0,v1=b1;
			else
				if (k1==1e7)
					u1=b1,v1=b0;
				else
				{
					v1=(k0*b1-k1*b0)/(k0-k1);
					u1=(v1-b1)/k1;
				}
			vx=u0-u1,vy=v0-v1;
			vz=vx*vx+vy*vy;
			d=sqrt(r*r/vz-1);
			u2=u1+vy*d,v2=v1-vx*d;
			if ((u2-ru>-eps) && (tu-u2>-eps))
				if ((v2-rv>-eps) && (tv-v2>-eps))
					return puts("YES"),0;
			u2=u1-vy*d,v2=v1+vx*d;
			if ((u2-ru>-eps) && (tu-u2>-eps))
				if ((v2-rv>-eps) && (tv-v2>-eps))
					return puts("YES"),0;
		}
	}
	for (i=1;i<=qz;i++)
	{
		u0=px+qu[i]-qx,v0=py+qv[i]-qy;
		r=sqrt((qx-px)*(qx-px)+(qy-py)*(qy-py));
		for (j=1;j<=pz;j++)
		{
			ru=min(pu[j-1],pu[j]),rv=min(pv[j-1],pv[j]);
			tu=max(pu[j-1],pu[j]),tv=max(pv[j-1],pv[j]);
			if (pu[j]!=pu[j-1])
				k0=1.0*(pv[j]-pv[j-1])/(pu[j]-pu[j-1]),b0=pv[j]-k0*pu[j];
			else
				k0=1e7,b0=pu[j];
			if (((k0==1e7) && (b0==u0)) || (k0*u0+b0==v0))
			{
				vx=u0-pu[j],vy=v0-pv[j];
				vz=sqrt(vx*vx+vy*vy);
				vx=vx/vz*r,vy=vy/vz*r;
				u2=u0+vx,v2=v0+vy;
				if ((u2-ru>-eps) && (tu-u2>-eps))
					if ((v2-rv>-eps) && (tv-v2>-eps))
						return puts("YES"),0;
				vx=-vx,vy=-vy;
				u2=u0+vx,v2=v0+vy;
				if ((u2-ru>-eps) && (tu-u2>-eps))
					if ((v2-rv>-eps) && (tv-v2>-eps))
						return puts("YES"),0;
				continue;
			}
			if (k0)
			{
				if (k0==1e7)
					k1=0;
				else
					k1=-1/k0;
				b1=v0-k1*u0;
			}
			else
				k1=1e7,b1=v0;
			if (k0==1e7)
				u1=b0,v1=b1;
			else
				if (k1==1e7)
					u1=b1,v1=b0;
				else
				{
					v1=(k0*b1-k1*b0)/(k0-k1);
					u1=(v1-b1)/k1;
				}
			vx=u0-u1,vy=v0-v1;
			vz=vx*vx+vy*vy;
			d=sqrt(r*r/vz-1);
			u2=u1+vy*d,v2=v1-vx*d;
			if ((u2-ru>-eps) && (tu-u2>-eps))
				if ((v2-rv>-eps) && (tv-v2>-eps))
					return puts("YES"),0;
			u2=u1-vy*d,v2=v1+vx*d;
			if ((u2-ru>-eps) && (tu-u2>-eps))
				if ((v2-rv>-eps) && (tv-v2>-eps))
					return puts("YES"),0;
		}
	}
	return puts("NO"),0;
}
