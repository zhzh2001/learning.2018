#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("bp.in","r",stdin);
	freopen("bp.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=(1<<16)+3;
int mod;
ll power(ll x,ll y){
	x%=mod;
	ll ans=1%mod;
	while(y){
		if (y&1)
			ans=ans*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return ans;
}
ll fac[maxn],invf[maxn],pw[maxn];
ll c(int n,int m){
	if (n<m)
		return 0;
	return fac[n]*invf[m]%mod*invf[n-m]%mod;
}
int m,n;
ll t1[maxn],t2[maxn],*f=t1,*g=t2;
namespace bf{
	void work(){
		f[0]=1;
		for(int o=1;o<=m;o++){
			for(int i=0;i<=n;i++){
				g[i]=0;
				if (i<o)
					continue;
				for(int j=0;j<i;j++)
					g[i]=(g[i]+f[j]*c(n-j,i-j)%mod*pw[j])%mod;
			}
			swap(f,g);
		}
		ll ans=0;
		for(int i=0;i<=n;i++)
			ans=(ans+f[i])%mod;
		printf("%lld",(ans%mod+mod)%mod);
	}
}
struct fft{
	int n,k;
	ll omega[maxn],bitrev[maxn];
	void init(int len){
		n=1,k=0;
		while(n<=len)
			n<<=1,k++;
		omega[0]=1;
		ll wn=power(3,(mod-1)/n);
		for(int i=1;i<n;i++){
			omega[i]=omega[i-1]*wn%mod;
			bitrev[i]=(bitrev[i>>1]>>1)|((i&1)<<(k-1));
		}
	}
	ll wn[maxn];
	void trans(ll *a,bool rev){
		for(int i=0;i<n;i++)
			if (bitrev[i]<i)
				swap(a[bitrev[i]],a[i]);
		for(int i=1,qwq=k-1;i<n;i<<=1,qwq--){
			for(int j=0;j<i;j++)
				wn[j]=omega[j<<qwq];
			for(int j=0;j<n;j+=i<<1)
				for(int k=j;k<j+i;k++){
					ll t=a[k+i]*wn[k-j];
					a[k+i]=(a[k]-t)%mod;
					a[k]=(a[k]+t)%mod;
				}
		}
		if (rev){
			reverse(a+1,a+n);
			ll inv=power(n,mod-2);
			for(int i=0;i<n;i++)
				a[i]=a[i]*inv%mod;
		}
	}
	void test(){
		init(10);
		static ll a[maxn];
		a[0]=1,a[1]=2,a[2]=2;
		a[7]=1;
		trans(a,0);
		for(int i=0;i<n;i++)
			a[i]=a[i]*a[i]%mod;
		trans(a,1);
		for(int i=0;i<n;i++)
			printf("a[%d]=%lld\n",i,(a[i]+mod)%mod);
	}
}t;
ll qwq[maxn];
int main(){
	init();
	m=readint(),n=readint();
	mod=readint();
	// t.test();
	fac[0]=1;
	for(int i=1;i<=n;i++)
		fac[i]=fac[i-1]*i%mod;
	invf[n]=power(fac[n],mod-2);
	for(int i=n;i;i--)
		invf[i-1]=invf[i]*i%mod;
	pw[0]=1;
	for(int i=1;i<=n;i++)
		pw[i]=2*pw[i-1]%mod;
	if (n<=500 || mod!=998244353){
		bf::work();
		return 0;
	}
	t.init(n*2+2);
	for(int i=1;i<=n;i++)
		qwq[i]=invf[i];
	t.trans(qwq,0);
	f[0]=fac[n];
	while(m--){
		for(int i=0;i<=n;i++)
			f[i]=f[i]*pw[i]%mod;
		t.trans(f,0);
		for(int i=0;i<t.n;i++)
			f[i]=f[i]*qwq[i]%mod;
		t.trans(f,1);
		// for(int i=0;i<=n;i++)
			// printf("f[%d]=%lld\n",i,f[i]*invf[n-i]%mod);
		for(int i=n+1;i<t.n;i++)
			f[i]=0;
	}
	ll ans=0;
	for(int i=0;i<=n;i++)
		ans=(ans+f[i]*invf[n-i])%mod;
	printf("%lld",(ans%mod+mod)%mod);
}