#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
const int mod=998244353;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("hs.in","r",stdin);
	freopen("hs.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=100002;
ll power(ll x,ll y){
	ll ans=1;
	while(y){
		if (y&1)
			ans=ans*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return ans;
}
struct segtree{
	ll t[maxn*4][4]; //a[0][0],a[0][1],a[1][0],a[1][1]
	ll a[maxn];
	void maintain(int o){
		/*for(int i=0;i<2;i++)
			for(int j=0;j<2;j++){
				t[o][i*2+j]=0;
				for(int k=0;k<2;k++)
					t[o][i*2+j]+=t[o*2+1][i*2+k]*t[o*2][k*2+j];
				t[o][i*2+j]%=mod;
			}*/
		t[o][0]=(t[o*2+1][0]*t[o*2][0]+t[o*2+1][1]*t[o*2][2])%mod;
		t[o][1]=(t[o*2+1][0]*t[o*2][1]+t[o*2+1][1]*t[o*2][3])%mod;
		t[o][2]=(t[o*2+1][3]*t[o*2][2]+t[o*2+1][2]*t[o*2][0])%mod;
		t[o][3]=(t[o*2+1][3]*t[o*2][3]+t[o*2+1][2]*t[o*2][1])%mod;
	}
	void build(int o,int l,int r){
		if (l==r){
			t[o][0]=power(a[l],mod-2);
			t[o][1]=1-t[o][0];
			t[o][2]=1;
			return;
		}
		int mid=(l+r)/2;
		build(o*2,l,mid);
		build(o*2+1,mid+1,r);
		maintain(o);
	}
	int p;
	void update(int o,int l,int r){
		if (l==r){
			t[o][0]=power(a[l],mod-2);
			t[o][1]=1-t[o][0];
			t[o][2]=1;
			return;
		}
		int mid=(l+r)/2;
		if (p<=mid)
			update(o*2,l,mid);
		else update(o*2+1,mid+1,r);
		maintain(o);
	}
	int ql,qr;
	ll ans[4],tmp[4];
	void query(int o,int l,int r){
		if (ql<=l && qr>=r){
			// printf("on %d %d\n",l,r);
			tmp[0]=(t[o][0]*ans[0]+t[o][1]*ans[2])%mod;
			tmp[1]=(t[o][0]*ans[1]+t[o][1]*ans[3])%mod;
			tmp[2]=(t[o][3]*ans[2]+t[o][2]*ans[0])%mod;
			tmp[3]=(t[o][3]*ans[3]+t[o][2]*ans[1])%mod;
			memcpy(ans,tmp,sizeof(ans));
			return;
		}
		int mid=(l+r)/2;
		if (ql<=mid)
			query(o*2,l,mid);
		if (qr>mid)
			query(o*2+1,mid+1,r);
	}
}t;
ll a[maxn],inv[maxn],k[maxn];
int main(){
	init();
	int n=readint(),q=readint();
	for(int i=1;i<=n;i++){
		ll x=readint(),y=readint();
		t.a[i]=x*power(y,mod-2)%mod;
	}
	t.build(1,1,n);
	while(q--){
		int op=readint();
		// fprintf(stderr,"WTF\n");
		if (op==1){
			int d=readint();
			ll x=readint(),y=readint();
			t.a[d]=x*power(y,mod-2)%mod;
			t.p=d;
			t.update(1,1,n);
		}else{
			int l=readint(),r=readint();//set a[l]=x;
			/*k[l-1]=0,k[l]=1;
			for(int i=l;i<=r;i++){ //f[i]=a[i]*f[i+1]+(1-a[i])*f[i-1]
				// f[i+1]=(f[i]-(1-a[i])*f[i-1])/a[i]
				k[i+1]=(k[i]-(1-a[i])*k[i-1])%mod*inv[i]%mod;
			}*/
			t.ql=l,t.qr=r;
			memset(t.ans,0,sizeof(t.ans));
			t.ans[0]=t.ans[3]=1;
			t.query(1,1,n);
			// for(int i=0;i<4;i++)
				// printf("t.ans[%d]=%lld\n",i,t.ans[i]);
			//k[r+1]x=1
			ll ans=power(t.ans[0],mod-2);
			printf("%lld\n",(ans%mod+mod)%mod);
		}
	}
}