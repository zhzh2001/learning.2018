#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
const int mod=998244353;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("hs.in","r",stdin);
	freopen("bf.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=100002;
ll power(ll x,ll y){
	ll ans=1;
	while(y){
		if (y&1)
			ans=ans*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return ans;
}
ll a[maxn],inv[maxn],k[maxn];
int main(){
	init();
	int n=readint(),q=readint();
	for(int i=1;i<=n;i++){
		ll x=readint(),y=readint();
		a[i]=x*power(y,mod-2)%mod;
		inv[i]=power(a[i],mod-2);
	}
	while(q--){
		int op=readint();
		if (op==1){
			int d=readint();
			ll x=readint(),y=readint();
			a[d]=x*power(y,mod-2)%mod,inv[d]=power(a[d],mod-2);
		}else{
			int l=readint(),r=readint();//set a[l]=x;
			k[l-1]=0,k[l]=1;
			for(int i=l;i<=r;i++){ //f[i]=a[i]*f[i+1]+(1-a[i])*f[i-1]
				// f[i+1]=(f[i]-(1-a[i])*f[i-1])/a[i]
				k[i+1]=(k[i]-(1-a[i])*k[i-1])%mod*inv[i]%mod;
			}
			//k[r+1]x=1
			ll ans=power(k[r+1],mod-2);
			printf("%lld\n",(ans%mod+mod)%mod);
		}
	}
}