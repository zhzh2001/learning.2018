//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("hs.in");
ofstream fout("hs.out");
const long long mod=998244353;
long long n,q,ans;
long long x[100003],y[100003];
long long f[100003],g[100003];
inline long long qpow(long long a,long long b){
	long long ans=1;
	for(;b;b>>=1,a=a*a%mod){
		if(b&1){
			ans=ans*a%mod;
		}
	}
	return ans;
}
inline long long calc(long long x,long long y){
	return x*qpow(y,mod-2)%mod;
}
inline void baoli(){
	while(q--){
		int opt,d,l,r;
		fin>>opt;
		if(opt==1){
			fin>>d>>l>>r;
			x[d]=l,y[d]=r;
		}else{
			fin>>l>>r;
			fout<<calc(x[l],y[l])<<endl;
		}
	}
}
int main(){
	fin>>n>>q;
	for(int i=1;i<=n;i++){
		fin>>x[i]>>y[i];
	}
	if(n==1){
		baoli();
		return 0;
	}
	while(q--){
		int opt,d,l,r;
		fin>>opt;
		if(opt==1){
			fin>>d>>l>>r;
			x[d]=l,y[d]=r;
		}else{
			fin>>l>>r;
			if(l==r){
				fout<<calc(x[l],y[l])<<endl;
				continue;
			}
			f[l]=1,g[l]=(1-calc(x[l],y[l])+mod)%mod;
			for(int i=l+1;i<=r;i++){
				f[i]=((1-calc(x[i],y[i])+mod)%mod*g[i-1]%mod+mod)%mod;
			}
			fout<<f[r]*calc(x[r],y[r])%mod<<endl;
		}
	}
	return 0;
}
/*

in:
3 7
1 3
1 2
2 3
2 1 1
2 1 2
2 1 3
1 2 2 3
2 1 1
2 1 2
2 1 3

out:
332748118
598946612
166374059
332748118
748683265
887328314

*/
