//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("bp.in");
ofstream fout("bp.out");
long long m,n,mod;
long long ans,f[503][503];
long long fac[100003],inv[100003],bin[100003];
inline long long C(int n,int m){
	if(n==m){
		return 1;
	}
	if(!n||!m){
		return 0;
	}
	if(n<m){
		return 0;
	}
	return fac[n]*inv[n-m]%mod*inv[m]%mod;
}
inline long long qpow(long long a,long long b){
	long long ans=1;
	for(;b;b>>=1,a=a*a%mod){
		if(b&1){
			ans=ans*a%mod;
		}
	}
	return ans;
}
int main(){
	fin>>m>>n>>mod;
	fac[1]=1;
	for(int i=2;i<=100000;i++){
		fac[i]=fac[i-1]*i%mod;
	}
	inv[100000]=qpow(fac[100000],mod-2);
	for(int i=100000-1;i>=1;i--){
		inv[i]=inv[i+1]*(i+1)%mod;
	}
	bin[0]=1;
	for(int i=1;i<=100000;i++){
		bin[i]=bin[i-1]*2%mod;
	}
	f[0][0]=1;
	for(int i=1;i<=m;i++){
		for(int j=i;j<=n;j++){
			for(int k=1;k<=j;k++){
				f[i][j]=(f[i][j]+f[i-1][j-k]*C(n-j+k,k)%mod*bin[j-k]%mod)%mod;
			}
		}
	}
	for(int j=m;j<=n;j++){
		ans=(ans+f[m][j])%mod;
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
2 3 1000000007

out:
30

*/
