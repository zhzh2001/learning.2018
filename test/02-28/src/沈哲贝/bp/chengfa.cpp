#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define llu unsigned long long
#define ld double
#define llu unsigned long long
#define rep(i,x,y)  for(ll i=x;i<y;++i)
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define pi acos(-1)
#define pa pair<ll,ll>
#define lf else if
#define max(x,y)    ((x)<(y)?(y):(x))
#define min(x,y)    ((x)<(y)?(x):(y))
#define sqr(x)      ((x)*(x))
#define abs(x)      ((x)>0?(x):-(x))
#define Mul(x,y)    ((x)=1LL*(x)*(y)%mod)
#define Add(x,y)    ((x)=((x)+(y))%mod)
#define Max(x,y)    ((x)=((x)<(y)?(y):(x)))
#define Min(x,y)    ((x)=((x)>(y)?(y):(x)))
#define del(x)		((x)<0?(x)+mod:(x))
#define E(x)        return writeln(x),0
#define p(x)        printf("~%lld~\n",x)
#define pp(x,y)     printf("~~%lld %lld~~\n",x,y)
#define ppp(x,y,z)  printf("~~~%lld %lld %lld~~~\n",x,y,z)
#define pppp(a,b,c,d)	printf("~~~%lld %lld %lld %lld\n",a,b,c,d)
#define f_in(x)     freopen(x,"r",stdin)
#define f_out(x)    freopen(x,"w",stdout)
#define open(x)     f_in(x".in"),f_out(x".out")
#define fi first
#define se second
typedef complex<double> E;
namespace SHENZHEBEI{
#define NEG 1
#if NEG
    inline ll read(){    ll x=0,f=1; char ch=getchar();   for (;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1;  for (;isdigit(ch);ch=getchar())  x=x*10-48+ch;   return x*f; }
    inline void write(ll x){    if (x<0) putchar('-'),x=-x;  if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#else
    inline ll read(){    ll x=0; char ch=getchar();   for (;!isdigit(ch);ch=getchar());    for (;isdigit(ch);ch=getchar())  x=x*10-48+ch;   return x;   }
    inline void write(ll x){    if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#endif
    inline char readchar(){ char ch=getchar();   for(;isspace(ch);ch=getchar());  return ch;  }
    inline ll readstr(char *s){ char ch=getchar();   int cur=0;  for(;isspace(ch);ch=getchar());      for(;!isspace(ch);ch=getchar())  s[cur++]=ch;    s[cur]='\0';    return cur; }
    inline void writeln(ll x){  write(x);   puts("");   }
    inline ld getreal(){    static ld lbc;  scanf("%lf",&lbc);  return lbc; }
}using namespace SHENZHEBEI;
const ll N=800010,mod=998244353;
ll A[N],B[N],Ans[N],st[100],n;
void MUL(ll n,ll dep){
//	ppp(n,st,influ);
	if (n<=4){
		rep(i,0,2*n)Ans[i]=0;
		rep(i,0,n)rep(j,0,n)Add(Ans[i+j],A[i]*B[j]%mod);
		return;
	}
	ll Len=n>>1,x,y;
	MUL(Len,dep+1);
	rep(i,st[dep],st[dep]+n*2)Ans[i]=0;
	rep(i,0,Len)swap(A[i],A[i+Len]),swap(B[i],B[i+Len]);
	x=st[dep+1],y=st[dep];
	rep(i,0,n)	Ans[y+i]+=Ans[x+i],Ans[y+i+Len]+=Ans[x+i];
	MUL(Len,dep+1);
	rep(i,0,Len)A[i]=del(A[i+Len]-A[i]),B[i]=del(B[i]-B[i+Len]);
	x=st[dep+1],y=st[dep]+Len;
	rep(i,0,n)	Ans[y+i]+=Ans[x+i],Ans[y+i+Len]+=Ans[x+i];
	MUL(Len,dep+1);
	rep(i,0,Len)A[i]=del(A[i+Len]-A[i]),B[i]=del(B[i]-B[i+Len]),swap(A[i],A[i+Len]),swap(B[i],B[i+Len]);
	x=st[dep+1],y=st[dep]+Len;
	rep(i,0,n)	Ans[y+i]+=Ans[x+i];
	rep(i,0,2*n)Ans[st[dep]+i]%=mod;
}
void mk(ll n,ll dep){
	if (n<=4){
		st[dep]=0;
		return;
	}
	mk(n>>1,dep+1);
	st[dep]=st[dep+1]+n;
}
int main(){
	freopen("chengfa.in","r",stdin);
	n=read();
	rep(i,0,n)A[i]=read();
	rep(i,0,n)B[i]=read();
	mk(n,0LL);
	MUL(n,0LL);
	For(i,0,2*n-1)printf("%lld ",Ans[st[0]+i]);puts("");
}
/*
a->	[a1	][a2	]
b->	[b1	][b2	]
(a2-a1)*(b1-b2)->-a1*b1-a2*b2+a1*b2+a2*b1
a1*b1	a2*b2
f[i][j]->第i轮在j
f[i][j]->	f[i-1][j-1]*a[j-1]
		 	f[i-1][j+1]*(1-a[j+1])
0..3	4..7
0..3	4..7

(a1-a2)*(b2-b1)->	a1*b2+a2*b1-a1*b1-a2*b2
a1*b1	a1*b2+a2*b1-a1*b1-a2*b2+a1*b1+a2*b2				a2*b2

f[i][j]->	i->j的概率
gauss消元
f[l][r][x]->f[l][r][x-1] 
0..6
4..10
a+len
a
A-(A-B)=B

*/
