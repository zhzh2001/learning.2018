#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define llu unsigned long long
#define ld double
#define llu unsigned long long
#define rep(i,x,y)  for(ll i=x;i<y;++i)
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define pi acos(-1)
#define pa pair<ll,ll>
#define lf else if
#define max(x,y)    ((x)<(y)?(y):(x))
#define min(x,y)    ((x)<(y)?(x):(y))
#define sqr(x)      ((x)*(x))
#define abs(x)      ((x)>0?(x):-(x))
#define Mul(x,y)    ((x)=1LL*(x)*(y)%mod)
#define Add(x,y)    ((x)=((x)+(y))%mod)
#define Max(x,y)    ((x)=((x)<(y)?(y):(x)))
#define Min(x,y)    ((x)=((x)>(y)?(y):(x)))
#define E(x)        return writeln(x),0
#define p(x)        printf("~%lld~\n",x)
#define pp(x,y)     printf("~~%lld %lld~~\n",x,y)
#define ppp(x,y,z)  printf("~~~%lld %lld %lld~~~\n",x,y,z)
#define pppp(a,b,c,d)	printf("~~~%lld %lld %lld %lld\n",a,b,c,d)
#define del(x)		((x)<0?(x)+mod:(x))
#define f_in(x)     freopen(x,"r",stdin)
#define f_out(x)    freopen(x,"w",stdout)
#define open(x)     f_in(x".in"),f_out(x".out")
#define fi first
#define se second
typedef complex<double> E;
namespace SHENZHEBEI{
#define NEG 1
#if NEG
    inline ll read(){    ll x=0,f=1; char ch=getchar();   for (;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1;  for (;isdigit(ch);ch=getchar())  x=x*10-48+ch;   return x*f; }
    inline void write(ll x){    if (x<0) putchar('-'),x=-x;  if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#else
    inline ll read(){    ll x=0; char ch=getchar();   for (;!isdigit(ch);ch=getchar());    for (;isdigit(ch);ch=getchar())  x=x*10-48+ch;   return x;   }
    inline void write(ll x){    if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#endif
    inline char readchar(){ char ch=getchar();   for(;isspace(ch);ch=getchar());  return ch;  }
    inline ll readstr(char *s){ char ch=getchar();   int cur=0;  for(;isspace(ch);ch=getchar());      for(;!isspace(ch);ch=getchar())  s[cur++]=ch;    s[cur]='\0';    return cur; }
    inline void writeln(ll x){  write(x);   puts("");   }
    inline ld getreal(){    static ld lbc;  scanf("%lf",&lbc);  return lbc; }
}using namespace SHENZHEBEI;
const ll N=200010;
ll fac[N],Inv[N],bin[N],f[N],g[N],A[N],B[N],st[100],ans,mod,n,m,NN;
ll Ans[N];
ll ppow(ll x,ll k){ll ans=1;for(;k;k>>=1,Mul(x,x))if(k&1)Mul(ans,x);return ans;}
void MUL(ll *a,ll *b){
	ll tmp=0;
	For(i,0,n)Ans[i]=0;
	For(i,0,n)if (a[i]){
		For(j,i,n)Ans[j]+=a[i]*b[j-i];
		if (++tmp%15==0){For(j,i,n)Ans[j]%=mod;}
	}
	For(i,0,n)Ans[i]%=mod;
}
int main(){
	freopen("bp.in","r",stdin);
	freopen("bp.out","w",stdout);
	m=read();n=read();mod=read();
	fac[0]=1;For(i,1,n)fac[i]=fac[i-1]*i%mod;
	Inv[n]=ppow(fac[n],mod-2);FOr(i,n-1,1)Inv[i]=Inv[i+1]*(i+1)%mod;
	bin[0]=1;For(i,1,n)bin[i]=bin[i-1]*2%mod;
	f[0]=1;
	For(CI,1,m){
		For(i,0,n)	f[i]=f[i]*bin[i]%mod*fac[n-i]%mod;
		MUL(f,Inv);
		For(i,0,n)	f[i]=Ans[i]*(i==n?1:Inv[n-i])%mod;
	}For(i,0,n)Add(ans,f[i]);
	writeln((ans+mod)%mod);
}
/*
|n|*mod^2
*/
