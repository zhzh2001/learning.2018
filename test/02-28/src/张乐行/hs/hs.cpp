#include<cstdio>
#define ll long long
const int N=1e5+5,mo=998244353;
int n,m,i,p[N],v[N],x,y;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int Pow(int x,int n){
	int k=1;
	for (;n;n>>=1,x=(ll)x*x%mo)
		if (n&1) k=(ll)k*x%mo;
	return k;
}
void get(int x){v[x]=(ll)p[x]*(1-p[x+1])%mo;}
void change_it(){
	int x=read(),px=read(),py=read();
	p[x]=(ll)px*Pow(py,mo-2)%mo;
	if (x!=1) get(x-1);get(x);
}
void query_it(){
	int l=read(),r=read(),ans=1;
	for (i=r-1;i>=l;i--)
		ans=Pow((1-(ll)v[i]*ans)%mo,mo-2);
	ans=(ll)ans*(1-p[l])%mo;
	ans=1-ans;
	ans=(ans+mo)%mo;
	write(ans);putchar('\n');
}
int main(){
	freopen("hs.in","r",stdin);
	freopen("hs.out","w",stdout);
	n=read();m=read();
	for (i=1;i<=n;i++){
		int px=read(),py=read();
		p[i]=(ll)px*Pow(py,mo-2)%mo;
	}
	for (i=1;i<=n;i++) get(i);
	for (;m--;) if (read()==1)
		change_it(); else query_it();
}
