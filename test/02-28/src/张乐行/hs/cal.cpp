#include<cstdio>
#define ll long long
const int mo=998244353;
int Pow(int x,int n){
	int k=1;
	for (;n;n>>=1,x=(ll)x*x%mo)
		if (n&1) k=(ll)k*x%mo;
	return k;
}
int main(){
	int x=887328314;
	for (int i=1;;i++){
		int v=(ll)x*i%mo;
		if (v<i){
			printf("%d %d\n",v,i);
			return 0;
		}
	}
}
