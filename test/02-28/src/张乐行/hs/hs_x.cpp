#include<cstdio>
#define ls p<<1,l,m
#define rs p<<1|1,m+1,r
#define ll long long
const int N=1e5+5,P=4*N,mo=998244353;
int n,m,i,p[N],x,y;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int Pow(int x,int n){
	int k=1;
	for (;n;n>>=1,x=(ll)x*x%mo)
		if (n&1) k=(ll)k*x%mo;
	return k;
}
int cal(int k){return (ll)k*Pow(1-k,mo-2)%mo;}
struct tree{int mul,val;}f[P];
tree operator + (tree A,tree B){
	return (tree){(ll)A.mul*B.mul%mo,
		(A.val+(ll)A.mul*B.val)%mo};
}
void up(int p){f[p]=f[p<<1]+f[p<<1|1];}
void get(int px,int x){
	int v=(ll)p[x]*(1-p[x+1])%mo;
	f[px].mul=cal(v);f[px].val=1;
}
void change(int p,int l,int r){
	if (l==r){get(p,l);return;}int m=l+r>>1;
	if (x<=m) change(ls); else change(rs);up(p);
}
tree ask(int p,int l,int r){
	if (x<=l&&r<=y) return f[p];
	int m=l+r>>1;
	if (y<=m) return ask(ls);
	if (x>m) return ask(rs);
	return ask(ls)+ask(rs);
}
void build(int p,int l,int r){
	if (l==r){get(p,l);return;}
	int m=l+r>>1;build(ls);build(rs);up(p);
}
void change_it(){
	int tx=read(),px=read(),py=read();
	p[tx]=(ll)px*Pow(py,mo-2)%mo;
	if (tx!=1) x=tx-1,change(1,1,n);
	x=tx;change(1,1,n);
}
void query_it(){
	x=read();y=read();
	int v=ask(1,1,n).val;
	v=(ll)v*(1-p[x])%mo;v=(1-v)%mo;
	write((v+mo)%mo);putchar('\n');
}
int main(){
	freopen("hs.in","r",stdin);
	freopen("hs.out","w",stdout);
	n=read();m=read();
	for (i=1;i<=n;i++){
		int px=read(),py=read();
		p[i]=(ll)px*Pow(py,mo-2)%mo;
	}
	build(1,1,n);
	for (;m--;) if (read()==1)
		change_it(); else query_it();
}
