#include<cstdio>
#define ll long long
const int N=3e4+5;
int n,m,mo,pmo,i,num,f[N],g[N],ans;
int Pow(int x,int n){
	int k=1;
	for (;n;n>>=1,x=(ll)x*x%mo)
		if (n&1) k=(ll)k*x%mo;
	return k;
}
int fact[N],fv[N];
int C(int n,int m){return (ll)fact[n]*fv[m]%mo*fv[n-m]%mo;}
void init(int n){
	for (fact[0]=1,i=1;i<=n;i++)
		fact[i]=(ll)fact[i-1]*i%mo;
	for (fv[n]=Pow(fact[n],mo-2),i=n;i;i--)
		fv[i-1]=(ll)fv[i]*i%mo;
}
int fa[N],fb[N];
void Mul(int nx,int ny,int *a,int *b){
	int i,j;
	for (i=0;i<=n;i++){
		fa[i]=(ll)a[i]*Pow(2,(ll)i*ny%pmo)%mo*fv[i]%mo;
		fb[i]=(ll)b[i]*fv[i]%mo;
	}
	for (i=0;i<=n;i++) a[i]=0;
	for (i=0;i<=n;i++) for (j=0;i+j<=n;j++)
		a[i+j]=(a[i+j]+(ll)fa[i]*fb[j])%mo;
	for (i=0;i<=n;i++) a[i]=(ll)a[i]*fact[i]%mo;
}
int main(){
	freopen("bp.in","r",stdin);
	freopen("bl.out","w",stdout);
	scanf("%d%d%d",&m,&n,&mo);pmo=mo-1;init(n);
	for (f[0]=1,i=1;i<=n;i++) g[i]=1;
	for (i=0;(1<<i)<=m;Mul(1<<i,1<<i,g,g),i++)
		if ((m>>i)&1) Mul(num,1<<i,f,g),num+=1<<i;
	for (i=m;i<=n;i++) ans=(ans+(ll)f[i]*C(n,i))%mo;
	printf("%d",ans);
}
