#include <fstream>
#include <algorithm>
#include <cmath>
using namespace std;
ifstream fin("hs.in");
ofstream fout("hs.out");
const int N = 100005, P = 998244353;
const double eps = 1e-15;
int x[N], y[N];
long long qpow(long long a, int b)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % P;
		a = a * a % P;
	} while (b /= 2);
	return ans;
}
double ans;
void dfs(int k, double p, int l, int r)
{
	if (p < eps)
		return;
	if (k > r)
	{
		ans += p;
		return;
	}
	dfs(k + 1, p * 1. * x[k] / y[k], l, r);
	if (k - 1 >= l)
		dfs(k - 1, p * (1. - 1. * x[k] / y[k]), l, r);
}
int main()
{
	int n, m;
	fin >> n >> m;
	if (n == 1)
	{
		int x, y;
		fin >> x >> y;
		while (m--)
		{
			int opt;
			fin >> opt;
			if (opt == 1)
			{
				int d;
				fin >> d >> x >> y;
			}
			else
			{
				int l, r;
				fin >> l >> r;
				fout << x * qpow(y, P - 2) % P << endl;
			}
		}
	}
	else
	{
		for (int i = 1; i <= n; i++)
			fin >> x[i] >> y[i];
		while (m--)
		{
			int opt;
			fin >> opt;
			if (opt == 1)
			{
				int d, nx, ny;
				fin >> d >> nx >> ny;
				x[d] = nx;
				y[d] = ny;
			}
			else
			{
				int l, r;
				fin >> l >> r;
				ans = .0;
				dfs(l, 1., l, r);
				fout << ans << endl;
			}
		}
	}
	return 0;
}