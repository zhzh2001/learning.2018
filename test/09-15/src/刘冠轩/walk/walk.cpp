#include <bits/stdc++.h>
using namespace std;
const int N=5005;
int n,a[N],b[N],dp[N],c,k;
bitset<1001> B[1001];
void print(int x,int y){
	if(x==1&&y==1)return;
	if(B[x][y])print(x,y-1),putchar('R');
		else print(x-1,y),putchar('D');
}
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	scanf("%d%d",&n,&c);
	if(n>1000){
		for(int i=1;i<n;i++)printf("RD");
		return 0;
	}
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	for(int i=1;i<=n;i++)scanf("%d",&b[i]);
	for(int i=1;i<=n;i++){
		B[1][i]=1;
		dp[i]=dp[i-1]+a[1]*b[i]%c;
	}
	for(int i=2;i<=n;i++){
		dp[1]=dp[1]+a[i]*b[1]%c;
		for(int j=2;j<=n;j++)
			if(dp[j-1]>dp[j]){
				B[i][j]=1;
				dp[j]=dp[j-1]+a[i]*b[j]%c;
			}else dp[j]=dp[j]+a[i]*b[j]%c;
	}
	print(n,n);
	return 0;
}
