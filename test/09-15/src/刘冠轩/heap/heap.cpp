#include <bits/stdc++.h>
#define int long long
using namespace std;
const int N=1005;
int n,u,y,vis[N],use[N],ans;
void dfs(int x){
	if(x==u){
		if(vis[x/2]>y)return;
		dfs(x+1);return;
	}
	if(x>n){ans++;return;}
	for(int i=vis[x/2]+1;i<=n;i++)
	if(!use[i]){
		vis[x]=i;
		use[i]=1;
		dfs(x+1);
		use[i]=0;
	}
}
signed main(){
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout); 
	scanf("%d%d%d",&n,&u,&y);
	vis[u]=y;
	use[y]=1;
	dfs(1);
	cout<<ans%998244353;
	return 0;
}
