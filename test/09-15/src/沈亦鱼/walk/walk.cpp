#include<cstdio>
using namespace std;
int n,m,a[110],b[110],f[110][110];
char s[110][110];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void dfs(int i,int j){
	if(i+j==2)return;
	if(s[i][j]=='D')dfs(i-1,j);
	else dfs(i,j-1);
	putchar(s[i][j]);
}
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<=n;i++)
		b[i]=read();
	f[1][1]=a[1]*b[1]%m;
	for(int i=2;i<=n;i++){
		f[1][i]=f[1][i-1]+a[1]*b[i]%m;
		s[1][i]='R';
		f[i][1]=f[i-1][1]+a[i]*b[1]%m;
		s[i][1]='D';
	}
	for(int i=2;i<=n;i++)
		for(int j=2;j<=n;j++)
			if(f[i-1][j]>f[i][j-1]){
				f[i][j]=f[i-1][j]+a[i]*b[j]%m;
				s[i][j]='D';
			}
			else{
				f[i][j]=f[i][j-1]+a[i]*b[j]%m;
				s[i][j]='R';
			}
	dfs(n,n);
	return 0;
}
