#include<cstdio>
#include<algorithm>
using namespace std;
int mo=998244353,n;
long long x,y,sz[1100],sc[1100],c[1100][1100],f[1100][1100];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void dfs(int k){
	sz[k]=1;
	sc[k]=1;
	if(k*2<=n){
		dfs(k*2);
		sz[k]+=sz[k*2];
		sc[k]=sc[k]*sc[k*2]%mo*c[sz[k]-1][sz[k*2]]%mo;
	}
	if(k*2<n){
		dfs(k*2+1);
		sz[k]+=sz[k*2+1];
		sc[k]=sc[k]*sc[k*2+1]%mo*c[sz[k]-1][sz[k*2+1]]%mo;
	}
}
int main(){
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	n=read();
	for(int i=0;i<=n;i++)
		c[i][0]=1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=i;j++)
			c[i][j]=(c[i-1][j]+c[i-1][j-1])%mo;
	dfs(1);
	x=read();
	y=read();
	if(x==y&&x==1){
		printf("%lld\n",sc[1]);
		return 0;
	}
	f[x][0]=sc[x]%mo;
	for(int i=x,j=0;i>1;i>>=1,j++)
		for(int k=0;k<=min(y-1,sz[i]-sz[x]);k++)
			for(int kk=k+1;kk<=min(y-1,sz[i>>1]-sz[x]);kk++)
				f[i>>1][kk]=(f[i>>1][kk]+f[i][k]*sc[i^1]%mo*c[kk-1][k]%mo*c[sz[i>>1]-kk-1][sz[i]-k-1]%mo)%mo;
//	for(int i=1;i<=n;i++){
//		for(int j=0;j<=n;j++)
//			printf("%lld ",f[i][j]);
//		puts("");
//	}
	printf("%lld\n",f[1][y-1]);
	return 0;
}
