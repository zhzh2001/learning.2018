#include<cstdio>
#include<iostream>
using namespace std;
const int MAXN=1000005;
const int P=998244353;
int n,x,y,ans,a[MAXN];
bool used[MAXN];
void dfs(int k){
	if(k>n){
		ans=(ans+1)%P;
		return;
	}
	if(k==x){
		if(a[k]<a[k>>1])return;
		dfs(k+1);
		return;
	}
	for(int i=1;i<=n;i++)
		if(!used[i]&&i>a[k>>1]){
			used[i]=1;
			a[k]=i;
			dfs(k+1);
			used[i]=0;
		}
}
int main(){
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	scanf("%d%d%d",&n,&x,&y);
	a[x]=y;used[y]=1;
	dfs(1);
	printf("%d",ans);
}
