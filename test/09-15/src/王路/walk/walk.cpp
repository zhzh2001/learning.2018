#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <bitset>
using namespace std;

const int kMaxN = 1005, kInf = 0x3f3f3f3f;
int n, c, f[2][kMaxN], a[kMaxN], b[kMaxN], cnt;
bitset<kMaxN> bs[kMaxN]; // 1->R, 0->D
char buf[kMaxN << 1];

int main() {
	freopen("walk.in", "r", stdin);
	freopen("walk.out", "w", stdout);
	scanf("%d%d", &n, &c);
	for (int i = 1; i <= n; ++i) {
		scanf("%d", a + i);
	}
	for (int i = 1; i <= n; ++i) {
		scanf("%d", b + i);
	}
	for (int i = 1; i <= n; ++i) {
		int now = i & 1;
		for (int j = 0; j < kMaxN; ++j)
			f[now][j] = -kInf;
		for (int j = 1; j <= i; ++j) {
			int x = j, y = i - j + 1;
			// fprintf(stderr, "%d %d\n", x, y);
			if (f[now ^ 1][j] > f[now ^ 1][j - 1]) {
				f[now][j] = f[now ^ 1][j] + (a[x] * b[y]) % c;
				bs[x][y] = 1;
			} else {
				f[now][j] = f[now ^ 1][j - 1] + (a[x] * b[y]) % c;
				bs[x][y] = 0;
			}
		}
	}
	// fprintf(stderr, "%s\n", "-----");
	for (int i = n + 1; i < n * 2; ++i) {
		int now = i & 1;
		for (int j = 0; j < kMaxN; ++j)
			f[now][j] = -kInf;
		for (int j = i - n + 1; j <= n; ++j) {
			int x = j, y = i - j + 1;
			// fprintf(stderr, "%d %d\n", x, y);
			if (f[now ^ 1][j] > f[now ^ 1][j - 1]) {
				f[now][j] = f[now ^ 1][j] + (a[x] * b[y]) % c;
				bs[x][y] = 1;
			} else {
				f[now][j] = f[now ^ 1][j - 1] + (a[x] * b[y]) % c;
				bs[x][y] = 0;
			}
		}
	}
	for (int x = n, y = n; !(x == 1 && y == 1) && (x > 0 && y > 0); ) {
		if (bs[x][y]) {
			buf[++cnt] = 'R';
			--y;
		} else {
			buf[++cnt] = 'D';
			--x;
		}
	}
	reverse(buf + 1, buf + cnt + 1);
	puts(buf + 1);
	return 0;
}