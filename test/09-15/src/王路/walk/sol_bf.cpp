#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

static const int kMaxN = 5005;
int n, c, a[kMaxN], b[kMaxN];

namespace SubTask1 {
static const int kMaxN = 105;
int f[kMaxN][kMaxN], g[kMaxN][kMaxN];
int Main() {
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= n; ++j) {
      if (f[i - 1][j] > f[i][j - 1]) {
        f[i][j] = f[i - 1][j] + (a[i] * b[j]) % c;
        g[i][j] = 2; // D
      } else {
        f[i][j] = f[i][j - 1] + (a[i] * b[j]) % c;
        g[i][j] = 1; // R
      }
    }
  char buf[kMaxN * 2];
  int cnt;
  for (int x = n, y = n; !(x == 1 && y == 1); ) {
    if (g[x][y] == 1) {
      buf[++cnt] = 'R';
      y = y - 1;
    } else if (g[x][y] == 2) {
      buf[++cnt] = 'D';
      x = x - 1;
    }
  }
  reverse(buf + 1, buf + cnt + 1);
  puts(buf + 1);
  return 0;
}
}

namespace Solution {
static const int kMaxN = 5005;
int Main() {
  return 0;
}
} // namespace Solution

int main() {
  freopen("walk.in", "r", stdin);
  freopen("walk.out", "w", stdout);
  scanf("%d%d", &n, &c);
  for (int i = 1; i <= n; ++i)
    scanf("%d", a + i);
  for (int i = 1; i <= n; ++i)
    scanf("%d", b + i);
  if (n <= 100)
    return SubTask1::Main(); // 40pts
  return Solution::Main();
}