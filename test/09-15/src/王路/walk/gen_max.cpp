#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <Windows.h>
using namespace std;

inline int Rand32() { return (rand() << 15) + rand(); }
inline int Uniform(int l, int r) { return Rand32() % (r - l + 1) + l; }

int main() {
	freopen("walk.in", "w", stdout);
	srand(GetTickCount());
	int n = 1000, c = 1e5;
	printf("%d %d\n", n, c);
	for (int i = 1; i <= n; ++i)
		printf("%d%c", Uniform(1, 10000), " \n"[i == n]);
	for (int i = 1; i <= n; ++i)
		printf("%d%c", Uniform(1, 10000), " \n"[i == n]);
	return 0;
}