#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int kMod = 998244353;

int n, x, y;

namespace SubTask1 {
int b[20];
int Main() {
  for (int i = 1; i <= n; ++i)
    b[i] = i;
  int ans = 0;
  do {
    if (b[x] == y) {
      bool fl = true;
      for (int i = 1; i <= n; ++i) {
        for (int j = i; j > 1; j /= 2)
          if (b[j] < b[j / 2]) {
            fl = false;
            break;
          }
        if (!fl)
          break;
      }
      ans += fl;
    }
  } while (next_permutation(b + 1, b + n + 1));
  printf("%d\n", ans);
  return 0;
}
}

namespace SubTask2 {
int Main() {

	return 0;
}
}

int main() {
  freopen("heap.in", "r", stdin);
  freopen("heap.out", "w", stdout);
  scanf("%d%d%d", &n, &x, &y);
  if (n <= 10)
    return SubTask1::Main(); // 21pts
  return SubTask2::Main();
}