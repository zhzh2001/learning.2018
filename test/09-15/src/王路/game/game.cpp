#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline void Init() {
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);
}
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<int, int> pii;

namespace SubTask2 {
const int kMaxN = 20;
int T, n;
void SSubTask1() {
	puts(n == 1 ? "cqz" : "ysgh");
}
int Main() {
	Read(T);
	while (T--) {
		Read(n);
		if (n <= 20) // (60 * 0.9 = 54) pts
			SSubTask1();
		else
			puts("ysgh"); // fake
	}
	// for (int i = 1; i <= n; ++i) {
	// 	vector<bool> val(20, false);
	// 	printf("%s\n", Solve(1, val) ? "ysgh" : "cqz");
	// 	fflush(stdout);
	// }
	return 0;
}
} // namespace SubTask2

int op;

int main() {
  IO::Init();
	Read(op);
	if (op == 2)
		return SubTask2::Main();
	return 0;
}