#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline void Init() {
  freopen("bf.in", "r", stdin);
  freopen("bf.out", "w", stdout);
}
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<int, int> pii;

namespace SubTask2 {
const int kMaxN = 20;
int T, n;

bool Solve(bool c, int val) {
	if (val == ((1 << n) - 1))
		return true;
	for (int i = n; i >= 1; --i) {
		if ((val >> (i - 1)) & 1)
			continue;
		int nxt = val;
		for (int j = i; j; j /= 2) {
			nxt |= (1 << (j - 1));
		}
		bool res = Solve(c ^ 1, nxt);
		if (!res) {
			return true;
		}
	}
	return false;
}

int Main() {
	Read(T);
	for (int i = 7; i <= 7; ++i) {
		int val = 0;
		n = i;
		printf("%s\n", Solve(1, val) ? "ysgh" : "cqz");
		fflush(stdout);
	}
	return 0;
}
} // namespace SubTask2

int main() {
  IO::Init();
	return SubTask2::Main();
}