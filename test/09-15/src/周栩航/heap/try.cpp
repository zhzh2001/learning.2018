#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back
#define mo (998244353)


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
const int N=41;
int n,xx,yy,vis[N],qr[N],ql[N],sl,sr,a[N];
ll ans;
ll biao[]={0,1,1,2,3,8,20,80,210,896,3360,19200};
inline void Dfs(int x,int mx,int to[])
{
	if(x==mx+1)	{/*For(i,1,mx)	cout<<a[to[i]]<<' ';cout<<endl;*/ans++;return;}
	if(a[to[x]])	{Dfs(x+1,mx,to);return;}
	For(i,a[to[x]/2]+1,n)
	{
		if(xx/2==to[x])	if(i>yy)	continue;
		if(!vis[i])
		{
			a[to[x]]=i;vis[i]=1;
			Dfs(x+1,mx,to);
			a[to[x]]=0;vis[i]=0;
		}	
	}		
}
int main()
{
	n=read();xx=read();yy=read();
	a[xx]=yy;vis[yy]=1;vis[1]=1;
	int p=0;
	For(i,2,n)
	{
		int t=i;
		while(t>3)	t/=2;
		if(t==3)	qr[++sr]=i;else	ql[++sl]=i;
		if(i==xx)	if(t==3)	p=1;else	p=0;
	}
	ans=0;
	if(p==1)	Dfs(1,sr,qr);else	Dfs(1,sl,ql);
	int left=n-1-(p==1?sr:sl);
	cout<<sl<<' '<<sr<<endl;
	cout<<ans<<' '<<biao[left]<<endl;
	ans=1LL*ans*biao[left]%mo;
	writeln(ans);
}
