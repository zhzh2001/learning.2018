#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back
#define mo (998244353)
#define int ll


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
const int N=41;
int n,xx,yy,vis[N],qr[N],ql[N],sl,sr,a[N];
ll ans,Ans[N];
ll biao[]={0,1,1,2,3,8,20,80,210,896,3360,19200,79200,506880,2745600,21964800};
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2,x=x*x%mo)	if(y&1)	sum=sum*x%mo;	return sum;}
inline ll C(int n,int m)
{
	ll ret=1;
	For(i,n-m+1,n)	ret=ret*i%mo;
	For(i,1,m)	ret=ret*ksm(i,mo-2)%mo;
	return ret;
}
inline void Dfs(int x,int mx,int to[],int num)
{
	if(x==mx+1)	{/*For(i,1,mx)	cout<<a[to[i]]<<' ';cout<<endl;*/Ans[num]++;return;}
	int tmp=0;
	int t=to[x];
	while(t<=mx)	t*=2,tmp++;
	For(i,a[to[x]/2]+1,mx-tmp)
	{	
		if(!vis[i])
		{
			a[to[x]]=i;vis[i]=1;
			if(to[x]==xx)	Dfs(x+1,mx,to,i);
			else	Dfs(x+1,mx,to,num);
			a[to[x]]=0;vis[i]=0;
		}	
	}		
}
signed main()
{
	n=read();xx=read();yy=read();
	int p=0;
	For(i,2,n)
	{
		int t=i;
		while(t>3)	t/=2;
		if(t==3)	qr[++sr]=i;else	ql[++sl]=i;
		if(i==xx)	if(t==3)	p=1;else	p=0;
	}
	ans=0;
	if(p==1)	Dfs(1,sr,qr,-1);else	Dfs(1,sl,ql,-1);
	int mx=(p==1?sr:sl);
	if(xx==1)	if(yy!=1)	{puts("0");return 0;}else	ans=biao[mx]*C(n-1,mx)%mo;
	if(xx!=1)For(i,1,mx)	ans+=Ans[i]*C(yy-1-1,i-1)%mo*C(n-yy,mx-i)%mo,ans%=mo;//cout<<Ans[i]<<",,,"<<endl;
	int left=n-1-(p==1?sr:sl);
	ans=1LL*ans*biao[left]%mo;
	writeln(ans);
}
