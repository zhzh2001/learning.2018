#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=300005;
int n,end[N],dp[N],mx;
ll mo=998244353;
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2,x=x*x%mo)	if(y&1)	sum=sum*x%mo;	return sum;}
char a[200001],b[200001];
int main()
{
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	int op=read();
	if(op==2)
	{
		int T=read();
		while(T--)
		{
			n=read();
			if(n==1)	puts("cqz");else	puts("ysgh");
		}
	}
	else
	{
		scanf("%s%s",a+1,b+1);
		int len1=strlen(a+1);
		ll t1=0,t2=0;
		For(i,1,len1)
		{
			t1=t1*10+a[i]-'0';t1%=(mo-1);
			t2=t2*10+a[i]-'0';t2%=mo;
		}
		ll ans=((ksm(2,t1)-2*t2)%mo+mo)%mo*ksm(ksm(2,t1),mo-2)%mo;
		writeln(ans);
		//(2^n-2*n)/2^n
	}
}

