#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<cstdio>
#include<bitset>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back
#define int unsigned int 


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/
struct node{int x,y,val;bitset<3005> ans;}	zt[601],tmp[1201];
const int N=5005;
int n,m,a[N],b[N],top,c;
int vis[N],tim;
inline bool cmp(node x,node y){return x.val>y.val;}
inline void Yc(int step)
{
	if(step>2*(n-1))
	{
		For(i,1,step-1)	putchar(zt[1].ans[i]==1?'R':'D');
		return;
	}
	int n_top=0;
	For(i,1,top)
	{
		if(zt[i].x!=n)	
		{
			tmp[++n_top].x=zt[i].x+1;tmp[n_top].y=zt[i].y;tmp[n_top].val=zt[i].val+a[zt[i].x+1]*b[zt[i].y]%c;
			tmp[n_top].ans=zt[i].ans;
			tmp[n_top].ans[step]=0;
		}
		if(zt[i].y!=n)
		{
			tmp[++n_top].x=zt[i].x;tmp[n_top].y=zt[i].y+1;tmp[n_top].val=zt[i].val+a[zt[i].x]*b[zt[i].y+1]%c;
			tmp[n_top].ans=zt[i].ans;
			tmp[n_top].ans[step]=1;
		}
	}
	sort(tmp+1,tmp+n_top+1,cmp);
	int now=0;
	++tim;
	For(i,1,n_top)
	{
		if(vis[tmp[i].x]==tim)	continue;
		zt[++now]=tmp[i];
		vis[tmp[i].x]=tim;
		if(now==600)	break;
	}
	top=now;
	Yc(step+1);
}
signed main()
{
	freopen("walk.in","r",stdin);freopen("walk.out","w",stdout);
	n=read();c=read();
	For(i,1,n)	a[i]=read();
	For(i,1,n)	b[i]=read();
	zt[1].x=1;zt[1].y=1;zt[1].val=a[1]*b[1]%c;
	top=1;
	Yc(1);
}
/*
3 4
1 2 3
1 2 3
*/
