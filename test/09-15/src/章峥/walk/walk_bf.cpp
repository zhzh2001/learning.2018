#include<fstream>
#include<algorithm>
#include<string>
using namespace std;
ifstream fin("walk.in");
ofstream fout("walk.ans");
const int N=10005;
int a[N],b[N];
long long f[N][N];
bool path[N][N];
int main()
{
	int n,mod;
	fin>>n>>mod;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(int i=1;i<=n;i++)
		fin>>b[i];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(f[i][j-1]>f[i-1][j])
			{
				f[i][j]=f[i][j-1]+a[i]*b[j]%mod;
				path[i][j]=true;
			}
			else
				f[i][j]=f[i-1][j]+a[i]*b[j]%mod;
	fout<<f[n][n]<<endl;
	string ans;
	for(int x=n,y=n;x!=1||y!=1;)
		if(path[x][y])
		{
			ans='R'+ans;
			y--;
		}
		else
		{
			ans='D'+ans;
			x--;
		}
	fout<<ans<<endl;
	return 0;
}
