#include<cstdio>
using namespace std;
const int N=20008,M=100;
int a[N],b[N];
long long f[N],g[N];
char path[M][N/8],ans[N*2];
int main()
{
	FILE *fin=fopen("walk.in","r");
	FILE *fout=fopen("walk.out","wb");
	int n,mod;
	fscanf(fin,"%d%d",&n,&mod);
	for(int i=1;i<=n;i++)
		fscanf(fin,"%d",a+i);
	for(int i=1;i<=n;i++)
		fscanf(fin,"%d",b+i);
	for(int i=1;i<=n;i++)
	{
		f[i]=f[i-1]+a[1]*b[i]%mod;
		path[0][i/8]|=1<<(i%8);
	}
	int offset=1;
	for(int i=2;i<=n;i++)
	{
		for(int j=0;j<N/8;j++)
			path[offset][j]=0;
		for(int j=1;j<=n;j++)
			if(g[j-1]>f[j])
			{
				g[j]=g[j-1]+a[i]*b[j]%mod;
				path[offset][j/8]|=1<<(j%8);
			}
			else
				g[j]=f[j]+a[i]*b[j]%mod;
		for(int j=1;j<=n;j++)
			f[j]=g[j];
		if(++offset==M)
		{
			fwrite(path,1,sizeof(path),fout);
			offset=0;
		}
	}
	if(offset)
		fwrite(path,1,offset*sizeof(path[0]),fout);
	fclose(fout);
	fout=fopen("walk.out","rb");
	if(n<M)
	{
		fseek(fout,0,0);
		fread(path[M-n],1,n*sizeof(path[0]),fout);
	}
	else
	{
		fseek(fout,n*(N/8)-M*(N/8),0);
		fread(path[0],1,sizeof(path),fout);
	}
	int cc=n*2-2;
	offset=M-1;
	for(int x=n,y=n;x!=1||y!=1;)
	{
		if(path[offset][y/8]&(1<<(y%8)))
		{
			ans[--cc]='R';
			y--;
		}
		else
		{
			ans[--cc]='D';
			x--;
			if(--offset<0)
			{
				if(x<M)
				{
					fseek(fout,0,0);
					fread(path[M-x],1,x*sizeof(path[0]),fout);
				}
				else
				{
					fseek(fout,x*(N/8)-M*(N/8),0);
					fread(path[0],1,sizeof(path),fout);
				}
				offset=M-1;
			}
		}
	}
	fclose(fout);
	fout=fopen("walk.out","w");
	fprintf(fout,"%s\n",ans);
	fclose(fout);
	fclose(fin);
	return 0;
}
