#include<fstream>
using namespace std;
ifstream fin("heap.in");
ofstream fout("heap.out");
const int N=1000005,MOD=998244353,LOGN=25;
int fact[N],inv[N],sz[N],f[N],ssz[LOGN],dp[LOGN][N/10+5];
int qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}while(b/=2);
	return ans;
}
inline int c(int n,int m)
{
	if(m>n)
		return 0;
	return 1ll*fact[n]*inv[n-m]%MOD*inv[m]%MOD;
}
int main()
{
	int n,x,y;
	fin>>n>>x>>y;
	fact[0]=1;
	for(int i=1;i<=n;i++)
		fact[i]=1ll*fact[i-1]*i%MOD;
	inv[n]=qpow(fact[n],MOD-2);
	for(int i=n-1;i>=0;i--)
		inv[i]=1ll*inv[i+1]*(i+1)%MOD;
	for(int i=n;i;i--)
		if(i*2+1<=n)
		{
			f[i]=1ll*c(sz[i*2]+sz[i*2+1],sz[i*2])*f[i*2]%MOD*f[i*2+1]%MOD;
			sz[i]=sz[i*2]+sz[i*2+1]+1;
		}
		else if(i*2<=n)
		{
			f[i]=f[i*2];
			sz[i]=sz[i*2]+1;
		}
		else
			f[i]=sz[i]=1;
	int sn=0,ans=1;
	for(int i=x,pred;i;i/=2)
	{
		if(i<x)
		{
			ans=1ll*ans*f[i*2+!pred]%MOD;
			ssz[++sn]=sz[i]-sz[i*2+pred];
		}
		else
		{
			ans=1ll*ans*f[i]%MOD;
			ssz[++sn]=sz[i];
		}
		pred=i&1;
	}
	for(int i=1;i<=y;i++)
		dp[1][i]=c(n-y,ssz[1]-1);
	for(int i=2;i<=sn;i++)
	{
		for(int j=y-1;j;j--)
			dp[i][j]=(dp[i][j+1]+1ll*dp[i-1][j+1]*c(n-j-ssz[i-1],ssz[i]-1))%MOD;
		ssz[i]+=ssz[i-1];
	}
	fout<<1ll*ans*dp[sn][1]%MOD<<endl;
	return 0;
}
