#include<fstream>
#include<algorithm>
#include<string>
using namespace std;
ifstream fin("game.in");
ofstream fout("game.out");
namespace subtask1
{
const int MOD=998244353;
int qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}while(b/=2);
	return ans;
}
void main()
{
	string n,k;
	fin>>n>>k;
	if(n=="1"||n=="2")
		fout<<0<<endl;
	else if(n=="3")
		fout<<748683265<<endl;
	else
	{
		int p=0;
		for(int i=0;i<n.length();i++)
			p=(p*10ll+n[i]-'0')%(MOD-1);
		int tmp=qpow(2,p);
		p=0;
		for(int i=0;i<n.length();i++)
			p=(p*10ll+n[i]-'0')%MOD;
		fout<<1ll*(tmp-p*2+MOD*2ll)*qpow(tmp,MOD-2)%MOD<<endl;
	}
}
}
namespace subtask2
{
void main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int n;
		fin>>n;
		/*
		if(n<=18)
		{
			sg[0]=true;
			for(int i=1;i<(1<<n);i++)
			{
				sg[i]=false;
				for(int j=0;j<n;j++)
					if(i&(1<<j))
					{
						int mask=i;
						for(int k=j+1;k;k/=2)
							mask&=(1<<n)-1-(1<<(k-1));
						if(!sg[mask])
							sg[i]=true;
					}
			}
			if(sg[(1<<n)-1])
				fout<<"ysgh\n";
			else
				fout<<"cqz\n";
		}
		else
			fout<<"ysgh\n";
		*/
		if(n==1)
			fout<<"cqz\n";
		else
			fout<<"ysgh\n";
	}
}
}
int main()
{
	int opt;
	fin>>opt;
	if(opt==1)
		subtask1::main();
	else
		subtask2::main();
	return 0;
}
