#include<iostream>
using namespace std;
const int MOD=998244353;
int qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}while(b/=2);
	return ans;
}
int main()
{
	for(int i=1;i<=10000;i++)
		for(int j=i;j<=10000;j++)
			if(1ll*i*qpow(j,MOD-2)%MOD==3168257)
				cout<<i<<'/'<<j<<endl;
	return 0;
}
