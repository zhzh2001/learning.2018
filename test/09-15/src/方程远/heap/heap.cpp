#include<bits/stdc++.h>
using namespace std;
int n,place,num,ans;
int a[101],flag[101];
bool pd(int x,int y){
	if(x*2>y)
		return 1;
	if(x*2<=y&&a[x]>a[x*2])
		return 0;
	if(x*2+1>y)
		return 1;
	if(x*2+1<=y&&a[x]>a[x*2+1])
		return 0;
	return pd(x*2,y)&&pd(x*2+1,y);
}
void search(int x){
	if(x>n){
		ans++;
		ans%=998244353;
		return;
	}
	for(int i=1;i<=n;i++){
		if(!flag[i]){
			a[x]=i;
			if(x>place)
				if(a[place]!=num)
					return;
			flag[i]=1;
			if(pd(1,x))
				search(x+1);
			flag[i]=0;
		}
	}
}
int main(){
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	cin>>n>>place>>num;
	search(1);
	cout<<ans;
	return 0;
}
