#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0)x=-x,putchar('-');if(x>9)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
const ll mod=998244353;
inline ll f(ll a,ll b){
	ll ans=1;
	for(;b;b>>=1){
		if(b&1)ans=(ll)ans*a%mod;
		a=(ll)a*a%mod;
	}
	return ans%mod;
}
ll op,T,n;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	op=read();
	if(op==1){
		char ch[100005]={};
		ll po[15]={},m=1,d=1,t=0;
		po[0]=1;
		for(int i=1;i<=10;i++)
			po[i]=po[i-1]*2;
		cin>>(ch+1);
		for(int i=strlen(ch+1);i>=1;i--){
			m=m*f(2,d*(ch[i]-'0'))%mod;
			d=d*10%mod;
		}
		for(int i=1;i<=strlen(ch+1);i++){
			t=(t*10+(ch[i]-'0'))%mod;
		}
		m=m*f(2,mod-2)%mod;
		cout<<(((m-t+mod)%mod)*f(m,mod-2))%mod<<endl;
		return 0;
	}else{
		cin>>T;
		while(T--){
			cin>>n;
			if(n==1)cout<<"cqz"<<endl;
			else cout<<"ysgh"<<endl;
		}
	}
	return 0;
}
