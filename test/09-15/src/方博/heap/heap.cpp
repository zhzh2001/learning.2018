#include<bits/stdc++.h>
#define ll long long
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0)x=-x,putchar('-');if(x>9)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
using namespace std;
const int N=2005;
int vis[N];
int a[N];
int n,ans;
bool check()
{
	for(int i=n;i;i--)
		if(a[i]<=a[i/2])return false;
//	for(int i=1;i<=n;i++)
//		cout<<a[i]<<' ';
//	cout<<endl;
	return true;
}
void dfs(int k)
{
	if(a[k]!=0){
		dfs(k+1);
		return;
	}
	else if(k>n){
		if(check())ans++;
		return;
	}
	for(int i=a[k/2]+1;i<=n;i++)
		if(!vis[i]){
			a[k]=i;
			vis[i]=true;
			dfs(k+1);
			vis[i]=false;
			a[k]=0;
		}
}
int main()
{
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	a[0]=0;
	int t;
	n=read();t=read();a[t]=read();
	vis[a[t]]=true;
	dfs(1);
	writeln(ans);
}
