#include <bits/stdc++.h>
using namespace std;
const int maxn=1002;
bitset<maxn> qwq[maxn];
int ff[maxn],gg[maxn],*f=ff,*g=gg;
int a[maxn],b[maxn];
bool stk[maxn];
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	int n,mod;
	scanf("%d%d",&n,&mod);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	for(int i=1;i<=n;i++)
		scanf("%d",b+i);
	f[1]=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if (i==1 && j==1)
				continue;
			int val=a[i]*b[j]%mod;
			int x=f[j]+val,y=f[j-1]+val;
			if (i==1)
				x=-1;
			if (j==1)
				y=-1;
			if (x>y)
				qwq[i][j]=1,f[j]=x;
			else f[j]=y;
		}
	int x=n,y=n;
	int tp=0;
	while(x!=1 || y!=1){
		stk[++tp]=qwq[x][y];
		if (qwq[x][y])
			x--;
		else y--;
	}
	for(int i=tp;i;i--)
		putchar(stk[i]?'D':'R');
}