#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=1000002;
const int mod=998244353;
int sz[maxn];
ll dp[1002][1002],f[maxn];
bool par[maxn],sub[maxn];
ll fac[maxn],invf[maxn];
ll power(ll x,ll y){
	ll ans=1;
	while(y){
		if (y&1)
			ans=ans*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return ans;
}
ll c(int n,int m){
	if (n<m || m<0)
		return 0;
	return fac[n]*invf[m]%mod*invf[n-m]%mod;
}
int main(){
	init();
	int n=readint(),x=readint(),y=readint();
	for(int i=n;i;i--){
		sz[i]++;
		sz[i/2]+=sz[i];
		if (i==x)
			par[i]=1;
		par[i/2]|=par[i];
	}
	sub[x]=1;
	for(int i=x;i<=n;i++){
		if (i*2<=n)
			sub[i*2]=sub[i];
		if (i*2+1<=n)
			sub[i*2+1]=sub[i];
	}
	fac[0]=1;
	for(int i=1;i<=n;i++)
		fac[i]=fac[i-1]*i%mod;
	invf[n]=power(fac[n],mod-2);
	for(int i=n;i;i--)
		invf[i-1]=invf[i]*i%mod;
	for(int i=n;i;i--){
		if (i*2>=n){
			f[i]=1;
			continue;
		}
		f[i]=f[i*2]*f[i*2+1]%mod*c(sz[i]-1,sz[i*2])%mod;
		// fprintf(stderr,"f[%d]=%lld\n",i,f[i]);
	}
	if (x==1 && y==1)
		return printf("%lld",f[1]),0;
	for(int i=n;i;i--){ //dp[i][j] sz[i] j numbers<y
		if (sub[i]){
			dp[i][0]=f[i];
			continue;
		}
		if (!par[i]){
			for(int j=0;j<=sz[i];j++)
				dp[i][j]=f[i];
			continue;
		}
		if (i*2==x && x==n){
			dp[i][1]=1;
			continue;
		}
		for(int j=1;j<=sz[i];j++){
			//sz[i]-1-j >y
			for(int k=0;k<j;k++){ //left k<y
				//dp[i*2][k] dp[i*2+1][j-k]
				if (par[i*2]) //x in left
					dp[i][j]=(dp[i][j]+dp[i*2][k]*dp[i*2+1][j-1-k]%mod*c(j-1,k)%mod*c(sz[i]-1-j,sz[i*2]-1-k))%mod;
				else dp[i][j]=(dp[i][j]+dp[i*2][k]*dp[i*2+1][j-1-k]%mod*c(j-1,k)%mod*c(sz[i]-1-j,sz[i*2]-k))%mod;
			}
			// fprintf(stderr,"dp[%d][%d]=%lld\n",i,j,dp[i][j]);
		}
	}
	printf("%lld",(dp[1][y-1]%mod+mod)%mod);
}