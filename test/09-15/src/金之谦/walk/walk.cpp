#include <cstdio>
#include <bitset>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=5010;
int n,MOD,a[N],b[N],f[2][N];
bitset<1010>z[1010];
char ans[2*N];
int main()
{
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	n=read();MOD=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++)b[i]=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if(f[i&1][j-1]>f[i&1^1][j]){
				z[i][j]=0;
				f[i&1][j]=f[i&1][j-1];
			}else{
				z[i][j]=1;
				f[i&1][j]=f[i&1^1][j];
			}
			f[i&1][j]+=(a[i]*b[j]%MOD);
		}
	for(int i=2*n-2,x=n,y=n;i;i--){
		if(z[x][y]){
			ans[i]='D';x--;
		}else{
			ans[i]='R';y--;
		}
	}
	printf("%s",ans+1);
	return 0;
}