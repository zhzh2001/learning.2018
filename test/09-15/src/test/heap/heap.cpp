#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
#define PI pair<int,int>
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){write(a); puts("");}
inline void wri(ll a){write(a); putchar(' ');}
inline ull rnd(){
	return ((ull)rand()<<30^rand())<<4|rand()%4;
}
const int N=1<<22,mod=998244353;
ll ans,dp[N];
int n,x,y,sz[N],a[N],c[5005][5005];
void cal(){
	ll sum=1; 
	for(int i=x;i;i>>=1){
		int t=sz[i],y=0;
		if(a[i<<1])y=sz[i<<1];
		if(a[i<<1|1])y=sz[i<<1|1];
		sum=sum*c[n-a[i]-y][t-y-1]%mod;
	}
	ans=(ans+sum)%mod;
}
void dfs(int p,int t){
	if(p==0){
		cal(); return;
	}
	if(p==1){
		a[p]=1; dfs(p>>1,0); return;
	}
	t=min(t,n-sz[p]+1);
	for(int i=2;i<=t;i++){a[p]=i; dfs(p>>1,i-1);}
}
void init(int p){
	if(p>n){dp[p]=1; return;}
	init(p*2); init(p*2+1);
	sz[p]=sz[p*2]+sz[p*2+1]+1;
	dp[p]=dp[p<<1]*dp[p<<1|1]%mod*c[sz[p]-1][sz[p*2]]%mod;
}
int main(){
	freopen("heap.in","r",stdin); freopen("heap.out","w",stdout);
	cin>>n>>x>>y;
	for(int i=0;i<=n;i++){
		c[i][0]=1; for(int j=1;j<=i;j++)c[i][j]=(c[i-1][j-1]+c[i-1][j])%mod;
	}
	a[x]=y; init(1);
	dfs(x>>1,y-1);
	for(int i=x;i>1;i>>=1)ans=ans*dp[i^1]%mod;
	ans=ans*dp[x]%mod;
	cout<<ans<<endl;
}
