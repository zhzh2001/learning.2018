#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void wri(ll a){write(a); putchar(' ');}
inline void writeln(ll a){write(a); puts("");}
const int N=10005;
int n,c,dq,a[N],b[N],f[N],g[N],ans[N];
inline int cal(int x,int y){
	return a[x]*b[y]%c;
}
signed main(){
	freopen("walk.in","r",stdin); freopen("walk.out","w",stdout);
	n=dq=read(); c=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++)b[i]=read();
	for(int i=n-1;i;i--){
		memset(f,0,sizeof(f));
		for(int j=1;j<=i;j++){
			for(int k=1;k<=dq;k++)f[k]=max(f[k],f[k-1])+cal(j,k);
		}
		for(int j=n;j;j--)g[j]=max(g[j],g[j+1])+cal(i+1,j);
		int Ans=0;
		for(int j=1;j<=dq;j++)Ans=max(Ans,g[j]+f[j]);
		for(int j=1;j<=dq;j++)if(g[j]+f[j]==Ans){
			ans[i+1]=dq-j; dq=j; break; 
		}
	}
	ans[1]=dq-1;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=ans[i];j++)putchar('R'); if(i<n)putchar('D');
	}
}
/*
4 7
3 1 4 2
4 3 1 2

*/

