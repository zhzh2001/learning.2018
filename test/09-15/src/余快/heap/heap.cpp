/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define mod 998244353
//#define mod 1000000007
#define N 1000055
#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,fac[N],inv[N],g[N],sum[N],c,x1,vis[1005][1005],f[1005][1005];
//f[i][j] 第i个数，有j个大于c的方案数? 
inline void add(int &x,int k){
	x+=k;x-=(x>=mod)?mod:0;
}
int ksm(ll x,int k){
	int sum=1;
	while (k){
		if (k&1) sum=sum*x%mod;
		x=x*x%mod;k>>=1;
	}
	return sum;
}
inline int C(int n,int m){
	if (m<0||m>n||n<0) return 0;
	return (ll)fac[n]*inv[m]%mod*inv[n-m]%mod;
}
void dfs(int x){
	if (x>n) return;
	sum[x]=1;
	dfs(x*2);dfs(x*2+1);
	sum[x]+=sum[x*2]+sum[x*2+1];
	if (!g[x]) g[x]=g[x]+g[x*2]+g[x*2+1];
}
int dfs2(int x,int y){
	if (y<0||y>sum[x]-g[x]) return 0;
	if (g[x]&&y<sum[x1]-1) return 0;
	if (x==x1&&sum[x1]-y<=0) return 0;
	if (vis[x][y]) return f[x][y];
	vis[x][y]=1;
	if (x*2>n){
		return f[x][y]=1;
	}
	if (x*2==n){
		if (y==1||y==2) return f[x][y]=dfs2(x*2,1);
		else return f[x][y]=dfs2(x*2,0);
	}
	//枚举分给左边多少个大于c的
	f[x][y]=0;
	if (sum[x]-y-g[x]>0)
	F(i,0,y){
		add(f[x][y],(ll)dfs2(x*2,i)*dfs2(x*2+1,y-i)%mod*C(y,i)%mod*C(sum[x]-y-g[x]-1,sum[x*2]-i-g[x*2])%mod);
	}
	if (sum[x]-y-g[x]==0){
		if (x!=x1&&g[x]){
			return f[x][y]=0;
		}
		if (g[x]) add(f[x][y],(ll)dfs2(x*2,sum[x*2])*dfs2(x*2+1,sum[x*2+1])%mod*C(y,sum[x*2])%mod);
		else add(f[x][y],(ll)dfs2(x*2,sum[x*2])*dfs2(x*2+1,sum[x*2+1])%mod*C(y-1,sum[x*2])%mod);
//			add(f[x][y],(ll)dfs2(x*2,i)*dfs2(x*2+1,y-i)%mod*C(y,i)%mod*C(sum[x]-y-g[x],sum[x*2]-i-g[x*2])%mod);
	}
	return f[x][y];
}
int solve(int x){
	if (sum[x]==1||sum[x]==2) return 1;
	return (ll)solve(x*2)*solve(x*2+1)*C(sum[x]-1,sum[x*2])%mod;
}
void solve1(){
	dfs(1);
	wrn(solve(1));
}
signed main(){
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	n=read();x1=read();c=read();g[x1]=1;
	fac[0]=1;
	F(i,1,n) fac[i]=(ll)fac[i-1]*i%mod;
	inv[n]=ksm(fac[n],mod-2);
	D(i,n-1,0) inv[i]=(ll)inv[i+1]*(i+1)%mod;	
	if (x1==1&&c==1){
		solve1();return 0;
	}
	dfs(1);
	dfs2(1,n-c);
	wrn(f[1][n-c]%mod);
	return 0;
}
