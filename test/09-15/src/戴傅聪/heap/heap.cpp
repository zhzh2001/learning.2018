#include<bits/stdc++.h>
using namespace std;
#define mod 998244353
int fac[1000001];
inline int ksm(int a,int b)
{
	int res=1;
	for(;b;b>>=1,a=1ll*a*a%mod) if(b&1) res=1ll*res*a%mod;
	return res;
}
inline int inv(int x){return ksm(x,mod-2);}
inline int C(int n,int m){return 1ll*fac[n]*inv(1ll*fac[n-m]*fac[m]%mod)%mod;}
int size[1000001];int n;int l[1000001],r[1000001];
inline int getsize(int x)
{
	if(x>n) return 0;
	l[x]=x<<1;r[x]=x<<1|1;if(l[x]>n) l[x]=0;if(r[x]>n) r[x]=0;
	size[x]=1+getsize(x<<1)+getsize(x<<1|1);
	return size[x];
}
int f[1000001];
inline int DP(int now)
{
	if(now==0) return 1;
	int _l=l[now],_r=r[now],lv=DP(_l),rv=DP(_r);
	int lsize=size[_l];
	f[now]=1ll*lv*rv%mod*C(size[now]-1,lsize)%mod;
	return f[now];
}
bool sel[64];int A[64];int x,y;int ans=0;
inline void dfs(int now)
{
	if(now==1)
	{
		A[now]=1;
		dfs(now+1);
		
		return;
	}
	if(now>n)
	{
		ans++;if(ans>=mod) ans-=mod;
		return;
	}
	if(now==x)
	{
		if(y<A[x>>1]) return;
		A[x]=y;
		dfs(now+1);
		return;
	}
	for(int i=A[now>>1]+1;i<=n;i++) if(sel[i]==0)
	{
		A[now]=i;
		sel[i]=1;
		dfs(now+1);
		sel[i]=0;
	}
}
inline void bf()
{
	ans=0;sel[y]=1;
	dfs(1);
	printf("%d",ans);
}
int main()
{
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	fac[0]=1;for(int i=1;i<=1000000;i++) fac[i]=1ll*fac[i-1]*i%mod;
	scanf("%d%d%d",&n,&x,&y);
	if(x==1&&y==1) return getsize(1),printf("%d",DP(1)),0;
	getsize(1);bf();
}
