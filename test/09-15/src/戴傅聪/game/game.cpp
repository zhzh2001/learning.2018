#include<bits/stdc++.h>
using namespace std;
namespace subtask1
{
	#define mod 998244353
	inline int ksm(int a,int b)
	{
		int res=1;
		for(;b;b>>=1,a=1ll*a*a%mod) if(b&1) res=1ll*res*a%mod;
		return res;
	}
	inline void solve()
	{
		char c=getchar();int n=0,N=0;;
		while(!isdigit(c)) c=getchar();
		while(isdigit(c))
		{
			n=(1ll*n%(mod-1)*10%(mod-1)+c-'0')%(mod-1);
			N=((1ll*N%mod)*10%mod+c-'0')%mod;
			c=getchar();
		}
		string s;cin>>s;if(s=="0") return (void)printf("1");
		int kkk=ksm(2,n-1);
		printf("%lld",1ll*(kkk-N+mod)%mod*ksm(kkk,mod-2)%mod);
	}
}
namespace subtask2
{
	inline void solve()
	{
		int T;scanf("%d",&T);int x;
		while(T--)
		{
			scanf("%d",&x);
			if(x==1) puts("cqz");else puts("ysgh");
		}
	}
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int opt;scanf("%d",&opt);
	if(opt==1) subtask1::solve();
	if(opt==2) subtask2::solve();
}
/*
3 3
1/4

6 4
26/32

13 1
4083/4096
*/
/*

*/
