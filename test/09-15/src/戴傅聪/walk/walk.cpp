#include<cstdio>
#include<cstdlib>
#include<string>
#include<iostream>
using namespace std;
int f[101][101];bool lst[101][101];
int a[101],b[101];
int n,c;
inline void print(int x,int y)
{
	if(x==1&&y==1) return;
	if(lst[x][y]) print(x-1,y),putchar('D');
	else print(x,y-1),putchar('R');
}
namespace Random
{
	int nowtemp;string s;string tmp;int maxans;
	inline int Val(int x,int y){return 1ll*a[x]*b[y]%c;}
	inline void dfs(int nowx,int nowy,int nowans)
	{
		if(nowx==n&&nowy==n)
		{
			if(nowans>maxans){maxans=nowans;s=tmp;}
			return;
		}
		if(nowx==n) return (void)(tmp+='R',dfs(nowx,nowy+1,nowans+Val(nowx,nowy+1)));
		if(nowy==n) return (void)(tmp+='D',dfs(nowx+1,nowy,nowans+Val(nowx+1,nowy)));
		bool cmp=Val(nowx+1,nowy)>Val(nowx,nowy+1);
		if(rand()*rand()%100000<=nowtemp) cmp^=1;
		if(cmp) tmp+='D',dfs(nowx+1,nowy,nowans+Val(nowx+1,nowy));
		else tmp+='R',dfs(nowx,nowy+1,nowans+Val(nowx,nowy+1));
	}
	inline void solve()
	{
		srand(20030112);maxans=0;
		for(int i=5000;i>=0;i--)
		{
			nowtemp=i;tmp.clear();
			dfs(1,1,Val(1,1));
		}
		cout<<s<<endl;
	}
}
int main()
{
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	cin>>n>>c;
	for(int i=1;i<=n;i++) cin>>a[i];
	for(int i=1;i<=n;i++) cin>>b[i];
	if(n>100) return Random::solve(),0;
	for(int i=1;i<=n;i++) for(int j=1;j<=n;j++)
	{
		f[i][j]=max(f[i-1][j],f[i][j-1])+1ll*a[i]*b[j]%c;
		if(f[i-1][j]>f[i][j-1]&&i!=1) lst[i][j]=1;else lst[i][j]=0;
	}
	print(n,n);
}
/*
3 4
1 2 3
1 2 3
*/
