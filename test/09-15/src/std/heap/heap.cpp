#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void wri(ll a){write(a); putchar(' ');}
inline void writeln(ll a){write(a); puts("");}
const int N=1000005,mod=998244353,logN=25;
int fac[N],ni[N],n,x,y,tot,sz[N<<1],ycl[N<<1],siz[logN];
int sum[logN],dp[logN][N];
ll ans=1;
inline ll ksm(ll a,int b){
	int ans=1;
	for(;b;b>>=1){
		if(b&1)ans=ans*a%mod;
		a=a*a%mod;
	}
	return ans;
}
inline ll c(int a,int b){
	return a>=b?(ll)fac[a]*ni[b]%mod*ni[a-b]%mod:0;
}
void dfs(int p){
	if(p>n){
		ycl[p]=1; return;
	}
	dfs(p*2); dfs(p*2+1);
	sz[p]=sz[p*2]+sz[p*2+1];
	ycl[p]=(ll)ycl[p*2]*ycl[p*2+1]%mod*c(sz[p],sz[p*2])%mod;
	sz[p]++;
}
void solve(int x,int y){
	if(!x)return;
	solve(x/2,x);
	if(!y)ans=ans*ycl[x]%mod; else ans=ans*ycl[x*2==y?x*2+1:x*2]%mod;
	siz[++tot]=sz[x]-sz[y];
}
signed main(){
	freopen("heap.in","r",stdin); freopen("heap.out","w",stdout);
	n=read(); x=read(); y=read();
	fac[0]=1;
	for(int i=1;i<=n;i++)fac[i]=(ll)fac[i-1]*i%mod; ni[n]=ksm(fac[n],mod-2);
	for(int i=n-1;i>=0;i--)ni[i]=(ll)ni[i+1]*(i+1)%mod;
	dfs(1);
	solve(x,0);
	dp[tot][y]=c(n-y,siz[tot]-1);
	for(int i=y-1;i;i--)dp[tot][i]=dp[tot][i+1];
	sum[tot]=siz[tot];
	for(int i=tot-1;i;i--){
		sum[i]=sum[i+1]+siz[i];
		for(int j=y;j;j--){
			dp[i][j]=(dp[i][j+1]+dp[i+1][j+1]*c(n-j-sum[i+1],siz[i]-1))%mod;
		}
	}
	//cout<<dp[1][1]<<" "<<ans<<" "<<siz[1]<<endl;
	cout<<ans*dp[1][1]%mod<<endl;
}

