#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define gc getchar
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=20005;
int f[N],g[N],a[N],b[N],c,n;
inline int cal(int x,int y){
	return a[x]*b[y]%c;
}
int t1,t2;
void solve(int n1,int m1,int n2,int m2){
	if(m1==m2){
		for(int i=n1;i<n2;i++)putchar('D'); return;
	}
	if(n1==n2){
		for(int i=m1;i<m2;i++)putchar('R'); t2+=m2-m1;
		return;
	}
	int mid=(n1+n2)>>1;
	for(int j=m1-1;j<=m2;j++)f[j]=g[j]=0;
	for(int i=n1;i<=mid;i++){
		for(int j=m1;j<=m2;j++){
			f[j]=max(f[j],f[j-1])+cal(i,j);
		}
	}
	for(int i=n2;i>mid;i--){
		for(int j=m2;j>=m1;j--){
			g[j]=max(g[j],g[j+1])+cal(i,j);
		}
	}
	int ans=0;
	for(int i=m1;i<=m2;i++)ans=max(ans,f[i]+g[i]);
	for(int i=m1;i<=m2;i++)if(f[i]+g[i]==ans){
		solve(n1,m1,mid,i); putchar('D'); t1++; solve(mid+1,i,n2,m2); return;
	}
}
int main(){
	freopen("walk.in","r",stdin); freopen("walk.out","w",stdout);
	n=read(); c=read();
	for(int i=1;i<=n;i++)a[i]=read(); for(int i=1;i<=n;i++)b[i]=read();
	solve(1,1,n,n);
	//cout<<t1<<" "<<t2<<endl;
}
