#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
#define PI pair<int,int>
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){write(a); puts("");}
inline void wri(ll a){write(a); putchar(' ');}
inline ull rnd(){
	return ((ull)rand()<<30^rand())<<4|rand()%4;
}
int n;
const int mod=998244353;
inline pair<ll,ll> Read(){
	ll x = 0,xx=0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	{x = (x * 10 + ch - '0')%mod; xx=(xx*10+ch-'0')%(mod-1);}
	return positive ? mp(x,xx) : mp(-x,-xx);
}
inline ll ksm(ll a,int b){
	int ans=1;
	for(;b;b>>=1){
		if(b&1)ans=ans*a%mod;
		a=a*a%mod;
	}
	return ans;
}
int main(){
	freopen("game.in","r",stdin); freopen("game.out","w",stdout);
	int op=read();
	if(op==2){
		int T=read();
		while(T--){
			int n=read();
			if(n==1)puts("cqz"); else puts("ysgh");
		}	
	}else{
		pair<ll,ll> n=Read();
		int t=ksm(2,n.second-1);
		cout<<(t-n.first+mod)*ksm(t,mod-2)%mod<<endl;
	}
	
}

