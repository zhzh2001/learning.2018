#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
const int mod=998244353;
inline pair<ll,ll> read(){
	ll x = 0,xx=0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	{x = (x * 10 + ch - '0')%mod; xx=(xx*10+ch-'0')%(mod-1);}
	return positive ? mp(x,xx) : mp(-x,-xx);
}
inline void write(ll a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
inline ll ksm(ll a,int b){
	int ans=1;
	for(;b;b>>=1){
		if(b&1)ans=ans*a%mod;
		a=a*a%mod;
	}
	return ans;
}
int n;
int main(){
	freopen("vector.in","r",stdin); freopen("vector.out","w",stdout);
	pair<ll,ll> n=read();
	int t=ksm(2,n.second-1);
	cout<<(t-n.first+mod)*ksm(t,mod-2)%mod<<endl;
}
