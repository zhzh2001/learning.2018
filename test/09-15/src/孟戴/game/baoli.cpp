#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
//ifstream fin("game.in");
//ofstream fout("game.out");
const long long mod=998244353;
inline long long f(long long a,long long b){
	long long ans=1;
	for(;b;b>>=1){
		if(b&1)ans=(long long)ans*a%mod;
		a=(long long)a*a%mod;
	}
	return ans%mod;
}
long long op,T,n;
int num[18];
int dfs(int flag){//1:ysgs这一步操作,0:小司机这一步操作
	//cout<<flag<<" "<<num[1]<<" "<<num[2]<<endl;
	if(num[0]==0){
		return flag^1;//1：ysgs输，0：小司机输
	}
	int wh[6],ans=flag;
	wh[0]=0;
	for(int i=1;i<=n;i++){
		if(!num[i]){
			continue;
		}
		int now=i;
		while(num[now]&&now>=1){
			num[now]=0;
			wh[++wh[0]]=now;
			num[0]--;
			now=now/2;
		}
		int sta=dfs(flag^1);
		for(int j=1;j<=wh[0];j++){
			num[wh[j]]=1;
			num[0]++;
		}
		wh[0]=0;
		if(flag==0&&sta==1){
			return flag=1;
		}else if(flag==1&&sta==0){
			return flag=0;
		}
	}
	return flag;
}
int main(){
	cin>>T;
	while(T--){
		cin>>n;
		num[0]=n; 
		for(int i=1;i<=n;i++){
			num[i]=1;
		}
		if(dfs(1)){
			cout<<"cqz"<<endl;
		}else{
			cout<<"ysgh"<<endl;
		}
	}
	return 0;
}
//3 3 1/4
//6 4 13/16 26/32
//2*12=4083/4096
//n k (2^(n-1)-n)/2^(n-1)
