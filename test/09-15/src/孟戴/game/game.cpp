//#include<iostream>
#include<algorithm>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("game.in");
ofstream fout("game.out");
const long long mod=998244353;
inline long long f(long long a,long long b){
	long long ans=1;
	for(;b;b>>=1){
		if(b&1)ans=(long long)ans*a%mod;
		a=(long long)a*a%mod;
	}
	return ans%mod;
}
long long op,T,n;
int main(){
	fin>>op;
	if(op==1){
		char ch[100003];
		long long bin[13];
		bin[0]=1;
		for(int i=1;i<=10;i++){
			bin[i]=bin[i-1]*2;
		}
		fin>>(ch+1);
		long long mi=1,di=1,tot=0;
		for(int i=strlen(ch+1);i>=1;i--){
			mi=mi*f(2,di*(ch[i]-'0'))%mod;
			di=di*10%mod;
		}
		for(int i=1;i<=strlen(ch+1);i++){
			tot=(tot*10+(ch[i]-'0'))%mod;
		}
		mi=mi*f(2,mod-2)%mod;
		fout<<(((mi-tot+mod)%mod)*f(mi,mod-2))%mod<<endl;
		return 0;
	}else{
		fin>>T;
		while(T--){
			fin>>n;
			if(n==1){
				fout<<"cqz"<<endl;
			}else if(n==2){
				fout<<"ysgh"<<endl;
			}else if(n==3){
				fout<<"ysgh"<<endl;
			}else if(n==4){
				fout<<"ysgh"<<endl;
			}else if(n==5){
				fout<<"ysgh"<<endl;
			}else if(n==6){
				fout<<"ysgh"<<endl;
			}else if(n==7){
				fout<<"ysgh"<<endl;
			}else if(n==8){
				fout<<"ysgh"<<endl;
			}else if(n==9){
				fout<<"ysgh"<<endl;
			}else if(n==10){
				fout<<"ysgh"<<endl;
			}else{
				fout<<"ysgh"<<endl;
			}
		}
	}
	return 0;
}
//81*81*81*81*81
//3 3 1/4
//6 4 13/16 26/32
//2*12=4083/4096
//n k (2^(n-1)-n)/2^(n-1)
