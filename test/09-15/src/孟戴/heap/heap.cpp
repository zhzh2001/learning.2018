#include<fstream>
#include<algorithm>
#include<math.h>
using namespace std;
ifstream fin("heap.in");
ofstream fout("heap.out");
int n,ans;
int vis[2003];
int a[2003];
inline bool check(){
	for(int i=n;i;i--){
		if(a[i]<=a[i/2])return false;
	}
	return true;
}
void dfs(int k){
	if(a[k]!=0){
		dfs(k+1);
		return;
	}
	if(k>n){
		if(check())ans++;
		return;
	}
	for(int i=a[k/2]+1;i<=n;i++){
		if(!vis[i]){
			a[k]=i,vis[i]=true,dfs(k+1),vis[i]=false,a[k]=0;
		}
	}
}
int main(){
	a[0]=0;
	int t;
	fin>>n>>t;
	fin>>a[t];
	vis[a[t]]=true;
	dfs(1);
	fout<<ans<<endl;
	return 0;
}
