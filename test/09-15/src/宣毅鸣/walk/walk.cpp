#include<cstdio>
using namespace std;
typedef long long ll;
const int N1=105,N2=5005;
int n,m,a[N2],b[N2];
ll f[N1][N1];
char ans[N2*2];
bool d[N1][N1];
int main()
{
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)scanf("%d",&a[i]);
	for (int i=1;i<=n;i++)scanf("%d",&b[i]);
	if (n<=100)
	 {
	 	f[1][1]=a[1]*b[1]%m;
	 	for (int i=1;i<=n;i++)
		 for (int j=1;j<=n;j++)
		  if (i!=1||j!=1)
		   {
		   	if (i!=1)d[i][j]=0,f[i][j]=f[i-1][j];
		   	if (j!=1&&f[i][j-1]>f[i][j])d[i][j]=1,f[i][j]=f[i][j-1];
		   	f[i][j]+=a[i]*b[j]%m;
		   }  
		int x=n,y=n;
		while (x!=1||y!=1)
		 {
		 	if (!d[x][y])ans[x+y-2]='D',x--;
		 	else ans[x+y-2]='R',y--;
		 }   
		for (int i=1;i<=n+n-2;i++)putchar(ans[i]); 
	 }
	else
	 {
	 	for (int i=1;i<=n;i++)scanf("%d",&a[i]);
	 	for (int i=1;i<=n;i++)scanf("%d",&b[i]);
	 	int x1=1,y1=1;
	 	while (x1!=n||y1!=n)
	 	 {
	 	 	if (x1==n)putchar('R'),y1++;
	 	 	else if (y1==n)putchar('D'),x1++;
	 	 	else if (a[x1+1]*b[y1]%m>a[x1]*b[y1+1])putchar('D'),x1++;
	 	 	else putchar('R'),y1++;
		 }
	 } 	 
	return 0; 
}
