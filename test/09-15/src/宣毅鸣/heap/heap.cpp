#include<bits/stdc++.h>
using namespace std;
const int N=21,M=998244353;
int dp[N][1<<N],n,xx,yy;
int dfs(int x,int y)
{
	if (dp[x][y]!=-1)return dp[x][y];
	if (x==yy)
	 {
	 	if (xx==1&&yy==1)return 1;
	 	if (!(y&(1<<(xx-1)))||!(y&(1<<(xx/2-1))))return 0;
	 	return dp[x][y]=dfs(x-1,y^(1<<(xx-1)));
	 }	
	if (x==1)return y==1;
	dp[x][y]=0; 
	for (int i=1;i<=n;i++)
	 if ((y&(1<<(i-1)))&&(y&(1<<(i/2-1))))(dp[x][y]+=dfs(x-1,y^(1<<(i-1))))%=M;
	return dp[x][y];
}
int main()
{
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	scanf("%d%d%d",&n,&xx,&yy);
	memset(dp,-1,sizeof dp);
	printf("%d",dfs(n,(1<<n)-1));
	return 0;
}
