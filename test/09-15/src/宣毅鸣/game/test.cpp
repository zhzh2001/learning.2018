#include<bits/stdc++.h>
using namespace std;
int f[1<<20];
int dfs(int x)
{
	if (x==0)return 1;
	if (f[x]!=-1)return f[x];
	f[x]=0;
	for (int i=1;i<=18;i++)
	 if (x&(1<<(i-1)))
	  {
	  	int s=x;
	  	for (int k=i;k;k/=2)
		 if (s&(1<<(k-1)))s^=(1<<(k-1));
	  	if (!dfs(s))return f[x]=1;
	  }
	return 0;  
}
int main()
{
	for (int i=0;i<1<<18;i++)f[i]=-1;
	for (int i=1;i<1<<18;i++)
	dfs(i);
	for (int i=1;i<=18;i++)printf("%d\n",f[(1<<i)-1]);
}
