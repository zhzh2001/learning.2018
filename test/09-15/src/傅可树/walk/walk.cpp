#include<cstdio>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e3+5;
int b[N],a[N],n,C;
inline void init(){
	n=read(); C=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<=n;i++) b[i]=read();
} 
const int dx[]={0,1};
const int dy[]={1,0};
int cur,dp[2][N],g[2][N][N/30];
inline void upd(int x,int y,int opt){
	int X=x+dx[opt],Y=y+dy[opt];
	if (X>0&&X<=n&&Y>0&&Y<=n){
		int step=X+Y-2,tmp=(step-1)/31+1,q=step-(tmp-1)*31;
		int w=dp[cur][y]+a[X]*b[Y]%C;
		if (x==X){
			if (dp[cur][Y]<w) {
				dp[cur][Y]=w;
				for (int j=1;j<=tmp;j++) g[cur][Y][j]=g[cur][y][j];
				g[cur][Y][tmp]=g[cur][Y][tmp]+opt*(1<<q-1);
			}
		}else{
			if (dp[cur^1][Y]<w) {
				dp[cur^1][Y]=w;
				for (int j=1;j<=tmp;j++) g[cur^1][Y][j]=g[cur][y][j];
				g[cur^1][Y][tmp]=g[cur^1][Y][tmp]+opt*(1<<q-1);
			}
		}
	}
} 
inline void solve(){
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++) dp[cur^1][j]=0;
		for (int j=1;j<=n;j++) {
			upd(i,j,0); upd(i,j,1);
		}
		cur^=1;
	}
	cur^=1;
	int step=n*2-2,tmp=(step-1)/31+1,q=step%31;
	for (int i=1;i<=tmp;i++){
		for (int j=0;j<31;j++) {
			if (!(g[cur][n][i]&(1<<(j)))) putchar('R');
				else putchar('D');
			if ((i-1)*31+j+1==step) return;
		}
	}
}
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	init();
	solve();
	return 0;
}
