#include<cstdio>
#include<cstring>
#include<algorithm>
#include<bitset>
#define ll int
#define N 1005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll a[N],b[N],c,f[2][N],n,now;
bitset<N> g[N];
void sol(ll x,ll y){
	if (x==y&&x==1) return;
	if (g[x][y]) sol(x-1,y);
	else sol(x,y-1);
	if (g[x][y]) putchar('D');
	else putchar('R');
}
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	n=read(); c=read();
	rep(i,1,n) a[i]=read();
	rep(i,1,n) b[i]=read();
	rep(i,1,n){
		f[now][1]=f[now^1][1]+a[i]*b[1]%c;
		f[now][i]=f[now^1][i-1]+a[1]*b[i]%c;
		g[i][1]=1;
		rep(j,2,i-1){
			if (f[now^1][j-1]<f[now^1][j]){
				f[now][j]=f[now^1][j]+a[i-j+1]*b[j]%c;
				g[i-j+1][j]=1;
			}else f[now][j]=f[now^1][j-1]+a[i-j+1]*b[j]%c;
		}
		now^=1;
	}
	rep(i,n+1,n*2-1){
		rep(j,1,2*n-i)
		if (f[now^1][j+1]>f[now^1][j]){
			g[n-j+1][i-n+j]=1;
			f[now][j]=f[now^1][j+1]+a[n-j+1]*b[i-n+j]%c;
		}else f[now][j]=f[now^1][j]+a[n-j+1]*b[i-n+j]%c;
		now^=1;
	}
	sol(n,n);
}
