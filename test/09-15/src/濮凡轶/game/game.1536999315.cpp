#include <cstdio>
#include <algorithm>

using namespace std;

inline void sol2()
{
	int T;
	scanf("%d", &T);
	while(T--)
	{
		int ask;
		scanf("%d", &ask);
		puts(ask == 1 ? "cqz" : "ysgh");
	}
}

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	int cmd;
	scanf("%d", &cmd);
	if(cmd == 2)
		sol2();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
