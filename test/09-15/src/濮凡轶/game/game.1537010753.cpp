#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

char c[1000005];

inline void sol1()
{
	scanf("%s", c);
	int len = strlen(c);
	if(len != 1 && c[len-1] != '0' && c[len-1] != '1')
	{
		if((c[len-1] ^ 48) & 1)
			puts("748683265");
		else
			puts("187170817");
	}
	else if(c[len-1] == '0')
		puts("1");
	else
		puts("0");
}

inline void sol2()
{
	int T;
	scanf("%d", &T);
	while(T--)
	{
		int ask;
		scanf("%d", &ask);
		puts(ask == 1 ? "cqz" : "ysgh");
	}
}

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	int cmd;
	scanf("%d", &cmd);
	if(cmd == 2)
		sol2();
	else
		sol1();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
