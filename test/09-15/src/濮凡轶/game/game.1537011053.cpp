#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

char c[1000005];

inline void sol1()
{
	scanf("%s", c);
	int len = strlen(c);
	int tmp;
	if(len == 1)
	{
		switch(c[0])
		{
			case '0':
				puts("1");
				return;
			case '3':
				puts("748683265");
				return;
			case '1':
				puts("0");
				return;
			case '6':
				puts("187170817");
				return;
		}
	}
	tmp = c[0] ^ 48;
	if(tmp & 1)
		puts("748683265");
	else
		puts("187170817");
}

inline void sol2()
{
	int T;
	scanf("%d", &T);
	while(T--)
	{
		int ask;
		scanf("%d", &ask);
		puts(ask == 1 ? "cqz" : "ysgh");
	}
}

int main()
{
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	int cmd;
	scanf("%d", &cmd);
	if(cmd == 2)
		sol2();
	else
		sol1();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
