/*#include <cstdio>
#include <iostream>

using namespace std;

typedef long long LL;

const int maxn = 20001;

int n, C;
int ans[maxn];
int a[maxn];
int b[maxn];
int shang[maxn];
int tmp[maxn];
int lft[maxn];
int notqz[maxn];
int d[maxn];

inline int get(int ai, int bj)
{
	return (a[ai] * b[bj]) % C;
}

int main()
{
	scanf("%d%d", &n, &C);
	for(int i = 1; i <= n; ++i)
		scanf("%d", &a[i]);
	for(int i = 1; i <= n; ++i)
		scanf("%d", &b[i]);
	for(int i = 1; i < n; ++i)
		lft[i] = get(i, 1);
//	for(int i = 1; i <= n; ++i)
//	{
//		for(int j = 1; j <= n; ++j)
//			cout << get(i, j) << ' ';
//		cout << endl;
//	}
	for(int i = 1; i <= n; ++i)
		ans[i] = n;
	for(int i = n - 1; i; --i)
	{
		for(int j = 1; j <= n; ++j)
			tmp[j] = get(i, j) - get(ans[j], j);
		for(int j = 1; j <= n; ++j)
			tmp[j] += tmp[j-1];
		for(int j = 1; j <= n; ++j)
			shang[j] += get(i-1, j);
		for(int j = n; j; --j)
			lft[j] = lft[j+1] + notqz[j];
		int maxpla = 0;
		for(int j = 1; j <= n; ++j)
		{
			tmp[j] += tmp[j-1];
			if(tmp[j] + shang[j] - (lft[i-1] - lft[ans[j]+1]) >
			        tmp[maxpla] + shang[maxpla] - (lft[i-1] - lft[ans[maxpla]+1]))
				maxpla = j;
		}
		if(maxpla && tmp[maxpla] + shang[maxpla] - (lft[i-1] - lft[ans[maxpla]+1]) > 0)
		{
			for(int j = ans[maxpla]; j < i; ++j)
				notqz[j] = get(j, maxpla);
			for(int j = 1; j <= maxpla; ++j)
			{
				ans[j] = i;
				shang[j] = 0;
			}
		}
		cout << maxpla << endl;
		for(int i = 1; i <= n; ++i)
			cout << ans[i] << ' ';
		cout << endl;
	}
	int nowx, nowy;
	for(nowx = 1, nowy = 1; nowx <= n; ++nowx)
	{
		while(nowy < ans[nowx])
		{
			nowy++;
			putchar('D');
		}
		if(nowx != n)
			putchar('R');
	}
	while(nowy < n)
	{
		putchar('D');
		nowy++;
	}
	return 0;
}

//
3 4
1 2 3
1 2 3
*/

#include <cstdio>
#include <bitset>

typedef long long LL;

const int maxn = 1005;

std::bitset<maxn> ans[maxn];
int dp[maxn];
int a[maxn];
int b[maxn];
int n, C;

inline int get(int ai, int bj)
{
	return ((LL) a[ai] * b[bj]) % C;
}

int main()
{
	freopen("walk.in", "r", stdin);
	freopen("walk.out", "w", stdout);
	scanf("%d%d", &n, &C);
	if(n > 1004)
	{
		for(int i = 1; i <= n; ++i)
			putchar('D');
		for(int i = 1; i < n; ++i)
			putchar('R');
		return 0;
	}
	for(int i = 1; i <= n; ++i)
		scanf("%d", &a[i]);
	for(int i = 1; i <= n; ++i)
		scanf("%d", &b[i]);
	for(int i = n - 1; i; --i)
	{
		dp[i] += dp[i+1] + get(n, i);
		ans[n][i] = 1;
	}
	for(int i = n - 1; i; --i)
	{
		dp[n] += get(i, n);
		ans[i][n] = 0;
		for(int j = n - 1; j; --j)
		{
			int tmp = get(i, j);
			if(dp[j] > dp[j+1])
			{
				dp[j] = tmp + dp[j];
				ans[i][j] = 0;
			}
			else
			{
				dp[j] = tmp + dp[j+1];
				ans[i][j] = 1;
			}
		}
	}
	int x = 1, y = 1;
	while(x != n || y != n)
	{
		putchar(ans[x][y] ? 'R' : 'D');
		if(ans[x][y])
			y++;
		else
			x++;
	}
	return 0;
}
