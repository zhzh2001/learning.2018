#include <cstdio>
#define N 5020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
int a[N], b[N], c;
inline calc(int x, int y) {
  return a[x] * b[y] % c;
}
int main(int argc, char const *argv[]) {
  freopen("walk.in", "r", stdin);
  freopen("walk.out", "w", stdout);
  int n = read(); c = read();
  for (int i = 1; i <= n; ++ i) {
    a[i] = read();
  }
  for (int i = 1; i <= n; ++ i) {
    b[i] = read();
  }
  int x = 1, y = 1;
  long long ans = calc(1, 1);
  while (x < n || y < n) {
    if (x < n && calc(x + 1, y) > calc(x, y + 1)) {
      putchar('D');
      ++ x;
    } else {
      putchar('R');
      ++ y;
    }
    ans += calc(x, y);
  }
  putchar('\n');
  printf("%lld\n", ans);
  return 0;
}