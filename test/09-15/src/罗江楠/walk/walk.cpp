// int __A;
#include <cstdio>
#define N 5020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
int a[N], b[N], c;
inline int calc(int x, int y) {
  return a[x] * b[y] % c;
}
inline int max(int a, int b) {
  return a > b ? a : b;
}
const int kSize = 9;
int dp[kSize][kSize];
bool make_choice(int x, int y) {
  for (int i = 0; i < kSize; ++ i) {
    for (int j = 0; j < kSize; ++ j) {
      dp[i][j] = calc(x + i, y + j);
    }
  }
  for (int i = kSize - 1; ~ i; -- i) {
    for (int j = kSize - 1; ~ j; -- j) {
      if (!i && !j) break;
      int mx = 0;
      if (i < kSize - 1) {
        mx = max(mx, dp[i + 1][j]);
      }
      if (j < kSize - 1) {
        mx = max(mx, dp[i][j + 1]);
      }
      dp[i][j] += mx;
    }
  }
  return dp[1][0] > dp[0][1];
}
int arr[105][105];
bool chc[105][105];
void baoli(int n) {
  for (int i = 1; i <= n; ++ i) {
    for (int j = 1; j <= n; ++ j) {
      arr[i][j] = calc(i, j);
    }
  }
  for (int i = n - 1; i; -- i) {
    chc[i][n] = 1;
    arr[i][n] += arr[i + 1][n];
    chc[n][i] = 0;
    arr[n][i] += arr[n][i + 1];
  }
  for (int i = n - 1; i; -- i) {
    for (int j = n - 1; j; -- j) {
      if (arr[i + 1][j] > arr[i][j + 1]) {
        chc[i][j] = 1;
        arr[i][j] += arr[i + 1][j];
      } else {
        chc[i][j] = 0;
        arr[i][j] += arr[i][j + 1];
      }
    }
  }
}
// int __B;
int main(int argc, char const *argv[]) {
  freopen("walk.in", "r", stdin);
  freopen("walk.out", "w", stdout);
  int n = read(); c = read();
  for (int i = 1; i <= n; ++ i) {
    a[i] = read();
  }
  for (int i = 1; i <= n; ++ i) {
    b[i] = read();
  }
  if (n > 100) {
  
    int x = 1, y = 1;
    // long long ans = calc(1, 1); // SIGN_CALCANS
    while (x < n || y < n) {
      if (make_choice(x, y)) {
        putchar('D');
        ++ x;
      } else {
        putchar('R');
        ++ y;
      }
      // ans += calc(x, y); // SIGN_CALCANS
    }
    putchar('\n');
    // printf("%lld\n", ans); // SIGN_CALCANS
    return 0;
  
  }
  baoli(n);
  // special(n);

  int x = 1, y = 1;
  // long long ans = calc(1, 1); // SIGN_CALCANS
  while (x < n || y < n) {
    if (chc[x][y]) {
      putchar('D');
      ++ x;
    } else {
      putchar('R');
      ++ y;
    }
    // ans += calc(x, y); // SIGN_CALCANS
  }
  putchar('\n');
  // printf("%lld\n", ans); // SIGN_CALCANS
  return 0;
}