#include <bits/stdc++.h>
#define N 100020
#define mod 998244353
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

namespace Task1 {

ll fast_pow(ll x, ll y) {
  ll z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}
ll inv(ll x) {
  return fast_pow(x, mod - 2);
}
ll get_mod(ll fz, ll fm) {
  return fz * inv(fm) % mod;
}

char str[100020];

int main() {

  scanf("%s", str);
  int len = strlen(str);
  ll n = 0;
  for (int i = 0; i < len; ++ i) {
    n = (n * 10 + str[i] - '0') % mod;
  }
  ll fm = fast_pow(2, n - 1);
  ll fz = ((fm - n) % mod + mod) % mod;
  printf("%lld\n", get_mod(fz, fm));

  return 0;
}

} // end namespace Task1

namespace Task2 {

bool resolve(int x) {
  if (x & 1) -- x;
  bool res = 0;
  while (x) {
    if (x & 1) res = !res;
    x >>= 1;
  }
  return res;
}

int main() {
  int T = read();
  while (T --) {
    int x = read();
    puts(resolve(x) ? "ysgh" : "cqz");
  }
  return 0;
}

} // end namespace Task2

int main(int argc, char const *argv[]) {
  freopen("game.in", "r", stdin);
  freopen("game.out", "w", stdout);

  int op = read();
  if (op == 1) {
    return Task1::main();
  } else {
    return Task2::main();
  }
}