#include <bits/stdc++.h>
#define N 1000020
#define mod 998244353
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

ll fast_pow(ll x, ll y) {
  ll z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}
ll inv(ll x) {
  return fast_pow(x, mod - 2);
}
ll frac[N];
// select m items in n items
ll C(ll n, ll m) {
  if (n < 0 || m < 0) return 1;
  return frac[n] * inv(frac[m]) % mod * inv(frac[n - m]) % mod;
}

int n, x, y;
int sz[N];

int calc_size(int x) {
  sz[x] = 1;
  if ((x << 1) <= n) sz[x] += calc_size(x << 1);
  if ((x<<1|1) <= n) sz[x] += calc_size(x<<1|1);
  return sz[x];
}

ll __tmp[N];

ll fulfill(int x) {
  if (__tmp[sz[x]]) return __tmp[sz[x]];
  int size = sz[x] - 1;
  ll res = 1;
  if ((x<<1|1) <= n) {
    res = res * C(size, sz[x << 1]) % mod;
    res = res * fulfill(x << 1) % mod;
    res = res * fulfill(x<<1|1) % mod;
  }
  return __tmp[sz[x]] = res;
}

int bitcount(int x) {
  return x == 1 ? 1 : (bitcount(x >> 1) + 1);
}

int main(int argc, char const *argv[]) {
  freopen("heap.in", "r", stdin);
  freopen("heap.out", "w", stdout);
  n = read(); x = read(); y = read();
  frac[0] = 1;
  for (int i = 1; i <= 1000000; ++ i) {
    frac[i] = frac[i - 1] * i % mod;
  }
  calc_size(1);
  // printf("%d\n", fulfill(1));

  int nbt = bitcount(x);
  ll res = C(y - 2, nbt - 2);

  // printf("%lld\n", res);

  if ((x << 1) <= n) res = res * C(n - y, sz[x << 1]) % mod * fulfill(x << 1) % mod;
  if ((x<<1|1) <= n) res = res * C(n - y, sz[x<<1|1]) % mod * fulfill(x<<1|1) % mod;

  // printf("%lld\n", res);

  while (x > 1) {
    int t = x & 1;
    x >>= 1;
    if (!t) {
      // is the left child of x
      res = res * C(n - bitcount(x) - sz[x << 1], sz[x<<1|1]) % mod * fulfill(x<<1|1) % mod;
    } else {
      res = res * C(n - bitcount(x) - sz[x<<1|1], sz[x << 1]) % mod * fulfill(x << 1) % mod;
    }
  }

  printf("%lld\n", res);

  return 0;
}