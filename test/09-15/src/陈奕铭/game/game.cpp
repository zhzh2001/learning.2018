#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int mod = 998244353;
int op;
int n,k;

inline int qpow(int x,int n){
	int ans = 1;
	while(n){
		if(n&1) ans = 1LL*ans*x%mod;
		x = 1LL*x*x%mod;
		n >>= 1;
	}
	return ans;
}


signed main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	op = read();
	if(op == 1){
		n = read(); k = read();
		if(k == 0){
			puts("1");
			return 0;
		}
		if(n == 1){
			puts("0");
		}else{
			int pos = qpow(2,mod-2);
			printf("%d\n",qpow(pos,n-1));
		}
	}
	else{
		int T = read();
		while(T--){
			n = read();
			if(n == 1) puts("cqz");
			else puts("ysgh");
		}
	}
	return 0;
}