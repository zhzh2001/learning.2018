#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 1000005,mod = 998244353;
int n,x,y;
int size[N];
int num[N];	//当无约束时节点数为i的小根堆的个数
int sum[N],rev[N];
int bin[21];
int Log[N];
int q[23],top;
int dp[23][N];	//当前是哪个位置的数字，当前数字是几
int qian[23][N];	//dp的后缀和

inline int qpow(int x,int n){
	int ans = 1;
	while(n){
		if(n&1) ans = 1LL*ans*x%mod;
		x = 1LL*x*x%mod;
		n >>= 1;
	}
	return ans;
}

inline ll C(int n,int m){ return (1LL*sum[n]*rev[m])%mod*rev[n-m]%mod; }
inline void getQ(){
	int X = x;
	while(X){
		q[++top] = X;
		X >>= 1;
	}
}
inline ll P(int n,int m){ return 1LL*C(n,m)*num[m]%mod; }

signed main(){
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	n = read(); x = read(); y = read();
	bin[0] = 1; for(int i = 1;i <= 20;++i) bin[i] = bin[i-1]<<1;
	Log[1] = 0; for(int i = 2;i <= n+1;++i) Log[i] = Log[i>>1]+1;
	for(int i = n;i;--i){
		if(i*2 <= n) size[i] += size[i*2];
		if(i*2+1 <= n) size[i] += size[i*2+1];
		++size[i];
	}
	// printf("%d\n", size[1]);
	sum[0] = 1; for(int i = 1;i <= n;++i) sum[i] = 1LL*sum[i-1]*i%mod;
	rev[n] = qpow(sum[n],mod-2); for(int i = n-1;i >= 0;--i) rev[i] = 1LL*rev[i+1]*(i+1)%mod;
	num[1] = num[2] = 1;
	for(int i = 3;i <= n;++i){
		int Len = bin[Log[i]-1],pos = i-Len*2+1;
		int L = Len-1 + min(pos,Len);
		int R = i-L-1;
		num[i] = (1LL*C(i-1,R)*num[L])%mod*num[R]%mod;
	}
	if(x == 1 && y == 1){
		printf("%d\n", num[n]);
		return 0;
	}
	// for(int i = 1;i <= 12;++i) printf("%d\n",num[i]);
	getQ();
	if(n-y < size[x]-1){puts("0"); return 0;}		//可用数字比x的子树少
	if(y < top){puts("0"); return 0;} //可用数字比到x的路径少
	// puts("1");
	if(size[x]-1 == 0) dp[1][y] = 1;
	else dp[1][y] = 1LL*C(n-y,size[x]-1)*num[size[x]]%mod;	//初始化
	// printf("%d %d\n", dp[1][y],n-y);
	for(int i = 1;i <= y;++i) qian[1][i] = dp[1][y];
	for(int i = 2;i <= top;++i){
		int now = q[i],L = 0;
		if(now*2 == q[i-1]) L = size[now*2+1];		//需要填的数字个数
		else L = size[now*2];
		int R = size[now]-L-1;	//已经使用过的数字个数
		// printf("%d %d\n", min(n-(size[now]-1),y-i+1),top-i+1);
		for(int j = min(n-(size[now]-1),y-i+1);j >= top-i+1;--j){	//当前节点的数值范围
			dp[i][j] = 1LL*qian[i-1][j+1]*P(n-j-R,L)%mod;	//上一个位置的数字要比当前大的所有可能性
			qian[i][j] = (1LL*dp[i][j]+qian[i][j+1])%mod;	//计算后缀和
		}
		for(int j = top-i;j >= 1;--j) qian[i][j] = qian[i][j+1];
	}
	printf("%d\n", dp[top][1]);
	return 0;
}