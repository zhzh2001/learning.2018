#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

//D:1,R:0
const int N = 1005;
int a[N],b[N];
int n,c;
int dp[N],Dp[N];
unsigned len[N][64],Len[N][64];
char s[N*2];
signed main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	n = read(); c = read();
	for(int i = 1;i <= n;++i) a[i] = read();
	for(int i = 1;i <= n;++i) b[i] = read();
	Dp[1] = a[1]*b[1]%c;
	for(int i = 1;i <= n*2-2;++i){
		for(int x = 1;x <= n;++x){
			int y = i-x+1; if(y < 0) break;
			if(y < n){
				if(dp[x] < Dp[x]+a[x]*b[y+1]%c){
					dp[x] = Dp[x]+a[x]*b[y+1]%c;
					len[x][i/32] = Len[x][i/32]*2;
				}
			}
			if(x < n){
				if(dp[x+1] < Dp[x]+a[x+1]*b[y]%c){
					dp[x+1] = Dp[x]+a[x+1]*b[y]%c;
					len[x+1][i/32] = Len[x][i/32]*2+1;
				}
			}
		}
		memcpy(Dp,dp,sizeof(dp));
		memcpy(Len,len,sizeof(Len));
	}
	for(int i = n*2-2;i;--i){
		if(len[n][i/32]&1) s[i] = 'D';
		else s[i] = 'R';
		len[n][i/32] >>= 1;
	}
	// s[n*2-1] = 0;
	printf("%s\n",s+1);
	return 0;
}