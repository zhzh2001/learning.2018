#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 111, Mod = 998244353; 
int n, x, y, ans, ww; 
int a[N], vis[N], ding[N]; 

void dfs(int x) {
	if(ding[x]) dfs(x+1); 
	if(x == n+1) {
		++ans; 
		return; 
	}
	For(i, 1, n) {
		if(!vis[i] && i>a[x>>1] && a[ww]>a[ww>>1]) {
			a[x] = i; 
			vis[i] = 1; 
			dfs(x+1); 
			vis[i] = 0; 
		}
	}
}

int main() {
	freopen("heap.in", "r", stdin); 
	freopen("heap.out", "w", stdout); 
	n = read(); x = read(); y = read(); 
	a[1] = 1; a[x] = y; vis[1] = 1; vis[y] = 1; ww = x;
	ding[1] = 1; ding[x] = 1;   
	dfs(2); 
	writeln(ans); 
}

