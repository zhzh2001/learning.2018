#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 40, tt = 2097152; 
int w[tt], b[60], bin[60]; 
int n; 

int main() {
	n = read(); 
	w[0] = 1; w[1] = 0; 
	bin[1] = 1; 
	For(i, 2, N) bin[i] = bin[i-1]*2; 
	For(i, 2, tt) {
		int tot = 0, x = i; 
		while(x) {
			b[++tot] = x&1; 
			x = x>>1; 
		}
		For(j, 1, tot) {
			int wei = j, y = i;  
			while(b[wei] && wei!=0) {
				y -= bin[wei]; 
				wei/=2; 
			}
			if(y!=i && !w[y]) {
				w[i] = 1; 
				break; 
			}
		}
	}
//	writeln(w[127]); 
	For(i, 2, 22) 
//		write(i), putchar(' '), writeln(w[i]); 
		writeln(w[bin[i]-1]);
}

