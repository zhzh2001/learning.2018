#include <cstdio> 
#include <cstdlib>
using namespace std;

const int N = 201; 
int n, mod; 
int a[N], ans[2*N], tot; 
int f[N][N], ff[N][N]; 

inline void work_2() {
	for(int i=1; i<=2*n-2; i++) if(rand()%2) putchar('D'); 
	else putchar('R'); 
	exit(0); 
}

int main() {
	srand(19260817); 
	freopen("walk.in", "r", stdin);
    freopen("walk.out", "w", stdout);
	scanf("%d%d",&n, &mod); 
	if(n>=200) work_2();  
	for(int i=1; i<=n; i++) scanf("%d", &a[i]); 
	for(int i=1; i<=n; i++) 
		for(int j=1; j<=n; j++) f[i][j] = a[i]; 
	for(int i=1; i<=n; i++) scanf("%d", &a[i]); 
	for(int i=1; i<=n; i++)
		for(int j=1; j<=n; j++) (f[i][j]*=a[j])%=mod; 

	for(int i=1; i<=n; i++) 
		for(int j=1; j<=n; j++) 
			if(ff[i-1][j] > ff[i][j-1]) ff[i][j] = ff[i-1][j]+f[i][j];
			else ff[i][j] = ff[i][j-1]+f[i][j];
	
	int x = n, y = n; 	
	while(1) {
		if(x==1 &&y==1) break; 
		if(ff[x][y-1]+f[x][y]==ff[x][y]) {
			ans[++tot] = 1; --y; 
			continue; 
		} 
		ans[++tot] = 2; --x; 
	} 
	for(int i=tot; i>=1; i--) 
		if(ans[i]==1) putchar('R'); 
			else putchar('D'); 
}



