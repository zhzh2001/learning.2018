#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=2000005,mod=998244353;
int fac[N],inv[N],f[N],h[N],g[50][N],si[N],q[50];
int x,y,n,sum,m;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
inline int Inv(int x)
{
	return ksm(x,mod-2);
}
inline int C(int x,int y)
{
	return x<y?0:(ll)fac[x]*inv[y]%mod*inv[x-y]%mod;
}
inline void dfs(int x)
{
	f[x]=1;
	if (x>n)
		return;
	dfs(x*2);
	dfs(x*2+1);
	si[x]=si[x*2]+si[x*2+1]+1;
	f[x]=(ll)f[x*2]*f[x*2+1]%mod*C(si[x]-1,si[x*2])%mod;
	// cout<<x<<' '<<f[x]<<'\n';
}
inline void pre_work()
{
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	read(n);
	read(x);
	read(y);
	fac[0]=1;
	for (int i=1;i<=n;++i)
		fac[i]=((ll)fac[i-1]*i)%mod;
	inv[n]=Inv(fac[n]);
	for (int i=n-1;i>=0;--i)
		inv[i]=((ll)inv[i+1]*(i+1))%mod;
}
inline void up(int x)
{
	if (x==1)
		return;
	int y=x/2;
	sum=(ll)sum*f[x^1]%mod;
	q[++m]=si[y]-si[x];
	up(y);
}
inline void dp()
{
	g[0][y]=C(n-y,si[x]-1);
	for (int i=y-1;i>=1;--i)
		g[0][i]=g[0][i+1];
	h[0]=si[x];
	for (int i=1;i<=m;++i)
		h[i]=h[i-1]+q[i];
	for (int i=1;i<=m;++i)
		for (int j=y;j>=1;--j)
			g[i][j]=((ll)g[i][j+1]+(ll)g[i-1][j+1]*(C(n-j-h[i-1],q[i]-1)%mod))%mod;;
}
int main()
{
	pre_work();
	dfs(1);
	m=0;
	sum=f[x];
	up(x);
	dp();
	cout<<(ll)g[m][1]*sum%mod;
	return 0;
}
