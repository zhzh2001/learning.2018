#include<bits/stdc++.h>

using namespace std;
typedef long long ll;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return w * x;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
}
const int N = 1234;
int n, x, y;
int a[N];
int main() {
	setIO();
	n = read(), x = read(), y = read();
	if(n > 10) {
		puts("0");
		return 0;
	}	
	for(int i = 1; i <= n; ++i)
		a[i] = i;
	int ans = 0;
	do {
		bool fg = 1;
		if(a[x] != y) continue;
		for(int i = n; i > 1; --i)
			if(a[i] < a[i / 2]) {
				fg = 0;
				break;
			}
		if(fg) ++ans;
	} while(next_permutation(a +1, a + 1 + n));
	writeln(ans);
	return 0;
}
