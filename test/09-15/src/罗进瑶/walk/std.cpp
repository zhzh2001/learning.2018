#include<cstdio>
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return w * x;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
}
int g[110][110], n, p, a[110], b[110];
int pre[110][110];
char si[] = "DR";
char ans[110];
int top;
int dx[] = {-1, 0};
int dy[] = {0, -1};
int main() {
//	setIO();
	n = read(), p = read();
	for(int i = 1; i <= n; ++i)
		a[i] = read();
	for(int i = 1; i <= n; ++i)
		b[i] = read();
	if(n > 100) {
		int x = 1, y = 1;
		while(x != n || y != n) {
			ll res = -1;
			bool dir = 0;
			if(x < n) res = a[x + 1] * b[y] % p;
			if(y < n && a[x] * b[y + 1] % p > res) 
				dir = 1;
			if(dir) {
				++y;
				putchar('R');
			} else {
				++x;
				putchar('D');
			}
		}
		return 0;
	}
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= n; ++j)
			g[i][j] = (ll) a[i] * b[j] % p;
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= n; ++j) {
			if(i == 1 && j == 1) continue;
			ll res = 0;
			pre[i][j] = 0;
			res = g[i - 1][j];
			if(g[i][j - 1] > res) {
				res = g[i][j - 1];
				pre[i][j] = 1;
			}
			g[i][j] += res;
		}
	int x = n, y = n;
	int t1, t2;
	while(x != 1 || y != 1) {
		ans[++top] = si[pre[x][y]];
		t1 = x, t2 = y;
		x += dx[pre[t1][t2]];
		y += dy[pre[t1][t2]];
	}
	for(int i = top; i >= 1; --i)
		putchar(ans[i]);
	return 0;
}
