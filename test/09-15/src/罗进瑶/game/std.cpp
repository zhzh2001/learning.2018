#include<bits/stdc++.h>

using namespace std;
typedef long long ll;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return w * x;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}
int f[1 << 20], pre[1 << 20]; 
int get(int S, int t) {
	while(1) {
		if(!t) return S;
		if(S & 1 << (t - 1))
			S ^= (1 << (t - 1));
		t /= 2;
	}
	return S;
}
bitset<20>t;
int main() {
	int op = read();
	if(op == 2) {
		int T = read();
		int B = (1 << 19);
		f[0] = 1;
		for(int S = 1; S < B; ++S) {
			bool fg = 1;
			for(int i = 1; i <= 18; ++i) if(S == (1 << (i - 1))) {fg = 0; break;}
			if(!fg) continue;
			for(int i = 1; i <= 18; ++i)
				if(S & 1 << (i - 1)) {
					if(S == 1) puts("OK");
					int k = get(S, i);
					if((f[k] ^ 1) && f[S] == 0) pre[S] = k;
					f[S] |= (f[k] ^ 1);
				}
		}
	/*	for(int S = 0; S < (1 << 7); ++S){
			t = S;
			cout<<t << " " << f[S]<<endl;
		}
		writeln(f[1]);*/
	while(T--)
		{
			int x = read();
			if(x > 18) {puts("ysgh"); continue;}
			puts(f[(1 << x) - 1] ? "ysgh" : "cqz");
		}
	}

	return 0;
}
