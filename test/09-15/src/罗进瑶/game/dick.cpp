#include<bits/stdc++.h>

using namespace std;
typedef long long ll;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return w * x;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
}
bitset<10>t;
int work(int x) {
	int res = 0;
	while(x) {
		res ^= x;
		x /= 2;
	}
	return res;
}
int main() {
	int n;
	n = read();
	for(int i = 1; i <= n; ++i) {
		int p=work(i) ^ i;
		t = work(i) ^ i;
		cout<<t<< "  " << i <<" " << p<<" "<< i / (p == 0 ? 1 : p)<<endl;
	}
	return 0;
}
