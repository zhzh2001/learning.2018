#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>

#define Max 20005

using namespace std;

int n,c,k,size,now,maxnum,a[Max],b[Max],ans[Max],last[Max],f[2][Max];

inline void calc(int x,int y){
//	cout<<x<<" "<<y<<endl;
	if(x==1&&y==1){
		return;
	}
	for(int i=0;i<=y;i++)f[0][i]=0;f[1][0]=0;
	for(int i=1;i<=x;i++){
		for(int j=1;j<=y;j++){
			if(f[i&1^1][j]>f[i&1][j-1]){
				f[i&1][j]=f[i&1^1][j]+a[i]*b[j]%c;
				last[j]=1;
			}else{
				f[i&1][j]=f[i&1][j-1]+a[i]*b[j]%c;
				last[j]=0;
			}
		}
	}
//	cout<<f[x&1][y]<<endl;
	ans[++size]=last[y];
	if(ans[size]){
		calc(x-1,y);
	}else{
		calc(x,y-1);
	}
	return;
}

int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	ios::sync_with_stdio(false);
	srand(201811152);
	cin>>n>>c;
	for(int i=1;i<=n;i++)cin>>a[i];
	for(int i=1;i<=n;i++)cin>>b[i];
	if(n<=100){
		calc(n,n);
		for(int i=n*2-2;i>=1;i--){
			if(ans[i]){
				putchar('D');
			}else{
				putchar('R');
			}
		}
	}else{
		int x,y;
		for(int i=1;i<=10000;i++){
			x=1;y=1;
			now=a[x]*b[y]%c;
			for(int j=1;j<=n*2-2;j++){
				if(x==n||y==n){
					if(x==n){
						now+=a[x]*b[y+1]%c;
						y++;
						last[j]=0;
					}else{
						now+=a[x+1]*b[y]%c;
						x++;
						last[j]=1;
					}
					continue;
				}
				k=rand()%100;
				if(a[x+1]+b[y]%c<a[x]+b[y+1]%c){
					if(k%3){
						now+=a[x]*b[y+1]%c;
						y++;
						last[j]=0;
					}else{
						now+=a[x+1]*b[y]%c;
						x++;
						last[j]=1;
					}
				}else{
					if(k%3){
						now+=a[x+1]*b[y]%c;
						x++;
						last[j]=1;
					}else{
						now+=a[x]*b[y+1]%c;
						y++;
						last[j]=0;
					}
				}
			}
			if(now>maxnum){
				maxnum=now;
				for(int j=1;j<=n*2-2;j++){
					ans[j]=last[j];
				}
			}
		}
		for(int i=1;i<=n*2-2;i++){
			if(ans[i]){
				putchar('D');
			}else{
				putchar('R');
			}
		}
	}
	return 0;
}
