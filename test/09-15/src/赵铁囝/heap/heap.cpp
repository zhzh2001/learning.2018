#include <iostream>
#include <cstdio>

using namespace std;

int n,l,r,ans,a[20];
bool vis[20];

inline bool check(){
	a[0]=-1e9;
	if(a[l]!=r)return false;
	for(int i=1;i<=n;i++){
		if(a[i]<a[i/2])return false;
	}
	return true;
}

inline void dfs(int x){
	if(x==0){
//		for(int i=1;i<=n;i++)cout<<a[i]<<" ";cout<<endl;
		if(check())ans++;
		return;
	}
	if(x==l){
		dfs(x-1);
		return;
	}
	for(int i=1;i<=n;i++){
		if(!vis[i]){
			vis[i]=true;
			a[x]=i;
			dfs(x-1);
			vis[i]=false;
		}
	}
	return;
}

int main(){
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>l>>r;
	vis[r]=true;
	a[l]=r;
	dfs(n);
	cout<<ans<<endl;
	return 0;
}
