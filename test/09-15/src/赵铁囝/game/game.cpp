#include <iostream>
#include <cstdio>

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int t,n;

int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	if(read()==2){
		t=read();
		while(t--){
			n=read();
			if(n==1){
				puts("cqz");
			}else{
				puts("ysgh");
			}
		}
	}
	return 0;
}
