#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define maxn 1010
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int n,c,a[maxn],b[maxn],f[51][maxn],nown,nowm,ans[maxn<<1],cnt,tmpn,tmpm;
bool g[51][maxn];
int main()
{
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	nown=nowm=n=read();
	c=read();
	for(int i=1;i<=n;a[i++]=read());
	for(int i=1;i<=n;b[i++]=read());
	while(nown>50)
	{
		memset(f,0,sizeof f);
		memset(g,0,sizeof g);
		for(int i=1;i<=nown;i++)
			for(int j=1;j<=nowm;j++)
			{
				g[i%50][j]=f[(i-1)%50][j]>f[i%50][j-1];
				f[i%50][j]=max(f[(i-1)%50][j],f[i%50][j-1])+a[i]*b[j]%c;
			}
		tmpn=nown;
		tmpm=nowm;
		for(;tmpn>nown-50;)
		{
			ans[++cnt]=g[tmpn%50][tmpm];
			if(g[tmpn%50][tmpm]) tmpn--;
				else tmpm--;
		}
		nown=tmpn;
		nowm=tmpm;
	}
	memset(f,0,sizeof f);
	memset(g,0,sizeof g);
	for(int i=1;i<=nown;i++)
		for(int j=1;j<=nowm;j++)
		{
			g[i][j]=f[i-1][j]>f[i][j-1];
			f[i][j]=max(f[i-1][j],f[i][j-1])+a[i]*b[j]%c;
		}
	tmpn=nown;
	tmpm=nowm;
	for(;tmpn>0&&tmpm>0;)
	{
		ans[++cnt]=g[tmpn][tmpm];
		if(g[tmpn][tmpm]) tmpn--;
			else tmpm--;
	}
	for(int i=cnt-1;i;i--)
		putchar(ans[i]?'D':'R');
	return 0;
}
