#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int n,x,y,ans,vis[maxn],a[maxn];
void dfs(int k)
{
	if(k>n)
	{
		for(int i=1;i<=n;i++)
			if(a[i]<a[i>>1]) return;
		ans++;
		return;
	}
	if(k==x) dfs(k+1);
	else
	{
		for(int i=1;i<=n;i++)
			if(!vis[i]&&i>a[k>>1])
			{
				a[k]=i;
				vis[i]=1;
				dfs(k+1);
				vis[i]=0;
			}
	}
}
signed main()
{
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	n=read();
	x=read();
	y=read();
	vis[y]=1;
	a[x]=y;
	dfs(1);
	write(ans);
	return 0;
}
