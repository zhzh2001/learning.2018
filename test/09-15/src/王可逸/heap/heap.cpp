//21
#include<bits/stdc++.h>
#define int long long
using namespace std;
const int mod = 998244353;
int n, x, y;
int a[1200], ans;
bool vis[1200];
void dfs(int at) {
	if (at == n+1) {
		ans++;
		if(ans==mod) ans=0;
		return;
	}
	if (a[at]) {
		dfs(at + 1);
	}
	for (int i = 1; i<=n; i++) {
		if (vis[i]) continue;
		if(i<a[at>>1]) continue;
		if(a[at<<1]&&i>a[at<<1]) continue;
		if(a[at<<1|1]&&i>a[at<<1|1]) continue;
		a[at] = i;
		vis[i] = true;
		// cout << "a[" << at << "]=" << i << '\n';
		dfs(at + 1);
		vis[i] = false;
		if (at != x) a[at] = 0;
	}
}
signed main() {
	ios::sync_with_stdio(false);
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	cin >> n >> x >> y;
	a[x] = y;
	vis[y] = true;
	dfs(1);
	cout << ans;
	return 0;
}
