//40+?
#pragma GCC optimize(2)
#include<bits/stdc++.h>
#define int long long
using namespace std;
int f[101][101], a[25600], b[25600], n, mod;
bool from[101][101];
char ans[203];
signed main() {
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>mod;
	for (int i = 1; i <= n; i++) {
		cin>>a[i];
		a[i] %= mod;
	}
	for (int i = 1; i <= n; i++) {
		cin>>b[i];
		b[i] %= mod;
	}
	if (n <= 100) {
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++) {
				f[i][j] = a[i] * b[j] % mod;
				if (f[i - 1][j] > f[i][j - 1]) {
					from[i][j] = 1;
					f[i][j] += f[i - 1][j];
				} else {
					from[i][j] = 0;
					f[i][j] += f[i][j - 1];
				}
			}
		int l = n, r = n, cnt = 0;
		while (l != 1 || r != 1) {
			if (from[l][r]) {
				ans[++cnt] = 'D';
				l--;
			} else {
				ans[++cnt] = 'R';
				r--;
			}
		}
		// cout<<f[n][n];
		for (int i = cnt; i >= 1; i--) {
			cout<<ans[i];
		}
	} else {
		int l=1,r=1;
		while(l!=n||r!=n){
			if(a[l+1]*b[r]%mod>a[l]*b[r+1]%mod&&l!=n) {
				cout<<"D";
				l++;	
			} else {
				cout<<"R";
				r++;	
			}
		}
	}
	return 0;
}
