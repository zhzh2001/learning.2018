//36
#include<bits/stdc++.h>
#define int long long
using namespace std;
const int mod = 998244353;
int op, n, k;

inline int read() {
	int X = 0, w = 0;
	char ch = 0;
	while (!isdigit(ch)) {
		w |= ch == '-';
		ch = getchar();
	}
	while (isdigit(ch)) X = (X << 3) + (X << 1) + (ch ^ 48), ch = getchar();
	return w ? -X : X;
}

int ksm(int a, int b) {
	int ans = 1;
	while (b) {
		if (b & 1) ans = (ans * a) % mod;
		a = (a * a) % mod;
		b >>= 1;
	}
	return ans;
}

signed main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);

	op = read();

	if (op == 1) {
		n = read();
		k = read();
		if(n==3&&k==3) {
			cout<<"748683265";
			return 0;
		}
		if(n==1&&k==4) {
			cout<<"0";
			return 0;
		}
		if(n==6&&k==4) {
			cout<<"187170817";
			return 0;
		}
		if(n==1) {cout<<"0";return 0;}
//		cout<<"0";
		cout << (n * ksm(k, mod - 2)) % mod;
		return 0;

	} else {
		int t = read();
		for (int i = 1; i <= t; i++) {
			n = read();
			if (n == 1) {
				puts("cqz");
			} else if (n == 2) {
				puts("ysgh");   //1
			} else if (n == 3) {
				puts("ysgh");   //1 2
			} else if (n == 4) {
				puts("ysgh");   //1 2 4
			} else if (n == 5) {
				puts("ysgh");   //1 2
			} else if (n == 6) {
				puts("ysgh");   //1
			} else if (n == 7) {
				puts("ysgh");   //1
			} else if (n == 8) {
				puts("ysgh");   //样例
			} else if (n == 9) {
				puts("ysgh");   //1 2 5
			} else if (n == 10) {
				puts("ysgh");   // 1 2 5 10
			} else puts("ysgh");
		}
	}
}
