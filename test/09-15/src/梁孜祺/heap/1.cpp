#include<bits/stdc++.h>
#define ll long long
#define N 1005
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
int n, X, Y;
const int MOD = 998244353;
int a[N], ans, f[N][N], tnt[N][N];
int main() {
  n = read(); X = read(); Y = read();
  for (int i = n; i; i--) {
    if (i == X) {
      if (i * 2 > n) {
        for (int j = 1; j <= Y; j++) f[i][Y] = 1;
      }else {
        for (int j = 1; j <= Y; j++) f[i][j] = (1ll * f[i*2][Y+1] * f[i*2+1][Y+1] % MOD - tnt[i*2][Y+1] + MOD) % MOD;
      }
      continue;
    }

    if (i * 2 > n) {
      for (int j = n; j; j--) f[i][j] = f[i][j+1] + 1;
    }

    else {
      if (i * 2 + 1 > n) {
        for (int j = 1; j <= n; j++) f[i][j] = f[i*2][j+1];
      } else {
        for (int j = n; j >= 1; j--) 
          f[i][j] = (1ll * f[i*2][j+1] * f[i*2+1][j+1] % MOD - tnt[i*2][j+1] + MOD + f[i][j+1]) % MOD;
      }
    }
    
    if (i != n && i % 2 == 0) {
      for (int j = n; j; j--)
        tnt[i][j] = (1ll * f[i][j] * f[i+1][j] % MOD + tnt[i][j+1]) % MOD;
    }
  }
  for (int i = 1; i <= 5; i++) {
    for (int j = 1; j <= 5; j++) cout << f[i][j] << " ";
    cout << endl;
  }
  printf("%d\n", f[1][1] - f[1][2]);
  return 0;
}