#include<bits/stdc++.h>
#define ll long long
#define N 1000005
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
int n, X, Y;
const int MOD = 998244353;
int a[N], ans;
inline int check() {
  for (int i = 1; i <= n; i++)
    if (a[i] < a[i/2]) return 0;
  return 1;
}
inline void dfs(int x) {
  if (x == Y) dfs(x+1);
  if (x == n+1) {
    ans += check();
    return;
  }
  for (int i = 1; i <= n; i++)
    if (!a[i] && x > a[i/2]) {
      a[i] = x;
      dfs(x+1);
      a[i] = 0;
    }
}
int main() {
  freopen("heap.in","r",stdin);
  freopen("heap.out","w",stdout);
  n = read(); X = read(); Y = read();
  if (X == 1 && Y != 1 || X != 1 && Y == 1) return puts("0");
  a[X] = Y; a[1] = 1;
  dfs(2);
  printf("%d\n", ans);
  return 0;
}