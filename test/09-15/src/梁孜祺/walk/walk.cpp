#include<stdio.h>
#define ll long long
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
#define N 105
int n, MOD, a[N], b[N], f[105][105], re[105][105];
char c[N];
inline int max(int x, int y) {
  if (x > y) return x;
  return y;
}
int main() {
  freopen("walk.in","r",stdin);
  freopen("walk.out","w",stdout);
  n = read();
  MOD = read();
  for (int i = 1; i <= n; i++) a[i] = read();
  for (int i = 1; i <= n; i++) b[i] = read();
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= n; j++) {
      if (f[i-1][j] > f[i][j-1]) re[i][j] = 0;
      else re[i][j] = 1;
      f[i][j] = max(f[i-1][j], f[i][j-1]) + 1ll*a[i]*b[j]%MOD;
    }
  int i = n, j = n, tot = 0;
  while(1) {
    if (!re[i][j]) {
      c[++tot] = 'D';
      i--;
    } else {
      c[++tot] = 'R';
      j--;
    }
    if (i == 1 && j == 1) break;
  }
  for (int i = tot; i; i--) printf("%c",c[i]);
  return 0;
}