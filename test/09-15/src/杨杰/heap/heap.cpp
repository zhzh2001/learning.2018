#include <cstdio>
using namespace std;
const int N=1005,P=998244353;
int n,x,y,tp,ans;
int siz[N],col[N],vis[N];
#define ls (ts<<1)
#define rs (ts<<1|1)
void dfs(int ts) {
	siz[ts]=1;
	if (ls<=n) dfs(ls),siz[ts]+=siz[ls];
	if (rs<=n) dfs(rs),siz[ts]+=siz[rs];
}
void dfs1(int ts) {
	if (ts>n) {
		ans++;
		return;
	}
	if (ts==x) {
		dfs1(ts+1);
		return;
	}
	for (int i=col[ts>>1]+1;i<=n-siz[ts]+1;i++) {
		if (vis[i]) continue;
		col[ts]=i;vis[i]=1;
		dfs1(ts+1);
		vis[i]=0;
	}
}
int main() {
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	scanf("%d%d%d",&n,&x,&y);
	col[x]=y;vis[y]=1;
	dfs(1);
	dfs1(1);
	printf("%d\n",ans);
}
