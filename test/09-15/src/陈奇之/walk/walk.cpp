#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int maxn = 5005;
int f[14][maxn];
int m = 0;
int path[maxn*2];
int n,A[maxn],B[maxn],C;
bool mark = false;
inline void solve(int l,int r,int limit){
	if(l==r){
		//return ;
		if(r==n){
			++m;
			for(int j=1;j<=limit;++j)f[m][j] = max(f[m-1][j],f[m][j-1])+1ll*A[n]*B[j]%C;
			path[n] = n;
			while(path[n]>1 && (f[m][path[n]-1] + 1ll * A[n] * B[path[n]] % C == f[m][path[n]]))path[n]--;
			--m;
		}
		return ;
	}
	
	++m;
	int mid = (l + r) / 2;
	int *cur1 = f[m-1],*cur2 = f[m];
	int *cur3,*cur4;
	for(register int j=1;j<=limit;++j){
		cur1++;
		if(*cur1 > *cur2) *(cur2+1) = *cur1; else *(cur2+1) = *cur2;
		++cur2;
		*cur2 += 1ll * A[l] * B[j] % C;
	}
	cur3 = A+l+1;
	for(register int i=l+1;i<=mid;++i){
		cur1 = f[m],cur2 = f[m]+1;
		cur4 = B+1;
		for(register int j=1;j<=limit;++j){
			if(*cur1 > *cur2) *cur2 = *cur1;
			*cur2 += 1ll * (*cur3) * (*cur4) % C;
			cur1++;cur2++;cur4++;
		}
		cur3++;
	}//得到mid的答案
	
	solve(mid+1,r,limit);//继续得到
	
	//知道mid+1的哪个位置是最优的
	path[mid] = path[mid+1];
	while(path[mid]>1 && f[m][path[mid]-1] + 1ll * A[mid] * B[path[mid]] % C == f[m][path[mid]])
		path[mid]--;
	if(mark){

		while(path[mid]>1 && f[m][path[mid]-1] + 1ll * A[mid] * B[path[mid]] % C >= f[m][path[mid]]*0.999)
			path[mid]--;
		
	}
	m--;
	solve(l,mid,path[mid]);
}
inline int ran(){
	return rand() << 15 | rand();
}
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	scanf("%d%d",&n,&C);
	for(int i=1;i<=n;++i) scanf("%d",&A[i]);
	for(int i=1;i<=n;++i) scanf("%d",&B[i]);
/*	n = 5000,C = 100000;
	for(int i=1;i<=n;++i) A[i] = ran() % 10000;
	for(int i=1;i<=n;++i) B[i] = ran() % 10000;*/
	for(int i=1;i<=n;++i) f[0][0] = 0;
	if(n>=4000) mark=true;
	m=0;
	path[n] = n;
	solve(1,n,n);
	int x = 1;
	path[n+1] = n;
	for(int i=1;i<=n;++i){
		while(x < path[i+1]){
			printf("R");
			x++;
		}
		if(i!=n)printf("D");
	}
	
//	path[i]表示i-1什么时候向上
	
	return 0;
}

/*

5 2 3 6
4 3 1 2
2 5 4 1
1 6 2 4


5  7  10 16
9  12 13 18
11 17 21 22
   XX
.........

*/
