#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define gc getchar
#define pc putchar
inline ll rd(){
    ll x=0,f=1;char c=gc();
    for(;!isdigit(c);c=gc())if(c=='-')f=-1;
    for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
    return x*f;
}
inline void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
const int maxn = 2e6+233;
const int mod = 998244353;
int f[maxn],g[maxn],size[maxn];
int h[maxn];
int fac[maxn],inv[maxn];
int n,x,y;
void dfs(int u){
	size[u] = 1;
if(2*u<=n){	dfs(2*u);	size[u] += size[2*u];}
if(2*u+1<=n){	dfs(2*u+1);size[u] += size[2*u+1];}

}
inline int qpow(int a,int b){
	int ans = 1;
	for(;b;b>>=1,a=1ll*a*a%mod)
		if(b&1) ans = 1ll * ans * a % mod;
	return ans;
}
void init(int n){
	fac[0] = 1;
	Rep(i,1,n) fac[i] = 1ll * fac[i-1] * i % mod;
	inv[n] = qpow(fac[n],mod-2);
	Dep(i,n-1,0) inv[i] = 1ll * inv[i+1] * (i+1) % mod;
}
inline int C(int n,int m){
	if(n<m) return 0;
	return 1ll * fac[n] * inv[m] % mod * inv[n-m] % mod;
}
inline void add(int &x,int v){
	x+=v;
	if(x>=mod) x-=mod;
}
void calc(int u){
	h[u] = 1;
	if(2*u+1<=n){
		calc(2*u);
		calc(2*u+1);
		h[u] = 1ll * h[2*u] * h[2*u+1] % mod * C(size[2*u]+size[2*u+1],size[2*u]) % mod;
	} else
	if(2*u<=n){
		calc(2*u);
		h[u] = h[2*u];
	} else{
		h[u] = 1;
	}//Ҷ�ӽڵ� 
}
signed main(){
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	n = rd(),x = rd(),y = rd();
	init(n);
	dfs(1);
	calc(1);
	f[y] = 1ll * h[x] * C(n-y,size[x]-1) % mod;
h[n^1] = 1;
	while(x!=1){
		Dep(i,y-1,1) g[i] = (g[i+1] + f[i+1]) >= mod ? (g[i+1]+f[i+1] - mod) : (g[i+1]+f[i+1]);
		Rep(j,1,y)
		if(n-j-size[x]>=size[x/2]-size[x]-1){
			f[j] = 1ll * g[j] * h[x^1] % mod * C(n-j-size[x],size[x/2]-size[x]-1) % mod;
			g[j] = 0;
		} else
		{
			f[j] = 0;
			g[j] = 0;
		}
		x>>=1;
	}
	writeln(f[1]);
	return 0;
}
