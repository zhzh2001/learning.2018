#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define gc getchar
#define pc putchar
inline ll rd(){
    ll x=0,f=1;char c=gc();
    for(;!isdigit(c);c=gc())if(c=='-')f=-1;
    for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
    return x*f;
}
inline void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
namespace Subtask1{
	const int mod = 998244353;
	int n,k;
	inline int qpow(int a,int b){
		int ans = 1;
		for(;b;b>>=1,a=1ll*a*a%mod)
			if(b&1) ans=1ll*ans*a%mod;
		return ans;
	}
	void solve(){
		n = rd(),k = rd();
		if(n==1){
			puts("0");
		} else
		if(n==2){
			puts("0");
		} else
		if(n==3){
			writeln(1ll * qpow(4,mod-2) % mod);
		} else
		if(n==4){
			writeln(1ll * qpow(2,mod-2) % mod);
		} else{
			puts("毒瘤WZP");
		}
	}
}
//最大值不能超过一半 
//枚举第几个超过一半
 
namespace Subtask2{
	void solve(){
		int T = rd();
		while(T--){
			int x = rd();
			if(x == 1){
				puts("cqz");
			} else{
				puts("ysgh");
			}
		}
	}
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int opt = rd();
	if(opt == 2){
		Subtask2::solve();
	} else{
		Subtask1::solve();
	}
}
