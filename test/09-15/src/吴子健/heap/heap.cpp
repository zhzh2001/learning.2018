#include <bits/stdc++.h>
#define ll long long
const int mod=998244353;
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
ll sz[1000],f[1000];
vector<pair<ll,ll> > req;
ll N,x,y,need;
ll __c[1100][1100];
ll tr[1100];
bool used[1100];
ll vnum[1100];
ll ans;
ll C(int a,int b) {
	return __c[b+1][a+1]%mod;
}
int dfs(int pos,int upnum,int still) {
	if(upnum-1<still) return -1;
	if(pos==1) {

		
		req.clear();
		for(int t=x;t;t>>=1) {
			if(t*2<=N&&!tr[t*2]) {
				req.push_back(make_pair(tr[t]+1,sz[t*2]));
			} 
			if(t*2+1<=N&&!tr[t*2+1]) {
				req.push_back(make_pair(tr[t]+1,sz[t*2+1]));
			}
		}
		memset(vnum,0,sizeof(vnum));
		for(int i=0;i<req.size();++i) {
			for(int j=1;j<=N;++j) if(!used[j]) {
				if(j>=req[i].first)vnum[i]++;//,printf("got %d\n",j);
			}
			//printf("need %lld have %lld\n",req[i].second,vnum[i]);
		}
		ll numused=0;
		ll tmpans=1;
		for(int i=0;i<req.size();++i) {
			tmpans*=((ll)f[req[i].second]%mod*C(req[i].second,vnum[i]-numused))%mod;
			numused+=req[i].second;
			tmpans%=mod;
			//printf("tmpans=%lld\n",tmpans);
			//printf("need %d have %d\n",req[i].second,vnum[i]);
		}
		ans+=tmpans;
		ans%=mod;
		//printf("%lld\n",ans);
		return 0;
	}
	for(int i=upnum;i>still;--i) {
		tr[pos]=i;
		used[i]=true;
		dfs(pos>>1,i-1,still-1);
		tr[pos]=0;
		used[i]=false;
	}
}
int main() {
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	tr[1]=1;
	used[1]=true;
	__c[1][1]=1;
	for(int i=2;i<=1000;++i) for(int j=1;j<=i;++j) __c[i][j]=(__c[i-1][j]+__c[i-1][j-1])%mod;
	N=rd(),x=rd(),y=rd();
	//for(int i=1;i<=N;++i) sz[i]=1;
	//for(int i=N;i>=1;--i) sz[i]+=sz[i<<1]+sz[i<<1|1];
	
	f[1]=f[2]=1;f[3]=2;
	for(int i=4;i<=N;++i) {
		memset(sz,0,sizeof sz);
		for(int j=1;j<=i;++j) sz[j]=1;
		for(int j=N;j>=1;--j) sz[j]=sz[j]+sz[j<<1]+sz[j<<1|1];
		//printf("%d %d %d %d\n",sz[2],sz[3],sz[1],f[sz[2]]);
		f[i]=f[sz[2]]%mod*C(sz[2],sz[1]-1)%mod*f[sz[3]];f[i]%=mod;
		//printf("f[%d]=%d\n",i,f[i]);	
	}
	for(int t=x;t;t>>=1,++need);
	//cout<<"Need="<<need<<endl;
	need-=2;
	tr[x]=y;used[y]=true;
		dfs(x>>1,y-1,need);
		printf("%lld\n",ans%mod);
	return 0;
}
