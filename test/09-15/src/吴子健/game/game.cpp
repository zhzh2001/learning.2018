#include <bits/stdc++.h>
const int mod=998244353;
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
int qpow(int a,int b,int md){
	int ans=1;
	for(;b;b>>=1){
		if(b&1)ans=(long long)ans*a%md;
		a=(long long)a*a%md;
	}
	return ans%md;
}
int _Task;
namespace OPT1{
	int N,K;
	void main() {
		N=rd(),K=rd();
		if(N==1||N==2) {
			printf("0\n");
			return ;
		} else {
			
		}
	}	
};
namespace OPT2{
	int ans[300];
	int T,N;
	int f[1<<18];
	void main() {
		T=rd();
		while(T--) {
			N=rd();
			if(N<=15){
				memset(f,-1,sizeof f);
				f[0]=1;
				for(int i=1;i<(1<<N);++i) {
					for(int j=1;j<=N;++j) if((i>>(j-1))&1) {
						int s=i,t=j;
						while(t) {
							if(s&(1<<(t-1))) s^=(1<<(t-1));
							t/=2;
						}
						//printf("%d->%d\n",i,s);
						if(f[s]==0) f[i]=1;
					}
					if(f[i]!=1) f[i]=0;
					//if(i<1024)
					//printf("f[%d]=%d\n",i,f[i]);
					//if(((i+1)-((i+1)&(-i-1))==0)&&f[i]==0) printf("%d\n",i);
				}
				if(f[(1<<N)-1]==1) puts("ysgh");
				else puts("cqz");
			} else {
				puts("ysgh");
			}
			
			//for(int i=0;i<N;++i) printf("f[%d]=%d\n",((1<<i)-1),f[((1<<i)-1)]);
		}
	}	
};
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	/*srand(time(0));
	int tot=0;
	for(int i=1;i<=300000;++i) {
		int a=rand()*rand()%30000,b=rand()%(30000-a),c=rand()%(30000-b-a);
		double sum=a+b+c;
		//a*=3/sum,b*=3/sum,c*=3/sum;
		//printf("%d %d %d\n",a,b,c);
		if(a>=15000||b>=15000||c>=15000) ++tot;
	}
	printf("%d\n",tot);*/
	_Task=rd();
	if(_Task==1) {
		OPT1::main();
	} else {
		OPT2::main();
	}
	return 0;
}
