#include <cstdio>
#include <cstdlib>
#include <cstring>
//#include <iostream>
#define maxx(x,y) ((x)>(y)?(x):(y))
#define ll long long
using namespace std;
inline int rd() {
	int x=0,f=1;
	char ch=getchar();
	for(; ch>'9'||ch<'0'; ch=getchar()) if(ch=='-') f=-1;
	for(; '0'<=ch&&ch<='9'; ch=getchar()) x=x*10+ch-'0';
	return x*f;
}

ll N,C,a[5010],b[5010],maxv;

namespace to_max {
	ll f[2][5010],ans;
	ll get_max() {
		f[1][1]=a[1]*b[1]%C;
		for(int i=1; i<=N; ++i) {
			int now=i&1,pre=(i-1)&1;
			for(int j=1; j<=N; ++j) {
				if(i==1&&j==1) continue;
				f[now][j]=0;
				f[now][j]=maxx(f[pre][j],f[now][j-1])+(a[i]*b[j])%C;
				//printf("f[%d][%d]=%lld\n",i,j,f[now][j]);
			}
		}
		//cerr<<f[N&1][N]<<endl;
		return f[N&1][N];
	}
}

ll f[2][5010],g[2][5010];
char fp[2][5010],gp[2][5010];
char path[5010];int pcnt;
int main() {
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	N=rd(),C=rd();
	//cout<<N<<" "<<C<<endl;
	for(int i=1; i<=N; ++i) a[i]=rd();
	for(int i=1; i<=N; ++i) b[i]=rd();
	maxv=to_max::get_max();
	int nx=N,ny=N;
	for(int i=N; i>=1; --i) {
		memset(f,0,sizeof f);
		for(int k=1; k<=i; ++k) {
			for(int l=1; l<=N; ++l) {
				int now=(k&1),pre=(k-1)&1;
				if(f[pre][l]>f[now][l-1]) f[now][l]=f[pre][l]+(a[k]*b[l])%C,fp[now][l]='D';
				else f[now][l]=f[now][l-1]+(a[k]*b[l])%C,fp[now][l]='R';
				//printf("f[%d][%d]=%lld\n",k,l,f[now][l]);
			}
		}
		int now=i&1,pre=(i+1)&1;
		for(int j=N; j>=1; --j) {
			g[now][j]=0;
			if(g[pre][j]>g[now][j+1]) g[now][j]=g[pre][j]+(a[i]*b[j])%C,gp[now][j]='D';
			else g[now][j]=g[now][j+1]+(a[i]*b[j])%C,gp[now][j]='R';
			//printf("g[%d][%d]=%lld\n",i,j,g[now][j]);
		}
		while(true){
			if(f[nx&1][ny]+g[nx&1][ny]==maxv+a[nx]*b[ny]%C) {
			//	printf("in (%d,%d) %c\n",nx,ny,fp[nx&1][ny]);
				path[++pcnt]=fp[nx&1][ny];
			//	printf("path[%d]=%c\n",pcnt,path[pcnt]);
			}
			if(fp[nx&1][ny]=='R') --ny;
			else {
				--nx;break;
			}
		}
	}
/*	int x=1,y=1;
	ll ans=a[1]*b[1]%C;
	for(int i=pcnt-1;i>=1;--i) {
		if(path[i]=='D') ++x;
		else	++y;
		ans+=(a[x]*b[y])%C;
	}
	printf("%lld\n",maxv);
	printf("%lld\n",ans);*/
	for(int i=pcnt-1;i>=1;--i) printf("%c",path[i]);
	return 0;
}
