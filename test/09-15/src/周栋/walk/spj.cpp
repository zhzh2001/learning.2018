#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<iostream>
using namespace std;
const int N=5005;
int n,c,ans,a[N],b[N];
char ch[N*10];
void Return(double p,string s){
	cout<<s<<' '<<p<<'\n';
	exit(0);
}
int	main(){
	freopen("walk.in","r",stdin);
	freopen("ans.out","w",stdout);
	scanf("%d%d",&n,&c);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	for(int i=1;i<=n;i++)scanf("%d",&b[i]);
	freopen("walk.out","r",stdin);
	scanf("%s",ch);
	int t1=1,t2=1;
	for(int i=0;i<=2*(n-1);i++){
		if(t1>n||t2>n)Return(0,"WA");
		ans+=a[t1]*b[t2]%c;
		if(ch[i]=='D')t1++; else if(ch[i]=='R')t2++; else if(i<2*(n-1))Return(0,"WA");
	}
	int t;
	freopen("right.in","r",stdin);
	scanf("%d",&t);
	if(ans>t)Return(2,"NOIP 1200"); else
	if(ans==t)Return(1,"NOIP AK");
	else if(ans*5>=t*4)Return(0.2,"NOIP 500+");
	else Return(0,"NOIP 1=");
}
