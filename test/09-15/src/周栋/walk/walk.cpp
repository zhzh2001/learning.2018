#include <iostream>
#include <cstdio>
using namespace std;
typedef int ll;
const ll N=5010;
int a[N],b[N],f[2][N],c,n;
string s[N];
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	scanf("%d%d",&n,&c);
	for (ll i=1;i<=n;++i) scanf("%d",&a[i]);
	for (ll i=1;i<=n;++i) scanf("%d",&b[i]);
	f[1][1]=a[1]*b[1]%c;
	for (ll i=1;i<=n;++i)
		for (ll j=1;j<=n;++j){
			if (i>1) f[i&1][j]=0;
			if (i>1&&f[(i-1)&1][j]>f[i&1][j]) f[i&1][j]=f[(i-1)&1][j],s[j]+='D';
			if (j>1&&f[i&1][j-1]>f[i&1][j]) f[i&1][j]=f[i&1][j-1],s[j]=s[j-1]+'R';
			f[i&1][j]+=a[i]*b[j]%c;
		}
	cout<<s[n];
	return 0;
}
