#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
const ll P=998244353,N=1e6+10;
ll n,x,y,fac[N],inv[N],f[N],log2[N],size[N<<1],ins[N];
inline ll pow(ll x,ll y,ll P){
	ll ret=1;
	for (;y;y>>=1,x=x*x%P) if (y&1) ret=ret*x%P;
	return ret;
}
inline ll C(ll n,ll m){return fac[n]*inv[m]%P*inv[n-m]%P;}
inline ll solve(ll k,ll tt){
	if (k*2>n) return 1;
	ll v=size[k*2];
	if (!ins[k]){
		if (f[size[k]]) return f[size[k]];
		return f[size[k]]=C(k-1,v)*solve(k*2,0)%P*solve(k*2+1,0)%P;
	}else{
		if (ins[k]==1&&tt!=1) return 0;
		ll ret=0;
		for (ll i=ins[k];i<=tt;++i){
			if (ins[k*2]) ret+=C(size[k]-i,v-i+1)*solve(k*2,i-1)%P*solve(k*2+1,tt-i)%P;
			if (ins[k*2+1]) ret+=C(size[k]-i,v)*solve(k*2,tt-i)%P*solve(k*2+1,i-1)%P;
			ret%=P;
		}
		return max(1ll,ret);
	}
}
int main(){
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	scanf("%lld%lld%lld",&n,&x,&y);
	for (ll i=1;i<=n;++i) log2[i]=log2[i/2]+1;
	for (ll i=n;i;--i) size[i]=size[i*2]+size[i*2+1]+1;
	for (ll i=x,v=0;i;i>>=1) ins[i]=++v;
	fac[0]=1;for (ll i=1;i<=n;++i) fac[i]=fac[i-1]*i%P;
	inv[n]=pow(fac[n],P-2,P);
	for (ll i=n-1;~i;--i) inv[i]=inv[i+1]*(i+1)%P;
	printf("%lld\n",solve(1,y));
	return 0;
}
/*
20 7 9
56716963
*/
