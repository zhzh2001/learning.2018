#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
LL T,n;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	read();
	T=read();
	while(T-->0){
		n=read();
		if(n==1)puts("cqz");
		else puts("ysgh");
	}
	return 0;
}