#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,x,y,ans,vis[66];
void dfs(int p){
	if(p>n){
		ans++;
		return;
	}
	if(p==x){
		if(vis[y/2]!=0&&vis[y/2]<x){
			vis[y]=x;
			dfs(p+1);
			vis[y]=0;
		}
		return;
	}
	for(int i=1;i<=n;i++)if(!vis[i]&&i!=y&&vis[i/2]){
		vis[i]=p;
		dfs(p+1);
		vis[i]=0;
	}
}
int main(){
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	n=read();
	y=read();
	x=read();
	vis[0]=1;
	dfs(1);
	printf("%d\n",ans);
	return 0;
}