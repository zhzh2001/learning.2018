#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int a[20003],b[20003],n,m,fa[20003],fb[20003];
void work(int x1,int y1,int x2,int y2){
	if(x1==x2){
		for(int i=y1;i<y2;i++)putchar('R');
		return;
	}
	int mid=(x1+x2)/2;
	fa[y1]=a[x1]*b[y1]%m;
	for(int i=y1+1;i<=y2;i++)fa[i]=fa[i-1]+a[x1]*b[i]%m;
	for(int i=x1+1;i<=mid;i++){
		fa[y1]+=a[i]*b[y1]%m;
		for(int j=y1+1;j<=y2;j++){
			fa[j]=max(fa[j-1],fa[j])+a[i]*b[j]%m;
		}
	}
	for(int j=y1;j<=y2;j++)fb[j]=j;
	for(int i=mid+1;i<=x2;i++){
		fa[y1]+=a[i]*b[y1]%m;
		for(int j=y1+1;j<=y2;j++){
			if(fa[j-1]>fa[j]){
				fa[j]=fa[j-1];
				fb[j]=fb[j-1];
			}
			fa[j]+=a[i]*b[j]%m;
		}
	}
	int p=fb[y2];
	work(x1,y1,mid,p);
	putchar('D');
	work(mid+1,p,x2,y2);
}
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++)b[i]=read();
	work(1,1,n,n);
	return 0;
}