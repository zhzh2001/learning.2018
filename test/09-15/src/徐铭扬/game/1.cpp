#include<bits/stdc++.h>
using namespace std;
int T,TT,n;
bool dfs(int x){
	if (!x) return 1;
	for (int i=0;i<n;i++)
		if (x&(1<<i)){
			int t=i+1,tmp=x;
			while (t) tmp^=1<<t-1,t>>=1;
			if (dfs(tmp)) return 1;
		}
	return 0;
}
int main(){
	scanf("%d%d",&TT,&T);
	while (T--){
		scanf("%d",&n);
		puts(dfs((1<<n)-1)?"ysgh":"cqz");
	}
}
