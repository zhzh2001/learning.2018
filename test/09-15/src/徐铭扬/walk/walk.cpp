#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ull;
const int N=5001;
int n,M,i,x,y,xx,a[N],b[N],f[2][N],m,cnt,j;
ull g[2][N],rec[16][N],tmp;
bool ans[N];
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	scanf("%d%d",&n,&M);
	for (i=1;i<=n;i++) scanf("%d",&a[i]);
	for (i=1;i<=n;i++) scanf("%d",&b[i]);
	m=n*2-2;
	f[0][1]=a[1]*b[1]%M;
	for (i=2;i<=n;i++,xx^=1){
		for (x=1,y=i;y;x++,y--){
			if (f[xx][x-1]>f[xx][x]) g[xx^1][x]=g[xx][x-1]<<1;
			else g[xx^1][x]=g[xx][x]<<1|1;
			f[xx^1][x]=max(f[xx][x-1],f[xx][x])+a[x]*b[y]%M;
		}
		if ((2*n-1-i)%64==0){
			for (x=1,y=i;y;x++,y--) rec[cnt][x]=g[xx^1][x];
			cnt++;
		}
	}
	for (i=2;i<=n;i++,xx^=1){
		for (x=i,y=n;x<=n;x++,y--){
			if (f[xx][x-1]>f[xx][x]) g[xx^1][x]=g[xx][x-1]<<1;
			else g[xx^1][x]=g[xx][x]<<1|1;
			f[xx^1][x]=max(f[xx][x-1],f[xx][x])+a[x]*b[y]%M;
		}
		if ((n-i)%64==0){
			for (x=i,y=n;x<=n;x++,y--) rec[cnt][x]=g[xx^1][x];
			cnt++;
		}
	}
	x=n;y=n;
	for (i=cnt-1;i>=0;i--){
		tmp=rec[i][x];
		for (j=0;j<64;j++,ans[m--]=tmp&1,tmp>>=1){
			if (!i && !tmp) break;
			if (tmp&1) y--;
			else x--;
		}
	}
	for (i=1;i<=n*2-2;i++) putchar(ans[i]?'R':'D');
}
