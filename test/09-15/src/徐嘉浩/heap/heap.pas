var n,x,y:longint;
begin
 assign(input,'heap.in');
 assign(output,'heap.out');
 reset(input);
 rewrite(output);
 read(n,x,y);
 if n=1 then if(x=1)and(y=1) then writeln(1) else writeln(0);
 if n=2 then
 begin
  if(x=1)and(y=1) then writeln(1)
  else if(x=2)and(y=2) then writeln(1)
  else writeln(0);
 end;
 if n=3 then
 begin
  if(x=1)and(y=1) then writeln(2)
  else if(x=2)and(y=2)or(x=2)and(y=3)or(x=3)and(y=2)and(x=3)and(y=3)
  then writeln(1)
  else writeln(0);
 end;
 if n=4 then
 begin
  if(x=1)and(y=1) then writeln(3)
  else if(x=2)and(y=2)or(x=4)and(y=4) then writeln(2)
  else if(x=2)and(y=3)or(x=3)and(y=2)or(x=3)and(y=4)or(x=3)and(y=3)
  or(x=4)and(y=3)then writeln(1)
  else writeln(0);
 end;
 if n=5 then
 begin
  if(x=1)and(y=1) then writeln(8)
  else if(x=2)and(y=2) then writeln(6)
  else if(x=4)and(y=4)or(x=4)and(y=5)or(x=5)and(y=4)or(x=5)and(y=5)
  then writeln(3)
  else if(x=2)and(y=3)or(x=3)and(y=3)or(x=4)and(y=3)or(x=5)and(y=3)
  or(x=3)and(y=4)or(x=3)and(y=5)or(x=3)and(y=2) then writeln(2)
  else writeln(0);
 end;
 close(input);
 close(output); 
end.