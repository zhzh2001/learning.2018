#include <bits/stdc++.h>
#define file "lbc"
using namespace std;
typedef long long ll;
int read(){
	char c=getchar();
	int x=0;
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	printf("%d",read()+read());
}
