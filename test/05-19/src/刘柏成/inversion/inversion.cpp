#include <bits/stdc++.h>
#define file "inversion"
using namespace std;
typedef long long ll;
const int mod=1000000007;
int read(){
	char c=getchar();
	int x=0;
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
int a[200002],sum[200002];
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read();
	if (n==1)
		return puts("0"),0;
	bool flag=1,flag2=1;
	for(int i=1;i<=n;i++){
		a[i]=read();
		if (a[i]!=n)
			flag=0;
		if (a[i]<n-1)
			flag2=0;
	}
	if (n==2){
		if (a[1]==1)
			return puts("0"),0;
		else return puts("1"),0;
	}
	if (flag){
		ll ans=(1LL*n*(n-1)/2)%mod;
		ans=ans*ans%mod;
		for(int i=1;i<=n-2;i++)
			ans=ans*i%mod;
		printf("%lld",ans);
		return 0;
	}
	if (flag2){
		ll ans=(1LL*n*(n-1)/2)%mod;
		ans=ans*ans%mod;
		for(int i=1;i<=n-2;i++)
			ans=ans*i%mod;
		ll ans2=(1LL*(n-1)*(n-2)/2)%mod;
		ans2=ans2*ans2%mod;
		for(int i=1;i<=n-3;i++)
			ans2=ans2*i%mod;
		ll fac=1;
		for(int i=1;i<=n-1;i++)
			fac=fac*i%mod;
		for(int i=1;i<=n;i++)
			if (a[i]==n-1)
				ans=(ans-fac*(n-i)-ans2)%mod;
		printf("%lld\n",((ans%mod)+mod)%mod);
		return 0;
	}
	ll ans=0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			for(int k=1;k<=a[i];k++)
				for(int o=1;o<=a[j] && o<k;o++){
					for(int w=1;w<=n-2;w++)
						sum[w]=0;
					for(int w=1;w<=n;w++){
						if (w==i || w==j)
							continue;
						int t=a[w];
						if (t>=k)
							t--;
						if (t>=o)
							t--;
						sum[t]++;
					}
					ll now=1;
					for(int i=n-2;i;i--){
						sum[i]+=sum[i+1];
						if (sum[i]<=n-2-i){
							now=0;
							break;
						}
						now=now*(sum[i]-n+2+i);
						if (now>(1<<25))
							now%=mod;
					}
					ans=(ans+now)%mod;
				}
	printf("%lld",(ans%mod+mod)%mod);
	//fprintf(stderr,"%.5f",(double)clock()/CLOCKS_PER_SEC);
}
