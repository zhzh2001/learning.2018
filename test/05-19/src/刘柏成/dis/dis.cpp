#include <bits/stdc++.h>
#define file "dis"
using namespace std;
typedef long long ll;
const int mod=1000000007;
const int INF=0x3f3f3f3f;
int read(){
	char c=getchar();
	int x=0;
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
const int maxn=300002;
struct segtree{
	int n;
	int tg[maxn*4],ans[maxn*4],mn[maxn*4];
	int a[maxn];
	void build(int o,int l,int r){
		tg[o]=ans[o]=INF;
		if (l==r){
			mn[o]=a[l];
			return;
		}
		int mid=(l+r)/2;
		build(o*2,l,mid);
		build(o*2+1,mid+1,r);
		mn[o]=min(mn[o*2],mn[o*2+1]);
	}
	void init(int n){
		this->n=n;
		build(1,1,n);
	}
	int ul,ur,uv;
	void update(int o,int l,int r){
		if (ul<=l && ur>=r){
			tg[o]=min(tg[o],uv);
			ans[o]=min(ans[o],tg[o]+mn[o]);
			return;
		}
		int mid=(l+r)/2;
		if (ul<=mid)
			update(o*2,l,mid);
		if (ur>mid)
			update(o*2+1,mid+1,r);
		ans[o]=min(min(ans[o*2],ans[o*2+1]),tg[o]+mn[o]);
	}
	void modify(int l,int r,int v){
		ul=l,ur=r,uv=v;
		//fprintf(stderr,"modify %d %d %d\n",l,r,v);
		update(1,1,n);
	}
	int ql,qr;
	int query(int o,int l,int r,int qwq){
		qwq=min(qwq,tg[o]);
		if (ql<=l && qr>=r)
			return min(ans[o],mn[o]+qwq);
		int mid=(l+r)/2,res=INF;
		if (ql<=mid)
			res=min(res,query(o*2,l,mid,qwq));
		if (qr>mid)
			res=min(res,query(o*2+1,mid+1,r,qwq));
		return res;
	}
	int query(int l,int r){
		ql=l,qr=r;
		int ans=query(1,1,n,INF);
		//fprintf(stderr,"query %d %d=%d\n",l,r,ans);
		return ans;
	}
}t;
struct graph{
	int n,m;
	struct edge{
		int to,next;
	}e[maxn*4];
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
};
struct bf_graph : graph{
	bool can[maxn];
	int q[maxn];
	int d[maxn];
	int bfs(int s){
		int l=1,r=1;
		memset(d,-1,(n+1)*4);
		d[s]=0;
		q[1]=s;
		while(l<=r){
			int u=q[l++];
			if (can[u])
				return d[u];
			for(int i=first[u];i;i=e[i].next){
				int v=e[i].to;
				//printf("v=%d\n",v);
				if (d[v]!=-1)
					continue;
				d[v]=d[u]+1;
				q[++r]=v;
			}
		}
		return -1;
	}
}g1;
struct hld_graph :graph{
	int son[maxn],sz[maxn],fa[maxn],dep[maxn],id[maxn],top[maxn],cl;
	void dfs(int u){
		sz[u]=1;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (sz[v])
				continue;
			fa[v]=u;
			dep[v]=dep[u]+1;
			dfs(v);
			sz[u]+=sz[v];
			if (sz[v]>sz[son[u]])
				son[u]=v;
		}
	}
	void dfs2(int u,int tp){
		top[u]=tp;
		id[u]=++cl;
		if (son[u])
			dfs2(son[u],tp);
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (!id[v])
				dfs2(v,v);
		}
	}
	void prepare(){
		dfs(1);
		dfs2(1,1);
		//fprintf(stderr,"WTF\n");
		for(int i=1;i<=n;i++){
			t.a[id[i]]=-2*dep[i];
			//fprintf(stderr,"dep[%d]=%d\n",i,dep[i]);
		}
		//for(int i=1;i<=n;i++)
			//fprintf(stderr,"t.a[%d]=%d\n",i,t.a[i]);
		t.init(n);
		//fprintf(stderr,"WTF\n");
	}
	void update(int u){
		int qwq=dep[u];
		for(;u;u=fa[top[u]])
			t.modify(id[top[u]],id[u],qwq);
	}
	int query(int u){
		int ans=INF,qwq=dep[u];
		for(;u;u=fa[top[u]])
			ans=min(ans,t.query(id[top[u]],id[u])+qwq);
		return ans;
	}
}g2;
int dx[4]={1,0,-1,0};
int dy[4]={0,1,0,-1};
struct point{
	int x,y;
	bool operator <(const point& rhs)const{
		return x==rhs.x?y<rhs.y:x<rhs.x;
	}
	bool operator ==(const point& rhs)const{
		return x==rhs.x && y==rhs.y;
	}
}a[300002];
int n;
int getid(point x){
	int ret=lower_bound(a+1,a+n+1,x)-a;
	return a[ret]==x?ret:-1;
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=g1.n=g2.n=read();
	for(int i=1;i<=n;i++)
		a[i].x=read(),a[i].y=read();
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++){
		//fprintf(stderr,"a[%d]={%d,%d}\n",i,a[i].x,a[i].y);
		for(int j=0;j<4;j++){
			point qwq=(point){a[i].x+dx[j],a[i].y+dy[j]};
			int k=getid(qwq);
			if (k!=-1)
				g1.addedge(i,k),g2.addedge(i,k);
		}
	}
	int q=read();
	if (n<=1000 && q<=1000){
		while(q--){
			int op=read(),x=read(),y=read(),u=getid((point){x,y});
			if (op==1)
				g1.can[u]=1;
			else printf("%d\n",g1.bfs(u));
		}
	}else{
		g2.prepare();
		bool flg=0;
		while(q--){
			int op=read(),x=read(),y=read(),u=getid((point){x,y});
			if (op==1)
				g2.update(u),flg=1;
			else printf("%d\n",flg?g2.query(u):-1);
		}
	}
	//fprintf(stderr,"%.5f",(double)clock()/CLOCKS_PER_SEC);
}
