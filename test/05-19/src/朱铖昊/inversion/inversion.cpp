#include<bits/stdc++.h>
using namespace std;
const int mod=1e9+7;
#define ll long long
const int N=200005;
int a[N],f[N],g[N],n,ans;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline int work()
{
	int ans=1,p=0;
	for (int i=1;i<=n;++i)
	{
		p+=g[i];
		for (int j=1;j<=f[i];++j)
		{
			ans=(ll)ans*(i-p)%mod;
			++p;
		}
	}
	return ans;
}
inline bool check()
{
	for (int i=1;i<=n;++i)
		if (a[i]!=n)
			return false;
	//puts("!");
	return true;
}
inline bool pd()
{
	for (int i=1;i<=n;++i)
		if (f[i]>i)
			return true;
}
inline void lg()
{
	for (int i=1;i<=n;++i)
		f[i]+=f[i-1];
	if (pd())
	{
		//puts("!");
		puts("0");
		return;
	}
	else
	{
		cout<<rand()<<'\n';
	}
}
int main()
{
	srand(time(0));
	freopen("inversion.in","r",stdin);
	freopen("inversion.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
	{
		read(a[i]);
		++f[a[i]];
	}
	if (check())
	{
		int ans=((ll)n*(n-1)/2)%mod;
		ans=(ll)ans*ans%mod;
		for (int i=1;i<=n-2;++i)
			ans=(ll)ans*i%mod;
		cout<<ans;
		return 0;
	}
	if (n>=55)
	{
		lg();
		return 0;
	}
	/*for (int i=n;i>=1;--i)
	{
		int p=((ll)a[i]*(a[i]-1)/2)%mod;
		int tmp1=ask_sum(1,n,1,a[i],1);
		int tmp2=ask_pow(1,n,1,a[i],1);p
		int tmp3=(a[i]==n?0:ask_size(1,n,a[i]+1,n,1));
		(ans+=(((ll)tmp1*x%mod-tmp2+(ll)tmp3*p%mod)%mod+mod)%mod)%=mod
		insert(1,n,1,a[i]);
	}*/
	for (int i=1;i<n;++i)
		for (int j=i+1;j<=n;++j)
		{
			--f[a[i]];
			--f[a[j]];
			for (int k=2;k<=a[i];++k)
			{
				++g[k];
				for (int l=1;l<k&&l<=a[j];++l)
				{
					++g[l];
					(ans+=work())%=mod;
					--g[l];
				}
				--g[k];
			}
			++f[a[i]];
			++f[a[j]];
		}
	cout<<ans;
	return 0;
}
