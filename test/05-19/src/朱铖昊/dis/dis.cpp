#include<bits/stdc++.h>
using namespace std;
#define ll long long
const double pi=acos(-1.0);
const int inf=1e9+7;
const int M=10;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=300005;
int alp[M+5];
set<pair<int,int> > S[M+5];
struct point
{
	int x,y,id;
}a[N];
int s[N],e[N][4];
int q[N],vis[N],dis[N],l,r;
int n,m,opt,x,y;
int f[N];
map<pair<int,int>,int> mp;
inline bool cmp1(point a,point b)
{
	return a.x==b.x?a.y<b.y:a.x<b.x;
}
inline bool cmp2(point a,point b)
{
	return a.y==b.y?a.x<b.x:a.y<b.y;
}
inline void writeln(int x)
{
	if (x==0)
	{
		puts("0");
		return;
	}
	int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
inline void lg()
{
	for (int i=2;i<=M;++i)
		alp[i]=rand()%1000;
	read(m);
	while (m--)
	{
		read(opt);
		read(x);
		read(y);
		int p=mp[make_pair(x,y)];
		if (opt==1)
		{
			for (int i=1;i<=M;++i)
			{
				double q=(double)x*cos((double)alp[i]/1000.0*pi)
						-(double)y*sin((double)alp[i]/1000.0*pi);
				S[i].insert(make_pair(q,p));
			}
		}
		else
		{
			if (S[1].size()==0)
			{
				puts("-1");
				continue;
			}
			int dist=inf;
			for (int i=1;i<=M;++i)
			{
				double q=(double)x*cos((double)alp[i]/1000.0*pi)
						-(double)y*sin((double)alp[i]/1000.0*pi);
				auto it=S[i].lower_bound(make_pair(q,p)),it1=it;
				for (int j=1;j<=10;++j,--it1)
				{
					dist=min(dist,abs(x-a[it1->second].x)+abs(y-a[it1->second].y));
					if (it1==S[i].begin())
						break;
				}
				for (int j=1;j<=10&&it!=S[i].end();++j,++it)
					dist=min(dist,abs(x-a[it->second].x)+abs(y-a[it->second].y));
			}
			writeln(dist);
		}
	}
}
int h[N],fa[20][N];
inline bool check_fang()
{
	int shang=inf,xia=0,zuo=inf,you=0;
	for (int i=1;i<=n;++i)
	{
		shang=min(shang,a[i].x);
		xia=max(xia,a[i].x);
		zuo=min(zuo,a[i].y);
		you=max(you,a[i].y);
	}
	return (ll)(xia-shang+1)*(you-zuo+1)==(ll)n;
}
inline void dfs(int x,int y)
{
	fa[0][x]=y;
	for (int i=1;i<=19;++i)
		fa[i][x]=fa[i-1][fa[i-1][x]];
	for (int i=0;i<s[x];++i)
		if (e[x][i]!=y&&!h[e[x][i]])
		{
			h[e[x][i]]=h[x]+1;
			dfs(e[x][i],x);
		}
}
inline int lca(int x,int y)
{
	if (h[x]<h[y])
		swap(x,y);
	int p=h[x]-h[y];
	for (int i=0;i<=19;++i)
		if ((1<<i)&p)
			x=fa[i][x];
	for (int i=19;i>=0;--i)
		if (fa[i][x]!=fa[i][y])
		{
			x=fa[i][x];
			y=fa[i][y];
		}
	return x==y?x:fa[0][x];
}
inline void tree_lg()
{
	h[1]=1;
	dfs(1,0);
	for (int i=2;i<=M;++i)
		alp[i]=rand()%1000;
	read(m);
	while (m--)
	{
		read(opt);
		read(x);
		read(y);
		int p=mp[make_pair(x,y)];
		if (opt==1)
		{
			for (int i=1;i<=M;++i)
			{
				double q=(double)x*cos((double)alp[i]/1000.0*pi)
						-(double)y*sin((double)alp[i]/1000.0*pi);
				S[i].insert(make_pair(q,p));
			}
		}
		else
		{
			if (S[1].size()==0)
			{
				puts("-1");
				continue;
			}
			int dist=inf;
			for (int i=1;i<=M;++i)
			{
				double q=(double)x*cos((double)alp[i]/1000.0*pi)
						-(double)y*sin((double)alp[i]/1000.0*pi);
				auto it=S[i].lower_bound(make_pair(q,p)),it1=it;
				for (int j=1;j<=5;++j,--it1)
				{
					dist=min(dist,h[p]+h[it1->second]-2*h[lca(p,it1->second)]);
					if (it1==S[i].begin())
						break;
				}
				for (int j=1;j<=5&&it!=S[i].end();++j,++it)
					dist=min(dist,h[p]+h[it->second]-2*h[lca(p,it->second)]);
			}
			writeln(dist);
		}
	}
}
int main()
{
	srand(time(0));
	freopen("dis.in","r",stdin);
	freopen("dis.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
	{
		read(a[i].x);
		read(a[i].y);
		a[i].id=i;
		mp[make_pair(a[i].x,a[i].y)]=i;
	}
	if (check_fang())
	{
		lg();
		return 0;
	}
	sort(a+1,a+1+n,cmp1);
	for (int i=1;i<n;++i)
		if (a[i].x==a[i+1].x&&a[i].y+1==a[i+1].y)
		{
			int x=a[i].id,y=a[i+1].id;
			e[x][s[x]++]=y;
			e[y][s[y]++]=x;
		}
	sort(a+1,a+1+n,cmp2);
	for (int i=1;i<n;++i)
		if (a[i].y==a[i+1].y&&a[i].x+1==a[i+1].x)
		{
			int x=a[i].id,y=a[i+1].id;
			e[x][s[x]++]=y;
			e[y][s[y]++]=x;
		}
	if (n>=5005)
	{
		tree_lg();
		return 0;
	}
	read(m);
	while (m--)
	{
		read(opt);
		read(x);
		read(y);
		int p=mp[make_pair(x,y)];
		if (opt==1)
			f[p]=1;
		else
		{
			if (f[p])
			{
				puts("0");
				continue;
			}
			l=0;
			r=1;
			q[1]=p;
			vis[p]=m;
			dis[p]=0;
			bool b=false;
			while (l!=r)
			{	
				int now=q[++l];
				for (int i=0;i<s[now];++i)
					if (vis[e[now][i]]!=m)
					{
						vis[e[now][i]]=m;
						dis[e[now][i]]=dis[now]+1;
						q[++r]=e[now][i];
						if (f[e[now][i]])
						{
							b=true;
							writeln(dis[e[now][i]]);	
							break;
						}
					}
				if (b)
					break;
			}
			if (!b)
				puts("-1");
		}
	}
	return 0;
}
