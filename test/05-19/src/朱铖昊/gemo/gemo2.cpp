#include<bits/stdc++.h>
using namespace std;
const int N=21;
#define ll long long
const int mod=1e9+7;
int f[N],g[N],e[N][N],s[N],n,m,x,y,ans;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void dfs(int x,int y,int z)
{
	if (x==2)
	{
		for (int i=0;i<=y&&i<=s[1]&&i<=s[2];++i)
		{
			int sum=1,tmp=0;
			f[x]=i;
			for (int j=0;j<i;++j)
				g[j]=0;
			for (int j=1;j<=s[x];++j)
				if (f[e[x][j]]<i)
					++g[f[e[x][j]]];
				else
					tmp+=(f[e[x][j]]!=i);
			for (int j=0;j<i;++j)
				sum=(ll)sum*((1<<g[j])-1)%mod;
			sum=(ll)sum*(1<<tmp)%mod;
			--x;
			int sum1=1,tmp1=0;
			f[x]=i;
			for (int j=0;j<i;++j)
				g[j]=0;
			for (int j=1;j<=s[x];++j)
				if (f[e[x][j]]<i)
					++g[f[e[x][j]]];
				else
					tmp+=(f[e[x][j]]!=i);
			for (int j=0;j<i;++j)
				sum1=(ll)sum1*((1<<g[j])-1)%mod;
			sum1=(ll)sum1*(1<<tmp1)%mod;
			(ans+=(ll)z*sum%mod*sum1%mod)%=mod;
		}
		return;
	}
	for (int i=0;i<=y&&i<=s[x];++i)
	{
		int sum=1,tmp=0;
		f[x]=i;
		for (int j=0;j<i;++j)
			g[j]=0;
		for (int j=1;j<=s[x];++j)
			if (f[e[x][j]]<i)
				++g[f[e[x][j]]];
			else
				tmp+=(f[e[x][j]]!=i);
		for (int j=0;j<i;++j)
			sum=(ll)sum*((1<<g[j])-1)%mod;
		sum=(ll)sum*(1<<tmp)%mod;
		if (sum)
			dfs(x-1,(i==y?y+1:y),(ll)z*sum%mod);
	}
}
int main()
{
	freopen("gemo.in","r",stdin);
	freopen("gemo.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		e[x][++s[x]]=y;
	}
	dfs(n,0,1);
	int sum=1;
	for (int i=1;i<=m;++i)
		sum=sum*2LL%mod;
	ans=(sum-ans+mod)%mod;
	cout<<ans;
	return 0;
}
