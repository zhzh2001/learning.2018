#include<bits/stdc++.h>
using namespace std;
const int N=21;
#define ll long long
const int mod=1e9+7;
int f[N],g[N],e[N][N],s[N],n,m,x,y,ans;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline int work()
{
	int ans=1;
	for (int i=1;i<=n;++i)
	{
		int tmp=0;
		for (int j=0;j<f[i];++j)
			g[j]=0;
		for (int j=1;j<=s[i];++j)
			if (f[e[i][j]]<f[i])
				++g[f[e[i][j]]];
			else
				tmp+=(f[e[i][j]]!=f[i]);
		for (int j=0;j<f[i];++j)
			ans=(ll)ans*((1<<g[j])-1)%mod;
		ans=(ll)ans*(1<<tmp)%mod;
	}
	return ans;
}
inline void dfs(int x,int y)
{
	if (x==2)
	{
		for (int i=0;i<=y&&i<=s[1]&&i<=s[2];++i)
		{
			f[1]=f[2]=i;
			(ans+=work())%=mod;
		}
		return;
	}
	for (int i=0;i<=y&&i<=s[x];++i)
	{
		f[x]=i;
		dfs(x-1,(i==y?y+1:y));
	}
}
int main()
{
	freopen("gemo.in","r",stdin);
	freopen("gemo.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		e[x][++s[x]]=y;
	}
	dfs(n,0);
	int sum=1;
	for (int i=1;i<=m;++i)
		sum=sum*2LL%mod;
	ans=(sum-ans+mod)%mod;
	cout<<ans;
	return 0;
}
