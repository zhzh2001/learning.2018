#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
const int inf = 0x3f3f3f3f,oo = inf;
#define rd() read()
#define mem(x,v) memset(x,v,sizeof(x))
#define IL inline
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
#define pc putchar
char __wzp[1<<15|1],*__S=__wzp+32768;
#define gc() (__S>=__wzp+32768?(__wzp[fread(__wzp,sizeof(char),1<<15,stdin)]=EOF),*((__S=__wzp)++):*(__S++))
IL ll read(){
	RG ll x=0;char f=0;RG char c=gc();
	for(;!isdigit(c);c=gc())f|=(c=='-');
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return f?-x:x;
}
IL void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
IL void writeln(ll x){write(x);puts("");}
pair<int,int> a[300005];
const int dx[4] = {1,0,-1,0};
const int dy[4] = {0,1,0,-1};
bool vis[3005][3005];
int n;
bool check(int x,int y){
	if(x <= 0 || y <= 0 || x >= 999 || y >= 999) return false;
	if(vis[x][y]) return false;
	int tmp = vis[x][y-1] + vis[x-1][y] + vis[x][y+1] + vis[x+1][y];
	if(tmp >= 2) return false;
	return true;
}
int main(){
	srand(time(NULL));
	freopen("dis.in","w",stdout);
	n = 1000;
	writeln(n);
	printf("%d %d\n",500,500);
	a[1] = make_pair(500,500);
	vis[500][500] = 1;
	Rep(i,2,n){
		while(1){
			int id = rand() % (i - 1) + 1;
			int x = a[id] . first,y = a[id] . second;
			int t = rand() % 4;
			for(int d=0;d<4;d++){
				int xx = x + dx[(d+t)%4];
				int yy = y + dy[(d+t)%4];
				if(check(xx,yy)){
					a[i] = make_pair(xx,yy);
					vis[xx][yy] = true;
					write(xx),pc(' '),writeln(yy);
					break;
				}
			}
			if(a[i].first) break;
		}
	}
	int Q;
	Q = 30000;
	writeln(Q);
	Rep(i,1,Q){
		write(rand()%20 == 0),pc(' ');
		int id = rand() % n + 1;
		write(a[id].first),pc(' '),writeln(a[id].second);
	}
	return 0;
}
