#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
const int inf = 0x3f3f3f3f,oo = inf;
#define rd() read()
#define mem(x,v) memset(x,v,sizeof(x))
#define IL inline
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
#define pc putchar
char __wzp[1<<15|1],*__S=__wzp+32768;
#define gc() (__S>=__wzp+32768?(__wzp[fread(__wzp,sizeof(char),1<<15,stdin)]=EOF),*((__S=__wzp)++):*(__S++))
IL ll read(){
	RG ll x=0;char f=0;RG char c=gc();
	for(;!isdigit(c);c=gc())f|=(c=='-');
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return f?-x:x;
}
IL void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
IL void writeln(ll x){write(x);puts("");}
const int dx[4] = {1,0,-1,0};
const int dy[4] = {0,1,0,-1};
const int maxn = 300005;
void __bf();
void __solveTree();
void __solveChangFangXing();
int n,Q;
int x[maxn],y[maxn];
int dsu[maxn];
int find(int x){
	return dsu[x]==x?x:dsu[x]=find(dsu[x]);
}
map<pair<int,int>,int> mp;
int main(){
	freopen("dis.in","r",stdin);
	freopen("dis.out","w",stdout);
	n = rd();
	if(n<=1000) __bf();
	bool SubTree = true;
	Rep(i,1,n) dsu[i] = i;
	Rep(i,1,n){
		x[i] = rd(),y[i] = rd();
		mp[make_pair(x[i],y[i])] = i;
		rep(d,0,4){
			int xx = x[i] + dx[d];
			int yy = y[i] + dy[d];
			int j = mp[make_pair(xx,yy)];
			if(j){
				if(find(i) == find(j)){
					SubTree = false;
//					printf("%d - %d\n",i,j);
				}
				dsu[find(i)] = find(j);
			}
		}
	}
	if(SubTree)  __solveTree();
	if(!SubTree) __solveChangFangXing();
}

int tot = 0;
int minx,miny,maxx,maxy;
struct SegmentTree{
	static const int maxnode = maxn*32;
	int T[maxnode],root[maxnode],lc[maxnode],rc[maxnode];
	void insert(int &o,int l,int r,int y,int v){
		if(!o) o=++tot,T[o]=v;
		T[o] = min(T[o],v);
		if(l==r) return;
		int mid = (l + r) >> 1;
		if(y <= mid) insert(lc[o],l,mid,y,v); else
					 insert(rc[o],mid+1,r,y,v);
	}
	void Insert(int &o,int l,int r,int x,int y,int v){
		if(!o) o=++tot;
		insert(root[o],miny,maxy,y,v);
		if(l==r) return ;
		int mid = (l + r) >> 1;
		if(x <= mid) Insert(lc[o],l,mid,x,y,v); else
					 Insert(rc[o],mid+1,r,x,y,v);
	}
	int query(int o,int l,int r,int x,int y){
		if(!o) return inf;
		if(l==x&&r==y) return T[o];
		int mid = (l + r) >> 1;
		if(y<=mid) return query(lc[o],l,mid,x,y); else
		if(mid+1<=x) return query(rc[o],mid+1,r,x,y); else{
			return min(
				query(lc[o],l,mid,x,mid),
				query(rc[o],mid+1,r,mid+1,y)
			);
		}
	}
	int Query(int o,int l,int r,int Lx,int Rx,int Ly,int Ry){
//		printf("Query(%d %d %d)[%d %d]{%d}\n",o,l,r,Lx,Rx,T[o]);
		if(!o) return inf;
		if(l==Lx&&r==Rx){
			return query(root[o],miny,maxy,Ly,Ry);
		}
		int mid = (l + r) >> 1;
		if(l<=Lx&&Rx<=mid) return Query(lc[o],l,mid,Lx,Rx,Ly,Ry); else
		if(mid+1<=Lx&&Rx<=r) return Query(rc[o],mid+1,r,Lx,Rx,Ly,Ry); else{
			return min(
				Query(lc[o],l,mid,Lx,mid,Ly,Ry),
				Query(rc[o],mid+1,r,mid+1,Rx,Ly,Ry)
			);
		}
	}
}T[4];

void __solveChangFangXing(){
	minx = inf,maxx = -inf;
	miny = inf,maxy = -inf;
	Rep(i,1,n){
		minx = min(minx,x[i]);
		maxx = max(maxx,x[i]);
		miny = min(miny,y[i]);
		maxy = max(maxy,y[i]);
	}
	tot = 1;
	int root = 1;
	Q = rd();
	while(Q--){
		int op = rd();
		if(op == 1){
			int x = rd(),y = rd();
			T[0].Insert(root,minx,maxx,x,y,-x-y);//-x-y最小
			T[1].Insert(root,minx,maxx,x,y,x-y);//x-y最小 
			T[2].Insert(root,minx,maxx,x,y,y-x);//y-x最小
			T[3].Insert(root,minx,maxx,x,y,x+y);//x+y最小 
		} else{
			int x = rd(),y = rd();
			ll ans = inf;
		//	writeln(T[1].Query(root,minx,maxx,x,maxx,miny,y));
			ans = min(ans,(ll)+x+y+T[0].Query(root,minx,maxx,minx,x,miny,y));
			ans = min(ans,(ll)-x+y+T[1].Query(root,minx,maxx,x,maxx,miny,y));
			ans = min(ans,(ll)+x-y+T[2].Query(root,minx,maxx,minx,x,y,maxy));
			ans = min(ans,(ll)-x-y+T[3].Query(root,minx,maxx,x,maxx,y,maxy));
			if(ans >= maxx-minx+maxy-miny){
				puts("-1");
			} else{
				writeln(ans);
			}
		} 
	}
}

//---------------------------------------------
int Dfa[maxn],size[maxn],mx[maxn],mn[maxn];
int sum,rt;
int sdist[20][maxn],deep[maxn];
bool vis[maxn];
vector<int> edge[maxn];
void getroot(int u,int fa){
	size[u]=1;mx[u]=0;
	for(unsigned i=0;i<edge[u].size();++i){
		int v = edge[u][i];
		if(v==fa || vis[v])continue;
		getroot(v,u);
		size[u]+=size[v];
		mx[u] = max(mx[u],size[v]);
	}
	mx[u] = max(mx[u],sum - size[u]);
	if(mx[u]<mx[rt]) rt=u;
}

void dfs(int u,int fa,int dep,int pa){
    sdist[deep[pa]][u] = dep;
	size[u]=1;
	for(unsigned i=0;i<edge[u].size();++i){
		int v = edge[u][i];
		if(v == fa||vis[v]) continue;
		dfs(v,u,dep+1,pa);
		size[u] += size[v];
	}
}

void divide(int u,int tmpd){
	deep[u] = tmpd;
	vis[u] = true;
	for(unsigned i=0;i<edge[u].size();++i){
		int v = edge[u][i];
		if(vis[v]) continue;
		dfs(v,u,1,u);
		sum = size[v];
		rt = 0;
		getroot(v,u);
		Dfa[rt] = u;//divide tree father
		divide(rt,tmpd+1);
	}
}

void build(){
	sum = mx[0] = n;rt = 0;
	getroot(1,0);
	divide(rt,0);
}

void __solveTree(){
	Rep(i,1,n) edge[i] . clear();
	Rep(i,1,n){
		rep(d,0,4){
			int xx = x[i] + dx[d],yy = y[i] + dy[d];
			int j = mp[make_pair(xx,yy)];
			if(j){
				edge[i] . push_back(j);
//				printf("%d->%d\n",i,j);
			}
		}
	}
	build();
//	Rep(i,1,n){
//		printf("Dfa[%d] = %d\n",i,Dfa[i]);
//	}
	Q = rd();
	Rep(i,1,n) mn[i] = -1;
	while(Q--){
		int op = rd();
		if(op == 1){
			int x = rd(),y = rd();
			int id = mp[make_pair(x,y)];
			for(int o = id;o;o = Dfa[o]){
				if(mn[o] == -1 || mn[o] > sdist[deep[o]][id]){
					mn[o] = sdist[deep[o]][id];
//					printf("mn[%d] = %d\n",o,mn[o]);
				}
			}
		} else{
			int x = rd(),y = rd();
			int id = mp[make_pair(x,y)],ans = -1;
			for(int o = id;o;o = Dfa[o]){
				if(mn[o] == -1) continue;
				int tmp = sdist[deep[o]][id] + mn[o];
				if(ans == -1 || tmp < ans)
					ans = tmp;
			}
			writeln(ans);
		}
	}
}









#define vis VVVIIISSS
int vis[1055][1055];
int dist[1055][1055];
pair<int,int> q[1055];
void __bf(){
	mem(vis,0);
	Rep(i,1,n){
		x[i] = rd(),y[i] = rd();
		vis[x[i]][y[i]] = 1;
	}
	Q = rd();
	while(Q--){
		int op = rd();
		if(op == 1){
			int x = rd(),y = rd();
			vis[x][y] = 2;
		} else{
			int xx = rd(),yy = rd(),front,rear;
			Rep(i,1,n) dist[x[i]][y[i]] = -1;
			front = rear = 0;
			q[rear++] = make_pair(xx,yy);
			dist[xx][yy] = 0;
			int ans = -1;
			while(front<rear){
				int ux = q[front] . first;
				int uy = q[front] . second;
				if(vis[ux][uy] == 2){
					ans = dist[ux][uy];
					break;
				}
				front++;
				for(int d=0;d<4;++d){
					int vx = ux + dx[d];
					int vy = uy + dy[d];
					if(vis[vx][vy] && dist[vx][vy] == -1){
						dist[vx][vy] = dist[ux][uy] + 1;
						q[rear++] = make_pair(vx,vy);
					}
				}
			}
			writeln(ans);
		}
		
	}exit(0);
}



