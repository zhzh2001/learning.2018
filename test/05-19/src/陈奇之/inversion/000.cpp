#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
const int inf = 0x3f3f3f3f,oo = inf;
#define rd() read()
#define mem(x,v) memset(x,v,sizeof(x))
#define IL inline
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
#define pc putchar
#define gc getchar
IL ll read(){
	RG ll x=0;char f=0;RG char c=gc();
	for(;!isdigit(c);c=gc())f|=(c=='-');
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return f?-x:x;
}
const int Mod = 1e9+7;
int a[55],cnt[55];
int ans,res,n;
int main(){
	freopen("inversion.in","r",stdin);
	freopen("inversion.out","w",stdout);
	n = rd();
	Rep(i,1,n)
		a[i] = rd();
	ans = 0;
	Rep(i,1,n){
		Rep(j,i+1,n){
			Rep(x,1,a[i]){
				Rep(y,1,min(a[j],x-1)){
					mem(cnt,0);
					Rep(k,1,n){
						if(i==k || j==k) continue;
						int tmp = a[k];
						if(tmp >= x) tmp--;
						if(tmp >= y) tmp--;
						cnt[tmp]++;
					}
					int res = 1;
					Dep(k,n-2,1){
						res = 1ll * res * cnt[k] % Mod;
						cnt[k-1] += cnt[k] - 1;
					}
					printf("(%d[%d] %d[%d]) %d\n",i,x,j,y,res);
					ans = (ans + res) % Mod;
				}
			}
		}
	}
	printf("%d\n",ans);
	return 0;
}
