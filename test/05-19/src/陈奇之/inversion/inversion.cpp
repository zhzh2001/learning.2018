#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
const int inf = 0x3f3f3f3f,oo = inf;
#define rd() read()
#define mem(x,v) memset(x,v,sizeof(x))
#define IL inline
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
#define pc putchar
#define gc getchar
IL ll read(){
	RG ll x=0;char f=0;RG char c=gc();
	for(;!isdigit(c);c=gc())f|=(c=='-');
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return f?-x:x;
}
IL void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
IL void writeln(ll x){write(x);puts("");}
const int Mod = 1e9+7;
int a[55],cnt[55];
int ans,res,n;
int main(){
	freopen("inversion.in","r",stdin);
	freopen("inversion.out","w",stdout);
	n = rd();
	bool Sub6 = true,Sub8 = true;
	Rep(i,1,n){
		a[i] = rd();
		if(a[i]!=n) Sub6 = false;
		if(a[i]!=n-1 && a[i]!=n) Sub8 = false;
	}
	if(Sub6){
		int ans = 1;
		for(int i=3;i<=n;i++) ans = 1ll * ans * i % Mod;
		int res = 1ll * n * (n-1) / 2;
		printf("%d\n",1ll * ans * res % Mod);
		return 0;
	}
	if(Sub8){
		if(n==1){
			puts("0");
			return 0;
		}
		
		int ans = 1;
		for(int i=3;i<=n;i++) ans = 1ll * ans * i % Mod;
		ans = 1ll * ans * n % Mod;
		ans = 1ll * ans * (n-1) % Mod;
		ans = 1ll * ans * ((Mod+1)/2) % Mod;
		
		int fac = 1;
		for(int i=1;i<=n-1;i++) fac = 1ll * fac * i % Mod;
		int cnt = 0;
		Rep(i,1,n)
			if(a[i] == n-1){
				ans = (ans - 1ll * (n-i) * fac % Mod + Mod) % Mod;
				cnt++;
			}
		int tmp = 1;
		for(int i=3;i<=n-1;i++) tmp = 1ll * tmp * i % Mod;
		tmp = 1ll * tmp * (n-1) % Mod;
		tmp = 1ll * tmp * (n-2) % Mod;
		tmp = 1ll * tmp * ((Mod+1)/2) % Mod;
		ans = (ans - 1ll * tmp * cnt % Mod + Mod) % Mod;
		printf("%d\n",ans);
		return 0;
	}
	if(n<=50){
	ans = 0;
	Rep(i,1,n){
		Rep(j,i+1,n){
			Rep(x,1,a[i]){
				Rep(y,1,min(a[j],x-1)){
					mem(cnt,0);
					Rep(k,1,n){
						if(i==k || j==k) continue;
						int tmp = a[k];
						if(tmp >= x) tmp--;
						if(tmp >= y) tmp--;
						cnt[tmp]++;
					}
					int res = 1;
					Dep(k,n-2,1){
						res = 1ll * res * cnt[k] % Mod;
						cnt[k-1] += cnt[k] - 1;
					}
//					printf("(%d[%d] %d[%d]) %d\n",i,x,j,y,res);
					ans = (ans + res) % Mod;
				}
			}
		}
	}
	printf("%d\n",ans);
	}
	 else{
	 	puts("0");
	 }
	return 0;
}

/*
X     X
4 5 5 3 2





*/
