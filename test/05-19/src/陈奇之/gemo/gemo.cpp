#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
const int inf = 0x3f3f3f3f,oo = inf;
#define rd() read()
#define mem(x,v) memset(x,v,sizeof(x))
#define IL inline
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
#define pc putchar
char __wzp[1<<15|1],*__S=__wzp+32768;
#define gc() (__S>=__wzp+32768?(__wzp[fread(__wzp,sizeof(char),1<<15,stdin)]=EOF),*((__S=__wzp)++):*(__S++))
IL ll read(){
	RG ll x=0;char f=0;RG char c=gc();
	for(;!isdigit(c);c=gc())f|=(c=='-');
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return f?-x:x;
}
IL void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
IL void writeln(ll x){write(x);puts("");}
vector<int> edge[25];
int deg[25],vis[25][25],sg[25];
int x[25],y[25],q[25];
int n,m;
int main(){
	freopen("gemo.in","r",stdin);
	freopen("gemo.out","w",stdout);
	n = rd(),m = rd();
	int ans = 0;
	Rep(i,1,m){
		x[i] = rd(),y[i] = rd();
	}
	for(int S=0;S<(1<<m);++S){
		mem(vis,false);
		Rep(i,1,n) edge[i] . clear(),deg[i]=0;
		Rep(i,1,m){
			if(S >> (i-1) & 1){
				edge[y[i]] . push_back(x[i]);
				++deg[x[i]];
			}
		}
		int front=0,rear=0;
		Rep(i,1,n)if(!deg[i])q[rear++]=i;
		while(front<rear){
			int u = q[front++];
			Rep(i,0,n)
				if(!vis[u][i]){
					sg[u]=i;
					break;
				}

			for(unsigned i=0;i<edge[u].size();++i){
				int v = edge[u][i];
				vis[v][sg[u]]=true;
				if(--deg[v] == 0) q[rear++] = v;
			}
		}
		if(sg[1] ^ sg[2]) ans++;
	}
	writeln(ans);
	return 0;
}
