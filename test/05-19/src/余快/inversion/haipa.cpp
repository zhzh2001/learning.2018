#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}
int ans,n,m,a[N],fac[N];
signed main(){
	freopen("inversion.in","r",stdin);freopen("haipa.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) a[i]=read();
	ans=(ll)n*(n-1)/2%mod;fac[0]=1;
	for (int i=1;i<=n;i++) fac[i]=(ll)fac[i-1]*i%mod;
	ans=(ll)ans*fac[n]%mod;
	ll ni=(mod+1)/2;
	ans=(ll)ans*ni%mod;
	int ans2=0;
	n--;
	ans2=(ll)n*(n-1)/2%mod;
	ans2=(ll)ans2*fac[n]%mod;
	ni=(mod+1)/2;
	ans2=(ll)ans2*ni%mod;
	n++;
	int num=0;
	for (int i=1;i<=n;i++){
		if (a[i]==n-1){
			ans-=ans2;ans=(ans+mod)%mod;
			num=(ll)fac[n-1]*(ll)(n-i)%mod;
			ans-=num;ans=(ans+mod)%mod;
		}
	}
	wrn(ans);
	return 0;
}
/*
9
9 8 9 8 9 8 9 9 9
*/

