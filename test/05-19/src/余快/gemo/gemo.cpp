#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}
int n,m,f[105][105],vis[105],x[105],y[105];
int Next[N*2],head[N],to[N*2],nedge,ans;
#define V to[i]
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;}
int dfs2(int x,int y){
	if (x>y) swap(x,y);
	if (f[x][y]>=0) return f[x][y];
	for (int i=head[x];i;i=Next[i]){
		if (!dfs2(V,y)) return f[x][y]=1;
	}
	for (int i=head[y];i;i=Next[i]){
		if (!dfs2(x,V)) return f[x][y]=1;
	}
	return f[x][y]=0;
}
void check(){
	for (int i=1;i<=n;i++) head[i]=0;nedge=0;
	for (int i=1;i<n;i++){
		for (int j=i+1;j<=n;j++){
			f[i][j]=-1;
		}
	}
	for (int i=1;i<=m;i++) if (vis[i]) add(x[i],y[i]);
	ans+=dfs2(1,2);
}
void dfs(int x){
	if (x==m+1){
		check();return;
	}
	vis[x]=1;dfs(x+1);vis[x]=0;dfs(x+1);
}
signed main(){
	freopen("gemo.in","r",stdin);freopen("gemo.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=m;i++){
		x[i]=read();y[i]=read();
	}
	dfs(1);
	wrn(ans);
	return 0;
}
