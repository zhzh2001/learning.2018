#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define mp make_pair
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}
int n,m,dui[N],dis[N],q1,op,x,y,xmax,xmin,ymax,ymin,D,ans,root;
//可能是一个超级大拼接 
map < pair<int,int> ,int  >q;
int Next[N*4],head[N],to[N*4],nedge;
struct xx{
	int x,y;
}c[N];
#define V to[i]
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;}
int f1[4]={0,0,1,-1},f2[4]={1,-1,0,0};
void addnedge(){
	for (int i=1;i<=n;i++){
		for (int j=0;j<4;j++)
		if (q.find(mp(c[i].x+f1[j],c[i].y+f2[j]))!=q.end()){
			add(i,q[mp(c[i].x+f1[j],c[i].y+f2[j])]);
		}
	}
}
void solve1_bfs(int x){
	dui[1]=x;dis[x]=0;
	for (int l=1,r=1;l<=r;l++){
		int t=dui[l];
		for (int i=head[t];i;i=Next[i]){
			if (dis[t]+1<dis[V]){
				dis[V]=dis[t]+1;dui[++r]=V;
			}
		}
	}
}
void solve1(){
	for (int i=1;i<=n;i++) dis[i]=inf;
	for (int i=1;i<=q1;i++){
		op=read();x=read();y=read();
		if (op==1){
			solve1_bfs(q[mp(x,y)]);
		}
		else{
			int t=q[mp(x,y)];
			if (dis[t]==inf) puts("-1");
			else wrn(dis[t]);
		}
	}
}
signed main(){
	freopen("dis.in","r",stdin);freopen("std.out","w",stdout);
	n=read();
	xmax=-inf;xmin=inf;ymax=-inf;ymin=inf;
	for (int i=1;i<=n;i++){
		c[i].x=read();c[i].y=read();
		q[mp(c[i].x,c[i].y)]=i;
	}
	q1=read();
	addnedge();
	if (n<=1000&&q1<=1000){
		solve1();return 0;
	}	
	return 0;
}
/*
6
1 2
1 3
1 1
2 1
2 2
2 3
5
2 1 1
1 1 1
2 2 3
1 2 3
2 1 3
*/
