#include<bits/stdc++.h>
#define ll long long
#define N 300005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define mp make_pair
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}
int n,m,dui[N],dis[N],q1,op,x,y,xmax,xmin,ymax,ymin,D,ans,root,tot,a[N],b[N];
//可能是一个超级大拼接 
map < pair<int,int> ,int  >q;
int Next[N*4],head[N],to[N*4],nedge;
struct xx{
	int x,y;
}c[N];
#define V to[i]
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;}
int f1[4]={0,0,1,-1},f2[4]={1,-1,0,0};
void addnedge(){
	for (int i=1;i<=n;i++){
		for (int j=0;j<4;j++)
		if (q.find(mp(c[i].x+f1[j],c[i].y+f2[j]))!=q.end()){
			add(i,q[mp(c[i].x+f1[j],c[i].y+f2[j])]);
		}
	}
}
void solve1_bfs(int x){
	dui[1]=x;dis[x]=0;
	for (int l=1,r=1;l<=r;l++){
		int t=dui[l];
		for (int i=head[t];i;i=Next[i]){
			if (dis[t]+1<dis[V]){
				dis[V]=dis[t]+1;dui[++r]=V;
			}
		}
	}
}
void solve1(){
	for (int i=1;i<=n;i++) dis[i]=inf;
	for (int i=1;i<=q1;i++){
		op=read();x=read();y=read();
		if (op==1){
			solve1_bfs(q[mp(x,y)]);
		}
		else{
			int t=q[mp(x,y)];
			if (dis[t]==inf) puts("-1");
			else wrn(dis[t]);
		}
	}
}
struct P{
	int d[2],l,r,ma[2],mi[2],num,sum;
	int& operator[](int x){return d[x];}
	P(int x=0,int y=0)
	{l=0,r=0;d[0]=x,d[1]=y;} 
	void init(){for (int i=0;i<2;i++) ma[i]=mi[i]=d[i];}
}p[N];
bool operator <(P a,P b){
	return (a[D]==b[D])?a[D^1]<b[D^1]:a[D]<b[D];
}
int get_dis(P a,P b){
	return abs(a[0]-b[0])+abs(a[1]-b[1]);
}
struct kdtree{
	P t[N],g;
	void change(int x){
		P l=t[t[x].l],r=t[t[x].r];
		for (int i=0;i<2;i++){
			if (t[x].l) t[x].mi[i]=min(t[x].mi[i],l.mi[i]),t[x].ma[i]=max(t[x].ma[i],l.ma[i]);
			if (t[x].r) t[x].mi[i]=min(t[x].mi[i],r.mi[i]),t[x].ma[i]=max(t[x].ma[i],r.ma[i]);
		}
	}
	int getmin(int k,P p){
		int tmp=0;
		for(int i=0;i<2;i++)
			tmp+=max(0,t[k].mi[i]-p[i]);
		for(int i=0;i<2;i++)
			tmp+=max(0,p[i]-t[k].ma[i]);
		return tmp;
	}
	int build(int l,int r,int op){
		D=op;
		int mid=(l+r)>>1;
		nth_element(p+l,p+mid,p+r+1);
		t[mid]=p[mid];t[mid].init();
		if (l<mid) t[mid].l=build(l,mid-1,op^1);
		if (r>mid) t[mid].r=build(mid+1,r,op^1);
		change(mid);
		return mid;
	}
	void query(int x,int op){
		if (!t[x].sum) return;
		if (t[x].num) ans=min(ans,get_dis(g,t[x]));
		int dl=(t[x].l)?getmin(t[x].l,g):inf,dr=(t[x].r)?getmin(t[x].r,g):inf;
		if (dl<dr){
			if (dl<ans) query(t[x].l,op^1);
			if (dr<ans) query(t[x].r,op^1);
		}
		else{
			if (dr<ans) query(t[x].r,op^1);
			if (dl<ans) query(t[x].l,op^1);
		}
	}
	void insert(int x,int op){
		t[x].sum++;
		if (g[1]==t[x][1]&&g[0]==t[x][0]){
			t[x].num++;return;
		}
		D=op;
		if (g<t[x]) insert(t[x].l,op^1);
		else insert(t[x].r,op^1);
	}
	void insert(P x){
		g=x;
		insert(root,0);
	}
	int aquery(P x){//p=0为min,p=1为max 
		g=x;
		ans=inf,query(root,0);
		return ans;
	}
}kd;
void solve2(){
	//kd_tree 势力登场
	for (int i=1;i<=n;i++){
		p[i][0]=c[i].x;p[i][1]=c[i].y;
	}
	int num=0;
	root=kd.build(1,n,0);
	for (int i=1;i<=q1;i++){
	 	op=read();x=read();y=read();
	 	if (op==1){
	 		kd.insert(P{x,y});num++;
		}
		else{
			if (num==0) puts("-1");
			else wrn(kd.aquery(P{x,y}));
		}
	}
}
struct xx1{
	int s,fa,top,sum,dis,num;
}tree[N];
struct tree1{
	int l,r,num,sum,ans,min;
}t[N*4];
void dfs(int x){
	tree[x].sum=1;
	for (int i=head[x];i;i=Next[i]){
		if (to[i]==tree[x].fa) continue;
		tree[to[i]].fa=x;tree[to[i]].dis=tree[x].dis+1;
		dfs(to[i]);
		tree[x].sum+=tree[to[i]].sum;
		if (tree[to[i]].sum>tree[tree[x].s].sum) tree[x].s=to[i];
	}
}
void dfs2(int x){
	tot++;tree[x].num=tot;b[tot]=-tree[x].dis*2;
	if (tree[x].s>0){
	tree[tree[x].s].top=tree[x].top;
	dfs2(tree[x].s);
  }
  for (int i=head[x];i;i=Next[i]){
  	if (to[i]==tree[x].fa||to[i]==tree[x].s) continue;
  	tree[to[i]].top=to[i];
  	dfs2(to[i]);
	}
}
void build(int x,int l,int r){
	t[x].l=l;t[x].r=r;t[x].min=t[x].ans=inf;
	if (l==r){
		t[x].sum=b[l];return;
	}
	int mid=(l+r)>>1;
	build(x*2,l,mid);build(x*2+1,mid+1,r);
	t[x].sum=min(t[x*2].sum,t[x*2+1].sum);
}
//区间取min 
void add(int x,int l,int r,int k){
	if (l==t[x].l&&r==t[x].r){
		t[x].min=min(t[x].min,k);t[x].ans=min(t[x].ans,t[x].sum+t[x].min);
		return;
	}
	int mid=(t[x].l+t[x].r)>>1;
	if (r<=mid)add(x*2,l,r,k);
	else if (l>mid) add(x*2+1,l,r,k);
	else add(x*2,l,mid,k),add(x*2+1,mid+1,r,k);
	t[x].ans=min(t[x].ans,min(t[x*2].ans,t[x*2+1].ans));
}
void _add(int x,int y){
	int z=tree[x].dis;
	while (tree[x].top!=tree[y].top){
	if (tree[tree[x].top].dis<tree[tree[y].top].dis) swap(x,y);
	  add(1,tree[tree[x].top].num,tree[x].num,z);
	  x=tree[tree[x].top].fa;
  }
  if (tree[x].dis>tree[y].dis) swap(x,y);
  add(1,tree[x].num,tree[y].num,z);
}
int find(int x,int l,int r){
	if (l==t[x].l&&r==t[x].r){
		t[x].ans=min(t[x].ans,t[x].min+t[x].sum);
		return t[x].ans;
	}
	if (t[x].min!=inf){
		t[x*2].min=min(t[x*2].min,t[x].min);t[x*2+1].min=min(t[x*2+1].min,t[x].min);
	}
	int mid=(t[x].l+t[x].r)>>1;
	if (r<=mid) return find(x*2,l,r);
	if (l>mid) return find(x*2+1,l,r);
	return min(find(x*2,l,mid),find(x*2+1,mid+1,r));
}
int _find(int x,int y){
	int sum=inf;
	while (tree[x].top!=tree[y].top){
	if (tree[tree[x].top].dis<tree[tree[y].top].dis) swap(x,y);
	  sum=min(sum,find(1,tree[tree[x].top].num,tree[x].num));
	  x=tree[tree[x].top].fa;
    }
    if (tree[x].dis>tree[y].dis) swap(x,y);
    sum=min(sum,find(1,tree[x].num,tree[y].num));
    return sum;
}
void solve3(){
	dfs(1);tree[1].fa=1;tree[1].top=1;dfs2(1);
	build(1,1,n);
	int num=0;
	for (int i=1;i<=q1;i++){
		op=read();x=read();y=read();
		if (op==1){
			_add(q[mp(x,y)],1);num++;
		}
		else{
			if (num==0){
				puts("-1");continue;
			}
			int t=q[mp(x,y)];
			wrn(tree[t].dis+_find(t,1));
		}
	}
}
signed main(){
	freopen("dis.in","r",stdin);freopen("dis.out","w",stdout);
	n=read();
	xmax=-inf;xmin=inf;ymax=-inf;ymin=inf;
	for (int i=1;i<=n;i++){
		c[i].x=read();c[i].y=read();
		q[mp(c[i].x,c[i].y)]=i;
		xmax=max(xmax,c[i].x);xmin=min(xmin,c[i].x);
		ymax=max(ymax,c[i].y);ymin=min(ymin,c[i].y);
	}
	q1=read();
	ll hai=(ll)(xmax-xmin+1)*(ll)(ymax-ymin+1);
	if (hai==n){
		solve2();
		return 0;
	}
	addnedge();
	if (n<=1000&&q1<=1000){
		solve1();return 0;
	}
	if (nedge==(n-1)*2){
		solve3();
		return 0;
	}
	puts("QAQ");
	return 0;
}
/*
6
1 2
1 3
1 1
2 1
2 2
2 3
5
2 1 1
1 1 1
2 2 3
1 2 3
2 1 3
*/
