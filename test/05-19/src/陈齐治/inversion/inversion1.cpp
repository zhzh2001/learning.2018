#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=21,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void A(int&x,int y){(x+=y)%=P;}
int n,a[N],b[N],m,f[N][1<<N],ff[N][1<<N],g[N][N],gg[N][N];
int main(){
	freopen("inversion.in","r",stdin);
	freopen("inversion.out","w",stdout);
	int i,j,k,c,t;
	n=read();m=1<<n;
	for(i=1;i<=n;i++)a[i]=read(),b[i]=1<<(i-1);
	f[n][0]=1;
	for(i=n;i;i--)
		for(k=1;k<=n;k++)if(a[k]>=i)
			for(j=0;j<m;j++)if(f[i][j]&&((j|b[k])>j)){
				A(f[i-1][j|b[k]],f[i][j]);
				A(ff[i-1][j|b[k]],ff[i][j]);
				t=0;
				for(c=k+1;c<=n;c++)
					t+=((j|b[c])>j);
				A(ff[i-1][j|b[k]],1ll*f[i][j]*t%P);
				A(g[k][i],f[i][j]);
				A(gg[k][i],1ll*f[i][j]*t%P);
			}
	printf("%d\n",ff[0][m-1]);
	for(i=n;i;i--){
		m=0;
		for(k=1;k<=n;k++)A(m,g[k][i]);
		printf("%d %d ",i,m);
		m=0;
		for(k=1;k<=n;k++)A(m,a[k]>=i);
		printf("%d\n",m);
		for(k=1;k<=n;k++)printf("%d ",g[k][i]);puts("");
		for(k=1;k<=n;k++)printf("%d ",gg[k][i]);puts("");
	}
	return 0;
}