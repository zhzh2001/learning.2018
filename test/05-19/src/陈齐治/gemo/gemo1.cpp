#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
int n,m,X[N],Y[N],sg[N],t,v[N],h[N],ne[N],to[N],f[N],L,ans;
inline void addl(int x,int y){
	ne[++L]=h[x];h[x]=L;to[L]=y;
}
void solve(){
	int x,k;
	for(x=n;x;x--){
		t++;
		for(k=h[x];k;k=ne[k])
		if(f[k])v[sg[to[k]]]=t;
		for(k=0;v[k]==t;k++);
		sg[x]=k;
	}if(sg[1]^sg[2])ans=(ans+1)%P;
}
void dfs(int x){
	if(x>m){solve();return;}
	f[x]=1;dfs(x+1);f[x]=0;dfs(x+1);
}
int main(){
	freopen("gemo.in","r",stdin);
	freopen("gemo.out","w",stdout);
	int i;
	n=read();m=read();
	if(m>20){puts("443787881");return 0;}
	for(i=1;i<=m;i++){
		X[i]=read();Y[i]=read();
		addl(X[i],Y[i]);
	}
	dfs(1);printf("%d\n",ans);
	return 0;
}
