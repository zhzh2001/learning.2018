#include<bits/stdc++.h>
using namespace std;
const int N=300005;
int a[10],num,zz[N*4],fi[N*4],sum1[N],sum2[N],ne[N*4],f[N],x[N],y[N],xx,flag[N],opt,yy,n,m;
struct zz
{
	int x,t;
}b[N*4],now;
void jb(int x,int y)
{
	ne[++num]=fi[x];
	fi[x]=num;
	zz[num]=y;
}
int read()
{
	int x=0;char c;
	for (;c<'0'||c>'9';c=getchar());
	for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
	return x;
}
void write(int x)
{
	if (x==-1)
	 {
	 	puts("-1");
	 	return;
	 }
	int len=0;
	while (x)a[++len]=x%10,x/=10;
	for (int i=len;i;i--)putchar(a[i]+48);
	puts("");
}
void doit1()
{
	for (int i=1;i<=n;i++)
	 for (int j=1;j<=n;j++)
	  if (abs(x[i]-x[j])+abs(y[i]-y[j])==1)jb(i,j);
	scanf("%d",&m);
	while (m--)
	 {
	 	scanf("%d%d%d",&opt,&xx,&yy);
	 	int k=0;
	 	for (int i=1;i<=n;i++)
	 	 if (x[i]==xx&&y[i]==yy)k=i;
	 	if (opt==1)flag[k]=1;
	 	else
	 	 {
			memset(f,0,sizeof f);
			f[k]=1;
			int l=0,r=1,ans=-1;
			b[0].x=k;b[0].t=0;
			while (l<r)
			 {
				now=b[l++];
			 	if (flag[now.x])
			 	 {
			 	 	ans=now.t;
			 	 	break;
				 }
				for (int i=fi[now.x];i;i=ne[i])
				 if (!f[zz[i]])
				  {
				  	b[r].x=zz[i];
				  	b[r++].t=now.t+1;
				  	f[zz[i]]=1;
				  } 
			 } 
			write(ans);			 
		 }
	 }  
}
void insert2(int x)
{
	for (;x;x-=x&-x)sum2[x]++;
}
void insert1(int x)
{
	for (;x<=n;x+=x&-x)sum1[x]++;
}
int ask1(int x)
{
	int ans=0;
	for (;x;x-=x&-x)ans+=sum1[x];
	return ans;
}
int ask2(int x)
{
	int ans=0;
	for (;x<=n;x+=x&-x)ans+=sum2[x];
	return ans;
}
void doit2()
{
	scanf("%d",&m);
	int Minx=1e9,Miny=1e9;
	for (int i=1;i<=n;i++)
	 Minx=min(Minx,x[i]),Miny=min(Miny,y[i]);
	while (m--)
	 {
	 	int k;
	 	scanf("%d%d%d",&opt,&xx,&yy);
	 	if (x[1]==x[2])k=yy-Miny+1;
	 	else k=xx-Minx+1;
	 	if (opt==1)
	 	 {
	 	 	insert1(k);
	 	 	insert2(k);
		 }
		else
		 {
		 	int ans=1e9;
		 	if (ask1(n)-ask1(k-1)!=0)
		 	 {
		 	 	int num1=ask1(k-1),l=k,r=n;
		 	 	while (l<r)
		 	 	 {
		 	 		int mid=(l+r)/2;
		 	 		if (ask1(mid)==num1)l=mid+1;
		 	 		else r=mid;
				 }
				ans=min(ans,r-k);
			 }
			if (ask2(1)-ask2(k+1)!=0)
			 {
		 	 	int num1=ask2(k+1),l=1,r=k;
		 	 	while (l<r)
		 	 	 {
		 	 		int mid=(l+r+1)/2;
		 	 		if (ask2(mid)==num1)r=mid-1;
		 	 		else l=mid;
				 }				 
				ans=min(ans,k-r);			 	
			 }
			if (ans==1e9)ans=-1; 
			write(ans);
		 }
	 }
}
int main()
{
	freopen("dis.in","r",stdin);
	freopen("dis.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)x[i]=read(),y[i]=read();
	if (n<=1000)doit1();
	else doit2();
	return 0;
}
