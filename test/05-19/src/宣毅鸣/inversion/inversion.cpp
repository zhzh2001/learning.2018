#include<bits/stdc++.h>
using namespace std;
const int N=200005,M=1e9+7;
typedef long long ll;
ll ans;
int a[N],ff[N],f[500],n;
void doit1()
{
	for (int i=1;i<n;i++)
	 for (int j=i+1;j<=n;j++)
	  for (int k=1;k<=a[j]&&k<a[i];k++)
	   for (int l=k+1;l<=a[i];l++)
	    {
	    	memset(f,0,sizeof f);
	    	for (int x=1;x<=n;x++)
	    	 if (x!=i&&x!=j)f[a[x]]++;
	    	for (int x=n-1;x;x--)
	    	 f[x]+=f[x+1];
	    	int kk=0;ll sum=1; 
	    	for (int x=n;x;x--)
			 {
			 	if (x==l||x==k)continue;
			 	sum*=(f[x]-kk);sum%=M;
			 	kk++;
			 } 
			(ans+=sum)%=M; 
		}
	printf("%lld",ans);		
}
ll ksm(ll x,ll y)
{
	if (!y)return 1;
	ll z=ksm(x,y/2);
	z*=z;z%=M;
	if (y%2==1)z*=x;
	return z%M;
}
void doit2()
{
	ll ans1=1,ans2=1;
	n--;
	for (int i=1;i<=n;i++)
	 ans1*=i,ans1%=M,ans2*=i,ans2%=M;
	ans1*=n;ans1%=M;
	ans1*=(n-1);ans1%=M; 
	ans1*=ksm(4,M-2);ans1%=M;
	n++;
	for (int i=1;i<=n;i++)
	 if (a[i]==n)ans+=ans1+(n-i)*ans2,ans%=M;
	printf("%lld",ans); 
}
int cmp(int x,int y)
{
	return a[x]>a[y];
}
void doit4()
{
	puts("0");
}
int main()
{
	freopen("inversion.in","r",stdin);
	freopen("inversion.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)scanf("%d",&a[i]);
	for (int i=1;i<=n;i++)
	 if (a[i]<n-1)
	  {
	  	if (n<=100)doit1();
	  	else doit4();
	  	return 0;
	  }
	for (int i=1;i<=n;i++)
	 if (a[i]==n-1)
	  {
	  	doit2();
	  	return 0;
	  }
	ans=1;  
	for (int i=1;i<=n;i++)
	 ans*=i,ans%=M;
	ans*=n;ans%=M;
	ans*=(n-1);ans%=M; 
	ans*=ksm(4,M-2);ans%=M;
	printf("%lld",ans);   
	return 0;
}
