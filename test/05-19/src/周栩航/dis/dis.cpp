#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define IT set<int> :: iterator 
using namespace std; 
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c)){if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

int dis[1011][1011],vis[1011][1011],n;
bool hav[1011][1011],tre[1011][1011];
int dx[]={0,1,0,-1,0};
int dy[]={0,0,1,0,-1};
int tim;
inline void Bfs(int x,int y)
{
	if(tre[x][y]){puts("0");return;}
	++tim;
	vis[x][y]=tim;
	queue<int> qx,qy;
	qx.push(x);qy.push(y);dis[x][y]=0;
	while(!qx.empty())
	{
		int tx=qx.front(),ty=qy.front();
		qx.pop();qy.pop();
		For(dir,1,4)
		{
			int nx=tx+dx[dir],ny=ty+dy[dir];
			if(!hav[nx][ny])	continue;
			if(vis[nx][ny]==tim)	continue;
			dis[nx][ny]=dis[tx][ty]+1;
			if(tre[nx][ny])	{writeln(dis[nx][ny]);return;}
			vis[nx][ny]=tim;
			qx.push(nx);qy.push(ny);
		}
	}
	puts("-1");
}
set<int> S;
int x[500001],y[500001],flag;
inline void main2()
{
	For(i,1,n)	x[i]=read(),y[i]=read();
	bool flag=0;
	if(x[1]!=x[2])	{For(i,1,n)	swap(x[i],y[i]);flag=1;}
	int Q=read();
	S.insert(1e9);S.insert(-1e9);
	For(i,1,Q)
	{
		int op=read(),x=read(),y=read();
		int ans=1e9;
		if(flag)	swap(x,y);
		if(op==1)	S.insert(y);
			else	
			{
				IT it=S.lower_bound(y);
				if(*it!=-1e9||*it!=1e9)	ans=*it-y;
				if(*it!=-1e9)	it--;
				if(*it!=-1e9||*it!=1e9)	ans=min(ans,y-*it);
				if(ans==1e9)	ans=-1;
				writeln(ans);
			}
	}
	exit(0);
}
int main()
{
	freopen("dis.in","r",stdin);freopen("dis.out","w",stdout);
	n=read();
	if(n>1000)	main2();
	int x,y;
	For(i,1,n)	x=read(),y=read(),hav[x][y]=1;
	int Q=read();
	For(i,1,Q)
	{
		int op=read(),x=read(),y=read();
		if(op==1)	tre[x][y]=1;
		else	Bfs(x,y);
	}
}
/*
10
1 1
1 2 
1 3 
1 4 
1 5 
1 6
1 7
1 8
1 9
1 10
10
1 1 3
2 1 5
*/