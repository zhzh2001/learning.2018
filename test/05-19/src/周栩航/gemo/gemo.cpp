#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define IT set<int> :: iterator 
using namespace std; 
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c)){if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=16;
int n,m,edg[N][1<<N],dp[1<<N];
int mo=1e9+7;
inline int ksm(int x,int y){int sum=1;for(;y;y/=2,x=1LL*x*x%mo)	if(y&1)	sum=1LL*sum*x%mo;return sum;}
int main()
{
	freopen("gemo.in","r",stdin);freopen("gemo.out","w",stdout);
	n=read();m=read();
	For(i,1,m)
	{
		int x=read(),y=read();
		edg[x][1<<y-1]++;
	}
	int mx=(1<<n)-1;
	dp[0]=1;
	For(i,1,n)	For(j,0,mx)	edg[i][j]=edg[i][j-(j&-j)]+edg[i][j&-j];
	For(zt,1,mx)
	{
		int f1=zt&1,f2=zt>>1&1;
		if((f1^f2))	continue;
		dp[zt]=1;
		for(int alr=(zt-1)&zt;alr;alr=(alr-1)&zt)
		{
			int f11=alr&1,f22=alr>>1&1;
			//cout<<zt<<' '<<alr<<' '<<f11<<' '<<f22<<endl;
			if(f11^f22)	continue;
			int res=zt^alr;
			int tmp=1;
			For(poi,1,n)	
				if((1<<poi-1)&(res))	tmp=1LL*tmp*((1<<edg[poi][alr])-1)%mo;
					else	if((1<<poi-1)&(alr))	tmp=1LL*tmp*((1<<edg[poi][res]))%mo;
			dp[zt]=(1LL*dp[zt]+1LL*dp[res]*tmp)%mo;
			//cout<<zt<<' '<<dp[zt]<<endl;
		}
	}
	writeln((ksm(2,m)-dp[mx]+mo)%mo);
}
