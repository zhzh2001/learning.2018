#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std; 
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c)){if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=2001;
int poi[N],nxt[N],head[N],sg[21][21],in[N],ans,n,m,use[N],cnt;
struct node{int x,y;}e[N];
inline void Add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;}
inline bool SG(int p1,int p2)
{
	if(!in[p1]&&!in[p2])	return 0;
	if(sg[p1][p2]!=-1)	return sg[p1][p2];
	bool ok=0;
	for(int i=head[p1];i;i=nxt[i])
		ok|=(!SG(poi[i],p2));
	for(int i=head[p2];i;i=nxt[i])
		ok|=(!SG(p1,poi[i]));
	//cout<<p1<<' '<<p2<<' '<<ok<<endl;
	return sg[p1][p2]=ok;
}
inline void Solve()
{
	For(i,1,n)	head[i]=in[i]=0;cnt=0;
	For(i,1,m)	if(use[i])	Add(e[i].x,e[i].y),in[e[i].x]++;
	memset(sg,-1,sizeof sg);
	if(SG(1,2))	ans++;//else {For(i,1,m)	cout<<use[i]<<endl;cout<<endl;}
}
inline void Dfs(int x)
{
	if(x==m+1)	{Solve();return;}
	use[x]=1;Dfs(x+1);use[x]=0;Dfs(x+1);
}
int main()
{
	freopen("gemo.in","r",stdin);
	n=read();m=read();
	For(i,1,m)	e[i].x=read(),e[i].y=read();
	//use[2]=use[3]=1;Solve();
	Dfs(1);
	writeln(ans);
}