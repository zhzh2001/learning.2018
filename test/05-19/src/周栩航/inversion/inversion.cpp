#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
//#define int ll
using namespace std; 
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c)){if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

int q[20001],top,ans,a[300001];
ll n,mo=1e9+7;
inline ll Solve(ll x,ll y)
{
	if(x<0||y<0)	return 0;
	ll nn=x+y;
	ll tmp=1;
	For(i,1,nn-1)	tmp=tmp*i%mo;	
	tmp=x*tmp%mo;
	return tmp;
}
inline void main2()
{
	ll sum1=0,tot=0,x=0,y=0;
	Dow(i,1,n)
	{
		if(a[i]==n)	sum1+=tot,x++,sum1%=mo;
			else	tot++,y++;
	}
	ll ans=0,sum2=x*y-sum1;
	sum2=(sum2%mo+mo)%mo;
	
	ans=(ans+(x*(x-1)/2)%mo*(n-1)%mo*Solve(x-2+y,0)%mo)%mo;
	ans=(ans+((x*(x-1)/2)%mo*(((n-2)*(n-1)/2)%mo))%mo*Solve(x-2,y)%mo)%mo;

	
	ans=(ans+sum1*(n-1)%mo*Solve(x-2+y,0)%mo)%mo;
	ans=(ans+sum1*(((n-2)*(n-1)/2)%mo)%mo*Solve(x-1,y-1)%mo)%mo;
	
	ans=(ans+sum2*(((n-2)*(n-1)/2)%mo)%mo*Solve(x-1,y-1)%mo)%mo;
	
	ans=(ans+((y*(y-1)/2)%mo*(((n-2)*(n-1)/2)%mo))%mo*Solve(x,y-2)%mo)%mo;
	writeln(ans);
	exit(0);
}
signed main()
{
	freopen("inversion.in","r",stdin);
	freopen("inversion.out","w",stdout);
	n=read();For(i,1,n)	a[i]=read();
	if(n>=50)
		main2();
	
	For(i,1,n)	For(j,i+1,n)
	{
		For(t1,1,a[i])	For(t2,1,min(t1-1,(int)a[j]))
		{
			top=0;
			For(tmp,1,n)	if(tmp!=i&&tmp!=j)	
			{
				if(a[tmp]>=t1)	q[++top]=a[tmp]-2;
					else	if(a[tmp]>=t2)	q[++top]=a[tmp]-1;	else	q[++top]=a[tmp];
			}
			sort(q+1,q+top+1);
			int tans=1;
			For(tmp,1,n-2)	tans=1LL*tans*(q[tmp]-tmp+1)%mo;
			ans=(ans+tans);if(ans>=mo)	ans-=mo;
		}
	}
	writeln(ans);
}
/*
10
9 10 10 9 10 10 10 9 10 10
*/