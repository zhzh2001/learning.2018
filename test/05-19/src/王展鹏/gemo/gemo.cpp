#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=15,mod=1000000007;
int n,m,tong[N+1];
vector<int> e[N];
map<vector<int>,int>  M[N];
int dfs(int p,vector<int> v){
	if(p<0)return v[0]==v[1];
	if(p){if(M[p].count(v))return M[p][v];}
	int zs=e[p].size(),ans=0;
	memset(tong,0,sizeof(tong));
	for(unsigned j=0;j<e[p].size();j++){
		tong[v[e[p][j]]]++;
	}
	int ycl[N+1];
	ycl[n-p]=0;
	for(int j=0;j<n-p;j++){
		ycl[j]=1;
		for(int k=0;k<j;k++)ycl[j]=(ll)ycl[j]*((1<<tong[k])-1)%mod;
		for(int k=j;k<n-p;k++)ycl[j]=((ll)ycl[j]<<tong[k])%mod;
		//cout<<p<<" "<<v[0]<<" "<<v[1]<<" "<<v[2]<<" "<<j<<" "<<ycl[j]<<endl;
	}
	for(int j=0;j<n-p;j++)if(ycl[j]-ycl[j+1]){v[p]=j; ans=(ans+(ll)(ycl[j]-ycl[j+1])*dfs(p-1,v))%mod;}
	v[p]=0;
	//cout<<p<<" "<<v[0]<<" "<<v[1]<<" "<<v[2]<<" "<<ans<<endl;
	if(p)M[p][v]=ans;
	return ans;
}
int main(){
	freopen("gemo.in","r",stdin); freopen("gemo.out","w",stdout);
	n=read(); m=read();
	vector<int> v;
	v.resize(n); v[n-1]=0;
	for(int i=1;i<=m;i++){
		int u=read()-1,v=read()-1;
		e[u].push_back(v); 
	}
	int sum=1;
	for(int i=0;i<m;i++)sum=sum*2%mod;
	cout<<(sum+mod-dfs(n-2,v))%mod<<endl;
}
/*

*/

