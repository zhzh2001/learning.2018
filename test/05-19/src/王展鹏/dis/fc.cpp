#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=300005;
int n,x[N],y[N],mnx,mny,mxx,mxy;
#define zb(i,j) (i-1)*mxy+j
#define lowbit(i) i&-i
struct BIT{
	int c[N];
	void change(int a,int b,int x){
		for(int i=a;i<=mxx;i+=lowbit(i)){
			for(int j=b;j<=mxy;j+=lowbit(j)){
				c[zb(i,j)]=min(c[zb(i,j)],x);
			}
		}
	}
	int ask(int a,int b){
		int ans=1e9;
		for(int i=a;i;i-=lowbit(i)){
			for(int j=b;j;j-=lowbit(j)){
				ans=min(ans,c[zb(i,j)]);
			}
		}
		return ans;
	}
}T[4];
int vvv[N];
vector<int> v[N];
map<int,int> M[N];
inline bool find(int a,int b){
	return M[a].count(b);
}
int calc(int s){
	int l=0,r=0; static int q[N],bs[N],vis[N];
	for(int i=1;i<=n;i++)vis[i]=0;
	q[++r]=s;
	if(vvv[s])return 0;
	while(l<r){
		int t=q[++l]; if(vvv[t])return bs[l];
		for(unsigned i=0;i<v[t].size();i++)if(!vis[v[t][i]]){q[++r]=v[t][i]; vis[v[t][i]]=1; bs[r]=bs[l]+1;}
	}
	return -1;
}
const int K=20;
int in[N],out[N],tong[N],dep[N],ti,dp[N][K];
void dfs(int p,int fa){
	in[p]=++ti; dp[p][0]=fa; dep[p]=dep[fa]+1; tong[ti]=p;
	for(unsigned i=0;i<v[p].size();i++)if(v[p][i]!=fa){
		dfs(v[p][i],p);
	}
	out[p]=ti;
}

set<int> s;
#define fa(a,b) (in[a]<=in[b]&&out[a]>=out[b])
inline int lca(int a,int b){
	if(fa(a,b))return a; else if(fa(b,a))return b;
	for(int i=K-1;i>=0;i--)if(dp[a][i]&&!fa(dp[a][i],b))a=dp[a][i];
	//cout<<in[1]<<" "<<out[1]<<" "<<b<<" "<<fa(1,b)<<endl;
	return dp[a][0];
}
inline int qiu(int a,int b){
	//cout<<dep[a]<<" "<<dep[b]<<" "<<dp[a][0]<<endl;
	return dep[a]+dep[b]-2*dep[lca(a,b)];
}
int main(){
	freopen("std.out","r",stdin);
	ll ans=0;
	while(scanf("%d",&n)==1)ans+=n;
	cout<<ans<<endl;
}
