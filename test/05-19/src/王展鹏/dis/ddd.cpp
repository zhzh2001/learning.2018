#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=300005;
int n,x[N],y[N],mnx,mny,mxx,mxy;
#define zb(i,j) (i-1)*mxy+j
#define lowbit(i) i&-i
struct BIT{
	int c[N];
	void change(int a,int b,int x){
		for(int i=a;i<=mxx;i+=lowbit(i)){
			for(int j=b;j<=mxy;j+=lowbit(j)){
				c[zb(i,j)]=min(c[zb(i,j)],x);
			}
		}
	}
	int ask(int a,int b){
		int ans=1e9;
		for(int i=a;i;i-=lowbit(i)){
			for(int j=b;j;j-=lowbit(j)){
				ans=min(ans,c[zb(i,j)]);
			}
		}
		return ans;
	}
}T[4];
int vvv[N];
vector<int> v[N];
map<int,int> M[N];
inline bool find(int a,int b){
	return M[a].count(b);
}
int calc(int s){
	int l=0,r=0; static int q[N],bs[N],vis[N];
	for(int i=1;i<=n;i++)vis[i]=0;
	q[++r]=s;
	if(vvv[s])return 0;
	while(l<r){
		int t=q[++l]; if(vvv[t])return bs[l];
		for(unsigned i=0;i<v[t].size();i++)if(!vis[v[t][i]]){q[++r]=v[t][i]; vis[v[t][i]]=1; bs[r]=bs[l]+1;}
	}
	return -1;
}
int main(){
	freopen("dis.in","r",stdin); freopen("ddd.out","w",stdout);
	n=read();
	mnx=1e9,mxx=-1e9,mny=1e9,mxy=-1e9;
	for(int i=1;i<=n;i++){
		x[i]=read(); y[i]=read();
		mnx=min(mnx,x[i]); mxx=max(mxx,x[i]); mxy=max(mxy,y[i]); mny=min(mny,y[i]);
	}
	if((ll)(mxx-mnx+1)*(mxy-mny+1)==(ll)n){
		for(int i=1;i<N;i++)T[0].c[i]=T[1].c[i]=T[2].c[i]=T[3].c[i]=1e9;
		mxx-=mnx-1; mxy-=mny-1;
		for(int i=1;i<=n;i++){
			x[i]-=mnx-1; y[i]-=mny-1;
		}
		int q=read();
		for(int i=1;i<=q;i++){
			int op=read(),x=read(),y=read();
			x-=mnx-1; y-=mny-1;
			if(op==2){
				int jb=min(min(x+y+T[0].ask(x,y),T[3].ask(mxx-x+1,mxy-y+1)-x-y),min(x+T[1].ask(x,mxy-y+1)-y,y+T[2].ask(mxx-x+1,y)-x));
				writeln(jb>1e8?-1:jb);
			}else{
				T[0].change(x,y,-x-y); T[3].change(mxx-x+1,mxy-y+1,x+y); T[1].change(x,mxy-y+1,y-x); T[2].change(mxx-x+1,y,x-y);
			}
		}
	}else if(n<=1000){
		for(int i=1;i<=n;i++){
			M[x[i]][y[i]]=i;
		}
		for(int i=1;i<=n;i++){
			if(find(x[i],y[i]+1))v[i].push_back(M[x[i]][y[i]+1]);
			if(find(x[i]+1,y[i]))v[i].push_back(M[x[i]+1][y[i]]);
			if(find(x[i]-1,y[i]))v[i].push_back(M[x[i]-1][y[i]]);
			if(find(x[i],y[i]-1))v[i].push_back(M[x[i]][y[i]-1]);
		}
		int q=read();
		for(int i=1;i<=q;i++){
			int op=read(),x=read(),y=read();
			if(op==1)vvv[M[x][y]]=1;
			else writeln(calc(M[x][y]));
		}
	}else{
		for(int i=1;i<=n;i++){
			M[x[i]][y[i]]=i;
		}
		for(int i=1;i<=n;i++){
			if(find(x[i],y[i]+1))v[i].push_back(M[x[i]][y[i]+1]);
			if(find(x[i]+1,y[i]))v[i].push_back(M[x[i]+1][y[i]]);
			if(find(x[i]-1,y[i]))v[i].push_back(M[x[i]-1][y[i]]);
			if(find(x[i],y[i]-1))v[i].push_back(M[x[i]][y[i]-1]);
		}
		//dfs(1);
	}
}
