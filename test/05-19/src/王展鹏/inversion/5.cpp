#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=55,mod=1000000007;
int n,a[N],tong[N],tot;
int main(){
	//freopen("inversion.in","r",stdin); //freopen("inversion.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	if(n>50){
		ll sum=1;
		for(int i=1;i<=n-2;i++)sum=sum*i%mod;
		cout<<(ll)n*(n-1)/2%mod*((n*(n-1)/2)%mod)%mod*sum<<endl;
		return 0;
	}
	for(int i=1;i<=n;i++)for(int j=i+1;j<=n;j++){
		for(int k=1;k<=a[i];k++)for(int l=1;l<=a[j]&&l<k;l++){
			memset(tong,0,sizeof(tong));
			for(int o=1;o<=n;o++)if(o!=i&&o!=j){
				tong[a[o]-(a[o]>=k)-(a[o]>=l)]++;
			}
			ll ans=1; int sum=0;
			for(int o=n-2;o;o--){
				sum+=tong[o];
				ans=ans*(sum-(n-2-o))%mod;
			}
			//cout<<i<<" "<<j<<" "<<k<<" "<<l<<" "<<ans<<endl;
			tot=(tot+ans)%mod;
		}
	}
	cout<<tot<<endl;
}
/*
0 1 9 72 600 5400

*/
