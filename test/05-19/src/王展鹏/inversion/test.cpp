#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=1000005;
int n,x[N],y[N],mnx,mny,mxx,mxy;
#define zb(i,j) (i-1)*mxy+j
#define lowbit(i) i&-i
struct BIT{
	int c[N];
	void change(int a,int b,int x){
		for(int i=a;i<=mxx;i+=lowbit(i)){
			for(int j=b;j<=mxy;j+=lowbit(j)){
				c[zb(i,j)]=min(c[zb(i,j)],x);
			}
		}
	}
	int ask(int a,int b){
		int ans=1e9;
		for(int i=a;i;i-=lowbit(i)){
			for(int j=b;j;j-=lowbit(j)){
				ans=min(ans,c[zb(i,j)]);
			}
		}
		return ans;
	}
}T[4];
int main(){
	n=1e3;
	mnx=1e9,mxx=-1e9,mny=1e9,mxy=-1e9;
	mnx=1; mxx=n; mny=1; mxy=n;
	for(int i=1;i<N;i++)T[0].c[i]=T[1].c[i]=T[2].c[i]=T[3].c[i]=1e9;
	if((ll)(mxx-mnx+1)*(mxy-mny+1)>=(ll)n){
		mxx-=mnx-1; mxy-=mny-1;
		for(int i=1;i<=n;i++){
			x[i]-=mnx-1; y[i]-=mny-1;
		}
		int q=read();
		for(int i=1;i<=q;i++){
			int op=read(),x=read(),y=read();
			x-=mnx-1; y-=mny-1;
			if(op==2){ //cout<<q<<" "<<i<<" "<<op<<" "<<x<<" "<<y<<endl; while(1);
				writeln(min(min(x+y+T[0].ask(x,y),T[3].ask(mxx-x+1,mxy-y+1)-x-y),min(x+T[1].ask(x,mxy-y+1)-y,y+T[2].ask(mxx-x+1,y)-x)));
			}else{
				T[0].change(x,y,-x-y); T[3].change(mxx-x+1,mxy-y+1,x+y); T[1].change(x,mxy-y+1,y-x); T[2].change(mxx-x+1,y,x-y);
			}
		}
	}
}
/*
4
1 1 5
1 6 5
2 3 3
2 4 2
*/
