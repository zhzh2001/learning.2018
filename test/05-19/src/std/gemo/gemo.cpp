#include<cstdio>
using namespace std;
const int mo=1000000007;
const int N=32800;
int b[20],bit[N];
int f[N],n,m;
int main(){
	freopen("gemo.in","r",stdin);
	freopen("gemo.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		b[x]|=1<<(y-1);
	}
	for (int i=1;i<1<<n;i++)
		bit[i]=(i&1?bit[i>>1]*2+1:bit[i>>1]);
	f[0]=1;
	for (int i=1;i<1<<n;i++)
		if ((i&3)==3)
			for (int j=i;j;j=(j-1)&i){
				if ((j&3)!=0&&(j&3)!=3) continue;
				int tmp=1;
				for (int k=0;k<n;k++)
					if (i&(1<<k)){
						if (!(j&(1<<k)))
							tmp=1ll*tmp*bit[b[k+1]&j]%mo;
						else tmp=1ll*tmp*(bit[b[k+1]&(i^j)]+1)%mo;
					}
				f[i]=(f[i]+1ll*f[i^j]*tmp)%mo;
			}
		else if ((i&3)==0){
			f[i]=1;
			for (int j=0;j<n;j++)
				if (i&(1<<j))
					f[i]=1ll*f[i]*(bit[b[j+1]&i]+1)%mo;
	}
	int ans=1;
	for (int i=1;i<=m;i++)
		ans=2*ans%mo;
	printf("%d",(ans+mo-f[(1<<n)-1])%mo);
}
