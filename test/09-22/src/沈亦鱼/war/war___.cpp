#include<cstdio>
#include<algorithm>
using namespace std;
int n;
long long x,y,mmi,mma,s,ans,a[55],mi[55][55],ma[55][55],f[55][55];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void dfs(int st,int en){
//	if(l==2&&r==5)printf("%d %d\n",st,s);
	if((mmi<1100&&s+x+y*(mma-mmi)*(mma-mmi)>=ans)||(s>=ans))return;
	if(st>en){
		if(s+x+y*(mma-mmi)*(mma-mmi)<ans)ans=s+x+y*(mma-mmi)*(mma-mmi);
		return;
	}
	long long lmi=mmi,lma=mma;
	mmi=min(mmi,a[st]);
	mma=max(mma,a[st]);
	dfs(st+1,en);
	mmi=lmi;
	mma=lma;
	for(int i=0;i<=en-st;i++){
		s+=f[st][st+i];
		dfs(st+i+1,en);
		s-=f[st][st+i];
	}
}
int main(){
//	freopen("war.in","r",stdin);
//	freopen("war.out","w",stdout);
	n=read();
	x=read();
	y=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<=n;i++)
		for(int j=i;j<=n;j++){
			mi[i][j]=a[i];
			ma[i][j]=a[i];
			for(int k=i+1;k<=j;k++){
				mi[i][j]=min(mi[i][j],a[k]);
				ma[i][j]=max(ma[i][j],a[k]);
			}
		}
	for(int i=1;i<=n;i++)
		for(int j=i;j<=n;j++)
			f[i][j]=x+y*(ma[i][j]-mi[i][j])*(ma[i][j]-mi[i][j]);
	for(int i=1;i<=n+1;i++)
		mi[i][i-1]=1100;
	ans=x+y*(ma[1][n]-mi[1][n])*(ma[1][n]-mi[1][n]);
	s=0;
	mmi=1100;
	mma=0;
	dfs(1,n);
	printf("%lld",ans);
	return 0;
}/*
10
3 1
7 10 9 10 6 7 10 7 1 2

15
50 15
6 7 8 9 10 11 10 9 8 7 6 5 4 3 2

25
100 5
29 26 27 26 32 25 23 25 25 18 21 20 23 26 27 21 27 27 24 27 32 41 29 42 24
*/
