#include<cstdio>
#include<algorithm>
using namespace std;
int n,l,r;
long long x,y,mmi,mma,a[55],mi[55][55],ma[55][55],f[55][55];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
int main(){
//	freopen("war.in","r",stdin);
//	freopen("war.out","w",stdout);
	n=read();
	x=read();
	y=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<=n;i++)
		for(int j=i;j<=n;j++){
			mi[i][j]=a[i];
			ma[i][j]=a[i];
			for(int k=i+1;k<=j;k++){
				mi[i][j]=min(mi[i][j],a[k]);
				ma[i][j]=max(ma[i][j],a[k]);
			}
		}
	for(int i=1;i<=n+1;i++)
		mi[i][i-1]=1100;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n-i+1;j++){
			l=j;
			r=j+i-1;
			f[l][r]=x+y*(ma[l][r]-mi[l][r])*(ma[l][r]-mi[l][r]);
			for(int ii=l;ii<=r;ii++)
				for(int jj=ii;jj<=r;jj++)
					if(ii!=l||jj!=r){
						mmi=min(mi[l][ii-1],mi[jj+1][r]);
						mma=max(ma[l][ii-1],ma[jj+1][r]);
						f[l][r]=min(f[l][r],f[ii][jj]+x+y*(mma-mmi)*(mma-mmi));
//						if(l==2&&r==5){
//							printf("%d %d\n",ii,jj);
//							printf("%d\n",mma);
//						}
					}
		}
	printf("%lld",f[1][n]);
	return 0;
}/*
10
3 1
7 10 9 10 6 7 10 7 1 2

25
100 5
29 26 27 26 32 25 23 25 25 18 21 20 23 26 27 21 27 27 24 27 32 41 29 42 24
*/
