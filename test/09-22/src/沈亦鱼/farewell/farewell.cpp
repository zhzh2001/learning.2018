#include<cstdio>
#include<algorithm>
using namespace std;
int l,n,m,x,k1,k2;
long long s,a[110000],d1[110000],e1[110000],d2[110000],e2[110000];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void sw1(int x,int y){
	swap(d1[x],d1[y]);
	swap(e1[x],e1[y]);
}
void up1(int i){
	while(i>1){
		if(d1[i>>1]>d1[i])sw1(i>>1,i);
		else break;
		i>>=1;
	}
}
void down1(int i){
	int j=0;
	while(i*2<=n){
		if(d1[i*2]<d1[i*2+1])j=i*2;
		else j=i*2+1;
		if(d1[i]>d1[j])sw1(i,j);
		else break;
		i=j;
	}
}
void add1(int x){
	k1++;
	d1[k1]=x;
	e1[k1]=x;
	up1(k1);
}
void sw2(int x,int y){
	swap(d2[x],d2[y]);
	swap(e2[x],e2[y]);
}
void up2(int i){
	while(i>1){
		if(d2[i>>1]>d2[i])sw2(i>>1,i);
		else break;
		i>>=1;
	}
}
void down2(int i){
	int j=0;
	while(i*2<=n){
		if(d2[i*2]<d2[i*2+1])j=i*2;
		else j=i*2+1;
		if(d2[i]>d2[j])sw2(i,j);
		else break;
		i=j;
	}
}
void add2(int x){
	k2++;
	d2[k2]=x;
	e2[k2]=x;
	up2(k2);
}
int main(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	l=read();
	n=read();
	m=read();
	for(int i=1;i<=n;i++){
		x=read();
		add1(x);
	}
	for(int i=1;i<=m;i++){
		x=read();
		add2(x);
	}
	d1[n+1]=d2[m+1]=(long long)(1e14+1);
	for(int i=1;i<=l;i++){
		a[i]=d1[1];
		d1[1]+=e1[1];
		down1(1);
	}
	for(int i=1;i<=l;i++){
		s=max(s,a[l-i+1]+d2[1]);
		d2[1]+=e2[1];
		down2(1);
	}
	printf("%lld",s);
	return 0;
}
/*
用优先队列维护当前机器洗衣服的时间和洗衣结束的时间，每次去最小的结束时间加上洗衣所需时间加入队列。烘干时采用最大配最小的贪心策略。
*/
