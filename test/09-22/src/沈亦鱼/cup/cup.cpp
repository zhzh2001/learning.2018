#include<cstdio>
#include<cstring>
using namespace std;
int n,m,size,cnt,k,s,a[1100000],b[1100000],bi[1100000],us[1100000],p[1100000],f[1100000],g[1100000],bb[1100000];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
int gcd(int x,int y){
	if(!y)return x;
	else return gcd(y,x%y);
}
int main(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<=m;i++){
		b[i]=read();
		bb[b[i]]=1;
	}
	size=1e3;
	memset(us,1,sizeof(us));
	us[1]=0;
	for(int i=2;i<=size;i++){
		if(us[i])p[++cnt]=i;
		for(int j=1;j<=cnt&&i*p[j]<=size;j++){
			us[i*p[j]]=false;
			if(i%p[j]==0)break;
		}
	}
//	printf("%d\n",cnt);
//	for(int i=1;i<=cnt;i++)
//		printf("%d ",p[i]);
//	k=1;
//	for(int i=1;i<=cnt;i++)
//		if(b[k]==p[i]){
//			bi[i]=-1;
//			k++;
//		}
//		else bi[i]=1;
	f[1]=0;
	for(int i=2;i<=1e6;i++){
		s=0;
		for(int j=1;j<=cnt;j++)
			if(i%p[j]==0){
				s=1;
				if(bb[p[j]]==1)f[i]=f[i/p[j]]-1;
				else f[i]=f[i/p[j]]+1;
				break;
			}
		if(s==0){
			if(bb[i]==1)f[i]=-1;
			else f[i]=1;
		}
	}
	g[1]=a[1];
	s=f[a[1]];
	for(int i=2;i<=n;i++){
		g[i]=gcd(g[i-1],a[i]);
		s+=f[a[i]];
	}
	k=1;
	for(int i=n;i>0;i--){
		a[i]/=k;
		g[i]/=k;
		if(f[g[i]]<0){
			k*=g[i];
			s-=f[g[i]]*i;
		}//printf("%d\n",s);
	}
//	for(int i=1;i<=n;i++)
//		printf("%d ",a[i]);puts("");
	printf("%d",s);
	return 0;
}/*
32 1
2916 108 216 1458 34992 54 18 648 5832 6 36 324 432 104976 11664 17496 13122 4374 1296 72 972 1944 24 52488 486 144 48 12 26244 8748 3888 162
3
*/
