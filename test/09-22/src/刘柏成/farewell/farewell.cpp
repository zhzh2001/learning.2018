#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=100002;
int k,n,m;
ll a[maxn],b[maxn];
bool check(ll x){
	ll sum=0;
	for(int i=1;i<=n;i++){
		sum+=x/a[i];
		if (sum>=k)
			return true;
	}
	return sum>=k;
}
int main(){
	init();
	k=readint(),n=readint(),m=readint();
	for(int i=1;i<=n;i++)
		a[i]=readint();
	sort(a+1,a+n+1);
	bool flag=true;
	for(int i=1;i<=m;i++){
		b[i]=readint();
		if (b[i])
			flag=false;
	}
	sort(b+1,b+m+1);
	if (k==1)
		return printf("%lld",a[1]+b[1]),0;
	ll l=0,r=1LL*(k/n+1)*a[n];
	while(l<r){
		ll mid=(l+r)/2;
		if (check(mid)) //<=mid
			r=mid;
		else l=mid+1;
	}
	printf("%lld",l);
}