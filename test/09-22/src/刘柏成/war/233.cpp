#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	// freopen("war.in","r",stdin);
	// freopen("war.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int w[53],p[53],n,a,b;
int nums[53],cur;
int dp[53][53];
int f[53][53][53]; //min max
int sqr[2002];
inline void relax(int &a,int b){
	if (a>b)
		a=b;
}
int main(){
	init();
	srand(time(0));
	n=50,a=rand()%1501,b=rand()%5;
	for(int i=1;i<=n;i++)
		w[i]=nums[i]=rand()%50+1;
	sort(nums+1,nums+n+1);
	cur=unique(nums+1,nums+n+1)-nums-1;
	for(int i=1;i<=n;i++)
		p[i]=lower_bound(nums+1,nums+cur+1,w[i])-nums;
	memset(dp,0x3f,sizeof(dp));
	for(int i=1;i<=n;i++)
		dp[i][i]=a;
	for(int i=0;i<=n;i++)
		dp[i+1][i]=0;
	for(int i=1;i<=2000;i++)
		sqr[i]=i*i;
	for(int l=n;l;l--){
		memset(f,0x3f,sizeof(f));
		f[l][p[l]][p[l]]=0;
		for(int r=l+1;r<=n;r++){
			for(int i=l;i<r;i++){
				relax(dp[l][r],dp[l][i]+dp[i+1][r]);
				for(int j=1;j<=p[l];j++){
					int now=min(j,p[r]);
					for(int k=p[l];k<=cur;k++)
						relax(f[r][now][max(k,p[r])],f[i][j][k]+dp[i+1][r-1]);
				}
			}
			// printf("dp[%d][%d]=%d\n",l,r,dp[l][r]);
			for(int i=1;i<=p[l];i++)
				for(int j=p[l];j<=cur;j++)
					relax(dp[l][r],a+f[r][i][j]+b*sqr[nums[j]-nums[i]]);
			// printf("dp[%d][%d]=%d\n",l,r,dp[l][r]);
		}
	}
	printf("%d\n",dp[1][n]);
	printf("%.5f\n",1.0*clock()/CLOCKS_PER_SEC);
}