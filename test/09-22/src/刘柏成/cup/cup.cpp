#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int gcd(int a,int b){
	return b?gcd(b,a%b):a;
}
bool qwq[50003];
bool vis[50003];
int primes[50003],cur;
int n,m,a[2003],b[2003],pre[2003];
int f(int x){
	int ans=0;
	for(int i=1;i<=cur;i++){
		int t=primes[i];
		if (t*t>x)
			break;
		int sgn=qwq[t]?-1:1;
		while(x%t==0)
			x/=t,ans+=sgn;
	}
	if (x!=1){
		int p=lower_bound(b+1,b+m+1,x)-b;
		if (p!=m+1 && b[p]==x)
			ans--;
		else ans++;
	}
	return ans;
}
int dp[2003];
int main(){
	init();
	n=readint(),m=readint();
	vis[1]=1;
	for(int i=2;i<=50000;i++){
		if (vis[i])
			continue;
		primes[++cur]=i;
		for(int j=i*2;j<=50000;j+=i)
			vis[j]=1;
	}
	for(int i=1;i<=n;i++)
		a[i]=readint(),pre[i]=gcd(a[i],pre[i-1]);
	for(int i=1;i<=m;i++){
		b[i]=readint();
		if (b[i]<=50000)
			qwq[b[i]]=1;
	}
	sort(b+1,b+m+1);
	int sum=0;
	for(int i=1;i<=n;i++){
		sum+=f(a[i]);
		pre[i]=pre[i]==pre[i-1]?pre[i-1]:f(pre[i]);
	}
	for(int i=1;i<=n;i++){
		dp[i]=i*pre[i];
		for(int j=1;j<i;j++)
			dp[i]=min(dp[i],(i-j)*pre[i]+dp[j]);
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		ans=min(ans,dp[i]);
	printf("%d",sum-ans);
}