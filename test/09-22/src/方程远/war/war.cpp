#include<bits/stdc++.h>
#define int long long
using namespace std;
int n,a,b,ans=666666666,maxx,minn;
int f[10001],nxt[10001];
void search(int x){
//			cout<<"\nx="<<x<<"\n";
	if(nxt[0]>n){
		ans=min(ans,x);
//		cout<<"ans="<<ans<<"\n";
//		system("pause");
		return;
	}
	for(int i=0;i<=n;i=nxt[i]){
		for(int j=nxt[i];j<=n;j=nxt[j]){
//			for(int k=0;k<=n;k=nxt[k])
//				cout<<f[k]<<" ";
			int lft=i;
			int rht=j;
			maxx=0;
			minn=998244353;
			for(int k=nxt[i];k<=j;k=nxt[k]){
				maxx=max(f[k],maxx);
				minn=min(f[k],minn);
			}
			nxt[lft]=nxt[rht];
			search(x+a+b*(maxx-minn)*(maxx-minn));
			nxt[lft]=rht;
		}
	}
}
signed main(){
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	cin>>n>>a>>b;
	for(int i=1;i<=n;i++)
		cin>>f[i];
	for(int i=n;i>=0;i--)
		nxt[i]=i+1;
	search(0);
	cout<<a;
	return 0;
}
