#include<bits/stdc++.h>
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
int prime(int q){
	if(q==1)
		return 0;
	bool pd=0;
	int x=2;
	while(x<=floor(sqrt(q))&&(q%x!=0))
        x+=1;
    if(x>floor(sqrt(q)))
        pd=1;
	return pd;
}
int n,m,maxx,x,sum;
int a[2001],b[2001];
int good[1000001],pd[1000001],ans[1000001];
int main(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	read(n);
	read(m);
	for(int i=1;i<=n;i++){
		read(a[i]);
		maxx=max(maxx,a[i]);
	}
	for(int i=1;i<=maxx;i++)
		good[i]=pd[i]=prime(i);
	for(int i=1;i<=m;i++){
		read(x);
		good[x]=0;
	}
	ans[1]=1;
	for(int i=1;i<=maxx;i++)
		for(int j=2;j<=i;j++)
			if(pd[j]&&i%j==0){
				if(good[j])
					ans[i]=ans[i/j]+1;
				else
					ans[i]=ans[i/j]-1;
			}
	for(int i=1;i<=n;i++)
		sum+=ans[a[i]];
	cout<<sum;
}
