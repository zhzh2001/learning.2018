#include<bits/stdc++.h>
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
struct Edge{
	int a,b,fin;
}edge[10001];
bool cmp(Edge x,Edge y){
	return x.fin<y.fin;
}
int l,n,m,ans,res=998244353;
int a[10001],b[10001],A[10001],B[10001];
void search(int x){
	if(x>l){
		ans=0;
		for(int i=1;i<=x;i++){
			A[edge[i].a]++;
			edge[i].fin=A[edge[i].a]*a[edge[i].a];
		}
		sort(edge+1,edge+1+l,cmp);
		for(int i=1;i<=l;i++)
			B[edge[i].b]=max(B[edge[i].b],edge[i].fin)+b[edge[i].b];
		for(int i=1;i<=m;i++){
			ans=max(ans,B[i]);
			B[i]=0;
		}
		for(int i=1;i<=n;i++)
			A[i]=0;
		res=min(res,ans);
		return;
	}
	for(int i=1;i<=n;i++){
		edge[x].a=i;
		for(int j=1;j<=m;j++){
			edge[x].b=j;
			search(x+1);
		}
	}
}
int main(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	read(l);
	read(n);
	read(m);
	for(int i=1;i<=n;i++)
		read(a[i]);
	for(int i=1;i<=m;i++)
		read(b[i]);
	search(1);
	cout<<res;
	return 0;
}
