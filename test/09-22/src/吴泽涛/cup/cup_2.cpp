#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 2011, Val = 1e6+11; 
int n, m; 
int a[N], b[N], is_prime[Val], f[Val], mp[Val], prime[Val], tot; 

inline void work_1() {
	int ans = 0; 
	For(i, 1, n) {
		int x = a[i]; 
		int tt = int(sqrt(x)); 
		For(j, 2, tt) {
			int jia = 0; 
			if(f[j]!=-1) jia = 1; 
			else jia = -1; 
			while(x % j==0) {
				x/=j; 
				ans += jia; 
			}
		}
		if(x!=1) {
			if(f[x]!=-1) ++ans; 
			else --ans; 
		}
	}
	writeln(ans); 
	exit(0); 
}

int main() {
	freopen("cup.in", "r", stdin); 
	freopen("cup.out", "w", stdout); 
	n = read(); m = read(); 
	int tmp = 0; 
	For(i, 1, n) {
		a[i] = read(); 
		tmp = max(tmp, a[i]); 
	}
	For(i, 1, m) {
		b[i] = read(); 
		f[b[i]] = -1; 
	}
	work_1(); 
	
	
	f[1] = 0; is_prime[1] = 1; 
	For(i, 2, tmp) {
		if(!is_prime[i]) {
			prime[++tot] = i; 
			int x = i; 
			if(f[i]!=-1) f[i] = 1; 
			while(x <= tmp) {
				x += i; 
				is_prime[x] = 1;  
			}
		}
	}
	
	
	
	int ww = 0; 
	For(i, 1, n) ww += f[a[i]]; 
	writeln(ww);  
}



/*

5 2
4 20 34 10 10
2 5



*/


