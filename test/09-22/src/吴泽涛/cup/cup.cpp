#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 2011, Val = 1e6+11; 
int n, m; 
int a[N], b[N], f[Val], bd[3], ans; 

inline int gcd(int x, int y) {
	if(x%y==0) return y;  
	else return gcd(y, x%y); 
}

inline void work_1() {
	ans = 0; 
	For(i, 1, n) {
		int x = a[i]; 
		int tt = int(sqrt(x)); 
		For(j, 2, tt) {
			int jia = 0; 
			if(f[j]!=-1) jia = 1; 
			else jia = -1; 
			while(x % j==0) {
				x/=j; 
				ans += jia; 
			}
		}
		if(x!=1) {
			if(f[x]!=-1) ++ans; 
			else --ans; 
		}
	}
//	writeln(ans); 
}

int main() {
	freopen("cup.in", "r", stdin); 
	freopen("cup.out", "w", stdout); 
	n = read(); m = read(); 
	int tmp = 0; 
	For(i, 1, n) {
		a[i] = read(); 
		tmp = max(tmp, a[i]); 
	}
	For(i, 1, m) {
		b[i] = read(); 
		f[b[i]] = -1; 
	}
	work_1();  
	if(a[1]==1) {
		writeln(ans); 
		return 0; 
	}

	
	Dow(i, n, 1) {
		int G = a[1]; 
		For(j, 2, i) G = gcd(G, a[i]); 
		int x = G; 
		int tt = int(sqrt(x)); 
		bd[1] = bd[2] = 0; 
		For(j, 2, tt) {
			int jia; 
			if(f[j]!=-1) jia = 1; 
			else jia = 2; 
			while(x % j==0) {
				x/=j; 
				bd[jia]++; 
			}
		}
		if(x!=1) {
			if(f[x]!=-1) ++bd[1]; 
			else ++bd[2]; 
		}
		
		if(bd[1]>=bd[2]) continue; 
		else {
			ans += (bd[2]-bd[1])*i; 
			For(j, 1, i) a[j]/=G; 
		}
	}
	writeln(ans); 
}




