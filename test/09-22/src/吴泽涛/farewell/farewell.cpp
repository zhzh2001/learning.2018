#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 1e5+11; 
int L, n, m, a1, b1; 
int a[N], b[N];  

inline bool check(LL T) {
	int sum = 0; 
	For(i, 1, n) sum+= T/a[i];
	return sum >= L; 
}

inline void work_1() { 
	LL l = 0, r = a1*L; 
	while(l < r) {
		LL mid = (l+r)/2; 
		if(check(mid))
			r = mid; 
		else 
			l = mid+1; 
	}
	writeln(r); 
	exit(0); 
}

int main() {
	freopen("farewell.in", "r", stdin); 
	freopen("farewell.out", "w", stdout); 
	L = read(); n = read(); m = read(); 
	a1 = 1e9, b1 = 1e9; 
	For(i, 1, n) {
		a[i] = read(); 
		a1 = min(a1, a[i]); 
	}
	bool flag = 0; 
	For(i, 1, m) {
		b[i] = read(); 
		b1 = min(b1, b[i]); 
		if(b[i]!=0) flag = 1; 
	}
	if(L==0) {
		writeln(a1+b1); 
		return 0; 
	}
	if(flag==0) work_1(); 
}



