#include <map>
#include <cstdio>
#include <iostream>
using namespace std;
#define gc c = getchar()
int read(){
	int x = 0, f = 1; char gc;
	for(; !isdigit(c); gc) if(c == '-') f = -1;
	for(; isdigit(c); gc) x = x * 10 + c - '0';
	return x * f;
}
#undef gc
int n, m, a[2005], b[2005], GCD[2005];
map<int, int>f;
long long calc(int x){
	if(x == 1) return 0;
	if(f.count(x)) return f[x];
	bool flag = 0;
	for(long long i = 2; i * i <= x; i++)
		if(x % i == 0) {
			int tmp = (*lower_bound(b + 1, b + m + 1, i) == i) * 2;
			f[x] = calc(x / i) + 1 - tmp;
			flag = 1;
			break;
		}
	if(!flag) {
		int tmp = (*lower_bound(b + 1, b + m + 1, x) == x) * 2;
		f[x] = calc(x / x) + 1 - tmp;
	}
	return f[x];
}
int gcd(int x, int y){
	return y ? gcd(y, x % y) : x;
}
int main(){
	freopen("cup.in", "r", stdin);
	freopen("cup.out", "w", stdout);
	n = read(), m = read();
	for(int i = 1; i <= n; i++) a[i] = read();
	for(int i = 1; i <= m; i++) b[i] = read();
	bool f;
	do {
		f = 0;
		GCD[1] = a[1];
		for(int i = 2; i <= n; i++) GCD[i] = gcd(GCD[i - 1], a[i]);
		for(int i = n; i; i--) {
			int x = GCD[i], cnt_good = 0, cnt_bad = 0;
			for(long long j = 2; j * j <= x; j++)
				if(x % j == 0) {
					if(*lower_bound(b + 1, b + m + 1, j) == j) cnt_bad++;
					else cnt_good++;
					while(x % j == 0) x /= j;
				}
			if(x != 1) {
				if(*lower_bound(b + 1, b + m + 1, x) == x) cnt_bad++;
				else cnt_good++;
			}
			if(cnt_bad >= cnt_good && cnt_bad) {
				f = 1;
				for(int j = 1; j <= i; j++) a[j] /= GCD[i];
				break;
			}
		}
	} while(f);
	long long ans = 0;
	for(int i = 1; i <= n; i++) ans += calc(a[i]);
	printf("%lld", ans);
}
