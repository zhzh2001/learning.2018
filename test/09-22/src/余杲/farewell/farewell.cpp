#include <queue>
#include <cstdio>
#include <iostream>
using namespace std;
typedef long long LL;
#define gc c = getchar()
int read(){
	int x = 0, f = 1; char gc;
	for(; !isdigit(c); gc) if(c == '-') f = -1;
	for(; isdigit(c); gc) x = x * 10 + c - '0';
	return x * f;
}
#undef gc
int l, n, m;
int a[100005], b[100005];
struct A {
	A(){
	}
	A(int Id, LL Now){
		id = Id, now = Now;
	}
	int id;
	LL now;
	bool operator<(const A &other) const {
		if(now + a[id] != other.now + a[other.id]) return now + a[id] > other.now + a[other.id];
		return a[id] > a[other.id];
	}
};
struct B {
	B(){
	}
	B(int Id, LL Now){
		id = Id, now = Now;
	}
	int id;
	LL now;
	bool operator<(const B &other) const {
		if(now + b[id] != other.now + b[other.id]) return now + b[id] > other.now + b[other.id];
		return b[id] > b[other.id];
	}
};
priority_queue<A>qA;
priority_queue<B>qB;
int main(){
	freopen("farewell.in", "r", stdin);
	freopen("farewell.out", "w", stdout);
	l = read(), n = read(), m = read();
	for(int i = 1; i <= n; i++) {
		a[i] = read();
		qA.push(A(i, 0));
	}
	for(int i = 1; i <= m; i++) {
		b[i] = read();
		qB.push(B(i, 0));
	}
	LL ans = 0;
	for(int i = 1; i <= l; i++) {
		B x = qB.top(); qB.pop();
		A y = qA.top(); qA.pop();
		ans = max(ans, y.now + a[y.id] + b[x.id]);
		qB.push(B(x.id, y.now + a[y.id] + b[x.id]));
		qA.push(A(y.id, y.now + a[y.id]));
	}
	printf("%lld", ans);
}
