#include<bits/stdc++.h>
using namespace std;
int n,i,a,b,k,j,w[52],mn,mx,tmp,f[1<<20];
int main(){
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	scanf("%d%d%d",&n,&a,&b);
	if (n>20){
		printf("%d",a);
		return 0;
	}
	for (i=0;i<n;i++) scanf("%d",&w[i]);
	memset(f,63,sizeof(f));
	f[(1<<n)-1]=0;
	for (k=(1<<n)-1;k;k--){
		for (i=0;i<n;i++)
			if (k&(1<<i)){
				mn=mx=w[i];tmp=k;
				for (j=i;j<n;j++)
					if (k&(1<<j)){
						tmp^=1<<j;mx=max(mx,w[j]);mn=min(mn,w[j]);
						f[tmp]=min(f[tmp],f[k]+a+b*(mx-mn)*(mx-mn));
					}
			}
	}
	printf("%d",f[0]);
}
