#include<bits/stdc++.h>
using namespace std;
#define sqr(x) ((x)*(x))
int w[52],i,j,f[52][52],mn[52][52],mx[52][52],l,r,n,a,b,k,MX,MN,g[1<<18],tmp,fl;
int main(){
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	scanf("%d%d%d",&n,&a,&b);
	for (i=0;i<n;i++) scanf("%d",&w[i]);
	fl=1;
	for (i=1;i<n && fl;i++)
		if (w[i]!=w[i-1]) fl=0;
	if (fl){
		printf("%d",a);
		return 0;
	}
	if (n>18){
		memset(mn,63,sizeof(mn));
		for (i=0;i<n;i++)
			for (j=i;j<n;j++){
				if (j) mx[i][j]=max(mx[i][j-1],w[j]),mn[i][j]=min(mn[i][j-1],w[j]);
				else mx[i][j]=mn[i][j]=w[j];
				f[i][j]=a+b*sqr(mx[i][j]-mn[i][j]);
			}
		for (i=n-1;i>=0;i--)
			for (j=i;j<n;j++){
				for (k=i;k<j;k++) f[i][j]=min(f[i][j],f[i][k]+f[k+1][j]);
				for (l=i+1;l<=j;l++)
					for (r=l;r<j;r++) f[i][j]=min(f[i][j],
					f[l][r]+a+b*sqr(max(mx[i][l-1],mx[r+1][j])-min(mn[i][l-1],mn[r+1][j])));
			}
		printf("%d",f[0][n-1]);
	}else{
		memset(g,63,sizeof(g));
		g[(1<<n)-1]=0;
		for (k=(1<<n)-1;k;k--){
			for (i=0;i<n;i++)
				if (k&(1<<i)){
					MN=MX=w[i];tmp=k;
					for (j=i;j<n;j++)
						if (k&(1<<j)){
							tmp^=1<<j;MX=max(MX,w[j]);MN=min(MN,w[j]);
							g[tmp]=min(g[tmp],g[k]+a+b*(MX-MN)*(MX-MN));
						}
				}
		}
		printf("%d",g[0]);
	}
}
