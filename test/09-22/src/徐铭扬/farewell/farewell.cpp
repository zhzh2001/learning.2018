#include<bits/stdc++.h>
using namespace std;
const int N=100002;
struct node{
	int a,b;
	friend bool operator <(node x,node y){return x.a>y.a;}
}t;
priority_queue<node>q;
int l,n,m,i,a[N],b[N],mna,mnb,fl,mx;
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
int main(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	l=rd();n=rd();m=rd();
	mna=mnb=1e9;
	for (i=1;i<=n;i++) a[i]=rd(),q.push((node){a[i],a[i]}),mna=min(mna,a[i]);
	for (i=1;i<=m;i++) b[i]=rd(),mnb=min(mnb,b[i]);
	if (l==1){
		printf("%d",mna+mnb);
		return 0;
	}
	fl=1;
	for (i=1;i<=n;i++)
		if (b[i]) fl=0;
	if (fl){
		for (i=1;i<=l;i++){
			t=q.top();q.pop();
			mx=max(mx,t.a);
			q.push((node){t.a+t.b,t.b});
		}
	}
	printf("%d",mx);
}
/*
3 4 2
3 5 7 10
0 0
*/
