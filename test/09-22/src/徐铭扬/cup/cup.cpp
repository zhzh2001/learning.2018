#include<bits/stdc++.h>
using namespace std;
const int N=2002;
map<int,int>mp;
int a[N],cnt,sum,G,ans,i,j,n,m,tmp,mn,k;
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
int main(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	n=rd();m=rd();
	for (i=1;i<=n;i++) a[i]=rd();
	for (i=1;i<=m;i++) mp[rd()]=1;
	G=a[1];
	for (i=2;i<=n;i++) G=__gcd(G,a[i]);
	for (i=n;i;i--){
		tmp=G;sum=0;
		for (j=2;j*j<=G;j++)
			if (G%j==0){
				cnt=0;
				while (G%j==0) G/=j,cnt++;
				sum+=(mp[j]?-1:1)*cnt;
			}
		if (G>1) sum+=mp[G]?-1:1;
		if (sum<0){
			mn=1e9;
			for (j=1;j<=n && mn;j++){
				for (k=1;k<=mn;k++)
					if (a[j]%tmp==0) a[j]/=tmp;
					else break;
				mn=k-1;
			}
		}
		G=a[1];
		for (j=2;j<=i;j++) G=__gcd(G,a[j]);
	}
	for (i=1;i<=n;i++){
		for (j=2;j*j<=a[i];j++)
			if (a[i]%j==0){
				cnt=0;
				while (a[i]%j==0) a[i]/=j,cnt++;
				ans+=(mp[j]?-1:1)*cnt;
			}
		if (a[i]>1) ans+=mp[a[i]]?-1:1;
	}
	printf("%d",ans);
}
