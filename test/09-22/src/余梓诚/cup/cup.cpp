#include <map>
#include <ctime>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
#define rep(i,x,y) for (register int i=(x);i<=(y);i++)
#define drp(i,x,y) for (register int i=(x);i>=(y);i--)
void judge(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
}
inline char read(){
	static const int IN_LEN=1000000;
	static char buf[IN_LEN],*s,*t;
	return (s==t?t=(s=buf)+fread(buf,1,IN_LEN,stdin),(s==t?-1:*s++):*s++);
}
template<class T>
inline void read(T &x){
	static bool iosig;
	static char c;
	for (iosig=false,c=read();!isdigit(c);c=read()){
		if (c=='-') iosig=true;
		if (c==-1) return;
	}
	for (x=0;isdigit(c);c=read()) x=((x+(x<<2))<<1)+(c^'0');
	if (iosig) x=-x;
}
const int OUT_LEN=10000000;
char obuf[OUT_LEN],*ooh=obuf;
inline void print(char c){
	if (ooh==obuf+OUT_LEN) fwrite(obuf,1,OUT_LEN,stdout),ooh=obuf;
	*ooh++=c;
}
template<class T>
inline void print(T x){
	static int buf[30],cnt;
	if (x==0) print('0');
	else{
		if (x<0) print('-'),x=-x;
		for (cnt=0;x;x/=10) buf[++cnt]=x%10+48;
		while (cnt) print((char)buf[cnt--]);
	}
}
inline void flush(){fwrite(obuf,1,ooh-obuf,stdout);}
const int maxn=2100;
int n,m,a[maxn],b[maxn];
int sum[maxn][maxn];
int p[maxn],cnt[maxn],g[maxn];
int gcd(int x,int y){return y==0?x:gcd(y,x%y);}
map<int,bool> M;
long long pre(int num){
	int t=num;
	long long res=0;
	if (num==1) return 0;
	for (int i=2;i*i<=t;i++){
		if (t<i) break;
		while (t%i==0){
			t/=i;
			if (M[i]) res--;
			else res++;
			if (t==1) break;
		}
	}
	if (t>1){
		if (M[t]) res--;
		else res++;
	}
	return res;
}
int main(){
	judge();
	read(n),read(m);
	rep(i,1,n) read(a[i]),g[i]=((i==1)?a[i]:gcd(g[i-1],a[i]));
	rep(i,1,m) read(b[i]),M[b[i]]=1;
	//print(pre(1e9)),print('\n');
	//rep(i,1,n) print(g[i]),print(' '),print(pre(g[i])),print('\n');
	while (1){
		int pos=-1;
		drp(i,n,1){
			int t=pre(g[i]);
			if (t<0){pos=i;break;}
		}
		if (pos==-1) break;
		rep(i,1,pos) a[i]/=g[pos],g[i]/=g[pos];
	}
	long long ans=0;
	rep(i,1,n) ans+=pre(a[i]);
	print(ans),print('\n');
	//cerr<<clock()<<endl;
	return flush(),0;
}