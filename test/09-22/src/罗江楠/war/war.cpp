
// 我要这脸有何用
// 50 滚粗

#pragma GCC optimize("Ofast")

#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

int f[1<<16], w[20], a, b;

int calc(int x) {
  int mn =   2000;
  int mx = - 2000;
  for (int i = 0; i < 20; ++ i) {
    if (x >> i & 1) {
      mn = min(mn, w[i]);
      mx = max(mx, w[i]);
    }
  }
  return a + b * (mx - mn) * (mx - mn);
}

int _ts[21];

int subset(int x, int l, int r) {
  int top = 0;
  for (int i = 0; i < 20; ++ i) {
    if (x >> i & 1) {
      _ts[++ top] = i;
    }
  }
  int res = 0;
  for (int i = l; i <= r; ++ i) {
    res |= 1 << _ts[i];
  }
  return res;
}

int main(int argc, char const *argv[]) {
  freopen("war.in", "r", stdin);
  freopen("war.out", "w", stdout);

  int n = read();
  a = read();
  b = read();
  for (int i = 0; i < n; ++ i) {
    w[i] = read();
  }

  if (n <= 16) {

    for (int i = 1; i < (1 << n); ++ i) {
      int popcnt = __builtin_popcount(i);
      if (popcnt == 1) {
        f[i] = a;
      } else {
        f[i] = 1 << 30;
        for (int len = 1; len <= popcnt; ++ len) {
          for (int start = 1; start <= popcnt - len + 1; ++ start) {
            int j = subset(i, start, start + len - 1);
            f[i] = min(f[i], calc(j) + f[i - j]);
          }
        }
      }
    }
    printf("%d\n", f[(1 << n) - 1]);

  } else {

    // w[i] are the same
    printf("%d\n", a);

  }

  return 0;
}