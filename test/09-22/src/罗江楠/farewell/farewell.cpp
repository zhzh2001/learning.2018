#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

typedef pair<ll, ll> pii;
priority_queue<pii, vector<pii>, greater<pii> > bq;

ll a[N], b[N], c[N], d[N];
int main(int argc, char const *argv[]) {
  freopen("farewell.in", "r", stdin);
  freopen("farewell.out", "w", stdout);
  int l = read(), n = read(), m = read();
  for (int i = 1; i <= n; ++ i) {
    a[i] = read();
  }
  for (int i = 1; i <= m; ++ i) {
    b[i] = read();
  }
  // sort(a + 1, a + n + 1);
  // sort(b + 1, b + m + 1);

  // resolve a
  for (int i = 1; i <= n; ++ i) {
    bq.push(make_pair(a[i], a[i]));
  }
  for (int i = 1; i <= l; ++ i) {
    pii top = bq.top(); bq.pop();
    c[i] = top.first;
    top.first += top.second;
    bq.push(top);
  }

  // clean queue
  while (!bq.empty()) {
    bq.pop();
  }

  // resolve b
  for (int i = 1; i <= m; ++ i) {
    bq.push(make_pair(b[i], b[i]));
  }
  for (int i = 1; i <= l; ++ i) {
    pii top = bq.top(); bq.pop();
    d[i] = top.first;
    top.first += top.second;
    bq.push(top);
  }

  ll end_time = 0;
  for (int i = 1; i <= l; ++ i) {
    end_time = max(end_time, c[i] + d[l - i + 1]);
  }

  printf("%lld\n", end_time);

  return 0;
}
/*

a[i]直接优先队列做，算出来c[i]表示第i个材料最快完成的时间

对b[i]也直接做，算出d[i]

max{c[i]+d[l-i+1]}即为答案。正确性显然。

贪心萎掉的原因：

bad choice      
   |------------
|-----|-----|   
                
best choice     
|------------|  
   |-----|-----|



*/