#include <bits/stdc++.h>
#define N 2020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
bool mark[1000020];
int prime[1000020], cnt, pos = 1;
int res[1000020];
int b[N];
void filter_prime(int size) {
  for (int i = 2; i <= size; ++ i) {
    if (!mark[i]) prime[++ cnt] = i, res[i] = b[pos] == i ? (++ pos, -1) : 1;
    for (int j = 1; j <= cnt && prime[j] * i <= size; ++ j) {
      mark[prime[j] * i] = true;
      res[prime[j] * i] = res[i] + res[prime[j]];
      if (i % prime[j] == 0) {
        break;
      }
    }
  }
}
int a[N];
map<int, bool> is_bad_prime;
int f(int x) {
  if (x <= 1e6) return res[x];
  int ans = 0;
  for (int i = 1; x > 1 && i <= cnt; ++ i) {
    while (x % prime[i] == 0) {
      x /= prime[i];
      ans += res[prime[i]];
    }
    if (x <= 1e6) {
      ans += res[x];
      x = 1;
    }
  }
  // 如果在 [1..1e6] 之内的质数无法将 x 分解，说明这时候的 x 一定是一个质数
  if (x > 1) {
    if (is_bad_prime[x]) {
      -- ans;
    } else {
      ++ ans;
    }
  }
  return ans;
}
int main(int argc, char const *argv[]) {
  freopen("cup.in", "r", stdin);
  freopen("cup.out", "w", stdout);
  int n = read(), m = read();
  for (int i = 1; i <= n; ++ i) {
    a[i] = read();
  }
  for (int i = 1; i <= m; ++ i) {
    b[i] = read();
    is_bad_prime[b[i]] = true;
  }
  filter_prime(1e6);
  for (int i = n; i; -- i) {
    int gcd = a[1];
    for (int j = 2; j <= i; ++ j) {
      gcd = __gcd(gcd, a[j]);
    }
    if (f(gcd) < 0) {
      for (int j = 1; j <= i; ++ j) {
        a[j] /= gcd;
      }
    }
  }
  int ans = 0;
  for (int i = 1; i <= n; ++ i) {
    // printf("%d ", a[i]);
    ans += f(a[i]);
  }
  printf("%d\n", ans);
  return 0;
}
/*
f(x) 为x所有质因子是否为good prime的sum。
可以预处理出前1e6的答案，对于1e9的暴力分解质因数。

对于 a[1..i],发现除gcd就是减去 gcd 的答案。。。
判断一下f(gcd)<0即可

*/