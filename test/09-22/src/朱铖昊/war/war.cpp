#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=55;
int a[N],b[N],n,m,x,y;
map<int,int> mp;
ll f[N][N][N][N];
const ll inf=1e15+7;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline int sqr(int x)
{
	return x*x;
}
inline ll dfs(int l,int r,int mn,int mx)
{
	if (l>r)
		return 0;
	if (f[l][r][mn][mx])
		return f[l][r][mn][mx];
	if (l==r)
		return f[l][r][mn][mx]=x+(ll)y*sqr(b[mx]-b[mn]);
	ll p=min(dfs(l+1,r,min(mn,mp[a[l+1]]),max(mx,mp[a[l+1]])),
		  dfs(l+1,r,mp[a[l+1]],mp[a[l+1]])+x+(ll)y*sqr(b[mx]-b[mn]));
	for (int i=l+2;i<=r;++i)
		p=min(p,dfs(l+1,i-1,mp[a[l+1]],mp[a[l+1]])+
				dfs(i,r,min(mn,mp[a[i]]),max(mx,mp[a[i]])));
	return f[l][r][mn][mx]=p;
}
int main()
{
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	read(n);
	read(x);
	read(y);
	for (int i=1;i<=n;++i)
	{
		read(a[i]);
		b[i]=a[i];
	}
	sort(b+1,b+1+n);
	for (int i=1;i<=n;++i)
		if (b[i]!=b[m])
			b[++m]=b[i];
	for (int i=1;i<=m;++i)
		mp[b[i]]=i;
	cout<<dfs(1,n,mp[a[1]],mp[a[1]]);
	return 0;
}