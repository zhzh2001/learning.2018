#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=200005;
int a[N],b[N],t[N],n,m,sum,ans;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
struct heap
{
	int si;
	pair<int,int> a[N];
	inline void up(int x)
	{
		if (x==1)
			return;
		if (a[x]<a[x/2])
		{
			swap(a[x],a[x/2]);
			up(x/2);
		}
	}
	inline void down(int x)
	{
		if (x*2>si)
			return;
		int y=x*2;
		if (y+1<=si&&a[y+1]<a[y])
			++y;
		if (a[y]<a[x])
		{
			swap(a[y],a[x]);
			down(y);
		}
	}
}f,g;
inline bool check(int x)
{
	for (int i=1;i<=m;++i)
		g.a[i]=make_pair(b[i],b[i]);
	for (int i=sum;i;--i)
	{
		if (g.a[1].first+t[i]>x)
			return 0;
		g.a[1].first+=g.a[1].second;
		g.down(1);
	}
	return 1;
}
signed main()
{
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	read(sum);
	read(n);
	read(m);
	for (int i=1;i<=n;++i)
		read(a[i]);
	sort(a+1,a+1+n);
	for (int i=1;i<=m;++i)
		read(b[i]);
	sort(b+1,b+1+m);
	f.si=n;
	g.si=m;
	for (int i=1;i<=n;++i)
		f.a[i]=make_pair(a[i],a[i]);
	for (int i=1;i<=sum;++i)
	{
		t[i]=f.a[1].first;
		f.a[1].first+=f.a[1].second;
		f.down(1);
	}
	int l=t[sum],r=(int)1e14/n;
	ans=r;
	while (l<=r)
	{
		int mid=(l+r)/2;
		if (check(mid))
		{
			ans=mid;
			r=mid-1;
		}
		else
			l=mid+1;
	}
	cout<<ans;
	return 0;
}
