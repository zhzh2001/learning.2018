#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=2000,M=32000;
int top,n,m,ans;
set<int> s;
int b[M],f[M],v[M];
int a[N],g[N],w[N];
inline void prework()
{
	for (int i=2;i<M;++i)
	{
		if (!b[i])
			f[++top]=i;
		for (int j=1;j<=top&&(ll)i*f[j]<M;++j)
		{
			b[i*f[j]]=1;
			if (i%f[j]==0)
				break;
		}
	}
}
inline int gcd(int x,int y)
{
	return y==0?x:gcd(y,x%y);
}
inline int calc(int x)
{
	int sum=0;
	for (int i=1;i<=top;++i)
		while (x%f[i]==0)
		{
			x/=f[i];
			sum+=v[i];
		}
	if (x!=1)
	{
		if (s.count(x))
			--sum;
		else
			++sum;
	}
	return sum;
}
int main()
{
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	prework();
	read(n);
	read(m);
	for (int i=1;i<=n;++i)
		read(a[i]);
	for (int i=1;i<=m;++i)
		read(w[i]);
	//sort(w+1,w+1+m);
	int l=1;
	for (int i=1;i<=top;++i)
	{
		if (f[i]==w[l])
		{
			v[i]=-1;
			++l;
		}
		else
			v[i]=1;
	}
	for (int i=l;i<=m;++i)
		s.insert(w[i]);
	g[1]=a[1];
	for (int i=2;i<=n;++i)
		g[i]=gcd(g[i-1],a[i]);
	for (int i=1;i<=n;++i)
		ans+=calc(a[i]);
	int p=1;
	for (int i=n;i>=1;--i)
	{
		int q=calc(g[i]/p);
		if (q<0)
		{
			ans-=q*i;
			p=g[i];
		}
	}
	cout<<ans;
	return 0;
}