/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define mod 1000000007
#define N 55
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,w[N],f[N][N][N][N],g[N][N],a,b,p[N],tot;
inline int pd(int l,int r,int mi,int ma){
	F(i,l,r) if (w[i]>=mi&&w[i]<=ma) return 1;
	return 0;
}
int dp(int,int);
inline int get_f(int l,int r,int mi,int ma){
	if (f[l][r][mi][ma]!=-1) return f[l][r][mi][ma];
	int pd1=0;
	F(i,l,r) pd1+=(w[i]>=mi&&w[i]<=ma);
	if (pd1==r-l+1) return f[l][r][mi][ma]=0;
	if (!pd(l,r,mi,ma)) return f[l][r][mi][ma]=dp(l,r);
    f[l][r][mi][ma]=inf;
    F(i,l,r-1){
        f[l][r][mi][ma]=min(f[l][r][mi][ma],get_f(l,i,mi,ma)+get_f(i+1,r,mi,ma));
		f[l][r][mi][ma]=min(f[l][r][mi][ma],dp(l,i)+get_f(i+1,r,mi,ma));
	}
    return f[l][r][mi][ma];
}
inline int dp(int l,int r){
	if (g[l][r]!=-1) return g[l][r];
	g[l][r]=inf;
	F(i,1,tot){
		F(j,i,tot){
			if (pd(l,r,i,j)) g[l][r]=min(g[l][r],get_f(l,r,i,j)+a+(p[j]-p[i])*(p[j]-p[i])*b);
		}
	}
	return g[l][r];
}
signed main(){
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	n=read();a=read();b=read();
	F(i,1,n) w[i]=read(),p[i]=w[i];
	sort(p+1,p+n+1);tot=unique(p+1,p+n+1)-p-1;
	F(i,1,n) w[i]=lower_bound(p+1,p+tot+1,w[i])-p;
	memset(g,-1,sizeof(g));memset(f,-1,sizeof(f));
	wrn(dp(1,n));
	return 0;
}
