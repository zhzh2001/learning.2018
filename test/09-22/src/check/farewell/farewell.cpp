#include<cstdio>
#include<cstring>
#include<algorithm>
#define MXL 1000000+10
#define MXN 100000+10
using namespace std;
int l,n,m;
long long a[MXL],b[MXL];
class Solve{
	public:
		int s;
		long long k[MXN],v[MXN];
		void Init(int N){
			s=N;
			for(int i=1;i<=s;i++) scanf("%lld",&v[i]);
			sort(v+1,v+s+1);
			for(int i=1;i<=s;i++) k[i]=v[i];
			return;
		}
		long long Top(){return k[1];}
		void Adjust(){
			int t=1;
			k[1]+=v[1];
			while(t<s){
				int temp=t<<1;
				if(temp<=s&&k[temp+1]<k[temp]) temp++;
				if(k[t]>k[temp]) swap(k[t],k[temp]),swap(v[t],v[temp]),t=temp;
				else return;
			}
		}
}HA,HB;
long long ans;
int main(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	scanf("%d%d%d",&l,&n,&m);
	HA.Init(n);HB.Init(m);
	for(int i=1;i<=l;i++){
	    a[i]=HA.Top(),HA.Adjust();
	    b[i]=HB.Top(),HB.Adjust();
    }
	for(int i=1;i<=l;i++) ans=max(ans,a[i]+b[l-i+1]);
	printf("%lld",ans);
	return 0;
}

