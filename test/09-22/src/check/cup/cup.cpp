#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<string>
#include<algorithm>
#include<vector>
#include<map>
#include<set>
#include<list>
#include<queue>
#include<stack>
#include<bitset>
#include<deque>
using namespace std;
#define ll long long
#define inf 0x3f3f3f3f
#define ri register int
#define il inline
#define fi first
#define se second
#define mp make_pair
#define pi pair<int,int>
#define mem0(x) memset((x),0,sizeof (x))
#define mem1(x) memset((x),0x3f,sizeof (x))
#define pb push_back
#define gc getchar
template<class T>void in(T &x)
{
    x = 0; bool f = 0; char c = gc();
    while (c < '0' || c > '9') {if (c == '-') f = 1; c = gc();}
    while ('0' <= c && c <= '9') {x = (x << 3) + (x << 1) + (c ^ 48); c = gc();}
    if (f) x = -x;
}
#undef gc
#define N 5010
#define int ll
int n, m;
int bad[N];
int a[N];
map<int, bool>p;
il int gcd(int a, int b) {
    if (a < b) swap(a, b);
    int t;
    while (b) {
        t = a, a = b, b = t % b;
    }
    return a;
}
int b[N], cnt;
map<int, int>s;
il int divi(int x) {
    if (x == 1 || x == 0) return 0;
    if (s[x]) return b[s[x]];
    int ret = 0;
    for (ri i = 2, mi = sqrt(x); i <= mi; ++i) {
        if (x % i == 0) {
            int k = p[i] ? -1 : 1;
            while (x % i == 0) {
                x /= i;
                ret += k;
            }
        }
    }
    if (x > 1) {
        ret += (p[x] ? -1 : 1);
    }
    b[++cnt] = ret;
    s[x] = cnt;
    return ret;
}
int ans;
int f[N];
signed main() {
		freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout); 
    in(n), in(m);
    for (ri i = 1; i <= n; ++i) {
        in(a[i]);
    }
    for (ri i = 1; i <= m; ++i) {
        in(bad[i]);
        p[bad[i]] = 1;
    }
    for (ri i = 1; i <= n; ++i) {
        ans += divi(a[i]);
        f[i] = gcd(a[i], f[i - 1]);
    }
    int lst = 1;
    for (ri i = n, t; i >= 1; --i) {
        int g = f[i];
        /*for (ri j = 1; j <= i; ++j) {
            g = gcd(a[j], g);
        }*/
        g /= lst;
        t = divi(g);
        //printf("G %lld %lld\n", g, divi(g));
        if (t < 0) {
            lst *= g;
            ans -= t * i;
            /*for (ri j = 1; j <= i; ++j) {
                a[j] /= g;
            }*/
        }
    }
    /*for (ri i = 1; i <= n; ++i) {
        ans += divi(a[i]);
        //printf("A %lld\n", a[i]);
    }*/
    printf("%lld", ans);
    return 0;
}
