#include <algorithm>
#include <iostream>
#include <cstdio>
#include <queue>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return -1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10;
ll n,m,l,a[N],ans,num,tot;
struct node{
	ll ed,add;
	node(){}
	node(ll E,ll A):ed(E),add(A){}
	bool operator <(const node&res)const{
		return ed+add>res.ed+res.add;
	}
};
priority_queue<node>Ha;
struct Node{
	ll ed,add;
	Node(){}
	Node(ll E,ll A):ed(E),add(A){}
	bool operator <(const Node&res)const{
		return ed>res.ed;
	}
};
priority_queue<Node>H1;//正在工作的机器  
priority_queue<ll,vector<ll>,greater<ll> >H2;//等待工作的机器  
int main(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	read(l),read(n),read(m);
	for (ll i=1,x;i<=n;++i) read(x),Ha.push(node(0ll,x));
	for (ll i=1,x;i<=m;++i) read(x),H1.push(Node(0ll,x));
	for (ll i=1;i<=l;++i){
		node x=Ha.top();
		Ha.pop();
		a[++num]=x.ed;
		x.ed+=x.add;
		Ha.push(x);
	}
	while (!Ha.empty()){a[++num]=Ha.top().ed;Ha.pop();}
	sort(a+1,a+1+num);
//tot表示当前需要制作的数量  
	for (ll i=1;i<=num;++i){
		if (!a[i]) continue;
		ans=a[i];
		while (!H1.empty()&&H1.top().ed<=a[i]) H2.push(H1.top().add),H1.pop();
		++tot;
		while (!H2.empty()&&tot){
			ll x=H2.top();
//		cout<<a[i]<<' '<<x<<'\n';
			H2.pop();
			H1.push(Node(x+a[i],x));
			--tot;
		}
	}
	while (!H1.empty()){
		ans=max(ans,H1.top().ed);
		H1.pop();
	}
	wr(ans);
	return 0;
}
/*
10 10 10
28571 22507 8115 15837 23501 19946 13370 10754 29297 28672
13876 7330 7464 9049 20461 18793 26937 1841 3712 2200
^Z
24345
23501
22507
21508
19946
15837
13370
28838
*/
