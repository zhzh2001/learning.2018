#include <algorithm>
#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return -1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
ll n,a,b,f[100][100],w[200],Max[100][100],Min[100][100];
inline ll sqr(ll x){return x*x;}
inline ll solve(ll l,ll r,ll L,ll R){
	ll Mx=max(Max[l][r],Max[L][R]),Mn=min(Min[l][r],Min[L][R]);
	return b*sqr(Mx-Mn);
}
int main(){
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	read(n);
	read(a),read(b);
	for (ll i=1;i<=n;++i) read(w[i]);
	memset(f,0x3f,sizeof f);
	for (ll i=0;i<=n;++i) for (ll j=0;j<=n;++j) Min[i][j]=1e9,Max[i][j]=-1e9;
	for (ll i=0;i<=n;++i) for (ll j=0;j<i;++j) f[i][j]=0;
	for (ll i=1;i<=n;++i) for (ll j=i;j<=n;++j) for (ll k=i;k<=j;++k) Max[i][j]=max(Max[i][j],w[k]),Min[i][j]=min(Min[i][j],w[k]);
	for (ll i=n;i;--i)
		for (ll j=i;j<=n;++j){
			for (ll k=i;k<=j;++k) f[i][j]=min(f[i][j],f[i][k]+solve(i,i-1,k+1,j)+a);
			for (ll k=i;k<=j;++k) f[i][j]=min(f[i][j],solve(i,k,j+1,j)+f[k+1][j]+a);
			for (ll k=i;k<=j;++k) for (ll l=k;l<=j;++l) f[i][j]=min(f[i][j],f[k+1][l]+solve(i,k,l+1,j)+a);
		}
	wr(f[1][n]);
	return 0;
}
/*
25
100 5
29 26 27 26 32 25 23 25 25 18 21 20 23 26 27 21 27 27 24 27 32 41 29 42 24
*/
