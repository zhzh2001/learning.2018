#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return -1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=2010;
ll n,m,a[N],b[N],ans;
inline ll gcd(ll a,ll b){return b?gcd(b,a%b):a;}
inline ll solve(ll x){
	ll ret=0;
	for (ll i=2;i*i<=x;++i) if (x%i==0){
		ll v=(*lower_bound(b+1,b+1+m,i)==i)?-1:1;
		while (x%i==0) x/=i,ret+=v;
	}
	if (x>1) ret+=(*lower_bound(b+1,b+1+m,x)==x)?-1:1;
	return ret;
}
int main(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	read(n),read(m);
	for (ll i=1;i<=n;++i) read(a[i]);
	for (ll i=1;i<=m;++i) read(b[i]);
	for (ll i=n;i;--i){
		ll tt=a[1];
		for (ll j=2;j<=i;++j) tt=gcd(tt,a[j]);
		if (solve(tt)<0) for (ll j=1;j<=i;++j) a[j]/=tt;
	}
	ans=0;
	for (ll i=1;i<=n;++i) ans+=solve(a[i]);
	wr(ans);
	return 0;
}
/*
in:
3 1
2 4 8
2
out:
-3

in:
5 2
4 20 34 10 10
2 5
out:
-2
*/
