#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=55,INF=1e9;
int n,a,b,c[N];
inline void init(){
	n=read(); a=read(); b=read();
	for (int i=1;i<=n;i++) c[i]=read();
}
int mx[N][N],mn[N][N],dp[N][N][N][N];
inline int sqr(int x){
	return x*x;
}
inline int f(int x){
	return x*b+a;
}
int dfs(int l,int r,int x,int y){
	if (dp[l][r][x][y]!=-1) return dp[l][r][x][y];
	int &ans=dp[l][r][x][y]; ans=INF;
	if (!x&&!y) ans=min(ans,f(sqr(mn[l][r]-mx[l][r])));
		else ans=min(ans,); //ȫ��ȥ 
	for (int i=1;i<=) ans=min(ans,sqr(mn[]));
}
inline void solve(){
	memset(dp,-1,sizeof dp);
	for (int i=1;i<=n;i++){
		mx[i][i]=mn[i][i]=i;
		for (int j=i+1;j<=n;j++){
			if (c[mn[i][j-1]]>c[j]) mn[i][j]=j; else mn[i][j]=mn[i][j-1];
			if (c[mn[i][j-1]]<c[j]) mx[i][j]=j; else mx[i][j]=mx[i][j-1];
			mn[i][j]=mn[j][i]; mx[i][j]=mx[j][i];
		}
	}
	writeln(dfs(1,n,0,0));
}
int main(){	
	freopen("war.in","r",stdin); freopen("war.out","w",stdout);
	init(); solve();
	return 0;
}
