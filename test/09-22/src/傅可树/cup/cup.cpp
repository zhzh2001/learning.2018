#include<cstdio>
#include<map>
#include<algorithm>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=2005,M=1e5;
map<int,int> mp;
int n,m,a[N],b[N];
inline void init(){
	n=read(); m=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<=m;i++) b[i]=read(),mp[b[i]]=1;
}
bool mark[M];
int pri[M],tot;
inline void pre(){
	for (int i=2;i<M;i++){
		if (!mark[i]) pri[++tot]=i;
		for (int j=1;j<=tot&&i*pri[j]<M;j++){
			mark[i*pri[j]]=1;
			if (i%pri[j]==0) break;
		}
	}
}
inline int f(int x){
	int tmp=0;
	for (int i=1;pri[i]*pri[i]<=x;i++){
		while (x%pri[i]==0){
			x/=pri[i]; 
			if (mp[pri[i]]==1) tmp--; else tmp++; 
		}
	}
	if (x!=1) 
		if (mp[x]==1) tmp--; else tmp++;
	return tmp;
}
int c[N],v[N],pr,ans,be;
int gcd(int x,int y){return (!y)?x:gcd(y,x%y);}
inline void DP(){
	for (int i=n;i;i--){
		if (v[i]>0){
			be=i; break;
		}
	}
	int tmp=v[be];
	for (int i=be;i;i--){
		tmp=max(tmp,v[i]);
		ans+=tmp;
	}
}
inline void solve(){
	pre();
	for (int i=1;i<=n;i++) c[i]=f(a[i]),ans+=c[i];
	pr=a[1]; v[1]=-f(a[1]);
	for (int i=2;i<=n;i++){
		v[i]=-f(pr=gcd(pr,a[i]));
	}
	DP();
	writeln(ans);
}
int main(){
	freopen("cup.in","r",stdin); freopen("cup.out","w",stdout);
	init(); solve();
	return 0;
}
