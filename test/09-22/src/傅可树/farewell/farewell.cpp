#include<cstdio>
#include<cstring>
#include<algorithm>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e5+5,INF=1e15;
struct edge{
	int link,next,v,opp;
}e[N<<3];
int n,m,l,a[N],b[N];
inline void init(){
	l=read(); n=read(); m=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<=m;i++) b[i]=read();
}
int tot,cur[N<<1],head[N<<1],S,T,mid;
inline void add_edge(int u,int v,int w){
	e[++tot]=(edge){v,head[u],w,0}; head[u]=tot;
}
inline void insert(int u,int v,int w){
	add_edge(u,v,w); e[tot].opp=tot+1; add_edge(v,u,0); e[tot].opp=tot-1;
}
inline bool clean(){
	for (int i=S;i<=T;i++){
		cur[i]=head[i];
	}
}
int q[N<<1],h,t,dis[N<<1];
inline bool bfs(){
	for (int i=S;i<=T;i++) dis[i]=-1;
	h=t=0; q[++t]=S; dis[S]=0;
	while (h<t){
		int u=q[++h];
		for (int i=head[u];i;i=e[i].next){
			int v=e[i].link;
			if (e[i].v&&dis[v]==-1){
				dis[v]=dis[u]+1; q[++t]=v;
				if (v==T) return 1;
			}
		}
	}
	return 0;
}
inline void back(int i,int w){
	e[i].v-=w; e[e[i].opp].v+=w;
}
inline int dinic(int u,int flow){
	if (u==T||!flow) return flow;
	int used=0;
	for (int i=cur[u];i;i=e[i].next){
		int v=e[i].link;
		if (e[i].v&&dis[v]==dis[u]+1){
			int w=dinic(v,min(flow-used,e[i].v)); used+=w;
			back(i,w);
			if (w) cur[u]=i;
			if (used==flow) return flow;
		}
	}
	if (!used) dis[u]=-1;
	return used;
}
inline bool judge(int x){
	S=0; mid=n+m+1; T=n+m+2; int ans=0;
	for (int i=S;i<=T;i++) head[i]=0;
	for (int i=1;i<=n;i++){
		int tmp; if (!a[i]) tmp=INF; else tmp=x/a[i];
		insert(S,i,tmp);
		insert(i,mid,INF);
	}
	for (int i=1;i<=m;i++){
		int tmp; if (!b[i]) tmp=INF; else tmp=x/b[i];
		insert(i+n,T,tmp);
		insert(mid,i+n,INF);
	}
	while (bfs()){
		clean();
		ans+=dinic(S,INF);
		if (ans>=l) return 1;
	}
	return 0;
}
inline void solve(){
	if (l==1) {
		int ANS=0,mn=INF;
		for (int i=1;i<=n;i++) mn=min(mn,a[i]);
		ANS+=mn; mn=INF;
		for (int i=1;i<=m;i++) mn=min(mn,b[i]);
		ANS+=mn;
		writeln(ANS); return;
	}
	int l=0,r=INF;
	while (l<r){
		int MID=(l+r)>>1;
		if (judge(MID)) r=MID; else l=MID+1;
	}
	writeln(l);
}
signed main(){
	
	freopen("farewell.in","r",stdin); freopen("farewell.out","w",stdout);
	init(); solve();
	return 0;
}
