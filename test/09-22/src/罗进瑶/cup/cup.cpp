#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0'); }
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
}
const int M = 1200000, N = 1200000; 
int f[M];
set<int>g;
int n, m, a[N], b[N];
int pri[N], tot;
bool vis[M];
void shai() {
	for(int i = 2; i <= 1000000; ++i)
		if(!vis[i]) {
			pri[++tot] = i;
			for(int j = i + i; j <= 1000000; j += i)
				vis[j] = 1; 
		}
}
ll gcd(ll a, ll b) {
	if(!b) return a;
	return a % b ? gcd(b, a % b) : b;
}
map<int, bool>v;
ll F(int n) {
	if(v[n]) return f[n];
	if(n == 1) return 0;
	v[n] = 1;
	for(int i = 1; i <= tot; ++i)
		if(n % pri[i] == 0) {
			if(g.count(pri[i])) return f[n] = F(n / pri[i]) - 1;
			else return f[n] = F(n / pri[i]) + 1;
		}
	return g.count(n);
}
ll get(int x) {
	ll res = 0;
	for(int i = 1; i <= x; ++i)
		res += F(a[i]);
	return res;
}
ll ans;
void dfs(ll sum) {
	if(sum > ans) ans = sum;
	ll t = a[1];
	for(int i = 1; i <= n; ++i) {
		t = gcd(t, a[i]);
		if(t == 1) return;
		for(int j = 1; j <= i; ++j) sum -= F(a[j]),a[j] /= t;
		dfs(sum + get(i));
		for(int j = 1; j <= i; ++j) a[j] *= t, sum += F(a[j]);
	}
}
int main() {
	setIO();
	n = read(), m = read();
	for(int i = 1; i <= n; ++i)
		a[i] = read();
	for(int i = 1; i <= m; ++i)
		g.insert(b[i] = read());
	shai();
	for(int i = 1; i <= n; ++i)
		ans += F(a[i]);
	dfs(ans);
	writeln(ans);
	return 0;
}
