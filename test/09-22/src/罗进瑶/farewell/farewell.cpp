#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0'); }
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
}
const int N = 150000; 
int tot, n, m;
ll a[N] ,b[N];
ll s[N];
struct T {
	ll s, d;
	bool operator < (const T &rhs) const {
		return s + d > rhs.s + rhs.d;
	}
};
ll ans;
priority_queue<T>p;
bool fg3 = 1;
int main() {
	setIO();
	tot = read(), n = read(), m = read();
	for(int i = 1; i <= n; ++i) a[i] = read(), p.push((T){0, a[i]});
	for(int i = 1; i <= m; ++i) b[i] = read(), fg3 &= (b[i] == 0);
	if(fg3) {
		for(int XX = 1; XX <= tot; ++XX){
			T now = p.top(); p.pop();
			ans = max(ans, now.s + now.d);
			now.s += now.d;
			p.push(now);
		}
		writeln(ans);
		return 0; 
	}
	if(tot == 1 || (ll) n * m > (1e8)) {
		ll x = 1e17, y = 1e17;
		for(int i = 1;  i<= n; ++i)
			x = min(a[i], x);
		for(int i = 1; i <= m; ++i)
			y = min(b[i] ,y);
		writeln(x + y);
		return 0;
	}
	for(int XX = 1; XX <= tot; ++XX) {
		T now = p.top();p.pop();
		ll res = 1e18, k = 0;
		for(int j = 1; j <= m; ++j)
			if(max(s[j], now.s + now.d) + b[j] < res)
				res = max(s[j], now.s + now.d) + b[j] , k = j;
		s[k] =  max(s[k], now.s + now.d) + b[k];
		ans = max(ans, res);
		now.s += now.d;
		p.push(now);
	} 
/*	for(int XX = 1; XX <= tot; ++XX) {
		T now = p.top();p.pop();
		T y = q.top(); q.pop();
		ans = max(ans, max(now.s + now.d, y.s) + y.d);
	//	now.s += now.d;
		y.s = max(now.s + now.d, y.s) + y.d;
//		ll res = 1e18, k = 0;
		for(int j = 1; j <= m; ++j)
			if(max(s[j], now.s + now.d) + b[j] < res)
				res = max(s[j], now.s + now.d) + b[j] , k = j;
		s[k] =  max(s[k], now.s + now.d) + b[k];
		ans = max(ans, res);*/
	/*	now.s += now.d;
		p.push(now);
		q.push(y);
	} */
	writeln(ans);
	return 0;
}
