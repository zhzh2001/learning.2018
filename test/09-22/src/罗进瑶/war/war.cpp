#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
#define sqr(x) ((x) * (x))
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0'); }
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
}
const int N = 1234;
int n;
ll a, b;
ll w[N];
ll f[1 << 17], tot;
bool fg = 1;
int main() {
	setIO();
	n = read(), a = read(), b = read();
	for(int i = 1; i <= n; ++i) w[i] = read();
	for(int i = 2;  i<= n; ++i) fg &= (w[i] == w[i - 1]);
	if(fg) {
		writeln(a);
		return 0;
	}
	if(n > 6) {
		ll x = 1e9, y = -1e9;
		for(int i = 1; i <= n; ++i) {
			x = min(x, w[i]);
			y = max(y, w[i]);
		}
		writeln(a + b * sqr(x - y));
		return 0;
	} 
	memset(f, 0x3f, sizeof f);
	f[0] = 0;
	for(int S = 0; S < (1 << n); ++S)
		for(int j = 1; j < (1 << n); ++j)
			if((S & j) == 0) {
				ll x = -1e9, y = 1e9;
				int p = 0, q = 0;
				for(int k = 0; k < n; ++k) if((1 << k) & j) {
					p = k; break;
				}
				for(int k = n - 1; k >= 0; --k) if((1 << k) & j) {
					q = k; break;
				}
				bool fg = 1;
				for(int k = p; k <= q; ++k) if(!((S >> k & 1) || (j >> k & 1))) {fg = 0; break;}
				if(!fg) continue;
				for(int k = 1; k <= n; ++k)
					if(j & 1 << (k - 1)) {
						x = max(x, w[k]);
						y = min(y, w[k]);
					}
				f[S | j] = min(f[S | j], f[S] + a + b * sqr(x - y));
			}
	writeln(f[(1 << n) - 1]);
	return 0;
}
