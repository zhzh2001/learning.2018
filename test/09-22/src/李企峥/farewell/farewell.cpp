#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
using namespace std;
inline ll read()
{
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
struct D{
	ll t,k;
};
priority_queue<D>q;
ll l,n,m,ans;
ll a[100100],b[100100],f[100100],g[100100];
bool operator <(D a,D b)
{
	if(a.t==b.t)return a.k>b.k;
	else return a.t>b.t;
}
int main()
{
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	l=read();n=read();m=read();
	For(i,1,n)a[i]=read();
	For(j,1,m)b[j]=read();
	For(i,1,n)
	{
		D k;
		k.t=a[i],k.k=a[i];
		q.push(k);
	}
	For(i,1,l)
	{
		D t=q.top();
		f[i]=t.t;
		q.pop();
		t.t=t.t+t.k;
		q.push(t);
	}
	while(!q.empty())q.pop();
	For(i,1,m)
	{
		D k;
		k.t=b[i],k.k=b[i];
		q.push(k);
	}
	For(i,1,l)
	{
		D t=q.top();
		g[i]=t.t;
		q.pop();
		t.t=t.t+t.k;
		q.push(t);
	}
	For(i,1,l)ans=max(ans,f[i]+g[l-i+1]);
	cout<<ans<<endl;
	return 0;
}
