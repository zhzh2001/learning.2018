#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 10010
#define int long long
#define inf 0x3f3f3f3f3f3f3f3f
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int a[maxn],b[maxn],n,m,l,r;
int check(int k)
{
	int now=0;
	for(int i=1;i<=n;i++)
		now+=k/a[i];
	return now>=l;
}
int check2(int k)
{
	int now=0;
	for(int i=1;i<=m;i++)
		now+=k/b[i];
	return now>=l;
}
int solve1()
{
	int l=0,r=inf;
	while(l<r-1)
	{
		int m=l+r>>1;
		if(check(m)) r=m;
			else l=m;
	}
	return r;
}
int solve2()
{
	int l=0,r=inf;
	while(l<r-1)
	{
		int m=l+r>>1;
		if(check2(m)) r=m;
			else l=m;
	}
	return r;
}
signed main()
{
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	l=read();
	n=read();
	m=read();
	for(int i=1;i<=n;a[i++]=read());
	for(int i=1;i<=m;b[i++]=read());
	sort(a+1,a+n+1);
	sort(b+1,b+m+1);
	if(l==1)
	{
		write(a[1]+b[1]);
		return 0;
	}
	if(b[1]==b[m]&&b[1]==0)
	{
		write(solve1());
		return 0;
	}
	write(solve1()+solve2());
	return 0;
}
