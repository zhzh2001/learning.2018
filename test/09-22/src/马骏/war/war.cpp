#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 20
#define inf 0x3f3f3f3f
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int ans,c[maxn],n,a,b,d[maxn];
void dfs(int l,int r,int cost)
{
	if(l>r)
	{
		ans=min(ans,cost);
		return;
	}
	int tmp[maxn];
	for(int i=l;i<=r;i++)
		for(int j=i;j<=r;j++)
		{
			int mi=inf,ma=-inf;
			for(int k=i;k<=j;k++)
			{
				mi=min(mi,c[k]);
				ma=max(ma,c[k]);
			}
			for(int k=1;k<=n;k++)
				tmp[k]=c[k];
			for(int k=i;k<=j;k++)
				c[k]=c[k+j-i+1];
			dfs(l,r-j+i-1,cost+(ma-mi)*(ma-mi)*b+a);
			for(int k=1;k<=n;k++)
				c[k]=tmp[k];
		}
}
signed main()
{
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	n=read();
	a=read();
	b=read();
	for(int i=1;i<=n;i++)
		c[i]=d[i]=read();
	sort(d+1,d+n+1);
	if(d[1]==d[n])
	{
		write(a+b);
		return 0;
	}
	ans=inf;
	dfs(1,n,0);
	write(ans);
	return 0;
}
