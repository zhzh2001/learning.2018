#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 2010
#define inf 0x3f3f3f3f
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int vis[100000],prm[100000],r,c[maxn],n,m,ma,a[maxn],val[maxn],lastgcd,ok[maxn],f[maxn],ans;
void sushu(int k)
{
	for(int i=2;i<=sqrt(k);i++)
		for(int j=i*i;!vis[i]&&j<=k;j+=i)
			vis[j]=1;
	for(int i=2;i<=k;i++)
		if(!vis[i]) prm[++r]=i;
}
int fenjie(int x)
{
	// wln(x);
	int ans=0;
	for(int i=1;i<=r;i++)
	// {
		// putchar(':');
		// wln(prm[i]);
		if(!(x%prm[i]))
		{
			int val=0;
			for(;x%prm[i]==0;x/=prm[i])
				val++;
			if((*lower_bound(c+1,c+m+1,prm[i]))!=prm[i]) ans+=val;
				else ans-=val;
			// wln(ans);
		}
	// }
	if(x!=1)
	{
		if((*lower_bound(c+1,c+m+1,x))!=x) ans++;
			else ans--;
	}
	return ans;
}
int gcd(int x,int y)
{
	return y?gcd(y,x%y):x;
}
signed main()
{
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;ma=max(ma,a[i++]=read()));
	sushu((int)sqrt(ma));
	for(int i=1;i<=m;i++)
		c[i]=read();
	for(int i=1;i<=n;i++)
		val[i]=fenjie(a[i]);
	// for(int i=1;i<=r;i++)
		// wrs(prm[i]);
	// putchar('\n');
	// for(int i=1;i<=n;i++)
		// wrs(val[i]);
	lastgcd=a[1];
	ok[1]=fenjie(a[1]);
	// wrs(lastgcd);
	for(int i=2;i<=n;i++)
	{
		int nowgcd=gcd(lastgcd,a[i]);
		ok[i]=fenjie(nowgcd);
		lastgcd=nowgcd;
		// wrs(lastgcd);
	}
	ok[n+1]=0;
	// for(int i=1;i<=n;i++)
		// wrs(ok[i]);
	// putchar('\n');
	for(int i=1;i<=n;i++)
		f[i]=-inf;
	for(int i=1;i<=n;i++)
		f[n+1]+=val[i];
	// wln(f[n+1]);
	for(int i=n;i>0;i--)
		for(int j=i+1;j<n+2;j++)
			f[i]=max(f[i],f[j]+(ok[j]-ok[i])*i);
	// for(int i=1;i<=n;i++)
		// wrs(f[i]);
	ans=-inf;
	for(int i=1;i<=n;i++)
		ans=max(ans,f[i]);
	write(ans);
	return 0;
}
