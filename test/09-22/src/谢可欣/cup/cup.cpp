#include <iostream>
#include <cstdio>
//#define  int long long
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
const int N=1e6+1;
int tot=0,n,m,f[N],a[3000],p[3000],g[3000],tmp[3000],x[3000];
bool inp[N],vis[N],bad[N];
int gcd(int a,int b){
    return b?gcd(b,a%b):a;
}
int js(int x){
	if(x==1)return 0;
	if(vis[x])return f[x];
	vis[x]=1;
	for(int i=1;i<=tot;i++){
		if(x%p[i]==0){
//			cout<<p[i]<<" "<<bad[p[i]]<<" "<<bad[2]<<endl;
			if(bad[p[i]]==1)f[x]=js(x/p[i])-1;
			else f[x]=js(x/p[i])+1;
			break;
		}
	}
	return f[x];
}
signed main()
{
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=n;i++) a[i]=read(),tmp[i]=a[i];
	for(int i=1;i<=m;i++) x[i]=read(),bad[x[i]]=1;
	g[0]=0;int ff=1;
	for(int i=1;i<=n;i++){
		g[i]=gcd(a[i],g[i-1]);
		if(g[i]!=1)ff=0;
//		cout<<g[i-1]<<" "<<a[i]<<" "<<g[i]<<endl;
	}
	f[1]=0;vis[1]=1;
	for(int i=2; i<=3000; i++){
		if(!inp[i])p[++tot]=i;
		for(int j=1; j<=tot&&i*p[j]<=3000; j++) {
			inp[i*p[j]]=1;
			if(i%p[j]==0)break;
		}
	}
	if(ff){
		int ans=0;
		for(int i=1;i<=n;i++)js(a[i]);
		for(int i=1;i<=n;i++)ans+=f[a[i]];//,cout<<f[a[i]]<<" ";puts("");
		cout<<ans<<endl;
		return 0;
	}
	for(int i=1;i<=n;i++)js(a[i]);
//	for(int i=1;i<=n;i++)cout<<a[i]<<" ";puts("");
//	for(int i=1;i<=n;i++)cout<<f[a[i]]<<" ";puts("");
	/*int ans=-10000000;
	for(int i=1;i<=n;i++){
		int sum=0;
		for(int j=1;j<=n;j++)sum+=f[a[j]];
		cout<<sum<<" ";
		ans=max(ans,sum);
		for(int j=1;j<=n;j++)a[j]=tmp[j];
		for(int j=1;j<=i;j++)a[j]/=g[i];//,cout<<a[j]<<" ";puts("");
		for(int j=1;j<=n;j++)cout<<a[j]<<" ";puts("");
	}
	cout<<ans<<endl;*/
	int ans=0,flag=0;
	int tp=a[1]; 
	for(int i=1;i<=m;i++)if(tp%x[i]==0){
		flag=1;
	}
	if(flag){
		a[1]=1;
    	for(int i=2;i<=n;i++){
		    tp=gcd(tp,a[i]);
            a[i]/=tp;
	    }
	  	for(int i=1;i<=n;i++) ans+=f[a[i]];
      	cout<<ans<<endl;
    }
    else{
		int ans=-10000000;
		for(int i=1; i<=n; i++) {
			int sum=0;
			for(int j=1; j<=n; j++)sum+=f[a[j]];
//			cout<<sum<<" ";
			ans=max(ans,sum);
			for(int j=1; j<=n; j++)a[j]=tmp[j];
			for(int j=1; j<=i; j++)a[j]/=g[i]; 
		}
		cout<<ans<<endl;
	}
	return 0;
}
