#include <iostream>
#include <cstdio>
#include <queue>
#define int long long
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
struct arr{
	int rt,sum;
	bool operator < (const arr &b) const{
		return sum>b.sum;
	}
};
priority_queue<arr> qa,qb;
int a[100005],b[100005],sa[100005],sb[100005];
signed main()
{
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	int k=read(),n=read(),m=read();
	if(k==1){
		int mna=1e9+7,mnb=1e9+7,x;
		for(int i=1;i<=n;i++)x=read(),mna=min(mna,x);
		for(int i=1;i<=m;i++)x=read(),mnb=min(mnb,x);
		cout<<mna+mnb<<endl;
		return 0;
	}
	int flag=1;
	for(int i=1;i<=n;i++)a[i]=read(),qa.push((arr){i,a[i]});
	for(int i=1;i<=m;i++){
	    b[i]=read(),qb.push((arr){i,b[i]});
	    if(b[i]!=0)flag=0;
	}
	if(flag){
		arr aa;
		for(int i=1;i<=k;i++){
		    aa=qa.top();qa.pop();qa.push((arr){aa.rt,aa.sum+a[aa.rt]});
		}
		cout<<aa.sum<<endl;
		return 0;
	}
	arr aa,bb;
	int mxa=0,mxb=0,mna=1e9+7,mnb=1e9+7;
	for(int i=1;i<=k;i++){
		aa=qa.top();qa.pop();qa.push((arr){aa.rt,aa.sum+a[aa.rt]});
		sa[aa.rt]++;
		bb=qb.top();qb.pop();qb.push((arr){bb.rt,bb.sum+b[bb.rt]});
		sb[bb.rt]++;
//		cout<<aa.rt<<" "<<bb.rt<<endl;
//		cout<<aa.sum<<" "<<bb.sum<<endl;
	}
	for(int i=1;i<=n;i++){
		if(sa[i]){
			mxa=max(mxa,sa[i]*a[i]);
			mna=min(mna,sa[i]*a[i]);
		}
	}
	for(int i=1;i<=m;i++){
		if(sb[i]){
			mxb=max(mxb,sb[i]*b[i]);
			mnb=min(mnb,sb[i]*b[i]);
		}
	}
	int ans=mna+mxb;
	cout<<ans<<endl;
	return 0;
}
/*
10 10 10
28571 22507 8115 15837 23501 19946 13370 10754 29297 28672
13876 7330 7464 9049 20461 18793 26937 1841 3712 2200

26189
*/
