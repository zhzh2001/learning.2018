#include <cstdio>
#include <queue>
#include <algorithm>
using namespace std;
const int N=1e5+7;
#define ll long long
struct node {
	ll T,id,v;
	bool operator <(node x) const {
		return T+v>x.T+x.v;
	}
}A[N];
priority_queue<node> qa,qb;
ll l,n,m,cnta,cntb,cnt,ans;
ll a[N],b[N];
void Adb() {
	node k=qa.top();qa.pop();
	if (qb.empty()) {
		qb.push((node){k.T+b[1],1,b[1]});cntb=1;
		return;
	}
	if (cntb<m) {
		node kb=qb.top();
		if (max(kb.T,k.T)+kb.v<=k.T+b[cntb+1]) {
			qb.pop();
			qb.push((node){max(k.T,kb.T)+kb.v,kb.id,kb.v});
		}
		else cntb++,qb.push((node){k.T+b[cntb],cntb,b[cntb]});
	}
	else {
		node kb=qb.top();qb.pop();
		qb.push((node){max(k.T,kb.T)+kb.v,kb.id,kb.v});
	}
}
void Apop() {
	A[++cnt]=qa.top();qa.pop();A[cnt].v=0;
}
void Ada() {
	if (cnta<n) {
		node k=qa.top();
		if (k.T+k.v<=a[cnta+1]) {
			Apop();
			qa.push((node){k.T+k.v,k.id,k.v});
		}
		else cnta++,qa.push((node){a[cnta],cnta,a[cnta]});
	}
	else {
		node k=qa.top();
		Apop();
		qa.push((node){k.T+k.v,k.id,k.v});
	}
}
int main() {
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	scanf("%lld%d%d",&l,&n,&m);
	for (ll i=1;i<=n;i++) scanf("%lld",&a[i]);
	for (ll i=1;i<=m;i++) scanf("%lld",&b[i]);
	sort(a+1,a+n+1);sort(b+1,b+m+1);
	qa.push((node){a[1],1,a[1]});cnta=1;l--;
	while (l--) Ada();
	while (!qa.empty()) Apop();
	for (int i=1;i<=cnt;i++) qa.push(A[i]);
	while (!qa.empty()) Adb();
	while (!qb.empty()) ans=qb.top().T,qb.pop();
	printf("%lld\n",ans);
}
