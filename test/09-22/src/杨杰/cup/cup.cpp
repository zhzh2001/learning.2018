#include <cstdio>
#include <map>
#include <algorithm>
using namespace std;
const int N=2005;
#define ll long long
int n,m,mx;
ll ans;
int a[N],gd[N],bad[N];
map<int,int>flg,f,vis;
int gcd(int x,int y) {return !y?x:gcd(y,x%y);}
int F(int x) {
	if (vis[x]) return f[x];
	vis[x]=1;
	for (int i=2;i*i<=x;i++) {
		if (x%i==0) {
			if (flg[i]) f[x]=F(x/i)-1;
			else f[x]=F(x/i)+1;
			return f[x];
		}
	}
	if (flg[x]) f[x]=-1;
	else f[x]=1;
	return f[x];
}
int main() {
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	vis[1]=1;f[1]=0;
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]),mx=max(mx,a[i]);
	for (int i=1,w;i<=m;i++) scanf("%d",&bad[i]),flg[bad[i]]=1;
	gd[1]=a[1];
	for (int i=2;i<=n;i++) gd[i]=gcd(gd[i-1],a[i]);
	for (int i=n;i>=1;i--) {
		int p=0;
		for (int j=1;j<=m;j++) if (gd[i]%bad[j]==0) p=1;
		if (p) 	for (int j=1;j<=n;j++) a[j]/=gd[i],gd[j]/=gd[i];
	}
	for (int i=1;i<=n;i++) ans+=F(a[i]);
	printf("%lld\n",ans);
}
