#include <cmath>
#include <queue>
#include <cstdio>
#include <algorithm>
using namespace std;
#define int long long
const int N=500005;
int n,m,l,a[N],b[N],aa[N],bb[N],size=0;
struct node{int tim,de;}heap[N];
inline void Up(int x)
{
	while(x>1)
	{
		if(heap[x].tim<heap[x/2].tim){swap(heap[x],heap[x/2]); x/=2;}else break;
	}return;
}
inline void Down(int x)
{
	int y=x*2;
	while(y<=size)
	{
		if(y<size&&heap[y].tim>heap[y+1].tim)y++;
		if(heap[x].tim>heap[y].tim){swap(heap[x],heap[y]); x=y; y=x*2;}else break;
	}return;
}
inline void Insert(int tim,int de){size++; heap[size].tim=tim; heap[size].de=de; Up(size);}
inline node Top(){return heap[1];}
inline void Pop(){swap(heap[1],heap[size]); size--; Down(1);}
signed main()
{
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	int i,re=0; node tmp; scanf("%lld%lld%lld",&l,&n,&m);
	for(i=1;i<=n;i++){scanf("%lld",&a[i]); Insert(a[i],a[i]);}
	for(i=1;i<=l;i++){tmp=Top(); Pop(); aa[i]=tmp.tim; Insert(tmp.tim+tmp.de,tmp.de);}
	while(size)Pop();   for(i=1;i<=m;i++){scanf("%lld",&b[i]); Insert(b[i],b[i]);}
	for(i=1;i<=l;i++){tmp=Top(); Pop(); bb[i]=tmp.tim; Insert(tmp.tim+tmp.de,tmp.de);}
	reverse(bb+1,bb+l+1); for(i=1;i<=l;i++)re=max(re,aa[i]+bb[i]); printf("%lld\n",re);
}
