#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define M(a,v) memset(a,v,sizeof a)
const int N=20005;
int n,m,a[N],b[N],gg[N],ss[N],aa[N],ggg[N],sss[N];
bool bo[1000005];
inline int gcd(int x,int y){return (y==0)?x:gcd(y,x%y);}
int main()
{
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	int i,j,tmp,de,re=0; scanf("%d%d",&n,&m);for(i=1;i<=n;i++)scanf("%d",&a[i]);
	for(i=1;i<=m;i++)scanf("%d",&b[i]); sort(b+1,b+m+1); gg[1]=a[1]; for(i=2;i<=n;i++)gg[i]=gcd(gg[i-1],a[i]);
	if(b[m]<=1000000)
	{
		for(i=1;i<=m;i++)bo[b[i]]=1; memmove(aa,a,sizeof a); M(ss,0); M(sss,0);
		for(i=1;i<=n;i++)
		{
			for(j=2;j<=aa[i];j++)
			{
				if(aa[i]%j==0)
				{
					if(bo[j])de=-1;else de=1; while(aa[i]%j==0&&aa[i]>1){ss[i]+=de;aa[i]/=j;}
				}
			}re+=ss[i];
		}memmove(ggg,gg,sizeof gg);
		for(i=1;i<=n;i++)
		{
			for(j=2;j<=ggg[i];j++)
			{
				if(ggg[i]%j==0)
				{
					if(bo[j])de=-1;else de=1;	while(ggg[i]%j==0&&ggg[i]>1){sss[i]+=de;ggg[i]/=j;}
				}
			}
		}
		for(i=n;i>=1;i--)
		{
			if(sss[i]<0)
			{
				for(j=i-1;j>=1;j--)sss[j]-=sss[i]; re-=sss[i]*i;
			}
		}printf("%d\n",re);
	}
	else
	{
		memmove(aa,a,sizeof a); M(ss,0); M(sss,0);
		for(i=1;i<=n;i++)
		{
			for(j=2;j<=aa[i];j++)
			{
				if(aa[i]%j==0)
				{
					if(j<b[1]||j>b[m])de=1;
					else
					{
						tmp=lower_bound(b+1,b+m+1,j)-b;	if(b[tmp]==j)de=-1;else de=1;
					}while(aa[i]%j==0&&aa[i]>1){ss[i]+=de;aa[i]/=j;}
				}
			}re+=ss[i];
		}memmove(ggg,gg,sizeof gg);
		for(i=1;i<=n;i++)
		{
			for(j=2;j<=ggg[i];j++)
			{
				if(ggg[i]%j==0)
				{
					if(j<b[1]||j>b[m])de=1;
					else
					{
						tmp=lower_bound(b+1,b+m+1,j)-b; if(b[tmp]==j)de=-1;else de=1;
					}while(ggg[i]%j==0&&ggg[i]>1){sss[i]+=de;ggg[i]/=j;}
				}
			}
		}
		for(i=n;i>=1;i--)
		{
			if(sss[i]<0)
			{
				for(j=i-1;j>=1;j--)sss[j]-=sss[i]; re-=sss[i]*i;
			}
		}printf("%d\n",re);
	}return 0;
}
