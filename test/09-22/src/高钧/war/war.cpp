#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define M(a,v) memset(a,v,sizeof a)
#define int long long
const int N=505,B=(1LL<<17),inf=0x7ffffffffff;
int n,a,b,w[N],f[B],C[B],zz[B],cnt=0;
inline int get(int zt)
{
	int i,mi=inf,ma=-inf,re;
	for(i=1;i<=n;i++)
	{
		if(zt&(1<<(i-1))){mi=min(mi,w[i]); ma=max(ma,w[i]);}
	}re=(ma-mi)*(ma-mi); return re;
}
signed main()
{
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	int i,j,bo=0;
	scanf("%lld%lld%lld",&n,&a,&b); for(i=1;i<=n;i++)scanf("%lld",&w[i]);
	for(i=2;i<=n;i++)if(w[i]!=w[i-1]){bo=1;break;} if(!bo)return 0*printf("%lld\n",a);
	if(n<=16)
	{
		int ma,mi,zt=0,t;
		for(i=1;i<=n;i++)
		{
			zt=0; for(j=i;j<=n;j++){zt|=(1<<(j-1)); zz[++cnt]=zt;}
		}
		for(i=1;i<(1<<n);i++)
		{
			ma=-inf; mi=inf;
			for(j=1;j<=n;j++)
			{
				if(i&(1<<(j-1)))
				{
					ma=max(ma,w[j]); mi=min(mi,w[j]);
				}
			}C[i]=(ma-mi)*(ma-mi);
		}M(f,63); f[0]=0; for(i=1;i<=n;i++)f[(1<<(i-1))]=a; for(i=1;i<=cnt;i++)f[zz[i]]=min(f[zz[i]],b*C[zz[i]]+a);
		for(t=1;t<=50;t++)
		{
			for(i=1;i<=cnt;i++)
			{
				for(j=1;j<=cnt;j++)
				{
					if((zz[j]&zz[i])==zz[j]&&(zz[j]|zz[i])==zz[i]&&zz[i]!=zz[j])
					{
						f[zz[i]]=min(f[zz[i]],f[zz[j]]+a+b*C[zz[i]^zz[j]]);
					}else if(zz[i]==zz[j])f[zz[i]]=min(f[zz[i]],f[zz[j]]);
				}
			}
		}printf("%lld\n",f[(1<<n)-1]);
	}
	else
	{
		int ma,mi,ztt=0,t;
		for(i=1;i<=n;i++)
		{
			ztt=0; for(j=i;j<=n;j++){ztt|=(1<<(j-1)); zz[++cnt]=ztt;}
		}M(f,63); for(i=1;i<=cnt;i++)f[i]=min(f[i],b*get(zz[i])+a);
		for(t=1;t<=50;t++)
		{
			for(i=1;i<=cnt;i++)
			{
				for(j=1;j<=cnt;j++)
				{
					if(((zz[j]&zz[i])==zz[j])&&((zz[j]|zz[i])==zz[i])&&(zz[i]!=zz[j]))
					{
						f[i]=min(f[i],f[j]+a+b*get(zz[i]^zz[j]));
					}else if(zz[i]==zz[j])f[i]=min(f[i],f[j]);
				}
			}
		}for(i=1;i<=cnt;i++)if(zz[i]==((1<<n)-1)) return 0*printf("%lld\n",f[i]);
	}
}
