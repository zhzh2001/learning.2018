#include <bits/stdc++.h>
using namespace std;
int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=2005;
const int MX=40000;
int n,m,a[N],g[N],b[N],ans,f[N],f2[N],p[MX],q[MX],cnt;
int main(){
//	freopen("cup.in","r",stdin);
//	freopen("cup.out","w",stdout);
	for(int i=2;i<MX;i++)
		if(!q[i]){
			for(int j=i+i;j<MX;j+=i)q[j]=1;
			p[++cnt]=i;
		}
	n=read();m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		if(i>1)g[i]=__gcd(g[i-1],a[i]);
		else {g[i]=a[i];continue;}
		int s=sqrt(a[i]),k=a[i];
		for(int j=1;j<=cnt&&p[j]<s;j++)
			while(k%p[j]==0){
				k/=p[j];
				ans++;
			}
		if(k>1)ans++;
		a[i]/=g[i];
	}
	g[1]=g[2];
	for(int i=1;i<=1;i++){
		int s=sqrt(a[i]),k=a[i];
		for(int j=1;j<=cnt&&p[j]<s;j++)
			while(k%p[j]==0){
				k/=p[j];
				ans++;
			}
		if(k>1)ans++;
		a[i]/=g[i];
	}
	cnt=n;
	for(int i=1;i<=m;i++){
		b[i]=read();
		for(int j=1;j<=n;j++)
			while(g[j]%b[i]==0){
				g[j]/=b[i];
				f[j]++;
			}
		for(int j=1;j<=n;j++)
			while(a[j]%b[i]==0){
				a[j]/=b[i];
				ans-=2;
			}
	}
	for(int i=1;i<=n;i++){
		int s=sqrt(g[i]);
		for(int j=1;j<=cnt&&p[j]<=s;j++)
			while(g[i]%p[j]==0){
				g[i]/=p[j];
				f2[i]++;
			}
		if(g[i]>1)f2[i]++;
	}
	for(int i=n;i>=1;i--){
		int k=ans;
		if(f[i]>f2[i])ans-=f[i]+f2[i];
		else ans-=f[i]+f[i];
	}
	cout<<ans;
	return 0;
}
