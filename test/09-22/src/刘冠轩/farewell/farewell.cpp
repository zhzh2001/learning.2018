#include <bits/stdc++.h>
#define int long long
#define pi pair<int,int>
#define mp(a,b) make_pair(a,b)
using namespace std;
int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=100005;
int l,n,m,t[N],ans;
priority_queue<pi> Q,Q2;
signed main(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	l=read();n=read();m=read();
	for(int i=1;i<=n;i++){
		int a=read();
		Q.push(mp(-a,-a));
	}
	for(int i=1;i<=m;i++){
		int b=read();
		Q2.push(mp(0,-b));
	}
	for(int i=1;i<=l;i++){
		pi u=Q.top();Q.pop();
		t[i]=u.first;
		u.first+=u.second;
		Q.push(u);
	}
	for(int i=1;i<=l;i++){
		pi u=Q2.top();Q2.pop();
		if(t[i]<u.first)u.first=t[i]+u.second;
			else u.first+=u.second;
		ans=min(ans,u.first);
		Q2.push(u);
	}
	cout<<-ans;
	return 0;
}
