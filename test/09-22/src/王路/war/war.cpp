#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <queue>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 55, kInf = 0x3f3f3f;
int n, a, b;
ll w[kMaxN];

namespace SubTask1 {
static const int kSubN = 16;
ll f[1 << kSubN];
bool vis[kSubN];
int Solve() {
  queue<int> Q;
  fill_n(f, 1 << kSubN, 1LL << 60);
  f[(1 << n) - 1] = 0;
  Q.push((1 << n) - 1);
  vis[(1 << n) - 1] = true;
  while (!Q.empty()) {
    int u = Q.front();
    for (int l = 0; l < n; ++l) {
      int t = u;
      ll mx = 0, mn = 1LL << 60;
      for (int r = l; r < n; ++r) {
        if ((t >> r) & 1) {
          mx = max(mx, w[r + 1]);
          mn = min(mn, w[r + 1]);
          t &= (((1 << n) - 1) ^ (1 << r));
          f[t] = min(f[t], f[u] + a + 1LL * (mx - mn) * (mx - mn) * b);
          if (!vis[t]) {
            Q.push(t);
            vis[t] = true;
          }
        }
      }
    }
    vis[u] = false;
    Q.pop();
  }
  printf("%lld\n", f[0]);
  return 0;
}
}

namespace Spec1 {
int Solve() {
  printf("%d\n", a);
  return 0;
}
}

int main() {
  freopen("war.in", "r", stdin);
  freopen("war.out", "w", stdout);
  Read(n), Read(a), Read(b);
  bool same = true;
  for (int i = 1; i <= n; ++i) {
    Read(w[i]);
    if (w[i] != w[1])
      same = false;
  }
  if (same)
    return Spec1::Solve(); // 10pts
  if (n <= 16) {
    return SubTask1::Solve(); // 40pts
  }
  return 0;
}