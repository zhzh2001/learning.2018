#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 2005, kRange = 1e6 + 5;
int n, m, a[kMaxN], b[kMaxN], p[kRange], F[kRange], f[kRange];
bool notpri[kRange];

inline int gcd(int a, int b) { return __gcd(a, b); }

namespace Solution {
int Calc(int x) {
  if (x < kRange)
    return F[x];
  for (int j = 1; j <= p[0] && p[j] <= x; ++j) {
    if (x % p[j] == 0)
      return Calc(x / p[j]) + (*lower_bound(b + 1, b + m + 1, p[j]) == p[j] ? -1 : 1);
  }
  return (*lower_bound(b + 1, b + m + 1, x) == x ? - 1 : 1);
}
int Solve() {
  F[1] = 0;
  for (int i = 2; i < kRange; ++i) {
    if (!f[i]) {
      f[i] = p[++p[0]] = i;
      F[i] = (*lower_bound(b + 1, b + m + 1, i) == i ? - 1 : 1);
    }
    for (int j = 1; j <= p[0] && p[j] <= f[i] && i * p[j] < kRange; ++j) {
      f[i * p[j]] = p[j];
      F[i * p[j]] = F[i] + (*lower_bound(b + 1, b + m + 1, p[j]) == p[j] ? -1 : 1);
    }
  }
  int ans = 0;
  for (int i = 1; i <= n; ++i) {
    ans += Calc(a[i]);
  }
  for (int i = 1; i <= m; ++i) {
    while (a[1] % b[i] == 0) { // at most log times
      int delta = 0, g = a[1], j;
      for (j = 1; j <= n; ++j) {
        if (a[j] % b[i] == 0) {
          g = gcd(g, a[j]);
        } else break;
      }
      int mx = 0, mxpos = 0;
      for (int k = 1; k < j; ++k) {
        delta -= Calc(a[k]);
        delta += Calc(a[k] / g);
        if (delta >= mx) {
          mx = delta;
          mxpos = k;
        }
      }
      if (mx < 0)
        break;
      else {
        for (int k = 1; k <= mxpos; ++k) {
          a[k] /= g;
        }
        ans += delta;
      }
    }
  }
  printf("%d\n", ans);
  return 0;
}
}

int main() {
  freopen("cup.in", "r", stdin);
  freopen("cup.out", "w", stdout);
  Read(n), Read(m);
  for (int i = 1; i <= n; ++i)
    Read(a[i]);
  for (int i = 1; i <= m; ++i)
    Read(b[i]);
  return Solution::Solve(); // 100pts, need check
}