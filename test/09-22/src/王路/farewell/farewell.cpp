#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <set>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<ll, int> pii;

const int kMaxN = 1e5 + 5, kInf = 0x3f3f3f3f;
int l, n, m, a[kMaxN], b[kMaxN];

inline bool Check1(ll mid) {
  ll ans = 0;
  for (int i = 1; i <= n; ++i) {
    ans += mid / a[i];
  }
  return ans >= l;
}

namespace SubTask1 {
int Solve() {

  return 0;
}
}

namespace SubTask2 {
int Solve() {
  ll ans = 1LL << 60;
  for (ll l = 1, r = 1LL << 60; l <= r; ) {
    ll mid = (l + r) >> 1;
    // fprintf(stderr, "%lld %lld\n", l, r);
    if (Check1(mid)) {
      r = mid - 1;
      ans = mid;
    } else l = mid + 1;
  }
  printf("%lld\n", ans);
  return 0;
}
}

namespace Solution {
ll last1[kMaxN];
inline bool Check2(ll mid) {
  int cc = 1;
  memset(last1, 0x00, sizeof last1);
  set<pii> S;
  for (int i = 1; i <= m; ++i)
    S.insert(make_pair(b[i], i));
  for (int i = 1; i <= l; ++i) {
    ll cur = last1[cc] + a[cc];
    if (cc > n)
      return false;
    set<pii>::iterator last_nxt = S.upper_bound(make_pair(mid - cur, kInf));
    if (last_nxt == S.begin()) {
      ++cc;
      --i;
      continue;
    }
    pii val = *(--last_nxt);
    S.erase(last_nxt);
    last1[cc] += a[cc];
    S.insert(make_pair(val.first + b[val.second], val.second));
  }
  return true;
}
int Solve() {
  sort(a + 1, a + n + 1);
  sort(b + 1, b + m + 1);
  ll ans = 1LL << 60;
  for (ll l = 1, r = 1LL << 60; l <= r; ) {
    ll mid = (l + r) >> 1;
    if (Check2(mid)) {
      r = mid - 1;
      ans = mid;
    } else l = mid + 1;
  }
  printf("%lld\n", ans);
  return 0;
}
}

int main() {
  freopen("farewell.in", "r", stdin);
  freopen("farewell.out", "w", stdout);
  Read(l), Read(n), Read(m);
  for (int i = 1; i <= n; ++i)
    Read(a[i]);
  bool bzero = true;
  for (int i = 1; i <= m; ++i) {
    Read(b[i]);
    bzero &= (b[i] == 0);
  }
  if (l == 1) {
    return printf("%d\n", *min_element(a + 1, a + n + 1) +
      *min_element(b + 1, b + m + 1)), 0; // 10pts
  }
  // if (n <= 10 && m <= 10)
  //   return SubTask1::Solve(); // 20pts
  if (bzero) {
    return SubTask2::Solve(); // 20pts
  }
  return Solution::Solve(); // fake solution
}