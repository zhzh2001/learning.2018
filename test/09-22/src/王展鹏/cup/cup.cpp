#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
#define PI pair<int,int>
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){write(a); puts("");}
inline void wri(ll a){write(a); putchar(' ');}
inline ull rnd(){
	return ((ull)rand()<<30^rand())<<4|rand()%4;
}
const int N=100005;
int a[N],b[N],d[N],n,m,ans;
set<int> s;
int cal(int x){
	int ans=0;
	for(int i=2;i*i<=x;i++)while(x%i==0){
		x/=i; if(s.find(i)!=s.end())ans--; else ans++;
	}
	if(x>1){
		if(s.find(x)!=s.end())ans--; else ans++;
	}
	return ans;
}
int main(){
	freopen("cup.in","r",stdin); freopen("cup.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=m;i++)s.insert(read());
	for(int i=1;i<=n;i++){
		d[i]=__gcd(d[i-1],a[i]);
	}
	for(int i=n;i;i--)if(cal(d[i])<0){
		for(int j=1;j<=i;j++){
			a[j]/=d[i];d[j]/=d[i]; 
		}
	}
	for(int i=1;i<=n;i++)ans+=cal(a[i]);
	cout<<ans<<endl;
}

