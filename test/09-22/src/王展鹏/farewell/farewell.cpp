#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){write(a); puts("");}
inline void wri(ll a){write(a); putchar(' ');}
inline ull rnd(){
	return ((ull)rand()<<30^rand())<<4|rand()%4;
}
const int N=100005;
int l,n,m,a[N],b[N];
ll c[N],ans;
#define PI pair<ll,int>
struct heap{
	int n;
	PI q[N];
	void up(int x){
		for(int i=x;i>1;i>>=1)if(q[i>>1]<q[i])swap(q[i>>1],q[i]); else return;
	}
	void down(){
		for(int i=1,j;i*2<=n;i=j){
			j=i*2;
			if(j<n&&q[i<<1|1]>q[i<<1])j++;
			if(q[j]>q[i])swap(q[i],q[j]); else return;
		}
	}
	void push(PI x){
		q[++n]=x; up(n);
	}
	PI top(){
		return q[1];
	}
	void pop(){
		swap(q[1],q[n]); n--; down();
	}
	int size(){
		return n;
	}
}q;
int main(){
	freopen("farewell.in","r",stdin); freopen("farewell.out","w",stdout);
	l=read(); n=read(); m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=m;i++)b[i]=read();
	for(int i=1;i<=n;i++)q.push(mp(-a[i],i));
	for(int i=1;i<=l;i++){
		PI t=q.top(); q.pop(); 
		c[i]=-t.first; t.first-=a[t.second]; q.push(t);
	}
	while(q.size())q.pop();
	for(int i=1;i<=m;i++)q.push(mp(-b[i],i));
	for(int i=l;i;i--){
		PI t=q.top(); q.pop(); 
		ans=max(ans,c[i]-t.first); t.first-=b[t.second]; q.push(t);
	}
	cout<<ans<<endl;
}
/*

*/
