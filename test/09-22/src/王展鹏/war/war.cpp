#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
#define PI pair<int,int>
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){write(a); puts("");}
inline void wri(ll a){write(a); putchar(' ');}
inline ull rnd(){
	return ((ull)rand()<<30^rand())<<4|rand()%4;
}
const int N=55,inf=1e9;
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
int n,a,b,w[N],q[N],alb[N];
int dp[N][N][N][N],f[N][N];
int main(){
	freopen("war.in","r",stdin); freopen("war.out","w",stdout);
	n=read(); a=read(); b=read();
	for(int i=1;i<=n;i++)w[i]=q[i]=read();
	sort(&q[1],&q[n+1]); int tot=unique(&q[1],&q[n+1])-q-1;
	for(int i=1;i<=n;i++)alb[i]=lower_bound(&q[1],&q[tot+1],w[i])-q;
	for(int i=1;i<=n;i++)for(int j=i;j<=n;j++)for(int k=1;k<=tot;k++)for(int l=k;l<=tot;l++)dp[i][j][k][l]=inf;
	for(int i=n;i;i--){
		for(int j=i;j<=n;j++){
			f[i][j]=inf; dp[i][j][alb[j]][alb[j]]=f[i][j-1];
			for(int k=1;k<=tot;k++){
				for(int l=k;l<=tot;l++)if(dp[i][j][k][l]!=inf){
					//if(i==9&&j==10)cout<<l<<" wzp "<<k<<" "<<dp[i][j][k][l]<<endl;
					f[i][j]=min(f[i][j],dp[i][j][k][l]+b*(q[l]-q[k])*(q[l]-q[k])+a);
					for(int o=j+1;o<=n;o++)dp[i][o][min(k,alb[o])][max(l,alb[o])]=min(dp[i][o][min(k,alb[o])][max(l,alb[o])],dp[i][j][k][l]+f[j+1][o-1]);
				}
			}
			//cout<<f[i][j]<<" "<<i<<" "<<j<<endl;
			for(int k=i;k<j;k++)f[i][j]=min(f[i][k]+f[k+1][j],f[i][j]);
		}
	}
	cout<<f[1][n]<<endl;
}

