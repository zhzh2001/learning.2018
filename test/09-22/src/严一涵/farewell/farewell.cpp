#include <iostream>
#include <algorithm>
#include <cstdio>
#include <queue>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const LL N=100003;
LL n,m,k,a[N],ac[N],b[N],bc[N],c[N],ans=0,x,y,z;
struct cmpa{bool operator()(LL x,LL y){
	return a[x]*ac[x]>a[y]*ac[y];
}};
struct cmpb{bool operator()(LL x,LL y){
	return b[x]*bc[x]>b[y]*bc[y];
}};
priority_queue<LL,vector<LL>,cmpa>pa;
priority_queue<LL,vector<LL>,cmpb>pb;
int main(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	k=read();
	n=read();
	m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		ac[i]=1;
		pa.push(i);
	}
	for(int i=1;i<=m;i++){
		b[i]=read();
		bc[i]=1;
		pb.push(i);
	}
	for(int i=1;i<=k;i++){
		x=pa.top();
		pa.pop();
		c[i]=a[x]*ac[x];
		ac[x]++;
		pa.push(x);
	}
	for(int i=k;i>0;i--){
		x=pb.top();
		pb.pop();
		ans=max(ans,c[i]+b[x]*bc[x]);
		bc[x]++;
		pb.push(x);
	}
	printf("%lld\n",ans);
	return 0;
}