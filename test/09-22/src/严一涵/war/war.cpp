#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,a[16],A,B,f[1<<16],x,y,z;
int main(){
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	n=read();
	A=read();
	B=read();
	if(n>16){
		printf("%d\n",A);
		return 0;
	}
	for(int i=0;i<n;i++)a[i]=read();
	for(int i=0;i<(1<<n);i++)f[i]=n*A;
	f[(1<<n)-1]=0;
	for(int i=(1<<n)-1;i>=0;i--){
		for(int j=0;j<n;j++)if(i&(1<<j)){
			x=a[j];
			y=a[j];
			z=i;
			for(int k=j;k<n;k++)if(i&(1<<k)){
				x=max(a[k],x);
				y=min(a[k],y);
				z-=1<<k;
				f[z]=min(f[z],f[i]+A+B*(x-y)*(x-y));
			}
		}
	}
	printf("%d\n",f[0]);
	return 0;
}