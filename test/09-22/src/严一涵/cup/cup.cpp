#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,m,a[2003],b[2003],x,y,z,ans;
int gcd(int a,int b){
	static int c;
	while(b>0){
		c=a%b;
		a=b;
		b=c;
	}
	return a;
}
int main(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	n=read();
	m=read();
	ans=0;
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=m;i++)b[i]=read();
	for(int i=n;i>0;i--){
		x=a[1];
		for(int j=2;j<=i;j++)x=gcd(x,a[j]);
		z=x;
		y=0;
		for(int j=1;j<=m;j++)while(x%b[j]==0){
			x/=b[j];
			y--;
		}
		for(int j=2;j<=x/j;j++)while(x%j==0){
			x/=j;
			y++;
		}
		if(x>1){
			x=1;
			y++;
		}
		if(y<0){
			for(int j=1;j<=i;j++)a[j]/=z;
		}
		for(int j=1;j<=m;j++)while(a[i]%b[j]==0){
			a[i]/=b[j];
			ans--;
		}
		for(int j=2;j<=a[i]/j;j++)while(a[i]%j==0){
			a[i]/=j;
			ans++;
		}
		if(a[i]>1){
			a[i]=1;
			ans++;
		}
	}
	printf("%d\n",ans);
	return 0;
}