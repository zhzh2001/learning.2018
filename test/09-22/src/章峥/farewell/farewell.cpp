#include<fstream>
#include<queue>
#include<vector>
#include<algorithm>
using namespace std;
ifstream fin("farewell.in");
ofstream fout("farewell.out");
const int N=100005;
long long f[N];
int main()
{
	int l,n,m;
	fin>>l>>n>>m;
	typedef pair<long long,int> pii;
	priority_queue<pii,vector<pii>,greater<pii> > Q;
	for(int i=1;i<=n;i++)
	{
		int a;
		fin>>a;
		Q.push(make_pair(a,a));
	}
	for(int i=1;i<=l;i++)
	{
		f[i]=Q.top().first;
		Q.push(make_pair(Q.top().first+Q.top().second,Q.top().second));
		Q.pop();
	}
	while(!Q.empty())
		Q.pop();
	for(int i=1;i<=m;i++)
	{
		int b;
		fin>>b;
		Q.push(make_pair(b,b));
	}
	long long ans=0;
	for(int i=l;i;i--)
	{
		ans=max(ans,f[i]+Q.top().first);
		Q.push(make_pair(Q.top().first+Q.top().second,Q.top().second));
		Q.pop();
	}
	fout<<ans<<endl;
	return 0;
}
