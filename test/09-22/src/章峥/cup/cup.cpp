#include<fstream>
#include<algorithm>
#include<vector>
using namespace std;
ifstream fin("cup.in");
ofstream fout("cup.out");
const int N=2005,INF=1e9;
int b[N],sum[N],f[N];
pair<int,int> deci[N];
vector<pair<int,int> > a[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		int x;
		fin>>x;
		for(int j=2;j*j<=x;j++)
			if(x%j==0)
			{
				int cnt=0;
				for(;x%j==0;x/=j)
					cnt++;
				a[i].push_back(make_pair(j,cnt));
			}
		if(x>1)
			a[i].push_back(make_pair(x,1));
		sort(a[i].begin(),a[i].end());
	}
	for(int i=1;i<=m;i++)
		fin>>b[i];
	sort(b+1,b+m+1);
	int init=0;
	for(int i=1;i<=n;i++)
		for(int j=0;j<a[i].size();j++)
			if(*lower_bound(b+1,b+m+1,a[i][j].first)==a[i][j].first)
				init-=a[i][j].second;
			else
				init+=a[i][j].second;
	int dn=0;
	for(int i=n;i;i--)
	{
		vector<pair<int,int> > gcd=a[1];
		for(int j=2;j<=i;j++)
		{
			int x=0,y=0;
			while(x<gcd.size()&&y<a[j].size())
				if(gcd[x].first<a[j][y].first)
					gcd[x++].second=0;
				else if(gcd[x].first==a[j][y].first)
				{
					gcd[x].second=min(gcd[x].second,a[j][y].second);
					x++;y++;
				}
				else
					y++;
			while(x<gcd.size())
				gcd[x++].second=0;
		}
		vector<pair<int,int> > tmp(gcd);
		gcd.clear();
		int delta=0;
		for(int j=0;j<tmp.size();j++)
			if(tmp[j].second)
			{
				gcd.push_back(tmp[j]);
				if(*lower_bound(b+1,b+m+1,tmp[j].first)==tmp[j].first)
					delta+=tmp[j].second;
				else
					delta-=tmp[j].second;
			}
		if(gcd.size())
		{
			deci[++dn]=make_pair(i,delta);
			for(int j=1;j<=i;j++)
				for(int k=0,x=0;k<gcd.size();k++)
				{
					for(;a[j][x].first<gcd[k].first;x++);
					a[j][x].second-=gcd[k].second;
				}
		}
	}
	for(int i=1;i<=dn;i++)
		sum[i]=sum[i-1]+deci[i].second;
	int ans=init;
	for(int i=1;i<=dn;i++)
	{
		f[i]=-INF;
		for(int j=0;j<i;j++)
			f[i]=max(f[i],f[j]+(sum[i]-sum[j])*deci[i].first);
		ans=max(ans,init+f[i]);
	}
	fout<<ans<<endl;
	return 0;
}
