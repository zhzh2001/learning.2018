#include<fstream>
#include<algorithm>
#define min(a,b) (a)<(b)?(a):(b)
#define max(a,b) (a)>(b)?(a):(b)
using namespace std;
ifstream fin("war.in");
ofstream fout("war.out");
const int N=55,INF=1e9;
int n,a,b,w[N],wv[N],f[N][N][N],g[N][N];
inline void update(int& x,int y)
{
	if(y<x)
		x=y;
}
inline int sqr(int x)
{
	return x*x;
}
int main()
{
	fin>>n>>a>>b;
	for(int i=1;i<=n;i++)
		fin>>w[i];
	copy(w+1,w+n+1,wv+1);
	sort(wv+1,wv+n+1);
	for(int i=1;i<=n;i++)
		w[i]=lower_bound(wv+1,wv+n+1,w[i])-wv;
	fill_n(&g[0][0],sizeof(g)/sizeof(int),INF);
	for(int i=1;i<=n+1;i++)
		g[i][i-1]=0;
	for(int l=n;l;l--)
	{
		fill_n(&f[0][0][0],sizeof(f)/sizeof(int),INF);
		f[l][w[l]][w[l]]=0;
		for(int r=l;r<=n;r++)
			for(int mn=n;mn;mn--)
				for(int mx=mn;mx<=n;mx++)
				{
					for(int k=l;k<r;k++)
						update(f[r][min(mn,w[r])][max(mx,w[r])],f[k][mn][mx]+g[k+1][r-1]);
					for(int k=l;k<=r;k++)
						update(g[l][r],f[k][mn][mx]+g[k+1][r]+a+b*sqr(wv[mx]-wv[mn]));
				}
	}
	fout<<g[1][n]<<endl;
	return 0;
}
