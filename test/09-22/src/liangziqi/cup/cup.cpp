#include<bits/stdc++.h>
#define ll long long
#define N 2005
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
int n, m, a[N], b[N], c[N], ans, Tn;
inline int gcd(int x, int y) {
  return (!y) ? x : gcd(y, x%y);
}
inline void doit_nice() {
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= m; j++) {
      if (a[i] == 1 || a[i] < b[j]) break;
      while (a[i]%b[j] == 0) ans--, a[i] /= b[j];
    }
  }
  for (int i = 1; i <= n; i++) {
    for (int j = 2; j * j <= a[i]; j++) {
      if (a[i] == 1) break;
      while (a[i]%j == 0) ans++, a[i] /= j;
    }
    if (a[i] > 1) ans++;
  }
}
int main() {
  freopen("cup.in","r",stdin);
  freopen("cup.out","w",stdout);
  Tn = 1; n = read(); m = read();
  for (int i = 1; i <= n; i++)
    a[i] = read();
  for (int i = 1; i <= m; i++)
    b[i] = read();
  sort(b+1,b+1+m);
  memcpy(c, a, sizeof a);
  doit_nice();
  memcpy(a, c, sizeof c);
  c[1] = a[1];
  for (int i = 2; i <= n; i++) 
    c[i] = gcd(c[i-1], a[i]);
  for (int i = n; i; i--) {
    c[i] /= Tn;
    int x = c[i];
    if (c[i] == 1) continue;
    int sum = 0;
    for (int j = 1; j <= m; j++) {
      if (c[i] == 1 || c[i] < b[j]) break;
      while (c[i] % b[j] == 0) sum--, c[i] /= b[j];
    }
    for (int j = 2; j*j <= c[i]; j++) {
      if (c[i] == 1) break;
      while (c[i] % j == 0) sum++, c[i] /= j;
    }
    if (c[i] > 1) sum++;
    if (sum < 0) {
      ans -= sum * i;
      Tn *= x;
    }
  }
  printf("%d\n", ans);
  return 0;
}