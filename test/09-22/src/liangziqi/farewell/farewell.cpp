#include<bits/stdc++.h>
#define pa pair<long long, int>
#define ll long long
#define N 100005
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
const ll oo = 1000000000000000ll;
int l, n, m;
int a[N], b[N];
ll c[N];
inline int check(ll Time) {
  ll sum = 0;
  for (int i = 1; i <= n; i++) sum += Time / a[i];
  if (sum >= l) return 1;
  return 0;
}
priority_queue <pa, vector<pa>, greater<pa> > q;
priority_queue <int, vector<int>, greater<int> > aq;
inline int Check(ll Time) {
  while(!aq.empty()) aq.pop();
  while(!q.empty()) q.pop();
  for (int i = 1; i <= m; i++) 
    if (b[i] <= Time) aq.push(i);
  ll Time_m = 0;
  for (int i = 1; i <= l; i++) {
    Time_m = max(Time_m, c[i]);
    while(!q.empty() && q.top().first <= Time_m) {
      pa x = q.top();
      aq.push(x.second);
      q.pop();
    }
    if (aq.empty() || Time_m + b[aq.top()] > Time) {
      while(1) {
        if (q.empty()) return 0;
        if(!q.empty()) {
          pa x = q.top();
          Time_m = x.first;
          aq.push(x.second);
          q.pop();
        }
        int x = aq.top();
        if (Time_m + b[x] <= Time) {
          aq.pop();
          q.push(make_pair(Time_m + b[x], x));
          break;
        }
      }
    } else {
      int x = aq.top(); aq.pop();
      q.push(make_pair(Time_m + b[x], x));
    }
  }
  return 1;
}
int main() {
  freopen("farewell.in","r",stdin);
  freopen("farewell.out","w",stdout);
  l = read(); n = read(); m = read();
  for (int i = 1; i <= n; i++)
    a[i] = read();
  for (int i = 1; i <= m; i++)
    b[i] = read();
  sort(a+1,a+1+n);
  sort(b+1,b+1+m);
  ll L = 0, R = oo, ans;
  while (L <= R) {
    ll mid = L+R >> 1;
    if (check(mid)) ans = mid, R = mid-1;
    else L = mid+1;
  }
  for (int i = 1; i <= n; i++) {
    ll Time = 0;
    for (int j = 1; ; j++) {
      Time += a[i];
      if (Time > ans) break;
      c[++c[0]] = Time;
    }
  }
  sort(c+1,c+1+l);
  L = 0; R = oo, ans = 0;
  while (L <= R) {
    ll mid = L+R>>1;
    if (Check(mid)) ans = mid, R = mid-1;
    else L = mid+1;
  }
  printf("%lld\n", ans);
  return 0;
}