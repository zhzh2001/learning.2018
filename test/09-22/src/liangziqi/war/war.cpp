#include<bits/stdc++.h>
#define ll long long
#define N 55
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
const ll oo = 1000000000000000ll;
int n, A, B, a[N], vis[N];
ll b[N][N];
inline int sqr(int x) {
  return x*x;
}
ll ans = oo;
inline void dfs(ll sum) {
  int flag = 1;
  for (int i = 1; i <= n; i++)
    if (!vis[i]) flag = 0;
  if (flag) {
    ans = min(ans, sum);
    return;
  }
  bool t[N];
  for(int i = 1; i <= n; i++)
    for (int j = i; j <= n; j++) {
      ll mx = -oo, mn = oo;
      memset(t, 0, sizeof t);
      for (int k = i; k <= j; k++)
        if (!vis[k]) t[k] = 1, vis[k] = 1, mx = max(mx, (ll)a[k]), mn = min(mn, (ll)a[k]);
      if(mx == -oo) {
        continue;
      }
      dfs(sum + 1ll * sqr(mx - mn) * B + A);
      for (int k = i; k <= j; k++)
        if (t[k]) vis[k] = 0;
    }
}
int main() {
  freopen("war.in","r",stdin);
  freopne("war.out","w",stdout);
  n = read(); A = read(); B = read();
  for (int i = 1; i <= n; i++)
    a[i] = read();
  dfs(0);
  printf("%lld\n", ans);
  return 0;
}