#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
//ifstream fin("war.in");
//ofstream fout("war.out");
inline long long read(){
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
long long n,wa,wb;
long long w[53];
long long mn[53][53],mx[53][53];
long long jiyi[53][53][53][53];
bool vis[53][53][53][53];
inline long long sqr(const long long &x){
	return x*x;
}
inline long long max(const long long &a,const long long &b){
	if(a>b)return a;
	return b;
}
inline long long min(const long long &a,const long long &b){
	if(a<b)return a;
	return b;
}
long long dp(int x,int y,int s,int b){
	long long &ret=jiyi[x][y][s][b];
	if(vis[x][y][s][b]) return ret;
	vis[x][y][s][b]=1;
	//1.直接消去
	if(b==0){
		if(s==0) ret=sqr(w[mx[x][y]]-w[mn[x][y]])*wb+wa;
		else ret=sqr(w[mx[x][y]]-min(w[s],w[mn[x][y]]))*wb+wa;
	}else{
		if(s==0) ret=sqr(max(w[b],w[mx[x][y]])-w[mn[x][y]])*wb+wa;
		else ret=sqr(max(w[b],w[mx[x][y]])-min(w[s],w[mn[x][y]]))*wb+wa;
	}
	if(x==y) return ret;
	//2.分割成两个序列。 
	for(int i=x+1;i<=y;i++){
		ret=min(ret,dp(x,i-1,s,b)+dp(i,y,0,0));
		ret=min(ret,dp(x,i-1,0,0)+dp(i,y,s,b));
	}
	//3.把y算进后面连续的那段里面继续转移
	if(s==0){
		if(b==0) ret=min(ret,dp(x,y-1,y,y));
		else{
			if(w[y]>w[b]) ret=min(ret,dp(x,y-1,y,y));
			else ret=min(ret,dp(x,y-1,y,b));
		}
	}else{
		if(b==0){
			if(w[y]<w[s]) ret=min(ret,dp(x,y-1,y,y));
			else ret=min(ret,dp(x,y-1,s,y));
		}else{
			if(w[s]>w[y]) ret=min(ret,dp(x,y-1,y,b));
			else{
				if(w[y]>w[b]) ret=min(ret,dp(x,y-1,s,y));
				else ret=min(ret,dp(x,y-1,s,b));
			}
		}
	}
	return ret;
}
int main(){
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	n=read();
	wa=read();
	wb=read();
	for(int i=1;i<=n;i++)w[i]=read();
	for(int i=1;i<=n;i++){
		mn[i][i]=i,mx[i][i]=i;
		for(int j=i+1;j<=n;j++){
			mn[i][j]=mn[i][j-1],mx[i][j]=mx[i][j-1];
			if(w[mn[i][j-1]]>w[j]) mn[i][j]=j;
			if(w[mx[i][j-1]]<w[j]) mx[i][j]=j;
			mx[j][i]=mx[i][j],mx[j][i]=mx[i][j];
		}
	}
	writeln(dp(1,n,0,0));
	return 0;
}
/*

in:
10
3 1
7 10 9 10 6 7 10 7 1 2

15
50 15
6 7 8 9 10 11 10 9 8 7 6 5 4 3 2

25
100 5
29 26 27 26 32 25 23 25 25 18 21 20 23 26 27 21 27 27 24 27 32 41 29 42 24

out:
15

*/
