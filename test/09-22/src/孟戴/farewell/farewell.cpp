#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#include<queue>
using namespace std;
//ifstream fin("farewell.in");
//ofstream fout("farewell.out");
inline long long max(const long long &a,const long long &b){
	if(a>b)return a;
	return b;
}
inline long long read(){
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
long long l,n,m;
long long a[100003],b[100003],t1[100003],t2[100003];
struct wh{
	long long now,delta;
};
inline bool operator <(wh a,wh b){
	if(a.now==b.now) return a.delta>b.delta;
	return a.now>b.now;
}
priority_queue<wh>q;
int main(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	l=read();
	n=read();
	m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
	}
	for(int i=1;i<=m;i++){
		b[i]=read();
	}
	for(int i=1;i<=n;i++){
		wh x;
		x.now=a[i],x.delta=a[i];
		q.push(x);
	}
	for(int i=1;i<=l;i++){
		wh x=q.top();
		t1[i]=x.now;
		q.pop();
		x.now=x.now+x.delta;
		q.push(x);
	}
	while(!q.empty()){
		q.pop();
	}
	for(int i=1;i<=m;i++){
		wh x;
		x.now=b[i],x.delta=b[i];
		q.push(x);
	}
	for(int i=1;i<=l;i++){
		wh x=q.top();
		t2[i]=x.now;
		q.pop();
		x.now=x.now+x.delta;
		q.push(x);
	}
	long long ans=0;
	for(int i=1;i<=l;i++){
		ans=max(ans,t1[i]+t2[l-i+1]);
	}
	writeln(ans);
	return 0;
}
/*

in:
2 3 2
100 10 1
10 10

out:
12

*/
