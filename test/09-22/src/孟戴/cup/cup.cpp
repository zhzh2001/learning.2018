#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
//ifstream fin("cup.in");
//ofstream fout("cup.out");
inline long long read(){
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
long long n,m,a[3003],b[3003];
long long gcd(long long a,long long b){
	if(!b) return a;
	return gcd(b,a%b);
}
long long mark[100000],prime[100000],index;
inline void Prime(){
    for(int i=2;i<=50000;i++){
		if(mark[i]==0){
			prime[index++]=i;
		}
		for(int j=0;j<index&&prime[j]*i<=50000;j++){
			mark[i*prime[j]]=1;
			if(i%prime[j]==0){
                break;
			}
        }
    }
}
int main(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	Prime();
	n=read();
	m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
	}
	for(int i=1;i<=m;i++){
		b[i]=read();
	}
	for(int i=n;i>=1;i--){
		long long gcdd=a[1],nowgcd,now=0;
		for(int j=2;j<=i;j++){
			gcdd=gcd(a[j],gcdd);
		}
		nowgcd=gcdd;
		for(int j=1;j<=m;j++){
			while(gcdd%b[j]==0){
				gcdd=gcdd/b[j];
				now--;
			}
		}
		for(int j=0;j<=index-1;j++){
			while(gcdd%prime[j]==0){
				gcdd=gcdd/prime[j];
				now++;
			}
		}
		if(now<0){
			for(int j=1;j<=i;j++){
				a[j]=a[j]/nowgcd;
			}
		}
	}
	long long ans=0;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			while(a[i]%b[j]==0){
				a[i]=a[i]/b[j];
				ans--;
			}
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=0;j<=index-1;j++){
			while(a[i]%prime[j]==0){
				a[i]=a[i]/prime[j];
				ans++;
			}
		}
		if(a[i]!=1){
			ans++;
		}
	}
	writeln(ans);
	return 0;
}
/*
4 5
2 4 8 16
3 5 7 11 17
*/
