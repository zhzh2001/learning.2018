#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <ctime>

#define ll long long

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

ll n,a,b,l,r,k,maxn,size,minn,ans,now,c[100],d[100],num[100];

inline void calc(){
	now=0;size=n;
	for(ll i=1;i<=n;i++)c[i]=num[i];
	while(size){
		l=rand()%size+1;
		r=rand()%size+1;
		k=size;
		if(l>r)swap(l,r);
		maxn=-1e9;minn=1e9;
		for(ll i=l;i<=r;i++){
			maxn=max(maxn,c[i]);
			minn=min(minn,c[i]);
		}
		size=0;
		now+=a+b*(maxn-minn)*(maxn-minn);
		for(ll i=1;i<l;i++){
			d[++size]=c[i];
		}
		for(ll i=r+1;i<=k;i++){
			d[++size]=c[i];
		}
		for(ll i=1;i<=size;i++){
			c[i]=d[i];
		}
	}
	ans=min(ans,now);
}

int main(){
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	srand((ll)time(NULL));
	n=read();
	a=read();b=read();
	maxn=-1e9;
	minn=1e9;
	for(ll i=1;i<=n;i++){
		num[i]=read();
		maxn=max(maxn,num[i]);
		minn=min(minn,num[i]);
	}
	ans=(maxn-minn)*(maxn-minn)*b+a;
	for(ll i=1;i<=100000;i++){
		calc();
	}
	writeln(ans);
	return 0;
}
