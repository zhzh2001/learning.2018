#include <iostream>
#include <cstdio>

#define ll long long
#define Max 2005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

ll n,m,size,num[1000005],pri[1000005],a[Max];
bool vis[1000005],use[1000005];

inline ll gcd(ll x,ll y){
	return x%y?gcd(y,x%y):y;
}

int main(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	n=read();m=read();
	for(ll i=1;i<=n;i++)a[i]=read();
	for(ll i=1;i<=m;i++)use[read()]=true;
	for(ll i=2;i<=1000000;i++){
		if(!vis[i]){
			pri[++size]=i;
			if(use[i]){
				num[i]=-1;
			}else{
				num[i]=1;
			}
		}
		for(ll j=1;j<=size;j++){
			ll k=pri[j]*i;
			if(k>1000000)break;
			vis[k]=true;
			num[k]=num[i]+num[pri[j]];
			if(i%j==0)break;
		}
	}
	ll now=0;
	for(ll i=n;i>=1;i--){
		now=a[1];
		for(ll j=2;j<=i;j++){
			now=gcd(now,a[j]);
		}
		if(num[now]<=0){
			for(ll j=1;j<=i;j++){
				a[j]/=now;
			}
		}
	}
	ll ans=0;
	for(ll i=1;i<=n;i++){
		ans+=num[a[i]];
	}
	writeln(ans);
	return 0;
}
