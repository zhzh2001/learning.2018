#include <algorithm>
#include <iostream>
#include <cstdio>
#include <queue>

#define ll long long
#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

struct Node{
	ll num,last;
	inline bool operator<(const Node&x)const{
		return last<x.last;
	}
};

ll l,r,k,n,m,now,mid,a[Max],b[Max],c[Max],d[Max];

priority_queue<Node>que;

inline bool check(ll x){
	ll sum=0;
	for(int i=1;i<=n;i++){
		sum+=x/a[i];
	}
	if(sum>=k)return true;else return false;
}

inline bool check2(ll x){
	Node now;
	while(!que.empty())que.pop();
	for(int i=1;i<=m;i++){
		now.num=b[i];
		now.last=x-b[i];
		que.push(now);
	}
	for(int i=1;i<=k;i++){
		now=que.top();
		que.pop();
		if(c[i]>now.last)return false;
		now.last-=now.num;
		que.push(now);
	}
	return true;
}

inline bool cmp(int x,int y){
	return x>y;
}

int main(){
//	freopen("farewell.in","r",stdin);
//	freopen("farewell.out","w",stdout);
	k=read();n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=m;i++)b[i]=read();
	l=1;r=1e18;
	while(l+1<r){
		mid=(l+r)>>1;
		if(check(mid)){
			r=mid;
		}else{
			l=mid;
		}
	}
	if(check(l))r=l;
	now=r;
	r=0;
	for(int i=1;i<=n;i++){
		l=now/a[i];
		for(int j=1;j<=l;j++){
			c[++r]=a[i]*j;
		}
	}
	sort(c+1,c+r+1,cmp);
	l=1;r=1e18;
	while(l+1<r){
		mid=(l+r)>>1;
		if(check2(mid)){
			r=mid;
		}else{
			l=mid;
		}
	}
	if(check2(l))r=l;
	writeln(r);
	return 0;
}

