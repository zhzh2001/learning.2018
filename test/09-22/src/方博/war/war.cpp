#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=55;
ll anst,x,y,n,k;
ll a[N],tt[N];
bool vis[N];
void dfs(ll ans,ll t)
{
//	cout<<ans<<endl;
	if(ans>anst)return;
	if(k==n){
		anst=min(anst,ans);
		return;
	}
	for(int i=1;i<=n;i++)
		if(vis[i]!=true){
			ll ma=a[i],mi=a[i];
			for(int j=i;j<=n;j++){
				if(vis[j]==true)continue;
				ma=max(ma,a[j]),mi=min(mi,a[j]),k++,vis[j]=true,tt[j]=t;
				dfs(ans+x+y*(ma-mi)*(ma-mi),t+1);
			}
			for(int j=i;j<=n;j++)
				if(tt[j]==t)vis[j]=false,k--,tt[j]=0;
	}
}
int main()
{
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	n=read();x=read();y=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	int l=0;
	for(int i=1;i<=n;i++)
		if(a[i]!=a[i-1])a[++l]=a[i];
	n=l;
	if(l==1)anst=x;
	else {
		anst=n*x;
		dfs(0,1);
	}
	writeln(anst);
}
