#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=2005;
ll n,m,a[N],b[N];
ll gcd(ll x,ll y){if(y==0)return x;return gcd(y,x%y);}
ll mark[50005],p[50005];
inline void pre(){
    for(int i=2;i<=50005;i++){
		if(mark[i]==0)p[++p[0]]=i;
		for(int j=0;j<p[0]&&p[j]*i<50005;j++){
			mark[i*p[j]]=1;
			if(i%p[j]==0)break;
        }
    }
}
int main()
{
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=m;i++)b[i]=read();
	pre();
	for(int i=n;i;i--){
		ll tmp=a[1],now,cha=0;
		for(int j=2;j<=i;j++)tmp=gcd(a[j],tmp);
		now=tmp;
		for(int j=1;j<=m;j++){
			while(tmp%b[j]==0){
				tmp=tmp/b[j];
				cha--;
			}
		}
		for(int j=0;j<=p[0]-1;j++){
			while(tmp%p[j]==0){
				tmp=tmp/p[j];
				cha++;
			}
		}
		if(cha<0){
			for(int j=1;j<=i;j++){
				a[j]=a[j]/now;
			}
		}
	}
	ll ans=0;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			while(a[i]%b[j]==0){
				a[i]=a[i]/b[j];
				ans--;
			}
		}
		for(int j=0;j<=p[0];j++){
			while(a[i]%p[j]==0){
				a[i]=a[i]/p[j];
				ans++;
			}
		}
		if(a[i]!=1)ans++;
	}
	writeln(ans);
	return 0;
}
