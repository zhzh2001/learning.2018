#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=100005;
ll l,n,m;
ll a[N],b[N],ans1[N],ans2[N],ans;
struct xxx{
	ll t,k;
};
bool operator <(xxx a,xxx b){
	if(a.t==b.t)return a.k>b.k;
	else return a.t>b.t;
}
priority_queue<xxx>q;
int main()
{
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	l=read();n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=m;i++)b[i]=read();
	for(int i=1;i<=n;i++){
		xxx k;
		k.t=a[i],k.k=a[i];
		q.push(k);
	}
	for(int i=1;i<=l;i++){
		xxx t=q.top();
		ans1[i]=t.t;
		q.pop();
		t.t=t.t+t.k;
		q.push(t);
	}
	while(!q.empty())q.pop();
	for(int i=1;i<=m;i++){
		xxx k;
		k.t=b[i],k.k=b[i];
		q.push(k);
	}
	for(int i=1;i<=l;i++){
		xxx t=q.top();
		ans2[i]=t.t;
		q.pop();
		t.t=t.t+t.k;
		q.push(t);
	}
	for(int i=1;i<=l;i++)
		ans=max(ans,ans1[i]+ans2[l-i+1]);
	writeln(ans);
	return 0;
}
