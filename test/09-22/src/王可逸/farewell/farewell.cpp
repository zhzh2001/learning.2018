#include <bits/stdc++.h>
#define int long long
using namespace std;
int l, n, m, a[120000], b[120000], f[1200][1200], g[1200][1200], mn[1200];
bool b_zero = true;
struct node {
	int time, which;
	friend bool operator < (node a, node b) {
		if (a.time != b.time) return a.time > b.time;
		else return a.which > b.which;
	}
};
priority_queue <node> q1;
priority_queue <node> q2;
signed main() {
	freopen("farewell.in", "r", stdin);
	freopen("farewell.out", "w", stdout);
	ios::sync_with_stdio(false);
	cin >> l >> n >> m;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= l; j++)
			f[i][j] = 0x3f3f3f3f;
	for (int i = 1; i <= n; i++) cin >> a[i];
	for (int i = 1; i <= m; i++) {
		cin >> b[i];
		if (b[i]) b_zero = false;
	}
	sort(a + 1, a + n + 1);
	sort(b + 1, b + m + 1);
	if (l == 1) {
		cout << a[1] + b[1] << '\n';
		return 0;
	} else {
		if (b_zero) {
			for (int i = 1; i <= n; i++)
				for (int j = 1; j <= l; j++)
					f[i][j] = a[i] * j;
			for (int i = 2; i <= n; i++)
				for (int j = 1; j <= l; j++)
					for (int k = 1; k <= j; k++) {
						f[i][j] = min(f[i][j], max(f[i - 1][j - k], k * a[i]));
						// cout<<"f["<<i<<"]["<<j<<"]="<<f[i][j]<<'\n';
					}
			int ans = 0x3f3f3f3f;
			for (int i = 1; i <= n; i++)
				ans = min(ans, f[i][l]);
			cout << ans;
		} else {
			int ans = 0;
			int now_l2 = 0;
			for (int i = 1; i <= n; i++) {
				q1.push((node) {a[i], i});
			}
			for (int i = 1; i <= m; i++) {
				q2.push((node) {0, i});
			}
			while (now_l2 < l) {
				node mcl = q1.top();
				q1.pop();
				node wky = q2.top();
				q2.pop();
				// cout << "DEBUG " << mcl.time << " " << mcl.which << " " << wky.time << " " << wky.which << '\n';
				ans = max(mcl.time, wky.time);
				if (wky.time != 0) now_l2++;
				if(now_l2==l) break;
				q1.push((node) {mcl.time + a[mcl.which], mcl.which});
				q2.push((node) {max(wky.time, mcl.time) + b[wky.which], wky.which});
			}
			cout << ans;
		}
	}
}
																												