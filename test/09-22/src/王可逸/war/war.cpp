#include<bits/stdc++.h>
using namespace std;
int x,y,n,a[1200],l[1200],r[1200],ans,f[1200];
bool same;
inline int read() {
	int X=0,w=0;
	char ch=0;
	while(!isdigit(ch)) {
		w|=ch=='-';
		ch=getchar();
	}
	while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
	return w?-X:X;
}
int main() {
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	n=read();
	a=read();
	b=read();
	int mx=-0x3f3f3f3f,mn=0x3f3f3f3f;
	for(int i=1;i<=n;i++) {
		cin>>a[i];
		mx=max(mx,a[i]);
		mn=min(mn,a[i]);
		if(i!=1&&a[i]!=a[i-1]) same=false;
		if(i!=1) l[i]=i-1;
		if(i!=n) r[i]=i+1;
	}
	if(same) {
		cout<<a;
		return 0;
	}
	int remain=n;
	cout<<a+(mx-mn)*(mx-mn);
	return 0;
}