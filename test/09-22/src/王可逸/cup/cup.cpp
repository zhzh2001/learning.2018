#include <bits/stdc++.h>
using namespace std;
int n, a[2300], gg[2300], f[1200000];
bool gcd_one = 1;
map <int, bool> q;
bool zhi(int x) {
	if (x == 2 || q[x]) return true;
	for (int i = 1; i * i <= x; i++) {
		if (x % i == 0) return false;
	}
	return true;
}
int min_zhi(int x) {
	for (int i = 2; i * i <= x; i++)
		if (x % i == 0 && zhi(x)) return i;
	return x;
}
signed main() {
	freopen("cup.in", "r", stdin);
	freopen("cup.out", "w", stdout);
	cin >> n >> m;
	int mx = 0;
	for (int i = 1; i <= n; i++) {
		cin >> a[i];
		mx = max(mx, a[i]);
	}
	for (int i = 1; i <= n; i++) {
		int x = read();
		q[x] = true;
	}
	gg[1] = a[1];
	for (int i = 2; i <= n; i++) {
		gg[i] = __gcd(gg[i - 1], a[i]);
		if (gg[i] == 1) gcd_one = false;
	}
	int he = 0;
	for (int i = 2; i <= mx; i++) {
		int mn = min_zhi(i);
		if (q[mn]) f[i] = f[i / mn] - 1;
		else f[i] = f[i / mn] + 1;
	}
	for (int i = 1; i <= n; i++) he += f[a[i]];
	if (gcd_one) {
		if (f[a[1]] > f[1]) cout << he;
		else cout << he - f[a[1]] + f[1];
		return 0;
	} else cout << he;
}