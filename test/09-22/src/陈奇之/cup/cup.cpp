#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
namespace IO{
	char c,st[66];
	int tp=0,f;
	#define gc getchar
	#define pc putchar
	inline ll rd(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp)pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
}using IO::rd;using IO::write;using IO::writeln;

inline int gcd(int a,int b){
	if(!b) return a; else return gcd(b,a%b);
}
const int maxn = 32005;
map<int,bool> mark2;
bool mark1[maxn];
int a[maxn],b[maxn],w[maxn],n,m;

int f(int x){
	int ans = 0;
	for(int i=2;i*i<=x;++i){
		if(x%i==0){
			while(x%i==0){
				x/=i;
				ans+=mark1[i]?-1:1;
			}
		}
	}
	if(x>1){
		if(mark2[x]) ans += -1;
				else ans += 1;
	}
	return ans;
}

map<int,int> dp;
int dfs(int dv){
	if(dp.count(dv)) return dp[dv];
	dp[dv] = 0;
	int ans = 0;
	int tmp = f(dv);
	for(int i=1;i<=n;++i){
		if(a[i] < dv) break;
		ans = min(ans,dfs(a[i]) + (w[i] - tmp) * i);
	}
	return dp[dv] = ans;
}
int main(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	mem(mark1,false);mark2.clear();
	n = rd(),m = rd();
	Rep(i,1,n){
		a[i] = rd();
	}
	Rep(i,1,m){
		b[i] = rd();
		if(b[i] <= 32000) mark1[b[i]] = true,mark2[b[i]] = true; else
						  mark2[b[i]] = true;
	}
	int pre = 0,ans = 0;
	Rep(i,1,n){
		pre = gcd(pre,a[i]);
		ans += f(a[i]);
		a[i] = pre;
		w[i] = f(pre);
	}
	dp.clear();
	writeln(ans - dfs(1));
	return 0;
}
/*
2 2
2 2 5
2 17
2 5
2 5
*/
