#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
#define rank __RANK
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 55;
int dp[55][55];
int n,A,B,a[55];
//#define min(a,b) ((a) < (b) ? (a) : (b))
inline int dfs(int l,int r){
	if(l>r) return 0;
	if(l==r) return A;
	if(dp[l][r] != -1) return dp[l][r];
	int pos[55],b[55],f[55];
	pos[0] = 0;
	Rep(i,l,r){
		pos[++pos[0]] = a[i];
	}sort(pos+1,pos+1+pos[0]);
	pos[0] = unique(pos+1,pos+1+pos[0]) - pos - 1;
//	Rep(i,1,pos[0]) printf("%d ",pos[i]);
//	puts("");
	int & ans = dp[l][r] = 0x3f3f3f3f;
	int mn,mx;
	Rep(x,1,pos[0]){
		mn = pos[x];
		Rep(y,x,pos[0]){
			mx = pos[y];
			if(A+B*(mx-mn)*(mx-mn)>=ans) continue;
			int tmp = 0,last = l;
			b[0] = 0;
			Rep(i,l,r)
				if(mn<=a[i] && a[i]<=mx)
					b[++b[0]] = i;
			tmp = 0;
			f[0] = 0;
			int pos = 0;
			Rep(i,1,b[0]){
				f[i] = dfs(l,b[i]-1);
				Rep(j,1,i-1){
					f[i] = min(f[i],f[j]+dfs(b[j]+1,b[i]-1));
				}
			}
			f[b[0]+1] = 0x3f3f3f3f;
			Rep(i,1,b[0]){
				f[b[0]+1] = min(f[b[0]+1],f[i] + dfs(b[i]+1,r));
			}
			ans = min(ans,f[b[0]+1] + A + B * (mx - mn) * (mx - mn));
		}
	}
	Rep(i,l,r-1) ans = min(ans,dfs(l,i) + dfs(i+1,r));
	return ans;
}
int main(){
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	n = rd(),A = rd(),B = rd();
	Rep(i,1,n) a[i] = rd();
	mem(dp,-1);
	writeln(dfs(1,n));
	return 0;
}
/*
10
3 1
7 10 9 10 6 7 10 7 1 2
*/
/*
dp[1][6]

dp[2][4]

dp[2][4]=4
dp[7][7]=3

dp[1][8]
1,5,6,8
7 6 7 7
f[1][7][7] = 0
f[4][7][7] = 4
*/





