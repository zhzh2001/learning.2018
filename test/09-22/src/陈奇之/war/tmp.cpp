#include<bits/stdc++.h>
using namespace std;
#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
#define rank __RANK
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 52;
int dp[52][52];
int n,A,B,a[52];
inline int dfs(int l,int r){
	if(l==r) return A;
	if(dp[l][r] != -1) return dp[l][r];
	int f[52][52][52];
	int MN = 50,MX = 0;
	mem(f,0x3f);
	f[l-1][0][50] = 0;
	Rep(i,l-1,r-1){
		Rep(mx,0,50){
			Rep(mn,0,50){
				if(f[i][mx][mn] != 0x3f3f3f3f){
					f[i+1][max(mx,a[i+1])][min(mn,a[i+1])] = min(f[i+1][max(mx,a[i+1])][min(mn,a[i+1])],f[i][mx][mn]); 
					Rep(j,i+1,r){
						if(i+1==l&&j==r) continue;
						int tmp = f[i][mx][mn]+dfs(i+1,j);
						if(tmp < f[j][mx][mn]) f[j][mx][mn] = tmp;
					}
				}
			}
		}
	}
	int &ans = dp[l][r] = 0x3f3f3f3f;
	Rep(mx,0,50){
		Rep(mn,0,50){
			ans = min(ans,f[r][mx][mn] + A + B * (mx - mn) * (mx - mn));
				//出现了，选了至少一个数字的！ 
		}
	}
	return ans;
}
int main(){
	freopen("war.in","r",stdin);
	freopen("war1.out","w",stdout);
	n = rd(),A = rd(),B = rd();
	Rep(i,1,n) a[i] = rd();
	mem(dp,-1);
	writeln(dfs(1,n));
	return 0;
}
/*
10
3 1
7 10 9 10 6 7 10 7 1 2
*/
/*
dp[1][6]

dp[2][4]

dp[2][4]=4
dp[7][7]=3

dp[1][8]
1,5,6,8
7 6 7 7
f[1][7][7] = 0
f[4][7][7] = 4
*/





