#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
#define rank __RANK
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 1e5+233;
multiset<pair<int,int> > s;
multiset<pair<pair<int,int>,int> >ss;
multiset<pair<pair<int,int>,int> >::iterator iterr;
multiset<pair<int,int> >::iterator iter;
int l,n,m,V[maxn],a[maxn];
void init(){
	sort(a+1,a+1+n);
	Rep(i,1,n){
		if(l){
			s.insert(make_pair(a[i],a[i]));
			l--;
		}
	}
	V[0] = 0;
	while(s.size()){
		V[++V[0]] = s.begin() -> fi;
//		printf("(%d)\n",V[V[0]]);
		int w1 = s.begin() -> fi,w2 = s.begin() -> se;
		s.erase(s.begin());
		if(l){
			s.insert(make_pair(w1+w2,w2));
			l--;
		} else{
			if(s.size() && (s.rbegin() -> fi > w1 + w2)){
				iter = s.end();iter--;
				s.erase(iter);
				s.insert(make_pair(w1+w2,w2));
			}
		}
	}
//	writeln(V[0]);
}
int b[maxn];
ll ans = 0;
void init2(){
	sort(b+1,b+1+m);
	int l = 0;
	Rep(i,1,m){
		if(l+1<=V[0]){
			++l;
			ss.insert(make_pair(make_pair(V[l]+b[i],b[i]),V[l]));
		}
	}
	while(ss.size()){
		int w1 = ss.begin() -> fi.fi,w2 = ss.begin() -> fi.se;
		ans = max(ans,(ll)w1);
		ss.erase(ss.begin());
		if(l+1<=V[0]){
			++l;
			ss.insert(make_pair(make_pair(max(V[l],w1)+w2,w2),V[l]));
		} else{
			if(ss.size()){
				int u1 = ss.rbegin()->fi.fi,u2 = ss.rbegin() -> fi . se,u3 = ss.rbegin() -> se;
				if(u1 > max(u3,w1)+w2){
					iterr = ss.end();iterr--;
					ss.erase(iterr);
					ss.insert(make_pair(make_pair(max(u3,w1)+w2,w2),u3));
				}
			}
		}
	}
}
signed main(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	l = rd(),n = rd(),m = rd();
	Rep(i,1,n){
		a[i] = rd();
	}
	init();
	Rep(i,1,m){
		b[i] = rd();
	}
	init2();
	writeln(ans);
	return 0;
}
/*f[l][r]表示把区间[l,r]全部取出的最小代价

[l,r]
枚举子区间
[ll,rr]
考虑这时候取出[ll,rr]
然后变成递归[l,ll-1],[rr+1,r]
同时加上相应的代价？？ */
