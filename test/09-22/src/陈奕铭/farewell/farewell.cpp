#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int l,n,m;
int a[N],b[N];
ll st[N],ed[N];
ll Mx;
typedef pair<ll,ll> pll;
priority_queue<pll,vector<pll>,greater<pll> > Q;
priority_queue<pll,vector<pll>,greater<pll> > P;

signed main(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	l = read(); n = read(); m = read();
	for(int i = 1;i <= n;++i) a[i] = read();
	for(int i = 1;i <= m;++i) b[i] = read();
	sort(a+1,a+n+1); sort(b+1,b+m+1);
	for(int i = 1;i <= n;++i) Q.push(make_pair(a[i],a[i]));
	for(int i = 1;i <= l;++i){
		pll x = Q.top(); Q.pop();
		st[i] = x.first; x.first += x.second;
		Q.push(x);
	}
	for(int i = 1;i <= m;++i) P.push(make_pair(b[i],b[i]));
	for(int i = 1;i <= l;++i){
		pll x = P.top(); P.pop();
		ed[i] = x.first; x.first += x.second;
		P.push(x);
	}
	for(int i = 1;i <= l;++i) Mx = max(Mx,st[i]+ed[l-i+1]);
	printf("%lld\n", Mx);
	return 0;
}