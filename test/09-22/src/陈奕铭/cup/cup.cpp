#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 2005;
const int M = 100005;
int n,m;
int a[N],b[N];
bool vis[M];
int pri[M],cnt;
bool Val[M];
map<int,bool> val;
int sum;
int Gcd[N];
int Sum[N];
int Del;

void getpri(){
	for(int i = 2;i <= 100000;++i){
		if(!vis[i]) pri[++cnt] = i;
		for(int j = 1;j <= cnt;++j){
			if(1LL*pri[j]*i > 100000) break;
			vis[pri[j]*i] = true;
			if(i%pri[j] == 0) break;
		}
	}
	// for(int i = 1;i <= 10;++i) printf("%d\n",pri[i]);
	// 	printf("%d\n", cnt);
}

int gcd(int x,int y){
	if(y == 0) return x;
	return gcd(y,x%y);
}

inline int getSum(int x){
	int Sum = 0;
	for(int j = 1;j <= cnt;++j){
		if(1LL*pri[j]*pri[j] > x) break;
		while(x%pri[j] == 0){
			x /= pri[j];
			if(Val[pri[j]]) --Sum;
			else ++Sum;
		}
	}
	if(x > 1){
		if(x <= 100000){
			if(Val[x]) --Sum;
			else ++Sum;
		}else{
			if(val[x]) --Sum;
			else ++Sum;
		}
	}
	return Sum;
}

signed main(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	getpri();
	n = read(); m = read();
	for(int i = 1;i <= n;++i) a[i] = read();
	for(int i = 1;i <= m;++i){
		b[i] = read();
		if(b[i] <= 100000) Val[b[i]] = true;
		else val[b[i]] = true;
	}
	for(int i = 1;i <= n;++i){
		int x = a[i],Sum = 0;
		Sum = getSum(x);
		// printf("%d ",Sum);
		sum += Sum;
	}
	Gcd[2] = gcd(a[1],a[2]);
	Sum[2] = getSum(Gcd[2]);
	for(int i = 3;i <= n;++i){
		Gcd[i] = gcd(a[i],Gcd[i-1]);
		Sum[i] = getSum(Gcd[i]);
	}
	Sum[1] = getSum(a[1]);
	// for(int i = 2; i <= n;++i) printf("%d ",Gcd[i]); puts("");
	// for(int i = 2;i <= n;++i) printf("%d ",Sum[i]); puts("");
	// printf("%d\n", sum);
	for(int i = n;i >= 1;--i){
		if(Sum[i]-Del < 0){
			sum -= (Sum[i]-Del)*i;
			Del = Sum[i];
		}
	}
	printf("%d\n", sum);
	return 0;
}