#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 55;
int n,a,b;
int w[N];
int c[N],m,d[N],sum[N];
int l[N],r[N],cnt;
int pos;

signed main(){
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	n = read(); a = read(); b = read();
	pos = sqrt(a/b);
	for(int i = 1;i <= n;++i) w[i] = read();
	if(b == 0){
		printf("%d\n", a);
		return 0;
	}
	for(int i = 1;i <= n;++i)
		if(w[i] != w[i-1]) c[++m] = w[i];
	if(m == 1){printf("%d\n", a);return 0;}
	for(int i = 1;i <= m;++i){
		for(int j = 1;j <= cnt;++j){
			if(c[i] >= l[j] && c[i] <= r[j]) {d[i] = j;++sum[j];break;}
		}
		if(d[i] == 0){
			d[i] = ++cnt;
			sum[cnt] = 1;
			l[cnt] = d[i]-pos;
			r[cnt] = d[i]+pos;
		}
	}
	if(cnt == m){
		printf("%d\n",a*m);
		return 0;
	}
	return 0;
}