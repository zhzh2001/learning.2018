#include <bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a,_i=b;i<=_i;++i)
#define Rep(i,a,b) for (int i=b,_i=a;i>=_i;--i)
using namespace std;

inline int rd(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}
//defs====================================
int K,N,M;
int a[100010];
int b[100010];
struct sts{
	ll st,vv;
	ll mxt;
	sts(ll a=0,ll b=0){st=a,vv=b;}
	bool operator <(const sts&o) const { return st+vv<o.st+o.vv;}
	bool operator >(const sts&o) const { return st+vv>o.st+o.vv;}
	inline void use() { st+=vv;}
	inline ll endpt() {return st+vv;}
};
ll tim[100010],et[100010];
ll ans;
priority_queue<sts,vector<sts>,greater<sts> > QA,QB,QC;
//main====================================
int main() {
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	K=rd(),N=rd(),M=rd();
	for(int i=1;i<=N;++i) a[i]=rd(),QA.push(sts(0,a[i]));
	for(int i=1;i<=M;++i) b[i]=rd(),QB.push(sts(0,b[i]));
	for(int i=1;i<=K;++i) {
		sts minv=QA.top();QA.pop();
		tim[i]=minv.st+minv.vv;
		minv.use();
		QA.push(minv);
	}
	//sort(a+1,a+N+1);sort(b+1,b+M+1);
	//printf("\na:");For(i,1,N) printf("%lld ",a[i]); puts("\n");
	//printf("\nb:");For(i,1,M) printf("%lld ",b[i]); puts("\n");

	//For(i,1,K) printf("%lld ",tim[i]);puts("");
	//For(i,1,K) printf("%lld ",24348-tim[i]);puts("");
	for(int i=K;i>=1;--i) {
	//	printf("i=%d time=%lld\n",i,tim[i]);
		sts mvb(1e14,1e14);
		if(!QB.empty()) {
			mvb=QB.top();QB.pop();	
			mvb.st+=tim[i];			
		}
		sts mvc(1e14,1e14);
		if(!QC.empty()) {
			mvc=QC.top();
			QC.pop();
		}
		if(mvb<mvc) {
			//printf("Choose b:st=%lld vv=%lld\n",mvb.st,mvb.vv);
			et[i]=mvb.endpt();
			mvb.use();
			QC.push(mvb);			
		} else {
		//printf("Choose c:st=%lld vv=%lld\n",mvc.st,mvc.vv);
			et[i]=mvc.endpt();
			mvc.use();
			QC.push(mvc);
			mvb.st=0;
			QB.push(mvb);
		}

	}
	//For(i,1,K) printf("%lld ",et[i]);puts("");
	for(int i=1;i<=K;++i) ans=max(ans,et[i]);
	printf("%lld\n",ans);
	return 0;
}
