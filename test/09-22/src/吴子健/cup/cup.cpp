#include <bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;++i)
#define Rep(i,a,b) for (int i=b;i>=a;--i)
using namespace std;

inline int rd(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}
//defs====================================
int N,M;
int a[2010];
int b[20010];
bitset<1001000> bad;
int sf[1001000];//smallest factor
int f[1001000];

int gcd(int a,int b) {
	return b?gcd(b,a%b):a;
}
//main====================================
int main(){
//	cout<<gcd(1e9+7,2e9+14)<<endl;
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	N=rd(),M=rd();
	For(i,1,N) a[i]=rd();
	For(i,1,M) {
		b[i]=rd();
		bad[b[i]]=1;
	}
	f[1]=0;
	for(int i=2;i<=1000500;++i) {
		for(int j=1;j*i<=1000500;++j) {
			if(!sf[i*j]) {
				sf[i*j]=i;
			}
		}
	}
	for(int i=2;i<=1000500;++i){
		int t=bad[sf[i]]?-1:1;
		f[i]=f[i/sf[i]]+t;
	}
//	for(int i=1;i<=35;++i) printf("%d->f=%d sf=%d\n",i,f[i],sf[i]);
//	int t=0;
//	For(i,1,N) t+=f[a[i]];
//	cout<<t<<endl;
//For(i,1,N) printf("%d ",a[i]);puts("");
	for(int i=N;i>=1;--i) {
		int t=a[1];
		for(int j=2;j<=i;++j) t=gcd(t,a[j]);
		//printf("[1,%d] gcd=%d\n",i,t);
		if(f[t]<0) {
			for(int j=1;j<=i;++j) a[j]/=t;
			//for(int j=1;j<=N;++j) printf("%d ",a[j]);puts("");
		}
	}
	int ans=0;
	For(i,1,N) ans+=f[a[i]];
	cout<<ans<<endl;
	return 0;
}

