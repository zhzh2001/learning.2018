#include <bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a,_i=b;i<=_i;++i)
#define Rep(i,a,b) for (int i=b,_i=a;i>=_i;--i)
using namespace std;

inline int rd(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}
ll f[52][52][52][52];
ll g[52][52];
ll N,A,B,a[55];
//defs====================================
/**
	f[l][r][down][up] l��r ֵ���� [down,up]
	f[l][r][down][up]=min{
		f[l][r][down+1][up],f[l][r][down][up-1],
		f[l+1][r][down][up]+a[l] (a[l]>=down && a[l]<=up),
		f[l][r+1][down][up]+a[r] (a[r]>=down && a[r]<=up),
		f[l+1][r][any][any]+a[l] (a[l]>=down && a[l]<=up)
		f[l][r-1][any][any]+a[r] (a[r]>=down && a[r]<=l)
	} 
*/
int main() {
	ll t=0;
	memset(f,0x3f,sizeof f);
	memset(g,0x3f,sizeof g);
	N=rd(),A=rd(),B=rd();
	for(int i=1;i<=N;++i) a[i]=rd();
	for(int i=2;i<=N;++i) t=max(t,a[i]-a[i-1]);
	if(t==0 ) {
		cout<<A<<endl;
		exit(0);
	}
	for(int i=N;i>=1;--i) {
		ll mx=a[i],mn=a[i];
		for(int j=i;j<=N;++j) {
			mx=max(mx,a[j]),mn=min(mn,a[j]);
			for(int l=0;l<=mn;++l) {
				for(int r=mx;r<=50;++r) {
					f[i][j][l][r]=0;
				//						if(f[i][j][l][r]<1e7&&i>=3&&j<=9&&l>=8&&l<=11&&r>=8&&r<=11)
				//	printf("fff[%d][%d][%d][%d]=%lld\n",i,j,l,r,f[i][j][l][r]);
					g[i][j]=min(g[i][j],A+B*(r-l)*(r-l));
					//printf("ggg[%d][%d]=%lld\n",i,j,g[i][j]);
				}
			}
		}
	}
	for(int i=N;i>=1;--i) {
		for(int j=i;j<=N;++j) {
			for(int l=50;l>=1;--l) {
				for(int r=l+1;r<=50;++r) {
				//	if(f[i][j][l][r]<1e7&&i>=3&&j<=9&&l>=8&&l<=11&&r>=8&&r<=11)
				//	printf("f[%d][%d][%d][%d]=%lld\n",i,j,l,r,f[i][j][l][r]);
					f[i][j][l][r]=min(f[i][j][l+1][r],f[i][j][l][r]);
					f[i][j][l][r]=min(f[i][j][l][r-1],f[i][j][l][r]);
					if(a[i]>=l && a[i]<=r) f[i][j][l][r]=min(f[i+1][j][l][r],f[i][j][l][r]);
					if(a[j]>=l && a[j]<=r) f[i][j][l][r]=min(f[i][j-1][l][r],f[i][j][l][r]);
					for(int k=i;k<j;++k) {
						f[i][j][l][r]=min(g[k+1][j]+f[i][k][l][r],f[i][j][l][r]);
					}
					for(int k=j;k>i;--k) {
						f[i][j][l][r]=min(g[i][k-1]+f[k][j][l][r],f[i][j][l][r]);
					}
					if(a[j]>=l && a[j]<=r) f[i][j][l][r]=min(g[i][j-1],f[i][j][l][r]);			
					g[i][j]=min(g[i][j],f[i][j][l][r]+A+B*(r-l)*(r-l));
				//	if(f[i][j][l][r]<1e7&&i>=3&&j<=9&&l>=8&&l<=11&&r>=8&&r<=11)
				//	printf("f[%d][%d][%d][%d]=%lld\n",i,j,l,r,f[i][j][l][r]);
				}
			}
		//	printf("g[%d][%d]=%lld\n",i,j,g[i][j]);
		}
	}
	cout<<g[1][N]<<endl;
	return 0;
}
