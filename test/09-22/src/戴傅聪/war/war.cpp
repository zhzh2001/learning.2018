#include<bits/stdc++.h>
using namespace std;
inline void bitprint(int x)
{
	if(x>=2) bitprint(x>>1);
	putchar((x&1)+'0');
}
int n,a,b,s[51];int ans;
int minval[1<<16],maxval[1<<16],Log2[1<<16];
inline int lowbit(int x){return x&-x;}
inline void init()
{
	cin>>n>>a>>b;
	for(int i=1;i<=n;i++) cin>>s[i];
	bool flag=1;
	for(int i=1;i<n;i++) if(s[i]!=s[i+1]) flag=0;
	if(flag)
	{
		cout<<a;
		exit(0);
	}
	if(n<=16)
	{
		Log2[1]=1;for(int i=2;i<(1<<n);i++) Log2[i]=Log2[i>>1]+1;
		minval[0]=1000000000;maxval[0]=0;
		for(int i=1;i<(1<<n);i++) minval[i]=min(minval[i-lowbit(i)],s[Log2[lowbit(i)]]);
		for(int i=1;i<(1<<n);i++) maxval[i]=max(maxval[i-lowbit(i)],s[Log2[lowbit(i)]]);
	}
	ans=n*a;
}
inline int bet(int x,int y){return (1<<y)-(1<<(x-1));}
inline pair<int,int> getminmax(int x)
{
	return make_pair(maxval[x],minval[x]);
	int minn=100000000,maxx=0;
	for(int i=1;x;x>>=1,i++) if(x&1) minn=min(s[i],minn),maxx=max(s[i],maxx);
	return make_pair(maxx,minn);
}
int f[1<<16];bool vis[1<<16];
inline int sqr(int x){return x*x;}
inline int dp(int cur)
{
	if(cur==(1<<n)-1) return 0;
	if(vis[cur]) return f[cur];vis[cur]=1;f[cur]=1000000000;
	for(int i=1;i<=n;i++) for(int j=i;j<=n;j++)
	{
		int kkk=bet(i,j);if((cur&kkk)==kkk) continue;
		kkk=(cur&kkk)^kkk;pair<int,int> t=getminmax(kkk);
		f[cur]=min(f[cur],dp(cur|kkk)+a+b*sqr(t.first-t.second));
	}
	return f[cur];
}
int main()
{
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	init();
	cout<<dp(0);return 0;
}
/*
10
3 1
7 10 9 10 6 7 10 7 1 2
*/
/*
15
50 15
6 7 8 9 10 11 10 9 8 7 6 5 4 3 2
*/
