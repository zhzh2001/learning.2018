#include<bits/stdc++.h>
using namespace std;
#define int long long
struct node{int intime,needtime;};
bool operator <(const node &a,const node &b){return a.intime+a.needtime+a.needtime>b.intime+b.needtime+b.needtime;}
priority_queue<node> q;
int l,n,m;int a[100001],b[100001];
int fina[100001];
inline void init()
{
	scanf("%lld%lld%lld",&l,&n,&m);
	for(int i=1;i<=n;i++) scanf("%lld",a+i);
	for(int i=1;i<=m;i++) scanf("%lld",b+i);
	sort(a+1,a+n+1);int cur=1,tot=0;
	for(int i=1;i<=l;i++)
	{
		if(cur<=n)
		{
			if(q.size())
			{
				node p=q.top();q.pop();
				int ntime=p.intime+p.needtime;
				if(ntime+p.needtime<a[cur])
				{
					fina[++tot]=ntime;
					q.push((node){ntime,p.needtime});
				}
				else
				{
					q.push((node){0,a[cur]});q.push(p);
					cur++;
				}
			}
			else
			{
				q.push((node){0,a[cur]});
				cur++;
			}
		}
		else
		{
			node p=q.top();q.pop();
			fina[++tot]=p.intime+p.needtime;
			q.push((node){fina[tot],p.needtime});
		}
	}
	while(q.size())
	{
		node p=q.top();q.pop();
		fina[++tot]=p.intime+p.needtime;
	}
}
inline int Run()
{
	while(q.size()) q.pop();int ans=0;int cur=1;
	for(int i=1;i<=l;i++)
	{
		if(cur<=m)
		{
			if(q.size())
			{
				node p=q.top();q.pop();
				int ntime=p.intime+p.needtime;
				if(max(ntime,fina[i])+p.needtime<fina[i]+b[cur])
				{
					ans=max(ans,ntime);
					q.push((node){max(ntime,fina[i]),p.needtime});
				}
				else
				{
					q.push(p);q.push((node){fina[i],b[cur]});
					cur++;
				}
			}
			else
			{
				q.push((node){fina[i],b[cur]});
				cur++;
			}
		}
		else
		{
			node p=q.top();q.pop();
			int ntime=p.intime+p.needtime;ans=max(ans,ntime);
			q.push((node){max(ntime,fina[i]),p.needtime});
		}
	}
	while(q.size())
	{
		node p=q.top();q.pop();
		ans=max(ans,p.intime+p.needtime);
	}
	return ans;
}
signed main()
{
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	init();
	sort(fina+1,fina+l+1);
	sort(b+1,b+m+1);
	int ans=Run();sort(fina+1,fina+l+1,greater<int>());ans=min(ans,Run());
	for(int i=10000000/m/10;i;i--)
	{
		random_shuffle(fina+1,fina+l+1);
		ans=min(ans,Run());
	}
	printf("%lld\n",ans);
}
/*
5 4 2
1 2 3 100
10 10
*/
