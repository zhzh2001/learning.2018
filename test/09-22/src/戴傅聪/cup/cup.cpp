#include<bits/stdc++.h>
using namespace std;
inline int gcd(int a,int b)
{
	while(a&&b) if(a>b) a%=b;else b%=a;
	return max(a,b);
}
set<int> badprimes;
inline bool isprime(int x)
{
	for(int i=2;i*i<=x;i++) if(x%i==0) return 0;
	return 1;
}
bool vis[1000001];int f[1000001];
inline int calc(int x)
{
	if(x==1) return 0;
	if(x<=1000000){if(vis[x]) return f[x];vis[x]=1;}
	for(int i=2;i*i<=x;i++) if(x%i==0)
	{
		if(badprimes.count(i)) return (x<=1000000)?(f[x]=calc(x/i)-1):(calc(x/i)-1);
		else return (x<=1000000)?(f[x]=calc(x/i)+1):(calc(x/i)+1);
	}
	if(badprimes.count(x)) return (x<=1000000)?(f[x]=-1):-1;
	return (x<=1000000)?(f[x]=1):1;
}
int n,a[2001];int tot;
int main()
{
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	cin>>n;int m;cin>>m;int ans=0;
	for(int i=1;i<=n;i++) scanf("%d",a+i);
	for(int i=1,t;i<=m;i++) scanf("%d",&t),badprimes.insert(t);
	for(int i=n;i;i--)
	{
		int cur=0;for(int j=1;j<=i;j++) cur=gcd(cur,a[j]);
		if(calc(cur)<0) for(int j=1;j<=i;j++) a[j]/=cur;
	}
	for(int i=1;i<=n;i++) ans+=calc(a[i]);
	cout<<ans;return 0;
}
