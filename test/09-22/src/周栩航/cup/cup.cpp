#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=2005;
int a[N],b[N],n,m;
map<int,int> huai;
inline int gcd(int x,int y){return y==0?x:gcd(y,x%y);}
int main()
{
	freopen("cup.in","r",stdin);freopen("cup.out","w",stdout);
	n=read();m=read();
	For(i,1,n)	a[i]=read();
	For(i,1,m)	b[i]=read(),huai[b[i]]=1;
	Dow(end,1,n)
	{
		int now=a[1];
		For(i,2,end)	now=gcd(now,a[i]);
		int del=0,mx=sqrt(now),tmp=now;
		For(i,2,sqrt(now))	
		{
			if(now%i==0)
			{
				while(now%i==0)	
				{
					if(huai[i])	del--;else	del++;
					now/=i;
				}
			}
		}
		if(now>1)	if(huai[now])	del--;else	del++;
		if(del<0)	For(i,1,end)	a[i]/=tmp;
	}
	int ans=0;
	For(i,1,n)
	{
		int mx=sqrt(a[i]);
		int now=a[i];
		For(i,2,sqrt(now))	
		{
			if(now%i==0)
			{
				while(now%i==0)
				{
					if(huai[i])	ans--;else	ans++;
					now/=i;
				}
			}
		}
		if(now>1)	if(huai[now])	ans--;else	ans++;
	}
	writeln(ans);
}
