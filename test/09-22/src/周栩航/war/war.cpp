#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back
#define int ll


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=51;
int n,a,b,v[N],ans,Tim;
int dp[2000005];
signed main()
{
	freopen("war.in","r",stdin);freopen("war.out","w",stdout);
	n=read();a=read();b=read();
	For(i,1,n)	v[i]=read();
	bool tp=1;
	For(i,1,n)	if(v[i]!=v[1])	tp=0;
	if(tp)	{writeln(a);return 0;}
	dp[0]=0;
	int mx=(1<<n)-1;
	For(i,1,mx)	dp[i]=1e9;
	For(i,0,mx)
	{
		For(l,1,n)
			For(r,l,n)
			{
				int zt=i,mi=1e9,mx=0;
				For(p,l,r)	zt|=(1<<p-1);
				For(p,l,r)	if(!(i&(1<<p-1)))mi=min(mi,v[p]),mx=max(mx,v[p]);
				if(mx==0)	continue;
				dp[zt]=min(dp[zt],dp[i]+a+b*(mx-mi)*(mx-mi));
			}
	}
	writeln(dp[mx]);
}
/*
10
3 1
7 10 9 10 6 7 10 7 1 2
*/
