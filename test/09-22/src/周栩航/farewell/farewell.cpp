#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back
#define int ll


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=100005;
ll num,n,m,a[N],b[N],q[N],mx,tim[N];
inline void Get()
{
	int top=0;
	For(i,1,n)	
		for(int j=a[i];j<=mx;j+=a[i])
			q[++top]=j;
	sort(q+1,q+top+1);
}
inline bool check1(ll tmp)
{
	ll cnt=0;
	For(i,1,n)	cnt+=(tmp/a[i]);
	return cnt>=num;
}
inline bool check(ll mx)
{
	For(j,1,m)	tim[j]=0;
	int now=q[1];
	int i=1;
	while(i<=num)
	{
		bool ok=0;
		For(j,1,m)
		{
			if(tim[j]<=now&&now+b[j]<=mx)
			{
				tim[j]=now+b[j];
				ok=1;
				break;
			}
		}
		if(!ok)	return 0;
		++i;
		if(i>num)	break;
		ll tmp=q[i],tmp1=1e17,tmp2=1e17;
		For(j,1,m)	if(max(q[i],tim[j])+b[j]<=mx)	tmp2=min(tmp2,tim[j]);
		now=max(tmp,tmp2);
	}
	return 1;
}
signed main()
{
	freopen("farewell.in","r",stdin);freopen("farewell.out","w",stdout);
	num=read();n=read();m=read();
	ll l=1,r=1e17;
	For(i,1,n)	a[i]=read();
	For(i,1,m)	b[i]=read();
	sort(b+1,b+m+1);
	while(l<=r)
	{
		ll mid=(l+r)>>1;
		if(check1(mid))	mx=mid,r=mid-1;else	l=mid+1;
	}
	Get();
	l=q[num];r=1e17;
	ll ans=0;
	while(l<=r)
	{
		ll mid=l+r>>1;
		if(check(mid))	ans=mid,r=mid-1;else	l=mid+1;
	}
	writeln(ans);
}
