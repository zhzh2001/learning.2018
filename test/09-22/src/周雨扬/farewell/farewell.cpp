#include<bits/stdc++.h>
#define ll long long
#define pll pair<ll,ll>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
using namespace std;
int l,n,m,a[100005];
ll v1[100005],v2[100005];
priority_queue<pll > q;
void workprb(int sz1,int *a,int sz2,ll *v){
	while (!q.empty()) q.pop();
	For(i,1,sz1) q.push(pll(-a[i],i));
	For(i,1,sz2){
		ll val=q.top().first;
		int id=q.top().second;
		q.pop();
		v[i]=-val;
		q.push(pll(val-a[id],id));
	}
}
int main(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	scanf("%d%d%d",&l,&n,&m);
	For(i,1,n) scanf("%d",&a[i]);
	workprb(n,a,l,v1);
	For(i,1,m) scanf("%d",&a[i]);
	workprb(m,a,l,v2);
	ll ans=0;
	For(i,1,l) ans=max(ans,v1[i]+v2[l-i+1]);
	printf("%lld\n",ans);
}
