#include<cstdio>
#include<algorithm>
#define ll long long
#define pll pair<ll,ll>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
using namespace std;
const int N=55;
int n,a,b,v[N];
int f[N][N],fl[N],G[N];
int main(){
	freopen("war.in","r",stdin);
	freopen("war.out","w",stdout);
	scanf("%d%d%d",&n,&a,&b);
	For(i,1,n) scanf("%d",&v[i]);
	For(i,1,n) For(j,1,n) f[i][j]=(1<<30)-233;
	For(i,1,n) f[i][i]=a;
	for (int i=n;i;i--) For(mn,i,n) For(mx,i,n) if (v[mn]<v[mx]){
		For(k,i,n) fl[k]=(v[k]<v[mn]||v[k]>v[mx]),G[k]=(1<<30)-233;
		fl[i-1]=0; G[i-1]=a+b*(v[mx]-v[mn])*(v[mx]-v[mn]);
		For(k,i,n)
			if (!fl[k]) G[k]=G[k-1];
			else For(l,i-1,k-1) G[k]=min(G[k],G[l]+f[l+1][k]);
		For(j,max(mn,mx),n)	f[i][j]=min(f[i][j],G[j]);
	}
	printf("%d",f[1][n]);
}
/*
10
3 1
7 10 9 10 6 7 10 7 1 2
*/
