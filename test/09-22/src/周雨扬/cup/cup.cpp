#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
using namespace std;
const int N=2005;
int n,m,a[N],d[N][20];
map<int,int> bad,F;
int yk[20],G[N],GG[N];
int f[N][N];
void divide(int *d,int v){
	for (int i=2;i*i<=v;i++)
		if (v%i==0){
			d[++*d]=i;
			for (;v%i==0;v/=i);
		}
	if (v!=1) d[++*d]=v;
}
int getf(int x){
	if (F.find(x)!=F.end()) return F[x];
	For(i,1,*yk)
		if (x%yk[i]==0)
			return F[x]=getf(x/yk[i])+(bad[yk[i]]?-1:1);
	return F[x]=0;
}
int gcd(int x,int y){
	return y?gcd(y,x%y):x;
}
int main(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	scanf("%d%d",&n,&m);
	For(i,1,n){
		scanf("%d",&a[i]);
		divide(d[i],a[i]);
	}
	For(i,1,m){
		int x;
		scanf("%d",&x);
		bad[x]=1;
	}
	G[1]=GG[1]=a[1];  int tmp=1;
	For(i,2,n) G[i]=gcd(G[i-1],a[i]);
	For(i,2,n) if (G[i]!=G[i-1]) GG[++tmp]=G[i];
	GG[++tmp]=1; G[n+1]=1;
	For(i,1,n){
		memcpy(yk,d[i],sizeof(d[i]));
		For(j,1,tmp)  getf(a[i]/GG[j]);
	}
	memset(f,233,sizeof(f));
	f[n+1][n+1]=0;
	for (int i=n;i;i--){
		int mx=-(1<<30);
		int las=-1,lav=0;
		For(j,i+1,n+1){
			if (las!=G[j]){
				las=G[j];
				lav=F[a[i]/G[j]];
			}
			mx=max(mx,f[i+1][j]);
			f[i][j]=f[i+1][j]+lav;
			//printf("%d %d %d\n",i,j,f[i][j]);
		}
		f[i][i]=mx+F[a[i]/G[i]];
		//printf("%d %d %d\n",i,i,f[i][i]);
	}
	int ans=-(1<<30);
	For(i,1,n+1) ans=max(ans,f[1][i]);
	printf("%d\n",ans);
}
/*
5 2
4 20 34 10 10
2 5

4 5
2 4 8 16
3 5 7 11 17
*/
