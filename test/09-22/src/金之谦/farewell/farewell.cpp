#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int L,n,m,a[N],b[N],l[N],la[N],ma[N],ans=0;
struct ppap{int x,v;};
bool operator <(ppap a,ppap b){return a.v>b.v;}
priority_queue<ppap>q;
struct qqaq{int x,v;};
bool operator <(qqaq a,qqaq b){return a.v==b.v?a.x<b.x:a.v>b.v;}
set<qqaq>s,ss;
inline void work(int i){
	qqaq now=*(--ss.end());ss.erase(now);
	ma[now.x]+=l[i]+b[now.x]-la[now.x];
	ans=max(ans,ma[now.x]);
	s.erase((qqaq){now.x,la[now.x]-b[now.x]});
	la[now.x]=l[i];
	s.insert((qqaq){now.x,la[now.x]-b[now.x]});
	ss.insert((qqaq){now.x,ma[now.x]+b[now.x]-la[now.x]});
}
signed main()
{
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
	L=read();n=read();m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		q.push((ppap){i,a[i]});
	}
	for(int i=1;i<=L;i++){
		ppap now=q.top();q.pop();
		l[i]=now.v;q.push((ppap){now.x,now.v+a[now.x]});
	}
	while(!q.empty())q.pop();
	for(int i=1;i<=m;i++){
		b[i]=read();
		q.push((ppap){i,b[i]});
	}
	memset(la,-1,sizeof la);
	for(int i=L;i;i--){
		while(s.size()){
			if(s.begin()->v>=l[i]){
				qqaq now=*s.begin();s.erase(now);
				ss.erase((qqaq){now.x,ma[now.x]+b[now.x]-la[now.x]});
				q.push((ppap){now.x,b[now.x]});
			}
			else break;
		}
		int rp=1e18;
		if(ss.size()){
			qqaq now=*(--ss.end());
			rp=ma[now.x]+l[i]+b[now.x]-la[now.x];
		}
		if(!q.empty()){
			ppap now=q.top();
			if(la[now.x]==-1){
				if(l[i]+now.v>rp){
					work(i);continue;
				}
				la[now.x]=l[i];
				ma[now.x]=l[i]+now.v;
				ans=max(ans,ma[now.x]);
			}
			else la[now.x]-=now.v;
			q.pop();
			s.insert((qqaq){now.x,la[now.x]-b[now.x]});
			ss.insert((qqaq){now.x,ma[now.x]+b[now.x]-la[now.x]});
		}else work(i);
	}
	writeln(ans);
	return 0;
}