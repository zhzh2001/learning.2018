#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=2010,NN=1e6+10;
int n,m,a[N],b[N],pri[NN],vis[NN],cnt=0,bb[NN];
inline int gcd(int a,int b){return b?gcd(b,a%b):a;}
inline void get(){
	int NNN=NN-10;
	for(int i=2;i<=NNN;i++){
		if(!vis[i])pri[++cnt]=i;
		for(int j=1;j<=cnt;j++){
			if(i*pri[j]>NNN)break;
			vis[i*pri[j]]=1;
			if(i%pri[j]==0)break;
		}
	}
}
inline int f(int x){
	int sum=0;
	for(int j=1;j<=cnt&&pri[j]*pri[j]<=x;j++)if(x%pri[j]==0){
		int q=1;
		if(bb[pri[j]]==1)q=-1;
		while(x%pri[j]==0)sum+=q,x/=pri[j];
	}
	if(x>1){
		int p=lower_bound(b+1,b+m+1,x)-b,q=1;
		if(x==b[p])q=-1;
		sum+=q;
	}
	return sum;
}
int main()
{
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	get();
	n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=m;i++){
		b[i]=read();
		if(b[i]<=NN-10)bb[b[i]]=1;
	}
	for(int i=n;i;i--){
		int g=a[1];
		for(int j=2;j<=i;j++)g=gcd(g,a[j]);
		int p=f(g);
		if(p<0){
			for(int j=1;j<=i;j++)a[j]/=g;
		}
	}
	int ans=0;
	for(int i=1;i<=n;i++)ans+=f(a[i]);
	writeln(ans);
	return 0;
}