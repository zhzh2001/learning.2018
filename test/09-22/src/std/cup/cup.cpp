#include<bits/stdc++.h>
#define ll long long
#define N 500005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=a;i<=b;i++)
#define D(i,a,b) for (int i=a;i>=b;i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define fi first
#define se second
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,a[N],x,ans;
set <int> mp;
int fj(int x){
	int p,t=sqrt(x),num=0,x2=x;
	F(i,2,t){
		p=0;
		while (x2%i==0&&x2>1){
			p++;x2/=i;
		}
		if (p){
			if (mp.find(i)==mp.end()) num+=p;
			else num-=p;
		}
	}
	if (x2>1){
		if (mp.find(x2)==mp.end()) num++;
		else num--;
	}
	return num;
}
signed main(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout); 
	n=read();m=read();
	F(i,1,n) a[i]=read();
	F(i,1,m) mp.insert(read());
	D(i,n,1){
		x=a[1];
		F(j,2,i){
			x=__gcd(x,a[j]);
		}
		if (fj(x)<0){
			F(j,1,i) a[j]/=x;
		}
	}
	F(i,1,n) ans+=fj(a[i]);
	wrn(ans);
	return 0;
}
//cf402d
