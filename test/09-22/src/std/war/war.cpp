#include<iostream>
#include<vector>
#include<algorithm>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<cstdlib>
#include<ctime>
#include<queue>
#include<set>
using namespace std;
typedef long long LL;
const int N=52;
int gi() {
    int w=0;bool q=1;char c=getchar();
    while ((c<'0'||c>'9') && c!='-') c=getchar();
    if (c=='-') q=0,c=getchar();
    while (c>='0'&&c <= '9') w=w*10+c-'0',c=getchar();
    return q? w:-w;
}
int w[N],f[N][N],mx[N][N],mi[N][N];
int g[N][N][N],x[N];
inline void upd(int &x,int y) { y<x?x=y:0; }
int main()
{
    freopen("war.in","r",stdin);
    freopen("war.out","w",stdout);
    int n=gi(),i,j,k,a=gi(),b=gi(),l,r,len,lim,t,mid;
    for (i=1;i<=n;i++) w[i]=x[i]=gi();
    sort(x+1,x+1+n);len=unique(x+1,x+1+n)-x-1;
    for (i=1;i<=n;w[i++]=l) for (l=1,r=len;l!=r;w[i]<=x[mid=(l+r)>>1]?r=mid:l=mid+1);
    for (i=1;i<=n;i++) f[i][i]=a;
    for (lim=2;lim<=n;lim++)
        for (l=1,r=lim;r<=n;l++,r++) {
            for (i=l;i<=r;i++)
                for (j=len;j;j--)
                    for (k=len;k>=j;k--)
                        g[i][j][k]=1<<30;
            g[l][w[l]][w[l]]=0;
            for (i=l;i<r;i++)
                for (j=len;j;j--)
                    for (k=len;k>=j;k--)
                        if (g[i][j][k]!=1<<30) {
                            upd(g[i+1][min(j,w[i+1])][max(k,w[i+1])],g[i][j][k]);
                            for (t=r;t>i;t--)
                                upd(g[t][j][k],g[i][j][k]+f[i+1][t]);
                        }
            f[l][r]=1<<30;
            for (j=len;j;j--)
                for (k=len;k>=j;k--)
                    upd(f[l][r],g[r][j][k]+a+b*(x[j]-x[k])*(x[j]-x[k]));
        }
    printf("%d\n",f[1][n]);
    return 0;
}
