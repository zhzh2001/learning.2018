#include<bits/stdc++.h>
#define ll long long
#define int ll
#define put putchar('\n');
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
typedef long long LL;
const int maxn=1e5+5;
LL N[maxn],M[maxn],L[1000005];
struct node{
   int id; LL t;
}y,yy;
bool operator<(node a,node b){return a.t>b.t;}
signed main(){
	freopen("farewell.in","r",stdin);
	freopen("farewell.out","w",stdout);
    int l,n,m;
    l=read();n=read();m=read();
    priority_queue<node>Q,P;
    for ( int i=1;i<=n;i++ ) N[i]=read();
    for ( int i=1;i<=m;i++ ) M[i]=read();
    for ( int i=1;i<=n;i++ )
        y.id=i, y.t=N[i], Q.push(y);
    for ( int i=1;i<=m;i++ )
        yy.id=i, yy.t=M[i], P.push(yy);
    for ( int i=1;i<=l;i++ )
    {
        int id=Q.top().id;
        LL t=Q.top().t; Q.pop();
        L[i]=t;
        y.id=id; y.t=t+N[id];
        Q.push(y);
    }
    LL ans = 0;
    for ( int i=l;i>0;i-- )
    {
        int id=P.top().id;
        LL tt=P.top().t; P.pop();
        yy.id=id;
        yy.t=tt+M[id];
        P.push(yy);
        ans = max( ans, L[i]+tt );
    }
    printf( "%lld\n", ans );
    return 0;
}

