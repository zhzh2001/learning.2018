#include<cstdio>
#include<map>
#include<cstring>
#include<algorithm>
#define ll int
#define inf (1e9)
#define N 1000005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,a[N],flag[N],f[N],g[N],pri[N],num,ans;
map<ll,ll> b;
ll gcd(ll x,ll y) { 
	return y?gcd(y,x%y):x;
}
int main(){
	freopen("cup.in","r",stdin);
	freopen("cup.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n) a[i]=read();
	rep(i,1,m) b[read()]=1;
	rep(i,2,40000){
		if (!flag[i]) { pri[++num]=i,flag[i]=i; }
		for (ll j=1;j<=num&&i*pri[j]<=40000;++j){
			flag[pri[j]*i]=pri[j];
			if (!(i%pri[j])) break;
		}
	}
	rep(i,1,n) g[i]=gcd(a[i],g[i-1]);
	rep(i,1,n){
		ll tmp=a[i];
		rep(j,1,num){
			while (!(tmp%pri[j])){
				tmp/=pri[j];
				if (b[pri[j]]) --ans;
				else ++ans;
			}
		}
		if (b[tmp]) --ans;
		else if (tmp!=1) ++ans;
	}
	ll now=1;
	per(i,n,1){
		g[i]/=now;
		ll t=0,tmp=g[i];
		rep(j,1,num){
			while (!(tmp%pri[j])){
				tmp/=pri[j];
				if (b[pri[j]]) --t;
				else ++t;
			}
			if (tmp==1) break;
		}
		if (b[tmp]) --t;
		else if (tmp!=1) ++t;
		if (t<0){
			ans-=t*i;
			now*=g[i];
		}
	}
	printf("%d",ans);
}
