#include <cstring>
#include <fstream>
#include <algorithm>
#include <iostream>

using namespace std;

const int maxn = 60;

typedef long long LL;

LL dp[maxn][maxn][maxn];
LL maxx[maxn][maxn];
LL minn[maxn][maxn];

const LL inf = 0x3f3f3f3f3f3f3f3f;

inline LL sqr(LL a)
{
	return a * a;
}

int main()
{
	ifstream fin("war.in");
	ofstream fout("war.out");
	int n;
	fin >> n;
	LL a, b;
	fin >> a >> b;
	memset(dp, 0x3f, sizeof(dp));
	for(int i = 1; i <= n; ++i)
	{
		fin >> minn[i][i];
		maxx[i][i] = minn[i][i];
		dp[i][i][1] = 0;
	}
	for(int l = n; l; --l)
	{
		for(int r = l + 1; r <= n; ++r)
		{
			maxx[l][r] = max(maxx[l][r-1], maxx[r][r]);
			minn[l][r] = min(minn[l][r-1], minn[r][r]);
		}
	}
	for(int l = n; l; --l)
	{
		for(int r = l + 1; r <= n; ++r)
		{
			dp[l][r][1] = sqr(maxx[l][r] - minn[l][r]);
			for(int mid = l; mid < r; ++mid)
			{
				for(int k = 2; k <= (r - l + 1); ++k)
				{
					for(int K = 1; K < k; ++K)
						dp[l][r][k] = min(dp[l][r][k], dp[l][mid][k-K] + dp[mid+1][r][K]);
				}
			}
		}
	}
	LL ans = inf;
//	for(int l = 1; l <= n; ++l)
//		for(int r = l; r <= n; ++r)
//			cout << l << ' ' << r << ' ' << dp[l][r][r-l+1] << endl;
	for(int i = 1; i <= n; ++i)
		ans = min(ans, dp[1][n][i] * b + i * a);
	fout << ans << endl;
	fin.close();
	fout.close();
	return 0;
}
