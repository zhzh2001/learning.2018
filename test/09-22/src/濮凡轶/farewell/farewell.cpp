#include <cstdio>
#include <ctime>
#include <set>
#include <queue>
#include <vector>
#include <utility>
#include <algorithm>
#include <iostream>

#define deb(a) cout << #a << " = " << a << endl

using namespace std;

typedef long long LL;
typedef pair<int, int> pii;
typedef priority_queue<int> hHeap;
typedef priority_queue<pii> hHeapii;
typedef priority_queue<int, vector<int>, greater<int> > sHeap;
typedef priority_queue<pii, vector<pii>, greater<pii> > sHeapii;
typedef set<int> st;
typedef vector<int> ve;

const int inf = 0x3f3f3f3f;

int main()
{
	freopen("farewell.in", "r", stdin);
	freopen("farewell.out", "w", stdout);
	srand((unsigned) time(NULL));
	int n, m, l;
	scanf("%d%d%d", &l, &n, &m);
	unsigned cc = clock();
//	sHeap hh;
	ve hh;
	sHeapii ss;
//	while(!hh.empty())
//		hh.pop();
	while(!ss.empty())
		ss.pop();
	for(int i = 1, a; i <= n; ++i)
	{
		scanf("%d", &a);
		ss.push(make_pair(a, a));
	}
	for(int i = 1; i <= l; ++i)
	{
		pii tmp = ss.top();
		ss.pop();
		hh.push_back(tmp.first);
//		deb(tmp.first);
		tmp.first += tmp.second;
		ss.push(tmp);
	}
	sHeapii hh2;
	for(int i = 1, a; i <= m; ++i)
	{
		scanf("%d", &a);
		hh2.push(make_pair(a, a));
	}
	sHeapii bak = hh2;
	int anss = inf;
	int ans = 0;
	if(n <= 10)
	{
		do
		{
			hh2 = bak;
			int ans = 0;
			for(unsigned i = 0; i < hh.size(); ++i)
			{
				int now = hh[i];
//			deb(now);
				pii aa = hh2.top();
//			cout << aa.first << ' ' << aa.second << endl;
				hh2.pop();
				ans = max(max(aa.first, now + aa.second), ans);
				aa.first = max(now + aa.second, aa.first + aa.second);
				hh2.push(aa);
			}
			anss = min(anss, ans);
		}
		while(clock() - cc <= 1800 && next_permutation(hh.begin(), hh.end()));
	}
	else
	{
		sort(hh.begin(), hh.end());
		for(unsigned i = 0; i < hh.size(); ++i)
		{
			int now = hh[i];
//			deb(now);
			pii aa = hh2.top();
//			cout << aa.first << ' ' << aa.second << endl;
			hh2.pop();
			ans = max(max(aa.first, now + aa.second), ans);
			aa.first = max(now + aa.second, aa.first + aa.second);
			hh2.push(aa);
		}
		anss = min(ans, anss);
		ans = 0;
		hh2 = bak;
		sort(hh.begin(), hh.end(), greater<int>());
		for(unsigned i = 0; i < hh.size(); ++i)
		{
			int now = hh[i];
//			deb(now);
			pii aa = hh2.top();
//			cout << aa.first << ' ' << aa.second << endl;
			hh2.pop();
			ans = max(max(aa.first, now + aa.second), ans);
			aa.first = max(now + aa.second, aa.first + aa.second);
			hh2.push(aa);
		}
		anss = min(ans, anss);
		while(clock() - cc <= 1800)
		{
			hh2 = bak;
			random_shuffle(hh.begin(), hh.end());
			int ans = 0;
			for(unsigned i = 0; i < hh.size(); ++i)
			{
				int now = hh[i];
//			deb(now);
				pii aa = hh2.top();
//			cout << aa.first << ' ' << aa.second << endl;
				hh2.pop();
				ans = max(max(aa.first, now + aa.second), ans);
				aa.first = max(now + aa.second, aa.first + aa.second);
				hh2.push(aa);
			}
			anss = min(anss, ans);
		}
	}
	printf("%d\n", anss);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
