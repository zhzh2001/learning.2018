#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>
#include<map>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=50005;
const int Mod=1e9+7;

inline int Pow(int base,int k){
	int res=1;
	for (int i=1;i<=k;i*=2,base=1ll*base*base%Mod){
		if (i&k) res=1ll*res*base%Mod;
	}
	return res;
}

int n,q,Dat[N];
map<int,int>Ref[N],Lcm[N];
int Mav[N],Lcr[N],Res[N];
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	q=read();
	Rep(i,1,q){
		int opt=read(),x=read();
		if (opt==1){
			Dat[++n]=x;Res[n]=Lcr[n]=1;
			int val=x;
			for (int j=2;j*j<=val;j++){
				while (val%j==0){
					Ref[n][j]++;
					val/=j;
				}
			}
			if (val>1) Ref[n][val]++;
			Rep(j,1,n){
				Mav[j]=max(Mav[j],Dat[n]);
				for (map<int,int>::iterator it=Ref[n].begin();it!=Ref[n].end();it++){
					if (it->second>Lcm[j][it->first]){
						Lcr[j]=1ll*Lcr[j]*Pow(it->first,(it->second)-Lcm[j][it->first])%Mod;
						Lcm[j][it->first]=it->second;
					}
				}
				Res[j]=1ll*Res[j]*Pow(Lcr[j],Mav[j])%Mod;
//				printf("Mav[%d]=%d Lcr[%d]=%d\n",j,Mav[j],j,Lcr[j]);
			}
		}
		if (opt==2){
			int res=1;
			Rep(l,x,n) res=1ll*res*Res[l]%Mod;
			printf("%d\n",res);
		}
	}
}
/*
5
1 2
1 3
1 1
1 4
2 1

4
1 1
1 2
1 3
2 1
*/
