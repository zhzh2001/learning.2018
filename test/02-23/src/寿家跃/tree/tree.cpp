#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>
#include<map>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=25;
const int Mod=1e9+7;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
inline int Count(int val){
	int res=0;
	Rep(i,1,18) if ((val>>i-1)&1) res++;
	return res;
}
int n,m,Dat[N][N],f[1<<18][20];
map<int,int>Map;
int Solve(int val){
	if (Map.count(val)) return Map[val];
	if (val==0) return 0;
	if (val==1) return 1;
	int val_=val-1,ls=val_/2,rs=val_-ls;
	int res=(2ll*ls*rs+Solve(ls)+Solve(rs)+2ll*ls+2ll*rs+1)%Mod;
	return Map[val]=res;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),m=read();
	if (m==0){
		printf("%d\n",Solve(n));return 0;
	}
	Rep(i,2,n){
		Dat[i][i/2]++;
		Dat[i/2][i]++;
	}
	Rep(i,1,m){
		int u=read(),v=read();
		Dat[u][v]++,Dat[v][u]++;
	}
	/*
	Rep(i,1,n){
		Rep(j,1,n) printf("%d ",Dat[i][j]);puts("");
	}
	*/
	int Ans=0;
	Rep(i,1,(1<<n)-1) Rep(j,1,n) if ((i>>j-1)&1){
		if (Count(i)==1){
			f[i][j]=1;
		}
		Rep(k,1,n) if (((i>>k-1)&1)==0){
			Add(f[i^(1<<k-1)][k],1ll*f[i][j]*Dat[j][k]%Mod);
//			if ((i^(1<<k-1))==6) printf("i=%d val=%d kes=%d\n",i,f[i][j],Dat[j][k]);
		}
//		printf("f[%d][%d]=%d\n",i,j,f[i][j]);
		Add(Ans,f[i][j]);
	}
	printf("%d\n",Ans);
}
