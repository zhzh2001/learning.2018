#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=15;

int Dec[N];
inline int Able(int ch){
	if (ch>='0' && ch<='9') return ch-48;
	if (ch>='a' && ch<='z') return ch-97+10;
	return -1;
}
inline void Getstring(int *Dat){
	*Dat=0;int ch=getchar();
	while (Able(ch)==-1) ch=getchar();
	while (Able(ch)!=-1) Dat[++*Dat]=Able(ch),ch=getchar();
}
inline void Putchar(int ch){
	if (0<=ch && ch<=9) putchar(ch+48);
		else putchar(ch-10+97);
}
int P[N],P_[N],Q[N];
bool check(){
	Rep(i,1,*Dec) Q[i]=P[i]+Dec[i],P_[i]=P[i];
	Q[0]=0;
	Dep(i,*Dec,1) if (Q[i]>=16){
		Q[i-1]++;
		Q[i]-=16;
	}
	if (Q[0]!=0) return false;
	sort(P_+1,P_+*Dec+1);
	sort(Q+1,Q+*Dec+1);
	Rep(i,1,*Dec) if (P_[i]!=Q[i]) return false;
	return true;
}
void Dfs(int u){
	if (u>*Dec){
		if (check()){
			Rep(i,1,*Dec) Putchar(P[i]);puts("");
			exit(0);
		}
		return;
	}
	Rep(i,0,15){
		P[u]=i;Dfs(u+1);
	}
}
int main(){
	/*
	static int A[100],B[100];
	Getstring(A);
	Getstring(B);
	Rep(i,1,*A) A[i]=A[i]+B[i];
	Dep(i,*A,1) if (A[i]>15){
		A[i]-=16;A[i-1]++;
	}
	Rep(i,1,*A) Putchar(A[i]);puts("");return 0;
	*/
	freopen("hex.in","r",stdin);
	freopen("hex.out","w",stdout);
	Getstring(Dec);
	Dfs(1);
	puts("NO");
}
