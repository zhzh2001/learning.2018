#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
struct D{
	int l,r,mid;
	ll ans1,ans2;
}a[101000];
ll b[50100];
ll mo;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
ll gcd(ll x,ll y)
{
	if(y==0)return x;
	return gcd(y,x%y);
}
void build(int x,int y,int z)
{
	a[z].l=x;a[z].r=y;
	a[z].mid=(x+y)/2;
	if(x==y)
	{
		a[z].ans1=b[x];
		a[z].ans2=b[x];
		return;
	}
	build(x,a[z].mid,z*2);
	build(a[z].mid+1,y,z*2+1);
	a[z].ans1=max(a[z*2].ans1,a[z*2+1].ans1);
	a[z].ans2=a[z*2].ans2*a[z*2+1].ans2/gcd(a[z*2].ans2,a[z*2+1].ans2);
	a[z].ans2%=mo;
}
ll get(int x,int y,int z)
{
	if(x==a[z].l&&y==a[z].r)
		return a[z].ans1;
	if(y<=a[z].mid)return get(x,y,z*2);
	if(x>a[z].mid)return get(x,y,z*2+1);
	if(x<=a[z].mid&&y>a[z].mid)
	{
		return max(get(x,a[z].mid,z*2),get(a[z].mid+1,y,z*2+1));
	}
}
ll get1(int x,int y,int z)
{
	if(x==a[z].l&&y==a[z].r)
		return a[z].ans2;
	if(y<=a[z].mid)return get1(x,y,z*2);
	if(x>a[z].mid)return get1(x,y,z*2+1);
	if(x<=a[z].mid&&y>a[z].mid)
	{
		ll lzq=get1(x,a[z].mid,z*2);
		ll jzq=get1(a[z].mid+1,y,z*2+1);
		lzq/=gcd(lzq,jzq);
		return lzq*jzq%mo;
	}
}
ll solve(ll x,ll y)
{
	ll ans=y;x--;
	while(x>0)
	{
		if(x%2!=0)ans*=y,ans%=mo;
		y=(y*y)%mo;
		x/=2;
	}
	return ans;
}
int main()
{
	//freopen("math.in","r",stdin);
	//freopen("math.out","w",stdout);
	ll Q=read();
	ll n=0;
	mo=1e9+7;
	while(Q--)
	{
		ll fbbb=read();
		if(fbbb==1)
		{
			n++;
			b[n]=read();
		}
		else
		{
			ll ans=1;
			ll x=read();
			For(l,x,n)
				For(r,l,n)
				{
					ll ans1=0;
					ll ans2=1;
					For(i,l,r)
					{
						ans1=max(ans1,b[i]);
						ans2=ans2/gcd(ans2,b[i]);
						ans2*=b[i];
					}
					ans=ans*solve(ans1,ans2);
					cout<<ans1<<" "<<ans2<<endl;
					ans%=mo;
				}
			cout<<ans<<endl;
		}
	}
	return 0;
}

