#include<bits/stdc++.h>
using namespace std;
const int N = 20;
const int POS = 1048576;
const int BASE = 16;
char sa[N]; int lensa;
int p[N]; int lenp;
int T[N]; int lenT;
int num1[N],num2[N];

inline void addT(){
	--num1[T[1]];
	++num1[T[1]+1];
	T[1]++;
	for(int i = 1;i <= lenT;++i)
		if(T[i] == BASE){
			--num1[T[i]];
			++num1[0];
			--num1[T[i+1]];
			++num1[T[i+1]+1];
			++T[i+1];
			T[i] = 0;
		}
	if(T[lenT+1]) ++lenT;
}

inline void addp(){
	--num2[p[1]];
	++num2[p[1]+1];
	p[1]++;
	for(int i = 1;i <= lenp;++i)
		if(p[i] == BASE){
			--num2[p[i]];
			++num2[0];
			--num2[p[i+1]];
			++num2[p[i+1]+1];
			++p[i+1];
			p[i] = 0;
		}
}

inline bool pd(){
	for(int i = 1;i <= 15;++i)
		if(num1[i] != num2[i])
			return false;
	return true;
}

inline void Print(){
	num1[0] = 0;
	for(int i = 1;i <= lenT;++i)
		if(T[i] == 0) ++num1[0];
	num2[0] = 0;
	for(int i = 1;i <= lenp;++i)
		if(p[i] == 0) ++num2[0];
	num2[0] -= num1[0];
	for(int i = 1;i <= num2[0];++i)
		putchar('0');
	for(int i = lenT;i >= 1;--i){
		if(T[i] > 9) putchar(T[i]-10+'a');
		else putchar(T[i]+'0');
	}
	puts("");
}

int main(){
	freopen("hex.in","r",stdin);
	freopen("hex.out","w",stdout);
	scanf("%s",sa+1); int lensa = strlen(sa+1);
	for(int i = 1;i <= lensa;++i){
		char a = sa[lensa-i+1];
		if(a > '9' || a < '0'){ p[i] = a-'a'+10;}
		else p[i] = a-'0';
		++num2[p[i]];
	}
	lenp = lensa;
	while(lenp > 0 && p[lenp] == 0)
		--lenp;
	if(lenp == 0){
		puts("0");
		return 0;
	}
	lenT = 1;
	for(int i = 1;i <= POS;++i){
		if(pd()){
			Print();
			return 0;
		}
		addT();
		addp();
	}
	puts("NO");
	return 0;
}