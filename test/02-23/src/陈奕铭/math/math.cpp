#include<bits/stdc++.h>
using namespace std;
inline ll read(){
	ll x = 0;char ch = getchar();
	while(ch < '0' || ch > '9') ch = getchar();
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x;
}
#define mod 1000000007

ll pri[41605],cnt,newpri[35],cntn;
bool vis[500005];
vector<ll> divs[41605];
ll top,cntx; struct data{ ll x,id; }stk[50005],qis[35];
ll mx[800005],lim[800005],lalim[800005],lazylim[800005],lazymx[800005],lazymi[800005],lazycheng[800005];
ll size[800005];
ll n,num,ans;

inline bool cmp1(data a,data b){
	return a.id > b.id;
}

inline void getpri(){
	for(ll i = 2;i <= 500000;++i){
		if(!vis[i]) pri[++cnt] = i;
		for(ll j = 1;j <= cnt && pri[j]*i <= 50000;++0j){
			vis[pri[j]*i] = true;
			if(i%pri[j] == 0) break;
		}
	}	
}

inline ll quickmi(ll x,ll n){
	ll ans = 1;
	while(n){
		if(n&1) ans = ans*x%mod;
		x = x*x%mod;
	}
	return ans;
}

inline void newPri(ll x){
	cntn = 0;
	for(ll i = 1;i <= cnt && pri[i]*pri[i] <= x;++i){
		while(x%pri[i] == 0){
			newpri[++cntn] = pri[i];
			x /= pri[i];
		}
	}
	if(x != 1) newpri[++cntn] = x;
	cntx = 0;
	for(ll i = 1;i <= cntn;){
		int o = newpri[i];
		int p = divs[p].size()-1;
		while(newpri[i] == o && p >= 0){
			qis[++cntx] = (data){o,divs[o][p]};
			--p; ++i;
		}
		while(newpri[i] == o) ++i;
	}
	sort(qis+1,qis+cntx+1,cmp1);
}

inline ll nyuan(ll x){
	return quickmi(x,mod-2);
}

void build(int num,int l,int r){
	mx[num] = 0; lim[num] = 1;
	lazymx[num] = 0; lazylim[num] = 1;
	lalim[num] = 1; size[num] = r-l+1;
	lazymi[num] = 0; lazycheng[num] = 1;
	if(l == r) return;
	int mid = (l+r)>>1;
	build(num<<1,l,mid);
	build(num<<1|1,mid+1,r);
}

void cheng(ll num,ll l,ll r,ll x,ll y,ll v){
	if(x <= l && y <= r){
		lim[num] = lim[num]*quickmi(v,size[num])%mod;
		
	}
	pushdown(num);
}

void solved(ll x){
	while(top > 0 && stk[top].x <= x) --top;
	stk[++top] = (data){x,num+1};
	if(top > 1) coverMx(1,1,50000,stk[top-1].id+1,num+1,x);
	else coverMx(1,1,50000,1,num+1,x);
	cheng(1,1,50000,num+1,num+1,x);
	ll last = num;
	for(int i = 1;i <= cntx;++i){
		int o = qis[i].id;
		if(o < last){
			ans = ans*querylim(1,1,50000,o+1,last)%mod;
			ans = ans*quickmi(x,querymx(1,1,50000,o+1,last))%mod;
			cheng(1,1,50000,o+1,last,x);
			last = o;
		}
		x /= qis[i].x;
	}
	ans = ans*querylim(1,1,50000,1,last)%mod;
	ans = ans*quickmi(x,querymx(1,1,50000,1,last))%mod;
	cheng(1,1,50000,1,last,x);
}

signed main(){
	getpri();
	n = read();
	build(1,1,50000);
	for(ll i = 1;i <= n;++i){
		ll opt = read(),x = read();
		if(opt == 1){
			newPri(x);
			if(num == 0){
				ans += quickmi(x,x);
				coverMx(1,1,50000,1,1,x);
				cheng(1,1,50000,1,1,x);
				stk[++top] = (data){x,1};
				++num;
			}else{
				ans = quickmi(x,x)*ans%mod;
				solved(x);
			}
			pushPri();
		}else{
			ll opp = 1;
			if(x > 1) opp = nyuan(querylalim(1,1,50000,1,x-1));
			printf("%lld\n", ans*opp%mod);
		}
	}
	return 0;
}