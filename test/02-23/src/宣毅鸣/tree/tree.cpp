#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
typedef long long ll;
ll ans,n,num,m,f[200],ne[1000],fi[1000],zz[1000],x,y,p=0;
void jb(int x,int y)
{
	ne[++num]=fi[x];
	fi[x]=num;
	zz[num]=y;
}
const ll M=1e9+7;
void dfs(int x)
{
	ans++;
	for (int i=fi[x];i;i=ne[i])
	 if (!f[zz[i]])
	  {
	  	f[zz[i]]=1;
	  	dfs(zz[i]);
	  	f[zz[i]]=0;
	  }
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if (n<=18)
	 {
	 	while (m--)
	 	 {
	 	 	scanf("%d%d",&x,&y);
	 	 	jb(x,y);jb(y,x);
	 	 }
	 	for (int i=2;i<=n;i++)jb(i,i/2),jb(i/2,i);
	 	for (int i=1;i<=n;i++)f[i]=1,dfs(i),f[i]=0;
	 	printf("%lld\n",ans);
		return 0;	 	 
	 }
	printf("%lld",((m+1)*n%M*n%M-n*m%M+M)%M);
	return 0; 
}
