#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
typedef long long ll;
ll a[100005],b[100005],op,n,x,len,vvv1[100005],vvv2[100005];
const ll M=1e9+7;
ll ksm(ll x,ll y)
{
	if (!y)return 1;
	ll z=ksm(x,y/2);z*=z;z%=M;
	if (y&1)z*=x;
	return z%M;
}
ll gcd(ll x,ll y)
{
	if (!y)return x;
	return gcd(y,x%y);
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d",&n);int sum=0;
	for (int i=1;i<=n;i++)scanf("%d%d",&vvv1[i],&vvv2[i]),sum+=(vvv2[i]<=2||vvv1[i]==2);
	if (sum==n)
	 {
	 	for (int vvv=1;vvv<=n;vvv++)
	 	 {
	 	 	int op=vvv1[vvv],x=vvv2[vvv];
	 	 	if (op==1)a[++len]=x;
	 	 	else
	 	 	 {
	 	 	 	ll s1=(len-x+1)*(len-x+2)/2,s2=0;
	 	 	 	while (x<=len)
	 	 	 	 {
	 	 	 	 	while (a[x]==2&&x<=len)x++;
	 	 	 	 	if (x>len)break;
	 	 	 	 	int j=x;
	 	 	 		while (a[j]==1&&j<=len)j++;
	 	 	 		s2+=(j-x)*(j-x+1)/2;
	 	 	 		x=j;
	 	 	 	 }
	 	 	 	s1-=s2;
				printf("%lld\n",ksm(4,s1)); 
	 	 	 }
	 	 } 
	 	return 0; 
	 }
	for (int vvv=1;vvv<=n;vvv++)
	 {
	 	int op=vvv1[vvv],x=vvv2[vvv];
	 	if (op==1)a[++len]=x;
	 	else
	 	 {
	 	 	ll ans=1;
	 	 	for (int i=x;i<=len;i++)
	 	 	 {
	 	 	 	ll Max=0,Lcm=1,p=1;
	 	 	 	for (int j=1;j<=len;j++)b[j]=a[j];
	 	 	 	for (int j=i;j<=len;j++)
	 	 	 	 {
	 	 	 	 	Max=max(Max,a[j]);
	 	 	 	 	p*=b[j];p%=M;
	 	 	 	 	(ans*=ksm(p,Max))%=M;
	 	 	 	 	if (b[j]!=1)
					 for (int k=j+1;k<=len;k++)b[k]/=gcd(b[k],b[j]);
	 	 	 	 }
	 	 	 }
	 	 	printf("%lld\n",ans); 
	 	 }
	 }
	return 0; 
}
