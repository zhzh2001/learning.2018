#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define ls (i<<1)
#define rs (i<<1|1)
#define md ((l+r)>>1)
using namespace std;
const int W=500100,N=50010,M=N*4,P=1e9+7;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline int gcd(int a,int b){return b?gcd(b,a%b):a;}
inline int ksm(int x,int y){
	int z=1;for(;y;y>>=1,x=1ll*x*x%P)
	if(y&1)z=1ll*z*x%P;return z;
}
int n,m,a[N];
int tpm,qm[N];
int tpg,qg[N],gd[N];

int fk[M];
int ans[M],res[M];
int cv[M],sm[M];
int ad[M],ml[M];
void up(int i){
	ans[i]=1ll*ans[ls]*ans[rs]%P;
	res[i]=1ll*res[ls]*res[rs]%P;
	sm[i]=(sm[ls]+sm[rs])%(P-1);
	ml[i]=1ll*ml[ls]*ml[rs]%P;
}
void down(int i,int l,int r);
int get(int i,int l,int r,int x,int y){
	if(l>=x&&r<=y)return ans[i];
	down(i,l,r);int ss=1;
	if(x<=md)ss=1ll*ss*get(ls,l,md,x,y)%P;
	if(y>md)ss=1ll*ss*get(rs,md+1,r,x,y)%P;
	return ss;
}
void fuckm(int i,int l,int r,int x,int y,int k){
	if(l>=x&&r<=y){
		cv[i]=k;
		sm[i]=1ll*k*(r-l+1)%(P-1);
		res[i]=ksm(ml[i],k);
		return;
	}down(i,l,r);
	if(x<=md)fuckm(ls,l,md,x,y,k);
	if(y>md)fuckm(rs,md+1,r,x,y,k);
	if(r<=m)up(i);
}
void fuckg(int i,int l,int r,int x,int y,int k){
	if(l>=x&&r<=y){
		ad[i]=1ll*ad[i]*k%P;
		ml[i]=1ll*ml[i]*ksm(k,(r-l+1))%P;
		res[i]=1ll*res[i]*ksm(k,sm[i])%P;
		return;
	}down(i,l,r);
	if(x<=md)fuckg(ls,l,md,x,y,k);
	if(y>md)fuckg(rs,md+1,r,x,y,k);
	if(r<=m)up(i);
}
void add(int i,int l,int r,int x,int k){
	if(l==r){
		ans[i]=1;
		ml[i]=k;
		sm[i]=k;
		res[i]=ksm(k,k);
		return;
	}
	down(i,l,r);
	if(x<=md)add(ls,l,md,x,k);
	else add(rs,md+1,r,x,k);
	if(r<=m)up(i);
}
void chan(int i,int l,int r,int x,int y){
	if(l>=x&&r<=y){
		fk[i]=1;
		ans[i]=1ll*ans[i]*res[i]%P;
		return;
	}
	if(x<=md)chan(ls,l,md,x,y);
	if(y>md)chan(rs,md+1,r,x,y);
}
void down(int i,int l,int r){
	if(l==r)return;
	if(fk[i]){
		chan(ls,l,md,1,n);
		chan(rs,md+1,r,1,n);
		fk[i]=0;
	}
	if(cv[i]){
		fuckm(ls,l,md,1,n,cv[i]);
		fuckm(rs,md+1,r,1,n,cv[i]);
		cv[i]=0;
	}
	if(ad[i]>1){
		fuckg(ls,l,md,1,n,ad[i]);
		fuckg(rs,md+1,r,1,n,ad[i]);
		ad[i]=1;
	}
}
void pants(int i,int l,int r){
	if(l==r)return;
	down(i,l,r);
	pants(ls,l,md);pants(rs,md+1,r);
}

int tot,pri[W],vis[W],mn[W];
void pre(int n){
	int i,j,k;
	for(i=2;i<=n;i++){
		if(!vis[i]){
			pri[++tot]=i;
			mn[i]=tot;
		}
		for(j=1;j<=tot;j++){
			if(1ll*i*pri[j]>n)break;
			k=i*pri[j];
			vis[k]=1;mn[k]=j;
			if(i%pri[j]==0)break;
		}
	}
}
int h[N][21];
int main(){
	freopen("math.in","r",stdin);
	freopen("math1.out","w",stdout);
	register int i,j,k,o,x,ss,p;
	pre(500000);
	n=read();
	for(i=1;i<=n*4;i++)ad[i]=1;
	for(i=1;i<=n;i++){
		o=read();x=read();
		if(o==2){
			printf("%d\n",get(1,1,n,x,m));
			continue;
		}
		a[m+1]=x;
		
		while(tpm&&a[qm[tpm]]<=x)tpm--;
		qm[++tpm]=m+1;
		if(qm[tpm-1]+1<=m)
		fuckm(1,1,n,qm[tpm-1]+1,m,x);
		
		for(k=x;k>1;){
			j=mn[k];
			ss=0;
			while(k%pri[j]==0)k/=pri[j],ss++;
			for(p=1;p<=ss;p++){
				if(h[j][p]+1<=m-1){
					fuckg(1,1,n,h[j][p]+1,m-1,pri[j]);
				}h[j][p]=i;
			}
		}
		/*for(j=1;j<=tpg;j++){
			k=gd[j];
			gd[j]=gcd(gd[j],x);
			k=k/gd[j];
			fuckg(1,1,n,qg[j-1]+1,qg[j],k);
		}
		k=tpg;gd[k+1]=-1;
		tpg=0;
		for(j=1;j<=k;j++)
		if(gd[j]!=gd[j+1]){
			tpg++;
			qg[tpg]=qg[j];
			gd[tpg]=gd[j];
		}
		tpg++;
		qg[tpg]=m+1;
		gd[tpg]=x;
		if(1<=m)
		fuckg(1,1,n,1,m,x);*/
		
		m++;add(1,1,n,m,x);
		pants(1,1,n);
		chan(1,1,n,1,m);
		//fuckg(1,1,n,m,m,ksm(x,P-2));
		pants(1,1,n);
	}return 0;
}
