#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=50010,P=1e9+7;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
char o[N];
int n,a[N],ans[N],t[N],s[N],h[N];
bool dfs(int i,int hhh,int jj){
	if((n-i+1)*2<hhh)
	return 0;
	if(i>n)return 1;
	for(int hh,y,x=0;x<=15;x++){
		t[i]=x;
		s[i]=x-a[i];
		if(jj)s[i]+=16;
		if(i<n){
			s[i]--;
			y=s[i];
			if(y<0)goto out;
			if(y>15)goto out;
			hh=hhh;
			if(h[x]<0)hh--;
			else hh++;
			h[x]++;
			if(h[y]>0)hh--;
			else hh++;
			h[y]--;
			if(dfs(i+1,hh,1))return 1;
			h[x]--;h[y]++;
			out:s[i]++;
		}
		y=s[i];
		if(y<0)continue;
		if(y>15)continue;
		hh=hhh;
		if(h[x]<0)hh--;
		else hh++;
		h[x]++;
		if(h[y]>0)hh--;
		else hh++;
		h[y]--;
		if(dfs(i+1,hh,0))return 1;
		h[x]--;h[y]++;
	}t[i]=s[i]=0;return 0;
}
int main(){
	freopen("hex.in","r",stdin);
	freopen("hex.out","w",stdout);
	int i;
	scanf("%s",o+1);
	n=strlen(o+1);
	for(i=1;i<=n;i++){
		if(o[i]>='0'&&o[i]<='9')
		a[i]=o[i]-'0';
		else a[i]=10+o[i]-'a';
	}
	ans[1]=-1;
	if(dfs(1,0,0)){
		for(i=1;i<=n;i++)ans[i]=t[i];
	}
	for(i=n;i>1;i--){
		a[i]=-a[i];
		if(a[i]<0)a[i]+=16,a[i-1]++;
	}
	a[1]=-a[1];
	memset(h,0,sizeof(h));
	if(dfs(1,0,0)){
		if(ans[1]!=-1){
			for(i=1;i<=n;i++)
			if(ans[i]<t[i])goto out;
			else if(ans[i]>t[i])goto can;
		}
		can:for(i=1;i<=n;i++)ans[i]=t[i];
	}
	out:;
	if(ans[1]==-1)puts("NO");
	else{
		for(i=1;i<=n;i++)
		if(ans[i]<10)putchar(ans[i]+'0');
		else putchar(ans[i]-10+'a');
	}
	return 0;
}
