#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=1000100,P=1e9+7;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,ans;
int X[N],Y[N];
int calsz(int x){
	int res=0,mn=x,mx=x;
	while(mx<=n){
		res+=mx-mn+1;
		mx=mx*2+1;
		mn=mn*2;
	}if(n>=mn)res+=n-mn+1;
	return res;
}
int callv(int x){
	int res=0;
	while(x)x>>=1,res++;
	return res;
}
void work1(int x,int y){
	int sx0,sx1,sy0,sy1,sm,k;
	sx0=sx1=sy0=sy1=sm=0;
	while(x!=y){
		if(callv(x)<callv(y)){
			swap(x,y);
			swap(sx0,sy0);
			swap(sx1,sy1);
		}
		k=1;
		if(!sx0)k+=calsz(x*2);
		if(!sx1)k+=calsz(x*2+1);
		(ans+=1ll*k*k%P)%=P;
		(ans+=4ll*k*sm%P)%=P;
		sm+=k;
		sx0=sx1=0;
		if(x&1)sx1=1;
		else sx0=1;
		x=x/2;
	}
	/*k=1;
	if(!sx0&&!sy0)k+=calsz(x*2);
	if(!sx1&&!sy1)k+=calsz(x*2+1);*/
	k=n-sm;
	(ans+=1ll*k*k%P)%=P;
	(ans+=4ll*k*sm%P)%=P;
	sm+=k;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int i;
	n=read();m=read();
	for(i=1;i<=m;i++){
		X[i]=read();
		Y[i]=read();
	}
	if(m==0){
		ans=1ll*n*n%P;
		printf("%d\n",ans);
		return 0;
	}
	if(m==1){
		work1(X[1],Y[1]);
		printf("%d\n",ans);
		return 0;
	}
	
	return 0;
}
