#include<bits/stdc++.h>
#define ll long long
#define N 10005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int op2[N*2],x2[N*2],cnt,lmax,top,n,m,q,op,x,z[N],num[N],yk[N][50],g[N][50],p[N][N],vis[500005],tot,ff[500005];
ll ans,lcm;
ll ksm(ll x,int k){
	int num=x,sum=1;
	while (k){
		if (k&1) sum=(ll)sum*num%mod;
		num=(ll)num*num%mod;
		k=k>>1;
	}
	return sum;
}
int main(){
	freopen("math.in","r",stdin);freopen("math.out","w",stdout);
	q=read();
	for (int i=1;i<=q;i++){
		op2[i]=read();x2[i]=read();
		if (op2[i]==2) cnt++;
	}
	for (int i=1;i<=q;i++){
		op=op2[i];x=x2[i];
		if (op==1){
			z[++tot]=x;
			for (int j=2;j<=sqrt(z[tot]);j++){
				if (x%j==0){
					num[tot]++;yk[tot][num[tot]]=j;
					while (x%j==0&&x){
						x=x/j;g[tot][num[tot]]++;
					}
				}
			}
			if (x>1) yk[tot][++num[tot]]=x,g[tot][num[tot]]=1;
			top=0;lcm=1;lmax=0;
			p[tot+1][tot]=1;
			for (int j=tot;j;j--){
				for (int k=1;k<=num[j];k++){
					if (!vis[yk[j][k]]) ff[++top]=yk[j][k];
					if (g[j][k]>vis[yk[j][k]])
					lcm=(ll)lcm*ksm(yk[j][k],g[j][k]-vis[yk[j][k]])%mod,vis[yk[j][k]]=g[j][k];
				}
				lmax=max(lmax,z[j]);
				p[j][tot]=(ll)ksm(lcm,lmax)*(ll)p[j+1][tot]%mod;
			}
			for (int j=1;j<=top;j++) vis[ff[j]]=0;
		}
		else{
			ans=1;
			for (int j=x;j<=tot;j++){
				ans=(ll)(ans*p[x][j])%mod;
			}
			wrn(ans);
		}
	}
	return 0;
}
