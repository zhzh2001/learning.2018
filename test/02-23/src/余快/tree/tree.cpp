#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define int ll
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int a,b,top1,top2,z1[N],z2[N],num,yk,vis[N],n,m,num1,num2,x,y;
ll ans;
int nedge,Next[1000],head[1000],to[1000];
void add(int a,int b){
	Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;
}
int find(int l,int r){
	if (r>n) return max((ll)0,n-l+1);
	return (r-l+1)+find(l*2,r*2+1);
}
int dfs(int x){
	vis[x]=1;int sum=1;
	for (int i=head[x];i;i=Next[i]){
		if (!vis[to[i]]){
			sum+=dfs(to[i]);
		}
	}
	vis[x]=0;
	return sum;
}
signed main(){
	freopen("tree.in","r",stdin);freopen("tree.out","w",stdout);
	n=read();m=read();
	if (m==0){
		wrn(n*n%mod);
		return 0;
	}
	if (m==1){
		x=read();y=read();
		if (x>y) swap(x,y);ans=(ll)n*n%mod;
		/*ll num1=find(x,x),num2=find(y,y);
		ans=ans+(ll)num1*(ll)(n-num1)%mod;
		ans=ans%mod;
		ans=ans+(ll)num2*(ll)(n-num2)%mod;*/
		for (int i=x;i;i=i>>1) z1[++top1]=i;
		for (int i=y;i;i=i>>1) z2[++top2]=i;
		for (a=top1,b=top2;z1[a]==z2[b]&&a&&b;a--,b--);
		a++;b++;num=0;
		for (int i=1;i<a;i++){
			yk=find(z1[i],z1[i]);
			ans=(ans+(yk-num)*(n-yk+num)%mod)%mod;
			num=yk;
		}
		int a1=num;
		num=0;
		for (int i=1;i<b;i++){
			yk=find(z2[i],z2[i]);
			ans=(ans+(yk-num)*(n-yk+num)%mod)%mod;
			num=yk;
		}
		int b1=num;
		ans=(ans+a1+b1)%mod;
		int p=find(z1[a],z1[a]);
		ans=(ans+(n-p)*(a1+b1)%mod)%mod;
		wrn(ans);
		return 0;
	}
	if (n<=18){
		for (int i=1;i<=m;i++){
			x=read();y=read();add(x,y);add(y,x);
		}
		for (int i=2;i<=n;i++){
			add(i/2,i);add(i,i/2);
		}
		for (int i=1;i<=n;i++){
			ans+=dfs(i);
		}
		wrn(ans);
	}
	return 0;
}
