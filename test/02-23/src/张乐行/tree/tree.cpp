#include<cstdio>
#include<algorithm>
#include<map>
#define ll long long
using namespace std;
const int N=405,mo=1e9+7;
int n,m,nd,i,j,p[N],ans;
map<int,bool> in;
struct arr{int x,y;}a[10];
void climb(int x){
	for (;x&&in.find(x)==in.end();x>>=1)
		in[x]=1,p[++nd]=x;
}
int find(int x){
	int l=1,r=nd,m;
	while (l<r){m=l+r>>1;
		if (x<=p[m]) r=m; else l=m+1;
	}
	return l;
}
int et,he[N],sv[N],val[N];
struct edge{int l,to;}e[N*2];
void add(int x,int y){
	e[++et].l=he[x];he[x]=et;e[et].to=y;
	e[++et].l=he[y];he[y]=et;e[et].to=x;
}
int cal(int x){
	int l=x,r=x,res=0;
	for (;r<=n;l=l<<1,r=r<<1|1) res+=r-l+1;
	if (l<=n) res+=n-l+1;
	return res;
}
void dfs(int x){
	sv[x]=val[x]=cal(p[x]);
	for (int i=he[x];i;i=e[i].l){
		int y=e[i].to;if (y<x) continue;
		dfs(y);val[x]-=sv[y];
	}
}
int g[N];bool vis[N];
void solve(int x){
	vis[x]=1;g[x]++;
	for (int i=he[x];i;i=e[i].l)
		if (!vis[e[i].to]) solve(e[i].to);
	vis[x]=0;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=m;i++){
		int x,y;scanf("%d%d",&x,&y);
		a[i]=(arr){x,y};climb(x);climb(y);
	}
	if (nd==0) p[++nd]=1;sort(p+1,p+nd+1);
	for (i=2;i<=nd;i++) add(i,find(p[i]>>1));
	dfs(1);
	for (i=1;i<=m;i++)
		add(find(a[i].x),find(a[i].y));
	for (i=1;i<=nd;i++){
		for (j=1;j<=nd;j++) g[j]=0;
		solve(i);
		for (j=1;j<=nd;j++)
			ans=(ans+(ll)val[i]*val[j]%mo*g[j])%mo;
	}
	printf("%d",ans);
}
