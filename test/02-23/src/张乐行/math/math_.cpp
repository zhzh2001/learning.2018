#include<cstdio>
#include<vector>
#include<algorithm>
#define ls p<<1,l,m
#define rs p<<1|1,m+1,r
#define ll long long
using namespace std;
const int mo=1e9+7,pmo=mo-1;
const int N=1e5+5,P=2e5+5,M=5e5+5,PRIME=1e5+5;
int Q,n,m,now,i,a[N],x,y,k;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
struct que{int opt,x;}rq[N];
int t,pr[PRIME],pmn[N];
void init(int n){
	for (int i=2;i<=n;i++){
		if (!pmn[i]) pr[++t]=i,pmn[i]=t;
		for (int j=1;i*pr[j]<=n;j++){
			pmn[i*pr[j]]=j;
			if (i%pr[j]==0) break;
		}
	}
}
int Pow(int x,int n){
	int k=1;
	for (;n;n>>=1,x=(ll)x*x%mo)
		if (n&1) k=(ll)k*x%mo;
	return k;
}
struct Segmnet_Tree{
	int len[P],lcm_mul[P],lcm_smul[P];
	int mx_add[P],mx_sum[P],f_smul[P];
	void mul(int p,int k){
		lcm_mul[p]=(ll)lcm_mul[p]*k%mo;
		lcm_smul[p]=(ll)lcm_smul[p]*Pow(k,len[p])%mo;
		f_smul[p]=(ll)f_smul[p]*Pow(k,mx_sum[p])%mo;
	}
	void add(int p,int k){
		mx_add[p]+=k;
		mx_sum[p]=(mx_sum[p]+(ll)k*len[p])%pmo;
		f_smul[p]=(ll)f_smul[p]*Pow(lcm_smul[p],k)%mo;
	}
	void up(int p){
		int ld=p<<1,rd=p<<1|1;
		lcm_smul[p]=(ll)lcm_smul[ld]*lcm_smul[rd]%mo;
		mx_sum[p]=(mx_sum[ld]+mx_sum[rd])%pmo;
		f_smul[p]=(ll)f_smul[ld]*f_smul[rd]%mo;
	}
	void down(int p){
		if (lcm_mul[p]!=1){
			mul(p<<1,lcm_mul[p]);
			mul(p<<1|1,lcm_mul[p]);
			lcm_mul[p]=1;
		}
		if (mx_add[p]){
			add(p<<1,mx_add[p]);
			add(p<<1|1,mx_add[p]);
			mx_add[p]=0;
		}
	}
	void mul_lcm(int p,int l,int r){
		if (x<=l&&r<=y){mul(p,k);return;}
		int m=l+r>>1;down(p);
		if (x<=m) mul_lcm(ls);
		if (y>m) mul_lcm(rs);
		up(p);
	}
	void add_max(int p,int l,int r){
		if (x<=l&&r<=y){add(p,k);return;}
		int m=l+r>>1;down(p);
		if (x<=m) add_max(ls);
		if (y>m) add_max(rs);
		up(p);
	}
	int ask(int x){
		int p=1,l=1,r=n,res=1;
		for (;l<r;){
			int m=l+r>>1;down(p);
			if (x<=m){
				res=(ll)res*f_smul[p<<1|1]%mo;
				r=m;p=p<<1;
			}
			else l=m+1,p=p<<1|1;
		}
		return (ll)res*f_smul[p]%mo;
	}
	void build(int p,int l,int r){
		len[p]=r-l+1;
		lcm_mul[p]=lcm_smul[p]=f_smul[p]=1;
		if (l==r) return;int m=l+r>>1;
		build(ls);build(rs);
	}
	
}ST;
void Mul_lcm_it(int l,int r,int v){
	x=l;y=r;k=v;ST.mul_lcm(1,1,n);
}
void Add_max_it(int l,int r,int v){
	if (!v) return;
	x=l;y=r;k=v;ST.add_max(1,1,n);
}

int fn,f_n,q_n;
struct lcm_que{int x,k;}fm[51],f_m[51],q_m[51];
void merge_lcm(){
	q_n=0;int i,j;
	for (i=j=1;i<=fn||j<=f_n;){
		int px=fm[i].x,py=f_m[j].x;
		if (i<=fn&&j<=f_n&&px==py)
			q_m[++q_n]=(lcm_que){px,fm[i].k*f_m[j].k},i++,j++;
		else if (j>f_n||i<=fn&&px<py)
				q_m[++q_n]=(lcm_que){px,fm[i].k},i++;
			 else q_m[++q_n]=(lcm_que){py,f_m[j].k},j++;
	}
	for (fn=q_n,i=1;i<=fn;i++)	fm[i]=q_m[i];
}
struct Prime_Queue{
	int t,d[21],v[21];
	void push_it(int x,int k){
		for (f_n=0;t&&v[t]<k;t--)
			f_m[++f_n]=(lcm_que){d[t],k/v[t]};
		if (t) f_m[++f_n]=(lcm_que){d[t],1};
		merge_lcm();
		if (t&&v[t]==k) t--;
		t++;d[t]=x;v[t]=k;
	}
}q[PRIME];
int t_mx,q_mx[N];
void ins_it(int k){
	int i,nk=k;
	a[++now]=k;fm[fn=1]=(lcm_que){now,k};
	for (;k>1;){
		int x=pmn[k],p=pr[x],v=p;
		for (;k%v==0;v*=p);v/=p;k/=v;
		q[x].push_it(now,v);
	}
	fm[fn+1].x=0;k=nk;
	for (i=1;i<=fn;i++)
		Mul_lcm_it(fm[i+1].x+1,fm[i].x,fm[i].k);
	for (;t_mx&&k>=a[q_mx[t_mx]];t_mx--)
		Add_max_it(q_mx[t_mx-1]+1,q_mx[t_mx],k-a[q_mx[t_mx]]);
	Add_max_it(now,now,k);
	q_mx[++t_mx]=now;
}
void query_it(int x){
	write(ST.ask(x));putchar('\n');
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	for (Q=read(),i=1;i<=Q;i++){
		rq[i]=(que){read(),read()};
		if (rq[i].opt==1) n++,m=max(m,rq[i].x);
	}
	init(m);ST.build(1,1,n);
	for (i=1;i<=Q;i++)
		if (rq[i].opt==1) ins_it(rq[i].x);
		else query_it(rq[i].x);
}
