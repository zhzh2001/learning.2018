#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
const int N=5e4+5,mo=1e9+7;
int Q,n,i,a[N],lm[N],mx[N],ans[N];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int gcd(int a,int b){return b?gcd(b,a%b):a;}
int lcm(int a,int b){return a/gcd(a,b)*b;}
int Pow(int x,int n){
	int k=1;
	for (;n;n>>=1,x=(ll)x*x%mo)
		if (n&1) k=(ll)k*x%mo;
	return k;
}
void ins_it(){
	int x=read();int k=1;
	for (i=n;i;i--){
		k=lcm(k,gcd(a[i],x));
		lm[i]=(ll)lm[i]*(x/k)%mo;
	}
	a[++n]=x;lm[n]=x;int v=1;
	for (i=n;i;i--){
		mx[i]=max(mx[i+1],a[i]);
		v=(ll)v*Pow(lm[i],mx[i])%mo;
		ans[i]=(ll)ans[i]*v%mo;
	}
}
void query_it(){
	write(ans[read()]);putchar('\n');
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	for (Q=read(),i=1;i<=Q;i++) ans[i]=1;
	for (;Q--;)
		if (read()==1) ins_it();
		else query_it();
}
