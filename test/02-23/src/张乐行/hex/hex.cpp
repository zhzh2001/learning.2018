#include<cstdio>
#include<cstring>
const int N=20;
int n,m,nm,i,k,a[N],d[N],g[16];
char s[N];
int main(){
	freopen("hex.in","r",stdin);
	freopen("hex.out","w",stdout);
	scanf("%s",s);n=strlen(s);
	for (i=0;i<n;i++)
		a[n-i]=s[i]<='9'?s[i]-48:s[i]-87;
	for (i=n;i;i--) m=m<<4|a[i];
	nm=1<<4*n;
	for (int p=m;p<nm;p++){
		for (i=0;i<16;i++) g[i]=0;
		for (i=1,k=p;i<=n;i++,k>>=4) g[d[i]=k&15]++;
		for (i=1;i<=n;i++){
			d[i]-=a[i];
			if (d[i]<0) d[i]+=16,d[i+1]--;
			g[d[i]]--;if (g[d[i]]<0) break; 
		}
		if (i>n){
			for (i=n;i;i--)
				putchar(d[i]<=9?d[i]+48:d[i]+87);
			return 0;
		}
	}
	puts("NO");
}
