#include<bits/stdc++.h>
#define ll long long
#define N 50005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll q,len,a[N];
ll qpow(ll x,ll y){
	ll num=1;
	for (;y;y>>=1,x=x*x%mod) if (y&1) num=num*x%mod;
	return num;
}
ll gcd(ll x,ll y) { return y?gcd(y,x%y):x; }
ll lcm(ll x,ll y) { return x*y/gcd(x,y); }
void work1(){
	while (q--){
		ll opt=read();
		if (opt==1) a[++len]=read();
		else{
			ll x=read(),ans=1;
			rep(i,x,len){
				ll maxn=a[i],tmp=a[i];
				rep(j,i,len){
					maxn=max(a[j],maxn);
					tmp=lcm(tmp,a[j]);
					ans=ans*qpow(tmp,maxn)%mod;
				}
			}
			printf("%lld\n",ans);
		}
	}
}

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	q=read();
	if (q<=200) { work1(); return 0; }
	else return 233;
}
