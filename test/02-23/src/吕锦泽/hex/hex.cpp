#include<bits/stdc++.h>
#define ll int
#define N 105
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll a[N],b[N],t[N],cnt[N],ans[N],flag,len; char s[N];
void judge(){
	memset(cnt,0,sizeof cnt);
	rep(i,1,len) b[i]=a[i];
	rep(i,1,len) b[i]-=t[i];
	per(i,len,2) if (b[i]<0) --b[i-1],b[i]+=16;
	if (b[0]<0) return;
	rep(i,1,len) --cnt[b[i]];
	rep(i,1,len) ++cnt[a[i]];
	rep(i,0,15) if (cnt[i]!=0) return;
	flag=1;
	rep(i,1,len) if (b[i]>ans[i]) return;
	rep(i,1,len) ans[i]=b[i];
}
void dfs(ll now){
	if (now>len){
		judge();
		return;
	}
	rep(i,0,15){
		a[now]=i;
		dfs(now+1);
	}
}
int main(){
	freopen("hex.in","r",stdin);
	freopen("hex.out","w",stdout);
	scanf("%s",s+1);
	len=strlen(s+1);
	rep(i,1,len){
		if (s[i]>='0'&&s[i]<='9') t[i]=s[i]-'0';
		else t[i]=s[i]-'a'+10;
	}
	rep(i,1,len) ans[i]=200;
	dfs(1);
	if (!flag) puts("NO");
	else rep(i,1,len){
		if (ans[i]<10) printf("%d",ans[i]);
		else putchar('a'+ans[i]-10);
	}
}
