#include<bits/stdc++.h>
#define ll long long
#define N 10005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
map<ll,ll>mp;
ll n,m,a[N],b[N],cnt1,cnt2;
ll qpow(ll x,ll y){
	ll num=1;
	for (;y;y>>=1,x=x*x%mod) if (y&1) num=num*x%mod;
	return num;
}
ll get(ll x){
	ll now=1,l=x*2,r=l+1;
	while (r<=n){
		++now;
		r=r*2+1;
		l=l*2;
	}
	return (qpow(2,now)+max(0ll,n-l+1))%mod;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(); m=read();
	if (m==0){
		printf("%lld",n*n%mod);
	}else
	if (m==1){
		ll x=read(),y=read();
		ll tx=x,ty=y;
		while (tx){
			a[++cnt1]=tx;
			tx/=2;
		}
		while (ty){
			b[++cnt2]=ty;
			ty/=2;
		}
		sort(a+1,a+1+cnt1);
		sort(b+1,b+1+cnt2);
		ll ed=1,ans=n*2%mod*n%mod;
		ans=ans-n; ans%=mod;
		rep(i,1,cnt1) rep(j,1,cnt2) if (a[i]==b[j]) ed=max(ed,a[i]);
		rep(i,1,cnt1) if (a[i]>=ed) mp[a[i]]=1; rep(i,1,cnt2) if (b[i]>=ed) mp[b[i]]=1;
		tx=x; ty=y;
		while (tx!=ed/2){
			if (tx*2<=n&&!mp[tx*2]){
				ll tmp=get(tx*2);
				ans=ans-tmp*tmp%mod+tmp;
				ans%=mod;
			}
			if (tx*2+1<=n&&!mp[tx*2+1]){
				ll tmp=get(tx*2+1);
				ans=ans-tmp*tmp%mod+tmp;
				ans%=mod;
			}
			tx/=2;
		}
		while (ty!=ed/2){
			if (ty*2<=n&&!mp[ty*2]){
				ll tmp=get(ty*2);
				ans=ans-tmp*tmp%mod+tmp;
				ans%=mod;
			}
			if (ty*2+1<=n&&!mp[ty*2+1]){
				ll tmp=get(ty*2+1);
				ans=ans-tmp*tmp%mod+tmp;
				ans%=mod;
			}
			ty/=2;
		}
		ans=(ans-(n-get(ed)+2)*(n-get(ed)+2)%mod+n-get(ed)+2)%mod;
		printf("%lld",(ans+mod)%mod);
	}else return 233;
}
