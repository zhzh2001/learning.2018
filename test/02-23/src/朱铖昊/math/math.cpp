#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int mod=1e9+7,N=10005;
int a[N],f[N],n,Q,m,opt,x,sum;
pair<int,int> q[N],g[N];
inline bool check()
{
	for (int i=1;i<=n;++i)
		if (a[i]>2)
			return false;
	return true;
}
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
inline void bff5()
{
	int l=0,r=0;
	for (int i=1;i<=n;++i)
		if (a[i]==1)
		{
			if (!l)
			{
				l=i;
				r=i;
			}
			else
				++r;
		}
		else
		{
			if (l)
			{
				f[r]+=(r-l+1)*(r-l+2)/2;
				for (int j=l;j<=r;++j)
					g[j]=make_pair(l,r);
				l=0;
			}
		}
	f[0]=0;
	for (int i=1;i<=n;++i)
		f[i]+=f[i-1];
	for (int i=1;i<=m;++i)
	{
		l=q[i].first;
		r=q[i].second;
		int len=r-l+1;
		sum=len*(len+1)/2;
		int yi=f[r]-f[l-1];
		if (a[l]==1)
		{
			int L=g[l].first,R=g[l].second;
			yi-=(R-L+1)*(R-L+2)/2;
			yi+=(R-l+1)*(R-l+2)/2;
		}
		if (a[r]==1)
		{
			int L=g[r].first,R=g[r].second;
			if (r!=R)
				yi+=(r-L+1)*(r-L+2)/2;
		}
		cout<<ksm(2,(sum-yi)*2)<<'\n';
	}
}
int zs[N],tot;
inline void get_prime()
{
	for (int i=2;i<=1000;++i)
	{
		if (!f[i])
			zs[++tot]=i;
		for (int j=1;j<=tot&&zs[j]*i<=1000;++j)
		{
			f[zs[j]*i]=1;
			if (i%zs[j]==0)
				break;
		}
	}
}
const int M=170,NN=1005;
set<int> S;
int mx[NN][NN],lcm[NN][NN],ans[NN][NN],tong[M],zs_tong[NN][M];
inline void fj(int x)
{
	int k=a[x];
	for (int i=1;i<=tot;++i)
	{
		while (k%zs[i]==0)
		{
			k/=zs[i];
			++zs_tong[x][i];
		}
	}
	if (k!=1)
		zs_tong[x][0]=k;
}
inline void bl()
{
	get_prime();
	//cout<<tot<<'\n';
	for (int i=1;i<=n;++i)
		fj(i);
	for (int i=1;i<=n;++i)
	{
		mx[i][i]=a[i];
		ans[i][i]=ksm(a[i],a[i]);
		lcm[i][i]=a[i];
		memset(tong,0,sizeof(tong));
		for (int j=1;j<=tot;++j)
			tong[j]=zs_tong[i][j];
		S.clear();
		if (zs_tong[i][0])
			S.insert(zs_tong[i][0]);
		for (int j=i-1;j>=1;--j)
		{
			mx[j][i]=max(a[j],mx[j+1][i]);
			lcm[j][i]=lcm[j+1][i];
			for (int k=1;k<=tot;++k)
				if (zs_tong[j][k]>tong[k])
				{
					lcm[j][i]=(ll)lcm[j][i]*ksm(zs[k],zs_tong[j][k]-tong[k])%mod;
					tong[k]=zs_tong[j][k];
				}
			if (zs_tong[j][0])
			{
				if (!S.count(zs_tong[j][0]))
				{
					lcm[j][i]=(ll)lcm[j][i]*zs_tong[j][0]%mod;
					S.insert(zs_tong[j][0]);
				}
			}
			ans[j][i]=(ll)ans[j+1][i]*ksm(lcm[j][i],mx[j][i])%mod;
		}
	}
	for (int i=1;i<=m;++i)
	{
		sum=1;
		for (int j=q[i].first;j<=q[i].second;++j)
			sum=(ll)sum*ans[q[i].first][j]%mod;
		cout<<sum<<'\n';
	}
}
// inline void bl2()
// {
// 	int l=q[1].first,r=q[1].second,sum=1;
// 	for (int i=l;i<=r;++i)
// 		fj(i);
// 	for (int i=l;i<=r;++i)
// 	{
// 		int lcm=a[i];

// 		sum=(ll)sum*
// 	}
// }
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	read(Q);
	for (int i=1;i<=Q;++i)
	{
		read(opt);
		read(x);
		if (opt==1)
			a[++n]=x;
		else
			q[++m]=make_pair(x,n);
	}
	if (check())
		bff5();
	else
	{
		//if (m!=1)
			bl();
		//else
			//bl2();
	}
	return 0;
}