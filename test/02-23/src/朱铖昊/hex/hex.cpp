#include<bits/stdc++.h>
using namespace std;
const int N=20;
int a[N],b[N],g[N],f[N],det[N],n,sum;
char s[N];
inline void pd()
{
	int tag=0;
	memset(g,0,sizeof(g));
	for (int i=n;i;--i)
	{
		b[i]=det[i]+a[i]+tag;
		if (b[i]>=16)
		{
			b[i]-=16;
			tag=1;
		}
		else
			tag=0;
	}
	if (tag==1)
	{
		puts("No");
		exit(0);
	}
	for (int i=1;i<=n;++i)
		++g[b[i]];
	for (int i=0;i<=15;++i)
		if (f[i]!=g[i])
			return;
	for (int i=1;i<=n;++i)
		putchar(a[i]>=0&&a[i]<=9?a[i]+'0':a[i]-10+'a');
	exit(0);
}
inline void dfs(int x)
{
	if (x==n+1)
	{
		pd();
		return;
	}
	for (int i=0;i<16;++i)
	{
		a[x]=i;
		++f[i];
		dfs(x+1);
		--f[i];
	}
}
int main()
{
	freopen("hex.in","r",stdin);
	freopen("hex.out","w",stdout);
	scanf("%s",s+1);
	n=strlen(s+1);
	for (int i=1;i<=n;++i)
		if (s[i]>='0'&&s[i]<='9')
			det[i]=s[i]-'0';
		else
			det[i]=s[i]-'a'+10;
	for (int i=1;i<=n;++i)
		sum+=det[i];
	if (sum%15!=0)
	{
		puts("No");
		return 0;
	}
	dfs(1);
	puts("No");
	return 0;
}