#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int mod=1e9+7;
const int N=10005;
int la[N],to[N],pr[N],cnt,cntt,n,m,x,y,ans,sum,size[N],vis[N];
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
map<int,int> mp;
map<int,int>::iterator it;
inline void add(int x,int y)
{
	to[++cntt]=y;
	pr[cntt]=la[x];
	la[x]=cntt;
}
inline int get_size(int x)
{
	int size=0,l=x,r=x;
	while (l<=n)
	{
		size+=min(r,n)-l+1;
		l=l*2;
		r=r*2+1;
	}
	return size;
}
// inline void dfs(int x,int y)
// {
// 	for (int i=la[x];i;i=pr[i])
// 		if (to[i]!=y)
// 		{
// 			size[x]-=size[to[i]];
// 			dfs(to[i],x);
// 		}
// }
inline void dfs(int x)
{
	int k=mp[x];
	if (mp.count(x*2))
	{
		size[k]-=size[mp[x*2]];
		dfs(x*2);
	}
	if (mp.count(x*2+1))
	{
		size[k]-=size[mp[x*2+1]];
		dfs(x*2+1);
	}
}
inline void dfs1(int x)
{
	(sum+=size[x])%=mod;
	vis[x]=1;
	for (int i=la[x];i;i=pr[i])
		if (!vis[to[i]])
			dfs1(to[i]);
	vis[x]=0;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(n);
	read(m);
	cnt=0;
	mp[1]=++cnt;
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		for (int k=x;k;k/=2)
			if (!mp[k])
				mp[k]=++cnt;
		for (int k=y;k;k/=2)
			if (!mp[k])
				mp[k]=++cnt;
		add(mp[x],mp[y]);
		add(mp[y],mp[x]);
	}
	for (it=mp.begin();it!=mp.end();++it)
	{
		pair<int,int> now=*it;
		if (now.first!=1)
		{
			add(now.second,mp[now.first/2]);
			add(mp[now.first/2],now.second);
		}
		size[now.second]=get_size(now.first);
	}
	dfs(1);
	for (it=mp.begin();it!=mp.end();++it)
	{
		int now=it->second;
		sum=0;
		dfs1(now);
		(ans+=(ll)size[now]*sum%mod)%=mod;
	}
	cout<<ans;
	return 0;
}