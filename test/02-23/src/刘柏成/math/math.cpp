#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
    freopen("math.in","r",stdin);
    freopen("math.out","w",stdout);
    buf[fread(buf,1,MAXSIZE,stdin)]='\0';
    bufpos=0;
}
#if NEG
int readint(){
    bool isneg;
    int val=0;
    for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
    bufpos+=(isneg=buf[bufpos]=='-');
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return isneg?-val:val;
}
#else
int readint(){
    int val=0;
    for(;!isdigit(buf[bufpos]);bufpos++);
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return val;
}
#endif
char readchar(){
    for(;isspace(buf[bufpos]);bufpos++);
    return buf[bufpos++];
}
int readstr(char* s){
    int cur=0;
    for(;isspace(buf[bufpos]);bufpos++);
    for(;!isspace(buf[bufpos]);bufpos++)
        s[cur++]=buf[bufpos];
    s[cur]='\0';
    return cur;
}
ll ans[50002];
int tat[50002];
int a[50002];
pair<int,int> p[50002][20];
int t[500002];
ll power(ll x,ll y){
    ll ans=1;
    while(y){
        if (y&1)
            ans=ans*x%mod;
        x=x*x%mod;
        y>>=1;
    }
    return ans;
}
int main(){
    init();
    int q=readint(),cur=0;
    if (q>2000){
        while(q--){
            int op=readint(),x=readint();
            if (op==1){
                a[++cur]=x;
                bool flag=false;
                for(int i=cur;i>=1;i--){
                    if (a[i]!=1)
                        flag=true;
                    if (flag)
                        tat[i]++;
                }
            }else{
                int sum=0;
                for(int i=x;i<=cur;i++)
                    sum+=tat[i];
                printf("%lld\n",power(4,sum));
            }
        }
        return 0;
    }
    while(q--){
        int op=readint(),x=readint();
        // printf("op=%d x=%d\n",op,x);
        if (op==1){
            a[++cur]=x;
            ans[cur]=1;
            int now=0;
            for(int i=2;i*i<=x;i++)
                if (x%i==0){
                    int qwq=0;
                    while(x%i==0)
                        x/=i,qwq++;
                    p[cur][++now]=make_pair(i,qwq);
                    // printf("%d %d\n",i,qwq);
                }
            if (x!=1){
                p[cur][++now]=make_pair(x,1);
                // printf("%d %d\n",x,1);
            }
            int mmax=0;
            ll lcm=1;
            for(int i=cur;i>=1;i--){
                mmax=max(mmax,a[i]);
                for(int j=1;;j++){
                    int x=p[i][j].first,y=p[i][j].second;
                    // printf("%d %d\n",x,y);
                    if (!x)
                        break;
                    if (y>t[x]){
                        int k=y-t[x];
                        while(k--)
                            lcm=lcm*x%mod;
                        t[x]=y;
                    }
                }
                // printf("lcm=%lld mmax=%d\n",lcm,mmax);
                ans[i]=(ans[i]*power(lcm,mmax))%mod;
            }
            //memset(t,0,sizeof(t));
            for(int i=cur;i>=1;i--){
                for(int j=1;;j++){
                    int x=p[i][j].first;
                    if (!x)
                        break;
                    t[x]=0;
                }
            }
        }else {
            ll qwq=1;
            for(int i=x;i<=cur;i++)
                qwq=qwq*ans[i]%mod;
            printf("%lld\n",qwq);
        }
    }
}