#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
    freopen("hex.in","r",stdin);
    freopen("hex.out","w",stdout);
    buf[fread(buf,1,MAXSIZE,stdin)]=' ';
    bufpos=0;
}
#if NEG
int readint(){
    bool isneg;
    int val=0;
    for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
    bufpos+=(isneg=buf[bufpos]=='-');
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return isneg?-val:val;
}
#else
int readint(){
    int val=0;
    for(;!isdigit(buf[bufpos]);bufpos++);
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return val;
}
#endif
char readchar(){
    for(;isspace(buf[bufpos]);bufpos++);
    return buf[bufpos++];
}
int readstr(char* s){
    int cur=0;
    for(;isspace(buf[bufpos]);bufpos++);
    for(;!isspace(buf[bufpos]);bufpos++)
        s[cur++]=buf[bufpos];
    s[cur]='\0';
    return cur;
}
char s[22];
int get(char c){
    if (c>='0' && c<='9')
        return c-'0';
    else return c-'a'+10;
}
char put(int x){
    if (x<10)
        return x+'0';
    else return x-10+'a';
}
int n;
void print(int x){
    // printf("%d\n",x);
    for(int i=n-1;i>=0;i--)
        putchar(put((x>>(i*4))&15));
}
int cnt[16];
int main(){
    init();
    n=readstr(s);
    int x=0;
    for(int i=0;i<n;i++)
        x=x*16+get(s[i]);
    int tmp=1<<(4*n);
    for(int i=0;i<tmp-x;i++){ //i+x is permutation of i
        memset(cnt,0,sizeof(cnt));
        for(int j=0;j<n;j++)
            cnt[(i>>(j*4))&15]++;
        int qwq=i+x;
        for(int j=0;j<n;j++)
            cnt[(qwq>>(j*4))&15]--;
        bool flag=1;
        for(int j=0;j<16;j++)
            flag&=!cnt[j];
        if (flag)
            return print(i),0;
    }
    puts("NO");
}