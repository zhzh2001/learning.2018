#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
    freopen("tree.in","r",stdin);
    freopen("tree.out","w",stdout);
    buf[fread(buf,1,MAXSIZE,stdin)]='\0';
    bufpos=0;
}
#if NEG
int readint(){
    bool isneg;
    int val=0;
    for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
    bufpos+=(isneg=buf[bufpos]=='-');
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return isneg?-val:val;
}
#else
int readint(){
    int val=0;
    for(;!isdigit(buf[bufpos]);bufpos++);
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return val;
}
#endif
char readchar(){
    for(;isspace(buf[bufpos]);bufpos++);
    return buf[bufpos++];
}
int readstr(char* s){
    int cur=0;
    for(;isspace(buf[bufpos]);bufpos++);
    for(;!isspace(buf[bufpos]);bufpos++)
        s[cur++]=buf[bufpos];
    s[cur]='\0';
    return cur;
}
int n;
int dep(int u){
    return 32-__builtin_clz(u);
}
int lca(int u,int v){
    int du=dep(u),dv=dep(v);
    while(u!=v){
        if (du<dv)
            swap(u,v),swap(du,dv);
        u/=2;
        du--;
    }
    return u;
}
int sz(int u){
    int qwq=1,ans=0;
    while(u<=n){
        //[u,u+qwq)
        int tat=min(u+qwq-1,n);
        ans+=max(tat-u+1,0);
        u<<=1;
        qwq<<=1;
    }
    return ans;
}
int main(){
    init();
    n=readint();
    int m=readint();
    if (!m){
        printf("%lld",1LL*n*n%mod);
        return 0;
    }
    /*if (m==1){
        int u=readint(),v=readint(),lc=lca(u,v);
        int t=v/2;
        bool flag=v%2;
        ll ans=0;
        while(t!=lc){
            ans=(ans+1LL*(sz(t*2+(!flag))+1)*sz(t*2+flag))%mod;
            flag=t%2;
            t/=2;
        }
        ans+=sz(t*2+(!flag))*(n-sz(;
    }*/
}