#include<cstdio>
#include<ctype.h>
#include<cstring>
#include<cstdlib>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<cstdlib>
#include<algorithm>
using namespace std;
typedef long long ll;
#define RG register
#define pc putchar
#define gc getchar
#define rd read
#define men(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Down(i,a,b) for(RG int i=(a);i>=(b);--i)
inline ll read(){
	RG ll x=0,f=1;RG char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}

const int maxn = 50050;
const int Mod = 1e9+7;
inline int gcd(int a,int b){
	if(!b) return a; else return gcd(b,a%b);
}
int Pow(int a,int b){
	int ans = 1;
	for(;b;b>>=1,a=1ll*a*a%Mod)
		if(b&1) ans=1ll*ans*a%Mod;
	return ans;
}
#define inv(x) Pow(x,Mod-2)
int f[maxn],g[maxn],a[maxn],n,Q;
void solve1(){
	n = 0;
	while(Q--){
		int op = rd(),x = rd();
		if(op == 1){
			++n;
			a[n] = x;
		} else{
			int ans = 1;
			for(int l=x;l<=n;l++){
				for(int r=l;r<=n;r++){
					int LCM = 1,Max = 0;
					for(int i=l;i<=r;i++) LCM = LCM * a[i] / gcd(LCM,a[i]),Max = max(Max,a[i]);
					ans = 1ll * ans * Pow(LCM,Max) % Mod;
				}
			}
			printf("%lld\n",ans);
		}
	}	

}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
//	init();
	Q = rd();
//	if(Q<=5){
		solve1();
		return 0;
//	}
	
	n = 0;
	while(Q--){
		int op = rd(),x = rd();
		if(op == 1){
			RG int Gcd,pre,Max;
			++n;
			a[n] = x;
			pre = Gcd = Max = x;
			g[n] = Pow(x,x);f[n] = 1;
			Down(i,n-1,1){
				pre = 1ll * pre * a[i] % Mod;
				Gcd = gcd(Gcd,a[i]);
				Max = max(Max,a[i]);
				g[i] =  Pow(1ll * pre * inv(Pow(Gcd,n-i)) % Mod,Max);
			}
		} else{
			printf("%d\n",f[x]);
		}
	}	
	return 0;
}

/*
1
ans = 1
f[] = {1}
1 2
ans = 
f[] = {}
1 2 3

2 * 


2 3 1 4

*/

















