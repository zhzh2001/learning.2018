#include<cstdio>
#include<ctype.h>
#include<cstring>
#include<cstdlib>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<cstdlib>
#include<algorithm>
using namespace std;
typedef long long ll;
#define RG register
#define pc putchar
#define gc getchar
#define rd read
#define men(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Down(i,a,b) for(RG int i=(a);i>=(b);--i)
inline ll read(){
	RG ll x=0,f=1;RG char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
const int Mod = 1e9+7;
int deep(int x){
	if(x==1) return 1; else return deep(x>>1)+1;
}
int maxdeep;
int n,m,ans;
int x[23],y[23];
int size(int x){
	if(!x) return 0;
	int deepx = deep(x);
	int res = (1 << (maxdeep-deep(x))) - 1;
	int L = x,R = x;
	for(;deepx<maxdeep;deepx++)
		L=L*2,R=R*2+1;
	res += max(min(R,n) - L + 1,0);
	return res;
}
/*void dfs(int u){
	if(u > n) return;
	size[u] = 1;
	dfs(u*2),dfs(u*2+1);
	size[u] += size[u*2] + size[u*2+1];
}*/
int LCA(int x,int y){
	while(x!=y)
		if(x>y) x>>=1; else y>>=1;
	return x;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n = rd(),m = rd();
	ans = 1ll * n * n % Mod;
	maxdeep = deep(n);
	Rep(i,1,m)
		x[i] = rd(),y[i] = rd();
	if(m==0){
		printf("%d\n",ans);
		return 0;
	}
	if(m==1){
		int A = x[1],B = y[1],C = LCA(A,B),last=0;
		if(deep(A) > deep(B)) swap(A,B);
		if(C==A){
			while(B!=C){
				ans = (ans + 1ll * (size(B)-size(last)) * (n - size(B)) % Mod) % Mod;
				last = B;
				B >>= 1;
			}
			printf("%d\n",ans);
		} else{
			int anss = 0;
			#define ans anss
			while(B!=C){
				ans = (ans + 1ll * (size(B)-size(last)) * (n - size(B)) % Mod) % Mod;
				last = B;
				B >>= 1;
			}
			int tmpB = B,tmpC =C,tmpA =A;
			last = 0;
//			last = A;A>>=1;
			while((A>>1)!=C){
				ans = (ans + 1ll * (size(A)) * (size(A>>1)-size(A)) % Mod) % Mod;
				last = A;
				A >>= 1;
			}
			ans = (ans + size(A)) % Mod;
//			A=tmpA,B=tmpB,C=tmpC;
			last = B;B >>=1;
			while(B!=0){
				ans = (ans + 1ll * (size(B)-size(last) * size(A)) % Mod) % Mod;
				last = B;
				B >>= 1;
			}
			#undef ans
			anss = (1ll * anss * 2) % Mod;
			ans = (ans + anss) % Mod;
			printf("%d\n",ans);
		}
	}
	return 0;
}




























