#include <bits/stdc++.h>
#define LL long long
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}
void write(int x) {
	if(x<0) x=-1, putchar('-'); 
	if(x>9) write(x/10); 
	putchar(x%10+48); 
}
inline void writeln(int x) {
	write(x); putchar('\n'); 
}
const int mod = 1e9+7; 
const int N = 10011;    //
int Ques; 
int a[N]; 

int gcd(int x, int y) {
	if(!y) return x;  
	return gcd(y, x%y); 
}
int ksm(int x, int y) {
	int b[35], pos = 0; 
	while(y) {
		b[++pos] = y%2;
		y/=2; 
	}
	int res = 1; 
	Dow(i, pos, 1) {
		res=1ll*res*res%mod; 
		if(b[i]) res=1ll*res*x%mod; 
	}
	return res; 
}
inline LL lcm(int x,int y) {
	return (1ll*x*y)/gcd(x,y) %mod; 
}

inline void get_1() {
	int len = 0; 
	while(Ques--) {
		int opt = read(); 
		if(opt==1) {
			a[++len] = read(); 
			a[len]%=mod; 
			continue; 
		}
		int x = read(); 
		int ans = 1; 
		For(l, x, len) 
			For(r, l, len) {
				int Mx=0, Lcm=1; 
				For(i, l, r) Mx = max(Mx, a[i])%mod; 
				For(i, l, r) Lcm = lcm(a[i],Lcm)%mod;  
				int sum = ksm(Lcm, Mx)%mod; 
				ans = 1ll*ans*sum%mod; 
			}
		writeln(ans); 
	}
}

int main() {
	freopen("math.in","r",stdin); 
	freopen("math.out","w",stdout); 
	Ques = read(); 
	if(Ques<=300) get_1();  
}


/*

4 
1 1 
1 2 
1 3 
2 1



*/



