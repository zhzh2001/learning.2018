#include <bits/stdc++.h>
#define LL long long
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}

const int mod = 1e9+7; 
const int N = 1e6+11; 
int n, m, x, y, nedge; 
LL ans; 
int vis[N], head[N], fa[N], siz[N]; 
struct node{
	int to, pre; 
}e[N*2]; 
inline void add(int x, int y) {
	e[++nedge] = (node){ y, head[x] }; 
	head[x] = nedge; 
}

void dfs(int u, int fath) {
	fa[u] = fath; 
	for(int i=head[u]; i; i=e[i].pre) {
		int v = e[i].to; 
		if(v!=fath) dfs(v, u); 
	}
}

void getsize(int u) {
	vis[u] = 1; siz[u] = 1; 
	for(int i=head[u]; i; i=e[i].pre) {
		int v = e[i].to; 
		if(v==fa[u] || vis[v]) continue; 
		getsize(v); 
		siz[u] += siz[v]; 
	}
	
}

int main() {
	freopen("tree.in","r",stdin); 
	freopen("tree.out","w",stdout); 
	n = read(); m = read(); 
	if(m==0) {
		LL ans = 1ll*n*n%mod; 
		printf("%I64d\n", ans); 
		return 0; 
	}
	
	if(m==1) {
		x = read(), y = read(); 
		ans = (1ll*n*n%mod*2%mod+mod-n)%mod; 
		For(i, 2, n) {
			add(i, i/2); 
			add(i/2, i); 
		}
		dfs(x, -1); 
		while(y != -1) {
			getsize(y); 
			int sum = 1ll*siz[y]*(siz[y]-1) %mod; 
			ans = (ans+mod-sum)%mod; 
			y = fa[y]; 
		}
		printf("%lld\n", ans); 
	}
	
}




