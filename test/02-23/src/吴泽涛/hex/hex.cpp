#include <bits/stdc++.h>
#define LL long long
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}

const int N = 17; 
int len, TI; 
int a[N], New[N], c[N], b[N], f[16]; 

inline void check() {
	For(i, 0, len) c[i] = 0;
	For(i, 1, len) b[i] = New[i];  
	Dow(i, len, 1) {
		c[i] = a[i]+b[i]+c[i]; 
		c[i-1] += (c[i]>>4); 
		c[i]&=15; 
	}
	if(c[0]) return; 
	For(i, 0, 15) f[i] = 0; 
	For(i, 1, len) ++f[b[i]];  
	For(i, 1, len) {
		if(!f[c[i]]) return; 
		--f[c[i]]; 
	}
	char ch[10]; 
	For(i, 1, len) {
		if(New[i]>=10) ch[1] = char('a'+New[i]-10); 
		else ch[1] = '0'+New[i];
		cout<<ch[1];   
	}
	exit(0); 
}

void dfs(int x) {
	if(x>len) {
		++TI; 
		if(TI==10000000) {
			puts("NO"); 
			exit(0); 
		}
		check(); 
		return; 
	}
	For(i, 0, 15) {
		New[x] = i; 
		dfs(x+1); 
	}
}

void zyy(int x) {
	if(x>len) {
		++TI; 
		if(TI==6000000) {
			puts("NO"); 
			exit(0); 
		}
		check(); 
		return; 
	}
	if(rand()%2) 
		Dow(i, 15, 0) {
			New[x] = i; 
			zyy(x+1); 
		}
	else
		For(i, 0, 15) {
			New[x] = i; 
			zyy(x+1); 
		}
}


int main() {
	freopen("hex.in","r",stdin); 
	freopen("hex.out","w",stdout); 
	char s[20]; 
	srand(time(0)); 
	scanf("%s",s+1); 
	len = strlen(s+1); 
	For(i, 1, len) {
		if(s[i]>='0' && s[i]<='9') 
			a[i] = s[i]-'0'; 
		else 
			a[i] = s[i]-'a'+10; 
	}
	if(len<=7) dfs(1); 
	else zyy(1); 
	puts("NO"); 
}




