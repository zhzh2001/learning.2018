#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>

#include <algorithm>
#include <queue>
#include <stack>
#include <utility>

#define LL long long
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') {
		if(ch=='-') f = -1;
		ch = getchar();
	}
	while(ch>='0'&&ch<='9') {
		x = x*10+ch-48;
		ch = getchar();
	}
	return x * f;
}
void write(int x) {
	if(x<0) putchar('-'), x = -x;
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(int x) {
	write(x);
	putchar('\n');
}
// data
const int mod=1e9+7;
int N;
struct node {
	int val;
	int max,lcm;
	bool used;///可能还没有添加
} T[500100];
int a[50010];
int lcm[10010];
int maxval[18][50010];
int lev[1<<18];
int cnt;
// code
int max_gcd(int a,int b) {
	return b==0? a:max_gcd(b,a%b);
}
int qpow(int base,int index) {
	if(index==1)	return base;
	if(index==0)	return 0;
	if(index&1)		return ( qpow( (base*base)%mod,index>>1)%mod*base )%mod;
	else			return ( qpow( (base*base)%mod,index>>1)%mod)%mod;
}
LL check(int L,int R)
{
	int maxv=a[L];
	LL lcmv=a[L];
	for(int i=L+1;i<=R;++i)
	{
		maxv=max(maxv,a[i]);
		lcmv=( lcmv*(a[i]/max_gcd(a[i],lcmv)) )%mod;
	}
	//printf("[%d,%d] %d^%d",L,R,lcmv,maxv);
	return qpow(lcmv,maxv);
}
void task1() {
	int flag,x;
	LL ans=1;
	for(int i=1; i<=N; ++i) {
		flag=read(),x=read();
		ans=1;
		if(flag==1)	a[++cnt]=x;
		else {
			for(int i=x;i<=cnt;++i)
				for(int j=i;j<=cnt;++j)
				{
					LL t=check(i,j);
					ans*=t;
					ans%=mod;
				}
			cout<<ans<<endl;
		}
	}
}

/*void add(int pos,int x,int L=1,int R=50010,int id=1) {
	if(pos<L||pos>R)	return;
	if(L==R) {
		T[id].max=T[id].lcm=x;
		T[id].val=qpow(x,x);
	}
	int mid=(L+R)>>1;
	if(x<=mid) {
		add(pos,x,L,mid,id<<1);
		T[id]=T[id<<1];
	} else {
		add(pos,x,mid+1,R,id<<1|1);
		int level=lev[id];
		lcm[level][pos]=lcm[level][pos-1]*(a[pos]/max_gcd(lcm[level][pos-1],a[pos]));
		//maxval[level][pos]=max(maxval[])
	}
}*/
int main() {
	freopen("math.in","r+",stdin);
	freopen("math.out","w+",stdout);
	scanf("%d",&N);
	if(N<=2000)		task1();
	return 0;
}
