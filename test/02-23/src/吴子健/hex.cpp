#include <bits/stdc++.h>
#define LL unsigned long long
#define ch(x)	(x<10?x+'0':x-10+'a')
using namespace std;
inline int read() {
	LL x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') {
		if(ch=='-') f = -1;
		ch = getchar();
	}
	while(ch>='0'&&ch<='9') {
		x = x*10+ch-48;
		ch = getchar();
	}
	return x * f;
}
void write(LL x) {
	if(x<0) putchar('-'), x = -x;
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(LL x) {
	write(x);
	putchar('\n');
}

// data
int T;
char delta[30];
LL d_num,x_num;
char x[30],s[30];
int counter_x[20];///x的数位计数器
int counter_s[20];

int bound;
// code
int str_dec(LL& num,char*src,int *counter=NULL) {
	num=0;
	if(counter)	memset(counter,0,18);
	for(int i=1; i<=T; ++i) {
		int t=isdigit(src[i])?src[i]-'0':src[i]-'a'+10;
		num=num*16+t;
		if(counter)
			++counter[t];
	}
}
void num_str(LL num,char*dest=NULL,int *counter=NULL) {
	if(dest)	dest[T+1]='\0';
	if(counter)	memset(counter,0,18);
	for(int i=T; i>=1; --i) {
		int t=num%16;
		num/=16;
		if(counter)	++counter[t];
		if(dest)
			if(t<10)	dest[i]=t+'0';
			else		dest[i]=t-10+'a';
	}
}
bool isOK() {
	str_dec(x_num,x,counter_x);
	LL s_num=x_num+d_num;
//	printf("%ulld d_num=%ulld x_num=%ulld T=%ulld\n",s_num,d_num,x_num,T);
	if(s_num<(1<<(4*T))) {
		num_str(s_num,s,counter_s);
		if(memcmp(counter_x,counter_s,18)==0)	return true;
	}
	s_num=abs(x_num-d_num);
	if(s_num<(1<<(4*T))) {
		num_str(s_num,s,counter_s);
		if(memcmp(counter_x,counter_s,18)==0)	return true;
	}
	return false;
}
void dfs(int pos) {
	//printf("%s pos=%d\n",__func__,pos);
	//system("pause");
	for(int i=0; i<=15; ++i) {
		char t=ch(i);
		//printf("cur t=%c\n",t);
		x[pos]=t;
		//puts(x+1);
		if(pos==T-bound+1) {
			if(isOK()) {
				puts(x+1);
				exit(0);
			}
			else
				continue;
		}
		dfs(pos-1);
	}
}
int main() {
	freopen("hex.in","r+",stdin);
	freopen("hex.out","w+",stdout);
	gets(delta+1);
	T=strlen(delta+1);
	str_dec(d_num,delta);
	memset(x,'0',sizeof x);
	x[T+1]='\0';
	

/*	x_num=58081722750ull;
	num_str(x_num,x,counter_x);
	if(isOK())	puts(x+1);
	else exit(0);*/
	
	for(int i=1; i<=T; ++i) {
		bound=i;
		dfs(T);
	}
	puts("NO");
	return 0;
}






