#include <fstream>
#include <vector>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N = 20, MOD = 1e9 + 7;
bool vis[N];
vector<int> mat[N];
int ans;
void dfs(int k)
{
	vis[k] = true;
	ans++;
	for (int i = 0; i < mat[k].size(); i++)
		if (!vis[mat[k][i]])
			dfs(mat[k][i]);
	vis[k] = false;
}
int main()
{
	int n, m;
	fin >> n >> m;
	if (m == 0)
		fout << 1ll * n * n % MOD << endl;
	else
	{
		for (int i = 2; i <= n; i++)
		{
			mat[i].push_back(i / 2);
			mat[i / 2].push_back(i);
		}
		while (m--)
		{
			int u, v;
			fin >> u >> v;
			mat[u].push_back(v);
			mat[v].push_back(u);
		}
		for (int i = 1; i <= n; i++)
			dfs(i);
		fout << ans << endl;
	}
	return 0;
}