#include <stdio.h>
#include <algorithm>
#include <cstring>
using namespace std;
int main()
{
	freopen("hex.in", "r", stdin);
	freopen("hex.out", "w", stdout);
	char s[20];
	scanf("%s", s);
	int n = strlen(s);
	if (n <= 5)
	{
		int delta = strtoul(s, NULL, 16);
		for (int t = 0; t + delta < 1 << (n * 4); t++)
		{
			int s = t + delta;
			char ss[6] = {}, st[6] = {};
			sprintf(ss, "%0*x", n, s);
			sprintf(st, "%0*x", n, t);
			sort(ss, ss + n);
			sort(st, st + n);
			if (!strcmp(ss, st))
			{
				printf("%0*x\n", n, t);
				return 0;
			}
		}
	}
	puts("NO");
	return 0;
}