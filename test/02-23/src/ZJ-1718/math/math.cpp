#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;
ifstream fin("math.in");
ofstream fout("math.out");
const int N = 50005, MOD = 1e9 + 7;
typedef pair<int, int> item;
vector<item> p[N];
int a[N];
long long qpow(long long a, int b)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % MOD;
		a = a * a % MOD;
	} while (b /= 2);
	return ans;
}
int main()
{
	int n = 0, m;
	fin >> m;
	if (m <= 200)
		while (m--)
		{
			int opt, x;
			fin >> opt >> x;
			if (opt == 1)
			{
				a[++n] = x;
				for (int i = 2; i * i <= x; i++)
					if (x % i == 0)
					{
						int cnt = 0;
						for (; x % i == 0; x /= i, cnt++)
							;
						p[n].push_back(make_pair(i, cnt));
					}
				if (x > 1)
					p[n].push_back(make_pair(x, 1));
			}
			else
			{
				long long ans = 1;
				for (int l = x; l <= n; l++)
				{
					long long lv = 1;
					int mx = 0;
					map<int, int> lcm;
					for (int r = l; r <= n; r++)
					{
						mx = max(mx, a[r]);
						for (int i = 0; i < p[r].size(); i++)
						{
							int pr = p[r][i].first;
							for (; lcm[pr] < p[r][i].second; lcm[pr]++)
								lv = lv * pr % MOD;
						}
						ans = ans * qpow(lv, mx) % MOD;
					}
				}
				fout << ans << endl;
			}
		}
	else
		while (m--)
		{
			int opt, x;
			fin >> opt >> x;
			if (opt == 1)
				a[++n] = x;
			else
			{
				int cnt = 0, pred = 0;
				for (int i = x; i <= n; i++)
					if (a[i] == 1)
						cnt += pred;
					else
						cnt += (pred = i - x + 1);
				fout << qpow(4, cnt) << endl;
			}
		}
	return 0;
}