#include <bits/stdc++.h>
#define N 50020
#define mod 1000000007
#define ll long long
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
ll fast_pow(ll a, ll b) {
  ll c = 1;
  for (; b; b >>= 1, a = a * a % mod)
    if (b & 1) c = c * a % mod;
  return c;
}
ll inv(ll x) {
  return fast_pow(x, mod - 2);
}
ll gcd(ll a, ll b) {
  return b ? gcd(b, a % b) : a;
}
ll lcm(ll a, ll b) {
  return a * b % mod * inv(gcd(a, b)) % mod;
}
ll lm[N], mx[N], f[N];
int main(int argc, char const *argv[]) {
  // freopen("../../sample/math/2_1.in", "r", stdin);
  // // freopen("2_1.in", "r", stdin);
  // freopen("2_1.out", "w", stdout);
  freopen("math.in", "r", stdin);
  freopen("math.out", "w", stdout);
  int T = read(), len = 0;
  while (T --) {
    int op = read();
    ll x = read();
    if (op == 1) {
      for (int i = 1; i <= len; i++) {
        lm[i] = lcm(lm[i], x);
        mx[i] = max(mx[i], x);
        f[i] = f[i] * fast_pow(lm[i], mx[i]) % mod;
      }
      ++ len;
      f[len] = fast_pow(lm[len] = x, mx[len] = x);
    } else {
      ll ans = 1;
      for (int i = x; i <= len; i++)
        ans = ans * f[i] % mod;
      cout << ans << endl;
    }
  }
  // n^2log ...
  // why wa???
  return 0;
}