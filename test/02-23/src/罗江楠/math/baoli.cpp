#include <bits/stdc++.h>
#define N 50020
#define mod 1000000007
#define ll long long
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
ll a[N];
ll gcd(ll a, ll b) {
  return b ? gcd(b, a % b) : a;
}
ll lcm(ll a, ll b) {
  return a * b / gcd(a, b) % mod;
}
ll fast_pow(ll a, ll b) {
  // cout << a << " " << b << endl;
  ll c = 1;
  for (; b; b >>= 1, a = a * a % mod)
    if (b & 1) c = c * a % mod;
  return c;
}
int main(int argc, char const *argv[]) {
  freopen("../../sample/math/2_1.in", "r", stdin);
  freopen("2_1.out", "w", stdout);
  int T = read(), len = 0;
  while (T --) {
    int op = read(), x = read();
    if (op == 1) {
      a[++len] = x;
    } else {
      ll ans = 1;
      for (int i = x; i <= len; i++) {
        ll lm = a[i], mx = a[i];
        for (int j = i; j <= len; j++) {
          lm = lcm(lm, a[j]);
          mx = max(mx, a[j]);
          ans = ans * fast_pow(lm, mx) % mod;
        }
      }
      cout << ans << endl;
    }
  }
  return 0;
}