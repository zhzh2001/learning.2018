#include <bits/stdc++.h>
#define ll long long
#define mod 1000000007
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int main(int argc, char const *argv[]) {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);
  ll n = read();
  cout << n * n % mod << endl;
  return 0;
}