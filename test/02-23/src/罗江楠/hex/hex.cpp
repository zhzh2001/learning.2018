#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
ll pw[20];
char ch[20];
ll calc(char* ch, int len) {
  ll res = 0, k;
  for (int i = len, k; i; i--) {
    if (isdigit(ch[i])) k = ch[i] - '0';
    else k = ch[i] - 'a' + 10;
    res += k * pw[len - i];
  }
  return res;
}
bool equal(ll a, ll b, int len) {
  static char ax[20], bx[20];
  memset(ax, 0, sizeof ax);
  memset(bx, 0, sizeof bx);
  int at = 0, bt = 0;
  while (a) ax[++ at] = a % 16, a /= 16;
  while (b) bx[++ bt] = b % 16, b /= 16;
  sort(ax + 1, ax + len + 1);
  sort(bx + 1, bx + len + 1);
  for (int i = 1; i <= len; i++)
    if (ax[i] != bx[i]) return false;
  return true;
}
int output(ll a, int len) {
  static char ax[20];
  memset(ax, 0, sizeof ax);
  int at = 0;
  while (a) ax[++ at] = a % 16, a /= 16;
  for (int i = len; i; i--)
    if (ax[i] > 9) putchar(ax[i] + 'a' - 10);
    else putchar(ax[i] + '0');
  putchar('\n');
  return 0;
}
int main(int argc, char const *argv[]) {
  freopen("hex.in", "r", stdin);
  freopen("hex.out", "w", stdout);
  scanf("%s", ch + 1);
  pw[0] = 1;
  for (int i = 1; i < 20; i++)
    pw[i] = pw[i - 1] * 16;
  int len = strlen(ch + 1);
  ll val = calc(ch, len);
  ll mxi = pw[len + 1];
  for (ll i = 1; i + val < mxi; i++)
    if (equal(i, i + val, len))
      return output(i, len);
  puts("NO");
  return 0;
}