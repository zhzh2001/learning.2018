#include<iostream>
#include<cstdio>
#include<vector>
using namespace std;
#define mp make_pair
const int N=100005,mod=1000000007;
int sss[N],que1[N],que2[N],q[N],op[N],x[N],ttttt,que[N];
int t,zhan[N],top,r,lazypow[N<<2],lazymul[N<<2],mul[N<<2],sum[N<<2],tree[N<<2];
vector<pair<int,int> > v[500005];
inline int ksm(int x,int y){
	int s=1;
	for (;y;y/=2,x=(long long)x*x%mod)
		if(y&1)s=(long long)s*x%mod;
	return s;
}
inline void p(int nod){
	tree[nod]=(long long)tree[nod<<1]*tree[nod<<1|1]%mod;
	mul[nod]=(long long)mul[nod<<1]*mul[nod<<1|1]%mod;
	sum[nod]=(sum[nod<<1]+sum[nod<<1|1])%(mod-1);
}
inline void cao(int p,int x,int op,int len){
	if(op){
		lazypow[p]+=x;
		tree[p]=(long long)tree[p]*ksm(mul[p],x)%mod;
		sum[p]=(sum[p]+(long long)x*len)%(mod-1);
	}else{
		lazymul[p]=(long long)lazymul[p]*x%mod;
		mul[p]=(long long)mul[p]*ksm(x,len)%mod;
		tree[p]=(long long)tree[p]*ksm(x,sum[p])%mod;
	}
}
inline void pushdown(int p,int l,int r){
	int mid=(l+r)>>1;
	if(lazypow[p]){
		cao(p<<1,lazypow[p],1,mid-l+1); cao(p<<1|1,lazypow[p],1,r-mid); lazypow[p]=0;
	}
	if(lazymul[p]!=1){
		cao(p<<1,lazymul[p],0,mid-l+1); cao(p<<1|1,lazymul[p],0,r-mid); lazymul[p]=1;
	}
}
inline int ask(int l,int r,int i,int j,int nod){
    int mid=(l+r)>>1;
	if (l==i&&j==r) return tree[nod]; pushdown(nod,l,r);
	if (j<=mid) return ask(l,mid,i,j,nod<<1);
	else if(i>mid) return ask(mid+1,r,i,j,nod<<1|1);
	int left=ask(l,mid,i,mid,nod<<1);
	int right=ask(mid+1,r,mid+1,j,nod<<1|1);
	return (long long)left*right%mod;
}
inline void insert(int l,int r,int i,int j,int s,int op,int nod){
    int mid=(l+r)>>1;
	if(l==i&&r==j){
		cao(nod,s,op,r-l+1); return;
	}
    pushdown(nod,l,r);
    if(j<=mid)insert(l,mid,i,j,s,op,nod<<1); else
    if(i>mid)insert(mid+1,r,i,j,s,op,nod<<1|1); else{
    	insert(l,mid,i,mid,s,op,nod<<1); insert(mid+1,r,mid+1,j,s,op,nod<<1|1);
	}
	p(nod);
}
inline void build(int l,int r,int nod){int mid=(l+r)>>1;
	if(l==r){tree[nod]=lazymul[nod]=mul[nod]=1; sum[nod]=lazypow[nod]=0; return;} lazymul[nod]=1;
	build(l,mid,nod<<1); build(mid+1,r,nod<<1|1);
	p(nod);
}
inline void write(int a){
	if(a>=10)write(a/10);
	putchar('0'+a%10);
}
inline void writeln(int a){
	write(a); puts("");
}
inline int	read(){
	int x = 0; char ch = getchar(); bool positive = 1;
	for (; !isdigit(ch); ch = getchar())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = getchar())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
#include<cmath>
#define ll long long
ll ans[N];
int n,id[N];
inline void yyy(int l,int r,int nod){
	if(l==r){
		sss[l]=((ll)tree[nod]*sss[l+1])%mod; return;
	}
	pushdown(nod,l,r);
	int mid=(l+r)>>1;
	yyy(mid+1,r,nod<<1|1); yyy(l,mid,nod<<1);
}
int main(){
	freopen("math.in","r",stdin); freopen("math.out","w",stdout);
	int qq=read();
	for(int i=1;i<=500000;i++)v[i].push_back(mp(0,1e9));
	zhan[1]=1e9; int tot=q[top=1]=0;
	for(int i=1;i<=qq;i++){
		op[i]=read(); x[i]=read();
		if(op[i]==1)n++; else {que[++ttttt]=x[i]; id[ttttt]=i; ans[ttttt]=1;}
	}
	build(1,n,1); sss[n+1]=1;
	for(int i=1;i<=qq;i++)if(op[i]==1){
		tot++;
		int zs=t=x[i],meiju=sqrt(zs); r=0;
		for(int j=2;j<=meiju;j++)if(zs%j==0){
			que1[++r]=j; que2[r]=0;
			while(zs%j==0){que2[r]++; zs/=j;}
		}
		if(zs>1){
			que1[++r]=zs; que2[r]=1;
		} zs=t;
		for(int l=1;l<=r;l++){
			v[que1[l]].push_back(mp(tot,0));
			for(int j=v[que1[l]].size()-2;v[que1[l]][j+1].second<=que2[l];j--){
				//cout<<v[que1[l]][j].first+1<<" "<<v[que1[l]][j+1].second<<" "<<que1[l]<<" "<<j<<" "<<v[que1[l]][j+1].second<<endl;
			    if(que2[l]-v[que1[l]][j+1].second)insert(1,n,v[que1[l]][j].first+1,v[que1[l]][j+1].first,ksm(que1[l],que2[l]-v[que1[l]][j+1].second),0,1);
				v[que1[l]].pop_back();
			}
			v[que1[l]].push_back(mp(tot,que2[l]));
		}
		insert(1,n,tot,tot,zs,1,1);
		while(zhan[top]<=zs){
			if(zs-zhan[top])insert(1,n,q[top-1]+1,q[top],zs-zhan[top],1,1);
			top--;
		}
		q[++top]=tot; zhan[top]=zs;
		//cout<<ttttt<<endl;
		if(ttttt>10){yyy(1,n,1);
		for(int j=lower_bound(&id[1],&id[ttttt+1],i)-id;j<=ttttt;j++)ans[j]=ans[j]*sss[que[j]]%mod;}else{
			for(int j=lower_bound(&id[1],&id[ttttt+1],i)-id;j<=ttttt;j++)ans[j]=ans[j]*ask(1,n,que[j],n,1)%mod;
		}
	}
	for(int i=1;i<=ttttt;i++)writeln(ans[i]);
}

