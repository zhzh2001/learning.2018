#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
ll n,m,ans;
ll poi[34];
int root[10],x[10],y[10];
int lt[10],gt[10],rt[10];
const int mod=1e9+7;
int findroot(int x,int y)
{
	while(x!=y){
		if(x>y)swap(x,y);
		y/=2;
	}
	return x;
}//找到x,y的根 
bool checkroot(int x,int y)
{
	while(x<y)y/=2;
	return x==y;
}//判定x是否是y的根 
int get(int x)
{
	ll ret=1;
	int k=0,t=1;
	ll left=x;
	for(;poi[k]<x;k++);
	while(poi[k]<n){
//		if(x*2>n)return ret;
		ret=ret+poi[t];left=left*2;x=x*2+1;t++;
//		cout<<ret<<' '<<x<<' '<<n<<endl;
		if(poi[k]<x)k++;
		if(x>n)ret-=(x-max(n,left-1));
	}
	return ret;
}//取得子树大小
bool checkl(int x,int y)
{
	bool l=true;
	while(x>y){
		if(x%2==0)l=true;
		else l=false;
		x/=2;
	}
	return l;
}//x是在y的左子树还是右子树 
int main()
{ 
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	poi[0]=1;
	for(int i=1;i<=32;i++)
		poi[i]=poi[i-1]*2;
	n=read();m=read();
/*	for(int i=1;i<=n;i++)
		cout<<get(i)<<' ';
	cout<<endl;*/
	ans=n*n%mod;
	for(int i=1;i<=m;i++){
		x[i]=read();
		y[i]=read();
		if(x[i]>y[i])swap(x[i],y[i]);
		root[i]=findroot(x[i],y[i]);
		lt[i]=get(x[i]);
		gt[i]=get(root[i]);
		rt[i]=get(y[i]);
	}
	if(m>=1){
		for(int a=1;a<=m;a++){
			if(root[a]!=x[a]){
				ll t1,t2,t3,t4,t5;
				t1=get(1)-gt[a]+1;
				t2=get(root[a]*2);
				if(root[a]*2==x[a]||root[a]*2==y[a])t2=0;
				t3=get(root[a]*2+1);
				if(root[a]*2+1==x[a]||root[a]*2+1==y[a])t3=0;
				t4=lt[a];t5=rt[a];
//				cout<<t1<<' '<<t2<<' '<<t3<<' '<<t4<<' '<<t5<<endl;
				ans+=t1*t2*2;ans%=mod;
				ans+=t1*t3*2;ans%=mod;
				ans+=t1*t4*2;ans%=mod;
				ans+=t1*t5*2;ans%=mod;
				ans+=t2*t3*2;ans%=mod;
				ans+=t2*t4*2;ans%=mod;
				ans+=t2*t5*2;ans%=mod;
				ans+=t3*t4*2;ans%=mod;
				ans+=t3*t4*2;ans%=mod;
				ans+=t4*t5*2;ans%=mod;
			}
			else{
				ll t1,t2,t3;
				t1=get(1)-get(root[a]*2);
				if(checkl(y[a],x[a])){
					t2=get(x[a]*2)-rt[a];
				}else t2=get(x[a]*2+1)-rt[a];
				t3=rt[a];
//				cout<<t1<<' '<<t2<<' '<<t3<<endl;
				ans+=t1*t2*2;ans%=mod;
				ans+=t2*t3*2;ans%=mod;
				ans+=t3*t1*2;ans%=mod;
			}
		}
	}
	cout<<ans<<endl;
}
