#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
const int N=50005;
const int mod=1e9+7;
ll a[N];
ll qpow(ll x,ll y)
{
	ll ret=1;
	while(y){
		if(y&1)ret=ret*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return ret;
}
ll gcd(ll x,ll y)
{
	if(y>x)swap(x,y);
	if(x%y==0)return y;
	else return gcd(y,x%y);
}
ll lcm(ll x,ll y)
{
	return x*y/gcd(x,y);
}
int n,op,k,t,len;
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		op=read();k=read();
		if(op==1)a[++len]=k;
		else{
			ll ans=1;
			for(int j=k;j<=len;j++){
				ll now=a[j];
				ll ma=a[j];
				ans=ans*qpow(now,ma)%mod;
				for(int t=j+1;t<=len;t++){
					now=lcm(now,a[t])%mod;
					ma=max(a[t],ma);
					ans=ans*qpow(now,ma)%mod;
				}
			}
			printf("%lld\n",ans);
		}
	}
}
