#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int p=1000000007;

int d[1000000],g[1000000];
int i,j,k,l,n,r,s,t,u,v,w,x,y;

inline int power(int x,int y)
{
	int s=1;
	for (int i=30;i>=0;i--)
	{
		s=1LL*s*s%p;
		if ((1<<i)&y)
			s=1LL*s*x%p;
	}
	return s;
}

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;i++)
	{
		scanf("%d%d",&x,&y);
		if (x==1)
			u++,g[u]=y;
		else
		{
			s=1;
			for (j=y;j<=u;j++)
			{
//				memset(d,0,sizeof(d));
				r=1,w=0;
				for (k=j;k<=u;k++)
				{
					w=max(w,g[k]);
					for (l=2,t=g[k];l*l<=t;l++)
						if (! (t%l))
						{
							v=0;
							while (! (t%l))
								t=t/l,v++;
							for (;d[l]<v;d[l]++)
								r=1LL*r*l%p;
						}
					if (t>1)
						for (;d[t]<1;d[t]++)
							r=1LL*r*t%p;
					s=1LL*s*power(r,w)%p;
//					printf("%d %d %d %d\n",j,k,r,w);
				}
				for (k=j;k<=u;k++)
					for (l=1;l*l<=g[k];l++)
						if (! (g[k]%l))
							d[l]=0,d[g[k]/l]=0;
			}
			printf("%d\n",s);
		}
	}
	return 0;
}
