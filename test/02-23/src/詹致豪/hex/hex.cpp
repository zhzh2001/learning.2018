#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

int e[20000][20],u[20000][20],f[20000],f1[20000],f2[20000];
char c[20],d[20],v[20];
int i,j,k,n,t;

inline void calc()
{
	for (int i=1;i<=n;i++)
		if (c[i]==16)
			return;
//	for (int i=1;i<=n;i++) printf("%d ",c[i]); puts("");
	memset(f,0,sizeof(f));
	memset(f1,0,sizeof(f1));
	memset(f2,0,sizeof(f2));
	for (int i=0;i<(1<<n);i++)
		f[i]=f[i^(1<<(e[i][1]-1))]+c[e[i][1]];
	f1[0]=1;
	for (int i=0;i<(1<<n);i++)
		if ((f[i]>=0) && (f[i]<16))
			for (int j=1;j<=e[i][0];j++)
				f1[i]|=f1[i^(1<<(e[i][j]-1))];
	f2[(1<<n)-1]=1;
	for (int i=(1<<n)-1;i>=0;i--)
		for (int j=1;j<=e[i][0];j++)
			if ((f[i^(1<<(e[i][j]-1))]>=0) && (f[i^(1<<(e[i][j]-1))]<16))
				f2[i^(1<<(e[i][j]-1))]|=f2[i];
	bool b=false;
	for (int i=1;i<=n;i++)
	{
		t=16;
		for (int j=0;j<(1<<n);j++)
			if ((f2[j]) && (j&(1<<(i-1))) && (f1[j^(1<<(i-1))]))
				t=min(t,f[j^(1<<(i-1))]);
//		printf("%d %d\n",i,t);
		if (t==16)
			return;
		v[i]=t;
//		printf("%d %d\n",i,t);
		if (((! b) && (v[i]>d[i])) && (d[0]!=-1))
			return;
		if (v[i]<d[i])
			b=true;
//		for (int j=0;j<(1<<n);j++)
//			if ((f2[j]) && (j&(1<<(i-1))) && (f1[j^(1<<(i-1))]) && (f[j^(1<<(i-1))]==t)) printf("%d %d\n",j^(1<<(i-1)),j);
		memset(f1,0,sizeof(f1));
		memset(f2,0,sizeof(f2));
		f1[0]=1;
		for (int j=0;j<(1<<n);j++)
			if (f1[j])
				for (int k=1;k<=u[j][0];k++)
					if ((f[j^(1<<(u[j][k]-1))]>=0) && (f[j^(1<<(u[j][k]-1))]<16))
						if ((u[j][k]>i) || (f[j]==v[u[j][k]]))
							f1[j^(1<<(u[j][k]-1))]|=f1[j];
		f2[(1<<n)-1]=1;
		for (int j=(1<<n)-1;j>=0;j--)
			if (f1[j]&f2[j])
				for (int k=1;k<=e[j][0];k++)
					if ((f[j^(1<<(e[j][k]-1))]>=0) && (f[j^(1<<(e[j][k]-1))]<16))
						if ((e[j][k]>i) || (f[j^(1<<(e[j][k]-1))]==v[e[j][k]]))
							f2[j^(1<<(e[j][k]-1))]|=f2[j];
//		printf("%d %d\n",i,t);
//		for (int l=0;l<(1<<n);l++) printf("%d ",f[l]); puts("");
//		for (int l=0;l<(1<<n);l++) printf("%d ",f1[l]); puts("");
//		for (int l=0;l<(1<<n);l++) printf("%d ",f2[l]); puts("");
	}
//	for (int i=1;i<=n;i++) printf("%d ",v[i]); puts("UPD");
	for (int i=0;i<=n;i++)
		d[i]=v[i];
	return;
}

inline void dfs(int x,int y)
{
	if (x==n)
	{
		if (y==k)
			calc();
		return;
	}
	if ((n==14) && (k>=5) && (k<=8) && (d[0]!=-1)) return;
	dfs(x+1,y);
	c[x+1]=c[x+1]-16;
	c[x]=c[x]+1;
	dfs(x+1,y+1);
	c[x+1]=c[x+1]+16;
	c[x]=c[x]-1;
	return;
}

int main()
{
	freopen("hex.in","r",stdin);
	freopen("hex.out","w",stdout);
	scanf("%s",c+1);
	n=strlen(c+1);
	for (i=1;i<=n;i++)
	{
		if (c[i]<58)
			c[i]=c[i]-48;
		else
			c[i]=c[i]-87;
		t=t+c[i];
	}
	while (t%16)
		k++,t=t-15;
//	printf("%d %d\n",k,t);
	if ((k>=n) || (t))
		return puts("NO"),0;
	d[0]=-1;
	for (i=0;i<(1<<n);i++)
		for (j=1;j<=n;j++)
			if ((1<<(j-1))&i)
				e[i][0]++,e[i][e[i][0]]=j;
			else
				u[i][0]++,u[i][u[i][0]]=j;
	dfs(1,0);
	if (d[0]==-1)
		return puts("NO"),0;
//	for (i=1;i<=n;i++) printf("%d ",d[i]); puts("");
	for (i=1;i<=n;i++)
	{
		if (d[i]<10)
			d[i]=d[i]+48;
		else
			d[i]=d[i]+87;
		putchar(d[i]);
	}
	return 0;
}
