#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,m,nedge=0,p[200010],nex[200010],head[200010],ans=0;
bool vis[10010];
map<int,bool>mp;
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(int x){
	ans++;vis[x]=1;
	for(int k=head[x];k;k=nex[k])if(!vis[p[k]])dfs(p[k]);
	vis[x]=0;
}
inline int cal(int x){
	int l=x,r=x,ans=0;
	while(r<=n){
		ans+=r-l+1;
		l*=2;r=r*2+1;
	}
	if(l<=n)ans+=n-l+1;
	return ans;
}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();m=read();
	if(n<20){
		for(int i=2;i<=n;i++){
			addedge(i/2,i);addedge(i,i/2);
		}
		for(int i=1;i<=m;i++){
			int x=read(),y=read();
			addedge(x,y);addedge(y,x);
		}
		for(int i=1;i<=n;i++)dfs(i);
		writeln(ans);return 0;
	}
	ans=n*n%MOD;
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		for(int X=x;X;X/=2)mp[X]=1;
		int lca=y;
		for(;lca;lca/=2){
			if(mp[lca])break;
			mp[lca]=1;
		}
		int sum=0,la=0,cnt=0;
		for(;x!=lca;x/=2){
			int s=cal(x)%MOD;
			if(la==x*2)s=(s-cal(x*2)%MOD+MOD)%MOD;
			else if(la==x*2+1)s=(s-cal(x*2+1)%MOD+MOD)%MOD;
			la=x;p[++cnt]=s;sum=(sum+s)%MOD;
		}
		la=0;
		for(;y!=lca;y/=2){
			int s=cal(y)%MOD;
			if(la==y*2)s=(s-cal(y*2)%MOD+MOD)%MOD;
			else if(la==y*2+1)s=(s-cal(y*2+1)%MOD+MOD)%MOD;
			la=y;p[++cnt]=s;sum=(sum+s)%MOD;
		}
		int s=cal(lca)%MOD;s=(n%MOD-s+1+MOD)%MOD;
		if(!mp[lca*2])s=(s+cal(lca*2))%MOD;
		if(!mp[lca*2+1])s=(s+cal(lca*2+1))%MOD;
		p[++cnt]=s;sum=(sum+s)%MOD;
		for(int i=1;i<=cnt;i++)ans=(ans+p[i]*((sum-p[i]+MOD)%MOD)%MOD)%MOD;
		mp.clear();
	}
	writeln(ans);
	return 0;
}