#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,a[100010];
inline int mi(int a,int b){
	int x=1,y=a;
	while(b){
		if(b&1)x=x*y%MOD;y=y*y%MOD;b>>=1;
	}
	return x;
}
map<int,int>mp;
bool b[1000010];
int pri[1000010],Cnt=0;
inline void get(){
	b[1]=1;
	for(int i=2;i<=500000;i++){
		if(!b[i])pri[++Cnt]=i;
		for(int j=1;j<=Cnt;j++){
			if(i*pri[j]>500000)break;
			b[i*pri[j]]=1;
			if(i%pri[j]==0)break;
		}
	}
}
signed main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int n=0;get();
	for(int t=read();t;t--){
		int op=read(),x=read();
		if(op==1)a[++n]=x;
		else{
			int ans=1;
			for(int i=x;i<=n;i++){
				int maxx=a[i],lcm=1;
				mp.clear();
				for(int j=i;j<=n;j++){
					for(int i=1,p=a[j];i<=Cnt&&pri[i]<=a[j]&&p;i++)if(p%pri[i]==0){
						int cnt=0;
						while(p%pri[i]==0)p/=pri[i],cnt++;
						if(cnt>mp[i])lcm=lcm*mi(pri[i],cnt-mp[i])%MOD,mp[i]=cnt;
					}
					maxx=max(maxx,a[j]);
					ans=ans*mi(lcm,maxx)%MOD;
				}
			}
			writeln(ans);
		}
	}
	return 0;
}