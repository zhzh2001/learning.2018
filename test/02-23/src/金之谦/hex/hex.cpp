#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
char s[20],p[20],q[20];
int n,flag,ans[20],rk[20];
inline void check(){
	bool kp=0;
	for(int i=n;i;i--){
		rk[i]=(ans[i]+s[i]+kp)%16;
		kp=(ans[i]+s[i]+kp)/16;
	}
	if(kp)return;
	memset(p,0,sizeof p);memset(q,0,sizeof q);
	for(int i=1;i<=n;i++){
		p[ans[i]]++;q[rk[i]]++;
	}
	for(int i=0;i<16;i++)if(p[i]!=q[i])return;
	flag=1;
}
inline void dfs(int x){
	if(flag)return;
	if(x==n+1){check();return;}
	for(int i=0;i<16;i++){
		ans[x]=i;dfs(x+1);
		if(flag)return;
	}
}
inline void dfss(int x,int kp){
	if(clock()>900){
		puts("NO");
		exit(0);
	}
	int cnt=0;
	for(int i=0;i<16;i++)if(p[i]>0)cnt+=p[i];
	if(cnt>x)return;
	if(flag)return;
	if(!x){check();return;}
	int rp=rand()%2;
	if(rp){
		for(int i=15;i>=0;i--){
			ans[x]=i;int pk=(i+s[x]+kp)%16,Kp=(i+s[x]+kp)/16;
			p[i]--;p[pk]++;dfss(x-1,Kp);p[i]++;p[pk]--;
			if(flag)return;
		}
	}else{
		for(int i=0;i<16;i++){
			ans[x]=i;int pk=(i+s[x]+kp)%16,Kp=(i+s[x]+kp)/16;
			p[i]--;p[pk]++;dfss(x-1,Kp);p[i]++;p[pk]--;
			if(flag)return;
		}
	}
}
int main()
{
	freopen("hex.in","r",stdin);
	freopen("hex.out","w",stdout);
	scanf("%s",s+1);n=strlen(s+1);
	for(int i=1;i<=n;i++){
		if(s[i]>='0'&&s[i]<='9')s[i]-='0';
		else s[i]=s[i]-'a'+10;
	}
	if(n<=5)dfs(1);
	else{
		srand(time(0));
		dfss(n,0);
	}
	if(!flag)return puts("NO")&0;
	for(int i=1;i<=n;i++){
		if(ans[i]<=9)putchar(ans[i]+'0');
		else putchar(ans[i]-10+'a');
	}
	return 0;
}
