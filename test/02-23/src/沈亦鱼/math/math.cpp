#include<cstdio>
int n,m,q,k,b,mo=1000000007;
long long s,a[11000],lc[1100][1100],ma[1100][1100],ff[510000],p[510000];
long long po(long long x,long long k){
	long long ss=1;
	while(k){
		if(k%2){
			ss*=x;
			ss%=mo;
		}
		k/=2;
		x=x*x%mo;
	}
	return ss;
}
long long max(long long x,long long y){
	if(x>y)return x;
	else return y;
}
long long gcd(long long x,long long y){
	if(!y)return x;
	else return gcd(y,x%y);
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ff[1]=1;
	int i=2;
	int j;
	while(i<500000){
		while(ff[i]&i<=500000)i++;
		if(i>500000)break;
		m++;
		p[m]=i;
		j=i+i;
		while(j<=500000){
			ff[j]=1;
			j=j+i;
		}
		i++;
	}
	scanf("%d",&n);
	k=0;
	for(int i=1;i<=n;i++){
		scanf("%d%d",&q,&b);
		if(q==1){
			k++;
			a[k]=b;
			lc[k][k]=b;
			ma[k][k]=b;
			for(int i=k-1;i>0;i--){
				ma[i][k]=max(ma[i][k-1],a[k]);
				lc[i][k]=lc[i][k-1]*a[k]/gcd(lc[i][k-1],a[k]);
			}
		}
		else{
			s=1;
			for(int x=b;x<=k;x++)
				for(int y=x;y<=k;y++){
					s=s*po(lc[x][y],ma[x][y])%mo;
				}
/*			if(q+b==4){
				for(int i=1;i<=k;i++)
					printf("%d %lld\n",a[i],lc[2][i]);
			}*/
			printf("%lld\n",s);
		}
	}
	return 0;
}
