#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <set>
using namespace std;
typedef long long LL;
const LL md=1000000007;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL n,m,ans=0,a[10],b[10],c[100003],d[100003],ct=0,x,y,z;
bool vis[100003];
set<LL>isp;
LL gcnt(LL p){
	if((p<<1)>n)return 1;
	LL x=p,y=p,ans=0;
	while((y<<1|1)<=n)y=y<<1|1;
	while(x<<1<=n)x<<=1;
	if(x>y){
		ans=n-x+1;
		x>>=1;
	}
	ans+=(y-x+1<<1)-1;
	return ans;
}
void add(LL x){
	while(x>0){
		if(isp.count(x)==0){
			c[++ct]=x;
			isp.insert(x);
		}
		x>>=1;
	}
}
void dfs(LL u,LL cp){
	LL x=lower_bound(c+1,c+ct+1,u)-c,y;
	if(vis[x])return;
	vis[x]=1;
	ans=(ans+cp*d[x])%md;
	y=c[x]<<1;
	if(isp.count(y)>0)dfs(y,cp);
	if(isp.count(y+1)>0)dfs(y+1,cp);
	if(c[x]>1)dfs(c[x]>>1,cp);
	for(int i=1;i<=m;i++){
		if(a[i]==u)dfs(b[i],cp);
		if(b[i]==u)dfs(a[i],cp);
	}
	vis[x]=0;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();m=read();
	if(m==0){printf("%lld",n*n%md);return 0;}
	for(int i=1;i<=m;i++){a[i]=read();b[i]=read();add(a[i]);add(b[i]);}
	sort(c+1,c+ct+1);
	for(int i=1;i<=ct;i++){
		d[i]=1;y=c[i]<<1;
		if(isp.count(y)==0&&y<=n)d[i]+=gcnt(y);
		if(isp.count(y+1)==0&&y<n)d[i]+=gcnt(y+1);
	}
	for(int i=1;i<=ct;i++)dfs(c[i],d[i]);
	printf("%lld\n",ans);
	return 0;
}
