#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
typedef long long LL;
using std::sort;
char c[23];
LL n,v,x,y,z,q1[23],q2[23],k1,k2,ans=-0xbadbeef;
int main(){
	freopen("hex.in","r",stdin);
	freopen("hex.out","w",stdout);
	scanf("%s",c);
	n=strlen(c);
	if(n>6){
		puts("NO");
		return 0;
	}
	v=0;
	for(LL i=0;i<n;i++){
		v*=0x10;
		if(isdigit(c[i])){
			v+=c[i]-'0';
		}else{
			v+=0xa+c[i]-'a';
		}
	}
	x=0x1;
	{
		LL p=n;
		while(p--)x*=0x10;
	}
	x-=v;
	for(LL i=0;i<x;i++){
		k1=0;
		z=i;
		while(z>0){
			q1[++k1]=z%0x10;
			if(q1[k1]==0)k1--;
			z/=0x10;
		}
		k2=0;
		z=i+v;
		while(z>0){
			q2[++k2]=z%0x10;
			if(q2[k2]==0)k2--;
			z/=0x10;
		}
		if(k1==k2){
			bool fla=1;
			sort(q1+1,q1+k1+1);
			sort(q2+1,q2+k2+1);
			for(int j=1;j<=k1;j++)if(q1[j]!=q2[j])fla=0;
			if(fla){
				ans=i;
				break;
			}
		}
	}
	x+=v;
	if(ans==-0xbadbeef){
		puts("NO");
	}else
	for(int i=1;i<=n;i++){
		ans%=x;
		x/=0x10;
		z=ans/x;
		if(z<10)putchar(z+'0');
		else putchar(z-10+'a');
	}
	return 0;
}
