#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(ll i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
int opt[200001],sta[200001],mx[200001],Q,x[200001],cnt[200001],top;
ll pre[200001],lcm[200001];
ll mo=1e9+7;
inline ll gcd(ll x,ll y){return y==0?x:gcd(y,x%y);}
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2,x=x*x%mo)if(y&1)	sum=sum*x%mo; return sum;}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	Q=read();
	For(i,1,Q)
	{
		opt[i]=read();
		x[i]=read();
		cnt[opt[i]]++;
	}
	For(T,1,Q)
	{
		if(opt[T]==1)
		{
			sta[++top]=x[T];
			lcm[top]=mx[top]=x[T];
			pre[top]=ksm(lcm[top],mx[top]);
			Dow(i,1,top-1)
			{
				lcm[i]=(lcm[i]*sta[top])/gcd(lcm[i],sta[top]);
				mx[i]=max(mx[i],sta[top]);
				pre[i]=pre[i]*ksm(lcm[i],1LL*mx[i])%mo;
			}
		}
		else
		{
			ll ans=1;
			Dow(i,x[T],top)	ans=ans*pre[i]%mo;//cout<<ans<<' '<<pre[i]<<' '<<ans*pre[i]<<' '<<ans*pre[i]%mo<<endl;
			writeln(ans);
		
		}
	}
}
