#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
char s[20],to[20];
int t[20],c[20],a[20],b[20],len;
bool vis[20];
int tim;
inline void Check()
{
	tim++;if(tim>=1000000)	{puts("NO");exit(0);}
	For(i,1,len)
		c[i]=b[i]-a[i];
	For(i,1,len)
		if(c[i]<0)	c[i+1]--,c[i]+=16;
	if(c[len+1]==-1)	return;	
	For(i,1,len)	if(to[c[i]]!=s[len-i+1])	return;	
	Dow(i,1,len)	putchar(to[a[i]]);
	exit(0);
}
inline void Dfs1(int x)
{
	if(x==len+1)
	{
		Check();
		return;
	}
	For(i,1,len)	
	{
		if(!vis[i])	b[len-x+1]=a[i];else continue;
//		Dow(j,1,len)	if(a[j]>b[j])	return; else	if(a[j]<b[j])	break;
		vis[i]=1,Dfs1(x+1),vis[i]=0;
	}
}
inline void Dfs(int x)
{
	if(x==len+1){Dfs1(1);return;}
	For(i,0,15)	a[len-x+1]=i,Dfs(x+1);
}
int main()
{
	freopen("hex.in","r",stdin);freopen("hex.out","w",stdout);
	scanf("\n%s",s+1);
	For(i,0,9)	to[i]=i+'0';
	to[10]='a';to[11]='b';to[12]='c';to[13]='d';to[14]='e';to[15]='f';
	len=strlen(s+1);
	For(i,0,15)	t[i]=i;
	if(len>5)	random_shuffle(&t[0],&t[15+1]);
	Dfs(1);
	puts("NO");
}
