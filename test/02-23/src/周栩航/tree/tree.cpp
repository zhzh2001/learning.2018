#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
int vis[2000001],q[20001],top,x[2001],y[2001],siz[2000001],tsiz[2000001],n,m;
int mo=1e9+7;
int main()
{
	freopen("tree.in","r",stdin);freopen("tree.out","w",stdout);
	n=read();m=read();
	if(m==0)	{cout<<1LL*n*n%mo;return 0;}
	For(i,1,m)	x[i]=read(),y[i]=read();
	For(i,1,n)	siz[i]=1;
	Dow(i,2,n)	siz[i/2]+=siz[i];
	For(i,1,n)	tsiz[i]=siz[i]-1;
	if(m==1||m==4)
	{
		int lca=0;
		int tx=x[1];
		while(1)
		{
			vis[tx]=1;
			if(tx==1)	break;
			tx/=2;	
		}	
		tx=y[1];
		while(1)
		{
			if(vis[tx])	{lca=tx;break;}
			tx/=2;
		}
		tx=x[1];
		while(1)
		{
			if(tx==lca)	break;
			tsiz[tx/2]-=siz[tx];q[++top]=tx;
			tx/=2;
		}
		tx=y[1];
		while(1)
		{
			if(tx==lca)	break;
			tsiz[tx/2]-=siz[tx];q[++top]=tx;
			tx/=2;
		}
		ll ans=0;
		For(i,1,top)	ans+=1LL*tsiz[q[i]]*(tsiz[q[i]]+1)%mo,ans%=mo;
		For(i,1,top)	ans+=1LL*tsiz[q[i]]*(n-tsiz[q[i]]-1)*(m+1)%mo,ans%=mo;
		ll tmp=siz[1]-siz[lca];
		ans+=tmp*tmp%mo;ans%=mo;
		ans+=tmp*(siz[1]-tmp)*(m+1)%mo,ans%=mo;
		ans+=(top+1)*(top)*(m+1)%mo,ans%=mo;
		ans+=n;ans%=mo;
		writeln(ans);
	}
}
