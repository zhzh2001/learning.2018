#include<vector>
#include<cstdio>
#define pa pair<int,int>
#define mo 1000000007
#define N 100005
using namespace std;
namespace WYS_ShenLi{
	#define gc() getchar() 
	inline int read(){
		int x=0;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc());
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x; 
	}
}
using namespace WYS_ShenLi;
int n,top,cnt,ans,Q;
int a[N/2],q[N/2];
int p[10],num[10];
vector<pa> vec[N*5];
inline int power(int x,int y){
	int s=1;
	for (;y;){
		if (y&1) s=1ll*s*x%mo;
		if (y/=2) x=1ll*x*x%mo;
	}
	return s;
}
struct WoHaoCaiA{
	#define M 200000 
	int Mtg[M],Btg[M],Ctg[M],n;
	int Sum[M],Mul[M],Ans[M],AlbSum[M];
	int Alb[M],AlbBas[M],AlbMul[M];
	void build(int k,int l,int r){
		Mtg[k]=Mul[k]=Ans[k]=Alb[k]=1;
		Ctg[k]=Btg[k]=Sum[k]=0;
		AlbBas[k]=0; AlbMul[k]=1;
		AlbSum[k]=1;
		if (l==r) return;
		int mid=(l+r)/2;
		build(k*2,l,mid);
		build(k*2+1,mid+1,r);
	}
	void Mult(int k,int v,int sz){
		Mtg[k]=1ll*Mtg[k]*v%mo;
		Mul[k]=1ll*Mul[k]*power(v,sz)%mo;
		Ans[k]=1ll*Ans[k]*power(v,Sum[k])%mo;
	}
	void AddBas(int k,int v,int sz){
		Ans[k]=1ll*Ans[k]*power(Mul[k],v)%mo;
		Sum[k]=(Sum[k]+1ll*v*sz)%(mo-1);
		Btg[k]=(Btg[k]+v)%(mo-1);
	}
	void Cheng(int k,int c,int mul,int bas,int sum,int sz){
		Alb[k]=1ll*Alb[k]*power(Ans[k],c)%mo;
		Alb[k]=1ll*Alb[k]*power(Mul[k],bas)%mo;
		Alb[k]=1ll*Alb[k]*power(mul,sz)%mo;
		Alb[k]=1ll*Alb[k]*power(sum,Sum[k])%mo;
		Ctg[k]+=c;
		AlbBas[k]=(AlbBas[k]+bas)%(mo-1);
		AlbBas[k]=(AlbBas[k]+1ll*c*Btg[k])%(mo-1);
		AlbMul[k]=1ll*AlbMul[k]*mul%mo;
		AlbMul[k]=1ll*AlbMul[k]*power(Mtg[k],bas)%mo;
		AlbMul[k]=1ll*AlbMul[k]*power(Mtg[k],1ll*c*Btg[k]%(mo-1))%mo;
		AlbMul[k]=1ll*AlbMul[k]*power(1ll*sum%mo,Btg[k])%mo;
		AlbSum[k]=1ll*AlbSum[k]*sum%mo;
		AlbSum[k]=1ll*AlbSum[k]*power(Mtg[k],c)%mo;
	}
	void pushdown(int k,int l,int r){
		if (l==r) return; 
		int mid=(l+r)/2;
		if (Ctg[k]){
			Cheng(k*2,Ctg[k],AlbMul[k],AlbBas[k],AlbSum[k],mid-l+1);
			Cheng(k*2+1,Ctg[k],AlbMul[k],AlbBas[k],AlbSum[k],r-mid);
			Ctg[k]=0; AlbMul[k]=1; AlbBas[k]=0; AlbSum[k]=1;
		}
		if (Btg[k]!=0){
			AddBas(k*2,Btg[k],mid-l+1);
			AddBas(k*2+1,Btg[k],r-mid);
			Btg[k]=0;
		}
		if (Mtg[k]!=1){
			Mult(k*2,Mtg[k],mid-l+1);
			Mult(k*2+1,Mtg[k],r-mid);
			Mtg[k]=1;
		}
	}
	void pushup(int k){
		Mul[k]=1ll*Mul[k*2]*Mul[k*2+1]%mo;
		Sum[k]=(Sum[k*2]+Sum[k*2+1])%(mo-1);
		Ans[k]=1ll*Ans[k*2]*Ans[k*2+1]%mo;
		Alb[k]=1ll*Alb[k*2]*Alb[k*2+1]%mo;
	}
	void init(int x){
		build(1,1,n=x);
	}
	void mult(int k,int l,int r,int x,int y,int v){
		if (l==x&&r==y){
			Mult(k,v,r-l+1);
			return;
		}
		pushdown(k,l,r);
		int mid=(l+r)/2;
		if (y<=mid) mult(k*2,l,mid,x,y,v);
		else if (x>mid) mult(k*2+1,mid+1,r,x,y,v);
		else mult(k*2,l,mid,x,mid,v),mult(k*2+1,mid+1,r,mid+1,y,v);
		pushup(k);
	}
	void AddBas(int k,int l,int r,int x,int y,int v){
		if (l==x&&r==y){
			AddBas(k,v,r-l+1);
			return;
		}
		pushdown(k,l,r);
		int mid=(l+r)/2;
		if (y<=mid) AddBas(k*2,l,mid,x,y,v);
		else if (x>mid) AddBas(k*2+1,mid+1,r,x,y,v);
		else AddBas(k*2,l,mid,x,mid,v),
			 AddBas(k*2+1,mid+1,r,mid+1,y,v);
		pushup(k);
	}
	void save(int k,int l,int r,int x,int y){
		if (l==x&&r==y){
			Cheng(k,1,1,0,1,r-l+1);
			return;
		}
		pushdown(k,l,r);
		int mid=(l+r)/2;
		if (y<=mid) save(k*2,l,mid,x,y);
		else if (x>mid) save(k*2+1,mid+1,r,x,y);
		else save(k*2,l,mid,x,mid),
			 save(k*2+1,mid+1,r,mid+1,y);
		pushup(k);
	}
	int ask(int k,int l,int r,int x,int y){
		if (l==x&&r==y) return Alb[k];
		int mid=(l+r)/2; pushdown(k,l,r);
		if (y<=mid) return ask(k*2,l,mid,x,y);
		if (x>mid) return ask(k*2+1,mid+1,r,x,y);
		return 1ll*ask(k*2,l,mid,x,mid)*ask(k*2+1,mid+1,r,mid+1,y)%mo;
	}
	void mult(int l,int r,int v){
		if (v==1) return;
		mult(1,1,n,l,r,v);
	}
	void addbas(int l,int r,int v){
		if (v==0||l>r) return;
		AddBas(1,1,n,l,r,v);
	}
	void save(int r){
		if (r<1) return;
		save(1,1,n,1,r);
	}
	int work(int r){
		if (r<1) return 1;
		return ask(1,1,n,1,r);
	}
}IGVA;
void insert(){
	int tmp=a[++n]=read();
	cnt=0;
	for (int j=2;j*j<=tmp;j++)
		if (tmp%j==0){
			p[++cnt]=j; num[cnt]=0;
			for (;tmp%j==0;tmp/=j)
				num[cnt]++;
		}
	if (tmp!=1){
		p[++cnt]=tmp;
		num[cnt]=1;
	}
	for (int j=1;j<=cnt;j++){
		if (!vec[p[j]].size())
			vec[p[j]].push_back(pa(0,1e9));
		int sz=vec[p[j]].size()-1; 
		int la=n; vec[p[j]].push_back(pa(n,0));
		for (;;sz--){
			IGVA.mult(vec[p[j]][sz].first+1,vec[p[j]][sz+1].first,power(p[j],num[j]-vec[p[j]][sz+1].second));
			if (vec[p[j]][sz].second>num[j]) break;
		}
		vec[p[j]].resize(sz+1);
		vec[p[j]].push_back(pa(n,num[j]));
	}
	IGVA.addbas(n,n,a[n]);
	for (;;top--){
		if (a[q[top]]>a[n]) break;
		IGVA.addbas(q[top-1]+1,q[top],a[n]-a[q[top]]);
	}
	q[++top]=n;
	IGVA.save(n);
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout); 
	IGVA.init(50000);
	Q=read(); 
	a[0]=1e9; q[top=1]=0;
	while (Q--){
		int fl=read();
		if (fl==1) insert(); 
		else{
			int x=read();
			printf("%lld\n",1ll*power(IGVA.work(x-1),mo-2)*IGVA.work(n)%mo);
		}
	}
}
