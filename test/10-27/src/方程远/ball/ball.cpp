#include<bits/stdc++.h>
#define int long long
using namespace std;
int n,tot,sum,ans;
int a[121],b[121],num[121],next[121],used[121],flag[121];
void search(int x){
	if(next[num[x]]==num[x]){
		ans=max(sum,ans);
		return;
	}
	if(flag[num[x]]){
		search(num[x]+1);
		return;
	}
	tot=a[num[x]];
	while(tot){
		flag[next[num[x]]]++;
		next[num[x]]=next[next[num[x]]];
		tot--;
	}
	int X=b[num[x]];
	sum+=X;
	search(num[x]+1);
	sum-=X;
}
void prepare(int x){
	if(x>n){
		for(int i=1;i<=n;i++){
			next[i]=i+1;
			flag[i]=0;
		}
		next[n]=1;
		search(1);
		return;
	}
	for(int i=1;i<=n;i++)
		if(!used[i]){
			num[x]=i;
			used[i]++;
			prepare(x+1);
			used[i]--;
		}
}
signed main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	scanf("%lld",&n);
	for(int i=1;i<=n;i++)
		scanf("%lld%lld",&a[i],&b[i]);
	prepare(1); 
	printf("%lld",&ans);
	return 0;
}
