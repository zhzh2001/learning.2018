#include<bits/stdc++.h>
using namespace std;
#define int long long
const int M=998244353,N=200005;
int ne[N],fi[N],ans,zz[N],s1,s2,d[N],num,f[N];
int ff[N],n,a[N],b[N],l[N],r[N],fa[N];
void jb(int x,int y){
	ne[++num]=fi[x];
	fi[x]=num;
	zz[num]=y;
}
int read(){
	int x=0;char c;
	for (;c<'0'||c>'9';c=getchar());
	for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
	return x;
}
int ksm(int x,int y){
	if (!y)return 1;
	int z=ksm(x,y/2);
	z*=z;z%=M;
	if (y&1)z*=x;
	return z%M;
}
void dfs1(int x,int y){
	l[x]=a[x];r[x]=b[x];fa[x]=y;
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=y){
			dfs1(zz[i],x);
			l[x]+=l[zz[i]];
			r[x]+=r[zz[i]];
		}
	if (x==1)return;
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=y)(f[x]+=f[zz[i]])%=M;
	(f[x]+=d[x])%=M;
}
void dfs2(int x){
	int sum=0;
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]==fa[x])(sum+=ff[i])%=M;
		else (sum+=f[zz[i]])%=M;
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=fa[x])ff[i]=ff[i^1]=(sum-f[zz[i]]+d[x]+M)%M;
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=fa[x])dfs2(zz[i]);	
}
void dfs3(int x){
	if (x!=1)(ans+=f[x]*l[x]%M*(s2-r[x])%M)%=M;
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=fa[x]){
			dfs3(zz[i]);
			(ans+=(s1-l[zz[i]])%M*r[zz[i]]%M*ff[i]%M)%=M;
		}
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();num=1;
	for (int i=1;i<n;i++){
		int x=read(),y=read();
		jb(x,y);jb(y,x);
		d[x]++;d[y]++;
	}
	for (int i=1;i<=n;i++){
		a[i]=read();
		b[i]=read();
		s1+=a[i];
		s2+=b[i];
	}
	s1=ksm(s1,M-2);s2=ksm(s2,M-2);
	dfs1(1,0);dfs2(1);dfs3(1);
	printf("%lld",ans*s1%M*s2%M);
	return 0;
}
