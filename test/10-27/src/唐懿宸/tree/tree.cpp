#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<string>
#include<cctype>
#include<queue>
#include<map>
#include<vector>
#include<set>
#include<cmath>
#include<ctime>
using namespace std;

namespace TYC
{
	typedef long long ll;
	const int p=998244353;
	const int N=100010;
	int n,cnt,ed,Head[N],deg[N],vis[N];
	ll sum,Ans,invdeg[N],ps[N],pt[N];
	
	inline int read()
	{
		int x=0,f=0;char ch=getchar();
		while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
		while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
		return f?-x:x;
	}
	
	struct edge
	{
		int to,next;
	}E[N<<1];
	
	inline void add(int u,int v)
	{
		E[++cnt]=(edge){v,Head[u]};
		Head[u]=cnt;
	}
	
	inline ll qpow(ll x,ll tim)
	{
		ll ans=1;
		while(tim)
		{
			if(tim&1) ans=ans*x%p;
			x=x*x%p;
			tim>>=1;
		}
		return ans;
	}
	
	inline ll inv(ll x)
	{
		return qpow(x,p-2);
	}

	namespace Subtask1
	{
		void work()
		{
			Ans=(ps[1]*pt[2]%p+ps[2]*pt[1]%p)%p;
			printf("%lld\n",Ans);
		}
	}

	void dfs(int u,ll P,ll len)
	{
		if(u==ed) {sum=(len*P%p+sum)%p;return;}
		vis[u]++;
		if(vis[u]>2*n) return;
		for(int i=Head[u];i;i=E[i].next)
			dfs(E[i].to,P*invdeg[u]%p,len+1);
	}
	
	void work()
	{
		n=read();
		for(int i=1;i<n;i++)
		{
			int u=read(),v=read();
			add(u,v),add(v,u);
			deg[u]++,deg[v]++;
		}
		
		ll Start=0,End=0;
		for(int i=1;i<=n;i++)
		{
			ps[i]=read(),pt[i]=read();
			Start+=ps[i],End+=pt[i];
		}
		ll invS=inv(Start),invE=inv(End);
		for(int i=1;i<=n;i++)
		{
			ps[i]=(ll)ps[i]*invS%p;
			pt[i]=(ll)pt[i]*invE%p;
			invdeg[i]=inv(deg[i]);
		}
		
		if(n==2) {Subtask1::work();return;}
		
		for(int i=1;i<=n;i++)
			if(pt[i])
			{
				ed=i;
				for(int s=1;s<=n;s++)
					if(ps[s])
					{
						sum=0;
						memset(vis,0,sizeof(vis));
						dfs(s,1,0);
						Ans=((ll)sum*ps[s]%p*pt[i]%p+Ans)%p;
					}
			}
		printf("%lld\n",Ans);
	}
}

int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	TYC::work();
	return 0;
}
