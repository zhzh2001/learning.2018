#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<string>
#include<cctype>
#include<queue>
#include<map>
#include<vector>
#include<set>
#include<cmath>
#include<ctime>
using namespace std;

namespace TYC
{
	const int N=5010,MX=(1<<18)+100;
	int n,ans,tot,a[N],b[N],bin[N],list[N],dp[MX];

	inline int read()
	{
		int x=0,f=0;char ch=getchar();
		while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
		while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
		return f?-x:x;
	}

	inline int count(int x)
	{
		int num=0,now=0;
		while(x)
		{
			if(x&1) list[num++]=now;
			x>>=1,now++;
		}
		return num;
	}

	void work()
	{
		n=read();
		for(int i=0;i<n;i++)
			a[i]=read(),b[i]=read();
		bin[0]=1;
		for(int i=1;i<n;i++) bin[i]=bin[i-1]<<1;
		memset(dp,-1,sizeof(dp));
		dp[(1<<n)-1]=0;
		for(int i=(1<<n)-1;i>0;i--)
			if(~dp[i])
			{
				int num=count(i);
				for(int j=0;j<num;j++)
				{
					int now=list[j];
					if(a[now]>num) continue;
					int to=i;
					for(int k=j,res=a[now];res;res--,k=(k+1)%num)
						to^=bin[list[k]];
					dp[to]=max(dp[to],dp[i]+b[now]);
				}
				ans=max(ans,dp[i]);
			}
		ans=max(ans,dp[0]);
		printf("%d\n",ans);
	}
}

int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	TYC::work();
	return 0;
}
