#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<string>
#include<cctype>
#include<queue>
#include<map>
#include<vector>
#include<set>
#include<cmath>
#include<ctime>
using namespace std;

namespace TYC
{
	namespace IO
	{
		inline int read()
		{
			int x=0,f=0;char ch=getchar();
			while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
			while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
			return f?-x:x;
		}
		inline void write(int a)
		{
		    if(a<0) a=-a,putchar('-');
		    if(!a) putchar('0');
		    else
		    {
		    	int len=0;
		    	static int bask[15];
		    	while(a) bask[++len]=a%10,a/=10;
		    	for(int i=len;i;i--)
					putchar('0'+bask[i]);
			}
		}
	}
	
	using IO::write;
	using IO::read;
	
	const int N=15,inf=0x3f3f3f3f;
	int n,m,money,Rest,AnsStep,ans=inf,tot,cost[N],times[N][N],used[100010][N];
	
	struct Situation
	{
		int people,machine,tim,times;
	}Ans[100010],Present[100010];
	
	void dfs(int tim,int now)
	{
		if(!Rest) 
		{
			if(tim<ans)
			{
				ans=tim;
				AnsStep=tot;
				memcpy(Ans,Present,sizeof(Situation[tot+1]));
			}
			return;
		}
		int flag=0;
		for(int i=1;i<=n;i++)
			if(!used[tim][i]&&times[i][now])
			{
				flag=1;
				times[i][now]--;
				used[tim][i]=1;
				Rest--;
				Present[++tot]=(Situation){i,now,tim-1,1};
				if(now==m) dfs(tim+1,1);
				else dfs(tim,now+1);
				times[i][now]++;
				used[tim][i]=0;
				Rest++;
				tot--;
			}
		if(!flag)
		{
			if(now==m) dfs(tim+1,1);
			else dfs(tim,now+1);
		}
	}
	
	void work()
	{
		n=read(),m=read(),money=read();
		for(int i=1;i<=m;i++) cost[i]=read();
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
			{
				times[i][j]=read();
				Rest+=times[i][j];
			}
		dfs(1,1);
		write(ans);putchar('\n');
		for(int i=1;i<=m;i++) putchar('0');
		putchar('\n');
		write(AnsStep);putchar('\n');
		for(int i=1;i<=AnsStep;i++)
		{
			write(Ans[i].people),putchar(' ');
			write(Ans[i].machine),putchar(' ');
			write(Ans[i].tim),putchar(' ');
			write(Ans[i].times),putchar('\n');
		}
	}
}

int main()
{
	freopen("aysgh.in","r",stdin);
	freopen("aysgh.out","w",stdout);
	TYC::work();
	return 0;
}
