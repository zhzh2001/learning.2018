#include <cstdio>
#include <cctype>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 200
int n, m, k, p[N], a[N][N];
long long s, ans;
int main(){
	freopen("aysgh.in", "r", stdin);
	freopen("aysgh.out", "w", stdout);
	n = read(), m = read(), k = read();
	for (register int i = 1; i <= m; ++i) p[i] = read();
	for (register int i = 1; i <= n; ++i)
		for (register int j = 1; j <= m; ++j)
			a[i][j] = read();
	for (register int i = 1; i <= n; ++i){
		s = 0;
		for (register int j = 1; j <= m; ++j) s += a[i][j];
		ans = std :: max(s, ans);
	}
	for (register int j = 1; j <= m; ++j){
		s = 0;
		for (register int i = 1; i <= n; ++i) s += a[i][j];
		ans = std :: max(s, ans);
	}
	printf("%lld\n", ans);
	for (register int i = 1; i <= m; ++i) putchar('0');
	putchar('\n'); printf("0\n");
}
