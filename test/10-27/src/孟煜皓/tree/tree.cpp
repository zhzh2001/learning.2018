#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
#define P 998244353
int n;
long long x[N], y[N], sx, sy, ans;
int edge, to[N << 1], pr[N << 1], hd[N];
void addedge(int u, int v){
	to[++edge] = v, pr[edge] = hd[u], hd[u] = edge;
}
long long qpow(long long a, long long b){
	long long s = 1;
	for (; b; b >>= 1, a = a * a % P) if (b & 1) s = s * a % P;
	return s;
}
int main(){
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	n = read();
	for (register int i = 1, u, v; i < n; ++i)
		u = read(), v = read(), addedge(u, v), addedge(v, u);
	for (register int i = 1; i <= n; ++i)
		sx += x[i] = read(), sy += y[i] = read();
	ans = (x[1] * y[2] + x[2] * y[1]) % P;
	ans = ans * qpow(sx * sy % P, P - 2) % P;
	printf("%lld", ans);
}
