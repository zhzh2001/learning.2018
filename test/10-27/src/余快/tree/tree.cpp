/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 998244353
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
//#define getchar gc
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,fa[N],u[N],d[N],num[N],sum[N],p[N],fx[N],fy[N],sx,sy,ssx;
//u,d 表示i这个点的向上和从上面往下走的步数的期望
int Next[N*2],nedge,head[N],to[N*2];
#define V to[i]
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;}
void add_ne(int a,int b){add(a,b);add(b,a);}
void g_u(int x){
	u[x]=1;
	go(i,x){
		if (V==fa[x]) continue;
		fa[V]=x;g_u(V);u[x]+=u[V]+1;
	}
	if (x==1) u[x]--;
}
void g_d(int x){
	go(i,x){
		if (V==fa[x]) continue;
		d[V]=u[x]-u[V]+d[x];g_d(V);
	}
}
inline int tadd(int &x,int k){x+=k;x-=(x>=mod)?mod:0;}
void dfs(int x){
	sum[x]=fx[x];
	go(i,x){
		if (V==fa[x]) continue;
		dfs(V);sum[x]+=sum[V];tadd(num[x],(num[V]+1LL*sum[V]*u[V]%mod)%mod);
	}
}
void dfs2(int x){
	tadd(ans,1LL*p[x]*ssx%mod*fy[x]%mod*sy%mod);
	go(i,x){
		if (fa[x]==V) continue;
		p[V]=p[x]-1LL*sum[V]*u[V]%mod+1LL*(sx-sum[V])*d[V]%mod;;
		p[V]=(p[V]%mod+mod)%mod;
		dfs2(V);
	}
}
int ksm(ll x,int k){
	int sum=1;
	while (k){
		if (k&1) sum=sum*x%mod;
		x=x*x%mod;k>>=1;
	}
	return sum;
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	F(i,1,n-1) add_ne(read(),read());
	F(i,1,n) fx[i]=read(),fy[i]=read(),sx+=fx[i],sy+=fy[i];
	ssx=ksm(sx,mod-2);sy=ksm(sy,mod-2);
	g_u(1);g_d(1);
	dfs(1);
	p[1]=num[1];dfs2(1);
	wrn(ans);
	return 0;
}
