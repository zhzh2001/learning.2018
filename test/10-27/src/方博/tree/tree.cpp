#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define int long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int p=998244353;
const int N=100005;
void exgcd(ll a,ll b,ll &x,ll &y){
    if(b==0){x=1;y=0;return;}
    exgcd(b,a%b,x,y);
    ll tmp=x;x=y;y=tmp-a/b*y;
}
inline ll get(ll k)
{
	ll x,y;
	exgcd(k,p,x,y);
	x=(x%p+p)%p;
	return x;
}
ll x[N],y[N],sum[N],sumx[N],sumy[N],sz[N],ff[N],f[N];
ll pa[N],inv[N];
ll head[N],nxt[N*2],tail[N*2],t,g[N];
struct xxx{
	int x,y;
}a[N];
ll sx,sy;
int n;
ll ans;
void dfs1(int now,int fa){
	f[now]=1;
	sz[now]=1;
	ff[now]=fa;
	sumx[now]=x[now]*get(sx)%p,sumy[now]=y[now]*get(sy)%p;
	for(int i=head[now];i;i=nxt[i]){
		if(tail[i]!=fa){
			dfs1(tail[i],now);
			sz[now]+=sz[tail[i]];
			sumx[now]+=sumx[tail[i]],sumy[now]+=sumy[tail[i]];
			sumx[now]%=p;
			sumy[now]%=p;
			f[now]+=f[tail[i]];
		}
	}
}
void dfs2(int x,int fa){
	double sum=0;
	sum=sum+g[fa]+1;
	for(int i=head[x];i;i=nxt[i]){
		if(tail[i]!=fa){
			sum=sum+f[tail[i]];
		}
	}
	for(int i=head[x];i;i=nxt[i]){
		if(tail[i]!=fa){
			g[tail[i]]=sum-f[tail[i]];
			dfs2(tail[i],x);
		}
	}
}
void calc(int x,int fa){
	for(int i=head[x];i;i=nxt[i]){
		if(tail[i]!=fa){
			calc(tail[i],x);
			ans+=sumx[tail[i]]*f[tail[i]]%p*(sumy[1]-sumy[tail[i]]+p)%p;
			ans+=(sumx[1]-sumx[tail[i]]+p)%p*g[tail[i]]%p*sumy[tail[i]]%p;
		}
	}
}
void addto(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	pa[0]=1;
	for(int i=1;i<=n;i++)
		pa[i]=pa[i-1]*i%p;
	inv[n]=get(pa[n]);
	for(int i=n-1;i;i--)
		inv[i]=inv[i+1]*i%p;
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addto(x,y);
		addto(y,x);
	}
	for(int i=1;i<=n;i++){
		a[i].x=read(),a[i].y=read();
		sx+=a[i].x,sy+=a[i].y;
	}
	ll ans=0;
	if(n==2){
		ans=(a[1].x*get(sx))%p*(a[2].y*get(sy))%p+(a[1].y*get(sy))%p*(a[2].x*get(sx))%p;
		ans%=p;
		writeln(ans);
	}
	else {
		dfs1(1,0),dfs2(1,0);
		calc(1,0);
		writeln(ans);
	}
}
//*��MDND 
