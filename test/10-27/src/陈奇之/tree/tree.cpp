#include<bits/stdc++.h>
using namespace std;
#define Rep(i,a,b) for(register int i=(a);i<=(b);++i)
#define rep(i,a,b) for(register int i=(a);i<(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=(b);--i)
typedef long long ll;
const int maxn = 1e5+233;
const int mod = 998244353;
struct Edge{
	int to,nxt;
	Edge(){}
	Edge(int to,int nxt):to(to),nxt(nxt){}
}edge[maxn * 2];
int first[maxn],nume;
int A[maxn],B[maxn],K[maxn],f[maxn],deg[maxn];
int n,x[maxn],y[maxn];
ll ans,X,Y;
void Addedge(int a,int b){
	edge[nume] = Edge(b,first[a]);
	first[a] = nume++;
}
inline int qpow(int a,int b){
	int ans = 1;
	for(;b;b>>=1,a=1ll*a*a%mod)	
		if(b&1) ans=1ll*ans*a%mod;
	return ans;
}
inline int inv(int x){
	return qpow(x,mod-2);
}
void dfs1(int u,int fa){
//	printf("dfs1(%d %d)\n",u,fa);
	if(deg[u] == 1 && fa != 0){
		K[u] = 1;
		B[u] = 1;
		return ;
	}
	A[u] = 0;B[u] = 0;
	for(int e=first[u];~e;e=edge[e].nxt){
		int v=edge[e].to;
		if(v==fa) continue;
		dfs1(v,u);
		A[u] = (A[u] + K[v]) % mod;
		B[u] = (B[u] + B[v]) % mod;
	}
	A[u] = 1ll * A[u] * inv(deg[u]) % mod;
	A[u] = (mod + 1 - A[u]) % mod;
	A[u] = inv(A[u]);
	
	B[u] = 1ll * B[u] * inv(deg[u]) % mod;
	B[u] = (B[u] + 1)  % mod;
	
	K[u] = 1ll * inv(deg[u]) * A[u] % mod;
	B[u] = 1ll * B[u] * A[u] % mod;
//	printf("B[%d] = %d\n",u,B[u]); 
}

void dfs2(int u,int fa){
	for(int e=first[u];~e;e=edge[e].nxt){
		int v=edge[e].to;
		if(v==fa) continue;
		f[v] = (1ll * K[v] * f[u] + B[v]) % mod;
		dfs2(v,u);
	}
}

signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d",&n);
	if(n==1) return puts("0"),0;
	memset(first,-1,sizeof(first));nume = 0;
	bool flag = true;
	rep(i,1,n){
		int a,b;
		scanf("%d%d",&a,&b);
		if(a+1!=b) flag=false;
		Addedge(a,b),Addedge(b,a);
		deg[a]++;deg[b]++;
	}
	Rep(i,1,n){
		scanf("%d%d",&x[i],&y[i]);
		X+=x[i],Y+=y[i];
	}
	if(flag){
		ll sx = 0,sv = 0;
		for(int i=1;i<=n;++i){
			ans = (ans + 1ll * sv * y[i] % mod) % mod;
			sx = sx + x[i];
			sv = (sv + 1ll * sx * (2*i-1)) % mod;
		}
		sx=0,sv=0;
		Dep(i,n,1){
			ans = (ans + 1ll * sv * y[i] % mod) % mod;
			sx = sx + x[i];
			sv = (sv + 1ll * sx * (2 * (n-i+1) - 1)) % mod;
		}
		printf("%lld\n",ans* qpow(X,mod-2) % mod * qpow(Y,mod-2) % mod);
		return 0;
	}
	ans = 0;
	Rep(i,1,n){
		dfs1(i,0);
		f[i] = 0;
		dfs2(i,0);
		Rep(j,1,n)
			ans = (ans + 1ll * x[j] * y[i] % mod * f[j] % mod) % mod;
	}
	printf("%lld\n",1ll * ans * qpow(X,mod-2) % mod * qpow(Y,mod-2) % mod);
	return 0;
}

//586
