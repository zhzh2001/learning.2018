#include<bits/stdc++.h>
using namespace std;
#define Rep(i,a,b) for(register int i=(a);i<=(b);++i)
#define rep(i,a,b) for(register int i=(a);i<(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=(b);--i)
typedef long long ll;
#define gc getchar
const int maxn = 5005;
int n,a[maxn],b[maxn],f[maxn];
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	scanf("%d",&n);
	Rep(i,1,n)
		scanf("%d%d",&a[i],&b[i]);
	memset(f,0,sizeof(f));
	f[0] = 0;
	Rep(i,1,n){
		Dep(j,n,0){
			if(j>=a[i])
				f[j] = max(f[j],f[j-a[i]]+b[i]);
		}
	}
	int ans = 0;
	Rep(i,0,n) ans=max(ans,f[i]);
	printf("%d\n",ans);
	return 0;
}
