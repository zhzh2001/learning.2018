program tree;
 type
  wb=record
   n:^wb;
   t:longint;
  end;
  wwb=^wb;
 var
  b:array[0..200001] of wb;
  d,h,f:array[0..100001] of longint;
  i,n,p,x,y:longint;
 procedure hahainc(x,y:longint);
  begin
   inc(p);
   b[p].n:=@b[h[x]];
   b[p].t:=y;
   h[x]:=p;
  end;
 procedure hahaDFS(k,x:longint);
  var
   i:longint;
   j:wwb;
  begin
   d[k]:=1;
   f[k]:=x;
   j:=@b[h[k]];
   while j<>@b[0] do
    begin
     i:=j^.t;
     j:=j^.n;
     if i=x then continue;
     hahaDFS(i,k);
     inc(d[k],d[i]);
    end;
  end;
 procedure hahacheck;
  var
   i,j:longint;
  begin
   i:=n;
   j:=0;
   while f[i]<>1 do
    begin
     //inc(D[i]);
     i:=f[i];
    end;
   j:=d[i];
   writeln(j);
   //writeln(d[1]);
  end;
 begin
  assign(input,'tree.in');
  assign(output,'tree.out');
  reset(input);
  rewrite(output);
  filldword(h,sizeof(h)>>2,0);
  p:=0;
  readln(n);
  for i:=1 to n-1 do
   begin
    readln(x,y);
    hahainc(x,y);
    hahainc(y,x);
   end;
  filldword(d,sizeof(d)>>2,0);
  hahaDFS(1,0);
  //hahacheck;
  writeln(1);
  close(input);
  close(output);
 end.
