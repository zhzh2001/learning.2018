#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <vector>
using namespace std;

namespace zuiyt
{
	const int N = 150, OPT = 2e6 + 10;
	int p[N], n, m, b, arr[N][N], sum, cnt;
	struct node
	{
		int i, j, x, y;	
	}opt[OPT];
	vector<int> g[N];
	namespace Hungary
	{
		int b[N], c[N], time;
		
		bool path(const int u)
		{
			for (int i = 0; i < g[u].size(); i++)
			{
				int v = g[u][i];
				if (c[v] == time)
					continue;
				c[v] = time;
				if (!b[v] || path(b[v]))
				{
					b[v] = u;
					return true;
				}
			}	
			return false;
		}
		void solve(const int t)
		{
			time = 0;
			memset(b, 0, sizeof(int[m + 1]));
			memset(c, 0, sizeof(int[m + 1]));
			for (int i = 1; i <= n; i++)
			{
				++time;
				path(i);
			}
			for (int i = 1; i <= m; i++)
				if (b[i])
				{
					--arr[b[i]][i], --sum;
					opt[cnt++] = (node){b[i], i, t, 1};
				}
		}
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin.tie(0);
		cin >> n >> m >> b;
		for (int i = 0; i < m; i++)
			cin >> p[i];
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++)
				cin >> arr[i][j], sum += arr[i][j];
		int t = 0;
		while (sum)
		{
			for (int i = 1; i <= n; i++)
			{
				g[i].clear();
				for (int j = 1; j <= m; j++)
					if (arr[i][j])
						g[i].push_back(j);
			}
			Hungary::solve(t++);
			if (!sum)
				break;
		}
		cout << t << '\n';
		for (int i = 0; i < m; i++)
			cout << 0;
		cout << '\n';
		cout << cnt << '\n';
		for (int i = 0; i < cnt; i++)
			cout << opt[i].i << ' ' << opt[i].j << ' ' << opt[i].x << ' ' << opt[i].y << '\n';
		return 0;
	}	
}
int main()
{
	freopen("aysgh.in", "r", stdin);
	freopen("aysgh.out", "w", stdout);
	return zuiyt::work();
}
