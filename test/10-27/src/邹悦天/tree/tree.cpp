#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

namespace zuiyt
{
	typedef long long ll;
	const int N = 1010, Jumpmelon = 998244353;
	vector<int> g[N];
	ll power(ll a, ll b, const int p = Jumpmelon)
	{
		ll ans = 1;
		while (b)
		{
			if (b & 1)
				ans = ans * a % p;
			a = a * a % p;
			b >>= 1;
		}
		return ans;
	}
	ll inv(ll a, const int p = Jumpmelon)
	{
		return power(a, p - 2, p);
	}
	int n, x[N], y[N], X, Y;
	namespace n2//n <= 2
	{
		inline int solve()
		{
			if (n <= 1)
				cout << 0;
			else
				cout << (x[1] * inv(X) % Jumpmelon * y[2] % Jumpmelon * inv(Y) % Jumpmelon + 
						x[2] * inv(X) % Jumpmelon * y[1] % Jumpmelon * inv(Y) % Jumpmelon) % Jumpmelon;
		}
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin.tie(0);
		cin >> n;
		for (int i = 1; i < n; i++)
		{
			int a, b;
			cin >> a >> b;
			g[a].push_back(b);
			g[b].push_back(a);
		}
		for (int i = 1; i <= n; i++)
			cin >> x[i] >> y[i], X += x[i], Y += y[i];
		if (n <= 2)
			n2::solve();
		return 0;	
	}	
}
int main()
{
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	return zuiyt::work();	
}
