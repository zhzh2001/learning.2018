#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>

using namespace std;

namespace zuiyt
{
	const int N = 20;
	int n, arra[N << 1], arrb[N << 1], a[N], b[N], dp[1 << N], lg2[1 << N];
	inline int lowbit(const int x)
	{
		return x & (-x);	
	}
	inline void cover(int &x, const int l, const int r)
	{
		x &= ~(((1 << (r - l + 1)) - 1) << l);	
	}
	int solve(const int st)
	{
		memcpy(a, arra + st, sizeof(int[n]));
		memcpy(b, arrb + st, sizeof(int[n]));
		memset(dp, 0, sizeof(int[1 << n]));
		static int buf[N];
		for (int i = (1 << n) - 1; i >= 0; i--)
		{
			int tmp = i, num = 0;
			while (tmp)
				buf[num++] = lg2[lowbit(tmp)], tmp -= lowbit(tmp);
			for (int j = 0; j < num; j++)
			{
				if (j + a[buf[j]] - 1 >= num)
					continue;
				int tmp = i;
				cover(tmp, buf[j], buf[j + a[buf[j]] - 1]);
				dp[tmp] = max(dp[tmp], dp[i] + b[buf[j]]);
			}
		}
		return dp[0];
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin.tie(0);
		cin >> n;
		for (int i = 0; i < n; i++)
		{
			lg2[1 << i] = i;
			cin >> arra[i] >> arrb[i], arra[i + n] = arra[i], arrb[i + n] = arrb[i];
		}
		int ans = 0;
		for (int i = 0; i < n; i++)
			ans = max(ans, solve(i));
		cout << ans;
		return 0;
	}	
}
int main()
{
	freopen("ball.in", "r", stdin);
	freopen("ball.out", "w", stdout);
	return zuiyt::work();	
}
