#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
#define go(st) for(int i=head[st],ed=E[i].ed;i;i=E[i].nxt,ed=E[i].ed)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
const ll mod=998244353;
inline ll qpow(ll a,ll x) {
	ll ans=1;
	for(;x;x>>=1,a=(a*a)%mod) if(x&1) ans=(ans*a)%mod;
	return ans;
} 
const int MAXN=200010;
int N;
struct edge {
	int ed,nxt;
}E[MAXN];
ll head[MAXN],Ecnt;
ll son[MAXN],depth[MAXN];
ll xx[MAXN],yy[MAXN],X,Y;
void addEdge(int st,int ed) {
	E[++Ecnt].ed=ed,E[Ecnt].nxt=head[st],head[st]=Ecnt;
}
ll up[MAXN],down[MAXN],uson[MAXN];
ll chx[MAXN],chy[MAXN];
void dfs_u(int st,int fa) {
	up[st]=(son[st]+1)%mod;
	go(st) if(ed!=fa){
		dfs_u(ed,st);
		chx[st]=(chx[st]+chx[ed])%mod;
		chy[st]=(chy[st]+chy[ed])%mod;
		uson[st]=(uson[st]+up[ed])%mod;
		up[st]=(up[st]+up[ed])%mod;
	}
}
void dfs_d(int st,int fa) {
	if(fa) {
		down[st]=((son[fa]+1ll+down[fa]+uson[fa]-up[st]+mod)%mod+mod)%mod;
	}
	go(st) if(ed!=fa){
		dfs_d(ed,st);
	}
}
ll ans=0;
void dfs2(int st,int fa) {
	go(st) if(ed!=fa) {
		dfs2(ed,st);
		ans+=down[ed]*(chy[ed])%mod*(mod+1-chx[ed])%mod;
		ans%=mod;
		ans+=up[ed]*(chx[ed])%mod*(mod+1-chy[ed])%mod;
		ans%=mod;
	}
}
//main========================
int main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	N=rd();
	for(int i=1;i<N;++i) {
		int x=rd(),y=rd();
		addEdge(x,y),addEdge(y,x);
		++son[x],++son[y];
	}
	for(int i=1;i<=N;++i) --son[i];
	for(int i=1;i<=N;++i) {
		int x=rd(),y=rd();
		xx[i]=x,yy[i]=y;
		X+=x,Y+=y;
	}
	for(int i=1;i<=N;++i) chx[i]=xx[i]*qpow(X,mod-2)%mod,chy[i]=yy[i]*qpow(Y,mod-2)%mod;
	depth[1]=1;
	dfs_u(1,0);up[1]=0;
	dfs_d(1,0);down[1]=0;
	dfs2(1,0);
	printf("%lld\n",(ans%mod+mod)%mod);
	return 0;
}

