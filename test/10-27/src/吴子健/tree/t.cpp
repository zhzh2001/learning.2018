#include<bits/stdc++.h>
#define ll long long
using namespace std;
const ll mod=1000000007;
const int N=100010;
int head[N],Next[N<<1],vet[N<<1],tot,n,lg;
int d[N],pre[N][20];ll up[N],down[N];
void add(int x,int y){
    tot++;
    vet[tot]=y;
    Next[tot]=head[x];
    head[x]=tot;
}//邻接表
void dfsf(int u,int fa){
    for(int i=head[u];i;i=Next[i]){
        int v=vet[i];
        if(v==fa)continue;
        dfsf(v,u);
        up[u]=(up[u]+up[v]+1)%mod;
    }
    up[u]=(up[u]+1)%mod;printf("up[%d]=%d\n",u,up[u]);
}//产生up[]数组
void dfsg(int u,int fa){
    ll sum=0;
    for(int i=head[u];i;i=Next[i]){
        int v=vet[i];
        if(v==fa)sum=(sum+down[u]+1)%mod;
        else sum=(sum+up[v]+1)%mod;
    }
    for(int i=head[u];i;i=Next[i]){
        int v=vet[i];
        if(v==fa)continue;
        down[v]=((sum-up[v])%mod+mod)%mod;
        dfsg(v,u);
    }
}//产生down[]数组
void dfs(int u,int fa,int dep){
    d[u]=dep;pre[u][0]=fa;
    up[u]=(up[fa]+up[u])%mod;
    down[u]=(down[fa]+down[u])%mod;
    for(int i=head[u];i;i=Next[i]){
        int v=vet[i];
        if(v!=fa)dfs(v,u,dep+1);
    }
}//lca的预处理
int lca(int u,int v){
    if (d[u]<d[v])swap(u,v);
    int del=d[u]-d[v];
    if (del)
        for (int i=0;i<=lg&&del;i++,del>>=1)
            if(del&1)u=pre[u][i];
    if (u==v) return u;
    for (int i=lg;i>=0;i--)
        if (pre[u][i]!=pre[v][i]){
            u=pre[u][i];
            v=pre[v][i];
        }
    return pre[u][0];
}//lca基本操作
int main(){
    int Q;scanf("%d%d",&n,&Q);
    lg=log(n)/log(2)+1;//求出最大的2^lg的lg
    for(int i=1;i<n;i++){
        int x,y;
        scanf("%d%d",&x,&y);
        add(x,y);add(y,x);
    }//构图
    dfsf(1,-1);
    up[1]=0;down[1]=0;
    dfsg(1,-1);
    dfs(1,-1,0);
    for(int i=1;i<=lg;i++)
        for(int j=1;j<=n;j++)
            pre[j][i]=pre[pre[j][i-1]][i-1];//lca预处理
    while(Q--){
        int u,v;
        scanf("%d%d",&u,&v);
        int _lca=lca(u,v);
        ll ans=(up[u]-up[_lca]+down[v]-down[_lca]+mod)%mod;//注意取模时的负数
        printf("%lld\n",ans);
    }
    return 0;
}
