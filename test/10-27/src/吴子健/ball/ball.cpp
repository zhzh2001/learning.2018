#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
ll N,a[30],b[30];
ll f[(1<<19)+1];
ll g[(1<<19)+1],h[(1<<19)+1];
//void display(int now) {
//	if(now<=0) return;
//	display(h[now]);
//	if(g[now]>=0)
//	LOG("choose %d b=%d\n",g[now],b[g[now]]);
//}
//main========================
int main() {
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	memset(g,-1,sizeof g);
	memset(h,-1,sizeof h);
	N=rd();
	for(int i=0;i<N;++i) a[i]=rd(),b[i]=rd();
	for(int i=0;i<(1<<N);++i) {
	//	LOG("consider i=%d\n",i);
		for(int j=0;j<N;++j) if(((1<<j)&i)==0) {
			//LOG("\t choose j=%d\n",j);
			if(N-__builtin_popcount(i)>=a[j]) {
				int t=i;
				for(int k=j,cnt=0;cnt<a[j];k=(k+1)%N) {
					if(((1<<k)&t)==0) {
					//	LOG("\t del k=%d\n",k );
						++cnt;
						t|=(1<<k);
					}
				}
				if(f[i]+b[j]>f[t]) {
					f[t]=f[i]+b[j];
					g[t]=j;
					h[t]=i;
				}
			}
		}
	}
	printf("%lld\n",f[(1<<N)-1]);
	//display((1<<N)-1);
	return 0;
}

