#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
int N,M,B;
int C[1000];
int a[1000][1009],remain=0; 
int ans=1e9;
bool visit[300][20];
int acl,bcl;
void dfs1(int);
void dfs2(int tim,int now,bool *vis,int redel=0) {
	bcl=clock();
	if(bcl-acl>=1.5*CLOCKS_PER_SEC) {
		return;
	} 
	if(remain==0) {
		ans=min(ans,tim);
	}
	if(now>N&&redel) {
		dfs1(tim+1);
		return;
	} else if(now>N) return;
	//LOG("tim=%d now=%d redel=%d\n",tim,now,redel);
	dfs2(tim,now+1,vis,redel);
	for(int i=1;i<=M;++i) if(!vis[i]) {
		bool deled=0;
		if(a[now][i]) --a[now][i],--remain,deled=true;
		vis[i]=true;
		//LOG("\tat time %d,the %d guy choose %d remain=%d\n",tim,now,i,remain);
		dfs2(tim,now+1,vis,redel+deled);
		vis[i]=false;
		if(deled) ++a[now][i],++remain;
	}
}
void dfs1(int cur) {
	bcl=clock();
	if(bcl-acl>=1.5*CLOCKS_PER_SEC) {
		return;
	}
	//LOG("dfs time=%d\n",cur);
	if(cur>ans) return;
	if(remain==0) {
		ans=min(cur,ans);
		return;
	}
	dfs2(cur,1,visit[cur]);
}
//main========================
int main() {
	freopen("aysgh.in","r",stdin);
	freopen("aysgh.out","w",stdout);
	acl=clock();
	N=rd(),M=rd(),B=rd();
	for(int i=1;i<=M;++i) C[i]=rd();
	for(int i=1;i<=N;++i) 
		for(int j=1;j<=M;++j) 
			a[i][j]=rd(),remain+=a[i][j];
	int t=remain;
	//	cout<<remain<<endl;
	dfs1(0);
	printf("%d\n",ans);	
	for(int i=1;i<=M;++i) printf("%d",0);
	printf("\n%d\n",t);
	for(int i=1;i<=t;++i) {
		printf("%d %d %d %d\n",rand(),rand(),rand(),rand());
	}
	return 0;
}

