#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,a[18],b[18],t,p,q,f[1<<18];
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n=read();
	for(int i=0;i<n;i++){
		a[i]=read();
		b[i]=read();
	}
	f[0]=0;
	for(int i=1;i<(1<<n);i++){
		t=0;
		for(int j=0;j<n;j++)if(i&(1<<j))t++;
		for(int j=0;j<n;j++)if(i&(1<<j)){
			if(a[j]<=t){
				p=i;
				q=a[j];
				for(int k=j;k<n&&q>0;k++)if(p&(1<<k)){
					p^=1<<k;
					q--;
				}
				for(int k=0;k<j&&q>0;k++)if(p&(1<<k)){
					p^=1<<k;
					q--;
				}
				f[i]=max(f[i],f[p]+b[j]);
			}
		}
	}
	printf("%d\n",f[(1<<n)-1]);
	return 0;
}
