#include <iostream>
#include <algorithm>
#include <cstdio>
#include <ctime>
#include <utility>
#include <vector>
#include <cstring>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,m,b,cs[53],a[53][53],p,x,tot,ans;
bool t1[53][503],t2[53][503];
vector<pair<int,int> >ar,asr;
template <typename T>
void random_shuffle(vector<T>&a){
	int n=a.size();
	for(int i=0;i<n;i++){
		swap(a[i],a[rand()%n]);
	}
}
int ga(int x,int y){
	int ans=1;
	while(t1[x][ans]||t2[y][ans])ans++;
	return ans;
}
int main(){
	freopen("aysgh.in","r",stdin);
	freopen("aysgh.out","w",stdout);
	srand(time(NULL));
	n=read();
	m=read();
	b=read();
	p=0;
	for(int i=1;i<=m;i++){
		cs[i]=read();
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			a[i][j]=read();
			p+=a[i][j];
			for(int k=1;k<=a[i][j];k++){
				ar.push_back(make_pair(i,j));
			}
		}
	}
	ans=50;
	for(int i=1;i<=10000;i++){
		random_shuffle(ar);
		memset(t1,0,sizeof(t1));
		memset(t2,0,sizeof(t2));
		tot=0;
		for(int i=0;i<p;i++){
			x=ga(ar[i].first,ar[i].second);
			t1[ar[i].first][x]=1;
			t2[ar[i].second][x]=1;
			tot=max(tot,x);
		}
		if(tot<ans){
			ans=tot;
			asr=ar;
		}
	}
	printf("%d\n",ans);
	for(int i=1;i<=m;i++)putchar('0');
	puts("");
	printf("%d\n",p);
	memset(t1,0,sizeof(t1));
	memset(t2,0,sizeof(t2));
	ar=asr;
	for(int i=0;i<p;i++){
		x=ga(ar[i].first,ar[i].second);
		t1[ar[i].first][x]=1;
		t2[ar[i].second][x]=1;
		printf("%d %d %d 1\n",ar[i].first,ar[i].second,x-1);
	}
	return 0;
}
