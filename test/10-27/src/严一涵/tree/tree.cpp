#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const LL md=998244353;
void exgcd(LL a,LL b,LL&x,LL&y){
	if(b==0){
		x=1;
		y=0;
		return;
	}
	exgcd(b,a%b,y,x);
	y-=a/b*x;
}
LL inv(LL v){
	v%=md;
	LL x,y;
	exgcd(v,md,x,y);
	return (x%md+md)%md;
}
vector<int>e[100003];
int n;
LL x[100003],y[100003];
LL sx,sy,ix,iy,p[100003],f[100003],ans;
void dfs(int u,int fa){
	p[u]=1;
	f[u]=0;
	for(vector<int>::iterator i=e[u].begin();i!=e[u].end();i++){
		if(*i==fa)continue;
		dfs(*i,u);
		p[u]+=p[*i]+1;
		f[u]+=f[*i]+p[*i]*x[*i];
		x[u]+=x[*i];
	}
}
void ref(int u,int fa){
	if(fa!=-1){
		f[u]=f[fa]-p[u]*x[u]+(p[fa]-p[u]-1)*(sx-x[u]);
		p[u]=p[fa];
	}
	ans+=f[u]%md*ix%md*y[u]%md*iy%md;
	ans%=md;
	for(vector<int>::iterator i=e[u].begin();i!=e[u].end();i++){
		if(*i==fa)continue;
		ref(*i,u);
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		static int u,v;
		u=read();
		v=read();
		e[u].push_back(v);
		e[v].push_back(u);
	}
	for(int i=1;i<=n;i++){
		x[i]=read();
		y[i]=read();
		sx+=x[i];
		sy+=y[i];
	}
	ix=inv(sx);
	iy=inv(sy);
	dfs(1,-1);
	ref(1,-1);
	printf("%lld\n",ans);
	return 0;
}
