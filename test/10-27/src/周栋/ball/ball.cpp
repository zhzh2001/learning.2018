#include <iostream>
#include <cstring>
#include <cstdio>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)x=-x,pc('-');
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=10010;
ll n,a[N],b[N],s[N],ans;
ll f1[1<<18];
ll f[N][N];
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	read(n);
	for (ll i=1;i<=n;++i) read(a[i]),read(b[i]);
	if (n<=18){
		for (ll i=0;i<(1<<n);++i){
			ll k=0;
			for (ll j=0;j<n;++j) k+=(i&(1<<j))>>j;
			for (ll j=0;j<n;++j)
				if (!(i&(1<<j))&&k+a[j+1]<=n){
					ll t=i;
					for (ll u=j,v=0;v<a[j+1];u=(u+1)%n){
						if (!(t&(1<<u))) t|=1<<u,++v;
					}
					f1[t]=max(f1[t],f1[i]+b[j+1]);
				}
			ans=max(ans,f1[i]);
		}wr(ans);
		return 0;
	}
//	for (ll i=n+1;i<2*n;++i) a[i]=a[i-n],b[i]=b[i-n];
	memset(f,-0x3f,sizeof f);
	for (ll i=2*n-1;i;--i){
		for (ll j=0;j<=min(n,2*n-i);++j){
			j?f[i][j]=max(0,f[i+1][j-1]):f[i][j]=0;
			f[i][j]=max(f[i][j],f[i+1][j+a[i]-1]+b[i]);
			ans=max(ans,f[i][j]);
		}
		if (i<=n) for (ll j=0;j<n;++j) f[i][j]=f[i][j+1];
		f[i][0]=0;
	}
	wr(ans);
	return 0;
}
//sxdakking
/*
4
1 2
1 3
2 10
1 2
*/
