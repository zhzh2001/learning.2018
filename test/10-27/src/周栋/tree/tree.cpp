#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)x=-x,pc('-');
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10,P=998244353;
ll n,head[N],edge,a[N],b[N],A,B,ins[N],V[N],cnt,rt,sa[N],ssa[N],ans;
struct edge{ll to,net;}e[N<<1];
inline ll pow(ll x,ll y){
	ll ret=1;
	for(;y;y>>=1,x=x*x%P) if (y&1) ret=ret*x%P;
	return ret;
}
inline void addedge(ll x,ll y){
	e[++edge].net=head[x],head[x]=edge,e[edge].to=y;
	e[++edge].net=head[y],head[y]=edge,e[edge].to=x;
	++ins[x],++ins[y];
}
inline void dfs(ll x,ll fa){
	V[++cnt]=x;
	for (ll i=head[x];i;i=e[i].net)
		if (e[i].to!=fa){
			dfs(e[i].to,x);
		}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(n);
	for (ll i=1,x,y;i<n;++i) read(x),read(y),addedge(x,y);
	for (ll i=1;i<=n;++i) read(a[i]),read(b[i]),A+=a[i],B+=b[i];
	if (n==1) return wr(0),0;
	if (n==2) return wr( (a[1]*b[2] + a[2]*b[1])*pow(A*B%P,P-2)%P ),0;
	for (ll i=1;i<=n;++i) if (ins[i]==1){rt=i;break;}
	dfs(rt,0);
	for (ll i=1;i<=n;++i) sa[i]=(sa[i-1]+pow(i-1,2)*(a[V[i]]))%P;
	for (ll i=n;i;--i) ssa[i]=(ssa[i+1]+pow(n-i,2)*(a[V[i]]))%P;
	for (ll i=1;i<=n;++i){
		ans+=b[V[i]]*(pow(i-1,2)-sa[i-1])%P;
		ans+=b[V[i]]*(pow(n-i,2)-ssa[i+1])%P;
		ans%=P;
	}
	wr(ans*pow(A*B%P,P-2)%P);
	return 0;
}
//sxdakking
/*


4
1 2
2 3
3 4
0 0
1 0
0 1
0 0
3
*/
