#include <bits/stdc++.h>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
const ll NN=1<<14;
char ANS[NN+3];
ll ngt;
inline void pc(char x){ANS[ngt++]=x;if(ngt==NN){cout<<ANS;ngt=0;}}
inline void end(){ANS[ngt]=0;cout<<ANS;}
inline void wr(ll x){
	if(x<0)x=-x,pc('-');
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=130,T=5e6+10,INF=0x3f3f3f3f;
ll n,m,b,p[N],a[N][N],tot,OJ[N],ed[N],Max,vis[N];
struct Ans{
	ll a,b,c,d;
	Ans(){}
	Ans(ll A,ll B,ll C,ll D):a(A),b(B),c(C),d(D){}
	inline void aout(){
		wr(a),pc(' ');
		wr(b),pc(' ');
		wr(c),pc(' ');
		wr(d),pc('\n');
	}
}ans[T];
int main(){
	freopen("aysgh.in","r",stdin);
	freopen("aysgh.out","w",stdout);
	read(n),read(m),read(b);
	for (ll i=1;i<=m;++i) read(p[i]);
	for (ll i=1;i<=n;++i){
		vis[i]=m;
		for (ll j=1;j<=m;++j){
			read(a[i][j]);
			if (!a[i][j]) a[i][j]=INF,--vis[i];
		}
	}
	if (b==0){
		do{
			ll k1=-1,k2=-1;
			for (ll i=1;i<=n;++i) if (vis[i]&&(k1==-1||ed[i]<ed[k1])) k1=i;
			if (k1==-1) break;
			for (ll i=1;i<=m;++i) if (a[k1][i]!=INF&&(k2==-1||OJ[k2]>OJ[i])) k2=i;
			ans[++tot]=Ans(k1,k2,max(ed[k1],OJ[k2]),a[k1][k2]);
			ed[k1]=OJ[k2]=max(ed[k1],OJ[k2])+a[k1][k2];
			Max=max(Max,ed[k1]);
			--vis[k1];
			a[k1][k2]=INF;
		}while (1);
		wr(Max),pc('\n');
		for (ll i=1;i<=m;++i) pc('0');pc('\n');
		wr(tot),pc('\n');
		for (ll i=1;i<=tot;++i) ans[i].aout();
		end();
		return 0;
	}
	return 0;
}
//sxdakking
/*
3 2 0
3 7
3 1
3 1
1 5
*/
