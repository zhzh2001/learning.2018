#include<queue>
#include<vector>
#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
struct FastIO{
	char gc(){
		static char ibuf[1<<14],*p1=ibuf,*p2=ibuf;
		return (p1==p2&&(p2=(p1=ibuf)+fread(ibuf,1,1<<14,stdin),p1==p2))?EOF:*p1++;
	}
	int siz;
	char obuf[1<<14];
	void pc(char x){
		if(siz==1<<14){
			printf(obuf);
			siz=0;
		}
		obuf[siz++]=x;
	}
	void End(){
		printf(obuf);
	}
}fastIO;
#define gc c=fastIO.gc()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
#define pc fastIO.pc
void write(int x){
	if(x<0)pc('-'),x=-1;
	if(x>9)write(x/10);
	pc(x%10^'0');
}
void writeln(int x){
	write(x);
	pc('\n');
}
struct st{
	st(){}
	st(int start_time,int end_time,int per,int OJ){
		s=start_time;
		t=end_time;
		p=per;
		oj=OJ;
	}
	int s,t,p,oj;
	bool operator<(const st &b)const{
		return t>b.t;
	}
};
vector<st>ans;
priority_queue<st>Que;
int T,n,m,b;
int a[125][125],cost[125],person[125];
bool used[125],can[125];
void init(int p){
	if(Que.empty())return;
	st s=Que.top();Que.pop();
	init(p);
	if(s.p==p){
		used[s.oj]=0;
		a[p][s.oj]=s.t-T+1;
		ans.push_back(st(s.s,T-1,s.p,s.oj));
		return;
	}
	Que.push(s);
}
void match(int p){
	if(can[p])return;
	init(p);
	can[p]=1;
	for(int i=1;i<=m;i++)
		if(a[p][i]){
			if(used[i])match(person[i]);
			Que.push(st(T,T+a[p][i],p,i));
			person[i]=p;
			used[i]=1;
			return;
		}
}
int main(){
	freopen("aysgh.in","r",stdin);
	freopen("aysgh.out","w",stdout);
	n=read(),m=read(),b=read();
	for(int i=1;i<=m;i++)cost[i]=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=read();
	for(int i=1;i<=min(n,m);i++){
		Que.push(st(0,a[i][i],i,i));
		person[i]=i;
		used[i]=1;
	}
	while(!Que.empty()){
		st now=Que.top();
		T=now.t+1;
		memset(can,0,sizeof(can));
		match(now.p);
	}
	writeln(T-1);
	for(int i=1;i<m;i++)pc('0');
	writeln(0);
	writeln(ans.size());
	for(int i=0;i<(int)ans.size();i++){
		write(ans[i].p),pc(' '),write(ans[i].oj),pc(' '),write(ans[i].s),pc(' ');
		writeln(ans[i].t-ans[i].s);
	}
	fastIO.End();
}
