#include<cstdio>
#include<iostream>
#define int long long
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int P=998244353;
const int MAXN=1e5+5;
int nedge,head[MAXN];
struct Edge{
	int to,nxt;
}edge[MAXN<<1];
void add(int x,int y){
	edge[++nedge].to;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
int pw(int a,int n){
	int ans=1;
	while(n){
		if(n&1)ans=ans*a%P;
		a=a*a%P;
		n>>=1;
	}
	return ans;
}
int x[MAXN],y[MAXN],X,Y;
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int n=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		add(x,y),add(y,x);
	}
	for(int i=1;i<=n;i++){
		x[i]=read(),y[i]=read();
		X+=x[i],Y+=y[i];
	}
	X=pw(X,P-2),Y=pw(Y,P-2);
	if(n==1)return puts("0"),0;
	if(n==2)return printf("%lld",(x[1]*X%P*y[2]%P*Y%P+x[2]*X%P*y[1]%P*Y%P)%P),0;
	// for(int S=1;S<=n;S++)
	// 	for(int T=1;T<=n;T++)
	// 		dfs(S,0);
}
