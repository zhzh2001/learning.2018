#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
namespace Dango
{
	const int MAXN=100005,MOD=998244353;
	int n,head[MAXN],to[MAXN*2],nxt[MAXN*2],cnt,size[MAXN];
	long long x[MAXN],y[MAXN],ans,totx,toty;
	long long pow_(long long a,long long b)
	{
		long long result=1;
		while(b)
		{
			if(b&1)result=result*a%MOD;
			a=a*a%MOD;
			b>>=1;
		}
		return result;
	}
	void add(int u,int v)
	{
		cnt++;
		to[cnt]=v;
		nxt[cnt]=head[u];
		head[u]=cnt;
	}
	long long dfs(int u,int e)
	{
		if(u==e)return 0;
		long long result=0;
		for(int i=head[u];i;i=nxt[i])
		{
			int v=to[i];
			result=(result+dfs(v,e)*pow_(size[u],MOD-2)%MOD)%MOD;
		}
		return result+1;
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin>>n;
		for(int i=1;i<n;i++)
		{
			int u,v;
			cin>>u>>v;
			add(u,v);
			add(v,u);
			size[u]++;
			size[v]++;
		}
		for(int i=1;i<=n;i++)
		{
			cin>>x[i]>>y[i];
			totx+=x[i];
			toty+=y[i];
		}
		toty=pow_(toty,MOD-2);
		totx=pow_(totx,MOD-2);
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
			{
				if(x[i]*y[j]==0)continue;
				ans+=dfs(i,j)*x[i]%MOD*y[j]%MOD;
			}
		cout<<ans*totx%MOD*toty%MOD;
		return 0;
	}
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	return Dango::work();
}
