#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
namespace Dango
{
	const int MAXN=5005;
	int n,a[MAXN],b[MAXN],dp[MAXN][MAXN],ans;
	int solve()
	{

		for(int i=n;i>=1;i--)
			for(int j=0;j<=n-a[i];j++)
				ans=max(ans,(dp[i][j]=max(dp[i][j],dp[i+1][j+a[i]-1]+b[i])));
		cout<<ans;
		return 0;
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin>>n;
		for(int i=1;i<=n;i++)
			cin>>a[i]>>b[i];
		if(n>18)return solve();
		for(int k=1;k<=n;k++)
		{
			for(int i=0;i<n;i++){a[i]=a[i+1];b[i]=b[i+1];}
			a[n]=a[0];b[n]=b[0];
			memset(dp,0,sizeof(dp));
			for(int i=1;i<=n;i++)
				for(int j=a[i];j<=n;j++)
					ans=max(ans,(dp[min(i+a[i],n+1)][j-a[i]]=max(dp[min(i+a[i],n+1)][j-a[i]],dp[i][j]+b[i])));
		}
		cout<<ans;
		return 0;
	}
}
int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	return Dango::work();
}			

