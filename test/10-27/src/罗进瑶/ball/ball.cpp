#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("ball.in", "r", stdin);
	freopen("ball.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch -'0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0'); }
inline void writeln(ll x) {write(x);puts("");}
const int N = 21;
int n;
struct T {
	int x, y, i;
}a[N], b[N];
int ans;
void dfs(int ct, int sum) {
	ans = max(ans, sum);
	if(ct == 0) return;
	T c[N];
	for(int i = 1; i <= ct; ++i) c[i] = b[i];
	for(int i = 1; i <= ct; ++i)
		if(c[i].x <= ct) {
			int tot = 0;
			for(int j = (i + c[i].x - 1 + ct)% ct + 1; j != i; j = j % ct + 1)
				b[++tot] = c[j];
			dfs(tot, sum + c[i].y);
		}
}
int main() {
	setIO();
	n = read();
	if(n > 18) {
		puts("0");
		return 0;
	}
	for(int i = 1; i <= n; ++i)
		a[i].x = read(), a[i].y = read(), a[i].i = i, b[i] = a[i];
	dfs(n, 0);
	writeln(ans);
	return 0;
}
