#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch -'0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0'); }
inline void writeln(ll x) {write(x);puts("");}
const int N = 310000;
const ll p = 998244353;
int ver[N], nxt[N], en, head[N];
void add(int x, int y) {
	ver[++en] = y, nxt[en] = head[x], head[x] = en;
}
int n;
ll ksm(ll a, ll b) {
	ll res = 1;
	for(; b; b >>= 1, a = a * a % p)
		if(b & 1)
			res = res * a % p;
	return res;
}
inline ll inv(ll x) {return ksm(x, p - 2);}
ll x[N], y[N], X, Y;
int main() {
	setIO();
	n = read();
	for(int i = 1; i < n; ++i) {
		int u = read(), v = read();
		add(u, v);
		add(v,u);
	}
	for(int i = 1 ;i <= n; ++i) {
		x[i] = read(), y[i] = read();
		X += x[i], Y += y[i];
	}
	if(n == 1) {puts("0");return 0;}
	else if(n == 2) {writeln((x[1] * y[2] % p * inv(X) % p * inv(Y) % p + y[1] * x[2] % p * inv(X) % p * inv(Y) % p) % p);}
	else puts("0");
	return 0;
}
