#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <limits>
#include <vector>
using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;

namespace IO {
static const int kMaxSize = 1 << 20;
char buf[kMaxSize], *S = buf, *T = buf;
bool neg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMaxSize, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = neg = 0;
  char ch = NC();
  for (; isspace(ch); ch = NC())
    ;
  if (ch == '-')
    neg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (neg) x = -x;
}
inline char RC() {
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  return ch;
}
inline int RS(char *s) {
  int cur = 0, ch = NC();
  for (; isspace(ch); ch = NC());
  for (; ch != EOF && !isspace(ch); ch = NC())
    s[cur++] = ch;
  return s[cur] = 0, cur;
}
} // namespace IO

using IO::RI;
using IO::RS;
using IO::RC;

const int kMaxN = 1e5 + 5, kMod = 998244353;
int n, head[kMaxN], ecnt, inv[kMaxN], ind[kMaxN], x[kMaxN], y[kMaxN];
struct Edge { int to, nxt; } e[kMaxN << 1];
int sumx = 0, sumy = 0;
inline void Insert(int u, int v) {
  e[++ecnt] = (Edge) { v, head[u] };
  head[u] = ecnt;
  ++ind[v];
}
ll QuickPow(ll x, ll y) {
  ll ret = 1;
  for (; y; y >>= 1, x = x * x % kMod)
    if (y & 1) ret = ret * x % kMod;
  return ret;
}
namespace Spec1 {
int Solve() {
  if (n == 1) {
    printf("%d\n", 0);
  } else {
    ll ans = 0;
    for (int i = 1; i <= n; ++i) {
      for (int j = 1; j <= n; ++j) {
        ans += x[i] * QuickPow(sumx, kMod - 2) % kMod * y[i] % kMod * QuickPow(sumy, kMod - 2) % kMod * (i == j ? 0 : 1);
        ans %= kMod;
      }
    }
    printf("%lld\n", (ans % kMod + kMod) % kMod);
  }
  return 0;
}
}
namespace SubTask1 {
ll f[kMaxN], pro, ans;
void DFS(int x, int fa = 0) {
  if (::y[x]) {
    ans += pro * y[x] * QuickPow(sumy, kMod - 2) % kMod * f[x] % kMod;
    ans %= kMod;
  }
  for (int i = head[x]; i; i = e[i].nxt) {
    if (e[i].to == fa)
      continue;
    f[e[i].to] = inv[ind[x]] * (f[x] + 1) + (1 + ind[e[i].to]) * QuickPow((QuickPow(ind[x], kMod - 2) - ind[e[i].to] + kMod) % kMod, kMod - 2);
    // f[e[i].to] = (f[x] + 1) * inv[ind[x]];
    DFS(e[i].to, x);
  }
}
int Solve() {
  for (int i = 1; i <= n; ++i) { // S
    memset(f, 0x00, sizeof f);
    pro = x[i] * QuickPow(sumx, kMod - 2) % kMod; 
    DFS(i);
  }
  printf("%lld\n", (ans % kMod + kMod) % kMod);
  return 0;
}
} // namespace SubTask1
int main() {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);
  RI(n);
  for (int i = 1, a, b; i < n; ++i) {
    RI(a), RI(b);
    Insert(a, b);
    Insert(b, a);
  }
  for (int i = 1; i <= n; ++i) {
    RI(x[i]), RI(y[i]);
    sumx += x[i], sumy += y[i];
  }
  for (int i = 1; i <= n; ++i)
    inv[i] = QuickPow(i, kMod - 2);
  if (n <= 2) // 30 pts
    return Spec1::Solve();
  if (n <= 5000) // 40 pts
    return SubTask1::Solve();
  return 0;
}