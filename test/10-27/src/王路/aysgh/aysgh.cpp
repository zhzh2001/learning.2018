#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <limits>
#include <vector>
using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;

namespace IO {
static const int kMaxSize = 1 << 20;
char buf[kMaxSize], *S = buf, *T = buf;
bool neg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMaxSize, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = neg = 0;
  char ch = NC();
  for (; isspace(ch); ch = NC())
    ;
  if (ch == '-')
    neg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (neg) x = -x;
}
inline char RC() {
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  return ch;
}
inline int RS(char *s) {
  int cur = 0, ch = NC();
  for (; isspace(ch); ch = NC());
  for (; ch != EOF && !isspace(ch); ch = NC())
    s[cur++] = ch;
  return s[cur] = 0, cur;
}
} // namespace IO

using IO::RI;
using IO::RS;
using IO::RC;

const int kMaxN = 125, kMaxT = 1e7 + 5;
int n, m, b, a[kMaxN][kMaxN], tog[kMaxN];

int main() {
  freopen("aysgh.in", "r", stdin);
  freopen("aysgh.out", "w", stdout);
  RI(n), RI(m), RI(b);
  for (int i = 1; i <= m; ++i) {
    RI(p[i]);
  }
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= m; ++j) {
      RI(a[i][j]);
      tog[i] += a[i][j];
    }
  }
  if (b == 0) {
    printf("%d\n", *max_element(tog + 1, tog + n + 1));
    for (int i = 1; i <= m; ++i) {
      putchar('0');
    }  putchar('\n');
    printf("%d\n", 0);
    return 0;
  }
  printf("%d\n", *min_element(tog + 1, tog + n + 1));
  for (int i = 1; i <= m; ++i)
    putchar('0');
  puts("");
  printf("%d\n", 0);
  return 0;
}