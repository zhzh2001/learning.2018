#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <limits>
#include <vector>
using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;

namespace IO {
static const int kMaxSize = 1 << 20;
char buf[kMaxSize], *S = buf, *T = buf;
bool neg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMaxSize, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = neg = 0;
  char ch = NC();
  for (; isspace(ch); ch = NC())
    ;
  if (ch == '-')
    neg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (neg) x = -x;
}
inline char RC() {
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  return ch;
}
inline int RS(char *s) {
  int cur = 0, ch = NC();
  for (; isspace(ch); ch = NC());
  for (; ch != EOF && !isspace(ch); ch = NC())
    s[cur++] = ch;
  return s[cur] = 0, cur;
}
} // namespace IO

using IO::RI;
using IO::RS;
using IO::RC;

const int kMaxN = 5005;
int n, a[kMaxN], b[kMaxN], ans, id[kMaxN], ban[kMaxN], rem;

inline void Shift() {
  for (int j = 1; j <= n; ++j) {
    a[j - 1] = a[j];
    b[j - 1] = b[j];
  }
  a[n] = a[0], b[n] = b[0];
  a[0] = 1, b[0] = 0;
}

inline void Update(int &x, int y) {
  if (y > x) x = y;
}

inline bool Choose(int x) {
  if (x + a[x] - 1 > rem)
    return false;
  int lim = a[x];
  for (int c = 0; c < lim; ++c) {
    while (ban[x]) {
      x = x % n + 1;
    }
    ban[x] = true;
    --rem;
  }
  return true;
}

namespace Solution {
ll dp[kMaxN][kMaxN];
ll DP(int l, int r) {
  if (l > r)
    return 0;
  if (~dp[l][r])
    return dp[l][r];
  ll &res = dp[l][r];
  res = 0;
  for (int i = l + 1; i <= r; ++i)
    res = max(res, DP(l + 1, r - a[l] + i - l) + b[l]);
  return res;
}
int Solve() {
  ll t = 0;
  for (int i = 1; i <= min(n, 18); ++i) {
    memset(dp, 0xff, sizeof dp);
    t = max(DP(1, n), t);
    Shift();
  }
  printf("%lld\n", t);
  return 0;
}
}

int main() {
  freopen("ball.in", "r", stdin);
  freopen("ball.out", "w", stdout);
  RI(n);
  for (int i = 1; i <= n; ++i) {
    RI(a[i]), RI(b[i]);
  }
  if (n <= 10) { // 40 pts
    for (int i = 1; i <= n; ++i) {
      id[i] = i;
    }
    int ans = 0;
    do {
      for (int i = 1; i <= n; ++i) {
        ban[i] = false;
      }
      rem = n;
      int tmp = 0;
      for (int i = 1; i <= n; ++i) {
        if (Choose(id[i]))
          tmp += b[id[i]];
      }
      ans = max(ans, tmp);
    } while (next_permutation(id + 1, id + n + 1));
    printf("%d\n", ans);
    return 0;
  }
  return Solution::Solve();
}