#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 10010
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int n,a[maxn],b[maxn],f[2][maxn],ans;
signed main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n=read();
	for(int i=1;i<=n;b[i++]=read())
		a[i]=read();
	for(int i=n+1;i<n<<1;i++)
	{
		a[i]=a[i-n];
		b[i]=b[i-n];
	}
	int tmp=(n<<1)-1;
	for(int i=tmp;i>=n;i--)
	{
		memset(f,0,sizeof f);
		for(int k=i;k>=i-n+1;k--)
		{
			f[k&1][0]=0;
			for(int j=1;j<=i-k+1;j++)
				f[k&1][j]=f[k&1^1][j-1];
			for(int j=a[k];j<=i-k+1;j++)
				f[k&1][j-a[k]]=max(f[k&1][j-a[k]],f[k&1^1][j-1]+b[k]);
		}
		for(int k=0;k<=n;k++)
			ans=max(ans,f[(i-n)&1^1][k]);
	}
	write(ans);
	return 0;
}
