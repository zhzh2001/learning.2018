#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100010
#define MOD 998244353
#define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int power(int a,int b,int mod)
{
	int ans=1;
	for(;b;b>>=1,a=(a*a)%mod)
		if(b&1) ans=(ans*a)%mod;
	return ans;
}
int n,sumx,sumy,x[maxn],y[maxn];
vector<int> a[maxn];
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read();
		a[x].push_back(y);
		a[y].push_back(x);
	}
	for(int i=1;i<=n;i++)
	{
		sumx+=x[i]=read();
		sumy+=y[i]=read();
	}
	if(n==1)
	{
		write(0);
		return 0;
	}
	if(n==2)
	{
		if(sumx*sumy==0) write(0);
			else write((x[1]*power(sumx,MOD-2,MOD)%MOD*y[2]%MOD*power(sumy,MOD-2,MOD)%MOD+x[2]*power(sumx,MOD-2,MOD)%MOD*y[1]%MOD*power(sumy,MOD-2,MOD)%MOD)%MOD);
		return 0;
	}
	write(0);
	return 0;
}
