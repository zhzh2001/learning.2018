#include<bits/stdc++.h>
using namespace std;
inline int read() {
  int x = 0, f = 0; char ch = getchar();
  for (;ch<'0'||ch>'9';ch=getchar()) if (ch=='-') f=1;
  for (;ch>='0'&&ch<='9';ch=getchar()) x = (x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const int N = 5005;
int n, a[N], b[N], f[N][N];
int main() {
  freopen("ball.in","r",stdin);
  freopen("ball.out","w",stdout);
  n = read();
  for (int i = 1; i <= n; i++)
    a[i] = read(), b[i] = read();
  for (int i = n; i; i--) {
    for (int j = 0; j <= n; j++)
      f[i][j] = f[i+1][j];
    for (int j = 0; j <= n-a[i]; j++)
      f[i][j] = max(f[i][j], f[i+1][j+a[i]]+b[i]);
  }
  int mx = 0;
  for (int i = 1; i <= n; i++)
    for (int j = 0; j <= n; j++)
      mx = max(mx, f[i][j]);
  printf("%d\n", mx);
  return 0;
}
/*
f[i][j]表示当前第i个数，包括她自己后面有j个数的方案
f[i][j] = max(f[i][j], f[i+1][j]);//不选第i个数
f[i][j] = max(f[i][j], f[i+1][j+a[i]]+b[i])//选第i个数
感觉好像想错了
怎么过了大样例
不太妙。。。
4
1 2
1 3 
2 10
1 2
*/