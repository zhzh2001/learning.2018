#include<bits/stdc++.h>
#define N 125
using namespace std;
inline int read() {
  int x = 0, f = 0; char ch = getchar();
  for (;ch<'0'||ch>'9';ch=getchar()) if (ch=='-') f=1;
  for (;ch>='0'&&ch<='9';ch=getchar()) x = (x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
int n, m, b, p[N], a[N][N];
int main() {
  freopen("aysgh.in","r",stdin);
  freopen("aysgh.out","w",stdout);
  n = read(); m = read(); b = read();
  for (int i = 1; i <= m; i++)
    p[i] = read();
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= m; j++)
      a[i][j] = read();
  int ans = 0;
  for (int i = 1; i <= n; i++) {
    int sum = 0;
    for (int j = 1; j <= m; j++)
      sum += a[i][j];
    ans = max(ans, sum);
  }
  for (int j = 1; j <= m; j++) {
    int sum = 0;
    for (int i = 1; i <= n; i++)
      sum += a[i][j];
    ans = max(ans, sum);
  }
  printf("%d\n", ans);
  puts("0");
  return 0;
}