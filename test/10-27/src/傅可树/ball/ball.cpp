#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=5005;
int n,a[N],b[N];
inline void init(){
	n=read();
	for (int i=1;i<=n;i++) {
		a[i]=read(); b[i]=read();
	}
}
int dp[N],ans;
inline void solve(){
	for (int i=1;i<=n;i++){
		for (int j=n;j>=a[i];j--){
			dp[j]=max(dp[j],dp[j-a[i]]+b[i]);
		}
	}
	for (int i=1;i<=n;i++) ans=max(ans,dp[i]);
	writeln(ans);
}
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	init(); solve();
	return 0;
}
