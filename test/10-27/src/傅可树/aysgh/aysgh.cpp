#include<cstdio>
#include<algorithm>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=125;
int n,m,b,ton[N],a[N][N],p[N];
inline void init(){
	n=read(); m=read(); b=read();
	for (int i=1;i<=m;i++) p[i]=read();
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			a[i][j]=read();
			ton[j]+=a[i][j];
		}
	}
}
int ans;
inline void solve(){
	for (int i=1;i<=m;i++){
		ans=max(ans,ton[i]);
	}
	writeln(ans);
}
signed main(){
	freopen("aysgh.in","r",stdin); freopen("aysgh.out","w",stdout);
	init(); solve();
	return 0;
}
