#pragma GCC optimize ("O2")
#include<cstdio>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e5+5,mod=998244353;
struct edge{
	int link,next;
}e[N<<1];
int sumx,sumy,n,head[N],tot,x[N],y[N],degree[N];
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add_edge(u,v); add_edge(v,u);
	degree[u]++; degree[v]++;
}
inline void init(){
	n=read();
	for (int i=1;i<n;i++){
		insert(read(),read());
	}
	for (int i=1;i<=n;i++){
		x[i]=read(); y[i]=read();
		sumx+=x[i]; sumy+=y[i];
	}
}
inline int Pow(int x,int k){
	int y=1;
	for (;k;k>>=1,x=x*x%mod) if (k&1) y=y*x%mod;
	return y;
}
inline int Inv(int x){
	return (Pow(x,mod-2)+mod)%mod;
}
inline int fz(int x,int y){
	return x*Inv(y)%mod;
}
int rt,g[N],b[N],f[N],ans;
void dfs(int u,int fa){
	int sumg=0,sumb=0;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u); sumg+=g[v]; sumb+=b[v];
		}
	}
	int tmp=Inv(degree[u]-sumg+mod);
	g[u]=tmp;
	b[u]=(sumb+degree[u])%mod*tmp%mod;
}
void Dfs(int u,int fa){
	if (rt==u) f[u]=0;
		else f[u]=(f[fa]*g[u]%mod+b[u])%mod;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if  (v!=fa){
			Dfs(v,u);
		}
	}
	if (x[u]) {
		ans=(ans+f[u]*fz(y[rt],sumy)%mod*fz(x[u],sumx)%mod)%mod;
	}
}
inline void solve(){
	for (int i=1;i<=n;i++){
		if (!y[i]) continue;
		dfs(i,0); rt=i;
		Dfs(i,0);
	}
	writeln(ans);
}
signed main(){
	freopen("tree.in","r",stdin); freopen("tree.out","w",stdout);
	init(); solve();
	return 0;
}
