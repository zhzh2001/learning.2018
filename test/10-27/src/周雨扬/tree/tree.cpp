#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
const int mo=998244353;
const int N=100005;
int n,deg[N],X[N],Y[N];
int sx[N],sy[N],tx[N],ty[N];
int fx[N],fy[N],sumx[N];
int Sx[N],Sy[N];
int Fx[N],Fy[N],Sumx[N];
int SX,SY,ans;
int head[N],tot;
struct edge{
    int to,next;
}e[N*2];
void add(int x,int y){
    e[++tot]=(edge){y,head[x]};
    head[x]=tot; deg[x]++;
}
int power(int x,int y){
    int s=1;
    for (;y;y/=2,x=1ll*x*x%mo)
        if (y&1) s=1ll*s*x%mo;
    return s;
}
void getfunc(int x,int fa){
    sx[x]=sy[x]=0;
    fx[x]=sumx[x]=X[x]; fy[x]=0;
    for (int i=head[x];i;i=e[i].next)
        if (e[i].to!=fa){
            getfunc(e[i].to,x);
            sx[x]=(sx[x]+tx[e[i].to])%mo;
            sy[x]=(sy[x]+ty[e[i].to])%mo;
            sumx[x]=(sumx[x]+sumx[e[i].to])%mo;
            fx[x]=(fx[x]+1ll*fx[e[i].to]*tx[e[i].to])%mo;
            fy[x]=(fy[x]+1ll*fy[e[i].to]*tx[e[i].to]+1ll*sumx[e[i].to]*ty[e[i].to])%mo;
        }
    int xx=deg[x],yy=-deg[x];
    xx=(xx+mo-sx[x])%mo,yy=(yy+mo-sy[x])%mo;
    tx[x]=power(xx,mo-2);
    ty[x]=1ll*(mo-yy)*tx[x]%mo;
}
void getfunc2(int x,int fa){
    for (int i=head[x];i;i=e[i].next)
        if (e[i].to!=fa){
            int SX=(sx[x]+Sx[x]+1ll*mo-tx[e[i].to])%mo;
            int SY=(sy[x]+Sy[x]+1ll*mo-ty[e[i].to])%mo;
            int FX=(fx[x]+Fx[x]+1ll*mo-1ll*fx[e[i].to]*tx[e[i].to]%mo)%mo;
            int FY=(fy[x]+Fy[x]+2ll*mo-1ll*fy[e[i].to]*tx[e[i].to]%mo-1ll*sumx[e[i].to]*ty[e[i].to]%mo)%mo;
            int SUMX=(sumx[x]+Sumx[x]+1ll*mo-sumx[e[i].to])%mo;
            int XX=deg[x],YY=-deg[x];
            XX=(XX+mo-SX)%mo; YY=(YY+mo-SY)%mo;
            int TX=power(XX,mo-2),TY=1ll*(mo-YY)*TX;
            Sx[e[i].to]=TX;
            Sy[e[i].to]=TY;
            Sumx[e[i].to]=SUMX;
            Fx[e[i].to]=1ll*FX*TX%mo;
            Fy[e[i].to]=1ll*FY*TX%mo;
            Fy[e[i].to]=(Fy[e[i].to]+1ll*SUMX*TY%mo)%mo;
            getfunc2(e[i].to,x);
        }
}
int main(){
    freopen("tree.in","r",stdin);
    freopen("tree.out","w",stdout);
    scanf("%d",&n);
    For(i,1,n-1){
        int x,y;
        scanf("%d%d",&x,&y);
        add(x,y); add(y,x);
    }
    For(i,1,n){
        scanf("%d%d",&X[i],&Y[i]);
        SX+=X[i]; SY+=Y[i];
    }
    getfunc(1,0);
    getfunc2(1,0);
    For(i,1,n) ans=(ans+1ll*(fy[i]+Fy[i])*Y[i])%mo;
    printf("%lld",1ll*ans*power(1ll*SX*SY%mo,mo-2)%mo);
}
/*
3
1 2
1 3
1 0
0 2
0 3
*/
