#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define int long long
const int N=355;
int a[N][N],T[N],opt[N],n,m,K,v[N],rk[N],rrk[N];
ll mx,S,f[N],g[N];
inline bool cmp(int x,int y)
{
	return g[x]>g[y];
}
inline void sc(int x,int y,int t)
{
	if (T[y]!=t)
		T[y]=t;
	else
		return;
	int _y=y;
	if (y>m)
		_y-=m;
	if (!a[x][_y])
		return;
	--f[x];
	--g[_y];
	--a[x][_y];
	// for (int i=2;i<=x;++i)
	// 	cout<<"\t\t";
	printf("%lld %lld %lld %lld\n",x,y,t,1);
}
inline bool used(int x,int t)
{
	if (opt[x])
		return T[x]==t&&T[x+m]==t;
	else
		return T[x]==t;
}
inline int res(int x,int t)
{
	// return opt[x]?(g[x]+1)/2:g[x];
	return opt[x]?(mx*2-2-min(T[x],t-1)-min(T[x+m],t-1)-g[x]+1)/2:(mx-1-min(T[x],t-1)-g[x]);
}
inline bool pd(int x,int y,int z,int t)
{
	if (!a[x][y])
		return 0;
	if (!a[x][z])
		return 1;
	bool _y=used(y,t),_z=used(z,t);
	if (!_y&&_z)
		return 1;
	if (_y&&!_z)
		return 0;
	int __y=res(y,t),__z=res(z,t);
	return __y<__z;
}
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline bool cmp2(int x,int y)
{
	return f[x]>f[y];
}
signed main()
{
	freopen("aysgh.in","r",stdin);
	freopen("aysgh.out","w",stdout);
	read(n);
	read(m);
	read(K);
	for (int i=1;i<=m;++i)
		read(v[i]);
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
		{
			read(a[i][j]);
			f[i]+=a[i][j];
			g[j]+=a[i][j];
			S+=a[i][j];
		}
	for (int i=1;i<=n;++i)
		mx=max(mx,f[i]);
	for (int i=1;i<=m;++i)
		rk[i]=i;
	sort(rk+1,rk+1+m,cmp);
	for (int i=1;i<=m;++i)
		if (K>v[rk[i]])
		{
			K-=v[rk[i]];
			opt[rk[i]]=1;
		}
		else
			break;
	for (int i=1;i<=m;++i)
		mx=max(mx,opt[i]?(g[i]+1)/2:g[i]);
	// write(mx);
	printf("%d",mx);
	if (n>50||m>50||mx>2500)
	{
		puts("");
		for (int i=1;i<=m;++i)
			putchar('0');
		puts("");
		puts("0");
		return 0;
	}
	putchar('\n');
	for (int i=1;i<=m;++i)
	{
		putchar(opt[i]+'0');
		T[i]=T[i+m]=-1;
	}
	for (int i=1;i<=n;++i)
		rrk[i]=i;
	sort(rrk+1,rrk+1+n,cmp2);
	putchar('\n');
	// write(S);
	printf("%lld\n",S);
	for (int t=0;t<mx;++t)
	{
		for (int _i=1;_i<=n;++_i)
		{
			int i=rrk[_i];
			if (f[i]==0)
				continue;
			int p=1;
			for (int j=2;j<=m;++j)
				if (pd(i,j,p,t))
					p=j;
			if (!opt[p]||(T[p]<T[p+m]))
				sc(i,p,t);
			else
				sc(i,p+m,t);
		}
		sort(rrk+1,rrk+1+n,cmp2);
	}
	return 0;
}