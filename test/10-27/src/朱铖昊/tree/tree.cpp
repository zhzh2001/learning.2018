#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=100005;
const int mod=998244353;
int u,v,n,ans,X,Y;
int x[N],y[N],si[N];
int cnt,to[N*2],la[N],pr[N*2];
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
inline void dfs(int _x,int _y)
{
	si[_x]=1;
	for (int i=la[_x];i;i=pr[i])
		if (to[i]!=_y)
		{
			dfs(to[i],_x);
			(x[_x]+=x[to[i]])%=mod;
			(y[_x]+=y[to[i]])%=mod;
			si[_x]+=si[to[i]];
		}
	int up=si[_x]*2-1,down=(n-si[_x])*2-1;
	ans=(ans+(ll)x[_x]*(Y-y[_x])%mod*up%mod+
			 (ll)y[_x]*(X-x[_x])%mod*down%mod)%mod;
}
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(n);
	for (int i=1;i<n;++i)
	{
		read(u);
		read(v);
		add(u,v);
		add(v,u);
	}
	for (int i=1;i<=n;++i)
	{
		read(x[i]);
		read(y[i]);
		x[i]%=mod;
		y[i]%=mod;
		(X+=x[i])%=mod;
		(Y+=y[i])%=mod;
	}
	dfs(1,0);
	ans=(ll)ans*ksm(X,mod-2)%mod*ksm(Y,mod-2)%mod;
	cout<<ans;
	return 0;
}