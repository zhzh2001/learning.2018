#include<bits/stdc++.h>
using namespace std;
const int N=5005;
int a[N],b[N],f[N],n;
inline void read(int &x)

{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
	{
		read(a[i]);
		read(b[i]);
	}
	for (int i=1;i<=n;++i)
		for (int j=n;j>=a[i];--j)
			f[j]=max(f[j],b[i]+f[j-a[i]]);
	cout<<f[n];
	return 0;
}