#include<iostream>
#include<cstdio>
#define ll long long
#define maxn 6000
using namespace std;
void read(int &x){
    int f=1;
    x=0;
    char s=getchar();
    while(s<'0'||s>'9'){if(s=='-') f=-1;s=getchar();}
    while(s>='0'&&s<='9'){x=x*10+s-'0';s=getchar();}
    x*=f;
}
void write(ll x){
    if(x<0){putchar('-');x=-x;}
    if(x>9) write(x/10);
    putchar(x%10+'0');
}
int m,n,high,a[3*maxn],b[3*maxn];
int dfs(int h){
	if(h == high)
		return b[high];
	int nexts = (h + a[h] > n) ? n : h + a[h];
	return max(dfs(h + 1),dfs(nexts) + b[h]);
}
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	read(n);
	high = n;
	for(int i = 1;i <= n;i++){
		read(a[i]),read(b[i]);
	}
	write(dfs(1));
	return 0;
}
