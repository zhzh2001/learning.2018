#include <cstdio>
#include <fstream>
#include <iostream>
#include <algorithm>

using namespace std;

namespace sxdaknoip
{
	static const int maxn = 5005;
	
	int n;
	struct Ball { int a, b; } b[maxn];
	int dp[maxn];
	
	int main()
	{
		ifstream fin("ball.in");
		ofstream fout("ball.out");
		fin >> n;
		for(int i = 1; i <= n; ++i)
			fin >> b[i].a >> b[i].b;
		int ans = 0;
		for(int i = 1; i <= n; ++i)
			for(int j = n - b[i].a; j >= 0; --j)
				dp[j + b[i].a] = max(dp[j] + b[i].b, dp[j + b[i].a]);
		for(int i = 1; i <= n; ++i)
			ans = max(dp[i], ans);
		fout << ans;
		fin.close();
		fout.close();
		return 0;
	}
}

int main()
{
	sxdaknoip::main();
	return 0;
}
