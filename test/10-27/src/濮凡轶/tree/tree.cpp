#include <fstream>
#include <algorithm>

using namespace std;

#define int long long

const int mod = 998244353;

inline int pow(int x, int y) {
	int ans;
	for(ans = 1; y; y >>= 1, (x *= x) %= mod) {
		if(y & 1) {
			(ans *= x) %= mod;
		}
	}
	return ans;
}

signed main()
{
	ifstream fin("tree.in");
	ofstream fout("tree.out");
	int n;
	fin >> n;
	int a, b, c, d;
	n--;
	while(n--)
		fin >> a >> b;
	fin >> a >> b >> c >> d;
	int X = a + c;
	int Y = b + d;
	fout << (a * d % mod + b * c % mod) * pow(X, mod - 2) % mod * pow(Y, mod - 2) % mod;
	fin.close();
	fout.close();
	return 0;
}
