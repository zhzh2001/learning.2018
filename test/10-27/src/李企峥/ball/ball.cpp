#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define fi first
#define sd second 
using namespace std;
ll f[5010];
pair<ll,ll> a[5010];
ll n,ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n=read();
	For(i,1,n)
	{
		a[i].fi=read();
		a[i].sd=read();
	}
	For(i,1,n)f[i]=-1e9;
	f[0]=0;
	For(i,1,n)
	{
		Rep(j,a[i].fi,n)
		{
			f[j]=max(f[j],f[j-a[i].fi]+a[i].sd);
		}
	}
	ans=0;
	For(i,1,n)ans=max(ans,f[i]);
	cout<<ans<<endl;
	return 0;
}


