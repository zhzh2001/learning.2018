#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,x,y;
int ans,a1,a2,b1,b2,X,Y;
int x1,x2;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int ex_gcd(int a,int b,int&x,int &y)
{
	if(b==0)
	{
		x=1;
		y=0;
		return a;
	}
	int ans=ex_gcd(b,a%b,x,y);
	int tmp=x;
	x=y;
	y=tmp-a/b*y;
	return ans;
} 
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	For(i,1,n-1)x=read(),y=read();
	cin>>a1>>b1>>a2>>b2;
	X=a1+a2;Y=b1+b2;
	X*=Y;
	x1=y=0;
	ans=ex_gcd(a1*b2,X,x1,y);
	x2=y=0;
	ans=ex_gcd(a2*b1,X,x2,y);
	ans=(x1%X+X)%X+(x2%X+X)%X;
	ans%=X;
	cout<<ans<<endl;
	return 0;
}
