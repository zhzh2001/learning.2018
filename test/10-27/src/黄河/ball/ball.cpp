#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define p 998244353
using namespace std;

#define N 10005
inline LL read(){
	LL x=0;int ch=getchar(),f=1;
	while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
	if (ch=='-'){f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
int n,a[N],b[N],*H=a, *HH=b,boo[N],ans;
void dfs(int now){
	ans=std::max(ans,now);
	rep(i,1,n){
		if (boo[i]) continue;
		int j,k;
		for (j,i,k=0;k<H[i]&&j<=n;++j) if (!boo[j]) ++k;
		if (k<H[i]) continue;
		for (j=i,k=0;k<H[i]&&j<=n;++j) if (!boo[j]) ++k,boo[j]=i;
		dfs(now+HH[i]);
		for (j=i,k=0;k<H[i]&&j<=n;++j) if (boo[j]==i)++k,boo[j]=0;
	}
}
int main(){
	freopen("ball.in", "r", stdin);
	freopen("ball.out", "w", stdout);
	n=read();
	rep(i,1,n)
		a[i]=read(),b[i]=read(),a[n+i]=a[i],b[n+i]=b[i];
	for (register int h=1;h<=n;++h,++H,++HH) dfs(0);
	printf("%d", ans);
}
