#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define N 100010
#define p 998244353
using namespace std;
inline LL read(){
	LL x=0;int ch=getchar(),f=1;
	while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
	if (ch=='-'){f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
int n,x[N],y[N];
LL X,Y,ans;
inline LL qpow(LL k,LL base){
	LL res=1;
	while (base){
		if (base&1) res=res%p*k%p;
		base/=2;
		k=k*k%p;
	}
	return res;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	if (n<=1) {printf("0\n");return 0;};
	//if (n==2) {printf("1\n");return 0;};
	if (n==2){
		rep(i,1,n-1) {
			int u=read(),v=read();//add(u,v);add(v,u);
		}
		rep(i,1,n){
			x[i]=read(),y[i]=read();
			(X+=x[i])%p,(Y+=y[i])%p;
		}		
		LL a=x[1]*1LL*y[2]%p*1LL+x[2]*1LL*y[1]%p*1LL,b=X%p*1LL*Y*1LL%p;
		a%=p;
	//	LL g=3;
	//	a/=g,b/=g;
		ans=a*1LL*qpow(b,p-2);
		ans=ans%p;
	//	printf("%d\n",qpow(8,4));
		//printf("%d %d\n",a,b);
		printf("%lld",ans);
	}	
	return 0;
}
