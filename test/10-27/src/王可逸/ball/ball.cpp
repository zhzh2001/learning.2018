#include <bits/stdc++.h>
#define int long long
using namespace std;
int n,a[120000],b[120000],ans,now,cnt,head[240000],chu[240000],ansa,ansb,v[240000];
bool vis[120000];
vector <int> p[120000];
struct node {
	int next,to;
} sxd[240000];
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	head[u]=cnt;
}
void dfs(int at,int remain,int num,int pre) {
	int jian=0;
	p[at].clear();
//	cout<<"dfs:"<<at<<" "<<remain<<" "<<num<<" "<<pre<<'\n';
//	puts("F1");
//	for(int i=1;i<=n;i++) cout<<vis[i]<<" ";
//	puts("");
	int sxd=at;
	for(int i=1; i<=a[at]; i++) {
		while(vis[sxd]) {
			sxd++;
			if(sxd==n+1) sxd=1;
		}
		vis[sxd]=true;
		jian++;
		p[at].push_back(sxd);
	}
//	puts("F2");
//	for(int i=1;i<=n;i++) cout<<vis[i]<<" ";
//	puts("");
	for(int i=1; i<=n; i++) {
		if(vis[i]==false && remain-jian>=a[i])
			dfs(i,remain-jian,num+b[at],at);
	}
	for(int i=0; i<(int)p[at].size(); i++) vis[p[at][i]]=false;
//	puts("F3");
//	for(int i=1;i<=n;i++) cout<<vis[i]<<" ";
//	puts("");
	ans=max(ans,num+b[at]);
}
void subtask1() {
	for(int i=1; i<=n; i++) {
		memset(vis,0,sizeof vis);
		dfs(i,n,0,0);
	}
	cout<<ans;
}
void fen(int at,int color,int f) {
	vis[at]=true;
	if(color) ansa+=b[at];
	else ansb+=b[at];
	for(int i=head[at];i;i=sxd[i].next) {
		if(sxd[i].to==f) continue;
		fen(sxd[i].to,color^1,at); 
	}
}
signed main() {
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	scanf("%lld",&n);
	for(int i=1; i<=n; i++) scanf("%lld%lld",&a[i],&b[i]);
//	if(n<=10) subtask1();
//	else if(n<=18) subtask2();
//	else subtask3();
	if(n<=18) subtask1();
	else {
		for(int i=1; i<=n; i++) {
			int now=0,at=i;
			while(now<a[i]) {
				if(at!=i) {
					add(i,at);
					v[at]=v[i]=true;
					chu[at]++;
				}
				at++;
				if(at==n+1) at=1;
				now++;
			}
		}
		for(int i=1;i<=n;i++) {
			if(!chu[i] && v[i])	{
				fen(i,0,0);	
			}	
		}
		int ww=0;
		for(int i=1;i<=n;i++) if(!v[i]) ww+=b[i];
		cout<<max(ansa,ansb)+ww;
	}
	return 0;
}
//sxdakking
