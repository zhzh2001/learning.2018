#include <bits/stdc++.h>
#define int long long
using namespace std;
const int mod=998244353;
const int N=120000;
int n,m,cnt,totx,toty,head[N],x[N],y[N];
struct node {int next,to;} sxd[N];
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;	
	head[u]=cnt;
}
int ksm(int a,int b) {
	int ans=1;
	while(b) {
		if(b&1) ans=(ans*a)%mod;
		a=(a*a)%mod;
		b>>=1;
	}
	return ans;
}
void subtask1() {
	int ansa=x[1]*y[2]+x[2]*y[1];
	int ansb=(x[1]+x[2])*(y[1]+y[2]);
	int inv=ksm(ansb,mod-2);
	cout<<(ansa*inv)%mod;
}
signed main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);	
	cin>>n;
	for(int i=1;i<n;i++) {
		int x,y;
		cin>>x>>y;
		add(x,y);
		add(y,x);	
	}
	for(int i=1;i<=n;i++) {
		cin>>x[i]>>y[i];	
	}
	if(n==2) {
		subtask1();	
	} else puts("1");
}
//sxdakking
