#include <cstdio>
using namespace std;
const int N=1e5+7,P=998244353;
struct edge {
	int t,nxt;
}e[N<<1];
int n,cnt,X,Y;
int head[N],x[N],y[N];
int ksm(int a,int b) {
	int s=1;
	for (;b;b>>=1,a=1ll*a*a%P) if (b&1) s=1ll*s*a%P;
	return s;
}
void solve1() {
	X=ksm(X,P-2);Y=ksm(Y,P-2);
	int ans=1ll*y[2]*x[1]%P;
	ans=(ans+1ll*y[1]*x[2]%P)%P;
	printf("%lld\n",1ll*ans*Y%P*X%P);
}
void add(int u,int t) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;
}
int main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d",&n);
	for (int i=1,u,v;i<n;i++) scanf("%d%d",&u,&v),add(u,v),add(v,u);
	for (int i=1;i<=n;i++) scanf("%d%d",&x[i],&y[i]),X+=x[i],Y+=y[i];
	if (n==1) return puts("0"),0;
	if (n==2) solve1();
}
