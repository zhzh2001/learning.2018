#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;
const int N=5005;
int n,ln,ans;
int a[N],b[N],id[N],vis[N];
vector<int>v[N];
void dfs(int x,int s) {
	ans=max(ans,s);
	for (int i=1;i<=n;i++) if (!vis[i] && a[i]<=ln) {
		v[x].clear();
		for(int j=1,k=i;j<=a[i];k=k%n+1,j++) {
			while (vis[k]) k=k%n+1;
			vis[k]=1;v[x].push_back(k);
		}ln-=a[i];
		dfs(x+1,s+b[i]);
		for (int j=0;j<v[x].size();j++) vis[v[x][j]]=0;ln+=a[i];
	}
}
int main() {
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	scanf("%d",&n);ln=n;
	for (int i=1;i<=n;++i) {
		scanf("%d%d",&a[i],&b[i]);
	}
	dfs(1,0);
	printf("%d\n",ans);
}
