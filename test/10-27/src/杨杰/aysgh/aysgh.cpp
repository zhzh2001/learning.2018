#include <cstdio>
using namespace std;
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
const int N=15;
int v[N],a[N][N],vis[N*100][N],fan[N*100][N],now[N*100][N];
int n,m,b,ans=1e9,T;
void dfs(int);
void dfs1(int);
void dfs1(int x,int id) {
	if (x>n) return dfs(id+1);
	for (int i=1;i<=m;i++) if (a[x][i] && !vis[id][i]) {
		vis[id][i]=1;a[x][i]--;now[id][x]=i;
		dfs1(x+1,id);
		vis[id][i]=0;a[x][i]++;now[id][x]=0;
	}
	dfs1(x+1,id);
}
void dfs(int x) {
	if (x>=ans) return;
	int pd=0;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++) if (a[i][j]) pd=1;
	if (!pd) {
		ans=x-1;
		for (int i=1;i<=x;i++)
			for (int j=1;j<=n;j++) fan[i][j]=now[i][j];
		return;
	}
	dfs1(1,x);
}
int main() {
	freopen("aysgh.in","r",stdin);
	freopen("aysgh.out","w",stdout);
	n=read(),m=read(),b=read();
	for (int i=1;i<=m;i++) scanf("%d",&v[i]);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++) a[i][j]=read(),T+=a[i][j];
	dfs(1);
	printf("%d\n",ans);
	for (int i=1;i<=n;i++) putchar('0');
	puts("");
	printf("%d\n",T);
	for (int i=1;i<=ans;i++)
		for (int j=1;j<=n;j++) if (fan[i][j]) printf("%d %d %d 1\n",j,fan[i][j],i-1);
}
