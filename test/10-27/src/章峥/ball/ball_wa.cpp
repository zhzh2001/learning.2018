#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("ball.in");
ofstream fout("ball.out");
const int N=5005;
int a[N],b[N],f[N][N];
int dp(int i,int j)
{
	if(i>j)
		return 0;
	if(~f[i][j])
		return f[i][j];
	f[i][j]=0;
	for(int k=i;k<=j;k++)
		if(a[k]<=j-i+1)
		{
			if(a[k]<=j-k+1)
				f[i][j]=max(f[i][j],dp(i,k-1)+b[k]+dp(k+a[k],j));
			else
				f[i][j]=max(f[i][j],b[k]+dp(i+a[k]-(j-k+1),k-1));
		}
	return f[i][j];
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i]>>b[i];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			f[i][j]=-1;
	fout<<dp(1,n)<<endl;
	return 0;
}
