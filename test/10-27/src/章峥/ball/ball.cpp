#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("ball.in");
ofstream fout("ball.out");
const int N=5005;
int a[N],b[N],f[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i]>>b[i];
	for(int i=1;i<=n;i++)
		for(int j=n;j>=a[i];j--)
			f[j]=max(f[j],f[j-a[i]]+b[i]);
	fout<<f[n]<<endl;
	return 0;
}
