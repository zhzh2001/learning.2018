#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N=100005,MOD=998244353;
int head[N],v[N*2],nxt[N*2],e,x[N],y[N],dp[N],X,Y,invX,invY;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
long long qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}while(b/=2);
	return ans;
}
void dfs(int u,int fat)
{
	int deg=0,k=0,val=0;
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			dfs(v[i],u);
			deg++;
			k=(k+1ll*(Y-y[v[i]])*invY)%MOD;
			val=(val+1ll*y[v[i]]*invY%MOD+1ll*(Y-y[v[i]])*invY%MOD*(dp[v[i]]+1)%MOD)%MOD;
		}
	if(k==deg)
		dp[u]=1;
	else
		dp[u]=val*qpow(deg-k+MOD,MOD-2)%MOD;
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<n;i++)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	for(int i=1;i<=n;i++)
	{
		fin>>x[i]>>y[i];
		X+=x[i];Y+=y[i];
	}
	invX=qpow(X,MOD-2);
	invY=qpow(Y,MOD-2);
	int ans=0;
	for(int i=1;i<=n;i++)
		if(x[i])
		{
			dfs(i,0);
			ans=(ans+1ll*x[i]*invX%MOD*(Y-y[i])%MOD*invY%MOD*dp[i]%MOD)%MOD;
		}
	fout<<ans<<endl;
	return 0;
}
