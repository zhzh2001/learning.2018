#include<fstream>
using namespace std;
ifstream fin("aysgh.in");
ofstream fout("aysgh.ans");
const int N=125;
int p[N],a[N][N],n,m,b,tot,ans,cc;
bool sel[N][N];
void dfs(int tim,int k,int solve)
{
	if(solve==tot)
	{
		ans=tim;
		return;
	}
	if(tim>=ans)
		return;
	if(k==n+1)
		dfs(tim+1,1,solve);
	else
	{
		bool flag=false;
		for(int i=1;i<=m;i++)
			if(a[k][i]&&!sel[tim][i])
			{
				sel[tim][i]=true;
				a[k][i]--;
				dfs(tim,k+1,solve+1);
				a[k][i]++;
				sel[tim][i]=false;
				flag=true;
			}
		if(!flag)
			dfs(tim,k+1,solve);
	}
}
int main()
{
	fin>>n>>m>>b;
	for(int i=1;i<=m;i++)
		fin>>p[i];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			fin>>a[i][j];
			tot+=a[i][j];
		}
	ans=1e9;
	dfs(1,1,0);
	fout<<ans<<endl;
	return 0;
}
