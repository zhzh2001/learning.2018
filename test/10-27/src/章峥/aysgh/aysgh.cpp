#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("aysgh.in");
ofstream fout("aysgh.out");
const int N=125;
int p[N],a[N][N],n,m,b,tot,ans,cc,plan[N][N],ansplan[N][N];
bool sel[N][N];
void dfs(int tim,int k,int solve)
{
	if(solve==tot)
	{
		ans=tim;
		for(int i=1;i<=tim;i++)
			for(int j=1;j<=n;j++)
				ansplan[i][j]=plan[i][j];
		return;
	}
	if(++cc>1e7||tim>=ans)
		return;
	if(k==n+1)
		dfs(tim+1,1,solve);
	else
	{
		bool flag=false;
		for(int i=1;i<=m;i++)
			if(a[k][i]&&!sel[tim][i])
			{
				sel[tim][i]=true;
				a[k][i]--;
				plan[tim][k]=i;
				dfs(tim,k+1,solve+1);
				a[k][i]++;
				sel[tim][i]=false;
				flag=true;
			}
		if(!flag)
		{
			plan[tim][k]=0;
			dfs(tim,k+1,solve);
		}
	}
}
int main()
{
	fin>>n>>m>>b;
	for(int i=1;i<=m;i++)
		fin>>p[i];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			fin>>a[i][j];
			tot+=a[i][j];
		}
	ans=2e9;
	if(n<=4&&m<=4&&tot<=50)
		dfs(1,1,0);
	int ans2=0;
	for(int i=1;i<=n;i++)
	{
		int sum=0;
		for(int j=1;j<=m;j++)
			sum+=a[i][j];
		ans2=max(ans2,sum);
	}
	for(int j=1;j<=m;j++)
	{
		int sum=0;
		for(int i=1;i<=n;i++)
			sum+=a[i][j];
		ans2=max(ans2,sum);
	}
	fout<<min(ans,ans2)<<endl;
	for(int i=1;i<=m;i++)
		fout.put('0');
	fout<<endl;
	if(ans>ans2)
		fout<<0<<endl;
	else
	{
		int cnt=0;
		for(int i=1;i<=ans;i++)
			for(int j=1;j<=n;j++)
				cnt+=(bool)ansplan[i][j];
		fout<<cnt<<endl;
		for(int i=1;i<=ans;i++)
			for(int j=1;j<=n;j++)
				if(ansplan[i][j])
					fout<<j<<' '<<ansplan[i][j]<<' '<<i-1<<' '<<1<<endl;
	}
	return 0;
}
