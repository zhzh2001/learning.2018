#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=125;
int n,m,a[N][N],sum[N],id[N],hav[N][N],ans_num,ansi[5000000],ansj[5000000],ansx[5000000],ansy[5000000],use[N],mx;
inline bool cmp(int x,int y){return sum[x]>sum[y];}

inline bool check()
{
	int tim=0;
	memset(hav,0,sizeof hav);
	For(i,1,n)	
	{
		ansi[++ans_num]=i,ansj[ans_num]=id[1],ansx[ans_num]=tim,ansy[ans_num]=a[i][id[1]];
		For(j,1,a[i][id[1]])
			hav[tim][i]=1,tim++;
	}
	int ans=0;
	ans=max(ans,tim);
	For(j,2,m)
	{
		For(i,1,n)
		{
			tim=0;
			int alr=0;
			while(1)
			{
				if(alr>=a[i][id[j]])	break;
				if(hav[tim][i]||use[tim]){++tim;continue;}
				ansi[++ans_num]=i,ansj[ans_num]=id[j],ansx[ans_num]=tim,ansy[ans_num]=1;
				use[tim]=1;++tim;alr++;
			}
			ans=max(ans,tim);
		}
		For(t,0,ans)	use[t]=0;
	}
	writeln(ans);
	For(i,1,m)	putchar('0');
	puts("");
	if(mx>50)	{puts("0");return 1;}
	writeln(ans_num);
	For(i,1,ans_num)	{write_p(ansi[i]);write_p(ansj[i]);write_p(ansx[i]);writeln(ansy[i]);}
}
int main()
{
	freopen("aysgh.in","r",stdin);freopen("aysgh.out","w",stdout);
	n=read();m=read();read();
	For(i,1,m)	read();
	For(i,1,n)	For(j,1,m)	a[i][j]=read(),mx=max(mx,a[i][j]);
	For(j,1,m)	For(i,1,n)	sum[j]+=a[i][j];
	For(i,1,m)	id[i]=i;
	sort(id+1,id+m+1,cmp);
	check();
}
