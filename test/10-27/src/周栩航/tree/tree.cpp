#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
const int N=200005;
ll mo=998244353;
ll poi[N],nxt[N],head[N],cnt,sx,sy,x[N],y[N],n,sumx[N],sumy[N],in[N],ans;
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2,x=x*x%mo)	if(y&1)	sum=sum*x%mo;	return sum;}
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;in[x]++;in[y]++;}
inline void Dfs(int t,int fa)
{
	sumx[t]=x[t];sumy[t]=y[t];
	for(int i=head[t];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		Dfs(poi[i],t);
		sumx[t]+=sumx[poi[i]];
		sumy[t]+=sumy[poi[i]];
	}
	ans+=sumx[t]*ksm(sx,mo-2)%mo*(sy-sumy[t])%mo*ksm(sy,mo-2)%mo*(2*(in[t]-1)+1)%mo;
}
inline void Solve(int t)
{
	Dfs(t,t);
}
int main()
{
	freopen("tree.in","r",stdin);freopen("tree.out","w",stdout);
	n=read();For(i,1,n-1)	add(read(),read());
	For(i,1,n)	sx+=(x[i]=read()),sy+=(y[i]=read());
	if(n==2)
	{
		ll ans=(x[1]*ksm(sx,mo-2)%mo*y[2]%mo*ksm(sy,mo-2)%mo+x[2]*ksm(sx,mo-2)%mo*y[1]%mo*ksm(sy,mo-2)%mo)%mo;
		writeln(ans);
		return 0;
	}
	int p=0;
	For(i,1,n)	{if(in[i]==1&&p<2)	Solve(i);p++;}
	writeln(ans);
}
