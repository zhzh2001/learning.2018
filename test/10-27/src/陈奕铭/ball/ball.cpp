#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
typedef long long ll;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if (ch == '-')f = -1;ch= getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 5005;
int dp[N];
int n;
int a[N],b[N];

signed main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;++i){
		a[i] = read(); b[i] = read();
	}
	for(int i = 1;i <= n;++i)
		for(int j = n;j >= a[i];--j){
			dp[j] = max(dp[j-a[i]]+b[i],dp[j]);
		}
	printf("%d\n",dp[n]);
	return 0;
}
