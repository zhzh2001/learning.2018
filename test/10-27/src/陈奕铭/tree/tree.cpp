#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<queue>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if (ch == '-')f = -1;ch= getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int mod = 998244353;
const int N = 100005;
int n;
int a[N],b[N];
int to[N*2],nxt[N*2],head[N],cnt;

inline void insert(int x,int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}

inline int qpow(int x,int n){
	int ans = 1;
	while(n){
		if(n&1) ans = 1LL*ans*x%mod;
		x = 1LL*x*x%mod;
		n >>= 1;
	}
	return ans;
}

signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n = read();
	for(int i = 1;i < n;++i){
		int x = read(),y = read();
		insert(x,y); insert(y,x);
	}
	for(int i = 1;i <= n;++i) a[i] = read(),b[i] = read();
	if(n <= 2){
		int ans = 0;
		ans = ((1LL*a[1]*qpow((a[1]+a[2])%mod,mod-2)%mod)*(1LL*b[2]*qpow((b[1]+b[2])%mod,mod-2)%mod)%mod);
		ans = (ans+((1LL*a[2]*qpow((a[1]+a[2])%mod,mod-2)%mod)*(1LL*b[1]*qpow((b[1]+b[2])%mod,mod-2)%mod)%mod))%mod;
		printf("%d\n",ans);
	}
	return 0;
}
