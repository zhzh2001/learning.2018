#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
#define rep(i,x,y) for (register int i=(x);i<=(y);i++)
#define drp(i,x,y) for (register int i=(x);i>=(y);i--)
inline char read(){
    static const int IN_LEN=1000000;
    static char buf[IN_LEN],*s,*t;
    return (s==t?t=(s=buf)+fread(buf,1,IN_LEN,stdin),(s==t?-1:*s++):*s++);
}
template<class T>
inline void read(T &x){
    static bool iosig;
    static char c;
    for (iosig=false,c=read();!isdigit(c);c=read()){
        if (c=='-') iosig=true;
        if (c==-1) return;
    }
    for (x=0;isdigit(c);c=read()) x=((x+(x<<2))<<1)+(c^'0');
    if (iosig) x=-x;
}
const int OUT_LEN=10000000;
char obuf[OUT_LEN],*ooh=obuf;
inline void print(char c){
    if (ooh==obuf+OUT_LEN) fwrite(obuf,1,OUT_LEN,stdout),ooh=obuf;
    *ooh++=c;
}
template<class T>
inline void print(T x){
    static int buf[30],cnt;
    if (x==0) print('0');
    else{
        if (x<0) print('-'),x=-x;
        for (cnt=0;x;x/=10) buf[++cnt]=x%10+48;
        while (cnt) print((char)buf[cnt--]);
    }
}
inline void flush(){
    fwrite(obuf,1,ooh-obuf,stdout);
}
void judge(){
    freopen("tree.in","r",stdin);
    freopen("tree.out","w",stdout);
}
#define p 998244353
const int N=11000;
int n,x[N],y[N];
long long ans,X,Y;
inline long long pow(long long k,long long pp){
	long long res=1;
	while (pp){
		if (pp&1) res=res%p*k%p;
		pp>>=1;
		k=k*k%p;
	}
	return res;
}
int main(){
	judge();
	read(n);
	rep(i,1,n-1){
		int u,v;
		read(u),read(v);
	}
	rep(i,1,n){
		read(x[i]),read(y[i]);
		(X+=x[i])%p,(Y+=y[i])%p;
	}
	if (n<=1) return puts("0"),0;		
	long long t=x[1]*1LL*y[2]%p*1LL+x[2]*1LL*y[1]%p*1LL,tt=X%p*1LL*Y*1LL%p;
	t%=p;
	ans=t*1LL*pow(tt,p-2);
	ans=ans%p;
	print(ans),print('\n');
	return flush(),0;
}
/*
2
1 2
1 1
2 2


776412275
*/
