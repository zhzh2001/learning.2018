#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 10005
int n, a[N], b[N], *A = a, *B = b, vis[N], ans;
void dfs(int s){
	ans = std :: max(ans, s);
	for (register int i = 1; i <= n; ++i){
		if (vis[i]) continue;
		int j, k;
		for (j = i, k = 0; k < A[i] && j <= n; ++j) if (!vis[j]) ++k;
		if (k < A[i]) continue;
		for (j = i, k = 0; k < A[i] && j <= n; ++j) if (!vis[j]) ++k, vis[j] = i;
		dfs(s + B[i]);
		for (j = i, k = 0; k < A[i] && j <= n; ++j) if (vis[j] == i) ++k, vis[j] = 0;
	}
}
int main(){
	freopen("ball.in", "r", stdin);
	freopen("ball.out", "w", stdout);
	n = read();
	for (register int i = 1; i <= n; ++i)
		a[i] = read(), b[i] = read(), a[n + i] = a[i], b[n + i] = b[i];
	for (register int h = 1; h <= n; ++h, ++A, ++B) dfs(0);
	printf("%d", ans);
}
