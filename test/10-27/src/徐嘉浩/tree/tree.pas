var n,x,y,ans:int64; i:longint;
    a,b:array[0..100000]of int64;
    p:int64;
function kuai(k,t:int64):int64;
begin
 if t=1 then exit(k);
 if t mod 2=0 then exit(2*(kuai(k,t div 2)mod p)mod p)
 else exit((2*(kuai(k,t div 2)mod p)mod p*k)mod p);
end;
begin
 assign(input,'tree.in');
 assign(output,'tree.out');
 reset(input);
 rewrite(output);
 p:=998244353;
 read(n);
 for i:=1 to n-1 do read(x,y);
 x:=0; y:=0;
 for i:=1 to n do
 begin
  read(a[i],b[i]);
  x:=x+a[i]; y:=y+b[i];
 end;
 writeln((a[1]*kuai(x,p-2)mod p*b[2]mod p*kuai(y,p-2)mod p+
 a[2]*kuai(x,p-2)mod p*b[1]mod p*kuai(y,p-2)mod p)mod p);
 close(input);
 close(output); 
end.