#include<bits/stdc++.h>
using namespace std;
#define mod 998244353
inline int ksm(int a,int b)
{
	int res=1;
	for(;b;b>>=1,a=1ll*a*a%mod) if(b&1) res=1ll*res*a%mod;
	return res;
}
#define maxn 100100
int head[maxn],ver[maxn<<1],nxt[maxn<<1],tot;
inline void add(int a,int b)
{
	nxt[++tot]=head[a];ver[tot]=b;head[a]=tot;
	nxt[++tot]=head[b];ver[tot]=a;head[b]=tot;
}
int ans=0,n;
int vx[maxn],vy[maxn],totx,toty;
int prox[maxn],proy[maxn];
inline void init()
{
	scanf("%d",&n);
	for(int i=1,a,b;i<n;i++) scanf("%d%d",&a,&b),add(a,b);
	for(int i=1;i<=n;i++) scanf("%d%d",vx+i,vy+i),totx+=vx[i],toty+=vy[i];
	int inv=ksm(totx,mod-2);
	for(int i=1;i<=n;i++) prox[i]=1ll*vx[i]*inv%mod;
	inv=ksm(toty,mod-2);
	for(int i=1;i<=n;i++) proy[i]=1ll*vy[i]*inv%mod;
}
int size[maxn];
inline void dfs(int x,int lst,int from,int dep)
{
	size[x]=1;
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==lst) continue;
		dfs(y,x,from,dep+1);size[x]+=size[y];
	}
	if(prox[from]==0||proy[x]==0||x==from) return;
	int S=n-size[x],D=dep;
	int res=(1ll*S*S%mod-1ll*(S-D)*(S-D)%mod+mod)%mod;
//	cout<<from<<' '<<x<<' '<<S<<' '<<D<<' '<<res<<endl;
	res=1ll*prox[from]*proy[x]%mod*res%mod;
	(ans+=res)%=mod;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	init();
	for(int i=1;i<=n;i++) dfs(i,0,i,0);
	printf("%d",ans);
}
/*
2*size^2-2*size*dis+dis^2
*/
/*
..T
4 3 0
size^2-(size-dis)^2;
*/
/*
7
1 2
1 3
1 4
1 5
4 6
5 7
1 0
0 1
0 0
0 0
0 0
0 0
0 0
*/
