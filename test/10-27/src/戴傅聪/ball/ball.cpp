#include<bits/stdc++.h>
using namespace std;
int bitcnt[1<<18],f[1<<18];bool vis[1<<18];
int a[19],b[19],n;
inline int dp(int x)
{
	if(vis[x]) return f[x];vis[x]=1;
	for(int i=1;i<=n;i++) if(n-bitcnt[x]>=a[i]&&(x&(1<<(i-1)))==0)
	{
		int k=x,cnt=0,cur=i;
		while(cnt<a[i])
		{
			if(cur>n) cur=1;
			if((k&(1<<(cur-1)))!=0){cur++;continue;}
			k|=(1<<(cur-1));
			cur++;cnt++;
		}
		f[x]=max(f[x],dp(k)+b[i]);
	}
	return f[x];
}
int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	bitcnt[0]=0;for(int i=1;i<(1<<18);i++) bitcnt[i]=bitcnt[i&(i-1)]+1;
	memset(f,0,sizeof(f));
	scanf("%d",&n);
	for(int i=1;i<=n;i++) scanf("%d%d",a+i,b+i);
	printf("%d\n",dp(0));
}
