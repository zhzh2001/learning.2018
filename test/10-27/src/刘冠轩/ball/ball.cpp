#include <bits/stdc++.h>
using namespace std;
inline int read(){
    int t=0;bool p=0;char ch=getchar();
    for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
    for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
    return p?-t:t;
}
const int N=100005;
int n,m,a[N],b[N],nt[N],pre[N],x[N],to,ans,sum,now;
bool vis[N];
int main(){
    freopen("ball.in","r",stdin);
    freopen("ball.out","w",stdout);
    n=read();srand(time(0));
    for(int i=1;i<=n;i++)a[i]=read(),b[i]=read(),x[i]=i;
    for(int t=15000000/n;t;t--){
        for(int i=1;i<=n;i++)nt[i]=i+1,pre[i]=i-1,vis[i]=0;
        nt[n]=1;pre[1]=n;sum=0;now=n;
        for(int i=1;i<=n;i++)
            if(!vis[x[i]]&&a[x[i]]<=now){
                to=x[i];sum+=b[x[i]];now-=a[x[i]];
                for(int j=a[x[i]];j;j--,to=nt[to])vis[to]=1;
                nt[pre[x[i]]]=to;pre[to]=pre[x[i]];
            }
        ans=max(ans,sum);
        random_shuffle(x+1,x+n+1);
    }
    cout<<ans;
    return 0;
}
