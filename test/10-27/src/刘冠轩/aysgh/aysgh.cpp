#include <bits/stdc++.h>
#define pc putchar
using namespace std;
inline int read(){
    int t=0;bool p=0;char ch=getchar();
    for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
    for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
    return p?-t:t;
}
inline void write(int a){
    if(a<0)a=-a,pc('-');
    if(a>=10)write(a/10);
    pc('0'+a%10);
}
inline void writeln(int a){write(a); pc('\n');}
inline void wri(int a){write(a); pc(' ');}
int n,m,b,ans,t[108],T;
bool vis[108];
struct node{int s[108];}a[108];
struct ANS{int a,b,x,y;}Ans[5000005];
bool cmp(node x,node y){return x.s[0]>y.s[0];}
int main(){
    freopen("aysgh.in","r",stdin);
    freopen("aysgh.out","w",stdout);
    n=read();m=read();b=read();
    for(int i=1;i<=m;i++)read();
    for(int i=1;i<=n;i++){
        for(int j=1;j<=m;j++){
            a[i].s[j]=read();
            a[i].s[0]+=a[i].s[j];
            t[j]+=a[i].s[j];
        }
        ans=max(ans,a[i].s[0]);
    }
    for(int i=1;i<=m;i++)ans=max(ans,t[i]);
    writeln(ans);
    for(int i=1;i<=m;i++)pc('0');
    pc('\n');
    sort(a+1,a+n+1,cmp);
    if(ans>100000){puts("0");return 0;}
    for(int i=0;i<ans;i++){
        memset(vis,0,sizeof(vis));
        for(int j=1;j<=n;j++){
            for(int k=1;k<=m;k++)
                if(!vis[k]&&a[j].s[k]){
                    vis[k]=1;
                    a[j].s[k]--;
                    Ans[++T].a=j;
                    Ans[T].b=k;
                    Ans[T].x=i;
                    Ans[T].y=1;
                    break;
                }
        }
    }
    writeln(T);
    for(int i=1;i<=T;i++){
        wri(Ans[i].a);
        wri(Ans[i].b);
        wri(Ans[i].x);
        writeln(Ans[i].y);
    }
    return 0;
}
