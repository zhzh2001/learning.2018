#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
    int t=0;bool p=0;char ch=getchar();
    for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
    for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
    return p?-t:t;
}
const int P=998244353;
int n,ans,x[10],y[10],X,Y;
int ksm(int a,int b){
    if(b==1)return a;
    int s=ksm(a,b/2);
    s*=s;
    if(b&1)s*=a;
    return s%P;
}
signed main(){
    freopen("tree.in","r",stdin);
    freopen("tree.out","w",stdout);
    n=read();
    for(int i=1;i<n;i++)read(),read();
    x[1]=read();y[1]=read();
    x[2]=read();y[2]=read();
    X=x[1]+x[2];Y=y[1]+y[2];
    ans=(x[1]*y[2]+y[1]*x[2]);
    ans*=ksm(X,P-2)%P*ksm(Y,P-2)%P;
    cout<<ans%P;
    return 0;
}
