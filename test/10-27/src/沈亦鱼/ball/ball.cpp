#include<cstdio>
#include<algorithm>
using namespace std;
int n,a[5100],b[5100],f[5100];
int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		b[i]=read();
	}
	for(int i=1;i<=n;i++)
		for(int j=n;j>=a[i];j--)
			f[j]=max(f[j],f[j-a[i]]+b[i]);
	printf("%d",f[n]);
	return 0;
}
