#include<cstdio>
#include<algorithm>
using namespace std;
int n,l,r,ul1,ur1,ul2,ur2,ans,a[5100],b[5100],f[110][110][110];
int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		b[i]=read();
		f[1][i][i+a[i]-1]=b[i];
	}
	for(int i=2;i<=n;i++)
		for(int l1=1;l1<=n;l1++)
			for(int r1=1;r1<=n;r1++)
				for(int l2=1;l2<=n;l2++)
					for(int r2=1;r2<=n;r2++)
						if(l1!=l2){
							ul1=l1;
							ur1=r1;
							if(ur1<ul1)ur1+=n;
							ul2=l2;
							ur2=r2;
							if(ur2<ul2)ur2+=n;
							if(ur1>=ul2){
								l=ul1;
								r=ur2+ur1-ul2+1;
							}
							else{
								l=ul1;
								r=ur2;
							}
								if(l+n<=r)continue;
								r=(r-1)%n+1;
//								if(l==3&&r==1){
//									printf("%d %d %d %d\n",l1,r1,l2,r2);
//								}
//								if(l>r)swap(l,r);
								f[i][l][r]=max(f[i][l][r],f[i-1][l1][r1]+f[1][l2][r2]);
						}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++){
				ans=max(ans,f[k][i][j]);
//				if(f[k][i][j]==16)printf("%d %d %d\n",k,i,j);
			}
//	printf("%d\n",f[2][3][4]);
	printf("%d",ans);
	return 0;
}
