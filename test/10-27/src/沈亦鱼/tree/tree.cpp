#include<cstdio>
using namespace std;
int mo=998244353,n,u,v,tot,p[210000],ne[210000],he[110000],x[110000],y[110000],son[110000];
long long sy,s,sx[110000];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
long long pwr(long long x,int y){
	long long z=1;
	while(y){
		if(y&1)z=z*x%mo;
		x=x*x%mo;
		y>>=1;
	}
	return z;
}
void adg(int u,int v){
	tot++;
	p[tot]=v;
	ne[tot]=he[u];
	he[u]=tot;
}
void dfs(int k,int fa){
	sx[k]=x[k];
	son[k]=1;
	for(int i=he[k];i;i=ne[i])
		if(p[i]!=fa){
			dfs(p[i],k);
			son[k]+=son[p[i]];
			sx[k]+=sx[p[i]];
		}
}
void sol(int k,int fa){//printf("%d\n",s);
	for(int i=he[k];i;i=ne[i])
		if(p[i]!=fa){
			sol(p[i],k);
			if(sx[1]-sx[p[i]]>0){
				s+=(long long)y[p[i]]*((sx[1]-sx[p[i]])*(son[1]-son[p[i]])*2-1)%mo;
				if(s>mo)s-=mo;
				if(s<0)s+=mo;
			}
			if(sx[p[i]]>0){
				s+=(long long)y[k]*(sx[p[i]]*son[p[i]]*2-1)%mo;
				if(s>mo)s-=mo;
				if(s<0)s+=mo;
			}
		}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		u=read();
		v=read();
		adg(u,v);
		adg(v,u);
	}
	for(int i=1;i<=n;i++){
		x[i]=read();
		y[i]=read();
		sy+=y[i];
	}
	dfs(1,0);
	sol(1,0);
	s=s*pwr(sx[1]*sy%mo,mo-2)%mo;
	printf("%d",s);
	return 0;
}/*
f[i]=p*f[j]+q*f[k]+(p+q)*p*f[j]+(p+q)*q*f[k]
=p*f[j]+q*f[k]+p*p*f[j]+p*q*f[j]+p*q*f[k]+q*q*f[k]

f[i]=(1-s)*f[i]+(1-s)*(1-s)*f[i]
1*p*f[j]+(1-s)*p*f[j]+(1-s)*(1-s)*p*f[j]

f[s]=(1-p)*f[s]+x
f[s]=x/p
*/
