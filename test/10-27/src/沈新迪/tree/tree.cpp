#include<bits/stdc++.h>
using namespace std;
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return ff*x;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
const long long mod=998244353;
long long n,X,Y,ans;
long long xx[100005],yy[100005];
long long f[100005],S[100005],g[100005];
long long sum[100005];
long long tot,head[100005],nx[200005],to[200005];
void jia(long long aa,long long bb)
{
	tot++;
	nx[tot]=head[aa];
	to[tot]=bb;
	head[aa]=tot;
	return;
}
long long ksm(long long aa,long long bb)
{
	long long ans=1;
	while(bb)
	{
		if(bb&1) ans=(ans*aa)%mod;
		bb>>=1;
		aa=(aa*aa)%mod;
	}
	return ans;
}
void dfs(long long rt,long long fa)
{
	f[rt]=1;
	for(long long i=head[rt];i;i=nx[i])
	{
		long long yy=to[i];
		if(yy==fa) continue;
		dfs(yy,rt);
		f[rt]=(f[rt]+f[yy]+1)%mod;
	}
	return;
}
void getans(long long rt,long long fa)
{
	sum[rt]=xx[rt];//cout<<sum[rt]<<" "<<rt<<endl;
	for(long long i=head[rt];i;i=nx[i])
	{
		long long yy=to[i];
		if(yy==fa) continue;
		getans(yy,rt);
		sum[rt]=(sum[rt]+sum[yy])%mod;
		S[rt]=(S[rt]+S[yy]+sum[yy]*f[yy]%mod)%mod;
	}
	return;
}
void qwq(long long rt,long long fa)
{
	ans=(ans+g[rt]*yy[rt]%mod)%mod;
	for(long long i=head[rt];i;i=nx[i])
	{
		long long yy=to[i];
		if(yy==fa) continue;
		g[yy]=(g[rt]-sum[yy]*f[yy]%mod+(sum[1]-sum[yy]+mod)%mod*(f[1]-f[yy]-1+mod)%mod+mod)%mod;
		qwq(yy,rt);
	}
	return;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(long long i=1;i<n;++i)
	{
		long long x=read(),y=read();
		jia(x,y);jia(y,x);
	}
	for(long long i=1;i<=n;++i) xx[i]=read(),yy[i]=read(),X=(X+xx[i])%mod,Y=(Y+yy[i])%mod;
	X=ksm(X,mod-2);Y=ksm(Y,mod-2);
	dfs(1,1);
	getans(1,1);//cout<<sum[3]<<endl;
	g[1]=S[1];//cout<<g[1]<<endl;
	qwq(1,1);
	ans=ans*X%mod*Y%mod;
	write(ans);
	return 0;
}
