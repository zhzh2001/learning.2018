#include <bits/stdc++.h>

#define Max 205

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Ans{
	int i,j,x,y;
}c[Max*Max];

int l,r,n,m,T,b,ans,mid,p[Max],sum1[Max],sum2[Max],num[Max*2],a[Max][Max];
bool vis[Max];

inline bool check(int x){
	int now=0;
	memset(vis,0,sizeof vis);
	for(int i=1;i<=n;i++){
		if(sum1[i]>x)return false;
	}
	for(int i=1;i<=m;i++){
		if(sum2[i]>x){
			if(sum2[i]<=x*2){
				vis[i]=true;
				now+=p[i];
				if(now>b)return false;
			}else{
				return false;
			}
		}
	}
	return true;
}

int main(){
	freopen("aysgh.in","r",stdin);
	freopen("aysgh.out","w",stdout);
	n=read();m=read();b=read();
	for(int i=1;i<=m;i++)p[i]=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			a[i][j]=read();
			sum1[i]+=a[i][j];
			sum2[j]+=a[i][j];
		}
	}
	l=1;r=1e9;
	while(l<=r){
		mid=(l+r)>>1;
		if(check(mid)){
			ans=mid;
			r=mid-1;
		}else{
			l=mid+1;
		}
	}
	writeln(ans);
//	return 0;
	check(ans);
	for(int i=1;i<=m;i++)write(vis[i]);
	puts("");
	for(int i=1;i<=n;i++){
		int t=0;
		for(int j=1;j<=m;j++){
			int now=min(ans-num[j],a[i][j]);
			num[j]+=now;
			a[i][j]-=now;
			c[++T].i=i;
			c[T].j=j;
			c[T].x=t;
			c[T].y=now;
			t+=now;
			if(a[i][j]){
				num[j+m]+=a[i][j];
				c[++T].i=i;
				c[T].j=j+m;
				c[T].x=t;
				c[T].y=a[i][j];
				t+=a[i][j];
				a[i][j]=0;
			}
//			cout<<T<<endl;
		}
	}
	writeln(T);
	for(int i=1;i<=T;i++){
		write(c[i].i);putchar(' ');
		write(c[i].j);putchar(' ');
		write(c[i].x);putchar(' ');
		write(c[i].y);puts("");
	}
	return 0;
}
