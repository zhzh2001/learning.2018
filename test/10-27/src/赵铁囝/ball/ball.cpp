#include <bits/stdc++.h>

#define Max 5005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int n,ans,a[Max],b[Max],f[Max][Max];

int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();b[i]=read();
	}	
	memset(f,-0x3f,sizeof f);
	f[n+1][0]=0;
	for(int i=n;i>=1;i--){
		for(int j=0;j<=n;j++){
			f[i][j]=max(f[i][j],f[i+1][j]);
			if(j-a[i]>=0)f[i][j]=max(f[i][j],f[i+1][j-a[i]]+b[i]);
//			cout<<i<<" "<<j<<" "<<f[i][j]<<endl;
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=n;j>=0;j--){
			ans=max(ans,f[i][j]);
		}
	}
	writeln(ans);
	return 0;
}
/*
4
1 2
1 3
2 10
1 2
*/
