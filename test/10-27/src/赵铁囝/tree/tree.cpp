#include <bits/stdc++.h>

#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

const int wzp=9982544353;

struct Edge{
	int v,to;
}e[Max*2];

int n,l,r,size,a[Max],b[Max],f[Max],head[Max];

inline void add(int u,int v){
	e[++size].v=v;e[size].to=head[u];head[u]=size;
}

int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		l=read();r=read();
		add(l,r);
	}
	for(int i=1;i<=n;i++){
		a[i]=read();
		b[i]=read();
	}
	return 0;
}
