#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Mul(x,y)	((x)=(x)*(y)%mod)
#define Add(x,y)	((x)=((x)+(y))%mod)
ll read(){
	ll x=0,f=1;	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	puts("");
}
void Max(ll &x,ll y){x=x<y?y:x;}
const ll N=2000;
ll ans,n,m,b,p[N],should[N][N],vis[N][N],Vis[N][N];
int main(){
	srand(time(0));
	ll n=10,X=8,ans=0,tim=0;
	For(n,2,10){
		For(X,1,n-1){
			ans=0;
			For(tim,1,10000000){
				ll now=0,x=X;
				for(;;){
					++now;
					if (x==1)++x;
					else x+=(rand()&1)?1:-1;
					if (x==n)break;
				}ans+=now;
			}
			printf("%lf	",1.0*ans/10000000);
		}
		puts("");
	}
}
