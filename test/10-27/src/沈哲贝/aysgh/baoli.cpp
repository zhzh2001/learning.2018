#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
ll read(){
	ll x=0,f=1;	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	puts("");
}
const ll N=5010;
vector<ll>g[N];
ll n,vis[N],cnt[N];
int main(){
	srand(time(0));
	n=read();
	For(i,1,n-1){
		ll x=read(),y=read();
		g[x].push_back(y),g[y].push_back(x);
	}
	For(i,1,100000){
		memset(vis,0,sizeof vis);vis[1]=1;
		ll x=1,ci=0;
		For(j,1,100){
			x=g[x][rand()%g[x].size()];
			++ci;
			if (!vis[x]){
				vis[x]=1;
				cnt[x]+=ci;
			}
		}
	}
	For(i,1,n)write(cnt[i]),putchar(' ');
}
