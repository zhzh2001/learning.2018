#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Mul(x,y)	((x)=(x)*(y)%mod)
#define Add(x,y)	((x)=((x)+(y))%mod)
ll read(){
	ll x=0,f=1;	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	puts("");
}
const ll N=400010,mod=998244353;
vector<ll>g[N];
ll a[N],b[N],x[N],y[N],xx[N],yy[N],total_x,total_y,n,ans,nn,Root;
ll ppow(ll x,ll k){
	ll ans=1;for(;k;k>>=1,Mul(x,x))if (k&1)Mul(ans,x);
	return ans;
}
void dfs(ll x,ll fa){
	a[++nn]=xx[x];	b[nn]=yy[x];
	if ((g[x].size()==1)&&fa)		return;
	if (g[x][0]==fa)dfs(g[x][1],x);
	else			dfs(g[x][0],x);
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	if (n==1)return puts("0"),0;
	rep(i,1,n){
		ll x=read(),y=read();
		g[x].push_back(y);
		g[y].push_back(x);
	}
	For(i,1,n)	total_x+=(x[i]=read()),
				total_y+=(y[i]=read());
	For(i,1,n)	xx[i]=x[i]*ppow(total_x,mod-2)%mod,
				yy[i]=y[i]*ppow(total_y,mod-2)%mod;
	For(i,1,n)if (g[i].size()==1){
		Root=i;
		break;
	}
	dfs(Root,0);
	For(i,1,n)Add(a[i],a[i-1]),Add(b[i],b[i-1]);
	For(i,1,n){
		Add(ans,-(a[i]-a[i-1])*(i-1)%mod*(i-1)%mod*(b[n]-b[i]));
		Add(ans,(b[i]-b[i-1])*(i-1)%mod*(i-1)%mod*a[i-1]);
	}
	FOr(i,n,1)Add(a[i],-a[i-1]),Add(b[i],-b[i-1]);
	reverse(a+1,a+n+1);
	reverse(b+1,b+n+1);
	For(i,1,n)Add(a[i],a[i-1]),Add(b[i],b[i-1]);
	For(i,1,n){
		Add(ans,-(a[i]-a[i-1])*(i-1)%mod*(i-1)%mod*(b[n]-b[i]));
		Add(ans,(b[i]-b[i-1])*(i-1)%mod*(i-1)%mod*a[i-1]);
	}
	writeln((ans+mod)%mod);
}
/*
O(n)树上高斯消元 
gai_lv[1][i]=gai_lv[1][j]*fang_an[j][i]
3
1 2
1 3
1 0
0 2
0 3

g[x][y1]=(g[y2][x]+g[y3][x])/3+1
g[y][x1]=(g[x1][y]+g[x2][y]+g[x3][y])/4+1

*/
