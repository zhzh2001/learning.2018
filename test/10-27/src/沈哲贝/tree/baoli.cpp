#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Mul(x,y)	((x)=(x)*(y)%mod)
#define Add(x,y)	((x)=((x)+(y))%mod)
ll read(){
	ll x=0,f=1;	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	puts("");
}
const ll N=400010,mod=998244353;
vector<ll>g[N];
ll x[N],y[N],total_x,total_y,n,ans;
ll ppow(ll x,ll k){
	ll ans=1;for(;k;k>>=1,Mul(x,x))if (k&1)Mul(ans,x);
	return ans;
}
int main(){
	n=read();
	rep(i,1,n){
		ll x=read(),y=read();
		g[x].push_back(y);
		g[y].push_back(x);
	}
	For(i,1,n)	total_x+=(x[i]=read()),
				total_y+=(y[i]=read());
	For(i,1,n)	x[i]=x[i]*ppow(total_x,mod-2)%mod,
				y[i]=y[i]*ppow(total_y,mod-2)%mod;
	For(i,1,n){
		dfs(i,0,x[i]);
		
	}
	writeln(ans);
}
/*
O(n)树上高斯消元 
*/
