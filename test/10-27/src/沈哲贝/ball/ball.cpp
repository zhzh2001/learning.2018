#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
ll read(){
	ll x=0,f=1;	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	puts("");
}
const ll N=210;
ll f[N][N],a[N],b[N],n,ans;
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x>y?y:x;}
void work(){
	memset(f,-60,sizeof f);
	f[n+1][0]=0;
	FOr(i,n,1)For(j,-1,n){
		if (j>=0)Max(f[i][j+1],f[i+1][j]);
		if (j+a[i]<=n-i+1)Max(f[i][j+1],f[i+1][j+a[i]]+b[i]);
	}
	For(i,0,n)Max(ans,f[1][i]);
}
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n=read();
	For(i,1,n)a[i]=read(),b[i]=read();
	For(i,1,n){
		work();
		For(i,0,n-1)a[i]=a[i+1],b[i]=b[i+1];
		a[n]=a[0],b[n]=b[0];
	}writeln(ans);
}
