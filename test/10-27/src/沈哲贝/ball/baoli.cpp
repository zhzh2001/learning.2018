
/*
4
1 2
1 3
2 10
1 2
*/
#include<bits/stdc++.h>
using namespace std;
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
ll read(){
	ll x=0,f=1;	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)putchar('-'),x=-x;
	if (x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	puts("");
}
const ll N=5010;
ll choice[N][N*2],a[N],b[N],g[N*2],h[N*2],f[N*2],u[N*2],n,ans;
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x>y?y:x;}
void Dp1(){
	memset(choice,-60,sizeof choice);
	memset(f,-60,sizeof f);
	f[n]=0;
	FOr(i,n,1){
		memset(u,-60,sizeof u);
		For(j,0,n*2){
			if (j-a[i]+1>=0){
				Max(u[j-a[i]+1],f[j]+b[i]);
				choice[i][j-a[i]+1]=f[j]+b[i];
				writeln(j-a[i]+1);
			}
			if (j+1<=2*n)	Max(u[j+1],f[j]);
		}
		FOr(j,2*n,0){
			Max(choice[i][j],choice[i+1][j]);
			Max(choice[i][j],choice[i][j+1]);
		}
		memcpy(f,u,sizeof u);
	}
	ans=choice[1][n];
	For(i,1,n){
		For(j,0,2*n)write(choice[i][j]),putchar(' ');
		puts("");
	}
	writeln(ans);
	for(;;);
}
void Dp2(){
	memset(g,-60,sizeof g);
	g[n]=0;
	For(i,1,n){
		memset(h,-60,sizeof h);
		For(j,0,n*2){
			if (j-1>=0)			Max(h[j],g[j-1]);//多存一个
			if (j+a[j]-1<=2*n)	Max(h[j],g[j]+b[i]);//强制选这个位置 
		}
		memcpy(g,h,sizeof h);
		For(j,n,n*2)
			if (j>=n)Max(ans,g[j]+choice[i+1][2*n-j]);
//		writeln(ans);
	}
}
int main(){
	freopen("ball.in","r",stdin);
	n=read();
	For(i,1,n)a[i]=read(),b[i]=read();
	Dp1();Dp2();
	writeln(ans);
}
/*
choice[i][j]
4
1 2
1 2
1 3
2 10
*/
