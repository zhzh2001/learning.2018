#include <bits/stdc++.h>
#define N 100020
#define mod 998244353
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

inline long long fast_pow(long long x, long long y) {
  long long z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}
inline long long inv(long long x) {
  return fast_pow(x, mod - 2);
}

long long a[N], b[N];
long long total_a, total_b, total, ans;

int to[N<<1], nxt[N<<1], head[N], cnt, inq[N];
void insert(int x, int y) {
  ++ inq[x]; ++ inq[y];
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
  to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}
int sz[N], n, rp[N];
void dfs(int x, int f) {
  sz[x] = 1; rp[x] = 1;
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    dfs(to[i], x);
    sz[x] += sz[to[i]];
    a[x] += a[to[i]];
    rp[x] = rp[x] * (rp[to[i]] + 1) % mod;
    ans = (ans + a[to[i]] * (rp[to[i]] + 1) % mod * b[x] % mod) % mod;
  }
  rp[x] = rp[x] * inv(inq[x]) % mod;
  ans = (ans + (total_a - a[x]) * (n * inv(rp[x])) % mod * b[x] % mod) % mod;
}

int main(int argc, char const *argv[]) {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);

  n = read();


  for (int i = 1; i < n; ++ i) {
    insert(read(), read());
  }
  for (int i = 1; i <= n; ++ i) {
    total_a += a[i] = read();
    total_b += b[i] = read();
  }

  total = total_a * total_b % mod;

  if (n == 1) {
    puts("0");
  } else if (n == 2) {
    printf("%lld\n", (a[1] * inv(total_a) % mod * (b[2] * inv(total_b) % mod) % mod
                    + a[2] * inv(total_a) % mod * (b[1] * inv(total_b) % mod) % mod) % mod);
  } else {
    dfs(1, 0);
    printf("%lld\n", ans * inv(total) % mod);
  }

  return 0;
}