#include <bits/stdc++.h>
#define N 5020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int a[N], b[N], f[N<<1][N], cnt;
int main(int argc, char const *argv[]) {
  freopen("ball.in", "r", stdin);
  freopen("ball.out", "w", stdout);

  int n = read();
  for (int i = 1; i <= n; ++ i) {
    a[i + n] = a[i] = read();
    b[i + n] = b[i] = read();
  }

  int ans = 0;
  for (int l = 1; l < n; ++ l) {
    int r = l + n - 1;
    for (int i = r; i >= l; -- i) {
      for (int j = 0; j <= r-i+1; ++ j) {
        f[i][j] = f[i + 1][j - 1];
      }
      for (int j = a[i]; j <= r-i+1; ++ j) {
        f[i][j - a[i]] = max(f[i][j - a[i]], f[i + 1][j - 1] + b[i]);
      }
    }
    for (int i = 0; i <= n; ++ i) {
      ans = max(ans, f[l][i]);
    }
  }
  printf("%d\n", ans);

  return 0;
}
