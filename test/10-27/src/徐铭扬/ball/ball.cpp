#include<bits/stdc++.h>
using namespace std;
const int N=30;
int ans,a[N],b[N],n,i;
bool vis[N];
void dfs(int x){
	if (x>ans) ans=x;
	for (int i=0;i<n;i++)
		if (!vis[i]){
			int cnt=0,rec[11];
			vis[i]=1;
			for (int j=(i+1)%n;j!=i && cnt<a[i]-1;j=(j+1)%n)
				if (!vis[j]) vis[j]=1,rec[cnt++]=j;
			if (cnt==a[i]-1) dfs(x+b[i]);
			vis[i]=0;
			for (int j=0;j<cnt;j++) vis[rec[j]]=0;
		}
}
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	scanf("%d",&n);
	for (i=0;i<n;i++) scanf("%d%d",&a[i],&b[i]);
	dfs(0);
	printf("%d",ans);
}
