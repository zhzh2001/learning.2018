#include<bits/stdc++.h>
using namespace std;
const int N=10002;
int n,i,l,r,j,f[N][N],ans,a[N],b[N];
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
int main(){
//	freopen("ball.in","r",stdin);
//	freopen("ball.out","w",stdout);
	n=rd();
	for (i=1;i<=n;i++) a[i]=a[i+n]=rd(),b[i]=b[i+n]=rd();
	for (i=1;i<2*n;i++)
		if (i+a[i]-1<2*n) f[i][i+a[i]-1]=b[i];
	for (i=2*n-1;i;i--)
		for (j=i+a[i]-1;j<=2*n-1;j++)
			for (l=i+1;l<=j;l++){
				r=j-i+l-a[i];
				if (r<=j){
					f[i][j]=max(f[i][j],f[l][r]+b[i]);
				}
			}
	for (i=1;i<=n;i++)
		for (j=i;j<=i+n-1;j++) ans=max(ans,f[i][j]),printf("f[%d][%d]=%d\n",i,j,f[i][j]);
	printf("%d",ans);
}
/*
4
1 2
1 3
2 10
1 2
*/
