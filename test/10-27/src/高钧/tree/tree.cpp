#include <bits/stdc++.h>
#define PB push_back
using namespace std;
const int N=100005,md=998244353;
int n,X[N],Y[N],XX,YY;
inline int ksm(int x,int y){int re=1;while(y){if(y&1)re=re*x%md; x=x*x%md; y>>=1;}return re;}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int i,x,y; scanf("%d",&n);
	for(i=1;i<n;i++)
	{
		scanf("%d%d",&x,&y);
	}for(i=1;i<=n;i++)scanf("%d%d",&X[i],&Y[i]),XX+=X[i],YY+=Y[i];
	printf("%d\n",(X[1]*Y[2]+X[2]*Y[1])*ksm(XX*YY,md-2)%md);
}
