#include <bits/stdc++.h>
using namespace std;
const int N=5005;
int n,f[2][N],cnt=0,re=0;
struct ball{int a,b;}ba[N],bb[N];
inline void solve()
{
	int o,i,j; memset(f,0,sizeof f); f[0][o=0]=0;
	for(i=1;i<=n;i++)
	{
		o^=1; for(j=1;j<=i;j++)f[o][j]=max(f[o][j],f[o^1][j-1]);
		for(j=0;j<=i;j++)if(j+ba[i].a-1<=i-1&&j+ba[i].a-1>=0)
		{
			f[o][j]=max(f[o][j],f[o^1][j+ba[i].a-1]+ba[i].b);
		}
	}for(i=0;i<=n;i++)re=max(re,f[o][i]);
}
int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	int i,t; scanf("%d",&n); for(i=1;i<=n;i++)scanf("%d%d",&bb[i].a,&bb[i].b),ba[i]=bb[i];
	if(n<=18)
	{
		for(t=1;t<=n;t++)
		{
			cnt=0; for(i=t;i<=n;i++)ba[++cnt]=bb[i]; for(i=1;i<t;i++)ba[++cnt]=bb[i]; solve();
		}printf("%d\n",re);
	}
	else
	{
		for(t=n-5;t<=n;t++)
		{
			random_shuffle(ba+1,ba+n+1); solve();
		}printf("%d\n",re);
	}
}
