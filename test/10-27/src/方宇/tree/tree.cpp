#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy (998244353*1ll)
#define N 100010
using namespace std;
LL a[N],b[N],c[N],up[N],down[N],sum[N],sum_up[N],fa[N],x[N],y[N];
LL n,m,i,j,k,l,r,X,Y,ans;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
void add(LL k,LL l,LL r){a[k]=r; b[k]=c[l]; c[l]=k;}
LL pow(LL k,LL v)
{
	LL sum=1;
	while(v)
	{
		if(v&1) sum=(sum*k)%fy;
		k=(k*k)%fy;
		v/=2;
	}
	return sum;
}
void dfs_up(LL k,LL fm)
{
	if(k!=1) up[k]=sum[k];
	for(LL j=c[k];j;j=b[j])
	  if(fm!=a[j])
	  {
	  	dfs_up(a[j],k);
	  	if(k!=1) up[k]=(up[k]+up[a[j]])%fy;
	  	sum_up[k]=(sum_up[k]+up[a[j]])%fy; 
	  }
}
void dfs_down(LL k,LL fm)
{
	for(LL j=c[k];j;j=b[j])
	  if(fm!=a[j])
	  {
	  	down[a[j]]=(down[k]+sum[k]+sum_up[k]-up[a[j]])%fy;
	  	dfs_down(a[j],k);
	  }
}
void dfs(LL begin,LL k,LL fm,LL sum)
{
	ans=(ans+(((sum*x[begin])%fy*y[k])%fy*pow(X,fy-2))%fy*pow(Y,fy-2))%fy;
	for(LL j=c[k];j;j=b[j])
	  if(fm!=a[j])
	  {
	  	if(a[j]==fa[k]) dfs(begin,a[j],k,sum+up[k]);
	  	else dfs(begin,a[j],k,sum+down[a[j]]);
	  }
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	rep(i,1,n-1)
	{
		l=read(); r=read();
		sum[l]++; sum[r]++;
		add(i*2-1,l,r); add(i*2,r,l);
	}
	rep(i,1,n)
	{
		x[i]=read(); X+=x[i];
		y[i]=read(); Y+=y[i];
	}
	dfs_up(1,0);
	dfs_down(1,0);
	rep(i,1,n) dfs(i,i,0,0);
	printf("%lld",ans);
	return 0;
}
