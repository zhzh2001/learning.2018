#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 50010
using namespace std;
LL a[N],b[N],f[N];
LL n,m,i,j,k;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n=read();
	rep(i,1,n){a[i]=read(); b[i]=read();}
	rep(i,1,n)
	  dep(j,n,a[i]) f[j]=max(f[j],f[j-a[i]]+b[i]);
	printf("%lld",f[n]);
	return 0;
}
