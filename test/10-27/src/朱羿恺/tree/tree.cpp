#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10, mod = 998244353;
int n,x,y;
ll a[N],b[N],suma,sumb;
int tot,first[N],last[N<<1],to[N<<1];
inline void Add(int x,int y){to[++tot]=y,last[tot]=first[x],first[x]=tot;}
inline ll power(int x,int y){
	int ans=1;
	for (;y;y>>=1,x=1ll*x*x%mod) if (y&1) ans=1ll*ans*x%mod;
	return ans;
}
namespace Subtask1{
	inline ll gcd(ll a,ll b){return !b?a:gcd(b,a%b);}
	inline void solve(){
		ll ans=a[1]*b[2]+b[1]*a[2],Ans=suma*sumb,Gcd=gcd(ans,Ans);
		ans/=Gcd,Ans/=Gcd;
		printf("%d",ans%mod*power(Ans%mod,mod-2)%mod);
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	For(i,1,n-1) x=read(),y=read(),Add(x,y),Add(y,x);
	For(i,1,n) a[i]=read(),b[i]=read(),suma+=a[i],sumb+=b[i];
	if (n==2) return Subtask1::solve(),0;
}
/*
2
1 2
5 3
4 9
*/
