#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 5010;
int n,ans,a[N],b[N];
namespace Subtask1{
	int dp[300010],ans,p[20];
	inline void solve(){
		p[0]=1;
		For(i,1,n) p[i]=p[i-1]<<1;
		For(i,0,p[n]-1){
			int sum=0;ans=max(ans,dp[i]);
			For(j,1,n) if (p[j-1]&i) sum++;
			For(j,1,n) 
				if (!(p[j-1]&i)){
					if (a[j]>n-sum) continue;
					int last=i;
					for (register int k=1,now=j;k<=a[j];now=(now==n)?1:now+1) 
						if (!(p[now-1]&i)) last+=(p[now-1]),k++;
					dp[last]=max(dp[last],dp[i]+b[j]);
				}
		}
		printf("%d\n",ans);
	}
}
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n=read();
	For(i,1,n) a[i]=read(),b[i]=read();
	if (n<=18) return Subtask1::solve(),0;
}
/*
4
1 2
1 3
2 10
1 2
*/
