#include <bits/stdc++.h>
using namespace std;
#define file "tree"
typedef long long ll;
int read(){
	char c=getchar();
	int x=0;
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
const int maxn=100002;
const int mod=998244353;
ll power(ll x,ll y){
	x%=mod;
	ll ans=1;
	while(y){
		if (y&1)
			ans=ans*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return ans;
}
struct graph{
	int n,m;
	struct edge{
		int to,next;
	}e[maxn*2];
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	ll sz[maxn],sx[maxn];
	ll x[maxn],y[maxn];
	ll ans,tot;
	void dfs(int u,int fa){
		sz[u]=1;
		sx[u]=x[u];
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa)
				continue;
			dfs(v,u);
			sz[u]+=sz[v];
			sx[u]+=sx[v];
		}
		sx[u]%=mod;
		tot=(tot+(sz[u]*2-1)*sx[u])%mod;
	}
	ll d1[maxn],d2[maxn];
	void dfs2(int u,int fa){
		d1[u]=(d1[fa]+(sz[u]*2-1)*sx[u])%mod;
//		if (u==1)
//			printf("tot=%lld d1=%lld d2=%lld\n",tot,d1[u],d2[u]);
		ans=(ans+(tot-d1[u]+d2[u])*y[u])%mod;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa)
				continue;
			d2[v]=(d2[u]+((n-sz[v])*2-1)*(sx[1]-sx[v]))%mod;
			dfs2(v,u);
		}
	}
	ll work(){
		dfs(1,0);
//		fprintf(stderr,"WTF\n");
		dfs2(1,0);
		return ans;
	}
}g;
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=g.n=read();
	ll qx=0,qy=0;
	for(int i=1;i<n;i++){
		int u=read(),v=read();
		g.addedge(u,v);
		g.addedge(v,u);
	}
//	fprintf(stderr,"WTF\n");
//	int st=0,en=0;
	for(int i=1;i<=n;i++){
		g.x[i]=read(),g.y[i]=read();
//		if (g.y[i])
//			en=i;
//		if (g.x[i])
//			st=i;
		qx=(qx+g.x[i])%mod;
		qy=(qy+g.y[i])%mod;
	}
//	fprintf(stderr,"st=%d en=%d\n",st,en);
	ll qwq=power(qx*qy%mod,mod-2);
//	fprintf(stderr,"WTF\n");
	ll ans=g.work();
//	printf("%lld\n",ans);
	ans=ans*qwq%mod;
//	fprintf(stderr,"WTF\n");
	printf("%lld",(ans%mod+mod)%mod);
}
