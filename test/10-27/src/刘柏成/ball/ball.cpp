#include <bits/stdc++.h>
using namespace std;
#define file "ball"
int read(){
	char c=getchar();
	int x=0;
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
int f[5002];
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++){
		int x=read(),y=read();
		for(int j=n;j>=x;j--)
			f[j]=max(f[j],f[j-x]+y);
	}
	printf("%d",f[n]);
}
