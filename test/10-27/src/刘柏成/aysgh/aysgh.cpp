#include <bits/stdc++.h>
using namespace std;
#define file "aysgh"
int read(){
	char c=getchar();
	int x=0;
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
const int maxn=1000002;
const int maxm=10000002;
const int INF=0x3f3f3f3f;
struct graph{
	int n,m;
	graph(){
		m=1;
	}
	struct edge{
		int to,cap,next;
	}e[maxm];
	int first[maxn],head[maxn];
	int addedge(int from,int to,int cap){
		e[++m]=(edge){to,cap,first[from]};
		first[from]=m;
		e[++m]=(edge){from,0,first[to]};
		first[to]=m;
		return m-1;
	}
	int d[maxn];
	int t;
	int q[maxn];
	bool bfs(int s){
		int l=1,r=1;
		q[1]=s;
		memset(d,-1,(n+1)*4);
		d[s]=0;
		while(l<=r){
			int u=q[l++];
			for(int i=first[u];i;i=e[i].next){
				int v=e[i].to;
				if (d[v]!=-1 || !e[i].cap)
					continue;
				d[v]=d[u]+1;
				q[++r]=v;
				if (v==t)
					return true;
			}
		}
		return false;
	}
	int dfs(int u,int flow){
		if (u==t)
			return flow;
		for(int& i=head[u];i;i=e[i].next){
			int v=e[i].to;
			if (!e[i].cap || d[v]!=d[u]+1)
				continue;
			int res=dfs(v,min(flow,e[i].cap));
			if (res){
				e[i].cap-=res;
				e[i^1].cap+=res;
				return res;
			}
		}
		return 0;
	}
	int ans;
	int dinic(int s,int t){
		this->t=t;
		while(bfs(s)){
			for(int i=1;i<=n;i++)
				head[i]=first[i];
			int flow;
			while(flow=dfs(s,INF))
				ans+=flow;
		}
		return ans;
	}
}g;
int idx[52][52];
int p[52],a[52][52];
int tat[1002][52][52];
struct ans{
	int i,j,x,y;
}stk[5000002];
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read(),m=read(),b=read(),sum=0;
	int s=1,t=2;
	g.n=2;
	for(int i=1;i<=m;i++)
		p[i]=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			idx[i][j]=++g.n;
			g.n++;
			int x=read();
			sum+=x;
			g.addedge(idx[i][j],idx[i][j]+1,x);
		}
//	printf("g.n=%d\n",g.n);
	for(int o=1;;o++){
		for(int i=1;i<=n;i++){
			int qwq=++g.n;
			g.addedge(s,qwq,1);
			for(int j=1;j<=m;j++)
				tat[o][i][j]=g.addedge(qwq,idx[i][j],1);
		}
		for(int i=1;i<=m;i++){
			int qwq=++g.n;
			g.addedge(qwq,t,1);
			for(int j=1;j<=n;j++)
				g.addedge(idx[j][i]+1,qwq,1);
		}
		if (g.dinic(s,t)==sum){
			printf("%d\n",o);
			for(int i=1;i<=m;i++)
				putchar('0');
			puts("");
			int cur=0;
			for(int i=1;i<=n;i++)
				for(int j=1;j<=o;j++){
					for(int k=1;k<=m;k++)
						if (!g.e[tat[j][i][k]].cap){
							stk[++cur]=(ans){i,k,j,1};
							if (cur>5000000)
								return puts("0"),0;
						}
				}
			printf("%d\n",cur);
			for(int i=1;i<=cur;i++)
				printf("%d %d %d %d\n",stk[i].i,stk[i].j,stk[i].x-1,stk[i].y);
			return 0;
		}
	}
}
