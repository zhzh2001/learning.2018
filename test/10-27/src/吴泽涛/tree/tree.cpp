#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 1011, Mod = 998244353; 
int n, X, Y;  
struct node{
	int x, y; 
}a[N];

inline int ksm(int x, int y) {
	int b[40], tot = 0; 
	while(y) {
		b[++tot] = y&1; 
		y/=2; 
	}
	int res = 1; 
	Dow(i, tot, 1) {
		res = 1ll*res*res %Mod; 
		if(b[i]) res = 1ll*res*x %Mod; 
	}
	return res; 
}

inline int inv(int x) {
	return ksm(x, Mod-2); 
}

inline void work_1() {
	int ans = 0; 
	int u = read(), v = read(); 
	a[1].x = read(); a[1].y = read(); 
	a[2].x = read(); a[2].y = read(); 
	X = a[1].x+a[2].x; X = inv(X); 
	Y = a[1].y+a[2].y; Y = inv(Y);  
	ans = (1ll*a[1].x*X%Mod*a[2].y%Mod*Y%Mod + 1ll*a[2].x*X%Mod*a[1].y%Mod*Y%Mod) %Mod; 
	printf("%d\n", ans); 
	exit(0); 
}

int main() {
	freopen("tree.in", "r", stdin); 
	freopen("tree.out", "w", stdout);  
	n = read(); 
	if(n <= 2) work_1(); 
	puts("233"); 
}



/*

2
1 2
2 3
3 2


2
1 2
3 5
5 3



*/





