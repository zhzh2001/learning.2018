#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 10011, M = 300011; 
int n, ans;
int fa[N], bin[N+11], dp[M], vis[N], visit[N], f[N];
int fade[N], b[N], tamp[N];

inline void work_1() {
	dp[0] = 0;
	For(i, 1, n) fa[i] = i; 
	bin[0] = 1; 
	For(i, 1, n) bin[i] = bin[i-1]*2;  
	For(i, 0, bin[n]-1){
		int len=0; 
		if(dp[i] > ans) ans = dp[i]; 
		For(j, 1, n) {
			if(!(bin[j-1]&i)) {
				tamp[++len]=j;
			}
		}
		For(j, 1, len){
			int wzt=0, tot=0, fake=j;
			if (fade[tamp[j]]<=len) {
				while (tot<fade[tamp[j]]){
					wzt = wzt|(bin[tamp[fake]-1]);
					fake=fake%len+1; ++tot;
				}
				dp[i|wzt]=max(dp[i|wzt],dp[i]+b[tamp[j]]);
			}
		}
	}
	writeln(ans); 
	exit(0); 
}

int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n = read();
	For(i, 1, n) {
		fade[i] = read(); 
		b[i] = read();
	}
	For(i, 0, M-11) dp[i] = -1;  
	if(n <= 18) work_1(); 
	For(i, 1, n) 
		Dow(j, n, fade[i]) 
			f[j] = max(f[j], f[j-fade[i]]+b[i]); 
	int ans = -100; 
	For(i, 0, n) if(f[i] > ans) ans = f[i]; 
	writeln(ans); 
}



