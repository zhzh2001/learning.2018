#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 5005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,a[N],b[N],f[N],ans;
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=read(),b[i]=read();
	rep(i,1,n) per(j,n,0)
		if (j-a[i]>=0) f[j]=max(f[j],f[j-a[i]]+b[i]);
	rep(i,0,n) ans=max(ans,f[i]);
	printf("%d",ans);
}
