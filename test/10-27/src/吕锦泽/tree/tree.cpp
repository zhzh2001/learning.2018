#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define mod 998244353
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
struct edge{ ll to,next; }e[N<<1];
ll n,X,Y,x[N],y[N],tot,head[N];
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
ll qpow(ll x,ll y){
	ll num=1; x%=mod;
	for (;y;y>>=1,x=x*x%mod)
		if (y&1) num=num*x%mod;
	return num;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	rep(i,2,n){
		ll u=read(),v=read();
		add(u,v); add(v,u);
	}
	rep(i,1,n){
		X+=x[i]=read();
		Y+=y[i]=read();
	}
	if (n<=2){
		if (n==1) puts("0");
		else printf("%lld",(x[1]*y[2]%mod+x[2]*y[1]%mod)%mod*qpow(X*Y,mod-2)%mod);
		return 0;
	}
}
/*
2
1 2
1 1
1 1

*/
