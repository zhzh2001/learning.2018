#include <iostream>
#include <cstdio>
#define int long long
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c>='0'&&c<='9')sum=sum*10+c-'0',c=getchar();return sum*tot;}
const int mod=998244353;
int ed[10],be[10];
inline int power(int x,int k){
	int ans=1;
	for(;k;k>>=1,x=1ll*x*x%mod)if(k&1)ans=1ll*ans*x%mod;
	return ans%mod;
}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int n=read(),x=0,y=0;
	for(int i=1;i<n;i++){
		int x=read(),y=read();
	}
	for(int i=1;i<=n;i++){
		be[i]=read();ed[i]=read();
		x+=be[i];y+=ed[i];
	}
	int X=power(x,mod-2),Y=power(y,mod-2);
//	cout<<X<<" "<<Y<<endl;
	int ans=be[1]%mod*X%mod*ed[2]%mod*Y%mod;
	ans=(ans+be[2]%mod*X%mod*ed[1]%mod*Y%mod)%mod;
	cout<<ans<<endl;
	return 0;
}
/*
2
1 2
6 6 6 6
*/
