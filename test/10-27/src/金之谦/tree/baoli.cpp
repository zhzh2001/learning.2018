#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int MOD=998244353,N=1e5+10;
int n,f[N],d[N],g[N];
int nedge=0,p[2*N],nex[2*N],head[2*N];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
int fa[N][20],inv[N],dep[N],sf[N],sg[N];
inline int mi(int a,int b){
	int x=1,y=a;
	for(;b;b>>=1,y=y*y%MOD)if(b&1)x=x*y%MOD;
	return x;
}
inline void dfs(int x,int Fa){
	fa[x][0]=Fa;dep[x]=dep[Fa]+1;
	for(int j=1;j<20;j++)fa[x][j]=fa[fa[x][j-1]][j-1];
	for(int k=head[x];k;k=nex[k])if(p[k]!=Fa){
		dfs(p[k],x);
		f[x]=(f[x]+inv[d[x]-1]*(1+f[p[k]])%MOD)%MOD;
	}
	f[x]=(f[x]+1)%MOD;
//	cout<<x<<" pp "<<f[x]<<endl;
}
inline void dfss(int x,int Fa){
//	cout<<x<<" qq "<<g[x]<<endl;
	sf[x]=(f[x]+sf[Fa])%MOD;
	sg[x]=(g[x]+sg[Fa])%MOD;
	int kp=Fa?inv[d[x]-1]*(1+g[x])%MOD:0;
	for(int k=head[x];k;k=nex[k])if(p[k]!=Fa){
		kp=(kp+inv[d[x]-1]*(1+f[p[k]])%MOD)%MOD;
	}
//	cout<<kp<<endl;
	for(int k=head[x];k;k=nex[k])if(p[k]!=Fa){
		g[p[k]]=((kp-inv[d[x]-1]*(1+f[p[k]])%MOD+MOD)%MOD+1)%MOD;
		dfss(p[k],x);
	}
}
inline int LCA(int x,int y){
	if(dep[x]<dep[y])swap(x,y);
	for(int i=19;i>=0;i--)if(dep[fa[x][i]]>=dep[y])x=fa[x][i];
	for(int i=19;i>=0;i--)if(fa[x][i]!=fa[y][i])x=fa[x][i],y=fa[y][i];
	if(x!=y)x=fa[x][0];return x;
}
int X[N],Y[N];
signed main()
{
//	freopen("tree.in","r",stdin);
//	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)inv[i]=mi(i,MOD-2);
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addedge(x,y);addedge(y,x);
		d[x]++;d[y]++;
	}
	dfs(1,0);dfss(1,0);
	int sumx=0,sumy=0;
	for(int i=1;i<=n;i++){
		X[i]=read(),Y[i]=read();
		sumx+=X[i];sumy+=Y[i];
	}
	int ans=0,invx=mi(sumx,MOD-2),invy=mi(sumy,MOD-2);
//	cout<<invx<<' '<<invy<<endl;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)if(i!=j){
			int gl=X[i]*invx%MOD*Y[j]%MOD*invy%MOD;
//			cout<<i<<' '<<j<<' '<<gl<<endl;
			if(gl==0)continue;
			int lca=LCA(i,j);
			gl=gl*(((sf[i]-sf[lca]+MOD)%MOD+(sg[j]-sg[lca]+MOD)%MOD)%MOD)%MOD;
//			cout<<gl<<endl;
			ans=(ans+gl)%MOD;
		}
	writeln(ans);
	return 0;
}
/*
f[i]:从i向上到fa期望 
g[i]:从fa向下到i期望 
f[i]=sigma(E(f[p[k]+1))+1=(1/(d[i]-1))*sigma(f[p[k]+1)+1;
g[i]=sigma(E(f[p[k]+1))+E(g[fa]+1)+1
*/
