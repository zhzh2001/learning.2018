#pragma GCC optimize 2
#include <bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int f[220][25], s[220][220], g[220][220][220];
int mp[220][220], cx[220][220], cy[220][220];
int calcx(int x, int l, int r) { // ----
  // printf("%d | %d | %d\n", x, l, r);
  return cx[x][r] - cx[x][l - 1];
}
int calcy(int y, int l, int r) { // |
  return cy[y][r] - cy[y][l - 1];
}
int main(int argc, char const *argv[]) {
  freopen("draw.in", "r", stdin);
  freopen("draw.out", "w", stdout);
  int n = read(), m = read(), k = read();
  if (m < k * 4 - 1 || n < 3) return puts("Orz J!!!"), 0;
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= m; j++) {
      mp[i][j] = read();
      cx[i][j] = cx[i][j - 1] + mp[i][j];
      cy[j][i] = cy[j][i - 1] + mp[i][j];
    }
  }
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= m; j++) {
      g[i][j][2] = mp[i][j]+mp[i][j-1];
      for (int s = 3; s <= j; s++) {
        g[i][j][s] = max(calcx(i, j-s+1, j), g[i][j][s-1]);
      }
    }
  }
  // cout << g[3][2][2] << endl;
  // for (int i = 1; i <= n; i++, puts(""))
  //   for (int j = 1; j <= m; j++)
  //     printf("%d ", cx[i][j]);


  // calc the f[l][r][1] which means [l, r] has 1 J the max ans.

  memset(f, -63, sizeof f);
  memset(s, -63, sizeof s);
  for (int i = 2; i <= m; i += 2) {
    for (int j = 1; j <= m - i; j++) { // [l, r]
      int l = j, r = j + i;
      int ans = - 1 << 30;
      // printf("calc (%d,%d)\n", l, r);
      for (int t = 1; t <= n - 2; t++) { // h of top
        for (int len = 1; len <= n - t - 1; len++) { // length of b
          // for (int cl = 2; cl <= (r-l)/2+1; cl++) { // length of c
            int a = calcx(t, l, r);
            int b = calcy((r+l)/2, t+1, t+len);
            int c = g[t+len+1][(r+l)/2][(r-l)/2+1];
            // printf("When t=%d,len=%d,cl=%d, a=%d,b=%d,c=%d\n", t, len, cl, a, b, c);
            ans = max(ans, a+b+c);
          // }
        }
      }
      s[l][r] = ans;
    }
  }
  for (int i = 3; i <= m; i++) {
    for (int j = 1; j <= m - i; j++) {
      int l = j, r = j + i;
      s[l][r] = max(s[l][r], max(s[l][r-1], s[l+1][r]));
    }
  }
  // for (int i = 1; i <= m; i++) {
  //   for (int j = i+1; j <= m; j++) {
  //     if (s[i][j] != -1044266559) printf("s[%d][%d] = %d\n", i ,j, s[i][j]);
  //   }
  // }
  // cout << "ayariki" << endl;

  // for (int l = 1; l <= m; l++) {
  //   for (int r = l + 2; r <= m; r++) {
  //     printf("f[%d][%d][1] = %d\n", l, r, f[l][r][1]);
  //   }
  // }

  // count answer.
  for (int i = 1; i <= m; i++)
    f[i][1] = s[1][i];

  for (int j = 2; j <= k; j++) {
    for (int i = 1; i <= m; i++) {
      // f[i][j]
      for (int w = 0; w < i-1; w++) {
        f[i][j] = max(f[i][j], f[w][j - 1] + s[w+2][i]);
        // printf("f[%d][%d] <= f[%d][%d]+s[%d][%d] => %d\n", i,j,w,j-1,w+2,i,f[i][j]);
        // printf("f[%d][%d] = %d\n", w, j-1, f[w][j-1]);
      }
    }
  }

  // cout << f[1][3][1] << endl;
  // cout << f[5][9][1] << endl;
  // cout << f[11][17][1] << endl;
  // cout << f[1][9][2] << endl;
  cout << f[m][k] << endl;
  return 0;
}