#include <bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
// 20% banzai!
int main(int argc, char const *argv[]) {
  freopen("draw.in", "r", stdin);
  freopen("draw.out", "w", stdout);
  int n = read(), m = read(), k = read();
  if (m < k * 4 - 1 || n < 3) return puts("Orz J!!!"), 0;
  if (!(m & 1)) m --;
  int a = m - k + 1;
  int b = (n - 2) * k;
  int c = (a - k) / 2 + k;
  printf("%d\n", a + b + c);
  return 0;
}