#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
	while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int x1[N], y1[N], x2[N], y2[N];
ll calc(int t, int x) {
	ll xx = 1ll + min(x2[x], t) - max(x1[x], -t);
	ll yy = 1ll + min(y2[x], t) - max(y1[x], -t);
	if (xx <= 0 || yy <= 0) return 0;
	return xx * yy;
}
int main(int argc, char const *argv[]) {
	freopen("paint.in", "r", stdin);
	freopen("paint.out", "w", stdout);
	int n = read();
	for (int i = 1; i <= n; i++) {
		x1[i] = read();
		y1[i] = read();
		x2[i] = read();
		y2[i] = read();
	}
	int q = read();
	// 暴力出奇迹，n^2过10w
	while (q --) {
		int t = read();
		ll ans = 0;
		for (int i = 1; i <= n; i++)
			ans += calc(t, i);
		cout << ans << endl;
	}
	return 0;
}
// ass we can...
