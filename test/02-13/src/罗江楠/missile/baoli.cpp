#include <bits/stdc++.h>
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
	while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
const double PI = 3.141592653589793;
double calc(double a, int v0, int k1, int k2) {
	double vx = cos(a) * v0;
	double vy = sin(a) * v0;
	double t  = vy / 10;
	double X  = 2 * vx * t;
	double Y  = .5 * vy * t;
	return k1 * X + k2 * Y;
}
int main(int argc, char const *argv[]) {
	freopen("missile.in", "r", stdin);
	freopen("missile.ans", "w", stdout);
	// printf("%.15lf\n", acos(-1));
	int T = read();
	while (T --) {
		int v0 = read(), k1 = read(), k2 = read();
		double ans = 0;
		int s = 0, k = 0;
		for (s = 0; s <= 1571; s++) {
			double res = calc(s / 1000., v0, k1, k2);
			if (res >= ans) ans = res, k = s;
			else break;
		}
		printf("%.3lf %.3lf\n", k / 1000., ans);
	}
	return 0;
}