#include <bits/stdc++.h>
using namespace std;
int main(int argc, char const *argv[]) {
	srand(time(0));
	freopen("missile.in", "w", stdout);
	int T = 1000;
	cout << T << endl;
	while (T --) {
		int v = rand() * rand() % 10 + 1;
		int k1 = rand() * rand() % 11;
		int k2 = rand() * rand() % 11;
		while (k1 + k2 == 0) {
			k1 = rand() * rand() % 11;
			k2 = rand() * rand() % 11;
		}
		cout << v << " " << k1 << " " << k2 << endl;
	}
	return 0;
}
