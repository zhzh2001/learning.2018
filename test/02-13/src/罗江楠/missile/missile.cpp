// orz szb
// orz szb
// orz szb
// orz szb
// orz szb
// orz zyy
// orz zyy
// orz zyy
// orz zyy
// orz zyy

#include <bits/stdc++.h>
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
	while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
const double PI = 3.141592653589793;
int v0, k1, k2;
int main(int argc, char const *argv[]) {
	freopen("missile.in", "r", stdin);
	freopen("missile.out", "w", stdout);
	// printf("%.15lf\n", acos(-1));
	int T = read();
	while (T --) {
		v0 = read(); k1 = read(); k2 = read();
		printf("%.3lf %.3lf\n", atan2(4*k1, -k2) / 2, (sqrt(4*k1*k1+.25*k2*k2)+k2*.5)*v0*v0*.05);
	}
	return 0;
}
