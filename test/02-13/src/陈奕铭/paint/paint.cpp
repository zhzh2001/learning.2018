#include<bits/stdc++.h>
using namespace std;
const int LEN = 200005;
char STR[LEN],*ST = STR,*ED = STR;
inline char gc(){
	if(ST == ED){
		ED = (ST = STR) + fread(STR,1,LEN,stdin);
		if(ST == ED) return EOF;
	}
	return *ST++;
}
inline int read(){
	int x = 0,f = 1;char ch = gc();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = gc();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = gc();}
	return x*f;
}
#define ll long long

const int N = 100005;
int n,num,numl;
int add,fu;
int m;
int q[N];
bool vis[N*8];
ll Ans[N];
struct node{
	int x;
	int val;
	int id;
	friend bool operator < (node a,node b){
		return a.x < b.x;
	}
}lie[N*2],Right[N*8],up[N*8];
struct juj{
	int x,y;
}ju[N*8];

inline void jiantu(int x1,int x2,int y1,int y2){
	if(y1 > 1 && x1 > 1){	//下左有空隙 
		Right[++num] = (node){x2,1,num};
		up[num] = (node){y2,1,num};
		ju[num] = (juj){x2,y2};
		Right[++num] = (node){x1-1,1,num};
		up[num] = (node){y1-1,1,num};
		ju[num] = (juj){x1-1,y1-1};
		Right[++num] = (node){x2,-1,num};
		up[num] = (node){y1-1,-1,num};
		ju[num] = (juj){x2,y1-1};
		Right[++num] = (node){x1-1,-1,num};
		up[num] = (node){y2,-1,num};
		ju[num] = (juj){x1-1,y2};
	}
	else if(y1 > 1){	//下有空隙 
		Right[++num] = (node){x2,1,num};
		up[num] = (node){y2,1,num};
		ju[num] = (juj){x2,y2};
		Right[++num] = (node){x2,-1,num};
		up[num] = (node){y1-1,-1,num};
		ju[num] = (juj){x2,y1-1};
	}
	else if(x1 > 1){	//左有空隙 
		Right[++num] = (node){x2,1,num};
		up[num] = (node){y2,1,num};
		ju[num] = (juj){x2,y2};
		Right[++num] = (node){x1-1,-1,num};
		up[num] = (node){y2,-1,num};
		ju[num] = (juj){x1-1,y2};
	}
	else{
		Right[++num] = (node){x2,1,num};
		up[num] = (node){y2,1,num};
		ju[num] = (juj){x2,y2};
		add += 1;
	}
}

void solve1(){
	sort(lie+1,lie+numl+1);
	int top = 1;
	ll sum = 0,ans = 0;
	int Mx = q[m],top2 = 1;
	for(int i = 1;i <= numl;++i)
		sum += lie[i].val;
	for(int i = 1;i <= Mx;++i){
		ans += sum;
		if(q[top2] == i){
			Ans[top2] += ans;
			++top2;
		}
		while(top <= numl && lie[top].x <= i)
			sum -= lie[top++].val;
	}
}

void solve2(){
	sort(Right+1,Right+1+num);
	sort(up+1,up+num+1);
	int topr = 1,topu = 1;
	ll sumr = 0,sumu = 0,ans = 0;
	int Mx = q[m],top2 = 1;
	for(int i = 1;i <= Mx;++i){
		ans += sumu+sumr+add;
		sumu += add;
		sumr += add;
		if(q[top2] == i){
			Ans[top2] += ans;
			++top2;
		}
		while(topr <= num && Right[topr].x <= i){
			if(!vis[Right[topr].id])
				sumr -= Right[topr].x*Right[topr].val;
			else sumr -= ju[Right[topr].id].y*Right[topr].val;
			if(!vis[Right[topr].id]){
				vis[Right[topr].id] = true;
				add -= Right[topr].val;
			}
			++topr;
		}
		while(topu <= num && up[topu].x <= i){
			if(!vis[up[topu].id])
				sumu -= up[topu].x*up[topu].val;
			else sumu -= ju[up[topu].id].x*up[topu].val;
			if(!vis[up[topu].id]){
				vis[up[topu].id] = true;
				add -= up[topu].val;
			}
			++topu;
		}
	}
}

int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;++i){
		int x1 = read(),y1 = read(),x2 = read(),y2 = read();
		// 翻转至上方 
		if(y2 < 0){ //xia
			int b1 = y1,b2 = y2;
			y1 = -b2;
			y2 = -b1;
		}
		else if(x2 < 0){ //zuo
			int a1 = x1,a2 = x2,b1 = y1,b2 = y2;
			y1 = -a2; y2 = -a1; x1 = b1; x2 = b2;
		}
		else if(x1 > 0){	//you
			int a1 = x1,a2 = x2,b1 = y1,b2 = y2;
			y1 = a1; y2 = a2; x1 = -b2; x2 = -b1;
		}
		if(x1 < 0){	//有左边部分 
			if(x2 >= 0)
				jiantu(1,-x1,y1,y2);
			else jiantu(-x2,-x1,y1,y2);
		}
		if(x2 > 0){	//有右边部分 
			if(x1 <= 0)
				jiantu(1,x2,y1,y2);
			else jiantu(x1,x2,y1,y2);
		}
		if(x1 <= 0 && x2 >= 0){	//有中间部分 
			if(y1 > 1) lie[++numl] = (node){y1-1,-1,numl};
			lie[++numl] = (node){y2,1,numl};
		}
	}
	m = read();
	for(int i = 1;i <= m;++i) q[i] = read();
	solve1();
	solve2();
	for(int i = 1;i <= m;++i)
		printf("%lld\n",Ans[i]);
	return 0;
}
