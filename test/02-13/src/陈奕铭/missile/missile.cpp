#include<bits/stdc++.h>
using namespace std;
const int LEN = 200005;
char STR[LEN],*ST = STR,*ED = STR;
inline char gc(){
	if(ST == ED){
		ED = (ST = STR) + fread(STR,1,LEN,stdin);
		if(ST == ED) return EOF;
	}
	return *ST++;
}
inline int read(){
	int x = 0,f = 1;char ch = gc();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = gc();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = gc();}
	return x*f;
}

int T;
int v0,k1,k2,v2;
double a,tana,ans,ansa;
double v3,sum;

int main(){
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	T = read();
	while(T--){
		v0 = read(); k1 = read(); k2 = read();
		if(k1 == 0)
			printf("1.571 %.3lf\n",k2*(v0*v0)/20.0);
		else if(k2 == 0)
			printf("0.785 %.3lf\n",v0*v0*k1/10.0);
		else{
			a = 0.001;
			ans = 0;
			v3 = v0*v0/20.0;
			while(a <= 1.571){
				tana = tan(a);
				sum = (k2*tana*tana+4*k1*tana)/(tana*tana+1);
				if(sum > ans){
					ans = sum;
					ansa = a;
				}
				a += 0.001;
			}
			printf("%.3lf %.3lf\n",ansa,ans*v3);
		}
	}
	return 0;
}
