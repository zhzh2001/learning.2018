#include<bits/stdc++.h>
#define ll long long
#define N 205
#define inf 1000000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
int k,a[N][N],n,m,b[N][N][N],c[N][N],f[N][25],ans=-inf;
inline void cwo(){
	For(i,1,n) For(j,1,m) c[i][j]=c[i][j-1]+a[i][j];
	memset(b,-63,sizeof b);
	For(i,1,n) Rep(j,m,1) Rep(k,j-2,0) b[i][k+1][j]=max(c[i][j]-c[i][k],b[i][k+2][j]);
}
bool flag;
int C[N],ttt;
inline int solve(int l,int r,int x){
	int cnt=0,mx=-inf;
	if(flag){ttt+=a[x+1][r];return C[x+2]-ttt+a[x+1][r];}
	memset(C,-63,sizeof C);
	For(i,x+1,n-1){
		cnt+=a[i][r];
		mx=max(mx,cnt+b[i+1][l][r]);
		C[i+1]=cnt+b[i+1][l][r];
	}
	Rep(i,n-1,x+2) C[i]=max(C[i],C[i+1]);
	ttt+=a[x+1][r];
	flag=1;
	return mx;
}
int main(){
	freopen("draw.in","r",stdin);
	freopen("draw.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	For(i,1,n) For(j,1,m) scanf("%d",&a[i][j]);
	if(k*4-1>m||n<3){puts("Orz J!!!");return 0;}
	cwo();memset(f,-63,sizeof f);f[0][0]=0;
	For(i,1,m+1) For(j,0,k){
		f[i][j]=max(f[i][j],f[i-1][j]);
		if(j==k) continue;
		for(int K=3;K+i-1<=m;K+=2){
			flag=0;ttt=0;
			For(p,1,n-3){
				int cnt=c[p][K+i-1]-c[p][i-1];
				cnt+=solve(i,i+K/2,p);
				f[min(i+K,m+1)][j+1]=max(f[min(i+K,m+1)][j+1],cnt+f[i-1][j]);
			}
		}
	}
	printf("%d\n",f[m+1][k]);
	return 0;
}
