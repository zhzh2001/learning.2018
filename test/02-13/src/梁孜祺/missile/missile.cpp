#include<bits/stdc++.h>
#define pi 3.1415926535897932384626433832795
#define jb 1e-15
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int T,v0,k1,k2;
inline void solve(){
	double p=1.0*v0*v0/10;
	if(k2==0){
		printf("0.785 ");
		printf("%.3lf\n",1.0*p*k1);
	}else if(k1==0){
		printf("1.571 ");
		printf("%.3lf\n",1.0*p*(1.0*k2/2));
	}else{
		double K1=1.0*k1,K2=1.0*k2/4;
		double P=sqrt(K1*K1+K2*K2);
		printf("%.3lf %.3lf\n",atan2(K1,-K2)/2,p*(P+K2));
	}
}
int main(){
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	T=read();
	while(T--) v0=read(),k1=read(),k2=read(),solve();
	return 0;
}
