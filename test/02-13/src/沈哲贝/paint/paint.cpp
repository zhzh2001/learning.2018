#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define p(x)	printf("%lld ",x)
#define pp(x,y)	printf("%lld %lld\n",x,y)
using namespace std;
ll read(){char ch=getchar();ll x=0,f=1;for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
void write(ll x){if (x<0)putchar('-'),x=-x;if (x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const ll N=1000010;
ll n,Q,Sqr[N],Sam[N],answ[N];
void cover(ll x1,ll yA,ll x2,ll y2,ll det){
	if (x1<0&&x2>0)	return cover(0,yA,0,y2,-det),cover(0,yA,x2,y2,det),cover(x1,yA,0,y2,det),void(0);
	if (yA<0&&y2>0)	return cover(x1,0,x2,0,-det),cover(x1,0,x2,y2,det),cover(x1,yA,x2,0,det),void(0);
	if (x1>0)		return cover(0,yA,x2,y2,det),cover(0,yA,x1-1,y2,-det),void(0);
	if (x2<0)		return cover(x1,yA,0,y2,det),cover(x2+1,yA,0,y2,-det),void(0);
	if (yA>0)		return cover(x1,0,x2,y2,det),cover(x1,0,x2,yA-1,-det),void(0);
	if (y2<0)		return cover(x1,yA,x2,0,det),cover(x1,y2+1,x2,0,-det),void(0);
	ll X1=abs(x1),YA=abs(yA),X2=abs(x2),Y2=abs(y2);
	Sqr[min(max(X1,X2),max(YA,Y2))]+=det;
	Sam[min(max(X1,X2),max(YA,Y2))]-=det*(min(max(X1,X2),max(YA,Y2))+1);
	Sam[max(max(X1,X2),max(YA,Y2))]+=det*(min(max(X1,X2),max(YA,Y2))+1);
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	For(i,1,n){
		ll x1=read(),yA=read(),x2=read(),y2=read();
		cover(x1,yA,x2,y2,1);//return 0;
	}
	FOr(i,1000000,0)Sqr[i]+=Sqr[i+1],Sam[i]+=Sam[i+1];
	For(i,0,1000000)answ[i]=Sqr[i]*(2*i+1)+Sam[i];
	For(i,1,1000000)answ[i]+=answ[i-1];
	Q=read();
	For(i,1,Q){
		ll x=read();
		writeln(answ[x]);
	}
}
