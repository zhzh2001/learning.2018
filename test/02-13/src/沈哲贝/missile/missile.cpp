#include<bits/stdc++.h>
#define ll long long
#define ld double
#define pi acos(-1) 
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define Max(x,y)	x=(x)<(y)?(y):(x)
#define Min(x,y)	x=(x)<(y)?(x):(y) 
using namespace std;
ll read(){char ch=getchar();ll x=0,f=1;for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
void write(ll x){if (x<0)putchar('-'),x=-x;if (x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const ld g=10;
ld V,k2,k1,ans,cho,add,NOW,Ans,v1,v2,t,up,right,ans_now;
inline bool change(ld a){
	bool fl=0;
	v1=V*sin(a),v2=V*cos(a),t=v1/g,ans_now=(v1*t-g*t*t/2)*k1+(t*2*v2)*k2;
	if (ans_now>=ans)ans=ans_now,cho=a,1;
	if (ans_now>=Ans)return Ans=ans_now,1;
	return 0;
} 
int main(){
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	ll T=read(),sum=T;
	while(T--){
		V=read(),k2=read(),k1=read(),ans=0,cho=0,add=sum<=100?0.002:0.5;
		for(ld a=0;a<=pi/2;a+=add){
			Ans=0;
			ld l=a,r=min((double)pi/2,(double)a+add),mid1,mid2;
			For(TIME,1,30){
				mid1=(l+l+r)/3;
				mid2=(l+r+r)/3;
				change(mid1);
				if (change(mid2))l=mid1;
				else	r=mid2;
			}
		}
		printf("%.3lf %.3lf\n",cho,ans);
	}
}
/*
v sin a	��ֱ 
v cos a ˮƽ
t=(v sin a /g)
up->	gt^2/2 
right=	2t*v cos a
*/
