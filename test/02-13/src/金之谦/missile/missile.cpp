#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef double D;
const D pi=acos(-1);
const D eps=1e-9;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int v,k1,k2;
inline D sqr(D x){return x*x;}
inline D cal(D a){
	D vup=v*sin(a),vgo=v*cos(a);
	D t=vup/10*2,lup=sqr(vup)/20;
	D lgo=vgo*t;
	return lgo*k1+lup*k2;
}
int main()
{
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	for(int T=read();T;T--){
		v=read();k1=read(),k2=read();
		D l=pi/4,r=pi/2;
		while(r-l>eps){
			D kp=(r-l)/3;
			D m1=l+kp,m2=m1+kp;
			if(cal(m1)>cal(m2))r=m2;
			else l=m1;
		}
		printf("%.3lf %.3lf\n",l,cal(l));
	}
	return 0;
}
