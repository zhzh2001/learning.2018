#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
struct ppap{int x1,y1,x2,y2;}a[100010],b[100010];
int t[100010],n;
inline int xx(int x,int y){
	if(x>0&&y>0)return 1;
	if(x<0&&y>0)return 2;
	if(x<0&&y<0)return 3;
	if(x>0&&y<0)return 4;
	return 0;
}
inline int cal(int xx,int yy,int x,int y,int v){
	if(xx>v||yy>v)return 0;
	int rx=min(x,v),ry=min(y,v);
	return (rx-xx+1)*(ry-yy+1);
}
signed main()
{
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	int cnt=0;
	for(int i=1;i<=n;i++){
		a[i].x1=read();a[i].y1=read();
		a[i].x2=read();a[i].y2=read();
		int p1=xx(a[i].x1,a[i].y1),p2=xx(a[i].x2,a[i].y2);
		if(p1!=p2&&p1*p2){
			if(p1*p2==4){
				b[++cnt]=(ppap){a[i].x1,1,a[i].x2,a[i].y2};
				b[++cnt]=(ppap){a[i].x1,1,a[i].x2,-a[i].y1};
				b[++cnt]=(ppap){a[i].x1,0,a[i].x2,0};
			}else if(p1*p2==2){
				b[++cnt]=(ppap){1,a[i].y1,-a[i].x1,a[i].y2};
				b[++cnt]=(ppap){1,a[i].y1,a[i].x2,a[i].y2};
				b[++cnt]=(ppap){0,a[i].y1,0,a[i].y2};
			}else if(p1*p2==6){
				b[++cnt]=(ppap){-a[i].x2,1,-a[i].x1,a[i].y2};
				b[++cnt]=(ppap){-a[i].x2,1,-a[i].x1,-a[i].y1};
				b[++cnt]=(ppap){-a[i].x2,0,-a[i].x1,0};				
			}else if(p1*p2==12){
				b[++cnt]=(ppap){1,-a[i].y2,a[i].x2,-a[i].y1};
				b[++cnt]=(ppap){1,-a[i].y2,-a[i].x1,-a[i].y1};
				b[++cnt]=(ppap){0,-a[i].y2,0,-a[i].y1};				
			}
		}else{
			int xx=abs(a[i].x1),yy=abs(a[i].y1),rx=abs(a[i].x2),ry=abs(a[i].y2);
			int px=min(xx,rx),py=min(yy,ry);rx=max(xx,rx);ry=max(yy,ry);
			b[++cnt]=(ppap){px,py,rx,ry};
		}
	}
	int m=read();
	for(int i=1;i<=m;i++){
		t[i]=read();int ans=0;
		for(int j=1;j<=cnt;j++)ans+=cal(b[j].x1,b[j].y1,b[j].x2,b[j].y2,t[i]);
		writeln(ans);
	}
	return 0;
}
