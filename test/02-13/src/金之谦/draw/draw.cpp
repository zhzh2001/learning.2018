#pragma GCC optimize("Ofast")
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define max(x,y) ((x)>(y))?(x):(y)
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int f[210][30],a[110][210],p[110][210][210],q[110][210][210],n,m,K;
int main()
{
	freopen("draw.in","r",stdin);
	freopen("draw.out","w",stdout);
	n=read();m=read();K=read();
	int i,j,k,l,kp;
	for(i=1;i<=n;i++)
		for(j=1;j<=m;j++)a[i][j]=read();
	memset(f,-0x3f,sizeof f);
	memset(p,-0x3f,sizeof p);
	memset(q,-0x3f,sizeof q);
	f[0][0]=0;
	for(i=1;i<=n;i++)
		for(j=1;j<=m;j++){
			kp=0;p[i][j][j]=0;
			for(k=j-1;k&&(j-k<=(m+1)/2);k--){
				kp+=a[i][k];p[i][j][k]=kp;
				if(k!=j-1)p[i][j][k]=max(p[i][j][k],p[i][j][k+1]);
			}
		}
	for(i=1;i<n-1;i++)
		for(j=2;j<m;j++)
			for(k=j-1;k&&(j-k<=(m+1)/2);k--){
				kp=a[i+1][j];
				for(l=i+2;l<=n;l++){
					kp+=a[l][j];q[i][j][k]=max(q[i][j][k],kp+p[l][j][k]);
				}
			}
	int s,rp;
	for(int i=1;i<=m+1;i++)
		for(int k=0;k<=K;k++){
			f[i][k]=max(f[i][k],f[i-1][k]);
			if(i==m+1||k==K)continue;
			for(int j=1;j<=n;j++){
				s=a[j][i]+a[j][i+1]+a[j][i+2];
				for(int l=i+2;l<=m;l+=2){
					rp=(i+l)/2;
					f[l+1][k+1]=max(f[l+1][k+1],f[i-1][k]+s+q[j][rp][i]);
					s+=a[j][l+1]+a[j][l+2];
				}
			}
		}
	if(f[m+1][K]<-1e9)puts("Orz J!!!");
	else writeln(f[m+1][K]);
	return 0;
}
