#include <cstdio> 
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}
inline void write(int x) {
	if(x>9) write(x/10); 
	putchar(x%10+48); 
}
inline void writeln(int x) { write(x); putchar('\n'); }
const int N = 100011; 
struct node{
	int sx, sy, tx, ty; 
}a[N];
int n, Ques; 

inline int max(int x, int y) {
	return x>y?x:y; 
}
inline int min(int x, int y) {
	return x<y?x:y; 
}

int main() {
	freopen("paint.in","r",stdin); 
	freopen("paint.out","w",stdout); 
	n = read(); 
	For(i, 1, n) {
		a[i].sx = read(); a[i].sy=read(); a[i].tx = read(); a[i].ty = read(); 	
	}
	Ques = read(); 
	while(Ques--) {
		int t = read(); LL ans = 0; 
		For(i, 1, n) {
			int x = max(a[i].sx, -t); 
			int y = max(a[i].sy, -t); 
			int xx = min(a[i].tx, t); 
			int yy = min(a[i].ty, t); 
			LL len1 = xx-x+1, len2 = yy-y+1; 
			if(len1<=0) continue; 
			if(len2<=0) continue;
			ans += len1*len2;  
		}
		printf("%lld\n", ans); 
	}
}





