#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;

double alpha; 
double pai = 3.1415926535; 
int T; 

int main() {
	freopen("missile.in","r",stdin); 
	freopen("missile.out","w",stdout); 
	scanf("%d", &T); 
	while(T--) {
		double v, k1, k2, S =0.0; 
		scanf("%lf%lf%lf", &v, &k1, &k2); 
		for(double i=0.0; i<=1.60; i+=0.001) {   
			double y = v*v*sin(i)*sin(i)/20.0; 
			double x = v*v*sin(i)*cos(i)/5.0; 
			if(k1*x+k2*y > S) S = k1*x+k2*y, alpha = i; 
		}
		printf("%.3lf %.3lf\n", alpha, S); 
	}
	return 0; 
}




