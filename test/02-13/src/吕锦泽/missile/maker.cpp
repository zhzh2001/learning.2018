#include<bits/stdc++.h>
#define ll int
#define eps (1e-8)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll T;
ll get(ll l,ll r){
	return l+rand()%(r-l+1);
}
int main(){
	srand(time(0));
	freopen("missile.in","w",stdout);
	T=20000; puts("20000");
	rep(i,1,T){
		printf("%d %d %d\n",get(1,100),get(1,1000),get(1,1000));
	}
}
