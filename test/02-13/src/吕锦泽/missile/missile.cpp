#include<bits/stdc++.h>
#define ll int
#define eps (1e-9)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll T,v,k1,k2; double pi=3.141592653,ans,g=10,l,r;
double calc(double a){
	double vx=cos(a)*v,vy=sin(a)*v,t=vy/g,num=t*2*vx*k1+t*t*g/2*k2;
	return num;
}
int main(){
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	T=read();
	while (T--){
		v=read(),k1=read(),k2=read();
		l=0,r=pi/2;
		while (r>eps+l){
			double mid1=(r+l)/2,mid2=(mid1+r)/2;
			double ans1=calc(mid1),ans2=calc(mid2);
			if (ans1>ans2) ans=ans1,r=mid2; else ans=ans2,l=mid1; 
		}
		printf("%.3lf %.3lf\n",l,ans);
	}
}
