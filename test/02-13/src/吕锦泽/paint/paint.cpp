#include<bits/stdc++.h>
#define ll long long
#define eps (1e-8)
#define N 200005
#define M 1000005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
struct data{ ll a,b,c,d; }f[N];
ll n,m;
ll get(ll x1,ll y1,ll x2,ll y2,ll x3,ll y3,ll x4,ll y4){
	if (x1>x3&&y1>y3) return 0;
	return min(x4-x3+1,x4-x1+1)*min(y4-y3+1,y4-y1+1);
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	rep(i,1,n){
		ll a=read(),b=read(),c=read(),d=read();
		f[i]=(data){a,b,c,d};
	}
	m=read();
	rep(i,1,m){
		ll t=read(),ans=0;
		rep(j,1,n) ans+=get(-t,-t,t,t,f[j].a,f[j].b,f[j].c,f[j].d);
		printf("%lld\n",ans);
	}
}
