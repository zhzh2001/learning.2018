#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=100005;
const int M=1000005;
ll a[M];//差分答案 
ll b[M];//差分a数组
int lx,dy,rx,uy;
int n,q;
inline void solve(int l,int r,int d,int u)
{
	int tl,tr,tu,td;
	tl=abs(l);tr=abs(r);tu=abs(u);td=abs(d);
	if(tl>tr)swap(tl,tr);
	if(td>tu)swap(td,tu);
//通过一系列变换使所有布料进入第一象限 
//	cout<<tl<<' '<<tr<<' '<<tu<<' '<<td<<endl;
	int k=max(tl,td),kk=min(tl,td);
	a[k]+=k-kk+1;
	a[tr+1]-=min(tr+1,tu)-td+1;
	a[tu+1]-=min(tu+1,tr)-tl+1;
	if(tr+1==tu+1)a[tr+1]-=1;
	b[k+1]+=2;
	b[min(tr+1,tu+1)+1]-=2;
}
inline void make(int l,int r,int d,int u)
{
	if(d<0&&u<0)d=abs(d),u=abs(u);
	if(l<0&&r<0)l=abs(l),r=abs(r);
	if(l>r)swap(l,r);
	if(d>u)swap(d,u);
	if(l<0)solve(0,r,d,u),solve(1,-l,d,u);
	else if(d<0)solve(l,r,0,u),solve(l,r,1,-d);
	else solve(l,r,d,u);
//通过一系列变换使所有布料进入第一象限 
}
int main()
{
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		lx=read();dy=read();
		rx=read();uy=read();
		make(lx,rx,uy,dy);
	}
	q=read();
	int j=1;
	ll tx=0,now=0,ans=0;
	for(int i=1;i<=q;i++){
		int tt=read();
		while(j<=tt){
			now+=a[j];
			tx+=b[j];
			now+=tx;
			ans+=now;
			j++;
		}
		writeln(ans);
	}
	return 0;
}
