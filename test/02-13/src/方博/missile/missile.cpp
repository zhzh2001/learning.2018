#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')ch=getchar();
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
const double pi=3.141592653;
const double eps=1e-9;
const int g=10;
int v1,k1,k2;
double ans;
inline double getans(double k)
{
	double vx=cos(k)*v1;
	double vy=sin(k)*v1;
	double t=vy/g;
	double ret=t*t*g/2*k2+t*2*vx*k1;
	return ret;
}
int main()
{
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	int T=read();
	while(T--){
		v1=read();k1=read();k2=read();
		double l=0;
		double r=pi/2;
		ans=0;
		while(l+eps<r){
			double mid1=(l+r)/2;
			double mid2=(mid1+r)/2;
			double ans1=getans(mid1);
			double ans2=getans(mid2);
			if(ans1>ans2)ans=ans1,r=mid2;
			else ans=ans2,l=mid1;
		}
		printf("%.3lf %.3lf\n",l,ans);
	}
	return 0;
}
