#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
using namespace std;
typedef long long LL;
const LL oo=1ll<<60;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
struct rect{
	int a,b,c,d;
}A[400003],B[400003];
void swap(int&a,int&b){int c=a;a=b;b=c;}
LL adp[1000043],lk[1000043];
int N=1000010;
void ins(LL l,LL r,LL a,LL b){
	adp[l]+=a;
	adp[r+1]-=a;
	lk[l+1]+=b;
	lk[r+1]-=b;
	adp[r+1]-=(r-l)*b;
}
void getssss(){
	for(LL i=1;i<=N;i++){
		lk[i]+=lk[i-1];
		adp[i]+=lk[i];
	}
	for(LL i=1;i<=N;i++){
		adp[i]+=adp[i-1];
	}
	for(LL i=1;i<=N;i++){
		adp[i]+=adp[i-1];
	}
}
int n,m,x,y,z;
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		A[i].a=read();
		A[i].c=read();
		A[i].b=read();
		A[i].d=read();
	}
	x=0;
	for(int i=1;i<=n;i++){
		B[++x]=A[i];
		if(A[i].a<0&&A[i].b>0){
			B[x].b=-1;
			B[++x]=A[i];
			B[x].a=0;
		}
	}
	n=0;
	for(int i=1;i<=x;i++){
		A[++n]=B[i];
		if(B[i].c<0&&B[i].d>0){
			A[n].d=-1;
			A[++n]=B[i];
			A[n].c=0;
		}
	}
	for(int i=1;i<=N;i++)adp[i]=lk[i]=0;
	for(int i=1;i<=n;i++){
		#define a A[i].a
		#define b A[i].b
		#define c A[i].c
		#define d A[i].d
		if(a<0)a=-a;
		if(b<0)b=-b;
		if(c<0)c=-c;
		if(d<0)d=-d;
		if(a>b)swap(a,b);
		if(c>d)swap(c,d);
		a++;
		b++;
		c++;
		d++;
		if(b<d){
			swap(b,d);
			swap(a,c);
		}
		if(a>d){
			ins(a,b,d-c+1,0);
			continue;
		}
		if(b!=d){
			ins(d+1,b,d-c+1,0);
		}
		if(a<c){
			ins(c,d,c-a,0);
		}else if(a>c){
			ins(a,d,a-c,0);
		}
		ins(max(a,c),d,1,2);
		#undef a
		#undef b
		#undef c
		#undef d
	}
	m=read();
	getssss();
	for(int i=1;i<=m;i++){
		x=read();
		printf("%lld\n",adp[x+1]);
	}
	return 0;
}
