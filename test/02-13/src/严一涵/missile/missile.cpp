#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
const long double hPI=asin(1.0);
LL v,k1,k2,T;
long double ans,val;
long double calc(long double al){
	long double sinal=sin(al);
	long double x=k1*v*v*sin(2*al)/10.0;
	long double y=k2*v*v*sinal*sinal/20.0;
	return x+y;
}
long double bs(){
	long double l=0,r=hPI;
	long double lm,rm,mv;
	while(abs(r-l)>1e-15){
		mv=(r-l)/3;
		lm=l+mv;
		rm=r-mv;
		if(calc(lm)>calc(rm))r=rm;
		else l=lm;
	}
	return l;
}
int main(){
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	T=read();
	while(T--){
		v=read();
		k1=read();
		k2=read();
		ans=bs();
		val=calc(ans);
		printf("%.3lf %.3lf\n",(double)ans,(double)val);
	}
	return 0;
}
