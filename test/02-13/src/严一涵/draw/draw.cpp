#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
using namespace std;
typedef long long LL;
const LL oo=1ll<<60;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL a[103][203],sum[103][203],fc[103][203],fb[103][203],fj[203][23],n,m,k,x,y,z;
int main(){
	freopen("draw.in","r",stdin);
	freopen("draw.out","w",stdout);
	n=read();
	m=read();
	k=read();
	for(LL i=1;i<=n;i++)for(LL j=1;j<=m;j++){
		a[i][j]=read();
		sum[i][j]=sum[i][j-1]+a[i][j];
		fc[i][j]=-oo;
	}
	fj[0][0]=0;
	for(LL l=1;l<=k;l++)fj[0][l]=-oo;
	for(LL i=1;i<=m;i++){
		for(LL l=0;l<=k;l++)fj[i][l]=fj[i-1][l];
		for(LL j=1;i-j-j>0;j++){
			y=i-j;
			z=i-j-j-1;
			for(LL l=1;l<=n;l++){
				fc[l][y]=max(fc[l][y],sum[l][y]-sum[l][z]);
			}
			fb[n][y]=-oo;
			for(LL l=n-1;l>0;l--){
				fb[l][y]=max(fb[l+1][y],fc[l+1][y])+a[l][y];
			}
			x=-oo;
			for(LL l=1;l<=n-2;l++){
				x=max(x,sum[l][i]-sum[l][z]+fb[l+1][y]);
			}
			fj[i][1]=max(fj[i][1],x);
			if(z>0)for(LL l=2;l<=k;l++){
				fj[i][l]=max(fj[i][l],fj[z-1][l-1]+x);
			}
		}
	}
	if(fj[m][k]<=-6000000){
		puts("Orz J!!!");
	}else printf("%lld\n",fj[m][k]);
	return 0;
}
