#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef long double db;
const int MAXSIZE=10000020;
const db pi=acos(-1.0);
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
    freopen("missile.in","r",stdin);
    freopen("missile.out","w",stdout);
    buf[fread(buf,1,MAXSIZE,stdin)]='\0';
    bufpos=0;
}
#if NEG
int readint(){
    bool isneg;
    int val=0;
    for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
    bufpos+=(isneg=buf[bufpos]=='-');
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return isneg?-val:val;
}
#else
int readint(){
    int val=0;
    for(;!isdigit(buf[bufpos]);bufpos++);
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return val;
}
#endif
char readchar(){
    for(;isspace(buf[bufpos]);bufpos++);
    return buf[bufpos++];
}
int readstr(char* s){
    int cur=0;
    for(;isspace(buf[bufpos]);bufpos++);
    for(;!isspace(buf[bufpos]);bufpos++)
        s[cur++]=buf[bufpos];
    s[cur]='\0';
    return cur;
}
db v2,k1,k2;
db f(db x){
    db a=sin(x),b=cos(x);
    return k1*v2*a*b/5+k2*v2*a*a/20;
}
int main(){
    init();
    int n=readint();
    while(n--){
        v2=readint(),k1=readint(),k2=readint();
        v2=v2*v2;
        db l=0,r=pi/2;
        while(r-l>1e-8){
            db mid=(l+r)/2,mmid=(r+mid)/2;
            if (f(mid)>f(mmid))
                r=mmid;
            else l=mid;
        }
        db ans=l+4e-9;
        printf("%.3f %.3f\n",(double)(ans),(double)(f(ans)));
    }
    // fprintf(stderr,"%d",clock());
}
