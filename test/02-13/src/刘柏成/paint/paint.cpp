#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=20000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
    freopen("paint.in","r",stdin);
    freopen("paint.out","w",stdout);
    buf[fread(buf,1,MAXSIZE,stdin)]='\0';
    bufpos=0;
}
#if NEG
int readint(){
    bool isneg;
    int val=0;
    for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
    bufpos+=(isneg=buf[bufpos]=='-');
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return isneg?-val:val;
}
#else
int readint(){
    int val=0;
    for(;!isdigit(buf[bufpos]);bufpos++);
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return val;
}
#endif
char readchar(){
    for(;isspace(buf[bufpos]);bufpos++);
    return buf[bufpos++];
}
int readstr(char* s){
    int cur=0;
    for(;isspace(buf[bufpos]);bufpos++);
    for(;!isspace(buf[bufpos]);bufpos++)
        s[cur++]=buf[bufpos];
    s[cur]='\0';
    return cur;
}
ll s2[1000005],s1[1000005],s0[1000005];
void add(int x,int y,int sgn){ //i<=x,j<=y +=sgn
    //a[t]+=(min(t,x)+t+1)*(min(t,y)+t+1)
    //a[t]+=t^2+t(min(t,x)+min(t,y))+min(t,x)min(t,y)+min(t,x)+t+min(t,y)+t+1
    int fr=max(0,max(-x,-y));
    if (x>y)
        swap(x,y);
    //fr<=t<x
    //a[t]+=4t^2+4t+1
    if (fr<x){
        s2[fr]+=4*sgn;
        s2[x]-=4*sgn;
        s1[fr]+=4*sgn;
        s1[x]-=4*sgn;
        s0[fr]+=sgn;
        s0[x]-=sgn;
    }
    //x<=t<y
    //a[t]+=t^2+t(x+t)+xt+3t+x+1
    //a[t]+=2t^2+2xt+3t+x+1
    if (max(fr,x)<y){
        s2[max(fr,x)]+=2*sgn;
        s2[y]-=2*sgn;
        s1[max(fr,x)]+=(2*x+3)*sgn;
        s1[y]-=(2*x+3)*sgn;
        s0[max(fr,x)]+=(x+1)*sgn;
        s0[y]-=(x+1)*sgn;
    }
    //y<=t
    //a[t]+=t^2+t(x+y)+xy+x+y+2t+1;
    s2[max(fr,y)]+=sgn;
    s1[max(fr,y)]+=(x+y+2)*sgn;
    s0[max(fr,y)]+=(1LL*x*y+x+y+1)*sgn;
}
int main(){
    init();
    int n=readint();
    while(n--){
        int x1=readint(),y1=readint(),x2=readint(),y2=readint();
        add(x2,y2,1);
        add(x1-1,y2,-1);
        add(x2,y1-1,-1);
        add(x1-1,y1-1,1);
    }
    for(int i=1;i<=1000000;i++){
        s0[i]+=s0[i-1];
        s1[i]+=s1[i-1];
        s2[i]+=s2[i-1];
    }
    int q=readint();
    while(q--){
        ll t=readint();
        printf("%lld\n",s2[t]*t*t+s1[t]*t+s0[t]);
    }
}