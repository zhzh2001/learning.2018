#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
    freopen("draw.in","r",stdin);
    freopen("draw.out","w",stdout);
    buf[fread(buf,1,MAXSIZE,stdin)]='\0';
    bufpos=0;
}
#if NEG
int readint(){
    bool isneg;
    int val=0;
    for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
    bufpos+=(isneg=buf[bufpos]=='-');
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return isneg?-val:val;
}
#else
int readint(){
    int val=0;
    for(;!isdigit(buf[bufpos]);bufpos++);
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return val;
}
#endif
char readchar(){
    for(;isspace(buf[bufpos]);bufpos++);
    return buf[bufpos++];
}
int readstr(char* s){
    int cur=0;
    for(;isspace(buf[bufpos]);bufpos++);
    for(;!isspace(buf[bufpos]);bufpos++)
        s[cur++]=buf[bufpos];
    s[cur]='\0';
    return cur;
}
int n,m,k;
int a[205][205],b[205],c[205];
int sumr[205][205],sumc[205][205];
int minr[105][205][205];
int work(int l,int r){
    int mid=(l+r)/2;
    for(int i=1;i<=n;i++)
        c[i]=max(sumr[i][mid]-minr[i][l-1][mid-2],-INF);
    int now=-INF,ans=-INF;
    for(int i=n-1;i>1;i--){
        b[i]=-INF;
        now=max(now,sumc[i][mid]+c[i+1]);
        b[i]=now-sumc[i-1][mid];
        /*for(int j=i;j<n;j++)
            b[i]=max(b[i],sumc[j][mid]-sumc[i-1][mid]+c[j+1]);*/
        ans=max(ans,sumr[i-1][r]-sumr[i-1][l-1]+b[i]);
    }
    return ans;
}
int mmax[205][205];
int f[205],g[205];
int main(){
    init();
    n=readint(),m=readint(),k=readint();
    for(int i=1;i<=n;i++)
        for(int j=1;j<=m;j++){
            a[i][j]=sumr[i][j]=sumc[i][j]=readint();
            sumr[i][j]+=sumr[i][j-1];
            sumc[i][j]+=sumc[i-1][j];
        }
    memset(minr,0x3f,sizeof(minr));
    for(int i=1;i<=n;i++)
        for(int j=0;j<=m;j++)
            for(int k=j;k<=m;k++)
                minr[i][j][k]=min(minr[i][j][k-1],sumr[i][k]);
    for(int i=0;i<=m;i++)
        for(int j=0;j<=m;j++)
            mmax[i][j]=-INF;
    for(int i=1;i<=m;i++)
        for(int j=i+2;j<=m;j+=2)
            mmax[i][j]=work(i,j);
    for(int i=1;i<=m;i++)
        for(int j=i;j<=m;j++)
            mmax[i][j]=max(mmax[i][j],mmax[i][j-1]); //left is fixed,right is not
    k--;
    f[0]=-INF;
    for(int i=1;i<=m;i++){
        f[i]=-INF;
        for(int j=1;j<=i;j++)
            f[i]=max(f[i],mmax[j][i]);
    }
    while(k--){
        g[0]=-INF;
        for(int i=1;i<=m;i++){
            g[i]=-INF;
            for(int j=2;j<=i-2;j++)
                g[i]=max(g[i],mmax[j][i]+f[j-2]);
        }
        swap(f,g);
    }
    int qwq=0;
    for(int i=1;i<=n;i++)
        for(int j=1;j<=m;j++)
            if (a[i][j]<0)
                qwq+=a[i][j];
    if (qwq>f[m])
        return puts("Orz J!!!"),0;
    printf("%d",f[m]);
    // fprintf(stderr,"%d",clock());
}
