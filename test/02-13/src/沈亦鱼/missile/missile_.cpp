#include<cmath>
#include<cstdio>
double th,v1,v2,t1,t2,x,y,s,ans;
int v,p,q;
int main(){
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&v,&p,&q);
		ans=0;
		for(int i=1;i<=1571;i++){
			th=i;
			th=th/1000;
			v1=v*cos(th);
			v2=v*sin(th);
			t2=v2/10;
			y=t2*v2-5*t2*t2;
			t1=t2+sqrt(y/5);
			x=v1*t1;
			if(p*x+q*y>ans){
				s=th;
				ans=p*x+q*y;
			}
		}
		printf("%0.3f %0.3f\n",s,ans);
	}
	return 0;
}
