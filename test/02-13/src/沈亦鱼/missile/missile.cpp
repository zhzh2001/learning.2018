#include<cmath>
#include<cstdio>
double l,n,m,r,k,s,ans;
int v,p,q;
double che(double th){
	double v1,v2,t1,t2,x,y;
	v1=v*cos(th);
	v2=v*sin(th);
	t2=v2/10;
	y=t2*v2-5*t2*t2;
	t1=t2+sqrt(y/5);
	x=v1*t1;
	return p*x+q*y;
}
int main(){
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		l=0;
		r=1.5707964;
		scanf("%d%d%d",&v,&p,&q);
		ans=0;
		while(l+0.0000100<r){
			k=(r-l)/3;
			n=l+k;
			m=r-k;
			if(che(n)>che(m))r=m;
			else l=n;
		}
		for(int i=0;i<=100;i++){
			n=i;
			n=n/1000000;
			if(che(l+n)>ans){
				s=l+n;
				ans=che(s);
			}
		}
		printf("%0.3f %0.3f\n",s,ans);
	}
	return 0;
}
