#include <bits/stdc++.h>
#define LL long long
using namespace std;
LL t,n,q,xl[100005],yl[100005],xr[100005],yr[100005],ans;
LL read()
{
	LL data=0,w=1;
	char ch=0;
	while(ch!='-'&&(ch<'0'||ch>'9'))
		ch=getchar();
	if(ch=='-')
	{
		w=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		data=data*10+ch-'0';
		ch=getchar();
	}
	data*=w;
	return data;
}
void write(LL x)
{
	if(x<0)
	{
		putchar('-');
		x=-x;
	}
	if(x>9) write(x/10);
	putchar(x%10+'0');
}
int main()
{
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	for(LL i=1;i<=n;i++)
	{
		xl[i]=read();
		yl[i]=read();
		xr[i]=read();
		yr[i]=read();
	}
	q=read();
	for(LL i=1;i<=q;i++)
	{
		t=read();
		ans=0;
		for(LL j=1;j<=n;j++)
		{
			if(xl[j]>=0&&yl[j]>=0) ans+=max(0LL,min(xr[j]-xl[j]+1,t-xl[j]+1))*max(0LL,min(yr[j]-yl[j]+1,t-yl[j]+1));
			if(xr[j]<=0&&yl[j]>=0) ans+=max(0LL,min(xr[j]-xl[j]+1,t+xr[j]+1))*max(0LL,min(yr[j]-yl[j]+1,t-yl[j]+1));
			if(xr[j]<=0&&yr[j]<=0) ans+=max(0LL,min(xr[j]-xl[j]+1,t+xr[j]+1))*max(0LL,min(yr[j]-yl[j]+1,t+yr[j]+1));
			if(xl[j]>=0&&yr[j]<=0) ans+=max(0LL,min(xr[j]-xl[j]+1,t-xl[j]+1))*max(0LL,min(yr[j]-yl[j]+1,t+yr[j]+1));
			if(yl[j]>0&&xl[j]<0&&xr[j]>0) ans+=(min(t,xr[j])-max(-t,xl[j])+1)*min(max(0LL,t-yl[j]+1),yr[j]-yl[j]+1);
			if(yr[j]<0&&xl[j]<0&&xr[j]>0) ans+=(min(t,xr[j])-max(-t,xl[j])+1)*min(max(0LL,t+yr[j]+1),yr[j]-yl[j]+1);
			if(xl[j]>0&&yl[j]<0&&yr[j]>0) ans+=(min(t,yr[j])-max(-t,yl[j])+1)*min(max(0LL,t-xl[j]+1),yr[j]-yl[j]+1);
			if(xr[j]>0&&yl[j]<0&&yr[j]>0) ans+=(min(t,yr[j])-max(-t,yl[j])+1)*min(max(0LL,t+yr[j]+1),yr[j]-yl[j]+1);
		}
		write(ans);
		puts("");
	}
}