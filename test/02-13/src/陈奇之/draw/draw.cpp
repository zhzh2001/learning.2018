#include<bits/stdc++.h>
using namespace std;
#define gc getchar
#define pc putchar
#define RG register
#define mem(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i = (a);i < (b);i++)
#define Rep(i,a,b) for(RG int i = (a);i <=(b);i++)
#define rd read
inline int read(){
	int T = 0,f = false;
	char c = gc();
	for(;!isdigit(c);c=gc()) f |= (c == '-');
	for(;isdigit(c);c=gc()) T = (T << 1) + (T << 3) + (c ^ 48);
	return f ? -T : T;
}
#define N 205 
const int inf = 0x3f3f3f3f;
int n,m,K;
int pre[N][N][N],s[N][N],f[N][N][N],dp[N][N],g[N][N],a[N][N];
int main(){
	freopen("draw.in","r",stdin);
	freopen("draw.out","w",stdout);
	n = rd(),m = rd(),K = rd();
	if(n <= 2 || (K*3+K-1) > m){
		puts("Orz J!!!");
		return 0;
	}
	Rep(i,1,n){
		s[i][0] = 0;
		Rep(j,1,m)
			a[i][j] = rd(),
			s[i][j] = s[i][j-1] + a[i][j];
	}
	Rep(i,1,n)Rep(j,1,m){
		pre[i][j][j-1] = a[i][j-1];
//		printf("pre[%d][%d][%d] = %d\n",i,j,j-1,pre[i][j][j-1]);
		for(int k=j-2;k>=1;k--){
			pre[i][j][k] = max(pre[i][j][k+1],s[i][j-1] - s[i][k-1]);
//			printf("pre[%d][%d][%d] = %d\n",i,j,k,pre[i][j][k]);
		}
	}
	mem(f,0xc0);
	for(int i=n-2;i>=1;i--){
		Rep(j,1,m){
			Rep(k,1,j-1){
				f[i][j][k]=max(f[i+1][j][k]+a[i+1][j],pre[i+2][j][k]+a[i+1][j]+a[i+2][j]);
				//f[1][2][1] = max(f[2][2][1]+1,pre[3][2][1])
//				printf("f[%d][%d][%d] = %d\n",i,j,k,f[i][j][k]);
			}
		}
	}
	Rep(i,1,m)Rep(j,i+1,m){
		if((i+j)&1) continue;
		int x = (i+j)>>1;
		g[i][j] = -inf;
		Rep(k,1,n){
			g[i][j] = max(g[i][j],s[k][j]-s[k][i-1]+f[k][x][i]);
//			printf("(%d)+(%d)\n",s[k][j]-s[k][i-1],f[k][x][i]);
		}
//		printf("g[%d][%d] = %d\n",i,j,g[i][j]);
	}
	mem(dp,0xc0);
	dp[0][0] = 0;
	Rep(i,1,m){
		Rep(k,0,K) dp[i][k] = max(dp[i][k],dp[i-1][k]);
		Rep(j,1,m){
			Rep(k,0,K){
				dp[j+1][k+1] = max(dp[j+1][k+1],dp[i-1][k] + g[i][j]);
			}
		}
	}
	dp[m+1][K] = max(dp[m][K],dp[m+1][K]);
	printf("%d\n",dp[m+1][K]);
//设f[i][j]|(j-i+1)%2=0,i!=j表示以[i,j]为两端的最大美观度。
/*
pre[i,j,k]表示第i行，向左到k
f[i,j,k]表示以(i,j)向下延伸，向左最多延伸到K
这样最大是多少 
*/ 
}
/*
5 8 2
 1  1  1  1  1  1  1  1
 1  1 -9 -9 -9  1  1  1
 1 -9  1  1 -1  1 -9  1
 1  1  1 -1 -1  1  1  1
 9  3 -4  5  2  3  0  0
*/



