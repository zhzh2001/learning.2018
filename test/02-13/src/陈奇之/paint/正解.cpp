#include<bits/stdc++.h>
using namespace std;
#define gc getchar
#define pc putchar
#define RG register
#define mem(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i = (a);i < (b);i++)
#define Rep(i,a,b) for(RG int i = (a);i <=(b);i++)
#define rd read
#define y1 _____y1
#define M 1000005
inline int read(){
	int T = 0,f = false;
	char c = gc();
	for(;!isdigit(c);c=gc()) f |= (c == '-');
	for(;isdigit(c);c=gc()) T = (T << 1) + (T << 3) + (c ^ 48);
	return f ? -T : T;
}
int n;
int a[M];
struct Node{
	int lazy,val,db;
};
#define mid ((l+r)>>1)
#define lson (o<<1)
#define rson ((o<<1)|1)
void pushdown(int o,int l,int r){
	int f = T[o].db;
	int L = 1,R = (r - l + 1) * 2 - 1;
	if(T[o].lazy){
		T[lson].val+=T[o].lazy;
		T[lson].lazy+=T[o].lazy;
		T[rson].val+=T[o].lazy;
		T[rson].lazy+=T[o].lazy;
		T[o].lazy=0;
	}
	T[rson].db+=T[rson]
}
void build(int o,int l,int r){
}
void add1(int o,int l,int r,int x,int y,int v){
	if(l == x && r == y){
		T[o].val += v;
		T[o].lazy += v;
	}
}
void add1(int l,int r,int v){
	printf("add1(%d %d %d)\n",l,r,v);
	Rep(i,l,r) a[i]+=v;
}
void add2(int l,int v){
	int r = l+v-1;
	printf("add2(%d %d)\n",l,r);
	v = 1;
	Rep(i,l,r) a[i]+=v,v+=2;
}
void add(int L,int R,int U,int D){
	//保证在第一象限 
	printf("%d %d %d %d\n",L,R,U,D);
	puts("");
	int tmp = max(D,L);
	if(D>L)add1(D,U,D-L);
	if(L>D)add1(L,R,L-D);
	add2(max(L,D),min(U-max(L,D)+1,R-max(L,D)+1));
	if(R>U)add1(U+1,R,U-D+1);
	if(U>R)add1(R+1,U,R-L+1);
}

void solve(int x1,int y1,int x2,int y2){
	int L,R,U,D;
	U = max(y1,y2);
	D = min(y1,y2);
	L = min(x1,x2);
	R = max(x1,x2);
	if(L <= 0) add(max(-R,0),-L,U,D);
	if(R > 0) add(max(1,L),R,U,D);
	//表示x1,y1,x2,y2这个矩形
	//保证在x轴上方 
}
int main(){
	n = rd();
	Rep(i,1,n){
		int x1 = rd(),y1 = rd(),x2 = rd(),y2 = rd();
		if(x1<0&&x2<0){
			solve(-y1,-x1,-y2,-x2);
		} else
		if(x1>0&&x2>0){
			solve(y1,x1,y2,x2);
		} else
		if(y1<0&&y2>0){
			solve(-x1,-y1,-x2,-y2);
		} else{
			solve(x1,y1,x2,y2);
		}
	}
	int Q = rd();
	rep(i,1,M)a[i]=a[i-1]+a[i];
	while(Q--){
		int w = rd();
		printf("%d\n",a[w]);
	}
}
/*
3
-2 1 1 2
1 0 2 1
-3 -3 -2 0
2
1 2
*/
