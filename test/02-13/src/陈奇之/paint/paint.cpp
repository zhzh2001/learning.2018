#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define gc getchar
#define pc putchar
#define RG register
#define mem(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i = (a);i < (b);i++)
#define Rep(i,a,b) for(RG int i = (a);i <=(b);i++)
#define rd read
#define y1 _____y1
inline int read(){
	int T = 0,f = false;
	char c = gc();
	for(;!isdigit(c);c=gc()) f |= (c == '-');
	for(;isdigit(c);c=gc()) T = (T << 1) + (T << 3) + (c ^ 48);
	return f ? -T : T;
}
#define N 100050
ll x1[N],x2[N],y1[N],y2[N];
ll solve(int L1,int R1,int D1,int U1,int L2,int R2,int D2,int U2){
	int l = max(L1,L2),r = min(R1,R2),u = min(U1,U2),d = max(D1,D2);
	if(l>r||d>u) return 0;
	return 1ll * (r-l+1) * (u-d+1);
}
ll ans;
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	int n = rd();
	Rep(i,1,n){
		x1[i] = rd(),y1[i] = rd(),
		x2[i] = rd(),y2[i] = rd();
		if(x1>x2)swap(x1,x2);
		if(y1>y2)swap(y1,y2);
	}
	int Q = rd();
	ans = 0;
	while(Q--){
		int t = rd();
		ans = 0;
		Rep(i,1,n) ans+=solve(-t,t,-t,t,x1[i],x2[i],y1[i],y2[i]);
		printf("%lld\n",ans);
	}
}

/*
4
5 1 8 4
-8 1 -5 4
-10 2 10 3
6 0 8 10
6
1 2 3 4 7 9
*/





