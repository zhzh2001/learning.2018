#include<bits/stdc++.h>
using namespace std;
#define gc getchar
#define pc putchar
#define RG register
#define mem(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i = (a);i < (b);i++)
#define Rep(i,a,b) for(RG int i = (a);i <=(b);i++)
#define rd read
#define sqr(x) ((x)*(x))
typedef long double ld;
const double pi = acos(-1.0);
inline int read(){
	int T = 0,f = false;
	char c = gc();
	for(;!isdigit(c);c=gc()) f |= (c == '-');
	for(;isdigit(c);c=gc()) T = (T << 1) + (T << 3) + (c ^ 48);
	return f ? -T : T;
}
int v0,k1,k2;
double f(double a){
	double sina = sin(a),cosa = cos(a);
	double X = 2.0*sqr(v0)*cosa*sina/10;
	double Y = 1.0*sqr(v0)*sqr(sina)/20;
	return k1*X+k2*Y;
}
void solve(){
	v0 = rd(),k1 = rd(),k2 = rd();
	double L = 0,R = pi/2;
	while(R-L > 1e-6){
		double Lmid = L + (R-L) / 3;
		double Rmid = R - (R-L) / 3;
		if(f(Lmid) < f(Rmid))
			L = Lmid;
		else
			R = Rmid;
	}
	printf("%.3lf %.3lf\n",(double)L,(double)f(L));
}
int main(){
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	int T = rd();
	while(T--){
		solve();
	}
	return 0;
}
/*
10*t^2 = 
*/
