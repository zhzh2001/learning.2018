#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define ls (i<<1)
#define rs (i<<1|1)
using namespace std;
const int N=2000100;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(int x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int ln[N*4],ad[N*4],n,mx;
void upd(int i,int l,int r){
	if(ad[i]){
		ln[i]=r-l+1;
		return;
	}ln[i]=ln[ls]+ln[rs];
}
void add(int i,int l,int r,int x,int y,int k){
	if(l>=x&&r<=y){
		ad[i]+=k;
		upd(i,l,r);return;
	}int md=(l+r)>>1;
	if(x<=md)add(ls,l,md,x,y,k);
	if(y>md)add(rs,md+1,r,x,y,k);
	upd(i,l,r);
}
int get(int i,int l,int r,int x,int y){
	if(ad[i]){return y-x+1;}
	if(l==r)return 0;
	int md=(l+r)>>1;
	if(y<=md)return get(ls,l,md,x,y);
	if(x>md)return get(rs,md+1,r,x,y);
	return get(ls,l,md,x,md)+get(rs,md+1,r,md+1,y);
}
struct cqz{int i,l,r,k;}q[N];
inline bool operator<(cqz i,cqz j){return i.i<j.i;}
int X1[N],X2[N],Y1[N],Y2[N],ans[N],sm;
void solve(){
	int i,k;sm=0;
	memset(ad,0,sizeof(ad));
	memset(ln,0,sizeof(ln));
	for(i=1;i<=n;i++){
		if(X2[i]<=0)continue;
		q[++sm]=(cqz){X1[i],Y1[i],Y2[i],1};
		q[++sm]=(cqz){X2[i]+1,Y1[i],Y2[i],-1};
	}
	sort(q+1,q+sm+1);
	k=1;
	for(i=1;i<=mx;i++){
		while(k<=sm&&q[k].i<=i){
			add(1,-mx,mx,q[k].l,q[k].r,q[k].k);
			k++;
		}
		ans[i]+=get(1,-mx,mx,-i,i-1);
	}
	for(i=1;i<=n;i++){
		swap(X1[i],Y2[i]);
		swap(X2[i],Y1[i]);
		swap(X1[i],X2[i]);
		Y1[i]=-Y1[i];Y2[i]=-Y2[i];
	}
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	int i;n=read();
	for(i=1;i<=n;i++){
		X1[i]=read();
		Y1[i]=read();
		X2[i]=read();
		Y2[i]=read();
		mx=max(mx,abs(Y2[i]));mx=max(mx,abs(X2[i]));
		mx=max(mx,abs(Y1[i]));mx=max(mx,abs(X1[i]));
		if(X1[i]<=0&&X2[i]>=0&&Y1[i]<=0&&Y2[i]>=0)ans[0]=1;
	}mx++;
	for(i=1;i<=4;i++)solve();
	for(i=1;i<=mx;i++)ans[i]+=ans[i-1];
	for(int Q=read();Q--;){
		i=read();
		printf("%d\n",ans[i]);
	}return 0;
}
