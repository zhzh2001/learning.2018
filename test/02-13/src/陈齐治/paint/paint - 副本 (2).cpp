#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define ls (i<<1)
#define rs (i<<1|1)
using namespace std;
const int N=2000100,M=200100;
char Buf[N],*S=Buf+N,*T=Buf+N;
inline char getc(){
	if(S==T){
		fread(Buf,N,sizeof(char),stdin);
		S=Buf;
	}return *S++;
}
inline int read(){
	int x=0,c=getc(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getc());
	for(;c>='0'&&c<='9';c=getc())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
ll ans[N],K,B;
int n,mx;
struct cqz{int i,k,b;}q[M*24];
inline bool operator<(cqz i,cqz j){return i.i<j.i;}
int X1[M],X2[M],Y1[M],Y2[M],sm;
inline void solve(){
	register int i,t,l,r,t1,t2,T;
	for(i=1;i<=n;i++){
		if(X2[i]<=0)goto out;
		t=max(X1[i]+1,1);
		T=X2[i];
		l=-t;r=t-1;
		if(Y1[i]>0)t1=Y1[i]+1;else t1=-Y1[i];
		if(Y2[i]>0)t2=Y2[i]+1;else t2=-Y2[i];
		if(min(t1,t2)>t){
			q[++sm]=(cqz){t,2,r-l+1};
			t=min(t1,t2);
			l=-t;r=t-1;
			if(t<T)q[++sm]=(cqz){t,-2,-(r-l+1)};
			else{
				t=T+1;
				l=-t;r=t-1;
				q[++sm]=(cqz){t,-2,-(r-l+1)};
				goto out;
			}
		}
		l=-t;r=t-1;
		l=max(l,Y1[i]);
		r=min(r,Y2[i]);
		if(max(t1,t2)>t){
			q[++sm]=(cqz){t,1,r-l+1};
			t=max(t1,t2);
			l=-t;r=t-1;
			l=max(l,Y1[i]);
			r=min(r,Y2[i]);
			if(t<T)q[++sm]=(cqz){t,-1,-(r-l+1)};
			else{
				t=T+1;
				l=-t;r=t-1;
				l=max(l,Y1[i]);
				r=min(r,Y2[i]);
				q[++sm]=(cqz){t,-1,-(r-l+1)};
				goto out;
			}
		}
		q[++sm]=(cqz){t,0,r-l+1};
		q[++sm]=(cqz){T+1,0,-(r-l+1)};
		
		
		out:swap(X1[i],Y2[i]);
		swap(X2[i],Y1[i]);
		swap(X1[i],X2[i]);
		Y1[i]=-Y1[i];Y2[i]=-Y2[i];
	}
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	int i,k;
	n=read();
	for(i=1;i<=n;i++){
		X1[i]=read();
		Y1[i]=read();
		X2[i]=read();
		Y2[i]=read();
		mx=max(mx,abs(Y2[i]));mx=max(mx,abs(X2[i]));
		mx=max(mx,abs(Y1[i]));mx=max(mx,abs(X1[i]));
		if(X1[i]<=0&&X2[i]>=0&&Y1[i]<=0&&Y2[i]>=0)ans[0]=1;
	}mx++;
	for(i=1;i<=4;i++)solve();
	sort(q+1,q+sm+1);
	for(i=1;i<=mx;i++){
		B+=K;
		while(k<=sm&&q[k].i<=i){
			K+=q[k].k;
			B+=q[k].b;
			k++;
		}
		ans[i]=ans[i-1];
		ans[i]+=B;
	}
	for(int Q=read();Q--;){
		i=read();
		write(ans[i]);putchar('\n');
	}return 0;
}
