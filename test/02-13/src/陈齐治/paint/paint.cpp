#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define ls (i<<1)
#define rs (i<<1|1)
using namespace std;
const int N=2000100,M=200100;
char Buf[N],*S=Buf+N,*T=Buf+N;
inline char getc(){
	if(S==T){
		fread(Buf,N,sizeof(char),stdin);
		S=Buf;
	}return *S++;
}
inline int read(){
	int x=0,c=getc(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getc());
	for(;c>='0'&&c<='9';c=getc())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
ll ans[N],ln[N*4];
int ad[N*4],n,mx;
inline void upd(int i,int l,int r){
	ln[i]=ln[ls]+ln[rs]+1ll*ad[i]*(r-l+1);
}
void add(int i,int l,int r,int x,int y,int k){
	if(l>=x&&r<=y){
		ad[i]+=k;
		upd(i,l,r);return;
	}int md=(l+r)>>1;
	if(x<=md)add(ls,l,md,x,y,k);
	if(y>md)add(rs,md+1,r,x,y,k);
	upd(i,l,r);
}
ll res;
inline void get(int i,int l,int r,int x,int y){
	if(l==x&&r==y){res+=ln[i];return;}
	int md=(l+r)>>1;res+=1ll*ad[i]*(y-x+1);
	if(y<=md)get(ls,l,md,x,y);
	else if(x>md)get(rs,md+1,r,x,y);
	else get(ls,l,md,x,md),get(rs,md+1,r,md+1,y);
}
struct cqz{int i,l,r,k;}q[M];
inline bool operator<(cqz i,cqz j){return i.i<j.i;}
int X1[M],X2[M],Y1[M],Y2[M],sm;
inline void solve(){
	register int i,k;sm=0;
	memset(ad,0,sizeof(ad));
	memset(ln,0,sizeof(ln));
	for(i=1;i<=n;i++){
		if(X2[i]<=0)continue;
		q[++sm]=(cqz){X1[i],Y1[i],Y2[i],1};
		q[++sm]=(cqz){X2[i]+1,Y1[i],Y2[i],-1};
	}
	sort(q+1,q+sm+1);
	k=1;
	for(i=1;i<=mx;i++){
		while(k<=sm&&q[k].i<=i){
			add(1,-mx,mx,q[k].l,q[k].r,q[k].k);
			k++;
		}
		res=0;
		get(1,-mx,mx,-i,i-1);
		ans[i]+=res;
	}
	for(i=1;i<=n;i++){
		swap(X1[i],Y2[i]);
		swap(X2[i],Y1[i]);
		swap(X1[i],X2[i]);
		Y1[i]=-Y1[i];Y2[i]=-Y2[i];
	}
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	int i;n=read();
	for(i=1;i<=n;i++){
		X1[i]=read();
		Y1[i]=read();
		X2[i]=read();
		Y2[i]=read();
		mx=max(mx,abs(Y2[i]));mx=max(mx,abs(X2[i]));
		mx=max(mx,abs(Y1[i]));mx=max(mx,abs(X1[i]));
		if(X1[i]<=0&&X2[i]>=0&&Y1[i]<=0&&Y2[i]>=0)ans[0]=1;
	}mx++;
	for(i=1;i<=4;i++)solve();
	for(i=1;i<=mx;i++)ans[i]+=ans[i-1];
	for(int Q=read();Q--;){
		i=read();
		write(ans[i]);putchar('\n');
	}return 0;
}
