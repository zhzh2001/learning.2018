#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=310;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline int max(int x,int y){return x>y?x:y;}
int n,m,K,a[N][N],s[N][N],h[N][N];
int v[N][N],g[N][N],f[N][N],oo;
void solve(int len){
	register int i,j,k,d,x;
	k=len/2+1;
	for(i=1;i<=n;i++)
	for(j=1;j<=m;j++)if(j-k>=0)
	v[i][j]=max(v[i][j],s[i][j]-s[i][j-k]);
	for(i=1;i<=m;i++){
		j=i+len-1;
		if(j>m)continue;
		d=(i+j)/2;
		x=-oo;
		for(k=1;k<n;k++){
			g[i][j]=max(g[i][j],h[k][d]+x+v[k+1][d]);
			x=max(x,s[k][j]-s[k][i-1]-h[k][d]);
		}
	}
}
int main(){
	freopen("draw.in","r",stdin);
	freopen("draw.out","w",stdout);
	memset(g,-60,sizeof(g));
	memset(f,-60,sizeof(f));
	oo=-f[0][0];
	register int i,j,k;
	n=read();m=read();K=read();
	for(i=1;i<=n;i++)
	for(j=1;j<=m;j++){
		v[i][j]=-oo;
		a[i][j]=read();
		s[i][j]=s[i][j-1]+a[i][j];
		h[i][j]=h[i-1][j]+a[i][j];
	}
	for(k=2;k<=m;k++){
		for(i=1;i<=m;i++){
			j=i+k-1;if(j>m)continue;
			g[i][j]=max(g[i][j-1],g[i+1][j]);
		}if(k&1)solve(k);
	}
	f[0][0]=0;
	for(i=1;i<=m;i++)
	for(k=1;k<=K;k++){
		f[i][k]=f[i-1][k];
		f[i][k]=max(f[i][k],f[0][k-1]+g[1][i]);
		for(j=1;j<i;j++)
		f[i][k]=max(f[i][k],f[j][k-1]+g[j+2][i]);
	}
	if(f[m][K]<-n*m*200)puts("Orz J!!!");
	else printf("%d\n",f[m][K]);
	return 0;
}
