#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define db double
using namespace std;
int T;
db v0,k1,k2,a,s;
int main(){
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	scanf("%d",&T);
	for(int i=1;i<=T;i++){
		scanf("%lf%lf%lf",&v0,&k1,&k2);
		k1*=4;k2=-k2;
		a=atan2(k1,k2);
		s=k2*cos(a)+k1*sin(a);
		s-=k2;s/=40;
		s=s*v0*v0;a/=2;
		printf("%.3lf %.3lf\n",a,s);
	}return 0;
}
