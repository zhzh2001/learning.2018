#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

struct treenode
{
	long long addK,addB,K,B;
};

treenode tree[4000000];

inline void pushdown(int l,int r,int node)
{
	if (tree[node].addK)
	{
		tree[node].K=tree[node].K+tree[node].addK*(r-l+1);
		if (l!=r)
		{
			tree[node<<1].addK=tree[node<<1].addK+tree[node].addK;
			tree[node<<1|1].addK=tree[node<<1|1].addK+tree[node].addK;
		}
		tree[node].addK=0;
	}
	if (tree[node].addB)
	{
		tree[node].B=tree[node].B+tree[node].addB*(r-l+1);
		if (l!=r)
		{
			tree[node<<1].addB=tree[node<<1].addB+tree[node].addB;
			tree[node<<1|1].addB=tree[node<<1|1].addB+tree[node].addB;
		}
		tree[node].addB=0;
	}
	return;
}

inline void pushup(int l,int r,int node)
{
	int mid=(l+r)>>1;
	pushdown(l,mid,node<<1);
	pushdown(mid+1,r,node<<1|1);
	tree[node].K=tree[node<<1].K+tree[node<<1|1].K;
	tree[node].B=tree[node<<1].B+tree[node<<1|1].B;
	return;
}

inline void add(int l,int r,int x,int y,int k,int b,int node)
{
	pushdown(l,r,node);
	if ((l==x) && (r==y))
	{
		tree[node].addK=tree[node].addK+k;
		tree[node].addB=tree[node].addB+b;
	}
	else
	{
		int mid=(l+r)>>1;
		if (y<=mid)
			add(l,mid,x,y,k,b,node<<1);
		else
			if (x>mid)
				add(mid+1,r,x,y,k,b,node<<1|1);
			else
			{
				add(l,mid,x,mid,k,b,node<<1);
				add(mid+1,r,mid+1,y,k,b,node<<1|1);
			}
		pushup(l,r,node);
	} 
	return;
}

inline long long ask(int l,int r,int x,int y,int z,int node)
{
	pushdown(l,r,node);
	if ((! tree[node].K) && (! tree[node].B))
		return 0;
	if ((l==x) && (r==y))
		return tree[node].K*z+tree[node].B;
	int mid=(l+r)>>1;
	if (y<=mid)
		return ask(l,mid,x,y,z,node<<1);
	if (x>mid)
		return ask(mid+1,r,x,y,z,node<<1|1);
	return ask(l,mid,x,mid,z,node<<1)+ask(mid+1,r,mid+1,y,z,node<<1|1);
}

const int inf=1000000;

struct edge
{
	int p,l,r,o;
	
	inline bool operator < (const edge t) const
	{
		return p<t.p;
	}
};

edge L[120000],R[120000];
int T[120000];
long long W[120000];
int i,k,n,nl,nr,q,x1,x2,y1,y2;

inline void addL(int p,int l,int r,int o)
{
	nl++,L[nl]=(edge) {p,l,r,o};
	return;
}

inline void addR(int p,int l,int r,int o)
{
	nr++,R[nr]=(edge) {p,l,r,o};
	return;
}

inline int fastscanf()
{
	int t=0,p=1;
	char c=getchar();
	while ((! ((c>47) && (c<58))) && (c!='-'))
		c=getchar();
	if (c=='-')
		p=-p,c=getchar();
	while ((c>47) && (c<58))
		t=t*10+c-48,c=getchar();
	return t*p; 
}

inline void input()
{
	freopen("paint.in","r",stdin);
	n=fastscanf();
	for (i=1;i<=n;i++)
	{
		x1=fastscanf(),y1=fastscanf(),x2=fastscanf(),y2=fastscanf();
		y1=y1,y2=y2;
		if (x2<0)
			addL(-x1,y1,y2,-1),addL(-x2,y1,y2,1);
		else
		if (x1>=0)
			addR(x1,y1,y2,1),addR(x2,y1,y2,-1);
		else
			addL(-x1,y1,y2,-1),addL(1,y1,y2,1),
			addR(0,y1,y2,1),addR(x2,y1,y2,-1);
	}
	sort(L+1,L+nl+1);
	sort(R+1,R+nr+1);
	q=fastscanf();
	for (i=1;i<=q;i++)
		T[i]=fastscanf();
	return;
}

inline void solve(edge *E,int n)
{
	memset(tree,0,sizeof(tree));
	for (i=1,k=1;i<=q;i++)
	{
		for (;(k<=n) && (E[k].p<=T[i]);k++)
			if (E[k].o==1)
				add(0,2*inf,E[k].l+inf,E[k].r+inf,1,-E[k].p+1,1);
			else
				add(0,2*inf,E[k].l+inf,E[k].r+inf,-1,E[k].p,1);
		W[i]=W[i]+ask(0,2*inf,inf-T[i],inf+T[i],T[i],1);
	}
	return;
}

inline void output()
{
	freopen("paint.out","w",stdout);
	for (i=1;i<=q;i++)
		printf("%lld\n",W[i]);
	return;
}

int main()
{
	input();
	solve(L,nl);
	solve(R,nr);
	output();
	return 0;
}
