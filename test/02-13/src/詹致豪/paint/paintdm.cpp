#include <cstdio>
#include <ctime>
#include <algorithm>

using namespace std; 

int main()
{
	freopen("paint.in","w",stdout);
	srand((int) time(0));
	int N=1000,Q=1000;
	printf("%d\n",N);
	for (int i=1;i<=N;i++)
	{
		int x1=rand()%2001-1000,x2=rand()%2001-1000;
		if (x1>x2) swap(x1,x2);
		int y1=rand()%2001-1000,y2=rand()%2001-1000;
		if (y1>y2) swap(y1,y2);
		printf("%d %d %d %d\n",x1,y1,x2,y2);
	}
	printf("%d\n",Q);
	for (int i=1,t=0;i<=Q;i++)
		t++,printf("%d\n",t);
	return 0;
} 
