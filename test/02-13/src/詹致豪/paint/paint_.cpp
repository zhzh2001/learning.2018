#include <cstdio>

const int inf=1000;

int d[3000][3000];
int i,j,k,n,q,t,x1,x2,y1,y2;
long long s;

int main()
{
	freopen("paint.in","r",stdin);
	freopen("paint_.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;i++)
	{
		scanf("%d%d%d%d",&x1,&y1,&x2,&y2);
		for (j=x1+inf;j<=x2+inf;j++)
			for (k=y1+inf;k<=y2+inf;k++)
				d[j][k]++;
	}
	scanf("%d",&q);
	for (i=1;i<=q;i++)
	{
		scanf("%d",&t);
		s=0;
		for (j=inf-t;j<=inf+t;j++)
			for (k=inf-t;k<=inf+t;k++)
				s=s+d[j][k];
		printf("%lld\n",s);
	}
	return 0;
} 
