#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

int d[210][210],e[210][210][210],f[210][210],g[210][210],h[210][210],w[210][210][210];
int i,j,k,l,m,n,s;

int main()
{
	freopen("draw.in","r",stdin);
	freopen("draw.out","w",stdout);
	scanf("%d%d%d",&n,&m,&l);
	if ((n<3) || (m<4*l-1))
	{
		printf("Orz J!!!");
		return 0;
	}
	for (i=1;i<=n;i++)
		for (j=1;j<=m;j++)
			scanf("%d",&g[i][j]);
	for (i=1;i<=n;i++)
		for (j=1;j<=m;j++)
			h[i][j]=h[i][j-1]+g[i][j];
	memset(w,250,sizeof(w));
	memset(e,250,sizeof(e));
	memset(d,250,sizeof(d));
	memset(f,250,sizeof(f));
	for (i=1;i<=m;i++)
		for (j=1;j<=n;j++)
			for (k=i+1;k<=m;k++)
			{
				w[i][j][k]=g[j][k-1];
				w[i][j][k]=max(w[i][j][k],w[i][j][k-1]+g[j][k-1]);
			}
	for (i=1;i<=m;i++)
		for (j=n-1;j>0;j--)
			for (k=i;k<=m;k++)
			{
				e[i][j][k]=g[j+1][k]+w[i][j+1][k];
				e[i][j][k]=max(e[i][j][k],e[i][j+1][k]+g[j+1][k]);
			}
	for (i=1;i<=m;i++)
		for (j=1;j<=m;j++)
			if ((i&1)==(j&1))
				for (k=1;k<=n;k++)
					d[i][j]=max(d[i][j],h[k][j]-h[k][i-1]+e[i][k+1][(i+j)/2]+g[k+1][(i+j)/2]);
	for (i=m;i>0;i--)
		for (j=1;j<=m;j++)
			d[i][j]=max(d[i][j],max(d[i][j-1],d[i+1][j]));
	for (i=1;i<=m;i++)
		f[1][i]=d[1][i];
	for (i=2;i<=l;i++)
		for (j=1;j<=m;j++)
			for (k=1;k<j;k++)
				if (f[i-1][k-1]+d[k+1][j]>f[i][j])
					f[i][j]=f[i-1][k-1]+d[k+1][j];
	for (i=1;i<=m;i++)
		s=max(s,f[l][i]);
	printf("%d",s);
	return 0;
}
