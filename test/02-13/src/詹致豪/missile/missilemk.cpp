#include <cstdio>
#include <cmath>
#include <ctime>
#include <algorithm>

using namespace std;

inline double calc(double i,int x,int y)
{
	return x*sin(i)*sin(i)+y*sin(i)*cos(i);
}

int main()
{
	freopen("missile.out","w",stdout); 
	srand((int) time(0));
	int x=rand()<<15|rand(),y=rand()<<15|rand();
	for (double i=0.01;i<acos(-1)/2;i=i+0.01)
		printf("%.5lf\n",calc(i,x,y)-calc(i-0.01,x,y));
	return 0;
}
