#include <cstdio>
#include <cmath>

const long double pi=acos(-1);

int h,k1,k2,t,v;
long double l,r,tmp,mid1,mid2;

inline long double calc(long double x)
{
	long double sinx=sin(x),cosx=cos(x);
	return 2*v*v*sinx*cosx/10*k1+v*v*sinx*sinx/2/10*k2;
}

int main()
{
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout); 
	scanf("%d",&t);
	for (h=1;h<=t;h++)
	{
		scanf("%d%d%d",&v,&k1,&k2);
		l=0,r=pi/2;
		while (r-l>(long double) 1e-11)
		{
			tmp=(r-l)/3;
			mid1=l+tmp;
			mid2=l+tmp*2;
			if (calc(mid1)<calc(mid2))
				l=mid1;
			else
				r=mid2;
		}
		printf("%.3lf %.3lf\n",(double) l,(double) calc(l));
	}
	return 0;
} 
