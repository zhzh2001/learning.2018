#include<bits/stdc++.h>
#define pi acos(-1)
#define db double
using namespace std;
db calc(db ang,db v,db k1,db k2){
	db X=v*sin(ang);
	db Y=v*cos(ang);
	db times=X/10;
	db mxhei=1.0/2*times*times*10;
	db mxdis=2*times*Y;
	return mxdis*k1+mxhei*k2;
}
void solve(){
	int v,k1,k2;
	scanf("%d%d%d",&v,&k1,&k2);
	db l=0,r=pi/2;
	for (int i=1;i<=50;i++){
		db ll=(l*2+r)/3,rr=(l+2*r)/3;
		db v1=calc(ll,v,k1,k2);
		db v2=calc(rr,v,k1,k2);
		if (v1<v2) l=ll;
		else r=rr;
	}
	printf("%.3lf %.3lf\n",(l+r)/2,calc((l+r)/2,v,k1,k2));
}
int main(){
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--) solve();
}
/*
3
10 10 0
10 0 10
10 10 10
*/
