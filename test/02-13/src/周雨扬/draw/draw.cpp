#include<bits/stdc++.h>
#define N 105
#define M 205
using namespace std;
int n,m,K,Mx[N];
int a[N][M],b[N][M];
int v[N][M][M];
int trans[M][M];
int f[M][25][2];
int main(){
	freopen("draw.in","r",stdin);
	freopen("draw.out","w",stdout);
	scanf("%d%d%d",&n,&m,&K);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			scanf("%d",&a[i][j]);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			b[i][j]=b[i][j-1]+a[i][j];
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++){
			int S=a[i][j],mx=-1e7;
			for (int k=j-1;k;k--){
				S+=a[i][k];
				mx=max(mx,S);
				v[i][j][k]=mx;
			}
		}
	for (int i=1;i<=m;i++)
		for (int j=i+2;j<=m;j+=2){
			int mid=(i+j)/2;
			int mx=-1e7;
			Mx[n]=v[n][mid][i];
			for (int k=n-1;k;k--)
				Mx[k]=max(Mx[k+1]+a[k][mid],v[k][mid][i]);
			for (int k=n-2;k>0;k--)
				mx=max(mx,b[k][j]-b[k][i-1]+a[k+1][mid]+Mx[k+2]);
			trans[i][j]=mx;
			//printf("%d %d %d\n",i,j,mx);
		}
	for (int i=0;i<=m;i++)
		for (int j=0;j<=K;j++)
			f[i][j][0]=f[i][j][1]=-2e9;
	f[0][0][1]=0;
	for (int i=0;i<m;i++)
		for (int j=0;j<=K;j++)
			for (int ok=0;ok<2;ok++){
				f[i+1][j][1]=max(f[i+1][j][1],f[i][j][ok]);
				if (ok&&j!=K)
					for (int k=i+3;k<=m;k+=2)
						f[k][j+1][0]=max(f[k][j+1][0],f[i][j][ok]+trans[i+1][k]);
			}
	int ans=max(f[m][K][0],f[m][K][1]);
	if (ans<=-2e9+233333333)
		puts("Orz J!!!");
	else printf("%d\n",ans);       
}
/*
3 8 2
1 1 1 -1 -1 1 1 1
-1 1 -1 -1 -1 -1 1 -1
-1 1 -1 -1 -1 -1 1 -1
*/
