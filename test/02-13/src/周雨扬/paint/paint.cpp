#include<bits/stdc++.h>
#define ll long long
#define N 100005
using namespace std;
namespace FastIO{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	#define gc() getchar()
	inline int read(){
		int x=0,f=1; char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
		return f==1?x:-x;
	}
	inline void write(ll x){
		if (!x){
			puts("0");
			return;
		}
		int a[25],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar('0'+a[top--]));
		puts("");
	}
}
using namespace FastIO;
struct jdb{int id,x,l,r,v;}q[N*6];
int n,Q,sum,tot,bd[N*4];
ll ans[N];
bool cmp(const jdb &a,const jdb &b){
	if (a.x!=b.x)
		return a.x<b.x;
	return a.id<b.id;
}
struct Seg{
	ll tg[N*12];
	ll sum[N*12];
	ll ans;
	int sz[N*12];
	int lb,rb;
	void build(int k,int l,int r){
		sz[k]=bd[r]-bd[l-1];
		tg[k]=0; sum[k]=0;
		if (l==r) return;
		int mid=(l+r)/2;
		build(k*2,l,mid);
		build(k*2+1,mid+1,r);
	}
	void change(int k,int l,int r,int x,int y,int v){
		sum[k]+=1ll*v*(bd[y]-bd[x-1]);
		if (l==x&&r==y){
			tg[k]+=v;
			return;
		}
		int mid=(l+r)/2;
		if (y<=mid) change(k*2,l,mid,x,y,v);
		else if (x>mid) change(k*2+1,mid+1,r,x,y,v);
		else change(k*2,l,mid,x,mid,v),
			change(k*2+1,mid+1,r,mid+1,y,v);
	}
	void ask(int k,int l,int r,int x,int y){
		if (l==x&&r==y){
			ans+=sum[k];
			return;
		}
		ans+=tg[k]*(bd[y]-bd[x-1]);
		int mid=(l+r)/2;
		if (y<=mid) ask(k*2,l,mid,x,y);
		else if (x>mid) ask(k*2+1,mid+1,r,x,y);
		else ask(k*2,l,mid,x,mid),ask(k*2+1,mid+1,r,mid+1,y);
	}
	void init(){
		lb=2,rb=::sum;
		build(1,lb,rb);
	}
	void change(int l,int r,int v){
		change(1,lb,rb,l,r,v);
	}
	ll ask(int l,int r){
		ans=0;
		ask(1,lb,rb,l,r);
		return ans;
	}
}T1,T2;
ll ask(int x,int l,int r){
	return T1.ask(l,r)*x+T2.ask(l,r);
}
int ef(int v){
	int l=1,r=sum,ans=0;
	while (l<=r){
		int mid=(l+r)/2;
		if (bd[mid]>=v)
			ans=mid,r=mid-1;
		else l=mid+1;
	}
	return ans;
}
int main(){
	freopen("paint.in","r",stdin);
	//freopen("paint.out","w",stdout);
	n=read();
	int u=clock();
	for (int i=1;i<=n;i++){
		int x,y,X,Y;
		x=read(); y=read();
		X=read(); Y=read();
		q[++tot]=(jdb){1,x,y,Y,1};
		q[++tot]=(jdb){2,x,y,Y,-(x-1)};
		q[++tot]=(jdb){1,X+1,y,Y,-1};
		q[++tot]=(jdb){2,X+1,y,Y,X};
		bd[++sum]=y-1; bd[++sum]=Y;
	}
	printf("%d\n",clock()-u);
	Q=read();
	for (int i=1;i<=Q;i++){
		int x=read();
		q[++tot]=(jdb){3,x,-x,x,i};
		q[++tot]=(jdb){3,-x-1,-x,x,-i};
		bd[++sum]=-x-1; bd[++sum]=x;
	}
	printf("%d\n",clock()-u);
	sort(bd+1,bd+sum+1);
	sum=unique(bd+1,bd+sum+1)-bd-1;
	printf("%d\n",clock()-u);
	for (int i=1;i<=tot;i++){
		q[i].l=ef(q[i].l);
		q[i].r=ef(q[i].r);
	}
	printf("%d\n",clock()-u);
	T1.init(); T2.init();
	sort(q+1,q+tot+1,cmp);
	printf("%d\n",clock()-u);
	for (int i=1;i<=tot;i++)
		//printf("%d %d %d %d %d\n",id,x,bd[l-1],bd[r],v);
		if (q[i].id==1)
			T1.change(q[i].l,q[i].r,q[i].v);
		else if (q[i].id==2)
			T2.change(q[i].l,q[i].r,q[i].v);
		else{
			if (q[i].v<0)
				ans[-q[i].v]-=ask(q[i].x,q[i].l,q[i].r);
			else ans[q[i].v]-=ask(q[i].x,q[i].l,q[i].r);
			//printf("%d %lld\n",x,ans[abs(v)]);
		}
	printf("%d\n",clock()-u);
	//for (int i=1;i<=Q;i++)
	//	write(ans[i]);
}
/*
3
-2 1 1 2
1 0 2 1
-3 -3 -2 0
2
1 2

4
5 1 8 4
-8 1 -5 4
-10 2 10 3
6 0 8 10
6
1 2 3 4 7 9

1
1 1 1000000 1000000
3
100 10000 1000000
*/
