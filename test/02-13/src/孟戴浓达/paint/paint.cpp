//#include<iostream>
#include<algorithm>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<map>
#include<fstream>
using namespace std;
ifstream fin("paint.in");
ofstream fout("paint.out");
map<pair<int,int>,int> mp;
int n,q,mx;
int x1[100003],yy[100003],x2[100003],y2[100003];
int ans[100003];
int tt[100003];
inline void baoli(){
	for(int i=0;i<=mx;i++){
		int sum=0;
		for(int j=-i;j<=i;j++){
			sum+=mp[make_pair(i,j)];
			sum+=mp[make_pair(-i,j)];
			sum+=mp[make_pair(j,i)];
			sum+=mp[make_pair(j,-i)];
		}
		sum-=mp[make_pair(i,i)];
		sum-=mp[make_pair(-i,i)];
		sum-=mp[make_pair(i,-i)];
		sum-=mp[make_pair(-i,-i)];
		ans[i]=sum+ans[i-1];
	}
	fin>>q;
	for(int i=1;i<=q;i++){
		fin>>tt[i];
		if(tt[i]<=mx){
			fout<<ans[tt[i]]<<endl;
		}else{
			fout<<ans[mx]<<endl;
		}
	}
}
struct tree{
	int sum,tag;
}t[4000003];
inline void pushdown(int rt){
	if(!t[rt].tag){
		return;
	}
	t[rt*2].tag+=t[rt].tag;
	t[rt*2+1].tag+=t[rt].tag;
	t[rt].tag=0;
}
void build(int rt,int l,int r){
	if(l==r){
		t[rt].tag=t[rt].sum=0;
		return;
	}
	int mid=(l+r)/2;
	build(rt*2,l,mid),build(rt*2+1,mid+1,r);
}
inline long long query(int rt,int l,int r,int x){
	if(l==r){
		return t[rt].sum;
	}
	pushdown(rt);
	int mid=(l+r)/2;
	if(x<=mid){
		return query(rt*2,l,mid,x);
	}else{
		return query(rt*2+1,mid+1,r,x);
	}
}
inline void modify(int rt,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
		t[rt].tag+=val;
		return;
	}
	pushdown(rt);
	int mid=(l+r)/2;
	if(y<=mid){
		modify(rt*2,l,mid,x,y,val);
	}else if(x>mid){
		modify(rt*2+1,mid+1,r,x,y,val);
	}else{
		modify(rt*2,l,mid,x,mid,val);
		modify(rt*2+1,mid+1,r,mid+1,y,val);
	}
}
inline void solve(){
	for(int i=1;i<=n;i++){
		int mn=min(min(abs(x1[i]),abs(x2[i])),min(abs(yy[i]),abs(y2[i])));
		
	}
}
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>x1[i]>>yy[i]>>x2[i]>>y2[i];
		for(int j=x1[i];j<=x2[i];j++){
			for(int k=yy[i];k<=y2[i];k++){
				mp[make_pair(j,k)]++;
			}
		}
		mx=max(mx,abs(x1[i]));
		mx=max(mx,abs(yy[i]));
		mx=max(mx,abs(x2[i]));
		mx=max(mx,abs(y2[i]));
	}
	if(mx<=100){
		baoli();
	}else{
		solve();
	}
	return 0;
}
/*

in:
3
-2 1 1 2
1 0 2 1
-3 -3 -2 0
2
1 2
out:
5
15

in:
4
5 1 8 4
-8 1 -5 4
-10 2 10 3
6 0 8 10
6
1 2 3 4 7 9
out:
0
5
14
18
70
100

in:
1
1 1 1000000 1000000
3
100 10000 1000000
out:
10000
100000000
1000000000000

*/
