//#include<iostream>
#include<algorithm>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("draw.in");
ofstream fout("draw.out");
int n,m,k;
int a[303][303];
int sum[303][303];
int mx[203][303][303];
int mxj[303][303];
int jiyi[303][33];
int dp(int x,int k){
	int &fanhui=jiyi[x][k];
	if(fanhui!=-1){
		return fanhui;
	}
	if(k==0){
		return fanhui=0;
	}
	if(x<=0){
		return fanhui=-999999999;
	}
	fanhui=dp(x-1,k);
	if(k==1){
		for(int i=1;i<=x-2;i++){
			if((i+x)%2==0){
				fanhui=max(fanhui,mxj[i][x]);
			}
		}
	}else{
		for(int i=2;i<=x-2;i++){
			if((i+x)%2==0){
				fanhui=max(fanhui,mxj[i][x]+dp(i-2,k-1));
			}
		}
	}
	return fanhui;
}
inline void solve(){
	for(int i=1;i<=n;i++){
		for(int j=2;j<=m;j++){
			int sum=a[i][j-1]+a[i][j];
			mx[i][j-1][j]=sum;
			for(int k=j-2;k>=1;k--){
				sum+=a[i][k];
				mx[i][k][j]=max(mx[i][k+1][j],sum);
			}
		}
	}
	for(int i=1;i<=m;i++){
		for(int j=i+2;j<=m;j++){
			mxj[i][j]=-9999999999;
			if((i+j)%2==0){
				int mxx=-9999999999;
				mxx=mx[n][i][(i+j)/2];
				for(int k=n-2;k>=1;k--){
					mxx+=a[k+1][(i+j)/2];
					mxj[i][j]=max(mxj[i][j],sum[k][j]-sum[k][i-1]+mxx);
					mxx=max(mxx,mx[k+1][i][(i+j)/2]);
				}
			}
		}
	}
	memset(jiyi,-1,sizeof(jiyi));
	int ans=dp(m,k);
	if(ans==-999999999){
                  fout<<"Orz J!!!"<<endl;
                  return;
    }
	fout<<dp(m,k)<<endl;
}
int main(){
	fin>>n>>m>>k;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			fin>>a[i][j];
			sum[i][j]=sum[i][j-1]+a[i][j];
		}
	}
	if(4*k-1>m){
		fout<<"Orz J!!!"<<endl;
	}else{
		solve();
	}
	return 0;
}
/*

in:
3 8 2
 1 1  1 -1 -1  1 1  1
-1 1 -1 -1 -1 -1 1 -1
-1 1 -1 -1 -1 -1 1 -1

out:
8

in:
9 14 2
-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1
-1  1  1  1  1  1 -1 -1 -1 -1 -1 -1 -1 -1
-1 -1 -1  1 -1 -1 -1  1  1  1  1  1  1  1
-1 -1 -1  1 -1 -1 -1 -1 -1 -1  1 -1 -1 -1
-1 -1 -1  1 -1 -1 -1 -1 -1 -1  1 -1 -1 -1
-1 -1 -1  1 -1 -1 -1 -1 -1 -1  1 -1 -1 -1
-1 -1  1  1 -1 -1 -1 -1 -1 -1  1 -1 -1 -1
-1 -1 -1 -1 -1 -1 -1 -1 -1 -1  1 -1 -1 -1
-1 -1 -1  1 -1 -1 -1  1  1  1  1 -1 -1 -1
out:
27

*/
