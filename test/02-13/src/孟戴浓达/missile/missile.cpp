//#include<iostream>
#include<algorithm>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<fstream>
#include<iomanip>
using namespace std;
ifstream fin("missile.in");
ofstream fout("missile.out");
const long double eps=1e-9;
const long double pi=3.1415926535;
const long double g=10.0000000000;
int T;
long double v0,k1,k2;
inline long double sqr(long double x){
	return x*x;
}
int main(){
	fin>>T;
	while(T--){
		fin>>v0>>k1>>k2;
		if(k1==0){
			fout<<fixed<<setprecision(3)<<1.571<<" "<<(k2*sin(pi/2.000000000)*sin(pi/2.000000000)*v0*v0/20.00000000)<<endl;
			continue;
		}
		if(k2==0){
			fout<<fixed<<setprecision(3)<<0.785<<" "<<(k1*v0*v0*sin(pi/4.000000000)*cos(pi/4.000000000)/5.00000000)<<endl;
			continue;
		}
		long double ans1,ans2,ai,res;
		ans1=v0*v0*(sqrt(sqr(k2/4.000000000)+sqr(k1))+k2/4.000000000)/g;
		res=atan((double)(k2/(4.0*k1)));
		ai=((pi/2.0)+res)/2.0;
		fout<<fixed<<setprecision(3)<<ai<<" "<<ans1<<endl;
	}
	return 0;
}
/*

in:
3
10 10 0
10 0 10
10 10 10

out:
0.785 100.000
1.571 50.000
0.908 128.078

*/
