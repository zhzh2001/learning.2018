#include <fstream>
#include <cmath>
using namespace std;
ifstream fin("missile.in");
ofstream fout("missile.out");
const double pi = acos(-1.), eps = 1e-8, g = 10.;
double eval(double alpha, int v0, int k1, int k2)
{
	double vx = v0 * cos(alpha), vy = v0 * sin(alpha);
	double t = vy / g * 2.;
	double x = vx * t, y = vy * t / 4.;
	return k1 * x + k2 * y;
}
int main()
{
	int t;
	fin >> t;
	fout.precision(3);
	fout << fixed;
	while (t--)
	{
		int v0, k1, k2;
		fin >> v0 >> k1 >> k2;
		double l = 0, r = pi / 2.;
		while (r - l > eps)
		{
			double lmid = l + (r - l) / 3., rmid = r - (r - l) / 3.;
			if (eval(lmid, v0, k1, k2) > eval(rmid, v0, k1, k2))
				r = rmid;
			else
				l = lmid;
		}
		fout << l << ' ' << eval(l, v0, k1, k2) << endl;
	}
	return 0;
}