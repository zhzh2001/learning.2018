#include<cstdio>
using namespace std;
const int maxn=405;
int n,a[maxn][maxn];
inline void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		int x1,x2,y1,y2;
		scanf("%d%d%d%d",&x1,&y1,&x2,&y2);
		x1+=200; x2+=200; y2+=200; y1+=200;
		for (int x=x1;x<=x2;x++){
			for (int y=y1;y<=y2;y++){
				a[x][y]++;
			}
		}
	}
}
int s[maxn][maxn];
inline int get(int x1,int y1,int x2,int y2){
	return s[x2][y2]-s[x1-1][y2]-s[x2][y1-1]+s[x1-1][y1-1];
}
inline void solve(){
	for (int i=1;i<=400;i++){
		for (int j=1;j<=400;j++){
			s[i][j]=s[i-1][j]+s[i][j-1]-s[i-1][j-1]+a[i][j];	
		}
	}
	int q;
	scanf("%d",&q);
	for (int i=1;i<=q;i++){
		int t;
		scanf("%d",&t);
		int x1,x2,y1,y2; x1=y1=-t; x2=y2=t;
		x1+=200; y1+=200; x2+=200; y2+=200;
		printf("%d\n",get(x1,y1,x2,y2));
	}
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	init();
	solve();
	return 0;
}
