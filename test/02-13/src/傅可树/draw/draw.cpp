#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=105,maxm=205;
int n,m,K,a[maxn][maxm];
inline void init(){
	scanf("%d%d%d",&n,&m,&K);
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			scanf("%d",&a[i][j]);
		}
	}
}
int s1[maxn][maxm],s2[maxm][maxn],g[maxn][maxm][maxm];
inline int get(int i,int l,int r){
	return s1[i][r]-s1[i][l-1];
}
inline int get2(int i,int d,int u){
	return s2[i][u]-s2[i][d-1];
}
inline void prepare(){
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			s1[i][j]=s1[i][j-1]+a[i][j];
		}
	}
	for (int j=1;j<=m;j++){
		for (int i=1;i<=n;i++){
			s2[j][i]=s2[j][i-1]+a[i][j];
		}
	}
	memset(g,-20,sizeof(g));
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			int temp=a[i][j];
			for (int k=j-1;k>=1;k--){
				temp+=a[i][k];
				g[i][j][k]=max(g[i][j][k+1],temp);
			}
		}
	} // g为向左k步以内最大 
}
const int inf=1e8;
int f[maxm][maxm];
inline void work1(){
	memset(f,-20,sizeof(f));
	for (int i=1;i<=n;i++){
		for (int l=1;l<=m;l++){
			for (int len=3;l+len-1<=m;len+=2){
				int r=l+len-1,mid=(l+r)>>1; int temp=-inf;
				for (int k=1;i+k+1<=n;k++){
					temp=max(temp,get2(mid,i+1,i+k)+g[i+k+1][mid][l]);
				}
				f[l][r]=max(f[l][r],get(i,l,r)+temp);
			}
		}
	}
	for (int l=1;l<=m-2;l++){
		for (int len=3;l+len-1<=m;len++){
			int r=l+len-1;
			for (int k=l;k<r;k++){
				f[l][r]=max(f[l][r],max(f[l][k],f[k+1][r]));	
			}	
		}
	}
}
const int maxk=25;
int dp[maxm][maxk],mx[maxk][maxm];
inline void work2(){
	memset(dp,-20,sizeof(dp));
	memset(mx,-20,sizeof(mx));
	int ans=-inf; dp[0][0]=mx[0][0]=0;
	for (int i=1;i<=m;i++){
		dp[i][0]=mx[0][i]=0;
		for (int j=1;j<=K;j++){
			if (j==1){
				dp[i][j]=max(dp[i][j],f[1][i]);
			}else{
				for (int k=1;k<=i-4;k++){
					dp[i][j]=max(dp[i][j],mx[j-1][k]+f[k+2][i]);
				}
			}
			mx[j][i]=max(mx[j][i-1],dp[i][j]);
		}
		ans=max(ans,dp[i][K]);
	}
	printf("%d\n",ans);
}
inline void solve(){
	prepare();
	work1();
	work2();
}
int main(){
	freopen("draw.in","r",stdin);
	freopen("draw.out","w",stdout);
	init();
	if ((m+1)/4<K||n<3){
		puts("Orz J!!!");
		return 0;
	}
	solve();
	return 0;
}
