#include<bits/stdc++.h>
using namespace std;
const double pi=3.14159265358,eps=1e-6;
int T,v0,k1,k2;
inline double sqr(double x){
	return x*x;
}
inline double gettan(double x){
	double l=0,r=pi/2;
	while (r-l>eps){
		double mid=(l+r)/2;
		if (tan(mid)>=x){
			r=mid;
		}else{
			l=mid;
		}
	}
	return l;
}
int main(){
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d",&v0,&k1,&k2);
		double temp,res,ans,ai;
		if (!k1){
			ai=pi/2.0;
			ans=k2*sqr(v0)/20.0;
		}else{
			temp=sqrt(16.0*sqr(k1)+sqr(k2));
			res=atan((double)(k2/(4.0*k1)));
			ans=0;
			ans=(double)(k2+temp)*sqr(v0)/(4*10.0);
			ai=((pi/2.0)+res)/2.0;
		}
		printf("%.3lf %.3lf\n",ai,ans);
	}
	return 0;
}
