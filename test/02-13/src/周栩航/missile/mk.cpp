#pragma GCC optimize 2
#include<iostream>
#include<cstdio>
#include<cmath>
#include<queue>
#include<cstring>
#include<ctime>
#include<algorithm>
#define For(i,j,k)  for(register int i=j;i<=k;++i)
#define Dow(i,j,k)  for(register int i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define pb push_back
#define eps 1e-8
#define pa pair<int,int>
#define fir first
#define sec second
using namespace std;
inline ll read()
{
    ll t=0,f=1;char c=getchar();
    while(c<'0'||c>'9')   {if(c=='-') f=-1;c=getchar();}
    while(c>='0'&&c<='9') t=t*10+c-48,c=getchar();
    return t*f;
}
int n;
double v0,k1,k2;
inline double get(double a)
{
	double v1=sin(a)*v0,v2=cos(a)*v0;
	double X=v1*v1/(2*10.0);
	double t=v1/5.0;
	double Y=t*v2;
	swap(X,Y);
	double tmp=k1*X+k2*Y;
	return tmp;
}
int main()
{
//	freopen("missile.in","w",stdout);
	n=read();
	double PI=acos(-1);
	For(T,1,n)
	{
		v0=read();k1=read();k2=read();
		double l1=0,l2=PI/4,r=PI/2;
		For(i,1,60)
		{
			double mid1=(l1+l2)/2,mid2=(l2+r)/2;
			if(get(mid1)>get(mid2))
			{
				l1=mid1;
				if(get(l1)>get(l2))	l2=mid1;else l1=mid1;
			}else if(get(l2)>get(r))	r=mid2;else l2=mid2;
		}
		printf("%.3lf %3.lf\n",l1,get(l1));
	}
}
