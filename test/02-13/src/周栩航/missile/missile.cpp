#pragma GCC optimize 2
#include<iostream>
#include<cstdio>
#include<cmath>
#include<queue>
#include<cstring>
#include<ctime>
#include<algorithm>
#define For(i,j,k)  for(register int i=j;i<=k;++i)
#define Dow(i,j,k)  for(register int i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define pb push_back
#define eps 1e-8
#define pa pair<int,int>
#define fir first
#define sec second
using namespace std;
inline ll read()
{
    ll t=0,f=1;char c=getchar();
    while(c<'0'||c>'9')   {if(c=='-') f=-1;c=getchar();}
    while(c>='0'&&c<='9') t=t*10+c-48,c=getchar();
    return t*f;
}
int n;
long  double v0,k1,k2,ans;
long double get(long double a)
{
	long double v1=sin(a)*v0,v2=cos(a)*v0;
	long double X=v1*v1/(2*10.0);
	long double t=v1/5.0;
	long double Y=t*v2;
	swap(X,Y);
	long double tmp=k1*X+k2*Y;
	return tmp;
}
int main()
{
	freopen("missile.in","r",stdin);freopen("missile.out","w",stdout);
	long double PI=acos(-1);
	n=read();
	For(T,1,n)
	{
		v0=read();k1=read();k2=read();
		long  double l=0,r=PI/2;
		For(i,1,50)
		{
			long  double mid1=(l+r)/2,mid2=(r+mid1)/2;
			if(get(mid1)>get(mid2))	r=mid2;else l=mid1;
		}
		printf("%.3lf %.3lf\n",(double)l,(double)get(l));
	}
}
