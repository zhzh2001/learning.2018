#include<iostream>
#include<cstdio>
#include<cmath>
#include<queue>
#include<cstring>
#include<ctime>
#include<algorithm>
#define For(i,j,k)  for(register int i=j;i<=k;++i)
#define Dow(i,j,k)  for(register int i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define pb push_back
#define eps 1e-8
#define pa pair<int,int>
#define fir first
#define sec second
#define y1 orzwzporzzyy
using namespace std;
inline ll read()
{
    ll t=0,f=1;char c=getchar();
    while(c<'0'||c>'9')   {if(c=='-') f=-1;c=getchar();}
    while(c>='0'&&c<='9') t=t*10+c-48,c=getchar();
    return t*f;
}
inline void write(ll x1){if(x1>=10)	write(x1/10);putchar('0'+x1%10);}
inline void writeln(ll x1){write(x1);puts("");}
ll sig[1100001],alr[1100001],ans[1100001];
int n,x1,y2,y1,x2,top,Q;
struct node{int x1,y1,x2,y2;}	q[610001];
inline void Solve(int x,int y,int v)
{

	if(x==0||y==0)	return;	
	int mi=min(x,y);
	alr[mi]+=v;
	sig[min(x,y)+1]+=v*min(x,y);sig[max(x,y)+1]-=v*min(x,y);
}
int main()
{
	freopen("paint.in","r",stdin);freopen("paint.out","w",stdout);
	n=read();
	For(i,1,n)
	{
		x1=read();y1=read();x2=read();y2=read();
		if (x1<0&&y1<0){q[++top].x1=x1;q[top].y1=y1;q[top].x2=min(x2,-1);q[top].y2=min(y2,-1);}
		if (x2>0&&y2>0){q[++top].x2=x2;q[top].y2=y2;q[top].x1=max(x1,1);q[top].y1=max(y1,1);}
		if (x1<0&&y2>0){q[++top].x1=x1;q[top].y2=y2;q[top].x2=min(x2,-1);q[top].y1=max(y1,1);} 
		if (x2>0&&y1<0){q[++top].x2=x2;q[top].y1=y1;q[top].x1=max(x1,1);q[top].y2=min(y2,-1);} 
		if(x1<=0&&0<=x2)	sig[min(abs(y1),abs(y2))]++,sig[max(abs(y1),abs(y2))+1]--;
		if(y1<=0&&0<=y2)	sig[min(abs(x1),abs(x2))]++,sig[max(abs(x1),abs(x2))+1]--;	
	}
	For(i,1,top)
	{
		x1=min(abs(q[i].x1),abs(q[i].x2));
		x2=max(abs(q[i].x1),abs(q[i].x2));
		y1=min(abs(q[i].y1),abs(q[i].y2));
		y2=max(abs(q[i].y1),abs(q[i].y2));
		Solve(x2,y2,1);Solve(x1-1,y1-1,1);Solve(x1-1,y2,-1);Solve(x2,y1-1,-1);
	}
	Dow(i,1,1000000)	alr[i]+=alr[i+1];
	For(i,1,1000000)	sig[i]+=sig[i-1];
	For(i,1,1000000)	ans[i]=ans[i-1]+sig[i]+1LL*alr[i]*(i*2-1);
	Q=read();
	For(i,1,Q)
	{
		int t=read();
		writeln(ans[t]);
	}
}
