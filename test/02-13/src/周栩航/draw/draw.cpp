#include<iostream>
#include<cstdio>
#include<cmath>
#include<queue>
#include<cstring>
#include<ctime>
#include<algorithm>
#define For(i,j,k)  for(register int i=j;i<=k;++i)
#define Dow(i,j,k)  for(register int i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define pb push_back
#define eps 1e-8
#define pa pair<int,int>
#define fir first
#define sec second
using namespace std;
inline int read()
{
    int t=0,f=1;char c=getchar();
    while(c<'0'||c>'9')   {if(c=='-') f=-1;c=getchar();}
    while(c>='0'&&c<='9') t=t*10+c-48,c=getchar();
    return t*f;
}
int a[301][301],sum[301][301],q[2001],top,ycl[301][301],dp[501][50],n,m,k,num[2001];
inline int Got(int x1,int y1,int x2,int y2){return sum[x2][y2]-sum[x1-1][y2]-sum[x2][y1-1]+sum[x1-1][y1-1];}
inline void Find(int l,int r)
{
	int mid=l+r>>1;
	int top=0;
	ycl[l][r]=-1e9;
	q[0]=-1e9;
	For(i,3,n)
	{
		int tmp=-1e9;
		For(j,l,mid-1)	tmp=max(tmp,Got(i,j,i,mid)+Got(1,mid,i-1,mid));
		while(q[top]<=tmp&&top>0)	top--;
		q[++top]=tmp;num[top]=i;	
	} 
	reverse(q+1,q+top+1);reverse(num+1,num+top+1);
//	For(i,1,top)	cout<<num[i]<<' '<<q[i]<<endl;
	For(i,1,n-2)
	{
		int tmp=Got(i,l,i,r)-Got(1,mid,i,mid);	
	
		while(top>0&&num[top]-2<i)	top--;	
		tmp=tmp+q[top];
		ycl[l][r]=max(ycl[l][r],tmp);
	}
}
inline void Pre()
{
	For(i,1,m)
		for(int j=i+2;j<=m;j+=2)
			Find(i,j);
}
int main()
{
	freopen("draw.in","r",stdin);freopen("draw.out","w",stdout);
	n=read();m=read();k=read();//dp[i][j][0..1] 前i列,第i列为空画j个J最大值 第i个列是否空 mk状态 nm转移  复杂度nm^2k 
	For(i,1,n)	For(j,1,m)	a[i][j]=sum[i][j]=read();
	For(i,1,n)	For(j,1,m)	sum[i][j]+=sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1];
	Pre();//ycl[i][j]表示i~j列取到最大的J
//	For(i,1,m)	for(int j=i+2;j<=m;j+=2)	cout<<ycl[i][j]<<"<="<<i<<' '<<j<<endl;
	Find(1,7);
	For(i,0,m)	For(j,0,k)	dp[i][j]=-1e9;	
	dp[0][0]=0;
	For(i,1,m+1)
	{
		For(j,0,k)
		{
			dp[i][j]=dp[i-1][j];
				if(j!=0)	for(int t=i-1-2;t>=1;t-=2)	dp[i][j]=max(dp[i][j],dp[t-1][j-1]+ycl[t][i-1]);//cout<<dp[i][j]<<' '<<i<<' '<<j<<' '<<t-1<<' '<<dp[t-1][j-1]<<' '<<ycl[t][i-1]<<endl;
		}
	}
	int ans=-1e9;
	For(i,1,m+1)	ans=max(ans,dp[i][k]);
	if(ans<=-1e8)	puts("Orz J!!!");else printf("%d\n",ans);
}
/*
5 14 3
-29  -52  114  78   -187 138 10 96 92 91 134 -145 -126 1 
-49  -158 -30  89   145  82 159 -182 98 -47 22 49 65 58 
96   155  -14  -166 157  -126 121 -145 -82 124 132 -112 193 -144 
-194 -162 -190 123  -158 -150 16 162 -163 -57 174 25 71 192 
-85  99   194  189  -44  43 -108 162 -168 114 146 153 29 -183 

*/
