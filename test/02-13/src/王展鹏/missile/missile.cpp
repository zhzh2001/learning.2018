#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define PI pair<int,int>
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
double mx=M_PI/2;
double ans;
int main(){
	freopen("missile.in","r",stdin); freopen("missile.out","w",stdout);
	int T=read();
	while(T--){
		int v0=read(),k1=read(),k2=read(); ans=0;
		double l=0,r=mx;
		while(l+1e-9<r){
			double xx=(r-l)/3,lmid=l+xx,rmid=r-xx;
			double t=v0*sin(lmid)/10*2;
			double X=v0*cos(lmid)*t,Y=v0*sin(lmid)*(t/2)-5*(t/2)*(t/2);
			double t1=k1*X+k2*Y;
			t=v0*sin(rmid)/10*2;
			X=v0*cos(rmid)*t; Y=v0*sin(rmid)*(t/2)-5*(t/2)*(t/2);
			double t2=k1*X+k2*Y;
			ans=max(ans,max(t1,t2));
			if(t1<t2)l=lmid; else r=rmid;
		}
		printf("%.3lf %.3lf\n",l,ans);
	}
}
/*
t=vsinx/10
X=v0 cos x*t
Y=v0sin x*t-1/2*g*t^2
k1 v0 cos x*t+k2 v0 sinx*t-
*/
