#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define int long long
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=1000005;
int ans[N],x[N],y[N],n,ssss,sss,z[N],sssss;
void calc(int x1,int y1,int x2,int y2){
	if(x1<=y2){
		if(y2<x2){
			x[max(x1,y1)]+=max(x1,y1)-x1+1;
			x[y2+1]-=max(x1,y1)-x1+1;
			y[max(x1,y1)+1]++;
			y[y2+1]--; x[y2+1]-=y2-max(x1,y1);
		}else if(max(x1,y1)<=x2){
			x[max(x1,y1)]+=max(x1,y1)-x1+1;
			y[max(x1,y1)+1]++;
			y[x2+1]--; x[x2+1]-=x2-max(x1,y1);
			x[x2+1]+=x2-max(x1,y1);
			x[y2+1]-=x2-x1+1;
		}else{
			x[max(x1,y1)]+=x2-x1+1;
			x[y2+1]-=x2-x1+1;
		}
		//for(int i=max(x1,y1);i<=y2;i++)z[i]+=min(i,x2)-x1+1;
	}
	swap(x1,y1); swap(x2,y2);
	if(x1<y2){
		if(y2<=x2){
			x[max(x1,y1)]+=max(x1,y1)-x1;
			x[y2+1]-=max(x1,y1)-x1;
			y[max(x1,y1)+1]++;
			y[y2+1]--; x[y2+1]-=y2-max(x1,y1);
		}else if(max(x1,y1)-1<=x2){
			x[max(x1,y1)]+=max(x1,y1)-x1;
			y[max(x1,y1)+1]++;
			y[x2+1]--; x[x2+1]-=x2-max(x1,y1);
			x[x2+1]+=x2-max(x1,y1)+1;
			x[y2+1]-=x2-x1+1;
		}else{
			x[max(x1,y1)]+=x2-x1+1;
			x[y2+1]-=x2-x1+1;
		}
		//for(int i=max(x1,y1);i<=y2;i++)z[i]+=min(i-1,x2)-x1+1;
	}
}
signed main(){
	freopen("paint.in","r",stdin); freopen("paint.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		int x1=read(),y1=read(),x2=read(),y2=read();
		if(x2<0){
			x1=-x1; x2=-x2; swap(x1,x2);
		}
		if(y2<0){
			y1=-y1; y2=-y2; swap(y1,y2);
		}
		if(y1<0){
			swap(x1,y1); swap(x2,y2);
		}
		if(x1<0){
			calc(0,y1,x2,y2);
			//cout<<1<<" "<<y1<<" "<<x1<<" "<<y2<<endl;
			calc(1,y1,-x1,y2);
		}else{
			calc(x1,y1,x2,y2);
		}
	}
	for(int i=0;i<=1000000;i++){
		if(i)ans[i]=ans[i-1];
		//if(i<=3)cout<<x[i]<<" "<<y[i]<<endl;
		sss+=y[i]; //sss+=yyy[i]
		ssss+=sss; ssss+=x[i];
		ans[i]+=ssss;
		ans[i]+=z[i];
	}
	int q=read();
	while(q--){
		writeln(ans[read()]);
	}
}
/*
4
5 1 8 4
-8 1 -5 4
-10 2 10 3
6 0 8 10
7
1 2 3 4 7 9 100
*/

