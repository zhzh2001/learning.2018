#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define int long long
#define sqr(x) ((x)*(x))
#define mp make_pair
#define PI pair<int,int>
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=205;
int n,m,k,a[N][N],s1[N][N],s2[N][N],ans,pos[N][N],ppp[N][N],zs[N][N],zz[N][N];
void calc(){
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			s1[i][j]=s1[i-1][j]+a[i][j]; //if(i==2&&j==1)cout<<s1[i][j]<<" "<<a[i][j]<<endl;
			s2[i][j]=s2[i][j-1]+a[i][j];
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			zs[i][j]=zz[i][j]=-1e9;
			for(int k=j-1;k&&j*2-k<=m;k--){
				if(s2[i][2*j-k]-s2[i][k-1]>zs[i][j]){
					zs[i][j]=s2[i][2*j-k]-s2[i][k-1]; pos[i][j]=k;
				}
				if(s2[i][j]-s2[i][k-1]>zz[i][j]){
					zz[i][j]=s2[i][j]-s2[i][k-1]; ppp[i][j]=k;
				}
			}
		}
	}
	int mx=-2e9,posi,posj,posk;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			for(int k=i+2;k<=n;k++){
				if((ll)zs[i][j]+zz[k][j]+s1[k-1][j]-s1[i][j]>mx){
					mx=zs[i][j]+zz[k][j]+s1[k-1][j]-s1[i][j];
					posi=i; posj=j; posk=k;
				}
			}
		}
	}
	//cout<<posi<<" "<<posj<<" "<<posk<<" "<<s1[posk][posj]-s1[posi-1][posj]<<endl;
	ans+=mx;
	for(int i=posj;i>=pos[posi][posj];i--)a[posi][i]=a[posi][2*posj-i]=-1e9;
	for(int i=posi;i<=posk;i++)a[i][posj]=-1e9;
	for(int i=posj;i>=ppp[posk][posj];i--)a[posk][i]=-1e9;
}
signed main(){
	freopen("draw.in","r",stdin); freopen("draw.out","w",stdout);
	n=read(); m=read(); k=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			a[i][j]=read();
		}
	}
	for(int i=1;i<=k;i++)calc();
	/*for(int i=1;i<=n;i++){for(int j=1;j<=m;j++){
		cout<<a[i][j]<<" ";
	}puts("");}*/
	if(ans<-1000000000)puts("Orz J!!!"); else
	cout<<ans<<endl;
}

