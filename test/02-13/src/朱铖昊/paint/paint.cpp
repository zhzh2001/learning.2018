#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=100005;
int n,q,p,s,x,z,y;
struct matrix
{
	int s,x,z,y;
}a[N];
inline void read(int &x)
{
	x=0;
	char c=getchar();
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
inline void write(ll x)
{
	if (x==0)
		putchar('0');
	int a[20];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar('0'+a[a[0]--]);
	putchar('\n');
}
int main()
{
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
	{
		read(a[i].z);
		read(a[i].x);
		read(a[i].y);
		read(a[i].s);
	}
	read(q);
	while (q--)
	{
		ll ans=0;
		read(p);
		for (int i=1;i<=n;++i)
		{
			s=min(p,a[i].s);
			x=max(-p,a[i].x);
			z=max(-p,a[i].z);
			y=min(p,a[i].y);
			if (s>=x&&y>=z)
				ans+=(ll)(s-x+1)*(y-z+1);
		}
		write(ans);
	}
	return 0;
}