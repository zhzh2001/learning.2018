#include<bits/stdc++.h>
using namespace std;
const int N=105,M=205;
int a[N][M],q[N][M],v[N],z[N],n,m,g[M][M],f[25][M],K;
const int inf=1e9+7;
inline void read(int &x)
{
	x=0;
	char c=getchar();
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
/*struct segment_tree
{
	void update(int x)
	{
		a[x]=min(a[x*2],a[x*2+1]);
	}
	void build(int p,int l,int r,int x)
	{
		if (l==r)
		{
			a[i]=pr[p][l];
			return;
		}
		int mid=(l+r)/2;
		build(p,l,mid,x*2);
		build(p,mid+1,r,x*2+1);
		push_up(x);
	}
	int ask(int L,int R,int l,int r,int x)
	{
		if (L==R)
			return a[x];
		int mid=(l+r)/2;
		if (R<=mid)
			return ask(L,R,l,mid,x*2);
		if (L>mid)
			return ask(L,R,mid+1,r,x*2+1);
		return min(ask(L,mid,l,mid,x*2),ask(mid+1,R,mid+1,r,x*2+1));
	}
}t[N];*/
struct st_biao
{
	int a[8][M];
	inline void build(int x)
	{
		for (int i=0;i<=m;++i)
			a[0][i]=q[x][i];
		for (int i=1;i<=7;++i)
			for (int j=0;j+(1<<i)-1<=m;++j)
				a[i][j]=min(a[i-1][j],a[i-1][j+(1<<(i-1))]);
	}
	inline int ask(int l,int r)
	{
		int k=(int)log2(r-l+1);
		return min(a[k][l],a[k][r-(1<<k)+1]);
	}
}st[N];
inline int work(int l,int r)
{
	int mid=(l+r)/2,sum=-inf;
	for (int i=1;i<=n;++i)
		//v[i]=q[i][mid]-t[i].ask(l,mid-1,1,m,1);
		v[i]=q[i][mid]-st[i].ask(l-1,mid-2);
	z[0]=0;
	for (int i=1;i<=n;++i)
		z[i]=z[i-1]+a[i][mid];
	for (int i=1;i<=n;++i)
		v[i]=v[i]+z[i-1];
	for (int i=n-1;i>=1;--i)
		v[i]=max(v[i],v[i+1]);
	for (int i=1;i<=n-2;++i)
	{
		int tmp=q[i][r]-q[i][l-1];
		tmp+=v[i+2]-z[i];
		sum=max(sum,tmp);
	}
	return sum;
}
int main()
{
	freopen("draw.in","r",stdin);
	freopen("draw.out","w",stdout);
 	read(n);
 	read(m);
 	read(K);
 	if (K*4-1>m)
 	{
 		puts("Orz J!!!");
 		return 0;
 	}
 	for (int i=1;i<=n;++i)
 		for (int j=1;j<=m;++j)
 			read(a[i][j]);
 	for (int i=1;i<=n;++i)
 		for (int j=1;j<=m;++j)
 			q[i][j]=q[i][j-1]+a[i][j];
 	for (int i=1;i<=n;++i)
 	//	t[i].build(i,1,m,1);
 		st[i].build(i);
 	for (int i=1;i<=m;++i)
 		for (int j=i;j<=m;++j)
 			g[i][j]=-inf;
 	for (int i=1;i<=m;++i)
 		for (int j=i+2;j<=m;j+=2)
 			g[i][j]=work(i,j);
 	for (int i=1;i<=m;++i)
 		for (int j=i+3;j<=m;++j)
 			g[i][j]=max(g[i][j],g[i][j-1]);
 	for (int i=4;i<=m;++i)
 		for (int j=i-3;j>=1;--j)
 			g[j][i]=max(g[j][i],g[j+1][i]);
 	for (int i=0;i<=K;++i)
 		for (int j=0;j<=m;++j)
 			f[i][j]=-inf;
 	f[0][0]=0;
 	for (int i=1;i<=K;++i)
 		for (int j=1;j<=m;++j)
 		{
 			for (int k=1;k<j;++k)
 			{
 				int l=k-2;
 				if (l<0)
 					l=0;
 				f[i][j]=max(f[i][j],f[i-1][l]+g[k][j]);
 			}
 		}
 	int ans=-inf;
 	for (int i=1;i<=m;++i)
 		ans=max(ans,f[K][i]);
 	cout<<ans;
 	return 0;
}