#include<bits/stdc++.h>
using namespace std;
const double pi=acos(-1.0),g=10.0,eps=1e-4;
int t,v,k1,k2;
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline double work(double p,int v,int k1,int k2)
{
	double upv=(double)v*sin(p);
	double riv=(double)v*cos(p);
	double ti=upv/g;
	double high=upv*ti/2.0;
	double right=riv*ti*2.0;
	return high*k2+right*k1;
}
int main()
{
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	read(t);
	while (t--)
	{
		read(v);
		read(k1);
		read(k2);
		double l=0,r=pi/2,ans=233,last_ans=0;
		while (((r-l)>eps)||(ans-last_ans)>eps)
		{
			last_ans=ans;
			double lmid=l+(r-l)/3.0;
			double rmid=r-(r-l)/3.0;
			double lans=work(lmid,v,k1,k2);
			double rans=work(rmid,v,k1,k2);
			if (lans>rans)
			{
				ans=lans;
				r=rmid;
			}
			else
			{
				ans=rans;
				l=lmid;
			}
		}
		printf("%.3lf %.3lf\n",l,ans);
	}
	return 0;
}