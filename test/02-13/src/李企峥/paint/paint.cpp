#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,Q,t,sum,ans,ans1,xx,yy;
ll f[100100][10],f1[100000][10];
bool flag;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int work(int t,int xi,int yi,int xa,int ya)
{
	if(xa<=t&&xa>=-t)return min(abs(xa+t)+1,xa-xi+1)*abs(min(t,ya)-max(-t,yi)+1);
	if(ya<=t&&ya>=-t)return min(abs(ya+t)+1,ya-yi+1)*abs(min(t,xa)-max(-t,xi)+1);
	if(xi<=t&&xi>=-t)return min(abs(t-xi)+1,xa-xi+1)*abs(min(t,ya)-max(-t,yi)+1);
	if(yi<=t&&yi>=-t)return min(abs(t-yi)+1,ya-yi+1)*abs(min(t,xa)-max(-t,xi)+1);
	return 0;
}
int main()
{
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	For(i,1,n)For(j,1,4)f[i][j]=read();
	Q=read();
	while(Q--)
	{
		t=read();
		sum=0;ans1=0;
		For(i,1,n)
		{
			flag=true;
			For(j,1,4)if(abs(f[i][j])>t)flag=false;
			if(flag)
			{
				xx=abs(f[i][1]-f[i][3])+1;
				yy=abs(f[i][2]-f[i][4])+1;
				ans+=(xx*yy);
			}
			else
			{
				sum++;
				For(j,1,4)f1[sum][j]=f[i][j];
				ans1+=work(t,f[i][1],f[i][2],f[i][3],f[i][4]);
				//cout<<ans1<<endl;
			}
		}
		cout<<ans+ans1<<endl;
		For(i,1,sum)For(j,1,4)f[i][j]=f1[i][j];
		n=sum;
	}
	return 0;
}

