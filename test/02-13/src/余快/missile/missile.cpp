#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,t;
double mid1,mid2,ti,high,lon,v1,k1,k2,v,l,r,mid;
const double zero=1e-7;
double check(double a){
	v1=v*sin(a);
	ti=v1/10;
	high=v1*ti-10*ti*ti/2;
	lon=ti*2*v*cos(a);
	return k1*lon+k2*high;
}
signed main(){
	freopen("missile.in","r",stdin);freopen("missile.out","w",stdout);
	double pi=3.141592653589793;
	t=read();
	while (t--){
		v=read();k1=read();k2=read();
		l=zero;r=pi/2;
		while ((r-l)>zero){
			mid=(l+r)/2;
			mid1=mid-(r-l)/6;
			mid2=mid+(r-l)/6;
			if (check(mid1)>check(mid2)) r=mid2;
			else l=mid1;
		}
		printf("%.3lf %.3lf\n",l,check(l));
	}
	return 0;
}
