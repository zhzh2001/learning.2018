#include<bits/stdc++.h>
#define ll long long
#define N 205
#define M 205
#define K 23
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int l,r,n,m,k1,a[N][M],dui[M*4],yk[N][M][M],sum1[N][M],sum2[N][M],f[M][K],g[M][M],vis[M][M];
inline int max(int a,int b){
	return a>b?a:b;
}
signed main(){
	freopen("draw.in","r",stdin);freopen("draw.out","w",stdout);
	n=read();m=read();k1=read();
	if (m<3*k1+(k1-1)||n<3){
		printf("Orz J!!!\n");return 0;
	}
	for (int i=1;i<=n;i++) for (int j=1;j<=m;j++) a[i][j]=read();
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			sum1[i][j]=sum1[i][j-1]+a[i][j];
			sum2[i][j]=sum2[i-1][j]+a[i][j];
		}
	}
	for (int i=1;i<=n;i++){
		for (int k=1;k<=m/2;k++){
			l=r=1;dui[1]=0;
			for (int j=1;j<m;j++){
				while (j-dui[l]>k&&l<r) l++;
				yk[i][j+1][k+1]=sum1[i][j+1]-sum1[i][dui[l]];
				while (sum1[i][j]<sum1[i][dui[r]]&&l<=r) r--;
				r++;dui[r]=j;
			}
		}
	}
	for (int ii=2;ii<=m;ii++){
		for (int i=1;i+ii<=m;i++){
			int j=i+ii;
			g[i][j]=max(g[i+1][j],g[i][j-1]);
			if (ii%2==0){
			for (int k=1;k<=n;k++){
				for (int p=k+2;p<=n;p++){
					g[i][j]=max(g[i][j],sum1[k][j]-sum1[k][i-1]+sum2[p-1][i+ii/2]-sum2[k][i+ii/2]+yk[p][i+ii/2][ii/2+1]);
				}
			}
			}
		}
	}
	vis[0][0]=1;
	for (int i=1;i<=m;i++){
		vis[i][0]=1;
		for (int j=0;j<i-2;j++){
			if (j==i-3&&j!=0) continue;
			for (int k=1;k<=k1;k++){
				if (!vis[j][k-1]) continue;
				if (j==0) f[i][k]=max(f[i][k],f[j][k-1]+g[j+1][i]);
				else {
				f[i][k]=max(f[i][k],f[j][k-1]+g[j+2][i]);}
				vis[i][k]=1;
			}
		}
	}
	wr(f[m][k1]);
	return 0;
}
