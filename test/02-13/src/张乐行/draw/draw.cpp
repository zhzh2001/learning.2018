#include<cstdio>
#include<algorithm>
using namespace std;
const int N=105,M=205,K=25,inf=1e7;
int n,m,nk,i,j,x;
int a[N][M],v[N],g[N],mxv[N],lv[N],mxlv[N];
int val[M][M],f[M][K];
int read(){
	char c=getchar();int k=0,p=0;
	for (;c<48||c>57;c=getchar()) if (c=='-') p=1;
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;
	return p?-k:k;
}
void gmax(int &x,int k){if (k>x) x=k;}
int main(){
	freopen("draw.in","r",stdin);
	freopen("draw.out","w",stdout);
	n=read();m=read();nk=read();
	if (m<4*nk-1||n<=2){puts("Orz J!!!");return 0;}
	for (i=1;i<=n;i++)
		for (j=1;j<=m;j++) a[i][j]=read();
	for (i=1;i<=m;i++) for (j=1;j<=m+1;j++) val[i][j]=-inf;
	for (i=2;i<m;i++){
		for (x=1;x<=n;x++)
			v[x]=lv[x]=a[x][i],mxlv[x]=-inf,
			g[x]=g[x-1]+a[x][i];
		for (j=1;i-j>0&&i+j<=m;j++){
			int res=-inf;
			for (x=1;x<=n;x++) v[x]+=a[x][i-j]+a[x][i+j];
			for (mxv[0]=-inf,x=1;x<=n;x++)
				mxv[x]=max(mxv[x-1],v[x]-g[x]);
			for (x=3;x<=n;x++){
				lv[x]+=a[x][i-j];gmax(mxlv[x],lv[x]);
				gmax(res,mxlv[x]+g[x-1]+mxv[x-2]);
			}
			gmax(val[i-j][i+j+1],res);
		}
	}
	for (i=m-2;i;i--) for (j=i+3;j<=m+1;j++)
		gmax(val[i][j],max(val[i+1][j],val[i][j-1]));
	for (j=1;j<=nk;j++) f[0][j]=-inf;
	for (i=1;i<=m+1;i++) for (j=0;j<=nk;j++){
		f[i][j]=f[i-1][j];
		if (j) for (x=0;x+4<=i;x++)
			gmax(f[i][j],f[x][j-1]+val[x+1][i]);
	}
	printf("%d",f[m+1][nk]);
}
