#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
const double eps=1e-7;
const int N=1e5+5,M=1e6+5;
int n,m,i,j;ll f[3][M];
struct mat{int xa,ya,xb,yb;}p;
int read(){
	char c=getchar();int k=0,p=0;
	for (;c<48||c>57;c=getchar()) if (c=='-') p=1;
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;
	return p?-k:k;
}
void write(ll x){if (x>9) write(x/10);putchar(x%10+48);}
ll cal(mat A,mat B){
	int xa=max(A.xa,B.xa),xb=min(A.xb,B.xb);
	int ya=max(A.ya,B.ya),yb=min(A.yb,B.yb);
	if (xa>xb||ya>yb) return 0;
	return (ll)(xb-xa+1)*(yb-ya+1);
}
ll v[3];double a[4][4];
bool chk(double k){
	return k>-eps&&k<eps;
}
void work(int l,int r){
	int i,j,k;
	for (i=0;i<=2;i++){
		int x=l+i;if (x>r) break;
		v[i]=cal((mat){-x,-x,x,x},p);
	}
	if (r-l<=2){
		for (i=0;i<=r-l;i++)
			f[0][l+i]+=v[i],f[0][l+i+1]-=v[i];
		return;
	}
	for (i=0;i<=2;i++){
		int x=l+i;a[i][3]=v[i];
		a[i][0]=1;a[i][1]=x;a[i][2]=(ll)x*x;
	}
	for (i=0;i<=2;i++){
		if (chk(a[i][i])) for (j=i+1;j<=2;i++)
			if (!chk(a[j][i])) swap(a[i],a[j]);
		for (j=i+1;j<=2;j++){
			double v=a[j][i]/a[i][i];
			for (k=i;k<=3;k++)
				a[j][k]-=a[i][k]*v;
		}
	}
	for (i=2;i>=0;i--){
		double v=a[i][3]/a[i][i];
		f[i][l]+=v;f[i][r+1]-=v;
		for (j=0;j<i;j++) a[j][3]-=v*a[j][i];
	}
}
int d[10];
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	for (n=read(),m=1e6,i=1;i<=n;i++){
		p=(mat){read(),read(),read(),read()};
		d[1]=abs(p.xa);d[2]=d[1]+1;
		d[3]=abs(p.xb);d[4]=d[3]+1;
		d[5]=abs(p.ya);d[6]=d[5]+1;
		d[7]=abs(p.yb);d[8]=d[7]+1;
		d[9]=m+1;
		sort(d+1,d+9+1);
		for (j=1;j<=9;j++)
			work(d[j-1],d[j]-1);
	}
	for (j=0;j<=2;j++) for (i=1;i<=m;i++) 
		f[j][i]+=f[j][i-1];
	for (int Q=read();Q--;){
		int x=read();
		ll ans=f[0][x]+f[1][x]*x+f[2][x]*x*x;
		write(ans);putchar('\n');
	}
}
