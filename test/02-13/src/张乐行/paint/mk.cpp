#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
const int P=1e6+1;
int n,Q,i;
int ran(){return (rand()<<15)+rand();}
int get(){
	int x=ran()%P;
	if (rand()%2) x=-x;
	return x;
}
int main(){
	freopen("paint.in","w",stdout);
	srand(time(0));rand();
	n=1e5;Q=1e6;
	printf("%d\n",n);
	for (i=1;i<=n;i++){
		int xl=get(),xr=get();
		int yl=get(),yr=get();
		if (xl>xr) swap(xl,xr);
		if (yl>yr) swap(yl,yr);
		printf("%d %d %d %d\n",xl,xr,yl,yr);
	}
	printf("%d\n",Q);
	for (;Q--;){
		int x=ran()%P;
		printf("%d ",x);
	}
	putchar('\n');
}
