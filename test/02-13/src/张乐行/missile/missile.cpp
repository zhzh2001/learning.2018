#include<cstdio>
#include<cmath>
using namespace std;
const double eps=1e-9,pi2=acos(0);
int v0,k1,k2;int a,b;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
double cal(double alpha){
	double x=sin(alpha),y=sqrt(1-x*x);
	return a*x*y+b*x*x;
}
int main(){
	freopen("missile.in","r",stdin);
	freopen("missile.out","w",stdout);
	for (int TEST=read();TEST--;){
		v0=read();k1=read();k2=read();
		a=4*k1*v0*v0;b=k2*v0*v0;
		double l=0,r=pi2;
		while (r-l>eps){
			double m1=(2*l+r)/3,m2=(l+2*r)/3;
			if (cal(m1)<cal(m2)) l=m1; else r=m2;
		}
		printf("%.3lf %.3lf\n",l,cal(l)/20.0);
	}
}
