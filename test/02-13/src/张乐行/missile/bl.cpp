#include<cstdio>
#include<cmath>
using namespace std;
const double eps=1e-6,pi2=acos(0);
const int K=1e5;
int v0,k1,k2;int a,b;
double alpha,ans,S;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
int main(){
	freopen("missile.in","r",stdin);
	freopen("bl.out","w",stdout);
	for (int TEST=read();TEST--;){
		v0=read();k1=read();k2=read();
		a=4*k1*v0*v0;b=k2*v0*v0;S=0;
		for (int i=0;i<=pi2*K;i++){
			alpha=i;alpha/=K;
			double x=sin(alpha),y=sqrt(1-x*x);
			double res=a*x*y+b*x*x;
			if (res>S) ans=alpha,S=res;
		}
		S/=20.0;
		printf("%.3lf %.3lf\n",ans,S+eps);
	}
}
