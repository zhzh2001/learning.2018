#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef unsigned long long ull;
int n,x;
const int N=10000003;
ull a[N],y,b[N],z;
const int md=1<<23;
int head[md],next[N];
ull seed,opt,ans;	
inline ull rnd(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	if(opt==0){
		return seed;
	}else{
		return seed&((1ll<<32)-1);
	}
}
inline void pda(){
	register int x=n;
	for(register int i=md-1;i>=0;i--)if(head[i]!=-1){
		for(register int j=head[i];j!=-1;j=next[j])a[x--]=b[j];
	}
}
int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	scanf("%d%llu%llu",&n,&seed,&opt);
	for(register int i=1;i<=n;i++){
		a[i]=rnd();
	}
	y=md-1;
	for(register int i=0;i<md;i++)head[i]=-1;
	for(register int i=1;i<=n;i++){
		z=a[i]&y;
		next[i]=head[z];
		head[z]=i;
		b[i]=a[i];
	}
	pda();
	
	for(register int i=0;i<md;i++)head[i]=-1;
	for(register int i=1;i<=n;i++){
		z=(a[i]>>22)&y;
		next[i]=head[z];
		head[z]=i;
		b[i]=a[i];
	}
	pda();
	
	for(register int i=0;i<md;i++)head[i]=-1;
	for(register int i=1;i<=n;i++){
		z=(a[i]>>44)&y;
		next[i]=head[z];
		head[z]=i;
		b[i]=a[i];
	}
	pda();
	
	
	ans=a[2]-a[1];
	for(int i=2;i<=n;i++){
		if(a[i]-a[i-1]>ans){
			ans=a[i]-a[i-1];
		}
	}
	printf("%llu\n",ans);
	return 0;
}
