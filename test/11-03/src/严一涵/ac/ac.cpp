#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
int n;
LL md;
int main(){
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
	scanf("%d%lld",&n,&md);
	// n+1个点的图中，已经有一条边，可以使图联通的连边方案数*2即为答案。
	if(n==1){
		printf("%lld\n",2%md);
	}else if(n==2){
		printf("%lld\n",6%md);
	}else if(n==3){
		printf("%lld\n",48%md);
	}
	return 0;
}
