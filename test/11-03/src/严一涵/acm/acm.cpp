#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int T,n,m,a[100003],b[100003],x;
LL ans;
int main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	T=read();
	while(T-->0){
		n=read();
		ans=0;
		for(int i=1;i<=n;i++){
			a[i]=read();
		}
		if(n==0){
			puts("0");
			continue;
		}
		sort(a+1,a+n+1);
		m=1;
		b[1]=1;
		for(int i=2;i<=n;i++){
			if(a[i]!=a[i-1]){
				a[++m]=a[i];
				b[m]=1;
			}else{
				b[m]++;
			}
		}
		x=-1;
		for(int i=1;i<=m;i++){
			if(b[i]>=a[i]){
				ans+=b[i];
				b[i]%=a[i];
				ans-=b[i];
			}
			if(b[i]==1||a[i]==1)x=i;
		}
		if(x!=-1){
			if(a[x]==1)ans--;
			b[x]=0;
		}
		bool fg=0;
		for(int i=1;i<=m;i++){
			if(b[i]!=0){
				fg=1;
				ans+=a[i];
			}
		}
		if(!fg)ans++;
		printf("%lld\n",ans);
	}
	return 0;
}
