#include<bits/stdc++.h>
#define int unsigned long long
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=0,kk=1;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return k*kk; 
}void write(int x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(int x){write(x);puts("");}
using namespace std;
int n,a[10000010],v[65537][510],ans,seed,opt,b[100010];
inline unsigned long long rnd()
{
		seed^=seed<<13;
		seed^=seed>>7;
		seed^=seed<<17;
		return seed;
}

signed main(){
	freopen("acmd.in","r",stdin);freopen("acmd.out","w",stdout);
	n=read();seed=read();opt=read();
	for (int i=1;i<=n;i++){
		a[i]=opt?rnd()&((1LL<<32)-1):rnd();
		if (seed==0){n=i;break;}
	}
	for (int i=1;i<=n;i++){
		int dd=a[i]&65535;
		v[dd][++b[dd]]=a[i];
	}n=0;
	for (int i=0;i<65536;i++){
		int dd=b[i];b[i]=0;
		for (int j=1;j<=dd;j++)a[++n]=v[i][j];
	}
	for (int i=1;i<=n;i++){
		int dd=(a[i]>>16)&65535;
		v[dd][++b[dd]]=a[i];
	}n=0;
	for (int i=0;i<65536;i++){
		int dd=b[i];b[i]=0;
		for (int j=1;j<=dd;j++)a[++n]=v[i][j];
	}
	for (int i=1;i<=n;i++){
		int dd=(a[i]>>32)&65535;
		v[dd][++b[dd]]=a[i];
	}n=0;
	for (int i=0;i<65536;i++){
		int dd=b[i];b[i]=0;
		for (int j=1;j<=dd;j++)a[++n]=v[i][j];
	}
	for (int i=1;i<=n;i++){
		int dd=(a[i]>>48)&65535;
		v[dd][++b[dd]]=a[i];
	}n=0;
	for (int i=0;i<65536;i++){
		int dd=b[i];b[i]=0;
		for (int j=1;j<=dd;j++)a[++n]=v[i][j];
	}
	for (int i=1;i<n;i++)ans=max(ans,a[i+1]-a[i]);
	writeln(ans);//writeln(clock());
}/*
100000000000
0 0
*/
