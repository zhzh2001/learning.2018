#include<bits/stdc++.h>
#define int long long
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=0,kk=1;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return k*kk; 
}void write(int x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(int x){write(x);puts("");}
using namespace std;
int t,n,a[1000010],kk,now,ans;
signed main(){
	freopen("acm.in","r",stdin);freopen("acm.out","w",stdout);
	t=read();while (t--){
		n=read();for (int i=1;i<=n;i++)a[i]=read();
		sort(a+1,a+1+n);kk=now=0;//kk为最大的1所在地，now代表除了最大的1有没有空余的 
		for (int i=1;i<=n;){
			int dd=a[i],k=0;
			while (a[i]==dd&&i<=n)i++,k++;
			ans+=k/dd*dd;
			if (k%dd!=0){
				if (k%dd==1)now=kk,kk=dd;else now=dd;
				ans+=dd;
			}
		}if (kk&&now)ans-=kk;else if (kk)ans-=kk-1;else ans++;
		writeln(ans);ans=0;
	}
}
