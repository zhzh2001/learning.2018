#include<bits/stdc++.h>
typedef long long ll;
typedef unsigned long long ull;
void setIO() {
	freopen("acmd.in", "r", stdin);
	freopen("acmd.out", "w", stdout);
}
using namespace std;
const int N = 1e7 + 3;
int n, opt;
ull seed;
ull a[N], ans;
#define pii pair<ull,ull>
ull rnd()
{
		seed^=seed<<13;
		seed^=seed>>7;
		seed^=seed<<17;
		return seed;
}
namespace G1 {
	void main() {
		sort(a + 1, a + 1 + n);
		for(int i = 2; i <= n; ++i)
			ans = max(ans, a[i] - a[i - 1]);
		cout<<ans<<endl;		
	}
}
namespace G2 {
	set<ull>v[101000];
	void main() {
		const int p = 100000;
		for(int i = 1; i <= n; ++i) v[a[i] / p].insert(a[i]);
		set<ull>::iterator j, k;
		int pre = -1;
		for(int i = 0; i < 22000; ++i) {
			j = v[i].begin();
			if(j == v[i].end()) continue;
			if(~pre && v[pre].size()) ans = max(ans, *j - *v[pre].rbegin());
			k = j;
			++j;
			for(; j != v[i].end(); k = j,++j)
				ans = max(ans, *j - *k);
			pre = i;
		}
		cout<<ans<<endl;
	}
}
ll NUM = ((1ll<<32) - 1);
int main() {
	setIO();
	//cin>>n>>seed>>opt;
	srand(time(NULL)); 
	n = 10000000;
	bool fg2 = 1;
	/*for(int i = 1; i <= n; ++i) {
		a[i] = rnd();
		if(opt) a[i] &= NUM;
	}*/
	for(int i = 1; i <= n; ++i)
		a[i] = (ll)rand() * rand() * rand() % NUM;
	G2::main();
//	G1::main();
	return 0;
}
