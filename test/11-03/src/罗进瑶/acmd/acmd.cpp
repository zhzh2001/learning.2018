#include<bits/stdc++.h>
typedef long long ll;
typedef unsigned long long ull;
void setIO() {
	freopen("acmd.in", "r", stdin);
	freopen("acmd.out", "w", stdout);
}
using namespace std;
const int N = 1e7 + 3;
int n, opt;
ull seed;
ull a[N], ans;
#define pii pair<ull,ull>
inline ull rnd()
{
		seed^=seed<<13;
		seed^=seed>>7;
		seed^=seed<<17;
		return seed;
}
namespace G1 {
	void main() {
		sort(a + 1, a + 1 + n);
		for(register int i = 2; i <= n; ++i)
			ans = max(ans, a[i] - a[i - 1]);
		cout<<ans<<endl;		
	}
}
ll NUM = ((1ll<<32) - 1);
int main() {
	setIO();
	cin>>n>>seed>>opt;
	for(register int i = 1; i <= n; ++i) {
		a[i] = rnd();
		if(opt) a[i] &= NUM;
	}
	G1::main();
	return 0;
}
