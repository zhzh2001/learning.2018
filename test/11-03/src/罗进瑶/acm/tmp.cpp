#include<bits/stdc++.h>
typedef long long ll;
void setIO() {
	freopen("acm.in", "r", stdin);
	freopen("acm.out", "w", stdout);
}
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';
ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
const int N = 1e5 + 11;
ll a[N], n, b[N], tot, c[N];
ll ans;
ll solve() {
	ll res = 0;
	for(int i = (b[1] == 0 ? 2 : 1); i <= tot; ++i) {
		ll t1 = c[i] / b[i];
		ll t2 = c[i] % b[i];
		res += t1 * b[i];
		if(t2 > 0) res += b[i];
	}
	return res;
}
bool t1[N], t2[N];
int main() {
	setIO();
	int T = read();
	while(T--) {
		n = read();
		tot = 0;
		for(int i = 1; i <= n; ++i)
			b[++tot] = a[i] = read();
		sort(b +1, b + 1 + tot);
		tot = unique(b + 1, b +1 + tot) - b - 1;
		for(int i = 1; i <= tot; ++i) c[i] = 0;
		for(int i = 1; i <= n; ++i) {
			a[i] = lower_bound(b + 1,b + 1 + tot, a[i]) - b;
			++c[a[i]];
		}
		ll sum = solve();
		/*特殊的情况是0*/
		if(b[1] == 0) {
			ans = 1e17;
			for(int i = 2; i <= tot; ++i)
				if(c[i] % b[i] != 0)
					ans = min(ans, sum);
				else ans = min(ans, sum + b[i]);
			writeln(ans);
			continue;
		}
		for(int i = 1; i <= tot; ++i) {
			t1[i] = t1[i - 1];
			if(i > 1) t1[i] |= (c[i - 1] % b[i - 1] != 0);
		}
		for(int i = tot; i >= 1; --i) {
			t2[i] = t2[i + 1];
			if(i < tot) t2[i] |= (c[i + 1] % b[i + 1] != 0);
		}
		/*特殊的情况是1*/
		for(int i = (b[1] == 1 ? 2 : 1); i <= tot; ++i) if(!t1[i] && !t2[i])
			ans = min(ans, sum + 1 - (c[i] % b[i] == 1) * b[i]);
		else ans = min(ans, sum - (c[i] % b[i] == 1) * b[i]);
		if(b[1] == 1) {
			if(!t2[1]) ans = min(ans, sum + 2 - (c[1] % b[1]));
			else ans = min(ans, sum - 1);
		}
		writeln(ans);
	}
	return 0;
}
