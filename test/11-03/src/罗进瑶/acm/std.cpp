#include<bits/stdc++.h>
typedef long long ll;
void setIO() {
	freopen("acm.in", "r", stdin);
	freopen("std.out", "w", stdout);
}
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';
ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
const int N = 1e5 + 11;
int a[N], n, b[N], tot, c[N];
ll ans;
ll solve(int x) {
	bool fg = 0;
	ll res = 0;
	for(int i = 1; i <= tot; ++i) {
		ll t1 = c[i] / b[i];
		ll t2 = c[i] % b[i];
		if(t2 > 0 && !fg && i != x) {
			fg = 1;
			++t2;
			if(t2 == b[i]) ++t1, t2 = 0;
		}
		res += t1 * b[i];
		if(t2 > 0) res += b[i];
	}
	if(!fg) ++res;
	return res;
}
int main() {
	setIO();
	int T = read();
	while(T--) {
		n = read();
		tot = 0;
		for(int i = 1; i <= n; ++i)
			b[++tot] = a[i] = read();
		sort(b +1, b + 1 + tot);
		tot = unique(b + 1, b +1 + tot) - b - 1;
		for(int i = 1; i <= tot; ++i) c[i] = 0;
		for(int i = 1; i <= n; ++i) {
			a[i] = lower_bound(b + 1,b + 1 + tot, a[i]) - b;
			++c[a[i]];
		}
		ans = 1e17;
		for(int i = 1; i <= tot; ++i) {
			--c[i];
			ans = min(ans, solve(i));
			++c[i];
		}
		writeln(ans);
	}
	return 0;
}
