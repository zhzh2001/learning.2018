#include<bits/stdc++.h>
typedef long long ll;
void setIO() {
	freopen("acm.in", "w", stdout);
}
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';
ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
int random(int x){
	return (ll) rand() * rand() * rand() % x;
}
int main() {
	setIO();
	srand(time(NULL));
	int n = 10000;
	puts("1");
	writeln(n);
	for(int i = 1; i <= n; ++i)
		printf("%d ", random(11) + 1);
	puts("");
	return 0;
}
