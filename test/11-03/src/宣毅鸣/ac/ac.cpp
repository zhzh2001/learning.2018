#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=105;
const ll Ans[10]={0,2,6,48,834,28752,2713542,256097184};
ll ans;
int m,flag[N],n,cnt;
struct zz{
	int l,r;
}a[N],b[N];
int pd(int x){
	int num=0;
	for (int i=0;i<cnt;i++)
		if ((1<<i)&x)b[num].l=a[i].l,b[num++].r=a[i].r;
	for (int i=0;i<(1<<num);i++){
		int tot=0;
		for (int j=1;j<=n;j++){
			int flag=1;
			for (int k=0;k<num;k++)
				if ((b[k].l<=j&&j<=b[k].r)!=((1<<k)&i?1:0))flag=0;
			tot+=flag;
			if (tot>=2)return 0;
		}
	}
	return 1;
}
int main(){
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
	scanf("%d%d",&n,&m);
	printf("%lld\n",Ans[n]%m);
	return 0;
}
