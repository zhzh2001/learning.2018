#include<bits/stdc++.h>
using namespace std;
const int N=1e7+5,M=20;
#define ull unsigned long long
int n,opt,l;
ull a[N],seed,size,b[N],f[1<<M],ans;
ull Rand(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
ull get1(ull x){
	return x>>size;
}
int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	cin>>n>>seed>>opt;
	for (int i=1;i<=n;i++)a[i]=Rand();
	if (opt)for (int i=1;i<=n;i++)a[i]&=((1LL<<32)-1);
	if (opt)size=15;
	else size=47;
	ull Max=0;
	for (int i=1;i<=n;i++)f[get1(a[i])]++;
	for (int i=1;i<(1<<M);i++)f[i]+=f[i-1];
	for (int i=1;i<=n;i++)b[f[get1(a[i])]--]=a[i];
	for (int i=1;i<=n;i++)a[i]=b[i];
	for (int i=1,j=1;i<=n;i=j){
		ull k=get1(a[i]);
		while (j<=n&&k==get1(a[j]))j++;
		sort(a+i,a+j);
	}
	for (int i=2;i<=n;i++)ans=max(ans,a[i]-a[i-1]);
	cout<<ans;
	return 0;
}
