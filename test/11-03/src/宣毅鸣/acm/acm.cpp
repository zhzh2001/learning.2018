#include<bits/stdc++.h>
using namespace std;
const int N=100005;
typedef long long ll;
ll ans,sum;
int a[N],flag1[N],flag2[N],b[N];
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read(){
	int x=0;char c=0;
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())x=x*10+c-48;
	return x;
}
int main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	for (int T=read();T;T--){
		ans=1e18;sum=0;
		int n=read();
		for (int i=1;i<=n;i++)a[i]=read();
		sort(a+1,a+n+1);
		memset(flag2,0,sizeof flag2);
		memset(flag1,0,sizeof flag1);
		memset(b,0,sizeof b);
		int num=1;b[1]=1;
		for (int i=2;i<=n;i++){
			if (a[i]!=a[num])a[++num]=a[i];
			b[num]++;
		}
		n=num;int tot=0;
		for (int i=1;i<=n;i++){
			int k=((b[i]-1)/a[i]+1)*a[i];
			if (b[i]%a[i]==1)flag1[i]=a[i];
			sum+=k;
			if (k-b[i])flag2[i]=1,tot++;
		}
		for (int i=1;i<=n;i++)ans=min(ans,sum-flag1[i]+(tot-flag2[i]>=1?0:1));
		printf("%lld\n",ans);
	}
	return 0;
}
