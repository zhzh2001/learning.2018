#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 100010
using namespace std;
LL a[N],b[N],c[N],d[N],f[N];
LL n,m,i,j,k,l,r,T,cnt,ans;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	T=read();
	rep(_i,1,T){ 
		n=read();cnt=0;ans=0;
		rep(i,1,n) a[i]=0,b[i]=0;
		rep(i,1,n) f[i]=read();
		sort(f+1,f+n+1);
		rep(i,1,n){
			if(f[i]==f[i-1]) b[cnt]++;
			else a[++cnt]=f[i],b[cnt]=1;
		}
		rep(i,1,cnt) c[i]=b[i]/a[i],d[i]=b[i]%a[i];
		rep(i,1,cnt) ans+=a[i]*c[i];
		rep(i,1,cnt) if(d[i]) ans+=a[i];
		rep(i,1,cnt) if(!d[i]) c[i]--,d[i]=a[i];
		l=-1; r=-1;
		drp(i,cnt,1) if(d[i]==1){l=i;break;}
		rep(i,1,cnt){
			
			if(d[i]<a[i]&&r!=-1) r=0;
			if(d[i]<a[i]&&r==-1) r=i;
			if(!r) break;
		}
		if(l!=-1){
			if(a[l]==1){
				if(r!=-1) printf("%lld\n",ans-1);
				else printf("%lld\n",ans+1);
			}
			else
			if(r!=-1) {
				if(l==r) printf("%lld\n",ans-a[l]+1);
				else printf("%lld\n",ans-a[l]);
			}
			else printf("%lld\n",ans-a[l]+1);
		}
		else printf("%lld\n",ans+1);
	}
	return 0;
}
