#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 10010010
#define ull unsigned long long 
using namespace std;
inline ull read(){
    ull x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
ull a[N],maxs[N],mins[N],Max,Min,seed,n;
bool bo[N];
int opt;
ull rnd()
{
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
inline ull get_id(ull x){
	ull HH=(Max-Min)/n;
	int res=(int)((x-Min)/HH);
//	printf("%d ",res);
	return res;
}
inline ull max(ull x,ull y) {
	return (x>y)?x:y;
}
inline ull min(ull x,ull y) {
	return (x<y)?x:y;
}
int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	n=read();
	
	seed=read(),opt=read();
	Max=0;Min=((2<<64)-1);
//	printf("%llu %llu\n",Max,Min);
	rep(i,1,n) {
		a[i]=rnd();
		if (opt==1) a[i]=a[i]&(((1LL<<32)-1)*1ull);
	//	a[i]=read();
		Max=max(Max,a[i]),Min=min(Min,a[i]);	
	}
//	printf("%llu %llu %llu\n",Max,Min,Max-Min);
	if (Max==Min){printf("0\n");return 0;};
	memset(bo,0,sizeof(bo));
	int cnt;
	rep(i,1,n){
		int Id=get_id(a[i]);
		//printf("%llu %d ",a[i],Id);++cnt;
	//	if (cnt%10==0) printf("\n");
		maxs[Id]=bo[Id]?max(maxs[Id],a[i]):a[i];
		mins[Id]=bo[Id]?min(mins[Id],a[i]):a[i];
		bo[Id]=1;
	}
	ull ans=0,pre_max=0;
	int i=0;
	for (;i<=n;++i) if (bo[i]) {
		pre_max=maxs[i-1];break;	
	}
	for (;i<=n;i++){
		if (bo[i]){
			ans=max(ans,mins[i]-pre_max);
			pre_max=maxs[i];
		}
	}
	printf("%llu",ans);
	return 0;
}
