#include<bits/stdc++.h>
using namespace std;
const int N = 10000005;
int n, opt, tnt;
unsigned long long seed, a[2][N];
inline unsigned long long read(){
  seed^=seed<<13;
  seed^=seed>>7;
  seed^=seed<<17;
  return seed;
}
int c[256];
inline void SORTTTT() {
  for (int x = 0; x < 8; x++) {
    int d = x*8;
    for (int i = 0; i < 256; i++)
      c[i] = 0;
    for (int i = 1; i <= n; i++)
      c[(a[tnt][i]>>d)&255]++;
    for (int i = 1; i < 256; i++)
      c[i] += c[i-1];
    for (int i = n; i; i--)
      a[tnt^1][c[(a[tnt][i]>>d)&255]--] = a[tnt][i];
    tnt ^= 1;
  }
}
int main() {
  freopen("acmd.in","r",stdin);
  freopen("acmd.out","w",stdout);
  scanf("%d%llu%d", &n, &seed, &opt);
  unsigned long long Ad = (1ll<<32)-1;
  for (int i = 1; i <= n; i++) {
    a[0][i] = read();
    if (opt == 1) a[0][i]&=Ad;
  }
  if (n <= 1000000) sort(a[0]+1,a[0]+1+n);
  else SORTTTT();
  unsigned long long ans = 0;
  for (int i = 2; i <= n; i++)
    ans = max(ans, a[tnt][i]-a[tnt][i-1]);
  printf("%llu\n", ans);
  return 0;
}