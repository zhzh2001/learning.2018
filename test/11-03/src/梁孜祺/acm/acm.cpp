#include<bits/stdc++.h>
using namespace std;
#define gc getchar
inline int read() {
  int x=0, f=0; char ch=gc();
  for (;ch<'0'||ch>'9';ch=gc())
    if (ch=='-') f=1;
  for (;ch>='0'&&ch<='9';ch=gc())
    x = (x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const long long oo = 1e15;
const int N = 1000005;
int n, s[N], a[N];
inline void init() {
  n = read();
  for (int i = 1; i <= n; i++)
    s[i] = read();
}
long long ans, sum;
inline void solve() {
  ans = 0;
  for (int i = 1; i <= n; i++) {
    int mx;
    if (a[i]%s[i] == 0) mx = a[i]/s[i];
    else mx = a[i]/s[i]+1;
    ans += mx*s[i];
  }

//  cout << ans << endl;
  for (int i = n; i; i--)
    if (a[i]%s[i] == 1) {
      ans -= s[i];
      for (int j = 1; j <= n; j++) {
        if (j!=i&&a[j]%s[j]>0){
          printf("%lld\n", ans);
          return;
        }
      }
      ans++;
      printf("%lld\n", ans);
      return;
    }
  for (int i = 1; i <= n; i++)
    if (a[i]%s[i]>0) {
      printf("%lld\n", ans);
      return;
    }
  printf("%lld\n", ans+1);
}
int main() {
  freopen("acm.in","r",stdin);
  freopen("acm.out","w",stdout);
  int T = read();
  while (T--) {
    init(); sort(s+1,s+1+n);
    int tot = 1;a[1] = 1;
    for (int i = 2; i <= n; i++)
      if (s[i] == s[i-1]) a[tot]++;
      else s[++tot] = s[i], a[tot] = 1;
    n = tot;
    solve();
  }
  return 0;
}
/*
 
2 
4 
2 2 2 2 
5 
2 2 2 3 3    

*/