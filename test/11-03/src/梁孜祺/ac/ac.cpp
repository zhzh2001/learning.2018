#include<bits/stdc++.h>
using namespace std;
int n, mod, a[305], ans, l[16], r[16], tot, p[6];
int main() {
  freopen("ac.in","r",stdin);
  freopen("ac.out","w",stdout);
  scanf("%d%d", &n, &mod);
  for (int i = 1; i <= n; i++)
    for (int j = i; j <= n; j++)
      l[tot] = i, r[tot++] = j;
  for (int i = 1; i < (1<<tot); i++) {
    bool flag = 1;
    for (int j = 1; j <= n; j++) {
      memset(p, 0, sizeof p);
      for (int k = 0; k < tot; k++) {
        if ((1<<k)&i) {
          if (l[k] <= j && r[k] >= j) {
            for (int t = l[k]; t <= r[k]; t++) 
              if (!p[t]) p[t] = 1;
            for (int t = 1; t < l[k]; t++) p[t] = -1;
            for (int t = r[k]+1; t <= n; t++) p[t] = -1;
          }else {
            for (int t = l[k]; t <= r[k]; t++)
              p[t] = -1;
          }
        }
      }
      for (int k = 1; k <= n; k++)
        if (j != k&&p[k] != -1){flag = 0; break;}
      if (!flag) break;
    }
    if (flag) ans++;
  }
  printf("%d\n", ans);
  return 0;
}
/*
######
######
######
######
*/