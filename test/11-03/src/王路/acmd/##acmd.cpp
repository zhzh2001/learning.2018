#pragma GCC optimize 2
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
#include <functional>
#include <numeric>
using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;

const int kMaxN = 1e7 + 5, kBuk = 65536;

int n, opt, cnt[kBuk];
ull a[kMaxN], b[kMaxN];

int main() {
  freopen("acmd.in", "r", stdin);
  freopen("acmd.out", "w", stdout);
  ull _seed;
  scanf("%d%llu%d", &n, &_seed, &opt);
  for (register int i = 1; i <= n; ++i) {
    _seed ^= _seed << 13;
    _seed ^= _seed >> 7;
    _seed ^= _seed << 17;
    a[i] = _seed;
  }
  for (int d = 0, offset = 0; d < 4; ++d, offset += 16) {
    fill(cnt, cnt + kBuk, 0);
    for (register int i = 1; i <= n; ++i)
      ++cnt[((b[i] = a[i]) >> offset) & 0xffff];
    for (register int i = 1; i < kBuk; ++i)
      cnt[i] += cnt[i - 1];
    partial_sum(cnt, cnt + kBuk, cnt);
    for (register int i = n; i > 0; --i)
      a[cnt[(b[i] >> offset) & 0xffff]--] = b[i];
  }
  ull ans = 0;
  for (register int i = 2; i <= n; ++i) {
    ans = max(ans, a[i] - a[i - 1]);
  }
  printf("%lld\n", ans);
  return 0;
}