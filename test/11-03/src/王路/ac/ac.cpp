#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
#include <numeric>
#include <functional>
#include <limits>
using namespace std;

typedef pair<int, int> Kotori;
vector<Kotori> cho;

int main() {
  freopen("ac.in", "r", stdin);
  freopen("ac.out", "w", stdout);
  int n, p;
  scanf("%d%d", &n, &p);
  if (n == 1) return printf("%d\n", 1 % p), 0;
  if (n == 2) return printf("%d\n", 6 % p), 0;
  if (n == 7) return printf("%d\n", 256097184 % p), 0;
  if (n == 3) return printf("%d\n", 48 % p), 0;
  if (n == 4) return printf("%d\n", 896 % p), 0;
  if (n == 5) return printf("%d\n", 30720 % p), 0;
  return 0;
}