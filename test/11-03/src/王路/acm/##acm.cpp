#pragma GCC optimize 2
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
    ch = NC(), isneg = 1;
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

typedef map<int, int> Kotori;
typedef set<int> Kami;

const int kMaxN = 1e5 + 5;
int T, n, s[kMaxN];
Kotori mp;
vector<int> st, best;
ll ans;

int main() {
  freopen("acm.in", "r", stdin);
  freopen("acm.out", "w", stdout);
  RI(T);
  while (T--) {
    RI(n);
    mp.clear();
    st.clear();
    best.clear();
    ans = 0;
    for (int i = 1; i <= n; ++i) {
      RI(s[i]);
      ++mp[s[i]];
    }
    for (Kotori::iterator it = mp.begin(); it != mp.end(); ++it) {
      ans += (it->second / it->first) * it->first;
      it->second %= it->first;
      if (it->second) {
        ans += it->first;
        if (it->second == 1)
          best.push_back(it->first); // 可以减少代价
        st.push_back(it->first); // 不花费代价放入
      }
    }
    if (best.empty()) {
      ans += (st.size() < 2);
    } else {
      if (st.size() < 2) { // 自身不能选择
        ans = ans - best.back() + 1;
      } else {
        ans = ans - best.back();
      }
    }
    printf("%lld\n", ans);
  }
  return 0;
}