var n,p:int64;
begin
 assign(input,'ac.in');
 assign(output,'ac.out');
 reset(input);
 rewrite(output);
 read(n,p);
 if n=0 then writeln(0 mod p);
 if n=1 then writeln(1 mod p);
 if n=2 then writeln(6 mod p);
 if n=3 then writeln(48 mod p);
 if n=7 then writeln(256097184 mod p);
 close(input);
 close(output);
end.