var i,j:longint; t,n,x,tot,ans,p:int64;
    a,b,c:array[0..100000]of int64;
procedure sort(l,r:qword);
      var
         i,j,x,y: qword;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
 assign(input,'acm.in');
 assign(output,'acm.out');
 reset(input);
 rewrite(output);
 read(t);
 for j:=1 to t do
 begin
  ans:=0; p:=0;
  read(n);
  for i:=1 to n do read(a[i]);
  sort(1,n);
  tot:=1; b[1]:=a[1]; c[1]:=1;
  for i:=2 to n do
   if a[i]<>a[i-1] then
   begin
    inc(tot);
    b[tot]:=a[i];
    c[tot]:=1;
   end
   else inc(c[tot]);
  for i:=1 to tot do
  begin
   ans:=ans+b[i]*(c[i]div b[i]);
   c[i]:=c[i]mod b[i];
   if c[i]>0 then
   begin
    ans:=ans+b[i];
    inc(p);
   end;
  end;
  if p>0 then
  begin
    if p>1 then
    begin
      for i:=tot downto 1 do
        if c[i]=1 then
        begin
         dec(ans,b[i]);
         break;
        end;
    end
    else
      for i:=1 to tot do
        if c[i]>0 then
        begin
         inc(ans);
         dec(c[i]);
         if c[i]=0 then dec(ans,b[i]);
         break;
        end;
  end
  else inc(ans);
  writeln(ans);
 end;
 close(input);
 close(output);
end.