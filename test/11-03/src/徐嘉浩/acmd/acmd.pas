var i:longint; n,x,y,ans,k:qword;
    a:array[0..10000000]of qword;
procedure sort(l,r:qword);
      var
         i,j,x,y: qword;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
function rnd(var x:qword):qword;
begin
 x:=x xor(x<<13);
 x:=x xor(x>>7);
 x:=x xor(x<<17);
 exit(x);
end;
begin
 assign(input,'acmd.in');
 assign(output,'acmd.out');
 reset(input);
 rewrite(output);
 read(n);
 read(x,y); k:=(1<<32)-1;
 if y=0 then for i:=1 to n do a[i]:=rnd(x)
 else for i:=1 to n do a[i]:=rnd(x)and k;
 sort(1,n);
 ans:=a[2]-a[1];
 for i:=3 to n do
  if a[i]-a[i-1]>ans then ans:=a[i]-a[i-1];
 writeln(ans);
 close(input);
 close(output);
end.