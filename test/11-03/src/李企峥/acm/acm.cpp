#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
#define fi first
#define sd second
using namespace std;
ll n,ans,lzq,lqz,T;
ll a[100100],b[100100],c[100100];
ll x,y;
bool flag;
pair<int,int>d[100100];
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
    return x*f;
}
signed main()
{
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();
		For(i,1,n)a[i]=read();
		sort(a+1,a+n+1);
		x=0;y=0;
		For(i,1,n)
		{
			if(a[i]!=a[i-1])
			{
				c[x]=y;
				x++;
				b[x]=a[i];
				y=0;
			}
			y++;
		}
		c[x]=y;
		lqz=x;
		flag=true;
		lzq=0;
		ans=0;
		For(i,1,lqz)
		{
			c[i]%=b[i];
			if(c[i]!=0)
			{
				flag=false;
				lzq++;
				d[lzq].fi=c[i];
				d[lzq].sd=b[i];
				ans+=b[i]-c[i];
			}
		}
		if(flag)
		{
			cout<<n+1<<endl;
			continue;
		}
//		cout<<ans<<endl;
		sort(d+1,d+lzq+1);
//		cout<<lzq<<endl;
		x=0;
		while(d[x+1].fi==1)x++;
//		cout<<x<<endl;
//		cout<<ans<<endl;
		if(x!=0)
		{
			ans-=d[x].sd;
			ans++;
			Rep(i,1,lzq)
			{
				if(i==x)continue;
				ans--;
				break;
			}
		}
		cout<<n+ans<<endl;
	}
	return 0;
}

