#include<bits/stdc++.h>
#define ll long long
#define ull unsigned ll
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
#define fi first
#define sd second
using namespace std;
ull seed,mx;
ull a[10000100];
ll sz,n,opt;
inline ull read(){
    ull x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
    return x*f;
}
unsigned long long rnd()
{
		seed^=seed<<13;
		seed^=seed>>7;
		seed^=seed<<17;
		return seed;
}
void del(int wz)
{
	a[wz]=a[sz];
	sz--;
	while(wz*2<=sz)
	{
		ull mn=a[wz*2];int mn1=wz*2;
		if(mn1+1<=sz&&a[mn1+1]<mn)
		{
			mn1++;
			mn=a[mn1];
		}
		if(mn<a[wz])
		{
			swap(a[mn1],a[wz]);
			wz=mn1;
		}
		else return;
	}
	return;
}
void up(int wz)
{
	while(a[wz]<a[wz/2]&&wz>1)
	{
		swap(a[wz],a[wz/2]);
		wz/=2;
	}
	return;
}
void ins(ull x)
{
	sz++;
	a[sz]=x;
	up(sz);
	return;
}
signed main()
{
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	n=read();
	seed=read();opt=read();
	For(i,1,n)
	{
		seed=rnd();
		ull x=seed;
		if(opt==1)x=x&((1LL<<32)-1);
		ins(x);
	}
	ull x=a[1];
	del(1);
	while(sz>0)
	{
		ull y=a[1];
		mx=max(mx,y-x);
		del(1);
		x=y;
	}
	cout<<mx<<endl;
	return 0;
}

