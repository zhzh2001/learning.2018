#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
typedef unsigned int uint;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e7+10;
ull n,seed,opt,ans,tmp,val[N];
int a[N],b[N];
inline ull rnd(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
inline ull max(ull a,ull b){return a>b?a:b;}
namespace Subtask1{
	inline void Main(){
		For(i,1,n) val[i]=rnd();
		sort(val+1,val+1+n);
		For(i,2,n) ans=max(ans,val[i]-val[i-1]);
		printf("%lld\n",ans);
	}
}
const int maxn = 1<<16,Maxn = 1<<17;
int Max,R[N],c[N],cnt1[Maxn],cnt2[Maxn],cnt3[Maxn],cnt4[Maxn];
inline void Sort(){
	For(i,1,Maxn-1) cnt1[i]+=cnt1[i-1],cnt2[i]+=cnt2[i-1],cnt3[i]+=cnt3[i-1],cnt4[i]+=cnt4[i-1];
	For(i,1,n) c[cnt4[val[i]&(maxn-1)]--]=i;
	Dow(i,n,1) R[cnt3[b[c[i]]]--]=c[i];
	Dow(i,n,1) c[cnt2[a[R[i]]]--]=R[i];
	Dow(i,n,1) R[cnt1[val[c[i]]>>48]--]=c[i];
}
int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	n=read(),seed=read(),opt=read();
//	n=read();
	if (n<=1000000) return Subtask1::Main(),0;
	For(i,1,n){
		tmp=(opt)?rnd()&((1ll<<32)-1):rnd();
//		tmp=read();
		val[i]=tmp,b[i]=(tmp&((1ll<<32)-1))>>16,a[i]=(tmp>>32)&(maxn-1);
		cnt1[tmp>>48]++;
		cnt2[a[i]]++;
		cnt3[b[i]]++;
		cnt4[tmp&(maxn-1)]++;
	}
	Sort();
	For(i,1,n-1) ans=max(ans,val[R[i+1]]-val[R[i]]);
//	For(i,1,n) printf("%lld ",val[Rank[i]]);puts("");
	printf("%lld\n",ans);
}
/*
10000000
233 0
*/
