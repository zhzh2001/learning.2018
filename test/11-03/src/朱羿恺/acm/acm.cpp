#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(){
    ll x=0;int ch=gc(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=gc();
    if (ch=='-'){f=-1;ch=gc();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=gc();}
    return x*f;
}
const int N = 1e5+10;
int T,n,a[N],cnt,x,b[N],c[N],sum;
ll ans;
int main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	T=read();
	while (T--){
		n=read();
		For(i,1,n) a[i]=read();
		sort(a+1,a+1+n),cnt=0,x=1;
		For(i,1,n) 
			if (a[i]!=a[i+1]||i==n) b[++cnt]=a[i],c[cnt]=x,x=1;
				else x++; 
		/*For(i,1,cnt){
			For(j,1,cnt)
				if (i!=j){
					c[i]--,c[j]++,sum=0;
					For(k,1,cnt) sum+=1ll*b[k]*(c[k]/b[k]+(c[k]%b[k]>0));
					ans=min(ans,sum);
					c[i]++,c[j]--;
				}
			sum=0,c[i]--;
			For(k,1,cnt) sum+=1ll*b[k]*(c[k]/b[k]+(c[k]%b[k]>0));
			ans=min(ans,sum+1),c[i]++;
		}*/
		ans=1e18,sum=0;
		For(i,1,cnt) sum+=(c[i]%b[i]>0);
		ll Sum=0;
		For(i,1,cnt) Sum+=1ll*b[i]*(c[i]/b[i]+(c[i]%b[i]>0));
		ll x;
		For(i,1,cnt){
			x=Sum;
			if (c[i]%b[i]==1) x-=b[i];
			if (sum==1&&(c[i]%b[i]>0)||sum==0) x++;
			ans=min(ans,x);   
		}
		printf("%lld\n",ans); 
	}
}
/*
2
4
2 2 2 2
5
2 2 2 3 3
*/
