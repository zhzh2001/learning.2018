#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ull unsigned long long
inline int rd(){
	int x = 0; char ch = gc();
	for (; !isdigit(ch); ch = gc());
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return x;
}
void file() {
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
}
//defs=============================================
const int MAXN=1e5+10;
int T,N,a[MAXN];
int l[MAXN];
int but[MAXN];
void init();
void work();
ll tot;
int np[MAXN];
//main=============================================
int main() {
	file();
	T=rd();
	while(T--) {
		init();
		work();
	}
	return 0;
}

void init() {
//	memset(a,0,sizeof a);
//	memset(l,0,sizeof l);
//	memset(but,0,sizeof but);
	memset(np,0,sizeof np);
	*l=0;
	tot=0;
	
	N=rd();
	for(int i=1;i<=N;++i) a[i]=rd(),l[++*l]=a[i];
}
ll get_sum(int i,bool check=true) {
	if(but[i]%l[i]==0) return but[i];
	else {
		if(check) np[i]=true;
		return (but[i]/l[i]+1)*l[i];
	}
}
void work() {
	//1 lisan
	sort(l+1,l+*l+1);
	*l=unique(l+1,l+*l+1)-l-1;
	for(int i=1;i<=N;++i) a[i]=lower_bound(l+1,l+*l+1,a[i])-l;
	for(int i=1;i<=N;++i) ++but[a[i]];
	for(int i=1;i<=N;++i) if(but[i]) {
		tot+=get_sum(i);
	}
	//printf("tot=%lld\n",tot);
	for(int i=1;i<=N;++i) np[i]+=np[i-1];
	
	ll ans=1e17;
	//for(int i=0;i<=N;++i) printf("%d ",np[i]);puts("");
	for(int i=1;i<=N;++i) if(but[i]){
		//printf("but[%d]=%d l[%d]=%d\n",i,but[i],i,l[i]);
		ll res=tot-get_sum(i,false);
		//printf("%lld\n",res);
		--but[i];
		res+=get_sum(i,false);
		//printf("%lld\n",res);
		//printf("%d %d\n",np[i-1],np[N]-np[i]);
		if(np[i-1]==0 && (np[N]-np[i])==0) {
			if(l[i]!=1)
				++res;
			else {
				res+=2;
			}
		}
		//printf("%lld\n",res);
		ans=min(ans,res);
		++but[i];
	}
	printf("%lld\n",ans);
	for(int i=1;i<=N;++i) but[a[i]]=0;
}
