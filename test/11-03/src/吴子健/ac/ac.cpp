#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline ll rd() {
	ll x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
void file() {
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
}
ll N,P,ans;
const int MAXN=500;
pa a[300000];int cnt;
bool check(int S);
void solve_1();
void solve_2();
//main========================
int main() {
	file();
	N=rd(),P=rd();
	if(N<=5) {
		solve_1();
	} else {
		solve_2();
	}

	return 0;
}
ll f[MAXN],g[MAXN];
ll C[MAXN][MAXN];
ll qpow(ll x,ll y) {
	ll ans=1;
	for(;y;y>>=1,x=(x*x)%P) if(y&1) ans=(ans*x)%P;
	return ans;
}
ll S(int x){
    if(x==0) return 0;
    return qpow(2,(x*(x-1))/2);
}
void solve_2() {
    f[1]=1; g[1]=0;
    C[0][0]=1;
    for(int i=1;i<=N+2;i++){
        C[i][0]=1;
        for(int j=1;j<=i;j++)
            C[i][j]=(C[i-1][j-1]+C[i-1][j])%P;
    }
    for(int i=2;i<=N+2;i++){
        g[i]=0;
        for(int j=2;j<i;j++)
            g[i] = (g[i] + (C[i-2][j-2]*f[j]%P*S(i-j)%P))%P;
        f[i]=((S(i)-g[i])%P+P)%P;
        //printf("S[%d]=%lld,f[%d]=%lld,g[%d]=%lld\n",i,S(i),i,f[i],i,g[i]);
    }	
    ans=(f[N+1]%P+P)%P;
    printf("%lld\n",ans);
}
void solve_1() {
	for(int i=1;i<=N;++i) for(int j=i;j<=N;++j) a[++cnt]=mk(i,j);
	//printf("cnt=%d\n",cnt);
	for(int i=0;i<(1<<cnt);++i) {
		if(check(i)) {
			++ans;
			ans%=P;
		}
	}
	ans%=P;
	printf("%lld\n",ans);	
}
bool vis[10],mat[10][10];
void dfs(int st) {
	vis[st]=true;
	for(int i=0;i<=N;++i) if(mat[st][i]&&!vis[i]) dfs(i);
}
bool check(int S) {
	memset(vis,0,sizeof vis);
	memset(mat,0,sizeof mat);
	mat[0][N]=mat[N][0]=true;
	for(int i=1;i<=cnt;++i) if((1<<(i-1))&S) {
		mat[a[i].first-1][a[i].second]=mat[a[i].second][a[i].first-1]=true;
	}
	dfs(0);
	for(int i=0;i<=N;++i) if(vis[i]==0) {	
		return false;
	}
	return true;
}
