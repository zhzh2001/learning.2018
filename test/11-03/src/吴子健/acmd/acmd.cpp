#include <bits/stdc++.h>
typedef long long ll;
typedef unsigned long long ull;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ull unsigned long long
inline ull read(){
	ull x = 0; char ch = gc();
	for (; !isdigit(ch); ch = gc());
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return x;
}
ull seed,opt;
unsigned long long rnd() {
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
void file() {
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
}
//defs=============================================
const int MAXN=1e7+200000; 
int N;
ull ans=0,maxv=0,minv=-1,butsize;
ull a[MAXN];
ull mxbut[MAXN],mnbut[MAXN];
bool sz[MAXN];
/**
考虑桶排序
4096个桶?
桶内最大差值不知道
设值域为[L,R]
据鸽巢原理,diff>=(int)(R-L)/(N-1)
只要桶内差一定小于diff就可以规避该问题
 对于值域分至少N个桶! 
*/
//main=============================================
int main() {
	file();
	N=read(); seed=read(); opt=read();
	for(int i=1;i<=N;++i) {
		ull x=rnd();
		if(opt==1) x=x&((1LL<<32)-1);
		a[i]=x;
		if(x>maxv) maxv=x;
		if(x<minv) minv=x;
	}
	butsize=(maxv-minv+1)/N+1;
//	LOG("L=%llu\n",maxv-minv+1);
	if(!butsize) butsize=1;
	int max_id=(maxv-minv)/butsize+1;
	for(int i=0;i<=max_id+100;++i) mnbut[i]=-1;
//	LOG("maxid=%llu\n",max_id);
//	LOG("butsize=%llu\n",butsize);
	for(int i=1;i<=N;++i) {

		int id=(a[i]-minv)/butsize+1;
		//	printf("a[%d]=%llu belogns to but %d\n",i,a[i],id);
		if(id>max_id) max_id=id;
		if(a[i]>mxbut[id]) mxbut[id]=a[i];
		if(a[i]<mnbut[id]) mnbut[id]=a[i];
		//printf("[%llu,%llu]\n",mnbut[id],mxbut[id]);
		sz[id]=true;
	}
//	LOG("maxid=%llu\n",max_id);
	int i=1,j=-1;
	ull max_value=0;
	for(i=1;i<=max_id;++i) if(sz[i]) {
		if(j!=-1) {
		//	printf("cmp but[%d] to but[%d]\n",i,j);
			if(mnbut[i]-mxbut[j]>ans) ans=mnbut[i]-mxbut[j];
			//cout<<ans<<endl;
		}
		j=i;
	}
	cout<<ans<<endl;
	return 0;
}

