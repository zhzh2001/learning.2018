#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
const int MAXN=1e6;
int N=1e7;
//main========================
int main() {
	freopen("acmd.in","w",stdout);
	cout<<N<<" "<<(unsigned long long)rand()*rand()<<" "<<rand()%2<<endl;
	return 0;
}

