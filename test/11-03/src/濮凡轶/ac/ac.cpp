// dp? 反正也来不及打了 

#include <fstream>
#include <algorithm>

using namespace std;

typedef long long LL;

const int maxn = 305;

LL dp[maxn][maxn];

int main()
{
	ifstream fin("ac.in");
	ofstream fout("ac.out");
	int n;
	LL mod;
	fin >> n >> mod;
	for(int l = 1; l <= n; ++l)
		for(int r = 1; r <= l; ++r)
			dp[l][r] = 1;
	for(int l = n; l; --l)
	{
		for(int r = l + 1; r <= n; ++r)
		{
			for(int mid = l; mid <= r; ++mid)
			{
				(dp[l][r] += dp[l][mid - 1] * dp[mid + 1][r] % mod) %= mod;
				for(int a = mid + 1; a <= r; ++a)
					for(int b = l; b <= mid; ++b)
						(dp[l][r] += (dp[l][mid] * dp[mid + 1][r] * 8) % mod) %= mod;
			}
		}
	}
	fout << dp[1][n] << endl;
	fin.close();
	fout.close();
	return 0;
	return 0;
}
