// 如果因为细节被卡就a的出题人
// 别忘了初始化

#include <cstdio>
#include <cctype>

inline char gc()
{
	static char buf[233333],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,233333,stdin),p1==p2)?EOF:*p1++;
}

#define dd c = gc()
template<class T>
inline int read(T& x)
{
	x = 0;
	bool f = false;
	char dd;
	for(; !isdigit(c); dd)
	{
		if(c == '-')
			f = false;
		if(c == EOF)
			return -1;
	}
	for(; isdigit(c); dd)
		x = (x << 1) + (x << 3) + (c ^ 48);
	if(f)
		x = -x;
}
#undef dd

#include <cstring>
#include <queue>
#include <iostream>
#include <algorithm>

using namespace std;

typedef long long LL;

const int maxn = 100005;
const int inf = 0x7fffffff;

int aa[maxn];
int ls[maxn];
int gs[maxn];
int cnt;
priority_queue<int> yi, fy, fyy;

inline void solve() // 为什么最近的题目都是多组数据
{
	cnt = 0;
	memset(gs, 0, sizeof(gs));
	memset(ls, 0, sizeof(ls));
	while(!yi.empty())
		yi.pop();
	while(!fy.empty())
		fy.pop();
	while(!fyy.empty())
		fyy.pop();
	int n;
	read(n);
	for(int i = 1; i <= n; ++i)
		read(aa[i]);
	sort(aa + 1, aa + n + 1);
	bool flag = false;
	int id = 0;
	for(int i = 1; i <= n; ++i)
	{
		if(aa[i] < 0)
		{
			flag = true;
			id = i; // 讲道理出题人应该不会这么毒吧
		}
		if(i == 1 || aa[i] != aa[i - 1])
			ls[++cnt] = aa[i];
		gs[cnt]++;
	}
	bool man = true;
	LL ans = 0;
	for(int i = 1; i <= n; ++i)
	{
		if(ls[i] > 0)
		{
			ans += (gs[i] / ls[i] + ((gs[i] % ls[i]) != 0)) * ls[i];
			gs[i] %= ls[i];
//			cout << i << ' ' << gs[i] << endl;
			if(gs[i])
			{
				man = false;
				if(gs[i] == 1 && gs[i] == ls[i] - 1)
					fyy.push(ls[i] - 1);
				else
				{
					if(gs[i] == 1)
						yi.push(ls[i] - 1);
					else if(gs[i] == ls[i] - 1)
						fy.push(1);
				}
			}
		}
	}
//	cout << "ans = " << ans << endl;
	if(!flag)
	{
		LL tmp = 0;
		if(!yi.empty() && !fy.empty())
			tmp = max(tmp, yi.top() + (LL) fy.top());
//		printf("%lld\n", tmp);
		if(!fyy.empty() && !fy.empty())
			tmp = max(tmp, fyy.top() + (LL) fy.top());
		if(!yi.empty() && !fyy.empty())
			tmp = max(tmp, fyy.top() + (LL) yi.top());
//		printf("%d\n", tmp);
		if(!man)
		{
			if(!yi.empty())
				tmp = max(tmp, (LL) yi.top() + 1);
			if(!fy.empty())
				tmp = max(tmp, (LL) fy.top());
			if(!fyy.empty())
				tmp = max(tmp, (LL) max(1, fyy.top() + 1));
		}
		else
			tmp = -1;
		ans -= tmp;
	}
	else
	{
		LL tmp = 0;
		if(!fy.empty())
			tmp = max(tmp, (LL) fy.top());
		if(!fyy.empty())
			tmp = max(tmp, (LL) max(1, fyy.top() + 1));
		if(man)
			tmp = -1;
		ans -= tmp;
	}
	printf("%lld\n", ans);
}

int main()
{
	freopen("acm.in", "r", stdin);
	freopen("acm.out", "w", stdout);
	int T;
	read(T);
	while(T--)
		solve();
	fclose(stdin);
	fclose(stdout);
	return 0;
}

// zdakking
// sxdakqueen
// stdak
