// 卡常？待会儿用nth_element试试
// 说不定能过呢，要有信仰 
// 假的 

#include <fstream>
#include <algorithm>

using namespace std;

typedef unsigned long long LL;

const int maxn = 10000005;

LL a[maxn];

int main()
{
	ifstream fin("acmd.in");
	ofstream fout("acmd.out");
	int n;
	fin >> n;
	LL sd;
	int opt;
	fin >> sd >> opt;
	register LL seed = sd;
	for(register int i = 1; i <= n; ++i)
	{
		seed ^= seed << 13;
		seed ^= seed >> 7;
		seed ^= seed << 17;
		a[i] = opt ? seed & (0xffffffffull) : seed;
	}
	int len = n / 3;
	int mid1 = len, mid2 = mid1 + len; // , mid3 = mid2 + len;
	nth_element(a + 1, a + n + 1, a + mid1);
	nth_element(a + mid1 + 1, a + n + 1, a + mid2);
	// nth_element(a + mid2 + 1, a + n + 1, a + mid3);
	sort(a + 1, a + mid1 + 1);
	sort(a + mid1 + 1, a + mid2 + 1);
	sort(a + mid2 + 1, a + n + 1);
	// sort(a + mid3 + 1, a + n + 1);
	LL ans = 0;
	for(register int i = 2; i <= n; ++i)
		ans = max(a[i] - a[i - 1], ans);
	fout << ans;
	fin.close();
	fout.close();
	return 0;
}
