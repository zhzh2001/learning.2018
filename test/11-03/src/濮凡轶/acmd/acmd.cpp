// 卡常？待会儿用nth_element试试
// 说不定能过呢，要有信仰 
// 老年机64位能卡过去我女装 

#pragma gcc optimize(2) // 不知道会不会被忽略 

#include <fstream>
#include <algorithm>

using namespace std;

unsigned long long a[10000005];

int main()
{
	ifstream fin("acmd.in");
	ofstream fout("acmd.out");
	register int n;
	fin >> n;
	register unsigned long long seed;
	register int opt;
	fin >> seed >> opt;
	for(register int i = 1; i <= n; ++i)
	{
		seed ^= seed << 13;
		seed ^= seed >> 7;
		seed ^= seed << 17;
		a[i] = opt ? seed & (0xffffffffull) : seed;
	}
	sort(a + 1, a + n + 1);
	register unsigned long long ans = 0;
	for(register int i = 2; i <= n; ++i)
		ans = a[i] - a[i - 1] > ans ? a[i] - a[i - 1] : ans;
	fout << ans;
	fin.close();
	fout.close();
	return 0;
}
