#include <bits/stdc++.h>
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define ull unsigned long long
inline ull read(){
	ull x = 0; char ch = gc();
	for (; !isdigit(ch); ch = gc());
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return x;
}
const int N=10000005;
int n,opt,cnt;
ull seed,ans,a[N];
inline ull rnd(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	seed=1;rnd();
	n=read();seed=read();opt=read();
	for(int i=1;i<=n;i++)a[i]=rnd();
	sort(a+1,a+n+1);
	for(int i=1;i<n;i++)ans=max(ans,a[i+1]-a[i]);
	cout<<ans;
	return 0;
}
