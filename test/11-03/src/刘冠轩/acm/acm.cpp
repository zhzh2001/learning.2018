#include <bits/stdc++.h>
#define int long long
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read(){
	int x = 0; char ch = gc();
	for (; !isdigit(ch); ch = gc());
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return x;
}
const int N=100005;
int n,m,s[N],sum,q[N],Q[N],cnt,mx,w;
signed main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	int T=read();
	while(T--){
		n=read();
		for(int i=1;i<=n;i++)s[i]=read();
		sort(s+1,s+n+1);sum=1;cnt=0;
		for(int i=2;i<=n;i++)
			if(s[i]!=s[i-1]){
				q[++cnt]=s[i-1];
				Q[cnt]=sum;
				sum=1;
			}else sum++;
		q[++cnt]=s[n];
		Q[cnt]=sum;
		sum=mx=w=0;
		for(int i=1;i<=cnt;i++){
			sum+=(Q[i]/q[i]*q[i]);
			if(Q[i]%q[i]>0){
				sum+=q[i];
				if(Q[i]%q[i]==1)mx=q[i];
			}else w++;
		}
		if(w==cnt){
			cout<<sum+1<<'\n';
			continue;
		}
		if(w==cnt-1){
			if(mx)cout<<sum-mx+1<<'\n';
			else cout<<sum+1<<'\n';
			continue;
		}
		cout<<sum-mx<<'\n';
	}
	return 0;
}
