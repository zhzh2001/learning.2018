#include <bits/stdc++.h>
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read(){
	int x = 0; char ch = gc();
	for (; !isdigit(ch); ch = gc());
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return x;
}
int n,P,cnt,ans;
int main(){
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
	n=read();P=read();
	if(n==1){cout<<2%P;return 0;}
	if(n==2){cout<<6%P;return 0;}
	if(n==3){cout<<48%P;return 0;}
	return 0;
}
