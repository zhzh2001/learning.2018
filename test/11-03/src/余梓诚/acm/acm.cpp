#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
void judge(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
}
const int N=110000,inf=1<<30;
int n,T,a[N],sum[N];
int main(){
	judge();
	scanf("%d",&T);
	while (T--){
		int ans=inf,Max=0;
		memset(sum,0,sizeof(sum));
		scanf("%d",&n);
		for (int i=1;i<=n;i++) scanf("%d",&a[i]),Max=max(Max,a[i]);
		for (int i=1;i<=n;i++){
			int t=0; bool flag=0;
			for (int j=1;j<=n;j++){
				if (i==j) continue;
				if (sum[a[j]]<a[j]) sum[a[j]]++;
				else t+=a[j],sum[a[j]]=1;
			}
			for (int j=1;j<=Max;j++){
				if (!sum[j]) continue;
				t+=j;
				if (sum[j]==j);
				else if (j!=a[i]) flag=1;
			}
			if (!flag) t++;
			ans=min(ans,t);
		}
		printf("%d\n",ans);
	}
	return 0;
}
