#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
void judge(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
}
#define inf ((1<<64)-1)
const int N=10000001;
int n,opt;
unsigned long long Max=0,Min=inf,seed;
unsigned long long ans;
unsigned long long rnd(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
unsigned long long MAX(unsigned long long a,unsigned long long b){
	return (a>b)?a:b;
}
unsigned long long MIN(unsigned long long a,unsigned long long b){
	return (a<b)?a:b;
}
unsigned long long a[N];
void run(){
	unsigned long long* minx=new unsigned long long[n+1]();
	unsigned long long* maxn=new unsigned long long[n+1]();
	bool* flag=new bool[n+1]();
	unsigned long long len=(Max-Min)/(n-1);
	for (register int i=0;i<n;i++){
		unsigned long long t=(a[i]-Min)/len;
		if (!flag[t]){
			flag[t]=1;
			minx[t]=maxn[t]=a[i];
			continue;
		}
		minx[t]=MIN(minx[t],a[i]);
		maxn[t]=MAX(maxn[t],a[i]);
	}
	unsigned long long pre=maxn[0];
	for (register int i=1;i<n+1;i++){
        if (flag[i]){
	        ans=MAX(ans,minx[i]-pre);
	        pre=maxn[i];
        }
    }
    delete []minx;
	delete []maxn;
	delete []flag;
}
int main(){
	judge();
	cin>>n>>seed>>opt;
	for (register int i=0;i<n;i++){
		a[i]=rnd();
		if (opt==1) a[i]=a[i]&((1LL<<32)-1);
		Max=MAX(Max,a[i]);
		Min=MIN(Min,a[i]);
	}
	if (Max==Min) return puts("0"),0;
	run();
	cout<<ans<<endl;
	return 0;
}
