#include<bits/stdc++.h>
using namespace std;
#define ull unsigned long long
ull seed,opt;int n;
inline ull rnd()
{
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
#define maxn 10000100
ull a[maxn];
inline void init()
{
	scanf("%d",&n);
	scanf("%llu%llu",&seed,&opt);
	for(register int i=1;i<=n;i++)
	{
		a[i]=rnd();
		if(opt) a[i]&=(1ll<<32)-1;
	}
}
/*
int head[16],nxt[maxn],tot;ull val[maxn];
inline void add(int a,ull b){nxt[++tot]=head[a];val[tot]=b;head[a]=tot;}
ull stk[maxn];int top=0;
inline void Numsort()
{
	for(register int B=0;B<(opt?32:64);B+=4)
	{
		memset(head,0,sizeof(head));tot=0;
		for(register int i=1;i<=n;i++) add((a[i]>>B)&15,a[i]);
		register int cnt=0;
		for(register int i=0;i<16;i++)
		{
			register int cur=head[i];
			while(cur!=0) stk[++top]=val[cur],cur=nxt[cur];
			while(top) a[++cnt]=stk[top--];
		}
	}
}
*/
ull tmp[maxn];
inline void Mergesort(int l,int r)
{
	if(l==r) return;
	register int mid=(l+r)>>1;
	Mergesort(l,mid);Mergesort(mid+1,r);
	register int i=l,j=mid+1,k=l;
	while(i<=mid&&j<=r)
	{
		if(a[i]<a[j]) tmp[k]=a[i],i++,k++;
		else tmp[k]=a[j],j++,k++;
	}
	while(i<=mid) tmp[k]=a[i],i++,k++;
	while(j<=r) tmp[k]=a[j],j++,k++;
	for(i=l;i<=r;i++) a[i]=tmp[i];
}
int main()
{
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	init();
	Mergesort(1,n);
	//Numsort();
	ull ans=0;
	for(int i=2;i<=n;i++) ans=max(ans,a[i]-a[i-1]);
	printf("%llu",ans);
	return 0;
}
/*
10000000
6666668888888 0
*/
