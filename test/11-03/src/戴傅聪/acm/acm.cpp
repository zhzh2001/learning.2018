#include<bits/stdc++.h>
using namespace std;
#define maxn 100100
#define int long long
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read(){
	int x = 0; char ch = gc();
	for (; !isdigit(ch); ch = gc());
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return x;
}
int n,a[maxn];
int num[maxn],cnt[maxn],tot,totcnt;
int ans=1e18;int needed=0;
inline void init()
{
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	sort(a+1,a+n+1);tot=0;
	for(int i=1;i<=n;i++)
	{
		if(i==1||a[i]!=a[i-1]) num[++tot]=a[i],cnt[tot]=1;
		else cnt[tot]++;
	}
	totcnt=0;needed=0;
	for(int i=1;i<=tot;i++) totcnt+=(cnt[i]+num[i]-1)/num[i]*num[i];
	for(int i=1;i<=tot;i++) if(cnt[i]%num[i]) needed++;
	ans=1e18;
}
inline void Try(int x)
{
	if(cnt[x]%num[x]) needed--;
	if(cnt[x]%num[x]==1)
	{
		if(needed) ans=min(ans,totcnt-num[x]);
		else ans=min(ans,totcnt-num[x]+1);
	}
	else
	{
		if(needed) ans=min(ans,totcnt);
		else ans=min(ans,totcnt+1);
	}
	if(cnt[x]%num[x]) needed++;
}
inline void solve()
{
	init();
	for(int i=1;i<=tot;i++) Try(i);
	printf("%lld\n",ans);
}
#undef int
int main()
#define int long long
{
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	int T;T=read();
	while(T--) solve();
	return 0;
}
