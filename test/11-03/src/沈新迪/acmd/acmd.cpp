#include<bits/stdc++.h>
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ull unsigned long long
inline ull read(){
	ull x = 0; char ch = gc();
	for (; !isdigit(ch); ch = gc());
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return x;
}
unsigned long long n,seed,opt,ans;
unsigned long long a[10000005];
unsigned long long rnd()
{
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
int main()
{
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	n=read();seed=read();opt=read();
	for(unsigned long long i=1;i<=n;++i) seed=opt?rnd()&((1ll<<32)-1):rnd(),a[i]=seed;
	sort(a+1,a+n+1);
	for(unsigned long long i=1;i<n;++i) ans=max(ans,a[i+1]-a[i]);
	cout<<ans;
	return 0;
}
