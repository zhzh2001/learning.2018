#include<bits/stdc++.h>
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
int read()
{
	char ch=getchar();int x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
int T,n,mx,ff,jl;
long long ans;
int a[100005];
int main()
{
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();ans=n;mx=0;ff=0;jl=0;
		for(int i=1;i<=n;++i) a[i]=read();
		sort(a+1,a+n+1);
		for(int i=1,j=1;i<=n;i=j)
		{
			while(j<=n&&a[j]==a[i]) j++;
			if(a[i]<=0) {ff=1;continue;}
			int tmp=(j-i)%a[i];
			if((tmp==1||a[i]==1)&&!ff) mx=max(mx,a[i]);
			if(tmp) jl++,ans+=a[i]-tmp;
		}
		if(ff) 
		{
			if(ans!=n) ans--;
		}
		else
		{
			if(ans==n) ans++;
			else if(mx) ans-=mx;
			else if(jl==1) ans++;
			ans=max(ans,(long long)n);
		}
		write(ans);puts("");
	}
	return 0;
}
