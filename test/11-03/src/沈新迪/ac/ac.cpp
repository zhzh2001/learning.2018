#include<bits/stdc++.h>
using namespace std;
int n,mod,ans,tot;
int jc[305],ny[305];
int fa[305],fro[27],to[27];
int getfa(int aa)
{
	return fa[aa]==aa?aa:fa[aa]=getfa(fa[aa]);
}
int main()
{
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
	cin>>n>>mod;
	for(int i=1;i<=n;++i)
	for(int j=i+1;j<=n+1;++j) fro[++tot]=i,to[tot]=j;
	for(int i=1;i<(1<<tot);++i)
	{
		for(int j=1;j<=n;++j) fa[j]=j;
		fa[n+1]=1;
		int sum=0;
		for(int j=1;j<=tot;++j) 
		if(i&(1<<(j-1)))
		{
			int u=getfa(fro[j]),v=getfa(to[j]);
			if(u!=v) sum++,fa[u]=v;
		}
		if(sum==n-1) ans=(ans+1)%mod;
	}
	printf("%d",ans);
	return 0;
}
