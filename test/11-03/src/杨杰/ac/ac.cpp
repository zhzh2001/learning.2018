#include <cstdio>
using namespace std;
const int N=305;
struct edge {
	int u,v;
}e[N*N];
int n,P,cnt,ans;
int fa[N],vis[N*N];
int find(int x) {return x==fa[x]?x:fa[x]=find(fa[x]);}
int check() {
	for (int i=0;i<=n;i++) fa[i]=i;
	int num=n+1;
	for (int i=1;i<=cnt;i++) if (i==1 || vis[i]) {
		int u=find(e[i].u),v=find(e[i].v);
		if (u==v) continue;
		num--;fa[v]=u;
		if (num==1) break;
	}
	return num==1;
}
void dfs(int x,int num) {
	if (x>cnt) {
//		if (num<n) return;
		ans=(ans+check())%P;
		return;
	}
	vis[x]=0;
	dfs(x+1,num);
	vis[x]=1;
	dfs(x+1,num+1);
}
int main() {
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
	scanf("%d%d",&n,&P);
	for (int i=0;i<=n;i++)
		for (int j=n;j>i;j--) e[++cnt].u=i,e[cnt].v=j;
	dfs(1,0);
	printf("%d\n",ans%P);
}
