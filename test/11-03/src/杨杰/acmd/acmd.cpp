#include <bits/stdc++.h>
using namespace std;
#define ull unsigned long long
const int N=1e7+7;
int n;
ull seed,opt,a[N];
ull rnd() {
//	ull seed=rand();
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
int main() {
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	scanf("%d",&n);
	scanf("%llu%llu",&seed,&opt);
//	srand(seed);
	for (int i=1;i<=n;i++) 
		if (!opt) a[i]=rnd();
		else a[i]=rnd()&(1ll<<32);
	sort(a+1,a+n+1);
	ull ans=0;
	for (int i=2;i<=n;i++) ans=max(ans,a[i]-a[i-1]);
	printf("%llu\n",ans);
}
