#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=1e5+7;
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#define ll long long
bool cmp(int a,int b) {return a>b;};
int T,n;
int a[N],s[N],b[N];
ll ans;
int main() {
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	T=read();
	while (T--) {
		memset(s,0,sizeof s);
		ans=0;
		int la=0,flg=0,ct=0;
		n=read();
		for (int i=1;i<=n;i++) a[i]=read();
		sort(a+1,a+n+1);
		for (int i=1;i<=n;i++) {
			if (a[i]!=a[i-1]) b[++ct]=a[i],s[ct]=1;
			else s[ct]++;
		}
		for (int i=1;i<=ct;i++) {
			int x=s[i]/b[i];s[i]%=b[i];
			ans+=1ll*b[i]*x;
			if (s[i]) ans+=b[i],flg++;
			if (s[i]==1) la=b[i];
		}
		if (la) {
			if (flg>1) ans-=la;
			else ans-=la-1;
		}
		else if (flg<=1) ans++; 
		printf("%lld\n",ans);
	}
}
