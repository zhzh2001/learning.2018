#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int SIZE = 1 << 17;
char buffer[SIZE];
int pointer = SIZE;
char Advance() {
    if (pointer == SIZE) {
        fread(buffer, 1, SIZE, stdin);
        pointer = 0;
    }
    return buffer[pointer++];
}
int Read() {
    int answer = 0;
    char ch = Advance();
    while (!isdigit(ch))
        ch = Advance();
    while (isdigit(ch)) {
        answer = answer * 10 + ch - '0';
        ch = Advance();
    }
    return answer;
}

const int MAXN = 300;
int n,mod;
int f[1 + MAXN], g[1 + MAXN][1 + MAXN], C[1 + 2 * MAXN][1 + 2 * MAXN], p[1 + MAXN];

int ksm(int base, int power, int mod) {
    int answer = 1;
    while (power) {
        if (power % 2)
            answer = (1LL * answer * base) % mod;
        base = (1LL * base * base) % mod;
        power /= 2;
    }
    return answer;
}

void Precompute(int n, int mod) {
    for (int i = 0; i <= n; i++)
        p[i] = ksm(2, i * (i + 1) / 2, mod);
    g[0][0] = 1;
    for (int i = 0; i < n; i++)
        for (int j = 2 * i; j <= n; j++)
            for (int k = 2; k + j <= n; k++)
                g[i + 1][j + k] = (1LL * g[i][j] * p[k - 2] + g[i + 1][j + k]) % mod;
    C[0][0] = 1;
    for (int i = 1; i <= 2 * n; i++) {
        C[i][0] = 1;
        for (int j = 1; j <= i; j++)
            C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % mod;
    }
}

int main() {
    //freopen("tema.in", "r", stdin);
    //freopen("tema.out", "w", stdout);
    freopen("ac.in","r",stdin);
    freopen("ac.out","w",stdout);
    scanf("%d%d", &n, &mod);
    Precompute(n, mod);
    for (int i = 1; i <= n; i++) 
    {
        f[i] = p[i];
        for (int j = 1; j <= i; j++)
            for (int k = 2 * j; k <= i; k++) 
                f[i]=(f[i]-(ll)g[j][k]*f[i-k+j]%mod*C[i-k+j][j]%mod+mod)%mod;
    }
    printf("%d\n", f[n]);
    return 0;
}