#include<bits/stdc++.h>
using namespace std;
#define int long long
#define ll long long
const int N=100005;
pair<int,int> b[N];
int a[N];
int n,t,m;
inline void read(int &x)
{
    char c=getchar();
    x=0;
    while (c>'9'||c<'0')
        c=getchar();
    while (c>='0'&&c<='9')
    {
        x=x*10+c-'0';
        c=getchar();
    }
}
signed main()
{
    freopen("acm.in","r",stdin);
    freopen("acm.out","w",stdout);
    read(t);
    while (t--)
    {
        read(n);
        for (int i=1;i<=n;++i)
            read(a[i]);
        sort(a+1,a+1+n);
        m=0;
        for (int i=1;i<=n;++i)
            if (b[m].first!=a[i])
                b[++m]=make_pair(a[i],1);
            else
                ++b[m].second;
        ll ans=0;
        for (int i=1;i<=m;++i)
            ans+=b[i].first*((b[i].second+b[i].first-1)/b[i].first);
        int p=0;
        for (int i=1;i<=m;++i)
            if (b[i].second%b[i].first)
                ++p;
        for (int i=m;i>=1;--i)
            if (b[i].second%b[i].first==1||(b[i].first==1&&p!=0))
            {
                ans-=b[i].first;
                if (b[i].first!=1)
                    --p;
                break;
            }
        if (p==0)
            ++ans;
        cout<<ans<<'\n';
        for (int i=0;i<=m;++i)
            b[i]=make_pair(0,0);
    }
    return 0;
}