program acm;
 var
  a,b,c:array[0..100001] of longint;
  i,i1,n,t:longint;
  ssum,spum,squm,sgum:int64;
 procedure haha(l,r:longint);
  var
   o,p,m,t:longint;
  procedure hahaswap(var x,y:longint);
   begin
    t:=x;
    x:=y;
    y:=t;
   end;
  function hahacmp(x,y:longint):boolean;
   begin
    exit(x<y);
   end;
  begin
   o:=l;
   p:=r;
   m:=a[(l+r)>>1];
   repeat
    while hahacmp(a[o],m) do inc(o);
    while hahacmp(m,a[p]) do dec(p);
    if o>p then break;
    hahaswap(a[o],a[p]);
    inc(o);
    dec(p);
   until o>p;
   if o<r then haha(o,r);
   if l<p then haha(l,p);
  end;
 begin
  assign(input,'acm.in');
  assign(output,'acm.out');
  reset(input);
  rewrite(output);
  readln(t);
  for i1:=1 to t do
   begin
    readln(n);
    for i:=1 to n do
     read(A[i]);
    readln;
    haha(1,n);
    ssum:=0;
    spum:=0;
    squm:=0;
    sgum:=0;
    a[0]:=0;
    a[n+1]:=0;
    for i:=1 to n+1 do
     begin
      if (a[i]<>a[i-1]) and (squm>0) then
       begin
        if squm<>a[i-1] then
         begin
          inc(ssum,a[i-1]-squm+squm);
          inc(spum);
          b[spum]:=a[i-1];
          c[spum]:=squm;
          if squm=1 then sgum:=a[i-1];
         end;
        squm:=0;
       end;
      //if i=n+1 then continue;
      inc(squm);
      if squm>=a[i] then
       begin
        dec(squm,a[i]);
        inc(ssum,a[i]);
       end;
     end;
    if spum=0 then
     if a[n]<>1 then writeln(ssum+1)
                else writeln(ssum)
              else if spum=1 then
               if sgum=0 then writeln(ssum+1)
                         else writeln(ssum-sgum+1)
                             else writeln(ssum-sgum);
   end;
  close(input);
  close(output);
 end.
