#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define inf 0x3f3f3f3f
#define maxn 100010
#define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int n,a[maxn],ans;
vector<pair<int,int> > v;
signed main()
{
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	for(int t=read();t--;)
	{
		n=read();
		for(int i=1;i<=n;a[i++]=read());
		sort(a+1,a+n+1);
		a[n+1]=inf;
		ans=0;
		v.clear();
		for(int i=1;i<=n;i++)
		{
			int lst=i;
			for(;a[i+1]==a[i];i++);
			ans+=(i-lst+1)/a[i]*a[i];
			if((i-lst+1)%a[i])
			{
				ans+=a[i];
				v.push_back(make_pair(a[i],(i-lst+1)%a[i]));
			}
		}
		// wrs(v.size());
		// wrs(ans);
		int lll=v.size();
		for(int i=0;i<v.size();i++)
			if(v[i].second==1) lll=i;
		if(lll!=v.size()) ans-=v[lll].first;
		wln(v.size()?ans:ans+1);
	}
	return 0;
}
