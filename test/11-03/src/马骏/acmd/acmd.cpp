#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 10000010
#define CTT 100000
#define ull unsigned long long
#define inf 0xffffffffffffffff
using namespace std;
inline void write(ull x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+'0');}
inline ull read(){ull d=0;int w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
// void wln(int x){write(x);putchar('\n');}
// void wrs(int x){write(x);putchar(' ');}
int n,opt;
ull sd,a[maxn],mst[maxn],ans;
inline ull rd()
{
	sd^=sd<<13;
	sd^=sd>>7;
	sd^=sd<<17;
	return sd;
}
inline void jp()
{
	ull ma=a[0];
	register ull bkt[CTT];
	for(int i=1;i<n;i++)
		ma=max(ma,a[i]);
	for(ull exp=1;ma/exp>0;exp=inf/CTT>=exp?exp*CTT:inf)
	{
		memset(bkt,0,sizeof bkt);
		for(int j=0;j<n;j++)
			bkt[(a[j]/exp)%CTT]++;
		for(int j=1;j<CTT;j++)
			bkt[j]+=bkt[j-1];
		for(int j=n;j;j--)
		{
			mst[bkt[(a[j-1]/exp)%CTT]-1]=a[j-1];
			bkt[(a[j-1]/exp)%CTT]--;
		}
		for(int j=0;j<n;j++)
			a[j]=mst[j];
	}
}
signed main()
{
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	n=read();
	sd=read();
	opt=read();
	for(int i=0;i<n;i++)
	{
		a[i]=rd();
		if(opt) a[i]&=((ull)1<<32)-1;
	}
	jp();
	for(int i=1;i<n;i++)
		ans=max(ans,a[i]-a[i-1]);
	write(ans);
	return 0;
}
