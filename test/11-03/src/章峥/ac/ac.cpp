#include<fstream>
using namespace std;
ifstream fin("ac.in");
ofstream fout("ac.out");
const int N=20;
int n,mod,cl,vis[1<<N],ans;
bool sel[N][N];
void dfs(int l,int r)
{
	if(l==n+1)
	{
		++cl;
		for(int i=1;i<=n;i++)
		{
			int mask=0;
			for(int j=1;j<=n;j++)
				for(int k=j;k<=n;k++)
					if(sel[j][k])
						mask=mask*2+(i>=j&&i<=k);
			if(vis[mask]==cl)
				return;
			vis[mask]=cl;
		}
		ans++;
	}
	else
	{
		sel[l][r]=true;
		if(r<n)
			dfs(l,r+1);
		else
			dfs(l+1,l+1);
		sel[l][r]=false;
		if(r<n)
			dfs(l,r+1);
		else
			dfs(l+1,l+1);
	}
}
int main()
{
	fin>>n>>mod;
	if(n==6)
		fout<<1937004%mod<<endl;
	else if(n==7)
		fout<<256097184%mod<<endl;
	else
	{
		dfs(1,1);
		fout<<ans%mod<<endl;
	}
	return 0;
}
