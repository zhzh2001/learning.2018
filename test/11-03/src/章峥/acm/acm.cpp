#pragma GCC optimize 2
#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
inline char gc(){
    static char buf[1000000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ull int
inline ull read(){
	ull x = 0; char ch = gc();
	for (; !isdigit(ch); ch = gc());
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return x;
}
const int N=100005;
const long long INF=1e18;
int a[N];
pair<int,int> b[N];
int main()
{
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	int t=read();
	while(t--)
	{
		int n=read();
		for(int i=1;i<=n;i++)
			a[i]=read();
		sort(a+1,a+n+1);
		int cc=0,cnt=0;
		long long sum=0;
		for(int i=1,j=1;i<=n;i=j)
		{
			for(;j<=n&&a[j]==a[i];j++);
			b[++cc]=make_pair(a[i],j-i);
			sum+=(j-i)/a[i]*a[i];
			if((j-i)%a[i])
			{
				cnt++;
				sum+=a[i];
			}
		}
		long long ans=INF;
		for(int i=1;i<=cc;i++)
		{
			long long now=sum;
			if(b[i].second%b[i].first==1)
				now-=b[i].first;
			if(cnt-(bool)(b[i].second%b[i].first)==0)
				now++;
			ans=min(ans,now);
		}
		if(b[1].first==1)
		{
			if(cnt)
				ans=min(ans,sum-1);
			else
				ans=min(ans,sum+1);
		}
		printf("%lld\n",ans);
	}
	return 0;
}
