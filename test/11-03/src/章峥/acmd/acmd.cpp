#pragma GCC optimize 2
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("acmd.in");
ofstream fout("acmd.out");
const unsigned long long mask=(1ll<<32)-1,B0=2e13,B1=5000,INF=0xffffffffffffffffull;
const int B=1e6,N=1e7;
unsigned long long seed;
unsigned long long vmx[B],vmn[B];
int main()
{
	int n,opt;
	fin>>n>>seed>>opt;
	unsigned long long mn=INF;
	for(int i=0;i<B;i++)
	{
		vmx[i]=0;
		vmn[i]=INF;
	}
	for(int i=0;i<n;i++)
	{
		seed^=seed<<13;
		seed^=seed>>7;
		seed^=seed<<17;
		unsigned long long val=seed;
		if(opt)
		{
			val&=mask;
			if(val>vmx[val/B1])
				vmx[val/B1]=val;
			if(val<vmn[val/B1])
				vmn[val/B1]=val;
		}
		else
		{
			if(val>vmx[val/B0])
				vmx[val/B0]=val;
			if(val<vmn[val/B0])
				vmn[val/B0]=val;
		}
		if(val<mn)
			mn=val;
	}
	unsigned long long ans=0;
	for(int i=0;i<B;i++)
	{
		if(!vmx[i])
			continue;
		ans=max(ans,vmn[i]-mn);
		mn=vmx[i];
	}
	fout<<ans<<endl;
	return 0;
}
