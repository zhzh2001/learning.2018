#pragma GCC optimize 2
#include<fstream>
#include<algorithm>
#define min(x,y) (x)<(y)?(x):(y)
#define max(x,y) (x)>(y)?(x):(y)
using namespace std;
ifstream fin("acmd.in");
ofstream fout("acmd.out");
const unsigned long long mask=(1ll<<32)-1,B0=2e12,B1=500;
const int B=1e7,N=1e7;
unsigned long long seed;
int head[B],nxt[N],e;
unsigned long long v[N];
inline void add_val(int id,unsigned long long val)
{
	v[++e]=val;
	nxt[e]=head[id];
	head[id]=e;
}
int main()
{
	int n,opt;
	fin>>n>>seed>>opt;
	unsigned long long mn=0xffffffffffffffffull;
	for(int i=0;i<n;i++)
	{
		seed^=seed<<13;
		seed^=seed>>7;
		seed^=seed<<17;
		unsigned long long val=seed;
		if(opt)
		{
			val&=mask;
			add_val(val/B1,val);
		}
		else
			add_val(val/B0,val);
		mn=min(mn,val);
	}
	unsigned long long ans=0;
	for(int i=0;i<B;i++)
	{
		if(!head[i])
			continue;
		unsigned long long cmn=v[head[i]],cmx=v[head[i]];
		for(int j=head[i];j;j=nxt[j])
		{
			cmn=min(cmn,v[j]);
			cmx=max(cmx,v[j]);
		}
		ans=max(ans,cmn-mn);
		mn=cmx;
	}
	fout<<ans<<endl;
	return 0;
}
