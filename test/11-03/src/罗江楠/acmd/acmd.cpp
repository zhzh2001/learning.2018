#include <bits/stdc++.h>
#define N 10000020
using namespace std;

typedef unsigned long long ull;

inline ull read() {
  ull x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

ull seed, opt;

inline ull _rand() {
  seed ^= seed << 13;
  seed ^= seed >> 7;
  seed ^= seed << 17;
  return seed;
}

ull s1[N], s2[N], C[256];
ull *A = s1, *B = s2;

int main(int argc, char const *argv[]) {
  freopen("acmd.in", "r", stdin);
  freopen("acmd.out", "w", stdout);

  ull n = read();
  
  seed = read();
  opt = read();
  
  for (register int i = 0; i < n; ++ i) {
    A[i] = _rand();
    if (opt) {
      A[i] = A[i] & ((1ll << 32) - 1);
    }
  }

  for (int x = 0; x < 8; ++ x) {
    int d = x * 8;
    for (register int i = 0; i < 256; ++ i) {
      C[i] = 0;
    }
    for (register int i = 0; i < n; ++ i) {
      ++ C[(A[i] >> d) & 255];
    }
    for (register int i = 1; i < 256; ++ i) {
      C[i] += C[i - 1];
    }
    for (register int i = n - 1; ~ i; -- i) {
      B[-- C[(A[i] >> d) & 255]] = A[i];
    }
    swap(A, B);
  }

  ull ans = 0;
  for (register int i = 1; i < n; ++ i) {
    ans = max(ans, A[i] - A[i - 1]);
  }
  printf("%llu\n", ans);

  return 0;
}