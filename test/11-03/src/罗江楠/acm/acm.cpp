#include <bits/stdc++.h>
#define N 100020
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int x[N], t[N], a[N], top;

long long calc_ans() {
  long long res = 0;
  for (int i = 1; i <= top; ++ i) {
    res += 1ll * a[i] * (t[i] / a[i] + !!(t[i] % a[i]));
  }
  return res;
}

int main(int argc, char const *argv[]) {
  freopen("acm.in", "r", stdin);
  freopen("acm.out", "w", stdout);

  int T = read();

  while (T --) {
    int n = read();
    for (int i = 1; i <= n; ++ i) {
      x[i] = read();
    }
    top = 0;
    sort(x + 1, x + n + 1);
    for (int i = 1; i <= n; ++ i) {
      if (x[i] != x[i - 1]) {
        a[++ top] = x[i];
        t[top] = 1;
      } else {
        ++ t[top];
      }
    }

    if (top == 1) {
      -- t[top];
      a[++ top] = 1;
      t[top] = 1;
      printf("%lld\n", calc_ans());
      continue;
    }

    int movement = 0;
    for (int i = top; i; -- i) {
      if (t[i] % a[i] == 1) {
        movement = i;
        break;
      }
    }
    if (movement) {
      // try to decrease cost
      // puts("decrease!!!");
      for (int i = top; i; -- i) {
        if (i != movement && t[i] % a[i]) {
          -- t[movement];
          ++ t[i];
          break;
        }
      }
    } else {
      // try to avoid increase cost
      bool flag = 0;
      for (int i = top; i; -- i) {
        if (t[i] % a[i]) {
          ++ t[i];
          if (i == top) {
            -- t[top - 1];
          } else {
            -- t[top];
          }
          flag = 1;
          break;
        }
      }
      // i can only increase cost
      if (!flag) {
        // puts("increase!!!");
        -- t[top];
        if (a[1] != 1) {
          a[++ top] = 1;
          t[top] = 1;
        } else {
          ++ t[1];
        }
      }
    }

    printf("%lld\n", calc_ans());
  }

  return 0;
}
