#include <bits/stdc++.h>
#define N 100020
using namespace std;

typedef long long ll;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int n, mod;

inline ll fast_pow(ll x, ll y) {
  ll z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}
inline ll inv(ll x) {
  return fast_pow(x, mod - 2);
}

int fz[] = { 0, 1, 6, 48 };

int main(int argc, char const *argv[]) {
  freopen("ac.in", "r", stdin);
  freopen("ac.out", "w", stdout);

  int n = read(), mod = read();

  printf("%d\n", fz[n] % mod);

  return 0;
}