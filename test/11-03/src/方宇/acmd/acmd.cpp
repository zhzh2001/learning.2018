#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 1000000007
#define N 10000010
#define ull unsigned long long 
using namespace std;
ull maxs[N],mins[N],a[N],Max,Min,seed,n,opt;
bool boo[N];
ull read()
{
	ull x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
ull rnd()
{
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
inline ull get_id(ull x)
{
	ull HH=(Max-Min)/n;
	int res=(int)((x-Min)/HH);
	return res;
}
inline ull max(ull x,ull y){return (x>y)?x:y;}
inline ull min(ull x,ull y){return (x<y)?x:y;}
int main()
{
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	n=read();
	seed=read(),opt=read();
	Max=0;Min=((2<<64)-1);
	rep(i,1,n) 
	{
		a[i]=rnd();
		if (opt==1) a[i]=a[i]&(((1LL<<32)-1)*1ull);
		Max=max(Max,a[i]),Min=min(Min,a[i]);	
	}
	if (Max==Min){printf("0\n");return 0;};
	memset(boo,0,sizeof(boo));
	int cnt;
	rep(i,1,n)
	{
		int Id=get_id(a[i]);
		maxs[Id]=boo[Id]?max(maxs[Id],a[i]):a[i];
		mins[Id]=boo[Id]?min(mins[Id],a[i]):a[i];
		boo[Id]=1;
	}
	ull ans=0,pre_max=0;
	int i=0;
	for(;i<=n;++i) if (boo[i]) 
	{
		pre_max=maxs[i-1];break;	
	}
	for(;i<=n;i++)
	{
		if (boo[i])
		{
			ans=max(ans,mins[i]-pre_max);
			pre_max=maxs[i];
		}
	}
	printf("%llu",ans);
	return 0;
}
