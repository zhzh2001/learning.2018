#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020030101
#define N 100010
using namespace std;
LL a[N],b[N],c[N],d[N],f[N];
LL n,m,i,j,k,l,r,T,num,sum;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int main()
{
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	T=read();
	rep(t,1,T)
	{ 
		n=read(); num=0; sum=0;
		rep(i,1,n) a[i]=0,b[i]=0;
		rep(i,1,n) f[i]=read();
		sort(f+1,f+n+1);
		rep(i,1,n)
		{
			if(f[i]==f[i-1]) b[num]++;
			else a[++num]=f[i],b[num]=1;
		}
		rep(i,1,num) c[i]=b[i]/a[i],d[i]=b[i]%a[i];
		rep(i,1,num) sum+=a[i]*c[i];
		rep(i,1,num) if(d[i]) sum+=a[i];
		rep(i,1,num) if(!d[i]) c[i]--,d[i]=a[i];
		l=-1; r=-1;
		dep(i,num,1) if(d[i]==1){l=i; break;}
		rep(i,1,num)
		{
			if(d[i]<a[i]&&r!=-1) r=0;
			if(d[i]<a[i]&&r==-1) r=i;
			if(!r) break;
		}
		if(l!=-1)
		{
			if(a[l]==1)
			{
				if(r!=-1) printf("%lld\n",sum-1);
				else printf("%lld\n",sum+1);
			}
			else
			if(r!=-1) 
			{
				if(l==r) printf("%lld\n",sum-a[l]+1);
				else printf("%lld\n",sum-a[l]);
			}
			else printf("%lld\n",sum-a[l]+1);
		}
		else printf("%lld\n",sum+1);
	}
	return 0;
}
