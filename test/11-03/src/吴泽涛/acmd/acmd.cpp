#include <bits/stdc++.h>
#define LL unsigned long long
#define GG unsigned long long
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
void writeln(GG x) { write(x); putchar('\n'); }

const int N = 1e7+11; 
LL seed, opt; 
LL a[N], Mx; 
int n; 

unsigned long long rnd()
{
		seed^=seed<<13;
		seed^=seed>>7;
		seed^=seed<<17;
		return seed;
}

int main() {
    freopen("acmd.in", "r", stdin); 
    freopen("acmd.out", "w", stdout); 
    n = read();
    seed = read(); opt = read(); 
    For(i, 1, n) {
      a[i] = rnd(); 
      if(opt==1) a[i]=a[i]&((1LL<<32)-1); 
    }
    sort(a+1, a+n+1); 
    Mx = 0; 
    For(i, 2, n) 
      if(a[i]-a[i-1]>Mx) Mx = a[i]-a[i-1]; 
    writeln(Mx); 
}









