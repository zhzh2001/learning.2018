#include<bits/stdc++.h>
#define int long long
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define ull unsigned long long
inline ull read(){
	ull x=0;
	char ch=gc();
	for(;!isdigit(ch);ch=gc());
	for(;isdigit(ch);ch=gc())
		x=x*10+ch-'0';
	return x;
}
struct Edge{
	int mp,sum;
}edge[100001];
bool cmp(Edge x,Edge y){
	return x.mp>y.mp;
}
int t,n,x,ans,maxx,minn,flag,Flag,tot;
int a[100001];
void power(){
	for(int i=1;i<=n;i++){
		flag=1;
		for(int j=1;j<=tot;j++)
			if(edge[j].mp==a[i]){
				flag=0;
				edge[j].sum++;
				if(edge[j].sum>edge[j].mp){
					edge[j].sum=1;
					ans+=edge[j].mp;
				}
				break;
			}
		if(flag){
			tot++;
			edge[tot].mp=a[i];
			edge[tot].sum=1;
			ans+=edge[tot].mp;
		}
	}
	sort(edge+1,edge+1+tot,cmp);
	Flag=flag=0;
	for(int i=1;i<=tot;i++){
		if(edge[i].sum==1){
			flag=edge[i].mp;
			break;
		}
		if(edge[i].sum<edge[i].mp)
			Flag=1;
	}
	if(flag)
		ans-=flag;
	else
		if(!Flag)
			ans++;
	printf("%lld\n",ans);
	return;
}
signed main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	t=read();
	while(t--){
		n=read();
		if(n<=100){
			tot=ans=0;
			for(int i=1;i<=n;i++){
				a[i]=read();
				edge[i].mp=edge[i].sum=0;
			}
			power();
			continue;
		}
		ans=flag=Flag=maxx=0;
		minn=1000000000;
		for(int i=1;i<=n;i++){
			x=read();
			if(!edge[x].sum){
				edge[x].sum=1;
				ans+=x;
				maxx=max(maxx,x);
				minn=min(minn,x);
			}
			else{
				edge[x].sum++;
				if(edge[x].sum>x){
					edge[x].sum=1;
					ans+=x;
				}
			}
		}
		for(int i=maxx;i>=minn;i--){
			if(edge[i].sum==1){
				flag=i;
				break;
			}
			if(edge[i].sum<i)
				Flag=1;
		}
		if(flag)
			ans-=flag;
		else
			if(!Flag)
				ans++;
		printf("%lld\n",ans);
	}
	return 0;
}
