#include<cstdio>
#include<algorithm>
using namespace std;
int n,a[110000];
long long f,sf,ans,s,col,num;
inline int read(){
	unsigned long long x=0;
	char ch=getchar();
	while(!(ch>='0'&&ch<='9'))ch=getchar();
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x;
}
void sor(int l,int r){
	int i=l,j=r,x=a[(l+r)>>1];
	while(i<=j){
		while(a[i]<x)i++;
		while(x<a[j])j--;
		if(i<=j){
			swap(a[i],a[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}
int main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	int T=read();
	while(T--){
		n=read();
		for(int i=1;i<=n;i++)
			a[i]=read();
		sor(1,n);
		num=col=0;
		ans=s=0;
		f=0;
		for(int i=1;i<=n;i++)
			if(a[i]!=col){
				if(num!=col&&num>0){
					f++;
					sf=col;
				}
				if(num==1)s=max(s,col);
				col=a[i];
				num=1;
				ans+=a[i];
			}
			else{
				num++;
				if(num>col){
					ans+=col;
					num-=col;
				}
			}//printf("%lld %lld\n",f,sf);
		if(num!=col&&num>0){
			f++;
			sf=col;
		}
		if(num==1)s=max(s,col);
		if(!f)printf("%lld\n",ans+1);
		else if(f==1&&sf==s)printf("%lld\n",ans-s+1);
			else printf("%lld\n",ans-s);
	}
	return 0;
}
