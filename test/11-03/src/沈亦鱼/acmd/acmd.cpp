#include<cstdio>
#include<algorithm>
using namespace std;
#define ull unsigned long long
int n,opt;
ull seed,sl,sz,block,a,x,ans,mn[11000000],mx[11000000];
inline ull read(){
	ull x=0;
	char ch=getchar();
	while(!(ch>='0'&&ch<='9'))ch=getchar();
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x;
}
ull rnd(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
/*
void sor(int l,int r){
	int i=l,j=r;
	ull x=a[(l+r)>>1];
	while(i<=j){
		while(a[i]<x)i++;
		while(x<a[j])j--;
		if(i<=j){
			swap(a[i],a[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}
*/
int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	register int j=0;
	n=read();
	seed=read();
	opt=read();
//	if(n<=1e6){
//		for(register int i=1;i<=n;i++){
//			a[i]=rnd();
//			x=a[i]/block;
//		}
//		sor(1,0);
//		for(register int i=2;i<=n;i++)
//			ans=max(ans,a[i]-a[i-1]);
//		printf("%llu",ans);
//		return 0;
//	}
	sl=(1LL<<32)-1;
	if(opt)block=429ll;
	else block=1844674407370ll;
	for(register int i=1;i<=n;i++){
		a=rnd();
		if(opt)a&=sl;
		x=a/block;
		mx[x]=max(mx[x],a);
		if(!mn[x])mn[x]=a;
		else mn[x]=min(mn[x],a);
	}
	if(opt){
		sz=sl;
		sl/=block;
	}
	else{
		sz=1;
		sz<<=63;
		sz-=1;
		sz<<=1;
		sz+=1;
		sz/=block;
	}
	for(register int i=1;i<=sz;i++)
		if(mx[i]){
			j=i-1;
			while(!mx[j])j--;
			ans=max(ans,mn[i]-mx[j]);
		}
	printf("%llu",ans);
	return 0;
}/*
yyhakking;
*/
