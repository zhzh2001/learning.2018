#include<cstdio>
#include<algorithm>
using namespace std;
#define ull unsigned long long
int n,opt,j;
ull seed,sl,sz,block,x,ans,a[11000000];
inline ull read(){
	ull x=0;
	char ch=getchar();
	while(!(ch>='0'&&ch<='9'))ch=getchar();
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x;
}
ull rnd(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}

void sor(int l,int r){
	int i=l,j=r;
	ull x=a[(l+r)>>1];
	while(i<=j){
		while(a[i]<x)i++;
		while(x<a[j])j--;
		if(i<=j){
			swap(a[i],a[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}

int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd_.out","w",stdout);
	n=read();
	seed=read();
	opt=read();
		for(register int i=1;i<=n;i++)
			a[i]=rnd();
		sor(1,n);
		for(register int i=2;i<=n;i++)
			ans=max(ans,a[i]-a[i-1]);
		printf("%llu",ans);
	return 0;
}/*
yyhakking;
x y z
x+z>=(x+y+z+1)/2
y+z>(x+y+z)/2
0 1 2 3 4
*/
