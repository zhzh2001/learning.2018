//#include <bits/stdc++.h>
//#define int long long
//using namespace std;
//inline char gc(){
//    static char buf[100000],*p1=buf,*p2=buf;
//    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
//}
//#define gc getchar
//inline int read(){
//	register int  x = 0; register char ch = gc();
//	for (; !isdigit(ch); ch = gc());
//	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
//	return x;
//}
//int t,n,ans;
//map <int,int> mp; 
//set <int> q;
//signed main() {
//	freopen("acm.in","r",stdin); freopen("acm.out","w",stdout);	
//	t=read();
//	while(t--) {
//		n=read();
//		mp.clear();
//		ans=0;
//		q.clear();	
//		for(int i=1;i<=n;i++) {
//			int x=read();
//			mp[x]++;
//			q.insert(x);
//		}
//		int fu_mx=0,zheng_mn=1e18,mxat=0,mnat=0,cnt=0;
//		for(set <int> :: iterator it=q.begin();it!=q.end();it++) {
//			cnt++;
//			int x=*it;
//			if(mp[x]%x==1) {
//				if(fu_mx<x) {
//					fu_mx=x;
//					mxat=x;
//				}
//			} 
//			if(mp[x]%x!=1) {
//				if(fu_mx==0 && !mnat) {
//					mxat=x;
//				}
//			}
//			if(mp[x]%x!=0) {
//				if(zheng_mn>0 || mnat==mxat) {
//					zheng_mn=0;
//					mnat=x;	
//				}
//			}
//			if(mp[x]%x==0) {
//				if(zheng_mn>x){
//					zheng_mn=x;
//					mnat=x;
//				}
//			}	
//		}
//		if(cnt==1) {
//			mp[mxat]--;
//			ans++;
//		} else {
//			mp[mnat]++;
//			mp[mxat]--;	
//		}	
////		cout<<fu_mx<<" "<<zheng_mn<<" "<<mxat<<" "<<mnat<<'\n';
//		for(set <int> :: iterator it=q.begin();it!=q.end();it++) {
//			ans+=((mp[(*it)]-1)/(*it)+1)*(*it);
//		}
//		cout<<ans<<'\n';	
//	}	
//}





#include <bits/stdc++.h>
#define int long long
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	register int  x = 0; register char ch = gc();
	for (; !isdigit(ch); ch = gc());
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return x;
}
int t,n,ans=1e18,a[120000],now; 
map <int,int> mp;
set <int> q;
map <int,bool> vis;
signed main() {
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	t=read();
	while(t--) {
		mp.clear();
		q.clear();
		ans=1e18;
		n=read();
		bool same=true;
		for(register int i=1;i<=n;i++) {
			a[i]=read();
			mp[a[i]]++;	
			q.insert(a[i]);
			if(i>1 && a[i]!=a[i-1]) same=false;
		}
		bool flag=1;
		flag=same;
		now=0;
		for(set <int> :: iterator it=q.begin();it!=q.end();it++) {
			int k=*it;
			now+=((mp[(k)]-1)/(k)+1)*(k);
		}
		ans=now;
		
//		for(int i=1;i<=n;i++)
//			for(int j=i+1;j<=n;j++)
//				if(a[i]!=a[j]) {
//					int ww=now;
//					int sxd1=((mp[(a[i])]-1)/(a[i])+1)*(a[i]);int sxd2;if(mp[a[i]]+1) sxd2=((mp[(a[i])])/(a[i])+1)*(a[i]); else sxd2=1e18;
//					int pfy1=((mp[(a[j])]-1)/(a[j])+1)*(a[j]);int pfy2;if(mp[a[j]]-1) pfy2=((mp[(a[j])]-2)/(a[j])+1)*(a[j]); else pfy2=1e18;
//					ans=min(ans,now-sxd1+sxd2-pfy1+pfy2);
//					int sxd3;if(mp[a[i]]-1) sxd3=((mp[(a[i])]-2)/(a[i])+1)*(a[i]); else sxd3=1e18;
//					int pfy3;if(mp[a[j]]+1) pfy3=((mp[(a[j])])/(a[j])+1)*(a[j]); else pfy3=1e18;
//					ans=min(ans,now-sxd1+sxd3-pfy1+pfy3);	
//				}


//		for(int i=1;i<=n;i++)
//			for(int j=i+1;j<=n;j++) {
//				vis.clear();
//				mp[a[i]]++;
//				mp[a[j]]--;
//				now=0;
//				for(int k=1;k<=n;k++) {
//					if(!vis[a[k]] && mp[a[k]]) {
//						now+=((mp[a[k]]-2)/(a[k])+1)*(a[k]);
//					}
//				}
//				ans=min(ans,now);
//				mp[a[i]]-=2;
//				mp[a[j]]+=2;
//				now=0;
//				for(int k=1;k<=n;k++) {
//					if(!vis[a[k]] && mp[a[k]]) {
//						now+=((mp[a[k]]-2)/(a[k])+1)*(a[k]);
//						vis[a[k]]=1;
//					}
//				}
//				ans=min(ans,now);
//				mp[a[i]]++;
//				mp[a[j]]--;
//			}

		for(register set <int> :: iterator it1=q.begin();it1!=q.end();it1++) {
			set <int> :: iterator sxd=it1;
			sxd++;
			if(sxd!=q.end()) {
				for(register set <int> :: iterator it2=sxd;it2!=q.end();it2++) {
					if((*it1)!=(*it2)) {
						flag=0;
						mp[*it1]++;
						mp[*it2]--;
						int now=0;
						for(set <int> :: iterator k=q.begin();k!=q.end();k++) if(mp[(*k)]){
							now+=((mp[(*k)]-1)/(*k)+1)*(*k);
						}
						ans=min(ans,now);
						mp[*it1]-=2;
						mp[*it2]+=2;
						now=0;
						for(set <int> :: iterator k=q.begin();k!=q.end();k++) if(mp[(*k)]){
							now+=((mp[(*k)]-1)/(*k)+1)*(*k);
						}
						ans=min(ans,now);
						mp[*it1]++;
						mp[*it2]--;					
					}
				}
			}
		}
		
		
		if(flag) {
			int now=0;
			int k=a[1];
			now+=((mp[k]-2)/(k)+1)*(k);
			cout<<now+1<<'\n';
		} else cout<<ans<<'\n';
	}
}
