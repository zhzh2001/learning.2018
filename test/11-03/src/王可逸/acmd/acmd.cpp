#include <bits/stdc++.h>
#define int unsigned long long
#define max(a,b) a>b?a:b
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read(){
	register int  x = 0; register char ch = gc();
	for (; !isdigit(ch); ch = gc());
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return x;
}
inline void write(register int x) {
	if(x>=10) write(x/10);
	register char ch=x%10+'0';
	putchar(ch);	
}
int n,a[10000005],seed,opt,ans;
inline int rnd() {
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
inline void insert_sort(register int a[],register int l,register int r){
	for (register int i=l+1;i<=r;++i){
		register int tmp=a[i],j;
		for (j=i-1;j>=l&&a[j]>tmp;--j)a[j+1]=a[j];
		a[j+1]=tmp;
	}
}
inline void qsort(register int s,register int t){
	if (s+15>t){insert_sort(a,s,t);return;}
	register int i=s,j=t,m=a[s+rand()*rand()%(t-s+1)];//m=a[(s+t)>>1];
	while(i<=j){
		while (a[i]<m) i++;
		while (a[j]>m) j--;
		if (i<=j){
			register int t=a[i];
			a[i]=a[j];
			a[j]=t;
			i++;
			j--;
		}
	}
	if (i<t) qsort(i,t);
	if (s<j) qsort(s,j);
}
signed main() {
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	n=read();seed=read();opt=read();	
	for(register int i=1;i<=n;i++) {
		a[i]=rnd();
		if(opt) a[i]=a[i]&((1ll<<32)-1);
	}
	qsort(1,n);
	for(register int i=2;i<=n;i++) ans=max(ans,a[i]-a[i-1]);
	write(ans);
}
