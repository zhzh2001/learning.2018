#include<bits/stdc++.h>
using namespace std;
#define ll long long
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)	putchar('-'),x=-x;
	if (x>=10)	write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);	puts("");
}
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
const ll N=100010;
int main(){
	freopen("acm.in","w",stdout);
	srand(time(0));
	puts("1000");
	For(i,1,1000){
		ll n=rand()%8+1;
		writeln(n);
		For(i,1,n)write(rand()%8+1),putchar(' ');
		puts("");
	}
}
