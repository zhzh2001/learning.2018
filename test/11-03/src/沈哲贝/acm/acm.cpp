#include<bits/stdc++.h>
using namespace std;
#define ll long long
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)	putchar('-'),x=-x;
	if (x>=10)	write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);	puts("");
}
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
const ll N=200010;
struct dt{ll a,b;}p[N];
bool operator <(dt a,dt b){return a.a<b.a;}
map<ll,ll>mp;
ll nu_,ans,res,cnt,n,a[N],b[N],c[N];
void Init(){
	n=read();cnt=0;nu_=0;
	For(i,1,n)a[i]=read(),nu_+=a[i]==1;
	sort(a+1,a+n+1);
	For(i,1,n)if (a[i]==a[i-1])p[cnt].b++;
	else	p[++cnt].a=a[i],p[cnt].b=1;
}
void get_res(){
	res=0;
	For(i,1,cnt){
		if (p[i].b%p[i].a==0)res+=p[i].b;
		else	res+=(p[i].b/p[i].a+1)*p[i].a;
	}
}
bool Check2(){//有一个刚好多一个 
	ll choice=0;
	FOr(i,cnt,1)if (p[i].b%p[i].a==1){
		choice=i;
		--p[i].b;
		break;
	}
	if (!choice)return 0;
	For(i,1,cnt)if (p[i].b%p[i].a!=0){
		++p[i].b;
		choice=0;
		break;
	}
	if (choice)++ans;
	For(i,1,cnt){
		if(p[i].b%p[i].a==0)	ans+=p[i].b;
		else					ans+=(p[i].b/p[i].a+1)*p[i].a;
	}
	writeln(ans);
	return 1;
}//可以减少 
bool Check3(){//有一个少 
	ll Cnt=0;
	FOr(i,cnt,1){
		Cnt+=(p[i].b%p[i].a!=0);
	}
	if (Cnt&&nu_)writeln(res-1);
	else if (Cnt>=1)	writeln(res);
	else	writeln(res+1);
	return 1;
}//可以不变 ,或者 
ll gt(ll a,ll b){
	if (a%b==0)	return a/b*b;
	else		return (a/b+1)*b;
}
int main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	ll T=read();
	for(;T--;){
		Init();	ans=res=0;
		get_res();
		if ((nu_>0)&&cnt==1){
			writeln(nu_+1);
			continue;
		}
		if (cnt==1){
			ll ans=gt(p[1].b-1,p[1].a)+1;
			writeln(ans);
			continue;
		}
		if (Check2())continue;
		if (Check3())continue;
	}
//	writeln(clock());
}
