#include<bits/stdc++.h>
using namespace std;
#define ll long long
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)	putchar('-'),x=-x;
	if (x>=10)	write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);	puts("");
}
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
const ll N=110;
ll n,ans,cnt[N];
int main(){
	freopen("acm.in","r",stdin);
	freopen("baoli.out","w",stdout);
	ll T=read();
	for(;T--;){
		n=read();	ans=1e9;
		memset(cnt,0,sizeof cnt);
		For(i,1,n)	++cnt[read()];
		For(i,1,8)if (cnt[i])For(j,1,8)if (i!=j){
			--cnt[i];	++cnt[j];
			ll cost=0;
			For(k,1,8){
				if (cnt[k]%k==0)	cost+=cnt[k];
				else				cost+=(cnt[k]/k+1)*k;
			}
			Min(ans,cost);
			++cnt[i];	--cnt[j];
		}writeln(ans);
	}
}
