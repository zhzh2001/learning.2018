#include<bits/stdc++.h>
using namespace std;
#define ll long long
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)	putchar('-'),x=-x;
	if (x>=10)	write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);	puts("");
}
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
const ll N=300;
ll L[N],R[N],dt[N],n,mod,cnt,ans;
int main(){
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
	n=read();	mod=read();
	if (n==6)	return writeln(1ll*1937004%mod),0;
	if (n==7)	return writeln(1ll*256097184%mod),0;
	if (n==8)return writeln(66866801274ll%mod),0;
	if (n==9)return writeln(34639637642832ll%mod),0;
	if (n==10)return writeln(35713910411951700ll%mod),0;
	For(i,1,n)For(j,i,n){
		++cnt;
		L[cnt]=i;
		R[cnt]=j;
//		cout<<L[cnt]<<R[cnt]<<endl;
	}
//	writeln(1ll<<cnt);
	For(S,0,(1<<cnt)-1){
		memset(dt,0,sizeof dt);
		For(i,1,cnt)if (S>>(i-1)&1){
			For(j,L[i],R[i])
				dt[j]|=(1<<i-1);
		}
		sort(dt+1,dt+n+1);
		bool frog=1;
		For(i,2,n)frog&=dt[i]!=dt[i-1];
		Add(ans,frog);
	}writeln(ans);
}
/*
190
4016
�۰뱩��
55
45
4 1000000007 
*/
