#include<bits/stdc++.h>
using namespace std;
#define ll long long
ll read(){
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)	putchar('-'),x=-x;
	if (x>=10)	write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);	puts("");
}
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define For(i,x,y)	for(int i=x;i<=y;++i)
#define FOr(i,x,y)	for(int i=x;i>=y;--i)
#define rep(i,x,y)	for(int i=x;i<y;++i)
#define Min(x,y)	((x)=((x)<(y)?(x):(y)))
#define Max(x,y)	((x)=((x)<(y)?(y):(x)))
//void Max(unsigned ll &x,unsigned ll y){x=x<y?y:x;}
//void Min(unsigned ll &x,unsigned ll y){x=x<y?x:y;}
const ll N=3000010;
int n,w,W,pos,FBI,A;	bool ok[N];
unsigned ll a[10000010],b[N],c[N],opt,seed,ans,mx,lbc,cur;
unsigned ll rnd(){seed^=seed<<13;seed^=seed>>7;seed^=seed<<17;return seed;}
void baoli(){
	sort(a+1,a+n+1);
	For(i,2,n)Max(ans,a[i]-a[i-1]);
	writeln(ans);
	return;
}
int main(){
//	writeln(((sizeof a)+(sizeof b)+(sizeof c))/1024/1024);
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	cin>>n;
	cin>>seed;cin>>opt;W=(opt==1)?32:64;opt=(opt==1)?((1ll<<32)-1):(-1);
	if(seed==0){
		puts("0");
		return 0;
	}
	lbc=-1;
	For(i,1,n)	a[i]=rnd()&opt,Min(lbc,a[i]);
	if (n<=1000){
		baoli();
		return 0;
	}
	for(w=1;(1<<w)<=n;++w);
	if (n>=2000000)w-=3;
	A=W-w;	ll pos;
	For(i,1,n){
		pos=a[i]>>A;
		if (!ok[pos]){
			b[pos]=c[pos]=a[i];
			ok[pos]=1;
		}
		else{
			Min(b[pos],a[i]);
			Max(c[pos],a[i]);
		}
	}
//	printf("%lf\n",1.0*clock()/CLOCKS_PER_SEC);
	ans=0;	cur=lbc;
	For(i,0,(1<<w)){
		if (ok[i]){
			Max(ans,b[i]-cur);
			cur=c[i];
		}
	}
	cout<<ans<<endl;
//	printf("%lf\n",1.0*clock()/CLOCKS_PER_SEC);
}
