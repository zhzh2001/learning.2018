#include <bits/stdc++.h>

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Node{
	int l,r;
}a[100000];

struct Edge{
	int v,to;
}e[100000];

int n,cnt,wzp,ans,size,head[20];;
bool vis[100000],use[20];
queue<int>que;

inline void add(int u,int v){
	e[++size].v=v;e[size].to=head[u];head[u]=size;
}

inline int check(){
	memset(use,0,sizeof use);
	memset(head,-1,sizeof head);
	size=0;
	add(0,n);
	for(int i=1;i<=cnt;i++){
		if(vis[i]){
			add(a[i].l-1,a[i].r);
			add(a[i].r,a[i].l-1);
		}
	}
	que.push(0);
	while(!que.empty()){
		int u=que.front();
		que.pop();
		for(int i=head[u];i!=-1;i=e[i].to){
			int v=e[i].v;
			if(!use[v]){
				use[v]=true;
				que.push(v);
			}
		}
	}
	for(int i=1;i<=n;i++){
		if(!use[i])return false;
	}
	return true;
}

inline void dfs(int x){
	if(x==cnt+1){
//		for(int i=1;i<=cnt;i++)cout<<vis[i]<<" ";
//		cout<<check()<<endl<<endl;
		ans=(ans+check())%wzp;
		return;
	}
	vis[x]=true;
	dfs(x+1);
	vis[x]=false;
	dfs(x+1);
	return;
}

int main(){
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
	n=read();wzp=read();
	for(int i=1;i<=n;i++){
		for(int j=i;j<=n;j++){
			a[++cnt]=(Node){i,j};
		}
	}
	dfs(1);
	writeln(ans);
	return 0;
}
