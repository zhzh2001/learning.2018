#include <bits/stdc++.h>

#define ll long long
#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

ll t,n,cnt,sum,ans,flag,a[Max],num[Max],now[Max];

int main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	t=read();
	while(t--){
		n=read();
		cnt=0;
		memset(num,0,sizeof num);
		for(ll i=1;i<=n;i++)a[i]=read();
		sort(a+1,a+n+1);
		for(ll i=1;i<=n;i++){
			if(a[i]!=a[i-1]){
				num[++cnt]=1;
			}else{
				num[cnt]++;
			}
		}
		sum=n;
		n=unique(a+1,a+n+1)-a-1;
		flag=0;
		for(ll i=1;i<=n;i++){
			now[i]=(a[i]-num[i]%a[i])%a[i];
			if(now[i])flag++;
			sum+=now[i];
		}
		ans=1e18; 
		for(ll i=1;i<=n;i++){
			if(now[i]){
				if(flag>1){
					ans=min(ans,sum-now[i]+(a[i]-(num[i]-1)%a[i])%a[i]-1);
				}else{
					ans=min(ans,sum-now[i]+(a[i]-(num[i]-1)%a[i])%a[i]);
				}
			}else{
				if(flag){
					ans=min(ans,sum-now[i]+(a[i]-(num[i]-1)%a[i])%a[i]-1);
				}else{
					ans=min(ans,sum-now[i]+(a[i]-(num[i]-1)%a[i])%a[i]);
				}
			}
		}
		writeln(ans);
	}
	return 0;
}
