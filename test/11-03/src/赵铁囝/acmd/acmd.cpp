#include <bits/stdc++.h>

#define Max 10000005
#define ll unsigned long long

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

int n;
ll opt,ans,seed,a[Max];

ll rnd(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}

int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	n=read();
	seed=read();opt=read();
	for(int i=1;i<=n;i++){
		a[i]=rnd();
		if(opt==1){
			a[i]=a[i]&((1ll<<32)-1);
		}
//		cout<<a[i]<<" ";
	}
	sort(a+1,a+n+1);
	for(int i=2;i<=n;i++){
		ans=max(ans,a[i]-a[i-1]);
	}
	writeln(ans);
	return 0;
}
