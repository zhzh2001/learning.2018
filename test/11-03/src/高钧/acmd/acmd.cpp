#include <bits/stdc++.h>
using namespace std;
const int N=10000005;
#define int unsigned long long
inline int read(){int s=0;char c;while(!isdigit(c))c=getchar();while(isdigit(c)){s=s*10+c-'0';c=getchar();}return s;}
#define R(x) x=read()
inline void write(int x){char s[105];int tp=0; while(x){s[++tp]=(x%10)+'0';x/=10;}while(tp){putchar(s[tp]);tp--;}}
inline void writeln(int x){write(x);putchar('\n');}
int n,op,seed,a[N],re=0,B;
int rnd(){seed^=seed<<13; seed^=seed>>7; seed^=seed<<17; return seed;}
signed main()
{
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	int i; R(n);R(seed);R(op); if(op){B=((1LL<<32)-1);for(i=1;i<=n;i++)a[i]=(rnd()&B);}else for(i=1;i<=n;i++)a[i]=rnd();
	sort(a+1,a+n+1); for(i=2;i<=n;i++)re=max(re,a[i]-a[i-1]); writeln(re);
}
