#include <bits/stdc++.h>
using namespace std;
#define int long long
#define M(a,v) memset(a,v,sizeof a)
#define LB lower_bound
inline int read(){int s=0LL; char c; while(!isdigit(c))c=getchar(); while(isdigit(c)){s=s*10+c-'0';c=getchar();}return s;}
#define R(x) x=read()
inline void write(int x){char s[105];int tp=0; while(x){s[++tp]=(x%10)+'0';x/=10;}while(tp){putchar(s[tp]);tp--;}}
inline void writeln(int x){write(x);putchar('\n');}
const int N=100005;
int T,n,m,a[N],has[N],tong[N],ct[N],ma,bo,flg=0;
inline bool jud(){int i; bool boo=0; for(i=1;i<=n;i++){R(a[i]);boo|=(a[i]>100000);}if(boo)return false;return true;}
inline void solve1()
{
	int i,re; flg=bo=ma=re=0; m=0; for(i=1;i<=n;i++){R(a[i]); has[++m]=a[i];flg|=(a[i]==1);}
	sort(has+1,has+m+1); m=unique(has+1,has+m+1)-has-1; M(tong,0);
	for(i=1;i<=n;i++)
	{
		a[i]=LB(has+1,has+m+1,a[i])-has; tong[a[i]]++;
	}for(i=1;i<=m;i++){re+=((tong[i]-1)/has[i]+1)*has[i];ct[i]=tong[i]%has[i];}
	for(i=1;i<=m;i++)
	{
		bo+=(bool)(ct[i]); if(ct[i]==1)ma=max(ma,has[i]);
	}if(!bo)re++;else if(bo==1){if(ma)re-=(ma-1LL);else if(flg)re--;else flg++;}else re-=ma;
	writeln(re);
}
inline void solve2()
{
	int i,re; M(tong,0); flg=bo=ma=re=0; for(i=1;i<=n;i++){R(a[i]);tong[a[i]]++;flg|=(a[i]==1);}
	for(i=1;i<=100000;i++){re+=((tong[i]-1)/i+1)*i;ct[i]=tong[i]%i;}
	for(i=1;i<=100000;i++)
	{
		bo+=(bool)(ct[i]); if(ct[i]==1)ma=max(ma,i);
	}if(!bo)re++;else if(bo==1){if(ma)re-=(ma-1LL);else if(flg)re--;else flg++;}else re-=ma;
	writeln(re);
}
inline void solve3()
{
	while(T--)
	{
		int i,re; R(n); flg=bo=ma=re=0; m=0; for(i=1;i<=n;i++){R(a[i]);has[++m]=a[i];flg|=(a[i]==1);}
		sort(has+1,has+m+1); m=unique(has+1,has+m+1)-has-1; M(tong,0);
		for(i=1;i<=n;i++)
		{
			a[i]=LB(has+1,has+m+1,a[i])-has; tong[a[i]]++;
		}for(i=1;i<=m;i++){re+=((tong[i]-1)/has[i]+1)*has[i];ct[i]=tong[i]%has[i];}
		for(i=1;i<=m;i++)
		{
			bo+=(bool)(ct[i]); if(ct[i]==1)ma=max(ma,has[i]);
		}if(!bo)re++;else if(bo==1){if(ma)re-=(ma-1LL);else if(flg)re--;else flg++;}else re-=ma;
		writeln(re);
	}
}
inline void solve4()
{
	int i,re; flg=bo=ma=re=0; m=0; for(i=1;i<=n;i++){has[++m]=a[i];flg|=(a[i]==1);}
	sort(has+1,has+m+1); m=unique(has+1,has+m+1)-has-1; M(tong,0);
	for(i=1;i<=n;i++)
	{
		a[i]=LB(has+1,has+m+1,a[i])-has; tong[a[i]]++;
	}for(i=1;i<=m;i++){re+=((tong[i]-1)/has[i]+1)*has[i];ct[i]=tong[i]%has[i];}
	for(i=1;i<=m;i++)
	{
		bo+=(bool)(ct[i]); if(ct[i]==1)ma=max(ma,has[i]);
	}if(!bo)re++;else if(bo==1){if(ma)re-=(ma-1LL);else if(flg)re--;else flg++;}else re-=ma;
	writeln(re);
}
signed main()
{
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	R(T); if(T<3){solve3();return 0;}
	while(T--)
	{
		R(n);if(n<=100)solve1();else if(jud())solve2();else solve4();
	}return 0;
}
