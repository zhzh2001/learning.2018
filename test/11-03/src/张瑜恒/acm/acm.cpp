#include<bits/stdc++.h>
#define ll long long
#define int long long
using namespace std;
int T,n,m,a[100010],b[100010],c[100010],tmp;
long long s,sum,ans;
inline ll read(){ll tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
ll sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
void wr(ll x){if(x<0){putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
void wrn(ll x){wr(x);putchar('\n');}
signed main()
  {
   freopen("acm.in","r",stdin);
   freopen("acm.out","w",stdout);
   T=read();
   for (int i=1;i<=T;i++)
     {
      n=read();m=0;ans=1e15;
      for (int j=1;j<=n;j++) {a[j]=read();s=s+a[j];}
      sort(a+1,a+1+n);
      
      m=m+1;b[m]=a[1];c[m]=1;
      for (int j=2;j<=n;j++)
		if (a[j]!=a[j-1]) 
          {m=m+1;b[m]=a[j];c[m]=1;}
          else c[m]=c[m]+1;
          
      bool t=false;
      for (int j=1;j<=m-1;j++)
        {
         if (b[j+1]-b[j]!=1) {tmp=b[j]+1;t=true;break;}
		}
	  tmp=b[m]+1;
	  
      sum=0;
      for (int j=1;j<=m;j++) sum=sum+c[j]/b[j]*b[j]+c[j]%b[j]*b[j];
      for (int j=1;j<=m;j++)
        {
         for (int k=1;k<=m;k++)
           if (j!=k)
             {
              s=sum;
              s=s-c[j]/b[j]*b[j]-c[j]%b[j]*b[j];
              s=s+(c[j]-1)/b[j]*b[j]+(c[j]-1)%b[j]*b[j];
              s=s-c[k]/b[k]*b[k]-c[k]%b[k]*b[k];
              s=s+(c[k]+1)/b[k]*b[k]+(c[k]+1)%b[k]*b[k];
              if (s<ans) ans=s;
			 }
		 if (b[1]-1!=0)
		   {
		   	s=sum;
		   	s=s-c[j]/b[j]*b[j]-c[j]%b[j]*b[j];
            s=s+(c[j]-1)/b[j]*b[j]+(c[j]-1)%b[j]*b[j];
            s=s+b[1]-1;
            if (s<ans) ans=s;
		   }
		 if (b[1]>=2)
		   {
		    s=sum;
		   	s=s-c[j]/b[j]*b[j]-c[j]%b[j]*b[j];
            s=s+(c[j]-1)/b[j]*b[j]+(c[j]-1)%b[j]*b[j];
            s=s+1;
            if (s<ans) ans=s;
		   }
		 s=sum;
		 s=s-c[j]/b[j]*b[j]-c[j]%b[j]*b[j];
         s=s+(c[j]-1)/b[j]*b[j]+(c[j]-1)%b[j]*b[j];
         s=s+tmp;
         if (s<ans) ans=s;
	    }
	  wrn(ans);
     }
   return 0;
  }
