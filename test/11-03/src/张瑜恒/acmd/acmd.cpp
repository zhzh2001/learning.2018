#include<bits/stdc++.h>
#define ull unsigned long long
using namespace std;
int n,opt;
unsigned long long a[10000010],seed,ans,s;
inline ull read(){ull tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
ull sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
void wr(ull x){if(x<0){putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
void wrn(ull x){wr(x);putchar('\n');}
int main()
  {
   freopen("acmd.in","r",stdin);
   freopen("acmd.out","w",stdout);
   
   n=read();seed=read();opt=read();
   for (int i=1;i<=n;i++)
     {
      seed^=seed<<13;seed^=seed>>7;seed^=seed<<17;
      if (opt==0) a[i]=seed;
      if (opt==1) a[i]=seed&((1LL<<32)-1);
      //wrn(a[i]);
	 }
   sort(a+1,a+1+n);
   ans=0;
   for (int i=2;i<=n;i++)
     {
      s=a[i]-a[i-1];
      if (s>ans) ans=s;
	 }
   wrn(ans);
   return 0;
  }
