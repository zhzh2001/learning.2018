#include <bits/stdc++.h>
#define int unsigned long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e7+10;
int n,seed,op,a[N],b[N];
signed c[100010];
int *A=a,*B=b;
inline int rnd(){
	seed^=seed<<13;seed^=seed>>7;
	seed^=seed<<17;return seed;
}
inline void Sort(){
	int bin=65535;signed pt=64;
	if(op==1)pt=32;
	for(register signed d=0;d<pt;d+=16){
		for(register signed i=0;i<=bin;i++)c[i]=0;
		for(register signed i=1;i<=n;i++)c[(A[i]>>d)&bin]++;
		for(register signed i=1;i<=bin;i++)c[i]+=c[i-1];
		for(register signed i=n;i;i--)B[c[(A[i]>>d)&bin]--]=A[i];
		swap(A,B);
	}
}
signed main()
{
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	n=read();seed=read();op=read();
	for(register signed i=1;i<=n;i++)a[i]=rnd();
	Sort();int ans=0;
	for(register signed i=1;i<n;i++)ans=max(ans,A[i+1]-A[i]);
	writeln(ans);
	return 0;
}
