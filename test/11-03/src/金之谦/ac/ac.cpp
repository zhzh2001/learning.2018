#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=30;
int n,MOD,cnt=0,ans=0,b[N],c[N];
struct ppap{int l,r;}a[N];
inline int check(){
	int ans=0;
	for(int i=1;i<=n;i++){
		memset(c,0,sizeof c);
		for(int j=1;j<=cnt;j++)if(b[j]){
			if(a[j].l<=i&&a[j].r>=i){
				for(int k=1;k<a[j].l;k++)c[k]=1;
				for(int k=a[j].r+1;k<=n;k++)c[k]=1;
			}else for(int k=a[j].l;k<=a[j].r;k++)c[k]=1;
		}
		int s=0;
		for(int j=1;j<=n;j++)if(!c[j])s++;
		if(s==1)ans++;
	}
	if(ans==n)return 1;return 0;
}
inline void dfs(int x){
	if(x==cnt+1){
		if(check())ans=(ans+1)%MOD;
		return;
	}
	b[x]=1;dfs(x+1);
	b[x]=0;dfs(x+1);
}
int main()
{
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
	n=read();MOD=read();
	for(int i=1;i<=n;i++)
		for(int j=i;j<=n;j++)a[++cnt]=(ppap){i,j};
	dfs(1);writeln(ans);
	return 0;
}
