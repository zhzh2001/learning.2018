#include <bits/stdc++.h>
#define int long long
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int n,a[N];
signed main()
{
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	for(int T=read();T;T--){
		n=read();
		for(int i=1;i<=n;i++)a[i]=read();
		sort(a+1,a+n+1);
		int la=0,ma=0,ans=0,t=0,pt=0;
		for(int i=1;i<=n+1;i++)if(a[i]!=a[i-1]){
			if(la){
				int p=(i-la)%a[i-1];
				if(p){if(t)t=2;else t=1,pt=a[i-1];}
				if(p==1)ma=max(ma,a[i-1]);
				ans+=i-la+(p?a[i-1]-p:0);
			}la=i;
		}
		ans=ans-ma+1;
		if(t==2||t==1&&pt!=ma)ans--;
		writeln(ans);
	}
	return 0;
}
