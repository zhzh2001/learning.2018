#include<iostream>
#include<fstream>
#include<algorithm>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<map>
using namespace std;
ifstream fin("acm.in");
ofstream fout("acm.out");
inline long long read(){
	long long x=0;char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=(x<<3)+(x<<1)+(ch-48),ch=getchar();
	return x;
}
inline void write(long long x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
long long num[200005],sz[200005],nxt[200005],ans,n,T;
int s,t;
int main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	T=read();
	while(T--){
		n=read();
		for(register int i=1;i<=n;++i) num[i]=read();
		sort(num+1,num+n+1);
		for(register int i=n;i;--i) nxt[i]=(num[i]==num[i+1])?nxt[i+1]:i+1;
		for(register int i=1;i<=n;++i)
			if(num[i]!=num[i-1])
				sz[i]=nxt[i]-i;
		s=1,t;
		for(register int i=1;i<=n;i=nxt[i])
			if(num[i]^num[i-1])
				if(sz[i]%num[i]==1)s=i;
				else if((nxt[i]-i)%num[i])t=i;
		--sz[s],++sz[t];
		ans=(t==0)?1:0;
		for(register int i=1;i<=n;i=nxt[i]) ans+=(sz[i]+num[i]-1)/num[i]*num[i];
		write(ans),puts("");
	}
	return 0;
}
