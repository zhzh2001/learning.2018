#include<iostream>
#include<fstream>
#include<algorithm>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<map>
using namespace std;
ifstream fin("acmd.in");
ofstream fout("acmd.out");
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline unsigned long long read(){
	unsigned long long x=0;char ch=gc();
	for(;!isdigit(ch);ch=gc());
	for(;isdigit(ch);ch=gc())x=(x<<3)+(x<<1)+ch-'0';
	return x;
}
const int inf=~0U>>2;
using namespace std;
unsigned long long S0[10000003],S1[10000003];
unsigned long long n,C[1000]={},seed,ans,wtf,opt;
unsigned long long *A=S0,*B=S1;
int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	n=read(),seed=read(),opt=read();
	for(register int i=0;i<n;++i){
		seed^=seed<<13,seed^=seed>>7,seed^=seed<<17;
		A[i]=seed;
	}
	if(opt){
		for(register int i=0;i<n;i++)A[i]=A[i]&((1LL<<32)-1);
	}
	for(register int x=0;x<8;++x){
		int d=x<<3;
		for(register int i=0;i<256;++i) C[i]=0;
		for(register int i=0;i<n;++i) ++C[(A[i]>>d)&255];
		for(register int i=0;i<256;++i) if(i) C[i]=C[i]+C[i-1];
		for(register int i=n-1;i>=0;--i) B[--C[(A[i]>>d)&255]]=A[i];
		swap(A,B);
	}
	for(register int i=1;i<n;++i){
		wtf=A[i]-A[i-1];
		if(wtf>ans)ans=wtf;
	}
	fout<<ans<<endl;
	return 0;
}
