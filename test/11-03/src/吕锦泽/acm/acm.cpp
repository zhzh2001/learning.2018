#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return x*f;
}
struct data{ ll x,y; }b[N];
ll n,ans,a[N],tot,cnt,pos; 
int main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	ll T=read();
	while (T--){
		n=read(); ans=cnt=pos=0;
		rep(i,1,n) a[i]=read();
		sort(a+1,a+1+n);
		for (ll l=1,r=1;l<=n;l=++r){
			while (a[r+1]==a[l]&&r<n) ++r;
			ll len=r-l+1; ans+=len-len%a[l];
			if (len%a[l]) ++cnt,ans+=a[l];
			if (len%a[l]==1) pos=l;
		}
		if (cnt<=1) ++ans;
		else if (pos) ans-=a[pos];
		printf("%lld\n",ans);
	}
}

/*

2
4
2 2 2 2
5
2 2 2 3 3

*/
