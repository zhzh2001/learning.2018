#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
#define rg register
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ull &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ull x){
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e7+10;
ull seed,a[N],n,opt,ans;
ull b[N];
inline ull rnd(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
ull t1[1<<8],t2[1<<8],t3[1<<8],t4[1<<8],k=(1<<8)-1;
ull t5[1<<8],t6[1<<8],t7[1<<8],t8[1<<8];
inline void _sort_(ull *a){
	for (rg ull i=1;i<=n;++i){
		++t1[(a[i]>> 0)&k];
		++t2[(a[i]>> 8)&k];
		++t3[(a[i]>>16)&k];
		++t4[(a[i]>>24)&k];
		++t5[(a[i]>>32)&k];
		++t6[(a[i]>>40)&k];
		++t7[(a[i]>>48)&k];
		++t8[(a[i]>>56)&k];
	}
	for (rg ull i=1;i<=k;++i){
		t1[i]+=t1[i-1];
		t2[i]+=t2[i-1];
		t3[i]+=t3[i-1];
		t4[i]+=t4[i-1];
		t5[i]+=t5[i-1];
		t6[i]+=t6[i-1];
		t7[i]+=t7[i-1];
		t8[i]+=t8[i-1];
	}
/*	for (rg ull i=n;i;--i) b[t1[(a[i]>> 0)&k]--]=a[i];
	for (rg ull i=n;i;--i) a[t2[(b[i]>>16)&k]--]=b[i];
	for (rg ull i=n;i;--i) b[t3[(a[i]>>32)&k]--]=a[i];
	for (rg ull i=n;i;--i) a[t4[(b[i]>>48)&k]--]=b[i];*/
	for (rg ull i=n;i;--i) b[t1[(a[i]>> 0)&k]--]=a[i];
	for (rg ull i=n;i;--i) a[t2[(b[i]>> 8)&k]--]=b[i];
	for (rg ull i=n;i;--i) b[t3[(a[i]>>16)&k]--]=a[i];
	for (rg ull i=n;i;--i) a[t4[(b[i]>>24)&k]--]=b[i];
	if (opt) return;
	for (rg ull i=n;i;--i) b[t5[(a[i]>>32)&k]--]=a[i];
	for (rg ull i=n;i;--i) a[t6[(b[i]>>40)&k]--]=b[i];
	for (rg ull i=n;i;--i) b[t7[(a[i]>>48)&k]--]=a[i];
	for (rg ull i=n;i;--i) a[t8[(b[i]>>56)&k]--]=b[i];
}
int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	read(n);
	read(seed),read(opt);
	for (rg ull i=1;i<=n;++i) a[i]=rnd();
	if (opt==1) for (rg ull i=1;i<=n;++i) a[i]&=((1LL<<32)-1);
	_sort_(a);
	for (rg ull i=n;i>1;--i) ans=max(ans,a[i]-a[i-1]);
	wr(ans);
	return 0;
}
//sxdakking
/*
4
213541521 53 62430862436 32479
*/
