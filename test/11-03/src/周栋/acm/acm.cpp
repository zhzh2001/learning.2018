#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10;
ll T,a[N],t[N],v[N],n,tot,ans;
int main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	for (read(T);T;--T){
		read(n);
		for (ll i=1;i<=n;++i) read(a[i]);
		sort(a+1,a+1+n);
		for (ll i=1;i<=n;++i) t[i]=a[i];
		tot=unique(t+1,t+1+n)-t-1;
		v[1]=0;
		for (ll i=1,j=1;i<=n;++i){
			if (a[i]!=t[j]) v[++j]=0;
			++v[j];
		}
		ll k=0;
		for (ll i=tot;i&&!k;--i) if (v[i]%t[i]==1) k=i;
		if (!k) k=1;
		--v[k];
		bool flag=1;
		for (ll i=1;i<=tot&&flag;++i) if ((i^k)&&(v[i]%t[i]!=0)) flag=0,++v[i];
		ans=0;
		if (flag){
			if (t[k]!=1) ans=1;
			else{
				if (t[1]==2) ++t[1];
				else if (t[2]==2) ++t[2];
				else ans+=2;
			}
		}
		for (ll i=1;i<=tot;++i) ans+=v[i]%t[i]?(v[i]/t[i]+1)*t[i]:v[i];
		wr(ans),puts("");
	}
	return 0;
}
//sxdakking
/*
2
4
2 2 2 2
5
2 2 2 3 3
*/
