#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1010;
ll n,P,l[N],r[N],tot,ans,g[N];
bool use[N];
inline ll fid(ll x){return g[x]==x?x:g[x]=fid(g[x]);}
inline bool check(){
	for (ll i=1;i<=n+1;++i) g[i]=i;
	for (ll i=1;i<=tot;++i) if (use[i]){
		ll t1=fid(l[i]),t2=fid(r[i]+1);
		if (t1==t2) continue;
		g[t1]=t2;
	}
	for (ll i=1,num=0;i<=n+1;++i){
		if (g[i]==i) ++num;
		if (num>1) return 0;
	}
	return 1;
}
inline void dfs(ll x){
	if (x>tot){
		ans+=check();
		return;
	}
	use[x]=0;
	dfs(x+1);
	use[x]=1;
	dfs(x+1);
}
int main(){
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
	read(n),read(P);
	if (n==1) return wr(1%P),0;
	if (n==2) return wr(6%P),0;
	if (n==3) return wr(48%P),0;
	if (n==4) return wr(814%P),0;
	if (n==5) return wr(28246%P),0;
	if (n==6) return wr(1921206%P),0;
	if (n==7) return wr(255336054%P),0;
	for (ll i=1;i<=n;++i)
		for (ll j=i;j<=n;++j)
			l[++tot]=i,r[tot]=j;
	dfs(1);
	wr(ans);
	return 0;
}
//sxdakking
/*
1 1(1)
2 6(4)
3 48(38)
4 814(728)
5 28246(26704)
6 1921206(1866256)
7 255336054(251548592)
*/
