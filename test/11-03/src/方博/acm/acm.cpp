#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=100005;
map<int,int>h;
ll a[N];
int sz[N];
ll ans,n;
int nxt[N];
int main()
{
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	int T=read();
	while(T--){
		n=read();
		for(register int i=1;i<=n;++i)
			a[i]=read(),nxt[i]=i+1;
		sort(a+1,a+n+1);
		for(register int i=n;i;--i)
			if(a[i]==a[i+1])nxt[i]=nxt[i+1];
		for(register int i=1;i<=n;++i)
			if(a[i]!=a[i-1])
				sz[i]=nxt[i]-i;
		int k=1,t;
		for(register int i=1;i<=n;i=nxt[i])
			if(a[i]!=a[i-1])
				if(sz[i]%a[i]==1)k=i;
				else if((sz[i])%a[i]!=0)t=i;
		if(t==0)ans=1;
		else ans=0;
		sz[k]--,sz[t]++;
		for(register int i=1;i<=n;i=nxt[i])
			ans+=(sz[i]+a[i]-1)/a[i]*a[i];
		writeln(ans);
	}
}
