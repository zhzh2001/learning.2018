#include <bits/stdc++.h>
using namespace std;
#define rep(i,n) for(register int i=0;i<n;++i)
const int inf = ~0U >> 2, maxn = 10000010;
#define ull unsigned long long
ull S0[maxn], S1[maxn], n, C[256] = {};
ull *A = S0, *B = S1;
ull opt,seed;
ull ans;
ull rnd()
{
		seed^=seed<<13;
		seed^=seed>>7;
		seed^=seed<<17;
		return seed;
}
int main()
{
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	cin>>n>>seed>>opt;
	rep(i,n)
		if(opt==0)A[i]=rnd();
		else A[i]=rnd()&((1LL<<32)-1);
	rep(x, 8){
		int d = x * 8;
		rep(i, 256)C[i] = 0;
		rep(i, n)C[(A[i] >> d) & 255]++;
		rep(i, 256)if(i)C[i] += C[i-1];
		for(int i = n - 1; i >= 0; i--)B[--C[(A[i] >> d) & 255]] = A[i];
		swap(A, B);
	}
	rep(i,n)
		ans=max(ans,A[i+1]-A[i]);
	cout<<ans<<endl;
	return 0;
}
