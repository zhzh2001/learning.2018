//开了O2快排就能过了......
//#pragma GCC optimize(2) 
#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
typedef unsigned long long ull;
const int MAXN=1e7+5;
int n,opt;
ull seed;
inline ull Rand(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
ull a[MAXN];
int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	cin>>n>>seed>>opt;
	for(register int i=1;i<=n;i++){
		a[i]=Rand();
		if(opt)a[i]&=(1LL<<32)-1;
	}
	sort(a+1,a+n+1);
	register ull ans=0;
	for(register int i=2;i<=n;i++)ans=max(ans,a[i]-a[i-1]);
	cout<<ans<<endl;
}
