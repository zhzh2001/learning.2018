#include<vector>
#include<cstdio>
#include<iostream>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
struct Seg{
	Seg(){}
	Seg(int L,int R){
		l=L,r=R;
	}
	int l,r;
};
vector<Seg>v;
int n,P,ans,a[20][20];
inline bool check(int x,int* a){
	int id=1;
	for(int i=2;i<=n;i++)
		if(a[i]>a[id])id=i;
	if(a[id]!=a[x])return 0;
	int cnt=0;
	for(int i=1;i<=n;i++)
		if(a[i]==a[id])cnt++;
	return cnt==1;
}
void dfs(int k){
	if(k>=(int)v.size()){
		for(int i=1;i<=n;i++)
			if(!check(i,a[i]))return;
		ans=(ans+1)%P;
		return;
	}
	dfs(k+1);
	int l=v[k].l,r=v[k].r;
	for(int x=1;x<=n;x++)
		if(r<x||l>x)for(int i=l;i<=r;i++)a[x][i]--;
		else for(int i=l;i<=r;i++)a[x][i]++;
	dfs(k+1);
	for(int x=1;x<=n;x++)
		if(r<x||l>x)for(int i=l;i<=r;i++)a[x][i]++;
		else for(int i=l;i<=r;i++)a[x][i]--;
}
int main(){
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
	n=read(),P=read();
	for(int i=1;i<=n;i++)
		for(int j=i;j<=n;j++)
			v.push_back(Seg(i,j));
	dfs(0);
	printf("%d",ans);
}
