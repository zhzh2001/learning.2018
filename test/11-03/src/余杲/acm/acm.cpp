#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
inline char _gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
#define gc c=_gc()
inline int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=1e5+5;
int n,a[MAXN];
int main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	a[0]=0x3fffffff;
	for(register int T=read();T;T--){
		n=read();
		for(register int i=1;i<=n;i++)a[i]=read();
		sort(a+1,a+n+1);
		register long long ans=0;
		register bool f1=1,f2=0,f3=0;
		//f1表示当前是否有数据假掉 
		//f2表示是否有数能让当前这个数补到那里去 
		//f3表示是否有数需要补 
		for(register int i=n;i>=1;i--){
			register int cnt=1;
			while(a[i-1]==a[i])cnt++,i--;
			ans+=1LL*(cnt/a[i])*a[i];
			cnt%=a[i];
			if(cnt){
				if(f1&&cnt==1&&!f3)f3=1;
				else{
					ans+=a[i];
					f2=f1;
				}
			}
			if(f2&&f3)f1=f2=f3=0;
		}
		if(f1)ans++;//如果还有一个数据假掉 
		//强行把那个数变成一个 
		printf("%lld\n",ans);
	}
}
