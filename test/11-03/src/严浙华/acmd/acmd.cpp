#include<cstdio>
#include<algorithm>
#define ull unsigned long long
#define For(i,j,k) for(ull i = (int)(j);i <= (int)(k);i++)
#define Rep(i,j,k) for(int i = (int)(j);i >= (int)(k);i--)
#define gc getchar()
#define pc putchar
using namespace std;
void read(ull &x){
	ull f = 1;
	x = 0;
	char s = gc;
	while(s < '0' || s > '9'){if(s == '-') f = -1;s = gc;}
	while(s >= '0' && s <= '9'){x = x * 10 + s - '0';s = gc;}
}
void write(ull x){
	if(x < 0){pc('-');x = -x;}
	if(x > 9) write(x / 10);
	pc(x % 10 + '0');
}
#undef gc
#undef pc
ull seed,n,opt;
ull a[10000000] = {0,34,82,9,15,99,53,21,77,1,57},b[9999999];
ull rnd(){
seed^=seed<<13;
seed^=seed>>7;
seed^=seed<<17;
return seed;
}
int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	read(n);
	ull x;
	read(seed),read(opt);
	For(i,1,n)
		a[i] = rnd();
	if(opt)
		For(i,1,n)
			a[i]=a[i]&((1LL<<32)-1);
	sort(a + 1,a + 1 + n);
	For(i,1,n - 1){
		x = a[i + 1] - a[i];
		b[i] = x > 0 ? x : -x;
	}
	sort(b + 1,b + n);
	write(b[n - 1]);
	return 0;
}
