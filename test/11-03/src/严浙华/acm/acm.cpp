#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#define maxn 100000
#define maxx 100000
#define ull unsigned long long
#define For(i,j,k) for(ull i = (int)(j);i <= (int)(k);i++)
#define gc getchar()
#define pc putchar
using namespace std;
void read1(int &x){
	ull f = 1;
	x = 0;
	char s = gc;
	while(s < '0' || s > '9'){if(s == '-') f = -1;s = gc;}
	while(s >= '0' && s <= '9'){x = x * 10 + s - '0';s = gc;}
}
void read2(long long &x){
	ull f = 1;
	x = 0;
	char s = gc;
	while(s < '0' || s > '9'){if(s == '-') f = -1;s = gc;}
	while(s >= '0' && s <= '9'){x = x * 10 + s - '0';s = gc;}
}
void write(ull x){
	if(x < 0){pc('-');x = -x;}
	if(x > 9) write(x / 10);
	pc(x % 10 + '0');
}
#undef gc
#undef pc
int T,n;
long long s[maxn],maax;
long long a[maxx];
bool vis[maxn];
int main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	read1(T);
	For(i,1,T){
		read1(n);
		For(i,1,n){
			read2(s[i]);
			maax = max(maax,s[i]);
		}
		memset(a,0,sizeof(a));
		memset(vis,true,sizeof(vis));
		ull res = 0;
		For(i,1,n){
			ull ans = 0;
			For(j,1,i - 1){
				if(vis[s[j]]){
					a[s[j]]--;
					ans++;
					if(a[s[j]] == 0)
						vis[s[j]] = false;
				}
				else{
					vis[s[j]] = true;
					a[s[j]] = s[j] - 1;
					ans++;
				}
			}
			For(j,i + 1,n){
				if(vis[s[j]]){
					a[s[j]]--;
					ans++;
					if(a[s[j]] == 0)
						vis[s[j]] = false;
				}
				else{
					vis[s[j]] = true;
					a[s[j]] = s[j] - 1;
					ans++;
				}
			}
			For(k,1,maax){
				if(a[k] && k != s[i] && s[i]){
					while(a[k] && s[i]){
						s[i]--;
						a[k]--;
						ans++;
					}
					if(a[k])
						ans += a[k];
				}
				else if(a[k])
					ans += a[k];
			}
			if(s[i])
				ans += s[i];
			res = max(res,ans);
		}
		write(res);
		printf("\n");
	}
	return 0;
}
