#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
    int x = 0,f = 1;char ch = getchar();
    while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
    while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
    return x*f;
}

const int N = 305;
int f[N];
int dp[N][N];
int Dp[N][N];
int mod;
int n;

signed main(){
	n = read(); mod = read();
	f[1] = 1;
	for(int i = 2;i <= n;++i){
		memset(Dp,0,sizeof Dp);
		for(int j = 1;j < i;++j) Dp[1][j] = f[j];
		for(int j = 2;j <= i;++j){
			for(int k = j-1;k <= i;++k)
				for(int g = 1;g <= i-k;++g){
					(Dp[j][k+g] += 1LL*Dp[j-1][k]*f[g]% mod) %mod;
				}
			f[i] = (f[i]+1LL*Dp[j][i]*(j+1)%mod)%mod;
		}
	}
	for(int i = 1;i <= n;++i)
		printf("%d\n",f[i]*2);
	return 0;
}
