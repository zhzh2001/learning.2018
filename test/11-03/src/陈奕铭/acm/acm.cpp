#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
    int x = 0,f = 1;char ch = getchar();
    while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
    while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
    return x*f;
}

const int N = 100005;
int a[N];
int d[N],sum[N],cnt;
int n;

signed main(){
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
    int T = read();
    while(T--){
        n = read();
        for(int i = 1;i <= n;++i){
            a[i] = read();
        }
        sort(a+1,a+n+1); cnt = 0;
        for(int i = 1;i <= n;++i)
            if(a[i] != a[i-1]){
                d[++cnt] = a[i];
                sum[cnt] = 1;
            }else{
                ++sum[cnt];
            }
        bool flag = 0,has1 = 0,only1 = 0;
		ll Mxonly1 = 0;
		int weiman = 0,man = 0;
		ll ans = 0;
        for(int i = 1;i <= cnt;++i){
            if(d[i] == 1){
            	has1 = true;
            	ans += sum[i];
            	continue;
            }else{
            	ll p = sum[i]/d[i];
            	ll q = sum[i]%d[i];
            	if(q == 0){
            		++man;
            		ans += sum[i];
				}else{
					if(q == 1){
						only1 = true;
						Mxonly1 = d[i];
					}
					++weiman;
					ans += (p+1)*d[i];
				}
			}
        }
        if(only1){
        	if(weiman > 1){
        		ans -= Mxonly1;
			}else{
				ans -= Mxonly1-1;
			}
		}else if (weiman >= 2) ans += 0;
		else if(weiman == 1){
			if(man > 0) ans += 0;
			else ans += 1;
		}
		else ans += 1;
		printf("%lld\n",ans);
    }
    return 0;
}
