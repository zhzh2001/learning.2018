#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
inline int read(){
    int x = 0,f = 1;char ch = getchar();
    while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
    while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
    return x*f;
}
inline ull readull(){
    ull x = 0,f = 1;char ch = getchar();
    while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
    while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
    return x*f;
}

const int N = 10000005;
ull seed,opt;
inline ull rnd(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}

ull a[2][N];
int sum[1024];
int n;

signed main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
    n = read();
    seed = readull(); opt = read();
	bool p1 = 0,p2 = 1;
    for(register int i = 1;i <= n;++i){
    	a[0][i] = rnd();
	}
	if(opt == 1){
		ull Bin = 1023;
		for(int j = 0;j < 32;j += 10){
			for(int i = 0;i < 1024;++i) sum[i] = 0;
			for(register int i = n;i;--i)
				++sum[((a[p1][i])>>j)&Bin];
			for(int i = 1;i < 1024;++i) sum[i] += sum[i-1];
			for(register int i = n;i;--i)
				a[p2][sum[(a[p1][i]>>j)&Bin]--] = a[p1][i];
			p1 ^= 1;
			p2 ^= 1;
		}
	}else{
		ull Bin = 1023;
		for(int j = 0;j < 64;j += 10){
			for(int i = 0;i < 1024;++i) sum[i] = 0;
			for(register int i = n;i;--i)
				++sum[((a[p1][i])>>j)&Bin];
			for(int i = 1;i < 1024;++i) sum[i] += sum[i-1];
			for(register int i = n;i;--i)
				a[p2][sum[(a[p1][i]>>j)&Bin]--] = a[p1][i];
			p1 ^= 1;
			p2 ^= 1;
		}
	}
	ull ans = 0;
	for(register int i = 1;i < n;++i){
		ull x = a[p1][i]-a[p1][i-1];	
		if(x > ans)
			ans = x;
	}
//	for(int i = 0;i < n;++i) printf("%llu ",a[p1][i]); puts("");
	printf("%llu\n",ans);
    return 0;
} 
