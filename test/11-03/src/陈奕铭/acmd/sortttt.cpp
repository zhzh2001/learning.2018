#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
inline int read(){
    int x = 0,f = 1;char ch = getchar();
    while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
    while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
    return x*f;
}
inline ull readull(){
    ull x = 0,f = 1;char ch = getchar();
    while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
    while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
    return x*f;
}

const int N = 10000005;
ull seed,opt;
inline ull rnd(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}

ull a[N];
int n;

signed main(){
	freopen("acmd.in","r",stdin);
	freopen("sort1.out","w",stdout);
    n = read();
    seed = readull(); opt = read();
    ull y = 1LL<<62;
    y <<= 1;
    int pos1 = -1,pos2 = n;
	for(register int i = 0;i < n;++i){
    	ull x = rnd();
    	if(x&y) a[--pos2] = x;
    	else a[++pos1] = x;
	}  
	sort(a,a+pos1+1);
	sort(a+pos1+1,a+n);
    ull ans = 0;
    for(register int i = 1;i < n;++i)
    	if(a[i]-a[i-1] > ans) ans = a[i]-a[i-1];
	printf("%llu\n",ans);
	printf("%d\n",clock());
    return 0;
} 
