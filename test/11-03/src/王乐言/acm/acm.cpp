#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define ll long long
#define file "acm"
using namespace std;
const int M=1e5+1000;
ll read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
ll n,a[M],tmp[M],cnt=0;
ll bm[M];
ll ans=0;
bool cmp(ll a,ll b){return a<b;}
ll fd(ll x){
	ll l=1,r=cnt,mid;
	while(l<=r){
		mid=(l+r)>>1;
		if(tmp[mid]==x)return mid;
		if(tmp[mid]>x)r=mid-1;
		else l=mid+1;
	}
	return -1;
}
void work(){
	n=read();cnt=0;ans=0;
	memset(bm,0,sizeof(bm));
	memset(a,0,sizeof(a));
	memset(tmp,0,sizeof(tmp));
	for(ll i=1;i<=n;i++)a[i]=read();
	sort(a+1,a+1+n,cmp);
	for(ll i=1;i<=n;i++)tmp[++cnt]=a[i];
	cnt=unique(tmp+1,tmp+1+cnt)-(tmp+1);
	for(ll i=1;i<=n;i++)bm[(a[i]=fd(a[i]))]++;
	ll tot=0,maxn=-1;
	for(ll i=1;i<=cnt;i++){
		if(bm[i]%tmp[i]){
			//如果有多余 
			tot++;
			//统计多余的种数 
			ans+=(bm[i]/tmp[i]+1)*tmp[i];
			//累加答案 
			if(bm[i]%tmp[i]==1)
				maxn=max(maxn,i);
			//如果只多余一个，记录最大的大小 
		}else {
			ans+=bm[i];
			//如果刚好配对，我们累加答案。 
		}
	}
	//cout<<tmp[1]<<endl;
	if(tot==0){
		//如果所有都刚好配对
		printf("%lld\n",ans+1);
		return ; 
	}else {
		if(tot==1){
			//如果只多余一组 
			if(maxn!=-1)
				//如果多余一组只多余一个,减去所在组的值 
				ans-=tmp[maxn];
			printf("%lld\n",ans+1);
			//变为1 
			return ; 
		}else{
			if(maxn!=-1)
				//如果多余一组只多余一个,减去所在组的值 
				ans-=tmp[maxn];
			printf("%lld\n",ans);
			return ;
		}
	}
}
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int Case=read();
	while(Case--)work();
	return 0;
}
/* 如果发现某个值出现的次数小于它的值
 * 那么我们可以把它缩成一个球。
 * 我们必须把其中一个数据改变。
 * 求最小数据和 
 * 如果刚好全部配对，我们考虑把其中一个变为1
 * 剩下的肯定是很多不足以刚好配对的
 * 如果只有一个不足以刚好配对。
 * 分类讨论：只多出一个，答案减去值然后+1
 * 				多出多个，答案+1. 
 * 如果超过一个不足以配对。
 * 仍然是讨论，如果有一堆只多出一个，我们答案直接减去那个值（变成另一堆） 
 * 如果两堆都多出很多个，答案不变。
 * 装桶，排序+离散化 
 * 考虑排序+离散化  
 */ 

