#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define ull unsigned long long
#define ll long long
#define file "acmd"
using namespace std;
ull read(){
	char c;ull num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n;
ull seed,opt,a[10000009],maxn=0;
ull rnd(){
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
void init(){
	seed=read();opt=read();
	for(int i=1;i<=n;i++){
		a[i]=rnd();
		if(opt)a[i]=a[i]&((1ll<<32)-1);
	}
}
void init1(){
	for(int i=1;i<=n;i++)
		cin>>a[i];
}
bool cmp(ull a,ull b){return a<b;}
void work1(){
	sort(a+1,a+1+n,cmp);
	for(int i=2;i<=n;i++)
		maxn=max(maxn,(a[i]>a[i-1])?(a[i]-a[i-1]):(a[i-1]-a[i]));
	cout<<maxn<<endl;
}
void sort1(ull l,ull r){
	if(l>=r)return ;
	if(r-l+1==2){
		ull t;
		if(a[l]>a[r]){
			t=a[l];a[l]=a[r];a[r]=t;
		}
		if(a[r]-a[l]>maxn)maxn=a[r]-a[l];
		return ;
	}
	ull key=a[l],lmin=0,lmax=0,rmin=0,rmax=0;lmin-=1;rmin-=1;
	ull i=l,j=r;
	while(i!=j){
		while(a[j]>=key&&j>i){
			if(a[j]>rmax)rmax=a[j];
			if(a[j]<rmin)rmin=a[j];
			j--;
		}
		a[i]=a[j];
		while(a[i]<=key&&j>i){
			if(a[i]>lmax)lmax=a[i];
			if(a[i]<lmin)lmin=a[i];
			i++;
		}
		a[j]=a[i];
	}
	a[j]=key;	
	if(l!=j&&key-lmax>maxn)maxn=key-lmax;
	if(r!=j&&rmin-key>maxn)maxn=rmin-key;
	/*printf("l:%lld r:%lld\n",l,r);
	for(int i=1;i<=n;i++){
		cout<<a[i]<<"  ";
	}
	cout<<endl;
	cout<<lmin<<"  "<<key<<"  "<<rmax<<"  "<<maxn<<endl<<endl;*/
	if(rmax-rmin>maxn&&r>=j)sort1(j+1,r);
	if(lmax-lmin>maxn&&l<=j)sort1(l,j-1);
}
void work2(){
	sort1(1,n);
	/*for(int i=1;i<=n;i++){
		cout<<a[i]<<"  ";
	}*/
	cout<<maxn<<endl;
}
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	scanf("%d",&n);
	init();
	//init1();
	if(n<=500000)work1();
	else work2();
	return 0;
}
/* 乱搞吧 
 * 我们在快排途中如果区间长度为2，我们统计最大值，如果发现当前区间的长度小于最大值，直接跳过。
 */ 
