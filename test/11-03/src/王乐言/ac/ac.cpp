#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define ll long long
#define file "ac"
using namespace std;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,mod;
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=read();mod=read();
	if(n==1)printf("%d\n",1%mod);
	else if(n==2)printf("%d\n",5%mod);
	else if(n==3)printf("%d\n",48%mod);
	else cout<<"Give up"<<endl;
	return 0;
}

