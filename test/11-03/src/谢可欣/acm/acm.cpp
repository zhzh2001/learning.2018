#include <iostream>
#include <cstring>
#include <cstdio>
#include <map>
#define int long long
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
void wr(int x){if(x<0){putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
void wrn(int x){wr(x);putchar('\n');}
const int N=100005;
int pos,c[N],tmp[N],t[N];
map<int,int> gs,vis,one;
int pre(){
	int ans=0;
    for(int j=1;j<=pos;j++){
	    if(!gs[tmp[j]])continue;
	    int k=gs[tmp[j]]/tmp[j];
	    ans+=k*tmp[j];
		if(tmp[j]*k!=gs[tmp[j]]){
	    	ans+=tmp[j];
		}
	}
	return ans;
}
signed main()
{
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	int T=read();
	while(T--){
		int n=read(),mx=0,sum=0;
		if(n>100&&T!=1){
    		for(int i=1;i<=n;i++){
	    		c[i]=read(); sum+=c[i];
	    		if(c[i]>mx) mx=c[i];
	    	}
    		wrn(sum-mx);
	    }    
	    else {
	    	int flag=1,sxd=0,kkk=0; pos=0;
	    	gs.clear();vis.clear();one.clear();
	    	for(int i=1;i<=n;i++){
	    		c[i]=read();
	    		if(gs[c[i]])flag=0;
	    		else t[++sxd]=c[i];
	    		gs[c[i]]++; sum+=c[i];
	    		if(c[i]>mx) mx=c[i];
	    	}
//	    	cout<<"aaa"<<flag<<endl;
	    	if(flag){
	    		wrn(sum-mx); continue;
	    	}
	    	for(int i=1;i<=sxd;i++){
	    		if(gs[t[i]]==1)kkk+=t[i],one[t[i]]=1;
	    		else tmp[++pos]=t[i];
	    	}
	    	int aans=sum;
	    	int st=pre();
//	    	cout<<"aaa"<<aans<<endl;
	    	for(int i=1;i<=n;i++){
	    		if(vis[c[i]])continue;
	    		if(one[c[i]]){
	    			aans=min(aans,kkk+st-c[i]);
	    			continue;
	    		}
	    		vis[c[i]]=1; gs[c[i]]--;
	    		int comp=1,ans=0;
	    		for(int j=1;j<=pos;j++){
	    			if(!gs[tmp[j]])continue;
	    			int k=gs[tmp[j]]/tmp[j];
	    			ans+=k*tmp[j];
	    			if(tmp[j]*k!=gs[tmp[j]]){
	    				ans+=tmp[j];
	    				if(tmp[j]!=c[i])comp=0;
	    			}
	    		}
	    		ans+=comp; gs[c[i]]++;
	    		aans=min(aans,ans+kkk);
	    	}
	    	wrn(aans);
	    }
	}
	return 0;
}
