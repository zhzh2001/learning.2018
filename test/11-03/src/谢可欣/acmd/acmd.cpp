#include <algorithm>
#include <iostream>
#include <cstdio>
#define int unsigned long long
using namespace std;
int seed,opt,a[10000005];
unsigned long long rnd(){
	seed^=seed<<13; seed^=seed>>7;
	seed^=seed<<17; return seed;
}
signed main()
{
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	int n; cin>>n;
	cin>>seed>>opt;
	for(int i=1;i<=n;i++){
		a[i]=rnd();
	}
	sort(a+1,a+1+n);
	int ans=0;
	for(int i=1;i<n;i++){
		ans=max(ans,a[i+1]-a[i]);
	}
	cout<<ans<<endl;
	return 0;
}
