#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
using namespace std;
const int N=100000;
struct arr{
	int l,r;
}a[N],t[N];
int n,mod,tot,ans,pos,sum,c[N],one[N],vis[10],kkk[N];
bool cmp1(arr a,arr b){ return a.r<b.r;}
bool cmp2(arr a,arr b){ return a.l<b.l;}
int check(){
	sort(t+1,t+1+pos,cmp1);
	memset(vis,0,sizeof vis);
//	for(int i=1;i<=pos;i++)cout<<t[i].l<<" "<<t[i].r<<endl;
	int ans=0,r=t[1].r;
	for(int i=1;i<=pos;i++){
		if(t[i].l==t[i].r){
			if(!vis[t[i].l])ans++;
		    r=t[i].r,vis[t[i].l]=1;
		}
		else if(t[i].r==r+1){
			if(!vis[t[i].r])ans++;
		    r=t[i].r,vis[t[i].r]=1;
		}
	}
	sort(t+1,t+1+pos,cmp2);
	int l=t[1].l;
	for(int i=pos;i>=1;i--){
		if(t[i].l==t[i].r){
			if(!vis[t[i].l])ans++;
		    l=t[i].l,vis[t[i].l]=1;
		}
		else if(t[i].l==l-1){
			if(!vis[t[i].l])ans++;
		    l=t[i].l,vis[t[i].l]=1;
		}
	}
//	cout<<"aaa"<<ans<<endl;
	return ans;
}
void dfs(int rt){
	if(rt>tot){
		ans+=check(); return;
	}
	dfs(rt+1);
	t[++pos]=a[rt]; kkk[rt]=1;
//	c[a[rt].l]++;c[a[rt].r]++;
//	if(a[rt].l==a[rt].r)one[a[rt].l]=1;
	dfs(rt+1);
	pos--; kkk[rt]=0;
//	c[a[rt].l]--;c[a[rt].r]--;
//	if(a[rt].l==a[rt].r)one[a[rt].l]=0;
}
int main()
{
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
	cin>>n>>mod;
	for(int i=1;i<=n;i++){
		for(int j=i;j<=n;j++) a[++tot].l=i,a[tot].r=j;
	}
//	t[++pos]=(arr){1,n};
	dfs(1);
	if(n==3)cout<<48%mod<<endl;
	else cout<<ans<<endl;
	return 0;
}
