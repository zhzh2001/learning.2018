#include<bits/stdc++.h>
using namespace std;
int n,M,i,j,s,ans,l[21],r[21],K,fa[9],rx,ry;
bool fl;
int find(int x){
	return x==fa[x]?x:fa[x]=find(fa[x]);
}
int main(){
	freopen("ac.in","r",stdin);
	freopen("ac.out","w",stdout);
	scanf("%d%d",&n,&M);
	for (i=0;i<n;i++)
		for (j=i;j<n;j++) l[K]=i,r[K++]=j+1;
	for (s=0;s<1<<K;s++){
		for (i=0;i<=n;i++) fa[i]=i;
		fa[0]=n;
		for (i=0;i<K;i++)
			if (s&(1<<i)){
				rx=find(l[i]);ry=find(r[i]);
				if (rx!=ry) fa[rx]=ry;
			}
		fl=1;
		for (i=1;i<=n && fl;i++)
			if (find(i)!=find(i-1)) fl=0;
		ans+=fl;
	}
	printf("%d",ans);
}
