#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
const int N=1e7;
int n,opt,i;
ll a[N],sd,ans;
ll rnd(){
	sd^=sd<<13;
	sd^=sd>>7;
	sd^=sd<<17;
	return sd;
}
int main(){
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	scanf("%d%lld%d",&n,&sd,&opt);
	for (i=0;i<n;i++){
		a[i]=rnd();
		if (opt) a[i]&=(1ll<<32)-1;
	}
	sort(a,a+n);
	for (i=1;i<n;i++)
		if (a[i]-a[i-1]>ans) ans=a[i]-a[i-1];
	printf("%llu",ans);
}
