#include <iostream>
const unsigned long long b1 = (1ll << 16) - 1, b2 = (1ll << 32) - 1;
unsigned long long seed, a[10000005], b[10000005];
int n, opt, cnt[1 << 16];
unsigned long long rnd(){
	seed ^= seed << 13;
	seed ^= seed >> 7;
	seed ^= seed << 17;
	return seed;
}
void Sort(int n){
	for (register int i = 0; i <= b1; ++i) cnt[i] = 0;
	for (register int i = 1; i <= n; ++i) ++cnt[a[i] & b1];
	for (register int i = 1; i <= b1; ++i) cnt[i] += cnt[i - 1];
	for (register int i = n; i; --i) b[cnt[a[i] & b1]--] = a[i];
	for (register int i = 0; i <= b1; ++i) cnt[i] = 0;
	for (register int i = 1; i <= n; ++i) ++cnt[(b[i] >> 16) & b1];
	for (register int i = 1; i <= b1; ++i) cnt[i] += cnt[i - 1];
	for (register int i = n; i; --i) a[cnt[(b[i] >> 16) & b1]--] = b[i];
	for (register int i = 0; i <= b1; ++i) cnt[i] = 0;
	for (register int i = 1; i <= n; ++i) ++cnt[(a[i] >> 32) & b1];
	for (register int i = 1; i <= b1; ++i) cnt[i] += cnt[i - 1];
	for (register int i = n; i; --i) b[cnt[(a[i] >> 32) & b1]--] = a[i];
	for (register int i = 0; i <= b1; ++i) cnt[i] = 0;
	for (register int i = 1; i <= n; ++i) ++cnt[b[i] >> 48];
	for (register int i = 1; i <= b1; ++i) cnt[i] += cnt[i - 1];
	for (register int i = n; i; --i) a[cnt[b[i] >> 48]--] = b[i];
}
int main(){
	freopen("acmd.in", "r", stdin);
	freopen("acmd.out", "w", stdout);
	std :: cin >> n; 
	std :: cin >> seed >> opt;
	for (register int i = 1; i <= n; ++i) a[i] = rnd();
	if (opt) for (register int i = 1; i <= n; ++i) a[i] &= b2;
	Sort(n);
	unsigned long long ans = 0;
	for (register int i = 2; i <= n; ++i)
		a[i] - a[i - 1] > ans ? ans = a[i] - a[i - 1] : 0;
	std :: cout << ans;
}

