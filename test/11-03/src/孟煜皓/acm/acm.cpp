#include <cstdio>
#include <cctype>
#include <algorithm>
#include <cstring>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
int n, a[N], m, b[N], p[N], c[N];
long long s, ans;
long long calc(int i, int j){
	return 1ll * i * ((j + i - 1) / i);
}
int main(){
	freopen("acm.in", "r", stdin);
	freopen("acm.out", "w", stdout);
	int T = read();
	while (T--){
		ans = 0x3f3f3f3f3f3f3f3fll, s = 0, memset(c, 0, sizeof c);
		n = read(), m = 2, b[1] = 1, b[2] = 2;
		for (register int i = 1; i <= n; ++i) a[i] = read(), b[++m] = a[i];
		std :: sort(b + 1, b + 1 + m);
		m = std :: unique(b + 1, b + 1 + m) - b - 1;
		for (register int i = 1; i <= n; ++i)
			p[i] = std :: lower_bound(b + 1, b + 1 + m, a[i]) - b, ++c[p[i]];
		int j = 1, j2 = 2;
		for (register int i = 1; i <= m; ++i) s += calc(b[i], c[i]);
		for (register int i = 1; i <= m; ++i) if (c[i] % b[i]){ j = i, j2 = 1; break; }
		if (j == j2) j2 = 2;
		for (register int i = 1; i <= m; ++i) if (c[i] % b[i] && b[i] != b[j]){ j2 = i; break; }
		for (register int i = 1; i <= n; ++i){
			if (c[p[i]] % b[p[i]] == 1) s -= b[p[i]];
			if (b[p[i]] == b[j]){
				if (c[j2] % b[j2] == 0) s += b[j2];
				ans = std :: min(ans, s);
				if (c[j2] % b[j2] == 0) s -= b[j2];
			}
			else{
				if (c[j] % b[j] == 0) s += b[j];
				ans = std :: min(ans, s);
				if (c[j] % b[j] == 0) s -= b[j];
			}
			if (c[p[i]] % b[p[i]] == 1) s += b[p[i]];
		}
		printf("%lld\n", ans);
	}
}
