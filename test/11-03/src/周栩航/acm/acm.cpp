#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c)){if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
const int N=1e5+5;
ll n,a[N],q[N],qt[N],top,ans;
int main()
{
	freopen("acm.in","r",stdin);
	freopen("acm.out","w",stdout);
	int T=read();
	while(T--)
	{
		top=0;ans=0;
		n=read();
		For(i,1,n)	a[i]=read();
		sort(a+1,a+n+1);
		int tot=1;
		For(i,2,n)
		{
			if(a[i]!=a[i-1])	q[++top]=a[i-1],qt[top]=tot,tot=1;
			else	tot++;	
		}
		q[++top]=a[n];qt[top]=tot;
		int  flag=0,fro=0;	
		For(i,1,top)
		{
			ans+=q[i]*((qt[i]-1)/q[i]+1);
			if(qt[i]%q[i]!=0)	flag++,fro=q[i];
		}
		ll delta=0;
		For(i,1,top)
		{
			ll tmp=q[i]*((qt[i]-1)/q[i]+1)-(qt[i]==1?0:q[i]*((qt[i]-2)/q[i]+1));
			if(flag==0)	tmp--;
			if(flag==1)	if(fro==q[i])	tmp--;
			delta=max(delta,tmp);
		}
		ans-=delta;
		writeln(ans);
	}
}
