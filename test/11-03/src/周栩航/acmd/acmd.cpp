#include<cstdio>
#define For(i,j,k)	for(register int i=j;i<=k;++i)
#pragma GCC optimize(2)
#define Dow(i,j,k)	for(register int i=k;i>=j;--i)
#define ll unsigned long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
const int bas=60000,N=1e7+1;
struct node{unsigned short v[6];}	p[N],p1[N];
int cnt[bas+1],num,n;
inline void Sort()
{
	bool rev=0;
	For(j,0,num-1)
	{
		if(!(j&1))
		{
			For(i,0,bas-1)	cnt[i]=0;
			For(i,1,n)	cnt[p[i].v[j]]++;
			For(i,1,bas-1)	cnt[i]+=cnt[i-1];
			Dow(i,1,n)	p1[cnt[p[i].v[j]]--]=p[i];
			rev=1;
		}
		else
		{
			
			For(i,0,bas-1)	cnt[i]=0;
			For(i,1,n)	cnt[p1[i].v[j]]++;
			For(i,1,bas-1)	cnt[i]+=cnt[i-1];
			Dow(i,1,n)	p[cnt[p1[i].v[j]]--]=p1[i];
			rev=0;
		}
	}
	ll las=0,now=0;
	ll ans=0;
	if(!rev)	For(i,1,n)	
	{
		now=0;
		Dow(j,0,num-1)	now*=bas,now+=p[i].v[j];
		if(i>2)	if(now-las>ans)	ans=now-las;
		else	if(i==2)	ans=now-las;
		las=now;
	}
	else	For(i,1,n)
	{
		now=0;
		Dow(j,0,num-1)	now*=bas,now+=p1[i].v[j];
		if(i>2)	if(now-las>ans)	ans=now-las;
		else	if(i==2)	ans=now-las;
		las=now;
	}		
	writeln(ans);
}
ll seed,opt;
unsigned long long rnd()
{
	seed^=seed<<13;
	seed^=seed>>7;
	seed^=seed<<17;
	return seed;
}
int main()
{
	freopen("acmd.in","r",stdin);
	freopen("acmd.out","w",stdout);
	n=read();
	seed=read();opt=read();
	For(i,1,n)
	{
		ll tmp=rnd();
		if(opt==1)	tmp&=((1LL<<32)-1);
		int cnt=0;
		while(tmp)	p[i].v[cnt++]=tmp%bas,tmp/=bas;
		if(cnt>num)	num=cnt; 
	}
	Sort();
}
