#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=1005;
const int mod=1e9+7;
set<string> s;
int x[N],y[N];
int ans[2*N];
int n,t;
map<int,int>a;
int f[2*N][N];
int anst;
int mmp[N][N];
void dfs(int k,int a,int b)
{
	if(k>2*n){
		string ss;
		ss.clear();
		for(int i=1;i<=2*n;i++)
			ss+=char(ans[i]+'0');
		s.insert(ss);
	}
	if(a<=n){
		ans[k]=x[a];
		dfs(k+1,a+1,b);
	}
	if(b<=n){
		ans[k]=y[b];
		dfs(k+1,a,b+1);
	}
	return;
}
int main()
{
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		x[i]=read();
	for(int i=1;i<=n;i++)
		y[i]=read();
	for(int i=1;i<=n;i++)
		if(!a[x[i]])a[x[i]]=++t;
	for(int i=1;i<=n;i++)
		x[i]=a[x[i]],y[i]=a[y[i]];
	if(n<=10){
		dfs(1,1,1);
		writeln(s.size());
	}
	else{
		f[0][0]=1;
		for(int i=1;i<=2*n;i++){
			int l=max(i-n,0);
			int r=min(n,i);
			for(int j=l;j<=r;j++){
				if(i-j>1){
					if(mmp[x[i-j]][x[i-j-1]]!=i)(f[i][x[i-j]]+=f[i-1][x[i-j-1]])%=mod;
					if((mmp[x[i-j]][y[j]]!=i)&&x[i-j-1]!=y[j])(f[i][x[i-j]]+=f[i-1][y[j]])%=mod;
					mmp[x[i-j]][x[i-j-1]]=i;
					mmp[x[i-j]][y[j]]=i;
				}
				else if(i-j>0){
					if(mmp[x[i-j]][y[j]]!=i)(f[i][x[i-j]]+=f[i-1][y[j]])%=mod;
					mmp[x[i-j]][y[j]]=i;
				}
				if(j>1){
					if(mmp[y[j]][y[j-1]]!=i)(f[i][y[j]]+=f[i-1][y[j-1]])%=mod;
					if((mmp[y[j]][x[i-j]]!=i)&&y[j-1]!=x[i-j])(f[i][y[j]]+=f[i-1][x[i-j]])%=mod;
					mmp[y[j]][y[j-1]]=i;
						mmp[y[j]][x[i-j]]=i;
				}
				else if(j>0){
					if(mmp[y[j]][x[i-j]]!=i)(f[i][y[j]]+=f[i-1][x[i-j]])%=mod;
					mmp[y[j]][x[i-j]]=i;
				}
			}
		}
	//	for(int i=1;i<=2*n;i++)
	//		cout<<f[i][1]<<' ';
	//	cout<<endl;
		for(int i=0;i<=9;i++)
			anst=(anst+f[2*n][i])%mod;
		writeln(anst);
	}
}
