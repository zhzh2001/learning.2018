#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int mod=1e9+7;
int a[35];
int b[35];
char op;
ll n,m,ans;
void check(int k)
{
	int aa=0,bb=0;
	for(int i=1;i<=k;i++)
		if(a[i])aa^=i;
		else if(b[i])bb^=i;
	if(op=='='){
		if(aa==bb)ans++;
	}else if(op=='<'){
		if(aa<bb)ans++;
	}else if(op=='>'){
		if(aa>bb)ans++;
	}
	if(ans>1e18)ans%=mod;
	return;
}
void dfs(int k)
{
	if(k>max(n,m)){
		check(k-1);
		return;
	}
	dfs(k+1);
	if(k<=n){
		a[k]=1;
		dfs(k+1);
		a[k]=0;
	}
	if(k<=m){
		b[k]=1;
		dfs(k+1);
		b[k]=0;
	}
}
inline ll qpow(ll x,ll y)
{
	ll ret=1;
	while(y){
		if(y&1)ret=ret*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return ret;
}
int main()
{
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	cin>>n>>m>>op;
	dfs(1);
	writeln(ans%mod);
}
