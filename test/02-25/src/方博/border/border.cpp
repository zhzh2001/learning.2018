#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=1005;
char ch[N];
int n,q;
int a[N][N];
int main()
{
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	n=read();q=read();
	for(int i=1;i<=n;i++)
		cin>>ch[i];
	for(int i=1;i<=n;i++)
		for(int k=i+1;k<=n;k++)
			if(ch[i]==ch[k]){
				int j=0;
				while(ch[i+j]==ch[k+j]&&k+j<=n){
					a[i][k+j]=max(a[i][k+j],j+1);
					j++;
				}
			}
	for(int i=1;i<=q;i++){
		int l=read(),r=read();
		writeln(a[l][r]);
	}
}
