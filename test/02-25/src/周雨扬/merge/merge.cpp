#include<bits/stdc++.h>
using namespace std;
const int N=2055,mo=1000000007;
void upd(int &x,int y){
	(x+=y)>=mo?x-=mo:233;
}
int n,ans,c;
int a[N],b[N],f[2][N][N],g[N][N];
int fac[N],inv[N],C[N];
int main(){
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	for (int i=1;i<=n;i++)
		scanf("%d",&b[i]);
	f[0][0][0]=g[0][0]=1; c=0;
	C[0]=1;
	for (int i=1;i<=n;i++)
		for (int j=0;j<i;j++)
			upd(C[i],1ll*C[j]*C[i-1-j]%mo);
	int flag=1;
	for (int i=1;i<=n;i++)
		flag&=(a[i]==b[i]);
	if (flag){
		printf("%d\n",C[n]);
		return 0;
	}
	for (int i=0;i<=n;i++,c^=1){
		for (int j=0;j<=n;j++)
			for (int k=0;k<=n;k++){
				if (!(i+j+k)) continue;
				f[c][j][k]=0;
				int p=j,q=k;
				for (;p&&q&&i&&a[p]==b[q];p--,q--)
					upd(f[c][j][k],1ll*f[c^1][p-1][q-1]*C[j-p]%mo);
				g[j][k]=f[c][j][k];
			}
		for (int j=0;j<=n;j++)
			for (int k=0;k<=n;k++){
				if (k) upd(f[c][j][k],f[c][j][k-1]);
				if (j) upd(f[c][j][k],f[c][j-1][k]);
				int p=j,q=k;
				for (;p&&q&&a[p]==b[q];p--,q--)
					upd(f[c][j][k],mo-1ll*f[c][p-1][q-1]*C[j-p]%mo*2%mo);
			}
		upd(ans,f[c][n][n]);
	}
	printf("%d",ans);
}
