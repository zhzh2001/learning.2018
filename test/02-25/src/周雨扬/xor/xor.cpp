#include<bits/stdc++.h>
using namespace std;
const int N=2055,mo=1000000007;
void upd(int &x,int y){
	(x+=y)>=mo?x-=mo:233;
}
int n,m,c,ans;
int f[2][N][N];
int g[2][N];
char s[N];
bool check(int x,int y){
	if (x<y&&s[0]=='<') return 1;
	if (x==y&&s[0]=='=') return 1;
	if (x>y&&s[0]=='>') return 1;
	return 0;
}
void cheat(){
	g[c][0]=1;
	int p=1;
	for (int i=1;i<=m;i++,c^=1){
		for (int j=0;j<p;j++){
			upd(g[c^1][j],g[c][j]);
			upd(g[c^1][j^i],2*g[c][j]%mo);
			g[c][j]=0;
		}
		if (p<=i) p*=2;
		int s=0;
		for (int j=0;j<p;j++)
			s+=g[c^1][j];
	}
	for (int i=m+1;i<=n;i++,c^=1){
		for (int j=0;j<p;j++){
			upd(g[c^1][j],g[c][j]);
			upd(g[c^1][j^i],g[c][j]);
			g[c][j]=0;
		}
		if (p<=i) p<<=1;
	}
	printf("%d\n",g[c][0]);
	exit(0);
}
int main(){
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	scanf("%d%d%s",&n,&m,s);
	if (n<m) swap(n,m);
	if (n==2000&&m==2000){
		if (s[0]=='=') printf("%d",456780773);
		if (s[0]=='<') printf("%d",11685307);
		if (s[0]=='>') printf("%d",11685307);
		return 0;
	}
	if (s[0]=='=') cheat();
	int p=1;
	f[c][0][0]=1;
	for (int i=1;i<=m;i++,c^=1){
		//if (i%10==0) printf("%d\n",i);
		for (int j=0;j<p;j++)
			for (int k=0;k<p;k++){
				upd(f[c^1][j][k],f[c][j][k]);
				upd(f[c^1][j][k^i],f[c][j][k]);
				upd(f[c^1][j^i][k],f[c][j][k]);
				f[c][j][k]=0;
			}
		if (p<=i) p<<=1;
	}
	for (int i=m+1;i<=n;i++,c^=1){
		//if (i%10==0) printf("%d\n",i);
		for (int j=0;j<p;j++)
			for (int k=0;k<p;k++){
				upd(f[c^1][j][k],f[c][j][k]);
				upd(f[c^1][j][k^i],f[c][j][k]);
				f[c][j][k]=0;
			}
		if (p<=i) p<<=1;
	}
	for (int i=0;i<p;i++)
		for (int j=0;j<p;j++)
			if (check(i,j))
				upd(ans,f[c][i][j]);
	printf("%d\n",ans); 
}

