#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define DT 5
using namespace std;
int P[30][N],hsh[30][N];
int n,Q,a[30],b[30],nxt[N];
ll v[N];
map<ll,int> mp;
char s[N];
const int p[30]={
	491250841,357665827,887581259,276513997,785832413,837837941,290493121,607272649,668898709,915863947,
	964192633,534455143,15342331,492490451,168086573,835043527,297801149,189118637,813310591,853812391,
	996155609,944596931,467639149,179802991,952167253,163490567,577513381,232281877,259303763,91678661};
const int bas[30]={
	495841,35827,88759,27697,78413,837941,290121,6072649,698709,915847,
	964633,534543,15331,4924451,16873,835527,2971149,118637,811091,85391,
	996609,9431,469149,17991,95253,16567,5775181,2377,253763,961};
void get(int *a,int x,int y){
	for (int i=0;i<10;i++)
		a[i]=(hsh[i][y]+p[i]-1ll*hsh[i][x-1]*P[i][y-x+1]%p[i])%p[i];
}
bool check(int x,int y,int X,int Y){
	get(a,x,y);
	get(b,X,Y);
	for (int j=0;j<DT;j++)
		if (a[j]!=b[j])
			return 0;
	return 1;	
}
int main(){
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	scanf("%d%d%s",&n,&Q,s+1);
	for (int i=0;i<DT;i++){
		P[i][0]=1;
		for (int j=1;j<=n;j++)
			P[i][j]=1ll*P[i][j-1]*bas[i]%p[i];
	}
	for (int i=0;i<DT;i++)
		for (int j=1;j<=n;j++)
			hsh[i][j]=(1ll*hsh[i][j-1]*bas[i]+s[j])%p[i];
	for (int i=1;i<=n;i++){
		get(a,i,min(n,i+999));
		for (int j=0;j<DT;j++)
			v[i]=v[i]*233+a[j];
	}
	for (int i=n;i;i--){
		if (mp.count(v[i])) nxt[i]=mp[v[i]];
			else nxt[i]=n+1;
		mp[v[i]]=i;
	}
	ll sum=0;
	while (Q--){
		//if (Q%100==0) printf("%d %d\n",sum,Q);
		int x,y,ans=0;
		scanf("%d%d",&x,&y);
		for (int i=1;i<=min(y-x,1000);i++)
			if (check(x,x+i-1,y-i+1,y)) ans=i;
		for (int i=nxt[x];i+ans<=y;i=nxt[i]){
			int l=y-i+1; sum++;
			if (check(x,x+l-1,y-l+1,y)) ans=l;
		}
		printf("%d\n",ans);
	}
}
