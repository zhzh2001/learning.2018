#include <bits/stdc++.h>
#define N 2020
#define mod 1000000007
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
void add(int &a, int b) {
  a = (a + b % mod) % mod;
}
int a[N], b[N], f[N][N];
int main(int argc, char const *argv[]) {
  freopen("merge_xiao.in", "r", stdin);
  // freopen("merge_xiao.out", "w", stdout);

  int n = read();
  for (int i = 1; i <= n; i++)
    a[i] = read();
  for (int i = 1; i <= n; i++)
    b[i] = read();

  f[0][0] = 1;
  for (int i = 1; i <= n; i++)
    f[i][0] = f[0][i] = 1;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      add(f[i + 1][j], a[i + 1] == b[j] ? f[i][j])
      // printf("f[%d][%d] = %d\n", i, j, f[i][j]);
    }
  }
  cout << f[n][n] << endl;

  return 0;
}
/*

4
1 2 3 4
1 2 3 4

f[1][1] = 1 <= f[0][1] + f[1][0] (cause a[1] = b[1], delete 1)
f[1][2] = 2 <= f[0][2] + f[1][1]
f[1][3] = 3 <= f[0][3] + f[1][2]
f[1][4] = 4 <= f[0][4] + f[1][3]
f[2][1] = 2 <= f[1][1] + f[2][0]
f[2][2] = 2 <= f[2][1] + f[1][2] (cause a[2] = b[2], delete 2)
f[2][3] = 
f[2][4] = 
f[3][1] = 3
f[3][2] = 
f[3][3] = 
f[3][4] = 
f[4][1] = 4
f[4][2] = 
f[4][3] = 
f[4][4] = 14


1 5 4 5 8
8 9 7 8

let f[4][4] = x

if (a[5] == b[4]) {
  f[i+1][j] = f[i][j]
} else {
}


*/