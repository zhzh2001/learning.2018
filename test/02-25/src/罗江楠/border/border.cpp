#include <bits/stdc++.h>
#define N 200020
#define ll long long
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
char ch[N];
ll hsh1[N], hsh2[N];
ll mod1 = 1357817, mod2 = 8165149, mod3 = 1000000033;
// ll mod1 = 233, mod2 = 237, mod3 = 1000007;
ll pws1[N], pws2[N];
ll gethash1(int l, int r) {
  return ((hsh1[r] - hsh1[l - 1] * pws1[r - l + 1] % mod3) % mod3 + mod3) % mod3;
}
ll gethash2(int l, int r) {
  return ((hsh2[r] - hsh2[l - 1] * pws2[r - l + 1] % mod3) % mod3 + mod3) % mod3;
}
bool check(int l, int r, int d) {
  if (!d) return true;
  ll hl1 = gethash1(l, l + d - 1);
  ll hl2 = gethash2(l, l + d - 1);
  ll hr1 = gethash1(r - d + 1, r);
  ll hr2 = gethash2(r - d + 1, r);
  // printf("ASK FOR [%d, %2d : %d], returns %10d|%-10d %10d|%-10d\n", l, r, d, hl1, hr1, hl2, hr2);
  return hl1 == hr1 && hl2 == hr2;
}
int main(int argc, char const *argv[]) {
  freopen("border.in", "r", stdin);
  freopen("border.out", "w", stdout);
  // ifstream fin("border_da.out");
  int n = read(), q = read();
  scanf("%s", ch + 1);
  pws1[0] = pws2[0] = 1;
  for (int i = 1; i <= n; i++) {
    pws1[i] = pws1[i - 1] * mod1 % mod3;
    pws2[i] = pws2[i - 1] * mod2 % mod3;
    hsh1[i] = (hsh1[i - 1] * mod1 % mod3 + ch[i]) % mod3;
    hsh2[i] = (hsh2[i - 1] * mod2 % mod3 + ch[i]) % mod3;
  }
  // for (int i = 1; i <= n; i++)
  //   printf("%d %d\n", hsh1[i], hsh2[i]);
  while (q --) {
    int l = read(), r = read();
    for (int d = r-l; ~d; d--) {
      if (check(l, r, d)) {
        printf("%d\n", d);
        // int ans;
        // fin >> ans;
        // if (ans == d) printf("AC#%d\n", q);
        // else return puts("WA"), 0;
        break;
      }
    }
  }
  return 0;
}
/*

hsh1[i] = hsh1[i-1]*233+ch[i];
hi = (hi-2*233+ai-1)*233+ai
   = hi-2*233^2+ai-1*233+ai

*/