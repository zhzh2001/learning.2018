#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,a[10010],b[10010],ans=0,sum=0;
int f[2010][2010];
string s;
map<string,bool>mp;
inline void check(){
	if(!mp[s])mp[s]=1,ans++;
	ans=(ans<MOD)?ans:0;
}
inline void dfs(int x,int y){
	if(x==n+1&&y==n+1){check();return;}
	if(x<=n)s[x+y-1]=a[x]+'0',dfs(x+1,y);
	if(y<=n)s[x+y-1]=b[y]+'0',dfs(x,y+1);
}
signed main()
{
//	freopen("merge.in","r",stdin);
//	freopen("merge.out","w",stdout);
	n=read();s=' ';
	for(int i=1;i<=2*n;i++)s+=' ';
	for(int i=1;i<=n;i++)a[i]=read();
	bool flag=1;
	for(int i=1;i<=n;i++){
		b[i]=read();
		if(a[i]!=b[i])flag=0;
	}
	f[0][0]=1;
	for(int i=0;i<=n;i++){
		for(int j=0;j<=n;j++){
			f[i+1][j]=(f[i+1][j]+f[i][j])%MOD;
			f[i][j+1]=(f[i][j+1]+f[i][j])%MOD;
			if(a[i+1]==b[j+1])f[i+1][j+1]=(f[i+1][j+1]-f[i][j]+MOD)%MOD;
		}
	}
	writeln(f[n][n]);
	dfs(1,1);
	writeln(ans);
	return 0;
}
