#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <bitset>
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
bool a[2][2010];
int n[2],op,ans;
inline void check(){
	int a0=0,a1=0;
	for(int i=1;i<=max(n[0],n[1]);i++){
		if(a[0][i]&&a[1][i])return;
		if(a[0][i])a0^=i;
		if(a[1][i])a1^=i;
	}
	if(op==1&&a0<a1)ans++;
	if(op==2&&a0==a1)ans++;
	if(op==3&&a0>a1)ans++;
	ans=(ans<MOD)?ans:0;
}
inline void dfs(int op,int x){
	if(op==0)dfs(1,1);
	else check();
	for(int i=x;i<=n[op];i++){
		a[op][i]=1;dfs(op,i+1);
		a[op][i]=0;
	}
}
int main()
{
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	n[0]=read();n[1]=read();char ch[10];scanf("%s",ch);
	if(ch[0]=='<')op=1;
	if(ch[0]=='=')op=2;
	if(ch[0]=='>')op=3;
	dfs(0,1);
	writeln(ans);
	return 0;
}
