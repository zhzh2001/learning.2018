#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
#define pa pair<int,int>
#define mp make_pair
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int b1=23,b2=647,M1=19260817,M2=998244353;
int n,q;
char s[200010];
int h1[200010],h2[200010];
inline int mi(int a,int b,int MOD){
	int x=1,y=a;
	while(b){
		if(b&1)x=x*y%MOD;
		y=y*y%MOD;b>>=1;
	}
	return x;
}
inline pa cal(int l,int r){
	int k1=(h1[r]-h1[l-1]+M1)%M1;
	k1=k1*mi(mi(b1,l-1,M1),M1-2,M1)%M1;
	int k2=(h2[r]-h2[l-1]+M2)%M2;
	k2=k2*mi(mi(b2,l-1,M2),M2-2,M2)%M2;
	return mp(k1,k2);
}
signed main()
{
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	n=read();q=read();
	scanf("%s",s+1);
	for(int i=1;i<=n;i++){
		h1[i]=(mi(b1,i-1,M1)*(s[i]-'a')%M1+h1[i-1])%M1;
		h2[i]=(mi(b2,i-1,M2)*(s[i]-'a')%M2+h2[i-1])%M2;		
	}
	while(q--){
		int x=read(),y=read();
		int l=1,r=y-x,ans=0;
		while(l<=r){
			int mid=l+r>>1;
			if(cal(x,x+mid-1)==cal(y-mid+1,y))ans=mid,l=mid+1;
			else r=mid-1;
		}
		writeln(ans);
	}
	return 0;
}
