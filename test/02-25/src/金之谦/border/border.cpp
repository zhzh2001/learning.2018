#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
#define pa pair<int,int>
#define mp make_pair
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,q,nex[3010][3010];
char s[200010];
signed main()
{
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	n=read();q=read();
	scanf("%s",s+1);
	for(int i=1;i<=n;i++){
		int j=0;
		for(int k=i+1;k<=n;k++){
			while(j&&s[k]!=s[i+j])j=nex[i][j];
			if(s[k]==s[i+j])j++;
			nex[i][k]=j;
		}
	}
	while(q--){
		int x=read(),y=read();
		writeln(nex[x][y]);
	}
	return 0;
}
