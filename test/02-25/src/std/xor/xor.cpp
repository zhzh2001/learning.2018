#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
const int P=1e9+7;
class WinterAndSnowmen{
	public:
	inline void up(int &x){if (x>=P) x-=P;}
	int F[2048][2048][2],G[2048][2048];
	int getNumber(int n,int m){
		//考虑两者第一位不同的位置,这样之前的部分异或和一定为0。
		//只需记录总的异或和即可。 
		int c=max(n,m),now,ans=0;
		for (int diff=0;diff<11;diff++){
			memset(F,0,sizeof(F));
			F[0][0][0]=1;
			for (int i=0;i<c;i++)
			  for (int k=0;k<2048;k++)
			    for (int p=0;p<2;p++)
			      if (now=F[i][k][p]){
			      	up(F[i+1][k][p]+=now);
			      	if (i+1<=m)
			      	  up(F[i+1][k^(i+1)][p^(((i+1)>>diff)&1)]+=now);
			      	if (i+1<=n) 
			      	  up(F[i+1][k^(i+1)][p]+=now);
				    }
			int S=0;
			for (int i=diff+1;i<11;i++) S|=1<<i;
			for (int k=0;k<2048;k++)
			  if ((k&S)==0)
			    if ((k>>diff)&1) 
			      up(ans+=F[c][k][1]);
	  }return ans;
  }
  int sameNumber(int n,int m){
  	G[0][0]=1;
		int c=max(n,m),now;
  	for (int i=0;i<c;i++)
  	  for (int k=0;k<2048;k++)
  	    if (now=G[i][k]){
  	    	if (i+1<=n)
  	        up(G[i+1][k^(i+1)]+=now);
  	      if (i+1<=m)
  	        up(G[i+1][k^(i+1)]+=now);
  	      up(G[i+1][k]+=now);
  	    }
  	return G[c][0];
  }
}T;
int main(){
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	int n,m;char ch[2];
	scanf("%d%d%s",&n,&m,&ch);
	if (ch[0]=='=') return printf("%d\n",T.sameNumber(n,m)),0;
	if (ch[0]=='>') swap(n,m);
	printf("%d\n",T.getNumber(n,m));
}
