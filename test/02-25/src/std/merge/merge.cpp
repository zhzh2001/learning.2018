#include<cstdio>
#include<cstring>
#include<algorithm>
#include<set>
#include<map>
#include<vector>
#define rep(i,j,k) for(int i=(int)j;i<=(int)k;i++)
#define per(i,j,k) for(int i=(int)j;i>=(int)k;i--)
using namespace std;
typedef long long LL;
typedef double db;
const int N=2005;
const int P=1000000007;
inline int Pow(int a,int b){
	int c=1;
	for(;b;b>>=1,a=a*1ll*a%P)if(b&1)c=c*1ll*a%P;
	return c;
}
int fac[N<<1],inv[N<<1];
int n,a[N],b[N],f[N][N],g[N];
inline void add(int &a,int b){a+=b;if(a>=P)a-=P;}
inline void dec(int &a,int b){a-=b;if(a<0)a+=P;}
inline int C(int a,int b){
	int ret=fac[a]*1ll*inv[b]%P;
	return ret*1ll*inv[a-b]%P;
}
int main(){
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	scanf("%d",&n);
	rep(i,1,n)scanf("%d",&a[i]);
	rep(i,1,n)scanf("%d",&b[i]);
	fac[0]=1;rep(i,1,2*n)fac[i]=fac[i-1]*1ll*i%P;
	inv[2*n]=Pow(fac[2*n],P-2);
	per(i,2*n-1,0)inv[i]=inv[i+1]*1ll*(i+1)%P;
	f[0][0]=1;
	rep(i,1,n){
		g[i]=C(2*i-1,i-1);
		rep(j,1,i-1){
			dec(g[i],g[i-j]*1ll*C(2*j,j)%P);
		}
	}
	rep(i,0,n)rep(j,0,n){
		if(i)add(f[i][j],f[i-1][j]);
		if(j)add(f[i][j],f[i][j-1]);
		rep(l,1,n){
			if((l>i)||(l>j)||(a[i-l+1]!=b[j-l+1]))break;
			dec(f[i][j],f[i-l][j-l]*1ll*g[l]%P);
		}
	}
	printf("%d\n",f[n][n]);
	return 0;
}

