#include<cstdio>
#include<cstring>
#include<algorithm>
#include<set>
#include<map>
#include<vector>
#define max(a,b) ((a)<(b)?(b):(a))
#define pii pair<int,int>
#define fi first
#define se second
#define rep(i,j,k) for(int i=(int)j;i<=(int)k;i++)
#define per(i,j,k) for(int i=(int)j;i>=(int)k;i--)
using namespace std;
typedef long long LL;
typedef double db;
const int N=410000;
int n,Q;
char a[N];
//sam
int len[N],fail[N],go[N][26],tot,last;
int tail[N];
void expended(int x){
	int q=++tot;int p=last;
	len[q]=len[last]+1;last=tot;
	for(;p&&(!go[p][x]);p=fail[p])go[p][x]=q;
	if(!p){
		fail[q]=1;return;
	}
	else{
		int gt=go[p][x];
		if(len[gt]==len[p]+1){
			fail[q]=gt;
			return;
		}
		else{
			int nw=++tot;
			len[nw]=len[p]+1;
			fail[nw]=fail[gt];
			rep(i,0,25)go[nw][i]=go[gt][i];
			
			fail[gt]=fail[q]=nw;
			for(;p&&(go[p][x]==gt);p=fail[p])go[p][x]=nw;
		}
	}
}
//sam
//suffix tree
vector<int> son[N];
int fa[N],size[N],bg[N];
int bel[N],top[N],wei[N];
int ltot;
void dfs(int x){
	size[x]=1;
	rep(i,0,son[x].size()-1){
		dfs(son[x][i]);
		size[x]+=size[son[x][i]];
		if(size[son[x][i]]>size[bg[x]])bg[x]=son[x][i];
	}
}
int dfstot;
int dfn[N],ed[N];
void fen(int x){
	dfn[x]=++dfstot;
	if(bg[x])fen(bg[x]);
	rep(i,0,son[x].size()-1)if(son[x][i]^bg[x]){
		fen(son[x][i]);
		top[bel[son[x][i]]]=son[x][i];
	}
	if(bg[x]){
		bel[x]=bel[bg[x]];
	}
	else{
		bel[x]=++ltot;
		wei[ltot]=x;
	}
	ed[x]=dfstot;
}
//suffix tree
//segment tree
int ans[N];
vector<pii>Que[N];
int ori[N];
struct mes{
	int l,id,pos;
	inline mes(int _l=0,int _id=0,int _pos=0){
		l=_l;id=_id;pos=_pos;
	}
};
inline bool operator <(const mes &a,const mes &b){
	return a.l<b.l;
}
multiset<mes> info[2][N<<2];
mes mav[2][N<<2];
//segment tree
mes ask(int py,int me,int l,int r,int x,int y){
	if(x<=l&&r<=y)return mav[py][me];
	int mid=(l+r)>>1;
	mes ha=mes(-1,-1,-1);
	if(x<=mid){
		mes ret=ask(py,me<<1,l,mid,x,y);
		ha=max(ha,ret);
	}
	if(y>mid){
		mes ret=ask(py,me<<1|1,mid+1,r,x,y);
		ha=max(ha,ret);
	}
	return ha;
}
void add(int py,int me,int l,int r,mes gt){
	if(l==r){
		info[py][me].insert(gt);
		mav[py][me]=*(--info[py][me].end());
		return;
	}
	int mid=(l+r)>>1;
	if(gt.pos<=mid)add(py,me<<1,l,mid,gt);
	else add(py,me<<1|1,mid+1,r,gt);
	mav[py][me]=max(mav[py][me<<1],mav[py][me<<1|1]);
}
void del(int py,int me,int l,int r,mes gt){
	if(l==r){
		info[py][me].erase(--info[py][me].end());
		if(info[py][me].size()){
			mav[py][me]=*(--info[py][me].end());
		}
		else mav[py][me]=mes(-1,-1,-1);
		return;
	}
	int mid=(l+r)>>1;
	if(gt.pos<=mid)del(py,me<<1,l,mid,gt);
	else del(py,me<<1|1,mid+1,r,gt);
	mav[py][me]=max(mav[py][me<<1],mav[py][me<<1|1]);
}
void relax(int py,mes gt){
	add(py,1,1,dfstot,gt);
}
void calc(int py,int l,int r,int d,int limit){
	while(1){
		mes gt=ask(py,1,1,dfstot,l,r);
		if(gt.l>=limit&&gt.id>0){
			ans[gt.id]=max(ans[gt.id],d);
			del(py,1,1,dfstot,gt);
		}
		else break;
	}
}
void solve(int x){
	int y=tail[x];
	calc(1,dfn[y],ed[y],x,x-len[y]+1);
	
	for(;y;y=fa[top[bel[y]]]){
		calc(0,dfn[top[bel[y]]],dfn[y],x,x);
		calc(1,dfn[y],dfn[wei[bel[y]]],x,x-len[y]+1);
	}
}
void init(int x){
	rep(i,0,Que[x].size()-1){
		int y=tail[x];
		relax(1,mes(Que[x][i].fi,Que[x][i].se,dfn[y]));
		for(;y;y=fa[top[bel[y]]]){
			relax(0,mes(Que[x][i].fi+len[y]-1,Que[x][i].se,dfn[y]));
			relax(1,mes(Que[x][i].fi,Que[x][i].se,dfn[y]));
		}
	}
}
void build(int me,int l,int r){
	if(l==r){
		mav[0][me]=mav[1][me]=mes(-1,-1,-1);
		return;
	}
	int mid=(l+r)>>1;
	build(me<<1,l,mid);
	build(me<<1|1,mid+1,r);
	mav[0][me]=max(mav[0][me<<1],mav[0][me<<1|1]);
	mav[1][me]=max(mav[1][me<<1],mav[1][me<<1|1]);
}
int main(){
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	scanf("%d%d",&n,&Q);
	scanf("%s",a+1);
	tot=last=1;
	rep(i,1,n){
		expended(a[i]-'a');
		tail[i]=last;
	}
	rep(i,1,tot){
		fa[i]=fail[i];
		son[fail[i]].push_back(i);
	}
	dfs(1);
	fen(1);
	top[bel[1]]=1;
	build(1,1,dfstot);
	rep(i,1,Q){
		int l,r;scanf("%d%d",&l,&r);
		Que[r].push_back(pii(l,i));
		ori[i]=l;ans[i]=l-1;
	}
	per(i,n,1){
		solve(i);
		init(i);
	}
	rep(i,1,Q)printf("%d\n",ans[i]-ori[i]+1);
	return 0;
}





