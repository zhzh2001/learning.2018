#include<bits/stdc++.h>
using namespace std;
const int mod=1e9+7;
#define ll long long
int n,m,K;
char s[10];
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
inline int up(int x)
{
	int k=(int)log2(x);
	return ksm(2,k+1);
}
int dp[2048],tmp[2048];
inline int work_denyu()
{
	dp[0]=1;
	if (n>m)
		swap(n,m);
	for (int i=1;i<=n;++i)
	{
		memset(tmp,0,sizeof(tmp));
		for (int j=0;j<K;++j)
			(tmp[i^j]+=dp[j]*2%mod)%=mod;
		for (int j=0;j<K;++j)
			(dp[j]+=tmp[j])%=mod;
	}
	for (int i=n+1;i<=m;++i)
	{
		memset(tmp,0,sizeof(tmp));
		for (int j=0;j<K;++j)
			(tmp[i^j]+=dp[j])%=mod;
		for (int j=0;j<K;++j)
			(dp[j]+=tmp[j])%=mod;
	}
	return dp[0];
}
int f[512][512],g[512][512];
inline int bl()
{
	if (n>m)
	{
		swap(n,m);
		if (s[0]=='<')
			s[0]='>';
		else
			s[0]='<';
	}
	f[0][0]=1;
	for (int i=1;i<=n;++i)
	{
		memset(g,0,sizeof(g));
		for (int j=0;j<K;++j)
			for (int k=0;k<K;++k)
			{
				(g[j^i][k]+=f[j][k])%=mod;
				(g[j][k^i]+=f[j][k])%=mod;
			}
		for (int j=0;j<K;++j)
			for (int k=0;k<K;++k)
				(f[j][k]+=g[j][k])%=mod;
	}
	for (int i=n+1;i<=m;++i)
	{
		memset(g,0,sizeof(g));
		for (int j=0;j<K;++j)
			for (int k=0;k<K;++k)
				(g[j][k^i]+=f[j][k])%=mod;
		for (int j=0;j<K;++j)
			for (int k=0;k<K;++k)
				(f[j][k]+=g[j][k])%=mod;
	}
	int ans=0;
	if (s[0]=='<')
	{
		for (int i=0;i<K;++i)
			for (int j=i+1;j<K;++j)
				(ans+=f[i][j])%=mod;
	}
	else
	{
		for (int i=0;i<K;++i)
			for (int j=0;j<i;++j)
				(ans+=f[i][j])%=mod;
	}
	return ans;
}
int main()
{
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	read(n);
	read(m);
	K=up(max(n,m));
	scanf("%s",s);
	if (s[0]=='=')
	{
		cout<<work_denyu();
		return 0;
	}
	else
	{
		if (n==m)
		{
			int k=work_denyu();
			int x=ksm(3,n);
			x=(x-k+mod)%mod;
			x=(ll)x*ksm(2,mod-2)%mod;
			cout<<x;
			return 0;
		}
		else
		{
			cout<<bl();
			return 0;
		}
	}
	return 0;
}