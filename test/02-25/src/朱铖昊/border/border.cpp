#include<bits/stdc++.h>
using namespace std;
const int mod0=1e9+7,mod1=1e9+9;
#define ll long long
const int N=200005;
int Pow[N][2],has[N][2],n,q,l,r;
char s[N];
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
// inline int ksm(int x,int y)
// {
// 	int ans=1;
// 	for (;y;y/=2,x=(ll)x*x%mod)
// 		if (y&1)
// 			ans=(ll)ans*x%mod;
// 	return ans;
// }
inline pair<int,int> ha(int l,int r)
{
	return make_pair((has[r][0]-(ll)has[l-1][0]*Pow[r-l+1][0]%mod0+mod0)%mod0,
					(has[r][1]-(ll)has[l-1][1]*Pow[r-l+1][1]%mod1+mod1)%mod1);
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		putchar('\n');
		return;
	}
	int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
int main()
{
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	read(n);
	read(q);
	scanf("%s",s+1);
	for (int i=1;i<=n;++i)
	{
		has[i][0]=((ll)has[i-1][0]*27+s[i]-'a'+1)%mod0;
		has[i][1]=((ll)has[i-1][1]*27+s[i]-'a'+1)%mod1;
	}
	Pow[0][0]=Pow[0][1]=1;
	for (int i=1;i<=n;++i)
	{
		Pow[i][0]=(ll)Pow[i-1][0]*27%mod0;
		Pow[i][1]=(ll)Pow[i-1][1]*27%mod1;
	}
	// inv[n]=ksm(Pow[n],mod-2);
	// for (int i=n-1;i>=0;--i)
	// 	inv[i]=(ll)inv[i+1]*27%mod;
	while (q--)
	{
		read(l);
		read(r);
		bool b=0;
		for (int i=r-l,j=1;i>=0&&j<=1000;--i,++j)
			if (ha(l,i+l-1)==ha(r-i+1,r))
				{
					write(i);
					b=1;
					break;
				}
		if (b)
			continue;
		for (int i=min(4000,r-l);i>=0;--i)
		{
			if (i==0)
				puts("0");
			else
			{
				if (ha(l,i+l-1)==ha(r-i+1,r))
				{
					write(i);
					break;
				}
			}
		}
	}
	return 0;
}