#include<bits/stdc++.h>
using namespace std;
const int N=200005;
vector<pair<int,int> > q[N];
int n,ans[N],Q,x,y;
char s[N];
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		putchar('\n');
		return;
	}
	int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
int main()
{
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	read(n);
	read(Q);
	scanf("%s",s+1);
	for (int i=1;i<=Q;++i)
	{
		read(x);
		read(y);
		q[x].push_back(make_pair(y,i));
	}
	for (int i=1;i<=n;++i)
		if (q[i].size())
		{
			sort(q[i].begin(),q[i].end());
			int last=i,len=1;
			for (unsigned j=0;j<q[i].size();++j)
			{
				for (int k=last+1;k<=q[i][j].first;++k)
					if (s[k]!=s[i+(k-i)%len])
					{
						if (s[k]==s[i])
							len=k-i;
						else
							len=k-i+1;
					}
				last=q[i][j].first;
				ans[q[i][j].second]=q[i][j].first-i+1-len;
			}
		}
	for (int i=1;i<=Q;++i)
		write(ans[i]);
	return 0;
}