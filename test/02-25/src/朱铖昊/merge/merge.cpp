#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int mod=1e9+7,mod1=1e9+9,N=4005;
int n,m,ans,a[N],b[N],c[N];
set<pair<int,int> >s;
inline void dfs(int x,int y,pair<int,int> has)
{
	if (x==n&&y==n)
	{
		if (!s.count(has))
		{
			s.insert(has);
			++ans;
			//cout<<has.first<<' '<<has.second<<'\n';
			if (ans==mod)
				ans=0;
		}
		return;
	}
	if (x!=n)
	{
		pair<int,int> tmp;
		tmp.first=((ll)has.first*(n+1)%mod+a[x+1])%mod;
		tmp.second=((ll)has.second*(n+1)%mod1+a[x+1])%mod1;
		// if (!s.count(tmp))
		// {
		// 	s.insert(tmp);
			dfs(x+1,y,tmp);
		// }
	}
	if (y!=n)
	{
		pair<int,int> tmp;
		tmp.first=((ll)has.first*(n+1)%mod+b[y+1])%mod;
		tmp.second=((ll)has.second*(n+1)%mod1+b[y+1])%mod1;
		// if (!s.count(tmp))
		// {
			// s.insert(tmp);
			dfs(x,y+1,tmp);
		// }
	}
}
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline bool check()
{
	for (int i=1;i<=n;++i)
		if (a[i]!=b[i])
			return 0;
	return 1;
}
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
inline void bff()
{
	int ans=1,inv=1;
	for (int i=1;i<=n;++i)
		ans=(ll)ans*i%mod;
	inv=ksm(ans,mod-2);
	for (int i=n+1;i<=n*2;++i)
		ans=(ll)ans*i%mod;
	inv=(ll)inv*inv%mod*ksm(n+1,mod-2)%mod;
	ans=(ll)ans*inv%mod;
	cout<<ans;
}
int main()
{
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
	{
		// a[i]=i;
		read(a[i]);
		//c[i]=a[i];
	}
	for (int i=1;i<=n;++i)
	{
		// b[i]=i;
		read(b[i]);
		//c[i+n]=b[i];
	}
	if (check())
	{
		bff();
		return 0;
	}
	// sort(c+1,c+1+n*2);
	// for (int i=1;i<=n*2;++i)
	// 	if (c[i]!=c[m])
	// 		c[++m]=c[i];
	// for (int i=1;i<=n;++i)
	// 	a[i]=lower_bound(c+1,c+1+m,a[i])-c;
	// for (int i=1;i<=n;++i)
	// 	b[i]=lower_bound(c+1,c+1+m,b[i])-c;
	s.insert(make_pair(0,0));
	dfs(0,0,make_pair(0,0));
	cout<<ans;
	return 0;
}