#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define re register
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int q,n,m,ans[N],nxt[N],t;
char str[N];
struct xx{
	int l,r,i;
}z[N];
inline bool cmp(xx a,xx b){
	return a.l==b.l?a.r<b.r:a.l<b.l;
}
inline int fmax(int a,int b){
	return a>b?a:b;
}
int main(){
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	n=read();q=read();
	scanf("%s",str+1);
	for (int i=1;i<=q;i++){
		z[i].l=read();z[i].r=read();z[i].i=i;
	}
	sort(z+1,z+q+1,cmp);
	for (int i=1;i<=q;i++){
		if (z[i].l==z[i-1].l){
			for (re int j=z[i-1].r+1;j<=z[i].r;j++){
				while (str[j]!=str[t+1]&&t>=z[i].l) t=nxt[t];
				t=fmax(t,z[i].l-1);
				nxt[j]=(str[j]==str[t+1])?++t:z[i].l-1;	
			}
			ans[z[i].i]=nxt[z[i].r]-z[i].l+1;
		}
		else{
			t=z[i].l-1;
			for (re int j=z[i].l+1;j<=z[i].r;j++){
				while (str[j]!=str[t+1]&&t>=z[i].l) t=nxt[t];
				t=fmax(t,z[i].l-1);
				nxt[j]=(str[j]==str[t+1])?++t:z[i].l-1;
			}
			ans[z[i].i]=nxt[z[i].r]-z[i].l+1;
		}
	}
	for (int i=1;i<=q;i++){
		wrn(ans[i]);
	}
	return 0;
}
