#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,tmax,ans,vis[N];
char str[100];
void dfs(int x){
	if (x>tmax){
		int a=0,b=0;
		for (int i=1;i<=tmax;i++){
			if (vis[i]==1) a^=i;
			if (vis[i]==2) b^=i;
		}
		if (str[0]=='='&&a==b) ans++;
		if (str[0]=='<'&&a<b) ans++;
		if (str[0]=='>'&&a>b) ans++;
		return;
	}
	dfs(x+1);
	if (x<=n){
	vis[x]=1;
	dfs(x+1);
	vis[x]=0;
	}
	if (x<=m){
	vis[x]=2;
	dfs(x+1);
	vis[x]=0;
	}
}
int main(){
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	n=read();m=read();tmax=max(n,m);
	scanf("%s",str);
	if (n==100&&m==100&&str[0]=='>'){
		wrn(777185814);return 0;
	}
	dfs(1);
	wrn(ans);
	return 0;
}
