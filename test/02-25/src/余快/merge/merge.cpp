#include<bits/stdc++.h>
#define ll long long
#define N 105
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int ans,tot,n,m,a[N],b[N],f[N][N],g[N][N][N],z[N],p[N][N][N],yk,pp[N][N];
const int mod1=1000007;
int hash[mod1][22];
int dfs(int a1,int b1,int a2){
	if (a1<0||b1<0||a2<0||a1+b1-a2<0) return 0;
	if (a1==a2||b1==a1+b1-a2) {if (pp[a1][b1]==yk) return 0;pp[a1][b1]=yk;return f[a1][b1];}
	if (a1>a2) b1=a1+b1-a2,swap(a1,a2);
	if (g[a1][b1][a2]) {if (p[a1][b1][a2]==yk) return 0;p[a1][b1][a2]=yk;return g[a1][b1][a2];}
	if (a[a1]==a[a2]) g[a1][b1][a2]+=dfs(a1-1,b1,a2-1);
	if (a[a1]==b[a1+b1-a2]) g[a1][b1][a2]+=dfs(a1-1,b1,a2);
	if (b[b1]==a[a2]) g[a1][b1][a2]+=dfs(a1,b1-1,a2-1);
	if (b[b1]==b[a1+b1-a2]) g[a1][b1][a2]+=dfs(a1,b1-1,a2);
	return g[a1][b1][a2];
}
int hash2(int x){
	while (hash[x][0]){
		int pd=1;
		for (int i=1;i<=n*2;i++){
			if (z[i]!=hash[x][i]){
				pd=0;break;
			}
		}
		if (pd) return 0;
		else x=(x+1==mod1)?0:x+1;
	}
		hash[x][0]=1;
		for (int i=1;i<=n*2;i++){
			hash[x][i]=z[i];
		}
		return 1;
}
void dfs2(int x,int y,ll num){
	if (x==n+1&&y==n+1){
		ans+=hash2(num);return;
	}
	if (x<=n){
		z[++tot]=a[x];
		dfs2(x+1,y,((ll)num*107+a[x])%mod1);
		tot--;
	}
	if (y<=n){
		z[++tot]=b[y];
		dfs2(x,y+1,((ll)num*107+b[y])%mod1);
		tot--;
	}
}
int main(){
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<=n;i++) b[i]=read();
	if (n<=10){
		dfs2(1,1,0);
		wrn(ans);
		return 0;
	}
	for (int i=0;i<=n;i++) f[i][0]=f[0][i]=1;
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			yk=i*(n+1)+j;
			f[i][j]=(f[i-1][j]+f[i][j-1])%mod;
			if (a[i]==b[j]){
				f[i][j]-=dfs(i-1,j,i);
				f[i][j]=(f[i][j]%mod+mod)%mod;
			}
		}
	}
	wrn(f[n][n]);
	return 0;
}
