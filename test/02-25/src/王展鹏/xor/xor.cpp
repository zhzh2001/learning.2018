#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int mod=1000000007;
int n,m,f[2][1<<11][1<<11],g[1<<11][1<<11],ans;
char ch[100];
int main(){
	freopen("xor.in","r",stdin); freopen("xor.out","w",stdout);
	n=read(); m=read(); scanf("%s",ch);
	if(n==2000&&m==2000){
		if(ch[0]=='=')puts("456780773"); else puts("11685307"); return 0;
	}
	if(n>m){
		swap(n,m); if(ch[0]=='<')ch[0]='>'; else if(ch[0]=='>')ch[0]='<';
	}
	if(ch[0]=='='){
		g[0][0]=1;
		for(int i=1;i<=n;i++){
			int lim=1; while(lim<=i)lim<<=1;
			for(int j=0;j<lim;j++)g[i][j]=(g[i-1][j]+g[i-1][j^i]*2%mod)%mod;
		}
		for(int i=n+1;i<=m;i++){
			int lim=1; while(lim<=i)lim<<=1;
			for(int j=0;j<lim;j++)g[i][j]=(g[i-1][j]+g[i-1][j^i])%mod;
		}
		cout<<g[m][0]<<endl;
		return 0;
	}
	f[0][0][0]=1;
	for(int i=1;i<=n;i++){
		int lim=1; while(lim<=i)lim<<=1;
		for(int j=0;j<lim;j++){
			for(int k=0;k<lim;k++){
				//if((k^i)>=lim)cout<<k<<" "<<i<<" "<<lim<<endl;
				f[i&1][j][k]=((f[i&1^1][j][k]+f[i&1^1][j^i][k])%mod+f[i&1^1][j][k^i])%mod;
			}
		}
	}
	for(int i=n+1;i<=m;i++){
		int lim=1; while(lim<=i)lim<<=1;
		for(int j=0;j<lim;j++){
			for(int k=0;k<lim;k++){
				//if((k^i)>=lim)cout<<k<<" "<<i<<" "<<lim<<endl;
				f[i&1][j][k]=(f[i&1^1][j][k]+f[i&1^1][j][k^i])%mod;
			}
		}
	}
	int lim=1; while(lim<=m)lim<<=1;
 	if(ch[0]=='<'){
		for(int i=0;i<lim;i++)for(int j=0;j<i;j++)ans=(ans+f[m&1][j][i])%mod;
	}else if(ch[0]=='>'){
		for(int i=0;i<lim;i++)for(int j=0;j<i;j++)ans=(ans+f[m&1][i][j])%mod;
	}else{
		for(int i=0;i<lim;i++)ans=(ans+f[m&1][i][i])%mod;
	}
	cout<<ans<<endl;
}
