#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int mod=1000000007;
set<string> M;
int a[10005],ans,b[10005],n;
void dfs(int x,int y,string s){
	if(x>n&&y>n){
		if(!M.count(s)){
			ans++; M.insert(s);
		}
		return;
	}
	if(x<=n){
		dfs(x+1,y,s+(char)(a[x]+'0'));
	}
	if(y<=n){
		dfs(x,y+1,s+(char)(b[y]+'0'));
	}
}
inline int ksm(int a,int b){
	int t=1,y=a;
	while (b){
		if (b&1) t=(long long)t*y%mod;
		y=(long long)y*y%mod;
		b>>=1;
	}
	return t;
}
int main(){
	freopen("merge.in","r",stdin); freopen("merge.out","w",stdout);
	n=read();
	if(n<=10){
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++)b[i]=read();
	dfs(1,1,"");
	cout<<ans<<endl;}else{
		ll ans=1;
		for(int i=n+1;i<=2*n;i++)ans=ans*i%mod;
		for(int i=1;i<=n;i++)ans=ans*ksm(i,mod-2)%mod;
		ans=ans*ksm(n+1,mod-2)%mod;
		cout<<ans<<endl;
	}
}
/*
1 2 3 4 5
4 3 2 1 5
*/
