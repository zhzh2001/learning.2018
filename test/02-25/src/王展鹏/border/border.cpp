#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=200005,K=20;
int n,m,tong[N],SA[N],y[N],x[N],rk[N],h[N],height[N],logn[N];
char ch[N];
int dp[N][K];
#define cmp(a,b,k) y[a]==y[b]&&y[a+k]==y[b+k]
inline int ask(int a,int b){
	if(a==b)return n-a+1;
	a=rk[a]; b=rk[b]; if(a>b)swap(a,b); b--;
	int zs=logn[b-a+1];
	return min(dp[b][zs],dp[a+(1<<zs)-1][zs]);
}
void init(){
	int m=150;
	for(int i=1;i<=n;i++)tong[x[i]=ch[i]]++;
	for(int i=1;i<=m;i++)tong[i]+=tong[i-1];
	for(int i=n;i;i--)SA[tong[x[i]]--]=i; logn[0]=-1;
	for(int k=1;k<=n;k<<=1){
		int p=0;
		for(int i=n-k+1;i<=n;i++)y[++p]=i;
		for(int i=1;i<=n;i++)if(SA[i]>k)y[++p]=SA[i]-k;
		memset(tong,0,sizeof(tong));
		for(int i=1;i<=n;i++)tong[x[y[i]]]++;
		for(int i=1;i<=m;i++)tong[i]+=tong[i-1];
		for(int i=n;i;i--)SA[tong[x[y[i]]]--]=y[i];
		swap(x,y);
		x[SA[1]]=p=1;
		for(int i=2;i<=n;i++)x[SA[i]]=cmp(SA[i],SA[i-1],k)?p:++p;
		if(p>=n)break;
		m=p;
	}
	for(int i=1;i<=n;i<<=1)logn[i]=logn[i>>1]+1;
	for(int i=1;i<=n;i++)rk[SA[i]]=i;
	for(int i=1;i<=n;i++){
		int t=SA[rk[i]+1];
		for(h[i]=max(0,h[i-1]-1);ch[i+h[i]]==ch[t+h[i]];h[i]++);
		height[rk[i]]=h[i];
	}
	for(int i=1;i<=n;i++){dp[i][0]=height[i]; if(i>1&&!logn[i])logn[i]=logn[i-1]; }
	for(int j=1;j<K;j++)for(int i=1<<(j-1);i<=n;i++)dp[i][j]=min(dp[i][j-1],dp[i-(1<<(j-1))][j-1]);
}
int top,ans[N],zhan[N],q[N];
vector<pair<int,int> > v[N];
pair<int,int> a[N];
set<int> s[N],zs;
set<int>::iterator it;
int main(){
	freopen("border.in","r",stdin); freopen("border.out","w",stdout);
	n=read(); int m=read();
	scanf("%s",ch+1);
	//for(int i=1;i<=n;i++)ch[i]=rand()%1+'a';
	init();
	int top=1;
	zhan[top]=-1e9; q[1]=0;
	for(int i=1;i<=m;i++){
		int l=read(),r=read(),flag=0;
		for(int j=l+1;j<=l+100&&j<=r;j++){
			if(ask(l,j)>=r-j+1){ans[i]=r-j+1; flag=1; break;}
		}
		if(!flag)v[l].push_back(mp(r,i));
	}
	for(int i=1;i<n;i++){
		while(zhan[top]>=height[i]&&top){
			if(s[top].size()>zs.size())swap(s[top],zs);
			for(it=s[top].begin();it!=s[top].end();it++)zs.insert(*it);
			top--;
		}
		zhan[++top]=height[i]; zs.insert(SA[i]); swap(s[top],zs); zs.clear(); q[top]=i;
		for(unsigned j=0;j<v[SA[i+1]].size();j++)if(top<200){
			for(int k=2;k<=top;k++){
				it=s[k].upper_bound(v[SA[i+1]][j].first-zhan[k]);
				if(it!=s[k].end()&&*it>=SA[i+1])ans[v[SA[i+1]][j].second]=max(ans[v[SA[i+1]][j].second],v[SA[i+1]][j].first-*it+1);
			}
		}else{
			for(int k=2;k<=100;k++){
				it=s[k].upper_bound(v[SA[i+1]][j].first-zhan[k]);
				if(it!=s[k].end()&&*it>=SA[i+1])ans[v[SA[i+1]][j].second]=max(ans[v[SA[i+1]][j].second],v[SA[i+1]][j].first-*it+1);
			}
			for(int k=top-100;k<=top;k++){
				it=s[k].upper_bound(v[SA[i+1]][j].first-zhan[k]);
				if(it!=s[k].end()&&*it>=SA[i+1])ans[v[SA[i+1]][j].second]=max(ans[v[SA[i+1]][j].second],v[SA[i+1]][j].first-*it+1);
			}
		}
	}
	top=1;
	for(int i=n-1;i;i--){
		while(zhan[top]>=height[i]&&top){
			if(s[top].size()>zs.size())swap(s[top],zs);
			for(it=s[top].begin();it!=s[top].end();it++)zs.insert(*it);
			top--;
		}
		zhan[++top]=height[i]; zs.insert(SA[i+1]); swap(s[top],zs); zs.clear(); q[top]=i;
		//for(it=s[top].begin();it!=s[top].end();it++)cout<<*it<<" "; puts("");
		for(unsigned j=0;j<v[SA[i]].size();j++)if(top<200){
			for(int k=2;k<=top;k++){
				it=s[k].upper_bound(v[SA[i]][j].first-zhan[k]);
				if(it!=s[k].end()&&*it>=SA[i])ans[v[SA[i]][j].second]=max(ans[v[SA[i]][j].second],v[SA[i]][j].first-*it+1);
			}
		}else{
			for(int k=2;k<=100;k++){
				it=s[k].upper_bound(v[SA[i]][j].first-zhan[k]);
				if(it!=s[k].end()&&*it>=SA[i])ans[v[SA[i]][j].second]=max(ans[v[SA[i]][j].second],v[SA[i]][j].first-*it+1);
			}
			for(int k=top-100;k<=top;k++){
				it=s[k].upper_bound(v[SA[i]][j].first-zhan[k]);
				if(it!=s[k].end()&&*it>=SA[i])ans[v[SA[i]][j].second]=max(ans[v[SA[i]][j].second],v[SA[i]][j].first-*it+1);
			}
		}
	}
	for(int i=1;i<=m;i++){
		writeln(ans[i]);
	}
}
