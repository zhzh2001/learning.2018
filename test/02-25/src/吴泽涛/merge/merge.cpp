#include <bits/stdc++.h>
#define LL long long
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}

const int N = 2011, mod = 1e9+7; 
int n,tot, num, ans, y; 
map<LL, int> mp; 
int vis[N], a[N], a1, b[N], b1, num1, num2, p[35], pos; 

inline bool check() {
	a1 = 0; b1 = 0; num = 0;
	LL res = 0; 
	For(i, 1, tot) 
		if(vis[i]) res = res*10+a[++a1]; 
		else res = res*10+b[++b1];  
	if(!mp.count(res)) {
		++ans; 
		mp[res] = 1; 
	}
}

void dfs(int x) {
	if(x > tot) {
		check(); 
		return; 
	}
	vis[x] = 0; ++num1; if(num1<=n) dfs(x+1); --num1; 
	vis[x] = 1; ++num2; if(num2<=n) dfs(x+1); --num2; 
}

void work_1() {
	dfs(1); 
	printf("%d\n", ans); 
	exit(0); 
}

inline int ksm(int x) {
	int res = 1; 
	Dow(i, pos, 1) {
		res = 1ll*res*res%mod; 
		if(p[i]) res=1ll*res*x%mod; 
	}
	return res; 
}

void work_2() {
	LL ans = 1; 
	y = mod-2; 
	while(y) {
		p[++pos] = y&1; 
		y/=2; 
	} 
	For(i, n+2, tot) ans = ans*i%mod; 
	For(i, 2, n) ans = ans*ksm( i )%mod; 
	printf("%lld\n", ans); 
	exit(0); 
}

int main() {
	freopen("merge.in","r",stdin); 
	freopen("merge.out","w",stdout); 
	n = read(); tot=2*n; 
	For(i, 1, n) a[i] = read(); 
	For(i, 1, n) b[i] = read();
	bool flag = 0; 
	For(i, 1, n) 
		if(a[i] != b[i]) flag = 1; 
	if(!flag) work_2();  
	if(tot<=25) work_1(); 
}








