#include <bits/stdc++.h>
#define LL long long
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}
void write(int x) {
	if(x<0) putchar('-'), x=-x; 
	if(x>9) write(x/10); 
	putchar(x%10+48); 
}
inline void writeln(int x) { write(x); putchar('\n'); }

const int N = 2e5+11; 
int n, Q; 
char s[N];
int a[10],nxt[N]; 

inline int kmp(int l, int r) {
	nxt[l] = l-1; nxt[l-1] = l-1; nxt[l-2] = l-1; 
	For(i, l+1, r) {
		int x = nxt[i-1]; 
		while(x!=l-1 && s[x+1]!=s[i]) 
			x = nxt[x]; 
		if(s[x+1]==s[i]) nxt[i] = x+1; 
		else nxt[i] = l-1; 
	}
	return nxt[r]-l+1; 
}

int main() {
	freopen("border.in","r",stdin); 
	freopen("border.out","w",stdout); 
	n = read(); Q = read(); 
	scanf("%s", s+1); 
	while(Q--) {
		int l=read(), r = read(); 
		writeln( kmp(l, r) );   
	}
}





