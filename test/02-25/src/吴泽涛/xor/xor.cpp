#include <bits/stdc++.h> 
#define LL long long
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}

const int mod = 1e9+7, N = 2011; 
char s[10];
int c[2*N], a, b;  
int n, m, tot, sum; 

bool check() {
	a = 0; b = 0; 
	For(i, 1, n) 
		if(c[i]) a^=i;  
	For(i, n+1, tot) 
		if(c[i]) b^=(i-n);
	if(s[1]=='='&& a==b) return 1; 
	if(s[1]=='<'&& a<b) return 1; 
	if(s[1]=='>'&& a>b) return 1;  
	return 0; 
}

void dfs(int x) {
	if(x > tot) {
		if(check()) ++sum;  
		return; 
	}
	c[x] = 0; dfs(x+1); 
	if(x>n && c[x-n]) return; 
	c[x] = 1; dfs(x+1); 
}

inline void work_1() {
	dfs(1); 
	printf("%d\n", sum); 
	exit(0); 
}

int main() {
	freopen("xor.in","r",stdin); 
	freopen("xor.out","w",stdout); 
	n = read(); m = read(); 
	scanf("%s", s+1);
	if(n<m) {
		swap(n, m); 
		if(s[1]=='<') s[1]='>'; 
		else if(s[1]=='>') s[1]='<'; 
	}
	tot = n+m;  
	if(n+m<=25) work_1();          
}




