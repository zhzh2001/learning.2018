#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <set>
#include <cctype>
using namespace std;
template <typename Int>
inline void Read(Int &x) {
  x = 0;
  char ch;
  Int flag = 1;
  for (ch = getchar(); isspace(ch); ch = getchar())
    ;
  if (ch == '-') {
    flag = -1;
    ch = getchar();
  }
  for (; isdigit(ch); ch = getchar())
    x = x * 10 + ch - '0';
  x *= flag;
}
typedef long long ll;
typedef pair<int, int> pii;
typedef pair<ll, ll> pll;
const int kMaxN = 2005, kMod = 1e9 + 7;
const ll base1 = 233333, base2 = 100007, mod1 = 1e9 + 7, mod2 = 1e9 + 9;
int n, a[kMaxN], b[kMaxN], stk[kMaxN];
set<pll> s;
void DFS1(int dep, int x, int y) {
  if (x == n && y == n && dep == 2 * n) {
    ll hash1 = 0, hash2 = 0;
    for (int i = 1; i <= dep; ++i) {
      hash1 = (hash1 * base1 % mod1 + stk[i]) % mod1;
      hash2 = (hash2 * base2 % mod2 + stk[i]) % mod2;
    }
    s.insert(make_pair(hash1, hash2));
    return;
  }
  if (x != n) {
    stk[dep + 1] = a[x + 1];
    DFS1(dep + 1, x + 1, y);
  }
  if (y != n) {
    stk[dep + 1] = b[y + 1];
    DFS1(dep + 1, x, y + 1);
  }
}
inline int Force1() {
  for (int i = 1; i <= n; ++i)
    Read(a[i]);
  for (int i = 1; i <= n; ++i)
    Read(b[i]);
  DFS1(0, 0, 0);
  printf("%d\n", (int)s.size() % kMod);
  return 0;
} 
inline int Force2() {
  Force1();
  return 0;
}
int main() {
  freopen("merge.in", "r", stdin);
  freopen("merge.out", "w", stdout);
  Read(n);
  if (n <= 10)
    return Force1();
  if (n <= 100)
    return Force2();
  return 0;
}