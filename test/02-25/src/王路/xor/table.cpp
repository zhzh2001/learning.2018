#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <set>
#include <cctype>
using namespace std;
template <typename Int>
inline void Read(Int &x) {
  x = 0;
  char ch;
  Int flag = 1;
  for (ch = getchar(); isspace(ch); ch = getchar())
    ;
  if (ch == '-') {
    flag = -1;
    ch = getchar();
  }
  for (; isdigit(ch); ch = getchar())
    x = x * 10 + ch - '0';
  x *= flag;
}
typedef long long ll;
typedef pair<int, int> pii;
typedef pair<ll, ll> pll;
const int kMaxN = 2005, kMod = 1e9 + 7;
int n, m, f[kMaxN][kMaxN];
char ch;
inline bool Comp(int x1, int x2) {
  if (ch == '=')
    return x1 == x2;
  if (ch == '<')
    return x1 < x2;
  if (ch == '>')
    return x1 > x2;
}
int Force1() {
  int res = 0;
  for (int i = 0; i < (1 << n); ++i) {
    for (int j = 0; j < (1 << m); ++j) {
      if (i & j)
        continue;
      int x1 = 0, x2 = 0;
      for (int k = 0; k < n; ++k)
        if (i & (1 << k))
          x1 ^= (k + 1);
      for (int k = 0; k < m; ++k)
        if (j & (1 << k))
          x2 ^= (k + 1);
      res += Comp(x1, x2);
    }
  }
  printf("%5d ", f[n][m] = res);
  return 0;
}
int main() {
  freopen("table.out", "w", stdout);
  ch = '=';
  for (int i = 0; i <= 10; ++i) {
    for (int j = 0; j <= i; ++j) {
      n = i, m = j;
      Force1();
    }
    puts("");
  }
  // for (int i = 1; i <= 10; ++i) {
  //   for (int j = 1; j <= i; ++j)
  //     if (f[i][j] != f[i][j - 1] * 2LL - f[i - 1][j - 1] && f[i][j] % 2 == 1)
  //       printf("%d %d\n", i, j);
  // }
  return 0;
}