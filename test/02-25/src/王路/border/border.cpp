#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cctype>
using namespace std;
template <typename Int>
inline void Read(Int &x) {
  x = 0;
  char ch;
  Int flag = 1;
  for (ch = getchar(); isspace(ch); ch = getchar())
    ;
  if (ch == '-') {
    flag = -1;
    ch = getchar();
  }
  for (; isdigit(ch); ch = getchar())
    x = x * 10 + ch - '0';
  x *= flag;
}
typedef long long ll;
typedef pair<int, int> pii;
typedef pair<ll, ll> pll;
const int kMaxN = 2e5 + 5;
const ll kBase1 = 41, kBase2 = 29, kMod1 = 1e9 + 7, kMod2 = 1e9 + 9;
int n, q;
ll hash1[kMaxN], hash2[kMaxN], base1[kMaxN], base2[kMaxN];
char str[kMaxN];
pll GetHash(int l, int r) {
  return make_pair(
    (hash1[r] - (hash1[l - 1] * base1[r - l + 1] % kMod1) + kMod1) % kMod1, 
    (hash2[r] - (hash2[l - 1] * base2[r - l + 1] % kMod2) + kMod2) % kMod2);
}
int main() {
  freopen("border.in", "r", stdin);
  freopen("border.out", "w", stdout);
  scanf("%d%d\n%s", &n, &q, str + 1);
  int len = strlen(str + 1);
  base1[0] = base2[0] = 1;
  for (int i = 1; i <= len; ++i) {
    base1[i] = (base1[i - 1] * kBase1 % kMod1);
    base2[i] = (base2[i - 1] * kBase2 % kMod2);
  }
  for (int i = 1; i <= len; ++i) {
    hash1[i] = (hash1[i - 1] * kBase1 % kMod1 + (str[i] - 'a')) % kMod1;
    hash2[i] = (hash2[i - 1] * kBase2 % kMod2 + (str[i] - 'a')) % kMod2;
  }
  bool bf = true;
  while (q--) {
    int l, r;
    Read(l), Read(r);
    if (bf) {
      int ans = 0;
      for (int i = r - l; i > 0; --i) {
        if (GetHash(l, l + i - 1) == GetHash(r - i + 1, r)) {
          ans = i;
          break;
        }
      }
      printf("%d\n", ans);
    }
  }
  return 0;
}