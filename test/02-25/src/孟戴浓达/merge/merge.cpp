//#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<fstream>
using namespace std;
ifstream fin("merge.in");
ofstream fout("merge.out");
const int mod=1000000007;
int n,a[2003],b[2003];
long long jiyi[2003][2003];
long long dp(int x,int y){
	long long &fanhui=jiyi[x][y];
	if(fanhui!=-1){
		return fanhui;
	}
	if(x==n+1||y==n+1){
		return fanhui=1;
	}
	fanhui=0;
	if(a[x]==b[y]){
		if(a[x+1]==b[y+1]){
			fanhui+=(dp(x+1,y))%mod;
		}else{
			fanhui+=((dp(x+1,y)+dp(x,y+1))%mod-dp(x+1,y+1)+mod)%mod;
		}
		fanhui=fanhui%mod;
	}else{
		fanhui+=(dp(x+1,y)+dp(x,y+1))%mod;
		fanhui=fanhui%mod;
	}
	return fanhui;
}
int main(){
	fin>>n;
	if(n==1){
		fout<<"1"<<endl;
		return 0;
	}
	for(int i=1;i<=n;i++){
		fin>>a[i];
	}
	for(int i=1;i<=n;i++){
		fin>>b[i];
	}
	memset(jiyi,-1,sizeof(jiyi));
	long long ans=dp(1,1);
	fout<<ans<<endl;
	return 0;
}
/*
in:
10
5 7 3 1 6 4 2 10 9 8
2 8 9 1 5 6 10 4 3 7
out:
127224

3
1 2 3
1 3 2

in:
4
1 2 3 4
1 2 3 4
out:

*/
