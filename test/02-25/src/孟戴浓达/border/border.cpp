//#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<fstream>
using namespace std;
ifstream fin("border.in");
ofstream fout("border.out");
const int mod=34651094;
long long n,q;
char ch[200003];
long long has1[200003],has2[200003],q1[200003],q2[200003];
inline long long hash1(int x,int y){
	
	return has1[y]-has1[x-1]*q1[y-x+1]%mod;
}
inline int solve(int x,int y){
	int l=0,r=y-x;
	while(l<r-1){
		int mid=(l+r)/2;
		if(hash1(x,x+mid-1)==hash1(y-mid+1,y)){
			l=mid;
		}else{
			r=mid-1;
		}
	}
	if(hash1(x,x+r-1)==hash1(y-r+1,y)){
		return r;
	}else{
		return l;
	}
}
int P[200003];
char B[200003];
inline void solve(){
	int l,r,j;
	while(q--){
		j=0;
		fin>>l>>r;
		for(int i=l;i<=r;i++){
			B[i-l+1]=ch[i];
			P[i-l+1]=0;
		}
		for(int i=2;i<=r-l+1;i++){
			while(j&&B[j+1]!=B[i]) j=P[j];
			j+=(B[j+1]==B[i]);
			P[i]=j;
		}
		fout<<P[r-l+1]<<endl;
	}
}
int main(){
	fin>>n>>q;
	for(int i=1;i<=n;i++){
		fin>>ch[i];
	}
	if(n*q<=5000000000){
		solve();
		return 0;
	}
	has1[0]=q1[0]=1;
	for(int i=1;i<=n;i++){
		q1[i]=q1[i-1]*31%mod;
		has1[i]=(has1[i-1]*31+ch[i]-'a')%mod;
	}
	has2[n+1]=q2[n+1]=1;
	for(int i=n;i>=1;i--){
		q2[i]=q2[i+1]*31%mod;
		has2[i]=(has2[i+1]*31+ch[i]-'a')%mod;
	}
	while(q--){
		int l,r;
		fin>>l>>r;
		fout<<solve(l,r)<<endl;
	}
	return 0;
}
/*

in:
11 3
gabacababad
2 8
1 3
6 10

out:
3
0
3

*/
