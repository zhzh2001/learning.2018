//#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<fstream>
using namespace std;
ifstream fin("xor.in");
ofstream fout("xor.out");
const int mod=1000000007;
int n,m;
char opt;
int jiyi[513][513][302];
int dp(int x,int y,int now){
	int &fanhui=jiyi[x][y][now];
	if(fanhui!=-1) return fanhui;
	if(now==0){
		if(opt=='>'&&x>y)  return fanhui=1;
		if(opt=='='&&x==y) return fanhui=1;
		if(opt=='<'&&x<y)  return fanhui=1;
		return fanhui=0;
	}
	fanhui=dp(x,y,now-1);
	if(n>=now) fanhui=(fanhui+dp(x^now,y,now-1))%mod;
	if(m>=now) fanhui=(fanhui+dp(x,y^now,now-1))%mod;
	return fanhui;
}
long long jiyi2[2051][2003];
long long dp2(int x,int now){
	long long &fanhui=jiyi2[x][now];
	if(fanhui!=-1) return fanhui;
	if(now==0){
		if(x==0) return fanhui=1;
		return fanhui=0;
	}
	fanhui=dp2(x,now-1);
	if(n>=now) fanhui=(fanhui+dp2(x^now,now-1))%mod;
	if(m>=now) fanhui=(fanhui+dp2(x^now,now-1))%mod;
	return fanhui;
}
long long qpow(int a,int b){
	if(b==1){
		return a;
	}
	long long fanhui=qpow(a,b/2);
	fanhui=fanhui*fanhui%mod;
	if(b%2==1){
		fanhui=fanhui*a%mod;
	}
	return fanhui;
}
int main(){
	fin>>n>>m>>opt;
	if(n==m){
		memset(jiyi2,-1,sizeof(jiyi2));
		fout<<(qpow(3,n)-dp2(0,n)+mod)%mod*qpow(2,mod-2)%mod<<endl;
		return 0;
	}
	if(opt=='='){
		memset(jiyi2,-1,sizeof(jiyi2));
		fout<<dp2(0,max(n,m))<<endl;
		return 0;
	}
	if(n<=300&&m<=300){
		memset(jiyi,-1,sizeof(jiyi));
		fout<<dp(0,0,max(n,m))<<endl;
		return 0;
	}
	return 0;
}
/*
in:
100 100 >
out:
777185814

in:
3 5 <
out:
74

in:
10 20 =
out:
1889568

*/
