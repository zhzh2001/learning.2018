#include<bits/stdc++.h>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 200005
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n,Q,nxt[N];
char ch[N];
inline void solve(int l,int r){
	int tot=r-l+1;
	nxt[1]=0;int j=0;
	For(i,2,tot){
		while(j&&ch[j+l]!=ch[i+l-1]) j=nxt[j];
		if(ch[j+l]==ch[i+l-1]) j++;nxt[i]=j;
	}
}
struct que{
	int l,r,id;
	bool operator <(const que &a)const{
		return (l==a.l)?(r>a.r):l<a.l;
	}
}q[N];
int ans[N];
int main(){
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	n=read();Q=read();
	scanf("%s",ch+1);
	For(i,1,Q) q[i]=(que){read(),read(),i};
	sort(q+1,q+1+Q);
	For(i,1,Q){
		if(q[i].l!=q[i-1].l) solve(q[i].l,q[i].r),ans[q[i].id]=nxt[q[i].r-q[i].l+1];
		else ans[q[i].id]=nxt[q[i].r-q[i].l+1];
	}
	For(i,1,Q) printf("%d\n",ans[i]);
	return 0;
}
