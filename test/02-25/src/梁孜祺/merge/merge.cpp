#include<bits/stdc++.h>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 2005
#define mod 1000000007
using namespace std;
int n,a[N],b[N],ans,g[N][N];
vector <int> A;
map <vector<int>,int> mp;
inline void dfs(int x,int y){
	if(x==n&&y==n){
		if(!mp[A]) mp[A]=1,ans++;
		return;
	}
	if(x<n){
		A.push_back(a[x+1]);
		dfs(x+1,y);
		A.pop_back();
	}
	if(y<n){
		A.push_back(b[y+1]);
		dfs(x,y+1);
		A.pop_back();
	}
}
inline void solve(){
	g[0][0]=1;
	For(i,0,n) For(j,0,n){
		if(i==0&&j==0) continue;
		else if(!j) g[i][j]=g[i-1][j];
		else if(!i) g[i][j]=g[i][j-1];
		else if(a[i]!=b[j]) g[i][j]=(g[i-1][j]+g[i][j-1])%mod;
		else g[i][j]=(((g[i-1][j]+g[i][j-1]-g[i-1][j-1])%mod)+mod)%mod;
	}
	printf("%d\n",g[n][n]);
}
int main(){
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n) scanf("%d",&a[i]);
	For(i,1,n) scanf("%d",&b[i]);
	if(n<=10) dfs(0,0);
	else solve();
	printf("%d\n",ans);
	return 0;
}
