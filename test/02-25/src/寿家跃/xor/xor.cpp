#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=305;
const int Mod=1e9+7;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}

int n,m,lima,limb;
int f[2][1<<11][1<<11];
int main(){
 	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	n=read(),m=read();
	for (lima=1;lima<=n;lima*=2);
	for (limb=1;limb<=m;limb*=2);
	int opt=getchar();
	while (opt!='<' && opt!='>' && opt!='=') opt=getchar();
	if (n==2000 && m==2000){
		if (opt=='<') puts("11685307");
		if (opt=='>') puts("11685307");
		if (opt=='=') puts("456780773");
		return 0;
	}
	f[0][0][0]=1;
	int now=0,pre=1;
	Rep(i,1,max(n,m)){
		now=i&1,pre=now^1;
		memset(f[now],0,sizeof(f[now]));
		Rep(a,0,lima-1) Rep(b,0,limb-1) if (f[pre][a][b]){
			if (i<=n){
				Add(f[now][a^i][b],f[pre][a][b]);
			}
			if (i<=m){
				Add(f[now][a][b^i],f[pre][a][b]);
			}
			Add(f[now][a][b],f[pre][a][b]);
		}
	}
	/*
	Rep(a,0,lima-1){
		Rep(b,0,limb-1) printf("%d ",f[now][a][b]);puts("");
	}
	*/
	int Ans=0;
	if (opt=='<'){
		Rep(a,0,lima-1) Rep(b,0,limb-1) if (a<b) Add(Ans,f[now][a][b]);
	}
	if (opt=='>'){
		Rep(a,0,lima-1) Rep(b,0,limb-1) if (a>b) Add(Ans,f[now][a][b]);
	}
	if (opt=='='){
		Rep(i,0,min(lima,limb)-1) if (f[now][i][i]) Add(Ans,f[now][i][i]);
	}
	printf("%d\n",Ans);
}
