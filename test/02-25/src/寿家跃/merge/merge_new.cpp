#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=105;
const int Mod=1e9+7;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}

int n,d[2][N],Lcp[N][N];
int f[N][N][N][2];

int main(){
	n=read();
	Rep(i,1,n) d[0][i]=read();
	Rep(i,1,n) d[1][i]=read();
	Rep(x,1,n) Rep(y,1,n){
		int res=0;
		while (x+res<=n && y+res<=n && d[0][x+res]==d[1][y+res]) res++;
		Lcp[x][y]=res;
	}
	
	f[0][0][0][0]=1;
	f[0][0][0][1]=1;
	Rep(x,0,n) Rep(y,0,n) Rep(laslen,0,n) Rep(laspos,0,1){
		if (!f[x][y][laslen][laspos]) continue;
		int nowpos=laspos^1;
		if (nowpos==0){
			Rep(len,1,n-x){
				if (len>=laslen && Lcp[x+1][y+1-laslen]>=len) continue;
				Add(f[x+len][y][len][nowpos],f[x][y][laslen][laspos]);
			}
		}
		if (nowpos==1){
			Rep(len,1,n-y){
				Add(f[x][y+len][len][nowpos],f[x][y][laslen][laspos]);
			}
		}
	}
	int Ans=0;
	Rep(laslen,1,n) Rep(laspos,0,1){
		Add(Ans,f[n][n][laslen][laspos]);
//		printf("laslen=%d laspos=%d 
	}
	printf("%d\n",Ans);
}
