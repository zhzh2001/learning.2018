#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>
#include<string>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=2005;
const int Mod=1e9+7;

inline int Pow(int base,int k){
	int res=1;
	for (int i=1;i<=k;i*=2,base=1ll*base*base%Mod){
		if (i&k) res=1ll*res*base%Mod;
	}
	return res;
}

vector<string>V;

int n,a[N],b[N];
void Dfs(int x,int y,string s){
//	printf("x=%d y=%d s=%s\n",x,y,s.c_str());
	if (x>n && y>n){
//		printf("%s\n",s.c_str());
		V.push_back(s);return;
	}
	if (x<=n) Dfs(x+1,y,s+(char)a[x]);
	if (y<=n) Dfs(x,y+1,s+(char)b[y]);
}
inline int C(int x,int y){
	int res=1;
	Rep(i,x-y+1,x) res=1ll*res*i%Mod;
	Rep(i,1,y) res=1ll*res*Pow(i,Mod-2)%Mod;
	return res;
}
int main(){
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	n=read();
	Rep(i,1,n) a[i]=read();
	Rep(i,1,n) b[i]=read();
	bool Allsame=true;
	Rep(i,1,n) if (a[i]!=b[i]) Allsame=false;
	if (Allsame){
		printf("%d\n",1ll*C(2*n,n)*Pow(n+1,Mod-2)%Mod);return 0;
	}
	if (n<=10){
		Dfs(1,1,"");
		sort(V.begin(),V.end());
		V.push_back("!");
		int Ans=0;
		Rep(i,0,V.size()-2) if (V[i]!=V[i+1]) Ans++;
		printf("%d\n",Ans);
	}
}
/*
1 2 5 14 42 132 429 1430
1
1 1
1 2 1
1 3 3 1
1 4 6 4 1
1 5 10 10 5 1
2=1+1
5=2*1+1*3
14=5*1+2*3+1*3
42=14*1+5*4
3 3
1 2 3
1 2 3
f[0]=0;
f[1]=1;
f[2]=C(0,0)*Pow(2,2)+C(2,1)*Pow(2,1)
*/
