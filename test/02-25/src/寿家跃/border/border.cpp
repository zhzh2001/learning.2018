#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=200005;

int n,q,S[N],nex[N];

int Kmp(int len,int *S){
	nex[1]=0;
	Rep(i,2,len){
		int p=nex[i-1];
		while (p>0 && S[p+1]!=S[i]) p=nex[p];
		if (S[p+1]==S[i]) p++;
		nex[i]=p;
	}
	return nex[len];
}
int Dat[N];
int main(){
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	n=read(),q=read();
	Rep(i,1,n){
		int ch=getchar();
		while (ch<'a' || ch>'z') ch=getchar();
		S[i]=ch;
	}
	Rep(i,1,q){
		int l=read(),r=read();
		*Dat=0;
		Rep(j,l,r) Dat[++*Dat]=S[j];
		printf("%d\n",Kmp(*Dat,Dat));
	}
}
