#include<bits/stdc++.h>
#define ll long long
#define mod 1000000007
#define N 2005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,flag=1,a[N],b[N],f[N][N];
int main(){
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=read();
	rep(i,1,n) b[i]=read();
	rep(i,0,n) f[i][0]=f[0][i]=1;
	rep(i,1,n) rep(j,1,n){
		if (i==j&&a[i]!=b[j]) flag=0;
		if (a[i]==b[j]){
			if (i==j&&flag) f[i][j]=f[i-1][j];
			else f[i][j]=((f[i-1][j]+f[i][j-1]-f[i-1][j-1])%mod+mod)%mod;
		}
		else f[i][j]=(f[i-1][j]+f[i][j-1])%mod;
	}
	printf("%lld",f[n][n]);
}
