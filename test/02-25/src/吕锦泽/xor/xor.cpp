#include<bits/stdc++.h>
#define ll int
#define mod 1000000007
#define N 10005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
char s[N]; ll a,b,n,m,maxn,ans;
ll judge(ll a,ll b){
	if (s[0]=='<') return a<b;
	if (s[0]=='=') return a==b;
	if (s[0]=='>') return a>b;
}
void dfs(ll now){
	if (now>maxn){
		ans+=judge(a,b);
		ans%=mod;
		return;
	}
	if (now<=n){
		a^=now;
		dfs(now+1);
		a^=now;
	}
	if (now<=m){
		b^=now;
		dfs(now+1);
		b^=now;
	}
	dfs(now+1);
}
int main(){
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	n=read(); m=read(); scanf("%s",s);
	maxn=max(n,m); dfs(1);
	printf("%d",ans);
}
