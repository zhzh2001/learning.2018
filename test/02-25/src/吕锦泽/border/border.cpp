#include<bits/stdc++.h>
#define ll int
#define mod 1000000007
#define N 200005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
char s[N]; ll n,m,nxt[N];
int main(){
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	n=read(); m=read();
	scanf("%s",s+1);
	while (m--){
		ll l=read(),r=read();
		for (ll i=1,j=0;i<=r-l+1;++i){
			while (j&&s[l+i-1]!=s[l+j]) j=nxt[j];
			if (j+1<i&&s[l+i-1]==s[l+j]) ++j; nxt[i]=j;
		}
		printf("%d\n",nxt[r-l+1]);
	}
}
