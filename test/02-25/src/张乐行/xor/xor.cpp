#include<cstdio>
#include<algorithm>
using namespace std;
const int N=2048,mo=1e9+7;
int n,m,nk,f[N][2],ff[N][2],ans;
char sc[5],opt;
void add(int &x,int k){x=(x+k)%mo;}
void solve(int w){
	int i,j,k;
	for (i=0;i<nk;i++) f[i][0]=f[i][1]=0;
	f[0][0]=1;
	for (i=1;i<=m;i++){
		int v=(i&w)>0;
		for (j=0;j<nk;j++) for (k=0;k<2;k++)
			ff[j][k]=f[j][k];
		for (j=0;j<nk;j++) for (k=0;k<2;k++){
			if (i<=n) add(f[j^i][k^v],ff[j][k]);
			add(f[j^i][k],ff[j][k]);
		}
	}
}
int main(){
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	scanf("%d%d%s",&n,&m,sc);opt=sc[0];
	if (n>m){
		swap(n,m);
		if (opt=='<') opt='>';
		else if (opt=='>') opt=='<';
	}
	for (nk=1;nk<=m;nk<<=1);
	if (opt=='='){
		solve(1);
		printf("%d",(f[0][0]+f[0][1])%mo);
		return 0;
	}
	for (int k=1;k<=m;k<<=1){
		solve(k);
		for (int i=k;i<k*2;i++)
			add(ans,(opt=='<'?f[i][0]:f[i][1]));
	}
	printf("%d",ans);
}
