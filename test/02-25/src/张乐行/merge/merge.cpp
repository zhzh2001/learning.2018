#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=2005,mo=1e9+7;
int n,i,j,a[N],b[N],f[N][N],g[N][N];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
int cal(int xa,int ya,int xb){
	if (xa==xb) return f[xa][ya];
	if (g[xa][ya]!=-1) return g[xa][ya];
	int yb=xa+ya-xb,res=0;
	if (a[xa]==b[yb]) res=(res+cal(xa-1,ya,xb));
	if (b[ya]==a[xb]) res=(res+cal(xa,ya-1,xb-1));
	return g[xa][ya]=res;
}
int main(){
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	for (n=read(),i=1;i<=n;i++) a[i]=read();
	for (b[0]=-1,i=1;i<=n;i++) b[i]=read();
	memset(g,-1,sizeof(g));
	for (i=0;i<=n;i++) f[0][i]=1;
	for (i=1;i<=n;i++){
		f[i][0]=1;
		for (j=1;j<=n;j++){
			f[i][j]=(f[i-1][j]+f[i][j-1])%mo;
			if (a[i]==b[j])
				f[i][j]=(f[i][j]-cal(i-1,j,i))%mo;
		}
	}
	printf("%d",(f[n][n]+mo)%mo);
}
