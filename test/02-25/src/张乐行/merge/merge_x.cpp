#include<cstdio>
#include<algorithm>
using namespace std;
const int N=2005,mo=1e9+7;
int n,i,j,a[N],b[N],f[N][N];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
int main(){
	freopen("merge_xiao.in","r",stdin);
//	freopen("merge.out","w",stdout);
	for (n=read(),i=1;i<=n;i++) a[i]=read();
	for (b[0]=-1,i=1;i<=n;i++) b[i]=read();
	for (i=0;i<=n;i++) f[0][i]=1;
	for (i=1;i<=n;i++){
		f[i][0]=1;
		for (j=1;j<=n;j++){
			f[i][j]=(f[i-1][j]+f[i][j-1])%mo;
			if (a[i]==b[j]){
				f[i][j]=(f[i][j]-f[i-1][j-1])%mo;
				if (a[i-1]==b[j-1])
					f[i][j]=(f[i][j]-f[i-2][j])%mo;
			}
		}
	}
	printf("%d",(f[n][n]+mo)%mo);
}
