#include<cstdio>
#include<algorithm>
using namespace std;
const int N=2e5+5,LG=20;
int n,Q,i,j,k,a[N];char s[N];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int rk[N],lg[N],mn[N][LG];
struct Suffix_Array{
	int i,j,k,p,v[N],s[2][N],r[2][N];
	void Get_height(int *s,int *r){
		for (i=1,k=0;i<=n;i++){
			for (;a[i+k]==a[s[r[i]-1]+k];k++);
			mn[r[i]][0]=k;if (k) k--;
		}
	}
	void radix(int *sa,int *ra,int *sb,int *rb){
		for (i=1;i<=n;i++) v[ra[sa[i]]]=i;
		for (i=n;i;i--) if (sa[i]-k>0)
			sb[v[ra[sa[i]-k]]--]=sa[i]-k;
		for (i=n-k+1;i<=n;i++) sb[v[ra[i]]--]=i;
		for (i=1;i<=n;i++) rb[sb[i]]=rb[sb[i-1]]+
			(ra[sb[i]]!=ra[sb[i-1]]||ra[sb[i]+k]!=ra[sb[i-1]+k]);
	}
	void suffix(){
		a[0]=a[n+1]=-1;
		for (i=1;i<=n;i++) v[a[i]]++;
		for (i=1;i<26;i++) v[i]+=v[i-1];
		for (p=0,i=1;i<=n;i++) s[p][v[a[i]]--]=i;
		for (i=1;i<=n;i++) r[p][s[p][i]]=
			r[p][s[p][i-1]]+(a[s[p][i]]!=a[s[p][i-1]]);
		for (k=1;k<n;k<<=1,p^=1)
			radix(s[p],r[p],s[p^1],r[p^1]);
		for (i=1;i<=n;i++) rk[i]=r[p][i];
		Get_height(s[p],r[p]);
	}
}SA;
void Build_ST(){
	for (lg[0]=-1,i=1;i<=n;i++) lg[i]=lg[i>>1]+1;
	for (j=1,k=1;(k<<1)<=n;j++,k<<=1)
		for (i=1;i<=n-(k<<1)+1;i++)
			mn[i][j]=min(mn[i][j-1],mn[i+k][j-1]);
}
int lcp(int x,int y){
	x=rk[x];y=rk[y];
	if (x>y) swap(x,y);x++;
	int v=lg[y-x+1],k=1<<v;
	return min(mn[x][v],mn[y-k+1][v]);
}
int main(){
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	n=read();Q=read();scanf("%s",s);
	for (i=0;i<n;i++) a[i+1]=s[i]-97;
	SA.suffix();Build_ST();
	for (;Q--;){
		int l=read(),r=read();
		for (i=l+1;i<=r&&lcp(l,i)<r-i+1;i++);
		write(r-i+1);putchar('\n');
	}
}
