#include <cstdio>
#include <map>

using namespace std;

const int p=1000000007;

map < pair <long long,long long> ,bool> M;

int c[5000][5000],u[5000],v[5000],z[5000];
int i,j,n,s;

inline void dfs(int x,int y)
{
	if ((x==n+1) && (y==n+1))
	{
		long long z1=0,z2=0;
		for (int i=1;i<=n;i++)
			z1=z1*10+(z[i]-1);
		for (int i=n+1;i<=n+n;i++)
			z2=z2*10+(z[i]-1);
		if (! M[make_pair(z1,z2)])
			s++,M[make_pair(z1,z2)]=true;
	}
	if (x<=n)
	{
		z[x+y-1]=u[x];
		dfs(x+1,y);
	}
	if (y<=n)
	{
		z[x+y-1]=v[y];
		dfs(x,y+1);
	}
	return;
}

inline int power(int x,int y)
{
	int s=1;
	for (int i=30;i>=0;i--)
	{
		s=1LL*s*s%p;
		if ((1<<i)&y)
			s=1LL*s*x%p;
	}
	return s;
}

int main()
{
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	scanf("%d",&n);
	if (n<=10)
	{
		for (i=1;i<=n;i++)
			scanf("%d",&u[i]);
		for (i=1;i<=n;i++)
			scanf("%d",&v[i]);
		dfs(1,1);
		printf("%d",s);
	}
	else
	{
		for (i=0;i<=2*n;i++)
			for (j=0;j<=i;j++)
				if ((j==0) || (j==i))
					c[i][j]=1;
				else
					c[i][j]=(c[i-1][j]+c[i-1][j-1])%p;
		printf("%d",1LL*c[2*n][n]*power(n+1,p-2)%p);
	}
	return 0;
}
