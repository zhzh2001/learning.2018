#include <cstdio>
#include <algorithm>

using namespace std;

int sa[500000],rk[500000],hei[500000],st[500000][20];
int w[500000],x[500000],y[500000],z[500000];
char c[500000];
int i,j,k,l,m,n,t,u,v;

inline void input()
{
	scanf("%d%d",&n,&m);
	scanf("%s",c+1);
	return;
}

inline void buildSA()
{
	for (i=1;i<=n;i++) y[c[i]]++;
	for (i=97;i<=122;i++) y[i]=y[i]+y[i-1];
	for (i=n;i>0;i--) sa[y[c[i]]--]=i;
	for (i=1,t=0;i<=n;i++) if (c[sa[i]]==c[sa[i-1]]) rk[sa[i]]=t; else rk[sa[i]]=++t;
	for (i=1;i<=n;i++) z[i]=rk[i];
	for (k=1;t<n;k=k<<1)
	{
		for (i=n-k+1,l=0;i<=n;i++) x[++l]=i;
		for (i=1;i<=n;i++) if (sa[i]>k) x[++l]=sa[i]-k;
		for (i=1;i<=n;i++) y[i]=0;
		for (i=1;i<=n;i++) y[z[x[i]]]++;
		for (i=1;i<=n;i++) y[i]=y[i-1]+y[i];
		for (i=n;i>0;i--) sa[y[z[x[i]]]--]=x[i];
		for (i=1,t=0;i<=n;i++) if ((z[sa[i]]==z[sa[i-1]]) && (z[sa[i]+k]==z[sa[i-1]+k])) rk[sa[i]]=t; else rk[sa[i]]=++t;
		for (i=1;i<=n;i++) z[i]=rk[i];
	}
	return;
}

inline void getheight()
{
	for (i=1,k=0;i<=n;i++)
	{
		k=max(0,k-1);
		for (;c[i+k]==c[sa[rk[i]-1]+k];k++);
		hei[rk[i]]=k;
	}
	return;
}

inline void buildST()
{
	for (i=2;i<=n;i++)
		w[i]=w[i>>1]+1;
	for (i=n;i>0;i--)
	{
		st[i][0]=hei[i+1];
		for (j=1;i+(1<<j)<=n;j++)
			st[i][j]=min(st[i][j-1],st[i+(1<<(j-1))][j-1]);
	}
	return;
}

inline int LCP(int x,int y)
{
	if (x==y)
		return n-x+1;
	x=rk[x],y=rk[y];
	if (x>y)
		swap(x,y);
	return min(st[x][w[y-x]],st[y-(1<<w[y-x])][w[y-x]]);
}

struct treenode
{
	int add,max;
};

treenode tree[2000000];

inline void pushdown(int node)
{
	if (tree[node].add)
	{
		tree[node].max=tree[node].max+tree[node].add;
		tree[node<<1].add=tree[node<<1].add+tree[node].add;
		tree[node<<1|1].add=tree[node<<1|1].add+tree[node].add;
		tree[node].add=0;
	}
	return;
}

inline void pushup(int node)
{
	pushdown(node<<1);
	pushdown(node<<1|1);
	tree[node].max=max(tree[node<<1].max,tree[node<<1|1].max);
	return;
}

inline void inserttree(int l,int r,int x,int y,int z,int node)
{
	pushdown(node);
	if ((l==x) && (r==y))
		tree[node].add=tree[node].add+z;
	else
	{
		int mid=(l+r)>>1;
		if (y<=mid)
			inserttree(l,mid,x,y,z,node<<1);
		else
			if (x>mid)
				inserttree(mid+1,r,x,y,z,node<<1|1);
			else
			{
				inserttree(l,mid,x,mid,z,node<<1);
				inserttree(mid+1,r,mid+1,y,z,node<<1|1);
			}
		pushup(node);
	}
	return;
}

inline int asktree(int l,int r,int x,int node)
{
	pushdown(node);
	if (tree[node].max<x)
		return n+1;
	if (l==r)
		return l;
	int mid=(l+r)>>1;
	if (tree[node<<1].max>=x)
		return asktree(l,mid,x,node<<1);
	else
		return asktree(mid+1,r,x,node<<1|1);
}

struct query
{
	int l,r,o;
	
	inline bool operator < (const query t) const
	{
		return rk[l]>rk[t.l];
	}
};

query q[500000];
int o[500000];

inline void output()
{
	for (i=1;i<=m;i++)
	{
		scanf("%d%d",&u,&v);
		t=0;
		for (j=max(u+1,v-1500);j<=v;j++)
			if (LCP(u,j)>=v-j+1)
			{
				t=v-j+1;
				break;
			}
		printf("%d\n",t);
	}
/*	for (i=1;i<=m;i++)
		scanf("%d%d",&q[i].l,&q[i].r),q[i].o=i;
	sort(q+1,q+m+1);
	for (k=1;(k<=m) && (q[k].l==n);k++);
	for (i=n-1;i>0;i--)
	{
		u=v=i+1;
		for (;(t>0) && (hei[i+1]<hei[o[t]]);t--)
			inserttree(1,n,x[o[t]],y[o[t]],hei[i]-hei[o[t]],1),v=y[o[t]];
		t++,o[t]=i+1,x[o[t]]=u,y[o[t]]=v;
		inserttree(1,n,x[o[t]],x[o[t]],hei[i]+sa[i+1]+1,1);
		for (;(k<=m) && (rk[q[k].l]==i);k++)
			w[q[k].o]=max(0,q[k].r-asktree(1,n,q[k].r+1,1)+1);
	}
	for (i=1;i<=m;i++)
		printf("%d\n",w[i]);*/
	return;
}

int main()
{
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	input();
	buildSA();
	getheight();
	buildST();
	output();
	return 0;
}
