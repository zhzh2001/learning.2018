#include <cstdio>
#include <algorithm>

using namespace std;

const int p=1000000007;

int e[2048][2048],f[2048][2048],h[2048][2048];
int i,j,k,l,n1,n2,s,t;
char c;

inline void subtask3()
{
	f[0][0]=1;
	for (i=1;i<=n1;i++)
		for (j=0;j<2048;j++)
			f[i][j]=(f[i-1][j]+2LL*f[i-1][j^i])%p;
	for (i=n1+1;i<=n2;i++)
		for (j=0;j<2048;j++)
			f[i][j]=(f[i-1][j]+f[i-1][j^i])%p;
	if (c=='=')
		printf("%d",f[n2][0]);
	else
	{
		for (i=1;i<2048;i++)
			s=(s+f[n2][i])%p;
		printf("%d",500000004LL*s%p);
	}
	return;
}

inline void FWT_XOR(int *T,int t)
{
	int x,y;
	for (int i=1;i<l;i=i<<1)
		for (int j=0;j<l;j=j+(i<<1))
			for (int k=0;k<i;k++)
			{
				x=T[j+k],y=T[i+j+k];
				if (t) x=x*500000004LL%p,y=y*500000004LL%p;
				T[j+k]=(x+y)%p,T[i+j+k]=(x-y+p)%p;
			}
	return;
}

inline void subtask2()
{
	f[0][0]=1;
	for (i=1;i<=n1;i++)
	{
		for (l=1;l<=i;l=l<<1);
		for (j=0;j<l;j++)
			for (k=0;k<l;k++)
				h[j][k]=f[j][k];
		for (j=0;j<l;j++)
			for (k=0;k<l;k++)
				f[j][k]=(f[j][k]+(h[j^i][k]+h[j][k^i])%p)%p;
	}
	t=l;
	e[n1][0]=1;
	for (i=n1+1;i<=n2;i++)
		for (j=0;j<2048;j++)
			e[i][j]=(e[i-1][j]+e[i-1][j^i])%p;
	for (l=1;l<=n2;l=l<<1);
	FWT_XOR(e[n2],0);
	for (j=0;j<t;j++)
		FWT_XOR(f[j],0);
	for (j=0;j<t;j++)
		for (k=0;k<l;k++)
			f[j][k]=1LL*f[j][k]*e[n2][k]%p;
	for (j=0;j<t;j++)
		FWT_XOR(f[j],1);
	for (j=0;j<2048;j++)
		for (k=0;k<2048;k++)
		{
			if ((c=='<') && (j<k))
				s=(s+f[j][k])%p;
			if ((c=='>') && (j>k))
				s=(s+f[j][k])%p;
		}
	printf("%d",s);
	return;
}

int main()
{
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	scanf("%d%d",&n1,&n2);
	getchar(),c=getchar();
	if (n1>n2)
	{
		swap(n1,n2);
		if (c!='=')
			if (c=='>')
				c='<';
			else
				c='>';
	}
	if ((c=='=') || (n1==n2))
		subtask3();
	else
		subtask2();
	return 0;
}
