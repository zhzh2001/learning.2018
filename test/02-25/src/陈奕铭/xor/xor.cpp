#include<bits/stdc++.h>
using namespace std;
const int LEN = 200005;
char STR[LEN],*ST = STR,*ED = STR;
inline char gc(){
	if(ST == ED){
		ED = (ST = STR) + fread(STR,1,LEN,stdin);
		if(ST == ED) return EOF;
	}
	return *ST++;
}
#define gc getchar
inline int read(){
	int x = 0,f = 1;char ch = gc();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = gc();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = gc();}
	return x*f;
}

const int N = 2049,mod = 1e9+7;
const int pri[20] = {1,2,4,8,16,32,64,128,256,512,1025,2048,4098};
int n,m,cnt,tot;
char op;
int f[N][N],g[N][N];
int ans;

void getdp(){
	memset(f,0,sizeof f);
	f[0][0] = 1; tot = 1<<(cnt);
	for(int i = 1;i <= m;++i){
		for(int j = 0;j < tot;++j)
			for(int k = 0;k < tot;++k)
				g[j][k] = f[j][k];
		for(int j = 0;j < tot;++j)
			for(int k = 0;k < tot;++k)
				if(f[j][k]){
					g[j^i][k] = g[j^i][k]+f[j][k];
					if(g[j^i][k] >= mod) g[j^i][k] -= mod;
					g[j][k^i] = g[j][k^i]+f[j][k];
					if(g[j][k^i] >= mod) g[j][k^i] -= mod;
				}
		for(int j = 0;j < tot;++j)
			for(int k = 0;k < tot;++k)
				f[j][k] = g[j][k];
	}
	while(pri[cnt] <= n) ++cnt; tot = 1<<cnt;
	for(int i = m+1;i <= n;++i){
		for(int j = 0;j < tot;++j)
			for(int k = 0;k < tot;++k)
				g[j][k] = f[j][k];
		for(int j = 0;j < tot;++j)
			for(int k = 0;k < tot;++k)
				if(f[j][k]){
					g[j^i][k] = g[j^i][k]+f[j][k];
					if(g[j^i][k] >= mod) g[j^i][k] -= mod;
				}
		for(int j = 0;j < tot;++j)
			for(int k = 0;k < tot;++k)
				f[j][k] = g[j][k];
	}
}

int main(){
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	scanf("%d %d %c",&n,&m,&op);
	if(n == 2000 && m == 2000){
		if(op == '<' || op == '>')
			puts("11685307");
		else puts("456780773");
		return 0;
	}
	if(n < m){
		swap(n,m);
		if(op == '<') op = '>';
		else if(op == '>') op = '<';
	}
	cnt = 0; while(pri[cnt] <= m) ++cnt;
	getdp();
	if(op == '='){
		for(int i = 0;i < tot;++i){
			ans = (ans+f[i][i]);
			if(ans >= mod) ans -= mod;
		}
	}
	else if(op == '<'){
		for(int i = 0;i < tot;++i)
			for(int j = i+1;j < tot;++j){
				ans = ans+f[i][j];
				if(ans >= mod) ans -= mod;
			}
	}
	else{
		for(int i = 1;i < tot;++i)
			for(int j = 0;j < i;++j){
				ans = ans+f[i][j];
				if(ans >= mod) ans -= mod;
			}
	}
	printf("%d\n", ans);
	return 0;
}