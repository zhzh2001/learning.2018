#include<bits/stdc++.h>
using namespace std;
const int LEN = 200005;
char STR[LEN],*ST = STR,*ED = STR;
inline char gc(){
	if(ST == ED){
		ED = (ST = STR) + fread(STR,1,LEN,stdin);
		if(ST == ED) return EOF;
	}
	return *ST++;
}
#define gc getchar
inline int read(){
	int x = 0,f = 1;char ch = gc();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = gc();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = gc();}
	return x*f;
}

const int N = 2005,mod = 1e9+7;
int a[N],b[N];
int n;
int dp[N][N];
struct node{
	int x[21];
	friend bool operator <(node a,node b){
		for(int i = 1;i <= n*2;++i){
			if(a.x[i] < b.x[i])
				return true;
			else if(a.x[i] > b.x[i])
				return false;
		}
		return false;
	}
}p;
map<node,bool> mp;

void dfs(int x,int y){
	if(x == n+1){
		while(y <= n) p.x[x+y-1] = b[y],y++;
		mp[p] = true;
		return;
	}
	if(y == n+1){
		while(x <= n) p.x[x+y-1] = a[x],x++;
		mp[p] = true;
		return;
	}
	p.x[x+y-1] = a[x];
	dfs(x+1,y);
	p.x[x+y-1] = b[y];
	dfs(x,y+1);
}

inline bool pd(){
	for(int i = 1;i <= n;++i)
		if(a[i] != b[i])
			return false;
	return true;
	// return false;
}

void solvesmae(){
	for(int i = 1;i <= n;++i)
		for(int j = 1;j <= n;++j){
			if(a[i] != b[j])
				dp[i][j] = (dp[i-1][j]+dp[i][j-1])%mod;
			else dp[i][j] = dp[i-1][j];
		}
	printf("%d\n", dp[n][n]);
}

int main(){
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;++i) a[i] = read();
	for(int i = 1;i <= n;++i) b[i] = read();
	dp[0][0] = 1;
	for(int i = 1;i <= n;++i)
		dp[i][0] = dp[0][i] = 1;
	if(pd()) solvesmae();
	else if(n <= 10){
		dfs(1,1);
		printf("%lld\n",mp.size()%mod);
	}
	else{
		for(int i = 1;i <= n;++i)
			for(int j = 1;j <= n;++j){
					dp[i][j] = (dp[i-1][j]+dp[i][j-1])%mod;
				if(a[i] == b[j]){
					int x = 1;
					for(;i-x > 0 && j-x > 0 && a[i-x] == b[j-x];++x);
					dp[i][j] = ((dp[i][j]-dp[i-1][j-1])%mod+mod)%mod;
					if(x > 1){
						for(int I = 2;I <= x;++I)
							dp[i][j] = ((dp[i][j]-dp[i-1][j-I]-dp[i-I][j-1])%mod+mod)%mod;
						dp[i][j] = (dp[i][j]+dp[i-2][j-2])%mod;
						for(int I = 3;I <= x;++I)
							dp[i][j] = (dp[i][j]+dp[i-2][j-I]+dp[i-I][j-2])%mod;
					}
				}
				// printf("%d ",dp[i][j]);
			}
		printf("%d\n",dp[n][n]);
	}
	return 0;
}