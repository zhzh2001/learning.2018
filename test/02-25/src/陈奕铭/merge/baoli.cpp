#include<bits/stdc++.h>
using namespace std;
const int LEN = 200005;
char STR[LEN],*ST = STR,*ED = STR;
inline char gc(){
	if(ST == ED){
		ED = (ST = STR) + fread(STR,1,LEN,stdin);
		if(ST == ED) return EOF;
	}
	return *ST++;
}
#define gc getchar
inline int read(){
	int x = 0,f = 1;char ch = gc();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = gc();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = gc();}
	return x*f;
}

const int N = 2005,mod = 1e9+7;
int n;
struct node{
	int x[21];
	friend bool operator <(node a,node b){
		for(int i = 1;i <= n*2;++i){
			if(a.x[i] < b.x[i])
				return true;
			else if(a.x[i] > b.x[i])
				return false;
		}
		return false;
	}
}p;
map<node,bool> mp;
int a[N],b[N];

void dfs(int x,int y){
	if(x == n+1){
		while(y <= n) p.x[x+y-1] = b[y],y++;
		mp[p] = true;
		return;
	}
	if(y == n+1){
		while(x <= n) p.x[x+y-1] = a[x],x++;
		mp[p] = true;
		return;
	}
	p.x[x+y-1] = a[x];
	dfs(x+1,y);
	p.x[x+y-1] = b[y];
	dfs(x,y+1);
}

int main(){
	n = read();
	for(int i = 1;i <= n;++i) a[i] = read();
	for(int j = 1;j <= n;++j) b[j] = read();
	for(int i = 1;i <= 10;++i) p.x[i] = 0;
	dfs(1,1);
	printf("%lld\n",mp.size()%mod);
	return 0;
}