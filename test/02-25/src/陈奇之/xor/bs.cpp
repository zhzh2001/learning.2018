#include<cstdio>
#include<ctype.h>
#include<cstring>
#include<cstdlib>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<cstdlib>
#include<algorithm>
using namespace std;
typedef long long ll;
#define RG register
#define pc putchar
#define gc getchar
#define rd read
#define mem(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Down(i,a,b) for(RG int i=(a);i>=(b);--i)
inline ll read(){
	RG ll x=0,f=1;RG char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
const int maxn = 2005;
const int Mod = 1e9+7;
int n,m,A[maxn],B[maxn];
char c;
int cnt = 0,count = 0;
int f[maxn][maxn];
void dfs(int deep){
	if(deep == m+1){
//		printf("Case %d\n",++cnt);
		int _w = 0,w = 0;
		for(int i=1;i<=A[0];i++) /*printf("%d ",A[i]),*/_w^=A[i];
//		printf(":%d|",_w);
		for(int i=1;i<=B[0];i++) /*printf("%d ",B[i]),*/w^=B[i];
//		printf(":%d\n",w);
		f[_w][w] ++;
//		puts("");
		return;
	}
	if(deep<=n){
		A[0]++;A[A[0]] = deep;
		dfs(deep+1);
		A[0]--;
	}
	if(deep<=m){
		B[0]++;B[B[0]] = deep;
		dfs(deep+1);
		B[0]--;
	}
	dfs(deep+1);
}
int main(){
	n = rd(),m = rd(),c=gc();
	while(c!='<'&&c!='>'&&c!='=') c=gc();
//	if(n>m){
//		swap(n,m);
//		if(c=='<') c='>'; 
//	}
	A[0]=B[0]=0;
	dfs(1);
//	rep(i,0,512)rep(j,0,512)
//		if(f[i][j]) printf("f[%d][%d] = %d\n",i,j,f[i][j]);
	int ans = 0;
	rep(i,0,512) rep(j,0,512)
		if(i>j) (ans+=f[i][j])%=Mod;;
	printf("%d\n",ans);
	return 0;
}













