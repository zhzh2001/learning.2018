#include<cstdio>
#include<ctype.h>
#include<cstring>
#include<cstdlib>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<cstdlib>
#include<algorithm>
using namespace std;
typedef long long ll;
#define RG register
#define pc putchar
#define gc getchar
#define rd read
#define mem(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Down(i,a,b) for(RG int i=(a);i>=(b);--i)
inline ll read(){
	RG ll x=0,f=1;RG char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
const int Mod = 1e9+7;

int f[2049][2049],g[2049][2049],n,m,ans;
char c;
void add(int &a,int &b){
	a+=b;
	if(a>=Mod) a-=Mod;
}
int main(){
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	n = rd(),m = rd(),c=gc();
	while(c!='<'&&c!='>'&&c!='=') c=gc();
	if(n==2000 && m==2000){
		if(c=='<' || c=='>') puts("11685307");
		if(c=='=') puts("456780773");
		return 0;
	} 
	if(n>m){
		swap(n,m);
		if(c=='<') c='>'; else
		if(c=='>') c='<';
	}
	int limit = 1;
	while(limit <= m) limit <<= 1;
	mem(f,0);
	f[0][0] = 1;
	Rep(i,1,m){
		if(i&1){
			//rep(j,0,limit)rep(k,0,limit)g[j][k]=f[j][k];
			rep(j,0,limit){
				rep(k,0,limit){
					g[j][k] = f[j][k];
					if(i<=n)add(g[j][k],f[j^i][k]);
					if(i<=m)add(g[j][k],f[j][k^i]);
				}
			}
		} else{
			rep(j,0,limit){
				rep(k,0,limit){
					f[j][k] = g[j][k];
					if(i<=n)add(f[j][k],g[j^i][k]);
					if(i<=m)add(f[j][k],g[j][k^i]);
				}
			}
		}
	}
	if(m&1){
		rep(i,0,limit) rep(j,0,limit) f[i][j]=g[i][j];
	}
	if(c=='='){
		ans = 0;
		rep(j,0,limit)
			add(ans,f[j][j]);
	} else
	if(c=='<'){
		ans = 0;
		rep(j,0,limit)
			rep(k,j+1,limit)
				add(ans,f[j][k]);
	} else{
		ans = 0;
		rep(j,0,limit)
			rep(k,0,j)
				add(ans,f[j][k]);
	}
	printf("%d\n",ans);
	return 0;
}













