#include<cstdio>
#include<ctype.h>
#include<cstring>
#include<cstdlib>
#include<ctime>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<cstdlib>
#include<algorithm>
using namespace std;
typedef unsigned long long ull;
typedef long long ll;
#define RG register
#define pc putchar
#define gc getchar
#define rd read
#define men(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Down(i,a,b) for(RG int i=(a);i>=(b);--i)
#define hash _____hash
inline ll read(){
	RG ll x=0,f=1;RG char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
const ull Base1 = 233;
const ull Base2 = 2333;
const int maxn = 2e5+12;
ull Pow1[maxn],Pow2[maxn],Hash1[maxn],Hash2[maxn];
#define hash1(l,r) (Hash1[r] - Hash1[l-1] * Pow1[r-l+1])
#define hash2(l,r) (Hash2[r] - Hash2[l-1] * Pow2[r-l+1])
inline bool check(int L,int R,int len){
	int r1 = L+len-1,l2 = R-len+1;
	if(hash1(L,r1) != hash1(l2,R)) return false;
	if(hash2(L,r1) != hash2(l2,R)) return false;
	return true;
}
int n,Q;
char s[maxn];
int main(){
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	srand('W'+'Z'+'P'+'A'+'K'+'Z'+'J'+'O'+'I');
	srand('Z'+'Y'+'Y'+'A'+'K'+'Z'+'J'+'O'+'I'+rand());
	srand('L'+'Z'+'H'+'A'+'K'+'Z'+'J'+'O'+'I'+rand());
	srand('X'+'Y'+'M'+'A'+'K'+'Z'+'J'+'O'+'I'+rand());
	srand('L'+'B'+'C'+'A'+'K'+'Z'+'J'+'O'+'I'+rand());
	srand('Z'+'X'+2003+'A'+'K'+'Z'+'J'+'O'+'I'+rand());
	srand('Y'+'K'+26+'A'+'K'+'Z'+'J'+'O'+'I'+rand());
	n = rd();Q = rd();
	Pow1[0] = Pow2[0] = 1;
	Rep(i,1,n) Pow1[i] = Pow1[i-1] * Base1;
	Rep(i,1,n) Pow2[i] = Pow2[i-1] * Base2;
	scanf("%s",s+1);
	Rep(i,1,n) s[i]-='a'-1;
	Rep(i,1,n) Hash1[i] = Hash1[i-1] * Base1 + s[i];
	Rep(i,1,n) Hash2[i] = Hash2[i-1] * Base2 + s[i];
	Rep(i,1,Q){
		RG int l = rd(),r = rd(),len=0;
		if(r-l+1 <= 1001){
			for(len=r-l;len>=1;len--)
				if(check(l,r,len)) break;
			printf("%d\n",len);
		} else{
			RG int L = 0,R = min(10,R-L+1);
			for(RG int j=1;j<=20;j++){
				int w;
				for(int k=0;k<=5;k++){
					if(R-L<=2)break;
//					w = (1ll * w * 1234 + 9998) % (R-L-1) + L;
					w = rand() % (R-L-1) + L;
					if(!(1<=w&&w<r-l+1))continue;
					if(check(l,r,w)){
						len = max(len,w);
						L = w+1;
					}
				}
				if(R*2 <= r-l) R*=2; else R = r-l;
			}
			R = min(min(R,L+1005),r-l);
			for(int ww=R;ww>=L;ww--)
				if(check(l,r,ww)) len=max(len,ww);
//			for(int ww=r-l;ww>=1&&ww>=r-l-50;ww--)
//				if(check(l,r,ww)) len = max(len,ww);
			printf("%d\n",len);
		}
	}
	return 0;
}
