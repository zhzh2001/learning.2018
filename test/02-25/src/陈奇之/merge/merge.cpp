#include<cstdio>
#include<ctype.h>
#include<cstring>
#include<cstdlib>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<cstdlib>
#include<algorithm>
using namespace std;
typedef long long ll;
#define RG register
#define pc putchar
#define gc getchar
#define rd read
#define mem(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Down(i,a,b) for(RG int i=(a);i>=(b);--i)
inline ll read(){
	RG ll x=0,f=1;RG char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
const int Mod1 = 998244353;
const int Mod2 = 1e9+7;
int n,a[2005],b[2005],c[4005],ans;
map<int,bool> f1,f2;
bool check(){
	unsigned long long res1 = 0;
	unsigned long long res2 = 0;
	for(int i=1;i<=2*n;i++) res1 = (1ll * res1 * 2333 + c[i]) % Mod1;
	for(int i=1;i<=2*n;i++) res2 = (1ll * res2 * 233 + c[i]) % Mod2;
//	for(int i=1;i<=2*n;i++) printf("%d ",c[i]);puts("");
	if((!f1[res1] || !f2[res2])){
		f1[res1] = true;
		f2[res2] = true;
		return true;
	}
	return false;
}
void dfs(int i,int l,int r){
	if(i==2*n+1){
		if(check()) ans++;
		return ;
	}
	if(l<=n){
		c[i] = a[l];
		dfs(i+1,l+1,r);
	}
	if(r<=n){
		c[i] = b[r];
		dfs(i+1,l,r+1);
	}
}
const int Mod = 1e9+7;
int Pow(int a,int b){
	int ans = 1;
	for(;b;b>>=1,a=1ll*a*a%Mod)
		if(b&1)ans=1ll*ans*a%Mod;
	return ans;
}
int fac[2005];
int main(){
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	n = rd();
	Rep(i,1,n) a[i] = rd();
	Rep(i,1,n) b[i] = rd();
	if(n<=10){
		ans = 0;
		dfs(1,1,1);
		printf("%d\n",ans);
	} else{
		fac[0] = 1;
		Rep(i,1,2*n) fac[i] = 1ll * fac[i-1] * i % Mod;
		printf("%lld\n",1ll * fac[2*n] * Pow(fac[n],Mod-2) % Mod * Pow(fac[n],Mod-2) % Mod * Pow(n+1,Mod-2) % Mod);
	}
}



/*
9
1 2 3 4 5 6 7 8 9
1 2 3 4 5 6 7 8 9
*/





