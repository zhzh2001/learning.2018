#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;
inline int rd()
{
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') f = -1;
		ch = getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		x = x*10+ch-48;
		ch = getchar();
	}
	return x * f;
}
//data
const int mod=1e9+7;
int N,M;
char flag[2];
int ans=0;
int q[300],qcnt=0;
//code
int check(int vv)
{
	int ret=0;
	memset(q,0,sizeof q);
	qcnt=0;
	for(int i=0; (1<<i)<=vv; ++i)
		if(vv&(1<<i))
			q[++qcnt]=i+1;
	ret=q[1];
	for(int i=2; i<=qcnt; ++i)
		ret=ret xor q[i];
	return ret;
}
void calc(int x,int y,char rela)
{
	int fx=check(x),fy=check(y);
	if(fx<fy&&rela=='<')
	{
		++ans;
	}
	else if(fx==fy&&rela=='=')	++ans;
	else if(fx>fy&&rela=='>')	++ans;
	ans%=mod;
}
//main
int main()
{
	freopen("xor.in","r+",stdin);
	freopen("xor.out","w+",stdout);
	N=rd(),M=rd();
	scanf("%1s",flag);
	for(int i=0; i<(1<<N); ++i)
	{
		for(int j=0; j<(1<<M); ++j)
		{
			if(i&j)	continue;
			calc(i,j,flag[0]);
		}
	}
	printf("%d\n",ans);
	return 0;
}
