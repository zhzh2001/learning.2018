#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;
inline int rd() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}
//data
int N,Q;
const int MAXN=2e5+10,base=359;
const LL mod=1e9+7;
char ss[MAXN];
LL hashit[MAXN];
LL bpow[MAXN];//base的幂次方提前计算 
//code
LL get_hash(int L,int R)
{
	LL ret=0;
	ret=hashit[R]-hashit[L-1]*bpow[R-L+1]%mod;
	ret=(ret%mod+mod)%mod;
	return ret;
}
void work(int x,int y)
{
	for(int d=y-x;d>0;--d)
	{
		if(get_hash(x,x+d-1)==get_hash(y-d+1,y))
		{
			printf("%d\n",d);
			return;
		}
	}
	puts("0");
}
//main
int main()
{
	freopen("border.in","r+",stdin);
	freopen("border.out","w+",stdout);
	N=rd(),Q=rd();
	gets(ss+1);
	bpow[0]=1;
	for(int i=1;i<=N;++i)
	{
		hashit[i]=(hashit[i-1]*base+ss[i])%mod;
		bpow[i]=bpow[i-1]*base%mod;		
	}
	for(int i=1;i<=Q;++i)
	{
		int x=rd(),y=rd();
		work(x,y);
	}
	return 0;
}
