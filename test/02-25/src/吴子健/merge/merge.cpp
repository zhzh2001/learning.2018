#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;
inline int rd()
{
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') f = -1;
		ch = getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		x = x*10+ch-48;
		ch = getchar();
	}
	return x * f;
}
//data
int N;
int a[2010],b[2010];
LL f[2010][2010];
const int mod=1e9+7;
const LL mod2=1e15+9;
int tar[2010];
LL hashit_tab[int(1e7)],hcnt;
LL hashit()
{
	LL ret=0;
	for(int i=1;i<=2*N;++i)
		ret=(ret*359+tar[i])%mod2;
	return ret;
}
//code
void dfs(int x,int y)
{
	if(x>N&&y>N)
	{
		hashit_tab[++hcnt]=hashit();
		return;		
	}
	if(x<=N)
	{
		tar[x+y-1]=a[x];
		dfs(x+1,y);
	}
	if(y<=N)
	{
		tar[x+y-1]=b[y];
		dfs(x,y+1);
	}
}
//main
int main()
{
	freopen("merge.in","r+",stdin);
	freopen("merge.out","w+",stdout);
	N=rd();
	for(int i=1; i<=N; ++i)
		a[i]=rd();
	for(int i=1; i<=N; ++i)
		b[i]=rd();
	for(int i=0; i<=N; ++i)
		f[i][0]=f[0][i]=1;
	if(N<=10)
	{
		dfs(1,1);
		sort(hashit_tab+1,hashit_tab+hcnt+1);
		hcnt=unique(hashit_tab+1,hashit_tab+hcnt+1)-hashit_tab-1;
		printf("%lld\n",hcnt%mod);
		return 0;
	}
	else if(memcmp(a+1,b+1,N*sizeof (int))==0)
		for(int i=1; i<=N; ++i)
			for(int j=1; j<=N; ++j)
			{
				if(a[i]==b[j])
					f[i][j]=f[i-1][j];
				else
					f[i][j]=(f[i-1][j]+f[i][j-1])%mod;
				f[i][j]%=mod;
				//printf("f[%d][%d]=%d\n",i,j,f[i][j]);
			}
	else
	{
		for(int i=1; i<=N; ++i)
			for(int j=1; j<=N; ++j)
			{
				if(a[i]==b[j])
				{
					f[i][j]=f[i-1][j]+f[i][j-1]-f[i-1][j-1];
					if(a[i-1]==b[j-1]&&i-1>0&&j-1>0)
						f[i][j]-=f[i-2][j-2];				
				}
				else
					f[i][j]=(f[i-1][j]+f[i][j-1])%mod;
				f[i][j]%=mod;
				//printf("f[%d][%d]=%d\n",i,j,f[i][j]);
			}
	}
	printf("%lld\n",(f[N][N]%mod+mod)%mod);

	return 0;
}
