#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
using namespace std;
const int N=25,M2=1856379,M1=1095738,P=3452356;
int a[N],b[N],f[P],n,ans,v1[P],v2[P];
int find(int x,int y)
{
	int k=1LL*x*y%P;
	for (;f[k]&&(x!=v1[k]||y!=v2[k]);k=(k+1)%P);
	if (f[k])return -1;
	return k;
}
void dfs(int x,int y,int h1,int h2)
{
	if (x>n&&y>n)
	 {
	 	int l=find(h1,h2);
	 	if (l==-1)return;
	 	ans++;
	 	f[l]=1;
	 	v1[l]=h1;
		v2[l]=h2;
	 	return;
	 }
	if (x<=n)dfs(x+1,y,(1LL*h1*23+a[x])%M1,(1LL*h2*52+a[x])%M2);
	if (y<=n)dfs(x,y+1,(1LL*h1*23+b[y])%M1,(1LL*h2*52+b[y])%M2); 
}
int main()
{
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)scanf("%d",&a[i]);
	for (int i=1;i<=n;i++)scanf("%d",&b[i]);
	dfs(1,1,0,0);
	printf("%d",ans);
	return 0;
}
