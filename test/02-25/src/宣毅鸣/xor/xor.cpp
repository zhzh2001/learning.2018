#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
const ll N=2048,M=1e9+7,inv=M/2+1;
ll dp[2][N][N],f[2][N];
int n,m,ppp[N];
char s[10];
ll ksm(ll x,ll y)
{
	if (!y)return 1;
	ll z=ksm(x,y/2);
	z*=z;z%=M;
	if (y&1)z*=x;
	return z%M;
}
int main()
{
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	scanf("%d%d%s",&n,&m,&s);
	ppp[0]=1;
	for (int i=1;i<N;i++)ppp[i]=1<<((int)log2(i)+1);
	f[0][0]=1;
	if (n>m)
	 {
	 	swap(n,m);
	 	s[0]=s[0]=='>'?'<':'>';
	 }	
	for (int i=1;i<=m;i++)
	 for (int j=0;j<ppp[i];j++)
	  {
	   	int t=i&1,p=t^1;
	   	f[t][j]=f[p][j];
	   	if (i<=n)f[t][j]+=f[p][j^i];
	   	f[t][j]+=f[p][j^i];
	   	while (f[t][j]>M)f[t][j]-=M;
	  }
	if (s[0]=='=')
	 {	 	   	  
	 	printf("%d",f[m&1][0]);
	 	return 0;
	 }
	if (n==m)
	 {
	 	printf("%lld",1LL*(ksm(3,n)-f[m&1][0]+M)%M*inv%M);
	 	return 0;
	 }
	dp[0][0][0]=1;
	for (int i=1;i<=m;i++)
	 for (int j=0;j<ppp[i];j++)
	  for (int k=0;k<ppp[i];k++)
	   {
	   	int t=i&1,p=t^1;
	   	dp[t][j][k]=dp[p][j][k];
	   	if (i<=n)dp[t][j][k]+=dp[p][j^i][k];
	   	dp[t][j][k]+=dp[p][j][k^i];
	   	while (dp[t][j][k]>M)dp[t][j][k]-=M;
	   }
	ll ans=0;   
	if (s[0]=='>')
	 {
	 	for (int j=0;j<ppp[m];j++)
	 	 for (int k=0;k<j;k++)ans+=dp[m&1][j][k];
	 }
	if (s[0]=='<')
	 {
	 	for (int j=0;j<ppp[m];j++)
	 	 for (int k=j+1;k<ppp[m];k++)ans+=dp[m&1][j][k];   
	 }
	printf("%lld",ans%M);
	return 0; 
}
