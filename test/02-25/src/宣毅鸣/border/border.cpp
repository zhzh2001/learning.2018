#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
using namespace std;
const int N=200005;
const int P1=1246523,P2=1523461,d1=46,d2=63;
int n,m,D1[N],D2[N],l,r;
char s[N];
void init()
{
	D1[0]=D2[0]=1;
	for (int i=1;i<N;i++)D1[i]=D1[i-1]*d1%P1,D2[i]=D2[i-1]*d2%P2;
}
int main()
{
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	init();
	scanf("%d%d",&n,&m);
	scanf("%s",&s);
	while (m--)
	 {
	 	scanf("%d%d",&l,&r);
	 	l--;r--;
	 	int ans=0,lh1=0,lh2=0,rh1=0,rh2=0;
	 	for (int d=1;d<=r-l;d++)
	 	 {
	 	 	lh1=(lh1*d1+s[l+d-1])%P1;
	 	 	lh2=(lh2*d2+s[l+d-1])%P2;
	 	 	rh1=(s[r-d+1]*D1[d-1]+rh1)%P1;
	 	 	rh2=(s[r-d+1]*D2[d-1]+rh2)%P2;
	 	 	if (lh1==rh1&&lh2==rh2)ans=d;	
	 	 }
	 	printf("%d\n",ans); 
	 }
	return 0; 
}
