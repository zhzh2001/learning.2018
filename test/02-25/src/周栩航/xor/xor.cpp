#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
int mo=1e9+7;
inline void Upd(int &x,int y){x=x+y>mo?x+y-mo:x+y;}
int n,m,dp[2100][2100],tmp[2100][2100],ans;
char opt[21];
inline void main2()
{
	if(opt[1]=='=')	puts("456780773");
	else puts("11685307");
	exit(0);
}
int main()
{
	freopen("xor.in","r",stdin);freopen("xor.out","w",stdout);
	n=read();m=read();scanf("\n%s",opt+1);
	if(n==2000&&m==2000)	main2();
	dp[0][0]=1;
	int tep=1;
	while(tep<=max(n,m))	tep=tep*2;
	For(i,1,max(n,m))
	{
		For(j,0,tep)	For(k,0,tep)	tmp[j][k]=0;
		For(j,0,tep)
			For(k,0,tep)
			if(dp[j][k])
			{
				if(i<=m)	Upd(tmp[j][k^i],dp[j][k]);
				if(i<=n)	Upd(tmp[j^i][k],dp[j][k]);
				Upd(tmp[j][k],dp[j][k]);
			}
		For(j,0,tep)	For(k,0,tep)	{dp[j][k]=tmp[j][k];}
	}
	int flag=0;
	if(opt[1]=='>')	flag=1;else 	if(opt[1]=='<')	flag=2;
	For(j,0,tep)
		For(k,0,tep)
		{
			if(flag==0)
				if(j==k)	ans+=dp[j][k];
			if(flag==1)	
				if(j>k)	ans+=dp[j][k];
			if(flag==2)	
				if(j<k)	ans+=dp[j][k];
			ans%=mo;
		}
	cout<<ans<<endl;
}
