#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
ll mo[]={0,100046911,100874153,102140501,104024539,1000000009};
char s[300001];
ll pre[300001][6],bas=37;
int nxt[200005][26],las[200005][26];
int n,q,l,r;
inline ll ksm(ll x,int y,ll mo){ll sum=1;for(;y;y/=2,x=x*x%mo) if(y&1)	sum=sum*x%mo;return sum;}
inline ll Get(int l,int r,int p)
{
	ll tmp=(pre[r][p]-pre[l-1][p]*ksm(bas,r-l+1,mo[p]))%mo[p]; 
	tmp=tmp<0?tmp+mo[p]:tmp;
	return tmp;
}
inline bool check(int tl,int tr,int len)
{
	int tl_tmp=tl+len-1,tr_tmp=tr-len+1;
	For(i,1,5)	if(Get(tl,tl_tmp,i)!=Get(tr_tmp,tr,i))	return 0;
	return 1;
}
inline void Solve(int tl,int tr)
{
	int ans=0;
	int len1=1,len2=1;
	l=tl+len1-1,r=tr-len2+1;
	while(1)
	{
		if(len1>len2)
			r=las[r-1][s[tl]-'a'],len2=tr-r+1;
			else	l=nxt[l+1][s[tr]-'a'],len1=-(tl-l-1);
		if(r<=tl||l>=tr)	break;
		if(len1==len2)	if(check(tl,tr,len1))	ans=len1;
	} 
	writeln(ans);
}
int main()
{
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	n=read();q=read();
	scanf("\n%s",s+1);
	For(i,1,n)
	{
		For(j,1,5)
			pre[i][j]=(pre[i-1][j]*bas+s[i])%mo[j];
	}
	For(j,0,25)	nxt[n+1][j]=n+1;
	Dow(i,1,n)
		For(j,0,25)
		{
			nxt[i][j]=nxt[i+1][j];
			if(j==s[i]-'a')	nxt[i][j]=i;
		}
	For(i,1,n)
		For(j,0,25)
		{
			las[i][j]=las[i-1][j];
			if(j==s[i]-'a')	las[i][j]=i;
		}
	For(i,1,q)
	{
		l=read();r=read();
		Solve(l,r);
	}
}
