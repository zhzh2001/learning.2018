#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
int dp[2051][2051],sum[2051][2051],n,a[2051],b[2051];
int mo=1e9+7;
inline void Solve()
{
	dp[0][0]=1;sum[0][0]=1;
	For(i,1,n+1)
	{
		For(j,0,n)	
			if(a[i]!=b[j])
			{
//				For(k,0,j)		Upd(dp[i][j],dp[i-1][k]);
				dp[i][j]=sum[i-1][j];
				if(j==0)	sum[i][j]=dp[i][j];
				else sum[i][j]=(sum[i][j-1]+dp[i][j])%mo;
			}	
			else sum[i][j]=sum[i][j-1];
	}
	cout<<dp[n+1][n]<<endl;
	exit(0);
}
int main()
{
	n=read();	
	For(i,1,n)	a[i]=read();
	For(i,1,n)	b[i]=read();
	Solve();
}
