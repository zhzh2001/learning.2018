#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<map>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define inf 1e9
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
map<pair<ll,ll>,int> vis;
int n,a[2001],b[2001],ans=0;
ll bas=37,mo=1e9+7;
inline void Dfs(int x,int y,ll now,ll now1)
{
	if(x==n+1&&y==n+1)	{if(!vis[mk(now,now1)])	vis[mk(now,now1)]=1,ans=(ans+1)%mo;return;}
	if(x!=n+1)	Dfs(x+1,y,(now*bas+a[x])%mo,now1*bas+a[x]);
	if(y!=n+1)	Dfs(x,y+1,(now*bas+b[y])%mo,now1*bas+b[y]);
}
int dp[2051][2051],sum[2051][2051];
inline void Upd(int &x,int y){x=(x+y)%mo;}
inline void main2()
{
	dp[0][0]=1;sum[0][0]=1;
	For(i,1,n+1)
	{
		For(j,0,n)	
			if(j!=i)
			{
				dp[i][j]=sum[i-1][j];
				if(j==0)	sum[i][j]=dp[i][j];
				else sum[i][j]=sum[i][j-1]+dp[i][j];
			}	else sum[i][j]=sum[i][j-1];
	}
	cout<<dp[n+1][n]<<endl;
	exit(0);
}
int main()
{
	freopen("merge.in","r",stdin);freopen("merge.out","w",stdout);
	n=read();	
	if(n>100)	
		main2();
	For(i,1,n)	a[i]=read();
	For(i,1,n)	b[i]=read();
	Dfs(1,1,0,0);
	cout<<ans<<endl;
}
