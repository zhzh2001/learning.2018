#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,Q;
bool flag;
struct D{
	int le;
	int w[20100];
}a[100];
string s;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	n=read();Q=read();
	cin>>s;
	For(i,0,n-1)
	{
		a[s[i]-'a'+1].le++;
		a[s[i]-'a'+1].w[a[s[i]-'a'+1].le]=i;
	}
	while(Q--)
	{
		ll l=read(),r=read();
		l--;r--;
		ll ans=0;
		ll xl=s[l]-'a'+1;
		ll xr=s[r]-'a'+1;
		ll yl=1,yr=a[xr].le;
		while(a[xl].w[yl]<=l&&yl!=a[xl].le)yl++;
		while(a[xr].w[yr]>=r&&yr!=0)yr--;
		while(a[xl].w[yl]<=r&&yl!=a[xl].le)
		{
			while(r-a[xl].w[yl]!=a[xr].w[yr]-l&&yl!=a[xl].le&&yr!=0)
			{
				if(r-a[xl].w[yl]>a[xr].w[yr]-l)yl++;
											else yr++;
				if(a[xl].w[yl]>r||a[xr].w[yr]<l)break;
			}
			if(a[xl].w[yl]>r||a[xr].w[yr]<l)break;
			flag=true;
			For(i,1,a[xr].w[yr]-l+1)
			{
				if(s[l+i-1]!=s[a[xl].w[yl]+i-1])
				{
					flag=false;
					break;
				}
			}
			if(flag)
			{
				ans=a[xr].w[yr]-l+1;
				break;
			}
			yl++;yr--;
		}
		cout<<ans<<endl;
	}
	return 0;
}

