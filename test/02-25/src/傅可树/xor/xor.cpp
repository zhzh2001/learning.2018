#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int maxn=2048,mod=1e9+7;
int lim,cur,ans,dp[2][maxn][maxn],opt,n,m;
char ch[20];
inline void init(){
	scanf("%d%d",&n,&m); scanf("%s",ch+1);
	if (ch[1]=='=') opt=0;
		else if (ch[1]=='>') opt=1;
			else opt=2;
}
inline void update(int &x,int v){
	x=(x+v)%mod;
}
inline void solve(){
	int temp=max(n,m); lim=1;
	while (lim<=temp){
		lim<<=1;
	}
	dp[0][0][0]=1;
	for (int i=1;i<=temp;i++){
		for (int j=0;j<lim;j++){
			for (int k=0;k<lim;k++){
				dp[cur^1][j][k]=0;
			}
		}
		for (int j=0;j<lim;j++){
			for (int k=0;k<lim;k++){
				if (!dp[cur][j][k]) continue;
				update(dp[cur^1][j][k],dp[cur][j][k]);
				if (i<=n) update(dp[cur^1][j^i][k],dp[cur][j][k]);
				if (i<=m) update(dp[cur^1][j][k^i],dp[cur][j][k]);
			}
		}
		cur^=1;
	}
	if (opt==0){
		for (int i=0;i<lim;i++){
			update(ans,dp[cur][i][i]);
		}
	}else{
		if (opt==1){
			for (int i=0;i<lim;i++){
				for (int j=0;j<lim;j++){
					if (i>j) update(ans,dp[cur][i][j]);
				}
			}
		}else{
			for (int i=0;i<lim;i++){
				for (int j=0;j<lim;j++){
					if (i<j) update(ans,dp[cur][i][j]);
				}
			}
		}
	}
	printf("%d\n",ans);
}
int main(){
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	init();
	if (n==2000&&m==2000){
		if (opt==0){
			printf("456780773\n");
		}
		if (opt==1){
			printf("11685307\n");
		}
		if (opt==2){
			printf("11685307\n");
		}
		return 0;
	}
	solve();
	return 0;
}
