#include<cstdio>
#include<cstring>
using namespace std;
inline int read(){int x=0,f=1; int ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int maxn=2e5+5;
int n,m;
char s[maxn],S[maxn];
inline void init(){
	n=read(); m=read();
	scanf("%s",s+1);
}
int Next[maxn];
inline void travel(int l,int r){
	for (int i=l;i<=r;i++){
		S[i-l+1]=s[i];
	}
	int len=r-l+1;
	int j=0; Next[1]=0;
	for (int i=2;i<=len;i++){
		Next[i]=0;
		while (j&&S[j+1]!=S[i]) j=Next[j];
		if (S[j+1]==S[i]) j++;
		Next[i]=j;
	}
	writeln(Next[len]);
}
inline void solve(){
	for (int i=1;i<=m;i++){
		int l=read(),r=read();
		travel(l,r);
	}
}
int main(){
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	init();
	solve();
	return 0;
}
