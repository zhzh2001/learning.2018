#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
typedef long long LL;
const LL md=1e9+7;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL hash[3][200003],n,m,jk[3][200003],P[]={md,90334567,64377239},x,y,z,A,B,C,D,ans;
LL ckk[200003],tp[33],lst[200003];
char c[200003];
int main(){
	freopen("border.in","r",stdin);
	freopen("border.out","w",stdout);
	n=read();
	m=read();
	scanf("%s",c+1);
	jk[0][0]=jk[1][0]=jk[2][0]=1ll;
	for(LL i=1;i<=n;i++){
		for(LL j=0;j<3;j++){
			hash[j][i]=(hash[j][i-1]*26+c[i]-'a')%P[j];
			jk[j][i]=jk[j][i-1]*26%P[j];
		}
		if(i>1&&c[i]==c[i-1]){
			ckk[i]=ckk[i-1]+1;
		}
		lst[i]=tp[c[i]-'a'];
		tp[c[i]-'a']=i;
	}
	for(LL i=1;i<=m;i++){
		x=read();
		y=read();
		ans=0;
		if(x+ckk[y]>=y)ans=y-x;
		else
		for(LL j=lst[y];j>=x;j=lst[j]){
			bool fla=1;
			for(LL k=0;k<3;k++){
				A=((hash[k][j]-hash[k][x-1]*jk[k][j-x+1]%P[k])%P[k]+P[k])%P[k];
				z=y-j+x;
				B=((hash[k][y]-hash[k][z-1]*jk[k][y-z+1]%P[k])%P[k]+P[k])%P[k];
				if(A!=B)fla=0;
			}
			if(fla){
				ans=std::max(ans,j-x+1);
				break;
			}
		}
		printf("%lld\n",ans);
	}
	return 0;
}
