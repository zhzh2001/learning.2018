#include <iostream>
#include <algorithm>
#include <cstdio>
#include <set>
#include <string>
#include <cstring>
typedef long long LL;
using std::string;
using std::set;
const LL md=1e9+7;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL f[2003][2003],n,a[2003],b[2003],x,y,ans=0;
bool same=1;
set<string>p;
string sss;
char c[103];
void dfs(LL x,LL y){
	if(x>n&&y>n){
		sss=c;
		if(p.count(sss))return;
		p.insert(sss);
		ans++;
		return;
	}
	if(y<=n){
		c[x+y-2]=b[y]-1+'0';
		dfs(x,y+1);
		c[x+y-2]=0;
	}
	if(x<=n){
		c[x+y-2]=a[x]-1+'0';
		dfs(x+1,y);
		c[x+y-2]=0;
	}
}
int main(){
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	n=read();
	for(LL i=1;i<=n;i++)a[i]=read();
	for(LL i=1;i<=n;i++){
		b[i]=read();
		if(a[i]!=b[i])same=0;
	}
	if(same){
		for(LL i=0;i<=n;i++)f[i][0]=f[0][i]=1;
		for(LL i=1;i<=n;i++){
			for(LL j=1;j<=n;j++){
				f[i][j]=(f[i-1][j]+f[i][j-1])%md;
				if(i==j)f[i][j]=f[i-1][j];
			}
		}
		printf("%lld\n",f[n][n]);
	}else if(n<=10){
		dfs(1,1);
		printf("%lld\n",ans);
	}
	return 0;
}
