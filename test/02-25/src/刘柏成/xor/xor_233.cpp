#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int f[2049][2049],g[2049][2049];
inline void add(int &x,int y){
	x+=y;
	if (x>=mod)
		x-=mod;
}
int main(){
	init();
	int n=readint(),m=readint();
	char c=readchar();
	if (n>m){
		swap(n,m);
		if (c=='>')
			c='<';
		else if (c=='<')
			c='>';
	}
	f[0][0]=1;
	for(int i=1;i<=m;i++){
		memset(g,0,sizeof(g));
		for(int j=0;j<2048;j++)
			for(int k=0;k<2048;k++){
				add(g[j][k^i],f[j][k]);
				if (i<=n)
					add(g[j^i][k],f[j][k]);
				add(g[j][k],f[j][k]);
			}
		swap(f,g);
	}
	int ans=0;
	for(int i=0;i<2048;i++)
		for(int j=0;j<2048;j++)
			if ((i<j && c=='<') || (i==j && c=='=') || (i>j && c=='>'))
				add(ans,f[i][j]);
	printf("%d",ans);
}