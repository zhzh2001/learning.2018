#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("merge.in","r",stdin);
	freopen("bf.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int n,k;
int a[22],b[22];
struct arr{
	int a[22];
	bool operator <(const arr & rhs)const{
		return lexicographical_compare(a,a+k,rhs.a,rhs.a+k);
	}
	bool operator ==(const arr & rhs)const{
		for(int i=0;i<k;i++)
			if (a[i]!=rhs.a[i])
				return false;
		return true;
	}
}qwq[233333];
int main(){
	init();
	n=readint();
	k=2*n;
	for(int i=1;i<=n;i++)
		a[i]=readint();
	for(int i=1;i<=n;i++)
		b[i]=readint();
	int cur=0;
	for(int i=0;i<(1<<k);i++)
		if (__builtin_popcount(i)==n){
			int ca=0,cb=0;
			int *ans=qwq[++cur].a;
			for(int j=0;j<k;j++)
				if (i>>j&1)
					ans[j]=a[++ca];
				else ans[j]=b[++cb];
		}
	sort(qwq+1,qwq+cur+1);
	printf("%d",unique(qwq+1,qwq+cur+1)-qwq-1);
}