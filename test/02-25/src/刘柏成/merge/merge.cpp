#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("merge.in","r",stdin);
	freopen("merge.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int n;
int a[2003],b[2003];
namespace bf{
	int k;
	struct arr{
		int a[22];
		bool operator <(const arr & rhs)const{
			return lexicographical_compare(a,a+k,rhs.a,rhs.a+k);
		}
		bool operator ==(const arr & rhs)const{
			for(int i=0;i<k;i++)
				if (a[i]!=rhs.a[i])
					return false;
			return true;
		}
	}qwq[233333];
	void work(){
		k=2*n;
		for(int i=1;i<=n;i++)
			a[i]=readint();
		for(int i=1;i<=n;i++)
			b[i]=readint();
		int cur=0;
		for(int i=0;i<(1<<k);i++)
			if (__builtin_popcount(i)==n){
				int ca=0,cb=0;
				int *ans=qwq[++cur].a;
				for(int j=0;j<k;j++)
					if (i>>j&1)
						ans[j]=a[++ca];
					else ans[j]=b[++cb];
			}
		sort(qwq+1,qwq+cur+1);
		printf("%d",unique(qwq+1,qwq+cur+1)-qwq-1);
	}
}
ll dp[2003][2003],g[2003][2003];
//internal get(i,j-1,i-1,j) where a[i]==b[j]
int ni,nj;
pair<int,int> tmp[4000005];
int cur;
ll dfs(int x1,int y1,int x2,int y2){ //a[1..x1] b[1..y1] vs a[1..x2] b[1..y2]
	int k1=ni-x1,k2=ni-1-x2;
	ll &ans=g[k1][k2];
	if (ans>=0)
		return ans;
	if (x1==x2){
		// assert(y1==y2);
		// fprintf(stderr,"%d %d\n",x1,y1);
		return dp[x1][y1];
	}
	ans=0;
	tmp[++cur]=make_pair(k1,k2);
	if (x1 && y2 && a[x1]==b[y2])
		ans+=dfs(x1-1,y1,x2,y2-1);
	if (y1 && x2 && b[y1]==a[x2])
		ans+=dfs(x1,y1-1,x2-1,y2);
	if (ans>=mod)
		ans-=mod;
	// printf("dfs %d %d %d %d=%lld\n",x1,y1,x2,y2,ans);
	return ans;
}
int main(){
	init();
	n=readint();
	if (n<=10){
		bf::work();
		return 0;
	}
	for(int i=1;i<=n;i++)
		a[i]=readint();
	for(int i=1;i<=n;i++)
		b[i]=readint();
	for(int i=0;i<=n;i++)
		dp[0][i]=dp[i][0]=1;
	bool flag=1;
	for(int i=1;i<=n;i++)
		if (a[i]!=b[i]){
			flag=0;
			break;
		}
	if (flag){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				if (i==j)
					dp[i][j]=dp[i-1][j];
				else dp[i][j]=(dp[i-1][j]+dp[i][j-1])%mod;
		printf("%lld",(dp[n][n]%mod+mod)%mod);
		return 0;
	}
	memset(g,-1,sizeof(g));
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if (a[i]==b[j]){
				ni=i,nj=j;
				cur=0;
				dp[i][j]=(dp[i-1][j]+dp[i][j-1]-dfs(i,j-1,i-1,j))%mod;
				for(int k=1;k<=cur;k++)
					g[tmp[k].first][tmp[k].second]=-1;
			}else dp[i][j]=(dp[i-1][j]+dp[i][j-1])%mod;
			// printf("dp[%d][%d]=%lld\n",i,j,dp[i][j]);
		}
	printf("%lld",(dp[n][n]%mod+mod)%mod);
}