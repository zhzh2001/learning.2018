#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=100005;
int seed,id[N],a[N],n,K,opt,pos;
inline int nxt(int i)
{
	seed=(ll)seed*48271%2147483647;
	return seed%(n-i+1)+i;
}
inline void write(int x)
{
	int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
}
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	cin>>n>>opt>>K;
	if (opt==4)
		cin>>seed;
	for (int i=1;i<=n;++i)
		id[i]=i;
	for (int i=1;i<=n;++i)
	{
		if (opt==1)
			pos=i;
		if (opt==2)
			pos=n;
		if (opt==3)
			pos=(i+n)/2;
		if (opt==4)
			pos=nxt(i);
		// cout<<pos<<' ';
		a[id[pos]]=i;
		swap(id[pos],id[i]);
	}
	// puts("");
	for (int i=1;i<=n;++i)
	{
		write(a[i]);
		putchar(' ');
	}
	return 0;
}