#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int mod=1e9+7;
inline void read(int &x)
{
	char c=getchar();
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
const int N=55;
int n,a[N],dp[N][N][N],f[N],g[N],ans[3],ANS,opt,p[N],cnt,nxj;
inline int ksm(int x,ll y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
inline void work(int x,int y)
{
	if (x%2==y)
	{
		cout<<ksm(2,(ll)n*(n-1)/2);
	}
	else
	{
		puts("0");
	}
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	read(n);
	read(opt);
	if (n>10000)
	{
		work(n,opt);
		return 0;
	}
	dp[0][0][0]=1;
	for (int i=1;i<=n;++i)
	{
		read(a[i]);
		if (a[i]==-1)
			p[cnt++]=i;
	}
	for (int K=0;K<(1<<cnt);++K)
	{
		for (int i=0;i<cnt;++i)
			a[p[i]]=(K>>i)&1;
		for (int i=1;i<=n;++i)
		{
			for (int j=0;j<=n;++j)
				for (int k=0;k<=n;++k)
					dp[i][j][k]=0;
			f[i]=f[i-1]+(a[i]==0);
			g[i]=g[i-1]+(a[i]==1);
		}
		// for (int i=0;i<n;++i)
		// 	if (a[i+1]==0)
		// 	{
		// 		for (int j=0;j<=2&&j<=f[i];++j)
		// 		{
		// 			for (int k=1;k<=2&&j<=g[i];++k)
		// 			{
		// 				dp[i+1][j][k]=((ll)dp[i+1][j][k]+((ll)dp[i][j][k]<<(g[i]-1))%mod)%mod;
		// 				dp[i+1][3-j][k]=((ll)dp[i+1][3-j][k]+((ll)dp[i][j][k]<<(g[i]-1))%mod)%mod;
		// 			}
		// 			nxj=j==2?1:j+1;
		// 			dp[i+1][nxj][0]=((ll)dp[i+1][nxj][0]+((ll)dp[i][j][0]<<g[i])%mod)%mod;
		// 		}
		// 	}
		// 	else
		// 	{
		// 		for (int j=0;j<=2&&j<=g[i];++j)
		// 		{
		// 			for (int k=1;k<=2&&k<=f[i];++k)
		// 			{
		// 				dp[i+1][k][j]=((ll)dp[i+1][k][j]+((ll)dp[i][k][j]<<(f[i]-1))%mod)%mod;
		// 				dp[i+1][k][3-j]=((ll)dp[i+1][k][3-j]+((ll)dp[i][k][j]<<(f[i]-1))%mod)%mod;
		// 			}
		// 			nxj=j==2?1:j+1;
		// 			dp[i+1][0][nxj]=((ll)dp[i+1][0][nxj]+((ll)dp[i][0][j]<<f[i])%mod)%mod;
		// 		}
		// 	}
		// for (int i=0;i<=2;++i)
		// 	for (int j=0;j<=2;++j)
		// 		ans[(i+j)&1]+=dp[n][i][j];
		// ans[0]<<=(f[n]*(f[n]-1)/2);
		// ans[1]<<=(f[n]*(f[n]-1)/2);
		// ans[0]<<=(g[n]*(g[n]-1)/2);
		// ans[1]<<=(g[n]*(g[n]-1)/2);
		// cout<<ans[opt]<<' ';
		for (int i=0;i<n;++i)
		for (int j=0;j<=f[i];++j)
			for (int k=0;k<=g[i];++k)
				// if (a[i]==0)
				// {
				// 	// dp[i][j][k]=(k==0?(j==0?0:(dp[i-1][j-1][k]<<g[i])):((j==0?dp[i-1][j][k]:(dp[i-1][j][k]+dp[i-1][j-1][k])))<<(g[i]-1));
				// 	// dp[i-1][j][k]->dp[i][j][k];
				// 	// dp[i-1][j-1][k]->dp[i][j][k];
				// 	if (j==0&&k==0)
				// 		continue;

				// }
				// else
				// {
				// 	// dp[i][j][k]=(f[i-1]==0?(k==0?0:dp[i-1][j][k-1]):(k==0?dp[i-1][j][k]:(dp[i-1][j][k]+dp[i-1][j][k-1]))<<(f[i-1]-1));
				// 	// dp[i-1][j][k]->dp[i][j][k];
				// 	// dp[i-1][j][k-1]->dp[i][j][k];
				// }
				if (a[i+1]==0)
				{
					if (k!=0)
					{
						dp[i+1][j][k]+=dp[i][j][k]<<(g[i]-1);
						dp[i+1][j+1][k]+=dp[i][j][k]<<(g[i]-1);
					}
					else
						dp[i+1][j+1][k]+=dp[i][j][k]<<g[i];
				}
				else
				{
					if (j!=0)
					{
						dp[i+1][j][k]+=dp[i][j][k]<<(f[i]-1);
						dp[i+1][j][k+1]+=dp[i][j][k]<<(f[i]-1);
					}
					else
						dp[i+1][j][k+1]+=dp[i][j][k]<<f[i];
				}
		ans[0]=0;
		ans[1]=0;
		for (int i=0;i<=f[n];++i)
			for (int j=0;j<=g[n];++j)
				ans[(i+j)&1]+=dp[n][i][j];
		ans[0]=(ll)ans[0]*ksm(2,f[n]*(f[n]-1)/2+g[n]*(g[n]-1)/2)%mod;
		ans[1]=(ll)ans[1]*ksm(2,f[n]*(f[n]-1)/2+g[n]*(g[n]-1)/2)%mod;
		// ans[0]<<=(f[n]*(f[n]-1)/2);
		// ans[1]<<=(f[n]*(f[n]-1)/2);
		// ans[0]<<=();
		// ans[1]<<=(g[n]*(g[n]-1)/2);
		// cout<<ans[opt]<<' ';
		(ANS+=ans[opt])%=mod;
	}
	cout<<ANS;
	return 0;
}