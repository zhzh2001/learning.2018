#include<bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	char c=getchar();
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
const int N=25;
int a[N],f[N],g[N],dp[N][N][N];
int ans[2];
int n;
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
		read(a[i]);
	for (int i=1;i<=n;++i)
	{
		f[i]=f[i-1]+(a[i]==0);
		g[i]=g[i-1]+(a[i]==1);
	}
	dp[0][0][0]=1;
	for (int i=0;i<n;++i)
		for (int j=0;j<=f[i];++j)
			for (int k=0;k<=g[i];++k)
				// if (a[i]==0)
				// {
				// 	// dp[i][j][k]=(k==0?(j==0?0:(dp[i-1][j-1][k]<<g[i])):((j==0?dp[i-1][j][k]:(dp[i-1][j][k]+dp[i-1][j-1][k])))<<(g[i]-1));
				// 	// dp[i-1][j][k]->dp[i][j][k];
				// 	// dp[i-1][j-1][k]->dp[i][j][k];
				// 	if (j==0&&k==0)
				// 		continue;

				// }
				// else
				// {
				// 	// dp[i][j][k]=(f[i-1]==0?(k==0?0:dp[i-1][j][k-1]):(k==0?dp[i-1][j][k]:(dp[i-1][j][k]+dp[i-1][j][k-1]))<<(f[i-1]-1));
				// 	// dp[i-1][j][k]->dp[i][j][k];
				// 	// dp[i-1][j][k-1]->dp[i][j][k];
				// }
				if (a[i+1]==0)
				{
					if (k!=0)
					{
						dp[i+1][j][k]+=dp[i][j][k]<<(g[i]-1);
						dp[i+1][j+1][k]+=dp[i][j][k]<<(g[i]-1);
					}
					else
						dp[i+1][j+1][k]+=dp[i][j][k]<<g[i];
				}
				else
				{
					if (j!=0)
					{
						dp[i+1][j][k]+=dp[i][j][k]<<(f[i]-1);
						dp[i+1][j][k+1]+=dp[i][j][k]<<(f[i]-1);
					}
					else
						dp[i+1][j][k+1]+=dp[i][j][k]<<f[i];
				}
	for (int i=0;i<=f[n];++i)
		for (int j=0;j<=g[n];++j)
			ans[(i+j)&1]+=dp[n][i][j];
	ans[0]<<=(f[n]*(f[n]-1)/2);
	ans[1]<<=(f[n]*(f[n]-1)/2);
	ans[0]<<=(g[n]*(g[n]-1)/2);
	ans[1]<<=(g[n]*(g[n]-1)/2);
	cout<<ans[0]<<' '<<ans[1];
}