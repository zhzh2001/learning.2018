#include<bits/stdc++.h>
using namespace std;
const int LGN=17;
const int N=200005;
vector<int> e[N];
vector<int> v[N];
struct bus
{
	int x,y;
}q[N];
int fa[N][LGN+1],f[N][LGN+1];
int n,x,y,z,m,t;
int h[N];
int ans;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void dfs(int x)
{
	for (int i=1;i<=LGN;++i)
		fa[x][i]=fa[fa[x][i-1]][i-1];
	for (unsigned i=0;i<e[x].size();++i)
	{
		h[e[x][i]]=h[x]+1;
		dfs(e[x][i]);
	}
}
inline void dfs1(int x)
{
	for (unsigned i=0;i<e[x].size();++i)
	{
		dfs1(e[x][i]);
		if (h[f[e[x][i]][0]]<h[f[x][0]]&&h[f[e[x][i]][0]]!=0)
			f[x][0]=f[e[x][i]][0];
	}
	if (f[x][0]==x)
		f[x][0]=0;
}
inline void dfs2(int x)
{
	for (int i=1;i<=LGN;++i)
		f[x][i]=f[f[x][i-1]][i-1];
	for (unsigned i=0;i<e[x].size();++i)
		dfs2(e[x][i]);
}
inline int kth(int x,int p)
{
	for (int i=0;i<=LGN;++i)
		if ((p>>i)&1)
			x=fa[x][i];
	return x;
}
inline void writeln(int x)
{
	int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
inline int lca(int x,int y)
{
	if (h[x]<h[y])
		swap(x,y);
	int p=h[x]-h[y];
	x=kth(x,p);
	// for (int i=0;i<=LGN;++i)
	// 	if ((p>>i)&1)
	// 		x=fa[x][i];
	for (int i=LGN;i>=0;--i)
		if (fa[x][i]!=fa[y][i])
		{
			x=fa[x][i];
			y=fa[y][i];
		}
	return x==y?x:fa[x][0];
}
inline int up(int x,int y)
{
	int ans=0;
	for (int i=LGN;i>=0;--i)
		if (h[f[x][i]]>h[y])
		{
			ans+=1<<i;
			x=f[x][i];
		}
	return f[x][0]==0?-1:ans+1;
}
inline int check(int x,int y,int _x,int _y)
{
	if (h[x]>h[_x]||kth(_x,h[_x]-h[x])!=x)
		return 0;
	if (h[y]>h[_y]||kth(_y,h[_y]-h[y])!=y)
		return 0;
	return 1;
}
inline int pd(int x,int y,int z)
{
	if (v[z].size()>2000)
		return 0;
	for (unsigned i=0;i<v[z].size();++i)
		if (check(x,y,q[v[z][i]].x,q[v[z][i]].y)||check(x,y,q[v[z][i]].y,q[v[z][i]].x))
			return 0;
	return 1;
}
inline int work(int x,int y,int z)
{
	int ans1=0;
	for (int i=LGN;i>=0;--i)
		if (h[f[x][i]]>h[z])
		{
			ans1+=1<<i;
			x=f[x][i];
		}
	if (f[x][0]==0)
		return -1;
	int ans2=0;
	for (int i=LGN;i>=0;--i)
		if (h[f[y][i]]>h[z])
		{
			ans2+=1<<i;
			y=f[y][i];
		}
	if (f[y][0]==0)
		return -1;
	return ans1+ans2+pd(x,y,z)+1;
}
int main()
{
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	read(n);
	for (int i=2;i<=n;++i)
	{
		read(fa[i][0]);
		e[fa[i][0]].push_back(i);
	}
	h[1]=1;
	h[0]=n+1;
	dfs(1);
	read(m);
	for (int i=1;i<=n;++i)
		f[i][0]=i;
	for (int i=1;i<=m;++i)
	{
		read(q[i].x);
		read(q[i].y);
		if (q[i].x==q[i].y)
			continue;
		z=lca(q[i].x,q[i].y);
		if (z==q[i].x)
		{
			if (h[f[q[i].y][0]]>h[z])
				f[q[i].y][0]=z;
		}
		if (z==q[i].y)
		{
			if (h[f[q[i].x][0]]>h[z])
				f[q[i].x][0]=z;
		}
		if (z!=q[i].x&&z!=q[i].y)
		{
			v[z].push_back(i);
			if (h[f[q[i].y][0]]>h[z])
				f[q[i].y][0]=z;
			if (h[f[q[i].x][0]]>h[z])
				f[q[i].x][0]=z;
		}
	}
	h[0]=0;
	dfs1(1);
	dfs2(1);
	read(t);
	while (t--)
	{
		read(x);
		read(y);
		if (x==y)
		{
			puts("0");
			continue;
		}
		z=lca(x,y);
		if (z==x)
		{
			int ans=up(y,z);
			if (ans==-1)
				puts("-1");
			else
				writeln(ans);
		}
		if (z==y)
		{
			int ans=up(x,z);
			if (ans==-1)
				puts("-1");
			else
				writeln(ans);
		}
		if (z!=x&&z!=y)
		{
			int ans=work(x,y,z);
			if (ans==-1)
				puts("-1");
			else
				writeln(ans);
		}
	}
	return 0;
}