#include<bits/stdc++.h>
#define For(i,j,k)  for(int i=j;i<=k;++i)
#define Dow(i,j,k)  for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
    ll t=0,f=1;char c=getchar();
	while(!isdigit(c)) {if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
int n,a[500001],cnt,mx,b[500001],t[500001],flag,tp,mb_cnt;
ll seed;
inline ll Rand(int l,int r)
{
	seed=seed*48271%2147483647;
	return seed%(r-l+1)+l;
}
inline void Sort(int a[],int l,int r)
{
	int i=l,j=r;
	int tt=Rand(l,r);
	if(!flag)b[t[tt]]=a[tt]=mx;mx--;
	swap(t[tt],t[r]);swap(a[tt],a[r]);
	if(l<r)Sort(a,l,r-1);
}
inline void Sort1(int a[],int l,int r)
{
	int i=l,j=r;
	int tt=l;
	if(!flag)b[t[tt]]=a[tt]=mx;mx--;
	swap(t[tt],t[r]);swap(a[tt],a[r]);
	if(l<r)Sort1(a,l,r-1);
}
inline void Sort2(int a[],int l,int r)
{
	int i=l,j=r;
	int tt=r;
	if(!flag)b[t[tt]]=a[tt]=mx;mx--;
	swap(t[tt],t[r]);swap(a[tt],a[r]);
	if(l<r)Sort2(a,l,r-1);
}
inline void Sort3(int a[],int l,int r)
{
	int i=l,j=r;
	int tt=(l+r)/2;
	if(!flag)b[t[tt]]=a[tt]=mx;mx--;
	swap(t[tt],t[r]);swap(a[tt],a[r]);
	if(l<r)Sort3(a,l,r-1);
}

int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();mx=n;
	tp=read();mb_cnt=read();
	if(tp==4)	seed=read();
	For(i,1,n)	a[i]=0,t[i]=i;
	if(tp==4)	Sort(a,1,n);
	else
	{
		if(tp==1)	Sort1(a,1,n);
		if(tp==2)	Sort2(a,1,n);
		if(tp==3)	Sort3(a,1,n);
	}
	Dow(i,1,n)	if(!b[i])	b[i]=mx--;
	For(i,1,n)	write_p(b[i]);
}
