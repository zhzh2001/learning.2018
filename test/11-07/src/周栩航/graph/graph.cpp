#include<bits/stdc++.h>
#define For(i,j,k)  for(int i=j;i<=k;++i)
#define Dow(i,j,k)  for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
    ll t=0,f=1;char c=getchar();
	while(!isdigit(c)) {if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=7;
int n,p,col[N],ans,d[N][N],in[N],dp[N];
ll mo=1e9+7;
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2,x=x*x%mo)	if(y&1)	sum=sum*x%mo;	return sum;}
inline void check()
{
	For(i,1,n)	dp[i]=1;
	For(i,1,n)	For(j,i+1,n)	if(d[i][j])	in[j]++;
	queue<int> Q;
	For(i,1,n)	if(!in[i])	Q.push(i);
	while(!Q.empty())
	{
		int x=Q.front();Q.pop();
		For(i,x+1,n)
		if(d[x][i])
		{
			in[i]--;
			if(col[x]==1-col[i])	dp[i]+=dp[x];
			if(in[i]==0)	Q.push(i);
		}
	}
	int tmp=0;
	For(i,1,n)	tmp+=dp[i];
	if((tmp&1)==p)		ans++;
}
inline void Dfs(int x,int y)
{
	if(x==n)	{check();return;}
	if(y==n+1)	{Dfs(x+1,x+2);return;}
	d[x][y]=1;Dfs(x,y+1);d[x][y]=0;Dfs(x,y+1);
}
inline void Dfs_col(int x)
{
	if(x==n+1)	{Dfs(1,2);return;}
	if(col[x]!=-1)	{Dfs_col(x+1);return;}
	col[x]=1;Dfs_col(x+1);col[x]=0;Dfs_col(x+1);
	col[x]=-1;
}
int main()
{
	freopen("graph.in","r",stdin);freopen("graph.out","w",stdout);
	n=read();p=read();
	if(n>=10)
	{
		if((n&1)==p)
		{
			writeln(ksm(2,1LL*n*(n-1)/2));
			return 0;
		}else	
		{
			writeln(0);
			return 0;
		}
	}
	For(i,1,n)	col[i]=read();
	Dfs_col(1);
	writeln(ans);
}
