#include<bits/stdc++.h>
#define For(i,j,k)  for(int i=j;i<=k;++i)
#define Dow(i,j,k)  for(int i=k;i>=j;--i)
#define ll long long
#define mk make_pair
using namespace std;
inline ll read()
{
    ll t=0,f=1;char c=getchar();
	while(!isdigit(c)) {if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=100005;
int Fa[N],head[N],nxt[N],poi[N],l[N],r[N],id[N],top,q[N],cnt,que,m,d[N],n;
int tim,in[N],out[N];
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;}
inline bool cmp(int x,int y){return l[x]!=l[y]?l[x]<l[y]:r[x]>r[y];}
inline void main3()
{
	m=read();
	For(i,1,m)	l[i]=read(),r[i]=read(),id[i]=i;
	For(i,1,m)	if(l[i]>r[i])	swap(l[i],r[i]);
	sort(id+1,id+m+1,cmp);
	top=1;
	q[top]=id[1];
	For(i,2,m)	if(r[id[i]]>r[q[top]])	q[++top]=id[i];
	que=read();
	For(i,2,top)	if(l[q[i]]>r[q[i-1]])	d[i]=1;
	For(i,1,top)	d[i]+=d[i-1];
	For(i,1,que)
	{
		int s=read(),e=read();
		if(s==e)	{writeln(0);continue;}
		if(s>e)	swap(s,e);
		int tl=0,tr=top,ans1=0;
		while(tl<=tr)
		{
			int mid=tl+tr>>1;
			if(l[q[mid]]<=s)	ans1=mid,tl=mid+1;else	tr=mid-1;
		}
		tl=0;tr=top;int ans2=0;
		while(tl<=tr)
		{
			int mid=tl+tr>>1;
			if(r[q[mid]]>=e)	ans2=mid,tr=mid-1;else	tl=mid+1;
		}
		if(d[ans2]==d[ans1])	writeln(ans2-ans1+1);else	puts("-1");
	}
	exit(0);
}
map<pair<int,int>,int >vis;
int alr[N];
inline void main2()
{
	m=read();
	For(i,1,m)	
	{
		int x=read(),y=read();
		alr[x]=1;alr[y]=1;
		vis[mk(x,y)]=1;
	}
	For(i,1,n)	if(alr[i])	vis[mk(1,i)]=1;
	que=read();
	For(i,1,que)
	{
		int x=read(),y=read();
		if(x==y)	{writeln(0);continue;}
		if(vis[mk(x,y)]||vis[mk(y,x)])	puts("1");
		else	if(alr[x]&&alr[y])	puts("2");
		else	puts("-1");
	}
	exit(0);
}
int dep[N],dis[N],lca[N];
inline void Dfs(int x)
{
	in[x]=++tim;
	for(int i=head[x];i;i=nxt[i])	dep[poi[i]]=dep[x]+1,Dfs(poi[i]);
	out[x]=tim;
}

inline int  LCA(int x,int y)
{
	if(x==y)	return x;
	while(x!=y)
	{
		if(dep[x]<dep[y])	swap(x,y);
		x=Fa[x];
	}
	return x;
}
inline bool inc(int x,int y)//y shi bushi x de zi shu 
{return in[x]<=in[y]&&out[y]<=out[x];}
inline void Bfs(int s,int e)
{
	queue<int> Q;
	For(i,1,n)	dis[i]=0;
	dis[s]=1;
	Q.push(s);
	while(!Q.empty())
	{
		int x=Q.front();Q.pop();
		For(i,1,m)	if(x==lca[i]||(inc(x,l[i])^inc(x,r[i])))
		{
			int p1=l[i],p2=r[i];
			while(p1!=lca[i])
			{
				if(!dis[p1])	dis[p1]=dis[x]+1,Q.push(p1);//cout<<"push"<<p1<<endl;
				p1=Fa[p1];
			}
			while(1)
			{
				if(!dis[p2])	dis[p2]=dis[x]+1,Q.push(p2);//cout<<"push"<<p2<<endl;
				if(p2==lca[i])	break;
				p2=Fa[p2];	
			}
		}
	}
	if(!dis[e])	writeln(-1);else	writeln(dis[e]-1);
}
int main()
{
	freopen("bus.in","r",stdin);freopen("bus.out","w",stdout);
	bool flag1=1,flag2=1;
	n=read();
	For(i,2,n)	Fa[i]=read(),add(Fa[i],i);
	For(i,2,n)	if(Fa[i]!=1)	flag1=0;
	For(i,2,n)	if(Fa[i]!=i-1)	flag2=0;
	if(flag2)	main3();
	if(flag1)	main2();
	Dfs(1);
	m=read();
	For(i,1,m)	l[i]=read(),r[i]=read(),lca[i]=LCA(l[i],r[i]);
	que=read();
	For(i,1,que)
	{
		int s=read(),e=read();
		if(s==e){writeln(0);continue;}
		Bfs(s,e);
	}
}
