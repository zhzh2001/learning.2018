#include<bits/stdc++.h>
using namespace std;
int n,type,cnt;int seed;
inline int F(int l,int r)
{
	if(type==3) return (l+r)>>1;
	seed=1ll*seed*48271%2147483647;
	return seed%(r-l+1)+l;
}
int a[100100];
inline void sort(int l,int r,int cnt)
{
	if(l==r){a[l]=cnt;return;}
	int x=F(l,r);
	a[x]=cnt;swap(a[x],a[l]);
	sort(l+1,r,cnt+1);
	swap(a[x],a[l]);
}
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	scanf("%d%d%d",&n,&type,&cnt);
	if(type==1||type==2)
	{
		for(int i=1;i<=n;i++) printf("%d ",i);
		return 0;
	}
	if(type==4) scanf("%d",&seed);
	sort(1,n,1);
	for(int i=1;i<=n;i++) printf("%d ",a[i]);
}
