#include<bits/stdc++.h>
using namespace std;
#define maxn 10
#define mod 1000000007
inline int ksm(int a,int b)
{
	int res=1;
	for(;b;b>>=1,a=1ll*a*a%mod) if(b&1) res=1ll*res*a%mod;
	return res;
}
int col[maxn];bool edge[maxn][maxn];int ans=0;int n,p;
int f[maxn];
inline bool check()
{
	memset(f,0,sizeof(f));
	for(int i=1;i<=n;i++) f[i]=1;
	for(int i=1;i<=n;i++) for(int j=1;j<=n;j++) if(edge[i][j]&&(col[i]^col[j])) f[j]+=f[i],f[j]&=1;
	int all=0;for(int i=1;i<=n;i++) all+=f[i];all&=1;return all==p;
}
inline void dfs2(int x,int y)
{
	if(x==n){if(check()) ans++;return;}
	edge[x][y]=1;if(y==n) dfs2(x+1,x+2);else dfs2(x,y+1);
	edge[x][y]=0;if(y==n) dfs2(x+1,x+2);else dfs2(x,y+1);
}
inline void dfs1(int now)
{
	if(now>n){dfs2(1,2);return;}
	if(col[now]!=-1){dfs1(now+1);return;}
	col[now]=0;dfs1(now+1);
	col[now]=1;dfs1(now+1);
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	scanf("%d%d",&n,&p);
	if(n>6){printf("%d",n%2==p?ksm(2,1ll*n*(n-1)/2%(mod-1)):0);return 0;}
	for(int i=1;i<=n;i++) scanf("%d",col+i);
	dfs1(1);printf("%d",ans);return 0;
}
