#include<bits/stdc++.h>
using namespace std;
#define maxn 200100
namespace Solve2
{
	bool tofa[maxn];set<int> S[maxn];
	inline void solve2()
	{
		int m;scanf("%d",&m);
		for(int i=1;i<=m;i++)
		{
			int a,b;scanf("%d%d",&a,&b);
			if(a!=1&&b!=1) S[a].insert(b),S[b].insert(a);
			tofa[a]=tofa[b]=1;
		}
		int q;scanf("%d",&q);
		for(int i=1;i<=q;i++)
		{
			int a,b;scanf("%d%d",&a,&b);
			if(a==b) puts("0");
			else if(tofa[a]==0||tofa[b]==0) puts("-1");
			else
			{
				if(a==1||b==1||S[a].count(b)) puts("1");
				else puts("2");
			}
		}
		exit(0);
	}
}
int fa[maxn],n;
namespace Solve1
{
	int f[maxn][20];
	struct segment_tree
	{
		#define ls (p<<1)
		#define rs (p<<1|1)
		int maxv[maxn<<2],cov[maxn<<2];
		inline void pushup(int p){maxv[p]=maxv[ls]+maxv[rs];}
		inline void setcov(int p,int val){maxv[p]=max(maxv[p],val);cov[p]=max(cov[p],val);}
		inline void pushdown(int p)
		{
			if(cov[p]==0) return;
			setcov(ls,cov[p]);
			setcov(rs,cov[p]);
			cov[p]=0;
		}
		inline void cover(int p,int l,int r,int ql,int qr,int Val)
		{
			if(ql<=l&&qr>=r) return (void)setcov(p,Val);
			int mid=(l+r)>>1;pushdown(p);
			if(ql<=mid) cover(ls,l,mid,ql,qr,Val);
			if(qr>mid) cover(rs,mid+1,r,ql,qr,Val);
			pushup(p);
		}
		inline void OUTPUT(int p,int l,int r)
		{
			if(l==r) return (void)(f[l][0]=maxv[p]);
			pushdown(p);int mid=(l+r)>>1;
			OUTPUT(ls,l,mid);
			OUTPUT(rs,mid+1,r);
		}
	}seg;
	int lst[maxn];
	inline void solve1()
	{
		int m;scanf("%d",&m);
		for(int i=1,a,b;i<=m;i++)
		{
			scanf("%d%d",&a,&b);
			if(a>b) swap(a,b);
			if(a!=b)seg.cover(1,1,n,a,b-1,b);
		}
		seg.OUTPUT(1,1,n);
		lst[n]=n+1;
		for(int i=n;i;i--) if(f[i][0]==0) lst[i]=i;else lst[i]=lst[i+1];
		for(int j=1;j<20;j++) for(int i=1;i<=n;i++) f[i][j]=f[f[i][j-1]][j-1];
		int q;scanf("%d",&q);
		for(int i=1,a,b;i<=q;i++)
		{
			scanf("%d%d",&a,&b);
			if(a>b) swap(a,b);
			if(a==b){puts("0");continue;}
			if(lst[a]<b){puts("-1");continue;}
			int ans=0;for(int j=19;j>=0;j--) if(f[a][j]<b&&f[a][j]!=0) a=f[a][j],ans+=1<<j;
			printf("%d\n",ans+1);
		}
		exit(0);
	}
}
namespace BF
{
	#define mn 5010
	int head[mn],nxt[mn<<1],ver[mn<<1],tot;
	int Fa[mn];
	inline void addedge(int a,int b)
	{
		nxt[++tot]=head[a];ver[tot]=b;head[a]=tot;
		nxt[++tot]=head[b];ver[tot]=a;head[b]=tot;
	}
	int id[mn],size[mn],id_cnt,fa[mn],dep[mn];
	inline void dfs(int x,int fat)
	{
		id[x]=++id_cnt;size[x]=1;fa[x]=fat;dep[x]=dep[fat]+1;
		for(int i=head[x];i;i=nxt[i])
		{
			int y=ver[i];if(y==fat) continue;
			dfs(y,x);size[x]+=size[y];
		}
	}
	inline bool isinsubtree(int x,int rt){return id[x]>=id[rt]&&id[x]<=id[rt]+size[rt]-1;}
	inline int lca(int a,int b)
	{
		while(!isinsubtree(a,b)) b=fa[b];
		return b;
	}
	int upto[mn];
	int u[mn],v[mn],m;
	inline void solve(int a,int b)
	{
		id_cnt=0;dfs(a,0);
		//root=a;
		memset(upto,0,sizeof(upto));
		for(int i=1;i<=m;i++)
		{
			int L=lca(u[i],v[i]);
			int now=u[i];
			while(now!=L)
			{
				if(upto[now]==0||dep[upto[now]]>dep[L]) upto[now]=L;
				now=fa[now];
			}
			now=v[i];
			while(now!=L)
			{
				if(upto[now]==0||dep[upto[now]]>dep[L]) upto[now]=L;
				now=fa[now];
			}
		}
		int cur=b,cnt=0;
		while(upto[cur]&&cur!=a) cnt++,cur=upto[cur];
		if(cur!=a) puts("-1");else printf("%d\n",cnt);
	}
	inline void bf()
	{
		for(int i=2;i<=n;i++) addedge(i,Fa[i]);scanf("%d",&m);
		for(int i=1;i<=m;i++) scanf("%d%d",u+i,v+i);
		int q;scanf("%d",&q);
		for(int i=1;i<=q;i++)
		{
			int a,b;scanf("%d%d",&a,&b);
			solve(a,b);
		}
	}
}
int main()
{
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	bool Flag1,Flag2;Flag1=Flag2=1;
	scanf("%d",&n);
	for(int i=2;i<=n;i++)
	{
		scanf("%d",fa+i);
		if(fa[i]!=i-1) Flag1=0;
		if(fa[i]!=1) Flag2=0;
	}
	if(Flag2) Solve2::solve2();
	if(Flag1) Solve1::solve1();
	if(n<=5000)
	{
		for(int i=2;i<=n;i++) BF::Fa[i]=fa[i];
		BF::bf();
	}
	return 0;
}
