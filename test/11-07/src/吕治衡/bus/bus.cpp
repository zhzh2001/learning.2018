#include<bits/stdc++.h>
//#define int long long
#define minn(x,y) (de[x]<de[y]?x:y)
#define N 200010
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=0,kk=1;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return k*kk; 
}void write(int x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(int x){write(x);puts("");}
using namespace std;
struct lsg{int x,y,z,k;}que[500010],qwq[500010],q[500010];
int dfn[N],low[N],a[N*2][2],last[N],f[22][N],fh[N],g[N];
int n,m,ans[N],kk,kkk,ddd,dd,de[N],flag,ff[N],faq[N],dian[N];
int x,y,z;
void doit(int x,int y){a[++kk][0]=last[x];a[kk][1]=y;last[x]=kk;}//建边
void add(int i,int j,int z){//添加一个x,y,z的询问，在同一子树内就直接减去即可 
	if (dfn[i]>dfn[j])swap(i,j);
	if (low[i]>=low[j]){ans[z]--;return;}
	int x=dfn[i],xx=low[i],y=dfn[j],yy=low[j];
	que[++ddd].x=x-1;que[ddd].y=y;que[ddd].z=yy;que[ddd].k=z;
	que[++ddd].x=xx;que[ddd].y=y;que[ddd].z=yy;que[ddd].k=z;
}
void put(int x,int y){//加入一个x,y的点 
	if (dfn[x]>dfn[y])swap(x,y);
	qwq[++kkk].x=dfn[x];qwq[kkk].y=dfn[y]; 
}
void dfs(int x,int fa,int dee){//建立lca的倍增表，处理dfn和low
	dfn[x]=++dd;de[x]=dee;f[0][x]=fa;
	for (int i=last[x];i;i=a[i][0])
		if (a[i][1]!=fa)dfs(a[i][1],x,dee+1);
	low[x]=dd;
}
inline int lca(int x,int y){//求lca 
	if (de[x]<de[y])swap(x,y);
	for (int i=20;i>=0;i--)if (de[f[i][x]]>=de[y])x=f[i][x];
	for (int i=20;i>=0;i--)if (f[i][x]!=f[i][y])x=f[i][x],y=f[i][y];
	if (x!=y)x=f[0][x];return x;
}
void fa(int x,int dd){//权值并查集
	if (de[fh[fh[x]]]>de[dd]&&fh[x]!=x){
		fa(fh[x],dd);
		g[x]+=g[fh[x]];fh[x]=fh[fh[x]];
	}else if (fh[x]==x&&de[x]>de[dd])flag=1;
}
void clc(int x,int fa){//更新最小到达深度 
	g[x]=1;
	for (int i=last[x];i;i=a[i][0]){
		clc(a[i][1],x);fh[x]=minn(fh[x],fh[a[i][1]]);
	}
}
void putit(int x){for (;x<=n;x+=(x&(-x)))ff[x]++;}//对ff数组的树状数组
int find(int x){int ans=0;for (;x;x-=(x&(-x)))ans+=ff[x];return ans;} 
bool pd(lsg x,lsg y){return x.x<y.x;} 
void cal(){//离线二维数点 
	sort(que+1,que+1+ddd,pd);sort(qwq+1,qwq+1+kkk,pd);
	memset(faq,0,sizeof(faq));int k=1;
	for (int i=1;i<=ddd;i++){
		while (qwq[k].x<=que[i].x&&k<=kkk)putit(qwq[k].y),k++;
		if (!faq[que[i].k])faq[que[i].k]=1,dian[que[i].k]-=find(que[i].z)-find(que[i].y-1);
			else dian[que[i].k]+=find(que[i].z)-find(que[i].y-1);
	}
}
bool cmp(lsg x,lsg y){return de[x.z]>de[y.z];}
signed main(){
	freopen("bus.in","r",stdin);freopen("bus.out","w",stdout);
	n=read();for (int i=1;i<n;i++)doit(read(),i+1);
	dfs(1,0,1);m=read();
	for (int i=1;i<=20;i++)for (int j=1;j<=n;j++)f[i][j]=f[i-1][f[i-1][j]];
	for (int i=1;i<=n;i++)fh[i]=i;
	for (int i=1;i<=m;i++){
		x=read();y=read();z=lca(x,y);put(x,y);
		fh[x]=minn(fh[x],z);fh[y]=minn(fh[y],z);//权值并查集即可 
	}clc(1,0);//dfs 
	m=read();kk=0;
	for (int i=1;i<=m;i++){
		x=read();y=read();z=lca(x,y);
		q[i].x=x;q[i].y=y;q[i].z=z;q[i].k=i;
		ans[i]=2;
	}sort(q+1,q+1+m,cmp);//lca从深到浅
	for (int i=1;i<=m;i++){ 
		x=q[i].x;y=q[i].y;z=q[i].z;
		flag=0;fa(x,z);fa(y,z);
		if (flag){ans[q[i].k]=-2e9;continue;}
		int xx=fh[x],yy=fh[y];
		if (de[xx]>de[z])ans[q[i].k]+=g[x];else xx=x;
		if (de[yy]>de[z])ans[q[i].k]+=g[y];else yy=y;
		add(xx,yy,q[i].k);
	}cal();for (int i=1;i<=m;i++)if (dian[i]>0)ans[i]--;
	for (int i=1;i<=m;i++)writeln(ans[i]<0?-1:ans[i]);
}/*
*/
