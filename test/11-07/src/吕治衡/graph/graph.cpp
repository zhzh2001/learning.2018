#include<bits/stdc++.h>
#define int long long
#define lsg 1000000007
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=0,kk=1;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return k*kk; 
}void write(int x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(int x){write(x);puts("");}
using namespace std;
int n,p,x,f[2][2][2],g[2][2][2],d,dd,ans;
signed main(){ 
	freopen("graph.in","r",stdin);freopen("graph.out","w",stdout);
	n=read();p=read();f[0][0][0]=1;d=1;
	for (int l=1;l<=n;l++){
		x=read();memset(g,0,sizeof(g));
		if (x!=1){//白色! 
			for (int i=0;i<=1;i++)
				for (int j=0;j<=1;j++)
					for (int k=0;k<=1;k++)
						if (f[i][j][k]){
							int xx,yy,zz;
							if (k){//有没有奇数的黑色呢 
								(g[!i][1][k]+=f[i][j][k]*dd)%=lsg;
								(g[i][j][k]+=f[i][j][k]*dd)%=lsg;
							}else (g[!i][1][0]+=f[i][j][k]*d)%=lsg;
						} 
		}
		if (x!=0){//黑色! 
			for (int i=0;i<=1;i++)
				for (int j=0;j<=1;j++)
					for (int k=0;k<=1;k++)
						if (f[i][j][k]){
							int xx,yy,zz;
							if (j){//有没有奇数的白色呢 
								(g[!i][j][1]+=f[i][j][k]*dd)%=lsg;
								(g[i][j][k]+=f[i][j][k]*dd)%=lsg;
							}else (g[!i][0][1]+=f[i][j][k]*d)%=lsg;
						} 
		}
		for (int i=0;i<=1;i++)
			for (int j=0;j<=1;j++)
				for (int k=0;k<=1;k++)f[i][j][k]=g[i][j][k]%lsg;
		ans=ans;
		dd=d;d=(d<<1)>=lsg?(d<<1)-lsg:(d<<1);
	}for (int j=0;j<=1;j++)for (int k=0;k<=1;k++)(ans+=f[p][j][k])%=lsg;
	writeln(ans);
}/*
f[i][j][k]当前价值%2=i,白色奇数点j个黑色k个的方案数 
多于一个对立奇数点时，一半可能这个点是奇数，另一半可能这个点是偶数 
否则必然是奇数。所以j=0..1 k=0..1 
10 1
-1 0 -1 1 1 -1 -1 -1 -1 -1
*/
