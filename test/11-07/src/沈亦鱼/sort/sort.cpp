#include<cstdio>
using namespace std;
int n,opt,cnt,l,mid,r,k,a[110000],fa[110000];
long long seed;
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
int gf(int x){
	if(x==fa[x])return x;
	fa[x]=gf(fa[x]);
	return fa[x];
}
void rnd(){
	seed=seed*48271%2147483647;
}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();
	opt=read();
	cnt=read();
	if(opt==1||opt==2){
		for(int i=1;i<=n;i++)
			printf("%d ",i);
		return 0;
	}
	if(opt==3){
		for(int i=1;i<=n;i++)
			fa[i]=i;
		l=1;
		r=n;
		k=1;
		while(l<=r){
			mid=(l+r)>>1;
			a[gf(mid)]=k;
			fa[mid]=l;
			l++;
			k++;
		}
		for(int i=1;i<=n;i++)
			printf("%d ",a[i]);
	}
	if(opt==4){
		seed=read();
		for(int i=1;i<=n;i++)
			fa[i]=i;
		l=1;
		r=n;
		k=1;
		while(l<=r){
			rnd();
			mid=seed%(r-l+1)+l;
			a[gf(mid)]=k;
			fa[mid]=l;
			l++;
			k++;
		}
		for(int i=1;i<=n;i++)
			printf("%d ",a[i]);
	}
	return 0;
}
