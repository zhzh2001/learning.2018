#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,q,k,fa[210000],vis[210000];
long long u,v,a[410000];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
/*
void adg(int u,int v){
	tot++;
	p[tot]=v;
	ne[tot]=he[u];
	he[u]=tot;
}
*/
void sor(int l,int r){
	int i=l,j=r;
	long long x=a[(l+r)>>1];
	while(i<=j){
		while(a[i]<x)i++;
		while(x<a[j])j--;
		if(i<=j){
			swap(a[i],a[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}
int sea(long long x){
	int l=0,mid=0,r=k+1;
	while(l+1<r){//printf("%d %d\n",l,r);
		mid=(l+r)>>1;
		if(a[mid]<=x)l=mid;
		else r=mid;
	}
	return l;
}
int main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	int ff=0;
	for(int i=2;i<=n;i++){
		fa[i]=read();
		if(fa[i]>1)ff=1;
	}
	if(ff==0){
		m=read();
		for(int i=1;i<=m;i++){
			u=read();
			v=read();
			if(u==v)continue; 
			if(u>v)swap(u,v);
			if(u==1)vis[v]=1;
			else{
				vis[u]=1;
				vis[v]=1;
				k++;
				a[k]=u*(n+1)+v;
				k++;
				a[k]=v*(n+1)+u;
			}
		}
		sor(1,k);
		q=read();
		for(int i=1;i<=q;i++){
			u=read();
			v=read();//printf("%d %d\n",u,v);
			if(u==v){
				puts("0");
				continue;
			}
			if(u>v)swap(u,v);
			if(u==1){
				if(vis[v])puts("1");
				else puts("-1");
				continue;
			}
			if(vis[u]==0||vis[v]==0){
				puts("-1");
				continue;
			}//printf("%d %lld\n",sea(u*(n+1)+v),a[sea(u*(n+1)+v)]);
			if(a[sea(u*(n+1)+v)]!=u*(n+1)+v)puts("2");
			else puts("1");
		}
		return 0;
	}
	m=read();
	for(int i=1;i<=m;i++){
		u=read();
		v=read();
//		adg(u,v);
//		adg(v,u);
	}
	q=read();
	for(int i=1;i<=q;i++){
		u=read();
		v=read();
		printf("%d\n",(u+v)/(n/3));
	}
	return 0;
}
