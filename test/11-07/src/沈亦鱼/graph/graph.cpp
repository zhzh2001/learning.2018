#include<cstdio>
using namespace std;
int mo=1000000007,n,m,ff,s,a[1100000],g[6];
long long f[1100000];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
long long pwr(long long x,long long y){
	long long z=1;
	while(y){
		if(y&1)z=z*x%mo;
		x=x*x%mo;
		y>>=1;
	}
	return z;
}
int sol(int k,int x){
	int sm=1;
	for(int i=k+1;i<=n;i++)
		if(((1<<(i-1))&x)==0&&(g[k]&(1<<(i-1)))>0&&a[i]!=a[k])sm+=sol(i,x|(1<<(i-1)));
	return sm;
}
void dfs(int k){
	if(k>n){
		int p=0;
		for(int i=1;i<=n;i++)
			p+=sol(i,1<<(i-1));
//		printf("%d %d!\n",a[1],a[2]);
//		if(a[1]==1&&a[2]==0)printf("%d\n",p);
		if(p%2==m){
			s++;//printf("%d %d\n",a[1],a[2]);
		}
		return;
	}
	if(a[k]==-1){
		a[k]=0;
		dfs(k);
		a[k]=1;
		dfs(k);
		a[k]=-1;
	}
	else{
		for(int i=0;i<(1<<n);i+=1<<k){
			g[k]=i;
			dfs(k+1);
		}
	}
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		if(a[i]>-1)ff=1;
	}
	if(n>5&&ff){
		if(n%2==m)printf("%lld",pwr(2,(long long)n*(n-1)/2));
		else puts("0");
		return 0;
	}
	if(n>5&&!ff){
//		f[1]=1;
//		f[2]=2;
//		for(int i=3;i<=n;i++)
//			f[i]=(f[i-1]+f[i-2])%mo;
		long long x=n;
		x=x*(x+1)/2;
		x=pwr(2,x);
//		long long y=pwr(2,f[n]);
		long long y=n-2;
		y=y*(y+1)/2;
		y=pwr(2,y+2);
		x=x-y;
		if(x<0)x+=mo;
		if(x&1)x+=mo;
		x/=2;
		if(n%2==m)printf("%lld",(x+y)%mo);
		else printf("%lld",x%mo);
		return 0;
	}
	s=0;
	dfs(1);
	printf("%d",s);
	return 0;
}
