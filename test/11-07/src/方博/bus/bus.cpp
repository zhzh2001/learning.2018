#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){
	ll x=0,f=0;char ch=getchar();
	while(ch<'0'||ch>'9')f|=(ch=='-'),ch=getchar();
	while(ch>='0'&&ch<='9')x=(x<<3)+(x<<1)+ch-48,ch=getchar();
	return f?-x:x;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x),puts("");
}
const int N=200005;
int n,m,q,p[N];
int head[N],to[2*N],nxt[2*N],tot;
inline void add(ll u,ll v){
	to[++tot]=v,nxt[tot]=head[u],head[u]=tot;
}
int dep[N],in[N],out[N],tim,pos[N],top[N];
int sz[N],son[N];
int f[N][23],st[N][23];
int u[N],v[N];
inline int lca(int x,int y){
	if(dep[x]>dep[y])swap(x,y);
	for(int i=20;i>=0;i--){
		if(dep[f[y][i]]>=dep[x])y=f[y][i];
	}
	if(x==y)return x;
	for(int i=20;i>=0;i--){
		if(f[y][i]!=f[x][i])x=f[x][i],y=f[y][i];
	}
	return f[x][0];
}
void dfs(int x,int fa){
//	f[x][0]=fa;
	dep[x]=dep[fa]+1;
	sz[x]=1;
	for(int i=head[x];i;i=nxt[i]){
		if(to[i]!=fa){
			dfs(to[i],x);
			sz[x]+=sz[to[i]];
			if(!son[x]||sz[to[i]]>sz[son[x]]){
				son[x]=to[i];
			}
		}
	}
}
void dfs2(int x,int tp){
	in[x]=++tim;
	pos[tim]=x;
	top[x]=tp;
	if(son[x]){
		dfs2(son[x],tp);
	}
	for(int i=head[x];i;i=nxt[i]){
		if(to[i]!=f[x][0]&&to[i]!=son[x]){
			dfs2(to[i],to[i]);
		}
	}
	out[x]=tim;
}
const int M=100000000;
struct tree{
	int mn,tag;
}t[800003];
inline void pushdown(int rt){
	if(t[rt].tag!=M){
		t[rt<<1].tag=min(t[rt].tag,t[rt<<1].tag);
		t[rt<<1|1].tag=min(t[rt].tag,t[rt<<1|1].tag);
		t[rt<<1].mn=min(t[rt].tag,t[rt<<1].mn);
		t[rt<<1|1].mn=min(t[rt].tag,t[rt<<1|1].mn);
		t[rt].tag=M;
	}
}
void build(int rt,int l,int r){
	t[rt].mn=M,t[rt].tag=M;
	if(l==r){
		return;
	}
	int mid=(l+r)>>1;
	build(rt<<1,l,mid),build(rt<<1|1,mid+1,r);
}
void change(int rt,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
		t[rt].tag=min(t[rt].tag,val);
		t[rt].mn=min(t[rt].mn,val);
		return;
	}
	pushdown(rt);
	int mid=(l+r)/2;
	if(y<=mid)change(rt<<1,l,mid,x,y,val);
	else if(x>mid)change(rt<<1|1,mid+1,r,x,y,val);
	else change(rt<<1,l,mid,x,mid,val),change(rt<<1|1,mid+1,r,mid+1,y,val);
	
}
int query(int rt,int l,int r,int x){
	if(l==r)return t[rt].mn;
	pushdown(rt);
	int mid=l+r>>1;
	if(x<=mid)return query(rt<<1,l,mid,x);
	else return query(rt<<1|1,mid+1,r,x);
}
void upd(int x,int y,int val){
	while(top[x]!=top[y]){
		if(dep[x]<dep[y]){
			swap(x,y);
		}
		change(1,1,tim,in[top[x]],in[x],val);
		x=f[top[x]][0];
	}
	if(dep[x]>dep[y]){
		swap(x,y);
	}
	change(1,1,tim,in[x],in[y],val);
}
map<int,int>mmp[N];
int main()
{
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	for(int i=1;i<=n-1;++i){
		p[i]=read();
		add(p[i],i+1);
		f[i+1][0]=p[i];
	}
	dfs(1,0);
	dfs2(1,1);
	f[1][0]=1;
	for(int j=1;j<=20;j++)
		for(int i=1;i<=n;i++)
			f[i][j]=f[f[i][j-1]][j-1];
	build(1,1,tim);
	m=read();
	for(int i=1;i<=m;i++){
		u[i]=read(),v[i]=read();
		int lc=lca(u[i],v[i]);
		if(lc!=u[i]&&lc!=v[i]){
			int uu=u[i],vv=v[i];
			for(int j=20;j>=0;j--)
				if(dep[f[uu][j]]>dep[lc])uu=f[uu][j];
			for(int j=20;j>=0;j--)
				if(dep[f[vv][j]]>dep[lc])vv=f[vv][j];
			mmp[vv][uu]=mmp[uu][vv]=1;
//			cout<<uu<<' '<<vv<<endl;
		}
//		cout<<uu<<' '<<vv<<endl;
		upd(u[i],v[i],dep[lc]);
	}
	for(int i=1;i<=n;i++){
		st[i][0]=query(1,1,tim,in[i]);
		int now=i;
		for(int j=20;j>=0;j--)
			if(dep[f[now][j]]>=st[i][0])
				now=f[now][j];
		st[i][0]=now;
	}
	for(int j=1;j<=20;j++)
		for(int i=1;i<=n;i++)
			st[i][j]=st[st[i][j-1]][j-1];
	q=read();
	for(int i=1;i<=q;i++){
		int x,y,step1=0,step2=0;
		x=read(),y=read();
		int lc=lca(x,y);
		if(dep[st[x][20]]>dep[lc]||dep[st[y][20]]>dep[lc]){
			puts("-1");
			continue;
		}
		for(int j=20;j>=0;j--)
			if(dep[st[x][j]]>dep[lc])x=st[x][j],step1+=(1<<j);
		for(int j=20;j>=0;j--)
			if(dep[st[y][j]]>dep[lc])y=st[y][j],step2+=(1<<j);
		int xx=x,yy=y;
		for(int j=20;j>=0;j--)
			if(dep[f[xx][j]]>dep[lc])xx=f[xx][j];
		for(int j=20;j>=0;j--)
			if(dep[f[yy][j]]>dep[lc])yy=f[yy][j];
		int k=2;
		if(y==lc||x==lc)k--;
		else if(mmp[xx][yy])k--;
		writeln(step1+step2+k);
	}
	return 0;
}
/*
7
1 1 1 4 5 6
4
4 2
5 4
1 3
6 7
5
4 5
3 5
7 2
3 2
5 3
*/
