#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x)
{
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=1000005,Mod=1e9+7; 
int n,p,ans,sum; 
int c[N],tmp[N],vis[N]; 
int edge[21][21];  
void calc(int k,int fa) {
	++sum; 
	for(int i=k+1;i<=n;i++)
		if(edge[k][i]) {
			if(tmp[k]+tmp[i]==1) calc(i,k); 
		}
} 

bool check(){
	int cnt=0; 
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			edge[i][j]=0; 
	for(int i=1;i<=n-1;i++)
		for(int j=i+1;j<=n;j++){
			if(!vis[++cnt]) continue; 
			edge[i][j]=1; 
		}
	sum=0; 
	for(int i=1;i<=n;i++)
		calc(i,i); 
	return sum %2==p; 
}

void dfs2(int x) {
	if(x>n*(n-1)/2) {
		if(check()) ++ans; 
		return; 
	}
	vis[x]=0;
	dfs2(x+1); 
	vis[x]=1;
	dfs2(x+1); 
}

void dfs1(int x)
{
	if(x>n){
		dfs2(1); 
		return; 
	}
	if(c[x]!=-1){
		tmp[x]=c[x]; 
		dfs1(x+1); 
	}
	else{
		tmp[x]=1;
		dfs1(x+1); 
		tmp[x]=0;
		dfs1(x+1); 
	}
}
int main()
{
	freopen("graph.in","r",stdin); 
	freopen("graph.out","w",stdout); 
	n=read();p=read(); 
	for(int i=1;i<=n;i++)c[i]=read(); 
	if(n<=50){
		dfs1(1);
		writeln(ans);
	}else {
		if(n%2==p)puts("1");
		else puts("0");
	}
	return 0;
}
