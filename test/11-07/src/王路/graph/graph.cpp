#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <functional>
#include <limits>
#include <numeric>
#include <utility>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-') {
    ch = NC(), isneg = 1;
  }
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 1e6 + 5, kMod = 1e9 + 7;
int n, p, a[kMaxN];
int c[10];

struct Graph {
  int head[23], ecnt, t;
  struct Edge { int to, nxt; } e[2333];
  void clear() {
    memset(head, 0x00, sizeof head);
    ecnt = 0;
  }
  void insert(int x, int y) {
    e[++ecnt] = (Edge) { y, head[x] };
    head[x] = ecnt;
  }
  void DFS(int x, int fa = -1) {
    ++t;
    for (int i = head[x]; i; i = e[i].nxt) {
      if (e[i].to == fa)
        continue;
      if (c[e[i].to] != c[x]) {
        DFS(e[i].to, x);
      }
    }
  }
} g;

namespace SubTask1 {
int total = 0;
int bcnt;
pair<int, int> b[24];
void DFS(int x) {
  if (x == n) {
    int ans = 0;
    for (int i = 1; i <= n; ++i) {
      g.t = 0;
      g.DFS(i);
      ans += g.t;
    }
    if ((ans & 1) == p)
      ++total;
    return;
  }
  if (a[x + 1] == -1) {
    c[x + 1] = 0;
    DFS(x + 1);
    c[x + 1] = 1;
    DFS(x + 1);
  } else {
    c[x + 1] = a[x + 1];
    DFS(x + 1);
  }
}
int Solve() {
  for (int i = 1; i <= n; ++i) {
    for (int j = i + 1; j <= n; ++j) {
      b[bcnt++] = make_pair(i, j);
    }
  }
  for (int i = 0; i < (1 << bcnt); ++i) {
    g.clear();
    for (int j = 0; j < bcnt; ++j) {
      if (i >> j & 1) {
        g.insert(b[j].first, b[j].second);
      }
    }
    DFS(0);
  }
  printf("%d\n", total);
  return 0;
}
}

ll QuickPow(ll x, ll y) {
  ll ret = 1;
  for (; y; y >>= 1, x = x * x % kMod) {
    if (y & 1)
      ret = ret * x % kMod;
  }
  return ret;
}

namespace SubTask2 {
int Solve() {
  if ((n & 1) == p) {
    printf("%d\n", QuickPow(2, 1LL * n * (n - 1) >> 1));
  } else {
    puts("0");
  }
}
}

int main() {
  freopen("graph.in", "r", stdin);
  freopen("graph.out", "w", stdout);
  RI(n), RI(p);
  bool same = true;
  for (int i = 1; i <= n; ++i) {
    RI(a[i]);
    if (a[i] != a[1])
      same = false;
  }
  if (n <= 5) { // 30 pts
    return SubTask1::Solve();
  }
  if (same) { // 10 pts
    return SubTask2::Solve();
  }
  return 0;
}

// 40 pts