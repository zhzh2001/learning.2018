#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <functional>
#include <limits>
#include <numeric>
#include <utility>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-') {
    ch = NC(), isneg = 1;
  }
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 1e6 + 5, kMod = 1e9 + 7;
int n, p, a[kMaxN], f[kMaxN][2], black[kMaxN], white[kMaxN];
int bin[kMaxN], fact[kMaxN], rfact[kMaxN];

ll QuickPow(ll x, ll y) {
  ll ret = 1;
  for (; y; y >>= 1, x = x * x % kMod) {
    if (y & 1)
      ret = ret * x % kMod;
  }
  return ret;
}

void Update(int &x, int y) {
  x = x + y;
  if (x > kMod)
    x -= kMod;
}

ll Combin(int n, int m) {
  if (n < m || m < 0) 
    return 0;
  return 1LL * fact[n] * rfact[m] % kMod * rfact[n - m] % kMod;
}

int main() {
  freopen("graph.in", "r", stdin);
  freopen("graph.out", "w", stdout);
  RI(n), RI(p);
  bin[0] = 1;
  fact[0] = 1;
  for (int i = 1; i <= n; ++i) {
    RI(a[i]);
    bin[i] = (bin[i - 1] << 1) % kMod;
    black[i] = black[i - 1] + (a[i] == 0);
    white[i] = white[i - 1] + (a[i] == 1);
    fact[i] = 1LL * fact[i - 1] * i % kMod;
  }
  rfact[n] = QuickPow(fact[n], kMod - 2);
  for (int i = n; i > 0; --i) {
    rfact[i - 1] = 1LL * rfact[i] * i % kMod;
  }
  f[1][1] = 1 + (a[1] == -1);
  for (int i = 1; i < n; ++i) {
    if (a[i + 1] != 0) {
      // solve white
      for (int j = 0; j <= i - white[i]; ++j) {
        Update(f[i + 1][0], f[i][j & 1] * Combin(i, j) % kMod * bin[min(i - black[i], i - j)] % kMod);
        Update(f[i + 1][1], f[i][(j & 1) ^ 1] * Combin(i, j) % kMod * bin[min(i - black[i], i - j)] % kMod);
      }
    }
    if (a[i + 1] != 1) {
      // solve black
      for (int j = 0; j <= i - black[i]; ++j) {
        Update(f[i + 1][0], f[i][j & 1] * Combin(i, j) % kMod * bin[min(i - white[i], i - j)] % kMod);
        Update(f[i + 1][1], f[i][(j & 1) ^ 1] * Combin(i, j) % kMod * bin[min(i - white[i], i - j)] % kMod);
      }
    }
  }
  printf("%d\n", f[n][p]);
  return 0;
}

