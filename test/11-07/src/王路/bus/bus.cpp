#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <functional>
#include <limits>
#include <numeric>
#include <utility>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-') {
    ch = NC(), isneg = 1;
  }
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 2e5 + 5, kInf = 0x3f3f3f3f;
int n, m, q, head[kMaxN], ecnt, fa[20][kMaxN], dep[kMaxN];
struct Edge { int to, nxt; } e[kMaxN << 1];

inline void Insert(int x, int y) {
  e[++ecnt] = (Edge) { y, head[x] };
  head[x] = ecnt;
}

void DFS(int x) {
  dep[x] = dep[fa[0][x]] + 1;
  for (int i = head[x]; i; i = e[i].nxt) {
    if (e[i].to == fa[0][x])
      continue;
    fa[0][e[i].to] = x;
    DFS(e[i].to);
  }
}

namespace SubTask1 {
int f[105][105];
int p[105];
int Solve() {
  RI(m);
  memset(f, 0x3f, sizeof f);
  for (int i = 1; i <= n; ++i) {
    f[i][i] = 0;
  }
  for (int i = 1, u, v; i <= m; ++i) {
    RI(u), RI(v);
    int pu = u, pv = v;
    int ccc = 0;
    if (dep[u] < dep[v])
      swap(u, v);
    for (; dep[u] > dep[v]; u = fa[0][u]) {
      p[++ccc] = u;
    }
    for (; u != v; u = fa[0][u], v = fa[0][v]) {
      p[++ccc] = u;
      p[++ccc] = v;
    }
    int lca = u == v ? u : fa[0][u];
    p[++ccc] = lca;
    for (int i = 1; i <= ccc; ++i)
      for (int j = i + 1; j <= ccc; ++j) {
        f[p[i]][p[j]] = f[p[j]][p[i]] = 1;
      }
  }
  for (int k = 1; k <= n; ++k) {
    for (int i = 1; i <= n; ++i) {
      for (int j = 1; j <= n; ++j) {
        f[i][j] = min(f[i][j], f[i][k] + f[k][j]);
      }
    }
  }
  RI(q);
  for (int i = 1, u, v; i <= q; ++i) {
    RI(u), RI(v);
    if (f[u][v] < kInf) {
      printf("%d\n", f[u][v]);
    } else {
      puts("-1");
    }
  }
  return 0;
}
}

namespace SubTask2 {
vector<int> G[kMaxN];
int Solve() {
  RI(m);
  for (int i = 1, u, v; i <= m; ++i) {
    RI(u), RI(v);
    G[u].push_back(v);
    G[v].push_back(u);
    G[u].push_back(1);
    G[v].push_back(1);
  }
  for (int i = 1; i <= n; ++i) {
    sort(G[i].begin(), G[i].end());
  }
  RI(q);
  for (int i = 1, u, v; i <= q; ++i) {
    RI(u), RI(v);
    if (u < v)
      swap(u, v);
    if (v == 1) {
      if (G[u].size())
        puts("1");
      else
        puts("-1");
    } else {
      if (*lower_bound(G[u].begin(), G[u].end(), v) == v)
        puts("1");
      else {
        if (G[v].size() && G[u].size()) {
          puts("2");
        } else {
          puts("-1");
        }
      }
    }
  }
  return 0;
}
}

int main() {
  freopen("bus.in", "r", stdin);
  freopen("bus.out", "w", stdout);
  RI(n);
  bool flo = 1;
  for (int i = 2, fa; i <= n; ++i) {
    RI(fa);
    if (fa != 1)
      flo = 0;
    Insert(fa, i);
    Insert(i, fa);
  }
  DFS(1);
  if (n <= 100) { // 20
    return SubTask1::Solve();
  }
  if (flo) { // 10
    return SubTask2::Solve();
  }
  return 0;
}