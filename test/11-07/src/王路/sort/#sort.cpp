#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <functional>
#include <limits>
#include <numeric>
#include <utility>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-') {
    ch = NC(), isneg = 1;
  }
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 1e5 + 5;
int n, t, cnt, a[kMaxN], id[kMaxN], ans[kMaxN];
ll seed;
char obuf[1 << 22], *op = obuf;

int F(int l, int r) {
  switch (t) {
  case 1:
    return l;
  case 2:
    return r;
  case 3:
    return (l + r) >> 1;
  case 4:
    seed = 1LL * seed * 48271 % numeric_limits<int>::max();
    return seed % (r - l + 1) + l;
  }
  return -1;
}

int main() {
  freopen("sort.in", "r", stdin);
  freopen("sort.out", "w", stdout);
  RI(n), RI(t), RI(cnt);
  if (t == 4) {
    RI(seed);
  }
  for (int i = 1; i <= n; ++i) {
    id[i] = i;
  }
  for (int i = 1; i <= n; ++i) {
    int pos = F(i, n);
    ans[id[pos]] = i;
    swap(id[i], id[pos]);
  }
  for (int i = 1; i <= n; ++i) {
    op += sprintf(op, "%d%c", ans[i], " \n"[i == n]);
  }
  fwrite(obuf, 1, op - obuf, stdout);
  return 0;
}

// 100 pts
