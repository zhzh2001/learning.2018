#include <bits/stdc++.h>

using namespace std;

const int Maxn = 1e5+7;

int n, type, cnt, x, num;
struct Node
{
	int v, id;
	bool operator < (const Node &b) const { return id < b.id; }
} a[Maxn];

inline int fx(int l, int r)
{
	x = (int)(1ll*x*48271%2147483647);
	return x%(r-l+1)+l;
}

void build3(int l, int r)
{
	int mid = (l+r)>>1;
	a[mid].v = ++num;
	// printf("a[%d(%d)]=%d\n", mid, a[mid].id, num);
	swap(a[mid], a[l]);
	// i = l, j = l
	if(l+1 <= r) build3(l+1, r);
}

void build3_2(int l, int r)
{
	while(l <= r)
	{
		int mid = (l+r)>>1;
		a[mid].v = ++num;
		swap(a[mid], a[l]);
		l++;
	}
}

void build4(int l, int r)
{
	int mid = fx(l, r);
	a[mid].v = ++num;
	// printf("a[%d(%d)]=%d\n", mid, a[mid].id, num);///
	// printf("l:%d r%d\n", l , r);////
	swap(a[mid], a[l]);
	if(l+1 <= r) build4(l+1, r);
}

void build4_2(int l, int r)
{
	while(l <= r)
	{
		int mid = fx(l, r);
		a[mid].v = ++num;
		swap(a[mid], a[l]);
		l++;
	}
}

int main()
{
	freopen("sort.in", "r", stdin);
	freopen("sort.out", "w", stdout);
	scanf("%d%d%d", &n, &type, &cnt);
	if(type == 1 || type == 2) // l, r
	{
		for(int i = 1; i < n; ++i)
			printf("%d ", i);
		printf("%d\n", n);
	}
	else if(type == 3) // (l+r)/2
	{
		num = 0;
		for(int i = 1; i <= n; ++i) a[i].id = i;
		build3_2(1, n);
		sort(a+1, a+n+1);
		for(int i = 1; i < n; ++i)
			printf("%d ", a[i].v);
		printf("%d\n", a[n].v);
	}
	else if(type == 4) // random
	{
		scanf("%d", &x);
		num = 0;
		for(int i = 1; i <= n; ++i) a[i].id = i;
		build4_2(1, n);
		sort(a+1, a+n+1);
		for(int i = 1; i < n; ++i)
			printf("%d ", a[i].v);
		printf("%d\n", a[n].v);		
	}
	return 0;
}
