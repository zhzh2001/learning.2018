#include <bits/stdc++.h>

using namespace std;

const int Brute = 5e3+7;
const int Maxn = 2e5+7;
const int MOD = 1e9+7;

int n, m, Q;
int fa[Maxn], pre[Maxn], p[Maxn][2];
int e[Brute][Brute];
map<pair<int, int>, int> mmp;

int getf(int s) { return fa[s] == s ? s : fa[s] = getf(fa[s]); }
inline void make_father(int x, int y)
{
	int fx = getf(x), fy = getf(y);
	if(fx != fy) fa[fy] = fx;
}

inline void connect(int u, int v)
{
	queue<int> q;
	vector<int> va;
	memset(pre, 0, sizeof(int)*(n+2));
	q.push(u);
	pre[u] = u;
	int cur;
	while(q.size())
	{
		cur = q.front();
		q.pop();
		for(int i = 0, to; i < 2; ++i)
		{
			to = p[cur][i];
			if(pre[to]) continue;
			pre[to] = cur;
			if(to == v) break;
			q.push(to);
		}
	}
	cur = v;
	while(cur != u)
	{
		make_father(u, cur);
		if(n < Brute) va.push_back(cur);
		cur = pre[cur];
	}
	if(n < Brute)
	{
		va.push_back(u);
		for(int i = 0; i < va.size(); ++i)
			for(int j = 0; j < va.size(); ++j)
				e[va[i]][va[j]] = e[va[j]][va[i]] = 1;
	}
}

inline int bfs(int u, int v)
{
	queue<int> q;
	memset(pre, 0, sizeof(int)*(n+2));
	q.push(u);
	pre[u] = 1;
	while(q.size())
	{
		int cur = q.front();
		q.pop();
		for(int i = 1; i <= n; ++i)
		{
			if(e[cur][i] && !pre[i])
			{
				pre[i] = pre[cur]+1;
				if(i == v) return pre[i]-1;
				q.push(i);
			}
		}
	}
	return -1;
}

int main()
{
	freopen("bus.in", "r", stdin);
	freopen("bus.out", "w", stdout);
	scanf("%d", &n);
	int flag1 = 1, flag2 = 0;
	for(int i = 1; i <= n; ++i) fa[i] = i;
	for(int i = 2; i <= n; ++i)
	{
		scanf("%d", &p[i][0]);
		p[p[i][0]][1] = i;
		if(p[i][0] != i-1) flag1 = 0; // pi == i
		if(p[i][0] == 1) flag2++;   // pi == 1
	}
	scanf("%d", &m);
	for(int i = 1, u, v; i <= m; ++i)
	{
		scanf("%d%d", &u, &v);
		if(u > v) swap(u, v);
		connect(u, v);
		if(flag2 == n-1)
			mmp.insert(make_pair(make_pair(u, v), 1));
	}
	scanf("%d", &Q);
	for(int i = 1, u, v; i <= Q; ++i)
	{
		scanf("%d%d", &u, &v);
		if(u == v)
		{
			puts("0");
			continue;
		}
		if(u > v) swap(u, v);
		if(getf(u) != getf(v)) puts("-1");
		else
		{
			if(flag2 == n-1)
			{
				if(u == 1 || mmp.count(make_pair(u, v))) puts("1");
				else puts("2");
			}
			else if(n < Brute)
				printf("%d\n", bfs(u, v));
			else puts("GIVE UP!");
		}
	}
	return 0;
}
