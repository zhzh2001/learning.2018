#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline GG read(){
	GG k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(GG x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(GG x){ write(x);puts("");}

const int N = 1000011, Mod = 1e9+7; 
int n, p, ans, sum; 
int color[N], tmp[N], bian[N]; 
int E[21][21];  

inline int ksm(int x, int y) {
	int b[70], tot = 0; 
	while(y) {
		b[++tot] = y&1; 
		y/=2; 
	}
	int ans = 1; 
	Dow(i, tot, 1) {
		ans = 1ll*ans*ans% Mod; 
		if(b[i]) ans = 1ll*ans*x% Mod; 
 	}
 	return ans; 
}

void calc(int u, int fa) {
	++sum; 
	For(i, u+1, n) 
		if(E[u][i]) {
			if(tmp[u]+tmp[i]==1) calc(i, u); 
		}
} 

bool check() {
	int cnt = 0; 
	For(i, 1, n) For(j, 1, n) E[i][j] = 0; 
	For(i, 1, n-1) 
		For(j, i+1, n) {
			if(!bian[++cnt]) continue; 
			E[i][j] = 1; 
		}
	sum = 0; 
	For(i, 1, n) calc(i, i); 
	return sum %2==p; 
}

void dfs_2(int x) {
	if(x>n*(n-1)/2) {
		if(check()) ++ans; 
		return; 
	}
 	bian[x] = 0; dfs_2(x+1); 
 	bian[x] = 1; dfs_2(x+1); 
}

void dfs_1(int x) {
	if(x > n) {
		dfs_2(1); 
		return; 
	}
	if(color[x]!=-1) {
		tmp[x] = color[x]; 
		dfs_1(x+1); 
	}
	else{
		tmp[x] = 1; dfs_1(x+1); 
		tmp[x] = 0; dfs_1(x+1); 
	}
}

inline void work_1() {
	dfs_1(1); 
	writeln(ans); 
	exit(0); 
}

int main() {
	freopen("graph.in", "r", stdin); 
	freopen("graph.out", "w", stdout); 
	n = read(); p = read(); 
	For(i, 1, n) color[i] = read(); 
	if(n<=5) work_1(); 
	bool flag = 0; 
	if(color[1]==-1) flag = 1; 
	For(i, 1, n) if(color[i]!=color[1]) flag = 1; 
	if(!flag) {
		if(p==1) 
			puts("0");  
		else {
			int ans = 1; 
			LL y = 1ll*n*(n-1)/2; 
			writeln(ksm(2, y)); 
		}
		return 0; 
	}
}






