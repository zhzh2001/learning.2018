#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline GG read(){
	GG k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(GG x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(GG x){ write(x);puts("");}

const int N = 200011, Mod = 20010929; 
int n, m, Q; 
int fa[N], visit[N], ha[Mod+10];  

inline void work_1() {
	m = read(); 
	For(i, 1, m) {
		int x=read(), y = read(); 
		if(x>y) swap(x, y); 
		visit[x] = 1; visit[y] = 1;
		ha[(1ll*x*201937+y) %Mod] = 1;  
	}
	Q = read();  
	if(m>0) visit[1] = 1; 
	For(i, 1, Q) {
		int x = read(), y = read();  
		if(x==y) {
			puts("0"); continue; 
		}
 		if(!visit[x] || !visit[y]) {
			puts("-1"); 
			continue; 
		}
		if(x>y)swap(x, y); 
		if(x==1) {
			puts("1"); 
			continue; 
		}
		if(ha[(1ll*x*201937+y)%Mod]) puts("1");
		else puts("2"); 
	}
}

int main() {
	freopen("bus.in", "r", stdin); 
	freopen("bus.out", "w", stdout); 
	n = read(); 
	For(i, 2, n) fa[i] = read(); 
	bool flag = 0; 
	For(i, 2, n) 
		if(fa[i]!=1) flag = 1; 
	if(!flag) work_1();
	flag = 0; 
}









