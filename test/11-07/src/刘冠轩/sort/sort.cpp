#include <bits/stdc++.h>
using namespace std;
#define int long long
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc()) x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int n,type,cnt,seed,mid,K,ans[200005],LL,PP;
int ran(int l,int r){
	seed=seed*48271%2147483647;
	return seed%(r-l+1)+l;
}
signed main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();type=read();cnt=read();
	if(type==4)seed=read();
	if(type==1)for(int i=1;i<=n;i++)cout<<i<<' ';
	else if(type==2)for(int i=1;i<=n;i++)cout<<n-i+1<<' ';
	else if(type==3){
		n-=2;
		mid=n/2+1;
		for(int i=1,j=2;i<mid;i++,j+=2)cout<<j<<' ';
		for(int i=mid,j=1;i<=n;i++,j+=2)cout<<j<<' ';
		cout<<n+1<<' '<<n+2;
	}else{
		for(PP=1;PP<=n;PP++){
			int SS=ran(PP,n);
			if(ans[SS])break;
			else ans[SS]=PP;
		}
		LL=1;
		for(int i=PP;i<=n;i++){
			while(ans[LL])LL++;
			ans[LL]=i;
		}
		for(int i=1;i<=n;i++)cout<<ans[i]<<' ';
	}
	return 0;
}
