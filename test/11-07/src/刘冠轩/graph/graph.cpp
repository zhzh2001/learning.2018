#include <bits/stdc++.h>
#define int long long
using namespace std;
#define gc getchar
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc()) x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
const int N=205;
const int P=10000007;
int n,a[N],p,ans,f[N];
void solve(int x,int y){
	if(x>n){
		if(y&1==p)ans++;
		return;
	}
	for(int i=0;i<=(1<<(x-1))-1;i++){
		f[x]=1;
		for(int j=1;j<x;j++)
			if(a[j]!=a[x]&&(1<<(j-1))&i)f[x]+=f[j];
		solve(x+1,y+f[x]);
	}
}
void dfs(int x){
	if(x>n){
		solve(1,0);
		return;
	}
	if(a[x]==-1){
		a[x]=0;
		dfs(x+1);
		a[x]=1;
		dfs(x+1);
		a[x]=-1;
	}else dfs(x+1);
}
int ksm(int x,int y){
	int s=1;
	for(;y;x=x*x%P,y>>=1)if(y&1)s=(s*x)%P;
	return s;
}
signed main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();p=read();
	for(int i=1;i<=n;i++)a[i]=read();
	if(n<=200)dfs(1);else if(p==n&1)ans=ksm(2,n*(n-1)/2);
	cout<<ans;
	return 0;
}
