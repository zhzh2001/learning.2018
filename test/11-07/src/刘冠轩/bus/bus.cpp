#include <bits/stdc++.h>
using namespace std;
#define gc getchar
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc()) x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
const int N=5005;
struct edge{
	int to,nt,dis;
	bool operator <(const edge b)const{
		return to==b.to?nt<b.nt:to<b.to;
	}
}e[N*2];
int n,m,cnt,head[N],x,y,kk,A[200005];
void add(int u,int v,int w){
	e[++cnt]=(edge){v,head[u],w};
	head[u]=cnt;
	e[++cnt]=(edge){u,head[v],w};
	head[v]=cnt;
}
map<edge,int> M;
bool vv[200005];
int main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	if(n<=5005){
		for(int i=2;i<=n;i++)add(i,read(),0x7f);
		m=read();
		for(int i=1;i<=m;i++){
			x=read();y=read();
			add(x,y,1);
		}
	}else{
		for(int i=2;i<=n;i++)kk=read();
		if(kk==1){
			m=read();
			for(int i=1;i<=m;i++){
				x=read();y=read();
				M[(edge){x,y,0}]=1;
				M[(edge){y,x,0}]=1;
				vv[x]=vv[y]=1;
			}
			m=read();
			for(int i=1;i<=m;i++){
				x=read();y=read();
				if(x==1){
					if(vv[y])puts("1");else puts("-1");
				}else if(y==1){
					if(vv[x])puts("1");else puts("-1");
				}else if(vv[x]&&vv[y]){
					if(M[(edge){x,y,0}]==1)puts("1");
					else puts("2");
				}else puts("-1");
			}
		}
	}
	return 0;
}
