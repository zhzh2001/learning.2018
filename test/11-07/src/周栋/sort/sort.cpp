#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline void yyhakking(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
}
const ll N=1e5+10;
ll n,m,cnt,seed,ans[N],tot,id[N];
inline ll gg(){
	seed=seed*48271%2147483647;
	return seed;
}
inline ll gget(ll l,ll r){
	if (m==1) return l;
	if (m==2) return r;
	if (m==3) return (l+r)/2;
	return gg()%(r-l+1)+l;
}
inline void solve(ll l,ll r){
	while (1){
		if (l>r) return;
		if (l==r){
			ans[id[l]]=++tot;
			return;
		}
		ll x=gget(l,r);
		ans[id[x]]=++tot;
		swap(id[l],id[x]);
		++l;
	}
}
/*

	ll i=l,j=r,x=a[gget(l,r)];
	do{
		for (;a[i]<x;++i) ++cnt;
		for (;a[i]<x;++i) ++cnt;
		if (i<=j){
			swap(a[i],a[j]);
			++i,--j;
			cnt+=2;
		}
	}while(i<=j);
	if (l<j) sort(l,j);
	if (i<r) sort(i,r);
*/
int main(){
	yyhakking();
	read(n),read(m),read(cnt);
	if (m==1||m==2){
		for (ll i=1;i<=n;++i) wr(i),pc(' ');
		return 0;
	}
	if (m==4) read(seed);
	for (ll i=1;i<=n;++i) id[i]=i;
	solve(1,n);
	for (ll i=1;i<=n;++i,pc(' ')) wr(ans[i]);
	return 0;
}
//sxdakking
