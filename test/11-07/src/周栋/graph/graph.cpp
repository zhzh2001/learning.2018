#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline void yyhakking(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
}
const ll P=1e9+7,N=1e6+10;
ll a[N],n,p;//ans[N][2][2][2];
ll e[20][20],ans;
inline ll pow(ll x,ll y){
	ll ret=1;
	for (;y;y>>=1,x=x*x%P) if (y&1) ret=ret*x%P;
	return ret;
}
inline ll dfs1(ll x){
	ll ret=1;
	for (ll i=x+1;i<=n;++i)
		if (e[x][i]&&a[x]!=a[i]) ret+=dfs1(i);
	return ret;
}
inline void dfs(ll x,ll y){
	if (y>n) ++x,y=x+1;
	if (x==n){
		ll num=0;
		for (ll i=1;i<=n;++i)
			num+=dfs1(i);
		ans+=(num%2==p);
		if (ans>P) ans-=P;
		return;
	}
	if (a[x]==-1){
		a[x]=1;
		dfs(x,y);
		a[x]=0;
		dfs(x,y);
		a[x]=-1;
		return;
	}
	e[x][y]=0;
	dfs(x,y+1);
	e[x][y]=1;
	dfs(x,y+1);
}
int main(){
	yyhakking();
	read(n),read(p);
	for (ll i=1;i<=n;++i) read(a[i]);
	if (n<=5) {
		dfs(1,2),wr(ans);
		return 0;
	}
/*	if (a[n]!=-1) ans[1][1][1][a[i]]=1;
	else ans[1][1][1][0]=ans[1][1][1][1]=1;
	for (ll i=1;i<=n;++i){
		if (a[i]!=-1){
			for (ll j=1;j<=i;++j)
			for (ll k=0;k<2;++k)
				for (ll l=0;l<2;++l)
					ans[i][
		}
		*/
/*		if (a[i]==-1||a[i]==0){
			ll k=1,num0=0,num1=0;
			for (ll j=i+1;j<=n;++j){
				if (a[j]==0||a[j]==-1) k=k*2%P;
				if (a[j]==1||a[j]==-1) (num1+=ans[j][1])%=P;
			}
			num1=num1*k%P;
			num0=(pow(2,(n-i+1)*(n-i)/2)-num1+P)%P*k%P;
			ans[i][0]+=num0;
			ans[i][1]+=num1;
		}
		if (a[i]==-1||a[i]==1){
			ll k=1,num0=0,num1=0;
			for (ll j=i+1;j<=n;++j){
				if (a[j]==1||a[j]==-1) k=k*2%P;
				if (a[j]==0||a[j]==-1) (num1+=ans[j][1])%=P;
			}
			num1=num1*k%P;
			num0=(pow(2,(n-i+1)*(n-i)/2)-num1+P)%P*k%P;
			ans[i][0]+=num0;
			ans[i][1]+=num1;
		}
		ans[i][0]%=P;
		ans[i][1]%=P;
*/
//		cout<<i<<' '<<ans[i][0]<<' '<<ans[i][1]<<'\n';
//	}
	
	return 0;
}
//sxdakking
/*
10 1
-1 0 -1 1 1 -1 -1 -1 -1 -1

2 0
-1 -1
*/
