#include <iostream>
#include <cstdio>
#include <set>
#include <algorithm>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline void yyhakking(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
}
const ll N=2e5+10;
ll n,fa[N],m,q,ans[N];
struct node{ll x,y;}a[N];
bool operator <(node a,node b){
	return a.y<b.y;
}
struct Node{ll x,y,id;}A[N];
bool operator <(Node a,Node b){
	return a.y<b.y;
}
set<ll>V[N];
int main(){
	yyhakking();
	read(n);
	for (ll i=1;2<=n;++i) read(fa[i]);
	read(m);
	for (ll i=1;i<=m;++i) read(a[i].x),read(a[i].y);
	read(q);
	for (ll i=1;i<=q;++i) read(A[i].x),read(A[i].y),A[i].id=i;
	bool flag=1;
	for (ll i=1;i<=n;++i) if (fa[i]!=1) flag=0;
	if (flag){
		for (ll i=1;i<=m;++i){
			V[a[i].x].insert(a[i].y);
			V[a[i].y].insert(a[i].x);
		}
		for (ll i=1;i<=q;++i){
			if (A[i].x>A[i].y) swap(A[i].x,A[i].y);
			if (A[i].x==1){
				if (q) puts("1");
				else puts("-1");
			}
			if (V[A[i].x].empty()||V[A[i].y].empty()){
				puts("-1");continue;
			}
			if (V[A[i].x].count(A[i].y)) puts("1");
			else puts("0");
		}
		return 0;
	}
	flag=1;
	for (ll i=2;i<=n;++i) if (fa[i]!=i-1) flag=0;
	if (flag){
		for (ll i=1;i<=m;++i) if (a[i].x>a[i].y) swap(a[i].x,a[i].y);
		for (ll i=1;i<=q;++i) if (A[i].x>A[i].y) swap(A[i].x,A[i].y);
		sort(a+1,a+1+m);
		sort(A+1,A+1+q);
		for (ll i=1,l=1;i<=q;++i){
			while (a[l].y<=a[i].x) ++l;
			ll j=l;
			while (A[i].x<A[i].y){
				while (a[j].y<=a[i].x) ++j;
				++ans[A[i].id];
				A[i].x=a[i].y;
			}
		}
		for (ll i=1;i<=q;++i) wr(ans[i]),puts("");
		return 0;
	}
	for (ll i=1;i<=q;++i) puts("-1");
	return 0;
}
//sxdakking
