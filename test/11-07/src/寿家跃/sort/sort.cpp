#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=100005;

int n,type,cnt,Seed,Trs[N],Dat[N];

inline int F(int l,int r){
	if (type==1) return l;
	if (type==2) return r;
	if (type==3) return (l+r)>>1;
	if (type==4){
		Seed=1ll*Seed*48271%2147483647;
		return l+Seed%(r-l+1);
	}
}

int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(),type=read(),cnt=read();
	if (type==4) Seed=read();
	Rep(i,1,n) Trs[i]=i;
	Dep(i,n,1){
		int pos=F(1,i);Dat[Trs[pos]]=i;
		swap(Trs[pos],Trs[i]);
	}
	Rep(i,1,n) printf("%d ",Dat[i]);puts("");
}
