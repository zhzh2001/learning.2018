#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=200005;
const int INF=1e9+7;

int n,m;
int lines,front[N];
struct Edge{
	int next,to;
}E[N*2];
inline void Addline(int u,int v){
	E[++lines]=(Edge){front[u],v};front[u]=lines;
}

int fa[N];
inline int find(int u){
	return fa[u]!=u?fa[u]=find(fa[u]):u;
}

int father[N],deep[N];
vector<int>V[N];

int Dis[N],head,tail,Q[N];

void Sep(int x,int y,int dis){
	x=find(x),y=find(y);
	while (x!=y){
		if (deep[x]<deep[y]) swap(x,y);
		Dis[x]=dis;
		fa[x]=father[x];
		x=find(x);
	}
	if (x!=0){
		Dis[Q[++tail]=x]=dis;
		fa[x]=father[x];
	}
}
int size[N],heavy[N];

void Dfs(int u){
	size[u]=1;
	for (int i=front[u],v;i!=0;i=E[i].next){
		Dfs(v=E[i].to);
		size[u]+=size[v];
		if (size[v]>size[heavy[u]]) heavy[u]=v;
	}
}
int top[N],dep[N];

void Par(int u,int t,int d){
//	printf("u=%d t=%d d=%d\n",u,t,d);
	top[u]=t,dep[u]=d;
	int son=heavy[u];
	if (son) Par(son,t,d);
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=son){
			Par(v,v,d+1);
		}
	}
}

inline int Lca(int x,int y){
	while (top[x]!=top[y]){
//		printf("x=%d y=%d\n",x,y);
		if (dep[x]<dep[y]) swap(x,y);
		x=father[top[x]];
	}
	return deep[x]<deep[y]?x:y;
}

int f[N][20];

inline int Skip(int pos,int top){
	int res=0;
	Dep(i,19,0){
		if (deep[f[pos][i]]>deep[top]){
			pos=f[pos][i];
			res=res+(1<<i);
		}
	}
//	printf("res=%d\n",res);
	if (deep[pos]>deep[top]){
		pos=Dis[pos];
		if (deep[pos]<=deep[top]) return res+1;
			else return INF;
	}
	return res;
}

int main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	deep[1]=1;
	Rep(i,2,n){
		int fa=read();father[i]=fa;deep[i]=deep[fa]+1;
		Addline(fa,i);
	}
	Dfs(1);
	Par(1,1,0);
	m=read();
	Rep(i,1,n) fa[i]=i;
	memset(Dis,0,sizeof(Dis));
	Rep(i,1,m){
		int x=read(),y=read();
		Sep(x,y,Lca(x,y));
	}
//	Rep(i,1,n) printf("Dis[%d]=%d\n",i,Dis[i]);
	Rep(i,1,n){
		f[i][0]=Dis[i];
		Rep(j,1,19) f[i][j]=f[f[i][j-1]][j-1];
	}
	int q=read();
	Rep(i,1,q){
		int a=read(),b=read(),lca=Lca(a,b);
		int Ans=Skip(a,lca)+Skip(b,lca);
//		printf("a=%d b=%d\n",Skip(a,lca),Skip(b,lca));
		if (Ans>=INF) puts("-1");else printf("%d\n",Ans);
	}
}
/*
7
1 1 1 4 5 6
4
4 2
5 4
1 3
6 7
6
4 5
3 5
7 2
3 2
5 3
2 4
*/
