#include <bits/stdc++.h> 
using namespace std;
#define int long long
inline int read(){int s=0;char c;while(!isdigit(c))c=getchar();while(isdigit(c)){s=s*10+c-'0';c=getchar();}return s;}
inline void write(int x)
{
	char c[35];int tp=0; if(x==0){putchar('0');putchar(' ');return;}
	while(x){c[++tp]=x%10+'0';x/=10;}while(tp){putchar(c[tp]);tp--;}putchar(' ');
}
const int N=100005;
int n,op,up,seed,a[N];
inline int rnd()
{
	seed=seed*48271%2147483647; return seed;
}
inline void solve1()
{
	int i; for(i=1;i<=n;i++)write(i);
}
inline void solve2()
{
	int i; for(i=n;i>=1;i--)write(i);
}
inline void solve3()
{
	int i; for(i=1;i<=n;i++){if(i&1)a[(i+n)/2]=i;else a[i/2]=i;} for(i=1;i<=n;i++)write(a[i]);
}
inline void solve4()
{
	int i; for(i=1;i<=n;i++)a[i]=i; random_shuffle(a+1,a+n+1); for(i=1;i<=n;i++)write(a[i]);
}
signed main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(); op=read(); up=read();
	if(op==1)solve1();
	else if(op==2)solve2();
	else if(op==3)solve3();
	else solve4();
}
