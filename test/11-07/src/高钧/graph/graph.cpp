#include <bits/stdc++.h>
using namespace std;
inline int read(){int s=0,f=0;char c;while(!isdigit(c)){f|=c=='-';c=getchar();}while(isdigit(c)){s=s*10+c-'0';c=getchar();}return f?-s:s;}
const long long md=1000000007;
const int N=100005;
int n,p,c[N],mp[6][6],co[6],ans=0,ll[N],e[N][2],ct;
inline long long ksm(long long x,long long y)
{
	long long re=1LL;
	for(;y;y>>=1)
	{
		if(y&1)re=1LL*re*x%md; x=1LL*x*x%md;
	}return re;
}
inline bool jud()
{
	int i,j,k,ii,jj,pp=n;
	for(i=1;i<n;i++)
	{
		int la=0;
		for(j=i+1;j<=n;j++)if(co[j]!=co[i]&&mp[i][j])
		{
			la++;
			for(k=j+1;k<=n;k++)if(co[k]!=co[j]&&mp[j][k])
			{
				la++;
				for(ii=k+1;ii<=n;ii++)if(co[ii]!=co[k]&&mp[k][ii])
				{
					la++;
					for(jj=ii+1;jj<=n;jj++)if(co[jj]!=co[ii]&&mp[ii][jj])
					{
						la++;
					}
				}
			}
		}pp+=la;
	}if((pp%2)==p)return 1;return 0;
}
inline void dfs1(int x)
{
	if(x==ct+1)
	{
		if(jud())ans++; return;
	}mp[e[x][0]][e[x][1]]=1; dfs1(x+1); mp[e[x][0]][e[x][1]]=0; dfs1(x+1);
}
inline void dfs(int x)
{
	if(x==n+1)
	{
		memset(mp,0,sizeof mp);
		dfs1(1); return;
	}if(co[x]!=-1)dfs(x+1); else{co[x]=0; dfs(x+1); co[x]=1; dfs(x+1);}
}
inline void solve1()
{
	int i,j; ct=0; memmove(co,c,sizeof co);
	for(i=1;i<n;i++)for(j=i+1;j<=n;j++){e[++ct][0]=i;e[ct][1]=j;} dfs(1);
	printf("%d\n",ans);
}
inline void solve2()
{
	int tmp=n*(n-1)/2; if(p==(n%2))printf("%lld\n",1LL*ksm(2LL,1LL*tmp)%md);else puts("0");
}
inline void solve3()
{
	int tmp=n*(n-1)/2+n; printf("%lld\n",1LL*ksm(2LL,1LL*tmp)%md*ksm(2LL,md-2)%md);
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	int i,bo=0; n=read(); p=read(); for(i=1;i<=n;i++){c[i]=read();bo|=(c[i]!=-1);} if(n<=10)solve1();else {if(bo)solve2();else solve3();}
}
