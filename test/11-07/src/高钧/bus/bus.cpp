#include <bits/stdc++.h>
using namespace std;
const int N=205,M=2000005;
int n,m,Q,F[200005][23],dep[N],tot=0,Next[M],to[M],val[M],head[M],pp[N],dis[N][N],bo[N][N],ok[N],arr[N],fa[200005],dd[N],ook[N];
set<int>s[200005];
inline void add(int x,int y,int z)
{
	Next[++tot]=head[x]; to[tot]=y; val[tot]=z; head[x]=tot;
}
inline int ask(int x,int y)
{
	int i; if(dep[x]<dep[y])swap(x,y);
	for(i=8;~i;i--)if(dep[F[x][i]]>=dep[y])x=F[x][i]; if(x==y)return x;
	for(i=8;~i;i--)if(F[x][i]!=F[y][i])x=F[x][i],y=F[y][i];return F[x][0];
}
struct rec{int x,w;};
inline bool operator<(const rec &p,const rec &q){return p.w>q.w;}
inline void Dijkstra(int s)
{
	priority_queue<rec>q; q.push((rec){s,0}); memset(arr,0,sizeof arr); dis[s][s]=0; int i;
	while(!q.empty())
	{
		int x=q.top().x; q.pop(); if(arr[x])continue; arr[x]=1;
		for(i=head[x];i;i=Next[i])
		{
			if(dis[s][to[i]]>dis[s][x]+val[i])
			{
				dis[s][to[i]]=dis[s][x]+val[i]; q.push((rec){to[i],dis[s][to[i]]});
			}
		}
	}
}
inline void solve1()
{
	int i,j,k,x,y,ll; dep[1]=1; memset(dis,63,sizeof dis);
	for(i=2;i<=n;i++)
	{
		scanf("%d",&F[i][0]); dep[i]=dep[F[i][0]]+1;
	}for(i=1;i<=8;i++)for(j=1;j<=n;j++)F[j][i]=F[F[j][i-1]][i-1];
	scanf("%d",&m);
	for(i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y); ll=ask(x,y); *pp=0;
		while(x!=ll)
		{
			pp[++*pp]=x; x=F[x][0];
		}
		while(y!=ll)
		{
			pp[++*pp]=y; y=F[y][0];
		}pp[++*pp]=ll;
		for(j=1;j<*pp;j++)
		{
			for(k=j+1;k<=*pp;k++){add(pp[j],pp[k],1);add(pp[k],pp[j],1);}
		}
	}scanf("%d",&Q);
	while(Q--)
	{
		scanf("%d%d",&x,&y);
		if(ok[x]){if(dis[x][y]>1e9)puts("-1");else printf("%d\n",dis[x][y]);continue;}
		if(ok[y]){if(dis[y][x]>1e9)puts("-1");else printf("%d\n",dis[y][x]);continue;}
		Dijkstra(x); ok[x]=1; if(dis[x][y]>1e9)puts("-1");else printf("%d\n",dis[x][y]);
	}
}
inline int ask1(int x,int y)
{
	int i; if(dep[x]<dep[y])swap(x,y);
	for(i=19;~i;i--)if(dep[F[x][i]]>=dep[y])x=F[x][i]; if(x==y)return x;
	for(i=19;~i;i--)if(F[x][i]!=F[y][i])x=F[x][i],y=F[y][i];return F[x][0];
}
inline int gf(int x)
{
	return (fa[x]==x)?x:(fa[x]=gf(fa[x]));
}
inline void uni(int xx,int yy)
{
	fa[yy]=xx; dd[xx]=dd[xx]+dd[yy]+1;
}
inline void solve3()
{
	int i,j,x,y,xx,yy,ll; fa[1]=1; dd[1]=0;
	for(i=2;i<=n;i++)
	{
		scanf("%d",&F[i][0]); fa[i]=i; dd[i]=0;
	}for(i=1;i<=19;i++)for(j=1;j<=n;j++)F[j][i]=F[F[j][i-1]][i-1]; scanf("%d",&m);
	for(i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y); ll=ask1(x,y);
		while(x!=ll)
		{
			xx=gf(x); yy=gf(F[x][0]); if(xx!=yy)uni(xx,yy);
		}
	}
}
inline void solve2()
{
	int i,x,y; fa[1]=1;
	for(i=2;i<=n;i++)
	{
		scanf("%d",&F[i][0]); fa[i]=i;
	}scanf("%d",&m);
	for(i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y); ook[x]=ook[y]=1; s[x].insert(y); s[y].insert(x);
	}scanf("%d",&Q);
	while(Q--)
	{
		scanf("%d%d",&x,&y); if(!ook[x]||!ook[y]){puts("-1");continue;}
		if((s[x].find(y)!=s[x].end())||(s[y].find(x)!=s[y].end()))puts("1");else puts("2");
	}
}
int main()
{
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	scanf("%d",&n); if(n<=200)solve1(); else solve2();
}
