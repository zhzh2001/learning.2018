// f[i][32]表示到第i个点[00,11,01,10,不合法]的方案数 
// 孤点好蛋疼啊 
// 为什么忽然又感觉孤点和其他点一样了

#include <cstdio>
#include <algorithm>

using namespace std;

inline char gc() // 自己打了就不用出题人的了吧 
{
	static const int L = 233333;
	static char sxd[L], *sss = NULL, *ttt = NULL;
	if(sss == ttt)
	{
		ttt = (sss = sxd) + fread(sxd, 1, L, stdin);
		if(sss == ttt)
			return EOF;
	}
	return *sss++;
}

#define dd c = gc()
template<class T>
inline bool read(T& x)
{
	bool f = (x = 0);
	char dd;
	for(; !isdigit(c); dd)
	{
		if(c == EOF)
			return false;
		if(c == '-')
			f = true;
	}
	for(; isdigit(c); dd)
		x = (x << 1) + (x << 3) + (c ^ 48);
	if(f)
		x = -x;
	return 1;
}
#undef dd

int n, p;
int dp[2][32];

inline void solve(int now, int col)
{
	for(register int i = 0; i < 32; ++i)
	{
		dp[now][i] = dp[now ^ 1][i];
		dp[now][i] += dp[now ^ 1][i ^ 1];
	}
}

int main()
{
	freopen("graph.in", "r", stdin);
	freopen("graph.out", "w", stdout);
	read(n), read(p);
	int now = 0;
	dp[now][0] = dp[now][1] = dp[now][2] = dp[now][3] = 1;
	for(int i = 1, col; i <= n; ++i)
	{
		now ^= 1;
		read(col);
		if(col == 0 || col == -1)
		{
		}
	}
	fclose(stdin);
	fclose(stdout);
}
