#include <cstdio>
#include <cctype>

inline char gc()
{
	static const int L = 233333;
	static char sxd[L], *sss = NULL, *ttt = NULL;
	if(sss == ttt)
	{
		ttt = (sss = sxd) + fread(sxd, 1, L, stdin);
		if(sss == ttt)
			return EOF;
	}
	return *sss++;
}

#define dd c = gc()
template<class T>
inline bool read(T& x)
{
	bool f = (x = 0);
	char dd;
	for(; !isdigit(c); dd)
	{
		if(c == EOF)
			return false;
		if(c == '-')
			f = true;
	}
	for(; isdigit(c); dd)
		x = (x << 1) + (x << 3) + (c ^ 48);
	if(f)
		x = -x;
	return 1;
}
#undef dd

template<class T>
inline void write(T& x)
{
	int di[20];
	*di = 0;
}

int main()
{
	int a, b;
	while(read(a) && read(b))
		writeln(x);
	return 0;
}
