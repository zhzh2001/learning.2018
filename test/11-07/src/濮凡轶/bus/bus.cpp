#include <cstdio>
#include <algorithm>

using namespace std;

const int maxn = 200005;

int n;

struct Edge
{
	int to, nxt;
} e[maxn << 1];

int first[maxn];

inline void add_edge(int from, int to)
{
	static int cnt = 0;
	e[++cnt].nxt = first[from];
	first[from] = cnt;
	e[cnt].to = to;
	e[++cnt].nxt = first[to];
	first[to] = cnt;
	e[cnt].to = from;
}

int fa[maxn];
int ffa[maxn];
int siz[maxn];
int dep[maxn];
int son[maxn];

inline void dfs1(int now)
{
	for(int i = first[now]; i; i = e[i].nxt)
	{
		int to = e[i].to;
		if(fa[to] != now)
		{
			fa[to] = now;
			dep[to] = dep[now] + 1;
			dfs1(to);
			siz[now] += siz[to];
			if(!son[now] || siz[son[now]] < siz[to])
				son[now] = to;
		}
	}
}

int ll[maxn];

inline void dfs2(int now)
{
	if(son[now])
	{
	}
}

int main()
{
	freopen("bus.in", "r", stdin);
	freopen("bus.out", "w", stdout);
	fclose(stdin);
	fclose(stdout);
}
