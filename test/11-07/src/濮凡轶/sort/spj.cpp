#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>

using namespace std;

const int maxn = 100005;

int a[maxn];

inline int f(int l, int r)
{
	return (l + r) >> 1;
}

long long tmp = 0;

inline void sort(int l, int r)
{
	int i = l, j = r, x = a[f(l, r)];
	do
	{
		while(a[i] < x)
		{
			tmp++;
			if(tmp > 10000000 && !(tmp % 10000))
				printf("%lld\n", tmp);
			i++;
		}
		while(x < a[j])
		{
			tmp++;
			if(tmp > 10000000 && !(tmp % 10000))
				printf("%lld\n", tmp);
			j--;
		}
		if(i <= j)
		{
			swap(a[i], a[j]);
			tmp += 2;
			i++, j--;
		}
	}
	while(i <= j);
	if(l < j)
		sort(l, j);
	if(i <= r)
		sort(i, r);
}

int main()
{
	ifstream fin("sort.out");
	ifstream ffin("sort.in");
	int n;
	ffin >> n;
	ffin.close();
	for(int i = 1; i <= n; ++i)
		fin >> a[i];
	fin.close();
	sort(1, n);
	for(int i = 1; i <= n; ++i)
	{
		if(a[i] != i)
		{
			puts("NO");
			return 0;
		}
	}
	cout << tmp << endl;
	puts("YES");
	return 0;
}
