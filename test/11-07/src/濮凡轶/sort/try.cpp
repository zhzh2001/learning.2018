// 这题有毒吧
// 想a出题人

#include <ctime>
#include <cstdlib>
#include <queue>
#include <fstream>
#include <utility>
#include <iostream>
#include <algorithm>

using namespace std;

ifstream fin("sort.in");
ofstream fout("sort.out");

typedef long long LL;

int n, type, cnt, rnd;

inline void solve1()
{
	for(int i = 1; i <= n; ++i)
		fout << i << ' ';
}

inline void solve2()
{
	for(int i = n; i; --i)
		fout << i << ' ';
}

inline int Rnd()
{
	return rnd = (LL) rnd * 48271 % 2147483647;
}

const int maxn = 100005;

int solve3_a[maxn];
int t[maxn];

typedef pair<int, int> pii;

inline void solve233(int l, int r)
{
	static int cnt = 0;
	if(l > r)
		return;
	int mid = (l + r) >> 1;
//	cout << t[mid] << endl;
	solve3_a[t[mid]] = ++cnt;
	swap(t[mid], t[l]);
	solve233(l + 1, r);
}

inline void solve3()
{
//	int mid = (n >> 1);
//	int cnt = 0;
	for(int i = 1; i <= n; ++i)
		t[i] = i;
	solve233(1, n);
//	for(int i = n; i > mid; --i)
//		solve3_a[i] = ++cnt;
//	for(int i = 1; i <= mid; ++i)
//		solve3_a[i] = ++cnt;
	for(int i = 1; i <= n; ++i)
		fout << solve3_a[i] << ' ';
}

inline void solve2333(int l, int r)
{
	static int cnt = 0;
	if(l > r)
		return;
	int mid = Rnd() % (r - l + 1) + l;
//	cout << t[mid] << endl;
	solve3_a[t[mid]] = ++cnt;
	swap(t[mid], t[l]);
	solve233(l + 1, r);
}

inline void solve4()
{
	fin >> rnd;
//	int mid = (n >> 1);
//	int cnt = 0;
	for(int i = 1; i <= n; ++i)
		t[i] = i;
	solve2333(1, n);
//	for(int i = n; i > mid; --i)
//		solve3_a[i] = ++cnt;
//	for(int i = 1; i <= mid; ++i)
//		solve3_a[i] = ++cnt;
//	for(int i = 1; i <= n; ++i)
//		fout << solve3_a[i] << ' ';
	for(int i = 1; i <= n; ++i)
		fout << solve3_a[i] << ' ';
}

int main()
{
	fin >> n >> type >> cnt;
	switch(type)
	{
		case 1:
			solve1();
			break;
		case 2:
			solve2();
			break;
		case 3:
			solve3();
			break;
		case 4:
			solve4();
			break;
	}
	return 0;
}
