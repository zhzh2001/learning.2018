#include <iostream>
#include <cstdio>
#define int long long
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
const int mod=1e9+7;
struct arr{
	int u,v;
}a[1000];
int n,p,ans,tot,cnt,mp[1000][1000],col[1000],tmp[1000];
inline int power(int x,int k){
	int ans=1; 
	for(;k;k>>=1,x=1ll*x*x%mod)if(k&1)ans=1ll*ans*x%mod;
	return ans%mod;
}
int dfs3(int rt){
	int sum=1;
	for(int i=1;i<=n;i++){
		if(mp[rt][i]&&col[i]!=col[rt]){
			sum+=dfs3(i);
		}
	}
	return sum;
}
void dfs2(int rt){
	if(rt>cnt){
		int sum=0;
		for(int i=1;i<=n;i++)sum+=dfs3(i);
		if(sum%2==p){
			ans=(ans+1)%mod;
			/*cout<<"aaa";for(int i=1;i<=n;i++)cout<<col[i]<<" ";puts("");
			for(int i=1;i<=n;i++){
				for(int j=1;j<=n;j++) cout<<mp[i][j]<<" ";puts("");
			}puts("");*/
		}
		return ;
	}
	dfs2(rt+1);
	mp[a[rt].u][a[rt].v]=1;
	dfs2(rt+1);
	mp[a[rt].u][a[rt].v]=0;
}
void dfs1(int rt){
	if(rt>tot){
		dfs2(1);
		return;
	}
	col[tmp[rt]]=0; dfs1(rt+1);
	col[tmp[rt]]=1; dfs1(rt+1);
	col[tmp[rt]]=-1;
}
signed main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(),p=read(); int flag=1;
	for(int i=1;i<=n;i++){
		col[i]=read(); 
		if(i!=1&&col[i]!=col[i-1]) flag=0;
		if(col[i]==-1) tmp[++tot]=i;
	}
	if(flag){
		if(n%2!=p) puts("0");
		else {
		    int tot=(n-1)*n/2;
	     	int ans=power(2,tot)%mod;
	     	cout<<ans<<endl;
	     }
	     return 0;
	}
	for(int i=1;i<=n;i++)
	    for(int j=i+1;j<=n;j++) a[++cnt].u=i,a[cnt].v=j;
	dfs1(1);
	cout<<ans<<endl;
	return 0;
}
