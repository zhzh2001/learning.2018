#include<bits/stdc++.h>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int P=2147483647;
const int MAXN=1e5+5;
int n,type,cnt,seed,x;
int ans[MAXN],a[MAXN];
int Rand(int l,int r){
	x=1LL*x*48271%P;
	return x%(r-l+1)+l;
}
int f(int l,int r){
	if(type==1)return l;
	if(type==2)return r;
	if(type==3)return (l+r)/2;
	return Rand(l,r);
}
int tot;
void Sort(int l,int r){
	int i=l,j=r,x=a[f(l,r)];
	do{
		while(a[i]<x)i++,tot++;
		while(x<a[j])j--,tot++;
		if(i<=j){
			swap(a[i],a[j]);
			i++,j--;
			tot+=2;
		}
	}while(i<=j);
	if(l<j)Sort(l,j);
	if(i<r)Sort(i,r);
}
int Swap_num(){
	tot=0;x=seed;
	for(int i=1;i<=n;i++)a[i]=ans[i];
	Sort(1,n);
	return tot;
}
void write(int x){
	if(x>9)write(x/10);
	putchar(x%10^'0');
}
void wrs(int x){
	write(x);
	putchar(' ');
}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(),type=read(),cnt=read();
	if(type==4)seed=read();
	if(type<3){
		for(register int i=1;i<=n;i++)wrs(i);
		return 0;
	}
	if(type==3){
		for(register int i=2;i<=n/2;i++)wrs(i);
		wrs(1),wrs(n);
		for(register int i=n/2+1;i<n;i++)wrs(i);
		return 0;
	}
	srand((signed)time(NULL));
	for(int i=1;i<=n;i++)ans[i]=i;
	while(Swap_num()<=cnt)random_shuffle(ans+1,ans+n+1);
	for(int i=1;i<=n;i++)wrs(ans[i]);
}
