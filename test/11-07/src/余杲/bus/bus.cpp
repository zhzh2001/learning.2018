#include<queue>
#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
inline char _gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
#define gc c=_gc()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=2e5+5;
bool a[5005][5005];
int fa[MAXN],d[MAXN];
int nedge,head[MAXN];
struct Edge{
	int to,nxt;
}edge[MAXN<<1];
int vis[5005];
void add(int x,int y){
	edge[++nedge].to=y;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
queue<int>q;
int main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	int n=read();
	for(register int i=2;i<=n;i++)fa[i]=read(),d[i]=d[fa[i]]+1;
	int m=read(); 
	for(register int i=1;i<=m;i++){
		register int x=read(),y=read();
		while(x!=y){
			if(d[x]<d[y])swap(x,y);
			a[i][x]=1;
			x=fa[x];
		}
		a[i][x]=1;
	}
	for(register int i=1;i<=m;i++)
		for(register int j=i+1;j<=m;j++)
			for(register int k=1;k<=n;k++)
				if(a[i][k]&&a[j][k]){
					add(i,j),add(j,i);
					break;
				}
	for(register int T=read();T;T--){
		int x=read(),y=read();
		memset(vis,0x3f,sizeof(vis));
		for(register int i=1;i<=m;i++)
			if(a[i][x])q.push(i),vis[i]=1;
		while(!q.empty()){
			int x=q.front();q.pop();
			for(register int i=head[x];i;i=edge[i].nxt){
				int y=edge[i].to;
				if(vis[y]<1e9)continue;
				vis[y]=vis[x]+1;
				q.push(y);
			}
		}
		int ans=1e9;
		for(register int i=1;i<=m;i++)
			if(a[i][y])ans=min(ans,vis[i]);
		if(ans!=1e9)printf("%d\n",ans);
		else puts("-1");
	}
}
