#include<vector>
#include<cstdio>
#include<queue>
#include<cstring>

using namespace std;

const int N=5003;
int n,m,q,ans;
int p[N];
bool vis[N];
vector<int> g[N];
vector<int> gc[N];
bool can[N];

void Init();

bool dfs(int cur,int end_place,int deep)//处理公交车是否可以到达
{
    //printf("->%d",cur);
    if(cur==end_place){
        ans=min(ans,deep);//数据保证只有一条路
        vis[cur]=false;
        can[cur]=true;
        return true;
    }else{
        for(unsigned int i=0;i<g[cur].size();i++)
            if(!vis[g[cur][i]])
            {
                vis[g[cur][i]]=true;
                if(dfs(g[cur][i],end_place,deep+1)) can[cur]=true;
                vis[g[cur][i]]=false;
            }
        return can[cur];
    }
}

void bfs(int st_p,int ed_p)//处理查询
{
    int deep=0;
    queue<int> q;
    q.push(st_p);
    while(!q.empty())
    {
        int cur=q.front();q.pop();
        if(cur==ed_p) ans=min(ans,deep);
        deep++;
        for(unsigned int i=0;i<g[cur].size();i++)
            if(!vis[g[cur][i]])
            {
                q.push(g[cur][i]);
            }
    }
}

int main()
{
    Init();
    for(int i=1;i<=q;i++)
    {
        memset(vis,0,sizeof(vis));
        ans=0x3f3f3f3f;
        int a,b;
        scanf("%d%d",&a,&b);
        vis[a]=true;
        dfs(a,b,0);
        vis[a]=false;
        printf("%d\n",ans==0x3f3f3f3f?-1:ans);
    }
    printf("\n");
    for(int i=1;i<=n;i++) printf("%d ",can[i]);
    return 0;
}

void Init()
{
    freopen("bus.in","r",stdin);
    scanf("%d",&n);
    for(int i=1;i<=n-1;i++){
        scanf("%d",&p[i]);
        g[i+1].push_back(p[i]);
        g[p[i]].push_back(i+1);
    }
    scanf("%d",&m);
    for(int i=1;i<=m;i++){
        int u,v;
        //can[u]=can[v]=true;
        scanf("%d%d",&u,&v);
        gc[u].push_back(v);
        gc[v].push_back(u);
    }
    scanf("%d",&q);
}
//好难受啊,暴力都写不完
//太久没碰了
