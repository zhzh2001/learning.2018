#include<cstdio>

using namespace std;

const int MAXN=1000003;
const int MOD=1e9+7;
int n,p,ans;
int a[MAXN],f[MAXN],ff[MAXN][2];
bool mdzz;

int dfs(int now,int deep)
{
    printf("now->%d deep->%d\n",now,deep);
    for(int i=1;i<=n;i++) printf("%d ",f[i]); printf("\n");
    bool has_houji=false;
    for(int i=now+1;i<=n;i++){
        if(a[i]!=a[now] || a[i]==-1){
            f[now]+=dfs(i,deep+1);
            has_houji=true;
        }
    }
    return (deep%2==p || has_houji)?f[now]:0;
}

int main()
{
    freopen("graph.in","r",stdin);
    freopen("graph.out","w",stdout);
    scanf("%d%d",&n,&p);
    for(int i=1;i<=n;i++){
        scanf("%d",&a[i]);f[i]=1;
    }
    if(n>200){
        for(int i=2;!mdzz && i<=n;i++)
            if(a[i]!=a[i-1] && a[i]!=-1) mdzz=true;
        if((!mdzz) && n%2==p && a[1]!=-1) printf("%d\n",n);
    }
    for(int i=1;i<=n;i++){
        printf("\n");
        dfs(i,0);
    }
    dfs(1,0);
    printf("\n"); for(int i=1;i<=n;i++) printf("%d ",f[i]); printf("\n");
    for(int i=1;i<=n;i++){
        if(f[i]%2==p) ans=(ans+1)%MOD;
    }
    printf("%d\n",ans);
    return 0;
}
//GiveUp
