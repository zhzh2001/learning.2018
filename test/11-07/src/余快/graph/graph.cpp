#include <bits/stdc++.h>
#define ll long long
#define mp make_pair
#define be begin()
#define en end()
#define pa pair <int,int>
#define pb push_back
#define put putchar('\n')
#define re register
#define F(i,a,b) for (re int i=(a);i<=(b);i++)
#define D(i,a,b) for (re int i=(a);i>=(b);i--)
#define go(i,x) for (re int i=head[x];i;i=Next[i])
#define N 500055
#define mod 1000000007
using namespace std;
#define gc getchar
inline int read(){char c;int f=1,x=0;for (c=gc();c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
for (;c>='0'&&c<='9';c=gc()) x=x*10+c-'0';return (f==1)?x:-x;}
inline void wr(int x){if (x<0){putchar('-');wr(-x);}else{if (x>=10) wr(x/10);putchar(x%10+'0');}}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);put;}
int n,m,ans,p1,num[N],a[N],yk[3];
//t2好难啊 
//f[i][j]表示i个白色为奇数的，j个黑色为奇数的方案数
//转移枚举这个点跟多少异色点连边 
//答案看(i+j)&1的值
//打个70压压惊 
inline void add(int &x,int k){
	x+=k;x-=(x>=mod)?mod:0;
}
int ksm(ll x,ll k){
	int sum=1;
	while (k){
		if (k&1) sum=sum*x%mod;
		x=x*x%mod;k>>=1;
	}
	return sum;
}
int fac[N],inv[N],f[205][205],g[205][205];
inline int C(int n,int m){
	return (n<m||m<0)?0:1LL*fac[n]*inv[m]%mod*inv[n-m]%mod;
}
void init_c(int n){
	fac[0]=1;F(i,1,n) fac[i]=1LL*fac[i-1]*i%mod;
	inv[n]=ksm(fac[n],mod-2);D(i,n-1,0) inv[i]=1LL*inv[i+1]*(i+1)%mod;
}
void solve1(){
	int pd=1;
	F(i,2,n){
		if (a[i]!=a[i-1]) pd=0;
	}
	if (pd==0){
		puts("too hard!");return;
	}
	if (a[1]>=0){
		if ((p1&1)==(n&1)) wrn(ksm(2,1LL*n*(n-1)/2));
		else wrn(0);
	}
	else{
		int t=1;
		F(i,1,n){
			if (i<=3) t=1LL*t*num[1]%mod;
			else t=1LL*t*num[i-2]%mod;
		}
		int ans=ksm(2,1LL*n*(n-1)/2);
		ans=1LL*ans*num[n]%mod;
		if (p1+(n&1)==1){
			ans=(ans-t+mod)%mod;
			wrn(1LL*ans*ksm(2,mod-2)%mod);
		}
		else{
			ans=(ans+t)%mod;
			wrn(1LL*ans*ksm(2,mod-2)%mod);
		}
	}
}
signed main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();p1=read();init_c(n);num[0]=1;
	F(i,1,n) num[i]=1LL*num[i-1]*2%mod;
	F(i,1,n) a[i]=read();
	if (n>2){
		solve1();return 0;
	}
	f[0][0]=1;
	F(ci,1,n){
		int p=a[ci];
		F(i,0,ci){
			F(j,0,ci-i){
				if (!f[i][j]) continue;
				if (p==0||p==-1){
					F(k,0,j){
						add(g[i+((k&1)?0:1)][j],1LL*f[i][j]*C(j,k)%mod*num[ci-j-1]%mod);
					}
				}
				if (p==1||p==-1){
					F(k,0,i){
						add(g[i][j+((k&1)?0:1)],1LL*f[i][j]*C(i,k)%mod*num[ci-i-1]%mod);
					}
				}
			}
		}
		F(i,0,ci+1){
			F(j,0,ci-i+1){
				f[i][j]=g[i][j];g[i][j]=0;
			}
		}
		
		/*yk[0]=yk[1]=0;
		F(i,0,n){
			F(j,0,n){
			add(yk[(i+j)&1],f[i][j]);
			}
		}
		wri(ci);wri(yk[0]);wrn(yk[1]);*/
	}
	F(i,0,n){
		F(j,0,n){
			if (((i+j)&1)==p1) add(ans,f[i][j]);
		}
	}
	wrn(ans);
	return 0;
}
