#include <bits/stdc++.h>
#define ll long long
#define mp make_pair
#define be begin()
#define en end()
#define pa pair <int,int>
#define pb push_back
#define put putchar('\n')
#define re register
#define F(i,a,b) for (re int i=(a);i<=(b);i++)
#define D(i,a,b) for (re int i=(a);i>=(b);i--)
#define go(i,x) for (re int i=head[x];i;i=Next[i])
#define N 200055
#define fi first
#define se second
//#define mod
using namespace std;
#define gc getchar
inline int read(){char c;int f=1,x=0;for (c=gc();c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
for (;c>='0'&&c<='9';c=gc()) x=x*10+c-'0';return (f==1)?x:-x;}
inline void wr(int x){if (x<0){putchar('-');wr(-x);}else{if (x>=10) wr(x/10);putchar(x%10+'0');}}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);put;}
int n,m,ans[N],p[N],g[N][21],ti,tot,x,y,q;
#define V to[i]
int Next[N*2],to[N*2],head[N],nedge;
void add(int a,int b){
	Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;
}
void add_ne(int a,int b){
	add(a,b);add(b,a);
}
int fa[N][21],dis[N],in1[N],out1[N];
void dfs(int x){
	in1[x]=++ti;
	go(i,x){
		if (fa[x][0]==V) continue;
		dis[V]=dis[x]+1;fa[V][0]=x;dfs(V);
	}
	out1[x]=ti;
}
int pd(int x,int y){
	return (in1[x]<=in1[y]&&out1[x]>=out1[y]);
}
int g_lca(int x,int y){
	if (x==y) return x;
	if (dis[x]<dis[y]) swap(x,y);
	D(i,20,0){
		if (!pd(fa[x][i],y)) x=fa[x][i];
	}
	return fa[x][0];
}
void init(){
	F(i,1,20) F(j,1,n) fa[j][i]=fa[fa[j][i-1]][i-1];
}
void dfs2(int x){
	go(i,x){
		if (V==fa[x][0]) continue;
		dfs2(V);
		if (dis[g[V][0]]<dis[g[x][0]]) g[x][0]=g[V][0];
	}
}
pa g_d(int x,int y){
	if (x==y) return mp(0,x);
	if (dis[g[x][20]]>dis[y]) return mp(-500000000,x);
	int num=0;
	D(i,20,0){
		if (dis[g[x][i]]>dis[y]){
			x=g[x][i];num+=(1<<i);
		}
	}
	return mp(num+1,x);
}
struct xx{
	int x,i,l,r,c;
}c[N*2];
bool cmp(xx a,xx b){
	return (a.x<b.x);
}
vector <int> a[N];
struct tr1{
	int t[N];
	void add(int x,int k){
		for (;x<=n;x+=x&-x) t[x]+=k;
	}
	int query(int x){
		int sum=0;
		for (;x;x-=x&-x) sum+=t[x];
		return sum;
	}
}tr;
signed main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	F(i,2,n) add_ne(read(),i);
	dfs(1);fa[1][0]=1;
	init();
	m=read();
	F(i,1,n) g[i][0]=i;
	F(i,1,m){
		x=read();y=read();
		a[in1[x]].pb(in1[y]);
		a[in1[y]].pb(in1[x]);
		int lca=g_lca(x,y);
		if (dis[lca]<dis[g[x][0]]) g[x][0]=lca;
		if (dis[lca]<dis[g[y][0]]) g[y][0]=lca;
	}
	dfs2(1);g[1][0]=1; 
	F(i,1,20) F(j,1,n) g[j][i]=g[g[j][i-1]][i-1];
	q=read();
	F(i,1,q){
		x=read();y=read();
		if (dis[x]<dis[y]) swap(x,y);
		int lca=g_lca(x,y);
		if (lca==y) ans[i]=g_d(x,y).fi,p[i]=-1;
		else{
			pa t=g_d(x,lca);
			ans[i]=-2;ans[i]+=t.fi;x=t.se;
			t=g_d(y,lca);
			ans[i]+=t.fi;y=t.se;
			if (in1[x]>=out1[y]) swap(x,y);
			c[++tot].i=i;c[tot].c=-1;c[tot].x=in1[x]-1;c[tot].l=in1[y];c[tot].r=out1[y];
			c[++tot].i=i;c[tot].c=1;c[tot].x=out1[x];c[tot].l=in1[y];c[tot].r=out1[y];
		}
	}
	sort(c+1,c+tot+1,cmp);
	int t=1;
	F(i,1,tot){
		for (;t<=c[i].x;t++){
			if (a[t].empty()) continue;
			F(j,0,a[t].size()-1) tr.add(a[t][j],1);
		}
		p[c[i].i]+=c[i].c*(tr.query(c[i].r)-tr.query(c[i].l-1));
	}
	F(i,1,q){
		if (p[i]>0) ans[i]=ans[i]+1;
		if (p[i]==0) ans[i]=ans[i]+2;
		if (ans[i]<0) puts("-1");
		else wrn(ans[i]);
	}
	return 0;
}
