#include <bits/stdc++.h>
#define ll long long
#define mp make_pair
#define be begin()
#define en end()
#define pa pair <int,int>
#define pb push_back
#define put putchar('\n')
#define re register
#define F(i,a,b) for (re int i=(a);i<=(b);i++)
#define D(i,a,b) for (re int i=(a);i>=(b);i--)
#define go(i,x) for (re int i=head[x];i;i=Next[i])
#define N 500055
//#define mod
using namespace std;
#define gc getchar
inline int read(){char c;int f=1,x=0;for (c=gc();c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
for (;c>='0'&&c<='9';c=gc()) x=x*10+c-'0';return (f==1)?x:-x;}
inline void wr(int x){if (x<0){putchar('-');wr(-x);}else{if (x>=10) wr(x/10);putchar(x%10+'0');}}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);put;}
int n,m,ans,tot,a[N],b[N],T,hop,ra;
void solve1(){F(i,1,n) wri(i);}
void solve2(){D(i,n,1) wri(i);}
void dfs(int l,int r){
	if (l>r) return;
	int mid=(l+r)>>1;
	a[b[mid]]=++tot;
	b[mid]=b[l];
	dfs(l+1,r);
}
void solve3(){
	F(i,1,n) b[i]=i;dfs(1,n);
	F(i,1,n) wri(a[i]);
}
int rad(int l,int r){
	ra=1LL*ra*48271%2147483647;
	return ra %(r-l+1)+l;
}
void dfs2(int l,int r){
	if (l>r) return;
	int mid=rad(l,r);
	a[b[mid]]=++tot;
	b[mid]=b[l];
	dfs2(l+1,r);
}
void solve4(){
	F(i,1,n) b[i]=i;dfs2(1,n);
	F(i,1,n) wri(a[i]);
}
signed main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();T=read();hop=read();
	if (T==1) solve1();
	if (T==2) solve2();
	if (T==3) solve3();
	if (T==4){
		ra=read();solve4();
	}
	return 0;
}
