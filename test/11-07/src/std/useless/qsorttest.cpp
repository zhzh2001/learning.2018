#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <random>
using namespace std;
const int n = 1e5;
int a[n], b[n], cc;
int cmp(const void *a, const void *b)
{
	const int va = *(const int *)a;
	const int vb = *(const int *)b;
	if (++cc == 66666)
		cout << va << ' ' << vb << endl;
	return va - vb;
}
int main()
{
	minstd_rand gen(time(NULL));
	for (int i = 0; i < n; i++)
		a[i] = uniform_int_distribution<>(1, 1e9)(gen);
	for (;;)
	{
		cc = 0;
		copy(a, a + n, b);
		cout << clock() << ' ';
		qsort(a, n, sizeof(int), cmp);
	}
	return 0;
}