#include <iostream>
#include <algorithm>
#include <ctime>
#include <random>
using namespace std;
const int n = 1e5;
int a[n], b[n], cc;
bool cmp(int a, int b)
{
	if (++cc == 66666)
		cout << a << ' ' << b << endl;
	return a < b;
}
int main()
{
	minstd_rand gen(time(NULL));
	for (int i = 0; i < n; i++)
		a[i] = uniform_int_distribution<>(1, 1e9)(gen);
	for (;;)
	{
		cc = 0;
		copy(a, a + n, b);
		cout << clock() << ' ';
		sort(b, b + n, cmp);
	}
	return 0;
}