#include <iostream>
#include <algorithm>
using namespace std;
const int n = 1e5, INF = 1e9;
int a[n], p[n], now, pivot, cnt;
bool cmp(int x, int y)
{
	++cnt;
	if (a[x] == INF && a[y] == INF)
		if (x == pivot)
			a[x] = now++;
		else
			a[y] = now++;
	if (a[x] == INF)
		pivot = x;
	else if (a[y] == INF)
		pivot = y;
	return a[x] < a[y];
}
int cmp(const void *px, const void *py)
{
	const int x = *(const int *)px;
	const int y = *(const int *)py;
	++cnt;
	if (a[x] == INF && a[y] == INF)
		if (x == pivot)
			a[x] = now++;
		else
			a[y] = now++;
	if (a[x] == INF)
		pivot = x;
	else if (a[y] == INF)
		pivot = y;
	return a[x] - a[y];
}
int cmpt(const void *px, const void *py)
{
	const int x = *(const int *)px;
	const int y = *(const int *)py;
	++cnt;
	return x - y;
}
int main()
{
	for (int i = 0; i < n; i++)
	{
		a[i] = INF;
		p[i] = i;
	}
	//sort(p, p + n, cmp);
	qsort(p, n, sizeof(int), cmp);
	cout << cnt << endl;
	cnt = 0;
	qsort(a, n, sizeof(int), cmpt);
	cout << cnt << endl;
	cin.get();
	cin.get();
	return 0;
}