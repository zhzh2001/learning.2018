#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("sort.in");
ofstream fout("sort.out");
const int N = 100005, A = 48271, MOD = 2147483647;
int id[N], ans[N];
int main()
{
	int n, type, cnt;
	fin >> n >> type >> cnt;
	long long seed = 0;
	if(type == 4)
		fin >> seed;
	for (int i = 1; i <= n; i++)
		id[i] = i;
	int now = 0;
	for (int i = 1; i <= n; i++)
	{
		int pos;
		switch (type)
		{
		case 1:
			pos = i;
			break;
		case 2:
			pos = n;
			break;
		case 3:
			pos = (i + n) / 2;
			break;
		case 4:
			seed = seed * A % MOD;
			pos = seed % (n - i + 1) + i;
			break;
		}
		ans[id[pos]] = ++now;
		swap(id[i], id[pos]);
	}
	for (int i = 1; i <= n; i++)
		fout << ans[i] << ' ';
	fout << endl;
	return 0;
}
