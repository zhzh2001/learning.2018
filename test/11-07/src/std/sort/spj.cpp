#include <cstdio>
#include <ctime>
#include <cassert>
#include <cstdlib>
#include <algorithm>
using namespace std;
const int N = 100005, A = 48271, MOD = 2147483647;
int n, k, a[N], cnt, lim;
bool vis[N];
FILE *fres;
void Return(double score, const char *msg = nullptr)
{
	fprintf(fres, "%.3lf\n", score);
	if (msg)
		fprintf(fres, "%s\n", msg);
	exit(0);
}
long long seed;
void sort(int l, int r)
{
	int i = l, j = r, x;
	switch (k)
	{
	case 1:
		x = a[l];
		break;
	case 2:
		x = a[r];
		break;
	case 3:
		x = a[(l + r) / 2];
		break;
	case 4:
		seed = seed * A % MOD;
		x = a[seed % (r - l + 1) + l];
		break;
	}
	do
	{
		for (; a[i] < x; i++)
			if (++cnt > lim)
				Return(1., "Accepted");
		for (; x < a[j]; j--)
			if (++cnt > lim)
				Return(1., "Accepted");
		if (i <= j)
		{
			swap(a[i++], a[j--]);
			if ((cnt += 2) > lim)
				Return(1., "Accepted");
		}
	} while (i <= j);
	if (l < j)
		sort(l, j);
	if (i < r)
		sort(i, r);
}
int main(int argc, char *argv[])
{
	if (argc < 4)
		return 1;
	FILE *fin = fopen(argv[1], "r"), *fout = fopen(argv[3], "r");
	fres = argc > 4 ? fopen(argv[4], "w") : stdout;
	fscanf(fin, "%d%d%d", &n, &k, &lim);
	if (k == 4)
		fscanf(fin, "%lld", &seed);
	for (int i = 1; i <= n; i++)
	{
		fscanf(fout, "%d", a + i);
		if (a[i] <= 0 || a[i] > n || vis[a[i]])
			Return(.0, "Presentation Error");
		vis[a[i]] = true;
	}
	sort(1, n);
	Return(.0, "Wrong Answer");
	return 0;
}
