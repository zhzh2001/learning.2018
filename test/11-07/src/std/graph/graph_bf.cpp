#include<cstdio>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=56,mod=1e9+7;
int dp[N][N][N][N],n,p,a[N],bin[N],ans;
inline void init(){
	n=read(); p=read();
	for (int i=1;i<=n;i++) a[i]=read();
	bin[0]=1;
	for (int i=1;i<N;i++) bin[i]=bin[i-1]*2%mod;
}
inline void upd(int &x,int w){
	x=(x+w)%mod;
}
inline void solve(){
	dp[0][0][0][0]=1;
	for (int i=1;i<=n;i++){
		for (int w0=0;w0<i;w0++){
			for (int w1=0;w1<i-w0;w1++){
				for (int b0=0;b0<i-w0-w1;b0++){
					int b1=i-1-w0-w1-b0;
					if (!dp[i-1][w0][w1][b0]) continue;
					if (a[i]!=1){
						if (b1) {
							upd(dp[i][w0][w1+1][b0],bin[i-2]*dp[i-1][w0][w1][b0]%mod);
							upd(dp[i][w0+1][w1][b0],bin[i-2]*dp[i-1][w0][w1][b0]%mod);
						}else{
							upd(dp[i][w0][w1+1][b0],bin[i-1]*dp[i-1][w0][w1][b0]%mod);
						}
					}
					if (a[i]!=0){
						if (w1) {
							upd(dp[i][w0][w1][b0+1],bin[i-2]*dp[i-1][w0][w1][b0]%mod);
							upd(dp[i][w0][w1][b0],bin[i-2]*dp[i-1][w0][w1][b0]%mod);
						}else{
							upd(dp[i][w0][w1][b0],bin[i-1]*dp[i-1][w0][w1][b0]%mod);
						}
					}
				}
			}
		}
	}
	for (int w0=0;w0<=n;w0++)
		for (int w1=0;w1<=n-w0;w1++)
			for (int b0=0;b0<=n-w0-w1;b0++){
				int b1=n-w0-w1-b0;
				if ((w1+b1)%2==p) upd(ans,dp[n][w0][w1][b0]);
			}
	writeln(ans);
}
signed main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	init();	
	solve();
	return 0;	
}
