#include<cstdio>
#include<cstring>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int mod=1e9+7;
int ans,n,p;
inline void init(){
	n=read(); p=read();
}
int cur,pow1,pow2,dp[2][2][2][2];
inline void update(int &x,int y){
	x=x+y;
	if (x>=mod) x-=mod;
}
inline void solve(){
	dp[0][0][0][0]=1; pow1=1; pow2=0; cur=0;
	for (int i=1;i<=n;i++){
		memset(dp[cur^1],0,sizeof dp[cur^1]);
		int x=read();
		for (int wb1=0;wb1<2;wb1++){
			for (int hasb1=0;hasb1<2;hasb1++){
				for (int hasw1=0;hasw1<2;hasw1++){
					if (x!=1){ //white
						if (hasb1){
							update(dp[cur^1][wb1^1][hasb1][hasw1|1],1ll*dp[cur][wb1][hasb1][hasw1]*pow2%mod);
							update(dp[cur^1][wb1][hasb1][hasw1],1ll*dp[cur][wb1][hasb1][hasw1]*pow2%mod);
						}else{
							update(dp[cur^1][wb1^1][hasb1][hasw1|1],1ll*dp[cur][wb1][hasb1][hasw1]*pow1%mod);
						}
					}
					if (x!=0){
						if (hasw1){
							update(dp[cur^1][wb1^1][hasb1|1][hasw1],1ll*dp[cur][wb1][hasb1][hasw1]*pow2%mod);
							update(dp[cur^1][wb1][hasb1][hasw1],1ll*dp[cur][wb1][hasb1][hasw1]*pow2%mod);
						}else{
							update(dp[cur^1][wb1^1][hasb1|1][hasw1],1ll*dp[cur][wb1][hasb1][hasw1]*pow1%mod);
						}
					}
				}
			}
		}
		pow2=pow1; pow1=pow1*2%mod; cur^=1;
	}
	for (int hasb1=0;hasb1<2;hasb1++){
		for (int hasw1=0;hasw1<2;hasw1++){
			update(ans,dp[cur][p][hasb1][hasw1]);
		}
	}
	writeln(ans);
}
signed main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	init(); solve();
	return 0;
}
