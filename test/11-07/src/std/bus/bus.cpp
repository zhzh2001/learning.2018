#include<bits/stdc++.h>
#define rg register
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
inline void write(rg int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(rg int x){write(x); puts("");}
const int N=2e5+5;
struct edge{
	int link,next;
}e[N<<1];
struct event{
	int x,y,id,opt;
}b[N*5];
int lg[N],m,q,n,head[N],tot;
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add_edge(u,v); add_edge(v,u);
}
int mx,g[N][20],dep[N],fa[N][20],l[N],r[N],cnt;
void dfs(int u,int Fa){
	dep[u]=dep[Fa]+1; fa[u][0]=Fa; l[u]=++cnt; mx=max(mx,dep[u]);
	for (rg int i=1;(1<<i)<=dep[u];i++) fa[u][i]=fa[fa[u][i-1]][i-1];
	for (rg int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=Fa) dfs(v,u);
	}
	r[u]=cnt;
}
inline int LCA(int u,int v){
	if (dep[u]<dep[v]) swap(u,v);
	int delta=dep[u]-dep[v];
	for (rg int i=0;(1<<i)<=delta;i++) if (delta&(1<<i)) u=fa[u][i];
	if (u==v) return u;
	for (rg int i=lg[dep[u]]+1;i>=0;i--) if (fa[u][i]!=fa[v][i]) u=fa[u][i],v=fa[v][i];
	if (u!=v) return fa[u][0];
	return u;
}
inline void init(){
	n=read(); lg[0]=-1;
	for (rg int i=1;i<N;i++) lg[i]=lg[i>>1]+1;
	for (rg int i=2;i<=n;i++) insert(i,read());
	dfs(1,0); m=read(); tot=0;
	for (rg int i=1;i<=m;i++){
		int u=read(),v=read(),lca=LCA(u,v);
		if (l[u]>l[v]) swap(u,v);
		if (!g[u][0]||dep[lca]<dep[g[u][0]]) g[u][0]=lca;
		if (!g[v][0]||dep[lca]<dep[g[v][0]]) g[v][0]=lca;
		b[++tot]=(event){l[u],l[v],0,1};
	}
}
void Dfs(int u,int Fa){
	for (rg int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=Fa){
			Dfs(v,u);
			if (!g[u][0]||g[v][0]&&dep[g[v][0]]<dep[g[u][0]]) g[u][0]=g[v][0];
		}
	}
}
inline bool cmp(event A,event B){
	return A.x<B.x||(A.x==B.x&&A.id<B.id);
}
int bit[N];
inline int lowbit(rg int x){
	return x&(-x);
}
inline void update(rg int x){
	for (;x<=n;x+=lowbit(x)) bit[x]++;
}
inline int query(rg int x){
	int sum=0;
	for (;x;x-=lowbit(x)) sum+=bit[x];
	return sum;
}
int ans[N],ANS[N];
inline void solve(){
	Dfs(1,0);
	for (rg int i=1;i<=n;i++) if (g[i][0]==i) g[i][0]=0;
	for (rg int i=1;i<=lg[mx]+1;i++)
		for (int j=1;j<=n;j++) g[j][i]=g[g[j][i-1]][i-1];
	q=read();
	for (rg int i=1;i<=q;i++){
		int u=read(),v=read(),lca=LCA(u,v);
		if (l[u]>l[v]) swap(u,v);
		for (rg int j=lg[dep[u]]+1;j>=0;j--) if (g[u][j]&&dep[g[u][j]]>dep[lca]) u=g[u][j],ans[i]+=(1<<j);
		for (rg int j=lg[dep[v]]+1;j>=0;j--) if (g[v][j]&&dep[g[v][j]]>dep[lca]) v=g[v][j],ans[i]+=(1<<j);
		if ((!g[u][0]&&u!=lca)||(!g[v][0]&&v!=lca)) {ans[i]=-1; continue;}
		if (u==lca||v==lca) ans[i]++;
			else {
				ans[i]+=2;
				b[++tot]=(event){r[u],r[v],i,1};
				b[++tot]=(event){l[u]-1,r[v],i,-1};
				b[++tot]=(event){r[u],l[v]-1,i,-1};
				b[++tot]=(event){l[u]-1,l[v]-1,i,1};
			}
	}
	sort(b+1,b+1+tot,cmp);
	for (rg int i=1;i<=tot;i++){
		if (!b[i].id) update(b[i].y);
			else ANS[b[i].id]+=b[i].opt*query(b[i].y);
	}
	for (rg int i=1;i<=q;i++) writeln(ans[i]-(ANS[i]>0));
}
int main(){
	freopen("bus.in","r",stdin); freopen("bus.out","w",stdout);
	init(); solve();
	return 0;
}
