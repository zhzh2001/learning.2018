#include<cstdio>
#include<cstring>
#include<algorithm>
#define lowbit(x) (x&-x)
bool isdigit(char c){return c>='0'&&c<='9';}
int read(){
    int s=0,t=1;char c;
    while(!isdigit(c=getchar()))if(c=='-')t=-1;
    do{s=s*10+c-'0';}while(isdigit(c=getchar()));
    return s*t;
}   
using namespace std;
const int maxn=200010;
int n,deep[maxn],first[maxn],tot,f[maxn][22],fac[22],ans[maxn],in[maxn],ou[maxn],cnt,ask,c[maxn],ANS[maxn];
struct edge{int v,from;}e[maxn*2];
struct cyc{
    int x,y,k,id;
    bool operator < (const cyc &a)const{
        return x<a.x||(x==a.x&&id<a.id);
    }
}d[maxn*5];
void insert(int u,int v){tot++;e[tot].v=v;e[tot].from=first[u];first[u]=tot;}
void modify(int x,int k){for(int i=x;i<=n;i+=lowbit(i))c[i]+=k;}
int query(int x){if(!x)return 0;int ans=0;for(int i=x;i>=1;i-=lowbit(i))ans+=c[i];return ans;}
namespace lca{
    int f[maxn][22];
    void dfs(int x,int fa){
        in[x]=++cnt;
        for(int j=1;(1<<j)<=deep[x];j++)f[x][j]=f[f[x][j-1]][j-1];
        for(int i=first[x];i;i=e[i].from)if(e[i].v!=fa){
            deep[e[i].v]=deep[x]+1;
            f[e[i].v][0]=x;
            dfs(e[i].v,x);
        }
        ou[x]=cnt;
    }
    int lca(int x,int y){
        if(deep[x]<deep[y])swap(x,y);
        int d=deep[x]-deep[y];
        for(int j=0;(1<<j)<=d;j++)if((1<<j)&d)x=f[x][j];
        if(x==y)return x;
        for(int j=20;j>=0;j--)if((1<<j)<=deep[x]&&f[x][j]!=f[y][j]){
            x=f[x][j],y=f[y][j];
        }
        return f[x][0];
    }
}
void dfs(int x,int fa){
    for(int i=first[x];i;i=e[i].from)if(e[i].v!=fa){
        dfs(e[i].v,x);
        if(!f[x][0]||(f[e[i].v][0]&&deep[f[e[i].v][0]]<deep[f[x][0]]))f[x][0]=f[e[i].v][0];//!0
    }
}   
int main(){
	freopen("bus.in","r",stdin); freopen("bus.out","w",stdout);
    n=read();
    for(int i=2;i<=n;i++){
        int u=read();
        insert(u,i);insert(i,u);
    }
    lca::dfs(1,0);
    int m=read();
    for(int i=1;i<=m;i++){
        int a=read(),b=read(),t=lca::lca(a,b);
        if(in[a]>in[b])swap(a,b);//swap
        if(!f[a][0]||deep[t]<deep[f[a][0]])f[a][0]=t;
        if(!f[b][0]||deep[t]<deep[f[b][0]])f[b][0]=t;
        d[++ask]=(cyc){in[a],in[b],1,0};
    }
    dfs(1,0);
    for(int x=1;x<=n;x++)if(f[x][0]==x)f[x][0]=0;//can't set itself
    for(int j=1;j<=20;j++)for(int x=1;x<=n;x++)f[x][j]=f[f[x][j-1]][j-1];
    int q=read();
    fac[0]=1;for(int i=1;i<=20;i++)fac[i]=fac[i-1]*2;
    for(int i=1;i<=q;i++){
        int x=read(),y=read(),t=lca::lca(x,y);
        if(in[x]>in[y])swap(x,y);//
        for(int j=20;j>=0;j--)if(f[x][j]&&deep[f[x][j]]>deep[t])x=f[x][j],ans[i]+=fac[j];
        for(int j=20;j>=0;j--)if(f[y][j]&&deep[f[y][j]]>deep[t])y=f[y][j],ans[i]+=fac[j];
        if((!f[x][0]&&x!=t)||(!f[y][0]&&y!=t)){ans[i]=-1;continue;}
        if(x==t||y==t)ans[i]++;else{
            ans[i]+=2;
            d[++ask]=(cyc){ou[x],ou[y],1,i};
            d[++ask]=(cyc){in[x]-1,ou[y],-1,i};
            d[++ask]=(cyc){ou[x],in[y]-1,-1,i};
            d[++ask]=(cyc){in[x]-1,in[y]-1,1,i};
        }
        
    }
    sort(d+1,d+ask+1);
    for(int i=1;i<=ask;i++){
        if(!d[i].id){
            modify(d[i].y,d[i].k);
        }
        else{
            ANS[d[i].id]+=d[i].k*query(d[i].y);
        }
    }
    for(int i=1;i<=q;i++)printf("%d\n",ans[i]-(ANS[i]>0));
    return 0;
}
