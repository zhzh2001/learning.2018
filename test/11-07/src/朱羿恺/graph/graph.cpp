#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e6+10, mod = 1e9+7;
int n,m,p,a[N];
inline int power(int a,ll b){
	int ans=1;
	for (;b;b>>=1,a=1ll*a*a%mod) if (b&1) ans=1ll*ans*a%mod;
	return ans;
}
namespace Subtask1{
	int cnt,x[50],y[50];
	inline void init(){
		For(i,1,n) For(j,i+1,n) x[++cnt]=i,y[cnt]=j;
	}
	int dp[N];
	bool e[50][50];
	inline int d(int u){
		int sum=1;
		For(i,u+1,n) if (a[i]!=a[u]&&e[u][i]) sum+=d(i);
		return sum;
	}
	inline bool check(){
		int sum=0;
		For(i,1,n) sum+=d(i);
		return (sum&1)==m;
	}
	int ans;
	inline void Dfs(int k){
		if (k>cnt){ans+=check();return;}
		e[x[k]][y[k]]=0,Dfs(k+1),e[x[k]][y[k]]=1,Dfs(k+1);
	}
	inline void dfs(int k){
		if (k>n){Dfs(1);return;}
		if (a[k]!=-1) dfs(k+1);
			else a[k]=1,dfs(k+1),a[k]=0,dfs(k+1);
	}
	inline void Main(){
		init(),dfs(1),printf("%d\n",ans);
	} 
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(),m=read();
	if (m==1) p=((n&1)==0);
		else p=n&1;
	For(i,1,n) a[i]=read();
	bool flag=1;
	For(i,2,n) if (a[i]!=a[i-1]) flag=0;
	if (flag&&a[1]!=-1) return printf("%d",(n&1)==m?power(2,1ll*n*(n-1)/2):0),0;
	if (n<=5) return Subtask1::Main(),0;
}
