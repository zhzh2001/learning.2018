#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 2e5+10; 
int n,m,q,fa[N];
int tot,first[N],to[N],last[N];
inline void Add(int x,int y){to[++tot]=y,last[tot]=first[x],first[x]=tot;}
int size[N],son[N],dep[N];
inline void dfs(int u){
	dep[u]=dep[fa[u]]+1,size[u]=1;
	cross(i,u){
		int v=to[i];
		dfs(v),size[u]+=size[v];
		if (size[son[u]]<size[v]) son[u]=v;
	}
}
int top[N];
inline void dfs(int u,int Top){
	top[u]=Top;
	if (son[u]) dfs(son[u],Top);
	cross(i,u) if (to[i]!=son[u]) dfs(to[i],to[i]);
}
inline int lca(int x,int y){
	for (;top[x]!=top[y];dep[top[x]]>dep[top[y]]?x=fa[top[x]]:y=fa[top[y]]);
	return dep[x]<dep[y]?x:y;
}
namespace Subtask1{
	int tot,a[110],dis[110][110];
	inline void Main(){
		For(i,1,n) For(j,1,n) dis[i][j]=1e9;
		For(i,1,n) dis[i][i]=0;
		m=read();
		while (m--){
			int x=read(),y=read(),Lca=lca(x,y);tot=0;
			while (x!=Lca) a[++tot]=x,x=fa[x];
			a[++tot]=Lca;
			while (y!=Lca) a[++tot]=y,y=fa[y];
			For(i,1,tot) For(j,i+1,tot) dis[a[i]][a[j]]=dis[a[j]][a[i]]=1;
		}
		For(k,1,n) For(i,1,n) For(j,1,n) dis[i][j]=min(dis[i][j],dis[i][k]+dis[k][j]);
		q=read();
		while (q--){
			int x=read(),y=read();
			printf("%d\n",dis[x][y]==1e9?-1:dis[x][y]);
		}
	}
}
namespace Subtask2{
	vector<int>v[N];
	inline void Main(){
		m=read();
		while (m--){
			int x=read(),y=read();
			if (x==y) continue;
			v[x].push_back(y),v[y].push_back(x);
		}
		For(i,1,n) sort(v[i].begin(),v[i].end());
		For(i,1,n) v[i].push_back(n+1);q=read();
		while (q--){
			int x=read(),y=read();
			if (x==y){puts("0");continue;}
			if (x!=1&&y!=1){
				if (v[x].size()==1||v[y].size()==1){puts("-1");continue;}
				int z=lower_bound(v[x].begin(),v[x].end(),y)-v[x].begin();
				if (v[x][z]==y) puts("1");
					else puts("2");
			} else {
				if (y==1) swap(x,y);
				if (v[y].size()==1) puts("-1");
					else puts("1");
			}
		}
	}
}
namespace Subtask3{
	struct node{
		int l,r;
	}b[N];
	inline bool cmp(node a,node b){return a.l<b.l;}
	int f[N][21],Max;
	inline void Query(int x,int y){
		int ans=0;
		if (x==y){puts("0");return;}
		if (f[x][20]<y){puts("-1");return;}
		Dow(i,20,0) if (f[x][i]<y) ans+=(1<<i),x=f[x][i];
		printf("%d\n",ans+1);
	}
	inline void Main(){
		m=read();
		For(i,1,m) b[i]=(node){read(),read()};
		For(i,1,m) if (b[i].l>b[i].r) swap(b[i].l,b[i].r);
		sort(b+1,b+1+m,cmp);
		int l=1;Max=0;
		For(i,1,n){
			while (b[l].l<=i&&l<=m) Max=max(Max,b[l].r),l++;
			f[i][0]=Max;//printf("%d ",Max);
		}//puts("");
		For(j,1,20)
			For(i,1,n) f[i][j]=f[f[i][j-1]][j-1];
		q=read();
		while (q--){
			int x=read(),y=read();
			if (x>y) swap(x,y);
			Query(x,y);
		}
	}	
};
int Min[N],pos[N],f[N][21],F[N][21];
inline void Dfs1(int u){
	cross(i,u){
		Dfs1(to[i]);
		if (Min[to[i]]<Min[u]) Min[u]=Min[to[i]],pos[u]=pos[to[i]];
	}
}
inline int Query(int x,int y){
	if (x==y) return 0;
	int ans=0;
	Dow(i,20,0) if (f[x][i]>dep[y]) ans+=(1<<i),x=F[x][i];
	return ans+1;
}
int main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	For(i,2,n) fa[i]=read(),Add(fa[i],i);
	dfs(1),dfs(1,1);
	if (n<=100) return Subtask1::Main(),0;
	bool flag=(fa[2]==1);
	For(i,3,n) if (fa[i]!=fa[i-1]) flag=0;
	if (flag) return Subtask2::Main(),0;
	flag=1;
	For(i,1,n) if (fa[i]!=i-1) flag=0;
	if (flag) return Subtask3::Main(),0;
	m=read();
	For(i,1,n) Min[i]=n+1;
	while (m--){
		int x=read(),y=read(),Lca=lca(x,y);
		if (dep[Lca]<Min[x]) Min[x]=dep[Lca],pos[x]=Lca;
		if (dep[Lca]<Min[y]) Min[y]=dep[Lca],pos[y]=Lca;
	}
	Dfs1(1);
	For(i,1,n) f[i][0]=Min[i],F[i][0]=pos[i];
	For(j,1,20)
		For(i,1,n)
			if (f[i][j-1]!=n+1) f[i][j]=f[f[i][j-1]][j-1],F[i][j]=F[F[i][j-1]][j-1];
				else f[i][j]=n+1;
	q=read();
	while (q--){
		int x=read(),y=read(),Lca=lca(x,y);
		if (f[x][20]>dep[Lca]||f[y][20]>dep[Lca]) puts("-1");
			else printf("%d\n",Query(x,Lca)+Query(y,Lca));
	}
}
/*
7
1 1 1 4 5 6
4
4 2
5 4
1 3
6 7
5
4 5
3 5
7 2
3 2
5 3
*/
