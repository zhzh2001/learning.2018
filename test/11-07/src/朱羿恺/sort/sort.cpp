#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,opt,cnt,pos[N],val[N];
ll x[N];
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(),opt=read(),cnt=read();
	if (opt==1||opt==2){
		For(i,1,n) printf("%d ",i);
		return 0;
	}
	if (opt==3){
		For(i,1,n) pos[i]=i;
		For(l,1,n){
			int r=n,mid=l+r>>1;
			swap(pos[l],pos[mid]),val[pos[l]]=l;
		}
		For(i,1,n) printf("%d ",val[i]);
		return 0;
	}
	if (opt==4){
		x[0]=read();
		For(i,1,n) x[i]=(x[i-1]*48271)%2147483647;
		For(i,1,n) pos[i]=i;
		For(l,1,n){
			int r=n,mid=x[l]%(r-l+1)+l;//printf("%d %d %d\n",l,r,mid);
			swap(pos[l],pos[mid]),val[pos[l]]=l;
		}
		For(i,1,n) printf("%d ",val[i]);
		return 0;
	}
}
