#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int mod=2147483647;
const int maxn=100000+10;
int xi,n,type,cnt,cntx=0;
int A[maxn],ans[maxn];
bool vis[maxn];
inline int read()
{
	int f=1,x=0;
	char c;
	c=getchar();
	while(c>'9' || c<'0'){if(c=='-')f=-1;c=getchar();}
	while(c>='0' && c<='9'){x=x*10+(c-'0');c=getchar();}
	return f*x;
}

inline int random(int l,int r)
{
	xi=(xi*48271)%mod;
	return xi%(r-l+1)+l;
}
inline int F(int l,int r)
{
	if(type==1)return l;
	if(type==2)return r;
	if(type==3)return (l+r)>>1;
	if(type==4)return random(l,r);
}
inline void Qsort(int l,int r)
{
	int i=l,j=r;
	int x=A[F(l,r)];
	while(i<=j)
	{
		while(A[i]<x){i++;cntx++;}
		while(x<A[j]){j--;cntx++;}
		if(i<=j){
			swap(A[i],A[j]);
			i++;
			cntx++;
			j--;
			cntx++;
		}
	}
	if(l<j)Qsort(l,j);
	if(i<r)Qsort(i,r);
	return;
}
inline void buildA(int i)
{
	if(i>n){
		cntx=0;
		for(int j=1;j<=n;j++)
		{
			A[j]=ans[j];
		}
		Qsort(1,n);
		if(cntx>cnt)
		{
			for(int j=1;j<=n;j++)
			{
				cout<<ans[j];
				if(j==n)cout<<endl;
				else cout<<" ";
			}
			exit(1);
		}
		return;
	}
	for(int k=1;k<=n;k++)
	{
		if(!vis[k])
		{
			ans[i]=k;
			vis[k]=true;
			buildA(i+1);
			vis[k]=false;
		}
	}
}
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();type=read(),cnt=read();
	if(type==4)xi=read();
	buildA(1);
	return 0;
}
