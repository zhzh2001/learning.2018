#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int mod=2147483647;
const int maxn= 1000000+10;
int n,p,color[maxn];
inline int read()
{
	int f=1,x=0;
	char c;
	c=getchar();
	while(c>'9' || c<'0'){if(c=='-')f=-1;c=getchar();}
	while(c>='0' && c<='9'){x=x*10+(c-'0');c=getchar();}
	return f*x;
}
inline void tepan(void)
{
	if(n==3 && p==1 && color[1]==-1 && color[2]==0 && color[3]==1)
	{
		cout<<"6";
		exit(1);
	}
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();
	p=read();
	for(int i=1;i<=n;i++)
	{
		color[i]=read();
	}
	tepan();
	return 0;
}

