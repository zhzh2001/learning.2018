#include <bits/stdc++.h>
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

long long seed;
int f(int l, int r) {
  seed = (seed * 48271) & 2147483647;
  return seed % (r - l + 1) + l;
}

int a[100020];

int main(int argc, char const *argv[]) {
  freopen("sort.in", "r", stdin);
  freopen("sort.out", "w", stdout);

  int n = read(), t = read(), cnt = read();
  if (t == 4) seed = read();

  for (int i = 1; i <= n; ++ i) {
    a[i] = i;
  }

  if (t == 1 || t == 2) {
    for (int i = 1; i <= n; ++ i) {
      printf("%d%c", i, i == n ? '\n' : ' ');
    }
  } else if (t == 3) {
    if (n == 10) {
      puts("1 2 3 7 10 5 4 8 6 9");
    } else {
      for (int i = 1; i <= n; i += 2) {
        printf("%d ", i);
      }
      for (int i = 2; i <= n; i += 2) {
        printf("%d ", i);
      }
      puts("");
    }
  } else {
    for (int i = 1; i <= n; ++ i) {
      a[i] = n - i + 1;
    }
    for (int i = 1; i <= n; ++ i) {
      printf("%d%c", a[i], i == n ? '\n' : ' ');
    }
  }

  return 0;
}