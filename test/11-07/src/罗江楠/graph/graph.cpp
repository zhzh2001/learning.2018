#include <bits/stdc++.h>
#define N 1000020
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int a[N], n, p, ans;
/*
int dfs2() {

}

void dfs(int x) {
  if (x == n + 1) {
    dfs2();
    return;
  }
  if (a[x] == -1) {
    a[x] = 1;
    dfs(x + 1);
    a[x] = 0;
    dfs(x + 1);
  } else {
    dfs(x + 1);
  }
}
*/
typedef long long ll;
const int mod = 1000000007;
ll fast_pow(ll x, ll y, ll p) {
  ll z = 1;
  for (; y; y >>= 1, x = x * x % p) {
    if (y & 1) {
      z = z * x % p;
    }
  }
  return z;
}
ll inv(ll x, ll p) {
  return fast_pow(x, p - 2, p);
}
int main(int argc, char const *argv[]) {
  freopen("graph.in", "r", stdin);
  freopen("graph.out", "w", stdout);

  /*
  n = read(); p = read();
  for (int i = 1; i <= n; ++ i) {
    a[i] = read();
  }
  dfs(1);
  */
  ll n = read(), p = read();
  if (n % 2 == p % 2) {
    ll k = (n % 2) ? ((n - 1) / 2 * n % (mod - 1)) : (n / 2 * (n - 1) % (mod - 1));
    // printf("%lld\n", k);
    printf("%lld\n", fast_pow(2, k, mod));
  } else {
    puts("0");
  }
  return 0;
}
