#include <bits/stdc++.h>
#define N 200020
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

struct __bus {
  int u, v;
  friend bool operator < (const __bus &a, const __bus &b) {
    return a.u == b.u ? a.v < b.v : a.u < b.u;
  }
} bs[N], qs[N];
int fa[N], n, m, q;

namespace subtask1 {
#define SMALL_N 5020
int to[SMALL_N<<1], nxt[SMALL_N<<1], head[SMALL_N], cnt;
void insert(int x, int y) {
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
  to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}
int f[SMALL_N][14];
int dep[SMALL_N];
void dfs(int x, int f) {
  subtask1::f[x][0] = f;
  dep[x] = dep[f] + 1;
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) {
      continue;
    }
    dfs(to[i], x);
  }
}
int lca(int x, int y) {
  if (dep[x] < dep[y]) {
    swap(x, y);
  }
  int t = dep[x] - dep[y];
  for (int i = 13; ~i; -- i) {
    if (t >> i & 1) {
      x = f[x][i];
    }
  }
  if (x == y) return x;
  for (int i = 13; ~i; -- i) {
    if (f[x][i] != f[y][i]) {
      x = f[x][i];
      y = f[y][i];
    }
  }
  return x == y ? x : f[x][0];
}
typedef bitset<SMALL_N> bst;
bst stop[SMALL_N];
int calc(int x, int y) {
  int z = lca(x, y);
  vector<int> v1, v2;
  while (x != z) v1.push_back(x), x = f[x][0];
  while (y != z) v2.push_back(y), y = f[y][0];
  v1.push_back(z);
  for (size_t i = v2.size() - 1; ~i; -- i) {
    v1.push_back(v2[i]);
  }
  int ans = 1;
  bst now = stop[v1[0]];
  for (size_t i = 1; i < v1.size(); ++ i) {
    // printf("pass %d\n", v1[i]);
    int x = v1[i], y = v1[i - 1];
    bst chs = (stop[x] & stop[y]);
    if (!chs.count()) {
      return -1;
    }
    now &= chs;
    if (!now.count()) {
      // printf("need to swith\n");
      ++ ans;
      now = chs;
    }
  }
  return ans;
}
void solve() {
  for (int i = 2; i <= n; ++ i) {
    insert(fa[i], i);
  }
  dfs(1, 0);
  for (int i = 1; i < 14; ++ i) {
    for (int j = 1; j <= n; ++ j) {
      f[j][i] = f[f[j][i - 1]][i - 1];
    }
  }
  for (int i = 1; i <= m; ++ i) {
    int x = bs[i].u, y = bs[i].v;
    int z = lca(x, y);
    while (x != z) {
      stop[x][i] = 1;
      x = f[x][0];
    }
    while (y != z) {
      stop[y][i] = 1;
      y = f[y][0];
    }
    stop[z][i] = 1;
  }
  for (int i = 1; i <= q; ++ i) {
    printf("%d\n", calc(qs[i].u, qs[i].v));
  }
}
}


namespace subtask2 {
int a[N][14];
int query(int l, int r) {
  int k = log2(r - l + 1);
  return min(a[l][k], a[r-(1<<k)+1][k]);
}
void solve() {
  sort(bs + 1, bs + n + 1);
  for (int i = 1; i <= m; ++ i) {
    ++ a[bs[i].u][0];
    -- a[bs[i].v + 1][0];
  }
  for (int i = 1; i <= n; ++ i) {
    a[i][0] += a[i - 1][0];
  }
  for (int i = 1; i < 14; ++ i) {
    for (int j = 1; j <= n; ++ j) {
      a[j][i] = min(a[j][i - 1], a[j - (1 << (i - 1)) + 1][i - 1]);
    }
  }
  for (int i = 1; i <= q; ++ i) {
    puts(query(qs[i].u, qs[i].v) ? "1" : "-1");
  }
}
}


namespace subtask3 {
set<__bus> st;
bool vis[N];
void solve() {
  for (int i = 1; i <= m; ++ i) {
    if (bs[i].u == bs[i].v) {
      continue;
    }
    st.insert(bs[i]);
    vis[bs[i].u] = true;
    vis[bs[i].v] = true;
  }
  for (int i = 1; i <= q; ++ i) {
    if (qs[i].u == 1) {
      puts(vis[qs[i].v] ? "1" : "-1");
    } else if (!vis[qs[i].u] || !vis[qs[i].v]) {
      puts("-1");
    } else if (st.find(qs[i]) != st.end()) {
      puts("1");
    } else {
      puts("2");
    }
  }
}
}

int main(int argc, char const *argv[]) {
  freopen("bus.in", "r", stdin);
  freopen("bus.out", "w", stdout);

  bool f1 = true, f2 = true;
  n = read();
  for (int i = 2; i <= n; ++ i) {
    fa[i] = read();
    f1 &= (fa[i] == i - 1);
    f2 &= (fa[i] == 1);
  }
  m = read();
  for (int i = 1; i <= m; ++ i) {
    bs[i].u = read();
    bs[i].v = read();
    if (bs[i].u > bs[i].v) {
      swap(bs[i].u, bs[i].v);
    }
  }
  q = read();
  for (int i = 1; i <= q; ++ i) {
    qs[i].u = read();
    qs[i].v = read();
    if (qs[i].u > qs[i].v) {
      swap(qs[i].u, qs[i].v);
    }
  }

  if (n <= 5000 && m <= 5000) {
    subtask1::solve();
  } else if (f1) {
    subtask2::solve();
  } else if (f2) {
    subtask3::solve();
  }

  return 0;
}