#include <bits/stdc++.h>
using namespace std;
#define file "graph"
typedef long long ll;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT){
		TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
		if (SSS==TTT)
			return -1;
	}
	return *SSS++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
const int mod=1000000007;
ll dp[1000002][2][2][2],pw[1000002]; //1 b1 w1
int a[1000002];
inline void upd(ll &x,ll y){
	x=(x+y)%mod;
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read(),p=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	pw[0]=1;
	for(int i=1;i<=n;i++)
		pw[i]=pw[i-1]*2%mod;
	if (a[1]!=1)
		dp[1][1][1][0]=1;
	if (a[1])
		dp[1][1][0][1]=1;
	for(int i=1;i<n;i++){
		int o=a[i+1];
		for(int j=0;j<2;j++){
			//dp[i][j][0][1]
			if (o!=1){ //can black
				upd(dp[i+1][j][0][1],dp[i][j][0][1]*pw[i-1]);
				upd(dp[i+1][j^1][1][1],dp[i][j][0][1]*pw[i-1]);
			}
			if (o) //can white
				upd(dp[i+1][j^1][0][1],dp[i][j][0][1]*pw[i]);
			//dp[i][j][0][1]
			if (o){ //can white
				upd(dp[i+1][j][1][0],dp[i][j][1][0]*pw[i-1]);
				upd(dp[i+1][j^1][1][1],dp[i][j][1][0]*pw[i-1]);
			}
			if (o!=1) //can balck
				upd(dp[i+1][j^1][1][0],dp[i][j][1][0]*pw[i]);
			//dp[i][j][1][1]
			if (o!=1){ //can black
				upd(dp[i+1][j][1][1],dp[i][j][1][1]*pw[i-1]);
				upd(dp[i+1][j^1][1][1],dp[i][j][1][1]*pw[i-1]);
			}
			if (o){ //can white
				upd(dp[i+1][j][1][1],dp[i][j][1][1]*pw[i-1]);
				upd(dp[i+1][j^1][1][1],dp[i][j][1][1]*pw[i-1]);
			}	
		}
	}
	printf("%lld",(dp[n][p][0][0]+dp[n][p][1][0]+dp[n][p][0][1]+dp[n][p][1][1])%mod);
}
