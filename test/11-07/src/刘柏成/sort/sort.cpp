#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define file "sort"
const int L=1000000;
const int mod=2147483647;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT){
		TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
		if (SSS==TTT)
			return -1;
	}
	return *SSS++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int n,typ;
ll now;
int gt(int l,int r){
	if (typ==1)
		return l;
	else if (typ==2)
		return r;
	else if (typ==3)
		return (l+r)/2;
	else {
		now=(now*48271)%mod;
		return now%(r-l+1)+l;
	}
}
const int maxn=100002;
int a[maxn];
/*void qs(int l,int r){
	fprintf(stderr,"qs %d %d\n",l,r);
	for(int i=l;i<=r;i++)
		fprintf(stderr,"a[%d]=%d ",i,a[i]);
	int i=l,j=r,x=a[gt(l,r)];
	fprintf(stderr,"choose %d\n",x);
	do{
		while(a[i]<x)
			i++;
		while(x<a[j])
			j--;
		if (i<=j){
			swap(a[i],a[j]);
			i++;
			j--;
		}
	}while(i<=j);
	if (l<j)
		qs(l,j);
	if (i<r)
		qs(i,r);
}*/
int qwq[maxn];
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=read();
	typ=read();
	read();
	if (typ==4)
		now=read();
	for(int i=1;i<=n;i++)
		qwq[i]=i;
	for(int i=n;i;i--){
		//fprintf(stderr,"%d\n",gt(i));
//		for(int j=1;j<=i;j++)
//			fprintf(stderr,"qwq[%d]=%d\n",j,qwq[j]);
		int& p=qwq[gt(1,i)];
		//fprintf(stderr,"%d\n",p);
		a[p]=i;
		swap(p,qwq[i]);
	}
	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
//	qs(1,n);
}
