#include <bits/stdc++.h>
using namespace std;
#define file "bus"
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT){
		TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
		if (SSS==TTT)
			return -1;
	}
	return *SSS++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
struct bfs_graph{
	int m;
	struct edge{
		int to,next;
	}e[100002];
	int first[402],q[402],d[402];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	void bfs(int s){
		int l=1,r=1;
		q[1]=s;
		memset(d,-1,sizeof(d));
		d[s]=0;
		while(l<=r){
			int u=q[l++];
			for(int i=first[u];i;i=e[i].next){
				int v=e[i].to;
				if (d[v]!=-1)
					continue;
				d[v]=d[u]+1;
				q[++r]=v;	
			}
		}
	}
}g2;
struct tree_graph{
	int n,m,id;
	struct edge{
		int to,next;
	}e[402];
	int first[402];
	void init(int n){
		this->n=id=n;
	}
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	int dep[402],fa[402];
	void dfs(int u){
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (fa[u]==v)
				continue;
			fa[v]=u;
			dep[v]=dep[u]+1;
			dfs(v);
		}
	}
	void pre(int u,int v){
		id++;
		while(u!=v){
			if (dep[u]<dep[v])
				swap(u,v);
			g2.addedge(u,id);
			g2.addedge(id,u);
			u=fa[u];
		}
		g2.addedge(u,id);
		g2.addedge(id,u);
	}
	int query(int u,int v){
		g2.bfs(u);
		return g2.d[v]==-1?-1:g2.d[v]/2;
	}
}g;
pair<int,int> ww[200002];
bool on[200002];
void work(int n){
	for(int i=2;i<=n;i++)
		read();
	int m=read();
	for(int i=1;i<=m;i++){
		int u=read(),v=read();
		if (u>v)
			swap(u,v);
		if (u!=v)
			on[u]=on[v]=1;
		ww[i]=make_pair(u,v);
	}
	int q=read();
	sort(ww+1,ww+m+1);
	for(int i=1;i<=q;i++){
		int a=read(),b=read();
		if (a>b)
			swap(a,b);
		if (a==b){
			puts("0");
			continue;
		}
		if (a==1){
			if (on[b])
				puts("1");
			else puts("-1");
			continue;
		}
		pair<int,int> qwq=make_pair(a,b);
		int p=lower_bound(ww+1,ww+m+1,qwq)-ww;
		if (p!=m+1 && ww[p]==qwq){
			puts("1");
			continue;
		}
		if (on[a] && on[b])
			puts("2");
		else puts("-1");
	}
}
int p[200002];
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read();
	if (n>100)
		return work(n),0;
	g.init(n);
	for(int i=2;i<=n;i++){
		p[i]=read();
		g.addedge(i,p[i]);
		g.addedge(p[i],i);
	}
	g.dfs(1);
	int m=read();
	while(m--){
		int u=read(),v=read();
		g.pre(u,v);
	}
	int q=read();
	while(q--){
		int u=read(),v=read();
		//printf("query %d %d\n",u,v);
		printf("%d\n",g.query(u,v));
	}
}
