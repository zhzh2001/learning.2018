#include<bits/stdc++.h>
using namespace std;
const long long L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc()
{
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline long long read()
{
	long long x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
long long n,m,cnt,ram,now;
long long a[100005],shu[100005];
long long js(long long l,long long r)
{
	ram=ram*48271%2147483647;
	return ram%(r-l+1)+l;
}
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	scanf("%lld%lld%lld",&n,&m,&cnt);
	if(m==4) scanf("%lld",&ram);
	for(long long i=1;i<=n;++i) a[i]=i;
	for(long long r=n,l=1;r;--r)
	{
		if(m==1) now=1;
		else if(m==2) now=r;
		else if(m==3) now=(l+r)/2;
		else now=js(l,r);
		shu[a[now]]=r;
		swap(a[now],a[r]);
	}
	for(long long i=1;i<=n;++i) write(shu[i]),putchar(' ');
	return 0;
}
