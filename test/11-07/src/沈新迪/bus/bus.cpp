#include<bits/stdc++.h>
using namespace std;
const long long L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc()
{
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
#define gc getchar
inline long long read()
{
	long long x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
int n,m,q,cnt;
int tot,head[200005],nx[400005],to[400005];
int dep[200005],faa[200005],fat[200005];
void jia(int aa,int bb)
{
	tot++;
	nx[tot]=head[aa];
	to[tot]=bb;
	head[aa]=tot;
	return;
}
void dfs(int rt,int fa)
{
	dep[rt]=dep[fa]+1;
	fat[rt]=fa;
	faa[rt]=rt;
	for(int i=head[rt];i;i=nx[i])
	{
		int yy=to[i];
		if(yy==fa) continue;
		dfs(yy,rt);
	}
	return;
}
int mn(int aa,int bb)
{
	return dep[aa]>dep[bb]?bb:aa;
}
int mx(int aa,int bb)
{
	return dep[aa]>dep[bb]?aa:bb;
}
int main()
{
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	for(int i=2;i<=n;++i)
	{
		int x=read();
		jia(x,i);jia(i,x);
	}
	m=read();
	dfs(1,1);
	for(int i=1;i<=m;++i)
	{
		int x=read(),y=read(),X=x,Y=y;
		if(dep[x]>dep[y]) swap(x,y);
		while(dep[y]>dep[x]) y=fat[y];
		while(x!=y) x=fat[x],y=fat[y];
		if(x!=y) x=fat[x],y=fat[y];
		while(X!=x) faa[X]=mn(x,faa[X]),X=fat[X];
		while(Y!=y) faa[Y]=mn(y,faa[Y]),Y=fat[Y];//cout<<x<<" "<<faa[5]<<endl;
	}
	q=read();
	for(int i=1;i<=q;++i)
	{
		int x=read(),y=read(),X=x,Y=y;
		if(dep[x]>dep[y]) swap(x,y);
		while(dep[y]>dep[x]) y=fat[y];
		while(x!=y) x=fat[x],y=fat[y];
		if(x!=y) x=fat[x],y=fat[y];
		int ans=0;
		while(X!=Y)
		{
			if(dep[Y]>dep[X]&&faa[Y]!=Y) Y=mx(faa[Y],x),ans++;
			else if(dep[X]>=dep[Y]&&faa[X]!=X) X=mx(faa[X],x),ans++;
			else {ans=-1;break;}
		}
		write(ans);puts("");
	}
	return 0;
}
