#include<bits/stdc++.h>
using namespace std;
const long long L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc()
{
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline long long read()
{
	long long x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
const long long mod=1e9+7;
long long n,p,now,ans,ff=0,X[1000005];
long long f[2][3][3][3][3][2];
long long A[5]={17,19,14,12},B[5]={5,13,26,18};
long long ok(long long yuan,long long wz)
{
	if(yuan&(1<<(wz-1))) return 1;
	return 0;
}
long long ksm(long long aa,long long bb)
{
	long long ans=1;
	while(bb)
	{
		if(bb&1) ans=(ans*aa)%mod;
		bb=bb>>1;
		aa=(aa*aa)%mod;
	}
	return ans;
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(),p=read();
	for(long long i=1;i<=n;++i) {X[i]=read();if(X[i]==0) ff++;}
	if(ff==n||ff==0)
	{
		if(n%2!=p) puts("0");
		else write(ksm(2,n*(n-1)/2));
		return 0;
	}
	f[now][0][0][0][0][0]=1;
	for(long long i=1;i<=n;++i)
	{
		now^=1;
		long long x=X[i];
		if(x==0||x==-1)
		for(long long l=0;l<32;++l)
		{
			long long& qwq=f[now][(l&16)>>4][(l&8)>>3][(l&4)>>2][(l&2)>>1][l&1];
//			long long a=(l&16)>>4,b=(l&8)>>3,c=(l&4)>>2,d=(l&2)>>1;
			for(long long j=0;j<4;++j)
			{
				long long tmp=l^A[j];
				qwq=(qwq+f[now^1][(tmp&16)>>4][(tmp&8)>>3][(tmp&4)>>2][(tmp&2)>>1][tmp&1])%mod;
			}
		}
		if(x==1||x==-1)
		for(long long l=0;l<32;++l)
		{
			long long& qwq=f[now][(l&16)>>4][(l&8)>>3][(l&4)>>2][(l&2)>>1][l&1];
			for(long long j=0;j<4;++j)
			{
				long long tmp=l^B[j];
				qwq=(qwq+f[now^1][(tmp&16)>>4][(tmp&8)>>3][(tmp&4)>>2][(tmp&2)>>1][tmp&1])%mod;
			}
		}
//		if(x==0||x==-1)
//		{
//			for(int l=1;l<16;++l)
//			{
//				int a=ok(l,4),b=ok(l,3),c=ok(l,2),d=ok(l,1);
//				if(a==1) 
//				{
//					f[now][1][b][c][d][0]=(f[now][1][b][c][d][0]+f[now^1][2][b][c][d][1])%mod;
//					f[now][1][b][c][d][1]=(f[now][1][b][c][d][1]+f[now^1][2][b][c][d][0])%mod;
//					if(!c) f[now][1][b][c][d][0]=(f[now])
//				}
//			}
////			f[now][1][2][2][2][1]=(f[now^1][2][2][2][2][0]+f[now^1][0][2][2][2][0])%mod;
////			f[now][1][2][2][1][1]=(f[now^1][2][2][2][1][0]+f[now^1][0][2][2][1][0]+f[now^1][2][2][2][0][0]+f[now^1][0][2][2][0][0])%mod;
////			f[now][1][2][1][1][1]=(f[now^1][2][2][1][1][0]+f[now^1][0][2][1][1][0]+f[now])
//		}
		for(long long l=0;l<32;++l) 
		{//if(i==3) cout<<f[now][(l&16)>>4][(l&8)>>3][(l&4)>>2][(l&2)>>1][l&1]<<endl;
			f[now^1][(l&16)>>4][(l&8)>>3][(l&4)>>2][(l&2)>>1][l&1]=0;
		}
	}
	for(long long l=0;l<32;++l) if((l&1)==p) ans=(ans+f[now][(l&16)>>4][(l&8)>>3][(l&4)>>2][(l&2)>>1][l&1])%mod;
	write(ans);
	return 0;
}
