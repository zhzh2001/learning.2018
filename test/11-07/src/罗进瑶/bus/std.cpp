#include<bits/stdc++.h>
typedef long long ll;
void setIO() {
	freopen("bus.in", "r", stdin);
	freopen("bus.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();
}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = - x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int N = 5100; 
int n, d[N], f[N][21];
int len[N * 2], p[N], val[N], m;
bitset<11100>v[N];
int ver[N * 2], nxt[N * 2], en, head[N];
void add(int x, int y) {
	ver[++en] = y, nxt[en] = head[x], head[x] = en;
}
void dfs(int x, int F) {
	f[x][0] = F;
	for(int i = 1; i <= 20; ++i) f[x][i] = f[f[x][i - 1]][i - 1];
	d[x] = d[F] + 1;
	for(int i = head[x]; i;i = nxt[i]) {
		int y = ver[i];
		if(y == F) continue;
		dfs(y, x);
	}
}
int LCA(int x, int y) {
	if(d[x] > d[y]) swap(x, y);
	for(int i = 20 ; i >= 0; --i)
		if(d[f[y][i]] >= d[x]) y = f[y][i];
	if(y == x) return x;
	for(int i = 20 ; i>= 0; --i)
		if(f[x][i] != f[y][i]) x = f[x][i], y = f[y][i];
	return f[x][0];
}
void update(int x, int y, int id) {
	int ct = 0;
	while(x ^ y) {
		v[x][id] = 1;
		if(len[id] - ct > val[x]) p[x] = id, val[x] = len[id] - ct;
		x = f[x][0];
		++ct;
	}
}
int ask(int x, int y) {
	int t1 = 0, t2 = 0, ans = 0;
	while(x ^ y) {
		if(d[x] < d[y]) swap(x, y), swap(t1, t2);
		if(v[x][t2 ^ 1]) return ans;
		if(v[x][t1]) x = f[x][0];
		else {
			if(!p[x]) return -1;
			t1 = p[x];
			if(v[x][t2 ^ 1]) return ans;
			x = f[x][0];
			++ans;
		}
	}
	return ans;
}
int main() {
	setIO();
	n = read();
	for(int i = 2; i <= n; ++i) {
		int x = read();
		add(x, i);
		add(i, x);
	}
	dfs(1, 1);
	m = read();
	for(int i = 1 ; i <= m; ++i) {
		int x = read(), y = read();
		int z = LCA(x, y);
		len[i * 2] = d[x] - d[z];
		len[i * 2 + 1] = d[y] - d[z];
		update(x, z, i * 2);
		update(y, z, i * 2 + 1);
	}
	int Q = read();
	while(Q--) {
		int x = read(), y = read();
		writeln(ask(x, y));
	}
	return 0;
}
