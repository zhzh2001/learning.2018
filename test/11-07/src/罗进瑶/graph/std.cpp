#include<bits/stdc++.h>
typedef long long ll;
void setIO() {
	freopen("graph.in", "r", stdin);
	freopen("graph.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();
}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = - x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int N = 15;
int n, p, a[N];
struct Edge {
	int u, v, nxt;
}e[N * N];
int en, ans, head[N];
void add(int x, int y) {
	e[++en].u = x, e[en].v = y, e[en].nxt = head[x], head[x] = en;
}
int S = 0, res;
bool vis[N];
void dfs2(int x) {
	++res;
	vis[x] = 1;
	for(int i = head[x]; i; i = e[i].nxt)
		if(S & (1 << (i - 1))) {
			int y = e[i].v;
			if(vis[y] || a[y] == a[x]) continue;
			dfs2(y);
		}
	vis[x] = 0;
}
int solve() {
	int cnt = 0;
	int B = (1 << en);
	for(S = 0; S < B; ++S) {
		res = 0;
		for(int i = 1; i <= n; ++i) dfs2(i);
		if(res % 2 == p) ++cnt;
	}
	return cnt;
}
void dfs(int x) {
	if(x == n + 1) {
		ans += solve();
		return;
	}
	if(~a[x]) dfs(x + 1);
	else {
		a[x] = 0;
		dfs(x + 1);
		a[x] = 1;
		dfs(x + 1);
	}
}
int main() {
	n = read(), p = read();
	for(int i = 1; i <= n; ++i) a[i] = read();
	for(int i = 1; i <= n; ++i)
		for(int j = i + 1; j <= n; ++j)
			add(i ,j);
	dfs(1);
	writeln(ans);
	return 0;
}
