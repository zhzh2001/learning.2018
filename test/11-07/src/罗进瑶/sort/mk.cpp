#include<bits/stdc++.h>
typedef long long ll;
void setIO() {
	freopen("sort.in", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();
}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = - x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;

int n = 1e5, t = 1, c = 2e9; 
const int p = 1e9 + 7;
int main() {
	setIO();
	cout<<n<<" " << t<<" " << c<<endl;
	if(t == 4) cout<<(ll) rand() * rand() * rand() % p<<endl;
	return 0;
}
