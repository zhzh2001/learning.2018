#include<bits/stdc++.h>
typedef long long ll;
void setIO() {
	freopen("sort.in", "r", stdin);
	freopen("sort.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();
}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = - x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;

int n, t, c, num;
const int N = 1e5 + 100;
int a[N], b[N];
#define pii pair<int,int>
#define fi first
#define nd second
#define mk make_pair
queue<pii>q;

ll x;
int main() {
	setIO();
	cin>>n>>t>>c;
	if(t == 4) cin>>x;
	if(t == 1) {
		for(int i = n; i; --i)
			printf("%d ", i);
		return 0;
	}
	if(t == 2) {
		for(int i = 1; i <= n; ++i)
			printf("%d ", i);
		return 0;
	}
	if(t == 3) {
		int T = n;
		for(int i = 1; i <= n; ++i) b[i] = i;
		num = n;
		while(T) {
			int mid = (1 + T) >> 1;
			a[b[mid]] = num--;
			b[mid] = b[T];
			--T;
		}
		for(int i = 1; i <= n; ++i) printf("%d ", a[i]);
		return 0;
	}
	for(int i = 1; i <= n; ++i) b[i] = i;
	num = n;
	int T = n;
	while(T) {
		x = x * 48271 % 2147483647;
		int mid = x % T + 1;
		a[b[mid]] = num--;
		b[mid] = b[T];
		--T;
	}
	for(int i = 1; i <= n; ++i) printf("%d ", a[i]);
	return 0;
}
