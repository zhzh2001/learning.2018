#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
inline void write(ll x){
	if(x<0) putchar('-'),x=-x;
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x); puts("");
}
const int N=200005,K=20;
int n,m,q,ti,zs[N],c[N],ans[N],dep[N],in[N],out[N],sv[N],tv[N],sq[N],tq[N],f[N][K],g[N][K];
vector<int> e[N],v[N],p[N];
struct data{
	int x,id,y,v;
}qq[N<<1];
bool operator <(data a,data b){
	return a.x<b.x;
}
inline bool cmp(int x,int y){
	return in[sv[x]]<in[sv[y]];
}
inline int Min(int x,int y){
	return dep[x]<dep[y]?x:y;
}
void dfs(int p){
	in[p]=++ti;
	for(unsigned i=0;i<e[p].size();i++){
		dep[e[p][i]]=dep[p]+1; dfs(e[p][i]);
	}
	out[p]=ti;
}
void bao(int p){
	for(unsigned i=0;i<e[p].size();i++){
		bao(e[p][i]);
		g[p][0]=Min(g[p][0],g[e[p][i]][0]);
	}
	if(dep[g[p][0]]>=dep[p])g[p][0]=0;
}
bool check(int x,int y){
	for(int i=K-1;i>=0&&dep[x]>dep[y];i--)if(g[x][i])x=g[x][i];
	return dep[x]<=dep[y];
}
inline bool fa(int x,int y){
	return in[x]<=in[y]&&out[x]>=out[y];
}
int lca(int x,int y){
	if(fa(x,y))return x; else if(fa(y,x))return y;
	for(int i=K-1;i>=0;i--)if(f[x][i]&&!fa(f[x][i],y))x=f[x][i];
	return f[x][0];
}
int get(int x,int y){
	if(x==y)return 0;
	int ans=0;
	for(int i=K-1;i>=0;i--)if(g[x][i]&&dep[g[x][i]]>dep[y]){x=g[x][i]; ans+=1<<i;} 
	return ans+1;
}
int Get(int x,int y){
	for(int i=K-1;i>=0;i--)if(y>>i&1)x=g[x][i];
	return x;
}
#define lowbit(i) i&-i
inline void change(int pos){
	for(int i=pos;i<N;i+=lowbit(i))c[i]++;
}
inline int ask(int pos){
	int ans=0;
	for(int i=pos;i;i-=lowbit(i))ans+=c[i];
	return ans;
}
int main(){
	freopen("bus.in","r",stdin); freopen("bus.out","w",stdout);
	n=read(); dep[0]=1e9;
	for(int i=2;i<=n;i++){
		int s=f[i][0]=read();
		e[s].push_back(i);
	}
	dfs(1);
	m=read();
	for(int i=1;i<K;i++)for(int j=1;j<=n;j++)f[j][i]=f[f[j][i-1]][i-1];
	for(int i=1;i<=m;i++){
		sv[i]=read(); tv[i]=read(); 
		if(in[sv[i]]>in[tv[i]])swap(sv[i],tv[i]);
		int k=lca(sv[i],tv[i]); 
		if(sv[i]!=k&&tv[i]!=k){
			v[k].push_back(i);
		}
		g[sv[i]][0]=Min(g[sv[i]][0],k); g[tv[i]][0]=Min(g[tv[i]][0],k);
	}
	bao(1);
	for(int i=1;i<K;i++)for(int j=1;j<=n;j++)g[j][i]=g[g[j][i-1]][i-1]; 
	//cout<<get(4,0)<<endl; return 0;
	q=read();
	for(int i=1;i<=q;i++){
		sq[i]=read(); tq[i]=read(); int k=lca(sq[i],tq[i]);
		if(!check(sq[i],k)||!check(tq[i],k)){ans[i]=-1; continue;}
		if(in[sq[i]]>in[tq[i]])swap(sq[i],tq[i]);
		p[k].push_back(i);
	}
	for(int i=1;i<=n;i++){
		sort(v[i].begin(),v[i].end(),cmp);
		int tt=0;
		for(unsigned cnm=0;cnm<p[i].size();cnm++){
			int j=p[i][cnm];
			int xx=get(sq[j],i),yy=get(tq[j],i);
			ans[j]=xx+yy; 
			if(sq[j]==i||tq[j]==i)continue;
			int tx=Get(sq[j],xx-1),ty=Get(tq[j],yy-1);
			qq[++tt]=(data){out[tx],j,ty,1};
			qq[++tt]=(data){in[tx]-1,j,ty,-1};
		}
		sort(&qq[1],&qq[tt+1]);
		unsigned k=0;
		for(int j=1;j<=tt;j++){
			while(k<v[i].size()&&in[sv[v[i][k]]]<=qq[j].x){
				change(in[tv[v[i][k]]]); k++;
			}
			zs[qq[j].id]+=qq[j].v*(ask(out[qq[j].y])-ask(in[qq[j].y]-1));
		}
	}
	for(int i=1;i<=q;i++){
		if(zs[i])ans[i]--;
		writeln(ans[i]);
	}
}
/*
7
1 1 1 4 5 6
4
4 2
5 4
1 3
6 7
5
4 5
3 5
7 2
4 2
5 3

*/
