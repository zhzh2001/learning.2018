#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
inline void write(ll x){
	if(x<0) putchar('-'),x=-x;
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
const int N=100005;
ll seed;
int type,a[N],pos[N],dq;
ll rnd(int l,int r){
	if(type==1)return 1;
	else if(type==2)return r;
	else if(type==3)return (l+r)>>1;
	else {
		seed=seed*48271 % 2147483647;
		return seed%(r-l+1)+l;
	}
}

void solve(int l,int r){
	if(l>r)return;
	int t=rnd(l,r);
	//cout<<l<<" "<<r<<t<<endl;
	if(t<r){
		a[pos[t]]=dq--;
		swap(pos[t],pos[r]);
	}else{
		a[pos[r]]=dq--;
	}
	solve(l,r-1);
}
int main(){
	freopen("sort.in","r",stdin); freopen("sort.out","w",stdout);
	int n=dq=read(); type=read(); read();
	if(type==4)seed=read();
	for(int i=1;i<=n;i++)pos[i]=i;
	solve(1,n);
	for(int i=1;i<=n;i++){
		write(a[i]); putchar(' ');
	}
}
/*
1 3 4 2 5 6
*/
