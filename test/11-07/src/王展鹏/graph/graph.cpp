#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define gc getchar
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
inline void write(ll x){
	if(x<0) putchar('-'),x=-x;
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
const int mod=1000000007;
ll alb=(mod+1)/2,n,p;
inline void add(ll &a,ll b){
	a=(a+b)%mod;
}
ll f[2][2][2][2];
int main(){
	freopen("graph.in","r",stdin); freopen("graph.out","w",stdout);
	n=read(); p=read();
	f[0][0][0][0]=1;
	for(int i=0;i<n;i++){
		int x=read(),dq=i&1,dd=dq^1;
		memset(f[dd],0,sizeof(f[dd]));
		if(x!=1){
			for(int j=0;j<2;j++){
				for(int k=0;k<2;k++){
					for(int l=0;l<2;l++)if(f[dq][j][k][l]){
						if(k==0)add(f[dd][1][k][l^1],f[dq][j][k][l]*alb*2);
						else {add(f[dd][1][k][l^1],f[dq][j][k][l]*alb); add(f[dd][j][k][l],f[dq][j][k][l]*alb);}
					}
				}
			}
		}
		if(x!=0){
			for(int j=0;j<2;j++){
				for(int k=0;k<2;k++){
					for(int l=0;l<2;l++)if(f[dq][j][k][l]){
						if(j==0)add(f[dd][j][1][l^1],f[dq][j][k][l]*alb*2);
						else {add(f[dd][j][1][l^1],f[dq][j][k][l]*alb); add(f[dd][j][k][l],f[dq][j][k][l]*alb);}
					}
				}
			}
		}
		//for(int j=0;j<2;j++)for(int k=0;k<2;k++)for(int l=0;l<2;l++)if(f[dd][j][k][l])cout<<f[dd][j][k][l]<<" "<<i+1<<" "<<j<<" "<<k<<" "<<l<<endl;
		alb=alb*2%mod;
	}
	ll ans=0;
	for(int i=0;i<2;i++)for(int j=0;j<2;j++)ans=(ans+f[n&1][i][j][p])%mod;
	cout<<ans<<endl;
}
/*
dp[i][j][k][l]表示做到第i个点，是否有黑点为奇数，是否有白点是奇数，答案奇偶 
答案为%2=k 
如果当前是黑点，考虑左边任意一个白点
方案数是任意一个集合奇偶性取反 
事实上，我们考虑如果每个dp值都是偶数那当前必然是奇数
否则奇数和偶数方案数相等 
//上面都是瞎写的 
*/
