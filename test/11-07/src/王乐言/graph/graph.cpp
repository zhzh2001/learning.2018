#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <queue>
#include <vector>
#define ull unsigned long long
#define ll long long
#define file "graph"
using namespace std;
const int N=1000009;
const int mod=1e9+7;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,p,col[N];
int g[209][209];
int f[209],ans;
void work1(){
	if(n%2!=p){
		printf("0\n");
		return ;
	}
	int ans=0,tt=1;
	for(int i=1;i<n;i++){
		ans=ans+i;
		if(ans>=mod-1)ans-=mod-1;
	}
	for(int i=1;i<=ans;i++)
		tt=1ll*tt*2%mod;
	printf("%d\n",tt);
}
void DP(){
	memset(f,0,sizeof(f));
	for(int i=n;i>=1;i--){
		for(int j=i+1;j<=n;j++){
			if(!g[i][j]||col[i]==col[j])continue;
			f[i]=(f[i]+f[j])%2;
		}
		f[i]=(f[i]+1)%2;
	}
	int now=0;
	for(int i=1;i<=n;i++)now=(now+f[i])%2;
	if(now==p){ans=(ans+1)%mod;}
}
void dfs2(int d){
	if(d>n){DP();}
	for(int i=0;i<(1<<(n-d));i++){
		for(int j=1;j<=n;j++)g[d][j]=0;
		for(int j=0;(1<<j)<=i;j++)
			if(i&(1<<j))g[d][n-j]=1;
		dfs2(d+1);
	}
}
void dfs(int d){
	if(d>n){dfs2(1);return ;}
	if(col[d]!=-1)dfs(d+1);
	else{
		col[d]=1;
		dfs(d+1);
		col[d]=0;
		dfs(d+1);
	}
}
void work2(){
	dfs(1);
	printf("%d\n",ans);
}

int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int flag1=1,flag2=1;
	n=read();p=read();
	for(int i=1;i<=n;i++){
		col[i]=read();
		if(col[i]!=-1)flag2=0;
		if((i>=2&&col[i]!=col[i-1])||col[i]==-1)flag1=0;
	}
	if(flag1)work1();
	else if(n<=1000)work2();
	else cout<<"Give UP"<<endl;
	
	return 0;
}
/* 如何能组成一条黑白相间的路径
 * 每个点都是一条路径
 * 对于一个点，考虑加入其它点组成路径
 * 一个点只有和与它颜色不同的点连边才能累加贡献
 *
 * 研究好图的性质，如果后面节点已经组成好图了，那么当前点只有连偶数条有效边才能组成好图。
 * 如果后面节点没有组成好图，那么当前点只有连奇数条边才能组成好图
 * 可以边循环边统计后面所有图的方案数，那么后面有没有组成好图的方案数就清楚了。 
 * f[i][j]表示第i号节点为j颜色的时候，与后面节点组成的好图个数。 
 * 有色点连无色点怎么统计呢
 * 相同颜色，没有贡献，不同颜色，有贡献。  
 * 发现颜色不同的时候是完全不同的两种情况。 
 * 连或者不连的情况累加起来，然后所有后面的情况累乘起来 
 * 两边各算一次，统计起来
 * 无色点连无色点呢？
 * 发现四种情况是互相独立的
 *  
 */
 
 /* 暴力
  * 暴力连边，选色
  * 然后dp，f[i]表示以第i个点为起点的路径方案数。
  */ 
