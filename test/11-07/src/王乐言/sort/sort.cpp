#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <queue>
#include <vector>
#define ull unsigned long long
#define ll long long
#define file "sort"
using namespace std;
ull read(){
	char c;ull num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,type,cnt,a[100009];
int b[100009];
ull seed;
int rnd(int l,int r){
	seed=seed*48271%2147483647;
	return seed%(r-l+1)+l;
}
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=read();type=read();cnt=read();
	if(type==1||type==2){
		for(int i=1;i<=n;i++)
			printf("%d ",i);	
		printf("\n");
	}
	else if(type==3){
		for(int i=1;i<=n;i++)a[i]=i;
		for(int i=1;i<=n;i++)
			swap(a[i],a[(n+i)/2]);
		for(int i=1;i<=n;i++)
			b[a[i]]=i;
		for(int i=1;i<=n;i++)
			printf("%d ",b[i]);
		printf("\n");
	}else if(type==4){
		seed=read();
		for(int i=1;i<=n;i++)a[i]=i;
		for(int l=1;l<=n;l++)
			swap(a[l],a[rnd(l,n)]);
		for(int i=1;i<=n;i++)
			b[a[i]]=i;
		for(int i=1;i<=n;i++)
			printf("%d ",b[i]);
		printf("\n");
	}
	return 0;
}

