#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <queue>
#include <vector>
#define ull unsigned long long
#define ll long long
#define file "bus"
using namespace std;
const int N=200009;
struct ed{
	int u,v;
}e[N];
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,m,x,y,now,tmp,cnt,fa[N];
bool f[N];
int head[N],nxt[N*3],ver[N*3],tot=1;
void add(int x,int y){
	ver[++tot]=y;nxt[tot]=head[x];head[x]=tot;
	ver[++tot]=x;nxt[tot]=head[y];head[y]=tot;
}
bool cmp(ed a,ed b){
	if(a.u==b.u)return a.v<b.v;
	else return a.u<b.u;
}
void work1(){
	m=read();
	for(int i=1;i<=m;i++){
		e[i].u=read();
		e[i].v=read();
	}
	sort(e+1,e+1+n,cmp);
	int q=read();
	while(q--){
		x=read();y=read();
		now=x;tmp=0;cnt=0;
		int j=1;
		while(now<y){
			if(e[j].u>now&&tmp<now){
				cnt=-1;
				break;
			}
			if(e[j].u>now&&tmp>now){
				now=tmp;cnt++;
				continue;
			}
			tmp=max(tmp,e[j].v);
			j++;
		}
		printf("%d\n",cnt);
	}
}

void work2(){
	m=read();
	for(int i=1;i<=m;i++){
		x=read();y=read();
		f[x]=1;f[y]=1;
		add(x,y);
	}
	int q=read();
	while(q--){
		int flag=1;
		x=read();y=read();
		if(!f[x]||!f[y]){
			printf("-1\n");
			continue;
		}
		for(int i=head[x];i;i=nxt[i])
			if(ver[i]==y){
				flag=0;
				printf("1\n");
				break;
			}
		if(flag)printf("2\n");
	}
}
//void work3();
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=read();
	int flag1=1,flag2=1;
	for(int i=2;i<=n;i++){
		fa[i]=read();
		if(fa[i]!=1)flag1=0;
		if(fa[i]!=i-1)flag2=0;
	}
	if(flag2)work1();
	else if(flag1)work2();
	else cout<<"Give Up"<<endl;
	return 0;
}
/* 转化题意，给定一棵树，上面有m段颜色，有q个询问。  
 * 求每个询问的两个端点间路径的最小颜色覆盖数。 
 * 数据范围200000，对于每个询问我们最多用logn的时间回答
 * 发现每次只要贪心地取在当前x到y路径上深入最远的公交肯定是最优的
 * give up，考虑部分分。
 * 对于pi=i的，发现是一条链，我们只要在链上跑一下线段覆盖就可以了
 *  
 */ 
