#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int M=1e9+7;
struct node{
	int to,ne;
}e[25];
int a[6],n,p,cnt,ans,tot,h[6],i;
void dfs(int u){
	cnt++;
	for (int i=h[u],v;i;i=e[i].ne)
		if (a[u]!=a[v=e[i].to]) dfs(v);
}
void edge(int x,int y){
	if (y==n){
		edge(x+1,x+2);
		return;
	}
	if (x==n){
		cnt=0;
		for (int i=0;i<n;i++) dfs(i);
		if ((cnt&1)==p) ans++;
		return;
	}
	edge(x,y+1);
	e[++tot]=(node){y,h[x]};
	h[x]=tot;
	edge(x,y+1);
}
void point(int x){
	if (x==n){
		edge(0,1);
		return;
	}
	if (a[x]==-1){
		a[x]=0;point(x+1);
		a[x]=1;point(x+1);
	}else point(x+1);
}
int pw(int x,ll y){
	int z=1;
	for (;y;y>>=1,x=1ll*x*x%M)
		if (y&1) z=1ll*z*x%M;
	return z;
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	scanf("%d%d",&n,&p);
	if (n>6){
		if ((n&1)==p) printf("%d\n",pw(2,1ll*n*(n-1)/2));
		else puts("0");
		return 0;
	}
	for (i=0;i<n;i++) scanf("%d",&a[i]);
	point(0);
	printf("%d",ans);
}
