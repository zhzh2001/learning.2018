#include<bits/stdc++.h>
using namespace std;
const int N=5002,M=200002;
struct node{
	int to,ne;
}e[N<<1];
int dep[N],h[N],fa[N][14],lca,x,y,n,i,tot,m,q,tag;
struct kk{
	int x,y,id;
}a[M];
int vis[M],col[M],ans[M],cnt,l,r;
bool fl;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int rd(){
	int x=0,fl=1;char ch=gc();
	for (;ch<48||ch>57;ch=gc())if(ch=='-')fl=-1;
	for (;48<=ch&&ch<=57;ch=gc())x=(x<<3)+(x<<1)+(ch^48);
	return x*fl;
}
inline void wri(int a){if(a<0)a=-a,putchar('-');if(a>=10)wri(a/10);putchar(a%10|48);}
inline void wln(int a){wri(a);puts("");}
void add(int x,int y){
	e[++tot]=(node){y,h[x]};
	h[x]=tot;
}
void dfs(int u,int f){
	for (int i=1;i<14;i++) fa[u][i]=fa[fa[u][i-1]][i-1];
	dep[u]=dep[f]+1;
	for (int i=h[u],v;i;i=e[i].ne)
		if ((v=e[i].to)!=f) fa[v][0]=u,dfs(v,u);
}
int LCA(int x,int y){
	if (dep[x]<dep[y]) swap(x,y);
	for (int i=13;~i;i--)
		if (dep[fa[x][i]]>=dep[y]) x=fa[x][i];
	if (x==y) return x;
	for (int i=13;~i;i--)
		if (fa[x][i]!=fa[y][i]) x=fa[x][i],y=fa[y][i];
	return fa[x][0];
}
bool cmp(kk a,kk b){return a.x/m<b.x/m || a.x/m==b.x/m && a.y>b.y;}
inline void jian(int x){if (!(--vis[col[x]])) cnt--;}
inline void jia(int x){if (!(vis[col[x]]++)) cnt++;}
int main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=rd();
	if (n<=5000){
		for (i=2;i<=n;i++) x=rd(),add(i,x),add(x,i);
		dfs(1,0);
		m=rd();
		for (i=1;i<=m;i++){
			x=rd(),y=rd();
			lca=LCA(x,y);
			for (;x!=lca;x=fa[x][0]) col[x]=i;
			for (;y!=lca;y=fa[y][0]) col[y]=i;
		}
		for (q=rd();q--;){
			x=rd(),y=rd();tag++;
			lca=LCA(x,y);cnt=0;fl=1;
			for (;x!=lca && fl;x=fa[x][0]){
				if (!col[x]) fl=0;
				if (vis[col[x]]!=tag) cnt++,vis[col[x]]=tag;
			}
			for (;y!=lca && fl;y=fa[y][0]){
				if (!col[y]) fl=0;
				if (vis[col[y]]!=tag) cnt++,vis[col[y]]=tag;
			}
			wln(fl?cnt:-1);
		}
		return 0;
	}
	for (i=1;i<n;i++){
		x=rd();
		if (x!=1) fl=1;
	}
	if (fl){
	for (m=rd();m--;){
		tag++;
		x=rd(),y=rd();
		if (x>y) swap(x,y);
		for (i=x;i<y;i++) col[i]=tag;
	}
	for (i=1;i<=q;i++) a[i].x=rd(),a[i].y=rd(),a[i].id=i;
	m=sqrt(n);
	sort(a+1,a+n+1,cmp);
	l=1;r=1;
	for (i=1;i<=n;i++){
		while (l<a[i].x) jian(l++);
		while (l>a[i].x) jia(--l);
		while (r<a[i].y) jia(r++);
		while (r>a[i].y) jian(--r);
		ans[a[i].id]=(vis[0]?cnt:-1);
	}
	for (i=1;i<=q;i++) wln(ans[i]);
	}else{
	for (m=rd();m--;){
		x=rd(),y=rd();
		col[x]=col[y]=++tag;
	}
	for (q=rd();q--;)
		x=rd(),y=rd();
		if (x==1) wln(col[y]?1:-1);
		else if (y==1) wln(col[x]?1:-1);
		else wln(col[x] && col[y]?(col[x]!=col[y]?2:1):-1);
	}
}
