#include<bits/stdc++.h>
using namespace std;
const int N=100002;
int n,opt,cnt,sd,i,a[N],M,tot,b[N];
inline void wri(int a){if(a<0)a=-a,putchar('-');if(a>=10)wri(a/10);putchar(a%10|48);}
inline void wln(int a){wri(a);puts("");}
int rnd(int l,int r){
	if (opt==3) return l+r>>1;
	sd=1ll*sd*48271%M;
	return sd%(r-l+1)+l;
}
void srt(int l,int r){
	int i=l,j=r,x=a[rnd(l,r)];
	do{
		while (a[i]<x) i++,tot++;
		while (x<a[j]) j--,tot++;
		if (i<=j) swap(a[i],a[j]),i++,j--,tot+=2;
	}while (i<=j);
	if (l<j) srt(l,j);
	if (i<r) srt(i,r);
}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	scanf("%d%d%d",&n,&opt,&cnt);
	if (opt<3){
		for (i=1;i<n;i++) wri(i),putchar(' ');
		wln(n);
		return 0;
	}
	M=(1ll<<32)-1;
	srand(time(0));
	for (i=1;i<=n;i++) a[i]=i;
	while (1){
		tot=0;
		random_shuffle(a+1,a+n+1);
		memcpy(b,a,sizeof(a));
		srt(1,n);
		if (tot>=cnt){
			for (i=1;i<n;i++) wri(b[i]),putchar(' ');
			wln(b[n]);
			return 0;
		}
	}
}
