#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const LL md=1000000007;
LL n,p;
LL pow(LL x,LL n){
	if(n==1)return x;
	LL ans=pow(x,n/2);
	ans=ans*ans%md;
	if(n&1)ans=ans*x%md;
	return ans;
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();
	p=read();
	if(n%2!=p%2){
		puts("0");
	}else{
		printf("%lld\n",pow(2,n*(n-1)/2));
	}
	return 0;
}
