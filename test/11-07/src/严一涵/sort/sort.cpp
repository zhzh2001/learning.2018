#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,type,cnt,a[100003],b[100003];
LL seed;
int F(int l,int r){
	if(type==1){
		return l;
	}else if(type==2){
		return r;
	}else if(type==3){
		return (l+r)/2;
	}else if(type==4){
		seed*=48271;
		seed%=2147483647;
		return seed%(r-l+1)+l;
	}
}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();
	type=read();
	cnt=read();
	if(type==4)seed=read();
	for(int i=n;i>1;i--){
		b[i]=F(1,i);
	}
	for(int i=1;i<=n;i++){
		a[i]=i;
	}
	for(int i=2;i<=n;i++){
		swap(a[b[i]],a[i]);
	}
	for(int i=1;i<=n;i++){
		printf("%d ",a[i]);
	}
	return 0;
}
