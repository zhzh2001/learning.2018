#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,m,q,f[23][200003],x,y,z;
int main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	for(int i=2;i<=n;i++){
		read();
		f[0][i]=i;
	}
	m=read();
	for(int i=1;i<=m;i++){
		x=read();
		y=read();
		if(x>y)swap(x,y);
		f[0][x]=max(f[0][x],y);
	}
	for(int i=1;i<=n;i++){
		f[0][i]=max(f[0][i],f[0][i-1]);
	}
	for(int i=1;i<=20;i++){
		for(int j=1;j<=n;j++){
			f[i][j]=f[i-1][f[i-1][j]];
		}
	}
	q=read();
	for(int i=1;i<=q;i++){
		x=read();
		y=read();
		z=1;
		if(x>y)swap(x,y);
		if(f[20][x]<y){
			puts("-1");
			continue;
		}
		if(x==y){
			puts("0");
			continue;
		}
		for(int j=20;j>=0;j--)if(f[j][x]<y){
			z+=1<<j;
			x=f[j][x];
		}
		printf("%d\n",z);
	}
	return 0;
}
