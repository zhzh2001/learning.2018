#include <cstdio>
#include <cstring>
#include <map>
#include <algorithm>
using namespace std;
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
void write(int x) {if (x<0) x=-x,putchar('-');if (x>=10) write(x/10);putchar(x%10|'0');}
void wln(int x) {write(x);puts("");}
void wsp(int x) {write(x);putchar(' ');}
const int N=2e5+7;
struct edge {
	int t,nxt;
}e[N<<1];
int n,m,Q,cnt,T;
int E[305][305],fa[N],st[305],head[N],dep[N],dfn[N];
void add(int u,int t) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;
}
void solve1() {
	m=read();
	memset(E,60,sizeof E);
	int inf=E[0][0];
	for (int i=1;i<=n;i++) E[i][i]=0;
	for (int i=1,u,v;i<=m;i++) {
		u=read(),v=read();
		int tp=0;
		while (u!=v) if (dep[u]>dep[v]) st[++tp]=u,u=fa[u];
		else st[++tp]=v,v=fa[v];
		st[++tp]=u;
		for (int j=1;j<=tp;j++)
			for (int k=j+1;k<=tp;k++) E[st[j]][st[k]]=E[st[k]][st[j]]=1;
	}
	for (int k=1;k<=n;k++)
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				E[i][j]=min(E[i][j],E[i][k]+E[k][j]);
	Q=read();
	while (Q--) {
		int u=read(),v=read();
		if (E[u][v]==inf) puts("-1");
		else wln(E[u][v]);
	}
}
void dfs(int u,int F) {
	fa[u]=F;dep[u]=dep[F]+1;dfn[u]=++T;
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==F) continue;
		dfs(t,u);
	}
}
int tr[N<<2],flg[N<<2];
#define ls (ts<<1)
#define rs (ts<<1|1)
void pushup(int ts) {tr[ts]=max(tr[ls],tr[rs]);}
void pushdown(int ts) {
	int v=flg[ts];flg[ts]=0;
	tr[ls]=max(tr[ls],v);tr[rs]=max(tr[rs],v);
	flg[ls]=max(flg[ls],v);flg[rs]=max(flg[rs],v);
}
void update(int ts,int l,int r,int L,int R,int mr) {
	if (tr[ts]>=mr) return;
	if (l==L && r==R) {
		tr[ts]=mr;flg[ts]=mr;
		return;
	}
	int mid=(l+r)>>1;
	pushdown(ts);
	if (R<=mid) update(ls,l,mid,L,R,mr);
	else if (L>mid) update(rs,mid+1,r,L,R,mr);
	else update(ls,l,mid,L,mid,mr),update(rs,mid+1,r,mid+1,R,mr);
	pushup(ts);
}
int query(int ts,int l,int r,int L,int R) {
	if (l==L && r==R) return tr[ts];
	int mid=(l+r)>>1;
	pushdown(ts);
	if (R<=mid) return query(ls,l,mid,L,R);
	else if (L>mid) return query(rs,mid+1,r,L,R);
	else return max(query(ls,l,mid,L,mid),query(rs,mid+1,r,mid+1,R));
}
void solve2() {
	m=read();
	for (int i=1,u,v;i<=m;i++) {
		u=read();v=read();
		if (dfn[u]>dfn[v]) swap(u,v);
		update(1,1,n,dfn[u],dfn[v],dfn[v]);
	}
	Q=read();
	for (int i=1,u,v,ans,l,r;i<=Q;i++) {
		u=read();v=read();
		if (dfn[u]>dfn[v]) swap(u,v);
		ans=0,l=dfn[u],r=dfn[u];
		while (r<dfn[v]) {
			ans++;
			int nr=query(1,1,n,l,r);
			if (nr<=r) r=dfn[v],ans=-1;
			else r=nr;
		}
		wln(ans);
	}
}
map<int,int>mp[N];
int vis[N];
void solve3() {
	m=read();
	for (int i=1,u,v;i<=m;i++) {
		u=read();v=read();
		vis[u]=1;vis[v]=1;
		mp[u][v]=1;mp[v][u]=1;
	}
	Q=read();
	for (int i=1,u,v;i<=Q;i++) {
		u=read();v=read();
		if (u>v) swap(u,v);
		if (u==1) {
			if (vis[v]) puts("1");
			else puts("-1");
		}
		else {
			if (vis[u] && vis[v]) {
				if (mp[u].count(v)) puts("1");
				else puts("-1");
			}
			else puts("-1");
		}
	}
}
int main() {
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	scanf("%d",&n);
	int pd1=0,pd2=0;
	for (int i=2;i<=n;i++) {
		fa[i]=read(),add(fa[i],i);
		if (fa[i]!=1) pd1=1;
		if (fa[i]!=i-1) pd2=1;
	}
	dfs(1,0);
	if (n<=300) solve1();
	else if (!pd2) solve2();
	else if (!pd1) solve3();
}
