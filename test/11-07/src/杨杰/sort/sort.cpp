#include <cstdio>
using namespace std;
const int P=2147483647;
const int N=1e5+7;
int n,type,cnt,seed,now;
int x[N],la[N];
int F(int l,int r) {
	if (type==1) return l;
	else if (type==2) return r;
	else if (type==3) return (l+r)>>1;
	else {
		seed=48271ll*seed%P;
		return seed%(r-l+1)+l;
	}
}
int find(int x) {return x==la[x]?x:la[x]=find(la[x]);}
void ysort(int l,int r) {
	int mid=F(l,r);
	mid=find(mid);
	x[mid]=++now;
	la[mid]=l;
	if (l==r) return;
	ysort(l+1,r);
}
int main() {
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	scanf("%d%d%d",&n,&type,&cnt);
	if (type==4) scanf("%d",&seed);
	for (int i=1;i<=n;i++) la[i]=i;
	ysort(1,n);
	for (int i=1;i<=n;i++) printf("%d ",x[i]);
}
