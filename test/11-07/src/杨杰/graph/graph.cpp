#include <cstdio>
using namespace std;
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
void write(int x) {if (x<0) x=-x,putchar('-');if (x>=10) write(x/10);putchar(x%10|'0');}
void wln(int x) {write(x);puts("");}
void wsp(int x) {write(x);putchar(' ');}
const int N=1e6+7,P=1e9+7;
int n,p,ans;
int col[N],f[N][2][2],g[N][2][2];
void Ad(int &x,int y) {x+=y;if (x>=P) x-=P;}
int main() {
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();p=read();
	for (int i=1;i<=n;i++) col[i]=read();
	for (int i=1;i<=n;i++) {
		if (col[i]!=0) f[i][1][1]=1;
		if (col[i]!=1) f[i][0][1]=1;
		int j=i-1;
		int a0=f[j][1][0],a1=f[j][1][1],b0=f[i][0][0],b1=f[i][0][1];
			f[i][0][0]=(1ll*a0*b0%P+1ll*a1*b1%P)%P;
			f[i][0][1]=(1ll*a0*b1%P+1ll*a1*b0%P)%P;
		a0=f[j][0][0],a1=f[j][0][1],b0=f[i][1][0],b1=f[i][1][1];
			f[i][1][0]=(1ll*a0*b0%P+1ll*a1*b1%P)%P;
			f[i][1][1]=(1ll*a0*b1%P+1ll*a1*b0%P)%P;
			Ad(f[i][1][0],f[j][1][1]);
			Ad(f[i][1][1],f[j][1][0]);
			Ad(f[i][0][0],f[j][0][1]);
			Ad(f[i][0][1],f[j][0][0]);
	}
	printf("%d\n",(f[n][0][p]+f[n][1][p])%P);
}
