#include<cstdio>
#include<cctype>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<iostream>
#include<map>
#include<set>
#include<vector>
#include<queue>
#define LL long long 
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define dep(i,r,l) for(int i=r;i>=l;i--)
#define fy 19750502019771008
#define N 100010
using namespace std;
int num[N],ans[N];
int n,m,i,j,k,l,r;
int boo,cnt;
LL s;
int read()
{
	int x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int f(int boo,int l,int r)
{
	if(boo==1) return l;
	if(boo==2) return r;
	if(boo==3) return (l+r)/2;
	s=(s*48271*1ll)%2147483647;
	return s%(r-l+1)+l;
}
void swap(int &a,int &b){int t=a; a=b; b=t;}
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(); boo=read(); cnt=read(); 
	if(boo==4) s=read();
	rep(i,1,n) num[i]=i;
	rep(i,1,n)
	{
		k=f(boo,i,n);
		ans[num[k]]=i;
		swap(num[i],num[k]);
	}
	rep(i,1,n) printf("%d ",ans[i]);
	return 0;
}
