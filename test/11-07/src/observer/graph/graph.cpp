#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("graph.in");
ofstream fout("graph.out");
const int N=1000005,MOD=1e9+7;
int n,p,a[N],ans,f[10];
bool mat[10][10];
void dfs2(int u,int v)
{
	if(u==n)
	{
		int sum=0;
		for(int i=1;i<=n;i++)
		{
			f[i]=1;
			for(int j=1;j<i;j++)
				if(mat[j][i]&&(a[j]^a[i]))
					f[i]+=f[j];
			sum+=f[i];
		}
		if((sum&1)==p)
			ans++;
	}
	else
	{
		mat[u][v]=true;
		if(v<n)
			dfs2(u,v+1);
		else
			dfs2(u+1,u+2);
		mat[u][v]=false;
		if(v<n)
			dfs2(u,v+1);
		else
			dfs2(u+1,u+2);
	}
}
void dfs(int k)
{
	if(k==n+1)
		dfs2(1,2);
	else if(a[k]==-1)
	{
		a[k]=0;
		dfs(k+1);
		a[k]=1;
		dfs(k+1);
		a[k]=-1;
	}
	else
		dfs(k+1);
}
long long qpow(long long a,long long b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}while(b/=2);
	return ans;
}
int main()
{
	fin>>n>>p;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	if(count(a+1,a+n+1,-1)==n)
	{
		int bas=qpow(2,1ll*(1+n)*n/2-1);
		int del=qpow(2,1ll*(n-1)*(n-2)/2+1);
		if((n&1)==p)
			fout<<(bas+del)%MOD<<endl;
		else
			fout<<(bas-del+MOD)%MOD<<endl;
		return 0;
	}
	if(count(a+1,a+n+1,a[1])==n)
	{
		if((n&1)==p)
			fout<<qpow(2,1ll*n*(n-1)/2)<<endl;
		else
			fout<<0<<endl;
		return 0;
	}
	dfs(1);
	fout<<ans<<endl;
	return 0;
}
