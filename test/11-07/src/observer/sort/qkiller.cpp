#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("qkiller.in");
ofstream fout("qkiller.out");
const int N = 15, A = 48271, MOD = 2147483647;
int n, type, a[N], b[N], cnt;
long long tmp, seed;
void sort(int l, int r)
{
	int i = l, j = r, x;
	switch (type)
	{
	case 1:
		x = a[l];
		break;
	case 2:
		x = a[r];
		break;
	case 3:
		x = a[(l + r) / 2];
		break;
	case 4:
		seed = seed * A % MOD;
		x = a[seed % (r - l + 1) + l];
		break;
	}
	do
	{
		for (; a[i] < x; i++)
			++cnt;
		for (; x < a[j]; j--)
			++cnt;
		if (i <= j)
		{
			swap(a[i++], a[j--]);
			cnt += 2;
		}
	} while (i <= j);
	if (l < j)
		sort(l, j);
	if (i < r)
		sort(i, r);
}
int main()
{
	fin >> n >> type;
	if (n > 10)
	{
		for (int i = 1; i <= n; i++)
			fout << i << ' ';
		fout << endl;
		return 0;
	}
	if (type == 4)
		fin >> tmp;
	for (int i = 1; i <= n; i++)
		b[i] = i;
	do
	{
		cnt = 0;
		copy(b + 1, b + n + 1, a + 1);
		seed = tmp;
		sort(1, n);
		if (cnt > 45)
		{
			for (int i = 1; i <= n; i++)
				fout << b[i] << ' ';
			fout << endl;
			break;
		}
	} while (next_permutation(b + 1, b + n + 1));
	return 0;
}
