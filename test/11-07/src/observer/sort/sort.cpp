#include<bits/stdc++.h>
#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
const ll inf = 1e14;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define rank Rankkk
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
#define qsort QSORT
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void wri(ll x){write(x),pc(' ');}
void writeln(ll x){write(x),pc('\n');}
const int maxn = 1e5+233;
ll X;
int n,a[maxn],type,cnt;
inline int random(int l,int r){
	X=X*48271%2147483647ll;
	return X%(r-l+1)+l;
}
inline int F(int l,int r){
	if(type==1) return l;
	if(type==2) return r;
	if(type==3) return (l+r)/2;
	return random(l,r);
}
int ans=0,tot=0;
vector<pair<int,int> > res;
void qsort(int l,int r){
	if(ans > cnt){
		Rep(i,l,r) a[i]=++tot;
		return ;
	}
	int i=l,j=r,x=F(l,r);
	a[x]=++tot;
	for(;;){
		//while(a[i]<a[x]) i++,ans++;
		//false��a[i] > a[x]
		while(x!=j) j--,ans++;
		if(i<=j){
			swap(a[i],a[j]);
			if(j==x) x=i; else
			if(i==x) x=j;
			res.push_back({i,j});
			++i,--j;
			ans+=2;
		}
		if(i>j) break;
	}
	if(l<j) qsort(l,j);if(i<r) qsort(i,r);
}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	tot=0;
	n = read(),type = read(),cnt=read();
	if(type==4) X=read();
	qsort(1,n);
	Dep(i,res.size()-1,0)
		swap(a[res[i].fi],a[res[i].se]);
	Rep(i,1,n) printf("%d ",a[i]?a[i]:n);
	return 0;
}

