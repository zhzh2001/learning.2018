#include<fstream>
#include<random>
#include<windows.h>
#include<algorithm>
#include<iostream>
using namespace std;
ofstream fout("bus.in");
const int n=2e5,m=2e5,q=2e5;
pair<int,int> bus[m+5];
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<endl;
	for(int i=1;i<n;i++)
		fout<<i<<' ';
	fout<<endl;
	fout<<m<<endl;
	uniform_int_distribution<> dn(1,n),dbus(1,2),dbus2(3,9),coin(0,1),dlow(0,23333),dq(1,n);
	int cnt=0,miss=0;
	for(int i=1,j;i<=n-2;i=j)
	{
		j=i+dbus(gen);
		if(dlow(gen))
			bus[++cnt]=make_pair(i,j);
		else
			miss++;
	}
	bus[++cnt]=make_pair(n-2,n);
	cerr<<miss<<' '<<cnt<<endl;
	while(cnt<m)
	{
		int u=dn(gen),v=u;
		if(coin(gen))
			v+=dbus(gen);
		else
			v+=dbus2(gen);
		if(v<=n)
			bus[++cnt]=make_pair(u,v);
	}
	shuffle(bus+1,bus+cnt+1,gen);
	for(int i=1;i<=m;i++)
	{
		if(coin(gen))
			swap(bus[i].first,bus[i].second);
		fout<<bus[i].first<<' '<<bus[i].second<<endl;
	}
	fout<<q<<endl;
	for(int i=1;i<=q;)
	{
		int u=dn(gen),v=u+dq(gen);
		if(v<=n)
		{
			if(coin(gen))
				swap(u,v);
			fout<<u<<' '<<v<<endl;
			i++;
		}
	}
	return 0;
}
