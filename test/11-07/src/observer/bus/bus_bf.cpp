#include <fstream>
#include <algorithm>
#include <vector>
#include <map>
#include <queue>
using namespace std;
ifstream fin("bus.in");
ofstream fout("bus.out");
const int N = 200005, L = 20;
vector<int> mat[N], vertex[N], mate[N];
int dep[N], f[N][L], vis[N], dest[N];
pair<int, int> bus[N];
void dfs(int u)
{
	for (int v : mat[u])
	{
		dep[v] = dep[u] + 1;
		f[v][0] = u;
		dfs(v);
	}
}
int lca(int u, int v)
{
	if (dep[u] < dep[v])
		swap(u, v);
	int delta = dep[u] - dep[v];
	for (int i = 0; delta; i++, delta /= 2)
		if (delta & 1)
			u = f[u][i];
	if (u == v)
		return u;
	for (int i = L - 1; i >= 0; i--)
		if (f[u][i] != f[v][i])
		{
			u = f[u][i];
			v = f[v][i];
		}
	return f[u][0];
}
int main()
{
	int n;
	fin >> n;
	for (int i = 2; i <= n; i++)
	{
		int p;
		fin >> p;
		mat[p].push_back(i);
	}
	dfs(1);
	for (int j = 1; j < L; j++)
		for (int i = 1; i <= n; i++)
			f[i][j] = f[f[i][j - 1]][j - 1];
	int m;
	fin >> m;
	for (int i = 1; i <= m; i++)
		fin >> bus[i].first >> bus[i].second;
	for (int i = 1; i <= m; i++)
	{
		int a = lca(bus[i].first, bus[i].second);
		vertex[a].push_back(i);
		for (int j = bus[i].first; j != a; j = f[j][0])
			vertex[j].push_back(i);
		for (int j = bus[i].second; j != a; j = f[j][0])
			vertex[j].push_back(i);
	}
	for (int i = 1; i <= n; i++)
		for (int j = 0; j < vertex[i].size(); j++)
			for (int k = 0; k < vertex[i].size(); k++)
				mate[vertex[i][j]].push_back(vertex[i][k]);
	int q;
	fin >> q;
	for (int i = 1; i <= q; i++)
	{
		int u, v;
		fin >> u >> v;
		queue<pair<int, int>> Q;
		for (int bus : vertex[u])
		{
			Q.push(make_pair(bus, 1));
			vis[bus] = i;
		}
		for (int bus : vertex[v])
			dest[bus] = i;
		bool found = false;
		while (!Q.empty())
		{
			pair<int, int> u = Q.front();
			Q.pop();
			if (dest[u.first] == i)
			{
				fout << u.second << '\n';
				found = true;
				break;
			}
			for (int v : mate[u.first])
				if (vis[v] < i)
				{
					Q.push(make_pair(v, u.second + 1));
					vis[v] = i;
				}
		}
		if (!found)
			fout << "-1\n";
	}
	return 0;
}