#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("bus.in");
ofstream fout("bus.out");
const int N=200005;
int rmost[N];
int main()
{
	int n;
	fin>>n;
	for(int i=2;i<=n;i++)
	{
		int u;
		fin>>u;
	}
	int m;
	fin>>m;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		if(u>v)
			swap(u,v);
		rmost[u]=max(rmost[u],v);
	}
	for(int i=1;i<=n;i++)
		if(rmost[i-1]>=i)
			rmost[i]=max(rmost[i],rmost[i-1]);
	int q;
	fin>>q;
	while(q--)
	{
		int u,v;
		fin>>u>>v;
		if(u>v)
			swap(u,v);
		int ans=0;
		for(;u<v&&rmost[u]!=u;u=rmost[u])
			ans++;
		if(u<v)
			fout<<-1<<endl;
		else
			fout<<ans<<endl;
	}
	return 0;
}
