#include<fstream>
#include<random>
#include<windows.h>
#include<algorithm>
#include<iostream>
using namespace std;
ofstream fout("bus.in");
const int n=170000,nn=2e5,m=2e5,q=2e5;
pair<int,int> bus[m+5];
int pred[m];
int main()
{
	minstd_rand gen(GetTickCount());
	uniform_int_distribution<> dn(1,n),dbus(1,2),dbus2(3,9),coin(0,1),dlow(0,15000),dlow2(0,233),dq(1,n);
	fout<<nn<<endl;
	int cc=0;
	pred[++cc]=dn(gen);
	for(int i=1;i<n;i++)
		fout<<i<<' ';
	for(int i=n+1;i<=nn;i++)
		if(dlow2(gen))
		{
			uniform_int_distribution<> df(1,cc);
			fout<<pred[df(gen)]<<' ';
			pred[++cc]=i;
		}
		else
		{
			uniform_int_distribution<> dp(1,i-1);
			fout<<dp(gen)<<' ';
		}
	fout<<endl;
	fout<<m<<endl;
	int cnt=0,miss=0;
	for(int i=1,j;i<=n-2;i=j)
	{
		j=i+dbus(gen);
		if(dlow(gen))
			bus[++cnt]=make_pair(i,j);
		else
			miss++;
	}
	bus[++cnt]=make_pair(n-2,n);
	cerr<<miss<<' '<<cnt<<endl;
	while(cnt<m)
	{
		int u=dn(gen),v=u;
		if(coin(gen))
			v+=dbus(gen);
		else
			v+=dbus2(gen);
		if(v<=n)
			bus[++cnt]=make_pair(u,v);
	}
	shuffle(bus+1,bus+m+1,gen);
	for(int i=1;i<=m;i++)
	{
		if(coin(gen))
			swap(bus[i].first,bus[i].second);
		fout<<bus[i].first<<' '<<bus[i].second<<endl;
	}
	fout<<q<<endl;
	for(int i=1;i<=q;)
	{
		int u=dn(gen),v=u+dq(gen);
		if(dbus2(gen)==4)
			v+=dlow(gen);
		if(v<=n)
		{
			if(coin(gen))
				swap(u,v);
			fout<<u<<' '<<v<<endl;
			i++;
		}
	}
	return 0;
}
