#include <cstdio>
#include <algorithm>
using namespace std;
FILE *fres;
void Return(double score, const char *msg = nullptr)
{
	fprintf(fres, "%.3lf\n", score);
	if (msg)
		fprintf(fres, "%s\n", msg);
	exit(0);
}
int main(int argc, char *argv[])
{
	if (argc < 4)
		return 1;
	FILE *fans = fopen(argv[2], "r"), *fout = fopen(argv[3], "r");
	fres = argc > 4 ? fopen(argv[4], "w") : stdout;
	bool wrong = false;
	int x;
	while (~fscanf(fans, "%d", &x))
	{
		int y;
		fscanf(fout, "%d", &y);
		if ((x == -1 && y != -1) || (x != -1 && y == -1))
			Return(.0, "Wrong Answer");
		int error = abs(x - y);
		if (error > 1)
			Return(.0, "Wrong Answer");
		else if (error == 1)
			wrong = true;
	}
	if (wrong)
		Return(.4, "Partially Correct");
	Return(1., "Accepted");
	return 0;
}