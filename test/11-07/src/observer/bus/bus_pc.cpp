#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;
ifstream fin("bus.in");
ofstream fout("bus.out");
const int N = 200005, L = 20;
vector<int> mat[N];
int n, dep[N], f[N][L], low[N][L], into[N], outo[N], tim, ans[N], cnt[N];
pair<int, int> points[N];
struct query
{
	int x, y, id;
	bool operator<(const query &rhs) const
	{
		return x < rhs.x;
	}
} queries[N * 4];
void dfs(int u)
{
	into[u] = ++tim;
	for (int v : mat[u])
	{
		dep[v] = dep[u] + 1;
		f[v][0] = u;
		dfs(v);
	}
	outo[u] = tim;
}
int lca(int u, int v)
{
	if (dep[u] < dep[v])
		swap(u, v);
	int delta = dep[u] - dep[v];
	for (int i = 0; delta; i++, delta /= 2)
		if (delta & 1)
			u = f[u][i];
	if (u == v)
		return u;
	for (int i = L - 1; i >= 0; i--)
		if (f[u][i] != f[v][i])
		{
			u = f[u][i];
			v = f[v][i];
		}
	return f[u][0];
}
void dfs2(int u)
{
	for (int v : mat[u])
	{
		dfs2(v);
		if (low[v][0] && dep[low[v][0]] <= dep[u] && (!low[u][0] || dep[low[v][0]] < dep[low[u][0]]))
			low[u][0] = low[v][0];
	}
}
struct BIT
{
	int tree[N];
	void modify(int x, int val)
	{
		for (; x <= n; x += x & -x)
			tree[x] += val;
	}
	int query(int x)
	{
		int ans = 0;
		for (; x; x -= x & -x)
			ans += tree[x];
		return ans;
	}
} T;
int main()
{
	fin >> n;
	for (int i = 2; i <= n; i++)
	{
		int p;
		fin >> p;
		mat[p].push_back(i);
	}
	dfs(1);
	for (int j = 1; j < L; j++)
		for (int i = 1; i <= n; i++)
			f[i][j] = f[f[i][j - 1]][j - 1];
	int m;
	fin >> m;
	for (int i = 1; i <= m; i++)
	{
		int u, v;
		fin >> u >> v;
		int a = lca(u, v);
		if (!low[u][0] || dep[a] < dep[low[u][0]])
			low[u][0] = a;
		if (!low[v][0] || dep[a] < dep[low[v][0]])
			low[v][0] = a;
		if (into[u] > into[v])
			swap(u, v);
		points[i] = make_pair(into[u], into[v]);
	}
	dfs2(1);
	for (int j = 1; j < L; j++)
		for (int i = 1; i <= n; i++)
			low[i][j] = low[low[i][j - 1]][j - 1];
	int q;
	fin >> q;
	int qn = 0;
	for (int i = 1; i <= q; i++)
	{
		int u, v;
		fin >> u >> v;
		int a = lca(u, v);
		for (int j = L - 1; j >= 0; j--)
			if (dep[low[u][j]] > dep[a])
			{
				u = low[u][j];
				ans[i] += 1 << j;
			}
		for (int j = L - 1; j >= 0; j--)
			if (dep[low[v][j]] > dep[a])
			{
				v = low[v][j];
				ans[i] += 1 << j;
			}
		if (!low[u][0] || dep[low[u][0]] > dep[a] || !low[v][0] || dep[low[v][0]] > dep[a])
			ans[i] = -1;
		else if (u == a || v == a)
			cnt[i] = 1;
		else
		{
			if (into[u] > into[v])
				swap(u, v);
			queries[++qn] = {outo[u], outo[v], i};
			queries[++qn] = {into[u] - 1, outo[v], -i};
			queries[++qn] = {outo[u], into[v] - 1, -i};
			queries[++qn] = {into[u] - 1, into[v] - 1, i};
		}
	}
	points[++m] = make_pair(n + 1, n + 1);
	sort(points + 1, points + m + 1);
	sort(queries + 1, queries + qn + 1);
	for (int i = 1, j = 1, k = 1; i <= m; i = j)
	{
		for (; k <= qn && queries[k].x < points[i].first; k++)
			if (queries[k].id > 0)
				cnt[queries[k].id] += T.query(queries[k].y);
			else
				cnt[-queries[k].id] -= T.query(queries[k].y);
		for (; j <= m && points[j].first == points[i].first; j++)
			T.modify(points[j].second, 1);
	}
	for (int i = 1; i <= q; i++)
	{
		if (~ans[i])
		{
//			if (!cnt[i])
//				ans[i]++;
			ans[i]++;
		}
		fout << ans[i] << '\n';
	}
	return 0;
}
