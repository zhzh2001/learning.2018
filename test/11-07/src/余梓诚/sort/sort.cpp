#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
void judge(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
}
const int N=110000;
int n,tp,cnt,seed;
int a[N],pos[N];
int rnd(){
	return seed=1ll*seed*48271%2147483647;
}
void work(){
	int l=1,r=n;
	while (l!=r){
		int id;
		if (tp==1) id=l;
		if (tp==2) id=r;
		if (tp==3) id=l+r>>1;
		if (tp==4) id=rnd()%(r-l+1)+l;
		int x=id,y=r;
		swap(a[pos[x]],a[pos[y]]);
		swap(pos[x],pos[y]);
		r--;
	}
}
int main(){
	judge();
	scanf("%d%d%d",&n,&tp,&cnt);
	if (tp==4) scanf("%d",&seed);
	for (int i=1;i<=n;i++) a[i]=i,pos[i]=i;
	work();
	for (int i=1;i<=n;i++) printf("%d ",a[i]);
}
