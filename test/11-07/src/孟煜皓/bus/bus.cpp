#include <cstdio>
#include <cctype>
#include <algorithm>
#include <cstring>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) ch == '-' ? f ^= 1 : 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 200005
int n, m, q;
int fa[N], dep[N], sz[N], son[N], top[N], idx, dfn[N];
int edge, to[N], pr[N], hd[N];
struct bus{
	int x, y, z;
	bool operator < (const bus &res) const {
		return z < res.z;
	}
}a[N << 1];
bool cmp(bus a, bus b){
	return dep[a.z] < dep[b.z];
}
void addedge(int u, int v){
	to[++edge] = v, pr[edge] = hd[u], hd[u] = edge;
}
void dfs(int u){
	sz[u] = 1;
	for (register int i = hd[u], v; i; i = pr[i])
		dep[v = to[i]] = dep[u] + 1, dfs(v), sz[u] += sz[v], 
		!son[u] || sz[v] > sz[son[u]] ? son[u] = v : 0;
}
void dfs(int u, int tp){
	top[u] = tp, dfn[u] = ++idx;
	if (son[u]) dfs(son[u], tp);
	for (register int i = hd[u], v; i; i = pr[i])
		if ((v = to[i]) != son[u]) dfs(v, v);
}
int LCA(int u, int v){
	while (top[u] != top[v]){
		if (dep[top[u]] < dep[top[v]]) std :: swap(u, v);
		u = fa[top[u]];
	}
	if (dep[u] < dep[v]) std :: swap(u, v);
	return v;
}
struct Segment_Tree{
	int a[N << 2];
	void modify(int u, int l, int r, int L, int R, int x){
		if (L <= l && r <= R) return a[u] = x, (void)0;
		int mid = (l + r) >> 1;
		if (L <= mid) modify(u << 1, l, mid, L, R, x);
		if (R > mid) modify(u << 1 | 1, mid + 1, r, L, R, x);
	}
	int query(int u, int l, int r, int x){
		if (l <= x && x <= r && a[u]) return a[u];
		if (l == r) return 0;
		int mid = (l + r) >> 1;
		if (x <= mid) return query(u << 1, l, mid, x);
		else return query(u << 1 | 1, mid + 1, r, x);
	}
}T;
int p[N][20];
void Modify(int u, int v, int x){
	while (top[u] != top[v]){
		if (dep[top[u]] < dep[top[v]]) std :: swap(u, v);
		T.modify(1, 1, n, dfn[top[u]], dfn[u], x);
		u = fa[top[u]];
	}
	if (dep[u] < dep[v]) std :: swap(u, v);
	T.modify(1, 1, n, dfn[v], dfn[u], x);
}
bool in(int x, int y){
	return dfn[y] <= dfn[x] && dfn[x] < dfn[y] + sz[y];
}
int Query(int u, int v){
	if (u == v) return 0;
	int lca = LCA(u, v), ans = 0;
	for (register int i = 19; ~i; --i)
		if (dep[p[u][i]] > dep[lca]) ans += 1 << i, u = p[u][i];
	for (register int i = 19; ~i; --i)
		if (dep[p[v][i]] > dep[lca]) ans += 1 << i, v = p[v][i];
	if (p[u][0] == u && u != lca || p[v][0] == v && v != lca) return -1;
	if (u == lca || v == lca) return ans + 1;
	int L = std :: lower_bound(a + 1, a + 1 + m, (bus){0, 0, lca}) - a;
	int R = std :: upper_bound(a + 1, a + 1 + m, (bus){0, 0, lca}) - a - 1;
	if (R - L > 1e8 / q) return ans + 2;
	for (register int i = L; i <= R; ++i) if (in(a[i].x, u) && in(a[i].y, v)) return ans + 1;
	for (register int i = L; i <= R; ++i) if (in(a[i].x, v) && in(a[i].y, u)) return ans + 1;
	return ans + 2;
}
int main(){
	freopen("bus.in", "r", stdin);
	freopen("bus.out", "w", stdout);
	n = read();
	for (register int i = 2; i <= n; ++i) fa[i] = read(), addedge(fa[i], i);
	dfs(1), dfs(1, 1);
	m = read();
	for (register int i = 1; i <= m; ++i)
		a[i].x = read(), a[i].y = read(), a[i].z = LCA(a[i].x, a[i].y);
	for (register int i = 1; i <= n; ++i) a[++m] = (bus){i, i, i};
	std :: sort(a + 1, a + 1 + m, cmp);
//	for (register int i = 1; i <= m; ++i) printf("%d %d %d\n", a[i].x, a[i].y, a[i].z);
	for (register int i = m; i; --i) Modify(a[i].x, a[i].y, a[i].z);
	for (register int i = 1; i <= n; ++i) p[i][0] = T.query(1, 1, n, dfn[i]);
	for (register int j = 1; j <= 19; ++j)
		for (register int i = 1; i <= n; ++i)
			p[i][j] = p[p[i][j - 1]][j - 1];
//	for (register int i = 1; i <= n; ++i) printf("%d %d %d\n", p[i][0], p[i][1], p[i][2]);
	std :: sort(a + 1, a + 1 + m);
	q = read();
	while (q--){
		int u = read(), v = read();
		printf("%d\n", Query(u, v));
	}
}
