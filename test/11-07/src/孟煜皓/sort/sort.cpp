#include <cstdio>
#include <algorithm>
void print(int x){
	x < 10 ? putchar(x + 48) : (print(x / 10), putchar(x % 10 + 48)); 
}
#define N 100005
int n, t, cnt, x, pos[N], a[N];
int rnd(){
	return x = 1ll * x * 48271 % 2147483647;
}
int F(int l, int r){
	return t == 3 ? (l + r) >> 1 : rnd() % (r - l + 1) + l;
}
int main(){
	freopen("sort.in", "r", stdin);
	freopen("sort.out", "w", stdout);
	scanf("%d%d%d", &n, &t, &cnt);
	if (t == 1 || t == 2){
		for (register int i = 1; i <= n; ++i) print(i), putchar(' ');
		return 0;
	}
	if (t == 4) scanf("%d", &x);
	for (register int i = 1; i <= n; ++i) pos[i] = i;
	for (register int i = 1; i <= n; ++i){
		int p = F(i, n);
		a[pos[p]] = i, std :: swap(pos[i], pos[p]);
	}
	for (register int i = 1; i <= n; ++i) print(a[i]), putchar(' ');
}
