var n,p,k:int64; i,j:longint;
    a,sum:array[0..100000]of int64;
    c:array[0..3000,0..3000]of int64;
function add(x,y:int64):int64;
begin
 inc(x,y);
 if x>=p then dec(x,p);
 exit(x);
end;
begin
 assign(input,'graph.in');
 assign(output,'graph.out');
 reset(input);
 rewrite(output);
 p:=1000000007;
 read(n,k);
 if n mod 2<>k then writeln(0)
 else begin
 for i:=0 to n do c[0,i]:=1;
 for i:=1 to n do
  for j:=1 to n do c[i,j]:=add(c[i-1,j-1],c[i,j-1]);
 for i:=1 to n do
  for j:=0 to i do sum[i]:=add(sum[i],c[j,i]);
 end;
 k:=1;
 for i:=2 to n do k:=k*sum[i-1]mod p;
 writeln(k);
 close(input);
 close(output);
end.