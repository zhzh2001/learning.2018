type arr=array[0..10]of longint;
var n,m,k,p,t,x,y,pp,ans,xx,ma:int64; i,j:longint;
    d:array[0..10]of longint;
    c:array[0..10]of boolean;
    b:array[0..3628800,0..10]of longint;
procedure sort(l,r: longint;a:arr);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         pp:=pp*48271 mod 2147483647;;
         x:=a[pp mod(r-l+1)+l];
         repeat
           while a[i]<x do
           begin
            inc(i);
            inc(ans);
           end;
           while x<a[j] do
           begin
            dec(j);
            inc(ans);
           end;
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
                inc(ans,2);
             end;
         until i>j;
         if l<j then
           sort(l,j,a);
         if i<r then
           sort(i,r,a);
      end;
procedure dfs(k:longint);
var i:longint;
begin
 if k=n+1 then
 begin
  inc(t);
  for i:=1 to n do b[t,i]:=d[i];
  exit;
 end;
 for i:=1 to n do
  if not c[i] then
  begin
   c[i]:=true;
   d[k]:=i;
   dfs(k+1);
   c[i]:=false;
  end;
end;
begin
 assign(input,'sort.in');
 assign(output,'sort.out');
 reset(input);
 rewrite(output);
 read(n,m,k);
 if (m=1)or(m=2) then
 begin
  for i:=1 to n do write(i,' ');
 end;
 if m=3 then
 begin
  for i:=2 to (n+1)div 2 do write(i,' ');
  write(1,' ',n,' ');
  for i:=(n+1)div 2+1 to n-1 do write(i,' ');
 end;
 if m=4 then
 begin
  read(p);
  dfs(1);
  for i:=1 to t do
  begin
   pp:=p;
   ans:=0;
   sort(1,n,b[i]);
   if ans>k then
   begin
    for j:=1 to n do write(b[i,j],' ');
    break;
   end;
  end;
 end;
 close(input);
 close(output); 
end.