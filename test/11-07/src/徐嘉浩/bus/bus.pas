uses math;
var n,m,q,i,j,x,y,ans,t,k:longint;
    a:array[0..200000]of longint;
    tree:array[0..800000]of longint;
procedure build(nod,l,r:longint);
begin
 if l=r then begin tree[nod]:=a[l]; exit; end;
 build(nod*2,l,(l+r)div 2);
 build(nod*2+1,(l+r)div 2+1,r);
 tree[nod]:=max(tree[nod*2],tree[nod*2+1]);
end;
function find(nod,l,r,ll,rr:longint):longint;
begin
 if (l=ll)and(r=rr) then exit(tree[nod]);
 if rr<=(l+r)div 2 then exit(find(nod*2,l,(l+r)div 2,ll,rr));
 if ll>(l+r)div 2 then exit(find(nod*2+1,(l+r)div 2+1,r,ll,rr));
 exit(max(find(nod*2,l,(l+r)div 2,ll,(l+r)div 2),
 find(nod*2+1,(l+r)div 2+1,r,(l+r)div 2+1,rr)));
end;
begin
 assign(input,'bus.in');
 assign(output,'bus.out');
 reset(input);
 rewrite(output);
 read(n);
 for i:=1 to n-1 do read(x);
 read(m);
 for i:=1 to m do
 begin
  read(x,y);
  if x>y then begin x:=x+y; y:=x-y; x:=x-y; end;
  a[x]:=y;
 end;
 build(1,1,n);
 read(q);
 for i:=1 to q do
 begin
  read(x,y);
  if x>y then begin x:=x+y; y:=x-y; x:=x-y; end;
  t:=x; ans:=0;
  while t<y do
  begin
   k:=find(1,1,n,x,t);
   if k=t then begin writeln(-1); break; end;
   inc(ans); t:=k;
  end;
  if t>=y then writeln(ans);
 end;
 close(input);
 close(output);
end.