program sort;
 var
  a,b:array[0..100001] of longint;
  mot,i,n,p,teby,ntc,che:longint;
  x:int64;
 begin
  assign(input,'sort.in');
  assign(output,'sort.out');
  reset(input);
  rewrite(output);
  mot:=2147483647;
  che:=48271;
  readln(n,ntc,teby);
  if ntc=4 then readln(x);
  for i:=1 to n do
   b[i]:=i;
  for i:=n downto 1 do
   begin
    if ntc=4 then x:=x*che mod mot;
    if ntc=1 then p:=1;
    if ntc=2 then p:=i;
    if ntc=3 then p:=(1+i)>>1;
    if ntc=4 then p:=x mod (i-1+1)+1;
    a[b[p]]:=i;
    b[p]:=b[i];
   end;
  for i:=1 to n-1 do
   write(a[i],' ');
  writeln(a[n]);
  close(input);
  close(output);
 end.
