#include <bits/stdc++.h>
#define For(i,a,b) for(register int i=a;i<=b;i++)
#define Dow(i,a,b) for(register int i=a;i>=b;i--)
using namespace std;
inline char gc() {
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read() {
	int x = 0;
	char ch = gc();
	bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
int n,m,q,fa[2400][24],dep[2400],cnt,head[120000];
struct node {
	int next,to;
} sxd[240000];
vector <int> bus[120][120];
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	head[u]=cnt;
}
set <int> b[230000];
set <int> qq;
bool one=true;
void dfs(int x) {
	For(i,1,20) {
		fa[x][i]=fa[fa[x][i-1]][i-1];
	}
	for(int i=head[x]; i; i=sxd[i].next) {
		fa[sxd[i].to][0]=x;
		dep[sxd[i].to]=dep[x]+1;
		dfs(sxd[i].to);
	}
}
int lca(int x,int y) {
	if (dep[x]>dep[y])swap(x,y);
	Dow(i,20,0) if ((dep[x])<=(dep[y]-(1<<i))) y=fa[y][i];
	Dow(i,20,0) if (fa[x][i]^fa[y][i]) x=fa[x][i],y=fa[y][i];
	return (x^y)?fa[x][0]:x;
}
void pfy(int x,int y,int w) {
	while(x!=y) {
		if(dep[x]<dep[y]) swap(x,y);
		int xx=x,yy=fa[x][0];
		bus[yy][xx].push_back(w);
		x=fa[x][0];
	}
}
int calc(int x,int y) {
	qq.clear();
	while(x!=y) {
		if(dep[x]<dep[y]) swap(x,y);
		int xx=x,yy=fa[x][0];
		swap(xx,yy);
		if(bus[xx][yy].empty()) return -1;
		for(int i=0; i<(int)bus[xx][yy].size(); i++) {
			qq.insert(bus[xx][yy][i]);
		}
		x=fa[x][0];
	}
	return qq.size();
}
//set <int> cc[5200];
//void xkx(int x,int ff,int y) {
//	if(x==y) {
//		cout<<cc[y].size();
//		return;
//	}
//	for(int i=head[x];i;i=sxd[i].next) {
//		if(sxd[i].to==ff) continue;
//		cc[sxd[i].to]=cc[]
//
//
//	}
//
//}
//int calc(int x,int y) {
//	queue <int> q;
//	memset(cc,0,sizeof cc);
//	xkx(x,0,y);
//
//
//
//
//}
void oo() {
	cin>>m;
	for(int i=1; i<=m; i++) {
		int x,y;
		x=read();
		y=read();
		if(x!=1) b[x].insert(i);
		if(y!=1) b[y].insert(i);
	}
	cin>>q;
	for(int i=1; i<=m; i++) {
		int x,y;
		x=read();
		y=read();
		if(x==1 && !b[y].empty()) {
			puts("1");
			continue;
		}
		if(y==1 && !b[x].empty()) {
			puts("1");
			continue;
		}
		if(b[x].empty() || b[y].empty()) {
			puts("-1");
			continue;
		}
		bool flag=false;
		for(set <int> :: iterator it=b[x].begin(); it!=b[x].end(); it++) {
			if(b[y].find(*it)!=b[y].end()) {
				flag=true;
				break;
			}
		}
		if(flag) puts("1");
		else puts("2");
	}
}
void noip() {
	dfs(1);
	cin>>m;
	For(i,1,m) {
		int x,y;
		x=read();
		y=read();
		pfy(x,y,i);
	}
	cin>>q;
	For(i,1,q) {
		int x,y;
		x=read();
		y=read();
		cout<<calc(x,y)<<'\n';
	}
}
signed main() {
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	cin>>n;
	for(int i=2; i<=n; i++) {
		int x;
		cin>>x;
		add(x,i);
		if(x!=1) one=false;
	}
	if(one) {
		oo();
	} else {
		noip();
	}
}
