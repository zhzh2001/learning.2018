#include <bits/stdc++.h>
#define int long long
using namespace std;
const int mod=1e9+7;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
int n,p,a[1200000],tu[230][230],b[230],ans,tot;
bool same=1;
int ksm(int a,int b) {
	int ans=1;
	while(b) {
		if(b&1) ans=(ans*a)%mod;
		a=(a*a)%mod;
		b>>=1;
	}
	return ans;
}
int dfs(int at) {
	int now=1;
	for(int i=at+1;i<=n;i++) {
		if(b[i]!=b[at] && tu[at][i])
			now+=dfs(i);
	}
	return now;	
}
void subtask1() {
	for(int color=0; color<(1<<n); color++) {
		bool flag=true;
		for(int i=1; i<=n; i++) {
			if(a[i]!=-1 && (color & (1<<(i-1)))) {
				flag=false;
				break;
			}
		}
		if(flag==false) continue;
		for(int i=1;i<=n;i++) if(a[i]>0) b[i]=a[i]; else b[i]=(color & (1<<(i-1)));
		for(int load=0;load<(1<<(n*(n-1)/2));load++) {
			memset(tu,0,sizeof tu);
			int cnt=0;
			for(int i=1;i<n;i++)
				for(int j=i+1;j<=n;j++) {
					cnt++;
					if(load & (1<<(cnt-1))) tu[i][j]=true;
				}
			ans=0;	
			for(int i=1;i<=n;i++) ans+=dfs(i);
			if(ans%2==p)  {
				tot++;		
			}
		}
	}
	cout<<tot;
}
signed main() {
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	cin>>n>>p;
	for(int i=1; i<=n; i++) {
		a[i]=read();
		if(i!=1  && a[i]!=-1 && a[i]!=a[i-1])
			same=false;
	}
	if(same) {
		cout<<ksm(2,(n-1)*n/2);
		return 0;
	}
	subtask1();
	return 0;
}
