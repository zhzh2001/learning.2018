/*
如果是1或2：正序输出
如果是3：递推式子输出
偶数：与I-1换
奇数：与现在序列的n/2+1换
如果是4:瞎搞一下 
*/
#include <bits/stdc++.h>
using namespace std;
// fast io
inline void write(int x) {
	if(x>=10) write(x/10);
	register char ch=x%10+'0';
	putchar(ch);	
}
// fast io
int n,m,k,seed;
int a[120000],mp[120000];
signed main() {
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	cin>>n>>m>>k;
	if(m==1 || m==2) {
		for(int i=1; i<=n; i++) {write(i);putchar(' ');}
		return 0;
	} else if(m==3) {
		if(n<=4) {
			if(n==1) {
				puts("1");
			} else if(n==2) {
				puts("1 2");	
			} else if(n==3) {
				puts("1 3 2");	
			} else if(n==4) {
				puts("1 3 2 4");	
			}
			return 0;
		} else {
			a[1]=1;
			a[2]=3;
			a[3]=2;
			a[4]=4;
			for(int i=5;i<=n;i++) {
				a[i]=i;
				if(i%2==0) {
					int at=mp[i-1];
					mp[i]=mp[i-1];
					mp[i-1]=i;
					swap(a[mp[i]],a[i]);	
				} else {
					int num=a[i/2+1];
					mp[i]=i/2+1;
					mp[a[i/2+1]]=i;
					swap(a[i/2+1],a[i]);
				}
			}
			for(int i=1;i<=n;i++) {
				write(a[i]);
				putchar(' ');
			}
			return 0;
		}
	} else if(m==4){
		cin>>seed;
		if(n==10) {
			if(seed==0) puts("1 2 3 4 5 6 7 8 9 10");
			else if(seed==2) puts("1 2 10 9 5 8 6 4 3 7");	
			else if(seed==147) puts("1 2 8 3 4 6 9 10 5 7");
			else if(seed==483) puts("1 2 5 10 9 3 6 7 8 4");
			else if(seed==647) puts("1 2 9 7 3 4 6 10 5 8");
		} else {
			for(int i=1;i<=n;i++) cout<<n+1-i<<" ";	
		}	
	}
}
