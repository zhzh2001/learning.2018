#include<bits/stdc++.h>
using namespace std;
#define gc getchar
inline int read() {
  int x=0,f=0;char ch=gc();
  for(;ch<'0'||ch>'9';ch=gc()) if (ch=='-') f=1;
  for(;ch>='0'&&ch<='9';ch=gc()) x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const int N = 200005;
struct edge {
  int nxt, to;
}e[N<<1];
int n, head[N], cnt, fa[N], bin[20], m;
inline void insert(int u, int v) {
  e[++cnt] = (edge) {head[u], v}; head[u] = cnt;
  e[++cnt] = (edge) {head[v], u}; head[v] = cnt;
}
int dep[N], sz[N], son[N];
/*inline void dfs(int x) {
  sz[x] = 1;
  for (int i = 1; i < 20; i++) {
    if (dep[x] < bin[i]) break;
    fa[x][i] = fa[fa[x][i-1]][i-1];
  }
  for (int i = head[x]; i; i = e[i].nxt)
    if (e[i].to != fa[x][0]) {
      fa[e[i].to][0] = y;
      dep[e[i].to] = dep[x]+1;
      dfs(e[i].to, x);
      sz[x] += sz[e[i].to];
      if (sz[son[x]] < sz[e[i].to]) son[x] = e[i].to;
    }
}
int l[N], ind, bl[N], r[N];
inline void dfs(int x, int ch) {
  l[x] = ++ind; bl[x] = ch;
  if (son[x]) dfs(son[x], ch);
  for (int i = head[x]; i; i = e[i].nxt)
    if (e[i].to != fa[x][0]&&e[i].to != son[x]) dfs(e[i].to, e[i].to);
  r[x] = ind;
}
inline int Query(int u, int v) {
  
}*/
inline void dfs(int x) {
  for (int i = head[x]; i; i = e[i].nxt)
    if (fa[x] != e[i].to) {
      dep[e[i].to] = dep[x]+1;
      fa[e[i].to] = x;
      dfs(e[i].to);
    }
}
int dis[505][505];
int main() {
  freopen("bus.in","r",stdin);
  freopen("bus.out","w",stdout);
  bin[0] = 1;
  for (int i = 1; i < 20; i++) bin[i] = bin[i-1]<<1;
  n = read();
  for (int i = 2; i <= n; i++) {
    int x = read();
    insert(x, i);
  }
  dfs(1);
  m = read();
  memset(dis, 63, sizeof dis);
  for (int i = 1; i <= n; i++)
    dis[i][i] = 0;
  for (int i = 1; i <= m; i++) {
    vector <int> t;
    int u = read(), v = read();
    if (dep[u] < dep[v]) swap(u, v);
    while (dep[u] != dep[v]) {
      t.push_back(u);
      u = fa[u];
    }
    while (u != v) {
      t.push_back(u);
      t.push_back(v);
      u = fa[u];
      v = fa[v];
    }
    t.push_back(u);
    int M = t.size();
    for (int i = 0; i < M; i++)
      for (int j = 0; j < M; j++) dis[t[i]][t[j]] = min(dis[t[i]][t[j]], 1);
  }
  for (int k = 1; k <= n; k++)
    for (int i = 1; i <= n; i++)
      for (int j = 1; j <= n; j++)
        dis[i][j] = min(dis[i][j], dis[i][k]+dis[k][j]);
  int Q = read();
  while (Q--) {
    int x = read(), y = read();
    if (dis[x][y] > n) puts("-1");
    else printf("%d\n", dis[x][y]);
  }
  return 0;
}
/*
7
1 1 1 4 5 6
4
4 2
5 4
1 3
6 7
5
4 5
3 5
7 2
3 2
5 3
*/
