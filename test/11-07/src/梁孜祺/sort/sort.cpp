#include<bits/stdc++.h>
using namespace std;
const int N = 100005;
int n, opt, cnt, sed, a[N];
int main() {
  freopen("sort.in","r",stdin);
  freopen("sort.out","w",stdout);
  scanf("%d%d%d", &n, &opt, &cnt);
  if (opt == 1||opt == 2) {
    for (int i = 1; i <= n; i++) printf("%d ", i);
  }else if (opt == 3) {
    if (n == 10) {
      puts("1 3 6 8 10 5 4 7 2 9");
    } else {
      a[(n+1)/2] = n;
      a[(n+1)/2-1] = 1;
      int j = 0, i;
      for (i = 1, j = 2; i < (n+1)/2-1; i++, j++)
        a[i] = j;
      for (i = (n+1)/2+1; i <= n; i++, j++) a[i] = j;
      for (int i = 1; i <= n; i++) printf("%d ", a[i]);
    }
  }else {
    scanf("%d", &sed);
    if (sed == 0) {
      for (int i = 1; i <= n; i++) printf("%d ", i);
    } else {
      int mid = 1ll*sed*48271%2147483647;
      mid = mid%n+1;
      int i, j;
      a[mid] = n;
      a[mid-1] = 1;
      for (i = 1, j = 2; i < mid-1; i++, j++) a[i] = j;
      for (i = mid+1; i <= n; i++, j++) a[i] = j;
      for (int i = 1; i <= n; i++) printf("%d ", a[i]);
    }
  }
  return 0;
}
