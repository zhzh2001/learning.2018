#include<bits/stdc++.h>
using namespace std;
#define gc getchar
inline int read() {
  int x=0,f=0;char ch=gc();
  for(;ch<'0'||ch>'9';ch=gc()) if (ch=='-') f=1;
  for(;ch>='0'&&ch<='9';ch=gc()) x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
int n, p, A, B, C, m;
int main() {
  freopen("graph.in","r",stdin);
  freopen("graph.out","w",stdout);
  n = read(); m = read();
  for (int i = 1; i <= n; i++) {
    int x = read();
    if (x == 1) A++;
    if (x == 0) B++;
    if (x == -1) C++;
  }
  if (n%2 == m) printf("%d\n", n);
  else puts("0");
  return 0;
}
