#include <bits/stdc++.h>
#define mkp make_pair
#define pa pair<int,int>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=2e5+10;
int nedge=0,p[2*N],nex[2*N],head[2*N];
int n,m,fa[N][18],dep[N];
bool b[5010][5010];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(int x){
	dep[x]=dep[fa[x][0]]+1;
	for(int j=1;j<18;j++)fa[x][j]=fa[fa[x][j-1]][j-1];
	for(int k=head[x];k;k=nex[k])dfs(p[k]);
}
inline int LCA(int x,int y){
	if(dep[x]<dep[y])swap(x,y);
	for(int i=17;i>=0;i--)if(dep[fa[x][i]]>=dep[y])x=fa[x][i];
	for(int i=17;i>=0;i--)if(fa[x][i]!=fa[y][i])x=fa[x][i],y=fa[y][i];
	if(x!=y)x=fa[x][0];return x;
}
map<pa,bool>mp;
vector<int>ep[N],e[N];
inline void Main(){
	m=read();
	static int b[N]={0};
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		if(x>y)swap(x,y);
		b[x]=b[y]=1;
		mp[mkp(x,y)]=1;
	}
	for(int Q=read();Q;Q--){
		int x=read(),y=read();
		if(x>y)swap(x,y);
		if(mp[mkp(x,y)])puts("1");
		else if(b[x]&&b[y])puts("2");
		else puts("-1");
	}
	exit(0);
}
int dist[N];
queue<int>q;
int main()
{
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	bool flag=1;
	for(int i=2;i<=n;i++){
		int x=read();fa[i][0]=x;
		if(x>1)flag=0;
		addedge(x,i);
	}
	if(flag)Main();
	dfs(1);
	m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		int lca=LCA(x,y);
		b[i][i]=1;
		while(1){
			for(int j=0;j<ep[x].size();j++){
				int to=ep[x][j];
				if(!b[i][to]){
					b[i][to]=1;
					e[i].push_back(to);
					e[to].push_back(i);
				}
			}
			ep[x].push_back(i);
			if(x==lca)break;
			x=fa[x][0];
		}
		x=y;
		while(1){
			for(int j=0;j<e[x].size();j++){
				int to=e[x][j];
				if(!b[i][to]){
					b[i][to]=1;
					e[i].push_back(to);
					e[to].push_back(i);
				}
			}
			if(x==lca)break;
			ep[x].push_back(i);
			x=fa[x][0];
		}
	}
	for(int Q=read();Q;Q--){
		int x=read(),y=read();
		for(int i=1;i<=m;i++)dist[i]=-1;
		for(int i=0;i<ep[x].size();i++)q.push(ep[x][i]),dist[ep[x][i]]=0;
		while(!q.empty()){
			int now=q.front();q.pop();
			for(int i=0;i<e[now].size();i++)if(dist[e[now][i]]==-1){
				dist[e[now][i]]=dist[now]+1;
				q.push(e[now][i]);
			}
		}
		int ans=1e9;
		for(int i=0;i<ep[y].size();i++)ans=min(ans,dist[ep[y][i]]);
		writeln(ans);
	}
	return 0;
}
