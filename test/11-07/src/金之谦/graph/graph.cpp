#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e6+10,MOD=1e9+7;
int n,P,ans=0,s[210],bin[210];
short a[N],d[210][210];
inline int mi(int a,int b){
	int x=1,y=a;
	for(;b;b>>=1ll,y=y*y%MOD)if(b&1ll)x=x*y%MOD;
	return x;
}
inline void upd(int &x,int y){x=(x+y)%MOD;}
inline void check(){
	int ss=0;
	for(int i=1;i<=n;i++){
		s[i]=1;
		for(int j=1;j<i;j++)if(a[i]^a[j]&&d[j][i])s[i]+=s[j];
		ss=(ss+s[i])%2;
	}
	if(P==ss)ans++;
}
inline void dfss(int x){
	if(x==n+1){check();return;}
	for(int j=0;j<bin[x-1];j++){
		for(int k=1;k<x;k++)if((1<<(k-1))&j)d[k][x]=1;
		dfss(x+1);
		for(int k=1;k<x;k++)if((1<<(k-1))&j)d[k][x]=0;
	}
}
inline void dfs(int x){
	while(x<=n&&a[x]>-1)x++;
	if(x==n+1){dfss(1);return;}
	a[x]=1;dfs(x+1);
	a[x]=0;dfs(x+1);
	a[x]=-1;
}
inline void Main(){
	if(P==(n&1)){
		writeln(mi(2,n*(n-1)/2ll));
	}else puts("0");
	exit(0);
}
signed main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();P=read();
	bool flag=1;
	for(int i=1;i<=n;i++){
		a[i]=read();
		if(a[i]==-1||(a[i]>1&&a[i]!=a[i-1]))flag=0;
	}
	if(flag)Main();
	bin[0]=1;
	for(int i=1;i<=n;i++)bin[i]=bin[i-1]<<1;
	dfs(1);writeln(ans);
	return 0;
}
