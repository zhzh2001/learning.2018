#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int n,op,cnt,a[N],b[N],Cnt=0;
int seed;
inline void pt(int l,int r){
	if(l>r)return;
	int mid=l+r>>1;
	a[mid]=Cnt--;
	pt(l,mid-1);pt(mid+1,r);
}
inline int ran(int l,int r){
	if(op==3)return l+r>>1;
	seed=(seed*48271)%2147483647ll;
	return seed%(r-l+1)+l;
}
inline void Sort(int l,int r){
	int i=l,j=r,mid=b[ran(l,r)];
	do{
		while(b[i]<mid)i++,Cnt++;
		while(mid<b[j])j--,Cnt++;
		if(i<=j)swap(b[i],b[j]),i++,j--,Cnt+=2;
	}while(i<=j);
	if(l<j)Sort(l,j);
	if(i<r)Sort(i,r);
}
signed main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();op=read();cnt=read();
	if(op==1){
		for(int i=n;i;i--)write(i),putchar(' ');
			return 0;
	}
	if(op==2){
		for(int i=1;i<=n;i++)write(i),putchar(' ');
		return 0;
	}
	if(op==3){
		for(int i=1;i<=n;i++)a[i]=i;
		do{
			Cnt=0;
			for(int i=1;i<=n;i++)b[i]=a[i];
			Sort(1,n);
			if(Cnt>cnt){
				for(int i=1;i<=n;i++)write(a[i]),putchar(' ');
				break;
			}
		}while(next_permutation(a+1,a+n+1));
		return 0;
	}
	if(op==4){
		int Seed=read();
		for(int i=1;i<=n;i++)a[i]=i;
		do{
			Cnt=0;seed=Seed;
			for(int i=1;i<=n;i++)b[i]=a[i];
			Sort(1,n);
			if(Cnt>cnt){
				for(int i=1;i<=n;i++)write(a[i]),putchar(' ');
				break;
			}
		}while(next_permutation(a+1,a+n+1));
		return 0;
	}
	return 0;
}
