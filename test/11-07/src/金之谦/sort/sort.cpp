#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int n,op,cnt,a[N],b[N],Cnt=0;
int seed;
inline int ran(int l,int r){
	if(op==3)return l+r>>1;
	seed=(seed*48271)%2147483647ll;
	return seed%(r-l+1)+l;
}
signed main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();op=read();cnt=read();
	if(op==1){
		for(int i=n;i;i--)write(i),putchar(' ');
		return 0;
	}
	if(op==2){
		for(int i=1;i<=n;i++)write(i),putchar(' ');
		return 0;
	}
	if(op==4)seed=read();
	int Cnt=n;
	for(int i=n;i;i--)b[i]=ran(1,i);
	for(int i=1;i<=n;i++)a[i]=i;
	for(int i=1;i<=n;i++){
		swap(a[b[i]],a[i]);
	}
	for(int i=1;i<=n;i++)write(a[i]),putchar(' ');
	return 0;
}
