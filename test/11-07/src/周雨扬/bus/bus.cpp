#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define ll long long
#define pii pair<int,int>
using namespace std;

const int LBC=2333333;
char LZH[LBC],*SSS=LZH,*TTT=LZH;
inline char gc(){
	if (SSS==TTT){
		TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
		if (SSS==TTT) return EOF;
	}
	return *SSS++;
}
inline int read(){
	int x=0; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
const int N=200005,LG=20;
int dep[N],fa[N][LG];
ll Dep[N]; int Fa[N][LG];
pii v[N][LG];
int n,Q,lg[N];
struct edge{
	int to,next;
}e[N*2];
int dfn[N],ed[N];
int head[N],T,tot;
int DEP[N];
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
}
void dfs(int x){
	dfn[x]=++T;
	for (int i=head[x];i;i=e[i].next)
		dfs(e[i].to);
	ed[x]=T;
}
const int M=5000005;
int ls[M],rs[M],sz[M];
int rt[N],nd;
vector<int> chg[N];
void insert(int &nk,int k,int l,int r,int p){
	sz[nk=++nd]=sz[k]+1;
	ls[nk]=ls[k]; rs[nk]=rs[k];
	if (l==r) return;
	int mid=(l+r)/2;
	if (p<=mid) insert(ls[nk],ls[k],l,mid,p);
	else insert(rs[nk],rs[k],mid+1,r,p);
}
int ask(int k,int l,int r,int x,int y){
	if (!k||(l==x&&r==y)) return sz[k];
	int mid=(l+r)/2;
	if (y<=mid) return ask(ls[k],l,mid,x,y);
	if (x>mid) return ask(rs[k],mid+1,r,x,y);
	return ask(ls[k],l,mid,x,mid)+ask(rs[k],mid+1,r,mid+1,y);
}
int LCA(int x,int y){
	if (dep[x]<dep[y]) swap(x,y);
	int tmp=dep[x]-dep[y];
	For(i,0,lg[tmp]) if (tmp&(1<<i)) x=fa[x][i];
	Rep(i,lg[dep[x]],0)
		if (fa[x][i]!=fa[y][i])
			x=fa[x][i],y=fa[y][i];
	return x==y?x:fa[x][0];
}
int jump1(int x,int y){
	Rep(i,lg[dep[x]-dep[y]],0)
		if (dep[fa[x][i]]>dep[y])
			x=fa[x][i];
	return x;
}
ll jump2(int &x,int y){
	if (x==y) return 0;
	ll ans=Dep[x];
	Rep(i,lg[DEP[x]-DEP[y]+1],0)
		if (dep[Fa[x][i]]>dep[y])
			x=Fa[x][i];
	return ans-Dep[Fa[x][0]];
}
inline void write(int x){
	if (x==-1){
		puts("-1");
		return;
	}
	if (x==0){
		puts("0");
		return;
	}
	static int a[15],top;
	for (top=0;x;a[++top]=x%10,x/=10);
	for (;top;putchar(a[top--]+'0'));
	puts("");
}
int main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read(); dep[1]=1;
	For(i,2,n) lg[i]=lg[i/2]+1;
	For(i,2,n){
		add(fa[i][0]=read(),i);
		dep[i]=dep[fa[i][0]]+1;
	}
	fa[1][0]=1; dfs(1);
	For(i,1,n) v[i][0]=pii(dep[i],i);
	For(j,1,n) For(i,1,lg[dep[j]]){
		fa[j][i]=fa[fa[j][i-1]][i-1];
		v[j][i]=pii(1e9,1e9);
	}
	Q=read();
	while (Q--){
		int x=read(),y=read(),L;
		L=LCA(x,y);
		if (x!=L&&y!=L){
			if (dfn[x]>dfn[y]) swap(x,y);
			chg[dfn[x]].push_back(dfn[y]);
		}
		int del=dep[x]-dep[L];
		For(i,0,lg[del])
			if (del&(1<<i)){
				v[x][i]=min(v[x][i],pii(dep[L],L));
				x=fa[x][i];
			}
		del=dep[y]-dep[L];
		For(i,0,lg[del])
			if (del&(1<<i)){
				v[y][i]=min(v[y][i],pii(dep[L],L));
				y=fa[y][i];
			}
	}
	For(i,1,n){
		rt[i]=rt[i-1];
		for (int j=0;j<chg[i].size();j++)
			insert(rt[i],rt[i],1,n,chg[i][j]);
	}
	Rep(j,n,1) Rep(i,lg[dep[j]],1){
		v[j][i-1]=min(v[j][i-1],v[j][i]);
		v[fa[j][i-1]][i-1]=min(v[fa[j][i-1]][i-1],v[j][i]);
	}
	Fa[1][0]=1; Dep[1]=DEP[1]=1;
	For(i,2,n){
		if (v[i][0].second==i)
			Fa[i][0]=fa[i][0],Dep[i]=n+2333;
		else Fa[i][0]=v[i][0].second,Dep[i]=1;
		Dep[i]+=Dep[Fa[i][0]];
		DEP[i]=DEP[Fa[i][0]]+1;
	}
	For(j,1,n) For(i,1,lg[DEP[j]])
		Fa[j][i]=Fa[Fa[j][i-1]][i-1];
	Q=read();
	while (Q--){
		int x=read(),y=read(),L; ll ans;
		L=LCA(x,y);
		ans=jump2(x,L)+jump2(y,L);
		if (L!=x&&L!=y){
			if (dfn[x]>dfn[y]) swap(x,y);
			if (ask(rt[dfn[x]-1],1,n,dfn[y],ed[y])!=
				ask(rt[ed[x]],1,n,dfn[y],ed[y]))
					ans--;
		}
		write(ans>n?-1:ans);
	}
}
/*
7
1 1 1 4 5 6
4
4 2
5 4
1 3
6 7
6
4 5
3 5
7 2
3 2
5 3
4 2

*/
