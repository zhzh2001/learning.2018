#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
const int mo=1000000007;
const int N=1000005;
int n,p,c,ans;
int f[2][3][3];
const int LBC=233333;
char LZH[LBC],*SSS=LZH,*TTT=LZH;
inline char gc(){
	if (SSS==TTT){
		TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
		if (SSS==TTT) return EOF;
	}
	return *SSS++;
}
inline int read(){
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=0;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return f?x:-x;
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(); p=read();
	f[0][2][2]=1; c=0;
	int P1=1,P2=0;
	For(i,1,n){
		int x=read();
		if (x!=1)
			For(H1,0,2) For(B1,0,2){
				int v=f[c][H1][B1];
				if (!v) continue;
				int tr0=(B1==2?P1:P2);
				int tr1=(B1==2?0:P2);
				int tr=(H1==2?1:H1^1);
				f[c^1][tr][B1]=(f[c^1][tr][B1]+1ll*v*tr0)%mo;
				f[c^1][H1][B1]=(f[c^1][H1][B1]+1ll*v*tr1)%mo;
			}//putHei
		if (x!=0)
			For(H1,0,2) For(B1,0,2){
				int v=f[c][H1][B1];
				if (!v) continue;
				int tr0=(H1==2?P1:P2);
				int tr1=(H1==2?0:P2);
				int tr=(B1==2?1:B1^1);
				f[c^1][H1][tr]=(f[c^1][H1][tr]+1ll*v*tr0)%mo;
				f[c^1][H1][B1]=(f[c^1][H1][B1]+1ll*v*tr1)%mo;
			}//putBai
		For(H1,0,2) For(B1,0,2)
			f[c][H1][B1]=0;
		c^=1; P2=P1; P1=P1*2%mo;
	}
	For(H1,0,2) For(B1,0,2)
		if (((H1^B1)&1)==p) ans=(ans+f[c][H1][B1])%mo;
	printf("%d\n",ans);
}
