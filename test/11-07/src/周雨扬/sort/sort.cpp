#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
const int N=100005;
int n,tp,cnt,seed;
int a[N],pos[N];
int rnd(){
	return seed=1ll*seed*48271%2147483647;
}
int getID(int l,int r){
	if (tp==1) return l;
	if (tp==2) return r;
	if (tp==3) return (l+r)/2;
	return rnd()%(r-l+1)+l;
}
void work(){
	int l=1,r=n;
	while (l!=r){
		int id=getID(l,r);
		int x=id,y=r;
		swap(a[pos[x]],a[pos[y]]);
		swap(pos[x],pos[y]);
		r--;
	}
}
void writechar(int x){
	static int a[15],top;
	for (top=0;x;a[++top]=x%10,x/=10);
	for (;top;putchar(a[top--]+'0'));
	putchar(' ');
}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	scanf("%d%d%d",&n,&tp,&cnt);
	if (tp==4) scanf("%d",&seed);
	For(i,1,n) a[i]=i,pos[i]=i;
	work();
	For(i,1,n) writechar(a[i]);
}
