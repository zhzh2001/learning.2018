#include <iostream>
#include <cstdio>
using namespace std;
int n,type,cnt,x,a[100005];
long long m[100005];
inline int read(){
	int a=0,f=1;
	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())a=(a<<1)+(a<<3)+ch-'0';
	return a*f;
}
void sort1(int l,int r,int v){
	a[(l+r)/2]=v;
	int s=l,t=r,x=a[(l+r)/2];
	while(s<=t){
		while(a[s]<x)s++;
		while(a[t]>x)t--;
		if(s<=t){
			swap(a[s],a[t]);
			s++;
			t--;
		}
	}
	if(l<t)sort1(l,t,v-1);
}
void sort2(int l,int r,int v,int k){
	int d=m[k]%(r-l+1)+l;
	a[d]=v;
	int s=l,t=r,x=a[d];
	while(s<=t){
		while(a[s]<x)s++;
		while(a[t]>x)t--;
		if(s<=t){
			swap(a[s],a[t]);
			s++;
			t--;
		}
	}
	if(l<t)sort2(l,t,v-1,k+1);
}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(),type=read(),cnt=read();
	if(type==1||type==2){
		for(int i=1;i<=n;i++)cout<<n-i+1;
	}
	if(type==3){
		for(int i=1;i<=n;i++)a[i]=0;
		sort1(1,n,n);
		for(int i=1;i<=n;i++)cout<<a[i];
	}
	if(type==4){
		m[0]=read();
		if(n==10)cout<<"1541905871";
		else{
			for(int i=1;i<=n;i++)m[i]=(m[i-1]*48271)%2147483647;
			sort2(1,n,n,1);
			for(int i=1;i<=n;i++)cout<<a[i];
		}
	}
	return 0;
}
