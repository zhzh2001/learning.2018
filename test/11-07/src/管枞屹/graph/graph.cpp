#include <iostream>
#include <cstdio>
using namespace std;
const int mod=1000000007;
int n,p,d,a[1000005],ans=0,t=1,w=0,b=0,y[1000005];
long long f[1000005],sum=0;
inline int read(){
	int a=0,f=1;
	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())a=(a<<1)+(a<<3)+ch-'0';
	return a*f;
}
void dfs(int x){
	while(x<=n&&a[x]!=-1)x++;
	if(x==n+1){
		for(int i=1;i<=n;i++)f[i]=1;
		for(int i=2;i<=n;i++){
			for(int j=1;j<i;j++)
				if(a[j]^a[i])f[i]+=f[j];
		}
		for(int i=1;i<=n;i++)sum+=f[i];
		long long k=1,a=2,q=((b-2)*(b-1)/2+(w-1)*(w-2)/2)%mod;
		while(q){
			if(q&1)k=(k*a)%mod;
			q>>=1;
			a=(a*a)%mod;
		}
		sum-=n;
		if(sum&1)ans=(ans+(sum+1)/2*k)%mod;
		else ans=(ans+(sum/2+(((sum+n)&1)&p))*k)%mod;
	}
	if(a[x]==-1){
		a[x]=0,b++;
		dfs(x+1);
		b--;
		a[x]=1,w++;
		dfs(x+1);
		w--;
		a[x]=-1;
	}
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(),p=read();
	for(int i=1;i<=n;i++){a[i]=read();if(a[i])w++;if(a[i]==0)b++;if(a[i]!=a[1])t=0;}
	d=n%2;
	if(t==1&&a[1]!=-1){
		if(d!=p)cout<<0<<endl;
		else {
			long long k=1,a=2,m=(n*(n-1)/2)%mod;
			while(m){
				if(m&1)k=(k*a)%mod;
				m>>=1;
				a=(a*a)%mod;
			}
			cout<<k<<endl;
		}
		return 0;
	}
	dfs(1);
	cout<<ans<<endl;
	return 0;
}
