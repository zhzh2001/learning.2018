#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;++i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=0;
	while (ch<'0'||ch>'9') { if (ch=='-') f=1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return f?-x:x;
}
ll n,p,ans,tot,a[N],b[N],f[6][6];
void work(ll u){
	++tot;
	rep(i,u+1,n) if (f[u][i]&&b[u]!=b[i]) work(i);
}
void judge(){
	tot=0;
	rep(i,1,n) work(i);
	if (tot%2==p) ++ans;
}
void ddfs(ll now){
	if (now==n+1){
		judge();
		return;
	}
	b[now]=a[now];
	if (a[now]!=-1) ddfs(now+1);
	else{
		b[now]=1; ddfs(now+1);
		b[now]=0; ddfs(now+1);
	}
}
ll qpow(ll x,ll y){
	ll num=1; x%=mod;
	for (;y;y>>=1,x=x*x%mod) if (y&1) num=num*x%mod;
	return num;
}
void dfs(ll now){
	if (now==n+1){
		ddfs(1);
		return;
	}
	rep(i,0,(1<<(n-now))-1){
		memset(f,0,sizeof f);
		rep(j,1,n-now) if ((1<<(j-1))&i){
			f[i][now+j]=1;
		}
		dfs(now+1);
	}
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(); p=read();
	rep(i,1,n) a[i]=read();
	if (n<=5){
		dfs(1);
		printf("%lld",ans);
		return 0;
	}
	else{
		if ((n&1)==p) printf("%lld",qpow(2,n*(n-1)/2));
		else puts("0");
	}
}
