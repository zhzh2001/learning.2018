#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;++i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=0;
	while (ch<'0'||ch>'9') { if (ch=='-') f=1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return f?-x:x;
}
ll num,n,ty,cnt,a[N],sum,now,pos[N],tmp;
ll get(ll l,ll r){
	num=num*48271%2147483647;
	return num%(r-l+1)+l;
}
ll F(ll l,ll r){
	if (ty==1) return l;
	if (ty==2) return r;
	if (ty==3) return l+r>>1;
	if (ty==4) return get(l,r);
}
void solve(ll l,ll r){
	sum+=r-l+1;
	if (sum>=cnt||r<l) return;
	ll mid=F(l,r);
	a[pos[mid]]=now; --now;
	swap(pos[mid],pos[r]);
	solve(l,r-1);
}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=now=read(); ty=read(); cnt=read();
	if (ty==4) num=read();
	rep(i,1,n) pos[i]=i; solve(1,n);
	rep(i,1,n){
		if (!a[i]) a[i]=now,--now;
		printf("%lld ",a[i]);
	}
}
