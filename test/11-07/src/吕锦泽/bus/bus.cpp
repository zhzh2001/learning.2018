#include<cstdio>
#include<map>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;++i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=0;
	while (ch<'0'||ch>'9') { if (ch=='-') f=1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return f?-x:x;
}
map<ll,ll> mp[N];
ll n,m,q,fa[N],flag[N];
int main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read(); rep(i,2,n) fa[i]=read();
	m=read(); 
	rep(i,1,m){
		ll u=read(),v=read();
		if (u==v) continue;
		flag[u]=flag[v]=1;
		mp[u][v]=1;
		mp[v][u]=1;
		mp[u][1]=1;
		mp[v][1]=1;
		mp[1][u]=1;
		mp[1][v]=1;
	}
	q=read();
	while (q--){
		ll u,v;
		u=read();v=read();
		if (mp[u][v]) puts("1");
		else{
			if (flag[u]&&flag[v]) puts("2");
			else puts("-1");
		}
	}
}
