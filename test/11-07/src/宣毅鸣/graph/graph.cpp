#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int M=1e9+7,N=10;
int cnt,a[N],b[N][N],ans,n,p;
ll ksm(int x,ll y){
    if (!y)return 1;
    ll z=ksm(x,y/2);
    z*=z;z%=M;
    if (y&1)z*=x;
    return z%M;
}
void dfs3(int x){
    cnt^=1;
    for (int i=x+1;i<=n;i++)
        if (b[x][i]&&(a[x]^a[i]))dfs3(i);
}
void dfs2(int x,int y){
    if (x>n){
        cnt=0;
        for (int i=1;i<=n;i++)dfs3(i);
        if (cnt==p)ans++;
        return;
    }
    if (y>n){
        dfs2(x+1,x+2);
        return;
    }
    b[x][y]=0;
    dfs2(x,y+1);
    b[x][y]=1;
    dfs2(x,y+1);
}
void dfs(int x){
    if (x>n){
        dfs2(1,2);
        return;
    }
    if (a[x]!=-1){
        dfs(x+1);
        return;
    }
    a[x]=1;
    dfs(x+1);
    a[x]=0;
    dfs(x+1);
    a[x]=-1;
}
int main(){
    freopen("graph.in","r",stdin);
    freopen("graph.out","w",stdout);
    scanf("%d%d",&n,&p);
    for (int i=1;i<=n;i++)scanf("%d",&a[i]);
    if (n>5){
        if (n%2==p){
            printf("%lld\n",ksm(2,(ll)n*(n-1)/2));
            return 0;
        }
        else {
            printf("0");
            return 0;
        }
    }
    dfs(1);
    printf("%d",ans);
    return 0;
}
