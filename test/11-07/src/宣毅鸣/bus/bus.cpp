#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=200005,M=534789,N1=5005;
int flag1[M],flag2[M],flag3[M],f_lca[N1][N1];
int read(){
    int x=0;char c=0;
    for (;c<'0'||c>'9';c=getchar());
    for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
    return x;
}
struct Bus{
    int x,y,lca;
}bus[N];
int m,q,tot,x,y,cnt1,cnt2,v3[20][N],v2[20][N];
int v[N],sd[N],in[N],out[N],l,fa[N],flag[N],ne[N],fi[N],zz[N],n;
inline int find(int x,int y){
    int k=(ll)x*y%M;
    for (;flag1[k]&&(flag2[k]!=x||flag3[k]!=y);k=(k+1)%M);
    if (flag1[k])return -1;
    return k;
}
inline void jb(int x,int y){
    ne[++tot]=fi[x];
    fi[x]=tot;
    zz[tot]=y;
}
inline void dfs(int x,int y){
    sd[x]=y;
    in[x]=++l;
    for (int i=fi[x];i;i=ne[i])dfs(zz[i],y+1);
    out[x]=++l;
}
inline int pd(int x,int y){
    return in[x]<=in[y]&&out[x]>=out[y];
}
void doit2(){
    for (int i=1;i<=m;i++)bus[i].x=read(),bus[i].y=read();
    for (int i=1;i<=m;i++){
        if (bus[i].x>bus[i].y)swap(bus[i].x,bus[i].y);
        int l=find(bus[i].x,bus[i].y);
        flag1[l]=1;flag2[l]=bus[i].x;flag3[l]=bus[i].y;
        if (bus[i].x!=1)flag[bus[i].x]=1;
        if (bus[i].y!=1)flag[bus[i].y]=1;
    }
    q=read();
    while (q--){
        scanf("%d%d",&x,&y);
        if (x>y)swap(x,y);
        if (x==y){
            puts("0");
            continue;
        }
        if (x==1){
            if (flag[y])puts("1");
            else puts("-1");
            continue;
        }
        if (find(x,y)==-1){
            puts("1");
            continue;
        }
        if (flag[x]&&flag[y]){
            puts("2");
            continue;
        }
        puts("-1");
    }
}
int Lca(int x,int y){
    if (f_lca[x][y])return f_lca[x][y];
    if (x==y)return f_lca[x][y]=x;
    if (sd[x]>sd[y])return f_lca[x][y]=Lca(fa[x],y);
    else return f_lca[x][y]=Lca(x,fa[y]);
}
int cmp(Bus x,Bus y){
    return x.x<y.x;
}
void doit1(){
    for (int i=1;i<=m;i++)bus[i].x=read(),bus[i].y=read();
    for (int i=1;i<=m;i++)
        if (bus[i].x>bus[i].y)swap(bus[i].x,bus[i].y);
    sort(bus+1,bus+m+1,cmp);
    int ri=0;
    for (int i=1,j=1;i<=n;i++){
        while (j<=m&&bus[j].x==i){
            ri=max(ri,bus[j].y);
            j++;
        }
        ri=max(ri,i);
        v[i]=ri;
    }
    for (int i=1;i<=n;i++)v2[0][i]=v[i];
    for (int i=1;i<=19;i++)
        for (int j=1;j<=n;j++)v2[i][j]=v2[i-1][v2[i-1][j]];
    q=read();
    while (q--){
        scanf("%d%d",&x,&y);
        if (x>y)swap(x,y);
        if (x==y){
            printf("0\n");
            continue;
        }
        int ans=0;
        for (int i=19;i>=0;i--)
            if (v2[i][x]<y)x=v2[i][x],ans+=(1<<i);
        if (v2[19][x]<y){
            puts("-1");
            continue;
        }    
        else printf("%d\n",ans+1);
    }    
}
void doit3(){
    dfs(1,1);
    for (int i=1;i<=n;i++)v[i]=i;     
    for (int i=1;i<=m;i++){
        bus[i].x=read();bus[i].y=read();
        bus[i].lca=Lca(bus[i].x,bus[i].y);
        int xx=bus[i].x;
        while (xx!=bus[i].lca){
            if (sd[v[xx]]>sd[bus[i].lca])v[xx]=bus[i].lca;
            xx=fa[xx];
        }
        xx=bus[i].y;
        while (xx!=bus[i].lca){
            if (sd[v[xx]]>sd[bus[i].lca])v[xx]=bus[i].lca;
            xx=fa[xx];
        }       
    }
    q=read();
    while (q--){
        x=read();y=read();
        if (x==y){
            puts("0");
            continue;
        }
        int k=Lca(x,y),ans=0,flag=0;
        while (sd[v[x]]>sd[k]){
            if (v[x]==x){
                flag=1;
                puts("-1");
                break;
            }
            x=v[x],ans++;
        }
        if (flag)continue;
        while (sd[v[y]]>sd[k]){
            if (v[y]==y){
                flag=1;
                puts("-1");
                break;
            }
            y=v[y],ans++;
        }
        if (flag)continue;
        if (sd[x]<sd[y])swap(x,y);
        if (pd(y,x)){
            printf("%d\n",ans+1);
            continue;
        }
        for (int i=1;i<=m;i++)
            if ((pd(x,bus[i].x)&&pd(y,bus[i].y))||
            (pd(y,bus[i].x)&&pd(x,bus[i].y))){
                flag=1;
                printf("%d\n",ans+1);
                break;
            }
        if (!flag)printf("%d\n",ans+2);    
    }
}
void dfs2(int x,int y){
    sd[x]=y;
    in[x]=++l;
    for (int i=fi[x];i;i=ne[i])dfs(zz[i],y+1);
    out[x]=++l;
}
int lca(int x,int y){
    if (sd[x]<sd[y])swap(x,y);
    if (x==y)return x;
    for (int i=19;i>=0;i--)
        if (v3[i][x]!=0&&!pd(v3[i][x],y))x=v3[i][x];
    return v3[0][x]; 
}
void dfs3(int x){
    for (int i=fi[x];i;i=ne[i]){
        dfs3(zz[i]);
        if (sd[v[zz[i]]]<sd[v[x]])v[x]=v[zz[i]];
    }
}
void doit4(){
    dfs2(1,1);
    for (int i=1;i<=m;i++)bus[i].x=read(),bus[i].y=read();
    for (int i=1;i<=n;i++)v3[0][i]=fa[i];
    for (int i=1;i<=19;i++) 
        for (int j=1;j<=n;j++)v3[i][j]=v3[i-1][v3[i-1][j]];
    for (int i=1;i<=m;i++)bus[i].lca=lca(bus[i].x,bus[i].y);
    for (int i=1;i<=n;i++)v[i]=i;
    for (int i=1;i<=m;i++){
        if (sd[v[bus[i].x]]>sd[bus[i].lca])v[bus[i].x]=bus[i].lca;
        if (sd[v[bus[i].y]]>sd[bus[i].lca])v[bus[i].y]=bus[i].lca;
    }
    dfs3(1); 
    for (int i=1;i<=n;i++)v2[0][i]=v[i];
    for (int i=1;i<=19;i++)
        for (int j=1;j<=n;j++)
            v2[i][j]=v2[i-1][v2[i-1][j]];
    q=read();
    while (q--){
        x=read();y=read();
        int k=lca(x,y),ans=0;
        if (x==y){
            puts("0");
            continue;
        }
        if (sd[x]<sd[y])swap(x,y);
        for (int i=19;i>=0;i--)
            if (sd[v2[i][x]]>sd[k])x=v2[i][x],ans++;
        for (int i=19;i>=0;i--)
            if (sd[v2[i][y]]>sd[k])y=v2[i][y],ans++; 
        if (y==k){
            printf("%d\n",ans+1);
            continue;
        }            
        printf("%d\n",ans+2);  
    }        
}
int main(){
    freopen("bus.in","r",stdin);
    freopen("bus.out","w",stdout);
    n=read();
    for (int i=2;i<=n;i++){
        int x=read();
        fa[i]=x;
        jb(x,i);
        if (x==i-1)cnt1++;
        if (x==1)cnt2++;
    }
    m=read();
    if (cnt1==n-1)doit1();
    else if (cnt2==n-1)doit2();
    else if (n<=5000)doit3();
    return 0;
}
