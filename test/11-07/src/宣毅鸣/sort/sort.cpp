#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=100005;
int n,opt,v[N],cnt,a[N],tot,S;
int find(int x){
    if (v[x]==x)return x;
    return v[x]=find(v[x]);
}
void doit(int l,int r){         
    int p;
    if (opt==1)p=l;
    if (opt==2)p=r;
    if (opt==3)p=(l+r)/2;
    if (opt==4){
        S=S*48271%2147483647;
        p=S%(r-l+1)+l;       
    }
    v[p]=find(v[p]);
    a[v[p]]=++tot;
    v[p]=find(r);
    if (l!=r)doit(l,r-1);
}
signed main(){
    freopen("sort.in","r",stdin);
    freopen("sort.out","w",stdout);
    scanf("%lld%lld%lld",&n,&opt,&cnt);
    if (opt==1){
        printf("1 ");
        for (int i=2;i<=n;i++)printf("%lld ",n-i+2);
        return 0;
    }
    if (opt==2){
        for (int i=1;i<=n;i++)printf("%lld ",n-i+1);
        return 0;
    }
    if (opt==4)scanf("%lld",&S);
    for (int i=1;i<=n;i++)v[i]=i;
    doit(1,n);
    for (int i=1;i<=n;i++)printf("%lld ",a[i]);
    return 0;
}
