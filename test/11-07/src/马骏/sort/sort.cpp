#include <map>
#include <set>
#include <cmath>
#include <queue>
#include <vector>
#include <bitset>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100010
//#define MOD
#define int long long
using namespace std;
char gc(){static char buf[100000],*p1=buf+100000,*p2=p1;if(p1==p2)
{p1=buf;p2=buf+fread(buf,1,100000,stdin);}return *p1++;}
#define gc getchar
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+48);}
int read(){int d=0,w=1;char c=gc();for(;c<'0'||c>'9';c=gc())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=gc())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int n,mode,cnt,sd,a[maxn],now[maxn];
int rd()
{
	sd=sd*48271%2147483647;
	return sd;
}
signed main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();
	mode=read();
	cnt=read();
	if(mode==1||mode==2)
	{
		for(int i=1;i<=n;wrs(i++));
		return 0;
	}
	if(mode==3)
	{
		if(n&1)
		{
			for(int i=1;i<=n;i+=2)
				a[(n-1>>1)+(i+1>>1)]=i;
			for(int i=2;i<n;i+=2)
				a[i>>1]=i;
		}
		else
		{
			for(int i=2;i<=n;i+=2)
				a[(n>>1)+(i>>1)]=i;
			for(int i=1;i<n;i+=2)
				a[(n>>1)-(i>>1)]=i;
		}
		for(int i=1;i<=n;wrs(a[i++]));
		return 0;
	}
	sd=read();
	for(int i=1;i<=n;i++)
		now[i]=i;
	for(int i=1;i<=n;i++)
	{
		int tmp=rd()%(n-i+1)+i;
		a[now[tmp]]=i;
		swap(now[tmp],now[i]);
	}
	for(int i=1;i<=n;wrs(a[i++]));
	return 0;
}
