#include <map>
#include <set>
#include <cmath>
#include <queue>
#include <vector>
#include <bitset>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 110
#define maxm 200010
//#define MOD
//#define int long long
using namespace std;
char gc(){static char buf[100000],*p1=buf+100000,*p2=p1;if(p1==p2)
{p1=buf;p2=buf+fread(buf,1,100000,stdin);}return *p1++;}
#define gc getchar
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+48);}
int read(){int d=0,w=1;char c=gc();for(;c<'0'||c>'9';c=gc())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=gc())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
struct node
{
	int u,v;
	friend bool operator < (node a,node b)
	{
		return a.u<b.u||a.u==b.u&&a.v<b.v;
	}
};
node a[maxm];
int bb[maxn<<1][2],h[maxn],r,n,m,q,ma,cnt;
void add(int x,int y)
{
	bb[++r][0]=y;
	bb[r][1]=h[x];
	h[x]=r;
}
signed main()
{
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	for(int i=2;i<=n;i++)
	{
		int x=read();
		ma=max(ma,x);
		add(x,i);
		add(i,x);
	}
	if(ma==1)
	{
		cnt=m=read();
		for(int i=1;i<=m;i++)
		{
			a[i].u=read();
			a[i].v=read();
			if(a[i].u>a[i].v) swap(a[i].u,a[i].v);
			if(a[i].u!=1)
			{
				a[++cnt].u=1;
				a[cnt].v=a[i].u;
				a[++cnt].u=1;
				a[cnt].v=a[i].v;
			}
		}
		sort(a+1,a+cnt+1);
		q=read();
		for(int i=1;i<=q;i++)
		{
			int x=read(),y=read();
			if(x>y) swap(x,y);
			if(x==y)
			{
				wln(0);
				continue;
			}
			if(x==1)
			{
				node tmp;
				tmp.u=1;
				tmp.v=y;
				node *p=lower_bound(a+1,a+cnt+1,tmp);
				if(p->u==x&&p->v==y) wln(1);
					else wln(-1);
				continue;
			}
			node tmp;
			tmp.u=x;
			tmp.v=y;
			node *p=lower_bound(a+1,a+cnt+1,tmp);
			if(p->u==x&&p->v==y)
			{
				wln(1);
				continue;
			}
			tmp.u=1;
			tmp.v=x;
			p=lower_bound(a+1,a+cnt+1,tmp);
			if(p->u!=1||p->v!=x)
			{
				wln(-1);
				continue;
			}
			tmp.u=1;
			tmp.v=y;
			p=lower_bound(a+1,a+cnt+1,tmp);
			if(p->u!=1||p->v!=y)
			{
				wln(-1);
				continue;
			}
			wln(2);
		}
		return 0;
	}
	return 0;
}
