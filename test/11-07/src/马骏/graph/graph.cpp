#include <map>
#include <set>
#include <cmath>
#include <queue>
#include <vector>
#include <bitset>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 20
#define MOD 1000000007
#define int long long
using namespace std;
char gc(){static char buf[100000],*p1=buf+100000,*p2=p1;if(p1==p2)
{p1=buf;p2=buf+fread(buf,1,100000,stdin);}return *p1++;}
#define gc getchar
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+48);}
int read(){int d=0,w=1;char c=gc();for(;c<'0'||c>'9';c=gc())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=gc())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int n,p,a[maxn],c[maxn][maxn],b[maxn],f[maxn],ans;
int power(int x,int y,int mod)
{
	int ans=1,now=x;
	for(;y;y>>=1)
	{
		if(y&1) (ans*=now)%=mod;
		now=now*now%mod;
	}
	return ans;
}
void dfs2(int x,int y)
{
	if(x==n)
	{
		int cnt=0;
		for(int i=1;i<=n;i++)
		{
			f[i]=1;
			for(int j=1;j<i;j++)
				if(c[j][i]&&b[j]!=b[i]) f[i]+=f[j];
			cnt+=f[i];
		}
		if(cnt&1==p) ans++;
		return;
	}
	c[x][y]=0;
	int nx,ny;
	nx=x;
	ny=y+1;
	if(y+1>n)
	{
		nx=x+1;
		ny=nx+1;
	}
	dfs2(nx,ny);
	c[x][y]=1;
	dfs2(nx,ny);
}
void dfs(int k)
{
	if(k>n)
	{
		dfs2(1,2);
		return;
	}
	if(a[k]!=-1)
	{
		b[k]=a[k];
		dfs(k+1);
	}
	else
	{
		b[k]=0;
		dfs(k+1);
		b[k]=1;
		dfs(k+1);
	}
}
signed main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();
	p=read();
	for(int i=1;i<=n;a[i++]=read());
	if(n<=50)
	{
		dfs(1);
		write(ans);
	}
	else if((n&1)==p) write(power(2,n*(n-1)>>1,MOD));
		else write(0);
	return 0;
}
