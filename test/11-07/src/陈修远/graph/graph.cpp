#include <bits/stdc++.h>

using namespace std;

const int mo=1e9+7.1;

#define N 1000007
//#define int long long
#define ll long long
int n,p;
int col[N];
ll ans[N][2][2];
int ff[2];

int qp(int a,int b)
{
	if(b<=0) return 1;
	int t=qp(a,b/2);
	int ans=(b&1?a:1);
	return 1LL*ans*t%mo*t%mo;
}

void solve(int x,int c)
{
	if(x>n) return;
	if(c==-1){solve(x,0);solve(x,1);return;}
	int a,b;
	a=ff[!c];
	b=x-1-a;
	ff[c]=ff[c]+((1^ff[!c])&1);
	//ans[x][c][1]=(ans[x-1][0][0]+ans[x-1][1][0])*()+(ans[x-1][0][1]+ans[x-1][1][1])*()
	ans[x][c][1]=(ans[x-1][c][0]+ans[x-1][!c][0]*qp(2,a-1)%mo*qp(2,b)%mo+ans[x-1][!c][1]*(a==0?0:qp(2,a-1))%mo*qp(2,b)%mo)%mo;
	ans[x][c][0]=(ans[x-1][c][1]+ans[x-1][!c][1]*qp(2,a-1)%mo*qp(2,b)%mo+ans[x-1][!c][0]*(a==0?0:qp(2,a-1))%mo*qp(2,b)%mo)%mo;
	solve(x+1,col[x+1]);
}

int main()
{
	ios::sync_with_stdio(0);
	freopen("graph.in","r",stdin);freopen("graph.out","w",stdout);
	cin>>n>>p;
	for(int i=1;i<=n;++i) cin>>col[i];
	//teshu
	bool flag=1;
	if(col[1]!=-1){for(int i=2;i<=n;++i) if(col[i]!=col[i-1]){flag=0;break;}}
	else flag=0;
	if(flag)
	{
		if(n&1!=p)
		{
			cout<<0<<endl;
		}
		else
		{
			cout<<qp(2,n*(n-1)/2%mo)<<endl;
		}
		return 0;
	}
	if(col[1]==-1)
	{
		ff[0]=ff[1]=1;
		ans[1][0][1]=ans[1][1][1]=1;
	}
	else if(col[1]==0)
	{
		ff[0]=1;
		ans[1][0][1]=1;
	}
	else
	{
		ff[1]=1;
		ans[1][1][1]=1;
	}
	solve(2,col[2]);
	for(int i=1;i<=n;++i) cout<<ans[i][0][p]<<" "<<ans[i][0][!p]<<" "<<ans[i][1][p]<<" "<<ans[i][1][!p]<<endl;
	return 0;
}
