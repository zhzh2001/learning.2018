#include <bits/stdc++.h>

using namespace std;

#define N 100007

int a[N];
int n,f,r0,cnt;
const int mo=2147483647;
int bef[N];

int ra[N];
int rai=1;

int ran(int l,int r)
{
	r0=1LL*r0*48271%mo;
	return ra[rai++]=r0%(r-l+1)+l;
}

int F(int l,int r)
{
	if(f==3) return (l+r)/2;
	return ran(l,r);
}

int getbef(int pos)
{
	while(bef[pos]!=pos) pos=bef[pos];
	return pos;
}

void solve()
{
	if(f==1 || f==2)
	{
		for(int i=1;i<=n;++i)
			cout<<i<<" ";
		return;
	}
	for(int i=1;i<=n;++i) bef[i]=i;
	if(f==4) cin>>r0;
	int l=1,r=n;
	int p=0;
	while(++p<=n)
	{
		int pos=F(l++,r);
		if(bef[pos]==pos)
		{
			a[pos]=p;
			bef[pos]=p;
		}
		else
		{
			int t=getbef(pos);
			a[t]=p;
			bef[t]=p;
		}
	}
}
/*
int ct=0;
void st(int l,int r)
{
	int i=l,j=r;
	r0=1541905871;
	int x=a[F(l,r)];
	do{
		while(a[i]<x) ++i,++ct;
		while(x<a[j]) --j,++ct;
		if(i<=j) swap(a[i],a[j]),i++,j--,ct+=2;
	}while(i<=j);
	if(l<j) st(l,j);
	if(i<r) st(i,r);
}
*/
int main()
{
	ios::sync_with_stdio(0);
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	cin>>n>>f>>cnt;
	solve();
	//for(int i=1;i<=n;++i)
	//	cout<<ra[i]<<" ";
	//cout<<endl;
	for(int i=1;i<=n;++i)
		cout<<a[i]<<" ";
	cout<<endl;
	//st(1,n);
	//cout<<endl<<ct;
	return 0;
}
