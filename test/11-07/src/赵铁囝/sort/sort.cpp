#include <bits/stdc++.h>

#define Max 100005
#define ll long long

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int n,t,cnt,sed,a[Max],ans[Max];

inline int rnd(){
	sed=(ll)sed*48271%2147283647;
	return sed;
}

inline int f(int l,int r){
//	cout<<l<<" "<<r<<endl;
	if(t==1)return l;
	if(t==2)return r;
	if(t==3)return (l+r)/2;
	if(t==4)return rnd()%(r-l+1)+l;
}

/*inline void sort(int l,int r){
	int i=l,j=r;
	int x=a[f(l,r)];
	do{
		while(a[i]<x)i++;
		while(x<a[j])j--;
		if(i<=j){
			swap(a[i],a[j]);
			i++;
			j--;
		}
	}while(i<=j);
	if(l<j)sort(l,j);
	if(i<r)sort(i,r);
}*/

inline void solve(int l,int r,int now){
	int x=f(l,r);
	ans[a[x]]=now;
	if(l==r)return;
	a[x]=a[l];
	solve(l+1,r,now+1);
	return;
}

int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();t=read();cnt=read();
	if(t==4){
		sed=read();
	}
	for(int i=1;i<=n;i++)a[i]=i;
	solve(1,n,1);
	for(int i=1;i<=n;i++){
		write(ans[i]);putchar(' ');
	}
	return 0;
}
