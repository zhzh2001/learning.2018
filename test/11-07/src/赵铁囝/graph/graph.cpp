#include <bits/stdc++.h>

#define ll long long
#define Max 1000005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

const int wzp=1e9+7;

int n,ans,flag,f[100],a[Max],b[1000][1000];

inline int pow(int x,int y){
	int ans=1;
	while(y){
		if(y&1)ans=(ll)ans*x%wzp;
		x=(ll)x*x%wzp;
		y>>=1;
	}
	return ans;
}

inline bool check(){
	memset(f,0,sizeof f);
	int sum=0;
	for(int i=1;i<=n;i++){
		f[i]=1;
		for(int j=1;j<i;j++){
			if(b[j][i]){
				f[i]=(f[i]+f[j])%wzp;
			}
		}
		sum+=f[i];
	}
	return (sum&1)==flag;
}

inline void dfs2(int l,int r){
	if(r==n+1){
		if(check()){
			int c=0,b=0;
			for(int i=1;i<=n;i++){
				if(a[i]==1)c++;
				if(a[i]==0)b++;
			}
			ans=(ans+pow(2,c*(c-1)/2+b*(b-1)/2))%wzp;
		}
		return;
	}
	if(a[l]==a[r]){
		b[l][r]=0;
		if(r==n){
			dfs2(l+1,l+2);
		}else{
			dfs2(l,r+1);
		}
	}else{
		b[l][r]=0;
		if(r==n){
			dfs2(l+1,l+2);
		}else{
			dfs2(l,r+1);
		}
		b[l][r]=1;
		if(r==n){
			dfs2(l+1,l+2);
		}else{
			dfs2(l,r+1);
		}
	}
	return;
}

inline void dfs(int x){
	if(x==n+1){
		dfs2(1,2);
		return;
	}
	if(a[x]==-1){
		a[x]=1;
		dfs(x+1);
		a[x]=0;
		dfs(x+1);
		a[x]=-1;
	}else{
		dfs(x+1);
	}
}

int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();flag=read();
	for(int i=1;i<=n;i++)a[i]=read();
	dfs(1);
	writeln(ans);
	return 0;
}
