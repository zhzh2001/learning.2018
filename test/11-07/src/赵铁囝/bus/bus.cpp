#include <bits/stdc++.h>

#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Edge{
	int v,to;
}e[Max*2];

struct Segtree{
	int l,r,lazy,minn,maxn,sum;
}st[Max*4];

int n,m,q,l,r,lca,cnt,size,a[Max],w[Max],id[Max],hs[Max],fa[Max],dep[Max],top[Max],son[Max],head[Max];

inline void add(int u,int v){
	e[++size].v=v;e[size].to=head[u];head[u]=size;
}

inline void dfs(int u,int last){
	son[u]=1;
	hs[u]=0;
	for(int i=head[u];i;i=e[i].to){
		int v=e[i].v;
		if(v==last)continue;
		dfs(v,u);
		son[u]+=son[v];
		if(son[v]>son[hs[u]]){
			hs[u]=v;
		}
	}
	return;
}

inline void dfs2(int u,int last,int topf){
	id[u]=++cnt;
	w[cnt]=u;
	fa[cnt]=id[last];
	dep[cnt]=dep[fa[cnt]]+1;
	if(!topf)topf=u;
	top[cnt]=topf;
	if(hs[u])dfs2(hs[u],u,topf);
	for(int i=head[u];i;i=e[i].to){
		int v=e[i].v;
		if(v==last||v==hs[u])continue;
		dfs2(v,u,0);
	}
	return;
}

inline int LCA(int x,int y){
	while(top[x]!=top[y]){
		if(dep[top[x]]<dep[top[y]])swap(x,y);
		x=fa[top[x]];
	}
	if(dep[x]>dep[y])swap(x,y);
	return x;
}

inline void build(int node,int l,int r){
	st[node].l=l;st[node].r=r;
	if(l==r){
		st[node].sum=1;
		st[node].minn=n;
		st[node].lazy=1e9;
		return;
	}
	int mid=(l+r)>>1;
	build(node<<1,l,mid);
	build(node<<1|1,mid+1,r);
	return;
}

inline void up(int node){
	st[node].minn=min(st[node<<1].minn,st[node<<1|1].minn);
	st[node].maxn=max(st[node<<1].maxn,st[node<<1|1].maxn);
	if(st[node<<1].maxn==st[node<<1|1].minn){
		st[node].sum=st[node<<1].sum+st[node<<1|1].sum-1;
	}else{
		st[node].sum=st[node<<1].sum+st[node<<1|1].sum;
	}
	return;
}

inline void down(int node){
	st[node<<1].lazy=min(st[node].lazy,st[node<<1].lazy);
	st[node<<1].minn=min(st[node].lazy,st[node<<1].minn);
	st[node<<1].maxn=min(st[node].lazy,st[node<<1].maxn);
	st[node<<1|1].lazy=min(st[node].lazy,st[node<<1|1].lazy);
	st[node<<1|1].minn=min(st[node].lazy,st[node<<1|1].minn);
	st[node<<1|1].maxn=min(st[node].lazy,st[node<<1|1].maxn);
	return;
}

inline void change(int node,int l,int r,int k){
	if(st[node].l>r||st[node].r<l)return;
	if(st[node].l>=l&&st[node].r<=r){
		st[node].minn=min(st[node].minn,k);
		st[node].maxn=min(st[node].maxn,k);
		st[node].lazy=min(st[node].lazy,k);
		return;
	}
	down(node);
	change(node<<1,l,r,k);
	change(node<<1|1,l,r,k);
	up(node);
	return;
}

inline void change_tree(int x,int y,int k){
	while(top[x]!=top[y]){
		if(dep[top[x]]<dep[top[y]])swap(x,y);
		change(1,top[x],x,k);
		x=fa[top[x]];
	}
	if(dep[x]>dep[y])swap(x,y);
	change(1,x,y,k);
}

inline int query_min(int node,int l,int r){
	if(st[node].l>r||st[node].r<l)return 1e9;
	if(st[node].l>=l&&st[node].r<=r)return st[node].minn;
	down(node);
	return min(query_min(node<<1,l,r),query_min(node<<1|1,l,r));
}

inline int query_max(int node,int l,int r){
	if(st[node].l>r||st[node].r<l)return 0;
	if(st[node].l>=l||st[node].r<=r)return st[node].maxn;
	down(node);
	return max(query_max(node<<1,l,r),query_max(node<<1|1,l,r));
}

inline int query_sum(int node,int l,int r){
	if(st[node].l>r||st[node].r<l)return 0;
	if(st[node].l>=l&&st[node].r<=r)return st[node].sum;
	down(node);
	return query_sum(node<<1,l,r)+query_sum(node<<1|1,l,r);
}

inline int calc(int x,int lca){
	int l=lca,r=x,mid,ans=lca;
	if(query_max(1,lca,x)==1e9)return 1e9;
	while(l<=r){
		mid=(l+r)>>1;
		if(query_min(1,mid,mid)<=dep[lca]){
			ans=mid;
			r=mid-1;
		}else{
			l=mid+1;
		}
	}
	return query_sum(1,ans,x);
}

inline int query_tree(int x,int y){
	int ans=0,lca=LCA(x,y);
	ans=calc(x,lca)+calc(y,lca);
	if(ans>1e9)return -1;
	return ans;
}

int main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	for(int i=2;i<=n;i++){
		a[i]=read();
		add(a[i],i);
		add(i,a[i]);
	}
	dfs(1,1);
	dfs2(1,1,1);
	build(1,1,n);
	m=read();
	for(int i=1;i<=m;i++){
		l=id[read()];r=id[read()];
		lca=LCA(l,r);
		change_tree(l,r,dep[lca]);
	}
	q=read();
	for(int i=1;i<=q;i++){
		l=id[read()];r=id[read()];
		writeln(query_tree(l,r));
	}
	return 0;
}
