#include<bits/stdc++.h>
#define int long long
using namespace std;
struct Edge{
	int color,sum,fathers;
	int father[21];
}a[1000001];
int n,p,sum,ans;
int mp[21][21];
void search_line(int x,int y){
	if(x>=n){
		sum=0;
		for(int i=1;i<=n;i++)
			a[i].sum=1;
		for(int i=2;i<=n;i++)
			for(int j=1;j<=i;j++)
				if(mp[i][j]&&a[i].color!=a[j].color)
					a[i].sum+=a[j].sum;
		for(int i=1;i<=n;i++)
			sum+=a[i].sum;
		if(sum%2==p)
			ans++;
		return;
	}
	mp[x][y]=mp[y][x]=1;
	if(y+1<=n)
		search_line(x,y+1);
	else
		search_line(x+1,x+2);
	mp[x][y]=mp[y][x]=0;
	if(y+1<=n)
		search_line(x,y+1);
	else
		search_line(x+1,x+2);
}
void search_point(int x){
	if(x>n){
		search_line(1,2);
		return;
	}
	if(a[x].color==-1){
		a[x].color=1;
		search_point(x+1);
		a[x].color=0;
		search_point(x+1);
		return;
	}
	search_point(x+1);
	return;
}
signed main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	scanf("%lld%lld",&n,&p);
	for(int i=1;i<=n;i++)
		scanf("%lld",&a[i].color);
	if(n>20){
		if(n%2!=p)
			printf("0");
		else
			printf("1");
		return 0;
	}
	search_point(1);
	printf("%lld",ans);
	return 0;
}
