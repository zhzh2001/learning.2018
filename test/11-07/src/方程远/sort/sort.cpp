#include<bits/stdc++.h>
using namespace std;
int n,type,cnt;
int a[100001];
void qsort(int l,int r){
	int i=l;
	int j=r;
	do{
		swap(a[i],a[j]);
		i++;
		j--;
	}while(i<=j);
	if(l<j)
		qsort(l,j);
	if(i<r)
		qsort(i,r);
	return;
}
signed main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout); 
	scanf("%d%d%d",&n,&type,&cnt);
	for(int i=1;i<=n;i++)
		a[i]=i;
	qsort(1,n);
	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
	return 0; 
}
