#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<stdio.h>
#include<fstream>
using namespace std;
ifstream fin("sort.in");
ofstream fout("sort.out");
inline long long read(){
	long long k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(long long x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
long long n,opt,cnt,num[100003],chan[100003];
inline void solve(){
	if(opt==2){
		for(register int i=1;i<=n;++i)chan[i]=i-1;
	}else if(opt==3){
		for(register int i=1;i<=n;++i)chan[i]=(1+i)/2-1;
	}else if(opt==4){
		chan[n+1]=read();
		for(register int i=n;i;--i){
			chan[i]=(long long)chan[i+1]*(long long)48271%2147483647;
		}
	}
}
inline void change(){
	for(register int i=1;i<=n;++i){
		num[i]=i;
	}
	for(register int i=1;i<=n;++i){
		swap(num[(chan[i])%(i-1+1)+1],num[i]);
	}
}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(),opt=read(),cnt=read();
	solve(),change();
	for(register int i=1;i<=n;++i){
		write(num[i]),putchar(' ');
	}
	return 0;
}
/*

in:
10 4 45
1541905871

*/
