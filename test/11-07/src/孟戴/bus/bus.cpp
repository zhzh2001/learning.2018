#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<stdio.h>
#include<fstream>
//#define cin fin
//#define cout fout
using namespace std;
ifstream fin("bus.in");
ofstream fout("bus.out");
inline int max(int &x,int &y){return x>y?x:y;}
inline int min(int &x,int &y){return x<y?x:y;}
inline int read(){
	int x=0;char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=(x<<3)+(x<<1)+ch-48,ch=getchar();
	return x;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x),puts("");
}
int n,m,q,p[300003];
int head[300003],to[500003],nxt[500003],tot;
inline void add(int u,int v){
	to[++tot]=v,nxt[tot]=head[u],head[u]=tot;
}
int dep[300003],in[300003],out[300003],tim,top[300003];
int sz[300003],son[300003];
int f[300003][23],st[300003][23];
int u[300003],v[300003];
inline int lca(int x,int y){
	if(dep[x]>dep[y])swap(x,y);
	for(register int i=20;i>=0;i--){
		if(dep[f[y][i]]>=dep[x])y=f[y][i];
	}
	if(x==y)return x;
	for(register int i=20;i>=0;i--){
		if(f[y][i]!=f[x][i])x=f[x][i],y=f[y][i];
	}
	return f[x][0];
}
void dfs(int x,int fa){
	f[x][0]=fa;
	dep[x]=dep[fa]+1;
	sz[x]=1;
	for(register int i=head[x];i;i=nxt[i]){
		if(to[i]!=fa){
			dfs(to[i],x);
			sz[x]+=sz[to[i]];
			if(!son[x]||sz[to[i]]>sz[son[x]]){
				son[x]=to[i];
			}
		}
	}
}
void dfs2(int x,int tp){
	in[x]=++tim;
	top[x]=tp;
	if(son[x]){
		dfs2(son[x],tp);
	}
	for(register int i=head[x];i;i=nxt[i]){
		if((to[i]^f[x][0])&&(to[i]^son[x])){
			dfs2(to[i],to[i]);
		}
	}
	out[x]=tim;
}
struct tree{
	int mn,tag;
}t[1000003];
inline void pushdown(int rt){
	if(t[rt].tag^10000000){
		t[rt<<1].tag=min(t[rt].tag,t[rt<<1].tag);
		t[rt<<1|1].tag=min(t[rt].tag,t[rt<<1|1].tag);
		t[rt<<1].mn=min(t[rt].tag,t[rt<<1].mn);
		t[rt<<1|1].mn=min(t[rt].tag,t[rt<<1|1].mn);
		t[rt].tag=10000000;
	}
}
void build(int rt,int l,int r){
	t[rt].mn=10000000,t[rt].tag=10000000;
	if(l==r){
		return;
	}
	int mid=(l+r)>>1;
	build(rt<<1,l,mid),build(rt<<1|1,mid+1,r);
}
void update(int rt,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
		t[rt].tag=min(t[rt].tag,val);
		t[rt].mn=min(t[rt].mn,val);
		return;
	}
	pushdown(rt);
	int mid=(l+r)>>1;
	if(y<=mid){
		update(rt<<1,l,mid,x,y,val);
	}else if(x>mid){
		update(rt<<1|1,mid+1,r,x,y,val);
	}else{
		update(rt<<1,l,mid,x,mid,val),update(rt<<1|1,mid+1,r,mid+1,y,val);
	}
}
int query(int rt,int l,int r,int x){
	if(l==r){
		return t[rt].mn;
	}
	pushdown(rt);
	int mid=(l+r)>>1;
	if(x<=mid){
		return query(rt<<1,l,mid,x);
	}else{
		return query(rt<<1|1,mid+1,r,x);
	}
}
void upd(int x,int y,int val){
	while(top[x]!=top[y]){
		if(dep[x]<dep[y]){
			swap(x,y);
		}
		update(1,1,tim,in[top[x]],in[x],val);
		x=f[top[x]][0];
	}
	if(dep[x]>dep[y]){
		swap(x,y);
	}
	update(1,1,tim,in[x],in[y],val);
}
struct mod{
	int pos,tag,x,y;
}mdf[800003];
int tot2,tot1;
struct quer{
	int pos,x,y,ans1,ans2,id,tag;
}que[1000003];
int anss[400003],tott[400003];
inline bool cmp1(quer a,quer b){
	return a.pos<b.pos;
}
inline bool cmp2(mod a,mod b){
	return a.pos<b.pos;
}
int tree[400003];
inline void update(int x,int val){
	for(;x<=tim+1;x+=x&(-x)) tree[x]+=val;
}
inline int query(int x){
	int ret=0;
	if(x==0)return 0;
	for(;x;x-=x&(-x)) ret+=tree[x];
	return ret;
}
int main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	for(register int i=1;i<=n-1;++i){
		p[i]=read();
		add(p[i],i+1),add(i+1,p[i]);
	}
	dfs(1,0);
	dfs2(1,1);
	f[1][0]=1;
	for(register int j=1;j<=20;++j){
		for(register int i=1;i<=n;++i){
			f[i][j]=f[f[i][j-1]][j-1];
		}
	}
	build(1,1,tim);
	m=read();
	for(register int i=1;i<=m;++i){
		u[i]=read(),v[i]=read();
		int lc=lca(u[i],v[i]);
		upd(u[i],v[i],dep[lc]);
		if(lc!=u[i]&&lc!=v[i]){
			mdf[++tot2].pos=in[u[i]],mdf[tot2].x=in[v[i]],mdf[tot2].tag=1;
			mdf[++tot2].pos=in[v[i]],mdf[tot2].x=in[u[i]],mdf[tot2].tag=1;
		}
	}
	for(register int i=1;i<=n;++i){
		st[i][0]=query(1,1,tim,in[i]);
		int now=i;
		for(register int j=20;j>=0;--j){
			if(dep[f[now][j]]>=st[i][0]){
				now=f[now][j];
			}
		}
		st[i][0]=now;
	}
	for(register int j=1;j<=20;++j){
		for(register int i=1;i<=n;++i){
			st[i][j]=st[st[i][j-1]][j-1];
		}
	}
	q=read();
	for(register int i=1;i<=q;++i){
		int x,y,lc,step1=0,step2=0;
		x=read(),y=read();
		lc=lca(x,y);
		if(dep[st[x][20]]>dep[lc]||dep[st[y][20]]>dep[lc]){
			anss[i]=-1;
			tott[i]=-1;
			continue;
		}
		for(register int j=20;j>=0;--j){
			if(dep[st[x][j]]>dep[lc])x=st[x][j],step1+=(1<<j);
		}
		for(register int j=20;j>=0;--j){
			if(dep[st[y][j]]>dep[lc])y=st[y][j],step2+=(1<<j);
		}
		if(lc==x||lc==y){
			anss[i]=step1+step2+1;
			tott[i]=-1;
			continue;
		}
		que[++tot1].pos=in[x]-1,que[tot1].x=in[y],que[tot1].y=out[y],que[tot1].ans1=step1+step2,que[tot1].id=i,que[tot1].ans2=0,que[tot1].tag=-1;
		que[++tot1].pos=out[x],que[tot1].x=in[y],que[tot1].y=out[y],que[tot1].ans1=step1+step2,que[tot1].id=i,que[tot1].ans2=0,que[tot1].tag=1;
		que[++tot1].pos=in[y]-1,que[tot1].x=in[x],que[tot1].y=out[x],que[tot1].ans1=step1+step2,que[tot1].id=i,que[tot1].ans2=0,que[tot1].tag=-1;
		que[++tot1].pos=out[y],que[tot1].x=in[x],que[tot1].y=out[x],que[tot1].ans1=step1+step2,que[tot1].id=i,que[tot1].ans2=0,que[tot1].tag=1;
	}
	sort(que+1,que+tot1+1,cmp1);
	sort(mdf+1,mdf+tot2+1,cmp2);
	int now=1;
	for(register int i=1;i<=tot1;++i){
		while(mdf[now].pos<=que[i].pos&&now<=tot2){
			update(mdf[now].x,mdf[now].tag);
			++now;
		}
		que[i].ans2=query(que[i].y)-query(que[i].x-1);
	}
	for(register int i=1;i<=tot1;++i){
		tott[que[i].id]+=que[i].ans2*que[i].tag;
	}
	for(register int i=1;i<=tot1;++i){
		anss[que[i].id]=que[i].ans1;
	}
	for(register int i=1;i<=q;i++){
		if(tott[i]>0){
			writeln(anss[i]+1);
		}else if(tott[i]==-1){
			writeln(anss[i]);
		}else{
			writeln(anss[i]+2);
		}
	}
	return 0;
}
/*
5
1 2 1 4
1
3 5
1
2 4

in:
7
1 1 1 4 5 6
4
4 2
5 4
1 3
6 7
5
4 5
3 5
7 2
3 2
5 3

out:
1
3
-1
2
3

*/
