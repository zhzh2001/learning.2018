#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0'; ch = getchar();}
	return x*f;
}


//用接口更新答案，同时再用其它接口更新新的接口

const int mod = 1e9+7;
int n,p;
int dp[2][2][2][2];// [0][1]
int Dp[2][2][2][2];
int DP[2][2][2][2];

inline void update(int &x,int y){
	x += y; if(x >= mod) x -= mod;
}

signed main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n = read(); p = read();
	int x = read();
	if(x != 1){ //x == 0 || x== -1
		dp[1][0][1][0] = 1;
		dp[1][0][0][0] = 1;
	}
	if(x != 0){ //x == 1 || x == -1;
		dp[0][1][0][1] = 1;
		dp[0][1][0][0] = 1;
	}
	for(int i = 2;i <= n;++i){
		x = read();
		memset(Dp,0,sizeof Dp);
		memset(DP,0,sizeof DP);
		for(int a = 0;a < 2;++a)
			for(int b = 0;b < 2;++b)
				for(int c = 0;c < 2;++c)
					for(int d = 0;d < 2;++d){   //用dp[a][b][c][d]来更新
						if(dp[a][b][c][d])
						for(int e = 0;e < 2;++e)
							for(int f = 0;f < 2;++f){
								if(x != 1){
									update(Dp[a^d^1][b][e][f],dp[a][b][e][f]);
									update(Dp[a^d^1][b][e^d^1][f],dp[a][b][e][f]);
								}
								if(x != 0){
									update(Dp[a^c^1][b][e][f],dp[a][b][e][f]);
									update(Dp[a^c^1][b][e][f^c^1],dp[a][b][e][f]);
								}
							}
					}
		for(int a = 0;a < 2;++a)
			for(int b = 0;b < 2;++b)
				for(int c = 0;c < 2;++c)
					for(int d = 0;d < 2;++d)
						dp[a][b][c][d] = Dp[a][b][c][d];
	}
	int ans = 0;
	for(int a = 0;a < 2;++a)
		for(int b = 0;b < 2;++b) if((a^b^p) == 0){
			for(int c = 0;c < 2;++c)
				for(int d = 0;d < 2;++d)
					update(ans,dp[a][b][c][d]);
//			if(dp[a][b][0][0] > 0) update(ans,dp[a][b][0][0]);
//			else if(dp[a][b][0][1] > 0) update(ans,dp[a][b][0][1]);
//			else if(dp[a][b][1][0] > 0) update(ans,dp[a][b][1][0]);
//			else if(dp[a][b][1][1] > 0) update(ans,dp[a][b][1][1]);

		}
//	ans
	printf("%d\n",ans);
	return 0;
}
