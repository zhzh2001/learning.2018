#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0'; ch = getchar();}
	return x*f;
}

const int N = 100005;
int n,type,cnt;
int Ram;
int a[N];
int num;

inline int getRa(int l,int r){
	Ram = 1LL * Ram * 48271 % 2147483647LL;
	return Ram%(r-l+1)+l;
}

void solve1(int l,int r){
	int x = (l+r)>>1;
	cnt -= (r-l);
	if(cnt >= 0) solve1(l+1,r);
	swap(a[x],a[l]);
}

void solve2(int l,int r){
	int x = getRa(l,r);
	cnt -= (r-l);
	if(cnt >= 0) solve2(l+1,r);
	swap(a[x],a[l]);
}

signed main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n = read(); type = read(); cnt = read();
	if(type == 1){
		for(int i = 1;i <= n;++i)
			printf("%d ",i);
		puts("");
		return 0;
	}else if(type == 2){
		for(int i = 1;i <= n;++i)
			printf("%d ",i);
		puts("");
		return 0;
	}else if(type == 3){
		for(int i = 1;i <= n;++i) a[i] = i;
		solve1(1,n);
		for(int i = 1;i <= n;++i)
			printf("%d ",a[i]);
		puts(""); return 0;
	}else{
		Ram = read();
		for(int i = 1;i <= n;++i) a[i] = i;
		solve2(1,n);
		for(int i = 1;i <= n;++i)
			printf("%d ",a[i]);
		puts(""); return 0;
	}
	return 0;
}
