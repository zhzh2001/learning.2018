#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0'; ch = getchar();}
	return x*f;
}

typedef pair<int,int> pii;
const int N = 200005;
int n,m,q;
int fa[N];
int to[N*2],nxt[N*2],head[N],cnt;
int dp[N];
//int que[N],head,tail;
struct node{
	int x,y;
}Bus[N];
int Busnum;
int ans[N];
struct payh{
	int x,y;
}path[N];
int pathnum;
set<pii > S;
bool vis[N];
int dep[N];

inline bool cmp(node x,node y){
	if(x.x != y.x) return x.x < y.x;
	return x.y < y.x;
}
inline bool cmp2(node a,node b){
	if(a.x != b.x) return a.x < b.x;
	return a.y < b.y;
}

inline void insert(int x,int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}



signed main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n = read(); m = read(); q = read();
	bool flag1 = false,flag2 = false;
	for(int i = 2;i <= n;++i){
		fa[i] = read();
		insert(fa[i],i);
		insert(i,fa[i]);
		if(fa[i] != i-1) flag1 = true;
		if(fa[i] != 1) flag2 = true;
	}
	if(!flag2){
		for(int i = 1;i <= m;++i){
			int x = read(),y = read();
			if(x > y) swap(x,y);
			if(x != 1){
				S.insert(make_pair(x,y));
				vis[x] = vis[y] = true;
			}
			else vis[y] = true;
		}
		for(int i = 1;i <= q;++i){
			int x = read(),y = read();
			if(x == y){
				puts("0");
				continue;
			}
			if(x > y) swap(x,y);
			if(x != 1){
				if(S.count(make_pair(x,y))) puts("1");
				else{
					if(vis[x] && vis[y]) puts("2");
					else puts("-1");
				}
			}else{
				if(vis[y]) puts("1");
				else puts("-1");
			}
		}
		return 0;
	}

	return 0;
}
