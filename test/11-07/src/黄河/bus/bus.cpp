#include<cstdio>
#include<cctype>
#include<iostream>
#include<string>
#include<cstring>
#include<algorithm>
#include<map>
#define re register
#define rep(x,a,b) for (re int x=(int)(a);x<=(int)(b);++x)
#define drp(x,a,b) for (re int x=(int)(a);x>=(int)(b);--x)
#define LL long long
#define inf 0x3f3f3f3f
using namespace std;
const int mo=1e9+7;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int n,m,q,x,y;
map<LL,bool> bo,boo;
int main(){
	freopen("bus.in","r",stdin);
	freopen("bus.out","w",stdout);
	n=read();
	rep(i,2,n) x=read();//,add(i,x),add(x,i);
	m=read();
	rep(i,1,m){
		int x=read(),y=read();
		boo[x]=boo[y]=boo[1]=1;
		LL HH=(x*x%mo*x%mo+y*y%mo*y%mo)%mo;
		bo[HH]=1;
	}
	q=read();
	rep(i,1,q){
		int x=read(),y=read();
		if (!boo[x]||!boo[y]) {
			printf("-1\n");continue;
		}
		LL HH=(x*x%mo*x%mo+y*y%mo*y%mo)%mo;
		if (bo[HH]) printf("1\n");else printf("2\n");
	}
	return 0;
}
