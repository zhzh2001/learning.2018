#include<cstdio>
#include<cctype>
#include<iostream>
#include<string>
#include<cstring>
#include<algorithm>
#define re register
#define rep(x,a,b) for (re int x=(int)(a);x<=(int)(b);++x)
#define drp(x,a,b) for (re int x=(int)(a);x>=(int)(b);--x)
#define LL long long
#define inf 0x3f3f3f3f
using namespace std;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N=1010;
const int mo=1e9+7;
int Color[N],x[N],y[N],Id[N],To[N<<1],Next[N<<1],head[N],cnt,tot,ans,n,p,num;
LL m;
inline void add (int u,int v){
	To[++cnt]=v;
	Next[cnt]=head[u];
	head[u]=cnt;
}
inline void dfs(int u,int fa){
	for (int i=head[u];i;i=Next[i]){
		int v=To[i];
		if (Color[v]==Color[u]) continue;
		tot++;
		dfs(v,u);
	}
}
inline LL qpow(LL k,LL base){
	LL res=1;
	for (;base;base>>=1,k=k*k%mo) if (base&1) res=res*k%mo;
	return res;
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(),p=read();
	m=n*(n-1)/2;
	bool flag=1;
	rep(i,1,n) {
		Color[i]=read();
		if ((i!=1&&Color[i]!=Color[i-1])||(Color[i]!=-1)) flag=0;
		if (Color[i]==-1){
			num++;Id[num]=i;
		}
	}
	if (flag&&n!=1) {
		if (n%2!=p) {
			printf("0\n");return 0;
		}else{
			LL res=qpow(2ll,m)%mo;
			printf("%lld\n",res);
			return 0;
		}
	}
//	printf("%d\n",num);
	rep(i,0,((1<<num)-1)){
		rep(j,1,num) Color[Id[j]]=i&(1<<(j-1));
		tot=0;
		rep(j,1,n-1)rep(k,j+1,n) x[++tot]=j,y[tot]=k;
		tot=0;
		rep(j,0,((1<<m)-1)){
			memset(head,0,sizeof(head));
			memset(To,0,sizeof(To));
			memset(Next,0,sizeof(Next));
			cnt=0;
			rep(k,1,m) if (j&(1<<(k-1))) add(x[k],y[k]);
			tot=n;
			rep(k,1,n) 
				dfs(k,0);
			if (tot%2==p) {
				ans++;
			//	printf("%d\n",i);
			//	printf("%d\n",tot);
			//	rep(k,1,n) if (j&(1<<(k-1))) printf("%d %d\n",x[k],y[k]);
			}
		}
	}
	printf("%d\n",ans);
	return 0;
}
