#include<cstdio>
#include<cctype>
#include<iostream>
#include<string>
#include<cstring>
#include<algorithm>
#define re register
#define rep(x,a,b) for (re int x=(int)(a);x<=(int)(b);++x)
#define drp(x,a,b) for (re int x=(int)(a);x>=(int)(b);--x)
#define LL long long
#define inf 0x3f3f3f3f
using namespace std;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N=100010;
int Id[N],ans[N],n,m,j,Type,cnt;
LL x;
int Solve(int l,int r){
	if(Type==1) return l;
	if(Type==2) return r;
	if(Type==3) return (l+r)/2;
	x=(x*48271*1ll)%2147483647;
	return x%(r-l+1)+l;
}
inline void swap(int &a,int &b){int t=a; a=b; b=t;}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(),Type=read(),cnt=read();
	if (Type==4) x=read();
	rep(i,1,n) Id[i]=i;
	rep(i,1,n){
		int j=Solve(i,n);
		ans[Id[j]]=i;
		swap(Id[i],Id[j]);
	}
	rep(i,1,n) printf("%d ",ans[i]);
	return 0;
}
