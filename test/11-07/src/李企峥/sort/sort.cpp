#include<bits/stdc++.h>
#define ll long long
#define int long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
#define pb push_back
using namespace std;
bool flag,ff[20];
int a[20],b[20];
int n,cnt,zhongzi,suiji,type;
int ans;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
    return x*f;
}
int random(int l,int r)
{
	suiji=suiji*48271%2147483647;
	return suiji%(r-l+1)+l;
}
int f(int l,int r)
{
	if(type==1)return l;
	if(type==2)return r;
	if(type==3)return (l+r)/2;
	return random(l,r);
}
void qsort(int l,int r)
{
	int i=l;
	int j=r;
	int x=a[f(l,r)];
	while(true)
	{
		while(a[i]<x)
		{
			i++;
			ans++;
		}
		while(x<a[j])
		{
			j--;
			ans++;
		}
		if(i<=j)
		{
			swap(a[i],a[j]);
			i++;j--;
			ans+=2;
		}
		if(i>j)break;
	}
	if(l<j)qsort(l,j);
	if(i<r)qsort(i,r);
}
void dfs(int x)
{
	if(x==n+1)
	{
		ans=0;
		if(type==4)suiji=zhongzi;
		For(i,1,n)a[i]=b[i];
		qsort(1,n);
		if(ans>cnt)
		{
			flag=true;
		}
		return;
	}
	For(i,1,n)
	{
		if(ff[i])continue;
		b[x]=i;
		ff[i]=true;
		dfs(x+1);
		if(flag)return;
		ff[i]=false;
	}
	return;
}
signed main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();type=read();cnt=read();
	if(type==4)zhongzi=read();
	if(n<=10)
	{
		memset(ff,false,sizeof(ff));
		flag=false;
		dfs(1);
		For(i,1,n)cout<<b[i]<<" ";
		return 0;
	}
	For(i,1,n)cout<<i<<" ";
	return 0;
}
