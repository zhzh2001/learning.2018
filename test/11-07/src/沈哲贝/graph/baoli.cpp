#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define rep(i,x,y)  for(ll i=x;i<y;++i)
#define Add(x,y)    ((x)=((x)+(y))%mod)
#define Mul(x,y)    ((x)=((x)*(y))%mod)
#define pa pair<ll,ll>
#define mk make_pair
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
    ll x=0,f=1; char ch=getchar();
    for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
    for(;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
    return x*f;
}
void write(ll x){
    if (x<0)putchar('-'),x=-x;
    if (x>=10)write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);puts("");
}
const ll mod=1e9+7;
ll f[2][2][2][2],g[2][2][2][2],n,p,ans;
ll ppow(ll x,ll k){
    ll ans=1;
    for(;k;k>>=1,Mul(x,x)){
        if (k&1)Mul(ans,x);
    }
    return ans;
}
void Mk1(ll n){
    For(x,0,1)For(y,0,1)For(a,0,1){
        if (n>1){
            Add(g[x^1][y][1][1],f[x][y][a][1]*ppow(2,n-2));
            Add(g[x  ][y][a][1],f[x][y][a][1]*ppow(2,n-2));
        }   Add(g[x^1][y][1][0],f[x][y][a][0]*ppow(2,n-1));
    }
/*    For(a,0,1)For(b,0,1)For(c,0,1)For(d,0,1){
        write(f[n][a][b][c][d]),putchar(' ');
    }puts("");*/
}
void Mk2(ll n){
    For(x,0,1)For(y,0,1)For(b,0,1){
        if (n>1){
            Add(g[x][y^1][1][1],f[x][y][1][b]*ppow(2,n-2));
            Add(g[x][y  ][1][b],f[x][y][1][b]*ppow(2,n-2));
        }   Add(g[x][y^1][0][1],f[x][y][0][b]*ppow(2,n-1));
    }
}
int main(){
    freopen("graph.in","r",stdin);
    freopen("graph.out","w",stdout);
    n=read();   p=read();
    f[0][0][0][0]=1;
    For(i,1,n){
        memset(g,0,sizeof g);
        ll opt=read();
        if (opt==-1||opt==0)    Mk1(i);
        if (opt==-1||opt==1)    Mk2(i);
        memcpy(f,g,sizeof g);
    }
    For(a,0,1)For(b,0,1)For(c,0,1)For(d,0,1)
    if ((a^b)==p)   Add(ans,f[a][b][c][d]);
    writeln(ans);
}
/*
最后的答案就是f[n][0][0]+f[n][1][1] 
*/
