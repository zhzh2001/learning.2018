
#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define rep(i,x,y)  for(ll i=x;i<y;++i)
#define Add(x,y)    ((x)=((x)+(y))%mod)
#define Mul(x,y)    ((x)=((x)*(y))%mod)
#define pa pair<ll,ll>
#define mk make_pair
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
    ll x=0,f=1; char ch=getchar();
    for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
    for(;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
    return x*f;
}
void write(ll x){
    if (x<0)putchar('-'),x=-x;
    if (x>=10)write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);puts("");
}
const ll N=200010;
ll n,a[N],pos[N],tp,cnt,mx,x;
void work1(){
    For(i,1,n)write(i),putchar(' ');
    puts("");
}
void work2(){
    For(i,1,n)write(n-i+1),putchar(' ');
    puts("");
}
ll get(ll l,ll r){
    if (x==-1)return (l+r)/2;    //writeln(x);

    return (x=(x*48271%2147483647))%(r-l+1)+l;
}
void Sort(ll l,ll r){
    ll i=l,j=r,mid=get(i,j);
    a[pos[mid]]=mx--;
    ll x=a[pos[mid]];
//    For(i,1,n)write(a[pos[i]]),putchar(' ');puts("");
    do{
//        i=i<mid?(mid-1):(r-1);
        for(;a[pos[i]]<x;++i){
            if (i<mid)i=max(i,mid-1);
            else if (i>mid)i=max(i,r-1);
        }
        for(;a[pos[j]]>x;--j);
        if (i<=j)swap(pos[i++],pos[j--]),cnt+=2;
    }while(i<=j);
//    cout<<i<<' '<<j<<endl;
  //  cout<<cnt<<endl;
    if (l<j)Sort(l,j);
    if (i<r)Sort(i,r);
}
int main(){
    freopen("sort.in","r",stdin);
    freopen("sort.out","w",stdout);
    n=read();   tp=read();  read();mx=n;
    if (tp==1)return work1(),0;
    if (tp==2)return work2(),0;
    if (tp==3)x=-1;
    else    x=read();
    For(i,1,n)pos[i]=i;
    Sort(1,n);
    For(i,1,n)if (!a[i])a[i]=mx--;
    For(i,1,n)write(a[i]),putchar(' ');    puts("");
//    printf("%lf\n",1.0*clock()/CLOCKS_PER_SEC);
}
