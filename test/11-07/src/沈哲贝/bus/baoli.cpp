#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define rep(i,x,y)  for(ll i=x;i<y;++i)
#define Add(x,y)    ((x)=((x)+(y))%mod)
#define Mul(x,y)    ((x)=((x)*(y))%mod)
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
    ll x=0,f=1; char ch=getchar();
    for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
    for(;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
    return x*f;
}
void write(ll x){
    if (x<0)putchar('-'),x=-x;
    if (x>=10)write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);puts("");
}
const ll N=400010;
struct pa{ll first,second;};
vector<ll>g[N],h[N],change[N],ask[N],top[N];
vector<pa>que[N];
ll ans[N],Up[N],dep[N],L[N],R[N],pre[N],u[N],v[N],a[N],b[N],ls[N*20],rs[N*20],fks[N],tr[N],lzh[N],q[N],in[N],mark[N];
ll fa[100010][22],lnk[100010][22];
ll n,m,ind,Q,cnt,Size,to;
ll Lca(ll x,ll y){
    if (dep[x]>dep[y])swap(x,y);
    FOr(i,20,0)if (dep[y]-(1<<i)>=dep[x])   y=fa[y][i];
    FOr(i,20,0)if (fa[x][i]!=fa[y][i])      x=fa[x][i],y=fa[y][i];
    return x==y?x:fa[x][0];
}
void Change(ll &x,ll y){
    if (dep[x]>dep[y])x=y;
}
void dfs1(ll x){
    L[x]=++ind;
    For(i,1,20)fa[x][i]=fa[fa[x][i-1]][i-1];
    rep(i,0,g[x].size()){
        ll to=g[x][i];
        fa[to][0]=x;
        dep[to]=dep[x]+1;
        dfs1(to);
    }
    R[x]=ind;
}
void dfs2(ll x){
    rep(i,0,g[x].size())dfs2(g[x][i]),Change(Up[x],Up[g[x][i]]);
    lnk[x][0]=Up[x];
}
void insert(ll &x,ll l,ll r,ll pos){
    ++cnt;  ls[cnt]=ls[x];  rs[cnt]=rs[x];  x=cnt;
    if (l==r)return;
    ll mid=(l+r)>>1;
    if (pos<=mid)   insert(ls[x],l,mid,pos);
    else            insert(rs[x],mid+1,r,pos);
}
ll merge(ll a,ll b){
    if (!a||!b)return a+b;
    ls[a]=merge(ls[a],ls[b]);
    rs[a]=merge(rs[a],rs[b]);
    return a;
}
ll query(ll x,ll l,ll r){
    if (!x)return 0;
    ll mid=(l+r)>>1;
    if (r<=mid)return query(ls[x],l,r);
    else if (l>mid)return query(rs[x],l,r);
    else return query(ls[x],l,mid)|query(rs[x],mid+1,r);
}
bool cmp(ll a,ll b){return L[a]<L[b];}
void make_father(ll a,ll b){
    h[b].push_back(a);
}
void dfs(ll x){
    tr[x]=0;
    rep(i,0,change[x].size())insert(tr[x],1,n,L[change[x][i]]);
    rep(i,0,h[x].size())dfs(h[x][i]),merge(tr[x],tr[h[x][i]]);
    rep(i,0,que[x].size()){
        if (query(tr[x],L[que[x][i].second],R[que[x][i].second]))
            fks[que[x][i].first]=1;
    }
    h[x].clear();
}
void Build_Fault(){
    sort(lzh+1,lzh+Size+1,cmp);
    q[to=1]=lzh[1];    in[lzh[1]]=1;   in[0]=1;
    For(i,2,Size){
        ll tmp=q[i];
        for(;to>=2;){
            tmp=Lca(q[to],lzh[i]);
            if (dep[tmp]<dep[q[to-1]])   make_father(q[to],q[to-1]),in[q[to--]]=0;
            else    break;
        }
        if (!in[tmp])   in[tmp]=1,q[++to]=tmp;
        if (!in[lzh[i]])in[lzh[i]]=1,q[++to]=lzh[i];
    }
    for(;to>=2;)make_father(q[to],q[to-1]),--to;
}
void calc(ll x){
    if (!mark[x])   mark[x]=1,lzh[++Size]=x;
    Size=0;
    rep(i,0,ask[x].size()){
        ll a=u[ask[x][i]],b=v[ask[x][i]];
        if (!mark[a])   lzh[++Size]=a,mark[a]=1;
        if (!mark[b])   lzh[++Size]=b,mark[b]=1;

        que[a].push_back((pa){que[b],ask[x][i]});
        que[b].push_back((pa){que[a],ask[x][i]});
    }
    rep(i,0,top[x].size()){
        ll a=u[top[x][i]],b=v[top[x][i]];
        if (!mark[a])   lzh[++Size]=a,mark[a]=1;
        if (!mark[b])   lzh[++Size]=b,mark[b]=1;

        change[a].push_back(b);
        change[b].push_back(a);
    }
    Build_Fault();
    dfs(lzh[1]);
    For(i,1,Size){
        ll x=lzh[i];
        que[x].clear();
        change[x].clear();
        mark[x]=0;
    }
}
int main(){
    freopen("bus1.in","r",stdin);
//    freopen("bus.out","w",stdout);
    n=read();
    For(i,2,n){
        pre[i]=read();
        g[pre[i]].push_back(i);
    }
    dfs1(1);
    m=read();
    For(i,1,n)Up[i]=i;
    For(i,1,m){
        u[i]=read(),v[i]=read();    ll tmp=Lca(u[i],v[i]);
        Change(Up[u[i]],tmp);
        Change(Up[v[i]],tmp);
        top[tmp].push_back(i);
//        cout<<u[i]<<' '<<v[i]<<' '<<tmp<<endl;
    }
//    For(i,1,n)write(Up[i]),putchar(' ');puts("");
    dfs2(1);
//    For(i,1,n)write(Up[i]),putchar(' ');puts("");
    For(i,1,20)For(x,1,n)lnk[x][i]=lnk[lnk[x][i-1]][i-1];
    Q=read();
    For(i,1,Q){
        a[i]=read(),b[i]=read();    ll tmp=Lca(a[i],b[i]),cost=0,x=a[i],y=b[i];
        FOr(i,20,0)if(dep[lnk[x][i]]>dep[tmp])  cost+=1<<i,x=lnk[x][i];
        FOr(i,20,0)if(dep[lnk[y][i]]>dep[tmp])  cost+=1<<i,y=lnk[y][i];
        if      (dep[lnk[x][0]]>dep[tmp]||dep[lnk[y][0]]>dep[tmp])ans[i]=-1;
        else if (a[i]==tmp&&b[i]==tmp)  ans[i]=0;
        else if (a[i]==tmp||b[i]==tmp)  ans[i]=cost+1;
        else    ans[i]=cost+2,ask[tmp].push_back(i);
    }
    For(i,1,n)calc(i);
    For(i,1,Q)  writeln(ans[i]-fks[i]);
}
/*
�����Ŀ��� 

7
1 1 1 4 5 6
4
4 2
5 4
1 3
6 7
5
4 5
3 5
7 2
3 2
5 3
*/
