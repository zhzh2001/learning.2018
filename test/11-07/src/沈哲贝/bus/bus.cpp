#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define rep(i,x,y)  for(ll i=x;i<y;++i)
#define Add(x,y)    ((x)=((x)+(y))%mod)
#define Mul(x,y)    ((x)=((x)*(y))%mod)
#define pa pair<ll,ll>
#define mk make_pair
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
    ll x=0,f=1; char ch=getchar();
    for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')f=-1;
    for(;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
    return x*f;
}
void write(ll x){
    if (x<0)putchar('-'),x=-x;
    if (x>=10)write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);puts("");
}
const ll N=400010;
vector<ll>g[N];
map<pa,ll>mp;
ll ans[N],Up[N],dep[N],L[N],R[N],pre[N],u[N],v[N],a[N],b[N];
ll fa[200010][22],lnk[200010][22];
ll n,m,ind,Q,fl1=1,fl2=1;
ll Lca(ll x,ll y){
    if (dep[x]>dep[y])swap(x,y);
    FOr(i,20,0)if (dep[y]-(1<<i)>=dep[x])   y=fa[y][i];
    FOr(i,20,0)if (fa[x][i]!=fa[y][i])      x=fa[x][i],y=fa[y][i];
    return x==y?x:fa[x][0];
}
void Change(ll &x,ll y){
    if (dep[x]>dep[y])x=y;
}
void dfs1(ll x){
    L[x]=++ind;
    For(i,1,20)fa[x][i]=fa[fa[x][i-1]][i-1];
    rep(i,0,g[x].size()){
        ll to=g[x][i];
        fa[to][0]=x;
        dep[to]=dep[x]+1;
        dfs1(to);
    }
    R[x]=ind;
}
void dfs2(ll x){
    rep(i,0,g[x].size())dfs2(g[x][i]),Change(Up[x],Up[g[x][i]]);
    lnk[x][0]=Up[x];
}
bool In(ll a,ll b){
    return (L[a]<=L[b])&&(R[b]<=R[a]);
}//u在x的子树中
int main(){
    freopen("bus.in","r",stdin);
    freopen("bus.out","w",stdout);
    n=read();
    For(i,2,n){
        pre[i]=read();
        g[pre[i]].push_back(i);
        fl1&=pre[i]==(i-1);
        fl2&=pre[i]==1;
    }
    dfs1(1);
    m=read();
    For(i,1,n)Up[i]=i;
    For(i,1,m){
        u[i]=read(),v[i]=read();    ll tmp=Lca(u[i],v[i]);
        Change(Up[u[i]],tmp);
        Change(Up[v[i]],tmp);
        if (fl2)mp[mk(u[i],v[i])]=1;
//        cout<<u[i]<<' '<<v[i]<<' '<<tmp<<endl;
    }
//    For(i,1,n)write(Up[i]),putchar(' ');puts("");
    dfs2(1);
//    For(i,1,n)write(Up[i]),putchar(' ');puts("");
    For(i,1,20)For(x,1,n)lnk[x][i]=lnk[lnk[x][i-1]][i-1];
    Q=read();
    For(i,1,Q){
        a[i]=read(),b[i]=read();    ll tmp=Lca(a[i],b[i]),cost=0,x=a[i],y=b[i];
        FOr(i,20,0)if(dep[lnk[x][i]]>dep[tmp])  cost+=1<<i,x=lnk[x][i];
        FOr(i,20,0)if(dep[lnk[y][i]]>dep[tmp])  cost+=1<<i,y=lnk[y][i];
//        cout<<x<<' '<<y<<endl;
        if      (dep[lnk[x][0]]>dep[tmp]||dep[lnk[y][0]]>dep[tmp])ans[i]=-1;
        else if (a[i]==tmp&&b[i]==tmp)  ans[i]=0;
        else if (a[i]==tmp||b[i]==tmp)  ans[i]=cost+1;
        else{
            ans[i]=cost+2;
            if (n<=5000&&m<=5000)
            For(j,1,m){
                if (In(x,u[j])&&In(y,v[j])){--ans[i];break;}
                if (In(x,v[j])&&In(y,u[j])){--ans[i];break;}
            }//u在x的子树中 
            else if (fl2){
                ans[i]-=mp[mk(x,y)]||mp[mk(y,x)];
            }
        }
    }
    For(i,1,Q)  writeln(ans[i]);
}
/*
生命的快乐 

7
1 1 1 4 5 6
4
4 2
5 4
1 3
6 7
5
4 5
3 5
7 2
3 2
5 3
*/
