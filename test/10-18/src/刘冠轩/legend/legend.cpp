#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=50005;
int n,q,h,l,r,v,a[N],ans;
int main(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	n=read();q=read();h=read();
	for(int i=1;i<=q;i++){
		l=read();r=read();v=read();
		for(int j=l;j<=r;j++)a[j]+=v;
		ans=0;
		for(int j=2;j<n;j++){
			if(a[j]>a[j-1])l=j;
			else if(a[j]<a[j-1])l=j+1;
			if(a[j]>a[j+1])ans+=j-l+1;
		}
		cout<<ans<<'\n';
	}
	return 0;
}
