#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=50005;
int f[N],r[N],rr[N],R[N],n,c[10],cnt,sq,sz[N],ans[N],Ans;
bool dfs(int u,int x){
	R[f[u]]--;
	sz[f[u]]+=sz[u]+1;
	if((sz[u]+1)%x!=0&&r[u]-rr[u]>1)return 0;
	if((sz[u]+1)%x==0)rr[f[u]]++;
	if(R[f[u]]==0)return dfs(f[u],x);
	return 1;
}
bool check(int x){
	bool f=1;
	memset(sz,0,sizeof(sz));
	memset(rr,0,sizeof(rr));
	for(int i=1;i<=n;i++)R[i]=r[i];
	for(int i=n;i;i--)if(R[i]==0&&f)f&=dfs(i,x);
	return f;
}
void solve(){
	cout<<"Case #"<<++cnt<<'\n';
	memset(r,0,sizeof(r));Ans=0;
	for(int i=2;i<=n;i++)r[f[i]]++;
	cout<<1<<'\n';
	for(int j=2;j<=sq;j++)
		if(n%j==0){
			if(check(j))ans[++Ans]=j;
			if(n/j!=j&&check(n/j))ans[++Ans]=n/j;
		}
	sort(ans+1,ans+Ans+1);
	for(int j=1;j<=Ans;j++)cout<<ans[j]<<'\n';
	cout<<n<<'\n';
}
int main(){
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	n=read();sq=sqrt(n);
	for(int i=1;i<=9;i++)c[i]=read();
	for(int i=2;i<=n;i++)f[i]=read();
	solve();
	for(int i=1;i<=9;i++){
		for(int j=2;j<=n;j++)
			f[j]=((f[j]+c[i])%(j-1)+j-1)%(j-1)+1;
		solve();
	}
	return 0;
}
