#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=100005;
struct node{
	int x,y;
	bool operator <(const node b)const{
		return x==b.x?y<b.y:x<b.x;
	}
}t,f;
priority_queue<node> Q,Q2;
int n,u,Ans,ans[N],vis[N],mx=1e9;
bool eat[N];
int main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	Ans=n=read();
	for(int i=1;i<=n;i++){
		u=read();
		Q.push((node){u,i});
		Q2.push((node){-u,-i});
	}
	for(int i=1;i<n;i++){
		while(eat[Q.top().y])Q.pop();
		while(eat[-Q2.top().y])Q2.pop();
		f=Q.top();Q.pop();
		t=Q2.top();Q2.pop();
		if(vis[-t.y]){
			if (vis[-t.y]<=mx)Ans=vis[-t.y],mx=min(mx,i);
				else break;
		}
		ans[i]=-t.y;
		vis[f.y]=i;
		eat[-t.y]=1;
		f.x+=t.x;
		t.x=-f.x;
		t.y=-f.y;
		Q.push(f);
		Q2.push(t);
	}
	sort(ans+1,ans+Ans);
	cout<<Ans-1<<endl;
	for(int i=1;i<Ans;i++)cout<<ans[i]<<' ';
	return 0;
}
