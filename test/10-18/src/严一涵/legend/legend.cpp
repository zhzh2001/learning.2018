#include <iostream>
#include <algorithm>
#include <cstdio>
#include <map>
#include <queue>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
inline void write(int x){
	static int c[23];
	static int n;
	n=0;
	if(x<0){
		putchar('-');
		x=-x;
	}
	if(x==0){
		putchar('0');
		return;
	}
	while(x>0){
		c[n++]=x%10;
		x/=10;
	}
	while(n-->0){
		putchar(c[n]+'0');
	}
}
#undef dd
#define ls (nod<<1)
#define rs (nod<<1|1)
int qv[100013],qvt,n,m,H;
struct ts{
	int lp,rp,ll,lv,rl,rv,ans,lazy;
}tree[4*100013];
struct que{
	int a,b,c;
}qq[50003];
void pushup(int nod){
	ts &nt=tree[nod],&lt=tree[ls],&rt=tree[rs];
	nt.ans=lt.ans+rt.ans;
	nt.ll=lt.ll;
	nt.lv=lt.lv;
	nt.rl=rt.rl;
	nt.rv=rt.rv;
	if(lt.rl==rt.lp-lt.lp){
		if(rt.ll==rt.rp-lt.rp){
			if(lt.rv==rt.lv){
				nt.ll=nt.rl=nt.rp-nt.lp+1;
			}else if(lt.rv>rt.lv){
				nt.rl=0;
			}else{
				nt.ll=0;
			}
		}else{
			if(lt.rv==rt.lv){
				if(rt.ll==0)nt.ll=0;
				else nt.ll+=rt.ll;
			}else if(lt.rv<rt.lv){
				nt.ll=0;
				nt.ans+=rt.ll;
			}
		}
	}else{
		if(rt.ll==rt.rp-lt.rp){
			if(lt.rv==rt.lv){
				if(lt.rl==0)nt.rl=0;
				else nt.rl+=lt.rl;
			}else if(lt.rv>rt.lv){
				nt.rl=0;
				nt.ans+=lt.rl;
			}
		}else{
			if(lt.rv==rt.lv){
				if(lt.rl>0&&rt.ll>0)
					nt.ans+=lt.rl+rt.ll;
			}else if(lt.rv>rt.lv){
				nt.ans+=lt.rl;
			}else{
				nt.ans+=rt.ll;
			}
		}
	}
}
void pushins(int nod,int v){
	ts &nt=tree[nod];
	nt.lv+=v;
	nt.rv+=v;
	nt.lazy+=v;
}
void pushdown(int nod){
	ts &nt=tree[nod],&lt=tree[ls],&rt=tree[rs];
	if(nt.lazy!=0){
		pushins(ls,nt.lazy);
		pushins(rs,nt.lazy);
		nt.lazy=0;
	}
}
void build(int nod,int l,int r){
	ts &nt=tree[nod],&lt=tree[ls],&rt=tree[rs];
	nt.lazy=0;
	if(l==r){
		nt.lp=qv[l];
		nt.rp=qv[l+1]-1;
		nt.ll=nt.rl=nt.rp-nt.lp+1;
		nt.lv=nt.rv=H;
		nt.ans=0;
		return;
	}
	int mid=(l+r)/2;
	build(ls,l,mid);
	build(rs,mid+1,r);
	nt.lp=lt.lp;
	nt.rp=rt.rp;
	pushup(nod);
}
void ins(int nod,int x,int y,int z){
	ts &nt=tree[nod],&lt=tree[ls],&rt=tree[rs];
	if(x==nt.lp&&y==nt.rp){
		pushins(nod,z);
		return;
	}
	pushdown(nod);
	if(x<=lt.rp)ins(ls,x,min(y,lt.rp),z);
	if(y>=rt.lp)ins(rs,max(x,rt.lp),y,z);
	pushup(nod);
}
#undef ls
#undef rs
void unqu(){
	int x=qvt;
	qvt=1;
	for(int i=2;i<=x;i++)if(qv[i]!=qv[i-1])qv[++qvt]=qv[i];
}
void cot(){
	return;
	puts("{");
	for(int i=1;tree[i].rp!=0;i++){
		ts&p=tree[i];
		if(p.ans==0)continue;
		printf("id= %d  %d - %d : left(l= %d ,v= %d ) right(l= %d ,v= %d ) ans= %d\n",i,p.lp,p.rp,p.ll,p.lv,p.rl,p.rv,p.ans);
	}
	puts("}");
}
int main(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	n=read();
	m=read();
	H=read();
	qvt=2;
	qv[1]=1;
	qv[2]=n+1;
	for(int i=1;i<=m;i++){
		qq[i].a=read();
		qq[i].b=read();
		qq[i].c=read();
		qv[++qvt]=qq[i].a;
		qv[++qvt]=qq[i].b+1;
	}
	sort(qv+1,qv+qvt+1);
	unqu();
	build(1,1,qvt-1);
	for(int i=1;i<=m;i++){
		ins(1,qq[i].a,qq[i].b,qq[i].c);
		write(tree[1].ans);
		putchar('\n');
		if(i<=7)cot();
	}
	return 0;
}
