#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
inline void write(int x){
	static int c[23];
	static int n;
	n=0;
	if(x<0){
		putchar('-');
		x=-x;
	}
	if(x==0){
		putchar('0');
		return;
	}
	while(x>0){
		c[n++]=x%10;
		x/=10;
	}
	while(n-->0){
		putchar(c[n]+'0');
	}
}
#undef dd
int n,f[100003],c[100003],p[9];
void calc(){
	write(1);
	putchar('\n');
	for(int i=2;i<=n;i++)if(n%i==0){
		bool flag=1;
		memset(c,0,sizeof(c));
		for(int j=n;j>=1;j--){
			c[j]++;
			if(c[j]>i){
				flag=0;
				break;
			}
			if(c[j]!=i)c[f[j]]+=c[j];
		}
		if(flag){
			write(i);
			putchar('\n');
		}
	}
}
int main(){
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	n=read();
	for(int i=0;i<9;i++)p[i]=read();
	for(int i=2;i<=n;i++)f[i]=read();
	puts("Case #1:");
	calc();
	for(int i=0;i<9;i++){
		printf("Case #%d:\n",i+2);
		for(int j=2;j<=n;j++){
			f[j]=(f[j]+p[i])%(j-1);
			if(f[j]<0)f[j]+=j-1;
			f[j]++;
		}
		calc();
	}
	return 0;
}
