#include <iostream>
#include <algorithm>
#include <cstdio>
#include <map>
#include <queue>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,ansCnt,e[100003],f[100003];
bool ans[100003];
priority_queue<pair<int,int> >a,b;
priority_queue<pair<int,int>,vector<pair<int,int> >,greater<pair<int,int> > >c,d;
void mat(){
	while(!b.empty()&&a.top()==b.top()){
		a.pop();
		b.pop();
	}
	while(!d.empty()&&c.top()==d.top()){
		c.pop();
		d.pop();
	}
}
void push(pair<int,int> x){
	a.push(x);
	c.push(x);
}
pair<int,int> topMax(){
	mat();
	return a.top();
}
pair<int,int> topMin(){
	mat();
	return c.top();
}
void popMax(){
	mat();
	d.push(a.top());
	a.pop();
}
void popMin(){
	mat();
	b.push(c.top());
	c.pop();
}
int size(){
	mat();
	return a.size();
}
bool empty(){
	mat();
	return a.empty();
}
int main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		push(make_pair(read(),i));
	}
	for(int i=1;i<n;i++){
		pair<int,int>x=topMax();
		pair<int,int>y=topMin();
		popMax();
		popMin();
		e[i]=x.second;
		f[i]=y.second;
		x.first-=y.first;
		ansCnt++;
		ans[y.second]=1;
		push(x);
	}
	int q=n;
	for(int i=n-1;i>0;i--){
		if(ans[e[i]]){
			for(int j=i;j<q;j++){
				ans[f[j]]=0;
				ansCnt--;
			}
			q=i;
		}
	}
	printf("%d\n",ansCnt);
	for(int i=1;i<=n;i++)if(ans[i]){
		printf("%d ",i);
	}
	return 0;
}
