#pragma GCC optimize 2
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back
#define fir first
#define sec second
#define pa pair<int,int>
#define mk make_pair

using namespace std;

inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void write_p(ll x){write(x);putchar(' ');}
inline void writeln(ll x){write(x);puts("");}

int main()
{
	freopen("lions.in","w",stdout);
	int n=100000;
	cout<<n<<endl;
	For(i,1,n)	cout<<(rand()<<10|rand())<<' ';
}
