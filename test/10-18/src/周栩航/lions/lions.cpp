#pragma GCC optimize 2
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back
#define fir first
#define sec second
#define pa pair<int,int>
#define mk make_pair

using namespace std;

inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void write_p(ll x){write(x);putchar(' ');}
inline void writeln(ll x){write(x);puts("");}

const int N=300005;
priority_queue<pa,vector<pa>,greater<pa> > q_mi;
priority_queue<pa> q_mx;
pa a[N];
int n,ans[N],gg[N],be_max[N],tim,to[N],t[N],vis[N],cnt,head[N],nxt[N],poi[N],tans;
inline bool cmp(int x,int y){return be_max[x]<be_max[y];}
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;}
inline void Dfs(int x,int can)
{
	for(int i=head[x];i;i=nxt[i])
		Dfs(poi[i],-can);
	if(can==-1)	if(be_max[x])	tans=min(tans,be_max[x]-1);
}
int main()
{
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	For(i,1,n)	a[i].fir=read(),a[i].sec=i;
	sort(a+1,a+n+1);
	For(i,1,n)	q_mx.push(a[i]),q_mi.push(a[i]);
//	cout<<"???"<<endl;
	while(!q_mx.empty()&&!q_mi.empty())
	{
		pa tmx,tmi;
		while(!q_mx.empty())
		{
			pa tmp=q_mx.top();q_mx.pop();
			if(gg[tmp.sec])	continue;
			tmx=tmp;	
			break;
		}
		while(!q_mi.empty())
		{
			pa tmp=q_mi.top();q_mi.pop();
			if(gg[tmp.sec])	continue;
			tmi=tmp;	
			break;
		}
		if(tmi.sec==tmx.sec)	break;
		be_max[tmx.sec]=++tim;//最后一次成为最大值的时刻 
		
		ans[tim]=tmi.sec;
		gg[tmi.sec]=tim;//被吃掉的时刻
		to[tmi.sec]=tmx.sec;//被谁吃掉 
		tmx.fir-=tmi.fir;
		q_mx.push(tmx);q_mi.push(tmx);
	}
	For(i,1,n)	t[i]=i;
	sort(t+1,t+n+1,cmp);
	tans=n*2;
	For(i,1,n)	if(to[i])	add(to[i],i);
	For(i,1,n)	if(!vis[i]&&!to[i])	Dfs(i,1);
	
	sort(ans+1,ans+tans+1);
	writeln(tans);
	For(i,1,tans)	write_p(ans[i]);
}
