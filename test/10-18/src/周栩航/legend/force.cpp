#pragma GCC optimize 2
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define pb push_back
#define fir first
#define sec second
#define ll long long
#define pa pair<int,int>
#define mk make_pair
#define IT set<node> :: iterator 
#define inf 2e9

using namespace std;

inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void write_p(ll x){write(x);putchar(' ');}
inline void writeln(ll x){write(x);puts("");}

const int N=500005;
int n,Q,l,r,v,a[N];
int main()
{
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	n=read();Q=read();read();
	For(ttt,1,Q)
	{
		int l=read(),r=read(),v=read();
		For(j,l,r)	a[j]+=v;
		int tot=0,now=0,sta=0,en=0;
		For(i,1,n)	if(a[i]!=a[1])	{sta=i;break;}
		Dow(i,1,n)	if(a[i]!=a[n])	{en=i;break;}
		
		int tl=sta,rr=sta;      
		For(i,sta+1,en)	
		{
			if(a[i]==a[i-1])	rr=i;else
			{
				if(a[tl]>a[tl-1]&&a[rr]>a[rr+1])	tot+=(rr-tl+1);
				tl=rr+1;rr=tl;
			}
		}	
		if(a[tl]>a[tl-1]&&a[rr]>a[rr+1])	tot+=(rr-tl+1);
		writeln(tot);
	}
}
