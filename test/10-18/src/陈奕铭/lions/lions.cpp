#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int n;
int a[N];
bool vis;

signed main(){
	n = read(); 
	for(int i = 1;i <= n;++i){
		a[i] = read();
		if(a[i] != 0 && a[i] != 1) vis = true;
	}
	if(!vis){
		int sum = 0;
		for(int i = 1;i <= n;++i)
			if(a[i] == 0) ++sum;
		if(sum == n){
			printf("%d\n", sum-1);
			for(int i = 1;i < n;++i) printf("%d ",i);
			return 0;
		}
		else{
			for(int i = 1;i <= n;++i)
				if(a[i] == 0) printf("%d ",i);
			printf("%d\n", sum);
			return 0;
		}
	}
	return 0;
}