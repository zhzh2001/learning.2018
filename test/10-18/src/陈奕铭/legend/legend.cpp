#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 50005;
int n,q,h;
struct tree{
	int ls,rs;
	int l1,l2,r1,r2;
	int l1len,l2len,r1len,r2len;
	int sum,lazy;
	bool k3;
}tr[N*32];
int tot;

inline void PushDown(int k,int v){
	tr[k].lazy += v;
	tr[k].l1 += v; tr[k].r1 += v;
	if(tr[k].l2len != 0) tr[k].l2 += v,tr[k].r2 += v;
}
inline void pushdown(int k,int l,int r){
	if(tr[k].ls == 0){
		int mid = (l+r)>>1;
		tr[k].ls = ++tot;
		int ls = tr[k].ls;
		tr[ls].ls = tr[ls].rs = 0;
		tr[ls].l1 = 0; tr[ls].l1len = mid-l+1;
		tr[ls].l2 = 0; tr[ls].l2len = 0;
		tr[ls].r1 = 0; tr[ls].r1len = mid-l+1;
		tr[ls].r2 = 0; tr[ls].r2len = 0;
		tr[ls].sum = 0; tr[ls].lazy = 0;
		tr[ls].k3 = 0;

		tr[k].rs = ++tot;
		int rs = tr[k].rs;
		tr[rs].ls = tr[rs].rs = 0;
		tr[rs].l1 = 0; tr[rs].l1len = r-mid;
		tr[rs].l2 = 0; tr[rs].l2len = 0;
		tr[rs].r1 = 0; tr[rs].r1len = r-mid;
		tr[rs].r2 = 0; tr[rs].r2len = 0;
		tr[rs].sum = 0; tr[rs].lazy = 0;
		tr[rs].k3 = 0;
	}
	if(tr[k].lazy){
		PushDown(tr[k].ls,tr[k].lazy);
		PushDown(tr[k].rs,tr[k].lazy);
		tr[k].lazy = 0;
	}
}

inline void update(int k){
	int l = tr[k].ls,r = tr[k].rs;
	tr[k].sum = tr[l].sum + tr[r].sum;
	tr[k].k3 = tr[l].k3|tr[r].k3;
	tr[k].lazy = 0;

	if(!tr[l].k3){			//说明左边只有两块以下
		if(tr[l].l1 == tr[r].l1 && tr[l].l2len == 0) tr[k].l1 = tr[l].l1,tr[k].l1len = tr[l].l1len + tr[r].l1len;	
		else{
			tr[k].l1 = tr[l].l1,tr[k].l1len = tr[l].l1len;
			if(tr[r].l2len != 0) tr[k].k3 = 1;
		}
		if(tr[l].l2len == 0){
			if(tr[r].l1 != tr[l].l1){
				if(tr[r].l2len != 0) tr[k].k3 = 1;
				tr[k].l2 = tr[r].l1,tr[k].l2len = tr[r].l1len;
			}
			else{
				if(tr[r].l2len != 0) tr[k].l2 = tr[r].l2,tr[k].l2len = tr[r].l2len;
				else tr[k].l2 = tr[k].l2len = 0;
			}
		}else{
			if(tr[l].l2 != tr[r].l1) tr[k].l2 = tr[l].l2,tr[k].l2len = tr[l].l2len,tr[k].k3 = 1;
			else{
				tr[k].l2 = tr[l].l2; tr[k].l2len = tr[l].l2len + tr[r].l1len;
				if(tr[r].l2len != 0) tr[k].k3 = 1;
			}
		}
	}else{
		tr[k].l1 = tr[l].l1; tr[k].l2 = tr[l].l2;
		tr[k].l1len = tr[l].l1len; tr[k].l2len = tr[l].l2len;
	}

	if(!tr[r].k3){	//说明右边只有两块以下
		if(tr[r].r1 == tr[l].r1 && tr[r].r2len == 0) tr[k].r1 = tr[r].r1,tr[k].r1len = tr[r].r1len + tr[l].r1len;	
		else tr[k].r1 = tr[r].r1,tr[k].r1len = tr[r].r1len;
		if(tr[r].r2len == 0){
			if(tr[l].r1 != tr[r].r1){
				if(tr[l].r2len != 0) tr[k].k3 = 1;
				tr[k].r2 = tr[l].r1,tr[k].r2len = tr[l].r1len;
			}
			else{
				if(tr[l].r2len != 0) tr[k].r2 = tr[l].r2,tr[k].r2len = tr[l].r2len;
				else tr[k].r2 = tr[k].r2len = 0;
			}
		}else{
			if(tr[r].r2 != tr[l].r1) tr[k].r2 = tr[r].r2,tr[k].r2len = tr[r].r2len,tr[k].k3 = 1;
			else{
				tr[k].r2 = tr[r].r2; tr[k].r2len = tr[r].r2len + tr[l].r1len;
				if(tr[l].r2len != 0) tr[k].k3 = 1;
			}
		}
	}else{
		tr[k].r1 = tr[r].r1; tr[k].r2 = tr[r].r2;
		tr[k].r1len = tr[r].r1len; tr[k].r2len = tr[r].r2len;
	}

	if(tr[l].r1 == tr[r].l1){	//中间两块相同
		if(tr[l].r1 > tr[l].r2 && tr[l].r2len != 0 && tr[r].l1 > tr[r].l2 && tr[r].l2len != 0)
			tr[k].sum += tr[l].r1len + tr[r].l1len;
	}else if(tr[l].r1 > tr[r].l1){
		if(tr[l].r1 > tr[l].r2 && tr[l].r2len != 0)
			tr[k].sum += tr[l].r1len;
	}else{
		if(tr[r].l1 > tr[r].l2 && tr[r].l2len != 0)
			tr[k].sum += tr[r].l1len;
	}
}

void change(int num,int l,int r,int x,int y,int v){
	if(x <= l && r <= y){
		PushDown(num,v);
		return;
	}
	int mid = (l+r)>>1;
	pushdown(num,l,r);
	if(x <= mid) change(tr[num].ls,l,mid,x,y,v);
	if(y > mid) change(tr[num].rs,mid+1,r,x,y,v);
	update(num);
}

signed main(){
	 freopen("legend.in","r",stdin);
	 freopen("legend.out","w",stdout);
	n = read(); q = read(); h = read();
	++tot;
	tr[1].ls = tr[1].rs = 0;
	tr[1].l1 = 0; tr[1].l1len = n;
	tr[1].l2 = 0; tr[1].l2len = 0;
	tr[1].r1 = 0; tr[1].r1len = n;
	tr[1].r2 = 0; tr[1].r2len = 0;
	tr[1].sum = 0; tr[1].lazy = 0;
	tr[1].k3 = 0;
	pushdown(1,1,n);
	for(int i = 1;i <= q;++i){
		int x = read(),y = read(),v = read();
		change(1,1,n,x,y,v);
		printf("%d\n", tr[1].sum);
	}
	return 0;
}
