#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}


const int N = 1200005;
int n;
int c[10];
int f[N];

signed main(){
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	n = read(); for(int i = 1;i <= 9;++i) c[i] = read();
	for(int i = 1;i <= n;++i) f[i] = read();
	for(int i = 1;i <= 10;++i){
		printf("Case #%d:\n", i);
		printf("%d\n",1);
		printf("%d\n",n);
		if(i < 10){
			for(int j = 1;j <= n;++j){
				f[j] = ((f[j]+c[i])%(j-1)+j-1)%(j-1)+1;
			}
		}
	}
	return 0;
}