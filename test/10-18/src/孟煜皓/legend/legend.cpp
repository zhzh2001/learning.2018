#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 50005
#define M 200005
int n, h, q;
namespace Fake{
	int a[N];
	int query(){
		int ans = 0;
		for (register int l = 1, r = 1; l <= n; l = ++r){
			while (a[r] == a[r + 1]) ++r;
			if (a[l] > a[l - 1] && a[r] > a[r + 1]) ans += r - l + 1;
		}
		return ans;
	}
	void Main(){
		a[0] = a[n + 1] = 2000000000;
		while (q--){
			int l = read(), r = read(), x = read();
			for (register int i = l; i <= r; ++i) a[i] += x;
			printf("%d\n", query());
		}
	}
};
struct SegMent_Tree{
	int L[M], R[M], lv[M], rv[M], lz[M], sum[M];
	bool fl[M], fr[M];
	void update(int u, int l, int r, int mid){
		int ls = u << 1, rs = u << 1 | 1;
		sum[u] = sum[ls] + sum[rs], lv[u] = lv[ls], rv[u] = rv[rs];
		if (fr[ls] && fl[rs] && rv[ls] == lv[rs]) sum[u] += R[ls] + L[rs];
		if (fr[ls] && rv[ls] > lv[rs]) sum[u] += R[ls];
		if (fl[rs] && lv[rs] > rv[ls]) sum[u] += L[rs];
		if (L[ls] == mid - l + 1)
			if (rv[ls] == lv[rs]) L[u] = L[ls] + L[rs], fl[u] = fl[rs];
			else L[u] = L[ls], fl[u] = rv[ls] > lv[rs];
		else L[u] = L[ls], fl[u] = fl[ls];
		if (R[rs] == r - mid)
			if (lv[rs] == rv[ls]) R[u] = R[rs] + R[ls], fr[u] = fr[ls];
			else R[u] = R[rs], fr[u] = lv[rs] > rv[ls];
		else R[u] = R[rs], fr[u] = fr[rs];
	}
	void build(int u, int l, int r){
		if (l == r) return L[u] = R[u] = 1, (void)0;
		int mid = l + r >> 1;
		build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
		update(u, l, r, mid);
	}
	void down(int u){
		if (!lz[u]) return;
		int x = lz[u]; lz[u] = 0, lz[u << 1] += x, lz[u << 1 | 1] += x;
		lv[u << 1] += x, rv[u << 1] += x, lv[u << 1 | 1] += x, rv[u << 1 | 1]+=x;
	}
	void modify(int u, int l, int r, int L, int R, int x){
		if (L <= l && r <= R) return lz[u] += x, lv[u] += x, rv[u] += x, (void)0;
		int mid = l + r >> 1; down(u);
		if (L <= mid) modify(u << 1, l, mid, L, R, x);
		if (R > mid) modify(u << 1 | 1, mid + 1, r, L, R, x);
		update(u, l, r, mid);
	}
}T;
int main(){
	freopen("legend.in", "r", stdin);
	freopen("legend.out", "w", stdout);
	n = read(), q = read(), h = read();
	if (n <= 1000) return Fake :: Main(), 0;
	T.build(1, 1, n);
	while (q--){
		int l = read(), r = read(), x = read();
		T.modify(1, 1, n, l, r, x), printf("%d\n", T.sum[1]);
	}
}
