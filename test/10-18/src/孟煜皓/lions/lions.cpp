#include <cstdio>
#include <cctype>
#include <algorithm>
#include <set>
using std :: set; 
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
int n, a[N], b[N], ans;
struct node{
	int v, p;
	bool operator < (const node &res) const {
		return v > res.v || v == res.v && p > res.p;
	}
	node(int V = 0, int P = 0){ v = V, p = P; }
};
set<node> S;
int main(){
	freopen("lions.in", "r", stdin);
	freopen("lions.out", "w", stdout);
	n = read();
	for (register int i = 1; i <= n; ++i) S.insert(node(read(), i));
	for (register int i = 1; i < n; ++i){
		node u = *(S.begin()), v = *(--S.end());
		S.erase(u), S.erase(v);
		a[i] = v.p, b[u.p] = i;
		S.insert(node(u.v - v.v, u.p));
	}
	int m = n - 1;
	for (register int i = n - 1; i; i = std :: min(i - 1, m))
		if (b[a[i]]) m = b[a[i]] - 1;
	std :: sort(a + 1, a + 1 + m);
	printf("%d\n", m);
	for (register int i = 1; i <= m; ++i) printf("%d ", a[i]);
}
