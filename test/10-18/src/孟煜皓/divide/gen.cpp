#include <cstdio>
#include <cstdlib>
#include <windows.h>
int rnd(int a, int b){
	return ((rand() << 15) + rand()) % (b - a + 1) + a;
}
int main(){
	freopen("divide.in", "w", stdout);
	srand(GetTickCount());
	int n = 1200000;
	printf("%d", n);
	for (register int i = 1; i <= 9; ++i) printf(" %d", rnd(-100000000, 100000000));
	putchar('\n');
	for (register int i = 2; i <= n; ++i)
		printf("%d", rnd(1, i - 1)), putchar(i == n ? '\n' : ' ');
}