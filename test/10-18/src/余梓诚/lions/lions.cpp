#include <set>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace  std;

void judge(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
}

const int N=110000;
int n,a[N],b[N];
bool used[N];
set<pair<int,int> >s;

int main(){
	judge();
	ios::sync_with_stdio(false);
	cin>>n;
	for (int i=1;i<=n;i++){
		int x; cin>>x;
		s.insert(make_pair(x,i));
	}
	for (int i=1;i<n;i++){
		pair<int,int> E=*(--s.end()),B=*(s.begin());
		s.erase(B); s.erase(E);
		a[i]=B.second; b[E.second]=i;
		s.insert(make_pair(E.first-B.first,E.second));
	}
	int ans=n-1;
	for (int i=n-1;i;i=min(i-1,ans)){
		if (b[a[i]]) ans=b[a[i]]-1;
	}
	sort(a+1,a+ans+1);
	cout<<ans<<endl;
	for (int i=1;i<=ans;i++) cout<<a[i]<<" ";
	return 0;
}
