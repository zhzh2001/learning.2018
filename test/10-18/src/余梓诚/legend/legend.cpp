#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

void judge(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
}

const int N=51000;
int H,n,Q,a[N];

int sum[N<<2],L[N<<2],R[N<<2],lv[N<<2],rv[N<<2],lazy[N<<2];
bool fl[N<<2],fr[N<<2];
void change(int k,int l,int r,int mid){
	sum[k]=sum[k<<1]+sum[k<<1|1],lv[k]=lv[k<<1],rv[k]=rv[k<<1|1];
	if (fr[k<<1]&&fl[k<<1|1]&&rv[k<<1]==lv[k<<1|1]) sum[k]+=R[k<<1]+L[k<<1|1];
	if (fr[k<<1]&&rv[k<<1]>lv[k<<1|1]) sum[k]+=R[k<<1];
	if (fl[k<<1|1]&&lv[k<<1|1]>rv[k<<1]) sum[k]+=L[k<<1|1];
	if (L[k<<1]==mid-l+1){
		if (rv[k<<1]==lv[k<<1|1]) L[k]=L[k<<1]+L[k<<1|1],fl[k]=fl[k<<1|1];
		else L[k]=L[k<<1],fl[k]=(rv[k<<1]>lv[k<<1|1]);
	}else L[k]=L[k<<1],fl[k]=fl[k<<1];
	if (R[k<<1|1]==r-mid){
		if (lv[k<<1|1]==rv[k<<1]) R[k]=R[k<<1|1]+R[k<<1],fr[k]=fr[k<<1];
		else R[k]=R[k<<1|1],fr[k]=(lv[k<<1|1]>rv[k<<1]);
	}else R[k]=R[k<<1|1],fr[k]=fr[k<<1|1];
}
void buildtree(int k,int l,int r){
	if (l==r){
		lv[k]=rv[k]=H; L[k]=R[k]=1;
		return;
	}
	int mid=l+r>>1;
	buildtree(k<<1,l,mid);
	buildtree(k<<1|1,mid+1,r);
	change(k,l,r,mid);
}
void pushdown(int k){
	if (!lazy[k]) return;
	lazy[k<<1]+=lazy[k]; lazy[k<<1|1]+=lazy[k];
	lv[k<<1]+=lazy[k]; lv[k<<1|1]+=lazy[k];
	rv[k<<1]+=lazy[k]; rv[k<<1|1]+=lazy[k];
	lazy[k]=0;
}
void change(int k,int l,int r,int L,int R,int num){
	if (l>R||r<L) return;
	if (l>=L&&r<=R){
		lazy[k]+=num; lv[k]+=num; rv[k]+=num;
		return;
	}
	pushdown(k);
	int mid=l+r>>1;
	change(k<<1,l,mid,L,R,num);
	change(k<<1|1,mid+1,r,L,R,num);
	change(k,l,r,mid);
}

int main(){
	judge();
	ios::sync_with_stdio(false);
	cin>>n>>Q>>H;
	if (n<=1000){
		for (int i=1;i<=n;i++) a[i]=H;
		for (int i=1;i<=Q;i++){
			int l,r,v; cin>>l>>r>>v;
			for (int j=l;j<=r;j++) a[j]+=v;
			l=r=-1;
			int now=0,ans=0;
			//for (int j=1;j<=n;j++) cout<<a[j]<<" ";
			//cout<<endl;
			for (int j=2;j<n;j++){
				if (r==-1){
					l=r=j; now=1;
					continue;
				}
				if (a[j]==a[r]) now++,r++;
				else{
					if (a[l-1]<a[l]&&a[r+1]<a[r]) ans+=now;
					l=r=j,now=1;
				}
			}
			//cout<<l<<" "<<r<<endl;
			if (a[l]>a[l-1]&&a[r]>a[r+1]) ans+=now;
			cout<<ans<<endl;
		}
	}else{
		buildtree(1,1,n);
		for (int i=1;i<=Q;i++){
			int l,r,v; cin>>l>>r>>v;
			change(1,1,n,l,r,v);
			cout<<sum[1]<<endl;
		}
	}
	return 0;
}
