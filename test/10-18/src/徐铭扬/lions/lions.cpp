#include<bits/stdc++.h>
using namespace std;
const int N=100002;
struct node{
	int x,id;
	friend bool operator <(node a,node b){
		return a.x<b.x || a.x==b.x && a.id<b.id;
	}
}t1,t2,a[N];
vector<int>kill[N];
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
void wri(int x){if(x<0)putchar('-'),x=-x;if(x>=10)wri(x/10);putchar(x%10|48);}
void wln(int x){wri(x);putchar('\n');}
int n,i,k,ans[N],cnt,ki[N],be[N],s[N];
set<node>S;
set<node>::iterator it;
int main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=rd();
	for (i=1;i<=n;i++) a[i].x=rd(),a[i].id=i,S.insert(a[i]);
	for (i=1;i<n;i++){
		it=S.begin();
		t1=*it;
		S.erase(t1);
		it=S.end();it--;
		t2=*it;
		S.erase(t2);
		t2.x-=t1.x;
		S.insert(t2);
		ans[++cnt]=t1.id;
		ki[i]=t2.id;
		be[i]=t1.id;
		kill[t2.id].push_back(i);
	}
	for (i=1;i<=n;i++) s[i]=kill[i].size()-1;
	k=n-1;
	for (i=n-1;i;i--){
		while (s[be[i]]>0 && kill[be[i]][s[be[i]]]>k) s[be[i]]--;
		if (s[be[i]]>=0 && kill[be[i]][s[be[i]]]<k) k=kill[be[i]][s[be[i]]]-1;
	}
	wln(k);
	sort(ans+1,ans+k+1);
	for (i=1;i<k;i++) wri(ans[i]),putchar(' ');
	wri(ans[k]);
}
