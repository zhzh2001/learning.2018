#include<bits/stdc++.h>
using namespace std;
const int N=50001;
int n,m,k,i,l,r,v,a[N],j,ans;
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
void wri(int x){if(x<0)putchar('-'),x=-x;if(x>=10)wri(x/10);putchar(x%10|48);}
void wln(int x){wri(x);putchar('\n');}
int main(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	n=rd();m=rd();k=rd();
	while (m--){
		l=rd();r=rd();v=rd();
		for (i=l;i<=r;i++) a[i]+=v;
		ans=0;
		for (i=2;i<n;i=j){
			j=i+1;
			while (j<n && a[j]==a[j-1]) j++;
			if (a[i-1]<a[i] && a[j]<a[i]) ans+=j-i;
		}
		wln(ans);
	}
}
