#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
struct ppap{int v1,v2;}a[N];
bool operator <(ppap a,ppap b){
	return a.v1==b.v1?a.v2<b.v2:a.v1<b.v1;
}
int b[N],n,d[N];
set<ppap>s;
set<ppap>::iterator it;
inline bool cmp(ppap a,ppap b){return d[a.v2]>d[b.v2];}
int main()
{
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i].v1=read();
		a[i].v2=i;
		s.insert(a[i]);
	}
	for(int i=1;i<n;i++){
		it=s.end();it--;
		ppap now=*it,qaq=*s.begin();
		d[qaq.v2]=b[now.v2]=i;now.v1-=qaq.v1;
		s.erase(it);s.erase(s.begin());
		s.insert(now);
	}
	int ans=n-1;
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++)if(d[a[i].v2]){
		if(ans>=d[a[i].v2]){
			if(b[a[i].v2])ans=min(b[a[i].v2]-1,ans);
		}
	}
	writeln(ans);
	for(int i=1;i<=n;i++)
		if(d[i]&&d[i]<=ans)write(i),putchar(' ');
	return 0;
}