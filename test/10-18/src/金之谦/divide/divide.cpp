#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=2e6+10;
int n,c[20],fa[N],b[N],s[N];
int main()
{
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	n=read();
	for(int i=1;i<10;i++)c[i]=read();
	for(int i=2;i<=n;i++)fa[i]=read();
	for(int j=1;j<11;j++){
		printf("Case #%d:\n",j);
		memset(b,0,sizeof b);
		memset(s,0,sizeof s);
		for(int i=n;i;i--){
			s[i]++;
			s[fa[i]]+=s[i];
		}
		for(int i=1;i*i<=n;i++)if(n%i==0){
			for(int k=n;k;k--){
				if(s[k]%i==0)cnt++;
			}
			if(cnt%i==0)b[i]=1;
		}
		for(int i=2;i<=n;i++)fa[i]=((fa[i]+c[j])%(i-1)+i-1)%(i-1)+1;
		for(int i=1;i<=n;i++)if(b[i])writeln(i);
	}
	return 0;
}