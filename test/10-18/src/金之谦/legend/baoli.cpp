#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=50010;
int n,Q,H,pp;
int K[N],lk[N],rk[N],a[N],lma[N],rma[N],na[N],lj[N];
inline void upd(int x){
	lma[x]=lk[x];
	for(int i=lk[x]+1;i<=rk[x];i++){
		if(a[i]==a[i-1])lma[x]=i;
		else{
			if(a[i]>a[i-1])lma[x]=-1;
			break;
		}
	}
	rma[x]=rk[x];
	for(int i=rk[x]-1;i>=lk[x];i--){
		if(a[i]==a[i+1])rma[x]=i;
		else{
			if(a[i]>a[i+1])rma[x]=-1;
			break;
		}
	}
	na[x]=0;int l=-1;
	for(int i=lk[x]+1;i<rk[x];i++){
		if(a[i]>a[i-1])l=i;
		if(a[i]>=a[i-1]&&a[i]>a[i+1]&&l>-1){
			na[x]+=i-l+1;
			l=-1;
		}
	}
}
int main()
{
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	n=read();Q=read();H=read();
	pp=sqrt(n);
	for(int i=1;i<=n;i++){
		a[i]=H;
		K[i]=(i-1)/pp+1;
		if(K[i]!=K[i-1]){
			rk[K[i]-1]=i-1;
			lk[K[i]]=i;
		}
	}
	rk[K[n]]=n;
	int cnt=K[n];
	for(int i=1;i<=cnt;i++){
		lma[i]=rk[i];
		rma[i]=lk[i];
		na[i]=0;
	}
	while(Q--){
		int l=read(),r=read(),v=read();
		if(K[l]==K[r]){
			for(int i=l;i<=r;i++)a[i]+=v;
			upd(K[l]);
		}else{
			for(int i=K[l]+1;i<K[r];i++)lj[i]+=v;
			for(int i=l;i<=rk[K[l]];i++)a[i]+=v;
			for(int i=lk[K[r]];i<=r;i++)a[i]+=v;
			upd(K[l]);upd(K[r]);
		}
		int ans=na[1],lt;
		if(rma[1]!=1)lt=rma[1];else lt=-1;
		for(int i=2;i<=cnt;i++){
			ans+=na[i];
			if(a[rk[i-1]]+lj[i-1]<a[lk[i]]+lj[i])lt=lk[i];
			else if(a[rk[i-1]]+lj[i-1]>a[lk[i]]+lj[i]){
				if(lt!=-1)ans+=rk[i-1]-lt+1;
				lt=-1;
			}
			if(lma[i]!=rk[i]){
				if(lt!=-1&&lma[i]>-1)ans+=lma[i]-lt+1;
				lt=rma[i];
			}
		}
		writeln(ans);
	}
	return 0;
}