#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("lions.in", "r", stdin);
	freopen("lions.out","w",stdout);
}
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' &&  ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x){if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
const int N = 100010;
int n;
struct T {
	int i, v;
}a[N];
bool operator <(T _x, T _y) {
	if(_x.v != _y.v) return _x.v < _y.v;
	return _x.i < _y.i;
}
int kill[N];
int main() {
	setIO();
	n = read();
	for(int i = 1; i <= n; ++i)
		a[i].v = read(), a[i].i = i;
	int res = 0;
	for(int i = 1; i <= n; ++i)
		if(a[i].v == 0) kill[i] = 1, ++res;
	writeln(res);
	for(int i = 1; i <= n; ++i)
		if(kill[i])	
			printf("%d ", i);
	puts("");
	
	return 0;
}
