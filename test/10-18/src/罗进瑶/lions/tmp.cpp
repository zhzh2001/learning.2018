#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("lions.in", "r", stdin);
	freopen("lions.out","w",stdout);
}
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' &&  ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x){if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
const int N = 100010;
struct T {
	int i, v;
	bool operator <(const T rhs) const {
		if(rhs.v == v) return i < rhs.i;
		return v < rhs.v;
	}
}a[N];
set<T>v;
int n, kill[N], cut[N];
bool check(int x) {
	v.clear();
	for(int i = 1; i <= n; ++i) v.insert(a[i]), kill[i] = cut[i] = 0;
	set<T>::iterator l, r;
	while(x--) {
		l = v.begin();
		r = v.end();
		--r;
		T x = *l, y = *r;
		v.erase(l);v.erase(r);
		cut[y.i] = 1;
		kill[x.i] = 1;
		printf("%d->%d %d\n", y.i, x.i, y.v - x.v);
		v.insert((T){y.i, y.v - x.v});
	}
	for(int i = 1; i <= n; ++i)
		if(kill[i] && cut[i]) return 0;
	return 1;
}
int main() {
	n = read();
	for(int i = 1; i <= n; ++i)
		a[i].v = read(), a[i].i = i;
	int l = 1, r = n;
/*	while(r - l > 1) {
		int mid = (l + r) >> 1;
		if(check(mid)) l = mid;
		else r = mid;
	}*/
	writeln(check(2));
/*	check(l);
	int res = 0;
	for(int i = 1; i <= n; ++i)
		res += kill[i];
	writeln(res);
	for(int i = 1; i <= n; ++i)
		if(kill[i])
			printf("%d ", i);*/
	return 0;
}
