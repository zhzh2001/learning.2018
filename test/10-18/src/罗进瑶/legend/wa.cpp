#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("legend.in", "r", stdin);
	freopen("legend.out","w",stdout);
}
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' &&  ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x){if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
int n, Q, h;
const int N = 51000;
int ls[N * 4], rs[N * 4], tag[N * 4];
struct Tree {
	int mx, mn, llen, rlen, vl, vr, len;
}t[N * 4];
#define lch (o<<1)
#define rch (o<<1|1)
void up(int o) {
	t[o].mx = max(t[lch].mx, t[rch].mx);
	t[o].mn = min(t[lch].mn, t[rch].mn);
	if(t[o].mx == t[o].mn) t[o].llen = t[o].rlen = rs[o] - ls[o] + 1;
	else {
		t[o].vl = t[lch].vl;
		t[o].llen = t[lch].llen;
		t[o].vr = t[rch].vr;
		t[o].rlen = t[rch].rlen;
		if(t[lch].vr == t[o].vr) t[o].rlen += t[lch].rlen;
		if(t[rch].vl == t[o].vl) t[o].llen += t[rch].llen;
		t[o].len = t[lch].len + t[rch].len;
		if(t[lch].vr > t[rch].vl) t[o].len -= t[rch].llen;
		else if(t[rch].vl > t[lch].vr) t[o].len -= t[lch].rlen;
	}
}
void down(int o) {
	if(tag[o]) {
		t[lch].vr += tag[o];
		t[rch].vr += tag[o];
		t[lch].mn += tag[o];
		t[rch].mx += tag[o];
		tag[lch] += tag[o];
		tag[rch] += tag[o];
		tag[o] = 0;
	}
}
void build(int o, int l, int r) {
	if(l == r) {
		t[o].vl = t[o].vr = t[o].mx = t[o].mn = h;
		t[o].len = t[o].llen = t[o].rlen = 1;
		return;
	}
	int mid = (l + r) >> 1;
	if(l <= mid) build(lch, l, mid);
	if(r > mid) build(rch, mid + 1, r);
	up(o);
}
void insert(int o, int l, int r, int val) {
	if(l <= ls[o] && r >= rs[o]) {
		t[o].vl += val;
		t[o].vr += val;
		t[o].mx += val;
		t[o].mn += val;
		tag[o] += val;
	}
	int mid = (ls[o] + rs[o]) >> 1;
	if(l <= mid) insert(lch, l,r , val);
	if(r > mid) insert(rch, l, r, val);
	up(o);
}
int main() {
	n = read(), Q = read(), h = read();
	for(int i = 1; i <= Q; ++i) {
		int x = read(), y = read();
		
	}
	return 0;
}
