#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("legend.in", "r", stdin);
	freopen("legend.out","w",stdout);
}
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' &&  ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x){if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
int n, Q, h;
const int N = 51000;
int a[N];
int main() {
	setIO();
	n = read(), Q = read(), h = read();
	if(n > 1100) {
		puts("0");
		return 0;
	}
	for(int i = 1; i <= n; ++i) a[i] = h;
	for(int i = 1; i <= Q; ++i) {
		int x = read(), y = read(), z = read();
		for(int j = x; j <= y; ++j)
			a[j] += z;
		int res = 0;
		for(int j = 2; j < n; ++j) {
			int k = j;
			while(k < n && a[k] == a[j]) ++k;
			if(a[j] > a[j - 1] && a[k] < a[k - 1]) res += k - j;
		}
		writeln(res);
	}
	return 0;
}
