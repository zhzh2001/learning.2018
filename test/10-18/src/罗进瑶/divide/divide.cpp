#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("divide.in", "r", stdin);
	freopen("divide.out","w",stdout);
}
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' &&  ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x){if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
int main() {
	setIO();
	puts("0");
	return 0;
}
