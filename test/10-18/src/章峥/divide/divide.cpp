#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("divide.in");
ofstream fout("divide.out");
const int k=9,N=1200005;
int n,f[N],c[k],cnt[N],sz[N];
void solve()
{
	fill(sz+1,sz+n+1,0);
	for(int i=n;i;i--)
	{
		sz[i]++;
		sz[f[i]]+=sz[i];
	}
	fill(cnt+1,cnt+n+1,0);
	for(int i=1;i<=n;i++)
		cnt[sz[i]]++;
	for(int i=1;i<=n;i++)
		if(n%i==0)
		{
			int now=0;
			for(int j=i;j<=n;j+=i)
				now+=cnt[j];
			if(now==n/i)
				fout<<i<<'\n';
		}
}
int main()
{
	fin>>n;
	for(int i=0;i<k;i++)
		fin>>c[i];
	for(int i=2;i<=n;i++)
		fin>>f[i];
	fout<<"Case #1:\n";
	solve();
	for(int i=0;i<k;i++)
	{
		fout<<"Case #"<<i+2<<":\n";
		for(int j=2;j<=n;j++)
			f[j]=((f[j]+c[i])%(j-1)+j-1)%(j-1)+1;
		solve();
	}
	return 0;
}
