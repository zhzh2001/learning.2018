#include<fstream>
#include<random>
#include<windows.h>
#include<algorithm>
using namespace std;
ofstream fout("legend.in");
const int n=50000,m=10000;
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<' '<<m<<' '<<n<<endl;
	for(int i=1;i<=m;i++)
	{
		uniform_int_distribution<> dn(1,n),dv(-1000,1000);
		int l=dn(gen),r=dn(gen);
		if(l>r)
			swap(l,r);
		fout<<l<<' '<<r<<' '<<dv(gen)<<endl;
	}
	return 0;
}
