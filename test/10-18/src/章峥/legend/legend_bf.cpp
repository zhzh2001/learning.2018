#include<fstream>
using namespace std;
ifstream fin("legend.in");
ofstream fout("legend.ans");
const int N=50005;
int a[N];
int main()
{
	int n,q,h;
	fin>>n>>q>>h;
	while(q--)
	{
		int l,r,v;
		fin>>l>>r>>v;
		for(int i=l;i<=r;i++)
			a[i]+=v;
		int ans=0;
		for(int i=1;i<n;)
			if(a[i]<a[i+1])
			{
				int j=i+1;
				for(;j<n&&a[j]==a[i+1];j++);
				if(a[j]<a[i+1])
					ans+=j-i-1;
				if(j==n)
					break;
				i=j-1;
			}
			else
				i++;
		fout<<ans<<endl;
	}
	return 0;
}
