#include<fstream>
using namespace std;
ifstream fin("legend.in");
ofstream fout("legend.out");
const int N=50005,UNDEF=1e9;
struct node
{
	int ll,rl,lv,rv,lnv,rnv,lazy,inside;
}tree[N*4];
inline void pullup(int id,int l,int r)
{
	tree[id].inside=tree[id*2].inside+tree[id*2+1].inside;
	tree[id].ll=tree[id*2].ll;
	tree[id].lv=tree[id*2].lv;
	tree[id].lnv=tree[id*2].lnv;
	tree[id].rl=tree[id*2+1].rl;
	tree[id].rv=tree[id*2+1].rv;
	tree[id].rnv=tree[id*2+1].rnv;
	if(tree[id*2].rv==tree[id*2+1].lv&&tree[id*2].rnv!=UNDEF&&tree[id*2].rnv<tree[id*2].rv&&tree[id*2+1].lnv!=UNDEF&&tree[id*2+1].lnv<tree[id*2+1].lv)
		tree[id].inside+=tree[id*2].rl+tree[id*2+1].ll;
	if(tree[id*2].rnv!=UNDEF&&tree[id*2].rnv<tree[id*2].rv&&tree[id*2+1].lv<tree[id*2].rv)
		tree[id].inside+=tree[id*2].rl;
	if(tree[id*2+1].lnv!=UNDEF&&tree[id*2+1].lnv<tree[id*2+1].lv&&tree[id*2].rv<tree[id*2+1].lv)
		tree[id].inside+=tree[id*2+1].ll;
	int mid=(l+r)/2;
	if(tree[id*2].ll==mid-l+1)
		if(tree[id*2].lv==tree[id*2+1].lv)
		{
			tree[id].ll+=tree[id*2+1].ll;
			tree[id].lnv=tree[id*2+1].lnv;
		}
		else
			tree[id].lnv=tree[id*2+1].lv;
	if(tree[id*2+1].rl==r-mid)
		if(tree[id*2+1].rv==tree[id*2].rv)
		{
			tree[id].rl+=tree[id*2].rl;
			tree[id].rnv=tree[id*2].rnv;
		}
		else
			tree[id].rnv=tree[id*2].rv;
}
void build(int id,int l,int r)
{
	if(l==r)
	{
		tree[id].ll=tree[id].rl=1;
		tree[id].lnv=tree[id].rnv=UNDEF;
	}
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		pullup(id,l,r);
	}
}
inline void pushdown(int id)
{
	if(tree[id].lazy)
	{
		tree[id*2].lazy+=tree[id].lazy;
		tree[id*2+1].lazy+=tree[id].lazy;
		tree[id*2].lv+=tree[id].lazy;
		tree[id*2].rv+=tree[id].lazy;
		if(tree[id*2].lnv!=UNDEF)
			tree[id*2].lnv+=tree[id].lazy;
		if(tree[id*2].rnv!=UNDEF)
			tree[id*2].rnv+=tree[id].lazy;
		tree[id*2+1].lv+=tree[id].lazy;
		tree[id*2+1].rv+=tree[id].lazy;
		if(tree[id*2+1].lnv!=UNDEF)
			tree[id*2+1].lnv+=tree[id].lazy;
		if(tree[id*2+1].rnv!=UNDEF)
			tree[id*2+1].rnv+=tree[id].lazy;
		tree[id].lazy=0;
	}
}
void modify(int id,int l,int r,int L,int R,int delta)
{
	if(L<=l&&R>=r)
	{
		tree[id].lazy+=delta;
		tree[id].lv+=delta;
		tree[id].rv+=delta;
		if(tree[id].lnv!=UNDEF)
			tree[id].lnv+=delta;
		if(tree[id].rnv!=UNDEF)
			tree[id].rnv+=delta;
	}
	else
	{
		pushdown(id);
		int mid=(l+r)/2;
		if(L<=mid)
			modify(id*2,l,mid,L,R,delta);
		if(R>mid)
			modify(id*2+1,mid+1,r,L,R,delta);
		pullup(id,l,r);
	}
}
int main()
{
	int n,q,h;
	fin>>n>>q>>h;
	build(1,1,n);
	while(q--)
	{
		int l,r,v;
		fin>>l>>r>>v;
		modify(1,1,n,l,r,v);
		fout<<tree[1].inside<<endl;
	}
	return 0;
}
