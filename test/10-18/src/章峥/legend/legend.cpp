#include<fstream>
using namespace std;
ifstream fin("legend.in");
ofstream fout("legend.out");
const int M=3e6,UNDEF=1e9,L=M-1,R=M-2;
struct node
{
	int ls,rs,ll,rl,lv,rv,lnv,rnv,lazy,inside;
}tree[M];
int cc;
inline void pullup(int id,int l,int r)
{
	int mid=(l+r)/2;
	if(!tree[id].ls)
	{
		tree[id].ls=L;
		tree[L].ll=tree[L].rl=mid-l+1;
	}
	if(!tree[id].rs)
	{
		tree[id].rs=R;
		tree[R].ll=tree[R].rl=r-mid;
	}
	tree[id].inside=tree[tree[id].ls].inside+tree[tree[id].rs].inside;
	tree[id].ll=tree[tree[id].ls].ll;
	tree[id].lv=tree[tree[id].ls].lv;
	tree[id].lnv=tree[tree[id].ls].lnv;
	tree[id].rl=tree[tree[id].rs].rl;
	tree[id].rv=tree[tree[id].rs].rv;
	tree[id].rnv=tree[tree[id].rs].rnv;
	if(tree[tree[id].ls].rv==tree[tree[id].rs].lv&&tree[tree[id].ls].rnv!=UNDEF&&tree[tree[id].ls].rnv<tree[tree[id].ls].rv&&tree[tree[id].rs].lnv!=UNDEF&&tree[tree[id].rs].lnv<tree[tree[id].rs].lv)
		tree[id].inside+=tree[tree[id].ls].rl+tree[tree[id].rs].ll;
	if(tree[tree[id].ls].rnv!=UNDEF&&tree[tree[id].ls].rnv<tree[tree[id].ls].rv&&tree[tree[id].rs].lv<tree[tree[id].ls].rv)
		tree[id].inside+=tree[tree[id].ls].rl;
	if(tree[tree[id].rs].lnv!=UNDEF&&tree[tree[id].rs].lnv<tree[tree[id].rs].lv&&tree[tree[id].ls].rv<tree[tree[id].rs].lv)
		tree[id].inside+=tree[tree[id].rs].ll;
	if(tree[tree[id].ls].ll==mid-l+1)
		if(tree[tree[id].ls].lv==tree[tree[id].rs].lv)
		{
			tree[id].ll+=tree[tree[id].rs].ll;
			tree[id].lnv=tree[tree[id].rs].lnv;
		}
		else
			tree[id].lnv=tree[tree[id].rs].lv;
	if(tree[tree[id].rs].rl==r-mid)
		if(tree[tree[id].rs].rv==tree[tree[id].ls].rv)
		{
			tree[id].rl+=tree[tree[id].ls].rl;
			tree[id].rnv=tree[tree[id].ls].rnv;
		}
		else
			tree[id].rnv=tree[tree[id].ls].rv;
	if(tree[id].ls==L)
		tree[id].ls=0;
	if(tree[id].rs==R)
		tree[id].rs=0;
}
inline void pushdown(int id,int l,int r)
{
	if(tree[id].lazy)
	{
		int mid=(l+r)/2;
		if(!tree[id].ls)
		{
			tree[id].ls=++cc;
			tree[cc].ll=tree[cc].rl=mid-l+1;
			tree[cc].lnv=tree[cc].rnv=UNDEF;
		}
		if(!tree[id].rs)
		{
			tree[id].rs=++cc;
			tree[cc].ll=tree[cc].rl=r-mid;
			tree[cc].lnv=tree[cc].rnv=UNDEF;
		}
		tree[tree[id].ls].lazy+=tree[id].lazy;
		tree[tree[id].rs].lazy+=tree[id].lazy;
		tree[tree[id].ls].lv+=tree[id].lazy;
		tree[tree[id].ls].rv+=tree[id].lazy;
		if(tree[tree[id].ls].lnv!=UNDEF)
			tree[tree[id].ls].lnv+=tree[id].lazy;
		if(tree[tree[id].ls].rnv!=UNDEF)
			tree[tree[id].ls].rnv+=tree[id].lazy;
		tree[tree[id].rs].lv+=tree[id].lazy;
		tree[tree[id].rs].rv+=tree[id].lazy;
		if(tree[tree[id].rs].lnv!=UNDEF)
			tree[tree[id].rs].lnv+=tree[id].lazy;
		if(tree[tree[id].rs].rnv!=UNDEF)
			tree[tree[id].rs].rnv+=tree[id].lazy;
		tree[id].lazy=0;
	}
}
void modify(int& id,int l,int r,int L,int R,int delta)
{
	if(!id)
	{
		id=++cc;
		tree[id].ll=tree[id].rl=r-l+1;
		tree[id].lnv=tree[id].rnv=UNDEF;
	}
	if(L<=l&&R>=r)
	{
		tree[id].lazy+=delta;
		tree[id].lv+=delta;
		tree[id].rv+=delta;
		if(tree[id].lnv!=UNDEF)
			tree[id].lnv+=delta;
		if(tree[id].rnv!=UNDEF)
			tree[id].rnv+=delta;
	}
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(L<=mid)
			modify(tree[id].ls,l,mid,L,R,delta);
		if(R>mid)
			modify(tree[id].rs,mid+1,r,L,R,delta);
		pullup(id,l,r);
	}
}
int main()
{
	int n,q,h;
	fin>>n>>q>>h;
	int root=0;
	tree[L].lnv=tree[L].rnv=tree[R].lnv=tree[R].rnv=UNDEF;
	while(q--)
	{
		int l,r,v;
		fin>>l>>r>>v;
		modify(root,1,n,l,r,v);
		fout<<tree[1].inside<<'\n';
	}
	return 0;
}
