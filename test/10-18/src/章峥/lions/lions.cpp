#include<fstream>
#include<set>
#include<algorithm>
using namespace std;
ifstream fin("lions.in");
ofstream fout("lions.out");
const int N=100005;
bool flag[N],ans[N];
int id[N];
struct lion
{
	int ability,age;
	lion(int ability,int age):ability(ability),age(age){}
	bool operator<(const lion& rhs)const
	{
		if(ability==rhs.ability)
			return age>rhs.age;
		return ability>rhs.ability;
	}
	bool operator==(const lion& rhs)const
	{
		return age==rhs.age;
	}
};
int main()
{
	int n;
	fin>>n;
	set<lion> S;
	for(int i=1;i<=n;i++)
	{
		int x;
		fin>>x;
		S.insert(lion(x,i));
	}
	for(int i=1;i<n;i++)
	{
		lion hi=*S.begin(),lo=*S.rbegin();
		id[i]=lo.age;
		lion now(hi.ability-lo.ability,hi.age);
		S.erase(S.begin());S.erase(--S.end());
		S.insert(now);
		flag[i]=S.size()>1&&*S.rbegin()==now;
	}
	for(int i=1;i<n;i++)
		if(flag[i])
		{
			int j=i+1;
			for(;j<n&&flag[j];j++);
			if((j-i)%2==0)
				ans[id[i]]=true;
			break;
		}
		else
			ans[id[i]]=true;
	fout<<count(ans+1,ans+n+1,true)<<endl;
	for(int i=1;i<=n;i++)
		if(ans[i])
			fout<<i<<' ';
	fout<<endl;
	return 0;
}
