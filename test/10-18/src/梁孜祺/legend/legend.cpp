#include<bits/stdc++.h>
#define N 200010
using namespace std;
#define ll long long
#define gc getchar
inline int read(){
  int x = 0; char ch = gc(); bool positive = 1;
  for (; !isdigit(ch); ch = gc()) if (ch == '-')  positive = 0;
  for (; isdigit(ch); ch = gc())  x = x * 10 + ch - '0';
  return positive ? x : -x;
}
const int oo = 1e9;
int n, Q, H;
struct que {
  int l, r, v, tag;
}q[N];
int f[N], pos[N], h[N];
int main() {
  freopen("legend.in","r",stdin);
  freopen("legend.out","w",stdout);
  n = read(); Q = read(); H = read();
  for (int i = 1; i <= Q; i++)
    q[i] = (que) {read(), read(), read()};
  for (int i = 1; i <= n; i++)
    f[i] = H;
  for (int i = 1; i <= Q; i++) {
    for (int j = q[i].l; j <= q[i].r; j++) f[j] += q[i].v;
    int sum = 0;
    for (int k = 0, j = 2; j < n; j++)
      if (f[j] > f[j-1]) {
        for (k = j; k < n && f[k] == f[j]; k++);
        if (f[k] < f[j]) sum += k-j;
        j = k-1;
      }
    printf("%d\n", sum);
  }
  return 0;
}