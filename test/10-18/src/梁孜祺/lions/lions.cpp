#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define gc getchar
inline int read(){
  int x = 0; char ch = gc(); bool positive = 1;
  for (; !isdigit(ch); ch = gc()) if (ch == '-')  positive = 0;
  for (; isdigit(ch); ch = gc())  x = x * 10 + ch - '0';
  return positive ? x : -x;
}
struct data {
  int value, year;
  bool operator < (const data & a) const {
    return (value == a.value)?(year > a.year):(value > a.value);
  }
};
#define N 100005
set <data> s;
const int oo = 1e9;
int n, B[N], A[N], c[N];
int main() {
  freopen("lions.in","r",stdin);
  freopen("lions.out","w",stdout);
  n = read();
  for (int i = 1; i <= n; i++) {
    A[i] = read();
    s.insert((data){A[i], i});
  }
  for (int i = 1; i < n; i++) {
    data a = *s.begin();
    s.erase(a);
    data b = *--s.end();
    s.erase(b);
    c[a.year] = i;
    data c = (data){a.value-b.value, a.year};
    B[++B[0]] = (b.year);
    s.insert(c);
  }
  int ans = oo;
  for (int i = n; i; i--)
    if (c[B[i]]&&ans > c[B[i]]) ans = c[B[i]], i = ans;
  sort(B+1,B+ans);
  printf("%d\n", ans-1);
  for (int i = 1; i < ans; i++)
    printf("%d ", B[i]);
  return 0;
}