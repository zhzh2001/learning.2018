#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int M=3500000,inf=1000000005;
int n,Q,nodecnt,tree[M],L[M],R[M],lson[M],rson[M],len[M];
map<int,int> A;
bool empty[M];
void push_up(int nod){
	tree[nod]=tree[lson[nod]]+tree[rson[nod]]+max(0,R[lson[nod]]+L[rson[nod]]+1);
	L[nod]=empty[lson[nod]]?len[lson[nod]]+L[rson[nod]]:L[lson[nod]];
	R[nod]=empty[rson[nod]]?len[rson[nod]]+R[lson[nod]]:R[rson[nod]];
	empty[nod]=empty[lson[nod]]&&empty[rson[nod]];
}
void make(int &nod,int l,int r){
	if(nod)return; nod=++nodecnt; empty[nod]=1; len[nod]=r-l+1; L[nod]=R[nod]=-inf;
}
void insert(int l,int r,int pos,int de,int &nod){
	if(l==r){
		A[l]+=de; int t=A[l]; tree[nod]=0; L[nod]=t<0?0:-inf; R[nod]=t>0?0:-inf;  empty[nod]=t==0; return;
	}
	int mid=(l+r)>>1;
	make(lson[nod],l,mid); make(rson[nod],mid+1,r);
	if(pos<=mid)insert(l,mid,pos,de,lson[nod]);
	else insert(mid+1,r,pos,de,rson[nod]);
	push_up(nod);
	//cout<<l<<" "<<r<<" "<<nod<<" "<<pos<<" "<<tree[nod]<<" "<<L[nod]<<" "<<R[nod]<<" "<<L[rson[nod]]<<" "<<empty[nod]<<endl;
}
int main(){
	freopen("legend.in","r",stdin); freopen("legend.out","w",stdout);
	n=read(); Q=read(); read(); int rt=0;
	make(rt,2,n);
	while(Q--){
		int l=read(),r=read(),w=read();
		if(l>1)insert(2,n,l,w,rt);
		if(r<n)insert(2,n,r+1,-w,rt);
		writeln(tree[1]);
	}
}
