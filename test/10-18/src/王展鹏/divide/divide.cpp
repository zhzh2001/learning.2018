#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=1200005;
int n,c[10],fa[N],sz[N],tong[N],ans[N];
int main(){
	freopen("divide.in","r",stdin); freopen("divide.out","w",stdout);
	n=read(); for(int i=1;i<=9;i++)c[i]=read();
	for(int i=2;i<=n;i++)fa[i]=read();
	for(int i=0;i<10;i++){
		if(i)for(int j=2;j<=n;j++)fa[j]=((fa[j]+c[i])%(j-1)+j-1)%(j-1)+1;
		memset(sz,0,sizeof(sz)); memset(tong,0,sizeof(tong));
		for(int j=n;j;j--){
			sz[fa[j]]+=++sz[j]; tong[sz[j]]++;
		}
		int tot=0;
		for(int j=1;j<=n;j++)if(n%j==0){
			int jb=0;
			for(register int k=j;k<=n&&jb>=k/j-1;k+=j)jb+=tong[k];
			if(jb==n/j)ans[++tot]=j;
		}
		printf("Case #%d:\n",i+1);
		for(int j=1;j<=tot;j++)writeln(ans[j]);
	}
}
