#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=100005;
int n,tot,ans,vis[N],q[N];
PI a[N];
void bj(int x){
	q[++tot]=x; vis[x]=1;
}
void clear(int zs){
	while(tot>zs)vis[q[tot--]]=0;
}
set<PI> s;
set<PI>::iterator it;
void solve(){
	if(s.size()==1)return ;
	PI mx=*s.rbegin(),mn=*s.begin();
	s.erase(s.begin()); it=s.end(); it--; s.erase(it);
	s.insert(mp(mx.first-mn.first,mx.second));
	int zs=tot; bj(mn.second);
	solve();
	//for(int i=1;i<=n;i++)cout<<vis[i]<<" "; puts("");
	//cout<<mn.second<<" "<<mx.second<<" "<<endl;
	if(vis[mx.second])clear(zs);
}
int main(){
	freopen("lions.in","r",stdin); freopen("lions.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)s.insert(a[i]=mp(read(),i));
	solve();
	for(int i=1;i<=n;i++)if(vis[i])ans++;
	writeln(ans);
	for(int i=1;i<=n;i++)if(vis[i]){
		write(i); putchar(' ');
	}
}
/*
a1 a2 a3 a4 a5
a5-a1���ҽ��� 
*/
