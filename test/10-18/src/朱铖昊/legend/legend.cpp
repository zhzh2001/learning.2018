#include<bits/stdc++.h>
using namespace std;
#define pi pair<int,int>
pi g[10];
const int N=50005;
struct query
{
	int l,r,h;
}q[N];
int a[N*4],t[N*16];
int n,m,Q,h,L,cnt,ans;
set<int> s;
map<int,int> mp;
inline void read(int &x)
{
	char c=getchar();
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
inline void add(int x,int l,int r,int L,int R,int h)
{
	if (l==L&&r==R)
	{
		t[x]+=h;
		return;
	}
	int mid=(l+r)/2;
	if (L>mid)
		add(x*2+1,mid+1,r,L,R,h);
	if (R<=mid)
		add(x*2,l,mid,L,R,h);
	if (L<=mid&&R>mid)
	{
		add(x*2,l,mid,L,mid,h);
		add(x*2+1,mid+1,r,mid+1,R,h);
	}
}
inline int ask(int x,int l,int r,int y)
{
	if (l==r)
		return t[x];
	int mid=(l+r)/2;
	if (y<=mid)
		return t[x]+ask(x*2,l,mid,y);
	else
		return t[x]+ask(x*2+1,mid+1,r,y);
}
inline int calc(pi x)
{
	if (x.first==1||x.second==n+1)
		return 0;
	int p=ask(1,1,n,x.first),l=ask(1,1,n,x.first-1),r=ask(1,1,n,x.second);
	if (p>l&&p>r)
		return a[x.second]-a[x.first];
	else
		return 0;
}
inline pi find(int x)
{
	set<int>::iterator it=s.upper_bound(x),it1=it;
	--it;
	return (pi){*it,*it1};
}
inline int get(query x)
{
	g[0]=(pi){0,0};
	if (x.l!=1)
		g[1]=find(x.l-1);
	else
		g[1]=(pi){0,0};
	g[2]=find(x.l);
	g[3]=find(x.r);
	if (x.r!=n)
		g[4]=find(x.r+1);
	else
		g[4]=(pi){0,0};
	int nn=0,sum=0;
	for (int i=1;i<=4;++i)
		if (g[i]!=g[nn])
		{
			g[++nn]=g[i];
			sum+=calc(g[i]);
		}
	return sum;
}
inline void modify(query x)
{
	if (x.l!=1)
	{
		int l1=ask(1,1,n,x.l-1),l2=ask(1,1,n,x.l);
		if (l1==l2)
		{
			if (s.count(x.l))
				s.erase(x.l);
		}
		else
			s.insert(x.l);
	}
	if (x.r!=n)
	{
		int r1=ask(1,1,n,x.r),r2=ask(1,1,n,x.r+1);
		if (r1==r2)
		{
			if (s.count(x.r+1))
				s.erase(x.r+1);
		}
		else
			s.insert(x.r+1);
	}
}
int main()
{
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	read(L);
	read(Q);
	read(h);
	for (int i=1;i<=Q;++i)
	{
		read(q[i].l);
		read(q[i].r);
		read(q[i].h);
		a[++cnt]=q[i].l;
		a[++cnt]=q[i].r;
		a[++cnt]=q[i].l-1;
		a[++cnt]=q[i].r+1;
	}
	ans=0;
	sort(a+1,a+1+cnt);
	for (int i=1;i<=cnt;++i)
		if (a[i]!=a[n]&&a[i]<=L)
			a[++n]=a[i];
	for (int i=1;i<=n;++i)
		mp[a[i]]=i;
	for (int i=1;i<=Q;++i)
	{
		q[i].l=mp[q[i].l];
		q[i].r=mp[q[i].r];
	}
	t[1]=h;
	s.insert(1);
	s.insert(n+1);
	for (int i=1;i<=Q;++i)
	{
		// cout<<s.size()<<'\n';
		// for (auto it=s.begin();it!=s.end();++it)
		// 	cout<<*it<<' ';
		// puts("");
		// for (int j=1;j<=n;++j)
		// 	cout<<ask(1,1,n,j)<<' ';
		// puts("");
		ans-=get(q[i]);
		add(1,1,n,q[i].l,q[i].r,q[i].h);
		modify(q[i]);
		ans+=get(q[i]);
		printf("%d\n",ans);
	}
	return 0;
}