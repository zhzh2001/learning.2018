#include<bits/stdc++.h>
using namespace std;
#define pi pair<int,int>
const int N=100005;
int a[N],b[N],n,ti;
pi f[N];
set<pi> s;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int main()
{
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
	{
		read(a[i]);
		s.insert((pi){a[i],i});
	}
	for (int i=1;i<n;++i)
	{
		pi mn=*s.begin(),mx=*--s.end();
		// cout<<mx.first<<' '<<mn.first<<'\n';
		f[i]=pi(mx.second,mn.second);
		mx.first-=mn.first;
		s.erase(s.begin());
		s.erase(--s.end());
		s.insert(mx);
	}
	b[s.begin()->second]=1;
	ti=n;
	for (int i=n-1;i>=1;--i)
		if (!b[f[i].first])
		{
			for (int j=ti-1;j>=i;--j)
				b[f[j].second]=1;
			ti=i;
		}
	printf("%d\n",ti-1);
	for (int i=1;i<=n;++i)
		if (!b[i])
			printf("%d ",i);
	// cout<<'\n'<<clock()<<'\n';
	return 0;
}