#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=1200005;
int p[M],s[M],g[M];
int f[N];
int c[15],n,m;
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
inline char gc()
{
	if(S == T)
	{
		T = (S = STR) + fread(STR,1,LEN,stdin);
		if(S == T) 
			return EOF;
	}
	return *S++;
}
inline void read(int &x)
{
	char c=gc();
	while (c>'9'||c<'0')
		c=gc();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=gc();
	}
}
inline void readf(int &x)
{
	char c=gc();
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=gc();
	}
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=gc();
	}
	x*=y;
}
inline void writeln(int x)
{
	int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
inline void pre_work()
{
	for (int i=1;i<=n;++i)
		if (n%i==0)
			f[++m]=i;
}
inline void work(int x)
{
	// s[1]=1;
	// cout<<c[x]<<'\n';
	// for (int i=2;i<=n;++i)
	// {
	// 	p[i]=((a[i]+c[x])%(i-1)+i-1)%(i-1)+1;
	// 	s[i]=1;
	// }
	for (int i=1;i<=n;++i)
		s[i]=1;
	for (int i=n;i>=2;--i)
		s[p[i]]+=s[i];
	printf("Case #%d:\n",x);
	// for (int i=2;i<=n;++i)
	// 	cout<<p[i]<<' ';
	// putchar('\n');
	puts("1");
	if (x==1||(long long)n*m<=(long long)5e6)
	{
		for (int i=2;i<m;++i)
		{
			bool b=true;
			for (int j=n;j;--j)
			{
				g[j]++;
				if (g[j]>f[i])
				{
					b=false;
					if (j<=n/2)
					{
						for (int k=1;k<=n;++k)
							g[k]=0;
					}
					else
					{
						for (int k=j;k<=n;++k)
							g[k]=g[p[k]];
					}
					break;
				}
				if (g[j]!=f[i])
					g[p[j]]+=g[j];
			}
			if (b)
			{
				for (int j=1;j<=n;++j)
					g[j]=0;
				writeln(f[i]);
			}
		}
	}
	writeln(n);
	if (x!=10&&(long long)n*m<=(long long)5e6)
	{
		for (int i=2;i<=n;++i)
			p[i]=((p[i]+c[x])%(i-1)+i-1)%(i-1)+1;
	}
}
int main()
{
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	read(n);
	pre_work();
	// cout<<m;
	for (int i=1;i<=9;++i)
		readf(c[i]);
	for (int i=2;i<=n;++i)
		read(p[i]);
	for (int i=1;i<=10;++i)
		work(i);
	// cout<<clock();
	return 0;
}