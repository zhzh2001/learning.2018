#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

typedef pair<int, int> kotori;

multiset<kotori> st;

int ans[N], cnt;

int main(int argc, char const *argv[]) {
  freopen("lions.in", "r", stdin);
  freopen("lions.out", "w", stdout);

  int n = read();
  for (int i = 1; i <= n; ++ i) {
    int x = read();
    st.insert(make_pair(x, i));
  }

  while (st.size() > 3) {
    set<kotori>::iterator eater = -- st.end();
    set<kotori>::iterator food = st.begin();

    int eater_id = eater -> second;
    int food_id = food -> second;

    st.erase(eater);
    st.erase(food);
    st.insert(make_pair(eater -> first - food -> first, eater -> second));

    ans[++ cnt] = food_id;

    if (st.begin() -> second == eater_id) {
      break;
    }
  }

  sort(ans + 1, ans + cnt + 1);
  printf("%d\n", cnt);
  for (int i = 1; i <= cnt; ++ i) {
    printf("%d%c", ans[i], i == cnt ? '\n' : ' ');
  }

  return 0;
}