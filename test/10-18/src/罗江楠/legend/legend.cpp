#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

int n, q, h, max_size, xs[N];

struct __segment_tree {
  // 答案
  int ans[N<<2];
  // 右边是否是 _- | 右端点的值 | 右边相等的长度
  int rvd[N<<2], rval[N<<2], rlen[N<<2];
  // 左边是否是 -_ | 左端点的值 | 左边相等的长度
  int lvd[N<<2], lval[N<<2], llen[N<<2];
  // 是否全段都相等
  int all[N<<2];
  // 区间加的 lazy 标记
  int tg[N<<2];

  void push_down(int x) {
    int ls = x << 1, rs = x << 1 | 1;
    if (tg[x]) {
      rval[ls] += tg[x];
      lval[ls] += tg[x];
      rval[rs] += tg[x];
      lval[rs] += tg[x];
      tg[ls] += tg[x];
      tg[rs] += tg[x];
      tg[x] = 0;
    }
  }

  void push_up(int x, int l, int r) {
    int ls = x << 1, rs = x << 1 | 1;
    ans[x] = ans[ls] + ans[rs];
    all[x] = all[ls] && all[rs] && rval[ls] == lval[rs];
    rval[x] = rval[rs];
    lval[x] = lval[ls];
    rlen[x] = all[rs] && rval[ls] == lval[rs] ? rlen[ls] + rlen[rs] : rlen[rs];
    llen[x] = all[ls] && lval[rs] == rval[ls] ? llen[ls] + llen[rs] : llen[ls];
    rvd[x] = all[rs] ? ((rval[ls] == lval[rs] && rvd[ls]) || rval[ls] < lval[rs]) : (rvd[rs]);
    lvd[x] = all[ls] ? ((lval[rs] == rval[ls] && lvd[rs]) || lval[rs] < rval[ls]) : (lvd[ls]);
    
    if (rvd[ls] && lvd[rs] && rval[ls] == lval[rs]) {
      ans[x] += rlen[ls] + llen[rs];
    } else if (rvd[ls] && rval[ls] > lval[rs]) {
      ans[x] += rlen[ls];
    } else if (lvd[rs] && rval[ls] < lval[rs]) {
      ans[x] += llen[rs];
    }
  }

  void build(int x, int l, int r) {
    if (l == r) {
      ans[x] = 0;
      rvd[x] = false;
      lvd[x] = false;
      rlen[x] = xs[l + 1] - xs[l];
      llen[x] = xs[l + 1] - xs[l];
      rval[x] = (!l || l == max_size) ? 2e9 : h;
      lval[x] = (!l || l == max_size) ? 2e9 : h;
      all[x] = true;
      return;
    }
    int mid = (l + r) >> 1;
    build(x << 1, l, mid);
    build(x << 1 | 1, mid + 1, r);
    push_up(x, l, r);
  }

  void update(int x, int l, int r, int L, int R, int v) {
    // if (x == 1) printf("%d %d %d %d %d\n", x, l, r, L, R);
    if (l == L && r == R) {
      rval[x] += v;
      lval[x] += v;
      tg[x] += v;
      return;
    }
    push_down(x);
    int mid = (L + R) >> 1;
    if (r <= mid) update(x << 1, l, r, L, mid, v);
    else if (l > mid) update(x << 1 | 1, l, r, mid + 1, R, v);
    else update(x << 1, l, mid, L, mid, v), update(x << 1 | 1, mid + 1, r, mid + 1, R, v);
    push_up(x, L, R);
  }
}sg;

int ls[N], rs[N], cnt, vs[N];

int main(int argc, char const *argv[]) {
  freopen("legend.in", "r", stdin);
  freopen("legend.out", "w", stdout);

  n = read(); q = read(); h = read();

  for (int i = 1; i <= q; ++ i) {
    int l = read(), r = read(), v = read();
    xs[++ cnt] = l;
    xs[++ cnt] = r + 1;
    ls[i] = l;
    rs[i] = r + 1;
    vs[i] = v;
  }
  xs[++ cnt] = n + 1;
  sort(xs + 1, xs + cnt + 1);
  cnt = unique(xs + 1, xs + cnt + 1) - xs - 1;
  for (int i = 1; i <= q; ++ i) {
    ls[i] = lower_bound(xs + 1, xs + cnt + 1, ls[i]) - xs;
    rs[i] = lower_bound(xs + 1, xs + cnt + 1, rs[i]) - xs - 1;
  }
  max_size = cnt + 1;

  sg.build(1, 0, max_size);

  for (int i = 1; i <= q; ++ i) {
    sg.update(1, ls[i], rs[i], 0, max_size, vs[i]);
    printf("%d\n", sg.ans[1]);
  }

  return 0;
}