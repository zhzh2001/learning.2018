#include <bits/stdc++.h>
#define N 2000020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
int a[15], fa[N];

int to[N], nxt[N], head[N], cnt;
void insert(int x, int y) {
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}

// fr[x] -> x 的子树中空余的大小
int timber, n, fr[N];
bool dfs(int x) {
  fr[x] = 1;
  for (int i = head[x]; i; i = nxt[i]) {
    if (!dfs(to[i])) return false;
    fr[x] += fr[to[i]];
  }
  if (fr[x] > timber) {
    return false;
  }
  fr[x] = fr[x] % timber;
  return true;
}
int possible_solutions[N], _ps_top, _output_times;
void resolve() {
  printf("Case #%d:\n", ++ _output_times);
  for (int i = 1; i <= _ps_top; ++ i) {
    timber = possible_solutions[i];
    if (dfs(1)) printf("%d\n", timber);
  }
}

int main(int argc, char const *argv[]) {
  freopen("divide.in", "r", stdin);
  freopen("divide.out", "w", stdout);

  n = read();

  for (int i = 1; i * i <= n; ++ i) {
    if (n % i == 0) {
      possible_solutions[++ _ps_top] = (i);
      if (i * i < n) {
        possible_solutions[++ _ps_top] = (n / i);
      }
    }
  }
  sort(possible_solutions + 1, possible_solutions + _ps_top + 1);

  for (int i = 1; i <= 9; ++ i) {
    a[i] = read();
  }

  for (int i = 2; i <= n; ++ i) {
    fa[i] = read();
    insert(fa[i], i);
  }

  resolve();

  for (int i = 1; i <= 9; ++ i) {
    memset(head, 0, sizeof head);
    cnt = 0;
    for (int x = 2; x <= n; ++ x) {
      fa[x] = ((fa[x]+a[i])%(x-1)+x-1)%(x-1)+1;
      insert(fa[x], x);
    }
    resolve();
  }

  return 0;
}