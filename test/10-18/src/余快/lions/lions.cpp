/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans[N],tot,vis[N],f[N];
pa z[N];
struct xx{
	pa a;
	int i,j;
	friend bool operator<(xx a,xx b){
		return (a.a>b.a);
	}
	friend bool operator>(xx a,xx b){
		return (a.a<b.a);
	}
}a[N],b,c;
set<xx> q;
set<xx>::iterator it;
signed main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	F(i,1,n){
		a[i].a=mp(read(),i);
		a[i].i=i;
	}
	sort(a+1,a+n+1);F(i,1,n) a[i].j=i;
	F(i,1,n) q.insert(a[i]);
	while (!q.empty()){
		b=*q.be;
		it=q.en;it--;
		c=*it;
		if (b.a==c.a) break;
		q.erase(q.be);
		q.erase(it);
		b.a.fi-=c.a.fi;
		z[++tot]=mp(b.j,c.j);
		q.insert(b);
	}
	int t=tot;
	D(i,tot,1){
		if (!vis[z[i].fi]){
			vis[z[i].se]++;
			f[i]=1;
		}
		else{
			F(j,i+1,t) vis[z[j].se]--;
			t=i-1;
			f[i]=0;
		}
	}
	int cnt=0;
	F(i,1,tot){
		if (f[i]) ans[++cnt]=a[z[i].se].i;
		else break;
	}
	sort(ans+1,ans+cnt+1);
	wrn(cnt);
	F(i,1,cnt) wri(ans[i]);
	return 0;
}
