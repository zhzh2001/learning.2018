/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,hhh,q1,a[N],x,y,v;
set <pa> q;
set<pa>::iterator it,it2;
void ins(int l,int r){
	if (l>r) return;
	if (l>1&&r<n) ans+=(a[l-1]>0&&a[r]<0)?r-l+1:0;
	q.insert(mp(l,r));
}
int que(int l,int r){
	if (l==1||r==n) return 0;
	return (a[l-1]>0&&a[r]<0)?r-l+1:0;
}
void bing(int l1,int r1,int x,int l2,int r2,int k){
	int l=x,r=x;
	if (r1+1==l2){
		a[x]-=k;
		ans-=que(l1,r1);ans-=que(l2,r2);
		a[x]+=k;l=l1;r=r2;
		q.erase(mp(l1,r1));q.erase(mp(l2,r2));
		q.insert(mp(l,r));
		ans+=que(l,r);
	}
}
void change(int x,int k){
	if (x==0||k==0||x==n) return;
	a[x]+=k;
	int l=0,r=0;
	if (a[x]==0){
		it=q.lower_bound(mp(x+1,0));
		if (it==q.en){
			it2=it;it2--;
			bing((*it2).fi,(*it2).se,x,-1,-1,k);
			return;
		}
		if (it!=q.be){
			it2=it;it2--;
			bing((*it2).fi,(*it2).se,x,(*it).fi,(*it).se,k);
		}
		else{
			bing(-1,-1,x,(*it).fi,(*it).se,k);
		}
	}
	else{
		if (a[x]==k){
			it=q.lower_bound(mp(x+2,0));
			it--;
			if ((*it).fi<=x&&x<=((*it).se)){
				ans-=que((*it).fi,(*it).se);
				l=(*it).fi;r=(*it).se;q.erase(it);
				ins(l,x);ins(x+1,r);
				k=0;
			}
		}
		it=q.lower_bound(mp(x+1,0));
		if (it!=q.end()){
			a[x]-=k;ans-=que((*it).fi,(*it).se);a[x]+=k;
			ans+=que((*it).fi,(*it).se);
		}
		if (it!=q.be){
			it--;
			a[x]-=k;ans-=que((*it).fi,(*it).se);a[x]+=k;
			ans+=que((*it).fi,(*it).se);			
		}
	}
}
signed main(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	n=read();q1=read();hhh=read();
	q.insert(mp(1,n));
	while (q1--){
		x=read();y=read();v=read();
		change(x-1,v);
		change(y,-v);
		wrn(ans);
	}
	return 0;
}
