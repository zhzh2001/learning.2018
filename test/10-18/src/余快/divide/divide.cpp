/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 1200055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans[N],asyy,p,z[N],tot,t,num[N],fa[N],cnt,c[11],sum[N];
const int maxn=1200000;
bool vis[maxn+10];
int Next[N*2],head[N],to[N*2],nedge,pd,pp[N],ttt;
#define V to[i]
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;}
void dfs(int x){
	if (sum[x]<p){num[x]=sum[x];return;}
	num[x]=1;
	go(i,x){
		if (!pd) return;
		dfs(V);num[x]+=num[V];
		if (num[x]>p){
			pd=0;return;
		}
	}
	num[x]=(num[x]==p)?0:num[x];
}
void dfs2(int x){
	sum[x]=1;pp[x]=0;
	go(i,x){
		dfs2(V);
		sum[x]+=sum[V];
		if (sum[V]==1) pp[x]++;
	}
	ttt=max(ttt,pp[x]);
}
bool check(int x){
	if (ttt>=x) return 0;
	p=x;pd=1;
	dfs(1);
	return pd;
}
void solve(){
	printf("Case #%d:\n",++asyy);
	nedge=0;F(i,1,n) head[i]=0;ttt=0;
	F(i,2,n) add(fa[i],i);
	dfs2(1);
	ans[cnt=1]=1;ans[++cnt]=n;
	F(i,2,t){
		if (n/i*i==n){
			if (check(i)) ans[++cnt]=i;
			if (i!=n/i){
				if (check(n/i)) ans[++cnt]=n/i;
			}
		}
	}
	sort(ans+1,ans+cnt+1);
	F(i,1,cnt) wrn(ans[i]);
}
void solve2(){
	printf("Case #%d:\n",++asyy);
	ans[cnt=1]=1;ans[++cnt]=n;
	F(i,1,cnt) wrn(ans[i]);
}
signed main(){
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	n=read();t=sqrt(n);
	if (n>700000){
		solve2();
		F(ci,1,9) solve2();
		return 0;
	}
	/*F(i,2,maxn){
		if (!vis[i]) z[++tot]=i;
		for (int j=1;j<=tot&&z[j]<=maxn/i;j++){
			vis[i*z[j]]=1;if (i%z[j]==0) break;
		}
	}*/
	F(i,1,9) c[i]=read();
	F(i,2,n) fa[i]=read();
	solve();
	F(ci,1,9){
		F(i,2,n) fa[i]=((fa[i]+c[i])%(i-1)+i-1)%(i-1)+1;
		solve();
	}
	return 0;
}
