#include<bits/stdc++.h>
using namespace std;typedef long long ll;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef pair<int,int> pii;
const int inf = 0x3f3f3f3f,oo = inf;
#define pi 3.14159265358979323846
#define IL inline
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<int(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
#define pc putchar
#define gc getchar
IL ll read(){
    RG ll lson=0;char f=0;RG char c=gc();
    for(;!isdigit(c);c=gc())f|=(c=='-');
    for(;isdigit(c);c=gc())lson=(lson<<1)+(lson<<3)+(c^48);
    return f?-lson:lson;
}
IL void write(ll lson){if(lson<0)lson=-lson,pc('-');if(lson>=10)write(lson/10);pc(lson%10+'0');}
IL void writeln(ll lson){write(lson);puts("");}
IL void writeln(ll lson,char c,ll rson){write(lson);pc(c);writeln(rson);}
IL void writeln(ll lson,char c,ll rson,char d,ll z){write(lson);pc(c);write(rson);pc(d);writeln(z);}
#define debug(x) printf(#x" = %d\n",x);
#define rd() read()
#define rdb() readdb()
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define y1 ____y1
#define union _union
const int maxn = 100005;
int n,q,h;
int a[maxn];
/*ll calc(){
	int ans = 0;
	bool st = false;
	int cnt = 0;
	for(int i=2;i<=n+1;++i){
		if(a[i] > 0){
			cnt = 0;
			st = true;
		} else
		if(a[i] == 0){
			cnt++;
		} else{
			if(st){
				ans += cnt + 1;
				st = false;
			}
		}
	}
	return ans;
}*/
int calc(){
	int ans = 0;
	for(int i=2,j;i<=n;i=j+1){
		if(a[i] > a[i-1]){
			for(j=i;a[i]==a[j] && j<=n;j++);
			j--;
			if(j!=n && a[j]>a[j+1]) ans += (j-i+1);
		} else j = i;
	}
	return ans;
}
int main(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	n = rd(),q = rd();rd();
	Rep(i,2,n+1) a[i] = 0;
	while(q--){
		int l = rd(),r = rd(),v = rd();
		Rep(i,l,r) a[i] += v;
//		a[l] += v;
//		a[r+1] -= v;
//		Rep(i,1,n+1) {
//			printf("%d ",a[i]);
///		}puts("");
		writeln(calc());
	}
	return 0;
}
/*
5 5 5 5 5 
4 5 5 5 5 
4 5 5 4 4

*/
