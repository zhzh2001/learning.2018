#include<bits/stdc++.h>
using namespace std;typedef long long ll;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef pair<int,int> pii;
const int inf = 0x3f3f3f3f,oo = inf;
#define pi 3.14159265358979323846
#define IL inline
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<int(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
#define pc putchar
#define gc getchar
IL ll read(){
    RG ll lson=0;char f=0;RG char c=gc();
    for(;!isdigit(c);c=gc())f|=(c=='-');
    for(;isdigit(c);c=gc())lson=(lson<<1)+(lson<<3)+(c^48);
    return f?-lson:lson;
}
IL void write(ll lson){if(lson<0)lson=-lson,pc('-');if(lson>=10)write(lson/10);pc(lson%10+'0');}
IL void writeln(ll lson){write(lson);puts("");}
IL void writeln(ll lson,char c,ll rson){write(lson);pc(c);writeln(rson);}
IL void writeln(ll lson,char c,ll rson,char d,ll z){write(lson);pc(c);write(rson);pc(d);writeln(z);}
#define debug(x) printf(#x" = %d\n",x);
#define rd() read()
#define rdb() readdb()
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define y1 ____y1
#define union _union
const int maxn = 100005;
int a[maxn],n;
set<pair<int,int> > s;
bitset<maxn> dead;
pair<int,int> st[maxn];
int top = 0;
int x[maxn],y[maxn];
void solve(int dep){
	if(dep == n){
		return ;
	}//如果只剩下它了 ，显然全部会剩下
	st[++top] = make_pair(dep,y[dep]);
	dead[y[dep]] = true;
	solve(dep+1);
	if(dead[x[dep]]){
		while(st[top] . fi >= dep){
			dead[st[top].se] = false;
			top--;
		}
	}
}
int main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n = rd();
	Rep(i,1,n){
		a[i] = rd();
		s.insert(make_pair(a[i],i));
	}
	Rep(i,1,n-1){
		int A = s.begin() -> se,B = s.rbegin() -> se;
		s.erase(*s.begin());
		s.erase(*s.rbegin());
		x[i] = B;y[i] = A;
		a[B] -= a[A];
		s.insert(make_pair(a[B],B));
//		printf("%d kill %d\n",B,A);
	}
	solve(1);
	int ans = 0;
	Rep(i,1,n){
		if(dead[i]){
			ans++;
		}
	}writeln(ans);
	Rep(i,1,n){
		if(dead[i])	printf("%d ",i);
	}return 0;
	return 0;
}
