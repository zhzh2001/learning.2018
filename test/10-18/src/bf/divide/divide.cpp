#include<cstdio>
#include<cstring>
using namespace std;
int n,cnt,b[100],c[1300000],q[1300000],f[1300000];
inline int read(){
	char ch=getchar();
	int f=1,x=0;
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+(ch-'0');
		ch=getchar();
	}
	return x*f;
}
int main(){
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	n=read();
	for(int i=1;i<=9;i++)b[i]=read();
	for(int i=2;i<=n;i++)f[i]=read();
	for(int t=1;t<=10;t++){
		printf("Case #%d:\n",t);
		memset(c,0,sizeof(c));
		memset(q,0,sizeof(q));
		for(int i=n;i>=1;i--){
			q[i]++;
			if(i>1)q[f[i]]+=q[i];
			c[q[i]]++;
		}
		printf("%d\n",1);
		for(int i=2;i<=n-1;i++)
			if(n%i==0){
				cnt=n/i;
				for(int j=i;cnt>=0&&j<=n;j+=i)
					cnt-=c[j];
				if(cnt==0)printf("%d\n",i);	
			}
		printf("%d\n",n);
		for(int i=2;i<=n;i++)
			f[i]=((f[i]+b[t])%(i-1)+i-1)%(i-1)+1;
	}
}
