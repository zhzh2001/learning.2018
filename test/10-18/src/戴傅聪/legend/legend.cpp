#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cassert>
using namespace std;
#define maxn 500100
struct node
{
	int l,r;bool lc,rc;
	int llen,rlen;
	bool same;int cnt;
	inline void setadd(int add){l+=add;r+=add;}
};
inline node make_node(int val,int len)
{
	node ans;
	ans.l=ans.r=val;
	ans.lc=ans.rc=0;
	ans.llen=ans.rlen=len;
	ans.same=1;
	ans.cnt=0;
	return ans;
}
inline node Merge(node left,node right)
{
	node ans;
	ans.l=left.l;ans.r=right.r;
	ans.same=left.same&&right.same&&(left.r==right.l);
	ans.lc=left.lc;ans.rc=right.rc;
	if(left.same){if(left.r==right.l) ans.lc=right.lc;else ans.lc=left.r>right.l;}
	if(right.same){if(left.r==right.l) ans.rc=left.rc;else ans.rc=right.l>left.r;}
	ans.llen=left.llen;ans.rlen=right.rlen;
	if(left.same&&left.r==right.l) ans.llen=left.llen+right.llen;
	if(right.same&&left.r==right.l) ans.rlen=right.rlen+left.rlen;
	ans.cnt=left.cnt+right.cnt;
	if(left.r<right.l) if(right.lc) ans.cnt+=right.llen;
	if(left.r>right.l) if(left.rc) ans.cnt+=left.rlen;
	if(left.r==right.l) if(left.rc&&right.lc) ans.cnt+=left.rlen+right.llen;
	return ans;
}
int n,m,h;int size[maxn];
struct segment_tree
{
	#define ls (p<<1)
	#define rs (p<<1|1)
	node v[maxn<<2];int addv[maxn<<2];
	inline void pushup(int p){v[p]=Merge(v[ls],v[rs]);}
	inline void setadd(int p,int add){v[p].setadd(add);addv[p]+=add;}
	inline void pushdown(int p)
	{
		if(addv[p]) setadd(ls,addv[p]),setadd(rs,addv[p]);
		addv[p]=0;
	}
	inline int query(){return v[1].cnt;}
	inline void add(int p,int l,int r,int ql,int qr,int Add)
	{
		if(ql<=l&&qr>=r) return (void)setadd(p,Add);
		pushdown(p);int mid=(l+r)>>1;
		if(ql<=mid) add(ls,l,mid,ql,qr,Add);
		if(qr>mid) add(rs,mid+1,r,ql,qr,Add);
		pushup(p);
	}
	inline void build(int p,int l,int r)
	{
		if(l==r) return (void)(v[p]=make_node(h,size[l]));
		int mid=(l+r)>>1;
		build(ls,l,mid);
		build(rs,mid+1,r);
		pushup(p);
	}
}seg;
int l[maxn],r[maxn],v[maxn];
int tmp[maxn],tot;
int t[maxn],Ttot;
int ql[maxn],qr[maxn],N;
inline int getid(int x)
{
	int l=1,r=N;
	while(l<=r)
	{
		int mid=(l+r)>>1;
		if(ql[mid]<=x&&qr[mid]>=x) return mid;
		if(qr[mid]<x) l=mid+1;else r=mid-1;
	}
	assert(0);return 0;
}
inline void init()
{
	scanf("%d%d%d",&n,&m,&h);
	for(int i=1;i<=m;i++) scanf("%d%d%d",l+i,r+i,v+i),tmp[++tot]=l[i],tmp[++tot]=r[i];
	sort(tmp+1,tmp+tot+1);
	for(int i=1;i<=tot;i++) if(i==1||tmp[i]!=tmp[i-1]) t[++Ttot]=tmp[i];
	for(int i=1;i<=Ttot;i++)
	{
		if(i>1&&t[i-1]+1<t[i]) ql[++N]=t[i-1]+1,qr[N]=t[i]-1;
		ql[++N]=t[i];qr[N]=t[i];
	}
	for(int i=1;i<=N;i++) size[i]=qr[i]-ql[i]+1;
}
int main()
{
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	init();
	seg.build(1,1,n);
	for(int i=1;i<=m;i++)
	{
		seg.add(1,1,n,getid(l[i]),getid(r[i]),v[i]);
		printf("%d\n",seg.query());
	}
}
