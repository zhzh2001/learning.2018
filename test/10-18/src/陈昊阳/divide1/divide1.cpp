#include<cstdio>
#include<cstring>
#include<string>
#include<cmath>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#include<set>
#define rep(x,a,b) for (int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (int x=hd[a];~x;x=nx[x])
#define LL long long
#define inf 0x3f3f3f3f
#define sqr(x) ((x)*(x))
#define N 100010
using namespace std;
int To[N<<1],Head[N<<1],Next[N<<1],fa[N],n,cnt,x,c[10],ys[N],tot,size[N],d[N];
inline void add(int u,int v){
	To[++cnt]=v;
	Next[cnt]=Head[u];
	Head[u]=cnt;
}
inline void init(int I){
	if(I==1){
		rep(i,1,n-1){
			scanf("%d",&x);
			add(i+1,x);
			add(x,i+1);
			fa[i+1]=x;
			d[x]++;
			d[i+1]++;
		}
	}
	else{
		memset(To,0,sizeof(To));
		memset(Head,0,sizeof(Head));
		memset(Next,0,sizeof(Next));
		memset(d,0,sizeof(d));
		rep(i,1,n-1) fa[i+1]=((fa[i+1]+c[I])%i+i)%i+1;
		rep(i,1,n-1){
			add(i+1,fa[i]);
			add(fa[i],i+1);
			d[i+1]++;
			d[fa[i]]++;
		}
	}
}
inline bool Judge(int u,int X){
	if (size[u]==X) {
//		size[fa[u]]-=x;
		int x=u;
		size[x]=0;
		while (x!=1) size[fa[u]]==X,x=fa[u];
		size[x]-=X;
		return 1;
	}
	for (int i=Head[u];i;i=Next[i]){
		int v=To[i];
		if (v==fa[u]) continue;
		bool g=Judge(v,X);
		if (!g) return 0;
		if (size[u]==X){
		int x=u;
		size[x]=0;
		while (x!=1) size[fa[u]]==X,x=fa[u];
		size[x]-=X;
		return 1;
		}
	}
	if (size[u]>X) return 0;
	return 1;
 }
inline void dfs(int u){
		size[u]=1;
	for (int i=Head[u];i;i=Next[i]){
		int v=To[i];
		if (v==fa[u]) continue;
		 dfs(v);
		size[u]+=size[v];
//		printf("%d ",size[u]);
	}
}
int main(){
	scanf("%d",&n);
	rep(i,2,n-1) if (n%i==0) ys[++tot]=i;
	rep(i,1,9) scanf("%d",&c[i]);
	rep(I,1,10){
		init(I);
//		rep(i,1,n)printf("%d ",fa[i]);
		printf("Case #%d:\n",I);
		printf("1\n");
		dfs(1);
//		rep(i,1,n) printf("%d ",size[i]);
		rep(i,1,tot){
			if (Judge(1,ys[i])) printf("%d\n",ys[i]);
	//		printf("%d ",i);
		}
		printf("%d\n",n);
	}
	return 0;
}

