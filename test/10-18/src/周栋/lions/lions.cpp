#include <iostream>
#include <algorithm>
#include <cstdio>
#include <set>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10;
ll n,now,ans,pre[N];
struct node{
	ll x,id;
	node(){}
	node(ll X,ll I):x(X),id(I){}
	bool operator <(const node&res)const{
		return x==res.x?id<res.id:x<res.x;
	}
}a[N];
bool yyh(node a,node b){return a.id<b.id;}
set<node>S;
int main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	read(n);
	for (ll i=1;i<=n;++i) read(a[i].x),a[i].id=i,S.insert(a[i]);
	sort(a+1,a+1+n);
	ans=n;
	do{
		if (S.size()==0) break;
		node l=*S.begin();
		S.erase(S.begin());
		if (S.size()==0) break;
		node r=*(--S.end());
		S.erase(--S.end());
		if (pre[l.id]) ans=min(ans,pre[l.id]);
		r.x-=l.x;
		pre[r.id]=now++;
		S.insert(r);
	}while(1);
	if (!((n-ans)&1)) ++ans;
	wr(ans),puts("");
	sort(a+1,a+1+ans,yyh);
	for (ll i=1;i<=ans;++i,pc(' ')) wr(a[i].id);
	return 0;
}
