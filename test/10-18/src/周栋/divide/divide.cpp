#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=12e5+10;
ll c[15],fa[N],n,tot,color[N],bel[N];
inline bool pd(ll x){
	memset(color,0,sizeof color);
	memset(bel,0,sizeof bel);
	for (ll i=1;i<=n/x;++i){
		for (ll j=n;j;--j)
			if (!bel[fa[j]]) bel[fa[j]]=bel[j],++color[i];
			else if (color[i]<x&&bel[fa[j]]!=i) return 0;
	}
	return 1;
}
int main(){
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	read(n);
	for (ll i=1;i<=9;++i) read(c[i]);
	for (ll i=2;i<=n;++i) read(fa[i]);
	for (ll i=1;i<=10;++i){
		printf("Case #%d:\n",++tot);
		for (ll j=1;j<=n;++j) if (n%j==0&&pd(j)) printf("%d\n",j);
		if (i<10) for (ll j=2;j<=n;++j) fa[j]=((fa[j]+c[i])%(j-1)+j-1)%(j-1)+1;
	}
	return 0;
}
