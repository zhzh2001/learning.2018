#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=3e5+10,INF=0x3f3f3f3f;
ll n,m,H,tot,s[N];
struct node{ll l,r,v;}a[N];
struct tree{ll num,L,R,Lx,Rx,Ll,Rl,lazy;}T[N<<2];
#define ls T[id<<1]
#define rs T[id<<1|1]
inline void up(ll id){
	T[id].L=ls.L;
	T[id].R=rs.R;
	T[id].Ll=ls.Ll,T[id].Rl=rs.Rl;
	if (ls.Lx==INF){
		if (ls.R!=rs.L) T[id].Lx=rs.L;
		else T[id].Lx=rs.Lx,T[id].Ll+=rs.Ll;
	}else T[id].Lx=ls.Lx;
	if (rs.Rx==INF){
		if (rs.L!=ls.R) T[id].Rx=ls.R;
		else T[id].Rx=ls.Rx,T[id].Rl+=ls.Rl;
	}else T[id].Rx=rs.Rx;
	T[id].num=ls.num+rs.num;
	if (ls.Rx<ls.R&&ls.R>rs.L) T[id].num+=ls.Rl;
	if (rs.Lx<rs.L&&rs.L>ls.R) T[id].num+=rs.Ll;
	if (ls.R==rs.L&&ls.Rx<ls.R&&rs.Lx<rs.L) T[id].num+=ls.Rl+rs.Ll;
}
inline void push(ll id){
	if (T[id].lazy){
		ll x=T[id].lazy;
		ls.L+=x,ls.R+=x,ls.lazy+=x;
		rs.L+=x,rs.R+=x,rs.lazy+=x;
		if (ls.Lx!=INF) ls.Lx+=x;
		if (ls.Rx!=INF) ls.Rx+=x;
		if (rs.Lx!=INF) rs.Lx+=x;
		if (rs.Rx!=INF) rs.Rx+=x;
		T[id].lazy=0;
	}
}
inline void build(ll id,ll l,ll r){
	if (l==r){
		T[id].num=0,T[id].L=T[id].R=H;
		T[id].Lx=T[id].Rx=INF;
		T[id].Ll=T[id].Rl=s[l+1]-s[l];
		return;
	}
	ll mid=(l+r)>>1;
	build(id<<1,l,mid);
	build(id<<1|1,mid+1,r);
	up(id);
}
inline void change(ll id,ll l,ll r,ll L,ll R,ll x){
	if (l==L&&r==R){
		T[id].lazy+=x;
		T[id].L+=x;
		T[id].R+=x;
		if (T[id].Lx!=INF) T[id].Lx+=x;
		if (T[id].Rx!=INF) T[id].Rx+=x;
		return;
	}push(id);
	ll mid=(L+R)>>1;
	if (r<=mid) change(id<<1,l,r,L,mid,x);
	else if (l>mid) change(id<<1|1,l,r,mid+1,R,x);
	else change(id<<1,l,mid,L,mid,x),change(id<<1|1,mid+1,r,mid+1,R,x);
	up(id);
}
int main(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	read(n),read(m),read(H);
	for (ll i=1;i<=m;++i){
		read(a[i].l),read(a[i].r),read(a[i].v);
		s[++tot]=a[i].l,s[++tot]=a[i].r;
		if (a[i].l>1) s[++tot]=a[i].l-1;
		if (a[i].r>1) s[++tot]=a[i].r-1;
		if (a[i].l<n) s[++tot]=a[i].l+1;
		if (a[i].r<n) s[++tot]=a[i].r+1;
	}
	sort(s+1,s+1+tot);
	tot=unique(s+1,s+1+tot)-s-1;
	s[tot+1]=n+1;
	build(1,1,tot);
	for (ll i=1,l,r;i<=m;++i){
		l=lower_bound(s+1,s+1+tot,a[i].l)-s;
		r=lower_bound(s+1,s+1+tot,a[i].r)-s;
		change(1,l,r,1,tot,a[i].v);
		wr(T[1].num),puts("");
	}
	return 0;
}
