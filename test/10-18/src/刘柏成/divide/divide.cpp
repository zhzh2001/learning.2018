#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=11000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
void write(int x){
	if (x>=10)
		write(x/10);
	putchar('0'+x%10);
}
void writeln(int x,char c='\n'){
	if (x<0){
		putchar('-');
		x=-x;
	}
	write(x);
	putchar(c);
}
const int maxn=1200005;
int c[11],n,fa[maxn],sz[maxn],cnt[maxn];
void work(int x){
	for(int i=2;i<=n;i++){
		fa[i]=(fa[i]+x)%(i-1);
		if (fa[i]<0)
			fa[i]+=i;
		else fa[i]++;
	}
	memset(sz,0,(n+1)*4);
	memset(cnt,0,(n+1)*4);
	for(int i=n;i;i--){
		sz[i]++;
		sz[fa[i]]+=sz[i];
	}
	for(int i=1;i<=n;i++)
		cnt[sz[i]]++;
	for(int i=1;i<=n;i++){
		if (n%i==0){
			int sum=0;
			for(int j=i;j<=n;j+=i)
				sum+=cnt[j];
			if (sum>=n/i)
				writeln(i);
		}
	}
}
int main(){
	init();
	n=readint();
	c[0]=-1;
	for(int i=1;i<10;i++)
		c[i]=readint();
	for(int i=2;i<=n;i++)
		fa[i]=readint();
	for(int i=0;i<10;i++){
		printf("Case #%d:\n",i+1);
		work(c[i]);
	}
	// fprintf(stderr,"%.5f\n",1.0*clock()/CLOCKS_PER_SEC);
}