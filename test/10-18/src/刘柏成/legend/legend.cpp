#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=3000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
void write(int x){
	if (x>=10)
		write(x/10);
	putchar('0'+x%10);
}
void writeln(int x,char c='\n'){
	if (x<0){
		putchar('-');
		x=-x;
	}
	write(x);
	putchar(c);
}
struct segtree{
	struct node{
		int ls,rs,pre,suf,sum,val; //pre after <0 suf before >0
	}t[4000003];
	int n,cur;
	void pu(int o,int l,int r){
		int mid=(l+r)/2;
		node &ls=t[t[o].ls],&rs=t[t[o].rs];
		if (!ls.val && !rs.val){
			t[o].val=0;
			return;
		}
		t[o].val=1;
		if (!ls.val){
			t[o].pre=rs.pre+mid-l+1;
			t[o].suf=rs.suf;
			t[o].sum=rs.sum;
			return;
		}
		if (!rs.val){
			t[o].pre=ls.pre;
			t[o].suf=ls.suf+r-mid;
			t[o].sum=ls.sum;
			return;
		}
		t[o].pre=ls.pre;
		t[o].suf=rs.suf;
		t[o].sum=ls.sum+rs.sum;
		if (ls.suf>=0 && rs.pre>=0)
			t[o].sum+=ls.suf+rs.pre+1;
	}
	int rt;
	int p,v;
	void update(int &o,int l,int r){
		if (!o)
			o=++cur;
		if (l==r){
			t[o].val+=v;
			t[o].pre=t[o].suf=-INF;
			if (t[o].val<0)
				t[o].pre=0;
			else t[o].suf=0;
			return;
		}
		int mid=(l+r)/2;
		if (p<=mid)
			update(t[o].ls,l,mid);
		else update(t[o].rs,mid+1,r);
		pu(o,l,r);
	}
	void update(int p,int v){
		this->p=p;
		this->v=v;
		update(rt,1,n);
	}
}t;
int main(){
	init();
	int n=readint(),q=readint();
	readint();
	t.n=n-1;
	while(q--){
		int l=readint(),r=readint(),v=readint();
		if (l>1)
			t.update(l-1,v);
		if (r<n)
			t.update(r,-v);
		if (!t.t[1].val)
			puts("0");
		else writeln(t.t[1].sum);
	}
	// fprintf(stderr,"%.5f\n",1.0*clock()/CLOCKS_PER_SEC);
}