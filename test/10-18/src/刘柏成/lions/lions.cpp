#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
void write(int x){
	if (x>=10)
		write(x/10);
	putchar('0'+x%10);
}
void writeln(int x,char c='\n'){
	if (x<0){
		putchar('-');
		x=-x;
	}
	write(x);
	putchar(c);
}
typedef pair<int,int> pii;
const int maxn=100003;
set<pii> s;
int a[maxn],b[maxn]; //a[i] eat b[i]
bool eaten[maxn];
int main(){
	init();
	int n=readint();
	for(int i=1;i<=n;i++)
		s.insert(make_pair(readint(),i));
	for(int i=1;i<n;i++){
		pii x=*s.begin();
		set<pii>::iterator it=s.end();
		it--;
		pii y=*it;
		a[i]=y.second;
		b[i]=x.second;
		s.erase(s.begin());
		s.erase(it);
		y.first-=x.first;
		s.insert(y);
	}
	int lst=n;
	for(int i=n-1;i>=1;i--){
		if (eaten[a[i]]){
			for(int j=i+1;j<lst;j++)
				eaten[b[j]]=false;
			lst=i;
		}else eaten[b[i]]=true;
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		if (eaten[i])
			ans++;
	writeln(ans);
	for(int i=1;i<=n;i++)
		if (eaten[i])
			writeln(i,' ');
}