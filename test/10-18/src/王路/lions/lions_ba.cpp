#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <set>
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 1e5 + 5;
int n, c[kMaxN], ans[kMaxN];
set<pii> s;
int main() {
  freopen("lions.in", "r", stdin);
  freopen("lions.out", "w", stdout);
  Read(n);
  for (int i = 1; i <= n; ++i) {
    Read(c[i]);
    s.insert(make_pair(c[i], i));
  }
  while (true) {
    pii t = *s.rbegin(), b = *s.begin();
    s.erase(b);
    s.erase(t);
    int c = t.first - b.first;
    // 吃了之后成为最小值且下一个最大值能吃它才退出
    if (c <= s.begin()->first && (s.size() < 2 || s.rbegin()->first - c >= s.begin()->first))
      break;
    s.insert(make_pair(t.first - b.first, t.second));
    ans[++ans[0]] = b.second;
  }
  sort(ans + 1, ans + ans[0] + 1);
  printf("%d\n", ans[0]);
  for (int i = 1; i <= ans[0]; ++i)
    printf("%d%c", ans[i], " \n"[i == ans[0]]);
  return 0;
}
