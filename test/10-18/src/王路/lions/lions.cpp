#pragma GCC optimize 2
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <set>
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 1e5 + 5;
int n, c[kMaxN], ans[kMaxN];
set<pii> s;
pii smx[kMaxN], smn[kMaxN];
int ins[kMaxN], mndep = INT_MAX, cho[kMaxN], ppa[kMaxN], pcnt;
int Work(int d) {
  if (s.size() == 1) {
    cho[d] = s.begin()->second;
    return -1;
  }
  smx[d] = *s.rbegin(), smn[d] = *s.begin();
  ins[smx[d].second] = d;
  cho[d] = smn[d].second;
  s.erase(smx[d]);
  s.erase(smn[d]);
  s.insert(make_pair(smx[d].first - smn[d].first, smx[d].second));
  int last_mxdead = Work(d + 1);
  s.erase(make_pair(smx[d].first - smn[d].first, smx[d].second));
  s.insert(smx[d]);
  s.insert(smn[d]);
  ins[smx[d].second] = 0;
  if (smx[d].second == last_mxdead) {
    mndep = d;
    return -1;
  }
  if (ins[smn[d].second]) {
    return smn[d].second;
  }
  if (d < mndep)
    ans[++ans[0]] = smn[d].second;
  return last_mxdead;
}
int main() {
  freopen("lions.in", "r", stdin);
  freopen("lions.out", "w", stdout);
  Read(n);
  for (int i = 1; i <= n; ++i) {
    Read(c[i]);
    s.insert(make_pair(c[i], i));
  }
  Work(1);
  // fprintf(stderr, "%d\n", mndep);
  printf("%d\n", ans[0]);
  sort(ans + 1, ans + ans[0] + 1);
  for (int i = 1; i <= ans[0]; ++i)
    printf("%d%c", ans[i], " \n"[i == ans[0]]);
  return 0;
}