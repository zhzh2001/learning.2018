#pragma GCC optimize 2
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <map>
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
int n, q, h;
namespace SubTask1 {
const int kMaxN = 1005, kMaxQ = 1005;
int a[kMaxN];
int Solve() {
  for (int i = 1; i <= n; ++i)
    a[i] = h;
  for (int i = 1, l, r, v; i <= q; ++i) {
    Read(l), Read(r), Read(v);
    for (int j = l; j <= r; ++j)
      a[j] += v;
    int ans = 0, last = -1;
    for (int i = 2; i <= n; ++i) {
      if (a[i - 1] < a[i]) {
        last = 1;
      } else if (a[i] == a[i - 1]) {
        ++last;
      } else if (a[i - 1] > a[i]) {
        ans = max(ans, last);
        last = 0;
      }
    }
    printf("%d\n", ans);
  }
  return 0;
}
} // namespace SubTask1
namespace SubTask2 {
const int kMaxN = 1e9, kMaxQ = 50005;
struct Atom {
  int l, r, val;
} qry[kMaxQ];
int p[kMaxQ << 1];
map<int, int> pos;
int Solve() {
  for (int i = 1; i <= q; ++i) {
    Read(qry[i].l), Read(qry[i].r), Read(qry[i].val);
    p[++p[0]] = qry[i].l, p[++p[0]] = qry[i].r;
  }
  sort(p + 1, p + p[0] + 1);
  p[0] = unique(p + 1, p + p[0] + 1) - p - 1;
  for (int i = 1; i <= p[0]; ++i) {
    pos[p[i]] = i;
  }
  
  return 0;
}
} // namespace SubTask2
int main() {
  freopen("legend.in", "r", stdin);
  freopen("legend.out", "w", stdout);
  Read(n), Read(q), Read(h);
  if (n <= 1000)
    return SubTask1::Solve();
  return SubTask2::Solve();
}