#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 1e9, kMaxQ = 50005;
int n, q, h;
struct SegTree {
  static const int kNode = 2e6;
  int son[kNode][2], all[kNode], add[kNode], rt, ncnt;
  int rlen[kNode], rval[kNode], llen[kNode], lval[kNode], ans[kNode];
  // all: if val same, all equals that value
  //      otherwise, INT_MAX
  inline SegTree() : rt(1), ncnt(1) {
    fill(all, all + kNode, INT_MAX);
  }
  inline int Alloc() { return ++ncnt; }
  inline void UpdateAdd(int &x, int l, int r, int val) {
    if (!x)
      x = Alloc();
    if (all[x] != INT_MAX) {
      all[x] += val;
    } else {
      add[x] += val;
    }
  }
  inline void UpdateCov(int &x, int l, int r, int val) {
    if (!x)
      x = Alloc();
    all[x] = val;
    lval[x] = rval[x] = val;
    llen[x] = rlen[x] = r - l + 1;
    ans[x] = r - l + 1;
  }
  inline void PushDown(int x, int l, int r, int mid) {
    if (all[x] != INT_MAX) {
      UpdateCov(son[x][0], l, mid, all[x]);
      UpdateCov(son[x][1], mid + 1, r, all[x]);
    }
    if (add[x]) {
      UpdateAdd(son[x][0], l, mid, add[x]);
      UpdateAdd(son[x][1], mid + 1, r, add[x]);
    }
  }
  inline void PushUp(int x, int l, int r, int mid) {
    ans[x] = max(ans[son[x][0]], ans[son[x][1]]);
    if (all[son[x][0]] == all[son[x][1]])
      all[x] = all[son[x][0]];
    rlen[x] = all[son[x][1]] != INT_MAX ? rlen[son[x][1]] + (all[son[x][1]] == rval[son[x][0]]) * rlen[son[x][0]] : rlen[son[x][1]];
    rval[x] = rval[son[x][1]];
    llen[x] = all[son[x][0]] != INT_MAX ? llen[son[x][0]] + (lval[son[x][1]] == all[son[x][0]]) * llen[son[x][1]] : llen[son[x][0]];
    lval[x] = lval[son[x][0]];
  }
  void IAdd(int &x, int l, int r, int ql, int qr, int val) {
    if (!x) x = Alloc();
    if (ql <= l && r <= qr) {
      UpdateAdd(x, l, r, add[x]);
      return;
    }
    int mid = (l + r) >> 1;
    PushDown(x, l, r, mid);
    if (qr <= mid) {
      IAdd(son[x][0], l, mid, ql, qr, val);
    } else if (ql > mid) {
      IAdd(son[x][1], mid + 1, r, ql, qr, val);
    } else {
      IAdd(son[x][0], l, mid, ql, qr, val);
      IAdd(son[x][1], mid + 1, r, ql, qr, val);
    }
    PushUp(x, l, r, mid);
  }
} seg;
int main() {
  freopen("legend.in", "r", stdin);
  freopen("legend.out", "w", stdout);
  Read(n), Read(q), Read(h);
  seg.all[seg.rt] = h;
  for (int i = 1, l, r, v; i <= q; ++i) {
    Read(l), Read(r), Read(v);
    seg.IAdd(seg.rt, 1, n, l, r, v);
    printf("%d\n", seg.ans[seg.rt]);
  }
  return 0;
}