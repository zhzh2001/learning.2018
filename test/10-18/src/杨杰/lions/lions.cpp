#include <cstdio>
#include <queue>
#include <algorithm>
using namespace std;
const int N=1e5+7;
int n,ans;
int a[N];
#define pii pair<int,int>
#define mk make_pair
#define fi first
#define se second
priority_queue<pii> q1;
priority_queue<pii,std::vector<pii>,greater<pii> >q2;
int main() {
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) {
		scanf("%d",&a[i]);
		q1.push(mk(a[i],i));q2.push(mk(a[i],i));
	}
	while (1) {
		while (q1.top().fi!=a[q1.top().se]) q1.pop();
		while (q2.top().fi!=a[q2.top().se]) q2.pop();
		pii n1=q1.top(),n2=q2.top();q1.pop();q2.pop();
		if (n1.se==n2.se) break;
		while (q2.top().fi!=a[q2.top().se]) q2.pop();
		if (n1.se!=q2.top().se) {
			if (n1.fi-n2.fi<q2.top().fi || n1.fi-n2.fi==q2.top().fi && n1.se<q2.top().se) {
				while (q1.top().fi!=a[q1.top().se]) q1.pop();
				pii n11=q1.top();
				if (n11.se!=q2.top().se) {
					if (n11.fi-n1.fi+n2.fi>q2.top().fi) break;
					else if (n11.fi-n1.fi+n2.fi==q2.top().fi && n11.se>=q2.top().se) break;
					a[n2.se]=-1;
					break;
				}
			}
		}
		a[n1.se]-=a[n2.se];a[n2.se]=-1;
		q1.push(mk(a[n1.se],n1.se));q2.push((mk(a[n1.se],n1.se)));
	}
	for (int i=1;i<=n;i++) if (a[i]==-1) ans++;
	printf("%d\n",ans);
	for (int i=1;i<=n;i++) if (a[i]==-1) printf("%d ",i);
}
