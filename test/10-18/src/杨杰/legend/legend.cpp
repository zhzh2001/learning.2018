#include <cstdio>
#include <algorithm>
using namespace std;
const int N=2e5+7,inf=1e9;
struct node {int l,r,v;}q[N];
struct tree {
	int s,l,r,lh,rh,ln,rn,lc,rc,ok,add;//s答案,lh左边高度,ln左边长度,lc左边合法,rh右边高度，rn右边长度,rc右边合法,ok是否相同,add
	#define ls (ts<<1)
	#define rs (ts<<1|1)
	#define l(ts) (tr[ts].l)
	#define r(ts) (tr[ts].r)
	#define s(ts) (tr[ts].s)
	#define lh(ts) (tr[ts].lh)
	#define rh(ts) (tr[ts].rh)
	#define lc(ts) (tr[ts].lc)
	#define rc(ts) (tr[ts].rc)
	#define ln(ts) (tr[ts].ln)
	#define rn(ts) (tr[ts].rn)
	#define ok(ts) (tr[ts].ok)
	#define add(ts) (tr[ts].add)
}tr[N<<2];
int n,Q,H,cnt;
int b[N];
void pushup(int ts) {
	s(ts)=s(ls)+s(rs);
//	if (rh(ls)==lh(rs) && rc(ls) && lc(rs)) s(ts)+=rn(ls)+ln(rs)+b[l(rs)]-b[r(ls)]-1;
//	else if (rh(ls)>lh(rs) && rc(ls)) s(ts)+=rn(ls)+b[l(rs)]-b[r(ls)]-1;
//	else if (rh(ls)<lh(rs) && lc(rs)) s(ts)+=ln(rs)+b[l(rs)]-b[r(ls)]-1;
	if (rh(ls)==lh(rs) && rc(ls) && lc(rs)) s(ts)+=rn(ls)+ln(rs);
	else if (rh(ls)>lh(rs) && rc(ls)) s(ts)+=rn(ls);
	else if (rh(ls)<lh(rs) && lc(rs)) s(ts)+=ln(rs);
	lh(ts)=lh(ls);rh(ts)=rh(rs);
	lc(ts)=lc(ls);rc(ts)=rc(rs);
	if (ok(ls)) {if (rh(ls)>lh(rs)) lc(ts)=1;}
	if (ok(rs)) {if (rh(ls)<lh(rs)) rc(ts)=1;}
	if (ok(ls) && ok(rs) && rh(ls)==lh(rs)) ok(ts)=1;
	else ok(ts)=0;
	ln(ts)=ln(ls);rn(ts)=rn(rs);
//	if (ok(ls) && lh(rs)==rh(ls)) ln(ts)+=ln(rs)+b[l(rs)]-b[r(ls)]-1,lc(ts)=lc(rs);
//	if (ok(rs) && lh(rs)==rh(ls)) rn(ts)+=rn(ls)+b[l(rs)]-b[r(ls)]-1,rc(ts)=rc(ls);
	if (ok(ls) && lh(rs)==rh(ls)) ln(ts)+=ln(rs),lc(ts)=lc(rs);
	if (ok(rs) && lh(rs)==rh(ls)) rn(ts)+=rn(ls),rc(ts)=rc(ls);
}
void pushdown(int ts) {
	if (add(ts)==0) return;
	int v=add(ts);add(ts)=0;
	lh(ls)+=v;rh(ls)+=v;lh(rs)+=v;rh(rs)+=v;
	add(ls)+=v;add(rs)+=v;
}
void build(int ts,int l,int r) {
	l(ts)=l;r(ts)=r;
	if (l==r) {
		lh(ts)=rh(ts)=H;
		ln(ts)=rn(ts)=1;
		ok(ts)=1;
		return;
	}
	int mid=(l+r)>>1;
	build(ls,l,mid);build(rs,mid+1,r);
	pushup(ts);
}
void update(int ts,int L,int R,int v) {
	if (l(ts)==L && r(ts)==R) {
		lh(ts)+=v;rh(ts)+=v;
		add(ts)+=v;
		return;
	}
	int mid=(l(ts)+r(ts))>>1;
	pushdown(ts);
	if (R<=mid) update(ls,L,R,v);
	else if (L>mid) update(rs,L,R,v);
	else update(ls,L,mid,v),update(rs,mid+1,R,v);
	pushup(ts);
}
int main() {
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	scanf("%d%d%d",&n,&Q,&H);
	for (int i=1;i<=Q;i++) {
		scanf("%d%d%d",&q[i].l,&q[i].r,&q[i].v);
//		b[++cnt]=q[i].l;b[++cnt]=q[i].r;
//		b[++cnt]=q[i].l-1;b[++cnt]=q[i].r+1;
	}
//	sort(b+1,b+cnt+1);
//	cnt=unique(b+1,b+cnt+1)-b-1;
//	for (int i=1;i<cnt;i++) b[i]=b[i+1];
	build(1,1,n);
	for (int i=1;i<=Q;i++) {
//		int l=lower_bound(b+1,b+cnt+1,q[i].l)-b;
//		int r=lower_bound(b+1,b+cnt+1,q[i].r)-b;
		update(1,q[i].l,q[i].r,q[i].v);
//		update(1,l,r,q[i].v);
		printf("%d\n",tr[1].s);
	}
}
