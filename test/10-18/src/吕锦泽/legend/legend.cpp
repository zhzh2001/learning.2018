#include<set>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
#define ll long long
#define inf (1ll<<60)
#define N 100005
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,q,H,ans,val;
struct data{ ll x,pos; };
set<data> S;
bool operator <(data x,data y) { return x.pos<y.pos; }
ll judge(data x,data y){
	if (x.x>0&&y.x<0) return y.pos-x.pos;
	return 0;
}
void add(ll p){
	if (p==1||!val||p>n) return;
	data now=*lower_bound(S.begin(),S.end(),(data){0,p});
	if (now.pos==p){
		data left=*--lower_bound(S.begin(),S.end(),(data){0,p});
		data right=*++lower_bound(S.begin(),S.end(),(data){0,p});
		if (now.x+val==0){
			ans-=judge(left,now);
			ans-=judge(now,right);
			ans+=judge(left,right);
			S.erase(now);
		}else{
			ans-=judge(left,now);
			ans-=judge(now,right);
			S.erase(now);
			now.x+=val;
			S.insert(now);
			ans+=judge(left,now);
			ans+=judge(now,right);
		}
	}else{
		data right=now;
		data left=*--lower_bound(S.begin(),S.end(),(data){0,p});
		data tmp;
		tmp.x=val; tmp.pos=p;
		ans-=judge(left,right);
		ans+=judge(left,tmp);
		ans+=judge(tmp,right);
		S.insert(tmp);
	}
}
int main(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	n=read(); q=read(); H=read();
	S.insert((data){-inf,1});
	S.insert((data){inf,n+1});
	while (q--){
		ll l=read(),r=read(); val=read();
		add(l);
		val=-val;
		add(r+1);
		printf("%lld\n",ans);
	}
}
