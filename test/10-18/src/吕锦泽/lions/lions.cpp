#include<set>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
#define ll int
#define inf (1<<25)
#define N 100005
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,A[N],B[N],C[N],ans=inf; 
struct data{ ll x,y; };
bool operator <(data x,data  y){
	if (x.x==y.x) return x.y>y.y;
	else return x.x>y.x;
}
set<data> S;
int main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	rep(i,1,n){
    	A[i]=read();
    	S.insert((data){A[i], i});
	}
	rep(i,1,n-1){
    	data a=*S.begin(),b=*--S.end();
    	S.erase(a); S.erase(b);
    	C[a.y]=i; data c=(data){a.x-b.x,a.y};
    	B[++B[0]]=(b.y); S.insert(c);
  	}
  	per(i,n,1) if (C[B[i]]&&ans>C[B[i]]) ans=C[B[i]],i=ans;
  	sort(B+1,B+ans); printf("%d\n",ans-1);
  	rep(i,1,ans-1) printf("%d ",B[i]);
}
