#include<bits/stdc++.h>
#define For(i,x,y) for (int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (int i=(x);i>=(y);i--)
#define cross(i,k) for (int i=first[k];i;i=last[i])
#define x first
#define y second
#define mp make_pair
using namespace std;
typedef long long ll;
typedef pair<int,int> pa;
inline int read(){
	int x=0;int ch=getchar(),f=1;
	while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
	if (ch=='-'){f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
const int N = 1e5+10;
set<pa>s;
int n,k,Ans,ans[N],eat[N],beat[N],a[N],vis[N];
int main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	For(i,1,n) a[i]=read(),s.insert(mp(a[i],i));
	For(k,1,n-1){
		pa Min=*(s.begin()),Max=*(--s.end());
		s.erase(Max),s.erase(Min);
		eat[Max.y]=k,beat[k]=ans[k]=Min.y;
		s.insert(mp(Max.x-Min.x,Max.y));
	}
	Ans=n-1;
	for (int j=n-1;j;j=min(j-1,Ans))
		if (eat[beat[j]]) Ans=eat[beat[j]]-1;
	printf("%d\n",Ans);
	sort(ans+1,ans+1+Ans);
	For(i,1,Ans) printf("%d ",ans[i]);
}
