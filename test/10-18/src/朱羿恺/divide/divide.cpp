#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
#define it set<int>::iterator
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1200010;
int n,c[10],fa[N],cnt,a[N];
int tot,first[N],last[N<<1],to[N<<1];
inline void Add(int x,int y){to[++tot]=y,last[tot]=first[x],first[x]=tot;}
int size[N],b[N],sum,x;
inline void dfs(int u){
	size[u]=1;
	cross(i,u) dfs(to[i]),size[u]+=size[to[i]];
}
/*inline void solve(int k){
	dfs(1);
	printf("Case #%d:\n",k);
	For(i,1,n)
		if (n%i==0){
			int sum=0;
			For(j,1,n) if (size[j]%i==0) sum++;
			if (sum==n/i) printf("%d\n",i);
		}
}*/
inline void solve(int k){
	dfs(1);
	//For(i,1,n) printf("%d ",size[i]);puts("");
	printf("Case #%d:\n1\n",k);
	For(i,1,n) b[i]=0;
	For(i,1,n) b[size[i]]++;
	For(k,1,cnt){
		int i=a[k];
		sum=0,x=n/i;
		For(j,1,x){
			sum+=b[j*i];
			if (sum==x) break;
		}
		if (sum==x) printf("%d\n",i);
	}
	printf("%d\n",n);
}
int main(){
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	n=read();
	For(i,2,n-1) if (n%i==0) a[++cnt]=i; 
	For(i,1,9) c[i]=read();
	For(i,2,n) fa[i]=read(),Add(fa[i],i);
	solve(1);
	For(i,1,9){
		For(j,1,n) first[j]=0;
		tot=0;
		For(j,2,n) fa[j]=((fa[j]+c[i])%(j-1)+j-1)%(j-1)+1,Add(fa[j],j);
		solve(i+1);
	}
}
