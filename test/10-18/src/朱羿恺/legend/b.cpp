#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
#define it set<int>::iterator
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 5e4+10;
int n,h,q;
namespace Subtask1{
	int a[N],l,r,x;
	inline int Query(){
		int ans=0;
		for (int l=1,r=1;l<=n;l=++r){
			for (;a[r]==a[r+1];r++);
			if (a[l]>a[l-1]&&a[r]>a[r+1]) ans+=r-l+1;
		}
		return ans;
	}
	inline void Main(){
		a[0]=a[n+1]=1e9;
		For(i,1,n) a[i]=h;
		while (q--){
			l=read(),r=read(),x=read();
			For(i,l,r) a[i]+=x;
			printf("%d\n",Query());
		}
	}
};
struct SegMent_Tree{
	int L[N<<2],R[N<<2],lv[N<<2],rv[N<<2],lazy[N<<2],sum[N<<2];
	bool fl[N<<2],fr[N<<2];
	inline void push_up(int u,int l,int r,int mid){
		int ls=u<<1,rs=u<<1^1;
		sum[u]=sum[ls]+sum[rs],lv[u]=lv[ls],rv[u]=rv[rs];
		if (fr[ls]&&fl[rs]&&rv[ls]==lv[rs]) sum[u]+=R[ls]+L[rs];
		if (L[ls]==mid-l+1){
			if (rv[ls]==lv[rs]) L[u]=L[ls]+L[rs],fl[u]=fl[rs];
				else L[u]=L[ls],fl[u]=(rv[ls]>lv[rs]);
		} else L[u]=L[ls],fl[u]=fl[ls];
		if (R[rs]==r-mid){
			if (lv[rs]==rv[ls]) R[u]=R[rs]+R[ls],fr[u]=fr[ls];
				else R[u]=R[rs],fr[u]=(lv[rs]>rv[ls]);
		} else R[u]=R[rs],fr[u]=fr[rs];
	}
	inline void Build(int u,int l,int r){
		if (l==r){L[u]=R[u]=1,lv[u]=rv[u]=h;return;}
		int mid=l+r>>1;Build(u<<1,l,mid),Build(u<<1^1,mid+1,r);
		push_up(u,l,r,mid);
	}
	inline void push_down(int u){
		if (!lazy[u]) return;
		int x=lazy[u];lazy[u]=0;
		lazy[u<<1]+=x,lazy[u<<1^1]+=x;
		lv[u<<1]+=x,rv[u<<1]+=x,lv[u<<1^1]+=x,rv[u<<1^1]+=x;
	}
	inline void update(int u,int l,int r,int ql,int qr,int x){
		if (l>=ql&&r<=qr){lazy[u]+=x,lv[u]+=x,rv[u]+=x;return;}
		int mid=l+r>>1;push_down(u);
		if (qr<=mid) update(u<<1,l,mid,ql,qr,x);
		else if (ql>mid) update(u<<1^1,mid+1,r,ql,qr,x);
		else update(u<<1,l,mid,ql,qr,x),update(u<<1^1,mid+1,r,ql,qr,x);
		push_up(u,l,r,mid);
	}
}t;
int l,r,x;
int main(){
	freopen("legend.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read(),q=read(),h=read();
	if (n<=1000) return Subtask1::Main(),0;
	t.Build(1,1,n);
	while (q--){
		l=read(),r=read(),x=read();
		t.update(1,1,n,l,r,x),printf("%d\n",t.sum[1]);
	}
}
