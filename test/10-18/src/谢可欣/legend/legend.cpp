#include <iostream>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
void wr(int x){if(x<0){putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
void wri(int x){wr(x);putchar(' ');}
void wrn(int x){wr(x);putchar('\n');}
int a[100000];
int main()
{
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	int last=0;
	int n=read(),q=read(),h=read();
	for(int i=1;i<=n;i++)a[i]=h;
	while(q--){
		int l=read(),r=read(),w=read();
		if(l==1&&r==n){
			wrn(last);continue;
		}
		for(int i=l;i<=r;i++)a[i]+=w;
//		for(int i=1;i<=n;i++)cout<<a[i]<<" ";puts("");
		int ans=0,sum=0,now=-700000000;
		for(int i=2;i<=n;i++){
//			cout<<now<<" ";
			if(now==a[i])sum++;
			else if(a[i-1]<a[i]){
				sum=1;now=a[i];
			}
			else if(a[i-1]>a[i])ans+=sum,now=-700000000,sum=0;
		}
		wrn(ans);last=ans;
	}
	return 0;
}
