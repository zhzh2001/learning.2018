#include <algorithm>
#include <iostream>
#include <cstdio>
#include <queue>
#define int long long 
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
void wr(int x){if(x<0){putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
void wri(int x){wr(x);putchar(' ');}
void wrn(int x){wr(x);putchar('\n');}
struct big{
	int x,id;
	bool operator < (const big &b)const{
		if(x==b.x)return id<b.id;
		return x<b.x;
	}
};
struct sma{
	int x,id;
	bool operator < (const sma &b)const{
		if(x==b.x)return id>b.id;
		return x>b.x;
	}
};
priority_queue<big>q,t; priority_queue<sma>s;
int top,a[100005],uexi[100005],ans[100005],eat[100005];
signed main()
{
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		q.push((big){a[i],i}); s.push((sma){a[i],i});
	}
	int sum=n;
	while(!q.empty()&&!s.empty()){
//		for(int i=1;i<=n;i++)cout<<eat[i]<<" ";puts("");
	    while(!q.empty()&&(uexi[q.top().id]||a[q.top().id]!=q.top().x))q.pop();
		while(!s.empty()&&(uexi[s.top().id]||a[s.top().id]!=s.top().x))s.pop();
		big u=q.top();q.pop();
		sma k=s.top();s.pop();
//		cout<<u.id<<" "<<k.id<<endl;
        /*t=q;
        while(!t.empty()){
        	cout<<t.top().id<<" "<<t.top().x<<endl;
        	t.pop();
		}*/
	    while(!q.empty()&&(uexi[q.top().id]||a[q.top().id]!=q.top().x))q.pop();
		while(!s.empty()&&(uexi[s.top().id]||a[s.top().id]!=s.top().x))s.pop();
		if(eat[k.id]){
//			cout<<"aaa"<<endl;
			if(u.x-k.x<s.top().x)break;
			else {
				top--;
				break;
			}
		}
		if(sum==3){
			if(u.x-k.x<s.top().x)break;
		}
		eat[u.id]=1;uexi[k.id]=1;ans[++top]=k.id;
		a[u.id]-=k.x; sum--;
		q.push((big){a[u.id],u.id});s.push((sma){a[u.id],u.id});
	}
	wrn(top); sort(ans+1,ans+1+top);
	for(int i=1;i<=top;i++)wri(ans[i]);
	return 0;
}
/*
6 
7 3 5 6 4 1
*/
