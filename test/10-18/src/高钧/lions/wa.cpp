#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=100005,inf=2e9+7;
int n,a[N],b[N],bo1[N],bo2[N],rt,re[N],pp,pre[N],in1[N];
struct segtree{int l,r,mi,ma,miid,maid;inline int mid(){return (l+r)>>1;}}Tree[N<<2];
#define c1 x<<1
#define c2 x<<1|1
inline void Ami(int x,int y){Tree[x].mi=Tree[y].mi;Tree[x].miid=Tree[y].miid;}
inline void Ama(int x,int y){Tree[x].ma=Tree[y].ma;Tree[x].maid=Tree[y].maid;}
inline void Up(int x){if(Tree[c1].mi!=Tree[c2].mi){if(Tree[c1].mi<Tree[c2].mi)Ami(x,c1); else Ami(x,c2);}else Ami(x,c1);if(Tree[c1].ma!=Tree[c2].ma){if(Tree[c1].ma>Tree[c2].ma)Ama(x,c1); else Ama(x,c2);}else Ama(x,c2);}
inline void build(int l,int r,int x){Tree[x].l=l;Tree[x].r=r;if(l==r){Tree[x].mi=Tree[x].ma=a[l];Tree[x].maid=Tree[x].miid=l;return;}int mid=(l+r)>>1;build(l,mid,c1);build(mid+1,r,c2); Up(x);}
inline void ins(int po,int x,int v){if(Tree[x].l==Tree[x].r){Tree[x].mi=Tree[x].ma=v;return;}int mid=Tree[x].mid();if(po<=mid)ins(po,c1,v);else ins(po,c2,v); Up(x);}
inline void del(int po,int x){if(Tree[x].l==Tree[x].r){Tree[x].mi=inf;Tree[x].ma=-1;return;}int mid=Tree[x].mid();if(po<=mid)del(po,c1);else del(po,c2); Up(x);}
int main()
{
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	int i,p1,p2; scanf("%d",&n); memset(bo1,0,sizeof bo1); memset(bo2,0,sizeof bo2);
	for(i=1;i<=n;i++){scanf("%d",&a[i]);b[i]=a[i];} build(1,n,1);
	for(i=1;i<n;i++)
	{
		p1=Tree[1].miid; p2=Tree[1].maid; bo1[p2]=1; bo2[p1]=1; a[p2]-=a[p1]; del(p1,1); ins(p2,1,a[p2]); re[i]=p1; in1[p2]++;
	}for(i=1;i<=n;i++)if(!bo2[i]){rt=i;break;} memmove(a,b,sizeof b); build(1,n,1);
	for(i=1;i<n;i++)
	{
		p1=Tree[1].miid; p2=Tree[1].maid; if(bo1[p1]){pp=pre[p1];break;} a[p2]-=a[p1]; pre[p2]=i; del(p1,1); ins(p2,1,a[p2]);
	}printf("%d\n",pp); sort(re+1,re+pp+1); for(i=1;i<=pp;i++)printf("%d ",re[i]);
}
