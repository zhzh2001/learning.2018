#include<cstdio>
#include<cstring>
#include<string>
#include<cmath>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#include<set>
#define rep(x,a,b) for (int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (int x=hd[a];~x;x=nx[x])
#define LL long long
#define inf 0x3f3f3f3f
#define sqr(x) ((x)*(x))
#define N 100000+10
using namespace std;
int a[N],b[N],h,j,m,n,x;
LL ans;
set< pair<int,int> > f;
int main()
{
	freopen("lions.in","r",stdin);
    freopen("lions.out","w",stdout); 
    scanf("%d",&n);
    rep(i,1,n)
    {
    	scanf("%d",&x);
    	f.insert(make_pair(x,i));
    }
    rep(i,1,n-1)
    {
    	pair<int,int> u=*(--f.end()),v=*(f.begin());
    	f.erase(u),f.erase(v);
    	a[i]=v.second;
    	b[u.second]=i;
    	f.insert(make_pair(u.first-v.first,u.second));
    }
    int ans=n-1;
    for(int i=n-1;i;i=min(i-1,ans)) 
    if (b[a[i]]) ans=b[a[i]]-1;
    sort(a+1,a+ans+1);
    printf("%d\n",ans);
    rep(i,1,ans) printf("%d ",a[i]);
	return 0;
}

