#include<bits/stdc++.h>
#define LL long long 
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 500010
using namespace std;
struct node
{
	LL min,max;
}Q[N],V[N],num;
LL a[N],b[N],l[N],r[N],pre[N],boo[N],last[N],ans[N];
LL n,m,i,j,k,sum;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
void down_min(LL k)
{
	for(LL i=k<<1;i<=num.min;k=i,i=k<<1)
	{
		if(i<num.min&&(a[Q[i].min]>a[Q[i+1].min]||a[Q[i].min]==a[Q[i+1].min]&&b[Q[i].min]>b[Q[i+1].min])) i++;
		if(a[Q[i].min]<a[Q[k].min]||a[Q[i].min]==a[Q[k].min]&&b[Q[i].min]<b[Q[k].min])
		  swap(Q[i].min,Q[k].min),swap(V[Q[i].min].min,V[Q[k].min].min);
		else return;
	}
}
void down_max(LL k)
{
	for(LL i=k<<1;i<=num.max;k=i,i=k<<1)
	{
		if(i<num.max&&(a[Q[i].max]<a[Q[i+1].max]||a[Q[i].max]==a[Q[i+1].max]&&b[Q[i].max]<b[Q[i+1].max])) i++;
		if(a[Q[i].max]>a[Q[k].max]||a[Q[i].max]==a[Q[k].max]&&b[Q[i].max]>b[Q[k].max])
		  swap(Q[i].max,Q[k].max),swap(V[Q[i].max].max,V[Q[k].max].max);
		else return;
	}
}
void up_min(LL k)
{
	for(LL i=k>>1;i;k=i,i=k>>1)
	  if(a[Q[i].min]>a[Q[k].min]||a[Q[i].min]==a[Q[k].min]&&b[Q[i].min]>b[Q[k].min]) 
	    swap(Q[i].min,Q[k].min),swap(V[Q[i].min].min,V[Q[k].min].min);
	  else return;
}
void up_max(LL k)
{
	for(LL i=k>>1;i;k=i,i=k>>1)
	  if(a[Q[i].max]<a[Q[k].max]||a[Q[i].max]==a[Q[k].max]&&b[Q[i].max]<b[Q[k].max]) 
	    swap(Q[i].max,Q[k].max),swap(V[Q[i].max].max,V[Q[k].max].max);
	  else return;
}
void del_min(LL k)
{
	Q[k].min=Q[num.min--].min;
	V[Q[k].min].min=k;
	down_min(k);
}
void del_max(LL k)
{
	Q[k].max=Q[num.max--].max;
	V[Q[k].max].max=k;
	down_max(k);
}
void ins_min(LL k)
{
	Q[++num.min].min=k;
	V[k].min=num.min;
	up_min(num.min);
}
void ins_max(LL k)
{
	Q[++num.max].max=k;
	V[k].max=num.max;
	up_max(num.max);
}
void check(LL k)
{
	if(boo[k]) return;
	check(pre[l[k]]);
	boo[k]=0-boo[pre[l[k]]];
}
int main()
{
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=read(),b[i]=i;
	rep(i,1,n) 
	  ins_min(i),ins_max(i);
	rep(i,1,n-1)
	{
		l[i]=Q[1].max; r[i]=Q[1].min;
		a[Q[1].max]-=a[Q[1].min];
		del_max(V[Q[1].min].max);
		del_min(1);
		up_min(V[Q[1].max].min);
		down_max(1);
	}
	rep(i,1,n-1) 
	{
		if(last[l[i]]<i) last[l[i]]=i;
		pre[r[i]]=i;
	}
	rep(i,1,n-1)
	{
	  	if(!pre[l[i]]) boo[i]=1;
		if(last[l[i]]>0&&last[l[i]]!=i) boo[i]=1;
	}
	rep(i,1,n-1) 
	  if(!boo[i]) check(i);
	rep(i,1,n-1)
	{
		if(boo[i]==-1) break;
		ans[r[i]]=1;
	}
	rep(i,1,n) sum+=ans[i];
	printf("%lld\n",sum);
	rep(i,1,n)
	  if(ans[i]) printf("%lld ",i);
	return 0;
}
