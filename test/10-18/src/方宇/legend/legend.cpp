#include<cstdio>
#include<cstring>
#include<string>
#include<cmath>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#include<set>
#define rep(x,a,b) for (int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (int x=hd[a];~x;x=nx[x])
#define LL long long
#define inf 0x3f3f3f3f
#define sqr(x) ((x)*(x))
#define N 100010
using namespace std;
int n,q,h,L,R,v,a[N];
int main(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	scanf("%d%d%d",&n,&q,&h);
	rep(i,1,n) a[i]=h;
	a[n+1]=h;
	rep(i,1,q){
		scanf("%d%d%d",&L,&R,&v);
		int ans=0;
		rep(j,L,R) a[j]+=v;
		int l=2,r;
		while (l<=n-1){
			while ((l<=n-1)&&a[l]<=a[l-1])l++;
			r=l;
			while (a[r+1]==a[l]&&(r<=n-1)) r++;
			if (a[r+1]<a[r]) ans+=r-l+1;	
			l=r+1;		
		}
		printf("%d\n",ans);
	}
	return 0; 
} 
