#include <algorithm>
#include <iostream>
#include <cstdio>

#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Lion{
	int num,id,last;
	inline bool operator<(const Lion&x)const{
		if(num!=x.num){
			return num<x.num;
		}
		return id<x.id;
	}
}l,r,a[Max],b[Max],heap1[Max],heap2[Max];

int n,top1,top2,size,id1[Max],id2[Max],ans[Max],last[Max];
bool vis[Max];

inline bool cmp(Lion x,Lion y){
	return y<x;
}

inline void down1(int x){
	int y=x*2;
	if(y>top1)return;
	if(y+1<=top1&&heap1[y]<heap1[y+1])y++;
	while(heap1[x]<heap1[y]){
		swap(id1[heap1[x].id],id1[heap1[y].id]);
		swap(heap1[x],heap1[y]);
		x=y;
		y=x*2;
		if(y>top1)return;
		if(y+1<=top1&&heap1[y]<heap1[y+1])y++;
	}
	return;
}

inline void up2(int x){
	int y=x/2;
	if(!y)return;
	while(heap2[x]<heap2[y]){
		swap(id2[heap2[x].id],id2[heap2[y].id]);
		swap(heap2[x],heap2[y]);
		x=y;
		y=x/2;
		if(!y)return;
	}
	return;
}

inline void down2(int x){
	int y=x*2;
	if(y>top2)return;
	if(y+1<=top2&&heap2[y+1]<heap2[y])y++;
	while(heap2[y]<heap2[x]){
		swap(id2[heap2[x].id],id2[heap2[y].id]);
		swap(heap2[x],heap2[y]);
		x=y;
		y=x*2;
		if(y>top2)return;
		if(y+1<=top2&&heap2[y+1]<heap2[y])y++;
	}
	return;
}

int main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i].num=read();
		a[i].id=i;
	}
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++){
		heap1[i]=a[i];
		id1[a[i].id]=i;
	}
	top1=top2=n;
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++){
		heap2[i]=a[i];
		id2[a[i].id]=i;
	}
	for(int i=1;i<n;i++){
		l=heap1[1];
		r=heap2[1];
//		cout<<l.id<<" "<<l.num<<endl;
//		cout<<r.id<<" "<<r.num<<endl;
		b[i]=r;
		b[i].last=last[r.id];
		ans[++size]=r.id;
		last[l.id]=size;
		vis[r.id]=true;
		l.num-=r.num;
		id1[heap1[top1].id]=id1[r.id];
		heap1[id1[r.id]]=heap1[top1--];
		down1(id1[r.id]);
		id2[heap2[top2].id]=1;
		heap2[1]=heap2[top2--];
		down2(1);
		heap1[1].num=l.num;
		down1(1);
		heap2[id2[l.id]].num=l.num;
		up2(id2[l.id]);
		/*for(int i=1;i<=top1;i++){
			cout<<i<<" "<<heap1[i].id<<" "<<heap1[i].num<<" "<<id1[heap1[i].id]<<endl;
		}
		cout<<endl;
		for(int i=1;i<=top2;i++){
			cout<<i<<" "<<heap2[i].id<<" "<<heap2[i].num<<" "<<id2[heap2[i].id]<<endl;
		}*/
	}
	for(int i=size;i;i--){
//		cout<<size<<" "<<i<<endl;
		while(b[i].last){
//			cout<<i<<" "<<b[i].last<<endl;
			size=b[i].last-1;
			i=b[i].last-1;
			if(!i)break;
		}
		if(!i)break;
	}
	writeln(size);
	sort(ans+1,ans+size+1);
	for(int i=1;i<=size;i++){
		write(ans[i]);putchar(' ');
	}
	return 0;
}
