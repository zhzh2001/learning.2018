#include<set>
#include<cstdio>
#include<algorithm>
using namespace std;
#define X	first
#define Y	second
#define n	100005
#define MK	make_pair
#define PA	pair<int,int>
#define CH	(ch=getchar())
int	N,A[n],B[n];
set<PA>F;
int	read(){
	int x=0,ch;
	for(;CH<'0'||ch>'9';);
	for(;ch>='0'&&ch<='9';CH)
		(x*=10)+=ch-'0';
	return x;
}
int	main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	N=read();
	for(int i=1,x;i<=N;i++){
		x=read();
		F.insert(MK(x,i));
	}
	for(int i=1;i<N;i++){
		PA u=*(--F.end()),v=*(F.begin());
		F.erase(u);	
		F.erase(v);
		A[i]=v.Y;
		B[u.Y]=i;
		F.insert(MK(u.X-v.X,u.Y));
	}
	int Ans=N-1;
	for(int i=N-1;i;i=min(i-1,Ans))
		if(B[A[i]])Ans=B[A[i]]-1;
	sort(A+1,A+Ans+1);
	printf("%d\n",Ans);
	for(int i=1;i<=Ans;i++)
		printf("%d ",A[i]);
}
