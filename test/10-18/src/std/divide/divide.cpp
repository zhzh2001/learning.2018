#include<cstdio>
#include<cstring>
using namespace std;
int n,b[11],c[1200005],q[1200005],f[1200005];
inline int read(){
	char ch=getchar();
	int f=1,x=0;
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+(ch-'0');
		ch=getchar();
	}
	return x*f;
}
inline void write(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){write(a); puts("");}
int L[2005],l;
int main(){
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	n=read();
	for(int i=1;i<=9;i++)
		b[i]=read();
	for(int i=2;i<=n;i++)
		f[i]=read();
	register int cnt,tmp;
	for(int t=1;t<=10;t++){
		printf("Case #%d:\n",t);
		memset(c,0,sizeof(c));
		memset(q,0,sizeof(q));
		for(register int i=n;i>=1;i--){
			q[i]++;
			if(i>1)q[f[i]]+=q[i];
			c[q[i]]++;
		}
		for(register int i=1;i*i<=n;i++)
			if(n%i==0){
				cnt=n/i;
				for(register int j=i;j<=n;j+=i)
					cnt-=c[j];
				if(cnt==0) writeln(i);
				if(i*i!=n){
					tmp = n / i;
   					cnt=i;
					for(register int j=tmp;j<=n;j+=tmp)
						cnt-=c[j];
					if(cnt==0) L[++l] = tmp;
				}
			}
		while(l) writeln(L[l--]);
		register int res = b[t];
		for(register int i=2;i<=n;i++){
		   f[i] = (f[i]+res)%(i-1);
		   if(f[i]<0) f[i]+=i-1; else
		   if(f[i]>=(i-1)) f[i]-=i-1;
		   f[i]++;
		}
	}
}
