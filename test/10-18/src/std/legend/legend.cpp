#include<map>
#include<cstdio>
using namespace std;
#define INF	(1<<30)
#define Map	map<int,int>
#define CH	(ch=getchar())
#define	For(i,a,b)	for(int i=a;i<=b;i++) 
int	N,Q,Ans;
Map	F;
int	read(){
	int x=0,f=0,ch;
	for(;CH<'0'||ch>'9';)
		f=(ch=='-');
	for(;ch>='0'&&ch<='9';CH)
		(x*=10)+=ch-'0';
	return f?-x:x;
}
void Modify(int x,int v){
	if(!v)return;
	int fx=F[x];
	if(fx){
		if(fx>0&&fx+v>0||fx<0&&fx+v<0){
			F[x]+=v;
			return;
		}
		int l=(--F.find(x))->first,r=(++F.find(x))->first,t=fx+v,fl=F[l],fr=F[r];
		if(fx<0&&fl>0&&fr>0)Ans-=x-l;
		if(fx<0&&fl>0&&fr<0)Ans+=r-x;
		if(fx>0&&fl<0&&fr<0)Ans-=r-x;
		if(fx>0&&fl>0&&fr<0)Ans+=x-l;
		F.erase(F.find(x));	Modify(x,t);
	}
	else{
		int l=(--F.find(x))->first,r=(++F.find(x))->first,fl=F[l],fr=F[r];
		F[x]=fx=v;
		if(fx<0&&fl>0&&fr<0)Ans-=r-x;
		if(fx<0&&fl>0&&fr>0)Ans+=x-l;
		if(fx>0&&fl>0&&fr<0)Ans-=x-l;
		if(fx>0&&fl<0&&fr<0)Ans+=r-x;
	}
}
int	main(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	scanf("%d%d%*d",&N,&Q);
	F.clear();
	F[1]=-INF;
	F[N+1]=INF;
	for(;Q--;){
		int l=read(),r=read(),v=read();
		Modify(l,v);
		Modify(r+1,-v);
		printf("%d\n",Ans);
	}
	return 0;
}
