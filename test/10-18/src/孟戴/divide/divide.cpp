//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("divide.in");
ofstream fout("divide.out");
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
int n,x,c[13];
int sz[1200003],fa[1200003],pri[100003];
int main(){
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	n=read();
	for(register int i=1;i<=9;++i) c[i]=read();
	for(register int i=2;i<=n;++i) fa[i]=read();
	x=n;
	for(int j=2;j*j<=n;++j){
		if(x%j==0){
			pri[++pri[0]]=j;
			while(x%j==0) x=x/j;
		}
	}
	if(x!=0) pri[++pri[0]]=x;
	for(register int i=0;i<=9;++i){
		if(i!=0){
			sz[1]=0;
			for(register int j=2;j<=n;++j){
				sz[j]=0;
				fa[j]=fa[j]+c[i];
				if(fa[j]>=j-1) fa[j]=fa[j]%(j-1);
				fa[j]=fa[j]+1;
			}
		}
		for(register int j=n;j>=2;--j){
			sz[j]++;
			sz[fa[j]]=sz[fa[j]]+sz[j];
		}
		sz[1]++;
		fout<<"Case #";
		write(i+1);
		fout<<":"<<endl;
		writeln(1);
		for(register int j=1;j<=pri[0];++j){
			int cnt=0;
			for(register int k=1;k<=n;++k){
				cnt+=(sz[k]%pri[j]==0);
				if(cnt*pri[j]>=n){
					writeln(pri[j]);
					break;
				}
			}
		}
		writeln(n);
	}
	return 0;
}
/*

in:
6 19940105 19940105 19940105 19940105 19940105 19940105 19940105 19940105 19940105
1 2 3 4 5

out:

*/
