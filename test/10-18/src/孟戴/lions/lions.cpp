//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#include<set>
using namespace std;
ifstream fin("lions.in");
ofstream fout("lions.out");
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
set<pair<long long,long long> >s;
set<pair<long long,long long> >::iterator it1,it2;
pair<long long,long long>k,k1;
long long n,a[200003],d[200003],e[200003],ans[200003];
inline void solve(){
	for(int i=1;i<=n-1;i++){
		it1=s.begin();
		it2=s.end();
		--it2;
		k=*it1,k1=*it2;
		s.erase(it1),s.erase(it2);
		int tx=k.first,ty=k.second,ex=k1.first,ey=k1.second;
		s.insert(make_pair(ex-tx,ey));
		e[i]=ey,d[ty]=i;
	}
}
int main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		s.insert(make_pair(a[i]=read(),i));
		d[i]=n+1;
	}
	solve();
	int md=n;
	for(int i=n-1;i>=1;i--){
		if(d[e[i]]<=md&&i<=md){
			md=i-1;
		}
	}
	for(int i=1;i<=n;i++){
		if(d[i]<=md){
			ans[++ans[0]]=i;
		}
	}
	sort(ans+1,ans+md+1);
	fout<<md<<endl;
	for(int i=1;i<=md;i++){
		fout<<ans[i]<<" ";
	}
	return 0;
}
/*

in:
7
8 2 5 1 7 3 3

out:
4
2 4 6 7

*/
