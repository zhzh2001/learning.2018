/*#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("legend2.in");
ofstream fout("legend.out");
int n,q,H;
struct tree{
	int llen,lc,rlen,rc,ans,tag,lh,rh,len,add;
}t[1000003];
inline tree pushup(tree a,tree b){
	tree c;
	c.lh=a.lh,c.rh=b.rh;
	c.tag=a.tag&b.tag;
	c.ans=a.ans+b.ans;
	if(a.rh==b.lh){
		if(a.tag&&b.tag){
			c.ans=0,c.llen=c.rlen=0,c.rc=c.lc=-1000000;
		}else if(a.tag){
			c.llen=a.len+b.llen,c.lc=b.lc;
			c.rlen=b.rlen,c.rc=b.rc;
		}else if(b.tag){
			c.rlen=b.len+a.rlen,c.rc=a.rc;
			c.llen=a.llen,c.lc=b.lc;
		}else{
			if(a.rc==-b.lc&&a.rc>=1){
				c.ans+=b.llen+a.rlen;
			}
			c.llen=a.llen,c.rlen=b.rlen;
			c.lc=a.lc,c.rc=b.rc;
		}
	}else{
		if(a.tag&&b.tag){
			c.llen=a.len,c.rlen=b.len;
			c.lc=b.lh-a.rh,c.rc=b.lh-a.rh;
		}else if(a.tag){
			c.llen=a.len,c.rlen=b.rlen;
			c.lc=b.lh-a.rh,c.rc=b.rc;
		}else if(b.tag){
			c.rlen=b.len,c.llen=a.llen;
			c.rc=b.lh-a.rh,c.lc=a.lc;
		}else{
			c.rlen=b.rlen,c.llen=a.llen;
			c.rc=b.rc,c.lc=a.lc;
		}
	}
	return c;
}
inline void pushup(int rt){
	t[rt].lh=t[rt<<1].lh,t[rt].rh=t[rt<<1|1].rh;
	t[rt].tag=0;
	t[rt].ans=t[rt<<1].ans+t[rt<<1|1].ans;
	if(t[rt<<1].rh==t[rt<<1|1].lh){
		if(t[rt<<1].tag&&t[rt<<1|1].tag){
			t[rt].llen=t[rt].rlen=0,t[rt].rc=t[rt].lc=-10000000;
			t[rt].tag=1;
		}else if(t[rt<<1].tag){
			t[rt].llen=t[rt<<1].len+t[rt<<1|1].llen,t[rt].lc=t[rt<<1|1].lc;
			t[rt].rlen=t[rt<<1|1].rlen,t[rt].rc=t[rt<<1|1].rc;
		}else if(t[rt<<1|1].tag){
			t[rt].rlen=t[rt<<1|1].len+t[rt<<1].rlen,t[rt].rc=t[rt<<1].rc;
			t[rt].llen=t[rt<<1].llen,t[rt].lc=t[rt<<1].lc;
		}else{
			if(t[rt<<1].rc==-t[rt<<1|1].lc&&t[rt<<1].rc>=1){
				t[rt].ans+=t[rt<<1|1].llen+t[rt<<1].rlen;
			}
			t[rt].llen=t[rt<<1].llen,t[rt].rlen=t[rt<<1|1].rlen;
			t[rt].lc=t[rt<<1].lc,t[rt].rc=t[rt<<1|1].rc;
		}
	}else{
		if(t[rt<<1].tag&&t[rt<<1|1].tag){
			t[rt].llen=t[rt<<1].len,t[rt].rlen=t[rt<<1|1].len;
			t[rt].lc=t[rt<<1|1].lh-t[rt<<1].rh,t[rt].rc=t[rt<<1|1].lh-t[rt<<1].rh;
		}else if(t[rt<<1].tag){
			t[rt].llen=t[rt<<1].len,t[rt].rlen=t[rt<<1|1].rlen;
			t[rt].lc=t[rt<<1|1].lh-t[rt<<1].rh,t[rt].rc=t[rt<<1|1].rc;
			if(t[rt<<1|1].lh-t[rt<<1].rh==-t[rt<<1|1].lc&&t[rt<<1|1].lc<=-1){
				t[rt].ans+=t[rt<<1|1].llen;
			}
		}else if(t[rt<<1|1].tag){
			t[rt].rlen=t[rt<<1|1].len,t[rt].llen=t[rt<<1].llen;
			t[rt].rc=t[rt<<1|1].lh-t[rt<<1].rh,t[rt].lc=t[rt<<1].lc;
			if(t[rt<<1|1].lh-t[rt<<1].rh==-t[rt<<1].rc&&t[rt<<1].rc>=1){
				t[rt].ans+=t[rt<<1].rlen;
			}
		}else{
			t[rt].rlen=t[rt<<1|1].rlen,t[rt].llen=t[rt<<1].llen;
			t[rt].rc=t[rt<<1|1].rc,t[rt].lc=t[rt<<1].lc;
			if(t[rt<<1|1].lh-t[rt<<1].rh==-t[rt<<1].rc&&t[rt<<1].rc>=1){
				t[rt].ans+=t[rt<<1].rlen;
			}
			if(t[rt<<1|1].lh-t[rt<<1].rh==-t[rt<<1|1].lc&&t[rt<<1|1].lc<=-1){
				t[rt].ans+=t[rt<<1|1].llen;
			}
		}
	}
}
inline void pushdown(int rt){
	if(t[rt].add!=0){
		t[rt<<1].lh+=t[rt].add,t[rt<<1|1].rh+=t[rt].add;
		t[rt<<1].add+=t[rt].add,t[rt<<1|1].add+=t[rt].add;
		t[rt].add=0;
	}
}
void build(int rt,int l,int r){
	if(l==r){
		t[rt].len=1,t[rt].lc=t[rt].rc=-10000000;
		t[rt].llen=t[rt].rlen=0,t[rt].lh=t[rt].rh=H,t[rt].tag=1;
		return;
	}
	int mid=l+r>>1;
	build(rt<<1,l,mid),build(rt<<1|1,mid+1,r);
	pushup(rt);
}
void update(int rt,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
		t[rt].lh+=val,t[rt].rh+=val,t[rt].add+=val;
		return;
	}
	pushdown(rt);
	int mid=l+r>>1;
	if(y<=mid){
		update(rt<<1,l,mid,x,y,val);
	}else if(x>mid){
		update(rt<<1|1,mid+1,r,x,y,val);
	}else{
		update(rt<<1,l,mid,x,mid,val),update(rt<<1|1,mid+1,r,mid+1,y,val);
	}
	pushup(rt);
}
int main(){
	fin>>n>>q>>H;
	build(1,1,n);
	while(q--){
		int x,y,val;
		fin>>x>>y>>val;
		update(1,1,n,x,y,val);
		//cout<<"wtf"<<query(1,1,n,1,n).rc<<endl;
		fout<<t[1].ans<<endl;
	}
	return 0;
}
/*

in:
5 5 5
1 1 -1
4 5 -1
4 4 1
1 5 1
4 5 -1

out:
0
2
3
3
2

*/
#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#include<set>
#include<map>
using namespace std;
ifstream fin("legend.in");
ofstream fout("legend.out");
inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x),puts("");
}
using namespace std;
inline bool pd(int x,int y){
	return (x>0&&y<0);
}
map<int,int>a;
set<pair<int,int> >s;
int n,q,v,ans;
inline void update(int k,int p){
	set<pair<int,int> >::iterator le,ri,it;
	int lf,ls,rf,rs;
	if(a[p]!=0){
		k+=a[p];
		it=s.find(make_pair(p,a[p]));
		--it,le=it,--it;
		it=s.find(make_pair(p,a[p]));
		++it,ri=it,++it;
		lf=le->first,ls=le->second;
		rf=ri->first,rs=ri->second;
		if(rf<=n&&pd(a[p],rs)){
			ans-=rf-p;
		}
		if(lf>=2&&pd(ls,a[p])){
			ans-=p-lf;
		}
		if(lf>=2&&rf<=n&&pd(ls,rs)){
			ans+=rf-lf;
		}
		s.erase(make_pair(p,a[p]));
	}
	a[p]=k;
	if(a[p]==0){
		return;
	}
	s.insert(make_pair(p,k));
	it=s.find(make_pair(p,k));
	--it,le=it,--it;
	it=s.find(make_pair(p,k));
	++it,ri=it,++it;
	lf=le->first,ls=le->second;
	rf=ri->first,rs=ri->second;
	if(lf>=2&&rf<=n&&pd(ls,rs)){
		ans-=rf-lf;
	}
	if(rf<=n&&pd(k,rs)){
		ans+=rf-p;
	}
	if(lf>=2&&pd(ls,k)){
		ans+=p-lf;
	}
}
int main(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	n=read(),q=read(),v=read();
	s.insert(make_pair(0,-1001)),s.insert(make_pair(n+2,1001));
	while(q--){
		int l=read(),r=read(),v=read();
		update(v,l),update(-v,r+1),writeln(ans);
	}
	return 0;
}
/*

in:
5 5 5
1 1 -1
4 5 -1
4 4 1
1 5 1
4 5 -1

out:
0
2
3
3
2

*/
