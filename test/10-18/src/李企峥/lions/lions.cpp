#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
const int inf=1e9;
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
struct D{
  int akk,od;
  bool operator <(const D &a) const {return (akk==a.akk)?(od>a.od):(akk>a.akk);}
};
set<D> s;
int n,a[100100],b[100100],c[100100];
int main() 
{
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	For(i,1,n)
	{
		a[i]=read();
		s.insert((D){a[i],i});
	}
	For(i,1,n-1)
	{
		D x=*s.begin();
		s.erase(x);
    	D y=*--s.end();
	    s.erase(y);
	    c[x.od]=i;
	    D c=(D){x.akk-y.akk,x.od};
	    b[++b[0]]=(y.od);
	    s.insert(c);
	}
	int ans=inf;
	for (int i=n;i;i--)if(c[b[i]]&&ans>c[b[i]]) ans=c[b[i]],i=ans;
	sort(b+1,b+ans);
	ans--;
	cout<<ans<<endl;
	For(i,1,ans)cout<<b[i]<<" ";
	return 0;
}
