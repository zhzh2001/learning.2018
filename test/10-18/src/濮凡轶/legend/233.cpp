#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

#define int long long

#define deb(a) cout << #a << " = " << a << endl;

const int maxn = 50005;

//int cnt = 0;

int n;

struct Tree
{
	struct Node
	{
		int ll, rr; // 左，右 0 的个数
		int l, r; // 最左边第一个非0位，最右边第一个非0位
		int M; // 中间已知高地的长度

		Node* ls;
		Node* rs;

		friend Node operator + (Node a, Node b)
		{
			Node ans;
			if(!a.l)
			{
				if(b.l <= 0)
					ans.ll = a.ll + b.ll;
				else
					ans.ll = 0;
				ans.l = b.l;
			}
			else
			{
				ans.l = a.l;
				ans.ll = a.ll;
			}
			if(!b.r)
			{
				if(a.r >= 0)
					ans.rr = a.rr + b.rr;
				else
					ans.rr = 0;
				ans.r = a.r;
			}
			else
			{
				ans.r = b.r;
				ans.rr = b.rr;
			}
			ans.M = a.M + b.M;
			if(a.r > 0 && b.l < 0)
				ans.M += a.rr + b.ll + 1;
			return ans;
		}

		Node()
		{
			l = r = M = 0;
			ll = rr = 1;
			ls = rs = NULL;
		}
		
		Node(int len)
		{
			l = r = M = 0;
			ll = rr = len;
			ls = rs = NULL;
		}
	};

	inline Node node(int len)
	{
		Node a(len);
		return a;
	}

	Node* root;

	int t;

	inline void push_up(Node* &k, int l, int r)
	{
		int mid = (l + r) >> 1;
		if(k->ls == NULL && k->rs == NULL)
			*k = node(r - l + 1);
		else if(k->rs == NULL)
			*k = *(k->ls) + node(r - mid);
		else if(k->ls == NULL)
			*k = node(mid - l + 1) + *(k->rs);
		else
			*k = *(k->ls) + *(k->rs);
	}

	inline int query()
	{
		return root->M;
	}

	inline void change(int l, int r, Node* &k, int K,
	                   int x)
	{
		if(k == NULL)
			k = new Node();
		if(l == r)
		{
			k += t;
			k->l += x;
			k->r += x;
			k->ll = k->rr = (k->l == 0);
			k->M = 0;
			return;
		}
		int mid = (l + r) >> 1;
		if(K <= mid)
			change(l, mid, k->ls, K, x);
		else
			change(mid + 1, r, k->rs, K, x);
		push_up(k, l, r);
	}

	Tree()
	{
		root = new Node(n);
	}
} tr;

signed main()
{
//	freopen("legend.in", "r", stdin);
//	freopen("legend.out", "w", stdout);
	int Q;
	scanf("%lld%lld%*d", &n, &Q);
	n++;
	while(Q--)
	{
		int l, r, v;
//		cnt++;
		scanf("%lld%lld%lld", &l, &r, &v);
		if(l > 1)
			tr.change(1, n, tr.root, l, v);
		if(r + 1 < n)
			tr.change(1, n, tr.root, r + 1, -v);
		printf("%lld\n", tr.query());
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
