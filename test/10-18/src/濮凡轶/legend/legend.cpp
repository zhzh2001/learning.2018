#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

#define int long long

#define deb(a) cout << #a << " = " << a << endl;

const int maxn = 50005;

//int cnt = 0;

int n;

#define ls(x) (x << 1)
#define rs(x) (x << 1 | 1)

struct Tree
{
	struct Node
	{
		int ll, rr; // 左，右 0 的个数
		int l, r; // 最左边第一个非0位，最右边第一个非0位
		int M; // 中间已知高地的长度

		friend Node operator + (Node a, Node b)
		{
			Node ans;
			if(!a.l)
			{
				if(b.l <= 0)
					ans.ll = a.ll + b.ll;
				else
					ans.ll = 0;
				ans.l = b.l;
			}
			else
			{
				ans.l = a.l;
				ans.ll = a.ll;
			}
			if(!b.r)
			{
				if(a.r >= 0)
					ans.rr = a.rr + b.rr;
				else
					ans.rr = 0;
				ans.r = a.r;
			}
			else
			{
				ans.r = b.r;
				ans.rr = b.rr;
			}
			ans.M = a.M + b.M;
			if(a.r > 0 && b.l < 0)
				ans.M += a.rr + b.ll + 1;
			return ans;
		}
	} no[maxn << 2];

	int t;

	inline void build_tree()
	{
		for(t = 1; t <= n; t <<= 1);
		for(int i = t + 1; i <= t + n; ++i)
		{
			no[i].l = no[i].r = no[i].M = 0;
			no[i].ll = no[i].rr = 1;
		}
		for(int i = t - 1; i; --i)
			no[i] = no[ls(i)] + no[rs(i)];
	}

	inline int query()
	{
		return no[1].M;
	}

	inline void change(int k, int x)
	{
//		if(cnt == 31083)
//		{
//			deb(no[k].l);
//			for(int i = t + 1; i <= t + 10; ++i)
//				cout << no[i].l << ' ';
//			cout << endl;
//		}
		k += t;
		no[k].l += x;
		no[k].r += x;
		no[k].ll = no[k].rr = (no[k].l == 0);
		no[k].M = 0;
		for(k >>= 1; k; k >>= 1)
			no[k] = no[ls(k)] + no[rs(k)];
//		if(cnt == 31083)
//		{
//			deb(no[k].l);
//			for(int i = t + 1; i <= t + 10; ++i)
//				cout << no[i].l << ' ';
//			cout << endl;
//		}
	}
} tr;

signed main()
{
	freopen("legend.in", "r", stdin);
	freopen("legend.out", "w", stdout);
	int Q;
	scanf("%lld%lld%*d", &n, &Q);
	n++;
	tr.build_tree();
	while(Q--)
	{
		int l, r, v;
//		cnt++;
		scanf("%lld%lld%lld", &l, &r, &v);
		if(l > 1)
			tr.change(l, v);
		if(r + 1 < n)
			tr.change(r + 1, -v);
//		tr.query();
		printf("%lld\n", tr.query());
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
