#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

const int maxn = 100005;

struct Lion
{
	int nl, old;

	inline bool operator < (const Lion& other) const
	{
		return this->nl < other.nl
		       || (this->nl == other.nl
		           && this->old < other.old);
	}
} ll[maxn];

inline Lion operator - (Lion a, Lion b)
{
	Lion ans;
	ans.nl = a.nl - b.nl;
	ans.old = a.old;
	return ans;
}

int n;

inline bool pan(int xian)
{
	int l, r;
	for(l = 1, r = n; r > xian; --r)
	{
		Lion now = ll[r];
		while(l <= xian && ll[xian] < now - ll[l])
		{
			now = now - ll[l];
			l++;
		}
		if(l > xian)
			return true;
	}
	return false;
}

int Ans[maxn];

int main()
{
	scanf("%d", &n);
	for(int i = 1; i <= n; ++i)
	{
		scanf("%d", &ll[i].nl);
		ll[i].old = i;
	}
	sort(ll + 1, ll + n + 1);
//	int ans = 0;
	for(int i = 1; i <= n; ++i)
		cout << ll[i].nl << ' ' << ll[i].old << endl;
	cout << endl;
	return 0;
	int l = 1, r = n;
	while(l < r)
	{
		int mid = (l + r) >> 1;
		if(pan(mid))
			l = mid;
		else
			r = mid - 1;
	}
	printf("%d\n", l);
	return 0;
}
