#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

const int maxn = 100005;

struct Lion
{
	int nl, old;
	
	inline bool operator < (const Lion& other) const
	{
		return this->nl < other.nl || (this->nl == other.nl && this->old < other.old);
	}
} ll[maxn];

inline Lion operator - (Lion a, Lion b)
{
	Lion ans;
	ans.nl = a.nl - b.nl;
	ans.old = a.old;
	return ans;
}

int Ans[maxn];

int main()
{
	freopen("lions.in", "r", stdin);
	freopen("lions.out", "w", stdout);
	int n;
	scanf("%d", &n);
	for(int i = 1; i <= n; ++i)
	{
		scanf("%d", &ll[i].nl);
		ll[i].old = i;
	}
	sort(ll + 1, ll + n + 1);
	int ans = 0;
//	for(int i = 1; i <= n; ++i)
//		cout << ll[i].nl << ' ' << ll[i].old << endl;
//	cout << endl;
	for(int l = 1, r = 1; l < n && r <= n; ++l)
	{
		ans = l;
		while(r <= n && ll[r] - ll[l] < ll[l + 1])
			r++;
//		cout << l << ' ' << r << endl;
//		cout << (ll[r] - ll[l]).nl << endl;
		if(r > n)
			break;
	}
	ans--;
	printf("%d\n", ans);
	for(int i = 1; i <= ans; ++i)
	Ans[i] = ll[i].old;
	sort(Ans + 1, Ans + ans + 1);
	for(int i = 1; i <= ans; ++i)
		printf("%d ", Ans[i]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
