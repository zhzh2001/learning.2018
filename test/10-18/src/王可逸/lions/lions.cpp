#include <bits/stdc++.h>
#define int long long
using namespace std;
const int N=120000;
inline int read(){
    int x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}
struct node1 {
	int num,at;
	friend bool operator < (node1 a,node1 b) {
		if(a.num!=b.num) return a.num<b.num;
		else return a.at<b.at;
	}
};
struct node2 {
	int num,at;
	friend bool operator < (node2 a,node2 b) {
		if(a.num!=b.num) return a.num>b.num;	
		else return a.at>b.at;
	}
};
priority_queue <node1> q1;
priority_queue <node2> q2;
int n,eatx[N],eaty[N],cnt;
bool vis[N];
signed main() {
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) {
		int x=read();
		q1.push((node1) {x,i});
		q2.push((node2) {x,i});
	}
	for(int i=1;i<n;i++) {
		node1 w1=q1.top();
		while(vis[w1.at]) {
			q1.pop();
			w1=q1.top();
		}
		q1.pop();
		node2 w2=q2.top();
		while(vis[w2.at]) {
			q2.pop();
			w2=q2.top();
		}
		q2.pop();
		vis[w2.at]=true;
		eatx[i]=w1.at;
		eaty[i]=w2.at;
		w1.num-=w2.num;
		q1.push(w1);
		q2.push((node2) {w1.num,w1.at});
	}
//	for(int i=1;i<=n-1;i++) 
//		cout<<eatx[i]<<" "<<eaty[i]<<'\n';
	int now=n-2;
	for(;now>=1;now--) {
		if(eaty[now+1]==eatx[now]) continue;
		else break;
	}
	now++;
	if(now==n-2) now--;
	memset(vis,0,sizeof vis);
	for(int i=1;i<=now;i++) vis[eaty[i]]=1;
	cout<<now<<'\n';
	for(int i=1;i<=n;i++) if(vis[i]) {write(i);putchar(' ');}
	return 0;
}
