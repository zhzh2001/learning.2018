#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
    int x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}
const int N=56000;
int n,q,h,l,r,v,a[N],xkx[N],tree[N<<2],add[N<<2],tree2[N<<2],add2[N<<2];
set <int> vis;
int count() {
	int num=0,ans=0;
	bool up=false;
	for(int i=2;i<=n;i++) {
		if(a[i-1]<a[i]) up=true,num=1;
		else if(a[i-1]==a[i] && up==true) num++;
		else if(a[i-1]>a[i] && up==true) {
			up=false;
			ans+=num;
		}
	}
	return ans;
}
void subtask1() {
	for(int i=1;i<=n;i++) a[i]=h;
	while(q--) {
		l=read();r=read(),v=read();
		// if(q%500==0) cout<<q<<'\n';
		for(int i=l;i<=r;i++) a[i]+=v;
		// for(int i=1;i<=n;i++) cout<<a[i]<<" ";
		// cout<<'\n';
		write(count());
		puts("");
	}
}
void build(int rt,int l,int r) {
	if(l==r) {
		tree[rt]=a[l];
		return;
	}
	int mid=(l+r) >> 1;
	build(rt<<1,l,mid);
	build(rt<<1,mid+1,r);
	tree[rt]=tree[rt<<1]+tree[rt<<1|1];
}
void pushdown(int rt,int l,int r) {
	if(add[rt]!=0) {
		add[rt<<1]+=add[rt];
		add[rt<<1|1]+=add[rt];
		tree[rt<<1]+=add[rt]*l;
		tree[rt<<1|1]+=add[rt]*r;
		add[rt]=0;
	}
}
void update(int L,int R,int C,int l,int r,int rt) {
//	cout<<L<<" "<<R<<" "<<C<<" "<<l<<" "<<r<<" "<<rt<<'\n';
	if(L<=l && r<=R) {
		add[rt]=C;
		tree[rt]+=(r-l+1)*C;
		return;
	}
	int mid=(l+r) >> 1;
	pushdown(rt,mid-l+1,r-mid);
	if(L<=mid) update(L,R,C,l,mid,rt<<1);	
	if(R>mid) update(L,R,C,mid+1,r,rt<<1|1);
	tree[rt]=tree[rt<<1]+tree[rt<<1|1];
}
int find(int at,int l,int r,int rt) {
//	cout<<"finding "<<at<<" "<<l<<" "<<r<<" "<<rt<<'\n';
	if(l==r && l==at) return tree[rt];
	int mid=(l+r) >> 1;
	pushdown(rt,mid-l+1,r-mid);
	if(at<=mid) return find(at,l,mid,rt<<1);
	else return find(at,mid+1,r,rt<<1|1);
}
void build2(int rt,int l,int r) {
	if(l==r) {
		tree[rt]=xkx[l];
		return;
	}
	int mid=(l+r) >> 1;
	build2(rt<<1,l,mid);
	build2(rt<<1,mid+1,r);
	tree2[rt]=tree2[rt<<1]+tree2[rt<<1|1];
}
void pushdown2(int rt,int l,int r) {
	if(add2[rt]!=0) {
		add2[rt<<1]+=add2[rt];
		add2[rt<<1|1]+=add2[rt];
		tree2[rt<<1]+=add2[rt]*l;
		tree2[rt<<1|1]+=add2[rt]*r;
		add2[rt]=0;
	}
}
void update2(int L,int R,int C,int l,int r,int rt) {
//	cout<<L<<" "<<R<<" "<<C<<" "<<l<<" "<<r<<" "<<rt<<'\n';
	if(L<=l && r<=R) {
		add2[rt]=C;
		tree2[rt]+=(r-l+1)*C;
		return;
	}
	int mid=(l+r) >> 1;
	pushdown2(rt,mid-l+1,r-mid);
	if(L<=mid) update2(L,R,C,l,mid,rt<<1);	
	if(R>mid) update2(L,R,C,mid+1,r,rt<<1|1);
	tree2[rt]=tree2[rt<<1]+tree2[rt<<1|1];
}
int find2(int at,int l,int r,int rt) {
	if(l==r && l==at) return tree2[rt];
	int mid=(l+r) >> 1;
	pushdown2(rt,mid-l+1,r-mid);
	if(at<=mid) return find2(at,l,mid,rt<<1);
	else return find2(at,mid+1,r,rt<<1|1);
}
int sum(int L,int R,int l,int r,int rt) {
	if(L<=l && r<=R) return tree[rt];
	int mid=(l+r) >> 1,tot=0;
	pushdown2(rt,mid-l+1,r-mid);
	if(L<=mid) tot+=sum(L,R,l,mid,rt<<1);
	if(R>mid) tot+=sum(L,R,mid+1,r,rt<<1|1);
	return tot;
}
int count2() {
	int cnt=0,ans=0,last=0;
	for(set <int> :: iterator it=vis.begin();it!=vis.end();it++) {
		cnt++;
		if(cnt==1) {
			last=*it;
			continue;	
		}
		if(xkx[last]==0 && xkx[*it]==2) {
			ans+=sum(last+1,*it-1,1,n,1);
		} 
	}
	return ans;
}
void subtask2() {
	/*
		xkx[i]=0:a[i-1]<a[i];
		xkx[i]=1:a[i-1]=a[i];
		xkx[i]=2:a[i-1]>a[i];
	*/
	build(1,1,n);
	update(1,n,h,1,n,1);
	for(int i=1;i<=n;i++) xkx[i]=1;
	build2(1,1,n);
	while(q--) {
		l=read(),r=read(),v=read();	
		update(l,r,v,1,n,1);
		int oldl=0,oldr=0;
		if(l!=1) oldl=find2(l,1,n,1);
		if(r!=n) oldr=find2(r+1,1,n,1);
		
		
//		cout<<oldl<<" "<<oldr<<'\n';
		
		cout<<"update:"<<find(1,1,n,1)<<" "<<find(2,1,n,1)<<" "<<find(3,1,n,1)<<" "<<find(4,1,n,1)<<" "<<find(5,1,n,1)<<'\n';
		
		
		
		
		
		if(l!=1){
			cout<<"l:"<<find(l-1,1,n,1)<<" "<<find(l,1,n,1)<<'\n';
			if(find(l-1,1,n,1)<find(l,1,n,1)) xkx[l]=0;
			if(find(l-1,1,n,1)>find(l,1,n,1)) xkx[l]=2;
			if(find(l-1,1,n,1)==find(l,1,n,1)) xkx[l]=1;
		} 
		if(r!=n){
			cout<<"r:"<<find(r,1,n,1)<<" "<<find(r+1,1,n,1)<<'\n';
			if(find(r,1,n,1)<find(r+1,1,n,1)) xkx[r+1]=0;
			if(find(r,1,n,1)>find(r+1,1,n,1)) xkx[r+1]=2;
			if(find(r,1,n,1)==find(r+1,1,n,1)) xkx[r+1]=1;
		}
		cout<<"l && r+1 "<<l<<" "<<xkx[l]<<" "<<r+1<<" "<<xkx[r+1]<<'\n';
		if(l!=1) update2(l,l,xkx[l]-oldl,1,n,1);
		if(r!=n) update2(r+1,r+1,xkx[r+1]-oldr,1,n,1);
		
		
		cout<<find2(1,1,n,1)<<" "<<find2(2,1,n,1)<<" "<<find2(3,1,n,1)<<" "<<find2(4,1,n,1)<<" "<<find2(5,1,n,1)<<'\n';
		
		
		
		if(l!=1){
			if(xkx[l]!=1) vis.insert(l);
			if(xkx[l]==1 && vis.find(l)!=vis.end()) vis.erase(l);
		}
		if(r!=n) {
			if(xkx[r+1]!=1) vis.insert(r+1);
			if(xkx[r+1]==1 && vis.find(r+1)!=vis.end()) vis.erase(r+1);
		}	
//		for(set <int> :: iterator it=vis.begin();it!=vis.end();it++) cout<<*it<<" ";
//		cout<<'\n';
//		write(count2());
//		puts("");
	}
}
signed main() {
	freopen("legend.in","r",stdin); 
	freopen("legend.out","w",stdout);
	n=read();q=read();h=read();
//	if(n<=1200) subtask1();
//	else if(n<=50000) subtask2();
//	else subtask3();
	subtask1();
}
