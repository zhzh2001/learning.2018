#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
#define pa pair<int,int>
#define mk make_pair
#define fi first
#define se second
set<pa>s;
const int N=100005;
int n;
int a[N];
int d[N];//记录狮子死亡时间 
int e[N];//记录这次狮子进食序号 
int ant[N];
int main()
{
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		s.insert(mk(a[i],i));
		d[i]=n+1;
	}
	for(int i=1;i<n;i++){
		set<pa>::iterator it,it2;
		it=s.begin();
		it2=s.end();it2--;
		pa k,k1;
		k=*it;k1=*it2;
		s.erase(it);
		s.erase(it2);
		int tx,ty,ex,ey;
		tx=k.fi;ty=k.se;
		ex=k1.fi;ey=k1.se;
		s.insert(mk(ex-tx,ey));
		e[i]=ey;d[ty]=i;
	}
	int et=n;
	for(int i=n-1;i;i--)
		if(d[e[i]]<=et&&i<=et)et=i-1;
	for(int i=1;i<=n;i++)
		if(d[i]<=et)ant[++ant[0]]=i;
	sort(ant+1,ant+et+1);
	writeln(et);
	for(int i=1;i<=et;i++)
		write(ant[i]),putchar(' ');
}
