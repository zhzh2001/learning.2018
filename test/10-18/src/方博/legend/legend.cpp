#include<bits/stdc++.h>
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
#define pa pair<int,int>
#define mk make_pair
#define fi first
#define se second
using namespace std;
set<pa>s;
int n,q,v;
int ans;
map<int,int>a;
inline bool check(int x,int y)
{
	if(x>0&&y<0)return true;
	return false;
}
void change(int k,int p)
{
	set<pa>::iterator le,ri,it;
	int lef,les,rif,ris;
	if(a[p]!=0){
		int pp=a[p];
		k+=pp;
		it=s.find(mk(p,pp));it--;le=it--;
		it=s.find(mk(p,pp));it++;ri=it++;
		lef=le->fi;les=le->se;
		rif=ri->fi;ris=ri->se;
		if(rif<n+1&&check(pp,ris))ans-=rif-p;
		if(lef>1&&check(les,pp))ans-=p-lef;
		if(lef>1&&rif<n+1&&check(les,ris))ans+=rif-lef;
		s.erase(mk(p,pp));
	}
	a[p]=k;
	if(k==0)return;
	s.insert(mk(p,k));
	it=s.find(mk(p,k));it--;le=it--;
	it=s.find(mk(p,k));it++;ri=it++;
	lef=le->fi;les=le->se;
	rif=ri->fi;ris=ri->se;
	if(lef>1&&rif<n+1&&check(les,ris))ans-=rif-lef;
	if(rif<n+1&&check(k,ris))ans+=rif-p;
	if(lef>1&&check(les,k))ans+=p-lef;
}
int main()
{
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	n=read();q=read();v=read();
	s.insert(mk(0,-1001));
	s.insert(mk(n+2,1001));
	for(int i=1;i<=q;i++){
		int l,r,v;
		l=read();r=read();v=read();
		change(v,l);
		change(-v,r+1);
		writeln(ans);
	}
}
