#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=1200005;
int n,c[13];
int head[N],nxt[2*N],tail[2*N],tot;
int sz[N],fa[N],p[N];
inline void addto(int x,int y)
{
	tail[++tot]=y;
	nxt[tot]=head[x];
	head[x]=tot;
}
int main()
{
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	n=read();
	for(int i=1;i<=9;i++)
		c[i]=read();
	for(int i=2;i<=n;i++)
		fa[i]=read();
	int now=n;
	for(int j=2;j*j<=n;j++){
		if(now%j==0){
			p[++p[0]]=j;
			while(now%j==0){
				now=now/j;
			}
		}
	}
	if(now!=0)p[++p[0]]=now;
	for(int i=0;i<=9;i++){
		if(i!=0){
			tot=0;
			head[1]=0;
			for(int j=2;j<=n;j++){
				head[j]=0;
				fa[j]=(fa[j]+c[i])%(j-1)+1;
			}
		}
		for(int j=2;j<=n;j++)
			addto(fa[j],j);
		for(int j=n;j>=1;j--){
			sz[j]=1;
			for(int k=head[j];k;k=nxt[k])sz[j]=sz[j]+sz[tail[k]];
		}
		cout<<"Case #";write(i+1);
		cout<<":";puts("");writeln(1);
		for(int j=1;j<=p[0];j++){
			int tmp=0;
			for(int k=1;k<=n;k++){
				if(sz[k]%p[j]==0)tmp++;
				if(tmp>=n/p[j]){
					writeln(p[j]);
					break;
				}
			}
		}
		writeln(n);
	}
	return 0;
}
