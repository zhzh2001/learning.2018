//#include<cstdio>
//#include<iostream>
//#include<algorithm>
//using namespace std;
//#define gc c=getchar()
//int read(){
//	int x=0,f=1;char gc;
//	for(;!isdigit(c);gc)if(c=='-')f=-1;
//	for(;isdigit(c);gc)x=x*10+c-'0';
//	return x*f;
//}
//#undef gc
//const int MAXN=50005;
//int q,h;
//int l[MAXN],r[MAXN],v[MAXN];
//int n,pos[MAXN<<1];
//int ll[MAXN<<1],rr[MAXN<<1];
//struct Segtree{
//	int l,r,x,lazy;
//}tree[MAXN<<2];
//void build(int k,int l,int r){
//	tree[k].l=l;tree[k].r=r;
//	if(l==r){
//		tree[k].x=h;
//		return;
//	}
//	int mid=(l+r)>>1;
//	build(k<<1,l,mid);build(k<<1|1,mid+1,r);
//}
//void pushdown(int k){
//	if(tree[k].lazy==0)return;
//	tree[k<<1].lazy+=tree[k].lazy;
//	tree[k<<1|1].lazy+=tree[k].lazy;
//	tree[k].lazy=0;
//}
//void upd(int k,int l,int r,int v){
//	if(tree[k].l>r||tree[k].r<l)return;
//	if(l<=tree[k].l&&tree[k].r<=r){
//		tree[k].lazy+=v;
//		return;
//	}
//	pushdown(k);
//	upd(k<<1,l,r,v);upd(k<<1|1,l,r,v);
//}
//int query(int k,int l,int r){
//	if(tree[k].l>r||tree[k].r<l)return 0;
//	if(l<=tree[k].l&&tree[k].r<=r)return tree[k].x+tree[k].lazy;
//	pushdown(k);
//	return query(k<<1,l,r)+query(k<<1|1,l,r);
//}
//int main(){
//	n=read(),q=read(),h=read();
//	n=0;
//	for(int i=1;i<=q;i++){
//		l[i]=read(),r[i]=read(),v[i]=read();
//		pos[++n]=l[i];
//		pos[++n]=r[i];
//	}
//	sort(pos+1,pos+n+1);
//	n=unique(pos+1,pos+n+1)-pos-1;
//	build(1,1,n);
//	for(int i=1;i<=n;i++)ll[i]=1,rr[i]=n;
//	int ans=0;
//	for(int i=1;i<=q;i++){
//		int L=lower_bound(pos+1,pos+n+1,l[i])-pos;
//		int R=lower_bound(pos+1,pos+n+1,r[i])-pos;
//		int prel=query(1,L,L);
//		int prer=query(1,R,R);
//		upd(1,L,R,v[i]);
//		int LR=L-1;
//		int RL=R+1;
//		if(LR<1&&RL>n){
//			printf("%d\n",ans);
//			continue;
//		}
//		if(LR<1){
//			int RR=rr[RL];
//			if(RR==n){
//				printf("%d\n",ans);
//				continue;
//			}
//		}
//		if(RL>n){
//			int LL=ll[LR];
//			if(LL==1){
//				printf("%d\n",ans);
//				continue;
//			}
//		}
////		L=query(1,L,L),R=query(1,R,R);
////		LL=query(1,LL,LL),RR=query(1,RR,RR);
////		cout<<L<<' '<<R<<' '<<LL<<' '<<RR<<endl;
//	}
//}
#pragma GCC optimize(2)
#include<cstdio>
#include<iostream>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
int a[50005];
int main(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	int n=read(),q=read(),h=read();
	for(int i=1;i<=n;i++)a[i]=h;
	a[0]=a[n+1]=0x7fffffff;
	while(q--){
		int l=read(),r=read(),v=read();
		for(register int i=l;i<=r;i++)a[i]+=v;
		int ans=0;
		for(register int i=1;i<=n;i++){
			int L=i;
			while(a[i]==a[i+1])i++;
			int R=i;
			if(a[L]>a[L-1]&&a[R]>a[R+1])ans+=R-L+1;
		}
		printf("%d\n",ans);
	}
}
