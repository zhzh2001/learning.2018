#pragma GCC opitimize(2)
#include<set>
#include<cstdio>
#include<iostream>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
struct Data{
	Data(){}
	Data(int X,int Id){
		x=X,id=Id;
	}
	int id,x;
	bool operator<(const Data &b)const{
		if(x!=b.x)return x<b.x;
		return id<b.id;
	}
};
const int MAXN=1e5+5;
set<Data>s;
Data opt[MAXN][3];
int ans,num;
bool vis[MAXN];
void del(int d){
	if(d>num)return;
	del(d+1);
	s.erase(opt[d][2]);
	s.insert(opt[d][1]);
	s.insert(opt[d][0]);
	ans--;
	vis[opt[d][1].id]=0;
}
void solve(int d){
	if(s.size()<2)return;
	Data Max=*s.rbegin();
	Data Min=*s.begin();
	s.erase(s.begin());
	s.erase(--s.end());
	Data nxt=Data(Max.x-Min.x,Max.id);
	s.insert(nxt);
	ans++;
	vis[Min.id]=1;
	num++;
	opt[num][0]=Max;
	opt[num][1]=Min;
	opt[num][2]=nxt;
	solve(d+1);
	if(vis[Max.id]){
		del(d);
		num=d-1;
	}
}
int main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++)s.insert(Data(read(),i));
	solve(1);
	printf("%d\n",ans);
	for(int i=1;i<=n;i++)
		if(vis[i])printf("%d ",i);
}
