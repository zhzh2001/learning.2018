#include<bits/stdc++.h>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e5+5,INF=1e9;
struct node{
	int x,y; 
};
inline bool operator <(node A,node B){
	if (A.x==B.x) return A.y>B.y;
	return A.x>B.x;
}
set<node> se;
int n,cnt,a[N],ans,kill[N],pos[N]; 
inline void init(){
	n=read(); ans=INF;
	for (int i=1;i<=n;i++){
    	a[i]=read();
    	se.insert((node){a[i],i});
	}
}
inline void solve(){
	for(int i=1;i<n;i++){
    	node A=*se.begin(); se.erase(A);
		node B=*--se.end(); se.erase(B);
    	kill[++cnt]=B.y; 
    	pos[A.y]=i; node tmp=(node){A.x-B.x,A.y};
		se.insert(tmp);
  	}
  	for (int i=n;i;i--) if (pos[kill[i]]&&ans>pos[kill[i]]) ans=pos[kill[i]],i=ans;
  	ans--; sort(kill+1,kill+ans+1); 
	writeln(ans);
	for(int i=1;i<=ans;i++) write(kill[i]),putchar(' ');
}
int main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	init(); solve();
	return 0;
}
