#include<cstdio>
#include<algorithm>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=50005,INF=1e9;
struct node{
	int l,r,l1,l2,l3,r1,r2,r3,sum,plu;
}a[N<<2];
int n,q,H;
inline void pushup(int k){
	a[k].l1=a[k<<1].l1; a[k].r1=a[k<<1|1].r1; int s1=a[k<<1].r-a[k<<1].l+1,s2=a[k<<1|1].r-a[k<<1|1].l+1;
	a[k].l2=(a[k<<1].l2==s1)?a[k<<1].l2+a[k<<1|1].l2*(a[k<<1].r1==a[k<<1|1].l1):a[k<<1].l2;
	a[k].r2=(a[k<<1|1].r2==s2)?a[k<<1].r2*(a[k<<1].r1==a[k<<1|1].l1)+a[k<<1|1].r2:a[k<<1|1].r2;
	if (a[k<<1].l3==INF) a[k].l3=(a[k<<1].r1==a[k<<1|1].l1)?a[k<<1|1].l3:a[k<<1|1].l1;
		else a[k].l3=a[k<<1].l3;
	if (a[k<<1|1].r3==INF) a[k].r3=(a[k<<1].r1==a[k<<1|1].l1)?a[k<<1].r3:a[k<<1].r1;
		else a[k].r3=a[k<<1|1].r3;
	a[k].sum=a[k<<1].sum+a[k<<1|1].sum;
	if (a[k<<1].r1==a[k<<1|1].l1){
		a[k].sum+=(a[k<<1].r3<a[k<<1].r1&&a[k<<1|1].l3<a[k<<1|1].l1)?a[k<<1].r2+a[k<<1|1].l2:0;
	}else{
		a[k].sum+=(a[k<<1].r3<a[k<<1].r1&&a[k<<1|1].l1<a[k<<1].r1)?a[k<<1].r2:0;
		a[k].sum+=(a[k<<1].r1<a[k<<1|1].l1&&a[k<<1|1].l3<a[k<<1|1].l1)?a[k<<1|1].l2:0;
	}
}
void build(int k,int l,int r){
	a[k].l=l; a[k].r=r;
	if (l==r){
		a[k]=(node){l,r,H,1,INF,H,1,INF,0,0};
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid); build(k<<1|1,mid+1,r); 
	pushup(k);
}
inline void Plus(int k,int v){
	a[k].l1+=v; a[k].r1+=v;
	if (a[k].l3!=INF) a[k].l3+=v; if (a[k].r3!=INF) a[k].r3+=v;
	a[k].plu+=v;
}
inline void pushdown(int k){
	if (a[k].plu){
		Plus(k<<1,a[k].plu); Plus(k<<1|1,a[k].plu);
		a[k].plu=0;
	}
}
void update(int k,int l,int r,int x,int y,int v){
	if (l==x&&r==y){
		Plus(k,v);	
		return;	
	}
	int mid=(l+r)>>1; pushdown(k);
	if (mid>=y) update(k<<1,l,mid,x,y,v);
		else if (mid<x) update(k<<1|1,mid+1,r,x,y,v);
			else update(k<<1,l,mid,x,mid,v),update(k<<1|1,mid+1,r,mid+1,y,v);
	pushup(k);
} 
inline void init(){
	n=read(); q=read(); H=read();
	build(1,1,n);
}
inline void solve(){
	for (int i=1;i<=q;i++){
		int L=read(),R=read(),v=read();
		update(1,1,n,L,R,v);
		writeln(a[1].sum);
	}
}
signed main(){
	freopen("legend.in","r",stdin); freopen("legend.out","w",stdout);
	init(); solve();
	return 0;
}
