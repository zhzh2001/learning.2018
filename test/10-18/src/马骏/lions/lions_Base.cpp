// 此程序用于骗分，方法确定有误
// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100010
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
struct arr
{
	int val,cnt;
	friend bool operator < (arr a,arr b)
	{
		return a.val>b.val||a.val==b.val&&a.cnt>b.cnt;
	}
};
set<arr> s;
arr a[maxn];
int n,b[maxn],f[maxn],ans,aans;
int dfs(int k)
{
	if(s.size()==2) return k==1;
	arr tmp1=*s.begin(),tmp2=*s.rbegin();
	tmp1.val-=tmp2.val;
	s.erase(s.begin());
	s.erase(tmp2);
	s.insert(tmp1);
	return dfs(k^1);
}
signed main()
{
	// freopen("lions.in","r",stdin);
	// freopen("lions.out","w",stdout);
	n=read();
	for(int i=1;i<=n;s.insert(a[i++]))
	{
		a[i].val=read();
		a[i].cnt=i;
	}
	if(n==1)
	{
		wln(0);
		return 0;
	}
	sort(a+1,a+n+1);
	for(;;)
	{
		if(s.size()==1)
		{
			aans=1;
			break;
		}
		arr tmp1=*s.begin(),tmp2=*s.rbegin();
		if(f[tmp2.cnt])
		{
			aans=f[tmp2.cnt];
			break;
		}
		f[tmp1.cnt]=tmp2.cnt;
		tmp1.val-=tmp2.val;
		s.erase(s.begin());
		s.erase(tmp2);
		s.insert(tmp1);
	}
	for(int i=1;i<=n;i++)
		if(aans==a[i].cnt) ans=i;
	if(s.size()>2&&dfs(0)) ans--;
	wln(n-ans);
	for(int i=ans+1;i<=n;i++)
		b[i-ans]=a[i].cnt;
	sort(b+1,b+n-ans+1);
	for(int i=1;i<=n-ans;i++)
		wrs(b[i]);
	return 0;
}
