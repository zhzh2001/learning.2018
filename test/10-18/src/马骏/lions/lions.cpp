// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100010
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
struct arr
{
	int val,cnt;
	friend bool operator < (arr a,arr b)
	{
		return a.val>b.val||a.val==b.val&&a.cnt>b.cnt;
	}
};
arr a[maxn];
set<arr> s;
vector<int> v;
int n,f[maxn][2],bb[maxn][2],r,now;
signed main()
{
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
	{
		a[i].val=read();
		a[i].cnt=i;
		s.insert(a[i]);
	}
	if(n==1)
	{
		wln(0);
		return 0;
	}
	for(int now=1;s.size()!=1;now++)
	{
		arr tmp1=*s.begin(),tmp2=*s.rbegin();
		if(f[tmp2.cnt][0])
		{
			bb[++r][0]=tmp2.cnt;
			bb[r][1]=now;
		}
		f[tmp1.cnt][0]=tmp2.cnt;
		f[tmp1.cnt][1]=now;
		tmp1.val-=tmp2.val;
		s.erase(s.begin());
		s.erase(tmp2);
		s.insert(tmp1);
	}
	now=n-1;
	for(int i=r;i;i--)
	{
		if(bb[i][1]>now) continue;
		now=f[bb[i][0]][1]-1;
	}
	wln(now);
	sort(a+1,a+n+1);
	for(int i=n-now+1;i<=n;i++)
		v.push_back(a[i].cnt);
	sort(v.begin(),v.end());
	for(int i=0;i<v.size();i++)
		wrs(v[i]);
	return 0;
}
