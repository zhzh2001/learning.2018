// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 50010
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int q,n,h,a[maxn];
signed main()
{
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	n=read();
	q=read();
	h=read();
	for(int i=1;i<=n;a[i++]=h);
	for(int i=1;i<=q;i++)
	{
		int x=read(),y=read(),z=read();
		for(int j=x;j<=y;a[j++]+=z);
		for(x=1+1;a[x]==a[x-1];x++);
		for(y=n-1;a[y]==a[y+1];y--);
		int ans=0;
		for(int j=x;j<=y;j++)
		{
			int cnt=1,begin=j;
			for(;a[j+1]==a[j];j++)
				cnt++;
			if(a[begin-1]<a[begin]&&a[j+1]<a[j]) ans+=cnt;
		}
		wln(ans);
	}
	return 0;
}
