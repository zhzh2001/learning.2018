#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 50011; 
int n, Q, H; 
int a[N]; 

int main() {
	freopen("legend.in", "r", stdin); 
	freopen("legend.out", "w", stdout); 
	n = read(); Q = read(); H = read(); 
	while(Q--) {
		int l = read(), r = read(), v = read(); 
		a[0]=a[1]+1; a[n+1]=a[n]+1; 
		For(i, l, r) a[i] += v; 
		l = 2;
		int sum = 0; 
		while(l < n) {
			while(a[l-1]>=a[l] && l<=n) ++l; 
			r = l; 
			while(a[r]==a[r+1] && r<=n) ++r;  
			if(l>1 && r<n && a[l-1]<a[l] && a[r]>a[r+1]) sum += r-l+1; 
			l = r+1; 
		}
		writeln(sum); 
	}
}


