#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define pii pair<int,int>
#define fi first
#define se second
using namespace std;
int rnd(){
	int x=0;
	For(i,1,9) x=x*10+rand()%10;
	return x;
}
int main(){
	freopen("legend.in","w",stdout);
	printf("1000000000 50000 0\n");
	For(i,1,50000){
		int l=rnd()+1,r=rnd()+1,v=rnd()%888;
		if (rnd()&1) v=-v;
		if (l>r) swap(l,r);
		printf("%d %d %d\n",l,r,v);
	}
}
/*
5 5 5
1 1 -1
4 5 -1
4 4 1
1 5 1
4 5 -1
*/
