#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define pii pair<int,int>
#define fi first
#define se second
using namespace std;
const int LBC=2333333;
char LZH[LBC],*SSS=LZH,*TTT=LZH;
inline char gc(){
	if (SSS==TTT){
		TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
		if (SSS==TTT) return EOF;
	}
	return *SSS++;
}
inline int read(){
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=0;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return f?x:-x;
}
const int M=5000000;
int tg[M],ls[M],rs[M];
int sz,rt,n,Q,ans;
void change(int &k,int l,int r,int x,int y,int v){
	if (!k) k=++sz;
	if (l==x&&r==y) return tg[k]+=v,void(0);
	int mid=(l+r)/2;
	if (y<=mid) change(ls[k],l,mid,x,y,v);
	else if (x>mid) change(rs[k],mid+1,r,x,y,v);
	else{
		change(ls[k],l,mid,x,mid,v);
		change(rs[k],mid+1,r,mid+1,y,v);
	}
}
int ask(int k,int l,int r,int p){
	if (!k||(l==r)) return tg[k];
	int mid=(l+r)/2;
	if (p<=mid) return tg[k]+ask(ls[k],l,mid,p);
	return tg[k]+ask(rs[k],mid+1,r,p);
}
int calc(int l,int r){
	if (l<=0||r>=n+1) return 0;
	int lv=ask(rt,0,n+1,l-1);
	int rv=ask(rt,0,n+1,r+1);
	int mv=ask(rt,0,n+1,l);
	if (lv<mv&&rv<mv) return r-l+1;
	return 0;
}
struct node{
	int fi,se,v;
	bool operator <(const node &a)const{
		return fi<a.fi||(fi==a.fi&&se<a.se);
	}
};
set<node> S;
void divide(int p,int v){
	set<node>::iterator it,nxt,pre;
	it=S.lower_bound((node){p,1e9,0});
	ans-=it->v; it--;
	int L=it->fi,R=it->se;
	ans-=it->v; S.erase(it);
	change(rt,0,n+1,p+1,n+1,v);
	if (L<=p){
		int val=calc(L,p); ans+=val;
		S.insert((node){L,p,val});
	}
	if (p+1<=R){
		int val=calc(p+1,R); ans+=val;
		S.insert((node){p+1,R,val});
	}
	it=S.lower_bound((node){R,1e9,0});
	node tmp=*it; S.erase(it);
	ans+=(tmp.v=calc(tmp.fi,tmp.se));
	S.insert(tmp);
	it=S.lower_bound((node){p,1e9,0});
	for (;;){
		nxt=it; nxt++;
		if (nxt==S.end()||it==S.end()||ask(rt,0,n+1,nxt->fi)!=ask(rt,0,n+1,it->fi)) break;
		ans-=it->v+nxt->v;
		L=it->fi; R=nxt->se;
		S.erase(it); S.erase(nxt);
		int val=calc(L,R); ans+=val;
		S.insert((node){L,R,val});
		it=S.lower_bound((node){p,1e9,0});
	}
	it=S.lower_bound((node){p,1e9,0});
	for (;;){
		pre=it; pre--;
		if (it==S.begin()||ask(rt,0,n+1,pre->fi)!=ask(rt,0,n+1,it->fi)) break;
		ans-=it->v+pre->v;
		L=pre->fi; R=it->se;
		S.erase(it); S.erase(pre);
		int val=calc(L,R); ans+=val;
		S.insert((node){L,R,val});
		it=S.lower_bound((node){p,1e9,0});
	}
	it=S.lower_bound((node){p,1e9,0});
	if (it!=S.begin()){
		it--;
		for (;;){
			pre=it; pre--;
			if (it==S.begin()||ask(rt,0,n+1,pre->fi)!=ask(rt,0,n+1,it->fi)) break;
			ans-=it->v+pre->v;
			L=pre->fi; R=it->se;
			S.erase(it); S.erase(pre);
			int val=calc(L,R); ans+=val;
			S.insert((node){L,R,val});
			it=S.lower_bound((node){p,1e9,0});
			if (it==S.begin()) break;
			it--;
		}
	}
}
void writeln(int x){
	if (!x){
		puts("0");
		return;
	}
	static int a[20],top;
	for (top=0;x;a[++top]=x%10,x/=10);
	for (;top;putchar(a[top--]+'0'));
	puts("");
}
int main(){
	freopen("legend.in","r",stdin);
	freopen("legend.out","w",stdout);
	n=read(); Q=read(); read();
	change(rt,0,n+1,0,0,1e9);
	change(rt,0,n+1,n+1,n+1,1e9);
	S.insert((node){1,n,0});
	S.insert((node){0,0,0});
	S.insert((node){n+1,n+1,0});
	For(i,1,Q){
		int l=read(),r=read(),v=read();
		divide(l-1,v); divide(r,-v);
		writeln(ans);
	}
}
/*
5 5 5
1 1 -1
4 5 -1
4 4 1
1 5 1
4 5 -1
*/
