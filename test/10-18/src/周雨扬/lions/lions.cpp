#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define pii pair<int,int>
#define fi first
#define se second
using namespace std;
const int LBC=2333333;
char LZH[LBC],*SSS=LZH,*TTT=LZH;
inline char gc(){
	if (SSS==TTT){
		TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
		if (SSS==TTT) return EOF;
	}
	return *SSS++;
}
inline int read(){
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=0;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return f?x:-x;
}
void write(int x){
	if (!x){
		putchar('0');
		return;
	}
	static int a[20],top;
	for (top=0;x;a[++top]=x%10,x/=10);
	for (;top;putchar(a[top--]+'0'));
}
set<pii > S;
const int N=100005;
int kill[N],safe[N];
int n,q[N],ans[N];
vector<int> vec[N];
int main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	n=read();
	if (n==1){
		puts("0");
		return 0;
	}
	For(i,1,n) S.insert(pii(read(),i));
	For(i,1,n-1){
		pii ed=*S.begin(); S.erase(S.begin());
		pii beg=*(--S.end()); S.erase(--S.end());
		vec[beg.se].push_back(ed.se);
		kill[ed.se]=i;
		S.insert(pii(beg.fi-ed.fi,beg.se));
	}
	int h=0,t=0;
	For(i,1,n) if (!kill[i]) q[++t]=i,safe[i]=1;
	while (h!=t){
		int x=q[++h];
		For(i,0,vec[x].size()-1)
			if (vec[vec[x][i]].size()){
				safe[vec[x][i]]=1;
				safe[vec[vec[x][i]].back()]=1;
				q[++t]=vec[x][i];
				q[++t]=vec[vec[x][i]].back();
				vec[vec[x][i]].pop_back();
			}
	}
	int mn=n+1;
	For(i,1,n) if (kill[i]&&safe[i])
		mn=min(mn,kill[i]);
	For(i,1,n) if (kill[i]&&kill[i]<mn) ans[++*ans]=i;
	write(*ans); puts("");
	For(i,1,*ans) write(ans[i]),putchar(' ');
}
