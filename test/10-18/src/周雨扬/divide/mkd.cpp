#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
inline int read(){
	#define gc getchar
	int x=0; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
int rnd(){
	int x=0;
	For(i,1,9) x=x*10+rand()%10;
	return x;
}
int main(){
	freopen("divide.in","w",stdout);
	printf("%d",1200000);
	For(i,1,9) printf(" %d",rand()*32768+rand());
	puts("");
	For(i,2,1200000) printf("%d\n",rnd()%(i-1)+1);
}
