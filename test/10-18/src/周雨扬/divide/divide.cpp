#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
const int LBC=2333333;
char LZH[LBC],*SSS=LZH,*TTT=LZH;
inline char gc(){
	if (SSS==TTT){
		TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
		if (SSS==TTT) return EOF;
	}
	return *SSS++;
}
inline int read(){
	int x=0; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
struct edge{
	int to,next;
}e[2333333];
const int N=1200005;
int divi[23333],cnt[N];
int G[N],sz[N],head[23333];
int n,tot,f[N],c[12];
void init(){
	For(i,1,n) if (n%i==0) divi[++*divi]=i;
	For(i,1,*divi) For(j,i,*divi)
		if (divi[j]%divi[i]==0){
			e[++tot]=(edge){j,head[i]};
			head[i]=tot;
		}
	For(i,1,*divi) For(j,1,n/divi[i])
		G[divi[i]*j]=i;
}
int main(){
	freopen("divide.in","r",stdin);
	freopen("divide.out","w",stdout);
	n=read(); init();
	For(i,1,9) c[i]=read();
	For(i,2,n) f[i]=read();
	For(T,1,10){
		printf("Case #%d:\n",T);
		For(i,1,n) sz[i]=1,cnt[i]=0;
		Rep(i,n,1) sz[f[i]]+=sz[i],cnt[G[sz[i]]]++;
		For(i,1,*divi){
			int sum=0;
			for (int j=head[i];j;j=e[j].next)
				sum+=cnt[e[j].to];
			if (sum*divi[i]==n)
				printf("%d\n",divi[i]);
		}
		if (T!=10)
			For(i,2,n)
				f[i]=((f[i]+c[T])%(i-1)+i-1)%(i-1)+1;
	}
}
