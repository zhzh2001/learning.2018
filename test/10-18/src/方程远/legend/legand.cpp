#include<bits/stdc++.h>
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
inline void write(int x){
	if(x<0)
		x=-x,putchar('-');
	if(x>9)
		write(x/10);
	putchar(x%10+48);
}
inline void writeln(int x){
	write(x);
	putchar('\n');
}
int n,q,h,x,y,z,flag,sum,ans;
int a[50001];
int main(){
	freopen("legand.in","r",stdin);
	freopen("legand.out","w",stdout);
	read(n);
	read(q);
	read(h);
	for(int i=1;i<=n;i++)
		a[i]=h;
	while(q--){
		read(x);
		read(y);
		read(z);
		for(int i=x;i<=y;i++)
			a[i]+=z;
		ans=0;
		for(int i=1;i<=n;i++){
			if(a[i]>a[i-1]){
				flag=1;
				sum=1;
			}
			if(flag){
				if(a[i]==a[i-1])
					sum++;
				if(a[i]<a[i-1]){
					ans+=sum;
					flag=0;
				}
			}
		}
		writeln(ans);
	}
	return 0;
}
