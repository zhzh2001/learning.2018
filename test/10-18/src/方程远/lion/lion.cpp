#include<bits/stdc++.h>
#define int long long
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
inline void write(int x){
	if(x<0)
		x=-x,putchar('-');
	if(x>9)
		write(x/10);
	putchar(x%10+48);
}
inline void Write(int x){
	write(x);
	putchar(' ');
}
inline void writeln(int x){
	write(x);
	putchar('\n');
}
struct Edge{
	int age,power;
	friend bool operator<(const Edge &x,const Edge &y){
		return x.power==y.power?x.age<y.age:x.power<y.power;
	}
	friend bool operator>(const Edge &x,const Edge &y){
		return x.power==y.power?x.age>y.age:x.power>y.power;
	}
}edge[100001];
bool cmp(Edge x,Edge y){
	return x>y;
}
int eat[100001],eaten[100001],killed[100001];
bool dead[100001];
int N,n,tot,ans;
signed main(){
	freopen("lions.in","r",stdin);
	freopen("lions.out","w",stdout);
	read(N);
	n=N;
	for(int i=1;i<=N;i++){
		edge[i].age=i;
		read(edge[i].power);
	}
	sort(edge+1,edge+1+N,cmp);
	while(n>=1){
		if(edge[1].power>edge[n].power){
			killed[edge[1].age]++;
			dead[edge[n].age]=1;
			tot++;
			eat[tot]=edge[1].age;
			eaten[tot]=edge[n].age;
			edge[1].power-=edge[n].power;
			n--;
			int x=1;
			while(((edge[x]<edge[x+1]))&&x<n){
				swap(edge[x].age,edge[x+1].age);
				swap(edge[x].power,edge[x+1].power);
				x++;
			}
			continue;
		}
		break;
	}
	while(tot>0){
		if(killed[eaten[tot]]){
			int x=tot;
			while(tot>0&&eat[tot]!=eaten[x]){
				killed[eat[tot]]--;
				dead[eaten[tot]]=0;
				tot--;
			}
			killed[eat[tot]]--;
			dead[eaten[tot]]=0;
		}
		tot--;
	}
	for(int i=1;i<=N;i++)
		if(dead[i])
			ans++;
	writeln(ans);
	for(int i=1;i<=N;i++)
		if(dead[i])
			Write(i);
	return 0;
}
