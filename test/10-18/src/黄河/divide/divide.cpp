#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 1200005
int n, c[N], fa[N], sz[N], b[N];
int edge, to[N << 1], pr[N << 1], hd[N];
void addedge(int u, int v){
	to[++edge] = v, pr[edge] = hd[u], hd[u] = edge;
}
void dfs(int u){
	sz[u] = 1;
	for (register int i = hd[u]; i; i = pr[i])
		dfs(to[i]), sz[u] += sz[to[i]];
}
int main(){
	freopen("divide.in", "r", stdin);
	freopen("divide.out", "w", stdout);
	n = read(), c[0] = -1;
	for (register int i = 1; i <= 9; ++i) c[i] = read();
	for (register int i = 2; i <= n; ++i) fa[i] = read();
	for (register int T = 0; T <= 9; ++T){
		printf("Case #%d:\n", T + 1);
		edge = 0, memset(hd, 0, sizeof hd);
		for (register int i = 2; i <= n; ++i)
			fa[i] = ((fa[i] + c[T]) % (i - 1) + i - 1) % (i - 1) + 1, addedge(fa[i], i);
		dfs(1);
		memset(b, 0, sizeof b);
		for (register int i = 1; i <= n; ++i) ++b[sz[i]];
		for (register int i = 1, s; i <= n; ++i)
			if (n % i == 0){
				s = 0;
				for (register int j = n / i; j; --j) s += b[i * j];
				if (s >= n / i) printf("%d\n", i);
			}
	}
}