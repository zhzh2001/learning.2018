#include <bits/stdc++.h>
#define int long long
using namespace std;
const int mod=1e9+7;
const int N=100005;
int n,m,ans=1,l[N],r[N],k[N],fac[N],v[N];
inline int read() {
	int data=0,w=1;
	char ch=0;
	while(ch!='-'&&(ch<'0'||ch>'9'))
		ch=getchar();
	if(ch=='-') {
		w=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9') {
		data=data*10+ch-'0';
		ch=getchar();
	}
	data*=w;
	return data;
}
inline int ksm(int a,int b) {
	int ans=1;
	while(b) {
		if(b&1) ans=(ans*a)%mod;
		a=(a*a)%mod;
		b>>=1;
	}
	return ans;
}
inline int inv(int x) {
	return ksm(x,mod-2);
}
inline int C(int n,int m) {
	return fac[n]*v[m]%mod*v[n-m]%mod;
}
inline void subtask1() {
	for(register int i=1; i<=m; i++) {
		int now=0;
		for(int j=l[i]; j<=r[i]; j++) {
			now+=C(j,k[i]+j-l[i]);
			now%=mod;
		}
		ans*=now;
		ans%=mod;
	}
	cout<<ans;
}
signed main() {
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read();
	m=read();
	for(register int i=1; i<=m; i++) {
		l[i]=read();
		r[i]=read();
		k[i]=read();
	}
	fac[0]=1;
	for(register int i=1; i<=max(n,m); i++) fac[i]=fac[i-1]*i%mod;
	for(register int i=0; i<=max(n,m); i++) v[i]=inv(fac[i]);
	subtask1();
}
