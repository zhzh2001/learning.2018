#include <bits/stdc++.h>
using namespace std;
int n,k,a[120000][12],he[2300][2300],head[4500000],cnt,ans=0;
bool vis[2300];
struct node {
	int next,to;
} sxd[4500000];
inline int read() {
	int data=0,w=1;
	char ch=0;
	while(ch!='-'&&(ch<'0'||ch>'9'))
		ch=getchar();
	if(ch=='-') {
		w=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9') {
		data=data*10+ch-'0';
		ch=getchar();
	}
	data*=w;
	return data;
}
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	head[u]=cnt;
}
void dfs(int at) {
	vis[at]=true;
	ans++;
	for(int i=head[at]; i; i=sxd[i].next) {
		if(!vis[sxd[i].to]) dfs(sxd[i].to);
	}
}
void subtask1() {
	for(register int i=1; i<=n; i++) {
		if(i==1) {
			cout<<"1 ";
			continue;
		}
		int tot=0;
		for(register int j=1; j<i; j++) {
			for(register int w=1; w<=k; w++) {
				if(a[j][w]>a[i][w]) {
					add(j,i);
					goto L1;
				}
			}
L1:
			;
			for(register int w=1; w<=k; w++) {
				if(a[j][w]<a[i][w]) {
					add(i,j);
					goto L2;
				}
			}
L2:
			;
		}
		for(register int j=1; j<=i; j++) {
			memset(vis,0,sizeof vis);
			ans=0;
			dfs(j);
			if(ans==i) tot++;
		}
		cout<<tot<<" ";
	}
}
void subtask2() {
	for(int i=1; i<=n; i++) {
		cout<<"1 ";
	}
	return;
}
signed main() {
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	cin>>n>>k;
	for(int i=1; i<=n; i++) for(int j=1; j<=k; j++) a[i][j]=read();
	if(k==1) subtask2();
	else subtask1();
}

