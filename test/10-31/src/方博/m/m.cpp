#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
const int mod=1e9+7;
const int N=100005;
ll n,m;
ll p[N];
ll inv[N];
inline ll qpow(ll x,ll y)
{
	ll ret=1;
	while(y){
		if(y&1)ret=ret*x%mod;
		x=x*x%mod;
		y/=2;
	}
	return ret;
}
ll C(int x,int y)
{
	return p[x]*inv[y]%mod*inv[x-y]%mod;
}
int main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read();m=read();
	p[0]=1;
	for(int i=1;i<=n+1;i++)
		p[i]=p[i-1]*i%mod;
	inv[n+1]=qpow(p[n+1],mod-2);
	for(int i=n;i;i--)
		inv[i]=inv[i+1]*(i+1)%mod;
	inv[0]=inv[1];
	ll ans=1;
	for(int i=1;i<=m;i++){
		int l,r,k;
		l=read();r=read();k=read();
		ans=ans*((C(r+1,l-k+1)-C(l,l-k+1)+mod)%mod)%mod;
	}
	writeln(ans);
}
