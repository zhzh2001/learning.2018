#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
const long long mod=2333;
const int M=2005;
const int N=45;
int n,m,q,ed,lim;
int f[N][N][35];
int num[N],tim;
int tail[M],head[N],nxt[M],tot;
int d[N][35];
int g[N][405];
inline void addto(int x,int y)
{
	nxt[++tot]=head[x];
	head[x]=tot;
	tail[tot]=y;
}
int dfs(int s,int len)
{
	int &ret=g[s][len];
	if(ret!=-1)return ret;
	ret=0;
	if(len>lim)return ret;
	if(s==ed)ret++;
	for(int i=head[s];i;i=nxt[i])
		ret=(ret+dfs(tail[i],len+1))%mod;
	return ret;
}
int main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		addto(x,y);
		if(x!=y)addto(y,x);
		f[x][y][0]=f[y][x][0]=1;
	}
	for(int l=1;l<=30;l++)
		for(int k=1;k<=n;k++)
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
					f[i][j][l]=(f[i][j][l]+f[i][k][l-1]*f[k][j][l-1]%mod)%mod;
	q=read();
	while(q--){
		int s,l,r;
		memset(d,0,sizeof(d));
		memset(g,-1,sizeof(g));
		s=read(),ed=read(),l=read(),r=read();
		lim=r-l;tim=0;
		for(int i=0;i<=30;i++)
			if(l&(1<<i))
				num[++tim]=i;
		d[s][tim]=1;
		for(int i=tim;i>=1;i--)
			for(int j=1;j<=n;j++)
				for(int k=1;k<=n;k++)
					d[k][i-1]=(d[k][i-1]+d[j][i]*f[j][k][num[i]]%mod)%mod;
		int ans=0;
		for(int i=1;i<=n;i++)
			ans=(ans+d[i][0]*dfs(i,0)%mod)%mod;
		writeln(ans);
	}
	return 0;
}
