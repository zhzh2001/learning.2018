#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
const int N=2005;
const int M=N*5;
struct xxx{
	int k[6],i;
}a[N];
int ma[6];
int vis[N];
int head[N],tail[2*M],nxt[2*M];
int tot,ans;
void addto(int x,int y)
{
	nxt[++tot]=head[x];
	head[x]=tot;
	tail[tot]=y;
}
int dfs(int k,int m)
{
//	cout<<k<<' '<<m<<endl;
	if(k<=m)ans++;
	vis[k]=true;
	for(int i=head[k];i;i=nxt[i]){
		int v=tail[i];
		if(vis[v])continue;
//		cout<<v<<'!'<<endl;
		dfs(v,m);
	}
}
bool com(xxx a,xxx b){return a.i<b.i;}
bool com1(xxx a,xxx b){return a.k[1]<b.k[1];}
bool com2(xxx a,xxx b){return a.k[2]<b.k[2];}
bool com3(xxx a,xxx b){return a.k[3]<b.k[3];}
bool com4(xxx a,xxx b){return a.k[4]<b.k[4];}
bool com5(xxx a,xxx b){return a.k[5]<b.k[5];}
int n,k;
int main()
{
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read();k=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=k;j++)
			a[i].k[j]=read(),a[i].i=i;
	if(k==1){
		for(int i=1;i<=n;i++)
			write(1),putchar(' ');
		return 0;
	}
	sort(a+1,a+n+1,com1);
	for(int i=1;i<n;i++)
		addto(a[i].i,a[i+1].i);
	if(k>1){
		sort(a+1,a+n+1,com2);
		for(int i=1;i<n;i++)
			addto(a[i].i,a[i+1].i);
	}
	if(k>2){
		sort(a+1,a+n+1,com3);
		for(int i=1;i<n;i++)
			addto(a[i].i,a[i+1].i);
	}
	if(k>3){
		sort(a+1,a+n+1,com4);
		for(int i=1;i<n;i++)
			addto(a[i].i,a[i+1].i);
	}
	if(k>4){
		sort(a+1,a+n+1,com5);
		for(int i=1;i<n;i++)
			addto(a[i].i,a[i+1].i);
	}
	sort(a+1,a+n+1,com);
	for(int i=1;i<=k;i++)
		ma[i]=1;
	write(1),putchar(' ');
	for(int i=2;i<=n;i++){
		memset(vis,0,sizeof(vis));
		for(int j=1;j<=k;j++)
			if(a[i].k[j]>a[ma[j]].k[j])ma[j]=i;
		ans=0;
		for(int j=1;j<=k;j++)
			if(!vis[ma[j]])dfs(ma[j],i);
		write(ans);putchar(' ');
	}
}
