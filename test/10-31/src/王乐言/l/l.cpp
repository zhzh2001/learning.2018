#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define ll long long
using namespace std;
const int M=1009,N=50;
const int Maxn=10000009;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,m,u,v,s,t,l,r,ans;
int hh,tt,q[Maxn],ti[Maxn];
int ver[M*3],nxt[M*3],head[N*3],tot=1;
int add(int u,int v){
	ver[++tot]=v;nxt[tot]=head[u];head[u]=tot;
}
void work(){
	s=read();t=read();l=read();r=read();
	hh=1;tt=0;ans=0;
	q[++tt]=s;ti[tt]=0;
	//cout<<q[hh];
	while(hh!=tt+1){
		int x=q[hh];
		if(ti[hh]>=l&&x==t)ans=(ans+1)%2333;
		for(int i=head[x];i;i=nxt[i]){
			//if(ver[i]==t&&ti[hh]+1<l)continue;
			if(ti[hh]+1<=r){
				tt=(tt+1)%Maxn;
				q[tt]=ver[i];
				ti[tt]=ti[hh]+1;
			}
		}
		hh=(hh+1)%Maxn;
		//cout<<x<<endl;
	}
	cout<<ans<<endl;
}
int main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		u=read();v=read();
		add(u,v);add(v,u);
	}
	int Case=read();
	while(Case--)work();
	return 0;
}

