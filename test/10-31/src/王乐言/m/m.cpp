#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define ll long long
using namespace std;
const int mod=1000000007;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,m,l,r,k;
ll now,ans=1,sj[2009][2009];
int C(int m,int k){
	return sj[m][k];
}
void init(){
	sj[0][0]=1;
	for(int i=1;i<=2004;i++){
		sj[i][0]=1;
		for(int j=1;j<=i;j++)
			sj[i][j]=(sj[i-1][j]+sj[i-1][j-1])%mod;
	}
}
ll multi(ll a,ll b){
	int base=1,ans=0;
	for(;b;b>>=1,base=(base*2)%mod)
		if(b&1)ans=(ans+a*base%mod)%mod;
	return ans;
}
int main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	init();
	//cout<<multi(3,6)<<endl;
	n=read();m=read();
	//cout<<n<<m<<endl;
	for(int i=1;i<=m;i++){
		l=read();r=read();k=read();
		now=0;
		/*for(int j=0;j<=(r-l);j++)
			now+=C(l+j,k+j);*/
		//cout<<1<<endl;
		//cout<<C(r+1,k+r-l)<<endl;
		now=((C(r+1,k+r-l)-C(l,k-1))%mod+mod)%mod;
		//cout<<now<<endl;
		ans=multi(ans,now);
		//cout<<i<<endl;
	}
	cout<<ans<<endl;
	return 0;
}

