#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define ll long long
using namespace std;
const int N=2009,inf=(1<<31)-1;
const int Maxn=N*(N-1)/2;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,k,f[200009][10],g[N][N];
int ver[Maxn],nxt[Maxn],tot=1;
int head[N],s,t,q[Maxn],d[N];
int edge[Maxn];
void add(int u,int v,int w){
	ver[++tot]=v;edge[tot]=w;nxt[tot]=head[u];head[u]=tot;
	ver[++tot]=u;edge[tot]=0;nxt[tot]=head[v];head[v]=tot;
}
bool bfs(){
	memset(d,0,sizeof(d));
	int hh=1,tt=0;
	q[++tt]=s;d[s]=1;
	while(hh!=tt+1){
		int x=q[hh];
		for(int i=head[x];i;i=nxt[i])if(edge[i]&&!d[ver[i]]){
			d[ver[i]]=d[x]+1;
			q[(tt+1)%Maxn]=ver[i];
			if(ver[i]==t)return 1;
		}
		hh=(hh+1)%Maxn;
	}
	return 0;
}
int dinic(int x,int flow){
	if(x==t)return flow;
	int res=flow,k;
	for(int i=head[x];i&&res;i=nxt[i]){
		if(edge[i]&&d[ver[i]]==d[x]+1){
			k=dinic(ver[i],min(res,edge[i]));
			if(!k)d[ver[i]]=0;
			edge[i]-=k;
			edge[i^1]+=k;
			res-=k;
		}
	}
	return flow-res;
}
int main()
{
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read();k=read();t=n+1;
	if(k==1){
		for(int i=1;i<=n;i++)printf("1 ");
		printf("\n");return 0;
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=k;j++)
			f[i][j]=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			int flag1=0,flag2=0;
			for(int t=1;t<=k;t++){
				if(f[i][t]>f[j][t])flag1=1;
				if(f[i][t]<f[j][t])flag2=1;
			}
			if(flag1)g[i][j]=1;
			if(flag2)g[i][j]=1;
		}
	}
	for(int now=1;now<=n;now++){
		int ans=0;
		for(int i=1;i<=now;i++){
			s=i;
			memset(head,0,sizeof(head));
			memset(nxt,0,sizeof(nxt));
			memset(ver,0,sizeof(ver));
			memset(edge,0,sizeof(edge));tot=1;
			for(int p=1;p<=now;p++)
				for(int k=1;k<=now;k++)
					if(g[p][k])add(p,k,1);
			for(int i=1;i<=now;i++)add(i,t,1);
			int maxflow=0,flow,tmp=0;
			while(tmp=tmp+1,bfs())
				while(flow=dinic(s,inf))maxflow+=flow;
			//cout<<maxflow<<endl;
			if(maxflow==now)ans++;
		}
		printf("%d ",ans);
	}
	printf("\n");
	return 0;
}
/* 对于每个点，分为它能战胜的和它不能战胜的。  
 * 这个点最终获胜的条件是，它不能战胜的点一定能被它
 * 能战胜的点战胜。
 * 建图，从每个点出发标记一下能战胜点。
 * 然后发现是一道网络流。。 
 * 每个点向汇点连一条容量为1的边
 * 每个点向可战胜点连一条容量为1的边
 * 以起点为源点，流量为+inf。  
 * 跑完网络流之后看看汇点是不是为now。。
 * 然而我不会写网络流了 
 */ 

