#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const int md=2333;
int n,m,q,s,t,l,r,ans;
int v[2][43],x[1003],y[1003];
int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=m;i++){
		x[i]=read();
		y[i]=read();
	}
	q=read();
	while(q-->0){
		s=read();
		t=read();
		l=read();
		r=read();
		ans=0;
		
		for(int i=1;i<=n;i++)v[0][i]=0;
		v[0][s]=1;
		for(int i=1;i<=r;i++){
			int ti=i&1,li=ti^1;
			for(int j=1;j<=n;j++){
				v[ti][j]=0;
			}
			for(int j=1;j<=m;j++){
				v[ti][x[j]]+=v[li][y[j]];
				v[ti][y[j]]+=v[li][x[j]];
			}
			for(int j=1;j<=n;j++){
				v[ti][j]%=md;
			}
			if(i>=l){
				ans+=v[ti][t];
				ans%=md;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
