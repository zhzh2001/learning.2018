#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const LL md=1000000007;
LL n,m,l,r,k,ans,f[2003][2003];
LL fac[100003],inv[100003];
void exgcd(LL a,LL b,LL&x,LL&y){
	if(b==0){
		x=1;
		y=0;
		return;
	}
	exgcd(b,a%b,y,x);
	y-=a/b*x;
}
LL ginv(LL v){
	LL x,y;
	exgcd(v,md,x,y);
	x%=md;
	if(x<0)x+=md;
	return x;
}
LL C(LL n,LL m){
	return fac[m]*inv[n]%md*inv[m-n]%md;
}
void init(){
	fac[0]=1;
	for(int i=1;i<=n;i++){
		fac[i]=fac[i-1]*i%md;
	}
	inv[n]=ginv(fac[n]);
	for(int i=n;i>0;i--){
		inv[i-1]=inv[i]*i%md;
	}
	for(int l=n;l>=1;l--){
		for(int k=0;k<=l;k++){
			f[l-k][l]=(f[l-k][l+1]+C(k,l))%md;
		}
	}
}
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read();
	m=read();
	init();
	ans=1;
	for(int i=1;i<=m;i++){
		l=read();
		r=read();
		k=read();
		ans*=f[l-k][l]-f[l-k][r+1];
		ans%=md;
	}
	if(ans<0)ans+=md;
	printf("%lld\n",ans);
	return 0;
}
