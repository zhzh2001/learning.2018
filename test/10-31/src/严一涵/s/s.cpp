#include <iostream>
#include <algorithm>
#include <cstdio>
#include <set>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
inline void write(int x){
	static int c[23],n;
	n=0;
	if(x<0){
		putchar('-');
		x=-x;
	}
	if(x==0){
		putchar('0');
		return;
	}
	while(x>0){
		c[n++]=x%10;
		x/=10;
	}
	while(n-->0){
		putchar(c[n]+'0');
	}
}
#undef dd
int n,m;
struct st{
	int k[5];
	void get(){
		for(int i=0;i<m;i++)k[i]=read();
	}
	bool operator<(st b){
		for(int i=0;i<m;i++)if(k[i]>b.k[i])return 0;
		return 1;
	}
	friend bool operator==(st a,st b){
		return !(a<b||b<a);
	}
	void gmx(st b){
		for(int i=0;i<m;i++)k[i]=max(k[i],b.k[i]);
	}
	void gmn(st b){
		for(int i=0;i<m;i++)k[i]=min(k[i],b.k[i]);
	}
};
struct vt{
	st a,b;
	int p;
	void get(){
		a.get();
		b=a;
		p=1;
	}
	friend bool operator<(vt a,vt b){
		return a.a<b.b;
	}
	friend bool operator==(vt a,vt b){
		return !(a<b||b<a);
	}
	void merge(const vt&r){
		a.gmx(r.a);
		b.gmn(r.b);
		p+=r.p;
	}
};
vt gg;
multiset<vt>sxd;
typedef multiset<vt>::iterator avt;
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++){
		gg.get();
		for(avt i=sxd.find(gg);i!=sxd.end()&&*i==gg;i++){
			gg.merge(*i);
		}
		sxd.erase(gg);
		sxd.insert(gg);
		write(sxd.rbegin()->p);
		putchar(' ');
	}
	return 0;
}
