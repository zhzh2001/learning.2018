#include<bits/stdc++.h>
#define ll long long
#define mod 1000000007
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,a[N][6];
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n) rep(j,1,m) a[i][j]=read();
	if (n<=2000){
		return 233;
	}else rep(i,1,n) printf("1 ");
}
