#include<bits/stdc++.h>
#define ll int
#define mod 2333
#define N 45
#define M 205
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,q,s[N],t[N],l,r,tot,ans[N],now=1;
struct data{ ll a[N][N]; }A;
struct query{ ll pos,id; }Q[N*M];
bool cmp(query x,query y) { return x.pos<y.pos; }
data operator *(data x,data y){
	data z; rep(i,1,n) rep(j,1,n) z.a[i][j]=0;
	rep(i,1,n) rep(k,1,n) if (x.a[i][k]) rep(j,1,n)
		z.a[i][j]=(z.a[i][j]+x.a[i][k]*y.a[k][j])%mod;
	return z;
}
data qpow(data x,ll y){
	if (!y) return x;
	data num; rep(i,1,n) rep(j,1,n) num.a[i][j]=0;
	rep(i,0,n) num.a[i][i]=1;
	for (;y;y>>=1,x=x*x) if (y&1) num=num*x;
	return num;
}
int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read(); m=read();
	rep(i,1,m){
		ll u=read(),v=read();
		A.a[u][v]=A.a[v][u]=1;
	}
	q=read();
	rep(k,1,q){
		s[k]=read(); t[k]=read();
		l=read(); r=read();
		rep(i,l,r) Q[++tot]=(query){i,k};
	}
	sort(Q+1,Q+1+tot,cmp); data tmp=A;
	rep(i,1,tot){
		if (now<Q[i].pos) tmp=tmp*qpow(A,Q[i].pos-now);
		now=Q[i].pos; (ans[Q[i].id]+=tmp.a[s[Q[i].id]][t[Q[i].id]])%=mod;
	}
	rep(i,1,q) printf("%d\n",ans[i]);
//	printf("%lld",clock());
}
/*
2 1
1 2
3
1 2 1 1
1 2 2 3
1 2 1 3

*/
