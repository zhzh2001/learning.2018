#include<bits/stdc++.h>
#define ll long long
#define mod 1000000007
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,ans=1,l,r,k,fac[N];
ll qpow(ll x,ll y){
	ll num=1;
	for (;y;y>>=1,x=x*x%mod) if (y&1) num=num*x%mod;
	return num;
}
ll C(ll n,ll m) { return fac[n]*qpow(fac[m]*fac[n-m]%mod,mod-2)%mod; }
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read(); m=read(); fac[0]=1;
	rep(i,1,n+1) fac[i]=fac[i-1]*i%mod;
	while (m--){
		l=read(); r=read(); k=read();
		(ans*=C(r+1,l-k+1)-C(l,l-k+1))%=mod;
	}
	printf("%lld",(ans%mod+mod)%mod);
}

/*
3 2
1 2 1
2 3 1

*/
