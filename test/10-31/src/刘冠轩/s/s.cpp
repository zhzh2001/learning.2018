#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=2005;
int n,k,a[N][6],mx[6],sum,head[N],cnt,dfn[N],low[N],no[N],stk[N],top,T,tot,ans;
bool f1,f2,ff[N];
struct edge{int to,nt;}e[N*N];
void add(int u,int v){
	e[++cnt]=(edge){v,head[u]};
	head[u]=cnt;
}
void dfs(int u){
	stk[++top]=u;
	dfn[u]=low[u]=++T;
	for(int i=head[u];i;i=e[i].nt){
		if(!dfn[e[i].to]){
			dfs(e[i].to);
			low[u]=min(low[u],low[e[i].to]);
		}else if(!no[e[i].to])low[u]=min(low[u],dfn[e[i].to]);
	}
	if(dfn[u]==low[u]){
		tot++;no[u]=tot;
		while(stk[top]!=u){
			no[stk[top]]=tot;
			top--;
		}
	}
}
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read();k=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=k;j++)a[i][j]=read();
	cout<<1<<' ';
	for(int i=1;i<=k;i++)mx[i]=a[1][i];
	for(int i=2;i<=n;i++){
		sum=0;
		for(int j=1;j<=k;j++)
			if(a[i][j]>=mx[j]){
				mx[j]=a[i][j];
				sum++;
			}
		if(sum==k){cout<<1<<' ';continue;}
		for(int j=1;j<i;j++){
			f1=f2=0;
			for(int l=1;l<=k;l++)
				if(a[j][l]<a[i][l]&&!f1){
					f1=1;add(i,j);
				}else if(a[j][l]>a[i][l]&&!f2){
					f2=1;add(j,i);
				}
		}
		memset(dfn,0,sizeof(dfn));
		memset(low,0,sizeof(low));
		memset(no,0,sizeof(no));
		memset(ff,0,sizeof(ff));
		top=T=tot=0;
		for(int j=1;j<=i;j++)if(!dfn[i])dfs(i);
		for(int j=1;j<=i;j++)
			for(int o=head[j];o;o=e[o].nt)
				if(no[j]!=no[e[o].to])ff[e[o].to]=1;
		ans=0;
		for(int j=1;j<=i;j++)ans+=!ff[j];
		cout<<ans<<' ';
	}
	return 0;
}
