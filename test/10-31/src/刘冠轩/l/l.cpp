#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int P=2333;
int ans[2][44],n,m,Q,l,r,s,t,x,y,cnt,head[44],Ans;
struct edge{int to,nt;}e[10000];
void add(int u,int v){
	e[++cnt]=(edge){v,head[u]};
	head[u]=cnt;
}
int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		x=read();y=read();
		add(x,y);add(y,x);
	}
	Q=read();
	while(Q--){
		s=read();t=read();l=read();r=read();
		memset(ans,0,sizeof(ans));
		ans[0][s]=1;Ans=0;
		for(int T=1;T<=r;T++)
			for(int j=1;j<=n;j++){
				ans[T&1][j]=0;
				for(int i=head[j];i;i=e[i].nt)
					ans[T&1][j]+=ans[(T&1)^1][e[i].to];
				ans[T&1][j]%=P;
				if(l<=T&&j==t)Ans=(Ans+ans[T&1][j])%P;
			}
		cout<<Ans<<'\n';
	}
	return 0;
}
