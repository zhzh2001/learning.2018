#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int P=1e9+7;
const int N=100005;
int n,m,l,r,k,ans=1,sum,jc[N],ny[N];
int ksm(int x,int y){
	int ans=1;
	for(;y;y>>=1,x=x*x%P)if(y&1)ans=(ans*x)%P;
	return ans;
}
signed main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read();m=read();jc[0]=1;ny[0]=1;
	for(register int i=1;i<=n;i++)jc[i]=jc[i-1]*i%P;
	ny[n]=ksm(jc[n],P-2);
	for(register int i=n-1;i;i--)ny[i]=ny[i+1]*(i+1)%P;
	while(m--){
		sum=0;
		l=read();r=read();k=read();
		for(register int i=l;i<=r;i++)sum+=jc[i]*ny[i-l+k]%P;
		sum=sum%P*ny[l-k]%P;
		ans=ans*sum%P;
	}
	cout<<ans;
	return 0;
}
