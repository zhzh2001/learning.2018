#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
void writeln(GG x) { write(x); putchar('\n'); }

const int Mod = 1e9+7, N = 1e5+11; 
int n, m;
int fact[N];
int b[60], tot; 
LL ans;  

inline int ksm(int x, int y) {
	int res = 1; 
	Dow(i, tot, 1) {
		res = 1ll*res*res %Mod; 
		if(b[i]) res = 1ll*res*x %Mod; 
	}
	return res; 
}

inline int C(int n, int m) {
	return 1ll*fact[n]*ksm(fact[m], Mod-2) %Mod*ksm(fact[n-m], Mod-2) %Mod; 
} 

int main() {
	freopen("m.in", "r", stdin); 
	freopen("m.out", "w", stdout); 
	n = read(); m = read(); int y = Mod-2; 
	while(y) {
		b[++tot] = y%2; y/=2; 
	}
	ans = 1; fact[0] = 1; 
	For(i, 1, 1e5) fact[i] = 1ll*fact[i-1]*i % Mod; 
	For(i, 1, m) {
		int l = read(), r = read(), k = read(); 
		k = l-k; 
		ans = ans*( 1ll*C(r+1, k+1)+Mod-C(l, k+1) ) %Mod; 
	}
	writeln(ans);  
}


/*



3 2
1 2 1
2 3 1


*/


