#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
void writeln(GG x) { write(x); putchar('\n'); }

const int N = 50, INF = 1e9; 
int n, m, s, t, l, r, len, ans, Q; 
int mp[N][N], f[N][N]; 

void dfs(int u, int len) {
	if(len+f[u][t] > r) return; 
	if(u == t && len>=l && len<=r) ++ans; 
	For(v, 1, n) {
		if(mp[u][v]) {
			dfs(v, len+1); 
		}
	}
}

int main() {
	freopen("l.in", "r", stdin); 
	freopen("l.out", "w", stdout);
	n = read(); m = read(); 
	For(i, 1, n) For(j, 1, n) f[i][j] = INF; 
	For(i, 1, m) {
		int x = read(), y = read(); 
		mp[x][y] = 1; mp[y][x] = 1; 
		f[x][y] = 1; f[y][x] = 1;
	} 
	For(k, 1, n)
		For(i, 1, n) 
			For(j, 1, n) 
				f[i][j] = min(f[i][j], f[i][k]+f[k][j]); 
	For(i, 1, n) f[i][i] = 0; // 2
	Q = read();  
	while(Q--) {
		s = read(); t = read(); l = read(); r = read(); 
		ans = 0; 
		dfs(s, 0); 
		writeln(ans); 
	}
}












