#include <bits/stdc++.h>
#define LL long long
#define GG long long 
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
void writeln(GG x) { write(x); putchar('\n'); }

const int N = 100011; 
struct node{
	int val[6]; 
}a[N];
int n, K;
int tmp[3000], tot, ying[3011][3011], shu[3011][3011], num[3011], visit[N];  

inline bool calc(int x, int y) {
	For(i, 1, K) 
		if(a[x].val[i] > a[y].val[i]) return 1; 
	return 0; 
}

void dfs_2(int u) {
	visit[u] = 1; 
	For(v, 1, n) {
		if(!visit[v] && shu[u][v]) {
			++num[v]; 
			dfs_2(v); 
		} 
	}
}


void dfs_1(int u, int wh) {
	visit[u] = 1;
	For(v, 1, n) {
		if(!visit[v] && ying[u][v]) {
			++num[ wh ]; 
			dfs_1(v, wh); 
		} 
	}
}

inline void work_1() {
	tmp[++tot] = 1; 
	write(tot); putchar(' '); 

	For(i, 2, n) {
		For(j, 1, i-1) 
			if(calc(i, j)) ying[i][j] = ying[j][i] = 1; 
		For(j, 1, i-1) 
			if(calc(j, i)) shu[i][j] = shu[j][i] = 1;
		For(j, 1, n) visit[j] = 0; 
		dfs_1(i, i); 
		For(j, 1, n) visit[j] = 0;
		dfs_2(i); 
		int sum = 0; 
		For(j, 1, n) if(num[j]>=i-1) ++sum; 
		write(sum); putchar(' '); 
	}
}

int main() {
	freopen("s.in", "r", stdin); 
	freopen("s.out", "w", stdout); 
	n = read(); K = read(); 
	For(i, 1, n) 
		For(j, 1, K) 
			a[i].val[j] = read(); 
	if(n == 1) {
		For(i, 1, n) putchar('1'), putchar(' '); 
		return 0; 
	}
	if(n<=3000) work_1(); 
}


/*

3 2
1 2
2 1
3 3



*/






