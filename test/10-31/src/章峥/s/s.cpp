#include<fstream>
#include<algorithm>
#include<set>
using namespace std;
ifstream fin("s.in");
ofstream fout("s.out");
const int N=100005,K=5;
int n,k;
struct node
{
	int sz,mn[K],mx[K];
	bool operator<(const node& rhs)const
	{
		for(int i=0;i<k;i++)
			if(mx[i]>rhs.mn[i])
				return false;
		return true;
	}
	node operator+(const node& rhs)const
	{
		node ret;
		ret.sz=sz+rhs.sz;
		for(int i=0;i<k;i++)
		{
			ret.mn[i]=min(mn[i],rhs.mn[i]);
			ret.mx[i]=max(mx[i],rhs.mx[i]);
		}
		return ret;
	}
	node& operator+=(const node& rhs)
	{
		return *this=*this+rhs;
	}
};
typedef set<node>::iterator iter;
int main()
{
	fin>>n>>k;
	set<node> S;
	for(int i=1;i<=n;i++)
	{
		node now;
		now.sz=1;
		for(int j=0;j<k;j++)
		{
			fin>>now.mn[j];
			now.mx[j]=now.mn[j];
		}
		pair<iter,iter> its=S.equal_range(now);
		for(iter it=its.first;it!=its.second;++it)
			now+=*it;
		S.erase(its.first,its.second);
		S.insert(now);
		fout<<S.rbegin()->sz<<' ';
	}
	return 0;
}
