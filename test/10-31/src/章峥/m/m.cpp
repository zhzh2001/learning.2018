#include<fstream>
using namespace std;
ifstream fin("m.in");
ofstream fout("m.out");
const int N=100005,MOD=1e9+7;
int fact[N],inv[N];
long long qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}while(b/=2);
	return ans;
}
inline long long c(int n,int m)
{
	return 1ll*fact[n]*inv[n-m]%MOD*inv[m]%MOD;
}
int main()
{
	int n,m;
	fin>>n>>m;
	n++;
	fact[0]=1;
	for(int i=1;i<=n;i++)
		fact[i]=1ll*fact[i-1]*i%MOD;
	inv[n]=qpow(fact[n],MOD-2);
	for(int i=n-1;i>=0;i--)
		inv[i]=1ll*inv[i+1]*(i+1)%MOD;
	int ans=1;
	while(m--)
	{
		int l,r,k;
		fin>>l>>r>>k;
		ans=1ll*ans*(c(r+1,k+r-l)-c(l,k-1)+MOD)%MOD;
	}
	fout<<ans<<endl;
	return 0;
}
