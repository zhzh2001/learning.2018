#pragma GCC optimize 2
#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("l.in");
ofstream fout("l.out");
const int N=42,MOD=2333;
int n,m;
struct matrix
{
	int mat[N][N];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix res;
		for(int i=0;i<=n+1;i++)
			for(int j=0;j<=n+1;j++)
				for(int k=0;k<=n+1;k++)
					res.mat[i][j]+=mat[i][k]*rhs.mat[k][j];
		for(int i=0;i<=n+1;i++)
			for(int j=0;j<=n+1;j++)
				res.mat[i][j]%=MOD;
		return res;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
};
matrix I()
{
	matrix res;
	for(int i=1;i<=n+1;i++)
		res.mat[i][i]=1;
	return res;
}
matrix qpow(matrix a,int b)
{
	matrix ans=I();
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}while(b/=2);
	return ans;
}
int main()
{
	fin>>n>>m;
	matrix trans;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		trans.mat[u][v]=trans.mat[v][u]=1;
	}
	trans.mat[n+1][n+1]=1;
	int q;
	fin>>q;
	while(q--)
	{
		int s,t,l,r;
		fin>>s>>t>>l>>r;
		matrix init;
		init.mat[s][0]=1;
		trans.mat[n+1][t]=1;
		fout<<((qpow(trans,r+1)*init).mat[n+1][0]-(qpow(trans,l)*init).mat[n+1][0]+MOD)%MOD<<endl;
		trans.mat[n+1][t]=0;
	}
	return 0;
}
