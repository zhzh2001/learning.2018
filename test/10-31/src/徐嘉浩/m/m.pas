var i,j:longint;
    p,l,r,k,ans,n,m:int64;
    c,f:array[-1..2000,-1..2000]of int64;
begin
 assign(input,'m.in');
 assign(output,'m.out');
 reset(input);
 rewrite(output);
 p:=1000000007;
 read(n,m);
 for i:=0 to n do c[0,i]:=1;
 for i:=1 to n do
  for j:=i to n do c[i,j]:=(c[i,j-1]+c[i-1,j-1])mod p;
 for i:=0 to n do
  for j:=i to n do f[i,j]:=(f[i,j-1]+c[i,j])mod p;
 ans:=1;
 for i:=1 to m do
 begin
  read(l,r,k);
  ans:=ans*(((f[l-k,r]-f[l-k,l-1])mod p+p)mod p)mod p;
 end;
 writeln(ans);
 close(input);
 close(output);
end.