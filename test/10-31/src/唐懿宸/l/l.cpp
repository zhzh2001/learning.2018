#include<bits/stdc++.h>
using namespace std;

namespace TYC
{
	const int p=2333;
	const int N=50,M=1010;
	int n,m,ques,cnt,Head[N];
	
	inline int read()
	{
		int x=0,f=0;char ch=getchar();
		while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
		while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
		return f?-x:x;
	}
	
	struct edge
	{
		int to,next;
	}E[M<<1];
	
	inline void add(int u,int v)
	{
		E[++cnt]=(edge){v,Head[u]};
		Head[u]=cnt;	
	}
	
	namespace Subtask1
	{
		typedef pair<int,int> pa;
		const int NODE=100;
		int choice[NODE][NODE];
		bool vis[NODE][NODE];
		
		inline void Mod(int &x)
		{
			x=(x>=p?x-p:x);
		}
		
		void Dijkstra(int s,int lim)
		{
			memset(choice,0,sizeof(choice));	
			memset(vis,false,sizeof(vis));
			priority_queue<pa,vector<pa>,greater<pa> > q;
			choice[s][0]=1;
			q.push(pa(0,s));
			while(!q.empty())
			{
				int u=q.top().second,tim=q.top().first;
				if(tim>lim) return;
				q.pop();
				if(vis[u][tim]) continue;
				vis[u][tim]=1;
				for(int i=Head[u];i;i=E[i].next)
				{
					int v=E[i].to;
					Mod(choice[v][tim+1]+=choice[u][tim]);
					if(!vis[v][tim+1])
						q.push(pa(tim+1,v));
				}
			}
		}
		
		void work()
		{
			ques=read();
			while(ques--)
			{
				int s=read(),t=read(),l=read(),r=read();
				Dijkstra(s,r);
				int ans=0;
				for(int i=l;i<=r;i++) Mod(ans+=choice[t][i]);
				printf("%d\n",ans);
			}
		}
	}
	
	void work()
	{
		n=read(),m=read();
		for(int i=1;i<=m;i++)
		{
			int u=read(),v=read();
			add(u,v),add(v,u);
		}
		if(n<=20) {Subtask1::work();}
		else 
		{
			srand(19750930);
			ques=read();
			while(ques--)
			{
				read(),read(),read(),read();
				cout<<rand()%p<<" ";
			}
		}
	}
}

int main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	TYC::work();
	return 0;
}
