#include<bits/stdc++.h>
using namespace std;

namespace TYC
{
	typedef long long ll;
	const ll p=1e9+7;
	const int N=2010;	
	int n,m;
	ll C[N][N],sum[N][N];
	
	inline int read()
	{
		int x=0,f=0;char ch=getchar();
		while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
		while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
		return f?-x:x;
	}
	
	inline ll Mod(ll x)
	{
		return x>=p?x-p:x;
	}
	
	inline void init()
	{
		C[0][0]=1;
		for(int i=1;i<=n;i++)
		{
			C[i][0]=1;
			for(int j=1;j<=n;j++)
				C[i][j]=Mod(C[i-1][j-1]+C[i-1][j]);
		}
		for(int i=0;i<=n;i++)
			for(int j=1;j<=n;j++)
				sum[i][j]=Mod(sum[i][j-1]+C[j][i]);
	}
	
	void work()
	{
		n=read(),m=read();
		init();
		ll ans=1;
		for(int kase=1;kase<=m;kase++)
		{
			int l=read(),r=read(),k=read();
			ans=(Mod(sum[l-k][r]-sum[l-k][l-1]+p)*ans)%p;
		}
		printf("%lld\n",ans%p);
	}
}

int main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	TYC::work();
	return 0;
}
