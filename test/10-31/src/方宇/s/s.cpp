#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 100010
using namespace std;
LL n,m,i,j,k,l,r,ans;
LL a[N][10],pre[N],boo[N];
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
bool check(LL l,LL r)
{
	rep(i,1,m)
	  if(a[l][i]>a[r][i]) return 0;
	return 1;
}
int main()
{
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read(); m=read();
	if(m==1)
	{
		rep(i,1,n) printf("1\n");
		return 0;
	}
	rep(i,1,n)
	  rep(j,1,m) a[i][j]=read();
	rep(i,1,n)
	{
	  rep(j,1,i-1)
	  {
	  	if(check(i,j))
	  	  {
	  	  	boo[i]=1;
	  	  	break;
	  	  }
	  }
	  if(boo[i]) continue;
	  rep(j,i+1,n)
	  {
	  	if(check(i,j))
		  {
		  	pre[j]++;
		  	break;
		  } 
	  }
	}
	ans=0;
	rep(i,1,n)
	{
		ans=ans+1-boo[i]-pre[i];
		printf("%lld\n",ans);
	}
	return 0;
}
