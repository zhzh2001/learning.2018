#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy (LL)(1e9+7)
#define N 2010
using namespace std;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
LL ans,C[N][N],D[N][N];
int n,m,l,r,k;
inline void pre()
{
	C[0][0]=1;
	C[1][0]=1;C[1][1]=1;
	rep(i,1,n) C[i][0]=1;
	rep(i,2,n)
	{
		rep(j,1,i-1)
		C[i][j]=(C[i-1][j-1]+C[i-1][j])%fy;
		C[i][i]=1;
	}
}
int main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read(),m=read();
	ans=1;
	pre();
	rep(i,0,n)
	{
		rep(j,i+1,n)
		D[j][i]=(D[j-1][i]+C[j][j-i])%fy;
	}
	rep(i,1,m)
	{
		l=read(),r=read(),k=read();
		LL sum=0;
		if (l>=k)sum=(D[r][l-k]-D[l-1][l-k]+fy)%fy;
		if (k==0) sum=(sum+1)%fy;
		ans=(ans*sum)%fy;
	}
	printf("%lld\n",ans);
}

