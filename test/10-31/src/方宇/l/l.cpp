#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 1010
using namespace std;
LL a[N],b[N],c[N];
LL n,m,i,j,k,l,r,x,y,ans;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
void add(LL k,LL l,LL r){a[k]=r; b[k]=c[l]; c[l]=k;}
void DFS(LL k,LL s)
{
	if(s>r) return;
	if(s>=l&&k==y) ans=(ans+1)%2333;
	for(LL j=c[k];j;j=b[j]) DFS(a[j],s+1);
}
int main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read(); m=read();
	rep(i,1,m)
	{
		l=read(); r=read();
		add(i*2-1,l,r); add(i*2,r,l);
	}
	m=read();
	rep(i,1,m)
	{
		ans=0;
		x=read(); y=read(); l=read(); r=read();
		DFS(x,0);
		printf("%lld\n",ans);
	}
	return 0;
}
