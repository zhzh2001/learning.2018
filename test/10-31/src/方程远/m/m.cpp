#include<bits/stdc++.h>
#define int long long
using namespace std;
const int P=10000007;
int n,m,x,y,z,sum,num,ans=1;
int C(int x,int y){
	int num=1;
	if(x==y)
		return num;
	for(int i=x;i>=y;i--)
		num=(num*i)%P;
	for(int i=1;i<=(x-y);i++)
		num=(num/i)%P;
	return num;
}
signed main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	while(m--){
		scanf("%lld%lld%lld",&x,&y,&z);
		num=C(x,z);
		sum+=num;
		for(int i=x+1;i<=y;i++){
			num=(num*i/(i-x+z))%P;
			sum=(sum+num)%P;
		}
		ans=(ans*sum)%P;
	}
	printf("%lld",ans);
	return 0;
}
