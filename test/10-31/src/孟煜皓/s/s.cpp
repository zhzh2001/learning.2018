#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register int f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
int n, k, b[N], a[N][5], ans;
int check(int x, int y){
	for (register int i = 0; i < k; ++i)
		if (a[x][i] > a[y][i]) return 1;
	return 0;
}
int main(){
	freopen("s.in", "r", stdin);
	freopen("s.out", "w", stdout);
	n = read(), k = read();
	if (k == 1){
		for (register int i = 1; i <= n; ++i) printf("1 ");
		return 0;
	}
	for (register int i = 1; i <= n; ++i)
		for (register int j = 0; j < k; ++j)
			a[i][j] = read();
	ans = 0;
	for (register int i = 1; i <= n; ++i){
		int flag = 0;
		for (register int j = 1; j < i; ++j)
			if (check(j, i)){ flag = 1; break; }
		if (!flag){
			for (register int j = 1; j < i; ++j) b[j] = 0;
			b[i] = 1; ans = 1; printf("%d ", ans); continue;
		}
		for (register int j = 1; j < i; ++j)
			if (b[j] && check(i, j)) { b[i] = 1; ++ans; break; }
		printf("%d ", ans);
	}
}
