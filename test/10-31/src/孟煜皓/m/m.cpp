#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register int f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
#define P 1000000007
int n, m, fac[N], inv[N], ans;
int qpow(int a, int b){
	int s = 1;
	for (; b; b >>= 1, a = 1ll * a * a % P) if (b & 1) s = 1ll * s * a % P;
	return s;
}
void pre(int n){
	fac[0] = 1;
	for (register int i = 1; i <= n; ++i) fac[i] = 1ll * fac[i - 1] * i % P;
	inv[n] = qpow(fac[n], P - 2);
	for (register int i = n; i; --i) inv[i - 1] = 1ll * inv[i] * i % P;
}
int C(int n, int m){
	return m > n ? 0 : 1ll * fac[n] * inv[m] % P * inv[n - m] % P;
}
int main(){
	freopen("m.in", "r", stdin);
	freopen("m.out", "w", stdout);
	n = read(), m = read();
	ans = 1, pre(n + 1);
	for (register int i = 1; i <= m; ++i){
		int l = read(), r = read(), k = read();
		ans = 1ll * ans * (C(r + 1, l - k + 1) - C(l, l - k + 1) + P) % P;
	}
	printf("%d", ans);
}
