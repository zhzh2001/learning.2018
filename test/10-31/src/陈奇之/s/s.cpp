#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
namespace IO{
	const int L=1<<15;char ibuf[L|1],*iS=ibuf,*iT=ibuf,obuf[L|1],*oS=obuf,*oT=obuf+L,c,st[66];int tp=0,f;
	inline char gc(){if(iS==iT) iT=(iS=ibuf)+fread(ibuf,sizeof(char),L,stdin);return (*iS++);}
	inline void flush(){fwrite(obuf,sizeof(char),oS-obuf,stdout);oS = obuf;}
	inline void pc(char c){*oS++=c;if(oS==oT) flush();}
	inline ll read(){ll x=0;f=1;for(c=gc();!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
	struct IOflusher{~IOflusher(){flush();}}_ioflusher_;
}using IO::read;using IO::write;using IO::writeln;using IO::wri;using IO::gc;using IO::pc;
const int maxn = 1e5+233;
int a[maxn][6],f[maxn],ans[maxn];
int n,k,cmp;
struct node{
	int up[6],down[6],size;
	bool operator < (const node &w) const{
		return up[cmp] < w.up[cmp];
	}
};
set<node> s;
inline node merge(node a,node b){
	node tmp = a;
	rep(i,0,k){
		tmp.up[i] = max(tmp.up[i],b.up[i]);
		tmp.down[i] = min(tmp.down[i],b.down[i]);
	}tmp.size+=b.size;
	return tmp;
}
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n = read(),k = read();
	Rep(i,1,n)
		rep(j,0,k)
			a[i][j] = read();
	node now,need;
	rep(j,0,k) now.up[j]=now.down[j]=a[1][j];now.size=1;
	s.insert(now);
	wri(1);
	Rep(i,2,n){
		int mx,mn;
		rep(j,0,k) now.up[j]=now.down[j]=a[i][j];now.size=1;
		mx=mn=a[i][0];
		rep(j,0,k){
			cmp = j;
			need.up[j] = a[i][j];
			set<node>::iterator iter = s.lower_bound(need);
			if(iter==s.end()){
				assert(iter != s.begin());
				iter--;
				mx=max(mx,iter->up[0]);
				mn=min(mn,iter->up[0]+1);
				continue;
			}
			node tmp = *iter;
			if(tmp.down[j] <= a[i][j] && a[i][j] <= tmp.up[j]){
				mx = max(mx,tmp.up[0]);
				mn = min(mn,tmp.down[0]);
			} else{
				mn=min(mn,tmp.down[0]);
				mx=max(mx,tmp.down[0]-1);
			}
		}
		cmp = 0;
		need.up[0] = mn;	
		set<node>::iterator iter = s.lower_bound(need);
		for(;;){
			if(iter==s.end()) break;
			node tmp = *iter;
			if(tmp.up[0] <= mx){
				now = merge(now,tmp);
				s.erase(iter++);
			} else break;
		}
		s.insert(now);
		wri(s.rbegin() -> size);
	}
	return 0;
}
/*
考虑两个点

如果存在一个大，一个小，那么两个人的答案是相同的——连一条边

A全部都比B大,那么B一定是输的,A一定是胜利的

考虑整个图，是一条链

二分到一个位置，是否可以相等？
*/

/*
S[i]中存在的表示i中含有的。
在所有S中存在的就是会被i打败的。

在之前那些没有被打败的集合中

找到一个S的交中存在的元素，并打败他，然后把打败的集合删掉。
 */
