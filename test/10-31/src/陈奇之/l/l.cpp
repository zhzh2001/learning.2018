#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
namespace IO{
	const int L=1<<15;char ibuf[L|1],*iS=ibuf,*iT=ibuf,obuf[L|1],*oS=obuf,*oT=obuf+L,c,st[66];int tp=0,f;
	inline char gc(){if(iS==iT) iT=(iS=ibuf)+fread(ibuf,sizeof(char),L,stdin);return (*iS++);}
	inline void flush(){fwrite(obuf,sizeof(char),oS-obuf,stdout);oS = obuf;}
	inline void pc(char c){*oS++=c;if(oS==oT) flush();}
	inline ll read(){ll x=0;f=1;for(c=gc();!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
	struct IOflusher{~IOflusher(){flush();}}_ioflusher_;
}using IO::read;using IO::write;using IO::writeln;using IO::wri;using IO::gc;using IO::pc;
const int mod = 2333;
const int mod2 = mod*mod;
const int maxn = 42;
int n,m;
int g[maxn],f[34][maxn][maxn];
inline void add(int &x,int v){
	x+=v;
	if(x>=mod2) x-=mod2;
}
int tmp2[maxn][maxn];
inline void times1(int res[maxn][maxn],int a[maxn][maxn],int b[maxn][maxn]){
	Rep(i,1,n){
		Rep(j,1,n){
			tmp2[i][j]=0;
			Rep(k,1,n)
				add(tmp2[i][j],a[i][k]*b[k][j]); 
		}
	}
	Rep(i,1,n)Rep(j,1,n)res[i][j]=tmp2[i][j]%mod;
}
int tmp[maxn];
inline void times2(int f[maxn],int a[maxn],int b[maxn][maxn]){
	Rep(i,1,n){
		tmp[i] = 0;
		Rep(j,1,n) add(tmp[i],a[j]*b[j][i]);
	}
	Rep(i,1,n) f[i]=tmp[i]%mod;
}
int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n = read(),m = read();
	mem(f[0],0);
	Rep(i,1,m){
		int a = read(),b = read();
		if(a!=b) f[0][a][b]++,f[0][b][a]++; else
				 f[0][a][a]++;
	}
	Rep(i,1,30) times1(f[i],f[i-1],f[i-1]);
	int q = read();
	for(int i=1;i<=q;++i){
		int s = read(),t = read(),l = read(),r = read();
		int ans = 0;
		Rep(i,1,n) g[i] = 0;g[s] = 1;
		for(int t=0;t<=30;++t) if((l-1) >> t & 1) times2(g,g,f[t]);
		Rep(w,l,r){
			times2(g,g,f[0]);
			ans=(ans+g[t])%mod;
		}
		writeln(ans);
	}
	return 0;
}

/*
预处理出log个矩阵

每次向量乘矩阵即可 */
