#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
namespace IO{
	const int L=1<<15;char ibuf[L|1],*iS=ibuf,*iT=ibuf,obuf[L|1],*oS=obuf,*oT=obuf+L,c,st[66];int tp=0,f;
	inline char gc(){if(iS==iT) iT=(iS=ibuf)+fread(ibuf,sizeof(char),L,stdin);return (*iS++);}
	inline void flush(){fwrite(obuf,sizeof(char),oS-obuf,stdout);oS = obuf;}
	inline void pc(char c){*oS++=c;if(oS==oT) flush();}
	inline ll read(){ll x=0;f=1;for(c=gc();!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
	struct IOflusher{~IOflusher(){flush();}}_ioflusher_;
}using IO::read;using IO::write;using IO::writeln;using IO::wri;using IO::gc;using IO::pc;
const int mod = 1e9+7;
const int maxn = 2e5+233;
int fac[maxn],inv[maxn];
inline int qpow(int a,int b){
	int ans = 1;
	for(;b;b>>=1,a=1ll*a*a%mod)
		if(b&1) ans=1ll*ans*a%mod;
	return ans;
}
void init(int n){
	fac[0] = 1;
	Rep(i,1,n) fac[i] = 1ll * fac[i-1] * i % mod;
	inv[n] = qpow(fac[n],mod-2);
	Dep(i,n-1,0) inv[i] = 1ll * inv[i+1] * (i+1) % mod;
}
inline int C(int n,int m){
	if(n<m) return 0;
	return 1ll * fac[n] * inv[m] % mod * inv[n-m] % mod;
}
int ans,n,m;
int calc(int l,int r,int k){
	return (C(r+1,l-k+1) - C(l,l-k+1) + mod) % mod;
}
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	init(200000);
	n = read(),m = read();
	ans =1;
	while(m--){
		int l = read(),r = read(),k = read();
		ans = 1ll * ans * calc(l,r,k) % mod;
	}
	writeln(ans);
	return 0;
}
