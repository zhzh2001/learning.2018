#include<bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0, f=0; char ch=getchar();
  for(;ch<'0'||ch>'9';ch=getchar()) if (ch='-') f=1;
  for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const int N = 200005, mod = 1e9+7;
int n, m;
long long Pow[N], ans;
inline int ksm(int x, int y) {
  int sum = 1;
  while (y) {
    if (y&1) sum = 1ll*sum*x%mod;
    y >>= 1;
    x = 1ll*x*x%mod;
  }
  return sum;
}
inline int C(int n, int m) {
  if (n == 0) return 0;
  return 1ll*Pow[n]*ksm(1ll*Pow[m]*Pow[n-m]%mod, mod-2)%mod;
}
inline int solve(int l, int r, int k) {
//  cout << r+k-1 << " "<< k << endl;
//  cout << l+k-2 <<" "<< k << endl;
//  cout << C (r+k-1, k) << " "<< C(l+k-2, k) << endl;
  return (C(r+1, k) - C(l, k) + mod)%mod;
}
signed main() {
  freopen("m.in","r",stdin);
  freopen("m.out","w",stdout);
  n = read(); m = read(); ans = 1;
  Pow[0] = 1;
  for (int i = 1; i < N; i++) Pow[i] = 1ll*Pow[i-1]*i%mod;
  for (int i = 1; i <= m; i++) {
    int l = read(), r = read(), k = l-read()+1;
    ans = (1ll*ans*solve(l, r, k))%mod;
//    printf("%d\n", ans);
  }
  printf("%lld\n", ans);
  return 0;
}