#include<bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0, f=0; char ch=getchar();
  for(;ch<'0'||ch>'9';ch=getchar()) if (ch='-') f=1;
  for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const int N = 45, M = 205, mod = 2333;
int n, m, q;
struct Matrix {
  int a[N][N];
  Matrix(){memset(a, 0, sizeof a);}
  Matrix cheng(Matrix a, Matrix b) {
    Matrix c;
    for (int i = 1; i <= n; i++)
      for (int k = 1; k <= n; k++)
        if (a.a[i][k])
          for (int j = 1; j <= n; j++)
            c.a[i][j] = (c.a[i][j]+1ll*a.a[i][k]*b.a[k][j]%mod)%mod;
    return c;
  }
  Matrix Cheng(Matrix a, int y) {
    Matrix c;
    for (int i = 0; i <= n; i++)
      c.a[i][i] = 1;
    while (y) {
      if (y&1) c = cheng(c, a);
      y >>= 1;
      a = cheng(a, a);
    }
    return c;
  }
}A, B, C;
int S[N], T[N];
struct que {
  int x, id;
  bool operator <(const que & a)const {
    return x < a.x;
  }
}qu[N*M];
inline bool cmp(que a, que b) {
  return a.id < b.id;
}
int ans[N];
int main() {
  freopen("l.in","r",stdin);
  freopen("l.out","w",stdout);
  n = read(); m = read();
  for (int i = 1; i <= m; i++) {
    int u = read(), v = read();
    A.a[u][v] = 1;
    A.a[v][u] = 1;
  }
  q = read();
  int tot = 0;
  for (int i = 1; i <= q; i++) {
    S[i] = read(); T[i] = read();
    int l = read(), r = read();
    for (int j = l; j <= r; j++)
      qu[++tot] = (que){j-1, i};
  }
  sort(qu+1, qu+1+tot);
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= n; j++) 
      B.a[i][j] = A.a[i][j];
  for (int i = 1; i <= tot; i++) {
/*    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= n; j++)
        cout << B.a[i][j] <<" ";
      cout << "\n";
    }*/
    if (qu[i].x > qu[i-1].x) B = C.cheng(B, C.Cheng(A, qu[i].x - qu[i-1].x));
//    cout << S[qu[i].id] <<" "<< T[qu[i].id] << " "<<B.a[S[qu[i].id]][T[qu[i].id]]<<endl;
//    qu[i].ans = (qu[i].ans+B.a[S[qu[i].id]][T[qu[i].id]])%mod;
    ans[qu[i].id] = (ans[qu[i].id]+B.a[S[qu[i].id]][T[qu[i].id]])%mod;
  }
  for (int i = 1; i <= q; i++)
    printf("%d\n", ans[i]);
  return 0;
}