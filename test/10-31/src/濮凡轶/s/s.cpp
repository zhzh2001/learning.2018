#include <fstream>
#include <algorithm>

using namespace std;

namespace sxdaknoip
{
	const int maxk = 6;

	int minn[maxk];
	int maxx[maxk];
	int man[maxk];

	int main()
	{
		ifstream fin("s.in");
		ofstream fout("s.out");
		int n, k;
		fin >> n >> k;
		int ans = 0;
		for(int i = 1; i <= n; ++i)
		{
			for(int j = 1; j <= k; ++j)
				fin >> man[j];
			if(i == 1)
			{
				ans = 1;
				for(int j = 1; j <= k; ++j)
					minn[j] = maxx[j] = man[j];
			}
			else
			{
				bool ff = true;
				for(int j = 1; j <= k; ++j)
				{
					if(maxx[j] > man[j])
					{
						ff = false;
						break;
					}
				}
				if(ff)
				{
					ans = 1;
					for(int j = 1; j <= k; ++j)
						maxx[j] = minn[j] = man[j];
				}
				else
				{
					ff = false;
					for(int j = 1; j <= k; ++j)
					{
						if(man[j] > minn[j])
						{
							ff = true;
							break;
						}
					}
					if(ff)
					{
						ans++;
						for(int j = 1; j <= k; ++j)
						{
							minn[j] = min(minn[j], man[j]);
							maxx[j] = max(maxx[j], man[j]);
						}
					}
				}
			}
			fout << ans << ' ';
		}
		fin.close();
		fout.close();
		return 0;
	}
}

int main()
{
	sxdaknoip::main();
	return 0;
}
