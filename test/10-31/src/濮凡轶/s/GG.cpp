#include <cstdio>
#include <algorithm>

const int maxn = 100005;

int n, K;

struct Splay
{
	struct Node
	{
		Node* s[2];
		int nl[6], siz;
		int maxx[6], minn[6];
		
		Node()
		{
			s[0] = s[1] = NULL, siz = 1;
		}

		inline void maintain()
		{
			siz = 1;
			for(int i = 1; i <= K; ++i)
				maxx[i] = minn[i] = nl[i];
			if(ls != NULL)
			{
				siz += ls->siz;
				for(int i = 1; i <= K; ++i)
				{
					maxx[i] = max(maxx[i], ls->maxx[i]);
					minn[i] = min(minn[i], ls->minn[i]);
				}
			}
			if(rs != NULL)
			{
				siz += rs->siz;
				for(int i = 1; i <= K; ++i)
				{
					maxx[i] = max(maxx[i], ls->maxx[i]);
					minn[i] = min(minn[i], ls->minn[i]);
				}
			}
		}

		inline int cmp(int k)
		{
			int sizl = (ls == NULL) ? 1 : ls->siz + 1;
			if(k == sizl)
				return -1;
			return sizl < k;
		}
	};

	Node* root;
	
	Splay() { root = new Node; }

	inline int getsiz(Node* x)
	{
		return x == NULL ? 0 : x->siz;
	}

	inline void rotate(Node* &o, int d)
	{
		Node* k = o->s[d^1];
		o->s[d^1] = k->s[d];
		k->s[d] = o;
		o->maintain();
		k->maintain();
		o = k;
	}

	inline void splay(Node* &o, int k)
	{
		int d = o->cmp(k);
		if(~d)
		{
			k = d ? k - getsiz(o->ls) - 1;
			int d2 = k->s[d]->cmp(k);
			if(~d2)
			{
				int k2 = d2 ? k - getsiz(o->s[d]->s[d2]) - 1 : k;
				splay(o->s[d]->s[d2], k2);
				if(d == d2)
					rotate(o, d ^ 1);
				else
					rotate(o->s[d], d2 ^ 1);
			}
			rotate(o, d ^ 1);
		}
	}
	
	int nowx[6];
	
	inline int findl(Node* o)
	{
		if(o == NULL)
			return 0;
		int d = 1;
		for(int i = 1; i <= K; ++i)
		{
			if(nowx[i] > o->nl[i])
			{
				d = 0;
				break;
			}
		}
		int L = findl(o->s[d]);
		return d ? L + getsiz(o->s[0]) + 1 : L;
	}
	
	inline int findr(Node* o)
	{
		if(o == NULL)
			return 0;
		int d = 0;
		for(int i = 1; i <= K; ++i)
		{
			if(nowx[i] < o->nl[i])
			{
				d = 1;
				break;
			}
		}
		int R = findl(o->s[d]);
		return d ? R + getsiz(o->s[0]) + 1 : L;
	}
	
	inline void solve()
	{
		for(int i = 1; i <= K; ++i)
			read(nowx[i]);
		int L = findl(root);
		int R = findr(root);
		if(L <= R)
		{
			splay(root, L - 1);
			splay(root->rs, R - L + 1);
		}
	}
}

int main()
{
}
