#include <cstdio>
#include <cctype>

inline char gc()
{
	static const int L = 233333;
	static char sxd[L], *sss = NULL, *ttt = NULL;
	return (sss == ttt) && (ttt = (sss = sxd) + fread(sxd, 1, L, stdin), sss == ttt) ? EOF : *sss++;
}

#define dd c = gc()
template<class T>
inline int read(T& x)
{
	char dd;
	x = 0;
	bool f = false;
	for(; !isdigit(c); dd)
	{
		if(c == '-')
			f = true;
		else if(c == EOF)
			return 0;
	}
	for(; isdigit(c); dd)
		x = (x << 1) + (x << 3) + (c ^ 48);
	return (x = f ? -x : x), 1;
}
#undef dd

using namespace std;

const int maxn = 2333;
const int mod = 1000000007;

typedef long long LL;

LL changshu[maxn];
LL xishu[maxn];

inline LL suan(LL l, LL k)
{
	return (xishu[l - k] * (l - k) % mod + changshu[l - k]) % mod;
}

inline LL solve(LL l, LL r, LL k)
{
	return suan(r - k, k) - suan(l - 1 - k, k);
}

int main()
{
	freopen("m.in", "r", stdin);
//	freopen("m.out", "w", stdout);
	int n, m;
	read(n), read(m);
	for(int i = 1; i <= n; ++i)
	{
		changshu[i] = (changshu[i-1] + (i * (LL) (i - 1)) % mod) % mod;
		xishu[i] = (xishu[i-1] + i) % mod;
	}
	for(int i = 1; i <= m; ++i)
	{
		LL l, r, k;
		read(l), read(r), read(k);
		printf("%lld\n", solve(l, r, k));
	}
	fclose(stdin);
//	fclose(stdout);
	return 0;
}
