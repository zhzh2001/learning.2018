#include <cstdio>
#include <cctype>

inline char gc()
{
	static const int L = 233333;
	static char sxd[L], *sss = NULL, *ttt = NULL;
	return (sss == ttt) && (ttt = (sss = sxd) + fread(sxd, 1, L, stdin), sss == ttt) ? EOF : *sss++;
}

#define dd c = gc()
template<class T>
inline int read(T& x)
{
	char dd;
	x = 0;
	bool f = false;
	for(; !isdigit(c); dd)
	{
		if(c == '-')
			f = true;
		else if(c == EOF)
			return 0;
	}
	for(; isdigit(c); dd)
		x = (x << 1) + (x << 3) + (c ^ 48);
	return (x = f ? -x : x), 1;
}
#undef dd

using namespace std;

const int maxn = 2333;
const int mod = 1000000007;

typedef long long LL;

int C[maxn][maxn];

int main()
{
	freopen("m.in", "r", stdin);
	freopen("m.out", "w", stdout);
	int n, m;
	read(n), read(m);
	C[0][0] = 1;
	for(int i = 1; i <= n; ++i)
	{
		C[i][0] = 1;
		for(int j = 1; j <= i; ++j)
			C[i][j] = (C[i-1][j-1] + C[i-1][j]) % mod;
	}
	for(int i = 0; i <= n; ++i)
		for(int j = 1; j <= n; ++j)
			(C[j][i] += C[j-1][i]) %= mod;
	LL ans = 1;
	for(int i = 1; i <= m; ++i)
	{
		int l, r, k;
		read(l), read(r), read(k);
		(ans *= (C[r][l-k] - C[l-1][l-k]) % mod) %= mod;
	}
	printf("%lld\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
