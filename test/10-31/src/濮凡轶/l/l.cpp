#include <cstdio>
#include <cctype>

inline char gc()
{
	static const int L = 233333;
	static char sxd[L], *sss = NULL, *ttt = NULL;
	return (sss == ttt) && (ttt = (sss = sxd) + fread(sxd, 1, L, stdin), sss == ttt) ? EOF : *sss++;
}

#define dd c = gc()
template<class T>
inline int read(T& x)
{
	char dd;
	x = 0;
	bool f = false;
	for(; !isdigit(c); dd)
	{
		if(c == '-')
			f = true;
		else if(c == EOF)
			return 0;
	}
	for(; isdigit(c); dd)
		x = (x << 1) + (x << 3) + (c ^ 48);
	return (x = f ? -x : x), 1;
}
#undef dd

#include <fstream>
#include <iostream>
#include <algorithm>

struct Edge
{
	int to, nxt;
} e[88];

const int mod = 2333;

int first[44];

inline void add_edge(int from, int to)
{
	static int cnt = 0;
	e[++cnt].nxt = first[from];
	first[from] = cnt;
	e[cnt].to = to;
	e[++cnt].nxt = first[to];
	first[to] = cnt;
	e[cnt].to = from;
}

int r, n, m, q, s;
int ans[31][100001];

struct QQ
{
	int s, t, l, r, id;

	inline bool operator < (const QQ& other) const
	{
		return this->s < other.s || (this->s == other.s && this->r > other.r);
	}
} Q[31];

//using namespace std;

inline void dfs(int now, int dist)
{
//	cout << "now = " << now << ' ' << dist << endl;
	if(dist > r)
		return;
	ans[now][dist]++;
	if(ans[now][dist] == mod)
		ans[now][dist] = 0;
	for(int i = first[now]; i; i = e[i].nxt)
	{
		int to = e[i].to;
		dfs(to, dist + 1);
	}
}

int Ans[31];

int main()
{
	freopen("l.in", "r", stdin);
	read(n), read(m);
	for(int i = 1, f, t; i <= m; ++i)
	{
		read(f), read(t);
		add_edge(f, t);
	}
	read(q);
//	std::cout << q << std::endl;
	for(int i = 1; i <= q; ++i)
		read(Q[i].s), read(Q[i].t), read(Q[i].l), read(Q[i].r), Q[i].id = i;
	std::sort(Q + 1, Q + q + 1);
	for(int i = 1; i <= q; ++i)
	{
		if(s != Q[i].s)
		{
			r = Q[i].r;
			s = Q[i].s;
			dfs(Q[i].s, 1);
//			cout << endl;
		}
		for(int j = Q[i].l; j <= Q[i].r; ++j)
			Ans[Q[i].id] += ans[Q[i].s][j];
	}
	std::ofstream fout("l.out");
	for(int i = 1; i <= q; ++i)
		fout << Ans[i] << '\n';
	fout.close();
	fclose(stdin);
	return 0;
}
