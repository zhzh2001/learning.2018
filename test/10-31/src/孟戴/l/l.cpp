#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#include<stdio.h>
using namespace std;
ifstream fin("l.in");
ofstream fout("l.out");
const int mod=2333;
inline long long read(){
	bool pos=1;long long x=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(long long x){
	if(x<0)x=-x,putchar('-');
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x),puts("");
}
int n,m,q,T,lim;
int f[43][43][33];
int num[43],tim;
int to[2503],head[43],nxt[2503],tot;
inline void add(int x,int y){
	to[++tot]=y,nxt[tot]=head[x],head[x]=tot;
}
int dp[43][33];
int jiyi[43][303];
int jyhss(int s,int l){
	int &ret=jiyi[s][l];
	if(ret!=-1){
		return ret;
	}
	ret=0;
	if(l>lim){
		return ret;
	}
	if(s==T){
		ret++;
	}
	for(int i=head[s];i;i=nxt[i]){
		ret=(ret+jyhss(to[i],l+1))%mod;
	}
	return ret;
}
int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read(),m=read();
	for(register int i=1;i<=m;++i){
		int u=read(),v=read();
		if(u==v){
			add(u,v);
		}else{
			add(u,v),add(v,u);
		}
		f[u][v][0]=1,f[v][u][0]=1;
	}
	for(register int l=1;l<=30;++l){
		for(register int k=1;k<=n;++k){
			for(register int i=1;i<=n;++i){
				for(register int j=1;j<=n;++j){
					f[i][j][l]=(f[i][j][l]+f[i][k][l-1]*f[k][j][l-1]%mod)%mod;
				}
			}
		}
	}
	q=read();
	while(q--){
		int s,t,l,r;
		memset(dp,0,sizeof(dp));
		memset(jiyi,-1,sizeof(jiyi));
		tim=0;
		s=read(),t=read(),l=read(),r=read();
		lim=r-l;
		T=t;
		for(int i=0;i<=30;++i){
			if(l&(1<<i)){
				num[++tim]=i;
			}
		}
		dp[s][tim]=1;
		for(register int i=tim;i>=1;--i){
			for(register int j=1;j<=n;++j){
				for(register int k=1;k<=n;++k){
					dp[k][i-1]=(dp[k][i-1]+dp[j][i]*f[j][k][num[i]]%mod)%mod;
				}
			}
		}
		int ans=0;
		for(register int i=1;i<=n;++i){
			ans=(ans+dp[i][0]*jyhss(i,0)%mod)%mod;
		}
		fout<<ans<<endl;
	}
	return 0;
}
/*

in:
2 1
1 2
3
1 2 1 3
1 2 2 3
1 2 1 1


out:
1
1
2

*/
