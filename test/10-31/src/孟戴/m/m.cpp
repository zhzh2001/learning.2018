#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#include<stdio.h>
using namespace std;
ifstream fin("m.in");
ofstream fout("m.out");
inline long long read(){
	bool pos=1;long long x=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);puts("");
}
const long long mod=1000000007;
long long n,m,fac[100003],inv[100003];
inline long long qpow(long long x,long long y){
	long long ret=1;
	for(;y;y>>=1){
		if(y&1){
			ret=ret*x%mod;
		}
		x=x*x%mod;
	}
	return ret;
}
inline long long C(long long x,long long y){
	if(x==y){
		return 1;
	}
	return fac[x]*inv[y]%mod*inv[x-y]%mod;
}
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read(),m=read();
	fac[0]=1;
	for(register long long i=1;i<=n+1;++i){
		fac[i]=fac[i-1]*i%mod;
	}
	inv[n+1]=qpow(fac[n+1],mod-2);
	for(register long long i=n;i;--i){
		inv[i]=inv[i+1]*(i+1)%mod;
	}
	inv[0]=inv[1];
	long long ans=1;
	while(m--){
		long long l,r,k;
		l=read(),r=read(),k=read();
		ans=(ans*((C(r+1,l-k+1)-C(l,l-k+1)+mod)%mod))%mod;
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
3 2
1 2 1
2 3 1

out:
10

*/
