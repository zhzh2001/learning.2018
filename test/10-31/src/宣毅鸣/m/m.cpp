#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=100005,M=1e9+7;
int n,m,fac[N],inv[N],inv2[N];
int read(){
	int x=0;char c;
	for (;c<'0'||c>'9';c=getchar());
	for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
	return x;
}
int ksm(int x,int y){
	int z=1;
	for (;y;y>>=1,(x*=x)%=M)
		if (y&1)(z*=x)%=M;
	return z;
}
signed main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read();m=read();
	int ans=1;
	fac[0]=1;
	for (int i=1;i<N;i++)fac[i]=fac[i-1]*i%M;
	for (int i=0;i<N;i++)inv[i]=ksm(fac[i],M-2);
	for (int i=0;i<N;i++)inv2[i]=ksm(i,M-2);
	while (m--){
		int x=read(),y=read(),z=read();
		z=x-z;
		(ans*=inv[z]*(fac[y+1]*inv[y-z]%M*inv2[z+1]%M-
		fac[x]*inv[x-z-1]%M*inv2[z+1]%M+M)%M)%=M;
	}
	printf("%lld",ans);
	return 0;
}
