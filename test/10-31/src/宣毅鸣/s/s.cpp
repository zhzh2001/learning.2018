#include<bits/stdc++.h>
using namespace std;
const int N=2005,M=7;
int n,m,fflag[N],now[M],b[N][M],g[M];
struct zz{
	int x,y;
}a[M][N];
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m==1){
		for (int i=1;i<=n;i++)printf("1 ");
		return 0;
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++)now[j]=1;
		for (int j=1;j<=m;j++){
			scanf("%d",&b[i][j]);
			int x=b[i][j],k=i-1;
			while (k&&x>a[j][k].x)a[j][k+1]=a[j][k],k--;
			a[j][k+1].x=x;a[j][k+1].y=i;
		}
		memset(fflag,0,sizeof fflag);
		int ans=1;
		now[1]=2;
		fflag[a[1][1].y]=1;
		for (int j=1;j<=m;j++)g[j]=b[a[1][1].y][j];
		if (i!=1)while (1){
			int flag=0;
			for (int j=1;j<=m;j++){
				while (j<=i&&fflag[a[j][now[j]].y]){
					if (j==i)flag=1;
					j++;
				}
			}
			if (flag)break;
			for (int j=1;j<=m;j++)
				if (a[j][now[j]].x>g[j]){
					for (int v=1;v<=m;v++)g[v]=min(g[v],b[a[j][now[j]].x][v]);
					fflag[a[j][now[j]].y]=1;
					j++;
					flag=1;
					break;
				}
			if (!flag)break;
			ans++;
		}
		printf("%d\n",ans);
	}
	return 0;
}
