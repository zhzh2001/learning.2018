#include<bits/stdc++.h>
typedef long long ll;
void setIO () {
	freopen("m.in", "r", stdin);
	freopen("m.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = - 1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;

int C[2100][2100], n, m;
ll g[2100][2100]; 
const int p = 1e9 + 7;
void init() {
	C[0][0] = g[0][0] = 1;
	for(int i = 1; i <= n; ++i) {
		g[i][0] = C[i][0] = 1;
		for(int j = 1; j <= i; ++j) {
			C[i][j] = (C[i - 1][j] + C[i - 1][j - 1]) % p;
			g[i][j] = (g[i - 1][j - 1] + C[i][j]) % p;
		}	
	}
}
inline ll solve(int x, int y, int z) {
/*	ll res = 0;
	for(int i = x; i <= y; ++i) {
		res += C[i][i - x + z];
		if(res >= p) res %= p;
	}
	return res;*/
	return g[y][y - x + z] - g[x - 1][z - 1];
}
int main() {
	setIO();
	n = read(), m = read();
	init();
	ll ans = 1;
	int x, y, z;
	for(int i = 1; i <= m; ++i) {
		x = read(), y = read(), z = read();
		ans = ans * solve(x, y, z) %p;
	}
	writeln(ans);
	return 0;
}
