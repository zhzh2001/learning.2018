#include<bits/stdc++.h>
typedef long long ll;
void setIO () {
	freopen("s.in", "r", stdin);
	freopen("s.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = - 1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int N = 120000;
int n, m;
int a[N][6], b[6];
bool f[N];
/*
从后往前第一个能被打败的地方 push_back(i)
对于每一个属性查询小于他的数量
*/
int main() {
	setIO();
	n = read(), m = read();
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= m; ++j) a[i][j] = read();
	for(int i = 1; i <= n; ++i) {
		for(int j = 1; j <= m; ++j) b[j] = max(b[j],a[i][j]);
		int ans = 0;
		for(int j = 1; j <= i; ++j) for(int k = 1; k <= m; ++k)
			if(a[j][k] == b[k]) {
				++ans;
				break;
			}
		printf("%d ", ans);		
	}
	return 0;
}
