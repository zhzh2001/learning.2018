#include<bits/stdc++.h>
typedef long long ll;
void setIO () {
	freopen("l.in", "r", stdin);
	freopen("l.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = - 1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int N = 60, M = 2100;
struct Edge {
	int u, v, nxt;
}e[M];
int head[N], en;
void add(int x, int y) {
	e[++en].u = x, e[en].v = y, e[en].nxt = head[x], head[x] = en;
}
int n, m;
int f[N][110000];
int s, t, l, r;
const int p = 2333;
int dfs(int x, int val) {
	if(val > r) return 0;
	if(~f[x][val]) return f[x][val];
	f[x][val] = 0;
	if(x == t && val >= l) ++f[x][val];
	for(int i = head[x]; i;i = e[i].nxt) {
		int y = e[i].v;
		(f[x][val] += dfs(y, val + 1)) %= p;
	}
	return f[x][val];
}
int main() {
	setIO(); 
	n = read(), m = read();
	for(int i = 1; i <= m; ++i) {
		int x = read(), y = read();
		add(x, y);
		add(y, x);
	}
	int Q = read();
	while(Q--) {
		memset(f, -1, sizeof f);
		s = read(), t = read(), l = read(), r = read(); 
		if(r > 100000) {
			puts("0");
			return 0;
		}
		writeln(dfs(s, 0) % p);
	}
	return 0;
}
