#pragma GCC optimize 2
#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
    ch = NC(), isneg = 1;
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 1e5 + 5;

struct Atom { int a[5], id; } c[kMaxN];
int n, k;

char outbuf[1 << 21], *p = outbuf;

namespace SubTask1 {
set<int> S; // 保存前缀中可能赢得玩家编号
inline bool Win(int a, int b) {
  for (int j = 0; j < ::k; ++j) {
    if (c[a].a[j] > c[b].a[j])
      return true;
  }
  return false;
}
int ban[2333];
int Solve() {
  S.insert(1);
  p += sprintf(p, "%d", 1);
  // int swwind = 23333333;
  for (int i = 2; i <= n; ++i) {
    bool isking = true;
    ban[0] = 0;
    for (set<int>::iterator it = S.begin(); it != S.end(); ++it) {
      bool zmg = Win(i, *it), fmg = Win(*it, i);
      if (!zmg && fmg) {
        isking = false;
      } else if (zmg && !fmg) {
        ban[++ban[0]] = *it;
      }
    }
    for (int j = 1; j <= ban[0]; ++j)
      S.erase(ban[j]);
    if (isking)
      S.insert(i);
    p += sprintf(p, " %d", (int)S.size());
  }
  p += sprintf(p, "\n");
  return 0;
}
} // namespace SubTask1

namespace SubTask2 { 
map<int, int> mp;
int Solve() {
  for (int i = 1; i <= n; ++i) {
    ++mp[c[i].a[0]];
    p += sprintf(p, "%d%c", mp.rbegin()->second, " \n"[i == n]);
  }
  return 0;
}
} // namespace SubTask2

int main() {
  freopen("s.in", "r", stdin);
  freopen("s.out", "w", stdout);
  RI(n), RI(k);
  for (int i = 1; i <= n; ++i) {
    c[i].id = i;
    for (int j = 0; j < k; ++j)
      RI(c[i].a[j]);
  }
  if (k == 1) { // 10 pts
    SubTask2::Solve();
  }
  else if (n <= 2000) { // 30 pts
    SubTask1::Solve();
  }
  fwrite(outbuf, 1, p - outbuf, stdout);
  return 0;
}