#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

inline int Rnd32() { return (rand() << 15) + rand(); }
inline int Uni32(int l, int r) { return Rnd32() % (r - l + 1) + l; }

int main() {
  freopen("l.in", "w", stdout);
  srand(GetTickCount());
  int n = 20, m = Uni32(500, 1000);
  printf("%d %d\n", n, m);
  for (int i = 1; i <= m; ++i)
    printf("%d %d\n", Uni32(1, n), Uni32(1, n));
  int q = 40;
  printf("%d\n", q);
  for (int i = 1; i <= q; ++i) {
    int l = Uni32(1, 10);
    printf("%d %d %d %d\n", Uni32(1, n), Uni32(1, n), l, Uni32(0, 200) + l);
  }
  return 0;
}