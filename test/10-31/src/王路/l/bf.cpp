#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
    ch = NC(), isneg = 1;
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMod = 2333, kMaxN = 45, kMaxM = 1005, kMaxT = 233;
int n, m, q;
struct Edge { int a, b; } e[kMaxM];
int f[kMaxT][kMaxN];

int main() {
  freopen("l.in", "r", stdin);
  freopen("bf.out", "w", stdout);
  RI(n), RI(m);
  for (int i = 1; i <= m; ++i) {
    RI(e[i].a), RI(e[i].b);
  }
  RI(q);
  for (int i = 1, s, t, l, r; i <= q; ++i) { // O(q \times m \times r), for 
    RI(s), RI(t), RI(l), RI(r);
    memset(f, 0x00, sizeof f);
    f[0][s] = 1;
    int ans = 0;
    for (int T = 1; T <= r; ++T) {
      for (int k = 1; k <= m; ++k) {
        f[T][e[k].b] = (f[T][e[k].b] + f[T - 1][e[k].a]) % kMod;
        if (e[k].a != e[k].b) f[T][e[k].a] = (f[T][e[k].a] + f[T - 1][e[k].b]) % kMod;
      }
      if (T >= l && T <= r)
        ans = (ans + f[T][t]) % kMod;
    }
    printf("%d\n", ans);
    fflush(stdout);
  }
  return 0;
}