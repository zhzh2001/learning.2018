#pragma GCC optimize 2
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
    ch = NC(), isneg = 1;
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 45, kMaxM = 1005, kMaxT = 1e9, kMod = 2333;

struct Edge { int a, b; } e[kMaxM];
struct Matrix {
  static const int kMSize = 40;
  int a[kMSize][kMSize], lim;
  inline Matrix() {
    memset(a, 0x00, sizeof a);
  }
  inline Matrix(int n, int flag) {
    memset(a, 0x00, sizeof a);
    lim = n;
    for (int i = 0; i < n; ++i)
      a[i][i] = flag;
  }
  inline Matrix operator*(const Matrix &other) const {
    Matrix ret(lim, 0);
    for (int i = 0; i < lim; ++i)
      for (int j = 0; j < lim; ++j) {
        ret.a[i][j] = 0;
        for (int k = 0; k < lim; ++k) {
          ret.a[i][j] = (ret.a[i][j] + a[i][k] * other.a[k][j] % kMod) % kMod;
        }
      }
    return ret;
  }
};
inline Matrix QuickPow(Matrix x, ll y) {
  Matrix ret(x.lim, 1); // 注意检查是否为单位矩阵
  for (; y; y >>= 1, x = x * x)
    if (y & 1) ret = ret * x;
  return ret;
}
int n, m, q, f[233][kMaxN];
int main() {
  freopen("l.in", "r", stdin);
  freopen("l.out", "w", stdout);
  RI(n), RI(m);
  for (int i = 1; i <= m; ++i) {
    RI(e[i].a), RI(e[i].b);
    --e[i].a, --e[i].b;
  }
  RI(q);
  Matrix trans(n, 0);
  for (int i = 1; i <= m; ++i) {
    ++trans.a[e[i].a][e[i].b];
    if (e[i].a != e[i].b) ++trans.a[e[i].b][e[i].a];
  }
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      trans.a[i][j] %= kMod;
  for (int i = 1, s, t, l, r; i <= q; ++i) {
    RI(s), RI(t), RI(l), RI(r);
    --s, --t;
    memset(f, 0x00, sizeof f);
    Matrix st(n, 0);
    st.a[s][0] = 1;
    Matrix res = QuickPow(trans, l) * st;
    int ans = 0;
    for (int i = 0; i < n; ++i) {
      f[0][i] = res.a[i][0];
    }
    ans = (ans + f[0][t]) % kMod;
    for (int T = l + 1; T <= r; ++T) {
      int cur = T - l;
      for (int j = 1; j <= m; ++j) {
        f[cur][e[j].b] = (f[cur][e[j].b] + f[cur - 1][e[j].a]) % kMod;
        if (e[j].a != e[j].b) f[cur][e[j].a] = (f[cur][e[j].a] + f[cur - 1][e[j].b]) % kMod;
      }
      ans = (ans + f[cur][t]) % kMod;
    }
    printf("%d\n", (ans % kMod + kMod) % kMod);
  }
  return 0;
}