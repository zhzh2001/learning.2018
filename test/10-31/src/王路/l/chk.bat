@echo off
for /L %%s in (1, 1, 100000) do (
	echo %%s
	gen.exe
	bf.exe
	l.exe
	fc bf.out l.out
	if errorlevel == 1 pause
)