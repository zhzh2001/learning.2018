#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

inline int Rnd32() { return (rand() << 15) + rand(); }
inline int Uni32(int l, int r) { return Rnd32() % (r - l + 1) + l; }

int main() {
  freopen("m.in", "w", stdout);
  srand(GetTickCount());
  int n = 2000, m = 2000;
  printf("%d %d\n", n, m);
  for (int i = 1; i <= m; ++i) {
    int l = Uni32(1, n), r = Uni32(1, n);
    if (l > r)
      swap(l, r);
    int k = Uni32(0, l - 1);
    printf("%d %d %d\n", l, r, k);
  }
  return 0;
}