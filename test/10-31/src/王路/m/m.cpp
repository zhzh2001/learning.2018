#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
    ch = NC(), isneg = 1;
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMod = 1e9 + 7, kMaxN = 2e5 + 5, kMaxVal = 2e5;

int n, m;
ll fact[kMaxN], rfact[kMaxN];

inline ll QuickPow(ll x, ll y) {
  ll ret = 1;
  x %= kMod;
  for (; y; y >>= 1, x = x * x % kMod)
    if (y & 1) ret = ret * x % kMod;
  return ret;
}

inline ll Combin(ll n, ll m) {
  if (n < m || m < 0)
    return 0;
  return fact[n] * rfact[m] % kMod * rfact[n - m] % kMod;
}

int main() {
  freopen("m.in", "r", stdin);
  freopen("m.out", "w", stdout);
  RI(n), RI(m);
  fact[0] = 1;
  for (int i = 1; i <= kMaxVal; ++i)
    fact[i] = (fact[i - 1] * i) % kMod;
  rfact[kMaxVal] = QuickPow(fact[kMaxVal], kMod - 2);
  for (int i = kMaxVal; i > 0; --i)
    rfact[i - 1] = (rfact[i] * i) % kMod;
  ll ans = 1;
  for (int i = 1, l, r, k; i <= m; ++i) {
    RI(l), RI(r), RI(k);
    ans = (ans * (Combin(r + 1, r - l + k) - Combin(l, k - 1))) % kMod;
  }
  printf("%lld\n", (ans % kMod + kMod) % kMod);
  return 0;
}