@echo off
for /L %%s in (1, 1, 100000) do (
	echo %%s
	gen.exe
	bf.exe
	m.exe
	fc bf.out m.out
	if errorlevel == 1 pause
)