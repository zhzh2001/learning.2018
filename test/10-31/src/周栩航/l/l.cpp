#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#pragma GCC optimize 2
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

int n,m,q,mo=2333;
struct mat{int a[42][42],sx,sy;mat(){memset(a,0,sizeof a);sx=sy=0;}}zy;
inline mat operator * (mat x,mat y)
{
	mat tmp;
	tmp.sy=y.sy;tmp.sx=x.sx;
	For(k,1,y.sx)
		For(i,1,x.sx)	if(x.a[i][k])
			For(j,1,y.sy)	if(y.a[k][j])
				tmp.a[i][j]+=x.a[i][k]*y.a[k][j],tmp.a[i][j]%=mo;
	return tmp;
}
mat one;
inline mat ksm(mat x,int y)
{
	if(y==0)	return one;
	mat sum=x;y--;
	for(;y;y/=2,x=x*x)	if(y&1)	sum=sum*x;
	return sum;
}
inline void out(mat x)
{
	For(i,1,x.sx)
	{
		For(j,1,x.sy)	write_p(x.a[i][j]);
		puts("");
	}
}
int main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read();m=read();
	For(i,1,n+1)	one.a[i][i]=1;one.sx=one.sy=n+1;
	For(i,1,m)
	{
		int x=read(),y=read();
		zy.a[x][y]=zy.a[y][x]=1;
	}
	zy.sx=zy.sy=n+1;
	q=read();
	For(tt,1,q)
	{
		int s=read(),t=read(),l=read(),r=read();
		mat ne=zy;
		ne.a[n+1][t]=1;ne.a[n+1][n+1]=1;
		mat tmp=ksm(ne,l-1),ans;
		ans.sx=n+1;ans.sy=1;
		ans.a[s][1]=1;
		
		int ans1=(tmp*ans).a[n+1][1]+(tmp*ans).a[t][1];
		tmp=tmp*ksm(ne,r-l+1);
		int ans2=(tmp*ans).a[n+1][1]+(tmp*ans).a[t][1];
		writeln(((ans2-ans1)%mo+mo)%mo);
	}
}
