#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
int n,m;
const int N=2e5+5;
ll fac[N],rev[N],l[N],r[N],k[N],mo=1e9+7;
inline ll C(ll x,ll y){if(x<y)	return 0;if(y==0)	return 1;return fac[x]*rev[y]%mo*rev[x-y]%mo;}
inline ll Get(int t,int l,int k){return C(t+1,l-k+1);}
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2,x=x*x%mo)	if(y&1)	sum=sum*x%mo;	return sum;}
int main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read();m=read();
	ll ans=1;
	fac[0]=1;
	For(i,1,n+1)	fac[i]=fac[i-1]*i%mo;
	rev[n+1]=ksm(fac[n+1],mo-2);
	Dow(i,0,n)	rev[i]=rev[i+1]*(i+1)%mo;
	For(i,1,m)
	{
		l[i]=read();r[i]=read();
		k[i]=read();
		ans*=(Get(r[i],l[i],k[i])-Get(l[i]-1,l[i],k[i])+mo)%mo;
		ans=(ans%mo+mo)%mo;
	}
	writeln(ans);
}
