#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=2005;
int n,k,a[N][6],out[N],in[N],poi[N*N],nxt[N*N],head[N],qx[N*N],qy[N*N],dfn[N],low[N],q[N];
int tsz[N],sz[N],num,tnum,cnt,to[N],top,bel[N];
bool inq[N];
inline bool win(int x,int y)
{
	For(i,1,k)	if(a[x][i]>a[y][i])	return 1;
	return 0;
}
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;}
int tim;
inline void Tar(int x)
{
	dfn[x]=low[x]=++tim;q[++top]=x;inq[x]=1;
	for(int i=head[x];i;i=nxt[i])
	{
		if(!dfn[poi[i]])	Tar(poi[i]),low[x]=min(low[x],low[poi[i]]);
		else	if(inq[poi[i]])	low[x]=min(low[x],low[poi[i]]);
	}
	if(low[x]==dfn[x])
	{
		tnum++;
		while(1)
		{
			inq[q[top]]=0;to[q[top]]=tnum;
			tsz[tnum]+=sz[q[top]];
			top--;
			if(q[top+1]==x)	break;
		}
	}
}
int id[N];
inline bool cmp(int x,int y){return qx[x]!=qx[y]?qx[x]<qx[y]:qy[x]<qy[y];}
inline Solve(int t)
{
	For(i,1,num+1)	dfn[i]=0,to[i]=0,tsz[i]=0;
	tnum=0;top=0;tim=0;
	For(i,1,num+1)	if(!dfn[i])	Tar(i);
	For(i,1,t)	bel[i]=to[bel[i]];
	int etop=0;
	For(x,1,num+1)
		for(int i=head[x];i;i=nxt[i])	qx[++etop]=to[x],qy[etop]=to[poi[i]];
	For(i,1,etop)	id[i]=i;
	sort(id+1,id+etop+1,cmp);
	For(x,1,num+1)	head[x]=0,in[x]=0;cnt=0;
	For(ii,1,etop)
	if(qx[id[ii]]!=qx[id[ii-1]]||qy[id[ii]]!=qy[id[ii-1]])
	{
		int i=id[ii];
	
		if(qx[i]!=qy[i])	add(qx[i],qy[i]),in[qy[i]]++;
	}
	swap(sz,tsz);swap(num,tnum);
	For(i,1,num)	if(!in[i])	write_p(sz[i]);
}
bool vis[N],vis1[N];
int main()
{
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read();k=read();
	if(k==1)	{For(i,1,n)	write_p(1);return 0;}
	For(i,1,n)	For(j,1,k)	a[i][j]=read();
	bool tp=1;
	For(i,1,n)	For(j,1,k)	if(a[i][j]!=i)	tp=0;
	if(tp)	{For(i,1,n)	write_p(1);return 0;}
	write_p(1);
	sz[1]=1;bel[1]=1;num=1;
	For(i,2,n)
	{
		sz[num+1]=1;bel[i]=num+1;
		For(j,1,num+1)	vis[j]=0,vis1[j]=0;
		For(j,1,i-1)
		{
			if(win(i,j))	if(!vis[bel[j]])	add(bel[i],bel[j]),vis[bel[j]]=1;
			if(win(j,i))	if(!vis1[bel[j]])	add(bel[j],bel[i]),vis1[bel[j]]=1;
		}
		Solve(i);
	}
}
/*
5 2
1 4
2 3
3 2 
4 1 
5 5
*/
