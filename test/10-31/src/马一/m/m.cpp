#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
namespace Dango
{
	const int MAXN=100005,MOD=1000000007;
	int n,m,l,r,k;
	long long fac[MAXN],inv[MAXN],ans=1;
	long long pow_(long long a,long long b)
	{
		long long result=1;
		while(b)
		{
			if(b&1)result=result*a%MOD;
			a=a*a%MOD;
			b>>=1;
		}
		return result;
	}
	void init(int a)
	{
		fac[0]=1;
		for(int i=1;i<=a;i++)
			fac[i]=fac[i-1]*i%MOD;
		inv[a]=pow_(fac[a],MOD-2);
		for(int i=a-1;i>=0;i--)
			inv[i]=inv[i+1]*(i+1)%MOD;
	}
	long long C(int a,int b){return fac[a]*inv[b]%MOD*inv[a-b]%MOD;}
	int work()
	{
		ios::sync_with_stdio(false);
		cin>>n>>m;
		init(n+1);
		for(int i=1;i<=m;i++)
		{
			long long result=0;
			cin>>l>>r>>k;
			result=((C(r+1,l-k+1)-C(l,l-k+1))%MOD+MOD)%MOD;
			ans=ans*result%MOD;
		}
		cout<<ans;
		return 0;
	}
}
int main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	return Dango::work();
}
