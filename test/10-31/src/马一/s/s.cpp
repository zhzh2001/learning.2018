#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
namespace Dango
{
	const int MAXN=100005,MAXK=15;
	int n,k,x[MAXN][MAXK],ans;
	bool v[MAXN];
	int read()
	{
		int x=0,f=0;
		char ch=getchar();
		while(!isdigit(ch)){f|=(ch=='-');ch=getchar();}
		while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
		return f?-x:x;
	}
	int work()
	{
		n=read();k=read();
		if(k==1)
		{
			for(int i=1;i<=n;i++)
				printf("1 ");
			return 0;
		}
		for(int i=1;i<=n;i++)
			for(int j=1;j<=k;j++)
				x[i][j]=read();
		for(int i=1;i<=n;i++)
		{
			v[i]=true;
			ans++;
			for(int j=1;j<i;j++)
			{
				if(!v[j])continue;
				bool flag1=true,flag2=true;
				for(int q=1;q<=k;q++)
				{
					if(x[i][q]<x[j][q])flag1=false;
					else flag2=false;
				}
				if(flag1){ans--;v[j]=false;}
				if(flag2){ans--;v[i]=false;break;}
			}
			printf("%d ",ans);
		}
		return 0;
	}
}
int main()
{
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	return Dango::work();
}
