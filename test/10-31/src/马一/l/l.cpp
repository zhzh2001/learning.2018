#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<queue>
using namespace std;
namespace Dango
{
	const int MAXN=2005;
	int n,k,p[MAXN][6];
	int head[MAXN],to[MAXN*MAXN],nxt[MAXN],cnt;
	bool vis[MAXN];
	void add(int u,int v)
	{
		cnt++;
		to[cnt]=v;
		nxt[cnt]=head[u];
		head[u]=cnt;
	}
	bool bfs(int a,int s)
	{
		queue<int> q;
		memset(vis,0,sizeof(vis));
		q.push(a);
		vis[a]=true;
		while(!q.empty())
		{
			int u=q.front();q.pop();
			for(int i=head[u];i;i=nxt[i])
			{
				int v=to[i];
				if(!vis[v])
				{
					vis[v]=true;
					q.push(v);
				}
			}
		}
		for(int i=1;i<=s;i++)
			if(!vis[i])return false;
		return true;
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin>>n>>k;
		if(k==1)
		{
			for(int i=1;i<=n;i++)
				cout<<1<<" ";
			return 0;
		}
		for(int i=1;i<=n;i++)
			for(int j=1;j<=k;j++)
				cin>>p[i][j];
		for(int i=1;i<=n;i++)
		{
			int ans=0;
			for(int j=1;j<i;j++)
				for(int l=1;l<=k;l++)
					if(p[i][l]>p[j][l])
					{
						add(i,j);
						break;
					}
			for(int j=1;j<i;j++)
				for(int l=1;l<=k;l++)
					if(p[i][l]<p[j][l])
					{
						add(j,i);
						break;
					}
			for(int j=1;j<=i;j++)
				if(bfs(j,i))ans++;
			cout<<ans<<" ";
		}
		return 0;
	}
}
int main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	return Dango::work();
}
