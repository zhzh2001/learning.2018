#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int P=2333;
int nedge,head[50];
struct Edge{
	int to,nxt;
}edge[2005];
void add(int x,int y){
	edge[++nedge].to=y;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
int f[30][100005];
int s,t,l,r;
void dfs(int x,int s){
	if(s+1>r)return;
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		f[y][s+1]=(f[y][s+1]+f[x][s])%P;
		dfs(y,s+1);
	}
}
int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	int n=read(),m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		add(x,y),add(y,x);
	}
	for(int q=read();q;q--){
		s=read(),t=read(),l=read(),r=read();
		memset(f,0,sizeof(f));
		f[s][0]=1;
		dfs(s,0);
		int ans=0;
		for(int i=l;i<=r;i++)ans=(ans+f[t][i])%P;
		printf("%d\n",ans);
	}
}
