#include<cstdio>
#include<iostream>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int P=1e9+7;
int c[2005][2005];
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	int n=read(),m=read();
	c[0][0]=1;
	for(int i=1;i<=n;i++){
		c[i][0]=1;
		for(int j=1;j<=n;j++)
			c[i][j]=(c[i-1][j-1]+c[i-1][j])%P;
	}
	for(int i=1;i<=n;i++)
		for(int j=0;j<=i;j++)
			c[i][j]=(c[i-1][j]+c[i][j])%P;
	int ans=1;
	while(m--){
		int l=read(),r=read(),k=read();
		int sum=c[r][l-k]-c[l-1][l-k];
		sum=(sum%P+P)%P;
		ans=1LL*ans*sum%P;
	}
	printf("%d",ans);
}
