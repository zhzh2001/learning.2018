#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
inline char _gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return (p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2))?EOF:*p1++;
}
#define gc c=_gc()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=1e5+5;
int nedge,head[2005];
struct Edge{
	int to,nxt;
}edge[4000005];
void add(int x,int y){
	edge[++nedge].to=y;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
int n,k,ans,Max[10],Min[10];
int x[2005][10];
inline bool check(const int &a,const int &b){
	for(int i=1;i<=k;i++)
		if(x[a][i]>x[b][i])return 1;
	return 0;
}
int tot,scc,dfn[2005],low[2005],siz[2005],deg[2005],col[2005],top,st[2005];
bool vis[2005];
void dfs(int x){
	dfn[x]=low[x]=++tot;st[++top]=x;vis[x]=1;
	for(register int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		if(!dfn[y]){
			dfs(y);
			low[x]=min(low[x],low[y]);
		}else if(vis[y])low[x]=min(low[x],dfn[y]);
	}
	if(low[x]==dfn[x]){
		scc++;deg[scc]=0;siz[scc]=0;
		int u;
		do{
			u=st[top--];
			vis[u]=0;
			siz[scc]++;
			col[u]=scc;
		}while(u!=x);
	}
}
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read(),k=read();
	if(k==1){
		for(int i=1;i<=n;i++)printf("1 ");
		return 0;
	}
	if(n<=2000){
		for(register int i=1;i<=n;i++)
			for(register int j=1;j<=k;j++)
				x[i][j]=read();
		for(register int i=1;i<=n;i++){
			for(register int j=1;j<i;j++){
				if(check(i,j))add(i,j);
				if(check(j,i))add(j,i);
			}
			scc=tot=0;
			for(register int j=1;j<=i;j++)col[j]=dfn[j]=low[j]=0;
			for(register int j=1;j<=i;j++)
				if(!dfn[j])dfs(j);
			for(register int j=1;j<=i;j++)
				for(register int p=head[j];p;p=edge[p].nxt){
					int q=edge[p].to;
					if(col[q]!=col[j])deg[col[q]]++;
				}
			ans=0;
			for(register int j=1;j<=scc;j++)
				if(!deg[j])ans+=siz[j];
			printf("%d ",ans);
		}
		return 0;
	}
	ans=1;
	for(int i=1;i<=k;i++)Min[i]=Max[i]=read();
	printf("1");
	for(int i=2;i<=n;i++){
		bool f=0;
		int cnt=0;
		for(int j=1;j<=k;j++){
			int x=read();
			if(x>Min[j])f=1;
			if(x>Max[j])cnt++;
			Min[j]=min(x,Min[j]);
			Max[j]=max(x,Max[j]);
		}
		if(cnt==k)ans=0;
		ans+=f;
		printf(" %d",ans);
	}
}
