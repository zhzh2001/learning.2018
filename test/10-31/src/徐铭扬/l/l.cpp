#include<bits/stdc++.h>
using namespace std;
const int P=2333;
int n,m,i,q,s,t,x,y,l,r;
struct M{
	int a[42][42];
	friend M operator *(M x,M y){
		M z;memset(z.a,0,sizeof(z.a));
		for (int i=0;i<n;i++)
			for (int j=0;j<n;j++){
				for (int k=0;k<n;k++) z.a[i][j]+=x.a[i][k]*y.a[k][j];
				z.a[i][j]%=P;
			}
		return z;
	}
	friend M operator ^(M x,int y){
		M z;memset(z.a,0,sizeof(z.a));
		for (int i=0;i<n;i++) z.a[i][i]=1;
		for (;y;y>>=1,x=x*x)
			if (y&1) z=z*x;
		return z;
	}
	friend M operator +(M x,M y){
		for (int i=0;i<n;i++)
			for (int j=0;j<n;j++) x.a[i][j]+=y.a[i][j];
		return x;
	}
}A,B,E;
M sum(M A,int n,M T){
	if (n==1) return A;
	M B=A^(n/2);
	M C=sum(A,n/2,B)*(B+E);
	if (n&1) C=C+T;
	return C;
}
int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=m;i++) scanf("%d%d",&x,&y),x--,y--,A.a[x][y]=A.a[y][x]=1;
	for (i=0;i<n;i++) E.a[i][i]=1;
	for (scanf("%d",&q);q--;){
		scanf("%d%d%d%d",&s,&t,&l,&r);
		s--;t--;
		B=sum(A,r-l+1,A^(r-l+1))*(A^(l-1));
		printf("%d\n",(B.a[s][t]%P+P)%P);
	}
}
