#include<bits/stdc++.h>
using namespace std;
const int M=1e9+7,N=100002;
int n,m,i,l,r,k,ans,f[N],x,y;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int rd(){
	int x=0,fl=1;char ch=gc();
	for (;ch<48||ch>57;ch=gc())if(ch=='-')fl=-1;
	for (;48<=ch&&ch<=57;ch=gc())x=(x<<3)+(x<<1)+(ch^48);
	return x*fl;
}
inline void wri(int a){if(a<0)a=-a,putchar('-');if(a>=10)wri(a/10);putchar(a%10|48);}
inline void wln(int a){wri(a);puts("");}
void ex_gcd(int a,int b,int &x,int &y){
	if (!b) x=1,y=0;
	else ex_gcd(b,a%b,y,x),y-=a/b*x;
}
int C(int n,int m){
	ex_gcd(f[m],M,x,y);
	int t=1ll*f[n]*x%M;
	ex_gcd(f[n-m],M,x,y);
	t=1ll*t*x%M;
	return t;
}
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=rd(),m=rd();
	f[0]=1;
	for (i=1;i<=n+1;i++) f[i]=1ll*f[i-1]*i%M;
	ans=1;
	for (;m--;){
		l=rd(),r=rd(),k=rd();
		ans=1ll*ans*(C(r+1,l-k+1)-C(l,l-k+1))%M;
	}
	printf("%d",(ans+M)%M);
}

