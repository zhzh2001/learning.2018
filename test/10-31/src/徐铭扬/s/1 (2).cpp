#include<bits/stdc++.h>
using namespace std;
int n,m,i,a[100002][5],mx[5],j,cnt,t,las;
bool fl;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int rd(){
	int x=0,fl=1;char ch=gc();
	for (;ch<48||ch>57;ch=gc())if(ch=='-')fl=-1;
	for (;48<=ch&&ch<=57;ch=gc())x=(x<<3)+(x<<1)+(ch^48);
	return x*fl;
}
inline void wri(int a){if(a<0)a=-a,putchar('-');if(a>=10)wri(a/10);putchar(a%10|48);}
inline void wln(int a){wri(a);puts("");}
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=rd();m=rd();
	for (i=0;i<n;i++)
		for (t=0;t<m;t++) a[i][t]=rd();
	for (i=0;i<n;i++){
		fl=1;
		for (t=0;t<m && fl;t++)
			if (a[i][t]<mx[t]) fl=0;
		if (fl){
			cnt++;
			wri(cnt);if (i<n) putchar(' ');
			continue;
		}
		cnt=0;
		for (j=las;j<=i;j++)
			for (t=0;t<m;t++) mx[t]=max(mx[t],a[j][t]);
		las=i+1;
		wri(i);if (i<n) putchar(' ');
	}
}
