#include<bits/stdc++.h>
using namespace std;
// #define unsigned long long
const int mod=2333;
int n,m,x,y,l,r,q;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=45;
unsigned a[N][N],f[N][N],g[N][N],h[N][N],ans;
int main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		// if (a[x][y]==1)
		// 	cout<<i<<' '<<x<<' '<<y<<'\n';
		a[x][y]=1;
		a[y][x]=1;
	}
	read(q);
	while (q--)
	{
		read(x);
		read(y);
		read(l);
		read(r);
		int t=l-1;
		for (int i=1;i<=n;++i)
			for (int j=1;j<=n;++j)
				f[i][j]=(i==j?1:0);
		for (int i=1;i<=n;++i)
			for (int j=1;j<=n;++j)
				g[i][j]=a[i][j];
		while (t)
		{
			if (t&1)
			{
				for (int i=1;i<=n;++i)
					for (int j=1;j<=n;++j)
						h[i][j]=0;
				for (int i=1;i<=n;++i)
					for (int j=1;j<=n;++j)
						for (int k=1;k<=n;++k)
							h[i][k]+=f[i][j]*g[j][k];
				for (int i=1;i<=n;++i)
					for (int j=1;j<=n;++j)
						f[i][j]=h[i][j]%mod;
			}
			for (int i=1;i<=n;++i)
				for (int j=1;j<=n;++j)
					h[i][j]=0;
			for (int i=1;i<=n;++i)
				for (int j=1;j<=n;++j)
					for (int k=1;k<=n;++k)
						h[i][k]+=g[i][j]*g[j][k];
			for (int i=1;i<=n;++i)
				for (int j=1;j<=n;++j)
					g[i][j]=h[i][j]%mod;
			t/=2;
		}
		ans=0;
		for (int i=l;i<=r;++i)
		{
			for (int i=1;i<=n;++i)
				for (int j=1;j<=n;++j)
					h[i][j]=0;
			// memset(h,0,sizeof(h));
			for (int i=1;i<=n;++i)
				for (int j=1;j<=n;++j)
					for (int k=1;k<=n;++k)
						h[i][k]+=(a[j][k]?f[i][j]:0);//*a[j][k];
			for (int i=1;i<=n;++i)
				for (int j=1;j<=n;++j)
					f[i][j]=h[i][j]%mod;
			ans+=f[x][y];
		}
		printf("%d\n",ans%mod);
	}
	return 0;
}