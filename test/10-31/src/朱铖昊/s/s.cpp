#include<bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=100005;
struct data
{
	int size,mx[7],mn[7];
}f[N],a;
int fa[N],cnt,s[N][2],root,n,m;
inline int isroot(int x)
{
	return s[fa[x]][0]!=x&&s[fa[x]][1]!=x;
}
inline int get_root(int x)
{
	while (!isroot(x))
		x=fa[x];
	return x;
}
inline void rotate(int x)
{
	int y=fa[x],z=fa[y],l=(s[y][1]==x),r=l^1;
	if (!isroot(y))
		s[z][(s[z][1]==y)]=x;
	s[y][l]=s[x][r];
	s[x][r]=y;
	fa[y]=x;
	fa[x]=z;
}
// inline void rotate(int x,int y)
// {
// 	if (y==root)
// 		root=x;
// 	if (fa[y])
// 	{
// 		fa[x]=fa[y];
// 		if (s[fa[x]][0]==y)
// 			s[fa[x]][0]=x;
// 		else
// 			s[fa[x]][1]=x;
// 	}
// 	else
// 		fa[x]=0;
// 	int p=(s[x][1]==y);
// 	s[y][p]=s[x][!p];
// 	fa[s[y][p]]=y;
// 	fa[y]=x;
// 	s[x][!p]=y;
// }
/*inline void del(int x)
{
	while (s[x][0])
		rotate(s[x][0]);
	if (!s[x][1])
	{
		if (s[fa[x]][0]==x)
			s[fa[x]][0]=0;
		else
			s[fa[x]][1]=0;
		if (isroot(x))
			root=0;
		else
			root=get_root(fa[x]);
		fa[x]=0;
	}
	else
	{
		fa[s[x][1]]=fa[x];
		if (isroot(x))
			root=s[x][1];
		else
		{
			root=get_root(fa[x]);
			if (s[fa[x]][0]==x)
				s[fa[x]][0]=s[x][1];
			else
				s[fa[x]][1]=s[x][1];
		}
		fa[x]=0;
		s[x][1]=0;
	}
}*/
inline void del(int x)
{
	while (s[x][0]||s[x][1])
	{
		if (s[x][0])
			rotate(s[x][0]);
		else
			rotate(s[x][1]);
	}
	if (isroot(x))
		root=0;
	else
	{
		root=get_root(fa[x]);
		if (s[fa[x]][0]==x)
			s[fa[x]][0]=0;
		else
			s[fa[x]][1]=0;
		fa[x]=0;
	}
}
inline void merge(data &a,data b)
{
	a.size+=b.size;
	for (int i=1;i<=m;++i)
	{
		a.mx[i]=max(a.mx[i],b.mx[i]);
		a.mn[i]=min(a.mn[i],b.mn[i]);
	}
}
inline int work(data x,data y)
{
	int sum=0;
	for (int i=1;i<=m;++i)
	{
		if (y.mx[i]>x.mx[i])
			++sum;
		if (y.mx[i]>x.mn[i])
			++sum;
		if (y.mn[i]>x.mx[i])
			++sum;
		if (y.mn[i]>x.mn[i])
			++sum;
	}
	return sum;
}
inline void splay(int x)
{
	while (!isroot(x))
	{
		int y=fa[x],z=fa[y];
		if (!isroot(y)&&((s[z][1]==y)==(s[y][1]==x)))
			rotate(y);
		rotate(x);
	}
}
inline void add(int &x,data y,int FA)
{
	if (!x)
	{
		x=++cnt;
		fa[x]=FA;
		f[cnt]=y;
		splay(x);
		return;
	}
	int p=work(f[x],y);
	if (p==0)
		add(s[x][1],y,x);
	if (p==m*4)
		add(s[x][0],y,x);
	if (p>0&&p<m*4)
	{
		merge(y,f[x]);
		del(x);
		// if (!isroot(root)&&root!=0)
		// 	puts("!!!");
		// root=get_root(root);
		add(root,y,0);
	}
}
inline int first(int x)
{
	while (s[x][0])
		x=s[x][0];
	return x;
}
int main()
{
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	read(n);
	read(m);
	root=0;
	if (m==1)
	{
		for (int i=1;i<=n;++i)
			printf("1 ");
		return 0;
	}
	for (int i=1;i<=n;++i)
	{
		// cout<<i<<' ';
		a.size=1;
		for (int j=1;j<=m;++j)
		{
			read(a.mx[j]);
			a.mn[j]=a.mx[j];
		}
		add(root,a,0);
		root=get_root(cnt);
		printf("%d ",f[first(root)].size);
	}
	return 0;
}