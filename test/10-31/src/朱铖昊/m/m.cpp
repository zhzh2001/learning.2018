#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=100005;
const int mod=1e9+7;
int n,m,x,y,k,ans;
int fac[N],inv[N];
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline int C(int x,int y)
{
	return (ll)fac[x]*inv[y]%mod*inv[x-y]%mod;
}
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
int main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	fac[0]=1;
	for (int i=1;i<N;++i)
		fac[i]=(ll)fac[i-1]*i%mod;
	inv[N-1]=ksm(fac[N-1],mod-2);
	for (int i=N-2;i>=0;--i)
		inv[i]=(ll)inv[i+1]*(i+1)%mod;
	read(n);
	read(m);
	ans=1;
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		read(k);
		ans=(ll)ans*((C(y+1,x-k+1)-C(x,x-k+1)+mod)%mod)%mod;
	}
	cout<<ans;
}