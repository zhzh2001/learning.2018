#include <bits/stdc++.h>
using namespace std;
#define file "s"
typedef long long ll;
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
int k;
struct atom{
	int sz,mn[6],mx[6];
	bool operator <(const atom& rhs)const{
		for(int i=1;i<=k;i++)
			if (mx[i]>rhs.mn[i])
				return true;
		return false;
	}
	atom& operator +=(const atom& rhs){
		sz+=rhs.sz;
		for(int i=1;i<=k;i++)
			mn[i]=min(mn[i],rhs.mn[i]),mx[i]=max(mx[i],rhs.mx[i]);
		return *this;
	}
};
set<atom> s;
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read();
	k=read();
	while(n--){
		atom t;
		t.sz=1;
		for(int i=1;i<=k;i++)
			t.mn[i]=t.mx[i]=read();
		set<atom>::iterator it=s.upper_bound(t);
		for(;;){
			if (it==s.end() || !(*it<t))
				break;
			t+=*it;
			s.erase(it++);
		}
		s.insert(t);
		printf("%d ",s.begin()->sz);
	}
}
