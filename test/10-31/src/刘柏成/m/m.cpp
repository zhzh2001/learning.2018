#include <bits/stdc++.h>
using namespace std;
#define file "m"
typedef long long ll;
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
const int mod=1000000007;
ll power(ll x,ll y){
	x%=mod;
	ll ans=1;
	while(y){
		if (y&1)
			ans=ans*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return ans;
}
const int maxn=110003;
ll fac[maxn],invf[maxn];
void init(int n){
	fac[0]=1;
	for(int i=1;i<=n;i++)
		fac[i]=fac[i-1]*i%mod;
	invf[n]=power(fac[n],mod-2);
	for(int i=n;i;i--)
		invf[i-1]=invf[i]*i%mod;
}
ll c(int n,int m){
	if (n<m || m<0)
		return 0;
	return fac[n]*invf[m]%mod*invf[n-m]%mod;
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read(),m=read();
	init(n+1);
	ll ans=1;
	while(m--){
		int l=read(),r=read(),k=read();
		if (k>l)
			return puts("0"),0;
		ans=(ans*(c(r+1,l-k+1)-c(l,l-k+1)))%mod;
	}
	printf("%lld",(ans%mod+mod)%mod);
}