#include <bits/stdc++.h>
using namespace std;
#define file "lbc"
typedef long long ll;
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	printf("%d",read()+read());
}