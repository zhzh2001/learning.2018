#include <bits/stdc++.h>
using namespace std;
#define file "l"
typedef long long ll;
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
const int mod=2333;
int n;
struct mat{
	int a[42][42];
	mat(){
		memset(a,0,sizeof(a));
	}
	mat operator *(const mat& rhs)const{
		mat ans;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				for(int k=1;k<=n;k++)
					ans.a[i][k]+=a[i][j]*rhs.a[j][k];
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				ans.a[i][j]%=mod;
		return ans;
	}
	mat operator +(const mat& rhs)const{
		mat ans;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++){
				ans.a[i][j]=a[i][j]+rhs.a[i][j];
				if (ans.a[i][j]>=mod)
					ans.a[i][j]-=mod;
			}
		return ans;
	}
}sum[202],st,now;
mat power(mat x,int y){
	mat ans;
	for(int i=1;i<=n;i++)
		ans.a[i][i]=1;
	while(y){
		if (y&1)
			ans=ans*x;
		x=x*x;
		y>>=1;
	}
	return ans;
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=read();
	int m=read();
	while(m--){
		int u=read(),v=read();
		st.a[u][v]=st.a[v][u]=1;
	}
	for(int i=1;i<=n;i++)
		now.a[i][i]=1;
	sum[0]=now;
	for(int i=1;i<=200;i++){
		now=now*st;
		sum[i]=sum[i-1]+now;
		// printf("%d\n",sum[i].a[1][1]);
	}
	int q=read();
	while(q--){
		int s=read(),t=read(),l=read(),r=read();
		if (l>r)
			return puts("0"),0;
		now=power(st,l);
		now=now*sum[r-l];
		printf("%d\n",now.a[s][t]);
	}
//	fprintf(stderr,"%.5f",1.0*clock()/CLOCKS_PER_SEC);
}
