#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cctype>
#include <vector>
using namespace std;
namespace zuiyt
{
	template<typename T>
	inline void read(T &x)
	{
		char c;
		bool f = false;
		x = 0;
		do
			c = getchar();
		while (c != '-' && !isdigit(c));
		if (c == '-')
			f = true, c = getchar();
		do
			x = x * 10 + c - '0', c = getchar();
		while (isdigit(c));
		if (f)
			x = -x;	
	}	
	template<typename T>
	inline void write(T x)
	{
		static char buf[30];
		char *pos = buf;
		if (x < 0)
			putchar('-'), x = -x;
		do
			*pos++ = x % 10 + '0';
		while (x /= 10);
		while (pos > buf)
			putchar(*--pos);
	}
	typedef long long ll;
	const int Jumpmelon = 2333, N = 31, T = 1e5 + 10;
	int l, r, s, goal, f[N][T];
	vector<int> g[N];
	int dfs(const int u, const int t)
	{
		if (~f[u][t])
			return f[u][t];
		if (t == r)
			return f[u][t] = (u == goal);
		f[u][t] = 0;
		if (u == goal && t >= l)
			++f[u][t];
		for (int i = 0; i < g[u].size(); i++)
			f[u][t] = (f[u][t] + dfs(g[u][i], t + 1)) % Jumpmelon;
		return f[u][t];
	}
	int work() 
	{
		int n, m, ans = 1;
		read(n), read(m);
		for (int i = 0; i < m; i++)
		{
			int a, b;
			read(a), read(b);
			g[a].push_back(b);
			g[b].push_back(a);	
		}
		int q;
		read(q);
		while (q--)
		{
			read(s), read(goal), read(l), read(r);
			memset(f, -1, sizeof(int[n + 1][T]));
			write(dfs(s, 0));
			putchar('\n');
		}
		return 0;
	}
}
int main()
{
	freopen("l.in", "r", stdin);
	freopen("l.out", "w", stdout);
	return zuiyt::work();	
}
