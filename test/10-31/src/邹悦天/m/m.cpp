#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cctype>
using namespace std;
namespace zuiyt
{
	template<typename T>
	inline void read(T &x)
	{
		char c;
		bool f = false;
		x = 0;
		do
			c = getchar();
		while (c != '-' && !isdigit(c));
		if (c == '-')
			f = true, c = getchar();
		do
			x = x * 10 + c - '0', c = getchar();
		while (isdigit(c));
		if (f)
			x = -x;	
	}	
	template<typename T>
	inline void write(T x)
	{
		static char buf[30];
		char *pos = buf;
		if (x < 0)
			putchar('-'), x = -x;
		do
			*pos++ = x % 10 + '0';
		while (x /= 10);
		while (pos > buf)
			putchar(*--pos);
	}
	typedef long long ll;
	const int Jumpmelon = 1e9 + 7, N = 2010;
	int fac[N], finv[N], sum[N][N];
	inline int power(int a, int b, const int p = Jumpmelon)
	{
		int ans = 1;
		while (b)
		{
			if (b & 1)
				ans = (ll)ans * a % p;
			a = (ll)a * a % p;
			b >>= 1;
		}
		return ans;
	}
	inline int inv(const int a, const int p = Jumpmelon)
	{
		return power(a, p - 2, p);	
	}
	inline int C(const int n, const int m, const int p = Jumpmelon)
	{
		return (ll)fac[n] * finv[m] % Jumpmelon * finv[n - m] % Jumpmelon;	
	}
	void init(const int n)
	{
		fac[0] = 1;
		for (int i = 1; i < N; i++)
			fac[i] = (ll)fac[i - 1] * i % Jumpmelon;
		finv[N - 1] = inv(fac[N - 1]);
		for (int i = N - 1; i > 0; i--)
			finv[i - 1] = (ll)finv[i] * i % Jumpmelon;
		for (int i = 0; i <= n; i++)
		{
			sum[i][0] = C(i, 0);
			for (int j = 1; j + i <= n; j++)
				sum[i][j] = (sum[i][j - 1] + C(j + i, j)) % Jumpmelon;
		}
	}
	int work() 
	{
		int n, m, ans = 1;
		read(n), read(m);
		init(n);
		while (m--)
		{
			int l, r, k;
			read(l), read(r), read(k);
			ans = (ll)ans * ((sum[l - k][r + k - l] - (k ? sum[l - k][k - 1] : 0) + Jumpmelon) % Jumpmelon) % Jumpmelon;
		}
		write(ans);
		return 0;
	}
}
int main()
{
	freopen("m.in", "r", stdin);
	freopen("m.out", "w", stdout);
	return zuiyt::work();	
}
