#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cctype>
using namespace std;
namespace zuiyt
{
	template<typename T>
	inline void read(T &x)
	{
		char c;
		bool f = false;
		x = 0;
		do
			c = getchar();
		while (c != '-' && !isdigit(c));
		if (c == '-')
			f = true, c = getchar();
		do
			x = x * 10 + c - '0', c = getchar();
		while (isdigit(c));
		if (f)
			x = -x;	
	}	
	template<typename T>
	inline void write(T x)
	{
		static char buf[30];
		char *pos = buf;
		if (x < 0)
			putchar('-'), x = -x;
		do
			*pos++ = x % 10 + '0';
		while (x /= 10);
		while (pos > buf)
			putchar(*--pos);
	}
	typedef long long ll;
	const int Jumpmelon = 2333, N = 1e5 + 10, K = 6;
	int arr[N][K], p[N], size[N], in[N];
	bool mp[N][N];
	int f(const int x)
	{
		return x == p[x] ? x : p[x] = f(p[x]);	
	}
	int work() 
	{
		int n, k;
		read(n), read(k);
		if (k == 1)
		{
			for (int i = 1; i <= n; i++)
				write(1), putchar(' ');
			return 0;	
		}
		for (int i = 1; i <= n; i++)
		{
			for (int j = 1; j <= k; j++)
				read(arr[i][j]);
			p[i] = i;
			size[i] = 1;
			for (int j = 1; j < i; j++)
			{
				int b = f(j);
				if (mp[i][b] && mp[b][i])
					continue;
				for (int x = 1; x <= k; x++)
					if (arr[j][x] < arr[i][x])
					{
						if (!mp[i][b])
							mp[i][b] = true, in[b]++;//add(f(i), f(j))
					}
					else if (!mp[b][i])
						mp[b][i] = true, in[i]++;//add(f(j), f(i))
			}
			while (1)
			{
				bool flag = false;
				for (int j = 1; j < i; j++)
				{
					int a = f(i), b = f(j);
					if (a == b)
						continue;
					if (mp[a][b] && mp[b][a])
					{
						flag = true;
						size[b] += size[a];//add a to b
						p[a] = b;
						for (int x = 1; x <= i; x++)
						{
							if (f(x) != x)
								continue;
							if (mp[a][x])
							{
								if (mp[b][x] || x == b)
									in[x]--;
								else
									mp[b][x] = true;
							}
							if (mp[x][a])
							{
								if (mp[x][b])
									in[b]--;
								else
									mp[x][b] = true;
							}
						}
					}
				}
				if (!flag)
					break;		
			}
			int ans = 0;
			for (int j = 1; j <= i; j++)
				if (f(j) == j && !in[j])
					ans += size[j];
			write(ans), putchar(' ');
		}
		return 0;
	}
}
int main()
{
	freopen("s.in", "r", stdin);
	freopen("s.out", "w", stdout);
	return zuiyt::work();	
}
