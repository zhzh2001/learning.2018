#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10,MOD=1e9+7;
int jie[N],inv[N];
inline int mi(int a,int b){
	int x=1,y=a;
	for(;b;b>>=1,y=y*y%MOD)if(b&1)x=x*y%MOD;
	return x;
}
inline int C(int x,int y){
	return jie[x]*inv[y]%MOD*inv[x-y]%MOD;
}
int n,m;
signed main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	jie[0]=1;
	for(int i=1;i<=N-10;i++)jie[i]=jie[i-1]*i%MOD;
	inv[N-10]=mi(jie[N-10],MOD-2);
	for(int i=N-11;i>=0;i--)inv[i]=inv[i+1]*(i+1)%MOD;
	n=read();m=read();
	int ans=1;
	for(int i=1;i<=m;i++){
		int l=read(),r=read(),x=read();
		int p=(C(r+1,l-x+1)-C(l,l-x+1)+MOD)%MOD;
		ans=ans*p%MOD;
	}
	writeln(ans);
	return 0;
}
