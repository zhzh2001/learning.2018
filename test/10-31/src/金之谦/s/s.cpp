#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int n,K,a[N][10],ma[N][10],b[10],ans=0;
int fa[N],s[N];
inline int find(int x){
	return fa[x]==x?x:fa[x]=find(fa[x]);
}
inline void Main(){
	for(int i=1;i<=n;i++)write(1),putchar(' ');
	exit(0);
}
int main()
{
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read();K=read();
	for(int i=1;i<=n;i++)fa[i]=i,s[i]=1;
	if(K==1)Main();
	int pt=1;
	for(int i=1;i<=n;i++){
		int ks=0,flag=0;
		for(int j=1;j<=K;j++){
			a[i][j]=ma[i][j]=read();
			if(a[i][j]>a[pt][j])flag=1;
			if(a[i][j]>b[j])ks++,b[j]=a[i][j];
		}
		if(ks==K){
			pt=i;
			write(1);putchar(' ');
			continue;
		}
		if(flag){
			fa[find(i)]=pt;s[pt]++;
			for(int j=1;j<=K;j++){
				a[pt][j]=min(a[pt][j],a[i][j]);
				ma[pt][j]=max(ma[pt][j],ma[i][j]);
			}
			for(int k=1;k<i;k++){
				int fx=find(k);
				if(fx==k&&fx!=pt){
					int flag=0;
					for(int j=1;j<=K;j++)if(a[i][j]<ma[k][j])flag=1;
					if(flag){
						fa[k]=pt,s[pt]+=s[fx];
						for(int j=1;j<=K;j++){
							a[pt][j]=min(a[pt][j],a[k][j]);
							ma[pt][j]=max(ma[pt][j],ma[k][j]);
						}
					}
				}
			}
		}
		write(s[pt]);putchar(' ');
	}
	return 0;
}
