#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int MOD=2333;
int n,m,s,t,l,r;
struct juzhen{
	int a[45][45];
	inline void clear(){
		memset(a,0,sizeof a);
	}
	inline void pre(){
		for(int i=1;i<=n;i++)a[i][i]=1;
	}
}cs,ans,ks,sk;
inline juzhen cheng(juzhen a,juzhen b){
	juzhen c;c.clear();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=1;k<=n;k++)c.a[i][j]=(c.a[i][j]+a.a[i][k]*b.a[k][j]%MOD)%MOD;
	return c;
}
inline juzhen mi(juzhen a,int b){
	juzhen x,y=a;x.clear();x.pre();
	for(;b;b>>=1,y=cheng(y,y))if(b&1)x=cheng(x,y);
	return x;
}
struct ppap{int s,t,l,r,i;}p[45];
int anss[45];
inline bool cmp(ppap a,ppap b){return a.l<b.l;}
int main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read();m=read();
	cs.clear();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		cs.a[x][y]=cs.a[y][x]=1;
	}
	int T=read();
	for(int i=1;i<=T;i++){
		p[i].s=read();p[i].t=read();p[i].l=read();p[i].r=read();
		p[i].i=i;
	}
	sort(p+1,p+T+1,cmp);
	int la=0;sk.clear();sk.pre();
	for(int k=1;k<=T;k++){
		s=p[k].s,t=p[k].t,l=p[k].l,r=p[k].r;
		ans.clear();ans.a[1][s]=1;
		sk=cheng(sk,mi(cs,l-la));
		ans=cheng(ans,sk);
		la=l;
		int sum=0;
		for(int i=l;i<=r;i++){
			sum=(sum+ans.a[1][t])%MOD;
			ks.clear();
			for(int j=1;j<=n;j++)
				for(int k=1;k<=n;k++)ks.a[1][j]=(ks.a[1][j]+ans.a[1][k]*cs.a[k][j]%MOD)%MOD;
			ans=ks;
		}
		anss[p[k].i]=sum;
	}
	for(int i=1;i<=T;i++)writeln(anss[i]);
	return 0;
}
