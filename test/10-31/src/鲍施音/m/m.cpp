#include <iostream>
#include <cstdio>

using namespace std;
const int mod=1000000000+7;

int read()
{
	char c;int num,f=1;
	while(c=getchar(),c<'0' || c>'9') if(c=='-') f=-1;num=c-'0';
	while(c=getchar(),c>='0' && c<='0') num=num*10+c-'0';
	return f*num;
}
int n,m,sum[100010],ans=1;
struct node
{
	int k,l,r;
}qaq[100010];
int po(int x)
{
	int num=1;
	for(int i=2;i<=x;i++)
		num*=i;
	return num;
}

int main() 
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++)
	{
		qaq[i].l=read();qaq[i].r=read();qaq[i].k=read();
		for(int j=1;j<i;j++)
		{
			if(qaq[j].l==qaq[i].l)
			{
				if(qaq[j].r<=qaq[i].r)
				    qaq[j].l=0;
				else
				    qaq[i].l=0;
			}
		}
	}
	for(int i=1;i<=m;i++)
	{
		if(qaq[i].l==0)
		    continue;
		for(int j=qaq[i].l;j<=qaq[i].r;j++)
		{
			int a=qaq[i].k+j-qaq[i].l;
			sum[i]+=po(j)/(po(a)*po(j-a));
		}
		ans*=sum[i];
		ans%=mod;
	}
	cout<<ans<<endl;
	return 0;
}
