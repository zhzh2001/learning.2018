#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

int read()
{
	char c;int num,f=1;
	while(c=getchar(),c<'0' || c>'9') if(c=='-') f=-1;num=c-'0';
	while(c=getchar(),c>='0' && c<='0') num=num*10+c-'0';
	return f*num;
}
int n,k,a[100010][6],num[10],sum[100010];

int main()
{
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read(),k=read();
	for(int i=1;i<=k;i++)
		num[i]=1;
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=k;j++)
		    a[i][j]=read();
		if(i==1)
		{
			cout<<"1 ";
			continue;
		}
		int ans=0;
		memset(sum,0,sizeof(sum));
		for(int z=1;z<=k;z++)
			if(a[i][z]>num[z])
			{
				num[z]=i;
				sum[num[z]]++;
			}
		int maxx=sum[1];
		for(int j=1;j<=i;j++)
		{
			if(sum[j]==maxx)
			    ans++;
			else if(sum[j]>maxx)
			{
				maxx=sum[j];
				ans=1;
			}
		}
		cout<<ans<<" ";
	}
	return 0;
}
