#include <iostream>
#include <cstdio>
#include <cstring>
#define INF 0x3f3f3f3f

using namespace std;
const int mod=2333;

int read()
{
	char c;int num,f=1;
	while(c=getchar(),c<'0' || c>'9') if(c=='-') f=-1;num=c-'0';
	while(c=getchar(),c>='0' && c<='0') num=num*10+c-'0';
	return f*num;
}
int n,m,d[50][50],q,ans,l,r,tim;
void dfs(int s,int t,int ste)
{
	if(s==t && tim>=l && tim<=r)
        ans++;
	else if(s!=t && tim>r)
	{
		tim-=ste-1;
		return ;
	}
	for(int i=1;i<=n;i++)
	    if(d[s][i]!=INF)
		{
			tim++;
			dfs(i,t,ste+1);
		}
}

int main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read(),m=read();
	memset(d,INF,sizeof(d));
	for(int i=1;i<=m;i++)
	{
		int a,b;
		a=read(),b=read();
		d[a][b]=d[b][a]=1;
	}
	q=read(); 
	for(int i=1;i<=q;i++)
	{
		int s,t;
		s=read(),t=read(),l=read(),r=read();
		ans=0,tim=0;
		dfs(s,t,0);
		cout<<ans<<endl;
	}
	return 0;
}
