#include <cstdio>
using namespace std;
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
void write(int x) {if (x<0) x=-x,putchar('-');if (x>=10) write(x/10);putchar(x%10|'0');}
void wln(int x) {write(x);puts("");}
void wsp(int x) {write(x);putchar(' ');}
const int N=1e5+7,P=1e9+7;
int n,m,ans=1;
int jc[N]={1},inv[N];
int ksm(int a,int b) {
	int s=1;
	for (;b;b>>=1,a=1ll*a*a%P) if (b&1) s=1ll*s*a%P;
	return s;
}
int C(int n,int m) {return 1ll*jc[n]*inv[m]%P*inv[n-m]%P;}
int main() {
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n;i++) jc[i]=1ll*jc[i-1]*i%P;
	inv[n]=ksm(jc[n],P-2);
	for (int i=n-1;~i;i--) inv[i]=1ll*inv[i+1]*(i+1)%P;
	for (int i=1,l,r,k,now;i<=m;i++) {
		l=read(),r=read(),k=read();k=l-k;
		now=0;
		for (int j=l;j<=r;j++) now=(now+C(j,k))%P;
		ans=1ll*ans*now%P;
	}
	wln(ans);
}
