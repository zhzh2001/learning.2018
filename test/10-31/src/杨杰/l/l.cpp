#include <cstdio>
#include <cstring>
using namespace std;
const int N=50,P=2333;
struct edge {
	int t,nxt;
}e[2007];
int n,m,Q,cnt,ans,now;
int head[N],f[2][N];
void add(int u,int t) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;
}
int main() {
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,u,v;i<=m;i++) scanf("%d%d",&u,&v),add(u,v),add(v,u);
	scanf("%d",&Q);
	while (Q--) {
		int S,T,l,r;
		memset(f,0,sizeof f);ans=0;
		scanf("%d%d%d%d",&S,&T,&l,&r);
		f[0][S]=1;now=0;
		for (int t=1;t<=r;t++) {
			now=!now;
			for (int i=1;i<=n;i++) {
				f[now][i]=0;
				for (int j=head[i];j;j=e[j].nxt) {
					int t=e[j].t;
					f[now][i]=(f[now][i]+f[!now][t])%P;
				}
			}
			if (t>=l) ans=(ans+f[now][T])%P;
		}
		printf("%d\n",ans);
	}
}
