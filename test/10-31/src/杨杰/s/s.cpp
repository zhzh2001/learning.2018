#include <cstdio>
#include <set>
using namespace std;
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
void write(int x) {if (x<0) x=-x,putchar('-');if (x>=10) write(x/10);putchar(x%10|'0');}
void wln(int x) {write(x);puts("");}
void wsp(int x) {write(x);putchar(' ');}
const int N=1e5+7;
set<int> s;
set<int>::iterator it;
int n,k,tp;
int f[N][6],st[N];
int main() {
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read();k=read();
	for (int i=1;i<=n;i++) for (int j=1;j<=k;j++) f[i][j]=read();
	for (int i=1;i<=n;i++) {
		int p=1;
		for (it=s.begin();it!=s.end();it++) {
			int id=*it,ck1=0,ck2=0;
			for (int j=1;j<=k;j++) if (f[i][j]>f[id][j]) ck1=1;else ck2=1;
			if (!ck1) {p=0;break;}if (!ck2) st[++tp]=id;
		}
		while (tp) s.erase(st[tp]),tp--;
		if (p) s.insert(i);
		wsp(s.size());
	}
}
