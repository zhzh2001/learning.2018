#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
const int MAXN=1e5+10;
const int mod=1e9+7;
namespace __C{
ll a[MAXN],b[MAXN];
void init(int X) {
	a[0]=a[1]=b[0]=b[1]=1;
	for(int i=2; i<=X; i++) b[i]=b[i-1]*i%mod;
    for(int i=2; i<=X; i++) a[i]=(mod-mod/i)*a[mod%i]%mod;
    for(int i=2; i<=X; i++) a[i]=a[i-1]*a[i]%mod;
}
ll C(int x,int y) {
    if(x<y) return 0;
    else if(x<mod) return b[x]*a[y]%mod*a[x-y]%mod;
    else return C(x/mod,y/mod)*C(x%mod,y%mod)%mod;
}	
}
using __C::C;
//defs========================

//ll C[3090][3090];
int N,M;
ll ans=1;
ll s[3090][3090];
//main========================
int main() {
//freopen("m.in","r",stdin);
	__C::init(MAXN-5);
	N=rd(),M=rd();
	for(int i=0;i<=N;++i) {
		for(int j=i;j<=N;++j) {
			s[i][j]=(s[i][j-1]+C(j,i))%mod;
		}
	}
	for(int i=1;i<=M;++i) {
		int l=rd(),r=rd(),k=rd();
		ll t=0;
		t=(s[l-k][r]-s[l-k][l-1]+mod)%mod;
		//printf("t=%lld\n",t);
		ans=(ans*t)%mod;
	}
	printf("%lld\n",ans);
	return 0;
}

