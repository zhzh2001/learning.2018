#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
int N,M,Q;
const int mod=2333;
struct Matrix{
	int a[41][41];
	Matrix() { memset(a,0,sizeof a); }
	Matrix operator *(Matrix &o) {
		Matrix ret;
		for(int k=1;k<=N;++k) for(int i=1;i<=N;++i) for(int j=1;j<=N;++j) {
			ret.a[i][j]=(ret.a[i][j]+a[i][k]*o.a[k][j])%mod;
			//if(ret.a[i][j]>1e6) ret.a[i][j]%=mod;
		}
		return ret;
	}
	void Set1() {
		for(int i=1;i<=N;++i) a[i][i]=1;
	}
	void disp() {
		for(int i=1;i<=N;++i,puts(""))
			for(int j=1;j<=N;++j) printf("%d ",a[i][j]);
	}
};
Matrix qpow(Matrix x,ll y) {
	Matrix ans;ans.Set1();
	for(;y;y>>=1,x=x*x) if(y&1) ans=ans*x;
	return ans;
}
//defs========================
Matrix mov,res;
//main========================
int main() {
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	N=rd(),M=rd();
	for(int i=1;i<=M;++i) {
		int x=rd(),y=rd();
		mov.a[y][x]=mov.a[x][y]=true;
	}
//	mov.disp();
//	(mov*mov).disp();
	Q=rd();
	int S,T,L,R;
	while(Q--) {
		S=rd(),T=rd(),L=rd(),R=rd();

		res=qpow(mov,L);
//		res.disp();
		int ans=res.a[S][T];
		for(int i=L+1;i<=R;++i) {
			res=res*mov;
			ans=(ans+res.a[S][T])%mod;
		}
		printf("%d\n",ans%mod);
	}
	return 0;
}

