#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
//defs========================
const int MAXN=1e5+10;
int N,K;
int a[MAXN][6];
namespace sub_10{
	void main() {
		for(int i=1;i<=N;++i) {
			printf("%d ",1);
		}
		exit(0);
	}
}
namespace sub_30 {
	int fa[MAXN],sz[MAXN],lev[MAXN];
	int get_fa(int x) {
		return fa[x]?get_fa(fa[x]):x;
	}
	void join(int x,int y) {
		int fx=get_fa(x),fy=get_fa(y);
		if(fx!=fy) {
			if(sz[fx]>sz[fy]) {
				fa[fy]=fx;
				sz[fx]+=sz[fy];
				lev[fx]=max(lev[fx],lev[fy]);
			} else {
				fa[fx]=fy;
				sz[fy]+=sz[fx];
				lev[fy]=max(lev[fx],lev[fy]);
			}
		}
	}
	int get_re(int x,int y) {
		bool big=0,sml=0;
		for(int i=0;i<K;++i) if(a[x][i]>a[y][i]) big=1; else sml=1;
		if(big&&sml) return 2;//mutual
		if(big) return 1;//big
		if(sml) return 0; 
		return -1;
	}
	void main() {
		for(int i=1;i<=N;++i) sz[i]=1;
		printf("1 ");
		for(int i=2;i<=N;++i) {
			int biggest=1;
			for(int j=1;j<i;++j) {
				int rela=get_re(i,j);
				int fi=get_fa(i),fj=get_fa(j);
				//printf("%d->%d rela=%d\n",i,j,rela);
				if(rela==2) {
					join(i,j);
				} else if(rela==1&&fj==biggest) {
					biggest=fi;
				} 	
			}
			printf("%d ",sz[get_fa(i)]);
			//if(i>2000) exit(0);
		}
	}
}
//main========================
int main() {
//	freopen("s.in","r",stdin);
//	freopen("s.out","w",stdout);
	N=rd(),K=rd();
	for(int i=1;i<=N;++i) for(int j=0;j<K;++j) a[i][j]=rd();
	if(K==1) sub_10::main();
	if(N<=3000) sub_30::main();
	return 0;
}

