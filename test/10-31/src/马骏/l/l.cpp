#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 50
#define maxm 5000
#define maxlr 100010
#define MOD 2333
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int n,m,s,t,l,r,q,bb[maxm<<1][2],h[maxn],rr,vis[maxn][maxlr],f[maxn][maxlr];
queue<pair<int,int> > qe;
void add(int x,int y)
{
	bb[++rr][0]=y;
	bb[rr][1]=h[x];
	h[x]=rr;
}
signed main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=m;i++)
	{
		int x=read(),y=read();
		add(x,y);
		add(y,x);
	}
	for(q=read();q--;)
	{
		s=read();
		t=read();
		l=read();
		r=read();
		for(;!qe.empty();qe.pop());
		memset(vis,0,sizeof vis);
		qe.push(make_pair(s,0));
		memset(f,0,sizeof f);
		f[s][0]=1;
		while(!qe.empty())
		{
			pair<int,int> pp=qe.front();
			vis[pp.first][pp.second]=0;
			qe.pop();
			if(pp.second>=r) continue;
			for(int i=h[pp.first];i;i=bb[i][1])
			{
				(f[bb[i][0]][pp.second+1]+=f[pp.first][pp.second])%=MOD;
				if(!vis[bb[i][0]][pp.second+1])
				{
					vis[bb[i][0]][pp.second+1]=1;
					qe.push(make_pair(bb[i][0],pp.second+1));
				}
			}
		}
		int ans=0;
		for(int i=l;i<=r;i++)
			(ans+=f[t][i])%=MOD;
		wln(ans);
	}
	return 0;
}
