#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100010
#define maxk 5
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int k,n,a[maxn][maxk],vis[maxn],siz[maxn];
struct node
{
	int ma[5],mi[5];
	friend bool operator < (node a,node b)
	{
		for(int i=0;i<k;i++)
			if(a.ma[i]>b.mi[i]) return 0;
		return 1;
	}
};
node haha[maxn];
signed main()
{
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read();
	k=read();
	for(int i=1;i<=n;i++)
		for(int j=0;j<k;a[i][j++]=read());
	if(k==1)
	{
		for(int i=1;i<=n;i++)
			wrs(1);
		return 0;
	}
	for(int i=1;i<=n;i++)
	{
		node now;
		int ss=1;
		for(int j=0;j<k;j++)
			now.ma[j]=now.mi[j]=a[i][j];
		for(int j=1;j<i;j++)
		{
			if(vis[j]) continue;
			int llk1=0,llk2=0;
			for(int kk=0;kk<k;kk++)
			{
				if(now.ma[kk]>haha[j].mi[kk]) llk1=1;
				if(now.mi[kk]<haha[j].ma[kk]) llk2=1;
			}
			if(llk1&&llk2)
			{
				for(int kk=0;kk<k;kk++)
				{
					now.ma[kk]=max(now.ma[kk],haha[j].ma[kk]);
					now.mi[kk]=min(now.mi[kk],haha[j].mi[kk]);
				}
				vis[j]=1;
				ss+=siz[j];
			}
		}
		siz[i]=ss;
		for(int j=0;j<k;j++)
		{
			haha[i].mi[j]=now.mi[j];
			haha[i].ma[j]=now.ma[j];
		}
		int lala;
		for(int j=1;j<=i;j++)
			if(!vis[j])
			{
				lala=j;
				break;
			}
		for(int j=1;j<=i;j++)
		{
			if(vis[j]) continue;
			if(haha[lala]<haha[j]) lala=j;
		}
		wrs(siz[lala]);
	}
	return 0;
}
