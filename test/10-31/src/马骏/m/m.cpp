#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100010
#define MOD 1000000007
#define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int ni[maxn],n,m,jcn[maxn],jc[maxn],ans;
void init(int n,int mod)
{
	ni[0]=0;
	ni[1]=1;
	for(int i=2;i<=n;i++)
		ni[i]=(-(mod/i)*ni[mod%i]%mod+mod)%mod;
}
int calc(int n,int m)
{
	if(m<0||n<0||m>n) return 0;
	return jc[n]*jcn[m]%MOD*jcn[n-m]%MOD;
}
signed main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read()+1;
	m=read();
	jc[0]=1;
	for(int i=1;i<=n;i++)
		jc[i]=jc[i-1]*i%MOD;
	init(n,MOD);
	jcn[0]=1;
	for(int i=1;i<=n;i++)
		jcn[i]=jcn[i-1]*ni[i]%MOD;
	ans=1;
	for(int i=1;i<=m;i++)
	{
		int l=read(),r=read(),k=read();
		(ans*=calc(r+1,k+r-l)-calc(l,k-1))%=MOD;
	}
	write((ans%MOD+MOD)%MOD);
	return 0;
}
