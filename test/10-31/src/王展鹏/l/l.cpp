#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
inline void write(int x){
	if(x<0) putchar('-'),x=-x;
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
const int N=41,mod=2333;
struct matrix{
	int a[N][N];
	inline int* operator [](int x){
		return a[x];
	}
}e,zs;
int n,m,q;
matrix operator *(matrix a,matrix b){
	matrix zs;
	memset(zs.a,0,sizeof(zs.a));
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++)if(a.a[i][j]){
			for(int k=1;k<=n;k++){
				zs.a[i][k]+=a.a[i][j]*b.a[j][k];
			}
		}
	}
	for(int i=1;i<=n;i++)for(int j=1;j<=n;j++)zs[i][j]%=mod;
	return zs;
}
matrix ksm(matrix zs,int s,int k){
	matrix ans; memset(ans.a,0,sizeof(ans.a));
	ans[1][s]=1;
	for(;k;k>>=1){
		if(k&1)ans=ans*zs;
		zs=zs*zs;
	}
	return ans;
}
int main(){
	freopen("l.in","r",stdin); freopen("l.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		e[x][y]=e[y][x]=1;
	}
	q=read();
	while(q--){
		int s=read(),t=read(),l=read(),r=read(),ans=0;
		zs=ksm(e,s,l);
		for(int i=l;i<=r;i++){
			ans+=zs[1][t];
			zs=zs*e;
		}
		cout<<ans%mod<<endl;
	}
	return 0;
}
/*
q*n^3*log l
+q*n^2*200
*/

