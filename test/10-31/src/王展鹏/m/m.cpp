#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
inline void write(int x){
	if(x<0) putchar('-'),x=-x;
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
const int N=100005,mod=1000000007;
ll fac[N],ni[N];
int n,m;
int c(int x,int y){
	return fac[x]*ni[y]%mod*ni[x-y]%mod;
}
int f(int x,int y){
	if(y==-1)return 0;
	return c(x+1,y);
}
inline ll ksm(ll a,int b){
	int ans=1;
	for(;b;b>>=1){
		if(b&1)ans=ans*a%mod;
		a=a*a%mod;
	}
	return ans;
}
int main(){
	freopen("m.in","r",stdin); freopen("m.out","w",stdout);
	for(int i=fac[0]=1;i<N;i++)fac[i]=fac[i-1]*i%mod; ni[N-1]=ksm(fac[N-1],mod-2);
	for(int i=N-1;i;i--)ni[i-1]=ni[i]*i%mod;
	n=read(); m=read();
	ll ans=1;
	while(m--){
		int l=read(),r=read(),k=read();
		ans=ans*(f(r,k+r-l)-f(l-1,k-1)+mod)%mod;
	}
	cout<<ans<<endl;
	return 0;
}
/*
(i,0)+(i+1,1)+(i+2,2)+(i+3,3)+(i+k,k)
i!*(i+1)!*(i+2)!...
1!*2!...k!
000000
000000
000000
000000
000000
*/
