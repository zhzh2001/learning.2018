#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
inline void write(int x){
	if(x<0) putchar('-'),x=-x;
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
struct data{
	int mx[5],mn[5],sz;
}a;
data operator +(data a,data b){
	for(int i=0;i<5;i++){
		a.mx[i]=max(a.mx[i],b.mx[i]);
		a.mn[i]=min(a.mn[i],b.mn[i]);
	}
	a.sz+=b.sz;
	return a;
}
bool operator <(data a,data b){
	for(int i=0;i<5;i++)if(a.mx[i]>b.mn[i])return 1;
	return 0;
}
set<data> s;
set<data>::iterator it;
int n,k;
int main(){
	freopen("s.in","r",stdin); freopen("s.out","w",stdout);
	n=read(); k=read();
	for(int i=1;i<=n;i++){
		for(int j=0;j<k;j++)a.mx[j]=a.mn[j]=read();
		a.sz=1;
		for(it=s.upper_bound(a);it!=s.end()&&a<*it&&*it<a;){
			a=a+*it; s.erase(it++);
		}
		s.insert(a);
		write((*s.begin()).sz); putchar(' ');
	}
	return 0;
}


