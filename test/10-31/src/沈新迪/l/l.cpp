#include<bits/stdc++.h>
using namespace std;
int read()
{
	char ch=getchar();int x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return ff*x;
}
void write(int aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar('0'+aa%10);
	return;
}
const int mod=2333;
int n,m,q,sum;
int mul[45][45],chen[45][45],ans[2][45],tmp[45][45];
void ksm(int bb)
{
	while(bb)
	{
		if(bb&1) 
		{
			for(int i=1;i<=1;++i) for(int j=1;j<=n;++j) for(int k=1;k<=n;++k)
			tmp[i][j]=(tmp[i][j]+ans[i][k]*mul[j][k]%mod)%mod;
			for(int i=1;i<=1;++i) for(int j=1;j<=n;++j)
			ans[i][j]=tmp[i][j],tmp[i][j]=0;
		}
		bb>>=1;
		for(int i=1;i<=n;++i) for(int j=1;j<=n;++j) for(int k=1;k<=n;++k)
		tmp[i][j]=(tmp[i][j]+mul[i][k]*mul[k][j]%mod)%mod;
		for(int i=1;i<=n;++i) for(int j=1;j<=n;++j)
		mul[i][j]=tmp[i][j],tmp[i][j]=0;
	}
	return;
}
int main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;++i)
	{
		int x=read(),y=read();
		chen[x][y]=1;chen[y][x]=1;
	}
	q=read();
	while(q--)
	{
		memset(ans,0,sizeof(ans));
		sum=0;
		int s=read(),t=read(),l=read(),r=read();
		for(int i=1;i<=n;++i) for(int j=1;j<=n;++j) mul[i][j]=chen[i][j];
		ans[1][s]=1;
		ksm(l);
		for(int i=l;i<=r;++i)
		{
			sum=(sum+ans[1][t])%mod;
			if(i==r) continue;
			for(int j=1;j<=1;++j) for(int k=1;k<=n;++k) for(int l=1;l<=n;++l)
			tmp[j][k]=(tmp[j][k]+ans[j][l]*chen[l][k]%mod)%mod;
			for(int j=1;j<=1;++j) for(int k=1;k<=n;++k) 
			ans[j][k]=tmp[j][k],tmp[j][k]=0;
		}
		write(sum);puts("");
	}
	return 0;
}
