#include<bits/stdc++.h>
using namespace std;
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return ff*x;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar('0'+aa%10);
	return;
}
const long long mod=1e9+7;
long long n,m,sum=1;
long long jc[200005],ny[200005];
long long ksm(long long aa,long long bb)
{
	long long ans=1;
	while(bb)
	{
		if(bb&1) ans=(ans*aa)%mod;
		bb>>=1;
		aa=(aa*aa)%mod;
	}
	return ans;
}
int main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read(),m=read();
	jc[0]=1;
	for(long long i=1;i<=n*2;++i) jc[i]=jc[i-1]*i%mod;
	ny[n*2]=ksm(jc[n*2],mod-2);
	for(long long i=n*2;i;i--) ny[i-1]=ny[i]*i%mod;
	while(m--)
	{
		long long l=read(),r=read(),k=read(),i=l-k;
		r=r-i;
		long long tmp=jc[i+r+1]*ny[i+1]%mod*ny[r]%mod;
		l=l-1-i;
		if(l>=0) tmp=(tmp-jc[i+l+1]*ny[i+1]%mod*ny[l]%mod+mod)%mod;
		sum=(sum*tmp)%mod;
	}
	write(sum);
	return 0;
}
