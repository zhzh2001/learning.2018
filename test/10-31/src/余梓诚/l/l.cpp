#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register int f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 45
#define P 2333
int n, m, q;
struct Matrix{
	int r, c;
	long long a[N][N];
	void clear(){
		r = c = 0;
		for (register int i = 0; i < N; ++i)
			for (register int j = 0; j < N; ++j)
				a[i][j] = 0;
	}
	void operator *= (const Matrix &res){
		Matrix ret; ret.clear();
		ret.r = r, ret.c = res.c;
		for (register int i = 1; i <= r; ++i)
			for (register int k = 1; k <= c; ++k)
				for (register int j = 1; j <= res.c; ++j)
					ret.a[i][j] += a[i][k] * res.a[k][j];
		*this = ret;
		for (register int i = 1; i <= r; ++i)
			for (register int j = 1; j <= c; ++j)
				a[i][j] %= P;
	}
}A, G, S;
Matrix qpow(Matrix a, int b){
	Matrix s = a;
	for (--b; b; b >>= 1, a *= a) if (b & 1) s *= a;
	return s;
}
int main(){
	freopen("l.in", "r", stdin);
	freopen("l.out", "w", stdout);
	G.r = G.c = n = read(), m = read();
	for (register int i = 1, u, v; i <= m; ++i)
		u = read(), v = read(), G.a[u][v] = G.a[v][u] = 1;
	q = read(), A.r = 1, A.c = n;
	while (q--){
		register int s = read(), t = read(), l = read(), r = read(), ans = 0;
		A.a[1][s] = 1, S = A, S *= qpow(G, l);
		for (register int i = l; i <= r; ++i) (ans += S.a[1][t]) %= P, S *= G;
		printf("%d\n", ans), A.a[1][s] = 0;
	}
}
