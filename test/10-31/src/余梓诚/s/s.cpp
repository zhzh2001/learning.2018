#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
void judge(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
}
const int N=110000;
int n,a[6][N],k,sum[N],b[N];
bool used[N];
bool check(int x,int y){
	for (int i=0;i<k;i++) if (a[i][x]>a[i][y]) return 1;
	return 0;
}
int main(){
	judge();
	scanf("%d%d",&n,&k);
	if (k==1){
		int Max=0;
		for (int i=1;i<=n;i++) printf("1 ");
		return 0;
	}
	for (int i=1;i<=n;i++){
		for (int j=0;j<k;j++) scanf("%d",&a[j][i]);
	}
	used[1]=1; sum[1]=1;
	for (int i=2;i<=n;i++){
		for (int j=1;j<i;j++){
			if (!used[j]) continue;
			if (check(i,j)) used[i]=1;
			if (!check(j,i)) sum[i]--,used[j]=0;
		}
		sum[i]=sum[i]+sum[i-1]+used[i];
	}
	for (int i=1;i<=n;i++) printf("%d ",sum[i]); puts("");
	return 0;
}
