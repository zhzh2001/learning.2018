#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
const int N=5100;
int n,m,c[N][N];
long long ans=1,sum[N][N];
void judge(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
}
#define mo 1000000007
void init(){
	for (int i=0;i<=n;i++) c[i][0]=c[i][i]=1;
	for (int i=1;i<=n;i++){
		for (int j=1;j<i;j++) c[i][j]=(c[i-1][j]+c[i-1][j-1])%mo;
	}
	for (int i=0;i<=n;i++){
		for (int j=i+1;j<=n;j++) sum[i][j]=(sum[i][j-1]+c[j][j-i])%mo;
	}
}
int main(){
	judge();
	scanf("%d%d",&n,&m);
	init();
	for (int i=1,l,r,k;i<=m;i++){
		scanf("%d%d%d",&l,&r,&k);
		long long t=(sum[l-k][r]-sum[l-k][l-1]+mo)%mo;
		//printf("#%lld\n",t);
		if (k==0) t=(t+1)%mo;
		ans=ans*t%mo;
		//printf("%lld\n",ans);
	}
	printf("%lld\n",ans);
	return 0;
}
