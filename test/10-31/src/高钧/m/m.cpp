#include <bits/stdc++.h>
using namespace std;
#define int long long
const int N=200005,md=1000000007;
int n,m,re=1,jc[N],inv[N],l,r,k;
int ksm(int x,int k){int re=1;while(k){if(k&1)re=re*x%md;x=x*x%md;k>>=1;}return re;}
int C(int n,int m){return jc[n]*inv[m]%md*inv[n-m]%md;}
signed main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	int i; scanf("%lld%lld",&n,&m);jc[0]=1;inv[0]=1;for(i=1;i<=200000;i++){jc[i]=jc[i-1]*i%md;inv[i]=ksm(jc[i],md-2);}
	for(i=1;i<=m;i++)
	{
		scanf("%lld%lld%lld",&l,&r,&k); re=re*(C(r+1,l-k+1)-C(l,l-k+1)+md)%md;
	}printf("%lld\n",re);
}
