const int N=100005,md=1000000007;
int n,m,jc[N],invjc[N];
inline int ksm(int x,int y){int re=1;while(y){if(y&1)re=re*x%md;x=x*x%md;y>>=1;}return re;}
inline int C(int nn,int mm){return jc[nn]*invjc[mm]*invjc[nn-mm];}
scanf("%lld%lld",&n,&m); jc[0]=invjc[0]=1LL;
for(i=1;i<=n;i++){jc[i]=1LL*jc[i-1]*i%md;invjc[i]=ksm(jc[i],md-2);}
