#include <bits/stdc++.h>
using namespace std;
const int N=105,md=23333,M=2005;
int Q,n,m,tot=0,Next[M],to[M],head[M],bo[N];
inline void add(int x,int y){Next[++tot]=head[x];to[tot]=y;head[x]=tot;}
struct rec{int x,w;};
priority_queue<rec>q;
inline bool operator<(const rec &p,const rec &q){return p.w>q.w;}
inline int bfs(int s,int t,int l,int r)
{
	int i,wa=0; q.push((rec){s,0}); rec tmp;
	while(!q.empty())
	{
		tmp=q.top(); q.pop();
		if(tmp.w>r)break; if(tmp.x==t&&tmp.w>=l)wa++;
		for(i=head[tmp.x];i;i=Next[i])
		{
			q.push((rec){to[i],tmp.w+1});
		}
	}return wa;
}
int main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	int i,x,y,z,s,t,l,r,re; scanf("%d%d",&n,&m);
	for(i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y); add(x,y); add(y,x); if(x==y)bo[x]=1;
	}scanf("%d",&Q);
	while(Q--)
	{
		scanf("%d%d%d%d",&s,&t,&l,&r); re=bfs(s,t,l,r); printf("%d\n",re%md);
	}
}
