program mc;
 var
  fac:array[0..100001] of int64;
  i,m,n,mot,x,y,z:longint;
  ssum:int64;
 function hahaksm(x,y:int64):int64;
  begin
   hahaksm:=1;
   while y>0 do
    begin
     if y and 1=1 then hahaksm:=hahaksm*x mod mot;
     x:=x*x mod mot;
     y:=y>>1;
    end;
  end;
 function hahacp(x,y:longint):int64;
  begin
   if x>y then exit(0);
   exit(fac[y]*hahaksm(fac[x],mot-2) mod mot*hahaksm(fac[y-x],mot-2) mod mot);
  end;
 begin
  assign(input,'m.in');
  assign(output,'m.out');
  reset(input);
  rewrite(output);
  mot:=1000000007;
  readln(n,m);
  fac[0]:=1;
  for i:=1 to n+1 do
   fac[i]:=fac[i-1]*i mod mot;
  ssum:=1;
  for i:=1 to m do
   begin
    readln(x,y,z);
    ssum:=ssum*(hahacp(x-z+1,y+1)-hahacp(x-z+1,x)+mot) mod mot;
   end;
  writeln(ssum);
  close(input);
  close(output);
 end.
