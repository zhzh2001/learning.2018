#include<cstdio>
using namespace std;
const long long p=1000000007;
int n,m;
long long c[2010][2010],d[2010][2010],ans;
int main()
  {
   freopen("m.in","r",stdin);
   freopen("m.out","w",stdout);
   
   scanf("%d%d",&n,&m);
   for (int i=0;i<=n;i++) c[i][0]=1;
   for (int i=1;i<=n;i++)
     for (int j=1;j<=i;j++)
       c[i][j]=(c[i-1][j]+c[i-1][j-1])%p;
   for (int i=1-n;i<=1;i++)
     for (int j=1;j<=n;j++)
       {
        int t=i+j-1;long long sum=0;
        if ((t>=0) && (t<=j)) sum=c[j][t];
        d[i+n][j]=(d[i+n][j-1]+sum)%p;
	   }
   ans=1;
   for (int i=1;i<=m;i++)
     {
      int l,r,k;
	  scanf("%d%d%d",&l,&r,&k);
	  long long sum=d[k-l+1+n][r]-d[k-l+1+n][l-1];
	  while (sum<0) sum=sum+p;
	  ans=ans*sum%p;
	 }
   printf("%lld\n",ans);
   return 0;
  }
