#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 2010
using namespace std;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
LL ans,C[N][N],D[N][N];
int n,m,l,r,k;
inline void pre(){
	C[0][0]=1;
	C[1][0]=1;C[1][1]=1;
	rep(i,1,n) C[i][0]=1;
	rep(i,2,n){
		rep(j,1,i-1)
		C[i][j]=(C[i-1][j-1]+C[i-1][j])%mo;
		C[i][i]=1;
	}
}
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read(),m=read();
	ans=1;
	pre();
	rep(i,0,n){
		rep(j,i+1,n)
		D[j][i]=(D[j-1][i]+C[j][j-i])%mo;
	}
	rep(i,1,m){
		l=read(),r=read(),k=read();
		LL sum=0;
		if (l>=k)sum=(D[r][l-k]-D[l-1][l-k]+mo)%mo;
		if (k==0) sum=(sum+1)%mo;
	/*	//if (i==1) printf("%lld %lld\n",D[r][l-k],D[l-1][l-k]);
		LL sum1=0;
		rep(j,l,r){
			if (j<k+j-l) break;
			sum1=(sum1+C[j][k+j-l])%mo;
			//printf("%d %d %lld\n",j,k+j-l,C[j][k+j-l]);
		}
		if (sum1!=sum) printf("%d %lld %lld %d %d %d\n",i,sum,sum1,l,r,k);
		//printf("%lld\n",sum);*/
		ans=(ans*sum)%mo;
	}
	printf("%lld\n",ans);
}

