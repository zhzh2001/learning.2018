#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 101000
using namespace std;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int n,m,a[N][6],tot,sta[N];
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read(),m=read();
	rep(i,1,n){
		rep(j,1,m) a[i][j]=read();
	}
	if (m==1) {
		rep(i,1,n) printf("1 ");
		return 0;
	}
	printf("1 ");
	tot=1;
	sta[tot]=1;
	rep(i,2,n){
		bool flag1=1,flag2=1;
		rep(j,1,tot){
			bool g=0,g1=0;
			int h=sta[j];
			if (h==0) continue;
			rep(k,1,m) {
				if (a[i][k]<a[h][k]) flag2=0,g1=1;
				if (a[i][k]>=a[h][k]) g=1;
			}
			if (!g1) {
				tot--;
				rep(k,j,tot) sta[k]=sta[k+1];
				sta[tot+1]=0;
			}
			if (!g) flag1=0;
		}
		if (flag2) tot=1,sta[tot]=i;
			else if (flag1) tot++,sta[tot]=i;
		printf("%d ",tot);
	}
	return 0;
}
