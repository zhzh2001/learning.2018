#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 2333
#define N 10010
using namespace std;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int To[N<<1],nxt[N<<1],head[N],cnt,n,m,T,s,t,l,r,ans;
inline void add(int u,int v){
	To[++cnt]=v;
	nxt[cnt]=head[u];
	head[u]=cnt;
}
inline void dfs(int u,int now){
	if (now>r) return;
	if (u==t){
		if ((now>=l)&&(now<=r)) ans++,ans%=mo;
		return;
	}
	for (int i=head[u];i;i=nxt[i]){
		dfs(To[i],now+1);
	}
}
int main(){
	freopen("l.in","r",stdin);
	freopen("r.out","e",stdout);
	n=read(),m=read();
	rep(i,1,m){
		int x=read(),y=read();
		add(x,y);if (x!=y) add(y,x);
	}
	T=read();
	rep(i,1,T){
		s=read(),t=read(),l=read(),r=read();
		dfs(s,0);
		printf("%d\n",ans);
	}
	return 0;
}
