#include<cstdio>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e5+5;
int n,k,a[6][N];
inline void init(){
	n=read(); k=read();
	for (int i=1;i<=n;i++){
		for (int j=1;j<=k;j++){
			a[j][i]=read();
		}
	}
}
inline void solve(){
	for (int i=1;i<=n;i++){
		write(1); putchar(' ');
	}
}
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	init(); solve();
	return 0;
}
