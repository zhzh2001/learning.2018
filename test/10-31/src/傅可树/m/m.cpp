#pragma GCC optimize ("O2")
#include<cstdio>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e5+5,mod=1e9+7;
int fac[N];
inline int pow(int x,int k){
	int y=1;
	for (;k;k>>=1,x=x*x%mod) if (k&1) y=y*x%mod;
	return y;
}
inline int inv(int x){
	return pow(x,mod-2);
}
inline int C(int n,int m){
	if (m>n) return 0;
	return fac[n]*inv(fac[m])%mod*inv(fac[n-m])%mod;
}
int n,m;
inline void init(){
	n=read(); m=read();
	fac[0]=1; fac[1]=1;
	for (int i=2;i<N;i++){
		fac[i]=fac[i-1]*i%mod;
	}
}
int ans;
inline void solve(){
	ans=1;
	for (int i=1;i<=m;i++){
		int l=read(),r=read(),k=read();
		k=l-k;
		int tmp1=C(r+1,k+1);
		int tmp2=C(l,k+1);
		ans=ans*(tmp1-tmp2+mod)%mod;
	}
	writeln(ans);
}
signed main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	init();
	solve();
	return 0;
}
