#pragma GCC optimize ("O2")
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=42,mod=2333;
int n,m;
struct matrix{
	int mat[N][N];
	inline void clean(){
		memset(mat,0,sizeof mat);
	}
	inline void one(){
		clean();
		for (int i=1;i<=n;i++){
			mat[i][i]=1;
		}
	}
}ans[N];
matrix A;
inline void update(int &x,int y){
	x=x+y;
	if (x>mod) x-=mod;
}
inline matrix operator *(matrix A,matrix B){
	matrix C; C.clean();
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			for (int k=1;k<=n;k++){
				update(C.mat[i][j],A.mat[i][k]*B.mat[k][j]%mod);
			}
		}
	}
	return C;
}
inline matrix pow(matrix A,int k){
	matrix B; B.one();
	for (;k;k>>=1,A=A*A) if (k&1) B=B*A;
	return B;
}
inline void init(){
	n=read(); m=read();
	for (int i=1;i<=m;i++){
		int u=read(),v=read();
		A.mat[u][v]=1; A.mat[v][u]=1;
	}
}
struct event{
	int x,id;
}q[N*201];
int Q,s[N],t[N],tot,Ans[N];
inline bool cmp(event A,event B){
	return A.x<B.x;
}
inline void solve(){
	Q=read();
	for (int i=1;i<=Q;i++){
		s[i]=read(),t[i]=read(); int l=read(),r=read();
		for (int j=l;j<=r;j++){
			q[++tot]=(event){j,i};
		}
	}
	sort(q+1,q+1+tot,cmp); matrix B=A; int now=1;
	for (int i=1;i<=tot;i++){
		int S=s[q[i].id],T=t[q[i].id];
		while (now<q[i].x) B=B*pow(A,q[i].x-now),now=q[i].x;
		update(Ans[q[i].id],B.mat[S][T]);
	}
	for (int i=1;i<=Q;i++){
		writeln(Ans[i]);
	}
}
int main(){
	freopen("l.in","r",stdin); freopen("l.out","w",stdout);
	init();
	solve();
	return 0;
}
