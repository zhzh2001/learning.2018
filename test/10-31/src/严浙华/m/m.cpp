#include<cstdio>
#include<iostream>
#define maxm 100000
#define ll long long
#define MOD 1000000000 + 7
#define For(i,a,b) for(int i = (int)(a);i <= (int)(b);i++)
using namespace std;
void read(ll &x){
    int f = 1;
    x = 0;
    char s = getchar();
    while(s < '0' || s > '9'){if(s == '-') f = -1;s = getchar();}
    while(s >= '0' && s <= '9'){x = x * 10 + s - '0';s = getchar();}
    x *= f;
}
void write(ll x){
    if(x < 0){putchar('-');x = -x;}
    if(x > 9) write(x / 10);
    putchar(x % 10 + '0');
}
ll n,m,b[maxm],i = 1,ans = 1,l,r,k;
ll zh(int x,int y){
	ll e = 1,f = 1;
	if(x == y)
		return 1;
	else{
		For(i,y + 1,x)
			e *= i;
		e %= MOD;
		For(i,1,x - y)
			f *= i;
		f %= MOD;
		e /= f;
		return e;
	}
}
ll plan(int s,int h,int w){
	ios::sync_with_stdio;
	int plans;
	For(i,s,h){
		if(w + i - s > i)
			plans += 0;
		else
			plans += zh(i,w + i - s);
	}
	return plans %= MOD;
}
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	read(n),read(m);
	For(i,1,m){
		cin>>l>>r>>k;
		ans = ans * plan(l,r,k);
	}
	write(ans);
	return 0;
}
