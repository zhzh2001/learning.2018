#include<cstdio>
#define maxn 45
#define For(i,a,b) for(int i = (int)(a);i <= (int)(b);i++)
int n,m,f[maxn][maxn],a,b,q,s,t,l,r,ans,dis;
void read(int &x){
    int f = 1;
    x = 0;
    char s = getchar();
    while(s < '0' || s > '9'){if(s == '-') f = -1;s = getchar();}
    while(s >= '0' && s <= '9'){x = x * 10 + s - '0';s = getchar();}
    x *= f;
}
void write(int x){
    if(x < 0){putchar('-');x = -x;}
    if(x > 9) write(x / 10);
    putchar(x % 10 + '0');
}
void dfs(int x,int l,int h,int w){
	if(dis >= h && dis <= w && l == t){
		ans++;
		if((w - dis) % 2 == 0 && (w - dis) != 0)
		ans++;
		dis = 0;
	}
	else{
		For(i,1,n){
			if(f[x][i] && x != i){
				dis++;
				dfs(i,l,h,w);
				dis--;
			}
		}
	}
	return;
}
int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	read(n),read(m);
	For(i,1,m){
		read(a),read(b);
		f[a][b] = 1;
		f[b][a] = 1;
	}
	read(q);
	For(i,1,q){
		read(s),read(t),read(l),read(r);
		dfs(s,t,l,r);
		ans %= 2333;
		write(ans);
		printf("\n");
		ans = 0;
	}
	return 0;
}
