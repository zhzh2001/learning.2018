#include <bits/stdc++.h>
#define int long long
using namespace std;
const int mod=2333;
inline int read() {
	int data=0,w=1;
	char ch=0;
	while(ch!='-'&&(ch<'0'||ch>'9'))
		ch=getchar();
	if(ch=='-') {
		w=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9') {
		data=data*10+ch-'0';
		ch=getchar();
	}
	data*=w;
	return data;
}
int head[120000],cnt,n,m,q,s,t,l,r,ans,dis[50][50];
struct node {
	int next,to;
} sxd[240000];
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	head[u]=cnt;
}
void dfs(int at,int zhong,int l,int r,int d) {
	if(at==zhong && d>=l && d<=r) {
		ans++; if(ans==mod) ans=0;
	}
	if(d>=r) return;
	if(dis[at][zhong]+d>r) return;
	for(int i=head[at]; i; i=sxd[i].next) {
		dfs(sxd[i].to,zhong,l,r,d+1);
	}
}
signed main() {
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	cin>>n>>m;
	memset(dis,0x3f,sizeof dis);
	for(int i=1; i<=m; i++) {
		int x=read(),y=read();
		add(x,y);
		add(y,x);
		dis[x][y]=dis[y][x]=1;
	}
	for(int k=1; k<=n; k++)
		for(int i=1; i<=n; i++)
			for(int j=1; j<=n; j++)
				dis[i][j]=min(dis[i][j],dis[i][k]+dis[k][j]);
	cin>>q;
	while(q--) {
		s=read(),t=read(),l=read(),r=read();
		if(l>r) {
			puts("0");
			continue;
		}
		dfs(s,t,l,r,0);
		cout<<ans<<'\n';
	}
	return 0;
}
