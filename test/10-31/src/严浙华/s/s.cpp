#include<cstdio>
#include<iostream>
#include<algorithm>
#define For(i,a,b) for(int i= (int)(a);(int)(i) <= (int)(b);(int)(i)++)
using namespace std;
void read(int &x){
    int f = 1;
    x = 0;
    char s = getchar();
    while(s < '0' || s > '9'){if(s == '-') f = -1;s = getchar();}
    while(s >= '0' && s <= '9'){x = x * 10 + s - '0';s = getchar();}
    x *= f;
}
void write(int x){
    if(x < 0){putchar('-');x = -x;}
    if(x > 9) write(x / 10);
    putchar(x % 10 + '0');
}
int n,k,a[100010][10],b[100010],ans,sl;
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	read(n),read(k);
	if(k == 1){
		For(i,1,n)
			read(b[i]);
		sort(b + 1,b + 1 + n);
		for(int i = n;i > 1;i--){
			if(b[i] == b[i - 1])
				ans++;
			else break;
		}
		ans++;
		write(ans);
		printf("\n");
		return 0;
	}
	For(i,1,n)
		For(j,1,k)
			read(a[i][j]);
	For(z,1,n){
		if(z == 1){
			write(1);
			printf(" ");
		}
		else{
			ans = 0;
			For(i,1,z){
				For(j,1,z)
					For(m,1,k)
						if(a[i][m] > a[j][m]){
							sl++;
							break;
						}
				if(sl == z - 1){
					ans++;
				}
				sl = 0;
			}	
			write(ans);
			printf(" ");
		}
	}
	return 0;
}
