#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10;
ll n,k,ans[N],num,a[N][7];
inline bool tt(ll i,ll j){
	for (ll v=1;v<=k;++v)
		if (a[i][v]>a[j][v]) return 1;
	return 0;
}
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	read(n),read(k);
	for (ll i=1;i<=n;++i) for (ll j=1;j<=k;++j) read(a[i][j]);
	if (k==1){
		ll Ma=-0x3f3f3f3f;
		for (ll i=1;i<=n;++i){
			if (a[i][1]>Ma) Ma=a[i][1],num=0;
			if (a[i][1]==Ma) ++num;
			pc(num==1?'1':'0');
			pc(' ');
		}
		return 0;
	}
	for (ll i=1,l=1;i<=n;++i){
		for (ll j=l;j<i;++j)
			if (tt(i,j)){
				ans[i]=1;
				if (j==l) ans[i]+=ans[l-1],l=1;
				break;
			}
		ans[i]+=ans[i-1];
		bool flag=1;
		for (ll j=l;j<i&&flag;++j) flag&=!tt(j,i);
		if (flag) ans[i]=1,l=i;
	}
	for (ll i=1;i<=n;++i,pc(' ')) wr(ans[i]);
	return 0;
}
//sxdakking
