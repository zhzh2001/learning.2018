#include <iostream>
#include <cstring>
#include <cstdio>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
#define rg register
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll P=2333,N=55;
ll n,m,D[N][N][N],ans,q,f[2][N][N],v;
inline ll DD(ll S,ll T,ll x){
	memset(f[0],0,sizeof f[0]);
	f[v=0][S][S]=1;
	for (rg ll t=0;t<=31;++t) if ((x>>t)&1){
		v^=1;memset(f[v],0,sizeof f[v]);
		for (rg ll k=1;k<=n;++k)
			for (rg ll j=1;j<=n;++j)
				f[v][S][j]=(f[v][S][j]+f[v^1][S][k]*D[k][j][t]%P)%P;
	}
	return f[v][S][T];
}
int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	read(n),read(m);
	for (rg ll i=1,a,b;i<=m;++i){
		read(a),read(b);
		++D[a][b][0];
		if (a^b) ++D[b][a][0];
	}
	for (rg ll t=1;t<=31;++t)
		for (rg ll k=1;k<=n;++k)
			for (rg ll i=1;i<=n;++i)
				for (rg ll j=1;j<=n;++j)
					(D[i][j][t]+=D[i][k][t-1]*D[k][j][t-1]%P)%=P;
	read(q);
	for (rg ll i=1,s,t,l,r;i<=q;++i){
		read(s),read(t),read(l),read(r);
		ans=0;
		for (rg ll j=l;j<=r;++j) ans=(ans+DD(s,t,j))%P;
		wr(ans),puts("");
	}
	return 0;
}
//sxdakking
/*
2 2
1 1
1 2
1
*/
