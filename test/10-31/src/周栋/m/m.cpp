#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll P=1e9+7,N=1e5+10;
ll n,m,ans=1,fac[N],inv[N],sum;
inline ll pow(ll x,ll y){
	ll ret=1;
	for (;y;y>>=1,x=x*x%P) if (y&1) ret=ret*x%P;
	return ret;
}
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	read(n),read(m);
	fac[0]=1;for (ll i=1;i<=n;++i) fac[i]=fac[i-1]*i%P;
	inv[n]=pow(fac[n],P-2);
	for (ll i=n-1;~i;--i) inv[i]=inv[i+1]*(i+1)%P;
	for (ll i=1,l,r,k;i<=m;++i){
		read(l),read(r),read(k);
		sum=0;
		for (ll j=l;j<=r;++j) sum=(sum+fac[j]*inv[j-l+k])%P;
		ans=ans*sum%P*inv[l-k]%P;
		if (!ans) break;
	}
	wr(ans);
	return 0;
}
//sxdakking
