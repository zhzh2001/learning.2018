#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 45,mod = 2333;
int n,m;
int dist1[N],dist2[N];
int mp[N][N];

struct matrix{
	int a[41][41],n;
	matrix(){
		memset(a,0,sizeof a);
	}
	friend matrix operator *(matrix a,matrix b){
		matrix ans; ans.n = a.n;
		for(int i = 1;i <= a.n;++i)
			for(int j = 1;j <= a.n;++j){
				for(int k = 1;k <= a.n;++k)
					ans.a[i][j] += a.a[i][k] * b.a[k][j];
				ans.a[i][j] %= mod;
			}
		return ans;
	}
}a1;

inline matrix qpow(matrix a,int n){
	matrix ans;
	ans.n = a.n;
	for(int i = 1;i <= a.n;++i) ans.a[i][i] = 1;
	while(n){
		if(n & 1) ans = ans * a;
		a = a * a;
		n >>= 1;
	}
	return ans;
}

signed main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n = read(); m = read();
	for(int i = 1;i <= m;++i){
		int x = read(),y = read();
		mp[x][y] = mp[y][x] = 1;
	}
	int q = read();
	while(q--){
		int s = read(),t = read(),l = read(),r = read();
		for(int i = 1;i <= n;++i)
			for(int j = 1;j <= n;++j)
				a1.a[i][j] = mp[i][j];
		for(int i = 1;i <= n;++i) dist1[i] = 0;
		int ans = 0;
		a1.n = n;
		if(l != 0){
			a1 = qpow(a1,l);
			for(int j = 1;j <= n;++j)
				dist1[j] = a1.a[j][s];
		}else dist1[s] = 1;
		r -= l; ans += dist1[t];
		// for(int i = 1;i <= n;++i,puts(""))
		// 	for(int j = 1;j <= n;++j)
		// 		printf("%d ",a1.a[i][j]);
		while(r--){
			for(int i = 1;i <= n;++i){
				dist2[i] = 0;
				for(int j = 1;j <= n;++j)
					if(mp[i][j])
						dist2[i] += dist1[j];
				dist2[i] %= mod;
			}
			for(int i = 1;i <= n;++i)
				dist1[i] = dist2[i];
			ans = (ans+dist1[t])%mod;
		}
		printf("%d\n", ans);
	}
	return 0;
}