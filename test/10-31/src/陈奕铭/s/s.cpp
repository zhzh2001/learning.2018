#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int Mi[6],Mx[6];
int a[N][6];
struct grp{
	int Mi[6],Mx[6];
	int size;
}gs[N];
int n,k,cnt;
int num,top;
typedef pair<int,int> pii;
set<pii> S;

inline int Min(int x,int y){if(x < y) return x; return y;}
inline int Max(int x,int y){if(x > y) return x; return y;}


bool check(int x,int y){
	bool flag1 = false,flag2 = false;
	for(int i = 1;i <= k;++i){
		if(gs[x].Mi[i] > gs[y].Mi[i] && gs[x].Mi[i] < gs[y].Mx[i])
			return true;
		if(gs[x].Mx[i] > gs[y].Mi[i] && gs[x].Mx[i] < gs[y].Mx[i])
			return true;
		if(gs[y].Mi[i] > gs[x].Mi[i] && gs[y].Mi[i] < gs[x].Mx[i])
			return true;
		if(gs[y].Mx[i] > gs[x].Mi[i] && gs[y].Mx[i] < gs[x].Mx[i])
			return true;
		if(gs[x].Mi[i] < gs[y].Mi[i]){
			if(flag2) return true;
			flag1 = true;
		}
		if(gs[x].Mi[i] > gs[y].Mi[i]){
			if(flag1) return true;
			flag2 = true;
		}
	}
	return false;
}


signed main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n = read(); k = read();
	for(int i = 1;i <= n;++i)
		for(int j = 1;j <= k;++j)
			a[i][j] = read();
	printf("%d ",1);
	for(int i = 1;i <= k;++i){
		gs[1].Mi[i] = gs[1].Mx[i] = a[1][i];
		gs[1].size = 1;
	}
	S.insert(make_pair(a[1][1],1));
	for(int i = 2;i <= n;++i){
		for(int j = 1;j <= k;++j){
			gs[i].Mi[j] = gs[i].Mx[j] = a[i][j];
		}
		gs[i].size = 1;
		int pos = 0;
		set<pii> ::iterator it = S.lower_bound(make_pair(a[i][1],i)),it2;
		if(it != S.begin()) it--;
		while(it != S.end()){
			if(check(i,it->second) == 1){
				int x = it->second;
				for(int j = 1;j <= k;++j){
					gs[i].Mi[j] = Min(gs[i].Mi[j],gs[x].Mi[j]);
					gs[i].Mx[j] = Max(gs[i].Mx[j],gs[x].Mx[j]);
				}
				gs[i].size += gs[x].size;
				it2 = it; it++;
				S.erase(it2);
			}
			else{
				++pos;
				if(pos == 2) break;
			}
		}

		it = S.lower_bound(make_pair(gs[i].Mi[1],i));
		if(it != S.end()) it++;
		if(it == S.end() && it != S.begin()) it--;
		pos = 0;
		while(1){
			if(check(i,it->second) == 1){
				int x = it->second;
				for(int j = 1;j <= k;++j){
					gs[i].Mi[j] = Min(gs[i].Mi[j],gs[x].Mi[j]);
					gs[i].Mx[j] = Max(gs[i].Mx[j],gs[x].Mx[j]);
				}
				gs[i].size += gs[x].size;
				it2 = it;
				if(it != S.begin()){
					it--;
					S.erase(it2);
				}else{
					S.erase(it);
					break;
				}
			}
			else{
				++pos;
				if(pos == 2)
					break;
			}
		}
		S.insert(make_pair(gs[i].Mi[1],i));
		printf("%d ",gs[S.rbegin()->second].size);
	}
	puts("");
	return 0;
}


//"font_face": "Menlo",