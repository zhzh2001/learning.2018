#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005,mod = 1e9+7;
int sum[N],rev[N];
int n,m;
int ans;

int qpow(int x,int n){
	int ans = 1;
	while(n){
		if(n&1) ans = 1LL * ans * x % mod;
		x = 1LL * x * x % mod;
		n >>= 1;
	}
	return ans;
}

inline ll C(int n,int m){
	return (1LL * sum[n] * rev[m]) % mod * rev[n-m] % mod;
}

signed main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	sum[0] = 1; for(int i = 1;i <= 100002;++i) sum[i] = 1LL * sum[i - 1] * i% mod;
	rev[100002] = qpow(sum[100002],mod - 2);
	for(int i = 100002;i >= 1;--i) rev[i-1] = 1LL * rev[i] * i % mod;
	n = read(); m = read();
	ans = 1;
	while(m--){
		int l = read(),r = read(),k = read();
		ll num = C(r + 1,l - k + 1) - C(l,l - k + 1);
		// printf("%lld %lld ",C(r + 1,l - k + 1),C(l,l - k + 1));
		// printf("%lld\n", num);
		ans = 1LL * ans * num % mod;
	}
	printf("%d\n", ans);
	return 0;
}