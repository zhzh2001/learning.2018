#include<cstdio>
#include<algorithm>
using namespace std;
int mo=1000000007,n,m,l,r,k;
long long ans,s,x,y,fac[110000],ffac[110000];
int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
void exgcd(long long a,long long b,long long &x,long long &y){
	if(!b)x=1,y=0;
	else{
		exgcd(b,a%b,y,x);
		y-=a/b*x;
	}
}
long long pwr(long long x,int y){
	long long z=1;
	while(y){
		if(y&1)z=z*x%mo;
		x=x*x%mo;
		y>>=1;
	}
	return z;
}
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read();
	m=read();
	ffac[0]=fac[0]=1;
	for(int i=1;i<=n;i++)
		fac[i]=fac[i-1]*i%mo;
	for(int i=1;i<=n;i++)
		ffac[i]=ffac[i-1]*fac[i]%mo;
	ans=1;
	for(int i=1;i<=m;i++){
		l=read();
		r=read();
		k=read();
		s=ffac[r];
		exgcd(ffac[l-1],mo-2,x,y);
		s=s*x%mo;
		s=s*ffac[k-1]%mo;
		exgcd(ffac[k+r-l],mo-2,x,y);
		s=s*x%mo;//printf("%lld\n",k+r-l);
		exgcd(pwr(fac[l-k+1],r-l+1),mo-2,x,y);
		s=s*x%mo;
		s+=mo;
		s%=mo;
		ans=ans*s%mo;
	}
	printf("%lld\n",ans);
	return 0;
}
