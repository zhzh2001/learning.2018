#include<cstdio>
#include<algorithm>
using namespace std;
int mo=1000000007,n,m,l,r,k;
long long ans,s,x,y,fac[110000],ni[110000],a[2100][2100];
int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
void exgcd(long long a,long long b,long long &x,long long &y){
	if(!b)x=1,y=0;
	else{
		exgcd(b,a%b,y,x);
		y-=a/b*x;
	}
}
long long pwr(long long x,int y){
	long long z=1;
	while(y){
		if(y&1)z=z*x%mo;
		x=x*x%mo;
		y>>=1;
	}
	return z;
}
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read();
	m=read();
	fac[0]=1;
	for(int i=1;i<=n;i++)
		fac[i]=fac[i-1]*i%mo;
	ni[n]=pwr(fac[n],mo-2);
	for(int i=n-1;i>0;i--)
		ni[i]=ni[i+1]*(i+1)%mo;
	ni[0]=1;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=n;j++){
			a[i][j]=fac[i];
			if(i>=j)a[i][j]=a[i][j]*ni[i-j]%mo;
			a[i][j]=(a[i-1][j]+a[i][j])%mo;
		}
	ans=1;
	for(int i=1;i<=m;i++){
		l=read();
		r=read();
		k=read();
		s=a[r][l-k]-a[l-1][l-k];//printf("%lld\n",s);
		if(s<0)s+=mo;
//		exgcd(pwr(fac[l-k],r-l+1),mo,x,y);
		s=s*pwr(fac[l-k],mo-2)%mo;
		if(s<0)s+=mo;
		ans=ans*s%mo;
	}
	printf("%lld",ans);
	return 0;
}
