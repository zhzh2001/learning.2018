#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int mo=2333,n,m,q,u,v,tot,sta,enn,ll,rr,st[8100],en[8100],b[8100],id[8100],p[41][41],a[41][41],c[41][41],s[41][41],an[41];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
void sor(int l,int r){
	int i=l,j=r,x=b[(l+r)>>1];
	while(i<=j){
		while(b[i]<x)i++;
		while(x<b[j])j--;
		if(i<=j){
			swap(st[i],st[j]);
			swap(en[i],en[j]);
			swap(b[i],b[j]);
			swap(id[i],id[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}
void cheng(int x){
	while(x){
		if(x&1){
			memset(c,0,sizeof(c));
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
					for(int k=1;k<=n;k++)
						c[i][j]+=s[i][k]*a[k][j];
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
					s[i][j]=c[i][j]%mo;
		}
		if(x==1)return;
		memset(c,0,sizeof(c));
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				for(int k=1;k<=n;k++)
					c[i][j]+=a[i][k]*a[k][j];
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				a[i][j]=c[i][j]%mo;
		x>>=1;
	}
}
int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++)
		s[i][i]=1;
	for(int i=1;i<=m;i++){
		u=read();
		v=read();
		p[u][v]=1;
		p[v][u]=1;
	}
	q=read();
	for(int i=1;i<=q;i++){
		sta=read();
		enn=read();
		ll=read();
		rr=read()-ll;
		for(int j=0;j<=rr;j++){
			tot++;
			st[tot]=sta;
			en[tot]=enn;
			b[tot]=ll+j;
			id[tot]=i;
		}
	}
	sor(1,tot);
	for(int i=1;i<=tot;i++){
		for(int j=1;j<=n;j++)
			for(int k=1;k<=n;k++)
				a[j][k]=p[j][k];
		cheng(b[i]-b[i-1]);
		an[id[i]]+=s[st[i]][en[i]];
//		printf("%d\n",b[i]);
//		printf("%d\n%d %d\n%d %d\n",b[i],s[1][1],s[1][2],s[2][1],s[2][2]);
	}
	for(int i=1;i<=q;i++)
		printf("%d\n",an[i]%mo);
}/*
0 1
1 0

1 0
0 1

0 1
1 0
*/
