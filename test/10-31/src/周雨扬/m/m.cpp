#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define ll long long
using namespace std;
const int mo=1000000007;
inline int read(){
	int x=0;
	#define gc getchar
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
const int N=100005;
int n,Q,fac[N],inv[N];
int C(int x,int y){
	if (x<y) return 0;
	return 1ll*fac[x]*inv[y]%mo*inv[x-y]%mo;
}
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	fac[0]=inv[0]=inv[1]=1;
	For(i,2,N-1) inv[i]=1ll*(mo-mo/i)*inv[mo%i]%mo;
	For(i,1,N-1){
		fac[i]=1ll*fac[i-1]*i%mo;
		inv[i]=1ll*inv[i-1]*inv[i]%mo;
	}
	n=read(); Q=read();
	int ans=1;
	while (Q--){
		int l=read(),r=read(),k=read();
		ans=1ll*ans*(C(r+1,l-k+1)-C(l,l-k+1)+mo)%mo;
	}
	printf("%d",ans);
}

