#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define ll long long
using namespace std;
int n,m;
const int mo=2333;
struct Mat{
	int a[82][82];
	Mat(){
		memset(a,0,sizeof(a));
	}
}a,p[35];
Mat operator *(const Mat &a,const Mat &b){
	Mat c;
	For(i,1,n) For(j,1,n) if (a.a[i][j])
		For(k,1,2*n) c.a[i][k]+=a.a[i][j]*b.a[j][k];
	For(i,1,n) For(j,n+1,2*n) if (a.a[i][j])
		c.a[i][j]+=a.a[i][j]*b.a[j][j];
	For(i,n+1,2*n)
		c.a[i][i]+=a.a[i][i]*b.a[i][i];
	For(i,1,2*n) For(j,1,2*n) c.a[i][j]%=mo;
	return c;
}
int calc(int s,int t,int l){
	Mat ans;
	ans.a[s][s]=1;
	for (int i=0;l;i++,l/=2)
		if (l&1) ans=ans*p[i];
	return ans.a[s][t+n];
}
int main(){
	freopen("l.in","w",stdout);
	printf("40 %d\n",40*41/2);
	For(i,1,40) For(j,i,40) printf("%d %d\n",i,j);
	printf("%d\n",40);
	For(i,1,40) printf("%d %d %d %d\n",rand()%40+1,rand()%40+1,(1<<28)-1,(1<<29)-1);
}
