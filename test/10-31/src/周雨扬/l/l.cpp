#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define ll long long
using namespace std;
int n,m;
const int mo=2333;
struct Mat{
	int a[82][82];
	Mat(){
		memset(a,0,sizeof(a));
	}
}a,p[35];
Mat operator *(const Mat &a,const Mat &b){
	Mat c;
	For(i,1,n) For(j,1,n) if (a.a[i][j])
		For(k,1,2*n) c.a[i][k]+=a.a[i][j]*b.a[j][k];
	For(i,1,n) For(j,n+1,2*n) if (a.a[i][j])
		c.a[i][j]+=a.a[i][j]*b.a[j][j];
	For(i,n+1,2*n)
		c.a[i][i]+=a.a[i][i]*b.a[i][i];
	For(i,1,2*n) For(j,1,2*n) c.a[i][j]%=mo;
	return c;
}
int calc(int s,int t,int l){
	Mat ans;
	ans.a[s][s]=1;
	for (int i=0;l;i++,l/=2)
		if (l&1) ans=ans*p[i];
	return ans.a[s][t+n];
}
int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	scanf("%d%d",&n,&m);
	For(i,1,m){
		int x,y;
		scanf("%d%d",&x,&y);
		a.a[x][y]=a.a[y][x]=1;
		a.a[x][y+n]=a.a[y][x+n]=1;
	}
	For(i,1,n) a.a[n+i][n+i]=1;
	p[0]=a;
	For(i,1,29) p[i]=p[i-1]*p[i-1];
	int Q; scanf("%d",&Q);
	while (Q--){
		int s,t,l,r;
		scanf("%d%d%d%d",&s,&t,&l,&r);
		printf("%d\n",(calc(s,t,r)-calc(s,t,l-1)+mo)%mo);
	}
}
