#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define ll long long
using namespace std;
const int LBC=2333333;
char LZH[LBC],*SSS=LZH,*TTT=LZH;
inline char gc(){
	if (SSS==TTT){
		TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
		if (SSS==TTT) return EOF;
	}
	return *SSS++;
}
inline int read(){
	int x=0;
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
inline void writeln(int x){
	if (!x){
		putchar('0');
		return;
	}
	static int a[20],top;
	for (top=0;x;a[++top]=x%10,x/=10);
	for (;top;putchar(a[top--]+'0'));
	putchar(' ');
}
const int N=100005;
struct node{
	int l,r,sz;
	bool operator <(const node &a)const{
		if (l!=a.l) return l<a.l;
		return r<a.r;
	}
};
int n,k,tp[N][6],id[N],pos[N];
vector<pair<int,int> > vec[N];
int pre[N],nxt[N];
set<node> SS;
set<int> S;
void merge(int r,int l){
	set<node>::iterator it,pre;
	it=SS.upper_bound((node){r,1e9,0}); it--;
	int L=it->l,R=it->r,SZ=it->sz;
	pre=it;
	for (;;){
		if (pre==SS.begin()){
			SS.erase(it); break;
		}
		pre--; SS.erase(it);
		if (pre->r<l) break;
		L=(it=pre)->l; SZ+=pre->sz;
	}
	SS.insert((node){L,R,SZ});
}
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read(); k=read();
	For(i,1,n) For(j,1,k) tp[i][j]=n-read()+1;
	For(i,1,n) id[i]=i;
	For(i,2,k){
		For(j,1,n) pos[j]=tp[j][i],id[tp[j][i]]=j;
		For(j,1,n+1) pre[j]=j-1;
		For(j,0,n) nxt[j]=j+1;
		Rep(j,n,2){
			int x=pos[j],pre=::pre[x],nxt=::nxt[x];
			if (pre!=0&&tp[id[pre]][1]>tp[j][1]) vec[j].push_back(make_pair(id[pre],j));
			if (nxt!=n+1&&tp[j][1]>tp[id[nxt]][1]) vec[j].push_back(make_pair(j,id[nxt]));
			::nxt[::pre[x]]=::nxt[x]; ::pre[::nxt[x]]=::pre[x];
		}
	}
	For(i,1,n) pos[i]=tp[i][1],id[tp[i][1]]=i;
	For(i,1,n){
		set<node>::iterator it;
		it=SS.upper_bound((node){pos[i],1e9,0});
		if (it==SS.begin())
			SS.insert((node){pos[i],pos[i],1});
		else{
			--it;
			if (it->l<=pos[i]&&pos[i]<=it->r){
				int L=it->l,R=it->r,SZ=it->sz;
				SS.erase(it);
				SS.insert((node){L,R,SZ+1});
			}
			else SS.insert((node){pos[i],pos[i],1});
		}
		for (int j=0;j<vec[i].size();j++){
			int fr=vec[i][j].first;
			int to=vec[i][j].second;
			if (pos[fr]<=pos[to]) continue;
			merge(pos[fr],pos[to]);
		}
		writeln(SS.begin()->sz);
	}
}
/*
3 2
1 2
2 1
3 3
*/
