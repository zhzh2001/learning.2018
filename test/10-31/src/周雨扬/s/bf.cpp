#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define ll long long
using namespace std;
const int N=205;
bitset<N> f[N];
int n,k;
int tp[205][5];
bool check(int i,int j){
	For(p,1,k) if (tp[i][p]>tp[j][p]) return 1;
	return 0;
}
int main(){
	freopen("s.in","r",stdin);
	freopen("bf.out","w",stdout);
	scanf("%d%d",&n,&k);
	For(i,1,n) For(j,1,k) scanf("%d",&tp[i][j]);
	For(i,1,n){
		f[i][i]=1;
		For(j,1,i-1){
			f[i][j]=check(i,j);
			f[j][i]=check(j,i);
		}
		For(K,1,i) For(I,1,i)
			if (f[I][K]) f[I]|=f[K];
		int ans=i;
		For(I,1,i) For(J,1,i)
			if (!f[I][J]){
				ans--; break;
			}
		printf("%d ",ans);
	}
}
