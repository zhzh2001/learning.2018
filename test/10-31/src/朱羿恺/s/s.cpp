#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,m,a[N][6],Max[6];
inline bool check(int x){
	For(i,0,m-1) if (a[x][i]<Max[i]) return 0;
	return 1;
}
int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read(),m=read();
	For(i,1,n) For(j,0,m-1) a[i][j]=read();
	if (m==1){For(i,1,n) printf("1 ");return 0;}
	printf("1 ");
	For(i,0,m-1) Max[i]=a[1][i];
	For(i,2,n){
		if (check(i)){
			printf("1 ");
			For(j,0,m-1) Max[i]=a[i][j];
			continue;	
		} else {
			printf("%d ",i);
			For(j,0,m-1) Max[i]=max(Max[i],a[i][j]);
		}
	}
}
