#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
//#pragma GCC optimize(2) 
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 42, mod = 2333;
int n,m,x,y,l,r;
bool e[N][N];
struct ma{
	int c[N][N];
	inline ma operator * (const ma &b)const{
		ma ans;
		For(i,1,n+1) For(j,1,n+1){
			ans.c[i][j]=0;
			For(k,1,n+1) ans.c[i][j]=(ans.c[i][j]+c[i][k]*b.c[k][j])%mod;
		}
		return ans;
	}
}tmp,ans;
inline void Build(){
	memset(tmp.c,0,sizeof tmp.c);
	For(i,1,n) For(j,1,n) if (e[i][j]) tmp.c[i][j]=1;
//	For(i,1,n){
//		For(j,1,n) printf("%d ",tmp.c[i][j]);
//		puts("");
//	}
}
ma dp;
inline ma power(ma x,int y){
	ma ans=x;y--;
	for (;y;y>>=1,x=x*x) if (y&1) ans=ans*x;
	return ans;
}
inline void Print(){For(i,1,n) printf("%d ",dp.c[1][i]);puts("");}
/*inline int solve(int s,int t,int l,int r){
	memset(dp.c,0,sizeof dp.c),dp.c[1][s]=1;
	ma ret=power(tmp,l);dp=dp*ret;
	int ans=dp.c[1][t];//printf("%d\n",ans);Print();
	For(i,l+1,r) dp=dp*tmp,ans=(ans+dp.c[1][t])%mod;//,printf("%d\n",ans),Print();
	return ans;
}*/
inline int solve(int s,int t,int l,int r){
	memset(dp.c,0,sizeof dp.c),dp.c[1][s]=1;
	For(i,1,n+1) tmp.c[i][n+1]=0,tmp.c[n+1][i]=0;
	ma ret=power(tmp,l);dp=dp*ret;//dp.c[1][n+1]=dp.c[1][t];
	For(i,1,n+1) dp.c[n+1][i]=0;
	For(i,2,n+1) dp.c[i][n+1]=0;
	For(i,1,n+1) tmp.c[i][n+1]=0,tmp.c[n+1][i]=0;
	tmp.c[t][n+1]=tmp.c[n+1][n+1]=1;
	/*For(i,1,n+1){
		For(j,1,n+1) printf("%d ",tmp.c[i][j]);
		puts("");
	}*/
	ret=power(tmp,r-l+1),dp=dp*ret;
	/*For(i,1,n+1){
		For(j,1,n+1) printf("%d ",ret.c[i][j]);
		puts("");
	}*/
	return dp.c[1][n+1];
}
int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read(),m=read();
	For(i,1,m) x=read(),y=read(),e[x][y]=e[y][x]=1;
	Build();int q=read();
	while (q--){
		x=read(),y=read(),l=read(),r=read();
		printf("%d\n",solve(x,y,l,r));
	}
}
/*
4 4
1 2
1 4
2 4
2 3
1
1 2 1 3
*/
