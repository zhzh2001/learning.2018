#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10, mod = 1e9+7;
int n;
ll fac[N],inv[N];
inline ll power(int x,int y){
	int ans=1;
	for (;y;y>>=1,x=1ll*x*x%mod) if (y&1) ans=1ll*ans*x%mod;
	return ans;
}
inline void init(){
	n=read();fac[0]=1;
	For(i,1,n+1) fac[i]=fac[i-1]*i%mod;
	For(i,0,n+1) inv[i]=power(fac[i],mod-2);
//	For(i,1,10) printf("%lld %lld\n",fac[i],inv[i]);
}
int m,l,r,k;
ll ans;
inline ll C(int n,int m){return fac[n]*inv[m]%mod*inv[n-m]%mod; }
inline void calc(){
	m=read(),ans=1;
	while (m--){
		l=read(),r=read(),k=read();
		ll K=l-k+1;
		ans=ans*((C(r+1,K)-C(l,K)+mod)%mod)%mod;
	}
	printf("%lld",ans);
}
int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	init(),calc();
}
