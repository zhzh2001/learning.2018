#include<bits/stdc++.h>
#define ll long long
#define int long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
#define pb push_back
using namespace std;
vector<int>a[40];
int n,m,Q;
int s,t,l,r,ans;
inline ll read(){
    ll x=0,f=1;
    char ch = getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
    return x*f;
}
void dfs(int x,int tm)
{
	if(tm>r)return;
	if(x==t)
	{
		if(tm>=l&&tm<=r)ans++;
	}
	if(a[x].size()==0)return;
	For(i,0,a[x].size()-1)
	{
		dfs(a[x][i],tm+1);
	}
}
signed main() 
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read();m=read();
	For(i,1,m)
	{
		int x,y;
		x=read();y=read();
		a[x].pb(y);
		a[y].pb(x);
	}
	Q=read();
	while(Q--)
	{
		ans=0;
		s=read();t=read();l=read();r=read();
		dfs(s,0);
		cout<<ans<<endl;
	}
	return 0;
}
