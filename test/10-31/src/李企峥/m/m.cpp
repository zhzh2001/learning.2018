#include<bits/stdc++.h>
#define ll long long
#define int long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
inline ll read(){
    ll x=0,f=1;
    char ch = getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
    return x*f;
}
ll n,m,l,r,k,mo,ans;
ll C[2010][2010],sum[2010][2010];
signed main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read();m=read();
	mo=1e9+7;
	C[0][0]=1;
	For(i,1,n)
	{
		C[i][0]=1;
		For(j,1,i)C[i][j]=(C[i-1][j-1]+C[i-1][j])%mo;
	}
	For(i,1,n)
	{
		sum[i][0]=1;
		For(j,1,n)sum[i][j]=(sum[i-1][j-1]+C[i][j])%mo;
	}
	ans=1;
	while(m--)
	{
		l=read();r=read();k=read();
		ans=ans*((sum[r][r-l+k]-sum[l-1][k-1]+mo)%mo)%mo;
	}
	cout<<ans<<endl;
	return 0;
}
