#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
const int mod=1e9+7;
const int N=2005;
ll n,m;
ll C[N][N];
ll s[N][N];
int main()
{
	n=read();m=read();
	C[0][0]=1;
	for(int i=1;i<=n;i++){
		C[i][0]=1;
		for(int j=1;j<=i;j++)
			C[i][j]=(C[i-1][j-1]+C[i-1][j])%mod;
	}
	for(int i=1;i<=n;i++)
		s[i][0]=1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			s[i][j]=(s[i-1][j-1]+C[i][j])%mod;
	ll ans=1;
	for(int i=1;i<=m;i++){
		int l,r,k;
		l=read();r=read();k=read();
		ans=ans*((s[r][r-l+k]-s[l-1][k-1]+mod)%mod)%mod;
	}
	writeln(ans);
}
