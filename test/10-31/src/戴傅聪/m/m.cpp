#include<bits/stdc++.h>
using namespace std;
#define mod 1000000007
inline int ksm(int a,int b)
{
	int res=1;
	for(;b;b>>=1,a=1ll*a*a%mod) if(b&1) res=1ll*a*res%mod;
	return res;
}
#define maxn 100100
int fac[maxn],invfac[maxn];
inline void init()
{
	fac[0]=1;for(int i=1;i<maxn;i++) fac[i]=1ll*fac[i-1]*i%mod;
	invfac[maxn-1]=ksm(fac[maxn-1],mod-2);
	for(int i=maxn-2;i>=0;i--) invfac[i]=1ll*invfac[i+1]*(i+1)%mod;
}
inline int C(int n,int m){if(m<0) return 0;return 1ll*fac[n]*invfac[n-m]%mod*invfac[m]%mod;}
int main()
{
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	init();
	int n,m;scanf("%d%d",&n,&m);
	int ans=1;int l,r,k;
	while(m--)
	{
		scanf("%d%d%d",&l,&r,&k);
		ans=1ll*(C(r+1,k+r-l)-C(l,k-1)+mod)%mod*ans%mod;
	}
	cout<<ans<<endl;
}
