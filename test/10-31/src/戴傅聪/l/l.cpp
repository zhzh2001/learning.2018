#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
#define mod 2333
int u[1010],v[1010];int n,m;
struct Matrix
{
	int size,a[41][41];
	Matrix(){memset(a,0,sizeof(a));}
	inline void makeOne()
	{
		memset(a,0,sizeof(a));size=n;
		for(int i=1;i<=n;i++) a[i][i]=1;
	}
	inline void makeit()
	{
		for(register int i=1;i<=m;i++) a[u[i]][v[i]]=a[v[i]][u[i]]=1;
		size=n;
	}
	Matrix operator *(const Matrix &p)const
	{
		Matrix ans;ans.size=size;
		for(register int i=1;i<=size;i++) for(register int k=1;k<=size;k++) for(register int j=1;j<=size;j++) ans.a[i][j]+=a[i][k]*p.a[k][j];
		for(register int i=1;i<=size;i++) for(register int j=1;j<=size;j++) ans.a[i][j]%=mod;
		return ans;
	}
};
inline Matrix ksm(Matrix a,int b)
{
	Matrix res=a;
	if(b==0){res.makeOne();return res;}b--;
	for(;b;b>>=1,a=a*a) if(b&1) res=res*a;
	return res;
}
Matrix x;
inline void solve(int s,int t,int l,int r)
{
	Matrix To=ksm(x,l);
	int ans=To.a[s][t];
	for(register int i=l+1;i<=r;i++)
	{
		To=To*x;
		ans+=To.a[s][t];
	}
	ans%=mod;
	printf("%d\n",ans);
}
int main()
{
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(register int i=1;i<=m;i++) scanf("%d%d",u+i,v+i);
	x.makeit();
	int q;scanf("%d",&q);
	int s,t,l,r;
	while(q--)
	{
		scanf("%d%d%d%d",&s,&t,&l,&r);
		solve(s,t,l,r);
	}
}
