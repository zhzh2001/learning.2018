#include <bits/stdc++.h>

#define ll long long
#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

const ll wzp=1e9+7;

ll n,m,l,r,k,ans,now,fact[Max*2];

inline ll pow(ll x,ll y){
	ll ans=1;
	while(y){
		if(y&1)ans=ans*x%wzp;
		x=x*x%wzp;
		y>>=1;
	}
	return ans;
}

inline ll C(ll n,ll m){
	if(m<0)return 0;
	if((!m)||(m>n))return 1;
	return fact[n]*pow(fact[m]*fact[n-m]%wzp,wzp-2)%wzp;
}
/*	   1
      1 1
     1 2 1
    1 3 3 1
   1 4 6 4 1
 1 5 10 10 5 1
1 6 15 20 15 6 1*/

int main(){
	freopen("m.in","r",stdin);
	freopen("m.out","w",stdout);
	n=read();m=read();
	ans=1;
	fact[0]=1;
	for(ll i=1;i<=200000;i++)fact[i]=fact[i-1]*i%wzp;
	for(ll i=1;i<=m;i++){
		l=read();r=read();k=read();
//		now=0;
		/*for(ll j=l;j<=r;j++){
			now=(now+C(j,j+k-l))%wzp;
		}*/
		now=(C(r+1,r+k-l)-(C(l,k-1))%wzp+wzp)%wzp;
//		cout<<now<<endl;
//		puts("233");
		/*if(now==479951285){
			cout<<l-1<<" "<<k-1<<" "<<C(l,k-1)<<endl;
			cout<<r<<" "<<r+k-l<<" "<<C(r+1,r+k-l)<<endl;
		}*/
		ans=ans*now%wzp;
	}
	writeln(ans);
	return 0;
}
