#include <bits/stdc++.h>

#define ll long long
#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

const ll wzp=1e9+7;

ll n,m,l,r,k,ans,now,fact[Max];

inline ll pow(ll x,ll y){
	ll ans=1;
	while(y){
		if(y&1)ans=ans*x%wzp;
		x=x*x%wzp;
		y>>=1;
	}
	return ans;
}

inline ll C(ll n,ll m){
	if((!n)||(m>n))return 0;
	if(!m)return 1;
	return fact[n]*pow(fact[m]*fact[n-m]%wzp,wzp-2)%wzp;
}

int main(){
	freopen("m.in","r",stdin);
	freopen("1.out","w",stdout);
	n=read();m=read();
	ans=1;
	fact[0]=1;
	for(ll i=1;i<=100000;i++)fact[i]=fact[i-1]*i%wzp;
	for(ll i=1;i<=m;i++){
		l=read();r=read();k=read();
		now=0;
		for(ll j=l;j<=r;j++){
			now=(now+C(j,j+k-l))%wzp;
		}
//		cout<<now<<endl;
		ans=ans*now%wzp;
	}
	writeln(ans);
	return 0;
}
