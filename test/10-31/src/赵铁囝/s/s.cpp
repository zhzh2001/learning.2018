#include <bits/stdc++.h>

#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int n,k,ans;
bool vis[Max];

struct Node{
	int num[6];
	inline bool operator<(const Node&x)const{
		for(int i=1;i<=k;i++){
			if(num[i]>x.num[i])return false;
		}
		return true;
	}
	inline bool operator>(const Node&x)const{
		for(int i=1;i<=k;i++){
			if(num[i]>x.num[i])return true;
		}
		return false;
	}
}a[Max],maxn;

int main(){
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	n=read();k=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=k;j++){
			a[i].num[j]=read();
		}
	}
	if(k==1){
		for(int i=1;i<=n;i++){
			putchar('1');putchar(' ');
		}
		return 0;
	}
	for(int i=1;i<=n;i++){
		bool flag=false;
		for(int j=1;j<i;j++){
			if(vis[j]&&a[i]>a[j]){
				flag=true;
				break;
			}
		}
		if(flag){
			vis[i]=true;
			ans++;
		}
		if(maxn<a[i]){
			ans=1;
			memset(vis,0,sizeof vis);
			vis[i]=true;
		}
		for(int j=1;j<=k;j++){
			maxn.num[j]=max(maxn.num[j],a[i].num[j]);
		}
		write(ans);putchar(' ');
	}
	return 0;
}
