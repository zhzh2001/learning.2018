#include <iostream>
#include <cstdio>

#define Max 1005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

const int wzp=2333;

struct Edge{
	int v,to;
}e[Max*2];

struct Node{
	int a[50],sum;
}num[32][50],ans[50],last[50];

int n,m,sum,l,r,q,s,t,now,size,head[Max];

inline void add(int u,int v){
	e[++size].v=v;e[size].to=head[u];head[u]=size;
}

inline int calc(int s,int t,int x){
	int now=0;
	for(int i=1;i<=n;i++){
		ans[i].sum=0;
	}
	ans[s].sum=1;
	for(int i=30;i>=0;i--){
		if(now+(1ll<<i)<=x){
			now+=(1ll<<i);
			for(int j=1;j<=n;j++){
				last[j]=ans[j];
				ans[j].sum=0;
			}
			for(int j=1;j<=n;j++){
				for(int k=1;k<=n;k++){
					ans[j].sum=(ans[j].sum+num[i][j].a[k]*last[k].sum%wzp)%wzp;
				}
			}
		}
	}
	/*cout<<now<<" "<<x<<endl;
	for(int i=1;i<=n;i++){
		cout<<ans[i].sum<<" ";
	}
	cout<<endl;*/
	return ans[t].sum;
}

int main(){
	freopen("l.in","r",stdin);
	freopen("l.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		l=read();r=read();
		if(l!=r)add(l,r);
		add(r,l);
	}
	for(int i=1;i<=n;i++){
		for(int j=head[i];j;j=e[j].to){
			int v=e[j].v;
			num[0][i].a[v]=(num[0][i].a[v]+1)%wzp;
		}
	}
	for(int i=1;i<=30;i++){
		for(int j=1;j<=n;j++){
			for(int k=1;k<=n;k++){
				for(int p=1;p<=n;p++){
					num[i][j].a[p]=(num[i][j].a[p]+num[i-1][j].a[k]*num[i-1][k].a[p]%wzp)%wzp;
				}
			}
		}
	}
	/*for(int i=0;i<=1;i++){
		for(int j=1;j<=n;j++){
			for(int k=1;k<=n;k++){
				cout<<num[i][j].a[k]<<" ";
			}
			cout<<endl;
		}
		cout<<endl;
	}*/
	q=read();
	while(q--){
		s=read();t=read();l=read();r=read();
		sum=0;
		for(int i=l;i<=r;i++){
			sum=(sum+calc(s,t,i))%wzp;
		}
		writeln(sum);
	}
	return 0;
}
