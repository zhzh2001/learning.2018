#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int a[N][5], n, k;

inline bool mustwin(int i, int j) {
  for (int x = 0; x < k; ++ x) {
    if (a[i][x] < a[j][x]) {
      return false;
    }
  }
  return true;
}
inline bool canwin(int i, int j) {
  for (int x = 0; x < k; ++ x) {
    if (a[i][x] > a[j][x]) {
      return true;
    }
  }
  return false;
}

vector<int> yz[N];
bool can_fail[N];

int main(int argc, char const *argv[]) {
  freopen("s.in", "r", stdin);
  freopen("s.out", "w", stdout);

  n = read(); k = read();

  if (k == 1) {
    for (int i = 1; i <= n; ++ i) {
      puts("1");
    }
    return 0;
  }

  int ans = 0;

  for (int i = 1; i <= n; ++ i) {
    for (int j = 0; j < k; ++ j) {
      a[i][j] = read();
    }
    for (int j = 1; j < i; ++ j) {
      if (mustwin(i, j)) {
        yz[j].push_back(i);
      }
    }
    for (int j = 1; j < i; ++ j) {
      can_fail[i] |= canwin(j, i);
      can_fail[j] |= canwin(i, j);
    }
    int ans = 0;
    for (int j = 1; j <= i; ++ j) {
      bool can_win = true;
      for (vector<int>::iterator it = yz[j].begin(); it != yz[j].end(); ++ it) {
        int yzs = *it;
        if (!can_fail[yzs]) {
          can_win = false;
          break;
        }
      }
      ans += can_win;
    }
    printf("%d ", ans);
  }
  puts("");

  return 0;
}
/*

对于一个队伍，只要一定能战胜它的队伍都可能战败，那么他就有可能成为冠军。

队伍与队伍的关系分为两种
1. 互相能打败对方
2. 一方绝对能打败另一方

对于一个不能成为冠军的队伍，一定能有几个能够压制它的队伍。
我们记为 yz[i] :: [Int]

答案就是 ∑[all can_fail yz]


怎么还 WA 呢
气死
不做了

*/