#include <bits/stdc++.h>
#define N 200020
#define mod 1000000007
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

long long frac[N];
inline long long fast_pow(long long x, long long y) {
  long long z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}
inline long long inv(long long x) {
  return fast_pow(x, mod - 2);
}
inline long long C(long long n, long long m) {
  if (n < m) return 0;
  return frac[n] * inv(frac[m]) % mod * inv(frac[n - m]) % mod;
}

int main(int argc, char const *argv[]) {
  freopen("m.in", "r", stdin);
  freopen("m.out", "w", stdout);

  for (int i = frac[0] = 1; i <= 2e5; ++ i) {
    frac[i] = frac[i - 1] * i % mod;
  }

  int n = read(), m = read();
  long long ans = 1;
  for (int i = 1; i <= m; ++ i) {
    int l = read(), r = read(), k = read();
    long long res = (C(r + 1, l - k + 1) - C(l, l - k + 1) + mod) % mod;
    ans = ans * res % mod;
  }
  printf("%lld\n", ans);

  return 0;
}