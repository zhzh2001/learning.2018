#include <bits/stdc++.h>
#define mod 2333
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

struct Matrix {
  int mat[40][40];
  inline void clear() {
    memset(mat, 0, sizeof mat);
  }
  Matrix() {
    clear();
  }
  Matrix(bool flag) {
    clear();
    for (int i = 0; i < 40; ++ i) {
      mat[i][i] = 1;
    }
  }
  friend Matrix operator * (const Matrix &a, const Matrix &b) {
    Matrix c;
    for (int k = 0; k < 40; ++ k) {
      for (int i = 0; i < 40; ++ i) if (a.mat[i][k]) {
        for (int j = 0; j < 40; ++ j) if (b.mat[k][j]) {
          c.mat[i][j] = (c.mat[i][j] + a.mat[i][k] * b.mat[k][j]) % mod;
        }
      }
    }
    return c;
  }
  friend Matrix operator ^ (Matrix a, long long b) {
    Matrix c(true);
    for (; b; b >>= 1, a = a * a) {
      if (b & 1) {
        c = c * a;
      }
    }
    return c;
  }
} ways[30];

struct query {
  int x, s, t, id;
} qs[10020];
int cnt;
bool cmp(const query &a, const query &b) {
  return a.x == b.x ? a.id < b.id : a.x < b.x;
}

Matrix query(int x) {
  Matrix ret(true);
  for (int bit = 29; ~bit; -- bit) {
    if (x >> bit & 1) {
      ret = ret * ways[bit];
    }
  }
  return ret;
}

int res[50];

int main(int argc, char const *argv[]) {
  freopen("l.in", "r", stdin);
  freopen("l.out", "w", stdout);

  int n = read(), m = read();

  for (int i = 1; i <= m; ++ i) {
    int x = read() - 1, y = read() - 1;
    ways[0].mat[x][y] = ways[0].mat[y][x] = 1;
  }

  for (int i = 1; i < 30; ++ i) {
    ways[i] = ways[i - 1] * ways[i - 1];
  }
  int q = read();
  for (int i = 1; i <= q; ++ i) {
    int s = read() - 1, t = read() - 1;
    int l = read(), r = read();
    for (int x = l; x <= r; ++ x) {
      qs[++ cnt].x = x;
      qs[cnt].s = s;
      qs[cnt].t = t;
      qs[cnt].id = i;
    }
  }

  sort(qs + 1, qs + cnt + 1, cmp);
  Matrix now = ways[0];
  int pw = 1;
  for (int i = 1, j = 1; i <= cnt; i = ++ j) {
    while (j < cnt && qs[j + 1].x == qs[i].x) {
      ++ j;
    }
    now = now * query(qs[i].x - pw);
    for (int k = i; k <= j; ++ k) {
      res[qs[k].id] = (res[qs[k].id] + now.mat[qs[k].s][qs[k].t]) % mod;
    }
    pw = qs[i].x;
  }
  for (int i = 1; i <= q; ++ i) {
    printf("%d\n", res[i]);
  }

  return 0;
}