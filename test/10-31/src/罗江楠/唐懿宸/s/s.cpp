#include<bits/stdc++.h>
using namespace std;

namespace TYC
{
	const int N=100010;
	int n,k,a[N][8];
	
	namespace IO
	{
		inline int read()
		{
			int x=0,f=0;char ch=getchar();
			while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
			while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
			return f?-x:x;
		}
		inline void write(int x)
		{
			if(x<0) putchar('-'),x=-x;
			if(!x) putchar('0');
			else
			{
				int len=0;
				static int bask[15];
				while(x) bask[++len]=x%10,x/=10;
				for(;len;len--) putchar('0'+bask[len]);
			}
			putchar(' ');
		}
	}
	using IO::read;
	using IO::write;
	
	namespace Subtask1
	{
		void work()
		{
			for(int i=1;i<=n;i++) write(1);
		}
	}
	
	namespace Subtask2
	{
		bool win[N];
		
		void work()
		{
			int ans=0;
			for(int i=1;i<=n;i++)
			{
				int flag=1;
				for(int j=1;j<=n;j++)
					if(win[j]) 
					{
						int Iwin=0,Jwin=0;
						for(int num=1;num<=k;num++)
							if(a[i][num]>a[j][num]) Iwin|=1;
							else Jwin|=1;
						if(!Iwin) {flag=0;break;}
						if(!Jwin) win[j]=false,ans--;
					}
				if(flag) win[i]=true,ans++;
				write(ans);
			}
		}
	}
	
	void work()
	{
		n=read(),k=read();
		for(int i=1;i<=n;i++)
			for(int j=1;j<=k;j++)
				a[i][j]=read();
		if(k==1) Subtask1::work();
		else Subtask2::work();
	}
}

int main()
{
	freopen("s.in","r",stdin);
	freopen("s.out","w",stdout);
	TYC::work();
	return 0;
}
