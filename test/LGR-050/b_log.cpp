#include <iostream>
#include <algorithm>
#include <set>
using namespace std;
multiset<long long> S;
void generate_array(int n, int m, int seed)
{
	unsigned x = seed;
	for (int i = 0; i < n; ++i)
	{
		x ^= x << 13;
		x ^= x >> 17;
		x ^= x << 5;
		S.insert(x % m + 1);
	}
}
int main()
{
	int n, m, seed;
	cin >> n >> m >> seed;
	generate_array(n, m, seed);
	while (S.size() >= 2)
	{
		long long a = *S.begin();
		S.erase(S.begin());
		long long b = *S.begin();
		S.erase(S.begin());
		S.insert(min(a, b) * 2);
	}
	cout << *S.begin() << endl;
	return 0;
}