#include <iostream>
using namespace std;
const int N = 1000005, M = 2000, MOD = 998244353;
int c[M + 5][M + 5], a[N];
int main()
{
	ios::sync_with_stdio(false);
	c[0][0] = c[1][0] = c[1][1] = 1;
	for (int i = 2; i <= M; i++)
	{
		c[i][0] = c[i][i] = 1;
		for (int j = 1; j < i; j++)
			c[i][j] = (c[i - 1][j - 1] + c[i - 1][j]) % MOD;
	}
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	int q;
	cin >> q;
	while (q--)
	{
		int x, y;
		cin >> x >> y;
		int ans = 0;
		for (int i = 0; i <= x; i++)
			ans = (ans + 1ll * a[(y + i - 1) % n + 1] * c[x][i]) % MOD;
		cout << ans << endl;
	}
	return 0;
}