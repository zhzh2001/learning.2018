#include <iostream>
using namespace std;
const int MOD = 1e9 + 7;
int qpow(long long a, int b)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % MOD;
		a = a * a % MOD;
	} while (b /= 2);
	return ans;
}
int main()
{
	int n, k;
	cin >> n >> k;
	if (k == 0)
		cout << qpow(n, MOD - 2) << endl;
	else if (k == 1)
		cout << 1ll * (n - 1) * qpow(n, MOD - 2) % MOD << endl;
	return 0;
}