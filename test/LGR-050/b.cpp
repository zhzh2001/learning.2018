#include <iostream>
#include <algorithm>
using namespace std;
const int N = 10000005;
const long long INF = 1e18;
int b[N], a[N];
long long q[N];
void generate_array(int n, int m, int seed)
{
	unsigned x = seed;
	for (int i = 0; i < n; ++i)
	{
		x ^= x << 13;
		x ^= x >> 17;
		x ^= x << 5;
		b[x % m + 1]++;
	}
}
int main()
{
	int n, m, seed;
	cin >> n >> m >> seed;
	generate_array(n, m, seed);
	int cc = 0;
	for (int i = 1; i <= m; i++)
		for (int j = 1; j <= b[i]; j++)
			a[++cc] = i;
	int l = 1, ql = 1, qr = 0;
	while ((n - l + 1) + (qr - ql + 1) >= 2)
	{
		long long u = min(l <= n ? 1ll * a[l] : INF, ql <= qr ? q[ql] : INF);
		if (u == a[l])
			l++;
		else
			ql++;
		long long v = min(l <= n ? 1ll * a[l] : INF, ql <= qr ? q[ql] : INF);
		if (v == a[l])
			l++;
		else
			ql++;
		q[++qr] = max(min(u, v) * 2, v);
	}
	if (l == n)
		cout << a[n] << endl;
	else
		cout << q[ql] << endl;
	return 0;
}