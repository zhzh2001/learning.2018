#include <iostream>
using namespace std;
const int N = 20;
int n, m, col[N], ans;
void dfs(int k, int cnt)
{
	if (k == n + 1)
	{
		if (cnt == 2 * n)
			ans++;
	}
	else
	{
		dfs(k + 1, cnt);
		for (int i = 1; i <= m; i++)
		{
			if (col[i] > 1)
				continue;
			col[i]++;
			if (cnt + 1 <= 2 * n)
				dfs(k + 1, cnt + 1);
			if (cnt + 2 <= 2 * n)
				for (int j = i + 1; j <= m; j++)
					if (col[j] <= 1)
					{
						col[j]++;
						dfs(k + 1, cnt + 2);
						col[j]--;
					}
			col[i]--;
		}
	}
}
int main()
{
	cin >> n >> m;
	dfs(1, 0);
	cout << ans << endl;
	return 0;
}