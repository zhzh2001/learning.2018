#include <iostream>
#include <algorithm>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	long long n, m;
	int s, q;
	cin >> n >> m >> s >> q;
	long long l = 1, r = m * 2 - 1, maxv = 0;
	while (s--)
	{
		long long a, b;
		cin >> a >> b;
		long long nl = m - (a - 1) % m + 1;
		if (nl > m)
			nl -= m;
		long long nr = m - b % m + 1;
		if (nr - m >= nl)
			nr -= m;
		l = max(l, nl);
		r = min(r, nr);
		maxv = max(maxv, b);
	}
	double tmp = 1. * n * m - l + 1;
	if (l > r || (tmp < 2e18 && maxv > n * m - l + 1))
		cout << "Impossible!\n";
	else if (l < r)
		cout << "Uncertain!\n";
	else
	{
		long long ans = 0;
		while (q--)
		{
			long long a;
			cin >> a;
			long long row = (a - (m - l + 1) + m - 1) / m + 1;
			long long col = (a - (m - l + 1) + m - 1) % m + 1;
			//cout << row << ' ' << col << endl;
			if (row <= n)
				ans ^= row ^ col;
		}
		cout << ans << endl;
	}
	return 0;
}