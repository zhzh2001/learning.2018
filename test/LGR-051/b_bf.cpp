#include <iostream>
using namespace std;
const int N = 2005;
pair<int, int> con[N];
int main()
{
	ios::sync_with_stdio(false);
	long long n;
	int m, s, q;
	cin >> n >> m >> s >> q;
	for (int i = 1; i <= s; i++)
		cin >> con[i].first >> con[i].second;
	int id = 0;
	for (int i = 1; i <= m; i++)
	{
		bool flag = true;
		for (int j = 1; j <= s; j++)
			if ((con[j].first - (m - i + 1) + m - 1) / m != (con[j].second - (m - i + 1) + m - 1) / m)
			{
				flag = false;
				break;
			}
		if (flag)
		{
			if (id)
			{
				cout << "Uncertain!\n";
				return 0;
			}
			else
				id = i;
		}
	}
	if (!id)
		cout << "Impossible!\n";
	else
	{
		long long ans = 0;
		while (q--)
		{
			long long x;
			cin >> x;
			long long row = (x - (m - id + 1) + m - 1) / m + 1;
			int col = (x - (m - id + 1) + m - 1) % m + 1;
			if (row <= n)
				ans ^= row ^ col;
		}
		cout << ans << endl;
	}
	return 0;
}