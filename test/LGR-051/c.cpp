#include <iostream>
#include <vector>
using namespace std;
const int N = 5005;
int d[N][N];
vector<pair<int, int>> mat[N];
pair<int, int> que[N];
void dfs(int root, int k, int fat)
{
	for (auto e : mat[k])
		if (e.first != fat)
		{
			d[root][e.first] = d[root][k] + e.second;
			dfs(root, e.first, k);
		}
}
int main()
{
	int n, m;
	cin >> n >> m;
	for (int i = 1; i < n; i++)
	{
		int u, v, w;
		cin >> u >> v >> w;
		mat[u].push_back(make_pair(v, w));
		mat[v].push_back(make_pair(u, w));
	}
	for (int i = 1; i <= m; i++)
		cin >> que[i].first >> que[i].second;
	for (int i = 1; i <= n; i++)
		dfs(i, i, i);
	long long ans = 1e18;
	for (int i = 1; i <= n; i++)
	{
		long long now = 0;
		for (int j = 1; j <= m; j++)
			now = max(now, (long long)d[que[j].first][i] + d[i][que[j].second]);
		ans = min(ans, now);
	}
	cout << ans << endl;
	return 0;
}