#include <iostream>
using namespace std;
const int N = 5005;
int a[N];
short sum[N * N / 2];
int main()
{
	int n, m, k;
	cin >> n >> m >> k;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	int cc = 0;
	for (int i = 1; i <= n; i++)
		for (int j = i + 1; j <= n; j++)
		{
			cc++;
			sum[cc] = (j == i + 1 ? 0 : sum[cc - 1]) + (__builtin_popcount(a[i] ^ a[j]) == k);
		}
	while (m--)
	{
		int l, r;
		cin >> l >> r;
		int ans = 0;
		for (int i = l; i < r; i++)
			ans += sum[(2 * n - i) * (i - 1) / 2 + r - i];
		cout << ans << endl;
	}
	return 0;
}