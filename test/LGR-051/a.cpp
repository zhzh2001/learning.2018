#include <iostream>
using namespace std;
int main()
{
	long long k, mod;
	cin >> k >> mod;
	long long now = 0;
	for (int i = 1;; i++)
	{
		now = (now * 10 + 1) % mod;
		if (now == k)
		{
			cout << i << endl;
			break;
		}
	}
	return 0;
}