#include <iostream>
#include <random>
#include <ctime>
using namespace std;
long long gcd(long long a, long long b)
{
	return b ? gcd(b, a % b) : a;
}
int main()
{
	minstd_rand gen(time(NULL));
	for (;;)
	{
		long long mod = uniform_int_distribution<long long>(2, 1e9)(gen);
		long long k = uniform_int_distribution<long long>(0, mod - 1)(gen);
		if (gcd(mod, k) == 1)
		{
			cout << k << ' ' << mod << endl;
			break;
		}
	}
	cin.get();
	cin.get();
	return 0;
}