#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("31.in");
ofstream fout("31.out");
const int N = 5005, M = 35, MOD = 998244353, INF = 1e9;
int a, b, c, n, f[N][N], d[M][M], ans;
bool mat[M][M];
void dfs(int state, int u, int v)
{
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			d[i][j] = mat[i][j] ? 1 : INF;
	for (int k = 1; k <= n; k++)
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
	for (int i = 1; i < a; i++)
		for (int j = i + 1; j <= a; j++)
			if (d[i][j] < 3)
				return;
	for (int i = 1; i < b; i++)
		for (int j = i + 1; j <= b; j++)
			if (d[a + i][a + j] < 3)
				return;
	for (int i = 1; i < c; i++)
		for (int j = i + 1; j <= c; j++)
			if (d[a + b + i][a + b + j] < 3)
				return;
	ans++;
	switch (state)
	{
	case 1:
		for (int i = u; i <= a; i++)
			for (int j = i == u ? v : 1; j <= b; j++)
			{
				mat[i][a + j] = mat[a + j][i] = true;
				dfs(1, i, j + 1);
				mat[i][a + j] = mat[a + j][i] = false;
			}
		u = v = 1;
	case 2:
		for (int i = u; i <= a; i++)
			for (int j = i == u ? v : 1; j <= c; j++)
			{
				mat[i][a + b + j] = mat[a + b + j][i] = true;
				dfs(2, i, j + 1);
				mat[i][a + b + j] = mat[a + b + j][i] = false;
			}
		u = v = 1;
	case 3:
		for (int i = u; i <= b; i++)
			for (int j = i == u ? v : 1; j <= c; j++)
			{
				mat[a + i][a + b + j] = mat[a + b + j][a + i] = true;
				dfs(3, i, j + 1);
				mat[a + i][a + b + j] = mat[a + b + j][a + i] = false;
			}
	}
}
int main()
{
	fin >> a >> b >> c;
	n = a + b + c;
	if (c == 0)
	{
		if (a < b)
			swap(a, b);
		for (int i = 1; i <= a; i++)
		{
			f[i][1] = i + 1;
			for (int j = 2; j < i; j++)
				f[i][j] = (1ll * f[i - 1][j - 1] * j + f[i - 1][j]) % MOD;
			if (i > 1)
				f[i][i] = (1ll * f[i - 1][i - 1] * i + f[i][i - 1]) % MOD;
		}
		fout << f[a][b] << endl;
	}
	else
	{
		for (int i = 1; i <= n; i++)
			mat[i][i] = true;
		dfs(1, 1, 1);
		fout << ans << endl;
	}
	return 0;
}