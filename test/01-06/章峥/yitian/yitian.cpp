#include <fstream>
#include <queue>
#include <algorithm>
using namespace std;
ifstream fin("yitian.in");
ofstream fout("yitian.out");
int n;
struct state
{
	char mat[4][4];
	int step;
	state(char mat[4][4], int step) : step(step)
	{
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				this->mat[i][j] = mat[i][j];
	}
};
bool vis[1 << 16];
int main()
{
	fin >> n;
	if (n > 4)
	{
		fout << -1 << endl;
		return 0;
	}
	char mat[4][4];
	int h = 0;
	for (int i = 0; i < n; i++)
	{
		fin >> mat[i];
		for (int j = 0; j < n; j++)
			h = h * 2 + (mat[i][j] == '#');
	}
	queue<state> Q;
	Q.push(state(mat, 0));
	vis[h] = true;
	while (!Q.empty())
	{
		state k = Q.front();
		Q.pop();
		bool flag = true;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				if (k.mat[i][j] == '.')
				{
					flag = false;
					break;
				}
		if (flag)
		{
			fout << k.step << endl;
			return 0;
		}
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
			{
				char now[4][4];
				for (int x = 0; x < n; x++)
					for (int y = 0; y < n; y++)
						now[x][y] = k.mat[x][y];
				h = 0;
				for (int t = 0; t < n; t++)
				{
					now[t][j] = k.mat[i][t];
					for (int l = 0; l < n; l++)
						h = h * 2 + (now[t][l] == '#');
				}
				if (!vis[h])
				{
					Q.push(state(now, k.step + 1));
					vis[h] = true;
				}
			}
	}
	fout << -1 << endl;
	return 0;
}