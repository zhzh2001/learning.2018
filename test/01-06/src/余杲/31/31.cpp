#include<cstdio>
#include<iostream>
using namespace std;
const int P=998244353;
int a,b,c;
long long f[5001][5001];
int main(){
	freopen("31.in","r",stdin);
	freopen("31.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>a>>b>>c;
	for(int i=0;i<=5000;i++)f[i][0]=1;
	for(int i=0;i<=5000;i++)f[0][i]=1;
	for(int i=1;i<=5000;i++)
		for(int j=1;j<=5000;j++)
			f[i][j]=(f[i-1][j]+j*f[i-1][j-1])%P;
	printf("%lld",f[a][b]*f[b][c]%P*f[a][c]%P);
	return 0;
}
