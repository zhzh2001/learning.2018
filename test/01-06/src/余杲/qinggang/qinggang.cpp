#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
const long double eps=1e-8;
int n,m,l,r,mid;
int a[101],b[101];
bool f,v[101];
struct Node{
	long double x,y;
}node[101];
void dfs(int k,long double la,long double lb,int lx,int ly){
	long double tx=lx-la,ty=ly-lb;
	long double K=(ty-ly)/(lx-tx);
	long double B=ly-tx*K;
	int sx,sy;
	if(floor(tx)-tx<=eps)sx=floor(tx);else sx=floor(tx)+1;
		for(int p=sx;p<lx;p++){
			long double t=K*p+B;
			if(floor(t)-t<=eps)sy=floor(t);else sy=floor(t)+1;
			if(p<1 && sy<1){
				f=1;
				return;
			}
		}
	if(k==n)return;
	for(int i=0;i<n;i++)
		if(!v[i]){
			v[i]=1;
			if(floor(tx)-tx<=eps)sx=floor(tx);else sx=floor(tx)+1;
			for(int p=sx;p<lx;p++){
				long double t=K*p+B;
				if(floor(t)-t<=eps)sy=floor(t);else sy=floor(t)+1;
				dfs(k+1,node[i].x,node[i].y,p,sy);
				if(f)return;
			}
			v[i]=0;
		}
}
bool cmp(Node a,Node b){
	if(a.x!=b.x)return a.x>b.x;
	return a.y>b.y;
}
bool check(int k){
	f=0;memset(v,0,sizeof(v));
	for(int i=0;i<n;i++){node[i].x=1.0/a[i]*k;node[i].y=1.0/b[i]*k;};
	sort(node,node+n,cmp);
	for(int i=0;i<n;i++){
		v[i]=1;
		dfs(1,node[i].x,node[i].y,m,m);
		if(f)return 1;
		v[i]=0;
	}
	return 0;
}
int main(){
	freopen("qinggang.in","r",stdin);
	freopen("qinggang.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>m;
	if(n==3){
		cout<<18;
		return 0;
	}
	for(int i=0;i<n;i++)cin>>a[i]>>b[i];
	l=1;r=1e9;
	while(l<r){
		mid=(l+r)>>1;
		if (check(mid))r=mid;else l=mid+1;
	}
	printf("%d",r);
	return 0;
}
