#include <bits/stdc++.h>
#define ll long long
#define mod 998244353
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll fast_pow(ll a, ll b) {
	ll c = 1;
	for (; b; b >>= 1, a = a * a % mod)
		if (b & 1) c = c * a % mod;
	return c;
}
ll frac[5020];
ll inv(ll x) {
	return fast_pow(x, mod - 2);
}
ll C(int n, int m) {
	ll res = frac[n] * inv(frac[m]) % mod * inv(frac[n - m]) % mod;
	return res;
}
ll work(int a, int b) {
	if (a > b) swap(a, b);
	ll res = 1;
	for (int i = 1; i <= a; i++) { // 枚举连几条边
		ll ans = C(b, i) * frac[i] % mod * C(a, i) % mod;
		res = (res + ans) % mod;
	}
	return res;
}
int main(int argc, char const *argv[]) {
	freopen("31.in", "r", stdin);
	freopen("31.out", "w", stdout);
	frac[0] = 1;
	for (int i = 1; i <= 5000; i++)
		frac[i] = frac[i - 1] * i % mod;
	int a = read(), b = read(), c = read();
	ll ans = 1;
	ans = ans * work(a, b) % mod;
	ans = ans * work(b, c) % mod;
	ans = ans * work(c, a) % mod;
	cout << ans << endl;
	return 0;
}