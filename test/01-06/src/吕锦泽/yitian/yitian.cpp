#include<bits/stdc++.h>
#define ll int
#define N 1005
#define mod 998244353
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
char s[N][N]; ll ok[N],can[N],boo,ans,n,tmp=mod;
int main(){
	freopen("yitian.in","r",stdin);
	freopen("yitian.out","w",stdout);
	n=read();
	rep(i,1,n) scanf("%s",s[i]+1),ok[i]=1;
	rep(i,1,n) rep(j,1,n) if (s[i][j]=='#') can[j]=1; else ok[j]=0;
	rep(i,1,n){
		ll flag=1,num=0;
		rep(j,1,n){
			if (s[i][j]=='.'){
				if (can[i]) ++num;
				else flag=0;
			}
		}
		if (flag) boo=1;
		tmp=min(tmp,num);
	}
	if (!boo) return puts("-1");
	ans=n+tmp; rep(i,1,n) ans-=ok[i];
	printf("%d",ans);
}
