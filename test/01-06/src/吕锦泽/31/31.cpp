#include<bits/stdc++.h>
#define ll long long
#define N 5005
#define mod 998244353
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll a,b,c,ans,f[N][N],sum[N],fks[N],ljn[N];
int main(){
	freopen("31.in","r",stdin);
	freopen("31.out","w",stdout);
	a=read(); b=read(); c=read();
	if (b>a) swap(a,b); if (c>a) swap(a,c); if (c>b) swap(b,c);
	sum[0]=1; rep(i,1,a) sum[i]=sum[i-1]*i%mod; f[0][0]=1;
	rep(i,1,a) { f[i][0]=1; rep(j,1,i) f[i][j]=(f[i-1][j]+f[i-1][j-1])%mod; }
	rep(i,0,c) fks[i]=f[c][i]*f[a][i]%mod*sum[i]%mod;
	rep(i,0,c) ljn[i]=f[c][i]*f[b][i]%mod*sum[i]%mod;
	rep(i,1,c) fks[i]=(fks[i-1]+fks[i])%mod;
	rep(i,1,c) ljn[i]=(ljn[i-1]+ljn[i])%mod;
	rep(i,0,b) (ans+=fks[c]*ljn[c]%mod*f[a][i]%mod*f[b][i]%mod*sum[i]%mod)%=mod;
	printf("%lld",ans);
}
