#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 5005,mod = 998244353;
int dp[N][N];
int CC[N][N];
int a,b,c;
int ans = 1;
inline int Min(int a,int b){
	if(a < b) return a;
	return b;
}
inline int Max(int a,int b){
	if(a > b) return b;
	return a;
}

void solve(int a,int b){
	if(a > b) swap(a,b);
	// printf("%d %d\n",a,b);
	int sum = 0;
	for(int i = 0;i < a;++i){
		int A = a-i;
		sum = (sum+1LL*dp[b][A]*CC[a][i]%mod)%mod;
	}
	++sum; if(sum >= mod) sum -= mod;
	ans = 1LL*ans*sum%mod;
	// puts("");
}

int main(){
	freopen("31.in","r",stdin);
	freopen("31.out","w",stdout);
	a = read(); b = read(); c = read();
	CC[0][0] = 1;
	for(int i = 1;i <= 5000;++i){
		CC[i][0] = 1;
		for(int j = 1;j <= i;++j){
			CC[i][j] = CC[i-1][j]+CC[i-1][j-1];
			if(CC[i][j] >= mod) CC[i][j] -= mod;
		}
	}
	dp[0][0] = 1;
	for(int i = 1;i <= 5000;++i){
		dp[i][0] = 1;
		for(int j = 1;j <= 5000;++j)
			dp[i][j] = 1LL*dp[i][j-1]*(i-j+1)%mod;
	}
	solve(a,b); if(ans == 0){ puts("0"); return 0; }
	solve(a,c);	if(ans == 0){ puts("0"); return 0; }
	solve(b,c); if(ans == 0){ puts("0"); return 0; }
	printf("%d\n", ans);
	// printf("%d\n",clock());
	return 0;
}