#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 5005;
char mp[N][N];
bool hs[N];
int n;
int hang,num;
int ans;

inline int Min(int x,int y){
	if(x < y) return x;
	return y;
}

int main(){
	freopen("yitian.in","r",stdin);
	freopen("yitian.out","w",stdout);
	n = read();
	bool flag = true;
	for(int i = 1;i <= n;++i){
		scanf("%s",mp[i]+1);
		for(int j = 1;j <= n;++j)
			if(mp[i][j] == '#')
				flag = false;
	}
	if(flag){	//没有#的情况
		puts("-1");
		return 0;
	}
	for(int j = 1;j <= n;++j){		//全列个数
		bool flag2 = true;
		for(int i = 1;i <= n;++i){
			if(mp[i][j] == '.') flag2 = false;
			else hs[j] = true;
		}
		if(flag2) ++num;
	}
	ans = 100000;
	for(int i = 1;i <= n;++i){	//枚举补满哪一行
		int pos = 0;
		if(!hs[i]) ++pos;		//这列都没有，就拿随便一行补上去
		for(int j = 1;j <= n;++j)
			if(mp[i][j] == '.') ++pos;
		ans = Min(ans,pos+n-num);
	}
	printf("%d\n", ans);
	return 0;
}