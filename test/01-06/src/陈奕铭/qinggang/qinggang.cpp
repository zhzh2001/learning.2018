#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){
	ll x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const long double eps = 1e-10;
const int N = 105;
ll n,m;
ll a[N],b[N];
ll ans;
ll dp[N][N];
ll x[N][N],top[N];

inline int Min(int a,int b){
	if(a < b) return a;
	return b;
}
inline void Max(ll &a,ll b){
	if(a < b) a = b;
}

bool check(ll k){
	for(int i = 1;i <= n;++i){
		x[i][0] = k/a[i];
		top[i] = Min(k/b[i],100);
		for(int j = 1;j <= top[i];++j)
			x[i][j] = int((long double)k/(long double)a[i] - ((long double)b[i]*(long double)j)/(long double)a[i]+eps);
	}
	int num = 0;
	for(int i = 1;i <= n;++i)
		num += top[i];
	if(num < m) return false;
	memset(dp,0,sizeof dp);
	for(int j = top[1];j >= 0;--j)
		Max(dp[1][j],x[1][j]);
	for(int i = 2;i <= n;++i){	//组别
		for(int j = top[i];j >= 0;--j)	//费用
			for(int z = m;z >= j;--z)
				if(dp[i-1][z-j] > 0)
					Max(dp[i][z],dp[i-1][z-j]+x[i][j]);
	}
	if(dp[n][m] >= m) return true;
	return false;
}

int main(){
	freopen("qinggang.in","r",stdin);
	freopen("qinggang.out","w",stdout);
	n = read(); m = read();
	for(int i = 1;i <= n;++i) a[i] = read(),b[i] = read();
	// if(n == 1){
	// 	ans = m*(a[1]+b[1]);
	// 	printf("%lld\n", ans);
	// 	return 0;
	// }
	ll l = 1,r = 100000200LL,mid;
	while(l <= r){
		mid = (l+r)>>1;
		if(check(mid)){
			ans = mid;
			r = mid-1;
		}
		else l = mid+1;
	}
	printf("%lld\n", ans);
	return 0;
}