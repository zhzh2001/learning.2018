#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
using namespace std;
const int MX=111,inf=0x3f3f3f3f;
int n,m;
int d[MX][2];
int ava[MX][MX],mx[MX][MX];

bool ck(int t){
	memset(ava,0,sizeof(ava));
	for(int i=0;i<=m;i++)
		for(int j=0;j<=m;j++)
			ava[i][j]=(i*d[1][0]+j*d[1][1]<=t);
	for(int i=2;i<=n;i++){
		memset(mx,~inf,sizeof(mx));
		for(int j=0;j<=m;j++)
			for(int k=0;k<=m;k++)
				if(ava[j][k])mx[j][k]=d[i][0]*j+d[i][1]*k;
		for(int j=1;j<=m;j++)mx[j][0]=max(mx[j][0],mx[j-1][0]);
		for(int j=1;j<=m;j++)mx[0][j]=max(mx[0][j],mx[0][j-1]);
		for(int j=1;j<=m;j++)
			for(int k=1;k<=m;k++)
				mx[j][k]=max(mx[j][k],max(mx[j][k-1],mx[j-1][k]));
		for(int j=0;j<=m;j++)
			for(int k=0;k<=m;k++)
				ava[j][k]=(mx[j][k]>=d[i][0]*j+d[i][1]*k-t);
	}
	return ava[m][m];
}
int main(){
	freopen("qinggang.in","r",stdin);freopen("qinggang.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)scanf("%d%d",&d[i][0],&d[i][1]);
	int l=0,r=200000001;
	while(l<r){
		int mid=(l+r)>>1;
		if(ck(mid))r=mid;else l=mid+1;
	}
	printf("%d\n",l);
	return 0;
}
