#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>

using namespace std;

const int MX=1011;

int n;
char a[MX][MX];
bool hs[MX];

int wk(){
	bool flg=false;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			flg=flg||(a[i][j]=='#');
	if(!flg)return -1;
	int s=0;
	for(int i=1;i<=n;i++){
		bool flg=false;
		for(int j=1;j<=n;j++)flg=flg||(a[j][i]=='.');
		for(int j=1;j<=n;j++)hs[i]=hs[i]||(a[j][i]=='#');
		s+=flg;
	}
	int mn=n+1;
	for(int i=1;i<=n;i++){
		int t=0;
		for(int j=1;j<=n;j++)t+=(a[i][j]=='.');
		if(t&&!hs[i])++t;
		mn=min(mn,t);
	}
	return s+mn;
}
int main(){
	freopen("yitian.in","r",stdin);freopen("yitian.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%s",a[i]+1);
	printf("%d\n",wk());
	return 0;
}
