#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
const int mod = 998244353;

typedef vector<int> vi;

int egcd(int a, int b, int& x, int& y) {
    if (b == 0) { x = 1; y = 0; return a; }
    int x1, y1, g = egcd(b, a%b, x1, y1); //a*y + b*(x - (a/b)*y)
    x = y1, y = x1-y1*(a/b);
    return g;
}
int pow(int a, int b, int m) {
	if (b == 1) return a%m;
	ll res = pow(a, b / 2, m);
	res *= res; res %= m;
	res *= (b % 2 ? a : 1); res %= m;
	return (int)res;
}

//http://codeforces.com/contest/869/problem/C
//Test: code_submit.py bati06test 869C modinv.cpp
//TODO: write inv function here!
int inv(int a) { return pow(a, mod - 2, mod); } //lfkda��sofk

int solve(int a, int b) {
	ll res = 0, t = 1;
	for (int i = 0; i <= min(a, b); ++i) {
		res += t; res %= mod;
		t *= a - i; t %= mod;
		t *= b - i; t %= mod;
		t *= inv(i + 1); t %= mod;
	}
	return (int)res;
}

int main() {
	freopen("31.in","r",stdin);freopen("31.out","w",stdout);
	int a, b, c; cin >> a >> b >> c;

	ll res = 1;
	res *= solve(a, b); res %= mod;
	res *= solve(a, c); res %= mod;
	res *= solve(b, c); res %= mod;

	cout << res << endl;
}
