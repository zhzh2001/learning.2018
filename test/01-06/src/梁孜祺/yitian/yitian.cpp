#include<bits/stdc++.h>
#define ll long long
#define N 1005
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,a[N][N],c[N],p;
char A[N][N];
int main(){
	freopen("yitian.in","r",stdin);
	freopen("yitian.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n) scanf("%s",A[i]+1);
	For(i,1,n) For(j,1,n) a[i][j]=(A[i][j]=='#');
	For(j,1,n){
		bool f=1;
		For(i,1,n) if(a[i][j]==0) f=0;
		if(!f) p++;
	}
	int sum,ans=-1;
	For(i,1,n) For(j,1,n) if(a[i][j]==1) c[j]=1;
	For(i,1,n){
		sum=0;bool flag=1;
		For(j,1,n) if(a[i][j]==0){
			if(!c[i]){flag=0;break;}
			sum++;
		}
		if(!flag) continue;
		if(ans==-1) ans=sum+p;
		else ans=min(ans,sum+p);
	}
	printf("%d\n",ans);
	return 0;
}
