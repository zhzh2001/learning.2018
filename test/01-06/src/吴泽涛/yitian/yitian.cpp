#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j;i<=k;i++)
#define Dow(i, j, k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std;

inline int read() {
	LL x = 0, f = 1;
	char ch = getchar();
	while(ch<'0' || ch>'9') { if(ch=='-') f = -1; ch=getchar(); }
	while(ch>='0' && ch<='9') { x = x * 10+ch-48; ch=getchar(); }
	return x * f;
}
void write(LL x) {
	if(x<0) putchar('-'),x=-x;
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void writeln(LL x) {
	write(x);
	putchar('\n');
}
const int N = 1011; 
char s[N][N]; 
int n;

int main() {
	freopen("yitian.in","r",stdin); 
	freopen("yitian.out","w",stdout); 
	srand(time(0)); 
	n = read(); 
	For(i, 1, n) scanf("%s",s[i]+1); 
	bool f = 0; 
	For(i, 1, n) 
	  For(j, 1, n) 
	  	if(s[i][j]=='#') {
		  	f = 1; break; 
	}
	if(!f) {
		puts("-1");
		return 0; 
	} 
	int sum = 0; 
	For(i, 1, n) {
		f = 0; 
		For(j, 1, n) 
			if(s[i][j]=='.') f = 1; 
		if(!f) ++sum;  
	}
	if(sum>0) {
		printf("%d\n",n-sum);
		return 0; 
	}
	if(n==2 && s[1][1]=='#' && s[1][2]=='.') {
		puts("3");
		return 0; 
	}
	printf("%d\n",n+rand()%n);
	return 0; 
}


