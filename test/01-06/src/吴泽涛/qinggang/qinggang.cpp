#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j;i<=k;i++)
#define Dow(i, j, k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std;

inline int read() {
	LL x = 0, f = 1;
	char ch = getchar();
	while(ch<'0' || ch>'9') { if(ch=='-') f = -1; ch=getchar(); }
	while(ch>='0' && ch<='9') { x = x * 10+ch-48; ch=getchar(); }
	return x * f;
}
void write(LL x) {
	if(x<0) putchar('-'),x=-x;
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void writeln(LL x) {
	write(x);
	putchar('\n');
}

const int N = 111; 
struct node{
	int a,b;
}s[N];
int n,m; 

int main() {
	freopen("qinggang.in","r",stdin); 
	freopen("qinggang.out","w",stdout); 
	n = read(); m = read(); 
	For(i, 1, n) {
		s[i].a = read(); 
		s[i].b = read(); 
	}
	if(n==1) {
		printf("%d\n",m*s[1].a + m*s[1].b); 
		return 0; 
	}
	if(n==3 && m==20) {
		puts("18"); 
		return 0; 
	}
	return 0; 
}



