#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=5005,mod=998244353;
typedef unsigned long long ll;
int a,b,c;
ll bin[maxn];
int C[maxn][maxn];
inline void init(){
	scanf("%d%d%d",&a,&b,&c);
}
inline void prepare(){
	for (int i=0;i<=5000;i++){
		C[i][0]=1;
		for (int j=1;j<=i;j++){
			(C[i][j]=C[i-1][j]+C[i-1][j-1])%=mod;
		}
	}
	bin[0]=1;
	for (int i=1;i<=5000;i++){
		bin[i]=bin[i-1]*i%mod;
	}
}
ll ans;
inline ll work(int x,int y){
	ll temp=0;
	for (int i=0;i<=min(x,y);i++){
		temp+=bin[i]*(ll)C[x][i]%mod*(ll)C[y][i];
		temp%=mod;
	}
	return temp;
}
inline void solve(){
	prepare();
	ans=1;
	ans*=work(a,b); ans%=mod;
	ans*=work(b,c); ans%=mod;
	ans*=work(c,a); ans%=mod;
	printf("%lld\n",ans);
}
int main(){
	freopen("31.in","r",stdin);
	freopen("31.out","w",stdout);
	init();
	solve();
	return 0;
}
