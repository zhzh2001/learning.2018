#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=1005,inf=1e9;
int n,s1,sum[maxn],s2,a[maxn][maxn];
bool flag;
inline void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		char s[maxn];
		scanf("%s",s+1);
		for (int j=1;j<=n;j++){
			if (s[j]=='#') a[i][j]=1,flag=1,sum[i]++;
		}
		if (sum[i]==n){
			s1++;
		}
	}
	for (int j=1;j<=n;j++){
		int ss=0;
		for (int i=1;i<=n;i++){
			ss+=a[i][j];
		}
		if (ss==n) s2++;
	}
}
int ans;
inline int work(int x,int y){
	int temp=sum[y],res=0;
	if (!a[y][y]){
		temp++;
	}
	res=n-temp+n+1-s2;
	if (x==y) res--;
	return res;
}
inline void solve(){
	ans=inf;
	if (s1>0){
		ans=min(ans,n-s2);
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			if (a[i][j]){
				ans=min(ans,work(i,j));
			}
		} 
	}
	printf("%d\n",ans);
}
int main(){
	freopen("yitian.in","r",stdin);
	freopen("yitian.out","w",stdout);
	init();
	if (!flag) {
		puts("-1");
	}else{
		solve();
	}
	return 0;
}
