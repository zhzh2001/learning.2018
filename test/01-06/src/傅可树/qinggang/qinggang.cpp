#include<cstdio>
using namespace std;
const int maxn=105;
int n,m,a[maxn],b[maxn];
inline void init(){
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++){
		scanf("%d%d",&a[i],&b[i]);
	}
}
inline bool judge(int mid){
	double A=1.0*mid/a[1],B=1.0*mid/b[1];
	double x=A*B/(A+B);
	return x>=m;
}
inline void solve(){
	int l=1,r=100000000;
	while (l<r){
		int mid=(l+r)>>1;
		if (judge(mid)){
			r=mid;
		}else{
			l=mid+1;
		}
	}
	printf("%d\n",l);
}
int main(){
	freopen("qinggang.in","r",stdin);
	freopen("qinggang.out","w",stdout);
	init();
	solve();
	return 0;
}
