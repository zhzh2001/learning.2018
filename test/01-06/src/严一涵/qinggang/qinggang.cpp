#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <string.h> 
using namespace std;
typedef long long LL;
const LL oo=1ll<<60;
#define d c=getchar()
inline LL read(){LL a=0,b=1;char d;while(!isdigit(c)&&c!='-')d;
	if(c=='-'){b=-b;d;}while(isdigit(c)){a=a*10+c-'0';d;}return a*b;}
#undef d
LL n,m,a[103],b[103],le[103][103],f[103];
bool check(LL k){
	memset(le,0,sizeof(le));
	for(int i=1;i<=m;i++)f[i]=-oo;
	f[0]=0;
	for(int i=1;i<=n;i++)for(int j=0;j<=m;j++){
		if(j*a[i]>k)le[i][j]=-oo;else
		for(int l=0;l<=m;l++)if(k>=j*a[i]+l*b[i])le[i][j]=l;else break;
	}
	for(int i=1;i<=n;i++){
		for(int j=m;j>=0;j--){
			f[j]=max(f[j],f[j]+le[i][0]);
			for(int l=j;l>0;l--)f[j]=max(f[j],f[j-l]+le[i][l]);
		}
	}
	return f[m]>=m;
}
LL bs(LL l,LL r){
	LL mid;
	while(l<r){
		mid=l+r>>1;
		if(check(mid))r=mid;
		else l=mid+1;
	}
	return l;
}
int main(){
	freopen("qinggang.in","r",stdin);
	freopen("qinggang.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();b[i]=read();
	}
	printf("%lld\n",bs(1,100000000));
	return 0;
}
