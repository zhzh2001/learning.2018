#include <iostream>
#include <algorithm>
#include <stdio.h>
using namespace std;
typedef long long LL;
const LL oo=1ll<<60,md=998244353;
#define d c=getchar()
inline LL read(){LL a=0,b=1;char d;while(!isdigit(c)&&c!='-')d;
	if(c=='-'){b=-b;d;}while(isdigit(c)){a=a*10+c-'0';d;}return a*b;}
#undef d
LL f[5003][5003],a,b,c,n;
int main(){
	freopen("31.in","r",stdin);
	freopen("31.out","w",stdout);
	a=read();b=read();c=read();
	n=max(a,max(b,c));
	for(int j=0;j<=n;j++)f[0][j]=1;
	for(int i=1;i<=n;i++){
		f[i][0]=1;
		for(int j=1;j<=n;j++)f[i][j]=(f[i-1][j-1]*j+f[i-1][j])%md;
	}
	printf("%lld\n",f[a][b]*f[b][c]%md*f[a][c]%md);
	return 0;
}
