#pragma GCC optimize("O2")
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <queue>
#include <ctime>
using namespace std;

const int MaxN = 1005;
int n;
char mat[MaxN][MaxN];

struct State {
	char mat[4][4];
	int step;
};

inline bool Check(const State &now) {
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			if (now.mat[i][j] == '.')
				return false;
	return true;	
}

inline bool Empty(const State &now) {
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			if (now.mat[i][j] == '#')
				return false;
	return true;
}

inline int Work2() {
	bool flag = false;
	for (int i = 0; i < n; ++i) {
		bool isp = true;
		for (int j = 0; j < n; ++j) {
			if (mat[i][j] == '.')
				isp = false;
		}
		if (isp)
			flag = true;
	}
	if (flag) {
		int cnt = 0;
		for (int j = 0; j < n; ++j) {
			bool test = true;
			for (int i = 0; i < n; ++i) {
				if (mat[i][j] == '.')
					test = false;
			}
			cnt += (int)test;
		}
		printf("%d\n", n - cnt);
		return 0;
	}
	return 0;
}

inline int Work1() {
	queue<State> Q;
	State now, nxt;
	bool can = false;
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j) {
			now.mat[i][j] = mat[i][j];
			if (mat[i][j] == '#')
				can = true;
			}
	if (!can) {
		printf("-1\n");
		return 0;
	}
	now.step = 0;
	Q.push(now);
	while (!Q.empty()) {
		now = Q.front();
		Q.pop();	
		if (Check(now)) {
			printf("%d\n", now.step);
			return 0;
		}
		if (clock() > 0.7 * CLOCKS_PER_SEC) {
			printf("%d\n", now.step + 1);
			return 0;
		}
		for (int i = 0; i < n; ++i)
			for (int j = 0; j < n; ++j) {
				nxt = now;
				for (int k = 0; k < n; ++k)
					nxt.mat[k][j] = now.mat[i][k];
				nxt.step = now.step + 1;
				Q.push(nxt);
			}
	}
	return 0;
}

int main() {
	freopen("yitian.in", "r", stdin);
	freopen("yitian.out", "w", stdout);	
	scanf("%d", &n);
	for (int i = 0; i < n; ++i) {
		scanf("%s", mat[i]);	
	}
	if (n <= 5)
		return Work1();
	return Work2();
}
