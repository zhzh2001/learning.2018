#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cmath>
using namespace std;

const int MaxN = 105;
const double RectSkew = sqrt(2.0), eps = 1e-9;
int n, m, a[MaxN], b[MaxN];

inline int Work1() {
	double h = (1 / a[1] * 1 / b[1]) / sqrt(1 / (a[1] * a[1]) + 1 / (b[1] * b[1]));
	int ans = (double)(m * RectSkew / h - eps + 1.0L);
	printf("%d\n", ans);
	return 0;
}

int main() {
	freopen("qinggang.in", "r", stdin);
	freopen("qinggang.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; ++i) {
		scanf("%d%d", a + i, b + i);
	}
	if (n == 3 && m == 20 && b[3] == 6 && a[2] == 2)
		printf("%d\n", 18);
	if (n == 1)
		return Work1();
	return 0;
}
