#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

typedef long long ll;

const ll MaxN = 5005, Mod = 998244353;

ll a, b, c;
ll fact[MaxN];

inline ll QuickPow(ll x, ll y) {
	ll ret = 1;
	for (; y; y >>= 1, x = (x * x) % Mod) {
		if (y & 1)
			ret = (ret * x) % Mod;
	}
	return ret;
}

inline int Work1() {
	ll ans = 1;
	for (int i = 1; i <= a; ++i)
		ans = (ans * QuickPow(2, b)) % Mod;
	printf("%lld\n", ans);
	return 0;
}

inline ll C(ll c, ll i) {
	return fact[c] * QuickPow(fact[i], Mod - 2) % Mod * QuickPow(fact[c - i], Mod - 2) % Mod;	
}

inline int Work2() {
	ll ans1 = 0, ans2 = 1;
//	if (a == 1)
//		ans1 = (b * c % Mod + b + c + 1) % Mod;
	for (int i = 0; i <= min(b, a); ++i) {
		ll pans1 = C(b, i) * C(a, i) % Mod * fact[i] % Mod;
		for (int j = 0; j <= min(c, a); ++j) {
			ll pans2 = (pans1 * C(c, j) % Mod * C(a, j) % Mod * fact[j]) % Mod;
			ans1 = (ans1 + pans2) % Mod;
		}
	}
	cerr << ans1 << endl;
	for (int i = 1; i <= b; ++i) {
		ll pans2 = C(b, i) * fact[i] % Mod * C(c, i) % Mod;
		cerr << C(b, i) << ' ' << fact[i] << ' '  << C(c, i) << endl;
		ans2 = (ans2 + pans2) % Mod;
	}
	cerr << ans2 << endl;
	printf("%lld\n", ans1 * ans2 % Mod);
	return 0;	
}

int main() {
	freopen("31.in", "r", stdin);
	freopen("31.out", "w", stdout);
	scanf("%lld%lld%lld", &a, &b, &c);
//	if (a > b)
//		swap(a, b);
//	if (a > c)
//		swap(a, c);
//	if (b > c)
//		swap(b, c);
	fact[0] = 1;
	for (int i = 1; i < MaxN; ++i)
		fact[i] = fact[i - 1] * i % Mod;
	if (c == 0)
		return Work1();
	return Work2();
}
