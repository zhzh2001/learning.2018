#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

typedef long long ll;

const ll MaxN = 5005, Mod = 998244353;

int a, b, c;
ll fact[MaxN], C[MaxN][MaxN];

int Work() {
	ll ans1 = 0, ans2 = 1;
	for (int i = 0; i <= min(b, a); ++i) {
		ll pans1 = C[b][i] * C[a][i] % Mod * fact[i] % Mod;
		for (int j = 0; j <= min(c, a); ++j) {
			ll pans2 = (pans1 * C[c][j] % Mod * C[a][j] % Mod * fact[j]) % Mod;
			ans1 = (ans1 + pans2) % Mod;
		}
	}
	for (int i = 1; i <= b; ++i) {
		ll pans2 = C[b][i] * fact[i] % Mod * C[c][i] % Mod;
		ans2 = (ans2 + pans2) % Mod;
	}
	printf("%lld\n", ans1 * ans2 % Mod);
	return 0;
}

int main() {
	freopen("31.in", "r", stdin);
	freopen("31.out", "w", stdout);
	scanf("%d%d%d", &a, &b, &c);
	fact[0] = 1;
	for (int i = 1; i < MaxN; ++i) {
		fact[i] = (fact[i - 1] * i) % Mod;
	}
	C[0][0] = 1;
	for (int i = 1; i < MaxN; ++i) {
		C[i][0] = 1;
		for (int j = 1; j <= i; ++j) {
			C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % Mod;
		}
	}
	return Work();
}
