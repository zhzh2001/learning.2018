#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
struct D{
	ll x,y;
}a[110];
ll n,m,l,r,mid,ans;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
bool cmp(D a,D b)
{
	if(a.y*b.x>b.y*a.x)return true;
	return false;
}
bool solve(ll k)
{
	ll x=m,y=m,i=0;
	ll xx,xs,xy,yy;
	while(x>0&&i<n)
	{
		i++;
		xx=k/a[i].x;
		if(xx>x)
		{
			xx=x;
			xs=k-xx*a[i].x;
			xy=xs/a[i].y;
			x-=xx;
			y-=xy;
			break;
		}
		xs=k%a[i].x;
		xy=xs/a[i].y;
		x-=xx;
		y-=xy;
	}
	while(y>0&&i<n)
	{
		i++;
		yy=k/a[i].y;
		y-=yy;
	}
	if(x<=0&&y<=0)return true;
	return false;
}
int main()
{
	freopen("qinggang.in","r",stdin);
	freopen("qinggang.out","w",stdout);
	n=read();m=read();
	For(i,1,n)
	{
		a[i].x=read();
		a[i].y=read();
	}
	sort(a+1,a+n+1,cmp);
	l=1;r=1e8;
	while(l<r)
	{
		mid=(l+r)/2;
		if(solve(mid))r=mid;
					else l=mid+1;
	}
	ans=l;
	For(i,1,n)swap(a[i].x,a[i].y);
	sort(a+1,a+n+1,cmp);
	l=1;r=1e8;
	while(l<r)
	{
		mid=(l+r)/2;
		if(solve(mid))r=mid;
					else l=mid+1;
	}
	ans=min(ans,l);
	cout<<ans<<endl;
}

