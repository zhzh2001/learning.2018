#include<cstdio>
#include<iostream>
#include<cstring>
#define ll long long
using namespace std;
int f[500][500][500];
const int mod=998244353;
int a,b,c;
ll ans=1;
int main()
{
	freopen("31.in","r",stdin);freopen("31.out","w",stdout);
	scanf("%d%d%d",&a,&b,&c);
	if (a&&b&&c) ans=(ans+a*b*c)%mod;
	if (a&&b) ans=(ans+a*b)%mod;
	f[0][0][0]=1;
	for (int i=0;i<=a;++i)
	{
		for (int j=0;j<=b;++j)
		{
			for (int k=0;k<=c;++k) if (i||j||k)
			{
				f[i][j][k]=1;
				for (int di=0;di<=i;++di)
				{
					for (int dj=0;dj<=j;++dj)
					{
						for (int dk=0;dk<=k;++dk) if (di||dj||dk)
						{
							f[i][j][k]=(f[i][j][k]+f[i-di][j-dj][k-dk]*f[di][dj][dk])%mod;
						}
					}
				}
				f[i][j][k]--;
			}
		}
	}
	cout<<f[a][b][c]<<endl;
	fclose(stdin);fclose(stdout);
	return 0;
}
