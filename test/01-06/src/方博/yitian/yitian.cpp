#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){char ch=getchar();ll x=0,f=1;for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
const int N=1005;
ll n,ans;
char a[N][N];
int b[N][N];
int need[N],z[N];
bool have[N],bb;
int f[N],t;
int main()
{
	freopen("yitian.in","r",stdin);
	freopen("yitian.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)scanf("%s",a[i]+1);
		
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if(a[i][j]=='#')bb=true;
			b[i][j]=(a[i][j]=='#');
			if(!b[i][j])need[i]++;
			else have[j]=true,z[j]++;
		}
	
	if(!bb){puts("-1");return 0;}
	
	for(int i=1;i<=n;i++)if(!need[i])t++;
	
	if(t!=0){t=0;for(int i=1;i<=n;i++)if(z[i]==n)t++;writeln(n-t);return 0;}
	
	ans=1e9;
	for(int i=1;i<=n;i++)
		if(have[i]&&need[i])ans=min(ans,need[i]+n);
		
	writeln(ans);
	return 0;
}
