#include<bits/stdc++.h>
using namespace std;
const int N=105;
#define ll long long
const double eps=1e-9;
struct xxx{
	int a,b;
	double k;
}a[N];
ll n,m,ans;
inline ll read(){char ch=getchar();ll x=0,f=1;for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
bool com(xxx a,xxx b){return a.k>b.k;}
bool check(ll k)
{
	double l=0,d=0;
	for(int i=1;i<=n;i++)
		d+=1.0*k/a[i].b;
	for(int i=1;i<=n;i++){
		d-=1.0*k/a[i].b;
		l+=1.0*k/a[i].a;
		if(l>=m&&d>=m)return true;
		if(fabs(m-d)<=eps&&fabs(m-l)<=eps)return true;
		else if(m>l&&m>d)return false;
		else if(l>m&&d<m){
			l-=1.0*k/a[i].a;
			d+=1.0*k/a[i].b;
			d-=(m-l)/a[i].k;
			l=m;
			if((fabs(m-d)<=eps&&fabs(m-l)<=eps)||(l>=m&&d>=m))return true;
			else return false;
		}
	}
	return false;
}
int main()
{
	freopen("qinggang.in","r",stdin);
	freopen("qinggang.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)
		a[i].a=read(),a[i].b=read(),a[i].k=1.0*a[i].b/a[i].a;
	sort(a+1,a+n+1,com);
	ll l=1,r=1e12;
	while(l<=r){
		ll mid=(l+r)/2;
		if(check(mid))r=mid-1,ans=mid;
		else l=mid+1;
	}
	printf("%lld",ans);
	return 0;
}
