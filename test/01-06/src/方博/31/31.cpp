#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){char ch=getchar();ll x=0,f=1;for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
int a,b,c;
const ll mod=998244353;
int n;
ll C[5005][5005];
ll pn[5005];
int tot;
ll ans;
ll ab,ac,bc;
int main()
{
	freopen("31.in","r",stdin);
	freopen("31.out","w",stdout);
	a=read();b=read();c=read();
	
	if(a>b)swap(a,b);
	if(b>c)swap(b,c);
	if(a>b)swap(a,b);
	
	C[0][0]=1;pn[0]=1;
	for(int i=1;i<=c;i++)pn[i]=pn[i-1]*i%mod;
	for(int i=1;i<=c;i++){
		C[i][0]=1;
		for(int j=1;j<=c;j++)
			C[i][j]=(C[i-1][j-1]+C[i-1][j])%mod;
	}
	
	ab=ac=bc=1;
	for(int i=1;i<=a;i++)
		ab=(ab+C[a][i]*C[b][i]*pn[i])%mod;
	for(int i=1;i<=a;i++)
		ac=(ac+C[a][i]*C[c][i]*pn[i])%mod;
	for(int i=1;i<=b;i++)
		bc=(bc+C[b][i]*C[c][i]*pn[i])%mod;
	
	ans=(ab*ac)%mod*bc%mod;
	writeln(ans);
	return 0;
}
