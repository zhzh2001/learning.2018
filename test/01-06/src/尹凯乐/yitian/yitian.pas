program yitian;
 uses math;
 var
  a:array[0..1001,0..1001] of char;
  i,j,n,s,smin,ssum,upass:longint;
 begin
  assign(input,'yitian.in');
  assign(output,'yitian.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
   begin
    for j:=1 to n do
     read(A[i,j]);
    readln;
   end;
  ssum:=0;
  for i:=1 to n do
   begin
    upass:=0;
    for j:=1 to n do
     if a[j,i]='.' then upass:=1;
    if upass=1 then inc(ssum,0)
               else inc(ssum,1);
   end;
  smin:=1008208820;
  for i:=1 to n do
   begin
    s:=0;
    upass:=0;
    for j:=1 to n do
     if a[i,j]='.' then inc(s);
    for j:=1 to n do
     if a[j,i]='#' then upass:=1;
    if (s=0) or (upass=1) then smin:=min(smin,s+n-ssum);
   end;
  if smin=1008208820 then writeln('-1')
                     else writeln(smin);
  close(input);
  close(output);
 end.
