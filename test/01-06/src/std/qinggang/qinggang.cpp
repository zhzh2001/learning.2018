#include <bits/stdc++.h>
#define int long long
using namespace std;
int n, m, x[1000], y[1000], f[1000], l, r, mid;
int read()
{
	char c = getchar();
	while (c != '-' && (c < '0' || c > '9'))
		c = getchar();
	int k = 1, kk = 0;
	if (c == '-')
		k = -1, c = getchar();
	while (c >= '0' && c <= '9')
		kk = kk * 10 + c - '0', c = getchar();
	return k * kk;
}
int doit(int xx, int yy, int zz, int k) { return (yy - zz) * x[xx] <= k ? (k - (yy - zz) * x[xx]) / y[xx] : -1e9; }
bool pd(int x)
{
	memset(f, 200, sizeof(f));
	f[0] = 0;
	for (int i = 1; i <= n; i++)
		for (int j = m; j >= 0; j--)
			for (int k = j; k >= 0; k--)
				f[j] = max(f[j], f[k] + doit(i, j, k, x));
	return f[m] >= m;
}
signed main()
{
	freopen("qinggang.in", "r", stdin);
	freopen("qinggang.out", "w", stdout);
	n = read();
	m = read();
	for (int i = 1; i <= n; i++)
		x[i] = read(), y[i] = read();
	l = 0;
	r = 1e18;
	while (l < r)
	{
		mid = (l + r) / 2;
		if (pd(mid))
			r = mid;
		else
			l = mid + 1;
	}
	cout << l << endl;
}
