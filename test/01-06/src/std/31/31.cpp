#include <bits/stdc++.h>
using namespace std;
#define int long long
int x, y, z, f[5010][5010], lsg, n, xx, yy, zz;
signed main()
{
	freopen("31.in", "r", stdin);
	freopen("31.out", "w", stdout);
	cin >> x >> y >> z;
	n = max(x, max(y, z));
	lsg = 998244353;
	for (int i = 0; i <= n; i++)
		f[0][i] = f[i][0] = 1;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			(f[i][j] = f[i - 1][j - 1] * i + f[i][j - 1]) %= lsg;
	xx = max(f[x][y], f[y][x]);
	yy = max(f[x][z], f[z][x]);
	zz = max(f[z][y], f[y][z]);
	cout << xx * yy % lsg * zz % lsg << endl;
}
