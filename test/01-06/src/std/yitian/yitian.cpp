#include <bits/stdc++.h>
using namespace std;
int n, f1[10000], f2[10000], ans;
char c[3000][3000];
bool f[10000];
int main()
{
	freopen("yitian.in", "r", stdin);
	freopen("yitian.out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n; i++)
		scanf("%s", c[i] + 1);
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			if (c[i][j] == '#')
				f[j] = true, f1[i]++, f2[j]++;
	ans = -1;
	for (int i = 1; i <= n; i++)
		ans = max(f1[i] + f[i] - 1, ans);
	if (ans == -1)
	{
		cout << -1 << endl;
		return 0;
	}
	ans = n - ans;
	for (int i = 1; i <= n; i++)
		if (f2[i] != n)
			ans++;
	cout << ans << endl;
}
