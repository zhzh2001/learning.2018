#include <bits/stdc++.h>
#define MOD 998244353
using namespace std;
long long ans,f[5005][5005],k[5005],a,b,c;
int main()
{
	freopen("31.in","r",stdin);
	freopen("31.out","w",stdout);
	scanf("%lld%lld%lld",&a,&b,&c);
	long long t=max(max(a,b),c);
	memset(f,sizeof(f),0);
	f[0][0]=1;
	for(int i=1;i<=t;i++)
		for(int j=0;j<=i;j++)
			f[i][j]=(f[i-1][j]+f[i-1][j-1])%MOD;
	k[0]=1;
	for(int i=1;i<=t;i++)
		k[i]=k[i-1]*i%MOD;
	long long p=min(a,b),ab=0,ac=0,bc=0;
	for(int i=1;i<=p;i++)
		ab=(ab+(f[a][i]*f[b][i])%MOD*k[i]%MOD)%MOD;
	p=min(a,c);
	for(int i=1;i<=p;i++)
		ac=(ac+(f[a][i]*f[c][i])%MOD*k[i]%MOD)%MOD;
	p=min(b,c);
	for(int i=1;i<=p;i++)
		bc=(bc+(f[c][i]*f[b][i])%MOD*k[i]%MOD)%MOD;
	ans=((1+ab+bc+ac)%MOD+ab*bc%MOD+ac*bc%MOD+ab*ac%MOD)%MOD;
	ans=ans+ab*ac%MOD*bc%MOD;
	printf("%lld",ans);
}