#include<bits/stdc++.h>
#define LL long long
using namespace std;

int a,b,c;

LL C(int y,int x){
  LL ans=1;
  for (int i=x+1;i<=y;i++) ans=ans*i/(i-x)%998244353;
  return ans%998244353;
}

int main(){
  freopen("31.in","r",stdin);
  freopen("31.out","w",stdout);
  cin>>a>>b>>c;
  if (a<b) swap(a,b);
  if (a<c) swap(a,c);
  if (b<c) swap(b,c);
  LL s1=0,s2=0,s3=0,o=1;
  for (int i=1;i<=min(a,b);i++)
  {
    o*=i;
    s1=(s1+C(a,i)*C(b,i)*o)%998244353;
  }
  o=1;
  for (int i=1;i<=min(a,c);i++)
  {
    o*=i;
    s2=(s2+C(a,i)*C(c,i)*o)%998244353;
  }
  o=1;
  for (int i=1;i<=min(c,b);i++)
  {
    o*=i;
    s3=(s3+C(c,i)*C(b,i)*o)%998244353;
  }
  LL ans=s1+s2+s3+s1*s2+s1*s3+s2*s3+s1*s2*s3+1;
  cout<<ans%998244353;
}
