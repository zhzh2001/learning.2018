#include <fstream>
#include <string>
#include <algorithm>
using namespace std;
ifstream fin("yitian.in");
ofstream fout("yitian.out");
const int N = 1005;
int row[N], col[N];
int main()
{
	int n;
	fin >> n;
	bool flag = false;
	for (int i = 0; i < n; i++)
	{
		string s;
		fin >> s;
		for (int j = 0; j < n; j++)
			if (s[j] == '#')
			{
				flag = true;
				row[i]++;
				col[j]++;
			}
	}
	if (!flag)
	{
		fout << -1 << endl;
		return 0;
	}
	int ans = n * 2;
	for (int i = 0; i < n; i++)
		ans = min(ans, n - row[i] + (col[i] == 0));
	for (int i = 0; i < n; i++)
		ans += col[i] < n;
	fout << ans << endl;
	return 0;
}