#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("qinggang.in");
ofstream fout("qinggang.out");
const int N = 105, INF = 1e9;
int a[N], b[N], f[N][N];
int main()
{
	int n, m;
	fin >> n >> m;
	for (int i = 1; i <= n; i++)
		fin >> a[i] >> b[i];
	int l = 1, r = 2e8;
	while (l < r)
	{
		int mid = (l + r) / 2;
		fill_n(&f[0][0], sizeof(f) / sizeof(int), -INF);
		f[0][0] = 0;
		for (int i = 1; i <= n; i++)
			for (int j = 0; j <= m; j++)
				for (int k = max(j - mid / a[i], 0); k <= j; k++)
					f[i][j] = max(f[i][j], f[i - 1][k] + ((mid - a[i] * (j - k)) / b[i]));
		if (f[n][m] >= m)
			r = mid;
		else
			l = mid + 1;
	}
	fout << r << endl;
	return 0;
}