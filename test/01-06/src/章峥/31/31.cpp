#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("31.in");
ofstream fout("31.out");
const int n = 5000, MOD = 998244353;
int a, b, c, f[n + 5][n + 5];
int main()
{
	fin >> a >> b >> c;
	for (int i = 1; i <= n; i++)
	{
		f[i][1] = i + 1;
		for (int j = 2; j < i; j++)
			f[i][j] = (1ll * f[i - 1][j - 1] * j + f[i - 1][j]) % MOD;
		if (i > 1)
			f[i][i] = (1ll * f[i - 1][i - 1] * i + f[i][i - 1]) % MOD;
	}
	if (c)
		fout << 1ll * f[max(a, b)][min(a, b)] * f[max(a, c)][min(a, c)] % MOD * f[max(b, c)][min(b, c)] % MOD << endl;
	else
		fout << 1ll * f[max(a, b)][min(a, b)] << endl;
	return 0;
}