#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;

double a[200],b[200],wi,lo,x,y;
long long n,m,l,r,mid,nl,nr;

int main()
{
	freopen("qinggang.in","r",stdin);
	freopen("qinggang.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>m;
	for (int i=0;i<n;i++)
	{
		cin>>a[i]>>b[i];
		a[i]=1/a[i];
		b[i]=1/b[i];
	}
	l=0;r=100000000;
	while (l<r)
	{
		mid=(l+r)/2;
		nl=nr=0;
		for (int i=0;i<n;i++)
		{
			wi=a[i]*mid;
			lo=b[i]*mid;
			nl=trunc(nl+wi);
			nr=trunc(nr+lo);
		}
		x=nl;
		y=nr;
		if (-x*m/y+x<=sqrt(m)) l=mid+1;
		else r=mid;
	}
	cout<<r<<endl;
	return 0;
}
