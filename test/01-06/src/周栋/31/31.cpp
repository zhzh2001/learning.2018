#include <iostream>
#include <cstdio>
using namespace std;

const long long MOD=998244353;
long long ans,a,b,c,l[5001],r[5001],x1,x2;
int f[10001][10001],m;

long long q(int k)
{
	int x=1;
	for (int i=1;i<=k;i++)
		x=(x*i)%MOD;
	return x;
}
void dfs(int ll,int rr,int k)
{
	if (ll<0||rr<0) return;
	if (k>a)
	{
		int v=1,ll=b,rr=c;
		for (int i=1;i<k;i++)
		{
			v=v*f[ll][l[i]]%MOD*f[rr][r[i]]%MOD;
			ll-=l[i];
			rr-=r[i];
		}
		x1=(x1+v)%MOD;
		return;
	}
	for (int i=0;i<=1;i++)
		for (int j=0;j<=1;j++)
		{
			l[k]=i;
			r[k]=j;
			dfs(ll-i,rr-j,k+1);
		}
}
int main()
{
	freopen("31.in","r",stdin);
	freopen("31.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>a>>b>>c;
	if (c==0)
	{
		int x=a;
		a=c;
		c=x;
	}
	if (b==0)
	{
		int x=a;
		a=b;
		b=x;
	}
	m=max(max(a+b,b+c),a+c);
	for (int i=0;i<=m;i++)
		for (int j=0;j<=i;j++)
			if (j==0) f[i][j]=1;
			else f[i][j]=(f[i-1][j]+f[i-1][j-1])%MOD;
	x1=0;
	dfs(b,c,1);
	x2=0;
	f[0][1]=1;
	m=min(b,c);
	for (int i=0;i<=m;i++)
		x2=(0ll+x2+f[c][i]*f[b][i]%MOD*q(i)%MOD)%MOD;
	cout<<x1*x2%MOD<<endl;
	return 0;
}
