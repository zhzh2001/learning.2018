#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define Max(x,y) ((x)>(y)?(x):(y))
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 500005,mod = 1e9+7;
char s[N];
int sum,powsm;
int a,b,x,yushu;
int ans,tot;

struct Bignum{
	int a[N],num;
	Bignum(){memset(a,0,sizeof a); num = 1;}
}A;
struct Matrix{
	int a[3][3]; Matrix(){memset(a,0,sizeof a);}
	friend Matrix operator *(Matrix a,Matrix b){
		Matrix ans;
		for(int i = 0;i < 3;++i)
			for(int j = 0;j < 3;++j)
				for(int k = 0;k < 3;++k)
					ans.a[i][j] = (ans.a[i][j]+1LL*a.a[i][k]*b.a[k][j]%mod)%mod;
		return ans;
	}
}MA,MB;
inline Matrix Bqpow(Matrix x,Bignum a){
	Matrix ans; for(int i = 0;i < 3;++i) ans.a[i][i] = 1;
	for(int i = 1;i <= a.num;++i){
		Matrix tmp; int j; for(j = 0;j < 3;++j) tmp.a[j][j] = 1;
		for(j = 1;j <= a.a[i];++j) tmp = tmp*x;
		ans = ans*tmp;
		for(;j <= 10;++j) tmp = tmp*x;
		x = tmp;
	}
	return ans;
}

inline Matrix getans(Matrix a,Matrix b){
	Matrix ans;
	for(int i = 0;i < 3;++i)
		for(int k = 0;k < 3;++k)
			ans.a[i][0] = (ans.a[i][0]+1LL*a.a[i][k]*b.a[k][0]%mod)%mod;
	return ans;
}

signed main(){
	freopen("trifunc.in","r",stdin);
	freopen("trifunc.out","w",stdout);
	a = read(); b = read();
	scanf("%s",s+1); int len = strlen(s+1);
	for(int i = len;i;--i) A.a[i] = s[len-i+1]-'0';
	A.num = len;
	x = sqrt(1LL*b*b-1LL*a*a);
	// printf("%d\n", x);
	MA.a[0][0] = MA.a[1][1] = a;
	MA.a[0][1] = x; MA.a[1][0] = -x;
	MA.a[2][0] = MA.a[2][2] = 1;
	
	MB.a[0][0] = x;
	MB.a[1][0] = a;
	MB.a[2][0] = 0;
	MA = Bqpow(MA,A);
	MB = getans(MA,MB);
	printf("%d\n", MB.a[2][0]);
	return 0;
}