#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define Max(x,y) ((x)>(y)?(x):(y))
#define BIN 100000000
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 1005,mod = 1e9+7;
struct Bignum{
	ll a[N],num;
	Bignum(){memset(a,0,sizeof a); num = 1;}
}A,B;
//BIN压8位
char s[N];

inline ll Bqpow(ll x,Bignum a){
	ll ans = 1;
	for(ll i = 1;i <= a.num;++i){
		for(ll p = 1;p <= 8;++p){
			ll tmp = 1,j;
			for(j = 1;j <= a.a[i]%10;++j) tmp = tmp*x%mod;
			ans = ans*tmp%mod;
			for(;j <= 10;++j) tmp = tmp*x%mod;
				// printf("%lld %lld\n",a.a[i]%10,tmp);
			a.a[i] /= 10;
			x = tmp;
		}
	}
	return ans;
}

signed main(){
	scanf("%s",s+1); ll len = strlen(s+1);
	for(ll i = len;i;--i) A.a[(i-1)/8+1] = A.a[(i-1)/8+1]*10+s[len-i+1]-'0';
	A.num = (len-1)/8+1;
	printf("%lld\n", Bqpow(2,A));
	return 0;
}