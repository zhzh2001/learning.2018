#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 300005;
int d[N],cnt;
int n,g;
int val[N];
int ans[N],Ans;
struct node{
	int day,num,opt;
	friend bool operator <(node a,node b){ return a.day < b.day; }
}a[N];
struct PRQ{
	priority_queue<pair<int,int> > A,B;
	void clean(){ 
		while(!B.empty() && A.top().first == B.top().first){
			pair<int,int> t = A.top();
			A.pop(),B.pop();
			--t.second; if(t.second > 0) A.push(t);
		}
	}
	pair<int,int> top(){ clean(); return A.top(); }
	void push(int x,int n){ A.push(make_pair(x,n)); }
	void erase(int x){ B.push(make_pair(x,1)); }
}Q;

inline int erfen(int x){
	int l = 1,r = cnt;
	while(l <= r){
		int mid = (l+r)>>1;
		if(d[mid] == x) return mid;
		if(d[mid] > x) r = mid-1;
		else l = mid+1;
	}
	return l;
}

signed main(){
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	n = read(); g = read();
	for(int i = 1;i <= n;++i){
		a[i].day = read(); a[i].num = read(); a[i].opt = read();
		d[i] = a[i].num;
	}
	sort(a+1,a+n+1); 
	Q.push(g,n+1);
	sort(d+1,d+n+1);cnt = 1;
	for(int i = 2;i <= n;++i) if(d[i] != d[i-1]) d[++cnt] = d[i];
	for(int i = 1;i <= cnt;++i) val[i] = g;
	for(int i = 1;i <= n;++i){
		if(a[i].opt == 0) continue;
		bool flag = false;
		int t = erfen(a[i].num);
		if(val[t] == Q.top().first) flag = true;
		Q.erase(val[t]); val[t] += a[i].opt;
		Q.push(val[t],1);
		if(val[t] == Q.top().first) flag = true;
		if(flag) ans[++Ans] = a[i].day;
	}
	printf("%d\n",Ans);
	for(int i = 1;i <= Ans;++i) printf("%d\n", ans[i]);
	return 0;
}