#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 1e7,POS = 1e7,mod = 1e9+7,mo2 = 999999937;
const ll M = 1e11;
int cnt;
ll l,r,num;
int pri[N];
ll p[POS+5];
bool vis[N+5];

void getpri(){
	for(int i = 2;i <= N;++i){
		if(!vis[i]) pri[++cnt] = i;
		for(int j = 1;j <= cnt && 1LL*pri[j]*i <= N;++j){
			vis[i*pri[j]] = true;
			if(i%pri[j] == 0) break;
		}
	}
}

signed main(){
	freopen("primehash.in","r",stdin);
	freopen("primehash.out","w",stdout);
	getpri(); puts("1");
	l = read(); r = read(); if(l == 1)++l;
	if(r <= 1e7){
		num = 0;
		for(ll i = l;i <= r;++i){
			if(!vis[i]) p[++num] = i;
		}
		printf("%lld ",num);
		ll now = 1,ans = 0;
		for(ll i = num;i;--i){
			ans = (ans+p[i]*now%mod)%mod;
			now = now*mo2%mod;
		}
		printf("%lld\n", ans);
	}else{
		num = 0;
		for(ll i = l;i <= r;++i){
			bool flag = true;
			for(int j = 1;j <= cnt && 1LL*pri[j]*pri[j] <= i;++j)
				if(i%pri[j] == 0){flag = false; break;}
			if(flag) p[++num] = i;
		}
		ll now = 1,ans = 0; printf("%lld ", num);
		// printf("%d\n",p[1]);
		for(ll i = num;i;--i){
			ans = (ans+p[i]*now%mod)%mod;
			now = now*mo2%mod;
		}
		printf("%lld\n", ans);
	}
	return 0;
}