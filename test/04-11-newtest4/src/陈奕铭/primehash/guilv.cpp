#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 1e6;
const ll M = 1e11;
int cnt;ll num;
int pri[N];
bool vis[N+5];

void getpri(){
	for(int i = 2;i <= N;++i){
		if(!vis[i]) pri[++cnt] = i;
		for(int j = 1;j <= cnt && 1LL*pri[j]*i <= N;++j){
			if(1LL*i*pri[j] <= N) vis[i*pri[j]] = true;
			++num;
			if(i%pri[j] == 0) break;
		}
	}
	printf("%d %lld\n", cnt,num);
	for(int i = 1;i <= cnt;++i) printf("%d,",pri[i]);
}

signed main(){
	freopen("biao.out","w",stdout);
	getpri();
	return 0;
}