#include<cstdio>
#include<cstring>
#include<algorithm>
#include<ctime>
#include<cmath>
#include<set>
#include<map>
#include<vector>
#include<string>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=y;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=y;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,ch=getchar(),res=0;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
int main(){
	freopen("trifunc.in","w",stdout);
	printf("%d %d\n",24,25);
	Rep(i,1,500000) printf("%d",rand()%10);puts("");
}
