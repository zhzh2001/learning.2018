#include<cstdio>
#include<cstring>
#include<algorithm>
#include<ctime>
#include<cmath>
#include<set>
#include<map>
#include<vector>
#include<string>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=y;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=y;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,ch=getchar(),res=0;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int L=500005;
const int Mod=1e9+7;

int a,b,n,Dat[L];
struct Matrix{
	int v[2][2];
	Matrix(){
		memset(v,0,sizeof(v));
	}
};
inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
inline int Pow(int base,int val){
	if (base==0 && val==Mod-2) exit(-1);
	int res=1;
	for (int i=1;i<=val;i*=2,base=1ll*base*base%Mod){
		if (i&val) res=1ll*res*base%Mod;
	}
	return res;
}

inline Matrix operator * (Matrix A,Matrix B){
	Matrix Res;
	Rep(i,0,1) Rep(j,0,1) Rep(k,0,1) Add(Res.v[i][j],1ll*A.v[i][k]*B.v[k][j]%Mod);
	return Res;
}
int main(){
	freopen("trifunc.in","r",stdin);
	freopen("trifunc.out","w",stdout);
	a=read(),b=read();
	int len=0,ch=getchar();
	while (ch<'0' || ch>'9') ch=getchar();
	while (ch>='0' && ch<='9') Dat[++len]=ch-48,ch=getchar();
	
	int c=(int)(sqrt(1ll*b*b-1ll*a*a)+1e-9);
	Matrix R,Base;
	R.v[0][0]=c,R.v[0][1]=a;
	Base.v[0][0]=a,Base.v[0][1]=Mod-c;
	Base.v[1][0]=c,Base.v[1][1]=a;
	
	Matrix Inval,Base_=Base;
	Rep(i,0,1) Inval.v[i][i]=1;
	Rep(i,0,1) Add(Base.v[i][i],Mod-1);
	if (Base.v[0][0]==0){
		Rep(i,0,1) swap(Base.v[0][i],Base.v[1][i]),swap(Inval.v[0][i],Inval.v[1][i]);
	}
	int Kes=1ll*Base.v[1][0]*Pow(Base.v[0][0],Mod-2)%Mod;
	Rep(i,0,1){
		Add(Base.v[1][i],Mod-1ll*Kes*Base.v[0][i]%Mod);
		Add(Inval.v[1][i],Mod-1ll*Kes*Inval.v[0][i]%Mod);
	}
	Kes=1ll*Base.v[0][1]*Pow(Base.v[1][1],Mod-2)%Mod;
	Rep(i,0,1){
		Add(Base.v[0][i],Mod-1ll*Kes*Base.v[1][i]%Mod);
		Add(Inval.v[0][i],Mod-1ll*Kes*Inval.v[1][i]%Mod);
	}
	Rep(i,0,1) Inval.v[0][i]=1ll*Inval.v[0][i]*Pow(Base.v[0][0],Mod-2)%Mod;
	Rep(i,0,1) Inval.v[1][i]=1ll*Inval.v[1][i]*Pow(Base.v[1][1],Mod-2)%Mod;
	Base=Base_;
	Matrix P,Bmat[10];
	Rep(i,0,1) P.v[i][i]=1,Bmat[0].v[i][i]=1;
	Rep(i,1,9) Bmat[i]=Bmat[i-1]*Base;
	Rep(i,1,len){
//		if (i%10000==0) printf("i=%d\n",i);
		Matrix P2=P*P;
		Matrix P4=P2*P2;
		P=P4*P4*P2;
		P=P*Bmat[Dat[i]];
	}
	Rep(i,0,1) Add(P.v[i][i],Mod-1);
	R=R*(P*Inval);
	printf("%d\n",R.v[0][0]);
}
