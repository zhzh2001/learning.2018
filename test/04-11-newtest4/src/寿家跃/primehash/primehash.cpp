#include<cstdio>
#include<cstring>
#include<algorithm>
#include<ctime>
#include<cmath>
#include<set>
#include<map>
#include<vector>
#include<string>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=y;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=y;i--)

using namespace std;

typedef long long ll;

inline ll read(){
	int f=1,ch=getchar();ll res=0;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=10000005;
const int Mod=1e9+7;
const int base=999999937;

int B[N],Pri[N],Sum[N];

inline int isprime(int val){
	if (val==1) return false;
	Rep(i,1,*Pri){
		if (Pri[i]*Pri[i]>val) return true;
		if (val%Pri[i]==0) return false;
	}
	return true;
}
inline ll Solve(ll n,int m){
	if (!m) return n ;
	if (m==1) return n-n/2;
	if (n<=N-5){
		if (Sum[n]<=m) return 1;
		if (Sum[(int)(sqrt(n)+1e-9)]<=m) return Sum[n]-m+1;
	}
	return Solve(n,m-1)-Solve(n/Pri[m],m-1);
}
inline ll Cal(ll n){
	if (n<=1) return 0;
	int Sqn=(int)(sqrt(n)+1e-9);
	return Sum[Sqn]+Solve(n,Sum[Sqn])-1;
}
int main(){
	freopen("primehash.in","r",stdin);
	freopen("primehash.out","w",stdout);
	Rep(i,2,N-5){
		if (!B[i]) Pri[++*Pri]=i;
		for (int j=1,k;j<=*Pri && (k=Pri[j]*i)<=N-5;j++){
			B[k]=true;
			if (i%Pri[j]==0) break;
		}
	}
	Sum[1]=0;
	Rep(i,2,N-5) Sum[i]=Sum[i-1]+(B[i]?0:1);
	ll L=read(),R=read();
//	/*
	if (R-L<=1000){
		int res=0,ans=0;
		Rep(i,L,R) if (isprime(i)) ans++,res=(1ll*res*base+i)%Mod;
		printf("%d %d\n",ans,res);
		return 0;
	}
//	*/
	printf("%lld %d\n",Cal(R)-Cal(L-1),0);
}
