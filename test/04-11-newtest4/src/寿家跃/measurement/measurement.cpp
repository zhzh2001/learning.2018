#include<cstdio>
#include<cstring>
#include<algorithm>
#include<ctime>
#include<cmath>
#include<set>
#include<map>
#include<vector>
#include<string>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=y;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=y;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,ch=getchar(),res=0;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=300005;

map<int,int>Boz;int cnt;
map<int,int>Map;
multiset<int>Set;
vector<int>Ans;

int hashval[N],Gra[N];
int n,g;
struct node{
	int day,pos,val;
}Q[N];
inline bool Cmp(node A,node B){
	return A.day<B.day;
}
int main(){
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	n=read(),g=read();
	Rep(i,1,n){
		Q[i].day=read(),Q[i].pos=read(),Q[i].val=read();
	}
	sort(Q+1,Q+n+1,Cmp);
	Rep(i,1,n) if (!Boz[Q[i].pos]){
		Boz[Q[i].pos]=++cnt;
	}
	Rep(i,1,cnt){
		hashval[i]=rand();
		Set.insert(Gra[i]=g);
		Map[g]^=hashval[i];
	}
	Rep(i,1,n){
		int pos=Boz[Q[i].pos];
		int lastmax=*(--Set.end()),lastval=Map[lastmax];
		Set.erase(Set.find(Gra[pos]));
		Map[Gra[pos]]^=hashval[pos];
		
		Set.insert(Gra[pos]+=Q[i].val);
		Map[Gra[pos]]^=hashval[pos];
		int nowmax=*(--Set.end()),nowval=Map[nowmax];
		if (nowmax!=lastmax) Ans.push_back(Q[i].day);
			else if (nowval!=lastval) Ans.push_back(Q[i].day);
	}
	printf("%d\n",Ans.size());
	Rep(i,0,Ans.size()-1) printf("%d\n",Ans[i]);
}
