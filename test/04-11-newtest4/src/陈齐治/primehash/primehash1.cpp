#include<stdio.h>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const unsigned int P=1e9+7,B=999999937;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
ll n,sm,ans;
bool v[1500000001];
int main(){
	freopen("primehash.in","r",stdin);
	freopen("primehash1.out","w",stdout);
	ll i,j;
	n=3000000000ll;
	sm=1;ans=2;
	for(i=3;i<=n;i+=2){
		if(!v[i/2]){
			sm++;
			ans=1ll*ans*B%P;
			ans=(ans+i)%P;
		}
		for(j=i+i;j<=n;j+=i)
		if(j&1)v[j/2]=1;
		if((i+1)%3000000==0){
			printf("{%lld,%lld},",sm,ans);
		}
	}
	return 0;
}
