#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100000100,P=1e9+7,B=999999937;
inline ll read(){
	ll x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,Sm,p[N],v[N];
int main(){
	freopen("primehash.in","r",stdin);
	freopen("primehash2.out","w",stdout);
	register int i,j;
	ll l,r,smr,sml,ansr,ansl;
	l=read();r=read();
	n=r;smr=ansr=0;
	for(i=2;i<=n;i++){
		if(!v[i]){
			p[++Sm]=i;
			if(i>=l){
				smr++;
				ansr=1ll*ansr*B%P;
				ansr=(ansr+i)%P;
			}
		}
		for(j=1;j<=Sm&&1ll*p[j]*i<=n;j++){
			v[p[j]*i]=1;
			if(i%p[j]==0)break;
		}
	}
	write(smr);putchar(' ');
	write(ansr);putchar('\n');
	return 0;
}
