#include<stdio.h>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=300100;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
struct ls{int x,*i;}a[N];
inline bool operator<(ls i,ls j){return i.x<j.x;}
struct cqz{int t,i,k;}q[N];
inline bool operator<(cqz i,cqz j){return i.t<j.t;}
int n,m,G,mx,w[N],tr[N*4],sm[N*4],ans,res[N];
int main(){
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	register int i,x,y,k;
	m=read();G=read();
	for(i=1;i<=m;i++){
		q[i].t=read();
		a[i].x=read();a[i].i=&q[i].i;
		q[i].k=read();
	}
	
	n=1;
	sort(a+1,a+m+1);
	for(i=1;i<=m;i++){
		*a[i].i=n;
		n+=(a[i].x!=a[i+1].x);
	}
	
	for(mx=1;mx<n;mx<<=1);mx--;
	for(i=1;i<=n;i++)tr[mx+i]=G,sm[mx+i]=1,w[i]=G;
	for(i=mx;i;i--)tr[i]=G,sm[i]=sm[i<<1]+sm[i<<1|1];
	
	sort(q+1,q+m+1);
	for(i=1;i<=m;i++){
		x=tr[1];y=sm[1];
		tr[mx+q[i].i]+=q[i].k;
		w[q[i].i]+=q[i].k;
		for(k=(mx+q[i].i)/2;k;k/=2){
			tr[k]=max(tr[k<<1],tr[k<<1|1]);sm[k]=0;
			tr[k<<1|1]==tr[k]?sm[k]+=sm[k<<1|1]:0;
			tr[k<<1]==tr[k]?sm[k]+=sm[k<<1]:0;
		}if(tr[1]!=x||sm[1]!=y)res[++ans]=q[i].t;
		if(q[i].k<0&&tr[1]==w[q[i].i]&&sm[1]==1)ans--;
	}printf("%d\n",ans);
	for(i=1;i<=ans;i++)printf("%d\n",res[i]);
	return 0;
}
