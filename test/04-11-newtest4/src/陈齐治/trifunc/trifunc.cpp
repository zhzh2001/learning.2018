#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=500100,P=1e9+7;
int a,b,c,m,x[3],X[3],y[3][3],yy[3][3],z[3][3];char s[N];
int main(){
	freopen("trifunc.in","r",stdin);
	freopen("trifunc.out","w",stdout);
	register int i,j,k,l;
	scanf("%d%d",&a,&b);
	c=sqrt(1.0*b*b-1.0*a*a+0.1);
	while(1ll*a*a+1ll*c*c<1ll*b*b)c++;
	while(1ll*a*a+1ll*c*c>1ll*b*b)c--;
	scanf("%s",s+1);m=strlen(s+1);
	x[0]=c;x[1]=a;
	y[0][0]=y[1][1]=a;
	y[0][2]=y[2][2]=yy[2][2]=1;
	y[1][0]=c;y[0][1]=P-c;
	for(;m;m--){
		s[m]-='0';
		for(i=0;i<3;i++)for(j=0;j<3;j++)
		z[i][j]=y[i][j],y[i][j]=(i==j?1:0);
		for(l=0;l<s[m];l++){
			for(i=0;i<2;i++)
			for(j=0;j<3;j++){
				yy[i][j]=0;
				for(k=0;k<3;k++)
				(yy[i][j]+=1ll*y[i][k]*z[k][j]%P)%=P;
			}swap(y,yy);
		}
		for(i=0;i<3;i++){
			X[i]=0;
			for(k=0;k<3;k++)
			(X[i]+=1ll*x[k]*y[k][i]%P)%=P;
		}
		for(i=0;i<3;i++)x[i]=X[i];
		for(l=s[m];l<10;l++){
			for(i=0;i<2;i++)
			for(j=0;j<3;j++){
				yy[i][j]=0;
				for(k=0;k<3;k++)
				(yy[i][j]+=1ll*y[i][k]*z[k][j]%P)%=P;
			}swap(y,yy);
		}
	}
	printf("%d\n",x[2]);
	return 0;
}
