#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int mod = 1000000007;
const int fck = 999999937;
ll hsh[3500], ans[3500], size = 3000000000ll;
bitset<60000> mark;
int pri[60000], cnt;
void prepare() {
  mark[0] = mark[1] = 1;
  for (int i = 2; i < 60000; i++) {
    if (!mark[i]) pri[++ cnt] = i;
    for (int j = 1; j <= cnt && i * pri[j] < 60000; j++) {
      mark[i * pri[j]] = 1;
      if (i % pri[j]) break;
    }
  }
}
bool isprime(ll x) {
  if (x < 60000) return !mark[x];
  for (int i = 1; i <= cnt && pri[i] * pri[i] <= x; i++)
    if (x % pri[i] == 0) return false;
  return true;
}
int main(int argc, char const *argv[]) {
  prepare();
  for (ll i = 0; i <= 200; i++) {
    ll l = i * 1000000, r = (i+1)*1000000-1;
    for (ll j = l; j <= r; j++) {
      if (isprime(j)) {
        ans[i] ++;
        hsh[i] = (hsh[i] * fck % mod + j) % mod;
      }
    }
    printf("finish work %d\n", i);
  }
  freopen("output.cpp", "w", stdout);
  printf("int p[]={");
  for (int i = 0; i <= 3000; i++)
    printf("%d,", ans[i]);
  printf("};\n");

  printf("int h[]={");
  for (int i = 0; i <= 3000; i++)
    printf("%d,", hsh[i]);
  printf("};\n");
  return 0;
}