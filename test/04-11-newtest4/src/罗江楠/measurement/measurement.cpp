#include <bits/stdc++.h>
#include <ext/pb_ds/priority_queue.hpp>
#define N 300020

namespace IOStream {
  class InputStream {
  private:
    static const size_t defaultBufsz = 1e6;
    FILE *stream;
    size_t bufsz;
    char *buf, *p;
    bool good;
    void fetch() {
      if (!buf)
        p = buf = new char [bufsz+1];
      size_t sz = fread(buf,1,bufsz,stream);
      p = buf;
      buf[sz] = '\0';
      good = sz;
    }
    char nextchar() {
      if (!*p) fetch();
      return *p++;
    }
    template <typename Int>
    void readint(Int& x) {
      char c;
      for (c = nextchar(); isspace(c); c = nextchar())
        ;
      x = 0;
      Int sign = 1;
      if (c == '-')
        sign = -1, c = nextchar();
      if (c == '+')
        c = nextchar();
      for (; isdigit(c); c = nextchar())
        x = x * 10 + c - '0';
      x *= sign;
    }
  public:
    InputStream():
    stream(NULL),
    bufsz(defaultBufsz),
    buf(NULL),
    p(NULL),
    good(false) {
    }
    explicit InputStream(const std::string& filename, size_t bufsz = defaultBufsz):
    stream(fopen(filename.c_str(), "r")),
    bufsz(bufsz),
    buf(NULL),
    p(NULL),
    good(false) {
      fetch();
    }
    explicit InputStream(FILE *stream, size_t bufsz = defaultBufsz):
    stream(stream),
    bufsz(bufsz),
    buf(NULL),
    p(NULL),
    good(false) {
      fetch();
    }
    bool close() {
      return !fclose(stream);
    }
    ~InputStream() {
      close();
      delete [] buf;
    }
    operator bool () const {
      return good;
    }
    bool operator ! () const {
      return !good;
    }
    bool open(const std::string& filename, size_t bufsz = defaultBufsz) {
      stream = fopen(filename.c_str(), "r");
      this -> bufsz = bufsz;
      fetch();
      return stream;
    }
    bool open(FILE *stream, size_t bufsz = defaultBufsz) {
      this -> stream = stream;
      this -> bufsz = bufsz;
      fetch();
      return stream;
    }
    InputStream& operator >> (short& value) {
      readint(value);
      return *this;
    }
    InputStream& operator >> (unsigned short& value) {
      readint(value);
      return *this;
    }
    InputStream& operator >> (int& value) {
      readint(value);
      return *this;
    }
    InputStream& operator >> (unsigned int& value) {
      readint(value);
      return *this;
    }
    InputStream& operator >> (long& value) {
      readint(value);
      return *this;
    }
    InputStream& operator >> (unsigned long& value) {
      readint(value);
      return *this;
    }
    InputStream& operator >> (long long& value) {
      readint(value);
      return *this;
    }
    InputStream& operator >> (unsigned long long& value) {
      readint(value);
      return *this;
    }
    InputStream& operator >> (char& c) {
      for (c = nextchar(); isspace(c); c = nextchar())
        ;
      return *this;
    }
    InputStream& operator >> (std::string& s) {
      char c;
      for (c = nextchar(); isspace(c); c = nextchar())
        ;
      s.clear();
      for (; good && !isspace(c); c = nextchar())
        s += c;
      return *this;
    }
    friend InputStream& getline(InputStream& is, std::string& s, char delim = '\n');
  };
  InputStream& getline(InputStream& is, std::string& s, char delim) {
    char c;
    s.clear();
    for (c = is.nextchar(); is.good && c != delim; c = is.nextchar())
      s += c;
    return is;
  }

  class OutputStream {
  private:
    static const size_t defaultBufsz = 1e6;
    FILE *stream;
    size_t bufsz;
    char *buf, *p, dig[25];
  public:
    OutputStream():
    stream(NULL),
    bufsz(defaultBufsz),
    buf(NULL),
    p(NULL) {
    }
    explicit OutputStream(const std::string& filename, size_t bufsz = defaultBufsz):
    stream(fopen(filename.c_str(), "w")),
    bufsz(bufsz),
    buf(new char [bufsz]),
    p(buf){
    }
    explicit OutputStream(FILE *stream, size_t bufsz = defaultBufsz):
    stream(stream),
    bufsz(bufsz),
    buf(new char [bufsz]),
    p(buf){
    }
    bool close() {
      return !fclose(stream);
    }
    void flush() {
      fwrite(buf, 1, p - buf, stream);
      p = buf;
    }
  private:
    void writechar(char c) {
      *p ++ = c;
      if (p == buf + bufsz)
        flush();
    }
    template <typename Int>
    void writeint(Int x) {
      if (x < 0)
        writechar('-'), x = -x;
      int len = 0;
      do
        dig[++ len] = x % 10;
      while (x /= 10);
      for (; len; len --)
        writechar(dig[len] + '0');
    }
  public:
    ~OutputStream() {
      flush();
      close();
      delete [] buf;
    }
    bool open(const std::string& filename, size_t bufsz = defaultBufsz) {
      stream = fopen(filename.c_str(), "w");
      this -> bufsz = bufsz;
      delete [] buf;
      p = buf = new char [bufsz];
      return stream;
    }
    bool open(FILE *stream, size_t bufsz = defaultBufsz) {
      this -> stream = stream;
      this -> bufsz = bufsz;
      delete [] buf;
      p = buf = new char [bufsz];
      return stream;
    }
    OutputStream& operator << (short value) {
      writeint(value);
      return *this;
    }
    OutputStream& operator << (unsigned short value) {
      writeint(value);
      return *this;
    }
    OutputStream& operator << (int value) {
      writeint(value);
      return *this;
    }
    OutputStream& operator << (unsigned int value) {
      writeint(value);
      return *this;
    }
    OutputStream& operator << (long value) {
      writeint(value);
      return *this;
    }
    OutputStream& operator << (unsigned long value) {
      writeint(value);
      return *this;
    }
    OutputStream& operator << (long long value) {
      writeint(value);
      return *this;
    }
    OutputStream& operator << (unsigned long long value) {
      writeint(value);
      return *this;
    }
    OutputStream& operator << (char c) {
      writechar(c);
      return *this;
    }
    OutputStream& operator << (const std::string& s) {
      for (size_t i = 0; i < s.length(); i++)
        writechar(s[i]);
      return *this;
    }
    OutputStream& operator << (OutputStream& (*func) (OutputStream&)) {
      return func(*this);
    }
    friend OutputStream& endl(OutputStream& os);
  };
  OutputStream& endl(OutputStream& os) {
    os.writechar('\n');
    return os;
  }
}

IOStream::InputStream fin("measurement.in");
IOStream::OutputStream fout("measurement.out");

int n, g;
/**
 * I love pbds.
 * @param {number} first Output of a cow
 * @param {number} second Id of a cow
 */
typedef typename __gnu_pbds::priority_queue<int> fuck_pbds;
fuck_pbds p;
std::map<int, fuck_pbds::point_iterator> ps;
struct task {
  int day, id, value;
  friend bool operator < (const task &a, const task &b) {
    return a.day < b.day;
  }
}tsk[N];
std::vector<int> res;
std::map<int, int> op;
int main(int argc, char const *argv[]) {
  fin >> n >> g;
  p.push(g);
  for (int i = 1; i <= n; i++) {
    fin >> tsk[i].day >> tsk[i].id >> tsk[i].value;
  }
  std::sort(tsk + 1, tsk + n + 1);
  for (int i = 1; i <= n; i++) {
    int mx = p.top();
    int &day = tsk[i].day, &id = tsk[i].id, &value = tsk[i].value;
    if (!op.count(id)) {
      op[id] = g;
      ps[id] = p.push(g);
    }
    int last = op[id];
    int now = last + value;
    p.erase(ps[id]);
    int another = p.top(); // rk2
    ps[id] = p.push(now);
    op[id] = now;
    int nmx = p.top();
    bool change = false;
    if (value > 0) {
      // 涨分
      // change |= another > now                 // false;
      change |= another == now;                  // true;
      change |= another < now && another > last; // true;
      change |= another == last;                 // true;
      // change |= another < last                // false;
    } else {
      // 掉分
      // change |= another > last                // false;
      change |= another == last;                 // true;
      change |= another < last && another > now; // true;
      change |= another == now;                  // true;
      // change |= another < now                 // false;
    }
    if (change) {
      res.push_back(day);
    }
  }
  fout << res.size() << IOStream::endl;
  for (int day : res) {
    fout << day << IOStream::endl;
  }
  return 0;
}
