#include <bits/stdc++.h>
#include <fstream>
using namespace std;

ifstream fin("trifunc.in");
ofstream fout("trifunc.out");
const int mod = 1000000007;
double a, b, c, n;
int main(int argc, char const *argv[]) {
  fin >> b >> c >> n;
  a = sqrt(c * c - b * b);
  string lastsin = "a", lastcos = "b";
  string ans = lastsin;
  for (int i = 2; i <= n; i++) {
    string nowsin = ("("+lastsin+")*b") + "+" + ("("+lastcos+")*a");
    string nowcos = ("("+lastcos+")*b") + "-" + ("("+lastsin+")*a");
    ans += "\n" + nowsin;
    lastsin = nowsin;
    lastcos = nowcos;
  }
  cout << ans << endl;
  return 0;
}
/*

sin2x = 2sinxcosx
c^2i sin2x = 2 (c^i sinx) (c^i /(1-sin^x))
           = 2c^2i sinx(1 - sin^x)
           = 2c^

ans = \sum i∈[1..n] c^i*sin(ix)


*/