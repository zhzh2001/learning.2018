#include <bits/stdc++.h>
#include <fstream>
using namespace std;

ifstream fin("trifunc.in");
ofstream fout("trifunc.out");
const int mod = 1000000007;
double a, b, c, n;
int main(int argc, char const *argv[]) {
  fin >> b >> c >> n;
  a = sqrt(c * c - b * b);
  double lastsin = a, lastcos = b;
  double ans = lastsin;
  for (int i = 2; i <= n; i++) {
    double nowsin = lastsin * b + lastcos * a;
    double nowcos = lastcos * b - lastsin * a;
    ans += nowsin;
    lastsin = nowsin;
    lastcos = nowcos;
  }
  string x = std::to_string(std::round(ans));
  bool f = 0;
  long long res = 0;
  for (char c : x) {
    if (c == '-') {
      f = 1;
    } else if (c == '.') {
      break;
    } else {
      res = ((res << 1) + (res << 3) + c - '0') % mod;
    }
  }
  if (f) res = (mod - res) % mod;
  fout << res << endl;
  return 0;
}
/*




*/