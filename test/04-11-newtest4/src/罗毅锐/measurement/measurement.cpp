#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
const int inf=0x7fffffff;
const int N=100010;
const int M=1010;
inline int read(){
    int x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
int mk[300010];
struct data{int dy,num,d;}opt[300010];
map<int,int> mp;
int n,g,lst,gnum;
int ans;
struct NODE{
	int num;
	bool operator<(const NODE&b)const{
		if(mk[mp[num]]>mk[mp[b.num]])return true;
		else if(mk[mp[num]]==mk[mp[b.num]])return num<b.num;
		else return false;
		/*if(vis.count(num)){
			if(vis.count(b.num)){
				if(mk[mp[num]]>mk[mp[b.num]])return true;
				else if(mk[mp[num]]==mk[mp[b.num]])return num<b.num;
				else return false;
			}
			else{
				if(mk[mp[num]]>g)return true;
				else if(mk[mp[num]]==g)return num<b.num;
				else return false;
			}
		}
		else{
			if(vis.count(b.num)){
				if(g>mk[mp[b.num]])return true;
				else if(g==mk[mp[b.num]])return num<b.num;
				else return false;
			}
			else return num<b.num;
		}*/
	}
};
bool cmp(data a,data b){return a.dy<b.dy;}
set <NODE>st;
int q[300010];
//queue<int >q;
int main(){
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	n=read();g=read();
	for(int i=1;i<=n;i++){opt[i].dy=read();opt[i].num=read();opt[i].d=read();}
	sort(opt+1,opt+n+1,cmp);
	for(int i=1;i<=n;i++)mp[opt[i].num]=i;
	for(int i=1;i<=n;i++)st.insert((NODE){opt[i].num});
	for(int i=1;i<=n;i++)mk[mp[opt[i].num]]=g;
	lst=g,gnum=n;
	for(int i=1;i<=n;i++){
		int num=opt[i].num,d=opt[i].d,dy=opt[i].dy;
		if(d==0)continue;
		st.erase(st.find((NODE){num}));
		if(mk[mp[num]]==g)gnum--;
		mk[mp[num]]+=d;
		if(mk[mp[num]]==g)gnum++;
		st.insert((NODE){num});
		if(mk[mp[num]]-d==lst){
			q[++ans]=dy;//q.push(dy);
			if(d>0)lst=mk[mp[num]];
			else{
				set<NODE>:: iterator f=st.begin();
				lst=(*f).num;
				lst=mk[mp[lst]];
			}
		}
		else{
			if(mk[mp[num]]>lst){
				q[++ans]=dy;//q.push(dy);
				lst=mk[mp[num]];
			}
			else if(mk[mp[num]]==lst)q[++ans]=dy;//q.push(dy);
		}
	}
	printf("%d\n",ans);
	for(int i=1;i<=ans;i++)printf("%d\n",q[i]);
	/*while(!q.empty()){
		printf("%d\n",q.front());
		q.pop();
	}*/
	return 0;
}
