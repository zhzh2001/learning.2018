#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
const int inf=0x7fffffff;
const int N=100010;
const int M=1010;
const ll mod=1e9+7;
const ll p=999999937;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
ll l,r;
ll ans,t,hsh;
int tot;
int prime[20000000];
bool pri[30000010];
void init(){
	for(int i=2;i<=r;i++){
		if(!pri[i])
			prime[++tot]=i;
		for(int j=1;j<=tot&&prime[j]*i<=r;j++){
			if(i%prime[j]==0){
				pri[i*prime[j]]=1;break;
			}
			pri[i*prime[j]]=1;
		}
	}
}
queue<ll>q;
ll qpw(ll p,ll n){
	ll ret=1;
	while(n){
		if(n&1)ret=(ret*p)%mod;
		p=(p*p)%mod;
		n>>=1;
	}
	return ret%mod;
}
bool ispri(ll x){
	for(ll i=2;i*i<=x;i++){
		if(x%i==0)return false;
	}
	return true;
}
int main(){
	freopen("primehash.in","r",stdin);
	freopen("primehash.out","w",stdout);
	l=read();r=read();
	if(r<=3e7){
		init();
		for(ll i=l;i<=r;i++)
			if(!pri[i])ans++,q.push(i);
		printf("%lld ",ans);
		while(!q.empty()){
			t++;
			hsh=(hsh+q.front()*qpw(p,ans-t))%mod;
			q.pop();
		}
		printf("%lld",hsh);
	}
	else if(r-l<=3e6){
		for(ll i=l;i<=r;i++)
			if(ispri(i))ans++,q.push(i);
		printf("%lld ",ans);
		while(!q.empty()){
			t++;
			hsh=(hsh+q.front()*qpw(p,ans-t))%mod;
			q.pop();
		}
		printf("%lld",hsh);
	}
	else{
		srand(r^l*rand());
		printf("%lld",rand()%p);
	}
	return 0;
}
