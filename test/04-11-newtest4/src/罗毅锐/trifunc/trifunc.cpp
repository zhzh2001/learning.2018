#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
const int inf=0x7fffffff;
const int N=100010;
const int M=1010;
const ll mod=1e9+7;
const ll p=999999937;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
ll qpw(ll p,ll n){
	ll ret=1;
	while(n){
		if(n&1)ret=(ret*p)%mod;
		p=(p*p)%mod;
		n>>=1;
	}
	return ret%mod;
}
ll a,b;
ll n;
long double ans,s[361];
char ch[300010];
int main(){
	freopen("trifunc.in","r",stdin);
	freopen("trifunc.out","w",stdout);
	a=read();b=read();cin>>n;
	long double x=acos((long double)a/b);
	for(int i=1;i<=min(360ll,n);i++)s[i]=sin(x*i);
	for(int i=1;i<=n;i++)ans=ans+s[i%360]*qpw(b,i);
	printf("%lld",(ll)(ans+0.5)%mod);
	return 0;
}
