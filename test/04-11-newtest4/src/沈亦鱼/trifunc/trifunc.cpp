#include<cmath>
#include<cstdio>
using namespace std;
int mo=1000000007,aa,bb,n,lb;
double a,b,c,lc,la,s;
int main(){
	freopen("trifunc.in","r",stdin);
	freopen("trifunc.out","w",stdout);
	scanf("%d%d%d",&aa,&bb,&n);
	a=aa;
	b=bb;
	c=acos(a/b);
	lc=0;
	lb=1;
	for(int i=1;i<=n;i++){
		lb=lb*bb%mo;
		lc+=c;
		la=sin(lc);
		s+=la*lb;
		while(s>=1e9+7)s-=mo;
		while(s<=-(1e9+7))s+=mo;
	}
	printf("%d",int(s+0.5)%mo);
	return 0;
}
