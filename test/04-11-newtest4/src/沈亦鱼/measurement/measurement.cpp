#include<cstdio>
#include<iostream>
using namespace std;
int n,g,x,y,z,k,num,ans,la,ti[310000],a[310000],b[310000],c[1100000],id[3100000],tr[3100000][11],s[310000];
void sor(int l,int r){
	int x=ti[(l+r)>>1],i=l,j=r;
	while(i<=j){
		while(ti[i]<x)i++;
		while(x<ti[j])j--;
		if(i<=j){
			swap(ti[i],ti[j]);
			swap(a[i],a[j]);
			swap(b[i],b[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}
void bid(int k,int l,int r){
	if(l==r){
		c[k]=g;
		return;
	}
	int mid=(l+r)>>1;
	if(l<=mid)bid(k*2,l,mid);
	if(mid<r)bid(k*2+1,mid+1,r);
	c[k]=max(c[k*2],c[k*2+1]);
}
void cag(int k,int l,int r,int loc,int y){
	if(l==r){
		x=c[k];
		c[k]+=y;
		return;
	}
	int mid=(l+r)>>1;
	if(loc<=mid)cag(k*2,l,mid,loc,y);
	else cag(k*2+1,mid+1,r,loc,y);
	c[k]=max(c[k*2],c[k*2+1]);
}
int main(){
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	k=1;
	scanf("%d%d",&n,&g);
	for(int i=1;i<=n;i++){
		scanf("%d%d%d",&ti[i],&y,&b[i]);
		x=1;
		while(y){
			z=y%10;
			if(tr[x][z]>0)x=tr[x][z];
			else{
				k++;
				tr[x][z]=k;
				x=k;
			}
			y/=10;
		}
		if(id[x])a[i]=id[x];
		else{
			num++;
			id[x]=num;
			a[i]=num;
		}
	}
	num++;
	bid(1,1,num);
	sor(1,n);
	la=g;
	for(int i=1;i<=n;i++){
		cag(1,1,num,a[i],b[i]);
		if(x==la){
			if(b[i]>0){
				la=x+b[i];
				ans++;
				s[ans]=ti[i];
			}
			if(b[i]<0){
				la=c[1];
				ans++;
				s[ans]=ti[i];
			}
			continue;
		}
		if(x+b[i]==la){
			ans++;
			s[ans]=ti[i];
			continue;
		}
		if(x+b[i]>la){
			la=x+b[i];
			ans++;
			s[ans]=ti[i];
			continue;
		}
	}
	printf("%d\n",ans);
	for(int i=1;i<=ans;i++)
		printf("%d\n",s[i]);
	return 0;
}
/*
4 10
7 3 +3
4 2 -1
9 3 -1
1 1 +2
*/
