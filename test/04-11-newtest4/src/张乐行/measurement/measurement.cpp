#include<cstdio>
#include<map>
#include<set>
#include<algorithm>
using namespace std;
const int N=3e5+5;
int n,m,i,an,ans[N];
struct arr{int t,x,k;}a[N];
bool operator < (arr A,arr B){return A.t<B.t;}
multiset<int> q;
map<int,int> f;
int read(){
	char c=getchar();int k=0,p=0;
	for (;c<48||c>57;c=getchar()) if (c=='-') p=1;
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;
	return p?-k:k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int get(){
	multiset<int>::iterator it;
	it=q.end();it--;return *it;
}
int main(){
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	n=read();m=read();
	for (i=1;i<=n;i++)
		a[i]=(arr){read(),read(),read()};
	sort(a+1,a+n+1);q.insert(m);
	for (i=1;i<=n;i++) if (a[i].k){
		int x=a[i].x,k=a[i].k,v;
		if (f.find(x)==f.end()) v=f[x]=m;
		else v=f[x];
		f[x]+=k;
		int mx=get();
		q.erase(q.find(v));
		if (v==m) q.insert(m);
		int tmx=get();
		q.insert(v+k);
		if (v==mx){
			if (v+k>tmx&&tmx!=mx) continue;
		}
		else{
			if (v+k<tmx) continue;
		}
		ans[++an]=a[i].t;
	}
	write(an);putchar('\n');
	for (i=1;i<=an;i++)
		write(ans[i]),putchar('\n');
}
