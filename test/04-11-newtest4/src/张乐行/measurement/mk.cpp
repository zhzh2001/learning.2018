#include<cstdio>
#include<ctime>
#include<map>
#include<algorithm>
using namespace std;
const int X=1e9,K=1000+1,T=1e9;
int n,m,i;
map<int,bool> mp;
int ran(){return (rand()<<15)+rand();}
int main(){
	freopen("measurement.in","w",stdout);
	srand(time(0));rand();
	n=3e5;m=5e8;
	printf("%d %d\n",n,m);
	for (i=1;i<=n;i++){
		int t;
		while (t=ran()%T+1,mp.find(t)!=mp.end());
		mp[t]=1;
		int x=ran()%X+1,k=ran()%K;
		if (rand()%2) k=-k;
		printf("%d %d %d\n",t,x,k);
	}
}
