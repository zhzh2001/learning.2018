#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;
const int N=1e6;
bool vis[N];int t,p[N],i,j;
int m,top,pos[N],lt[N];ll stk[N],f[N];
void init(int n){
	for (t=0,i=1;i<=n;i++) vis[i]=0;
	for (i=2;i<=n;i++) if (!vis[i]){
		p[++t]=i;
		for (j=i+i;j<=n;j+=i) vis[j]=1;
	}
}
ll cal(ll n){
	if (!n) return 0;
	int m=sqrt(n);init(m);top=0;
	for (ll i=1;i<=n;i=n/(n/i)+1)
		stk[++top]=n/i;
	reverse(stk+1,stk+top+1);
	int i,j;
	for (i=1;i<=top;i++) if (stk[i]>m)
		pos[n/stk[i]]=i;
	for (i=1;i<=top;i++) f[i]=stk[i],lt[i]=0;
	int st=1;
	for (i=1;i<=t;i++){
		int pn=p[i];ll pt=(ll)pn*pn;
		for (;stk[st]<pt;st++);
		for (j=top;j>=st;j--){
			ll x=stk[j]/pn;
			int px=x<=m?x:pos[n/x];
			f[j]=f[j]-(f[px]-(i-lt[px]-1));
			lt[j]=i;
		}
	}
	return f[top]-1+t;
}
int main(){
	freopen("zg.in","r",stdin);
	ll l,r;
	scanf("%lld%lld",&l,&r);
	printf("%lld",cal(r)-cal(l-1));
}
