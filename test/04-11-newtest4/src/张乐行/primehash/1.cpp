#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
const int B=999999937,mo=1e9+7;
const int M=1e6,N=1e8+5,T=1e5,TU=8e7;
const ll LIM=1e11;
int t,p[M],i,j,cnt,k;
bool f[N];
void shake(ll l,ll r){
	for (int j=0;j<=r-l;j++) f[j]=0;
	for (int i=1;(ll)p[i]*p[i]<=r;i++){
		int x=p[i];ll j=(l+x-1)/x*x;
		if (j==x) j+=x;
		for (;j<=r;j+=x) f[j-l]=1;
	}
	for (int j=0;j<=r-l;j++) if (!f[j]){
		cnt++;k=((ll)k*B+j+l)%mo;
	}
}
int main(){
	for (i=2;i<=M;i++) if (!f[i]){
		p[++t]=i;
		for (j=i+i;j<M;j+=i) f[j]=1;
	}
	ll l=9325064354ll,r=l+TU;
	for (ll i=l;i<=r;i+=T){
		shake(i,min(i+T-1,r));
	}
	printf("%d %d\n",cnt,k);
}
