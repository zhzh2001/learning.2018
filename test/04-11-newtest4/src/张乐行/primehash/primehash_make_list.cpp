#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
const int B=999999937,mo=1e9+7;
const int M=1e6,N=1e8+5,T=1e5,TU=8e7;
const ll LIM=1e11;
int t,p[M],i,j,cnt,k;
bool f[N];
void shake(ll l,ll r){
	for (int j=0;j<=r-l;j++) f[j]=0;
	for (int i=1;(ll)p[i]*p[i]<=r;i++){
		int x=p[i];ll j=(l+x-1)/x*x;
		if (j==x) j+=x;
		for (;j<=r;j+=x) f[j-l]=1;
	}
	for (int j=0;j<=r-l;j++) if (!f[j]){
		cnt++;k=((ll)k*B+j+l)%mo;
	}
}
int main(){
	freopen("1.in","w",stdout);
	for (i=2;i<=M;i++) if (!f[i]){
		p[++t]=i;
		for (j=i+i;j<M;j+=i) f[j]=1;
	}
	for (ll i=1;i<=LIM;i+=T){
		shake(i==1?2:i,i+T-1);
		if ((i+T-1)%TU==0){
			printf("%d %d\n",cnt,k);
			cnt=0;k=0;
		}
	}
}
