#include<cstdio>
#define ll long long
const int mo=1e9+7;
int Pow(int x,int n){
	int k=1;
	for (;n;n>>=1,x=(ll)x*x%mo)
		if (n&1) k=(ll)k*x%mo;
	return k;
}
int main(){
	int x=99999937;
	printf("%d\n",Pow(x,mo-2));
}
