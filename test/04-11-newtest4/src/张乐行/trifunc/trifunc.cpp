#include<cstdio>
#include<cmath>
#include<cstring>
#define ll long long
const int N=5e5+5,mo=1e9+7;
struct Matrix{int a,b,c,d;};
Matrix operator + (Matrix A,Matrix B){
	return (Matrix){(A.a+B.a)%mo,(A.b+B.b)%mo,
		(A.c+B.c)%mo,(A.d+B.d)%mo};
}
Matrix operator * (Matrix A,Matrix B){
	return (Matrix){
		((ll)A.a*B.a+(ll)A.b*B.c)%mo,
		((ll)A.a*B.b+(ll)A.b*B.d)%mo,
		((ll)A.c*B.a+(ll)A.d*B.c)%mo,
		((ll)A.c*B.b+(ll)A.d*B.d)%mo
	};
}
int ra,rb,n,i,j,ans;
char s[N];
Matrix ZERO,B,pw[N],f[N];
Matrix Pow(Matrix x,int n){
	Matrix k=ZERO;
	for (;n;n>>=1,x=x*x) if (n&1) k=k*x;
	return k;
}
int main(){
	freopen("trifunc.in","r",stdin);
	freopen("trifunc.out","w",stdout);
	scanf("%d%d%s",&ra,&rb,s+1);
	n=strlen(s+1);
	int k=sqrt((ll)rb*rb-(ll)ra*ra);
	B=(Matrix){ra,k,mo-k,ra};
	ZERO=(Matrix){1,0,0,1};pw[0]=B;
	for (i=1;i<=n;i++) pw[i]=Pow(pw[i-1],10);
	for (f[1]=ZERO,i=1;i<n;i++)
		f[i+1]=f[i]*Pow(pw[n-i],s[i]-48);
	Matrix v=B;
	for (i=n;i;i--){
		Matrix lv=v;v=(Matrix){0,0,0,0};
		for (j=1;j<=s[i]-48;j++)
			v=v*pw[n-i]+lv;
		Matrix res=v*f[i];ans=(ans+res.b)%mo;
		for (;j<=10;j++) v=v*pw[n-i]+lv;
	}
	printf("%d",ans);
}
