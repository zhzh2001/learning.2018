#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define RG register
#define pc putchar
#define gc getchar
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
inline int read(){
    int x=0,f=1;
	char c=gc();
    for(;!isdigit(c);c=gc())if(c=='-')f=-1;
    for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
    return x*f;
}
#define rd read
inline void write(int x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
#define debug(x) printf(#x" = %d\n",x);
#define mem(x,v) memset(x,v,sizeof(x))
const int Mod = 1e9+7;
ll ppow(ll a,ll b){
	ll ans = 1;
	for(;b;b>>=1,a=1ll*a*a % Mod)
		if(b&1) ans=1ll*ans*a % Mod;
	return ans;
}
struct Ma{
	int v[3][3];
	Ma operator * (const Ma &w) const{
		Ma ans;
		rep(i,0,3){
			rep(j,0,3){
				ans.v[i][j] = 0;
				rep(k,0,3)
					ans.v[i][j] = (ans.v[i][j] + 1ll * v[i][k] * w.v[k][j] % Mod + Mod)%Mod;
			}
		}
		return ans;
	}
}ans,M;
char s[500500];
Ma ppow(Ma a){
	scanf("%s",s+1);
	Ma ans;mem(ans.v,0),ans.v[0][0]=ans.v[1][1]=ans.v[2][2]=1;
	Ma t1;mem(t1.v,0),t1.v[0][0]=t1.v[1][1]=t1.v[2][2]=1;
	Ma t2;mem(t2.v,0),t2.v[0][0]=t2.v[1][1]=t2.v[2][2]=1;
	Ma t4;mem(t4.v,0),t4.v[0][0]=t4.v[1][1]=t4.v[2][2]=1;
	Ma t8;mem(t8.v,0),t8.v[0][0]=t8.v[1][1]=t8.v[2][2]=1;
	Ma t10;
	t1 = a;
	t2 = t1*t1;
	t4 = t2*t2;
	t8 = t4*t4;
	for(int i=strlen(s+1);i>=1;i--){
		t10 = t2*t8;
		s[i]-='0';
		if(s[i]&1) ans=ans*t1;
		if(s[i]>>1&1) ans=ans*t2;
		if(s[i]>>2&1) ans=ans*t4;
		if(s[i]>>3&1) ans=ans*t8;
		t1=t10;
		t2=t1*t1;
		t4=t2*t2;
		t8=t4*t4;
	}
	return ans;
}

ll a,b,n;
//ll sqrt_mod(ll x){
	//return sqrt(x);
//}
int main(){
	freopen("trifunc.in","r",stdin);
	freopen("trifunc.out","w",stdout);
	a = rd(),b = rd();
	ll x = a;
	ll y = (ll)sqrt(1ll*b*b-1ll*a*a) % Mod;
	
	M.v[0][0] = x;M.v[0][1] = y;M.v[0][2] = 0;
	M.v[1][0] = -y;M.v[1][1] = x;M.v[1][2] = 0;
	M.v[2][0] = 1;M.v[2][1] = 0;M.v[2][2] = 1;
	mem(ans.v,0);
	ans.v[0][0] = y;
	ans.v[1][0] = x;
	ans.v[2][0] = 0;
	ans = ppow(M) * ans; 
//	ans.write();
	writeln(ans.v[2][0]);
	
	return 0;
}
/*
WZP 280+
ZYY 300
YK  250+
My  ???
JZQ 250+
ZXH 280+
LZH 280+
SYY 150+
 
*/
