#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define RG register
#define pc putchar
#define gc getchar
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
inline int read(){
    int x=0,f=1;
	char c=gc();
    for(;!isdigit(c);c=gc())if(c=='-')f=-1;
    for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
    return x*f;
}
#define rd read
inline void write(int x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
#define debug(x) printf(#x" = %d\n",x);
#define mem(x,v) memset(x,v,sizeof(x))
const int Mod = 1e9+7;
ll ppow(ll a,ll b){
	ll ans = 1;
	for(;b;b>>=1,a=1ll*a*a)
		if(b&1) ans=1ll*ans*a;
	return ans;
}
ll f[23333333],g[22233333];
ll ans = 0;
int main(){
	freopen("trifunc.in","r",stdin);
	freopen("std.out","w",stdout);
	ll a = rd(),b = rd(),n = rd();
	g[1] = a;
	f[1] = sqrt(b*b - a*a);
	Rep(i,1,n){
		if(i!=1){
			f[i] = ((ll)f[1] * g[i-1]%Mod + g[1] * f[i-1]%Mod)%Mod;
			g[i] = ((ll)g[1] * g[i-1]%Mod - f[1] * f[i-1]%Mod)%Mod;
		}
		ans = (ans + f[i] + Mod)%Mod;
	}
	printf("%lld\n",ans,(ll)ans % Mod);
	return 0;
	return 0;
}
