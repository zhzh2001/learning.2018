//Hello Wolrd
#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define RG register
#define pc putchar
#define gc getchar
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
inline int read(){
    int x=0,f=1;
	char c=gc();
    for(;!isdigit(c);c=gc())if(c=='-')f=-1;
    for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
    return x*f;
}
#define rd read
inline void write(int x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
#define debug(x) printf(#x" = %d\n",x);
#define mem(x,v) memset(x,v,sizeof(x))
/*#define int ll
struct SegmentTree{
	#define lson o<<1
	#define rson o<<1|1
	#define mid ((l+r)>>1)
	void change(int o,int l,int r,int x,int y,int v){
		if(l==x&&r==y){
			T[o].v+=v;
			T[o].tag+=v;
			return ;
		}
		pushdown(o,l,r);
		if(y<=mid) change(lson,l,mid,x,y,v); else
		if(mid+1<=x) change(rson,mid+1,r,x,y,v); else{
			change(lson,l,mid,x,mid,v); 
			change(rson,mid+1,r,mid+1,y,v);
		}
	}
	int query(int )
};*/
const int maxn = 300500;
struct node{
	int t,id,change;
	bool operator < (const node &w) const{
		return t < w.t;
	}
}a[maxn];
map<int,int> v;
map<int,int,greater<int> > cnt;
vector<int> ans;
int n,g;
int main(){
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	n = rd(),g = rd();
	Rep(i,1,n){
		a[i].t = rd();
		a[i].id = rd();
		a[i].change = rd();
		if(!v.count(a[i].id))
			v[a[i].id] = g;
//		printf("%d %d %d\n",a[i].t,a[i].id,a[i].change);
	}
	sort(a+1,a+1+n);
    cnt[g] = n;
    int la = g,lb = n;
    ans.clear();
	Rep(i,1,n){
        int last=v[a[i].id];
		v[a[i].id]+=a[i].change;
        if(cnt[last]==1)cnt.erase(last);else cnt[last]--;
        cnt[v[a[i].id]]++;
        int ra=cnt.begin()->first,rb=cnt.begin()->second;
        if(la != ra || lb != rb){
			ans.push_back(a[i].t);
			la = ra;
			lb = rb;
		}
    }
    writeln(ans.size());
    for(unsigned int i=0;i<ans.size();++i){
    	writeln(ans[i]);
	}

	return 0;
}
