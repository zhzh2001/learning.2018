#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define RG register
#define pc putchar
#define gc getchar
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
const int Mod = 1e9+7;
inline ll read(){
    ll x=0,f=1;
	char c=gc();
    for(;!isdigit(c);c=gc())if(c=='-')f=-1;
    for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
    return x*f;
}
#define rd read
inline void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
#define debug(x) printf(#x" = %d\n",x);
#define mem(x,v) memset(x,v,sizeof(x))
const int maxn = 3e7+5;
bool Isprime(ll x){
	for(int i=2;1ll*i*i<=x;i++)
		if(x%i==0) return false;
	return true;
}
int main(){
	freopen("primehash.in","r",stdin);
	freopen("std.out","w",stdout);
	ll ans1=0,ans2=0;
	ll a = rd(),b = rd();
	int cnt=0;
	Rep(j,a,b){
		if(Isprime(j)){
			ans1++;
			ans2=(1ll*ans2*999999937+j)%Mod;
		}
	}
	write(ans1);pc(' ');writeln(ans2);
    return 0;
}
