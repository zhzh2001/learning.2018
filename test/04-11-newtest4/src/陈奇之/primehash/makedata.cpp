#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define RG register
#define pc putchar
#define gc getchar
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
const int Mod = 1e9+7;
inline ll read(){
    ll x=0,f=1;
	char c=gc();
    for(;!isdigit(c);c=gc())if(c=='-')f=-1;
    for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
    return x*f;
}
#define rd read
inline void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
#define debug(x) printf(#x" = %d\n",x);
#define mem(x,v) memset(x,v,sizeof(x))
ll ran(){
	return 1ll*rand()*RAND_MAX+rand();
}
int main(){
	freopen("primehash.in","r",stdin);
	srand(time(NULL)+gc()+gc()+gc()+gc()+gc()+gc()+gc()+gc()+gc());
	freopen("primehash.in","w",stdout);
	ll a = ran() % 1000000000 + 1;
	ll b = ran() % 100000 + a + 1;
	printf("%lld %lld\n",a,b);
    return 0;
}
