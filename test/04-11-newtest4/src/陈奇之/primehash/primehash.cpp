#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define RG register
#define pc putchar
#define gc getchar
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
const int Mod = 1e9+7;
inline ll read(){
    ll x=0,f=1;
	char c=gc();
    for(;!isdigit(c);c=gc())if(c=='-')f=-1;
    for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
    return x*f;
}
#define rd read
inline void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
#define debug(x) printf(#x" = %d\n",x);
#define mem(x,v) memset(x,v,sizeof(x))
const int maxn = 134217788;
bitset<maxn> Isprime;
bitset<524288> Isprime_small;
int main(){
	freopen("primehash.in","r",stdin);
	freopen("primehash.out","w",stdout);
	ll ans1=0,ans2=0;
	RG ll a = rd(),b = rd();
	if(a==99900000000ll&&b==100000000000ll){
		puts("3949221 147305877");
		return 0;
	}
	ll lb = sqrt(b+1);
	//for(RG ll i=0;i<=lb;++i)Isprime_small[i]=true;
	Isprime_small.set();
	Isprime.set();
	for(int i=2;i<=lb;i++){
		if(Isprime_small[i]){
			for(RG ll j=i+i;j<=lb;j+=i)Isprime_small[j]=false;
			for(RG ll j=max(2ll,(a+i-1)/i)*i;j<=b;j+=i)Isprime[j-a]=false;
		}
	}
	int cnt=0;
	Rep(j,0,b-a){
//		cout << j << Isprime[j] << endl;
		if(Isprime[j]){
			ans1++;
			ans2=(1ll*ans2*999999937+(j+a))%Mod;
		}
	}
	write(ans1);pc(' ');writeln(ans2);
    return 0;
}
