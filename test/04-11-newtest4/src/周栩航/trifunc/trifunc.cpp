#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long

using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0)	{putchar('-');write(-x);return;}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}

int n[600005];
char s[600005];

ll x,y,len,mo=1e9+7;
struct mat
{
	ll a[4][4];
	mat(){For(i,1,3)	For(j,1,3)	a[i][j]=0;}
} tmp,ans;

inline ll getmo(ll x)
{
	ll tmp=x;
	ll las=tmp/mo*mo;
	if(x<0&&x!=las)	las-=mo;
	return x-las;
}
inline mat operator * (mat x,mat y)
{
	mat tmp;
	For(k,1,3)
		For(i,1,3)
			if(x.a[i][k])
			For(j,1,3)
				if(y.a[k][j])
				tmp.a[i][j]+=x.a[i][k]*y.a[k][j],tmp.a[i][j]=getmo(tmp.a[i][j]);
	return tmp;
}
mat one;
inline mat ksm(mat x)
{
	mat sum=one;
	For(i,1,len)
	{
		mat t_tmp=x;
		For(j,0,3)	
		{
			if(n[i]>>j&1)	sum=sum*t_tmp;
			t_tmp=t_tmp*t_tmp;	
		}	
		mat tmp=x,ttmp=x*x;
		For(j,1,3)	tmp=tmp*tmp;
		x=tmp*ttmp;
	}
	return sum;
}
int main()
{
	freopen("trifunc.in","r",stdin);freopen("trifunc.out","w",stdout);
	one.a[1][1]=one.a[2][2]=one.a[3][3]=1;
	x=read();y=read();scanf("\n%s",s+1);
	len=strlen(s+1);
	For(i,1,len)	n[i]=s[len-i+1]-'0';
	ll b=x,a=sqrt(y*y-x*x);
	mat tmp;
	tmp.a[1][1]=tmp.a[2][2]=b;
	tmp.a[1][2]=a;tmp.a[2][1]=-a;
	tmp.a[3][1]=tmp.a[3][3]=1;
	
	ans.a[1][1]=a;ans.a[2][1]=b;ans.a[3][1]=0;
	
	n[1]--;
	int tep=1;
	while(n[tep]<0)	n[tep]+=10,n[tep+1]--,tep++;
	while(len&&!n[len])	len--;	
	
	if(len)	ans=ksm(tmp)*ans;
	writeln(((ll)(ans.a[3][1]+ans.a[1][1])%mo+mo)%mo);
}
