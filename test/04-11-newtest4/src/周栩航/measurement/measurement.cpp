#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long

using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0)	{putchar('-');write(-x);return;}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}

const int N=1500005,M=400001;
struct node{int d,p,v;}	e[M];
int tr_mx[N],tr_tot[N],n,ans[M],tot_ans,tot,q[M],top;
map<int,int> mp;
inline bool cmp(node x,node y){return x.d<y.d;}
inline void Build(int x,int l,int r)
{
	if(l==r)	{tr_mx[x]=0;tr_tot[x]=1;return;}
	int mid=l+r>>1;
	Build(x<<1,l,mid);Build(x<<1|1,mid+1,r);
}
inline void Up(int x)
{
	if(tr_mx[x<<1]>tr_mx[x<<1|1])	
	{
		tr_mx[x]=tr_mx[x<<1];
		tr_tot[x]=tr_tot[x<<1];
	}
	else
	{
		if(tr_mx[x<<1]<tr_mx[x<<1|1])
		{
			tr_mx[x]=tr_mx[x<<1|1];
			tr_tot[x]=tr_tot[x<<1|1];
		}
		else
		{
			tr_mx[x]=tr_mx[x<<1|1];
			tr_tot[x]=tr_tot[x<<1|1]+tr_tot[x<<1];
		}
	}
}
inline void Upd(int x,int l,int r,int to,int v)
{
	if(l==r){tr_mx[x]+=v;return;}
	int mid=l+r>>1;
	if(to<=mid)	Upd(x<<1,l,mid,to,v);
	else	Upd(x<<1|1,mid+1,r,to,v);
	Up(x);
}
int main()
{
	freopen("measurement.in","r",stdin);freopen("measurement.out","w",stdout);
	n=read();read();
	For(i,1,n)
	{
		e[i].d=read(),e[i].p=read(),e[i].v=read();
		q[++top]=e[i].p;
	}
	sort(e+1,e+n+1,cmp);
	ll las_mx=0,las_tot=n+1;
//	n++;
	For(i,1,top)	if(!mp[q[i]])	mp[q[i]]=++tot;
	
	For(i,1,n)	e[i].p=mp[e[i].p];
	++tot;
	Build(1,1,tot);
	For(i,1,n)
	{
		Upd(1,1,tot,e[i].p,e[i].v);
		int mx=tr_mx[1],t_tot=tr_tot[1];
		if(mx!=las_mx||t_tot!=las_tot)	ans[++tot_ans]=e[i].d;
		las_mx=mx;las_tot=t_tot;
	}
	writeln(tot_ans);
	For(i,1,tot_ans)	writeln(ans[i]);
}
