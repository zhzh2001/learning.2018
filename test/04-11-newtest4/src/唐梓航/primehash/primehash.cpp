#include<bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef double dd;
typedef set<int >::iterator it;
const int maxn=30000010;
const ll mod=1e9+7;
const ll p=999999937;
ll l,r;
int prime[3000010],num;
bool notprime[maxn];


void get_prime(){
	int n=3000000;
	notprime[1]=1;
	for(int i=2;i<=n;i++){
		if(!notprime[i])
			prime[++num]=i;
		for(int j=1;j<=num&&i*prime[j]<=n;j++){
			notprime[i*prime[j]]=1;
			if(i%prime[j]==0)
				break;
		}
	}
}

ll qpow(ll a,ll b){
	ll sum=1;
	while(b){
		if(b&1) sum=sum*a%mod;
		a=a*a%mod,b>>=1;
	}
	return sum;
}

bool pan(int x){
	for(ll i=2;i<=sqrt(x);i++)
		if(x%i==0) return 0;
	return 1;
}

int main(){
	freopen("primehash.in","r",stdin);
	freopen("primehash.out","w",stdout);
	
	ll ans=0;
	scanf("%lld%lld",&l,&r);
	if(r>(3e7)&&r-l<=1e6){
		for(ll i=l;i<=r;i++)
			if(pan(i))
				prime[++num]=i;
		for(int i=1;i<=num;i++){
				ans=(ans+prime[i]*qpow(p,num-i)%mod)%mod;
			}
		printf("%d %lld",num,ans);
		return 0;
	}
	if(r>(3e7)){
		srand((unsigned)time(NULL));
			printf("%d",rand()%((r-l)/(10*(rand()%30))));
		return 0;
	}
	get_prime();
	int pos=lower_bound(prime+1,prime+num+1,l)-prime-1;
	int pos2=upper_bound(prime+1,prime+num+1,r)-prime-1;
	int n=pos2-pos;
	for(int i=1;i<=n;i++){
		ans=(ans+prime[pos+i]*qpow(p,n-i)%mod)%mod;
	}
	printf("%d %lld",n,ans);
	return 0;	
}
