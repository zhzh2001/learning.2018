#include<bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef double dd;
typedef multiset<ll>::iterator it;
const int maxn=300010;
ll n,k,ans[maxn];
ll g;
multiset<ll> s;//->ֵ 
map<ll ,ll > f; //first->��� second->ֵ 

struct node{
	ll day;
	ll num,pos;	
}d[maxn];

bool cm(node a,node b){
	return a.day<b.day;	
}

inline void read(ll &x)
{
    ll f=1;x=0;char s=getchar();
    while(s<'0'||s>'9'){if(s=='-')f=-1;s=getchar();}
    while(s>='0'&&s<='9'){x=x*10+s-'0';s=getchar();}
    x*=f;
}

int main(){
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	scanf("%lld%lld",&n,&g);
	for(ll i=1;i<=n;i++)
		read(d[i].day),read(d[i].pos),read(d[i].num);
/*		scanf("%d%lld%lld",&d[i].day,&d[i].pos,&d[i].num);
		*/
		
	sort(d+1,d+n+1,cm);
	ll maxx=g;
	for(ll i=1;i<=n;i++)
		if(!f.count(d[i].pos)) 
			f[d[i].pos]=g,s.insert(g);

	for(ll i=1;i<=n;i++){
		if(f[d[i].pos]==maxx){
			if(d[i].num==0) continue;
			f[d[i].pos]+=d[i].num;
			if(d[i].num>0){
				it t=s.end();t--,t--;
				if((*t)==maxx) ans[++k]=d[i].day;
				t++;
				s.erase(t);
				maxx+=d[i].num;
				s.insert(maxx); 
			}
			else{
				it t=s.end();t--,t--;
				if(maxx+d[i].num<=*t)
					ans[++k]=d[i].day;
				t++;
				s.erase(t);
				s.insert(maxx+d[i].num);
				it q=s.end();q--;
				maxx=*q;		
			}
		}
		else{
			ll val=f[d[i].pos];
			it t=s.find(val);
			if(d[i].num>0){
				it q=s.end();q--;
				if(val+d[i].num>=*q)
					maxx=val+d[i].num,ans[++k]=d[i].day;
			}
			s.erase(t);
			s.insert(val+d[i].num);
			f[d[i].pos]+=d[i].num;
		}
	}
	printf("%lld\n",k);
	for(int i=1;i<=k;i++)
		printf("%lld\n",ans[i]);
	return 0;	
}
