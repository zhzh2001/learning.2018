#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
using namespace std;
ifstream fin("measurement.in");
ofstream fout("measurement.out");
const int N = 300005;
struct cow
{
	int day, id, delta;
	bool operator<(const cow &rhs) const
	{
		return day < rhs.day;
	}
} c[N];
int main()
{
	int n, g;
	fin >> n >> g;
	for (int i = 1; i <= n; i++)
		fin >> c[i].day >> c[i].id >> c[i].delta;
	sort(c + 1, c + n + 1);
	vector<int> ans;
	if (count_if(c + 1, c + n + 1, [=](const cow &c) {
			return c.id == ::c[1].id;
		}) == n)
	//all ids are equal
	{
		int now = g;
		for (int i = 1; i <= n; i++)
		{
			int tmp = now + c[i].delta;
			if ((now < g && tmp >= g) || (now == g) || (now > g && tmp <= g))
				ans.push_back(c[i].day);
			now = tmp;
		}
	}
	else
	{
		set<int> ids;
		if (count_if(c + 1, c + n + 1, [&](const cow &c) {
				return ids.insert(c.id).second;
			}) == n)
		//all ids are different
		{
			int mx = g;
			for (int i = 1; i <= n; i++)
			{
				int tmp = g + c[i].delta;
				if (tmp >= mx || (mx == g && tmp < g))
					ans.push_back(c[i].day);
				mx = max(mx, tmp);
			}
		}
		map<int, int> val;
		if (count_if(c + 1, c + n + 1, [](const cow &c) {
				return c.delta > 0;
			}) == n)
		//all deltas are positive
		{
			int mx = g, sum = 1e9;
			for (int i = 1; i <= n; i++)
			{
				int now;
				if (val.find(c[i].id) != val.end())
					now = val[c[i].id] + c[i].delta;
				else
					now = g + c[i].delta;
				if (now > mx)
				{
					if (sum != 1 || val[c[i].id] != mx)
					{
						ans.push_back(c[i].day);
						sum = 1;
					}
					mx = now;
				}
				else if (now == mx)
				{
					ans.push_back(c[i].day);
					sum++;
				}
				val[c[i].id] = now;
			}
		}
		else if (count_if(c + 1, c + n + 1, [](const cow &c) {
					 return c.delta < 0;
				 }) == n)
		//all deltas are negative
		{
			set<int> ids;
			for (int i = 1; i <= n; i++)
				if (ids.find(c[i].id) == ids.end())
				{
					ids.insert(c[i].id);
					ans.push_back(c[i].day);
				}
		}
	}
	fout << ans.size() << endl;
	for (auto i : ans)
		fout << i << endl;
	return 0;
}