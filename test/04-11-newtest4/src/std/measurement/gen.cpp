#include <fstream>
#include <random>
#include <ctime>
#include <set>
using namespace std;
ofstream fout("measurement.in");
const int n = 255555, m = 300000;
int a[n + 5];
int main()
{
	minstd_rand gen(time(NULL));
	uniform_int_distribution<> dg(1, 1e9);
	fout << m << ' ' << dg(gen) << endl;
	for (int i = 1; i <= n; i++)
	{
		uniform_int_distribution<> d(1, 1e9);
		a[i] = d(gen);
	}
	set<int> times;
	for (int i = 1; i <= m; i++)
	{
		uniform_int_distribution<> opt(0, 3), delta(-5000, 5000), dn(1, n), dn2(1, 5), dt(1, 1e9);
		int id;
		if (opt(gen))
			id = a[dn2(gen)];
		else
			id = a[dn(gen)];
		int t = dt(gen), del = delta(gen);
		while (times.find(t) != times.end())
			t = dt(gen);
		times.insert(t);
		fout << t << ' ' << id << ' ' << showpos << del << noshowpos << endl;
	}
	return 0;
}