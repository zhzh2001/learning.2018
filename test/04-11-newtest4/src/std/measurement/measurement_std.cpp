#include <cstdio>
#include <cctype>
#include <map>
#include <algorithm>
#include <vector>
using namespace std;
FILE *fin = fopen("measurement.in", "r"), *fout = fopen("measurement.out", "w");
const int SZ = 1e6;
char buf[SZ], *p = buf, *pend = buf;
inline int nextchar()
{
	if (p == pend)
	{
		pend = (p = buf) + fread(buf, 1, SZ, fin);
		if (pend == buf)
			return EOF;
	}
	return *p++;
}
template <typename Int>
inline void read(Int &x)
{
	char c = nextchar();
	Int sign = 1;
	for (; !isdigit(c); c = nextchar())
		if (c == '-')
			sign = -1;
	x = 0;
	for (; isdigit(c); c = nextchar())
		x = x * 10 + c - '0';
	x *= sign;
}
inline void writechar(char c)
{
	if (p == pend)
	{
		fwrite(buf, 1, SZ, fout);
		p = buf;
	}
	*p++ = c;
}
inline void flush()
{
	fwrite(buf, 1, p - buf, fout);
}
int dig[20];
template <typename Int>
inline void writeln(Int x)
{
	if (x < 0)
	{
		writechar('-');
		x = -x;
	}
	int len = 0;
	do
		dig[++len] = x % 10;
	while (x /= 10);
	for (; len; len--)
		writechar(dig[len] + '0');
	writechar('\n');
}
const int N = 300005;
struct day
{
	int date, cow, delta;
	bool operator<(const day &rhs) const
	{
		return date < rhs.date;
	}
} a[N];
int main()
{
	int n, g;
	read(n);
	read(g);
	for (int i = 1; i <= n; i++)
	{
		read(a[i].date);
		read(a[i].cow);
		read(a[i].delta);
	}
	sort(a + 1, a + n + 1);
	map<int, int> cnt, val;
	val[0] = n + 1;
	vector<int> ans;
	for (int i = 1; i <= n; i++)
	{
		int &ref = cnt[a[i].cow];
		int predcnt = val[ref]--;
		bool predtop = ref == val.rbegin()->first;
		if (predcnt == 1)
			val.erase(ref);
		ref += a[i].delta;
		int nowcnt = ++val[ref];
		bool nowtop = ref == val.rbegin()->first;
		if ((predtop && (!nowtop || predcnt > 1 || nowcnt > 1)) || (!predtop && nowtop))
			ans.push_back(a[i].date);
	}
	p = buf;
	pend = buf + SZ;
	writeln(ans.size());
	for (int i : ans)
		writeln(i);
	flush();
	return 0;
}