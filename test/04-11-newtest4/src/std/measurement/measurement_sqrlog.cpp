#include <fstream>
#include <map>
#include <set>
#include <algorithm>
#include <vector>
using namespace std;
ifstream fin("measurement.in");
ofstream fout("measurement.out");
const int N = 300005;
struct day
{
	int date, cow, delta;
	bool operator<(const day &rhs) const
	{
		return date < rhs.date;
	}
} a[N];
int main()
{
	int n, g;
	fin >> n >> g;
	map<int, int> cnt;
	map<int, set<int>> val;
	for (int i = 1; i <= n; i++)
	{
		fin >> a[i].date >> a[i].cow >> a[i].delta;
		cnt[a[i].cow] = g;
		val[g].insert(a[i].cow);
	}
	sort(a + 1, a + n + 1);
	val[g].insert(0);
	vector<int> ans;
	for (int i = 1; i <= n; i++)
	{
		int old = cnt[a[i].cow], maxval = val.rbegin()->first, sz = val.rbegin()->second.size();
		val[old].erase(a[i].cow);
		if (sz == 1)
			val.erase(old);
		val[old + a[i].delta].insert(a[i].cow);
		if ((val.rbegin()->first != maxval || val.rbegin()->second.size() != sz) && !(sz == 1 && old == maxval && val.rbegin()->second.size() == 1 && *(val.rbegin()->second.begin()) == a[i].cow))
			ans.push_back(a[i].date);
		cnt[a[i].cow] += a[i].delta;
	}
	fout << ans.size() << endl;
	for (int i : ans)
		fout << i << endl;
	return 0;
}