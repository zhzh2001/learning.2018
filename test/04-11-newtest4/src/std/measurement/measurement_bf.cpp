#include <fstream>
#include <algorithm>
#include <vector>
using namespace std;
ifstream fin("measurement.in");
ofstream fout("measurement.out");
const int N = 300005;
struct day
{
	int date, cow, delta;
	bool operator<(const day &rhs) const
	{
		return date < rhs.date;
	}
} a[N];
pair<int, int> cow[N], now[N];
int main()
{
	int n, g;
	fin >> n >> g;
	for (int i = 1; i <= n; i++)
	{
		fin >> a[i].date >> a[i].cow >> a[i].delta;
		cow[i].first = g;
		cow[i].second = a[i].cow;
	}
	sort(a + 1, a + n + 1);
	sort(cow + 1, cow + n + 1);
	cow[n + 1] = make_pair(g, 0);
	int cnt = unique(cow + 1, cow + n + 2) - cow - 1;
	vector<int> ans;
	for (int i = 1; i <= n; i++)
	{
		copy(cow + 1, cow + cnt + 1, now + 1);
		for (int j = 1; j <= cnt; j++)
			if (now[j].second == a[i].cow)
				now[j].first += a[i].delta;
		sort(now + 1, now + cnt + 1);
		bool flag = false;
		int j = cnt;
		for (; j && now[j].first == now[cnt].first && cow[j].first == cow[cnt].first; j--)
			if (now[j].second != cow[j].second)
			{
				flag = true;
				break;
			}
		if (cow[j].first == cow[cnt].first || now[j].first == now[cnt].first)
			flag = true;
		if (flag)
			ans.push_back(a[i].date);
		copy(now + 1, now + cnt + 1, cow + 1);
	}
	fout << ans.size() << endl;
	for (int i : ans)
		fout << i << endl;
	return 0;
}