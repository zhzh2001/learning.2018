#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("primehash.in");
ofstream fout("primehash.out");
long long f[340000], g[340000];
long long query(long long n)
{
    long long m;
    for (m = 1; m * m <= n; ++m)
        f[m] = n / m - 1;
    for (long long i = 1; i <= m; ++i)
        g[i] = i - 1;
    for (long long i = 2; i <= m; ++i)
    {
        if (g[i] == g[i-1])
            continue;
        for (long long j = 1; j <= min(m - 1, n / i / i); ++j)
        {
            if (i * j < m)
                f[j] -= f[i * j] - g[i - 1];
            else
                f[j] -= g[n / i / j] - g[i - 1];
        }
        for (long long j = m; j >= i * i; --j)
            g[j] -= g[j / i] - g[i - 1];
    }
    return f[1];
}
int main()
{
	long long l, r;
    fin >> l >> r;
    fout << query(r) - query(l - 1) << endl;
    return 0;
}