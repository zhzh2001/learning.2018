#include <fstream>
#include <cmath>
using namespace std;
ifstream fin("primehash.in");
ofstream fout("primehash.out");
const int SQ = 1e6 + 5, L = 1e8 + 5, B = 999999937, MOD = 1e9 + 7;
bool bl[SQ], bl2[L];
int main()
{
	long long l, r;
	fin >> l >> r;
	int sr = floor(sqrt(r) + .5);
	for (int i = 2; i * i <= sr; i++)
		if (!bl[i])
			for (int j = i * i; j <= sr; j += i)
				bl[j] = true;
	for (int i = 2; i <= sr; i++)
		if (!bl[i])
		{
			long long j = 1ll * i * i - l;
			if (j < 0)
				j = (j % i + i) % i;
			for (; l + j <= r; j += i)
				bl2[j] = true;
		}
	int cnt = 0;
	long long h = 0;
	for (long long i = l; i <= r; i++)
	{
		if (i == 1)
			continue;
		if (i == 2)
		{
			h = (h * B + 2) % MOD;
			cnt++;
		}
		else if (!bl2[i - l])
		{
			h = (h * B + i) % MOD;
			cnt++;
		}
	}
	fout << cnt << ' ' << h << endl;
	return 0;
}