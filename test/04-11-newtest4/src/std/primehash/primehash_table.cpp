#include <fstream>
#include <cmath>
#include <algorithm>
#include <ctime>
using namespace std;
ofstream fout("table.txt");
const long long n = 1e11;
const int m = 1e6, B = 1 << 16, P = 1e5, B2 = 1e8, BASE = 999999937, MOD = 1e9 + 7;
bool bl[m + 5], buf[B];
int p[P], arrh[n / B2 + 5], arrcnt[n / B2 + 5];
long long nxt[P];
int main()
{
	fill(bl + 2, bl + m + 1, true);
	for (int i = 2; i * i <= m; i++)
		if (bl[i])
			for (int j = i * i; j <= m; j += i)
				bl[j] = false;
	int h = 2, cnt = 1, b = 0;
	long long l = 2;
	for (int np = 3, pn = 0; l <= n; l += B)
	{
		fill(buf, buf + B, true);
		long long high = min(l + B - 1, n);
		for (; 1ll * np * np <= high; np += 2)
			if (bl[np])
			{
				p[++pn] = np;
				nxt[pn] = 1ll * np * np - l;
				if (nxt[pn] < 0)
					nxt[pn] = (nxt[pn] % np + np) % np;
				if ((l + nxt[pn]) % 2 == 0)
					nxt[pn] += np;
			}
		for (int i = 1; i <= pn; i++)
		{
			long long j = nxt[i];
			for (; j < B; j += p[i] * 2)
				buf[j] = false;
			nxt[i] = j - B;
		}
		for (int i = l & 1 ? 0 : 1; i < B && l + i <= n; i += 2)
		{
			if ((l + i) % B2 == 1)
			{
				arrh[++b] = h;
				arrcnt[b] = cnt;
				h = cnt = 0;
			}
			if (l + i > 1 && buf[i])
			{
				h = (1ll * h * BASE + l + i) % MOD;
				cnt++;
			}
		}
	}
	for (int i = 1; i <= b; i++)
		fout << arrh[i] << ',';
	fout << endl;
	for (int i = 1; i <= b; i++)
		fout << arrcnt[i] << ',';
	fout << clock() << endl;
	return 0;
}