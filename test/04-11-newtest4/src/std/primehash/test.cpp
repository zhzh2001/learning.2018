#include <iostream>
using namespace std;
int main()
{
	long long n;
	cin >> n;
	for (long long i = 2; i * i <= n; i++)
		if (n % i == 0)
		{
			int cnt = 0;
			for (; n % i == 0; cnt++, n /= i)
				;
			cout << i << ' ' << cnt << endl;
		}
	if (n > 1)
		cout << n << ' ' << 1 << endl;
	cin.get();
	cin.get();
	return 0;
}