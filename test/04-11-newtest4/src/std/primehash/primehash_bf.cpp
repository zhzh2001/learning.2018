#include <fstream>
#include <bitset>
using namespace std;
ifstream fin("primehash.in");
ofstream fout("primehash.out");
const int N = 3e8 + 5, B = 999999937, MOD = 1e9 + 7;
bitset<N> p;
int main()
{
	int l, r;
	fin >> l >> r;
	p[1] = true;
	for (int i = 2; i * i <= r; i++)
		if (!p[i])
			for (int j = i * i; j <= r; j += i)
				p[j] = true;
	int h = 0, cnt = 0;
	for (int i = l; i <= r; i++)
		if (!p[i])
		{
			h = (1ll * h * B + i) % MOD;
			cnt++;
		}
	fout << cnt << ' ' << h << endl;
	return 0;
}