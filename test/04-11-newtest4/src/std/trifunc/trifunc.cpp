#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
using namespace std;
ifstream fin("trifunc.in");
ofstream fout("trifunc.out");
const int MOD = 1e9 + 7;
struct matrix
{
	int mat[3][3];
	matrix()
	{
		memset(mat, 0, sizeof(mat));
	}
	matrix operator*(const matrix &rhs) const
	{
		matrix ret;
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				for (int k = 0; k < 3; k++)
					ret.mat[i][j] = (ret.mat[i][j] + 1ll * mat[i][k] * rhs.mat[k][j]) % MOD;
		return ret;
	}
} trans[10];
matrix I()
{
	matrix ret;
	for (int i = 0; i < 3; i++)
		ret.mat[i][i] = 1;
	return ret;
}
matrix qpow(matrix a, int b)
{
	matrix ans = I();
	do
	{
		if (b & 1)
			ans = ans * a;
		a = a * a;
	} while (b /= 2);
	return ans;
}
int main()
{
	int x, r;
	string n;
	fin >> x >> r >> n;
	int y = floor(sqrt(1.0 * r * r - 1.0 * x * x) + .5);
	x = (x + MOD) % MOD;
	trans[0] = I();
	trans[1].mat[0][0] = x;
	trans[1].mat[0][1] = y;
	trans[1].mat[1][0] = MOD - y;
	trans[1].mat[1][1] = x;
	trans[1].mat[2][0] = trans[1].mat[2][2] = 1;
	for (int i = 2; i < 10; i++)
		trans[i] = trans[i - 1] * trans[1];
	matrix prod = I();
	for (char d : n)
		prod = qpow(prod, 10) * trans[d - '0'];
	matrix init;
	init.mat[0][0] = y;
	init.mat[1][0] = x;
	fout << (prod * init).mat[2][0] << endl;
	return 0;
}