#include <fstream>
#include <gmpxx.h>
#include <cstring>
#include <cmath>
using namespace std;
ifstream fin("trifunc.in");
ofstream fout("trifunc.out");
const int MOD = 1e9 + 7;
struct matrix
{
	int mat[3][3];
	matrix()
	{
		memset(mat, 0, sizeof(mat));
	}
	matrix operator*(const matrix &rhs) const
	{
		matrix ret;
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				for (int k = 0; k < 3; k++)
					ret.mat[i][j] = (ret.mat[i][j] + 1ll * mat[i][k] * rhs.mat[k][j]) % MOD;
		return ret;
	}
};
matrix I()
{
	matrix ret;
	for (int i = 0; i < 3; i++)
		ret.mat[i][i] = 1;
	return ret;
}
matrix qpow(matrix a, mpz_class b)
{
	string s = b.get_str(2);
	matrix ans = I();
	for (int i = s.length() - 1; i >= 0; i--)
	{
		if (s[i] == '1')
			ans = ans * a;
		a = a * a;
	}
	return ans;
}
int main()
{
	int x, r;
	mpz_class n;
	fin >> x >> r >> n;
	int y = floor(sqrt(1.0 * r * r - 1.0 * x * x) + .5);
	x = (x + MOD) % MOD;
	matrix trans;
	trans.mat[0][0] = x;
	trans.mat[0][1] = y;
	trans.mat[1][0] = MOD - y;
	trans.mat[1][1] = x;
	trans.mat[2][0] = trans.mat[2][2] = 1;
	matrix init;
	init.mat[0][0] = y;
	init.mat[1][0] = x;
	fout << (qpow(trans, n) * init).mat[2][0] << endl;
	return 0;
}