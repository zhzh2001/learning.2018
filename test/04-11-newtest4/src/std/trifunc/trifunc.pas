program trifunc;
uses math;
const
  p=1000000007;
var
  x,r,n,i:longint;
  angle,sum:extended;
begin
  assign(input,'trifunc.in');
  reset(input);
  assign(output,'trifunc.out');
  rewrite(output);
  read(x,r,n);
  angle:=arccos(x/r);
  sum:=0.0;
  for i:=1 to n do
    sum:=sum+sin(i*angle)*power(r,i);
  writeln((round(sum) mod p+p) mod p);
  close(input);
  close(output);
end.