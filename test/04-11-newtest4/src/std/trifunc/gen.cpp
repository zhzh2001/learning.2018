#include <fstream>
#include <random>
#include <ctime>
using namespace std;
ofstream fout("trifunc.in");
const int a = 487517547, b = 812529245, len = 5e5;
int main()
{
	minstd_rand gen(time(NULL));
	fout << a << ' ' << b << ' ';
	for (int i = 1; i <= len; i++)
	{
		uniform_int_distribution<> d('0', '9');
		fout.put(d(gen));
	}
	fout << endl;
	return 0;
}