#include <fstream>
#include <cmath>
using namespace std;
ifstream fin("trifunc.in");
ofstream fout("trifunc.out");
const int MOD = 1e9 + 7;
int main()
{
	int x, r, n;
	fin >> x >> r >> n;
	double alpha = acos(1.0 * x / r), sum = .0;
	for (int i = 1; i <= n; i++)
		sum += sin(alpha * i) * pow(r, i);
	fout << ((long long)floor(sum + .5) % MOD + MOD) % MOD << endl;
	return 0;
}