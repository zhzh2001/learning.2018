#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
using namespace std;
typedef long long LL;
const LL md=1000000007;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL A,B,N,Bi;
double alpha,ans=0;
int main(){
	freopen("trifunc.in","r",stdin);
	freopen("trifunc.out","w",stdout);
	A=read();
	B=read();
	N=read();
	Bi=1;
	alpha=acos((double)(A)/B);
	for(LL i=1;i<=N;i++){
		(Bi*=B)%=md;
		ans=fmod(ans+Bi*sin(i*alpha),md);
	}
	ans+=md;
	ans=fmod(ans,md);
	printf("%.0lf",ans);
	return 0;
}
