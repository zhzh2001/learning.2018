#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
const LL md=1000000007,P=999999937;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL L,R,pri[30000003],prc=0,ans1=0,ans2=0;
bool vis[30000003];
bool check(LL v){
	for(LL i=1;v/i/i>0;i++)if(v%i==0){
		return 0;
	}
	return 1;
}
int main(){
	freopen("primehash.in","r",stdin);
	freopen("primehash.out","w",stdout);
	L=read();
	R=read();
	if(L==R){
		if(check(L)){
			printf("1 %lld\n",L);
		}else{
			puts("0 0");
		}
		return 0;
	}
	vis[1]=1;
	for(LL i=2;i<=R;i++){
		if(!vis[i]){
			pri[++prc]=i;
		}
		for(LL j=1;j<=prc;j++){
			if(R/pri[j]<i)break;
			vis[i*pri[j]]=1;
			if(i%pri[j]==0)break;
		}
	}
	for(LL j=1;j<=prc;j++)if(pri[j]>=L){
		ans1++;
		ans2=(ans2*P+pri[j])%md;
	}
	printf("%lld %lld\n",ans1,ans2);
	return 0;
}
