#include <iostream>
#include <algorithm>
#include <cstdio>
#include <map>
using namespace std;
typedef long long LL;
const LL l9=10000000000ll;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL n,m,lb,lc,cs=0,ans[300003];
struct p3{LL a,b,c;}a[300003];
bool operator<(p3 a,p3 b){
	return a.a<b.a;
}
map<LL,LL>lt,cn;
map<LL,LL>::reverse_iterator ite;
int main(){
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	n=read();
	m=read();
	cn.clear();
	cn[m]=l9;
	for(LL i=1;i<=n;i++){
		a[i].a=read();
		a[i].b=read();
		a[i].c=read();
		lt[a[i].b]=m;
	}
	lb=m;
	lc=l9;
	sort(a+1,a+n+1);
	for(LL i=1;i<=n;i++){
		LL&x=lt[a[i].b];
		cn[x]--;
		x+=a[i].c;
		cn[x]++;
		ite=cn.rbegin();
		if(ite->first!=lb||ite->second!=lc){
			lb=ite->first;
			lc=ite->second;
			ans[++cs]=a[i].a;
		}
	}
	printf("%lld\n",cs);
	for(LL i=1;i<=cs;i++){
		printf("%lld\n",ans[i]);
	}
	return 0;
}
