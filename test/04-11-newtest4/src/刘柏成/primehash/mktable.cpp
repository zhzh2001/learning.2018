#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int mod=1000000007;
const int base=999999937;
int primes[10000002],cur;
bool ntprime[10000002];
void sieve(int n){
	ntprime[1]=1;
	for(int i=2;i<=n;i++){
		if (!ntprime[i])
			primes[++cur]=i;
		for(int j=1;j<=cur;j++){
			int now=i*primes[j];
			if (now>n)
				break;
			ntprime[now]=1;
			if (i%primes[j]==0)
				break;
		}
	}
}
bool vis[30000002];
void segsieve(ll l,ll r,ll &cnt,ll &ans){
	for(int i=0;i<=r-l;i++)
		vis[i]=0;
	for(int i=1;i<=cur;i++){
		ll k=primes[i];
		if (k*k>r)
			break;
		for(ll j=l%k==0?l:l+k-l%k;j<=r;j+=k)
			vis[j-l]=1;
	}
	for(ll i=l;i<=r;i++)	
		if (!vis[i-l]){
//			printf("%lld\n",i);
			ans=(ans*base+i)%mod;
			cnt++;
		}
}
ll x[502],y[502];
int main(){
	freopen("primehash.in","r",stdin);
	freopen("table.out","w",stdout);
	ll l=1,r=10000000000LL;
	if (r-l<=30000000){
		ll tmp=sqrt(r)+0.5;
		sieve(tmp);
		ll cnt=0,ans=0; 
		segsieve(l,r,cnt,ans);
		printf("%lld %lld\n",cnt,ans);
	}
	sieve(sqrt(r)+0.5);
	ll now=l;
	ll cnt=0,ans=0,tp=0;
	while(now<=r){
		segsieve(now,min(now+29999999,r),cnt,ans);
		now+=30000000;
		x[++tp]=cnt;
		y[tp]=ans;
	}
	printf("int x[502]={0,");
	for(int i=1;i<=tp;i++)
		printf("%lld%c",x[i],i==tp?'}':',');
	puts(";");
	printf("int y[502]={0,");
	for(int i=1;i<=tp;i++)
		printf("%lld%c",y[i],i==tp?'}':',');
	puts(";");
}
