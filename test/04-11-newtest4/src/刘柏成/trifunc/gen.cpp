#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int main(){
	freopen("trifunc.in","w",stdout);
	mt19937 w(time(0));
	ll b=w()%100000000+1;
	ll a=w()%(2*b+1)-b;
	printf("%lld %lld ",a,b);
	int n=500000;
	while(n--)
		putchar(n==499999?w()%9+'1':w()%10+'0');
}
