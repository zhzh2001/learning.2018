#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int mod=1000000007;
ll power(ll x,ll y){
	ll ans=1;
	x%=mod;
	while(y){
		if (y&1)
			ans=ans*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return ans;
}
struct cp{
	ll a,b; //a+bi
	cp(ll a=0,ll b=0):a(a),b(b){}
	void print(){
		printf("%lld %lld\n",(a+mod)%mod,(b+mod)%mod);
	}
	cp operator +(const cp& rhs)const{
		return cp((a+rhs.a)%mod,(b+rhs.b)%mod);
	}
	cp operator -(const cp& rhs)const{
		return cp((a-rhs.a)%mod,(b-rhs.b)%mod);
	}
	cp operator *(const cp& rhs)const{
		return cp((a*rhs.a-b*rhs.b)%mod,(a*rhs.b+b*rhs.a)%mod);
	}
	cp operator /(const cp& rhs)const{
//		printf("dividing %lld %lld and %lld %lld\n",a,b,rhs.a,rhs.b);
		ll inv=power(rhs.a*rhs.a+rhs.b*rhs.b,mod-2);
//		printf("inv=%lld %lld\n",inv,inv*18%mod);
		return cp((a*rhs.a+b*rhs.b)%mod*inv%mod,(b*rhs.a-a*rhs.b)%mod*inv%mod);
	}
};
cp power(cp x,int* y,int l){
	cp ans=1;
	for(int i=0;i<l;i++){
		int k=y[i];
		while(k--)
			ans=ans*x;
		cp t=x;
		x=x*x;
		x=x*x;
		x=x*x;
		x=x*t;
		x=x*t;
	}
	return ans;
}
cp power(cp x,ll y){
	cp ans=1;
	while(y){
		if (y&1)
			ans=ans*x;
		x=x*x;
		y>>=1;
	}
	return ans;
}
char s[500003];
int n[500003];
int main(){
	freopen("trifunc.in","r",stdin);
	freopen("trifunc.out","w",stdout);
	ll a,b;
	scanf("%lld%lld",&a,&b);
	ll c=sqrt(b*b-a*a)+0.5;
	if (!c)
		return puts("0"),0;
	while(c*c>b*b-a*a)
		c--;
	while(c*c<b*b-a*a)
		c++;
	c%=mod;
	scanf("%s",s);
//	ll tat;
//	sscanf(s,"%lld",&tat);
	int l=strlen(s);
	reverse(s,s+l);
	for(int i=0;i<l;i++)
		n[i]=s[i]-'0';
	n[0]++;
	for(int i=0;i<l;i++)
		if (n[i]==10)
			n[i+1]++,n[i]=0;
	if (n[l])
		l++;
	cp qwq(a,c),ans=power(qwq,n,l);
//	ans=power(qwq,tat+1);
	/*qwq.print();
	ans.print();*/
	ans=(ans-1)/(qwq-1);
	printf("%lld\n",(ans.b%mod+mod)%mod);
//	fprintf(stderr,"%d",clock());
}
