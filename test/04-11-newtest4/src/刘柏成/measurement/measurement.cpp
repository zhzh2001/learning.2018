#include <bits/stdc++.h>
#include <ext/pb_ds/priority_queue.hpp>
using namespace std;
typedef long long ll;
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
struct query{
	int t,p,x;
	bool operator <(const query& rhs)const{
		return t<rhs.t;
	}
}qu[300003];
int nums[300003];
int now[300003];
__gnu_pbds::priority_queue<pair<int,int> >q;
__gnu_pbds::priority_queue<pair<int,int> >::point_iterator it[300003];
map<int,int> mp;
int ans[300003],tp=0;
int main(){
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	int n=read(),cur=0;
	read();
	for(int i=1;i<=n;i++){
		qu[i].t=read(),qu[i].p=read(),qu[i].x=read();
		nums[++cur]=qu[i].p;
	}
	nums[++cur]=0;
	sort(nums+1,nums+cur+1);
	cur=unique(nums+1,nums+cur+1)-nums-1;
	sort(qu+1,qu+n+1);
	for(int i=1;i<=n;i++)
		qu[i].p=lower_bound(nums+1,nums+cur+1,qu[i].p)-nums;
	for(int i=1;i<=cur;i++)
		it[i]=q.push(make_pair(0,i)),mp[0]++;
	for(int i=1;i<=n;i++){
		pair<int,int> lst=q.top();
		int lstc=mp[lst.first];
		mp[now[qu[i].p]]--;
		now[qu[i].p]+=qu[i].x;
		mp[now[qu[i].p]]++;
		q.modify(it[qu[i].p],make_pair(now[qu[i].p],qu[i].p));
		pair<int,int> now=q.top();
		int nowc=mp[now.first];
//		printf("%d %d %d %d\n",lst.first,lstc,now.first,nowc);
		if (lstc!=nowc){
			ans[++tp]=qu[i].t;
			continue;
		}
		if (now.first==lst.first)
			continue;
		if (lstc==1 && nowc==1 && lst.second==now.second)
			continue;
		ans[++tp]=qu[i].t;
	}
	printf("%d\n",tp);
	for(int i=1;i<=tp;i++)
		printf("%d\n",ans[i]);
}
