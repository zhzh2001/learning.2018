#include <cstdio>

const int N=1000000;

bool b[1200000];
int h[1200000],p[1200000];
long long q,l,r;

inline void pre()
{
	for (int i=2;i<=N;i++)
		if (! b[i])
			for (int j=i+i;j<=N;j=j+i)
				b[j]=true;
	for (int i=2;i<=N;i++)
	{
		h[i]=h[i-1];
		if (! b[i])
			q++,p[q]=i,h[i]++;
	}
	return;
}

inline long long dfs(long long x,int y)
{
	if (! y)
		return x;
	if (x<=p[y])
		return 1;
	int z=0;
	for (;(y>1) && (1LL*p[y]*p[y]>x);y--,z++);
	return dfs(x,y-1)-dfs(x/p[y],y-1)-z;
}

inline long long calc(long long x)
{
	if (! x)
		return 1;
	int k=1;
	for (;(k<=q) && (p[k]<=x);k++);
	return dfs(x,k-1)+k-1;
}

inline bool check(long long x)
{
	for (int i=2;1LL*i*i<=x;i++)
		if (! (x%i))
			return false;
	return true;
}

inline void calc0(long long l,long long r)
{
	int p1=999999937,p2=1000000007,p3=1;
	long long s1=0,s2=0;
	for (long long i=r;i>=l;i--)
		if (check(i))
			s1++,s2=(s2+i%p2*p3%p2)%p2,p3=1LL*p3*p1%p2;
	printf("%lld %lld\n",s1,s2);
	return;
}

int main()
{
	freopen("primehash.in","r",stdin);
	freopen("primehash.out","w",stdout);
	pre();
	scanf("%lld%lld",&l,&r);
	if (r-l<=10000)
		calc0(l,r);
	else
		printf("%lld",calc(r)-calc(l-1));
	return 0;
}
