#include <cstdio>
#include <cmath>

const double pi=acos(-1);

int main()
{
	printf("%.5lf %.5lf\n",sin(pi/4+pi/3),sin(pi/4)*cos(pi/3)+sin(pi/3)*cos(pi/4));
	printf("%.5lf %.5lf\n",cos(pi/4+pi/7),cos(pi/4)*cos(pi/7)-sin(pi/7)*sin(pi/4));
	return 0;
}
