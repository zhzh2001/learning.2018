#include <cstdio>
#include <cstring>
#include <cmath>

const int p=1000000007;

struct matrix
{
	int g[5][5];
};

matrix first,basic,empty,result;

inline matrix matrixmul(matrix x,matrix y)
{
	matrix z;
	memset(z.g,0,sizeof(z.g));
	for (int i=1;i<=3;i++)
		for (int j=1;j<=3;j++)
			for (int k=1;k<=3;k++)
				z.g[i][j]=(z.g[i][j]+1LL*x.g[i][k]*y.g[k][j])%p;
	return z;
}

char c[600000];
long long g[600000],h[600000];
int i,j,n,o,q,t,u,v,w,x,y;

int main()
{
	freopen("trifunc.in","r",stdin); freopen("trifunc.out","w",stdout);
	scanf("%d%d%s",&x,&y,c+1),u=((long long) (trunc(sqrt(y*y-x*x))+0.5))%p,v=(x+p)%p;
	first.g[1][1]=u,first.g[1][2]=v,empty.g[1][1]=1,empty.g[2][2]=1,empty.g[3][3]=1;
	basic.g[1][1]=v,basic.g[1][2]=p-u,basic.g[1][3]=1,basic.g[2][1]=u,basic.g[2][2]=v,basic.g[3][3]=1;
	n=strlen(c+1); w=(1<<29)-1; o=1; q=1000000000;
	for (i=n;i>0;i--) { t=t+(c[i]-48)*o,o=o*10; if (! ((n-i+1)%9)) g[0]++,g[g[0]]=t,t=0,o=1; } if (t) g[0]++,g[g[0]]=t;
	while (g[0]) { for (i=g[0];i>1;i--) g[i-1]=g[i-1]+(g[i]&w)*q,g[i]=g[i]>>29; h[0]++,h[h[0]]=w&g[1],g[1]=g[1]>>29; while ((g[0]) && (! g[g[0]])) g[0]--; }
	result=empty; for (i=h[0];i>0;i--) for (j=28;j>=0;j--) { result=matrixmul(result,result); if (h[i]&(1<<j)) result=matrixmul(result,basic); }
	result=matrixmul(first,result); printf("%d",result.g[1][3]);
	return 0;
}
