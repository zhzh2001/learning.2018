#include <cstdio>
#include <ctime>
#include <algorithm>
#include <map>

using namespace std;

map <int,pair <int,int> > M;

inline int Rand()
{
	return rand()<<15|rand();
}

int main()
{
	freopen("measurement.in","w",stdout);
	srand((int) time(0));
	int g[500000];
	int n=300000,m=500000000,x=Rand()%1000000000+1,y;
	printf("%d %d\n",n,m);
	for (int i=1;i<=n;i++)
	{
		while (M[x].first) x=Rand()%1000000000+1;
		g[i]=x,M[x]=make_pair(Rand()%50+1,Rand()%100000-50000);
	}
	for (int i=1;i<=n;i++)
		x=Rand()%n+1,y=Rand()%n+1,swap(g[x],g[y]);
	for (int i=1;i<=n;i++)
	{
		printf("%d %d ",g[i],M[g[i]].first);
		if (M[g[i]].second>=0) printf("+%d\n",M[g[i]].second+1); else printf("%d\n",M[g[i]].second);
	}
	return 0;
}
