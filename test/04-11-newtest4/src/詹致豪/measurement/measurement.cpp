#include <cstdio>
#include <algorithm>

using namespace std;

const int inf=1000000000;

struct treenode
{
	int left,right,max,maxnum,maxpos;
};

treenode tree[10000000];
int root,sum_node,prenum,prepos;

inline void merge(int l,int r,int mid,treenode x,treenode y,treenode &z)
{
	if (! x.maxnum)
		x.maxnum=mid-l+1,x.maxpos=l;
	if (! y.maxnum)
		y.maxnum=r-mid,y.maxpos=mid+1;
	z.max=max(x.max,y.max),z.maxnum=0;
	if (x.max==z.max)
		z.maxnum=z.maxnum+x.maxnum,z.maxpos=x.maxpos;
	if (y.max==z.max)
		z.maxnum=z.maxnum+y.maxnum,z.maxpos=y.maxpos;
	return;
}

inline void inserttree(int l,int r,int x,int y,int &node)
{
	if (! node)
		sum_node++,node=sum_node;
	if (l==r)
		tree[node].max=tree[node].max+y,tree[node].maxnum=1,tree[node].maxpos=l;
	else
	{
		int mid=(l+r)>>1;
		if (x<=mid)
			inserttree(l,mid,x,y,tree[node].left);
		else
			inserttree(mid+1,r,x,y,tree[node].right);
		merge(l,r,mid,tree[tree[node].left],tree[tree[node].right],tree[node]);
	}
	return;
}

struct query
{
	int x,y,z;
	
	inline bool operator < (const query t) const
	{
		return x<t.x;
	}
};

query q[500000];

inline int fastscanf()
{
	int t=0,p=1;
	char c=getchar();
	while ((! ((c>47) && (c<58))) && ((c!='+') && (c!='-')))
		c=getchar();
	if (c=='+')
		p=1,c=getchar();
	if (c=='-')
		p=-1,c=getchar();
	while ((c>47) && (c<58))
		t=t*10+c-48,c=getchar();
	return t*p;
}

int w[500000];
int i,m,n;

int main()
{
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	n=fastscanf(),fastscanf();
	for (i=1;i<=n;i++)
		q[i].x=fastscanf(),q[i].y=fastscanf(),q[i].z=fastscanf();
	sort(q+1,q+n+1);
	inserttree(0,inf,0,0,root);
	for (i=1;i<=n;i++)
	{
		prenum=tree[root].maxnum;
		prepos=tree[root].maxpos;
		inserttree(0,inf,q[i].y,q[i].z,root);
		if ((prenum!=tree[root].maxnum) || (prepos!=tree[root].maxpos))
			m++,w[m]=q[i].x;
	}
	printf("%d\n",m);
	for (i=1;i<=m;i++)
		printf("%d\n",w[i]);
	return 0;
}
