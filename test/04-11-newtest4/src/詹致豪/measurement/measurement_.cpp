#include <cstdio>
#include <algorithm>

using namespace std;

const int inf=1000000000;

struct query
{
	int x,y,z;
	
	inline bool operator < (const query t) const
	{
		return x<t.x;
	}
};

query q[500000];

inline int fastscanf()
{
	int t=0,p=1;
	char c=getchar();
	while ((! ((c>47) && (c<58))) && ((c!='+') && (c!='-')))
		c=getchar();
	if (c=='+')
		p=1,c=getchar();
	if (c=='-')
		p=-1,c=getchar();
	while ((c>47) && (c<58))
		t=t*10+c-48,c=getchar();
	return t*p;
}

int g[500000],w[500000];
int i,j,k,m,n;
long long r,t,tt;

int main()
{
	freopen("measurement.in","r",stdin);
	freopen("measurement_.out","w",stdout);
	n=fastscanf(),k=fastscanf();
	for (i=0;i<=50;i++)
		g[i]=k;
	for (i=1;i<=n;i++)
		q[i].x=fastscanf(),q[i].y=fastscanf(),q[i].z=fastscanf();
	sort(q+1,q+n+1);
	for (i=1;i<=n;i++)
	{
		g[q[i].y]=g[q[i].y]+q[i].z;
		t=g[0],tt=1;
		for (j=1;j<=50;j++)
			if (g[j]>t)
				t=g[j],tt=1LL<<j;
			else
				if (g[j]==t)
					tt=tt|(1LL<<j);
		if (r!=tt)
			m++,w[m]=q[i].x;
		r=tt;
	}
	printf("%d\n",m);
	for (i=1;i<=m;i++)
		printf("%d\n",w[i]);
	return 0;
}
