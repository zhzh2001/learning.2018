#include <bits/stdc++.h>
#define ll long long
#define N 300005
#define inf 1000000000
#define mod (1000000007)
#define gc getchar()
#define db double
#define ldb long db
#define int ll
using namespace std;
inline ll read(){
	ll sum=0,p=1;char c=gc;
	for (;c<'0'||c>'9';c=gc) if (c=='-') p=-1;
	for (;c>='0'&&c<='9';c=gc) sum=sum*10+c-'0';
	return sum*p;
}
inline void wr(int x){
	if (x<0) putchar('-'),x=-x;
	if (x<=9) putchar('0'+x);
	else wr(x/10),putchar('0'+x%10);
}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);putchar('\n');}
ll a,b,n,sum;
ldb xpp,ans;
void add(ll &x,ll y){
	x+=y;if (x>mod) x-=mod;
	if (x<=-mod) x+=mod;
}
signed main(){
	freopen("trifunc.in","r",stdin);
	freopen("trifunc.out","w",stdout);
	a=read();b=read();
	if (a==0) return puts("0"),0;
	n=read();sum=1;
	ldb a1=a,b1=b;
	ldb arc=acos(a1/b1);
	for (int i=n;i;i--){
		ldb po=i;
		ldb ij=sin(po*arc);
		xpp+=ij;
		xpp*=b;
		ll yk=floor(xpp);
		xpp-=yk/mod*mod;
	}
	ll yk=round(xpp);
	if (yk<0) yk+=mod;
	wrn(yk);
	return 0;
}
