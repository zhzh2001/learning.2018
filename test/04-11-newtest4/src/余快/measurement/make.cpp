#include <bits/stdc++.h>
#define ll long long
#define N 300005
#define inf 1000000000
#define gc getchar()
using namespace std;
inline int read(){
	int sum=0,p=1;char c=gc;
	for (;c<'0'||c>'9';c=gc) if (c=='-') p=-1;
	for (;c>='0'&&c<='9';c=gc) sum=sum*10+c-'0';
	return sum*p;
}
inline void wr(int x){
	if (x<0) putchar('-'),x=-x;
	if (x<=9) putchar('0'+x);
	else wr(x/10),putchar('0'+x%10);
}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);putchar('\n');}
inline void wrn(int x,int y){wri(x);wrn(y);}
int g,n,m,f[N],ti,x,y,vis[N];
signed main(){
//		freopen("measurement.in","r",stdin);
	srand(time(NULL));
	freopen("measurement.in","w",stdout);
	n=rand()%6+3;
	g=rand()%10+5;
	wrn(n,g);
	for (int i=1;i<=15;i++) f[i]=g;
	for (int i=1;i<=n;i++){
		ti=rand()%15+1;
		while (vis[ti]) ti=rand()%15+1;vis[ti]=1;
		x=rand()%15+1;y=rand()%10-5;
		while (f[x]-y<0) x=rand()%15+1,y=rand()%10-5;
		wri(ti);wri(x);
		if (y<0) putchar('-');
		else putchar('+');
		wrn(abs(y));
		f[x]-=y;
	}
}
