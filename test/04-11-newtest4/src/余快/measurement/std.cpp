#include <bits/stdc++.h>
#define ll long long
#define N 3000005
#define inf 1000000000
#define gc getchar()
using namespace std;
inline int read(){
	int sum=0,p=1;char c=gc;
	for (;c<'0'||c>'9';c=gc) if (c=='-') p=-1;
	for (;c>='0'&&c<='9';c=gc) sum=sum*10+c-'0';
	return sum*p;
}
inline void wr(int x){
	if (x<0) putchar('-'),x=-x;
	if (x<=9) putchar('0'+x);
	else wr(x/10),putchar('0'+x%10);
}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);putchar('\n');}
struct xx{
	int i,x,y;
}z[N];
int e,tot[2],zh[N][2],f[N],n,tmax,g;
bool cmp(xx a,xx b){
	return a.i<b.i;
}
void get(int x){
	int sum=0;tot[x]=0;
	for (int i=1;i<=tmax;i++){
		if (f[i]>sum) tot[x]=0,sum=f[i];
		if (f[i]==sum) zh[++tot[x]][x]=i;
	}
}
int pd(){
	if (tot[0]!=tot[1]) return 1;
	for (int i=1;i<=tot[0];i++){
		if (zh[i][0]!=zh[i][1]) return 1;
	}
	return 0;
}
int cnt,ans[N];
signed main(){
	freopen("measurement.in","r",stdin);
	freopen("std.out","w",stdout);
	n=read();g=read();
	for (int i=1;i<=n;i++){
		z[i].i=read();z[i].x=read();z[i].y=read();
		tmax=max(tmax,z[i].x);
	}
	tmax++;
	sort(z+1,z+n+1,cmp);
	for (int i=1;i<=tmax;i++){
		f[i]=g;
	}
	get(e);
	for (int i=1;i<=n;i++){
		e^=1;
		f[z[i].x]+=z[i].y;
		get(e);
		if (pd()) ans[++cnt]=z[i].i;
	}
	wrn(cnt);
	for (int i=1;i<=cnt;i++) wrn(ans[i]);
}
