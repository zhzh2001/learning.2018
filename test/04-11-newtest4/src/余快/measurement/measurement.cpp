#include <bits/stdc++.h>
#define ll long long
#define N 300005
#define inf 1000000000
#define gc getchar()
using namespace std;
inline int read(){
	int sum=0,p=1;char c=gc;
	for (;c<'0'||c>'9';c=gc) if (c=='-') p=-1;
	for (;c>='0'&&c<='9';c=gc) sum=sum*10+c-'0';
	return sum*p;
}
inline void wr(int x){
	if (x<0) putchar('-'),x=-x;
	if (x<=9) putchar('0'+x);
	else wr(x/10),putchar('0'+x%10);
}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);putchar('\n');}
struct xx{
	int i,a,v;
}z[N];
bool cmp(xx a,xx b){
	return a.i<b.i;
}
map <int,int> p;
int pnum,n,g,x,tot,root,f[N],ans[N],cnt,tmax,yk;
struct tree1{
	int l,r,num;
}t[N*30];
/*inline int query(int x,int l,int r){
	if (!x) return 0;
	if (r==l){
		if (t[x].max>0) return r;
		else return 0;
	}
	else return t[x].max;
}*/
void newp(int &x){x=++pnum;}
void add(int &x,int k,int p,int l,int r){
	if (!x) newp(x);
	t[x].num+=p;
	if (l==r) return;
	int mid=(l+r)>>1;
	if (k<=mid) add(t[x].l,k,p,l,mid);
	else add(t[x].r,k,p,mid+1,r);
//	t[x].max=max(query(t[x].l,l,mid),query(t[x].r,mid+1,r));
}
int query(int x,int l,int r){
	if (l==r){yk=t[x].num;return l;}
	int mid=(l+r)>>1;
	if (t[x].r&&t[t[x].r].num) return query(t[x].r,mid+1,r);
	else return query(t[x].l,l,mid);
}
signed main(){
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	n=read();g=read();
	for (int i=1;i<=n;i++){
		z[i].i=read();x=read();
		if (p.find(x)!=p.end()) z[i].a=p[x];
		else z[i].a=p[x]=++tot;
		z[i].v=read();
	}
	sort(z+1,z+n+1,cmp);
	root=0;
	add(root,g,10000000,1,inf);
	for (int i=1;i<=tot;i++) f[i]=g; 
	for (int i=1;i<=n;i++){
		tmax=query(1,1,inf);
		if (f[z[i].a]==tmax){
			add(root,f[z[i].a],-1,1,inf);
			f[z[i].a]+=z[i].v;
			add(root,f[z[i].a],1,1,inf);
			if (z[i].v!=0){
			if (yk>1) ans[++cnt]=z[i].i;
			else {
				int xpp=query(1,1,inf);
				if (xpp!=f[z[i].a]||yk>1) ans[++cnt]=z[i].i;
			}
			}
		}
		else{
			add(root,f[z[i].a],-1,1,inf);
			f[z[i].a]+=z[i].v;
			if (f[z[i].a]>=tmax) ans[++cnt]=z[i].i;
			add(root,f[z[i].a],1,1,inf);
		}
	}
	wrn(cnt);
	for (int i=1;i<=cnt;i++) wrn(ans[i]);
	return 0;
} 
