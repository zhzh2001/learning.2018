#include <bits/stdc++.h>
#define ll long long
#define N 300005
#define inf 1000000000
#define gc getchar()
#define mod 1000000007
using namespace std;
inline ll read(){
	ll sum=0,p=1;char c=gc;
	for (;c<'0'||c>'9';c=gc) if (c=='-') p=-1;
	for (;c>='0'&&c<='9';c=gc) sum=sum*10+c-'0';
	return sum*p;
}
inline void wr(int x){
	if (x<0) putchar('-'),x=-x;
	if (x<=9) putchar('0'+x);
	else wr(x/10),putchar('0'+x%10);
}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);putchar('\n');}
inline void add(ll &x,int y){
	x+=y;if (x>mod) x-=mod;
}
ll l,r,p,tot,ans,cnt;
int vis[30000005];
ll z[6000005],f[1500000];
inline int check(int x){
	for (int i=1;i<=tot&&z[i]<=x/z[i];i++){
		if (x%z[i]==0) return 0;
	}
	return 1;
}
signed main(){
	freopen("primehash.in","r",stdin);
	freopen("std.out","w",stdout);
	l=read();r=read();p=999999937;
	if (r-l<=3000000){
		for (int i=2;i<=sqrt(r);i++){
			if (!vis[i]) z[++tot]=i;
			for (int j=1;j<=tot&&z[j]<=sqrt(r)/i;j++){
				vis[z[j]*i]=1;
				if (i%z[j]==0) break;
			}
		}
		for (ll i=l;i<=r;i++){
			if (check(i)) f[++cnt]=i;
		}
		int sum=1;
		for (int i=cnt;i;i--){
			add(ans,(ll)f[i]*sum%mod);
			sum=(ll)sum*p%mod;
		}
		wri(cnt);wrn(ans);
	}
	return 0;
}
