#include <bits/stdc++.h>
#define ll long long
#define N 300005
#define inf 1000000000
#define gc getchar()
#define int ll
using namespace std;
inline int read(){
	int sum=0,p=1;char c=gc;
	for (;c<'0'||c>'9';c=gc) if (c=='-') p=-1;
	for (;c>='0'&&c<='9';c=gc) sum=sum*10+c-'0';
	return sum*p;
}
inline void wr(int x){
	if (x<0) putchar('-'),x=-x;
	if (x<=9) putchar('0'+x);
	else wr(x/10),putchar('0'+x%10);
}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);putchar('\n');}
ll l,r;
signed main(){
	freopen("primehash.in","w",stdout);
	srand(time(NULL));
	l=(rand()<<6)|rand();r=(rand()<<5)|rand();
	if (l>r) swap(l,r);
	wri(l);wrn(r);
}
