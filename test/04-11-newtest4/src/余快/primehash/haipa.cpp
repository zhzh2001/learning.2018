#include <bits/stdc++.h>
#define ll long long
#define N 300005
#define inf 1000000000
#define gc getchar()
using namespace std;
inline int read(){
	int sum=0,p=1;char c=gc;
	for (;c<'0'||c>'9';c=gc) if (c=='-') p=-1;
	for (;c>='0'&&c<='9';c=gc) sum=sum*10+c-'0';
	return sum*p;
}
inline void wr(int x){
	if (x<0) putchar('-'),x=-x;
	if (x<=9) putchar('0'+x);
	else wr(x/10),putchar('0'+x%10);
}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);putchar('\n');}
int p1[11]={2,3,5,7,11,13,17,37,31,41,19};
ll ksm(ll x,ll k,ll p){
	ll sum=1;
	while (k){
		if (k&1) sum=x*sum%p;
		x=x*x%p;
		k=k>>1;
	}
	return sum;
}
signed main(){
	int x=334153;
	for (int i=2;i<=100000;i++) if (x%i==0) wrn(i);
	for (int i=0;i<=10;i++){
		wrn(ksm(p1[i],x-1,x)); 
	}	
	return 0;
}
