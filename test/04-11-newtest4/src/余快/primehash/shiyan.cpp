#include <bits/stdc++.h>
#define ll long long
#define N 300005
#define inf 1000000000
#define gc getchar()
#define mod 1000000007
using namespace std;
inline ll read(){
	ll sum=0,p=1;char c=gc;
	for (;c<'0'||c>'9';c=gc) if (c=='-') p=-1;
	for (;c>='0'&&c<='9';c=gc) sum=sum*10+c-'0';
	return sum*p;
}
inline void wr(int x){
	if (x<0) putchar('-'),x=-x;
	if (x<=9) putchar('0'+x);
	else wr(x/10),putchar('0'+x%10);
}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);putchar('\n');}
inline void add(ll &x,int y){
	x+=y;if (x>mod) x-=mod;
}
ll l,r,p,tot,ans;
int vis[30000005];
ll z[60000005];
int p1[11]={2,3,5,7,11,13,17,37,31,41,19};
inline void add2(ll &x,ll y,ll p){
	x+=y;if (x>p) x-=p;
}
ll ksc(ll x,ll y,ll p){
	ll ans=0;
	while (y){
		if (y&1) add2(ans,x,p);
		x=x<<1;
		if (x>p) x-=p;
		y>>=1;
	}
	return ans;
}
ll ksm(ll x,ll k,ll p){
	ll sum=1;
	while (k){
		if (k&1) sum=ksc(sum,x,p);
		x=ksc(x,x,p);
		k=k>>1;
	}
	return sum;
}
int check(ll x){
	if ((x&1)==0&&x!=2) return 0;
	if (x%3==0&&x!=3) return 0;
	for (int i=0;i<=10&&p1[i]<x;i++){
		if (ksm(p1[i],x-1,x)!=1) return 0;
	}	
	return 1;
}
signed main(){
//	freopen("primehash.in","r",stdin);
	freopen("shiyan.out","w",stdout);
	l=read();r=read();p=999999937;
	if (r<=30000000){
		for (int i=2;i<=r;i++){
			if (!vis[i]) z[++tot]=i;
			if (check(i)==vis[i]) wrn(i);
			for (int j=1;j<=tot&&z[j]<=r/i;j++){
				vis[z[j]*i]=1;
				if (i%z[j]==0) break;
			}
		}
		/*tot=0;
		for (int i=l;i<=r;i++){
			if (!vis[i]) z[++tot]=i;
		}
		int sum=1;
		for (int i=tot;i;i--){
			add(ans,(ll)z[i]*sum%mod);
			sum=(ll)sum*p%mod;
		}		
		wri(tot);wrn(ans);*/
		return 0;
	}
	if (r-l<=3000000){
		for (ll i=l;i<=r;i++){
			if (check(i)) z[++tot]=i;
		}
		int sum=1;
		for (int i=tot;i;i--){
			add(ans,(ll)z[i]*sum%mod);
			sum=(ll)sum*p%mod;
		}
		wri(tot);wrn(ans);
	}
	return 0;
}
