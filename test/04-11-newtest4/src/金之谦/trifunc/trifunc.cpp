#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int MOD=1e9+7;
struct juzhen{
	int c[4][4];
}a,ans,zz,kk;
inline juzhen cheng(juzhen a,juzhen b){
	memset(zz.c,0,sizeof zz.c);
	for(int i=1;i<4;i++)
		for(int j=1;j<4;j++)
			for(int k=1;k<4;k++)zz.c[i][j]=(zz.c[i][j]+a.c[i][k]*b.c[k][j])%MOD;
	return zz;
}
const int N=5e5+10;
char n[N];
int s,c,l;
inline int sqr(int x){return x*x;}
inline juzhen Mi(juzhen a,int b){
	if(b==0)return kk;
	juzhen x=a,y=a;b--;
	while(b){
		if(b&1)x=cheng(x,y);y=cheng(y,y);b>>=1;
	}
	return x;
}
inline juzhen mi(juzhen a){
	juzhen x=kk,y;
	for(int i=1;i<=l;i++){
		x=cheng(x,Mi(a,n[i]-'0'));
		if(i<l)x=Mi(x,10);
	}
	return x;
}
signed main()
{
	freopen("trifunc.in","r",stdin);
	freopen("trifunc.out","w",stdout);
	int c=read();int k=read();
	if(c==k)return puts("0")&0;
	scanf("%s",n+1);l=strlen(n+1);
	kk.c[1][1]=kk.c[2][2]=kk.c[3][3]=1;
	s=sqrt(sqr(k)-sqr(c));c=(c+MOD)%MOD;
	ans.c[1][1]=s;ans.c[1][2]=c;
	a.c[1][1]=a.c[2][2]=c;
	a.c[2][1]=s;a.c[1][2]=(-s+MOD)%MOD;
	a.c[1][3]=a.c[3][3]=1;
	ans=cheng(ans,mi(a));
	writeln(ans.c[1][3]);
	return 0;
}
