#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=300010;
struct ppap{int d,p,v;}a[N];
inline bool cmp(ppap a,ppap b){return a.d<b.d;}
map<int,int>mp;
multiset<int>s;
int n,m,cnt=0,g[N],ans[N];
int main()
{
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++){
		a[i].d=read();a[i].p=read();a[i].v=read();
		if(!mp[a[i].p])mp[a[i].p]=++cnt;
		a[i].p=mp[a[i].p];
	}
	mp.clear();
	sort(a+1,a+n+1,cmp);
	mp[m]=cnt+1;
	for(int i=0;i<=cnt;i++)s.insert(m),g[i]=m;
	int lam=m,las=cnt+1;
	multiset<int>::iterator it;
	for(int i=1;i<=n;i++){
		it=s.lower_bound(g[a[i].p]);
		bool flag=(g[a[i].p]==lam);
		if(mp[g[a[i].p]]>1)flag=0;
		int now=*it;s.erase(it);mp[now]--;
		now+=a[i].v;g[a[i].p]=now;mp[now]++;
		s.insert(now);
		it=s.end();it--;
		int ma=*it;
		if(lam==ma){
			if(mp[ma]!=las)ans[++ans[0]]=a[i].d;
		}else{
			if(g[a[i].p]==ma&&mp[ma]==1&&flag)continue;
			ans[++ans[0]]=a[i].d;
		}
		lam=ma;las=mp[ma];
	}
	writeln(ans[0]);
	for(int i=1;i<=ans[0];i++)writeln(ans[i]);
	return 0;
}
