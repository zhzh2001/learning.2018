#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll MOD=1e9+7;
const ll P=999999937;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
ll l,r;
int pri[20000010],cnt=0;
bool b[100000010];
inline void get(int n){
	for(int i=2;i<=n;i++){
		if(!b[i])pri[++cnt]=i;
		for(int j=1;j<=cnt;j++){
			if(i*pri[j]>n)break;
			b[i*pri[j]]=1;
			if(i%pri[j]==0)break;
		}
	}
}
inline ll sqr(ll x){return x*x;}
int main()
{
	freopen("primehash.in","r",stdin);
	freopen("primehash.out","w",stdout);
	l=read(),r=read();
	if(r<=1e8){
		get(r);pri[++cnt]=1e9+7;
		int i=upper_bound(pri+1,pri+cnt+1,r)-pri-1;
		ll ans=0,anss=0,p=1;
		for(;pri[i]>=l;i--){
			ans++;anss=(anss+p*(ll)pri[i]%MOD)%MOD;
			p=p*P%MOD;
		}
		write(ans);putchar(' ');writeln(anss);
	}else{
		get(sqrt(r));
		ll ans=0,anss=0,p=1;
		for(ll i=r;i>=l;i--){
			bool flag=1;
			for(int j=1;j<=cnt;j++){
				if(sqr(pri[j])>r)break;
				if(i%pri[j]==0){flag=0;break;}
			}
			if(flag){
				ans++;anss=(anss+p*i%MOD)%MOD;
				p=p*P%MOD;
			}
		}
		write(ans);putchar(' ');writeln(anss);
	}
	return 0;
}
