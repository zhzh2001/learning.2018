#include<bits/stdc++.h>
#define ll long long
#define N 22000000
#define BLK 1854000
using namespace std;
//------------------ZhouGe Sieve------------------
int cnt[N],pri[N/12],tot;
int f[70][100005];
bool fl[N];
void init(){
	for (int i=2;i<N;i++){
		cnt[i]=cnt[i-1];
		if (!fl[i]){
			pri[++tot]=i;
			cnt[i]++;
		}
		for (int j=1;i*pri[j]<N;j++){
			fl[i*pri[j]]=1;
			if (i%pri[j]==0) break;
		}
	}
	for (int i=1;i<=100000;i++)
		f[0][i]=i;
	for (int j=1;j<=68;j++)
		for (int i=1;i<=100000;i++)
			f[j][i]=f[j-1][i]-f[j-1][i/pri[j]];
}
struct HashMap{
	#define Mo 2111111
	int head[Mo];
	ll key[Mo];
	ll val[Mo];
	int nxt[Mo];
	int tot;
	HashMap(){
		tot=0;
		memset(head,0,sizeof(head));
	}
	bool find(ll hsh,ll &v){
		int tmp=hsh%Mo;
		for (int i=head[tmp];i;i=nxt[i])
			if (key[i]==hsh){
				v=val[i];
				return 1;
			}
		return 0;
	}
	void set(ll hsh,ll v){
		int tmp=hsh%Mo;
		key[++tot]=hsh; val[tot]=v;
		nxt[tot]=head[tmp]; head[tmp]=tot;
	}
}mp;
int times=0;
ll dp(ll x,int y){
	if (!y) return x;
	if (pri[y]*pri[y]>=x) return max(cnt[x]-y,0)+1;
	if (x<=100000&&y<70) return f[y][x];
	ll hsh=x*2000+y,val;
	if (mp.find(hsh,val)) return val;
	val=dp(x,y-1)-dp(x/pri[y],y-1);
	mp.set(hsh,val);
	return val;
}
ll calc(ll x){
	if (x<N) return cnt[x];
	int tmp=0;
	for (;1ll*pri[tmp]*pri[tmp]*pri[tmp]<=x;tmp++);
	tmp--;
	ll ans=tmp+dp(x,tmp)-1;
	for (tmp++;1ll*pri[tmp]*pri[tmp]<=x;tmp++)
		ans-=cnt[x/pri[tmp]]-tmp+1;
	return ans;
}
//-----------------------------Biao---------------------------
/*#define pa pair<ll,ll>
#define mo 1000000007
#define BAS 999999937
#define SZ 10000000
bool mp[SZ];
int LBC[BLK+5];
void init2(int x){
	LBC[0]=1;
	for (int i=1;i<=BLK;i++)
		LBC[i]=1ll*LBC[i-1]*999999937%mo;
}
pa calc222(ll x){
	if (!x) return pa(0,0);
	pa tmp;
	tmp.first=calc(x);
	int blk=tmp.first/BLK;
	int val=0,rest=tmp.first%BLK,now=0;
	ll lb=max(x-SZ,0),rb=x;
	for (;rest;rb-=SZ,lb=max(rb-SZ,0){
		printf("%lld %.10lf %d\n",rb,rb/1e11,sum);
		for (int i=1;i<=SZ;i++)
			mp[i]=1;
		if (lb==0) mp[1]=0;
		for (int i=1;1ll*pri[i]*pri[i]<=rb;i++){
			ll lim;
			if (!lb) lim=2*pri[i];
			else{
				lim=lb/pri[i]+(lb%pri[i]!=0);
				lim*=pri[i];
				lim-=lb;
			}
			for (;lim<=rb-lb;lim+=pri[i]) mp[lim]=0;
		}
		for (int i=rb-lb;i>=1;i--)
			if (mp[i]){
				val=(val+1ll*LBC[now]*((lb+i)%mo))%mo;
				now++; rest--;
				if (!rest) break;
			}
	}
	val=(val+1ll*biao[blk]*LBC[now])%mo;
	tmp.second=i;
	return tmp;
}*/
int main(){
	ll l;
	init();
	scanf("%lld",&l);
	int u=clock();
	printf("%lld\n",calc(l));
	printf("%d %d\n",clock()-u,times);
}
/*
int power(int x,int y){
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
int main(){
	ll l,r;
	init();
	init2();
	scanf("%lld",&l,&r);
	pa ans1=calc222(l-1);
	pa ans2=calc222(r);
	printf("%lld ",ans2.first-ans1.first);
	printf("%lld ",(ans2.second-1ll*ans1.second*power(BAS,(ans2.first-ans1.first)%(mo-1))%mo+mo)%mo);
	int u=clock();
	printf("%d\n",clock()-u);
}
*/
