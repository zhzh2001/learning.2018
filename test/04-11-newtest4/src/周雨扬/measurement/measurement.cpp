#include<bits/stdc++.h>
using namespace std;
#define N 300005
struct que{
	int t,x,delta;
}q[N];
bool cmp(que a,que b){
	return a.t<b.t;
}
map<int,int> mp,val;
int n,k,tot;
int ans[N];
int lap,lav;
#define gc getchar
inline int read(){
	int x=0,f=0;
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=1;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return (f?-x:x);
}
inline void write(int x){
	if (x==0){
		puts("0");
		return;
	}
	static int a[20],top;
	for (top=0;x;a[++top]=x%10,x/=10);
	for (;top;putchar(a[top--]+'0'));
	puts("");
}
int main(){
	freopen("measurement.in","r",stdin);
	freopen("measurement.out","w",stdout);
	n=read(); k=read();
	mp[k]=1e9;
	lap=k; lav=1e9;
	for (int i=1;i<=n;i++)
		q[i].t=read(),q[i].x=read(),q[i].delta=read();
	sort(q+1,q+n+1,cmp);
	for (int i=1;i<=n;i++){
		int v;
		if (val.find(q[i].x)==val.end())
			v=k;
		else v=val[q[i].x];
		mp[v]--; mp[v-q[i].delta]++;
		val[q[i].x]=v-q[i].delta;
		map<int,int>::iterator it=mp.begin();
		for (;!it->second;){
			mp.erase(it);
			it=mp.begin();
		}
		if (it->first!=lap||it->second!=lav)
			ans[++tot]=q[i].t;
		lap=it->first;
		lav=it->second;
	}
	write(tot);
	for (int i=1;i<=tot;i++)
		write(ans[i]);
}
