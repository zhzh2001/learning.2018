#include<bits/stdc++.h>
#define ll long long
#define mo 1000000007
using namespace std;
int power(int x,int y){
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
struct jdb{
	int x,y;
};
jdb mul(jdb a,jdb b,int w){
	jdb c;
	c.y=(1ll*a.x*b.y+1ll*b.x*a.y)%mo;
	c.x=(1ll*a.x*b.x+1ll*a.y*b.y%mo*w)%mo;
	return c;
}
int cipolla(int a){
	if (!a) return 0;
	if (power(a,(mo-1)/2)>1) return -1;
	int tmp=1;
	for (;power((1ll*tmp*tmp+mo-a)%mo,mo/2)<=1;tmp++);
	jdb s,x; int times=(mo+1)/2;
	s.x=1; s.y=0; x.x=tmp; x.y=1;
	int val=(1ll*tmp*tmp+mo-a)%mo;
	for (;times;times/=2){
		if (times&1) s=mul(s,x,val);
		x=mul(x,x,val);
	}
	return s.x;
	//if (1ll*s.x*s.x%mo!=a) printf("%d %d %d\n",tmp,s.x,a);
}
struct Mat{
	ll a[6][6];
	Mat(){
		memset(a,0,sizeof(a));
	}
	friend Mat operator *(const Mat &a,const Mat &b){
		Mat c;
		for (int i=1;i<=4;i++)
			for (int j=1;j<=4;j++){
				for (int k=1;k<=4;k++)
					c.a[i][j]+=a.a[i][k]*b.a[k][j];
				c.a[i][j]%=mo;
			}
		return c;	
	}
};
Mat power(Mat x,int y){
	Mat s; s=x; y--;
	for (;y;){
		if (y&1) s=s*x;
		if (y/=2) x=x*x;
	}
	return s;
}
#define N 500005
int a,b;
char n[N];
Mat c,ans,f[N];
int main(){
	freopen("trifunc.in","r",stdin);
	freopen("trifunc.out","w",stdout);
	scanf("%d%d%s",&a,&b,n+1);
	int len=strlen(n+1);
	reverse(n+1,n+len+1);
	int COS=a;
	int SIN=cipolla((1ll*b*b+mo-1ll*a*a%mo)%mo);
	//printf("%d %d\n",COS,SIN);
	c.a[1][2]=SIN; c.a[1][3]=COS;
	c.a[2][2]=COS; c.a[3][2]=SIN;
	c.a[3][3]=COS; c.a[2][3]=mo-SIN;
	c.a[2][4]=c.a[4][4]=1;
	f[1]=ans=c;
	for (int i=2;i<=len;i++)
		f[i]=power(f[i-1],10);
	for (int i=1;i<=len;i++)
		if (n[i]-'0')
			ans=ans*power(f[i],n[i]-'0');
	printf("%d",ans.a[1][4]);
}
