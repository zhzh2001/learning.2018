#include<bits/stdc++.h>
#define mo 1000000007
using namespace std;
int power(int x,int y){
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
struct jdb{
	int x,y;
};
jdb mul(jdb a,jdb b,int w){
	jdb c;
	c.y=(1ll*a.x*b.y+1ll*b.x*a.y)%mo;
	c.x=(1ll*a.x*b.x+1ll*a.y*b.y%mo*w)%mo;
	return c;
}
int cipolla(int a){
	if (!a) return 0;
	if (power(a,(mo-1)/2)>1) return -1;
	int tmp=1;
	for (;power((1ll*tmp*tmp+mo-a)%mo,mo/2)<=1;tmp++);
	jdb s,x; int times=(mo+1)/2;
	s.x=1; s.y=0; x.x=tmp; x.y=1;
	int val=(1ll*tmp*tmp+mo-a)%mo;
	for (;times;times/=2){
		if (times&1) s=mul(s,x,val);
		x=mul(x,x,val);
	}
	return s.x;
}
int rnd(){
	int x=0;
	for (int i=1;i<=9;i++)
		x=x*10+rand()%10;
	return x;
}
int main(){
	freopen("trifunc.in","w",stdout);
	srand(time(NULL));
	int x=rnd()+1,y=rnd()+1;
	for (;cipolla((1ll*x*x+1ll*y*y)%mo)==-1;x=rnd()+1,y=rnd()+1);
	int tmp=cipolla((1ll*x*x+1ll*y*y)%mo);
	printf("%d %d ",x*(rand()&1?1:-1),tmp);
	for (int i=1;i<=500000;i++)
		putchar('9');
}
