#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;
const int N=505,M=1e5+7;
int n,m,d,K,L,Q;

struct node {
	int x,y,id;
	int find(int pos) {
		int t=(y-pos)%(2*L-1);
		int xx=(x+t-1)%(2*L-1)+1;
		if (xx<=L) return xx;
		else return 2*L-xx+1;
	}
	bool operator <(node a) const {
		return y<a.y;
	}
}p[M<<1];
vector<node> v[N<<1];
int f[2][N],ans[M];
int main() {
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	scanf("%d%d%d",&n,&m,&d);L=n-d+1;
	scanf("%d",&K);
	for (int i=1,x,y;i<=K;i++) {
		char s[10];
		scanf("%d%d%s",&x,&y,s);
		if (s[0]=='-') x=2*L-x+1;
		p[i].x=x;p[i].y=y;
	}
	sort(p+1,p+K+1);
	scanf("%d",&Q);
	for (int i=1,x,y;i<=Q;i++) {
		scanf("%d%d",&x,&y);
		v[y%(2*L-1)].push_back((node){x,y,i});
	}
	for (int i=0;i<2*L-1;i++) if (!v[i].empty()){
		sort(v[i].begin(),v[i].end());
		int s=v[i].size()-1,ss=K;
		for (int k=1;k<=n;k++) f[(m+1)&1][k]=1;
		for (int j=m;j>=1;j--) {
			int l=1,r=n;
			if (ss && p[ss].y==j) l=p[ss].find(i),r=l+d-1,ss--;
			for (int k=1;k<=n;k++) f[j&1][k]=0;
			for (int k=l;k<=r;k++) {
				f[j&1][k]|=f[(j+1)&1][k+1]|f[(j+1)&1][k]|f[(j+1)&1][k-1];
			}
			while (~s && v[i][s].y==j) ans[v[i][s].id]=f[j&1][v[i][s].x],s--;
			if (s==-1) break;
		}
	}
	for (int i=1;i<=Q;i++) printf("%d\n",ans[i]);
}
