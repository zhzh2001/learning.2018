#include <cstdio>
#include <algorithm>
using namespace std;
double A,V,L,D,lim,ans=1e10;
bool check(double t) {
	double v=A*t,v1,t1,t2,t3,t4,d1,d2,d3,d4,d5;
	d1=A*t*t/2;
	if (v>lim) t2=(v-lim)/A;else t2=0;
	d3=(v+lim)/2*t2;
	if (d1+d3>D) return 0;
	else d2=D-d1-d3,t1=d2/v;
	v=min(v,lim);
	double l=L-D;
	t3=(V-v)/A;
	if (v*t3+A*t3*t3/2>l) v1=sqrt(2*A*l+v*v),t3=(v1-v)/A,t4=0;
	else t4=(l-v*t3-A*t3*t3/2)/V;
	ans=min(ans,t+t1+t2+t3+t4);
	return 1;
}
int main() {
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	scanf("%lf%lf%lf%lf%lf",&A,&V,&L,&D,&lim);
	double l=0,r=V/A;
	while (r-l>1e-7) {
		double mid=(l+r)/2;
		if (check(mid)) l=mid;
		else r=mid;
	}
	printf("%.10lf\n",ans);
}
