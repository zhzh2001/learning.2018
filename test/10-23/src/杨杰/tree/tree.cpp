#include <cstdio>
#include <algorithm>
using namespace std;
const int N=250007;
#define ll long long
struct edge {
	int t,nxt,a,b;
}e[N<<1];
int n,m,cnt,ans1;
int head[N];
ll f[N],ans=1e17,now;
void add(int u,int t,int a,int b) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;e[cnt].a=a;e[cnt].b=b;
}
void dfs(int u,int fa,int id) {
	f[u]=0;
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==fa) continue;
		dfs(t,u,id);
		now=max(now,f[u]+f[t]+e[i].a+e[i].b*id);
		f[u]=max(f[u],f[t]+e[i].a+e[i].b*id);
	}
}
int main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,u,v,a,b;i<n;i++) scanf("%d%d%d%d",&u,&v,&a,&b),add(u,v,a,b),add(v,u,a,b);
	for (int i=1;i<=m;i++) {
		now=0;
		dfs(1,0,i);
		if (now<ans) ans=now,ans1=i;
	}
	printf("%d %lld\n",ans1,ans);
}
