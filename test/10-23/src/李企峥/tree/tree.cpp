#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
using namespace std;
inline ll read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
const int N = 50011; 
struct D{
	int to,pre,val,del; 
}e[100100];
struct E{
	int x,y,a,b; 
}a[100100];
int head[50100]; 
ll dep[50100],ans; 
int n,lim,cnt,jdb;  
 void add(int x,int y,int v,int del) 
 {
	e[++cnt].to=y; 
	e[cnt].val=v; 
	e[cnt].pre=head[x]; 
	e[cnt].del=del; 
	head[x]=cnt; 
}
void dfs(int u,int fa) 
{
	for(int i=head[u];i;i=e[i].pre) 
	{
		int v=e[i].to; 
		if(v!=fa) 
		{
			dep[v]=dep[u]+e[i].val; 
			dfs(v,u); 
		}
	}
}
ll calc() 
{
	dep[0]=1e18;dep[0]=-dep[0]; 
	int S=0,T=0;
	For(i,1,n)dep[i]=0; 
	dfs(1,-1); 
	For(i,1,n)if(dep[S]<dep[i]) S=i; 
	For(i,1,n) dep[i] = 0; 
	dfs(S,-1); 
	For(i,1,n)if(dep[T]<dep[i]) T=i; 
	return dep[T]; 
}
int main() 
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(); 
	lim=read(); 
	ans=1e18; 
	For(i,1,n-1) 
	{
		a[i].x=read(); 
		a[i].y=read(); 
		a[i].a=read(); 
		a[i].b=read(); 
		add(a[i].x,a[i].y,a[i].a,a[i].b); 
		add(a[i].y,a[i].x,a[i].a,a[i].b); 
	} 
	ll tmp=calc(); 
	if(tmp<ans)ans=tmp,jdb=0; 
	For(i,1,lim) 
	{
		For(j,1,cnt)e[j].val+=e[j].del; 
		ll tmp=calc(); 
		if(tmp<ans)ans=tmp,jdb=i; 
	}
	cout<<jdb<<endl<<ans;
	return 0;
}


