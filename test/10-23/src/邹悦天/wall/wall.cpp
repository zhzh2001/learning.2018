#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <queue>
#include <set>
using namespace std;

namespace zyt
{
	const int N = 510;
	struct point
	{
		int x, y;	
	};
	int n, m, d, k;
	const int UP = 0, DOWN = 1;
	struct door
	{
		int x, y;
		bool dir;
		bool operator < (const door &b) const
		{
			return y < b.y;	
		}
	};
	set<door> sd;
	inline int cal(int t, const int x, bool dir)
	{
		int ans;
		t %= (n - d) * 2;
		if (dir == DOWN)
		{
			if (x + t <= n - d + 1)
				ans = x + t;
			else
				return cal(t - (n - d + 1 - x), n - d + 1, UP);
		}
		else
		{
			if (x - t >= 1)
				ans = x - t;
			else
				return cal(t - (x - 1), 1, DOWN);
		}
		return ans;
	}
	const int dx[] = {-1, 0, 1};
	bool judge(const point &p, const int t)
	{
		if (p.x < 1 || p.x > n || p.y < 1 || p.y > m + 1)
			return false;
		set<door>::iterator it = sd.find((door){0, p.y, 0});
		if (it == sd.end())
			return true;
		else
		{
			int x = cal(t, it->x, it->dir);
			return p.x >= x && p.x < x + d;
		}
	}
	bool bfs(const point &s)
	{
		queue<point> q;
		static int vis[N];
		q.push(s);
		for (int i = 1; i <= n; i++)
			vis[i] = s.x;
		while (!q.empty())
		{
			point u = q.front();
			q.pop();
			for (int i = 0; i < 3; i++)
			{
				point v = (point){u.x + dx[i], u.y + 1};
				if (judge(v, v.y - s.y))
				{
					if (v.y == m + 1)
						return true;
					else if (vis[v.x] != v.y)
						q.push(v), vis[v.x] = v.y;
				}
			}
		}
		return false;
	}
	void readchar(char &c)
	{
		do
			c = cin.get();
		while (c != '+' && c != '-');	
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin.tie(0);
		cin >> n >> m >> d >> k;
		for (int i = 0; i < k; i++)
		{
			int x, y;
			char c;
			cin >> x >> y;
			readchar(c);
			if (c == '+')
				sd.insert((door){x, y, DOWN});
			else
				sd.insert((door){x, y, UP});
		}
		int q;
		cin >> q;
		while (q--)
		{
			int x, y;
			cin >> x >> y;
			cout << (bfs((point){x, y}) ? 1 : 0) << '\n';
			cout.flush();
		}
		return 0;
	}
}
int main()
{
	freopen("wall.in", "r", stdin);
	freopen("wall.out", "w", stdout);
	return zyt::work();	
}
