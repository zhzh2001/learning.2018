#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cstring>
#include <cmath>
#include <iomanip>
using namespace std;

namespace zuiyt
{
	double a, vm, L, d, limit;
	inline double sq(const double x)
	{
		return x * x;	
	}
	inline double Xspeed_on_Ts(const double v0, const double T, const double a)
	{
		return v0 * T + a * sq(T) / 2;
	}
	inline double Xspeed_to_v(const double v0, const double v, const double a)
	{
		return Xspeed_on_Ts(v0, (v - v0) / a, a);	
	}
	inline double Tspeed_on_X(const double v0, const double X, const double a)
	{
		if (a > 0)
			return (-v0 + sqrt(sq(v0) + 2 * X * a)) / a;
		else
			return (-v0 - sqrt(sq(v0) + 2 * X * a)) / a;
	}
	inline double Vspeed_on_X(const double v0, const double X, const double a)
	{
		return v0 + Tspeed_on_X(v0, X, a) / a;
	}
	inline double Tspeed_to_v(const double v0, const double v, const double a)
	{
		return (v - v0) / a;	
	}
	pair<double, double> solve1()//Time and Speed when going through the barricade
	{
		double vmax = min(sqrt((sq(limit) + 2 * a * d) / 2.0), vm);
		//cannot speed up to limit
		if (Xspeed_to_v(0, limit, a) > d)
			return make_pair(Tspeed_on_X(0, d, a), Vspeed_on_X(0, d, a));
		else
		{
			double X1 = Xspeed_to_v(0, vmax, a), X2 = Xspeed_to_v(vmax, limit, -a);
			return make_pair(Tspeed_to_v(0, vmax, a) + (d - X1 - X2) / vmax + Tspeed_to_v(vmax, limit, -a), limit);
		}
	}
	double solve2(const double v0)
	{
		if (Xspeed_to_v(v0, vm, a) > L - d)
			return Tspeed_on_X(v0, L - d, a);
		else
			return Tspeed_to_v(v0, vm, a) + (L - d - Xspeed_to_v(v0, vm, a)) / vm;	
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin.tie(0);
		cin >> a >> vm >> L >> d >> limit;
		pair<double, double> ans = solve1();
		cout << fixed << setprecision(8) << ans.first + solve2(ans.second);
		return 0;
	}	
}
int main()
{
	freopen("expedition.in", "r", stdin);
	freopen("expedition.out", "w", stdout);
	return zuiyt::work();	
}
