#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
using namespace std;

namespace zuiyt
{
	typedef long long ll;
	const int N = 2.5e5 + 10;
	int n, limit;
	struct edge
	{
		int to, a, b, next;	
	}e[N << 1];
	int head[N], cnt;
	inline void add(const int a, const int b, const int c, const int d)
	{
		e[cnt] = (edge){b, c, d, head[a]}, head[a] = cnt++;
	}
	namespace Chain
	{
		void work()
		{
			ll ans = 0, sum = 0, id;
			for (int i = 0; i < cnt; i += 2)
				ans += e[i].a, sum += e[i].b;
			if (sum > 0)
				cout << "0\n" << ans;
			else
				cout << limit << '\n' << ans + sum * limit;
		}
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin.tie(0);
		cin >> n >> limit;
		bool flag_chain = true, flag_flower = true;
		memset(head, -1, sizeof(int[n + 1]));
		for (int i = 1; i < n; i++)
		{
			int x, y, a, b;
			cin >> x >> y >> a >> b;
			if (x != i || y != i + 1)
				flag_chain = false;
			if (x != 1 || y != i + 1)
				flag_flower = false;
			add(x, y, a, b);
			add(y, x, a, b);
		}
		//if (flag_chain)
			Chain::work();
		return 0;
	}	
}
int main()
{
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	return zuiyt::work();	
}
