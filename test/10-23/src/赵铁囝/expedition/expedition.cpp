#include <bits/stdc++.h>

#define eps 1e-12

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int a,v,L,d,limit;
double t,ans,l,r,mid;

inline void write(double x){
	printf("%.10lf\n",x);
}

inline bool check(double x){
	double now=1.0*x*a,sum=0.5*a*x*x,last,aa,bb,t;
	last=d-sum;
//	write(x);write(now);write(sum);write(last);
	if(now>v)return false;
	if(last<0)return false;
	if(now<limit){
		ans=x+last/now;
		return true;
	}
	aa=0.5*(now*now-limit*limit)/last;
	aa=abs(aa);
	t=(now-limit)/a;
	bb=t*now-0.5*a*t*t;
//	write(aa);write(t);write(bb);
	if(aa<a){
		ans=x+(last-bb)/now+t;
		return true;
	}
	return false;
}

inline double calc(int x,double now_v){
	double sum=0,t=0,now;
	t=(v-now_v)/a;
//	write(t);
	now=now_v*t+0.5*a*t*t;
//	write(now);
	if(now<L){
//		write(L-now);
		sum=t+(L-now)/v;
	}else{
		l=0;r=1e9;
		while(l+eps<r){
			mid=(l+r)/2.0;
			if(now_v*mid+0.5*a*mid*mid<L){
				l=mid;
			}else{
				r=mid;
			}
		}
		sum=l;
	}
	return sum;
}

int main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a=read();v=read();L=read();d=read();limit=read();
	l=0;r=1e9;
	while(l+eps<r){
		mid=(l+r)/2.0;
		if(check(mid)){
			l=mid;
		}else{
			r=mid;
		}
//		cout<<endl;
	}
	L-=d;
	ans=ans+calc(l-d,min(1.0*v,min(1.0*limit,ans*a)));
	write(ans);
	return 0;
}

