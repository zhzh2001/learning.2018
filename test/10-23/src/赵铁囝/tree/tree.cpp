#include <bits/stdc++.h>

#define Max 50005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Edge{
	int v,to,a,b;
}e[Max*2];

int n,m,l,r,a,b,now,ans,ans_max,size,dep[Max],head[Max];

inline void add(int u,int v,int a,int b){
	e[++size].v=v;e[size].to=head[u];e[size].a=a;e[size].b=b;head[u]=size;
}

inline void dfs(int u,int fa,int x){
	for(int i=head[u];i;i=e[i].to){
		int v=e[i].v;
		if(v==fa)continue;
		dep[v]=dep[u]+e[i].a+e[i].b*x;
		dfs(v,u,x);
	}
	return;
}

inline int calc(int x){
	int node=1;
	dep[1]=0;
	dfs(1,1,x);
	for(int i=1;i<=n;i++){
		if(dep[i]>dep[node]){
			node=i;
		}
	}
	dep[node]=0;
	dfs(node,node,x);
	for(int i=1;i<=n;i++){
		if(dep[i]>dep[node]){
			node=i;
		}
	}
	return dep[node];
}

int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<n;i++){
		l=read();r=read();
		a=read();b=read();
		add(l,r,a,b);
		add(r,l,a,b);
	}
	ans_max=1e9;
	for(int i=0;i<=m;i++){
		now=calc(i);
		if(now<ans_max){
			ans_max=now;
			ans=i;
		}
	}
	writeln(ans);
	writeln(ans_max);
	return 0;
}

