#include <bits/stdc++.h>

#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Door{
	int x,y;
	bool flag;
	char dir;
	inline void init(){
		x=read();
		y=read();
		scanf("%s",&dir);
		if(dir=='-'){
			flag=false;
		}else{
			flag=true;
		}
	}
	inline bool operator<(const Door&a)const{
		return y<a.y;
	}
}a[Max];

int n,m,d,k,q,x,y,L,R,Mid;
short int b[2][1005][1005],l[1005][Max],r[1005][Max];
bool flag[2][1005][1005];

inline bool check(int x,int y){
	int now,sum,wzp=(n-d)*2;
	L=1;R=k;
	while(L<=R){
		Mid=(L+R)>>1;
		if(a[Mid].y>=y){
			now=Mid;
			R=Mid-1;
		}else{
			L=Mid+1;
		}
	}
	sum=a[now].y-y;
	L=max(1,x-sum);
	R=min(n,x+sum);
	sum=a[k].y-y;
	sum%=wzp;
	if(l[sum][now]>r[sum][now])return false;
//	cout<<L<<" "<<R<<" "<<l[sum][now]<<" "<<r[sum][now]<<endl;
	if((R>=l[sum][now]&&R<=r[sum][now])||(L>=l[sum][now]&&L<=r[sum][now])||(L<=l[sum][now]&&R>=r[sum][now])||(L>=l[sum][now]&&R<=r[sum][now])){
		return true;
	}else{
		return false;
	}
}

inline int calc(int id,int now){
	int wzp=(n-d)*2;
	now%=wzp;
//	cout<<a[id].x<<" "<<now<<endl;
	return b[a[id].flag][a[id].x][now];
}

int main(){
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	n=read();m=read();d=read();
	k=read();
	for(int i=1;i<=k;i++){
		a[i].init();
	}
	sort(a+1,a+k+1);
	for(int i=1;i<(n-d)*2;i++){
		b[0][i][0]=i;
		b[1][i][0]=i;
		if(i==1||i+d-1==n){
			flag[0][i][0]=1;
			flag[1][i][0]=0;
		}else{
			flag[0][i][0]=0;
			flag[1][i][0]=1;
		}
		for(int j=1;j<(n-d)*2;j++){
			if(flag[0][i][j-1]){
				b[0][i][j]=b[0][i][j-1]+1;
			}else{
				b[0][i][j]=b[0][i][j-1]-1;
			}
			if(b[0][i][j]==1||b[0][i][j]+d-1==n){
				flag[0][i][j]=flag[0][i][j-1]^1;
			}else{
				flag[0][i][j]=flag[0][i][j-1];
			}
			if(flag[1][i][j-1]){
				b[1][i][j]=b[1][i][j-1]+1;
			}else{
				b[1][i][j]=b[1][i][j-1]-1;
			}
			if(b[1][i][j]==1||b[1][i][j]+d-1==n){
				flag[1][i][j]=flag[1][i][j-1]^1;
			}else{
				flag[1][i][j]=flag[1][i][j-1];
			}
		}
	}
	int wzp=(n-d)*2;
	for(int i=0;i<wzp;i++){
		L=calc(k,i);
		R=L+d-1;
		l[i][k]=L;
		r[i][k]=R;
		int sum=i,now=0;
		for(int j=k-1;j>=1;j--){
			now=a[j+1].y-a[j].y;
			sum=((sum-now)%wzp+wzp)%wzp;
			L=calc(j,sum);
			R=L+d-1;
//			cout<<L<<" "<<R<<endl;
			l[i][j]=max(L,max(1,l[i][j+1]-now));
			r[i][j]=min(R,min(n,r[i][j+1]+now));
			if(l[i][j]>r[i][j]){
				for(int p=1;p<=j;p++){
					l[i][p]=2;
					r[i][p]=1;
					goto loop;
				}
			}
//			cout<<i<<" "<<j<<" "<<l[i][j]<<" "<<r[i][j]<<endl;
		}
		loop:;
	}
//	cout<<l[4][1]<<" "<<r[4][1]<<endl;
	q=read();
	while(q--){
		x=read();y=read();
		writeln(check(x,y));
	}
	return 0;
}

