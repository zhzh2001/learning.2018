#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=250002;
struct node{
	int to,ne,a,b;
}e[N<<1];
int ans,lim,n,i,x,y,a,b,tot,h[N],k,t,tmp;
ll mn,len,dis[N];
bool vis[N];
queue<int>q;
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
void add(int x,int y,int a,int b){
	e[++tot]=(node){y,h[x],a,b};
	h[x]=tot;
}
void bfs(int u){
	memset(vis,0,n+1);
	q.push(u);dis[u]=0;vis[u]=1;
	while (!q.empty()){
		u=q.front();q.pop();
		for (int i=h[u],v;i;i=e[i].ne)
			if (!vis[v=e[i].to]){
				vis[v]=1;
				dis[v]=dis[u]+e[i].a+t*e[i].b;
				q.push(v);
				if (dis[v]>len) len=dis[v],k=v;
			}
	}
}
void solve(int x){
	t=x;
	len=0;k=1;bfs(1);
	len=0;bfs(k);
	if (len<mn) mn=len,ans=x;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=rd();lim=rd();mn=1e17;
	for (i=1;i<n;i++) x=rd(),y=rd(),a=rd(),b=rd(),add(x,y,a,b),add(y,x,a,b);
	tmp=lim;
	lim=min(10000000/n,lim);
	for (i=0;i<lim;i++) solve(i);
	solve(tmp);
	printf("%d\n%lld",ans,mn);
}
