#include<bits/stdc++.h>
using namespace std;
double a,v,L,d,lim,t1,t2,T,S,t;
int main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	scanf("%lf%lf%lf%lf%lf",&a,&v,&L,&d,&lim);
	lim=min(lim,v);t=lim/a;
	if (0.5*a*t*t>d) T=sqrt(2*d/a);
	else{
		t1=sqrt(lim*lim/a/a/2+d/a);t2=t1-lim/a;
		if (t1*a>v){
			t1=v/a;t2=(v-lim)/a;
			T=t1+t2+(d-0.5*a*t1*t1-(v*t2-0.5*a*t2*t2))/v;
		}else T=t1+t2;
	}
	t=(v-lim)/a;L-=d;
	if (lim*t+0.5*a*t*t<=L) S=t+(L-(lim*t+0.5*a*t*t))/v;
	else S=(-lim+sqrt(lim*lim+2*a*L))/a;
	printf("%.9lf",T+S);
}
