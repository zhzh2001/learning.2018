#include<set>
#include<map>
#include<cstdio>
#include<cctype>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
#define fy 20021218020021218
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define N 100010
using namespace std;
LL n,m,i,j,k,l,r,h,x,y,s,p;
struct node
{
	LL a,b,v,d;
}a[N];
char c[N][10];
void get_num(LL k,LL l,LL r,LL d,LL &x,LL &y)
{
	k%=2*n;
	if(d==1)
	{
		if(l>k){x=l-k; y=r-k; return;}
		else{k-=l-1; r-=l-1; l=1; d=2;}
	}
	if(d==2)
	{
		if(n-r>=k){x=l+k; y=r+k; return;}
		else{k-=n-r; l+=n-r; r=n; d=1;}
	}
	if(d==1)
	{
		if(l>k){x=l-k; y=r-k; return;}
		else{k-=l-1; r-=l-1; l=1; d=2;}
	}
	if(d==2)
	{
		if(n-r>=k){x=l+k; y=r+k; return;}
		else{k-=n-r; l+=n-r; r=n; d=1;}
	}
}
LL max(LL a,LL b){if(a<b) return b; return a;}
LL min(LL a,LL b){if(a<b) return a; return b;}
void check(LL k,LL &l,LL &r,LL x,LL y)
{
	int ansl=max(l-k,1*1ll),ansr=min(r+k,n);
	if(ansl>y||ansr<x) {l=-1; r=-1; return;}
	l=max(ansl,x); r=min(ansr,y);
}
bool cmp(node x,node y){return x.v<y.v;}
int main()
{
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	scanf("%lld%lld%lld",&n,&m,&k);
	scanf("%lld",&m);
	rep(i,1,m)
	{
		scanf("%lld%lld",&a[i].a,&a[i].v); scanf("%s",c[i]+1);
		if(c[i][1]=='+') a[i].d=2; else a[i].d=1; a[i].b=a[i].a+k-1;
	}
	sort(a+1,a+m+1,cmp);
	scanf("%lld",&k);
	rep(i,1,k)
	{
		scanf("%lld%lld",&l,&h); r=l;
		s=1; p=h;
		while(a[s].v<=h) s++;
		rep(j,s,m)
		{
			get_num(a[j].v-h,a[j].a,a[j].b,a[j].d,x,y);
			check(a[j].v-p,l,r,x,y);
			if(l==-1&&r==-1) break;
			p=a[j].v;
		}
		if(l==-1&&r==-1) printf("0\n");
		else printf("1\n");
	}
	return 0;
}
