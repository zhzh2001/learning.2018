#include<set>
#include<map>
#include<cmath>
#include<cstdio>
#include<cctype>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
#define fy 20021218020021218
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define N 100010
using namespace std;
long double a,Vm,L,d,limit,Tm,T,S,ans,V;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int main()
{
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a=read(); Vm=read(); L=read(); d=read(); limit=read();
	Tm=Vm/a; T=sqrt(2*d/a);
	if(Vm<=limit||T>=Tm&&T*a<=limit)
	{
		S=Vm*Tm/2;
		if(S>=L) ans=sqrt(2*L/a);
		else ans=Tm+(L-S)/Vm;
		printf("%.15lf",(double)ans);
	}
	else
	{
		S=Vm*Tm/2+(Vm+limit)*(Vm-limit)/a/2;
		if(S<=d) ans=(Vm-limit)/a+Tm+(d-S)/Vm;
		else {ans=sqrt((d+limit*limit/a/2)/a); ans+=(ans*a-limit)/a;}
		V=sqrt(2*(L-d)*a+limit*limit);
		if(V>=Vm) ans+=(Vm-limit)/a+((L-d)-(Vm+limit)*(Vm-limit)/a/2)/Vm;
		else ans+=(sqrt(limit*limit+2*a*(L-d))-limit)/a;
		printf("%.15lf",(double)ans);
	}
	return 0;
}




