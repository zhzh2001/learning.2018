#include <iostream>
#include <cstdio>
#include <queue>
#include <cstring>
#include <ctime>
#include <cstdlib>
#include <cmath>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=3e5+10;
ll head[N],n,lim,edge,D[N],l,r,ans;
bool flag=1;
bool vis[N];
struct edge{ll to,net,a,b;}e[N<<1];
inline void addedge(){
	ll x,y,a,b;
	read(x),read(y),read(a),read(b);
	if (x+1!=y) flag=0;
	e[++edge].to=y,e[edge].net=head[x],head[x]=edge,e[edge].a=a,e[edge].b=b;
	e[++edge].to=x,e[edge].net=head[y],head[y]=edge,e[edge].a=a,e[edge].b=b;
}
inline ll far(ll k,ll t){
	queue<ll>Q;
	Q.push(1);
	memset(vis,0,sizeof vis);
	D[1]=0;
	vis[1]=1;
	while (!Q.empty()){
		ll x=Q.front();Q.pop();
		for (ll i=head[x];i;i=e[i].net)
			if (!vis[e[i].to]) vis[e[i].to]=1,D[e[i].to]=D[x]+e[i].a+e[i].b*k,Q.push(e[i].to);
	}
	t=1;
	for (ll i=2;i<=n;++i) if (D[t]<D[i]) t=i;
	Q.push(t);
	memset(vis,0,sizeof vis);
	D[t]=0;
	vis[t]=1;
	while (!Q.empty()){
		ll x=Q.front();Q.pop();
		for (ll i=head[x];i;i=e[i].net)
			if (!vis[e[i].to]) vis[e[i].to]=1,D[e[i].to]=D[x]+e[i].a+e[i].b*k,Q.push(e[i].to);
	}
	t=1;
	for (ll i=2;i<=n;++i) if (D[t]<D[i]) t=i;
	return D[t];
}
inline ll query(ll k){
	queue<ll>Q;
	Q.push(1);
	memset(vis,0,sizeof vis);
	D[1]=0;
	vis[1]=1;
	while (!Q.empty()){
		ll x=Q.front();Q.pop();
		for (ll i=head[x];i;i=e[i].net)
			if (!vis[e[i].to]) vis[e[i].to]=1,D[e[i].to]=D[x]+e[i].a+e[i].b*k,Q.push(e[i].to);
	}
	for (ll i=1;i<=n;++i) D[i]+=D[i-1];
	ll Min=D[1],ret=0;
	for (ll i=1;i<=n;++i){
		Min=min(Min,D[i]);
		ret=max(ret,D[i]-Min);
	}
	for (ll i=n;i;--i) D[i]-=D[i-1];
	for (ll i=n;i;--i) D[i]+=D[i+1];
	Min=D[n];
	for (ll i=n;i;--i){
		Min=min(Min,D[i]);
		ret=max(ret,D[i]-Min);
	}
	return ret;
}
inline ll check(ll x){
	if (flag) return query(x);
	ll Max=0;
	for (ll i=0;i<5;++i) Max=max(Max,far(x,rand()%n+1));
	return Max;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	srand((ll)time(NULL));
	read(n),read(lim);
	for (ll i=1;i<n;++i) addedge();
	l=0,r=lim;
	for (;l<=r;){
		ll mid2=(l+r)/2;
		ll mid1=(l+mid2)/2;
		if (check(mid1)<=check(mid2)) ans=mid1,r=mid2-1;
		else ans=mid2,l=mid1+1;
	}
	while (ans<lim&&check(ans)>check(ans+1)) ++ans;
	wr(ans),puts("");
	wr(check(ans));
	return 0;
}
//sxdakking
