#include <iostream>
#include <cstdio>
#include <cstring>
#include <bitset>
#include <algorithm>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10;
inline ll gg(){char c=gc();for(;c!='-'&&c!='+';c=gc());return c=='+'?1:-1;}
ll n,m,k,d,q;
bitset<1010>f,tmp;
struct node{
	ll x,y,f;
	inline void init(){read(x),read(y),f=gg();}
	bool operator <(const node&res)const{
		return y<res.y;
	}
}a[N];
inline ll work(ll now,ll time,ll flag){
	//���ڣ�(n-d)*2
	time%=(n-d)*2;
	while(1){
		if (flag<0){
			if (now>time) return now-time;
			else time-=now,now=1;
		}
		if (flag>0){
			if (n-(now+d-1)>time) return now+time;
			else time-=n-(now+d-1)+1,now=n;
		}
		flag=-flag;
	}
}
int main(){
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	read(n),read(m),read(d);
	for (ll i=0;i<d;++i) tmp[i]=1;
	read(k);
	for (ll i=1;i<=k;++i) a[i].init();
	sort(a+1,a+1+k);
	read(q);
	for (ll x,y,l,r;q;--q){
		read(x),read(y);
		for (ll i=1;i<=n;++i) f[i]=0;
		f[x]=1;
		l=r=x;
		for (ll i=1;i<=k;++i){
			if (a[i].y<y) continue;
			ll t=a[i].y-max(a[i-1].y,y);
			for (ll u=t;l>=1&&u;--l,--u) f[l-1]=1;
			for (ll u=t;r<=n&&u;++r,--u) f[r+1]=1;
			f&=tmp<<work(a[i].x,a[i].y-y+1,a[i].f);
			for (;l<=n&&!f[l];++l);
			for (;r>=1&&!f[r];--r);
			if (l>r) break;
		}
		bool flag=0;
		for (ll i=0;i<n;++i) flag|=f[i];
		wr(flag),puts("");
	}
	return 0;
}
//sxdakking
/*
6 10 2
2
3 8 -
4 3 +
6
4 2
3 2
1 1
1 7
3 8
5 6
*/
