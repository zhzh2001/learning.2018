#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;
typedef long long ll;
typedef double db;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
ll a,Vm,L,d,limit;
db Vd,Ved,t1,t2,t3;
int main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	read(a),read(Vm),read(L),read(d),read(limit);
	if (Vm<=limit||L<d){
		Ved=sqrt(2*a*L);
		if (Ved<=Vm) printf("%.10lf",sqrt(2*L/(db)a));
		else{
			t1=Vm/(db)a;
			t2=(L-0.5*a*t1*t1)/db(Vm);
			printf("%.10lf",t1+t2);
		}
		return 0;
	}
	Vd=sqrt(2*a*d);
	if (Vd<=limit){
		Ved=sqrt(2*a*L);
		if (Ved<=Vm){
			printf("%.10lf",sqrt(2*L/(db)a));
	/*
	t^2 = 2L / a
	*/
		}else{
			t1=Vm/(db)a;
			t2=(L-0.5*a*t1*t1)/db(Vm);
			printf("%.10lf",t1+t2);
	/*
	t1= Vm / a 
	t2= ( d - 1/2 * a * t1^2 ) / Vm
	*/
		}
	}else{
		if (Vd<=Vm){
			t1=sqrt(d/(db)a+limit*limit/db(2*a*a))*2;
			t1-=limit/(db)a;
		}else{
			t1=Vm/(db)a + (Vm-limit)/(db)a + (d-(2*Vm*Vm-limit*limit)/(db)(2*a))/(db)Vm;
		}
		Ved=limit+sqrt(2*a*(L-d));
		
		if (Ved<=Vm){
			t2=(sqrt(2*a*(L-d)+limit*limit)-limit)/(db)a;
			printf("%.10lf",t1+t2);
	/*
	
	*/
		}else{
			t2=(Vm-limit)/(db)a;
			t3=(L-d-0.5*a*t2*t2-limit*t2)/(db)Vm;
			printf("%.10lf",t1+t2+t3);
		}
	}
	return 0;
}
//sxdakking
/*

*/
