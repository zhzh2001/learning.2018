#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 510
#define maxk 100010
#define maxq 100010
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
struct arr
{
	int x,y,pt;
	friend bool operator < (arr a,arr b)
	{
		return a.y>b.y;
	}
};
arr dor[maxk];
int n,m,d,k,q,f[maxk][2],lll,ans[maxq];
struct node
{
	int x,y,cnt,ans;
	friend bool operator < (node a,node b)
	{
		return ((dor[1].y-a.y-1)%(n-d<<1)+(n-d<<1))%(n-d<<1)+1>((dor[1].y-b.y-1)%(n-d<<1)+(n-d<<1))%(n-d<<1)+1;
	}
};
char c;
node qe[maxq];
int find(int x)
{
	int l=1,r=k+1;
	for(;l<r-1;)
	{
		int m=l+r>>1;
		if(dor[m].y>=x) l=m;
			else r=m;
	}
	return l;
}
void chuli(int k)
{
	for(;((dor[1].y-qe[lll].y-1)%(n-d<<1)+(n-d<<1))%(n-d<<1)+1+k==n-d<<1&&lll<=q;lll++)
	{
		int kkk=find(qe[lll].y);
		if(qe[lll].x>=f[kkk][0]-dor[kkk].y+qe[lll].y&&qe[lll].x<=f[kkk][1]+dor[kkk].y-qe[lll].y) qe[lll].ans=1;
			else qe[lll].ans=0;
	}
}
signed main()
{
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	n=read();
	m=read();
	d=read();
	k=read();
	for(int i=1;i<=k;i++)
	{
		dor[i].x=read();
		dor[i].y=read();
		for(c=getchar();c!='+'&&c!='-';c=getchar());
		dor[i].pt=(c=='+');
	}
	sort(dor+1,dor+k+1);
	dor[n].y=0;
	dor[0].y=m;
	q=read();
	for(int i=1;i<=q;i++)
	{
		qe[i].x=read();
		qe[i].y=read();
		qe[i].cnt=i;
	}
	if(d>=n-1)
	{
		for(int i=1;i<=q;i++)
			wln(1);
		return 0;
	}
	sort(qe+1,qe+q+1);
	lll=1;
	for(int i=0;i<n-d<<1;i++)
	{
		f[0][0]=1;
		f[0][1]=n;
		int now=i,flag=0;
		for(int j=1;j<=k;j++)
		{
			if(flag) continue;
			int l=dor[j].x,r=l+d-1;
			if(dor[j].pt==0)
			{
				if(n-r>=now)
				{
					l+=now;
					r+=now;
				}
				if(n-r<now&&n-r+n-d>=now)
				{
					l=n*2-d+1-now-r;
					r=n*2-now-r;
				}
				if(n-r+n-d<now)
				{
					l=1+now-2*n+r+d;
					r=d*2+now-2*n+r;
				}
			}
			if(dor[j].pt==1)
			{
				if(l>now)
				{
					l-=now;
					r-=now;
				}
				if(l<=now&&now-l+1<=n-d)
				{
					r=d+now-l+1;
					l=2+now-l;
				}
				if(now-l+1>n-d)
				{
					r=n*2-d-now+l-1;
					l=n*2-d*2-now+l;
				}
			}
			l=max(l,f[j-1][0]-dor[j-1].y+dor[j].y);
			r=min(r,f[j-1][1]+dor[j-1].y-dor[j].y);
			f[j][0]=l;
			f[j][1]=r;
			if(l>r) flag=1;
			(now+=dor[j].y-dor[j+1].y)%=n-d<<1;
		}
		chuli(i);
	}
	for(int i=1;i<=q;i++)
		ans[qe[i].cnt]=qe[i].ans;
	for(int i=1;i<=q;i++)
		wln(ans[i]);
	return 0;
}
