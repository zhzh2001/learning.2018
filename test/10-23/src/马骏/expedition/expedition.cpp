#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
// #define maxn
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
long double a,vm,l,d,limit,v,t;
void solve1()
{
	if(limit>=vm||sqrt(2*a*d)<=limit)
	{
		v=sqrt(2*a*d);
		if(v>vm)
		{
			t=vm/a+(d-vm/2/a)/vm;
			v=vm;
		}
		else t=v/a;
		return;
	}
	long double k=(2*vm*vm-limit*limit)/2/a;
	if(k<=d) t=(d-k)/vm+(vm*2-limit)/a;
		else t=limit/a+(sqrt(4*limit*limit+4*a*(d-limit*limit/2/a))-2*limit)/a;
	v=limit;
}
void solve2()
{
	if((vm*vm-v*v)/2/a<l-d)
	{
		long double k=(vm*vm-v*v)/2/a;
		t+=(vm-v)/a+(l-d-k)/vm;
	}
	else t+=(sqrt(v*v+2*a*(l-d))-v)/a;
}
signed main()
{
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a=read();
	vm=read();
	l=read();
	d=read();
	limit=read();
	solve1();
	// printf("%.10lf %.10lf\n",(double)t,(double)v);
	solve2();
	printf("%.10lf",(double)t);
	return 0;
}
