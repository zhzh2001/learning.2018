#include<cstdio>
#include<cstring>
#include<string>
#include<iostream>
#include<algorithm>
#define rep(x,a,b) for(int x=(int)a;x<=(int)b;++x)
#define drp(x,a,b) for(int x=(int)a;x>=(int)b;--x)
#define LL long long
#define inf 0x3f3f3f3f
using namespace std;
int n,m,d,k,x,y,T;
char ch;
struct node{
	int x,y,flag;
}a[100010];
bool cmp(node a,node b){
	return a.y<b.y;
}
bool check(int l,int r,int st,int en){
	if (l<=r){
		if (st<=en){
			if (l>en||r<st) return 1;
			}else
			{
				if (l>en&&r<st) return 1;
			}	
	}else{
		if (st<=en){
			if (st>l&&en<r) return 1;
		}else{
			return 0;
		}
	}
	return 0;
}
int main(){
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	scanf("%d%d%d",&n,&m,&d);
	scanf("%d",&k);
	rep(i,1,k){
		scanf("%d%d%c%c",&x,&y,&ch,&ch);
		a[i].x=x;
		a[i].y=y;
		if (ch=='-') a[i].flag=-1;else a[i].flag=1;
	}
	scanf("%d",&T);
	sort(a+1,a+k+1,cmp);

	while (T--){
		scanf("%d%d",&x,&y);
		int l=x,r=x;
		a[0].y=y;
		bool Fl=0;
		int cnt=0;
		rep(i,1,k){
			if (a[i].y<=y) continue;
			int Del;
			cnt++;
			if (cnt!=1) Del=a[i].y-a[i-1].y;
				else Del=a[i].y-y;
	//		printf("%d\n",Del);
			l=(l-Del);
			r=(r+Del);
	//		printf("%d %d\n",l,r);
			if (l<1)l=1;
			if (r>n)r=n;
			int st,en;
			st=a[i].x;en=a[i].x+k-1;
			st=st+(a[i].y-y)*a[i].flag;
			st=(st+n*(a[i].y-y))%n;
			if (st==0) st=n;
			en=en+(a[i].y-y)*a[i].flag;
			en=(en+n*(a[i].y-y))%n;	
			if (en==0) en=n;
	//		printf("%d %d\n",st,en);	
			if (check(l,r,st,en)) {
				printf("0\n");
				Fl=1;
				break;
			}	
			l=st;r=en;
		}
		if (!Fl) printf("1\n");
	}
	return 0;
}

