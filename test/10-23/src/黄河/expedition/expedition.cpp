#include<cstdio>
#include<cstring>
#include<string>
#include<iostream>
#include<algorithm>
#define rep(x,a,b) for(register int x=(int)a;x<=(int)b;++x)
#define drp(x,a,b) for(register int x=(int)a;x>=(int)b;--x)
#define LL long long
#define inf 0x3f3f3f3f
double a,vm,m,d,Lim;
double l,r,Mid,t1,t2,v1,x1,x2,t,v;
bool Check(double t1){
	double v1=a*t1,x1=a*t1*t1/2;
	if (v1<=Lim) return true;
	double x2=(Lim*Lim-v1*v1)/(-2*a);
	return x1+x2<=d;
}
int main(){
	freopen("expedition.in", "r", stdin);
	freopen("expedition.out", "w", stdout);
	scanf("%lf%lf%lf%lf%lf",&a,&vm,&m,&d,&Lim);
	l=0,r=vm/a;
	rep(i,1,100)
		if (Check(Mid=(l+r)/2)) t1=l=Mid; else r=Mid;
	v1=a*t1,x1=a*t1*t1/2;
	if (v1<=Lim)v=v1,t=t1+(d-x1)/v1;
	else v=Lim,x2=(v1*v1-v*v)/(2*a),t2=(v1-v)/a,t=t1+t2+(d-x1-x2)/v1;
	m-=d;
	v1=sqrt(2*a*m+v*v);
	if (v1<=vm)t+=(v1-v)/a;
	else t1=(vm-v)/a,x1=v*t1+a*t1*t1/2,t+=t1+(m-x1)/vm;
	printf("%.15lf",t);
}
