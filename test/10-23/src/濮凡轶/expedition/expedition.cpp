#include <cstdio>
#include <cmath>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	freopen("expedition.in", "r", stdin);
	freopen("expedition.out", "w", stdout);
	double a, vm, L, d, lim;
	scanf("%lf%lf%lf%lf%lf", &a, &vm, &L, &d, &lim);
	double ans = 0;
	double T_to_vm = vm / a;
	double D_to_vm = T_to_vm * (vm / 2.);
	double T_to_lim = lim / a;
	double D_to_lim = T_to_lim * (lim / 2.);
	if(vm > lim && D_to_lim < d)
	{
		// cout << "haha" << endl;
		double del_D_to_lim = d - D_to_lim;
		// double D_maxx = D_to_lim + del_D_to_lim / 2.;
		// cout << D_maxx << endl;
		double V_pj = (lim + sqrt(lim * lim + a * del_D_to_lim)) / 2.;
		ans += del_D_to_lim / V_pj;
		L -= del_D_to_lim;
	}
	// cout << ans << endl;
	cout << D_to_vm << endl;
	if(D_to_vm >= L)
		ans += sqrt(2. * L / a);
	else
	{
		ans += T_to_vm;
		L -= D_to_vm;
		ans += L / vm;
	}
	printf("%.12f", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

/*
3 6 10 4 4
3 6 4 4 4
*/
