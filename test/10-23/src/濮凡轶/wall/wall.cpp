// 为什么非得滚存 

#include <utility>
#include <fstream>
#include <iostream>
#include <algorithm>

using namespace std;

const int maxn = 505;
const int maxq = 100005;
const int maxk = 100005;
const int maxt = maxn << 1; // time

ifstream fin("wall.in");
ofstream fout("wall.out");

// #define fin cin
// #define fout cout

namespace zdaknoip
{
	int n, m, d, k, q, all_cntt, ms;
	
	struct qj
	{
		int l, r; // 包含 
		
		qj(int l, int r)
		{
			this->l = l;
			this->r = r;
		}
		
		qj() {  }
	};
	
	struct Door
	{
		int k; // 列 
		qj z; // 行的顶端和低端（可行） 
		bool first_up; // 开始时往上走 
		
		inline bool operator < (const Door& other) const
		{
			return this->k < other.k;
		}
	} door[maxk];
	
	struct Ask
	{
		int x, y, id; // x是行， y是列
		
		inline bool operator < (const Ask& other) const
		{
			return this->y < other.y;
		}
	} ask[maxq];
	
	inline qj jiao(qj a, qj b)
	{
		qj ans;
		ans.l = max(a.l, b.l);
		ans.r = min(a.r, b.r);
		return ans;
	}
	
	inline int hv_jiao(qj a, qj b)
	{
		qj tmp = jiao(a, b);
		if(tmp.l <= tmp.r)
			return 1;
		else
			return 0;
	}
	
	inline qj getqj(qj a, int tim, int up)
	{
		if(up)
		{
			a.l -= tim;
			a.r -= tim;
			while(a.l < 1 || a.r > n)
			{
				// puts("fafa");
				if(a.r > n)
				{
					int tmp = a.r - n;
					a.r = n;
					a.l = a.r - d + 1;
					a.l -= tmp;
					a.r -= tmp;
				}
				if(a.l <= 0)
				{
					int tmp = 1 - a.l;
					a.l = 1;
					a.r = a.l + d - 1;
					a.l += tmp;
					a.r += tmp;
				}
			}
		}
		else
		{
			a.l += tim;
			a.r += tim;
			while(a.l < 1 || a.r > n)
			{
				// cout << a.l << ' ' << a.r << endl;
				if(a.r > n)
				{
					int tmp = a.r - n;
					a.r = n;
					a.l = a.r - d + 1;
					a.l -= tmp;
					a.r -= tmp;
				}
				if(a.l <= 0)
				{
					int tmp = 1 - a.l;
					a.l = 1;
					a.r = a.l + d - 1;
					a.l += tmp;
					a.r += tmp;
				}
			}
		}
		return a;
	}
	
	inline qj zh_tim(Door a, int tim)
	{
		// tim++;
		tim %= ms;
		qj ans = a.z;
		return getqj(ans, tim, a.first_up);
	}
	
	qj dp[maxt], dpp[maxt];
	
	int ans[maxn];
	
	int main()
	{
		fin >> n >> m >> d;
		fin >> k;
		for(int i = 1; i <= k; ++i)
		{
			char c[3];
			fin >> door[i].z.l >> door[i].k >> c;
			door[i].z.r = door[i].z.l + d - 1;
			door[i].first_up = (c[0] == '-');
		}
		sort(door + 1, door + k + 1);
		fin >> q;
		for(int i = 1; i <= q; ++i)
		{
			fin >> ask[i].x >> ask[i].y;
			ask[i].id = i;
		}
		sort(ask + 1, ask + q + 1);
		/* int L, R, LL, RR;
		while(cin >> L >> R >> LL >> RR){
			qj ans = jiao(qj(L, R), qj(LL, RR));
			cout << ans.l << ' ' << ans.r << endl;
			cout << hv_jiao(qj(L, R), qj(LL, RR)) << endl;
		} */
		ms = (n - d) << 1;
		for(int idoor = k, iask = q; iask; )
		{
			// cout << idoor << ' ' << iask << ' ' << ask[iask].y << ' ' << door[idoor].k << endl;
			if(ask[iask].y > door[idoor].k)
			{
				if(idoor == k)
				{
					ans[ask[iask].id] = 1;
					iask--;
					continue;
				}
				qj can_get(ask[iask].x, ask[iask].x);
				int tim = door[idoor + 1].k - ask[iask].y;
				can_get.l -= tim;
				can_get.l = max(can_get.l, 1);
				can_get.r += tim;
				can_get.r = min(can_get.r, n);
				// cout << "233 " << can_get.l << ' ' << can_get.r << ' ' << ask[iask].id << endl;
				ans[ask[iask].id] = hv_jiao(can_get, dp[tim]);
				iask--;
			}
			else
			{
				if(idoor == k)
				{
					for(int i = 0; i < ms; ++i)
						dp[i] = zh_tim(door[idoor], i);
				}
				else
				{
					int tim = door[idoor + 1].k - door[idoor].k;
					for(int i = 0; i < ms; ++i)
					{
						qj can_do = zh_tim(door[idoor], i);
						can_do.l -= tim;
						can_do.l = max(1, can_do.l);
						can_do.r += tim;
						can_do.r = min(n, can_do.r);
						dpp[i] = jiao(dp[(i + tim) % ms], can_do);
					}
					for(int i = 0; i < ms; ++i)
						dp[i] = dpp[i];
				}
				idoor--;
			}
		}
		for(int i = 1; i <= q; ++i)
			fout << ans[i] << '\n';
		return 0;
	}
}

int main()
{
	zdaknoip::main();
	return 0;
}
