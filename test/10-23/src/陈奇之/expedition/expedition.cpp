#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
namespace IO{
	char c,st[66];int tp=0,f;
	#define gc getchar
	#define pc putchar
	inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
}using IO::read;using IO::write;using IO::writeln;using IO::wri;
#define rd read
namespace My{
void calc(double a,double b,double c,double &x){
	double delte = b * b - 4 * a * c;
	assert(delte > 0);
	x = (-b + sqrt(delte)) / (2 * a);
//	assert(x >= -1e-9);
}
double a,vm,L,d,limit,vmid,vlast,ans;
double reslen,x;
int main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a = rd(),vm = rd(),L = rd(),d = rd(),limit = rd();
	double t = sqrt(2.0 * d / a);//假设可以一直加速，到关口的时间，从而得到速度 
	ans = 0;
	vmid = a * t;
	if(vmid < limit && vmid < vm){
		ans += t;
	} else
	if(vm < vmid && vm < limit){
		double tmp = vm / a;//加速到最快需要的时间
		ans += tmp + (d - 0.5 * tmp * vm) / vm;
		vmid = vm;
	} else{
		double tmp = limit / a;
		ans += tmp;
		reslen = d - 0.5 * tmp * limit;
		if(vmid - vm > vm - limit){
			double tr = (vm - limit) / a;
			reslen -= tr * tr * a / 2 * 2 + tr * limit * 2;
			ans += reslen / vm;
			ans += 2 * tr;
		}//中间要到vm一段时间
		else {
			//假设时间2x
			//x * x * a / 2  * 2 + 2 * x * limit = reslen 
			calc(a,2*limit,-reslen,x);//解出x
			ans += 2 * x;
		}//到不了vm 
		vmid = limit;
	}
//	debug(vmid);
	//假设花费时间x到最终
	//x * vmid + x * x * a / 2 = (L - d)
	calc(a/2,vmid,-(L-d),x);
	vlast = vmid + x * a;
	if(vlast < vm){
		ans += x;
	} else{
		double tmpt = (vm - vmid) / a;
		ans += tmpt;
		reslen = L - d;
		reslen -= tmpt * tmpt * a / 2;
		reslen -= tmpt * vmid;
		ans += reslen / vm;
	}
	printf("%.15lf\n",ans);
	return 0;
}
}
int main(){
	My::main();
	return 0;
}
