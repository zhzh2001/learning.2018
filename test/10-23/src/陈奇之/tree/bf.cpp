#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
namespace IO{
	char c,st[66];int tp=0,f;
	#define gc getchar
	#define pc putchar
	inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
}using IO::read;using IO::write;using IO::writeln;using IO::wri;
#define rd read
const int maxn = 250005;
struct Edge{
	int to,nxt,a,b;
	ll dist;
	Edge(){}
	Edge(int to,int nxt,int a,int b):
		to(to),nxt(nxt),a(a),b(b){dist=0;}
}edge[maxn * 2];
int first[maxn],nume;
int n,lim;
void Addedge(int a,int b,int c,int d){
	edge[nume] = Edge(b,first[a],c,d);
	first[a] = nume++;
}
ll f[maxn],dep[maxn],ans;
void dfs(int u,int fa){
	ll mx=0,mx2=0;
	f[u] = 0;
	for(int e=first[u];~e;e=edge[e].nxt){
		int v=edge[e].to;
		if(v==fa) continue;
		dfs(v,u);
		f[v]+=edge[e].dist;
		if(f[v] > mx){
			mx2 = mx;
			mx = f[v];
		} else
		if(f[v] > mx2)
			mx2 = f[v];
		f[u]=max(f[u],f[v]);
	}
	ans = max(ans,mx+mx2);
}
ll calc(int limit){
	for(int i=0;i<nume;++i)
		edge[i] . dist =edge[i].a+1ll*edge[i].b * limit;
	ans = 0;
	dfs(1,0);
	return ans;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("bf.out","w",stdout);
	n = rd(),lim = rd();
	mem(first,-1);nume = 0;
	rep(i,1,n){
		int x = rd(),y = rd(),a = rd(),b = rd();
		Addedge(x,y,a,b);
		Addedge(y,x,a,b);
	}
	int now = 0;
	for(int i=1;i<=lim;++i){
		if(calc(i) < calc(now)){
			now = i;
		}
	}
	writeln(now);
	writeln(calc(now));
	return 0;
}

