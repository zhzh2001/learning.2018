#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
namespace IO{
	char c,st[66];int tp=0,f;
	#define gc getchar
	#define pc putchar
	inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
}using IO::read;using IO::write;using IO::writeln;using IO::wri;
#define rd read
namespace My{
const int maxn = 250005;
struct Edge{
	int to,nxt,a,b;
	ll dist;
	Edge(){}
	Edge(int to,int nxt,int a,int b):
		to(to),nxt(nxt),a(a),b(b){dist=0;}
}edge[maxn * 2];
int first[maxn],nume;
int n,lim;
void Addedge(int a,int b,int c,int d){
	edge[nume] = Edge(b,first[a],c,d);
	first[a] = nume++;
}
ll f[maxn],dep[maxn],ans;
int q[maxn],pre[maxn];
void predfs(int u,int fa){
	pre[u] = fa;
	q[++q[0]] = u;
	for(int e=first[u];~e;e=edge[e].nxt){
		int v=edge[e].to;
		if(v==fa) continue;
		predfs(v,u);
	}
}
inline ll calc(int limit){
	for(int i=0;i<nume;++i)
		edge[i].dist=edge[i].a+1ll*edge[i].b * limit;
	ans = 0;
	ll mx,mx2;
	Dep(i,q[0],1){
		int u = q[i];
		mx = mx2 = 0;
		f[u] = 0;
		for(int e=first[u];~e;e=edge[e].nxt){
			int v=edge[e].to;
			if(v==pre[u]) continue;
			f[v]+=edge[e].dist;
			if(f[v] > mx) mx2 = mx,mx = f[v];
			else if(f[v] > mx2) mx2 = f[v];
			f[u]=max(f[u],f[v]);
		}
		ans = max(ans,mx+mx2);
	}
	return ans;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n = rd(),lim = rd();
	mem(first,-1);nume = 0;
	rep(i,1,n){
		int x = rd(),y = rd(),a = rd(),b = rd();
		Addedge(x,y,a,b);
		Addedge(y,x,a,b);
	}
	predfs(1,0);
	int l = 0,r = lim;
//	debug(calc(0));
//	debug(calc(1));
	while(r-l>=3){
		int lmid = l + (r-l)/3,rmid = r - (r-l)/3;
		ll ta = calc(lmid),tb = calc(rmid);
		if(ta <= tb) r = rmid;
				else l = lmid;
	}
	int pos = l;
	ll minv = calc(l);
	Rep(i,l+1,r){
		ll tmp = calc(i);
		if(tmp < minv){
			minv = tmp;
			pos = i;
		}
	}
	writeln(pos);
	writeln(minv);
	return 0;
}
}
int main(){
	My::main();
	return 0;
}
