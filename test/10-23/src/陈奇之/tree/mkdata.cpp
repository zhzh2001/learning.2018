#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
namespace IO{
	char c,st[66];int tp=0,f;
	#define gc getchar
	#define pc putchar
	inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
}using IO::read;using IO::write;using IO::writeln;using IO::wri;
#define rd read
const int maxn = 250005;
inline int ran(){
	return rand() << 15 | rand();
}
int main(){
	freopen("tree.in","w",stdout);
	srand(time(NULL));
	int n = 1000,lim = 100000;
	wri(n),writeln(lim);
	Rep(i,2,n){
		printf("%d %d %d %d\n",ran()%(min(20,i-1))+1,i,ran()%2000001-1000000,ran()%11-5);
	}
	return 0;
}

