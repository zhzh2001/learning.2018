#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
namespace IO{
	char c,st[66];int tp=0,f;
	#define gc getchar
	#define pc putchar
	inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
}using IO::read;using IO::write;using IO::writeln;using IO::wri;
#define rd read
namespace My{
const int maxK = 1e5+233;
const int maxQ = 1e5+233;
const int maxN = 1005;
struct Wall{
	int x,y,dir;
	bool operator < (const Wall &w) const{
		return y < w.y;
	}
}w[maxK];
struct Query{
	int x,y,id;
	bool operator < (const Query &w) const{
		return y < w.y;
	}
}q[maxQ];
int ans[maxQ];
pair<int,int> f[2][maxN];
pair<int,int> merge(pair<int,int> a,pair<int,int> b){
	return make_pair(max(a.fi,b.fi),min(a.se,b.se));
}
int n,m,d,K,Q;
int main(){
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	n = rd(),m = rd(),d = rd();
	if(n==d){
		int K=rd();Rep(i,1,K)rd(),rd(),rd();
		int Q=rd();Rep(i,1,Q)rd(),rd();
		Rep(i,1,Q) writeln(1);
		return 0;
		//ȫ��true 
	}
	K = rd();
	Rep(i,1,K){
		w[i] . x = rd();
		w[i] . y = rd();
		char ch = gc();
		while(ch != '-' && ch != '+') ch = gc();
		if(ch=='+') w[i].dir = 1; else w[i].dir = -1;
	}
	Q = rd();
	sort(w+1,w+1+K);
	Rep(i,1,Q){
		q[i] . x = rd();
		q[i] . y = rd();
		q[i] . id = i;
	}
	sort(q+1,q+1+Q);
	rep(k,0,2*(n-d)){
		f[(K+1)&1][k] = make_pair(1,n);
	}
	w[K+1].y = m + 1;
	w[0].y = 1;
	int j = Q;
	for(int i=K;i>=0;i--){
		for(;j>=1 && (w[i].y <= q[j].y && q[j].y <= w[i+1].y);j--){
			int len = w[i+1].y - q[j].y;
			pair<int,int> res = f[(i+1)&1][len%(2*(n-d))];
			if(res.fi <= res.se){
				int l = max(1,res.fi - len),r = min(n,res.se + len);
				if(l <= q[j].x && q[j].x <= r) ans[q[j].id] = 1;
							  			  else ans[q[j].id] = 0;
			} else{
				ans[q[j].id] = 0;
			}
		}
		//add i
		if(!i) continue;
		f[i&1][0] = make_pair(w[i].x,w[i].x+d-1);
		rep(k,1,2*(n-d)){
			if(f[i&1][k-1].se==n) w[i].dir = -1;
			if(f[i&1][k-1].fi==1) w[i].dir = 1;
			f[i&1][k].fi = f[i&1][k-1].fi + w[i].dir;
			f[i&1][k].se = f[i&1][k-1].se + w[i].dir;
		}
		rep(k,0,2*(n-d)){
			int len = w[i+1].y - w[i].y;
			int kk = (k + len) % (2*(n-d));
			if(f[(i+1)&1][kk].fi <= f[(i+1)&1][kk].se){
				f[i&1][k] = merge(f[i&1][k],make_pair(max(1,f[(i+1)&1][kk] . fi - len),min(f[(i+1)&1][kk] . se + len,n)));
			} else f[i&1][k] = make_pair(n+1,0);
		}
	}
	Rep(i,1,Q)
		writeln(ans[i]);
	return 0;
}
}
int main(){
	My::main();
	return 0;
}

