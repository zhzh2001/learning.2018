#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<iostream>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 1001,M = 100005;
int n,m,d;
int k,T,Q,tot;
int ans[M];
struct dor{
	int x,y;
	int opt,id;
	friend bool operator <(dor x,dor y){
		if(x.y == y.y){
			if(x.opt == 0) return true;
			return false;
		}
		return x.y < y.y;
	}
}doors[M*2];
int nowUp[N],nowDown[N],lastUp[N],lastDown[N];
int lastpos;

signed main(){
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	n = read(); m = read(); d = read();
	k = read();
	if(d == 0){
		int Mxy = 0;
		for(int i = 1;i <= k;++i){
			int x = read(),y = read();
			char opsl; scanf("%c",&opsl);
			Mxy = max(Mxy,y);
		}
		int Q = read();
		for(int i = 1;i <= Q;++i){
			int x = read(),y = read();
			if(y > Mxy) puts("1");
			else puts("0");
		}
		return 0;
	}
	if(d == n){
		for(int i = 1;i <= k;++i){
			int x = read(),y = read();
			char opsl;scanf("%c",&opsl);
		}
		int Q = read();
		for(int i = 1;i <= Q;++i){
			int x = read(),y = read();
			puts("1");
		}
		return 0;
	}
	for(int i = 1;i <= k;++i){
		doors[i].x = read();
		doors[i].y = read();
		doors[i].id = i;
		char opsl[3];scanf("%s",opsl+1);
		if(opsl[1] == '-') doors[i].opt = -1;
		else doors[i].opt = 1;
	}
	T = (n-d)*2;
	Q = read();
	for(int i = 1;i <= Q;++i){
		doors[i+k].x = read();
		doors[i+k].y = read();
		doors[i+k].id = i;
		doors[i+k].opt = 0;
	}
	sort(doors+1,doors+k+Q+1);
	tot = k+Q; lastpos = k+Q+1;
	doors[lastpos].y = m+1;
	for(int i = 0;i < T;++i){
		lastUp[i] = 1;
		lastDown[i] = n;
	}
	for(int p = tot;p >= 1;--p){
		if(doors[p].opt == 0){
			int t = (doors[lastpos].y - doors[p].y)%T;   //到上一个door的时间
			int tt = (doors[lastpos].y - doors[p].y);
			if(lastUp[t] == 0 && lastDown[t] == 0){
				ans[doors[p].id] = 0;
				continue;
			}
			if(doors[p].x >= max(1,lastUp[t]-tt) && doors[p].x <= min(lastDown[t]+tt,n))
				ans[doors[p].id] = 1;
			else ans[doors[p].id] = 0;
		}else{
			int t = (doors[lastpos].y - doors[p].y)%T;
			int tt = (doors[lastpos].y - doors[p].y);
			for(int i = 0;i < T;++i){
				int j = (i+t)%T;
				if(lastUp[j] == 0 && lastDown[j] == 0){
					nowUp[i] = 0;
					nowDown[i] = 0;
				}else{
					nowUp[i] = max(doors[p].x,lastUp[j]-tt);
					nowDown[i] = min(doors[p].x+d-1,lastDown[j]+tt);
					if(nowUp[i] > nowDown[i]) nowUp[i] = nowDown[i] = 0;
				}
				if(doors[p].opt == -1){         //移动门
					if(doors[p].x == 1){
						doors[p].opt = 1;
						++doors[p].x;
					}else --doors[p].x;
				}else{
					if(doors[p].x+d-1 == n){
						doors[p].opt = -1;
						--doors[p].x;
					}else ++doors[p].x;
				}
			}
			lastpos = p;
			memcpy(lastUp,nowUp,sizeof(nowUp));
			memcpy(lastDown,nowDown,sizeof(nowDown));
		}
	}
//	for(int i = 1;i <= tot;++i) printf("%d %d %d\n",doors[i].opt,doors[i].x,doors[i].y);
	for(int i = 1;i <= Q;++i) printf("%d\n",ans[i]);
	return 0;
}