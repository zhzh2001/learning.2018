#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<iostream>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

double a,vm,L,d,lim;
double t1,v1,ans;

signed main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a = read(); vm = read(); L = read(); d = read(); lim = read();
	if(lim >= vm){
		t1 = sqrt(2*L/a);
		v1 = t1*a;
		if(v1 <= vm){
			printf("%.9lf\n",t1);
			return 0;
		}else{
			t1 = vm/a;
			double x = L-0.5*a*t1*t1;
			t1 += x/vm;
			printf("%.9lf\n",t1);
			return 0;
		}
	}else{
		t1 = sqrt(2*d/a);
		v1 = t1*a;
		if(v1 <= lim){
			t1 = sqrt(2*L/a);
			v1 = t1*a;
			if(v1 <= vm){
				printf("%.9lf\n",t1);
				return 0;
			}else{
				t1 = vm/a;
				double x = L-0.5*a*t1*t1;
				t1 += x/vm;
				printf("%.9lf\n",t1);
				return 0;
			}
		}else{
			t1 = sqrt((2*d+lim*lim/a)/(2*a));
			v1 = t1*a;
			if(v1 <= vm){
				ans = t1+(v1-lim)/a;
			}else{
				t1 = vm/a;
				ans += t1;
				double x = 0.5*vm*t1;
				t1 = (vm-lim)/a;
				x += 0.5*(vm+lim)*t1;
				ans += t1;
				x = d-x;
				t1 = x/vm;
				ans += t1;
			}
			v1 = sqrt(2*a*(L-d)+lim*lim);
			if(v1 <= vm){
				ans += (v1-lim)/a;
				printf("%.9lf\n",ans);
				return 0;
			}else{
				t1 = (vm-lim)/a;
				ans += t1;
				double x = 0.5*(vm+lim)*t1;
				x = L-d-x;
				ans += x/vm;
				printf("%.9lf\n",ans);
				return 0;
			}
		}
	}
	return 0;
}
