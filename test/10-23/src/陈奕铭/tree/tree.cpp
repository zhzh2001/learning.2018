#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<iostream>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 105;
ll Ans,ans;
int n,lim;
int to[N*2],nxt[N*2],head[N],cnt,a[N*2],b[N*2];
ll mx,pos;
ll Mx1[N],Mx2[N];

inline void insert(int x,int y,int z,int w){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt; a[cnt] = z; b[cnt] = w;
}

void dfs(int x,int fa){
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa){
			dfs(to[i],x);
			ll w = a[i]+pos*b[i];
			if(Mx1[x] < Mx1[to[i]]+w){
				Mx2[x] = Mx1[x];
				Mx1[x] = Mx1[to[i]]+w;
			}else if(Mx2[x] < Mx1[to[i]]+w){
				Mx2[x] = Mx1[to[i]]+w;
			}
		}
	mx = max(Mx1[x]+Mx2[x],mx);
}

signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n = read(); lim = read();
	if(n <= 100){
		for(int i = 1;i < n;++i){
			int x = read(),y = read(),z = read(),w = read();
			insert(x,y,z,w);
			insert(y,x,z,w);
		}
		Ans = 1e16;
		for(int i = 0;i <= lim;++i){
			memset(Mx1,0,sizeof Mx1);
			memset(Mx2,0,sizeof Mx2);
			mx = 0;
			pos = i;
			dfs(1,1);
			if(mx < Ans){
				Ans = mx;
				ans = pos;
			}
		}
		printf("%lld\n%lld\n", ans,Ans);
	}
	return 0;
}