#include <cmath>
#include <cstdio>
using namespace std;
const double eps=1e-9;
double dv,mv,L,D,up,tim,dis,uptim,updis,hafdis,hhdis,ladis,latim,lamadis,del,ppt,pptdis;
inline double get(double S){double tt=(double)sqrt((double)2.00*S/dv);return tt;}
inline void solve_ma(){dis=mv*mv/(dv*2.00);if(dis+eps<L){tim=(double)(mv/dv)+(double)((L-dis)/mv);}else tim=get(L);}
int main()
{
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	scanf("%lf%lf%lf%lf%lf",&dv,&mv,&L,&D,&up);
	if(up>=mv) solve_ma();
	else
	{
		uptim=(double)up/dv; updis=(double)uptim*up*0.500;
		if(updis+eps<D)
		{
			hafdis=(double)(D-updis)*0.500; del=(double)4.00*up*up+8.00*dv*hafdis; ppt=(double)(-2.00*up+sqrt(del))/(2.00*dv);
			if(up+ppt*dv+eps>mv)
			{
				ppt=(double)((mv-up)/dv); pptdis=(double)(up+mv)*ppt*0.500; hhdis=D-updis; tim=(double)uptim+ppt*2.00+(hhdis-2.00*pptdis)/mv;
			}else tim=(double)uptim+(double)(ppt*2.00); ladis=(double)L-D; latim=((double)(mv-up))/dv; lamadis=latim*(up+mv)*0.500;
			if(lamadis+eps<ladis)
			{
				tim=(double)(tim+latim+((double)(ladis-lamadis)/mv)); 
			}else{del=(double)(4.00*up*up+8.00*dv*ladis); ppt=(double)(-2.00*up+sqrt(del))/(2.00*dv); tim=(double)(tim+ppt);}
		}else solve_ma();
	}printf("%.15lf\n",tim);
}
