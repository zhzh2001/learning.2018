#include <queue>
#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
#define int long long
using namespace std;
inline int read(){int s=0,f=0;char ch;while(!isdigit(ch))f|=(ch=='-'),ch=getchar();while(isdigit(ch))s=s*10+ch-'0',ch=getchar();return f?-s:s;}
#define r(x) x=read()
#define M(a,v) memset(a,v,sizeof a)
const int N=250005,M=2000005,TIM=30000000,inf=0x7fffffffffff;
int n,tot=0,Next[M],to[M],head[M],va[M],vb[M],d[N];
bool arr[N];
inline void add(int x,int y,int a,int b){Next[++tot]=head[x]; to[tot]=y; va[tot]=a; vb[tot]=b; head[x]=tot;}
inline int bfs(int s,int mid)
{
	int i,ma=s,x; queue<int>q; q.push(s); M(arr,0); d[s]=0; arr[s]=1;
	while(!q.empty())
	{
		x=q.front(); q.pop(); if(d[x]>d[ma])ma=x; for(i=head[x];i;i=Next[i])if(!arr[to[i]]){arr[to[i]]=1; d[to[i]]=max(0LL,d[x]+va[i]+mid*vb[i]); q.push(to[i]);}
	}return ma;
}
inline int jud(int mid){int i,ma=0,rt1=bfs(1,mid),rt2; rt2=bfs(rt1,mid); return max(d[rt2],0LL);}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int i,l=0,s,r,pp,mid1,mid2,s1,s2,x,y,a,b,bo=0,up,re=inf,tmp; r(n); r(r);
	for(i=1;i<n;i++)
	{
		r(x); r(y); r(a); r(b); add(x,y,a,b); add(y,x,a,b);
	}if(n<=50000)up=1LL*100;else up=TIM/(n*2);
	for(i=1;i<=up;i++)
	{
		pp=(r-l+1)/3; mid1=l+pp-1; mid2=r-pp+1; s1=jud(mid1); s2=jud(mid2); if(s1<=s2){if(re>s1||(re==s1&&tmp>mid1))re=s1,tmp=mid1;r=mid2;}else {if(re>s2||(re==s2&&tmp>mid2))re=s2,tmp=mid2;l=mid1;}
	}if(r-l+1<=5){for(i=l;i<=r;i++){s=jud(i); if(s<re||(s==re&&i<tmp)){tmp=i;re=s;}}}
	printf("%lld\n%lld\n",tmp,re);
}
