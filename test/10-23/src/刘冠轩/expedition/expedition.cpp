#include <bits/stdc++.h>
using namespace std;
inline double read(){
	double t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
double a,v,L,d,t,ans,s,p;
int main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a=read();v=read();L=read();d=read();t=read();
	if((v<=t)||(sqrt(2*d/a)*a<=t)){
		if(sqrt(2*L/a)*a<=v){
			ans=sqrt(2*L/a);
		}else{
			ans=v/a+(L-(v/a*v/2))/v;
		}
	}else{
		s=d-t*t/a/2;
		ans=t/a;
		p=(v-t)*2/a;
		if(t*p+a*p*p/4>=s){
			p=(-t+sqrt(t*t+a*s))/a;
			ans+=p*2;
		}else{
			s-=(v-t)/a*(t+v);
			ans+=(v-t)*2/a+s/v;
		}
		s=(v-t)/a*(v+t)/2;
		if(s>=L-d){
			ans+=(-t+sqrt(t*t-a*(d-L)*2))/a;
		}else{
			ans+=(v-t)/a;
			s=L-d-s;
			ans+=s/v;
		}
	}
	printf("%.10lf",ans);
	return 0;
}
