#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=100005;
int n,m,l,k,x[N],f[N],X,Y,p;
char str[5];
bool flag;
void dfs(int X,int Y){
	if(Y==0||Y>n)return;
	if(X==m+1){flag=0;return;}
	if(!flag)return;
	if(x[X])if(min(x[X],x[X]+f[X]*l-f[X])>Y||max(x[X],x[X]+f[X]*l-f[X])<Y)return;
	for(int i=1;i<=m;i++)if(x[i]){
		if(x[i]+l*f[i]==0||x[i]+l*f[i]>m)
		f[i]=-f[i];else x[i]+=f[i];
	}
	dfs(X+1,Y+1);
	dfs(X+1,Y);
	dfs(X+1,Y-1);
	for(int i=1;i<=m;i++)if(x[i]){
		if(x[i]-l*f[i]==0||x[i]-l*f[i]>m)
		f[i]=-f[i];else x[i]-=f[i];
	}
}
int main(){
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	n=read();m=read();l=read();k=read();
	for(int i=1;i<=k;i++){
		X=read(),Y=read();
		x[Y]=X;
		scanf("%s",str);
		if(str[0]=='+')f[Y]=1;else f[Y]=-1;
	}
	p=read();
	for(int i=1;i<=p;i++){
		X=read();Y=read();
		flag=1;dfs(Y,X);
		if(!flag)puts("1");else puts("0");
	}
	return 0;
}
