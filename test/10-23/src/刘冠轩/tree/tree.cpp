#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=100005;
int n,lim,d,ans=0x3fffffff,head[N],cnt,p,mx,delta;
struct edge{int to,nt,a,b;}e[N*2];
void add(int u,int v,int a,int b){
	e[++cnt]=(edge){v,head[u],a,b};
	head[u]=cnt;
}
void dfs1(int u,int fa,int dis){
	if(dis>mx){mx=dis;p=u;}
	for(int i=head[u];i;i=e[i].nt)
	if(e[i].to!=fa)dfs1(e[i].to,u,dis+e[i].a+e[i].b*delta);
}
void dfs2(int u,int fa,int dis){
	if(dis>mx){mx=dis;p=u;}
	for(int i=head[u];i;i=e[i].nt)
	if(e[i].to!=fa)dfs2(e[i].to,u,dis+e[i].a+e[i].b*delta);
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();lim=read();
	for(int i=1;i<n;i++){
		int u=read(),v=read(),a=read(),b=read();
		add(u,v,a,b);add(v,u,a,b);
	}
	for(delta=0;delta<=lim;delta++){
		mx=-1;p=0;
		dfs1(1,0,0);
		dfs2(p,0,0);
		if(mx<ans){ans=mx;d=delta;}
	}
	cout<<d<<'\n'<<ans;
	return 0;
}
