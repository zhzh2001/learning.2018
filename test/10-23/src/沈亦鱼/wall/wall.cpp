#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,d,k,lh,q,lx,rx,y,ua,ub,zz,z,f,a[110000],b[110000];
char c[110000];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
void sor(int l,int r){
	int i=l,j=r,x=b[(l+r)>>1];
	while(i<=j){
		while(b[i]<x)i++;
		while(x<b[j])j--;
		if(i<=j){
			swap(a[i],a[j]);
			swap(b[i],b[j]);
			swap(c[i],c[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}
int main(){
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	n=read();
	m=read();
	d=read();
	k=read();
	lh=n*2-d*2+1;
	for(int i=1;i<=k;i++){
//		a[i]=read();
//		b[i]=read();
//		c[i]=getchar();
//		scanf("%c\n",c[i]);
		scanf("%d %d %c\n",&a[i],&b[i],&c[i]);
//		printf("! %c !\n",c[i]);
	}
	sor(1,k);
	q=read();
	for(int i=1;i<=q;i++){
		f=0;
		lx=rx=read();
		y=read();//printf("%d !\n",y);
		for(int j=1;j<=k;j++){//printf("%d %d %d !\n",j,b[j],y);
			if(b[j]>y){
				ua=a[j];
				ub=a[j]+d-1;
				zz=b[j]-y;
				z=(zz)%lh;
				if(c[i]=='+'){
					ua+=z;
					ub+=z;
					if(ub>n){
						ua-=(ub-n)*2;
						ub=n-(ub-n);
					}
					if(ua<0){
						ub+=-ua*2;
						ua=-ua;
					}
				}
				else{
					ua-=z;
					ub-=z;
					if(ua<0){
						ub+=-ua*2;
						ua=-ua;
					}
					if(ub>n){
						ua-=(ub-n)*2;
						ub=n-(ub-n);
					}
				}
				lx=max(0,lx-zz);
				rx=min(n,rx+zz);
				lx=max(lx,ua);
				rx=min(rx,ub);
				if(lx>rx){
					f=1;
					break;
				}
				y=b[j];//printf("%d !%d %d\n",j,ua,ub);
			}
		}
		if(f)puts("0");
		else puts("1");//printf("%d !\n",k);
	}
	return 0;
}
