#include<cstdio>
#include<algorithm>
using namespace std;
int n,u,v,a,b,l,ll,mid,rr,r,tot,p[510000],ne[510000],he[260000];
long long del,ans,s1,s2,us,ta[510000],tb[510000],f[260000];
int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
void dfs(int k,int fa){
	f[k]=0;
	long long s1=0,s2=0;
	for(int i=he[k];i;i=ne[i])
		if(p[i]!=fa){
			dfs(p[i],k);
			us=f[p[i]]+ta[i]+tb[i]*del;
			if(us>s1){
				s2=s1;
				s1=us;
			}
			else if(us>s2)s2=us;
		}
	f[k]=s1;
	ans=max(ans,s1+s2);
}
long long che(int x){
	del=x;
	ans=0;
	dfs(1,0);
	return ans;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	l=0;
	r=read()+1;
	for(int i=1;i<n;i++){
		u=read();
		v=read();
		a=read();
		b=read();
	tot++;
	p[tot]=v;
	ta[tot]=a;
	tb[tot]=b;
	ne[tot]=he[u];
	he[u]=tot;
	tot++;
	p[tot]=u;
	ta[tot]=a;
	tb[tot]=b;
	ne[tot]=he[v];
	he[v]=tot;
//printf("%d\n",i);
	}//printf("%d %d\n",l,r);
	while(l+1<r){
		mid=(r-l+1)/3;
		ll=l+mid;
		rr=r-mid;//printf("%d %d\n",ll,rr);
		if(che(ll)>che(rr))l=ll;
		else r=rr;//printf("%d %d\n",l,r);
	}
//	printf("%d %d %d %d %d\n",che(1),che(2),che(3),che(4),che(5));
//	for(int i=262144;i>0;i>>=1)
//		if(l>=i)if(che(l-i)==che(l))
	ll=che(l);
	rr=che(r);
	if(ll<=rr){
		printf("%d\n",l);
		printf("%lld",ll);
	}
	else{
		printf("%d\n",r);
		printf("%lld",rr);
	}
//	printf("%d\n",che(15));
	return 0;
}
