#include<cmath>
#include<cstdio>
using namespace std;
double eps=1e-10,a,b,v,l,d,li,ve,t,s,ut;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
int main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a=read();
	v=read();
	l=read();
	d=read();
	li=read();
	if(d-eps>l){
		t=v;
		t=t/a;
		s=t*v/2;
		if(s-eps<l){
			t+=(l-s)/v;
			printf("%0.10lf",t);
			return 0;
		}
		else{
			t=sqrt(l*2/a);
			printf("%0.10lf",t);
			return 0;
		}
	}
	else{
		b=li/d;
		if(a-eps<b){
			t=v;
			t=t/a;
			s=t*v/2;
			if(s-eps<l){
				t+=(l-s)/v;
				printf("%0.10lf",t);
				return 0;
			}
			else{
				t=sqrt(l*2/a);
				printf("%0.10lf",t);
				return 0;
			}
		}
		b=li/a;
		s=li*b/2;
		s=(d-s)/2;
		ve=sqrt(li*li+2*a*s);
		if(ve+eps>v){
			t=v/a;
			s=v*t/2;//printf("%0.10lf\n",t);
			ut=(v-li)/a;//printf("%0.10lf\n",ut);
			s+=v*ut-a*ut*ut/2;
			s=d-s;//printf("%0.10lf\n",s);
			t+=ut;
			t+=s/v;//printf("%0.10lf\n",t);
			if((v-li)/(l-d)+eps<a){
				ut=(v-li)/a;
				t+=ut;
				l=l-d-ut*li-a*ut*ut/2;
				t+=l/v;
			}
			else{
				ve=sqrt(li*li+(l-d)*a*2);
				t+=(ve-li)/a;
			}
			printf("%0.10lf",t);
			return 0;
		}
		else{
			t=ve/a+(ve-li)/a;//printf("%0.10lf",t);
			if((v-li)/(l-d)+eps<a){
				ut=(v-li)/a;
				t+=ut;
				l=l-d-ut*li-a*ut*ut/2;
				t+=l/v;
			}
			else{
				ve=sqrt(li*li+(l-d)*a*2);
				t+=(ve-li)/a;
			}
			printf("%0.10lf",t);
			return 0;
		}
	}
	return 0;
}
