#include <bits/stdc++.h>
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

double eryuanyici(double a, double b, double c) {
  // printf("a=%.10lf\n", a);
  // printf("b=%.10lf\n", b);
  // printf("c=%.10lf\n", c);
  return (-b+sqrt(b*b-4*a*c))/a*.5;
}

double work(double a, double vm, double L, double d, double limit) {

  if (limit >= vm || limit >= sqrt(2 * d * a)) {
    // just rush
    double t = sqrt(2 * L / a);
    if (t * a > vm) {
      double t1 = vm / a;
      double t2 = (L - 0.5 * a * t1 * t1) / vm;
      return t1 + t2;
    } else {
      return t;
    }
  } else {
    // must go fall
    // puts("fuckB");
    double t1 = limit / a;
    double t = sqrt((d + t1 * limit / 2) / a);
    double ans = 0;
    if (t * a > vm) {
      // must at vm for a while
      // puts("fuckD");
      double t2 = (d - ((vm*vm-limit*limit)*.5/a) - 0.5*vm*vm/a) / vm;
      ans = vm / a + t2 + (vm - limit) / a;
    } else {
      // go top and fell down
      ans = 2 * t - t1;
    }

    // printf("%.4lf\n", ans);

    // now v = limit
    // just rush
    double ts = eryuanyici(a * .5, limit, d - L);
    // printf("%.10lf\n", ts);
    if (ts * a + limit > vm) {
      // ends up rush before arrive
      double tt1 = (vm - limit) / a;
      double tt2 = (L - d - (vm * vm - limit * limit) / a / 2) / vm;
      // printf("%.10lf, %.10lf\n", tt1, tt2);
      return ans + tt1 + tt2;
    } else {
      // puts("fuckC");
      // rush until end
      return ts + ans;
    }
  }
}
/*
namespace test {

int cnt;
void describe(double a, double b, double c, double d, double e, double ans) {
  fprintf(stderr, "\nRuntask #%d\n", ++ cnt);
  double out = work(a, b, c, d, e);
  fprintf(stderr, "Out: %.10lf\nAns: %.10lf\n", out, ans);
  assert(fabs(ans - out) <= 1e-6);
  fprintf(stderr, "Pass\n");
}
void test() {
  describe(30, 90, 1000, 1000, 10, 13.796296296296296);
  describe(3, 10000, 10, 4, 4, 2.718458153642379);
  describe(3, 6, 10, 4, 4, 2.759201748);
  describe(1, 1, 2, 1, 3, 2.5);
}

} // namespace test
*/
int main(int argc, char const *argv[]) {

  freopen("expedition.in", "r", stdin);
  freopen("expedition.out", "w", stdout);

  double a = read(), vm = read(), L = read(), d = read(), limit = read();
  printf("%.10lf\n", work(a, vm, L, d, limit));
/*
  test::test();
*/
  return 0;
}