#include <bits/stdc++.h>
#define N 250020
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int to[N<<1], nxt[N<<1], head[N], cnt, as[N<<1], bs[N<<1];
void insert(int x, int y, int a, int b) {
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt; as[cnt] = a; bs[cnt] = b;
  to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt; as[cnt] = a; bs[cnt] = b;
}

long long LIMIT, dis[N], n;
void dfs(int x, int f) {
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    dis[to[i]] = dis[x] + as[i] + bs[i] * LIMIT;
    dfs(to[i], x);
  }
}
int limit_ans;
long long ans = 1ll << 60;
void updateAns(int limit, long long len) {
  if (len < ans) {
    limit_ans = limit;
    ans = len;
  } else if (len == ans) {
    limit_ans = min(limit_ans, limit);
  }
}

long long getLength(int limit) {
  ::LIMIT = limit;
  dis[1] = 0;
  dfs(1, 0);
  int mxp = 1;
  for (int i = 2; i <= n; ++ i) {
    if (dis[i] > dis[mxp]) {
      mxp = i;
    }
  }
  dis[mxp] = 0;
  dfs(mxp, 0);
  long long mxv = dis[1];
  for (int i = 2; i <= n; ++ i) {
    mxv = max(mxv, dis[i]);
  }
  updateAns(limit, mxv);
  return mxv;
}

int main(int argc, char const *argv[]) {

  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);

  n = read();
  int LIMIT_MAX = read();

  for (int i = 1; i < n; ++ i) {
    int x = read(), y = read();
    int a = read(), b = read();
    insert(x, y, a, b);
  }

  if (LIMIT_MAX < 100) {
    for (int i = 0; i <= LIMIT_MAX; ++ i) {
      getLength(i);
    }
  } else {
    for (int i = 0; i <= 50; ++ i) {
      getLength(i);
      getLength(LIMIT_MAX - i);
    }
  }

  printf("%d\n%lld\n", limit_ans, ans);

  return 0;
}