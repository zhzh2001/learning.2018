#include <bits/stdc++.h>
#define K 100020
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int n, m, d, k, q;

struct door {
  int x, y;
  bool isUp;
  door(){}
  door(int y):y(y){}
  door(int x, int y, bool z):x(x),y(y),isUp(z){}
  door time_pass(int t) {
    t = t % ((n - d) * 2);
    if (isUp) {
      if (x - t >= 1) {
        return door(x - t, y, x - t != 1);
      } else {
        return door(1, y, false).time_pass(t - (x - 1));
      }
    } else {
      if (n - (x + d - 1) >= t) {
        return door(x + t, y, x + t + d - 1 == n);
      } else {
        return door(n - d + 1, y, true).time_pass(t - (n - (x + d - 1)));
      }
    }
  }
}ds[K];

bool cmp(const door &a, const door &b) {
  return a.y < b.y;
}

char str[5];
bool ans[K];
// map<pair<pair<int, int>, pair<int, int>>, int> mp;

int main(int argc, char const *argv[]) {

  freopen("wall.in", "r", stdin);
  freopen("wall.out", "w", stdout);

  n = read(), m = read(), d = read();
  k = read();
  for (int i = 1; i <= k; ++ i) {
    ds[i].x = read();
    ds[i].y = read();
    scanf("%s", str);
    ds[i].isUp = str[0] == '-';
    if (ds[i].x == n - d + 1) ds[i].isUp = true;
    if (ds[i].x == 1) ds[i].isUp = false;
    // printf("%d, %d\n", i, ds[i].isUp);
  }
  sort(ds + 1, ds + k + 1, cmp);

  q = read();
  for (int i = 1; i <= q; ++ i) {
    int x = read(), y = read();
    int p = lower_bound(ds + 1, ds + k + 1, door(y), cmp) - ds;
    int nowy = y;
    int ll = x, rr = x;
    bool faked = false;
    // printf("starts from %d\n", p);
    for (int j = p; j <= k; ++ j) {
      int time = ds[j].y - nowy;
      door now = ds[j].time_pass(ds[j].y - y);
      int l = now.x, r = now.x + d - 1;
      // printf("the door is at  %d\n", now.x);

      ll = max(1, ll - time);
      rr = min(n, rr + time);

      if (ll > r || rr < l) {
        faked = true;
        break;
      }


      ll = max(ll, l);
      rr = min(rr, r);
      // pair<pair<int, int>, pair<int, int>> status =
      //   make_pair(make_pair(y % ((n-d)*2), j), make_pair(ll, rr));
      // // printf("not fake, [%d, %d]\n", ll, rr);
      // if (mp[status]) {
      //   faked = ans[mp[status]];
      //   break;
      // }
      // mp[status] = i;

      nowy = ds[j].y;
    }

    printf("%d\n", ans[i] = !faked);

  }

  return 0;
}