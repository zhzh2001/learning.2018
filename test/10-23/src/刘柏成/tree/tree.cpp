#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll INF=0x3f3f3f3f3f3f3f3fLL;
#define file "tree"
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
const int maxn=102;
struct graph{
	int n,m;
	struct edge{
		int to,ca,cb,next;
	}e[maxn*2];
	int first[maxn];
	void addedge(int from,int to,int ca,int cb){
		e[++m]=(edge){to,ca,cb,first[from]};
		first[from]=m;
	}
	int fa[maxn],dfn[maxn],cl;
	ll a[maxn],b[maxn];
	void dfs(int u){
		dfn[++cl]=u;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa[u])
				continue;
			fa[v]=u;
			a[v]=e[i].ca;
			b[v]=e[i].cb;
			dfs(v);
		}
	}
	ll mx[maxn];
	ll qwq[500002];
	pair<ll,ll> work(ll lim){
		dfs(1);
		for(ll i=0;i<=lim;i++){
			ll now=0;
			for(int j=1;j<=n;j++)
				mx[j]=0;
			for(int j=n;j>1;j--){
				int u=dfn[j],v=fa[u];
				ll val=b[u]*i+a[u];
//				printf("val=%lld\n",val);
				now=max(now,mx[u]+val+mx[v]);
				if (mx[u]+val>mx[v])
					mx[v]=mx[u]+val;
			}
//			printf("now=%lld\n",now);
			qwq[i]=now;
		}
		ll mn=qwq[0];
		for(ll i=0;i<=lim;i++)
			mn=min(mn,qwq[i]);
		for(ll i=0;i<=lim;i++)
			if (qwq[i]==mn)
				return make_pair(i,mn);
	}
}g;
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=g.n=read(),lim=read();
	for(int i=1;i<n;i++){
		int u=read(),v=read(),a=read(),b=read();
		g.addedge(u,v,a,b);
		g.addedge(v,u,a,b);
	}
	pair<ll,ll> ans=g.work(lim);
	printf("%lld\n%lld",ans.first,ans.second);
}
