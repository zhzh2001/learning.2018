#include <bits/stdc++.h>
using namespace std;
#define file "wall"
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
struct door{
	int x,y,dir;
	bool operator <(const door& rhs)const{
		return y<rhs.y;
	}
}w[100002];
struct query{
	int id,x,y;
	bool operator <(const query& rhs)const{
		return y<rhs.y;
	}
};
vector<query> p[1005];
bool ans[100002];
char s[10];
int n,m,d,k,q,md;
void work(int o){
	if (!p[o].size())
		return;
	int y=m,l=0,r=n-1,now=o;
	int cur=p[o].size()-1;
	for(int i=k;i;i--){ //answer (w[i].y,y]
		while(cur>=0 && p[o][cur].y>w[i].y){
			query& qwq=p[o][cur];
			ans[qwq.id]=(l-(y-qwq.y)<=qwq.x) && (r+(y-qwq.y)>=qwq.x);
			cur--;
		}
		int qwq=y-w[i].y;
		now=(now-qwq)%md;
		if (now<0)
			now+=md;
		int t=w[i].x;
		if (w[i].dir==-1 && t)
			t=md-t;
		t+=now;
		if (t>=md)
			t-=md;
		if (t>n-d)
			t=md-t;
		l=max(l-qwq,t);
		r=min(r+qwq,t+d-1);
		if (l>r)
			return;
//		fprintf(stderr,"o=%d now=%d l=%d r=%d t=%d\n",o,now,l,r,t);
		y=w[i].y;
	}
	while(cur>=0){
		query& qwq=p[o][cur];
		ans[qwq.id]=(l-(y-qwq.y)<=qwq.x) && (r+(y-qwq.y)>=qwq.x);
		cur--;
	}
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=read(),m=read(),d=read(),k=read();
	md=2*n-2*d;
	for(int i=1;i<=k;i++){
		w[i].x=read()-1,w[i].y=read()-1;
		scanf("%s",s);
		w[i].dir=s[0]=='+'?1:-1;
	}
	sort(w+1,w+k+1);
	q=read();
	if (n==d){
		while(q--)
			puts("1");
		return 0;
	}
	for(int i=1;i<=q;i++){
		query tmp;
		tmp.id=i,tmp.x=read()-1,tmp.y=read()-1;
		p[(m-tmp.y)%md].push_back(tmp);
	}
	for(int i=0;i<md;i++)
		sort(p[i].begin(),p[i].end());
	for(int o=0;o<md;o++)
		work(o);
	for(int i=1;i<=q;i++)
		puts(ans[i]?"1":"0");
}
