#include <bits/stdc++.h>
using namespace std;
#define file "expedition"
typedef long double db;
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
db qwq(db v0,db a,db vm,db l){ //vm>=v0
	db t=(vm-v0)/a;
	db fst=v0*t+0.5*a*t*t;
	if (fst<=l)
		return (l-fst)/vm+t;
	else return (sqrt(v0*v0+2*a*l)-v0)/a;
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	db a=read(),vm=read(),l=read(),d=read(),lim=read();
	if (vm<=lim)
		return printf("%.15f",(double)qwq(0,a,vm,l)),0;
	db v1=sqrt(2*a*d);
	if (v1<=lim)
		return printf("%.15f",(double)qwq(0,a,vm,l)),0;
	db sec=qwq(lim,a,vm,l-d);
	db mx=sqrt(a*d+lim*lim/2);
	if (mx<=vm){
		db t=(2*mx-lim)/a;
		printf("%.15f",double(t+sec));
	}else{
		db l1=(2*vm*vm-lim*lim)/(2*a),t1=(d-l1)/vm; 
		printf("%.15f",double(t1+(2*vm-lim)/a+sec));
	}
}
