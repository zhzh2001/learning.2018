#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 250010;
int n,d,x[N],y[N],l,r,mid,Mid,pos;
ll a[N],b[N],Ans;
int tot,first[N],to[N<<1],last[N<<1];
ll val[N<<1];
inline void Add(int x,int y,ll z){to[++tot]=y,val[tot]=z,last[tot]=first[x],first[x]=tot;}
ll ans,dp[N];
inline ll max(ll a,ll b){return a>b?a:b;}
inline void dfs(int u,int fa){
	dp[u]=0;
	cross(i,u) if (to[i]!=fa) dfs(to[i],u),dp[u]=max(dp[u],dp[to[i]]+val[i]);
}
inline void dfs(int u,int fa,ll Max){
	ans=max(ans,dp[u]+Max);//printf("%d %lld %lld\n",u,dp[u],Max);
	ll ma=0,pos=0,Ma=0,Pos=0;
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		if (dp[v]+val[i]>=ma) Ma=ma,Pos=pos,ma=dp[v]+val[i],pos=v;
		else if (dp[v]>Ma) Ma=dp[v]+val[i],Pos=v;
	}
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		if (v==pos) dfs(v,u,max(Max,Ma)+val[i]);
			else dfs(v,u,max(Max,ma)+val[i]);
	}
}
inline ll calc(ll X){
	tot=ans=0;
	For(i,1,n) first[i]=0;
	For(i,1,n-1) Add(x[i],y[i],a[i]+b[i]*X),Add(y[i],x[i],a[i]+b[i]*X);
	dfs(1,0),dfs(1,0,0);
	//For(i,1,n) printf("%lld ",dp[i]);puts("");
	return ans;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),d=read();
	For(i,1,n-1) x[i]=read(),y[i]=read(),a[i]=read(),b[i]=read();
	if (1ll*n*d<=(ll)1e8){
		Ans=(ll)1e12;
		For(i,0,d){ //printf("%d\n",i);
			ll sum=calc(i);
			if (sum<Ans) Ans=sum,pos=i;
		}
		printf("%d\n%lld\n",pos,Ans);
		return 0;
	}
	l=0,r=d;
  	while (l<r-1){
      	mid=l+r>>1,Mid=mid+r>>1;//printf("%d %d\n",l,r);
      	if (calc(mid)>calc(Mid)) l=mid;
        	else r=Mid;
	}
	//For(i,0,d) printf("%lld ",calc(i));puts("");
	//printf("%d %d\n",l,r);
	if (calc(l)>calc(r)) printf("%d\n%lld\n",r,calc(r));
		else printf("%d\n%lld\n",l,calc(l));
}
/*
5 5
1 2 20 -3
2 3 10 -3
3 4 22 -2
3 5 26 -3
*/
