#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
using namespace std;
inline int rd() {
	int x=0,f=1;
	char ch=getchar();
	for(; !isdigit(ch); ch=getchar()) if(ch=='-') f=-1;
	for(; isdigit(ch); ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
ll N,Lim;
struct edge {
	ll ed,nxt,a,b;
} E[600010];
int head[600010],Ecnt;
void addEdge(int st,int ed,int a,int b) {
	E[++Ecnt].ed=ed,E[Ecnt].nxt=head[st],head[st]=Ecnt;
	E[Ecnt].a=a,E[Ecnt].b=b;
}
ll f[600010],g[600010];
ll maxv=1e15,ans=0;
void dfs(int st,int fa,ll delta) {
	//cerr<<__func__<<" in "<<st<<endl;
	for(int i=head[st]; i; i=E[i].nxt) {
		int ed=E[i].ed;
		ll vv=E[i].a+E[i].b*delta;
		//cerr<<"ed="<<ed<<" vv="<<vv<<endl;
		if(ed!=fa) {
			dfs(ed,st,delta);
			f[st]=max(f[st],g[st]+g[ed]+vv);
			f[st]=max(f[st],g[st]+vv);
			f[st]=max(f[st],g[st]);
			g[st]=max(g[st],g[ed]+vv);
			g[st]=max(g[st],vv);
			g[st]=max(g[st],0ll);
		}
	}
	f[st]=max(f[st],g[st]);
	//fprintf(stderr,"f[%d]=%lld\n",st,f[st]);
}
ll calc_dia(ll delta) {
	memset(f,0,(N+N)*(sizeof(ll)));
	memset(g,0,(N+N)*(sizeof(ll)));
	ll ret=-1e13;
	dfs(1,0,delta);
	for(int i=1; i<=N; ++i) ret=max(ret,f[i]);
	return ret;
}
//main========================
int main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	N=rd(),Lim=rd();
	for(int i=1; i<N; ++i) {
		int x=rd(),y=rd(),a=rd(),b=rd();
		addEdge(x,y,a,b);
		addEdge(y,x,a,b);
	}
	if(N<=1000) {
		for(int i=0; i<=Lim; ++i) {
			ll d=calc_dia(i);
		//	cerr<<"d="<<d<<endl;
			if(d<maxv) {
				maxv=d;
				ans=i;
				//cerr<<i<<" "<<maxv<<endl;
			}
		}
		//cerr<<ans<<" "<<maxv<<endl;
		cout<<ans<<"\n"<<maxv<<endl;
	} else {
		int L=0,R=Lim;
		ll anoans=0,anoval=1e15;
		while(L<=R) {
			int mid=(L+R)/2;
			ll aa=calc_dia(mid-1),bb=calc_dia(mid),cc=calc_dia(mid+1);
			//cerr<<"mid="<<mid<<" "<<aa<<" "<<bb<<" "<<cc<<endl;
			if(aa>bb&&bb>cc) {
				if(anoval>bb)
					anoans=mid,anoval=bb;
				L=mid+1;
			} else if(aa<bb&&bb<cc) {
				if(anoval>bb)
					anoans=mid,anoval=bb;
				R=mid-1;
			} else if(aa>bb&&bb==cc) {
				anoans=mid,anoval=bb;
				break;
			} else if(aa==bb&&bb<=cc) {
				if(anoval>bb)
					anoans=mid,anoval=bb;
				R=mid-1;	
			}else if(aa>bb && bb<cc) {
				anoans=mid,anoval=bb;
				break;
			} else break;
		}//cerr<<anoans<<" "<<anoval<<endl;
		cout<<anoans<<"\n"<<anoval<<endl;
		exit(0);
	}

	return 0;
}

