#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
char _opt[20];
const int MAXN=3e5+10;
int N,M,d,K,Q;
int down[4400],mov[4400];
map<pair<int,int> ,int > vis; 
struct _dr {
	int x,y,direct;
	_dr pass_x(int tim) {
		tim%=2*(N-d);
		int pos=vis[mk(x,direct)]+tim;
		return (_dr){down[pos],y,mov[pos]};
	}
	bool operator <(const _dr& other)  const{
		return y<other.y;
	}
}door[MAXN],can[MAXN];
void calc(int x,int y);
//main========================
int main() {
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	N=rd(),M=rd(),d=rd(),K=rd();
	down[0]=1,mov[0]=true;
	for(int i=1;i<=4000;++i) {
		if(N==d) {
			down[i]=down[i-1];
			if(!vis[mk(down[i],mov[i])]) vis[mk(down[i],mov[i])]=i;
			if(!vis[mk(down[i],mov[i]^1)]) vis[mk(down[i],mov[i]^1)]=i;
		} else {
			mov[i]=mov[i-1],down[i]=down[i-1]+(mov[i]?1:-1);
			if(down[i]==N-d+1||down[i]==1) {
				if(!vis[mk(down[i],mov[i])]) vis[mk(down[i],mov[i])]=i;
				mov[i]^=1;
			}
		}
		//if(i<=30) printf("time=%d pos=%d,move=%d\n",i,down[i],mov[i]);
		if(!vis[mk(down[i],mov[i])]) vis[mk(down[i],mov[i])]=i;
	}
	for(int i=1;i<=K;++i) {
		door[i].x=rd(),door[i].y=rd();
		scanf("%1s",_opt);
		if(_opt[0]=='+') door[i].direct=true;
		else	door[i].direct=false;	
	}
	sort(door+1,door+K+1);
	Q=rd();
	for(int i=1;i<=Q;++i) {
		int x=rd(),y=rd();
		calc(x,y);
	}
	return 0;
}
void calc(int x,int y) {
	int i;
	for(i=1;i<=K;++i) if(door[i].y>=y) break;
	if(i>K) { puts("1");return; }
	else {
		int ny=y;
	//	printf("i=%d (%d,%d)\n",i,door[i].x,door[i].y);
		int l=x,r=x;
		for(;i<=K;++i) {
			_dr dr=door[i].pass_x(door[i].y-y);
			l=max(1,l-(door[i].y-ny));r=min(N,r+(door[i].y-ny));
		//	printf("time passed %d\n[%d,%d],door=[%d,%d]\n",door[i].y-y,l,r,dr.x,dr.x+d-1);
			if((l>dr.x+d-1||r<dr.x)) {
				puts("0") ;return;
			}
			l=max(l,dr.x),r=min(r,dr.x+d-1);
		//	printf("can from [%d,%d]\n",l,r);
			ny=door[i].y;
		}
		puts("1");
	} 
}
