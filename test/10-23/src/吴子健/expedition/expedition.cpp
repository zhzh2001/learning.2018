#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
//defs========================
db a,v,L,D,limit;
db varrive;
db calc_tim_1() {
	db ret=0;
	if(v<limit) {
//		cerr<<"v<limit"<<endl;
//		cerr<<sqrt(2.0*a*D)<<endl;
		if(sqrt(2.0*a*D)>v) {
//			cerr<<"end with v"<<endl;
			db x1=1.0*v*v/2.0/a,t1=v/a;
			db x2=D-x1,t2=1.0*x2/v;
//			cerr<<x1<<" "<<t1<<" "<<x2<<" "<<t2<<endl;
			ret=t1+t2;
			varrive=v;
		} else {
//			cerr<<"end with accelerate"<<endl;
			varrive=sqrt(2.0*a*D);
			ret=varrive/a;
		}
	} else {//limit<vm
//		cerr<<"limit<vm"<<endl;
		if(sqrt(2.0*a*D)>=limit) {
//			cerr<<"end with limit"<<endl;
			varrive=limit;
			db v_peak=sqrt(limit*limit/2.0+a*D);
			if(v_peak<=v) {//ɽ�� 
//			cerr<<"peak "<<v_peak<<endl;
				ret=v_peak/a+(v_peak-limit)/a;
			} else {//��ԭ 
//			cerr<<"plateau "<<endl; 
				db t_mid=(D-v*v/2.0/a-(v*v-limit*limit)/2.0/a )/v; 
				ret=t_mid+v/a+(v-limit)/a;
			}
		} else {//varrive<limit<v
			varrive=sqrt(2.0*a*D);
			ret=varrive/a;
		}		
	}
//	cerr<<"tim1="<<ret<<endl;
	return ret;
}
db calc_tim_2() {
	db ret=0;
	db ideal_arrive_v=sqrt(varrive*varrive+2.0*a*(L-D));
//	cerr<<varrive<<" "<<ideal_arrive_v<<endl;
	if(ideal_arrive_v>v) {
		db x1=(v*v-varrive*varrive)/2.0/a,t1=(v-varrive)/a;
		db x2=(L-D)-x1,t2=x2/v;
		ret=t1+t2;
//			cerr<<x1<<" "<<t1<<" "<<x2<<" "<<t2<<endl;
	} else {
		ret=(ideal_arrive_v-varrive)/a;
	}
//	cerr<<"tim2="<<ret<<endl;
	return ret;
}
//main========================
int main() {
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a=rd(),v=rd(),L=rd(),D=rd(),limit=rd();
	db tim1=calc_tim_1();
	db tim2=calc_tim_2();
	db ans=tim1+tim2;
	printf("%.15lf\n",ans);
	return 0;
}

