#pragma GCC optimize 2
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstring>
#include <cctype>
#include <limits>
#include <algorithm>
#include <functional>
#include <numeric>
#include <bitset>
#include <cassert>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
  	isneg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
  	x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
inline char RC() {
	char ch;
	for (ch = NC(); isspace(ch); ch = NC())
		;
	return ch;
}
}

using IO::RI;
using IO::RC;

const int kMaxK = 1e5, kMaxN = 505;

int N, M, d, K, q;
bool f[kMaxK][kMaxN];
struct Wall {
	int xl, xr, y, dir;
	inline bool operator<(const Wall &other) const {
		return y < other.y;
	}
	inline Wall Next(int t) {
		Wall ret;
		t %= 2 * (N - d);
		ret = *this;
		if (d == N)
			return ret;
		while (t) {
			if (ret.xr == N)
				ret.dir = -1;
			if (ret.xl == 1)
				ret.dir = 1;
			if (ret.dir > 0) {
				int o = min(t, N - ret.xr);
				ret.xl += o;
				ret.xr += o;
				t -= o;
			} else {
				int o = min(t, ret.xl - 1);
				ret.xl -= o;
				ret.xr -= o;
				t -= o;
			}
		}
		return ret;
	}
} c[kMaxK], b[kMaxK];

// 10 + 15

int main() {
  freopen("wall.in", "r", stdin);
  freopen("wall.out", "w", stdout);
  RI(N), RI(M), RI(d), RI(K);
  for (int i = 1; i <= K; ++i) {
  	RI(c[i].xl), RI(c[i].y);
  	char opt = RC();
  	c[i].xr = c[i].xl + d - 1;
  	c[i].dir = (opt == '+') ? 1 : -1;
  }
  sort(c + 1, c + K + 1);
  RI(q);
  if (N == 2 && d == 1) {
  	for (int i = 1; i <= q; ++i)
  		puts("1");
  	return 0;
  }
  while (q--) { // O(Q \times N \times d ^ 2)
  	int x, y;
  	RI(x), RI(y);
  	// fprintf(stderr, ":: Processing %d %d\n", x, y);
  	int kkk = 0;
  	b[0].xl = x, b[0].xr = x, b[0].y = y;
  	for (int j = 1; j <= K; ++j) {
  		if (c[j].y <= y)
  			continue;
  		b[++kkk] = c[j].Next(c[j].y - y);
  		// fprintf(stderr, "(%d, %d) %d\n", b[j].xl, b[j].xr, b[j].y);
  	}
  	memset(f, 0x00, sizeof f);
		f[0][x] = true;
		++kkk;
		b[kkk].xl = 1, b[kkk].xr = N;
		b[kkk].y = M + 1;
		for (int j = 1; j <= kkk; ++j) {
			for (int lp = b[j - 1].xl; lp <= b[j - 1].xr; ++lp) {
				if (f[j - 1][lp]) {
					for (int p = b[j].xl; p <= b[j].xr; ++p) {
						if (b[j].y - b[j - 1].y >= abs(p - lp)) {
							f[j][p] = true;
						}	
					}
				}
			}
		}
		bool can = false;
		for (int j = 1; j <= N; ++j)
			can |= f[kkk][j];
		puts(can ? "1" : "0");
  }
  return 0;
}
