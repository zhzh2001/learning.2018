#pragma GCC optimize 2
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstring>
#include <cctype>
#include <limits>
#include <algorithm>
#include <functional>
#include <numeric>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
  	isneg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
  	x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
}

using IO::RI;

const int kMaxN = 5e5 + 5, kInf = 0x3f3f3f3f;

int n, lim;
struct Edg { int x, y, a, b; } c[kMaxN];

namespace SubTask1 {

int h[105], ecnt, d1[105], d2[105], pos, mx, ans = kInf;
struct Edge { int to, nxt, w; } e[kMaxN];

inline void AddEdge(int u, int v, int w) {
	e[++ecnt] = (Edge) { v, h[u], w };
	h[u] = ecnt;
}

void DFS1(int x, int f = 0) {
	if (d1[x] > mx) {
		pos = x;
		mx = d1[x];
	}
	for (int i = h[x]; i; i = e[i].nxt) {
		if (e[i].to == f)
			continue;
		d1[e[i].to] = d1[x] + e[i].w;
		DFS1(e[i].to, x);
	}
}

void DFS2(int x, int f = 0) {
	if (d2[x] > mx) {
		pos = x;
		mx = d2[x];
	}
	for (int i = h[x]; i; i = e[i].nxt) {
		if (e[i].to == f)
			continue;
		d2[e[i].to] = d2[x] + e[i].w;
		DFS2(e[i].to, x);
	}
}

int Solve() {
	int d = -1;
	for (int delta = 0; delta <= lim; ++delta) {
		memset(h, 0x00, sizeof h);
		ecnt = 0;
		// fprintf(stderr, ":: Processing delta = %d\n", delta);
		for (int i = 2; i <= n; ++i) {
			int w = c[i].a + delta * c[i].b;
			AddEdge(c[i].x, c[i].y, w);
			AddEdge(c[i].y, c[i].x, w);
			// fprintf(stderr, "%d\n", w);
		}
		memset(d1, 0x00, sizeof d1);
		memset(d2, 0x00, sizeof d2);
		mx = 0;
		pos = -1;
		DFS1(1);
		mx = 0;
		DFS2(pos);
		// fprintf(stderr, "%d: %d\n", delta, mx);
		if (mx < 0)
			mx = 0;
		if (mx < ans) {
			ans = mx;
			d = delta;
		}
	}
	printf("%d\n%d", d, ans);
	return 0;
}

}

int main() {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);
  RI(n), RI(lim);
  for (int i = 2; i <= n; ++i) {
  	RI(c[i].x), RI(c[i].y), RI(c[i].a), RI(c[i].b);
  }
  if (n <= 100 && lim <= 500000) return SubTask1::Solve(); // 40 pts
  return 0;
}
