#!/bin/bash
if [ -d tmp ]; then
	rm tmp -rf
fi
mkdir tmp
g++ $1.cpp -o tmp/tmp.exe
for i in {1..100}
do
  if [ -e samples/$i.in ]; then
	cp "samples/$i.in" "tmp/$1.in"
	cd tmp
	./tmp.exe
	cd ..
	diff tmp/$1.out samples/$i.out
  fi
done