#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstring>
#include <cctype>
#include <cmath>
#include <limits>
#include <algorithm>
#include <functional>
#include <numeric>
using namespace std;

const double eps = 1e-10;

int a, vmx, L, d, lim;
double ans = .0;

int main() {
  freopen("expedition.in", "r", stdin);
  freopen("expedition.out", "w", stdout);
  scanf("%d%d%d%d%d", &a, &vmx, &L, &d, &lim);
  double v1 = min(lim, vmx), t1 = .0;
  if (v1 / a * v1 / 2.0 > d) {
    // 在前半段无法加到极速，就全程加速
    v1 = sqrt(2.0 * a * d);
    t1 = v1 / a;
  } else {
    if (vmx < lim) {
      // 加速到 vmx 通过 lim
      t1 = vmx / (a * 2.0) + (double)d / vmx;
    } else {
      // 在极速范围内尽可能先加速再减速
      double low = .0, high = vmx, x11 = .0, x13 = .0, vmid = .0;
      t1 = 1e18;
      while (high - low > eps) {
        vmid = (low + high) / 2;
        x11 = (vmid * vmid) / (a * 2);
        x13 = (vmid * vmid - v1 * v1) / (a * 2);
        // fprintf(stderr, ":: mid = %lf, x11 = %lf, x13 = %lf\n", vmid, x11, x13);
        if (x11 < 0 || x13 < 0) {
          low = vmid;
        } else if (x11 + x13 > d) {
          high = vmid;
        } else low = vmid;
      }
      // fprintf(stderr, "vmid = %lf\n", vmid);
      t1 = vmid / a + (vmid - v1) / a + (d - x11 - x13) / vmid;
    }
  }
  double v2 = min((double)vmx, (double)sqrt(2.0 * a * (L - d) + v1 * v1)), t2 = (v2 - v1) / a + (double)(L - d - (v2 + v1) / 2 * (v2 - v1) / a) / v2;
  // fprintf(stderr, "%lf %lf\n%lf %lf\n", v1, t1, v2, t2);
  printf("%.13lf\n", t1 + t2);
  return 0;
}
