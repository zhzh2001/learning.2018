#include <bits/stdc++.h>
using namespace std;
double a,vmax,zhong,kou,limit;
signed main() {
	 freopen("expedition.in","r",stdin);
	 freopen("expedition.out","w",stdout);
	cin>>a>>vmax>>zhong>>kou>>limit;

	/*速度到达limit的最小时间*/
	double limit_t=(limit-0)/a;

	/*速度到达vmax的最小时间*/
	double vmax_t=(vmax-0)/a;

	double t_kou=0;
	
	
	if(limit_t<vmax_t) {
		/*速度第一次到达Limit时的距离*/
		double limit_at=0*limit_t+a*limit_t*limit_t/2;
		
		double dis1=(kou-limit_at)/2;
 		
		/*1/2a*t^2+limit*t+dis1=0*/
		double t1=(-2*limit+sqrt(4*limit*limit+8*a*dis1))/(2*a);
		

		double v_that=limit+a*t1;
		if(v_that>vmax) {
			double t_x=(vmax*vmax-limit*limit)/(2*a);
			double tt=(vmax-limit)/a;
			t_kou=tt*2+(kou-limit_at-t_x*2)/vmax;
			t_kou+=limit_t;
		}

		/*到达关口的时间*/
		else t_kou=t1*2+limit_t;

		/*从limit到达vmax的最小时间*/
		double t_vmax=(vmax-limit)/a;

		/*用时间能行驶的距离*/
		double vmax_dis=limit*t_vmax+(a*t_vmax*t_vmax)/2;

		double ans=t_kou;


		if(kou+vmax_dis>zhong) {//加不到最大速度
			ans+=(-2*limit+sqrt(limit*limit*4+8*a*(zhong-kou)))/(2*a);
			printf("%.12lf",ans);
			return 0;
		} else {
			ans+=t_vmax;
			ans+=(zhong-kou-vmax_dis)/vmax;
			printf("%.12lf",ans);
			return 0;
		}
	} else {
		double vmax_at=0*vmax_t+a*vmax_t*vmax_t/2;
		double ans=vmax_t;
		ans+=(zhong-vmax_at)/vmax;	
		printf("%.12lf",ans);
		return 0;
	}
}
