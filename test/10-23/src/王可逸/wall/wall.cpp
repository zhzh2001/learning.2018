#include <bits/stdc++.h>
#define int long long
using namespace std;
const int N=120000;
struct DOOR {int x,y,toward;} door[N];
bool cmp(DOOR a,DOOR b) {return a.y<b.y;}
int n,m,d,k,q;
bool vis[230000];
inline int read(){
    int x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int dfs(int at,int oldx,int oldy,int time) {
	/*time表示还有几步到达这扇门*/
//	cout<<"dfs:"<<at<<" "<<oldx<<" "<<oldy<<" "<<time<<'\n';
	if(at>k) return true;
	int time1=time;
	time%=(2*n-2);
	if(door[at].toward==1) {
		if(time<=door[at].x+d-2) {
			for(int i=door[at].x-time,cnt=1;cnt<=d;i--,cnt++)
				if(abs(i-oldx)<=time1)  {
					if(dfs(at+1,i,door[at].y,door[at+1].y-door[at].y)==true) return true;	
				}
		}
		if(time>door[at].x+d-2 && time<=door[at].x+n-2) {
			for(int i=door[at].x+d-1-(time-(door[at].x-d-2)),cnt=1;cnt<=d;i++,cnt++) {
				if(abs(i-oldx)<=time1) {
					if(dfs(at+1,i,door[at].y,door[at+1].y-door[at].y)==true) return true;
				}
			}
		}
		if(time>n+door[at].x-2) {
			for(int i=n-(time-(n-door[at].x-2)),cnt=1;cnt<=d;i--,cnt++) {
				if(abs(i-oldx)<=time1) {
					if(dfs(at+1,i,door[at].y,door[at+1].y-door[at].y)==true) return true;
				}
			}
		}
	} else {
		if(time<=n-(door[at].x+d-1)) {
			for(int i=door[at].x+time,cnt=1;cnt<=d;i++,cnt++) {
				if(abs(i-oldx)<=time1) {
					if(dfs(at+1,i,door[at].y,door[at+1].y-door[at].y)==true) return true;
				}
			}
		}
		if(time>n-(door[at].x+d-1) && time<=(n*2-door[at].x-2*d+1)) {
			for(int i=n-(time-(n-(door[at].x+d-1))),cnt=1;cnt<=d;i--,cnt++) {
				if(abs(i-oldx)<=time1) {
					if(dfs(at+1,i,door[at].y,door[at+1].y-door[at].y)==true) return true;
				}
			}
		}
		if(time>(n*2-door[at].x-2*d+1)) {
			for(int i=1+(time-(n*2-door[at].x-2*d+1)),cnt=1;cnt<=d;i++,cnt++) {
				if(abs(i-oldx)<=time1) {
					if(dfs(at+1,i,door[at].y,door[at+1].y-door[at].y)==true) return true;
				}
			}
		}
	}
	return 0;
}
int ok(int x,int y) {
//	cout<<"ok:"<<x<<" "<<y<<'\n';
	for(int i=1;i<=k;i++)
		if(door[i].y>y) {
			if(dfs(i,x,y,door[i].y-y)) return true;	
			else return false;
		}
	return false;
}
signed main() {
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);	
	n=read();m=read();d=read();
	k=read();
	for(int i=1;i<=k;i++) {
		door[i].x=read();
		door[i].y=read();
		char ch;
		cin>>ch;
		door[i].toward=(ch=='+'?1:0);
	}
	sort(door+1,door+k+1,cmp);
	q=read();
	for(int i=1;i<=q;i++) {
		int x,y;
		cin>>x>>y;
		int w=ok(x,y);
		if(w==1) puts("1");
		else puts("0");
	}
	return 0;		
}
