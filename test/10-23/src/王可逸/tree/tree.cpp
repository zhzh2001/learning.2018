#include <bits/stdc++.h>
#define int long long
using namespace std;
const int N=230000;
int n,lim,cnt,mx=0x3f3f3f3f,mxat,ma,maat;
int head[5600000];
struct node {int next,to;} sxd[5600000];
map <pair<int,int> ,int> a;
map <pair<int,int> ,int> b;
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	head[u]=cnt;
}
inline int read(){
    int x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
void dfs(int at,int fa,int dis,int change) {
	if(dis>ma) {
		ma=dis;
		maat=at;	
	}
	for(int i=head[at];i;i=sxd[i].next){
		if(sxd[i].to==fa) continue;
		dfs(sxd[i].to,at,dis+a[make_pair(at,sxd[i].to)]+b[make_pair(at,sxd[i].to)]*change,change);
	}
}
signed main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);	
	n=read();lim=read();
	int tim=clock();
	for(int i=1;i<n;i++) {
		int xx,yy,aa,bb;
		xx=read();yy=read();aa=read();bb=read();
		a[make_pair(xx,yy)]=aa;
		a[make_pair(yy,xx)]=aa;
		b[make_pair(xx,yy)]=bb;
		b[make_pair(yy,xx)]=bb;
		add(xx,yy);
		add(yy,xx);
	} 
	for(int i=0;i<=lim;i++) {
		maat=0;
		ma=0;
		dfs(1,0,0,i);
		int xx=maat;
		maat=0;
		ma=0;
		dfs(xx,0,0,i);
		if(ma<mx) {
			mx=ma;
			mxat=i;	 
		}
		if(clock()-tim>2900) {cout<<mxat<<'\n'<<mx;return 0;}
	}
	cout<<mxat<<'\n';
	cout<<mx;
	return 0;	
}
