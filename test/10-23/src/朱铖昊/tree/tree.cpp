#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=250005;
int n,lim,u,v,x,y,val,pos;
ll ans,len,f[N];
inline void read(int &x)
{
	char c=getchar();
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
struct edge
{
	int v,a,b;
};
vector<edge> e[N];
inline void dfs(int x,int y)
{
	ll p=0;
	f[x]=0;
	for (unsigned i=0;i<e[x].size();++i)
		if (e[x][i].v!=y)
		{
			dfs(e[x][i].v,x);
			ll q=f[e[x][i].v]+e[x][i].a+e[x][i].b*val;
			if (q>f[x])
			{
				p=f[x];
				f[x]=q;
			}
			else
			{
				if (q>p)
					p=q;
			}
		}
	len=max(len,p+f[x]);
}
inline ll work(int p)
{
	val=p;
	len=0;
	dfs(1,0);
	return len;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(n);
	read(lim);
	for (int i=1;i<n;++i)
	{
		read(u);
		read(v);
		read(x);
		read(y);
		e[u].push_back((edge){v,x,y});
		e[v].push_back((edge){u,x,y});
	}
	int l=0,r=lim;
	ans=1LL<<60;
	while (l<r)
	{
		int p=(r-l)/3;
		int lmid=l+p,rmid=r-p;
		ll lans=work(lmid),rans=work(rmid);
		if (lans<=rans)
		{
			if (lans==ans&&lmid<pos)
				pos=lmid;
			if (lans<ans)
			{
				ans=lans;
				pos=lmid;
			}
			r=rmid-1;
		}
		else
		{
			if (rans==ans&&rmid<pos)
				pos=rmid;
			if (rans<ans)
			{
				ans=rans;
				pos=rmid;
			}
			l=lmid+1;
		}
	}
	printf("%d\n%lld",pos,ans);
	return 0;
}