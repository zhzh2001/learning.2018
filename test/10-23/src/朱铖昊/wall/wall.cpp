#include<bits/stdc++.h>
using namespace std;
const int N=100005;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
struct wall
{
	int x,y;
	bool operator<(const wall &a)const
	{
		return y<a.y;
	}
}a[N];
int n,m,d,K,q,x,y;
inline int f(int x)
{
	return x==0?2:(x<=n?x:n*2-x);
}
inline int work(int x,int y)
{
	int l=x,r=x;
	int p=1;
	while (a[p].y<=y&&p<=y)
		++p;
	if (p>K)
		return 1;
	l-=(a[p].y-y);
	r+=(a[p].y-y);
	l=max(l,f((a[p].x+a[p].y-y)%(n*2-2)));
	r=min(r,f((a[p].x+a[p].y-y)%(n*2-2))+d-1);
	if (r<l)
		return 0;
	for (int i=p+1;i<=K;++i)
	{
		l-=(a[i].y-a[i-1].y);
		r+=(a[i].y-a[i-1].y);
		l=max(l,f((a[i].x+a[i].y-y)%(n*2-2)));
		r=min(r,f((a[i].x+a[i].y-y)%(n*2-2))+d-1);
		if (r<l)
			return 0;
	}
	return 1;
}
int main()
{
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	read(n);
	read(m);
	read(d);
	n-=d-1;
	read(K);
	for (int i=1;i<=K;++i)
	{
		read(a[i].x);
		read(a[i].y);
		char c=getchar();
		while (c!='+'&&c!='-')
			c=getchar();
		if (c=='-'&&a[i].x!=1)
			a[i].x=2*n-a[i].x;
	}
	sort(a+1,a+1+K);
	read(q);
	if (n<=2)
	{
		for (int i=1;i<=q;++i)
			puts("1");
		return 0;
	}
	while (q--)
	{
		read(x);
		read(y);
		puts(work(x,y)?"1":"0");
	}
	return 0;
}