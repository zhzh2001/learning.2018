#include<bits/stdc++.h>
using namespace std;
#define db double
int a,L,d,p,v;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline db work(db v0,int v1,int a,int L)
{
	db t0=((db)v1-v0)/(db)a;
	db x0=t0*(v0+(db)v1)/2.0;
	if (x0<L)
		return t0+((db)L-x0)/(db)v1;
	double det=v0*v0+2.0*(db)a*(db)L;
	t0=(sqrt(det)-v0)/(db)a;
	return t0;
}
int main()
{
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	read(a);
	read(v);
	read(L);
	read(d);
	read(p);
	p=min(v,p);
	db t0=(db)p/(db)a;
	db x0=(db)p*t0/2.0;
	if (x0>d)
	{
		t0=sqrt(2.0*(db)d/(db)a);
		printf("%.12lf",t0+work(t0*(db)a,v,a,L-d));
		return 0;
	}
	db t1=(db)(v-p)/(db)a;
	db x1=t1*(db)(p+v)/2.0;
	if (x0+x1>d)
	{
		t1=sqrt((x0+(db)d)/(db)a);
		printf("%.12lf",t1*2-t0+work((db)p,v,a,L-d));
		return 0;
	}
	db t2=((db)d-x0-x1*2.0)/(db)v;
	printf("%.12lf",t0+t1*2+t2+work((db)p,v,a,L-d));
	return 0;
}