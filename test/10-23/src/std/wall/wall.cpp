#include<fstream>
#include<vector>
#include<algorithm>
using namespace std;
ifstream fin("wall.in");
ofstream fout("wall.out");
const int N=505,K=100005;
struct door
{
	int x,y,dir;
	bool operator<(const door& rhs)const
	{
		return y<rhs.y;
	}
}D[K];
struct quest
{
	int x,y,id;
	bool operator<(const quest& rhs)const
	{
		return y<rhs.y;
	}
};
vector<quest> Q[N*2];
int pos[N*2];
bool ans[K];
int main()
{
	int n,m,d,k;
	fin>>n>>m>>d>>k;
	for(int i=1;i<=k;i++)
	{
		char dir;
		fin>>D[i].x>>D[i].y>>dir;
		D[i].dir=dir=='+'?1:-1;
	}
	sort(D+1,D+k+1);
	int q;
	fin>>q;
	if(n==d)
	{
		for(int i=1;i<=q;i++)
			fout<<"1\n";
		return 0;
	}
	int len=(n-d)*2;
	for(int i=1;i<=q;i++)
	{
		int x,y;
		fin>>x>>y;
		Q[(m-y)%len].push_back({x,y,i});
	}
	pos[0]=1;
	for(int i=1,j=1;i<len;i++)
	{
		pos[i]=pos[i-1]+j;
		if(j==1&&pos[i]+d-1==n)
			j=-1;
		if(j==-1&&pos[i]==1)
			j=1;
	}
	for(int i=0;i<len;i++)
		if(Q[i].size())
		{
			sort(Q[i].begin(),Q[i].end());
			int l=1,r=n,pred=m+1,di=k,qi=Q[i].size()-1;
			while(~qi)
				if(di&&D[di].y>=Q[i][qi].y)
				{
					l=max(l-(pred-D[di].y),1);
					r=min(r+(pred-D[di].y),n);
					int tim=i-(m-D[di].y);
					int nl=pos[((D[di].x+D[di].dir*tim-1)%len+len)%len],nr=nl+d-1;
					l=max(l,nl);
					r=min(r,nr);
					if(l>r)
						break;
					pred=D[di--].y;
				}
				else
				{
					l=max(l-(pred-Q[i][qi].y),1);
					r=min(r+(pred-Q[i][qi].y),n);
					ans[Q[i][qi].id]=Q[i][qi].x>=l&&Q[i][qi].x<=r;
					pred=Q[i][qi--].y;
				}
		}
	for(int i=1;i<=q;i++)
		fout<<ans[i]<<'\n';
	return 0;
}
