#include<fstream>
#include<set>
using namespace std;
ifstream fin("input(7).txt");
ofstream fout("test.out");
int main()
{
	int n,m,d,k;
	fin>>n>>m>>d>>k;
	set<int> S;
	while(k--)
	{
		int x,y;
		char dir;
		fin>>x>>y>>dir;
		S.insert(y);
	}
	int q;
	fin>>q;
	while(q--)
	{
		int x,y;
		fin>>x>>y;
		if(S.find(y)!=S.end())
			fout<<"duplicated\n";
	}
	return 0;
}
