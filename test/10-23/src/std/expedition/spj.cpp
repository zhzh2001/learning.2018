#include<cstdio>
#include<cmath>
using namespace std;
const double eps=1e-6;
int main(int argc,char* argv[])
{
	if(argc<4)
		return 1;
	FILE *fans=fopen(argv[2],"r"),*fout=fopen(argv[3],"r"),*fres=argc>4?fopen(argv[4],"w"):stdout;
	double ans,out;
	fscanf(fans,"%lf",&ans);
	fscanf(fout,"%lf",&out);
	if(fabs(ans-out)<eps)
		fprintf(fres,"1.0\nAccepted\n");
	else
		fprintf(fres,"0.0\nWrong Answer\n");
	return 0;
}
