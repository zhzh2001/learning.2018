#include<bits/stdc++.h>

#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define eb emplace_back

using namespace std;

typedef pair<int, int> pii;
typedef long long ll;

const int N = 312345;
ll dp[N][2];
ll a[N], b[N];
vector <pii> g[N];
void solve (int u, int pu, int day) {
    dp[u][0] = 0;
    dp[u][1] = 0;
    vector <ll> w;
    for (auto q : g[u]) {
        int v, id; tie(v, id) = q;
        if (v == pu) continue;
		solve(v, u, day);
        dp[u][0] = max(dp[v][0], dp[u][0]);
        w.pb(dp[v][1] + a[id] + b[id] * day);
    }
   	if (w.size() >= 1) {
		nth_element(w.begin(), w.begin(), w.end(), greater<ll>());
		dp[u][0] = max(dp[u][0], w[0]);
		dp[u][1] = max(0LL, w[0]);
	}
	if (w.size() >= 2) {
		nth_element(w.begin(), w.begin()+1, w.end(), greater<ll>());
		dp[u][0] = max(dp[u][0], dp[u][1] + w[1]);
	}
}

int main() {
	freopen("input6.txt","r",stdin);
	freopen("output666.txt","w",stdout);
    int n, d;
    cin >> n >> d;
    for (int i = 1; i < n; i++) {
        int u, v, a, b; scanf("%d %d %d %d", &u, &v, &a, &b);
        ::a[i] = a;
        ::b[i] = b;
        g[u].pb({v, i});
        g[v].pb({u, i});
    }
    int lo = 0, hi = d;
	ll ans = -1;
    while (hi - lo > 6) {
        int mid1 = lo + (hi - lo) / 3;
        int mid2 = hi - (hi - lo) / 3;
        
		solve(1, 0, mid1);
		ll v1 = dp[1][0];
		solve(1, 0, mid2);
		ll v2 = dp[1][0];

		if (v1 > v2) {
			lo = mid1;
		} else {
			hi = mid2;
		}
    }
	solve(1, 0, lo);
	int best = lo;
	ans = dp[1][0];
	for (int x = lo+1; x <= hi; x++) {
		solve(1, 0, x);
		if (dp[1][0] < ans) {
			ans = dp[1][0];
			best = x;
		}
	}
	cout << best << endl;
	cout << ans << endl;
    return 0;

}
