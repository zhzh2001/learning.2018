#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;
	#define gc getchar
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=0;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return f?x:-x;
}
const int N=250005;
struct edge{
	int to,next,a,b;
}e[N*2];
int head[N],tot,n; 
void add(int x,int y,int a,int b){
	e[++tot]=(edge){y,head[x],a,b};
	head[x]=tot;
}
int A[N],B[N],fa[N],q[N];
void bfs_init(){
	int h=0,t=1; q[1]=1;
	while (h!=t){
		int x=q[++h];
		for (int i=head[x];i;i=e[i].next)
			if (e[i].to!=fa[x]){
				fa[e[i].to]=x;
				A[e[i].to]=e[i].a;
				B[e[i].to]=e[i].b;
				q[++t]=e[i].to;
			} 
	}
}
ll mx1[N],mx2[N];
ll calc(ll v){
	for (int i=1;i<=n;i++)
		mx1[i]=0,mx2[i]=-(1ll<<60);
	ll ans=0;
	for (int i=n;i>1;i--){
		int x=q[i];
		ans=max(ans,max(mx1[x],mx1[x]+mx2[x]));
		ll val=mx1[x]+A[x]+1ll*B[x]*v;
		if (val>=mx1[fa[x]])
			mx2[fa[x]]=mx1[fa[x]],mx1[fa[x]]=val;
		else mx2[fa[x]]=max(mx2[fa[x]],val);
	}
	return max(ans,max(mx1[1],mx1[q[1]]+mx2[q[1]]));
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout); 
	n=read();
	int l=0,r=read()-1;
	for (int i=1;i<n;i++){
		int x=read(),y=read(),a=read(),b=read();
		add(x,y,a,b); add(y,x,a,b);
	}
	bfs_init();
	int ans=r+1,lim=ans;
	while (l<=r){
		int mid=(l+r)/2;
		if (calc(mid)<=calc(mid+1))
			ans=mid,r=mid-1;
		else l=mid+1;
	}
	ll v0=calc(ans),v1=calc(ans+1);
	if (v0>v1&&ans!=lim) ans++,v0=v1;
	printf("%d\n%lld\n",ans,v0);
}
