#include<algorithm>
#include<iostream>
#include<fstream>
#include<math.h>
#include<string.h>
#include<stdio.h>
using namespace std;
ifstream fin("expedition.in");
ofstream fout("expedition.out");
const double eps=1e-9;
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
double a,vm,l,d,li,ans;
inline double solve(double v,double a,double vm,double l){
	double ret=0.0;
	if(l==0.0){
		return 0.0;
	}
	if(2.0*a*l>=vm*vm-v*v){
		ret=(vm-v)/a+(l-(vm*vm-v*v)/a/2.0)/vm;
	}else{
		double L=0.0,R=10000.0;
		while(L+eps<=R){
			double mid=(L+R)/2.0;
			if(mid*v+mid*mid*a/2.0-l>eps){
				R=mid;
			}else{
				L=mid;
			}
		}
		ret=L;
	}
	return ret;
}
int main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a=read(),vm=read(),l=read(),d=read(),li=read();
	if(vm<=li){
		ans=solve(0.0,a,vm,l);
	}else{
		if(d*2.0<=li){
			ans=solve(0.0,a,vm,l);
		}else{
			double L=0,R=10000,vn;
			while(L+eps<=R){
				double mid=(L+R)/2;
				vn=a*mid;
				if(a*mid*mid+(vn*vn-li*li)/a<=d*2.0){
					L=mid;
				}else{
					R=mid;
				}
			}
			if(vn<=vm){
				ans=L+(vn-li)/a;
				ans+=solve(li,a,vm,l-d);
			}else{
				vn=vm;
				ans=(l-(vn*vn/a/2.0+(vn*vn-li*li)/a/2.0))/vn+vn/a+(vn-li)/a;
				ans+=solve(li,a,vm,l-d);
			}
		}
	}
	printf("%.12lf",ans);
	return 0;
}
