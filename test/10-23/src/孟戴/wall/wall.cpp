#include<algorithm>
#include<fstream>
#include<math.h>
#include<string.h>
#include<stdio.h>
#define f(i,j) F[(i-1)*mod+j]
using namespace std;
ifstream fin("wall.in");
ofstream fout("wall.out");
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
inline int max(int &a,int &b){
	if(a>b)return a;
	return b;
}
inline int min(int &a,int &b){
	if(a<b)return a;
	return b;
}
int n,m,D,K,mod;
struct ppap{
	int x,y;
	char c[2];
}a[90003];
inline bool cmp(ppap a,ppap b){
	return a.y<b.y;
}
struct qj{
	int l,r;
}F[30000003];
inline int cal(int x,int k){
	int px=a[x].x;
	if(a[x].c[0]=='+'){
		if(px+k>n-D+1){
			k-=n-D-px+1;px=n-D+1;
			if(px-k<1){
				k-=px-1;px=1;
				px+=k;
			}else{
				px-=k;
			}
		}else{
			px+=k;
		}
	}else{
		if(px-k<1){
			k-=px-1;px=1;
			if(px+k>n-D+1){
				k-=n-D-px+1;px=n-D+1;
				px-=k;
			}else{
				px+=k;
			}
		}else{
			px-=k;
		}	
	}
	return px;
}
int main(){
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	n=read(),m=read(),D=read(),K=read(),mod=max(1,2*(n-D));
	for(register int i=1;i<=K;i++){
		a[i].x=read(),a[i].y=read();
		fin>>a[i].c;
	}
	sort(a+1,a+K+1,cmp);
	for(register int i=0;i<=mod-1;i++){
		int px=cal(K,i);
		f(K,i)=(qj){px,px+D-1};
	}
	for(register int i=K-1;i;i--){
		for(register int j=0;j<=mod-1;j++){
			int px=cal(i,j),pp=a[i+1].y-a[i].y,po=(j+pp)%mod;
			if(f(i+1,po).l>n){
				f(i,j)=(qj){n+1,0};
				continue;
			}
			int lx=max(1,f(i+1,po).l-pp),rx=min(n,f(i+1,po).r+pp);
			lx=max(px,lx);rx=min(px+D-1,rx);
			if(lx>rx)lx=n+1,rx=0;
			f(i,j)=(qj){lx,rx};
		}
	}
	for(register int Q=read();Q;Q--){
		int x=read(),y=read();
		int l=1,r=K,pos=K+1,ans=0;
		while(l<=r){
			int mid=l+r>>1;
			if(a[mid].y>=y){
				pos=mid,r=mid-1;
			}else{
				l=mid+1;
			}
		}
		if(pos==K+1){
			ans=1;
		}else{
			int pp=a[pos].y-y;
			int lx=max(1,x-pp),rx=min(n,x+pp),po=pp%mod;
			if(f(pos,po).l==n+1){
				ans=0;
			}else{
				lx=max(lx,f(pos,po).l),rx=min(rx,f(pos,po).r);
				if(lx<=rx){
					ans=1;
				}
			}
		}
		writeln(ans);
	}
	return 0;
}
/*
3 10 1
2
1 2 +
2 3 +
3
1 1
2 2
3 3
*/
