#include<algorithm>
//#include<iostream>
#include<fstream>
#include<math.h>
#include<string.h>
#include<stdio.h>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
long long head[250003],nxt[500003],to[500003],cost[500003],tot;
long long dep[250003];
inline void add(int u,int v,long long w){
	to[++tot]=v,cost[tot]=w,nxt[tot]=head[u],head[u]=tot;
	to[++tot]=u,cost[tot]=w,nxt[tot]=head[v],head[v]=tot;
}
long long mx1[250003],mx2[250003],anss[250003];
void dfs(int x,int fa){
	mx1[x]=0,mx2[x]=0;
	for(int i=head[x];i;i=nxt[i]){
		if(to[i]!=fa){
			dfs(to[i],x);
			if(mx1[to[i]]+cost[i]>mx1[x]){
				mx2[x]=mx1[x],mx1[x]=mx1[to[i]]+cost[i];
			}else if(mx1[to[i]]+cost[i]>mx2[x]){
				mx2[x]=mx1[to[i]]+cost[i];
			}
		}
	}
	anss[x]=mx1[x]+mx2[x];
}
struct edge{
	long long u,v,a,b;
}e[250003];
long long n,lim;
inline long long calc(long long x){
	memset(head,0,sizeof(head));
	tot=0;
	for(register int j=1;j<=n-1;j++){
		add(e[j].u,e[j].v,e[j].a+e[j].b*x);
	}
	dfs(1,0);
	int ans=1;
	for(register int j=2;j<=n;j++){
		if(anss[j]>anss[ans]){
			ans=j;
		}
	}
	return anss[ans];
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),lim=read();
	for(register int i=1;i<=n-1;i++){
		e[i].u=read(),e[i].v=read(),e[i].a=read(),e[i].b=read();
	}
	long long l=0,r=lim;
	while(l<r-4){
		long long mid1=(r-l)/3+l,mid2=(r-l)/3*2+l;
		long long ans1=calc(mid1),ans2=calc(mid2);
		if(ans1<=ans2){
			r=mid2;
		}else{
			l=mid1;
		}
	}
	long long ansa=l,ansb=calc(l);
	for(register long long i=l+1;i<=r;i++){
		long long ans=calc(i);
		if(ans<ansb){
			ansa=i,ansb=ans;
		}
	}
	fout<<ansa<<endl<<ansb<<endl; 
	return 0;
}
