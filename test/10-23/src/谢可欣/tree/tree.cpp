#include <iostream>
#include <cstring>
#include <cstdio>
#include <queue>
using namespace std;
inline int read(){int tot=1;char c=getchar();if(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')c=getchar(),tot=-1;
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
int ans,n,pos,k;
int x[1000],y[1000],a[1000],b[1000],tmp[1000000],ru[1000000],len[1000],w[1000][1000],head[3000],nxt[3000],v[3000];
void add(int u,int y){
	nxt[++pos]=head[u];
	v[pos]=y;
	head[u]=pos;
}
void dfs(int rt,int fa){
	if(fa)len[rt]=max(len[rt],len[fa]+w[rt][fa]);
	ans=max(ans,len[rt]);
	for(int i=head[rt];i;i=nxt[i]){
		int j=v[i];
		if(j==fa)continue;
		dfs(j,rt);
	}
}
int js(){
	for(int i=1;i<=n;i++){
		memset(len,0,sizeof len);
		dfs(i,0);
	}
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),k=read();
	for(int i=1;i<n;i++){
		x[i]=read();y[i]=read();a[i]=read(),b[i]=read();
		add(x[i],y[i]);add(y[i],x[i]);
		w[x[i]][y[i]]=a[i]+k*b[i];w[y[i]][x[i]]=a[i]+k*b[i];
	}
	int mn=100000000,delta;
	for(int i=1;i<=k;i++){
		for(int j=1;j<n;j++){
			w[x[j]][y[j]]=a[j]+i*b[j];w[y[j]][x[j]]=a[j]+i*b[j];
//			cout<<w[x[j]][y[j]]<<" ";
		}
//		puts("");puts("");
		ans=0;
		js();
		if(ans<mn){
			mn=ans;
			delta=i;
		}
	}
	cout<<delta<<endl<<mn<<endl;
	return 0;
}
