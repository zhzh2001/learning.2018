#include <iostream>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();if(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
char op[10];
int n,m,d,k,flag;
int dir[10000],dor[10000];
bool check(int t,int x,int y){
	for(int i=1;i<=t;i++){
		if(!dir[y]){
			dor[y]--;
			if(dor[y]==1)dir[y]=1-dir[y];
		}
		else{
			dor[y]++;
			if(dor[y]+d-1==n)dir[y]=1-dir[y];
		}
	}
	if(x>=dor[y]&&x<=(dor[y]+d-1))return 1;
	return 0;
}
void dfs(int t,int x,int y){
//	cout<<"aa"<<t<<" "<<x<<" "<<y<<endl;
	if(flag==1)return;
	if(y==m+2){
		flag=1;return;
	}
	if(dor[y]&&!check(t,x,y))return;
//	cout<<"kk"<<check(t,x,y)<<endl;
	if(x-1>=1)dfs(t+1,x-1,y+1);
	if(x+1<=n)dfs(t+1,x+1,y+1);
	dfs(t+1,x,y+1);
}
int main()
{
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
    n=read(),m=read(),d=read(),k=read();
    for(int i=1;i<=k;i++){
    	int x=read(),y=read();cin>>op;
    	dor[y]=x;dir[y]=(op[0]=='+');
	}
	int q=read();
	while(q--){
		flag=0;
		int x=read(),y=read();
		dfs(1,x,y);
		if(flag)puts("0");
		else puts("1");
	}
	return 0;
} 
