#include<bits/stdc++.h>
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = - x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("expedition.in", "r", stdin);
	freopen("expedition.out", "w", stdout);
}
using namespace std;
 
/*
第一行输入五个正整数 a,vm ,L,d,limit，分别表示加速度，最大速度，战场位置，关
口位置，关口限制速度
*/
#define pii pair<double,double>
#define mk make_pair
#define fi first
#define nd sceond
double a, vm, L, d, limit; 
#define pii pair<double,double>
#define mk make_pair
#define fi first
#define nd sceond
pii solve(double a, double b, double c) {
	double delta = sqrt(b *b - 4 * a * c);
	double X1 = (-b - delta) / 2 / a;
	double X2 = (-b + delta) / 2 / a;
	if(X1 < -1e-10) swap(X1, X2);
	return mk(X1, X2);
}
int main() {
	cin>>a>>vm>>L>>d>>limit;
	pii res = solve(a, 4 * limit, limit * limit / a - L);
	double x = res.fi + limit / a;
	printf("%lf %lf %lf\n", x,res.fi, x + res.fi);
	return 0;
}
