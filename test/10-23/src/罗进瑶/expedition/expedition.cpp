#include<bits/stdc++.h>
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = - x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("expedition.in", "r", stdin);
	freopen("expedition.out", "w", stdout);
}
using namespace std;
 
/*
第一行输入五个正整数 a,vm ,L,d,limit，分别表示加速度，最大速度，战场位置，关
口位置，关口限制速度
*/
#define pii pair<double,double>
#define mk make_pair
#define fi first
#define nd sceond
pii solve(double a, double b, double c) {
	double delta = sqrt(b *b - 4 * a * c);
	double X1 = (-b - delta) / 2 / a;
	double X2 = (-b + delta) / 2 / a;
	if(X1 < -1e-10) swap(X1, X2);
	return mk(X1, X2);
}
double a, vm, L, d, limit; 

double check(double x) {
	double Y = a * x;
	double X2 = x - limit / a;
	double Y2 = limit;
	return (Y2 - Y) / (X2 - x);
}
int main() {
	setIO();
	cin>>a>>vm>>L>>d>>limit;
	if(limit >= vm) {
		double t = sqrt(2 * L / a);
		if(a * t <= vm) printf("%.10lf\n", t);
		else {
			double t1 = vm / a;
			double s1 = 0.5 * a * t1 * t1;
			double s2 = L - s1;
			double t2 = s2 / vm;
			printf("%.10lf\n", t1 + t2);
		}
		return 0;
	}
	pii res = solve(a / 2, 0, -L);
	if(res.fi * a <= limit) {
		double t = sqrt(2 * L / a);
		if(a * t <= vm) printf("%.10lf\n", t);
		else {
			double t1 = vm / a;
			double s1 = 0.5 * a * t1 * t1;
			double s2 = L - s1;
			double t2 = s2 / vm;
			printf("%.10lf\n", t1 + t2);
		}
		return 0;
	}
	if(d == L) {
		double t1 = vm / a;
		double s1 = 0.5 * a * t1 * t1;
		if(s1 <= d) {
			double s2 = d - s1;
			double t2 = s2 / vm;
			printf("%.10lf\n", t1 + t2);
			return 0;
		} else {
			printf("%.10lf\n", sqrt(2 * d / a));
			return 0;
		}
	}
	double l = 0, r = vm / a;
	while(r - l > 1e-6) {
		double mid = (l + r) / 2;
		if(check(mid) < -a || check(mid) > 0) r = mid;
		else l = mid;
	} 
	r = r - fabs((a * r - limit) / r);
	printf("%.10lf\n", r);
	double t = (vm - limit) / a;
	if(limit * t + 0.5 * a * t * t <= L) {
		double s1 = limit * t + 0.5 * a * t * t;
		double s2 = (L - s1 - d);
		double t2 = s2 / vm;
		printf("%.10lf\n", t + r + t2);
	} else {
		pii res = solve(a / 2, limit, -L);
		printf("%.10lf\n", res.fi + r);
	}
	return 0;
}
