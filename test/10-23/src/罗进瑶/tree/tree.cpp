#include<bits/stdc++.h>
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = - x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
}
using namespace std;
const int N = 1050000;
int n, m;
struct Edge {
	int u, v, nxt;
	ll a, b, w;
}e[N * 2];
int en, head[N];
void add(int x, int y, int a, int b) {
	e[++en].u = x, e[en].v = y,
	e[en].a = a, e[en].b = b;
	e[en].nxt = head[x];
	head[x] = en;
	e[en].w = a;
}
ll dis[N], ans;
int f[N][21], d[N];

namespace GG {
	int rt;
	void dfs(int x, int F) {
		if(dis[x] > dis[rt]) rt = x;
		for(int i = head[x]; i;i = e[i].nxt) {
			int y = e[i].v;
			if(y == F) continue;
			dis[y] = dis[x] + e[i].w;
			dfs(y, x);
		}
	}
	void main() {
		ans = 1e16;
		ll delta = 0;
		if((ll) n* m <= 100000000) {
			for(int T = 1; T <= m; ++T) {
				for(int i = 1; i <= en; ++i)
					e[i].w = e[i].a + T * e[i].b;
				rt = 1;
				dis[1] = 0;	
				dfs(1, 0);
				dis[rt] = 0;
				dfs(rt, 0);
				if(dis[rt] < ans) {
					ans = dis[rt];
					delta = T;
				}			
			}
		}
		writeln(delta);
		writeln(ans);	
	}
}
void dfs(int x, int F) {
	f[x][0] = F;
	d[x] = d[F] + 1;
	for(int i = 1; i <= 20; ++i)
		f[x][i] = f[f[x][i - 1]][i - 1];
	for(int i = head[x]; i;i = e[i].nxt) {
		int y = e[i].v;
		if(y == F) continue;
		dis[y] = dis[x] + e[i].w;
		dfs(y, x);
	}
}
void up(int x, int F){
	for(int i = head[x]; i;i = e[i].nxt) {
		int y = e[i].v;
		if(y == F) continue;
		dis[y] = dis[x] + e[i].w;
		dfs(y, x);
	}
} 
ll LCA(int x, int y) {
	if(d[x] > d[y]) swap(x, y);
	for(int i = 20; i >= 0; --i)
		if(d[f[y][i]] >= d[x]) y = f[y][i];
	if(y == x) return y;
	for(int i = 20; i >= 0;--i)
		if(f[x][i] != f[y][i]) x = f[x][i], y = f[y][i];
	return f[x][0];
}
ll getdis(int x, int y) {
	int z = LCA(x, y);
	return dis[x] + dis[y] - 2 * dis[z];
}
int main() {
	setIO();
	n = read(), m = read();
	for(int i = 1; i < n; ++i) {
		int x = read(), y = read(), a = read(), b = read();
		add(x, y, a, b);
		add(y, x, a, b);
	}
	if(n > 100) {
		GG:main();
		return 0;
	}
	int delta = 0;
	dfs(1, 0);
	for(int i = 1; i <= n; ++i)
		for(int j = i + 1; j <= n; ++j)
			ans = max(ans, getdis(i,j));
	
	ll res = 0;
	for(int i = 1; i <= en; ++i) e[i].w = e[i].a + e[i].b * m;
	dis[1] = 0;
	up(1, 0);
	for(int i = 1; i <= n; ++i)
		for(int j = i + 1; j <= n; ++j)
			res = max(res, getdis(i,j));
	if(res < ans) {
		ans = res;
		delta = m;
	}
	writeln(delta);
	writeln(ans);
	return 0;
}
