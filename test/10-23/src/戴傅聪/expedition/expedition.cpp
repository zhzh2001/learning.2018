#include<algorithm>
#include<cstdio>
#include<iostream>
#include<cmath>
#include<cassert>
using namespace std;
double a,vmax,L,d,limit;double ans,curv;
const double eps=1e-10;
inline void solve1()
{
	double l,r;
	l=0.0;r=1000000000.0;
	while(l+eps<r)
	{
		double mid=(l+r)/2;
		//check
		double tup=min(mid,vmax/a);
		double tkeep=mid-tup;double tv=a*tup;
		double dis=0.5*tv*tup+tv*tkeep;
		if(dis>d){r=mid;continue;}
		double rest=d-dis;
		double fv=sqrt(tv*tv-2.0*a*rest);
		if(fv>=limit) r=mid;
		else l=mid;
		//end of check
	}
	double tup=min(l,vmax/a);
	double tkeep=l-tup;double tv=a*tup;
	double dis=0.5*tv*tup+tv*tkeep;
	double rest=d-dis;
	double fv=sqrt(tv*tv-2.0*a*rest);
	assert(fv-eps<=limit);
	ans=l;curv=fv;
	ans+=(tv-fv)/a;
}
inline void solve2()
{
	double tup=(vmax-curv)/a;double nxtv;
	double nxtdis=tup*curv+a*tup*tup/2;
	if(nxtdis>L-d)
	{
		nxtv=sqrt(a*(L-d)*2+curv*curv);
		ans+=(nxtv-curv)/a;
	}
	else
	{
		nxtv=vmax;
		ans+=tup;
		double restdis=L-d-nxtdis;
		ans+=restdis/vmax;
	}
}
int main()
{
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	scanf("%lf%lf%lf%lf%lf",&a,&vmax,&L,&d,&limit);
	solve1();solve2();
	printf("%.15f",ans);
}
/*
3 6 10 4 4
2.759201748
*/
/*
v=v0+a*t;
x=v0*t+a*t*t/2;
v*v-v0*v0=2*a*x;
*/
/*
v*v-v0*v0=2*a*x
v*v=2*a*x+v0*v0
*/
