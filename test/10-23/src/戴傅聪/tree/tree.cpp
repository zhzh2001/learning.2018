#include<algorithm>
#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
#define maxn 251000
int head[maxn],ver[maxn<<1],nxt[maxn<<1],va[maxn<<1],vb[maxn<<1],tot;
inline void add(int a,int b,int c,int d)
{
	nxt[++tot]=head[a];ver[tot]=b;va[tot]=c;vb[tot]=d;head[a]=tot;
	nxt[++tot]=head[b];ver[tot]=a;va[tot]=c;vb[tot]=d;head[b]=tot;
}
long long f[maxn];long long len_ans;int n,lim;
inline void treedp(int x,int fat,int del)
{
	f[x]=0;long long maxv=0,smaxv=0;
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];long long v=1ll*va[i]+1ll*vb[i]*del;if(y==fat) continue;
		treedp(y,x,del);
		long long F=f[y]+v;if(F>f[x]) f[x]=F;
		if(F>maxv) smaxv=maxv,maxv=F;
		else if(F==maxv) smaxv=F;
		else if(F>smaxv) smaxv=F;
	}
	len_ans=max(len_ans,maxv+smaxv);
	len_ans=max(len_ans,f[x]);
}
inline long long getlen(int del)
{
	len_ans=0;
	treedp(1,0,del);
	return len_ans;
}
inline void init()
{
	scanf("%d%d",&n,&lim);
	for(int i=1,a,b,c,d;i<n;i++) scanf("%d%d%d%d",&a,&b,&c,&d),add(a,b,c,d);
}
int res1=1e9;long long res2=1e18;
inline void update(int x,long long Getlen)
{
	if(Getlen>res2) return;
	if(x<res1||Getlen!=res2) res1=x;
	res2=Getlen;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	init();update(lim,getlen(lim));
	int l=0,r=lim-1;
	while(l<=r)
	{
		int mid=(l+r)>>1;
		long long c1=getlen(mid),c2=getlen(mid+1);
		update(mid,c1);update(mid+1,c2);
		if(c1==c2) break;
		if(c1>c2) l=mid+1;
		if(c1<c2) r=mid-1;
	}
	l=0,r=res1;
	while(l<=r)
	{
		int mid=(l+r)>>1;
		long long c=getlen(mid);
		if(c==res2) res1=min(res1,mid),r=mid-1;
		else l=mid+1;
	}
	printf("%d\n%lld",res1,res2);
}
