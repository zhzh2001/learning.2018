#include<cstdio>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
int n,m,d,K;
inline void init(){
	n=read(); m=read(); d=read(); K=read();
}
inline void solve(){
	for (int i=1;i<=K;i++){
		int x=read(),y=read(),opt; char s[20];
		scanf("%s",s+1);
		if (s[1]=='+') opt=1; else opt=0;
	}
	int q=read();
	for (int i=1;i<=q;i++){
		int x=read(),y=read();
		if (!d) writeln(0);
			else writeln(1);
	}
}
int main(){
	freopen("wall.in","r",stdin); freopen("wall.out","w",stdout);
	init(); solve();
	return 0;
}
