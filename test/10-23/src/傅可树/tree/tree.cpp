#include<cstdio>
#include<algorithm>
#include<cstring>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int INF=1e18,N=250005;
struct Edge{
	int u,v,x,y;
}E[N];
struct edge{
	int link,next,val;
}e[N<<1];
int res,id,mx,dp[N][2],ans,n,lim,tot,head[N];
inline void init(){
	n=read(); lim=read();
	for (int i=1;i<n;i++){
		int u=read(),v=read(),x=read(),y=read();
		E[i]=(Edge){u,v,x,y};
	}
}
int q[5];
inline int cmp(int x,int y){
	return x>y;
}
inline void merge(int u,int v){
	q[0]=dp[u][0]; q[1]=dp[u][1]; q[2]=v;
	sort(q,q+3,cmp);
	dp[u][0]=q[0]; dp[u][1]=q[1];
}
void dfs(int u,int fa){
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u);
			merge(u,dp[v][0]+e[i].val);
		}
	}
	res=max(dp[u][0]+dp[u][1],res);
}
inline void add_edge(int u,int v,int w){
	e[++tot]=(edge){v,head[u],w}; head[u]=tot;
}
inline void insert(int u,int v,int w){
	add_edge(u,v,w); add_edge(v,u,w);
}
inline int work(int delta){
	memset(head,0,sizeof head); tot=0;
	for (int i=1;i<n;i++){
		insert(E[i].u,E[i].v,E[i].x+E[i].y*delta);
	}
	memset(dp,0,sizeof dp); res=0;
	dfs(1,0);
	if (res<ans) ans=res,id=delta;
		else if (res==ans) id=min(id,delta);
	return res;
}
inline void solve(){
	int l=0,r=lim; ans=INF; work(0); work(lim);
	while (l+10<r){
		int tmp=(r-l+1)/3;
		int mid1=l+tmp,mid2=r-tmp,mid3=(l+r)>>1;
		int tmp1=work(mid1),tmp2=work(mid3),tmp3=work(mid2);
		if (tmp1==tmp2&&tmp2==tmp3) r=l;
		if (tmp1>=tmp2) l=mid1;
		if (tmp3>=tmp2) r=mid2;
	}
	for (int i=l;i<=r;i++){
		work(i);
	}
	writeln(id);
	writeln(ans);
}
signed main(){
	freopen("tree.in","r",stdin); freopen("tree.out","w",stdout);
	init(); solve();
	return 0;
}
