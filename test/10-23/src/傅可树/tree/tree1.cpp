#include<cstdio>
#include<algorithm>
#include<cstring>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int INF=1e18,N=250005;
struct Edge{
	int u,v,x,y;
}E[N];
struct edge{
	int link,next,val;
}e[N<<1];
int id,mx,dis[N],ans,n,lim,tot,head[N];
inline void init(){
	n=read(); lim=read();
	for (int i=1;i<n;i++){
		int u=read(),v=read(),x=read(),y=read();
		E[i]=(Edge){u,v,x,y};
	}
}
void dfs(int u,int fa){
	if (dis[u]>dis[mx]) mx=u; 
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dis[v]=dis[u]+e[i].val;
			dfs(v,u);
		}
	}
}
inline void add_edge(int u,int v,int w){
	e[++tot]=(edge){v,head[u],w}; head[u]=tot;
}
inline void insert(int u,int v,int w){
	add_edge(u,v,w); add_edge(v,u,w);
}
inline int work(int delta){
	memset(head,0,sizeof head); tot=0;
	for (int i=1;i<n;i++){
		insert(E[i].u,E[i].v,E[i].x+E[i].y*delta);
	}
	mx=1;
	for (int i=1;i<=n;i++) dis[i]=0;
	dfs(1,0);
	for (int i=1;i<=n;i++) dis[i]=0;
	dfs(mx,0); 
	int tmp=max(0ll,dis[mx]);
	if (tmp<ans) ans=tmp,id=delta;
		else if (tmp==ans) id=min(id,delta);
	return tmp;
}
inline void solve(){
	int l=0,r=lim; ans=INF; work(0); work(lim);
	while (l+10<r){
		int tmp=(r-l+1)/3;
		int mid1=l+tmp,mid2=r-tmp,mid3=(l+r)>>1;
		int tmp1=work(mid1),tmp2=work(mid3),tmp3=work(mid2);
		if (tmp1>=tmp2) l=mid1;
		if (tmp3>=tmp2) r=mid2;
	}
	for (int i=l;i<=r;i++){
		work(i);
	}
	writeln(id);
	writeln(ans);
}
signed main(){
	freopen("tree.in","r",stdin); freopen("tree.out","w",stdout);
	init(); solve();
	return 0;
}
