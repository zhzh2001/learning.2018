#include<bits/stdc++.h>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const double eps=1e-10;
double a,vm,L,d,limit;
double v0,V,T,ans,x1,x2;
inline void init(){
	a=read(); vm=read(); L=read(); d=read(); limit=read();
}
inline double sqr(double x) {
	return x*x;
}
inline void sol(double A,double B,double C){
	double delta=sqr(B)-4*A*C;
	x1=1.0*(-B-sqrt(delta))/(2*A),x2=1.0*(-B+sqrt(delta))/(2*A);
}
inline double dis(double v0,double t,double a){
	return v0*t+0.5*a*sqr(t);
}
inline bool check(double t){
	double v=v0+1.0*a*t;
	if (v>vm) return 0;
	double delta=v-limit,s2=dis(v0,t,a);
	if (s2>d) return 0;
	if (delta<0) {
		T=t+(d-s2)/v; V=v;
		return 1;
	}
	double t1=1.0*delta/a,s=dis(v0,t,a)+dis(v,t1,-a);
	if (s>d) return 0;
	T=t1+t+1.0*(d-s)/v; V=limit;
	return 1;
}
inline void solve(){
	double l=0,r=10000000; v0=0;
	while (l+eps<r){
		double mid=1.0*(l+r)/2;
		if (check(mid)){
			l=mid;
		}else{
			r=mid;
		}
	}
	ans+=T; v0=V; d=L-d; limit=vm;
	l=-1,r=10000000;
	while (l+eps<r){
		double mid=1.0*(l+r)/2;
		if (check(mid)){
			l=mid;
		}else{
			r=mid;
		}
	}
	ans+=T;
	printf("%.10lf\n",ans);
}
int main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	init(); solve();
	return 0;
}
