/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 10000000000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (register int i=(a);i<=(b);i++)
#define D(i,a,b) for (register int i=(a);i>=(b);i--)
#define go(i,t) for (register int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(ll x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(ll x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,l,r,lim,l1,l2;
ll dis[N],f[N],lon1;
struct xx{
	int x,y,a,b;
}z[N];
ll ans;
int Next[N*2],nedge,head[N],to[N*2],lon[N*2];
#define V to[i]
void add(int a,int b,int c){
	Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;lon[nedge]=c;
}
void add_ne(int x,int y,int v){
	add(x,y,v);add(y,x,v);
}
inline void dfs(int x,int y){
	f[x]=0;
	go(i,x){
		if (V==y) continue;
		dfs(V,x);lon1=max(lon1,f[x]+f[V]+lon[i]);
		f[x]=max(f[x],f[V]+lon[i]);
	}
}
inline ll check(int x){
	F(i,1,n) head[i]=0;nedge=0;
	F(i,1,n-1) add_ne(z[i].x,z[i].y,z[i].a+z[i].b*x);
	lon1=0;
	dfs(1,0);
	return lon1;
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();r=lim=read();
	F(i,1,n-1){
		z[i].x=read();z[i].y=read();z[i].a=read();z[i].b=read();
	}
	check(0);
	while (r-l>5){
		int num=(r-l)/3;
		l1=l+num,l2=l+num+num;
		if (check(l1)<=check(l2)) r=l2;
		else l=l1;
	}
	ans=inf;int p=0;
	F(i,max(0,l-5),min(lim,r+5)){
		ll t=check(i);
		if (t<ans) ans=t,p=i;
	}
	wrn(p);
	wrn(ans);
	return 0;
}
