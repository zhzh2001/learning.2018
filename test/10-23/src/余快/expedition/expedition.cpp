/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
#define eps 1e-9
#define db long double
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
db n,vmax,L,d,limit;
db t,lon,v,a;
double ans;
db get(db v0,db d){
	db lon=(vmax-v0)/a*(vmax+v0)/2;
	if (lon<d+eps) return (vmax-v0)/a+(d-lon)/vmax;
	return (-v0+sqrt(v0*v0+2*a*d))/a;
}
signed main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a=read();vmax=read();L=read();d=read();limit=read();
	t=vmax*1.0/a;
	lon=0.5*a*t*t;
	if (lon<d+eps) v=vmax;
	else v=1.0*a*sqrt(d*2.0*a);
	if (v<limit+eps){
		ans=sqrt(L*2.0*a);
		if (lon>L+eps) printf("%.9lf",ans);
		else{
			ans=(L-lon)/vmax+vmax/a;
			printf("%.9lf",ans);
		}
		return 0;
	}
	lon=lon+(vmax-limit)/a*(limit+vmax)/2;
	if (lon<d+eps){
		ans=(d-lon)/vmax+t+(vmax-limit)/a;
		ans+=get(limit,L-d);
		printf("%.9lf",ans);
		return 0;
	}
	ans=get(limit,L-d);
//	wrn(ans*(limit*2+ans*a)*0.5);
	v=sqrt(a*d+0.5*limit*limit);
	t=v/a+(v-limit)/a;
	ans+=t;
	printf("%.9lf",ans);
	return 0;
}
