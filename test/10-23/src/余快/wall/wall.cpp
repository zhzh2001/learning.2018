/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define put putchar('\n')
#define F(i,a,b) for (register int i=(a);i<=(b);i++)
#define D(i,a,b) for (register int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans[N],mod,q,d,k,a[N],l[N],r[N],b[N];
char str[5];
struct xx{
	int x,y,p;
}z[N];
struct xy{
	int x,y,p,i;
}c[N];
bool cmp2(xy a,xy b){
	return a.p<b.p;
}
bool cmp(xx a,xx b){
	return a.y<b.y;
}
void pre(int x){
	int t=0;
	F(i,1,k){
		if (i==1) t=((z[i].y-x)%mod);
		else t+=b[i];
		t-=(t>=mod)?mod:0;
		t+=(t<0)?mod:0;
		a[i]=z[i].x+t*z[i].p;
		a[i]=(a[i]<=0)?2-a[i]:a[i];
		a[i]=(a[i]>n-d+1)?n+n-d-d+2-a[i]:a[i];
		a[i]=(a[i]<=0)?2-a[i]:a[i];
		a[i]=(a[i]>n-d+1)?n+n-d-d+2-a[i]:a[i];
	}
	l[k+1]=0;r[k+1]=n+1;
	D(i,k,1){
		int t=z[i+1].y-z[i].y;
		l[i]=max(l[i+1]-t,a[i]);
		r[i]=min(r[i+1]+t,a[i]+d-1);
		if (l[i]>r[i]){
			F(j,1,i-1) l[j]=m,r[j]=0;
			break;
		}
	}
}
int query(int x,int y){
	int l1=1,r1=k+1;
	while (l1<r1){
		int mid=(l1+r1)>>1;
		if (z[mid].y>=y) r1=mid;
		else l1=mid+1;
	}
	int t=z[l1].y-y;
	return (max(l[l1],x-t)<=min(r[l1],x+t));
}
signed main(){
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	n=read();m=read();d=read();k=read();
	F(i,1,k){
		z[i].x=read();z[i].y=read();scanf("%s",str+1);
		if (str[1]=='-') z[i].p=-1;else z[i].p=1;
	}
	mod=(n-d)*2;
	sort(z+1,z+k+1,cmp);z[k+1].y=m+1;
	F(i,2,k) b[i]=(z[i].y-z[i-1].y)%mod;
	q=read();
	F(i,1,q){
		c[i].x=read();c[i].y=read();
		c[i].p=c[i].y%mod;c[i].i=i;
	}
	sort(c+1,c+q+1,cmp2);c[0].p=-1;
	F(i,1,q){
		if (c[i].p!=c[i-1].p) pre(c[i].p);
		ans[c[i].i]=query(c[i].x,c[i].y);
	}
	F(i,1,q) wrn(ans[i]);
	return 0;
}
