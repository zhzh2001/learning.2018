/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans[N],mod,q,d,k,a[N],x,y;
char str[5];
struct xx{
	int x,y,p;
}z[N];
bool cmp(xx a,xx b){
	return a.y<b.y;
}
int solve(int x,int y){
	F(i,1,k){
		if (z[i].y<y) continue;
		a[i]=z[i].x;int p=1;
		F(j,1,z[i].y-y){
			a[i]=a[i]+p*z[i].p;
			if (a[i]==n-d+2) a[i]=(n-d),p=-p;
			if (a[i]==0) a[i]=2,p=-p;
		}
	}
	int l=x,r=x;
	F(i,1,k){
		if (z[i].y<y) continue;
		int t=z[i].y-y;y=z[i].y;
		l=max(l-t,a[i]);r=min(r+t,a[i]+d-1);
		if (l>r) return 0;
	}
	return (l<=r);
}
signed main(){
	freopen("wall.in","r",stdin);
	freopen("std.out","w",stdout);
	n=read();m=read();d=read();k=read();
	F(i,1,k){
		z[i].x=read();z[i].y=read();scanf("%s",str+1);
		if (str[1]=='-') z[i].p=-1;
		else z[i].p=1;
	}
	sort(z+1,z+k+1,cmp);q=read();
	F(i,1,q){
		x=read();y=read();
		wrn(solve(x,y));
	}
	return 0;
}
