#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
namespace Dango
{
	const int MAXN=505,MAXM=100000005,MAXQ=100005;
	struct door
	{
		int x,y,dir;
	}w[100005];
	int n,m,d,k,q,x,y;
	bool cmp(door a,door b){return a.y<b.y;}
	int pfind(int a)
	{
		int l=1,r=k,ans=-1;
		while(l<=r)
		{
			int mid=(l+r)>>1;
			if(w[mid].y>a)
			{
				ans=mid;
				r=mid-1;
			}
			else l=mid+1;
		}
		return ans;
	}
	bool solve()
	{
		cin>>x>>y;
		int pos=pfind(y);
		if(pos==-1) return true;
		int l=x,r=x,now=y;
		for(int i=pos;i<=k;i++)
		{
			int nxt=w[i].y,c=nxt-now,ll=w[i].x,rr=w[i].x+d-1;
			int xx=(nxt-y)%(2*(n-d))*w[i].dir;
			ll+=xx;
			rr+=xx;
			ll--;rr--;
			while(ll<0||rr>=n)
			{
				if(ll<0){ll=-ll;rr=ll+1;}
				else{ll-=rr-n+1;rr=n-1-(rr-n+1);}
			}
			ll++;rr++;
			l=max(l-c,ll);
			r=min(r+c,rr);
			if(r<l)return false;
		}
		return true;
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin.tie(0);
		cin>>n>>m>>d;
		cin>>k;
		for(int i=1;i<=k;i++)
		{
			int x,y;
			string dir;
			cin>>x>>y>>dir;
			w[i].x=x;
			w[i].y=y;
			w[i].dir=(dir[0]=='+'?1:-1);
		}
		sort(w+1,w+k+1,cmp);
		cin>>q;
		for(int i=1;i<=q;i++)
			cout<<solve()<<"\n";
		return 0;
	}
}
int main()
{
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	return Dango::work();
}
