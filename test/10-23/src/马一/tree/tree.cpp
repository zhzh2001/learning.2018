#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<vector>
using namespace std;
namespace Dango
{
	const int MAXN=250005;
	int n,lim,head[MAXN],to[MAXN<<1],nxt[MAXN<<1],a[MAXN<<1],b[MAXN<<1],cnt,dp[MAXN],ans,anss=0x3f3f3f3f,sum;
	int read()
	{
		int x=0,f=0;
		char ch=getchar();
		while(!isdigit(ch)){f|=(ch=='-');ch=getchar();}
		while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
		return f?-x:x;
	}
	bool cmp(int a,int b){return a>b;}
	void add(int u,int v,int aa,int bb)
	{
		cnt++;
		to[cnt]=v;
		nxt[cnt]=head[u];
		a[cnt]=aa;
		b[cnt]=bb;
		head[u]=cnt;
	}
	void dfs(int u,int f,int delta)
	{
		dp[u]=0;
		vector<int> ve;
		for(int i=head[u];i;i=nxt[i])
		{
			int v=to[i],w=a[i]+delta*b[i];
			if(v==f)continue;
			dfs(v,u,delta);
			dp[u]=max(dp[u],dp[v]+w);
			ve.push_back(dp[v]+w);
		}
		sort(ve.begin(),ve.end(),cmp);
		int aa=ve.size()>=2?ve[0]+ve[1]:0;
		if(sum<max(aa,dp[u]))
			sum=max(aa,dp[u]);
	}
	int work()
	{
		n=read();lim=read();
		for(int i=1;i<n;i++)
		{
			int xx=read(),yy=read(),aa=read(),bb=read();
			add(xx,yy,aa,bb);
			add(yy,xx,aa,bb);
		}
		for(int i=0;i<=lim;i++)
		{
			sum=-0x3f3f3f3f;
			dfs(1,0,i);
			if(sum<anss)
			{
				anss=sum;
				ans=i;
			}
		}
		printf("%d\n%d",ans,anss);
		return 0;
	}
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	return Dango::work();
}
