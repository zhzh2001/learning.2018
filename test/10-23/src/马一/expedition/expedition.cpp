#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
namespace Dango
{
	const int MAXN=10005;
	double a,v,l,d,limit,ans;
	double pow2(double a){return a*a;}
	int work()
	{
		scanf("%lf%lf%lf%lf%lf",&a,&v,&l,&d,&limit);
		if(v<=limit||sqrt(2*a*d)<=limit)
		{
			double s;
			if((s=pow2(v)/2/a)<=l)
			{
				ans=v/a;
				l-=s;
				ans+=l/v;
			}
			else
				ans=sqrt(2*l/a);
		}
		else
		{
			double x=(pow2(limit)+2*a*d)/4/a,vx,s;
			if((vx=sqrt(2*a*x))<=v)
				ans+=(2*vx-limit)/a;
			else
			{
				double s1=pow2(v)/2/a,s2=(pow2(v)-pow2(limit))/2/a,s3=d-s1-s2;
				ans+=(2*v-limit)/a+s2/v;
			}
			l-=d;
			if((s=(pow2(v)-pow2(limit))/2/a)>l)
				ans+=(sqrt(pow2(limit)+2*a*l)-limit)/a;
			else ans+=(v-limit)/a+(l-s)/v;
		}
		printf("%.9lf",ans);
		return 0;
	}
}
int main()
{
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	return Dango::work();
}
