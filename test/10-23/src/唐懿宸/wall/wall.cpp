#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<cctype>
#include<string>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<map>
#include<queue>
#include<set>
#include<vector>
#include<ctime>
using namespace std;

namespace TYC
{
	const int N=510,K=1e5+10,inf=0x3f3f3f3f;
	int n,m,size,tot,ques;
	bool dp[2][N];

	inline int read()
	{
		int x=0,f=0;char ch=getchar();
		while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
		while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
		return f?-x:x;
	}

	struct Door
	{
		int x,y,dir;
		bool operator < (const Door &t) const
		{
			return y<t.y;
		}
	}door[K];

	inline int get_pos(Door now,int tim)
	{
		int loop=n-size;
		if(now.dir)
		{
			if(tim+now.x+size-1<=n)
				return now.x+tim;
			tim-=n-(now.x+size-1);
			tim%=2*loop;
			if(tim<=loop)
				return n-size+1-tim;
			else
				return tim-loop+1;
		}
		else
		{
			if(now.x-tim>=1)
				return now.x-tim;
			tim-=now.x-1;
			tim%=2*loop;
			if(tim<=loop)
				return 1+tim;
			else
				return n-size+1-(tim-loop);
		}
	}
	
	void solve_2()
	{
		for(int i=1;i<=ques;i++) puts("1");
	}

	void work()
	{
		n=read(),m=read(),size=read(),tot=read();
		char ch;
		for(int i=1;i<=tot;i++)
		{
			door[i].x=read(),door[i].y=read();
			do ch=getchar(); while(ch!='+'&&ch!='-');
			door[i].dir=(ch=='+');
		}
		sort(door+1,door+1+tot);
		door[tot+1]=(Door){inf,inf,2};
		ques=read();
		if(n==2) {solve_2();return;}
		while(ques--)
		{
			int Sx=read(),Sy=read();
			memset(dp,false,sizeof(dp));
			int point=1,flag=1;
			while(door[point].y<=Sy) point++;
			int nxt=door[point].y-Sy;
			int L=max(1,Sx-nxt),R=min(n,Sx+nxt);
			for(int i=L;i<=R;i++) dp[point&1][i]=true;
			for(int i=point;i<=tot;i++)
			{
				int now=(i&1);

				int l=get_pos(door[i],door[i].y-Sy),r=l+size-1;
				if(l>r) swap(l,r);
				for(int j=1;j<l;j++) dp[now][j]=false;
				for(int j=r+1;j<=n;j++) dp[now][j]=false;

				memset(dp[now^1],false,sizeof(bool[n+1]));

				int nxt=door[i+1].y-door[i].y,L=n,R=0;
				for(int j=1;j<=n;j++)
					if(dp[now][j]) {L=j;break;}
				for(int j=n;j;j--)
					if(dp[now][j]) {R=j;break;}
				if(L>R)
					{flag=0;break;}

				L=max(1,L-nxt),R=min(n,R+nxt);
				for(int i=L;i<=R;i++) dp[now^1][i]=true;
			}
			printf("%d\n",flag);
		}
	}
}

int main()
{
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	TYC::work();
	return 0;
}
