#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<cctype>
#include<string>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<map>
#include<queue>
#include<set>
#include<vector>
#include<ctime>
using namespace std;

namespace TYC
{
	typedef long long ll;
	const int N=110;
	int n,lim,cnt,Head[N];
	ll maxlen=0,ls[N],mx[N];
	
	inline int read()
	{
		int x=0,f=0;char ch=getchar();
		while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
		while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
		return f?-x:x;
	}
	
	struct Edge
	{
		int u,v,a,b;
	}e[N];
	
	struct edge
	{
		int to,next;
		ll w;
	}E[N<<1];
	
	inline void add(int u,int v,ll w)
	{
		E[++cnt]=(edge){v,Head[u],w};
		Head[u]=cnt;
	}
	
	void dfs(int u,int f)
	{
		ls[u]=mx[u]=0;
		for(int i=Head[u];i;i=E[i].next)
		{
			int v=E[i].to;
			if(v==f) continue;
			dfs(v,u);
			if(mx[u]<mx[v]+E[i].w)
				ls[u]=mx[u],mx[u]=mx[v]+E[i].w;
			else if(mx[v]+E[i].w>ls[u])
				ls[u]=mx[v]+E[i].w;
		}
		maxlen=max(maxlen,ls[u]+mx[u]);
	}
	
	void work()
	{
		n=read(),lim=read();
		for(int i=1;i<n;i++)
			e[i]=(Edge){read(),read(),read(),read()};
		ll ans=(1LL<<60),delta;
		for(int d=0;d<=lim;d++)
		{
			cnt=0;
			memset(Head,0,sizeof(Head));
			for(int j=1;j<n;j++)
			{
				int u=e[j].u,v=e[j].v;
				ll w=e[j].a+(ll)e[j].b*d;
				add(u,v,w),add(v,u,w);
			}
			maxlen=0;
			dfs(1,0);
			if(maxlen<ans) ans=maxlen,delta=d;
		}
		printf("%lld\n%lld\n",delta,ans);
	}
}

int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	TYC::work();
	return 0;
}
