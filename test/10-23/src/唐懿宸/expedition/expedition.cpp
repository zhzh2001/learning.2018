#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<cctype>
#include<string>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<map>
#include<queue>
#include<set>
#include<vector>
#include<ctime>
using namespace std;

namespace TYC
{
	const double eps=1e-8;
	double a,Vmax,Length,Distance,Limit;
	
	inline double sqr(double x)
	{
		return x*x;
	}
	
	inline bool eql(double x,double y)
	{
		if(y-eps<=x&&x<=y+eps) return true;
		else return false;
	}
	
	double solve_sp()
	{
		double Vtim=Vmax/a;
		double dis=0.5*a*sqr(Vtim);
		if(Length-dis>=eps)
			return (Length-dis)/Vmax+Vtim;
		else
		{
			double SqrTim=2.0*Length/a;
			return sqrt(SqrTim);
		}
	}
	
	double solve_More()
	{
		double Ans=0;
		double Vtim=Vmax/a,dis0=0.5*a*sqr(Vtim);
		Ans+=Vtim;
		
		double dis2=(sqr(Vmax)-sqr(Limit))/(2*a);
		double delta=sqr(Vmax)-2*a*dis2;
		double x1=(-Vmax+sqrt(delta))/(-a),x2=(-Vmax-sqrt(delta))/(-a),x;
		if(x1<0) x=x2;
		else if(x2<0) x=x1;
		else x=min(x1,x2);
		Ans+=x;
		
		double dis1=Distance-dis0-dis2;
		Ans+=dis1/Vmax;
		return Ans;
	}
	
	double solve_Vinf()
	{
		double La=Limit/a,Da=Distance/a;
		double delta=sqr(2*La)-4*(0.5*sqr(La)-Da);
		double x1=(-2*La+sqrt(delta))*0.5,x2=(-2*La-sqrt(delta))*0.5,x;
		if(x1<0) x=x2;
		else if(x2<0) x=x1;
		else x=min(x1,x2);
		double t1=x,t0=La+x;
		
		double Ans=0;
		if(a*t0-Vmax>eps) Ans=solve_More();
		else Ans=t0+t1;
		
		Length-=Distance;
		double Vtim=(Vmax-Limit)/a,dis=Limit*Vtim+0.5*a*sqr(Vtim);
		if(Length-dis>=eps)
			return Ans+(Length-dis)/Vmax+Vtim;
		else
		{
			delta=sqr(Limit)+2*a*Length;
			x1=(-Limit+sqrt(delta))/a,x2=(-Limit-sqrt(delta))/a;
			if(x1<0) x=x2;
			else if(x2<0) x=x1;
			else x=min(x1,x2);
			return x+Ans;
		}
	}
	
	void work()
	{
		scanf("%lf%lf%lf%lf%lf",&a,&Vmax,&Length,&Distance,&Limit);
		if(Limit-Vmax>=eps||eql(Limit,Vmax)||Distance-Length>eps)
			printf("%.9f",solve_sp());
		else 
			printf("%.9f",solve_Vinf());
	}
}

int main()
{
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	TYC::work();
	return 0;
}
