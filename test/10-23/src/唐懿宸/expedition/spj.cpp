#include<bits/stdc++.h>
using namespace std;
const long double eps=1e-6;

int main()
{
	long double ans,res;
	freopen("expedition2.ans","r",stdin);
	cin>>ans;
	freopen("expedition.out","r",stdin);
	cin>>res;
	if(res-eps<=ans&&ans<=res+eps) cout<<"AC\n";
	else cout<<"WA\n";
}
