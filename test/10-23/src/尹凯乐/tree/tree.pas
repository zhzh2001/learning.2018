program tree;
 uses math;
 type
  wb=record
   n:^wb;
   t,x,y:longint;
  end;
  wwb=^wb;
 var
  b:array[0..500001] of wb;
  h,d:array[0..250001] of int64;
  f,g:array[0..250001] of int64;
  i,k,m,n,o,p,x,y,z:longint;
  mc,smax:int64;
 procedure hahainc(x,y,z,w:longint);
  begin
   inc(p);
   b[p].n:=@b[h[x]];
   b[p].t:=y;
   b[p].x:=z;
   b[p].y:=w;
   h[x]:=p;
  end;
 procedure hahaBFS(k,x:longint);
  var
   i:longint;
   j,l:wwb;
  begin
   j:=@b[h[k]];
   while j<>@b[0] do
    begin
     l:=j;
     i:=j^.t;
     j:=j^.n;
     if i=x then continue;
     hahaBFS(i,k);
     if f[i]+l^.x+l^.y*mc>=f[k] then
      begin
       g[k]:=f[k];
       f[k]:=f[i]+l^.x+l^.y*mc;
      end
                   else if f[i]+l^.x+l^.y*mc>=g[k] then g[k]:=f[i]+l^.x+l^.y*mc;
     smax:=max(smax,f[k]+g[k]);
    end;
  end;
 function hahacpm(x,y:longint):boolean;
  var
   o,p:int64;
  begin
   filldword(f,sizeof(f)>>2,0);
   filldword(g,sizeof(g)>>2,0);
   smax:=0;
   mc:=x;
   hahaBFS(1,0);
   o:=smax;
   filldword(f,sizeof(f)>>2,0);
   filldword(g,sizeof(g)>>2,0);
   smax:=0;
   mc:=y;
   hahaBFS(1,0);
   p:=smax;
   exit(o<=p);
  end;
 begin
  assign(input,'tree.in');
  assign(output,'tree.out');
  reset(input);
  rewrite(output);
  p:=0;
  filldword(h,sizeof(h)>>2,0);
  readln(n,m);
  for i:=1 to n-1 do
   begin
    readln(k,x,y,z);
    hahainc(k,x,y,z);
    hahainc(x,k,y,z);
   end;
  o:=0;
  p:=m;
  while o<p do
   begin
    m:=(o+p)>>1;
    if hahacpm((o+m)>>1,(p+m+1)>>1) then p:=(p+m+1)>>1-1
                                    else o:=(o+m)>>1+1;
   end;
  writeln(p);
  filldword(f,sizeof(f)>>2,0);
  filldword(g,sizeof(g)>>2,0);
  smax:=0;
  mc:=p;
  hahaBFS(1,0);
  writeln(smax);
  close(input);
  close(output);
 end.
