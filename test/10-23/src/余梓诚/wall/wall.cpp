#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

#define rep(i,l,r) for(int i=l;i<=r;i++)
#define dep(i,r,l) for(int i=r;i>=l;i--)

const int N=110000;
int n,m,k,l,r,h,x,y,s,p;
struct node{
	int a,b,v,d;
}a[N];
char c[N][10];
void get_num(int k,int l,int r,int d,int &x,int &y){
	k%=2*n;
	if(d==1){
		if (l>k){
			x=l-k; y=r-k;
			return;
		}else k-=l-1,r-=l-1,l=1,d=2;
	}
	if(d==2){
		if(n-r>=k){
			x=l+k; y=r+k;
			return;
		}else k-=n-r,l+=n-r,r=n,d=1;
	}
	if(d==1){
		if (l>k){
			x=l-k; y=r-k;
			return;
		}else k-=l-1,r-=l-1,l=1,d=2;
	}
	if(d==2){
		if (n-r>=k){
			x=l+k; y=r+k;
			return;
		}
		else k-=n-r,l+=n-r,r=n,d=1;
	}
}
void check(int k,int &l,int &r,int x,int y){
	int ansl=max(l-k,1),ansr=min(r+k,n);
	if(ansl>y||ansr<x) return (void) (l=r=-1);
	l=max(ansl,x); r=min(ansr,y);
}
bool cmp(node x,node y){
	return x.v<y.v;
}
int main(){
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>m>>k>>m;
	rep(i,1,m){
		cin>>a[i].a>>a[i].v;
		cin>>c[i]+1;
		if (c[i][1]=='+') a[i].d=2;
		else a[i].d=1;
		a[i].b=a[i].a+k-1;
	}
	sort(a+1,a+m+1,cmp);
	cin>>k;
	rep(i,1,k){
		cin>>l>>h;
		r=l; s=1; p=h;
		while (a[s].v<=h) s++;
		rep(j,s,m){
			get_num(a[j].v-h,a[j].a,a[j].b,a[j].d,x,y);
			check(a[j].v-p,l,r,x,y);
			if (l==-1&&r==-1) break;
			p=a[j].v;
		}
		if (l==-1&&r==-1) printf("0\n");
		else printf("1\n");
	}
	return 0;
}
