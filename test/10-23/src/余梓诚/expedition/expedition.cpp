#include <cmath>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <algorithm>
using namespace std;

void judge(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
}

double a,vm,L,d,lim,tm,T,S,ans;

int main(){
	judge();
	ios::sync_with_stdio(false);
	cin>>a>>vm>>L>>d>>lim;
	tm=1.0*vm/(1.0*a); T=sqrt(2.0*d/(1.0*a));
	//cout<<tm<<" "<<T<<endl;
	if (lim>=vm||T>=tm&&T*a<=lim){
		S=1.0*vm*tm/2.0;
		//cout<<S<<endl;
		if(S>=L) ans=sqrt(2.0*L/(1.0*a));
		else ans=1.0*tm+(1.0*L-1.0*S)/(1.0*vm);
		//printf("%.15lf",(double)ans);
		cout<<fixed<<setprecision(15)<<ans<<endl;
	}else{
		double t1=1.0*vm*1.0*tm/2.0,t2=(1.0*vm+1.0*lim)*(1.0*vm-1.0*lim)/(1.0*a)/2.0;
		S=t1+t2;
		//cout<<t1<<" "<<t2<<" "<<S<<endl;
		if (S<=d){
			t1=(1.0*vm-1.0*lim)/(1.0*a); t2=1.0*tm+(1.0*d-1.0*S)/(1.0*vm);
			//cout<<t1<<" "<<t2<<endl;
			ans=t1+t2;
		}else{
			t1=sqrt((1.0*d+1.0*lim*1.0*lim/(1.0*a)/2.0)/(1.0*a));
			t2=(1.0*t1*1.0*a-1.0*lim)/(1.0*a);
			//cout<<t1<<" "<<t2<<endl;
			ans=1.0*t1+1.0*t2;
		}
		double V=sqrt(2.0*(1.0*L-1.0*d)*(1.0*a)+1.0*lim*1.0*lim);
		//cout<<V<<endl;
		if (V>=vm){
			t1=(1.0*vm-1.0*lim)/(1.0*a);
			t2=((1.0*L-1.0*d)-(1.0*vm+1.0*lim)*(1.0*vm-1.0*lim)/(1.0*a)/2.0)/(1.0*vm);
			//cout<<t1<<" "<<t2<<endl;
			ans+=t1+t2;
		}else ans+=(sqrt(1.0*lim*1.0*lim+2.0*1.0*a*(1.0*L-1.0*d))-1.0*lim)/(1.0*a);
		cout<<fixed<<setprecision(15)<<ans<<endl;
	}
	return 0;
}
