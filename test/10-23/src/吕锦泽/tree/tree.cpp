#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define ll long long
#define eps (1e-8)
#define N 250005
#define inf (1ll<<60)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,tot,R,det,head[N],g[N],f[N],lim,ans,anss;
struct edge{ ll to,next,a,b; }e[N<<1];
void add(ll u,ll v,ll w1,ll w2){
	e[++tot]=(edge){v,head[u],w1,w2}; head[u]=tot;
}
void dfs(ll u,ll last){
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue; dfs(v,u);
		if (f[v]+e[i].a+e[i].b*det>f[u]) g[u]=f[u],f[u]=f[v]+e[i].a+e[i].b*det;
		else g[u]=max(g[u],f[v]+e[i].a+e[i].b*det);
	}
	R=max(R,max(f[u]+g[u],f[u]));
	f[u]=max(f[u],0ll); g[u]=max(g[u],0ll);
}
ll work(ll x){
	det=x; R=0;
	rep(i,1,n) f[i]=g[i]=-inf;
	dfs(1,-1);
	return R;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(); lim=read();
	rep(i,2,n){
		ll u=read(),v=read(),w1=read(),w2=read();
		add(u,v,w1,w2); add(v,u,w1,w2);
	}
	ll l=0,r=lim; anss=0; ans=work(0);
   while (l<=r){
		ll mid1=(l+r)>>1,mid2=(mid1+r)>>1;
		ll t1=work(mid1),t2=work(mid2);
		if (t1<ans||t1==ans&&mid1<anss) anss=mid1,ans=t1;
		if (t2<ans||t2==ans&&mid2<anss) anss=mid2,ans=t2;
		if (t1<=t2) r=mid2-1; else l=mid1+1;
	}
	printf("%lld\n%lld\n",anss,ans);
}
