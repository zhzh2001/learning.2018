#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define ll long long
#define eps (1e-8)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll a,vm,L,d,li;
int main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a=read(); vm=read(); L=read(); d=read(); li=read();
	if (vm<=li){
		double t=(double)vm/a;
		if (vm*vm/(double)a/2.0>=L) return printf("%.9f",sqrt(L*2/a));
		else printf("%.9f",t+(L-vm*vm/(double)a/2.0)/vm);
	}else{
		if (li*li<d*a*2){
			double t1=li/(double)a,t2,t3,nowx=li*li/(double)a/2;
			
			double tt=(-li+sqrt(li*li-a*nowx+a*d))/a;
			if (a*tt+li>vm){
				t3=(vm-li)/(double)a*2+(d-nowx-(vm-li)/(double)a*(li+vm))/vm;
			}else t3=tt*2;
			
			if (vm*vm-li*li>=(L-d)*2*a) t2=((sqrt(li*li-2*a*d+2*a*L)-li)/a);
			else t2=(vm-li)/(double)a+(L-d-(vm*vm-li*li)/(double)a/2)/vm;
			
			printf("%.9f",t1+t2+t3);
		}else{
			double t=(double)vm/a;
			if (vm*vm/(double)a/2.0>=L) return printf("%.9f",sqrt(L*2/a));
			else printf("%.9lf",t+(L-vm*vm/(double)a/2.0)/vm);
		}
	}
}
