#include<cmath>
#include<cstdio>
#include<iostream>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const double eps=1e-12;
bool check(double a,double v0,double v,double dis,double lim){
	double x=(v*v-v0*v0)/2/a;//从lim速度到v所用的路程 
	dis-=x;
	v0=v;
	v=sqrt(max(-2*a*dis+v0*v0,0.0));//剩下的路程全拿来减速 
	return v<=lim;
}
int main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	double a=read(),vm=read(),L=read(),d=read(),ans;
	double lim=min((double)read(),vm);
	double t=lim/a;//到达lim速度时所需的时间 
	double x=0.5*a*t*t;//到达lim速度后的位置
	if(x>=d){//可以不管关卡限制 
		t=vm/a;//到达vm速度所需的时间 
		x=0.5*a*t*t;//到达vm速度后的位置 
		if(x>=L){//可以不管vm限制 
			ans=sqrt(2*L/a);
		}else{//先到vm速度再以vm速度到达终点 
			ans=t+(L-x)/vm;
		}
	}else{
		if(vm>lim){
			//中间的前段时间以a加速，后端时间以a减速 
			//二分最大速度 
			double l=lim/eps,r=vm/eps,mm=lim/eps;
			while(l<=r){
				double mid=(l+r)/2;
				if(check(a,lim,mid*eps,d-x,lim))l=mid+1,mm=mid;
				else r=mid-1;
			}
			mm*=eps;
			t=mm/a;
			double t1=abs(lim-mm)/a;
			ans=t+t1+max(0.0,(d-0.5*a*t*t-(mm*t1-0.5*a*t1*t1)))/mm;
		}else{
			ans=t+(d-x)/vm;
		}
		//过了关卡就能飙车，直接加到vm
		double v=sqrt(lim*lim+2*a*(L-d));//经过终点时的速度 
		if(v<=vm)ans+=(v-lim)/a;//没超过vm，直接加速 
		else{
			x=(vm*vm-lim*lim)/2/a;//速度加到vm所需的路程 
			ans+=(L-d-x)/vm+(vm-lim)/a;
		}
	}
	printf("%.15lf",ans);
}
