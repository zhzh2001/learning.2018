#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const double eps=1e-8;
const int MAXN=250005;
int nedge,head[MAXN];
struct Edge{
	int to,nxt;
	double w;
}edge[MAXN<<1];
void add(int x,int y,double z){
	edge[++nedge].to=y;
	edge[nedge].w=z;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
double d[MAXN];
int n,lim,x[MAXN],y[MAXN],a[MAXN],b[MAXN];
void dfs(int x,int fa){
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		if(y==fa)continue;
		d[y]=d[x]+edge[i].w;
		dfs(y,x);
	}
}
double sum[MAXN];
bool flag1=1,flag2=1,flag3;
inline long long f(double delta){
	if(flag1&&flag3){
		for(int i=1;i<n;i++)
			sum[i]=sum[i-1]+a[i]+b[i]*delta;
		double ans=0;
		for(register int i=1;i<n;i++)
			for(register int j=i;j<n;j++)
				ans=max(ans,sum[j]-sum[i-1]);
		return ans;
	}
	if(flag2){
		double Mx=-1e18,Mxn=-1e18;
		for(int i=1;i<n;i++){
			double tmp=a[i]+b[i]*delta;
			if(tmp>Mx){
				Mxn=Mx;
				Mx=tmp;
			}else if(tmp>Mxn)Mxn=tmp;
		}
		return max(0.0,Mx+Mxn);
	}
	nedge=0;
	memset(head,0,sizeof(head));
	memset(edge,0,sizeof(edge));
	for(int i=1;i<n;i++){
		add(x[i],y[i],a[i]+b[i]*delta);
		add(y[i],x[i],a[i]+b[i]*delta);
	}
	memset(d,0,sizeof(d));
	dfs(1,0);
	int p=1;
	for(int i=2;i<=n;i++)
		if(d[i]>d[p])p=i;
	memset(d,0,sizeof(d));
	dfs(p,0);
	for(int i=1;i<=n;i++)
		if(d[i]>d[p])p=i;
//	cout<<d[p]<<endl;
	return d[p];
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),lim=read();
	for(int i=1;i<n;i++){
		x[i]=read(),y[i]=read();
		a[i]=read(),b[i]=read();
		flag1&=x[i]==y[i]-1;
		flag2&=x[i]==1;
	}
	double l=0,r=lim;
	while(l+eps<r){
		double lmid=l+(r-l)/3;
		double rmid=r-(r-l)/3;
		if(f(lmid)<=f(rmid))r=rmid;
		else l=lmid;
	}
	int ans1=floor(l),ans2=ceil(l);
	flag3=ans1==ans2;
	if(flag3)return printf("%d\n%lld",ans1,f(ans1)),0;
	long long a=f(ans1),b=f(ans2);
	if(a>b)printf("%d\n%lld",ans2,b);
	else printf("%d\n%lld",ans1,a);
}
