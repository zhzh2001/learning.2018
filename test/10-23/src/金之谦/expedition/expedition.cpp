#include <bits/stdc++.h>
using namespace std;
typedef double D;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
D a,v,l,d,lim,ll;
inline D sqr(D x){return x*x;}
inline D case1(){
	D t=0;
	if(ll-l>1e-9)t=sqrt(2*l/a);
	else{
		D pl=sqr(v)/2/a,ql=l-pl;
		t+=sqrt(2*pl/a);
		t+=ql/v;
	}
	return t;
}//全程无关口阻挡 
inline D case2(){
	D pl=(sqr(v)-sqr(lim))/2/a,t=0;
	if(pl-(l-d)>1e-9){
		D v1=sqrt(2*a*(l-d)+sqr(lim));
		t+=(v1-lim)/a;
	}else{
		D ql=l-d-pl;
		D v1=sqrt(2*a*pl+sqr(lim));
		t+=(v1-lim)/a;
		t+=ql/v;
	}
	return t;
}//关口后 
inline D case3(){
	D l=lim,r=v,t=0;
	while(r-l>1e-9){
		D mid=(l+r)/2;
		D pl=sqr(mid)/2/a+(sqr(mid)-sqr(lim))/2/a;
		if(pl-d>=-1e-9)r=mid;
		else l=mid;
	}
	t+=l/a+(l-lim)/a;
	return t;
}//关口前阻挡（三角） 
int main()
{
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a=read();v=read();l=read();d=read();lim=read();
	ll=sqr(v)/2/a;D t=0;
	if(ll<=d){
		if(lim-v>1e-9){
			t=case1();
			printf("%.7lf",t);return 0;
		}
		D rl=(sqr(v)-sqr(lim))/2/a;
		if(ll+rl-d>1e-9)t+=case3();
		else{
			t+=v/a+(v-lim)/a;
			D pl=d-ll-rl;
			t+=pl/v;
		}
		t+=case2();
		printf("%.7lf",t);
		return 0;
	}
	if(lim-sqrt(2*a*d)>1e-9)t=case1();
	else t=case2()+case3();
	printf("%.7lf",t);
	return 0;
}
