#include <bits/stdc++.h>
#define f(i,j) F[(i-1)*MOD+j]
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int n,m,D,K,MOD;
struct ppap{int x,y;char c[2];}a[N];
inline bool cmp(ppap a,ppap b){return a.y<b.y;}
struct qj{int l,r;}F[30000010];
inline int cal(int x,int k){
	int px=a[x].x;
	if(a[x].c[0]=='+'){
		if(px+k>n-D+1){
			k-=n-D-px+1;px=n-D+1;
			if(px-k<1){
				k-=px-1;px=1;
				px+=k;
			}else px-=k;
		}else px+=k;
	}else{
		if(px-k<1){
			k-=px-1;px=1;
			if(px+k>n-D+1){
				k-=n-D-px+1;px=n-D+1;
				px-=k;
			}else px+=k;
		}else px-=k;		
	}
	return px;
}
int main()
{
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	n=read();m=read();D=read();
	K=read();MOD=max(1,2*(n-D));
	for(int i=1;i<=K;i++){
		a[i].x=read();a[i].y=read();
		scanf("%s",a[i].c);
	}
	sort(a+1,a+K+1,cmp);
	for(int i=0;i<MOD;i++){
		int px=cal(K,i);
		f(K,i)=(qj){px,px+D-1};
	}
	for(int i=K-1;i;i--)
		for(int j=0;j<MOD;j++){
			int px=cal(i,j),pp=a[i+1].y-a[i].y,po=(j+pp)%MOD;
			if(f(i+1,po).l>n){
				f(i,j)=(qj){n+1,0};
				continue;
			}
			int lx=max(1,f(i+1,po).l-pp),rx=min(n,f(i+1,po).r+pp);
			lx=max(px,lx);rx=min(px+D-1,rx);
			if(lx>rx)lx=n+1,rx=0;
			f(i,j)=(qj){lx,rx};
		}
	for(int Q=read();Q;Q--){
		int x=read(),y=read();
		int l=1,r=K,pos=K+1,ans=0;
		while(l<=r){
			int mid=(l+r)>>1;
			if(a[mid].y>=y)pos=mid,r=mid-1;
			else l=mid+1;
		}
		if(pos==K+1)ans=1;
		else{
			int pp=a[pos].y-y;
			int lx=max(1,x-pp),rx=min(n,x+pp),po=pp%MOD;
			if(f(pos,po).l==n+1)ans=0;
			else{
				lx=max(lx,f(pos,po).l);rx=min(rx,f(pos,po).r);
				if(lx<=rx)ans=1;
			}
		}
		writeln(ans);
	}
	return 0;
}
