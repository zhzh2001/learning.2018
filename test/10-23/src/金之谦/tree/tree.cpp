#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=3e5+10;
int n,lim;
int nedge=0,p[2*N],nex[2*N],head[2*N],ca[2*N],cb[2*N];
inline void addedge(int a,int b,int va,int vb){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
	ca[nedge]=va;cb[nedge]=vb;
}
int f[N],sum=0,li;
inline void dfs(int x,int fa){
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		dfs(p[k],x);
		int p1=f[p[k]]+ca[k]+cb[k]*li;
		sum=max(sum,f[x]+p1);
		if(p1>f[x])f[x]=p1;
	}
	sum=max(sum,f[x]);
}
inline int check(int x){
	li=x;sum=0;
	for(int i=1;i<=n;i++)f[i]=0;
	dfs(1,0);
	return sum;
}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();lim=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read(),a=read(),b=read();
		addedge(x,y,a,b);addedge(y,x,a,b);
	}
	int l=0,r=lim;
	while(r-l>10){
		int m1=l+(r-l+1)/3,m2=m1+(r-l+1)/3;
		if(check(m1)<=check(m2))r=m2;
		else l=m1;
	}
	int ans=1e18,anss=0;
	for(int i=l;i<=r;i++){
		int rp=check(i);
		if(rp<ans){
			ans=rp;anss=i;
		}
	}
	writeln(anss);writeln(ans);
	return 0;
}
