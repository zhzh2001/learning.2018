#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
const int N=100005,inf=1e9;
struct data{
    int x,y,dir;
}a[N];
int n,m,d,k,q,x[N],y[N],l[N],r[N],ans[N];
vector<int> v[1005];
inline bool cmp(int a,int b){
    return x[a]>x[b];
}
inline bool Cmp(data a,data b){
    return a.x>b.x;
}
inline void cao(int id){
    if(a[id].y==n-d+1)a[id].dir=-1;  else if(a[id].y==1)a[id].dir=1; a[id].y+=a[id].dir;
}
int main(){
    freopen("wall.in","r",stdin); freopen("wall.out","w",stdout);
    n=read(); m=read(); d=read(); int len=2*(n-d);
    k=read();
    for(int i=1;i<=k;i++){
        a[i].x=read(); a[i].y=read(); a[i].dir=getchar()=='-'?1:-1; swap(a[i].x,a[i].y);
    }
    sort(&a[1],&a[k+1],Cmp);
    for(int i=1;i<=k;i++){
        int t=a[i].x%len; for(int j=0;j<len-t;j++){cao(i); } 
    }
    q=read();
    if(len==0){
        while(q--)puts("1"); return 0;
    }
    for(int i=1;i<=q;i++){
        x[i]=read(); y[i]=read(); swap(x[i],y[i]);
        v[x[i]%len].push_back(i);
    }
    for(int i=0;i<len;i++){
        sort(v[i].begin(),v[i].end(),cmp);
    }
    l[0]=1; r[0]=n; a[0].x=m+1;
    for(int i=0;i<len;i++){
        int dq=0; 
        for(auto j:v[i]){
            while(dq<k&&a[dq+1].x>x[j]){
                dq++;
                int t=a[dq-1].x-a[dq].x; l[dq]=max(1,l[dq-1]-t); r[dq]=min(n,r[dq-1]+t); //cout<<l[dq-1]<<" "<<r[dq-1]<<" "<<t<<" "<<l[dq]<<" wzp "<<r[dq]<<endl;
                l[dq]=max(l[dq],a[dq].y); r[dq]=min(r[dq],a[dq].y+d-1); if(l[dq]>r[dq]){l[dq]=inf; r[dq]=-inf;}
            }
            //if(j==2)cout<<l[dq]<<" "<<r[dq]<<" "<<j<<" "<<i<<" "<<dq<<endl;
            if(l[dq]-y[j]>a[dq].x-x[j])ans[j]=0; else if(y[j]-r[dq]>a[dq].x-x[j])ans[j]=0; else ans[j]=1;
        }
        for(int j=1;j<=k;j++)cao(j);
    }
    for(int i=1;i<=q;i++)puts(ans[i]?"1":"0");
}
