#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
const int N=250005;
const ll inf=4e18;
//边全负 
ll dp[N],dq;
vector<PI > v[N];
int n,lim,x[N],y[N],a[N],b[N];
void solve(int p,int fa){
    dp[p]=0; ll mx=0;
    for(auto i:v[p])if(i.first!=fa){
        solve(i.first,p);
        ll t=dp[i.first]+i.second;
        if(t>dp[p]){mx=dp[p];dp[p]=t;}
        else mx=max(mx,t);
    }
    dq=max(dq,mx+dp[p]);
}
pair<ll,int> gao(int X){
    for(int i=1;i<=n;i++)v[i].clear();
    for(int i=1;i<n;i++){
        v[x[i]].push_back(mp(y[i],a[i]+b[i]*X));
        v[y[i]].push_back(mp(x[i],a[i]+b[i]*X));
    }
    dq=-inf;
    solve(1,0);
    return mp(dq,X);
}
int main(){
    freopen("tree.in","r",stdin); freopen("tree.out","w",stdout);
    n=read(); lim=read(); 
    for(int i=1;i<n;i++){
        x[i]=read(); y[i]=read(); a[i]=read(); b[i]=read();
    }
    int l=0,r=lim;
    pair<ll,int> ans=mp(inf,0);
    while(l<=r){
        int len=(r-l)/3,lmid=l+len,rmid=r-len;
        pair<ll,int> t1=gao(lmid),t2=gao(rmid);
        ans=min(ans,min(t1,t2));
        if(t1<t2)r=rmid-1; else l=lmid+1;
    }
    cout<<ans.second<<endl<<ans.first<<endl;
}
/*
直径最小

最长的路径最小

二分一个值以后check x的区间

然后发现是三分傻逼题qwq 
*/
