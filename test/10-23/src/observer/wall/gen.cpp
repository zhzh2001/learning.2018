#include <fstream>
#include <random>
#ifdef _WIN32
#include <windows.h>
#else
#include <ctime>
#endif
#include <map>
using namespace std;
ofstream fout("wall.in");
const int n = 2, m = 5, k = 3, q = 100;
int main()
{
#ifdef _WIN32
	minstd_rand gen(GetTickCount());
#else
	minstd_rand gen(time(NULL));
#endif
	uniform_int_distribution<> dd(1, n - 1), dn(1, n), dm(1, m), dir(0, 1);
	int d = 1;
	fout << n << ' ' << m << ' ' << d << endl;
	fout << k << endl;
	map<int, int> range;
	for (int i = 1; i <= k;)
	{
		int x = dn(gen), y = dm(gen);
		if (x + d - 1 <= n && range.find(y) == range.end())
		{
			fout << x << ' ' << y << ' ' << (dir(gen) ? '+' : '-') << endl;
			i++;
			range[y] = x;
		}
	}
	fout << q << endl;
	for (int i = 1; i <= q;)
	{
		int x = dn(gen), y = dm(gen);
		if (range.find(y) == range.end() || (x >= range[y] && x <= range[y] + d - 1))
		{
			fout << x << ' ' << y << endl;
			i++;
		}
	}
	return 0;
}
