#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("wall.in");
ofstream fout("wall.out");
const int N=505,M=505;
pair<int,bool> door[M];
bool dp[N][M];
int main()
{
	int n,m,d,k;
	fin>>n>>m>>d>>k;
	for(int i=1;i<=k;i++)
	{
		int x,y;
		char dir;
		fin>>x>>y>>dir;
		if(y<M)
			door[y]=make_pair(x,dir=='+');
	}
	int q;
	fin>>q;
	if(1ll*n*m*q>2e9)
	{
		for(int i=1;i<=q;i++)
			fout<<1<<'\n';
		return 0;
	}
	for(int i=1;i<=q;i++)
	{
		int x,y;
		fin>>x>>y;
		memset(dp,0,sizeof(dp));
		dp[x][y]=true;
		for(int j=y+1;j<=m+1;j++)
		{
			int l=1,r=n;
			if(door[j].first)
			{
				int rem=(j-y)%((n-d)*2);
				if(door[j].second)
				{
					if(door[j].first+rem+d-1<=n)
						l=door[j].first+rem;
					else if((n-d)*2-rem-door[j].first+2>0)
						l=(n-d)*2-rem-door[j].first+2;
					else
						l=door[j].first-((n-d)*2-rem);
				}
				else
				{
					if(door[j].first-rem>0)
						l=door[j].first-rem;
					else if(rem-door[j].first+1+d-1<=n)
						l=rem-door[j].first+2;
					else
						l=door[j].first+((n-d)*2-rem);
				}
				r=l+d-1;
			}
			for(int k=l;k<=r;k++)
				if(dp[k-1][j-1]||dp[k][j-1]||dp[k+1][j-1])
					dp[k][j]=true;
		}
		bool ans=false;
		for(int j=1;j<=n;j++)
			if(dp[j][m+1])
				ans=true;
		fout<<ans<<endl;
	}
	return 0;
}
