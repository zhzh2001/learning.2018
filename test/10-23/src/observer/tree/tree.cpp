#include <fstream>
#include <vector>
#include <cstring>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");

typedef long long int64;

const int kMaxN = 3e5+5;

struct Edge {
    int oth, x, y;

    int64 GetCost(int time) {
        return x + 1LL * y * time;
    }
};

vector<Edge> vertex[kMaxN];

bool visited[kMaxN];
int64 mx = 0;

Edge edge_to_parent[kMaxN];

void Relabel() {
    vector<int> bfs_queue = {1};
    visited[1] = true;
    
    int first = 0;
    while (first < (int)bfs_queue.size()) {
        int node = bfs_queue[first++];
        
        for (auto edge : vertex[node]) {
            int initial_node = edge.oth;
            if (visited[initial_node]) {
                continue;
            }
            
            visited[initial_node] = true;
            edge.oth = first - 1;
            edge_to_parent[bfs_queue.size()] = edge;
            bfs_queue.push_back(initial_node);
        }
    }
}

int n;

int64 dp[kMaxN];
int64 FindDiameter(int time) {
    memset(dp, 0, sizeof(dp));
    
    int64 mx_ans = 0;
    
    for (int i = n - 1; i >= 0; i -= 1) {
        if (i == 0) {
            break;
        }
        
        auto edge = edge_to_parent[i];
        int parent = edge.oth; 
        int64 current_cost = edge.GetCost(time) + dp[i];
        mx_ans = max(mx_ans, dp[parent] + current_cost);
        dp[parent] = max(dp[parent], current_cost);
    }

    return mx_ans;
}

int main() {
    int d; fin >> n >> d;
    for (int i = 1; i < n; i += 1) {
        int a, b, x, y; fin >> a >> b >> x >> y;   
        vertex[a].push_back({b, x, y});
        vertex[b].push_back({a, x, y});
    }

    Relabel();

    int64 mn_diameter = 1e18;
    int where = 0;

    auto Update = [&](int time, int64 diameter) {
        if (mn_diameter > diameter) {
            mn_diameter = diameter;
            where = time;
        } else if (mn_diameter == diameter and time < where) {
            where = time;
        }
    };

    Update(0, FindDiameter(0));
    Update(d, FindDiameter(d));

    int a = 0;
    int b = d;
    while (b - a >= 5) {
        int mid = (a + b) / 2;
        auto rezA = FindDiameter(mid);
        auto rezB = FindDiameter(mid + 1);
        Update(mid, rezA);
        Update(mid + 1, rezB);
        if (rezA > rezB)
            a = mid;  // (A)
        else
            b = mid + 1;
    }

    for (int i = a + 1; i <= b; ++i)
        Update(i, FindDiameter(i));

    fout << where << '\n' << mn_diameter << '\n';

    return 0;
}
