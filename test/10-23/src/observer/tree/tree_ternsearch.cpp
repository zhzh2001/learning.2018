#include <fstream>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N = 250005;
long long INF = 1e18;
int n, lim, head[N], v[N * 2], nxt[N * 2], a[N * 2], b[N * 2], e;
long long f[N], g[N];
inline void add_edge(int u, int v, int a, int b)
{
	::v[++e] = v;
	::a[e] = a;
	::b[e] = b;
	nxt[e] = head[u];
	head[u] = e;
}
void dfs(int k, int fat, int delta)
{
	f[k] = g[k] = 0;
	for (int i = head[k]; i; i = nxt[i])
		if (v[i] != fat)
		{
			dfs(v[i], k, delta);
			long long now = f[v[i]] + a[i] + b[i] * delta;
			if (now > f[k])
			{
				g[k] = f[k];
				f[k] = now;
			}
			else if (now > g[k])
				g[k] = now;
		}
}
long long eval(int delta)
{
	dfs(1, 0, delta);
	long long ans = 0;
	for (int i = 1; i <= n; i++)
		ans = max(ans, f[i] + g[i]);
	return ans;
}
int main()
{
	fin >> n >> lim;
	for (int i = 1; i < n; i++)
	{
		int u, v, a, b;
		fin >> u >> v >> a >> b;
		add_edge(u, v, a, b);
		add_edge(v, u, a, b);
	}
	int l = 0, r = lim;
	while (l < r)
	{
		int lmid = l + (r - l) / 3, rmid = r - (r - l) / 3;
		if (eval(lmid) < eval(rmid))
			r = rmid - 1;
		else
			l = lmid + 1;
	}
	fout << l << endl
		 << eval(l) << endl;
	return 0;
}