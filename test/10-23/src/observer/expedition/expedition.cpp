#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("expedition.in");
ofstream fout("expedition.out");
int main()
{
	double a,vm,l,d,w;
	fin>>a>>vm>>l>>d>>w;
	double ans=.0;
	double t1=vm/a;
	double x1=.5*a*t1*t1;
	double tt1=w/a;
	double xx1=.5*a*tt1*tt1;
	if(w>=vm)
	{
		if(x1>l)
			ans=sqrt(l*2./a);
		else
			ans=t1+(l-x1)/vm;
	}
	else
	{
		double t3=(vm-w)/a;
		double x3=(vm+w)*.5*t3;
		if(x1+x3<=d)
		{
			double x2=d-x1-x3;
			double t2=x2/vm;
			ans+=t2;
		}
		else
		{
			double v=sqrt(w*w*.5+a*d);
			t1=v/a;
			t3=(v-w)/a;
		}
		ans+=t1+t3;
		double t4=(vm-w)/a;
		double x4=(vm+w)*.5*t4;
		if(d+x4<=l)
		{
			double x5=l-d-x4;
			double t5=x5/vm;
			ans+=t5;
		}
		else
		{
			double v=sqrt(2*a*(l-d)+w*w);
			t4=(v-w)/a;
		}
		ans+=t4;
	}
	fout.precision(15);
	fout<<fixed<<ans<<endl;
	return 0;
}
