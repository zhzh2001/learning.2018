#include<bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0,f=0;char ch=getchar();
  for(;ch<'0'||ch>'9';ch=getchar())
    if (ch=='-') f=1;
  for(;ch>='0'&&ch<='9';ch=getchar())
    x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const int N = 250005;
int n, lim, cnt, head[N], top;
long long ans;
struct edge {
  int nxt, to, a, b;
  long long w;
}e[N<<1];
inline void insert(int u, int v, int a, int b) {
  e[++cnt] = (edge){head[u], v, a, b}; head[u] = cnt;
  e[++cnt] = (edge){head[v], u, a, b}; head[v] = cnt;
}
long long f[N];
inline void dfs(int x, int fa) {
  f[x] = 0;
  for (int y, i = head[x]; i; i = e[i].nxt)
    if ((y = e[i].to)!= fa) {
      dfs(y, x);
      f[x] = max(f[x], f[y]+e[i].w);
    }
}
inline void Dfs(int x, int fa) {
  bool flag = 0;
  long long mx = 0;
  for (int y, i = head[x]; i; i = e[i].nxt)
    if ((y = e[i].to)!= fa) {
      Dfs(y, x);
      if ((!flag)&& (f[y]+e[i].w == f[x])) {
        flag = 1;
        continue;
      }
      mx = max(mx, f[y]+e[i].w);
    }
  ans = max(ans, f[x]+mx);
}
inline long long check(int det) {
  for (int i = 1; i <= cnt; i++)
    e[i].w = 1ll*e[i].a+1ll*e[i].b*det;
  dfs(1, 0);
  ans = 0;
  Dfs(1, 0);
  return ans;
}
int main() {
  freopen("tree.in","r",stdin);
  freopen("tree.out","w",stdout);
  n = read();
  lim = read();
  for (int i = 1; i < n; i++) {
    int u = read(), v = read(), a = read(), b = read();
    insert(u, v, a, b);
  }
  int l = 0, r = lim;
  while (r-l > min(5, lim)) {
    int midl = l+(r-l)/3, midr = r-(r-l)/3;
    long long Midl = check(midl);
    long long Midr = check(midr);
  //  cout << midl <<" "<< Midl << endl;
  //  cout << midr <<" "<< Midr << endl;
    if (Midl <= Midr) {
      r = midr;
    }  else{
      l = midl;
    }
  }
  long long Ans = check(l);
  int ansmx = l;
  for (;l <= r; l++)
    if (Ans > check(l))
      Ans = check(l), ansmx = l;
  printf("%d\n", ansmx);
  printf("%lld\n", Ans);
  return 0;
}
