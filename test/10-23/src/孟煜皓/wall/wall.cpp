#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
int k, q;
int main(){
	freopen("wall.in", "r", stdin);
	freopen("wall.out", "w", stdout);
	read(), read(), read(), k = read();
	for (register int i = 1; i <= k; ++i) read(), read();
	q = read();
	for (register int i = 1; i <= q; ++i) printf("1\n");
}
