#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 250005
int n, lim, delta, lp;
struct node{
	int u, v, a, b;
}E[N];
int edge, to[N << 1], tw[N << 1], pr[N << 1], hd[N];
void addedge(int u, int v, int w){
	to[++edge] = v, tw[edge] = w, pr[edge] = hd[u], hd[u] = edge;
}
int dis[N], ans;
void dfs(int u, int fa = 0){
	ans = std :: max(ans, dis[u]);
	for (register int i = hd[u], v; i; i = pr[i])
		if ((v = to[i]) != fa) dis[v] = dis[u] + tw[i], dfs(v, u);
}
int LP(int d){
	edge = 0, memset(hd, 0, sizeof hd);
	for (register int i = 1, w; i < n; ++i)
		w = E[i].a + E[i].b * d, addedge(E[i].u, E[i].v, w), addedge(E[i].v, E[i].u, w);
	ans = 0;
	for (register int i = 1; i <= n; ++i) dis[i] = 0, dfs(i);
	return ans;
}
int main(){
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	n = read(), lim = read();
	for (register int i = 1; i < n; ++i)
		E[i].u = read(), E[i].v = read(), E[i].a = read(), E[i].b = read();
	lp = 0x7f7f7f7f;
	for (register int i = 0; i <= lim; ++i){
		int s = LP(i);
		if (s < lp) delta = i, lp = s;
	}
	printf("%d\n%d", delta, lp);
}
