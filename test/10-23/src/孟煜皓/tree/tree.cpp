#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 250005
int n, lim;
struct node{
	int u, v, a, b;
}E[N];
int edge, to[N << 1], tw[N << 1], pr[N << 1], hd[N];
void addedge(int u, int v, int w){
	to[++edge] = v, tw[edge] = w, pr[edge] = hd[u], hd[u] = edge;
}
long long dis[N], Dis[N], ans;
void dfs(int u, int fa = 0){
	dis[u] = 0, Dis[u] = 0; 
	for (register int i = hd[u], v; i; i = pr[i])
		if ((v = to[i]) != fa){
			dfs(v, u);
			register long long w = dis[v] + tw[i];
			if (w > dis[u]) Dis[u] = dis[u], dis[u] = w;
			else if (w > Dis[u]) Dis[u] = w;
		}
	ans = std :: max(ans, dis[u] + Dis[u]);
}
long long LP(int d){
	edge = 0;
	for (register int i = 1; i <= n; ++i) hd[i] = 0;
	for (register int i = 1, w; i < n; ++i)
		w = E[i].a + E[i].b * d, addedge(E[i].u, E[i].v, w), addedge(E[i].v, E[i].u, w);
	ans = 0, dfs(1);
	return ans;
}
int main(){
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	n = read(), lim = read();
	for (register int i = 1; i < n; ++i)
		E[i].u = read(), E[i].v = read(), E[i].a = read(), E[i].b = read();
	if (1ll * n * lim <= 50000000){ 
		int delta = 0;
		long long lp = 1000000000000000000ll;
		for (register int i = 0; i <= lim; ++i){
			long long s = LP(i);
			if (s < lp) delta = i, lp = s;
		}
		return printf("%d\n%lld", delta, lp), 0;
	}
	int l = 0, r = lim;
  	while (l < r - 1){
      	int mid = l + r >> 1, Mid = mid + r >> 1;
      	if (LP(mid) > LP(Mid)) l = mid; else r = Mid;
	}
	long long sl = LP(l), sr = LP(r);
	if (sl <= sr) printf("%d\n%lld", l, sl);
	else printf("%d\n%lld", r, sr);
}

