#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll tot,n,lim,ans1,ans2,e[1000001],next[1000001],head[1000001],jia1[1000001],jia2[1000001],mx[1000001],cmx[1000001];
void build(ll t,ll k,ll A,ll B){
	tot++;
	e[tot]=k;jia1[tot]=A;jia2[tot]=B;
	next[tot]=head[t];head[t]=tot;
}
ll dfs(ll x,ll fa,ll dlt){
	ll i,hh=-1e18,now,flag=0;
	for(i=head[x];i;i=next[i]){
		if(e[i]==fa)continue;
		hh=max(hh,dfs(e[i],x,dlt));
		now=mx[e[i]]+jia1[i]+jia2[i]*dlt;
		if(now>mx[x]){
			cmx[x]=mx[x];mx[x]=now;
		}
		 else if(now>cmx[x]){
		 	cmx[x]=now;
		 }
		flag=1; 
	}
	if(!flag)mx[x]=cmx[x]=0;
	return max(hh,mx[x]+cmx[x]);
}
int main(){
freopen("tree.in","r",stdin);
freopen("tree.out","w",stdout);	
	ll i,t,k,A,B;
	scanf("%lld%lld",&n,&lim);
	for(i=1;i<n;i++){
		scanf("%lld%lld%lld%lld",&t,&k,&A,&B);
		build(t,k,A,B);build(k,t,A,B);
	}
	memset(mx,-127,sizeof(mx));
	memset(cmx,-127,sizeof(cmx));
	ans1=dfs(1,0,0);
	memset(mx,-127,sizeof(mx));
	memset(cmx,-127,sizeof(cmx));
	ans2=dfs(1,0,lim);
	if(ans1<=ans2){
		puts("0");
		printf("%lld",ans1);
	}
	 else{
	 	printf("%lld\n%lld",lim,ans2);
	 }
}
