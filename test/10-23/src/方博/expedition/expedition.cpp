#include<bits/stdc++.h>
using namespace std;
#define ll long long
const double eps=1e-9;
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
double a,vm,l,d,li,ans;
double solve(double v,double a,double vm,double l)
{
	double ret=0;
	if(l==0)return 0;
	if(2*a*l>=vm*vm-v*v)ret=(vm-v)/a+(l-(vm*vm-v*v)/a/2)/vm;//一直加速至最大限速 
	else {
		double L=0,R=10000;
		while(L+eps<=R){
			double mid=(L+R)/2;
			if(mid*v+mid*mid*a/2-l>eps)R=mid;
			else L=mid;
		}
//		printf("%.9lf\n",L);
		ret=L;
	}
	return ret;
}
int main()
{
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a=read(),vm=read(),l=read(),d=read(),li=read();
	if(vm<=li){//若限速大于最大速度 
		ans=solve(0,a,vm,l);
	}
	else {
		if(d*2<=li)ans=solve(0,a,vm,l);//若关卡不用减速
		else {
			double L=0,R=10000;
			double vn;
			while(L+eps<=R){
				double mid=(L+R)/2;
				vn=a*mid;
				if(a*mid*mid+(vn*vn-li*li)/a<=d*2)L=mid;
				else R=mid;
			}
//			printf("%.8lf\n",L);
			if(vn<=vm){//如果减速时速度没超过最大速度 
				ans=L+(vn-li)/a;
				ans+=solve(li,a,vm,l-d);
			}else {
				vn=vm;
				ans=(l-(vn*vn/a/2+(vn*vn-li*li)/a/2))/vn+vn/a+(vn-li)/a;
				ans+=solve(li,a,vm,l-d);
			}
		}
	}
	printf("%.10lf",ans);
}
