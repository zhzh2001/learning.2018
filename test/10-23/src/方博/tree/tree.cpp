#include<bits/stdc++.h>
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=250005;
using namespace std;
struct edge{
	ll x,y,a,b;
}e[N];
ll head[N],nxt[2*N],tail[2*N],c[2*N],tot;
ll dep[N];
ll mx1[N],mx2[N],anst[N];
ll n,li;
inline void addto(int x,int y,ll w){
	tail[++tot]=y;
	c[tot]=w;
	nxt[tot]=head[x];
	head[x]=tot;
}
void dfs(int x,int fa)
{
	mx1[x]=0,mx2[x]=0;
	for(int i=head[x];i;i=nxt[i]){
		if(tail[i]!=fa){
			dfs(tail[i],x);
			if(mx1[tail[i]]+c[i]>mx1[x])mx2[x]=mx1[x],mx1[x]=mx1[tail[i]]+c[i];
			else if(mx1[tail[i]]+c[i]>mx2[x])mx2[x]=mx1[tail[i]]+c[i];
		}
	}
	anst[x]=mx1[x]+mx2[x];
}
inline ll solve(ll x){
	memset(head,0,sizeof(head));
	tot=0;
	for(int j=1;j<n;j++){
		addto(e[j].x,e[j].y,e[j].a+e[j].b*x);
		addto(e[j].y,e[j].x,e[j].a+e[j].b*x);
	}
	dfs(1,0);
	int ans=0;
	for(int j=1;j<=n;j++)
		if(!ans||anst[j]>anst[ans])ans=j;
	return anst[ans];
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),li=read();
	for(int i=1;i<n;i++)
		e[i].x=read(),e[i].y=read(),e[i].a=read(),e[i].b=read();
	ll l=0,r=li;
	while(l<r-3){
		ll mid1=(r-l)/3+l,mid2=(r-l)/3*2+l;
		ll ans1=solve(mid1),ans2=solve(mid2);
		if(ans1<=ans2)r=mid2;
		else l=mid1;
	}
	ll ansa=l,ansb=solve(l);
	for(ll i=l+1;i<=r;i++){
		ll ans=solve(i);
		if(ans<ansb)ansa=i,ansb=ans;
	}
	writeln(ansa);
	writeln(ansb);
	return 0;
}
