#include <bits/stdc++.h>
#define LL long long
#define inf (1ll<<60)
#define GG long long 
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) { write(x); puts(""); } 

const int N = 250011; 
LL n,tot,R,det,head[N],g[N],f[N],lim,wzt,anss;
struct edge{ LL to,next,a,b; }e[N*2];
void add(LL u,LL v,LL w1,LL w2){
	e[++tot]=(edge){v,head[u],w1,w2}; head[u]=tot;
}
void dfs(LL u,LL last){
	for (LL i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue; dfs(v,u);
		if (f[v]+e[i].a+e[i].b*det>f[u]) g[u]=f[u],f[u]=f[v]+e[i].a+e[i].b*det;
		else g[u]=max(g[u],f[v]+e[i].a+e[i].b*det);
	}
	R=max(R,max(f[u]+g[u],f[u]));
	f[u]=max(f[u],0ll); g[u]=max(g[u],0ll);
}
LL work(LL x){
	det=x; R=0;
	For(i, 1, n) f[i]=g[i]=-inf;
	dfs(1,-1);
	return R;
}


struct node{
	int x, y, a, b; 
}a[N];
LL dep[N]; 
int fake;  



void guess(int u, int fa, int &root) {
	if(dep[root] < dep[u]) root = u; 
	for(int i=head[u]; i; i=e[i].next) {
		int v = e[i].to; 
		if(v != fa) {
			dep[v] = dep[u] + e[i].a; 
			guess(v, u, root); 
		}
	}
}

inline LL calc() {
	dep[0] = 1e18; dep[0] = -dep[0]; int S = 0, T = 0; ; 
	For(i, 1, n) dep[i] = 0; 
	guess(1, -1, S); 
	For(i, 1, n) dep[i] = 0; 
	guess(S, -1, T); 
	return dep[T]; 
}

inline void work_1() {
	wzt = 1e18; 
	For(i, 1, n-1) {
		a[i].x=read(); a[i].y=read(); a[i].a=read(); a[i].b=read(); 
		add(a[i].x, a[i].y, a[i].a, a[i].b); 
		add(a[i].y, a[i].x, a[i].a, a[i].b); 
	} 
	LL tmp = calc(); 
	if(tmp < wzt) wzt = tmp, fake = 0; 
	For(i, 1, lim) {
		For(j, 1, tot) e[j].a += e[j].b; 
		LL tmp = calc(); 
		if(tmp < wzt) wzt = tmp, fake = i; 
	}
	writeln(fake); 
	writeln(wzt); 
	exit(0); 
}



int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(); lim=read();
	if(n<=100 && lim<=500000) work_1(); 
	
	
	For(i, 2, n){
		LL u=read(),v=read(),w1=read(),w2=read();
		add(u,v,w1,w2); add(v,u,w1,w2);
	}
	LL l=0,r=lim; anss=0; wzt=work(0);
   while (l<=r){
		LL mid1=(l+r)>>1;
		LL mid2=(mid1+r)>>1;
		LL t1=work(mid1),t2=work(mid2);
		if (t1<wzt) anss=mid1,wzt=t1;
		if (t2<wzt) anss=mid2,wzt=t2;
		if (t1<t2) r=mid2-1;
		else l=mid1+1;
	}
	writeln(anss); 
	writeln(wzt); 
}
