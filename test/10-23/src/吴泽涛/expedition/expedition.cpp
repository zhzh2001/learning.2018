#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
double a, vm, L, d, limit;
int main() {
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	scanf("%lf%lf%lf%lf%lf", &a, &vm, &L, &d, &limit);
	double t = sqrt(d*2/a);
	if (a*t <= vm) {
    	if (a*t <= limit) {
      		double t2 = sqrt(L*2/a);
      		if (a*t2 <= vm) {
        		printf("%.20lf\n", t2);
      		} else {
        		double t3 = vm/a;
        		double t4 = (L-t3*t3*a/2)/vm;
        		printf("%.20lf\n", t3+t4);
      		}
    	} else {
      		double t2 = limit/a, x = d-a*t2*t2/2;
      		double v = sqrt(a*x+limit*limit);
      		double t3 = t2+(v-limit)/a*2;
      		x = L-d;
      		v = sqrt(2*a*x+limit*limit);
      		if (v <= vm) {
        		double t4 = (v-limit)/a;
        		printf("%.20lf\n", t3+t4);
      		} else {
        		double t4 = (vm - limit)/a;
        		x = x-t4*(vm+limit)/2;
        		double t5 = x/vm;
        		printf("%.20lf\n", t3+t4+t5);
      		}
    	}
  } else {
    	if (a*t <= limit) {
      		double t2 = vm/a;
      		double t3 = (L-t2*vm/2)/vm;
      		printf("%.20lf\n", t2+t3);
    	} else {
      		double t2 = limit/a;
      		double x = d-limit*t2/2;
      		double v = sqrt(a*x+limit*limit);
      		double t3;
      		if (v <= vm) t3 = t2+(v-limit)/a*2;
      		else {
        		double t = (vm-limit)/a;
        		t3 = t2+(vm-limit)/a*2;
        		x = x-t*(vm+limit);
        		t3 += x/vm;
      		}
      		x = L-d;
      		t2 = (vm-limit)/a;
      		double t4;
      		if ((vm+limit)*t2/2 < x) {
        		x -= (vm+limit)*t2/2;
        		t4 = t2+x/vm;
      		} else {
        		v = sqrt(2*a*x+limit*limit);
        		t4 = (v-limit)/a;
      		}
      		printf("%.20lf\n", t3+t4);
    	}
  	}
}



/*



*/
