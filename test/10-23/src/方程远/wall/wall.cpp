#include<bits/stdc++.h>
#define int long long
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
struct Edge{
	int x,y,z;
}door[100001];
bool cmp(Edge x,Edge y){
	return x.y<y.y;
}
char ch;
int n,m,d,k,q,x,y,place,X,Y,Z,a,b,far,tot,t;
signed main(){
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	read(n);
	read(m);
	read(d);
	read(k);
	for(int i=1;i<=k;i++){
		read(door[i].x);
		read(door[i].y);
		ch=getchar();
		door[i].z=0;
		if(ch=='+')
			door[i].z=1;
	}
	sort(door+1,door+1+k,cmp);
	read(q);
	while(q--){
		tot=0;
		read(x);
		read(y);
		a=b=x;
		place=1;
		while(door[place].y<=y&&place<=k+1)
			place++;
		if(place>k){
			puts("1");
			goto next;
		}
		while(place<=k){
			far=door[place].y-y;
			tot+=far;
			t=tot%((n-d)*2);
			X=door[place].x;
			Y=X+d-1;
			if(door[place].z){
				if(n-Y>=t){
					Y+=t;
					X+=t;
				}
				else{
					t-=(n-Y);
					Y=n;
					X=Y-d+1;
					X-=t;
					Y-=t;
				}
			}
			else{
				if(X-t>=1){
					Y-=t;
					X-=t;
				}
				else{
					t-=(X-1);
					X=1;
					Y=X+d-1;
					X+=t;
					Y+=t;
				}
			}
			place++;
			bool flag=0;
			if(a<=X){
				if(X-a<=far){
					flag=1;
					a=X;
				}
			}
			else
				if(a<=Y){
					flag=1;
					a=max(a-far,X);
				}
				else{
					if(a-Y<=far){
						flag=1;
						a=max(a-far,X);
					}
				}
			if(b<=X){
				if(X-b<=far){
					flag=1;
					b=min(b+far,Y);
				}
			}
			else
				if(b<=Y){
					flag=1;
					b=min(b+far,Y);
				}
				else{
					if(b-Y<=far){
						flag=1;
						b=Y;
					}
				}
			if(!flag){
				puts("0");
				goto next;
			}
		}
		puts("1");
		next:;
	}
	return 0;
}
