#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
LL n,m;
struct edge{
	LL x,t,a,b;
	edge(){}
	edge(LL _x,LL _t,LL _a,LL _b){
		x=_x;
		t=_t;
		a=_a;
		b=_b;
	}
};
const LL oo=1ll<<60;
edge e[2*250003];
LL en[250003],et=0;
void adde(int x,int y,int a,int b){
	e[++et]=edge(en[x],y,a,b);
	en[x]=et;
	e[++et]=edge(en[y],x,a,b);
	en[y]=et;
}
LL dis[250003],vdis[250003],delta;
bool add(LL&a,LL&b,const LL&c){
	if(c>=a){
		b=a;
		a=c;
		return 1;
	}else if(c>b){
		b=c;
		return 1;
	}
	return 0;
}
void dfs(LL u,LL fa){
	dis[u]=0;
	vdis[u]=-oo;
	LL vv;
	for(LL i=en[u];i!=-1;i=e[i].x){
		if(e[i].t==fa)continue;
		vv=e[i].a+e[i].b*delta;
		dfs(e[i].t,u);
		if(!add(dis[u],vdis[u],dis[e[i].t]+vv))
			add(dis[u],vdis[u],vdis[e[i].t]+vv);
	}
}
LL dfa(LL u,LL fa,LL d){
	if(fa!=-1){
		if(dis[fa]!=dis[u]+d){
			add(dis[u],vdis[u],dis[fa]+d);
		}else{
			add(dis[u],vdis[u],vdis[fa]+d);
		}
	}
	LL ans=dis[u];
	for(LL i=en[u];i!=-1;i=e[i].x){
		if(e[i].t==fa)continue;
		ans=max(ans,dfa(e[i].t,u,e[i].a+e[i].b*delta));
	}
	return ans;
}
LL ck(LL v){
	delta=v;
	dfs(1,-1);
	return dfa(1,-1,0);
}
LL bbs(LL l,LL r,LL p){
	LL mid;
	while(l<r){
		mid=(l+r)/2;
		if(ck(mid)>p)l=mid+1;
		else r=mid;
	}
	return l;
}
LL bs(LL l,LL r){
	LL lm,rm;
	while((r-l)/3>0){
		lm=(r-l)/3;
		rm=r-lm;
		lm+=l;
		if(ck(lm)<=ck(rm))r=rm;
		else l=lm;
	}
	LL ans=ck(r),ansp=r;
	for(LL i=r-1;i>=l;i--){
		LL ai=ck(i);
		if(ai<=ans){
			ans=ai;
			ansp=i;
		}
	}
	return bbs(0,ansp,ans);
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++)en[i]=-1;
	for(int i=1;i<n;i++){
		static LL x,y,a,b;
		x=read();
		y=read();
		a=read();
		b=read();
		adde(x,y,a,b);
	}
	LL ans=bs(0,m);
	printf("%lld\n%lld\n",ans,ck(ans));
	return 0;
}
