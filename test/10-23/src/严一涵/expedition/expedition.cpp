#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
LL a,v,l,d,s;
double ans;
const double eps=1e-9;
inline void pa(double l){
	if(v*v>=2*a*l){
		ans+=sqrt(2.0*l/a);
	}else{
		ans+=0.5*v/a+1.0*l/v;
	}
}
int main(){
	freopen("expedition.in","r",stdin);
	freopen("expedition.out","w",stdout);
	a=read();
	v=read();
	l=read();
	d=read();
	s=read();
	ans=0;
	if(s>=v||s*s>=2*a*d){
		if(v*v>=2*a*l){
			ans+=sqrt(2.0*l/a);
		}else{
			ans+=0.5*v/a+1.0*l/v;
		}
		printf("%.15lf\n",ans);
		return 0;
	}
	double p1=0.5*s*s/a;
	double p2=0.5*v*v/a;
	if(p2-p1<d-p2){
		ans+=2.0*(v-s)/a;
		ans+=1.0*(d-p2-p2+p1)/v;
	}else{
		p2=(d+p1)/2.0;
		ans+=2*sqrt(2.0*p2/a)-2.0*s/a;
	}
	pa(l-d+p1);
	printf("%.15lf\n",ans);
	return 0;
}
