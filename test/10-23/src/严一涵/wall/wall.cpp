#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
inline int rc(){
	int dd;
	while(!isgraph(c))dd;
	return c;
}
#undef dd
struct sta{
	LL a,b;
}st[100003];
bool operator<(sta a,sta b){
	return a.b<b.b;
}
LL n,m,d;
LL k,q,md;
LL x,y;
void solve(LL x,LL y){
	LL p=1;
	while(st[p].b<y)p++;
	LL t=y,l=x,r=x;
	for(;p<=k;p++){
		l-=st[p].b-t;
		r+=st[p].b-t;
		LL k1=(st[p].a+st[p].b-y)%md;
		if(k1>md/2)k1=md-k1;
		l=max(l,k1+1);
		r=min(r,k1+d);
		t=st[p].b;
		if(l>r){
			puts("0");
			return;
		}
	}
	puts("1");
}
int main(){
	freopen("wall.in","r",stdin);
	freopen("wall.out","w",stdout);
	n=read();
	m=read();
	d=read();
	k=read();
	md=(n-d)*2;
	for(int i=1;i<=k;i++){
		st[i].a=read()-1;
		st[i].b=read();
		if(rc()=='-'){
			st[i].a=md-st[i].a;
			if(st[i].a==md)st[i].a=0;
		}
	}
	sort(st+1,st+k+1);
	q=read();
	for(int i=1;i<=q;i++){
		x=read();
		y=read();
		solve(x,y);
	}
	return 0;
}
