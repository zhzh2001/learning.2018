#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read(){
	GG k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(GG x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(GG x){ write(x);puts("");}

const int N = 3011; 
const double eps = 1e-7;  
struct line{
	int a, b, c; 
}e[N];
struct point{
	double x, y; 
}p[N];
int n, ans; 

inline bool ping(int x, int y) {
	if(e[x].a*e[y].b == e[y].a*e[x].b) return 1; 
	return 0; 
}

point getpoint(line l1,line l2) {
    double d = l1.a * l2.b - l2.a * l1.b;
    point p; 
    p.x = (l2.c * l1.b - l1.c * l2.b) / d;
    p.y = (l2.a * l1.c - l1.a * l2.c) / d;
    return p; 
}

inline bool cmp(point a, point b) {
	return a.y*b.x < b.y*a.x; 
}

inline double sqr(double x) {
	return x * x; 
}

int main() {
	freopen("geometry.in","r",stdin); 
	freopen("geometry.out","w",stdout); 
	n = read(); 
	For(i, 1, n) {
		e[i].a = read(); e[i].b = read(); e[i].c = read(); 
	}
	For(i, 1, n-2) 
	  For(j, i+1, n-1) 
	    For(k, j+1, n) {
	    	int tot = 0; 
	    	if(ping(i, j) || ping(i, k) || ping(j, k)) continue; 
	    	p[++tot] = getpoint(e[i], e[j]); 
	    	p[++tot] = getpoint(e[i], e[k]);
	    	p[++tot] = getpoint(e[j], e[k]);	
			sort(p+1, p+tot+1, cmp); 
			double a = p[1].x*p[3].x + p[1].y*p[3].y; 
			double b = sqrt(sqr(p[1].x) + sqr(p[1].y)) * sqrt(sqr(p[3].x)+sqr(p[3].y)); 
			p[1].x -= p[2].x; p[1].y -= p[2].y;  
			p[3].x -= p[2].x; p[3].y -= p[2].y;  
			double c = p[1].x*p[3].x + p[1].y*p[3].y; 
			double d = sqrt(sqr(p[1].x) + sqr(p[1].y)) * sqrt(sqr(p[3].x)+sqr(p[3].y));
			if( fabs(a/b+c/d)<eps ) ++ans; 
		}
	writeln(ans); 
}


/*
geometry


4 
1 0 0
0 1 0
1 1 -1
1 -1 2


*/





