#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read(){
	GG k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(GG x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(GG x){ write(x);puts("");}

const int N = 200011;
LL n,ans,fa[N],sz[N],cnt[N],flag[N],x[N],a[N],b[N];
struct edge{ 
	LL u,v,w; 
}e[N];

bool cmp(edge x,edge y) { return x.w<y.w; }
LL find(LL x) { return fa[x]==x?x:fa[x]=find(fa[x]); }
bool check(LL num){
	For(i, 1, n) {
		fa[i]=i; sz[i]=x[i]; 
		cnt[i]=1; 
		flag[i]=0;
	}
	For(i, 1, n-1){
		if (e[i].w >= num) break;
		LL p=find(e[i].u), q=find(e[i].v);
		if (p!=q){
			fa[p]=q; sz[q]+=sz[p]; cnt[q]+=cnt[p];
		}
	}
	LL len=0,sum=0;
	For(i, 1, n){
		LL p = find(i);
		if (!flag[p]) {
			a[++len]=cnt[p]; 
			b[len]=sz[p]; 
			sum+=sz[p]; 
			flag[p]=1;
		}
	}
	For(i, 1, len) 
		if (a[i]>sum-b[i]) return 0;
	return 1;
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n = read();
	For(i, 1, n-1){
		e[i].u=read(); e[i].v=read(); e[i].w=read();
	}
	For(i, 1, n) x[i]=read();
	sort(e+1,e+n,cmp);
	LL l=0,r=1e9;
	while (l<=r){
		LL mid=(l+r)/2;
		if (check(mid)) {
			ans=mid; l=mid+1;
		}
		else r=mid-1;
	}
	printf("%lld",ans);
	return 0;
}


