#include <bits/stdc++.h>
#define LL long long
#define GG long long 
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read(){
	GG k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(GG x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(GG x){ write(x);puts("");}

const int N=1e5+11;
int n, m, s, tot, cnt, K; 
LL all; 
int head[N], Log[N];
struct edge{
	int to, pre; 
}e[N*2];

inline void addedge(int x,int y) {
	e[++tot].to = y;
	e[tot].pre = head[x];
	head[x] = tot;
}
int a[3*N],dep[3*N],mn[21][3*N],p[3*N];
inline void dfs(int x, int fa){
    a[++cnt]=x; p[x]=cnt; dep[x]=dep[fa]+1;
    for(int i=head[x]; i; i=e[i].pre){
        int u = e[i].to;
        if(u==fa) continue;
        dfs(u,x);
        a[++cnt]=x;
    }
}

inline void work_1() {
	all = 0; 
	dfs(s, 0);
    for(int i=1;i<=cnt;++i) mn[0][i]=a[i];
    for(int i=1; i<=20; ++i)
        for(int j=1; j+(1<<i)<=cnt; ++j)
            if(dep[mn[i-1][j]]<dep[mn[i-1][j+(1<<(i-1))]])
                mn[i][j]=mn[i-1][j];
            else mn[i][j]=mn[i-1][j+(1<<(i-1))];
    for(int i=1;i<=cnt;++i)Log[i]=Log[i>>1]+1;
    
    For(i, 1, n-1) 
      For(j, i+1, n) {
      	int x = i, y = j; 
        if(p[x]>p[y]) swap(x,y);
        int k=Log[p[y]-p[x]+1], ans;
        if(dep[mn[k][p[x]]]<dep[mn[k][p[y]-(1<<k)+1]])
            ans=mn[k][p[x]];
        else ans=mn[k][p[y]-(1<<k)+1];
        int res = dep[x]+dep[y]-2*dep[ans]; 
        all += res/K+(res%K!=0); 
      }
    writeln(all); 
	exit(0); 
}

struct Node{
   int v,len;
};
LL dp[N];
int sum[N];

void wzt(int root, int father) {
	sum[root] = 1;
   	for(int i=head[root]; i; i=e[i].pre){
    	int son=e[i].to;
    	if(son==father) continue;
    	wzt(son, root);
    	sum[root] += sum[son];
    	dp[root] += dp[son]+(1ll*sum[son]*(n-sum[son]));
	}
}

inline void work_2() {
	wzt(1, -1); 
	writeln(dp[1]); 
	exit(0); 
}


int main() {
	freopen("tree.in", "r", stdin); 
	freopen("tree.out", "w", stdout); 
    n=read(); K = read(); 
	s = 1;
	Log[0]=-1;
    For(i, 1, n-1){
        int x=read(),y=read();
        addedge(x,y); addedge(y,x);
    }
    if(K==1) work_2(); 
    if(n<=4000) work_1(); 
    return 0;
}




/*


6 2
1 2
1 3
2 4
2 5
4 6



*/


