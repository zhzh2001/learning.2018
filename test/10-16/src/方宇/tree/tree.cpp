#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

inline char read()
{
    static const int IN_LEN=1000000;
    static char buf[IN_LEN],*s,*t;
    return (s==t?t=(s=buf)+fread(buf,1,IN_LEN,stdin),(s==t?-1:*s++):*s++);
}
template<class T>
inline void read(T &x)
{
    static bool iosig;
    static char c;
    for (iosig=false,c=read();!isdigit(c);c=read())
	{
        if (c=='-') iosig=true;
        if (c==-1) return;
    }
    for (x=0;isdigit(c);c=read()) x=((x+(x<<2))<<1)+(c^'0');
    if (iosig) x=-x;
}
const int OUT_LEN=10000000;
char obuf[OUT_LEN],*ooh=obuf;
inline void print(char c)
{
    if (ooh==obuf+OUT_LEN) fwrite(obuf,1,OUT_LEN,stdout),ooh=obuf;
    *ooh++=c;
}
template<class T>
inline void print(T x)
{
    static int buf[30],cnt;
    if (x==0) print('0');
    else
	{
        if (x<0) print('-'),x=-x;
        for (cnt=0;x;x/=10) buf[++cnt]=x%10+48;
        while (cnt) print((char)buf[cnt--]);
    }
}
inline void flush()
{
	fwrite(obuf,1,ooh-obuf,stdout);
}

void judge()
{
    freopen("tree.in","r",stdin);
    freopen("tree.out","w",stdout);
}

const int N=110000;
int n,k,dep[N],son[N],size[N],tp[N],fa[N];
long long ans;
struct info
{
	int to,nxt;
}e[N<<1];
int head[N],opt;
void add(int x,int y)
{
	e[++opt].nxt=head[x]; e[opt].to=y; head[x]=opt;
}

void dfs(int u)
{
	dep[u]=dep[fa[u]]+1; size[u]=1;
	for (int i=head[u];i;i=e[i].nxt)
	{
		int k=e[i].to;
		if (k==fa[u]) continue;
		fa[k]=u; dfs(k); size[u]+=size[k];
		if (!son[u]||size[k]>size[son[u]]) son[u]=k;
	}
}
void dfs(int u,int top)
{
	tp[u]=top;
	if (!son[u]) return;
	dfs(son[u],top);
	for (int i=head[u];i;i=e[i].nxt)
	{
		int k=e[i].to;
		if (k==fa[u]||k==son[u]) continue;
		dfs(k,k);
	}
}

int lca(int x,int y)
{
	while (tp[x]!=tp[y])
	{
		if (dep[tp[x]]>=dep[tp[y]]) x=fa[tp[x]];
		else y=fa[tp[y]];
	}
	return dep[x]<dep[y]?x:y;
}

int main()
{
	judge();
	read(n),read(k);
	for (int i=1,x,y;i<n;i++) read(x),read(y),add(x,y),add(y,x);
	dfs(1),dfs(1,1);
	if (k==1) return 0;
	for (int i=1;i<=n;i++)
	{
		for (int j=i+1;j<=n;j++)
		{
			int t=lca(i,j);
			int dis=dep[i]-dep[t]+dep[j]-dep[t];
			int t1=dis/k;
			if (dis%k) t1++;
			ans+=t1;
		}
	}
	print(ans),print('\n');
	return flush(),0;
}
