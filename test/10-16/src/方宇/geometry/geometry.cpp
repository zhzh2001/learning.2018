#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 1010
using namespace std;
int n,a[N],b[N],c[N];
LL ans;
double x[N][N],y[N][N],xx,yy,d1,d2;
const double eps=1e-6;
int main()
{
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	scanf("%d",&n);
	rep(i,1,n) scanf("%d%d%d",&a[i],&b[i],&c[i]);
	rep(i,1,n-1)
	{
		rep(j,i+1,n)
		{
			int a1=a[i],a2=a[j],b1=b[i],b2=b[j],c1=c[i],c2=c[j];
			x[i][j]=x[j][i]=(c1*b2-c2*b1)*1.0/((a2*b1-a1*b2)*1.0);
			y[i][j]=y[j][i]=(c2*a1-a2*c1)*1.0/((a2*b1-a1*b2)*1.0);
		}
	}
	rep(i,1,n-2)
	{
		rep(j,i+1,n-1)
		{
			rep(k,j+1,n)
			{
				double x1=x[i][j],y1=y[i][j];
				double x2=x[j][k],y2=y[j][k];
				double x3=x[i][k],y3=y[i][k];
				bool flag[4];
				memset(flag,0,sizeof(flag));
				if ((x1!=x2)||(y1!=y2)) flag[1]=1;
				if ((x2!=x3)||(y2!=y3)) flag[2]=1;
				if ((x3!=x1)||(y3!=y1)) flag[3]=1;
				if ((!flag[1])&&(!flag[2])&&(!flag[3])){xx=x1,yy=y1;}
					else if (flag[1]&&flag[2]&&flag[3])
				{
					xx=(x1*x1-x2*x2+y1*y1-y2*y2)*(y1-y3)*1.0;
					xx=xx-(x1*x1-x3*x3+y1*y1-y3*y3)*(y1-y2)*1.0;
					xx=xx/(2*(y1-y3)*(x1-x2)*1.0-2*(y1-y2)*(x1-x3)*1.0);
					yy=(x1*x1-x2*x2+y1*y1-y2*y2)*(x1-x3)*1.0;
					yy=yy-(x1*x1-x3*x3+y1*y1-y3*y3)*(x1-x2)*1.0;
					yy=yy/(2*(y1-y2)*(x1-x3)*1.0-2*(y1-y3)*(x1-x2)*1.0);
				}
				d1=(xx*xx+yy*yy);
				d2=((xx-x1)*(xx-x1)+(yy-y1)*(yy-y1));
				if (d1<d2) {double tmp=d1;d1=d2;d2=tmp;}
				if (d1-d2<=eps) ans++;
			}
		}
	}
	printf("%lld",ans);
	return 0;
}
