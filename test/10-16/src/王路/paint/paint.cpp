#pragma GCC optimize 2
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 1e5 + 5;
int n, cnt[kMaxN], head[kMaxN], ecnt, cc[kMaxN];
struct Edge { int to, nxt, w; } e[kMaxN << 1];
inline void Insert(int u, int v, int w) {
  e[++ecnt] = (Edge) { v, head[u], w };
  head[u] = ecnt;
}
bool Solve(int x, int mid, int f = 0, int d = 0) {
//  fprintf(stderr, "%d, %d, %d, %d\n", x, mid, f, d);
  if (cc[x] && d >= mid) {
    --cc[x];
    return true;
  }
  bool ret = false;
  for (int i = head[x]; i; i = e[i].nxt) {
    int to = e[i].to; 
    if (f == to)
      continue;
    ret |= Solve(to, mid, x, max(d, e[i].w));
    if (ret)
      return true;
  }
  return false;
}
inline bool Check(int mid) {
  for (int i = 1; i <= n; ++i)
    cc[i] = cnt[i];
  for (int i = 1; i <= n; ++i) {
    if (!Solve(i, mid, 0, 0))
      return false;
  }
  return true;
}
int main() {
  freopen("paint.in", "r", stdin);
  freopen("paint.out", "w", stdout);
  Read(n);
  int mx = 0;
  for (int i = 1, u, v, w; i < n; ++i) {
    Read(u), Read(v), Read(w);
    Insert(u, v, w);
    Insert(v, u, w);
    mx = max(mx, w);
  }
  for (int i = 1; i <= n; ++i) {
    Read(cnt[i]);
  }
  int low = 1, high = mx, ans = 0;
  while (low < high) { // 60 pts
    int mid = (low + high) >> 1;
    if (Check(mid))
      ans = mid, low = mid + 1;
    else high = mid - 1;
  }
  printf("%d\n", ans);
  return 0;
}
