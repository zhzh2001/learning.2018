#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 3005;
const long double eps = 1e-10;
struct Line {
  long double k, b; // y = kx + b
} l[kMaxN];
struct Point {
  long double x, y;
  Point() : x(.0), y(.0) {}
};
int n;
inline Point Cross(const Line &p, const Line &q) {
  Point ret;
  ret.x = (q.b - p.b) / (p.k - q.k);
  ret.y = p.k * ret.x + p.b;
  return ret;
}
inline long double Dist(const Point &p, const Point &q) {
  return sqrtl((p.x - q.x) * (p.x - q.x) + (p.y - q.y) * (p.y - q.y));
}
inline Point Middle(const Point &p, const Point &q) {
  Point ret;
  ret.x = (p.x + q.x) / 2.0L;
  ret.y = (p.y + q.y) / 2.0L;
  return ret; 
}
inline Line GetLine(const Point &p, double k) {
  Line ret;
  ret.k = k;
  ret.b = p.y - k * p.x;
  return ret;
}
inline void __W(long double x, char c = ' ') { fprintf(stderr, "%Lf%c", x, c); }
inline void __W(const Point &x) {
  __W(x.x), __W(x.y);
  fputs("\n", stderr); 
}
namespace SubTask1 {
int Solve() {
  Point A, B, C, D, E, O, zero;
  // O ��ʾԲ�� 
  long double da, db, dc;
  Line a, b;
  int ans = 0;
  for (int i = 1; i <= n; ++i) {
    for (int j = i + 1; j <= n; ++j) {
      if (abs(l[i].k - l[j].k) < eps)
        continue;
      A = Cross(l[i], l[j]);
      for (int k = j + 1; k <= n; ++k) {
        if (abs(l[i].k - l[k].k) < eps)
          continue;
        B = Cross(l[i], l[k]);
        if (abs(l[j].k - l[k].k) < eps)
          continue;
        C = Cross(l[j], l[k]);
//        da = Dist(A, B), db = Dist(A, C), dc = Dist(B, C);
//        if (da + db > dc && da + dc > db && db + dc > da) {
        D = Middle(A, B);
        a = GetLine(D, -1.0 / l[i].k);
        E = Middle(A, C);
        b = GetLine(E, -1.0 / l[j].k);
        O = Cross(a, b);
//        __W(Dist(zero, O));
//        __W(Dist(O, B), '\n');
        if (abs(Dist(zero, O) - Dist(O, B)) < eps) {
          ++ans; 
        }
//        }
      }
    }
  }
  printf("%d\n", ans);
  return 0;
}
} // namespace SubTask1
int main() {
  freopen("geometry.in", "r", stdin);
  freopen("geometry.out", "w", stdout);
  Read(n);
  for (int i = 1, a, b, c; i <= n; ++i) {
    Read(a), Read(b), Read(c);
    l[i].k = -1.0 * a / b;
    l[i].b = -1.0 * c / b;
  }
  if (n <= 100) return SubTask1::Solve(); // 50 pts
  return 0;
}
