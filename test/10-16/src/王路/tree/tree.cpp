#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#define int long long
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 1e5 + 5;
int n, k, head[kMaxN], ecnt;
struct Edge { int to, nxt; } e[kMaxN << 1];
inline void Insert(int u, int v) {
  e[++ecnt] = (Edge) { v, head[u] };
  head[u] = ecnt;
}
// Hint: 可能会爆栈
namespace SubTask1 {
const int kSubMaxK = 150;
int g[kMaxN][kSubMaxK];
// g[i][j] 表示以 i 为根的子树中相对深度 % k = j 的节点个数
int h[kMaxN];
// h[i] 表示以 i 为根的子树中，所有节点到 i 的 father 的答案 
int ans[kMaxN];
// ans[i] 表示以 i 为根的子树中的节点到 i 的答案
int __maxdep = 0, __dep = 0;
int fa[kMaxN];
void DFS1(int x) {
  __maxdep = max(__maxdep, ++__dep);
  g[x][0] = 1;
  h[x] = 0;
  ans[x] = 0;
  for (int i = head[x]; i; i = e[i].nxt) {
    int nxt = e[i].to;
    if (nxt == fa[x])
      continue;
    fa[nxt] = x;
    DFS1(nxt);
    for (int j = 1; j < k; ++j)
      g[x][j] += g[nxt][j - 1];
    g[x][0] += g[nxt][k - 1];
    h[x] += h[nxt];
    ans[x] += h[nxt];
  }
  h[x] += g[x][0];
  --__dep;
}
int sum[kMaxN][kSubMaxK], lst[kMaxN], sumans = 0;
void DFS2(int x) {
  for (int j = 1; j < k; ++j)
    sum[x][j] += sum[fa[x]][j - 1];
  sum[x][0] += sum[fa[x]][k - 1];
  for (int j = 0; j < k; ++j)
    sum[x][j] += g[x][j];
  for (int i = head[x]; i; i = e[i].nxt) {
    int nxt = e[i].to;
    if (nxt == fa[x])
      continue;
    for (int j = 1; j < k; ++j)
      sum[x][j] -= g[nxt][j - 1];
    sum[x][0] -= g[nxt][k - 1];
    lst[nxt] = lst[x] + ans[x] - h[nxt] + sum[x][0];
    DFS2(nxt);
    for (int j = 1; j < k; ++j)
      sum[x][j] += g[nxt][j - 1];
    sum[x][0] += g[nxt][k - 1];
  }
  sumans += ans[x] + lst[x];
//  fprintf(stderr, "%d: %d + %d = %d\n", x, ans[x], last, ans[x] + last);
}
int Solve() {
  DFS1(1);
  DFS2(1);
//  fprintf(stderr, "%d\n", sumans >> 1);
//  fprintf(stderr, "%d %d\n", __maxdep, sumans >> 1);
  printf("%lld\n", sumans >> 1);
  return 0;
}
} // namespace SubTask1
signed main() {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);
  Read(n), Read(k);
  for (int i = 1, u, v; i < n; ++i) {
    Read(u), Read(v);
    Insert(u, v);
    Insert(v, u);
  }
  return SubTask1::Solve(); // 100 pts, T->O(NK), M->O(NK)
}
