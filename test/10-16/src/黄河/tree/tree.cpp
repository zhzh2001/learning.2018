#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 1010
using namespace std;
struct Node{
   int tto,nx;
}ed[200010],Ed[20000000];
int Depth[100010],Size[100010],Head[100010],Heada[100010],a[10000000],b[10000000];
int pa[20000000],fa[100010],f[100010],c[100010],n,k,x,y,cnt,Cnt,m;
int findfa(int k){
   if (k!=fa[k])fa[k]=findfa(fa[k]);
   return fa[k];
  }
int max(int l,int r){
   if (l>r) return l;else return r;
}
void un(int x,int y){
   int rx=findfa(x);
   int ry=findfa(y);
   if (rx!=ry) fa[rx]=ry;
}
void add(int x,int y){
   ed[++cnt].tto=y;ed[cnt].nx=Head[x];Head[x]=cnt;
}
void adda(int x,int y){
   Ed[++Cnt].tto=y;Ed[Cnt].nx=Heada[x];Heada[x]=Cnt;
  }
void dfs(int u,int k){
   Size[u]=1;
   for (int i=Head[u];i;i=ed[i].nx){
      int j=ed[i].tto;
      if (j==k) continue;
      dfs(j,u);
      Size[u]=Size[u]+Size[j];
	 }
  }
void dfs1(int u,int k){
   Depth[u]=Depth[k]+1;
   for (int i=Head[u];i;i=ed[i].nx){
      int j=ed[i].tto;
      if (j==k) continue;
      dfs1(j,u);
	 }
  }
void Tarjan(int k){
   for (int i=Head[k];i;i=ed[i].nx){
      int v=ed[i].tto;
      if (v==f[k]) continue;
      f[v]=k;Tarjan(v);un(v,k);c[v]=1;
	 }
   for (int i=Heada[k];i;i=Ed[i].nx){
      int v=Ed[i].tto;
      if (c[v]) pa[i]=findfa(v);
	 }
  }
int main(){
   freopen("tree.in","r",stdin);
   freopen("tree.out","w",stdout);
   scanf("%d%d",&n,&k);
   rep(i,1,n-1){
      scanf("%d%d",&x,&y);
	  add(x,y);add(y,x);
   }
   /*if (k==1){
      dfs(1,0);
      int s=0;
      rep(i,1,n)
		for (int j=Head[i];j;j=ed[j].nx)
		   s=s+Size[i]+Size[ed[j].tto];
	  s/=2;
	  printf("%d",s);
	  return 0;
	 }*/
	if (k==1){
      dfs(1,0);
      int s=0;
      rep(i,1,n)
		for (int j=Head[i];j;j=ed[j].nx){
		   int v=ed[j].tto;
		   if (Size[i]>Size[v]) s+=(n-Size[v])*Size[v];
		                         else s+=(n-Size[i])*Size[i];
	      }
	  s/=2;
	  printf("%d",s);
	  return 0;
	 }

   rep(i,1,n-1)
   	rep(j,i+1,n){
        a[++m]=i;b[m]=j;
		adda(i,j);adda(j,i);
	   }
   rep(i,1,n) fa[i]=i,f[i]=i;
   Tarjan(1);
   Depth[0]=-1;
   dfs1(1,0);
   int ans=0;
   rep(i,1,m){
      int sum=max(pa[2*i],pa[2*i-1]),s=0;
      if ((sum==a[i]) || (sum==b[i])){
         if (Depth[a[i]]>Depth[b[i]])  s=s+Depth[a[i]]-Depth[b[i]];
           else s=s+Depth[b[i]]-Depth[a[i]];
		}else s=s+Depth[b[i]]+Depth[a[i]]-2*Depth[sum];
	  if (s%k==0) ans=ans+s/k;else ans=ans+s/k+1;
	 }
   printf("%d",ans);
   return 0;
  } 
