#include<cstdio>
#include<cstring>
#include<algorithm>
#include<string>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 1010
using namespace std;
int n,a[N],b[N],c[N];
LL ans;
double X[N][N],Y[N][N],Xx,Yy,d1,d2;
const double eps=1e-6;
int main(){
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	scanf("%d",&n);
	rep(i,1,n) scanf("%d%d%d",&a[i],&b[i],&c[i]);
	rep(i,1,n-1){
		rep(j,i+1,n){
			int a1=a[i],a2=a[j],b1=b[i],b2=b[j],c1=c[i],c2=c[j];
			X[i][j]=X[j][i]=(c1*b2-c2*b1)*1.0/((a2*b1-a1*b2)*1.0);
			Y[i][j]=Y[j][i]=(c2*a1-a2*c1)*1.0/((a2*b1-a1*b2)*1.0);
		}
	}
	rep(i,1,n-2){
		rep(j,i+1,n-1){
			rep(k,j+1,n){
				double X1=X[i][j],Y1=Y[i][j];
				double X2=X[j][k],Y2=Y[j][k];
				double X3=X[i][k],Y3=Y[i][k];
				bool flag[4];
				memset(flag,0,sizeof(flag));
				if ((X1!=X2)||(Y1!=Y2)) flag[1]=1;
				if ((X2!=X3)||(Y2!=Y3)) flag[2]=1;
				if ((X3!=X1)||(Y3!=Y1)) flag[3]=1;
				if ((!flag[1])&&(!flag[2])&&(!flag[3])) {
					Xx=X1,Yy=Y1;
				}else if (flag[1]&&flag[2]&&flag[3]){
					Xx=(X1*X1-X2*X2+Y1*Y1-Y2*Y2)*(Y1-Y3)*1.0;
					Xx=Xx-(X1*X1-X3*X3+Y1*Y1-Y3*Y3)*(Y1-Y2)*1.0;
					Xx=Xx/(2*(Y1-Y3)*(X1-X2)*1.0-2*(Y1-Y2)*(X1-X3)*1.0);
					Yy=(X1*X1-X2*X2+Y1*Y1-Y2*Y2)*(X1-X3)*1.0;
					Yy=Yy-(X1*X1-X3*X3+Y1*Y1-Y3*Y3)*(X1-X2)*1.0;
					Yy=Yy/(2*(Y1-Y2)*(X1-X3)*1.0-2*(Y1-Y3)*(X1-X2)*1.0);
				}
				d1=(Xx*Xx+Yy*Yy);
				d2=((Xx-X1)*(Xx-X1)+(Yy-Y1)*(Yy-Y1));
				if (d1<d2) {
					double tmp=d1;d1=d2;d2=tmp;
				}
				if (d1-d2<=eps) ans++;
			}
		}
	}
	printf("%lld",ans);
	return 0;
}
