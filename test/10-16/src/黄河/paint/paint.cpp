#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(i,x,y) for (re int i=(x);i<=(y);i++)
#define drp(i,x,y) for (re int i=(x);i>=(y);i--)
#define cross(i,k) for (re int i=Fir[k];i;i=last[i])
using namespace std;
typedef long long LL;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,k,x,y,a[N],c[N],Tot,Fir[N],To[N<<1],last[N<<1],val[N<<1];
inline void Add(int x,int y,int z){To[++Tot]=y,last[Tot]=Fir[x],Fir[x]=Tot,val[Tot]=z;}
int cnt,Pos[N];
struct Node{int sum,Size;}b[N];
inline bool cmp(Node a,Node b){return a.sum<b.sum;}
inline void dfs(int u,int fa,int x){
	b[Pos[u]].sum+=c[u],b[Pos[u]].Size++;
	cross(i,u){
		int v=To[i];
		if (v==fa) continue;
		if (val[i]>=x) Pos[v]=++cnt;
			else Pos[v]=Pos[u];
		dfs(v,u,x);
	}
}
inline bool check(int x){
	rep(i,1,cnt) b[i].Size=b[i].sum=0;
	Pos[1]=cnt=1,dfs(1,0,x);
	sort(b+1,b+1+cnt,cmp);
	int l=1,en=-1;
	drp(i,cnt,1){
		if (en!=-1){
			if (b[i].sum>=b[en].Size) b[i].sum-=b[en].Size,en=-1;
				else{b[en].Size-=b[i].sum;continue;}
		}
		while (b[i].sum>=b[l].Size&&i!=l&&l<=cnt) b[i].sum-=b[l].Size,l++;
		if (l>cnt) return 1;
		if (i==l){
			en=l,l++;
			while (b[i].sum>=b[l].Size&&l<=cnt) b[i].sum-=b[l].Size,l++;
		} else b[l].Size-=b[i].sum;
		
	}
	return 0;
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	rep(i,1,n-1) x=read(),y=read(),a[i]=read(),Add(x,y,a[i]),Add(y,x,a[i]);
	rep(i,1,n) c[i]=read();
	int l=-1e9,r=1e9,mid,ans;
	while (l<=r){
		mid=l+r>>1;
		if (check(mid)) ans=mid,l=mid+1;
			else r=mid-1;
	}
	printf("%d\n",ans);
}
