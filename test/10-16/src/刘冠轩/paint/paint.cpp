#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=100005;
int head[N],w[N],cnt,n,x,y,z,s,mx,ans=0x3fffffff;
struct edge{int to,nt,d;}e[N*2];
void add(int u,int v,int w){
	e[++cnt]=(edge){v,head[u],w};
	head[u]=cnt;
}
void find(int u,int fa,int MX){
	if(mx>=ans)return;
	if(MX>mx&&w[u])mx=MX,s=u;
	for(int i=head[u];i;i=e[i].nt)
		if(e[i].to!=fa)find(e[i].to,u,max(MX,e[i].d));
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		x=read();y=read();z=read();
		add(x,y,z);add(y,x,z);
	}
	for(int i=1;i<=n;i++)w[i]=read();
	for(int i=1;i<=n;i++){
		s=0;mx=0;
		find(i,0,0);
		w[s]--;
		ans=min(ans,mx);
	}
	cout<<ans;
	return 0;
}
