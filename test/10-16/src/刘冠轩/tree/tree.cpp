#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=100005;
int head[N],cnt,n,k,ans,x,y,sz[N];
struct edge{int to,nt;}e[N*2];
void add(int u,int v){
	e[++cnt]=(edge){v,head[u]};
	head[u]=cnt;
}
void dfs(int u,int fa,int dep){
	ans+=dep/k+(dep%k>0);
	for(int i=head[u];i;i=e[i].nt)
		if(e[i].to!=fa)dfs(e[i].to,u,dep+1);
}
void dfss(int u,int fa){
	sz[u]=1;
	for(int i=head[u];i;i=e[i].nt)
	if(e[i].to!=fa){
		dfss(e[i].to,u);
		ans+=sz[e[i].to]*(n-sz[e[i].to]);
		sz[u]+=sz[e[i].to];
	}
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();k=read();
	for(int i=1;i<n;i++){
		x=read();y=read();
		add(x,y);add(y,x);
	}
	if(n<=3000)for(int i=1;i<=n;i++)dfs(i,0,0);
	else dfss(1,0);
	cout<<(ans>>1);
	return 0;
}
