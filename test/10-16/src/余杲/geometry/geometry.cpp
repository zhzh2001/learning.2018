#include<bits/stdc++.h>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=3005;
struct Point{
	Point(){
		x=y=0;
	}
	Point(double X,double Y){
		x=X,y=Y;
	}
	double x,y;
	bool operator<(const Point &b)const{
		if(x!=b.x)return x<b.x;
		return y<b.y;
	}
};
double sqr(double x){return x*x;}
double dist(Point a,Point b){
	return sqrt(sqr(a.x-b.x)+sqr(a.y-b.y));
}
int num;
double a[MAXN],b[MAXN],c[MAXN];
struct Res{
	Res(){}
	Res(int Code,Point P){
		success=Code;
		p=P;
	}
	int success;
	Point p;
};
double f(int i,double x){
	double K=-a[i]/b[i],B=-c[i]/b[i];
	return K*x+B;
}
Point P(int i,int j){
	double K1=-a[i]/b[i],B1=-c[i]/b[i];
	double K2=-a[j]/b[j],B2=-c[j]/b[j];
	double x=(B2-B1)/(K1-K2);
	double y=f(i,x);
	return Point(x,y);
}
Res GetPoint(int i,int j){
	if((b[i]!=0)||(b[j]!=0)){
		if(b[i]!=0){
			if(b[j]==0)return Res(1,Point(-c[j]/a[j],f(i,-c[j]/a[j])));
			else return Res(1,P(i,j));
		}
		return Res(1,Point(-c[i]/a[i],f(j,-c[i]/a[i])));
	}
	return Res(0,Point());
}
double d[MAXN*MAXN];
set<Point>s[MAXN*MAXN];
int main(){
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++)a[i]=read(),b[i]=read(),c[i]=read();
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++){
			Res Get=GetPoint(i,j);
			if(Get.success)d[++num]=abs(dist(Get.p,Point(0,0)));
		}
	sort(d+1,d+num+1);
//	for(int i=1;i<=num;i++)cout<<d[i]<<' ';cout<<endl;
	num=unique(d+1,d+num+1)-d-1;
//	for(int i=1;i<=num;i++)cout<<d[i]<<' ';cout<<endl;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++){
			Res Get=GetPoint(i,j);
			if(Get.success){
				double dis=abs(dist(Get.p,Point(0,0)));
				int pos=lower_bound(d+1,d+num+1,dis)-d;
//				cout<<pos<<endl;
				s[pos].insert(Get.p);
			}
		}
	long long ans=0;
	for(int i=1;i<=num;i++){
		int tot=s[i].size();
//		cout<<i<<' '<<tot<<endl;
		ans+=tot*(tot-1)*(tot-2)/6;
	}
	printf("%lld",ans);
}
