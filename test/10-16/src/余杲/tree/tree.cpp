#include<cstdio>
#include<iostream>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=1e5+5;
int n,k;
int nedge,head[MAXN];
struct Edge{
	int to,nxt;
}edge[MAXN<<1];
void add(int x,int y){
	edge[++nedge].to=y;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
//long long ans,f[MAXN][150];
//void dfs(int x,int fa){
//	for(int i=head[x];i;i=edge[i].nxt){
//		int y=edge[i].to;
//		if(y==fa)continue;
//		dfs(y,x);
//		for(int j=0;j<k;j++)f[x][(j+1)%k]+=f[y][j];
//	}
////	long long sum=0;
////	for(int i=0;i<k;i++)sum+=f[x][i];
////	for(int i=0;i<k;i++)ans+=f[x][i]*(sum-f[x][i]);
//	static long long sum[150];
//	for(int i=1;i<k;i++)sum[i]=0;
//	for(int i=0;i<k;i++)sum[i]+=f[x][i];
//	for(int i=1;i<k;i++)sum[i]+=sum[i-1];
//	for(int i=0;i<k;i++)ans+=f[x][i]*(sum[(k-i)%k]-f[x][i])+2*f[x][i]*(sum[k-1]-sum[(k-i)%k]);
//	f[x][0]++;
//}
long long ans;
int d[MAXN],siz[MAXN],fa[MAXN],son[MAXN];
void dfs1(int x,int f){
	d[x]=d[f]+1;
	siz[x]=1;
	fa[x]=f;
	int Max=0;
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		if(y==f)continue;
		dfs1(y,x);
		siz[x]+=siz[y];
		if(siz[y]>Max)Max=siz[y],son[x]=y;
	}
}
int top[MAXN];
void dfs2(int x,int topf){
	top[x]=topf;
	if(!son[x])return;
	dfs2(son[x],topf);
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		if(y==fa[x]||y==son[x])continue;
		dfs2(y,y);
	}
}
int LCA(int x,int y){
	while(top[x]!=top[y]){
		if(d[top[x]]<d[top[y]])swap(x,y);
		x=fa[top[x]];
	}
	return d[x]<d[y]?x:y;
}
void dfs(int x){
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		if(y==fa[x])continue;
		dfs(y);
		ans+=1LL*siz[x]*(n-siz[y]);
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),k=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		add(x,y);add(y,x);
	}
	dfs1(1,0);
	dfs2(1,1);
	if(n<=3000){
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++){
				int lca=LCA(i,j);
				ans+=(d[i]+d[j]-2*d[lca])/k+(bool)((d[i]+d[j]-2*d[lca])%k);
			}
	}else dfs(1),ans/=k;
	printf("%lld",ans);
}
