#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=1e5+5;
int nedge,head[MAXN];
struct Edge{
	int to,w,nxt;
}edge[MAXN<<1];
struct E{
	int x,y,z;
	bool operator<(const E &b)const{
		return z<b.z;
	}
}e[MAXN];
void add(int x,int y,int z){
	edge[++nedge].to=y;
	edge[nedge].w=z;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
int siz[MAXN],a[MAXN];
void dfs(int x,int fa){
	siz[x]=1;
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		if(y==fa)continue;
		dfs(y,x);
		siz[x]+=siz[y];
		a[x]+=a[y];
	}
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	int n=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read(),z=read();
		add(x,y,z);add(y,x,z);
		e[i].x=x,e[i].y=y,e[i].z=z;
	}
	sort(e+1,e+n);
	for(int i=1;i<=n;i++)a[i]=read();
	dfs(1,0);
	for(int i=n-1;i>=1;i--){
		if(min(a[e[i].x],siz[e[i].y])<siz[e[i].y]&&
		   min(a[e[i].y],siz[e[i].x])<siz[e[i].x])continue;
		printf("%d",e[i].z);
		break;
	}
}
