#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 3005;
const double eps = 1e-5;
struct node{
	int a,b,c;
	friend bool operator <(node a,node b){
		if(a.a != b.a) return a.a < b.a;
		if(a.b != b.b) return a.b < b.b;
		return a.c < b.c;
	}
}x[N];
struct point{
	double x,y;
};
map<node,int> mp; 
int n;
ll ans;

int Gcd(int x,int y){
	if(y == 0) return x;
	return Gcd(y,x%y);
}

inline point getXXX(node x,node y){
	point ans;
	ans.x = ((double)(1LL*x.b*y.c - 1LL*y.b*x.c))/((double)(1LL*x.a*y.b - 1LL*y.a*x.b));
	ans.y = ((double)(1LL*x.a*y.c - 1LL*y.a*x.c))/((double)(1LL*x.b*y.a - 1LL*y.b*x.a));
	return ans;
}

inline point getXX(node x,point p){
	node y;
	y.a = -x.b;
	y.b = x.a;
	y.c = 0;
	point ans = getXXX(x,y);
	point ok;
	ok.x = ans.x*2-p.x;
	ok.y = ans.y*2-p.y;
	return ok;
}


inline bool getLine(point a,point b,int j){
	double x = a.x-b.x,y = a.y-b.y;
	double A = y,B = -x;
	if(A < 0) A = -A,B = -B;
	double C = -(A*a.x+B*a.y);
	int aa = (int)(A+eps);
	int bb,cc;
	if(B < 0) bb = (int)(B-eps);
	else bb = (int)(B+eps);

	if(C < 0) cc = (int)(C-eps);
	else cc = (int)(C+eps);

	if(abs(A - aa) > eps) return false;
	if(abs(B - bb) > eps) return false;
	if(abs(C - cc) > eps) return false;

	int AA = abs(aa),BB = abs(bb),CC = abs(cc);
	int gd = Gcd(AA,BB);
	gd = Gcd(gd,CC);
	aa = aa/gd; bb = bb/gd; cc = cc/gd;
	if(aa < 0) aa *= -1,bb *= -1,cc *= -1;
	node l; l.a = aa; l.b = bb; l.c = cc;
	int pos = mp[l];
	if(pos > j) return true;
	return false;
}

signed main(){
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	n = read(); int num = 0;
	for(int i = 1;i <= n;++i){
		int a = read(),b = read(),c = read();
		int A = abs(a),B = abs(b),C = abs(c);
		int gd = Gcd(A,B);
		gd = Gcd(gd,B);
		a /= gd; b /= gd; c /= gd;
		if(a < 0) a = -a,b = -b,c = -c;
		x[i].a = a; x[i].b = b; x[i].c = c;
		mp[x[i]] = i;
		if(x[i].c == 0) ++num;
	}
	for(int i = 1;i <= n;++i)
		for(int j = i+1;j <= n;++j){
			if(x[i].c == 0 && x[j].c == 0) continue;
			point xd = getXXX(x[i],x[j]);
			point A = getXX(x[i],xd);
			point B = getXX(x[j],xd);
			if(getLine(A,B,j)){++ans;}
		}
	if(num >= 3){
		ll tot = 1LL*(num)*(num-1)*(num-2)/3/2;
		ans += tot;
	}
	printf("%lld\n",ans);
	return 0;
}
