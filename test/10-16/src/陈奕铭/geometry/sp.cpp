#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

signed main(){
	double a = 9.001;
	printf("%d\n",(int)a);
	double b = -8.999;
	printf("%d\n", (int)b);
	return 0;
}