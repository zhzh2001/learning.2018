#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005,M = 155;
int n,k;
int to[N*2],nxt[N*2],head[N],cnt;
int w[N][M],W[N][M];
ll val[N][M],Val[N][M];
int Pow[M];
ll poval[M];
ll ans;

inline int insert(int a,int b){
	to[++cnt] = b; nxt[cnt] = head[a]; head[a] = cnt;
}

void dfs(int x,int fa){
	w[x][0] = 1; val[x][0] = 0;
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa){
			dfs(to[i],x);
			for(int j = 1;j <= k-2;++j){
				w[x][j+1] += w[to[i]][j];
				val[x][j+1] += val[to[i]][j];
			}
			w[x][0] += w[to[i]][k-1];
			val[x][0] += val[to[i]][k-1];
			w[x][1] += w[to[i]][0];
			val[x][1] += val[to[i]][0]+w[to[i]][0];
		}
}

void dfs2(int x,int fa){
	for(int i = 0;i < k;++i)
		ans += val[x][i]+Val[x][i];
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa){
			int y = to[i];
			memset(Pow,0,sizeof Pow);
			memset(poval,0,sizeof poval);
			for(int j = 2;j < k;++j){
				Pow[j] += w[x][j]-w[y][j-1];
				poval[j] += val[x][j]-val[y][j-1];
			}
			Pow[0] += w[x][0]-w[y][k-1];
			poval[0] += val[x][0]-val[y][k-1];
			Pow[1] += w[x][1]-w[y][0];
			poval[1] += val[x][1]-val[y][0]-w[y][0];

			for(int j = 1;j <= k-2;++j){
				W[y][j+1] += Pow[j]+W[x][j];
				Val[y][j+1] += poval[j]+Val[x][j];
			}
			W[y][0] += Pow[k-1]+W[x][k-1];
			Val[y][0] += poval[k-1]+Val[x][k-1];
			W[y][1] += Pow[0]+W[x][0];
			Val[y][1] += poval[0]+Val[x][0]+Pow[0]+W[x][0];
			dfs2(y,x);
		}
}

void dfs3(int x,int fa){
	w[x][0] = 1;
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa){
			dfs3(to[i],x);
			w[x][0] += w[to[i]][0];
		}
}

void dfs4(int x,int fa){
	ans += 1LL*w[x][1]*w[x][0];
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa){
			w[to[i]][1] += w[x][1]-w[to[i]][0]+w[x][0];
			dfs4(to[i],x);
		}
}

signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n = read(); k = read();
	for(int i = 1;i < n;++i){
		int x = read(),y = read();
		insert(x,y); insert(y,x);
	}
	if(k == 1){
		dfs3(1,1);
		dfs4(1,1);
		printf("%lld\n", ans);
	}else{
		dfs(1,1);
		dfs2(1,1);
		printf("%lld\n", ans/2);
	}
	return 0;
}