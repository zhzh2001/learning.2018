#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int n;
struct edge{
	int x,y,w;
	friend bool operator <(edge a,edge b){
		return a.w > b.w;
	}
}e[N];
int head[N],to[N*2],nxt[N*2],cnt;
int X[N];
ll tot,totX,totval;
int L[N],R[N],DFN,Val[N];
ll sum1[N<<2],sum2[N<<2],sumx1,sumx2;

inline void insert(int x,int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}

void dfs(int x,int fa){
	L[x] = ++DFN;
	Val[DFN] = X[x];
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa)
			dfs(to[i],x);
	R[x] = DFN;
}

void build(int num,int l,int r){
	if(l == r){
		sum1[num] = 1;
		sum2[num] = Val[l];
		return;
	}
	int mid = (l+r)>>1;
	build(num<<1,l,mid);
	build(num<<1|1,mid+1,r);
	sum1[num] = sum1[num<<1]+sum1[num<<1|1];
	sum2[num] = sum2[num<<1]+sum2[num<<1|1];
}

bool check(int num,int l,int r,int x){
	if(sum1[num] == 0) return true;
	if(l == r) return false;
	int mid = (l+r)>>1;
	if(x <= mid) return check(num<<1,l,mid,x);
	else return check(num<<1|1,mid+1,r,x);
}

inline void pushdown(int k){
	int l = k<<1,r = k<<1|1;
	if(sum1[k] == 0){
		sum1[l] = sum2[l] = sum1[r] = sum2[r] = 0;
	}
}

void change(int num,int l,int r,int x,int y){
	if(x <= l && r <= y){
		sum1[num] = sum2[num] = 0;
		return;
	}
	int mid = (l+r)>>1;
//	pushdown(num);
	if(x <= mid) change(num<<1,l,mid,x,y);
	if(y > mid) change(num<<1|1,mid+1,r,x,y);
	sum1[num] = sum1[num<<1]+sum1[num<<1|1];
	sum2[num] = sum2[num<<1]+sum2[num<<1|1];
}

void getsum1(int num,int l,int r,int x,int y){
	if(x <= l && r <= y){
		sumx1 += sum1[num];
		sumx2 += sum2[num];
		return;
	}
//	pushdown(num);
	int mid = (l+r)>>1;
	if(x <= mid) getsum1(num<<1,l,mid,x,y);
	if(y > mid) getsum1(num<<1|1,mid+1,r,x,y);
}

signed main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n = read();
	for(int i = 1;i < n;++i){
		e[i].x = read(); e[i].y = read(); e[i].w = read();
		insert(e[i].x,e[i].y); insert(e[i].y,e[i].x);
	}
	tot = n;
	for(int i = 1;i <= n;++i) X[i] = read(),totX += X[i];
	sort(e+1,e+n);
	dfs(1,1);
	build(1,1,n);
	for(int i = 1;i < n;++i){
		if(check(1,1,n,L[e[i].x]) || check(1,1,n,L[e[i].y])) continue;
		int x = e[i].x,y = e[i].y;
		if(L[x] < L[y]) swap(x,y);
		sumx1 = 0; sumx2 = 0;
		getsum1(1,1,n,L[x],R[x]);
		ll sumy1 = tot - sumx1;
		ll sumy2 = totX - sumx2;
		if(sumx1 < sumy1){			//x的子树比较小
			change(1,1,n,L[x],R[x]);
			tot -= sumx1;
			totX -= sumx2;
			totval += sumx2;
			if(totval >= tot){
				printf("%d\n",e[i].w);
				return 0;
			}
		}else{
			if(L[x] > 1) change(1,1,n,1,L[x]-1);
			if(R[x] < 1) change(1,1,n,R[x]+1,n);
			tot -= sumy1;
			totX -= sumy2;
			totval += sumy2;
			if(totval >= tot){
				printf("%d\n", e[i].w);
				return 0;
			}
		}
	}
	return 0;
}
