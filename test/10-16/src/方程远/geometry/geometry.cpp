#include<bits/stdc++.h>
using namespace std;
const double eps=0.00000001;
int n,x,y,z,ans;
double abx,aby,acx,acy,bcx,bcy,minx,miny,maxx,maxy,maxcx,maxcy;
double l,r,midax,miday,midcx,midcy,mid,K,B,K1,B1,circlex,circley;
double a[3001],b[3001];
int main(){
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout); 
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>x>>y>>z;
		a[i]=-((double)x/y);
		b[i]=-((double)z/y);
	}
	for(int i=1;i<=n-2;i++)
		for(int j=i+1;j<=n-1;j++)
			for(int k=j+1;k<=n;k++){
				abx=(b[j]-b[i])/(a[i]-a[j]);
				aby=abx*a[i]+b[i];
				acx=(b[k]-b[i])/(a[i]-a[k]);
				acy=acx*a[i]+b[k];
				bcx=(b[k]-b[j])/(a[j]-a[k]);
				bcy=bcx*a[j]+b[k];
				if(abx-acx<eps&&aby-acy<eps&&abx-bcx<eps&&aby-bcy<eps){
					if(abx<eps&&aby<eps)
						ans++;
					continue;
				}
				midax=abx+(acx-abx)/2;
				miday=aby+(acy-aby)/2;
				midcx=acx+(bcx-acx)/2;
				midcy=acy+(bcy-acy)/2;
				if(abx<=acx){
					minx=abx;
					miny=aby;
					l=abx;
					maxx=acx;
					maxy=acy;
					r=acx;
				}
				else{
					minx=acx;
					miny=acy;
					l=acx;
					maxx=abx;
					maxy=aby;
					r=abx;
				}
				while(1){
					mid=(l+r)/2;
					if(abs(((mid-minx)*(mid-minx)+miny*miny)-((mid-maxx)*(mid-maxx)+maxy*maxy))<eps)
						break;
					if(((mid-minx)*(mid-minx)+miny*miny)>((mid-maxx)*(mid-maxx)+maxy*maxy))
						r=mid;
					else
						l=mid;
				}
				K=miday/(midax-mid);
				B=-mid*K;
				if(bcx<=acx){
					minx=bcx;
					miny=bcy;
					l=bcx;
					maxx=acx;
					maxy=acy;
					r=acx;
				}
				else{
					minx=acx;
					miny=acy;
					l=acx;
					maxx=bcx;
					maxy=bcy;
					r=bcx;
				}
				while(1){
					mid=(l+r)/2;
					if(abs(((mid-minx)*(mid-minx)+miny*miny)-((mid-maxx)*(mid-maxx)+maxy*maxy))<eps)
						break;
					if(((mid-minx)*(mid-minx)+miny*miny)>((mid-maxx)*(mid-maxx)+maxy*maxy))
						r=mid;
					else
						l=mid;
				}
				K1=midcy/(midcx-mid);
				B1=-mid*K;
				circlex=(K1-K)/(B-B1);
				circley=circlex*K+B;
				if(((circlex-abx)*(circlex-abx)+(circley-aby)*(circley-aby))-(circlex*circlex+circley*circley)<eps)
					ans++;
			}
	cout<<ans;
	return 0;
}
