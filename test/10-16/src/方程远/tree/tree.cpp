#include<bits/stdc++.h>
#define int long long
using namespace std;
inline void read(int &q){
	int x=0,ans=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		ans=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*ans;
}
bool flag[100001];
int n,m,start,x,y,z,tot,sum;
int from[100001],to[500001],nxt[500001],w[500001],ans[100001];
priority_queue<pair<int,int> >que;
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout); 
	cin>>n>>m;
	for(int i=1;i<n;i++){
		cin>>x>>y;
		tot++;
		to[tot]=y;
		w[tot]=1;
		nxt[tot]=from[x];
		from[x]=tot;
		tot++;
		to[tot]=x;
		w[tot]=1;
		nxt[tot]=from[y];
		from[y]=tot;
	}
	for(start=1;start<=n;start++){
		for(int i=1;i<=n;i++){
			ans[i]=2147483647;
			flag[i]=0;
		}
		ans[start]=0;
		while(!que.empty())
			que.pop();
		que.push((pair<int,int>){0,start});
		while(!que.empty()){
			x=que.top().second;
			que.pop();
			if(flag[x])
				continue;
			flag[x]=1;
			for(int i=from[x];i;i=nxt[i]){
				y=to[i];
				z=w[i];
				if(ans[y]>ans[x]+z){
					ans[y]=ans[x]+z;
					que.push((pair<int,int>){-ans[y],y});
				}
			}
		}
		for(int i=start;i<=n;i++)
			if(ans[i]%m==0)
				sum+=ans[i]/m;
			else
				sum+=ans[i]/m+1;
	}
	cout<<sum;
	return 0;
}
