#include<bits/stdc++.h>
using namespace std;
int n,a,b,c,ans=2147483647;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout); 
	read(n);
	for(int i=1;i<=n;i++){
		read(a);
		read(b);
		read(c);
		ans=min(ans,c);
	}
	cout<<ans;
}
