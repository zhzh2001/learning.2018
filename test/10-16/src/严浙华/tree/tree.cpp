#include<iostream>
using namespace std;
int n,k,u,v,tot;
const int maxn = 1e5 + 10;
struct point{
	int u;
	int deep;
}a[maxn];
int f(int x,int y){

	int res = 0,re = 0;
	while(a[x].deep < a[y].deep && a[x].u != y && a[y].u != x){
		y = a[y].u;
		res++;
	}
	if(a[x].u == y || a[y].u == x)
		res++;
	while(a[x].u != y && a[y].u != x && a[x].u != a[y].u){
		x = a[x].u;
		y = a[y].u;
		res += 2;
	}
	if(a[x].deep == a[y].deep && a[x].u == a[y].u)
		res += 2;
	if(k != 1){
		re = res / k;
		if(res % k == 0)
			return re;
		else
			return re + 1;
	}
	else if(k == 1)
		return res;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>k;
	a[1].u = 0;
	a[1].deep = 1;
	for(int i = 1; i <= n - 1; i++){
		cin>>u>>v;
		a[v].u = u;
		a[v].deep = a[u].deep + 1;
	}
	for(int i = 1;i <= n - 1;i++)
		for(int j = i + 1;j <= n;j++)
				tot+=f(i,j);
	cout<<tot<<endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
}
