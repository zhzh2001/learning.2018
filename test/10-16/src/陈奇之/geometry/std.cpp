#include<bits/stdc++.h>
using namespace std;typedef long long ll;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef pair<int,int> pii;
const int inf = 0x3f3f3f3f,oo = inf;
#define pi 3.14159265358979323846
#define IL inline
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<int(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
#define pc putchar
#define gc getchar
IL ll read(){
    RG ll x=0;char f=0;RG char c=gc();
    for(;!isdigit(c);c=gc())f|=(c=='-');
    for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
    return f?-x:x;
}
IL void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
IL void writeln(ll x){write(x);puts("");}
#define debug(lson) printf(#lson" = %d\n",lson);
#define rd() read()
#define rdb() readdb()
#define pb push_back
#define mp make_pair
#define y1 ____y1
#define union _union
#define sqr(x) ((x) * (x))
const double eps = 1e-8;
int dcmp(double x){if(fabs(x) < eps) return 0;return x<0?-1:1;}
struct Point{
	double x,y;
	Point(double x=0,double y=0):x(x),y(y){};
	Point operator + (const Point &a)const{return Point(x+a.x,y+a.y);}
	Point operator - (const Point &a)const{return Point(x-a.x,y-a.y);}
	Point operator * (double a){return Point(x*a,y*a);}
	Point operator / (double a){return Point(x/a,y/a);}
	void out(){
		printf("[%lf %lf]\n",x,y);
	}
	bool operator < (const Point &b) const{
		return dcmp(y-b.y) ? y>b.y : x>b.x;
	}
}; 
double cross(Point a,Point b){return a.x*b.y-b.x*a.y;}
double dot(Point a,Point b){return a.x*b.x+a.y*b.y;}
struct Line{
	Point x,y; 
	Line(){}
	Line(Point x,Point y):x(x),y(y){}
};
Point Line_jiao(Line a,Line b){
	double r1 = cross(a.x-b.x,b.y-b.x),r2 = cross(b.y-b.x,a.y-b.x);
	return a.x+(a.y-a.x)*r1/(r1+r2);
}
double dist(Point a,Point b){return sqrt(sqr(a.x-b.x)+sqr(a.y-b.y));}
struct Segment{
	int u,v;
	Point p;
	bool operator < (const Segment &B) const{
		return cross(p,B.p) > 0;
	}
}A[3005*3005];
int m,n;
int pos[3005];
Point p[3234];

double area(Point a,Point b,Point c){
	return abs(cross(b-a,c-a));
}
inline ll C3(ll n){
	if(n<=2) return 0;
	return n * (n-1) * (n-2) / 6ll; 
}
int main(){
	freopen("geometry.in","r",stdin);
//	freopen("geometry.out","w",stdout);
	n = rd();
	Rep(i,1,n){
		int a=rd(),b=rd(),c=rd();
		Point A,B,C,D;
		double BD;
		if(!b){
			A = Point(1. * -c / a,-100);
			B = Point(1. * -c / a,100);
		} else{
			A = Point(-100,1. * (-c + 100 * a) / b);
			B = Point(+100,1. * (-c - 100 * a) / b);
		}
//		printf("%lf %lf %lf %lf\n",A.x,A.y,B.x,B.y);
		C = Point(0,0);
		BD = dot(A-B,C-B) / dist(A,B);
//		printf("%lf\n",BD);
		D = B + (A-B) / dist(A,B) * BD;
		p[i] = D;
//		printf("%lf %lf\n",p[i].x,p[i].y);
	}
	sort(p+1,p+1+n);
	Rep(i,1,n) pos[i] = i;
	
	Rep(i,1,n){
		Rep(j,i+1,n){
			A[++m] = (Segment){i,j,(p[i]-p[j]) / dist(p[i],p[j])};
		}
	}
	ll ans = 0;
	sort(A+1,A+1+m);
	Rep(i,1,m){
		int &a = pos[A[i].u],&b = pos[A[i].v];
		
		printf("%d %d\n",a,b);
		int l=1,r=min(a,b)-1,L = 0,R;
		if(i == 1 || (dcmp(cross(A[i].p,A[i-1].p)) != 0)) {
//			A[i].p.out();
//			A[i-1].p.out();
//			printf("~~%d %d\n",pos[A[i].u],pos[A[i].v]);
			while(l<=r){
				int mid = (l+r)>>1;
				if(dcmp(area(p[a],p[b],p[mid])) != 0){
					L = mid;
					l = mid + 1;
				} else{
					r = mid - 1;
				}
			}//找到最小的大于0的位置 
			l = max(a,b)+1,r = n,R = n+1;
			while(l<=r){
				int mid = (l+r)>>1;
				if(dcmp(area(p[a],p[b],p[mid])) != 0){
					R = mid;
					r = mid - 1;
				} else{
					l = mid + 1;
				}
			}
//			printf("%d %d\n",L+1,R-1);
			if(L+1<=R-1) ans += C3((R-1) - (L+1) + 1);
		}
		//统计这一条直线上的 
		swap(a,b);
		swap(p[a],p[b]);
	}
	writeln(ans);
	return 0;
}
