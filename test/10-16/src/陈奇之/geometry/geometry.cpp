#include<bits/stdc++.h>
using namespace std;typedef long long ll;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef pair<int,int> pii;
const int inf = 0x3f3f3f3f,oo = inf;
#define pi 3.14159265358979323846
#define IL inline
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<int(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
#define pc putchar
#define gc getchar
IL ll read(){
    RG ll x=0;char f=0;RG char c=gc();
    for(;!isdigit(c);c=gc())f|=(c=='-');
    for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
    return f?-x:x;
}
IL void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
IL void writeln(ll x){write(x);puts("");}
#define debug(lson) printf(#lson" = %d\n",lson);
#define rd() read()
#define rdb() readdb()
#define pb push_back
#define mp make_pair
#define y1 ____y1
#define union _union
#define sqr(x) ((x) * (x))
const double eps = 1e-8;
struct Point{
	double x,y;
	Point(double x=0,double y=0):x(x),y(y){};
	Point operator + (const Point &a)const{return Point(x+a.x,y+a.y);}
	Point operator - (const Point &a)const{return Point(x-a.x,y-a.y);}
	Point operator * (double a){return Point(x*a,y*a);}
	Point operator / (double a){return Point(x/a,y/a);}
	void out(){
		printf("[%lf %lf]\n",x,y);
	}
}; 
double cross(Point a,Point b){return a.x*b.y-b.x*a.y;}
struct Line{
	Point x,y; 
	Line(){}
	Line(Point x,Point y):x(x),y(y){}
};
Point Line_jiao(Line a,Line b){
	double r1 = cross(a.x-b.x,b.y-b.x),r2 = cross(b.y-b.x,a.y-b.x);
	return a.x+(a.y-a.x)*r1/(r1+r2);
}
double dist(Point a,Point b){return sqrt(sqr(a.x-b.x)+sqr(a.y-b.y));}
int dcmp(double x){if(fabs(x) < eps) return 0;return x<0?-1:1;}
Point center(Point A,Point B,Point C){
	Point p = (A+B) / 2;
	Point q = (A+C) / 2;
	if(dcmp(cross(p-C,A-C))==0){
		if(dcmp(dist(A,B)+dist(B,C)-dist(A,C))==0) return (A+C)/2;
		if(dcmp(dist(A,B)+dist(A,B)-dist(B,C))==0) return (B+C)/2;
		if(dcmp(dist(A,C)+dist(B,C)-dist(A,B))==0) return (A+B)/2;
	}
	Point _p=p+Point(-(p-A).y,(p-A).x);
	Point _q=q+Point(-(q-A).y,(q-A).x);
	Point answ = Line_jiao(Line(p,_p),Line(q,_q));
	return answ;
}
Point P1,P2,P3,P4;
bool check(){
	Point M = center(P1,P2,P3);
	double A = dist(M,P1);
	double B = dist(M,P2);
	double C = dist(M,P3);
	double D = dist(M,P4);
	if(dcmp(A-B)==0 && dcmp(A-C)==0 && dcmp(A-D)==0) return true;
	return false;
}
int n;
Line L[1234];
int main(){
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	n = rd();
//	n = 200;
if(n > 100){
	puts("0");
	return 0;
}
	Rep(i,1,n){
//		double a = rand() / RAND_MAX * 1000,
//			   b = rand() / RAND_MAX * 1000,
//			   c = rand() / RAND_MAX * 1000;
		int a=rd(),b=rd(),c=rd();
		if(!b){
			L[i] = Line(Point(1.*-c/a,0),Point(1.*-c/a,100));
		} else{
			L[i] = Line(Point(0,-1.*c/b),Point(100,1.*(-c-100*a)/b));
		}
	}
	ll ans = 0;
	Rep(i,1,n){
		Rep(j,i+1,n){
			Rep(k,j+1,n){
				P1 = Line_jiao(L[i],L[j]);
				P2 = Line_jiao(L[i],L[k]);
				P3 = Line_jiao(L[j],L[k]);
				P4 = Point(0,0);
//				P1.out(),P2.out();
//				P3.out(),P4.out();
//				puts("");
				if(check()) ans++;
			}
		}
	}
	writeln(ans);
	return 0;
}
