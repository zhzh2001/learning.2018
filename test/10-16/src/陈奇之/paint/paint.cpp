#include<bits/stdc++.h>
using namespace std;typedef long long ll;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef pair<int,int> pii;
const int inf = 0x3f3f3f3f,oo = inf;
#define pi 3.14159265358979323846
#define IL inline
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<int(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
#define pc putchar
#define gc getchar
IL ll read(){
    RG ll x=0;char f=0;RG char c=gc();
    for(;!isdigit(c);c=gc())f|=(c=='-');
    for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
    return f?-x:x;
}
IL void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
IL void writeln(ll x){write(x);puts("");}
#define debug(lson) printf(#lson" = %d\n",lson);
#define rd() read()
#define rdb() readdb()
#define pb push_back
#define mp make_pair
#define y1 ____y1
#define union _union
const int maxn = 1e5+233;
struct node{
	int a,b,c;
	node(){}
	node(int a,int b,int c):
		a(a),b(b),c(c){}
	bool operator < (const node &w) const{
		return c < w.c;
	}
}edge[maxn];
int fa[maxn],n;
ll sz1[maxn],sz2[maxn],tot,ans;
inline int find(int x){
	return fa[x] == x ? x : fa[x] = find(fa[x]);
}
int main(){
	tot = 0;
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n = rd();
	rep(i,1,n){
		int a = rd(),b = rd(),c = rd();
		edge[i] = node(a,b,c);
	}
	sort(edge+1,edge+n);
	Rep(i,1,n) fa[i] = i,sz1[i] = 1,sz2[i] = rd(),tot+=sz2[i];
	ans = edge[1].c;
	rep(i,1,n){
		int a = edge[i].a,b = edge[i].b;
		if(find(a) != find(b)){
			sz1[find(a)] += sz1[find(b)];
			sz2[find(a)] += sz2[find(b)];
			fa[find(b)] = find(a);
			if(sz1[find(a)] > tot-sz2[find(b)]){
				break;
			}//������
			else{
				if(i==n-1 || edge[i].c != edge[i+1].c){
					ans = edge[i+1].c;
				}
			}
		}
	}writeln(ans);
	return 0;
}
