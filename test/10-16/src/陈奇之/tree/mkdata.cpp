#include<bits/stdc++.h>
using namespace std;typedef long long ll;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef pair<int,int> pii;
const int inf = 0x3f3f3f3f,oo = inf;
#define pi 3.14159265358979323846
#define IL inline
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<int(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
#define pc putchar
#define gc getchar
IL ll read(){
    RG ll lson=0;char f=0;RG char c=gc();
    for(;!isdigit(c);c=gc())f|=(c=='-');
    for(;isdigit(c);c=gc())lson=(lson<<1)+(lson<<3)+(c^48);
    return f?-lson:lson;
}
IL void write(ll lson){if(lson<0)lson=-lson,pc('-');if(lson>=10)write(lson/10);pc(lson%10+'0');}
IL void writeln(ll lson){write(lson);puts("");}
IL void writeln(ll lson,char c,ll rson){write(lson);pc(c);writeln(rson);}
IL void writeln(ll lson,char c,ll rson,char d,ll z){write(lson);pc(c);write(rson);pc(d);writeln(z);}
#define debug(lson) printf(#lson" = %d\n",lson);
#define rd() read()
#define rdb() readdb()
#define pb push_back
#define mp make_pair
#define y1 ____y1
#define union _union
const int maxn = 1e5+233;
struct Edge{
	int to,nxt;
	Edge(){}
	Edge(int to,int nxt):to(to),nxt(nxt){}
}edge[maxn*2];
int first[maxn],nume;
void Addedge(int a,int b){
	edge[nume] = Edge(b,first[a]);
	first[a] = nume++;
}
int cnt[maxn][151],size[maxn],n,k;
ll ans = 0;
void dfs(int u,int fa){
	cnt[u][0] = 1;
	size[u] = 1;
	for(int e=first[u];~e;e=edge[e].nxt){
		int v = edge[e].to;
		if(v==fa) continue;
		dfs(v,u);
		size[u] += size[v];
		Dep(i,k-2,0) swap(cnt[v][i],cnt[v][(i+1)%n]);
		ans += 1ll * size[v] * (n - size[v]);
		for(int i=0;i<=size[u] && i<k;++i){
			for(int j=0;j<=size[v] && j<k;++j){
				int tmp = 0;
				if(i+j==0) tmp = k; else
				if(i+j<=k) tmp = i+j; else
						   tmp = i+j-k;
				ans -= 1ll * tmp * cnt[u][i] * cnt[v][j];
			}
		}
		rep(i,0,k) cnt[u][i] = cnt[u][i] + cnt[v][i];
	}
}
inline int ran(){
	return rand()<<15|rand();
}
int main(){
	freopen("tree.in","w",stdout);
	//freopen("tree.out","w",stdout);
	puts("100000 150");
	n = 100000;
	Rep(i,2,n){
		printf("%d %d\n",ran()%(i-1)+1,i);
//		int a = rd(),b = rd();
//		Addedge(a,b);
//		Addedge(b,a);
	}
	//有多少对点距离对k取膜余数为0~k 
	return 0;
}
