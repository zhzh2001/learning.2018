#include<bits/stdc++.h>
using namespace std;typedef long long ll;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef pair<int,int> pii;
const int inf = 0x3f3f3f3f,oo = inf;
#define pi 3.14159265358979323846
#define IL inline
#define RG register
#define rep(i,a,b) for(RG int i=(a);i<int(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Dep(i,a,b) for(RG int i=(a);i>=(b);--i)
#define pc putchar
#define gc getchar
IL ll read(){
    RG ll x=0;char f=0;RG char c=gc();
    for(;!isdigit(c);c=gc())f|=(c=='-');
    for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
    return f?-x:x;
}
IL void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);pc(x%10+'0');}
IL void writeln(ll x){write(x);puts("");}
#define debug(lson) printf(#lson" = %d\n",lson);
#define rd() read()
#define rdb() readdb()
#define pb push_back
#define mp make_pair
#define y1 ____y1
#define union _union
const int maxn = 1e5+233;
struct Edge{
	int to,nxt;
	Edge(){}
	Edge(int to,int nxt):to(to),nxt(nxt){}
}edge[maxn*2];
int first[maxn],nume;
void Addedge(int a,int b){
	edge[nume] = Edge(b,first[a]);
	first[a] = nume++;
}
int cnt[maxn][151],size[maxn],n,k;
ll ans = 0;
void dfs(int u,int fa){
	cnt[u][0] = 1;
	size[u] = 1;
	for(int e=first[u];~e;e=edge[e].nxt){
		int v = edge[e].to;
		if(v==fa) continue;
		dfs(v,u);
		Dep(i,k-2,0) swap(cnt[v][i],cnt[v][(i+1)%n]);
		ans += 1ll * size[v] * (n - size[v]);
		for(int i=0;i<=size[u] && i<k;++i){
			for(int j=0;j<=size[v] && j<k;++j){
				int tmp = 0;
				if(i+j==0) tmp = k; else
				if(i+j<=k) tmp = i+j; else
						   tmp = i+j-k;
				ans -= 1ll * tmp * cnt[u][i] * cnt[v][j];
			}
		}
		size[u] += size[v];
		rep(i,0,k) cnt[u][i] = cnt[u][i] + cnt[v][i];
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	memset(first,-1,sizeof(first));nume = 0;
	n = rd(),k = rd();
	rep(i,1,n){
		int a = rd(),b = rd();
		Addedge(a,b);
		Addedge(b,a);
	}
	memset(cnt,0,sizeof(cnt));
	dfs(1,0); 
//	writeln(1ll*n*(n-1)/2);
//	writeln(ans);
	writeln(ans/k + 1ll*n*(n-1)/2);
	//有多少对点距离对k取膜余数为0~k 
	return 0;
}
/*
1 : 1
2 : 1
……
k : 1
k+1 : 2
距离为0的，贡献-k 
距离为1的，贡献-1
距离为i的，贡献-i

1、从root开始跳的
2、不是从root开始跳的
如果距离为i,j都有，合起来贡献
(i+j)%k * f(i) * f(j) + i * f(i) + j * f(j)
i+j<=k
	(i+j) * f(i) * f(j) + i * f(i) + j * f(j)
i+j>k
	(i+j) * f(i) * f(j) + i * f(i) + j * f(j)
	- k * f(i) * f(j)
*/
