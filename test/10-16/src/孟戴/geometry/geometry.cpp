#include<iostream>
#include<algorithm>
#include<string.h>
#include<fstream>
#include<math.h>
#include<iomanip>
using namespace std;
const long double eps=1e-7;
//ifstream fin("geometry.in");
//ofstream fout("geometry.out");
int n;
struct point{
    long double x,y;
};
struct line{
	long double a,b,c,k,bb;
}l[3003];
inline long double dis(point a,point b){
    return ((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}
inline point getwai(point a,point b,point c){
    point w;
    point cen1,cen2;
    cen1.x=(a.x+b.x)/2.00,cen1.y=(a.y+b.y)/2.00;
    cen2.x=(a.x+c.x)/2.00,cen2.y=(a.y+c.y)/2.00;
    if(a.y==b.y){
		long double k2=-1.00/((a.y-c.y)/(a.x-c.x));
		long double b2=cen2.y-k2*cen2.x;
		w.x=cen1.x,w.y=cen1.x*k2+b2;
		return w;
    }else if(a.y==c.y){
		long double k1=-1.00/((a.y-b.y)/(a.x-b.x));
		long double b1=cen1.y-k1*cen1.x;
		w.x=cen2.x,w.y=cen2.x*k1+b1;
		return w;
    }else{
		long double k1=-1.00/((a.y-b.y)/(a.x-b.x));
		long double b1=cen1.y-k1*cen1.x;
		long double k2=-1.00/((a.y-c.y)/(a.x-c.x));
		long double b2=cen2.y-k2*cen2.x;
		w.x=(b2-b1)/(k1-k2),w.y=k1*w.x+b1;
		return w;
    }
}
inline point pp(line a,line b){
	point x;
	long double k1=a.k,k2=b.k,b1=a.bb,b2=b.bb;
	x.x=(b2-b1)/(k1-k2);
	x.y=x.x*k1+b1;
	return x;
}
inline void baoli(){
	for(int i=1;i<=n;i++){
		cin>>l[i].a>>l[i].b>>l[i].c;
		if(l[i].b==0){
			l[i].k=1e18,l[i].bb=0;
			continue;
		} 
		l[i].k=-(l[i].a/l[i].b),l[i].bb=-(l[i].c/l[i].b);
	}
	long long ans=0;
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			if(l[i].k!=l[j].k){
				for(int k=j+1;k<=n;k++){
					if(l[i].k==l[k].k||l[j].k==l[k].k){
						continue;
					}
					point p1=pp(l[i],l[j]),p2=pp(l[i],l[k]),p3=pp(l[j],l[k]);
					point oo=getwai(p1,p2,p3);
					if((oo.x*oo.x+oo.y*oo.y)<=dis(oo,p1)+eps&&(oo.x*oo.x+oo.y*oo.y)>=dis(oo,p1)-eps){
						ans++;
					}
				}
			}
		}
	}
	cout<<ans<<endl;
}
int main(){
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	cin>>n;
	if(n<=100){
		baoli();
	}
	return 0;
}
/*

in:
4
1 0 0
0 1 0
1 1 -1
1 -1 2
out:
2

*/
/*
k1x+b1=k2x+b2
x=(b2-b1)/(k1-k2)

ax+by+c=0
y=-c/b-a/bx
直线n y=k1x+b1
直线m y=k2x+b2
交点d x=(b2-b1)/(k1-k2),y=(b2*k1-k2*b1)/(k1-k2) 
原点o (0,0)
直线do中点 x=(b2-b1)/2(k1-k2),y=(b2*k1-k2*b1)/2(k1-k2)
kk=(b2*k1-k2*b1)/(b2-b1)
直线do中垂线  y=(b1-b2)/(b2*k1-k2*b1)x+bb
*/
