#include<iostream>
#include<algorithm>
#include<string.h>
#include<fstream>
#include<math.h>
#include<iomanip>
using namespace std;
inline long long read(){
    long long x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(long long x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
long long sum[100003],sz[100003],f[100003];
long long summ,ans,n;
inline int getfa(int k){
	if(f[k]!=k){
		f[k]=getfa(f[k]);
	}
	return f[k];
}
struct xxx{
	long long x,y,z;
}a[100003];
inline bool cmp(xxx a,xxx b){
	return a.z<b.z;
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	for(int i=1;i<=n-1;i++){
		a[i].x=read(),a[i].y=read(),a[i].z=read();
	}
	sort(a+1,a+n,cmp);
	for(int i=1;i<=n;i++){
		sum[i]=read(),sz[i]=1;
		summ+=sum[i],f[i]=i;
	}
	ans=a[1].z;
	for(int i=1;i<=n-1;i++){
		int fax=getfa(a[i].x),fay=getfa(a[i].y);
		f[fax]=fay;
		sum[fay]+=sum[fax];
		sz[fay]+=sz[fax];
		if(summ-sum[fay]<sz[fay]){
			writeln(ans);
			return 0;
		}
		ans=a[i+1].z;
	}
	return 0;
}
/*

in:
4
1 2 1
2 3 2
3 4 3
1
1
1
1

out:
2

*/
