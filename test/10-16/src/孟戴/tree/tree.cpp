#include<iostream>
#include<algorithm>
#include<string.h>
#include<fstream>
#include<math.h>
#include<iomanip>
using namespace std;
inline long long read(){
    long long x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(long long x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
int head[100003],nxt[200003],to[200003];
long long f[100002][152],g[100002][152],s[100003],sz[100003],tot;
long long ss[153];
long long ans,n,l;
inline void add(int x,int y){
	nxt[++tot]=head[x],head[x]=tot,to[tot]=y;
}
void dfs(int k,int fa){
	int ret=0;
	sz[k]=1;
	s[k]=0;
	f[k][l]=1;
	for(int i=head[k];i;i=nxt[i]){
		int v=to[i];
		if(v!=fa){
			dfs(v,k);
			ret+=s[k]*sz[v]+(s[v]+f[v][l])*sz[k];
			s[k]+=s[v];
			sz[k]+=sz[v];
			for(int y=1;y<l;y++){
				ret-=g[k][l-y-1]*f[v][y];
			}
			for(int x=1;x<l;x++){
				ret-=f[k][x]*f[v][l];
			}
			for(int x=1;x<=l;x++){
				f[k][x]+=f[v][x-1];
			}
			f[k][1]+=f[v][l];
			s[k]+=f[v][l];
			for(int x=1;x<=l;x++){
				g[k][x]=g[k][x-1]+f[k][x];
			}
		}
	}
	ans+=ret;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int k=sizeof(f);
	n=read(),l=read();
	for(int i=1;i<=n-1;i++){
		int x=read(),y=read();
		add(x,y),add(y,x);
	}
	dfs(1,1);
	writeln(ans);
	return 0;
}
