#include<bits/stdc++.h>
using namespace std;
#define int long long
struct node{
	int to,next;
}e[300500];
int sz[200050];
int n,k;
int cnt,dep[200000],p[100010][26],head[200030];
inline int read(){
	int k=0; int f=1; char ch=getchar();
	while(!isdigit(ch)){if(ch=='-') f=-1; ch=getchar();}
	while(isdigit(ch)){k=(k<<1)+(k<<3)+(ch^48);ch=getchar();}
	return k*f;
}
void add(int u,int v){
	++cnt; e[cnt].to=v;
	e[cnt].next=head[u];
	head[u]=cnt;
}
void dfs(int u,int fa){
	dep[u]=dep[fa]+1; 
	for(int i=1;(1<<i)<=dep[u];i++) p[u][i]=p[p[u][i-1]][i-1];
	for(int i=head[u];i;i=e[i].next){
		int v=e[i].to;
		if(v==fa) continue;
		p[v][0]=u;
		dfs(v,u);
	}
}
int getlca(int x,int y){
	if(dep[x]<dep[y]) swap(x,y);
	for(int i=25;i>=0;i--) if(dep[y]<=dep[p[x][i]]) x=p[x][i];
	if(x==y) return x;
	for(int i=25;i>=0;i--){
		if(p[x][i]==p[y][i]) continue;
		x=p[x][i]; y=p[y][i];
	}
	return p[x][0];
}
void dfs1(int u,int fa){
	sz[u]=1;
	for(int i=head[u];i;i=e[i].next){
		int v=e[i].to;
		if(v==fa) continue;
		dfs1(v,u);
		sz[u]+=sz[v];
	}
}
int anss;
void pre(int u,int fa){
	for(int i=head[u];i;i=e[i].next){
		int v=e[i].to;
		if(v==fa) continue;
		//ans[v]=ans[u]
		anss+=sz[v]*(n-sz[v]);
		pre(v,u);
	}
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	 n=read(); k=read();
	for(int i=1;i<n;i++){
		int x,y;
		x=read(); y=read();
		add(x,y); add(y,x);
	} 
	if(k==1&&n>3000){ dep[0]=-1;
		dfs1(1,0);
	//	for(int i=1;i<=n;i++) cout<<sz[i]<<" ";
	//	cout<<endl;
		pre(1,0);
		cout<<anss<<endl;
		return 0;
	}
	dfs(1,0); int ans=0;
	//for(int i=1;i<=n;i++) cout<<dep[i]<<" ";
	//cout<<endl;
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			int lc=getlca(i,j);
			int zh=dep[i]-dep[lc]+dep[j]-dep[lc];
			if(lc==i||lc==j) zh=abs(dep[i]-dep[j]);
		//	cout<<i<<" "<<j<<" "<<lc<<" "<<zh/k+(zh%k!=0)<<endl;
		//	if(i==4&&j==5) cout<<zh<<endl;
			ans+=zh/k;
			if(zh%k) ans++;
			//cout<<ans<<endl;
		}
	//	cout<<ans<<endl;
	}
	cout<<ans; //while(1){
//	}
}
