#include<bits/stdc++.h>
using namespace std;
#define int long long
struct node{
	int to,next,w;
}e[300050];
inline int read(){
	int k=0; int f=1; char ch=getchar();
	while(!isdigit(ch)){if(ch=='-') f=-1; ch=getchar();}
	while(isdigit(ch)){k=(k<<1)+(k<<3)+(ch^48);ch=getchar();}
	return k*f;
}
int cnt,n,zh,head[200050],sz[200050],szz[200050];
int cx[200050];
void add(int u,int v,int w)
{
	++cnt; e[cnt].to=v; e[cnt].next=head[u];
	e[cnt].w=w;
	head[u]=cnt;
}
void pre(int u,int fa){
	sz[u]=cx[u];  szz[u]=1;
	for(int i=head[u];i;i=e[i].next){
		int v=e[i].to;
		if(v==fa) continue;
		pre(v,u);
		sz[u]+=sz[v]; szz[u]+=szz[v];
	}
}
int gs,cs;
void dfs(int u,int fa,int x){
	for(int i=head[u];i;i=e[i].next){
		int v=e[i].to;
		if(v==fa) continue;
		if(e[i].w>=x){
			gs+=szz[v]; cs+=sz[v]; return;	
		}  
		else dfs(v,u,x);
	}
}
bool pd(int x){
	gs=0; cs=0;
	dfs(1,0,x);
	if(n-gs<=cs&&(zh-cs)>=gs) return true;
	return false;
}
signed main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	 n=read();int r=0;
	for(int i=1;i<n;i++){
		int x,y,z; 
		x=read(); y=read(); z=read();
		add(x,y,z); add(y,x,z); r=max(r,z);
	}
	for(int i=1;i<=n;i++) cx[i]=read();
	pre(1,0);
	for(int i=1;i<=n;i++) zh+=sz[i];
	int l=0; int ans=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if(pd(mid)) l=mid+1,ans=mid;
		else r=mid-1;
	}
	cout<<ans;
}
