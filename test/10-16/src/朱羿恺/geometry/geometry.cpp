#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
using namespace std;
const int N = 1010, mo = 1000000007;
int n,a[N],b[N],c[N];
ll ans;
double x[N][N],y[N][N],xx,yy,d1,d2;
const double eps=1e-6;
int main(){
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	n=read();
	For(i,1,n) a[i]=read(),b[i]=read(),c[i]=read();
	For(i,1,n-1){
		For(j,i+1,n){
			int a1=a[i],a2=a[j],b1=b[i],b2=b[j],c1=c[i],c2=c[j];
			x[i][j]=x[j][i]=(c1*b2-c2*b1)*1.0/((a2*b1-a1*b2)*1.0);
			y[i][j]=y[j][i]=(c2*a1-a2*c1)*1.0/((a2*b1-a1*b2)*1.0);
		}
	}
	For(i,1,n-2){
		For(j,i+1,n-1){
			For(k,j+1,n){
				double x1=x[i][j],y1=y[i][j];
				double x2=x[j][k],y2=y[j][k];
				double x3=x[i][k],y3=y[i][k];
				bool flag[4];
				memset(flag,0,sizeof(flag));
				if ((x1!=x2)||(y1!=y2)) flag[1]=1;
				if ((x2!=x3)||(y2!=y3)) flag[2]=1;
				if ((x3!=x1)||(y3!=y1)) flag[3]=1;
				if ((!flag[1])&&(!flag[2])&&(!flag[3])) xx=x1,yy=y1;
				else if (flag[1]&&flag[2]&&flag[3]){
					xx=(x1*x1-x2*x2+y1*y1-y2*y2)*(y1-y3)*1.0;
					xx=xx-(x1*x1-x3*x3+y1*y1-y3*y3)*(y1-y2)*1.0;
					xx=xx/(2*(y1-y3)*(x1-x2)*1.0-2*(y1-y2)*(x1-x3)*1.0);
					yy=(x1*x1-x2*x2+y1*y1-y2*y2)*(x1-x3)*1.0;
					yy=yy-(x1*x1-x3*x3+y1*y1-y3*y3)*(x1-x2)*1.0;
					yy=yy/(2*(y1-y2)*(x1-x3)*1.0-2*(y1-y3)*(x1-x2)*1.0);
				}
				d1=(xx*xx+yy*yy);
				d2=((xx-x1)*(xx-x1)+(yy-y1)*(yy-y1));
				if (d1<d2){double tmp=d1;d1=d2;d2=tmp;}
				if (d1-d2<=eps) ans++;
			}
		}
	}
	printf("%lld",ans);
}
