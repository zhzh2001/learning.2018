#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,k,x,y;
int tot,first[N],to[N<<1],last[N<<1];
inline void Add(int x,int y){to[++tot]=y,last[tot]=first[x],first[x]=tot;}
namespace Subtask1{
	ll ans;
	inline void dfs(int u,int fa,int dis){
		ans+=dis/k+(dis%k>0);
		cross(i,u) if (to[i]!=fa) dfs(to[i],u,dis+1);
	}
	inline void Main(){
		ans=0;
		For(i,1,n) dfs(i,i,0);
		printf("%lld",ans>>1);
	}	
};
namespace Subtask2{
	ll ans,sum[N],size[N];
	inline void dfs(int u,int fa){
		size[u]=1;
		cross(i,u) if (to[i]!=fa) dfs(to[i],u),size[u]+=size[to[i]];
	}
	inline void Dfs(int u,int fa){
		cross(i,u) if (to[i]!=fa) Dfs(to[i],u),ans+=1ll*size[to[i]]*(n-size[to[i]]);
	}
	inline void Main(){dfs(1,0),Dfs(1,0),printf("%lld\n",ans);}
};
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),k=read();
	For(i,1,n-1) x=read(),y=read(),Add(x,y),Add(y,x);
	if (n<=3000) return Subtask1::Main(),0;
	if (k==1) return Subtask2::Main(),0;
}
/*
6 1
1 2
1 3
2 4
2 5
4 6
*/
