#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,k,x,y,a[N],c[N],l=-1e9,r=1e9,mid,ans;
int tot,first[N],to[N<<1],last[N<<1],val[N<<1];
inline void Add(int x,int y,int z){to[++tot]=y,last[tot]=first[x],first[x]=tot,val[tot]=z;}
int cnt,id[N];
struct node{int sum,size;}b[N];
inline bool cmp(node a,node b){return a.sum<b.sum;}
inline void dfs(int u,int fa,int x){
	b[id[u]].sum+=c[u],b[id[u]].size++;
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		if (val[i]>=x) id[v]=++cnt;
			else id[v]=id[u];
		dfs(v,u,x);
	}
}
inline bool check(int x){
	For(i,1,cnt) b[i].size=b[i].sum=0;
	id[1]=cnt=1,dfs(1,0,x);
	sort(b+1,b+1+cnt,cmp);
	//For(i,1,n) printf("%d %d\n",b[i].sum,b[i].size);
	int l=1,last=-1;
	Dow(i,cnt,1){
		if (last!=-1){
			if (b[i].sum>=b[last].size) b[i].sum-=b[last].size,last=-1;
				else{b[last].size-=b[i].sum;continue;}
		}
		while (b[i].sum>=b[l].size&&i!=l&&l<=cnt) b[i].sum-=b[l].size,l++;
		if (l>cnt) return 1;
		if (i==l){
			last=l,l++;
			while (b[i].sum>=b[l].size&&l<=cnt) b[i].sum-=b[l].size,l++;
		} else b[l].size-=b[i].sum;
		
	}
	return 0;
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	For(i,1,n-1) x=read(),y=read(),a[i]=read(),Add(x,y,a[i]),Add(y,x,a[i]);
	For(i,1,n) c[i]=read();
	while (l<=r){
		mid=l+r>>1;//printf("%d %d\n",l,r);
		if (check(mid)) ans=mid,l=mid+1;
			else r=mid-1;
	}
	printf("%d\n",ans);
}
/*
4
1 2 1
2 3 2
3 4 3
1
1
1
1
*/
