#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10;
ll n,head[N],edge,k,f[N][151],s[N],ans,son[N],tmp[N];
struct edge{ll to,net;}e[N<<1];
inline void addedge(ll x,ll y){
	e[++edge].to=y,e[edge].net=head[x],head[x]=edge;
	e[++edge].to=x,e[edge].net=head[y],head[y]=edge;
}
inline ll D(ll x){return x>k?2:1;}
inline void dfs(ll x,ll fa){
	f[x][0]=1;
	son[x]=1;
	for (ll i=head[x];i;i=e[i].net)
		if (e[i].to!=fa){
			dfs(e[i].to,x);
			ans+=s[x]*son[e[i].to]+s[e[i].to]*son[x];
			for (ll j=k-1;~j;--j) tmp[j]=tmp[j+1]+f[e[i].to][j];
			for (ll j=0;j<k;++j) ans+=f[x][j]*(tmp[k-j]+tmp[0]);
		//	for (ll j1=0;j1<k;++j1)
		//		for (ll j2=0;j2<k;++j2)
		//			ans+=D(j1+j2+1)*f[x][j1]*f[e[i].to][j2];
			for (ll j=0;j<k-1;++j) f[x][j+1]+=f[e[i].to][j];
			s[x]+=f[e[i].to][k-1]+s[e[i].to];
			f[x][0]+=f[e[i].to][k-1];
			son[x]+=son[e[i].to];
		}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(n),read(k);
	for (ll i=1,x,y;i<n;++i){
		read(x),read(y);
		addedge(x,y);
	}
	dfs(1,0);
	wr(ans);
	return 0;
}
//sxdakking
/*
6 1
1 2
1 3
2 4
2 5
4 6
*/
