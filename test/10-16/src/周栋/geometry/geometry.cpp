#include <iostream>
#include <cmath>
#include <cstdio>
using namespace std;
typedef long long ll;
typedef double db;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=3100;
const db eps=1e-9;
ll n,ans;
struct line{
	ll a,b,c;
	line(){}
	line(ll A,ll B,ll C):a(A),b(B),c(C){}
	void init(){read(a),read(b),read(c);}
}A[N];
struct point{
	db a,b;
	point(){}
	point(ll A,ll B):a(A),b(B){}
};
inline point line_to_point(line x,line y){
	point ret;
	ret.a=(-x.c*y.a+y.c*x.a)/(db)(x.b*y.a-y.b*x.a);
	ret.b=(-x.c*y.b+y.c*x.b)/(db)(x.a*y.b-y.a*x.b);
	return ret;
}
inline db sqr(db x){return x*x;}
inline db dist(point p1,point p2){return sqrt(sqr(p1.a-p2.a)+sqr(p1.b-p2.b));}
inline db S(point x,point y,point z){
	db a=dist(x,y),b=dist(x,z),c=dist(y,z);
	db p=(a+b+c)/2.;
	return sqrt(p*(p-a)*(p-b)*(p-c));
}
inline bool check_four_point_on_round(point p1,point p2,point p3,point p4){
	db costhu1=S(p1,p2,p3)*2/dist(p1,p3)/dist(p2,p3);
	db costhu2=S(p1,p2,p4)*2/dist(p1,p4)/dist(p2,p4);
	db costhu3=S(p1,p3,p4)*2/dist(p1,p4)/dist(p3,p4);
	db costhu4=S(p1,p3,p2)*2/dist(p1,p2)/dist(p3,p2);
	return fabs(costhu1-costhu2)<eps&&fabs(costhu3-costhu4)<eps;
}
int main(){
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	read(n);
	for (ll i=1;i<=n;++i) A[i].init();
	for (ll i=1;i<=n;++i)
		for (ll j=i+1;j<=n;++j)
			for (ll k=j+1;k<=n;++k){
				point p1=point(0,0);
				point p2=line_to_point(A[i],A[j]);
				point p3=line_to_point(A[i],A[k]);
				point p4=line_to_point(A[j],A[k]);
				ans+=check_four_point_on_round(p1,p2,p3,p4);
			}
	wr(ans);
	return 0;
}
//sxdakking
