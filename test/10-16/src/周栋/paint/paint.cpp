#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar 
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10;
ll n,sum,a[N],ans,head[N],edge,son[N];
struct edge{ll to,net,pay;}e[N<<1];
inline void addedge(ll x,ll y,ll z){
	e[++edge].to=y,e[edge].net=head[x],head[x]=edge,e[edge].pay=z;
	e[++edge].to=x,e[edge].net=head[y],head[y]=edge,e[edge].pay=z;
}
inline ll dfs_son(ll x,ll fa){
	son[x]=1;
	for (ll i=head[x];i;i=e[i].net)
		if (e[i].to!=fa) son[x]+=dfs_son(e[i].to,x);
	return son[x];
}
inline ll dfs_ans(ll x,ll fa){
	ll tot=a[x];
	for (ll i=head[x];i;i=e[i].net)
		if (e[i].to!=fa){
			ll tmp=dfs_ans(e[i].to,x);
			tot+=tmp;
			if (tmp>=n-son[e[i].to]&&sum-tmp>=son[e[i].to]) ans=max(ans,e[i].pay);
		}
	return tot;
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	read(n);
	for (ll i=1,x,y,z;i<n;++i){
		read(x),read(y),read(z);
		addedge(x,y,z);
	}
	for (ll i=1;i<=n;++i) read(a[i]),sum+=a[i];
	dfs_son(1,0);
	dfs_ans(1,0);
	wr(ans);
	return 0;
}
//sxdakking
