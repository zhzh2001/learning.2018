#include<bits/stdc++.h>
using namespace std;
#define int long long
#define maxn 100100
int head[maxn],nxt[maxn<<1],ver[maxn<<1],tot;int n,k;
inline void addedge(int a,int b)
{
	nxt[++tot]=head[a];ver[tot]=b;head[a]=tot;
	nxt[++tot]=head[b];ver[tot]=a;head[b]=tot;
}
int size[maxn];int ans=0;
inline void dfs(int x,int fat)
{
	size[x]=1;
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;
		dfs(y,x);size[x]+=size[y];
	}
	ans+=size[x]*(n-size[x]);
}
inline void solve_1()
{
	dfs(1,0);
	printf("%lld",ans);
	exit(0);
}
inline void dfs(int x,int fat,int curdis)
{
	ans+=(curdis+k-1)/k;
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;
		dfs(y,x,curdis+1);
	}
}
inline void bf()
{
	for(int i=1;i<=n;i++) dfs(i,0,0);
	printf("%lld",ans>>1);exit(0);
}
inline void init()
{
	scanf("%lld%lld",&n,&k);
	for(int i=1,a,b;i<n;i++) scanf("%lld%lld",&a,&b),addedge(a,b);
}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	init();
	if(k==1) solve_1();
	if(n<=3000) bf();
	return puts("A���߾�"),0;
}
