#include<bits/stdc++.h>
using namespace std;
#define maxn 100100
struct edge
{
	int a,b,c;
	inline void read(){scanf("%d%d%d",&a,&b,&c);}
}e[maxn];int x[maxn];
int n,size[maxn],sumx[maxn];long long allsum;
int fa[maxn];inline int find(int x){return x==fa[x]?x:fa[x]=find(fa[x]);}
inline void clear(){for(int i=1;i<=n;i++) fa[i]=i;memset(size,0,sizeof(size));memset(sumx,0,sizeof(sumx));}
inline bool check(int val)
{
	clear();
	for(int i=1;i<n;i++) if(e[i].c<val&&find(e[i].a)!=find(e[i].b)) fa[find(e[i].a)]=find(e[i].b);
	for(int i=1;i<=n;i++) size[find(i)]++,sumx[find(i)]+=x[i];
	for(int i=1;i<n;i++) if(find(i)==i&&allsum-sumx[i]<size[i]) return 0;
	return 1;
}
int val[maxn],tot,tmp[maxn];
inline void init()
{
	scanf("%d",&n);
	for(int i=1;i<n;i++) e[i].read(),tmp[i]=e[i].c;
	for(int i=1;i<=n;i++) scanf("%d",x+i),allsum+=x[i];
	sort(tmp+1,tmp+n);tot=0;
	for(int i=1;i<n;i++) if(i==1||tmp[i]!=tmp[i-1]) val[++tot]=tmp[i];
}
int main()
{
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	init();
	int l=1,r=tot,ans=1;
	while(l<=r)
	{
		int mid=(l+r)>>1;
		if(check(val[mid])) ans=max(ans,mid),l=mid+1;else r=mid-1;
	}
	printf("%d",val[ans]);return 0;
}
