#include<bits/stdc++.h>
using namespace std;
#define eps (1e-5)
struct Line
{
	double a,b,c;
	inline void read(){scanf("%lf%lf%lf",&a,&b,&c);}
}l[3010];
struct Point
{
	double x,y;Point(){}Point(double _x,double _y){x=_x;y=_y;}
	inline void read(){scanf("%lf%lf",&x,&y);}
	Point operator +(const Point &p)const{return Point(x+p.x,y+p.y);}
	Point operator -(const Point &p)const{return Point(x-p.x,y-p.y);}
	Point operator /(const double &p)const{return Point(x/p,y/p);}
	inline double sqr(){return x*x+y*y;}
	inline void print(){printf("(%.10f,%.10f)\n",x,y);}
};
inline double dist(Point a,Point b){return sqrt((a-b).sqr());}
inline Point getPoint(Line x1,Line x2)
{
	Point ans;
	if(abs(x1.a)<=eps)
	{
		ans.y=-1.0*x1.c/x1.b;
		ans.x=(1.0*x2.b*x1.c/x1.b-x2.c)/x2.a;
		return ans;
	}
	if(abs(x1.b)<=eps)
	{
		ans.x=-x1.c/x1.a;
		ans.y=(x2.a*x1.c/x1.a-x2.c)/x2.b;
		return ans;
	}
	if(abs(x2.a)<=eps)
	{
		swap(x1,x2);
		ans.y=-1.0*x1.c/x1.b;
		ans.x=(1.0*x2.b*x1.c/x1.b-x2.c)/x2.a;
		return ans;
	}
	if(abs(x2.b)<=eps)
	{
		swap(x1,x2);
		ans.x=-x1.c/x1.a;
		ans.y=(x2.a*x1.c/x1.a-x2.c)/x2.b;
		return ans;
	}
	ans.y=(1.0*x1.a*x2.c/x2.a-x1.c)/(1.0*x1.b-1.0*x1.a*x2.b/x2.a);
	ans.x=1.0*(-1.0*x2.b*ans.y-x2.c)/(1.0*x2.a);
	return ans;
}
int n;
inline Line getmid(Point a,Point b)
{
	if(abs(a.y-b.y)<=eps){Line ans;ans.a=1.0;ans.b=0.0;ans.c=-(a.x+b.x)/2.0;return ans;}
	Point mid=(a+b)/2.0;
	double getk=-(a.x-b.x)/(a.y-b.y);
	double getb=mid.y-getk*mid.x;
	Line ans;
	ans.a=getk;ans.b=-1;ans.c=getb;
	return ans;
}
int main()
{
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	cin>>n;if(n>500) return puts("A���߾�"),0;
	for(int i=1;i<=n;i++) l[i].read();int ans=0;
	for(int i=1;i<=n;i++) for(int j=i+1;j<=n;j++) for(int k=j+1;k<=n;k++)
	{
		Point a=getPoint(l[i],l[j]),b=getPoint(l[i],l[k]),c=getPoint(l[j],l[k]);
		if(dist(a,b)<=eps||dist(a,c)<=eps||dist(b,c)<=eps) continue;
		Point Cen=getPoint(getmid(a,b),getmid(b,c));
		if(abs(dist(Cen,Point(0,0))-dist(Cen,a))<=eps) ans++;
	}
	cout<<ans;
}
