#include<bits/stdc++.h>
#define ll long long
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
#define N 100005 
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0' || ch>'9') {if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0' && ch<='9') {x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
struct edge{ ll to,next; }e[N<<1];
ll n,m,head[N],tot,ans,sz[N];
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
void dfs1(ll u,ll now,ll last){
	ans+=now/m; if (now%m) ++ans;
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue;
		dfs1(v,now+1,u);
			
	}
}
void dfs2_1(ll u,ll last){
	sz[u]=1;
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue; dfs2_1(v,u); sz[u]+=v;
	}
}
void dfs2_2(ll u,ll now,ll last){
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue; ans+=sz[v]*(sz[u]+now-sz[v]);
		dfs2_2(v,now+sz[u]-sz[v],u);
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(); m=read();
	rep(i,2,n){
		ll u=read(),v=read();
		add(u,v); add(v,u);
	}
	if (m==1){
		dfs2_1(1,-1); dfs2_2(1,0,-1);
		return printf("%lld",ans)&0;
	}else{
		rep(i,1,n) dfs1(i,0,-1);
		return printf("%lld",ans/2)&0;
	}
}
