#include<bits/stdc++.h>
#define ll long long
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
#define N 100005
#define M 160
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0' || ch>'9') {if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0' && ch<='9') {x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
struct edge{ ll to,next; }e[N<<1];
ll n,m,head[N],tot,ans[N],f[N][M],sum[N][M];
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
void dfs(ll u,ll last){
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue; dfs(v,u); ans[u]+=ans[v];
		rep(j,1,m-1) ans[u]+=f[v][j]*sum[u][m-j];
		rep(j,1,m-1) f[u][m]+=f[v][m-1]; f[u][0]+=f[v][m-1];
		per(j,m-1,1) sum[u][j]=sum[u][j+1]+f[u][j];
	}
	ans[u]+=f[u][0]; f[u][0]++;
}
int main(){
	n=read(); m=read();
	rep(i,2,n){
		ll u=read(),v=read();
		add(u,v); add(v,u);
	}
	dfs(1,-1);
	printf("%lld",ans[1]);
}
