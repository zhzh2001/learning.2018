#include<bits/stdc++.h>
#define ll long long
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
#define N 100005 
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0' || ch>'9') {if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0' && ch<='9') {x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(int x){
	if(x<0) putchar('-'),x=-x;
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
ll n,ans,fa[N],sz[N],cnt[N],flag[N],x[N],a[N],b[N];
struct edge{ ll u,v,w; }e[N];
bool cmp(edge x,edge y) { return x.w<y.w; }
ll find(ll x) { return fa[x]==x?x:fa[x]=find(fa[x]); }
bool check(ll num){
	rep(i,1,n) fa[i]=i,sz[i]=x[i],cnt[i]=1,flag[i]=0;
	rep(i,1,n-1){
		if (e[i].w>=num) break;
		ll p=find(e[i].u),q=find(e[i].v);
		if (p!=q){
			fa[p]=q; sz[q]+=sz[p]; cnt[q]+=cnt[p];
		}
	}
	ll len=0,sum=0;
	rep(i,1,n){
		ll p=find(i);
		if (!flag[p]) a[++len]=cnt[p],b[len]=sz[p],sum+=sz[p],flag[p]=1;
	}
	rep(i,1,len) if (a[i]>sum-b[i]) return 0;
	return 1;
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	rep(i,1,n-1){
		e[i].u=read();
		e[i].v=read();
		e[i].w=read();
	}
	rep(i,1,n) x[i]=read();
	sort(e+1,e+n,cmp);
	ll l=0,r=1e9;
	while (l<=r){
		ll mid=(l+r)>>1;
		if (check(mid)) ans=mid,l=mid+1;
		else r=mid-1;
	}
	printf("%lld",ans);
	return 0;
}
/*
4
1 2 1
2 3 2
3 4 3
1
1
1
1

*/

