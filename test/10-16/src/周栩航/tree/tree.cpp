#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pa pair<int,int>
#define fir first
#define sec second
#define mk make_pair
using namespace std;

inline ll read(){ll x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(ll x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(ll x){write(x); puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=100005;
ll ans;
int n,k,poi[N*2],nxt[N*2],head[N],cnt;
ll f[N][151],g[N][151];
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;}
inline void Dfs(int x,int fa,int dep)
{
	int td=dep%k;
	ll tans=0;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		Dfs(poi[i],x,dep+1);
		For(t,0,k-1)	if(f[x][t])
			For(j,0,k-1)	if(f[poi[i]][j])
			{
				int ys=t+j-2*td;
				tans+=(g[x][t]*f[poi[i]][j]+g[poi[i]][j]*f[x][t]-2*dep*f[x][t]*f[poi[i]][j]+f[x][t]*f[poi[i]][j]*(2*td-j-t))/k;
				if(ys>0)	tans+=f[x][t]*f[poi[i]][j]*((ys-1)/k+1);
				if(ys<0)	tans+=f[x][t]*f[poi[i]][j]*((ys)/k);
			}
	
		For(j,0,k-1)	f[x][j]+=f[poi[i]][j],g[x][j]+=g[poi[i]][j];//未加上x这个点 
	}
	For(j,0,k-1)
	{
		if(j<=td)	tans+=(g[x][j]+f[x][j]*(td-j)-dep*f[x][j])/k;
		else	tans+=(g[x][j]+f[x][j]*(td-j)-dep*f[x][j])/k+f[x][j];
	}
	ans+=tans;
	g[x][td]+=dep;f[x][td]++;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();k=read();
	For(i,1,n-1)	add(read(),read());
	Dfs(1,1,0);
	writeln(ans);
}
//f[i][j] i为根的子树节点中，dep%k==j的数量
//g[i][j] i为根的子树节点中，dep%k==j的深度和

/*
	当前深度为dep
	dep%k= td;
	
	j<=td
	(g[i][j]+f[i][j]*(td-j)-dep*f[i][j])/k
	
	j>td
	(g[i][j]+f[i][j]*(td-j)-dep*f[i][j])/k+f[i][j];
	
	j+t<=2*td
	(g[x][t]*f[poi[i]][j]+g[poi[i]][j]*f[x][t]+f[x][t]*f[poi[i]][j]*(2*td-j-t)-2*dep*f[x][t]*f[poi[i]][j])/k
	
	j+t>2*td
	(g[x][t]*f[poi[i]][j]+g[poi[i]][j]*f[x][t]+f[x][t]*f[poi[i]][j]*(2*td-j-t)-2*dep*f[x][t]*f[poi[i]][j])/k+f[x][j]*f[poi[i]][j]
*/ 
