#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pa pair<double,double>
#define fir first
#define sec second
#define eps 1e-6
#define mk make_pair
#define double long double
using namespace std;

inline ll read(){ll x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(ll x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(ll x){write(x); puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=3005;
int n,a,b,c,ans;
struct node{double a,b,c;}	l[N];
inline double Dis(pa x,pa y){return sqrt((x.fir-y.fir)*(x.fir-y.fir)+(x.sec-y.sec)*(x.sec-y.sec));}
inline pa Get(node l1,node l2)
{
	if(l1.a==0)	swap(l1,l2);//l1.b 可能=0(竖线）  l2.a可能=0（横线） 
	pa p1,p2;
	p1.fir=-l1.a/l1.b;p1.sec=-l1.c/l1.b;
	p2.fir=-l2.a/l2.b;p2.sec=-l2.c/l2.b;
	if(l1.b==0)
	{
		if(l2.a==0)
		{
			return mk(-l1.c/l1.a,-l2.c/l2.b);
		}else	
		{
			double x=-l1.c/l1.a;double y=p2.fir*x+p2.sec;
			return mk(x,y);
		}
	}
	if(l2.a==0)
	{
		double y=-l2.c/l2.b;
		double x=(-y*l1.b-l1.c)/l1.a;
		return mk(x,y);
	}
	double x=1.0*(p2.sec-p1.sec)/(1.0*(p1.fir-p2.fir));
	double y=p1.fir*x+p1.sec;
	pa tmp=mk(x,y);
	return tmp;
}
inline node Get_line(pa p1,pa p2)
{
	if(p1.fir==p2.fir)	return (node){0,1,(p1.sec+p2.sec)/2};
	double k=(p1.sec-p2.sec)/(p1.fir-p2.fir);
	double b=p1.sec-k*p1.fir;
	double x=(p1.fir+p2.fir)/2;
	double y=(p1.sec+p2.sec)/2;
	if(k==0)	return (node){1,0,(p1.fir+p2.fir)/2};
	k=-1/k;
	b=y-k*x;
	return (node){k,-1,b};
}

int main()
{
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	n=read();
	For(i,1,n)	l[i].a=read(),l[i].b=read(),l[i].c=read();
	For(i,1,n)	For(j,i+1,n)	For(k,j+1,n)
	{
		pa t1=Get(l[i],l[j]),t2=Get(l[j],l[k]),t3=Get(l[i],l[k]);
		if(fabs(t1.fir-t2.fir)<=eps&&fabs(t1.sec-t2.sec)<=eps)	{ans++;continue;}
		node line1=Get_line(t1,t2),line2=Get_line(t1,t3);
		pa O=Get(line1,line2);
		if(fabs(Dis(O,t1)-Dis(O,mk(0,0)))<=eps)	ans++;
	}
	writeln(ans);
}
