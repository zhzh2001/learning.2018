#include <bits/stdc++.h>
using namespace std;
const int N=520000;
int n,m,s,cnt,k,head[N],fa[N][25],depth[N];
struct node { int next,to,w;} sxd[N<<1];
inline int read() { int X=0,w=0; char ch=0; while(!isdigit(ch)) {w|=ch=='-';ch=getchar();} while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar(); return w?-X:X;}
inline void write(int x) { if(x<0) putchar('-'),x=-x; if(x>9) write(x/10); putchar(x%10+'0');}
void add(int u,int v,int w) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	sxd[cnt].w=w;
	head[u]=cnt;
}
void dfs(int at,int f) {
	depth[at]=depth[f]+1;
	fa[at][0]=f;
	for(int i=1; (1<<i)<=depth[at]; i++) fa[at][i]=fa[fa[at][i-1]][i-1];
	for(int i=head[at]; i; i=sxd[i].next) {
		if(sxd[i].to==f) continue;
		dfs(sxd[i].to,at);
	}
}
int lca(int x,int y) {
	if(depth[x]>depth[y]) swap(x,y);
	for(int i=20; i>=0; i--)
		if(depth[x]<=depth[y]-(1<<i)) y=fa[y][i];
	if(x==y) return x;
	for(int i=20; i>=0; i--)
		if(fa[x][i]!=fa[y][i]) { x=fa[x][i];y=fa[y][i];}
	return fa[x][0];
}
int times[1200];
int f[1200],used[1200],ans,ztn;
int main() {
	freopen("paint.in","r",stdin);freopen("paint.out","w",stdout);
	srand((int)time(0));
	int tim=clock();
	cin>>n;
	for(int i=1;i<n;i++) {int x=read(),y=read(),z=read();add(x,y,z);add(y,x,z);}
	dfs(1,0);
	for(int i=1;i<=n;i++) times[i]=read();
	while(clock()-tim<=900) {
		memset(used,0,sizeof used);
		for(int i=1;i<=n;i++) {
			L1:;
			f[i]=rand()%n+1;
			if(used[f[i]]+1>times[i]) goto L1;
			used[f[i]]++;
		}
		//for(int i=1;i<=n;i++) cout<<f[i]<<" ";
		//cout<<'\n';
		int now=0x3f3f3f3f;
		for(int i=1;i<=n;i++) if(i==f[i]) goto L2;
		for(int i=1;i<=n;i++) {
			int aa=i,bb=f[i];
			int ll=lca(aa,bb);
			// cout<<aa<<" "<<bb<<" "<<ll<<'\n';
			int mx=0;
			if(depth[aa]>depth[bb]) swap(aa,bb);
			// cout<<aa<<" "<<bb<<'\n';
			if(ll==aa) {
				while(aa!=bb) {
					for(int ii=head[bb];ii;ii=sxd[ii].next) {
						if(depth[sxd[ii].to]+1==depth[bb]) {
							bb=sxd[ii].to;
							mx=max(mx,sxd[ii].w);
						}
					}
				}
			} else {
				while(aa!=bb) {
					for(int ii=head[aa];ii;ii=sxd[ii].next) {
						if(depth[sxd[ii].to]+1==depth[aa]) {
							aa=sxd[ii].to;
							mx=max(mx,sxd[ii].w);
						}
					}
					for(int ii=head[bb];ii;ii=sxd[ii].next) {
						if(depth[sxd[ii].to]+1==depth[bb]) {
							bb=sxd[ii].to;
							mx=max(mx,sxd[ii].w);
						}
					}
				}
			}
			now=min(now,mx);
			if(now<ans) goto L2;
		}
		ztn++;
		ans=max(ans,now);
		L2:;
	}
	write(ans);
	// puts("");
	// write(ztn);
	return 0;
}