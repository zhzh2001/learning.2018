#include <bits/stdc++.h>
using namespace std;
const int N=520000;
int n,m,s,cnt,k,head[N],fa[N][25],depth[N],sz[N],bianx[N],biany[N];
struct node { int next,to;} sxd[N<<1];
inline int read() { int X=0,w=0; char ch=0; while(!isdigit(ch)) {w|=ch=='-';ch=getchar();} while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar(); return w?-X:X;}
inline void write(int x) { if(x<0) putchar('-'),x=-x; if(x>9) write(x/10); putchar(x%10+'0');}
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	head[u]=cnt;
}
void dfs(int at,int f) {
	depth[at]=depth[f]+1;
	fa[at][0]=f;
	int sz_now=1;
	for(int i=1; (1<<i)<=depth[at]; i++) fa[at][i]=fa[fa[at][i-1]][i-1];
	for(int i=head[at]; i; i=sxd[i].next) {
		if(sxd[i].to==f) continue;
		dfs(sxd[i].to,at);
		sz_now+=sz[sxd[i].to];
	}
	sz[at]=sz_now;
}
int lca(int x,int y) {
	if(depth[x]>depth[y]) swap(x,y);
	for(int i=20; i>=0; i--)
		if(depth[x]<=depth[y]-(1<<i)) y=fa[y][i];
	if(x==y) return x;
	for(int i=20; i>=0; i--)
		if(fa[x][i]!=fa[y][i]) { x=fa[x][i];y=fa[y][i];}
	return fa[x][0];
}
int main() {
	freopen("tree.in","r",stdin); freopen("tree.out","w",stdout);
	cin>>n>>k;
	for(int i=1;i<n;i++) {int x=read(),y=read();add(x,y);add(y,x);bianx[i]=x;biany[i]=y;}
	dfs(1,0);
	int ans=0;
	if(k==1) {
		for(int i=1;i<n;i++) {
			int x=bianx[i],y=biany[i];
			if(sz[x]>sz[y]) swap(x,y);
			ans+=sz[x]*(n-sz[x]);
		}
	} else {
		for(int i=1; i<n; i++)
			for(int j=i+1; j<=n; j++) {
				ans+=(depth[i]+depth[j]-2*depth[lca(i,j)]-1)/k+1;
			}
	}
	write(ans);
	return 0;
}