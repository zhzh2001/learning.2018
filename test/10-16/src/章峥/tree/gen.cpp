#include<fstream>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("tree.in");
const int n=100,k=5;
int f[n+5];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<' '<<k<<endl;
	for(int i=1;i<=n;i++)
		f[i]=i;
	for(int i=1;i<n;)
	{
		uniform_int_distribution<> dn(1,n);
		int u=dn(gen),v=dn(gen);
		if(getf(u)!=getf(v))
		{
			fout<<u<<' '<<v<<endl;
			f[getf(u)]=getf(v);
			i++;
		}
	}
	return 0;
}
