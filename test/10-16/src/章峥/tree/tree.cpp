#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N=100005,K=5;
int n,k,head[N],v[N<<1],nxt[N<<1],e,into[N],outo[N],t,d[N],rid[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int u,int fat)
{
	into[u]=++t;
	rid[t]=u;
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			d[v[i]]=d[u]+1;
			dfs(v[i],u);
		}
	outo[u]=t;
}
struct node
{
	long long sum;
	int lazy,cnt[K];
}tree[1<<18];
inline void pullup(int id)
{
	tree[id].sum=tree[id<<1].sum+tree[id<<1|1].sum;
	for(int i=0;i<k;i++)
		tree[id].cnt[i]=tree[id<<1].cnt[i]+tree[id<<1|1].cnt[i];
}
void build(int id,int l,int r)
{
	if(l==r)
	{
		tree[id].sum=(d[rid[l]]+k-1)/k;
		tree[id].cnt[d[rid[l]]%k]=1;
	}
	else
	{
		int mid=(l+r)>>1;
		build(id<<1,l,mid);
		build(id<<1|1,mid+1,r);
		pullup(id);
	}
}
inline int sign(int x)
{
	if(x>0)
		return 1;
	if(x<0)
		return -1;
	return 0;
}
int tmp[K];
void rotate(int *l,int *mid,int *r)
{
	int cc=0;
	for(int* it=mid;it!=r;++it)
		tmp[cc++]=*it;
	for(int* it=l;it!=mid;++it)
		tmp[cc++]=*it;
	for(int i=0;i<cc;i++)
		*l++=tmp[i];
}
inline void pushdown(int id)
{
	if(tree[id].lazy)
	{
		tree[id<<1].lazy+=tree[id].lazy;
		tree[id<<1|1].lazy+=tree[id].lazy;
		if(abs(tree[id].lazy)>=k)
		{
			int tot=0;
			for(int i=0;i<k;i++)
				tot+=tree[id<<1].cnt[i];
			tree[id<<1].sum+=tot*(abs(tree[id].lazy)/k)*sign(tree[id].lazy);
			tot=0;
			for(int i=0;i<k;i++)
				tot+=tree[id<<1|1].cnt[i];
			tree[id<<1|1].sum+=tot*(abs(tree[id].lazy)/k)*sign(tree[id].lazy);
			tree[id].lazy-=abs(tree[id].lazy)/k*k*sign(tree[id].lazy);
		}
		if(tree[id].lazy>0)
		{
			for(int i=k+1-tree[id].lazy;i<k;i++)
			{
				tree[id<<1].sum+=tree[id<<1].cnt[i];
				tree[id<<1|1].sum+=tree[id<<1|1].cnt[i];
			}
			tree[id<<1].sum+=tree[id<<1].cnt[0];
			tree[id<<1|1].sum+=tree[id<<1|1].cnt[0];
			rotate(tree[id<<1].cnt,tree[id<<1].cnt+k-tree[id].lazy,tree[id<<1].cnt+k);
			rotate(tree[id<<1|1].cnt,tree[id<<1|1].cnt+k-tree[id].lazy,tree[id<<1|1].cnt+k);
		}
		else
		{
			for(int i=-tree[id].lazy;i;i--)
			{
				tree[id<<1].sum-=tree[id<<1].cnt[i];
				tree[id<<1|1].sum-=tree[id<<1|1].cnt[i];
			}
			rotate(tree[id<<1].cnt,tree[id<<1].cnt-tree[id].lazy,tree[id<<1].cnt+k);
			rotate(tree[id<<1|1].cnt,tree[id<<1|1].cnt-tree[id].lazy,tree[id<<1|1].cnt+k);
		}
		tree[id].lazy=0;
	}
}
void modify(int id,int l,int r,int L,int R,int val)
{
	if(L<=l&&R>=r)
	{
		tree[id].lazy+=val;
		if(val==1)
		{
			tree[id].sum+=tree[id].cnt[0];
			rotate(tree[id].cnt,tree[id].cnt+k-1,tree[id].cnt+k);
		}
		else
		{
			tree[id].sum-=tree[id].cnt[1%k];
			rotate(tree[id].cnt,tree[id].cnt+1,tree[id].cnt+k);
		}
	}
	else
	{
		pushdown(id);
		int mid=(l+r)>>1;
		if(L<=mid)
			modify(id<<1,l,mid,L,R,val);
		if(R>mid)
			modify(id<<1|1,mid+1,r,L,R,val);
		pullup(id);
	}
}
long long ans;
void dfs2(int u,int fat)
{
	ans+=tree[1].sum;
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			modify(1,1,n,into[v[i]],outo[v[i]],-1);
			if(into[v[i]]>1)
				modify(1,1,n,1,into[v[i]]-1,1);
			if(outo[v[i]]<n)
				modify(1,1,n,outo[v[i]]+1,n,1);
			dfs2(v[i],u);
			modify(1,1,n,into[v[i]],outo[v[i]],1);
			if(into[v[i]]>1)
				modify(1,1,n,1,into[v[i]]-1,-1);
			if(outo[v[i]]<n)
				modify(1,1,n,outo[v[i]]+1,n,-1);
		}
}
int main()
{
	fin>>n>>k;
	if(k>5)
	{
		fout<<1ll*n*(n-1)/2<<endl;
		return 0;
	}
	for(int i=1;i<n;i++)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	dfs(1,0);
	build(1,1,n);
	dfs2(1,0);
	fout<<ans/2<<endl;
	return 0;
}
