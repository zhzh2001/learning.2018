#include<fstream>
#include<algorithm>
#include<cmath>
using namespace std;
ifstream fin("geometry.in");
ofstream fout("geometry.out");
const int N=3005;
const double eps=1e-12,inf=1e100;
double x[N],y[N],k[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		double a,b,c;
		fin>>a>>b>>c;
		x[i]=b*c/(a*a+b*b);
		y[i]=a*c/(a*a+b*b);
	}
	long long ans=0;
	for(int i=1;i<=n;i++)
	{
		int kn=0,ori=0;
		for(int j=i+1;j<=n;j++)
			if(x[i]==0&&y[i]==0&&x[j]==0&&y[j]==0)
				ori++;
			else
			{
				if(fabs(x[i]-x[j])<eps)
					k[++kn]=inf;
				else
					k[++kn]=(y[i]-y[j])/(x[i]-x[j]);
			}
		sort(k+1,k+kn+1);
		for(int j=1;j<=ori;j++)
			ans+=n-i-j;
		for(int j=1,t;j<=kn;j=t)
		{
			for(t=j;t<=kn&&k[t]-k[j]<eps;t++);
			ans+=(t-j-1)*(t-j)/2;
		}
	}
	fout<<ans<<endl;
	return 0;
}
