#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("paint.in");
ofstream fout("paint.out");
const int N=100005;
int x[N],f[N],sz[N];
struct edge
{
	int u,v,w;
	bool operator<(const edge& rhs)const
	{
		return w<rhs.w;
	}
}e[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<n;i++)
		fin>>e[i].u>>e[i].v>>e[i].w;
	sort(e+1,e+n);
	int tot=0;
	for(int i=1;i<=n;i++)
	{
		fin>>x[i];
		tot+=x[i];
		f[i]=i;
		sz[i]=1;
	}
	for(int i=1;i<n;i++)
	{
		int ru=getf(e[i].u),rv=getf(e[i].v);
		f[ru]=rv;
		sz[rv]+=sz[ru];
		x[rv]+=x[ru];
		if(sz[rv]>tot-x[rv])
		{
			fout<<e[i].w<<endl;
			break;
		}
	}
	return 0;
}
