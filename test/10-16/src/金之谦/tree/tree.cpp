#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int nedge=0,p[2*N],nex[2*N],head[2*N];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
int f[N][160],g[N][160],ans=0,n,K,s[N],ss[N];
inline void dfs(int x,int fa){
	f[x][0]=s[x]=1;ss[x]=0;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		dfs(p[k],x);
		ans+=ss[x]*s[p[k]]+s[x]*(ss[p[k]]+f[p[k]][0]);
		s[x]+=s[p[k]];ss[x]+=ss[p[k]];
		for(int i=1;i<K;i++){
			ans-=f[p[k]][i]*g[x][K-i-1]+f[x][i]*f[p[k]][0];
		}
		for(int i=K-1;i;i--)f[x][i]+=f[p[k]][i-1];
		f[x][0]+=f[p[k]][K-1];ss[x]+=f[p[k]][0];
		for(int i=1;i<K;i++)g[x][i]=g[x][i-1]+f[x][i];
	}
}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();K=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addedge(x,y);addedge(y,x);
	}
	dfs(1,0);
	writeln(ans);
	return 0;
}
