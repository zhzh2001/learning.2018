#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int n,sum=0;
struct ppap{int x,y,v;}a[N];
inline bool cmp(ppap a,ppap b){return a.v<b.v;}
int fa[N],s[N],b[N];
inline int find(int x){
	if(fa[x]==x)return x;
	return fa[x]=find(fa[x]);
}
signed main()
{
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		a[i].x=read();a[i].y=read();a[i].v=read();
	}
	for(int i=1;i<=n;i++){
		b[i]=read();
		fa[i]=i;s[i]=1;
		sum+=b[i];
	}
	sort(a+1,a+n,cmp);
	int i=1;
	for(;i<=n;i++){
		int fx=find(a[i].x),fy=find(a[i].y);
		if(fx!=fy){
			fa[fx]=fy;s[fy]+=s[fx];b[fy]+=b[fx];
			if(s[fy]>sum-b[fy])break;
		}
	}
	writeln(a[i].v);
	return 0;
}
