#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define eps 1e-6
#define maxn 110
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
double a[maxn],b[maxn],c[maxn];
int n,ans;
pair<double,double> calc(pair<pair<double,double>,double> a,pair<pair<double,double>,double> b)
{
	double a1=a.first.first,b1=a.first.second,c1=a.second,a2=b.first.first,b2=b.first.second,c2=b.second;
	if(b1==0) return make_pair(-c1/a1,-(a2*(-c1/a1)+c2)/b2);
	double a3=a1,c3=c1;
	(a1*=b2/b1)-=a2;
	(c1*=b2/b1)-=c2;
	// printf("%lf %lf\n",a1,c1);
	return make_pair(-c1/a1,-(a3*(-c1/a1)+c3)/b1);
}
double calc4(double a,double b,double c,double d)
{
	return a*c+b*d;
}
pair<pair<double,double>,double> calc2(pair<double,double> a,pair<double,double> b)
{
	if(a.first<b.first) swap(a,b);
	return make_pair(make_pair(a.first-b.first,a.second-b.second),-calc4((a.first+b.first)/2,(a.second+b.second)/2,a.first-b.first,a.second-b.second));
}
double calc3(double x)
{
	return x*x;
}
double juli(pair<double,double> a,pair<double,double> b)
{
	return calc3(a.first-b.first)+calc3(a.second-b.second);
}
signed main()
{
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	// printf("%lf %lf",calc(make_pair(make_pair(2,3),4),make_pair(make_pair(4,5),6)).first,calc(make_pair(make_pair(2,3),4),make_pair(make_pair(4,5),6)).second);
	// printf("%lf,%lf,%lf",calc2(make_pair(-1,0),make_pair(0,2)).first.first,calc2(make_pair(-1,0),make_pair(0,2)).first.second,calc2(make_pair(-1,0),make_pair(0,2)).second);
	n=read();
	for(int i=1;i<=n;i++)
	{
		a[i]=read();
		b[i]=read();
		c[i]=read();
	}
	for(int i=1;i<n-1;i++)
		for(int j=i+1;j<n;j++)
		{
			pair<double,double> point1=calc(make_pair(make_pair(a[i],b[i]),c[i]),make_pair(make_pair(a[j],b[j]),c[j]));
			for(int k=j+1;k<=n;k++)
			{
				pair<double,double> point2=calc(make_pair(make_pair(a[i],b[i]),c[i]),make_pair(make_pair(a[k],b[k]),c[k]));
				pair<double,double> point3=calc(make_pair(make_pair(a[j],b[j]),c[j]),make_pair(make_pair(a[k],b[k]),c[k]));
				pair<pair<double,double>,double> xian1=calc2(point1,point2);
				pair<pair<double,double>,double> xian2=calc2(point1,point3);
				pair<double,double> point4=calc(xian1,xian2);
				// printf("%d %d %d %lf %lf\n",i,j,k,point1.first,point1.second);
				if(abs(juli(point4,point1)-juli(point4,make_pair(0,0)))<eps) ans++;
			}
		}
	write(ans);
	return 0;
}
