#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100010
#define maxk 160
#define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
vector<int> a[maxn];
int n,k,f[maxn][maxk],ans[maxn],aans,ans2[maxn],f2[maxn][maxk];
void dfs1(int kk,int fa)
{
	for(int j=0;j<a[kk].size();j++)
	{
		int x=a[kk][j];
		if(x==fa) continue;
		dfs1(x,kk);
		for(int i=0;i<k;i++)
			f[kk][i]+=f[x][(i-1+k)%k];
		// ans[k]+=f[x][k-1];
		ans[kk]+=ans[x];
	}
	ans[kk]+=f[kk][0];
	f[kk][0]++;
	// wrs(kk);wrs(ans[kk]);wrs(f[kk][0]);wln(f[kk][1]);
}
void dfs2(int kk,int fa)
{
	for(int i=0;i<k;i++)
		f2[kk][i]=f[kk][i]+f2[fa][(i-1+k)%k];
	ans2[kk]=ans[kk]+ans2[fa]+f2[fa][k-1];
	for(int i=1;i<k;i++)
		aans+=f2[kk][i];
	aans+=ans2[kk];
	// {int ab=0;
	// ab+=ans2[kk];
	// for(int i=1;i<k;i++)
	// ab+=f2[kk][i];wrs(kk);wln(ab);}
	for(int j=0;j<a[kk].size();j++)
	{
		int x=a[kk][j];
		if(x==fa) continue;
		for(int i=0;i<k;i++)
			f2[kk][i]-=f[x][(i-1+k)%k];
		ans2[kk]-=ans[x]+f[x][k-1];
		dfs2(x,kk);
		for(int i=0;i<k;i++)
			f2[kk][i]+=f[x][(i-1+k)%k];
		ans2[kk]+=ans[x]+f[x][k-1];
	}
}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	k=read();
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read();
		a[x].push_back(y);
		a[y].push_back(x);
	}
	dfs1(1,0);
	// wrs(ans[1]);wln(f[1][1]);
	// write(ans[1]+f[1][1]);
	dfs2(1,0);
	write(aans>>1);
	return 0;
}
