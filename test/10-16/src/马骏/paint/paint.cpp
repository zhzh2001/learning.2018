#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100010
#define inf 0x3f3f3f3f
#define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int m,l,r,sum,a[maxn],n,f,siz[maxn],alr[maxn],bb[maxn<<1][3],h[maxn],rr;
void add(int x,int y,int z)
{
	bb[++rr][0]=y;
	bb[rr][1]=z;
	bb[rr][2]=h[x];
	h[x]=rr;
}
void dfs(int k,int fa)
{
	alr[k]=a[k];
	siz[k]=1;
	for(int i=h[k];i;i=bb[i][2])
	{
		if(bb[i][0]==fa) continue;
		dfs(bb[i][0],k);
		alr[k]+=alr[bb[i][0]];
		siz[k]+=siz[bb[i][0]];
	}
}
void dfs2(int k,int fa,int mst,int nown,int nowsum)
{
	for(int i=h[k];nown>0&&!f&&i;i=bb[i][2])
	{
		if(bb[i][0]==fa) continue;
		if(bb[i][1]>=mst)
		{
			if(siz[bb[i][0]]<=nowsum-alr[bb[i][0]]&&nown-siz[bb[i][0]]<=alr[bb[i][0]])
			{
				// wrs(m),wrs(k);wrs(bb[i][0]);
				// wrs(siz[bb[i][0]]);
				// wrs(nown-siz[bb[i][0]]);
				// wrs(alr[bb[i][0]]);
				// wln(nowsum-alr[bb[i][0]]);
				f=1;
				return;
			}
			if(siz[bb[i][0]]<=nowsum-alr[bb[i][0]])
			{
				nowsum-=siz[bb[i][0]];
				nown-=siz[bb[i][0]];
				continue;
			}
			if(nown-siz[bb[i][0]]<=alr[bb[i][0]])
			{
				dfs2(bb[i][0],k,mst,siz[bb[i][0]],alr[bb[i][0]]);
				return;
			}
		}
		else dfs2(bb[i][0],k,mst,nown,nowsum);
	}
	if(nown<=0) f=1;
}
int check(int k)
{
	f=0;
	dfs2(1,0,k,n,sum);
	return f;
}
signed main()
{
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	l=inf;
	r=-inf;
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read(),z=read();
		add(x,y,z);
		add(y,x,z);
		l=min(l,z);
		r=max(r,z);
	}
	for(int i=1;i<=n;i++)
		sum+=(a[i]=read());
	dfs(1,0);
	// for(int i=1;i<=n;i++)
		// wrs(siz[i]),wln(alr[i]);
	r++;
	for(;l<r-1;)
	{
		m=l+r>>1;
		if(check(m)) l=m;
			else r=m;
	}
	write(l);
	return 0;
}
