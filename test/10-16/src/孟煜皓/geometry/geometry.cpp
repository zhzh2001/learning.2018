#include <cstdio>
#define eps 1e-8
int n, ans;
double Abs(double a){
	return a > 0 ? a : -a;
}
struct Point{
	double x, y;
	bool operator == (const Point &res) const {
		return Abs(x - res.x) < eps && Abs(y - res.y) < eps;
	}
}A, B, C, D, O;
struct Line{
	double a, b, c;
}L[3005], f, g;
Point Mid(Point A, Point B){
	return (Point){(A.x + B.x) / 2, (A.y + B.y) / 2};
}
Line PB(Point A, Point B){
	Line res;
	Point C = Mid(A, B);
	res.a = A.x - B.x, res.b = A.y - B.y, res.c = -(res.a * C.x + res.b * C.y);
	return res;
}
Point Is(Line A, Line B){
	Point res;
	res.x = (A.c * B.b - B.c * A.b) / (A.b * B.a - B.b * A.a);
	res.y = (A.c * B.a - B.c * A.a) / (A.a * B.b - B.a * A.b);
	return res;
}
double Dis2(Point A, Point B){
	return (A.x - B.x) * (A.x - B.x) + (A.y - B.y) * (A.y - B.y);
}
int main(){
	freopen("geometry.in", "r", stdin);
	freopen("geometry.out", "w", stdout);
	scanf("%d", &n);
	for (register int i = 1; i <= n; ++i)
		scanf("%lf%lf%lf", &L[i].a, &L[i].b, &L[i].c);
	O = (Point){0, 0};
	for (register int i = 1; i <= n; ++i)
		for (register int j = i + 1; j <= n; ++j)
			for (register int k = j + 1; k <= n; ++k){
//				printf("Line : %d %d %d\n", i, j, k);
				A = Is(L[i], L[j]), B = Is(L[i], L[k]), C = Is(L[j], L[k]);
//				printf("Point : A(%.2lf, %.2lf) B(%.2lf, %.2lf) C(%.2lf, %.2lf)\n", A.x, A.y, B.x, B.y, C.x, C.y);
				if (A == B && A == O) { ++ans; continue; }
				f = PB(A, B), g = PB(A, C), D = Is(f, g);
//				printf("Line - f : %.2lfx + %.2lfy + %.2lf = 0, g = %.2lfx + %.2lfy + %.2lf = 0\n", f.a, f.b, f.c, g.a, g.b, g.c);
				if (Abs(Dis2(D, A) - Dis2(D, O)) < eps) ++ans;
			}
	printf("%d", ans);
}
