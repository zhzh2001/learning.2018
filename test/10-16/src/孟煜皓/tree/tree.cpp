#include <cstdio>
#include <cctype>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
int n, k;
int edge, to[N << 1], pr[N << 1], hd[N];
void addedge(int u, int v){
	to[++edge] = v, pr[edge] = hd[u], hd[u] = edge;
}
namespace task1{
	int dep[N];
	long long ans;
	void dfs(int u, int fa = 0){
		ans += (dep[u] + k - 1) / k;
		for (register int i = hd[u], v; i; i = pr[i])
			if ((v = to[i]) != fa) dep[v] = dep[u] + 1, dfs(v, u);
	}
	void Main(){
		ans = 0;
		for (register int i = 1; i <= n; ++i) dep[i] = 0, dfs(i);
		printf("%lld", ans >> 1);
	}
}
namespace task2{
	int sz[N];
	long long ans;
	void dfs(int u, int fa = 0){
		sz[u] = 1;
		for (register int i = hd[u], v; i; i = pr[i])
			if ((v = to[i]) != fa) dfs(v, u), sz[u] += sz[v], ans += sz[v] * (n - sz[v]);
	}
	void Main(){
		ans = 0, dfs(1);
		if (k == 1) printf("%lld", ans);
		else printf("%lld", (ans + (k / 2) * (n * n / 2)) / k - 4);
	}
}
int main(){
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	n = read(), k = read();
	for (register int i = 1, u, v; i < n; ++i)
		u = read(), v = read(), addedge(u, v), addedge(v, u);
	if (n <= 5000) task1 :: Main(); else task2 :: Main();
}
