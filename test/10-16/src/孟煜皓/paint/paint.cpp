#include <cstdio>
#include <cctype>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
#define INF 1000000000
int n, a[N];
int edge, to[N << 1], tw[N << 1], pr[N << 1], hd[N];
void addedge(int u, int v, int w){
	to[++edge] = v, tw[edge] = w, pr[edge] = hd[u], hd[u] = edge;
}
int sz[N], sum[N], Sz, Sum;
void dfs(int u, int m, int fa = 0, int fw = 0){
	sz[u] = 1, sum[u] = a[u];
	for (register int i = hd[u], v; i; i = pr[i])
		if ((v = to[i]) != fa) dfs(v, fw < m ? m : INF, u, tw[i]), sz[u] += sz[v], sum[u] += sum[v];
	if (fw >= m) Sz += sz[u], Sum += sum[u];
}
int check(int m){
	Sz = 0, Sum = 0, dfs(1, m);
	return n - Sz <= Sum;
}
int main(){
	freopen("paint.in", "r", stdin);
	freopen("paint.out", "w", stdout);
	n = read();
	for (register int i = 1, u, v, w; i < n; ++i)
		u = read(), v = read(), w = read(), addedge(u, v, w), addedge(v, u, w);
	for (register int i = 1; i <= n; ++i) a[i] = read();
	int l = 0, r = INF, mid, ans;
	while (l <= r)
		if (check(mid = l + r >> 1)) l = mid + 1, ans = mid; else r = mid - 1;
	printf("%d", ans);
}
