#include <iostream>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return tot*sum;}
const int N=200005;
int n,k,pos,ans,nxt[N],v[N],head[N],dep[N],fa[N],size[N];
void add(int u,int y){
	nxt[++pos]=head[u];
	v[pos]=y;
	head[u]=pos;
}
void dfs(int rt,int f){
	dep[rt]=dep[f]+1;
	fa[rt]=f;
	for(int i=head[rt];i;i=nxt[i]){
		int j=v[i];
		if(j==f)continue;
		dfs(j,rt);
	}
}
int js(int x,int y){
//	cout<<x<<" "<<y<<" ";
	int sum=0;
	while(x!=y){
		if(dep[x]<dep[y])swap(x,y);
		sum++;
		x=fa[x];
	}
	int ans=sum/k;
	if(k*ans!=sum)ans++;
//	cout<<ans<<endl;
	return ans;
}
int dfs2(int rt,int fa){
	size[rt]=1;
	for(int i=head[rt];i;i=nxt[i]){
		int j=v[i];
		if(j==fa)continue;
		size[rt]+=dfs2(j,rt);
	}
	return size[rt];
}
void dfs3(int rt,int fa){
	ans+=(size[1]-size[rt])*(size[rt]);
	for(int i=head[rt];i;i=nxt[i]){
		int j=v[i];
		if(j==fa)continue;
		dfs3(j,rt);
	}
	return ;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),k=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		add(x,y);add(y,x);
	}
	if(k==1){
		ans=0;
		dfs2(1,0); dfs3(1,0);
		cout<<ans<<endl;
		return 0;
	}
	dfs(1,0); ans=0;
//	for(int i=1;i<=n;i++)cout<<dep[i]<<" ";
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			ans+=js(i,j);
		}
	}
	cout<<ans<<endl;
	return 0;
}
/*
6 1
1 2
1 3
2 4
2 5
4 6
*/
