#include <iostream>
#include <cstdio>
#include <queue>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return tot*sum;}
const int N=20005;
int pos,jl,nxt[N],v[N],head[N],w[N],dep[N],fa[N],a[N],sum[N];
struct arr{
	int id,x,l;
	bool operator < (const arr &b) const{
		if(b.x==x)return l>b.l;
		return x<b.x;
	}
};
priority_queue<arr> q[3005];
void add(int u,int y,int ww){
	nxt[++pos]=head[u];
	v[pos]=y;w[pos]=ww;
	head[u]=pos;
}
void dfs(int rt,int f){
	dep[rt]=dep[f]+1;fa[rt]=f;
	for(int i=head[rt];i;i=nxt[i]){
		int j=v[i];
		if(j==f)continue;
		a[j]=w[i]; dfs(j,rt);
	}
}
int js(int x,int y){
	int ans=0;jl=0;
	while(x!=y){
		if(dep[x]<dep[y])swap(x,y);
		ans=max(ans,a[x]);jl++;
		x=fa[x];
	}
	ans=max(ans,a[x]);
	return ans;
}
int main()
{
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	int n=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read(),z=read();
		add(x,y,z);add(y,x,z);
	}
	for(int i=1;i<=n;i++)sum[i]=read();
	dfs(1,0);
//	for(int i=2;i<=n;i++)cout<<a[i]<<" ";puts("");
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if(j!=i){
				q[i].push((arr){j,js(i,j),jl});
            }
		}
	}
	int ans=1000000001;
	for(int i=1;i<=n;i++){
		while(!sum[q[i].top().id])q[i].pop();
		ans=min(ans,q[i].top().x);
		sum[q[i].top().id]--;
	}
	cout<<ans<<endl;
	return 0;
}
