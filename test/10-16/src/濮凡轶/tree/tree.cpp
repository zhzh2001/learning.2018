#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

typedef long long LL;

const int maxn = 100005;
const int maxk = 155;

#define get(a) (((a) + k) % k)

int n, k;

struct Edge
{
	int to, nxt;
} e[maxn << 1];

int first[maxn];

inline void add_edge(int from, int to)
{
	static int cnt = 0;
	e[++cnt].nxt = first[from];
	first[from] = cnt;
	e[cnt].to = to;
	e[++cnt].nxt = first[to];
	first[to] = cnt;
	e[cnt].to = from;
}

int dp[maxn][maxk];
int dpp[maxn][maxk];
//LL jl[maxn][maxk];
//LL tmp[maxn][maxk];
int siz[maxn];
int fa[maxn];

LL ans = 0;

inline void dfs(int now)
{
	siz[now] = 1;
	dp[now][0] = 1;
//	jl[now][0] = 1;
	for(int i = first[now]; i; i = e[i].nxt)
	{
		int to = e[i].to;
		if(to != fa[now])
		{
			fa[to] = now;
			dfs(to);
			siz[now] += siz[to];
			for(int K = 1; K < k; ++K)
			{
				dp[now][K] += dp[to][K - 1];
//				jl[now][K] += jl[to][K - 1];
			}
			dp[now][0] += dp[to][k - 1];
		}
	}
	ans += (LL) (dp[now][k - 1]) * (LL) (n - siz[now]);
}

inline void dfs_dpp(int now)
{
	if(now != 1)
	{
		for(int K = 0; K < k; ++K)
			dpp[now][K] += dp[now][K] - dp[now][get(K-2)] + dpp[fa[now]][get(K-1)];
	}
	else
	{
		for(int K = 0; K < k; ++K)
			dpp[now][K] = dp[now][K];
	}
	for(int i = first[now]; i; i = e[i].nxt)
	{
		int to = e[i].to;
		if(to != fa[now])
			dfs_dpp(to);
	}
	ans += (LL) (dpp[now][0] - dp[now][0]) * (LL) siz[now];
}

/*
inline void dfs_jl(int now)
{
	jl[now][0] = dp[now][0];
	for(int i = first[now]; i; i = e[i].nxt)
	{
		int to = e[i].to;
		if(to != fa[now])
		{
			fa[to] = now;
			dfs(to);
			for(int K = 1; K < k; ++K)
				jl[now][K] += jl[to][K - 1];
		}
	}
}

inline void dfs2(int now)
{
	if(now != 1)
	{
		tmp[now][0] = dpp[now][0];
		tmp[now][1] = jl[now][1] + dpp[fa[now]][0] - dp[now][0];
		for(int K = 2; K < k; ++K)
			tmp[now][K] += jl[now][K] - jl[now][K-2] + tmp[fa[now]][K-1];
	}
	else
	{
		for(int K = 0; K < k; ++K)
			tmp[now][K] = jl[now][K];
	}
	for(int i = first[now]; i; i = e[i].nxt)
	{
		int to = e[i].to;
		if(to != fa[now])
			dfs2(to);
	}
	for(int K = 1; K < k; ++K)
		ans += tmp[now][K];
}*/

int main()
{
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	scanf("%d%d", &n, &k);
	for(int i = 1, f, t; i < n; ++i)
	{
		scanf("%d%d", &f, &t);
		add_edge(f, t);
	}
	dfs(1);
//	cout << ans << endl;
	dfs_dpp(1);
	for(int i = 1; i <= n; ++i)
		ans -= dpp[i][0];
	ans += n * n;
	printf("%lld\n", ans >> 1);
//	for(int i = 1; i <= n; ++i)
//	{
//		cout << i << ' ';
//		for(int j = 0; j <= k; ++j)
//			cout << dpp[i][j] << ' ';
//		for(int j = 0; j <= k; ++j)
//			cout << dp[i][j] << ' ';
////		cout << siz[i];
//		cout << endl;
//	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
