#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;

typedef long long LL;

const int maxn = 100005;

int n;

struct Edge2
{
	int from, to, dist, id;

	inline bool operator < (const Edge2& other) const
	{
		return this->dist > other.dist;
	}
} E[maxn];

struct Edge
{
	int to, nxt, dist;
	bool flag;
} e[maxn << 1];

int first[maxn];

inline void add_edge(Edge2& ee)
{
	static int cnt = 0;
	ee.id = ++cnt;
	e[cnt].nxt = first[ee.from];
	e[cnt].flag = false;
	first[ee.from] = cnt;
	e[cnt].to = ee.to;
	e[cnt].dist = ee.dist;
	e[++cnt].nxt = first[ee.to];
	e[cnt].flag = false;
	first[ee.to] = cnt;
	e[cnt].to = ee.from;
	e[cnt].dist = ee.dist;
}

int siz[maxn];
int xx[maxn];
LL sum_all;
LL sumx[maxn];
bool fff[maxn];

inline bool dfs(int now, int fa)
{
//	cout << "now = " << now << endl;
	sumx[now] = xx[now];
	siz[now] = 1;
	for(int i = first[now]; i; i = e[i].nxt)
	{
		int to = e[i].to;
		if(to != fa)
		{
//			cout << "e = " << i << ' ' << e[i].flag << endl;
			if(e[i].flag)
				fff[to] = true;
//			cout << to << ' '<< now << ' ' << dfs(to, now) << endl;
			if(!dfs(to, now))
			{
//				cout << "to = " << to << endl;
//				puts("using namespace std;");
				return false;
			}
			if(!e[i].flag)
			{
				siz[now] += siz[to];
				sumx[now] += sumx[to];
			}
		}
	}
	if(fff[now])
	{
		if(sum_all - sumx[now] < ((LL) siz[now] << 1))
//		{
//			puts("haha");
			return false;
//		}
		else
			return true;
	}
	else
		return true;
}

int main()
{
	freopen("paint.in", "r", stdin);
	freopen("paint.out", "w", stdout);
	scanf("%d", &n);
	for(int i = 1; i < n; ++i)
	{
		scanf("%d%d%d", &E[i].from, &E[i].to, &E[i].dist);
		add_edge(E[i]);
	}
	sort(E + 1, E + n);
	for(int i = 1; i <= n; ++i)
	{
		scanf("%d", &xx[i]);
		sum_all += xx[i];
	}
	int l = 1, r = n - 1;
	while(l < r)
	{
		memset(fff, 0, sizeof(fff));
		fff[1] = true;
		int mid = (l + r) >> 1;
		for(int i = 1; i < n; ++i)
			e[E[i].id].flag = e[E[i].id + 1].flag = (i <= mid);
//		cout << dfs(1, 0) << endl;
//		for(int i = 1; i <= (n - 1) * 2; ++i)
//			cout << e[i].to << ' ' << e[i].dist << ' ' << e[i].flag << endl;
		if(dfs(1, 0))
		{
			r = mid;
//			puts("sxdaking");
		}
		else
			l = mid + 1;
//		cout << "ef now\n";
//		cout << mid << endl;
//		cout << endl;
//		for(int i = 1; i <= n; ++i)
//			cout << siz[i] << ' ';
//		cout << endl;
//		cout << "haha ";
//		cout << l << ' ' << r << endl;
	}
	printf("%d", E[r].dist);
//	int mid = r;
//	memset(fff, 0, sizeof(fff));
//	for(int i = 1; i < n; ++i)
//		e[E[i].id].flag = e[E[i].id + 1].flag = (i <= mid);
//	cout << endl << dfs(1, 0) << endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
}
