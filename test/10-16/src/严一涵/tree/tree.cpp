#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
#include <vector>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,k,f[100003][150],c[100003],cf[150];
vector<int>e[100003];
LL p[100003],ans;
void dfs(int u,int fa){
	c[u]=1;
	for(int i=0;i<k;i++)f[u][i]=0;
	f[u][0]=1;
	p[u]=0;
	for(vector<int>::iterator i=e[u].begin();i!=e[u].end();i++){
		if(*i==fa)continue;
		dfs(*i,u);
		c[u]+=c[*i];
		p[u]+=p[*i]+c[*i];
		for(int j=0;j<k;j++)f[u][(j+1)%k]+=f[*i][j];
	}
}
void gans(int u,int fa){
	if(fa!=-1){
		p[u]=p[fa]+n-c[u]-c[u];
		for(int i=0;i<k;i++)cf[(i+2)%k]=f[fa][(i+1)%k]-f[u][i];
		for(int i=0;i<k;i++)f[u][i]+=cf[i];
	}
	LL x=p[u];
	for(int i=1;i<k;i++){
		x-=f[u][i]*i;
		ans+=f[u][i];
	}
	ans+=x/k;
	for(vector<int>::iterator i=e[u].begin();i!=e[u].end();i++){
		if(*i!=fa)gans(*i,u);
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	k=read();
	for(int i=1;i<n;i++){
		static int x,y;
		x=read();
		y=read();
		e[x].push_back(y);
		e[y].push_back(x);
	}
	dfs(1,-1);
	ans=0;
	gans(1,-1);
	printf("%lld\n",ans/2);
	return 0;
}
