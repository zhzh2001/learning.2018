#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,f[100003],b[100003];
LL c[100003],tc=0;
struct edge{int a,b,c;}a[100003];
void init(){
	for(int i=1;i<=n;i++){
		f[i]=i;
		c[i]=b[i]+1;
	}
}
int getf(int u){return f[u]==u?u:(f[u]=getf(f[u]));}
void add(int a,int b){
	a=getf(a);
	b=getf(b);
	if(a==b)return;
	f[a]=b;
	c[b]+=c[a];
}
bool ck(int mid){
	init();
	for(int i=1;i<n;i++){
		if(a[i].c<mid){
			add(a[i].a,a[i].b);
		}
	}
	for(int i=1;i<=n;i++)if(f[i]==i){
		if(c[i]>tc)return 0;
	}
	return 1;
}
int bs(int l,int r){
	int mid;
	while(l<r){
		mid=l+(r-l+1)/2;
		if(ck(mid))l=mid;
		else r=mid-1;
	}
	return l;
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		a[i].a=read();
		a[i].b=read();
		a[i].c=read();
	}
	for(int i=1;i<=n;i++){
		b[i]=read();
		tc+=b[i];
	}
	printf("%d\n",bs(0,1000000000));
	return 0;
}
