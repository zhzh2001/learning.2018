#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
using namespace std;
struct Point{
	double a,b;
	Point(){
		a=0;
		b=0;
	}
	Point(double _a,double _b){
		a=_a;
		b=_b;
	}
	Point getMid(Point t){
		return Point((a+t.a)/2,(b+t.b)/2);
	}
	double getDis(Point t){
		return sqrt((a-t.a)*(a-t.a)+(b-t.b)*(b-t.b));
	}
};
class Line{
public:
	double a,b,c;
	Line(){
		a=1;
		b=0;
		c=0;
	}
	Line(double _a,double _b,double _c){
		a=_a;
		b=_b;
		c=_c;
	}
	Line(Line t,Point x){
		a=t.b;
		b=-t.a;
		c=-x.a*a-x.b*b;
	}
	void read(){
		scanf("%lf%lf%lf",&a,&b,&c);
	}
	Point ge(Line x){
		return Point((b*x.c-x.b*c)/(a*x.b-x.a*b),(a*x.c-x.a*c)/(x.a*b-a*x.b));
	}
};
Line a[3003],b,c;
Point w,x,y,z;
int n;
long long ans;
int main(){
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		a[i].read();
	}
	ans=0;
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			for(int k=j+1;k<=n;k++){
				x=a[i].ge(a[j]);
				y=a[i].ge(a[k]);
				z=a[j].ge(a[k]);
				b=Line(a[i],x.getMid(y));
				c=Line(a[k],y.getMid(z));
				w=b.ge(c);
				if(abs(w.getDis(Point())-w.getDis(x))<1e-9){
					ans++;
				}
			}
		}
	}
	printf("%lld\n",ans);
	return 0;
}
