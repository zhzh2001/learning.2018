#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e5+5;
struct edge{
	int u,v,w;
}e[N<<1];
int n,m,c[N],Sum;
inline void init(){
	n=read();
	for (int i=1;i<n;i++){
		e[i]=(edge){read(),read(),read()};
	}
	for (int i=1;i<=n;i++){
		c[i]=read(); Sum+=c[i];
	}
}
inline bool cmp(edge A,edge B){
	return A.w<B.w;
}
int tmp,fa[N],sz[N],sum[N];
int getfa(int x){
	return (fa[x]==x)?x:fa[x]=getfa(fa[x]);
}
inline void Union(int u,int v){
	int p=getfa(u),q=getfa(v);
	if (p!=q){
		fa[p]=q; sz[q]+=sz[p];
	}
}
inline bool judge(int mid){
	for (int i=1;i<=n;i++) fa[i]=i,sz[i]=c[i];
	memset(sum,0,sizeof sum);
	tmp=0;
	for (int i=1;i<=m;i++){
		if (e[i].w<mid){
			tmp=i;
			Union(e[i].u,e[i].v);
		}else{
			break;
		}
	}
	for (int i=1;i<=n;i++) {
		fa[i]=getfa(i);
		sum[fa[i]]++;
	}
	for (int i=1;i<=n;i++){
		if (sum[i]){
			int other=Sum-sz[i];
			if (other<sum[i]) {
				return 0;
			}
		}
	}
	return 1;
}
inline void solve(){
	m=n-1;
	sort(e+1,e+m+1,cmp);
	int l=1,r=1e9;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (judge(mid)) l=mid;
			else r=mid-1;
	} 
	writeln(l);
}
int main(){
	freopen("paint.in","r",stdin); freopen("paint.out","w",stdout);
	init(); solve();
	return 0;
}
