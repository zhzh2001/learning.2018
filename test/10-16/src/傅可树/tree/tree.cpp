#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e5+5,K=155,INF=1e9;
struct edge{
	int link,next;
}e[N<<1];
int n,k,tot,head[N];
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot; 
}
inline void insert(int u,int v){
	add_edge(u,v); add_edge(v,u);
}
inline void init(){
	n=read(); k=read();
	for (int i=1;i<n;i++){
		insert(read(),read());
	}
}
bool mark[N];
int Fa[N],root,sz[N],mx,all;
void getroot(int u,int fa){
	sz[u]=1; int tmp=0; Fa[u]=fa;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa&&!mark[v]){
			getroot(v,u); sz[u]+=sz[v];
			tmp=max(tmp,sz[v]);
		}
	}
	tmp=max(tmp,all-sz[u]);
	if (tmp<mx) mx=tmp,root=u;
}
int dis[N],g[K*2],mxdeep,size,siz,S,ss,sum[2*K],ans,ton[2*K];
void dfs(int u,int fa){
	dis[u]=dis[fa]+1; int p=(dis[u]-1)/k+1; ans+=p+S+(dis[u]/k)*(size-1); ss+=dis[u]/k; siz++;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa&&!mark[v]){
			dfs(v,u);
		}
	}
	int tmp=dis[u]%k; ton[tmp]++;
}
inline void calc(int u){
	dis[u]=0; size=1; ss=S=0;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (!mark[v]){
			siz=0; ss=S; dfs(v,u); S=ss; sum[0]=g[0]; size+=siz;
			if (k==1) continue;
			for (int j=1;j<2*k;j++) sum[j]=sum[j-1]+g[j];
			for (int j=0;j<k;j++) {
				ans+=2*ton[j]*(sum[2*k-1-j]-sum[k-j])+(sum[k-j]-sum[0]);
			}
			for (int j=0;j<k;j++) g[j]+=ton[j];
			for (int j=0;j<k;j++) ton[j]=0;
		}
	}
	for (int j=0;j<k;j++) g[j]=0;
}
void divide(int u){
	mark[u]=1;
	calc(u);
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (!mark[v]){
			if (Fa[u]==v) all=sz[v]-sz[u];
				else all=sz[v];
			root=0; mx=INF; getroot(u,0);
			divide(root);
		}
	}
}
inline void solve(){
	root=0; mx=INF; all=n; getroot(1,0);
	divide(root); writeln(ans);
} 
int main(){
	freopen("tree.in","r",stdin); freopen("tree.out","w",stdout);
	init(); solve();
	return 0;
}
