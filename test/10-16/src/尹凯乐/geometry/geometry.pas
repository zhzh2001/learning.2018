program geometry;
 type
  wxy=record
   x,y:double;
  end;
 var
  x,y,z:array[0..3001] of longint;
  a,b,c,d,e:wxy;
  i,j,k,n,ssum:longint;
 function hahapina(l,r:longint):wxy;
  var
   oc,pc,qc,rc:longint;
  begin
   hahapina.x:=0;
   hahapina.y:=0;
   oc:=y[l]*x[r];
   pc:=y[r]*x[l];
   qc:=z[l]*x[r];
   rc:=z[r]*x[l];
   //if oc-pc<>0 then
    //begin
   hahapina.y:=(rc-qc)/(oc-pc);
   if x[l]<>0 then hahapina.x:=(-z[l]-hahapina.y*y[l])/x[l]
              else hahapina.x:=(-z[r]-hahapina.y*y[r])/x[r];
    //end;
  end;
 function hahapat(l,r:longint):boolean;
  var
   o,p:longint;
  begin
   o:=y[l]*x[r];
   p:=y[r]*x[l];
   if o<>p then exit(true);
   exit(false);
  end;
 function hahaclc(x,y,z:wxy):wxy;
  var
   x1,x2,y1,y2,z1,z2:double;
  begin
   x1:=x.x-y.x;
   y1:=x.y-y.y;
   z1:=sqr(x.x)-sqr(y.x)+sqr(x.y)-sqr(y.y);
   x2:=z.x-y.x;
   y2:=z.y-y.y;
   z2:=sqr(z.x)-sqr(y.x)+sqr(z.y)-sqr(y.y);
   z1:=z1/2;
   z2:=z2/2;
   hahaclc.x:=(z1*y2-z2*y1)/(x1*y2-x2*y1);
   hahaclc.y:=(x1*z2-x2*z1)/(x1*y2-x2*y1);
  end;
 function hahamyh(x,y:wxy):double;
  begin
   exit(sqrt(sqr(x.x-y.x)+sqr(x.y-y.y)));
  end;
 begin
  assign(input,'geometry.in');
  assign(output,'geometry.out');
  reset(input);
  rewrite(output);
  readln(N);
  for i:=1 to n do
   readln(x[i],y[i],z[i]);
  e.x:=0;
  e.y:=0;
  ssum:=0;
  for i:=1 to n-2 do
   for j:=i+1 to n-1 do
    for k:=j+1 to n do
     if hahapat(i,j) and hahapat(i,k) and hahapat(j,k) then
      begin
       a:=hahapina(i,j);
       b:=hahapina(i,k);
       c:=hahapina(j,k);
       d:=hahaclc(a,b,c);
       if trunc(hahamyh(a,d)*10000)=trunc(hahamyh(e,d)*10000) then inc(ssum);
      end;
  writeln(ssum);
  close(input);
  close(output);
 end.
