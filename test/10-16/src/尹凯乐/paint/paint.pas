program paint;
 var
  a,b,c,t,x,y,z:array[0..100001] of longint;
  i,m,n,o,p,q,upass:longint;
  ssum:int64;
 function hahafind(k:longint):longint;
  begin
   if t[k]<>k then t[k]:=hahafind(t[k]);
   exit(t[k]);
  end;
 begin
  assign(input,'paint.in');
  assign(output,'paint.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n-1 do
   readln(x[i],y[i],z[i]);
  for i:=1 to n do
   read(a[i]);
  readln;
  upass:=0;
  for i:=1 to n do
   inc(ssum,a[i]);
  o:=0;
  p:=1008208820;
  //o:=2;
  //p:=2;
  while o<=p do
   begin
    m:=(o+p)>>1;
    for i:=1 to n do
     t[i]:=i;
    for i:=1 to n-1 do
     if z[i]<m then t[hahafind(x[i])]:=hahafind(y[i]);
    upass:=0;
    for i:=1 to n do
     begin
      b[i]:=0;
      c[i]:=0;
     end;
    for i:=1 to n do
     begin
      q:=hahafind(i);
      t[i]:=q;
     end;
    for i:=1 to n do
     begin
      inc(b[t[i]],a[i]);
      inc(c[t[i]]);
     end;
    for i:=1 to n do
     if (t[i]=i) and (ssum-b[i]<c[i]) then upass:=1;
    if upass=1 then p:=m-1
               else o:=m+1;
   end;
  writeln(p);
  close(input);
  close(output);
 end.
