program tree;
 type
  wb=record
   n:^wb;
   t:longint;
  end;
  wwb=^wb;
 var
  b:array[0..200001] of wb;
  h,f,q:array[0..100001] of longint;
  d:array[0..100001,0..151] of longint;
  p,i,k,m,n,x,y,root:longint;
  ssum:int64;
 procedure hahainc(x,y:longint);
  begin
   inc(p);
   b[p].n:=@b[h[x]];
   b[p].t:=y;
   h[x]:=p;
  end;
 procedure hahaDFS(x,y:longint);
  var
   i,j,o,p:longint;
   jc:wwb;
  begin
   jc:=@b[h[x]];
   inc(q[x]);
   inc(d[x,0]);
   while jc<>@b[0] do
    begin
     i:=jc^.t;
     jc:=jc^.n;
     if i=y then continue;
     hahaDFS(i,x);
     inc(ssum,int64(f[i])*q[x]+q[i]*int64(f[x]));
     inc(f[x],f[i]);
     inc(ssum,int64(q[i])*int64(q[x])-int64(d[i,0])*int64(d[x,0]));
     //o:=q[x]-d[x,0];
     o:=q[x];
     p:=0;
     for j:=0 to k-1 do
      begin
       //inc(p,d[i,j]);
       if k-j+1<=k-1 then p:=d[i,k-j+1];
       inc(ssum,o*p);
       dec(o,d[x,j]);
      end;
     inc(q[x],q[i]);
     for j:=0 to k-1 do
      inc(d[x,j],d[i,j]);
    end;
   d[x,k]:=0;
   for j:=k downto 1 do
    d[x,j]:=d[x,j-1];
   d[x,0]:=0;
   inc(f[x],d[x,k]);
   inc(d[x,0],d[x,k]);
   dec(d[x,k],d[x,k]);
  end;
 begin
  assign(input,'tree.in');
  assign(output,'tree.out');
  reset(input);
  rewrite(output);
  //randomize;
  readln(n,k);
  for i:=1 to n-1 do
   begin
    readln(x,y);
    hahainc(x,y);
    hahainc(y,x);
   end;
  ssum:=0;
  filldword(f,sizeof(f)>>2,0);
  filldword(d,sizeof(d)>>2,0);
  filldword(q,sizeof(q)>>2,0);
  //root:=random(n)+1;
  root:=n>>2+1;
  //root:=1;
  hahaDFS(root,0);
  writeln(ssum);
  close(input);
  close(output);
 end.
