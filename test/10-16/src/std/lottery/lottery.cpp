#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define int long long
const int N=5e5+7,inf=1e9;
int n,m,cnt,ans;
int a[N],b[N],c[N],tr[N];
int lowbit(int x) {return x&-x;}
int query(int x) {
	int sum=0;
	for (;x;x-=lowbit(x)) sum+=tr[x];
	return sum;
}
void add(int x) {
	for (;x<=cnt;x+=lowbit(x)) tr[x]++;
}
bool check(int x) {
	int sum=0;
	memset(tr,0,sizeof tr);
	add(lower_bound(b+1,b+cnt+1,0)-b);
	for (int i=1;i<=n;i++) {
		if (a[i]-x<b[1]);
		else if (a[i]-x>b[cnt]) sum+=query(cnt);
		else {
			int pos=lower_bound(b+1,b+cnt+1,a[i]-x)-b;
			if (b[pos]>a[i]-x) pos--;
			sum+=query(pos);
		}
		add(c[i]);
	}
	if (sum>=m) return 1;
	else return 0;
}
signed main() {
	freopen("lottery.in","r",stdin);
	freopen("lottery.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	for (int i=1;i<=n;i++) scanf("%lld",&a[i]);
	for (int i=1;i<=n;i++) a[i]=a[i-1]+a[i],b[i]=a[i];
	b[n+1]=0;
	sort(b+1,b+n+2);
	cnt=unique(b+1,b+n+2)-b-1;
	for (int i=1;i<=n;i++) c[i]=lower_bound(b+1,b+cnt+1,a[i])-b;
	int l=-inf*n,r=inf*n;
	while (l<r) {
		int mid=(l+r+1)>>1;
		if (check(mid)) l=mid;
		else r=mid-1;
	}
	printf("%lld\n",l);
}
