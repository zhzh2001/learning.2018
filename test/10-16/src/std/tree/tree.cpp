#include <cstdio>
using namespace std;
#define ll long long
const int N=1e5+7;
struct edge {
	int t,nxt;
}e[N<<1];
int n,K,cnt;
ll ans;
int head[N];
int f[N][155],g[N][155];
ll s[N],w[N];
void add(int u,int t) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;
}
void dfs(int u,int fa) {
	f[u][0]=1;
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==fa) continue;
		dfs(t,u);
		s[u]+=s[t]+f[t][0];
		for (int j=0;j<K-1;j++) f[u][j+1]+=f[t][j];
		f[u][0]+=f[t][K-1];
	}
	ans+=s[u];
}
void dfs2(int u,int fa) {
	if (u!=1) {
		w[u]=w[fa]+g[fa][0];
		for (int j=0;j<K-1;j++) g[u][j+1]+=g[fa][j];
		g[u][0]+=g[fa][K-1];
		ans+=w[u];
	}
	w[u]+=s[u];
	for (int i=0;i<K;i++) g[u][i]+=f[u][i];
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==fa) continue;
		w[u]-=s[t]+f[t][0];
		for (int j=0;j<K-1;j++) g[u][j+1]-=f[t][j];
		g[u][0]-=f[t][K-1];
		dfs2(t,u);
		w[u]+=s[t]+f[t][0];
		for (int j=0;j<K-1;j++) g[u][j+1]+=f[t][j];
		g[u][0]+=f[t][K-1];
	}
}
signed main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d%d",&n,&K);
	for (int i=1,u,v;i<n;i++) {
		scanf("%d%d",&u,&v);
		add(u,v);add(v,u);
	}
	dfs(1,0);
	dfs2(1,0);
	printf("%lld\n",ans/2);
}
