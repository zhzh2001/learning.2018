#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;
#define dd c=getchar()
inline int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
const int N=1e5+7;
struct edge {
	int u,t,w;
	bool operator <(edge a) const {
		return w<a.w;
	}
}e[N];
int n,sum,ans,can;
int X[N],siz[N],fa[N];
int find(int x) {return x==fa[x]?x:fa[x]=find(fa[x]);}
void merge(int a,int b) {
	fa[b]=a;
	siz[a]+=siz[b];
	X[a]+=X[b];
	if (siz[a]>sum-X[a]) can=1;
}
int main() {
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	if (n==1) return puts("0"),0;
	for (int i=1;i<n;i++) e[i].u=read(),e[i].t=read(),e[i].w=read();
	sort(e+1,e+n);
	for (int i=1;i<=n;i++) X[i]=read(),sum+=X[i];
	for (int i=1;i<=n;i++) fa[i]=i,siz[i]=1;
	for (int i=1;i<n;i++) {
		if (can) break;
		ans=e[i].w;
		int u=find(e[i].u),t=find(e[i].t);
		merge(u,t);
	}
	printf("%d\n",ans);
}
