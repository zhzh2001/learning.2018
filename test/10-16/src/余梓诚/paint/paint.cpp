#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

inline char read(){
    static const int IN_LEN=1000000;
    static char buf[IN_LEN],*s,*t;
    return (s==t?t=(s=buf)+fread(buf,1,IN_LEN,stdin),(s==t?-1:*s++):*s++);
}
template<class T>
inline void read(T &x){
    static bool iosig;
    static char c;
    for (iosig=false,c=read();!isdigit(c);c=read()){
        if (c=='-') iosig=true;
        if (c==-1) return;
    }
    for (x=0;isdigit(c);c=read()) x=((x+(x<<2))<<1)+(c^'0');
    if (iosig) x=-x;
}
const int OUT_LEN=10000000;
char obuf[OUT_LEN],*ooh=obuf;
inline void print(char c){
    if (ooh==obuf+OUT_LEN) fwrite(obuf,1,OUT_LEN,stdout),ooh=obuf;
    *ooh++=c;
}
template<class T>
inline void print(T x){
    static int buf[30],cnt;
    if (x==0) print('0');
    else{
        if (x<0) print('-'),x=-x;
        for (cnt=0;x;x/=10) buf[++cnt]=x%10+48;
        while (cnt) print((char)buf[cnt--]);
    }
}
inline void flush(){
	fwrite(obuf,1,ooh-obuf,stdout);
}

void judge(){
    freopen("paint.in","r",stdin);
    freopen("paint.out","w",stdout);
}

const int N=110000,inf=1<<30;
int n,ans,cnt,size[N],sum[N],t[N],l=inf,r=-inf;
bool used[N];
struct edge{
	int to,nxt,w;
}e[N<<1];
int head[N],opt;
void add(int x,int y,int z){
	e[++opt].to=y; e[opt].nxt=head[x]; e[opt].w=z; head[x]=opt;
}

void dfs(int u,int fa,bool flag,int num){
	if (flag) size[u]=1,sum[u]=t[u];
	for (int i=head[u];i;i=e[i].nxt){
		int k=e[i].to,w=e[i].w;
		if (k==fa) continue;
		if (w>=num) dfs(k,u,1,num);
		else dfs(k,u,flag,num);
		if (w>=num||flag) size[u]+=size[k],sum[u]+=sum[k];
		if (w>=num&&!flag) used[u]=1;
	}
}

bool check(int x){
	memset(used,0,sizeof(used));
	memset(sum,0,sizeof(sum));
	memset(size,0,sizeof(size));
	dfs(1,0,0,x);
	int t1=0,t2=0;
	for (int i=1;i<=n;i++){
		if (used[i]) t1+=size[i],t2+=sum[i];
	}
	return n-t1<=t2;
}

int main(){
	judge();
	read(n);
	for (int i=1,x,y,z;i<n;i++) read(x),read(y),read(z),add(x,y,z),add(y,x,z),l=min(l,z),r=max(r,z);
	for (int i=1;i<=n;i++) read(t[i]);
	while (l<=r){
		int mid=l+r>>1;
		if (check(mid)) l=mid+1,ans=mid;
		else r=mid-1;
	}
	print(ans),print('\n');
	return flush(),0;
}
