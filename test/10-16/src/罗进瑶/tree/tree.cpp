#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w",stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();
}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x);puts("");}
const int N = 300000;
int n, k;
int ver[N], nxt[N], en, head[N];
void add(int x, int y) {
	ver[++en] = y, nxt[en] = head[x], head[x] = en;
}
ll ans, siz[N];
int f[N][25], d[N];
namespace F1 {
	void dfs(int x, int F) {
		siz[x] = 1;
		for(int i = head[x]; i;i = nxt[i]) {
			int y = ver[i];
			if(y == F) continue;
			dfs(y, x);
			ans += siz[y] * (n - siz[y]);
			siz[x] += siz[y];
		}
	}
	void main() {
		dfs(1, 0);
		writeln(ans);
	}
}
namespace F2{
	void dfs(int x, int F) {
		d[x] = d[F] + 1;
		f[x][0] = F;
		for(int i = 1; i <= 20; ++i)
			f[x][i] = f[f[x][i - 1]][i - 1];
		for(int i = head[x]; i;i = nxt[i]) {
			int y = ver[i];
			if(y == F) continue;
			dfs(y, x);
		}
	}
	int lca(int x, int y) {
		if(d[y] < d[x]) swap(x, y);
		for(int i = 20; i >= 0; --i)
			if(d[f[y][i]] >= d[x]) y = f[y][i];
		if(y == x) return x;
		for(int i = 20; i >= 0; --i)
			if(f[x][i] != f[y][i]) x = f[x][i], y = f[y][i];
		return f[x][0];
	}
	void main() {
		dfs(1, 1);
		for(int i = 1; i <= n; ++i)
			for(int j = i + 1; j <= n; ++j)
				{
					int z = lca(i, j);
					ans += (int)ceil((d[i] + d[j] - 2 * d[z]) / (double)k);
				}
		writeln(ans);
	}
} 
int main() {
	setIO();
	int x, y, z;
	n = read(), k = read();
	for(int i = 1; i < n; ++i) {
		x = read(), y = read();
		add(x, y);
		add(y, x);
	}
	if(k == 1) F1::main();
	else if( n <= 3000)
		F2::main();
	else puts("");
	return 0;
}
