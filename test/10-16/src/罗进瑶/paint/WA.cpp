#include<bits/stdc++.h>
#define pii pair<int,int>
#define mk make_pair
#define fi first
#define nd second
typedef long long ll;
using namespace std;
void setIO() {
	freopen("paint.in", "r", stdin);
	freopen("paint.out", "w",stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();
}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x);puts("");}
const int N = 300000;
int n, b[N];
struct Edge {
	int u, v, w, nxt;
}e[N];
int en, head[N];
void add(int x, int y, int z) {
	e[++en].u = x, e[en].v = y, e[en].nxt = head[x], head[x] = en, e[en].w = z;
}
#define lch (o<<1)
#define rch (o<<1|1)
int w[N], L[N], R[N], num;
struct T1 {
	int ls[N * 4], rs[N * 4];
	pii v[N * 4];
	inline void up(int o) {
		if(v[lch].fi > v[rch].fi)
			v[o] = v[lch];
		else
			v[o] = v[rch];
	}
	void build(int o, int l, int r) {
		ls[o] = l, rs[o] = r;
		if(l == r) {
			v[o].fi = w[l];
			v[o].nd = l;
			return;
		}
		int mid = (l + r) >> 1;
		if(l <= mid) build(lch, l, mid);
		if(r > mid) build(rch, mid + 1, r);
		up(o);
	}
	inline pii merge(pii x, pii y) {
		if(x.fi > y.fi) return x;
		else return y;
	}
	pii ask(int o, int l, int r) {
		if(l > r) return mk(0, 0);
		if(l <= ls[o] && r >= rs[o]) return v[o];
		int mid = (ls[o] + rs[o]) >> 1;
		if(r <= mid) return ask(lch, l, r);
		if(l > mid) return ask(rch, l, r);
		return merge(ask(lch, l, r), ask(rch, l, r));
	}
	void change(int o, int x, int value) {
		if(ls[o] == rs[o]) {
			v[o].fi -= value;
			return;
		}
		int mid = (ls[o] + rs[o]) >> 1;
		if(x <= mid) change(lch, x, value);
		else change(rch, x, value);
		up(o);
	}
}t1;
struct T2 {
	int ls[N * 4], rs[N * 4], sum[N * 4], make[N * 4];
	inline void up(int o) {
		sum[o] = sum[lch] + sum[rch];
	}
	void down(int o) {
		if(make[o]) {
			sum[lch] = sum[rch] = 0;
			make[lch] = make[rch] = 1;
			make[o] = 0;
		}
	}
	void build(int o, int l, int r) {
		ls[o] = l, rs[o] = r;make[o] = 0;
		if(l == r) {
			sum[o] = 1;
			return;
		}
		int mid = (l + r) >> 1;
		if(l <= mid) build(lch, l, mid);
		if(r > mid) build(rch, mid + 1, r);
		up(o);
	}
	int ask(int o, int l, int r) {
		if(l > r) return 0;
		if(l <= ls[o] && r >= rs[o]) return sum[o];
		down(o);
		int mid = (ls[o] + rs[o]) >> 1;
		if(r <= mid) return ask(lch, l, r);
		if(l > mid) return ask(rch, l, r);
		return ask(lch, l, r) + ask(rch, l, r);
	}
	void change(int o, int l, int r) {
		if(l <= ls[o] && r >= rs[o]) {
			sum[o] = 0;
			make[o] = 1;
			return;
		}
		down(o);
		int mid = (ls[o] + rs[o]) >> 1;
		if(l <= mid) change(lch, l, r);
		if(r > mid)change(rch, l, r);
		up(o);
	}	
}t2;
int mid;
bool fg ;
void solve(int x, int F) {
	int z;
	if(fg) return;
	for(int i = head[x]; i && !fg; i = e[i].nxt) {
		int y = e[i].v;
		solve(y, x);
		if(e[i].w >= mid) {
			int t = t2.ask(1, L[y], R[y]);
			if(!t) continue;
			pii now = t1.merge(t1.ask(1, 1, L[y] - 1), t1.ask(1, R[y] + 1, n));
			while(t > 0 && now.fi > 0) {
				z = min(t, now.fi);
				t -= z;
				t1.change(1, now.nd, -z);
				if(t == 0) continue;
			}
			if(t > 0) {
				fg = 1;
				break;
			}
			t2.change(1, L[y], R[y]);
			
		}
	}
}

bool check() {
	t1.build(1, 1, n);
	t2.build(1, 1, n);
	fg = 0;
	solve(1, 0);
	printf("fg = %d %d\n", fg, t2.ask(1, 1, n));
	return !fg && t2.ask(1, 1, n) == 0;
}
void dfs(int x, int F) {
	L[x] = ++num;
	w[num] = b[x];
	for(int i = head[x]; i;i = e[i].nxt) {
		int y = e[i].v;
		if(y==F) continue;
		dfs(y,x);
	}
	R[x] = num;
}
int main() {
	int x, y, z;
	n = read();
	for(int i = 1; i < n; ++i) {
		x = read(), y = read(), z = read();
		add(x, y, z);
	}
	for(int i = 1; i <= n; ++i)
		b[i] = read();
	dfs(1, 0);
	for(int i = 1; i <= n; ++i)
		printf("%d:%d\n", i, b[i]);
	mid = 2;
/*	int r = 1e9 + 3, l = 0;
	while(r - l > 1) {
		 mid = (l + r) >> 1;
		if(check(mid)) l = mid;
		else r = mid;
	}
	writeln(l);*/
	writeln(check());
	return 0;
}
