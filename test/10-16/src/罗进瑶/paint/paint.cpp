#include<bits/stdc++.h>
#define pii pair<int,int>
#define mk make_pair
#define fi first
#define nd second
typedef long long ll;
using namespace std;
void setIO() {
	freopen("paint.in", "r", stdin);
	freopen("paint.out", "w",stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();
}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x);puts("");}
const int N = 300000;
int n, b[N];
struct Edge {
	int u, v, w, nxt;
}e[N];
int en, head[N], d[N], ind[N];
void add(int x, int y, int z) {
	e[++en].u = x, e[en].v = y, e[en].nxt = head[x], head[x] = en, e[en].w = z;
}
int mid, f[N][25], g[N][25];
void dfs(int x, int F) {
	d[x] = d[F] + 1;
	f[x][0] = F;
	for(int i = 1; i <= 20; ++i)
		f[x][i] = f[f[x][i - 1]][i - 1];
	for(int i = 1; i <= 20; ++i)
		g[x][i] = max(g[x][i - 1], g[f[x][i - 1]][i - 1]);
	for(int i = head[x]; i;i = e[i].nxt) {
		int y = e[i].v;
		if(y == F) continue;
		g[y][0] = e[i].w;
		dfs(y, x);
	}	
}	
int lca(int x, int y) {
	if(d[y] < d[x]) swap(x, y);
	for(int i = 20; i >= 0; --i)
		if(d[f[y][i]] >= d[x]) y = f[y][i];
	if(y == x) return x;
	for(int i = 20; i >= 0; --i)
		if(f[x][i] != f[y][i]) x = f[x][i], y = f[y][i];
	return f[x][0];
}
int dis(int x, int y) {
	if(x == y) return 0;
	int res = 0;
	for(int i = 20; i >= 0; --i)
		if(f[x][i] != y) {
			res = max(res, g[x][i]);
			x = f[x][i];
		}
	return max(res, g[x][0]);
}
int w[N];
bool check() {
	for(int i = 1; i <= n; ++i) w[i] = b[i];
	for(int i = 1;  i<= n; ++i) {
		bool fg = 0;
		for(int j = 1; j <= n; ++j)
			if(w[j] > 0 && j != i) {
				int z = lca(i, j);
				int t1 = dis(i, z), t2 = dis(j, z);
				int t3 = max(t1, t2);
				if(t3 >= mid) {
					--w[j];
					fg = 1;
					break;
				}
			}
		if(!fg) 
			return 0;
	}
	return 1;
}
int main() {
	setIO();
	srand(time(NULL));
	int x, y, z;
	n = read();
	for(int i = 1; i < n; ++i) {
		x = read(), y = read(), z = read();
		add(x, y, z);
		add(y, x, z);
	}
	for(int i = 1; i <= n; ++i)
		b[i] = read();
	dfs(1, 1);
	int r = 2 * n - 1, l = 1;
	while(r - l > 1) {
		int tmp = (l+r)>>1;
		mid = e[tmp].w;
		if(check()) l = tmp;
		else r = tmp;
	}
	writeln(e[l].w);
	return 0;
}
