#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("geometry.in", "r", stdin);
	freopen("geometry.out", "w",stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();
}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
void writeln(ll x) {write(x);puts("");}
const int N = 510;
int n;
struct P {
	double a, b;
}t[N];
struct T {
	double x, y;
};
T get(P a, P b) {
	T now;
	now.x = (b.b - a.b) / (a.a - b.a);
	now.y = a.a * now.x + a.b;
	return now;
}
#define sqr(x) ((x)*(x))
int ans;
int main() {
	setIO();
	n = read();
	double x, y, z;
	for(int i = 1; i <= n; ++i) {
		scanf("%lf%lf%lf",&x,&y,&z);
		t[i].a = -x / y;
		t[i].b = -z / y;
	}
	 {
		puts("0");
		return 0;
	}
	for(int i = 1; i <= n; ++i)
		printf("%d: %.2lfx+%.2lf\n", i, t[i].a, t[i].b);

	for(int i = 1;  i<= n; ++i)
		for(int j = i + 1; j <= n; ++j)
			for(int k = j+1; k <= n; ++k) {
				if(t[i].a == t[j].a || t[i].a == t[k].a || t[j].a == t[k].a) continue;
				T t1 = get(t[i], t[j]);
				T t2 = get(t[i], t[k]);
				T t3 = get(t[j], t[k]);
				T t4 = (T){(t1.x+ t2.x) / 2, (t1.y+ t2.y) / 2};
				T t5 = (T){(t4.x + t3.x) / 2, (t4.y + t3.y) / 2};
				double dst = sqr(t5.x - t1.x) + sqr(t5.y - t1.y);
				if(dst == sqr(t5.x) + sqr(t5.y))++ans;
			}
	writeln(ans);
	return 0;
}
