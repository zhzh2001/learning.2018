#include<cstdio>
#include<algorithm>
using namespace std;
int n,d[3100];
long long ans;
double x,y,a[3100],b[3100],c[3100],p[3100],q[3100];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void jd(int i,int j){
	if(a[i]==0&&b[j]==0){
		x=-c[j]/a[j];
		y=-c[i]/a[i];
	}
	if(a[j]==0&&b[i]==0){
		x=-c[i]/a[i];
		y=-c[j]/a[j];
	}
	x=(b[i]*c[j]-b[j]*c[i])/(a[i]*b[j]-a[j]*b[i]);
	y=(a[i]*c[j]-a[j]*c[i])/(a[j]*b[i]-a[i]*b[j]);
}
double jl(int i){
	return p[i]*p[i]+q[i]*q[i];
}
void sor(int l,int r){
	int i=l,j=r;
	double x=p[(l+r)>>1];
	while(i<=j){
		while(p[i]<x)i++;
		while(x<p[j])j--;
		if(i<=j){
			swap(p[i],p[j]);
			swap(q[i],q[j]);
			swap(d[i],d[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}
void sol(int z){
	int k=0;
	for(int i=1;i<=n;i++)
		if(i!=z){
			jd(i,z);
			k++;
			p[k]=x;
			q[k]=y;
			d[k]=i;printf("%d %0.2lf %0.2lf\n",i,x,y);
		}
	sor(1,k);//printf("%d\n",z);
	int l=1,r=k,l1=0,r1=0,l2=0,r2=0;
	while(l<r){
		while(jl(l)!=jl(r)&&l<r)
			if(jl(l)<jl(r))r--;
			else l++;
		if(l>=r)break;
		l1=l;
		r1=l+1;
		while(jl(r1)==jl(l1)){
			jd(d[l1],d[r1]);
			if(x!=p[l1]||y!=q[l1])break;
			r1++;
		}
		l2=l-1;
		r2=l;
		while(jl(r2)==jl(l2)){
			jd(d[l2],d[r2]);
			if(x!=p[r2]||y!=q[r2])break;
			l2--;
		}
		for(int i=l1;i<r1;i++)
			for(int j=r2;j>l1;j--){
				jd(d[i],d[j]);
				if(x*x+y*y==jl(l1))ans++;
			}
		l=r1;
		r=l2;
	}
}
int main(){
//	freopen("geometry.in","r",stdin);
//	freopen("geometry.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		b[i]=read();
		c[i]=read();
	}
	for(int i=1;i<=n;i++)
		sol(i);
	printf("%lld",ans/3);
	return 0;
}
