#include<cstdio>
#include<algorithm>
using namespace std;
int n;
long long ans;
double x,y,x1,y1,x2,y2,x3,y3,r,a[3100],b[3100],c[3100];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void yf(){
	x=((y2-y1)*(y3*y3-y1*y1+x3*x3-x1*x1)-(y3-y1)*(y2*y2-y1*y1+x2*x2-x1*x1))/(2.0*((x3-x1)*(y2-y1)-(x2-x1)*(y3-y1)));
	y=((x2-x1)*(x3*x3-x1*x1+y3*y3-y1*y1)-(x3-x1)*(x2*x2-x1*x1+y2*y2-y1*y1))/(2.0*((y3-y1)*(x2-x1)-(y2-y1)*(x3-x1)));
	r=(x1-x)*(x1-x)+(y1-y)*(y1-y);
}
void jd(int i,int j){
	if(a[i]==0&&b[j]==0){
		x=-c[j]/a[j];
		y=-c[i]/a[i];
	}
	if(a[j]==0&&b[i]==0){
		x=-c[i]/a[i];
		y=-c[j]/a[j];
	}
	x=(b[i]*c[j]-b[j]*c[i])/(a[i]*b[j]-a[j]*b[i]);
	y=(a[i]*c[j]-a[j]*c[i])/(a[j]*b[i]-a[i]*b[j]);
}
int dy(double x,double y){
	if(x-1e-8<y&&x+1e-8>y)return 1;
	else return 0;
}
int main(){
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		b[i]=read();
		c[i]=read();
	}
	for(int i=1;i<n-1;i++)
		for(int j=i+1;j<n;j++)
			for(int k=j+1;k<=n;k++){
				jd(i,j);
				x1=x;
				y1=y;
				jd(i,k);
				x2=x;
				y2=y;
				jd(j,k);
				x3=x;
				y3=y;
				yf();
//				printf("%0.9lf %0.9lf %0.9lf %0.9lf\n",x,y,x*x+y*y,r);
				if(dy(x*x+y*y,r))ans++;
			}
	printf("%lld",ans);
	return 0;
}
