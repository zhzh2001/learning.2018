#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,l,mid,r,ans,tot,u[110000],v[110000],w[110000],f[110000],p[210000],ne[210000],he[110000];
long long s,x[110000],sx[110000],son[110000];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void adg(int u,int v){
	tot++;
	p[tot]=v;
	ne[tot]=he[u];
	he[u]=tot;
}
void dfs(int k){
	f[k]=1;
	sx[k]=x[k];
	son[k]=1;
	for(int i=he[k];i;i=ne[i])
		if(!f[p[i]]){
			dfs(p[i]);
			son[k]+=son[p[i]];
			sx[k]+=sx[p[i]];
		}
}
int che(int bz){
	tot=0;
	for(int i=1;i<=n;i++){
		he[i]=0;
		f[i]=0;
	}
	for(int i=1;i<n;i++)
		if(w[i]<bz){
			adg(u[i],v[i]);
			adg(v[i],u[i]);
		}
	for(int i=1;i<=n;i++){
		if(!f[i])dfs(i);
		if(son[i]>s-sx[i])return 0;
	}
	return 1;
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		u[i]=read();
		v[i]=read();
		w[i]=read();
	}
	for(int i=1;i<=n;i++){
		x[i]=read();
		s+=x[i];
	}
	l=0;
	r=1000000001;
	while(l+1<r){
		mid=(l+r)>>1;
		if(che(mid))l=mid;
		else r=mid;//printf("%d %d %d\n",l,r,che(mid));
	}
	ans=0;
	for(int i=1;i<n;i++)
		if(w[i]<=l&&w[i]>ans)ans=w[i];
	printf("%d",ans);
	return 0;
}
