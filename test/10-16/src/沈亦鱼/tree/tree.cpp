#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,u,v,tot,p[210000],ne[210000],he[110000];
long long s,f[110000][160],dp[110000][160];
inline long long read(){
	long long ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void adg(int u,int v){
	tot++;
	p[tot]=v;
	ne[tot]=he[u];
	he[u]=tot;
}
void dfs(int u,int fa){
	f[u][0]=1;
	for(int i=he[u];i;i=ne[i])
		if(p[i]!=fa){
			dfs(p[i],u);
			for(int j=1;j<=m;j++)
				f[u][j]+=f[p[i]][j-1];
			f[u][0]+=f[p[i]][m];
			f[u][m+1]+=f[p[i]][m+1]+f[p[i]][m];
		}
}
void sol(int u,int fa){
	for(int i=0;i<=m+1;i++){
		dp[u][i]+=f[u][i];//printf("%d %d:%d\n",u,i,dp[u][i]);
		s+=dp[u][i];
	}
	s-=dp[u][0];
	for(int i=he[u];i;i=ne[i])
		if(p[i]!=fa){
			for(int j=2;j<=m;j++)
				dp[p[i]][j]+=dp[u][j-1]-f[p[i]][j-2];
			dp[p[i]][1]+=dp[u][0]-f[p[i]][m];
			dp[p[i]][0]+=dp[u][m]-f[p[i]][m-1];
			dp[p[i]][m+1]+=dp[u][m+1]-f[p[i]][m]-f[p[i]][m+1]+dp[u][m]-f[p[i]][m-1];
			sol(p[i],u);
		}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	m=read();m--;
	for(int i=1;i<n;i++){
		u=read();
		v=read();
		adg(u,v);
		adg(v,u);
	}
	dfs(1,0);
	sol(1,0);
	printf("%lld",s/2);
	return 0;
}
