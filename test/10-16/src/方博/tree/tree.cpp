#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x)
{
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=100005;
int head[N],nxt[2*N],tail[2*N];
ll f[N][155],s[N],sz[N],t;
ll sf[N][155];
ll ss[155];
ll ans,n,l;
void addto(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
void dfs(int k,int fa)
{
	int ret=0;
	sz[k]=1;
	s[k]=0;
	f[k][l]=1;
	for(int i=head[k];i;i=nxt[i]){
		int v=tail[i];
		if(v==fa)continue;
		dfs(v,k);
		ret+=s[k]*sz[v]+(s[v]+f[v][l])*sz[k];
		s[k]+=s[v];
		sz[k]+=sz[v];
		for(int y=1;y<l;y++)
			ret-=sf[k][l-y-1]*f[v][y];
		for(int x=1;x<l;x++)
			ret-=f[k][x]*f[v][l];
		for(int x=1;x<=l;x++)
			f[k][x]+=f[v][x-1];
		f[k][1]+=f[v][l];
		s[k]+=f[v][l];
		for(int x=1;x<=l;x++)
			sf[k][x]=sf[k][x-1]+f[k][x];
	}
	ans+=ret;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();l=read();
	for(int i=1;i<n;i++){
		int x,y;
		x=read(),y=read();
		addto(x,y);
		addto(y,x);
	}
	dfs(1,1);
	writeln(ans);
	return 0;
}
/*
6 2
1 2
1 3
2 4
2 5
4 6
*/
