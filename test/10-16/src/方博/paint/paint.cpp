#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x)
{
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=100005;
ll sum[N],sz[N];
ll f[N];
int gf(int k)
{
	if(f[k]!=k)f[k]=gf(f[k]);
	return f[k];
}
struct xxx{
	ll x,y,z;
}a[N];
ll n;
bool com(xxx a,xxx b)
{
	return a.z<b.z;
}
ll ss,ans;
int main()
{
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++)
		a[i].x=read(),a[i].y=read(),a[i].z=read();
	sort(a+1,a+n,com);
	for(int i=1;i<=n;i++)
		sz[i]=1,sum[i]=read(),ss+=sum[i],f[i]=i;
	ans=a[1].z;
	for(int i=1;i<n;i++){
		int tx=gf(a[i].x);
		int ty=gf(a[i].y);
		f[tx]=ty;
		sum[ty]+=sum[tx];
		sz[ty]+=sz[tx];
		if(ss-sum[ty]<sz[ty]){
			writeln(ans);
			return 0;
		}
		ans=a[i+1].z;
	}
	return 0;
}

