#include<bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0,f=0;char ch=getchar();
  for (;ch<'0'||ch>'9';ch=getchar()) if (ch=='-') f=1;
  for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-48;
  return f?-x:x;
}
int main() {
  printf("%d\n", read());
  return 0;
}
