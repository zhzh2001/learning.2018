#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline int read() {
  int x=0,f=0;char ch=getchar();
  for (;ch<'0'||ch>'9';ch=getchar()) if (ch=='-') f=1;
  for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-48;
  return f?-x:x;
}
#define N 100005
int n, m, head[N], cnt;
struct edge {
  int from, nxt, to;
}e[N<<1];
void insert(int u, int v) {
  e[++cnt] = (edge){u, head[u], v}; head[u] = cnt;
  e[++cnt] = (edge){v, head[v], u}; head[v] = cnt;
}
int dis[3005][3005], px;
void dfs(int x, int fa) {
  for (int i = head[x]; i; i = e[i].nxt)
    if (e[i].to != fa) {
      dis[px][e[i].to] = dis[px][x]+1;
      dfs(e[i].to, x);
	}
}
int sz[N], dep[N];
void Dfs(int x, int fa) {
  sz[x] = 1;
  for (int i = head[x]; i; i = e[i].nxt)
    if (e[i].to != fa) {
      dep[e[i].to] = dep[x]+1;
      Dfs(e[i].to, x);
      sz[x] +=sz[e[i].to];
	}
}
int main() {
  freopen("tree.in","r",stdin);
  freopen("tree.out","w",stdout);
  n = read(); m = read();
  for (int i = 1; i < n; i++) {
  	int u = read(), v = read();
  	insert(u, v);
  }
  if (m == 1) {
  	Dfs(1, 0);
  } else {
  	ll ans = 0;
  	for (px = 1; px <= n; px++) dfs(px, 0);
//  	cout << 0 << endl;
  	for (int i = 1; i <= n; i++)
  	  for (int j = 1; j < i; j++) {
  	    ans += dis[i][j]/m;
		if (dis[i][j]%m != 0) ans++; 	
	  }
	printf("%lld\n", ans);
  }
  return 0;
}
