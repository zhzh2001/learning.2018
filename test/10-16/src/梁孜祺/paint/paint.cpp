#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define N 100005 
int n, fa[N]; 
ll sz[N], Sum[N], flag[N], x[N], a[N], b[N];
const int oo = 1e9;
struct edge{
  int u, v, w;
}e[N];
int find(ll x) { 
  return (fa[x]==x)?x:(fa[x]=find(fa[x])); 
}
bool check(int mid){
  for (int i = 1; i <= n; i++)
	fa[i] = i, sz[i] = x[i], Sum[i] = 1, flag[i] = 0;
  for (int i = 1; i < n; i++) {
    if (e[i].w >= mid) continue;
    int u = find(e[i].u), v = find(e[i].v);
	if (u != v){
	  fa[u]=v; 
	  sz[v] += sz[u]; 
	  Sum[v] += Sum[u];
	}
  }
  int len = 0;
  ll sum = 0;
  for(int i = 1; i <= n; i++){
    int p=find(i);
    if (!flag[p]) 
	  a[++len]=Sum[p], b[len]=sz[p], sum+=sz[p], flag[p]=1;
  }
  for (int i = 1; i <= len; i++)
  	if (a[i]>sum-b[i]) return 0;
  return 1;
}
int main(){
  freopen("paint.in","r",stdin);
  freopen("paint.out","w",stdout);
  scanf("%d", &n);
  for (int i = 1; i < n; i++) {
  	scanf("%d%d%d",&e[i].u, &e[i].v, &e[i].w);
  }
  for (int i = 1; i <= n; i++) scanf("%d",&x[i]);
  int l = 0, r = oo, ans = 0;
  while (l <= r){
	ll mid = (l+r)>>1;
	if (check(mid)) ans = mid,l = mid+1;
	else r = mid-1;
  }
  printf("%lld\n",ans);
  return 0;
}
