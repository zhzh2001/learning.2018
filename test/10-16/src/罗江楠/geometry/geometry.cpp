#include <bits/stdc++.h>
#define N 100020
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
double a[N], b[N], c[N];
double theta[N];
const double PI = acos(-1);
typedef pair<double, double> point;

const point yuandian = make_pair(0.0, 0.0);

inline point getJiao(int i, int j) {
  double k1 = - a[i] / b[i];
  double b1 = - c[i];
  double k2 = - a[j] / b[j];
  double b2 = - c[j];

  if (!b[i]) return make_pair(- c[i] / a[i], k2 * (- c[i] / a[i]) + b2);
  if (!b[j]) return make_pair(- c[j] / a[j], k1 * (- c[j] / a[j]) + b1);

  double x = (b2 - b1) / (k1 - k2);
  double y = k1 * x + b1;
  return make_pair(x, y);
}

inline point getMiddle(const point &a, const point &b) {
  return make_pair((a.first + b.first) / 2, (a.second + b.second) / 2);
}

inline point getFunctionWithK(const point &a, int i) {
  double k = - ::b[i] / ::a[i];
  double b = a.second - k * a.first;
  return make_pair(k, b);
}

inline point getFunctionJiao(const point &a, const point &b) {
  double k1 = a.first;
  double b1 = a.second;
  double k2 = b.first;
  double b2 = b.second;

  double x = (b2 - b1) / (k1 - k2);
  double y = k1 * x + b1;
  return make_pair(x, y);
}

inline double getDistance(const point &a, const point &b) {
  return sqrt((a.first - b.first) * (a.first - b.first) + (a.second - b.second) * (a.second - b.second));
}

int main(int argc, char const *argv[]) {
  freopen("geometry.in", "r", stdin);
  freopen("geometry.out", "w", stdout);

  int n = read();

  for (int i = 1; i <= n; ++ i) {
    a[i] = read();
    b[i] = read();
    c[i] = read();
  }

  int ans = 0;
  // debug(getJiao(1, 2));

  for (int i = 1; i <= n; ++ i) {
    for (int j = i + 1; j <= n; ++ j) {
      for (int k = j + 1; k <= n; ++ k) {

        point pij = getJiao(i, j);
        point pik = getJiao(i, k);
        point pjk = getJiao(j, k);


        point mi = getMiddle(pij, pik);
        point mj = getMiddle(pij, pjk);
        point mk = getMiddle(pik, pjk);

        point zcxi = getFunctionWithK(mi, i);
        point zcxj = getFunctionWithK(mj, j);
        
        point yuanxin = getFunctionJiao(zcxi, zcxj);

        if (fabs(getDistance(yuanxin, yuandian) - getDistance(yuanxin, pij)) < 1e-6) {
          ++ ans;
        }
      }
    }
  }
  printf("%d\n", ans);

  return 0;
}

/*

a1x + b1y + c1 = 0
a2x + b2y + c2 = 0
a3x + b3y + c3 = 0
y=-(a1/b1)x-c1/b1
y=-(a2/b2)x-c2/b2
y=-(a3/b3)x-c3/b3

设 (x1, y1) 为圆的中心。

做平行线：

y = -(a1/b1)x+c1'

c1' = (a1/b1)x1+y1

做垂线：

y = -(b1/a1)x+c1''

c1'' = y1+(b1/a1)x1

  y = -(b1/a1)x+y1+(b1/a1)x1

与直线的交点为：

  y = -(b1/a1)x + y1+(b1/a1)x1
  y = -(a1/b1)x - c1/b1

  -(b1/a1)x + y1+(b1/a1)x1 = -(a1/b1)x - c1/b1
  ((b1/a1)-(a1/b1))x = c1/b1+y1+(b1/a1)x1
  x = (c1/b1+y1+(b1/a1)x1)/((b1/a1)-(a1/b1))

  {
    y = -(a1/b1)*(c1/b1+y1+(b1/a1)x1)/((b1/a1)-(a1/b1))-c1/b1
    x = (c1/b1+y1+(b1/a1)x1)/((b1/a1)-(a1/b1))
  }

  dist = 
    sqrt(((c1/b1+y1+(b1/a1)x1)/((b1/a1)-(a1/b1)) - x1)^2 + (-(a1/b1)*(c1/b1+y1+(b1/a1)x1)/((b1/a1)-(a1/b1))-c1/b1 - y1)^2);

  ((c1/b1+y1+(b1/a1)x1)/((b1/a1)-(a1/b1)) - x1)^2 + (-(a1/b1)*(c1/b1+y1+(b1/a1)x1)/((b1/a1)-(a1/b1))-c1/b1 - y1)^2
= 

*/