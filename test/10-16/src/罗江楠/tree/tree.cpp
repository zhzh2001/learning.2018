#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 100020
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

int to[N<<1], nxt[N<<1], cnt, head[N];
void insert(int x, int y) {
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
  to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}

long long n, k, ans;

void dfs(int x, int f, int d) {
  ans += (d / k) + !!(d % k);

  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    dfs(to[i], x, d + 1);
  }

}

int sz[N];
int dfs_sz(int x, int f) {
  sz[x] = 1;
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    sz[x] += dfs_sz(to[i], x);
  }
  return sz[x];
}

int main(int argc, char const *argv[]) {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);

  n = read(); k = read();

  for (int i = 1; i < n; ++ i) {
    insert(read(), read());
  }

  if (k == 1) {
    dfs_sz(1, 0);
    for (int i = 2; i <= n; ++ i) {
      ans += sz[i] * (n - sz[i]);
    }
  } else {
    for (int i = 1; i <= n; ++ i) {
      dfs(i, 0, 0);
    }
    ans >>= 1;
  }

  printf("%lld\n", ans);

  return 0;
}