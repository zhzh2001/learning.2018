#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 100020
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

struct edge {
  int x, y, w;
} e[N];

bool cmp(const edge &a, const edge &b) {
  return a.w < b.w;
}

int x[N], fa[N], xs[N], sz[N];
int find(int x) {
  return x == fa[x] ? x : fa[x] = find(fa[x]);
}
void merge(int x, int y) {
  int fx = find(x), fy = find(y);
  if (sz[fx] > sz[fy]) swap(fx, fy);
  fa[fx] = fy;
  sz[fy] += sz[fx];
  xs[fy] += xs[fx];
}

int n;
int d[N], b[N];
multiset<pair<int, int> > st;
bool check(int mid) {
  for (int i = 1; i <= n; ++ i) {
    fa[i] = i;
    xs[i] = x[i];
    sz[i] = 1;
  }
  for (int i = 1; i < n; ++ i) {
    if (e[i].w < e[mid].w) {
      merge(e[i].x, e[i].y);
    } else {
      break;
    }
  }
  int cnt = 0;
  st.clear();
  for (int i = 1; i <= n; ++ i) {
    if (fa[i] == i) {
      d[++ cnt] = sz[i];
      st.insert(make_pair(xs[i], cnt));
    }
  }
  for (int i = 1; i <= cnt; ++ i) {
    while (d[i]) {
      set<pair<int, int> >::iterator it = st.begin();
      if (it -> second == i) ++ it;
      if (it == st.end()) return false;
      const int cost = min(it -> first, d[i]);
      d[i] -= cost;
      st.erase(it);
      if (it -> first > cost) {
        st.insert(make_pair(it -> first - cost, it -> second));
      }
    }
  }
  return true;
}

int main(int argc, char const *argv[]) {
  freopen("paint.in", "r", stdin);
  freopen("paint.out", "w", stdout);

  n = read();

  for (int i = 1; i < n; ++ i) {
    e[i].x = read();
    e[i].y = read();
    e[i].w = read();
  }

  sort(e + 1, e + n, cmp);

  for (int i = 1; i <= n; ++ i) {
    x[i] = read();
  }

  int l = 1, r = n - 1;
  while (l <= r) {
    int mid = (l + r) >> 1;
    if (check(mid)) {
      l = mid + 1;
    } else {
      r = mid - 1;
    }
  }

  printf("%d\n", e[l - 1].w);

  return 0;
}