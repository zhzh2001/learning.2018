#include<bits/stdc++.h>
#define int long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
int head[100100],nxt[200100],tail[200100];
int s[100100],sz[100100],ss[155];
int f[100100][155],sf[100100][155];
int ans,n,l,t;
inline int read()
{
    int x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void add(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
void dfs(int k,int fa)
{
	int ret=0;
	sz[k]=1;
	s[k]=0;
	f[k][l]=1;
	for(int i=head[k];i;i=nxt[i])
	{
		int v=tail[i];
		if(v==fa)continue;
		dfs(v,k);
		ret+=s[k]*sz[v]+(s[v]+f[v][l])*sz[k];
		s[k]+=s[v];
		sz[k]+=sz[v];
		For(y,1,l-1)ret-=sf[k][l-y-1]*f[v][y];
		For(x,1,l-1)ret-=f[k][x]*f[v][l];
		For(x,1,l)f[k][x]+=f[v][x-1];
		f[k][1]+=f[v][l];
		s[k]+=f[v][l];
		For(x,1,l)sf[k][x]=sf[k][x-1]+f[k][x];
	}
	ans+=ret;
}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();l=read();
	For(i,1,n-1)
	{
		int x=read(),y=read();
		add(x,y);
		add(y,x);
	}
	dfs(1,1);
	cout<<ans<<endl;
	return 0;
}
