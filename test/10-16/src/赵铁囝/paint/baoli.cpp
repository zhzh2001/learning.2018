#include <algorithm>
#include <iostream>
#include <cstdio>

#define lowbit(x) x&(-x)
#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Edge{
	int v,w,to;
}e[Max*2];

struct Node{
	int l,r,w;
	inline bool operator<(const Node&x)const{
		return w>x.w;
	}
}a[Max];

int n,l,r,w,x,y,xx,yy,cnt,size,root,b[Max],c[Max],f[Max],g[Max],id[Max],fa[Max],dep[Max],num[Max],sum[Max],head[Max];

inline void add(int u,int v,int w){
	e[++size].v=v;e[size].to=head[u];e[size].w=w;head[u]=size;
}

inline void inc1(int x,int k){
	while(x<=n){
		f[x]+=k;
		x+=lowbit(x);
	}
	return;
}

inline void inc2(int x,int k){
	while(x<=n){
		g[x]+=k;
		x+=lowbit(x);
	}
	return;
}

inline int ask1(int x){
	int ans=0;
	while(x){
		ans+=f[x];
		x-=lowbit(x);
	}
	return ans;
}

inline int ask2(int x){
	int ans=0;
	while(x){
		ans+=g[x];
		x-=lowbit(x);
	}
	return ans;
}

inline void dfs(int u,int last){
	id[u]=++cnt;
	num[cnt]=u;
	fa[u]=last;
	dep[u]=dep[last]+1;
	sum[u]=1;
	for(int i=head[u];i;i=e[i].to){
		int v=e[i].v;
		if(v==last)continue;
		dfs(v,u);
		sum[u]+=sum[v];
	}
	return;
}

inline void dfs2(int u,int last){
	inc1(id[u],-b[u]);
	inc2(id[u],-c[u]);
	b[u]=c[u]=0;
	for(int i=head[u];i;i=e[i].to){
		int v=e[i].v;
		if(v==last)continue;
		dfs2(v,u);
	}
	return;
}

int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		l=read();r=read();w=read();
		add(l,r,w);
		add(r,l,w);
		a[i].l=l;a[i].r=r;a[i].w=w;
	}
	dfs(1,1);
	for(int i=1;i<=n;i++){
		b[i]=1;
		c[i]=read();
		inc1(id[i],b[i]);
		inc2(id[i],c[i]);
	}
	sort(a+1,a+n);
	root=1;
	x=ask1(cnt);
	y=ask2(cnt);
	for(int i=1;i<n;i++){
//		cout<<x<<" "<<y<<endl;
		l=a[i].l;r=a[i].r;
		if(!b[l]||!b[r])continue;
		if(dep[l]>dep[r])swap(l,r);
		xx=ask1(id[r]+sum[r]-1)-ask1(id[r]-1);yy=ask2(id[r]+sum[r]-1)-ask2(id[r]-1);
//		cout<<l<<" "<<r<<" "<<xx<<" "<<yy<<endl;
		if(xx<=y-yy){
			x-=xx;
			x-=min(x,yy);
			y-=xx;
			y-=min(y,xx);
			dfs2(r,fa[r]);
		}
		if(!x){
			writeln(a[i].w);
			break;
		}
	}
	return 0;
}
