#include <iostream>
#include <cstdio>
#include <map>

#define Max 3005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Node{
	int a,b,c;
	inline bool operator<(const Node&x)const{
		if(a!=x.a)return a<x.a;
		if(b!=x.b)return b<x.b;
		return c<x.c;
	}
}a[Max],now;

struct Number{
	int a,b;
	inline Number operator+(const int&x)const{
		Number ans;
		ans.a=a+b*x;
		ans.b=b;
		return ans;
	}
	inline Number operator*(const int&x)const{
		Number ans;
		ans.a=a*x;
		ans.b=b;
		return ans;
	}
	inline Number operator/(const int&x)const{
		Number ans;
		ans.a=a;
		ans.b=b*x;
		return ans;
	}
	inline Number operator+(const Number&x)const{
		Number ans;
		ans.a=a*x.b+x.a*b;
		ans.b=b*x.b;
		return ans;
	}
	inline Number operator-(const Number&x)const{
		Number ans;
		ans.a=a*x.b-x.a*b;
		ans.b=b*x.b;
		return ans;
	}
	inline Number operator/(const Number&x)const{
		Number ans;
		ans.a=a*x.b;
		ans.b=b*x.a;
		return ans;
	}
	inline Number operator*(const Number&x)const{
		Number ans;
		ans.a=a*x.a;
		ans.b=b*x.b;
		return ans;
	}
};

int n,ans;

map<Node,bool>vis;

inline int gcd(int x,int y){
	return x%y?gcd(y,x%y):y;
}

inline void calc(Node&x){
	if(x.a<0){
		x.a=-x.a;
		x.b=-x.b;
		x.c=-x.c;
	}
	if(!x.a||!x.b||!x.c)return;
	int now=gcd(gcd(x.a,x.b),x.c);
	x.a/=now;
	x.b/=now;
	x.c/=now;
	return;
}

inline void calc3(Number&x){
	int cnt=0;
	if(x.a<0){
		cnt++;
	}
	if(x.b<0){
		cnt++;
	}
	if(cnt==2){
		x.a=-x.a;
		x.b=-x.b;
	}
	if(cnt==1){
		if(x.a>0)x.a=-x.a;
		if(x.b<0)x.b=-x.b;
	}
	int now=gcd(x.a,x.b);
	if(!x.a)return;
	x.a/=now;
	x.b/=now;
	return;
}

inline Node calc2(Node a,Node b){
	Node ans;
	Number x,y,xx,yy,xxx,yyy,a1,b1,a2,b2;
	if(a.a==0){
		if(b.b==0){
			x.a=a.c;
			x.b=a.a;
			y.a=-b.c;
			y.b=b.b;
			xx.a=-a.c;
			xx.b=a.a;
			yy.a=b.c;
			yy.b=b.b;
//			a1.a=
//			a1.b=
//			b1.a=
//			b1.b=
//			ans.a=
//			ans.b=
//			ans.c=
		}else{
		}
		return ans;
	}
	x.a=a.c*b.b-b.c*a.b;
	x.b=b.a*a.b-a.a*b.b;
	calc3(x);
	y=(x*a.a+a.c)/a.b;
	y.a=-y.a;
	calc3(y);
	a1.a=-a.a;
	a1.b=a.b;
	b1.a=-a.c;
	b1.b=a.b;
	a2.a=a.b;
	a2.b=a.a;
	xx=b1/(a1-a2);
	xx.a=-xx.a;
	calc3(xx);
	yy=a2*xx;
	calc3(yy);
	xxx=xx*2-x;
	yyy=yy*2-y;
	a1.a=-b.a;
	a1.b=b.b;
	b1.a=-b.c;
	b1.b=b.b;
	a2.a=b.b;
	a2.b=b.a;
	xx=b1/(a1-a2);
	xx.a=-xx.a;
	calc3(xx);
	yy=a2*xx;
	calc3(yy);
	x=xx*2-x;
	y=yy*2-y;
	if(x.a/x.b==xxx.a/xxx.b){
		ans.a=1;
		ans.b=0;
		ans.c=x.a/x.b;
		return ans;
	}
	if(y.a/y.b==yyy.a/yyy.b){
		ans.a=0;
		ans.b=1;
		ans.c=y.a/y.b;
		return ans;
	}
//	cout<<x.a<<" "<<x.b<<" "<<y.a<<" "<<y.b<<endl;
//	a1=(yyy-y)/(x-xxx);
//	b1=y-a1*x;
	ans.a=a1.a*b1.b;
	ans.b=-a1.b*b1.b;
	ans.c=b1.a*a1.b;
	calc(ans);
//	cout<<ans.a<<" "<<ans.b<<" "<<ans.c<<endl;
	return ans;
}

int main(){
	freopen("geometry.in","r",stdin);
	freopen("geometry.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i].a=read();
		a[i].b=read();
		a[i].c=read();
		calc(a[i]);
		vis[a[i]]=true;
	}
	for(int i=1;i<n;i++){
		for(int j=i+1;j<=n;j++){
//			cout<<i<<" "<<j<<" ";
			now=calc2(a[i],a[j]);
			if(vis[now])ans++;
//			cout<<now.a<<" "<<now.b<<" "<<now.c<<endl;
		}
	}
	writeln(ans);
	return 0;
}
