#include <iostream>
#include <cstdio>
#include <cmath>

#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Edge{
	int v,to;
}e[Max*2];

int n,k,l,r,now,size,fa[Max],dep[Max],sum[Max],head[Max],f[Max][200];

inline void add(int u,int v){
	e[++size].v=v;e[size].to=head[u];head[u]=size;
}

inline void dfs(int u,int last){
	fa[u]=last;
	sum[u]=1;
	dep[u]=dep[last]+1;
	for(int i=head[u];i;i=e[i].to){
		int v=e[i].v;
		if(v==last)continue;
		dfs(v,u);
		sum[u]+=sum[v];
	}
	return;
}

inline int calc(int x,int y){
	int ans=0;
	if(dep[x]<dep[y])swap(x,y);
	while(dep[x]>dep[y]){
		ans++;
		x=fa[x];
	}
	while(x!=y){
		ans+=2;
		x=fa[x];y=fa[y];
	}
	return ans;
}

int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();k=read();
	for(int i=1;i<n;i++){
		l=read();r=read();
		add(l,r);
		add(r,l);
	}
	dfs(1,1);
	int ans=0;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			now=calc(i,j);
			ans+=now/k;
			ans+=now%k;
//			cout<<i<<" "<<j<<" "<<(now/k+now%k)<<endl;
		}
	}
	writeln(ans/2);
	return 0;
}
