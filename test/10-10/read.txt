由于某些题目读入可能较多，所以给出读入优化模板
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,LEN,stdin);
		if(S == T) return EOF;
	}
	return *S++;
}
#define ll long long
inline ll read()
{
    ll x=0,f=1;char ch=gc();
    for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
    for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
    return x*f;
}