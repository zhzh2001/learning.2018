#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,op,a[503],b[503],ans;
bool vis[503];
bool test0(){
	int s=a[1],l=a[1];
	for(int i=2;i<=n;i++){
		if(a[i]>=s){
			l=a[i]-s;
		}else if(a[i]>=l){
			l=(l^a[i])&1;
		}else{
			l-=a[i];
		}
		s+=a[i];
	}
	return l==0;
}
bool test1(){
	for(int i=1;i<=n;i++)b[a[i]]++;
	int p=n,x;
	while(1){
		while(p>0&&b[p]==0)p--;
		if(p==0)return 1;
		b[p]--;
		x=p;
		while(x>0){
			while(p>0&&b[p]==0)p--;
			if(p==0)return 0;
			if(b[p]>x){
				b[p-1]+=x;
				b[p]-=x;
				x=0;
			}else{
				b[p-1]+=b[p];
				x-=b[p];
				b[p]=0;
			}
		}
	}
}
int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	n=read();
	op=read();
	for(int i=1;i<=n+1;i++){
		a[i]=read();
	}
	if(op==0){
		for(int i=1;i<=n+1;i++){
			swap(a[i],a[n+1]);
			if(test0()){
				ans++;
				vis[i]=1;
			}
			swap(a[i],a[n+1]);
		}
	}else if(op==1){
		for(int i=1;i<=n+1;i++){
			swap(a[i],a[n+1]);
			if(test1()){
				ans++;
				vis[i]=1;
			}
			swap(a[i],a[n+1]);
		}
	}
	printf("%d\n",ans);
	for(int i=1;i<=n+1;i++)if(vis[i]){
		printf("%d\n",i);
	}
	return 0;
}
