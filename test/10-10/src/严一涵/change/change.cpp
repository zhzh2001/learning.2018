#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
inline void write(int x){
	static char c[23];
	static int n;
	if(x<0){
		putchar('-');
		x=-x;
	}
	if(x==0){
		putchar('0');
		return;
	}
	n=0;
	while(x>0){
		c[n++]=x%10;
		x/=10;
	}
	while(n-->0){
		putchar(c[n]+'0');
	}
}
#undef dd
int n,x;
bool vis[300003];
int main(){
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	n=read();
	x=0;
	for(int i=1;i<=n;i++){
		vis[read()]=1;
		while(vis[n-x])x++;
		write(i-x);
		putchar(' ');
	}
	return 0;
}
