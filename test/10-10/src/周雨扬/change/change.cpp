#include<bits/stdc++.h>
using namespace std;
const int LBC=2333333;
char LZH[LBC],*SSS=LZH,*TTT=LZH;
inline char gc(){
	if (SSS==TTT)
		TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char ch=getchar();
	for (;ch<'0'||ch>'9';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar())
		x=x*10-48+ch;
	return x;
}
void write(int x){
	if (!x){
		putchar('0');
		return;
	}
	static int a[20],top;
	for (top=0;x;a[++top]=x%10,x/=10);
	for (;top;putchar(a[top--]+'0'));
}
const int N=300005;
bool vis[N];
int n;
int main(){
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	n=read();
	int las=n;
	for (int i=1;i<=n;i++){
		int x=read(); vis[x]=1;
		for (;vis[las];las--);
		//printf("%d\n",las);
		write(i-(n-las));
		putchar(' ');
	}
}
