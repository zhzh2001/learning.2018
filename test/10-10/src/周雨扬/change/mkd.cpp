#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0;
	char ch=getchar();
	for (;ch<'0'||ch>'9';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar())
		x=x*10-48+ch;
	return x;
}
const int N=300005;
int a[300005];
int rnd(){
	int x=0;
	for (int i=1;i<=9;i++)
		x=x*10+rand()%10;
	return x;
}
int main(){
	freopen("change.in","w",stdout);
	printf("%d\n",300000);
	for (int i=1;i<=300000;i++) a[i]=i;
	for (int i=2;i<=300000;i++)
		swap(a[i],a[rnd()%(i-1)+1]);
	for (int i=1;i<=300000;i++)
		printf("%d\n",a[i]);
}
