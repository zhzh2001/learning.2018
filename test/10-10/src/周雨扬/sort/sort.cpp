#include<bits/stdc++.h>
using namespace std;
const int LBC=2333333;
char LZH[LBC],*SSS=LZH,*TTT=LZH;
inline char gc(){
	if (SSS==TTT)
		TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
	return *SSS++;
}
inline long long read(){
	long long x=0;
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
int n,m;
const int N=1005;
long long a[N][N],b[N][N],f[N][N];
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(); m=read();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			b[i][j]=b[i][j-1]+read();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			a[i][j]=a[i-1][j]+read();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			f[i][j]=max(f[i][j-1]+a[i][j],f[i-1][j]+b[i][j]);
	printf("%lld\n",f[n][m]);
}
/*
4 4
0 0 10 9
1 3 10 0
4 2 1 3
1 1 20 0
10 0 0 0
1 1 1 30
0 0 5 5
5 10 10 10 
*/
