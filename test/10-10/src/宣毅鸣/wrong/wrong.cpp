#include<bits/stdc++.h>
using namespace std;
const int N=505;
int n,opt,a[N],zhan[N],ans,cnt[N],b[N];
int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	scanf("%d%d",&n,&opt);
	for (int i=1;i<=n+1;i++)scanf("%d",&a[i]);
	if (!opt){
		for (int i=1;i<=n+1;i++){
			int sum=0,s=0;
			for (int j=1;j<=n+1;j++)
				if (j!=i)s=max(s,a[j]),sum+=a[j];
			if (sum%2==0&&sum-s>=s)zhan[++ans]=i;	
		}
	}
	else{
		for (int i=1;i<=n+1;i++){
			int flag=1;
			memset(cnt,0,sizeof cnt);
			for (int j=1;j<=n+1;j++)
				if (j!=i)cnt[a[j]]++;
			int now=n;
			while (now){
				while (now&&!cnt[now])now--;
				if (!now)break;
				cnt[now]--;
				for (int j=1;j<=n;j++)b[j]=cnt[j];
				int j=now,c=now;
				while (j&&c){
					int k=min(cnt[j],c);
					b[j]-=k;c-=k;
					b[j-1]+=k;
					j--;
				}
				if (c){
					flag=0;
					break;
				}
				for (int j=1;j<=n;j++)cnt[j]=b[j];
			}
			if (flag)zhan[++ans]=i;
		}
	}
	printf("%d\n",ans);
	for (int i=1;i<=ans;i++)printf("%d\n",zhan[i]);
	return 0;
}
