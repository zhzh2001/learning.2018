#include<bits/stdc++.h>
using namespace std;
const int N=300005;
int n,l,d[N];
const int LEN = 200005;
char STR[LEN],*S=STR,*T=STR;
inline char gc(){
	if (S==T){
		T=(S=STR)+fread(STR,1,LEN,stdin);
		if (S ==T)return EOF;
	}
	return *S++;
}
inline int read()
{
    int x=0;char ch=gc();
    for (;ch<'0'||ch>'9';ch=gc());
    for (;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
    return x;
}
void write(int x){
	if (x>=10)write(x/10);
	putchar(x%10+48);
}
void down(int x){
	int i=x;
	if (x*2<=l&&d[x*2]>d[x])i=x*2;
	if (x*2<l&&d[x*2+1]>d[i])i=x*2+1;
	if (i!=x){
		swap(d[x],d[i]);
		down(i);
	}
}
void up(int x){
	if (x==1)return;
	if (d[x/2]<d[x]){
		swap(d[x/2],d[x]);
		up(x/2);
	}
}
int main(){
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	n=read();int k=n,ans=0;
	for (int i=1;i<=n;i++){
		d[++l]=read();
		up(l);ans++;
		while (l&&d[1]==k)k--,ans--,d[1]=d[l--],down(1);
		write(ans);putchar(' ');
	}
	return 0;
}
