#include<bits/stdc++.h>
using namespace std;
#define int long long
const int LEN = 200005;
char STR[LEN],*S=STR,*T=STR;
inline char gc(){
	if (S==T){
		T=(S=STR)+fread(STR,1,LEN,stdin);
		if (S ==T)return EOF;
	}
	return *S++;
}
inline int read()
{
    int x=0;char ch=gc();
    for (;ch<'0'||ch>'9';ch=gc());
    for (;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
    return x;
}
const int N=1005;
int n,m,a[N][N],f[N][N],b[N][N];
signed main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)a[i][j]=read();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)b[i][j]=read();
	for (int i=1;i<=n;i++)
	 	for (int j=1;j<=m;j++)f[i][j]=f[i][j-1]+a[i][j];
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)a[i][j]=f[i][j]+a[i-1][j];
	for (int i=1;i<=n;i++)
	 	for (int j=1;j<=m;j++)f[i][j]=f[i][j-1]+b[i][j];
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)b[i][j]=f[i][j]+b[i-1][j];
	for (int i=1;i<=m;i++){
		int g=0;
		for (int j=1;j<=n;j++){
			g=max(g,f[j][i-1]-a[j][i]+b[j][i]-b[j][i-1]);			
			f[j][i]=a[j][i]+g;
		}
	}	
	printf("%lld",f[n][m]);	
	return 0;
}
