#include<bits/stdc++.h>
#define int long long
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
int n,m,sum,ans;
int A[1001],suma[1001],B[1001],sumb[1001];
int a[1001][1001],b[1001][1001],mp[1001][1001],flag[1001][1001];
int check(int x,int y){
	if(mp[x][y])
		return mp[x][y];
	if(x==0)
		return 2;
	if(y==0)
		return 1;
	if(flag[x][y]==2)
		return check(x-1,y);
	if(flag[x][y]==1)
		return check(x,y-1);
}
void search(int x,int y){
	if(x>n){
		sum=0;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				mp[i][j]=0;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++){
				mp[i][j]=check(i,j);
				if(mp[i][j]==1)
					sum+=a[i][j];
				else
					sum+=b[i][j];
			}
		ans=max(sum,ans);
		return;
	}
	flag[x][y]=1;
	y+1>m?search(x+1,1):search(x,y+1);
	flag[x][y]=2;
	y+1>m?search(x+1,1):search(x,y+1);
}
signed main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	read(n);
	read(m);
	if(n<=4&&m<=4){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				read(a[i][j]);
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				read(b[i][j]);
		search(1,1);
		cout<<ans;
		return 0;
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			read(sum);
			if(i==n-j+1)
				A[j]=sum;
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			read(sum);
			if(i==n-j+1)
				B[j]=sum;
		}
	suma[1]=A[1];
	for(int i=2;i<=n;i++)
		suma[i]=suma[i-1]+A[i];
	sumb[1]=B[1];
	for(int i=2;i<=n;i++)
		sumb[i]=sumb[i-1]+B[i];
	for(int i=0;i<=n;i++)
		ans=max(ans,suma[i]+sumb[n]-sumb[i]);
	cout<<ans;
	return 0;
}
