#include<bits/stdc++.h>
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
inline void write(int x){
	if(x<0)
		x=-x,putchar('-');
	if(x>9)
		write(x/10);
	putchar(x%10+48);
}
int n,x;
int a[300001];
map<int,int>mp;
int main(){
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	read(n);
	for(int i=1;i<=n;i++){
		read(a[i]);
		mp[a[i]]=1;
		x=n;
		while(mp[x])
			x--;
		if(!x)
			write(x);
		else
			write(i-n+x);
		putchar(' ');
	}
	return 0;
}
