#include<bits/stdc++.h>
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
inline void write(int x){
	if(x<0)
		x=-x,putchar('-');
	if(x>9)
		write(x/10);
	putchar(x%10+48);
}
inline void writeln(int x){
	write(x);
	putchar('\n');
}
int n,tot,op,Ans,max1,max2;
int a[501],ans[501];
int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	read(n);
	read(op);
	for(int i=1;i<=n+1;i++){
		read(a[i]);
		if(a[i]>=max2)
			if(a[i]>=max1){
				max2=max1;
				max1=a[i];
			}
			else
				max2=a[i];
		tot+=a[i];
	}
	for(int i=1;i<=n+1;i++){
		tot-=a[i];
		if((tot%2!=0)||(op==1&&tot>n*2)){
			tot+=a[i];
			continue;
		}
		if(a[i]==max1&&max2>tot/2){
			tot+=a[i];
			continue;
		}
		if(a[i]!=max1&&max1>tot/2){
			tot+=a[i];
			continue;
		}
		Ans++;
		ans[Ans]=i;
		tot+=a[i];
	}
	writeln(Ans);
	for(int i=1;i<=Ans;i++)
		writeln(ans[i]);
	return 0;
}
