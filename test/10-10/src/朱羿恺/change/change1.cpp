#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 3e5+10;
int n,x;
int sum[N<<2];
inline void update(int u,int l,int r,int ql){
	sum[u]++;
	if (l==r) return;int mid=l+r>>1;
	if (ql<=mid) update(u<<1,l,mid,ql);
		else update(u<<1^1,mid+1,r,ql);
}
inline int query(int u,int l,int r){
	if (sum[u]==r-l+1) return r-l+1;
	if (l==r) return 0;
	int mid=l+r>>1,ans=query(u<<1^1,mid+1,r);
	if (ans==r-mid) ans+=query(u<<1,l,mid);
	return ans;
}
int main(){
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	n=read();
	For(i,1,n) x=read(),update(1,1,n,x),printf("%d ",i-query(1,1,n));
}
