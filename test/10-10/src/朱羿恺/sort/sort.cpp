#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
using namespace std;
const int N = 1010;
int m,n;
ll a[N][N],b[N][N],suma[N][N],sumb[N][N],dp[N][N];
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
    n=read(),m=read();
    for(int i=1;i<=n;i++)
        for(int j=1;j<=m;j++) b[i][j]=read(),sumb[i][j]=b[i][j]+sumb[i][j-1];
    for(int i=1;i<=n;i++)
        for(int j=1;j<=m;j++) a[i][j]=read(),suma[i][j]=a[i][j]+suma[i-1][j];
    for(int i=n;i>=0;i--)
        for(int j=m;j>=0;j--) dp[i][j]=max(dp[i][j+1]+suma[i][j+1],dp[i+1][j]+sumb[i+1][j]);
    printf("%lld\n",dp[0][0]);
}
