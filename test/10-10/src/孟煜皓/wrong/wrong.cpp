#include <cstdio>
#include <algorithm>
#define N 505
int n, op, d[N], b[N], ans, s[N];
struct node{
	int d, id;
	bool operator < (const node &res) const {
		return d > res.d;
	}
}a[N];
void mergesort(int l, int mid, int r){
	int i = l, j = mid + 1, k = l;
	while (i <= mid && j <= r)
		if (d[i] >= d[j]) b[k++] = d[i++]; else b[k++] = d[j++];
	while (i <= mid) b[k++] = d[i++];
	while (j <= r) b[k++] = d[j++];
	for (i = l; i <= r; ++i) d[i] = b[i];
}
int main(){
	freopen("wrong.in", "r", stdin);
	freopen("wrong.out", "w", stdout);
	scanf("%d%d", &n, &op), ++n;
	for (register int i = 1; i <= n; ++i)
		scanf("%d", &a[i].d), a[i].id = i;
	if (op == 0){
		int sum = 0;
		for (register int i = 1; i <= n; ++i){
			sum = 0;
			for (register int j = 1; j <= n; ++j)
				if (j != i) sum += a[j].d;
			if (sum & 1) continue;
			int flag = 1;
			for (register int j = 1; j <= n; ++j)
				if (sum - a[j].d < a[j].d) { flag = 0; break; }
			if (flag) s[++ans] = i;
		}
		printf("%d\n", ans);
		for (register int i = 1; i <= ans; ++i) printf("%d\n", s[i]);
		return 0;
	}
	std :: sort(a + 1, a + 1 + n);
	for (register int i = 1; i <= n; ++i){
		int m = 0, flag = 1;
		for (register int j = 1; j <= n; ++j)
			if (j != i) d[++m] = a[j].d;
		for (register int j = 1, p; j <= m; ++j){
			for (register int k = j + 1; k <= m; ++k){
				if (!d[k]) break;
				--d[j], --d[k];
				if (!d[j]) { p = k; break; }
			}
			if (d[j]) { flag = 0; break; }
			mergesort(j + 1, p, m);
		}
		if (flag) s[++ans] = a[i].id;
	}
	std :: sort(s + 1, s + 1 + ans);
	printf("%d\n", ans);
	for (register int i = 1; i <= ans; ++i) printf("%d\n", s[i]);
}