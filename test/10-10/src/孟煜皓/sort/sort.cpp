#include <cstdio>
#include <cctype>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = !f;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 1005
int n, m, a[N][N], b[N][N];
long long dp[N][N];
int main(){
	freopen("sort.in", "r", stdin);
	freopen("sort.out", "w", stdout);
	n = read(), m = read();
	for (register int i = 1; i <= n; ++i)
		for (register int j = 1; j <= m; ++j)
			a[i][j] = a[i][j - 1] + read();
	for (register int i = 1; i <= n; ++i)
		for (register int j = 1; j <= m; ++j)
			b[i][j] = b[i - 1][j] + read();
	for (register int i = 1; i <= n; ++i)
		for (register int j = 1; j <= m; ++j)
			dp[i][j] = std :: max(dp[i - 1][j] + a[i][j], dp[i][j - 1] + b[i][j]);
	printf("%lld\n", dp[n][m]);
}