#include <algorithm>
#include <iostream>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);putchar('\n');}
int a[100000],b[100000],ans[1000000],top;
int main()
{
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	int n=read(),op=read(),sum=0;
	for(int i=1;i<=n+1;i++)a[i]=read(),sum+=a[i];
	if(!op){
		for(int i=1;i<=n+1;i++){
			if((sum-a[i])%2==0)ans[++top]=i;
		}
		wrn(top);
	    for(int i=1;i<=top;i++)wrn(ans[i]);
	    return 0;
	}
	for(int i=1;i<=n+1;i++){
		int tmp=0,sum=0,flag=1;
		for(int j=1;j<=n+1;j++){
			if(j!=i)b[++tmp]=a[j],sum+=a[j];
		}
		if(sum&1)continue;
//		cout<<"kk"<<i<<" ";for(int j=1;j<=tmp;j++)cout<<b[j]<<" ";puts("");cout<<sum<<endl<<endl;
		for(int j=1;j<=n;j++){
		    sort(b+1,b+1+tmp);
		    int t=n-1;
		    while(b[n]&&b[t]){
			    b[n]--;b[t]--;t--;
		    }
			if(b[n]){
				flag=0;break;
			}
	    }
	    sort(b+1,b+1+n);
	    if(flag&&!b[n])ans[++top]=i;
	}
	wrn(top);
	for(int i=1;i<=top;i++)wrn(ans[i]);
	return 0;
}
