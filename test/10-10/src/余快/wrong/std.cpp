/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 555
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,tot,z[N],a[N],op,id[N],cnt,c[N],tmp[N];
ll sum;
bool cmp(int x,int y){
	return a[x]<a[y];
}
void solve0(){
	F(i,1,n) id[i]=i;
	sort(id+1,id+n+1,cmp);
	if ((sum-a[id[1]])%2==0&&a[id[2]]<=sum-a[id[1]]-a[id[2]]) z[++tot]=id[1];
	F(i,2,n){
		if ((sum-a[id[i]])%2==0&&a[id[1]]<=sum-a[id[1]]-a[id[i]]) z[++tot]=id[i];
	}
	sort(z+1,z+tot+1);
	wrn(tot);
	F(i,1,tot) wrn(z[i]);
}
bool check(int x){
	if ((sum-a[x])&1) return 0;
	cnt=0;
	F(i,1,n){
		if (i!=x) c[++cnt]=a[i];
	}
	F(i,1,n-1){
		sort(c+1,c+cnt+1);
		if (cnt-c[cnt]<=0) return 0;
		F(j,cnt-c[cnt],cnt-1){
			c[j]--;
			if (c[j]<0) return 0;
		}
		cnt--;
	}
	return 1;
}
void solve1(){
	//���ر� 
	F(i,1,n){
		if (check(i)) z[++tot]=i;
	}
	wrn(tot);
	F(i,1,tot) wrn(z[i]);
}
signed main(){
	freopen("wrong.in","r",stdin);freopen("std.out","w",stdout);
	n=read()+1;op=read();
	F(i,1,n) sum+=a[i]=read();
	if (op==0) solve0();
	else solve1();
	return 0;
}
