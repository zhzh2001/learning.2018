#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("wrong.in", "r", stdin);
	freopen("wrong.out", "w", stdout);
}
const int N = 510;
int n, a[N], op, b[N];
bool ans[N];
bool cmp(int x, int y) {
	return x > y;
}
bool solve(int x){
	int m = 0;
	for(int i = 1; i <= n + 1; ++i)
		if(i != x) b[++m] = a[i];
	sort(b + 1, b + 1 + m, cmp);
	if(op == 0) {
		priority_queue<int>q;
		for(int i = 1;  i<= m; ++i)
			q.push(b[i]);
		while(!q.empty() && q.size() > 1) {
			int x = q.top();q.pop();
			int y = q.top(); q.pop();
			int t = min(x, y);
			x -= t;
			y -= t;
			if(x) q.push(x);
			if(y) q.push(y);
		}
		return q.empty();
	}
	for(int i = 1; i <= m; ++i)
			for(int j = i + 1; j <= m && b[i]; ++j)
				if(b[j]) {
					if(op) --b[j], --b[i];
					else {
						int t = min(b[j], b[i]);
						b[i] -= t;
						b[j] -= t;
					}
			}
	for(int i = 1; i <= m; ++i)
		if(b[i]) return 0;
	return 1;
}
int main() {
//	setIO();
	n = read(), op = read();
	for(int i = 1; i <= n + 1; ++i)
		a[i] = read();
	int res = 0;
	for(int i = 1; i <= n + 1; ++i)
		ans[i] = solve(i), res += ans[i];
	writeln(res);
	for(int i = 1; i <= n + 1; ++i)
		if(ans[i])
			writeln(i);
	return 0;
}
