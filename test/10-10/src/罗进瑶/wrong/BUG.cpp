#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define pii pair<int,int>
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("wrong.in", "r", stdin);
	freopen("wrong.out", "w", stdout);
}
const int N = 510;
int n, a[N], op, b[N];
bool ans[N], vis[N][N];
bool cmp(int x, int y) {
	return x > y;
}
struct T {
	multiset<int>v;
	int ned;
	bool operator <(const T &rhs) const {
		return ned < rhs.ned;
	}
};
void print(T J){
	multiset<int>::iterator it;
	it = J.v.begin();
	printf("%d %d\n", J.v.size(), J.ned);
	printf("v= ");
	while(it != J.v.end()) printf("%d ", *it), ++it;
	puts("");
} 
bool solve(int x){
	int m = 0;
	for(int i = 1; i <= n + 1; ++i)
		if(i != x) b[++m] = a[i];
	sort(b + 1, b + 1 + m, cmp);
	if(op == 0) {
		priority_queue<int>q;
		for(int i = 1;  i<= m; ++i)
			q.push(b[i]);
		while(!q.empty() && q.top() >0 && q.size() > 1) {
			int x = q.top();q.pop();
			int y = q.top(); q.pop();
			int t = min(x, y);
			x -= t;
			y -= t;
			if(x) q.push(x);
			if(y) q.push(y);
		}
		if(!q.empty() && q.top() == 0) return 0;
		return q.empty();
	}
	priority_queue<T>q;
	for(int i = 1; i <= m; ++i) if(b[i]){
		T now;
		now.v.insert(b[i]);
		now.ned = b[i];
		q.push(now);
	}
	while(!q.empty() && q.size() > 1 && q.top().ned > 0) {
		T x = q.top(); q.pop();
		T y = q.top(); q.pop();
		int t = min(min(x.ned, y.ned), (int)x.v.size() * (int)y.v.size());
		multiset<int>to;
		multiset<int>::iterator it;
		it = x.v.end();
		--it;
		int tot = 0;
		bool fg = 1;
		int rest = t;
		while(fg && rest > 0) {
			int val = min(rest, *it);
			if(*it > val)to.insert(*it - val);
			rest -= val;
			tot += *it - 1;
			if(it == x.v.begin()) fg = 0;
			--it;
		}
		while(fg) {
			to.insert(*it), tot += *it;
			if(it == x.v.begin()) fg = 0;
			--it;
		}
	/*	printf("to= ");
		it = to.begin();
		while(it != to.end()) printf("%d ", *it), ++it;*/
		it = y.v.end(); 
		--it;
		fg = 1;
		rest = t;
		while(fg && rest > 0) {
			int val = min(rest, *it);
			if(*it > val)to.insert(*it - val);
			tot += *it - 1;
			if(it == y.v.begin()) fg = 0;
			--it;
		}
		while(fg) {
			to.insert(*it),  tot += *it;
			if(it == y.v.begin()) fg = 0;	
			--it;
		}
		puts("NEW:");
		print(x);
		print(y);
		print((T){to, tot});
		printf("t=%d\n", t);
		puts("---");
		if(tot) q.push((T){to, tot});
	}
	return q.empty() || !q.empty() && q.top().ned == 0;
}
int main() {
//	setIO();
	n = read(), op = read();
	for(int i = 1; i <= n + 1; ++i)
		a[i] = read();
	writeln(solve(1));
/*	int res = 0;
	for(int i = 1; i <= n + 1; ++i)
		ans[i] = solve(i), res += ans[i];
	writeln(n + 1 - res);
	for(int i = 1; i <= n + 1; ++i)
		if(!ans[i])
			writeln(i);*/
	return 0;
}

