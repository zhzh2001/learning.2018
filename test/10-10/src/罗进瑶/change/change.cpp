#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("change.in", "r", stdin);
	freopen("change.out", "w", stdout);
}
const int N = 610000;
int n, a[N], t[N], b[N];
int p;
int solve(int x) {
	while(t[p]) --p;
	return x - (n - p);
}
int main() { 
	setIO();
	p = n = read();
	for(int i = 1; i <= n; ++i) {
		a[i] = read();
		t[a[i]] = 1;
		printf("%d ", solve(i));
	}
	return 0;
}
