#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("sort.in", "r", stdin);
	freopen("sort.out", "w", stdout);
}
const int N = 1100;
int n, m;
ll a[N][N], b[N][N];
ll s[N][N], t[N][N]; //s���� 
ll f[N][N];
ll gu(int x1, int y1, int x2, int y2) {
	return t[x2][y2] - t[x1 - 1][y2] - t[x2][y1 - 1] + t[x1 - 1][y1 - 1];
}
ll gl(int x1, int y1, int x2, int y2) {
	return s[x2][y2] - s[x1 - 1][y2] - s[x2][y1 - 1] + s[x1 - 1][y1 - 1];
}
ll dp(int x, int y) {
	if(x == 1 && y == 1) return max(a[1][1], b[1][1]);
	if(~f[x][y]) return f[x][y];
	f[x][y] = 0;	
	if(x == 1) {
		for(int i = 0; i <= y; ++i)
			f[x][y] = max(f[x][y], s[1][i] + t[1][y] - t[1][i]); 
		return f[x][y];
	}
	if(y == 1) {
		for(int i = 0; i <=  x; ++i)
			f[x][y] = max(f[x][y], t[i][1] + s[x][1] - s[i][1]);
		return f[x][y];
	}
	for(int i = x + 1; i >= 2; --i)
		for(int j = y; j >= 2; --j) {
			if(i == x + 1 && j == y + 1) continue;
			f[x][y] = max(f[x][y], 
			dp(i - 1, j - 1) + 
			gl(i, 1, x, j) + 
			gu(1, j, i - 1, y) +
			gu(i, j + 1, x, y)
			);			
		}

	return f[x][y];
}
int main() {
	setIO();
	n = read(), m = read();
	for(int i = 1; i <= n; ++i) {
		for(int j = 1; j <= m; ++j) {
			a[i][j] = read();
			s[i][j] = s[i - 1][j] + s[i][j - 1] - s[i - 1][j - 1] + a[i][j];
		}
	}
	for(int i = 1; i <= n; ++i) {
		for(int j = 1; j <= m; ++j) {
			b[i][j] = read();
			t[i][j] = t[i - 1][j] + t[i][j - 1] - t[i - 1][j - 1] + b[i][j];
		}
	}
	memset(f, -1, sizeof f);
	writeln(dp(n, m));
	return 0;
}
