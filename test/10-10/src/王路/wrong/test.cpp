#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T) return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 505;
int n, op, a[kMaxN], ans[kMaxN];
int main() {
  freopen("wrong.in", "r", stdin);
  freopen("test.out", "w", stdout);
  Read(n), Read(op);
  int zerocnt = 0, sum = 0, mx = 0, smx = 0;
  for (int i = 1; i <= n + 1; ++i) {
    Read(a[i]);
    sum += a[i];
    if (a[i] > a[mx]) {
      smx = mx;
      mx = i;
    } else if (a[i] > a[smx])
      smx = i;
    zerocnt += a[i] == 0;
  }
  for (int i = 1; i <= n + 1; ++i) {
    int res = sum - a[i];
    if (res % 2)
      continue;
    res >>= 1;
    int pmx = a[i == mx ? smx : mx];
    if (op == 1 && pmx >= n - zerocnt)
      continue;
    if (op == 1 && res >= n * (n - 1) / 2)
      continue;
    ans[++ans[0]] = i;
  }
  for (int i = 0; i <= ans[0]; ++i)
    printf("%d\n", ans[i]);
  return 0;
}