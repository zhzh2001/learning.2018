#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <random>
#include <Windows.h>
using namespace std;

static mt19937 gen(GetTickCount());

template <typename T> inline T UniformRandInt(T l, T r) {
  uniform_int_distribution<T> d(l, r);
  return d(gen);
}
template <typename T> inline T UniformRandReal(T l, T r) {
  uniform_real_distribution<T> d(l, r);
  return d(gen);
}
inline bool UniformRandBool(double ptrue = 0.5) {
  bernoulli_distribution d(ptrue);
  return d(gen);
}

const int kMaxN = 3005;

int main() {
  freopen("wrong.in", "w", stdout);
  int n = 500, op = UniformRandInt(0, 1);
  printf("%d %d\n", n, op);
  for (int i = 1; i <= n + 1; ++i)
    printf("%d%c", UniformRandInt(1, op ? n - 1 : n * n), " \n"[i == n + 1]);
  return 0;
}