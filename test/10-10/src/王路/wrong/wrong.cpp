#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <functional>
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T) return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 505;
int n, op, a[kMaxN], ans[kMaxN], b[kMaxN];
int main() {
  freopen("wrong.in", "r", stdin);
  freopen("wrong.out", "w", stdout);
  Read(n), Read(op);
  for (int i = 1; i <= n + 1; ++i)
    Read(a[i]);
  if (op == 0) {
    int sum = 0;
    for (int i = 1; i <= n + 1; ++i) {
      sum += a[i];
    }
    for (int i = 1; i <= n + 1; ++i) {
      int cur = sum - a[i];
      if (cur & 1)
        continue;
      ans[++ans[0]] = i;
    }
  } else {
    int sum = 0;
    for (int i = 1; i <= n + 1; ++i) {
      sum += a[i];
    }
    for (int i = 1; i <= n + 1; ++i) {
      int cur = sum - a[i];
      if (cur & 1)
        continue;
      int mx = 0, ava = 0;
      for (int j = 1; j <= n + 1; ++j) {
        if (i != j) {
          mx = max(mx, a[j]);
          ava += a[j] != 0;
        }
      }
      if (mx != 0 && mx > ava - 1)
        continue;
      if (cur / 2 > n * (n - 1) / 2)
        continue;
      int offset = 0;
      for (int j = 1; j <= n; ++j) {
        offset += i == j;
        b[j] = a[j + offset];
      }
      sort(b + 1, b + n + 1, greater<int>());
      bool can = false;
      // int l = 1, r = n;
      // for (; r > l; ) {
      //   for (int j = l + 1; j <= r; ++j) {
      //     if (b[l] > 0 && b[j] > 0)
      //       --b[l], --b[j];
      //   }
      //   if (b[l] > 0) {
      //     can = true;
      //     break;
      //   }
      //   while (b[l] == 0)
      //     ++l;
      //   while (b[r] == 0)
      //     --r;
      // }
      if (!can)
        ans[++ans[0]] = i;
    }
  }
  for (int i = 0; i <= ans[0]; ++i)
    printf("%d\n", ans[i]);
  return 0;
}