#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <functional>
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T) return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 505;
int n, op, ans[kMaxN], b[kMaxN];
pair<int, int> a[kMaxN];
int main() {
  freopen("wrong.in", "r", stdin);
  freopen("bf.out", "w", stdout);
  Read(n), Read(op);
  for (int i = 1; i <= n + 1; ++i) {
    Read(a[i].first);
    a[i].second = i;
  }
  sort(a + 1, a + n + 2, greater<pii>());
  if (op == 1) {
    for (int i = 1; i <= n + 1; ++i) {
      int offset = 0;
      for (int j = 1; j <= n; ++j) {
        offset += i == j;
        b[j] = a[j + offset].first;
      }
      int l = 1, r = n;
      bool can = false;
      for (; r > l; ) {
        for (int j = l + 1; j <= r; ++j) {
          if (b[l] > 0 && b[j] > 0)
            --b[l], --b[j];
        }
        if (b[l] > 0) {
          can = true;
          break;
        }
        while (b[l] == 0)
          ++l;
        while (b[r] == 0)
          --r;
      }
      for (int j = 1; j <= n; ++j)
        if (b[j] != 0)
          can = true;
      if (!can)
        ans[++ans[0]] = a[i].second;
    }
  } else {
    for (int i = 1; i <= n + 1; ++i) {
      int offset = 0;
      for (int j = 1; j <= n; ++j) {
        offset += i == j;
        b[j] = a[j + offset].first;
      }
      int l = 1, r = n;
      while (r > l) {
        b[l] -= min(b[r], b[l]);
        b[r] -= min(b[r], b[l]);
        if (b[l] == 0)
          ++l;
        if (b[r] == 0)
          --r;
      }
      bool can = false;
      for (int j = 1; j <= n; ++j)
        if (b[j] != 0)
          can = true;
      if (!can)
        ans[++ans[0]] = a[i].second;
    }
  }
  sort(ans + 1, ans + ans[0] + 1);
  for (int i = 0; i <= ans[0]; ++i)
    printf("%d\n", ans[i]);
  return 0;
}