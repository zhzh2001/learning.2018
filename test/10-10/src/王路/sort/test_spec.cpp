#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T) return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 1005;
const ll kInfL = 1LL << 60;
int n, m;
ll f[kMaxN][kMaxN], a[kMaxN][kMaxN], b[kMaxN][kMaxN], x;
int main() {
  freopen("sort.in", "r", stdin);
  freopen("test.out", "w", stdout);
  Read(n), Read(m);
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= m; ++j) {
      Read(x);
      a[i][j] = a[i - 1][j] + a[i][j - 1] - a[i - 1][j - 1] + x;
    }
  }
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= m; ++j) {
      Read(x);
      b[i][j] = b[i - 1][j] + b[i][j - 1] - b[i - 1][j - 1] + x;
    }
  }
  ll ans = 0;
  for (int i = 1; i <= n; ++i) {
    // ans = max(ans, b[i][n] + a[n][n] - a[i][n]);
    if (b[i][n] + a[n][n] - a[i][n] > ans) {
      ans = b[i][n] + a[n][n] - a[i][n];
      printf("%d\n", i);
    }
  }
  printf("%lld\n", ans);
  return 0;
}