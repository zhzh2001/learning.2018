#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <random>
#include <Windows.h>
using namespace std;

static mt19937 gen(GetTickCount());

template <typename T> inline T UniformRandInt(T l, T r) {
  uniform_int_distribution<T> d(l, r);
  return d(gen);
}
template <typename T> inline T UniformRandReal(T l, T r) {
  uniform_real_distribution<T> d(l, r);
  return d(gen);
}
inline bool UniformRandBool(double ptrue = 0.5) {
  bernoulli_distribution d(ptrue);
  return d(gen);
}

const int kMaxN = 3005;
bool vis[kMaxN];

int main() {
  freopen("change.in", "w", stdout);
  int n = 20;
  printf("%d\n", n);
  for (int i = 1; i <= n; ++i) {
    int t;
    do {
      t = UniformRandInt(1, n);
    } while (vis[t]);
    vis[t] = true;
    printf("%d%c", t, " \n"[i == n]);
  }
  return 0;
}