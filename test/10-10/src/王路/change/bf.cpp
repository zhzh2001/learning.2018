#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T) return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 3005;
int n, a[kMaxN];
bool vis[kMaxN];
int main() {
  freopen("change.in", "r", stdin);
  freopen("bf.out", "w", stdout);
  Read(n);
  for (int i = 1; i <= n; ++i) {
    Read(a[i]); // a[i] 是个排列
  }
  for (int i = 1; i <= n; ++i) {
    sort(a + 1, a + i + 1);
    memset(vis, 0x00, sizeof vis);
    for (int j = 1; j <= i; ++j) {
      vis[a[j]] = true;
    }
    int l = 1, r = n;
    int ans = 0;
    while (l < r) {
      while (vis[r])
        --r;
      if (l < r && vis[l] && !vis[r]) {
        // fprintf(stderr, "%d %d\n", l, r);
        ++ans;
        vis[l] = false;
      }
      ++l;
    }
    printf("%d%c", ans, " \n"[i == n]);
  }
  return 0;
}