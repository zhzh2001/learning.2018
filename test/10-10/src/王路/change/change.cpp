#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T) return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;
typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 3e5 + 5;
int n, a[kMaxN], f[kMaxN];
bool vis[kMaxN];
int main() {
  freopen("change.in", "r", stdin);
  freopen("change.out", "w", stdout);
  Read(n);
  for (int i = 1; i <= n; ++i) {
    Read(a[i]);
  }
  vis[a[1]] = true;
  int l = 0, r = n, ans = 0;
  for (int i = 1; i <= n; ++i) {
    vis[a[i]] = true;
    for (; vis[r]; --r, --ans)
      ;
    ++ans;
    printf("%d%c", ans, " \n"[i == n]);
  }
  return 0;
}