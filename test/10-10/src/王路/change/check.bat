@echo off
for /L %%i in (1, 1, 10000) do (
	echo %%i:
	mkd.exe
	bf.exe
	change.exe
	fc bf.out change.out
	if errorlevel == 1 pause
)