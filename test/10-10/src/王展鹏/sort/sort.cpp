#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define int long long
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=1005;
int dq,n,m,ans,a[N][N],b[N][N],s1[N][N],s2[N][N],dp[N][N];
signed main(){
	freopen("sort.in","r",stdin); freopen("sort.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++)a[i][j]=read();
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++)b[i][j]=read();
	}
	for(int i=1;i<=n;i++){
		for(int j=m;j;j--)s1[i][j]=s1[i][j+1]+b[i][j];
		for(int j=1;j<=m;j++)s2[i][j]=s2[i][j-1]+a[i][j];
	}
	for(int i=1;i<=n;i++){
		dq=0;
		for(int j=1;j<=m+1;j++){
			dq=max(dq,dp[i-1][j]);
			dp[i][j]=dq+s1[i][j]+s2[i][j-1];
		}
	}
	for(int i=1;i<=m+1;i++)ans=max(ans,dp[n][i]);
	cout<<ans<<endl;
}
