#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
inline ll read(){
	ll x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(ll a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(ll a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=505;
int n,a[N],tong[N],jb[N];
vector<int> ans;
void solve1(){
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++){
		ll sum=0;
		int mx=0;
		for(int j=1;j<=n;j++)if(i!=j){
			sum+=a[j]; mx=max(mx,a[j]);
		}
		if(mx<=sum-mx&&sum%2==0)ans.push_back(i);
	}
	cout<<ans.size()<<endl;
	for(unsigned i=0;i<ans.size();i++)cout<<ans[i]<<endl;
}
void solve2(){
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++){
		memset(tong,0,sizeof(tong));
		int sum=0;
		for(int j=1;j<=n;j++)if(i!=j){
			tong[a[j]]++; sum+=a[j];
		}
		if(sum&1)continue;
		int flag=0; 
		for(int i=n;i&&flag==0;i--)while(tong[i]){
			tong[i]--;
			int t=i;
			memset(jb,0,sizeof(jb));
			for(int j=i;j;j--)if(tong[j]>=t){
				tong[j]-=t; jb[j-1]=t; t=0; break;
			}else{
				t-=tong[j]; jb[j-1]=tong[j]; tong[j]=0;
			}
			for(int j=1;j<i;j++)tong[j]+=jb[j];
			//cout<<i<<" "<<t<<" "<<tong[1]<<" "<<tong[2]<<endl;
			if(t){
				flag=1; break;
			}
		}
		if(!flag)ans.push_back(i);
	}
	cout<<ans.size()<<endl;
	for(unsigned i=0;i<ans.size();i++)cout<<ans[i]<<endl;
}
int main(){
	freopen("wrong.in","r",stdin); freopen("wrong.out","w",stdout);
	n=read()+1; int op=read();
	if(op==0)solve1();
	else solve2();
}
