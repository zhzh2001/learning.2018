#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 511; 
int n, opt, num;
LL all;  
int a[N], ans[N], tmp[N]; 

inline bool cmp(int a, int b) {
	return a > b; 
}

inline bool check() {
	return 1; 
}

int main() {
	freopen("wrong.in", "r", stdin); 
	freopen("wrong.out", "w", stdout); 
	n = read(); opt = read(); 
	For(i, 1, n+1) a[i] = read(); 
	For(i, 1, n+1) {
		int tot = 0; 
		all = 0;  
		For(j, 1, n+1) 
			if(i==j) continue; 
			else tmp[++tot] = a[j], all += a[j];
		if(all%2==1) continue; 
		sort(tmp+1, tmp+tot+1, cmp); 
		if(check()) ans[++num] = i; 
 	}
 	writeln(num); 
 	For(i, 1, num) writeln(ans[i]);   
}

