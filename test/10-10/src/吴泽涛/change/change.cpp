#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 300011; 
int n, R, ans; 
int a[N]; 
int tree[N]; 

inline int lowbit(int x) {
	return x&(-x); 
}

inline void add(int x) {
	for(; x<=n; x+=lowbit(x)) 
		++tree[x];
}

inline int query(int x) {
	int res = 0; 
	for(; x; x-=lowbit(x)) 
		res += tree[x]; 
	return res; 
}

int main() {
	freopen("change.in", "r", stdin); 
	freopen("change.out", "w", stdout); 
	n = read(); 
	R = n; 
	For(i, 1, n) {         
		int x = read(); 
		a[x] = 1; 
		if(x==R) while(a[R]) --R;  
		add(x); 
		ans = query(R);
		write(ans); putchar(' ');  
	}
}



/*

4
1 3 4 2

*/






