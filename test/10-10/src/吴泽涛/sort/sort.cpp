#include<bits/stdc++.h>
#define LL long long
#define GG long long 
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 1011; 
int n, m; 
LL sum1[N][N], sum2[N][N], sum[N][N]; 

int main() {
	freopen("sort.in", "r", stdin); 
	freopen("sort.out", "w", stdout); 
	n = read(); m = read(); 
	For(i, 1, n) 
		For(j, 1, m) 
			sum1[i][j] = sum1[i-1][j]-sum1[i-1][j-1]+sum1[i][j-1]+read(); 
	For(i, 1, n) 
		For(j, 1, m) 
			sum2[i][j] = sum2[i-1][j]-sum2[i-1][j-1]+sum2[i][j-1]+read();
	For(i, 1, n) 
		For(j, 1, m) 
			sum[i][j]=max(sum[i][j-1]-sum2[i][j-1]+sum2[i][j],sum[i-1][j]-sum1[i-1][j]+sum1[i][j]);
	writeln(sum[n][m]); 
}

