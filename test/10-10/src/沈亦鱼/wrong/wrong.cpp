#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,an,a[510],ans[510];
long long s,ma1,ma2;
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	n=read()+1;
	m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		if(a[i]>ma1){
			ma2=ma1;
			ma1=a[i];
		}
		else if(a[i]>ma2)ma2=a[i];
		s+=a[i];
	}
	if(s&1==1){
		for(int i=1;i<=n;i++)
			if(a[i]%2==1){
				if(a[i]==ma1){
					if(ma2<=s-ma1-ma2){
						an++;
						ans[an]=i;
					}
				}
				else if(ma1<=s-ma1-a[i]){
						an++;
						ans[an]=i;
					}
			}
	}
	else{
		for(int i=1;i<=n;i++)
			if(a[i]%2==0){
				if(a[i]==ma1){
					if(ma2<=s-ma1-ma2){
						an++;
						ans[an]=i;
					}
				}
				else if(ma1<=s-ma1-a[i]){
						an++;
						ans[an]=i;
					}
			}
	}
	printf("%d\n",an);
	for(int i=1;i<=an;i++)
		printf("%d\n",ans[i]);
	return 0;
}
