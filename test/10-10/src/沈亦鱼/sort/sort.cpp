#include<cstdio>
#include<algorithm>
using namespace std;
int n,m;
long long a[1100][1100],b[1100][1100],f[1100][1100];
inline long long read(){
	long long ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			b[i][j]=read();
	for(int i=n;i>0;i--)
		for(int j=m;j>0;j--)
			a[i][j]+=a[i+1][j];
	for(int i=n;i>0;i--)
		for(int j=m;j>0;j--)
			b[i][j]+=b[i][j+1];
	for(int i=n;i>0;i--)
		for(int j=m;j>0;j--)
			f[i][j]=max(f[i][j+1]+a[i][j],f[i+1][j]+b[i][j]);
	printf("%lld",f[1][1]);
	return 0;
}
