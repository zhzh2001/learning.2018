#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,nn,mm;
long long s,ans,a[1100][1100],b[1100][1100],fa[1100][1100],fb[1100][1100],f[1100][1100],ff[1100][1100];
inline long long read(){
	long long ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void dfs(int n,int m){
	if(m==0){
		n--;
		m=mm;
		if(n==0){
			if(s>ans){
				ans=s;
				for(int i=1;i<=nn;i++)
					for(int j=1;j<=mm;j++)
						ff[i][j]=f[i][j];
			}
			return;
		}
	}
	fa[n-1][m]=a[n-1][m]+fa[n][m];
	fb[n-1][m]=b[n-1][m]+fb[n][m];
	if(n-1==0)s+=fb[n-1][m];
	f[n][m]=1;
	dfs(n,m-1);
	if(n-1==0)s-=fb[n-1][m];
	fa[n-1][m]=a[n-1][m];
	fb[n-1][m]=b[n-1][m];
	fa[n][m-1]=a[n][m-1]+fa[n][m];
	fb[n][m-1]=b[n][m-1]+fb[n][m];
	if(m-1==0)s+=fa[n][m-1];
	f[n][m]=2;
	dfs(n,m-1);
	if(m-1==0)s-=fa[n][m-1];
	fa[n][m-1]=a[n][m-1];
	fb[n][m-1]=b[n][m-1];
}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort_.out","w",stdout);
	n=read();
	m=read();
	nn=n;
	mm=m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			fa[i][j]=a[i][j]=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			fb[i][j]=b[i][j]=read();
	ans=0;
	dfs(n,m);
	printf("%lld\n",ans);
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++)
			printf("%d ",ff[i][j]);
		puts("");
	}
	return 0;
}
