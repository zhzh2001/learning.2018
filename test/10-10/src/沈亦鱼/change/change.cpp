#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,k,a[310000];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void up(int i){
	while(i>1){
		if(a[i]>a[i>>1])swap(a[i],a[i>>1]);
		else return;
		i>>=1;
	}
}
void down(int i){
	int j=0;
	while(i*2<=k){
		if(a[i*2+1]<a[i*2])j=i*2;
		else j=i*2+1;
		if(a[i]<a[j])swap(a[i],a[j]);
		else return;
		i=j;
	}
}
int main(){
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	n=read();
	m=n+1;
	k=0;
	for(int i=1;i<=n;i++){
		k++;
		a[k]=read();
		a[k+1]=0;
		up(k);
		while(k>0&&a[1]==m-1){
			m--;
			a[1]=a[k];
			a[k]=-1;
			k--;
			down(1);
		}
		printf("%d ",k);
	}
	return 0;
}
