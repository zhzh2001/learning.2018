#include<bits/stdc++.h>
#define N 300005
#define ll long long
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
int n, f[N], a[N];
inline int find(int x){
  if (x == f[x]) return x;
  return (f[x] = find(f[x]));
}
int main() { 
  freopen("change.in","r",stdin);
  freopen("change.out","w",stdout);
  n = read(); 
  for (int i = 1; i <= n+1; i++) f[i] = i;
  for (int i = 1; i <= n; i++)
    a[i] = read();
  for (int i = 1; i <= n; i++) {
    int x = a[i];
    f[x] = find(x-1);
    int p = find(n);
    printf("%d ", i - (n-p));
  }
  return 0;
}