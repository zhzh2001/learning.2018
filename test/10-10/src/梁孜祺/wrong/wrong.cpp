#include<bits/stdc++.h>
#define N 505
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
int n, op, in[N], ans[N], f[25005];
set <int> s;
inline void doit(int T) {
  int sum = 0;;
  for (int i = 1; i <= n; i++)
    if (T != i) sum += in[i];
  if (sum&1) {
    ans[T] = 1;
    return;
  }
  s.clear();
  for (int i = 1; i <= n; i++)
    if (i != T) s.insert(-in[i]);
  for (int i = 1; i <= n; i++) {
    int x = -(*s.rbegin());
    s.erase(x);
    set <int>::iterator it;
    int j;
    for (j = 1,it = s.begin(); j <= x&&it != s.end(); it++, j++) {
      int y = *it;
      y--;
      s.erase(it);
      s.insert(y);
    }
    if (j <= x) {
      ans[T] = 1;
      return;
    } 
  }
}
int main() {
  freopen("wrong.in","r",stdin);
  freopen("wrong.out","w",stdout);
  n = read()+1;
  op = read();
  for (int i = 1; i <= n; i++) in[i] = read();
  sort(in+1,in+1+n);
  for (int T = 1; T <= n; T++) {
    if (!op) {
      int sum = 0;
      for (int j = 1; j <= n; j++)
        if (j != T) sum += in[j];
      if (sum&1) {
        ans[T] = 1;
        continue;
      }
      memset(f, 0, sizeof f);
      f[0] = 1; int mx = 0;
      for (int j = 1; j <= n; j++) 
        if (j != T) 
          for (int k = mx; k >= 0; k--)
            if (f[k]) {
              f[k+in[j]]|=f[k];
              mx = max(mx, k+in[j]);
            }
      if (!f[sum/2]) ans[T] = 1;
    } else doit(T);
  }
  int tot = 0;
  for (int i = 1; i <= n; i++)
    if (ans[i]) tot++;
  printf("%d\n", tot);
  for (int i = 1; i <= n; i++)
    if (ans[i]) printf("%d\n", i);
  return 0;
}