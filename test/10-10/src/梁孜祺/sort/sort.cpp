#include<bits/stdc++.h>
#define N 1005
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
int n, m;
long long a[N][N], b[N][N], f[N][N];
signed main() {
  freopen("sort.in","r",stdin);
  freopen("sort.out","w",stdout);
  n = read(); m = read();
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= m; j++)
      a[i][j] = read(), a[i][j] += a[i][j-1];
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= m; j++)
      b[i][j] = read(), b[i][j] += b[i-1][j];
  for (int i = 1; i <= n; i++) 
    for (int j = 1; j <= m; j++)
      f[i][j] = max(f[i-1][j]+a[i][j],f[i][j-1]+b[i][j]);
  printf("%lld\n",f[n][m]);
  return 0;
}
/*
f[i][j] = f[i-1][j-1] + max(b[i][j] + a[i][j-1], a[i][j] + b[i-1][j])

*/