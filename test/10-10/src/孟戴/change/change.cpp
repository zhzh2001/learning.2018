//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#define max(a,b) (a>b)?a:b
#define min(a,b) (a<b)?a:b
using namespace std;
ifstream fin("change.in");
ofstream fout("change.out");
char STR[200005],*S=STR,*T=STR;
inline char gc(){
	if(S==T){
		T=(S=STR)+fread(STR,1,200005,stdin);
		if(S==T)return EOF;
	}
	return *S++;
}
inline long long read(){
	long long x=0,f=1;char ch=gc();
	for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
	return x*f;
}
long long n,ans,pos[300003];
long long yes[300003];
long long fa[300003],mn[300003],mx[300003];
inline long long find(long long x){
	if(fa[x]==x){
		return x;
	}
	return fa[x]=find(fa[x]);
}
int main(){
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		pos[i]=read();
		fa[i]=i;
		mn[fa[i]]=mx[fa[i]]=i;
	}
	int num=0;
	for(int i=1;i<=n;i++){
		int x=find(pos[i]),y=fa[x];
		if(yes[pos[i]-1]){
			x=find(pos[i]),y=find(pos[i]-1);
			mn[x]=min(mn[y],mn[x]);
			fa[y]=x;
		}
		if(yes[pos[i]+1]){
			x=find(pos[i]),y=find(pos[i]+1);
			mx[x]=max(mx[y],mx[x]);
			fa[y]=x;
		}
		if(mx[x]==n){
			num=n-mn[x]+1;
		}
		fout<<i-num<<" ";
		yes[pos[i]]=1;
	}
	return 0;
}
/*

in:
8
6 8 3 4 7 2 1 5

out:
1 1 2 3 2 3 4 0

*/
