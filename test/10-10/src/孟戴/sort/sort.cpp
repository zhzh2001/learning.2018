//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#define max(a,b) (a>b)?a:b
#define min(a,b) (a<b)?a:b
using namespace std;
ifstream fin("sort.in");
ofstream fout("sort.out");
char STR[200005],*S=STR,*T=STR;
inline char gc(){
	if(S==T){
		T=(S=STR)+fread(STR,1,200005,stdin);
		if(S==T)return EOF;
	}
	return *S++;
}
inline long long read(){
	long long x=0,f=1;char ch=gc();
	for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
	return x*f;
}
long long n,m,mp1[1003][1003],mp2[1003][1003],dp[1003][1003];
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			mp1[i][j]=read();
			mp1[i][j]+=mp1[i-1][j]+mp1[i][j-1]-mp1[i-1][j-1];
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			mp2[i][j]=read();
			mp2[i][j]+=mp2[i-1][j]+mp2[i][j-1]-mp2[i-1][j-1];
		}
	}
	for(int i=n;i>=0;i--){
		for(int j=m;j>=0;j--){
			dp[i][j]=max(dp[i][j],dp[i+1][j]+mp1[i+1][j]-mp1[i][j]);
			dp[i][j]=max(dp[i][j],dp[i][j+1]+mp2[i][j+1]-mp2[i][j]);
		}
	}
	fout<<dp[0][0]<<endl;
	return 0;
}
