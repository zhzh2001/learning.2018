//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("wrong.in");
ofstream fout("wrong.out");
char STR[200005],*S=STR,*T=STR;
inline char gc(){
	if(S==T){
		T=(S=STR)+fread(STR,1,200005,stdin);
		if(S==T)return EOF;
	}
	return *S++;
}
inline long long read(){
	long long x=0,f=1;char ch=gc();
	for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
	return x*f;
}
long long n,op,tot,a[503],num[503],ans[503],t[503];
inline bool cmp(int a,int b){
	return a>b;
}
inline void solve(){
	tot=0;
	long long sum=0,mx1=0,mx2=0;
	for(int i=1;i<=n+1;i++){
		sum=sum+a[i];
		if(a[i]>mx1){
			mx2=mx1,mx1=a[i];
		}else{
			mx2=max(mx2,a[i]);
		}
	}
	for(int i=1;i<=n+1;i++){
		if(a[i]==mx1){
			if(a[i]%2==sum%2&&mx2<=sum-mx1){
				ans[++tot]=i;
			}
		}else{
			if(a[i]%2==sum%2&&mx1<=sum-a[i]){
				ans[++tot]=i;
			}
		}
	}
	fout<<tot<<endl;
	for(int i=1;i<=tot;i++){
		fout<<ans[i]<<endl;
	}
}
inline void solve2(){
	int len=0;
	for(int i=1;i<=n+1;i++){
		for(int j=1;j<=n+1;j++){
			if(j!=i){
				num[++len]=a[j];
			}
		}
		sort(num+1,num+n+1,cmp);
		while(num[1]>0){
			for(int j=2;j<=num[1]+1;j++){
				num[j]--;
			}
			len=0;
			int l1=2,l2=num[1]+2,r1=num[1]+1,r2=n;
			num[1]=0;
			while(l1<=r1&&l2<=r2){
				if(num[l1]<=num[l2]){
					t[++len]=num[l2],l2++;
				}else{
					t[++len]=num[l1],l1++;
				}
			}
			while(l1<=r1){
				t[++len]=num[l1],l1++;
			}
			while(l2<=r2){
				t[++len]=num[l2],l2++;
			}
			for(int j=1;j<=n;j++){
				num[j]=t[j];
            }
		}
		sort(num+1,num+n+1,cmp);
		if(num[n]==0){
        	ans[++tot]=i;
		}
    }
    fout<<tot<<endl;
    for(int i=1;i<=tot;i++){
    	fout<<ans[i]<<endl;
	}
}
int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
    n=read(),op=read();
    for(int i=1;i<=n+1;i++){
    	a[i]=read();
    }
    if(op==0){
    	solve();
	}else{
		solve2();
	}
	return 0;
}
