#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstring>
#include<cmath>
#include<algorithm>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 1010
using namespace std;
LL up[N][N],le[N][N],f[N][N];
LL n,m,i,j,k,l,r;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n)
		rep(j,1,n)
			le[i][j]=le[i][j-1]+read();
	rep(i,1,n)
		rep(j,1,m)
			up[i][j]=up[i-1][j]+read();
	rep(i,1,n)
		rep(j,1,m)
			f[i][j]=max(f[i-1][j]+le[i][j],f[i][j-1]+up[i][j]);
	printf("%lld",f[n][m]);
	return 0;
}
