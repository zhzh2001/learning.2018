#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstring>
#include<cmath>
#include<algorithm>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 500010
using namespace std;
LL l[N],r[N],boo[N];
LL n,m,i,j,k,ans;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
void f(LL lx,LL rx,LL ly,LL ry)
{
	LL len=ry-lx+1;
	l[lx]=len; r[ry]=len;
	if(ry==n) ans-=rx-lx+1;
}
int main()
{
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	n=read(); ans=0;
	rep(i,1,n)
	{
		k=read(); ans++;
		l[k]=1; r[k]=1; boo[k]=1;
		if(k==n) ans--;
		if(boo[k+1]) f(k-r[k]+1,k+l[k]-1,k+1,k+l[k+1]);
		if(boo[k-1]) f(k-r[k-1],k-1,k-r[k]+1,k+l[k]-1);
		printf("%lld ",ans);
	}
	return 0;
}
