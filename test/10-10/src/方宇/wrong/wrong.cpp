#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstring>
#include<cmath>
#include<algorithm>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 1010
using namespace std;
LL a[N],b[N],c[N],ans[N];
LL n,m,i,j,k,l,r,sum,s,maxx,boo;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int main()
{
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	n=read(); m=read(); sum=0;
	rep(i,1,n+1) a[i]=read();
	if(m)
	{
		rep(i,1,n+1)
		{
			rep(j,1,n) b[i]=0;
			l=0; r=0; s=0; boo=1;
			rep(j,1,n+1)
				if(i!=j) b[a[j]]++,r=max(r,a[j]),s+=a[j];
			if(s%2==1) continue;
			rep(j,1,n)
			{
				rep(k,0,r) c[k]=0;
				while(!b[r]) r--;
				b[r]--; l=r; k=r;
				while(k)
				{
					while(l&&!b[l]) l--;
					if(!l)
					{
						boo=0;
						break;
					}
					b[l]--; c[l-1]++; k--;
				}
				if(!l) break;
				rep(k,0,r) b[k]+=c[k];
			}
			if(boo) ans[++sum]=i;
		}
	}
	else
	{
		rep(i,1,n+1)
		{
			s=0; maxx=0;
			rep(j,1,n+1)
				if(i!=j)
				{
					s+=a[j];
					maxx=max(maxx,a[j]);
				}
			if(maxx<=s-maxx&&s%2==0) ans[++sum]=i;
		}
	}
	printf("%lld\n",sum);
	rep(i,1,sum) printf("%lld\n",ans[i]);
	return 0;
}
