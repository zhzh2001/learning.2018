#include<bits/stdc++.h>
using namespace std;
const int N=505;
int a[N],sum,f[N],n,opt,ans;
bool b[N];
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline bool pd0(int x)
{
	int p=sum-a[x];
	if ((p&1)==1)
		return false;
	int mx=0;
	for (int i=1;i<x;++i)
		mx=max(mx,a[i]);
	for (int i=x+1;i<=n+1;++i)
		mx=max(mx,a[i]);
	if (mx*2>sum)
		return false;
	return true;
}
inline void work0()
{
	for (int i=1;i<=n+1;++i)
		sum+=a[i];
	ans=0;
	for (int i=1;i<=n+1;++i)
	{
		b[i]=pd0(i);
		if (b[i])
			++ans;
	}
	printf("%d\n",ans);
	for (int i=1;i<=n+1;++i)
		if (b[i])
			printf("%d\n",i);
}
inline bool pd1(int x)
{
	if ((sum&1)!=(a[x]&1))
		return false;
	for (int i=1;i<n;++i)
		f[i]=0;
	for (int i=1;i<x;++i)
		++f[a[i]];
	for (int i=x+1;i<=n+1;++i)
		++f[a[i]];
	for (int i=n-1;i>=1;--i)
		while (f[i])
		{
			--f[i];
			int tot=i;
			int p=0,q=0;
			for (int j=i;j;--j)
			{	
				q=min(tot,f[j]);
				f[j]-=q;
				f[j]+=p;
				p=q;
				tot-=q;
				if (!tot)
				{
					f[j-1]+=q;
					break;
				}
			}
			if (tot)
			{
				// for (int i=1;i<n;++i)
				// 	cout<<f[i]<<' ';
				// puts("");
				return false;
			}
		}
	return true;
}
inline void work1()
{
	for (int i=1;i<=n+1;++i)
		sum+=a[i];
	ans=0;
	for (int i=1;i<=n+1;++i)
	{
		b[i]=pd1(i);
		if (b[i])
			++ans;
	}
	printf("%d\n",ans);
	for (int i=1;i<=n+1;++i)
		if (b[i])
			printf("%d\n",i);
}
int main()
{
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	read(n);
	read(opt);
	for (int i=1;i<=n+1;++i)
		read(a[i]);
	if (opt==0)
		work0();
	else
		work1();
	return 0;
}