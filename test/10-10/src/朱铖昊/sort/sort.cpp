#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
inline char gc()
{
	if(S == T)
	{
		T = (S = STR) + fread(STR,1,LEN,stdin);
		if(S == T) 
			return EOF;
	}
	return *S++;
}
inline void read(int &x)
{
	char c=gc();
	while (c>'9'||c<'0')
		c=gc();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=gc();
	}
}
inline void read(ll &x)
{
	char c=gc();
	while (c>'9'||c<'0')
		c=gc();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=gc();
	}
}
const int N=1005;
int n,m;
ll a[N][N],b[N][N],f[N][N],ans;
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
			read(a[i][j]);
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
			read(b[i][j]);
	for (int i=1;i<=n;++i)
	{
		ll p=0,mx=0;
		for (int j=1;j<=m;++j)
			p+=b[i][j];
		for (int j=0;j<=m;++j)
		{
			p-=b[i][j];
			p+=a[i][j];
			mx=max(mx,f[i-1][j]);
			f[i][j]=mx+p;
		}
	}
	ans=0;
	for (int i=0;i<=m;++i)
		ans=max(ans,f[n][i]);
	cout<<ans;
	return 0;
}