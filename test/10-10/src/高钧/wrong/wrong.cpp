#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define int long long
const int N=505;
int n,op,a[N],b[N],re[N],ou=0;
inline bool cmp(int x,int y){return x>y;}
inline bool jud(int x)
{
	int i,j; memmove(b,a,sizeof b); b[x]=0; sort(b+1,b+n+1,cmp);
	for(i=1;i<=n;i++)
	{
		if(!b[i])continue; for(j=i+1;j<=n;j++)if(b[j]>0){b[i]--;b[j]--;}if(b[i]){return 1;}
	}return 0;
}
signed main()
{
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	int i,S=0,bo; scanf("%lld%lld",&n,&op); n++; for(i=1;i<=n;i++)scanf("%lld",&a[i]),S+=a[i]; memset(re,0,sizeof re);
	if(op)
	{
		if(n<=300)
		{
			bo=S&1LL; if(bo){for(i=1;i<=n;i++)if(a[i]&1){re[i]=1;ou++;}}else{for(i=1;i<=n;i++)if((!(a[i]&1))&&(!jud(i))){re[i]=1;ou++;}}
		}
		else
		{
			bo=S%2LL; if(bo){for(i=1;i<=n;i++)if(a[i]%2){re[i]=1;ou++;}}else{for(i=1;i<=n;i++)if(!(a[i]&1)){re[i]=1;ou++;}}
		}
	}
	else
	{
		bo=S%2LL; if(bo){for(i=1;i<=n;i++)if(a[i]%2){re[i]=1;ou++;}}else{for(i=1;i<=n;i++)if(!(a[i]%2)){re[i]=1;ou++;}}
	}printf("%lld\n",ou);for(i=1;i<=n;i++)if(re[i])printf("%lld\n",i); return 0;
}
