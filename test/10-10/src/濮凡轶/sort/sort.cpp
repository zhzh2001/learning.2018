#include <cstdio>
#include <cctype>
#include <algorithm>

using namespace std;

const int maxn = 1005;

typedef long long LL;

#define dd c = getchar()
inline void read(LL& x)
{
	x = 0;
	char dd;
	bool flag = false;
	for(; !isdigit(c); dd)
		if(c == '-')
			flag = true;
	for(; isdigit(c); dd)
		x = (x << 1) + (x << 3) + (c ^ 48);
	if(flag)
		x = -x;
}
#undef dd

LL dp[maxn][maxn];
LL shang[maxn][maxn];
LL zuo[maxn][maxn];
LL qz[maxn][maxn];

int main()
{
	freopen("sort.in", "r", stdin);
	freopen("sort.out", "w", stdout);
	int n, m;
	scanf("%d%d", &n, &m);
	for(int i = 1; i <= n; ++i)
	{
		for(int j = 1; j <= m; ++j)
		{
			read(zuo[i][j]);
			zuo[i][j] += zuo[i][j-1];
		}
	}
	for(int i = 1; i <= n; ++i)
	{
		for(int j = 1; j <= m; ++j)
			read(shang[i][j]);
		for(int j = m; j; --j)
			shang[i][j] += shang[i][j+1];
	}
	for(int i = 1; i <= n; ++i)
		for(int j = 0; j <= m; ++j)
			qz[i][j] = zuo[i][j] + shang[i][j+1];
	for(int i = 1; i <= n; ++i)
	{
		dp[i][0] = dp[i-1][0] + qz[i][0];
		for(int j = 1; j <= m; ++j)
		{
			dp[i][j] = dp[i-1][j] + qz[i][j];
			dp[i][j] = max(dp[i][j-1], dp[i][j]);
		}
	}
	printf("%lld", dp[n][m]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
