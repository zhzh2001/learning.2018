#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>

using namespace std;

#define dd c = getchar()
inline void read(int& x)
{
	x = 0;
	char dd;
	bool flag = false;
	for(; !isdigit(c); dd)
	{
//		puts("haha");
		if(c == '-')
			flag = true;
	}
	for(; isdigit(c); dd)
		x = (x << 1) + (x << 3) + (c ^ 48);
	if(flag)
		x = -x;
}
#undef dd

inline void write(int x)
{
	if(x < 0)
	{
		putchar('-');
		x = -x;
	}
	if(x >= 10)
		write(x / 10);
	putchar((x % 10) | 48);
}

inline void writesp(int x)
{
	write(x);
	putchar(' ');
}

const int maxn = 300005;

int n;

#define ls(x) (x << 1)
#define rs(x) (x << 1 | 1)

struct Tree
{
	bool no[maxn<<2];
	
	inline void push_up(int k)
	{
		no[k] = no[ls(k)] & no[rs(k)];
	}
	
	inline void build()
	{
		memset(no, 0, sizeof(no));
	}
	
	inline void change(int l, int r, int k, int K)
	{
		if(l == r)
		{
			no[k] = true;
			return;
		}
		int mid = (l + r) >> 1;
		if(K <= mid)
			change(l, mid, ls(k), K);
		else
			change(mid + 1, r, rs(k), K);
		push_up(k);
	}
	
	inline int query(int l, int r, int k)
	{
		if(no[k])
			return 0;
		if(l == r)
			return l;
		int mid = (l + r) >> 1;
		return max(query(l, mid, ls(k)), query(mid + 1, r, rs(k)));
	}
} tr;

int main()
{
	freopen("change.in", "r", stdin);
	freopen("change.out", "w", stdout);
	read(n);
	tr.build();
	for(int i = 1, t; i <= n; ++i)
	{
//		t = read();
		read(t);
		tr.change(1, n, 1, t);
		writesp(i - (n - tr.query(1, n, 1)));
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
