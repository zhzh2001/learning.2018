#include <cstdio>
#include <queue>
#include <iostream>
#include <algorithm>

using namespace std;

const int maxn = 505;

int n;
int d[maxn];
int sta[maxn];
int st_top;
int ans[maxn];
int Ans;

inline void op_0()
{
	priority_queue<int> q;
	Ans = 0;
	for(int i = 1; i <= n; ++i)
		scanf("%d", &d[i]);
	for(int i = 1; i <= n; ++i)
	{
//		cout << i << endl;
		while(!q.empty())
			q.pop();
		for(int j = 1; j <= n; ++j)
			if(d[j] && i != j)
				q.push(d[j]);
		bool ff = true;
		while(!q.empty())
		{
			int top1 = q.top();
			q.pop();
			if(q.empty())
			{
				ff = false;
				break;
			}
			int top2 = q.top();
			q.pop();
			top1--;
			top2--;
			if(top1)
				q.push(top1);
			if(top2)
				q.push(top2);
		}
		if(ff && q.empty())
			ans[++Ans] = i;
	}
	printf("%d\n", Ans);
	for(int i = 1; i <= Ans; ++i)
		printf("%d\n", ans[i]);
}

inline void op_1()
{
	priority_queue<int> q;
	for(int i = 1; i <= n; ++i)
		scanf("%d", &d[i]);
	for(int i = 1; i <= n; ++i)
	{
		while(!q.empty())
			q.pop();
		for(int j = 1; j <= n; ++j)
			if(d[j] && i != j)
				q.push(d[j]);
		bool flag = true;
		while(!q.empty())
		{
			int top1 = q.top();
			q.pop();
			if(q.empty())
			{
				flag = false;
				break;
			}
			st_top = 0;
			while(top1--)
			{
				if(q.empty())
				{
					flag = false;
					break;
				}
				int top = q.top();
				q.pop();
				top--;
				if(top)
					sta[++st_top] = top;
			}
			if(!flag)
				break;
			for(int j = 1; j <= st_top; ++j)
				q.push(sta[j]);
		}
		if(flag)
			ans[++Ans] = i;
	}
	printf("%d\n", Ans);
	for(int i = 1; i <= Ans; ++i)
		printf("%d\n", ans[i]);
}

int main()
{
	freopen("wrong.in", "r", stdin);
	freopen("wrong.out", "w", stdout);
	int opt;
	scanf("%d%d", &n, &opt);
	n++;
	if(opt)
		op_1();
	else
		op_0();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
