#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read() {
	int X = 0, w = 0;
	char ch = 0;
	while (!isdigit(ch)) {
		w |= ch == '-';
		ch = getchar();
	}
	while (isdigit(ch)) X = (X << 3) + (X << 1) + (ch ^ 48), ch = getchar();
	return w ? -X : X;
}
struct node {
	int x, y;
} matrix[1200][1200];
int n, m, to[1200][1200]; //0 ���� 1 ����
signed main() {
	srand((int)time(0));
	freopen("sort.in", "r", stdin);
	freopen("sort.out", "w", stdout);
	n = read();
	m = read();
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			matrix[i][j].x = read();
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			matrix[i][j].y = read();
	for (int i = n; i >= 1; i--)
		for (int j = m; j >= 1; j--) {
			int xx = matrix[i][j].x, yy = matrix[i][j].y;
			if (i != n && to[i + 1][j] == 1) {
				xx += matrix[i + 1][j].x;
				yy += matrix[i + 1][j].y;
			}
			if (j != m && to[i][j + 1] == 0) {
				xx += matrix[i][j + 1].x;
				yy += matrix[i][j + 1].y;
			}
			matrix[i][j].x = xx;
			matrix[i][j].y = yy;
			if (xx > yy) to[i][j] = 0;
			else if (xx < yy)to[i][j] = 1;
			else {
				int w = rand() % 2;
				to[i][j] = w;
			}
		}
	int ans = 0;
	for (int i = 1; i <= m; i++) {
		if (to[1][i] == 1) {
			ans += matrix[1][i].y;
		}
	}
	for (int i = 1; i <= n; i++) {
		if (to[i][1] == 0) {
			ans += matrix[i][1].x;
		}
	}
	cout << ans;
	return 0;
}
