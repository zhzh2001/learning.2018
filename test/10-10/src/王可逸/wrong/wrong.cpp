#include <bits/stdc++.h>
using namespace std;
inline int read() {
	int X = 0, w = 0;
	char ch = 0;
	while (!isdigit(ch)) {
		w |= ch == '-';
		ch = getchar();
	}
	while (isdigit(ch)) X = (X << 3) + (X << 1) + (ch ^ 48), ch = getchar();
	return w ? -X : X;
}
inline void write(int x) {
	if (x < 0) putchar('-'), x = -x;
	if (x > 9) write(x / 10);
	putchar(x % 10 + '0');
}
int a[560], n, m, num, ff[560], b[560];
bool ok[560];
set <int> q;
bool vis[560][560];
priority_queue <int> dui;
int main() {
	freopen("wrong.in", "r", stdin);
	freopen("wrong.out", "w", stdout);
	n = read();
	m = read();
	if (m == 0) {
		for (int i = 1; i <= n + 1; i++) {
			a[i] = read();
			if (a[i] % 2 == 0) num++;
		}
		write(num);
		puts("");
		for (int i = 1; i <= n; i++) {
			if (a[i] % 2 == 0)  {
				write(i);
				puts("");
			}
		}
	} else {
		int he = 0;
		for (int i = 1; i <= n + 1; i++) {
			a[i] = read();
		}
		for (int i = 1; i <= n + 1; i++) he += a[i];
		for (int i = 1; i <= n + 1; i++) {
			b[i] = a[i];
			if (a[i] % 2 == 1 && he % 2 == 1)
				q.insert(a[i]);
			else if (a[i] % 2 == 0 && he % 2 == 0)
				q.insert(a[i]);
			ff[a[i]] = i;
		}

		for (set <int> :: iterator it = q.begin(); it != q.end(); it++) {
			while (!dui.empty()) dui.pop();
			int which = ff[*it];
			for (int i = 1; i <= n + 1; i++) {
				if (i != which)
					dui.push(a[i]);
			}
			while (dui.size() > 1) {
				int q1 = dui.top();
				dui.pop();
				int q2 = dui.top();
				dui.pop();
				q1--;
				q2--;
				if (q1) dui.push(q1);
				if (q2) dui.push(q2);
			}
			if (dui.size() == 0) ok[*it] = true;
		}
		int num = 0;
		for (int i = 1; i <= n + 1; i++) if (ok[a[i]]) num++;
		write(num);
		puts("");
		for (int i = 1; i <= n + 1; i++)
			if (ok[a[i]]) {
				write(i);
				puts("");
			}
	}
	return 0;
}

