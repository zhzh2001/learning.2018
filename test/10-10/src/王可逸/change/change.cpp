#include <bits/stdc++.h>
using namespace std;
inline int read() {
	int X = 0, w = 0;
	char ch = 0;
	while (!isdigit(ch)) {
		w |= ch == '-';
		ch = getchar();
	}
	while (isdigit(ch)) X = (X << 3) + (X << 1) + (ch ^ 48), ch = getchar();
	return w ? -X : X;
}
inline void write(int x) {
	if (x < 0) putchar('-'), x = -x;
	if (x > 9) write(x / 10);
	putchar(x % 10 + '0');
}
int a[320000], n, last = 0, at, ans[320000];
bool vis[320000];
int main() {
	freopen("change.in", "r", stdin);
	freopen("change.out", "w", stdout);
	n = read();
	at = n;
	for (int i = 1; i <= n; i++) {
		a[i] = read();
		vis[a[i]] = true;
		while (vis[at] == true) {
			at--;
			last++;
		}
		ans[i] = i - last;
	}
	for (int i = 1; i <= n; i++) {
		write(ans[i]);
		putchar(' ');
	}
	return 0;
}