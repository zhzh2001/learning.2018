#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=505;
int q[N],top,sum;
int n,op,d[N],ans[N],num,b[N*2],c[N*2],a[N*2];
inline void Sort(int l,int mid,int r)
{
	if(l>mid||mid+1>r)	return;
	int t1=0,t2=0;
	For(i,l,mid)	a[++t1]=q[i];
	For(i,mid+1,r)	b[++t2]=q[i];
	int n1=1,n2=1,n3=0;
	while(n1<=t1||n2<=t2)
	{
		if(n1>t1&&n2<=t2)	c[++n3]=b[n2++];
		if(n1<=t1&&n2>t2)	c[++n3]=a[n1++];
		if(n1<=t1&&n2<=t2)
		{
			if(a[n1]>b[n2])
				c[++n3]=b[n2++];
				else	c[++n3]=a[n1++];
		}
	}
	For(i,l,r)	q[i]=c[i-l+1];
}
inline bool check(int x)
{
	int sum=0;
	top=0;
	int mx=0; 
	For(i,1,n+1)	if(i!=x)	q[++top]=(n-1)-d[i],sum+=d[i],mx=max(mx,d[i]);
	if(op==0)	return (sum%2==0&&mx<=sum/2);
	sort(q+1,q+top+1);
	Dow(i,1,top)
	{
		int del=q[i];
		if(del>(i-1))	return 0;
		int mid=2333;
		Dow(j,i-del,i-1)	q[j]--,q[i]--,mid=j;
		Sort(1,mid-1,i-1);
	}
	For(i,1,top)	if(q[i])	return 0;
	return 1;
}
int main()
{
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	n=read();op=read();
	For(i,1,n+1)	d[i]=read();
	For(i,1,n+1)	if(check(i))	ans[++num]=i;
	writeln(num);
	For(i,1,num)	writeln(ans[i]);
}
/*
4 0
1 2 2 1 3 
*/
