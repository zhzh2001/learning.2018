#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=1005;
int n,m;
ll a[N][N],b[N][N],sa[N][N],sb[N][N],dp[N][N],mx[N][N],ans=-1e15;
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();m=read();
	For(i,1,n)	For(j,1,m)	a[i][j]=read();
	For(i,1,n)	For(j,1,m)	b[i][j]=read();
	For(i,1,n)	For(j,1,m)	sa[i][j]=sa[i][j-1]+a[i][j],sb[i][j]=sb[i][j-1]+b[i][j];
	memset(dp,0xc0,sizeof dp);memset(mx,0xc0,sizeof mx);
	For(i,0,m)	mx[0][i]=0;
	For(i,1,n)
		For(j,0,m)	dp[i][j]=mx[i-1][j]+sa[i][j]+(sb[i][m]-sb[i][j]),mx[i][j]=max(j==0?0:mx[i][j-1],dp[i][j]);
	For(i,1,m)	ans=max(ans,dp[n][i]);
	writeln(ans);
}
/*
4 4
0 0 10 9
1 3 10 0
4 2 1 3
1 1 20 0

10 0 0 0
1 1 1 30
0 0 5 5
5 10 10 10
*/
