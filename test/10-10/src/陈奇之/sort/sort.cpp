#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,LEN,stdin);
		if(S == T) return EOF;
	}
	return *S++;
}
inline int read(){
	register int x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 1055;
ll n,m,a[maxn][maxn],f[maxn][maxn];
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n = rd(),m = rd();
	Rep(i,1,n)
		Rep(j,1,m)
			a[i][j] += rd();
	ll ans = 0;
	Rep(i,1,n)
		Rep(j,1,m){
			ll x = rd();
			ans += x;
			a[i][j] -= x;
		}
//	writeln(ans);
	Rep(i,1,n){
		Rep(j,1,m) a[i][j] = a[i][j-1] + a[i][j];
//		Rep(j,1,n) sum[i][j] = max(sum[i][j],sum[i][j-1]);
	}
	f[0][0] = 0;
	Rep(i,1,n){
		ll S = 0;
		Rep(j,0,m){
			S = max(S,f[i-1][j]);
			f[i][j] = S + a[i][j];
		}
	}
	ll tmp = -0x3f3f3f3f3f3f3f3f;
	Rep(j,0,m) tmp = max(tmp,f[n][j]);
	writeln(ans + tmp);
	return 0;
}
