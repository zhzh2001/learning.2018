#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline int read(){
	register int x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 505;
ll n,m,a[maxn];
namespace wzp0{
	ll w[maxn];
	bool solve(int x){
		*w = 0;
		Rep(i,1,n+1) if(i!=x) w[++*w] = a[i];
		ll sum = 0,mx = 0;
		Rep(i,1,n){
			sum += w[i];
			mx = max(mx,w[i]);
		}
		if(sum % 2 == 0 && mx*2 <= sum) return true;
		return false;
	}
}
namespace wzp1{
	ll w[maxn],cnt[maxn],pos[maxn];
	bool solve(int x){
		*w = 0;
		Rep(i,1,n+1) if(i!=x) w[++*w] = a[i];
//		puts("");
		Dep(i,n,1){
//			sort(w+1,w+1+i);
//			Rep(j,1,n) printf("(%d)",w[j]);puts("");
			Rep(j,1,n) cnt[j]=0;
			Rep(j,1,i) cnt[w[j]]++;
			Rep(j,1,n) cnt[j] += cnt[j-1];
			Dep(j,i,1) pos[cnt[w[j]]--] = w[j];
			Rep(j,1,i) w[j] = pos[j];
			Dep(j,i-1,1){
				if(!w[i]) break;
				if(!w[j]) return false;
				w[i]--;
				w[j]--;
			}
			if(w[i]) return false;
		}
		return true;
	}
}
int ans[525];
int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	n = rd();
	int op = rd();
	Rep(i,1,n+1) a[i] = rd();
	Rep(i,1,n+1){
		bool flag;
		if(op == 0) flag = wzp0::solve(i); else
					flag = wzp1::solve(i);
		if(flag){
			ans[++ans[0]] = i;
		}
	}writeln(ans[0]);
	Rep(i,1,ans[0]){
		writeln(ans[i]);
	}
	return 0;
}
