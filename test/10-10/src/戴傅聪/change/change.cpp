#include<bits/stdc++.h>
using namespace std;
inline void write(int x)
{
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
#define maxn 300001
struct segment_tree
{
	#define lson(x) (x<<1)
	#define rson(x) (x<<1|1)
	int maxv[maxn<<2],addv[maxn<<2];
	inline void pushup(int p){maxv[p]=max(maxv[lson(p)],maxv[rson(p)]);}
	inline void setadd(int p,int add){maxv[p]+=add;addv[p]+=add;}
	inline void pushdown(int p)
	{
		if(addv[p]) setadd(lson(p),addv[p]),setadd(rson(p),addv[p]);
		addv[p]=0;
	}
	inline void setadd(int p,int l,int r,int ql,int qr,int add)
	{
		if(ql<=l&&qr>=r) return (void)setadd(p,add);
		pushdown(p);int mid=(l+r)>>1;
		if(ql<=mid) setadd(lson(p),l,mid,ql,qr,add);
		if(qr>mid) setadd(rson(p),mid+1,r,ql,qr,add);
		pushup(p);
	}
	inline int Qmax(int p,int l,int r,int ql,int qr)
	{
		if(ql<=l&&qr>=r) return maxv[p];
		int mid=(l+r)>>1,ans=-1000000000;pushdown(p);
		if(ql<=mid) ans=max(ans,Qmax(lson(p),l,mid,ql,qr));
		if(qr>mid) ans=max(ans,Qmax(rson(p),mid+1,r,ql,qr));
		return ans;
	}
}seg;
int n;
int main()
{
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	{
		int x;scanf("%d",&x);
		seg.setadd(1,1,n,x+1,n,1);
		seg.setadd(1,1,n,x,x,-1000000000);
		write(max(0,seg.Qmax(1,1,n,1,n)));putchar(' ');
	}
}
