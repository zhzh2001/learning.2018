#include<bits/stdc++.h>
using namespace std;
inline void write(int x)
{
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,LEN,stdin);
		if(S == T) return EOF;
	}
	return *S++;
}
inline int read()
{
    int x=0,f=1;char ch=gc();
    for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
    for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
    return x*f;
}
#define maxn 500100
int ch[maxn][2],fa[maxn],l[maxn],r[maxn],si[maxn],val[maxn],addv[maxn],maxv[maxn],tot,root;
#define lc ch[p][0]
#define rc ch[p][1]
queue<int> was;
inline void pushup(int p)
{
	if(p==0) return;
	si[p]=si[lc]+si[rc]+1;
	maxv[p]=max(maxv[lc],maxv[rc]);
	maxv[p]=max(maxv[p],val[p]);
}
inline void setadd(int p,int add)
{
	if(p==0) return;
	val[p]+=add;maxv[p]+=add;
	addv[p]+=add;
}
inline void pushdown(int p)
{
	if(addv[p]){setadd(lc,addv[p]);setadd(rc,addv[p]);}
	addv[p]=0;
}
inline void Pushdown(int p){if(fa[p]) Pushdown(fa[p]);pushdown(p);}
inline int getnew(int L,int R,int Val)
{
	int res=0;if(was.size()) res=was.front(),was.pop();else res=++tot;
	ch[res][0]=ch[res][1]=fa[res]=0;l[res]=L;r[res]=R;si[res]=1;val[res]=Val;addv[res]=0;maxv[res]=0;
	pushup(res);return res;
}
inline void rotate(int p)
{
	int f=fa[p],ff=fa[f],t=(ch[f][1]==p),w=ch[p][t^1];pushdown(f);pushdown(p);
	if(ff) ch[ff][ch[ff][1]==f]=p;ch[p][t^1]=f;ch[f][t]=w;
	if(w) fa[w]=f;fa[f]=p;fa[p]=ff;pushup(f);pushup(p);
	if(!ff) root=p;
}
inline void splay(int p,int to=0)
{
	Pushdown(p);
	while(fa[p]!=to)
	{
		int f=fa[p],ff=fa[f];
		if(ff!=to) rotate((ch[ff][0]==f)^(ch[f][0]==p)?f:p);
		rotate(p);
	}
}
inline void find(int cur,int x)
{
	pushdown(cur);
	if(x>=l[cur]&&x<=r[cur]) return (void)splay(cur);
	if(x<l[cur]) find(ch[cur][0],x);
	else find(ch[cur][1],x);
}
inline void remove(int p)
{
	while(pushdown(p),lc||rc) rotate(lc?lc:rc);was.push(p);
	int f=fa[p];if(ch[f][0]==p) ch[f][0]=0;else ch[f][1]=0;fa[p]=0;
	int cur=f;
	while(cur) pushup(cur),cur=fa[cur];splay(f);
}
inline void split(int mid)
{
	int p=root;
	if(mid==l[p]&&mid==r[p])
	{
		setadd(rc,1);
		remove(p);
	}
	else if(mid==l[p])
	{
		l[p]++;setadd(rc,1);
		val[p]++;pushup(p);
	}
	else if(mid==r[p])
	{
		r[p]--;setadd(rc,1);
		pushup(p);
	}
	else
	{
		int Ls=getnew(l[p],mid-1,val[p]),Rs=getnew(mid+1,r[p],val[p]);
		int ll=lc,rr=rc;
		ch[Ls][0]=ll;ch[Rs][1]=rr;
		fa[ll]=Ls;fa[rr]=Rs;fa[Ls]=fa[Rs]=p;
		lc=Ls;rc=Rs;pushup(lc);pushup(rc);pushup(p);
		setadd(rc,1);pushup(p);
		remove(p);
	}
}//split root
int n;
inline void init()
{
	n=read();
	root=getnew(1,n,0);
}
inline void dfs(int x)
{
	if(x==0) return;pushdown(x);
	dfs(ch[x][0]);
	dfs(ch[x][1]);
}
int main()
{
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	init();
	for(int i=1;i<=n;i++)
	{
		int x;x=read();
		find(root,x);split(x);
	//	dfs(root);
		write(maxv[root]);putchar(' ');
	}
}
