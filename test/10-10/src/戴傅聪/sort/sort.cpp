#include<bits/stdc++.h>
using namespace std;
#define int long long
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,LEN,stdin);
		if(S == T) return EOF;
	}
	return *S++;
}
#define ll long long
inline ll read()
{
    ll x=0,f=1;char ch=gc();
    for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
    for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
    return x*f;
}
#define maxn 1010
int f[maxn][maxn],sum1[maxn],sum2[maxn];
int a[maxn][maxn],b[maxn][maxn];int n,m;
int q[maxn],he,ta;
inline void init()
{
	scanf("%lld%lld",&n,&m);
	for(int i=1;i<=n;i++) for(int j=1;j<=m;j++) a[i][j]=read();
	for(int i=1;i<=n;i++) for(int j=1;j<=m;j++) b[i][j]=read();
}
inline int dp()
{
	for(int i=1;i<=m;i++) f[0][i]=-1e18;
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=m;j++) sum1[j]=sum1[j-1]+a[i][j],sum2[j]=sum2[j-1]+b[i][j];
		int maxval=-1e18;
		for(int j=0;j<=m;j++)
		{
			f[i][j]=-1e18;maxval=max(maxval,f[i-1][j]);
			f[i][j]=maxval+sum1[j]+sum2[m]-sum2[j];
		}
	}
	int ans=0;
	for(int i=1;i<=m;i++) ans=max(ans,f[n][i]);
	return ans;
}
signed main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	init();
	printf("%lld\n",dp());
	return 0;
}
