#pragma GCC optimize(2)
#include<bits/stdc++.h>
using namespace std;
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,LEN,stdin);
		if(S == T) return EOF;
	}
	return *S++;
}
inline int read()
{
    int x=0,f=1;char ch=gc();
    for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
    for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
    return x*f;
}
#define maxn 601
long long Sum=0;
int a[maxn];int n,opt;int t[maxn],tot;int ans[maxn],anscnt;
priority_queue<int> q;
inline bool check()
{
	if(!opt)
	{
		long long sum=0;
		for(int i=1;i<=n;i++) sum+=t[i];
		if(sum%2==1) return 1;
		int maxx=0;
		for(int i=1;i<=n;i++) maxx=max(maxx,t[i]);
		if(maxx>(sum>>1)) return 1;
		return 0;
	}
	for(int i=1;i<=n;i++)
	{
		sort(t+i,t+n+1,greater<int>());
		int cur=i+1;
		while(cur<=n&&t[i]){if(t[cur])t[cur]--,t[i]--;cur++;}
		if(t[i]) return 1;
	}
	return 0;
}
int main()
{
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	n=read();opt=read();
	for(int i=1;i<=n+1;i++) a[i]=read(),Sum+=a[i];
	for(int i=1;i<=n+1;i++)
	{
		tot=0;
		for(int j=1;j<=n+1;j++) if(j!=i) t[++tot]=a[j];
		if(!check()) ans[++anscnt]=i;
	}
	cout<<anscnt<<endl;
	for(int i=1;i<=anscnt;i++) printf("%d\n",ans[i]);
}
