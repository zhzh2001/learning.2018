#include<cstdio>
#include<cstring>
#include<string>
#include<cmath>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#define rep(x,a,b) for (int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (int x=hd[a];~x;x=nx[x])
#define LL long long
#define inf 0x3f3f3f3f
#define sqr(x) ((x)*(x))
#define N 1010
LL m,n,f[N][N],a[N][N],b[N][N],ha[N][N],hb[N][N];
LL max(LL x,LL y)
{
    if (x>y) return x;return y;
}
LL read(){
LL x=0,fh=1; char c=getchar();
  for(;!isdigit(c); c=getchar()) fh=(c=='-')?-1:fh;
  for(;isdigit(c); c=getchar()) x=(x<<3)+(x<<1)+(c^'0');
  return x*fh;
}
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
    n=read(),m=read();
    memset(a,0,sizeof(a));
    memset(b,0,sizeof(b));
    memset(ha,0,sizeof(ha));
    memset(hb,0,sizeof(hb));
    memset(f,0,sizeof(f));
    rep(i,1,n)
        rep(j,1,m)
            b[i][j]=read(),hb[i][j]=b[i][j]+hb[i][j-1];
    rep(i,1,n)
        rep(j,1,m)
            a[i][j]=read(),ha[i][j]=a[i][j]+ha[i-1][j];
    drp(i,n,0)
        drp(j,m,0)
            f[i][j]=max(f[i][j+1]+ha[i][j+1],f[i+1][j]+hb[i+1][j]);
    printf("%lld\n",f[0][0]);
    return 0;
}
