#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 300010
using namespace std;
int read(){
int x=0,fh=1; char c=getchar();
  for(;!isdigit(c); c=getchar()) fh=(c=='-')?-1:fh;
  for(;isdigit(c); c=getchar()) x=(x<<3)+(x<<1)+(c^'0');
  return x*fh;
}
int n,r,x,num,a[N];
int main(){
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
//	scanf("%d",&n);
	n=read();
	r=n;
	memset(a,0,sizeof(a));
	rep(i,1,n){
	//	scanf("%d",&x);
		x=read();
		a[x]=1;
		num=i-(n-r);
		while(a[r]==1) r--,num--;
		printf("%d ",num);
	}
	return 0;
}
