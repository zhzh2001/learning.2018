#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 510;
int n,k,a[N];
namespace Solve1{
	int b[N],c[N],sum,ans[N];
	inline void Merge(int l,int r,int L,int R){
		int k=0;
		while (l<=r&&L<=R) (b[l]<=b[L])?c[++k]=b[l++]:c[++k]=b[L++];
		while (l<=r) c[++k]=b[l++];
		while (L<=R) c[++k]=b[L++];
		For(i,1,R) b[i]=c[i];
	}
	inline void Main(){
		For(i,1,n+1){
			int cnt=0,flag=1,x;
			For(j,1,n+1) if (j!=i) b[++cnt]=a[j];
			sort(b+1,b+1+n);
			Dow(j,n,1){
				if (!b[j]) continue;
				Dow(k,j-1,1){
					if (!b[j]){x=k+1;break;}
					if (b[k]) b[k]--,b[j]--;
				}
				if (b[j]){flag=0;break;}
				Merge(1,x-1,x,j-1);
			}
			sum+=flag,ans[i]=flag;
		}
		printf("%d\n",sum);
		For(i,1,n+1) if (ans[i]) printf("%d\n",i);
	}
};
namespace Solve0{
	int x,sum,ans[N];
	inline void Main(){
		x=0;
		For(i,1,n+1) x=(x+a[i])&1;
		For(i,1,n+1){
			if (a[i]&1!=x) continue;
			ll Sum=0,flag=1;
			For(j,1,n+1) if (j!=i) Sum+=a[j];
			For(j,1,n+1) if (j!=i&&Sum-a[j]<a[j]){flag=0;break;}
			sum+=flag,ans[i]=flag;
		}
		printf("%d\n",sum);
		For(i,1,n+1) if (ans[i]) printf("%d\n",i);
	}
};
int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	n=read(),k=read();
	For(i,1,n+1) a[i]=read();
	if (k) Solve1::Main();
		else Solve0::Main();
}
