#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 1010
using namespace std;
int read(){
int x=0,fh=1; char c=getchar();
  for(;!isdigit(c); c=getchar()) fh=(c=='-')?-1:fh;
  for(;isdigit(c); c=getchar()) x=(x<<3)+(x<<1)+(c^'0');
  return x*fh;
}
int n,op,a[N],sum,cnt,ans[N],c[N],b[N];
inline void task1(){
	rep(i,1,n+1){
		int num=sum-a[i];
		if (num&1) continue;
		int Max=0;
		rep(j,1,n) if (a[j]>Max&&(j!=i)) Max=a[j];
		if (num<num-Max) continue;
		ans[++cnt]=i;
	}
	printf("%d\n",cnt);
	rep(i,1,cnt) printf("%d\n",ans[i]);
	return;
}
bool cmp(int x,int y){
	return x>y;
}
inline void Merge(int l,int r,int L,int R){
	int k=0;
	while (l<=r&&L<=R) (b[l]<=b[L])?c[++k]=b[l++]:c[++k]=b[L++];
	while (l<=r) c[++k]=b[l++];
	while (L<=R) c[++k]=b[L++];
	rep(i,1,R) b[i]=c[i];
}
inline void task2(){
	rep(i,1,n+1){
		int Cnt=0,flag=1,x;
		rep(j,1,n+1) if (j!=i) b[++Cnt]=a[j];
		sort(b+1,b+1+n);
		drp(j,n,1){
			if (!b[j]) continue;
			drp(k,j-1,1){
				if (!b[j]) {
					x=k+1;break;
				}
				if (b[k]) b[k]--,b[j]--;
			}
			if (b[j]) {
				flag=0;
				break;
			}
			Merge(1,x-1,x,j-1);
		}
		if (flag) ans[++cnt]=i;
	}
	printf("%d\n",cnt);
	rep(i,1,cnt) printf("%d\n",ans[i]);
		
}
int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	n=read(),op=read();
	sum=0;
	rep(i,1,n+1) a[i]=read(),sum+=a[i];
	if (op==0) task1();
		else task2();
	return 0;
}
