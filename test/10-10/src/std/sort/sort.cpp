#include<bits/stdc++.h>
using namespace std;
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,LEN,stdin);
		if(S == T) return EOF;
	}
	return *S++;
}
#define ll long long
inline ll read()
{
    ll x=0,f=1;char ch=gc();
    for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
    for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
    return x*f;
}
inline void write(ll x)
{
    if(x<0)putchar('-'),x=-x;
    if(x>9)write(x/10);
    putchar(x%10+'0');
}
inline void writeln(ll x)
{
    write(x);
    puts("");
}
const int N=1005;
ll s[N][N];
ll d[N][N];
ll n,m;
ll f[N][N];
ll a[N][N];
ll b[N][N];
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
    n=read();m=read();
    for(int i=1;i<=n;i++)
        for(int j=1;j<=m;j++)
            a[i][j]=read();
    for(int i=1;i<=n;i++)	
        for(int j=1;j<=m;j++)
            b[i][j]=read();
    for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
            a[i][j]=a[i][j-1]+a[i][j];
    for(int i=1;i<=n;i++)
        for(int j=1;j<=m;j++)
            b[i][j]=b[i-1][j]+b[i][j];
    for(int i=1;i<=n;i++)
        for(int j=1;j<=m;j++)
            f[i][j]=max(f[i-1][j]+a[i][j],f[i][j-1]+b[i][j]);
    writeln(f[n][m]);
    return 0;
}
