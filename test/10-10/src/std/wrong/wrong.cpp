
#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
    ll x = 0,f = 1;char ch = getchar();
    while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
    while(ch >= '0' && ch <= '9'){x = x*10+ch-'0'; ch = getchar();}
    return x*f;
}

inline void write(ll x)
{
    if(x<0)putchar('-'),x=-x;
    if(x>9)write(x/10);
    putchar(x%10+'0');
}
inline void writeln(ll x)
{
    write(x);
    puts("");
}
const int N = 505;
int a[N];
int c[N],d[N];
vector<int> b[N];
int n,tot;
int ans,p[N];
bool vis[N];
int num,to,op;

signed main(){
 	freopen("wrong.in","r",stdin);
 	freopen("wrong.out","w",stdout);
    n = read();op = read();
    if(op==0){
    	for(int i=1;i<=n+1;i++)
        	a[i]=read();
		for(int i=1;i<=n+1;i++){
			int mx=0,s=0;
			for(int j=1;j<=n+1;j++)
				if(i!=j){
					s+=a[j];
					if(a[j]>mx)mx=a[j];
				}
			if(s%2==0&&mx<=s/2)c[++tot]=i;
		}
    writeln(tot);
    for(int i=1;i<=tot;i++)
        writeln(c[i]);
	}
	else{
    for(int i = 1;i <= n+1;++i){
        int x = read();
        if(x >= n){++num; to = i; continue;}
        ++a[x];
        b[x].push_back(i);
        tot += x;
    }
    // printf("%d\n", num);
    if(num > 1){puts("0"); return 0;}
    else if(num == 1){
        if(tot%2 != 0){ puts("0"); return 0; }
        memcpy(c,a,sizeof(a));
        int top = n-1;
        bool flag = true;
        for(int t = 1;t <= n;++t){
            while(c[top] == 0) --top;	//top当前是最大的数。
            if(top == 0) break;
            int pos = top;
            c[top]--;		//当前数被取出
            for(int i = top;i >= 1;--i){
                if(pos > c[i]){
                    pos -= c[i];
                }else{
                    c[i-1] += pos; c[i] -= pos;
                    pos = 0;
                    for(int j = i;j <= top-1;++j){
                        c[j] += c[j+1];
                        c[j+1] = 0;
                    }
                    break;
                }
            }
            if(pos > 0){
                flag = false;
                break;
            }
        }
        if(flag){
            puts("1");
            printf("%d\n", to);
            return 0;
        }else{
            puts("0");
            return 0;
        }
    }
    for(int i = 0;i < n;++i){
        if(a[i] > 0){
            if((tot-i)%2 != 0) continue;
            for(int k = 0;k < n;++k) c[k] = a[k];	//将a转存到c中
            --c[i];	//该数字少一个
            int top = n-1;
            bool flag = true;
            for(int t = 1;t <= n;++t){
                while(c[top] == 0) --top;	//top当前是最大的数。
                if(top == 0) break;	//最大的数是0，说明已结束
                int pos = top;
                c[top]--;
                for(int k = top;k >= 1;--k){
                    if(pos > c[k]){
                        pos -= c[k];
                    }else{
                        c[k-1] += pos; c[k] -= pos;
                        pos = 0;
                        for(int j = k;j <= top-1;++j){
                            c[j] += c[j+1];
                            c[j+1] = 0;
                        }
                        break;
                    }
                }
                if(pos > 0){
                    // int sum = 0;
                    // for(int j = 1;j <= top;++j) sum += c[j];
                    // printf("# %d %d %d %d\n",i,pos,top,sum);
                    flag = false;
                    break;
                }
            }
            if(flag){	//说明删去当前数是可行的
                for(unsigned int j = 0;j < b[i].size();++j)
                    p[++ans] = b[i][j];
            }
        }
    }
    // puts("");
    printf("%d\n", ans);
    sort(p+1,p+ans+1);
    for(int i = 1;i <= ans;++i)
        printf("%d\n", p[i]);
    }
    return 0;
}

