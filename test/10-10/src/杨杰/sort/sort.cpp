#include <cstdio>
#include <algorithm>
using namespace std;
const int N=1005;
#define ll long long
int n,m;
int a[N][N],b[N][N];
ll ans,f[N][N],qa[N][N],qb[N][N];
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd

int main() {
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++) {
			b[i][j]=read();
			qb[i][j]=qb[i][j-1]+b[i][j];
		}
	for (int i=1;i<=n;i++) {
		for (int j=1;j<=m;j++) {
			a[i][j]=read();
			qa[i][j]=qa[i][j-1]+a[i][j];
		}
		qa[i][m+1]=qa[i][m];
	}
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m+1;j++) {
			f[i][j]=max(f[i-1][j]+qa[i-1][m+1]-qa[i-1][j]+qb[i][j-1]+a[i][j],
					f[i][j-1]+a[i][j]+b[i][j-1]-a[i][j-1]);
		}
	for (int i=1;i<=n+1;i++) ans=max(ans,f[n][i]+qa[n][m+1]-qa[n][i]);
	printf("%lld\n",ans);
}
