#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <algorithm>
using namespace std;
const int N=505;
int n,opt,cnt;
int a[N],b[N],ans[N],e[N][N];
bool cmp(int a,int b) {return a>b;}
bool check0(int x) {
	long long S=0;
	for (int i=1;i<=n;i++) if (i!=x) S=S+a[i];
	if (S&1) return 0;
	for (int i=1;i<=n;i++) if (i!=x && a[i]>S-a[i]) return 0;
	return 1;
}
#define pii pair<int,int>
#define mk make_pair
#define fi first
#define se second
priority_queue<pii> qa;
priority_queue<pii,std::vector<pii>,greater<pii> > qb;
bool check1(int x) {
	memset(e,0,sizeof e);
	for (int i=1;i<=n;i++) if (i!=x) b[i]=a[i],qa.push(mk(b[i],i)),qb.push(mk(b[i],i));
	while (1) {
		while (!qa.empty() && qa.top().fi!=b[qa.top().se]) qa.pop();
		while (!qb.empty() && qb.top().fi!=b[qb.top().se]) qb.pop();
		if (qa.empty() || qb.empty()) break;
		pii ha=qa.top(),hb=qb.top();qa.pop();qb.pop();
		if (ha.se==hb.se || e[ha.se][hb.se]) continue;
		b[ha.se]--;b[hb.se]--;
		e[ha.se][hb.se]=1;
		if (b[ha.se]) qa.push(mk(b[ha.se],ha.se)),qb.push(mk(b[ha.se],ha.se));
		if (b[hb.se]) qb.push(mk(b[hb.se],hb.se)),qa.push(mk(b[hb.se],hb.se));
	}
	for (int i=1;i<=n;i++) if (i!=x && b[i]) return 0;
	return 1;
}
int main() {
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	scanf("%d%d",&n,&opt);
	n++;
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<=n;i++) {
		if (opt==0 && check0(i)) ans[++cnt]=i;
		if (opt==1 && check0(i)) ans[++cnt]=i;
	}
	printf("%d\n",cnt);
	for (int i=1;i<=cnt;i++) printf("%d\n",ans[i]);
}
