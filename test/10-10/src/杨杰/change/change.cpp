#include <cstdio>
using namespace std;
const int N=300007;
int vis[N],tr[N];
int n;
int lowbit(int x) {return x&-x;}
void add(int x) {
	for (;x<=n;x+=lowbit(x)) tr[x]++;
}
int query(int x){
	if (x<0) return 0;
	int ans=0;
	for (;x;x-=lowbit(x)) ans+=tr[x];
	return ans;
}
int main() {
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	scanf("%d",&n);
	for (int i=1,r=n,x;i<=n;i++) {
		scanf("%d",&x);
		vis[x]=1;add(x);
		while (vis[r] && r) r--;
		printf("%d ",query(r-1));
	}
}
