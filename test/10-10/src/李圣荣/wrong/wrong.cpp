#include<bits/stdc++.h>
using namespace std;
#define int long long
int a[1000],ans[1000],gs;
inline int read(){
	int k=0; int f=1; char ch=getchar();
	while(!isdigit(ch)){if(ch=='-') f=-1; ch=getchar();}
	while(isdigit(ch)){k=(k<<1)+(k<<3)+(ch^48);ch=getchar();}
	return k*f;
}
signed main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	int n,op; cin>>n>>op;
	for(int i=1;i<=n+1;i++) a[i]=read();
	for(int i=1;i<=n+1;i++){
		int sum=0; int flag=0;
		for(int j=1;j<=n+1;j++){
			if(j==i) continue; sum+=a[j];
			if(a[j]==0) flag++;
		}
		if(sum%2==0&&sum/2>=n-flag-1) ans[++gs]=i;
	}
	cout<<gs<<endl;
	for(int i=1;i<=gs;i++) cout<<ans[i]<<endl;
	return 0;
}
