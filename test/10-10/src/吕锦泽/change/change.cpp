#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 300005
#define ll long long
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll now,n,pos,c[N],a[N];
void add(ll x) { for (ll i=x;i<=n;i+=i&-i) ++c[i]; }
ll get(ll x) {
	ll num=0;
	for (ll i=x;i;i-=i&-i) num+=c[i];
	return num;
}
int main(){
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	now=n=read();
	rep(i,1,n){
		pos=read();
		add(pos); a[pos]=1;
		while (a[now]==1&&now) --now;
		printf("%d ",get(now));
	}
}
