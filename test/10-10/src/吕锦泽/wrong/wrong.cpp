#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 1005
#define ll int
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,opt,a[N],b[N],ans[N],tmp[N],sum,f[N],anss;
bool cmp(ll x,ll y) { return x>y; }
int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	n=read(); opt=read();
	rep(i,1,n+1) sum+=a[i]=read();
	if (!opt){
		rep(i,1,n+1)
			if((sum-a[i])%2==0) f[i]=1,anss++;
			else if(a[i]>sum-a[i]) f[i]=1,anss++;
		printf("%d\n",anss);
		rep(i,1,n+1) if (f[i]) printf("%d\n",i);
		return 0;
	}
	rep(i,1,n+1){
		ll tt=0;
		rep(j,1,n+1) if (i!=j) b[++tt]=a[j];
		sort(b+1,b+1+n,cmp);
		while (b[1]>0){
			memset(tmp,0,sizeof tmp);
			rep(j,2,b[1]+1) --b[j];
			ll l1=2,r1=b[1]+1,l2=b[1]+2,r2=n;
			b[1]=0; ll len=0;
			while (l1<=r1&&l2<=r2){
				if (b[l1]>b[r1]) { tmp[++len]=b[l1]; ++l1; }
				else { tmp[++len]=b[l2]; ++l2; }
			}
			while (l1<=r1) { tmp[++len]=b[l1]; ++l1; }
			while (l2<=r2) { tmp[++len]=b[l2]; ++l2; }
			rep(j,1,n) b[j]=tmp[j];
		}
		sort(b+1,b+1+n,cmp);
		if (b[n]==0) ans[++ans[0]]=i;
	}
	printf("%d\n",ans[0]);
	rep(i,1,ans[0]) printf("%d\n",ans[i]);
}
