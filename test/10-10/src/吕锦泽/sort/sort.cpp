#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 1005
#define ll long long
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
ll n,m,sum1[N][N],sum2[N][N],f[N][N];
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n) rep(j,1,m)
		sum2[i][j]=sum2[i-1][j]+sum2[i][j-1]-sum2[i-1][j-1]+read();
	rep(i,1,n) rep(j,1,m)
		sum1[i][j]=sum1[i-1][j]+sum1[i][j-1]-sum1[i-1][j-1]+read();
	rep(i,1,n) rep(j,1,m)
		f[i][j]=max(f[i][j-1]+sum1[i][j]-sum1[i][j-1],f[i-1][j]+sum2[i][j]-sum2[i-1][j]);
	printf("%lld",f[n][m]);
}
