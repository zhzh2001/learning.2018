#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=610;
struct ppap{int x,i;}a[N];
int n,op,b[N],ans=0,anss[N],c[N];
inline bool cmp(ppap a,ppap b){return a.x<b.x;}
int main()
{
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	n=read()+1;op=read();
	int Sum=0;
	for(int i=1;i<=n;i++){
		a[i].x=read(),Sum+=a[i].x;
		a[i].i=i;
	}
	if(!op){
		for(int i=1;i<=n;i++){
			int sum=0;
			for(int j=1;j<=n;j++)if(i!=j){
				if(a[j].x&1)sum++;
			}
			if((sum&1)==0)anss[++ans]=i;
		}
		writeln(ans);
		for(int i=1;i<=ans;i++)writeln(anss[i]);
		return 0;
	}
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++){
//		cout<<i<<endl;
		int cnt=0;
		for(int j=1;j<=n;j++)if(j!=i)b[++cnt]=a[j].x;
		int sum=Sum-a[i].x;
		while(b[cnt]){
			if(b[cnt-1]==0)break;
			int k=cnt-1;
			for(;k;k--)if(b[k]){
				b[cnt]--;b[k]--;sum-=2;
				if(b[cnt]==0)break;
			}
			if(b[cnt])break;
			cnt--;
			int l1=1,l2=k,Cnt=0;
			while(l1<k&&l2<=cnt){
				if(b[l1]<b[l2])c[++Cnt]=b[l1],l1++;
				else c[++Cnt]=b[l2],l2++;
			}
			for(;l1<k;l1++)c[++Cnt]=b[l1];
			for(;l2<=cnt;l2++)c[++Cnt]=b[l2];
			for(int j=1;j<=cnt;j++)b[j]=c[j];//,write(b[j]),putchar(' ');
//			puts("");
		}
		if(!sum)anss[++ans]=a[i].i;
	}
	writeln(ans);
	sort(anss+1,anss+ans+1);
	for(int i=1;i<=ans;i++)writeln(anss[i]);
	return 0;
}
