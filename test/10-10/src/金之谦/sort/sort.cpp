#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1010;
int n,m,a[N][N],b[N][N],f[N][N][2];
signed main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)b[i][j]=b[i][j-1]+read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)a[i][j]=a[i-1][j]+read();
	f[1][1][0]=a[1][1];f[1][1][1]=b[1][1];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			if(i==1&&j==1)continue;
			f[i][j][0]=max(f[i][j-1][0]+a[i][j],f[i-1][j][0]+b[i][j-1]+a[i][j]-a[i-1][j]);
			f[i][j][1]=max(f[i-1][j][1]+b[i][j],f[i][j-1][1]+a[i-1][j]+b[i][j]-b[i][j-1]);
		}
	int ans=max(f[n][m][0],f[n][m][1]);
	writeln(ans);
	return 0;
}