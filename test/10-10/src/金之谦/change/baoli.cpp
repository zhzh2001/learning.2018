#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=100010;
int b[N],c[N],n;
int main()
{
	freopen("change.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	for(int t=1;t<=n;t++){
		int x=read();b[x]=1;
		for(int i=1;i<=n;i++)c[i]=b[i];
		int i=1,ans=0;
		for(;i<=n;)if(c[i]==1){
			int l=i,flag=0;
			while(l<n){
				if(!c[l+1])flag=1;
				swap(c[l],c[l+1]);
				l++;
			}
			ans+=flag;
			if(!flag)break;
		}else i++;
		write(ans);putchar(' ');
	}
	return 0;
}