#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=3e5+10;
bool b[N];
int f[N],n;
inline void sadd(int x){
	for(;x<=n;x+=x&-x)f[x]++;
}
inline int ssum(int x){
	int ans=0;
	for(;x;x-=x&-x)ans+=f[x];
	return ans;
}
int main()
{
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	n=read();
	int l=n;
	for(int i=1;i<=n;i++){
		int x=read();
		b[x]=1;
		while(b[l])l--;
		sadd(x);
		write(ssum(l));putchar(' ');
	}
	return 0;
}