#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int a[210];
int main()
{
	freopen("change.in","w",stdout);
	srand(time(0));
	int n=rand()%1000+1;
	writeln(n);
	for(int i=1;i<=n;i++)a[i]=i;
	random_shuffle(a+1,a+n+1);
	for(int i=1;i<=n;i++)write(a[i]),putchar(' ');
	return 0;
}