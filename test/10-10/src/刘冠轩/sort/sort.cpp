#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int LEN=200005;
char STR[LEN],*S=STR,*T=STR;
inline char gc(){
	if(S==T){
		T=(S=STR)+fread(STR,1,LEN,stdin);
		if(S==T)return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1;
	char ch=gc();
	for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
	return x*f;
}
const int N=1005;
ll n,m,a[N][N],b[N][N],f[N][N],ans,mx;
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)a[i][j]=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)b[i][j]=read();
	for(int i=1;i<=m;i++){
		for(int j=n-1;j;j--)a[j][i]+=a[j+1][i];
		for(int j=2;j<=n;j++)b[j][i]+=b[j-1][i];
	}
	for(int i=1;i<=m;i++){
		mx=0;
		for(int j=0;j<=n;j++){
			mx=max(f[j][i-1],mx);
			f[j][i]=mx+a[j+1][i]+b[j][i];
		}
	}
	for(int i=0;i<=n;i++)ans=max(ans,f[i][m]);
	cout<<ans;
	return 0;
}
