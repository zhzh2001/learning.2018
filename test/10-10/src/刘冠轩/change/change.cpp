#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int LEN=200005;
char STR[LEN],*S=STR,*T=STR;
inline char gc(){
	if(S==T){
		T=(S=STR)+fread(STR,1,LEN,stdin);
		if(S==T)return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1;
	char ch=gc();
	for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
	return x*f;
}
int n,p;
bool f[100005];
int main(){
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	p=n=read();
	for(int i=1;i<=n;i++){
		f[read()]=1;
		while(f[p]&&p)p--;
		cout<<i-(n-p)<<' ';
	}
	return 0;
}
