#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int LEN=200005;
char STR[LEN],*S=STR,*T=STR;
inline char gc(){
	if(S==T){
		T=(S=STR)+fread(STR,1,LEN,stdin);
		if(S==T)return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1;
	char ch=gc();
	for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
	return x*f;
}
int n,op,ans,a[1005],mx,z,Ans[1005],s;
bitset<40004> dp;
int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	n=read();op=read();
	for(int i=1;i<=n+1;i++){
		z+=(a[i]=read());
		mx=max(mx,a[i]);
	}
	if(!op){
		for(int i=1;i<=n+1;i++)
			if(((z-a[i])&1)==0&&(z-a[i]>=mx*2||a[i]==mx))Ans[++ans]=i;
		cout<<ans<<'\n';
		for(int i=1;i<=ans;i++)cout<<Ans[i]<<'\n';
		return 0;
	}
	s=n*(n-1);
	for(int i=1;i<=n+1;i++){
		if(((z-a[i])&1)==1)continue;
		dp.reset();dp[0]=1;
		for(int j=1;j<=n+1;j++)
			if(j!=i)dp|=dp<<(n-1-a[j]);
		if(dp[(s-z+a[i])/2])Ans[++ans]=i;
	}
	cout<<ans<<'\n';
	for(int i=1;i<=ans;i++)cout<<Ans[i]<<'\n';
	return 0;
}
