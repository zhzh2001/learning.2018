#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,LEN,stdin);
		if(S == T) return EOF;
	}
	return *S++;
}
inline ll read()
{
	ll x=0,f=1;char ch=gc();
	for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
	return x*f;
}
ll sum1[1002][1002],sum2[1002][1002],dp[1002][1002];
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	int n=read(),m=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			sum1[i][j]=read()+sum1[i][j-1];
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++)
			sum2[i][j]=read();
		for(int j=m;j;j--)
			sum2[i][j]+=sum2[i][j+1];
	}
	for(int i=1;i<=n;i++){
		dp[i][0]=dp[i-1][0]+sum2[i][1];
		for(int j=1;j<=m;j++) //<=j
			dp[i][j]=max(dp[i-1][j]+sum1[i][j]+sum2[i][j+1],dp[i][j-1]);
	}
	printf("%lld",dp[n][m]);
}