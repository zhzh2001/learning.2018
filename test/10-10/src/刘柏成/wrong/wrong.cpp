#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,LEN,stdin);
		if(S == T) return EOF;
	}
	return *S++;
}
inline ll read()
{
	ll x=0,f=1;char ch=gc();
	for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
	return x*f;
}
const int maxn=505;
namespace sub0{
	ll a[maxn];
	int ans[maxn];
	void work(int n){
		for(int i=1;i<=n+1;i++)
			a[i]=read();
		int cnt=0;
		for(int i=1;i<=n+1;i++){
			ll sum=0,mx=0;
			for(int j=1;j<=n+1;j++)
				if (j!=i)
					sum+=a[j],mx=max(mx,a[j]);
			if (sum%2==0 && sum>=2*mx)
				ans[++cnt]=i;
		}
		printf("%d\n",cnt);
		for(int i=1;i<=cnt;i++)
			printf("%d\n",ans[i]);
	}
}
int d[maxn];
bool check(int n){
	sort(d+1,d+n+1);
	reverse(d+1,d+n+1);
	int l=1,r=n;
	// for(int i=l;i<=r;i++)
		// fprintf(stderr,"d[%d]=%d\n",i,d[i]);
	while(l<=r && !d[r])
		r--;
	if (r<l)
		return true;
	while(l<=r){
		int u=d[l++],p=0;
		// for(int i=l;i<=r;i++)
			// fprintf(stderr,"d[%d]=%d\n",i,d[i]);
		for(int i=l;i<=r;i++){
			d[i]--;
			u--;
			if (!u){
				p=i;
				break;
			}
		}
		// for(int i=l;i<=r;i++)
			// fprintf(stderr,"d[%d]=%d\n",i,d[i]);
		if (!p)
			return false;
		int o=r;
		for(int i=p+1;i<=r;i++){
			if (d[i]<=d[p]){
				o=i-1;
				break;
			}
		}
		if (o==p){
			while(l<=r && !d[r])
				r--;
			continue;
		}
		int to=l;
		for(int i=p;i>=l;i--)
			if (d[i]!=d[p]){
				to=i+1;
				break;
			}
		int cb=o-p,x=d[p];
		for(int i=to;i<=o;i++)
			d[i]=i<to+cb?x+1:x;
		while(l<=r && !d[r])
			r--;
	}
	return true;
}
int a[maxn],ans[maxn];
int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	int n=read(),op=read();
	if (!op){
		sub0::work(n);
		return 0;
	}
	for(int i=1;i<=n+1;i++)
		a[i]=read();
	int cnt=0;
	for(int i=1;i<=n+1;i++){
		int cur=0;
		for(int j=1;j<=n+1;j++)
			if (j!=i)
				d[++cur]=a[j];
		if (check(n))
			ans[++cnt]=i;
	}
	printf("%d\n",cnt);
	for(int i=1;i<=cnt;i++)
		printf("%d\n",ans[i]);
}