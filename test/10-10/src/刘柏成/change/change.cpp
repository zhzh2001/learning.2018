#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,LEN,stdin);
		if(S == T) return EOF;
	}
	return *S++;
}
inline ll read()
{
	ll x=0,f=1;char ch=gc();
	for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
	return x*f;
}
bool qwq[300003];
int main(){
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	int n=read();
	int lst=n;
	for(int i=1;i<=n;i++){
		qwq[read()]=1;
		while(qwq[lst])
			lst--;
		printf("%d ",i-(n-lst));
	}
}