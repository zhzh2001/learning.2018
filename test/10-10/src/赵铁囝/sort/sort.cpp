#include <iostream>
#include <cstring>
#include <cstdio>

#define ll long long
#define Max 1005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

ll n,m,ans,a[Max][Max],b[Max][Max],f[Max][Max],suma[Max][Max],sumb[Max][Max],num[Max][Max];

int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();m=read();
	for(ll i=1;i<=n;i++){
		for(ll j=1;j<=m;j++){
			a[i][j]=read();
			suma[i][j]=suma[i][j-1]+a[i][j];
		}
	}
	for(ll i=1;i<=n;i++){
		for(ll j=1;j<=m;j++){
			b[i][j]=read();
			sumb[i][j]=sumb[i][j-1]+b[i][j];
		}
	}
	for(ll i=1;i<=n;i++){
		for(ll j=0;j<=m;j++){
			f[i][j]=num[i-1][j]+suma[i][j]+sumb[i][m]-sumb[i][j];
			num[i][j]=max(num[i][j-1],f[i][j]);
		}
	}
	writeln(num[n][m]);
	return 0;
}
