#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>

#define lowbit(x) x&(-x)
#define Max 1005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int n,op,ans,sum,a[Max],b[Max],c[Max],num[Max];
bool use[Max];

inline bool cmp(int x,int y){
	return x>y;
}

inline void add(int x,int add){
	while(x<=n){
		c[x]+=add;
		x+=lowbit(x);
	}
}

inline int ask(int x){
	int ans=0;
	while(x){
		ans+=c[x];
		x-=lowbit(x);
	}
	return ans;
}

inline bool check(int x){
	int size=0,now;
	for(int i=1;i<=n+1;i++){
		if(i==x)continue;
		b[++size]=a[i];
	}
	memset(num,0,sizeof num);
	sort(b+1,b+n+1,cmp);
	for(int i=1;i<=n;i++){
		num[i]=ask(i);
		if(b[i]>=num[i]){
			now=b[i]-num[i];
			if(n-i<now)return false;
			add(i+1,1);
			add(i+now+1,-1);
		}else{
			if(i==n)return false;
			add(i+1,num[i]-b[i]);
			add(i+2,b[i]-num[i]);
//			num[i+1]+=num[i]-b[i];
		}
	}
	return true;
}

int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	n=read();op=read();
	for(int i=1;i<=n+1;i++){
		a[i]=read();
		sum+=a[i];
	}
	for(int i=1;i<=n+1;i++){
		if((sum&1)==(a[i]&1)){
			use[i]=true;
			ans++;
		}
	}
	if(op){
		for(int i=1;i<=n;i++){
			if(!use[i]&&check(i)){
				use[i]=true;
				ans++;
			}
		}
	}
	writeln(ans);
	for(int i=1;i<=n+1;i++){
		if(use[i]){
			writeln(i);
		}
	}
	return 0;
}
