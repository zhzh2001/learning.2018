#include <iostream>
#include <cstring>
#include <cstdio>

#define Max 1005

using namespace std;

int n,x;
bool a[Max],b[Max];

inline bool check(){
	for(int i=1;i<=n;i++){
		if(b[i]){
			for(int j=i;j<=n;j++){
				if(!b[j]){
					return true;
				}
			}
			return false;
		}
	}
	return false;
}

inline int calc(){
	int sum=0;
	while(check()){
		int now;
		for(int i=n;i>=1;i--){
			if(!b[i]){
				now=i;
				break;
			}
		}
		for(int i=1;i<=n;i++){
			if(b[i]){
				for(int j=i+1;j<=now;j++){
					swap(b[j],b[j-1]);
				}
				sum++;
				break;
			}
		}
	}
	return sum;
}

int main(){
	freopen("change.in","r",stdin);
	freopen("1.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>x;
		a[x]=true;
		memcpy(b,a,sizeof a);
//		for(int j=1;j<=n;j++)cout<<b[j]<<" ";cout<<endl;
		cout<<calc()<<" ";
	}
	return 0;
}
