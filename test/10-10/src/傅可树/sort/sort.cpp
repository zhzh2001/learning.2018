#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
const int N=1005;
ll m,n,a[N][N],b[N][N],sum1[N][N],sum2[N][N];
inline ll read(){ll x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(ll x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(ll x){write(x); puts("");}
inline void init(){
	n=read(); m=read();
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			a[i][j]=read();
			sum1[i][j]=sum1[i][j-1]+a[i][j];
		}
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			b[i][j]=read();
			sum2[i][j]=sum2[i][j-1]+b[i][j];
		}
	}
}
ll dp[N][N],mx[N],ans;
inline void solve(){
	for (int i=1;i<=n;i++){
		for (int j=0;j<=m;j++){
			dp[i][j]=mx[j]+sum1[i][j]+sum2[i][m]-sum2[i][j];
		}
		mx[0]=0;
		for (int j=0;j<=m;j++){
			mx[j]=max(mx[j-1],dp[i][j]);
		}
	}
	for (int i=0;i<=m;i++) ans=max(ans,dp[n][i]);
	writeln(ans);
}
int main(){
	freopen("sort.in","r",stdin); freopen("sort.out","w",stdout);
	init(); solve();
	return 0;
}
