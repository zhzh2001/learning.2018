#pragma GCC optimize ("O2")
#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
typedef long long ll;
const int N=505;
int n,op,du[N],flag[N];
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
inline void init(){
	n=read(); op=read();
	for (int i=1;i<=n+1;i++){
		du[i]=read();
	}
}
int cho[N],tot;
inline bool cmp(int x,int y){
	return x>y;
}
int ans;
inline bool check(int x){
	tot=0;
	for (int i=1;i<=n+1;i++){
		if (i!=x) cho[++tot]=du[i];
	}
	if (op==1) {
		while (1){	
			ll S=0;
			for (int i=1;i<=n;i++){
				S+=cho[i];
			}
			if (S&1) return 0;
			sort(cho+1,cho+1+n,cmp);
			if (cho[1]==0) return 1;
			for (int i=2;i<=cho[1]+1;i++){
				if (!cho[i]) return 0;
				cho[i]--;
			}
			cho[1]=0;
		}
	}else{
		while (1){
			ll S=0;
			for (int i=1;i<=n;i++){
				S+=cho[i];
			}
			if (S&1) return 0;
			sort(cho+1,cho+1+n,cmp);
			if (cho[1]==0) {
				return 1;
			}
			if (cho[1]>0&&cho[2]==0) return 0;
			int tmp=min(cho[1],cho[2]);
			cho[1]-=tmp; cho[2]-=tmp;
		}
	}
}
inline void solve(){
	for (int i=1;i<=n+1;i++){
		if (check(i)) flag[i]=1,ans++;
	}
	writeln(ans);
	for (int i=1;i<=n+1;i++){
		if (flag[i]) writeln(i);
	}
}
int main(){
	freopen("wrong.in","r",stdin); freopen("wrong.out","w",stdout);
	init(); solve();
	return 0;
}
