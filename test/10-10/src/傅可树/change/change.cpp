#include<cstdio>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=3e5+5;
int n,bit[N];
inline void init(){
	n=read();
}
inline int lowbit(int x){return x&(-x);}
inline void update(int x,int v){
	for (;x<=n;x+=lowbit(x)) bit[x]+=v;
}
inline int query(int x){
	int ans=0;
	for (;x;x-=lowbit(x)) ans+=bit[x];
	return ans;
}
bool a[N];
int ans;
inline void solve(){
	for (int i=1;i<=n;i++){
		int x=read();
		a[x]=1; update(x,1); int tmp=i-query(x-1); ans++;
		if (tmp==n-x+1){
			int now=x;
			while (a[now]){
				ans--; now--;
			}
		}
		write(ans); putchar(' ');
	}
}
int main(){
	freopen("change.in","r",stdin); freopen("change.out","w",stdout);
	init(); solve();
	return 0;
}
