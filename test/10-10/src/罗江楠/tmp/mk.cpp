#include <bits/stdc++.h>
#define N 55
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
int a[N][N];
int main(int argc, char const *argv[]) {
  freopen(".out", "w", stdout);
  srand(time(0));
  for (int i = 1; i <= 50; ++ i) {
    memset(a, 0, sizeof a);
    for (int i = 1; i <= 10; ++ i) {
      int x = rand() % 5 + 1;
      int y = rand() % 5 + 1;
      if (x == y) continue;
      a[x][y] = a[y][x] = 1;
      // printf("added (%d, %d)\n", x, y);
    }
    vector<int> vec;
    for (int i = 1; i <= 5; ++ i) {
      int res = 0;
      for (int j = 1; j <= 5; ++ j) {
        res += a[i][j];
      }
      vec.push_back(res);
    }
    sort(vec.begin(), vec.end());
    int sum = 0;
    for (int i : vec) {
      printf("%2d ", i);
      sum += i;
    }
    printf("= %d\n", sum);
  }
  return 0;
}