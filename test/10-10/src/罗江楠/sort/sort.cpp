#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 1020
#define ll long long
using namespace std;
inline ll read(){
  ll x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
ll a[N][N], b[N][N], f[N][N];
int main(int argc, char const *argv[]) {
  freopen("sort.in", "r", stdin);
  freopen("sort.out", "w", stdout);

  int n = read(), m = read();
  for (int i = 1; i <= n; ++ i) {
    for (int j = 1; j <= m; ++ j) {
      a[i][j] = read();
      a[i][j] += a[i][j - 1];
    }
  }
  for (int i = 1; i <= n; ++ i) {
    for (int j = 1; j <= m; ++ j) {
      b[i][j] = read();
    }
    for (int j = m; j; -- j) {
      b[i][j] += b[i][j + 1];
    }
  }

  for (int i = 1; i <= n; ++ i) {
    long long max_value = 0;
    for (int j = 0; j <= m; ++ j) {
      max_value = max(max_value, f[i - 1][j]);
      f[i][j] = max_value + a[i][j] + b[i][j + 1];
    }
  }

  long long ans = 0;
  for (int i = 0; i <= m; ++ i) {
    ans = max(ans, f[n][i]);
  }
  printf("%lld\n", ans);

  return 0;
}