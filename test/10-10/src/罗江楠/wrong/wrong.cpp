#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 520
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

pair<int, int> a[N];
vector<int> ans;

namespace subtask1 {

// 判断奇偶性即可
void resolve(int n) {
  int sum = 0;
  for (int i = 1; i <= n; ++ i) {
    sum += a[i].first;
  }
  for (int i = 1; i <= n; ++ i) {
    if ((a[i].first & 1) == (sum & 1)) {
      ans.push_back(a[i].second);
    }
  }
}

} // namespace subtask1

namespace subtask2 {

int b[N];

void resolve(int n) {
  int sum = 0;
  for (int i = 1; i <= n; ++ i) {
    sum += a[i].first;
  }
  for (int i = 1; i <= n; ++ i) {
    bool valid = true;
    int now_sum = sum - a[i].first;
    int now_max = i == n ? a[n-1].first : a[n].first;
    int now_min = i == 1 ? a[2].first : a[1].first;

    if (now_sum & 1) valid = false;
    if (now_sum > (n - 2) * (n - 1)) valid = false;
    if (now_max >= n - 1) valid = false;
    // if (now_min < 1) valid = false;
    /*
    if (valid) {
      memset(b, 0, sizeof b);
      int cnt = 0;
      for (int j = 1; j <= n; ++ j) {
        if (j != i) b[++ cnt] = a[j].first;
      }
      reverse(b + 1, b + cnt + 1);
      for (int j = 1; j <= cnt; ++ j) {
        printf("%d\n", b[j]);
      }
      for (int j = 1; j <= cnt; ++ j) {
        for (int k = j + 1; k <= cnt; ++ k) {
          if (b[k]) {
            -- b[k];
            -- b[j];
          }
          if (!b[j]) {
            break;
          }
        }
        if (b[j]) valid = false;
      }
    }
    */
    if (valid) {
      ans.push_back(a[i].second);
    }
  }
}

} // namespace subtask2

int main(int argc, char const *argv[]) {
  freopen("wrong.in", "r", stdin);
  freopen("wrong.out", "w", stdout);

  int n = read() + 1, op = read();
  for (int i = 1; i <= n; ++ i) {
    a[i].first  = read();
    a[i].second = i;
  }
  sort(a + 1, a + n + 1);

  if (op) subtask2::resolve(n);
  else    subtask1::resolve(n);

  sort(ans.begin(), ans.end());

  printf("%u\n", ans.size());
  for (size_t i = 0; i < ans.size(); ++ i) {
    printf("%d\n", ans[i]);
  }

  return 0;
}