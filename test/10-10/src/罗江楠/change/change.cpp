#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 300020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
bool tong[N];
int main(int argc, char const *argv[]) {
  freopen("change.in", "r", stdin);
  freopen("change.out", "w", stdout);

  int n = read(), ed = n;
  for (int i = 1; i <= n; ++ i) {
    tong[read()] = true;
    while (tong[ed]) -- ed;
    printf("%d%c", i - (n - ed), i == n ? '\n' : ' ');
  }

  return 0;
}
