#include <bits/stdc++.h>
using namespace std;
inline char _gc(){
	static char buf[1 << 14], *p1 = buf, *p2 = buf;
	return p1 == p2 && (p2 = (p1 = buf) + fread(buf, 1, 1 << 14, stdin), p1 == p2) ? EOF : *p1++;
}
#define gc c = getchar()
int read(){
	int x = 0, f = 1; char gc;
	for(; !isdigit(c); gc) if(c == '-') f = -1;
	for(; isdigit(c); gc) x = x * 10 + c - '0';
	return x * f;
}
#undef gc
int n, m, ans, a[1005][1005], b[1005][1005];
int sta[1005][1005];
//1:left 2:up 3:none
int geta(int x, int y){
	if(!sta[x][y]) return 0;
	int ans = a[x][y];
	if(sta[x + 1][y] == 2) ans += geta(x + 1, y);
	if(sta[x][y + 1] == 1) ans += geta(x, y + 1);
	return ans;
}
int getb(int x, int y){
	if(!sta[x][y]) return 0;
	int ans = a[x][y];
	if(sta[x + 1][y] == 2) ans += geta(x + 1, y);
	if(sta[x][y + 1] == 1) ans += geta(x, y + 1);
	return ans;
}
int check(){
	int ans = 0;
	for(int i = 1; i <= n; i++) {
		if(sta[i][1] == 1) ans += geta(i, 1);
	}
	for(int i = 1; i <= m; i++) {
		if(sta[1][i] == 2) ans += getb(1, i);
	}
	return ans;
}
void dfs(int x, int y){
	if(x > n) {
		ans = max(ans, check());
		return;
	}
	if(y > m) {
		dfs(x + 1, 1);
		return;
	}
	sta[x][y] = 0;
	dfs(x, y + 1);
	sta[x][y] = 1;
	dfs(x, y + 1);
	sta[x][y] = 2;
	dfs(x, y + 1);
}
int main(){
	freopen("sort.in", "r", stdin);
	freopen("sort.out", "w", stdout);
	n = read(), m = read();
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= m; j++)
			a[i][j] = read();
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= m; j++)
			b[i][j] = read();
	dfs(1, 1);
	printf("%d", ans);
}
