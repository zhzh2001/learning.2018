#include <bits/stdc++.h>
using namespace std;
inline char _gc(){
	static char buf[1 << 14], *p1 = buf, *p2 = buf;
	return p1 == p2 && (p2 = (p1 = buf) + fread(buf, 1, 1 << 14, stdin), p1 == p2) ? EOF : *p1++;
}
#define gc c = _gc()
int read(){
	int x = 0, f = 1; char gc;
	for(; !isdigit(c); gc) if(c == '-') f = -1;
	for(; isdigit(c); gc) x = x * 10 + c - '0';
	return x * f;
}
#undef gc
int n, opt, deg[505], Ans[505];
bool a[505][505];
struct Data {
	int x, id;
	bool operator<(const Data &b) const {
		return x < b.x;
	}
} d[505];
bool check(int id){
	memset(a, 0, sizeof(a));
	int num = 0, tot = 0;
	for(int i = 1; i <= n; i++)
		if(i != id && deg[i]) {
			d[++num].x = deg[i];
			d[num].id = i;
			tot += d[num].x;
		}
	for(int cur = 1; cur <= num; cur++) {
		int i = num;
		sort(d + 1, d + num + 1);
		tot -= d[i].x + d[i - 1].x;
		while(d[i].x) {
			bool f = 0;
			for(int j = 1; j <= num; j++) {
				if(i == j) continue;
				if(opt && a[d[i].id][d[j].id]) continue;
				while(tot <= d[i - 1].x && d[i - 1].x && d[i].x) {
					if(opt && a[d[i].id][d[i - 1].id]) continue;
					d[i - 1].x--; d[i].x--;
					a[d[i].id][d[i - 1].id] = a[d[i - 1].id][d[i].id] = 1;
					f = 1;
				}
				if(!d[i].x) break;
				if(!d[j].x) continue;
				d[i].x--; d[j].x--;
				a[d[i].id][d[j].id] = a[d[j].id][d[i].id] = 1;
				tot--;
				f = 1;
			}
			if(!f) break;
		}
		if(d[i].x) return 1;
		tot += d[i - 1].x;
	}
	int sum = 0;
	for(int i = 1; i <= num; i++) sum += d[i].x;
	return sum & 1;
}
int main(){
	freopen("wrong.in", "r", stdin);
	freopen("wrong.out", "w", stdout);
	n = read() + 1, opt = read();
	int sum = 0;
	for(int i = 1; i <= n; i++) {
		deg[i] = read();
		sum += deg[i];
	}
	int ans = 0;
	for(int i = 1; i <= n; i++)
		// if(!check(i)) Ans[++ans] = i;
		if((sum - deg[i]) % 2 == 0) Ans[++ans] = i;
	printf("%d\n", ans);
	for(int i = 1; i <= ans; i++) printf("%d\n", Ans[i]);
}
