#include<fstream>
#include<random>
#include<windows.h>
#include<algorithm>
using namespace std;
ofstream fout("change.in");
const int n=3e5;
int a[n+5];
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<endl;
	for(int i=1;i<=n;i++)
		a[i]=i;
	shuffle(a+1,a+n+1,gen);
	for(int i=1;i<=n;i++)
		fout<<a[i]<<' ';
	fout<<endl;
	return 0;
}
