#include<cstdio>
#include<algorithm>
using namespace std;
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
FILE *fin=fopen("change.in","r"),*fout=fopen("change.out","w");
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,LEN,fin);
		if(S == T) return EOF;
	}
	return *S++;
}
#define ll long long
inline ll read()
{
    ll x=0,f=1;char ch=gc();
    for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
    for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
    return x*f;
}
const int N=300005;
int a[N];
bool block[N],space[N];
int main()
{
	int n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	block[n+1]=true;
	int ans=0;
	space[n+1]=true;
	for(int i=1;i<=n;i++)
	{
		ans+=!space[a[i]+1];
		block[a[i]]=true;
		if(space[a[i]+1])
		{
			space[a[i]]=true;
			for(int j=a[i]-1;j&&block[j];j--)
			{
				if(block[j])
					ans--;
				space[j]=true;
			}
		}
		fprintf(fout,"%d ",ans);
	}
	return 0;
}
