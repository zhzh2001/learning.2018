#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("wrong.in");
ofstream fout("wrong.out");
const int N=505;
int a[N],b[N],tmp[N];
bool ans[N];
int main()
{
	int n,op;
	fin>>n>>op;
	for(int i=1;i<=n+1;i++)
		fin>>a[i];
	for(int i=1;i<=n+1;i++)
	{
		int cc=0;
		for(int j=1;j<=n+1;j++)
			if(j!=i)
				b[++cc]=a[j];
		if(op)
		{
			ans[i]=true;
			while(cc)
			{
				for(int k=1;k<=n;k++)
					tmp[k]=0;
				for(int k=1;k<=cc;k++)
					tmp[b[k]]++;
				cc=0;
				for(int k=n;k;k--)
					for(int t=1;t<=tmp[k];t++)
						b[++cc]=k;
				int j=2;
				for(;j<=cc&&b[1];j++)
				{
					b[1]--;
					b[j]--;
				}
				if(b[1])
				{
					ans[i]=false;
					break;
				}
			}
		}
		else
		{
			sort(b+1,b+cc+1);
			while(cc)
			{
				for(int j=cc;j;j--)
					if(b[j-1]>b[j])
						swap(b[j-1],b[j]);
				int j=cc-1;
				bool flag=false;
				for(;j&&b[j]<=b[cc];j--)
				{
					b[cc]-=b[j];
					flag=true;
				}
				if(!flag)
					break;
				if(b[cc])
				{
					b[j+1]=b[cc];
					cc=j+1;
				}
				else
					cc=j;
			}
			ans[i]=!cc;
		}
	}
	fout<<count(ans+1,ans+n+2,true)<<endl;
	for(int i=1;i<=n+1;i++)
		if(ans[i])
			fout<<i<<endl;
	return 0;
}
