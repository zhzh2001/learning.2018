#include<cstdio>
#include<algorithm>
using namespace std;
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
FILE *fin=fopen("sort.in","r"),*fout=fopen("sort.out","w");
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,LEN,fin);
		if(S == T) return EOF;
	}
	return *S++;
}
#define ll long long
inline ll read()
{
    ll x=0,f=1;char ch=gc();
    for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
    for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
    return x*f;
}
const int N=1005;
int n,m;
long long a[N][N],b[N][N],f[N][N];
int main()
{
	n=read();m=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			a[i][j]=read();
			a[i][j]+=a[i][j-1];
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			b[i][j]=read();
			b[i][j]+=b[i-1][j];
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			f[i][j]=max(f[i-1][j]+a[i][j],f[i][j-1]+b[i][j]);
	fprintf(fout,"%lld\n",f[n][m]);
	return 0;
}
