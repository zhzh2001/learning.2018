#include <iostream>
#include <cstdio>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1010;
ll n,m,k,a[N][N],b[N][N],sa[N][N],sb[N][N],ans,f[N][N][2];
int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	read(n),read(m);
	for (ll i=1;i<=n;++i) for (ll j=1;j<=m;++j) read(a[i][j]);
	for (ll i=1;i<=n;++i) for (ll j=1;j<=m;++j) read(b[i][j]);
	for (ll i=1;i<=n;++i) for (ll j=1;j<=m;++j) sa[i][j]=sa[i-1][j]+a[i][j];
	for (ll i=1;i<=n;++i) for (ll j=1;j<=m;++j) sb[i][j]=sb[i][j-1]+b[i][j];
/*	for (ll i=1;i<=n;++i)
		for (ll j=1;j<=m;++j){
			f[i][j]=max(f[i-1][j]+sb[i-1][m]-sb[i-1][j],f[i][j-1]+sa[n][j-1]-sa[i][j-1])+a[i][j];
		}
	ans=f[n][m];
	for (ll i=1;i<=n;++i)
		for (ll j=1;j<=m;++j){
			f[i][j]=max(f[i-1][j]+sb[i-1][m]-sb[i-1][j],f[i][j-1]+sa[n][j-1]-sa[i][j-1])+b[i][j];
		}
	ans=max(ans,f[n][m]);*/
	for (ll i=1;i<=n;++i)
		for (ll j=1;j<=m;++j){
			f[i][j][0]=max(max(f[i-1][j][0],f[i-1][j][1])+sb[i-1][m]-sb[i-1][j],f[i][j-1][0]+sa[n][j-1]-sa[i][j-1])+a[i][j];
			f[i][j][1]=max(f[i-1][j][1]+sb[i-1][m]-sb[i-1][j],max(f[i][j-1][0],f[i][j-1][1])+sa[n][j-1]-sa[i][j-1])+b[i][j];
		}
	wr(max(f[n][m][0],f[n][m][1]));
	return 0;
}
/*

			f[i][j][0]=max(max(f[i-1][j][0],f[i-1][j][1])+sb[i-1][m]-sb[i-1][j],f[i][j-1][0]+sa[n][j-1]-sa[i][j-1])+a[i][j];
			f[i][j][1]=max(f[i-1][j][1]+sb[i-1][m]-sb[i-1][j],max(f[i][j-1][0],f[i][j-1][1])+sa[n][j-1]-sa[i][j-1])+b[i][j];
*/
