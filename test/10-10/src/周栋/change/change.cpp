#include <iostream>
#include <cstdio>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=3e5+10;
ll T[N<<2],x,n;
inline void add(ll id,ll l,ll r,ll x){
	++T[id];
	if (l==r) return;
	ll mid=(l+r)>>1;
	if (x<=mid) add(id<<1,l,mid,x);
	else add(id<<1|1,mid+1,r,x);
}
inline ll query(ll id,ll l,ll r){
	if (l==r) return n-r+T[id];
	ll mid=(l+r)>>1;
	if (T[id<<1|1]==(r-mid)) return query(id<<1,l,mid);
	return query(id<<1|1,mid+1,r);
}
int main(){
	freopen("change.in","r",stdin);
	freopen("change.out","w",stdout);
	read(n);
	for (ll i=1;i<=n;++i){
		read(x);
		add(1,1,n,x);
		wr(i-query(1,1,n)),pc(' ');
	}
	return 0;
}
