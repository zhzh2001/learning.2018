#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
ll n,op,ans[510],cnt,sum;
struct node{
	ll x,id;
}a[510];
int main(){
	freopen("wrong.in","r",stdin);
	freopen("wrong.out","w",stdout);
	read(n),read(op);
	if (op==0){
		for (ll i=1;i<=n;++i) read(a[i].x),a[i].id=i,sum+=a[i].x;
		for (ll i=1;i<=n;++i) if (((sum&1)&&!(a[i].x&1))||(a[i].x>sum/2)) ans[++cnt]=i;
		wr(cnt),puts("");
		for (ll i=1;i<=cnt;++i,puts("")) wr(ans[i]);
		return 0;
	}
	puts("0");
	return 0;
}
