#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
ll n,m,b[1010][1010],a[1010][1010],f[1010][1010];
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(); m=read();
	For(i,1,n) For(j,1,m)a[i][j]=a[i-1][j]+a[i][j-1]-a[i-1][j-1]+read();
	For(i,1,n) For(j,1,m)b[i][j]=b[i-1][j]+b[i][j-1]-b[i-1][j-1]+read();
	For(i,1,n) For(j,1,m)
		f[i][j]=max(f[i][j-1]+b[i][j]-b[i][j-1],f[i-1][j]+a[i][j]-a[i-1][j]);
	cout<<f[n][m]<<endl;
	return 0;
}
