#include<bits/stdc++.h>
#define int long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
inline int read(){
    int x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
int n,r,ans; 
int a[300100],b[300100]; 
inline int lowbit(int x){return x&(-x);}
inline void add(int x)
{
	for(;x<=n;x+=lowbit(x))b[x]++;
	return;
}
inline int query(int x) 
{
	int res=0; 
	for(;x;x-=lowbit(x))res+=b[x]; 
	return res; 
}
signed main() 
{
	freopen("change.in", "r", stdin); 
	freopen("change.out", "w", stdout); 
	n=read();r=n; 
	For(i,1,n) 
	{         
		int x=read(); 
		a[x]=1; 
		if(x==r)while(a[r]==1)r--;  
		add(x); 
		ans=query(r);
		cout<<ans<<" ";
	}
}
