#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <queue>
#include <vector>
#define file "magic"
#define ull unsigned long long
#define ll long long
using namespace std;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,k,f[100009],pos[100009];
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	bool flag=0;
	n=read();k=read();
	if(k==1){
		for(int i=1;i<=n;i++)
			printf("%d ",i);
		printf("\n");
		return 0;
	}
	for(int i=1;i<=n;i++)pos[f[i]=read()]=i;
	while(1){
		flag=1;
		for(int i=1;i<n;i++){
			if(pos[i+1]<=pos[i]-k){
				swap(f[pos[i+1]],f[pos[i]]);
				swap(pos[i+1],pos[i]);
				flag=0;

			}
		}
		if(flag)break;
	}
	for(int i=1;i<=n;i++)
		printf("%d ",f[i]);
	printf("\n");
	return 0;
}
/* 题意：
 * 给定一个排列，每次可以交换两个元素，要求元素之差的
 * 绝对值等于1，并且两个元素的距离不能小于k。
 * 求交换得到字典序最小的序列。
 * 发现做法是O(n)或是O(logn)的。
 * 应该会有贪心做法？
 * 
 */ 
/* 
4 2
4 2 3 1

8 3
4 5 7 8 3 1 2 6

*/

