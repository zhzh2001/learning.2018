#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <stdlib.h>
#include <queue>
#include <vector>
#define file "zhenxiang"
#define ull unsigned long long
#define ll long long
using namespace std;
const int N=100009;
ll read(){
	char c;ll num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,m,s,fa[30][N],d[N];
ll w[N],dis[N];
int head[N],nxt[N*3],ver[N*3],tot=1;
int leaf[N],line[N],child[N];
void add(int u,int v){
	ver[++tot]=v;nxt[tot]=head[u];head[u]=tot;
	ver[++tot]=u;nxt[tot]=head[v];head[v]=tot;
}
void dfs(int x,int pre){
	for(int i=1;i<=25;i++)
		fa[i][x]=fa[i-1][fa[i-1][x]];
	//倍增父节点 
	for(int i=head[x];i;i=nxt[i]){
		int y=ver[i];
		if(y==pre)continue;
		leaf[x]=1;
		//不是叶子 
		d[y]=d[x]+1;
		fa[0][y]=x;
		dis[y]=dis[x]+w[y];
		//权值和 
		dfs(y,x);
	}
}
int LCA(int x,int y){
	if(d[x]<d[y])swap(x,y);
	int r=d[x]-d[y];
	for(int i=0;i<23;i++)
		if((1<<i)&r)x=fa[i][x];
	if(x==y)return x;
	for(int i=23;i>=0;i--){
		if(fa[i][x]!=fa[i][y]){
			x=fa[i][x];
			y=fa[i][y];
		}
	}
	//cout<<x<<endl;
	return fa[0][x];
}
ll len(int x,int y,int &p){
	p=LCA(x,y);
	return 1ll*dis[x]+dis[y]-2*dis[p]+w[p];
}
void clear(int x){
	child[x]=1;
	for(int i=head[x];i;i=nxt[i]){
		int y=ver[i];
		if(y==fa[0][x])continue;
		clear(y);
	}
}
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	srand(19260817);
	n=read();s=rand()%n+1;
	for(int i=1;i<=n;i++)w[i]=read();
	for(int i=1;i<n;i++)add(read(),read());
	fa[0][s]=s;dis[s]=w[s];d[s]=1;dfs(s,-1);
	int p,xx,yy,t;
	ll maxn=0,tmp;
	for(int i=1;i<=n;i++)if(!leaf[i])
		for(int j=i;j<=n;j++)if(!leaf[j])
			if((tmp=len(i,j,t))>maxn)
				xx=i,yy=j,p=t,maxn=tmp;
	//cout<<len(7,9,p)<<endl;
	for(int i=xx;i!=p;i=fa[0][i])line[i]=1;
	for(int i=yy;i!=p;i=fa[0][i])line[i]=1;line[p]=1;
	//标记链 
	ll ans=maxn;
	//cout<<ans<<endl;
	//cout<<ans<<"  "<<xx<<"  "<<yy<<"  "<<p<<endl;
	maxn=0;
	for(int i=1;i<=n;i++)
		if(!leaf[i]&&i!=xx&&i!=yy){
			int k,dd=-1,t;
			if(d[(t=LCA(i,xx))]>dd)k=t,dd=d[t];
			//跟左端点求LCA 
			if(d[(t=LCA(i,yy))]>dd)k=t,dd=d[t];
			//跟右端点求LCA，取深度最大值
			if(d[k]<d[p])k=p;
			maxn=max(maxn,len(k,i,t)-w[k]); 
			//if(maxn==13)cout<<i<<"  "<<k<<"  "<<endl;
		}
	//所有点到链的最长链 
	clear(p);
	//标记子树
	//子树内部的最长链
	for(int i=1;i<=n;i++)if(!leaf[i]&&child[i]&&!line[i]){
		for(int j=1;j<=n;j++)if(!leaf[j]&&child[j]&&!line[j]){
			if(!line[LCA(i,j)]){
				int qwq;
				maxn=max(maxn,len(i,j,qwq));
			}
		}
	} 
	for(int i=1;i<=n;i++)if(!leaf[i]&&!child[i]){
		//maxn=max(maxn,len(i,p,t));
		for(int j=i;j<=n;j++)if(!leaf[j]&&!child[j])
			maxn=max(maxn,len(i,j,t));
		//cout<<i<<"  "<<maxn<<endl;
		//非子树的两个组成的链肯定不重复 
	}
	ans+=maxn;
	printf("%lld\n",ans);
	return 0;
}
/* 要求树上两条不重合的链的最大权值 
 * 我们发现开始节点肯定是根节点。
 * 如果不是根节点的话，我们可以往上走也可以往下走。
 * 肯定有一边没有吃到
 * 终点肯定也是根节点。
 * 先预处理出每个点到根的距离，然后O(n^2logn)LCA求出最大链长
 * 然后求出所有其他点叶子节点到链的最大链长
 * 方法是跟两端点同时求LCA，深度最大的那一个就是 
 * 然后标记LCA的所有叶子节点。
 * 再暴力枚举两个根节点求最大链长
 * 再求一下到LCA的链长
 * 答案就是第一条的链长加上第二条的链长
 * 70分应该能拿到 
 */ 
/* leaf表示是否为叶子0为是，1为否
 * dis表示到根节点（包括自己）的权值和
 */ 
/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
*/
