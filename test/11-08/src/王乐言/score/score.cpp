#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <queue>
#include <vector>
#define file "score"
#define ull unsigned long long
#define ll long long
using namespace std;
const int base=2000000;
const int mod=1e9+7;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int a,b,k,t,minn,maxn,pos;
int f[4010009][2],n=0;
int sum[4010009][2];
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	memset(f,0,sizeof(f));
	//printf("%.3f",(1.0*sizeof(f)/1024/1024));
	a=read();b=read();
	k=read();t=read();
	f[a-b+base][0]=1;
	minn=maxn=a-b+base;
	for(int i=0;i<t;i++){
		for(int j=-2*k*i-209;j<=2*k*i+209;j++)
			f[base+j][(i+1)%2]=0;
		for(int j=-2*k*i-209;j<=2*k*i+209;j++){
			for(int del=-2*k;del<=2*k;del++){
				pos=base+j+del;
				f[pos][(i+1)%2]=f[pos][(i+1)%2]+(2*k-abs(del)+1)*f[base+j][i%2]%mod;
				if(f[pos][(i+1)%2]>=mod)f[pos][(i+1)%2]-=mod;
			}
		}
		/*for(int j=-2*k*t-209;j<=2*k*t+209;j++)
			sum[j][(i+1)%2]=sum[j-1][(i+1)%2]+f[j][(i+1)%2];*/
	}
	int ans=0;
	for(int i=1;i<=2*k*t;i++){
		ans=ans+f[base+i][t%2];
		if(ans>=mod)ans-=mod;
	}
	printf("%d\n",ans);
	return 0;
}
/* 发现k*t比较小，变化的数不超过 4000*100=4e5
 * 考虑是不是能跑一个背包
 * f[i]表示xsbl比sw_wind多i分时的方案数
 * 枚举两个人一次打靶的分数差值，然后统计一下 
 * 我们发现分数差值=2k的有一个，=2k-1的有两个
 * =0的有2k+1个，=-1的有2k个...
 * 那么每次递推的时候就相应乘上系数就可以了。
 * 空间不够。。  滚动数组。。
 * 每滚一次清空一次。。
 * 不在乎常数了。 
 * 发现爆了，回头再优化吧 
 */ 
