#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("magic.in");
ofstream fout("magic.out");
const int N=100005;
int a[N];
int main()
{
	int n,k;
	fin>>n>>k;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(;;)
	{
		bool flag=false;
		for(int i=1;i<=n;i++)
			for(int j=i+k;j<=n;j++)
				if(abs(a[i]-a[j])==1&&a[i]>a[j])
				{
					swap(a[i],a[j]);
					flag=true;
				}
		if(!flag)
			break;
	}
	for(int i=1;i<=n;i++)
		fout<<a[i]<<' ';
	fout<<endl;
	return 0;
}
