#include<fstream>
#include<algorithm>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("magic.in");
const int n=6000;
int a[n+5];
int main()
{
	minstd_rand gen(GetTickCount());
	uniform_int_distribution<> d(1,n-1);
	int k=d(gen);
	fout<<n<<' '<<k<<endl;
	for(int i=1;i<=n;i++)
		a[i]=i;
	shuffle(a+1,a+n+1,gen);
	for(int i=1;i<=n;i++)
		fout<<a[i]<<' ';
	fout<<endl;
	return 0;
}
