#include<fstream>
#include<algorithm>
#include<queue>
using namespace std;
ifstream fin("magic.in");
ofstream fout("magic.out");
const int N=100005,INF=1e9;
int n,k,a[N],b[N],tree[N*4],head[N],v[N*2],nxt[N*2],e,ind[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void build(int id,int l,int r)
{
	tree[id]=INF;
	if(l<r)
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
	}
}
void modify(int id,int l,int r,int x,int val)
{
	if(l==r)
		tree[id]=val;
	else
	{
		int mid=(l+r)/2;
		if(x<=mid)
			modify(id*2,l,mid,x,val);
		else
			modify(id*2+1,mid+1,r,x,val);
		tree[id]=min(tree[id*2],tree[id*2+1]);
	}
}
int query(int id,int l,int r,int L,int R)
{
	if(L<=l&&R>=r)
		return tree[id];
	int mid=(l+r)/2;
	if(R<=mid)
		return query(id*2,l,mid,L,R);
	if(L>mid)
		return query(id*2+1,mid+1,r,L,R);
	return min(query(id*2,l,mid,L,R),query(id*2+1,mid+1,r,L,R));
}
int main()
{
	fin>>n>>k;
	if(k==1)
	{
		for(int i=1;i<=n;i++)
			fout<<i<<' ';
		fout<<endl;
		return 0;
	}
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		b[a[i]]=i;
	}
	build(1,1,n);
	for(int i=n;i;i--)
	{
		if(b[i]>1)
		{
			int res=query(1,1,n,max(b[i]-k+1,1),b[i]-1);
			if(res<INF)
			{
				add_edge(b[i],b[res]);
				ind[b[res]]++;
			}
		}
		if(b[i]<n)
		{
			int res=query(1,1,n,b[i]+1,min(b[i]+k-1,n));
			if(res<INF)
			{
				add_edge(b[i],b[res]);
				ind[b[res]]++;
			}
		}
		modify(1,1,n,b[i],i);
	}
	priority_queue<int,vector<int>,greater<int> > Q;
	for(int i=1;i<=n;i++)
		if(!ind[i])
			Q.push(i);
	int cc=0;
	while(!Q.empty())
	{
		int k=Q.top();Q.pop();
		a[k]=++cc;
		for(int i=head[k];i;i=nxt[i])
			if(!--ind[v[i]])
				Q.push(v[i]);
	}
	for(int i=1;i<=n;i++)
		fout<<a[i]<<' ';
	fout<<endl;
	return 0;
}
