#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("score.in");
ofstream fout("score.out");
const int M=200005,MOD=1e9+7;
int f[M],g[M];
int main()
{
	int a,b,k,t;
	fin>>a>>b>>k>>t;
	int bas=k*t+1,cc=0;
	for(int i=bas-k;i<=bas+k;i++)
		f[i]=++cc;
	for(int i=2;i<=t;i++)
	{
		for(int j=-i*k;j<=i*k;j++)
		{
			int l=max(j-k,-(i-1)*k),r=min(j+k,(i-1)*k);
			g[bas+j]=((g[bas+j-1]+f[bas+r])%MOD-f[bas+l-1]+MOD)%MOD;
		}
		for(int j=bas-i*k;j<=bas+i*k;j++)
			f[j]=g[j];
	}
	int ans=0;
	for(int i=-k*t;i<=k*t;i++)
	{
		int j=max(bas+b-a+i,bas-k*t-1);
		if(j<bas+k*t)
			ans=(ans+1ll*(f[bas+i]-f[bas+i-1]+MOD)*(f[bas+k*t]-f[j]+MOD))%MOD;
	}
	fout<<ans<<endl;
	return 0;
}
