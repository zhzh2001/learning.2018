#include<fstream>
using namespace std;
ifstream fin("score.in");
ofstream fout("score.ans");
const int MOD=1e9+7;
int a,b,k,t,ans;
void dfs2(int step,int score,int ascore)
{
	if(step==t+1)
		ans=(ans+(ascore>score))%MOD;
	else
		for(int i=-k;i<=k;i++)
			dfs2(step+1,score+i,ascore);
}
void dfs(int step,int score)
{
	if(step==t+1)
		dfs2(1,b,score);
	else
		for(int i=-k;i<=k;i++)
			dfs(step+1,score+i);
}
int main()
{
	fin>>a>>b>>k>>t;
	dfs(1,a);
	fout<<ans<<endl;
	return 0;
}
