#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("zhenxiang.in");
ofstream fout("zhenxiang.out");
const int N=100005;
int a[N],head[N],v[N*2],nxt[N*2],e;
long long diaroot,diaroot2,f[N],g[N],ans,dia[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int u,int fat)
{
	f[u]=g[u]=dia[u]=0;
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			dfs(v[i],u);
			if(f[v[i]]+a[v[i]]>=f[u])
			{
				g[u]=f[u];
				f[u]=f[v[i]]+a[v[i]];
			}
			else if(f[v[i]]+a[v[i]]>g[u])
				g[u]=f[v[i]]+a[v[i]];
			dia[u]=max(dia[u],dia[v[i]]);
		}
	dia[u]=max(dia[u],a[u]+f[u]+g[u]);
}
void dfs2(int u,int fat,long long len,long long diaroot)
{
	ans=max(ans,len+diaroot);
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			if(f[u]==f[v[i]]+a[v[i]])
				ans=max(ans,len+g[u]+dia[v[i]]);
			else
				ans=max(ans,len+f[u]+dia[v[i]]);
			dfs2(v[i],u,len+a[v[i]],diaroot);
		}
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(int i=1;i<n;i++)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	for(int i=1;i<=n;i++)
	{
		dfs(i,0);
		diaroot=diaroot2=0;
		for(int j=head[i];j;j=nxt[j])
			if(dia[v[j]]>=diaroot)
			{
				diaroot2=diaroot;
				diaroot=dia[v[j]];
			}
			else if(dia[v[j]]>diaroot2)
				diaroot2=dia[v[j]];
		ans=max(ans,a[i]+diaroot);
		for(int j=head[i];j;j=nxt[j])
			if(dia[v[j]]==diaroot)
				dfs2(v[j],i,a[i]+a[v[j]],diaroot2);
			else
				dfs2(v[j],i,a[i]+a[v[j]],diaroot);
	}
	fout<<ans<<endl;
	return 0;
}
