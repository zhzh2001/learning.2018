#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
int n,a[100001],u,v,mark[100001],edg[100001],head[100001],nxt[100001],tot=0,used[100001];
long long son[100001],ans=0,t=0,q=0;
void add(int x,int y){
	tot++;
	edg[tot]=y;
	nxt[tot]=head[x];
	head[x]=tot;
}
void dfs(int x,int from,long long p){
	for(int i=head[x];i;i=nxt[i]){
		if(!used[edg[i]]&&edg[i]!=from){
			dfs(edg[i],x,0);
			if(p<son[edg[i]]){
				p=son[edg[i]];
				mark[x]=edg[i];
			}
		}
	}
	son[x]+=p;
}
int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
    cin>>n;
    for(int i=1;i<=n;i++)cin>>a[i];
	for(int i=1;i<n;i++){
		cin>>u>>v;
		add(u,v);
		add(v,u);
	}
	for(int i=1;i<=n;i++){
		t=0,q=0;
		for(int j=1;j<=n;j++)son[j]=a[j],used[j]=0,mark[j]=0;
		dfs(i,0,0);
		t+=son[i];
		for(int j=i;j;j=mark[j])used[j]=1;
		for(int k=1;k<=n;k++){
			if(used[k])continue;
			for(int j=1;j<=n;j++)son[j]=a[j];
			dfs(k,0,0);
			if(q<son[k])q=son[k];
		}
		t+=q;
		if(ans<t)ans=t;
	}
	cout<<ans<<endl;
    return 0;
}
