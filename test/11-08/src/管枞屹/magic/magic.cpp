#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
const int mod=500004;
int n,k,a[100001],l[100001],pre[100001],nxt[100001],q[500005],s=0,t=0;
int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
    cin>>n>>k;
    if(k==1){for(int i=1;i<n;i++){cout<<i<<" ";}cout<<n<<endl;return 0;}
    for(int i=1;i<=n;i++)cin>>a[i],l[a[i]]=i;
	for(int i=2;i<=n;i++){
		pre[i]=l[i-1];
		if(l[i-1]-l[i]>=k)q[++t]=i-1;
	}
	for(int i=1;i<n;i++)nxt[i]=l[i+1];
	pre[1]=-1;nxt[n]=-1;
	while(s!=t){
		int d=q[s+1];
		if(l[d]-l[d+1]>=k){
			a[l[d]]=d+1,a[l[d+1]]=d;
			swap(l[d],l[d+1]);
			if(d-1>=1){
				nxt[d-1]=l[d];
				if(l[d-1]-l[d+1]<k&&l[d-1]-l[d]>=k){
					t++;
					if(t>mod)t-=mod;
					q[t]=d-1;
				}
			}
			if(d+2<=n){
				pre[d+2]=l[d+1];
				if(l[d]-l[d+2]<k&&l[d+1]-l[d+2]>=k){
					t++;
					if(t>mod)t-=mod;
					q[t]=d+1;
				}
			}
			nxt[d]=l[d+1];
			pre[d+1]=l[d];
		}
		s++;
	}
	for(int i=1;i<n;i++)cout<<a[i]<<" ";
	cout<<a[n]<<endl;
    return 0;
}
