#include <iostream>
#include <cstdio>
using namespace std;
const int mod=1000000007;
int a,b,k,t,f[2][500005],m[5005];
int main(){
    freopen("score.in","r",stdin);
    freopen("score.out","w",stdout);
    cin>>a>>b>>k>>t;
    for(int i=0;i<=2*k;i++)f[1][i]=m[i]=i+1;
    for(int i=2*k+1;i<=4*k;i++)f[1][i]=m[i]=4*k+1-i;
    int sum=b-a+2*k*t+1;
    long long ans=0;
    for(int i=2;i<=t;i++){
    	for(int j=min(0,sum-4*k*(t-i+1));j<=4*k*(i-1);j++){
    		for(int p=0;p<=4*k;p++){
    			if(j+p>=sum-4*k*(t-i))f[i&1][j+p]=(f[i&1][j+p]+f[(i-1)&1][j]*m[p])%mod;
    		}
    	}
    }
    for(int i=sum;i<=4*k*t;i++)ans=(ans+f[t&1][i])%mod;
    cout<<ans<<endl;
    return 0;
}
