#include <cstdio>
#include <cctype>
#include <algorithm>
#include <cstring>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 200005
int n, m, a[N], p[N];
struct Segment_Tree{
	int val[N << 2];
	void build(int u, int l, int r){
		val[u] = n + 1;
		if (l == r) return;
		int mid = (l + r) >> 1;
		build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
	}
	void modify(int u, int l, int r, int x, int y){
		if (l == r) return val[u] = y, (void)0;
		int mid = (l + r) >> 1;
		if (x <= mid) modify(u << 1, l, mid, x, y);
		else modify(u << 1 | 1, mid + 1, r, x, y);
		val[u] = std :: min(val[u << 1], val[u << 1 | 1]);
	}
	int query(int u, int l, int r, int L, int R){
		if (L <= l && r <= R) return val[u];
		int mid = (l + r) >> 1, ans = n + 1;
		if (L <= mid) ans = std :: min(ans, query(u << 1, l, mid, L, R));
		if (R > mid) ans = std :: min(ans, query(u << 1 | 1, mid + 1, r, L, R));
		return ans;
	}
}T;
int edge, to[N << 1], pr[N << 1], hd[N], d[N];
void addedge(int u, int v){
	to[++edge] = v, pr[edge] = hd[u], hd[u] = edge, ++d[v];
}
struct Heap{
	int h[N];
	int sz;
	bool empty(){ return !sz; }
	void push(int x){ h[++sz] = -x, std :: push_heap(h + 1, h + 1 + sz); }
	int pop(){ return std :: pop_heap(h + 1, h + 1 + sz), -h[sz--]; }
}H;
void toposort(){
	for (register int i = 1; i <= n; ++i) if (!d[i]) H.push(i);
	m = 0;
	while (!H.empty())
		for (register int i = hd[a[++m] = H.pop()], v; i; i = pr[i])
			if (!(--d[v = to[i]])) H.push(v);
}
int main(){
	freopen("magic.in", "r", stdin);
	freopen("magic.out", "w", stdout);
	n = read(), m = read();
	for (register int i = 1; i <= n; ++i) a[p[i] = read()] = i;
	T.build(1, 1, n);
	for (register int i = n; i; --i){
		register int j = T.query(1, 1, n, a[i], std :: min(a[i] + m - 1, n));
		if (j != n + 1) addedge(a[i], a[j]);
		j = T.query(1, 1, n, std :: max(a[i] - m + 1, 1), a[i]);
		if (j != n + 1) addedge(a[i], a[j]);
		T.modify(1, 1, n, a[i], i);
	}
	toposort();
	for (register int i = 1; i <= n; ++i) p[a[i]] = i;
	for (register int i = 1; i <= n; ++i) printf("%d ", p[i]);
}
