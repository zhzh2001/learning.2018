#include <cstdio>
#include <cctype>
#include <algorithm>
#include <cstring>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define P 1000000007
int a, b, k, n;
int dp[2][500005], *x = &dp[0][250000], *y = &dp[1][250000];
int main(){
	freopen("score.in", "r", stdin);
	freopen("score.out", "w", stdout);
	a = read(), b = read(), k = read(), n = read();
	x[a - b] = 1;
	for (register int i = 1; i <= n; ++i){
		std :: swap(x, y);
		memset(dp[i & 1], 0, sizeof dp[i & 1]);
		int s0 = 0, s1 = 0;
		int l = a - b - 2 * i * k, r = a - b + 2 * i * k;
		for (register int j = 0; j < 2 * k; ++j) s1 += y[l + j], s1 >= P ? s1 -= P : 0;
		for (register int j = l; j <= r; ++j)
			x[j] = (1ll * x[j - 1] - s0 + (s1 += y[j + 2 * k]) + P) % P, s0 = (1ll * s0 - y[j - 1 - 2 * k] + y[j] + P) % P, s1 = (1ll * s1 - y[j] + P) % P;
	}
	int ans = 0;
	for (register int i = a - b + 2 * n * k; i > 0; --i) ans += x[i], ans >= P ? ans -= P : 0;
	printf("%d", ans);
}
