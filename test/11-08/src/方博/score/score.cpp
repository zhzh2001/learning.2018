#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define M 200001
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=400005;
const int mod=1e9+7;
int a,b,k,t;
ll f[N];//表示某个人打靶打到x分的方案数
ll s[N];//前缀和
int main()
{
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	a=read(),b=read(),k=read(),t=read();
	f[M]=1;memset(s,0,sizeof(s));
	for(int i=1;i<=t;i++){
		int l=M-k*(i),r=M+k*(i);
		for(int j=l;j<=r;j++){
			s[j]=s[j-1]+f[j];
			if(s[j]>mod)s[j]%=mod;
		}
		for(int j=0;j<k;j++){
			f[l+j]=s[l+j+k]-s[l-1];
			if(f[l+j]<0)f[l+j]+=mod;
//			if(f[l+j]>mod)f[l+j]%=mod;
			f[r-j]=s[r]-s[r-j-k-1];
			if(f[r-j]<0)f[r-j]+=mod;
//			if(f[r-j]>mod)f[r-j]%=mod;
		}
		l+=k;r-=k;
		for(int j=l;j<=r;j++){
			f[j]=s[j+k]-s[j-k-1];
			if(f[j]<0)f[j]+=mod;
//			if(f[j]>mod)f[j]%=mod;
		}
//		for(int j=M-k*i;j<=M+k*i;j++)
//			cout<<M-j<<' '<<f[j]<<endl;
//		cout<<endl;
	}
	int l=M-k*t,r=M+k*t;
	for(int i=l;i<=r;i++){
		s[i]=s[i-1]+f[i];
		if(s[i]>mod)s[i]%=mod;
	}
	int v=a-b;
 	ll ans=0;
	for(int i=l+v;i<=r;i++){
		ans+=(ll)f[i]*s[i+v-1];
		if(ans>mod)ans%=mod;
//		cout<<i-M<<' '<<f[i]<<' '<<s[i+v-1]<<endl;
	}
	writeln(ans);
}
//复杂度O(2*k*t*t)
