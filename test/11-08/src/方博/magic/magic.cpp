#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=100005;
int n;
struct xxx{
	int k,i;
}a[N];
int p[N],k;
bool com(xxx a,xxx b)
{
	return a.k<b.k;
}
int main()
{
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n=read();k=read();
	for(int i=1;i<=n;i++)
		a[i].k=read(),a[i].i=i;
	if(k==1){
		for(int i=1;i<=n;i++)
			write(i),putchar(' ');
		return 0;
	}
	sort(a+1,a+n+1,com);
	for(int i=1;i<=n;i++){
		bool b=false;
		for(int j=n;j>i;j--)
			if(a[j-1].i-a[j].i>=k)
				swap(a[j],a[j-1]),b=true;
		if(b==false)break;
	}
	for(int i=1;i<=n;i++)
		write(a[i].i),putchar(' ');
}
