#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=100005;
int head[N],nxt[2*N],tail[2*N],tot;
inline void addto(int x,int y){
	tail[++tot]=y;
	nxt[tot]=head[x];
	head[x]=tot;
}
ll a[N];
ll f[N][5],ans,n;
void dfs(int k,int fa)
{
	f[k][0]=f[k][2]=f[k][3]=a[k];
	for(int i=head[k];i;i=nxt[i]){
		int v=tail[i];
		if(v==fa)continue;
		dfs(v,k);
		
		ans=max(ans,f[k][3]+f[v][0]);
		ans=max(ans,f[k][2]+max(f[v][4],f[v][2]));
		ans=max(ans,f[v][3]+f[k][0]);
		ans=max(ans,max(f[v][4],f[v][2])+f[k][4]);
		
		f[k][3]=max(f[k][3],f[v][0]+a[k]+f[k][4]);
		f[k][3]=max(f[k][3],max(f[v][4],f[v][2])+f[k][0]);
		f[k][3]=max(f[k][3],f[v][3]+a[k]);
		f[k][3]=max(f[k][3],max(f[v][4],f[v][2])+a[k]);
		
		ans=max(ans,f[k][3]);
		
		if(f[v][0]+a[k]>f[k][0])f[k][1]=f[k][0],f[k][0]=f[v][0]+a[k];
		else if(f[v][0]+a[k]>f[k][1])f[k][1]=f[v][0]+a[k];
		
		f[k][2]=max(f[k][0],f[k][0]+f[k][1]-a[k]);
		f[k][4]=max(f[k][4],max(f[v][4],f[v][2]));
		
		ans=max(ans,f[k][2]);
		ans=max(ans,f[k][0]);
	}
}
int main()
{
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
	}
	for(int i=1;i<=n-1;i++){
		int u,v;
		u=read(),v=read();
		addto(u,v),addto(v,u);
	}
	dfs(1,0);
	writeln(ans);
	return 0;
}
/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
*/
