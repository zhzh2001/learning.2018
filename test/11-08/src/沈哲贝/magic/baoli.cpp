#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define abs(x)		((x)<0?(-(x)):(x))
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define pa			pair<ll,ll>
#define mk			make_pair
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)	putchar('-'),x=-x;
	if (x>=10)	write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);	puts("");
}
const ll N=6010;
bitset<N>mp[N];
ll cnt,n,k,del[N],ans[N],a[N],b[N];
void dfs(ll x){
	if (del[x])return;
	For(i,1,n)if (mp[x][i]&&(x!=i)){
		dfs(i);
	}
	del[x]=1;	ans[x]=++cnt;
}
int main(){
	freopen("magic.in","r",stdin);
	freopen("baoli.out","w",stdout);
	srand(time(0));
	n=read();	k=read();
	For(i,1,n)a[b[i]=read()]=i;
	For(i,1,n){
		mp[a[i]][a[i]]=1;
		For(j,1,i-1)
			if (abs(a[j]-a[i])<k)	mp[a[i]]|=mp[a[j]];
	} 
//	For(i,1,n)write(a[i]),putchar(' ');puts("");
/*	For(i,1,n){
		For(j,1,n)write(mp[i][j]),putchar(' ');
		puts("");
	}*/
	For(i,1,n)dfs(i);
	For(i,1,n)write(ans[i]),putchar(' ');
//	printf("%lf\n",1.0*clock()/CLOCKS_PER_SEC);
//	puts("");
}
/*
������
n,n-1,n-2 
8 3
4 5 7 8 3 1 2 6
0 */
