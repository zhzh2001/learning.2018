#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define abs(x)		((x)<0?(-(x)):(x))
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define pa			pair<ll,ll>
#define mk			make_pair
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)	putchar('-'),x=-x;
	if (x>=10)	write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);	puts("");
}
const ll N=400010;
vector<ll>g[N];
pa mn[N];
ll a[N],ans[N],n,k,cnt;
bool operator <(pa a,pa b){return a.first<b.first;}
void Min(pa &x,pa y){x=min(x,y);}
void build(ll l,ll r,ll p){
	mn[p]=mk(n+1,n+1);
	if(l==r)return;		ll mid=(l+r)>>1;
	build(l,mid,p<<1);	build(mid+1,r,p<<1|1);
}
void change(ll l,ll r,ll p,ll pos,ll val){
	if (l==r){
		mn[p]=mk(val,pos);
		return;
	}ll mid=(l+r)>>1;
	if (pos<=mid)	change(l,mid,p<<1,pos,val);
	else			change(mid+1,r,p<<1|1,pos,val);
	mn[p]=min(mn[p<<1],mn[p<<1|1]);
}
pa query(ll l,ll r,ll p,ll s,ll t){
	if (s<=l&&r<=t)	return mn[p];
	ll mid=(l+r)>>1; pa ans=mk(n+1,n+1);
	if (s<=mid)	Min(ans,query(l,mid,p<<1,s,t));
	if (t>mid)	Min(ans,query(mid+1,r,p<<1|1,s,t));
	return ans;
}
void dfs(ll x){
	sort(g[x].begin(),g[x].end());
	rep(i,0,g[x].size())
		dfs(g[x][i]);
	ans[x]=++cnt;
}
int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	srand(time(0));
	n=read();	k=read();
	For(i,1,n)a[read()]=i;
	build(0,n,1);
	FOr(i,n,1){
		g[query(0,n,1,a[i]-k+1,a[i]-1).second].push_back(a[i]);
		change(0,n,1,a[i],i);
	}
	dfs(n+1);
	For(i,1,n)write(ans[i]),putchar(' ');
//	printf("%lf\n",1.0*clock()/CLOCKS_PER_SEC);
//	puts("");
}
/*
������
n,n-1,n-2 
8 3
4 5 7 8 3 1 2 6

4 2
4 2 3 1
0 */
