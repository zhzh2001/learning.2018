#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define abs(x)		((x)<0?(-(x)):(x))
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define pa			pair<ll,ll>
#define mk			make_pair
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)	putchar('-'),x=-x;
	if (x>=10)	write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);	puts("");
}
const ll N=210010,mod=1e9+7;
ll f[N],g[N],n,a,b,K,k,total,ans;
int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	b=read();a=read();K=(k=read())*2+1;n=read();
	f[0]=1;total=0;
	For(i,1,n){
		memset(g,0,sizeof g);
		total+=K;
		For(j,0,total){
			if (j)Add(f[j],f[j-1]);	g[j]=f[j];
			if (j>=K)Add(g[j],-f[j-K]);
		}memcpy(f,g,sizeof g);
	}
	a-=b;	++a;
	FOr(i,total,0)	Add(f[i],f[i+1]);
//	For(i,0,total)	write(g[i]),putchar(' ');puts("");
	For(i,0,total)	Add(ans,f[max(0ll,i+a)]%mod*g[i]);//cout<<f[max(0ll,i+a)]<<g[i]<<endl;
	Add(ans,mod);
	writeln(ans);
//	printf("%lf\n",1.0*clock()/CLOCKS_PER_SEC);
}
