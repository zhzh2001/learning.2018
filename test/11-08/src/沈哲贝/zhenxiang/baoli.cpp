#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define abs(x)		((x)<0?(-(x)):(x))
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define pa			pair<ll,ll>
#define mk			make_pair
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)	putchar('-'),x=-x;
	if (x>=10)	write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);	puts("");
}
const ll N=200010;
ll x[N],y[N],a[N],dis[N],mark[N],head[N],nxt[N],vet[N],vis[N],n,ans,tot;
void insert(ll x,ll y){
	nxt[++tot]=head[x];head[x]=tot;vet[tot]=y;mark[tot]=1;
}
void dfs(ll x,ll sum){
	vis[x]=1;	dis[x]=sum;
	for(ll i=head[x];i;i=nxt[i])if (!vis[vet[i]]&&mark[i])
		dfs(vet[i],sum+a[vet[i]]);
	vis[x]=0;
}
ll calc(ll x){
	For(i,1,n)dis[i]=0;
	dfs(x,a[x]);
	For(i,1,n)if (dis[i]>dis[x])x=i;
	For(i,1,n)dis[i]=0;
	dfs(x,a[x]);
	For(i,1,n)if (dis[i]>dis[x])x=i;
	For(i,1,n)dis[i]=0;
	dfs(x,a[x]);
	For(i,1,n)if (dis[i]>dis[x])x=i;
	return dis[x];
}
int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,1,n)a[i]=read();
	For(i,2,n){
		x[i]=read();		y[i]=read();
		insert(x[i],y[i]);	insert(y[i],x[i]);
	}
	For(i,2,n){
		mark[i*2-3]=mark[i*2-2]=0;
		Max(ans,calc(x[i])+calc(y[i]));
		mark[i*2-3]=mark[i*2-2]=1;
//		if (ans==8)cout<<x[i]<<' '<<y[i]<<endl;
	}
	writeln(ans);
}
/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
*/
