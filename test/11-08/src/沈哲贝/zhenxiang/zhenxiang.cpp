#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define abs(x)		((x)<0?(-(x)):(x))
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define pa			pair<ll,ll>
#define mk			make_pair
void Max(ll &x,ll y){x=x<y?y:x;}
void Min(ll &x,ll y){x=x<y?x:y;}
ll read(){
	ll x=0,f=1;	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';
	return x*f;
}
void write(ll x){
	if (x<0)	putchar('-'),x=-x;
	if (x>=10)	write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);	puts("");
}
const ll N=400010;
vector<ll>edge[N];
ll lzh[N],f[N],g[N],h[N],F[N],G[N],H[N],a[N],ans,n;
void Fault(ll x,ll to){
	F[x]=f[x];	G[x]=g[x];	H[x]=h[x];
	
	Max(H[x],lzh[x]+f[to]+a[x]);
	Max(ans,lzh[x]+g[to]);

	Max(F[x],f[to]+a[x]);
	Max(G[x],f[to]+f[x]);
	Max(ans,f[to]+g[x]);
	Max(ans,f[to]+h[x]);
	
	Max(H[x],g[to]+f[x]);
	Max(ans,g[to]+g[x]);
	
	Max(H[x],h[to]+a[x]);
	Max(ans,h[to]+f[x]);
	
	f[x]=F[x];	g[x]=G[x];	h[x]=H[x];
	
	Max(lzh[x],g[to]);
}
void dfs(ll x,ll fa){
	f[x]=g[x]=h[x]=a[x];Max(ans,a[x]);
	rep(i,0,edge[x].size())if (edge[x][i]!=fa){
		dfs(edge[x][i],x);
		Fault(x,edge[x][i]);
	}Max(g[x],lzh[x]);Max(g[x],f[x]);Max(h[x],g[x]);Max(ans,h[x]);
}
int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	For(i,1,n)a[i]=read();

	For(i,2,n){
		ll x=read(),y=read();
		edge[x].push_back(y);edge[y].push_back(x);
	}
	dfs(1,0);
//	For(i,1,n)cout<<f[i]<<' '<<g[i]<<' '<<h[i]<<endl;
	writeln(ans);
}
