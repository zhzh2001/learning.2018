#include<iostream>
#include<algorithm>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<queue>
#include<vector>
#include<fstream>
using namespace std;
ifstream fin("score.in");
ofstream fout("score.out");
const long long mod=1000000007;
inline long long read(){
	long long x=0;char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-48,ch=getchar();
	return x;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+48);
}
inline void writeln(long long x){
	write(x),puts("");
}
long long a,b,k,t,ans;
long long f[400005],sum[400005];
int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	a=read(),b=read(),k=read(),t=read();
	f[k*t+1]=1;
	for(register int i=1;i<=t;++i){
		int l=k*t+1-k*i,r=k*t+1+k*i;
		for(register int j=l;j<=r;++j){
			sum[j]=(sum[j-1]+f[j])%mod;
		}
		for(register int j=0;j<=k-1;++j){
			f[l+j]=(sum[l+j+k]-sum[l-1]+mod)%mod;
			f[r-j]=(sum[r]-sum[r-j-k-1]+mod)%mod;
		}
		l+=k,r-=k;
		for(register int j=l;j<=r;++j){
			f[j]=(sum[j+k]-sum[j-k-1]+mod)%mod;
		}
	}
	int l=k*t+1-k*t,r=k*t+1+k*t;
	for(register int i=l;i<=r;++i){
		sum[i]=(sum[i-1]+f[i])%mod;
	}
	for(register int i=l+a-b;i<=r;++i){
		ans=(ans+sum[i+a-b-1]*f[i]%mod)%mod;
	}
	writeln(ans);
	return 0;
}
/*

in:
1 1 1 2

out:
31

*/
