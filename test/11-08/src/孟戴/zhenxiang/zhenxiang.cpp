#include<iostream>
#include<algorithm>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<queue>
#include<vector>
#include<fstream>
using namespace std;
ifstream fin("zhenxiang.in");
ofstream fout("zhenxiang.out");
inline long long max(long long &a,long long &b){
	return a>b?a:b;
}
inline long long read(){
	long long x=0;char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-48,ch=getchar();
	return x;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+48);
}
inline void writeln(long long x){
	write(x),puts("");
}
int head[100003],nxt[200003],to[200003],tot;
inline void add(int x,int y){
	to[++tot]=y,nxt[tot]=head[x],head[x]=tot;
}
long long a[100003];
long long f[100003][6],ans,n;
/*
f[i][0]最长链，某个节点在根节点 
f[i][1]次长链，并且某个节点在根节点
f[i][2]最长链，经过根节点 即f[i][2]=max(f[i][0],f[i][0]+f[i][1]-a[i]);
f[i][3]有一条链+一条某个节点在根节点的情况下的最长链 f[i][3]=f[son[i]][2]+f[otherson[i]][0]+a[i],f[i][3]=max(f[son[i]][4],f[son[i]][2])+a[i]
f[i][4]某个节点的儿子当中的最长链f[i][4]=max(f[son[i]][2]);
*/
void dfs(int x,int fa){
	f[x][0]=f[x][2]=f[x][3]=a[x];
	for(register int i=head[x];i;i=nxt[i]){
		if(to[i]!=fa){
			dfs(to[i],x);
			ans=max(ans,f[x][3]+f[to[i]][0]);
			ans=max(ans,f[x][2]+max(f[to[i]][4],f[to[i]][2]));
			ans=max(ans,f[to[i]][3]+f[x][0]);
			ans=max(ans,max(f[to[i]][4],f[to[i]][2])+f[x][4]);
			f[x][3]=max(f[x][3],f[to[i]][0]+a[x]+f[x][4]);
			f[x][3]=max(f[x][3],max(f[to[i]][4],f[to[i]][2])+f[x][0]);
			f[x][3]=max(f[x][3],f[to[i]][3]+a[x]);
			f[x][3]=max(f[x][3],max(f[to[i]][4],f[to[i]][2])+a[x]);
			ans=max(ans,f[x][3]);
			if(f[to[i]][0]+a[x]>f[x][0]){
				f[x][1]=f[x][0],f[x][0]=f[to[i]][0]+a[x];
			}else if(f[to[i]][0]+a[x]>f[x][1]){
				f[x][1]=f[to[i]][0]+a[x];
			}
			f[x][2]=max(f[x][0],f[x][0]+f[x][1]-a[x]);
			f[x][4]=max(f[x][4],max(f[to[i]][4],f[to[i]][2]));
			ans=max(ans,f[x][2]);
			ans=max(ans,f[x][0]);
		}
	}
}
int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	for(register int i=1;i<=n;++i){
		a[i]=read();
	}
	for(register int i=1;i<=n-1;++i){
		int u,v;
		u=read(),v=read();
		add(u,v),add(v,u);
	}
	dfs(1,0);
	writeln(ans);
	return 0;
}
/*

in:
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9

out:
25

*/
