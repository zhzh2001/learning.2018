#include<iostream>
#include<algorithm>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<queue>
#include<vector>
#include<fstream>
using namespace std;
ifstream fin("magic.in");
ofstream fout("magic.out");
inline long long read(){
	long long f=0,x=0;char ch=getchar();
	while(ch<'0'||ch>'9')f|=ch=='-',ch=getchar();
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-48,ch=getchar();
	return f?-x:x;
}
inline void write(long long x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+48);
}
inline void writeln(long long x){
	write(x),puts("");
}
long long n,k;
struct wtf{int k,i;}a[100003];
int p[100003];
inline bool cmp(wtf a,wtf b){return a.k<b.k;}
inline void baoli(){
	for(register int i=1;i<=n;++i){
		fin>>a[i].k;
		a[i].i=i;
	}
	if(k==1){
		for(register int i=1;i<=n;++i){
			fout<<i<<" ";
		}
		return;
	}
	sort(a+1,a+n+1,cmp);
	for(register int i=1;i<=n;++i){
		for(register int j=n;j>i;--j){
			if(a[j-1].i-a[j].i>=k){
				swap(a[j],a[j-1]);
			}
		}
	}
	for(register int i=1;i<=n;++i){
		fout<<a[i].i<<" ";
	}
	fout<<endl;
}
long long num[100003],f[100003];
struct tree1{
	int sum;
}t[400003];
struct tree2{
	int mx,mn;
}tt[400003];
/*0:out,1:in*/
void build1(int rt,int l,int r){
	tt[rt].mx=0;
	if(l==r)return;
	int mid=l+r>>1;
	build1(rt<<1,l,mid),build1(rt<<1|1,mid+1,r);
}
void update1(int rt,int l,int r,int x,int val){
	if(l==r){
		tt[rt].mx=val;
		return;
	}
	int mid=l+r>>1;
	if(x<=mid)update1(rt<<1,l,mid,x,val);
	else update1(rt<<1|1,mid+1,r,x,val);
	tt[rt].mx=max(tt[rt<<1].mx,tt[rt<<1|1].mx);
}
int query1(int rt,int l,int r,int x,int y){
	if(l==x&&r==y){
		return tt[rt].mx;
	}
	int mid=l+r>>1;
	if(y<=mid)return query1(rt<<1,l,mid,x,y);
	else if(x>mid)return query1(rt<<1|1,mid+1,r,x,y);
	else return max(query1(rt<<1,l,mid,x,mid),query1(rt<<1|1,mid+1,r,mid+1,y));
}
void build2(int rt,int l,int r){
	tt[rt].mn=n+1;
	if(l==r)return;
	int mid=l+r>>1;
	build2(rt<<1,l,mid),build2(rt<<1|1,mid+1,r);
}
void update2(int rt,int l,int r,int x,int val){
	if(l==r){
		tt[rt].mn=val;
		return;
	}
	int mid=l+r>>1;
	if(x<=mid)update2(rt<<1,l,mid,x,val);
	else update2(rt<<1|1,mid+1,r,x,val);
	tt[rt].mn=min(tt[rt<<1].mn,tt[rt<<1|1].mn);
}
int query2(int rt,int l,int r,int x,int y){
	if(l==x&&r==y){
		return tt[rt].mn;
	}
	int mid=l+r>>1;
	if(y<=mid)return query2(rt<<1,l,mid,x,y);
	else if(x>mid)return query2(rt<<1|1,mid+1,r,x,y);
	else return min(query2(rt<<1,l,mid,x,mid),query2(rt<<1|1,mid+1,r,mid+1,y));
}
int lpos[100003],rpos[100003];
int cnt;
int head[100003],nxt[200003],to[200003],tot;
inline void add(int x,int y){
	to[++tot]=y,nxt[tot]=head[x],head[x]=tot;
}
void build(int rt,int l,int r){
	if(l==r){
		t[rt].sum=0; 
		return;
	}
	int mid=l+r>>1;
	
}
void add(int rt,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
	}
	int mid=l+r>>1;
}
int main(){
	//freopen("magic.in","r",stdin);
	//freopen("magic.out","w",stdout);
	fin>>n>>k;
	if(n<=6000||k==1){
		baoli();
		return 0;
	}
	for(register int i=1;i<=n;++i){
		num[i]=read();
		f[num[i]]=i;
	}
	build1(1,1,n);
	for(register int i=1;i<=n;++i){
		lpos[i]=query1(1,1,n,max(1ll,f[i]-k),min((long long)n,f[i]+k));
		update1(1,1,n,f[i],i);
	}
	build2(1,1,n);
	for(register int i=n;i>=1;--i){
		rpos[i]=query2(1,1,n,max(1ll,f[i]-k),min((long long)n,f[i]+k));
		update2(1,1,n,f[i],i);
	}
	cnt=n;
	build(1,1,n);
	for(register int i=1;i<=n;++i){
		if(lpos[i]>=1){
			add(1,1,n,1,lpos[i],1);
		}
		if(rpos[i]<=n){
			add(1,1,n,rpos[i],n,1);
		}
	}
	return 0;
}
/*
你能交换aj和ai当且仅当abs(j-i)>=k并且abs（aj-ai）==1
求进行交换后能得到的最小字典序的序列
变换后，相邻的元素差值绝对值大于k即可交换
 
in:
4 2
4 2 3 1

out:
8 3
4 5 7 8 3 1 2 6
1  2  3  4  5  6  7  8
6  7  5  1  2  8  3  4

1  6  7  5  2  8  3  4
1  2  6  7  5  8  3  4
1  2  6  7  5  3  8  4
1  2  6  7  5  3  4  8
1  2  6  7  5  8  3  4
*/
