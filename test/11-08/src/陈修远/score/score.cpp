#include <bits/stdc++.h>

using namespace std;

#define ll long long

int facinv[3005];
int fac[3005];

const int mo=1e9+7.5;
/*
int qp(int a,ll b)
{
	if(b==0) return 1;
	int t=qp(a,b/2);
	return b&1?1LL*t*t%mo*a%mo:1LL*t*t%mo;
}

void get()
{
	fac[1]=1;
	for(int i=2;i<=3000;++i) fac[i]=(1LL*fac[i-1]*i)%mo;
	facinv[3000]=qp(fac[3000],mo-2);
	for(int i=2999;i>=1;--i) facinv[i]=1LL*(i+1)*facinv[i+1]%mo;
	//for(int i=1;i<=30;++i) cout<<fac[i]<<" "<<facinv[i]<<endl;
}

int C(int m,int n)
{
	if(n==0) return 1;
	//cout<<fac[m]<<" "<<facinv[n]<<" "<<facinv[m-n]<<endl;
	return 1LL*fac[m]*facinv[n]%mo*facinv[m-n]%mo;
}
*/

int c[300007][105];

void get(int t,int k)
{
	for(int i=1;i<=k;++i) c[i][1]=1;
	for(int i=2;i<=t;++i)
	{
		for(int j=i;j<=i*k+i;++j)
		{
			for(int l=1;l<=k;++l)
			{
				c[j][i]=c[j][i]+c[j-l][i-1];
				if(c[j][i]>mo) c[j][i]-=mo;
			}
		}
		for(int j=i*k+i+1;j<=i*k*2+i;++j)
			c[j][i]=c[i*2+2*k*i-j][i];
	}
}

int C(int m,int n)
{
	return c[m+1][n+1];
}

int main()
{
	ios::sync_with_stdio(0);
	freopen("score.in","r",stdin);freopen("score.out","w",stdout);
	int a,b,k,t;
	cin>>a>>b>>k>>t;
	get(t,2*k+1);
	ll ans=0;
	if(a>2*k+b)
	{
		//cout<<1<<endl;
		for(int i=0;i<=2*k*t;++i)
		{
			for(int j=0;j<i;++j)
			{
				ans=(ans+2LL*C(t+j-1,t-1)*C(t+i-1,t-1))%mo;
			}
			ans=(ans+1LL*C(t+i-1,t-1)*C(t+i-1,t-1))%mo;
		}
		cout<<ans<<endl;
	}
	else if(a==b)
	{
		//cout<<2<<endl;
		for(int i=1;i<=2*k*t;++i)
		{
			for(int j=0;j<i;++j)
			{
				ans=(ans+1LL*C(t+j-1,t-1)*C(t+i-1,t-1))%mo;
			}
		}
		cout<<ans<<endl;
	}
	else if(b>=2*k*t+a){cout<<0<<endl;}
	else if(a>b)
	{
		//cout<<3<<endl;
		for(int i=a+t;i<=b+t+2*k*t;++i)
		{
			for(int j=b+t;j<i;++j)
			{
				ans=(ans+1LL*C(i-a-1,t-1)*C(j-b-1,t-1))%mo;
			}
		}
		for(int i=b+2*k*t+1;i<=a+2*k*t+t;++i)
		{
			for(int j=b+t;j<=b+2*k*t;++j)
			{
				ans=(ans+1LL*C(i-a-1,t-1)*C(j-b-1,t-1))%mo;
			}
		}
		cout<<ans<<endl;
	}
	else
	{
		//cout<<4<<endl;
		for(int i=b+t+1;i<=a+t+2*k*t;++i)
		{
			for(int j=b+t;j<i;++j)
			{
				//cout<<C(i,t-1)<<" "<<C(j,t-1)<<endl;
				ans=(ans+1LL*C(i-a-1,t-1)*C(j-b-1,t-1))%mo;
			}
		}
		cout<<ans<<endl;
	}
	return 0;
}
