#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define N 100007

const int mo=1e9+7.5;

ll ans;
int n;
int v[N];

int ea[N],eb[N];

vector<int> e[N];
map<int,map<int,bool> > d;

int st;
ll mmid[N],mst[N];
bool vis[N];
ll mxa[N];
int l[N],r[N],TO[N];

ll dfs(int x)
{
	int s=e[x].size();
	ll mx1=0,mx2=0;
	int i1=0,i2=0;
	bool flag=1;
	for(int i=0;i<s;++i)
	{
		if(d[x][e[x][i]])
		{
			flag=0;
			d[e[x][i]][x]=0;
			ll t=dfs(e[x][i]);
			d[e[x][i]][x]=1;
			if(t>mx1){mx2=mx1;i2=i1;mx1=t;i1=e[x][i];}
			else if(t>mx2){mx2=t;i2=e[x][i];}
		}
	}
	mmid[x]=mx1+mx2+v[x];
	mst[x]=mx1+v[x];
	mxa[x]=mmid[x];
	for(int i=0;i<s;++i)
	{
		if(d[x][e[x][i]])
		{
			if(mxa[e[x][i]]>mxa[x]) mxa[x]=mxa[e[x][i]],TO[x]=e[x][i];
		}
	}
	if(!TO[x]) l[x]=i1,r[x]=i2;
	if(x==1)cout<<i1<<i2<<endl;
	return mst[x];
}
/*
ll dfs1()
{
	for(int i=1;i<=n;++i)
	{
		if(e[i].size()==1)
		{
			st=i;
			return dfs(i);
		}
	}
}
*/

void gt(int ps)
{
	if(vis[ps]) return;
	if(ps<1) return;
	if(!TO[ps])
	{
		vis[ps]=1;
		//cout<<ps<<" "<<l[ps]<<" "<<r[ps]<<" "<<endl;
		gt(l[ps]);
		gt(r[ps]);
	}
	else
	{
		gt(TO[ps]);
	}
}

int mmm=0;

ll dfs2(int x)
{
	int mxx=0;
	int s=e[x].size();
	for(int i=0;i<s;++i)
	{
		if(d[x][e[x][i]] && !vis[e[x][i]])
		{
			d[e[x][i]][x]=0;
			int tem=dfs2(e[x][i]);
			if(tem>mxx) mxx=tem;
			d[e[x][i]][x]=1;
		}
	}
	return mxx+v[x];
}

int main()
{
	ios::sync_with_stdio(0);
	freopen("zhenxiang.in","r",stdin);freopen("zhenxiang.out","w",stdout);
	cin>>n;
	for(int i=1;i<=n;++i) cin>>v[i];
	for(int i=1;i<=n;++i)
	{
		int a,b;
		cin>>a>>b;
		ea[i]=a;
		eb[i]=b;
		d[a][b]=d[b][a]=1;
		e[a].push_back(b);
		e[b].push_back(a);
	}
	dfs(1);
	ans=mxa[1];
	gt(1);
	for(int i=1;i<=n;++i)
	{
		//cout<<vis[i]<<endl;
		if(!vis[i])
		{
			int tttt=dfs2(i);
			if(tttt>mmm) mmm=tttt;
		}
	}
	cout<<ans+mmm;
	return 0;
}
/*
9
1 2 3 4 5 6 7 8 9
1 1
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9

*/


