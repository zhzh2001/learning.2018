#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define N 100007

const int mo=1e9+7.5;

int a[N],pos[N],dis[N];
int n,k;

int main()
{
	ios::sync_with_stdio(0);
	freopen("magic.in","r",stdin);freopen("magic.out","w",stdout);
	cin>>n>>k;
	for(int i=1;i<=n;++i){cin>>a[i];pos[a[i]]=i;}
	for(int i=1;i<n;++i) dis[i]=pos[i+1]-pos[i];
	if(k==1)
	{
		for(int i=1;i<=n;++i) cout<<i<<" ";
		cout<<endl;
		return 0;
	}
	if(k==n-1)
	{
		if(a[1]-1==a[n]) swap(a[1],a[n]);
		for(int i=1;i<=n;++i) cout<<a[i]<<" ";
		cout<<endl;
		return 0;
	}
	bool flag=1;
	while(flag)
	{
		flag=0;
		for(int i=1;i<n;++i)
		{
			if(dis[i]<=-k)
			{
				flag=1,swap(a[pos[i]],a[pos[i+1]]),swap(pos[i],pos[i+1]),dis[i]=-dis[i],dis[i-1]-=dis[i],dis[i+1]-=dis[i];
				//for(int i=1;i<=n;++i) cout<<dis[i]<<" ";
				//cout<<endl;
			}
		}
	}
	for(int i=1;i<=n;++i) cout<<a[i]<<" ";
	cout<<endl;
	return 0;
}
/*
8 3
4 5 7 8 3 1 2 6

pos[]=6  7  5  1  2  8  3  4
dis[]=1 -2 -4  1  6 -5  1  0

3 5 7 8 4 1 2 6
2 5 7 8 4 1 3 6
1 5 7 8 4 2 3 6
1 4 7 8 5 2 3 6
1 3 7 8 5 2 4 6
1 3 7 8 5 2 4 6
*/


