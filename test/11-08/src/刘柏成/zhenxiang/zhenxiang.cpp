#include <bits/stdc++.h>
using namespace std;
#define file "zhenxiang"
typedef long long ll;
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
inline void tense(ll &x,ll y){
	if (x<y)
		x=y;
}
struct graph{
	int n,m;
	int first[100002];
	struct edge{
		int to,next;
	}e[200002];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	ll dp[100002][2][3];
	ll a[100002];
	ll ans;
	void dfs(int u,int fa){
		dp[u][0][1]=dp[u][0][2]=a[u];
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa)
				continue;
			dfs(v,u);
			tense(ans,dp[v][1][0]+dp[u][1][0]);
			tense(dp[u][1][2],dp[u][0][2]+dp[v][1][0]);
			tense(dp[u][1][2],dp[u][1][1]+dp[v][0][1]);
			tense(dp[u][1][2],dp[u][0][1]+dp[v][1][1]); 
			tense(dp[u][1][1],dp[u][0][1]+dp[v][1][0]);
			tense(dp[u][1][1],dp[u][1][0]+dp[v][0][1]+a[u]);
			tense(dp[u][1][1],dp[v][1][1]+a[u]);
			tense(dp[u][1][0],dp[v][1][0]);
			tense(dp[u][0][2],dp[v][0][1]+dp[u][0][1]);
			tense(dp[u][0][1],dp[v][0][1]+a[u]);
		}
		tense(dp[u][0][2],dp[u][0][1]);
		tense(dp[u][1][0],dp[u][0][2]);
		tense(dp[u][1][1],dp[u][1][0]);
		tense(dp[u][1][2],dp[u][1][1]);
	}
	ll work(){
		ans=0;
		dfs(1,0);
		for(int i=1;i<=n;i++)
			tense(ans,dp[i][1][2]);
		return ans;
	}
}g;
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=g.n=read();
	for(int i=1;i<=n;i++)
		g.a[i]=read();
	for(int i=1;i<n;i++){
		int u=read(),v=read();
		g.addedge(u,v);
		g.addedge(v,u);
	}
	printf("%lld",g.work());
}
