#include <bits/stdc++.h>
using namespace std;
#define file "score"
typedef long long ll;
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
const int off=210001;
const int mod=1000000007;
ll ff[420004],gg[420004],*f=ff,*g=gg;
ll tf[420004];
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int a=read(),b=read(),k=read()*2,t=read();
	f[a-b+off]=1;
	while(t--){
		for(int i=1;i<=2*off;i++){
			tf[i]=(tf[i-1]+f[i]*i)%mod;
			f[i]=(f[i]+f[i-1])%mod;
		}
		for(int i=1;i<=2*off;i++){
			//i-k..i k+1-i+p
			//i+1..i+k k+1-p+i
			int l=max(i-k,1);
			g[i]=((tf[i]-tf[l-1])+(f[i]-f[l-1])*(k+1-i))%mod;
			int r=min(i+k,2*off);
			g[i]+=((f[r]-f[i])*(k+1+i)-(tf[r]-tf[i]))%mod;
		}
		swap(f,g);
	}
	ll ans=0;
	for(int i=off+1;i<=2*off;i++)
		ans=(ans+f[i])%mod;
	printf("%lld",(ans%mod+mod)%mod);
}
