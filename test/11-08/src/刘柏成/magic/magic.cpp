#include <bits/stdc++.h>
using namespace std;
#define file "magic"
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
int n,k;
bool chk(int x,int y){
	return x-y>=k || y-x>=k; 
}
int a[100002],b[100002];
set<int> s;
vector<int> v,v2;
void solve(int l,int r){
	if (l>=r)
		return;
	int mn=l;
	for(int i=l;i<=r;i++)
		if (a[i]<a[mn])
			mn=i;
	for(int i=mn;i>l;i--)
		if (chk(a[i],a[i-1])){
			swap(a[i],a[i-1]);
			mn--;
		}else break;
	s.insert(a[mn]);
	for(int i=mn-1;i>=l;i--){
		set<int>::iterator it=s.lower_bound(a[i]-k+1);
		if (it!=s.end() && (*it<=a[i]+k-1))
			s.insert(a[i]),v2.push_back(a[i]);
		else v.push_back(a[i]);
	}
	int cur=l,t=a[mn];
	for(int i=(int)v2.size()-1;i>=0;i--)
		a[cur++]=v2[i];
	int w=cur;
	a[cur++]=t;
	for(int i=(int)v.size()-1;i>=0;i--)
		a[cur++]=v[i];
	s.clear();
	v.clear();
	v2.clear();
	solve(l,w-1);
	solve(w+1,r);
}
int ans[100002];
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=read(),k=read();
	if (k==1){
		for(int i=1;i<=n;i++)
			printf("%d ",i);
		return 0;
	}
	v.reserve(n);
	v2.reserve(n);
	for(int i=1;i<=n;i++)
		a[read()]=i;
	solve(1,n);
	for(int i=1;i<=n;i++)
		ans[a[i]]=i;
	for(int i=1;i<=n;i++)
		printf("%d ",ans[i]);
}
