#include<bits/stdc++.h>
using namespace std;
const int N=100002;
int n,m,i,j,a[N],pos[N],k;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int rd(){
	int x=0,fl=1;char ch=gc();
	for (;ch<48||ch>57;ch=gc())if(ch=='-')fl=-1;
	for (;48<=ch&&ch<=57;ch=gc())x=(x<<3)+(x<<1)+(ch^48);
	return x*fl;
}
inline void wri(int a){if(a<0)a=-a,putchar('-');if(a>=10)wri(a/10);putchar(a%10|48);}
inline void wln(int a){wri(a);puts("");}
int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n=rd(),k=rd();
	if (k==1){
		for (i=1;i<n;i++) wri(i),putchar(' ');
		wln(n);
		return 0;
	}
	for (i=1;i<=n;i++) a[i]=rd(),pos[a[i]]=i;
	for (i=1;i<=n;i++){
		for (;;){
			for (j=a[i];j>1;j--)
				if (pos[j-1]>i && pos[j-1]-pos[j]>=k) break;
			if (j>1){
				for (;j<=a[i] && pos[j-1]>i && pos[j-1]-pos[j]>=k;j++) swap(a[pos[j]],a[pos[j-1]]),swap(pos[j],pos[j-1]);
			}else break;
		}
	}
	for (i=1;i<n;i++) wri(a[i]),putchar(' ');
	wln(a[n]);
}
