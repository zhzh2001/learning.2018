#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=3002;
struct node{
	int to,ne;
}e[N<<1];
int fa[N][14],dep[N],h[N],i,x,y,tot,vis[N],T,lca,n,a[N],j;
bool lv[N];
ll ans,sum,tmp;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int rd(){
	int x=0,fl=1;char ch=gc();
	for (;ch<48||ch>57;ch=gc())if(ch=='-')fl=-1;
	for (;48<=ch&&ch<=57;ch=gc())x=(x<<3)+(x<<1)+(ch^48);
	return x*fl;
}
void add(int x,int y){
	e[++tot]=(node){y,h[x]};
	h[x]=tot;
}
void dfs1(int u,int f){
	fa[u][0]=f;dep[u]=dep[f]+1;
	for (int i=1;i<14;i++) fa[u][i]=fa[fa[u][i-1]][i-1];
	bool fl=0;
	for (int i=h[u],v;i;i=e[i].ne)
		if ((v=e[i].to)!=f) fl=1,dfs1(v,u);
	if (!fl) lv[u]=1;
}
int LCA(int x,int y){
	if (dep[x]<dep[y]) swap(x,y);
	for (int i=13;~i;i--)
		if (dep[fa[x][i]]>=dep[y]) x=fa[x][i];
	if (x==y) return x;
	for (int i=13;~i;i--)
		if (fa[x][i]!=fa[y][i]) x=fa[x][i],y=fa[y][i];
	return fa[x][0];
}
ll dfs2(int u,int fa){
	ll mx=0,sec=0;
	for (int i=h[u],v;i;i=e[i].ne)
		if ((v=e[i].to)!=fa){
			ll t=dfs2(v,u);
			if (t>mx) sec=mx,mx=t;
			else if (t>sec) sec=t;
		}
	if (vis[u]==T) return mx;
	tmp=max(tmp,mx+sec+a[u]);
	return mx+a[u];
}
int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=rd();
	for (i=1;i<=n;i++) a[i]=rd();
	for (i=1;i<n;i++) x=rd(),y=rd(),add(x,y),add(y,x);
	dfs1(1,0);
	for (i=1;i<=n;i++) if (lv[i])
		for (j=i;j<=n;j++) if (lv[j]){
			x=i,y=j;
			lca=LCA(x,y);T++;sum=0;
			for (;x!=lca;x=fa[x][0]) vis[x]=T,sum+=a[x];
			for (;y!=lca;y=fa[y][0]) vis[y]=T,sum+=a[y];
			vis[lca]=T;sum+=a[lca];
			tmp=0;dfs2(1,0);
			sum+=tmp;
			ans=max(ans,sum);
		}
	printf("%lld",ans);
}
