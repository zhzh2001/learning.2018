#include<bits/stdc++.h>
using namespace std;
const int M=1e9+7,N=210001;
int a,b,k,n,i,j,t,mn,mx,ans,f[2][N*2],g[2][N*2],s[2][N*2],x,y;
inline int add(int x,int y){x+=y;if(x>=M)x-=M;return x;}
int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	scanf("%d%d%d%d",&a,&b,&k,&n);
	f[0][a-b+N]=1;k<<=1;y=1;
	mn=a-b-k;mx=a-b+k;
	for (i=0;i<n;i++,mn-=k,mx+=k,x^=1,y^=1){
		for (j=mx-k;j<N;j++) g[x][j+N]=add(g[x][j-1+N],f[x][j+N]),s[x][j+N]=add(s[x][j-1+N],g[x][j+N]);
		for (j=mn;j<=mx;j++){
			f[y][j+N]=(1ll*(k+1)*(g[x][j+k+N]-g[x][j-k-1+N])%M-
			(1ll*k*(g[x][j+k+N]-g[x][j-k-1+N])+2*s[x][j-1+N]-s[x][j+k-1+N]-s[x][j-k-1+N])%M+M)%M;
			g[y][j+N]=add(g[y][j-1+N],f[y][j+N]);
			s[y][j+N]=add(s[y][j-1+N],g[y][j+N]);
		}
	}
	for (i=1;i<=mx;i++) ans=add(ans,f[x][i+N]);
	printf("%d",(ans+M)%M);
}
