#include<bits/stdc++.h>
using namespace std;
const int M=1e9+7,N=210001;
int a,b,k,n,i,j,t,mn,mx,ans,f[100][N*2];
int main(){
	scanf("%d%d%d%d",&a,&b,&k,&n);
	f[0][a-b+N]=1;
	mn=a-b-2*k;mx=a-b+2*k;
	for (i=0;i<n;i++,mn-=2*k,mx+=2*k)
		for (j=mn;j<=mx;j++)
			for (t=-2*k;t<=2*k;t++) f[i+1][j+N]+=f[i][j-t+N]*(2*k+1-abs(t));
	for (i=1;i<=mx;i++) ans+=f[n][i+N];
	printf("%d",ans);
}
