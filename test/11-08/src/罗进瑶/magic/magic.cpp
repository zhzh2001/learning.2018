#include<bits/stdc++.h>
void setIO() {
	freopen("magic.in", "r", stdin);
	freopen("magic.out", "w", stdout);
}
typedef long long ll;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int N = 210000;
int n, m, a[N];

int main() {
	setIO();
	n = read(), m = read();
	if(m == 1) {
		for(int i = 1; i <= n; ++i) printf("%d ", i);
		puts("");
		return 0;
	}
	for(int i = 1; i <= n; ++i) a[i] = read();
	puts("0");
	return 0;
}
