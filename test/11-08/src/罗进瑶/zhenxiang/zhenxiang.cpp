#include<bits/stdc++.h>
void setIO() {
	freopen("zhenxiang.in", "r", stdin);
	freopen("zhenxiang.out", "w", stdout);
}
typedef long long ll;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int N = 11510;
int ver[N * 2], nxt[N * 2], head[N], en;
int n;
ll a[N],ans;
void add(int x, int y) {
	ver[++en] = y, nxt[en] = head[x], head[x] = en;
}
int fa[N][21], d[N];
ll mx1[N], mx2[N], f[N], res;
void dfs1(int x, int F) {
	fa[x][0] = F;
	for(int i = 1; i <= 20; ++i) fa[x][i] = fa[fa[x][i - 1]][i - 1];
	d[x] = d[F] + 1;
	for(int i = head[x]; i;i = nxt[i]) {
		int y = ver[i];
		if(y == F) continue;
		dfs1(y, x);
		if(mx1[y] > mx1[x]) mx2[x] = max(mx1[x], mx2[y]), mx1[x] = mx1[y];
		else if(mx1[y] > mx2[x]) mx2[x] = mx1[y];
		f[x] = max(f[x], f[y]);
	}
	mx1[x] += a[x];
	mx2[x] += a[x];
	f[x] = max(f[x], mx1[x] + mx2[x] - a[x]);
} 
void ask(int x, int F) {
	mx1[x] = mx2[x] = f[x] = 0;
	for(int i = head[x]; i;i = nxt[i]) {
		int y = ver[i];
		if(y == F) continue;
		ask(y, x);
		if(mx1[y] > mx1[x]) mx2[x] = max(mx1[x], mx2[y]), mx1[x] = mx1[y];
		else if(mx1[y] > mx2[x]) mx2[x] = mx1[y];
		f[x] = max(f[x], f[y]);
	}
	mx1[x] += a[x];
	mx2[x] += a[x];
	f[x] = max(f[x], mx1[x] + mx2[x] - a[x]);
}
inline int LCA(int x, int y) {
	if(d[x] > d[y]) swap(x, y);
	for(int i = 20; ~i; --i)
		if(d[fa[y][i]] >= d[x]) y = fa[y][i];
	if(x == y) return x;
	for(int i = 20;~i;--i)
		if(fa[x][i] != fa[y][i]) x = fa[x][i], y = fa[y][i];
	return fa[x][0];
}
int s, t, tmp;
bool vis[N];
ll sum;
void solve(int x, int y) {
	while(1) {
		if(d[x] < d[y]) swap(x, y);
		vis[x] = 1;
		sum += a[x];
		for(int i = head[x]; i;i = nxt[i]) {
			int to = ver[i];
			if(to == fa[x][0] || vis[to]) continue;
		//	ask(to, x);
			res = max(res, f[to]);
		}
		if(x == tmp) break;
		x = fa[x][0];
	}
/*	x = fa[x][0];
	do {
		vis[x] = 1;
		for(int i = head[x]; i;i = nxt[i]) {
			int to = ver[i];
			if(y == fa[x][0] || vis[y]) continue;
			ask(y, x);
			res = max(res, f[y]);
		}
	}while(tmp != 1);*/
}
int main() {
	setIO();
	n = read();
	if(n > 300) {
		puts("0");
		return 0;
	}
	for(int i = 1; i <= n; ++i) a[i] = read();
	for(int i = 1; i < n; ++i) {
		int x = read(), y = read();
		add(x,y);add(y,x);
	} 
	dfs1(1, 0);
	for(s = 1; s <= n; ++s)
		for(t = s; t <= n; ++t) {
			tmp = LCA(s, t);
			memset(vis, 0, sizeof vis);
			res = sum = 0;
			solve(s, t);
			ans = max(ans, res + sum);
		}
	writeln(ans);
	return 0;
}
