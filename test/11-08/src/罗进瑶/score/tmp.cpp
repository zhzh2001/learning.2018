#include<bits/stdc++.h>
void setIO() {
	freopen("zhenxiang.in", "r", stdin);
	freopen("zhenxiang.out", "w", stdout);
}
typedef long long ll;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
int a, b, k, n;   
ll ans;
ll sum[520000];
const int p =1000000007;
ll get(int x, int y) {
	return sum[max(x, 0)] - sum[max(0,y - 1)];
}
namespace GG {
	const int M = 250;
	ll g[320000];
	int f[210][510];
	inline bool pd(int x) {
		return x >= 0 && x <= M * 2;
	}
	void main() {
		f[0][a - b + M] = 1;
		for(int i = -k; i <= k; ++i)
			for(int j = -k; j <= k; ++j)
				++g[(i - j) + M];
		for(int i = 1; i <= n; ++i)
			for(int x = 0; x <= M * 2; ++x)
					for(int t1 = -k * 2 ; t1 <= k * 2; ++t1)
							if(pd(x - t1))(f[i][x]+= (ll)f[i - 1][x - t1] * g[t1 + M]) %= p;
		for(int i = M * 2; i > M; --i) (ans += f[n][i]) %= p;
		writeln(ans);
	}
}
int main() {
//	setIO();
	a = read(), b = read(), k = read(), n = read();
	if(a <= 10 && b <= 10 && k <= 10 && n <= 10) {
		GG::main();
		return 0;
	}
	const int M = 250000;
	ll f[210][520000];
	memset(f, 0, sizeof f);
	f[0][a - b + M] = 1;
	for(int i = 1; i <= M * 2; ++i)
		sum[i] = sum[i - 1] + f[0][i];
	for(int i = 1; i <= n; ++i) {
		ll res = 0;
		for(int j = 50; j <= 2 * M - 2 * k; ++j) {
			res = res - get(j - 1, j - 2 * k - 1) + get(j + 2 * k, j);
			res %= p;
			f[i][j] = res;
		}
		sum[0] = 0; 
		for(int j = 1; j <= M * 2; ++j)
			sum[j] = (sum[j - 1] + f[i][j]) % p;
	}
	for(int i = M * 2; i > M; --i) (ans += f[n][i]) %= p;
	writeln(ans);
	return 0;
}
