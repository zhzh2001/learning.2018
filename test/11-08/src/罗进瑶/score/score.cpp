#include<bits/stdc++.h>
void setIO() {
	freopen("zhenxiang.in", "r", stdin);
	freopen("zhenxiang.out", "w", stdout);
}
typedef long long ll;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int M = 250;
int a, b, k, n; 
int f[210][510], ans, g[510];
const int p =1000000007;
inline bool pd(int x) {
	return x >= 0 && x <= M * 2;
}
int main() {
	setIO();
	a = read(), b = read(), k = read(), n = read();
	f[0][a - b + M] = 1;
	for(int i = -k; i <= k; ++i)
		for(int j = -k; j <= k; ++j)
			++g[(i - j) + M];
	for(int i = 1; i <= n; ++i)
		for(int x = 0; x <= M * 2; ++x)
				for(int t1 = -k * 2 ; t1 <= k * 2; ++t1)
						if(pd(x - t1))(f[i][x]+= (ll)f[i - 1][x - t1] * g[t1 + M]) %= p;
	for(int i = M * 2; i > M; --i) (ans += f[n][i]) %= p;
	writeln(ans);
	return 0;
}
