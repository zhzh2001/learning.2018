#include<cstdio>
#include<cctype>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 1000000007
#define N 100010
using namespace std;
LL n,m,i,j,k,l,r,t;
LL a,b,ans;
LL f[4*N],g[4*N],dp[N];
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int main()
{
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	a=read(); b=read(); k=read(); t=read(); f[2*t*k]=1;
	rep(i,0,2*k) dp[2*k+i]=2*k+1-i,dp[2*k-i]=2*k+1-i;
	rep(i,1,t)
	{
		rep(j,t*k,3*t*k) 
		  rep(h,0,4*k) (g[j+(h-2*k)]+=f[j]*dp[h])%=fy;
		rep(j,0,4*t*k){f[j]=g[j]; g[j]=0;}
	} 
	i=2*t*k-a+b+1;
	while(f[i]) (ans+=f[i++])%=fy;
	printf("%lld",ans);
	return 0;
}
