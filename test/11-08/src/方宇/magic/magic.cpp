#include<cstdio>
#include<cctype>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 19750502019771008
#define N 500010
using namespace std;
set<int> s;
int n,k,p[N],a[N],Deg[N],tr[N<<3],Head[N],To[N<<1],Next[N<<1],cnt;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
inline void build(int v,int l,int r)
{
	tr[v]=n+1;
	if (l==r) return;
	int mid=(l+r)>>1;
	build(v<<1,l,mid);build(v<<1|1,mid+1,r);
}
inline void upd_mul(int v,int l,int r,int x,int y)
{
		if(l==r) 
		{
			tr[v]=y;
			return;
		}
		int mid=(l+r)>>1;
		if (x<=mid) upd_mul(v<<1,l,mid,x,y);
			else upd_mul(v<<1|1,mid+1,r,x,y);
		tr[v]=(tr[v<<1]<tr[v<<1|1])?tr[v<<1]:tr[v<<1|1];
}
inline int query(int v,int l,int r,int x,int y)
{
	if (l==x&&r==y) return tr[v];
	int mid=(l+r)>>1;
	if (y<=mid) return query(v<<1,l,mid,x,y);
		else if (x>mid) return query(v<<1|1,mid+1,r,x,y);
		else
		{
			int h=query(v<<1,l,mid,x,mid),hh=query(v<<1|1,mid+1,r,mid+1,y);
			return (h<hh)?h:hh;
		}
}
inline void add(int u,int v)
{
	To[++cnt]=v;
	Next[cnt]=Head[u];
	Head[u]=cnt;
	Deg[v]++;
}
int main()
{
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n=read(),k=read();
	rep(i,1,n) p[i]=read(),a[p[i]]=i;
	build(1,1,n);
	dep(i,n,1)
	{
		int x;
		x=query(1,1,n,max(a[i]-k+1,1),a[i]);
		if (x<=n) add(a[i],a[x]);
		x=query(1,1,n,a[i],min(a[i]+k-1,n));
		if (x<=n) add(a[i],a[x]);
		upd_mul(1,1,n,a[i],i);
	}
	rep(i,1,n) if (!Deg[i]) s.insert(i);
	rep(i,1,n) 
	{
		int x=a[i]=*s.begin();
		s.erase(s.begin());
		for(int i=Head[x];i;i=Next[i])
			if (!(--Deg[To[i]])) s.insert(To[i]);
	}
	rep(i,1,n) p[a[i]]=i;
	rep(i,1,n) printf("%d ",p[i]);
	return 0;
}
