#include<map>
#include<cstdio>
#include<iostream>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int P=1e9+7;
int a,b,k,t;
map<int,map<int,int> >f[105];
void Add(int &x,int y){
	x=x+y>=P?x+y-P:x+y;
}
int pw(int a,int n){
	int ans=1;
	while(n){
		if(n&1)ans=1LL*ans*a%P;
		a=1LL*a*a%P;
		n>>=1;
	}
	return ans;
}		
int dfs(int n,int x,int y){
	if(x+k*(t-n+1)<=y-k*(t-n+1))return 0;
	if(x-k*(t-n+1)>y+k*(t-n+1))return pw(pw(2*k+1,t-n+1),2);
	if(n>t)return 1;
	if(f[n].count(x)&&f[n][x].count(y))return f[n][x][y];
	int ans=0;
	for(int i=-k;i<=k;i++)
		for(int j=-k;j<=k;j++)
			Add(ans,dfs(n+1,x+i,y+j));
	return f[n][x][y]=ans;
}
int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout); 
	a=read(),b=read(),k=read(),t=read();
	printf("%d",dfs(1,a,b));
}
