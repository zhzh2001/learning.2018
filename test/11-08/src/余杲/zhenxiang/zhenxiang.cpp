#include<cstdio>
#include<iostream>
using namespace std;
const int MAXN=1e5+5;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc 
int nedge,head[MAXN];
struct Edge{
	int to,nxt;
}edge[MAXN<<1];
void add(int x,int y){
	edge[++nedge].to=y;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
int a[MAXN];
long long f[MAXN][4];
//0 一条链 
//1 一个"人" 
//2 一条链加一个"人" 
//3 两个"人" 
void dfs1(int x,int fa){
	long long Max=0,Maxx=0;
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		if(y==fa)continue;
		dfs1(y,x);
		if(f[y][0]>Max){
			Maxx=Max;
			Max=f[y][0];
		}else if(f[y][0]>Maxx)Maxx=f[y][0];
	}
	f[x][0]=Max+a[x];
	f[x][1]=Max+Maxx+a[x];
}
void dfs2(int x,int fa,long long p,long long len){//到x这个点之前最大的路径长，最大的链长 
//	cout<<x<<' '<<Ren<<' '<<f[x][1]<<endl;
//	cout<<f[x][0]<<' '<<f[x][1]<<endl;
//	f[y][2]=Ren+len;
	f[x][3]=p+f[x][1];
	long long Max=len,Maxx=0,Maxxx=0;
	long long MMax=p,MMaxx=0;
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		if(y==fa)continue;
		if(f[y][0]>Max){
			Maxxx=Maxx;
			Maxx=Max;
			Max=f[y][0];
		}else if(f[y][0]>Maxx){
			Maxxx=Maxx;
			Maxx=f[y][0];
		}else if(f[y][0]>Maxxx)Maxxx=f[y][0];
		if(f[y][1]>MMax){
			MMaxx=MMax;
			MMax=f[y][1];
		}else if(f[y][1]>MMaxx)MMaxx=f[y][1];
	}
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		if(y==fa)continue;
//		向下延伸 
		long long len1=0,len2=0;
		if(Max==f[y][0])len1=Maxx,len2=Maxxx;
		else{
			len1=Max;
			if(Maxx==f[y][0])len2=Maxxx;
			else len2=Maxx;
		}//排除以下的情况 
		long long nxtp=0;
		if(MMax==f[y][1])nxtp=MMaxx;
		else nxtp=MMax;//原先就处理出的类似^的"人" 
		nxtp=max(nxtp,len1+len2+a[x]);//形状为<或>的"人" 
//		除向y延伸的最大"人" 
//		cout<<x<<' '<<y<<' '<<len1<<' '<<len2<<endl;
		dfs2(y,x,nxtp,len1+a[x]);
		f[x][3]=max(f[x][3],f[y][3]);
	}
//		f[x][2]=max(f[x][2],max(f[x][1]+f[y][0],f[x][0]+f[y][1]))
//		更新前提：f[x][1]不由f[y][0]转移，f[y][1]其中的链不与f[x][0]相交 
//		f[x][3]=max(f[x][3],max(f[y][3],f[x][1]+f[y][1]));
}
int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		add(x,y),add(y,x);
	}
	dfs1(1,0);
	dfs2(1,0,0,0);
	printf("%lld",f[1][3]);
}
/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
7
1 2 3 4 5 6 7
1 2
2 3
3 5
5 7
2 4
4 6
*/
