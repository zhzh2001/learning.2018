#include<bits/stdc++.h>
using namespace std;
const int N = 100005;
int n, a[N], head[N], cnt;
struct edge {int nxt, to;}e[N*2];
inline void insert(int u, int v) {
  e[++cnt] = (edge) {head[u], v}; head[u] = cnt;
  e[++cnt] = (edge) {head[v], u}; head[v] = cnt;
}
long long f[N], g[N], gg[N];
inline void dfs(int x, int fa) {
  f[x] = a[x];
  long long mx1, mx2, mx3;
  mx1 = mx2 = mx3 = 0;
  for (int y, i = head[x]; i; i = e[i].nxt)
    if ((y = e[i].to) != fa){
      dfs(y, x);
      g[x] = max(g[x], g[e[i].to]);
      f[x] = max(f[x], f[e[i].to]+a[x]);
      if (mx2 < f[e[i].to]) {
        mx2 = f[e[i].to];
        if (mx2 > mx1) swap(mx2, mx1);
      }
    }
  g[x] = max(g[x], mx2+mx1+a[x]);
  if (mx1 != mx2) {
    for (int y, i = head[x]; i; i = e[i].nxt)
      if ((y = e[i].to) != fa && f[x] != f[e[i].to]+a[x]) {
        gg[x] = max(gg[x], g[e[i].to]);
      }
  }else {
    for (int y, i = head[x]; i; i = e[i].nxt)
      if ((y = e[i].to) != fa) gg[x] = max(gg[x], g[e[i].to]);
  }
}
struct dota {long long val; int x;};
long long ans;
inline void dp(int x, int fa) {
  dota mx1, mx2, mx3;
  mx1 = mx2 = mx3 = (dota){0, 0};
  for (int y, i = head[x]; i; i = e[i].nxt)
    if ((y = e[i].to) != fa) {
      dp(y, x);
      if (mx3.val < f[e[i].to]) {
        mx3 = (dota){f[e[i].to], e[i].to};
        if (mx2.val < mx3.val) {
          swap(mx2, mx3);
          if (mx1.val < mx2.val) swap(mx1, mx2);
        }
      }
    }
  for (int y, i = head[x]; i; i = e[i].nxt) 
    if ((y = e[i].to) != fa) {
      if (mx1.x == e[i].to) ans = max(ans, g[e[i].to]+mx2.val+mx3.val+a[x]);
      else if (mx2.x == e[i].to) ans = max(ans, g[e[i].to]+mx1.val+mx3.val+a[x]);
      else ans = max(ans, g[e[i].to]+mx1.val+mx2.val+a[x]);
    }
  ans = max(ans, mx1.val+mx2.val+a[x]+gg[mx1.x]);
  ans = max(ans, mx1.val+mx2.val+a[x]+gg[mx2.x]);
  ans = max(ans, mx1.val+mx3.val+a[x]+gg[mx1.x]);
  ans = max(ans, mx1.val+mx3.val+a[x]+gg[mx3.x]);
  ans = max(ans, mx2.val+mx3.val+a[x]+gg[mx2.x]);
  ans = max(ans, mx2.val+mx3.val+a[x]+gg[mx3.x]);
//  if (x == 1) cout << mx1.val <<" "<< mx2.val <<" "<< mx3.val<<endl;
//  cout << x <<" "<< ans << endl;
}
int main() {
  freopen("zhenxiang.in","r",stdin);
  freopen("zhenxiang.out","w",stdout);
  scanf("%d",&n);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
  }
  for (int i = 1; i < n; i++) {
    int x, y; scanf("%d%d", &x, &y);
    insert(x, y);
  }
  dfs(1, 0);
  dp(1, 0);
  printf("%lld\n", ans);
  return 0;
}
/*
.in
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
.out
25




9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
2 7
4 5
4 6
5 8
5 9

read 37
but 42
jiade*/
