#include<bits/stdc++.h>
using namespace std;
const int mod = 1e9+7;
int a, b, k, t, f[105][200005], mx;
inline void update(int &x, int y) {
  if (y >= mod) y -= mod;
  x += y;
  if (x >= mod) x -= mod;
}
int main() {
  freopen("score.in","r",stdin);
  freopen("score.out","w",stdout);
  scanf("%d%d%d%d", &a, &b, &k, &t);
  for (int i = 0; i <= k*2*t; i++) f[0][i] = 1;
  for (int i = 1; i <= t; i++) {
    for (int j = 0; j <= k*2; j++)
      update(f[i][j], f[i-1][j]);
    for (int j = k*2+1; j <= k*2*t; j++)
      update(f[i][j], f[i-1][j]-f[i-1][j-k*2-1]+mod); 
    for (int j = 1; j <= k*t*2; j++)
      update(f[i][j], f[i][j-1]);
  }
  int ans = 0;
  for (int i = 0; i <= k*t*2; i++)
    update(ans, 1ll*(f[t][i]-((!i)?0:f[t][i-1])+mod)%mod*f[t][i-(b-a)-1]%mod);
/*  for (int i = 0; i <= t; i++) {
    for (int j = 0; j <= k*2*t; j++) cout << f[i][j] <<" ";
    cout <<"\n";
  }*/
  printf("%d\n", ans);
  return 0;
}
/*
f[i][j] = f[i-1][j]
*/
