#include<cstdio>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=215,mod=1e9+7;
int ans,dp[15][N][N],a,b,K,t;
inline void init(){
	a=read(); b=read(); K=read(); t=read();
}
inline void update(int &x,int y){
	x=(x+y)%mod;
}
inline void solve(){
	dp[0][a][b]=1; K=K*2;
	for (int i=1;i<=t;i++){
		for (int j=a;j<=210;j++){
			for (int k=b;k<=210;k++){
				if (dp[i-1][j][k]){
					for (int j1=0;j1<=K;j1++){
						for (int k1=0;k1<=K;k1++){
							update(dp[i][j+j1][k+k1],dp[i-1][j][k]);
						}
					}
				}
			}
		}
	}
	for (int i=a;i<=210;i++){
		for (int j=b;j<i;j++){
			update(ans,dp[t][i][j]);
		}
	}
	writeln(ans);
}
int main(){
	freopen("score.in","r",stdin); freopen("score.out","w",stdout);
	init(); solve();
	return 0;
}
