#include<cstdio>
#include<algorithm>
#define int long long
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=1e5+5;
int ans,dp[N][7],n,tot,head[N],w[N];
struct edge{
	int link,next;
}e[N<<1];
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add_edge(u,v); add_edge(v,u);
}
inline void init(){
	n=read();
	for (int i=1;i<=n;i++) w[i]=read();
	for (int i=1;i<n;i++){
		insert(read(),read());
	}
}
struct node{
	int id,v;
}q1[N],q2[N],q3[N],q4[N];
inline bool cmp(node A,node B){
	return A.v>B.v;
}
inline void upd1(int u,int a,int b){
	dp[u][2]=max(dp[u][2],w[u]+q1[a].v+q2[b].v);
}
inline void upd2(int u,int a,int b,int c){
	dp[u][3]=max(dp[u][3],w[u]+q1[a].v+q1[b].v+q2[c].v);	
}
inline void upd3(int u,int a,int b){
	dp[u][4]=max(dp[u][4],w[u]+q4[a].v+q1[b].v);
}
void dfs(int u,int fa){
	tot=0;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u);
		}
	}
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dp[u][0]=max(dp[u][0],dp[v][0]); dp[u][4]=max(dp[v][4]+w[u],dp[u][4]); dp[u][5]=max(dp[v][5],dp[u][5]);
			q1[++tot]=(node){v,dp[v][0]}; q2[tot]=(node){v,dp[v][1]}; q3[tot]=(node){v,dp[v][4]};
			q4[tot]=(node){v,dp[v][5]};
		}
	}
	sort(q1+1,q1+1+tot,cmp); sort(q2+1,q2+1+tot,cmp); sort(q3+1,q3+1+tot,cmp);
	sort(q4+1,q4+1+tot,cmp);
	dp[u][0]+=w[u]; dp[u][1]=q1[1].v+q1[2].v+w[u]; 
	dp[u][2]=dp[u][3]=dp[u][4]=dp[u][0]; dp[u][5]=max(dp[u][5],dp[u][1]);
	for (int i=1;i<=3;i++){
		for (int j=1;j<=3;j++){
			if (q1[i].id==q2[j].id) continue;
			upd1(u,i,j);
		}
	}
	for (int i=1;i<=5;i++){
		for (int j=1;j<=5;j++){
			if (q1[i].id&&q1[j].id&&q1[i].id==q1[j].id) continue;
			for (int k=1;k<=5;k++){
				if (q1[i].id&&q2[k].id&&q1[i].id==q2[k].id) continue;
				if (q1[j].id&&q2[k].id&&q1[j].id==q2[k].id) continue;
				upd2(u,i,j,k);
			}
		}
	}
	for (int i=1;i<=3;i++){
		for (int j=1;j<=3;j++){
			if (q4[i].id&&q1[j].id&&q4[i].id==q1[j].id) continue;
			upd3(u,i,j);
		}
	}
	for (int i=1;i<=3;i++){
		for (int j=1;j<=3;j++){
			if (q4[i].id&&q4[j].id&&q4[i].id==q4[j].id) continue;
			ans=max(ans,q4[i].v+q4[j].v);
		}
	}
	dp[u][4]=max(dp[u][4],w[u]+q4[1].v);
	for (int i=1;i<=3;i++){
		for (int j=1;j<=3;j++){
			if (q1[i].id&&q3[j].id&&q1[i].id==q3[j].id) continue;
			ans=max(ans,q3[j].v+w[u]+q1[i].v);
		}
	}
	for (int i=0;i<6;i++) ans=max(ans,dp[u][i]);
	for (int i=1;i<=tot;i++) q1[i]=q2[i]=q3[i]=q4[i]=(node){0,0};
}
inline void solve(){
	dfs(1,0); writeln(ans);
}
signed main(){
	freopen("zhenxiang.in","r",stdin); freopen("zhenxiang.out","w",stdout);
	init(); solve();
	return 0;
}
