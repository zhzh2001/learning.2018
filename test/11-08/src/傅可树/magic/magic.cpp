#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
#include<vector>
#define ls a[k].son[0]
#define rs a[k].son[1]
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=1e5+5;
struct node{
	int sum,son[2];
}a[N*30];
struct edge{
	int link,next;
}e[N<<1];
int cnt,root[N],ans[N],t,q[N],head[N],tot,deg[N],K,MN,MX,n,num[N],rk[N];
inline void init(){
	n=read(); K=read();
	for (int i=1;i<=n;i++){
		num[i]=read();		
	}
}
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot; deg[v]++;
}
void update(int &k,int pre,int l,int r,int x){
	k=++cnt,a[k]=a[pre];
	a[k].sum++;
	if (l==r) return;
	int mid=(l+r)>>1;
	if (mid>=x) update(ls,a[pre].son[0],l,mid,x);
		else update(rs,a[pre].son[1],mid+1,r,x);
}
priority_queue<int, vector<int>, greater<int> > heap;
int query(int k,int pre,int l,int r,int x,int y){
	if (!k) return 0;
	if (l==x&&r==y) return a[k].sum-a[pre].sum;
	int mid=(l+r)>>1;
	if (mid>=y) return query(ls,a[pre].son[0],l,mid,x,y);	
		else if (mid<x) return query(rs,a[pre].son[1],mid+1,r,x,y);
			else return query(ls,a[pre].son[0],l,mid,x,mid)+query(rs,a[pre].son[1],mid+1,r,mid+1,y);
}
inline bool check(int l,int r,int x){
	int sum=query(root[r],root[l-1],1,n,max(rk[x]-K+1,0),min(rk[x]+K-1,n));
	return sum>0;
}
inline int qry2(int x){
	if (x>n) return 0;
	int l=x,r=n;
	while (l<r){
		int mid=(l+r)>>1;
		if (check(x,mid,x-1)) r=mid;
			else l=mid+1;
	}
	if (check(x,l,x-1)) return l;
		else return 0;	
}
inline int qry1(int x){
	if (!x) return 0;
	int l=1,r=x;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (check(mid,x,x+1)) l=mid;
			else r=mid-1;
	}
	if (check(l,x,x+1)) return l;
		else return 0;
}
inline void solve(){
	for (int i=1;i<=n;i++){
		rk[num[i]]=i;
	}
	for (int i=1;i<=n;i++) update(root[i],root[i-1],1,n,rk[i]);
	for (int i=1;i<=n;i++){
		int x=qry1(i-1),y=qry2(i+1);
		if (x) add_edge(rk[x],rk[i]);
		if (y) add_edge(rk[i],rk[y]);
	}
	for (int i=1;i<=n;i++){
		if (!deg[i]){
			heap.push(i);
		}
	}
	while (!heap.empty()){
		int u=heap.top(); heap.pop(); q[++t]=u;
		for (int i=head[u];i;i=e[i].next){
			int v=e[i].link;
			if (!--deg[v]){
				heap.push(v);
			}
		}
	}
	for (int i=1;i<=n;i++) ans[q[i]]=i;
	for (int i=1;i<=n;i++) write(ans[i]),putchar(' ');
}
int main(){
	freopen("magic.in","r",stdin); freopen("magic.out","w",stdout);
	init();
	solve();
	return 0;
}
