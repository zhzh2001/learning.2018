#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
#include <map>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
int ans,x,s,k,t,tot,sc[3000],a[3000],b[3000],ta[3000],tb[3000],visa[10000],visb[10000];
const int mod=1000000007;
int tmp[5]={0,23,17,19};
bool check(){
	int flag=1,suma=x,sumb=s;
	for(int i=1;i<=t;i++){
		if(a[i]!=b[i])flag=0;
		suma+=a[i]; sumb+=b[i];
	}
	if(flag) return 0;
	return suma>sumb;
}
void dfs2(int rt,int sum){
	if(sum>t){
//		cout<<"bbb";for(int i=1;i<=t;i++)cout<<b[i]<<" ";//puts("");
        int kkk=5000;
        for(int i=1;i<=t;i++)kkk+=tmp[i]*b[i];
        if(visb[kkk])return ; 
        visb[kkk]=1; 
		if(check()){
//			cout<<"aaa";for(int i=1;i<=t;i++)cout<<a[i]<<" ";puts("");
//			cout<<"bbb";for(int i=1;i<=t;i++)cout<<b[i]<<" ";puts("");
			if(t==1)ans=(ans+1)%mod;
			if(t==2){
				int xa=1,xb=1;
				if(a[1]!=a[2])xa++; if(b[1]!=b[2])xb++;
//				cout<<"kkk"<<xa*xb<<endl;
				ans=(ans+xa*xb)%mod;
			} 
			if(t==3){
				int xa,xb;
				for(int i=1;i<=t;i++)ta[i]=a[i],tb[i]=b[i];
				sort(a+1,a+1+t);sort(b+1,b+1+t);
				if(a[1]==a[2]==a[3])xa=1;
				else if((a[1]!=a[2])&&(a[1]!=a[3])&&(a[2]!=a[3]))xa=6;
				else xa=3;
				if(b[1]==b[2]==b[3])xb=1;
				else if((b[1]!=b[2])&&(b[1]!=b[3])&&(b[2]!=b[3]))xb=6;
				else xb=3;
				ans=(ans+xa*xb)%mod;
				for(int i=1;i<=t;i++)a[i]=ta[i],b[i]=tb[i];
			}
		}
		return ;
	}
	if(rt>tot) return ;
	dfs2(rt+1,sum);
    b[sum]=sc[rt];
    dfs2(rt+1,sum+1);
}
void dfs1(int rt,int sum){
	if(sum>t){
//		cout<<"aaa";for(int i=1;i<=t;i++)cout<<a[i]<<" ";//puts("");
//        if(t==2&&visa[a[1]][a[2]])continue;
//        if(t==2)visb[a[1]][a[2]]=1;
        int kkk=5000;
        for(int i=1;i<=t;i++)kkk+=tmp[i]*a[i];
        if(visa[kkk])return ; 
        visa[kkk]=1;
//        cout<<"kkk"<<endl;
        memset(visb,0,sizeof visb);
		dfs2(1,1);return ;
    }
	if(rt>tot) return ;
	dfs1(rt+1,sum);
	a[sum]=sc[rt];
    dfs1(rt+1,sum+1); 
}
int main()
{
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	x=read(),s=read(),k=read(),t=read();
	for(int i=-k;i<=k;i++)
	   for(int j=1;j<=t;j++) sc[++tot]=i;
//	for(int i=1;i<=tot;i++) cout<<sc[i]<<" ";
    dfs1(1,1);
    cout<<ans<<endl;
    return 0;
}
