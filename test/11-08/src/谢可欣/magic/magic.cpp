#include <iostream>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
void wr(int x){if(x<0){putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
void wri(int x){wr(x);putchar(' ');}
const int N=200005;
int a[N],pos[N],ans[N];
int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	int n=read(),k=read(),flag=1;
	for(int i=1;i<=n;i++) a[i]=read(),pos[a[i]]=i;
//	for(int i=1;i<=n;i++) cout<<pos[i]<<" ";puts("");
	if(k==1){
		for(int i=1;i<=n;i++)wri(i);
		return 0;
	}
	while(flag){
		int flag=0;
   	    for(int i=1;i<n;i++){
    		if(pos[i+1]<pos[i]&&(pos[i]-pos[i+1]>=k)){
    			swap(pos[i+1],pos[i]); flag=1;
			}
    	}
    	if(!flag)break;
    }
    for(int i=1;i<=n;i++)ans[pos[i]]=i;
    for(int i=1;i<=n;i++)wri(ans[i]);
    return 0;
}
