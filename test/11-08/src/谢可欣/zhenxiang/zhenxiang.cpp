#include <iostream>
#include <cstring>
#include <cstdio>
#define int long long
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
const int N=300005;
int n,pos,v[N],w[N],nxt[N],head[N],dep[N],fa[N],vis[310];
void add(int u,int y){
	nxt[++pos]=head[u];
	v[pos]=y;
	head[u]=pos;
}
void dfs(int rt,int father){
	dep[rt]=dep[father]+1; fa[rt]=father;
	for(int i=head[rt];i;i=nxt[i]){
		int j=v[i];
		if(j==father)continue;
		dfs(j,rt);
	}
}
signed main()
{
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	int res=0ll; n=read();
	for(int i=1;i<=n;i++)w[i]=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read(); add(x,y);add(y,x);
	}
	dfs(1,0);
	int xx,yy,zz;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if(i==j)continue;
			memset(vis,0,sizeof vis);
			int x=i,y=j,ans=0ll;
//			cout<<"aaa"<<i<<" "<<j<<endl;
			while(x!=y){
				if(dep[x]<dep[y])swap(x,y);
				vis[x]=1; ans+=w[x];
				x=fa[x];
			}
			vis[x]=1; ans+=w[x];
//			cout<<"kk"<<ans<<"kk ";for(int q=1;q<=n;q++)cout<<vis[q]<<" ";puts(""); 
			int tmp=x,anst=ans;
			for(int k=1;k<=n;k++){
				if(vis[k])continue;
				int z=k;x=tmp;ans=anst-w[x]; vis[x]=0;
				while(x!=z){
					if(dep[x]<dep[z])swap(x,z);
					ans+=w[x]; x=fa[x];
					if(vis[x])break;
				}
				if(!vis[x])ans+=w[x];
//				cout<<ans<<" "<<i<<" "<<j<<" "<<k<<endl;
				res=max(res,ans);
			}
		}
	}
//	cout<<xx<<" "<<yy<<" "<<zz<<endl;
	cout<<res<<endl;
	return 0;
}
/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
*/
/*
12
2 1 3 4 15 10 20 5 6 7 8 9
1 2
1 3
1 4
2 5
2 6
6 7
4 8
4 9
4 10
9 11
9 12
*/
/*
8
1 2 3 4 5 6 7 8
1 2
1 3
1 4
2 5
2 6
3 7
3 8
*/
/*
7 
1 5 3 2 4 2 1
1 2
1 3
1 4
3 5
3 6
6 7
*/
 
