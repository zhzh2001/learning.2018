#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int t=0;char ch=getchar();bool p=0;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int Z=2000400;
const int P=1e9+7;
int a,b,k,t,S,mi,ma,ans,f[2][Z*2];
signed main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	a=read();b=read();k=read();t=read();
	S=a-b+Z;f[0][S]=1;mi=S;ma=S;
	for(int i=1;i<=t;i++){
		mi-=k*2;ma+=k*2;
		memset(f[i&1],0,sizeof(f[i&1]));
		for(int j=-k-k;j<=k+k;j++)
			for(int l=mi;l<=ma;l++)
				(f[i&1][l]+=f[(i&1)^1][l-j]*(k+k+1-abs(j)))%=P;
	}
	for(int i=Z+1;i<=ma;i++)(ans+=f[t&1][i])%=P;
	cout<<ans;
	return 0;
}
