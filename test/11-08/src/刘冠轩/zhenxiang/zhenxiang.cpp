#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int t=0;char ch=getchar();bool p=0;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=100005;
int n,x,y,a[N],head[N],cnt;
struct edge{int to,nt;}e[N*2];
void add(int u,int v){
	e[++cnt]=(edge){v,head[u]};
	head[u]=cnt;
}
int f1[N],f2[N],l1[N],x1[N],ans;
void dfs(int u,int fa){
	int mx1=0,mx2=0,mx3=0,mmx1,mmx2;
	for(int i=head[u];i;i=e[i].nt)
		if(e[i].to!=fa){
			dfs(e[i].to,u);
			if(!mx1)mx1=e[i].to;
			else if(l1[e[i].to]>=l1[mx1])mx3=mx2,mx2=mx1,mx1=e[i].to;
			else if(l1[e[i].to]>=l1[mx2])mx3=mx2,mx2=e[i].to;
			else if(l1[e[i].to]>l1[mx3])mx3=e[i].to;
			if(x1[e[i].to]>=mmx1)mmx2=mmx1,mmx1=f1[e[i].to];
			else if(x1[e[i].to]>mmx2)mmx2=f1[e[i].to];
		}
	ans=max(ans,mmx1+mmx2);
	l1[u]=l1[mx1]+a[u];
	f2[u]=f1[u]=l1[mx1]+l1[mx2]+a[u];
	ans=max(f1[u],ans);
	for(int i=head[u];i;i=e[i].nt)
		if(e[i].to!=fa){
			f2[u]=max(f2[u],f2[e[i].to]+a[u]);
			if(e[i].to!=mx1)ans=max(ans,f2[e[i].to]+l1[mx1]+a[u]);
						else ans=max(ans,f2[e[i].to]+l1[mx2]+a[u]);
			if(e[i].to!=mx1)f2[u]=max(f2[u],f1[e[i].to]+l1[mx1]+a[u]);
						else f2[u]=max(f2[u],f1[e[i].to]+l1[mx2]+a[u]);
			if(e[i].to!=mx1){
				if(e[i].to!=mx2)ans=max(ans,f1[e[i].to]+l1[mx1]+l1[mx2]+a[u]);
							else ans=max(ans,f1[e[i].to]+l1[mx1]+l1[mx3]+a[u]);
			}else ans=max(ans,f1[e[i].to]+l1[mx2]+l1[mx3]+a[u]);
		}
}
signed main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=2;i<=n;i++){
		x=read();y=read();
		add(x,y);add(y,x);
	}
	dfs(1,0);
	cout<<ans;
	return 0;
}
