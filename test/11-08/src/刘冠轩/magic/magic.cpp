#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;char ch=getchar();bool p=0;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=100005;
int n,k,a[N],w[N];
bool flag;
int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n=read();k=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		w[a[i]]=i;
	}
	for(;;){
		flag=1;
		for(int i=1;i<n;i++)
			if(w[i]-w[i+1]>=k)swap(w[i],w[i+1]),flag=0;
		if(flag)break;
	}
	for(int i=1;i<=n;i++)a[w[i]]=i;
	for(int i=1;i<=n;i++)cout<<a[i]<<' ';
	return 0;
}
