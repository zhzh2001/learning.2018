#include<bits/stdc++.h>
#define ll long long
//#define int long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
#define mod 1000000007
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=0;
	while (ch<'0'||ch>'9') { if (ch=='-') f=1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return f?-x:x;
}
ll n,k,tot,head[100100],lzq[100100],pos[100100];
struct D{
	ll to,nxt;
}e[10001000];
void add(ll u,ll v)
{
	e[++tot]=(D){v,head[u]}; head[u]=tot;
	++lzq[v];
}
priority_queue<int> Q;
signed main()
{
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n=read(); k=read();
	For(i,1,n)
	{
		ll x=read();
		pos[x]=i;
	}
	For(i,1,n)
		For(j,i+1,n)
			if(abs(pos[i]-pos[j])<k) add(pos[i],pos[j]);
	For(i,1,n)
		 if(!lzq[i])Q.push(-i);
	while(!Q.empty())
	{
		ll u=-Q.top();
		Q.pop();
		cout<<u<<" ";
		for (ll i=head[u],v=e[i].to;i;i=e[i].nxt,v=e[i].to)
			if (!(--lzq[v]))Q.push(-v);
	}
	return 0;
}
