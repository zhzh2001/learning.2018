/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(ll x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(ll x){wr(x);put;}inline void wri(ll x){wr(x);putchar(' ');}
inline void wrn(ll x,ll y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,a[N],fa[N];
ll f[N][3],ans;
//冷静分析。不是树形dp？ 
//很虚啊。 
int Next[N*2],head[N],to[N*2],nedge;
#define V to[i]
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;}
void add_ne(int a,int b){add(a,b);add(b,a);}
void dfs(int x){
	//f[x][0]表示1条升上来的 
	//f[x][1]表示有1条,没有升上来的
	//f[x][2]表示有1条和一条升上来的 
	f[x][0]=f[x][1]=f[x][2]=a[x];
	ll t=0;
	go(i,x){
		if (V==fa[x]) continue;
		fa[V]=x;dfs(V);
		ans=max(ans,f[x][2]+f[V][0]);
		ans=max(ans,f[x][1]+f[V][1]);
		ans=max(ans,f[x][0]+f[V][2]);
		f[x][2]=max(f[x][2],f[V][1]+f[x][0]);
		f[x][2]=max(f[x][2],f[V][0]+a[x]+t);
		t=max(t,f[V][1]);
		f[x][1]=max(f[x][1],f[V][1]);
		f[x][1]=max(f[x][1],f[V][0]+f[x][0]);
		f[x][0]=max(f[x][0],f[V][0]+a[x]);
	}
	ans=max(ans,max(max(f[x][0],f[x][1]),f[x][2]));
}
signed main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	F(i,1,n) a[i]=read();
	F(i,1,n-1) add_ne(read(),read());
	dfs(1);
	wrn(ans);
	return 0;
}
