/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,k,t,a,b,sum[N];
inline void add(int &x,int k){x+=k;x-=(x>=mod)?mod:0;}
struct xx{
	int f[N],g[N];
	void dp(int x){
		f[x]=1;
		int l=x,r=x;
		F(ci,1,t){
			l-=k;r+=k;
			F(i,l,r){
				F(j,i-k,i+k){
					add(g[i],f[j]);
				}
			}
			F(i,l,r){
				f[i]=g[i];g[i]=0;
			}
		}
	}
}a1,b1;
signed main(){
	freopen("score.in","r",stdin);
	freopen("std.out","w",stdout);
	a=read();b=read();k=read();t=read();
	a+=200000;b+=200000;
	a1.dp(a);b1.dp(b);
	F(i,1,400000){
		sum[i]=(sum[i-1]+b1.f[i])%mod;
	}
	F(i,1,400000){
		ans=(ans+1LL*a1.f[i]*sum[i-1])%mod;
	}
	wrn(ans);
	return 0;
}
