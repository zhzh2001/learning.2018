#include<bits/stdc++.h>
using namespace std;
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
long long n,ans;
long long a[100005],f[100005][4];
long long tot,head[100005],nx[200005],to[200005];
void jia(long long aa,long long bb)
{
	tot++;
	nx[tot]=head[aa];
	to[tot]=bb;
	head[aa]=tot;
	return;
}
void dfs(long long rt,long long fa)
{
	f[rt][0]=a[rt];f[rt][1]=a[rt];
	f[rt][2]=a[rt];f[rt][3]=a[rt];
	long long mx=0;
	for(long long i=head[rt];i;i=nx[i])
	{
		long long yy=to[i];
		if(yy==fa) continue;
		dfs(yy,rt);
		ans=max(ans,max(f[rt][3]+f[yy][0],f[rt][1]+f[yy][0]));
		ans=max(ans,max(f[rt][0]+f[yy][1],f[rt][2]+f[yy][2]));
		ans=max(ans,f[rt][0]+f[yy][3]);
		f[rt][3]=max(max(f[rt][3],mx+f[yy][0]+a[rt]),max(f[rt][0]+f[yy][2],f[yy][3]+a[rt]));
		f[rt][2]=max(f[rt][2],max(f[rt][0]+f[yy][0],f[yy][2]));
		f[rt][1]=max(f[rt][1],max(f[rt][0]+f[yy][0],f[yy][1]+a[rt]));
		f[rt][0]=max(f[rt][0],f[yy][0]+a[rt]);
		mx=max(mx,f[yy][2]);
	}
	ans=max(max(max(ans,f[rt][0]),max(f[rt][1],f[rt][2])),f[rt][3]);
	return;
}
int main()
{
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	for(long long i=1;i<=n;++i) a[i]=read();
	for(long long i=1;i<n;++i)
	{
		long long x=read(),y=read();
		jia(x,y);jia(y,x);
	}
	dfs(1,0);
	write(ans);
	return 0;
}
