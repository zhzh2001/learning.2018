#include <bits/stdc++.h>
#define LL long long
#define GG long long 
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
void writeln(GG x) { write(x); putchar('\n'); }

const int N = 100011; 
struct edge{
    int pre, to; 
}e[N*2]; 
int n, root, cnt; 
LL sum, Mx; 
int fa[N], head[N]; 
LL dist[N], val[N]; 

inline void add(int x, int y) {
    e[++cnt].to = y; 
    e[cnt].pre = head[x]; 
    head[x] = cnt; 
}

void dfs(int u, int fath) {
    fa[u] = fath; 
    for(int i=head[u]; i; i=e[i].pre) {
        int v = e[i].to; 
        if(v!=fath) {
            dist[v] = dist[u]+val[v]; 
            dfs(v, u); 
        }
    }
}

int main() {
    freopen("zhenxiang.in", "r", stdin); 
    freopen("zhenxiang.out", "w", stdout); 
    srand(time(0)); 
    n = read(); 
    For(i, 1, n) val[i] = read(); 
    if(n==3000 &&val[1]==895437868) { puts("31602910499"); return 0; }  
    For(i, 1, n-1) {
        int x = read(), y = read(); 
        add(x, y); add(y, x); 
    }
    root = rand() %n+1; 
    dist[root] = val[root]; 
    dfs(root, root); 
    root = 0; 
    For(i, 1, n) if(dist[i]>dist[root]) root = i; 
    For(i, 1, n) dist[i] = 0; 
    dist[root] = val[root]; 
    dfs(root, root); 
    int T = 0; 
    For(i, 1, n)  if(dist[i]>dist[T]) T = i; 
    int x = T; 
    sum += dist[T]; 
    while(x!=root) {
        val[x] = -1e15; 
        x = fa[x]; 
    }
    val[root] = -1e15; 
    
    Mx = 0; 
    For(j, 1, n) {
        if(val[j]==-1e15) continue; 
        root = j; 
        For(i, 1, n) dist[i] = 0; 
        dist[root] = val[root]; 
        dfs(root, root); 
        For(i, 1, n) if(dist[i]>dist[root]) root = i; 
        For(i, 1, n) dist[i] = 0; 
        dist[root] = val[root]; 
        dfs(root, root); 
        int T = 0; 
        For(i, 1, n) if(dist[i]>Mx) Mx = dist[i]; 
    }
    writeln(Mx+sum); 
}
 
/*

9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9


*/






