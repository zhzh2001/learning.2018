#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
void writeln(GG x) { write(x); putchar('\n'); }

const int N = 100011; 
int n, K; 
int a[N], pos[N], tmp[N]; 

inline void work_1() {
    For(i, 1, n) write(i), putchar(' ');
    exit(0);  
}

int main() {
    freopen("magic.in", "r", stdin); 
    freopen("magic.out", "w", stdout); 
	n = read(); K = read(); 
	For(i, 1, n) {
        a[i] = read(); 
        pos[a[i]] = i; 
    }
	if(K==1) {
	    For(i, 1, n) write(i), putchar(' '); 
	    return 0; 
    }
    work_1();
}








