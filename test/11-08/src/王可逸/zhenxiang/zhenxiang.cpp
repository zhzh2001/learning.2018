#include <bits/stdc++.h>
#define int long long
using namespace std;
//fast io
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
void write(int x) {
	if(x>=10) write(x/10);
	register char ch=x%10+'0';
	putchar(ch);	
}
//fast io
int w[120000],n,cnt,head[120000];
struct node {int next,to;} sxd[240000];
int f[120000][3],g[120000][3][3],h[120000][3][3],mx,ans;
bool vis[120000],vis2[120000];
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;	
	head[u]=cnt;
}
void dfs(int at,int fa) {
	int mx_2=w[at],mx_2_at=at,mx_1_mx=0,mx_1_sec=0,mx_1_mx_at=0,mx_1_sec_at=0,mx_0=0,mx_0_at=0;
	for(int i=head[at];i;i=sxd[i].next) {
		if(sxd[i].to==fa) continue;
		dfs(sxd[i].to,at);
		if(f[sxd[i].to][0]>mx_0) {
			mx_0=f[sxd[i].to][0];
			mx_0_at=sxd[i].to;	
		}
		if(f[sxd[i].to][2]+w[at]>mx_2) {
			mx_2=f[sxd[i].to][2]+w[at];
			mx_2_at=sxd[i].to;	
		}
		if(f[sxd[i].to][2]+w[at]>mx_1_mx) {
			mx_1_mx=f[sxd[i].to][2]+w[at];
			mx_1_mx_at=sxd[i].to;	
		} else if(f[sxd[i].to][2]+w[at]>mx_1_sec) {
			mx_1_sec=f[sxd[i].to][2]+w[at];
			mx_1_sec_at=sxd[i].to;
		}	
	}
	f[at][0]=mx_0;
	g[at][0][0]=mx_0_at;
	if(mx_1_mx_at && mx_1_sec_at) {
		f[at][1]=mx_1_mx+mx_1_sec-w[at];
		g[at][1][1]=mx_1_mx_at;
		g[at][1][2]=mx_1_sec_at;
		h[at][1][1]=mx_1_mx-w[at];
		h[at][1][2]=mx_1_sec-w[at];	
	}
	f[at][2]=mx_2;
	g[at][2][0]=mx_2_at;
}
void dfs2(int at,int fa) {
	int mx_2=w[at],mx_2_at=at,mx_1_mx=0,mx_1_sec=0,mx_1_mx_at=0,mx_1_sec_at=0,mx_0=0,mx_0_at=0;
	for(int i=head[at];i;i=sxd[i].next) {
		if(sxd[i].to==fa || vis2[sxd[i].to]) continue;
		dfs(sxd[i].to,at);
		if(f[sxd[i].to][0]>mx_0) {
			mx_0=f[sxd[i].to][0];
			mx_0_at=sxd[i].to;	
		}
		if(f[sxd[i].to][2]+w[at]>mx_2) {
			mx_2=f[sxd[i].to][2]+w[at];
			mx_2_at=sxd[i].to;	
		}
		if(f[sxd[i].to][2]+w[at]>mx_1_mx) {
			mx_1_mx=f[sxd[i].to][2]+w[at];
			mx_1_mx_at=sxd[i].to;	
		} else if(f[sxd[i].to][2]+w[at]>mx_1_sec) {
			mx_1_sec=f[sxd[i].to][2]+w[at];
			mx_1_sec_at=sxd[i].to;
		}	
	}
	f[at][0]=mx_0;
	g[at][0][0]=mx_0_at;
	if(mx_1_mx_at && mx_1_sec_at) {
		f[at][1]=mx_1_mx+mx_1_sec-w[at];
		g[at][1][1]=mx_1_mx_at;
		g[at][1][2]=mx_1_sec_at;
		h[at][1][1]=mx_1_mx-w[at];
		h[at][1][2]=mx_1_sec-w[at];	
	}
	f[at][2]=mx_2;
	g[at][2][0]=mx_2_at;
}
void qd(int at,int xkx) {
	if(!xkx) return;
	if(f[at][0]==xkx) {
		qd(g[at][0][0],xkx);
	}
	if(f[at][2]==xkx) {
		vis[at]=true;
		qd(g[at][2][0],xkx-w[at]);
	}
	if(f[at][1]==xkx) {
		vis[at]=true;
		qd(g[at][1][1],h[at][1][1]);
		qd(g[at][1][2],h[at][1][2]);
	}
}
signed main() {
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	cin>>n;
	for(int i=1;i<=n;i++) w[i]=read();
	for(int i=1;i<n;i++) {
		int x,y;
		x=read();
		y=read();	
		add(x,y),add(y,x);
	}
	dfs(1,0);
	for(int i=0;i<=2;i++) mx=max(mx,f[1][i]);
	ans=mx;
	qd(1,mx);
	mx=0;
	for(int i=1;i<=n;i++) vis2[i]=vis[i];
	memset(f,0,sizeof f);
	for(int i=2;i<=n;i++) {
		if(!vis2[i]) {
			dfs2(i,0);
			for(int j=0;j<=2;j++) mx=max(mx,f[i][j]);
		}
	}
	cout<<ans+mx<<'\n';
	return 0;
}
