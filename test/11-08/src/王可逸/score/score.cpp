#include <bits/stdc++.h>
#define int long long
using namespace std;
const int mod=1e9+7;
int a,b,k,t,len,ans[1200000],bb[1200000],tree[4800000],add[5200000],tot;
void build(int rt,int l,int r) {
	if(l==r) {
		tree[rt]=ans[l];
		return;
	}
	int mid=(l+r)>>1;
	build(rt<<1,l,mid);
	build(rt<<1|1,mid+1,r);
	tree[rt]=tree[rt<<1]+tree[rt<<1|1]%mod;
}
int he(int L,int R,int l,int r,int rt) {
	if(L>R) return 0;
	if(L<=l && r<=R) return tree[rt];
	int mid=(l+r)>>1,pfy=0;
	if(L<=mid) pfy=(pfy+he(L,R,l,mid,rt<<1))%mod;	
	if(R>mid) pfy=(pfy+he(L,R,mid+1,r,rt<<1|1))%mod;
	return pfy;
}
signed main() {
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	cin>>a>>b>>k>>t;
	for(register int i=1;i<=k*2+1;i++) ans[i]=1;
//	for(register int i=1;i<t;i++) {
//		for(register int j=1;j<=k*2+1+k*2*(i-1);j++) bb[j]=ans[j];
//		for(register int j=1;j<=k*2;j++) {
//			int st=1+j,cnt=1;
//			for(register int sxd=st;cnt<=k*2+1+k*2*(i-1);sxd++,cnt++) {
//				ans[sxd]=ans[sxd]+bb[cnt];
//				ans[sxd]%=mod;
//			}
//		}	
//	}
	for(register int i=1;i<t;i++) {
		int ww=k*2+1+k*2*(i-1);
		for(register int j=1;j<=ww;j++) bb[j]=ans[j];
		int now=bb[1];
		for(int j=2;j<=k*2+1;j++) {
//			cout<<"j="<<j<<'\n';
			now+=bb[j];
			now%=mod;	
			ans[j]=now;
		}
		for(int j=k*2+2,cnt=2;j<=ww;cnt++,j++) {
//			cout<<"j="<<j<<'\n';
			now-=bb[cnt-1];
			now=(now+mod)%mod;	
			now+=bb[j];
			now%=mod;
			ans[j]=now;
		}
		for(int j=ww+k*2,cnt=1;cnt<=k*2+1;cnt++,j--) {
			ans[j]=ans[cnt];	
		}
//		for(int j=1;j<=k*2+1+k*2*i;j++) cout<<ans[j]<<" ";
//		cout<<'\n';
	}
	len=k*2+1+k*2*(t-1);
//	for(int i=1;i<=len;i++) cout<<ans[i]<<" ";
//	cout<<'\n';
	build(1,1,len);
	for(register int i=1;i<=len;i++) {
		tot=(tot+ans[i]*he((1),(i-(b-a)-1),1,len,1)%mod)%mod;
	}
	cout<<tot<<'\n';
}
