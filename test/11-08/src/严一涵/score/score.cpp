#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long LL; 
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const LL md=1000000007;
LL x,y,z,m,n;
LL f[4000*103],g[4000*103];
void add(LL&a,LL b){a+=b;if(a>md)a-=md;if(a<0)a+=md;}
int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	x=read();
	y=read();
	m=read();
	n=read();
	memset(f,0,sizeof(f));
	f[x-y+2*n*m]=1;
	for(int i=1;i<=n;i++){
		x=f[0];
		y=0;
		for(int j=1;j<=m+m+1;j++)add(y,f[j]);
		z=0;
		for(int j=0;j<=m+m;j++)add(z,f[j]*(m+m-j+1)%md);
		for(int j=0;j<=4*n*m;j++){
			g[j]=z;
			add(z,-x);
			add(z,y);
			if(j>=m+m)add(x,-f[j-m-m]);
			add(x,f[j+1]);
			add(y,-f[j+1]);
			add(y,f[j+m+m+2]);
		}
		memcpy(f,g,sizeof(f));
	}
	z=0;
	for(int i=2*n*m+1;i<=4*n*m;i++){
		add(z,f[i]);
	}
	printf("%lld\n",z);
	return 0;
}
