#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,k,a[100003],b[100003],x;
int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n=read();
	k=read();
	if(k==1){
		for(int i=1;i<=n;i++)printf("%d ",i);
		return 0;
	}
	for(int i=1;i<=n;i++){
		x=read();
		a[x]=i;
	}
	for(int i=1;i<=n;i++){
		for(x=i;x>1&&a[x-1]>=a[x]+k;x--)swap(a[x],a[x-1]);
	}
	for(int i=1;i<=n;i++){
		b[a[i]]=i;
	}
	for(int i=1;i<=n;i++){
		printf("%d ",b[i]);
	}
	return 0;
}
