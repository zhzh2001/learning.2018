#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
typedef long long LL; 
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
LL n,a[100003],x,y,zk[100003],ck[100003],zl[100003],cl[100003],cck[100003],al[100003],tp[100003],nl[100003],ans;
vector<int>e[100003];
inline void Max(LL&a,LL&b,LL d){
	if(d>a){
		a=d;
	}else if(d>b){
		b=d;
	}
}
inline void Max(LL&a,LL&b,LL&c,LL d){
	if(d>a){
		a=d;
	}else if(d>b){
		b=d;
	}else if(d>c){
		c=d;
	}
}
void dfs(int u,int fa){
	zk[u]=a[u];
	ck[u]=a[u];
	zl[u]=0;
	cl[u]=0;
	for(vector<int>::iterator i=e[u].begin();i!=e[u].end();i++){
		if(*i==fa)continue;
		dfs(*i,u);
		Max(zk[u],ck[u],cck[u],zk[*i]+a[u]);
		Max(zl[u],cl[u],al[*i]);
	}
	al[u]=max(zl[u],zk[u]+ck[u]-a[u]);
}
void gns(int u,int fa){
	if(fa!=-1){
		tp[u]=max(tp[fa],max((zk[u]+a[fa]==zk[fa]?zk[fa]:ck[fa])+nl[fa]-a[fa],(al[u]==zl[fa]?zl[fa]:cl[fa])));
		if(zk[u]+a[fa]==zk[fa])tp[u]=max(tp[u],ck[fa]+cck[fa]-a[fa]);
		else if(zk[u]+a[fa]==ck[fa])tp[u]=max(tp[u],zk[fa]+cck[fa]-a[fa]);
		else tp[u]=max(tp[u],zk[fa]+ck[fa]-a[fa]);
		nl[u]=max(nl[fa]+a[u],(zk[u]+a[fa]==zk[fa]?zk[fa]:ck[fa])+a[u]);
	}else{
		tp[u]=0;
		nl[u]=a[u];
	}
	ans=max(ans,al[u]+tp[u]);
	for(vector<int>::iterator i=e[u].begin();i!=e[u].end();i++){
		if(*i==fa)continue;
		gns(*i,u);
	}
}
int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++){
		x=read();
		y=read();
		e[x].push_back(y);
		e[y].push_back(x);
	}
	dfs(1,-1);
	ans=0;
	gns(1,-1);
	printf("%lld\n",ans);
	return 0;
}
