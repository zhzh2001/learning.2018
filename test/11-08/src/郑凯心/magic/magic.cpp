#include <bits/stdc++.h>

using namespace std;

const int Maxn = 1e5+7;
const int P = 1e9+7;
const int TLE = 1e6;

typedef unsigned long long ull;
int n, k, cnt;
int a[Maxn], ans[Maxn];
map<ull, int> vis;

inline ull hashshit(int *x)
{
	ull res = 0;
	for(int i = 1; i <= n; ++i)
		res = res*P+x[i];
	return res;
}

inline bool cmp(int *x, int *y) // x < y
{
	for(int i = 1; i <= n; ++i)
	{
		if(x[i] < y[i]) return true;
		if(x[i] > y[i]) return false;
	}
	return false;
}

inline void print(int *x)
{
	for(int i = 1; i < n; ++i)
		printf("%d ", x[i]);
	printf("%d\n", x[n]);
}

void Fuckit()
{
	print(ans);
	exit(0);
}

void brute()
{
	for(int i = 1; i+k <= n; ++i)
	{
		for(int j = i+k; j <= n; ++j)
		{
			if(abs(a[i]-a[j]) == 1)
			{
				swap(a[i], a[j]);
				ull val = hashshit(a);
				if(++cnt >= TLE) Fuckit();
				if(!vis.count(val))
				{
					if(cmp(a, ans)) memcpy(ans, a, sizeof(int)*(n+2));
					vis.insert(make_pair(val, 1));
					brute();
				}
				swap(a[i], a[j]);
			}
		}
	}
}

int main()
{
	freopen("magic.in", "r", stdin);
	freopen("magic.out", "w", stdout);
	scanf("%d%d", &n, &k);
	for(int i = 1; i <= n; ++i) scanf("%d", a+i);
	if(k == 1)
	{
		for(int i = 1; i <= n; ++i) ans[i] = i;
		print(ans);
		return 0;
	}
	if(k == n)
	{
		print(a);
		return 0;
	}
	if(k == n-1)
	{
		if(a[1] > a[n] && a[1]-a[n] == 1)
			swap(a[1], a[n]);
		print(a);
		return 0;
	}
	memcpy(ans, a, sizeof(int)*(n+2));
	brute();
	print(ans);
	return 0;
}
