#include <bits/stdc++.h>

using namespace std;

const int Maxk = 1e3+7;
const int Maxt = 1e2+7;
const int MOD = 1e9+7;

int a, b, k, t;
long long ans;
int c[2][2*Maxk*Maxt], d[2][2*Maxk*Maxt];
int *f[2], *s[2];
// f[t][k] at t get val k

inline int sum(int i, int l, int r)
{
	if(l > r) return 0;
	return (s[i][r]-s[i][l-1]+MOD)%MOD;
}

int main()
{
	freopen("score.in", "r", stdin);
	freopen("score.out", "w", stdout);
	scanf("%d%d%d%d", &a, &b, &k, &t);
	
	f[0] = &c[0][Maxk*Maxt]; f[1] = &c[1][Maxk*Maxt];
	s[0] = &d[0][Maxk*Maxt]; s[1] = &d[1][Maxk*Maxt];
	f[0][0] = 1;
	for(int i = 0; i <= k; ++i) s[0][i] = 1;
	
	for(int i = 1; i <= t; ++i) // at t
		for(int j = -k*i; j <= k*i; ++j) // val at t
		{
			f[i&1][j] = sum((i+1)&1, max(j-k, -k*(i-1)),min(j+k, k*(i-1)));
			s[i&1][j] = (s[i&1][j-1]+f[i&1][j])%MOD;
		}

	for(int i = -k*t; i <= k*t; ++i)
		for(int j = max(i-a+b+1, -k*t); j <= k*t; ++j)
			(ans += 1ll*f[t&1][i]*f[t&1][j]%MOD) %= MOD;
	printf("%lld\n", ans);
	return 0;
}
