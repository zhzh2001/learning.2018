#include <bits/stdc++.h>

using namespace std;

const int Maxn = 1e5+7;

int n, a[Maxn];
long long ans, f[Maxn][3]; // at tree has one line or two
vector<int> e[Maxn];

long long treedp(int cur, int fa) // return the long one line, not must can connect
{
	vector<long long> tmp;
	long long secm = 0; // second long, can connect
    for(vector<int>::iterator it = e[cur].begin(); it != e[cur].end(); ++it)
    {
        if(*it == fa) continue;
        // all child's long (can't connect)
        //tmp.push_back(f[*it][0] = treedp(*it, cur));
        tmp.push_back(treedp(*it, cur));
        // first long and second (can connect);
        if(f[*it][1] > f[cur][1])
        {
        	secm = f[cur][1];
        	f[cur][1] = f[*it][1];
		}
		else secm = max(secm, f[*it][1]);
		// *it has more than two child
		if(f[*it][2])
        	f[cur][2] = max(f[cur][2], f[*it][2]);
    }
    
    f[cur][1] += a[cur]; // add it self
    if(secm) // cur has more than two child(one line)
	{
		secm += f[cur][1];
		f[cur][2] = max(f[cur][2], secm);
	}
    if(f[cur][2]) // cur has more than two child, add it self
		f[cur][2] += a[cur];
	
	// child's long two
	if(tmp.size() >= 2)
	{
		nth_element(tmp.begin(), tmp.end()-2, tmp.end());
		ans = max(ans, tmp.back()+tmp[tmp.size()-2]);
	}
	ans = max(ans, max(f[cur][1], f[cur][2]));
	// long is it(can connect && can't connect), it's child's long
	if(tmp.size()) secm = max(secm, tmp.back());
	return max(f[cur][1], secm);
}

inline void print()
{
	for(int i = 1; i <= n; ++i)
	{
		for(int j = 0; j < 3; ++j)
			printf("%lld ", f[i][j]);
		putchar('\n');
	}
}

int main()
{
	freopen("zhenxiang.in", "r", stdin);
	freopen("zhenxiang.out", "w", stdout);
	scanf("%d", &n);
	for(int i = 1; i <= n; ++i) scanf("%d", a+i);
	for(int i = 1, u, v; i < n; ++i)
	{
		scanf("%d%d", &u, &v);
		e[u].push_back(v);
		e[v].push_back(u);
	}
	//ans = max(ans, (f[1][0] = treedp(1, 0)));
	ans = max(ans, (treedp(1, 0)));
	//print()
	memset(f, 0, sizeof f);
	ans = max(ans, (treedp(n, 0)));
	//print();
	memset(f, 0, sizeof f);
	srand(time(NULL));
	ans = max(ans, (treedp(rand()%n+1, 0)));
	//print();
	printf("%lld\n", ans);
	return 0;
}
