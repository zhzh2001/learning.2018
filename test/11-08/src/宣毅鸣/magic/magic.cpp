#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int f[N],d[N],l,n,m,a[N],b[N];
void down(int x){
	int i=x;
	if (x*2<=l&&f[x*2]<f[x])i=x*2;
	if (x*2<l&&f[x*2+1]<f[i])i=x*2+1;
	if (i!=x){
		swap(f[x],f[i]);
		swap(d[f[i]],d[f[x]]);
		down(i);
	}
}
void up(int x){
	if (x==1)return;
	if (f[x/2]>f[x]){
		swap(f[x/2],f[x]);
		swap(d[f[x/2]],d[f[x]]);
		up(x/2);
	}
}
int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)scanf("%d",&a[i]),b[a[i]]=i;
	if (m==1){
		for (int i=1;i<=n;i++)printf("%d ",i);
		return 0;
	}
	for (int i=1;i<=n;i++)
		if (b[a[i]-1]-i>=m)f[++l]=i,d[i]=l;		
	while (l){
		int now=f[1],k=a[now];
		d[now]=0;
		f[1]=f[l--];d[f[1]]=1;
		if (d[b[k-1]]){
			int p=d[b[k-1]];d[b[k-1]]=0;			
			if (p==l)l--;
			else {
				f[p]=f[l--];
				d[f[p]]=p;
			}
		}
		if (d[b[k+1]]){
			int p=d[b[k+1]];d[b[k+1]]=0;	
			if (p==l)l--;
			else {
				f[p]=f[l--];
				d[f[p]]=p;
			}
		}
		swap(a[now],a[b[k-1]]);
		swap(b[k],b[k-1]);
		if (k>2&&b[k-2]-b[k-1]>=m){
			f[++l]=b[k-1];
			d[b[k-1]]=l;
		}
		if (b[k]-b[k+1]>=m&&k!=n){
			f[++l]=b[k+1];
			d[b[k+1]]=l;
		}
	}	
	for (int i=1;i<=n;i++)printf("%d ",a[i]);
	return 0;
}
