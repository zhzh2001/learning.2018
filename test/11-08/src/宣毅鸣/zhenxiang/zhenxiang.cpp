#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=200005;
int ne[N],fi[N],zz[N],tot,ans,x,y,f1[N],f2[N],f3[N],n,A[N];
int read(){
	int x=0;char c=0;
	for (;c<'0'||c>'9';c=getchar());
	for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
	return x;
}
void jb(int x,int y){
	ne[++tot]=fi[x];
	fi[x]=tot;
	zz[tot]=y;
}
void dfs1(int x,int y){
	f1[x]=A[x];
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=y){
			dfs1(zz[i],x);
			f1[x]=max(f1[x],f1[zz[i]]+A[x]);
		}
}
void dfs2(int x,int y){
	int max1=0,max2=0;
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=y){
			dfs2(zz[i],x);
			f2[x]=max(f2[x],f2[zz[i]]);
			if (max2<f1[zz[i]])max2=f1[zz[i]];
			if (max1<max2)swap(max1,max2);
		}
	f2[x]=max(f2[x],max1+max2+A[x]);	
}
void dfs3(int x,int y){
	int a[3]={0,0,0},b[3]={0,0,0};
	f3[x]=A[x];
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=y){
			dfs3(zz[i],x);
			f3[x]=max(f3[x],f3[zz[i]]+A[x]);
			if (f1[zz[i]]>f1[a[2]]){
				a[2]=zz[i];
				for (int i=2;i;i--)
					if (f1[a[i]]>f1[a[i-1]])swap(a[i],a[i-1]);
			}
			if (f2[zz[i]]>f2[b[2]]){
				b[2]=zz[i];
				for (int i=2;i;i--)
					if (f2[b[i]]>f2[b[i-1]])swap(b[i],b[i-1]);
			}
		}	
	for (int i=0;i<3;i++)
		for (int j=0;j<3;j++)
			if (a[i]!=b[j])f3[x]=max(f3[x],A[x]+f1[a[i]]+f2[b[j]]);		
}
void dfs4(int x,int y){
	int a[3]={0,0,0},b[3]={0,0,0},c[3]={0,0,0};
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=y){
			dfs4(zz[i],x);
			if (f1[zz[i]]>f1[a[2]]){
				a[2]=zz[i];
				for (int i=2;i;i--)
					if (f1[a[i]]>f1[a[i-1]])swap(a[i],a[i-1]);
			}
			if (f2[zz[i]]>f2[b[2]]){
				b[2]=zz[i];
				for (int i=2;i;i--)
					if (f2[b[i]]>f2[b[i-1]])swap(b[i],b[i-1]);
			}
			if (f3[zz[i]]>f3[c[2]]){
				c[2]=zz[i];
				for (int i=2;i;i--)
					if (f3[c[i]]>f3[c[i-1]])swap(c[i],c[i-1]);
			}			
		}
	for (int i=0;i<3;i++)
		for (int j=i+1;j<3;j++)
			for (int k=0;k<3;k++)
				if (a[i]!=b[k]&&a[j]!=b[k]&&a[i]!=a[j])
					ans=max(ans,f1[a[i]]+f1[a[j]]+f2[b[k]]+A[x]);
	for (int i=0;i<3;i++)
		for (int j=0;j<3;j++)
			if (a[i]!=c[j])ans=max(ans,f1[a[i]]+f3[c[j]]+A[x]);	
	for (int i=0;i<3;i++)
		for (int j=i+1;j<3;j++)
			if (b[i]!=b[j])ans=max(ans,f2[b[i]]+f2[b[j]]);					
}
signed main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++)A[i]=read();
	for (int i=1;i<n;i++){
		int x=read(),y=read();
		jb(x,y);jb(y,x);
	}
	dfs1(1,0);dfs2(1,0);
	dfs3(1,0);dfs4(1,0);		
	printf("%lld",ans);
	return 0;
}
