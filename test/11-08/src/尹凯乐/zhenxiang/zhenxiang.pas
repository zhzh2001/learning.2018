program zhenxiang;
 uses math;
 type
  wb=record
   n,t:longint;
  end;
 var
  b:array[0..200001] of wb;
  a,h,z,y,x:array[0..100001] of longint;
  d:array[0..100001] of int64;
  i,n:longint;
  o,p,q,smnax:int64;
 procedure hahainc(x,y:longint);
  begin
   inc(p);
   b[p].n:=h[x];
   b[p].t:=y;
   h[x]:=p;
  end;
 function hahaDFS(k,x:longint):int64;
  var
   i,j,p:longint;
   smax,snax,s:int64;
  begin
   hahaDFS:=0;
   smax:=0;
   snax:=0;
   j:=h[k];
   while j<>0 do
    begin
     p:=j;
     i:=b[j].t;
     j:=b[j].n;
     if (i=x) or (p=2*q-1) or (p=2*q) then continue;
     s:=hahaDFS(i,k);
     if d[i]>smax then
      begin
       snax:=smax;
       smax:=d[i];
      end
               else if d[i]>snax then snax:=d[i];
     hahaDFS:=max(hahaDFS,s);
    end;
   d[k]:=smax+a[k];
   hahaDFS:=max(hahaDFS,smax+snax+a[k]);
  end;
 begin
  assign(input,'zhenxiang.in');
  assign(output,'zhenxiang.out');
  reset(input);
  rewrite(output);
  p:=0;
  filldword(h,sizeof(h)>>2,0);
  readln(n);
  for i:=1 to n do
   read(a[i]);
  readln;
  for i:=1 to n-1 do
   begin
    readln(x[i],y[i]);
    hahainc(x[i],y[i]);
    hahainc(y[i],x[i]);
   end;
  smnax:=0;
  for i:=1 to n-1 do
   begin
    q:=i;
    o:=hahaDFS(x[q],0);
    p:=hahaDFS(y[q],0);
    smnax:=max(smnax,o+p);
   end;
  writeln(smnax);
  close(input);
  close(output);
 end.
