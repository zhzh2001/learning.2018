program score;
 const
  nmax=100000;
 var
  f:array[-200001..200001,0..1] of int64;
  b:array[-400001..400001] of int64;
  i,j,k,m,n,p,t,ac,bc,mot:longint;
  ssum:int64;
 begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  mot:=1000000007;
  readln(ac,bc,k,t);
  n:=bc-ac;
  m:=k*t<<1;
  fillqword(f,sizeof(f)>>3,0);
  fillqword(b,sizeof(b)>>3,0);
  f[0,0]:=1;
  for i:=0 to m<<1 do
   b[i]:=1;
  for i:=1 to t do
   begin
    p:=i and 1;
    for j:=-m to -m+2*k do
     f[-m,p]:=f[j,1-p]*(k<<1+1-j-m) mod mot;
    for j:=-m+1 to m do
     f[j,p]:=(f[j-1,p]-b[j-1]+b[j-1-k<<1-1]+b[j+k<<1]-b[j-1]+mot+mot) mod mot;
    for j:=-m to m<<1 do
     b[j]:=(b[j-1]+f[j,p]) mod mot;
   end;
  ssum:=0;
  for i:=n+1 to m do
   ssum:=(ssum+f[i,p]) mod mot;
  writeln(ssum);
  close(input);
  close(output);
 end.
