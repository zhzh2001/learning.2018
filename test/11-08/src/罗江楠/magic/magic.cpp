#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int p[N];

namespace __segment_tree {

int mx[N<<2];
void push_up(int x) {
  mx[x] = max(mx[x << 1], mx[x << 1 | 1]);
}
void update(int x, int k, int l, int r, int v) {
  if (l == r) {
    mx[x] = v;
    return;
  }
  int mid = (l + r) >> 1;
  if (k <= mid) update(x << 1, k, l, mid, v);
  else update(x << 1 | 1, k, mid + 1, r, v);
  push_up(x);
}
int query(int x, int l, int r, int L, int R) {
  if (l == L && r == R) {
    return mx[x];
  }
  int mid = (L + R) >> 1;
  if (r <= mid) return query(x << 1, l, r, L, mid);
  if (l > mid) return query(x << 1 | 1, l, r, mid + 1, R);
  return max(query(x << 1, l, mid, L, mid), query(x << 1 | 1, mid + 1, r, mid + 1, R));
}

} // namespace __segment_tree

using namespace __segment_tree;

vector<int> son[N];
priority_queue<int, vector<int>, greater<int> > que;

int ans[N];

int main(int argc, char const *argv[]) {
  freopen("magic.in", "r", stdin);
  freopen("magic.out", "w", stdout);

  int n = read(), k = read();
  for (int i = 1; i <= n; ++ i) {
    p[read()] = i;
  }
  for (int i = 1; i <= n; ++ i) {
    int pos = query(1, p[i], min(p[i] + k - 1, n), 1, n);
    if (pos) {
      son[p[pos]].push_back(p[i]);
    } else {
      que.push(p[i]);
    }
    update(1, p[i], 1, n, i);
  }

  int cnt = 0;
  while (!que.empty()) {
    int x = que.top(); que.pop();
    ans[x] = ++ cnt;
    for (size_t i = 0; i < son[x].size(); ++ i) {
      que.push(son[x][i]);
    }
  }

  for (int i = 1; i <= n; ++ i) {
    printf("%d%c", ans[i], i == n ? '\n' : ' ');
  }

  return 0;
}
/*
8 3
4 5 7 8 3 1 2 6

求出逆排列
发现对于在原排列上的操作等价于交换现在相邻两个绝对值大于等于k的元素

我们发现如果 k = 2, 而且 2 在 3 的后面，那么 2 永远不可能到达 3 的前面
也就是说绝对值不大于 k 的元素的相对顺序不会改变

6 7 5 1 2 8 3 4
+
  +
  ^-+
      +
        +
          +
    ^-------+
    ^---------+

将每个数与其前面第一个比他大的且不能越过的点连边
然后做一遍拓扑求出字典序最小的排列即可

最后再逆回来

*/