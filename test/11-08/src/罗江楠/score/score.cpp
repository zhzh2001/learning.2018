#include <bits/stdc++.h>
#define N 110020
#define mod 1000000007
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

struct Array {
  int a[N<<1];
  inline int& operator [] (int i) {
    return a[i + N];
  }
} now, pre;

int max_value;

inline int safe_query(int x) {
  if (x < - max_value || x > max_value) {
    return 0;
  }
  return pre[x];
}

int main(int argc, char const *argv[]) {
  freopen("score.in", "r", stdin);
  freopen("score.out", "w", stdout);

  int a = read(), b = read(), k = read(), t = read();

  now[0] = 1;
  for (int i = 1; i <= t; ++ i) {
    swap(now, pre);
    int mxv = i * k;
    int rsk = 1;
    for (int j = - mxv; j <= mxv; ++ j) {
      now[j] = rsk;
      rsk = (rsk + pre[j + k + 1]) % mod;
      rsk = (rsk - pre[j - k] + mod) % mod;
    }
  }

  max_value = k * t;
  for (int i = - max_value; i <= max_value; ++ i) {
    pre[i] = (pre[i - 1] + now[i]) % mod;
  }
  int ans = 0;
  // score of xsbl
  for (int i = - max_value; i <= max_value; ++ i) {
    ans = (ans + (long long) now[i] * safe_query(a + i - b - 1) % mod) % mod;
  }
  printf("%d\n", ans);

  return 0;
}