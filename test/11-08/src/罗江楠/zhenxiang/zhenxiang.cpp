#include <bits/stdc++.h>
#define N 100020
#define mod 1000000007
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

typedef long long ll;

int to[N<<1], nxt[N<<1], head[N], cnt;
void insert(int x, int y) {
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
  to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}

ll val[N], down[N], lian[N];
int fa[N];

ll dfs1(int x, int f) {
  fa[x] = f;
  ll mx1 = 0, mx2 = 0;
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    ll v = dfs1(to[i], x);
    if (v > mx1) {
      mx2 = mx1;
      mx1 = v;
    } else if (v > mx2) {
      mx2 = v;
    }
    down[x] = max(down[x], down[to[i]]);
  }
  down[x] = max(down[x], mx1 + mx2 + val[x]);
  lian[x] = mx1 + val[x];
  return lian[x];
}

ll ans;
void dfs2(int x, ll top_up, ll top_lian) {
  ans = max(ans, top_up + down[x]);
  ll mx1 = 0, mx2 = 0, mx3 = 0;
  ll umx1 = 0, umx2 = 0;
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == fa[x]) continue;
    ll v = lian[to[i]];
    if (v > mx1) {
      mx3 = mx2;
      mx2 = mx1;
      mx1 = v;
    } else if (v > mx2) {
      mx3 = mx2;
      mx2 = v;
    } else if (v > mx3) {
      mx3 = v;
    }
    ll u = down[to[i]];
    if (u > umx1) {
      umx2 = umx1;
      umx1 = u;
    } else if (u > umx2) {
      umx2 = u;
    }
  }
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == fa[x]) continue;
    ll v = lian[to[i]];
    ll chs1 = 0, chs2 = 0;
    if (v == mx1) {
      chs1 = mx2;
      chs2 = mx3;
    } else if (v == mx2) {
      chs1 = mx1;
      chs2 = mx3;
    } else {
      chs1 = mx1;
      chs2 = mx2;
    }
    if (top_lian > chs1) {
      chs2 = chs1;
      chs1 = top_lian;
    } else if (top_lian > chs2) {
      chs2 = top_lian;
    }
    ll chsu = down[to[i]] == umx1 ? umx2 : umx1;
    dfs2(to[i], max(chs1 + chs2 + val[x], max(top_up, chsu)), chs1 + val[x]);
  }
}

int main(int argc, char const *argv[]) {
  freopen("zhenxiang.in", "r", stdin);
  freopen("zhenxiang.out", "w", stdout);

  int n = read();
  for (int i = 1; i <= n; ++ i) {
    val[i] = read();
  }
  for (int i = 1; i < n; ++ i) {
    int x = read(), y = read();
    insert(x, y);
  }

  dfs1(1, 0);
  dfs2(1, 0, 0);

  printf("%lld\n", ans);

  return 0;
}
/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
*/