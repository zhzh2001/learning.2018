#include<cstdio>
#include<cctype>
#include<iostream>
#include<string>
#include<cstring>
#include<algorithm>
#include<set>
#define re register
#define rep(x,a,b) for (re int x=(int)(a);x<=(int)(b);++x)
#define drp(x,a,b) for (re int x=(int)(a);x>=(int)(b);--x)
#define cross(i,x) for (int i=Head[x];i;i=Next[i])
#define LL long long
#define inf 0x3f3f3f3f
#define mo 1000000007
using namespace std;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int a, b, k, n;
int dp[2][500005], *x = &dp[0][250000], *y = &dp[1][250000];
inline void tadd(int &x,int y){
	x+=y;x>=mo?x-=mo:0;
}
int main(){
	freopen("score.in", "r", stdin);
	freopen("score.out", "w", stdout);
	a=read(),b=read(),k=read(),n=read();
	x[a-b]=1;
	rep(i,1,n){
		swap(x,y);
		memset(dp[i&1],0,sizeof dp[i&1]);
		int h=0,hh=0;
		int l=a-b-2*i*k,r=a-b+2*i*k;
		rep(j,0,2*k-1) tadd(hh,y[l+j]);
		rep(j,l,r){
			x[j]=(1ll*x[j-1]-h+(hh+=y[j+2*k])+mo)%mo;
			h=(1ll*h-y[j-1-2*k]+y[j]+mo)%mo;
			hh=(1ll*hh-y[j]+mo)%mo;
		}
	}
	int ans=0;
	drp(i,a-b+2*n*k,1)tadd(ans,x[i]);
	printf("%d",ans);
	return 0;
}
