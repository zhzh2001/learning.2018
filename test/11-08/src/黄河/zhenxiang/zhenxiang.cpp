#include<cstdio>
#include<cctype>
#include<iostream>
#include<string>
#include<cstring>
#include<algorithm>
#define re register
#define rep(x,a,b) for (re int x=(int)(a);x<=(int)(b);++x)
#define drp(x,a,b) for (re int x=(int)(a);x>=(int)(b);--x)
#define LL long long
#define inf 0x3f3f3f3f
using namespace std;
const int N=1010;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int n,cnt,ans,a[N],To[N<<1],Next[N<<1],Head[N];
bool vis[N];
inline void dfs(int u,int now,bool bo){
	bool flag=0;
	for (int i=Head[u];i;i=Next[i]){
		int v=To[i];
		if (vis[v]) continue;
		flag=1;dfs(v,now+a[v],0);
	}
	if (!bo){
		rep(i,1,n) if (!vis[i]) dfs(i,now+a[i],1);
	}
	if (bo&&flag) {
		if (now>ans) ans=now;
		return;
	}
	return;
}
inline void add(int u,int v){
	To[++cnt]=v;
	Next[cnt]=Head[u];
	Head[u]=cnt;
}
int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=read();
	rep(i,1,n-1){
		int x=read(),y=read();
		add(x,y),add(y,x);
	}
	ans=0;
	rep(i,1,n){
		memset(vis,0,sizeof(vis));
		vis[i]=1;
		dfs(i,a[i],0);
	}
	printf("%d",ans);
	return 0;
}
