#include<cstdio>
#include<cctype>
#include<iostream>
#include<string>
#include<cstring>
#include<algorithm>
#include<set>
#define re register
#define rep(x,a,b) for (re int x=(int)(a);x<=(int)(b);++x)
#define drp(x,a,b) for (re int x=(int)(a);x>=(int)(b);--x)
#define cross(i,x) for (int i=Head[x];i;i=Next[i])
#define LL long long
#define inf 0x3f3f3f3f
using namespace std;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N=2e5+10;
set<int> s;
int n,k,p[N],a[N],Deg[N],tr[N<<3],Head[N],To[N<<1],Next[N<<1],cnt;
inline void build(int v,int l,int r){
	tr[v]=n+1;
	if (l==r) return;
	int mid=(l+r)>>1;
	build(v<<1,l,mid);build(v<<1|1,mid+1,r);
}
inline void upd_mul(int v,int l,int r,int x,int y){
		if(l==r) {
			tr[v]=y;return;
		}
		int mid=(l+r)>>1;
		if (x<=mid) upd_mul(v<<1,l,mid,x,y);
			else upd_mul(v<<1|1,mid+1,r,x,y);
		tr[v]=(tr[v<<1]<tr[v<<1|1])?tr[v<<1]:tr[v<<1|1];
}
inline int query(int v,int l,int r,int x,int y){
	if (l==x&&r==y) return tr[v];
	int mid=(l+r)>>1;
	if (y<=mid) return query(v<<1,l,mid,x,y);
		else if (x>mid) return query(v<<1|1,mid+1,r,x,y);
		else{
			int h=query(v<<1,l,mid,x,mid),hh=query(v<<1|1,mid+1,r,mid+1,y);
			return (h<hh)?h:hh;
		}
}
inline void add(int u,int v){
	To[++cnt]=v;
	Next[cnt]=Head[u];
	Head[u]=cnt;
	Deg[v]++;
}
int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n=read(),k=read();
	rep(i,1,n) p[i]=read(),a[p[i]]=i;
	build(1,1,n);
	drp(i,n,1){
		int x;
		x=query(1,1,n,max(a[i]-k+1,1),a[i]);
		if (x<=n) {
			add(a[i],a[x]);	
		//	printf("%d %d\n",a[i],a[x]);
		}
		x=query(1,1,n,a[i],min(a[i]+k-1,n));
		if (x<=n) {
			add(a[i],a[x]);
		//	printf("%d %d\n",a[i],a[x]);
		}
		upd_mul(1,1,n,a[i],i);
	}
	rep(i,1,n) if (!Deg[i]) s.insert(i);
	rep(i,1,n) {
		int x=a[i]=*s.begin();
		s.erase(s.begin());
		cross(i,x)
			if (!(--Deg[To[i]])) s.insert(To[i]);
	}
	rep(i,1,n) p[a[i]]=i;
	rep(i,1,n) printf("%d ",p[i]);
	return 0;
}
