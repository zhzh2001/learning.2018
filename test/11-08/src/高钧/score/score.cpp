#include <bits/stdc++.h>
using namespace std;
#define int long long
inline int read(){int f=0,s=0; char c;while(!isdigit(c)){f|=(c=='-');c=getchar();}while(isdigit(c)){s=s*10+c-'0';c=getchar();}return f?-s:s;}
#define R(x) x=read()
const int BB=200105,md=1000000007;
int a,b,k,T,B,f[2][(BB<<1)+5],tong[(BB<<1)+5];
inline void Ad(int &x,int y){x+=y;x-=(x>=md)?md:0;}
signed main()
{
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	int i,j,l,t,re=0,ct=0; R(a); R(b); R(k); R(T); B=k*2*T+max(a,b)+5;
	for(i=-k*2;i<=0;i++){tong[i+B]=++ct;}ct=0; for(i=k*2;i>0;i--){tong[i+B]=++ct;}
	f[t=0][a-b+B]=1LL;
	for(i=1;i<=T;i++)
	{
		t^=1; memset(f[t],0,sizeof f[t]);
		for(j=0;j<=k*2;j++)
		{
			for(l=0;l<=B*2;l++)if(f[t^1][l])Ad(f[t][l+j],f[t^1][l]*tong[j+B]%md);
		}
		for(j=-k*2;j<0;j++)
		{
			for(l=-j;l<=B*2;l++)if(f[t^1][l])Ad(f[t][l+j],f[t^1][l]*tong[j+B]%md);
		}
	}for(i=B+1;i<=B*2;i++)Ad(re,f[t][i]); printf("%lld\n",re);
}
