#include <bits/stdc++.h>
using namespace std;
#define int long long
inline int read(){int fl=0,s=0; char c;while(!isdigit(c)){fl|=(c=='-');c=getchar();}while(isdigit(c)){s=s*10+c-'0';c=getchar();}return fl?-s:s;}
#define R(x) x=read()
inline void write(int x){if(x<0)putchar('-');if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x); putchar('\n');}
const int N=100005,M=200005;
int f[N][8],c[N],n,tot=0,Next[M],to[M],head[M];
//0:一条链//必须经过子树根节点
//1:路径 //必须经过子树根节点
//2:链和一条路径,链必须经过子树根节点
//3:路径和一条链,路径必须经过子树根节点
//4:两条路径 //没有路径经过子树根节点
//5:两条路径 //有路径经过子树根节点
//6:两条链//必须有一条经过子树根节点
inline void add(int x,int y){Next[++tot]=head[x];to[tot]=y;head[x]=tot;}
inline void dfs(int x,int la)
{
	int i,ma0=0,ma1=0,la0,la1; for(i=0;i<=6;i++)f[x][i]=c[x]; if(!head[x])return;
	for(i=head[x];i;i=Next[i])if(to[i]!=la)
	{
		dfs(to[i],x);
		
		la0=f[x][0]; la1=f[x][1];
		
		f[x][0]=max(f[x][0],f[to[i]][0]+c[x]);
		
		f[x][1]=max(f[x][1],ma0+f[to[i]][0]+c[x]);
		
		f[x][6]=max(f[x][6],f[to[i]][0]+ma0+c[x]);
		
		f[x][2]=max(f[x][2],f[to[i]][2]+c[x]);
		f[x][2]=max(f[x][2],f[to[i]][1]+ma0+c[x]);
		f[x][2]=max(f[x][2],f[to[i]][0]+ma1+c[x]);
		f[x][2]=max(f[x][2],f[to[i]][6]+ma0+c[x]);
		
		f[x][3]=max(f[x][3],f[to[i]][0]+la1);
		f[x][3]=max(f[x][3],f[to[i]][6]+la0);
		
		f[x][4]=max(f[x][4],f[to[i]][1]+ma1);
		
		f[x][5]=max(f[x][5],la1+f[to[i]][1]);
		f[x][5]=max(f[x][5],f[to[i]][2]+la0);

		ma0=max(ma0,f[to[i]][0]);
		ma1=max(ma1,f[to[i]][1]);
	}return;
}
signed main()
{
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	int i,j,x,y,re=0; n=read(); for(i=1;i<=n;i++)c[i]=read(); memset(f,0,sizeof f);
	for(i=2;i<=n;i++)
	{
		x=read(); y=read(); add(x,y); add(y,x);
	}dfs(1,0);
	for(i=1;i<=n;i++)
	{
		for(j=0;j<=6;j++)re=max(re,f[i][j]);
	}writeln(re);
}
