#include <bits/stdc++.h>
using namespace std;
#define int long long
const int N=100005,M=200005;
int n,tot=0,Next[M],to[M],head[M],q[N],bo[N],dis[N],c[N],dep[N],fa[N],arr[N];
inline void add(int x,int y){Next[++tot]=head[x];to[tot]=y;head[x]=tot;}
inline int bfs(int s)
{
	int pp=0,i,x,tou=1,wei=2; q[tou]=s; dis[s]=c[s]; memset(bo,0,sizeof bo); bo[s]=1; fa[s]=0;
	while(tou<wei)
	{
		x=q[tou++]; if(dis[x]>dis[pp])pp=x;
		for(i=head[x];i;i=Next[i])if(!bo[to[i]])
		{
			fa[to[i]]=x; dis[to[i]]=dis[x]+c[to[i]]; bo[to[i]]=1; q[wei++]=to[i];
		}
	}return pp;
}
int DD=0,Dis[N];
inline void Dfs(int x,int la)
{
	int i,ma=0; Dis[x]=c[x];
	for(i=head[x];i;i=Next[i])if((to[i]!=la)&&(!arr[to[i]]))
	{
		Dfs(to[i],x);  ma=max(ma,Dis[x]+Dis[to[i]]); Dis[x]=max(Dis[x],Dis[to[i]]+c[x]);
	}DD=max(DD,ma);
}
inline int dfs(int x,int la)
{
	int i,ma_dis=0;
	for(i=head[x];i;i=Next[i])if((to[i]!=la)&&(!arr[to[i]]))
	{
		ma_dis=max(ma_dis,dfs(to[i],x));
	}return c[x]+ma_dis;
}
signed main()
{
	freopen("ts.in","r",stdin);
//	freopen("zhenxiang.in","r",stdin);
	int i,x,y,rt1,rt2,re=0,ma; scanf("%lld",&n); for(i=1;i<=n;i++)scanf("%lld",&c[i]);
	for(i=1;i<n;i++)
	{
		scanf("%lld%lld",&x,&y); add(x,y); add(y,x);
	}rt1=bfs(1); rt2=bfs(rt1); re+=dis[rt2]; x=rt2; memset(arr,0,sizeof arr);
	printf("re=%lld\n",re);
//	Dfs(1,0); printf("DD=%lld\n",DD);
//	printf("re=%lld\n",re);
	while(fa[x]!=0){arr[x]=1; x=fa[x];} arr[rt1]=1; x=rt2;
//	printf("fa[%lld]=%lld\n",8,fa[8]);
//	while(fa[x]!=0)
//	{
//		ma=max(ma,dfs(x,0)-c[x]);
////		printf("%lld %lld\n",x,ma);
//		x=fa[x];
//	}ma=max(ma,dfs(rt1,0)-c[rt1]);
	for(i=1;i<=n;i++)if(!arr[i])Dfs(i,0);
	printf("%lld\n",re+DD);
}
