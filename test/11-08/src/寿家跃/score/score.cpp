#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=1005;
const int Mod=1e9+7;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}

bool ST;

int f[2][N*405];
int a,b,k,t;

bool ED;

int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
//	printf("%lf\n",1.0*(&ED-&ST)/1024/1024);
	a=read(),b=read(),k=read(),t=read();
	int now=0,pre;
	f[now][2*k*t]=1;
	Rep(i,1,t){
		now=i&1,pre=now^1;
		memset(f[now],0,sizeof(f[now]));
		Rep(j,-2*k*t,2*k*t) if (f[pre][j+2*k*t]){
			Add(f[now][j-2*k+2*k*t],f[pre][j+2*k*t]);
			Add(f[now][j+1+2*k*t],(-2ll*f[pre][j+2*k*t]%Mod+Mod)%Mod);
			Add(f[now][j+2*k+2+2*k*t],f[pre][j+2*k*t]);
		}
		int Sum=0,Kes=0;
		Rep(j,-2*k*t,2*k*t){
			Add(Kes,f[now][j+2*k*t]);Add(Sum,Kes);
			f[now][j+2*k*t]=Sum;
		}
//		Rep(j,-2*k*t,2*k*t) printf("%.3d ",f[now][j+2*k*t]);puts("");
	}
	int Ans=0;
	Rep(i,b-a+1,2*k*t) Add(Ans,f[now][i+2*k*t]);
	printf("%d\n",Ans);
}
