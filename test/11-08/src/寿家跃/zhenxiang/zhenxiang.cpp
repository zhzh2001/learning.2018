#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=100005;

int n,Val[N];

int lines,front[N];

struct Edge{
	int next,to;
}E[N*2];

inline void Addline(int u,int v){
	E[++lines]=(Edge){front[u],v};front[u]=lines;
}

ll Dis[N];

void Getmax(int u,ll dis,int fa){
	dis+=Val[u];
	Dis[u]=dis;
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=fa){
			Getmax(v,dis,u);
		}
	}
}

int Root,Leaf;
bool flag,Online[N];

void Ontarget(int u,int fa){
	if (u==Leaf){
		flag=true;Online[u]=true;return;
	}
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=fa){
			Ontarget(v,u);
			if (flag) break;
		}
	}
	if (flag) Online[u]=true;
}

ll f[N][2],Maxsub[N];

void Dp(int u,int fa){
	f[u][1]=f[u][0]=Val[u];
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=fa){
			Dp(v,u);
			f[u][1]=max(f[u][1],f[u][0]+f[v][0]);
			f[u][1]=max(f[u][1],f[v][1]);
			f[u][0]=max(f[u][0],f[v][0]+Val[u]);
		}
	}
}

ll Ans;

void Solve(int u,int fa){
	ll Max=0,Mex=0;
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=fa && Online[v]){
			Ans=max(Ans,f[u][1]+Maxsub[v]);
			Solve(v,u);
		}
		if ((v=E[i].to)!=fa && !Online[v]){
			if (Maxsub[v]>Max){
				Mex=Max;Max=Maxsub[v];
			}
			else if (Maxsub[v]>Mex) Mex=Maxsub[v];
		}
	}
	Ans=max(Ans,Max+Mex);
}

int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	Rep(i,1,n) Val[i]=read();
	Rep(i,1,n-1){
		int x=read(),y=read();
		Addline(x,y),Addline(y,x);
	}
	Root=1,Leaf=1;
	Getmax(1,0,1);
	Rep(i,1,n) if (Dis[i]>Dis[Root]) Root=i;
	Getmax(Root,0,Root);
	Rep(i,1,n) if (Dis[i]>Dis[Leaf]) Leaf=i;
	flag=false;
	Ontarget(Root,Root);
	Dp(Root,Root);
	Rep(i,1,n) Maxsub[i]=f[i][1];
	Ans=0;
//	最优解中有一条是直径 
	Rep(i,1,n) if (Online[i]){
		for (int j=front[i];j!=0;j=E[j].next){
			int v=E[j].to;
			if (!Online[v]) Ans=max(Ans,Dis[Leaf]+Maxsub[v]);
		}
	}
//	printf("Ans=%d\n",Ans);
//	最优解的两条链都不与直径重合 
	Dp(Leaf,Leaf);
	Solve(Root,Root);
	printf("%lld\n",Ans);
}
/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9

6
1 1 1 1 1 1
1 2
2 3
2 4
4 5
4 6
*/
