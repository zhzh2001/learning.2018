#include<cstdio>
#include<algorithm>

using namespace std;

int n,k;
int w[100000];

inline bool chk(int i,int j)
{
    if(j<i) swap(i,j);
    return (w[j]<w[i] && j-i>=k && (w[i]-w[j]==1 || w[j]-w[i]==1));
}

int main()
{
    freopen("magic.in","r",stdin);
    freopen("magic.out","w",stdout);
    scanf("%d%d",&n,&k);
    for(int i=1;i<=n;i++)
        scanf("%d",&w[i]);
    if(k!=1){
        for(int nx=1;nx<=2;nx++){
            for(int i=1;i<=n;i++){
                for(int j=1;j<=n;j++){
                    if(chk(i,j)) swap(w[i],w[j]);
                }
            }
        }
    }else if(k==1){
        sort(w+1,w+1+n);
    }
    for(int i=1;i<=n;i++) printf("%d ",w[i]);
    return 0;
}
