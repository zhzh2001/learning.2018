#include<cstdio>

using namespace std;

const int MOD=1000000007;

int a,b,k,t,ans;

void dfs(int ax,int bx,int deep)
{
	if(deep==t){
        if(ax>bx){
            ans=(ans+1)%MOD;
		}
		return;
	}
	if((bx-ax)>(t-deep)*k) return;
	for(int i=-k;i<=k;i++){
		for(int j=-k;j<=k;j++){
			dfs(ax+i,bx+j,deep+1);
		}
	}
}

int main()
{
    freopen("score.in","r",stdin);
    freopen("score.out","w",stdout);
	scanf("%d%d%d%d",&a,&b,&k,&t);
	dfs(a,b,0);
	printf("%d\n",ans);
	return 0;
}
