#include <bits/stdc++.h>
#define ll long long
#define sp prin();system("pause");
using namespace std;
const int mod=1000000007;
const int maxn=100000+10;
int dat[maxn],ans[maxn],n,k;
inline int read()
{
	int f=1,x=0;
	char c;
	c=getchar();
	while(c>'9' || c<'0'){if(c=='-')f=-1;c=getchar();}
	while(c>='0' && c<='9'){x=x*10+(c-'0');c=getchar();}
	return f*x;
}
bool cmp(void)//<=
{
	bool flag=1;
	bool flag2=1;
	for(int i=1;i<=n;i++)
	{
		if(dat[i]>ans[i])flag=false;
		if(dat[i]!=ans[i])flag2=false;
	}
	return (flag && !flag2);
}
void cop(void)
{
	for(int i=1;i<=n;i++)ans[i]=dat[i];
}
void prin(void)
{
	for(int i=1;i<=n;i++)
	{
		cout<<dat[i];
	}
}
void dfs(void)
{
	if(cmp()){cop();}
	for(int i=1;i<=n;i++)
	{
		for(int j=i+k;j<=n;j++)
		{
			if(dat[i]-dat[j]==1){
				swap(dat[i],dat[j]); 
				if(cmp())dfs();
				swap(dat[i],dat[j]);
			}
		}
	}
}

int main()
{
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n=read();k=read();
	for(int i=1;i<=n;i++)
	{
		dat[i]=read();
	}
	if(k==1)
	{
		for(int i=1;i<=n;i++){
			cout<<i;
			if(i==n)cout<<endl;
			cout<<" ";
		}
		return 0;
	}
	cop();
	dfs();
	for(int i=1;i<=n;i++)
	{
		cout<<ans[i];
	}
	return 0;
}
