#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline void kkk(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
}
const ll N=1e5+10;
ll n,a[N],f[N][4],head[N],edge;
struct edge{ll to,net;}e[N];
inline void addedge(ll x,ll y){
	e[++edge].to=y,e[edge].net=head[x],head[x]=edge;
	e[++edge].to=x,e[edge].net=head[y],head[y]=edge;
}
inline void dfs(ll x,ll fa){
	f[x][0]=f[x][1]=f[x][2]=f[x][3]=a[x];
	//初始化  
	for (ll i=head[x],y;i;i=e[i].net)
		if (e[i].to!=fa){
			y=e[i].to;
			dfs(y,x);
			f[x][3]=max(f[x][3],max(f[y][3],max(f[x][2]+f[y][0],max(f[y][1]+f[x][1],f[y][2]+f[x][0]))));
		/*
		最复杂的情况： 
			两个人直接转 
			一个人字，一条链 + 一条链 
			还有一个人 + 一个人  的情况。。。 
		*/
			f[x][2]=max(f[x][2],max(f[y][1]+f[x][0],f[y][2]+a[x]));
			f[x][1]=max(f[x][1],max(f[y][1],f[x][0]+f[y][0]));
			f[x][0]=max(f[x][0],f[y][0]+a[x]);
		/*
			f[x][0] 一条链 
			f[x][1] 一个人字 
			f[x][2] 一个人字，一条链 
			f[x][3] 两个人  
		*/
		}
}
int main(){
	kkk();
	read(n);
	for (ll i=1;i<=n;++i) read(a[i]);
	for (ll i=1,x,y;i<n;++i){
		read(x),read(y);
		addedge(x,y);
	}
	dfs(1,0);
	wr(f[1][3]);
	return 0;
}
/*
好像是求最长的双直径  
以前模拟赛的题  
树形DP吧  
ps:ai>0(贪心直接连) 
*/ 
/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
*/
