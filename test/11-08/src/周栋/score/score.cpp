#include <iostream>
#include <cstdio>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline void kkk(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
}
const ll P=1e9+7,N=1e6+10;
ll a,b,k,t,f[N],s[N],ans,T,l;
int main(){
	kkk();
	read(a),read(b),read(k),read(t);
	f[1]=1;
	for (ll i=1;i<=2*t;++i){
		T=2*k*i+1;
		for (ll j=1;j<=T;++j){
			s[j]=(s[j-1]+f[j])%P;
			f[j]=(s[j]-s[max(0,j-2*k-1)]+P)%P;
		}
	}
	l=-2*t*k-1;
	for (ll i=1;i<=T;++i) if (i+l>b-a) ans=(ans+f[i])%P;
	wr(ans);
	return 0;
}
/*
貌似好像找规律发现 DP 可前缀和 优化 。。  
时间 0.6 秒。。 
*/
