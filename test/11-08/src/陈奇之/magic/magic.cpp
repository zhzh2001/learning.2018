#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
#define gc getchar
#define pc putchar
#define Rep(i,a,b) for(int i = (a);i<=(b);++i)
#define rep(i,a,b) for(int i = (a);i<int(b);++i)
#define Dep(i,a,b) for(int i = (a);i>=(b);--i)
#define mem(x,v) memset(x,v,sizeof(x))
inline ll read(){
	ll res = 0;char c = gc();int f = 1;
	for(;!isdigit(c);c=gc()) if(c=='-') f=-1;
	for(;isdigit(c);c=gc()) res = (res << 1) + (res << 3) + (c ^ '0');
	return res * f;
}
inline void write(ll x){if(x<0) pc('-'),x=-x;if(x<=9) pc(x+'0'); else write(x/10),pc(x%10+'0');}
inline void writeln(ll x){write(x);pc('\n');}
inline void wri(ll x){write(x);pc(' ');}
/* 
p[i] = a[i]

4->1
2->2
3->3
1->4

4 2 3 1
交换相邻两个大小不超过K的
限制：


->大的放尽可能后面
先放4 
 */
const int maxn = 1e5+233;
int a[maxn],n,k,deg[maxn];
vector<int> edge[maxn];
int ans[maxn],answ[maxn];
priority_queue<int> Q;int front,rear,q[maxn];
//set<pair<int,int> > s;
//set<pair<int,int> >::iterator iter;
int T[maxn<<2];
#define lson (o<<1)
#define rson (o<<1|1)
#define mid ((l+r)>>1)
void modify(int o,int l,int r,int x,int v){
	if(l==r) {
		T[o] = v;
		return ;
	}
	if(x <= mid) modify(lson,l,mid,x,v); else
				 modify(rson,mid+1,r,x,v);
	T[o]=min(T[lson],T[rson]);
}
int query(int o,int l,int r,int x,int y){
	if(l==x&&r==y){return T[o];}
	if(y<=mid) return query(lson,l,mid,x,y); else
	if(mid+1<=x) return query(rson,mid+1,r,x,y); else
	return min(query(lson,l,mid,x,mid),query(rson,mid+1,r,mid+1,y));
}
void build(int o,int l,int r){
	T[o] = n + 1;
	if(l!=r){
		build(lson,l,mid);
		build(rson,mid+1,r);
	}
}
int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n = read(),k = read();
	Rep(i,1,n) a[read()] = i;
//	Rep(i,1,n) printf("%d ",a[i]);puts("");
	//1放在4的前面 
	//可以得到放某个数的可行位置 
	//倒着做，每次把最大的放在最后面 
	build(1,1,n);
	Dep(i,n,1){
//		iter = s.lower_bound(make_pair(a[i],0));
		int j = query(1,1,n,a[i],min(n,a[i]+k-1));
		if(j!=n+1){//最小值 
			assert(a[j]-a[i]<k);
			edge[a[j]].push_back(a[i]);
			deg[a[i]]++;
//			printf("%d->%d\n",a[i],a[j]);
		}
		j = query(1,1,n,max(1,a[i]-k+1),a[i]);
//		printf("query(%d %d) = %d\n",max(1,a[i]-k-1),a[i],j);
		if(j!=n+1){//最小值 
			if(abs(a[j]-a[i])<k){
				edge[a[j]].push_back(a[i]);
				deg[a[i]]++;
//				printf("%d->%d\n",a[i],a[j]);
			}			
		}
		modify(1,1,n,a[i],i);
/*		if(iter != s.end()){
			int j = iter -> second;//第一个大于等于它的值 
			if(abs(a[j]-a[i])<k){
				edge[a[j]].push_back(a[i]);
				deg[a[i]]++;
				printf("%d->%d\n",a[i],a[j]);
			}
		}
		iter = s.upper_bound(make_pair(a[i],0));
		if(iter != s.begin()){
			iter--;
			int j = iter -> second;
			printf("j = %d\n",j);
			if(abs(a[j]-a[i])<k){
				edge[a[j]].push_back(a[i]);
				deg[a[i]]++;
				printf("%d->%d\n",a[i],a[j]);
			}
			puts(":#");
		}
		s.insert(make_pair(a[i],i));*/
	} 
/*	Rep(i,1,n){
		Rep(j,i+1,n){
			if(abs(a[j]-a[i])<k){
				edge[a[j]].push_back(a[i]);
				deg[a[i]]++;
				printf("%d->%d\n",a[i],a[j]);
			}
		}
		//一个点向右边第一个比它小k的点连边就行了 
		//第i个数出现再哪个第a[i]个位置 
		//第i个数不能和第j个数交换意味着
		//出现位置在a[i]的那个数字必须比出现位置在a[j]的位置小 
		//第i个位置出现的数一定比第j个位置出现的数小 
	}*/
	front = rear = 0;
	Rep(i,1,n){
		if(!deg[i]){
			if(i<=n) Q.push(i); else q[rear++] = i;
		}
	}
	while(front<rear){
		int u = q[front++];
		for(unsigned w=0;w<edge[u].size();++w){
			int v=edge[u][w];
			if(--deg[v] == 0){
				if(v<=n){
					Q.push(v);
				} else{
					q[rear++] = v;
				}
			}
		}
	}
	Dep(i,n,1){
		ans[i] = Q.top();Q.pop();
		front = rear = 0;
		q[rear++] = ans[i];
		while(front < rear){
			int u = q[front++];
			for(unsigned w=0;w<edge[u].size();++w){
				int v=edge[u][w];
				if(--deg[v] == 0){
					if(v<=n){
						Q.push(v);
					} else{
						q[rear++] = v;
					}
				}
			}
		}
	}//考虑最后一个位置放什么元素
	Rep(i,1,n) answ[ans[i]] = i;
	Rep(i,1,n) wri(answ[i]);
	//对于每个元素，找到右边第一个不能到达的 
	return 0;
}











