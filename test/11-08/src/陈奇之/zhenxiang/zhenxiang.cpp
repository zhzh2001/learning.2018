#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
#define gc getchar
#define pc putchar
#define Rep(i,a,b) for(register int i = (a);i<=(b);++i)
#define rep(i,a,b) for(register int i = (a);i<int(b);++i)
#define Dep(i,a,b) for(register int i = (a);i>=(b);--i)
#define mem(x,v) memset(x,v,sizeof(x))
inline ll read(){
	ll res = 0;char c = gc();int f = 1;
	for(;!isdigit(c);c=gc()) if(c=='-') f=-1;
	for(;isdigit(c);c=gc()) res = (res << 1) + (res << 3) + (c ^ '0');
	return res * f;
}
inline void write(ll x){if(x<0) pc('-'),x=-x;if(x<=9) pc(x+'0'); else write(x/10),pc(x%10+'0');}
inline void writeln(ll x){write(x);pc('\n');}
inline void wri(ll x){write(x);pc(' ');}
const int maxn = 1e5+233;
struct Edge{
	int to,nxt;
	Edge(){}
	Edge(int to,int nxt):to(to),nxt(nxt){}
}edge[maxn*2];
int first[maxn],nume;
int dep[maxn],fa[maxn];
int ansx,ansy;ll answ,ans,res;
ll f[maxn],g[maxn];
int w[maxn],n;
void Addedge(int a,int b){
	edge[nume] = Edge(b,first[a]);
	first[a] = nume++;
}
struct node{
	ll v;int id;
	bool operator < (const node &w) const{
		return v < w.v;
	}
}mx[maxn];
void dfs(int u){
	node mx1=(node){0ll,u},mx2=(node){0ll,u};
	for(int e=first[u];~e;e=edge[e].nxt){
		int v=edge[e].to;
		if(v==fa[u]) continue;
		fa[v] = u;
		dep[v] = dep[u] + 1;
		dfs(v);
		if(mx1 < mx[v]){
			mx2 = mx1;
			mx1 = mx[v];
		} else
		if(mx2 < mx[v])
			mx2 = mx[v];
	}
	mx[u] = mx1;
	mx[u] . v += w[u];
	if(mx1.v + mx2.v + w[u] > answ){
		ansx = mx1.id;
		ansy = mx2.id;
		answ = mx1.v + mx2.v + w[u];
	}
}
vector<int> VL,VR,V;
void cover(int x,int y){
	while(x != y){
		if(dep[x] > dep[y]){
			VL.push_back(x);
			x = fa[x];
		} else{
			VR.push_back(y);
			y = fa[y];
		}
	}
	VL.push_back(x);
	reverse(VR.begin(),VR.end());
	rep(i,0,VL.size()){
		V.push_back(VL[i]);
	}
	rep(i,0,VR.size()){
		V.push_back(VR[i]);
	}
}

int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n = read();
	Rep(i,1,n) w[i] = read();
	mem(first,-1);
	rep(i,1,n){
		int a = read(),b = read();
		Addedge(a,b);
		Addedge(b,a);
	}
	ansx = -1,ansy = -1,answ = -1;
	dep[1] = 0;fa[1] = 0;
	dfs(1);
	ans += answ;
	res=0;
	cover(ansx,ansy);
	rep(i,0,V.size()){
		int u = V[i];
		f[i] = 0;
		for(int e=first[u];~e;e=edge[e].nxt){
			int v=edge[e].to;
			if(i!=0 && v==V[i-1]) continue;
			if(i!=(int)V.size()-1 && v==V[i+1]) continue;
			fa[v]=u;
			answ=0;
			dfs(v);
//			printf("answ = %lld\n",answ);
			res=max(res,answ);
//			res=max(res,f[i]+mx[v].v);
			f[i]=max(f[i],mx[v].v);
		}
	}
	Dep(i,V.size()-1,0){
		g[i] = f[i];
		if(i!=(int)V.size()-1) g[i] = max(g[i],g[i+1]-w[V[i]]);
	}
	rep(i,0,V.size()-1){
		res=max(res,f[i]+g[i+1]);
	}
	writeln(ans+res);
	return 0;
}
