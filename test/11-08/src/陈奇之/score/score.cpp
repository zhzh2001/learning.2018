#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
#define gc getchar
#define pc putchar
#define Rep(i,a,b) for(register int i = (a);i<=(b);++i)
#define rep(i,a,b) for(register int i = (a);i<(b);++i)
#define Dep(i,a,b) for(register int i = (a);i>=(b);--i)
#define mem(x,v) memset(x,v,sizeof(x))
inline ll read(){
	ll res = 0;char c = gc();int f = 1;
	for(;!isdigit(c);c=gc()) if(c=='-') f=-1;
	for(;isdigit(c);c=gc()) res = (res << 1) + (res << 3) + (c ^ '0');
	return res * f;
}
inline void write(ll x){if(x<0) pc('-'),x=-x;if(x<=9) pc(x+'0'); else write(x/10),pc(x%10+'0');}
inline void writeln(ll x){write(x);pc('\n');}
inline void wri(ll x){write(x);pc(' ');}
const int mod = 1e9+7;
void add(int &x,int v){
	x+=v;
	if(x>=mod) x-=mod;
}
int f[2][2005*105+105],g[2][2005*105+105],ans;
int a,b,k,t;
int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	a = read(),b = read(),k = read(),t = read();
	int base = k * t,limit = max(2*base+a,2*base+b);
	mem(f[0],0);
	f[0][a+base] = 1;//f[i][j]表示A获得a分有多少种情况
	Rep(i,1,t){
		mem(f[i&1],0);
		Rep(j,0,limit){
			f[i&1][j] = (f[(i-1)&1][min(limit,j+k)] - f[(i-1)&1][max(0,j-k-1)] + mod) % mod;
			if(j)add(f[i&1][j],f[i&1][j-1]);
		}
	} 
	mem(g[0],0);
	g[0][b+base] = 1;//f[i][j]表示A获得a分有多少种情况
	Rep(i,1,t){
		mem(g[i&1],0);
		Rep(j,0,limit){
			g[i&1][j] = (g[(i-1)&1][min(limit,j+k)] - g[(i-1)&1][max(0,j-k-1)] + mod) % mod;
			if(j)add(g[i&1][j],g[i&1][j-1]);
		}
	}
	Rep(i,1,limit)
		add(g[t&1][i],g[t&1][i-1]);
	Rep(i,1,limit)
		add(ans,1ll*f[t&1][i] * g[t&1][i-1]%mod);
	writeln(ans);
	return 0;
}
