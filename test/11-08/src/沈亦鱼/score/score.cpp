#include<cstdio>
#include<algorithm>
using namespace std;
int mo=1000000007,a,b,k,t,sz;
long long x,y,s,f[2][210000];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
long long pwr(long long x,int y){
	long long z=1;
	while(y){
		if(y&1)z=z*x%mo;
		x=x*x%mo;
		y>>=1;
	}
	return z;
}
int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	a=read();
	b=read();
	k=read();
	t=read();
	sz=2*k*t;
	f[0][0]=1;
	for(int i=1;i<=t;i++){
		for(int j=0,sz=i*k*2;j<=sz;j++){
			f[i&1][j]=f[(i-1)&1][min(j,sz-k*2)];
			if(j-2*k>0)f[i&1][j]-=f[(i-1)&1][max(0,j-2*k-1)];
			if(j){//printf("%d %d %d\n",j,f[i&1][j],f[i&1][j-1]);
				f[i&1][j]+=f[i&1][j-1];
				if(f[i&1][j]<0)f[i&1][j]+=mo;
				if(f[i&1][j]>mo)f[i&1][j]-=mo;
			}
			else{
				if(f[i&1][j]<0)f[i&1][j]+=mo;
				if(f[i&1][j]>mo)f[i&1][j]-=mo;
			}
		}
	}
	s=0;
//	y=0;
//	if(a-1>b){
//		for(int i=0;i<=a-b;i++)
//			y+=f[t&1][a-b-1];
//	}
	for(int i=0;i<=sz;i++){
		x=f[t&1][i];
		if(i>0)x-=f[t&1][i-1];
		if(x<mo)x+=mo;
		if(i+a>b){
			s=s+x*f[t&1][a+i-b-1]%mo;
			if(s>mo)s-=mo;
		}//printf("%d %d\n",i,f[1][1]);
	}
	printf("%lld",s);
	return 0;
}/*
*/
