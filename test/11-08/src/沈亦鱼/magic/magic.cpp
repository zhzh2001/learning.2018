#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,x,y,a[110000],b[110000];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		b[a[i]]=i;
	}
	for(int i=1;i<=n;i++)
		while(a[i]>1){
			x=b[a[i]-1];//printf("%d %d\n",i,x);
			if(x-i<m){
				y=b[a[i]-2];//printf("%d\n",y);
				if(y-x<m)break;
				else{
					swap(a[x],a[y]);
					b[a[x]]=x;
					b[a[y]]=y;
					swap(a[i],a[y]);
					b[a[i]]=i;
					b[a[y]]=y;
				}
			}
			else{
				swap(a[i],a[x]);
				b[a[i]]=i;
				b[a[x]]=x;
			}//printf("%d : %d\n",i,a[i]);
		}
	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
	return 0;
}
