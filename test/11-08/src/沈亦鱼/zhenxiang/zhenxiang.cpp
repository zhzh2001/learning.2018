#include<cstdio>
#include<algorithm>
using namespace std;
int n,u,v,tot,p[210000],ne[210000],he[110000],pf[110000][5],pd[110000][5];
long long s,a[110000],f[110000][5],d[110000][5];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
void adg(int u,int v){
	tot++;
	p[tot]=v;
	ne[tot]=he[u];
	he[u]=tot;
}
void dfs(int k,int fa){
/*
1:一条线 最大、次大、第三大
2:一条链 最大、次大、第三大
3:一条线+一条链 最大、次大
*/
	f[k][1]=a[k];
	f[k][3]=f[k][2]=d[k][3]=d[k][2]=d[k][1]=0;
	long long tth=0,ptth=0,dtth=0,pdtth=0;
	for(int i=he[k];i;i=ne[i])
		if(p[i]!=fa){
			dfs(p[i],k);
			if(f[p[i]][1]+a[k]>f[k][1]){
				tth=d[k][1];
				ptth=pd[k][1];
				d[k][1]=f[k][1];
				pd[k][1]=pf[k][1];
				f[k][1]=f[p[i]][1]+a[k];
				pf[k][1]=p[i];
			}
			else if(f[p[i]][1]+a[k]>d[k][1]){
				tth=d[k][1];
				ptth=pd[k][1];
				d[k][1]=f[p[i]][1]+a[k];
				pd[k][1]=p[i];
			}
			else if(f[p[i]][1]+a[k]>tth){
				tth=f[p[i]][1]+a[k];
				ptth=p[i];
			}
/*
			if(d[p[i]][1]+a[k]>f[k][1]){
				tth=d[k][1];
				ptth=pd[k][1];
				d[k][1]=f[k][1];
				pd[k][1]=pf[k][1];
				f[k][1]=d[p[i]][1]+a[k];
				pf[k][1]=p[i];
			}
			else if(d[p[i]][1]+a[k]>d[k][1]){
				tth=d[k][1];
				ptth=pd[k][1];
				d[k][1]=d[p[i]][1]+a[k];
				pd[k][1]=p[i];
			}
			else if(d[p[i]][1]+a[k]>tth){
				tth=d[p[i]][1]+a[k];
				ptth=p[i];
			}
*/
			if(f[p[i]][2]>f[k][2]){
				dtth=d[k][2];
				pdtth=pd[k][2];
				d[k][2]=f[k][2];
				pd[k][2]=pf[k][2];
				f[k][2]=f[p[i]][2];
				pf[k][2]=p[i];
			}
			else if(f[p[i]][2]>d[k][2]){
				dtth=d[k][2];
				pdtth=pd[k][2];
				d[k][2]=f[p[i]][2];
				pd[k][2]=p[i];
			}
			else if(f[p[i]][2]>dtth){
				dtth=f[p[i]][2];
				pdtth=p[i];
			}
/*
			if(d[p[i]][2]>f[k][2]){
				dtth=d[k][2];
				pdtth=pd[k][2];
				d[k][2]=f[k][2];
				pd[k][2]=pf[k][2];
				f[k][2]=d[p[i]][2];
				pf[k][2]=p[i];
			}
			else if(d[p[i]][2]>d[k][2]){
				dtth=d[k][2];
				pdtth=pd[k][2];
				d[k][2]=d[p[i]][2];
				pd[k][2]=p[i];
			}
			else if(d[p[i]][2]>dtth){
				dtth=d[p[i]][2];
				pdtth=p[i];
			}
*/
			if(f[p[i]][3]+a[k]>f[k][3]){
				d[k][3]=f[k][3];
				pd[k][3]=pf[k][3];
				f[k][3]=f[p[i]][3]+a[k];
				pf[k][3]=p[i];
			}
			else if(f[p[i]][3]+a[k]>d[k][3]){
				d[k][3]=f[p[i]][3]+a[k];
				pd[k][3]=p[i];
			}//if(k==1)printf("!%d %d %d\n",f[k][1],d[k][1],f[p[i]][1]);
		}
//	if(f[k][3]<d[k][3]||d[k][2]<dtth)puts("1");
//	printf("%lld ",s);
	if(pf[k][3]==pf[k][1]){
		s=max(s,f[k][3]+d[k][1]-a[k]);//printf("%lld ",d[k][1]);
		s=max(s,d[k][3]+f[k][1]-a[k]);//printf("%lld ",s);
	}
	else s=max(s,f[k][3]+d[k][1]-a[k]);
//	printf("%lld\n",s);
//	printf("%d\n",f[k][3]);
//	printf("%lld %lld %lld %lld %lld %lld\n",f[k][1],d[k][1],tth,f[k][2],d[k][2],dtth);
	if(pf[k][1]!=pf[k][2]&&pd[k][1]!=pf[k][2])s=max(s,f[k][1]+d[k][1]+f[k][2]-a[k]);//if(k==1)printf("%d\n",s);
	if(pf[k][1]!=pd[k][2]&&pd[k][1]!=pd[k][2])s=max(s,f[k][1]+d[k][1]+d[k][2]-a[k]);//if(k==1)printf("%d\n",s);
	if(pf[k][1]!=pdtth&&pd[k][1]!=pdtth)s=max(s,f[k][1]+d[k][1]+dtth-a[k]);//if(k==1)printf("%d\n",s);
	if(pf[k][1]!=pf[k][2]&&ptth!=pf[k][2])s=max(s,f[k][1]+tth+f[k][2]-a[k]);//if(k==1)printf("%d\n",s);
	if(pf[k][1]!=pd[k][2]&&ptth!=pd[k][2])s=max(s,f[k][1]+tth+d[k][2]-a[k]);//printf("%lld\n",s);
	if(pf[k][1]!=pdtth&&ptth!=pdtth)s=max(s,f[k][1]+tth+dtth-a[k]);//printf("%lld\n",s);
//	if(k==1)printf("%d %d %d\n",f[k][1],tth,pd[k][2]);
	if(pd[k][1]!=pf[k][2]&&ptth!=pf[k][2])s=max(s,d[k][1]+tth+f[k][2]-a[k]);//printf("%lld\n",s);
	if(pd[k][1]!=pd[k][2]&&ptth!=pd[k][2])s=max(s,d[k][1]+tth+d[k][2]-a[k]);//printf("%lld\n",s);
	if(pd[k][1]!=pdtth&&ptth!=pdtth)s=max(s,d[k][1]+tth+dtth-a[k]);//printf("%lld\n",s);
	if(pf[k][1]==pf[k][2]){
		f[k][3]=max(f[k][3],d[k][1]+f[k][2]);
		f[k][3]=max(f[k][3],f[k][1]+d[k][2]);
	}
	else f[k][3]=max(f[k][3],f[k][1]+f[k][2]);
	f[k][2]=max(f[k][2],f[k][1]+d[k][1]-a[k]);
	f[k][2]=max(f[k][2],f[k][1]);
	f[k][3]=max(f[k][3],f[k][2]);
	s=max(s,f[k][3]);
//	printf("%d %d %d %d\n",k,f[k][1],f[k][2],f[k][3]);
//	printf("%d %lld\n",k,s);
	return;
}
void write(long long x){
	if(x>9)write(x/10);
	putchar(x%10+48);
}
int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<n;i++){
		u=read();
		v=read();
		adg(u,v);
		adg(v,u);
	}
	s=0;
	dfs(1,0);
	printf("%lld",s);
	return 0;
}/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
*/
