#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=210005,mod=1000000007;
int a,b,k,t;
ll dp[2][N],f[2][N],sum[2][N],ans;
#define min(x,y) ((x)<(y)?(x):(y))
int main(){
	freopen("score.in","r",stdin); freopen("score.out","w",stdout);
	cin>>a>>b>>k>>t;
	int T=2*k+1;
	dp[0][0]=sum[0][0]=1;
	for(int i=1;i<=t;i++){
		int dq=i&1,dd=dq^1,S=(i-1)*k*2;
		for(int j=0;j<=i*2*k;j++){
			int L=j-T,R=j+T-1,jj=min(j,S);
			dp[dq][j]=((f[dd][jj]-(L<0?(f[dd][min(-L-1,S)]-f[dd][0]):f[dd][min(L,S)]))+
			(sum[dd][jj]-(L<0?-(sum[dd][min(-L-1,S)]-sum[dd][0]):sum[dd][min(L,S)]))*(T-j)-
			(f[dd][min(R,S)]-f[dd][jj])+(sum[dd][min(R,S)]-sum[dd][jj])*(T+j))%mod;
			sum[dq][j]=((j?sum[dq][j-1]:0)+dp[dq][j])%mod;
			f[dq][j]=((j?f[dq][j-1]:0)+dp[dq][j]*j)%mod;
			//if(dp[dq][j])cout<<i<<" "<<j<<" "<<dp[dq][j]<<" "<<(f[dd][jj]-(L<0?(f[dd][min(-L-1,S)]-f[dd][0]):f[dd][min(L,S)]))<<endl;
		}
	}
	int S=t*k*2;
	if(a<=b)ans=sum[t&1][t*2*k]-sum[t&1][min(b-a,S)]; else ans=sum[t&1][t*2*k]+(sum[t&1][min(a-b-1,S)]-sum[t&1][0]);
	cout<<(ans%mod+mod)%mod<<endl;
}
/*
a>b
*/
