#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
int n,k;
set<vector<int> > s;
void dfs(vector<int> a){
	if(s.count(a))return; else s.insert(a);
	for(int i=0;i<n;i++){
		for(int j=i+k;j<n;j++)if(abs(a[i]-a[j])==1){
			swap(a[i],a[j]);
			dfs(a);
			swap(a[i],a[j]);
		}
	}
}
int main(){
	cin>>n>>k;
	vector<int> a;
	a.resize(n);
	for(int i=0;i<n;i++)a[i]=read();
	dfs(a);
	vector<int> mn=*s.begin();
	for(auto i:mn)cout<<i<<" " ;
}
