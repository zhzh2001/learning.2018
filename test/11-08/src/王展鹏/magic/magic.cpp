#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=100005;
int n,k,rd[N],ans[N],pos[N],tree[N<<2];
priority_queue<int> q;
vector<int> v[N];
void insert(int l,int r,int pos,int de,int nod){
	if(l==r){
		tree[nod]=de; return;
	}
	int mid=(l+r)>>1;
	if(pos<=mid)insert(l,mid,pos,de,nod<<1);
	else insert(mid+1,r,pos,de,nod<<1|1);
	tree[nod]=max(tree[nod<<1],tree[nod<<1|1]);
}
int ask(int l,int r,int i,int j,int nod){
	if(i>j)return 0;
	if(l==i&&r==j)return tree[nod];
	int mid=(l+r)>>1;
	if(j<=mid)return ask(l,mid,i,j,nod<<1);
	else if(i>mid)return ask(mid+1,r,i,j,nod<<1|1);
	else return max(ask(l,mid,i,mid,nod<<1),ask(mid+1,r,mid+1,j,nod<<1|1));
}
int main(){
	freopen("magic.in","r",stdin); freopen("magic.out","w",stdout);
	n=read(); k=read();
	for(int i=1;i<=n;i++)pos[read()]=i;
	for(int i=1;i<=n;i++){
		int x=ask(1,n,max(pos[i]-k+1,1),pos[i]-1,1);
		if(x){v[pos[i]].push_back(pos[x]); rd[pos[x]]++;}
		x=ask(1,n,pos[i]+1,min(pos[i]+k-1,n),1);
		if(x){v[pos[i]].push_back(pos[x]); rd[pos[x]]++;}
		insert(1,n,pos[i],i,1);
	}
	for(int i=1;i<=n;i++)if(rd[i]==0)q.push(i);
	for(int i=n;i;i--){
		int t=q.top();
		q.pop();
		ans[t]=i; 
		for(unsigned j=0;j<v[t].size();j++){
			if(--rd[v[t][j]]==0){
				q.push(v[t][j]);
			}
		}
	}
	for(int i=1;i<=n;i++){
		write(ans[i]); putchar(' ');
	}
}
/*
每次找一个最大位置使得可以和当前所有数换 
to1 to2 to3 to4 to5 to6
 4 6
x->y 连边当且仅当 x>y并且abs(ax-ay)<k 
发现只需要连O(n)条边
线段树+堆支持即可 
//为出题人点赞 
8 3
4 5 7 8 3 1 2 6

*/ 
