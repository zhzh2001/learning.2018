#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=100005;
map<int,ll> M[N],f[N];
multiset<ll> s[N],S[N];
vector<int> v[N];
int n,ls[N];
ll ans,a[N];
ll get(int p){
	set<ll>::iterator it=s[p].end();
	it--; ll x=*it; it--;
	return *it+x;
}
void dfs(int p,int fa){
	if(M[p].count(fa))return;
	if(!ls[p]){
		for(unsigned o=0;o<v[p].size();o++)if(v[p][o]!=fa){
			int i=v[p][o];
			dfs(i,p);
			s[p].insert(f[i][p]);
			S[p].insert(M[i][p]);
		}
	}else if(fa!=ls[p]){
		s[p].erase(s[p].find(f[fa][p]));
		S[p].erase(S[p].find(M[fa][p]));
		dfs(ls[p],p);
		s[p].insert(f[ls[p]][p]);
		S[p].insert(M[ls[p]][p]);
	}
	ls[p]=fa;
	f[p][fa]=s[p].size()?*s[p].rbegin()+a[p]:a[p];
	M[p][fa]=max(S[p].size()?*S[p].rbegin():0,a[p]+(s[p].size()==0?0:(s[p].size()==1?*s[p].rbegin()+a[p]:get(p))));
}
int main(){
	freopen("zhenxiang.in","r",stdin); freopen("zhenxinag.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++){
		int s=read(),t=read();
		v[s].push_back(t); v[t].push_back(s);
	}
	for(int i=1;i<n;i++){
		for(unsigned o=0;o<v[i].size();o++)if(i<v[i][o]){
			int j=v[i][o];
			dfs(i,j); dfs(j,i);
			ans=max(ans,M[i][j]+M[j][i]);
		}
	}
	cout<<ans<<endl;
}
/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9

*/
