#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
    ll x = 0,f = 1;char ch = getchar();
    while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
    while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
    return x*f;
}

void write(int x){
    if(x > 9) write(x/10);
    putchar('0'+x%10);
}
void wri(int x){
    write(x); putchar(' ');
}

const int N = 100005;
int n,k;
int a[N],b[N];
int Mi[N<<2];
int head[N],nxt[N*2],to[N*2],cnt;
int ru[N],m;

inline void insert(int x,int y){
    to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}

int query(int num,int l,int r,int x,int y){
    if(x <= l && r <= y) return Mi[num];
    int mid = (l+r)>>1,ans = 1e9;
    if(x <= mid) ans = query(num<<1,l,mid,x,y);
    if(y > mid) ans = min(ans,query(num<<1|1,mid+1,r,x,y));
    return ans;
}

void change(int num,int l,int r,int x,int y){
    Mi[num] = min(Mi[num],y);
    if(l == r) return;
    int mid = (l+r)>>1;
    if(x <= mid) change(num<<1,l,mid,x,y);
    else change(num<<1|1,mid+1,r,x,y);
}

void toposort(){
    priority_queue<int,vector<int>,greater<int> > Q;
    for(int i = 1;i <= n;++i)
        if(ru[i] == 0) Q.push(i);
    while(!Q.empty()){
        int x = Q.top(); Q.pop();
        b[x] = ++m;
        for(int i = head[x];i;i = nxt[i]){
            --ru[to[i]];
            if(ru[to[i]] == 0) Q.push(to[i]);
        }
    }
}

signed main(){
    freopen("magic.in","r",stdin);
    freopen("magic.out","w",stdout);
    n = read(); k = read();
    for(int i = 1;i <= n;++i){ int x = read(); a[x] = i; }
    memset(Mi,0x3f,sizeof Mi);
    for(int i = n;i >= 1;--i){
        int x;
        if(a[i] < n){
            x = query(1,1,n,a[i]+1,min(a[i]+k-1,n));
            if(x >= 1 && x <= n) insert(a[i],a[x]),++ru[a[x]];
        }
        if(a[i] > 1){
            x = query(1,1,n,max(a[i]-k+1,1),a[i]-1);
            if(x >= 1 && x <= n) insert(a[i],a[x]),++ru[a[x]];
        }
        change(1,1,n,a[i],i);
    }
    toposort();
    for(int i = 1;i <= n;++i) wri(b[i]); puts("");
    return 0;
}
