#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 1005,M = 105;
const int mod = 1000000007,pos = 100005;
int f[2*N*M];
int sum[2*N*M];
int a,b,k,t;
int ans;

inline int num(int x){
	return x+pos;
}

signed main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	a = read(); b = read(); k = read(); t = read();
	for(int i = 0;i <= t;++i){
		if(i == 0) f[num(0)] = 1;
		else{
			for(int j = -t*k;j <= t*k;++j){
				int l = max(j-k,-t*k),r = min(j+k,t*k);
				f[num(j)] = (sum[num(r)]-sum[num(l-1)])%mod;
			}
		}
		for(int j = -t*k;j <= t*k;++j)
			sum[num(j)] = (sum[num(j-1)]+f[num(j)])%mod;
	}
	a -= b;
	for(int i = -t*k;i <= t*k;++i)
		if(i+a-1 >= -t*k)
			ans = (ans+1LL*f[num(i)]*sum[num(min(t*k,i+a-1))]%mod)%mod;
	printf("%d\n",(ans%mod+mod)%mod);
	return 0;
}
