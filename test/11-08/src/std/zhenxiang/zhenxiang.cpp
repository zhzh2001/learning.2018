#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int head[N],nxt[N*2],to[N*2],cnt; 
int a[N];
int n;
ll Mx1dep[N],Mx2dep[N],Mx3dep[N];
ll Mxlian[N],Mx1sonlian[N],Mx2sonlian[N];
ll Mxfalian[N],Mxfadep[N];
ll ans;


inline void insert(int x,int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}

void dfs1(int x,int f){
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != f){
			int y = to[i];
			dfs1(y,x);
			if(Mx1dep[y]+a[y] > Mx1dep[x]){
				Mx3dep[x] = Mx2dep[x];
				Mx2dep[x] = Mx1dep[x];
				Mx1dep[x] = Mx1dep[y]+a[y];
			}else if(Mx1dep[y]+a[y] > Mx2dep[x]){
				Mx3dep[x] = Mx2dep[x];
				Mx2dep[x] = Mx1dep[y]+a[y];
			}else if(Mx1dep[y]+a[y] > Mx3dep[x]){
				Mx3dep[x] = Mx1dep[y]+a[y];
			}
			Mxlian[x] = max(Mxlian[x],Mxlian[y]);
			if(Mxlian[y] > Mx1sonlian[x]){
				Mx2sonlian[x] = Mx1sonlian[x];
				Mx1sonlian[x] = Mxlian[y];
			}else if(Mxlian[y] > Mx2sonlian[x])
				Mx2sonlian[x] = Mxlian[y];
		}
	Mxlian[x] = max(Mx1dep[x]+Mx2dep[x]+a[x],Mxlian[x]);
}

void dfs2(int x,int f){
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != f){
			int y = to[i];
			ll sum1 = 0;
			ll MMX1,MMX2;
			if(Mx1dep[y]+a[y] == Mx1dep[x]){
				MMX1 = Mx2dep[x];
				MMX2 = Mx3dep[x];				
			}
			else{
				MMX1 = Mx1dep[x];
				if(Mx1dep[y]+a[y] == Mx2dep[x]) MMX2 = Mx3dep[x];
				else MMX2 = Mx2dep[x];
			}
			sum1 = max(MMX1+MMX2+a[x],Mxfalian[x]);
			sum1 = max(sum1,MMX1+Mxfadep[x]);
			if(Mxlian[y] == Mx1sonlian[x]) sum1 = max(sum1,Mx2sonlian[x]);
			else sum1 = max(sum1,Mx1sonlian[x]);
			ans = max(sum1+Mxlian[y],ans);
			Mxfadep[y] = max(MMX1+a[x]+a[y],Mxfadep[x]+a[y]);
			Mxfalian[y] = max(sum1,Mxfadep[y]);
			dfs2(y,x);
		}
}

signed main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;++i) a[i] = read();
	for(int i = 1;i < n;++i){
		int x = read(),y = read();
		insert(x,y); insert(y,x);
	}
	dfs1(1,1);
	Mxfadep[1] = a[1];
	Mxfalian[1] = a[1];
	dfs2(1,1);
	printf("%lld\n", ans);
	return 0;
}
