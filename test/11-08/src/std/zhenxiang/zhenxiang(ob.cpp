#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int nedge=0,p[2*N],nex[2*N],head[2*N];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
int a[N],f[N][4],n;
inline void dfs(int x,int fa){
	int ma[3]={0};
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		dfs(p[k],x);
		f[x][3]=max(f[x][3],max(f[p[k]][0]+ma[2],f[p[k]][2]+ma[0])+a[x]);
		f[x][3]=max(f[x][3],f[x][1]+f[p[k]][1]);
		f[x][3]=max(f[x][3],f[x][2]+f[p[k]][0]);//相当于两个0和一个1 
		f[x][3]=max(f[x][3],f[p[k]][3]);
		f[x][2]=max(f[x][2],max(f[p[k]][0]+ma[1],f[p[k]][1]+ma[0])+a[x]);
		f[x][2]=max(f[x][2],f[p[k]][2]+a[x]);
		f[x][1]=max(f[x][1],ma[0]+f[p[k]][0]+a[x]);
		f[x][1]=max(f[x][1],f[p[k]][1]);
		f[x][0]=max(f[x][0],f[p[k]][0]+a[x]);
		if(f[p[k]][0]>ma[0])ma[0]=f[p[k]][0];
		if(f[p[k]][1]>ma[1])ma[1]=f[p[k]][1];
		if(f[p[k]][2]>ma[2])ma[2]=f[p[k]][2];
	}
	f[x][0]=max(f[x][0],a[x]);
	f[x][1]=max(f[x][1],a[x]);
	f[x][2]=max(f[x][2],ma[1]+a[x]);
	f[x][3]=max(f[x][3],f[x][2]);
}
signed main()
{
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
	}
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addedge(x,y);addedge(y,x);
	}
	dfs(1,0);
	writeln(f[1][3]);
	return 0;
}
/*
f[x][0]:从当前子树的根向下能到达的最长链
f[x][1]:当前子树内的一条最长链
f[x][2]:从当前子树的根向下能到达的链和子树内与其不相交的一条最长链的最大和
f[x][3]:当前子树内的两条不相交的链的最大和
*/
