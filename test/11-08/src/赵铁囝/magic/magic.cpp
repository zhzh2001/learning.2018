#include <bits/stdc++.h>

#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int n,k,a[Max],b[Max];

int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n=read();k=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		b[a[i]]=i;
	}
	if(k==1){
		sort(a+1,a+n+1);
		for(int i=1;i<=n;i++){
			write(a[i]);putchar(' ');
		}
		return 0;
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if(b[a[j]-1]){
//				cout<<a[j]<<" "<<j<<" "<<b[a[j]-1]<<endl;
				if(b[a[j]-1]-j>=k){
					swap(a[j],a[b[a[j]-1]]);
					swap(b[a[j]+1],b[a[j]]);
				}
			}
			/*for(int k=1;k<=n;k++){
				write(a[k]);putchar(' ');
			}
			cout<<endl;*/
		}
	}
	for(int i=1;i<=n;i++){
		write(a[i]);putchar(' ');
	}
	return 0;
}
