#include <bits/stdc++.h>

#define ll long long

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

const int wzp=1e9+7;

int a,b,k,t,ans,f[2][210000],g[2][210000],sum[2][2][210000];

int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	a=read();b=read();k=read();t=read();
	f[0][a]=1;g[0][b]=1;
	sum[0][0][0]=f[0][0];
	sum[1][0][0]=g[0][0];
	for(int i=1;i<=t*k*2+1000;i++){
		sum[0][0][i]=sum[0][0][i-1]+f[0][i];
		sum[1][0][i]=sum[1][0][i-1]+g[0][i];
	}
	for(int i=1,p=1;i<=t;i++,p^=1){
		for(int j=0;j<=t*k*2+1000;j++){
			/*f[p][j]=0;
			g[p][j]=0;
			for(int q=0;q<=min(k*2,j);q++){
				f[p][j]=(f[p][j]+f[p^1][j-q])%wzp;
				g[p][j]=(g[p][j]+g[p^1][j-q])%wzp;
			}*/
			if(j<=k*2){
				f[p][j]=sum[0][p^1][j];
				g[p][j]=sum[1][p^1][j];
			}else{
				f[p][j]=(sum[0][p^1][j]-sum[0][p^1][j-k*2-1]+wzp)%wzp;
				g[p][j]=(sum[1][p^1][j]-sum[1][p^1][j-k*2-1]+wzp)%wzp;
			}
			if(j>0){
				sum[0][p][j]=(sum[0][p][j-1]+f[p][j])%wzp;
				sum[1][p][j]=(sum[1][p][j-1]+g[p][j])%wzp;
			}else{
				sum[0][p][j]=f[p][j];
				sum[1][p][j]=g[p][j];
			}
//			cout<<g[p][j]<<" "<<endl;
		}
	}
//	for(int i=0;i<=a+t*k*2;i++)cout<<f[t&1][i]<<" ";cout<<endl;
//	for(int i=0;i<=b+t*k*2;i++)cout<<g[t&1][i]<<" ";cout<<endl;
	for(int i=1;i<=t*k*2+1000;i++){
//		ans=(ans+(ll)sum[i-1]*f[t&1][i]%wzp)%wzp;
		ans=(ans+(ll)sum[1][t&1][i-1]*f[t&1][i]%wzp)%wzp;
	}
	writeln(ans);
	return 0;
}
