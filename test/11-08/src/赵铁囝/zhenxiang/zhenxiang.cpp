#include <bits/stdc++.h>

#define ll long long
#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

struct Edge{
	int v,to;
}e[Max*2];
//0表示一条可以连的链，1表示一条不能连的，2表示一条可以连的和一条不能连的，3表示两条不能连的 
int n,l,r,size,a[Max],head[Max];
ll ans,f[Max][4];

inline void add(int u,int v){
	e[++size].v=v;e[size].to=head[u];head[u]=size;
}

inline void dfs(int u,int fa){
	ll b[4],c[4],d[4];
	int num1[4],num2[4];//b表示最大，c表示第二大，d表示第三大，num1表示最大的编号，num2表示次大的编号 
	for(int i=0;i<4;i++)b[i]=c[i]=d[i]=0;
	f[u][0]=a[u];
	for(int i=head[u];i;i=e[i].to){
		int v=e[i].v;
		if(v==fa)continue;
		dfs(v,u);
		for(int j=0;j<4;j++){
			if(f[v][j]>=b[j]){
				d[j]=c[j];
				c[j]=b[j];
				num2[j]=num1[j];
				b[j]=f[v][j];
				num1[j]=v;
			}else{
				if(f[v][j]>=c[j]){
					d[j]=c[j];
					c[j]=f[v][j];
					num2[j]=v;
				}else{
					if(f[v][j]>=d[j]){
						d[j]=f[v][j];
					}
				}
			}
		}
	}
	f[u][0]=max(f[u][0],b[0]+a[u]);
	f[u][1]=max(b[0]+c[0]+a[u],b[1]);
	f[u][2]=b[2]+a[u];
	if(num1[0]!=num1[1]){
		f[u][2]=max(f[u][2],b[0]+b[1]+a[u]);
	}else{
		f[u][2]=max(f[u][2],max(b[0]+c[1]+a[u],b[1]+c[0]+a[u]));
	}
	f[u][3]=max(b[1]+c[1],b[3]);
	if(num1[0]!=num1[2]){
		f[u][3]=max(f[u][3],b[0]+b[2]+a[u]);
	}
	if(num1[1]==num1[0]||num1[1]==num2[0]){
		if(num2[1]==num1[0]||num2[1]==num2[0]){
			f[u][3]=max(f[u][3],b[0]+c[0]+d[1]+a[u]);
		}else{
			f[u][3]=max(f[u][3],b[0]+c[0]+c[1]+a[u]);
		}
	}else{
		f[u][3]=max(f[u][3],b[0]+c[0]+b[1]+a[u]);
		if(num1[1]==num1[0]){
			f[u][3]=max(f[u][3],d[0]+c[0]+b[1]+a[u]);
		}else{
			f[u][3]=max(f[u][3],b[0]+d[0]+b[1]+a[u]);
		}
	}
	ans=max(ans,max(max(f[u][0],f[u][1]),max(f[u][2],f[u][3])));
	return;
}

int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++){
		l=read();r=read();
		add(l,r);
		add(r,l);
	}
	dfs(1,1);
	writeln(ans);
	return 0;
}
