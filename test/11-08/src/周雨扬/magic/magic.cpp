#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
const int N=100005;
const int M=265000;
struct node{
	int at;
	bool operator <(const node &a)const{
		return !(at<a.at);
	}
};
priority_queue<node> Q;
int n,k,a[N],pos[N],ans[N];
int val[N],pre[N];
namespace T2{
    int t[N];
    void change(int p){
	    for (;p<=n;p+=p&(-p)) t[p]++;
	}
    int ask(int p){
	    int ans=0;
	    for (;p;p-=p&(-p)) ans+=t[p];
	    return ans;
	}
}
namespace T3{
    int mn[M],tg[M];
    void build(int k,int l,int r){
	    tg[k]=0;
	    if (l==r){
		    mn[k]=pre[l];
		    return;
		}
	    int mid=(l+r)/2;
	    build(k*2,l,mid);
	    build(k*2+1,mid+1,r);
	    mn[k]=min(mn[k*2],mn[k*2+1]);
	}
    void pushdown(int k){
	    if (!tg[k]) return;
	    tg[k*2]+=tg[k]; tg[k*2+1]+=tg[k];
	    mn[k*2]-=tg[k]; mn[k*2+1]-=tg[k];
	    tg[k]=0;
	}
    void change(int k,int l,int r,int x,int y,int v){
	    if (l==x&&r==y){
		    if (mn[k]>v){
			    mn[k]-=v,tg[k]+=v;
			    return;
			}
		    else if (l==r){
			    Q.push((node){l});
			    mn[k]=1e9;
			    return;
			}
		}
		pushdown(k);
		int mid=(l+r)/2;
		if (y<=mid) change(k*2,l,mid,x,y,v);
		else if (x>mid) change(k*2+1,mid+1,r,x,y,v);
		else{
			change(k*2,l,mid,x,mid,v);
			change(k*2+1,mid+1,r,mid+1,y,v);
		}
		mn[k]=min(mn[k*2],mn[k*2+1]);
	}
}
int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
    scanf("%d%d",&n,&k); k--;
    For(i,1,n){
    	int x; scanf("%d",&x);
	    a[x]=i;
	}
    For(i,1,n){
	    pre[a[i]]=T2::ask(min(n,a[i]+k))+1;
	    pre[a[i]]-=T2::ask(max(1,a[i]-k)-1);
	    T2::change(a[i]);
	}
    T3::build(1,1,n);
    T3::change(1,1,n,1,n,1);
    while (!Q.empty()){
	    int x=Q.top().at; Q.pop();
	    ans[++*ans]=x;
	    T3::change(1,1,n,max(1,x-k),min(n,x+k),1);
	}
    For(i,1,n) printf("%d ",ans[i]);
}
/*
4 2
4 2 3 1
8 3
4 5 7 8 3 1 2 6
*/
