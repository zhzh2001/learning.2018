#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
const int N=1000005;
const int mo=1000000007;
int a,b,k,t,ans;
int f[N],g[N];
int main(){
    freopen("score.in","r",stdin);
    freopen("score.out","w",stdout);
    scanf("%d%d%d%d",&a,&b,&k,&t);
    f[0]=1;
    For(i,1,t){
        For(j,1,2*i*k) f[j]=(f[j-1]+f[j])%mo;
        Rep(j,2*i*k,2*k+1) f[j]=(f[j]+mo-f[j-2*k-1])%mo;
    }
    Rep(i,2*k*t,0) g[i]=f[i];
    Rep(i,2*k*t,0) f[i]=(f[i]+f[i+1])%mo;
    For(i,0,2*k*t)
        ans=(ans+1ll*g[i]*f[max(i+b-a+1,0)])%mo;
    printf("%d",ans);
}
