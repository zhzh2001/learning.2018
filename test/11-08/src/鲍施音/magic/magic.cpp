#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

int n,k,a[100000+5];

int main() 
{
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	scanf("%d%d",&n,&k);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	int flag=0;
	do
	{
		flag=0;
		for(int i=1;i<=n-k;i++)
			for(int j=i+k;j<=n;j++)
			{
				if((abs(a[i]-a[j]))==1 && a[j]<a[i])
				{
					swap(a[i],a[j]);
					flag=1;
				}
			}
	}while(flag==1);
	for(int i=1;i<=n;i++)
	    printf("%d ",a[i]);
	return 0;
}
