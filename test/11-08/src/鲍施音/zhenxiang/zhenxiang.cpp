#include <iostream>
#include <cstdio>

using namespace std;

const int INF=0x3f3f3f3f;
int n,a[100000+5],son[3000+5][3000+5],ans;
bool used[100000+5];
long long sum;

void dfs(int x)
{
	if(son[x][0]==0)
	    return ;
	for(int i=1;i<=son[x][0];i++)
	{
		ans+=a[son[x][i]];
		used[son[x][i]]=true;
		dfs(son[x][i]);
	}
}
void clear(int x)
{
	if(son[x][0]==0)
	    return ;
	used[x]=false;
	for(int i=1;i<=son[x][0];i++)
		clear(son[x][i]);
}
int main() 
{
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	for(int i=1;i<=n-1;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		if(u>v)
		    swap(u,v);
		son[u][++son[u][0]]=v;
	}
	int fir=0,sec=0,f=0,s=0;
	for(int i=1;i<=son[1][0];i++)
	{
		ans=0;
		ans+=a[son[1][i]];
		used[son[1][i]]=true;
		dfs(son[1][i]);
		if(ans>fir)
		{
			sec=fir; fir=ans;
			used[s]=false;
			clear(s);
			s=f; f=son[1][i];
		}
		else if(ans>sec)
		{
			sec=ans;
			used[s]=false;
			clear(s);
			s=son[1][i];
		}
		else
		{
			used[son[1][i]]=false;
			clear(son[1][i]);
		}
	}
	sum+=fir+sec+a[1];
	int maxn=0;
	for(int i=1;i<=n;i++)
		if(used[i]==false && a[i]>maxn)
		    maxn=a[i];
	sum+=maxn;
	printf("%lld\n",sum);
	return 0;
}
