#include<bits/stdc++.h>
#define lsg 1000000007
#define LL long long
inline LL read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	LL k=0,kk=1;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return k*kk; 
}void write(LL x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(LL x){write(x);puts("");}
using namespace std;
int a,b,k,t,f[2000010],dd,m,g[2000010];
LL ans;
signed main(){
	freopen("magic.in","r",stdin);freopen("magic.out","w",stdout);
	a=read();b=read();k=read()*2;t=read();
	f[0]=1;m=k;
	for (int i=1;i<=t;i++,m+=k){//前缀和优化dp 
		for (int j=1;j<=m;j++)f[j]=f[j]+f[j-1]>=lsg?f[j]+f[j-1]-lsg:f[j]+f[j-1];
		for (int j=m;j>=k+1;j--)f[j]=f[j]-f[j-k-1]<0?f[j]-f[j-k-1]+lsg:f[j]-f[j-k-1]; 
	}dd=b-a;//要超过多少 
	for (int i=m;i>=0;i--)g[i]=f[i]+g[i+1]>=lsg?f[i]+g[i+1]-lsg:f[i]+g[i+1];
	for (int i=max(0,-dd-1);i+dd+1<=m&&i<=m;i++)(ans+=1ll*f[i]*g[i+dd+1])%=lsg;//要求严格大于 
	writeln(ans);
}/*
每次0..2k
t次 t^2*k*2时间，t*k*2空间 
1 2 2 1
1 1 1 2
2 12 3 1
*/
