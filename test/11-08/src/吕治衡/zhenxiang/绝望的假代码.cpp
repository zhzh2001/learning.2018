#include<bits/stdc++.h>
#define lsg 1000000007
#define LL long long
#define int long long
inline LL read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	LL k=0,kk=1;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return k*kk; 
}void write(LL x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(LL x){write(x);puts("");}
using namespace std;
void doit(int x,int y){a[++kk][0]=last[x];a[kk][1]=y;last[x]=kk;}
void dfs(int x,int fa){
	int x=0,y=0,now=0,xx=0,yy=0;
	f[i][1]=f[i][2]=f[i][3]=f[i][4]=v[i];
	for (int i=last[x];i;i=a[i][0])
		if (a[i][1]!=fa){
			dfs(a[i][1],x);
			if (v[i]+f[a[i][1]][1]>f[i][1])x=f[i][1],f[i][1]=v[i]+f[a[i][1]][1];
				else x=max(x,v[i]+f[a[i][1]][1]);
			if (f[i][2]<f[a[i][1]][2])y=f[i][2],f[i][2]=f[a[i][1]][2],now=a[i][1];
				else y=max(y,f[a[i][1]][2]);
			f[i][3]=max(f[i][3],f[a[i][1]][4]);
			f[i][4]=max(f[i][4],f[a[i][1]][4]);
		}
	f[i][4]=max(f[i][4],f[i][2]+y);//不包含当前点的情况 
	for (int i=last[x];i;i=a[i][0])
		if (a[i][1]!=fa){
			xx=max(xx,v[i]+f[a[i][1]][1]);
			if (a[i][1]!=now)yy=max(yy,v[i]+f[a[i][1]][1]);
		}
	f[i][3]=max(f[i][3],max(f[i][2]+yy,y+xx));//最大自洽链和除那个子树以外的向上链或者次大自洽链 
	f[i][2]=max(f[i][2],f[i][1]+x-v[i]);//这个自洽链可以包含自己？
	
	f[i][1]=max(f[i][0],f[i][1]);f[i][2]=max(f[i][2],f[i][1]);
	f[i][3]=max(f[i][3],f[i][2]);f[i][4]=max(f[i][3],f[i][4]);
	ans=max(ans,f[i][4]);
}
signed main(){
	n=read();for (int i=1;i<=n;i++)v[i]=read();
	for (int i=1;i<=n;i++)x=read(),y=read(),doit(x,y),doit(y,x);
	dfs(1,0);writeln(ans); 
}/*
f[i][0]i的子树不参与 
f[i][1]为i的子树下有一条向上的链
f[i][2]i的子树下有一条自洽的链
f[i][3]一条自洽一条向上
f[i][4]两条自洽 
x为次大向上链
y为次大自洽链  
*/
