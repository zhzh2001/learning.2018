#include<bits/stdc++.h>
#define lsg 1000000007
#define LL long long
#define int long long
inline LL read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	LL k=0,kk=1;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return k*kk; 
}void write(LL x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(LL x){write(x);puts("");}
using namespace std;
int a[200010][2],kk,last[100010],f[100010][5],v[100010],ans,n,x,y;
int g[100010],gg[100010];
void doit(int x,int y){a[++kk][0]=last[x];a[kk][1]=y;last[x]=kk;}
void dfs(int x,int fa){
	for (int i=last[x];i;i=a[i][0])
		if (a[i][1]!=fa){
			dfs(a[i][1],x);
			int k=4;f[x][4]=f[a[i][1]][1];
			while (k>1&&f[x][k]>f[x][k-1])k--,swap(f[x][k],f[x][k+1]);
			int dd=max(f[a[i][1]][1]+f[a[i][1]][2]-v[a[i][1]],g[x]);
			if (dd>g[x])gg[x]=g[x],g[x]=dd;else gg[x]=max(gg[x],dd);
		}
	for (int i=1;i<=4;i++)f[x][i]+=v[x];
}
void clc(int x,int fa){
	if (fa){
		int k=4,xx=f[fa][1]+f[fa][2]+f[fa][3]-v[fa],yy=0;
		if (f[fa][1]==f[x][1]+v[fa])xx-=f[fa][1],f[x][4]=f[fa][2]+v[x];
			else if (f[fa][2]==f[x][1]+v[fa])xx-=f[fa][2],f[x][4]=f[fa][1]+v[x];
				else xx-=f[fa][3],f[x][4]=f[fa][1]+v[x];
		yy=max(g[x],f[x][1]+f[x][2]-v[x]);
		if (g[fa]==yy)xx=max(xx,gg[fa]);else xx=max(xx,g[fa]);
		ans=max(ans,xx+yy);
		while (k>1&&f[x][k]>f[x][k-1])k--,swap(f[x][k],f[x][k+1]); 
		if (xx>g[x])gg[x]=g[x],g[x]=xx;else gg[x]=max(gg[x],xx);
	}for (int i=last[x];i;i=a[i][0])
		if (a[i][1]!=fa)clc(a[i][1],x);
}
signed main(){
	freopen("zhenxiang.in","r",stdin);freopen("zhenxiang.out","w",stdout);
	n=read();for (int i=1;i<=n;i++)v[i]=read();
	for (int i=1;i<n;i++)x=read(),y=read(),doit(x,y),doit(y,x);
	dfs(1,0);clc(1,0);if (n==1)ans=v[1];
	writeln(ans); 
}/*
好的我们重新考虑正常做法
路径不能相交
就是用一条边把树切开
最大化两棵树的直径和
记录前三大的儿子深度即可
再记录不包含这个点的子树最长链 
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
*/
