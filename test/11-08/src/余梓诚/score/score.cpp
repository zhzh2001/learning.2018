#include<bits/stdc++.h>
using namespace std;
const int N=2100000;
const int mo=1000000007;
int a,b,k,t,ans;
int f[N],g[N];
int main(){
    freopen("score.in","r",stdin);
    freopen("score.out","w",stdout);
    scanf("%d%d%d%d",&a,&b,&k,&t);
    f[0]=1;
    for (int i=1;i<=t;i++){
        for (int j=1;j<=2*i*k;j++) f[j]=(f[j-1]+f[j])%mo;
		for (int j=2*i*k;j>=2*k+1;j--) f[j]=(f[j]+mo-f[j-2*k-1])%mo;
    }
	for (int i=2*k*t;i;i--) g[i]=f[i];
    for (int i=2*k*t;i;i--) f[i]=(f[i]+f[i+1])%mo;
    for (int i=0;i<=2*k*t;i++) ans=(ans+1ll*g[i]*f[max(i+b-a+1,0)])%mo;
    printf("%d",ans);
	return 0;
}
