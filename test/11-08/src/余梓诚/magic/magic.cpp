#include <bits/stdc++.h>
using namespace std;
void judge() {
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
}
const int N=6100;
int n,k,cnt,in[N],a[N],alb[N];
struct info {
	int to,nxt;
} e[N*N];
int head[N],opt;
void add(int x,int y) {
	e[++opt]=(info) {y,head[x]};
	in[y]++;
	head[x]=opt;
}
void top() {
	priority_queue<int,vector<int>,greater<int> > q;
	for (int i=1; i<=n; i++) {
		if (!in[i]) q.push(i);
	}
	while (q.size()) {
		int u=q.top();
		q.pop();
		alb[++cnt]=u;
		for (int i=head[u]; i; i=e[i].nxt) {
			int k=e[i].to;
			in[k]--;
			if (!in[k]) q.push(k);
		}
	}
}
int abs(int x) {
	return x<0?-x:x;
}
int main() {
	judge();
	scanf("%d%d",&n,&k);
	if (k==1) {
		for (int i=1; i<=n; i++) printf("%d ",&i);
		return 0;
	}
	for (int i=1; i<=n; i++) {
		int x;
		scanf("%d",&x);
		a[x]=i;
	}
	for (int i=1; i<=n; i++) {
		for (int j=i+1; j<=n; j++) {
			if (abs(a[i]-a[j])<k) add(a[i],a[j]);
		}
	}
	top();
	for (int i=1; i<=n; i++) printf("%d ",alb[i]);
	return 0;
}
