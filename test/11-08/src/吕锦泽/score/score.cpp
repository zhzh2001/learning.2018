#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 200005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=0;
	while (ch<'0'||ch>'9') { if (ch=='-') f=1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return f?-x:x;
}
ll a,b,k,t,f[N],sum[N],M,L,R,tmp,ans;
ll get(ll now,ll l,ll r){
	l=max(l,M-now*k);
	r=min(r,M+now*k);
	return sum[r]-sum[l-1];
}
void calc(ll x,ll y){
	if (y<0) tmp=sum[0];
	else if (y>R) tmp=0;
	else tmp=sum[y];
	ans=ans+x*tmp%mod;
}
int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	a=read(); b=read(); k=read(); t=read();
	M=k*t; L=0; R=M<<1; f[M]=1; sum[M]=1;
	rep(i,1,t){
		ll l=M-k*i,r=M+k*i;
		rep(j,l,r) f[j]=get(i-1,j-k,j+k);
		rep(j,l,r) sum[j]=(sum[j-1]+f[j])%mod;
	}
	per(i,R,L) sum[i]=(sum[i+1]+f[i])%mod;
	rep(i,L,R) calc(f[i],i+b-a+1);
	printf("%lld",(ans%mod+mod)%mod);
}
/*
1 2 2 2

*/
