#include<cstdio>
#include<queue>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=0;
	while (ch<'0'||ch>'9') { if (ch=='-') f=1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return f?-x:x;
}
ll n,k,tot,head[N],indeg[N],pos[N];
struct edge{ ll to,nxt; }e[10000000];
struct data{ ll val; };
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
	++indeg[v];
}
bool operator <(data x,data y) { return x.val>y.val; }
priority_queue<data> Q;
int main(){
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n=read(); k=read();
	rep(i,1,n) { ll x=read(); pos[x]=i;	}
	rep(i,1,n) rep(j,i+1,n)
		if (abs(pos[i]-pos[j])<k) add(pos[i],pos[j]);
	rep(i,1,n) if (!indeg[i]) Q.push((data){i});
	while (!Q.empty()){
		ll u=Q.top().val; Q.pop();
		printf("%d ",u);
		for (ll i=head[u],v=e[i].to;i;i=e[i].nxt,v=e[i].to)
			if (!(--indeg[v])) Q.push((data){v});
	}
}

/*
8 3
4 5 7 8 3 1 2 6

*/
