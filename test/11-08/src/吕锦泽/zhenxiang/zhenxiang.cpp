#include<cstdio>
#include<queue>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define inf (1ll<<60)
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=0;
	while (ch<'0'||ch>'9') { if (ch=='-') f=1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return f?-x:x;
}
struct edge{ ll to,nxt; }e[N<<1];
ll n,ans=-inf,ans1,ans2,tot,a[N],head[N],flag[N];
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
ll work(ll u,ll T,ll last){
	if (u==T){
		flag[u]=1;
		return a[u];
	}
	for (ll i=head[u],v=e[i].to;i;i=e[i].nxt,v=e[i].to){
		if (v==last) continue;
		ll tmp=work(v,T,u);
		if (tmp){
			flag[u]=1;
			return a[u]+tmp;
		}
	}
	return 0;
}
void dfs(ll u,ll last,ll now){
	ans2=max(ans2,now);
	for (ll i=head[u],v=e[i].to;i;i=e[i].nxt,v=e[i].to){
		if (v==last||flag[v]) continue;
		dfs(v,u,now+a[v]);
	}
}
int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=read();
	rep(i,2,n){
		ll u=read(),v=read();
		add(u,v); add(v,u);
	}
	per(i,n,1) per(j,i,1){
		memset(flag,0,sizeof flag);
		ans1=work(i,j,-1); ans2=-inf;
		rep(k,1,n) if (!flag[k]) dfs(k,-1,a[k]);
		ans=max(ans,ans1+ans2);
	}
	printf("%lld",ans);
}
/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9


*/
