#include <map>
#include <set>
#include <cmath>
#include <queue>
#include <cstdio>
#include <bitset>
#include <vector>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
#define maxn 100010
//#define MOD 
#define int long long
char gc()
{
	static char buf[100000],*p1=buf+100000,*p2=p1;
	if(p1==p2)
	{
		p1=buf;
		p2=buf+fread(buf,1,100000,stdin);
	}
	return *p1++;
}
#define gc getchar
int read()
{
	int d=0,w=1;
	char c=gc();
	for(;c<'0'||c>'9';c=gc())
		if(c=='-') w=-1;
	for(;c>='0'&&c<='9';c=gc())
		d=(d<<1)+(d<<3)+c-48;
	return d*w;
}
void write(int x)
{
	if(x<0)
	{
		putchar('-');
		x=-x;
	}
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void wrs(int x)
{
	write(x);
	putchar(' ');
}
void wln(int x)
{
	write(x);
	putchar('\n');
}
int ma,f[maxn],dis[maxn],vis[maxn],bb[maxn<<1][2],h[maxn],r,a[maxn],kk,k,n,ans;
void add(int x,int y)
{
	bb[++r][0]=y;
	bb[r][1]=h[x];
	h[x]=r;
}
void dfs(int k,int fa)
{
	f[k]=fa;
	dis[k]=dis[fa]+a[k];
	if(dis[k]>dis[kk]) kk=k;
	for(int i=h[k];i;i=bb[i][1])
		if(bb[i][0]!=fa&&!vis[bb[i][0]]) dfs(bb[i][0],k);
}
void dfs2(int k,int fa)
{
	f[k]=fa;
	dis[k]=dis[fa]+a[k];
	vis[k]=1;
	if(dis[k]>dis[kk]) kk=k;
	for(int i=h[k];i;i=bb[i][1])
		if(bb[i][0]!=fa&&!vis[bb[i][0]]) dfs(bb[i][0],k);
}
signed main()
{
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	for(int i=1;i<=n;a[i++]=read());
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read();
		add(x,y);
		add(y,x);
	}
	kk=1;
	dfs(1,0);
	dfs(kk,0);
	ans=dis[kk];
//	wrs(k);
//	wrs(kk);
//	wln(ans);
	for(;kk;kk=f[kk])
		vis[kk]=1;
	for(int i=1;i<=n;i++)
	{
		if(vis[i]) continue;
		kk=i;
		dfs(kk,0);
		dfs2(kk,0);
		ma=max(ma,dis[kk]);
	}
	write(ans+ma);
	return 0;
}
