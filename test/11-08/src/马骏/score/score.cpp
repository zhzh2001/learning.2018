#include <map>
#include <set>
#include <cmath>
#include <queue>
#include <cstdio>
#include <bitset>
#include <vector>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
#define maxn 200000
#define MOD 1000000007
#define int long long
char gc()
{
	static char buf[100000],*p1=buf+100000,*p2=p1;
	if(p1==p2)
	{
		p1=buf;
		p2=buf+fread(buf,1,100000,stdin);
	}
	return *p1++;
}
#define gc getchar
int read()
{
	int d=0,w=1;
	char c=gc();
	for(;c<'0'||c>'9';c=gc())
		if(c=='-') w=-1;
	for(;c>='0'&&c<='9';c=gc())
		d=(d<<1)+(d<<3)+c-48;
	return d*w;
}
void write(int x)
{
	if(x<0)
	{
		putchar('-');
		x=-x;
	}
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void wrs(int x)
{
	write(x);
	putchar(' ');
}
void wln(int x)
{
	write(x);
	putchar('\n');
}
int k,t,a,b,ni[maxn],jcn[maxn],jc[maxn],n,ans[maxn],aans,sum[maxn<<1];
int calc(int n,int m)
{
	if(n<m||n<0||m<0) return 0;
	return jc[n]*jcn[m]%MOD*jcn[n-m]%MOD;
}
signed main()
{
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	a=read();
	b=read();
	k=read();
	t=read();
	n=t+k*t;
	ni[0]=0;
	jc[0]=ni[1]=jcn[0]=sum[0]=jc[1]=jcn[1]=1;
	for(int i=2;i<=n;i++)
	{
		jc[i]=jc[i-1]*i%MOD;
		ni[i]=(-MOD/i*ni[MOD%i]%MOD+MOD)%MOD;
		jcn[i]=jcn[i-1]*ni[i]%MOD;
	}
	for(int i=k*t;i>=0;i--)
		ans[i]=calc(t+k*t-i-1,k*t-i);
	for(int i=-k*t+1;i<=k*t+a-b;i++)
		sum[i+k*t]=sum[i+k*t-1]+ans[i<0?-i:i];
	for(int i=-k*t;i<=k*t;i++)
		(aans+=i+k*t-1+a-b<0?0:(ans[i<0?-i:i]*sum[i+k*t-1+a-b]%MOD))%=MOD;
	write((aans%MOD+MOD)%MOD);
	return 0;
}
