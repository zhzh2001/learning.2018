#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
    ch = NC(), isneg = 1;
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 1e5 + 5;
int n, k, a[kMaxN], pos[kMaxN], ans[kMaxN];

int main() {
  freopen("magic.in", "r", stdin);
  freopen("magic.out", "w", stdout);
  RI(n), RI(k);
  for (int i = 1; i <= n; ++i) {
    RI(a[i]);
    pos[a[i]] = i;
    ans[i] = i;
  }
  if (k == 1) {
    for (int i = 1; i < n; ++i) {
      if (a[i] == a[i + 1] + 1) {
        swap(a[i], a[i + 1]);
      }
    }
    for (int i = 1; i <= n - 2; ++i) {
      if (a[i] == a[i + 2] + 1 && a[i + 2] == a[i + 1] + 1) {
        swap(a[i], a[i + 2]);
        swap(a[i + 1], a[i + 2]);
      }
    }
    for (int i = 1; i <= n; ++i) {
      printf("%d%c", a[i], " \n"[i == n]);
    }
    return 0;
  }
  for (int v = 1; v <= n; ++v) {
    for (int i = 1; i <= n; ++i) {
      for (int j = j + 1; j <= min(i + k, n); ++j) {
        if (a[i] == a[j] + 1) {
          swap(a[i], a[j]);
        }
      }
    }
  }
  for (int i = 1; i <= n; ++i) {
    printf("%d%c", a[i], " \n"[i == n]);
  }
  return 0;
}
