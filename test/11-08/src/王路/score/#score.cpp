#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
    ch = NC(), isneg = 1;
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxK = 1005, kMaxT = 105, kMod = 1e9 + 7, kMaxV = 3e5 + 5;;

int a, b, k, t;
ll f[2][kMaxV];

inline ll& DP(int i, int j) {
  return f[i][j + 150001];
}

int main() {
  freopen("score.in", "r", stdin);
  freopen("score.out", "w", stdout);
  RI(a), RI(b), RI(k), RI(t);
  int P = k * t;
  DP(0, 0) = 1;
  for (int j = -P; j <= P; ++j) {
    DP(0, j) = (DP(0, j - 1) + DP(0, j)) % kMod;
  }
  for (int i = 1; i <= t; ++i) {
    int cur = i & 1;
    memset(f[cur], 0x00, sizeof f[cur]);
    for (int j = -P; j <= P; ++j) {
      DP(cur, j) = (DP(cur ^ 1, min(j + k, P)) - DP(cur ^ 1, max(j - k - 1, -P - 1)) + kMod) % kMod;
    }
    for (int j = -P; j <= P; ++j) {
      DP(cur, j) = (DP(cur, j - 1) + DP(cur, j)) % kMod;
    }
  }
  ll total = 0;
  for (int i = -P; i <= P; ++i) {
    total = (total + (DP(t & 1, i) - DP(t & 1, i - 1) + kMod) % kMod * DP(t & 1, a - b + i - 1)) % kMod;
  }
  printf("%lld\n", (total % kMod + kMod) % kMod);
  return 0;
}
