#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
    ch = NC(), isneg = 1;
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 1e5 + 5;
int n, a[kMaxN];
ll ans;

struct Graph {
  int head[kMaxN], ecnt;
  ll f[4][kMaxN];
  struct Edge { int to, nxt; } e[kMaxN << 1];
  inline void Insert(int x, int y) {
    e[++ecnt] = (Edge) {y, head[x]};
    head[x] = ecnt;
  }
  void DFS(int x, int fa) {
    for (int i = head[x]; i; i = e[i].nxt) {
      if (e[i].to == fa)
        continue;
      DFS(e[i].to, x);
      f[2][x] = max(f[2][x], f[1][e[i].to]);
      if (f[0][e[i].to] > f[0][x]) {
        f[2][x] = max(f[2][x], f[1][x]);
        f[1][x] = f[0][x];
        f[0][x] = f[0][e[i].to];
      } else if (f[0][e[i].to] > f[1][x]) {
        f[2][x] = max(f[2][x], f[1][x]);
        f[1][x] = f[0][e[i].to];
      } else {
        f[2][x] = max(f[0][e[i].to] + f[1][e[i].to], f[2][x]);
        f[3][x] = max(f[3][x], f[3][e[i].to]);
      }
      f[2][x] = max(f[2][x], f[2][e[i].to]);
    }
    f[0][x] += a[x];
    ans = max(ans, f[0][x] + f[1][x] + f[2][x]);
    ans = max(ans, f[3][x] + f[0][x]);
    f[3][x] = f[0][x] + f[1][x];
  }
  int vis[kMaxN], s1, s2, t1, t2;
  ll tmp;
  void DFS2(int x, int fa) {
    if (vis[x])
      return;
    vis[x] = true;
    tmp += a[x];
    if (x == t2) {
      ans = max(ans, tmp);
      return;
    }
    for (int i = head[x]; i; i = e[i].nxt) {
      if (vis[e[i].to] || e[i].to == fa)
        continue;
      DFS2(e[i].to, x);
    }
    tmp -= a[x];
    vis[x] = false;
  }
  void DFS1(int x, int fa) {
    vis[x] = true;
    tmp += a[x];
    if (x == t1) {
      DFS2(s2, 0);
      return;
    }
    for (int i = head[x]; i; i = e[i].nxt) {
      if (e[i].to == fa)
        continue;
      DFS1(e[i].to, x);
    }
    tmp -= a[x];
    vis[x] = false;
  }
} g;

void Solve(int s1, int t1, int s2, int t2) {
  g.tmp = 0;
  g.s1 = s1, g.s2 = s2, g.t1 = t1, g.t2 = t2;
  g.DFS1(s1, 0);
}

int main() {
  freopen("zhenxiang.in", "r", stdin);
  freopen("zhenxiang.out", "w", stdout);
  RI(n);
  for (int i = 1; i <= n; ++i) {
    RI(a[i]);
  }
  for (int i = 1, u, v; i < n; ++i) {
    RI(u), RI(v);
    g.Insert(u, v);
    g.Insert(v, u);
  }
  if (n <= 30) {
    for (int s1 = 1; s1 <= n; ++s1) {
      for (int t1 = 1; t1 <= n; ++t1) {
        for (int s2 = 1; s2 <= n; ++s2) {
          if (s1 == s2 || t1 == s2)
            continue;
          for (int t2 = 1; t2 <= n; ++t2) {
            if (s1 == t2 || t1 == t2)
              continue;
            Solve(s1, t1, s2, t2);
          }
        }
      }
    }
  }
  for (int i = 1; i <= n; ++i) {
    memset(g.f, 0x00, sizeof g.f);
    g.DFS(i, 0);
  }
  printf("%lld\n", ans);
  return 0;
}
