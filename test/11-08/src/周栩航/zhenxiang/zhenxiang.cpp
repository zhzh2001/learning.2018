#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define int ll
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=300005;
int poi[N],nxt[N],head[N],in[N],out[N],fr1[N],fr2[N],vis[N],cnt,n,Fa[N],v[N],dep[N],fr3[N];
ll dp1[N],dp2[N],dp3[N],ans,tim,dp[N];
inline void add(int x,int y)
{
	poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;
	poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;
}
inline void Upd(int dp,int fr,int x)
{
	if(dp>dp1[x])	fr3[x]=fr2[x],fr2[x]=fr1[x],fr1[x]=fr,dp3[x]=dp2[x],dp2[x]=dp1[x],dp1[x]=dp;
	else	if(dp>dp2[x])	fr3[x]=fr2[x],fr2[x]=fr,dp3[x]=dp2[x],dp2[x]=dp;
	else	if(dp>dp3[x])	fr3[x]=fr,dp3[x]=dp;
}
inline bool inc(int x,int y){return in[x]<=in[y]&&in[y]<=out[x];}
inline void Dfs(int x,int fa)
{
	Fa[x]=fa;in[x]=++tim;
	bool leaf=1;
	ll mx=0,mx1=0;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		Dfs(poi[i],x);
		leaf=0;
		int tv=poi[i];
		if(dp[tv]>mx)	mx1=mx,mx=dp[tv];
		else	if(dp[tv]>mx1)	mx1=dp[tv];
		dp[x]=max(dp[x],dp[tv]);
		Upd(dp1[tv]+v[tv],fr1[tv],x);
	}
	ans=max(ans,mx1+mx);
	dp[x]=max(dp[x],dp1[x]+dp2[x]+v[x]);
	if(leaf)	fr1[x]=x;
	out[x]=tim;
}

inline void Dfs2(int x,ll ext,ll las)
{
	ans=max(ans,dp1[x]+dp2[x]+dp3[x]+v[x]);
	ans=max(ans,dp1[x]+dp2[x]+ext+v[x]);
	ans=max(ans,dp1[x]+dp2[x]+las+v[x]);
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==Fa[x])	continue;
		ll n_ext=ext+v[x];
		if(!inc(poi[i],fr1[x]))n_ext=max(n_ext,v[x]+dp1[x]);
		else	n_ext=max(n_ext,v[x]+dp2[x]);
		ll n_las=las;
		if(inc(poi[i],fr1[x]))	n_las=max(n_las,dp2[x]+dp3[x]+v[x]);
		else
		if(inc(poi[i],fr2[x]))	n_las=max(n_las,dp1[x]+dp3[x]+v[x]);
		else	n_las=max(n_las,dp1[x]+dp2[x]+v[x]);
		if(!inc(poi[i],fr1[x]))	n_las=max(n_las,dp1[x]+ext+v[x]);
		else	n_las=max(n_las,dp2[x]+ext+v[x]);
		Dfs2(poi[i],n_ext,n_las);
	}
}
signed main()
{
	freopen("zhenxiang.in","r",stdin);freopen("zhenxiang.out","w",stdout);
	n=read();
	For(i,1,n)	v[i]=read();
	For(i,1,n-1)	add(read(),read());
	Dfs(1,1);
	Dfs2(1,0,0);	
	writeln(ans);
}
