#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define int ll
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=50005;
int poi[N],nxt[N],head[N],fr1[N],fr2[N],vis[N],cnt,n,Fa[N],v[N],dep[N];
ll dp1[N],dp2[N],zans;
inline void add(int x,int y)
{
	poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;
	poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;
}
inline void Dfs1(int x)
{
	dp1[x]=0;dp2[x]=0;fr1[x]=0;fr2[x]=0;
	bool leaf=1;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==Fa[x])	continue;
		Dfs1(poi[i]);
		leaf=0;
		int v=poi[i];
		if(dp1[v]>dp1[x])	dp2[x]=dp1[x],fr2[x]=fr1[x],dp1[x]=dp1[v],fr1[x]=fr1[v];
		else	if(dp1[v]>dp2[x])	dp2[x]=dp1[v],fr2[x]=fr1[v];
	}
	dp1[x]+=v[x];if(fr2[x])	dp2[x]+=v[x];
	if(leaf)	fr1[x]=x;
}
inline void Dfs2(int x)
{
	vis[x]=1;
	dp1[x]=0;dp2[x]=0;fr1[x]=0;fr2[x]=0;
	bool leaf=1;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==Fa[x]||vis[poi[i]])	continue;
		Dfs2(poi[i]);
		leaf=0;
		int v=poi[i];
		if(dp1[v]>dp1[x])	dp2[x]=dp1[x],fr2[x]=fr1[x],dp1[x]=dp1[v],fr1[x]=fr1[v];
		else	if(dp1[v]>dp2[x])	dp2[x]=dp1[v],fr2[x]=fr1[v];
	}
	dp1[x]+=v[x];if(fr2[x])	dp2[x]+=v[x];
	if(leaf)	fr1[x]=x;
}
inline void Solve(int x)
{
	For(i,1,n)	dp1[i]=dp2[i]=fr1[i]=fr2[i]=0;
	Dfs1(x);
	ll ans=0;
	int p1=fr1[x],p2=fr2[x];
	if(!p2)	p2=x;
	while(p1!=p2)
	{
		if(dep[p1]<dep[p2])	swap(p1,p2);
		vis[p1]=1;p1=Fa[p1];
	}
	ans=dp1[x]+dp2[x]-(dp2[x]?v[x]:0);
	vis[p1]=1;
	For(i,1,n)	dp1[i]=dp2[i]=fr1[i]=fr2[i]=0;
	For(i,1,n)	if(!vis[i])	Dfs2(i);
	ll tans=0;
	For(i,1,n)	vis[i]=0,tans=max(dp1[i]+dp2[i]-(dp2[i]?v[i]:0),tans);
	zans=max(zans,ans+tans);
}
inline void Dfs(int x,int fa)
{
	Fa[x]=fa;
	for(int i=head[x];i;i=nxt[i])	if(poi[i]!=fa)	dep[poi[i]]=dep[x]+1,Dfs(poi[i],x);
}
signed main()
{
	
//	freopen("in.txt","r",stdin);freopen("ans.txt","w",stdout);
	freopen("zhenxiang.in","r",stdin);freopen("zhenxiang.ans","w",stdout);
	n=read();
	For(i,1,n)	v[i]=read();
	For(i,1,n-1)	add(read(),read());
	Dfs(1,1);
	Solve(1);
	For(rt,1,n)	Solve(rt);
	writeln(zans);
}
/*
10
22 952 122 109 430 778 974 610 888 2 
2 1
3 2
4 3
5 1
6 5
7 4
8 2
9 5
10 1

*/
