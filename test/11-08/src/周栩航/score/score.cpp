#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
int a,b,k,n,mo=1e9+7;
const int N=2500000;
int sum[N],dp[N];
int main()
{
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	a=read();b=read();k=read();n=read();
	dp[n*k]=1;For(i,0,2*n*k)	sum[i]=(i==0?0:sum[i-1])+dp[i];
	For(i,1,n)
	{
		For(j,0,2*n*k)
		{
			int l=max(0,j-k),r=min(2*n*k,j+k);
			dp[j]=((sum[r]-sum[l-1])%mo+mo)%mo;
		}
		For(i,0,2*n*k)	sum[i]=(i==0?0:sum[i-1])+dp[i],sum[i]%=mo;
	}
	Dow(i,0,2*n*k)	sum[i]=(i==2*n*k?0:sum[i+1])+dp[i],sum[i]%=mo;
	int ned=b-a+1;
	int ans=0;
	For(i,0,2*n*k-ned)
		ans+=1LL*dp[i]*sum[i+ned]%mo,ans%=mo;
	writeln(ans);
}
