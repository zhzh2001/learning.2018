#include<bits/stdc++.h>
using namespace std;
#define maxn 100100
#define int long long
int head[maxn],nxt[maxn<<1],ver[maxn<<1],tot;
inline void addedge(int a,int b)
{
	nxt[++tot]=head[a];ver[tot]=b;head[a]=tot;
	nxt[++tot]=head[b];ver[tot]=a;head[b]=tot;
}
int ans=0,f[maxn],g[maxn],a[maxn];int n;
inline void treedp(int x,int fat,int upv)
{
	int Max=0,Smax=0,Gmax=0,Sgmax=0;
	multiset<int> s;f[x]=g[x]=0;
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;
		treedp(y,x,upv+a[x]);f[x]=max(f[x],f[y]);g[x]=max(g[x],g[y]);
		if(f[y]>Max) Smax=Max,Max=f[y];
		else if(f[y]==Max) Smax=f[y];
		else if(f[y]>Smax) Smax=f[y];
		if(g[y]>Gmax) Sgmax=Gmax,Gmax=g[y];
		else if(g[y]==Gmax) Sgmax=g[y];
		else if(g[y]>Sgmax) Sgmax=g[y];
		s.insert(g[y]);
	}
	while(s.size()>3) s.erase(*s.begin());
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;bool flag=0;
		if(s.find(g[y])!=s.end())s.erase(s.find(g[y])),flag=1;
		if(s.size()) ans=max(ans,*--s.end()+f[y]+a[x]+upv);
		for(int j=nxt[i];j;j=nxt[j])
		{
			int z=ver[j];if(z==fat) continue;bool Flag=0;
			if(s.find(g[z])!=s.end()) s.erase(s.find(g[z])),Flag=1;
			if(s.size()) ans=max(ans,f[y]+f[z]+a[x]+*--s.end());
			if(Flag) s.insert(g[z]);
		}
		if(flag) s.insert(g[y]);
	}
	f[x]+=a[x];g[x]=max(g[x],Max+Smax+a[x]);ans=max(ans,g[x]);ans=max(ans,Sgmax+Gmax);
}
inline void Treedp(int x,int fat,int upv)
{
	multiset<int> s;multiset<int> ss;
	for(int i=head[x];i;i=nxt[i]) ss.insert(f[ver[i]]);
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;ss.erase(ss.find(f[y]));
		Treedp(y,x,max(upv,*--ss.end())+a[x]);
		ss.insert(f[y]);
		s.insert(g[y]);
	}
	while(s.size()>3) s.erase(*s.begin());
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;bool flag=0;
		if(s.find(g[y])!=s.end())s.erase(s.find(g[y])),flag=1;
		if(s.size()) ans=max(ans,*--s.end()+f[y]+a[x]+upv);
		if(flag) s.insert(g[y]);
	}
}
int CNT=0;
inline void fakedp(int x,int fat,int upv)
{
	int Max=0,Smax=0,Gmax=0,Sgmax=0;
	multiset<int> s;f[x]=g[x]=0;
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;
		fakedp(y,x,upv+a[x]);f[x]=max(f[x],f[y]);g[x]=max(g[x],g[y]);
		if(f[y]>Max) Smax=Max,Max=f[y];
		else if(f[y]==Max) Smax=f[y];
		else if(f[y]>Smax) Smax=f[y];
		if(g[y]>Gmax) Sgmax=Gmax,Gmax=g[y];
		else if(g[y]==Gmax) Sgmax=g[y];
		else if(g[y]>Sgmax) Sgmax=g[y];
		s.insert(g[y]);
	}
	while(s.size()>3) s.erase(*s.begin());
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;bool flag=0;
		if(s.find(g[y])!=s.end())s.erase(s.find(g[y])),flag=1;
		if(s.size()) ans=max(ans,*--s.end()+f[y]+a[x]+upv);
		for(int j=nxt[i];j;j=nxt[j])
		{
			int z=ver[j];if(z==fat) continue;bool Flag=0;
			if(++CNT>1000000) break;
			if(s.find(g[z])!=s.end()) s.erase(s.find(g[z])),Flag=1;
			if(s.size()) ans=max(ans,f[y]+f[z]+a[x]+*--s.end());
			if(Flag) s.insert(g[z]);
		}
		if(flag) s.insert(g[y]);
	}
	f[x]+=a[x];g[x]=max(g[x],Max+Smax+a[x]);ans=max(ans,g[x]);ans=max(ans,Sgmax+Gmax);
}
inline void Fakedp(int x,int fat,int upv)
{
	multiset<int> s;multiset<int> ss;
	for(int i=head[x];i;i=nxt[i]) ss.insert(f[ver[i]]);
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;ss.erase(ss.find(f[y]));
		Fakedp(y,x,max(upv,*--ss.end())+a[x]);
		ss.insert(f[y]);
		s.insert(g[y]);
	}
	while(s.size()>3) s.erase(*s.begin());
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;bool flag=0;
		if(s.find(g[y])!=s.end())s.erase(s.find(g[y])),flag=1;
		if(s.size()) ans=max(ans,*--s.end()+f[y]+a[x]+upv);
		if(flag) s.insert(g[y]);
	}
}
inline void init()
{
	scanf("%lld",&n);
	for(int i=1;i<=n;i++) scanf("%lld",a+i);
	for(int i=1,a,b;i<n;i++) scanf("%lld%lld",&a,&b),addedge(a,b);
}
#undef int
int main()
#define int long long
{
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	init();srand(666888);
	if(n<=3000) treedp(1,0,0),Treedp(1,0,0);
	else for(int i=1;i<=10;i++)
	{
		int x=1ll*rand()*rand()%n+1;
		fakedp(x,0,0);
		Fakedp(x,0,0);
	}
	printf("%lld",ans);
	return 0;
}
