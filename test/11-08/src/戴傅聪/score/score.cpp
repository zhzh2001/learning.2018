#include<bits/stdc++.h>
using namespace std;
#define mod 1000000007
inline void Add(int &x,int y){x=x+y>=mod?x+y-mod:x+y;}
int a,b,k,t;
const int BB=1100*210;
int f[2][BB];
int A[BB],B[BB];
const int Base=BB/2;
int main()
{
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	scanf("%d%d%d%d",&a,&b,&k,&t);
	memset(f,0,sizeof(f));
	f[0][0+Base]=1;
	for(int i=1;i<=t;i++)
	{
		int now=i&1,lst=now^1;memset(f[now],0,sizeof(f[now]));
		int l=1,r=1,nowsum=f[lst][r];
		for(int j=1;j<BB;j++)
		{
			while(r<BB-1&&r-j<k) r++,Add(nowsum,f[lst][r]);
			while(j-l>k) Add(nowsum,mod-f[lst][l]),l++;
			f[now][j]=nowsum;
		}
	}
	for(int i=a;i<BB;i++) A[i]=f[t&1][i-a];
	for(int i=b;i<BB;i++) B[i]=f[t&1][i-b];
	int nowsum=0,ans=0;
	for(int i=BB-1;i;i--)
	{
		Add(ans,1ll*nowsum*B[i]%mod);
		Add(nowsum,A[i]);
	}
	cout<<ans;return 0;
}
