uses math;
var a,b,k,t,i,j:longint; p,ans:int64;
    f,g:array[0..1,-100100..100100]of int64;
    x:array[-100101..100100]of qword;
    y,z:array[-100101..100100]of boolean;
function add(a,b:int64):int64;
begin
 inc(a,b);
 if a>=p then dec(a,p);
 exit(a);
end;
function kuai(a,b:int64):int64;
var c:int64;
begin
 //writeln(a,' ',b);
 if b<=1 then exit(a);
 c:=kuai(a,b div 2);
 if b mod 2=0 then exit(2*c mod p);
 exit((2*c mod p+a)mod p);
end;
procedure dfs(n,m:longint);
var i:longint;
begin
 for i:=-k to k do
 begin
  f[n+1,m+i]:=add(f[n+1,m+i],f[n,m]);
  y[m+i]:=true;
 end;
end;
begin
 assign(input,'score.in');
 assign(output,'score.out');
 reset(input);
 rewrite(output);
 p:=1000000007;
 read(b,a,k,t);
 b:=b-a;
 a:=0;
 f[0,a]:=1;
 dfs(0,a);
 for i:=1 to t-1 do
 begin
  //writeln('#######',i);
  f[0]:=f[1];
  fillchar(f[1],sizeof(f[1]),0);
  z:=y;
  fillchar(y,sizeof(y),0);
  for j:=-k*i to k*i do
   if z[j] then dfs(0,j);
 end;
 //for j:=-2 to 2 do writeln(j,' ',f[1,j]);
 for i:=-k*t to k*t do g[1,i+b]:=f[1,i];
 for i:=-k*t to k*t do x[i]:=x[i-1]+f[1,i];
 //writeln('########');
 for i:=-k*t+b to k*t+b do ans:=add(ans,kuai(g[1,i],x[i-1]));
 //writeln('##########');
 writeln(ans);
 close(input);
 close(output);
end.