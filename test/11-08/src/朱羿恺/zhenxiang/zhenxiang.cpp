#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,x[N],y[N];
ll a[N],ans,Max1,Max2;
int tot,first[N],to[N<<1],last[N<<1];
inline void Add(int x,int y){to[++tot]=y,last[tot]=first[x],first[x]=tot;}
ll dp[N];
inline void dfs(int u,int fa){
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		dfs(v,u),dp[u]=max(dp[u],dp[v]);
	}
	dp[u]+=a[u];
}
ll Dp[N];
inline void Dfs(int u,int fa){
	int pos=0,Pos=0;
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		if (dp[v]>dp[pos]) Pos=pos,pos=v;
		else if (dp[v]>dp[Pos]) Pos=v;
		Dfs(v,u),Dp[u]=max(Dp[u],Dp[v]);
	}
	Dp[u]=max(Dp[u],dp[pos]+dp[Pos]+a[u]);
}
inline void calc(int u,int fa,ll Max){
	multiset<ll>s;
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		s.insert(dp[v]);
	}
	int cnt,b[4];
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		s.erase(s.find(dp[v]));cnt=0;
		if (!s.empty()) for (multiset<ll>::iterator it=(--s.end());cnt<=2&&it!=s.begin();++cnt) b[cnt]=*it;
		b[++cnt]=Max;
		sort(b+1,b+1+cnt);
		ans=max(ans,Dp[v]+b[cnt]+b[cnt-1]+b[u]);
	}
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		calc(v,u,max(Max,*(--s.end()))+a[u]);
	}
}
inline void Dfs1(int u,int fa){
	int pos=0,Pos=0;
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		if (dp[v]>dp[pos]) Pos=pos,pos=v;
		else if (dp[v]>dp[Pos]) Pos=v;
	}
	Max1=max(Max1,dp[pos]+dp[Pos]+a[u]);
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		Dfs1(v,u);
	}
}
inline void Dfs2(int u,int fa){
	int pos=0,Pos=0;
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		if (dp[v]>dp[pos]) Pos=pos,pos=v;
		else if (dp[v]>dp[Pos]) Pos=v;
	}
	Max2=max(Max2,dp[pos]+dp[Pos]+a[u]);
	cross(i,u){
		int v=to[i];
		if (v==fa) continue;
		Dfs1(v,u);
	}
}
int main(){
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	For(i,1,n) a[i]=read();
	For(i,1,n-1) x[i]=read(),y[i]=read(),Add(x[i],y[i]),Add(y[i],x[i]);
	For(i,1,n-1){
		For(j,1,n) dp[j]=0;
		dfs(x[i],y[i]),dfs(y[i],x[i]);
		Max1=Max2=0;
		Dfs1(x[i],y[i]),Dfs2(y[i],x[i]);
		ans=max(ans,Max1+Max2);
	}
	printf("%lld\n",ans);
}
