#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
//#define int ll
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int mod = 1e9+7, M = 410000;
int a,b,K,n,m,dp[2][M],sum[2][M],Sum[2][M];
inline int Query(int k,int j,int l,int r){
	if (l>r) return 0;int X=sum[k][r]-(l==0?0:sum[k][l-1]);
	int x=1ll*(X<0?X+mod:X)*(2*K+1+j)%mod;X=Sum[k][r]-(l==0?0:Sum[k][l-1]);
	int y=(X<0?X+mod:X);
	return x-y<0?x-y+mod:x-y;
}
inline int query(int k,int j,int l,int r){
	if (l>r) return 0;int X=2*K+1-j,Y=sum[k][r]-(l==0?0:sum[k][l-1]);
	int x=1ll*(Y<0?Y+mod:Y)*(X<0?X+=mod:X)%mod;X=Sum[k][r]-(l==0?0:Sum[k][l-1]);
	int y=(X<0?X+mod:X);
	return x+y>=mod?x+y-mod:x+y;
}
inline void Mod(int &x){if (x>=mod) x-=mod;}
signed main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	a=read(),b=read(),K=read(),n=read(),m=(2*K+1)*n;
	if (b-a>m) return puts("0"),0;
	dp[0][a-b+m]=1,sum[0][a-b+m]=1,Sum[0][a-b+m]=a-b+m;
	For(i,a-b+m+1,m*2) sum[0][i]=sum[0][i-1],Sum[0][i]=Sum[0][i-1];
	For(i,1,n){
		memset(dp[i&1],0,sizeof dp[i&1]);
		memset(sum[i&1],0,sizeof sum[i&1]);
		memset(Sum[i&1],0,sizeof Sum[i&1]);
		For(j,0,m*2){ 
		//	For(k,0,min(2*K,j)) dp[i][j]+=dp[i-1][j-k]*(2*K+1-k);
		//	For(k,1,min(2*K,m-j)) dp[i][j]+=dp[i-1][j+k]*(2*K+1-k);
			Mod(dp[i&1][j]=query((i&1)^1,j,j-min(2*K,j),j)+Query((i&1)^1,j,j+1,j+min(2*K,m*2-j)));
			Mod(sum[i&1][j]=((!j)?0:sum[i&1][j-1])+dp[i&1][j]);
			Mod(Sum[i&1][j]=((!j)?0:Sum[i&1][j-1])+1ll*dp[i&1][j]*j%mod);
		}
	}
//	For(i,1,n){
//		For(j,0,m*2) printf("%d ",dp[i][j]);
//		puts("");
//	}
	int ans=0;
	For(i,m+1,m*2) Mod(ans+=dp[n&1][i]);//,printf("%d %d\n",dp[n&1][i],i);
	printf("%d\n",ans);
}
