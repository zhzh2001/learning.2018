#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
const int Mx=2e5+7,P=1e9+7;
int A,B,K,T,ans,d0,mx;
int f[2][Mx],s[Mx];
void Ad(int &x,int y) {x+=y;if (x>=P) x-=P;}
int main() {
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	A=read();B=read();K=read();T=read();
	mx=K*T*2;d0=K*T;
	int nw=0;
	f[nw][d0]=1;
	for (int i=d0;i<=mx;i++) s[i]=1;
	for (int i=1,l,r;i<=T;i++) {
		nw=!nw;
//		memset(f[nw],0,sizeof f[nw]);
		for (int j=0;j<=mx;j++) {
			r=min(j+K,mx);l=max(0,j-K);
			if (l) f[nw][j]=(s[r]-s[l-1]+P)%P;
			else f[nw][j]=s[r];
		}
		s[0]=f[nw][0];
		for (int i=1;i<=mx;i++) s[i]=(s[i-1]+f[nw][i])%P;
	}
	for (int i=0;i<=mx;i++) if (i+A-B>0){
		Ad(ans,1ll*f[nw][i]*s[i+A-B-1]%P);
	}
	printf("%d\n",ans);
}
