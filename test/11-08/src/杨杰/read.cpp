#include <cstdio>
#include <cstring>
using namespace std;
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
void write(int x) {if (x<0) putchar('-'),x=-x;if (x>=10) write(x/10);putchar(x%10|'0');}
void wln(int x) {write(x);puts("");}void wsp(int x) {write(x);putchar(' ');}
const int N=1e5+7;
struct edge {
	int t,nxt;
}e[N<<1];
int n,cnt,T,K;
int head[N],fa[N][20],dep[N],dfn[N],siz[N];
void add(int u,int t) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;
}
void dfs(int u,int F) {
	fa[u][0]=F;dep[u]=dep[F]+1;
	dfn[u]=++T;siz[u]=1;
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==F) continue;
		dfs(t,u);
		siz[u]+=siz[t];
	}
}
int main() {
	n=read();
	for (int i=1;i<n;i++) {
		u=read(),v=read();
		add(u,v);add(v,u);
	}
	dfs(1,0);
}
