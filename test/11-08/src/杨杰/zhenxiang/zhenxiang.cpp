#include <cstdio>
#include <algorithm>
using namespace std;
#define ll long long
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
const int N=1e5+7;
struct edge {
	int t,nxt;
}e[N<<1];
int n,cnt;
int head[N],v[N];
ll ans;
ll f[N][4];//0此节点为终点的链,1路径,2此节点为终点的链+路径,3此节点为终点的路径+路径 
void add(int u,int t) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;
}
void dfs(int u,int fa) {
	f[u][0]=v[u];
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==fa) continue;
		dfs(t,u);
		f[u][3]=max(f[u][3],f[t][2]+f[u][0]);
		f[u][3]=max(f[u][3],f[t][1]+f[u][1]);
		f[u][3]=max(f[u][3],f[t][0]+f[u][1]); 
		f[u][3]=max(f[u][3],f[t][0]+f[u][2]); 
		f[u][2]=max(f[u][2],f[t][2]+v[u]);
		f[u][2]=max(f[u][2],f[t][1]+f[u][0]);
		f[u][2]=max(f[u][2],f[t][0]+f[u][0]);
		f[u][1]=max(f[u][1],f[t][0]+f[u][0]);
		f[u][1]=max(f[u][1],f[t][1]);
		f[u][0]=max(f[u][0],f[t][0]+v[u]);
	}
	ans=max(ans,f[u][0]);
	ans=max(ans,f[u][1]);
	ans=max(ans,f[u][2]);
	ans=max(ans,f[u][3]);
}
int main() {
	freopen("zhenxiang.in","r",stdin);
	freopen("zhenxiang.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) v[i]=read();
	for (int i=1,u,v;i<n;i++) {
		u=read(),v=read();
		add(u,v);add(v,u);
	}
	dfs(1,0);
	printf("%lld\n",ans);
}
