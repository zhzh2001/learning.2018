#include <cstdio>
#include <algorithm>
using namespace std;
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
void write(int x) {if (x<0) putchar('-'),x=-x;if (x>=10) write(x/10);putchar(x%10|'0');}
void wln(int x) {write(x);puts("");}void wsp(int x) {write(x);putchar(' ');}
const int N=1e5+7;
int n,K;
int a[N],pos[N];
void change(int x) {
	if (x==n || pos[x]-pos[x+1]<K) return;
	swap(a[pos[x]],a[pos[x+1]]);
	swap(pos[x],pos[x+1]);
	change(x+1);
}
int main() {
	freopen("magic.in","r",stdin);
	freopen("magic.out","w",stdout);
	n=read();K=read();
	if (K==1) {
		for (int i=1;i<=n;i++) wsp(i);
		return 0;
	}
	for (int i=1;i<=n;i++) a[i]=read(),pos[a[i]]=i;
	for (int i=n;i>=1;i--) change(i);
	for (int i=1;i<=n;i++) wsp(a[i]);
}
