#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("guard.in");
ofstream fout("guard.ans");
const int N=200005;
long long p[N][5];
int main()
{
	int n,d;
	fin>>n>>d;
	for(int i=1;i<=n;i++)
		for(int j=0;j<d;j++)
			fin>>p[i][j];
	long long ans=0;
	int x,y;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
		{
			long long now=0;
			for(int k=0;k<d;k++)
				now+=abs(p[i][k]-p[j][k]);
			if(now>ans)
			{
				ans=now;
				x=i;y=j;
			}
		}
	fout<<ans<<endl;
//	fout<<x<<' '<<y<<endl;
	return 0;
}
