#include<fstream>
#include<random>
#include<windows.h>
#include<limits>
using namespace std;
ofstream fout("guard.in");
const int n=2e5;
int main()
{
	minstd_rand gen(GetTickCount());
	uniform_int_distribution<> dd(5,5);
	int d=dd(gen);
	fout<<n<<' '<<d<<endl;
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=d;j++)
		{
			uniform_int_distribution<> d(numeric_limits<int>::min(),numeric_limits<int>::max());
			fout<<d(gen)<<' ';
		}
		fout<<endl;
	}
	return 0;
}
