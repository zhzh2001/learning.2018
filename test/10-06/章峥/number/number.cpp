#pragma GCC optimize(2)
#include<fstream>
#include<ext/pb_ds/tree_policy.hpp>
#include<ext/pb_ds/assoc_container.hpp>
using namespace std;
ifstream fin("number.in");
ofstream fout("number.out");
const int N=100005,M=10000005;
int a[N],cnt[M];
struct node
{
	int cnt,num;
	node(int cnt,int num):cnt(cnt),num(num){}
	bool operator<(const node& rhs)const
	{
		if(cnt==rhs.cnt)
			return num<rhs.num;
		return cnt>rhs.cnt;
	}
};
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		cnt[a[i]]++;
	}
	__gnu_pbds::tree<node,__gnu_pbds::null_type> S;
	for(int i=1;i<=n;i++)
		S.insert(node(cnt[a[i]],a[i]));
	while(m--)
	{
		int x,y;
		fin>>x>>y;
		S.erase(node(cnt[a[x]],a[x]));
		cnt[a[x]]--;
		S.insert(node(cnt[a[x]],a[x]));
		a[x]=y;
		S.erase(node(cnt[a[x]],a[x]));
		cnt[a[x]]++;
		S.insert(node(cnt[a[x]],a[x]));
		fout<<S.begin()->num<<'\n';
	}
	return 0;
}
