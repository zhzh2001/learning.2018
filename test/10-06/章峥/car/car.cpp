#include<fstream>
#include<string>
using namespace std;
ifstream fin("car.in");
ofstream fout("car.out");
const int N=1000005;
int cnt[N][3];
int main()
{
	int n;
	fin>>n;
	string s;
	fin>>s;
	s=' '+s;
	for(int i=1;i<=n;i++)
	{
		cnt[i][0]=cnt[i-1][0]+(s[i]=='B');
		cnt[i][1]=cnt[i-1][1]+(s[i]=='C');
		cnt[i][2]=cnt[i-1][2]+(s[i]=='S');
	}
	int cc=0;
	for(int i=n;i;i--)
	{
		for(int j=1;j+i-1<=n;j++)
		{
			int a=cnt[j+i-1][0]-cnt[j-1][0];
			int b=cnt[j+i-1][1]-cnt[j-1][1];
			int c=cnt[j+i-1][2]-cnt[j-1][2];
			if(a==i||b==i||c==i||(a!=b&&a!=c&&b!=c))
			{
				fout<<i<<endl;
				return 0;
			}
			cc++;
		}
		if(cc>1e8)
			break;
	}
	int l=1;
	for(;s[l]==s[1];l++);
	int r=n;
	for(;s[r]==s[n];r--);
	for(int i=n-min(l-1,n-r);i;i--)
	{
		for(int j=1;j+i-1<=n;j++)
		{
			int a=cnt[j+i-1][0]-cnt[j-1][0];
			int b=cnt[j+i-1][1]-cnt[j-1][1];
			int c=cnt[j+i-1][2]-cnt[j-1][2];
			if(a==i||b==i||c==i||(a!=b&&a!=c&&b!=c))
			{
				fout<<i<<endl;
				return 0;
			}
		}
	}
	return 0;
}
