uses math;
var i,j,k:longint; n,m,ans,t:int64;
    a:array[0..1000000,0..5]of int64;
    b:array[0..1000000]of int64;
procedure qsort(l,r:int64);
var i,j,x,y:int64;
begin
 i:=l;j:=r;x:=b[(l+r) div 2];
 repeat
  while b[i]<x do inc(i);
  while x<b[j] do dec(j);
  if not(i>j) then
  begin
   y:=b[i];b[i]:=b[j];b[j]:=y;inc(i);dec(j);
  end;
 until i>j;
 if l<j then qsort(l,j);
 if i<r then qsort(i,r);
end;
begin
 assign(input,'guard.in');
 assign(output,'guard.out');
 reset(input);
 rewrite(output);
 read(n,m);
 for i:=1 to n do
  for j:=1 to m do read(a[i,j]);
 if m=1 then
 begin
  for i:=1 to n do b[i]:=a[i,1];
  qsort(1,n);
  writeln(b[n]-b[1]);
 end
 else begin
 for i:=1 to min(2500,n) do
  for j:=i+1 to min(2500,n) do
  begin
   t:=0;
   for k:=1 to m do
    if a[i,k]>a[j,k] then t:=t+a[i,k]-a[j,k]
    else t:=t+a[j,k]-a[i,k];
   if t>ans then ans:=t;
  end;
 writeln(ans);
 end;
 close(input);
 close(output); 
end.