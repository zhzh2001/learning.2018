#include <bits/stdc++.h>
using namespace std;
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
const int maxn=1000008;
const int INF=0x3f3f3f3f;
int a[maxn];
char s[maxn];
struct qwq{
	int x,c;
};
inline void tense(int &x,int y){
	if (x<y)
		x=y;
}
inline void relax(int &x,int y){
	if (x>y)
		x=y;
}
struct atom{
	int mx,mxc,sx,mn,mnc,sn;
	atom(){
		mx=mxc=sx=-INF;
		mn=mnc=sn=INF;
	}
	atom& operator +=(const qwq& a){
		if (a.c==mxc)
			tense(mx,a.x);
		else{
			if (a.x>mx)
				sx=mx,mx=a.x,mxc=a.c;
			else tense(sx,a.x);
		}
		if (a.c==mnc)
			relax(mn,a.x);
		else{
			if (a.x<mn)
				sn=mn,mn=a.x,mnc=a.c;
			else relax(sn,a.x);
		}
		return *this;
	}
	atom& operator +=(const atom& a){
		if (mxc==a.mxc){
			tense(mx,a.mx);
			tense(sx,a.sx);
		}else{
			if (a.mx>mx){
				sx=mx;
				tense(sx,a.sx);
				mx=a.mx;
				mxc=a.mxc;
			}else tense(sx,a.mx);
		}
		if (mnc==a.mnc){
			relax(mn,a.mn);
			relax(sn,a.sn);
		}else{
			if (a.mn<mn){
				sn=mn;
				relax(sn,a.sn);
				mn=a.mn;
				mnc=a.mnc;
			}else relax(sn,a.mn);
		}
		return *this;
	}
};
struct bit{
	int n;
	atom a[maxn];
	void add(int p,qwq v){
		for(;p<=n;p+=p&-p)
			a[p]+=v;
	}
	atom query(int p){
		atom ans;
		for(;p;p-=p&-p)
			ans+=a[p];
		return ans;
	}
}b1;
struct tib{
	int n;
	atom a[maxn];
	void add(int p,qwq v){
		for(;p;p-=p&-p)
			a[p]+=v;
	}
	atom query(int p){
		atom ans;
		for(;p<=n;p+=p&-p)
			ans+=a[p];
		return ans;
	}
}b2;
struct dt{
	int p,x,y;
	bool operator <(const dt& rhs)const{
		return x<rhs.x;
	}
}d[maxn];
int main(){
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	int n=read();
	scanf("%s",s+1);
	for(int i=1;i<=n;i++)
		if (s[i]=='B')
			a[i]=0;
		else if (s[i]=='C')
			a[i]=1;
		else a[i]=2;
	int ans=0,tat=0;
	for(int i=1;i<=n;i++){
		if (i==1 || a[i]!=a[i-1])
			tat=0;
		tat++;
		ans=max(ans,tat);
	}
	int nowx=0,nowy=0; //a[1]-a[0],a[2]-a[1];
	int off=1,to=0;
	for(int i=1;i<=n;i++){
		if (a[i]==0)
			nowx--;
		else if (a[i]==1)
			nowx++,nowy--;
		else nowy++;
		off=max(off,-nowy+1);
		d[i].p=i,d[i].x=nowx,d[i].y=nowy;
	}
	sort(d,d+n+1);
	for(int i=0;i<=n;i++)
		to=max(to,d[i].y+off);
	b1.n=b2.n=to;
	for(int i=0;i<=n;i++){
		int id=d[i].y+off;
		atom now=b1.query(id-1);
		now+=b2.query(id+1);
		if (now.mxc!=(d[i].x+d[i].y))
			ans=max(ans,now.mx-d[i].p);
		else ans=max(ans,now.sx-d[i].p);
		if (now.mnc!=(d[i].x+d[i].y))
			ans=max(ans,d[i].p-now.mn);
		else ans=max(ans,d[i].p-now.sn);
		if (d[i].x!=d[i+1].x){
			for(int j=i;j>=0;j--){
				if (d[j].x!=d[i].x)
					break;
				qwq now;
				now.x=d[j].p;
				now.c=d[j].x+d[j].y;
				b1.add(d[j].y+off,now);
				b2.add(d[j].y+off,now);
			}
		}
	}
	printf("%d",ans);
//	fprintf(stderr,"%.5f\n",1.0*clock()/CLOCKS_PER_SEC);
}
