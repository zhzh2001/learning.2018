#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
const ll INF=0x3f3f3f3f3f3f3f3fLL;
int n,d;
ll a[200002][5];
ll work(int x){
	ll mx=-INF,mn=INF;
	for(int i=1;i<=n;i++){
		ll now=0;
		for(int j=0;j<d;j++)
			if (x>>j&1)
				now-=a[i][j];
			else now+=a[i][j];
		mx=max(mx,now);
		mn=min(mn,now);
	}
	return mx-mn;
}
int main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read(),d=read();
	for(int i=1;i<=n;i++)
		for(int j=0;j<d;j++)
			a[i][j]=read();
	ll ans=0;
	for(int i=0;i<(1<<d);i++)
		ans=max(ans,work(i));
	printf("%lld",ans);
}
