#include <bits/stdc++.h>
#include <windows.h>
using namespace std;
typedef long long ll;
ll get(int w){
	ll ans=0;
	while(w--)
		ans=ans*10+(rand()%10);
	return ans;
}
int main(){
	freopen("guard.in","w",stdout);
	srand(GetTickCount());
	int n=3000,d=rand()%5+1;
	printf("%d %d\n",n,d);
	while(n--){
		for(int i=0;i<d;i++){
			ll qwq=get(11);
			ll now=1LL<<31;
			qwq%=now;
			if (rand()%2)
				qwq=-qwq;
			printf("%lld ",qwq);
		}
		putchar('\n');
	}
}
