#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
ll a[200002][5];
int main(){
	freopen("guard.in","r",stdin);
	freopen("bf.out","w",stdout);
	int n,d;
	scanf("%d%d",&n,&d);
	for(int i=1;i<=n;i++)
		for(int j=0;j<d;j++)
			scanf("%lld",&a[i][j]);
	ll ans=0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++){
			ll now=0;
			for(int k=0;k<d;k++){
				if (a[i][k]>a[j][k])
					now+=a[i][k]-a[j][k];
				else now+=a[j][k]-a[i][k];
			}
			ans=max(ans,now);
		}
	printf("%lld",ans);
}
