#include <bits/stdc++.h>
using namespace std;
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
template<typename T>
struct heap{
	priority_queue<T> q1,q2;
	void push(T x){
		q1.push(x);
	}
	void del(T x){
		q2.push(x);
	}
	T top(){
		while(!q1.empty() && !q2.empty() && q1.top()==q2.top()){
//			fprintf(stderr,"WTF\n");
			q1.pop();
			q2.pop();
		}
//		fprintf(stderr,"WTF\n");
		if (q1.empty())
			return T();
//		fprintf(stderr,"WTF\n");
		T ans=q1.top();
//		fprintf(stderr,"ans.second=%d\n",ans.second);
		return ans;
	}
};
heap<pair<int,int> > q;
int cnt[10000002],a[100002];
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	int n=read(),m=read();
	for(int i=1;i<=n;i++)
		cnt[a[i]=read()]++;
	for(int i=0;i<=10000000;i++)
		if (cnt[i])
			q.push(make_pair(cnt[i],-i));
//	fprintf(stderr,"WTF\n");
	while(m--){
		int x=read(),y=read();
//		fprintf(stderr,"x=%d y=%d\n",x,y);
		q.del(make_pair(cnt[a[x]],-a[x]));
		cnt[a[x]]--;
		q.push(make_pair(cnt[a[x]],-a[x]));
//		fprintf(stderr,"WTF\n");
		a[x]=y;
		q.del(make_pair(cnt[y],-y));
		cnt[y]++;
		q.push(make_pair(cnt[y],-y));
//		fprintf(stderr,"WTF\n");
		int ans=-q.top().second;
		printf("%d\n",ans);
//		fprintf(stderr,"WTF\n");
	}
}
