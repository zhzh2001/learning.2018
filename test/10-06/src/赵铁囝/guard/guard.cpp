#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
#include <queue>
#include <cmath>

#define ll long long
#define Max 200005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

struct Node{
	int num[6];
}a[Max];

int n,d,k,now,size,id[Max],num[Max],l[33],r[33];
ll ans,sum,b[33][33],c[Max];
bool flag;
queue<int>que[33];

int main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read();d=read();
	for(int i=1;i<=n;i++){
		now=0;
		for(int j=1;j<=d;j++){
			a[i].num[j]=read();
			if(a[i].num[j]<0)now=now*2+1;else now=now*2;
			a[i].num[j]=abs(a[i].num[j]);
		}
		que[now].push(i);
	}
	k=(1<<d)-1;
	for(int i=0;i<=k;i++){
		l[i]=size+1;
		while(!que[i].empty()){
			num[++size]=que[i].front();
			que[i].pop();
		}
		r[i]=size;
	}
	memset(b,-0x3f,sizeof b);
	// cout<<(1&1)<<endl;
	for(int i=0;i<=k;i++){
		for(int j=0;j<=k;j++){
			for(int p=l[i];p<=r[i];p++){
				now=1;sum=0;
				for(int q=1;q<=d;q++){
					if((now&j)){
						sum-=a[num[p]].num[q];
						// cout<<i<<" "<<j<<" "<<sum<<endl;
					}else{
						sum+=a[num[p]].num[q];
					}
					now=now<<1;
					// cout<<now<<endl;
				}
				c[p]=sum;
				// cout<<i<<" "<<j<<" "<<p<<" "<<c[p]<<endl;
			}
			if(l[i]<=r[i]){
				sort(c+l[i],c+r[i]+1);
				b[i][j]=c[r[i]];
			}
		}
	}
	for(int i=0;i<=k;i++){
		for(int j=0;j<=k;j++){
			for(int p=0;p<=k;p++){
				for(int q=0;q<=k;q++){
					now=1;
					flag=false;
					for(int o=1;o<=d;o++){
						if((i&now)==(j&now)){
							if((p&now)==(q&now)){
								flag=true;
								break;
							}
						}else{
							if((p&now)!=(q&now)){
								flag=true;
								break;
							}
						}
						now=now<<1;
					}
					if(!flag){
						// if(ans<b[i][p]+b[j][q])
						// cout<<i<<" "<<p<<" "<<j<<" "<<q<<endl;
						ans=max(ans,b[i][p]+b[j][q]);
					}
				}
			}
		}
	}
	writeln(ans);
	return 0;
}
