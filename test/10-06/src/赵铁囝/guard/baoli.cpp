#include <iostream>
#include <cstdio>
#include <cmath>

#define ll long long
#define Max 5005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

struct Node{
	ll num[11];
}a[Max];

ll n,d,ans;

inline ll calc(ll x,ll y){
	ll sum=0;
	for(ll i=1;i<=d;i++){
		sum+=abs(a[x].num[i]-a[y].num[i]);
	}
	return sum;
}

int main(){
	freopen("guard.in","r",stdin);
	freopen("guard1.out","w",stdout);
	n=read();d=read();
	for(ll i=1;i<=n;i++){
		for(ll j=1;j<=d;j++){
			a[i].num[j]=read();
		}
	}
	for(ll i=1;i<n;i++){
		for(ll j=i+1;j<=n;j++){
			ans=max(ans,calc(i,j));
		}
	}
	writeln(ans);
	return 0;
}
