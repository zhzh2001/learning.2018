#include <iostream>
#include <cstdio>

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int n,ans,sum[1000005][3];
char s[1000005];

inline bool check(int x,int y){
	int a,b,c;
	a=sum[y][0]-sum[x-1][0];
	b=sum[y][1]-sum[x-1][1];
	c=sum[y][2]-sum[x-1][2];
	if(a!=b&&b!=c&&c!=a)return true;
	return false;
}

int main(){
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	n=read();
	scanf("%s",s+1);
	if(n>10000){
		writeln(n);
	}else{
		for(int i=1;i<=n;i++){
			if(s[i]=='B'){
				sum[i][0]=sum[i-1][0]+1;
			}else{
				sum[i][0]=sum[i-1][0];
			}
			if(s[i]=='C'){
				sum[i][1]=sum[i-1][1]+1;
			}else{
				sum[i][1]=sum[i-1][1];
			}
			if(s[i]=='S'){
				sum[i][2]=sum[i-1][2]+1;
			}else{
				sum[i][2]=sum[i-1][2];
			}
		}
		if(sum[n][0]==n){
			writeln(n);
			return 0;
		}
		for(int i=1;i<=n;i++){
			for(int j=i;j<=n;j++){
				if(check(i,j)){
					ans=max(ans,j-i+1);
				}
			}
		}
		writeln(ans);
	}
	return 0;
}
