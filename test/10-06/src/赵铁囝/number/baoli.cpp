#include <iostream>
#include <cstring>
#include <cstdio>

#define Max 1005

using namespace std;

int n,m,maxn,num,l,r,a[Max],vis[10005];

int main(){
	freopen("number.in","r",stdin);
	freopen("number1.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>m;
	for(int i=1;i<=n;i++)cin>>a[i];
	for(int i=1;i<=m;i++){
		cin>>l>>r;
		a[l]=r;
		maxn=0;
		memset(vis,0,sizeof vis);
		for(int j=1;j<=n;j++){
			vis[a[j]]++;
			if(vis[a[j]]==maxn){
				if(num>a[j])num=a[j];
			}
			if(vis[a[j]]>maxn){
				maxn=vis[a[j]];
				num=a[j];
			}
		}
		cout<<num<<endl;
	}
	return 0;
}
