#include <algorithm>
#include <iostream>
#include <cstdio>
#include <queue>

#define Max 1000005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Change{
	int id,num;
}b[Max];

struct Heap{
	int id,num;
	inline bool operator<(const Heap&x)const{
		if(num!=x.num){
			return num<x.num;
		}else{
			return id>x.id;
		}
	}
}top;

int n,m,size,a[Max],w[Max*2],id[10000005],num[Max*2],vis[Max*2];

priority_queue<Heap>que;

int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		num[++size]=a[i];
	}
	for(int i=1;i<=m;i++){
		b[i].id=read();b[i].num=read();
		num[++size]=b[i].num;
	}
	sort(num+1,num+size+1);
	w[1]=num[1];
	id[num[1]]=1;
	for(int i=2;i<=size;i++){
		if(num[i]!=num[i-1]){
			w[i]=num[i];
			id[num[i]]=i;
		}
	}
	for(int i=1;i<=n;i++){
		vis[id[a[i]]]++;
	}
	for(int i=1;i<=size;i++){
		que.push((Heap){i,vis[i]});
	}
	for(int i=1;i<=m;i++){
		vis[id[a[b[i].id]]]--;
		vis[id[b[i].num]]++;
		a[b[i].id]=b[i].num;
		que.push((Heap){id[b[i].num],vis[id[b[i].num]]});
		// cout<<id[b[i].num]<<" "<<vis[id[b[i].num]]<<endl;
		// cout<<id[a[b[i].id]]<<" "<<id[b[i].num]<<endl;
		top=que.top();
		que.pop();
		while(vis[top.id]!=top.num){
			top.num=vis[top.id];
			que.push(top);
			top=que.top();
			que.pop();
		}
		que.push(top);
		// cout<<top.num<<" "<<top.id<<endl;
		writeln(w[top.id]);
	}
	return 0;
}
