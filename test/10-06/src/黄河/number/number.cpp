#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define rep(x,a,b) for (int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 100010
using namespace std;
int read(){
int x=0,fh=1; char c=getchar();
  for(;!isdigit(c); c=getchar()) fh=(c=='-')?-1:fh;
  for(;isdigit(c); c=getchar()) x=(x<<3)+(x<<1)+(c^'0');
  return x*fh;
}
struct node{
	int w,id;
}tr[N*8];
int a[N*2],b[N*2],c[N*2],t[N*2],rx[N*2],ry[N*2],n,m,nm,cnt,w[N*2];
map<int,int>d;
inline void pushup(int u){
	if (tr[u<<1].w>=tr[u<<1|1].w) tr[u].w=tr[u<<1].w,tr[u].id=tr[u<<1].id;
		else if (tr[u<<1].w==tr[u<<1|1].w) {
			tr[u].w=tr[u<<1].w;
			if (a[tr[u<<1].w]<a[tr[u<<1|1].w]) tr[u].id=tr[u<<1].id;
				else tr[u].id=tr[u<<1|1].id;
		}
		else tr[u].w=tr[u<<1|1].w,tr[u].id=tr[u<<1|1].id;	
}
inline void build(int u,int l,int r){
	if(l==r){
		tr[u].w=w[l];tr[u].id=l;return;
	}
	int mid=(l+r)>>1;
	build(u<<1,l,mid);
	build(u<<1|1,mid+1,r);
	pushup(u);
}
inline void add(int u,int l,int r,int x,int z){
	if (l==r){
		tr[u].w+=z;return;
	}
	int mid=(l+r)>>1;
	if (x<=mid) add(u<<1,l,mid,x,z);
		else add(u<<1|1,mid+1,r,x,z);
	pushup(u);
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
    n=read(),m=read();
    rep(i,1,n) a[i]=read(),b[i]=a[i];
    rep(i,1,m) rx[i]=read(),ry[i]=read(),b[n+i]=ry[i];
    sort(b+1,b+n+m+1);
    b[0]=-1;
    rep(i,1,n+m)
    	if (b[i]!=b[i-1]){
    		c[++cnt]=b[i];
    		d[b[i]]=cnt;
		}
	rep(i,1,n) ++w[d[a[i]]];
	build(1,1,cnt);
    rep(i,1,m){
    	int x=rx[i],y=ry[i];
    	add(1,1,cnt,d[a[x]],-1);
    	add(1,1,cnt,d[y],1);
    	printf("%d\n",c[tr[1].id]);
    	a[x]=y;
	}
	return 0;
}
