#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 200010
using namespace std;
inline int read(){
int x=0,fh=1; char c=getchar();
  for(;!isdigit(c); c=getchar()) fh=(c=='-')?-1:fh;
  for(;isdigit(c); c=getchar()) x=(x<<3)+(x<<1)+(c^'0');
  return x*fh;
}
int Rand(int l,int r){
	int ss=(r-l+1);
	return ((rand()<<15)+rand())%ss+l;
}
int n,d,x[N][6],a[N];
LL s,ans;
int main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	srand(time(0)+10008820);
	n=read(),d=read();
	if (d==1){
		rep(i,1,n) a[i]=read();
		sort(a+1,a+n+1);
		ans=(a[n]*1LL-a[1]*1LL)*1LL;
		//printf("%lld\n",ans);
	}
	else if (n<=5000){
		ans=0;
		rep(i,1,n){
		rep(j,1,d) x[i][j]=read();
		}
		rep(i,1,n){
			rep(j,i+1,n){
				s=0;
				rep(k,1,d) 
					if (x[i][k]>x[j][k]) s+=(x[i][k]*1LL-x[j][k]*1LL)*1LL;
							else s+=(x[j][k]*1LL-x[i][k]*1LL)*1LL;
				if (s>ans) ans=s;
			}
		}		
	}
	else{
		ans=0;
		rep(i,1,n){
		rep(j,1,d) x[i][j]=read();
		}		
		int hh=(int)(5000000/d);
		rep(i,1,hh){
			int l=Rand(1,n),r=Rand(1,n);
			s=0;
			rep(k,1,d) 
				if (x[l][k]>x[r][k]) s+=(x[l][k]*1LL-x[r][k]*1LL)*1LL;
						else s+=(x[r][k]*1LL-x[l][k]*1LL)*1LL;
			if (s>ans) ans=s;
		}
	}
	printf("%lld\n",ans);
	return 0;
}

