#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 1010
using namespace std;
int read(){
int x=0,fh=1; char c=getchar();
  for(;!isdigit(c); c=getchar()) fh=(c=='-')?-1:fh;
  for(;isdigit(c); c=getchar()) x=(x<<3)+(x<<1)+(c^'0');
  return x*fh;
}
int n,ans,nc,nb,ns;
char s[1000010];
inline void task1(){
	ans=1;
	rep(l,1,n-1){
		bool flag=1;
		nc=nb=ns=0;
		if (s[l]=='C') nc++;
			else if (s[l]=='B') nb++;
				else ns++;
		rep(r,l+1,n){
			if (s[r]=='C') nc++;
				else if (s[r]=='B') nb++;
					else ns++;
			if (nc==nb||nb==ns||ns==nc) flag=0;
			if (nc!=nb&&nb!=ns&&ns!=nc) flag=1;
			if (nc!=0&&nb==0&&ns==0) flag=1;
			if (nc==0&&nb!=0&&ns==0) flag=1;
			if (nc==0&&nb==0&&ns!=0) flag=1;
			if ((flag)&&((r-l+1)>ans)) ans=r-l+1; 			
		}
	}
	printf("%d",ans);
	return;
}
int main(){
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	n=read();
	scanf("%s",s+1);
	if (n<=50000) task1();
	else{
		bool g=1;
		rep(i,1,n) if (s[i]!='B')g=0;
		if (g) printf("%d",n);
		return 0;
	}
	return 0;
}

