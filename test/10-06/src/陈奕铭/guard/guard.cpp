#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const ll N = 200005,M = 35;
ll Mi[M],n,k;
ll a[N][5];
ll b[N][M];
ll bin[6] = {1,2,4,8,16,32};
ll ans;

inline void Min(ll &x,ll y){if(x > y)x = y;}
inline void Max(ll &x,ll y){if(x < y)x = y;}

signed main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n = read(); k = read();
	memset(Mi,0x3f,sizeof Mi);
	for(ll i = 1;i <= n;++i){
		for(ll j = 0;j < k;++j) a[i][j] = read();
		for(ll j = 0;j < bin[k];++j){
			ll sum = 0;
			for(ll z = 0;z < k;++z)
				if((j&bin[z]) == bin[z])
					sum += a[i][z];
				else sum -= a[i][z];
			Min(Mi[j],sum);
			b[i][j] = sum;
		}
	}
	// for(ll j = 0;j < bin[k];++j) printf("%lld ",Mi[j]); puts("");
	ans = 0;
	for(ll i = 1;i <= n;++i){
		for(ll j = 0;j < bin[k];++j){
			// printf("%lld ",b[i][j]);
			Max(ans,b[i][j]-Mi[j]);
		}
	}
	printf("%lld\n", ans);
	return 0;
}