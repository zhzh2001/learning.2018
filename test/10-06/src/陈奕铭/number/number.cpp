#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005,M = 200005;
int n,m,cnt;
int d[N*2],a[N];
int sum[M];
struct node{
	int x,y;
}op[N];
int Mx[M*4];

inline int erfen(int x){
	int l = 1,r = cnt,mid;
	while(l <= r){
		mid = (l+r)>>1;
		if(d[mid] == x) return mid;
		if(d[mid] < x) l = mid+1;
		else r = mid-1;
	}
	return l;
}

inline void update(int k){
	int l = k<<1,r = k<<1|1;
	if(sum[Mx[l]] >= sum[Mx[r]]) Mx[k] = Mx[l];
	else Mx[k] = Mx[r];
}

void build(int num,int l,int r){
	if(l == r){
		Mx[num] = l;
		return;
	}
	int mid = (l+r)>>1;
	build(num<<1,l,mid);
	build(num<<1|1,mid+1,r);
	update(num);
}

void change(int num,int l,int r,int x){
	if(l == r) return;
	int mid = (l+r)>>1;
	if(x <= mid) change(num<<1,l,mid,x);
	else change(num<<1|1,mid+1,r,x);
	update(num);
}

signed main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n = read(); m = read();
	for(int i = 1;i <= n;++i) a[i] = read(),d[i] = a[i];
	for(int i = 1;i <= m;++i){
		op[i].x = read(); op[i].y = read();
		d[i+n] = op[i].y;
	}
	sort(d+1,d+n+m+1);
	d[0] = -1;
	for(int i = 1;i <= n+m;++i)
		if(d[i] != d[i-1]) d[++cnt] = d[i];
	for(int i = 1;i <= n;++i){
		a[i] = erfen(a[i]);
		++sum[a[i]];
	}
	build(1,1,cnt);
	for(int i = 1;i <= m;++i){
		--sum[a[op[i].x]];
		change(1,1,cnt,a[op[i].x]);
		a[op[i].x] = erfen(op[i].y);
		++sum[a[op[i].x]];
		change(1,1,cnt,a[op[i].x]);
		printf("%d\n", d[Mx[1]]);
	}
	return 0;
}
