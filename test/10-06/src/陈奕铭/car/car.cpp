#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 1000005,M = 1000000;
// int nxt[N];
char s[N];
int n;
int sum1,sum2,sum3;
int ans;
int a[N],b[N],c[N];

// inline void getfail(){
// 	for(int p = 0,i = 2;i <= n;++i){
// 		while(p && s[i] != s[p+1]) p = nxt[p];
// 		if(s[i] == s[p+1]) ++p;
// 		nxt[i] = p;
// 	}
// }

inline void Max(int &x,int y){if(x < y) x = y;}

signed main(){
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	n = read();
	scanf("%s",s+1);
	int p = 0;
	for(int i = 1;i <= n;++i){
		if(s[i] == s[i-1]) ++p;
		else p = 1;
		Max(ans,p);
	}
	for(int i = 1;i <= n;++i){
		a[i] = a[i-1]; b[i] = b[i-1]; c[i] = c[i-1];
		if(s[i] == 'B') ++a[i];
		else if(s[i] == 'C') ++b[i];
		else ++c[i];
		if(a[i] != b[i] && b[i] != c[i] && c[i] != a[i]) Max(ans,i);
	}
	if(n >= 10000){
		for(int i = 1;i <= 5000;++i){
			if(ans >= n-i) break;
			for(int j = 0;j <= i;++j){
				int A = a[n-i+j]-a[j];
				int B = b[n-i+j]-b[j];
				int C = c[n-i+j]-c[j];
				if(A != B && B != C && A != C){
					Max(ans,n-i);
					break;
				}
			}
		}
	}else{
		for(int i = 1;i <= n;++i){
			for(int j = i;j <= n;++j){
				int A = a[j]-a[i-1];
				int B = b[j]-b[i-1];
				int C = c[j]-c[i-1];
				if(A != B && B != C && A != C)
					Max(ans,j-i+1);
			}
		}
	}
	printf("%d\n", ans);
	return 0;
}