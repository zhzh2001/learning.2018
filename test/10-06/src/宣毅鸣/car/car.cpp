#include<bits/stdc++.h>
using namespace std;
const int N=1e6+5;
int n,cnt[N][3],ans;
char s[N];
int main(){
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	scanf("%d%s",&n,s+1);
	for (int i=1;i<=n;i++){
		for (int j=0;j<3;j++)cnt[i][j]=cnt[i-1][j];
		if (s[i]=='B')cnt[i][0]++;
		if (s[i]=='S')cnt[i][1]++;
		if (s[i]=='C')cnt[i][2]++;
	}
	if (cnt[n][0]==n){
		printf("%d\n",n);
		return 0;
	}
	for (int i=n;i;i--)
		for (int j=0;j<=i-ans-1;j++)
			if (cnt[i][0]-cnt[j][0]!=cnt[i][1]-cnt[j][1]&&
			cnt[i][1]-cnt[j][1]!=cnt[i][2]-cnt[j][2]&&
			cnt[i][2]-cnt[j][2]!=cnt[i][0]-cnt[j][0])ans=max(ans,i-j);
	printf("%d",ans);	
	return 0;	
}
