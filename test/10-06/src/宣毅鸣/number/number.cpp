#include<bits/stdc++.h>
using namespace std;
const int N=1e6+5;
int b[N],a[N],x[N],y[N],n,f[N],d[N],tot,num,g[N],m;
int ef(int x){
	int l=1,r=num;
	while (l<r){
		int mid=(l+r)/2;
		if (b[mid]<x)l=mid+1;
		else r=mid;
	}
	return l;
}
void up(int x){
	if (x==1)return;
	if (f[d[x]]>f[d[x/2]]||(f[d[x]]==f[d[x/2]]&&d[x]<d[x/2])){
		swap(g[d[x]],g[d[x/2]]);
		swap(d[x],d[x/2]);
		up(x/2);
	}
}
void down(int x){
	int i=x;
	if (x*2<=num&&(f[d[x*2]]>f[d[x]]||(f[d[x*2]]==f[d[x]]&&d[x*2]<d[x])))i=x*2;
	if (x*2<num&&(f[d[x*2+1]]>f[d[i]]||(f[d[i]]==f[d[x*2+1]]&&d[x*2+1]<d[i])))i=x*2+1;
	if (i!=x){
		swap(d[i],d[x]);
		swap(g[d[i]],g[d[x]]);
		down(i);
	}
}
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)scanf("%d",&a[i]),b[++tot]=a[i];
	for (int i=1;i<=m;i++)scanf("%d%d",&x[i],&y[i]),b[++tot]=y[i];
	sort(b+1,b+tot+1);
	num=1;
	for (int i=2;i<=tot;i++)
		if (b[num]!=b[i])b[++num]=b[i];
	for (int i=1;i<=n;i++)a[i]=ef(a[i]),f[a[i]]++;
	for (int i=1;i<=num;i++)d[i]=g[i]=i;
	for (int i=num;i;i--)down(i);
	for (int i=1;i<=m;i++){
		int xx=ef(y[i]);
		f[a[x[i]]]--;
		down(g[a[x[i]]]);
		a[x[i]]=xx;
		f[a[x[i]]]++;
		up(g[a[x[i]]]);
		printf("%d\n",b[d[1]]);
	}	
	return 0;
}
