#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=200005,M=32;
int n,m,a[N][M],b[N][M],f[M];
int read(){
	int x=0,flag=1;char c;
	for (;c<'0'||c>'9';c=getchar())
		if (c=='-')flag=-1;
	for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
	return x*flag;
}
signed main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=0;j<m;j++)a[i][j]=read();
	for (int i=0;i<1<<m;i++)f[i]=-1e18;	
	for (int i=1;i<=n;i++)
		for (int j=0;j<1<<m;j++){
			int s=0;
			for (int k=0;k<m;k++)
				if (j&(1<<k))s+=a[i][k];
				else s-=a[i][k];
			f[j]=max(f[j],s);
			b[i][j]=s;	
		}	
	int ans=-1e18;	
	for (int i=1;i<=n;i++)
		for (int j=0;j<1<<m;j++)ans=max(ans,f[j^((1<<m)-1)]+b[i][j]);
	printf("%lld",ans);
	return 0;	
}
