#include<bits/stdc++.h>
#include<set>
#define pa pair<int,int>
#define mk make_pair
#define fi first
#define se second
#define ll long long
#define ull unsigned long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
using namespace std;
const int N=100005;
const int M=10000005;
int a[N];
set<pa>s;
int n,m;
int t;
int b[M];
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		b[a[i]]++;
	}
	for(int i=1;i<=n;i++)
		s.insert(mk(b[a[i]],M-a[i]));
	for(int i=1;i<=m;i++){
		int x,y;
		x=read();y=read();
		s.erase(mk(b[a[x]],M-a[x]));
		b[a[x]]--;
		if(b[a[x]]!=0)s.insert(mk(b[a[x]],M-a[x]));
		a[x]=y;
		if(b[a[x]]!=0)s.erase(mk(b[a[x]],M-a[x]));
		b[a[x]]++;
		s.insert(mk(b[a[x]],M-a[x]));
		set<pa>::iterator it=s.end();
		it--;
		pa k=*it;
		writeln(M-k.se);
	}
	return 0;
}
