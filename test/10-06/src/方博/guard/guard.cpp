#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=200005;
struct xxx{
	ll a,b,c,d,e;
	ll sum;
}a[N];
ll n,d;
ll ans;
bool com(xxx a,xxx b)
{
	return a.sum<b.sum;
}
int main()
{
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read();d=read();
	for(int i=1;i<=n;i++){
		a[i].a=read();
		if(d>1)a[i].b=read();
		else a[i].b=0;
		if(d>2)a[i].c=read();
		else a[i].c=0;
		if(d>3)a[i].d=read();
		else a[i].d=0;
		if(d>4)a[i].e=read();
		else a[i].e=0;
	}
	for(int i=-1;i<=1;i+=2)
		for(int j=-1;j<=1;j+=2)
			for(int k=-1;k<=1;k+=2)
				for(int l=-1;l<=1;l+=2)
					for(int m=1;m<=1;m+=2){
						for(int z=1;z<=n;z++)
							a[z].sum=a[z].a*i+a[z].b*j+a[z].c*k+a[z].d*l+a[z].e*m;
						sort(a+1,a+n+1,com);
						ans=max(abs(a[1].a-a[n].a)+abs(a[1].b-a[n].b)+abs(a[1].c-a[n].c)+abs(a[1].d-a[n].d)+abs(a[1].e-a[n].e),ans);
						for(int z=2;z<=n-1;z++){
							ans=max(abs(a[1].a-a[z].a)+abs(a[1].b-a[z].b)+abs(a[1].c-a[z].c)+abs(a[1].d-a[z].d)+abs(a[1].e-a[z].e),ans);
							ans=max(abs(a[n].a-a[z].a)+abs(a[n].b-a[z].b)+abs(a[n].c-a[z].c)+abs(a[n].d-a[z].d)+abs(a[1].e-a[z].e),ans);
						}
					}
	writeln(ans);
}
