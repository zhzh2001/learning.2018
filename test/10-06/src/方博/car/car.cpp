#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
int n;
string s;
const int N=1000005;
int f[N][3];//0-B 1-C 2-S
int ans;
int main()
{
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	cin>>n>>s;
	for(int i=1;i<=n;i++){
		f[i][0]=f[i-1][0];
		f[i][1]=f[i-1][1];
		f[i][2]=f[i-1][2];
		if(s[i-1]=='B')f[i][0]++;
		else if(s[i-1]=='C')f[i][1]++;
		else if(s[i-1]=='S')f[i][2]++;
	}
	if(f[n][0]==n||f[n][1]==n||f[n][2]==n){
		writeln(n);
		return 0;
	}
	if(n<=10000){
		for(int i=1;i<=n;i++)
			for(int j=i;j<=n;j++){
				if(j-i+1<=ans)continue;
				int a,b,c;
				a=f[j][0]-f[i-1][0];
				b=f[j][1]-f[i-1][1];
				c=f[j][2]-f[i-1][2];
				if(a!=b&&b!=c&&c!=a||(a==0&&b==0)||(a==0&&c==0)||(b==0&&c==0))ans=max(ans,j-i+1);
			}
		writeln(ans);
	}
	else{
		for(int i=1;i<=1000;i++)
			for(int j=n-1000;j<=n;j++){
				if(j-i+1<=ans)continue;
				int a,b,c;
				a=f[j][0]-f[i-1][0];
				b=f[j][1]-f[i-1][1];
				c=f[j][2]-f[i-1][2];
				if(a!=b&&b!=c&&c!=a||(a==0&&b==0)||(a==0&&c==0)||(b==0&&c==0))ans=max(ans,j-i+1);
			}
		writeln(ans);
	}
}
