#include<fstream>
#include<random>
#include<ctime>
using namespace std;
ofstream fout("number.in");
const int n=1e5,m=1e7;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<n<<endl;
	uniform_int_distribution<> dn(1,n),dm(0,m);
	for(int i=1;i<=n;i++)
		fout<<dm(gen)<<' ';
	fout<<endl;
	for(int i=1;i<=n;i++)
		fout<<dn(gen)<<' '<<dm(gen)<<endl;
	return 0;
}
