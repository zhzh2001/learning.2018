#include <bits/stdc++.h>
#define int long long
#define dis(x,y) abs(a[x][1]-a[y][1])+abs(a[x][2]-a[y][2])+abs(a[x][3]-a[y][3])+abs(a[x][4]-a[y][4])+abs(a[x][5]-a[y][5])
#define gc getchar
using namespace std;
//ifstream fin("guard.in");
//ofstream fout("guard.out");
inline int read() {
	int x=0,w=0;
	char ch=0;
	while(!isdigit(ch)) {
		w|=ch=='-';
		ch=gc();
	}
	while(isdigit(ch)) x=(x<<3)+(x<<1)+(ch^48),ch=gc();
	return w?-x:x;
}
inline void write(int x) {
	if(x<0) putchar('-'),x=-x;
	if(x>9) write(x/10);
	putchar(x%10+'0');
}
int n,num,a[230000][6],mx,sxd,tim,x,y;
signed main() {
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	srand((int)time(0));
	tim=clock();
	n=read();
	num=read();
	for(int i=1; i<=n; i++)
		for(int j=1; j<=num; j++) {
			a[i][j]=read();
		}
	if(n<=5000) {
		for(int i=1; i<=n; i++)
			for(int j=i+1; j<=n; j++)
				mx=max(mx,dis(i,j));
		write(mx);
		return 0;
	} else {
		while(clock()-tim<=1600) {
			x=(rand()+n)%n+1;
			y=(rand()+n)%n+1;;
			mx=max(mx,dis(x,y));
			sxd++;
		}
		write(sxd);
		puts("");
		write(mx);
		return 0;
	}
}
