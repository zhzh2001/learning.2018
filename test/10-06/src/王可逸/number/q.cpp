#include <bits/stdc++.h>
using namespace std;
int a[120000],n,m;
map <int,int> mp;
set <int> q;
inline int read() {
	int x=0,w=0;
	char ch=0;
	while(!isdigit(ch)) {
		w|=ch=='-';
		ch=getchar();
	}
	while(isdigit(ch)) x=(x<<3)+(x<<1)+(ch^48),ch=getchar();
	return w?-x:x;
}
inline void write(int x) {
	if(x<0) putchar('-'),x=-x;
	if(x>9) write(x/10);
	putchar(x%10+'0');
}
int main() {
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);	
	n=read();
	m=read();
	for(int i=1;i<=n;i++) {
		a[i]=read();
		mp[a[i]]++;
		q.insert(a[i]);
	}
	int x,y,mx,mxat,w;
	while(m--) {
		x=read();
		y=read();	
		mp[a[x]]--;
		mp[y]++;
		q.insert(y);
		mx=0;
		for(set <int> :: iterator it=q.begin();it!=q.end();it++) {
			w=*it;	
			if(mp[w]>mx || (mp[w]==mx && w<mx)) {
				mx=mp[w];
				mxat=w;	
			}
		}
		a[x]=y;
		write(mxat);
		puts("");
	}
}
