#include <bits/stdc++.h>
using namespace std; 
ifstream fin("number.in");
ofstream fout("number.out");
int n,m,mx,sec,thi,a[120000],x,y;
map <int,int> mp;
signed main() {
	fin>>n>>m;
	for(int i=1;i<=n;i++) {
		fin>>a[i];
		mp[a[i]]++;
		if(mp[a[i]]>mp[mx] || (mp[a[i]]==mp[mx] && a[i]<mx)) {
			thi=sec;
			sec=mx;
			mx=a[i];
		} else if ((a[i]!=mx) && (mp[a[i]]>mp[sec] || (mp[a[i]]==mp[sec] && a[i]<sec) )){
			thi=sec;
			sec=a[i];
		} else if( (a[i]!=sec && a[i]!=mx) && (mp[a[i]]>mp[thi] || (mp[a[i]]==mp[thi] && a[i]<thi))) {
			thi=a[i];
		}
	} 
	while(m--){
		fin>>x>>y;
		mp[a[x]]--;
		if(mp[sec] > mp[mx] || (mp[sec]==mp[mx] && sec<mx)) {
			swap(mx,sec);
		}
		if(mp[sec] < mp[thi] || (mp[sec] == mp[thi] && sec>thi)) {
			swap(sec,thi);
		}
		mp[y]++;
		if((y!=sec && y!=thi) && (mp[y] > mp[mx] || (mp[y]==mp[mx] && y<mx))) {
			thi=sec;
			sec=mx;
			mx=y;
		} else if((y!=mx && y!=thi) && (mp[y] > mp[sec] || (mp[y]==mp[sec] && y<sec))) {
			thi=sec;
			sec=y;
		} else if((y!=mx && y!=sec) && (mp[y] > mp[thi] || (mp[y]==mp[thi] && y<thi))) {
			thi=y;
		}
		if(mp[thi] > mp[sec] || (mp[sec]==mp[thi] && sec>thi)) {
			swap(sec,thi);
		}
		if(mp[sec] > mp[mx] || (mp[sec]==mp[mx] && sec<mx)) {
			swap(mx,sec);
		}
		a[x]=y;
		fout<<mx<<'\n';
	}
	return 0;
}
