#pragma GCC optimize(2)
#include <bits/stdc++.h>
using namespace std;
ifstream fin("car.in");
ofstream fout("car.out");
char a[1200000];
int n,x[1200000],y[1200000],z[1200000],tim,w,sxd;
bool b_only=true;
int main() {
	tim=clock();
	srand((int)time(0));
	fin>>n;
	fin>>a;
	for(int i=0; i<n; i++) {
		x[i+1]=x[i];
		y[i+1]=y[i];
		z[i+1]=z[i];
		if(a[i]!='B') {
			b_only=false;
		}
		if(a[i]=='C') {
			x[i+1]=x[i]+1;
		} else if(a[i]=='B') {
			y[i+1]=y[i]+1;
		} else if(a[i]=='S') {
			z[i+1]=z[i]+1;
		}
	}
	if(b_only) {
		fout<<n;
		return 0;
	}
	int mx=0;
	if(n<=30000) {
		for(int i=1; i<=n; i++) {
			for(int j=n; j>=i; j--) {
				if(x[j]-x[i-1]!=y[j]-y[i-1] && x[j]-x[i-1]!=z[j]-z[i-1] && y[j]-y[i-1]!=z[j]-z[i-1]) {
					mx=max(mx,j-i+1);
					break;
				}
			}
		}
		fout<<mx;
	} else {
		int i,j;
		while(clock()-tim<=2700) {
			i=(rand()+n)%n+1;
			j=(rand()+n)%n+1;
			if(x[j]-x[i-1]!=y[j]-y[i-1] && x[j]-x[i-1]!=z[j]-z[i-1] && y[j]-y[i-1]!=z[j]-z[i-1]) {
				mx=max(mx,j-i+1);
				sxd++;
			}
		}
//		fout<<sxd<<'\n';
		fout<<mx;
	}
	return 0;
}
