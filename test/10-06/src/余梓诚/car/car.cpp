#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

const int maxn=11000;
int a[maxn],sum[3][maxn],ans,n;
char s[maxn];

void judge(){
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
}

int main(){
	judge();
	scanf("%d%s",&n,s+1);
	for (int i=1;i<=n;i++){
		for (int j=0;j<3;j++) sum[j][i]=sum[j][i-1];
		if (s[i]=='B') sum[0][i]++;
		if (s[i]=='C') sum[1][i]++;
		if (s[i]=='S') sum[2][i]++;
	}
	if (sum[2][n]==n||sum[0][n]==n||sum[1][n]==n) return printf("%d\n",n),0;
	for (int len=n;len>=1;len--){
		if (len<=ans) break;
		for (int l=1;l+len-1<=n;l++){
			int r=l+len-1;
			int t1=sum[0][r]-sum[0][l-1],t2=sum[1][r]-sum[1][l-1],t3=sum[2][r]-sum[2][l-1];
			if ((t1==0&&t2==0)||(t2==0&&t3==0)||(t1==0&&t3==0)) ans=len;
			else if (t1!=t2&&t2!=t3&&t1!=t3) ans=len;
		}
	}
	printf("%d\n",ans);
	return 0;
}
