#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

const int maxn=210000;
const long long inf=1e17;
int n,D,a[6][maxn];
long long ans,Max,Min;

inline void read(int &x){
	char ch=getchar();
	int fg=1; x=0;
	while (!isdigit(ch)){
		if (ch=='-') fg=-1;
		ch=getchar();
	}
	while (isdigit(ch)){
		x=x*10+ch-'0';
		ch=getchar();
	}
	x*=fg;
}

void judge(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
}

void calc(int s,int x){
	long long res=0;
	for (int i=1;i<=D;i++){
		if (!(s&(1<<(i-1)))) res-=a[i][x];
		else res+=a[i][x];
	}
	if (res<Min) Min=res;
	if (res>Max) Max=res;
}

int main(){
	judge();
	read(n),read(D);
	for (int i=1;i<=n;i++){
		for (int j=1;j<=D;j++) read(a[j][i]);
	}
	int Num=(1<<D)-1;
	for (int s=0;s<=Num;s++){
		Max=-inf,Min=inf;
		for (int i=1;i<=n;i++) calc(s,i);
		if (Max-Min>ans) ans=Max-Min;
	}
	printf("%lld\n",ans);
	return 0;
}
