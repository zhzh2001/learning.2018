#include <cstdio>
#include <vector>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const int maxn=110000;
int n,m,root,a[maxn],opt,num[maxn<<2],lson[maxn<<2],rson[maxn<<2],Max[maxn<<2];

void judge(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
}

void change(int k){
	if (Max[lson[k]]>Max[rson[k]]) num[k]=num[lson[k]];
	else if (Max[rson[k]]>Max[lson[k]]) num[k]=num[rson[k]];
	else num[k]=min(num[lson[k]],num[rson[k]]);
	Max[k]=max(Max[lson[k]],Max[rson[k]]);
}
void change(int &k,int l,int r,int q,int p){
	num[!k?k=++opt:k=k]=l;
	if (l==r){
		Max[k]+=p;
		return;
	}
	int mid=l+r>>1;
	if (q<=mid) change(lson[k],l,mid,q,p);
	else change(rson[k],mid+1,r,q,p);
	change(k);
}

int main(){
	judge();
	scanf("%d%d",&n,&m);
	int N=10000000;
	for (int i=1;i<=n;i++) scanf("%d",&a[i]),change(root,0,N,a[i],1);
	for (int i=1;i<=m;i++){
		int x,y; scanf("%d%d",&x,&y);
		change(root,0,N,a[x],-1);
		change(root,0,N,y,1);
		a[x]=y;
		printf("%d\n",num[root]);
	}
	return 0;
}
