#include <bits/stdc++.h>
#define N 1000020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
std::ofstream fout("car.in");
int a[N];
int main(int argc, char const *argv[]) {
  srand(time(0));
  int n = 1000000;
  fout << n << endl;
  int b = n / 3, s = n / 3, c = n - b - s;
  for (int i = b + 1; i <= b + s; ++ i) {
    a[i] = 1;
  }
  for (int i = b + s + 1; i <= n; ++ i) {
    a[i] = 2;
  }
  for (int i = 2; i <= n; ++ i) {
    swap(a[i], a[rand()*rand()%(i-1)+1]);
  }
  for (int i = 1; i <= n; ++ i) {
    fout << "BSC"[a[i]];
  }
  fout << endl;
  return 0;
}