#include <bits/stdc++.h>
#define N 1000020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

int B[N], C[N], S[N];
inline int queryB(int l, int r) { return B[r] - B[l - 1]; }
inline int queryS(int l, int r) { return S[r] - S[l - 1]; }
inline int queryC(int l, int r) { return C[r] - C[l - 1]; }

int f[N];
char str[N];

int main(int argc, char const *argv[]) {
  freopen("car.in", "r", stdin);
  freopen("car.out", "w", stdout);

  int n = read();
  scanf("%s", str + 1);

  for (int i = 1; i <= n; ++ i) {
    if (str[i] == 'B') ++ B[i]; B[i] += B[i - 1];
    if (str[i] == 'S') ++ S[i]; S[i] += S[i - 1];
    if (str[i] == 'C') ++ C[i]; C[i] += C[i - 1];
  }

  // special judge abcabcabc...

  bool isBoy = str[1] != str[2] && str[1] != str[3] && str[2] != str[3];
  if (isBoy) {
    bool nvzhuang = true;
    for (int i = 4; i <= n; ++ i) {
      nvzhuang &= str[i] == str[i - 3];
    }
    if (nvzhuang) {
      return puts("1"), 0;
    }
  }

  int ans = 0;
  for (int i = 1; i <= n; ++ i) {
    f[i] = str[i] == str[i - 1] ? f[i - 1] + 1 : 1;
    ans = max(ans, f[i]);
  }

  for (int i = 1; i <= n; ++ i) {
    int b = queryB(i, n);
    int s = queryS(i, n);
    int c = queryC(i, n);
    for (int j = n; j >= i + ans; -- j) {
      if (b != s && s != c && b != c) {
        ans = max(ans, j - i + 1);
        break;
      }
      if (str[j] == 'B') -- b;
      if (str[j] == 'S') -- s;
      if (str[j] == 'C') -- c;
    }
  }

  printf("%d\n", ans);

  return 0;
}
