#pragma GCC optimize("Ofast")
#include <bits/stdc++.h>
#define N 200020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
long long p[N][5];
int n, k;
inline long long calc(int x, int y) {
  long long ans = 0;
  for (int i = 0; i < k; ++ i) {
    ans += llabs(p[x][i] - p[y][i]);
  }
  return ans;
}
int main(int argc, char const *argv[]) {
  freopen("guard.in", "r", stdin);
  freopen("guard.ans", "w", stdout);

  n = read(); k = read();
  for (int i = 1; i <= n; ++ i) {
    for (int j = 0; j < k; ++ j) {
      p[i][j] = read();
    }
  }

  long long ans = 0;

  for (int i = 1; i <= n; ++ i) {
    for (int j = i + 1; j <= n; ++ j) {
      ans = max(ans, calc(i, j));
    }
  }

  printf("%lld\n", ans);
  // fprintf(stderr, "%dms\n", clock());

  return 0;
}