#pragma GCC optimize("Ofast")
// %%% this is a little bit fake... %%%
#include <bits/stdc++.h>
#define N 200020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
long long p[N][5];
int id[N];
int COMPARE_INDEX;
int ITERATE_BOUND = 200;
bool cmp(const int &a, const int &b) {
  return p[a][COMPARE_INDEX] < p[b][COMPARE_INDEX];
}
int q[N], top;
int n, k;
inline long long calc(int x) {
  long long ans = 0;
  for (int i = 1; i <= n; ++ i) {
    long long res = 0;
    for (int j = 0; j < k; ++ j) {
      res += llabs(p[x][j] - p[i][j]);
    }
    ans = max(ans, res);
  }
  return ans;
}
int main(int argc, char const *argv[]) {
  freopen("guard.in", "r", stdin);
  freopen("guard.out", "w", stdout);

  n = read(); k = read();
  for (int i = 1; i <= n; ++ i) {
    for (int j = 0; j < k; ++ j) {
      p[i][j] = read();
    }
    id[i] = i;
  }

  for (COMPARE_INDEX = 0; COMPARE_INDEX < k; ++ COMPARE_INDEX) {
    sort(id + 1, id + n + 1, cmp);
    for (int i = 1; i <= min(ITERATE_BOUND, n); ++ i) {
      q[++ top] = id[i];
    }
    for (int i = max(n - ITERATE_BOUND, 1); i <= n; ++ i) {
      q[++ top] = id[i];
    }
  }

  sort(q + 1, q + top + 1);
  top = unique(q + 1, q + top + 1) - q - 1;

  long long ans = 0;
  for (int i = 1; i <= top; ++ i) {
    ans = max(ans, calc(q[i]));
  }

  printf("%lld\n", ans);
  fprintf(stderr, "%dms\n", clock());

  return 0;
}