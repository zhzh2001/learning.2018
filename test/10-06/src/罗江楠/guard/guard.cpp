#pragma GCC optimize("Ofast")
// %%% timber is akking %%%
#include <bits/stdc++.h>
#define N 200020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
long long p[N][5];
int id[N];
int COMPARE_INDEX;
int ITERATE_BOUND = 2000;
bool cmp(const int &a, const int &b) {
  return p[a][COMPARE_INDEX] < p[b][COMPARE_INDEX];
}
int q1[N], top1, q2[N], top2;
int n, k;
inline long long calc(int x, int y) {
  long long ans = 0;
  for (int i = 0; i < k; ++ i) {
    ans += llabs(p[x][i] - p[y][i]);
  }
  return ans;
}
int main(int argc, char const *argv[]) {
  freopen("guard.in", "r", stdin);
  freopen("guard.out", "w", stdout);

  n = read(); k = read();
  for (int i = 1; i <= n; ++ i) {
    for (int j = 0; j < k; ++ j) {
      p[i][j] = read();
    }
    id[i] = i;
  }

  long long ans = 0;

  for (COMPARE_INDEX = 0; COMPARE_INDEX < k; ++ COMPARE_INDEX) {
    sort(id + 1, id + n + 1, cmp);
    top1 = top2 = 0;
    for (int i = 1; i <= min(ITERATE_BOUND, n); ++ i) {
      q1[++ top1] = id[i];
    }
    for (int i = max(n - ITERATE_BOUND, 1); i <= n; ++ i) {
      q2[++ top2] = id[i];
    }

    for (int i = 1; i <= top1; ++ i) {
      for (int j = 1; j <= top2; ++ j) {
        ans = max(ans, calc(q1[i], q2[j]));
      }
    }
  }

  printf("%lld\n", ans);
  // fprintf(stderr, "%dms\n", clock());

  return 0;
}