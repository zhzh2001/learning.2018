#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
ofstream fout("guard.in");
int main(int argc, char const *argv[]) {
  srand(time(0));
  int n = 10000, d = 5;
  fout << n << " " << d << endl;
  for (int i = 1; i <= n; ++ i) {
    for (int j = 1; j <= d; ++ j) {
      fout << rand() * rand() << " ";
    }
    fout << endl;
  }
  return 0;
}