#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

map<int, int> mp;
set<int> st[N];

int mx;

inline void del(int x) {
  st[mp[x] --].erase(x);
  st[mp[x]].insert(x);

  if (!st[mx].size()) {
    -- mx;
  }
}

inline void add(int x) {
  st[mp[x] ++].erase(x);
  st[mp[x]].insert(x);

  mx = max(mx, mp[x]);
}

int a[N];

int main(int argc, char const *argv[]) {
  freopen("number.in", "r", stdin);
  freopen("number.out", "w", stdout);

  int n = read(), m = read();
  for (int i = 1; i <= n; ++ i) {
    a[i] = read();
    ++ mp[a[i]];
  }

  for (map<int, int>::iterator it = mp.begin(); it != mp.end(); ++ it) {
    st[it -> second].insert(it -> first);
    mx = max(mx, it -> second);
  }

  for (int i = 1; i <= m; ++ i) {
    int x = read(), v = read();
    del(a[x]);
    add(a[x] = v);

    printf("%d\n", *st[mx].begin());
  }

  return 0;
}