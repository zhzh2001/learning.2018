#include <cstdio>
#include <algorithm>
using namespace std;
#define ll long long
const int N=2e5+7;
const int mx[2]={-1,1};
int n,D;
int w[4];
ll ans;
struct node {
	ll x[5];
	bool operator <(node a) const {
		return x[D-1]<a.x[D-1];
	}
}p[N];
void solve(int x) {
	for (int i=0;i<D-1;i++) w[i]=x&1,x>>=1;
	ll mn=1e15;
	for (int i=1;i<=n;i++) {
		ll v=p[i].x[D-1];
		for (int j=0;j<D-1;j++) v+=mx[w[j]]*p[i].x[j];
		ans=max(ans,v-mn);
		mn=min(mn,v);
	}
}
int main() {
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	scanf("%d%d",&n,&D);
	for (int i=1;i<=n;i++) {
		for (int j=0;j<D;j++) scanf("%lld",&p[i].x[j]);
	}
	sort(p+1,p+n+1);
//	if (D==1) return printf("%lld\n",p[n].x[0]-p[1].x[0]),0;
	for (int i=0;i<1<<(D-1);i++) solve(i);
	printf("%lld\n",ans);
}
