#include <cstdio>
#include <algorithm>
using namespace std;
#define ll long long
const int N=2e5+7;
const int mx[2]={-1,1};
int n,D;
int w[4];
ll ans;
struct node {
	ll x[5];
}p[N];
int main() {
	freopen("guard.in","r",stdin);
	freopen("guard1.out","w",stdout);
	scanf("%d%d",&n,&D);
	for (int i=1;i<=n;i++) {
		for (int j=0;j<D;j++) scanf("%lld",&p[i].x[j]);
	}
	for (int i=1;i<=n;i++) 
		for (int j=1;j<=n;j++) {
			ll v=0;
			for (int k=0;k<D;k++)
				v+=abs(p[i].x[k]-p[j].x[k]);
			ans=max(ans,v);
		}
	printf("%lld\n",ans);
}
