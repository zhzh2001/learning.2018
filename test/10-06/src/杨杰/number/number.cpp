#include <cstdio>
#include <algorithm>
using namespace std;
const int N=1e5+7;
int a[N],b[N<<1],p[N],v[N],T[N<<3];
int n,m,cnt;
#define ls ts<<1
#define rs ts<<1|1
void add(int ts,int l,int r,int p,int v) {
	if (l==r) {T[ts]+=v;return;}
	int mid=(l+r)>>1;
	if (p<=mid) add(ls,l,mid,p,v);
	else add(rs,mid+1,r,p,v);
	T[ts]=max(T[ls],T[rs]);
}
int query(int ts,int l,int r) {
	if (l==r) return l;
	int mid=(l+r)>>1;
	if (T[ls]==T[ts]) return query(ls,l,mid);
	else return query(rs,mid+1,r);
}
int main() {
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]),b[++cnt]=a[i];
	for (int i=1;i<=m;i++) scanf("%d%d",&p[i],&v[i]),b[++cnt]=v[i];
	sort(b+1,b+cnt+1);
	cnt=unique(b+1,b+cnt+1)-b-1;
	for (int i=1;i<=n;i++) a[i]=lower_bound(b+1,b+cnt+1,a[i])-b;
	for (int i=1;i<=n;i++) add(1,1,cnt,a[i],1);
	for (int i=1;i<=m;i++) {
		v[i]=lower_bound(b+1,b+cnt+1,v[i])-b;
		add(1,1,cnt,a[p[i]],-1);a[p[i]]=v[i];
		add(1,1,cnt,a[p[i]],1);
		printf("%d\n",b[query(1,1,cnt)]);
	}
}
