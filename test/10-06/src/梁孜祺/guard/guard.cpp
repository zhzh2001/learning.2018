#include<bits/stdc++.h>
#define N 200005
#define ll long long
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
int n, d;
ll ans;
struct xy {
  ll a, b, c, d, e;
  bool operator <(const xy & b)const {
    return a < b.a;
  }
}a[N];
struct data {
  ll x[20];
}b[N];
inline bool cmp1(data a, data b) {
  return a.x[1] < b.x[1];
}
inline bool cmp2(data a, data b) {
  return a.x[2] < b.x[2];
}
inline bool cmp3(data a, data b) {
  return a.x[3] < b.x[3];
}
inline bool cmp4(data a, data b) {
  return a.x[4] < b.x[4];
}
inline bool cmp5(data a, data b) {
  return a.x[5] < b.x[5];
}
inline bool cmp6(data a, data b) {
  return a.x[6] < b.x[6];
}
inline bool cmp7(data a, data b) {
  return a.x[7] < b.x[7];
}
inline bool cmp8(data a, data b) {
  return a.x[8] < b.x[8];
}
inline bool cmp9(data a, data b) {
  return a.x[9] < b.x[9];
}
inline bool cmp10(data a, data b) {
  return a.x[10] < b.x[10];
}
inline bool cmp11(data a, data b) {
  return a.x[11] < b.x[11];
}
inline bool cmp12(data a, data b) {
  return a.x[12] < b.x[12];
}
inline bool cmp13(data a, data b) {
  return a.x[13] < b.x[13];
}
inline bool cmp14(data a, data b) {
  return a.x[14] < b.x[14];
}
inline bool cmp15(data a, data b) {
  return a.x[15] < b.x[15];
}
inline bool cmp16(data a, data b) {
  return a.x[16] < b.x[16];
}
int main() {
  freopen("guard.in","r",stdin);
  freopen("guard.out","w",stdout);
  n = read(); d = read();
  if (d == 1) {
    for (int i = 1; i <= n; i++) a[i].a = read();
    sort(a+1,a+1+n);
    printf("%lld\n", a[n].a-a[1].a);
  }
  if (d == 2) {
    for (int i = 1; i <= n; i++) {
      ll x = read(), y = read();
      b[i].x[1] = x+y;
      b[i].x[2] = x-y;
    }
    sort(b+1, b+1+n, cmp1);
    ans = b[n].x[1] - b[1].x[1];
    sort(b+1, b+1+n, cmp2);
    ans = max(ans, b[n].x[2] - b[1].x[2]);
    printf("%lld\n", ans);
  }
  if (d == 3) {
    for (int i = 1; i <= n; i++) {
      ll x = read(), y = read(), z = read();
      b[i].x[1] = x+y+z;
      b[i].x[2] = x-y+z;
      b[i].x[3] = x+y-z;
      b[i].x[4] = x-y-z;
    }
    sort(b+1, b+1+n, cmp1);
    ans = b[n].x[1] - b[1].x[1];
    sort(b+1, b+1+n, cmp2);
    ans = max(ans, b[n].x[2] - b[1].x[2]);
    sort(b+1, b+1+n, cmp3);
    ans = max(ans, b[n].x[3] - b[1].x[3]);
    sort(b+1, b+1+n, cmp4);
    ans = max(ans, b[n].x[4] - b[1].x[4]);
    printf("%lld\n", ans);
  }
  if (d == 4) {
    for (int i = 1; i <= n; i++) {
      ll x = read(), y = read(), z = read(), X = read();
      b[i].x[1] = x+y+z+X;
      b[i].x[2] = x-y+z+X;
      b[i].x[3] = x+y-z+X;
      b[i].x[4] = x+y+z-X;
      b[i].x[5] = x-y-z+X;
      b[i].x[6] = x-y+z-X;
      b[i].x[7] = x+y-z-X;
      b[i].x[8] = x-y-z-X;
    }
    sort(b+1, b+1+n, cmp1);
    ans = b[n].x[1] - b[1].x[1];
    sort(b+1, b+1+n, cmp2);
    ans = max(ans, b[n].x[2] - b[1].x[2]);
    sort(b+1, b+1+n, cmp3);
    ans = max(ans, b[n].x[3] - b[1].x[3]);
    sort(b+1, b+1+n, cmp4);
    ans = max(ans, b[n].x[4] - b[1].x[4]);
    sort(b+1, b+1+n, cmp5);
    ans = max(ans, b[n].x[5] - b[1].x[5]);
    sort(b+1, b+1+n, cmp6);
    ans = max(ans, b[n].x[6] - b[1].x[6]);
    sort(b+1, b+1+n, cmp7);
    ans = max(ans, b[n].x[7] - b[1].x[7]);
    sort(b+1, b+1+n, cmp8);
    ans = max(ans, b[n].x[8] - b[1].x[8]);
    printf("%lld\n", ans);
  }
  if (d == 5) {
    for (int i = 1; i <= n; i++) {
      ll x = read(), y = read(), z = read(), X = read(), Y = read();
      b[i].x[1] = x+y+z+X+Y;
      b[i].x[2] = x-y+z+X+Y;
      b[i].x[3] = x+y-z+X+Y;
      b[i].x[4] = x+y+z-X+Y;
      b[i].x[5] = x+y+z+X-Y;
      b[i].x[6] = x-y-z+X+Y;
      b[i].x[7] = x-y+z-X+Y;
      b[i].x[8] = x-y+z+X-Y;
      b[i].x[9] = x+y-z-X+Y;
      b[i].x[10]= x+y-z+X-Y;
      b[i].x[11]= x+y+z-X-Y;
      b[i].x[12]= x-y-z-X+Y;
      b[i].x[13]= x-y-z+X-Y;
      b[i].x[14]= x-y+z-X-Y;
      b[i].x[15]= x+y-z-X-Y;
      b[i].x[16]= x-y-z-X-Y;
    }
    sort(b+1, b+1+n, cmp1);
    ans = b[n].x[1] - b[1].x[1];
    sort(b+1, b+1+n, cmp2);
    ans = max(ans, b[n].x[2] - b[1].x[2]);
    sort(b+1, b+1+n, cmp3);
    ans = max(ans, b[n].x[3] - b[1].x[3]);
    sort(b+1, b+1+n, cmp4);
    ans = max(ans, b[n].x[4] - b[1].x[4]);
    sort(b+1, b+1+n, cmp5);
    ans = max(ans, b[n].x[5] - b[1].x[5]);
    sort(b+1, b+1+n, cmp6);
    ans = max(ans, b[n].x[6] - b[1].x[6]);
    sort(b+1, b+1+n, cmp7);
    ans = max(ans, b[n].x[7] - b[1].x[7]);
    sort(b+1, b+1+n, cmp8);
    ans = max(ans, b[n].x[8] - b[1].x[8]);
    sort(b+1, b+1+n, cmp9);
    ans = max(ans, b[n].x[9] - b[1].x[9]);
    sort(b+1, b+1+n, cmp10);
    ans = max(ans, b[n].x[10] - b[1].x[10]);
    sort(b+1, b+1+n, cmp11);
    ans = max(ans, b[n].x[11] - b[1].x[11]);
    sort(b+1, b+1+n, cmp12);
    ans = max(ans, b[n].x[12] - b[1].x[12]);
    sort(b+1, b+1+n, cmp13);
    ans = max(ans, b[n].x[13] - b[1].x[13]);
    sort(b+1, b+1+n, cmp14);
    ans = max(ans, b[n].x[14] - b[1].x[14]);
    sort(b+1, b+1+n, cmp15);
    ans = max(ans, b[n].x[15] - b[1].x[15]);
    sort(b+1, b+1+n, cmp16);
    ans = max(ans, b[n].x[16] - b[1].x[16]);
    printf("%lld\n", ans);
  }
  return 0;
}