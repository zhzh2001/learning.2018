#include<bits/stdc++.h>
#define N 100005
#define M 10000005
using namespace std;
int n, a[N], mx, pos, p[M], m;
set <pair<int, int> > s;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
int main() {
  freopen("number.in","r",stdin);
  freopen("number.out","w",stdout);
  n = read(); m = read();
  for (int i = 1; i <= n; i++) {
    a[i] = read();
    p[a[i]]++;
  }
  for (int i = 1; i < M; i++)
    if (p[i]) s.insert(make_pair(-p[i], i));
  for (int i = 1; i <= m; i++) {
    int x = read(), y = read();
    set <pair<int, int> >::iterator it = s.find(make_pair(-p[a[x]], a[x]));
    s.erase(it);
    p[a[x]]--;
    if (p[a[x]] != 0) s.insert(make_pair(-p[a[x]], a[x]));
    a[x] = y;
    if (p[y]) {
      it = s.find(make_pair(-p[y], y));
      s.erase(it);
    }
    p[y]++;
    s.insert(make_pair(-p[y], y));
    printf("%d\n", (*s.begin()).second);
  }
  return 0;
}