#include <iostream>
#include <cstdio>
#define int long long
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
int a[200005][6];
signed main()
{
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	int n=read(),d=read(),ans=0;
	if(d==1){
		int mx=0,mn=100000000000;
		for(int i=1;i<=n;i++){
			int x=read();
			mn=min(x,mn);mx=max(x,mx);
		}
		ans=mx-mn;
		cout<<ans<<endl;
		return 0;
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=d;j++) a[i][j]=read();
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++){
			int sum=0;
			for(int k=1;k<=d;k++){
				if(a[i][k]>a[j][k])sum+=(a[i][k]-a[j][k]);
				else sum+=(a[j][k]-a[i][k]);
			}
			ans=max(ans,sum);
		}
	cout<<ans<<endl;
	return 0;
}
