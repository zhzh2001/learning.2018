#include <algorithm>
#include <iostream>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);putchar('\n');}
struct arr{
	int x,sum;
}a[10000005];
bool cmp(arr a,arr b){
	if(a.sum==b.sum)return a.x<b.x;
	return a.sum>b.sum;
}
int tot,x[100005];
bool vis[10000005];
int find(int u){
	if(!vis[u])return -1;
	for(int i=1;i<=tot;i++){
		if(a[i].x==u)return i;
	}
	/*int l=1,r=tot,ans=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if(a[mid].x<=u)ans=mid,r=mid-1;
		else l=mid+1;
	}
//	cout<<ans<<" "<<u<<endl;
	return ans;*/
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	int n=read(),m=read(),mx=0;
	for(int i=1;i<=n;i++){
		x[i]=read();
		mx=max(mx,x[i]);
		if(!vis[x[i]]){
		    a[++tot].sum++,a[tot].x=x[i],vis[x[i]]=1;
	    }
		else {
			int wz=find(x[i]);
			a[wz].sum++;
		}
	}
	while(m--){
		int pos=read(),y=read();
//		for(int i=1;i<=tot;i++)cout<<"kkk"<<a[i].x<<" ";puts("");
		int wz=find(x[pos]),wzz=find(y);
//		cout<<wz<<" "<<wzz<<endl;
		a[wz].sum--;
		if(find(y)==-1)a[++tot].sum++,a[tot].x=y,vis[y]=1;
		else a[wzz].sum++;
		x[pos]=y;
		sort(a+1,a+1+tot,cmp);
		wrn(a[1].x);
	}
	return 0;
}
