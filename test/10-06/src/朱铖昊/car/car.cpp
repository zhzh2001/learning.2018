#include<bits/stdc++.h>
using namespace std;
const int N=1000005;
int x[N],y[N],z[N];
int n;
char c[N];
int len,ans;
inline bool check(int x,int y,int z)
{
	return x!=y&&x!=z&&y!=z;
}
inline bool tp()
{
	if (n<=10)
		return false;
	if (c[1]==c[2]||c[1]==c[3]||c[2]==c[3])
		return false;
	for (int i=4;i<=n;++i)
		if (c[i]!=c[i-1])
			return false;
	//return false;
	return true;
}
int main()
{
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	scanf("%d%s",&n,c+1);
	if (tp())
	{
		puts("1");
		return 0;
	}
	len=0;
	ans=1;
	for (int i=1;i<=n;++i)
	{
		if (c[i]==c[i-1])
		{
			++len;
			ans=max(ans,len);
		}
		else
			len=1;
	}
	for (int i=1;i<=n;++i)
	{
		x[i]=x[i-1];
		y[i]=y[i-1];
		z[i]=z[i-1];
		if (c[i]=='C')	
			++x[i];
		if (c[i]=='B')
			++y[i];
		if (c[i]=='S')
			++z[i];
	}
	for (int i=1;i<=n;++i)
		for (int j=n-i+1;j>ans;--j)
			if (check(x[i+j-1]-x[i-1],y[i+j-1]-y[i-1],z[i+j-1]-z[i-1]))
			{
				ans=j;
				break;
			}
	cout<<ans;
	return 0;
}