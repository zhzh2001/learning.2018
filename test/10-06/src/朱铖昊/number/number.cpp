#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=10000005;
int ti[M],a[N],n,m,mx,x,y;
set<int> s[N];
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void down()
{
	while (mx!=1&&s[mx].size()==0)
		--mx;
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=n;++i)
	{
		read(a[i]);
		if (ti[a[i]]!=0)
			s[ti[a[i]]].erase(a[i]);
		++ti[a[i]];
		s[ti[a[i]]].insert(a[i]);
	}
	mx=n;
	down();
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		s[ti[a[x]]].erase(a[x]);
		--ti[a[x]];
		if (ti[a[x]]!=0)
			s[ti[a[x]]].insert(a[x]);
		if (ti[y]!=0)
			s[ti[y]].erase(y);
		++ti[y];
		if (ti[y]>mx)
			mx=ti[y];
		s[ti[y]].insert(y);
		a[x]=y;
		down();
		printf("%d\n",*s[mx].begin());
	}
	return 0;
}