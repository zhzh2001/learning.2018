#include<bits/stdc++.h>
using namespace std;
#define int long long
const int inf=1e15+7;
inline void read(int &x)
{
	char c=getchar();
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
const int N=200005;
int a[N][7],n,m,mx,mn,ans;
int g[7];
signed main()
{
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
			read(a[i][j]);
	for (int i=0;i<(1<<(m-1));++i)
	{
		int k=i;
		for (int j=2;j<=m;++j)
		{
			if (k%2==0)
				g[j]=1;
			else
				g[j]=-1;
			k/=2;
		}
		mx=-inf;
		mn=inf;
		for (int j=1;j<=n;++j)
		{
			int cnt=a[j][1];
			for (int l=2;l<=m;++l)
				cnt+=g[l]*a[j][l];
			mx=max(cnt,mx);
			mn=min(cnt,mn);
		}
		ans=max(ans,mx-mn);
	}
	cout<<ans;
	return 0;
}