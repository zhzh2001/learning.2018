#include<bits/stdc++.h>
#define int long long
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
int n,d,x,sum,maxx,minn;
int a[2501][2501];
signed main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	minn=2147483647;
	maxx=-2147483647;
	read(n);
	read(d);
	if(n<=2500){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=d;j++)
				read(a[i][j]);
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++){
				sum=0;
				for(int k=1;k<=d;k++)
					sum+=abs(a[i][k]-a[j][k]);
				maxx=max(maxx,sum);
			}
		return cout<<maxx,0;
	}
	for(int i=1;i<=n;i++){
		sum=0;
		for(int j=1;j<=d;j++){
			read(x);
			sum+=x;
		}
		maxx=max(maxx,sum);
		minn=min(minn,sum);
	}
	cout<<maxx-minn;
	return 0;
}
