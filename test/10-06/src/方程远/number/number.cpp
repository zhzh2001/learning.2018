#include<bits/stdc++.h>
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
inline void write(int x){
	if(x<0)
		x=-x,putchar('-');
	if(x>9)
		write(x/10);
	putchar(x%10+48);
}
inline void writeln(int x){
	write(x);
	putchar('\n');
}
int n,m,x,y,maxx,place;
map<int,int>mp;
int a[100001];
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	read(n);
	read(m);
	for(int i=1;i<=n;i++)
		read(a[i]);
	for(int i=1;i<=n;i++){
		mp[a[i]]++;
		if((mp[a[i]]==maxx&&a[i]<place)||(mp[a[i]]>maxx)){
			maxx=mp[a[i]];
			place=a[i];
		}
	}
	while(m--){
		read(x);
		read(y);
		mp[a[x]]--;
		mp[y]++;
		if(place==a[x])
			maxx--;
		if((mp[y]==maxx&&y<place)||(mp[y]>maxx)){
			maxx=mp[y];
			place=y;
		}
		writeln(place);
	}
	return 0;
}
