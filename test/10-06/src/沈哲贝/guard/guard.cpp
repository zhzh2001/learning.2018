#include<bits/stdc++.h>
using namespace std;
#define ll long long
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
#define Fpn			freopen
#define IL			inline
IL void Max(ll &x,ll y){x=x<y?y:x;}
IL void Min(ll &x,ll y){x=x<y?x:y;}
IL ll max(ll x,ll y){return x<y?y:x;}
IL ll min(ll x,ll y){return x<y?x:y;}
IL void Print(ll *a,ll x,ll y){	For(i,x,y)	write(a[i]),putchar(' ');	}
const ll N=400010;
ll a[N][8],n,d,ans;
ll Dis(ll x,ll y){ll sum=0;For(i,1,d)sum+=abs(a[x][i]-a[y][i]);return sum;}
namespace SZB1{
	void work(){
		For(i,1,n)For(j,1,d)a[i][j]=read();
		For(i,1,n)For(j,i+1,n)Max(ans,Dis(i,j));
		writeln(ans);
	}
};
namespace SZB2{
	struct dt{ll m[5];}a[N],b[N],c[N];
	char s[N];
	bool cmp1(dt a,dt b){return a.m[1]<b.m[1];}
	bool cmp2(dt a,dt b){return a.m[2]<b.m[2];}
	bool cmp3(dt a,dt b){return a.m[3]<b.m[3];}
	IL void doit(ll l1,ll r1,ll l2,ll r2){
		sort(c+l1+1,c+r1+1,cmp1);
		sort(c+l2+1,c+r2+1,cmp2);
		ll pos=l1,mx=-1e12;
		For(i,l2,r2){
			for(;pos<r1&&c[pos].m[1]<=c[i].m[1];){
				Max(mx,c[pos].m[4]-c[pos].m[1]);
				++pos;
			}
			Max(ans,c[i].m[1]+c[i].m[4]+mx);
		}
		pos=l2,mx=-1e12;
		For(i,l1,r1){
			for(;pos<r2&&c[pos].m[1]<=c[i].m[1];){
				Max(mx,c[pos].m[4]-c[pos].m[1]);
				++pos;
			}
			Max(ans,c[i].m[1]+c[i].m[4]+mx);
		}
	}
	IL void Cdq3(ll l1,ll r1,ll l2,ll r2){
		doit(l1,r1,l2,r2);
	}

	void Cdq2(ll l1,ll r1,ll l2,ll r2){	
		if (l1>r1||l2>r2)return;
		if (l1==r1){
			Max(ans,abs(b[l1].m[1]-b[l2].m[1])+abs(b[l1].m[2]-b[l2].m[2])+abs(b[l1].m[3]-b[l2].m[3]));
			return;
		}
		ll mid=((l1+r1)>>1),mid1=((l2+r2)>>1);
		Cdq2(l1,mid,	l2,mid1); 
		Cdq2(mid+1,r1,	mid1+1,r2);
		For(i,l1,r1)c[i]=b[i];
		For(i,l2,r2)c[i]=b[i];
		
		For(i,l1,mid)	c[i].m[4]-=b[i].m[2];
		For(i,mid+1,r1)	c[i].m[4]+=b[i].m[2];
		For(i,l2,mid1)	c[i].m[4]-=b[i].m[2];
		For(i,mid1+1,r2)c[i].m[4]+=b[i].m[2];

		Cdq3(l1,mid,	mid1+1,r2);
		Cdq3(mid+1,r1,	l2,mid1);
	}

	void CCdq2(ll l1,ll r1,ll l2,ll r2){
		sort(b+l1,b+r1+1,cmp2);
		sort(b+l2,b+r2+1,cmp2);
		Cdq2(l1,r1,l2,r2);
	}

	void Cdq1(ll l,ll r){
		if (l>=r)return;
		ll mid=((l+r)>>1)+1;
		if (d==3)Cdq1(l,mid);
		if (d==3)Cdq1(mid+1,r);
		For(i,l,r)		b[i]=a[i];
		For(i,l,mid)	b[i].m[4]-=a[i].m[3];
		For(i,mid+1,r)	b[i].m[4]+=a[i].m[3];
		CCdq2(l,mid,mid+1,r);
	}
	void work(){
		For(i,1,n)For(j,1,d)a[i].m[j]=read();
		sort(a+1,a+n+1,cmp3);
		Cdq1(1,n);
		writeln(ans);
	}
};
int main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read();d=read();
	if (n<=2500)SZB1::work();
		else	SZB2::work();
}
