#include<bits/stdc++.h>
using namespace std;
#define ll long long
//#define pa pair<ll,ll>
//#define fi first
//#define se second
//#define mk make_pair
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
#define Fpn			freopen
#define IL			inline
IL void Max(ll &x,ll y){x=x<y?y:x;}
IL void Min(ll &x,ll y){x=x<y?x:y;}
IL ll max(ll x,ll y){return x<y?y:x;}
IL ll min(ll x,ll y){return x<y?x:y;}
IL void Print(ll *a,ll x,ll y){	For(i,x,y)	write(a[i]),putchar(' ');	}
ll sum[10000010],a[100010],n,m;
struct pa{ll fi,se;};
bool operator <(pa a,pa b){
	return a.fi==b.fi?a.se>b.se:a.fi<b.fi;
}
bool operator >(pa a,pa b){
	return a.fi==b.fi?a.se<b.se:a.fi>b.fi;
}
priority_queue<pa>Q;
void insert(ll x){
	++sum[x];
	Q.push((pa){sum[x],x});
}
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();	m=read();
	For(i,1,n){
		a[i]=read();
		insert(a[i]);
	}
	For(i,1,m){
		ll x=read();
		--sum[a[x]];
		a[x]=read();
		insert(a[x]);
		for(;sum[Q.top().se]<Q.top().fi;)
			Q.pop();
		writeln(Q.top().se);
	}
}
/*
3 2
1 1 1
2 2
3 2
*/
