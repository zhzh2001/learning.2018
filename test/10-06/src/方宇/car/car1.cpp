#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cctype>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218
#define N 2000010
using namespace std;
char a[N];
char ch,ch2,ch3;
int C,B,S,ansl,ansr;
int n,m,i,j,k,l,r,h,boo;
int read()
{
	int x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int min(int a,int b)
{
	if (a>b) return b;
	return a;
}
int max(int a,int b)
{
	if (a<b) return b;
	return a;
}
bool check(LL a,LL b,LL c)
{
	if (!a&&!b||!a&&!c||!b&&!c) return 1;
	if (a==b||a==c||b==c) return 0;
	return 1;
}
bool check2(LL a,LL b,LL c)
{
	if (a==b||a==c||b==c) return 1;
	return 0;
}
void find()
{
	if (C==B) k=S-C,ch='S';
	if (C==S) k=B-C,ch='B';
	if (B==S) k=C-B,ch='C';
}
int main()
{
//	freopen("car.in","r",stdin);
//	freopen("car.out","w",stdout);
	n=read(); scanf("%s",a+1);
	rep(i,1,n)
	{
		if (a[i]=='C') C++;
		if (a[i]=='B') B++;
		if (a[i]=='S') S++;
	}
	if (check(C,B,S))
	{
		printf("%d",n);
		return 0;
	}
	if (check2(C,B,S))
	{
		find();
		while (a[l+1]==ch&&l<n) l++;
		while (a[n-r]==ch&&r<n) r++;
		h=min(l,r);
		if (h==n)
		{
			printf("%d",n);
			return 0;
		}
		if (k<-1||h<k||h>k+1)
		{
			printf("%d",n-h-1);
			return 0;
		}
			n=n-k-1;
		if (h==k)
		{
			if (l<r)
			{
				rep(i,1,n) a[i]=a[i+k];
			}
			else
			{
				rep(i,1,n) a[i]=a[i+1];
			}
		}
		if (h==k+1)
		{
			if (l<r)
			{
				rep(i,1,n) a[i]=a[i+k+1];
			}
		}
	}
	if (n<2)
	{
		printf("%d",n);
		return 0;
	}
	if (a[1]==a[2]&&n==2)
	{
		printf("%d",n);
		return 0;
	}
	if (a[1]==ch||a[2]==ch||a[1]==a[n]||a[n]==ch||a[n-1]==ch)
	{
		printf("%d",n-2);
		return 0;
	}
	a[0]=ch; boo=1;
	rep(i,3,n)
	{
		if (a[i]!=a[i-3])
		{
			boo=0;
			if (a[i]!=ch)
			{
				ansl=i;
				break;
			}
			else
			{
				l=0; r=0;
				while (a[l+i+1]==ch&&l<n) l++;
				while (a[n-r]==ch&&r<n) r++;
				ansl=i+min(l,r);
			}
		}
	}
	if (boo)
	{
		printf("1");
		return 0;
	}
	a[n+1]=ch;
	dep(i,n-2,1)
	{
		if (a[i]!=a[i+3])
		{
			if (a[i]!=ch)
			{
				ansr=n-i+1;
				break;
			}
			else
			{
				l=0; r=0;
				while (a[l+1]==ch&&l<n) l++;
				while (a[i-r]==ch&&r<n) r++;
				ansr=i+min(l,r);
			}
		}
	}
	printf("%d",max(n-ansl,n-ansr));
	return 0;
}
