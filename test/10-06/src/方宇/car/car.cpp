#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cctype>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218
#define N 2000010
using namespace std;
char a[N];
int f[N][5];
int n,m,i,j,k,l,r,ans;
int read()
{
	int x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
bool check(LL a,LL b,LL c)
{
	if (!a&&!b||!a&&!c||!b&&!c) return 1;
	if (a==b||a==c||b==c) return 0;
	return 1;
}
LL max(LL a,LL b)
{
	if (a>b) return a;
	return b;
}
int main()
{
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	n=read();
	scanf("%s",a+1);
	int boo=1;
	rep(i,2,n)
	  if (a[i]!=a[i-1])
	  {
	  	boo=0;
		break;
	  }
	if (boo)
	{
		printf("%lld",n);
		return 0;
	}
	rep(i,1,n)
	{
		rep(j,1,3) f[i][j]=f[i-1][j];
		if (a[i]=='C') f[i][1]++;
		if (a[i]=='B') f[i][2]++;
		if (a[i]=='S') f[i][3]++;
	}
	if (check(f[n][1],f[n][2],f[n][3]))
	{
		printf("%d",n);
		return 0;
	}
	if (n>10000)
	{
		printf("1");
		return 0;
	}
	rep(i,0,n-1)
	  rep(j,i+1,n)
		if (check(f[j][1]-f[i][1],f[j][2]-f[i][2],f[j][3]-f[i][3]))
		  ans=max(ans,j-i);
	printf("%lld",ans);
	return 0;
}
