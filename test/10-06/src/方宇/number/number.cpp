#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cctype>
#define LL long long
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define dep(i,r,l) for(int i=r;i>=l;i--)
#define fy 20021218
#define N 500010
#define M 20000010
using namespace std;
int a[N],Q[N],h[N],boo[M];
int n,m,i,j,k,l,r,num;
int read()
{
	int x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
void swap(int &a,int &b)
{
	int t=a; a=b; b=t;
}
bool check(int x,int y)
{
	if(Q[x]>Q[y]) return 1;
	if(Q[x]<Q[y]) return 0;
	if(h[x]<h[y]) return 1;
	return 0;
}
void down(int k)
{
	for(int i=k<<1;i<=num;k=i,i=k<<1)
	{
		if(check(i+1,i)) i++;
		if(check(i,k))
		{
			swap(Q[i],Q[k]);
			swap(boo[h[i]],boo[h[k]]);
			swap(h[i],h[k]);
		}
		else return;
	}
}
void up(int k)
{
	for(int i=k>>1;i;k=i,i=k>>1)
	{
		if(!check(i,k))
		{
			swap(Q[i],Q[k]);
			swap(boo[h[i]],boo[h[k]]);
			swap(h[i],h[k]);
		}
		else return;
	}
}
void del(int k)
{
	if(Q[boo[k]]==1)
	{
		int j=boo[k];
		swap(Q[boo[k]],Q[num]);
		swap(h[boo[k]],h[num]);
		swap(boo[k],boo[h[j]]);
		up(j);
		boo[k]=0;
		num--;
	}
	else
	{
		Q[boo[k]]--;
		down(boo[k]);
	}
}
void ins(int k)
{
	if(boo[k]==0)
	{
		num++;
		Q[num]=1;
		h[num]=k;
		boo[k]=num;
		up(num);
	}
	else
	{
		Q[boo[k]]++;
		up(boo[k]);
	}
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n)
	{
		a[i]=read();
		ins(a[i]);
	}
	rep(i,1,m)
	{
		l=read(); r=read();
		del(a[l]);
		ins(r);
		a[l]=r;
		printf("%d\n",h[1]);
	}
	return 0;
}
