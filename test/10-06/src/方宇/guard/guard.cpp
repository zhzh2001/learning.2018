#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cctype>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218
#define N 10010
#define M 10
using namespace std;
LL a[N][M];
LL n,m,i,j,k,h,sum,ans;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
LL max(LL a,LL b)
{
	if (a>b) return a;
	return b;
}
int main()
{
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n)
	  rep(j,1,m) a[i][j]=read();
	rep(i,1,n-1)
	  rep(j,i+1,n)
	  {
	  	sum=0;
		rep(k,1,m)
		{
			h=a[i][k]-a[j][k];
			sum+=abs(h);
		}
		ans=max(ans,sum);
	  }
	printf("%lld",ans);
	return 0;
}
