#include<bits/stdc++.h>
#define LL long long
#define GG long long 
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline GG read() { 
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(LL x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(LL x) {
    write(x); puts("");
}
const int N = 200011; 
struct node{
	int a, b, c, d, e; 
	LL sum; 
}a[N];
LL ans; 
int n, m, d;
bool cmp(node a, node b) {
	return a.sum<b.sum; 
} 

signed main() {
	freopen("guard.in", "r", stdin); 
	freopen("guard.out", "w", stdout); 
	n = read(); d = read(); 
	For(i, 1, n) {
		a[i].a = read(); 
		if(d>1) a[i].b = read(); 
		if(d>2) a[i].c = read(); 
		if(d>3) a[i].d = read(); 
		if(d>4) a[i].e = read(); 
	}
	for(register int i=-1;i<=1;i+=2) 
		for(register int j=-1;j<=1;j+=2)
			for(register int k=-1;k<=1;k+=2)
				for(register int l=-1;l<=1;l+=2) {
					For(o, 1, n) 
						a[o].sum=1ll*a[o].e+a[o].a*i+a[o].b*j+a[o].c*k+a[o].d*l;
					sort(a+1, a+n+1, cmp); 
					LL tot = abs(1ll*a[1].a-a[n].a)+abs(1ll*a[1].b-a[n].b)+abs(1ll*a[1].c-a[n].c)+abs(1ll*a[1].d-a[n].d)+abs(1ll*a[1].e-a[n].e);
					if(tot > ans) ans = tot; 
					For(o, 2, n-1) { 
						ans = max(abs(1ll*a[1].a-a[o].a)+abs(1ll*a[1].b-a[o].b)+abs(1ll*a[1].c-a[o].c)+abs(1ll*a[1].d-a[o].d)+abs(1ll*a[1].e-a[o].e),ans);
						ans = max(abs(1ll*a[n].a-a[o].a)+abs(1ll*a[n].b-a[o].b)+abs(1ll*a[n].c-a[o].c)+abs(1ll*a[n].d-a[o].d)+abs(1ll*a[1].e-a[o].e),ans);
					}
				}
	writeln(ans); 
}


