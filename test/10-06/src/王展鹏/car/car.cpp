#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=1000005,s[6][3]={1,2,3,1,3,2,2,1,3,2,3,1,3,1,2,3,2,1};
int n,ans,tree[N<<2],sum[N][3],last[N];
char ch[N];
struct data{
	int x,y,z;
}a[N];
map<int,int> M[N*2];
bool cal(int l,int r){
	return a[r].x!=a[l].x&&a[r].y!=a[l].y&&a[r].z!=a[l].z;
}
int main(){
	freopen("car.in","r",stdin); freopen("car.out","w",stdout);
	n=read();
	scanf("%s",ch+1);
	for(int i=1;i<=n;i++){
		for(int j=0;j<3;j++)sum[i][j]=sum[i-1][j];
		if(ch[i]=='B')sum[i][0]++; else if(ch[i]=='C')sum[i][1]++; else sum[i][2]++;
		if(ch[i]==ch[i-1])last[i]=last[i-1]; else last[i]=i; ans=max(ans,i-last[i]+1);
	}
	for(int i=0;i<=n;i++){
		a[i].x=sum[i][1]-sum[i][0]; a[i].y=sum[i][2]-sum[i][1]; a[i].z=sum[i][2]-sum[i][0]; 
		if(!M[a[i].x+N].count(a[i].y))M[a[i].x+N][a[i].y]=i;
		for(int j=0;j<6;j++){
			int t1=s[j][1]-s[j][0],t2=s[j][2]-s[j][1]; 
			if(M[a[i].x-t1+N].count(a[i].y-t2))ans=max(ans,i-M[a[i].x-t1+N][a[i].y-t2]);
		}
	}
	for(int i=1;i<=n;i++)if(cal(0,i))ans=max(ans,i);
	for(int i=0;i<n;i++)if(cal(i,n))ans=max(ans,n-i);
	cout<<ans<<endl;
}
/*
n个二元组(x,y)
找到一个距离最远的二元组满足x和y和z都不相同 
aabbcccca
1 4 9
4 6 10
3 8 5
2 6 4
a1 b1 c1
a1+t b1+t c1+t+
a b c

*/ 
