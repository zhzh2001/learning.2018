#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=1000005;
vector<int> v[N*2];
void build(int l,int r,int nod){
	if(l==r){
		tree[nod]=a[l].v;
	}
	int mid=(l+r)>>1;
	build(l,mid,nod<<1); build(mid+1,r,nod<<1|1);
	tree[nod]=min(tree[nod<<1],tree[nod<<1|1]);
}
int ask(int l,int r,int i,int j,int nod){
	if(l==i&&r==j)return tree[nod];
	int mid=(l+r)>>1;
	if(j<=mid)return ask(l,mid,i,j,nod<<1);
	else if(i>mid)return ask(mid+1,r,i,j,nod<<1|1);
	else return min(ask(l,mid,i,mid,nod<<1),ask(mid+1,r,mid+1,j,nod<<1|1));
}
void insert(int l,int r,int pos,int de,int nod){
	if(l==r){
		tree[nod]=de; return;
	}
	int mid=(l+r)>>1;
	if(pos<=mid)insert(l,mid,pos,de,nod<<1); else insert(mid+1,r,pos,de,nod<<1|1);
	tree[nod]=min(tree[nod<<1],tree[nod<<1|1]);
}
int main(){
	n=read();
	scanf("%s",ch+1);
	for(int i=1;i<=n;i++){
		for(int j=0;j<3;j++)sum[i][j]=sum[i-1][j];
		if(ch[i]=='B')sum[i][0]++; else if(ch[i]=='C')sum[i][1]++; else sum[i][2]++;
		if(ch[i]==ch[i-1])last[i]=last[i-1]; else last[i]=i; ans=max(ans,i-last[i]+1);
	}
	int mn=N,mx=-N;
	for(int i=0;i<=n;i++){
		a[i].x=sum[i][1]-sum[i][0]; a[i].y=sum[i][2]-sum[i][0]; a[i].v=i;
		a[i].x+=N; a[i].y+=N;
	}
	sort(&a[0],&a[n+1],cmpy);
	for(int i=0;i<=n;i++){a[i].pos=i; v[a[i].x].push_back(a[i]); q[i]=a[i].y;}
	build(0,n,1);
	for(int i=0;i<2*N;i++)if(v[i].size()){
		for(unsigned j=0;j<v[i].size();j++)insert(0,n,v[i][j].pos,0,1);
		for(unsigned j=0;j<v[i].size();j++){
			int l=lower_bound(&q[0],&q[n+1],v[i][j].y)-q-1,r=upper_bound(&q[0],&q[n+1],v[i][j].y)-q;
			ans=max(ans,v[i][j].v-min(ask(0,n,1,l,1),ask(0,n,r+1,n,1)));
		}
		for(unsigned j=0;j<v[i].size();j++)insert(0,n,v[i][j].pos,v[i][j].v,1);
	}
	cout<<ans<<endl;
}
/*
n个二元组(x,y)
找到一个距离最远的二元组满足x和y都不相同 
abcabcabc

*/ 
