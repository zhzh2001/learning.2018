#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define int long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=200005;
int a[N][5],n,k,ans;
signed main(){
	n=read(); k=read();
	for(int i=1;i<=n;i++)for(int j=0;j<k;j++)a[i][j]=read();
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			ll sum=0;
			for(int l=0;l<k;l++)sum+=abs(a[i][l]-a[j][l]);
			ans=max(ans,sum);
		}
	}
	cout<<ans<<endl;
}
