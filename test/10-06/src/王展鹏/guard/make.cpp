#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
int n,k;
int main(){
	srand(time(0));
	n=rand()%1000+1; k=rand()%5+1;
	cout<<n<<" "<<k<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=k;j++){
			cout<<(rand()<<15^rand())<<" ";
		}puts("");
	}
}
