#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=200005,K=5;
ll ans,a[N][K],mx[32];
int n,k;
int main(){
	freopen("guard.in","r",stdin); freopen("guard.out","w",stdout);
	n=read(); k=read();
	for(int i=0;i<32;i++)mx[i]=-1e18;
	for(int i=1;i<=n;i++){
		for(int j=0;j<k;j++)a[i][j]=read();
		for(int j=0;j<(1<<k);j++){
			ll sum=0;
			for(int l=0;l<k;l++)if(j>>l&1)sum+=a[i][l]; else sum-=a[i][l];
			mx[j]=max(mx[j],sum);
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=0;j<(1<<k);j++){
			ll sum=0;
			for(int l=0;l<k;l++)if(j>>l&1)sum+=a[i][l]; else sum-=a[i][l];
			ans=max(ans,sum+mx[j^((1<<k)-1)]);
		}
	}
	cout<<ans<<endl;
}
