#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=100005;
int n,m,a[N],tong[N*100]; 
struct heap{
	priority_queue<PI> a,d;
	void push(PI x){
		a.push(x);
	}
	void erase(PI x){
		d.push(x);
	}
	PI top(){
		while(a.size()&&d.size()&&a.top()==d.top()){
			a.pop(); d.pop();
		}
		return a.top();
	}
}q;
int main(){
	freopen("number.in","r",stdin); freopen("number.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=n;i++)tong[a[i]=read()]++;
	//cout<<tong[4201459]<<" "<<tong[7374995]<<endl; return 0;
	for(int i=0;i<N*100;i++)if(tong[i])q.push(mp(tong[i],-i));
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		q.erase(mp(tong[a[x]],-a[x])); tong[a[x]]--; if(tong[a[x]])q.push(mp(tong[a[x]],-a[x]));
		a[x]=y; 
		if(tong[a[x]])q.erase(mp(tong[a[x]],-a[x])); tong[a[x]]++; q.push(mp(tong[a[x]],-a[x]));
		writeln(-q.top().second);
	}
}
