#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("car.in", "r", stdin);
	freopen("car.out", "w", stdout);
}
const int N = 2000000;
int n;
int c[N][4];
char s[N];
int ans;
int main() {
	setIO();
	n = read();
	cin>>(s + 1);
	for(int i = 1; i <= n; ++i) {
		c[i][1] = c[i - 1][1] + (s[i] == 'B');
		c[i][2] = c[i - 1][2] + (s[i] == 'C');
		c[i][3] = c[i - 1][3] + (s[i] == 'S');
	}
	for(int i = 1; i <= n; ++i)
		for(int j = n; j - i + 1 >= ans; --j) {
			int v1 = c[j][1] - c[i - 1][1];
			int v2 = c[j][2] - c[i - 1][2];
			int v3 = c[j][3] - c[i - 1][3];
			int V = v1 + v2 + v3;
			if(V == v1 || V == v2 || V == v3 || v1 != v2 && v1 != v3 && v2 != v3) {
				ans = max(ans, j - i + 1);
				break;
			}
		}
	writeln(ans);
	return 0;
}
