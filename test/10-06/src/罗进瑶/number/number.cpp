#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("number.in", "r", stdin);
	freopen("number.out", "w", stdout);
}
const int N = 120000;
int n, m, num;
set<int>v[N];
map<int, int>id;
int cnt[N * 3], a[N * 3], mx, b[N * 3];
void update(int x, int val) {
	v[cnt[x]].erase(b[x]);
	cnt[x] += val;
	v[cnt[x]].insert(b[x]);
	if(cnt[x] > mx) mx = cnt[x];
	while(!v[mx].size() && mx > 0)--mx;
}
int main() {
	setIO();
	n = read(), m = read();
	for(int i = 1; i <= n; ++i) {
		a[i] = read();
		if(!id[a[i]]) {
			id[a[i]] = ++num;
			b[num] = a[i];
			v[0].insert(a[i]);
		}
		update(id[a[i]], 1);
	}
	for(int i = 1; i <= m; ++i) {
		int x = read(), y = read();
		update(id[a[x]], -1);
		if(!id[y]) {
			id[y] = ++num;
			b[num] = y;
			v[0].insert(y);
		}
		update(id[y], 1);
		writeln(*v[mx].begin());
		a[x] = y;
	}
	return 0;
}
