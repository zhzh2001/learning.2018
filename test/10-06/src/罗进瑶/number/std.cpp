#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("number.in", "r", stdin);
	freopen("std.out", "w", stdout);
}
const int N = 1234505;
int cnt[N], a[N];
int solve() {
	int ans = 0;
	for(int i = 0; i <= 10000; ++i)
		if(cnt[i] > cnt[ans]) ans = i;
	return ans;
}
int main() {
	setIO();
	int n = read(), m = read();
	for(int i = 1; i <= n; ++i)
		a[i] = read(), ++cnt[a[i]];
	for(int i = 1; i <= m; ++i) {
		int x = read(), y = read();
		--cnt[a[x]];
		++cnt[y];
		a[x] = y;
		writeln(solve());
	}
	return 0;
}
