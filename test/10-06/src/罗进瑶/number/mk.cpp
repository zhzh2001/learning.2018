#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen(".in", "r", stdin);
	freopen(".out", "w", stdout);
}
int n = 6000, m = 6000;
int main() {
//	freopen("number.in","w",stdout);
	srand(time(NULL));
	printf("%d %d\n", n, m);
	for(int i = 1; i <= n; ++i)
		printf("%d ", rand() % 10000 + 1);
	puts("");
	for(int i = 1; i <= m; ++i) {
		printf("%d %d\n", rand() % n + 1, rand() % 10000);
	}
	return 0;
}
