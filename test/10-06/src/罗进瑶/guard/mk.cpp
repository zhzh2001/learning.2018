#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("guard.in", "w", stdout);
}
int n = 5000; 
int num[] = {-1, 1};
const ll p = 1e9 + 7;
int main() {
	setIO();
	srand(time(NULL));
	printf("%d %d\n", n, 2);
	for(int i = 1; i <= n; ++i) {
		printf("%lld %lld\n", (ll)rand() * rand() * rand() % p * num[rand() % 2], (ll)rand() * rand() * rand() % p * num[rand() % 2]);
	} 
	puts("");
	return 0;
}
