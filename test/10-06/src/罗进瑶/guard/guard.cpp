#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("guard.in", "r", stdin);
	freopen("guard.out", "w", stdout);
}
const int N = 210000;
struct T {
	ll x[7];
}a[N];
ll ans;
int n, d;
ll b[N * 5], tot;
struct G {
	ll c[N * 5];
	G() {memset(c, -0x3f, sizeof c);}
	void add(int x, ll v) {
		for(; x <= tot; x += x & -x)
			c[x] = max(c[x], v);
	}
	ll ask(int x) {
		ll res = -1e10;
		for(; x; x -= x & -x)
			res = max(res, c[x]);
		return res;
	}
}t1, t2;
bool cmpx(T _x, T _y) {
	if(_x.x[1] == _y.x[1]) return _x.x[2] < _y.x[2];
	return _x.x[1] < _y.x[1];
}
int main() {
	setIO();
	n = read();d=read();
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= d; ++j)
			a[i].x[j] = read(), b[++tot] = a[i].x[j];
	if(d == 1) {
		ll mx = -1e16, mn = 1e16;
		for(int i = 1; i <= n; ++i) {
			mx = max(mx, a[i].x[1]);
			mn = min(mn, a[i].x[1]);
		}
		writeln(mx - mn);
		return 0;
	}
	if(d == 2) {
		b[++tot] = -1e11;
		b[++tot] = 1e11;
		sort(b + 1, b + 1 + tot);
		tot = unique(b + 1, b + 1 + tot) - b - 1;		
		sort(a + 1, a + 1 + n, cmpx);
		for(int i = 1; i <= n; ++i) {
			int p = lower_bound(b + 1, b + 1 + tot, a[i].x[2]) - b;
			ll val1 = t1.ask(tot - p);
			ll val2 = t2.ask(p);
			ans = max(ans, max(val1 + a[i].x[1] - a[i].x[2], val2 + a[i].x[1] + a[i].x[2]));
			t1.add(tot - p, -a[i].x[1] + a[i].x[2]);
			t2.add(p, -a[i].x[2] - a[i].x[1]);
		}
		writeln(ans);
		return 0;
	}
	for(int i = 1; i <= n; ++i)
		for(int j = i + 1; j <= n; ++j) {
			ll res = 0;
			for(int k = 1; k <= d; ++k)
				res += abs(a[i].x[k] - a[j].x[k]);
			if(res > ans) ans = res;
		}
	writeln(ans);
	return 0;
}
