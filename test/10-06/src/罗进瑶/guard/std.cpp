#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("guard.in", "r", stdin);
	freopen("std.out", "w", stdout);
}
const int N = 210000;
struct T {
	ll x[7];
}a[N];
ll ans;
int n, d;
int main() {
	setIO();
	n = read();d=read();
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= d; ++j)
			a[i].x[j] = read();
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= n; ++j) {
			ll res = 0;
			for(int k = 1; k <= d; ++k)
				res += abs(a[i].x[k] - a[j].x[k]);
			if(res > ans) ans = res;
		}
	writeln(ans);
	return 0;
}
