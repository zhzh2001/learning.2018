#include<cstdio>
#include<cstring>
#include<ctime>
#include<algorithm>
#define ll long long
#define N 200005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
struct data{ ll a,b,c,d,e,num; }A[N];
ll n,m,ans; int e=1;
bool cmp(data x,data y) { return x.num<y.num; }
int main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n){
		A[i].a=read();
		if (m>1) A[i].b=read();
		if (m>2) A[i].c=read();
		if (m>3) A[i].d=read();
		if (m>4) A[i].e=read();
	}
	for (int a=-1;a<=1;a+=2)
	for (int b=-1;b<=1;b+=2)
	for (int c=-1;c<=1;c+=2)
	for (int d=-1;d<=1;d+=2){
		rep(i,1,n) A[i].num=A[i].a*a+A[i].b*b+A[i].c*c+A[i].d*d+A[i].e*e;
		sort(A+1,A+1+n,cmp);
		ans=max(ans,abs(A[n].a-A[1].a)+abs(A[n].b-A[1].b)+abs(A[n].c-A[1].c)+abs(A[n].d-A[1].d)+abs(A[n].e-A[1].e));
		rep(i,2,n-1){
			ans=max(ans,abs(A[n].a-A[i].a)+abs(A[n].b-A[i].b)+abs(A[n].c-A[i].c)+abs(A[n].d-A[i].d)+abs(A[n].e-A[i].e));
			ans=max(ans,abs(A[1].a-A[i].a)+abs(A[1].b-A[i].b)+abs(A[1].c-A[i].c)+abs(A[1].d-A[i].d)+abs(A[1].e-A[i].e));
		}
	}
	printf("%lld",ans);
}
