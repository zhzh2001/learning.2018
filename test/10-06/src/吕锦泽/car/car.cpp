#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 1000005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,ans,sum1[N],sum2[N],sum3[N]; char s[N];
int main(){
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	n=read(); scanf("%s",s+1);
	rep(i,1,n/2) swap(s[i],s[n-i+1]);
	rep(i,1,n){
		sum1[i]=sum1[i-1]+(s[i]=='B');
		sum2[i]=sum2[i-1]+(s[i]=='C');
		sum3[i]=sum3[i-1]+(s[i]=='S');
	}
	rep(i,1,n){
		rep(j,i+ans,n){
			ll x=sum1[j]-sum1[i-1];
			ll y=sum2[j]-sum2[i-1];
			ll z=sum3[j]-sum3[i-1];
			if (x!=y&&y!=z&&x!=z) ans=j-i+1;
		}
	}
	printf("%lld",ans);
}
