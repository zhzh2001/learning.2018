#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
#define M 8000000
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,a[N],cnt=1,m,left[M],right[M],num[M],maxn[M],ans[M];
void modfiy(ll l,ll r,ll pos,ll val,ll p){
	if (l==r) {
		num[p]+=val;
		maxn[p]=p;
		ans[p]=l;
		return;
	}
	ll mid=l+r>>1;
	if (pos<=mid){
		if (!left[p]) left[p]=++cnt;
		modfiy(l,mid,pos,val,left[p]);
	}else {
		if (!right[p]) right[p]=++cnt;
		modfiy(mid+1,r,pos,val,right[p]);
	}
	if (!right[p]) num[p]=num[left[p]],maxn[p]=maxn[left[p]];
	else if (!left[p]) num[p]=num[right[p]],maxn[p]=maxn[right[p]];
	else if (num[maxn[right[p]]]>num[maxn[left[p]]]){
		num[p]=num[right[p]];
		maxn[p]=maxn[right[p]];
		ans[p]=ans[right[p]];
	}else {
		num[p]=num[left[p]];
		maxn[p]=maxn[left[p]];
		ans[p]=ans[left[p]];
	}
}
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n){
		a[i]=read();
		modfiy(1,10000000,a[i],1,1);
	}
	rep(i,1,m){
		ll x=read(),y=read();
		modfiy(1,10000000,a[x],-1,1);
		modfiy(1,10000000,y,1,1);
		a[x]=y; printf("%d\n",ans[maxn[1]]);
	}
}
