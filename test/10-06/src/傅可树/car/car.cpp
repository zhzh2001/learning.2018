#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=1e6+5;
int n;
char s[N];
inline void init(){
	scanf("%d",&n);
	scanf("%s",s+1);
}
int cnt[30],ans;
inline void solve1(){
	for (int i=1;i<=n;i++){
		memset(cnt,0,sizeof cnt);
		for (int j=i;j<=n;j++){
			cnt[s[j]-'A']++;
			if (cnt['S'-'A']!=cnt['C'-'A']&&
			cnt['S'-'A']!=cnt['B'-'A']&&
			cnt['C'-'A']!=cnt['B'-'A']) {
				ans=max(ans,j-i+1);
			}
		}
	}
	printf("%d\n",ans);
}
inline void solve(){
	if (n<=10000){
		solve1();
	}else{
		printf("%d\n",n);
	}
}
int main(){
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	init();
	solve();
	return 0;
}
