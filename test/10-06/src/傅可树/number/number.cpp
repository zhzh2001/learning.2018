#include<cstdio>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e5+5,INF=1e7;
struct node{
	int son[2],mx,sum,MN;
}a[N*27];
int n,m,b[N],cnt,root;
inline void pushup(int k){
	if (a[a[k].son[0]].mx>a[a[k].son[1]].mx){
		a[k].MN=a[a[k].son[0]].MN;
		a[k].mx=a[a[k].son[0]].mx;
	}else{
		if (a[a[k].son[1]].mx>a[a[k].son[0]].mx){
			a[k].MN=a[a[k].son[1]].MN;
			a[k].mx=a[a[k].son[1]].mx;
		}else{
			a[k].MN=a[a[k].son[0]].MN;
			a[k].mx=a[a[k].son[0]].mx;
		}
	}
}
inline void update(int &k,int l,int r,int x,int v){
	if (!k) k=++cnt;
	if (l==r){a[k].sum+=v; a[k].mx+=v; a[k].MN=l; return;}
	int mid=(l+r)>>1;
	if (mid>=x) update(a[k].son[0],l,mid,x,v);
		else update(a[k].son[1],mid+1,r,x,v);
	pushup(k);
}
inline void init(){
	n=read(); m=read();
	for (int i=1;i<=n;i++) b[i]=read(),update(root,0,INF,b[i],1);
}
inline void solve(){
	for (int i=1;i<=m;i++){
		int x=read(),y=read();
		update(root,0,INF,b[x],-1);
		b[x]=y; update(root,0,INF,b[x],1);
		writeln(a[root].MN);
	}
}
int main(){
	freopen("number.in","r",stdin); freopen("number.out","w",stdout);
	init(); solve();
	return 0;
}
