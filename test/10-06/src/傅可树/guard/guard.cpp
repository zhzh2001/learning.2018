#include<cstdio>
#include<algorithm>
#include<cstring>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=2e5+5;
struct point{
	int d[5],id;
}b[N],c[N];
int n,D;
inline void init(){
	n=read(); D=read();
	for (int i=1;i<=n;i++){
		for (int j=0;j<D;j++){
			c[i].d[j]=b[i].d[j]=read();
		}
		b[i].id=i;
	}
}
struct node{
	int id,mn[5],mx[5];
}a[N*8];
int need[5],ans,cmpk;
inline bool cmp(point A,point B){
	return A.d[cmpk]<B.d[cmpk];
}
inline void pushup(int k,int son){
	for (int i=0;i<D;i++){
		a[k].mn[i]=min(a[k].mn[i],a[son].mn[i]);
		a[k].mx[i]=max(a[k].mx[i],a[son].mx[i]);
	}
}
void build(int k,int l,int r,int d){
	cmpk=d; int mid=(l+r)>>1; nth_element(b+l,b+mid+1,b+r+1,cmp);
	a[k].id=b[mid].id;
	for (int i=0;i<D;i++) a[k].mn[i]=a[k].mx[i]=b[mid].d[i];
	if (l==r) return;
	if (l<=mid-1) build(k<<1,l,mid-1,(d+1)%D),pushup(k,k<<1);
	if (r>=mid+1) build(k<<1|1,mid+1,r,(d+1)%D),pushup(k,k<<1|1);
}
inline int Abs(int x){
	return (x>0)?x:-x;
}
inline int dis(int x[],int y[]){
	int sum=0;
	for (int i=0;i<D;i++) sum+=Abs(x[i]-y[i]);
	return sum;
}
void query(int k,int l,int r){
	int tmp=0;
	for (int i=0;i<D;i++) tmp+=max(Abs(need[i]-a[k].mn[i]),Abs(need[i]-a[k].mx[i]));
	if (tmp<=ans) return;
	ans=max(ans,dis(need,c[a[k].id].d));
	if (l==r) return;
	int mid=(l+r)>>1;
	query(k<<1,l,mid-1);
	query(k<<1|1,mid+1,r);
}
inline void solve(){
	build(1,1,n,0); ans=0;
	for (int i=1;i<=n;i++){
		memcpy(need,b[i].d,sizeof b[i].d);
		query(1,1,n);
	}
	writeln(ans);
}
signed main(){
	freopen("guard.in","r",stdin); freopen("guard.out","w",stdout);
	init();
	solve();
	return 0;
}
