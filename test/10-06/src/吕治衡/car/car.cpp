#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}void write(ll x){if (x<0)putchar('-'),x=-x;if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
int n,f[1000010][3],ans,nxt[1000010],ff[1000010];
char s[1000010];
signed main(){
	freopen("car.in","r",stdin);freopen("car.out","w",stdout);
	n=read();scanf("%s",s+1);
	for (int i=1;i<=n;i++){
		f[i][0]=f[i-1][0];f[i][1]=f[i-1][1];f[i][2]=f[i-1][2];
		if (s[i]=='B')f[i][0]++;else if (s[i]=='C')f[i][1]++;else f[i][2]++;
	}for (int i=1;i<=n;i++)nxt[i]=i;
	for (int i=n-3;i>=1;i--)if (f[i+3][0]==f[i][0]+1&&f[i+3][1]==f[i][1]+1)nxt[i]=nxt[i+3];
	for (int i=n;i>=1;i--)if (s[i]==s[i+1])ff[i]=ff[i+1]+1;else ff[i]=1;
	for (int i=1;i<=n;i++)ans=max(ans,ff[i]);
	for (int i=n;i>=ans;i--)
		for (int j=1;j+ans-1<=i;j++){
			int xx=f[i][0]-f[j-1][0],yy=f[i][1]-f[j-1][1],zz=f[i][2]-f[j-1][2];
			if (xx==yy&&yy==zz){j=nxt[j];continue;}
			if (xx==yy&&s[j]=='S'||zz==yy&&s[j]=='B'||xx==zz&&s[j]=='C'){j+=ff[j];continue;}
			if (xx!=yy&&yy!=zz&&xx!=zz)ans=max(ans,i-j+1);
		}
	writeln(ans); 
}
/*
如果xx==yy==zz则可以通过nxt向后跳，否则慢慢走。 
xx=yy时可以跳一段连续的zz
9
CBBSSBCSC
*/ 
