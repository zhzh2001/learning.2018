#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0;char c=getchar();bool p=1;
	for(;!isdigit(c);c=getchar())if(c=='-')p=0;
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+(c^48);
	return p?x:-x;
}
const int N=200005;
int n,a[N],aa[N],cnt,m,l;
struct node{int num,sum;}b[N],t[N*4];
struct ask{int x,to;}k[N],kk[N];
bool cmp(ask a,ask b){return a.to<b.to;}
bool comp(node a,node b){return a.num<b.num;}
void pushup(int u){
	if(t[u<<1].sum<t[u<<1|1].sum)
		t[u]=t[u<<1|1];else t[u]=t[u<<1];
}
void build(int u,int l,int r){
	if(l==r){t[u]=b[l];return;}
	int mid=(l+r)>>1;
	build(u<<1,l,mid);
	build(u<<1|1,mid+1,r);
	pushup(u);
}
void update(int u,int l,int r,int v,int s){
	if(l==r){t[u].sum+=s;return;}
	int mid=(l+r)>>1;
	if(b[mid].num>=v)update(u<<1,l,mid,v,s);
			else update(u<<1|1,mid+1,r,v,s);
	pushup(u);
}
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)aa[i]=a[i]=read();
	sort(aa+1,aa+n+1);
	aa[0]=-1;
	for(int i=1;i<=n;i++)if(aa[i]!=aa[i-1])
		b[++cnt]=(node){aa[i],1};else b[cnt].sum++;
	for(int i=1;i<=m;i++){
		k[i].x=read();
		k[i].to=read();
		kk[i]=k[i];
	}
	sort(kk+1,kk+m+1,cmp);
	kk[0].to=-1;
	for(int i=1;i<=m;i++)if(kk[i].to!=kk[i-1].to){
		l=aa[lower_bound(aa+1,aa+n+1,kk[i].to)-aa];
		if(l!=kk[i].to)b[++cnt]=(node){kk[i].to,0};
	}
	sort(b+1,b+cnt+1,comp);
	build(1,1,cnt);
	for(int i=1;i<=m;i++){
		update(1,1,cnt,a[k[i].x],-1);
		update(1,1,cnt,k[i].to,1);
		a[k[i].x]=k[i].to;
		cout<<t[1].num<<'\n';
	}
	return 0;
}
