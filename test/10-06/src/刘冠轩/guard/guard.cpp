#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int x=0;char c=getchar();bool p=1;
	for(;!isdigit(c);c=getchar())if(c=='-')p=0;
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+(c^48);
	return p?x:-x;
}
int n,d,ans,x,s,a[200005][5];
signed main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read();d=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=d;j++)a[i][j]=read();
	if(n<=2501){
		for(int i=1;i<n;i++)
			for(int j=i+1;j<=n;j++){
				s=0;
				for(int k=1;k<=d;k++)
					s+=abs(a[i][k]-a[j][k]);
				ans=max(ans,s);
			}
		cout<<ans;
	}else if(d==1){
		int mx=0,mi=2147483647;
		for(int i=1;i<=n;i++)
			mx=max(mx,a[i][1]),mi=min(mi,a[i][1]);
		cout<<mx-mi;
	}else{
		srand(time(NULL));
		for(int i=1;i<=n;i++)
			for(int j=1;j<=800;j++){
				s=0;x=(x+rand()%n*rand())%n+1;
				for(int k=1;k<=d;k++)
					s+=abs(a[i][k]-a[x][k]);
				ans=max(ans,s);
			}
		cout<<ans;
	}
	return 0;
}
