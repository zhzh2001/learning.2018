#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0;char c=getchar();bool p=1;
	for(;!isdigit(c);c=getchar())if(c=='-')p=0;
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+(c^48);
	return p?x:-x;
}
const int N=100005;
char str[N];
int n,ans,B,C,S;
void calc(){
	if(B)
		if(C)
			if(S)if(B!=C&&B!=S&&C!=S)ans=max(ans,B+C+S);
			else if(B!=C)ans=max(ans,B+C);
		else if(S)if(B!=S)ans=max(ans,B+S);
			else ans=max(ans,B);
	else if(C)
			if(S)if(C!=S)ans=max(ans,C+S);
			else ans=max(ans,C);
		else if(S)ans=max(ans,S);
}
int main(){
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	n=read();
	if(n<=10001){
		scanf("%s",str+1);
		for(int i=1;i<=n;i++){
			B=C=S=0;
			for(int j=i;j<=n;j++){
				if(str[j]=='B')B++;
				else if(str[j]=='C')C++;
				else if(str[j]=='S')S++;
				calc();
			}
		}
		cout<<ans;
	}else cout<<n;
	return 0;
}
