#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e6+10;
int n,pre[N][3],ans;
char c[N];
namespace Subtask1{
	int ans,sum[3];
	inline int Get(char c){
		if (c=='C') return 0;
		if (c=='B') return 1;
		return 2;
	}
	inline void Main(){
		ans=0;
		For(i,1,n){
			sum[0]=sum[1]=sum[2]=0;
			For(j,i,n){
				sum[Get(c[j])]++;
				if (sum[0]!=sum[1]&&sum[1]!=sum[2]&&sum[0]!=sum[2]) ans=max(ans,j-i+1);
			}
		}
		printf("%d",ans);
	}	
};
inline int Get(char c){
	if (c=='C') return 0;
	if (c=='B') return 1;
	return 2;
}
int main(){
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	n=read(),scanf("%s",c+1);
	if (n<=10000) return Subtask1::Main(),0;
	bool flag=0;
	For(i,1,n) if (c[i]!='B') flag=1;
	if (!flag) return printf("%d",n),0;
	For(i,1,n){
		For(j,0,2) pre[i][j]=pre[i-1][j];
		pre[i][Get(c[i])]++;
	}
	For(i,1,n)
		Dow(j,n,i){
			if (j-i+1<=ans) break;
			int x=pre[j][0]-pre[i-1][0],y=pre[j][1]-pre[i-1][1],z=pre[j][2]-pre[i-1][2];
			if (x!=y&&y!=z&&x!=z) ans=j-i+1; 
		}
	printf("%d",ans);
}
