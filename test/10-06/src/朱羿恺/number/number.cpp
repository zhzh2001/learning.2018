#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,m,x,y,sum,a[N],b[N],cnt[10000010];
struct node{
	int cnt,val;
	inline bool operator < (const node &b)const{return cnt>b.cnt||cnt==b.cnt&&val<b.val;}
};
set<node>s;
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read(),m=read();
	For(i,1,n) a[i]=b[i]=read();
	sort(&b[1],&b[n+1]);
	x=b[1],sum=1;
	For(i,1,n)
		if (b[i]!=b[i+1]) s.insert((node){sum,x}),cnt[x]=sum,sum=1,x=b[i+1];
			else sum++;
	For(i,1,m){
		x=read(),y=read();
		s.erase((node){cnt[a[x]],a[x]});
		cnt[a[x]]--;
		if (cnt[a[x]]) s.insert((node){cnt[a[x]],a[x]});
		if (cnt[y]) s.erase((node){cnt[y],y});
		a[x]=y,cnt[y]++;
		s.insert((node){cnt[a[x]],a[x]});
		node t=*s.begin();
		printf("%d\n",t.val);
	}
}
