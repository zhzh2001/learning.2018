#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 2e5+10, inf = 1e9;
struct point{ll x[5];}a[N],b[N];
struct node{ll Max[5],Min[5];int ls,rs,size;point p;}t[N];
int n,k,rt,d;
ll ans,Max;
inline bool operator < (point a,point b){return a.x[d]<b.x[d];}
inline void push_up(int u){
    int ls=t[u].ls,rs=t[u].rs;
    For(i,0,k-1){
        t[u].Min[i]=t[u].Max[i]=t[u].p.x[i];
        if (ls) t[u].Min[i]=min(t[u].Min[i],t[ls].Min[i]),t[u].Max[i]=max(t[u].Max[i],t[ls].Max[i]);
        if (rs) t[u].Min[i]=min(t[u].Min[i],t[rs].Min[i]),t[u].Max[i]=max(t[u].Max[i],t[rs].Max[i]);
    }
    t[u].size=t[ls].size+t[rs].size+1;
}
inline int build(int l,int r,int D){
    int mid=l+r>>1;d=D,nth_element(b+l,b+mid,b+r+1),t[mid].p=b[mid];
    if (l<mid) t[mid].ls=build(l,mid-1,(D+1)%k);
    if (mid<r) t[mid].rs=build(mid+1,r,(D+1)%k);
    push_up(mid);return mid;
}
inline ll Dis(point a,point b){
	ll ans=0;
	For(i,0,k-1) ans+=abs(a.x[i]-b.x[i]);
	return ans;
}
inline ll GetMax(int u,point q){
    ll sum=0;
    For(i,0,k-1) sum+=max(abs(q.x[i]-t[u].Max[i]),abs(t[u].Min[i]-q.x[i]));
    return sum;
}
inline void QueryMax(int u,point q){
    Max=max(Max,Dis(q,t[u].p));
    int ls=t[u].ls,rs=t[u].rs;
	ll dl=-inf,dr=-inf;
    if (ls) dl=GetMax(ls,q);
    if (rs) dr=GetMax(rs,q);
    if (dl>dr){
        if (dl>Max) QueryMax(ls,q);
        if (dr>Max) QueryMax(rs,q);
    } 
    else {
        if (dr>Max) QueryMax(rs,q);
        if (dl>Max) QueryMax(ls,q);
    }
}
main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
    n=read(),k=read();
    For(i,1,n){
    	For(j,0,k-1) a[i].x[j]=read();
		b[i]=a[i];
	}
    rt=build(1,n,0);
    For(i,1,n) Max=-inf,QueryMax(rt,a[i]),ans=max(ans,Max);
    printf("%lld",ans);
}
