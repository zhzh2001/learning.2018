#include <ctime>
#include <cctype>
#include <cstdio>
#include <algorithm>

using namespace std;

typedef long long LL;

const int inf = 0x3f3f3f3f;
const int maxn = 200005;

int n, d;

#define dd c = getchar()
inline void read(LL& x)
{
	char dd;
	x = 0;
	bool f = false;
	for(; !isdigit(c); dd)
		if(c == '-')
			f = true;
	for(; isdigit(c); dd)
		x = (x << 1) + (x << 3) + (c ^ 48);
	if(f)
		x = -x;
}
#undef dd

#define dd c = getchar()
inline void read(int& x)
{
	char dd;
	x = 0;
	bool f = false;
	for(; !isdigit(c); dd)
		if(c == '-')
			f = true;
	for(; isdigit(c); dd)
		x = (x << 1) + (x << 3) + (c ^ 48);
	if(f)
		x = -x;
}
#undef dd

int begin_time;
inline int my_rand()
{
	static int seed = 2333333;
	return seed = (seed + 2434343) % n + 1;
}

int jz[20];

struct Point
{
	int wd[6];
	LL sum;
	friend bool operator < (Point a, Point b)
	{ return a.sum < b.sum; }
} p[maxn];

inline LL mhd(Point a, Point b)
{
	LL ans = 0;
	for(int i = 1; i <= d; ++i)
		ans += abs((LL) a.wd[i] - b.wd[i]);
	return ans;
}

inline void baoli()
{
	LL ans = 0;
	for(int i = 1; i <= n; ++i)
	{
		for(int j = 1; j <= d; ++j)
			read(p[i].wd[j]);
		for(int j = 1; j < n; ++j)
			ans = max(ans, mhd(p[i], p[j]));
	}
	printf("%lld\n", ans);
}

inline void solve_d_is_1()
{
	int maxx = -inf, minn = inf;
	for(int i = 1; i <= n; ++i)
	{
		int t;
		read(t);
		maxx = max(t, maxx);
		minn = min(t, minn);
	}
	printf("%lld\n", (LL) maxx - minn);
}

inline void pianfen()
{
	for(int i = 1; i <= d; ++i)
		jz[i] = my_rand();
	for(int i = 1; i <= n; ++i)
	{
		p[i].sum = 0;
		for(int j = 1; j <= d; ++j)
		{
			read(p[i].wd[j]);
			p[i].sum += (LL) p[i].wd[j] - jz[j];
		}
	}
	sort(p + 1, p + n + 1);
	LL ans = 0;
	for(int i = 1; i <= 3000; ++i)
		for(int j = max(i + 1, n - 3000); j <= n; ++j)
			ans = max(ans, mhd(p[i], p[j]));
	for(int i = 1; i <= d; ++i)
		jz[i] = my_rand();
	for(int i = 1; i <= n; ++i)
	{
		p[i].sum = 0;
		for(int j = 1; j <= d; ++j)
			p[i].sum += (LL) p[i].wd[j] - jz[j];
	}
	sort(p + 1, p + n + 1);
	for(int i = 1; i <= 3000; ++i)
		for(int j = max(i + 1, n - 3000); j <= n; ++j)
			ans = max(ans, mhd(p[i], p[j]));
	while(clock() < 1800)
		ans = max(ans, mhd(p[my_rand()], p[my_rand()]));
	printf("%lld\n", ans);
}

int main()
{
	freopen("guard.in", "r", stdin);
	freopen("guard.out", "w", stdout);
	begin_time = clock();
	read(n);
	read(d);
	if(!n)
		puts("0");
	else if(d == 1)
		solve_d_is_1();
	else if(n <= 3000)
		baoli();
	else
		pianfen();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
