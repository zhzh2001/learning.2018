#include <cstdio>
#include <cstdlib>
#include <algorithm>

using namespace std;

typedef long long LL;

const int maxn = 100000;

struct Point
{
	LL wd[6];
} p[maxn];

int n, d;

inline LL mhd(Point a, Point b)
{
	LL ans = 0;
	for(int i = 1; i <= d; ++i)
		ans += abs(a.wd[i] - b.wd[i]);
	return ans;
}

int main()
{
	scanf("%d%d", &n, &d);
	LL ans = 0;
	for(int i = 1; i <= n; ++i)
	{
		for(int j = 1; j <= d; ++j)
			scanf("%lld", &p[i].wd[j]);
		for(int j = 1; j < n; ++j)
			ans = max(ans, mhd(p[i], p[j]));
	}
	printf("%lld\n", ans);
	return 0;
}
