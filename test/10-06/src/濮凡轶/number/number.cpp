#include <cstdio>
#include <map>
#include <set>

using namespace std;

const int maxn = 100005;

int aa[maxn];

typedef map<int, int> mii;
typedef mii::iterator mit;

mii mp;

struct numb
{
	int num, cs;

	numb (int num, int cs)
	{
		this->num = num;
		this->cs = cs;
	}

	numb (pair<int, int> p)
	{
		this->num = p.first;
		this->cs = p.second;
	}

	friend operator < (numb a, numb b)
	{
		return (a.cs > b.cs) || (a.cs == b.cs && a.num < b.num);
	}

	friend operator == (numb a, numb b)
	{
		return (a.cs == b.cs) && (a.num == b.num);
	}
};

typedef set<numb> St;
typedef St::iterator Sit;
St st;

int main()
{
	freopen("number.in", "r", stdin);
	freopen("number.out", "w", stdout);
	st.clear();
	mp.clear();
	int n, m;
	scanf("%d%d", &n, &m);
	for(int i = 1; i <= n; ++i)
	{
		scanf("%d", &aa[i]);
		mp[aa[i]]++;
	}
	for(mit it = mp.begin(); it != mp.end(); ++it)
		st.insert(numb(*it));
	while(m--)
	{
		int from, to;
		scanf("%d%d", &from, &to);
		Sit it = st.find(numb(aa[from], mp[aa[from]]));
		st.erase(it);
		mp[aa[from]]--;
		if(mp[aa[from]] > 0)
			st.insert(numb(aa[from], mp[aa[from]]));
		it = st.find(numb(to, mp[to]));
		if(it != st.end())
			st.erase(it);
		mp[to]++;
		st.insert(numb(to, mp[to]));
		aa[from] = to;
		printf("%d\n", st.begin()->num);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
