/*#include <cstdio>
#include <cstring>
#include <algorithm>

using std::max;

typedef long long LL;

const int maxn = 100005;

char aa[maxn];
int a[maxn], b[maxn], c[maxn];

int main()
{
	freopen("car.in", "r", stdin);
	freopen("car.out", "w", stdout);
	int n;
	scanf("%d%s", &n, aa + 1);
	for(int i = 1; i <= n; ++i)
	{
		a[i] = a[i-1];
		b[i] = b[i-1];
		c[i] = c[i-1];
		switch(aa[i])
		{
			case 'B':
				a[i]++;
				break;
			case 'C':
				b[i]++;
				break;
			case 'S':
				c[i]++;
				break;
		}
	}
	int ans = 1;
	for(int i = 1; i <= n; ++i)
	{
		for(int j = n; j > i; --j)
		{
			int A = a[j] - a[i-1];
			int B = b[j] - a[i-1];
			int C = c[j] - c[i-1];
			if(A != B && B != C && C != A)
			{
				ans = max(ans, j - i + 1);
				break;
			}
		}
	}
	printf("%d", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}*/

#include <cstdio>
#include <cstring>
#include <algorithm>

using std::max;

typedef long long LL;

const int maxn = 100005;

char aa[maxn];
int a[maxn], b[maxn], c[maxn];

int main()
{
	freopen("car.in", "r", stdin);
	freopen("car.out", "w", stdout);
	int n;
	scanf("%d%s", &n, aa + 1);
	for(int i = 1; i <= n; ++i)
	{
		a[i] = a[i-1];
		b[i] = b[i-1];
		c[i] = c[i-1];
		switch(aa[i])
		{
			case 'B':
				a[i]++;
				break;
			case 'C':
				b[i]++;
				break;
			case 'S':
				c[i]++;
				break;
		}
	}
	for(int i = n; i; --i)
	{
		for(int j = i; j <= n; ++j)
		{
			int A = a[j] - a[j-i];
			int B = b[j] - a[j-i];
			int C = c[j] - c[j-i];
			if(A != B && B != C && C != A)
			{
				printf("%d", i);
				fclose(stdin);
				fclose(stdout);
				return 0;
			}
		}
	}
	puts("2");
	fclose(stdin);
	fclose(stdout);
	return 0;
}
