#include <iostream>
#include <cstdio>
#include <queue>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
#define dd c=gc()
inline ll read(){
	ll x=0,f=1;char dd;
	for(;!isdigit(c);dd){if(c=='-')f=-1;if(c==EOF)return EOF;}
	for(;isdigit(c);dd)x=(x<<1)+(x<<3)+(c^48);return x*f;
}
#undef dd
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5;
ll a[N+10],t[10000010],n,m;
struct node{
	node(){}
	node(ll X,ll Num) {num=Num;x=X;}
	ll x,num;
	bool operator <(const node&res)const{
		return num==res.num?x>res.x:num<res.num;
	}
};
priority_queue<node>H;
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read(),m=read();
	for (ll i=1;i<=n;++i) a[i]=read(),++t[a[i]];
	for (ll i=1;i<=n;++i) H.push(node(a[i],t[a[i]]));
	for (ll x,y;m;--m){
		x=read(),y=read();
		--t[a[x]];
		H.push(node(a[x],t[a[x]]));
		a[x]=y;
		++t[a[x]];
		H.push(node(a[x],t[a[x]]));
		while (H.top().num!=t[H.top().x]) H.pop();
		wr(H.top().x),puts("");
	}
	return 0; 
}
