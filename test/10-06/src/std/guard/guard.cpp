#include <cstdio>
#include <iostream>
using namespace std;
#define gc c = getchar()
int read(){
	int x = 0, f = 1; char gc;
	for(; !isdigit(c); gc) if(c == '-') f = -1;
	for(; isdigit(c); gc) x = x * 10 + c - '0';
	return x * f;
}
#undef gc
const long long INF = 1ll << 60;
int n, d, v[10], a[200005][10];
long long ans = -INF;
void dfs(int k){
	if(k > d) {
		long long tmp;
		long long Maxx = -INF, Maxy = -INF;
		for(int i = 1; i <= n; i++) {
			tmp = 0;
			for(int j = 1; j <= d; j++) tmp += v[j] * a[i][j];
			if(tmp > Maxx) Maxx = tmp;
			tmp = 0;
			for(int j = 1; j <= d; j++) tmp += -1 * v[j] * a[i][j];
			if(tmp > Maxy) Maxy = tmp;
		}
		ans = max(ans, Maxx + Maxy);
		return;
	}
	v[k] = -1; dfs(k + 1);
	v[k] = 1; dfs(k + 1);
}
int main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n = read(), d = read();
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= d; j++)
			a[i][j] = read();
	dfs(1);
	printf("%lld", ans);
}
