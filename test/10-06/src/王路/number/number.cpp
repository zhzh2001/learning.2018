#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <set>
#include <map>
#include <functional>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 1e5 + 5;

struct Node {
  int val, cnt;
  explicit inline Node(int a, int b) : val(a), cnt(b) {}
  inline bool operator<(const Node &other) const {
    if (cnt == other.cnt)
      return val > other.val;
    return cnt < other.cnt;
  }
};

int n, m, a[kMaxN];
set<Node> S;
map<int, int> cnt;

int main() {
  freopen("number.in", "r", stdin);
  freopen("number.out", "w", stdout);
  Read(n), Read(m);
  for (int i = 1; i <= n; ++i) {
    Read(a[i]);
    ++cnt[a[i]];
  }
  for (map<int, int>::iterator it = cnt.begin(); it != cnt.end(); ++it) {
    S.insert(Node(it->first, it->second));
  }
  for (int i = 1, x, y; i <= m; ++i) {
    Read(x), Read(y);
    S.erase(Node(y, cnt[y]));
    S.erase(Node(a[x], cnt[a[x]]));
    --cnt[a[x]];
    ++cnt[y];
    S.insert(Node(a[x], cnt[a[x]]));
    S.insert(Node(y, cnt[y]));
    a[x] = y;
    printf("%d\n", S.rbegin()->val);
  }
  return 0;
}