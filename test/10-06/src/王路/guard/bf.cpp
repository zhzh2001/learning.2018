#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
const int kMaxN = 2e5 + 5;

struct Node {
  ll pos[5];
} a[kMaxN];

int n, d;

int main() {
  freopen("guard.in", "r", stdin);
  freopen("bf.out", "w", stdout);
  scanf("%d%d", &n, &d);
  for (int i = 1; i <= n; ++i)
    for (int j = 0; j < d; ++j)
      scanf("%lld", &a[i].pos[j]);
  ll ans = 0;
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= n; ++j) {
      ll tmp = 0;
      for (int k = 0; k < d; ++k)
        tmp += llabs(a[i].pos[k] - a[j].pos[k]);
      ans = max(ans, tmp);
    }
  }
  printf("%lld\n", ans);
  return 0;
}