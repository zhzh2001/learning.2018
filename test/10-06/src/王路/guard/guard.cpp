#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<int, int> pii;
const int kMaxN = 2e5 + 5, kMaxD = 5;
int n, d;
struct Node {
  ll pos[5];
} a[kMaxN];
struct Atom {
  ll pos[kMaxD * kMaxD];
} c[kMaxN];

namespace Spec {
ll a[kMaxN];
int Solve() {
  for (int i = 1; i <= n; ++i)
    Read(a[i]);
  sort(a + 1, a + n + 1);
  printf("%lld\n", a[n] - a[1]);
  return 0;
}
} // namespace Spec

namespace Solution { // for d >= 2
inline Atom Transform(const Node &cur) {
  Atom ret;
  for (int i = 0; i < (1 << (d - 1)); ++i) {
    ret.pos[i] = cur.pos[0];
    for (int j = 0; j < d - 1; ++j)
      if (i & (1 << j)) {
        ret.pos[i] += cur.pos[j + 1];
      } else {
        ret.pos[i] -= cur.pos[j + 1];
      }
  }
  return ret;
}
static int sid = -1;
inline bool cmp(const Atom &a, const Atom &b) {
  return a.pos[sid] < b.pos[sid];
}
int Solve() {
  for (int i = 1; i <= n; ++i) {
    for (int j = 0; j < d; ++j)
      Read(a[i].pos[j]);
  }
  for (int i = 1; i <= n; ++i)
    c[i] = Transform(a[i]);
  int mx = 1 << (d - 1);
  ll ans = 0;
  for (int i = 0; i < mx; ++i) {
    sid = i;
    sort(c + 1, c + n + 1, cmp);
    ans = max(ans, c[n].pos[i] - c[1].pos[i]);
  }
  printf("%lld\n", ans);
  return 0;
}
} // namespace Solution

int main() {
  freopen("guard.in", "r", stdin);
  freopen("guard.out", "w", stdout);
  Read(n), Read(d);
  if (d == 1)
    return Spec::Solve(); // 5pts, which makes it no segment fault
  return Solution::Solve(); // 100pts
}
