#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

inline int Rand32() { return (rand() << 15) + rand(); }
inline int Uniform32(int l, int r) { return (Rand32() % (r - l + 1)) + l; }

int main() {
  freopen("guard.in", "w", stdout);
  srand(GetTickCount());
  int n = 2500, d = rand() % 5 + 1;
  printf("%d %d\n", n, d);
  for (int i = 1; i <= n; ++i) {
    for (int j = 0; j < d; ++j) {
      printf("%d%c", Uniform32(-1e9, 1e9), " \n"[j == d - 1]);
    }
  }
  return 0;
}