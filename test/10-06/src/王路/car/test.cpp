#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 1000005;
int n;
char s[kMaxN];
int f[30][30][30][30];

int main() {
  freopen("car.in", "r", stdin);
  freopen("car.out", "w", stdout);
  scanf("%d\n%s", &n, s + 1);
  int ans = 1;
  if (s[1] == 'B')
    f[1][1][0][0] = 0;
  else if (s[1] == 'S')
    f[1][0][1][0] = 0;
  else if (s[1] == 'C')
    f[1][0][0][1] = 0;
  for (int i = 1; i <= n; ++i) {
    for (int j = 0; j <= i; ++j) {
      for (int k = 0; k <= i; ++k) {
        for (int p = 0; p <= i; ++p) {
          ans = max(ans, f[i][j][k][p]);
          if (s[i + 1] == 'B') {
            f[i + 1][j + 1][k][p] = max(f[i + 1][j + 1][k][p], f[i][j][k][p] + 1);
          } else if (s[i + 1] == 'S') {
            f[i + 1][j][k + 1][p] = max(f[i + 1][j][k + 1][p], f[i][j][k][p] + 1);
          } else if (s[i + 1] == 'C') {
            f[i + 1][j][k][p + 1] = max(f[i + 1][j][k][p + 1], f[i][j][k][p] + 1);
          }
        }
      }
    }
  }
  printf("%d\n", ans);
  return 0;
}