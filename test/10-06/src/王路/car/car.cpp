#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <ctime>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 1000005;
int n;
char s[kMaxN];

namespace SubTask1 {
int Solve() {
  int ans = 0;
  for (int r = 1; r <= n; ++r) {
    int cnts = 0, cntb = 0, cntc = 0;
    for (int l = r; l >= 1; --l) {
      cnts += s[l] == 'S';
      cntb += s[l] == 'B';
      cntc += s[l] == 'C';
      if (cnts != cntb && cnts != cntc && cntb != cntc)
        ans = max(ans, r - l + 1);
    }
  }
  printf("%d\n", ans);
  return 0;
}
} // namespace SubTask1

namespace Solution {
const int kSubMaxN = ::kMaxN;
int sum_c[kSubMaxN], sum_s[kSubMaxN], sum_b[kSubMaxN], f[kSubMaxN];
int Solve() {
  int ans = 0;
  for (int i = 1; i <= n; ++i) {
    sum_c[i] = sum_c[i - 1] + s[i] == 'C';
    sum_s[i] = sum_s[i - 1] + s[i] == 'S';
    sum_b[i] = sum_b[i - 1] + s[i] == 'B';
    if (sum_c[i] != sum_s[i] && sum_c[i] != sum_b[i] && sum_b[i] != sum_s[i])
      ans = i;
  }
  int l = 1, r = 1, len = 1;
  vector<pii> vec;
  for (; r <= n; ++r) {
    int c = sum_c[r] - sum_c[l - 1],
        s = sum_s[r] - sum_s[l - 1],
        b = sum_b[r] - sum_b[l - 1];
    while (c != s || c != b || s != b) {
      ++l;
      c = sum_c[r] - sum_c[l - 1];
      s = sum_s[r] - sum_s[l - 1];
      b = sum_b[r] - sum_b[l - 1];
    }
    f[r] = r - l + 1;
    if (len > f[r]) {
      vec.clear();
      vec.push_back(make_pair(l, r));
    } else if (len == f[r])
      vec.push_back(make_pair(l, r));
  }
  for (int i = 0; i < (int)vec.size() && i < 100; ++i) {
    for (int j = vec[i].first; j >= 1; --j) {
      int r = vec[i].second, l = j;
      int c = sum_c[r] - sum_c[l - 1],
          s = sum_s[r] - sum_s[l - 1],
          b = sum_b[r] - sum_b[l - 1];
      if (c != s && c != b && s != b)
        ans = max(ans, r - l + 1);
    }
  }
  printf("%d\n", ans);
  return 0;
}
} // namespace Solution

int main() {
  freopen("car.in", "r", stdin);
  freopen("car.out", "w", stdout);
  scanf("%d\n%s", &n, s + 1);

  // if (n <= 10000)
  //   return SubTask1::Solve(); // 40pts, 3s, O2

  bool same = s[1] == 'B';
  for (int i = 2; i <= n; ++i)
    same &= s[i] == 'B';
  if (same)
    return printf("%d\n", n), 0; // 4pts

  return Solution::Solve(); // 100pts, 3s, O2, MLE
}