#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int main() {
  freopen("car.in", "w", stdout);
  srand(GetTickCount());
  int n = 10000;
  printf("%d\n", n);
  for (int i = 1; i <= n; ++i) {
    int c = rand() % 3;
    if (c == 0)
      putchar('S');
    else if (c == 1)
      putchar('C');
    else putchar('B');
  }
  puts("");
  return 0;
}