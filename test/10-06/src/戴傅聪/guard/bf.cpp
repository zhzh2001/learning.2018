#include<bits/stdc++.h>
using namespace std;
int d,n;
int cmpbase[5];
struct node
{
	int a[5];
	inline void read(){for(int i=0;i<d;i++) scanf("%d",a+i);}
	inline long long calccmp(){long long ans=0;for(int i=0;i<d;i++) ans+=1ll*a[i]*cmpbase[i];return ans;}
}a[200001];
inline bool cmp(node a,node b){return a.calccmp()<b.calccmp();}
inline long long Abs(long long a){return a<0?-a:a;}
inline long long calc(node a,node b)
{
	long long ans=0;
	for(int i=0;i<d;i++) ans+=Abs(1ll*a.a[i]-1ll*b.a[i]);
	return ans;
}
long long ans=0;
inline void getans()
{
	node findmin=a[1];
	for(int i=2;i<=n;i++) if(cmp(findmin,a[i])) findmin=a[i];
	for(int i=1;i<=n;i++) ans=max(ans,calc(findmin,a[i]));
}
inline void dfs(int cur)
{
	if(cur==d) return (void)getans();
	cmpbase[cur]=-1;dfs(cur+1);
	cmpbase[cur]=1;dfs(cur+1);
}
signed main()
{
	freopen("guard.in","r",stdin);
	freopen("bf.out","w",stdout);
	scanf("%d%d",&n,&d);
	for(int i=1;i<=n;i++) a[i].read();
	for(int i=1;i<=n;i++) for(int j=i+1;j<=n;j++) ans=max(ans,calc(a[i],a[j]));
	printf("%lld",ans);return 0;
}
