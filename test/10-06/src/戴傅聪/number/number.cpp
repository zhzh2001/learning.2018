#include<bits/stdc++.h>
using namespace std;
#define maxn 100001
map<int,int> cnt;
struct node
{
	int num,cnt;
	node(){}
	node(int a,int b){num=a;cnt=b;}
	bool operator <(const node &p)const{return cnt==p.cnt?num<p.num:cnt>p.cnt;}
};
multiset<node> s;
int n,m,a[maxn];
inline void init()
{
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
	{
		scanf("%d",a+i);
		cnt[a[i]]++;
	}
	for(map<int,int>::iterator it=cnt.begin();it!=cnt.end();it++) s.insert(node(it->first,it->second));
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	init();
	while(m--)
	{
		int x,y;scanf("%d%d",&x,&y);
		int t=a[x],c=cnt[a[x]]--;
		s.erase(s.find(node(t,c)));
		if(c-1) s.insert(node(t,c-1));
		a[x]=y;c=cnt[y]++;
		if(c) s.erase(s.find(node(y,c)));
		s.insert(node(y,c+1));
		printf("%d\n",s.begin()->num);
	}
}
/*
3 2
1 1 1
2 2
3 2
*/
