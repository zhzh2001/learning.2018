#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
ll sum,ans;
int n,d,i,j,k,a[200001][5];
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int rd(){
	int x=0,fl=1;char ch=gc();
	for (;ch<48||ch>57;ch=gc())if(ch=='-')fl=-1;
	for (;48<=ch&&ch<=57;ch=gc())x=(x<<3)+(x<<1)+(ch^48);
	return x*fl;
}
int main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=rd();d=rd();
	for (i=0;i<n;i++){
		for (j=0;j<d;j++) a[i][j]=rd();
		for (k=0;k<i;k++){
			sum=0;
			for (j=0;j<d;j++) sum+=abs((ll)a[i][j]-a[k][j]);
			ans=max(ans,sum);
		}
	}
	printf("%lld",ans);
}
