#include<bits/stdc++.h>
using namespace std;
const int N=100002;
struct node{
	int x,y;
	friend bool operator <(node a,node b){
		return a.x>b.x || a.x==b.x && a.y<b.y;
	}
};
int n,m,x,y,mp[10000002],a[N],i;
set<node>S;
set<node>::iterator it;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int rd(){
	int x=0,fl=1;char ch=gc();
	for (;ch<48||ch>57;ch=gc())if(ch=='-')fl=-1;
	for (;48<=ch&&ch<=57;ch=gc())x=(x<<3)+(x<<1)+(ch^48);
	return x*fl;
}
void wri(int x){if(x<0)putchar('-'),wri(-x);if(x>=10)wri(x/10);putchar(x%10|48);}
void wln(int x){wri(x);putchar('\n');}
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=rd();m=rd();
	for (i=1;i<=n;i++) a[i]=rd(),mp[a[i]]++;
	for (i=1;i<=n;i++) S.insert((node){mp[a[i]],a[i]});
	while (m--){
		x=rd();y=rd();
		S.erase((node){mp[a[x]],a[x]});
		S.insert((node){--mp[a[x]],a[x]});
		S.erase((node){mp[y],y});
		S.insert((node){++mp[y],y});
		it=S.begin();
		wln(it->y);
		a[x]=y;
	}
}
