#include <cstdio>
#include <algorithm>
using namespace std;
#define int long long
const int N=200005;
struct Pot{int x[5];}a[N],p[N];
inline bool cmp(Pot a,Pot b){return (a.x[1]!=b.x[1])?(a.x[1]<b.x[1]):(a.x[2]<b.x[2]);}
inline bool cmp1(Pot a,Pot b){return (a.x[2]!=b.x[2])?(a.x[2]<b.x[2]):(a.x[1]<b.x[1]);}
inline bool cmp2(Pot a,Pot b){return (a.x[1]!=b.x[1])?(a.x[1]<b.x[1]):(a.x[2]!=b.x[2])?a.x[2]<b.x[2]:a.x[3]<b.x[3];}
inline bool cmp3(Pot a,Pot b){return (a.x[2]!=b.x[2])?(a.x[2]<b.x[2]):(a.x[1]!=b.x[1])?a.x[1]<b.x[1]:a.x[3]<b.x[3];}
int n,d,cnt=0;
signed main()
{
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	int i,j,k; long long re=0,tmp=0; scanf("%lld%lld",&n,&d);
	for(i=1;i<=n;i++){for(j=1;j<=d;j++)scanf("%lld",&a[i].x[j]);}
	if(d==1){sort(a+1,a+n+1,cmp); re=(long long)(a[n].x[1]-a[1].x[1]); printf("%lld\n",re);}
	else if(n<=2500)
	{
		for(i=1;i<=n;i++)
		{
			for(j=1;j<=n;j++)
			{
				tmp=0LL;for(k=1;k<=d;k++){tmp=tmp+1LL*(long long)abs(a[i].x[k]-a[j].x[k]);}re=max(re,tmp);
			}
		}printf("%lld\n",re);
	}
	else if(d==2)
	{
		sort(a+1,a+n+1,cmp); for(i=1;i<=1000;i++){p[++cnt]=a[i];p[++cnt]=a[n-i+1];}
		sort(a+1,a+n+1,cmp1); for(i=1;i<=1000;i++){p[++cnt]=a[i];p[++cnt]=a[n-i+1];}
		for(i=1;i<=cnt;i++)
		{
			for(j=1;j<=cnt;j++)
			{
				re=max(re,abs(p[i].x[1]-p[j].x[1])+abs(p[i].x[2]-p[j].x[2]));
			}
		}printf("%lld\n",re);
	}
	else
	{
		sort(a+1,a+n+1,cmp2); for(i=1;i<=1000;i++){p[++cnt]=a[i];p[++cnt]=a[n-i+1];}
		sort(a+1,a+n+1,cmp3); for(i=1;i<=1000;i++){p[++cnt]=a[i];p[++cnt]=a[n-i+1];}
		for(i=1;i<=cnt;i++)
		{
			for(j=1;j<=cnt;j++)
			{
				tmp=0; for(k=1;k<=d;k++){tmp+=(abs(p[i].x[k]-p[j].x[k]));} re=max(re,tmp);
			}
		}printf("%lld\n",re);
	}
}
