#include <cstdio>
#include <algorithm>
using namespace std;
const int N=100005,B=10000005;
int n,m,co[B],a[N];
#define jud(x,y) co[x]>co[y]||(co[x]==co[y]&&x<y)
struct stetree{int l,r,ma,oo;}Tree[N<<2];
#define c1 x<<1
#define c2 x<<1|1
inline void Up(int x){if(Tree[c1].ma>Tree[c2].ma||(Tree[c1].ma==Tree[c2].ma&&Tree[c1].oo<Tree[c2].oo)){Tree[x].ma=Tree[c1].ma,Tree[x].oo=Tree[c1].oo;}else {Tree[x].ma=Tree[c2].ma,Tree[x].oo=Tree[c2].oo;}}
inline void build(int l,int r,int x){Tree[x].l=l; Tree[x].r=r; if(l==r){Tree[x].ma=0;Tree[x].oo=l;return;} int mid=(l+r)>>1; build(l,mid,c1); build(mid+1,r,c2); Up(x);}
inline void ins(int x,int po,int v){if(Tree[x].l==Tree[x].r){Tree[x].ma+=v;return;} int mid=(Tree[x].l+Tree[x].r)>>1; if(po<=mid)ins(c1,po,v); else ins(c2,po,v); Up(x);}
inline int que(int x){if(Tree[x].l==Tree[x].r)return Tree[x].oo; int mid=(Tree[x].l+Tree[x].r)>>1; if(Tree[c1].ma>Tree[c2].ma||(Tree[c1].ma==Tree[c2].ma&&Tree[c1].oo<Tree[c2].oo))return que(c1);else return que(c2);}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	int i,ma=0,x,y,re=0; scanf("%d%d",&n,&m);
	for(i=1;i<=n;i++) {scanf("%d",&a[i]); co[a[i]]++; ma=max(ma,a[i]); if(jud(a[i],re))re=a[i];}
	if(ma<=100) {while(m--){scanf("%d%d",&x,&y); ma=max(ma,y); if(a[x]==y){printf("%d\n",re);continue;} co[a[x]]--; co[y]++; a[x]=y; for(i=0;i<=ma;i++)if(jud(i,re))re=i; printf("%d\n",re);}}
	else if(n<=2000){while(m--){scanf("%d%d",&x,&y); if(a[x]==y){printf("%d\n",re);continue;} co[a[x]]--; co[y]++; a[x]=y; for(i=1;i<=n;i++)if(jud(a[i],re))re=a[i]; printf("%d\n",re);}}
	else {build(1,100000,1); for(i=1;i<=n;i++)ins(1,a[i],1); while(m--){scanf("%d%d",&x,&y); ins(1,a[x],-1); ins(1,y,1); a[x]=y; printf("%d\n",que(1));}}
}
