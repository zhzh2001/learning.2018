#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define int long long
#define N 40005
int n,m,c[N],tot=0,blo,l[N],r[N],cnt=0,mp[N],pos[N];
int f[205][N],g[205][N],co[N];
struct node{int w,id,x;}a[N];
inline bool cmp(node a,node b){return a.w!=b.w?a.w<b.w:a.id<b.id;}
inline bool cmp1(node a,node b){return a.id<b.id;}
inline int ask(int x,int y)
{
	int i,re=0;
	if(pos[y]-pos[x]<2)
	{
		for(i=x;i<=y;i++){co[a[i].x]++;if(co[a[i].x]>co[re]||(co[a[i].x]==co[re]&&a[i].x<re))re=a[i].x;}
		for(i=x;i<=y;i++)co[a[i].x]--; return mp[re];
	}
	else
	{
		re=g[pos[x]+1][pos[y]-1];
		for(i=x;i<=r[pos[x]];i++)
		{
			co[a[i].x]++;
			if(co[a[i].x]+f[pos[y]-1][a[i].x]-f[pos[x]][a[i].x]>co[re]+f[pos[y]-1][re]-f[pos[x]][re])re=a[i].x;
			else if(co[a[i].x]+f[pos[y]-1][a[i].x]-f[pos[x]][a[i].x]==co[re]+f[pos[y]-1][re]-f[pos[x]][re]&&a[i].x<re)re=a[i].x;
		}
		for(i=l[pos[y]];i<=y;i++)
		{
			co[a[i].x]++;
			if(co[a[i].x]+f[pos[y]-1][a[i].x]-f[pos[x]][a[i].x]>co[re]+f[pos[y]-1][re]-f[pos[x]][re])re=a[i].x;
			else if(co[a[i].x]+f[pos[y]-1][a[i].x]-f[pos[x]][a[i].x]==co[re]+f[pos[y]-1][re]-f[pos[x]][re]&&a[i].x<re)re=a[i].x;
		}
		for(i=x;i<=r[pos[x]];i++)co[a[i].x]--; for(i=l[pos[y]];i<=y;i++)co[a[i].x]--; return mp[re];
	}
}
signed main()
{
	int i,j,po,re,x,y; scanf("%lld%lld",&n,&m); blo=sqrt(n); cnt=n/blo+(bool)(n%blo);
	for(i=1;i<=cnt;i++)l[i]=(i-1)*blo+1,r[i]=i*blo; r[cnt]=n; for(i=1;i<=n;i++)pos[i]=(i-1)/blo+1;
	for(i=1;i<=n;i++) {scanf("%lld",&a[i].w); a[i].id=i;} sort(a+1,a+n+1,cmp);
	for(i=1;i<=n;i++)if(a[i].w!=a[i-1].w){mp[++tot]=a[i].w; a[i].x=tot;}else a[i].x=a[i-1].x; sort(a+1,a+n+1,cmp1);
	memset(f,0,sizeof f); for(i=1;i<=n;i++)f[pos[i]][a[i].x]++; memset(co,0,sizeof co);
	for(i=2;i<=cnt;i++)for(j=1;j<=tot;j++)f[i][j]+=f[i-1][j];
	for(i=1;i<=cnt;i++)
	{
		po=l[i]; re=0;
		for(j=po;j<=n;j++)
		{
			co[a[j].x]++;if(co[a[j].x]>co[re]||(co[a[j].x]==co[re]&&a[j].x<re))re=a[j].x; g[i][pos[j]]=re;
		}for(j=po;j<=n;j++)co[a[j].x]--;
	}
	for(i=1;i<=m;i++)
	{
		scanf("%lld%lld",&x,&y); x=(x+la-1)%n+1; y=(y+la-1)%n+1; if(x>y)swap(x,y); la=ask(x,y);	printf("%lld\n",la);
	}
}
