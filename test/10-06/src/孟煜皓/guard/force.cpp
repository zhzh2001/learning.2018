#include <cstdio>
#include <cctype>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = !f;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
int n, d;
long long a[200005][5], ans, s, mx, mn;
int main(){
	freopen("guard.in", "r", stdin);
	freopen("guard.ans", "w", stdout);
	n = read(), d = read();
	for (register int i = 1; i <= n; ++i)
		for (register int j = 0; j < d; ++j)
			a[i][j] = read();
	ans = 0;
	for (register int i = 1; i < n; ++i)
		for (register int j = i + 1; j <= n; ++j){
			s = 0;
			for (register int k = 0; k < d; ++k)
				s += a[i][k] > a[j][k] ? a[i][k] - a[j][k] : a[j][k] - a[i][k];
			ans = std :: max(ans, s);
		}
	return printf("%lld", ans), 0;
}