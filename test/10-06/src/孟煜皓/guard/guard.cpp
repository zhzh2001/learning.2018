#include <cstdio>
#include <cctype>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = !f;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
int n, d;
long long a[200005][5], ans, s, mx, mn;
int main(){
	freopen("guard.in", "r", stdin);
	freopen("guard.out", "w", stdout);
	n = read(), d = read();
	for (register int i = 1; i <= n; ++i)
		for (register int j = 0; j < d; ++j)
			a[i][j] = read();
	ans = 0;
	for (register int p = 0; p < (1 << d); ++p){
		mx = -1000000000000ll, mn = 1000000000000ll;
		for (register int i = 1; i <= n; ++i){
			s = 0;
			for (register int j = 0; j < d; ++j)
				if (p & (1 << j)) s -= a[i][j]; else s += a[i][j];
			mx = std :: max(mx, s), mn = std :: min(mn, s);
		}
		ans = std :: max(ans, mx - mn);
	}
	printf("%lld", ans);
}