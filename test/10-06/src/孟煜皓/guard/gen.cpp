#include <cstdio>
#include <cstdlib>
#include <windows.h>
#include <algorithm>
long long rnd(){
	return (1ll * rand() << 45) + (1ll * rand() << 30) + (1ll * rand() << 15) + 1ll * rand();
}
long long rnd(long long a, long long b){
	return rnd() % (b - a + 1) + a;
}
int main(){
	srand(GetTickCount());
	freopen("guard.in", "w", stdout);
	int n = rnd(1, 3000), d = rnd(1, 5);
	printf("%d %d\n", n, d);
	for (register int i = 1; i <= n; ++i){
		for (register int j = 0; j < d; ++j)
			printf("%lld ", rnd(-2147483647, 2147483647));
		putchar('\n');
	}
}