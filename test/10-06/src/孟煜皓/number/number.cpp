#include <cstdio>
#include <cctype>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = !f;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
#define M 1000005
int n, m, a[N];
struct Segment_Tree{
	int cnt, ls[M], rs[M], mx[M], num[M], rt;
	void add(int &u, int l, int r, int x, int y){
		if (!u) u = ++cnt, num[u] = l;
		if (l == r) return mx[u] += y, void(0);
		int mid = (l + r) >> 1;
		if (x <= mid) add(ls[u], l, mid, x, y);
		else add(rs[u], mid + 1, r, x, y);
		mx[u] = std :: max(mx[ls[u]], mx[rs[u]]);
		num[u] = mx[ls[u]] >= mx[rs[u]] ? num[ls[u]] : num[rs[u]];
	}
}T;
int main(){
	freopen("number.in", "r", stdin);
	freopen("number.out", "w", stdout);
	n = read(), m = read();
	for (register int i = 1; i <= n; ++i)
		a[i] = read(), T.add(T.rt, 1, 10000001, a[i] + 1, 1);
	while (m--){
		int x = read(), y = read();
		T.add(T.rt, 1, 10000001, a[x] + 1, -1);
		T.add(T.rt, 1, 10000001, y + 1, 1);
		printf("%d\n", T.num[T.rt] - 1);
		a[x] = y;
	}
}