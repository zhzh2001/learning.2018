#include <cstdio>
#include <cstdlib>
#include <windows.h>
#include <algorithm>
long long rnd(){
	return (1ll * rand() << 45) + (1ll * rand() << 30) + (1ll * rand() << 15) + 1ll * rand();
}
long long rnd(long long a, long long b){
	return rnd() % (b - a + 1) + a;
}
int main(){
	srand(GetTickCount());
	freopen("number.in", "w", stdout);
	int n = rnd(8000, 10000), m = 10000, t = 10000;
	printf("%d %d\n", n, m);
	for (register int i = 1; i <= n; ++i) printf("%lld ", rnd(0, t));
	putchar('\n');
	while (m--) printf("%lld %lld\n", rnd(1, n), rnd(0, t));
}