#include <cstdio>
#include <cctype>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = !f;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
int n, m, t, a[10005], b[10005];
int main(){
	freopen("number.in", "r", stdin);
	freopen("number.ans", "w", stdout);
	n = read(), m = read();
	for (register int i = 1; i <= n; ++i) ++b[a[i] = read()];
	while (m--){
		int x = read(), y = read(), mx = 0, id = 0;
		--b[a[x]], ++b[y], a[x] = y;
		for (register int i = 0; i <= 10000; ++i)
			if (b[i] > mx) mx = b[i], id = i;
		printf("%d\n", id);
	}
}