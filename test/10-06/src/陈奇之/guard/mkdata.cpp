#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
//int范围内！要longlong！傻逼出题人 
const int maxn = 200500;
const int maxm = 5;
int n,m;
ll f[1 << maxm],a[maxn][maxm];
inline int ran(){
	return rand() << 15 | rand();
}
int main(){
	freopen("guard.in","w",stdout);
	puts("200000 5");
	Rep(i,1,200000){
		Rep(j,1,5){
			printf("%d %d %d %d %d\n",ran(),ran(),ran(),ran(),ran());
		}
	}
	return 0;
}
/*
4 2
2 1
1 4
4 5
5 3
*/
