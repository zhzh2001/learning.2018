#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
//int范围内！要longlong！傻逼出题人 
/*const int maxn = 1000500;
int h1[maxn*3];
pair<int,int> h2[maxn*3];
pair<pair<int,int>,int> h3[maxn];
int num1,num2,num3;

char s[maxn];int n;
int Sa[maxn],Sb[maxn],Sc[maxn];
vector<int> V[maxn*7];
int ans;
int m,x[10],v[10];
void add(int w,int t){
	++m;
	x[++m] = make_pair();
	x[m] = w,v[m] = t;
	for(unsigned i=0;i<V[w].size();++i){
		printf("[%d]",V[w][i]);
	}puts("");
}


int check(int pos){
	int res = 0;
	Rep(i,1,m)
		res += x[i].se * (upper_bound(V[x[i].fi].begin(),V[x[i].fi].end(),pos) - V[x[i].fi].begin());
	return res;
}
int query(int pos,int A,int B,int C){
	printf("query(%d %d %d %d)\n",pos,A,B,C);
	m = 0;
	add(lower_bound(h1+1,h1+1+num1,A) - h1,1);
	add(lower_bound(h1+1,h1+1+num1,B) - h1,1);
	add(lower_bound(h1+1,h1+1+num1,C) - h1,1);
	if(A==B)add(lower_bound(h1+1,h1+1+num1,A) - h1,-1); else add(lower_bound(h2+1,h2+1+num2,make_pair(A,B)) - h2 + num1,-1);
	if(A==C)add(lower_bound(h1+1,h1+1+num1,A) - h1,-1); else add(lower_bound(h2+1,h2+1+num2,make_pair(A,C)) - h2 + num1,-1);
	if(B==C)add(lower_bound(h1+1,h1+1+num1,B) - h1,-1); else add(lower_bound(h2+1,h2+1+num2,make_pair(B,C)) - h2 + num1,-1);
	if(A==B&&B==C)add(lower_bound(h3+1,h3+1+num3,make_pair(make_pair(A,B),C)) - h3 + num1 + num2,1);
	int l = 0,r = pos - 1,ans = -1;
	while(l <= r){
		int mid = (l + r) >> 1;
		//1 to mid not can
		int tmp = check(mid);
//		printf("For(%d %d),rem = %d\n",0,mid,tmp);
		if(mid+1-tmp){//Have 
//			printf("[%d %d] (%d)\n",0,mid,pos);
			ans = pos - mid;
			r = mid - 1;
		} else{
			l = mid + 1;
		}
	}
	return ans;
}
void insert(int pos,int w){
	printf("Insert(%d %d)\n",w,pos);
	if(V[w].size() == 0 || V[w][V[w].size()-1] != pos){
		V[w] . push_back(pos);
		printf("insert(%d %d)\n",w,pos);
	}
}
void insert(int pos,int A,int B,int C){
	printf("real_insert(%d %d %d %d)\n",pos,A,B,C);
	insert(pos,lower_bound(h1+1,h1+1+num1,A) - h1);
	insert(pos,lower_bound(h1+1,h1+1+num1,B) - h1);
	insert(pos,lower_bound(h1+1,h1+1+num1,C) - h1);
	insert(pos,lower_bound(h2+1,h2+1+num2,make_pair(A,B)) - h2 + num1);
	insert(pos,lower_bound(h2+1,h2+1+num2,make_pair(A,C)) - h2 + num1);
	insert(pos,lower_bound(h2+1,h2+1+num2,make_pair(B,C)) - h2 + num1);
	insert(pos,lower_bound(h3+1,h3+1+num3,make_pair(make_pair(A,B),C)) - h3 + num1 + num2);
}*/
const int maxn = 1000500;
char s[maxn];int n;
int Sa[maxn],Sb[maxn],Sc[maxn];
int pos[100000],m;
int ans,cnt;
bool vis[maxn];
int solve();
#define A(i) (Sa[i] - Sb[i])
#define B(i) (Sa[i] - Sc[i])
#define C(i) (Sb[i] - Sc[i])
bool ck(int limit){
	bool flag = false;
	for(int i=1;i+limit-1<=n;++i){
		int j = i + limit - 1;
		//printf("(%d %d)\n",i,j);
		i--;
		if(A(i) != A(j) && B(i) != B(j) && C(i) != C(j)){
			if(m<=10000 && !vis[i]) pos[++m] = i,vis[i]=true;
			if(m<=10000 && i-1>=0 && !vis[i-1]) pos[++m] = i-1,vis[i-1]=true;
			if(m<=10000 && i-2>=0 && !vis[i-2]) pos[++m] = i-2,vis[i-2]=true;
			if(m<=10000 && !vis[j]) pos[++m] = j,vis[j]=true;
			if(m<=10000 && i+1<=n && !vis[i+1]) pos[++m] = i+1,vis[i+1]=true;
			if(m<=10000 && i+2<=n && !vis[i+2]) pos[++m] = i+2,vis[i+2]=true;
			flag = true;
		}
		i++;
	}
	return flag;
}
int main(){
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	n = rd();
	mem(vis,false);
	scanf("%s",s+1);
	ans = solve();
	Sa[0]=Sb[0]=Sc[0]=0;
	Rep(i,1,n){
		Sa[i] = Sa[i-1] + (s[i] == 'B');
		Sb[i] = Sb[i-1] + (s[i] == 'C');
		Sc[i] = Sc[i-1] + (s[i] == 'S');
	}
	Dep(i,20,ans+1) if(ck(i)) ans = max(ans,i);
	Dep(i,50,21) if(ck(i)) ans = max(ans,i);
	if(ck(100)) ans = max(ans,100);
	if(ck(1000)) ans = max(ans,1000);
	if(ck(10000)) ans = max(ans,10000);
	if(ck(100000)) ans = max(ans,100000);
	if(ck(n/2)) ans = max(ans,n/2);
//	Rep(i,ans+1,50)
//		if(ck(i)) ans = max(ans,i);
//	num1 = num2 = num3 = 0;
	cnt=0;
	Rep(i,1,n){
		if(s[i] == 'B' && ++cnt <= 1000){
			if(i-2>=0&&!vis[i-2])pos[++m] = i-2,vis[i-2]=true;
			if(i-1>=0&&!vis[i-1])pos[++m] = i-1,vis[i-1]=true;
			pos[++m] = i;
			if(i+1<=n&&!vis[i+1])pos[++m] = i+1,vis[i+1]=true;
			if(i+2<=n&&!vis[i+2])pos[++m] = i+2,vis[i+2]=true;
		}
	}
	cnt=0;
	Dep(i,n,1){
		if(s[i] == 'B' && ++cnt <= 1000){
			if(i-2>=0&&!vis[i-2])pos[++m] = i-2,vis[i-2]=true;
			if(i-1>=0&&!vis[i-1])pos[++m] = i-1,vis[i-1]=true;
			pos[++m] = i;
			if(i+1<=n&&!vis[i+1])pos[++m] = i+1,vis[i+1]=true;
			if(i+2<=n&&!vis[i+2])pos[++m] = i+2,vis[i+2]=true;
		}
	}
	cnt=0;
	Rep(i,1,n){
		if(s[i] == 'C' && ++cnt <= 1000){
			if(i-2>=0&&!vis[i-2])pos[++m] = i-2,vis[i-2]=true;
			if(i-1>=0&&!vis[i-1])pos[++m] = i-1,vis[i-1]=true;
			pos[++m] = i;
			if(i+1<=n&&!vis[i+1])pos[++m] = i+1,vis[i+1]=true;
			if(i+2<=n&&!vis[i+2])pos[++m] = i+2,vis[i+2]=true;
		}
	}
	cnt=0;
	Dep(i,n,1){
		if(s[i] == 'C' && ++cnt <= 1000){
			if(i-2>=0&&!vis[i-2])pos[++m] = i-2,vis[i-2]=true;
			if(i-1>=0&&!vis[i-1])pos[++m] = i-1,vis[i-1]=true;
			pos[++m] = i;
			if(i+1<=n&&!vis[i+1])pos[++m] = i+1,vis[i+1]=true;
			if(i+2<=n&&!vis[i+2])pos[++m] = i+2,vis[i+2]=true;
		}
	}
	
	cnt=0;
	Rep(i,1,n){
		if(s[i] == 'S' && ++cnt <= 1000){
			if(i-2>=0&&!vis[i-2])pos[++m] = i-2,vis[i-2]=true;
			if(i-1>=0&&!vis[i-1])pos[++m] = i-1,vis[i-1]=true;
			pos[++m] = i;
			if(i+1<=n&&!vis[i+1])pos[++m] = i+1,vis[i+1]=true;
			if(i+2<=n&&!vis[i+2])pos[++m] = i+2,vis[i+2]=true;
		}
	}
	cnt=0;
	Dep(i,n,1){
		if(s[i] == 'S' && ++cnt <= 1000){
			if(i-2>=0&&!vis[i-2])pos[++m] = i-2,vis[i-2]=true;
			if(i-1>=0&&!vis[i-1])pos[++m] = i-1,vis[i-1]=true;
			pos[++m] = i;
			if(i+1<=n&&!vis[i+1])pos[++m] = i+1,vis[i+1]=true;
			if(i+2<=n&&!vis[i+2])pos[++m] = i+2,vis[i+2]=true;
		}
	}
	sort(pos+1,pos+1+m);
	m = unique(pos+1,pos+1+m) - pos - 1;
//	Rep(i,0,n){
//		printf("%d (%d %d %d)\n",i,A(i),B(i),C(i));
//	}
	Rep(x,1,m){
		Rep(y,1,x-1){
			int i = pos[x],j = pos[y];
			if(A(i) != A(j) && B(i) != B(j) && C(i) != C(j)){
				ans = max(ans,i-j);
			}
		}
	}
	writeln(ans);
	return 0;
	
/*	Rep(i,1,n){
		h1[++num1] = A(i),h1[++num1] = B(i),h1[++num1] = C(i);
		h2[++num2] = make_pair(A(i),B(i)),h2[++num2] = make_pair(A(i),C(i)),h2[++num2] = make_pair(B(i),C(i));
		h3[++num3] = make_pair(make_pair(A(i),B(i)),C(i));
	}
	sort(h1+1,h1+1+num1);num1 = unique(h1+1,h1+1+num1) - h1 - 1;
	sort(h2+1,h2+1+num2);num2 = unique(h2+1,h2+1+num2) - h2 - 1;
	sort(h3+1,h3+1+num3);num3 = unique(h3+1,h3+1+num3) - h3 - 1;
	insert(0,A(0),B(0),C(0));
	Rep(i,1,n){
		ans = max(ans,query(i,A(i),B(i),C(i)));
		insert(i,A(i),B(i),C(i));
	}
	writeln(ans);*/
/*	(Sa[i] - Sa[j])(Sb[i] - Sb[j])(Sc[i] - Sc[j])
	一个都不能相等
	A(i) = Sa[i] - Sb[i]
	B(i) = Sa[i] - Sc[i]
	C(i) = Sb[i] - Sa[i] = B(i) - A(i)
	
	A(i),B(i)
	
	not same
	
	(Sa[i] - Sb[i]) - (Sa[j] - Sb[j]) != 0  and 
	(Sb[i] - Sc[i]) - (Sb[j] - Sc[j]) != 0  and 
	(Sa[i] - Sc[i]) - (Sa[j] - Sc[j]) != 0 
	
	
	
	A + B + C - AB - AC - BC + ABC
			A(i)=0,B(i)=0*/
	return 0;
}
int solve(){
	int ans = 0;
	for(int i=1,j;i<=n;i=j){
		//i to j-1
		for(j=i;j<=n && s[i]==s[j];j++);
		ans = max(ans,j-i);
	}
	return ans;
}//only 1
/*

*/
