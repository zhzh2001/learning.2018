#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
#define clock CLOCK
const int maxn =  100000+233;
struct node{
	int cnt,v,clk;
	node(){}
	node(int cnt,int v,int clk):
		cnt(cnt),v(v),clk(clk){}
	bool operator < (const node &w) const{
		if(cnt == w.cnt) return v > w.v; else return cnt < w.cnt;
	}
};
int h[maxn*2],cnt[maxn*2];
int n,m;
int x[maxn],y[maxn],a[maxn];
int clock[maxn];
priority_queue<node> Q;
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n = rd();m = rd();
	*h = 0;
	Rep(i,1,n){
		a[i] = rd();
		h[++*h] = a[i];
	}
	Rep(i,1,m){
		x[i] = rd(),y[i] = rd();
		h[++*h] = y[i];
	}
	sort(h+1,h+1+*h); *h = unique(h+1,h+1+*h) - h - 1;
	Rep(i,1,n) a[i] = lower_bound(h+1,h+1+*h,a[i]) - h;
	Rep(i,1,m) y[i] = lower_bound(h+1,h+1+*h,y[i]) - h;
	Rep(i,1,n) cnt[a[i]]++;
	Rep(i,1,*h)if(cnt[i]){
		Q . push(node(cnt[i],i,0));
		clock[i] = 0;
	} else clock[i] = -1;
	Rep(i,1,m){
		int pos = x[i],val = y[i];
		cnt[a[pos]]--;
		clock[a[pos]] = i;
		Q.push(node(cnt[a[pos]],a[pos],i));
		a[pos] = val;
		cnt[a[pos]]++;
		clock[a[pos]] = i;
		Q.push(node(cnt[a[pos]],a[pos],i));
		while(Q.top().clk != clock[Q.top().v]) Q.pop();
		writeln(h[Q.top().v]);
	}
	return 0;
}
