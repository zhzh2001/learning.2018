#include<fstream>
#include<stdio.h>
//#include<iostream>
#include<algorithm>
#define max(a,b) (a>b)?a:b
using namespace std;
ifstream fin("guard.in");
ofstream fout("guard.out");
inline long long read(){
	long long x=0,f=1; char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
long long n,d,ans;
struct md{
	long long d[6],sum;
}num[200003];
inline bool cmp(md a,md b){
	return a.sum<b.sum;
}
int main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read(),d=read();
	for(int i=1;i<=n;i++){
		num[i].d[1]=read();
		if(d>1)num[i].d[2]=read();
		if(d>2)num[i].d[3]=read();
		if(d>3)num[i].d[4]=read();
		if(d>4)num[i].d[5]=read();
	}
	for(long long i=-1;i<=1;i+=2)
		for(long long j=-1;j<=1;j+=2)
			for(long long k=-1;k<=1;k+=2)
				for(long long l=-1;l<=1;l+=2){
					for(register int x=1;x<=n;x++){
						num[x].sum=num[x].d[1]*i+num[x].d[2]*j+num[x].d[3]*k+num[x].d[4]*l+num[x].d[5];
					}
					int mn=1;
					for(register int x=2;x<=n;x++){
						if(num[i].sum<num[mn].sum){
							mn=i;
						}
					}
					for(register int x=2;x<=n-1;x++){
						ans=max(abs(num[n].d[1]-num[x].d[1])+abs(num[n].d[2]-num[x].d[2])+abs(num[n].d[3]-num[x].d[3])+abs(num[n].d[4]-num[x].d[4])+abs(num[mn].d[5]-num[x].d[5]),ans);
						ans=max(abs(num[mn].d[1]-num[x].d[1])+abs(num[mn].d[2]-num[x].d[2])+abs(num[mn].d[3]-num[x].d[3])+abs(num[mn].d[4]-num[x].d[4])+abs(num[mn].d[5]-num[x].d[5]),ans);
					}
					ans=max(abs(num[mn].d[1]-num[n].d[1])+abs(num[mn].d[2]-num[n].d[2])+abs(num[mn].d[3]-num[n].d[3])+abs(num[mn].d[4]-num[n].d[4])+abs(num[mn].d[5]-num[n].d[5]),ans);
				}
	fout<<ans<<endl;
	return 0;
}
