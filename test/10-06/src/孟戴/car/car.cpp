//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<time.h>
#include<fstream>
#define max(a,b) (a>b)?a:b
#define min(a,b) (a<b)?a:b
using namespace std;
ifstream fin("car.in");
ofstream fout("car.out");
long long n;
char ch[1000003];
long long num[1000003][4];//1:B,2:C,3:S
long long sum[1003][5];
long long las[1000003];
long long now=1,ans=0;
int main(){
	srand(19260817);
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>ch[i];
		num[i][1]=num[i-1][1],num[i][2]=num[i-1][2],num[i][3]=num[i-1][3];
		if(ch[i]=='B'){
			num[i][1]++;
		}else if(ch[i]=='C'){
			num[i][2]++;
		}else{
			num[i][3]++;
		}
		if(ch[i]==ch[i-1]){
			las[i]=las[i-1];
		}else{
			las[i]=i;
		}
		ans=max(ans,i-las[i]+1);
	}
	if(n<=10000){
		for(int i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				int a,b,c;
				a=num[j][1]-num[i-1][1],b=num[j][2]-num[i-1][2],c=num[j][3]-num[i-1][3];
				if(a!=b&&b!=c&&c!=a){
					ans=max(ans,j-i+1);
				}
			}
		}
		fout<<ans<<endl;
		return 0;
	}
	int a,b,c,cnt,mn; 
	for(register int i=1;i<=n;i++){
		a=num[i][2]-num[i][1],b=num[i][3]-num[i][2],c=num[i][1]-num[i][3];
		cnt=0,mn=3;
		for(register int j=1;j<=now;j++){
			cnt=(a!=sum[j][0])+(b!=sum[j][1])+(c!=sum[j][2]);
			if(cnt==3){
				ans=max(ans,i-sum[j][3]);
			}
			mn=min(mn,cnt);
		}
		if(mn>=2&&now<=100){
			now++;
			sum[now][0]=a,sum[now][1]=b,sum[now][2]=c,sum[now][3]=i;
		}else if(mn==1&&now>=75&&now<=100){
			if(rand()%4==1){
				now++;
				sum[now][0]=a,sum[now][1]=b,sum[now][2]=c,sum[now][3]=i;
			}
		}
	}
	fout<<ans<<endl;
	return 0;
}
