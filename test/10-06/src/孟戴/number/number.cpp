//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#include<map>
using namespace std;
ifstream fin("number.in");
ofstream fout("number.out");
inline long long read(){
	long long x=0,f=1;  char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
long long n,m,cnt,num[300003],wh[300003];
map<long long,long long>mp,mp2;
struct tree{
	long long mx,ans;
}t[2000003];
struct query{
	long long pos,val;
}q[300003];
inline void pushup(int rt){
	if(t[rt*2].mx<t[rt*2+1].mx){
		t[rt].mx=t[rt*2+1].mx,t[rt].ans=t[rt*2+1].ans;
		return;
	}
	t[rt].mx=t[rt*2].mx,t[rt].ans=t[rt*2].ans;
}
void build(int rt,int l,int r){
	if(l==r){
		t[rt].mx=0,t[rt].ans=l;
		return;
	}
	int mid=(l+r)/2;
	build(rt*2,l,mid),build(rt*2+1,mid+1,r);
	pushup(rt);
}
void update(int rt,int l,int r,int x,int val){
	if(l==r){
		t[rt].mx+=val;
		return;
	}
	int mid=(l+r)/2;
	if(x<=mid){
		update(rt*2,l,mid,x,val);
	}else{
		update(rt*2+1,mid+1,r,x,val);
	}
	pushup(rt);
}
int query(int rt,int l,int r,int x){
	if(l==r){
		return t[rt].mx;
	}
	int mid=(l+r)/2;
	if(x<=mid){
		return query(rt*2,l,mid,x);
	}else{
		return query(rt*2+1,mid+1,r,x);
	}
}
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=n;i++){
		num[i]=read();
		wh[++wh[0]]=num[i];
	}
	for(int i=1;i<=m;i++){
		q[i].pos=read(),q[i].val=read();
		wh[++wh[0]]=q[i].val;
	}
	sort(wh+1,wh+wh[0]+1);
	mp[wh[1]]=++cnt;
	for(int i=2;i<=wh[0];i++){
		if(wh[i]!=wh[i-1]){
			mp[wh[i]]=++cnt;
			mp2[mp[wh[i]]]=wh[i];
		}
	}
	build(1,1,cnt);
	for(int i=1;i<=n;i++){
		update(1,1,cnt,mp[num[i]],1);
	}
	for(int i=1;i<=m;i++){
		int x,y;
		x=q[i].pos,y=q[i].val;
		update(1,1,cnt,mp[num[x]],-1);
		update(1,1,cnt,mp[y],1);
		fout<<mp2[t[1].ans]<<endl;
		num[x]=y;
	}
	return 0;
}
/*

in:
3 2
1 1 1
2 2
3 2

out:
1
2

*/
