program guard;
 uses math;
 var
  x,y,z:array[0..1001] of longint;
  i,j,d,n,smax,smin:longint;
 begin
  assign(input,'guard.in');
  assign(output,'guard.out');
  reset(input);
  rewrite(output);
  readln(n,d);
  if d=1 then
   begin
    smax:=0;
    //smin:=1008208820;
    for i:=1 to n do
     begin
      readln(z[i]);
      smax:=max(smax,z[i]);
      smin:=min(smin,z[i]);
     end;
    writeln(smax-smin);
   end
         else
   begin
    smax:=0;
    smin:=1008208820;
    for i:=1 to n do
     readln(x[i],y[i]);
    smax:=0;
    for i:=1 to n-1 do
     for j:=i+1 to n do
      smax:=max(smax,abs(x[i]-x[j])+abs(y[i]-y[j]));
    writeln(smax);
   end;
  close(input);
  close(output);
 end.
