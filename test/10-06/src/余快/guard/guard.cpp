/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 160000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 200055
#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,d,vis[N],b,cnt;
struct xx{
	int a[6];
}z[N];
int rnd(){
	return (rand()<<5)+rand();
}
inline int jl(int a,int b){
	return (a>b)?a-b:b-a;
}
signed main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read();d=read();d--;srand(19260817);
	F(i,1,n) F(j,0,d) z[i].a[j]=read();
	F(i,1,100){
		b=rnd()%n+1;
		while (!vis[b]){
			vis[b]=1;cnt++;
			int num=0,nmax=0,p=0;
			F(i,1,n){
				num=0;
				F(j,0,d){
					num+=jl(z[i].a[j],z[b].a[j]);
				}
				if (num>nmax){
					nmax=num;p=i;
				}
			}
			ans=max(ans,nmax);b=p;
			if (cnt*n*d>inf) break;
		}
		if (cnt*n*d>inf) break;
	}
	wrn(ans);
	return 0;
}
