/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,x,y,a[N],cnt;
struct xx{
	int l,r,le,ri;
	pa ans;
}t[2000000];
void newp(int &x,int l,int r){
	x=++cnt;t[x].ans=mp(inf,inf);t[x].l=l;t[x].r=r;
}
void change(int x,int k,int p){
	if (t[x].l==t[x].r){
		if (t[x].ans.fi==inf){
			t[x].ans.fi=0;t[x].ans.se=t[x].l;
		}
		t[x].ans.fi-=p;
		return;
	}
	int mid=(t[x].l+t[x].r)>>1;
	if (k<=mid){
		if (!t[x].le) newp(t[x].le,t[x].l,mid);
		change(t[x].le,k,p);
	}
	else{
		if (!t[x].ri) newp(t[x].ri,mid+1,t[x].r);
		change(t[x].ri,k,p);
	}
	t[x].ans=mp(inf,inf);
	if (t[x].le) t[x].ans=min(t[x].ans,t[t[x].le].ans);
	if (t[x].ri) t[x].ans=min(t[x].ans,t[t[x].ri].ans);
}
signed main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();m=read();t[1].l=0;t[1].r=10000000;t[1].ans=mp(inf,inf);cnt=1;
	F(i,1,n) a[i]=read(),change(1,a[i],1);
	F(i,1,m){
		x=read();y=read();
		change(1,a[x],-1);a[x]=y;change(1,a[x],1);
		wrn(t[1].ans.se);
	}
	return 0;
}
