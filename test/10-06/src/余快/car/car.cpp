/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 1000055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,a[N],b[N],c[N],vis1[N*2],vis2[N*2],vis3[N*2],vis4[N],A,B;
struct xx{
	int a,b,i,d;
	pa c;
}z[N];
char str[N];
bool cmp(xx a,xx b){
	return a.c<b.c;
}
int check(int x){
	int pd=0,num=0;
	F(i,x,n){
		vis1[a[i-x]]++;vis2[b[i-x]]++;vis3[a[i-x]+b[i-x]-n]++;vis4[c[i-x]]++;
		num=vis1[a[i]]+vis2[b[i]]+vis3[a[i]+b[i]-n];
		num-=vis4[c[i]]*2;
		if (num<i-x+1){pd=i;break;}
	}
	int p=n;
	if (pd>0) p=pd;
	F(i,x,p){
		vis1[a[i-x]]--;vis2[b[i-x]]--;vis3[a[i-x]+b[i-x]-n]--;vis4[c[i-x]]--;
	}
	return (pd>0);
}
signed main(){
	freopen("car.in","r",stdin);
	freopen("car.out","w",stdout);
	n=read();scanf("%s",str+1);
	F(i,1,n){
		if (str[i]=='C') c[i]=1;
		if (str[i]=='B') c[i]=2;
	}
	A=0;B=0;
	F(i,1,n){
		if (c[i]==0) A++;
		if (c[i]==1) A--,B++;
		if (c[i]==2) B--;
		z[i].a=A+n;z[i].b=B+n;z[i].i=i;z[i].c=mp(A,B);
	}
	z[0].a=z[0].b=n;z[0].c=mp(n,n);
	sort(z,z+n+1,cmp);
	F(i,0,n){
		if (z[i].c!=z[i-1].c||i==1) z[i].d=z[i-1].d+1;
		else z[i].d=z[i-1].d;
	}
	F(i,0,n){
		int t=z[i].i;
		a[t]=z[i].a;b[t]=z[i].b;c[t]=z[i].d;
	}
	int l=1,r=n;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (check(mid)) l=mid;
		else r=mid-1;
	}
	wrn(l);
	return 0;
}
