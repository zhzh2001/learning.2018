#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
struct D{
	ll c[10];
}a[201000];
ll ans,d,n;
ll b[10];
bool cmp(D a,D b)
{
	return a.c[0]>b.c[0];
}
void dfs(int x)
{
	if(x==d)
	{
		For(i,1,n)
			For(j,1,d)
				a[i].c[0]+=a[i].c[j]*b[j];
		sort(a+1,a+n+1,cmp);
		ll s=0;
		For(j,1,d)
		{
			s+=abs(a[1].c[j]-a[n].c[j]);
		}
		ans=max(ans,s);
		For(i,2,n-1)
		{
			s=0;
			For(j,1,d)
				s+=abs(a[1].c[j]-a[i].c[j]);
			ans=max(ans,s);
			s=0;
			For(j,1,d)
				s+=abs(a[n].c[j]-a[i].c[j]);
			ans=max(ans,s);
		}
		return;
	}
	b[x+1]=1;
	dfs(x+1);
	b[x+1]=-1;
	dfs(x+1);
}
int main()
{
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read();d=read();
	For(i,1,n)
		For(j,1,d)
			a[i].c[j]=read();
	dfs(0);
	cout<<ans<<endl;
	return 0;
}

