#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
ll n,d;
ll a[201000][6];
ll x,y,ans,mx;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
int main()
{
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	srand(time(0));
	n=read();d=read();
	For(i,1,n)
		For(j,1,d)
		{
			a[i][j]=read();
		}
	For(i,1,9e6)
	{
		x=rand()%n+1;y=rand()%n+1;
		ans=0;
		For(i,1,d)ans+=abs(a[x][i]-a[y][i]);
		mx=max(mx,ans);
	}
	cout<<mx<<endl;
	return 0;
}

