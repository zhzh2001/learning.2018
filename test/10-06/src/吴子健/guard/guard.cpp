#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
int N,D;
struct pot{
	ll a[6];
	ll& operator [] (int x) {
		return a[x];
	} 
}P[200000*2];
ll f[200010];
ll maxv,minv,ans;
//main========================
int main() {
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	N=rd(),D=rd();
	for(int i=1;i<=N;++i) {
		for(int j=0;j<D;++j) 
			P[i].a[j]=rd();
	}
	for(int j=0;j<(1<<D);++j) {
		maxv=-1e14,minv=1e14;
		//printf("j=%d\n",j);
		for(int i=1;i<=N;++i) {
			f[i]=0;
			for(int k=0;k<D;++k) {
				if((1<<k)&j) {
					f[i]+=P[i][k];
				} else {
					f[i]-=P[i][k];
				}
			}
			//printf("f[%d]=%lld\n",i,f[i]);
			maxv=max(maxv,f[i]);
			minv=min(minv,f[i]);
		}
		//printf("maxv=%lld minv=%lld\n",maxv,minv);
		ans=max(ans,maxv-minv);
	}
	printf("%lld\n",ans);
	return 0;
}
