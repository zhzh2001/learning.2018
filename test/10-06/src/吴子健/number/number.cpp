#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
const int MAXN=1e6+10;
const int MAXM=1e9+100;
struct sts{
	int x,tim;
	int lc,rc;
}T[200000*16];
int cnt;
int Rt;
void merge(int x) {
	if(T[T[x].lc].tim>=T[T[x].rc].tim) T[x].x=T[T[x].lc].x,T[x].tim=T[T[x].lc].tim;
	else T[x].x=T[T[x].rc].x,T[x].tim=T[T[x].rc].tim;
}
void modify(int &x,int pos,int vv,int L=0,int R=MAXM) {
	if(!x) x=++cnt;
	if(L==R) {
		T[x].x=pos;
		T[x].tim=vv;
		return ;
	}
	int mid=(L+R)>>1;
	if(pos<=mid) modify(T[x].lc,pos,vv,L,mid);
	else modify(T[x].rc,pos,vv,mid+1,R);
	merge(x);
}
int qry(int &x,int pos,int L=0,int R=MAXM) {
	if(!x) return 0;
	if(L==R) return T[x].tim;
	int mid=(L+R)>>1;
	if(pos<=mid) return qry(T[x].lc,pos,L,mid);
	else return qry(T[x].rc,pos,mid+1,R);
}
int N,M;
int a[MAXN];
//main========================
int main() {
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	N=rd(),M=rd();
	for(int i=1;i<=N;++i) {
		int x=rd();
		a[i]=x;
		int val=qry(Rt,x)+1;
		modify(Rt,x,val);
	}
	for(int i=1;i<=M;++i) {
		int x=rd(),y=rd();
		int val=qry(Rt,a[x])-1;
		modify(Rt,a[x],val);
		val=qry(Rt,y)+1;
		modify(Rt,y,val);
		printf("%d\n",T[Rt].x);
		a[x]=y;
	}
	return 0;
}
