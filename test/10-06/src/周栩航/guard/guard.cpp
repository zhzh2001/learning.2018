#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long

using namespace std;

inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void write_p(ll x){write(x);putchar(' ');}
inline void writeln(ll x){write(x);puts("");}
const int N=200006;
struct node{ll v[7],tot;}	a[N];
int n,m;
ll ans;
inline bool cmp(node x,node y){return x.tot<y.tot;}
int main()
{
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read();m=read();
	For(i,1,n)
	{
		For(j,1,m)
			a[i].v[j]=read(),a[i].tot+=abs(a[i].v[j]);
	}
	sort(a+1,a+n+1,cmp);
	For(i,max(1,n-4000),n)	
		For(j,i+1,n)	
		{
			ll tmp=0;
			For(k,1,m)	tmp+=abs(a[i].v[k]-a[j].v[k]);
			ans=max(ans,tmp);
		}
	For(i,1,min(n,3000))	
		For(j,max(1,n-3000),n)	
		{
			ll tmp=0;
			For(k,1,m)	tmp+=abs(a[i].v[k]-a[j].v[k]);
			ans=max(ans,tmp);
		}
	writeln(ans);
}
