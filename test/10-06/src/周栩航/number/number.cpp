#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long

using namespace std;

inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void write_p(ll x){write(x);putchar(' ');}
inline void writeln(ll x){write(x);puts("");}

int n,m,cnt[10000005],a[200005];
struct pa{int c,x;};
priority_queue<pa> Q;
inline bool operator < (pa x,pa y){return x.c!=y.c?x.c<y.c:x.x>y.x;}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();m=read();
	For(i,1,n)	a[i]=read(),cnt[a[i]]++;
	For(i,1,10000000)	if(cnt[i])	Q.push((pa){cnt[i],i});
	For(i,1,m)
	{
		int x=read(),y=read();
		cnt[y]++;cnt[a[x]]--;
		Q.push((pa){cnt[y],y});
		Q.push((pa){cnt[a[x]],a[x]});
		a[x]=y;
		while(1)
		{
			pa tmp=Q.top();
			if(tmp.c==cnt[tmp.x]){writeln(tmp.x);break;}else	Q.pop();
		}
	}
}
