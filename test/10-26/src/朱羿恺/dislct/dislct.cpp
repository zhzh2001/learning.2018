#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 3e5+10;
int n,q,x[N],y[N],cnt,ans[N];
struct node{int opt,x;}Q[N];
bool flag[N];
int tot,first[N],to[N<<1],last[N<<1];
inline void Add(int x,int y){to[++tot]=y,last[tot]=first[x],first[x]=tot;}
int Size;
inline void GetSize(int u,int fa){
	Size++;
	cross(i,u) if (to[i]!=fa) GetSize(to[i],u);
}
int rt,size[N],Max[N];
bool vis[N];
inline void GetRoot(int u,int fa){
    Max[u]=0,size[u]=1,vis[u]=1;
    cross(i,u){
        int v=to[i];
        if (v==fa) continue;
        GetRoot(v,u),size[u]+=size[v];
        if (size[v]>Max[u]) Max[u]=size[v];
    }
    Max[u]=max(Max[u],Size-size[u]);
    if (Max[u]<Max[rt]||Max[u]==Max[rt]&&u<rt) rt=u; 
}
int dep[N];
inline void GetDep(int u,int fa){
	cross(i,u) if (to[i]!=fa) dep[to[i]]=dep[u]+1,GetDep(to[i],u);
}
int main(){
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n=read(),q=read();
	For(i,1,n-1) x[i]=read(),y[i]=read();
	For(i,1,q){
		Q[i]=(node){read(),read()};
		if (Q[i].opt==1) flag[Q[i].x]=1;
	}
	For(i,1,n-1) if (!flag[i]) Add(x[i],y[i]),Add(y[i],x[i]);
	Max[0]=1e9;
	For(i,1,n) if (!vis[i]) Size=rt=0,GetSize(i,0),GetRoot(i,0),dep[rt]=0,GetDep(rt,0);
	//For(i,1,n) printf("%d ",dep[i]);puts("");
	Dow(i,q,1)
		if (Q[i].opt==1){
			Add(x[Q[i].x],y[Q[i].x]),Add(y[Q[i].x],x[Q[i].x]);
			Size=rt=0,GetSize(x[Q[i].x],0),GetRoot(x[Q[i].x],0),dep[rt]=0,GetDep(rt,0);//,printf("%d %d 233\n",Size,rt);
		} else ans[++cnt]=dep[Q[i].x];
	Dow(i,cnt,1) printf("%d\n",ans[i]); 
}
