#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,m,l,q;
ll a[N],b[N],ans;
char opt[20];
namespace Subtask1{
	inline void Del(ll x){
		For(i,1,n) if (a[i]==x){b[i]--;break;}
	}
	inline void Add(ll y,ll x){
		For(i,1,n) if (a[i]==x){b[i]--,a[++n]=a[i]+y,b[n]=1;break;}
	}
	inline ll Query(ll x){
		ll ans=0;
		For(i,1,n) ans+=x%a[i]*b[i];
		return ans;
	}
	inline void Main(){
		while (q--){
			scanf("%s",opt+1);
			if (opt[1]=='a') a[++n]=read(),b[n]=1;
			else if (opt[1]=='b') Del(read());
			else if (opt[1]=='c') Add(-read(),read());
			else if (opt[1]=='e') Add(read(),read());
			else printf("%lld\n",Query(read()));
		}
	}
}
namespace Subtask2{
	ll Sum[N],sum,x;
	inline void Del(ll x){
		if (x>200) sum-=x;
			else Sum[x]--;
	}
	inline void Add(ll y,ll x){
		if (x>200) sum+=y;
		else if (x+y>200) sum+=x+y,Sum[x]--;
		else Sum[x]--,Sum[x+y]++; 
	}
	inline ll Query(ll x){
		ll ans=sum;
		For(i,1,200) ans+=x%i*Sum[i];
	//	For(i,1,200) if (Sum[i]) printf("%d %lld\n",i,Sum[i]);
		return ans;
	}
	inline void Main(){
		For(i,1,n) Sum[a[i]]+=b[i];
		Dow(i,1e5,201) sum+=Sum[i]*i;
		while (q--){
			scanf("%s",opt+1);
			if (opt[1]=='a') x=read(),x>200?sum+=x:Sum[x]++;
			else if (opt[1]=='b') Del(read());
			else if (opt[1]=='c') Add(-read(),read());
			else if (opt[1]=='e') Add(read(),read());
			else printf("%lld\n",Query(read()));
		}
	}
}
namespace Subtask3{
	ll Sum[N],sum,x;
	inline void Del(ll x){Sum[x]--;}
	inline void Add(ll y,ll x){Sum[x]--,Sum[x+y]++; }
	inline ll Query(ll x){
		ll ans=sum;
		For(i,1,m) ans+=x%i*Sum[i];
		return ans;
	}
	inline void Main(){
		For(i,1,n) Sum[a[i]]+=b[i];
		while (q--){
			scanf("%s",opt+1);
			if (opt[1]=='a') x=read(),Sum[x]++;
			else if (opt[1]=='b') Del(read());
			else if (opt[1]=='c') Add(-read(),read());
			else if (opt[1]=='e') Add(read(),read());
			else printf("%lld\n",Query(read()));
		}
	}
}
struct bit{
	ll c[N];
	inline void Add(int x,ll y){for (;x<=m;x+=x&-x) c[x]+=y;}
	inline ll Query(int x){ll ans=0;for (;x;x-=x&-x) ans+=c[x];return ans;}
}t1,t2;//t1人数 t2人数*权值 
inline void Add(int x){t1.Add(x,1),t2.Add(x,x);}
inline void Del(int x){t1.Add(x,-1),t2.Add(x,-x);}
inline void Add(int y,int x){Del(x),Add(x+y);}
inline ll Query(int x){
	ll ans=1ll*x*(t1.Query(m)-t1.Query(x)),cnt,sum,l,r;
	for (int L=1,R;L<=x;L=R+1){
		R=x/(x/L),l=L,r=R;
		if (x%l==0) l++;
		if (x%r==0) r--;
		if (l<=r) cnt=t1.Query(r)-t1.Query(l-1),sum=t2.Query(r)-t2.Query(l-1);
			else sum=cnt=0;//printf("%d %d %lld %lld\n",l,r,sum,cnt);
		ans+=cnt*x-sum*(x/L);
	}
	return ans;
}
int main(){
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n=read(),m=read(),l=read();
	For(i,1,n) a[i]=read(),b[i]=read();
	q=read();
	if (n<=5000) return Subtask1::Main(),0;
	if (l<=200) return Subtask2::Main(),0;
	if (q<=200) return Subtask3::Main(),0;
	For(i,1,n) t1.Add(a[i],b[i]),t2.Add(a[i],a[i]*b[i]);
	while (q--){
		scanf("%s",opt+1);
		if (opt[1]=='a') Add(read());
		else if (opt[1]=='b') Del(read());
		else if (opt[1]=='c') Add(-read(),read());
		else if (opt[1]=='e') Add(read(),read());
		else printf("%lld\n",Query(read()));
	}
}
