#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 310, mod = 23333;
int n,m;
namespace Subtask1{
	int C[N*N][N];
	inline void Mod(int &x){while (x>=mod) x-=mod;}
	inline void init(){
		For(i,1,n*m){
			C[i][1]=i;
			if (i<=n) C[i][i]=1;
			For(j,2,min(n,i-1)) Mod(C[i][j]=C[i-1][j-1]+C[i-1][j]);
		}
	}
	int dp[N][N];
	inline void solve(){
		memset(dp,0,sizeof dp);
		dp[1][1]=1;
		For(i,1,n-1)
			For(j,1,i){
				if (!dp[i][j]) continue;
				For(k,1,min(j*m,n-i)) Mod(dp[i+k][k]+=1ll*dp[i][j]*C[j*m][k]%mod);
			}
		int ans=0;
		For(i,1,n) Mod(ans+=dp[n][i]);
		printf("%d ",(ans+1)%mod);
	}
	inline void Main(){init(),solve();}
}
int main(){
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	n=read(),m=read();
	if (n<=300&&m<=300) return Subtask1::Main(),0;
}
