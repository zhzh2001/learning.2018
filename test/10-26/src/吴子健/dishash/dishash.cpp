#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define re register int
//#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
/**
考虑动态规划
f[i][j] i个儿子 子树和为j的树的数量 
*/
const int mod=23333;
int N,M; 
int f[1210][1210],g[1210];
//main========================
int main() {
	freopen("dishash.in","r",stdin);
 	freopen("dishash.out","w",stdout);
	N=rd(),M=rd();
	f[0][0]=f[0][1]=1;
	g[0]=g[1]=1;
	for(re j=1;j<=N;++j) {
		for(re i=0;i<=M;++i) {
			if(!i&&j==1) continue;
			if(i) {
				for(re k=0;k<j;++k) {
					f[i][j]+=f[i-1][j-k]*g[k];
					f[i][j]%=mod;
				}				
			}

		//	LOG("f[%d][%d]=%d\n",i,j,f[i][j]);
			//g[j]+=f[i][j];
			//g[j]%=mod;
		}
		g[j]=f[M][j]%mod;
	//	LOG("g[%d]=%d\n",j,g[j]);
	}
	printf("%d\n",(g[N]+1)%mod);
	return 0;
}

