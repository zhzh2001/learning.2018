#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
//#define LOG(format, args...) fprintf(stderr, format, args)
#define go(st) for(int i=head[st],ed=E[i].ed;i;i=E[i].nxt,ed=E[i].ed)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
//defs========================
int N,M;
const int MAXN=3e5+10;
struct edge {
	int ed,nxt,id;
}E[MAXN*2];
bool valid[MAXN*2];
int link[MAXN*2],rlink[MAXN*2];
int head[MAXN],Ecnt=1;
void addEdge(int st,int ed,int id) {
	E[++Ecnt].ed=ed,E[Ecnt].nxt=head[st],head[st]=Ecnt,E[Ecnt].id=id;
	link[id]=Ecnt,rlink[Ecnt]=id;
}
ll dis[MAXN],depth[MAXN],sz[MAXN],minv,tarid,tot;
void dis_dfs(int st,int fa,int anc) {
//	LOG("depth[%d]=%lld\n",st,depth[st]);
	sz[st]=1;
	go(st) if(ed!=fa&&valid[E[i].id]){
		depth[ed]=depth[st]+1;
		dis_dfs(ed,st,anc);
		sz[st]+=sz[ed];
	}
	dis[anc]+=depth[st];
}
void find_rt(int st,int fa) {
	if(dis[st]<minv||(dis[st]==minv&&st<tarid)) {
		minv=dis[st];
		tarid=st;
	}
//	LOG("dis[%d]=%lld\n",st,dis[st]);
	go(st) if(ed!=fa&&valid[E[i].id]) {
		dis[ed]=dis[st]-sz[ed]+(tot-sz[ed]);
		find_rt(ed,st);
	}
}
void rebuild(int now) {
	depth[now]=0,dis[now]=0,sz[now]=0;
	dis_dfs(now,0,now);
	minv=dis[now],tarid=now,tot=sz[now];
	find_rt(now,0);
	depth[tarid]=0,dis[tarid]=0,sz[tarid]=0;
//	LOG("new rt=%d\n",tarid);
	dis_dfs(tarid,0,tarid);
}
map<int,pair<int,int> > anoE;
//main========================
int main() {
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	memset(valid,1,sizeof valid);
	N=rd(),M=rd();
	for(int i=1;i<N;++i) {
		int x=rd(),y=rd();
		addEdge(x,y,i),addEdge(y,x,i);
		anoE[i]=mk(x,y);
	}
	rebuild(1);
	while(M--) {
		int opt=rd();
		if(opt==2) {
			int x=rd();
			printf("%lld\n",depth[x]);
		} else {
			int id=rd(),x=anoE[id].first,y=anoE[id].second;
//			LOG("x=%d y=%d\n",x,y);
			valid[id]=false;
			valid[link[id]^1]=false;
			rebuild(x);
			rebuild(y);
		}
	}
	return 0;
}

