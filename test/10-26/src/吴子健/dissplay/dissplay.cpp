#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define lb(x) ((x)&(-x)) 
#define re register int
//#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
char opt[20];
char &OP=opt[0];
const int MAXN=1e5;
int N,M,Lim,L;
/**
考虑除法分块 
*/
ll sum;
ll a[MAXN];//a[i]表示人数为i的师的数量
ll b[MAXN];//a[i]的树状数组维护
ll c[MAXN];//c[i]=a[i]*i
void add(int pos,int x) {
	for(int pre=pos;pos<=Lim;pos+=lb(pos)) b[pos]+=x,c[pos]+=x*pre;
}
ll qry_b(int pos) {
	ll ret=0;
	for(;pos;pos-=lb(pos)) ret+=b[pos];
	return ret;
}
ll qry_c(int pos) {
	ll ret=0;
	for(;pos;pos-=lb(pos)) ret+=c[pos];
	return ret;
}
//main========================
int main() {
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	N=rd(),Lim=rd(),L=rd();
	//int t=0;
	for(re i=1;i<=N;++i) {
		int x=rd(),y=rd();
		a[x]=y;
		//t+=x*y;
		add(x,y);
	}
	//LOG("%d %d\n",qry_c(Lim),t);
	//LOG("%lld\n",(ll)(5e4*sqrt(5e4)*log2(5e4)));
	M=rd();
	for(re i=1;i<=M;++i) {
		scanf("%s",opt);
		if(OP=='a') {
			int x=rd();
			++a[x];
			add(x,1);
		} else if(OP=='b') {
			int x=rd();
			--a[x];
			add(x,-1);
		} else if(OP=='c') {
			int x=rd(),y=rd();
			--a[x],++a[x-y];
			add(x,-1),add(x-y,1);
		} else if(OP=='e') {
			int x=rd(),y=rd();
			--a[x],++a[x+y];
			add(x,-1),add(x+y,1);
		} else if(OP=='q') {
			int x=rd();
			ll ans=0;
			ans=qry_b(Lim)*x;
//			LOG("pre=%lld=%lld*%d\n",ans,qry_b(Lim),x);
//			for(int i=1;i<=Lim;++i) {
//				ans+=(x%i)*a[i];
//			}
			for(re l=1,r;l<=x;l=r+1) {
				r=x/(x/l);
//				LOG("%d,%d\n",l,r);
				ans-=(ll)(x/l)*/*(r-l+1)**/(qry_c(r)-qry_c(l-1));
//				LOG("-=%lld\n",(ll)(x/l)/**(r-l+1)*/*(qry_c(r)-qry_c(l-1)));
			}
			printf("%lld\n",ans);
		}
	}
	return 0;
}

