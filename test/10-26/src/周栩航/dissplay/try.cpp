#include<bits/stdc++.h>
#define ll long long
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)

using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=100005;
int n,lim,a,blo,ans[304],L;
ll tr[N*4],lazy[N*4],lazy_gc[N*4];
inline void Push(int x,int l,int r)
{
	int mid=l+r>>1;
	if(lazy[x])
	{
		if(l==mid)	tr[x<<1]+=lazy[x];else		lazy[x<<1]+=lazy[x],lazy_gc[x<<1]+=lazy_gc[x];
		if(mid+1==r)	tr[x<<1|1]+=lazy[x]+(mid+1-l)*lazy_gc[x];else	lazy_gc[x<<1|1]+=lazy_gc[x],lazy[x<<1|1]+=lazy[x]+(mid+1-l)*lazy_gc[x];
		lazy[x]=0;lazy_gc[x]=0;
	}
}
inline void Add(int x,int l,int r,int ql,int qr,int sx,int gc)
{
	if(ql<=l&&r<=qr)
	{
		if(l==r)	tr[x]+=sx+gc*(l-ql);else		lazy[x]+=sx+gc*(l-ql),lazy_gc[x]+=gc;
		return;
	}
	Push(x,l,r);
	int mid=l+r>>1;
	if(ql>=mid+1)	Add(x<<1|1,mid+1,r,ql,qr,sx,gc);
	else	if(qr<=mid)	Add(x<<1,l,mid,ql,qr,sx,gc);
	else
	{
		Add(x<<1,l,mid,ql,mid,sx,gc);
		Add(x<<1|1,mid+1,r,mid+1,qr,sx+(mid+1-ql)*gc,gc);
	}
}
inline ll Get(int x,int l,int r,int to)
{
	if(l==r)	return tr[x];
	int mid=l+r>>1;
	Push(x,l,r);
	if(to<=mid)	return Get(x<<1,l,mid,to);else	return Get(x<<1|1,mid+1,r,to);
}
inline void Ins(int x,int tnum)
{
	if(x==1)	return;
	for(int i=1;i<=L;i+=x)
	{
		int top=min(L,i+x-2);
		Add(1,1,L,i,top,tnum,tnum);
	}
}
inline void Solve(int x)
{
	writeln(Get(1,1,L,x));
}
char opt[25];
int main()
{
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n=read();lim=read();L=read();
	For(i,1,n)
	{
		ll a=read(),b=read();
		Ins(a,b);
	}
	int m=read();
	For(i,1,m)
	{
		scanf("\n%s",opt+1);
		if(opt[1]=='a')	
		{
			int x=read();
			Ins(x,1);
		}
		if(opt[1]=='b')
		{
			int x=read();
			Ins(x,-1);
		}
		if(opt[1]=='c')
		{
			int x=read(),y=read();
			Ins(x-y,1);Ins(x,-1);
		}
		if(opt[1]=='e')
		{
			int x=read(),y=read();
			Ins(x+y,1);Ins(x,-1);
		}
		if(opt[1]=='q')	Solve(read());
	}
}

/*
4 4 4 4 4 4 1 1 1 1 2 2 2 2 2 2 7 7 7 7 7 7 10*10

*/
