#pragma GCC optimize 2
#include<bits/stdc++.h>
#define ll long long
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)

using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const ll mo=23333; 
ll n,m,ans;
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2,x=x*x%mo)	if(y&1)	sum=sum*x%mo;return sum;}
int main() 
{
	freopen("dishash.in","r",stdin); 
	freopen("dishash.out","w",stdout); 
	n=read();m=read(); 
	ans=1; 
	int d;
	For(i,n*m-n+2,n*m)	ans*=i,ans%=mo;
	For(i,1,n)	ans=ans*ksm(i,mo-2)%mo; 
	if(d>1)	ans=0;
	writeln((ans+1)%mo);
}
