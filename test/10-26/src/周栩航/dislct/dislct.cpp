#include<bits/stdc++.h>
#pragma GCC optimize 2
#define ll long long
#define For(i,j,k)	for(register int i=j;i<=k;++i)
#define Dow(i,j,k)	for(register int i=k;i>=j;--i)

using namespace std;
const int LLL=1000000;
char LZH[LLL];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,LLL,stdin);
	return *SSS++;
}
#define gc getchar
inline ll read(){
	ll x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=70000;
int poi[N*2],nxt[N*2],head[N],cnt,vis[N*2],alr[N],rt[N],top,dp[N],sz[N],dis[N],tsiz,n,m;
inline void add(int x,int y)
{
	poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;
	poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;
}
inline void Pre(int x,int fa)
{
	sz[x]=1;
	alr[x]=1;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa||vis[i])	continue;
		Pre(poi[i],x);
		sz[x]+=sz[poi[i]];
	}
}
inline void Get_rt(int x,int fa)
{
	dp[x]=tsiz-sz[x];
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa||vis[i])	continue;
		Get_rt(poi[i],x);
		dp[x]=max(dp[x],sz[poi[i]]);
	}
	if(dp[x]<dp[rt[top]])	rt[top]=x;
	if(dp[x]==dp[rt[top]])	if(x<rt[top])	rt[top]=x;
}
inline void Dfs(int x,int fa,int d)
{
	dis[x]=d;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa||vis[i])	continue;
		Dfs(poi[i],x,d+1);
	}
}
inline void Re()
{
	For(i,1,top)	rt[i]=0;
	top=0;dp[0]=n+1;
	For(i,1,n)	alr[i]=0;
	For(i,1,n)	if(!alr[i])	
	{
		top++;
		Pre(i,i);
		tsiz=sz[i];
		Get_rt(i,i);
	}
	For(i,1,top)	Dfs(rt[i],rt[i],0);
}
int main()
{
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n=read();m=read();
	For(i,1,n-1)	add(read(),read());
	Re();
	For(i,1,m)
	{
		int tx=read(),ty=read();
		if(tx==1)
		{
			vis[ty*2]=vis[ty*2-1]=1;
			Re();
		}
		else
			writeln(dis[ty]);
	}
}
