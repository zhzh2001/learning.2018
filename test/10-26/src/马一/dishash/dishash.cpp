#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
namespace Dango
{
	const int MAXN=1005,MOD=23333;
	int n,m,f[MAXN],dp[MAXN][MAXN];
	bool vis[MAXN][MAXN];
	int dfs(int a,int b)
	{
		if(!vis[a][b])
		{
			vis[a][b]=true;
			if(b==1)return dp[a][b]=f[a];
			for(int i=0;i<=a;i++)
			{
				dp[a][b]=dp[a][b]+f[i]*dfs(a-i,b-1)%MOD;
				while(dp[a][b]>=MOD)dp[a][b]-=MOD;
			}
		}
		return dp[a][b];
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin>>n>>m;
		f[0]=f[1]=1;
		for(int i=2;i<=n;i++)
			f[i]=dfs(i-1,m);
		cout<<(f[n]+1)%MOD;
		return 0;
	}
}
int main()
{
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	return Dango::work();
}
