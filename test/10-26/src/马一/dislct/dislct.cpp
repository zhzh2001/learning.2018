#include <cstdio>
#include <cctype>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<vector>
using namespace std;
namespace IO
{
	inline char gc()
	{
	    static char buf[1<<12],*p1=buf,*p2=buf;
	    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<12,stdin),p1==p2)?EOF:*p1++;
	}
	
	#define dd c = gc()
	inline void read(int& x)
	{
	    x = 0;
	    int dd;
	    bool f = false;
	    for(; !isdigit(c); dd)
	        if(c == '-')
	            f = true;
	    for(; isdigit(c); dd)
	        x = (x<<1) + (x<<3) + (c^48);
	    if(f)
	        x = -x;
	}
	#undef dd
}
namespace Dango
{
	const int MAXN=300005;
	struct Edge
	{
		int from,to;
	};
	vector<Edge> edges;
	vector<int> g[MAXN];
	int n,m,d[MAXN];
	long long dp1[MAXN],dp2[MAXN],size[MAXN];
	bool flag[MAXN*2];
	void add(int u,int v)
	{
		edges.push_back((Edge){u,v});
		edges.push_back((Edge){v,u});
		int s=edges.size();
		g[u].push_back(s-2);
		g[v].push_back(s-1);
	}
	void dfs(int u,int f)
	{
		size[u]=1;
		for(int i=0;i<g[u].size();i++)
		{
			if(flag[g[u][i]])continue;
			Edge& e=edges[g[u][i]];
			if(e.to==f)continue;
			dfs(e.to,u);
			size[u]+=size[e.to];
		}
	}
	void dfs1(int u,int f)
	{
		dp1[u]=0;
		for(int i=0;i<g[u].size();i++)
		{
			if(flag[g[u][i]])continue;
			Edge& e=edges[g[u][i]];
			if(e.to==f)continue;
			dfs1(e.to,u);
			dp1[u]+=dp1[e.to]+size[e.to];
		}
	}
	void dfs2(int u,int f,int anc)
	{
		for(int i=0;i<g[u].size();i++)
		{
			if(flag[g[u][i]])continue;
			Edge& e=edges[g[u][i]];
			if(e.to==f)continue;
			dp2[e.to]=dp2[u]+dp1[u]-dp1[e.to]-size[e.to]+size[anc]-size[e.to];
			dfs2(e.to,u,anc);
		}
	}
	int getroot(int u,int f)
	{
		int ans=u;
		for(int i=0;i<g[u].size();i++)
		{
			if(flag[g[u][i]])continue;
			Edge& e=edges[g[u][i]];
			if(e.to==f)continue;
			int x=getroot(e.to,u);
			if(dp1[ans]+dp2[ans]==dp1[x]+dp2[x])ans=min(ans,x);
			if(dp1[ans]+dp2[ans]>dp1[x]+dp2[x])ans=x;
		}
		return ans;
	}
	void getdeep(int u,int f)
	{
		for(int i=0;i<g[u].size();i++)
		{
			if(flag[g[u][i]])continue;
			Edge& e=edges[g[u][i]];
			if(e.to==f)continue;
			d[e.to]=d[u]+1;
			getdeep(e.to,u);
		}
	}
	void deal(int a)
	{
		dfs(a,0);
		dfs1(a,0);
		dp2[a]=0;
		dfs2(a,0,a);
		int root=getroot(a,0);
		d[root]=0;
		getdeep(root,0);
	}
	int work()
	{
		IO::read(n);IO::read(m);
		for(int i=1;i<n;i++)
		{
			int u,v;
			IO::read(u);IO::read(v);
			add(u,v);
		}
		deal(1);
		for(int i=1;i<=m;i++)
		{
			int opt,x;
			IO::read(opt);IO::read(x);
			if(opt==1)
			{
				x--;
				flag[x*2]=flag[x*2+1]=true;
				deal(edges[x*2].from);
				deal(edges[x*2].to);
			}
			if(opt==2)printf("%d\n",d[x]);
		}
		return 0;
	}
}
int main()
{
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	return Dango::work();
}
