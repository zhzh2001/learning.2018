#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<map>
using namespace std;
namespace Dango
{
	const int MAXN=100005;
	map<long long,long long> mapl;
	long long n,lim,L,q,tot;
	int work()
	{
		ios::sync_with_stdio(false);
		cin>>n>>lim>>L;
		for(int i=1;i<=n;i++)
		{
			long long a,b;
			cin>>a>>b;
			mapl[a]=b;
			tot+=b;
		}
		cin>>q;
		for(int i=1;i<=q;i++)
		{
			string opt;
			cin>>opt;
			if(opt[0]=='a')
			{
				long long x;
				cin>>x;
				mapl[x]++;
				tot++;
			}
			if(opt[0]=='b')
			{
				long long x;
				cin>>x;
				mapl[x]--;
				tot--;
			}
			if(opt[0]=='c')
			{
				long long x,y;
				cin>>x>>y;
				mapl[x]--;
				mapl[x-y]++;
			}
			if(opt[0]=='e')
			{
				long long x,y;
				cin>>x>>y;
				mapl[x]--;
				mapl[x+y]++;
			}
			if(opt[0]=='q')
			{
				long long x,ans=0,cnt=tot;
				cin>>x;
				for(int i=1;i<=x;i++)
				{
					ans+=x%i*mapl[i];
					cnt-=mapl[i];
				}
				ans+=x*cnt;
				cout<<ans<<"\n";
			}
		}
		return 0;
	}
}
int main()
{
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	return Dango::work();
}
