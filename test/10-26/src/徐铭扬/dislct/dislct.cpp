#include<bits/stdc++.h>
using namespace std;
const int N=50002;
struct node{
	int to,ne;
}e[N<<1];
int dep[N],h[N],i,sz[N],opt[N],x[N],y[N],n,m,z[N],tot,sum[N],rt,ans[N],now,tmp;
bool vis[N];
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int rd(){
    int x=0,fl=1;char ch=gc();
    for (;ch<48||ch>57;ch=gc())if(ch=='-')fl=-1;
    for (;48<=ch&&ch<=57;ch=gc())x=(x<<3)+(x<<1)+(ch^48);
    return x*fl;
}
inline void wri(int a){if(a<0)a=-a,putchar('-');if(a>=10)wri(a/10);putchar(a%10|48);}
inline void wln(int a){wri(a);puts("");}
void add(int x,int y){
	e[++tot]=(node){y,h[x]};
	h[x]=tot;
}
void dfs1(int u,int fa){
	sz[u]=1;vis[u]=1;tmp++;
	for (int i=h[u],v;i;i=e[i].ne)
		if ((v=e[i].to)!=fa){
			dep[v]=dep[u]+1;sum[now]+=dep[v];
			dfs1(v,u);
			sz[u]+=sz[v];
		}
}
void dfs2(int u,int fa){
	if (sum[u]<sum[rt] || sum[u]==sum[rt] && u<rt) rt=u;
	for (int i=h[u],v;i;i=e[i].ne)
		if ((v=e[i].to)!=fa){
			sum[v]=sum[u]+tmp-sz[v]-sz[v];
			dfs2(v,u);
		}
}
void find(){
	memset(vis,0,n+1);
	for (now=1;now<=n;now++)
		if (!vis[now]){
			tmp=0;
			sum[now]=0;
			dep[now]=0;dfs1(now,0);
			rt=now;
			dfs2(now,0);
			dep[rt]=0;dfs1(rt,0);
		}
}
int main(){
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n=rd();m=rd();
	for (i=1;i<n;i++) x[i]=rd(),y[i]=rd();
	for (i=1;i<=m;i++){
		opt[i]=rd(),z[i]=rd();
		if (opt[i]==1) vis[z[i]]=1;
	}
	for (i=1;i<=n;i++)
		if (!vis[i]) add(x[i],y[i]),add(y[i],x[i]);
	find();
	for (i=m;i;i--)
		if (opt[i]==1) add(x[z[i]],y[z[i]]),add(y[z[i]],x[z[i]]),find();
		else ans[i]=dep[z[i]];
	for (i=1;i<=m;i++)
		if (opt[i]==2) wln(ans[i]);
}
