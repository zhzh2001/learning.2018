#include<bits/stdc++.h>
using namespace std;
const int M=23333,N=1001;
int n,m,i,ans,c[N],a[N],f[N],g[N],x,y;
void dfs(int x,int lim){
	if (x==m){
		if (a[m-1]<=lim){
			int s=f[m],cnt=1;
			a[m]=lim;
			for (int i=1;i<=m;i++) s=s*c[a[i]]%M;
			for (int i=2;i<=m;i++)
				if (a[i]==a[i-1]) cnt++;
				else s=s*g[cnt]%M,cnt=1;
			s=s*g[cnt]%M;
			ans=(ans+s)%M;
		}
		return;
	}
	for (int i=a[x-1];i<=lim;i++) a[x]=i,dfs(x+1,lim-i);
}
void ex_gcd(int a,int b,int &x,int &y){
	if (!b) x=1,y=0;
	else ex_gcd(b,a%b,y,x),y-=a/b*x;
}
int main(){
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n>100 || m>10) return puts("0"),0;
	f[1]=g[1]=1;
	for (i=2;i<=m;i++) f[i]=f[i-1]*i%M,ex_gcd(i,M,x,y),g[i]=g[i-1]*(x+M)%M;
	c[0]=c[1]=1;
	for (i=2;i<=n;i++) ans=0,dfs(1,i-1),c[i]=ans;
	printf("%d",(c[n]+1)%M);
}
