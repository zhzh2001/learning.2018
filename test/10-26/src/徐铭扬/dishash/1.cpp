#include<bits/stdc++.h>
using namespace std;
const int M=23333,N=100001;
int n,m,i,f[N],g[N],ans,x,y;
void ex_gcd(int a,int b,int &x,int &y){
	if (!b) x=1,y=0;
	else ex_gcd(b,a%b,y,x),y-=a/b*x;
}
int main(){
	freopen("1.in","r",stdin);
	freopen("1.out","w",stdout);
	scanf("%d%d",&n,&m);
	f[1]=1;
	for (i=2;i<=2*n;i++) f[i]=1ll*f[i-1]*i%M;
	g[1]=1;
	for (i=2;i<=n+1;i++) ex_gcd(i,M,x,y),g[i]=1ll*g[i-1]*(x+M)%M;
	for (i=1;i<=n;i++) printf("%lld\n",1ll*f[2*i]*g[i]%M*g[i+1]%M);
}
