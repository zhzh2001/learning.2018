#include<bits/stdc++.h>
using namespace std;
const int N=100001;
typedef long long ll;
ll ans;
int n,m,i,x,y,cnt[N];
char s[10];
set<int>S;
set<int>::iterator it;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int rd(){
    int x=0,fl=1;char ch=gc();
    for (;ch<48||ch>57;ch=gc())if(ch=='-')fl=-1;
    for (;48<=ch&&ch<=57;ch=gc())x=(x<<3)+(x<<1)+(ch^48);
    return x*fl;
}
inline void wri(ll a){if(a<0)a=-a,putchar('-');if(a>=10)wri(a/10);putchar(a%10|48);}
inline void wln(ll a){wri(a);puts("");}
int main(){
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n=rd();rd();rd();
	for (i=1;i<=n;i++) x=rd(),y=rd(),cnt[x]=y,S.insert(x);
	for (m=rd();m--;){
		scanf("%s",s);x=rd();
		if (s[0]=='a')
			if (!(cnt[x]++)) S.insert(x);
		if (s[0]=='b')
			if (!(--cnt[x])) S.erase(x);
		if (s[0]=='c'){
			y=x-rd();
			if (!(--cnt[x])) S.erase(x);
			if (!(cnt[y]++)) S.insert(y);
		}
		if (s[0]=='e'){
			y=x+rd();
			if (!(--cnt[x])) S.erase(x);
			if (!(cnt[y]++)) S.insert(y);
		}
		if (s[0]=='q'){
			ans=0;
			for (it=S.begin();it!=S.end();it++) ans+=1ll*x%(*it)*cnt[*it];
			wln(ans);
		}
	}
}

