#include<bits/stdc++.h>
#define LL long long
#define GG long long
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 70000;
int ntr[N*2], pre[N*2], tott[N], cnt, xym[N*2]; 
int yx[N], rt[N], top, dp[N], sz[N], dis[N], wzt, n, m;
inline void add(int x,int y) {
	ntr[++cnt]=y;pre[cnt]=tott[x];tott[x]=cnt;
	ntr[++cnt]=x;pre[cnt]=tott[y];tott[y]=cnt;
}
inline void Pre(int x,int fa) {
	sz[x]=1;
	yx[x]=1;
	for(int i=tott[x];i;i=pre[i]) {
		if(ntr[i]==fa||xym[i])	continue;
		Pre(ntr[i],x);
		sz[x]+=sz[ntr[i]];
	}
}
inline void work_1(int x,int fa) {
	dp[x]=wzt-sz[x];
	for(int i=tott[x];i;i=pre[i]) {
		if(ntr[i]==fa||xym[i])	continue;
		work_1(ntr[i],x);
		dp[x]=max(dp[x],sz[ntr[i]]);
	}
	if(dp[x]<dp[rt[top]])	rt[top]=x;
	if(dp[x]==dp[rt[top]])	if(x<rt[top])	rt[top]=x;
}
inline void lzq(int x,int fa,int d) {
	dis[x]=d;
	for(int i=tott[x];i;i=pre[i]) {
		if(ntr[i]==fa||xym[i])	continue;
		lzq(ntr[i],x,d+1);
	}
}
inline void get_fire() {
	For(i, 1, top)	rt[i]=0;
	top=0;dp[0]=n+1;
	For(i, 1, n)	yx[i]=0;
	For(i, 1, n)	if(!yx[i])	{
		top++;
		Pre(i,i);
		wzt=sz[i];
		work_1(i,i);
	}
	For(i,1,top) lzq(rt[i],rt[i],0);
}
int main()
{
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n = read();m = read();
	For(i, 1, n-1)	add(read(),read());
	get_fire();
	For(i, 1, m) {
		int tx = read(), ty = read();
		if(tx==1) {
			xym[ty*2]=xym[ty*2-1]=1;
			get_fire();
		}
		else
			writeln(dis[ty]);
	}
}



/*


RP++

7 7
2 1
3 1
1 4
1 5
1 6
3 7
2 6
2 4
1 6
1 4
2 7
2 5
2 3




*/

