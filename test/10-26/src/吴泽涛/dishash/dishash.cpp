#include<bits/stdc++.h>
#define LL long long
#define GG long long
#define For(i, j, k) for(LL i=j; i<=k; i++)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const LL Mod = 23333; 
LL n, m; 
LL ans;
LL inv[100000];   

int main() {
	freopen("dishash.in", "r", stdin); 
	freopen("dishash.out", "w", stdout); 
	n = read(); m = read(); 
	ans = 1; 
	LL tmp = n*m; 
	For(i, n*m-n+2, tmp) ans = ans*i % Mod; 
	inv[1] = inv[0] = 1;
	For(i, 2, n) inv[i] = inv[Mod%i]*(Mod-Mod/i) % Mod; 
	For(i, 1, n) ans = ans*inv[i] %Mod; 
	ans = (ans+1) %Mod; 
	writeln(ans);  
}




