#include<bits/stdc++.h>
#define LL long long
#define GG long long 
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 5011; 
int n, lim, L, Q; 
int a[5011]; 
char ch[20]; 
struct node{
	int siz, num; 
}poi[N];

signed main() {
	freopen("dissplay.in", "r", stdin); 
	freopen("dissplay.out", "w", stdout); 
	n = read(); lim = read(); L = read(); 
	For(i, 1, n) {
		poi[i].siz = read(); 
		poi[i].num = read(); 
		a[poi[i].siz] = poi[i].num; 
	}
	Q = read(); 
	while(Q--) {
		scanf("%s", ch+1); 
		int x = read(); 
		if(ch[1] == 'a') {
			++a[x]; 
			continue; 
		}
		if(ch[1] == 'b') {
			--a[x]; 
			continue; 
		}
		if(ch[1] == 'c') {
			int y = read(); 
			--a[x]; ++a[x-y]; 
			continue; 
		}
		if(ch[1] == 'e') {
			int y = read(); 
			int tmp = min(x+y, lim); 
			--a[x]; ++a[tmp]; 
			continue; 
		}
		if(ch[1] == 'q') {
			x = min(x, L); 
			int sum = 0; 
			For(i, 2, lim) 
				sum = sum + x%i * max(0, a[i]); 
			writeln(sum); 
			continue; 
		}
	}
}

/*


5 10 10
4 6
1 4
2 6
7 6
10 10
10
add 1
cut 4 2
cut 10 3
query 3
break 2
expansion 1 4
add 6
cut 7 0
query 5
query 4


*/

