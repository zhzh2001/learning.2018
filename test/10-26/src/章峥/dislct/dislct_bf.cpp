#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("dislct.in");
ofstream fout("dislct.ans");
const int N=300005,INF=1e9;
int n,m,head[N],v[N*2],nxt[N*2],e;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int ans[N],sp,eu[N],ev[N],x[N],y[N],sz[N],val0,minv,minid,vis[N],id,dep[N],nodes[N],cc;
bool del[N];
void dfs(int u,int dep)
{
	vis[u]=id;
	val0+=dep;
	sz[u]=1;
	nodes[++cc]=u;
	for(int i=head[u];i;i=nxt[i])
		if(vis[v[i]]<id)
		{
			dfs(v[i],dep+1);
			sz[u]+=sz[v[i]];
		}
}
void dfs2(int u,int fat,int dep)
{
	vis[u]=id;
	val0+=dep;
	sz[u]=1;
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			dfs2(v[i],u,dep+1);
			sz[u]+=sz[v[i]];
		}
}
/*
void dfs2(int u,int fat,int sum)
{
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			int now=sum+n-sz[v[i]]-sz[v[i]];
			if(now<minv)
			{
				minv=now;
				minid=v[i];
			}
			else if(now==minv&&v[i]<minid)
				minid=v[i];
			dfs2(v[i],u,now);
		}
}
*/
void traverse(int u,int fat)
{
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			dep[v[i]]=dep[u]+1;
			traverse(v[i],u);
		}
}
int main()
{
	fin>>n>>m;
	for(int i=1;i<n;i++)
		fin>>eu[i]>>ev[i];
	for(int i=1;i<=m;i++)
	{
		fin>>x[i]>>y[i];
		if(x[i]==1)
			del[y[i]]=true;
	}
	for(int i=1;i<n;i++)
		if(!del[i])
		{
			add_edge(eu[i],ev[i]);
			add_edge(ev[i],eu[i]);
		}
	++id;
	for(int i=1;i<=n;i++)
		if(vis[i]<id)
		{
			val0=0;
			cc=0;
			dfs(i,0);
			minv=val0;minid=i;
			//dfs2(i,0,val0);
			for(int j=1;j<=cc;j++)
			{
				val0=0;
				dfs2(nodes[j],0,0);
				if(val0<minv)
				{
					minv=val0;
					minid=nodes[j];
				}
				else if(val0==minv)
					minid=min(minid,nodes[j]);
			}
			dep[minid]=0;
			traverse(minid,0);
		}
	for(int i=m;i;i--)
		if(x[i]==1)
		{
			add_edge(eu[y[i]],ev[y[i]]);
			add_edge(ev[y[i]],eu[y[i]]);
			++id;
			for(int i=1;i<=n;i++)
				if(vis[i]<id)
				{
					val0=0;
					cc=0;
					dfs(i,0);
					minv=val0;minid=i;
					//dfs2(i,0,val0);
					for(int j=1;j<=cc;j++)
					{
						val0=0;
						dfs2(nodes[j],0,0);
						if(val0<minv)
						{
							minv=val0;
							minid=nodes[j];
						}
						else if(val0==minv)
							minid=min(minid,nodes[j]);
					}
					dep[minid]=0;
					traverse(minid,0);
				}
		}
		else
			ans[++sp]=dep[y[i]];
	for(;sp;sp--)
		fout<<ans[sp]<<'\n';
	return 0;
}
