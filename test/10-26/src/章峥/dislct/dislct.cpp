#include<fstream>
#include<algorithm>
#include<vector>
using namespace std;
ifstream fin("dislct.in");
ofstream fout("dislct.out");
const int N=300005,INF=1e9;
vector<int> mat[N],layer[N];
int n,m,head[N],v[N*2],nxt[N*2],e;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int sz[N],f[N],dep[N],cent[N],dp[N],ans[N],sp,vis[N],eu[N],ev[N],x[N],y[N];
int getf(int x)
{
	return f[x]?getf(f[x]):x;
}
bool del[N];
void predfs(int u,int fat)
{
	for(int i=0;i<mat[u].size();i++)
		if(mat[u][i]!=fat)
		{
			dep[mat[u][i]]=dep[u]+1;
			layer[dep[mat[u][i]]].push_back(mat[u][i]);
			predfs(mat[u][i],u);
		}
}
void dfs(int u,int fat)
{
	sz[u]=1;
	f[u]=fat;
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			dfs(v[i],u);
			sz[u]+=sz[v[i]];
		}
}
void dfs2(int u,int fat,int root)
{
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			dfs2(v[i],u,root);
			dp[u]=max(dp[u],sz[v[i]]);
		}
	dp[u]=max(dp[u],sz[root]-sz[u]);
	if(dp[u]<dp[cent[root]]||(dp[u]==dp[cent[root]]&&u<cent[root]))
		cent[root]=u;
}
int main()
{
	fin>>n>>m;
	for(int i=1;i<n;i++)
	{
		fin>>eu[i]>>ev[i];
		mat[eu[i]].push_back(ev[i]);
		mat[ev[i]].push_back(eu[i]);
	}
	layer[0].push_back(1);
	predfs(1,0);
	for(int i=1;i<=m;i++)
	{
		fin>>x[i]>>y[i];
		if(x[i]==1)
			del[y[i]]=true;
	}
	for(int i=1;i<n;i++)
		if(!del[i])
		{
			add_edge(eu[i],ev[i]);
			add_edge(ev[i],eu[i]);
		}
	dp[0]=n;
	for(int i=0;i<n;i++)
		for(int j=0;j<layer[i].size();j++)
			if(!sz[layer[i][j]])
			{
				dfs(layer[i][j],0);
				dfs2(layer[i][j],0,layer[i][j]);
			}
	for(int i=1;i<=n;i++)
		vis[i]=m+1;
	for(int i=m;i;i--)
		if(x[i]==1)
		{
			int u=eu[y[i]],v=ev[y[i]];
			if(dep[u]<dep[v])
				swap(u,v);
			int ru=getf(u),rv=getf(v);
			f[u]=v;
			for(int j=v;j;j=f[j])
				sz[j]+=sz[ru];
			int cv=cent[rv];
			cent[rv]=0;
			for(int j=cent[ru];j;j=f[j])
			{
				dp[j]=max(dp[j],sz[rv]-sz[j]);
				if(dp[j]<dp[cent[rv]]||(dp[j]==dp[cent[rv]]&&j<cent[rv]))
					cent[rv]=j;
				dp[f[j]]=max(dp[f[j]],sz[j]);
			}
			for(int j=cv;j;j=f[j])
			{
				dp[j]=max(dp[j],sz[rv]-sz[j]);
				if(dp[j]<dp[cent[rv]]||(dp[j]==dp[cent[rv]]&&j<cent[rv]))
					cent[rv]=j;
				dp[f[j]]=max(dp[f[j]],sz[j]);
			}
		}
		else
		{
			int rt=getf(y[i]);
			for(int j=y[i];j;j=f[j])
				vis[j]=i;
			int a=cent[rt];
			int cnt=0;
			for(;a&&vis[a]>i;a=f[a])
				cnt++;
			for(int j=y[i];j!=a;j=f[j])
				cnt++;
			ans[++sp]=cnt;
		}
	for(;sp;sp--)
		fout<<ans[sp]<<'\n';
	return 0;
}
