#include<cstdio>
#include<cctype>
FILE *fin=fopen("dissplay.in","r"),*fout=fopen("dissplay.out","w");
inline char gc()
{
    static char buf[1<<20],*p1=buf,*p2=buf;
    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<20,fin),p1==p2)?EOF:*p1++;
}
#define dd c = gc()
inline void read(int& x)
{
    x = 0;
    int dd;
    bool f = false;
    for(; !isdigit(c); dd)
        if(c == '-')
            f = true;
    for(; isdigit(c); dd)
        x = (x<<1) + (x<<3) + (c^48);
    if(f)
        x = -x;
}
#undef dd
inline void read(char *s)
{
	int c=gc();
	for(;isspace(c);c=gc());
	for(;!isspace(c);c=gc())
		*s++=c;
}
const int N=100005;
int n,lim,l;
struct BIT
{
	long long tree[N];
	void modify(int x,long long val)
	{
		for(;x<N;x+=x&-x)
			tree[x]+=val;
	}
	long long query(int x)
	{
		long long ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
int main()
{
	read(n);read(lim);read(l);
	long long cnt=0;
	for(int i=1;i<=n;i++)
	{
		int a,b;
		read(a);read(b);
		T.modify(a,1ll*a*b);
		cnt+=b;
	}
	int m;
	read(m);
	while(m--)
	{
		char cmd[20];
		read(cmd);
		int x,y;
		read(x);
		long long ans;
		switch(cmd[0])
		{
			case 'a':
				T.modify(x,x);
				cnt++;
				break;
			case 'b':
				T.modify(x,-x);
				cnt--;
				break;
			case 'c':
				read(y);
				T.modify(x,-x);
				T.modify(x-y,x-y);
				break;
			case 'e':
				read(y);
				T.modify(x,-x);
				T.modify(x+y,x+y);
				break;
			case 'q':
				ans=x*cnt;
				for(int l=1,r;l<=x;l=r+1)
				{
					r=x/(x/l);
					ans-=(T.query(r)-T.query(l-1))*(x/l);
				}
				fprintf(fout,"%lld\n",ans);
				break;
		}
	}
	return 0;
}
