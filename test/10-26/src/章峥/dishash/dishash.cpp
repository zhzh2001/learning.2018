#include<fstream>
using namespace std;
ifstream fin("dishash.in");
ofstream fout("dishash.out");
const int MOD=23333;
int fact[MOD],inv[MOD];
long long qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}while(b/=2);
	return ans;
}
int c(int n,int m)
{
	if(m>n)
		return 0;
	return 1ll*fact[n]*inv[n-m]%MOD*inv[m]%MOD;
}
int lucas(long long n,long long m)
{
	if(!n)
		return 1;
	return 1ll*lucas(n/MOD,m/MOD)*c(n%MOD,m%MOD)%MOD;
}
int main()
{
	fact[0]=1;
	for(int i=1;i<MOD;i++)
		fact[i]=1ll*fact[i-1]*i%MOD;
	inv[MOD-1]=qpow(fact[MOD-1],MOD-2);
	for(int i=MOD-2;i>=0;i--)
		inv[i]=1ll*inv[i+1]*(i+1)%MOD;
	int n,m;
	fin>>n>>m;
	fout<<(1ll*lucas(1ll*n*m,n)*qpow((1ll*n*(m-1)+1)%MOD,MOD-2)+1)%MOD<<endl;
	return 0;
}
