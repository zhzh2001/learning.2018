#include <cstring>
#include <fstream>

const int maxn = 305;
const int mod = 23333;

int dp[maxn];
int dpp[maxn][maxn];

int main()
{
	std::ifstream fin("dishash.in");
	std::ofstream fout("dishash.out");
	int n, m;
	fin >> n >> m;
	dp[0] = dp[1] = 1;
	for(int i = 0; i <= m; ++i)
		dpp[0][i] = 1;
	for(int i = 1; i < n; ++i)
	{
		for(int j = 1; j <= m; ++j)
			for(int k = 0; k <= i; ++k)
				(dpp[i][j] += dp[k] * dpp[i-k][j-1]) %= mod;
		dp[i+1] = dpp[i][m];
	}
	fout << (dp[n] + 1) % mod;
	fin.close();
	fout.close();
	return 0;
}
