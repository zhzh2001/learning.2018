#include <fstream>

const int maxn = 305;
const int mod = 23333;

typedef long long LL;

LL fac[mod];
LL inv[mod];

inline LL pow(LL a, LL b)
{
	LL ans = 1;
	for(; b; b >>= 1, (a *= a) %= mod)
		if(b & 1)
			(ans *= a) %= mod;
	return ans;
}

inline LL C(LL n, LL m)
{
	if(n > m)
		return 0;
	return fac[m] * inv[n] % mod * inv[m - n] % mod;
}

inline LL Lucas(LL n, LL m)
{
	if(n > m)
		return 0LL;
	if(!n)
		return 1LL;
	return (Lucas(n / mod, m / mod) * C(n % mod, m % mod)) % mod;
}

inline void pre()
{
	fac[0] = 1;
	for(int i = 1; i < mod; ++i)
		fac[i] = (fac[i-1] * i) % mod;
	inv[mod-1] = pow(fac[mod-1], mod - 2);
	for(int i = mod - 2; ~i; --i)
		inv[i] = inv[i+1] * (i + 1) % mod;
}

int main()
{
	std::ifstream fin("dishash.in");
	std::ofstream fout("dishash.out");
	LL n, m;
	fin >> n >> m;
	pre();
	fout << (Lucas(n - 1, n * m) * pow(n, mod - 2) % mod + 1) % mod;
	fin.close();
	fout.close();
	return 0;
}
