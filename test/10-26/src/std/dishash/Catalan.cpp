#include <fstream>

const int mod = 23333;

int f[105];

int main()
{
	std::ifstream fin("dishash.in");
	std::ofstream fout("dishash.out");
    int n;
    fin >> n;
    f[0] = f[1] = 1;
    for(int i = 2; i <= n; ++i)
    	for(int j = 0; j < i; ++j)
            (f[i]+=(f[j]*f[i-j-1]) % mod) %= mod;
    fout << f[n] % mod;
	fin.close();
	fout.close();
    return 0;
}
