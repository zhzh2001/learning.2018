#include <cstring>
#include <fstream>

const int maxn = 305;
const int mod = 23333;

int dp[maxn];
int dpp[maxn];

int main()
{
	std::ifstream fin("dishash.in");
	std::ofstream fout("dishash.out");
	int n, m;
	fin >> n >> m;
	dp[0] = 1;
	for(int i = 1; i <= n; ++i)
	{
		memset(dpp, 0, sizeof(dpp));
		dpp[1] = 1;
		for(int j = 1; j <= m; ++j)
		{
			for(int k = i; k >= 0; --k)
			{
				int ans = 0;
				for(int l = 0; l < k; ++l)
					(ans += dp[l] * dpp[k-l]) %= mod;
				dpp[k] = ans;
			}
		}
		dp[i] = dpp[i];
	}
	fout << dp[n];
	fin.close();
	fout.close();
	return 0;
}
