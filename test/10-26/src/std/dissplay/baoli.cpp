#include <cstdio>
#include <map>
#include <iostream>

using namespace std;

typedef long long LL;

int main()
{
	freopen("splay.in", "r", stdin);
	freopen("splay.out", "w", stdout);
	int n;
	scanf("%d%*d%*d", &n);
	map<int, int> mp;
	mp.clear();
	for(int i = 1, a, b; i <= n; ++i)
	{
		scanf("%d%d", &a, &b);
		mp[a] += b;
	}
	int m;
	scanf("%d", &m);
	LL ans;
	while(m--)
	{
//		cout << m << endl;
		char cmd[8];
		int x, y;
		scanf("%s", cmd);
		switch(cmd[0])
		{
			case 'a':
				scanf("%d", &x);
				mp[x]++;
				break;
			case 'b':
				scanf("%d", &x);
				mp[x]--;
				break;
			case 'c':
				scanf("%d%d", &x, &y);
				mp[x]--;
				mp[x-y]++;
				break;
			case 'e':
				scanf("%d%d", &x, &y);
				mp[x]--;
				mp[x+y]++;
				break;
			case 'q':
				scanf("%d", &x);
				ans = 0;
				for(map<int, int>::iterator it = mp.begin(); it != mp.end(); ++it)
					ans += ((LL) x % it->first) * ((LL) it->second);
				printf("%lld\n", ans);
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
