#include <cstdio>
#include <cctype>
#include <cmath>
#include <iostream>

using namespace std;

typedef long long LL;

#define dd c = getchar()
inline void read(int& x)
{
	char dd;
	x = 0;
	bool f = false;
	for(; !isdigit(c); dd)
		if(c == '-')
			f = true;
	for(; isdigit(c); dd)
		x = (x << 1) + (x << 3) + (c ^ 48);
	if(f)
		x = -x;
}
#undef dd

inline void write(LL x)
{
	if(x < 0)
	{
		putchar('-');
		x = -x;
	}
	if(x >= 10)
	{
		write(x / 10);
		x %= 10;
	}
	putchar(x | 48);
}

inline void writeln(LL x)
{
	write(x);
	puts("");
}

const int maxn = 1000005;

int n;
int lim;
int ty;
LL ss;

LL a[maxn];
LL tong[maxn>>3];

#define lowbit(x) (x & -x)

inline void add(int pla, LL x)
{
	for(; pla <= lim; pla += lowbit(pla))
		a[pla] += x;
}

inline LL query(int pla)
{
	LL ans = 0;
	for(; pla; pla -= lowbit(pla))
		ans += a[pla];
	return ans;
}

inline void qadd(int x, int cnt)
{
	if(x <= ty)
		tong[x] += cnt;
	else
	{
		ss += cnt;
		LL tmp = -cnt * (LL) x;
		for(int i = x; i <= lim; i += x)
			add(i, tmp);
	}
}

inline LL qquery(int x)
{
	LL ans = 0;
	for(int i = 1; i <= ty; ++i)
		ans += tong[i] * (x % i);
	ans += ss * (LL) x + query(x);
	return ans;
}

char cmd[8];

int main()
{
	freopen("dissplay.in", "r", stdin);
	freopen("dissplay.out", "w", stdout);
	read(n);
	read(lim);
	read(lim);
	ty = sqrt(lim) + 1;
//	cout << ty << endl;
	for(int i = 1, a, b; i <= n; ++i)
	{
		read(a);
		read(b);
		qadd(a, b);
	}
	int m;
	read(m); // return 0;
	while(m--)
	{
		int x, y;
		scanf("%s", cmd);
		read(x);
		switch(cmd[0])
		{
			case 'a':
				qadd(x, 1);
				break;
			case 'b':
				qadd(x, -1);
				break;
			case 'c':
				read(y);
				qadd(x, -1);
				qadd(x - y, 1);
				break;
			case 'e':
				read(y);
				qadd(x, -1);
				qadd(x + y, 1);
				break;
			default:
				writeln(qquery(x));
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
