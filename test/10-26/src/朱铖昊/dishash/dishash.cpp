#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int mod=23333;
const int N=1005;
int f[N][N],n,m;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int main()
{
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	scanf("%d %d",&n,&m);
	f[0][m]=1;
	for (int i=1;i<=n;++i)
	{
		f[i][1]=f[i-1][m];
		for (int j=2;j<=m;++j)
			for (int k=0;k<=i;++k)
				f[i][j]=(f[i][j]+(ll)f[k][m]*f[i-k][j-1]%mod)%mod;
	}
	cout<<(f[n][m]+1)%mod;
	return 0;
}