#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define pl pair<ll,ll>
const int N=100005;
pl f[N],tmp1,tmp2;
ll ans;
int n,m,lim,q,x,y,r,L;
char s[10];
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void writeln(ll x)
{
	if (x==0)
	{
		puts("0");
		return;
	}
	register int a[20];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
inline void add(int x,int y)
{
	for (int i=x;i<=lim;i+=i&-i)
	{
		f[i].first+=y;
		f[i].second+=(ll)x*y;
	}
}
inline pl ask(int x)
{
	pl ans=(pl){0,0};
	for (;x;x-=x&-x)
	{
		ans.first+=f[x].first;
		ans.second+=f[x].second;
	}
	return ans;
}
int main()
{
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	read(n);
	read(lim);
	read(L);
	for (int i=1;i<=n;++i)
	{
		read(x);
		read(y);
		add(x,y);
	}
	read(q);
	while (q--)
	{
		scanf("%s",s+1);
		read(x);
		if (s[1]=='a')
			add(x,1);
		if (s[1]=='b')
			add(x,-1);
		if (s[1]=='c')
		{
			read(y);
			add(x,-1);
			if (x>y)
				add(x-y,1);
		}
		if (s[1]=='e')
		{
			read(y);
			add(x,-1);
			add(min(lim,x+y),1);
		}
		if (s[1]=='q')
		{
			ans=0;
			for (int l=1;l<=lim;l=r+1)
			{
				if (l>x)
				{
					tmp1=ask(lim);
					tmp2=ask(l-1);
					ans+=(ll)x*(tmp1.first-tmp2.first);
					break;
				}
				r=x/(x/l);
				r=min(r,lim);
				tmp1=ask(r);
				tmp2=ask(l-1);
				ans+=(ll)x*(tmp1.first-tmp2.first);
				ans-=(ll)(x/l)*(tmp1.second-tmp2.second);
			}
			// printf("%lld\n",ans);
			writeln(ans);
		}
	}
	// cout<<clock()<<'\n';
	return 0;
}