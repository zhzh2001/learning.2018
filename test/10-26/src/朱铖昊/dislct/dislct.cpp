#include<bits/stdc++.h>
using namespace std;
const int N=300005;
int to[N*2],la[N],pr[N*2],del[N],cnt;
int FA[N][20],h[N];
int si[N],mxs[N],fa[N],dad[N],zx[N];
int n,m,rt,x,y;
const int inf=1e9+7;
vector<int> ans;
pair<int,int> q[N];
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
inline void dfs(int x)
{
	for (int i=1;i<=18;++i)
		FA[x][i]=FA[FA[x][i-1]][i-1];
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=FA[x][0])
		{
			h[to[i]]=h[x]+1;
			FA[to[i]][0]=x;
			dfs(to[i]);
		}
}
inline void dfs(int x,int y)
{
	fa[x]=y;
	si[x]=1;
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=dad[x]&&!del[i])
		{
			dad[to[i]]=x;
			dfs(to[i],y);
			si[x]+=si[to[i]];
			mxs[x]=max(mxs[x],si[to[i]]);
		}
}
inline void get_rt(int x,int sum)
{
	if (max(sum-si[x],mxs[x])<=sum/2)
		rt=min(rt,x);
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=dad[x]&&!del[i])
			get_rt(to[i],sum);
}
inline void clear(int x,int y)
{
	mxs[x]=0;
	dad[x]=0;
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=y&&!del[i])
			clear(to[i],x);
}
inline int get_fa(int x)
{
	return x==fa[x]?x:fa[x]=get_fa(fa[x]);
}
inline int lca(int x,int y)
{
	if (h[x]<h[y])
		swap(x,y);
	int p=h[x]-h[y];
	for (int i=0;i<=18;++i)
		if ((p>>i)&1)
			x=FA[x][i];
	for (int i=18;i>=0;--i)
		if (FA[x][i]!=FA[y][i])
		{
			x=FA[x][i];
			y=FA[y][i];
		}
	return x==y?x:FA[x][0];
}
int main()
{
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	read(n);
	read(m);
	++cnt;
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		add(x,y);
		add(y,x);
	}
	h[1]=1;
	dfs(1);
	for (int i=1;i<=m;++i)
	{
		read(q[i].first);
		read(q[i].second);
		if (q[i].first==1)
			del[q[i].second*2]=del[q[i].second*2+1]=1;
	}
	for (int i=1;i<=n;++i)
		if (!fa[i])
		{
			rt=inf;
			dfs(i,i);
			get_rt(i,si[i]);
			zx[i]=rt;
		}
	for (int i=m;i;--i)
		if (q[i].first==1)
		{
			x=to[q[i].second*2],y=to[q[i].second*2+1];
			int p=get_fa(x),Q=get_fa(y);
			if (si[p]<si[Q]||(si[p]==si[Q]&&x>y))
			{
				swap(p,Q);
				swap(x,y);
			}
			clear(y,0);
			dfs(y,y);
			dad[y]=x;
			rt=inf;
			int pos=y,sum=si[y]+si[p];
			while (dad[pos])
			{
				mxs[dad[pos]]=max(mxs[dad[pos]],si[pos]);
				si[dad[pos]]+=si[y];
				pos=dad[pos];
				if (max(mxs[pos],sum-si[pos])<=sum/2)
					rt=min(rt,pos);
			}
			zx[p]=rt;
			del[q[i].second*2]=del[q[i].second*2+1]=0;
			fa[y]=p;
		}
		else
		{
			int p=zx[get_fa(q[i].second)];
			int Q=lca(p,q[i].second);
			ans.push_back(h[q[i].second]+h[p]-h[Q]*2);
		}
	for (int i=ans.size()-1;i>=0;--i)
		printf("%d\n",ans[i]);
	return 0;
}