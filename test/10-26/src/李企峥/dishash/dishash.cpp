#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
using namespace std;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
ll n,m,inv[1000100],fac[1000100],sum[1000100];
ll C(ll n,ll m)
{
	return fac[n]*sum[m]%23333*sum[n-m]%23333;
}
int main()
{
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	n=read(); m=read();
	inv[1]=inv[0]=1;fac[1]=fac[0]=1;sum[1]=sum[0]=1;
	For(i,2,n*m)inv[i]=inv[23333%i]*(23333-23333/i)%23333;
	For(i,2,n*m)sum[i]=sum[i-1]*inv[i]%23333;
	For(i,2,n*m)fac[i]=fac[i-1]*i%23333;
	cout<<(C(n*m,n)*inv[(m-1)*n+1]%23333+1)%23333;
}



