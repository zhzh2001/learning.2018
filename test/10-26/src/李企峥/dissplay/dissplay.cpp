#include<bits/stdc++.h>
#define ll long long
#define int long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Forr(i,a,b,c) for(int i=a;i<=b;i+=c)
using namespace std;
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
struct D{
	ll f,l,r,s,c,sz;
}f[1001000];
string s;
ll n,m,l,lim,sum,x,y;
void pushdown(int k)
{
	if(f[k].c!=0)
	{
		f[k*2].c+=f[k].c;
		f[k*2+1].c+=f[k].c;
		f[k*2].s+=f[k].c*f[k*2].sz;
		f[k*2+1].s+=f[k].c*f[k*2+1].sz;
		f[k].c=0;
	}
}
void build(int l,int r,int k)
{
	f[k].l=l,f[k].r=r;f[k].sz=r-l+1;
	if(l==r)
	{
		f[k].s=0;
		return;
	}
	int mid=(l+r)/2;
	build(l,mid,k*2);
	build(mid+1,r,k*2+1);
	f[k].s=f[k*2].s+f[k*2+1].s;
}
void change(int l,int r,int k,ll c)
{
	pushdown(k);
	if(l<=f[k].l&&f[k].r<=r)
	{
		f[k].c+=c;
		f[k].s+=c*f[k].sz;
		return;
	}
	int mid=(f[k].l+f[k].r)/2;
	if(l<=mid)change(l,r,k*2,c);
	if(r>mid)change(l,r,k*2+1,c);
	f[k].s=f[k*2].s+f[k*2+1].s;
}
ll query(int l,int r,int k)
{
	pushdown(k);
	if(l<=f[k].l&&f[k].r<=r)return f[k].s;
	int mid=(f[k].l+f[k].r)/2;
	ll ret=0;
	if(l<=mid)ret+=query(l,r,k*2);
	if(r>mid)ret+=query(l,r,k*2+1);
	f[k].s=f[k*2].s+f[k*2+1].s;
	return ret;
}
signed main()
{
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n=read();lim=read();l=read();
	build(1,l,1);
	For(i,1,n)
	{
		x=read();y=read();
		sum+=y;
		if(x!=1)
		{
			for(int k=x;k<=l;k+=x)
				change(k,k+x-1,1,k*y);
		}else sum-=y;
	}
	m=read();
	For(i,1,m)
	{
		cin>>s;
		if(s[0]=='a')
		{
			x=read();sum++;
			if(x!=1)Forr(k,x,l,x)change(k,k+x-1,1,k);
				else sum--;
		}
		if(s[0]=='b')
		{
			x=read();sum--;
			if(x!=1)Forr(k,x,l,x)change(k,k+x-1,1,-k);
				else sum++;
		}
		if(s[0]=='c')
		{
			x=read(),y=read();
			if(y==0)continue;
			if(x!=1)Forr(k,x,l,x)change(k,k+x-1,1,-k);
				else sum++;
			x-=y;
			if(x!=1)Forr(k,x,l,x)change(k,k+x-1,1,k);
				else sum--;
		}
		if(s[0]=='e')
		{
			x=read(),y=read();
			if(y==0)continue;
			if(x!=1)Forr(k,x,l,x)change(k,k+x-1,1,-k);
					else sum++;
			x+=y;
			if(x!=1)Forr(k,x,l,x)change(k,k+x-1,1,k);
				else sum--;
		}
		if(s[0]=='q')
		{
			x=read();
			cout<<(sum*x-query(x,x,1))<<endl;
		}
	}
	return 0;
}
