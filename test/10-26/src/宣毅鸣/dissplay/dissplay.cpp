#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=200005;
int read(){
	int x=0;char c;
	for (;c<'0'||c>'9';c=getchar());
	for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
	return x;
}
void write(int x){
	if (x>=10)write(x/10);
	putchar(x%10+48);
}
char s[100];
int num[N],n,m,L,Ans[N],a[N];
signed main(){
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	scanf("%lld%lld%lld",&n,&m,&L);
	L=min(L,200LL);
	while (n--){
		int x=read(),y=read();
		a[x]+=y;
		for (int i=0;i<=L;i++)Ans[i]+=y*(i%x);
	}
	scanf("%lld",&n);
	while (n--){
		scanf("%s",s);
		if (s[0]=='a'){
			int x=read();
			a[x]++;
			for (int i=0;i<=L;i++)Ans[i]+=i%x;
		}
		if (s[0]=='b'){
			int x=read();
			a[x]--;
			for (int i=0;i<=L;i++)Ans[i]-=i%x;
		}
		if (s[0]=='c'){
			int x=read(),y=read();
			a[x]--;a[x-y]++;
			for (int i=0;i<=L;i++)Ans[i]-=i%x-i%(x-y);
		}
		if (s[0]=='e'){
			int x=read(),y=read();
			a[x]--;a[x+y]++;
			for (int i=0;i<=L;i++)Ans[i]-=i%x-i%(x+y);
		}
		if (s[0]=='q'){
			int x=read();
			if (x<=L)write(Ans[x]),puts("");
			else {
				int ans=0;
				for (int i=1;i<=m;i++)ans+=(x%i)*a[i];
				write(ans);puts("");
			}
		}
	}
	return 0;
}
