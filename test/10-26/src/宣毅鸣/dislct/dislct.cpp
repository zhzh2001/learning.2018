#include<bits/stdc++.h>
using namespace std;
const int N=600005;
#define int long long
int fa[N],f[N],fi[N],A[N],B[N],hh[N],zz[N],tot,n,flag[N],m,size[N],ne[N];
int read(){
	int x=0;char c;
	for (;c<'0'||c>'9';c=getchar());
	for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
	return x;
}
void jb(int x,int y){
	ne[++tot]=fi[x];
	fi[x]=tot;
	zz[tot]=y;
}
void dfs(int x,int y){
	fa[x]=y;size[x]=1;
	for (int i=fi[x];i;i=ne[i])
		if (!flag[i]&&zz[i]!=y){
            hh[zz[i]]=hh[x]+1;
			dfs(zz[i],x);
			size[x]+=size[zz[i]];
		}
}
struct zz{
	int x,y;
}b[N],now;
void doit(int x){
	hh[x]=0;
	dfs(x,0);
	int l=0,r=1,s=0,h=x;
	b[0].x=x;b[0].y=0;
	memset(f,0,sizeof f);
	f[x]=1;
	while (l<r){
		now=b[l++];
		if (size[x]-2*size[now.x]>=0)continue;
		for (int i=fi[now.x];i;i=ne[i])
			if (!flag[i]&&!f[zz[i]]){
				b[r].x=zz[i];
				b[r].y=now.y+size[x]-2*size[zz[i]];
				if (b[r].y<s||(b[r].y==s&&b[r].x<h))h=b[r].x,s=b[r].y;
				r++;
				f[zz[i]]=1;
			}
	}
	hh[h]=0;
	dfs(h,0);
}
signed main(){
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	tot=0;
	for (int i=1;i<n;i++){
		A[i]=read(),B[i]=read();
		jb(A[i],B[i]);jb(B[i],A[i]);
	}
	doit(1);
	while (m--){
		int opt=read(),x=read();
		if (opt==1){
			int xx=zz[x*2],yy=zz[x*2-1];
			flag[x*2-1]=flag[x*2]=1;
			doit(xx);doit(yy);
		}
		else doit(x),printf("%lld\n",hh[x]);
	}
	return 0;
}
