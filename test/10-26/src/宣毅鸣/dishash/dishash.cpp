#include<bits/stdc++.h>
using namespace std;
const int N=1005,M=23333;
int n,m,f[N][N];
int main(){
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int j=0;j<=n;j++)
		for (int i=1;i<=m;i++){
			if (j<=1){
				f[i][j]=1;
				continue;
			}			
			if (i==1){
				f[i][j]=f[m][j-1];
				continue;
			}
			for (int k=0;k<j;k++)
				(f[i][j]+=f[i-1][j-k]*f[m][k])%=M;
		}
	printf("%d\n",(f[m][n]+1)%M);	
	return 0;
}
