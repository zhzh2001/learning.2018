#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<string>
#include<queue>
#include<vector>
#include<set>
#include<map>
#include<cctype>
#include<ctime>
using namespace std;

namespace TYC
{
	typedef long long ll;
	
	namespace IO
	{
		inline char gc()
		{
		    static char buf[1<<12],*p1=buf,*p2=buf;
		    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<12,stdin),p1==p2)?EOF:*p1++;
		}

		inline ll read()
		{
		    ll x=0;int f=0;char ch=gc();
		    while(!isdigit(ch)) f|=(ch=='-'),ch=gc();
		    while(isdigit(ch)) x=x*10+ch-'0',ch=gc();
		    return f?-x:x;
		}
	}
	
	using IO::gc;
	using IO::read;
	
	const int N=1e5+10;
	int n,ques,Limit,Candy;
	ll num[N];
	
	namespace Subtask1
	{
		void work()
		{
			ques=read();
			char ch;
			int x,y;
			ll ans;
			while(ques--)
			{
				do ch=gc(); while(!isalpha(ch));
				switch(ch)
				{
					case 'a': x=read(),num[x]++;break;
					case 'b': x=read(),num[x]--;break;
					case 'c':
						x=read(),y=read();
						num[x]--,num[x-y]++;
						break;
					case 'e':
						x=read(),y=read();
						num[x]--,num[x+y]++;
						break;
					case 'q':
						x=read();
						ans=0;
						for(int i=1;i<=Limit;i++)
							ans+=(ll)(x%i)*num[i];
						printf("%lld\n",ans);
				}
			}
		}
	}
	
	namespace Subtask2
	{
		ll Ans[N];
		
		void work()
		{
			for(int i=1;i<=Candy;i++)
				for(int j=1;j<=Limit;j++)
					Ans[i]+=(ll)(i%j)*num[j];
			ques=read();
			char ch;
			int x,y;
			while(ques--)
			{
				do ch=gc(); while(!isalpha(ch));
				switch(ch)
				{
					case 'a':
						x=read();
						num[x]++;
						for(int i=1;i<=Candy;i++) Ans[i]+=i%x;
						break;
					case 'b':
						x=read();
						num[x]--;
						for(int i=1;i<=Candy;i++) Ans[i]-=i%x;
						break;
					case 'c':
						x=read(),y=read();
						y=x-y;
						num[x]--,num[y]++;
						for(int i=1;i<=Candy;i++)
							Ans[i]-=i%x,Ans[i]+=i%y;
						break;
					case 'e':
						x=read(),y=read();
						y=x+y;
						num[x]--,num[y]++;
						for(int i=1;i<=Candy;i++)
							Ans[i]-=i%x,Ans[i]+=i%y;
						break;
					case 'q':
						printf("%lld\n",Ans[read()]);break;
				}
			}
		}
	}
	
	namespace Subtask3
	{
		#define lowbit(x) x&(-x)
		ll C[N<<1],Num;
		
		inline void add(int x,ll val)
		{
			for(;x<=Limit;x+=lowbit(x)) C[x]+=val;
		}
		
		inline ll query(int x)
		{
			ll sum=0;
			for(;x;x-=lowbit(x)) sum+=C[x];
			return sum;
		}
		
		inline void Add(int x)
		{
			add(x,(ll)-x*num[x]);
			num[x]++;
			Num++;
			add(x,(ll)x*num[x]);
		}
		
		inline void Dec(int x)
		{
			add(x,(ll)-x*num[x]);
			num[x]--;
			Num--;
			add(x,(ll)x*num[x]);
		}
		
		void work()
		{
			for(int i=1;i<=Limit;i++)
				add(i,i*num[i]),Num+=num[i];
			ques=read();
			char ch;
			int x,y;
			ll ans;
			while(ques--)
			{
				do ch=gc(); while(!isalpha(ch));
				switch(ch)
				{
					case 'a': Add(read());break;
					case 'b': Dec(read());break;
					case 'c':
						x=read(),y=read();
						Dec(x),Add(x-y);
						break;
					case 'e':
						x=read(),y=read();
						Dec(x),Add(x+y);
						break;
					case 'q':
						x=read();
						ans=(ll)x*Num;
						for(int l=1,r=1;l<=x;l=r+1)
						{
							r=x/(x/l);
							ans-=(ll)x/l*(query(r)-query(l-1));
						}
						printf("%lld\n",ans);
				}
			}
		}
	}
	
	void work()
	{
		n=read(),Limit=read(),Candy=read();
		for(int i=1;i<=n;i++)
		{
			int x=read(),y=read();
			num[x]+=y;
		}
		if(n<=3000) {Subtask1::work();return;}
		else if(Candy<=200) {Subtask2::work();return;}
		else {Subtask3::work();return;}
	}
}

int main()
{
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	TYC::work();
	return 0;
}
