#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<string>
#include<queue>
#include<vector>
#include<set>
#include<map>
#include<cctype>
#include<ctime>
using namespace std;

namespace TYC
{
	typedef long long ll;
	const int p=23333;
	ll n,m;

	namespace Subtask_Catalan
	{
		const int N=100010;
		ll fac[N];

		ll qpow(ll x,ll tim)
		{
			ll ans=1;
			for(;tim;x=x*x%p,tim>>=1)
				if(tim&1) ans=ans*x%p;
			return ans;
		}

		void work()
		{
			fac[0]=1;
			for(int i=1;i<=(n<<1);i++) fac[i]=fac[i-1]*i%p;
			ll inv=qpow(n+1,p-2),invfac=qpow(fac[n],p-2);
			cout<<(fac[2*n]*invfac%p*invfac%p*inv%p+1)%p<<"\n";
		}
	}

	namespace Subtask1
	{
		const int N=1010;
		ll dp[N],sum[N][N],tmp[N][N];

		void work()
		{
			dp[0]=1;
			for(int node=1;node<=n;node++)
			{
				sum[node][0]=1;
				for(int i=1;i<=m;i++)
				{
					for(int j=1;j<node;j++)
						for(int k=node-1;k>=j;k--)
							tmp[node][k]=(tmp[node][k]+sum[node][k-j]*dp[j]%p)%p;
					for(int j=1;j<node;j++)
						sum[node][j]=(sum[node][j]+tmp[node][j])%p,tmp[node][j]=0;
				}
				dp[node]=sum[node][node-1];
			}
			cout<<(dp[n]+1)%p<<"\n";
		}
	}

	void work()
	{
		ios::sync_with_stdio(false);
		cin.tie(0);
		cin>>n>>m;
		if(m==2) {Subtask_Catalan::work();return;}
		else Subtask1::work();
	}
}

int main()
{
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	TYC::work();
	return 0;
}
