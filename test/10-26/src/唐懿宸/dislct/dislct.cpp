#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<string>
#include<queue>
#include<vector>
#include<set>
#include<map>
#include<cctype>
#include<ctime>
using namespace std;

namespace TYC
{
	namespace IO
	{
		inline char gc()
		{
		    static char buf[1<<12],*p1=buf,*p2=buf;
		    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<12,stdin),p1==p2)?EOF:*p1++;
		}

		inline int read()
		{
		    int x=0,f=0;char ch=gc();
		    while(!isdigit(ch)) f|=(ch=='-'),ch=gc();
		    while(isdigit(ch)) x=x*10+ch-'0',ch=gc();
		    return f?-x:x;
		}
	}

	using IO::read;

	const int N=300010;
	int n,ques,cnt,tim,Head[N],dep[N],fa[N],belong[N];
	
	struct edge
	{
		int to,next,tag;
	}E[N<<1];
	
	inline void add(int u,int v)
	{
		E[++cnt]=(edge){v,Head[u],1};
		Head[u]=cnt;
		E[++cnt]=(edge){u,Head[v],1};
		Head[v]=cnt;
	}
	
	void dfs(int u,int f,int tim)
	{
		belong[u]=tim;
		dep[u]=dep[f]+1;
		fa[u]=f;
		for(int i=Head[u];i;i=E[i].next)
			if(E[i].tag)
			{
				int v=E[i].to;
				if(v==f) continue;
				dfs(v,u,tim);
			}
	}
	
	int get_dis(int u,int f)
	{
		int ans=0;
		dep[u]=dep[f]+1;
		ans+=dep[u];
		for(int i=Head[u];i;i=E[i].next)
			if(E[i].tag)
			{
				int v=E[i].to;
				if(v==f) continue;
				ans+=get_dis(v,u);
			}
		return ans;
	}
	
	void update(int s)
	{
		++tim,
		dfs(s,0,tim);
		int st=0,ed=0;
		for(int i=1;i<=n;i++)
			if(belong[i]==tim&&dep[i]>dep[st]) st=i;
		dep[st]=0;
		dfs(st,0,tim);
		for(int i=1;i<=n;i++)
			if(belong[i]==tim&&dep[i]>dep[ed]) ed=i;
		int len=dep[ed]-dep[st],mid=len/2,root=ed;
		while(mid)
			root=fa[root],mid--;
		if(len&1)
		{
			int t1=get_dis(root,0),t2=get_dis(fa[root],0);
			if(t1>t2) root=fa[root];
			else if(t1==t2) root=min(root,fa[root]);
		}
		dfs(root,0,tim);
	}
	
	void work()
	{
		n=read(),ques=read();
		for(int i=1;i<n;i++)
		{
			int u=read(),v=read();
			add(u,v);
		}
		dep[0]=-1;
		update(1);
		while(ques--)
		{
			int opt=read(),x=read();
			if(opt==1)
			{
				E[(x<<1)-1].tag=0,E[x<<1].tag=0;
				update(E[(x<<1)-1].to);
				update(E[x<<1].to);
			}
			else printf("%d\n",dep[x]);
		}
	}
}

int main()
{
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	TYC::work();
	return 0;
}
