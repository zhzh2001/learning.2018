#include<iostream>
#include<cstdio>
#include<cctype>
#include<cstring>
#define ll long long
#define maxn 1000000 + 10
using namespace std;
char w[150];
ll n,lim,l,a[maxn],b[maxn],y,high,m,lhw,ans;
ll x;
inline char gc(){
    static char buf[1<<12],*p1=buf,*p2=buf;
    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<12,stdin),p1==p2)?EOF:*p1++;
}
void write(ll x){
    if(x<0){putchar('-');x=-x;}
    if(x>9) write(x/10);
    putchar(x%10+'0');
}
void add(){
	cin>>x;
	a[x]++;
}
void breaks(){
	cin>>x;
	a[x]--;
}
void cut(){
	cin>>x;
	cin>>y;
	a[x]--;
	if(x - y <= 0)
		a[0]++;
	else
		a[x - y]++;
	high = (x - y) > high ? (x - y) : high;
}
void expasion(){
	cin>>x>>y;
	a[x]--;
	if(x + y >= lim)
		a[lim]++;
	else
		a[x + y]++;
	high = (x + y) > high ? (x + y) : high;
}
void lhwwan(){
	cin>>x;
	x = x > l ? l : x;
	for(ll i = 1;i <= high;i++){
		if(i < x){
			ll lhw = x;
			lhw %= i;
			ans += lhw * a[i];
		}
		if(i > x)
			ans += x * a[i];
	}
	cout<<ans<<endl;
	ans = 0;
}
int main(){
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>lim>>l;
	for(ll i = 1;i <= n;i++){
		cin>>x>>y;
		a[x] = y;
		high = x > high ? x : high;
	}
	cin>>m;
	for(ll i = 1;i <= m;i++){
		cin>>w;
		if(w[0] == 'a')
			add();
		else if(w[0] == 'b')
			breaks();
		else if(w[0] == 'c')
			cut();
		else if(w[0] == 'e')
			expasion();
		else if(w[0] == 'q'){
			lhwwan();
		}	
	}
	return 0;
}
