#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m,s,dp[11000000][15];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
void ad(int k,int x,int res,int z){
	if(x==m){
		dp[k][z]+=s*dp[k*m+1][res];//printf("|%d %d %d|\n",k,z,s);
		return;
	}
//	if(k==1&&z==2&&m==2)printf("%d\n",res);
	for(int i=0;i<=res;i++){
//		printf("%d %d %d\n",k*m-m+1+x,i,dp[k*m-m+1+x][i]);
		s*=dp[k*m-m+1+x][i];
		ad(k,x+1,res-i,z);
		s/=dp[k*m-m+1+x][i];
	}
}
void dfs(int k,int d){
//	printf("%d %d %d\n",k,d,m);
	dp[k][0]=1;
	if(d>=n){
		return;
	}
	dp[k][1]=1;
	for(int i=1;i<=m;i++)
		dfs(k*m-m+1+i,d+1);
	for(int i=2;i<=n-d;i++){
		s=1;//printf("|%d %d %d|\n",k,i,s);
		ad(k,1,i-1,i);
	}
}
int main(){
//	freopen("dishash.in","r",stdin);
	freopen("mkb.out","w",stdout);
	for(n=1;n<=5;n++){
		for(m=1;m<=5;m++){
			memset(dp,0,sizeof(dp));
//			printf("%d:\n",n);
			dfs(1,0);
			printf("%d ",dp[1][n]);
		}
		puts("");
	}
	return 0;
}
