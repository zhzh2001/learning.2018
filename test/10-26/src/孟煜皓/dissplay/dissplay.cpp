#include <cstdio>
#include <cctype>
#include <cstring>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
int n, m, k, q;
namespace HH_AK_IOI{ // O(nk+qk)
	long long ans[N];
	void Add(int x){
		for (register int i = 1; i <= k; ++i) ans[i] += i % x;
	}
	void Del(int x){
		for (register int i = 1; i <= k; ++i) ans[i] -= i % x;
	}
	void Main(){
		for (register int i = 1, x, y; i <= n; ++i){
			x = read(), y = read();
			for (register int j = 1; j <= k; ++j) ans[j] += 1ll * y * (j % x); 
		}
		q = read();
		for (register int i = 1, x; i <= q; ++i){
			char opt[10];
			scanf("%s", opt), x = read();
			if (opt[0] == 'a') Add(x);
			if (opt[0] == 'b') Del(x);
			if (opt[0] == 'c') Del(x), Add(x - read());
			if (opt[0] == 'e') Del(x), Add(x + read());
			if (opt[0] == 'q') printf("%lld\n", ans[x]);
		}
	}
}
namespace FY_AK_IOI{ // O(qm)
	int c[N];
	long long Query(int x){
		long long ans = 0;
		for (register int i = 1; i <= m; ++i) ans += 1ll * c[i] * (x % i);
		return ans;
	}
	void Main(){
		for (register int i = 1, x, y; i <= n; ++i)
			x = read(), y = read(), c[x] = y;
		q = read();
		for (register int i = 1, x; i <= q; ++i){
			char opt[10];
			scanf("%s", opt), x = read();
			if (opt[0] == 'a') ++c[x];
			if (opt[0] == 'b') --c[x];
			if (opt[0] == 'c') --c[x], ++c[x - read()];
			if (opt[0] == 'e') --c[x], ++c[x + read()];
			if (opt[0] == 'q') printf("%lld\n", Query(x));
		}
	}
}
int main(){
	freopen("dissplay.in", "r", stdin);
	freopen("dissplay.out", "w", stdout);
	n = read(), m = read(), k = read();
	if (k <= 200) return HH_AK_IOI :: Main(), 0;
	else return FY_AK_IOI :: Main(), 0;
}
