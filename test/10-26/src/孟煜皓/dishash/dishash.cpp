#include <cstdio>
#define N 1005
#define P 23333
int n, m, dp[N][N];
int main(){
	freopen("dishash.in", "r", stdin);
	freopen("dishash.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (register int i = 0; i <= m; ++i) dp[0][i] = 1;
	for (register int i = 1; i <= n; ++i){
		dp[i][1] = dp[i - 1][m];
		for (register int j = 2; j <= m; ++j)
			for (register int k = 0; k < i; ++k)
				(dp[i][j] += dp[i - k][j - 1] * dp[k][m]) %= P;
	}
	printf("%d", (dp[n][m] + 1) % P);
}
