#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define ll long long
#define pll pair<ll,ll>
#define se second
#define fi first
using namespace std;
int n,m,fac[23335];
const int mo=23333;
int power(int x,int y){
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
pll getF(ll x){
	if (x<mo) return pll(fac[x],0);
	pll ans=getF(x/mo);
	ans.se+=x/mo;
	ans.fi=ans.fi*fac[x%mo]%mo;
	ans.fi=ans.fi*power(fac[mo-1],x/mo%(mo-1))%mo;
	return ans;
}
int main(){
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout); 
	scanf("%d%d",&n,&m); fac[0]=1;
	For(i,1,mo-1) fac[i]=1ll*fac[i-1]*i%mo;
	pll x=getF(1ll*n*m);
	pll y=getF(n);
	pll z=getF(1ll*n*m-n+1);
	if (x.se-y.se-z.se) puts("1");
	else printf("%lld\n",(x.fi*power(y.fi*z.fi%mo,mo-2)+1)%mo);
}
//ans=C(n*m,n)/(n*m-n+1,mo-2)
