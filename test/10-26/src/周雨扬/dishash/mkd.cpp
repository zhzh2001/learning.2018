#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define ll long long
#define pll pair<ll,ll>
#define se second
#define fi first
using namespace std;
int n,m,fac[23335];
const int mo=23333;
int power(int x,int y){
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
pll getF(ll x){
	if (x<mo) return pll(fac[x],0);
	pll ans=getF(x/mo);
	ans.se+=x/mo;
	ans.fi=ans.fi*fac[x%mo]%mo;
	ans.fi=ans.fi*power(fac[mo-1],x/mo%(mo-2))%mo;
	return ans;
}
int main(){
	srand(time(NULL));
	freopen("dishash.in","w",stdout);
	printf("%d %d\n",rand()%500+1,rand()%500+1);
}
//ans=C(n*m,n)/(n*m-n+1,mo-2)
