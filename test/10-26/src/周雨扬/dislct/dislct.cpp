#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
using namespace std;
char buf[1<<20],*p1=buf,*p2=buf;
inline char gc(){
    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<20,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define pii pair<int,int>
inline int read(){
	int x=0; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
const int N=300005;
struct edge{
	int to,next,ban;
}e[N*2];
int head[N],tot;
void add(int x,int y){
	e[++tot]=(edge){y,head[x],0};
	head[x]=tot;
}
int n,Q,fa[N][20];
int sz[N],f[N],dep[N];
int rt[N],mx[N],ans[N];
void dfs(int x){
	sz[x]=1; f[x]=rt[x]=x;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa[x][0]){
			dep[e[i].to]=dep[x]+1;
			fa[e[i].to][0]=x;
			dfs(e[i].to);
		}
}
int get(int x){
	return f[x]==x?x:f[x]=get(f[x]);
}
void pathadd(int x,int y,int v){
	for (;x!=y;x=fa[x][0]){
		mx[fa[x][0]]=max(mx[fa[x][0]],sz[x]);
		sz[fa[x][0]]+=v;
	}
}
pii calc(int id){
	int rt=get(id);
	return pii(max(sz[rt]-sz[id],mx[id]),id);
}
int LCA(int x,int y){
	if (dep[x]<dep[y]) swap(x,y);
	int tmp=dep[x]-dep[y];
	For(i,0,18) if (tmp&(1<<i)) x=fa[x][i];
	for (int i=18;i>=0;i--)
		if (fa[x][i]!=fa[y][i])
			x=fa[x][i],y=fa[y][i];
	return x==y?x:fa[x][0]; 
}
int dis(int x,int y){
	return dep[x]+dep[y]-2*dep[LCA(x,y)];
}
int x[N],y[N];
int tp[N],id[N];
void merge(int X,int Y){
	int rtx=get(X),rty=get(Y);
	X=rt[get(X)],Y=rt[get(Y)];
	if (dep[rtx]<dep[rty])
		swap(rtx,rty);
	pathadd(rtx,rty,sz[rtx]);
	f[rtx]=rty;
	pii mn(1e9,1e9);
	for (;X!=Y;X=fa[X][0]){
		if (dep[X]<dep[Y]) swap(X,Y);
		mn=min(mn,calc(X));
	}
	mn=min(mn,calc(X));
	rt[get(X)]=mn.second;
}
inline void writeln(int x){
	if (!x){
		puts("0");
		return;
	}
	static int a[10],top;
	for (top=0;x;a[++top]=x%10,x/=10);
	for (;top;putchar(a[top--]+'0'));
	puts("");
}
int main(){
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n=read(); Q=read();
	For(i,1,n-1){
		x[i]=read(),y[i]=read();
		add(x[i],y[i]);
		add(y[i],x[i]);
	}
	For(i,1,Q){
		tp[i]=read(); id[i]=read();
		if (tp[i]==1){
			e[id[i]*2-1].ban=1;
			e[id[i]*2].ban=1;
		}
	}
	dfs(1);
	For(i,1,18) For(j,1,n)
		fa[j][i]=fa[fa[j][i-1]][i-1];
	For(i,1,n-1) if (!e[i*2].ban)
		merge(x[i],y[i]);
	for (int i=Q;i;i--)
		if (tp[i]==2){
			int fa=rt[get(id[i])];
			ans[i]=dis(id[i],fa);
		}
		else
			merge(x[id[i]],y[id[i]]);
	For(i,1,Q)
		if (tp[i]==2)
			writeln(ans[i]);
} 
