#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
using namespace std;
/*
inline char gc(){
    static char buf[1<<15],*p1=buf,*p2=buf;
    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<15,stdin),p1==p2)?EOF:*p1++;
}
*/
#define gc getchar
#define ll long long
inline int read(){
	int x=0; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
const int N=300005;
struct edge{
	int to,next,ban;
}e[N*2];
int head[N],tot;
void add(int x,int y){
	e[++tot]=(edge){y,head[x],0};
	head[x]=tot;
}
int n,Q,sz[N];
ll sum[N];
int q[N];
void dfs0(int x,int fa){
	sz[x]=1; sum[x]=0; q[++*q]=x;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa){
			dfs0(e[i].to,x);
			sz[x]+=sz[e[i].to];
			sum[x]+=sum[e[i].to]+sz[e[i].to];
		}
}
void dfs1(int x,int fa){
	if (fa) sum[x]=sum[fa]+(sz[q[1]]-sz[x])-sz[x];
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa) dfs1(e[i].to,x);
}
int find(int x){
	*q=0;
	dfs0(x,0); dfs1(x,0);
	int mn=1;
	For(i,2,*q)
		if (sum[q[i]]<sum[q[mn]]||
			(sum[q[i]]==sum[q[mn]]&&q[i]<q[mn])) mn=i;
	return q[mn];
}
int d[N],fr[N];
int dis(int S,int T){
	int h=0,t=1; q[1]=S;
	fr[S]=d[S]=0;
	while (h!=t){
		int x=q[++h];
		for (int i=head[x];i;i=e[i].next)
			if (e[i].to!=fr[x]){
				fr[e[i].to]=x;
				d[e[i].to]=d[x]+1;
				q[++t]=e[i].to;
			}
	}
	return d[T];
}
bool ban[N];
int x[N],y[N];
int tp[N],id[N],ans[N];
int main(){
	freopen("dislct.in","r",stdin);
	freopen("bf.out","w",stdout);
	n=read(); Q=read();
	For(i,1,n-1)
		x[i]=read(),y[i]=read();
	For(i,1,Q){
		tp[i]=read(); id[i]=read();
		if (tp[i]==1) ban[id[i]]=1;
	}
	For(i,1,n-1) if (!ban[i])
		add(x[i],y[i]),add(y[i],x[i]); 
	for (int i=Q;i;i--)
		if (tp[i]==2){
			int fa=find(id[i]);
			ans[i]=dis(id[i],fa);
		}
		else{
			add(x[id[i]],y[id[i]]);
			add(y[id[i]],x[id[i]]);
		}
	For(i,1,Q)
		if (tp[i]==2)
			printf("%d\n",ans[i]);
} 
