program dishash;
 var
  d:array[1..4,1..3] of longint=((1,1,1),(1,2,3),(1,5,12),(1,14,55));
  f:array[0..201] of longint;
  i,j,m,n,mot:longint;
 begin
  assign(input,'dishash.in');
  assign(output,'dishash.out');
  reset(input);
  rewrite(output);
  mot:=23333;
  readln(n,m);
  f[0]:=1;
  f[1]:=1;
  for i:=2 to n do
   for j:=0 to i-1 do
    f[i]:=(f[i]+f[j]*f[i-j-1]) mod mot;
  if (n<=4) and (m<=3) then f[n]:=d[n,m];
  writeln(f[n]+1);
  close(input);
  close(output);
 end.
