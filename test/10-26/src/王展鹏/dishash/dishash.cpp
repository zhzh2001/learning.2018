#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define int long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
const int mod=23333;
ll n,m;
inline ll ksm(ll a,ll b){
	int ans=1; a%=mod;
	for(;b;b>>=1){
		if(b&1)ans=ans*a%mod;
		a=a*a%mod;
	}
	return ans;
}
ll c(ll a,ll b){
	ll ans=1,sum=1;
	for(int i=a;i>a-b;i--)ans=ans*i%mod;
	for(int i=1;i<=b;i++)sum=sum*i%mod;
	return ans*ksm(sum,mod-2)%mod;
}
ll C(ll a,ll b){
	if(a<mod)return c(a,b);
	else return C(a/mod,b/mod)*c(a%mod,b%mod)%mod;
}
signed main(){
	freopen("dishash.in","r",stdin); freopen("dishash.out","w",stdout);
	cin>>n>>m;
	cout<<(C((ll)n*m,n)*ksm((m-1)*n+1,mod-2)+1)%mod;
	/*
	f[1]=f[0]=1; for(int i=0;i<=m;i++)g[i][0]=1;
	for(int i=1;i<n;i++){
		for(int j=1;j<=m;j++){
			for(int o=0;o<=i;o++){
				//g[k]+=g[k-o]*f[o];if(g[k]>=mod2)g[k]-=mod2;
				g[j][i]=(g[j][i]+g[j-1][i-o]*f[o])%mod;
			}
		}
		f[i+1]=g[m][i]; 
	}
	cout<<(f[n]+1)%mod<<endl;
	*/
}
/*
f[n]=sigma(f[a1]*f[a2]*f[a3]*f[a4]*f[a5]) sigma(ai)=n 
*/
