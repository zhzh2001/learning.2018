#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
inline void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x){write(x); putchar('\n');}
const int N=300005;
int n,m,nedge,zs[N],vis[N],ans[N],op[N],V[N],x[N],y[N],dep[N],fa[N],mx[N],gen[N],sz[N];
int son[N],nextt[N<<1],ed[N<<1];
inline void aedge(int a,int b){
	nextt[++nedge]=son[a]; son[a]=nedge; ed[nedge]=b;
}
void Merge(int a,int b){
	fa[b]=a; int rt;
	for(int i=a;i;i=fa[i]){mx[i]=max(mx[i],sz[b]); sz[i]+=sz[b]; rt=i;}
	while(1){
		int t=max(mx[gen[b]],sz[rt]-sz[gen[b]]); if(fa[gen[b]]==0||t*2<sz[rt]||(t*2==sz[rt]&&fa[gen[b]]>gen[b]))break; 
		gen[b]=fa[gen[b]];
		//cout<<sz[rt]<<" zyy "<<gen[b]<<" "<<sz[gen[b]]<<" "<<t<<" "<<mx[gen[b]]<<endl;
	}
	gen[rt]=gen[b];
}
inline int gf(int x){
	return fa[x]?gf(fa[x]):x;
}
int T;
inline int lca(int a,int b){
	T++;
	for(int i=a;i;i=fa[i])zs[i]=T;
	for(int i=b;i;i=fa[i])if(zs[i]==T)return i;
}
inline int getdep(int x){
	return fa[x]?getdep(fa[x])+1:0;
}
void dfs(int p,int fa){
	dep[p]=dep[fa]+1;
	for(int i=son[p];i;i=nextt[i])if(ed[i]!=fa)dfs(ed[i],p);
}
int main(){
	freopen("dislct.in","r",stdin); freopen("dislct.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<n;i++){
		x[i]=read(); y[i]=read();
		aedge(x[i],y[i]); aedge(y[i],x[i]);
	}
	dfs(1,0);
	for(int i=1;i<n;i++)if(dep[x[i]]>dep[y[i]])swap(x[i],y[i]);
	for(int i=1;i<=m;i++){
		op[i]=read(); V[i]=read();
		if(op[i]==1)vis[V[i]]=1;
	}
	for(int i=1;i<=n;i++){gen[i]=i; sz[i]=1;}
	for(int i=1;i<n;i++)if(!vis[i]){
		Merge(x[i],y[i]);
	}
	for(int i=m;i;i--){
		if(op[i]==1){
			Merge(x[V[i]],y[V[i]]); //cout<<x[i]<<" wzp "<<y[i]<<" "<<gen[1]<<endl;
		}
		else {
			int zx=gen[gf(V[i])];
			ans[i]=getdep(V[i])+getdep(zx)-2*getdep(lca(V[i],zx));
		}
	}
	for(int i=1;i<=m;i++)if(op[i]==2)writeln(ans[i]);
}

