#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define PI pair<ll,ll>
#define gc getchar
#define mp make_pair
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
inline void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x){write(x); putchar('\n');}
const int N=100005;
#define lowbit(i) i&-i
int n,m,lim,L;
char ch[100];
ll c1[N],c2[N];
inline void change(int a,ll b){
	ll t=a*b;
	for(int i=a;i<N;i+=lowbit(i)){
		c1[i]+=b; c2[i]+=t;
	}
}
inline PI ask(int x){
	ll ans1=0,ans2=0;
	for(int i=x;i;i-=lowbit(i)){
		ans1+=c1[i]; ans2+=c2[i];
	}
	return mp(ans1,ans2);
}
signed main(){
	freopen("dissplay.in","r",stdin); freopen("dissplay.out","w",stdout);
	n=read(); lim=read(); L=read();
	for(int i=1;i<=n;i++){
		int a=read(),b=read();
		change(a,b);
	}
	m=read();
	while(m--){
		scanf("%s",ch);
		if(ch[0]=='a'){
			int x=read();
			change(x,1);
		}else if(ch[0]=='b'){
			int x=read();
			change(x,-1);
		}else if(ch[0]=='c'){
			int x=read(),y=read();
			change(x,-1);
			change(x-y,1);
		}else if(ch[0]=='e'){
			int x=read(),y=read();
			change(x,-1);
			change(x+y,1);
		}else{
			int x=read(); PI dq=mp(0,0); ll ans=0;
			for(int i=1,pos;i<=x;i=pos+1){
				pos=x/(x/i);
				PI t=ask(pos);
				ans+=(t.first-dq.first)*x-(t.second-dq.second)*(x/i);
				dq=t;
				//cout<<t.first<<" "<<t.second<<endl;
			}//cout<<ans<<" "<<dq.first<<" "<<ask(x).first<<" "<<ask(100000).first<<" "<<ask(N-1).first-dq.first<<endl;
			ans+=(ask(N-1).first-dq.first)*x;
			writeln(ans);
		}
	}
}
/*
a%b=a-b*i
*/
