#include<bits/stdc++.h>
using namespace std;
const int M=100005;
int n,m,now;
int tot,head[M],nx[M<<1],to[M<<1];
int X[M],Y[M],ff[M],xx[M],vis[M];
int zx[M],dep[M],dfx[M<<1][20],id[M],cnt;
int fa[M],val[M],RT[M],sz[M];
int anss[M];
int read()
{
	char ch=getchar();int ff=1,x=0;//cout<<ch<<endl;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}//cout<<ch<<endl;
	while(ch>='0'&&ch<='9') {x=(x<<3)+(x<<1)+ch-48,ch=getchar();}
	return ff*x;
}
void write(int aa)
{//cout<<aa<<endl;
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
}
void jia(int aa,int bb)
{
	tot++;
	nx[tot]=head[aa];
	to[tot]=bb;
	head[aa]=tot;
	return;
}
void dfs(int rt,int faa)
{
	zx[rt]=faa;
	id[rt]=++cnt;
	dfx[cnt][0]=rt;
	dep[rt]=dep[faa]+1;
	for(int i=head[rt];i;i=nx[i])
	{
		int yy=to[i];
		if(yy==faa) continue;
		dfs(yy,rt);
		dfx[++cnt][0]=rt;
	}
	return;
}
int minn(int aa,int bb)
{
	return dep[aa]<dep[bb]?aa:bb;
}
void init()
{
	for(int i=1;(1<<i)<=cnt;++i)
	for(int j=1;j+(1<<i)-1<=cnt;++j)
	{
		dfx[j][i]=minn(dfx[j][i-1],dfx[j+(1<<(i-1))][i-1]);
	}
	return;
}
int getfa(int rt)
{
	return fa[rt]==rt?rt:fa[rt]=getfa(fa[rt]);
}
void getrt(int rt,int faa)
{
	sz[rt]=1;
	int flag=0,zh=getfa(rt);
	for(int i=head[rt];i;i=nx[i])
	{
		int yy=to[i];
		if(yy==faa) continue;
		getrt(yy,rt);
		if(!vis[(i+1)>>1])
		{
			sz[rt]+=sz[yy];
			if(sz[yy]*2>val[zh]) flag=1;
		}
	}
	if((val[zh]-sz[rt])*2>val[zh]) flag=1;
	if(!flag) RT[zh]=min(RT[zh],rt);
	return;
}
int st(int ll,int rr)
{
	if(ll>rr) swap(ll,rr);
	int oo=0,len=rr-ll+1;
	while(1<<(oo+1)<=len) oo++;
	return minn(dfx[ll][oo],dfx[rr-(1<<oo)+1][oo]);
}
void update(int rt,int flag,int zhi)
{
	while(getfa(rt)==flag)
	{
		sz[rt]+=zhi;
		rt=zx[rt];
	}
	return;
}
void bh(int rt,int mx,int bj)
{
	while(rt!=zx[mx])
	{
		int re=0;
		for(int i=head[rt];i;i=nx[i])
		if(!vis[(i+1)>>1])
		{
			int yy=to[i];
			if(yy==zx[rt]) continue;
			if(sz[yy]*2>val[bj]) re=1;
		}
		if((val[bj]-sz[rt])*2>val[bj]) re=1;
		if(!re) now=min(now,rt); 
		rt=zx[rt];
	}
	return;
}
int main()
{
	freopen("distree.in","r",stdin);
	freopen("distree2.out","w",stdout);
	n=read();
	for(int i=1;i<n;++i)
	{
		X[i]=read(),Y[i]=read();
		jia(X[i],Y[i]);
		jia(Y[i],X[i]);
	}
	dfs(1,0);
	init();
	m=read();
	for(int i=1;i<=m;++i)
	{
		ff[i]=read(),xx[i]=read();
		if(ff[i]==1) vis[xx[i]]=1;
	}
	for(int i=1;i<=n;++i) fa[i]=i,val[i]=1;
	for(int i=1;i<n;++i) 
	if(!vis[i])
	{
		int x=getfa(X[i]),y=getfa(Y[i]);
		if(x!=y) val[x]+=val[y],val[y]=0,fa[y]=x;
	}
	memset(RT,0x3f3f3f,sizeof(RT));
	getrt(1,1);
	int qwq=0;
//	for(int i = 1; i <= n; ++i)
//		cout << RT[getfa(i)] << endl;
//	return 0;
	for(int i=m;i>=1;--i)
	{
		if(ff[i]==3)
		{
			anss[++qwq]=RT[getfa(xx[i])];
		}
		else if(ff[i]==2)
		{
			int rt=RT[getfa(xx[i])],lc=st(id[rt],id[xx[i]]);
			anss[++qwq]=dep[rt]+dep[xx[i]]-(dep[lc]<<1);
		}
		else if(ff[i]==1)
		{
			vis[xx[i]]=0;
			int qq=X[xx[i]],pp=Y[xx[i]];
			if(dep[pp]<dep[qq]) swap(pp,qq);
			int fax=getfa(qq),fay=getfa(pp);
			int lc=st(id[RT[fax]],id[RT[fay]]);
			update(qq,fax,val[fay]);
			now=0x3f3f3f3f;
			val[fax]+=val[fay];
			val[fay]=0;
			fa[fay]=fax;
			bh(RT[fax],lc,fax);
			bh(RT[fay],lc,fax);
			RT[fax]=now;
		}
	}
	for(int i=qwq;i>=1;--i) write(anss[i]),puts("");
	return 0;
}
