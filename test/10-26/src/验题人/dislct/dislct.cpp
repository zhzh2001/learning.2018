#include <cstdio>
#include <cctype>
#include <algorithm>

using namespace std;

inline char gc()
{
    static char buf[1<<12],*p1=buf,*p2=buf;
    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<12,stdin),p1==p2)?EOF:*p1++;
}

#define dd c = gc()
inline void read(int& x)
{
    x = 0;
    int dd;
    bool f = false;
    for(; !isdigit(c); dd)
        if(c == '-')
            f = true;
    for(; isdigit(c); dd)
        x = (x<<1) + (x<<3) + (c^48);
    if(f)
        x = -x;
}
#undef dd

int dig[20];
template <typename Int>
inline void writeln(Int x)
{
	if (x < 0)
	{
		putchar('-');
		x = -x;
	}
	int len = 0;
	do
		dig[++len] = x % 10;
	while (x /= 10);
	for (; len; len--)
		putchar(dig[len] + '0');
	putchar('\n');
}

const int maxn = 1000005;

int n, m;

int fa[maxn];
int root[maxn];
int allsiz[maxn];
int nowsiz[maxn];

inline int getfa(int x)
{
	return x == fa[x] ? x : fa[x] = getfa(fa[x]);
}

inline void bing(int a, int b)
{
	int faa = getfa(a);
	int fab = getfa(b);
	allsiz[faa] += allsiz[fab];
	fa[fab] = faa;
}

struct Edge
{
	int to, nxt;
} e[maxn<<1];

int first[maxn];

int cnt = 0;
inline void add_edge(int from, int to)
{
	e[++cnt].nxt = first[from];
	first[from] = cnt;
	e[cnt].to = to;
	e[++cnt].nxt = first[to];
	first[to] = cnt;
	e[cnt].to = from;
}

namespace LCA
{
	int dep[maxn];

	struct Edge
	{
		int to, nxt;
	} e[maxn<<1];

	int first[maxn];

	int cnt = 0;
	inline void add_edge(int from, int to)
	{
		e[++cnt].nxt = first[from];
		first[from] = cnt;
		e[cnt].to = to;
		e[++cnt].nxt = first[to];
		first[to] = cnt;
		e[cnt].to = from;
	}

	int fa[maxn];
	int siz[maxn];
	int son[maxn];

	inline void dfs1(int now)
	{
		siz[now] = 1;
		for(int i = first[now]; i; i = e[i].nxt)
		{
			int to = e[i].to;
			if(fa[now] != to)
			{
				dep[to] = dep[now] + 1;
				fa[to] = now;
				dfs1(to);
				siz[now] += to;
				if(!son[now] || siz[son[now]] < siz[to])
					son[now] = to;
			}
		}
	}

	int ffa[maxn];

	inline void dfs2(int now)
	{
		if(son[now])
		{
			ffa[son[now]] = ffa[now];
			dfs2(son[now]);
		}
		for(int i = first[now]; i; i = e[i].nxt)
		{
			int to = e[i].to;
			if(to != son[now] && to != fa[now])
			{
				ffa[to] = to;
				dfs2(to);
			}
		}
	}

	inline void pre()
	{
		dep[1] = 1;
		dfs1(1);
		fa[1] = ffa[1] = 1;
		dfs2(1);
	}

	inline int lca(int x, int y)
	{
		while(ffa[x] != ffa[y])
		{
			if(dep[ffa[x]] < dep[ffa[y]])
				y = fa[ffa[y]];
			else
				x = fa[ffa[x]];
		}
		if(dep[x] < dep[y])
			return x;
		else
			return y;
	}

	inline int getjl(int a, int b)
	{
		return dep[a] + dep[b] - (dep[lca(a, b)] << 1);
	}
}

struct EEdge
{
	int from, to, del;
} ee[maxn];

int maxsizz[maxn];

inline void dfss(int now)
{
	nowsiz[now] = 1;
	int tt = getfa(now);
	for(int i = first[now]; i; i = e[i].nxt)
	{
		int to = e[i].to;
		if(!nowsiz[to] && getfa(to) == tt)
		{
			dfss(to);
			nowsiz[now] += nowsiz[to];
			maxsizz[now] = max(nowsiz[to], maxsizz[now]);
		}
	}
	maxsizz[now] = max(maxsizz[now], allsiz[getfa(now)] - nowsiz[now]);
	if(maxsizz[now] <= (allsiz[tt] >> 1))
	{
		if(!root[tt])
			root[tt] = now;
		else
			root[tt] = min(root[tt], now);
	}
}

inline void ddfs(int now)
{
	if(!nowsiz[now])
		dfss(now);
	for(int i = LCA::first[now]; i; i = LCA::e[i].nxt)
	{
		int to = LCA::e[i].to;
		if(to != LCA::fa[now])
			ddfs(to);
	}
}

struct ASK
{
	int opt, y;
} ask[maxn];

inline int minnn(int a, int b)
{
	if(LCA::dep[a] < LCA::dep[b])
		return a;
	else
		return b;
}

int anssss[maxn];
int cntans;

int main()
{
	freopen("dislct.in", "r", stdin);
	freopen("dislct.out", "w", stdout);
	read(n), read(m);
	for(int i = 1; i < n; ++i)
	{
		read(ee[i].from);
		read(ee[i].to);
		LCA::add_edge(ee[i].from, ee[i].to);
	}
	LCA::pre();
	for(int i = 1, opt; i <= m; ++i)
	{
		read(opt);
		ask[i].opt = opt;
		read(ask[i].y);
		if(opt == 1)
			ee[ask[i].y].del = true;
	}
	for(int i = 1; i <= n; ++i)
	{
		fa[i] = i;
		allsiz[i] = 1;
	}
	for(int i = 1; i < n; ++i)
	{
		if(!ee[i].del)
		{
			add_edge(ee[i].from, ee[i].to);
			bing(ee[i].from, ee[i].to);
		}
	}
	ddfs(1);
	cntans = 0;
	for(int i = m; i; --i)
	{
		if(ask[i].opt == 1)
		{
			int ra = root[getfa(ee[ask[i].y].from)];
			int rb = root[getfa(ee[ask[i].y].to)];
			int ll = LCA::lca(root[getfa(ee[ask[i].y].from)], root[getfa(ee[ask[i].y].to)]);
			bing(ee[ask[i].y].from, ee[ask[i].y].to);
			add_edge(ee[ask[i].y].from, ee[ask[i].y].to);
			int tt = getfa(ee[ask[i].y].from);
			root[tt] = 0;
			for(int kk = ra; kk != ll; kk = LCA::fa[kk])
			{
				maxsizz[kk] = 0;
				nowsiz[kk] = 1;
				for(int I = first[kk]; I; I = e[I].nxt)
				{
					int to = e[I].to;
					if(to != LCA::fa[kk] && getfa(to) == tt)
					{
						nowsiz[kk] += nowsiz[to];
						maxsizz[kk] = max(nowsiz[to], maxsizz[kk]);
					}
				}
				maxsizz[kk] = max(maxsizz[kk], allsiz[tt] - nowsiz[kk]);
				if(maxsizz[kk] <= (allsiz[tt] >> 1))
				{
					if(!root[tt])
						root[tt] = kk;
					else
						root[tt] = min(root[tt], kk);
				}
			}
			for(int kk = rb; ; kk = LCA::fa[kk])
			{
				maxsizz[kk] = 0;
				nowsiz[kk] = 1;
				for(int I = first[kk]; I; I = e[I].nxt)
				{
					int to = e[I].to;
					if(to != LCA::fa[kk] && getfa(to) == tt)
					{
						nowsiz[kk] += nowsiz[to];
						maxsizz[kk] = max(nowsiz[to], maxsizz[kk]);
					}
				}
				maxsizz[kk] = max(maxsizz[kk], allsiz[tt] - nowsiz[kk]);
				if(maxsizz[kk] <= (allsiz[tt] >> 1))
				{
					if(!root[tt])
						root[tt] = kk;
					else
						root[tt] = min(root[tt], kk);
				}
				if(kk == ll)
					break;
			}
		}
		else
			anssss[++cntans] = LCA::getjl(ask[i].y, root[getfa(ask[i].y)]);
	}
	for(int i = cntans; i; --i)
		writeln(anssss[i]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
