#include <set>
#include <fstream>

using namespace std;

int main()
{
	string aa;
	ifstream fin("distree.in");
	int n;
	fin >> n;
	getline(fin, aa);
	set<string> t;
	t.clear();
	for(int i = 1; i <= n; ++i)
	{
		getline(fin, aa);
		if(t.count(aa))
		{
			puts("NO");
			return 0;
		}
		t.insert(aa);
	}
	puts("YES");
	return 0;
}
