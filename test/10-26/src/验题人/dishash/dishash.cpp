#include<bits/stdc++.h>
using namespace std;
const long long mod=23333;
long long n,m;
long long jc[25005],ny[25005];
long long ksm(long long aa,long long bb)
{
	long long ans=1;
	while(bb)
	{
		if(bb&1) ans=(ans*aa)%mod;
		bb>>=1;
		aa=(aa*aa)%mod;
	}
	return ans;
}
long long C(long long aa,long long bb)
{
	return jc[bb]*ny[aa]%mod*ny[bb-aa]%mod;
}
long long lcs(long long aa,long long bb)
{
	if(aa==0) return 1;
	return lcs(aa/mod,bb/mod)*C(aa%mod,bb%mod)%mod;
}
int main()
{
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	jc[0]=1;
	for(long long i=1;i<mod;++i) jc[i]=(jc[i-1]*i)%mod;
	ny[mod-1]=ksm(jc[mod-1],mod-2);
	for(long long i=mod-1;i>0;--i) ny[i-1]=(ny[i]*i)%mod;
	printf("%lld",(lcs(n-1,n*m)*ksm(n,mod-2)%mod+1)%mod);
	return 0;
}
