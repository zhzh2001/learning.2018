#include <bits/stdc++.h>

#define ll long long

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

ll n,m,L,now,ans,lim,a[100005],b[205][205];
char s[100];

int main(){
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n=read();lim=read();L=read();
	if(L<=200){
		for(ll i=1;i<=n;i++){
			now=read();ans=read();
			for(ll j=0;j<=L;j++){
				b[j][j%now]+=ans;
			}
		}
		m=read();
		for(ll i=1;i<=m;i++){
			scanf("%s",s+1);
			if(s[1]=='a'){
				now=read();
				for(ll j=1;j<=L;j++){
					b[j][j%now]++;
				}
			}
			if(s[1]=='b'){
				now=read();
				for(ll j=1;j<=L;j++){
					b[j][j%now]--;
				}
			}
			if(s[1]=='c'){
				now=read();ans=read();
				for(ll j=1;j<=L;j++){
					b[j][j%now]--;
					b[j][j%(now-ans)]++;
				}
			}
			if(s[1]=='e'){
				now=read();ans=read();
				for(ll j=1;j<=L;j++){
					b[j][j%now]--;
					b[j][j%(now+ans)]++;
				}
			}
			if(s[1]=='q'){
				now=read();
				ans=0;
				for(ll j=0;j<=L;j++){
					ans+=j*b[now][j];
				}
				writeln(ans);
			}
		}
		return 0;
	}
	for(ll i=1;i<=n;i++){
		now=read();
		a[now]+=read();
	}
	m=read();
	for(ll i=1;i<=m;i++){
		scanf("%s",s+1);
		if(s[1]=='a'){
			a[read()]++;
		}
		if(s[1]=='c'){
			now=read();
			a[now]--;
			a[now-read()]++;
		}
		if(s[1]=='q'){
			now=read();
			ans=0;
			for(ll i=1;i<=lim;i++){
				ans+=(a[i]*(now%i));
			}
			writeln(ans);
		}
		if(s[1]=='b'){
			a[read()]--;
		}
		if(s[1]=='e'){
			now=read();
			a[now]--;
			a[now+read()]++;
		}
	}
	return 0;
}
