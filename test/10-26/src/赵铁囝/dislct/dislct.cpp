#include <bits/stdc++.h>

#define Max 10005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Edge{
	int v,to;
}e[Max*2];

int n,m,l,r,top,size,f[Max],g[Max],dep[Max],stk[Max],head[Max];
bool vis[Max*2];

inline void add(int u,int v){
	e[++size].v=v;e[size].to=head[u];head[u]=size;
}

inline void dfs1(int u,int fa){
	f[u]=0;
	g[u]=1;
	stk[++top]=u;
	for(int i=head[u];i;i=e[i].to){
		if(vis[i])continue;
		int v=e[i].v;
		if(v==fa)continue;
		dfs1(v,u);
		g[u]+=g[v];
		f[u]+=f[v]+g[v];
	}
	return;
}

inline void dfs2(int u,int fa){
	for(int i=head[u];i;i=e[i].to){
		if(vis[i])continue;
		int v=e[i].v;
		if(v==fa)continue;
		f[v]=f[u]+top-g[v]*2;
		dfs2(v,u);
	}
	return;
}

inline void dfs3(int u,int fa){
	dep[u]=dep[fa]+1;
	for(int i=head[u];i;i=e[i].to){
		if(vis[i])continue;
		int v=e[i].v;
		if(v==fa)continue;
		dfs3(v,u);
	}
	return;
}

inline int query(int x){
	dfs1(x,x);
	dfs2(x,x);
	int ans=0;
	f[0]=1e9;
	while(top){
//		cout<<f[stk[top]]<<" ";
		if(f[stk[top]]==f[ans]){
			ans=min(ans,stk[top]);
		}
		if(f[stk[top]]<f[ans]){
			ans=stk[top];
		}
		top--;
	}
//	cout<<endl;
	dep[ans]=-1;
	dfs3(ans,ans);
	return dep[x];
}

int main(){
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n=read();m=read();
	if(n>10000){
		query(1);
		for(int i=1;i<=m;i++){
			l=read();r=read();
			if(l==2){
				writeln(dep[r]);
			}
		}
		return 0;
	}
	size=1;
	for(int i=1;i<n;i++){
		l=read();r=read();
		add(l,r);
		add(r,l);
	}
	for(int i=1;i<=m;i++){
		l=read();r=read();
		if(l==1){
			vis[r<<1]=true;
			vis[r<<1|1]=true;
		}
		if(l==2){
			writeln(query(r));
		}
	}
	return 0;
}
