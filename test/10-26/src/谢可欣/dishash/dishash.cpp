#include <iostream>
#include <cstdio>
#include <map>
#define int long long
using namespace std;
const int mod=23333,N=2005;
map <int,int> fac,inv;
inline int power(int x,int k){
	int ans=1;
	for(;k;k>>=1,x=1LL*x*x%mod)if(k&1)ans=1LL*ans*x%mod;
	return ans;
}

int C(int n,int m){
	return fac[n]%mod*inv[m]%mod*inv[n-m]%mod;
}
signed main()
{
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","r",stdout);
	int n,m; scanf("%lld%lld",&n,&m);
	if(n==1){
		puts("2"); return 0;
	}
	if(m==1){
		printf("%lld",(power(2,n-1)+1)%mod);
		return 0;
	}
	if(m==3){
		if(n==2)puts("4");
		if(n==3)puts("6");
		if(n==4)puts("16");
		return 0;
	}
	fac[0]=1; for(int i=1;i<=N;i++) fac[i]=(fac[i-1]*i)%mod;
	inv[N]=power(fac[N],mod-2)%mod;
	for(int i=N-1;i>=1;i--)inv[i]=(inv[i+1]*(i+1))%mod;
	int ans=(C(n*2,n)-C(n*2,n-1)+mod+1)%mod;
//	cout<<C(n+m,n)<<" "<<C(n+m,n+1)<<endl;
	cout<<ans<<endl;
	return 0;
}
/**/
