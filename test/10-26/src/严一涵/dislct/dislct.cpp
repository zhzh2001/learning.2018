#include <iostream>
#include <algorithm>
#include <cstdio>
#include <utility>
#include <vector>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
pair<int,int>a[300003],b[300003];
bool vis[300003];
int n,m,x,y;
vector<int>e[300003],ans;
int sz[300003],ms[300003],dep[300003];
int f[300003],c[300003];
void init(){
	for(int i=1;i<=n;i++){
		f[i]=i;
		c[i]=1;
	}
}
int getf(int v){return f[v]==v?v:f[v]=getf(f[v]);}
void add(int a,int b){
	a=getf(a);
	b=getf(b);
	if(a!=b){
		f[a]=b;
		c[b]+=c[a];
	}
}
int gms(int u,int fa,int n){
	int ans=-1,x;
	sz[u]=1;
	ms[u]=0;
	for(vector<int>::iterator i=e[u].begin();i!=e[u].end();i++){
		if(*i==fa)continue;
		x=gms(*i,u,n);
		if(x!=-1){
			if(ans==-1)ans=x;
			else ans=min(ans,x);
		}
		sz[u]+=sz[*i];
		ms[u]=max(ms[u],sz[*i]);
	}
	ms[u]=max(ms[u],n-sz[u]);
	if(ms[u]<=n/2){
		if(ans==-1)ans=u;
		else ans=min(ans,u);
	}
	return ans;
}
void pdp(int u,int fa,int p){
	dep[u]=p;
	for(vector<int>::iterator i=e[u].begin();i!=e[u].end();i++){
		if(*i==fa)continue;
		pdp(*i,u,p+1);
	}
}
int main(){
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<n;i++){
		x=read();
		y=read();
		a[i]=make_pair(x,y);
	}
	for(int i=1;i<=m;i++){
		x=read();
		y=read();
		if(x==1)vis[y]=1;
		b[i]=make_pair(x,y);
	}
	init();
	for(int i=1;i<n;i++)if(!vis[i]){
		add(a[i].first,a[i].second);
		e[a[i].first].push_back(a[i].second);
		e[a[i].second].push_back(a[i].first);
	}
	for(int i=1;i<=n;i++)dep[i]=-1;
	for(int i=1;i<=n;i++)if(dep[i]==-1){
		pdp(gms(i,-1,c[getf(i)]),-1,0);
	}
	for(int i=m;i>=1;i--){
		if(b[i].first==1){
			int p=b[i].second;
			add(a[p].first,a[p].second);
			e[a[p].first].push_back(a[p].second);
			e[a[p].second].push_back(a[p].first);
			pdp(gms(a[p].first,-1,c[getf(a[p].first)]),-1,0);
		}else{
			ans.push_back(dep[b[i].second]);
		}
	}
	for(vector<int>::reverse_iterator i=ans.rbegin();i!=ans.rend();i++){
		printf("%d\n",*i);
	}
	return 0;
}
