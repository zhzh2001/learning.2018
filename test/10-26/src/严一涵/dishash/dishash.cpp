#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const int md=23333;
int n,m,f[1003][1003];
int main(){
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	n=read();
	m=read();
	for(int i=0;i<=m;i++)f[0][i]=1;
	f[1][0]=1;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			for(int k=0;k<i;k++){
				f[i][j]+=f[i-k][j-1]*f[k][m];
				if(f[i][j]>md)f[i][j]%=md;
			}
		}
	}
	printf("%d\n",(f[n][m]+1)%md);
	return 0;
}
