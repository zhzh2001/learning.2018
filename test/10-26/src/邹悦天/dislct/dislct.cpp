#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
using namespace std;

namespace zuiyt
{
	const int N = 3e5 + 10;
	struct edge
	{
		int to, next;
		bool del;	
	}e[N << 1];
	int n, m, f[N], size[N], head[N], cnt, bl_size[N], belong[N], bl_rot[N], dep[N];
	bool vis[N];
	inline void add(const int a, const int b)
	{
		e[cnt] = (edge){b, head[a], false}, head[a] = cnt++;
	}
	void dfs_rot(const int u, const int fa, const int rot)
	{
		size[u] = 1;
		bl_size[rot]++;
		belong[u] = rot;
		for (int i = head[u]; ~i; i = e[i].next)
		{
			int v = e[i].to;
			if (v == fa || e[i].del)
				continue;
			dfs_rot(v, u, rot);
			size[u] += size[v];
			f[u] = max(f[u], size[v]);
		}
	}
	void dfs_dep(const int u, const int fa)
	{
		for (int i = head[u]; ~i; i = e[i].next)
		{
			int v = e[i].to;
			if (v == fa || e[i].del)
				continue;
			dep[v] = dep[u] + 1;
			dfs_dep(v, u);	
		}	
	}
	void update()
	{
		memset(bl_size, 0, sizeof(int[n + 1]));
		memset(belong, 0, sizeof(int[n + 1]));
		memset(f, 0, sizeof(int[n + 1]));
		for (int i = 1; i <= n; i++)
			if (!belong[i])
			{
				dfs_rot(i, 0, i);
				bl_rot[i] = i;
			}
		for (int i = 1; i <= n; i++)
			f[i] = max(f[i], bl_size[belong[i]] - size[i]);
		for (int i = 1; i <= n; i++)
			if (f[i] < f[bl_rot[belong[i]]])
				bl_rot[belong[i]] = i;
		for (int i = 1; i <= n; i++)
			if (i == bl_rot[belong[i]])
			{
				dep[i] = 0;
				dfs_dep(i, 0);	
			}
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin.tie(0);
		cin >> n >> m;
		memset(head, -1, sizeof(int[n + 1]));
		for (int i = 1; i < n; i++)
		{
			int a, b;
			cin >> a >> b;
			add(a, b), add(b, a);
		}
		update();
		bool changed = false;
		while (m--)
		{
			int x, y;
			cin >> x >> y;
			if (x == 1)
			{
				changed = true;
				e[(y - 1) << 1].del = e[(y - 1) << 1 | 1].del = true;
			}
			else
			{
				if (changed)
					update(), changed = false;
				cout << dep[y] << '\n';
			}
		}
		return 0;	
	}	
}
int main()
{
	freopen("dislct.in", "r", stdin);
	freopen("dislct.out", "w", stdout);
	return zuiyt::work();	
}
