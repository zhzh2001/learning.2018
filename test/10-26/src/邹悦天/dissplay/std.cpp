#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <map>
#include <string>
#include <ctime>
using namespace std;

namespace zuiyt
{
	typedef long long ll;
	int n, lim, L, m;
	map<int, int> mp;
	inline void add(const int x)
	{
		if (mp.count(x))
			++mp[x];
		else
			mp[x] = 1;	
	}
	inline void del(const int x)
	{
		if (!(--mp[x]))
			mp.erase(x);	
	}
	inline ll query(const int x)
	{
		ll ans = 0;
		for (map<int, int>::iterator it = mp.begin(); it != mp.end(); it++)
			ans += x % it->first * (ll)it->second;	
		return ans;
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin.tie(0);
		cin >> n >> lim >> L;
		for (int i = 0; i < n; i++)
		{
			int a, b;
			cin >> a >> b;
			mp[a] = b;	
		}
		cin >> m;
		for (int i = 0; i < m; i++)
		{
			string opt;
			cin >> opt;
			if (opt == "add")
			{
				int x;
				cin >> x;
				add(x);
			}
			else if (opt == "break")
			{
				int x;
				cin >> x;
				del(x);
			}
			else if (opt == "cut")
			{
				int x, y;
				cin >> x >> y;
				del(x);
				add(x - y);
			}
			else if (opt == "expansion")
			{
				int x, y;
				cin >> x >> y;
				del(x);
				add(x + y);	
			}
			else if (opt == "query")
			{
				int x;
				cin >> x;
				cout << query(x) << '\n';
			}
		}
		cerr << clock() << ' ';
		return 0;	
	}	
}
int main()
{
	freopen("dissplay.in", "r", stdin);
	freopen("std.out", "w", stdout);
	return zuiyt::work();
}
