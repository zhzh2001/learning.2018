#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <map>
#include <string>
using namespace std;

namespace zuiyt
{
	typedef long long ll;
	const int N = 1e5 + 10;
	int n, lim, L, m;
	ll num;
	namespace Tree_Array
	{
		ll data[N];
		inline int lowbit(const int x)
		{
			return x & (-x);	
		}	
		inline void add(int a, const ll x)
		{
			while (a < N)
				data[a] += x, a += lowbit(a);
		}
		inline ll query(int a)
		{
			ll ans = 0;
			while (a)
				ans += data[a], a -= lowbit(a);
			return ans;
		}
		inline ll query(const int a, const int b)
		{
			if (a)
				return query(b) - query(a - 1);
			else
				return query(b);	
		}
	}
	inline void add(const int x)
	{
		++num;
		Tree_Array::add(x, x);	
	}
	inline void del(const int x)
	{
		--num;
		Tree_Array::add(x, -x);	
	}
	inline ll query(const int x)
	{
		ll ans = (ll)num * x;
		int pos = 1;
		while (pos <= x)
		{
			int tmp = x / (x / pos);
			ans -= Tree_Array::query(pos, tmp) * (x / pos);
			pos = tmp + 1;
		}
		return ans;
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin.tie(0);
		cin >> n >> lim >> L;
		for (int i = 0; i < n; i++)
		{
			int a, b;
			cin >> a >> b;
			Tree_Array::add(a, (ll)a * b);
			num += b;
		}
		cin >> m;
		for (int i = 0; i < m; i++)
		{
			string opt;
			cin >> opt;
			if (opt == "add")
			{
				int x;
				cin >> x;
				add(x);
			}
			else if (opt == "break")
			{
				int x;
				cin >> x;
				del(x);
			}
			else if (opt == "cut")
			{
				int x, y;
				cin >> x >> y;
				del(x);
				add(x - y);
			}
			else if (opt == "expansion")
			{
				int x, y;
				cin >> x >> y;
				del(x);
				add(x + y);	
			}
			else if (opt == "query")
			{
				int x;
				cin >> x;
				cout << query(x) << '\n';
			}
		}
		return 0;
	}	
}
int main()
{
	freopen("dissplay.in", "r", stdin);
	freopen("dissplay.out", "w", stdout);
	return zuiyt::work();
}
