#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cstring>
using namespace std;

namespace zuiyt
{
	typedef long long ll;
	const int N = 1e3 + 10, Jumpmelon = 23333;
	ll dp[N], f[N][N];
	int n, m;
	ll dfs(const int pos, const int n)
	{
		if (f[pos][n])
			return f[pos][n];
		if (pos == m - 1)
			return f[pos][n] = dp[n];
		for (int i = 0; i <= n; i++)
			f[pos][n] = (f[pos][n] + dp[i] * dfs(pos + 1, n - i) % Jumpmelon) % Jumpmelon;
		return f[pos][n];
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin.tie(0);
		cin >> n >> m;
		dp[0] = 1;
		for (int i = 1; i <= n; i++)
			dp[i] = dfs(0, i - 1);
		cout << (dp[n] + 1) % Jumpmelon;
		return 0;
	}	
}
int main()
{
	freopen("dishash.in", "r", stdin);
	freopen("dishash.out", "w", stdout);
	return zuiyt::work();	
}
