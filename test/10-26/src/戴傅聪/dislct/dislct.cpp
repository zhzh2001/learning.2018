#include<bits/stdc++.h>
using namespace std;
#define maxn 300100
int n,q;
struct LinkCutTree
{
	int ch[maxn][2],fa[maxn],size[maxn],si[maxn],esi[maxn];bool rev[maxn];
	inline bool nroot(int p){return ch[fa[p]][1]==p||ch[fa[p]][0]==p;}
	inline void pushup(int p){size[p]=size[ch[p][0]]+size[ch[p][1]]+si[p]+1;esi[p]=esi[ch[p][0]]+esi[ch[p][1]]+1;}
	inline void setrev(int p){if(p==0) return;swap(ch[p][0],ch[p][1]);rev[p]^=1;}
	inline void pushdown(int p){if(!rev[p]) return;setrev(ch[p][0]),setrev(ch[p][1]);rev[p]=0;}
	inline void Pushdown(int p){if(nroot(p)) Pushdown(fa[p]);pushdown(p);}
	inline void rotate(int p)
	{
		int f=fa[p],ff=fa[f],t=(ch[f][1]==p),w=ch[p][t^1];
		if(nroot(f)) ch[ff][ch[ff][1]==f]=p;ch[f][t]=w;ch[p][t^1]=f;
		if(w) fa[w]=f;fa[p]=ff;fa[f]=p;pushup(f);pushup(p);
	}
	inline void splay(int p)
	{
		Pushdown(p);
		while(nroot(p))
		{
			int f=fa[p],ff=fa[f];
			if(nroot(f)) rotate((ch[f][0]==p)^(ch[ff][0]==f)?f:p);
			rotate(p);
		}
	}
	inline void access(int x)
	{
		for(int y=0;x;x=fa[y=x])
		{
			splay(x);
			si[x]+=size[ch[x][1]];ch[x][1]=y;si[x]-=size[y];
			pushup(x);
		}
	}
	inline void makeroot(int x){access(x);splay(x);setrev(x);}
	inline void split(int x,int y){makeroot(x);access(y);splay(y);}
	inline void link(int x,int y){makeroot(x);fa[x]=y;si[y]+=size[x];}
	inline int getdis(int x,int y){split(x,y);return esi[y];}
	inline int findroot(int x)
	{
		access(x);splay(x);
		while(pushdown(x),ch[x][0]) x=ch[x][0];
		return x;
	}
	inline void dfs(int x)
	{
		if(x==0) return;
		pushdown(x);
		dfs(ch[x][0]);
		cout<<x<<' ';
		dfs(ch[x][1]);
	}
	inline int getcenter(int x,int y)
	{
		split(x,y);int lsize,rsize;
		lsize=rsize=0;
		int cur=y,ans=0,minv=1e9;
		while(cur)
		{
			pushdown(cur);
			int ls=size[ch[cur][0]]+lsize,rs=size[ch[cur][1]]+rsize;
			int v=max(ls,rs);
			if(v<minv) ans=cur,minv=v;
			else if(v==minv&&cur<ans) ans=cur;
			if(ls==rs) break;int nxt;
			if(ls>rs) rsize+=size[ch[cur][1]]+1+si[cur],nxt=ch[cur][0];
			else lsize+=size[ch[cur][0]]+1+si[cur],nxt=ch[cur][1];
			if(nxt==0){splay(cur);break;}
			else cur=nxt;
		}
		return ans;
	}
}lct;
int fa[maxn];
inline int find(int x){return x==fa[x]?x:fa[x]=find(fa[x]);}
int ans[maxn],top;
int opt[maxn],x[maxn];
int u[maxn],v[maxn];bool isin[maxn];
inline void init()
{
	scanf("%d%d",&n,&q);
	for(int i=1;i<n;i++) scanf("%d%d",u+i,v+i),isin[i]=1;
	for(int i=1;i<=q;i++)
	{
		scanf("%d%d",opt+i,x+i);
		if(opt[i]==1) isin[x[i]]=0;
	}
	for(int i=1;i<=n;i++) fa[i]=i;
	for(int i=1;i<n;i++) if(isin[i])
	{
		lct.link(u[i],v[i]);int x=lct.getcenter(find(u[i]),find(v[i]));
		fa[find(u[i])]=fa[find(v[i])]=fa[find(x)]=fa[x]=x;
	}
}
int main()
{
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	init();
	reverse(opt+1,opt+q+1);reverse(x+1,x+q+1);
	for(int i=1;i<=q;i++)
	{
		if(opt[i]==2) ans[++top]=lct.getdis(find(x[i]),x[i]);
		if(opt[i]==1)
		{
			lct.link(u[x[i]],v[x[i]]);int X=lct.getcenter(find(u[x[i]]),find(v[x[i]]));
			isin[x[i]]=1;fa[find(u[x[i]])]=fa[find(v[x[i]])]=fa[find(X)]=fa[X]=X;
		}
	}
	while(top) printf("%d\n",ans[top]-1),top--;
	return 0;
}
