#include<bits/stdc++.h>
using namespace std;
#define mod 23333
inline int ksm(int a,int b)
{
	int res=1;
	for(;b;b>>=1,a=1ll*a*a%mod) if(b&1) res=1ll*res*a%mod;
	return res;
}
int f[1001];int n,m;
inline void Add(int &x,int y)
{
	x+=y;
	if(x>=mod) x-=mod;
}
bool vis[1001][1001];int F[1001][1001];
inline int dfs(int x,int y)
{
	if(vis[x][y]) return F[x][y];
	if(y==1) return f[x];vis[x][y]=1;F[x][y]=0;
	for(int i=0;i<=x;i++) Add(F[x][y],dfs(i,y-1)*f[x-i]%mod);
	return F[x][y];
}
int main()
{
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	scanf("%d%d",&n,&m);f[0]=1;
	for(int i=1;i<=n;i++) f[i]=dfs(i-1,m);
	printf("%d",f[n]+1);
}
