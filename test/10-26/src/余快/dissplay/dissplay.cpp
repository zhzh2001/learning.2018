/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (re int i=(a);i<=(b);i++)
#define D(i,a,b) for (re int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 100055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
//#define getchar gc
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(ll x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(ll x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,lim,L,a[N],x,y;
ll num;
char str[N];
struct tr{
	int l,r;ll sum;
}t[N*4];
void build(int x,int l,int r){
	t[x].l=l;t[x].r=r;
	if (l==r){
		t[x].sum=1LL*a[l]*l;return;
	}
	int mid=(l+r)>>1;
	build(x*2,l,mid);build(x*2+1,mid+1,r);t[x].sum=t[x*2].sum+t[x*2+1].sum;
}
inline void add(int x,int k,int p){
	t[x].sum+=k*p;
	if (t[x].l==t[x].r) return;
	int mid=(t[x].l+t[x].r)>>1;
	if (k<=mid) add(x*2,k,p);
	else add(x*2+1,k,p);
}
inline ll query(int x,int p){
	int a=p/t[x].l,b=p/t[x].r;
	return (a==b)?a*t[x].sum:query(x*2,p)+query(x*2+1,p);
}
signed main(){
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n=read();lim=read();L=read();
	F(i,1,n){
		x=read();a[x]=read();num+=a[x];
	}
	build(1,1,lim);
	for (int q=read();q;q--){
		scanf("%s",str+1);
		if (str[1]=='a') add(1,read(),1),num++;
		if (str[1]=='b') add(1,read(),-1),num--;
		if (str[1]=='c'){
			x=read();y=read();
			add(1,x,-1);add(1,x-y,1);
		}
		if (str[1]=='e'){
			x=read();y=read();
			add(1,x,-1);add(1,x+y,1);
		}
		if (str[1]=='q'){
			x=read();wrn(1LL*num*x-query(1,x));
		}
	}
	return 0;
}
