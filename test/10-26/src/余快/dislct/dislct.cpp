/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
//#define getchar gc
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int suma,sum[N],n,m,ans[N],u[N],v[N],vis[N],cnt,_gfa[N],zh,gfa[N],siz[N],fa[N],b[N],a[N];
struct xx{
	int x,y;
}z[N];
int Next[N*2],head[N],to[N*2],nedge;
#define V to[i]
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;}
void add_ne(int a,int b){add(a,b);add(b,a);}
void dfs1(int x,int y){
	sum[x]=1;suma++;vis[x]=1;
	go(i,x){
		if (V==y) continue;
		dfs1(V,x);sum[x]+=sum[V];
	}
}
void g_zx(int x,int y){
	int pd=1;
	go(i,x){
		if (V==y) continue;
		if (sum[V]*2>suma||(sum[V]*2==suma&&V<x)){
			g_zx(V,x);pd=0;
		}
	}
	if (pd) zh=x;
} 
void dfs2(int x){
	gfa[x]=cnt;siz[x]=1;
	go(i,x){
		if (V==fa[x]) continue;
		fa[V]=x;dfs2(V);siz[x]+=siz[V];
	}
}
struct segent_tree{
	//支持区间加，单点查
	struct paweqa{
		int l,r,num;
	}t[N*4];
	void build(int x,int l,int r){
		t[x].l=l;t[x].r=r;
		if (l==r){
			t[x].num=siz[b[l]];return;
		}
		int mid=(l+r)>>1;
		build(x*2,l,mid);build(x*2+1,mid+1,r);
	}
	void add(int x,int l,int r,int k){
		if (l>r) return;
		if (t[x].l==l&&t[x].r==r){
			t[x].num+=k;return;
		}
		int mid=(t[x].l+t[x].r)>>1;
		if (r<=mid) add(x*2,l,r,k);
		else if (l>mid) add(x*2+1,l,r,k);
		else add(x*2,l,mid,k),add(x*2+1,mid+1,r,k);
	}
	int query(int x,int k){
		if (t[x].l==t[x].r) return t[x].num;
		int mid=(t[x].l+t[x].r)>>1;
		if (k<=mid) return query(x*2,k)+t[x].num;
		else return query(x*2+1,k)+t[x].num;
	} 
}s_t;
struct ppa{
	int fa[N],sum[N],top[N],tot,son[N],dis[N],len[2];
	pa alb[2][N];
	void dfs(int x){
		sum[x]=1;son[x]=0;
		go(i,x){
			if (V==fa[x]) continue;
			fa[V]=x;dis[V]=dis[x]+1;dfs(V);sum[x]+=sum[V];
			if (sum[V]>sum[son[x]]) son[x]=V;
		}
	}
	void g_lian(int x){
		if (!top[x]) top[x]=x;
		a[x]=++tot;b[tot]=x;
		if (son[x]) top[son[x]]=top[x],g_lian(son[x]);
		go(i,x){
			if (V==son[x]||V==fa[x]) continue;
			g_lian(V);
		}
	}
	void init(){
		dfs(1);
		g_lian(1);
		s_t.build(1,1,n);
	}
	void add(int x,int y,int k){
		while (top[x]!=top[y]){
			if (dis[x]>dis[y]) swap(x,y);
			s_t.add(1,a[top[y]],a[y],k);
			y=fa[top[y]];
		}
		if (dis[x]>dis[y]) swap(x,y);
		if (dis[y]!=dis[x]) s_t.add(1,a[x],a[y],k);
	}
	int query_dis(int x,int y){
		int sum=0;
		while (top[x]!=top[y]){
			if (dis[x]>dis[y]) swap(x,y);
			sum+=a[y]-a[top[y]]+1;
			y=fa[top[y]];
		}
		if (dis[x]>dis[y]) swap(x,y);
		sum+=a[y]-a[x];
		return sum;
	}
	void mo(int x,int y,int k){
		len[0]=len[1]=0;
		int e=0;
		while (top[x]!=top[y]){
			if (dis[x]>dis[y]) swap(x,y),e^=1;
			alb[e][++len[e]]=mp(a[y],a[top[y]]);
			y=fa[top[y]];
		}
		if (dis[x]>dis[y]) swap(x,y),e^=1;
		if (dis[y]!=dis[x]) alb[e][++len[e]]=mp(a[y],a[x]);
		D(i,len[1],1) alb[0][++len[0]]=alb[1][i],swap(alb[0][len[0]].fi,alb[0][len[0]].se);
		
		F(i,1,len[0]){
			int pd=(alb[0][i].fi<=alb[0][i].se)?1:-1;
			for (int j=alb[0][i].fi;;j+=pd){
				if (i==1&&j==alb[0][i].fi) continue;
				int t=s_t.query(1,j);
				if (t*2>s_t.query(1,a[_gfa[k]])||((t*2==s_t.query(1,a[_gfa[k]]))&&b[j]<=_gfa[k])){
					s_t.add(1,a[_gfa[k]],a[_gfa[k]],-t);
					_gfa[k]=b[j];
				}
				else return;
				if (j==alb[0][i].se) break;
			}
		}
	}
}tr;
void cg(int x,int y,int k){
	gfa[x]=k;siz[x]=1;
	go(i,x){
		if (gfa[V]!=y) continue;
		fa[V]=x;cg(V,y,k);siz[x]+=siz[V];
	}
	int pp=s_t.query(1,a[x]);
	s_t.add(1,a[x],a[x],siz[x]-pp);
}
void bing(int x,int y){
	if (x==94&&y==90){
		y=y;
	}
	int fx=gfa[x],fy=gfa[y];
	if (siz[_gfa[fx]]<_gfa[siz[_gfa[fy]]]) swap(fx,fy),swap(x,y);
	//重构+链加+单点查+单点改 
	fa[y]=x;cg(y,fy,fx);
	siz[_gfa[fx]]+=siz[y];
	s_t.add(1,a[_gfa[fx]],a[_gfa[fx]],siz[y]);
	tr.add(x,_gfa[fx],siz[y]);
	tr.mo(_gfa[fx],y,fx);
}
signed main(){
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n=read();m=read();
	F(i,1,n-1){
		u[i]=read();v[i]=read();
	}
	F(i,1,m){
		z[i].x=read();z[i].y=read();
		if (z[i].x==1) vis[z[i].y]=1;
	}
	F(i,1,n-1){
		if (!vis[i]) add_ne(u[i],v[i]);
		vis[i]=0;
	}
	F(i,1,n){
		if (!vis[i]){
			suma=0;cnt++;
			dfs1(i,0);
			g_zx(i,0);_gfa[cnt]=zh;
			dfs2(zh);
			siz[zh]=suma;
		}
	}
	F(i,1,n) head[i]=0;nedge=0;
	F(i,1,n-1) add_ne(u[i],v[i]);
	tr.init();
	D(i,m,1){
		if (z[i].x==2) ans[i]=tr.query_dis(z[i].y,_gfa[gfa[z[i].y]]);
		else bing(u[z[i].y],v[z[i].y]);
		/*if (i==12){
			wrn(z[i].y,_gfa[gfa[z[i].y]]);
			wrn(s_t.query(1,z[i].y),s_t.query(1,_gfa[gfa[z[i].y]]));
		}*/
	}
	F(i,1,m){
		if (z[i].x==2) wrn(ans[i]);
	}
	return 0;
}
