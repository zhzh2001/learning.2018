#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

#define rep(i,x,y) for (register int i=(x);i<=(y);i++)
#define drp(i,x,y) for (register int i=(x);i>=(y);i--)

inline char read(){
    static const int IN_LEN=1000000;
    static char buf[IN_LEN],*s,*t;
    return (s==t?t=(s=buf)+fread(buf,1,IN_LEN,stdin),(s==t?-1:*s++):*s++);
}
template<class T>
inline void read(T &x){
    static bool iosig;
    static char c;
    for (iosig=false,c=read();!isdigit(c);c=read()){
        if (c=='-') iosig=true;
        if (c==-1) return;
    }
    for (x=0;isdigit(c);c=read()) x=((x+(x<<2))<<1)+(c^'0');
    if (iosig) x=-x;
}
const int OUT_LEN=10000000;
char obuf[OUT_LEN],*ooh=obuf;
inline void print(char c){
    if (ooh==obuf+OUT_LEN) fwrite(obuf,1,OUT_LEN,stdout),ooh=obuf;
    *ooh++=c;
}
template<class T>
inline void print(T x){
    static int buf[30],cnt;
    if (x==0) print('0');
    else{
        if (x<0) print('-'),x=-x;
        for (cnt=0;x;x/=10) buf[++cnt]=x%10+48;
        while (cnt) print((char)buf[cnt--]);
    }
}
inline void flush(){
    fwrite(obuf,1,ooh-obuf,stdout);
}

void judge(){
    freopen("dishash.in","r",stdin);
    freopen("dishash.out","w",stdout);
}

const int N=11000,mo=23333;
int n,m,ans,inv[N];

//inv[mo%i]*(mo-mo/i);

int main(){
    judge();
    read(n),read(m);
    if (m==1) return puts("1"),0;
    if (n<=4&&m<=3){
    	if (m==1) print('2'),print('\n');
    	if (m==2){
    		if (n==1) print('2'),print('\n');
    		if (n==2) print('3'),print('\n');
    		if (n==3) print('6'),print('\n');
    		if (n==4) print('1'),print('5'),print('\n');
    	}
    	if (m==3){
    		if (n==1) print('2'),print('\n');
    		if (n==2) print('4'),print('\n');
    		if (n==3) print('1'),print('3'),print('\n');
    		if (n==4) print('5'),print('6'),print('\n');
    	}
    	return flush(),0;
	}
	if (m==2){
		int t=1;
		inv[1]=1;
		rep(i,2,n+1) inv[i]=inv[mo%i]*(mo-mo/i)%mo;
		rep(i,1,2*n) t=t*1ll*i%mo;
		rep(i,1,n) t=1ll*t*inv[i]%mo;
		rep(i,1,n+1) t=t*1ll*inv[i]%mo;
		print(t+1),print('\n');
	}
    return flush(),0;
}
