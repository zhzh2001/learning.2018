#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>

#define rep(i,x,y) for (register int i=(x);i<=(y);i++)
#define drp(i,x,y) for (register int i=(x);i>=(y);i--)

inline char read(){
    static const int IN_LEN=1000000;
    static char buf[IN_LEN],*s,*t;
    return (s==t?t=(s=buf)+fread(buf,1,IN_LEN,stdin),(s==t?-1:*s++):*s++);
}
template<class T>
inline void read(T &x){
    static bool iosig;
    static char c;
    for (iosig=false,c=read();!isdigit(c);c=read()){
        if (c=='-') iosig=true;
        if (c==-1) return;
    }
    for (x=0;isdigit(c);c=read()) x=((x+(x<<2))<<1)+(c^'0');
    if (iosig) x=-x;
}
const int OUT_LEN=10000000;
char obuf[OUT_LEN],*ooh=obuf;
inline void print(char c){
    if (ooh==obuf+OUT_LEN) fwrite(obuf,1,OUT_LEN,stdout),ooh=obuf;
    *ooh++=c;
}
template<class T>
inline void print(T x){
    static int buf[30],cnt;
    if (x==0) print('0');
    else{
        if (x<0) print('-'),x=-x;
        for (cnt=0;x;x/=10) buf[++cnt]=x%10+48;
        while (cnt) print((char)buf[cnt--]);
    }
}
inline void flush(){
    fwrite(obuf,1,ooh-obuf,stdout);
}

void judge(){
    freopen("dislct.in","r",stdin);
    freopen("dislct.out","w",stdout);
}

const int N=3100,inf=1<<30;
int n,m,size[N],dis[N],dep[N],fa[N],C[N],sum[N],rt[N],min[N],cnt;
bool used[N],vis[N],flag;
struct info{
	int to,nxt;
}e[N<<1];
int head[N],opt;
void add(int x,int y){
	e[++opt]=(info){y,head[x]};
	head[x]=opt;
}

void dfs(int s,int u){
	C[u]=cnt;
	for (int i=head[u];i;i=e[i].nxt){
		int k=e[i].to;
		if (k==fa[u]||used[i]) continue;
		fa[k]=u; dis[k]=dis[u]+1;
		sum[s]+=dis[k];
		dfs(s,k);
	}
}

void dfs(int u){
	dep[u]=dep[fa[u]]+1;
	for (int i=head[u];i;i=e[i].nxt){
		int k=e[i].to;
		if (k==fa[u]||used[i]) continue;
		fa[k]=u; dfs(k);
	}
}

int main(){
    judge();
	read(n),read(m);
	rep(i,1,n-1){
		int x,y; read(x),read(y);
		add(x,y),add(y,x);
	}
	rep(s,1,m){
		int op,x;
		read(op),read(x);
		if (op==1) used[x*2-1]=used[x*2]=1;
		else{
			memset(vis,0,sizeof(vis));
			memset(sum,0,sizeof(sum));
			memset(C,0,sizeof(C));
			rep(i,1,n) min[i]=inf;
			cnt=0;
			rep(i,1,n){
				if (!C[i]) cnt++;
				memset(fa,0,sizeof(fa));
				sum[i]=0,dis[i]=0,dfs(i,i);
				if (sum[i]<min[cnt]) min[cnt]=sum[i],rt[cnt]=i;
			}
			memset(fa,0,sizeof(fa));
			memset(dep,0,sizeof(dep));
			rep(i,1,cnt) dfs(rt[i]);
			print(dep[x]-1),print('\n');
		}
	}
    return flush(),0;
}
