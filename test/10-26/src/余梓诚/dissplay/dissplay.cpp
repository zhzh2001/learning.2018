#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

#define rep(i,x,y) for (register int i=(x);i<=(y);i++)
#define drp(i,x,y) for (register int i=(x);i>=(y);i--)

void judge(){
    freopen("dissplay.in","r",stdin);
    freopen("dissplay.out","w",stdout);
}

inline void read(int &x){
	x=0; int fg=1; char ch=getchar();
	while (!isdigit(ch)){if (ch=='-') fg=-1;ch=getchar();}
	while (isdigit(ch)){x=x*10+ch-'0';ch=getchar();} x*=fg;
}

const int N=110000;
int sum[N],a[N],b[N],n,m,lim,L;
long long ans;

int change(int x){
	return x<=200?x:201;
}

int main(){
    judge();
    read(n),read(lim),read(L);
    if (n<=100000&&lim<=100000&&L<=200){
    	rep(i,1,n) read(a[i]),read(b[a[i]]);
    	read(m);
		rep(i,1,m){
    		string s; int x,y; cin>>s>>x;
    		if (s=="add") b[change(x)]++;
    		if (s=="break") b[change(x)]--;
    		if (s=="cut") cin>>y,b[change(x)]--,b[change(x-y)]++;
    		if (s=="expansion") cin>>y,b[change(x)]--,b[change(min(lim,change(x+y)))]++;
    		if (s=="query"){
    			ans=0;
    			rep(j,1,201) ans+=b[j]*(x-(x/j)*j);
    			printf("%lld\n",ans);
    		}
    	}
    	return 0;
    }
    if (n<=100000&&lim<=100000&&L<=100000){
    	rep(i,1,n) read(a[i]),read(b[a[i]]);
    	read(m);
		rep(i,1,m){
    		string s; int x,y; cin>>s>>x;
    		if (s=="add") b[x]++;
    		if (s=="break") b[x]--;
    		if (s=="cut") cin>>y,b[x]--,b[x-y]++;
    		if (s=="expansion") cin>>y,b[x]--,b[min(lim,x+y)]++;
    		if (s=="query"){
    			ans=0;
    			rep(j,1,lim) ans+=b[j]*(x-(x/j)*j);
    			printf("%lld\n",ans);
    		}
    	}
    }
    return 0;
}
