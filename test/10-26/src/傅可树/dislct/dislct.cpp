#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=3e5+5,INF=1e9;
struct edge{
	int link,next;
}e[N<<1];
struct Edge{
	int u,v;
}E[N];
int m,head[N],tot,n;
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
} 
inline void insert(int u,int v){
	add_edge(u,v); add_edge(v,u);
}
inline void init(){
	n=read(); m=read(); tot=1;
	for (int i=1;i<n;i++){
		int u=read(),v=read();
		insert(u,v); E[i]=(Edge){u,v};
	} 
}
int now,mark[N<<1],All,dep[N],sz[N],mn,g[N],root[N];
void getroot(int u,int fa){
	sz[u]=1; g[u]=0;
	for (int i=head[u];i;i=e[i].next){
		if (mark[i]) continue;
		int v=e[i].link;
		if (v!=fa){
			getroot(v,u);
			sz[u]+=sz[v]; g[u]=max(g[u],sz[v]);
		}
	}
	g[u]=max(g[u],All-sz[u]);
	if (g[u]<now||(g[u]==now&&mn>u)) mn=u,now=g[u];
}
void dfs(int u,int fa){
	All++;
	for (int i=head[u];i;i=e[i].next){
		if (!mark[i]){
			int v=e[i].link;
			if (v!=fa) {
				dfs(v,u);
			}
		}
	}
}
void Dfs(int u,int fa){
	root[u]=mn; dep[u]=dep[fa]+1;
	for (int i=head[u];i;i=e[i].next){
		if (!mark[i]){
			int v=e[i].link;
			if (v!=fa) {
				Dfs(v,u);
			}
		}
	}
}
inline void build(int x){
	All=0; mn=x; now=INF;
	dfs(x,0); 
	getroot(x,0); 
	dep[0]=-1; Dfs(mn,0);
}
inline void solve(){
	build(1);
	for (int i=1;i<=m;i++){
		int x=read(),y=read();
		if (x==1){
			mark[y<<1]=1; mark[y<<1|1]=1;
			build(E[y].u); build(E[y].v);
		}else{
			writeln(dep[y]);
		}
	}
}
int main(){
	freopen("dislct.in","r",stdin); freopen("dislct.out","w",stdout);
	init(); solve();
	return 0;
}
