#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
inline ll read(){ll x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(ll x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(ll x){write(x); puts("");}
const int N=1000005,mod=23333;
ll n,m,inv[N],fac[N],sum[N];
ll C(ll n,ll m) {
	return fac[n]*sum[m]%mod*sum[n-m]%mod;
}
inline void init(){
	n=read(); m=read(); 
}
inline void solve(){
	inv[1]=inv[0]=1; 
	fac[1]=fac[0]=1; 
	sum[1]=sum[0]=1;
	for (int i=2;i<=n*m;i++) {
		inv[i]=inv[mod%i]*(mod-mod/i)%mod;
	}
	for (int i=2;i<=n*m;i++){
		sum[i]=sum[i-1]*inv[i]%mod;
	}
	for (int i=2;i<=n*m;i++){
		fac[i]=fac[i-1]*i%mod;
	}
	writeln((C(n*m,n)*inv[(m-1)*n+1]%mod+1)%mod);
}
int main(){
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	init(); solve();
	return 0;
}
