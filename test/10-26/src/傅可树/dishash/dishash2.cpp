#include<cstdio>
#include<algorithm>
inline int read(){
	int x=0,c=getchar(),f=1;
	while (c<'0'||c>'9') {
		if (c=='-') f=-1; 
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0) x=-x,putchar('-');
	if (x>10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
using namespace std;
const int N=305,mod=23333;
int n,m;
inline void init(){
	n=read(); m=read();
}
int dp[N][N];
inline void solve(){
	dp[1][0]=1;
	for (int i=2;i<=n;i++){
		for (int j=1;j<=m;j++){
			for (int k=0;k<i;k++){
				dp[i][j]+=dp[i-k][j-1]*dp[k][m];
			}
		} 
	}
	writeln(dp[n][m]);
}
int main(){
	init(); solve();
	return 0;
}
