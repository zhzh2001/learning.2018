#pragma GCC optimize ("O2")
#include<cstdio>
#define int long long
#define rg register
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e5+5;
int cnt,n,lim,L,A[N];
inline void init(){
	n=read(); lim=read(); L=read();
	for (int i=1;i<=n;i++){
		int x=read(),y=read();
		A[x]=y; cnt+=y;
	}
}
int sum[N<<2];
inline void pushup(int k){
	sum[k]=sum[k<<1]+sum[k<<1|1];
}
void build(int k,int l,int r){
	if (l==r){
		sum[k]=l*A[l];
		return;
	}
	int mid=(l+r)>>1; build(k<<1,l,mid); build(k<<1|1,mid+1,r); pushup(k);
}
void update(int k,int l,int r,int x,int v){
	if (l==r) {
		sum[k]+=v;
		return;
	}
	int mid=(l+r)>>1;
	if (mid>=x) update(k<<1,l,mid,x,v);
		else update(k<<1|1,mid+1,r,x,v);
	pushup(k);
}
int query(int k,int l,int r,int x,int y){
	if (l==x&&r==y){
		return sum[k];
	}
	int mid=(l+r)>>1;
	if (mid>=y) return query(k<<1,l,mid,x,y);
		else if (mid<x) return query(k<<1|1,mid+1,r,x,y);
			else return query(k<<1,l,mid,x,mid)+query(k<<1|1,mid+1,r,mid+1,y);
}
char s[20];
int ans;
inline int find(int x,int tmp,int y){
	int l=1,r=y;
	while (l<r){
		int mid=(l+r)>>1,res=x/mid;
		if (res==tmp) r=mid;
			else l=mid+1;
	}
	return l;
}
inline void solve(){
	build(1,1,lim);
	int q=read();
	while (q--){
		int x,y;
		scanf("%s",s+1);
		if (s[1]=='a') x=read(),update(1,1,lim,x,x),cnt++,A[x]++;
		if (s[1]=='b') x=read(),update(1,1,lim,x,-x),cnt--,A[x]--;
		if (s[1]=='c') x=read(),y=read(),update(1,1,lim,x,-x),update(1,1,lim,x-y,x-y),A[x]--,A[x+y]++;
		if (s[1]=='e') x=read(),y=read(),update(1,1,lim,x,-x),update(1,1,lim,x+y,x+y),A[x]--,A[x+y]++;
		if (s[1]=='q') {
			x=read(); ans=0; int lolo,roro=x;
			for (;roro;roro=lolo-1){
				int tmp=x/roro;
				lolo=find(x,tmp,roro);
				ans+=tmp*query(1,1,lim,lolo,roro);
				if (lolo<15) break;
			}
			for (int j=1;j<lolo;j++) ans+=x/j*A[j]*j;
			ans=cnt*x-ans;
			writeln(ans);
		}
	}
}
signed main(){
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	init(); solve();
	return 0;
}
