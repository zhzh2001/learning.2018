#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
using namespace std;
inline char gc(){
    static char buf[1<<18],*p1=buf,*p2=buf;
    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<18,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
inline int gettp(){
	char ch=gc();
	for (;ch<'a'||ch>'z';ch=gc());
	if (ch=='a') return 0;
	if (ch=='b') return 1;
	if (ch=='c') return 2;
	if (ch=='e') return 3;
	if (ch=='q') return 4;
}
#define ll long long
const int BLK=320;
const int N=111111;
int n,MX;
ll s[N],ss[BLK+2],sum;
int cnt[N];
void add(int x,int v){
	sum+=v;
	int be=(x-1)/BLK+1;
	For(i,x,be*BLK) s[i]+=1ll*v*x;
	For(i,be,BLK) ss[i]+=1ll*v*x;
}
ll S(int x){
	return s[x]+ss[(x-1)/BLK];
}
inline void writeln(ll x){
	if (!x){
		puts("0");
		return;
	}
	static int a[20],top;
	for (top=0;x;a[++top]=x%10,x/=10);
	for (;top;putchar(a[top--]+'0'));
	puts("");
}
void solve(int v){
	ll ans=sum*v,las=0;
	for (int l=1,r;l<=v;l=r+1){
		r=v/(v/l);
		ll now=S(r);
		ans-=(v/l)*(now-las);
		las=now;
	}
	writeln(ans);
}
int main(){
	freopen("dissplay.in","r",stdin);
	freopen("std.out","w",stdout);
	n=read(); read(); read();
	For(i,1,n){
		int x=read();
		add(x,read());
	}
	n=read();
	while (n--){
		int tp=gettp(),x;
		if (tp==0||tp==1) add(read(),tp?-1:1);
		if (tp==2||tp==3){
			add(x=read(),-1);
			add(x+=(tp==2?-read():read()),1);
		}
		if (tp==4) solve(read());
	}
}
/*
5 10 10
4 6
1 4
2 6
7 6
10 10
10
add 1
cut 4 2
cut 10 3
query 3
break 2
expansion 1 4
add 6
cut 7 0
query 5
query 4
*/
