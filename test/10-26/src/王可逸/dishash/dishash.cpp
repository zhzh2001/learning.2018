#include <bits/stdc++.h>
#define int long long
using namespace std;
const int mod=23333;
int n,m,ans,c[120],ans1[12][12];
void subtask1() {
	ans1[1][1]=1;
	ans1[2][1]=1;
	ans1[3][1]=1;
	ans1[4][1]=1;
	ans1[1][2]=1;
	ans1[2][2]=2;
	ans1[3][2]=5;
	ans1[4][2]=14;
	ans1[1][3]=1;
	ans1[2][3]=3;
	ans1[3][3]=12;
	cout<<(ans1[n][m]+1)%mod;
}
void subtask2() {
	c[0]=c[1]=c[2]=1;
	if(m==3) {
		c[3]=m;
		for(int i=4;i<=n+1;i++) {
			for(int x=1;x<=i-1;x++)
				for(int y=1;y<=i-1;y++)
					for(int z=1;z<=i-1;z++)
						if(x+y+z==i+1)
							c[i]=(c[i]+c[x]*c[y]%mod*c[z]%mod)%mod;	
		}
		cout<<(c[n+1]+1)%mod;
	}
	
	if(m==4) {
		c[3]=m;
		for(int i=4;i<=n+2;i++) {
			for(int a1=1;a1<=i-3;a1++)
				for(int a2=1;a2<=i-3;a2++)
					for(int a3=1;a3<=i-3;a3++)
						for(int a4=1;a4<=i-3;a4++)
							if(a1+a2+a3+a4==i+1) {
//								cout<<a1<<" "<<a2<<" "<<a3<<" "<<a4<<" "<<c[a1]*c[a2]*c[a3]*c[a4]<<'\n';
								c[i]=(c[i]+c[a1]*c[a2]%mod*c[a3]%mod*c[a4]%mod)%mod;	
							}
		}
		cout<<(c[n+2]+1)%mod;
	}
	if(m==5) {
		c[3]=m;
		for(int i=5;i<=n+3;i++) {
			for(int a1=1;a1<=i-4;a1++)
				for(int a2=1;a2<=i-4;a2++)
					for(int a3=1;a3<=i-4;a3++)
						for(int a4=1;a4<=i-4;a4++)
							for(int a5=1;a5<=i-4;a5++)
								if(a1+a2+a3+a4+a5==i+1)
									c[i]=(c[i]+c[a1]*c[a2]%mod*c[a3]%mod*c[a4]%mod*c[a5]%mod)%mod;		
		}
		cout<<(c[n+3]+1)%mod;
	}
	if(m==6) {
		c[3]=m;
		for(int i=6;i<=n+4;i++) {
			for(int a1=1;a1<=i-5;a1++)
				for(int a2=1;a2<=i-5;a2++)
					for(int a3=1;a3<=i-5;a3++)
						for(int a4=1;a4<=i-5;a4++)
							for(int a5=1;a5<=i-5;a5++)
								for(int a6=1;a6<=i-5;a6++)
									if(a1+a2+a3+a4+a5+a6==i+1)
										c[i]=(c[i]+c[a1]*c[a2]%mod*c[a3]%mod*c[a4]%mod*c[a5]%mod*c[a6]%mod)%mod;		
		}
		cout<<(c[n+4]+1)%mod;
	}
	if(m==7) {
		c[3]=m;
		for(int i=7;i<=n+5;i++) {
			for(int a1=1;a1<=i-6;a1++)
				for(int a2=1;a2<=i-6;a2++)
					for(int a3=1;a3<=i-6;a3++)
						for(int a4=1;a4<=i-6;a4++)
							for(int a5=1;a5<=i-6;a5++)
								for(int a6=1;a6<=i-6;a6++)
									for(int a7=1;a7<=i-6;a7++)
										if(a1+a2+a3+a4+a5+a6+a7==i+1)
											c[i]=(c[i]+c[a1]*c[a2]%mod*c[a3]%mod*c[a4]%mod*c[a5]%mod*c[a6]%mod*c[a7]%mod)%mod;		
		}
		cout<<(c[n+5]+1)%mod;
	}
	if(m==8) {
		c[3]=m;
		for(int i=8;i<=n+6;i++) {
			for(int a1=1;a1<=i-7;a1++)
				for(int a2=1;a2<=i-7;a2++)
					for(int a3=1;a3<=i-7;a3++)
						for(int a4=1;a4<=i-7;a4++)
							for(int a5=1;a5<=i-7;a5++)
								for(int a6=1;a6<=i-7;a6++)
									for(int a7=1;a7<=i-7;a7++)
										for(int a8=1;a8<=i-7;a8++)
											if(a1+a2+a3+a4+a5+a6+a7+a8==i+1)
												c[i]=(c[i]+c[a1]*c[a2]%mod*c[a3]%mod*c[a4]%mod*c[a5]%mod*c[a6]%mod*c[a7]%mod*c[a8]%mod)%mod;		
		}
		cout<<(c[n+6]+1)%mod;
	}
	if(m==9) {
		c[3]=m;
		for(int i=9;i<=n+7;i++) {
			for(int a1=1;a1<=i-8;a1++)
				for(int a2=1;a2<=i-8;a2++)
					for(int a3=1;a3<=i-8;a3++)
						for(int a4=1;a4<=i-8;a4++)
							for(int a5=1;a5<=i-8;a5++)
								for(int a6=1;a6<=i-8;a6++)
									for(int a7=1;a7<=i-8;a7++)
										for(int a8=1;a8<=i-8;a8++)
											for(int a9=1;a9<=i-8;a9++)
												if(a1+a2+a3+a4+a5+a6+a7+a8+a9==i+1)
													c[i]=(c[i]+c[a1]*c[a2]%mod*c[a3]%mod*c[a4]%mod*c[a5]%mod*c[a6]%mod*c[a7]%mod*c[a8]%mod*c[a9]%mod)%mod;		
		}
		cout<<(c[n+7]+1)%mod;
	}
	if(m==10) {
		c[3]=m;
		for(int i=10;i<=n+8;i++) {
			for(int a1=1;a1<=i-9;a1++)
				for(int a2=1;a2<=i-9;a2++)
					for(int a3=1;a3<=i-9;a3++)
						for(int a4=1;a4<=i-9;a4++)
							for(int a5=1;a5<=i-9;a5++)
								for(int a6=1;a6<=i-9;a6++)
									for(int a7=1;a7<=i-9;a7++)
										for(int a8=1;a8<=i-9;a8++)
											for(int a9=1;a9<=i-9;a9++)
												for(int a10=1;a10<=i-9;a10++) {
													if(a1+a2+a3+a4+a5+a6+a7+a8+a9+a10==i+1)
														c[i]=(c[i]+c[a1]*c[a2]%mod*c[a3]%mod*c[a4]%mod*c[a5]%mod*c[a6]%mod*c[a7]%mod*c[a8]%mod*c[a9]%mod*c[a10]%mod)%mod;	
													}
		}
		cout<<(c[n+8]+1)%mod;
	}
	
}
void subtask3() {
	c[1]=c[2]=1;
	for(int i=3;i<=n+2;i++) {
		for(int j=2;j<=i-1;j++)
			c[i]=(c[i]+c[j]*c[i+1-j])%mod;
	}
	cout<<(c[n+2]+1)%mod;
}
void subtask4() {
	
}
void subtask5() {
	
}
void subtask6() {
	
}
void subtask7() {
	
}
signed main() {
	freopen("dishash.in","r",stdin); freopen("dishash.out","w",stdout);
	cin>>n>>m;
	if(m==2) subtask3();
	else if(n<=4 && m<=3) subtask1();
	else if(n<=7 && m<=10) subtask2();
	else puts("2333");
	return 0;
}
