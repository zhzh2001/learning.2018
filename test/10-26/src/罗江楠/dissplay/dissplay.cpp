#include <bits/stdc++.h>

#define N 100020

using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int n, lim, L;
long long a[N], ans[100000];

struct __szsz {
  long long b[N];
  inline void update(int x, long long v) {
    for (; x <= lim; x += x & -x) {
      b[x] += v;
    }
  }
  inline long long query(int x) {
    long long ans = 0;
    for (; x; x ^= x & -x) {
      ans += b[x];
    }
    return ans;
  }
} sa;

inline void remove(int x) {
  // for (int i = 1; i < 100000; ++ i) {
  //   ans[i] -= (i % x);
  // }
  -- a[x];
  sa.update(x, -1);
  // sb.update(x, -x);
}
inline void add(int x) {
  // for (int i = 1; i < 100000; ++ i) {
  //   ans[i] += (i % x);
  // }
  ++ a[x];
  sa.update(x, 1);
  // sb.update(x, x);
}

inline long long baoli(int x) {
  // printf("? %lld\n", sb.query(x - 1));
  // printf("? %lld\n", (sa.query(lim) - sa.query(x)) * x);
  // return sb.query(x - 1) + (sa.query(lim) - sa.query(x)) * x;
  long long res = (sa.query(lim) - sa.query(x)) * x;
  for (int i = 1; i < x; ++ i) {
    res += a[i] * (x % i);
  }
  return res;
}

char str[20];

int main(int argc, char const *argv[]) {
  
  freopen("dissplay.in", "r", stdin);
  freopen("dissplay.out", "w", stdout);

  n = read(); lim = read(); L = read();

  for (int i = 1; i <= n; ++ i) {
    int x = read(), y = read();
    a[x] += y;
  }

  // for (int i = 1; i < 100000; ++ i) {
  //   for (int j = 1; j <= lim; ++ j) {
  //     ans[i] += a[j] * (i % j);
  //   }
  // }

  for (int i = 1; i <= lim; ++ i) {
    sa.update(i, a[i]);
    // sb.update(i, a[i] * i);
  }

  int q = read();
  while (q --) {
    scanf("%s", str);
    if (str[0] == 'a') {
      add(read());
    }
    if (str[0] == 'b') {
      remove(read());
    }
    if (str[0] == 'c') {
      int x = read(), y = read();
      remove(x);
      add(x - y);
    }
    if (str[0] == 'e') {
      int x = read(), y = read();
      remove(x);
      add(x + y);
    }
    if (str[0] == 'q') {
      int x = read();
      // if (x <= 100000) {
        // printf("%lld\n", ans[x]);
      //   printf("- %lld\n", baoli(x));
      // } else {
        printf("%lld\n", baoli(x));
      // }
    }
  }

  return 0;
}