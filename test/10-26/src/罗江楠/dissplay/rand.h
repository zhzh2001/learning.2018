#include <cstdlib>
#include <ctime>

class __pre
{
	public:
		__pre()
		{
			srand((unsigned) time(NULL));
		}
} __p;

inline double rand_d()
{
	return double(rand()) / RAND_MAX;
}

inline int rand(int r)
{
	return int(double(r) * rand_d());
}

inline long long rand(long long r)
{
	return (long long)(double(r) * rand_d());
}

inline int rand(int l, int r)
{
	return rand(r - l) + l;
}

inline long long rand(long long l, long long r)
{
	return rand(r - l) + l;
}
