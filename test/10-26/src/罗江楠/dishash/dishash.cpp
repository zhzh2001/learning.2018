#include <bits/stdc++.h>

#define N 1000020
#define mod 23333

using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

inline long long fast_pow(long long x, long long y) {
  long long z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}

inline long long inv(long long x) {
  return fast_pow(x, mod - 2);
}

long long frac[N], invs[N];

inline long long C(int n, int m) {
  return frac[n] * invs[frac[m]] % mod * invs[frac[n - m]] % mod;
}

int ans[N], n, m;
int _tmp[1020][1020];
long long dfs(int x, int rem) {
  if (~_tmp[x][rem]) return _tmp[x][rem];
  if (x == m) {
    return ans[rem];
  }
  if (!rem) return 1;
  long long total = 0;
  for (int i = 0; i <= rem; ++ i) {
    total = (total + ans[i] * dfs(x + 1, rem - i) % mod) % mod;
    // printf("%d %d\n", i, ans[i]);
  }
  return _tmp[x][rem] = total;
}

int main(int argc, char const *argv[]) {

  freopen("dishash.in", "r", stdin);
  freopen("dishash.out", "w+", stdout);

  n = read(); m = read();

  if (m == 2) {
    for (long long i = frac[0] = 1; i <= n * 2; ++ i) {
      frac[i] = frac[i - 1] * i % mod;
    }
    for (long long i = 1; i < mod; ++ i) {
      invs[i] = inv(i);
    }
    printf("%d\n", (C(2 * n, n) * inv(n + 1) % mod + 1) % mod);
  } else {
    memset(_tmp, -1, sizeof _tmp);
    ans[0] = 1;
    for (int i = 1; i <= n; ++ i) {
      ans[i] = dfs(1, i - 1);
    }
    printf("%d\n", (ans[n] + 1) % mod);
  }

  return 0;
}