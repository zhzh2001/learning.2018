#include <bits/stdc++.h>

#define N 300020

using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

struct __node {
  int x, y;
} es[N], qs[N];
bool mp[N];

int fa[N], szz[N];
int find(int x) {
  return fa[x] == x ? x : fa[x] = find(fa[x]);
}
inline void merge(int x, int y) {
  int fx = find(x), fy = find(y);
  fa[fx] = fy;
  szz[fy] += szz[fx];
}

int to[N<<1], nxt[N<<1], cnt, head[N];
void insert(int x, int y) {
  // printf("link %d %d\n", x, y);
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
  to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}

int sz[N], res[N];

void dfs(int x, int f) {
  sz[x] = 1; res[x] = 0;
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    dfs(to[i], x);
    sz[x] += sz[to[i]];
    res[x] += res[to[i]];
  }
  res[x] += sz[x] - 1;
}
int value, point, max_size;
inline void checkWeight(int x, int v) {
  // printf("value %d if %d\n", x, v);
  if (v < value) {
    point = x;
    value = v;
  } else if (v == value) {
    point = min(point, x);
  }
}
void dfs2(int x, int f, int topres) {
  checkWeight(x, topres + res[x]);
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    dfs2(to[i], x, topres + (res[x] - res[to[i]] - sz[to[i]]) + (max_size - sz[to[i]]));
  }
}
int findWeight(int x) {
  dfs(x, 0);
  value = 1 << 30;
  point = 1 << 30;
  max_size = szz[x];
  dfs2(x, 0, 0);
  return point;
}


int _to[N<<1], _nxt[N<<1], _cnt, _head[N], dep[N], _f[N][20];
void _insert(int x, int y) {
  _to[++ _cnt] = y; _nxt[_cnt] = _head[x]; _head[x] = _cnt;
  _to[++ _cnt] = x; _nxt[_cnt] = _head[y]; _head[y] = _cnt;
}
void _dfs(int x, int f) {
  _f[x][0] = f;
  dep[x] = dep[f] + 1;
  for (int i = _head[x]; i; i = _nxt[i]) {
    if (_to[i] == f) continue;
    _dfs(_to[i], x);
  }
}
int lca(int x, int y) {
  if (dep[x] < dep[y]) swap(x, y);
  int t = dep[x] - dep[y];
  for (int i = 19; ~i; -- i) {
    if (t >> i & 1) {
      x = _f[x][i];
    }
  }
  for (int i = 19; ~i; -- i) {
    if (_f[x][i] != _f[y][i]) {
      x = _f[x][i];
      y = _f[y][i];
    }
  }
  return x == y ? x : _f[x][0];
}

int weight[N], ans[N];

int length(int x, int y) {
  // printf("length: %d, %d\n", x, y);
  int z = lca(x, y);
  return dep[x] - dep[z] + dep[y] - dep[z];
}

int main(int argc, char const *argv[]) {

  freopen("dislct.in", "r", stdin);
  freopen("dislct.out", "w", stdout);

  int n = read(), m = read();
  for (int i = 1; i < n; ++ i) {
    es[i].x = read();
    es[i].y = read();
    _insert(es[i].x, es[i].y);
  }
  _dfs(1, 0);
  for (int i = 1; i < 20; ++ i) {
    for (int j = 1; j <= n; ++ j) {
      _f[j][i] = _f[_f[j][i - 1]][i - 1];
    }
  }

  // puts("???");

  for (int i = 1; i <= m; ++ i) {
    qs[i].x = read();
    qs[i].y = read();
    if (qs[i].x == 1) {
      mp[qs[i].y] = 1;
    }
  }

  for (int i = 1; i <= n; ++ i) {
    fa[i] = i;
    szz[i] = 1;
  }
  for (int i = 1; i < n; ++ i) {
    if (mp[i]) continue;
    insert(es[i].x, es[i].y);
    merge(es[i].x, es[i].y);
  }
  for (int i = 1; i <= n; ++ i) {
    if (fa[i] == i) {
      weight[i] = findWeight(i);
      // printf("weight[%d] = %d\n", i, weight[i]);
    }
  }

  for (int i = m; i; -- i) {
    if (qs[i].x == 1) {
      insert(es[qs[i].y].x, es[qs[i].y].y);
      merge(es[qs[i].y].x, es[qs[i].y].y);
      int fx = find(es[qs[i].y].x);
      weight[fx] = findWeight(fx);
    } else {
      ans[i] = length(qs[i].y, weight[find(qs[i].y)]);
    }
  }

  // printf("weight = %d\n", weight[find(1)]);
  // printf("max_size = %d\n", max_size);
  // for (int i = 1; i <= n; ++ i) {
  //   if (fa[i] == i) puts("LTK");
  // }
  for (int i = 1; i <= m; ++ i) {
    if (qs[i].x == 2) {
      printf("%d\n", ans[i]);
    }
  }
  
  return 0;
}

