#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '=')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int n,lim,L,m;
int a[N];

signed main(){
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n = read(); lim = read(); L = read();
	for(int i = 1;i <= n;++i){
		int x = read(),y = read();
		a[x] = y;
	}
	m = read();
	while(m--){
		char opt[20]; scanf("%s",opt+1);
		if(opt[1] == 'a'){
			int x = read();
			if(x > lim) x = lim;
			a[x]++;
		}else if(opt[1] == 'b'){
			int x = read(); a[x]--;
		}else if(opt[1] == 'c'){
			int x = read(),y = read();
			a[x]--; a[x-y]++;
		}else if(opt[1] == 'e'){
			int x = read(),y = read();
			if(x+y > lim) y = lim-x;
			a[x]--; a[x+y]++;
		}else{
			int x = read(); ll ans = 0;
			for(int i = 1;i <= lim;++i)
				ans += 1LL*(x%i)*a[i];
			printf("%lld\n",ans);
		}
	}
	return 0;
}
