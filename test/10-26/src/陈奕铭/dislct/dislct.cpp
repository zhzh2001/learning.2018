#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '=')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 300005;
int n,m;
int f[20][N];
int head[N],nxt[N*2],to[N*2],cnt = -1;
int dep[N];
int rt[N];
ll val[N],size[N],topf[N],Mi;
int Rt,Size;
bool vis[N*2];
int Bin[20];
struct node{
	int opt,x;
}op[N];
int fi[N];
int ans[N];
int road[N],Cnt;
bool Vis[N];
ll Val[N],ssize[N];

inline void insert(int x,int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}

void dfs(int x,int fa){
	for(int i = head[x];i != -1;i = nxt[i])
		if(to[i] != fa){
			f[0][to[i]] = x;
			dep[to[i]] = dep[x]+1;
			dfs(to[i],x);
		}
}

void dfs1(int x,int fa){
	val[x] = 0; size[x] = 1;
	fi[x] = Rt;
	for(int i = head[x];i != -1;i = nxt[i])
		if(!vis[i] && to[i] != fa){
			dfs1(to[i],x);
			size[x] += size[to[i]];
			val[x] += val[to[i]]+size[to[i]];
		}
}

int getf(int x){
	if(x == fi[x]) return x;
	return fi[x] = getf(fi[x]);
}

void dfs2(int x,int fa){
	Vis[x] = true;
	if(topf[x]+val[x] < Mi){
		Mi = topf[x]+val[x];
		Rt = x;
	}else if(topf[x]+val[x] == Mi && Rt > x) Rt = x;
	for(int i = head[x];i != -1;i = nxt[i])
		if(!vis[i] && to[i] != fa){
			int y = to[i];
			topf[y] = topf[x]+Size-size[y]*2+val[x]-val[y];
			dfs2(y,x);
		}
}

inline int getlca(int x,int y){
	if(dep[x] < dep[y]) swap(x,y);
	for(int j = 19;j >= 0;--j)
		if(dep[x]-dep[y] >= Bin[j])
			x = f[j][x];
	if(x == y) return x;
	for(int j = 19;j >= 0;--j)
		if(f[j][x] != f[j][y]){
			x = f[j][x];
			y = f[j][y];
		}
	return f[0][x];
}

signed main(){
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n = read(); m = read();
	memset(head,-1,sizeof head);
	for(int i = 1;i < n;++i){
		int x = read(),y = read();
		insert(x,y); insert(y,x);
	}
	for(int i = 1;i <= m;++i){
		op[i].opt = read(); op[i].x = read();
		if(op[i].opt == 1){
			int p = op[i].x*2-1;
			vis[p^1] = vis[p] = true;
		}
	}
	f[0][1] = 1;
	dfs(1,1);
	Bin[0] = 1; for(int i = 1;i <= 19;++i) Bin[i] = Bin[i-1]<<1;
	for(int j = 1;j <= 19;++j)
		for(int i = 1;i <= n;++i)
			f[j][i] = f[j-1][f[j-1][i]];
	for(int i = 1;i <= n;++i) fi[i] = i;
	for(int i = 1;i <= n;++i){
		if(!Vis[i]){
			Mi = 1e17;
			Rt = i;
			dfs1(i,i);
			Size = size[i];
			Rt = 0;
			dfs2(i,i);
			dfs1(Rt,Rt);
//			rt[Rt] = Rt;
		}
	}
//	printf("$$ %d %d\n",1,fi[1]);
//	printf("## %d %d\n",f[0][3],dep[3]);
	for(int i = m;i >= 1;--i){
		if(op[i].opt == 2){
			int x = op[i].x;
			int Lca = getlca(x,getf(x));
			ans[i] = dep[x]+dep[fi[x]]-dep[Lca]*2;
//			printf("@@ %d %d\n",x,fi[x]);
		}else{
			int x = op[i].x*2-1; 
			int a = to[x],b = to[x^1];
			a = getf(a); b = getf(b);
			int Lca = getlca(a,b);
			Cnt = 0;
			x = a; while(x != Lca) road[++Cnt] = x,x = f[0][x];
			road[++Cnt] = x;
			Cnt += dep[b]-dep[Lca];
			x = b; int kkkk = Cnt;
			while(x != Lca) road[kkkk--] = x,x = f[0][x];
			ll tot = 0;
			for(int j = 1;j <= Cnt;++j){
				if(getf(road[j]) == fi[a]){	//��ߵ��ǿ���
					if(j + 1 <= Cnt && getf(road[j+1]) == fi[a]){ //�ж��� 
//						Val[j] = val[road[j]]-val[road[j+1]]-size[road[j+1]];
						ssize[j] = size[road[j]] - size[road[j+1]];
					}else{
//						Val[j] = val[road[j]];
						ssize[j] = size[road[j]];
					}
				}else{
					if(j-1 >= 1 && getf(road[j-1]) == fi[b]){
//						Val[j] = val[road[j]]-val[road[j-1]]-size[road[j-1]];
						ssize[j] = size[road[j]] - size[road[j-1]];
					}
					else{
//						Val[j] = val[road[j]];
						ssize[j] = size[road[j]];
					}
				}
				tot += ssize[j];
			}
			int p = 1; ll pre = 0;
			for(;p < Cnt;++p){
				pre += ssize[p];
				if(tot-pre <= pre){
					if(tot-pre == pre && road[p+1] < road[p]) ++p;
					break;
				}
			}
//			val[road[1]] = Val[1];
			size[road[1]] = ssize[1];
			for(int j = 2;j < p;++j){
//				val[road[j]] = Val[j]+val[road[j-1]]+size[road[j-1]];
				size[road[j]] = ssize[j]+size[road[j-1]];
			}
//			val[road[Cnt]] = Val[Cnt];
			size[road[Cnt]] = ssize[Cnt];			
			for(int j = Cnt-1;j > p;--j){
//				val[road[j]] = Val[j]+val[road[j+1]]+size[road[j+1]];
				size[road[j]] = ssize[j]+size[road[j+1]];
			}
//			val[road[p]] = Val[p];
			size[road[p]] = ssize[p];
			if(p > 1){
//				val[road[p]] += val[road[p-1]]+size[road[p-1]];
				size[road[p]] += size[road[p-1]];
			}
			if(p < Cnt){
//				val[road[p]] += val[road[p+1]]+size[road[p+1]];
				size[road[p]] += size[road[p+1]];
			}
			fi[road[p]] = road[p];
			fi[getf(a)] = road[p];
			fi[getf(b)] = road[p];
		}
	}
	for(int i = 1;i <= m;++i)
		if(op[i].opt == 2) printf("%d\n",ans[i]);
	return 0;
}
