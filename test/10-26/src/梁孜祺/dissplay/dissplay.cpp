#include <bits/stdc++.h>
#define ll long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read(){
	GG k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(GG x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(GG x){ write(x);puts("");}
int n, lim, L;
set <pair<int, int> > s;
ll ans;
#define N 100005
int a[N];
int main() {
  freopen("dissplay.in","r",stdin);
  freopen("dissplay.out","w",stdout);
  n = read(); lim = read(); L = read();
  for (int i = 1; i <= n; i++) {
    int x = read(), y = read();
    a[x] = y;
  }
  int Q = read();
  while(Q--) {
    char s[10];
    scanf("%s",s);
    int x = read(), y;
    if (s[0] == 'a') a[x]++;
    if (s[0] == 'b') a[x]--;
    if (s[0] == 'c') {
      y = read();
      a[x]--;
      a[x-y]++;
    }
    if (s[0] == 'e') {
      y = read();
      a[x]--;
      a[x+y]++;
    }
    if (s[0] == 'q') {
      ll ans = 0;
      for (int i = 1; i <= lim; i++)
        ans = ans+1ll*i%x*a[i];
      printf("%lld\n", ans);
    }
  }
  return 0;
}
