#include<bits/stdc++.h>
#define ll long long
#define GG long long
#define For(i, j, k) for(ll i=j; i<=k; i++)
#define Dow(i, j, k) for(ll i=j; i>=k; i--)
using namespace std;
inline GG read() {
  GG x = 0, f = 1;
  char ch = getchar();
  while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
  while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
  return x * f;
}
inline void write(GG x) {
  if(x<0) putchar('-'), x = -x;
  if(x>9) write(x/10);
  putchar(x%10+48);
}
inline void writeln(GG x) {
  write(x); puts("");
}
const ll mo = 23333; 
ll n, m; 
ll ans;
ll inv[100000];   
inline void solve() {
	ans = 1; ll tmp = n*m; 
	For (i, n*m-n+2, tmp) ans = ans*i % mo; 
	inv[1] = inv[0] = 1;
	For(i, 2, n) inv[i] = inv[mo%i]*(mo-mo/i) % mo; 
	For(i, 1, n) ans = ans*inv[i] %mo; 
	ans = (ans+1) %mo; 
	writeln(ans); 
}
int main() {
	freopen("dishash.in", "r", stdin); 
	freopen("dishash.out", "w", stdout);
	n = read(); m = read();
	solve();
  return 0;
}
