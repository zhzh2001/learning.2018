#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 30000
#define mod 23333
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,inv[N],fac[N],sum[N];
ll C(ll n,ll m){ 
	if (n<mod&&m<mod) return fac[n]*sum[m]%mod*sum[n-m]%mod;
	else return C(n/mod,m/mod)*C(n%mod,m%mod)%mod;
}
ll qpow(ll x,ll y){
	ll num=1;
	for (;y;y>>=1,x=x*x%mod) if (y&1) num=num*x%mod;
	return num;
}
int main(){
	n=read(); m=read();
	rep(i,0,1) inv[i]=sum[i]=fac[i]=1;
	rep(i,2,mod) inv[i]=inv[mod%i]*(mod-mod/i)%mod;
	rep(i,2,mod) sum[i]=sum[i-1]*inv[i]%mod;
	rep(i,2,mod) fac[i]=fac[i-1]*i%mod;
	printf("%lld",C(n*m,n)*qpow((m-1)*n+1,mod-2)%mod+1);
//	C(nm,n)/((m-1)*n+1)
//	C(n,m)%p=C(n/p,m/p)*C(n%p,m%p)
}
