#include<cstdio>
#include<algorithm>
#define ll int
#define N 300005
#define INF (1e9)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
struct edge{ ll link,next; }e[N<<1];
struct Edge{ ll u,v; }E[N];
ll m,head[N],tot,n;
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void init(){
	n=read(); m=read(); tot=1;
	for (ll i=1;i<n;i++){
		ll u=read(),v=read();
		add(u,v); add(v,u); E[i]=(Edge){u,v};
	} 
}
ll now,mark[N<<1],All,dep[N],sz[N],mn,g[N],root[N];
void getroot(ll u,ll fa){
	sz[u]=1; g[u]=0;
	for (ll i=head[u];i;i=e[i].next){
		if (mark[i]) continue;
		ll v=e[i].link;
		if (v!=fa){
			getroot(v,u);
			sz[u]+=sz[v]; g[u]=max(g[u],sz[v]);
		}
	}
	g[u]=max(g[u],All-sz[u]);
	if (g[u]<now||(g[u]==now&&mn>u)) mn=u,now=g[u];
}
void dfs(ll u,ll fa){
	All++;
	for (ll i=head[u];i;i=e[i].next){
		if (!mark[i]){
			ll v=e[i].link;
			if (v!=fa) dfs(v,u);
		}
	}
}
void Dfs(ll u,ll fa){
	root[u]=mn; dep[u]=dep[fa]+1;
	for (ll i=head[u];i;i=e[i].next){
		if (!mark[i]){
			ll v=e[i].link;
			if (v!=fa) Dfs(v,u);
		}
	}
}
void build(ll x){
	All=0; mn=x; now=INF;
	dfs(x,0); getroot(x,0); 
	dep[0]=-1; Dfs(mn,0);
}
void solve(){
	build(1);
	rep(i,1,m){
		ll x=read(),y=read();
		if (x==1){
			mark[y<<1]=1; mark[y<<1|1]=1;
			build(E[y].u); build(E[y].v);
		}else printf("%d\n",dep[y]);
	}
}
int main(){
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	init(); solve();
}
