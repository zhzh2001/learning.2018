#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll cnt,n,lim,L,a[N],sum[N<<2],ans; char s[20];
void build(ll l,ll r,ll p){
	if (l==r) { sum[p]=l*a[l]; return; }
	ll mid=l+r>>1;
	build(l,mid,p<<1); build(mid+1,r,p<<1|1);
	sum[p]=sum[p<<1]+sum[p<<1|1];
}
void modfiy(ll l,ll r,ll pos,ll v,ll p){
	if (l==r) { sum[p]+=v; return; }
	ll mid=(l+r)>>1;
	if (mid>=pos) modfiy(l,mid,pos,v,p<<1);
	else modfiy(mid+1,r,pos,v,p<<1|1);
	sum[p]=sum[p<<1]+sum[p<<1|1];
}
ll query(ll l,ll r,ll s,ll t,ll p){
	if (l==s&&r==t) return sum[p];
	ll mid=(l+r)>>1;
	if (t<=mid) return query(l,mid,s,t,p<<1);
	else if (s>mid) return query(mid+1,r,s,t,p<<1|1);
	else return query(l,mid,s,mid,p<<1)+query(mid+1,r,mid+1,t,p<<1|1);
}
inline ll find(ll x,ll tmp,ll y){
	ll l=1,r=y;
	while (l<r){
		ll mid=(l+r)>>1,res=x/mid;
		if (res==tmp) r=mid; else l=mid+1;
	}
	return l;
}
int main(){
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n=read(); lim=read(); L=read();
	rep(i,1,n){
		ll x=read(),y=read();
		a[x]=y; cnt+=y;
	}
	build(1,lim,1);
	ll q=read();
	while (q--){
		scanf("%s",s); ll x,y;
		if (s[0]=='a'){
			x=read(); ++cnt;
			modfiy(1,lim,x,x,1);
		}else
		if (s[0]=='b'){
			x=read(); --cnt;
			modfiy(1,lim,x,-x,1);
		}else
		if (s[0]=='c'){
			x=read(); modfiy(1,lim,x,-x,1);
			y=read(); modfiy(1,lim,x-y,x-y,1);
		}else
		if (s[0]=='e'){
			x=read(); modfiy(1,lim,x,-x,1);
			y=read(); modfiy(1,lim,x+y,x+y,1);
		}else{
			x=read(); ans=cnt*x; ll l,r=x;
			for (;r;r=l-1){
				ll tmp=x/r; l=find(x,tmp,r);
				ans-=tmp*query(1,lim,l,r,1);
			}
			printf("%lld\n",ans);
		}
	}
}
