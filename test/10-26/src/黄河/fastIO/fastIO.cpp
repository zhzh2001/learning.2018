#include <cstdio>
#include <cctype>

inline char gc()
{
    static char buf[1<<12],*p1=buf,*p2=buf;
    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<12,stdin),p1==p2)?EOF:*p1++;
}

#define dd c = gc()
inline void read(int& x)
{
    x = 0;
    int dd;
    bool f = false;
    for(; !isdigit(c); dd)
        if(c == '-')
            f = true;
    for(; isdigit(c); dd)
        x = (x<<1) + (x<<3) + (c^48);
    if(f)
        x = -x;
}
#undef dd
