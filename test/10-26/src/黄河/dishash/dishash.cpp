#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 23333
#define N 1010
using namespace std;
inline LL read(){
	LL x=0;int ch=getchar(),f=1;
	while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
	if (ch=='-'){f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
int n,m,h[N],C[N][N],ans;
inline void pre(){
	C[0][0]=1;
	C[1][0]=1;C[1][1]=1;
	rep(i,1,m) C[i][0]=1;
	rep(i,2,m){
		rep(j,1,i-1)
		C[i][j]=(C[i-1][j-1]+C[i-1][j])%mo;
		C[i][i]=1;
	}
}
inline int  dfs(int now,int numf){
	if (now==n-1) return ((m)%mo);
	if (now==n) return 1;
	int ans=0;
	rep(i,1,n-now){
		ans=(ans+i*C[m][i]*dfs(now+i,i)%mo)%mo;
	}
	return ans%mo;
}
int main(){
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	n=read(),m=read();
	if (m==1||n==1){
		printf("2\n");return 0;
	}
	if (m==2){
		h[0]=1;h[1]=1;
		rep(i,2,n){
			rep(j,0,n-1)
			h[i]=(h[i]+(h[i-j-1]*h[j])%mo)%mo;
		//	printf("%d ",h[i]);
		}
		printf("%d\n",(h[n]+1)%mo);
		return 0;
	}
	if (m==3){
		if (n==1) printf("2");
		if (n==2) printf("4");
		if (n==3) printf("13");
		if (n==4) printf("56");
		return 0;
	}
	pre();
	int res=dfs(1,1);
	printf("%d\n",(res+1)%mo);
	return 0;
}
