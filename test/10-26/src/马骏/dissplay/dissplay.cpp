// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
// #define maxn 
#define maxlim 100010
#define int long long
// #define gc getchar
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
inline char gc(){if(SSS==TTT)TTT=(SSS=LZH)+fread(LZH,1,L,stdin);return *SSS++;}
inline int read(){int d=0,w=1;char c=gc();for(;c<'0'||c>'9';c=gc())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=gc())d=(d<<1)+(d<<3)+c-48;return d*w;}
inline void wln(int x){write(x);putchar('\n');}
inline void wrs(int x){write(x);putchar(' ');}
int tree[maxlim],a[maxlim],lim,n,m,tot;
char c;
void up(int cnt)
{
	tree[cnt]=tree[cnt<<1]+tree[cnt<<1|1];
}
void build(int cnt,int l,int r)
{
	if(l==r)
	{
		tree[cnt]=a[l]*l;
		return;
	}
	else
	{
		int m=l+r>>1;
		build(cnt<<1,l,m);
		build(cnt<<1|1,m+1,r);
		up(cnt);
	}
}
void update(int cnt,int l,int r,int mst,int addv)
{
	if(l==r)
	{
		tree[cnt]+=addv;
		return;
	}
	int m=l+r>>1;
	if(mst<=m) update(cnt<<1,l,m,mst,addv);
		else update(cnt<<1|1,m+1,r,mst,addv);
	up(cnt);
}
void upd(int x,int y)
{
	if(x==0) return;
	a[x]+=y;
	update(1,1,lim,x,y*x);
	tot+=y;
}
int query(int cnt,int l,int r,int ql,int qr)
{
	if(ql<=l&&r<=qr) return tree[cnt];
	int m=l+r>>1,ans=0;
	if(ql<=m) ans=query(cnt<<1,l,m,ql,qr);
	if(qr>m) ans+=query(cnt<<1|1,m+1,r,ql,qr);
	return ans;
}
void query(int x)
{
	int ans=0,sq=sqrt(x);
	if(sq*sq==x) sq--;
	for(int i=1;i<=sq;i++)
	{
		int r=x/i,l=x/(i+1)+1;
		if(r>lim) r=lim;
		if(l>r) continue;
//		wrs(l);
//		wrs(r);
//		wrs(ans);
//		wln(i);
		ans+=query(1,1,lim,l,r)*i;
	}
//	wrs(13251325);
//	wrs(ans);
	for(int i=1;x/i>=sqrt(x)&&i<=lim;i++)
		ans+=a[i]*(x/i)*i;
//	wln(ans);
	wln(x*tot-ans);
}
signed main()
{
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n=read();
	lim=read();
	read();
	for(int i=1;i<=n;i++)
	{
		int x=read(),y=read();
		a[x]=y;
		tot+=y;
	}
	build(1,1,lim);
//		for(int i=1;i<=lim;i++)
//			wrs(a[i]);
//		putchar('\n');
//		for(int i=1;i<=lim;i++)
//			wrs(query(1,1,lim,i,i));
//		putchar('\n');
	for(m=read();m--;)
	{
		for(c=gc();c!='a'&&c!='b'&&c!='c'&&c!='e'&&c!='q';c=gc());
		int x=read();
		if(c=='a') upd(x,1);
		if(c=='b') upd(x,-1);
		if(c=='c')
		{
			int y=read();
			upd(x,-1);
			upd(x-y,1);
		}
		if(c=='e')
		{
			int y=read();
			upd(x,-1);
			upd(x+y,1);
		}
		if(c=='q') query(x);
//		for(int i=1;i<=lim;i++)
//			wrs(a[i]);
//		putchar('\n');
//		for(int i=1;i<=lim;i++)
//			wrs(query(1,1,lim,i,i));
//		putchar('\n');
	}
	return 0;
}
