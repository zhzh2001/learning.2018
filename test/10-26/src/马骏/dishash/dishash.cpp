// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
// #define maxn 
#define int long long
#define gc getchar
#define MOD 23333
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
//inline char gc(){if(SSS==TTT)TTT=(SSS=LZH)+fread(LZH,1,L,stdin);return *SSS++;}
inline int read(){int d=0,w=1;char c=gc();for(;c<'0'||c>'9';c=gc())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=gc())d=(d<<1)+(d<<3)+c-48;return d*w;}
inline void wln(int x){write(x);putchar('\n');}
inline void wrs(int x){write(x);putchar(' ');}
int n,m,tmp,ans=1;
int power(int x,int y,int mod)
{
	int now=x,ans=1;
	for(;y;y>>=1,(now*=now)%=mod)
		if(y&1) (ans*=now)%=mod;
	return ans;
}
signed main()
{
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	n=read();
	m=read();
	tmp=n*m%MOD;
	for(int i=2;i<=n;i++)
		(ans*=tmp-n+i)%=MOD;
//	wln(ans);
	for(int i=2;i<=n;i++)
		(ans*=power(i,MOD-2,MOD))%=MOD;
	write((ans+MOD+1)%MOD);
	return 0;
}
