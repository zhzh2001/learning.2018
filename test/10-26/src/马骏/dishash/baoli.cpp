// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
// #define maxn 
// #define int long long
#define gc getchar
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
//inline char gc(){if(SSS==TTT)TTT=(SSS=LZH)+fread(LZH,1,L,stdin);return *SSS++;}
inline int read(){int d=0,w=1;char c=gc();for(;c<'0'||c>'9';c=gc())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=gc())d=(d<<1)+(d<<3)+c-48;return d*w;}
inline void wln(int x){write(x);putchar('\n');}
inline void wrs(int x){write(x);putchar(' ');}
int n,f[100],c[100][100];
signed main()
{
//	freopen(".in","r",stdin);
//	freopen(".out","w",stdout);
	n=read();
	f[0]=1;
	for(int i=1;i<=n;i++)
		for(int j=0;j<i;j++)
			f[i]+=f[j]*f[i-1-j];
	for(int i=1;i<=n;i++)
		wrs(f[i]);
	putchar('\n');
	memset(f,0,sizeof f);
	f[0]=1;
	for(int i=1;i<=n;i++)
		for(int j=0;j<i;j++)
			for(int k=0;k<i-j;k++)
				f[i]+=f[j]*f[k]*f[i-j-k-1];
	for(int i=1;i<=n;i++)
		wrs(f[i]);
	putchar('\n');
	c[0][0]=1;
	for(int i=1;i<=n*3;i++)
	{
		c[i][0]=1;
		for(int j=1;j<=i;j++)
			c[i][j]=c[i-1][j-1]+c[i-1][j];
	}
	for(int i=1;i<=n;i++)
		wrs(c[2*i][i]/((2-1)*i+1));
	putchar('\n');
	for(int i=1;i<=n;i++)
		wrs(c[3*i][i]/((3-1)*i+1));
//	for(int i=0;i<=n*2;i++,putchar('\n'))
//		for(int j=0;j<=i;j++)
//			wrs(c[i][j]);
	return 0;
}
//h(n)=C(2n,n)/(n+1) 
