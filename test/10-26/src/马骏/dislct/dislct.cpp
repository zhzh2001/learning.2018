// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 6000
// #define int long long
#define gc getchar
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
//inline char gc(){if(SSS==TTT)TTT=(SSS=LZH)+fread(LZH,1,L,stdin);return *SSS++;}
inline int read(){int d=0,w=1;char c=gc();for(;c<'0'||c>'9';c=gc())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=gc())d=(d<<1)+(d<<3)+c-48;return d*w;}
inline void wln(int x){write(x);putchar('\n');}
inline void wrs(int x){write(x);putchar(' ');}
int xx[maxn],yy[maxn],vis[maxn],siz[maxn],ssiz[maxn],d[maxn],f[maxn],ff[maxn],all,son[maxn],top[maxn],lok,n,m;
queue<int> q;
vector<int> a[maxn];
void dfs1(int k,int fa)
{
	vis[k]=1;
	q.push(k);
	siz[k]=1;
	for(int i=0;i<a[k].size();i++)
	{
		if(a[k][i]==fa||a[k][i]==0) continue;
		dfs1(a[k][i],k);
		siz[k]+=siz[a[k][i]];
	}
}
void dfs2(int k,int fa,int now)
{
	if(now<=all&&siz[son[k]]<=all)
	{
		lok=k;
		return;
	}
	for(int i=0;i<a[k].size()&&!lok;i++)
		if(a[k][i]!=fa&&a[k][i]!=0) dfs2(a[k][i],k,now+siz[k]-1-siz[a[k][i]]);
}
void dfs3(int k,int tp)
{
	top[k]=tp;
	if(!son[k]) return;
	dfs3(son[k],tp);
	for(int i=0;i<a[k].size();i++)
		if(a[k][i]!=f[k]&&a[k][i]!=son[k]) dfs3(a[k][i],a[k][i]);
}
void dfs4(int k,int fa)
{
	f[k]=fa;
	ssiz[k]=1;
	d[k]=d[fa]+1;
	for(int i=0;i<a[k].size();i++)
		if(a[k][i]!=fa)
		{
			dfs4(a[k][i],k);
			ssiz[k]+=ssiz[a[k][i]];
			if(ssiz[a[k][i]]>ssiz[son[k]]) son[k]=a[k][i];
		}
}
int lca(int x,int y)
{
	int ans=0;
	for(;top[x]!=top[y];)
	{
		if(d[top[x]]<d[top[y]]) swap(x,y);
		ans+=d[top[x]]-d[x]+1;
		x=f[top[x]];
	}
	if(d[x]>d[y]) swap(x,y);
	return ans+d[y]-d[x];
}
signed main()
{
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<n;i++)
	{
		int x=read(),y=read();
		xx[i]=x;
		yy[i]=y;
		a[x].push_back(y);
		a[y].push_back(x);
	}
	dfs4(1,0);
	dfs3(1,1);
	dfs1(1,0);
	all=n>>1;
	dfs2(1,0,0);
	for(;!q.empty();q.pop())
		ff[q.front()]=lok;
	for(int i=1;i<=m;i++)
	{
		int x=read(),y=read();
		if(x==2) wln(lca(y,ff[y]));
		if(x==1)
		{
			for(int i=0;i<=a[xx[y]].size();i++)
				if(a[xx[y]][i]==yy[y]) a[xx[y]][i]=0;
			for(int i=0;i<=a[yy[y]].size();i++)
				if(a[yy[y]][i]==xx[y]) a[yy[y]][i]=0;
			memset(vis,0,sizeof vis);
			for(int i=1;i<=n;i++)
				if(!vis[i])
				{
					dfs1(i,0);
					all=q.size()>>1;
					lok=0;
					dfs2(i,0,0);
					for(;!q.empty();q.pop())
						ff[q.front()]=lok;
				}
		}
	}
	return 0;
}
