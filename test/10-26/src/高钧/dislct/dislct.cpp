#include <bits/stdc++.h>
using namespace std;
const int N=50005,M=100005,inf=0x3f3f3f3f;
int n,m,tot=0,Next[M],to[M],head[M],flg[M],sz[N],siz=0,pp[N],rt=0,q[N],bo[N],ss[N];
inline void add(int x,int y){Next[++tot]=head[x]; to[tot]=y; head[x]=tot; flg[tot]=0;}
inline void getsz(int x,int la){int i; sz[x]=1; siz++; for(i=head[x];i;i=Next[i])if(to[i]!=la&&!flg[i]){getsz(to[i],x);sz[x]+=sz[to[i]];}}
inline void bfs(int s)
{
  int i,x,tou=1,wei=2; q[tou]=s; memset(bo,0,sizeof bo); bo[s]=1;  ss[s]=0;
  while(tou<wei)
  {
    x=q[tou++]; for(i=head[x];i;i=Next[i])if(!bo[to[i]]&&!flg[i]){bo[to[i]]=bo[x]+1; q[wei++]=to[i];}
  }for(i=1;i<=n;i++)ss[s]+=(bo[i]-1);
}
inline int bbfs(int s,int t)
{
  int i,x,tou=1,wei=2; q[tou]=s; memset(bo,0,sizeof bo); bo[s]=1;
  while(tou<wei){x=q[tou++]; if(x==t)return bo[x]-1; for(i=head[x];i;i=Next[i])if(!bo[to[i]]&&!flg[i]){bo[to[i]]=bo[x]+1; q[wei++]=to[i];}}
}
inline void getrt(int x,int la){int i; for(i=head[x];i;i=Next[i])if(to[i]!=la&&!flg[i]){ss[to[i]]=ss[x]-sz[to[i]]+siz-sz[to[i]]; getrt(to[i],x);}}
inline int solve(int x){int i; siz=0; getsz(x,0); bfs(x); getrt(x,0); ss[rt=n+1]=inf; for(i=1;i<=n;i++)if(bo[i]){if(ss[i]<ss[rt]||(ss[i]==ss[rt]&&i<rt))rt=i;} return bbfs(rt,x);}
int main()
{
  freopen("dislct.in","r",stdin);
  freopen("dislct.out","w",stdout);
  int i,x,y; scanf("%d%d",&n,&m);
  for(i=1;i<n;i++){scanf("%d%d",&x,&y); add(x,y); add(y,x);}
  for(i=1;i<=m;i++)
  {
    scanf("%d%d",&x,&y); if(x==1)flg[y*2-1]=flg[y*2]=1;else printf("%d\n",solve(y));
  }
}
