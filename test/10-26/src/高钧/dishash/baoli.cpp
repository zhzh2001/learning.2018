#include <cstdio>
#include <algorithm>
using namespace std;
const int N=160,B=90000,md=23333;
int n,m,f[N][N][N],jc[B+5],invjc[B+5],cnt=0;
inline int ksm(int x,int y){int re=1; while(y){if(y&1)re=re*x%md;x=x*x%md;y>>=1;}return re;}
inline int C(int n,int m){return jc[n]*invjc[m]%md*invjc[n-m]%md;}
inline void Ad(int &x,int y){x=x+y;x-=(x>=md)?md:0;x+=(x<0)?md:0;}
int main()
{
//  freopen("dishash.out","w",stdout);
	int i,re=0,ce,zqiu,qiu,laszqiu,lasqiu; scanf("%d%d",&n,&m); f[1][1][1]=1;
  jc[0]=invjc[0]=1; for(i=1;i<=B;i++)jc[i]=jc[i-1]*i%md,invjc[i]=ksm(jc[i],md-2);
  re=0;
 	for(ce=2;ce<=n;ce++)
 	{
    for(zqiu=2;zqiu<=n;zqiu++)
 	  {
      for(qiu=1;qiu<=zqiu&&qiu<=m;qiu++)
 	    {
	      for(lasqiu=1;lasqiu<=zqiu-qiu&&lasqiu<=m;lasqiu++)
	      {
          Ad(f[zqiu][ce][qiu],f[zqiu-qiu][ce-1][lasqiu]*C(lasqiu*m,qiu)%md);
        }
      }
    }
  }for(ce=1;ce<=n;ce++)for(qiu=1;qiu<=m;qiu++)Ad(re,f[n][ce][qiu]); Ad(re,1); printf("%d\n",re);
}
