#include <bits/stdc++.h>
using namespace std;
const long long N=100005;
long long n,m,up,L;
struct segtree
{
  long long s[N<<2];
  #define c1 (k<<1)
  #define c2 (k<<1|1)
  inline void ins(long long l,long long r,long long po,long long v,long long k)
  {
    if(l==r){s[k]+=v;return;}long long mid=(l+r)>>1; if(po<=mid)ins(l,mid,po,v,c1);else ins(mid+1,r,po,v,c2); s[k]=s[c1]+s[c2];
  }
  inline long long que(long long l,long long r,long long ll,long long rr,long long k,long long v)
  {
    if(!s[k])return 0; if(l==r){return s[k]*(v%l);} long long mid=(l+r)>>1;
    if(rr<=mid&&s[c1])return que(l,mid,ll,rr,c1,v);else if(ll>mid&&s[c2])return que(mid+1,r,ll,rr,c2,v);
    else{long long re=0; if(s[c1])re+=que(l,mid,ll,mid,c1,v); if(s[c2])re+=que(mid+1,r,mid+1,rr,c2,v); return re;}
  }
  inline long long ask(long long l,long long r,long long ll,long long rr,long long k)
  {
    if(!s[k])return 0; if(l==ll&&r==rr){return s[k];} long long mid=(l+r)>>1;
    if(rr<=mid&&s[c1])return ask(l,mid,ll,rr,c1);else if(ll>mid&&s[c2])return ask(mid+1,r,ll,rr,c2);
    else{long long re=0; if(s[c1])re+=ask(l,mid,ll,mid,c1); if(s[c2])re+=ask(mid+1,r,mid+1,rr,c2); return re;}
  }
}T;
int main()
{
  freopen("dissplay.in","r",stdin);
  freopen("dissplay.out","w",stdout);
  long long i,a,b; char s[10]; scanf("%lld%lld%lld",&n,&up,&L);
  for(i=1;i<=n;i++)
  {
    scanf("%lld%lld",&a,&b); T.ins(1,up,a,b,1);
  }scanf("%lld",&m);
  for(i=1;i<=m;i++)
  {
    scanf("%s",s+1);scanf("%lld",&a);
    if(s[1]=='a') T.ins(1,up,a,1,1);
    else if(s[1]=='b') T.ins(1,up,a,-1,1);
    else if(s[1]=='c') {scanf("%lld",&b); T.ins(1,up,a,-1,1); T.ins(1,up,a-b,1,1);}
    else if(s[1]=='e') {scanf("%lld",&b); T.ins(1,up,a,-1,1); T.ins(1,up,a+b,1,1);}
    else {if(a)b=T.que(1,L,1,a,1,a); b+=T.ask(1,L,a+1,up,1)*a; printf("%lld\n",b);}
  }
}
