#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("dislct.in", "r", stdin);
	freopen("dislct.out", "w", stdout);
}
const int N = 310000, M = N * 2;
struct Edge {
	int u, v, nxt;
}e[M];
int n, m, head[N], en;
bool cut[M];
void add(int x, int y) {
	e[++en].u = x, e[en].v = y, e[en].nxt = head[x], head[x] = en; 
} 
int sum = 0, siz[N];
void dfs(int x, int F) {
	siz[x] = 1;
	for(int i = head[x]; i;i = e[i].nxt) if(!cut[i]){
		int y = e[i].v;
		if(y == F) continue;
		dfs(y, x);
		siz[x] += siz[y];
	}
}
int g[N], rt;
void getrt(int x, int F) {
	g[x] = 0;
	for(int i = head[x]; i;i = e[i].nxt) if(!cut[i]){
		int y = e[i].v;
		if(y == F) continue;
		getrt(y, x);
		g[x] = max(siz[y], g[x]);
	}
	g[x] = max(g[x], sum - siz[x]);
	if(g[x] < g[rt]) rt = x;
	else if(g[x] == g[rt] && rt > x) rt = x;
}
int d[N];
void getdis(int x, int F) {
	d[x] = d[F] + 1;
	for(int i = head[x]; i; i = e[i].nxt) if(!cut[i]){
		int y = e[i].v;
		if(y == F) continue;
		getdis(y, x);
	}
}
void makedis(int x) {
	dfs(x, 0);
	sum = siz[x];
	rt = 0;
	getrt(x, 0);
	getdis(rt, 0);
}
void del(int x) {
	x *= 2;
	cut[x] = cut[x ^ 1] = 1;
	makedis(e[x].u);
	makedis(e[x].v);
}
int main() {
	setIO();
	n = read(), m = read();
	en = 1;
	for(int i = 1; i < n; ++i) {
		int x = read(), y = read();
		add(x, y);
		add(y, x);
	}
	int op, x;
	g[0] = 1e9;
	d[0] = -1;
	makedis(1);
	while(m--) {
		op = read(), x = read();
		if(op == 1) {
			if(!cut[x * 2])
				del(x);
		} else writeln(d[x]);
	}
	return 0;
}
