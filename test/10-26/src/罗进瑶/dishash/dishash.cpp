#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("dishash.in", "r", stdin);
	freopen("dishash.out", "w", stdout);
}
const int p = 23333;
const int N = 12345;
int n, m, f[N];
namespace G2 {
	void main() {
		f[0]  = 1;
		for(int i = 1; i <= n; ++i)
			for(int j = 0; j < i; ++j)
				(f[i] += f[j] * f[i - j - 1]) %= p;
		writeln((f[n] + 1) % p);		
	} 
} 
namespace G3 {
	void main() {
		f[0]  = 1;
		for(int i = 1; i <= n; ++i)
			for(int j = 0; j < i; ++j)
				for(int k = 0; j + k <= i - 1; ++k)
				(f[i] += f[j] * f[k] % p * f[i - 1 - j - k]) %= p;
		writeln((f[n] + 1) % p);		
	}
}
int main() {
	setIO();
	n = read(), m = read();
	if(n > 1000) {
		puts("0");
		return 0;
	}
	if(m == 2) G2::main();
	else if(m == 1) {ll res = 1; for(int i = 2; i <= n; ++i) res = res * 2 % p;writeln((res + 1) % p);return 0;}
	else if(m == 3) G3::main();
	else puts("0");
	return 0;
}
