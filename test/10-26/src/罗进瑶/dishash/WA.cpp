#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("dishash.in", "r", stdin);
	freopen("dishash.out", "w", stdout);
}
const int p = 23333;
const int N = 1234;
int n, m;
int f[N][N];
int main() {
	n = read(), m = read();
	f[0][0] = 1;
	for(int i = 1; i <= n; ++i) {
		for(int j = 1; j <= min(i, m); ++j)
			for(int a = 0; a < i; ++a)
				for(int k = 0; k <= min(i - 1 - a, m); ++k) {
					(f[i][j] += f[a][j - 1] * f[i - 1 - a][k]) %= p;
			//		if(i == 2 && j == 2) printf("(%d,%d,%d)(%d,%d,%d)\n", a, j - 1,f[a][j - 1], i - 1 - a, k, f[i - 1 - a][k]);
				}	
	}
/*	for(int i = 0; i <= n; ++i) {
		for(int j = 0;  j<= m; ++j)
			printf("%d ", f[i][j]);
		puts("");
	}*/
	int res = 0;
	for(int i = 1; i <= m; ++i)
		(res += f[n][m]) %= p;
	res = (res + 1) % p;
	writeln(res);
	return 0;
}

/*
f[i][j] 节点个数为i，根节点左深度为j 

f[i][j] = f[a][j - 1] * f[i - 1 - a][k]  
*/
