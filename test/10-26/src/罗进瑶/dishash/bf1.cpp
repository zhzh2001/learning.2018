#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("dishash.in", "r", stdin);
	freopen("dishash.out", "w", stdout);
}
const int p = 23333;
const int N = 12345;
int n, m, f[N];
int main() {
	n = read(), m = read();
	f[0]  = 1;
	for(int i = 1; i <= n; ++i)
		for(int j = 0; j < i; ++j)
			(f[i] += f[j] * f[i - j - 1]) %= p;
	writeln((f[n] + 1) % p);
	return 0;
}
