#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("dissplay.in", "r", stdin);
	freopen("dissplay.out", "w", stdout);
}
int n, lim, L;
ll cnt[210000];
ll ans;
char op[5];
ll query(int x) {
	ll res = 0;
	for(int i = 1; i <= lim; ++i)
		if(cnt[i])
			res += (x % i) * cnt[i];
	return res;
}
int main() {
	setIO();
	n = read(), lim = read(), L = read(); 
	for(int i = 1; i <= n; ++i) {
		int x = read(), y = read();
		cnt[x] = y;
	}
	int Q = read(), x;
	while(Q--) {
		scanf("%s", op);
		x = read();
		if(op[0] == 'a') {
			++cnt[x];
		} else if(op[0] == 'b') {
			--cnt[x];
		} else if(op[0] == 'c') {
			--cnt[x];
			++cnt[x - read()];
		} else if(op[0] == 'e') {
			--cnt[x];
			++cnt[x + read()];
		} else if(op[0] == 'q') {
			writeln(query(x));
		}
		cnt[0] = 0;
	}
	return 0;
}
