#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <limits>
#include <vector>
using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;

namespace IO {
static const int kMaxSize = 1 << 20;
char buf[kMaxSize], *S = buf, *T = buf;
bool neg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMaxSize, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = neg = 0;
  char ch = NC();
  for (; isspace(ch); ch = NC())
    ;
  if (ch == '-')
    neg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (neg) x = -x;
}
inline char RC() {
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  return ch;
}
inline int RS(char *s) {
  int cur = 0, ch = NC();
  for (; isspace(ch); ch = NC());
  for (; ch != EOF && !isspace(ch); ch = NC())
    s[cur++] = ch;
  return s[cur] = 0, cur;
}
} // namespace IO

using IO::RI;
using IO::RS;
using IO::RC;

const int kMod = 23333;
ll n, m;
ll fact[kMod << 1], rfact[kMod << 1];

inline ll QuickPow(ll x, ll y) {
  ll ret = 1;
  x %= kMod;
  for (; y; y >>= 1, x = x * x % kMod)
    if (y & 1) ret = ret * x % kMod;
  return ret;
}

inline ll Combin(ll n, ll m) {
  if (n < m || m < 0)
    return 0;
  return fact[n] * rfact[m] % kMod * rfact[n - m] % kMod;
}

inline ll Lucas(ll n, ll m, ll p) {
  if (m == 0)
    return 1;
  return Combin(n % p, m % p) * Lucas(n / p, m / p, p) % kMod;
}

int main() {
  freopen("dishash.in", "r", stdin);
  freopen("dishash.out", "w", stdout);
  RI(n), RI(m);
  fact[0] = 1;
  for (int i = 1; i < kMod; ++i)
    fact[i] = (fact[i - 1] * i) % kMod;
  rfact[kMod - 1] = QuickPow(fact[kMod - 1], kMod - 2);
  for (int i = kMod - 1; i > 0; --i)
    rfact[i - 1] = (rfact[i] * i) % kMod;
  ll ans = Lucas(1LL * n * m, n, kMod) * QuickPow(1LL * (m - 1) * n + 1, kMod - 2) % kMod;
  printf("%lld\n", (ans + 1) % kMod);
  return 0;
}
