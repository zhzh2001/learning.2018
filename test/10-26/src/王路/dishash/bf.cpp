#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <limits>
#include <vector>
using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;

namespace IO {
static const int kMaxSize = 1 << 20;
char buf[kMaxSize], *S = buf, *T = buf;
bool neg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMaxSize, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = neg = 0;
  char ch = NC();
  for (; isspace(ch); ch = NC())
    ;
  if (ch == '-')
    neg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (neg) x = -x;
}
inline char RC() {
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  return ch;
}
inline int RS(char *s) {
  int cur = 0, ch = NC();
  for (; isspace(ch); ch = NC());
  for (; ch != EOF && !isspace(ch); ch = NC())
    s[cur++] = ch;
  return s[cur] = 0, cur;
}
} // namespace IO

using IO::RI;
using IO::RS;
using IO::RC;

const int kMaxN = 2005;
long long f[kMaxN], n, m, c[kMaxN][kMaxN];

ll QuickPow(long long x, long long y) {
  ll ret = 1;
  for (; y; y >>= 1, x = x * x % 23333)
    if (y & 1) ret = ret * x % 23333;
  return ret;
}

int main() {
  freopen("bf.out", "w", stdout);
  n = 100, m = 4;
  f[0] = 1;
  c[0][0] = 1;
  for (int i = 1; i <= n; ++i) {
    c[i][0] = 1;
    for (int j = 1; j <= i; ++j)
      c[i][j] = (c[i - 1][j] + c[i - 1][j - 1]) % 23333;
  }
  // for (int i = 1; i <= n; ++i) {
    // for (int j = 0; j <= i; ++j)
      // printf("%d%c", c[i][j], " \n"[j == i]);
  // }
  for (int i = 1; i <= n; ++i) {
    int c = i;
    for (int lhs = 0; lhs < c; ++lhs) {
      for (int mid = 0; mid < c; ++mid) {
        if (lhs + mid >= c)
          break;
        for (int ppp = 0; ppp < c; ++ppp) {
        for (int rhs = 0; rhs < c; ++rhs) {
          if (lhs + mid + rhs + ppp == c - 1) {
            f[i] += (f[lhs] * f[mid] * f[rhs] * f[ppp]) % 23333;
            f[i] %= 23333;
          }
        }
        }
      }
    }
    printf("%d %d: %d %d\n", i, m, f[i], (::c[(int)m * i][i]) * QuickPow((m - 1) * i + 1, 23331) % 23333);
  }
  return 0;
}