#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <limits>
#include <vector>
using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;

namespace IO {
static const int kMaxSize = 1 << 20;
char buf[kMaxSize], *S = buf, *T = buf;
bool neg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMaxSize, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = neg = 0;
  char ch = NC();
  for (; isspace(ch); ch = NC())
    ;
  if (ch == '-')
    neg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (neg) x = -x;
}
inline char RC() {
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  return ch;
}
inline int RS(char *s) {
  int cur = 0, ch = NC();
  for (; isspace(ch); ch = NC());
  for (; ch != EOF && !isspace(ch); ch = NC())
    s[cur++] = ch;
  return s[cur] = 0, cur;
}
} // namespace IO

using IO::RI;
using IO::RS;
using IO::RC;

const int kMaxN = 3e5 + 5, kMaxM = 3e5 + 5, kInf = 0x3f3f3f3f;
int n, m, head[kMaxN], ecnt = 1, dep[kMaxN];
ll val[kMaxN], sz[kMaxN];
pii q[kMaxM];
int idx[kMaxN];
struct Edge {
  int fr, to, nxt, id;
  bool exist;
} e[kMaxM << 1];

inline void Insert(int x, int y, int id) {
  e[++ecnt] = (Edge) { x, y, head[x], id, true };
  head[x] = ecnt;
}

ll mndis, mnpos;

int DFS1(int x, int f = 0) {
  sz[x] = 1;
  val[x] = 0;
  for (int i = head[x]; i; i = e[i].nxt) {
    if (e[i].to == f || !e[i].exist)
      continue;
    DFS1(e[i].to, x);
    sz[x] += sz[e[i].to];
    val[x] = val[e[i].to] + sz[e[i].to];
  }
}

int rt;

int DFS3(int x, ll dis, int f = 0) {
  for (int i = head[x]; i; i = e[i].nxt) {
    if (e[i].to == f || !e[i].exist)
      continue;
    DFS3(e[i].to, dis + (sz[rt] - sz[e[i].to]) + val[x] - val[e[i].to] - sz[e[i].to], x);
  }
  if (dis + val[x] < mndis || (dis + val[x] == mndis && x < mnpos))
    mndis = dis + val[x], mnpos = x;
}

void DFS2(int x, int f = 0) {
  dep[x] = dep[f] + 1;
  for (int i = head[x]; i; i = e[i].nxt) {
    if (e[i].to == f || !e[i].exist)
      continue;
    DFS2(e[i].to, x);
  }
}

void Update(int x) {
  mnpos = -1, mndis = 1LL << 60;
  rt = x;
  DFS1(x, 0);
  DFS3(x, 0, 0);
  DFS2(mnpos, 0);
  // fprintf(stderr, "%d\n", mnpos);
}

namespace SubTask1 {
int Solve() {
  Update(1);
  for (int i = 1; i <= m; ++i) {
    int x = q[i].first, y = q[i].second;
    if (x == 1) {
      e[idx[y]].exist = false;
      e[idx[y] ^ 1].exist = false;
      Update(e[idx[y]].fr);
      Update(e[idx[y]].to);
    } else if (x == 2) {
      printf("%d\n", dep[y] - 1);
    }
  }
  return 0;
}
} // namespace SubTask1

int main() {
  freopen("dislct.in", "r", stdin);
  freopen("dislct.out", "w", stdout);
  RI(n), RI(m);
  for (int i = 1, x, y; i < n; ++i) {
    RI(x), RI(y);
    Insert(x, y, i);
    idx[i] = ecnt;
    Insert(y, x, i);
  }
  bool cntx = 0;
  for (int i = 1; i <= m; ++i) {
    RI(q[i].first), RI(q[i].second);
    cntx += q[i].first == 1;
  }
  if ((n <= 6000 && m <= 6000) || cntx == 0)
    return SubTask1::Solve(); // O(n \times mcnt(1)), 50 pts
  return 0;
}