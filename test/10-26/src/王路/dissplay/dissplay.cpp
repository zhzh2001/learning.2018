#pragma GCC optimize 2
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <limits>
#include <vector>
#include <map>
using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;

namespace IO {
static const int kMaxSize = 1 << 20;
char buf[kMaxSize], *S = buf, *T = buf;
bool neg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMaxSize, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = neg = 0;
  char ch = NC();
  for (; isspace(ch); ch = NC())
    ;
  if (ch == '-')
    neg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (neg) x = -x;
}
inline char RC() {
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  return ch;
}
inline int RS(char *s) {
  int cur = 0, ch = NC();
  for (; isspace(ch); ch = NC());
  for (; ch != EOF && !isspace(ch); ch = NC())
    s[cur++] = ch;
  return s[cur] = 0, cur;
}
} // namespace IO

using IO::RI;
using IO::RS;
using IO::RC;

const int kMaxN = 1e5 + 5, kMaxQ = 1e5 + 5;

int n, lim, L, m;
map<int, int> mp;
char opt[233];

struct Atom {
  char opt;
  int x, y;
} q[kMaxQ];

namespace SubTask1 {
int Solve() {
  for (int i = 1; i <= m; ++i) {
    int x = q[i].x, y = q[i].y;
    switch(q[i].opt) {
      case 'a': { ++mp[x]; break; } 
      case 'b': { --mp[x]; break; }
      case 'c': { --mp[x]; ++mp[x - y]; break; }
      case 'e': { --mp[x]; ++mp[x + y]; break; }
      case 'q': {
        ll ans = 0;
        for (map<int, int>::iterator it = mp.begin(); it != mp.end(); ++it) {
          ans += static_cast<ll>(it->second) * (x % it->first);
        }
        printf("%lld\n", ans);
        break;
      }
    }
  }
  return 0;
}
} // namespace SubTask1

namespace SubTask2 {
int Solve() {
  ll gel = 0;
  for (map<int, int>::iterator it = mp.begin(); it != mp.end(); ++it) {
    if (it->first > L)
      gel = gel + it->second;
  }
  for (int i = 1; i <= m; ++i) {
    int x = q[i].x, y = q[i].y;
    switch(q[i].opt) {
      case 'a': {
        ++mp[x];
        gel += (x > L);
        break; 
      } 
      case 'b': {
        --mp[x];
        gel -= (x > L);
        break; 
      }
      case 'c': {
        --mp[x];
        gel -= (x > L);
        ++mp[x - y];
        gel += (x - y > L);
        break; 
      }
      case 'e': {
        --mp[x];
        gel -= (x > L);
        ++mp[x + y];
        gel += (x + y > L);
        break;
      }
      case 'q': {
        ll ans = 0;
        for (map<int, int>::iterator it = mp.begin(); it != mp.end(); ++it) {
          if (it->first > L) // x < it->first
            break;
          ans += (it->second) * (x % it->first);
        }
        printf("%lld\n", ans + 1LL * x * gel);
        break;
      }
    }
  }
  return 0;
}
} // namespace SubTask2

int main() {
  freopen("dissplay.in", "r", stdin);
  freopen("dissplay.out", "w", stdout);
  RI(n), RI(lim), RI(L);
  for (int i = 1, a, b; i <= n; ++i) {
    RI(a), RI(b);
    mp[a] = b;
  }
  RI(m);
  int cntq = 0;
  for (int i = 1, x, y; i <= m; ++i) {
    RS(opt), RI(x);
    if (opt[0] == 'c' || opt[0] == 'e')
      RI(y);
    cntq += opt[0] == 'q';
    q[i].opt = opt[0], q[i].x = x, q[i].y = y;
  }
  if ((n <= 5000 && m <= 5000) || (cntq <= 200 && n <= 100000))
    return SubTask1::Solve(); // O(cntq \times n), 1 ~ 4, 8 ~ 10 :: 35 pts
  if (L <= 200)
    return SubTask2::Solve(); // O(cntq \times L), 5 ~ 7 :: 15 pts
  return 0;
}
