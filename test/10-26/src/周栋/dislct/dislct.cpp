#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10;
ll x[N],y[N],head[N],edge,n,m,fa[N],son[N],rt,dep[N];
struct edge{ll to,net;}e[N<<1];
inline void addedge(ll x,ll y){
	e[++edge].net=head[x],head[x]=edge,e[edge].to=y;
	e[++edge].net=head[y],head[y]=edge,e[edge].to=x;
}
inline void dfs(ll x){
	son[x]=1;
	for (ll i=head[x];i;i=e[i].net)
		if (e[i].to!=fa[x]){
			fa[e[i].to]=x;
			dfs(e[i].to);
			son[x]+=son[e[i].to];
		}
}
inline void get_rt(ll x){
	rt=x;
	for (ll i=head[x];i;i=e[i].net)
		if ((son[e[i].to]>n/2||(son[e[i].to]==n/2&&i>e[i].to))&&e[i].to!=fa[x]) get_rt(e[i].to);
}
inline void dfs1(ll x){
	for (ll i=head[x];i;i=e[i].net){
		if (e[i].to==fa[x]) continue;
		fa[e[i].to]=x;
		dep[e[i].to]=dep[x]+1;
		dfs1(e[i].to);
	}
}
int main(){
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	read(n),read(m);
	for (ll i=1;i<n;++i) read(x[i]),read(y[i]),addedge(x[i],y[i]);
	dfs(1);
	get_rt(1);
	memset(fa,0,sizeof fa);
	dfs1(rt);
	for (ll i=1,x,y;i<=m;++i){
		read(x),read(y);
		if (x==2) wr(dep[y]),puts("");
	}
	return 0;
}
//sxdakking
