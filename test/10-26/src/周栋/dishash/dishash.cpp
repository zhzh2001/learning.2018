#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll P=23333,N=1e6+10;
ll n,m,ans,fac[N],inv[N];
inline ll pow(ll x,ll y){
	ll ret=1;
	for (;y;y>>=1,x=x*x%P) if (y&1) ret=ret*x%P;
	return ret;
}
inline ll C(ll x,ll y){return fac[x]*inv[y]%P*inv[x-y]%P;}
int main(){
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	read(n),read(m);
	fac[0]=1;
	for (ll i=1;i<=m*n+1;++i) fac[i]=fac[i-1]*i%P;
	inv[m*n+1]=pow(fac[m*n+1],P-2);
	for (ll i=m*n;~i;--i) inv[i]=inv[i+1]*(i+1)%P;
	ans=fac[m*n];
	ans=ans*inv[n]%P*inv[n*m-n]%P;
	wr((C(n*m+1,n)*pow(m*n+1,P-2)%P+1)%P);
	return 0;
}
//sxdakking
/*
C(2n,n)/(n+1)
=(2n+1)!/n!/(n+1)!/(2n+1)
=C(2n+1,n)/(2n+1)
->C(n*m+1,n)/(n*m+1)
*/
