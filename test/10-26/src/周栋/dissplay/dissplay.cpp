#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline char gett(){char x=gc();for(;x<'a'||x>'z';x=gc());return x;}
const ll N=1e5+10;
ll n,lim,L,a[N],Q,ans,A[N],num;
int main(){
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	read(n),read(lim),read(L);
	for (ll i=1,x;i<=n;++i){
		read(x);
		read(a[x]);
		if (x>L) num+=a[x];
	}
	read(Q);
	for (ll i=1,x,y;i<=Q;++i){
		char opt=gett();
		read(x);
		if (opt=='a'){
			++a[x];
			if (x>L) ++num;
		}
		if (opt=='b'){
			--a[x];
			if (x>L) --num;
		}
		if (opt=='c'){
			read(y);
			--a[x];
			if (x>L) ++num;
			++a[x-y];
			if (x>L) --num;
		}
		if (opt=='e'){
			read(y);
			--a[x];
			if (x>L) --num;
			++a[x+y];
			if (x+y>L) ++num;
		}
		if (opt=='q'){
			ans=0;
			for (ll j=1;j<=min(L,lim);++j) ans=ans+x%j*a[j];
			wr(ans+num*x),puts("");
		}
	}
	return 0;
}
//sxdakking
