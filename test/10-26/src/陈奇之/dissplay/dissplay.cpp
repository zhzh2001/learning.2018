#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
namespace IO{
	char c,st[66];int tp=0,f;
	#define gc getchar
	#define pc putchar
	inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
}using IO::read;using IO::write;using IO::writeln;using IO::wri;
#define rd read
const int maxn = 100055;
ll a[maxn],All,c[maxn];
int n,m,lim,L;
#define lowbit(x) ((x) & (-x))
void add(int x,int y){
	a[x]+=y;
	ll tmp = x;
	for(;x<=L;x+=lowbit(x)) c[x]+=y*tmp;
}
ll query(int x,int y){
	if(y-x+1 <= 5){
		ll ans = 0;
		Rep(i,x,y) ans += a[i] * i;
		return ans;
	} else{
		ll ans = 0;
		for(x--;x;x-=lowbit(x)) ans-=c[x];
		for(;y;y-=lowbit(y)) ans+=c[y];
		return ans;
	}
}
signed main(){
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n = rd(),lim = rd(),L = rd();
	Rep(i,1,n){
		ll a = rd(),b = rd();
		All+=b;
		add(a,b);
	}
	m = rd();
	Rep(i,1,m){
		char opt[13];
		scanf("%s",opt);
		if(opt[0]=='a'){
			int x = rd();
			All += 1,add(x,1);
		} else
		if(opt[0]=='b'){
			int x = rd();
			All -= 1,add(x,-1);
		} else
		if(opt[0]=='c'){
			int x = rd(),y = rd();
			add(x,-1),add(x-y,1);
		} else
		if(opt[0]=='e'){
			int x = rd(),y = rd();
			add(x,-1),add(x+y,1);
		} else
		if(opt[0]=='q'){
			int x = rd();
			ll ans = All * x;
//			比x大的显然直接加上不用减小贡献吧 
			for(int i=1,j;i<=x;i=j+1){
				j=x/(x/i);
				ans = ans - query(i,j) * (x/i);
			}
			writeln(ans);
		}
	}
	return 0;
}
