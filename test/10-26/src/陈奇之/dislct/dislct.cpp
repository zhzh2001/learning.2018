#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
namespace IO{
	char c,st[66];int tp=0,f;
	#define gc getchar
	#define pc putchar
	inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
}using IO::read;using IO::write;using IO::writeln;using IO::wri;
#define rd read
const int maxn = 305050;
struct Edge{
	int to,nxt;
	Edge(){}
	Edge(int to,int nxt):to(to),nxt(nxt){}
}edge[maxn * 2];
int first[maxn],nume;
int X[maxn],Y[maxn];
int n,m;
int dep[maxn],fa[maxn],size[maxn],f[maxn],root[maxn];
bool mark[maxn];
void Addedge(int a,int b){
	edge[nume] = Edge(b,first[a]);
	first[a] = nume++;
}
inline int lca(int x,int y){
	if(x==y) return x;
	if(dep[x] > dep[y]) return lca(fa[x],y); else
						return lca(x,fa[y]);
}
inline int dist(int x,int y){
	return dep[x] + dep[y] - 2 * dep[lca(x,y)];
}
void dfs(int u){
	for(int e=first[u];~e;e=edge[e].nxt){
		int v=edge[e].to;
		if(v==fa[u]) continue;
		dep[v] = dep[u] + 1;
		fa[v] = u;
		dfs(v);
	}
}
int h[maxn];
inline void gpath(int x,int y){
	h[0] = dist(x,y)+1;
	int l=1,r = h[0];
	while(x!=y){
		if(dep[x]>dep[y]) h[l++]=x,x=fa[x]; else
						  h[r--]=y,y=fa[y];
	}
	h[l++]=x;
}
inline void move(int allsize,int &res,int x,int y){
	//x是y的父亲 
	if(dep[x] < dep[y]){
		res -= size[y];
		res += allsize - size[y];
	} else{
	//往爸爸那边爬
		res += size[x];
		res -= allsize - size[x];
	}
}
inline void merge(int tt){
//	debug(tt); 
	int u,v,res;
	int x = X[tt],y = Y[tt];
	if(dep[x] < dep[y]) swap(x,y);//y是父亲 
	mark[x] = true;
	int topy = y,topx = x;
	while(mark[topy]) topy = fa[topy];
	u = root[topx],v = root[topy];
	res = f[v];
	gpath(v,y);
	Rep(i,2,h[0]) move(size[topy],res,h[i-1],h[i]);
	res += size[topy];
	gpath(u,x);
	res += f[u];
	Rep(i,2,h[0]) move(size[topx],res,h[i-1],h[i]);
	int i = y;
	while(true){
		size[i] += size[x];
		if(mark[i]) i = fa[i]; else break;
	}
	Dep(i,h[0]-1,1) move(size[topy],res,h[i+1],h[i]);	
	gpath(u,v);
	root[topy] = 0;
	Rep(i,2,h[0]){
		int last = res;
		move(size[topy],res,h[i-1],h[i]);
		if(res > last){
			root[topy] = h[i-1];
			f[h[i-1]] = last;
			break;
		}
	}
	if(!root[topy]){
		root[topy] = h[h[0]];
		f[h[h[0]]] = res;
	}
}//i表示第几条边 
inline int query(int x){
	int topx = x;
	while(mark[topx]) topx=fa[topx];
	int u = root[topx];
	return dist(u,x);
}
bool del[maxn];
int qX[maxn],qY[maxn],ans[maxn];
int main(){
	freopen("dislct.in","r",stdin); 
	freopen("dislct.out","w",stdout); 
	n = rd(),m = rd();
	mem(first,-1);nume = 0;
	Rep(i,1,n-1){
		X[i] = rd(),Y[i] = rd();
		del[i] = false;
		Addedge(X[i],Y[i]);
		Addedge(Y[i],X[i]);
	}
	fa[1] = 0;dfs(1);
	Rep(i,1,n) size[i] = i,root[i] = i,f[i] = 0;
	Rep(i,1,m){
		qX[i] = rd(),qY[i] = rd();
		if(qX[i] == 1) del[qY[i]] = true;
	}
	Rep(i,1,n) size[i] = 1;
	mem(mark,false);//mark表示一个点到父亲的边是否连上 
	Rep(i,1,n-1){
		if(!del[i]) merge(i);//把边加上，变成联通块，同时维护root 
	}
	Dep(i,m,1){
		if(qX[i] == 1){
			merge(qY[i]);
		} else{
//			debug(qX[i]);
			ans[i] = query(qY[i]);
		}
	}
	Rep(i,1,m){
		if(qX[i] == 2) writeln(ans[i]);
	}
	return 0;
}
/*
7 7
2 1
3 1
1 4
1 5
1 6
3 7
2 6
2 4
1 6
1 4
2 7
2 5
2 3
*/
