#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
namespace IO{
	char c,st[66];int tp=0,f;
	#define gc getchar
	#define pc putchar
	inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
}using IO::read;using IO::write;using IO::writeln;using IO::wri;
#define rd read
const int mod = 23333;
const ll mod2 = 1ll * mod * mod * mod * mod;
const int maxn = 303;
int n,m;
ll f[maxn],F[maxn],G[maxn],tmp[maxn];
inline void add(ll &x,ll v){
	x+=v;
	if(x >= mod2) x-= mod2;
}
ll res[maxn];
void mul(ll f[],ll a[],ll b[],int n){
	if(n==1){
		f[0] = a[0] * b[0] % mod;
		return ;
	}
	l=(n+1)/2;
	int t1[n],t2[n],t3[n];
	mul(t1,a,b,l);
	mul(t2,a+l,b+l,n-l);
	rep(i,0,l) if(i+l<n) c[i] = a[i] + a[i+l]; else c[i]=a[i];
	rep(i,0,l) if(i+l<n) d[i] = b[i] + b[i+l]; else d[i]=b[i];
	mul(t3,c,d,l);
	
}
void times(ll f[],ll a[],ll b[],int n){
	rep(i,0,n) res[i] = 0;
	rep(i,0,n) rep(j,0,n)if(i+j<n) add(res[i+j],a[i]*b[j]);
	rep(i,0,n) f[i] = res[i] % mod;
}
int main(){
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	n = rd(),m = rd();
	f[0] = 1;
	Rep(i,1,n){
		Rep(j,0,i-1) F[j] = 0;F[0] = 1;
		Rep(j,0,i-1) tmp[j] = f[j];
		for(int w=m;w;w>>=1,times(tmp,tmp,tmp,i)){
			if(w&1) times(F,F,tmp,i);
		}
		f[i] = F[i-1] % mod;
		if(i==n)printf("%lld\n",(f[i]+1)%mod);
	}
	return 0;
}

