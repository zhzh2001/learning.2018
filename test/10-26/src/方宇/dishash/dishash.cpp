#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 23333
#define N 1010
using namespace std;
LL n,m,i,j,k,l,r,ans;
LL c[N][N],son[N][N],fa[N];
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
void dfs(LL k,LL l,LL r)
{
	if(k==n+1){ans=(ans+1)%fy; return;}
	rep(i,r+1,m) dfs(k+1,l,i);
	rep(i,l+1,k-1)
	  rep(j,1,m) dfs(k+1,i,j);
}
int main()
{
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	n=read(); m=read();
	if(m==2)
	{
		c[0][0]=1;
		rep(i,1,2*n)
		{
			c[i][0]=1;
			rep(j,1,n) c[i][j]=(c[i-1][j-1]+c[i-1][j])%fy;
		}
		ans=(c[2*n][n]-c[2*n][n-1]+1+fy)%fy;
		printf("%lld",ans);
		return 0;
	}
	dfs(2,1,0); ans=(ans+1)%fy;
	printf("%lld",ans);
	return 0;
}
