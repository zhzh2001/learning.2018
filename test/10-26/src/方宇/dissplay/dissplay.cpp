#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 1000010
using namespace std;
struct node
{
	LL l,r,num;
}tree[N];
LL a[N];
LL n,m,i,j,k,l,r,h,num,sum,s;
char ch[20];
void build(LL k,LL l,LL r)
{
	tree[k].l=l; tree[k].r=r;
	if(l==r) return;
	LL mid=(l+r)>>1;
	build(k*2,l,mid); build(k*2+1,mid+1,r);
}
void add(LL k,LL m,LL v)
{
	tree[k].num+=m*v; 
	if(tree[k].l==tree[k].r) return;
	LL mid=(tree[k].l+tree[k].r)>>1;
	if(m<=mid) add(k*2,m,v);
	else add(k*2+1,m,v);
}
LL query(LL k,LL l,LL r)
{
	if(tree[k].l==l&&tree[k].r==r) return tree[k].num;
	LL mid=(tree[k].l+tree[k].r)>>1;
	if(r<=mid) return query(k*2,l,r);
	if(l>mid) return query(k*2+1,l,r);
	return query(k*2,l,mid)+query(k*2+1,mid+1,r);
}
int main()
{
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	scanf("%lld%lld%lld",&n,&m,&k);
	build(1,1,max(m,k));
	rep(i,1,n)
	{
		scanf("%lld%lld",&m,&k);
		if(m!=0) add(1,m,k),s+=k;
	}
	scanf("%lld",&m);
	rep(i,1,m)
	{
		scanf("%s",ch+1);
		if(ch[1]=='a')
		{
			scanf("%lld",&k);
			add(1,k,1); s++;
		}
		if(ch[1]=='b')
		{
			scanf("%lld",&k);
			add(1,k,-1); s--;
		}
		if(ch[1]=='c')
		{
			scanf("%lld%lld",&l,&r);
			add(1,l,-1); 
			if(l==r) s--;
			else add(1,l-r,1);
		}
		if(ch[1]=='e')
		{
			scanf("%lld%lld",&l,&r);
			add(1,l,-1); add(1,l+r,1);
		}
		if(ch[1]=='q')
		{
			scanf("%lld",&k);
			h=trunc(sqrt(k)); num=h*2-(h==k/h); sum=s*k;
			rep(j,1,h) a[j]=j,a[num-j+1]=k/j; a[num+1]=fy;
			rep(j,1,num) 
			  sum-=a[j]*query(1,k/a[j+1]+1,k/a[j]);
			printf("%lld\n",sum);
		}
	}
	return 0;
}
