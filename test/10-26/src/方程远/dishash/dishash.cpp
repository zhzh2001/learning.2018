#include<bits/stdc++.h>
#define int long long
using namespace std;
int n,m;
int a[101];
int C(int x,int y){
	if(x==y)
		return 1;
	int sum=1;
	for(int i=x;i>y;i--)
		sum*=i;
	for(int i=1;i<=abs(x-y);i++)
		sum/=i;
	return sum;
}
int search(int last,int rest){
	if(!rest)
		return 1;
	int sum=0;
	for(int i=1;i<=min(m*last,rest);i++)
		sum+=C(m*last,i)*search(i,rest-i);
	return sum;
}
signed main(){
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if(m==2){
		a[0]=a[1]=1;
		a[2]=2;
		for(int i=3;i<=n;i++)
			for(int j=0;j<i;j++)
				a[i]+=a[j]*a[i-j-1];
		return printf("%lld",a[n]+1),0;
	}
	printf("%lld",search(1,n-1)+1);
	return 0;
}
