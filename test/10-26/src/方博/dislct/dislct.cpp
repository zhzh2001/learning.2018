#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=300005;
int f[N];
int vis[N];
int vist[N];
int sz[N],len[N],dep[N];
int head[N],nxt[2*N],tail[2*N],can[2*N];
int n,m,x,y,t;
inline void addto(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
void dfs(int k,int fa,int r)
{
	vist[k]=r;
	f[k]=fa;
	sz[k]=1;
	len[k]=0;
	for(int i=head[k];i;i=nxt[i]){
		if(can[i])continue;
		int v=tail[i];
		if(v==fa)continue;
		dep[v]=dep[k]+1;
		dfs(v,k,r);
		sz[k]+=sz[v];
		len[k]=len[v]+sz[v];
	}
}
void dfs2(int k,int fa,int r)
{
	for(int i=head[k];i;i=nxt[i]){
		if(can[i])continue;
		int v=tail[i];
		if(v==fa)continue;
		len[v]+=len[k]-len[v]+sz[r]-sz[v];
		dfs2(v,k,r);
	}
}
int gf(int x,int now)
{
	dfs(x,x,x);
	vis[x]=now;
	dfs2(x,x,x);
	for(int i=1;i<=n;i++)
		if(i!=x&&vist[i]==x){
			vis[i]=now;
		}
	int k=1;
	for(int i=2;i<=n;i++){
		if(len[i]<len[k])k=i;
	}
	dep[k]=0;
	dfs(k,k,k);
	for(int i=1;i<=n;i++)
		if(i!=k&&vist[i]==k){
			len[i]+=sz[k]-sz[i];
			vis[i]=now;
		}
	return k;
}
int main()
{
	freopen("dislct.in","r",stdin);
	freopen("dislct.our","w",stdout);
	n=read();m=read();
	for(int i=1;i<n;i++){
		x=read(),y=read();
		addto(x,y);
		addto(y,x);
	}
	memset(vis,-1,sizeof(vis));
	int now=0;
	for(int i=1;i<=m;i++){
		x=read();y=read();
		if(x==1){
			can[y*2-1]=1;
			can[y*2]=1;
			now=i;
		}else{
			if(vis[y]==now)writeln(dep[y]);
			else{
				gf(y,now);
				writeln(dep[y]);
			}
		}
	}
	return 0;
}
