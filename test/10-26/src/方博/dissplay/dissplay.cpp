#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=1e5+5;
/*struct tree{
	int t,l,r;
	ll s,c;
	int sz;
}t[N*4];
void pushdown(int k)
{
	if(t[k].c!=0){
		t[k<<1].c+=t[k].c;
		t[k<<1|1].c+=t[k].c;
		t[k<<1].s+=t[k].c*t[k<<1].sz;
		t[k<<1|1].s+=t[k].c*t[k<<1|1].sz;
		t[k].c=0;
	}
}
void build(int l,int r,int k)
{
	t[k].l=l,t[k].r=r;t[k].sz=r-l+1;
	if(l==r){
		t[k].s=0;
		return;
	}
	int mid=(l+r)>>1;
	build(l,mid,k<<1);
	build(mid+1,r,k<<1|1);
	t[k].s=t[k<<1].s+t[k<<1|1].s;
}
void change(int l,int r,int k,ll c)
{
//	cout<<t[k].l<<' '<<t[k].r<<' '<<l<<' '<<r<<endl;
	pushdown(k);
	if(l<=t[k].l&&t[k].r<=r){
		t[k].c+=c;
		t[k].s+=c*t[k].sz;
		return;
	}
	int mid=(t[k].l+t[k].r)>>1;
//	cout<<mid<<' '<<l<<' '<<r<<endl;
	if(l<=mid)change(l,r,k<<1,c);
	if(r>mid)change(l,r,k<<1|1,c);
	t[k].s=t[k<<1].s+t[k<<1|1].s;
}
ll query(int l,int r,int k)
{
	pushdown(k);
	if(l<=t[k].l&&t[k].r<=r){
		return t[k].s;
	}
	int mid=(t[k].l+t[k].r)>>1;
	ll ret=0;
	if(l<=mid)ret+=query(l,r,k<<1);
	if(r>mid)ret+=query(l,r,k<<1|1);
	t[k].s=t[k<<1].s+t[k<<1|1].s;
	return ret;
}*/
ll f[N];
int n,lim,l,m;
ll sum;
string s,st[N];
ll x,y;
struct xxx{
	int x,y;
}a[N];
int nxt[N];
int g[N];
void add(ll x,ll v){
	for(ll i=x;i<=l;i+=i&-i)f[i]+=v;
}
ll get(ll x){
	ll num=0;
	for(ll i=x;i;i-=i&-i)num+=f[i];
	return num;
}
inline void solve(int x,int c)
{
	for(int k=x;k<=l;k+=x){
		if(nxt[k]>k+x-1)
			k=nxt[k]/k*k;
		add(k,k*c);add(k+x,-k*c);
	}
}
int main()
{
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n=read();lim=read();l=read();
//	build(1,l,1);
	for(int i=1;i<=n;i++){
		x=read();y=read();
		sum+=y;
		if(x!=1){
			solve(x,y);
//			for(int k=x;k<=l;k+=x)
//				change(k,k+x-1,1,k*y);
		}else sum-=y;
//		for(int i=1;i<=l;i++)
//			cout<<query(i,i,1)<<' ';
//		cout<<endl;
//		cout<<sum<<endl;
	}
	m=read();
	for(int i=1;i<=m;i++){
		cin>>st[i];
		a[i].x=read();
		if(st[i][0]=='c'||st[i][0]=='e')a[i].y=read();
		if(st[i][0]=='q')g[++g[0]]=a[i].x;
	}
	sort(g+1,g+g[0]+1);
	int lz=1;
	for(int i=1;i<=n;i++){
		while(g[lz]<i&&lz<=g[0])lz++;
		nxt[i]=g[lz];
	}
	for(int i=1;i<=m;i++){
//		for(int i=1;i<=l;i++)
//			cout<<get(i)<<' ';
//		cout<<sum<<endl;
		s=st[i];x=a[i].x,y=a[i].y;
		if(s[0]=='a'){
			sum++;
			if(x!=1){
				solve(x,1);
//				for(int k=x;k<=l;k+=x)
//					change(k,k+x-1,1,k);
			}else sum--;
		}
		if(s[0]=='b'){
			sum--;
			if(x!=1){
//				for(int k=x;k<=l;k+=x)
//					change(k,k+x-1,1,-k);
				solve(x,-1);
			}else sum++;
		}
		if(s[0]=='c'){
			if(y==0)continue;
			if(x!=1){
				solve(x,-1);
			}else sum++;
			x-=y;
			if(x!=1){
				solve(x,1);
//				for(int k=x;k<=l;k+=x)
//					change(k,k+x-1,1,k);//,cout<<x<<' '<<k+x<<' '<<k<<endl;
			}else sum--;
		}
		if(s[0]=='e'){
//			x=read(),y=read();
			if(y==0)continue;
			if(x!=1){
				solve(x,-1);
//				for(int k=x;k<=l;k+=x)
//					change(k,k+x-1,1,-k);
			}else sum++;
			x+=y;
			if(x!=1){
				solve(x,1);
//				for(int k=x;k<=l;k+=x)
//					change(k,k+x-1,1,k);
			}else sum--;
		}
		if(s[0]=='q'){
//			x=read();
			writeln(sum*x-get(x));
		}
	}
	return 0;
}
//调和级数的复杂度是log

 
