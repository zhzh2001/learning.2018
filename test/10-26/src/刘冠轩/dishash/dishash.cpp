#include <bits/stdc++.h>
using namespace std;
inline char gc(){
    static char buf[1<<12],*p1=buf,*p2=buf;
    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<12,stdin),p1==p2)?EOF:*p1++;
}
#define dd c = gc()
inline int read() {
	int x = 0;int dd;bool f = false;
	for(; !isdigit(c); dd)if(c == '-')f = true;
	for(; isdigit(c); dd)x = (x<<1) + (x<<3) + (c^48);
	return (f)?-x:x;
}
#undef dd
const int P=23333;
int n,m,i,f[1005];
void dfs(int x,int y,int sum){
	if(x==m){(f[i]+=sum*f[y])%=P;return;}
	for(int j=0;j<=y;j++)dfs(x+1,y-j,sum*f[j]%P);
}
int main(){
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	n=read();m=read();f[0]=f[1]=1;
	for(i=2;i<=n;i++)dfs(1,i-1,1);
	cout<<(f[n]+1)%P;
	return 0;
}
