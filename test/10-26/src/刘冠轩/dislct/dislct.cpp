#include <bits/stdc++.h>
using namespace std;
inline char gc(){
    static char buf[1<<12],*p1=buf,*p2=buf;
    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<12,stdin),p1==p2)?EOF:*p1++;
}
#define dd c = gc()
inline int read() {
	int x = 0;int dd;bool f = false;
	for(; !isdigit(c); dd)if(c == '-')f = true;
	for(; isdigit(c); dd)x = (x<<1) + (x<<3) + (c^48);
	return (f)?-x:x;
}
#undef dd
const int N=100005;
int n,m,cnt,head[N],sz[N],mx[N],dep[N],u,v,SZ,rt;
struct edge{int to,nt;bool p;}e[N*2];
void add(int u,int v){
	e[++cnt]=(edge){v,head[u],1};
	head[u]=cnt;
}
void dfs(int u,int fa){
	dep[u]=dep[fa]+1;sz[u]=1;mx[u]=0;SZ++;
	for(int i=head[u];i;i=e[i].nt)
		if(e[i].to!=fa&&e[i].p){
			dfs(e[i].to,u);
			sz[u]+=sz[e[i].to];
			mx[u]=max(mx[u],sz[e[i].to]);
		}
}
void ddfs(int u,int fa){
	mx[u]=max(mx[u],SZ-sz[u]);
	if(mx[u]<mx[rt]||(mx[u]==mx[rt]&&u<rt))rt=u;
	for(int i=head[u];i;i=e[i].nt)
		if(e[i].to!=fa&&e[i].p)ddfs(e[i].to,u);
}
int main(){
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<n;i++){
		u=read();v=read();
		add(u,v);add(v,u);
	}
	dep[0]=-1;mx[rt]=0x7fffffff;
	for(int i=1;i<=m;i++){
		u=read();v=read();
		if(u==1)e[v*2].p=e[v*2-1].p=0;
		else{
			SZ=rt=0;
			dfs(v,0);
			ddfs(v,0);
			cout<<dep[rt]<<endl;
		}
	}
	return 0;
}
