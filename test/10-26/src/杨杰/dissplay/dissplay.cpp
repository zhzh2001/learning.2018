#include <cstdio>
using namespace std;
const int N=1e5+7;
#define ll long long
int n,lim,L,Q;
char s[100];
struct tree {
	ll s,ans;
}tr[N<<2];
#define ls (ts<<1)
#define rs (ts<<1|1)
void insert(int ts,int l,int r,int a,int b) {
	tr[ts].s+=b;tr[ts].ans+=a*b;
	if (l==r) return;
	int mid=(l+r)>>1;
	if (a<=mid) insert(ls,l,mid,a,b);
	else insert(rs,mid+1,r,a,b);
}
ll query(int ts,int l,int r,int L,int R) {
	if (l==L && r==R) return tr[ts].ans;
	int mid=(l+r)>>1;
	if (R<=mid) return query(ls,l,mid,L,R);
	else if (L>mid) return query(rs,mid+1,r,L,R);
	else return query(ls,l,mid,L,mid)+query(rs,mid+1,r,mid+1,R);
}
ll ask(int x) {
	ll ans=x*tr[1].s;
	int v,l,r;
	for (int i=1;i<=x;i=r+1) {
		v=x/i;
		l=i;
		if (v==1) r=x;
		else r=x/v;
		ans-=query(1,1,lim,l,r)*v;
	}
	return ans;
} 
int main() {
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	scanf("%d%d%d",&n,&lim,&L);
	for (int i=1,a,b;i<=n;i++) {
		scanf("%d%d",&a,&b);
		insert(1,1,lim,a,b);
	}
	scanf("%d",&Q);
	for (int i=1,x,y;i<=Q;i++) {
		scanf("%s",s);
		if (s[0]=='a') scanf("%d",&x),insert(1,1,lim,x,1);
		else if (s[0]=='b') scanf("%d",&x),insert(1,1,lim,x,-1);
		else if (s[0]=='c') scanf("%d%d",&x,&y),insert(1,1,lim,x,-1),insert(1,1,lim,x-y,1); 
		else if (s[0]=='e') scanf("%d%d",&x,&y),insert(1,1,lim,x,-1),insert(1,1,lim,x+y,1); 
		else scanf("%d",&x),printf("%lld\n",ask(x));
	}
}
