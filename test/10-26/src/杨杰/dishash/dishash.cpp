#include <cstdio>
using namespace std;
const int N=305,P=23333;
int n,m;
int f[N][N],g[N];
//f[i][j]表示i个节点形成j个森林的方案数，g[i]表示用i个节点形成一颗树的方案数 
int main() {
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++) f[0][i]=1;g[0]=1;
	for (int i=1;i<=n;i++) {
		f[i][1]=g[i]=f[i-1][m];
		for (int j=2;j<=m;j++) {
			for (int k=0;k<=i;k++)
				f[i][j]=(f[i][j]+f[k][j-1]*g[i-k]%P)%P;
		}
	}
	printf("%d\n",(g[n]+1)%P);
}
