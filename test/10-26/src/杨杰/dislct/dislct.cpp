#include <cstdio>
#include <algorithm>
using namespace std;
const int N=3e5+7;
struct edge {
	int t,nxt,flg;
}e[N<<1];
int n,m,cnt=1,sum,rt;
int head[N],dep[N],Bs[N],siz[N],F[N],rtsz[N];
void add(int u,int t) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;
}
void getroot(int u,int fa) {
	siz[u]=1;Bs[u]=0;
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==fa || e[i].flg) continue;
		getroot(t,u);
		siz[u]+=siz[t];
		if (siz[t]>Bs[u]) Bs[u]=siz[t];
	}
	Bs[u]=max(Bs[u],sum-siz[u]);
	if (Bs[u]<Bs[rt] || (Bs[u]==Bs[rt] && u<rt)) rt=u;
}
void dfs(int u,int fa,int sz) {
	F[u]=fa;siz[u]=1;dep[u]=dep[fa]+1;rtsz[u]=sz;
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==fa || e[i].flg) continue;
		dfs(t,u,sz);
		siz[u]+=siz[t];
	}
}
int main() {
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1,u,v;i<n;i++) {
		scanf("%d%d",&u,&v);
		add(u,v);add(v,u);
	}
	rt=0;Bs[rt]=1e9;sum=n;getroot(1,0);
	dep[0]=-1;dfs(rt,0,sum);
	for (int i=1,opt,x;i<=m;i++){
		scanf("%d%d",&opt,&x);
		if (opt==1) {
			int u=e[x<<1].t,v=e[x<<1|1].t;
			e[x<<1].flg=1;e[x<<1|1].flg=1;
			if (F[u]==v) swap(u,v);
			rt=0;sum=siz[v];getroot(v,0);
			dfs(rt,0,sum);
			rt=0;sum=rtsz[u]-sum;getroot(u,0);
			dfs(rt,0,sum);
		}
		else printf("%d\n",dep[x]);
	}
} 
