#include<cstdio>
#include<iostream>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=1e5+5;
int n,lim,L;
long long tree[MAXN];
int lowbit(int x){
	return x&-x;
}
void add(int x,int v){
	for(;x<=lim;x+=lowbit(x))tree[x]+=v;
}
long long sum(int x){
	long long ans=0;
	for(;x;x-=lowbit(x))ans+=tree[x];
	return ans;
}
char opt[20];
int cnt[MAXN];
int main(){
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n=read(),lim=read(),L=read();
	for(int i=1;i<=n;i++){
		int a=read(),b=read();
		cnt[a]+=b;
		add(a,b);
	}
	for(int m=read();m;m--){
		scanf("%s",opt);
		int x=read();
		if(opt[0]=='a'){
			cnt[x]++;
			add(x,1);
		}
		if(opt[0]=='b'){
			cnt[x]--;
			add(x,-1);
		}
		if(opt[0]=='c'){
			cnt[x]--,add(x,-1);
			x-=read();
			cnt[x]++,add(x,1);
		}
		if(opt[0]=='e'){
			cnt[x]--,add(x,-1);
			x+=read();
			cnt[x]++,add(x,1);
		}
		if(opt[0]=='q'){
			long long ans=0;
			for(int i=2;i<x;i++)ans+=1LL*cnt[i]*(x%i);
			ans+=1LL*(sum(lim)-sum(x))*x;
			printf("%lld\n",ans);
		}
	}
}
