#include<cstdio>
#include<iostream>
#define int long long
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int P=23333;
const int Max=22005;
int n,m,f[305][305];
int fac[Max],inv[Max];
int C(int n,int m){
	return fac[n]*inv[m]%P*inv[n-m]%P;
}
int pw(int a,int n,int P){
	int ans=1;
	while(n){
		if(n&1)ans=ans*a%P;
		a=a*a%P;
		n>>=1;
	}
	return ans;
}
int dfs(int lst,int rest,int num){
	if(!rest)return num;
	if(f[lst][rest])return f[lst][rest];
	int ans=0;
	for(int i=1;i<=m*lst&&i<=rest;i++){
		int nxt=dfs(i,rest-i,num*C(m*lst,i)%P);
		ans=(ans+nxt)%P;
	}
	return f[lst][rest]=ans;
}
signed main(){
//	freopen("dishash.in","r",stdin);
//	freopen("dishash.out","w",stdout);
	n=read(),m=read();
	fac[0]=1;
	for(int i=1;i<=Max;i++)fac[i]=fac[i-1]*i%P;
	inv[Max]=pw(fac[Max],P-2,P);
	for(int i=Max-1;~i;i--)inv[i]=inv[i+1]*(i+1)%P;
	if(m==2)return printf("%lld",(C(n*2,n)/(n+1)+1)%P),0;
	printf("%lld",dfs(1,n-1,1));
}
