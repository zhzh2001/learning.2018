#include<vector>
#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=5e4+5;
int nedge,head[MAXN];
bool can[MAXN];
struct Edge{
	int to,nxt,id;
}edge[MAXN<<1];
void add(int x,int y,int id){
	edge[++nedge].to=y;
	edge[nedge].id=id;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
int n,m;
int Min,weight[MAXN],siz[MAXN];
void Find(int x,int fa){
	siz[x]=1;
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		if(y==fa||!can[edge[i].id])continue;
		Find(y,x);
		weight[x]=max(weight[x],siz[y]);
		siz[x]+=siz[y];
	}
	if(n-siz[x]>weight[x])weight[x]=n-siz[x];
	if(weight[x]<weight[Min])Min=x;
	if(weight[x]==weight[Min]&&x<Min)Min=x;
}
int d[MAXN];
void dfs(int x,int fa){
	d[x]=d[fa]+1;
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		if(y==fa||!can[edge[i].id])continue;
		dfs(y,x);
	}
}
void FindW(){
	vector<int>rt;
	memset(siz,0,sizeof(siz));
	memset(weight,0,sizeof(weight));
	weight[0]=1e9;
	for(int i=1;i<=n;i++)
		if(!siz[i]){
			Min=0;
			Find(i,0);
			rt.push_back(Min);
		}
	for(int i=0;i<(int)rt.size();i++)dfs(rt[i],0);
}
int main(){
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		add(x,y,i),add(y,x,i);
		can[i]=1;
	}
	d[0]=-1;
	FindW();
	while(m--){
		int opt=read();
		if(opt==1){
			can[read()]=0;
			FindW();
		}else printf("%d\n",d[read()]);
	}
}
