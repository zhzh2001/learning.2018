#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int n,lim,L,f[N],a[N];
inline void sadd(int x,int v){
	for(;x<=lim;x+=x&-x)f[x]+=v;
}
inline int ssum(int x){
	int ans=0;
	for(;x;x-=x&-x)ans+=f[x];
	return ans;
}
signed main()
{
	freopen("dissplay.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();lim=read();L=read();
	for(int i=1;i<=n;i++){
		int x=read();a[x]=read();
		sadd(x,a[x]);
	}
	for(int Q=read();Q;Q--){
		char ch[10];scanf("%s",ch);
		if(ch[0]=='a'){
			int x=read();a[x]++;sadd(x,1);
		}
		if(ch[0]=='b'){
			int x=read();a[x]--;sadd(x,-1);
		}
		if(ch[0]=='c'){
			int x=read(),y=read();
			a[x]--;sadd(x,-1);
			x-=y;x=max(x,0ll);
			a[x]++;sadd(x,1);
		}
		if(ch[0]=='e'){
			int x=read(),y=read();
			a[x]--;sadd(x,-1);
			x+=y;x=min(x,lim);
			a[x]++;sadd(x,1);		
		}
		if(ch[0]=='q'){
			int x=read();
			int ans=x*(ssum(lim)-ssum(x));
			for(int i=1;i<x;i++)ans+=(x%i)*a[i];
			writeln(ans);
		}
	}
	return 0;
}
