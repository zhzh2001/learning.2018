#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int n,lim,L,a[N];
struct szsz{
	int f[N];
	inline void sadd(int x,int v){
		if(!x)return;
		for(;x<=lim;x+=x&-x)f[x]+=v;
	}
	inline int ssum(int x){
		int ans=0;
		for(;x;x-=x&-x)ans+=f[x];
		return ans;
	}
}f1,f2;
signed main()
{
	freopen("dissplay.in","r",stdin);
	freopen("dissplay.out","w",stdout);
	n=read();lim=read();L=read();
	for(int i=1;i<=n;i++){
		int x=read();a[x]=read();
		f1.sadd(x,a[x]);
		f2.sadd(x,x*a[x]);
	}
	for(int Q=read();Q;Q--){
		char ch[10];scanf("%s",ch);
		if(ch[0]=='a'){
			int x=read();a[x]++;
			f1.sadd(x,1);f2.sadd(x,x);
		}
		if(ch[0]=='b'){
			int x=read();a[x]--;
			f1.sadd(x,-1);f2.sadd(x,-x);
		}
		if(ch[0]=='c'){
			int x=read(),y=read();
			a[x]--;f1.sadd(x,-1);f2.sadd(x,-x);
			x-=y;x=max(x,0ll);
			a[x]++;f1.sadd(x,1);f2.sadd(x,x);
		}
		if(ch[0]=='e'){
			int x=read(),y=read();
			a[x]--;f1.sadd(x,-1);f2.sadd(x,-x);
			x+=y;x=min(x,lim);
			a[x]++;f1.sadd(x,1);f2.sadd(x,x);
		}
		if(ch[0]=='q'){
			int x=read();
			int ans=x*f1.ssum(lim);
			int p=1;
			while(p<=x){
				int q=x/p,qq=x/q;
				ans-=q*(f2.ssum(qq)-f2.ssum(p-1));
				p=qq+1;
			}
			writeln(ans);
		}
	}
	return 0;
}
