#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int MOD=23333,N=1010;
int n,m,f[N][N],ans=0;
int jie[1000010],inv[1000010];
inline int mi(int a,int b){
	int x=1,y=a;
	for(;b;b>>=1,y=y*y%MOD)if(b&1)x=x*y%MOD;
	return x;
}
inline int C(int x,int y){
	return jie[x]*inv[y]%MOD*inv[x-y]%MOD;
}
signed main()
{
	freopen("dishash.in","r",stdin);
	freopen("dishash.out","w",stdout);
	n=read();m=read();
	if(m>=MOD)m=MOD-1;
	jie[0]=1;
	for(int i=1;i<MOD;i++)jie[i]=jie[i-1]*i%MOD;
	inv[MOD-1]=mi(jie[MOD-1],MOD-2);
	for(int i=MOD-2;i>=0;i--)inv[i]=inv[i+1]*(i+1)%MOD;
	f[1][1]=1;int ans=0;
	for(int i=2;i<=n;i++)
		for(int j=1;j<min(i,n);j++){
			f[i][j]=0;
			for(int k=(j-1)/m+1;k<=min(i-j,n);k++){
				f[i][j]=(f[i][j]+f[i-j][k]*C(k*m,j)%MOD)%MOD;
			}
		}
	for(int j=1;j<=n;j++)ans=(ans+f[n][j])%MOD;
	ans=(ans+1)%MOD;
	writeln(ans);
	return 0;
}
