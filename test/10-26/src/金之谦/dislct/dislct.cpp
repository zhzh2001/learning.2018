#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=3e5+10;
int nedge=0,p[2*N],nex[2*N],head[2*N],c[2*N];
inline void addedge(int a,int b,int v){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
	c[nedge]=v;
}
struct ppap{int x,y;}a[N];
int b[N],dep[N],s[N],st[N],n,m,sp=0,rt=0;
vector<int>e;
inline void dfs(int x,int fa){
	s[x]=1;st[x]=0;sp++;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa&&!b[c[k]]){
		dfs(p[k],x);
		st[x]+=st[p[k]]+s[p[k]];
		s[x]+=s[p[k]];
	}
}
inline void dfss(int x,int fa){
	e.push_back(x);
	if(fa)st[x]=st[fa]-s[x]+(sp-s[x]);
	if(!rt||st[x]<st[rt]||(st[x]==st[rt]&&x<rt))rt=x;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa&&!b[c[k]])dfss(p[k],x);
}
inline void dfsss(int x,int fa){
	dep[x]=dep[fa]+1;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa&&!b[c[k]])dfsss(p[k],x);
}
int main()
{
	freopen("dislct.in","r",stdin);
	freopen("dislct.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<n;i++){
		int x,y;
		a[i].x=x=read();a[i].y=y=read();
		addedge(x,y,i);addedge(y,x,i);
	}
	dep[0]=-1;
	dfs(1,0);dfss(1,0);
	dfsss(rt,0);
	for(int i=1;i<=m;i++){
		int op=read(),x=read();
		if(op==1){
			b[x]=1;
			sp=0;dfs(a[x].x,0);
			rt=0;dfss(a[x].x,0);
			dfsss(rt,0);
			sp=0;dfs(a[x].y,0);
			rt=0;dfss(a[x].y,0);
			dfsss(rt,0);
		}else writeln(dep[x]);
	}
	return 0;
}
