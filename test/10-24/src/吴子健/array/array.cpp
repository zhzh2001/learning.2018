#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
//defs========================
int N,M;
char a[19][100010];
int cnt[100010][2];
int ans=1e9;
//main========================
int main() {
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	scanf("%d%d",&N,&M);
	for(int i=1;i<=N;++i)
		scanf("%s",a[i]+1);
	for(int i=1;i<=N;++i) for(int j=1;j<=M;++j) {
		a[i][j]-='0';
		cnt[j][a[i][j]]++;
	}
//	for(int i=1;i<=N;++i) {
//		for(int j=1;j<=M;++j) printf("%d ",int(a[i][j]));
//		puts("");
//	}
//	for(int j=1;j<=M;++j)
//		printf("0s=%d 1s=%d\n",cnt[j][0],cnt[j][1]);
	for(int i=0;i<(1<<N);++i) {
		for(int j=1;j<=N;++j) if((1<<(j-1))&i) {
			for(int k=1;k<=M;++k) {
				--cnt[k][a[j][k]],++cnt[k][a[j][k]^1];
			}
		}
		int ret=0;
		for(int k=1;k<=M;++k) ret+=min(cnt[k][0],cnt[k][1]);
		ans=min(ans,ret);
		for(int j=1;j<=N;++j) if((1<<(j-1))&i) {
			for(int k=1;k<=M;++k) {
				++cnt[k][a[j][k]],--cnt[k][a[j][k]^1];					
			} 	
		}
	}
	cout<<ans<<endl;
	return 0;
}

