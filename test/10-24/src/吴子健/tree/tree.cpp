#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
namespace Fast_IO{
	const int L=1000000;
	char LZH[L];
	char *SSS,*TTT;
	inline char gc(){
		if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
		return *SSS++;
	}
	inline int rd(){
		int x=0;
		char c=gc();
		for (;c<'0'||c>'9';c=gc());
		for (;c>='0'&&c<='9';c=gc()) x=(x<<1)+(x<<3)+c-48;
		return x;
	}
}
const int MAXN=1e6+10;
using Fast_IO::rd;
int N,Q;
struct edge {
	int ed,nxt,id;
}E[MAXN];
int head[MAXN],Ecnt=1;
void addEdge(int st,int ed,int id) {
	E[++Ecnt].ed=ed,E[Ecnt].nxt=head[st],head[st]=Ecnt;
	E[Ecnt].id=id;
}
void do_1();
void do_2();
void dfs_circle(int st,int fa);
int S[MAXN],top;
int ins[MAXN],vis[MAXN];
int cir[MAXN],rid[MAXN],ccnt;
int vai[MAXN];
//main========================
int main() {
	freopen("tree.in","r",stdin);
	exit(0);
	N=rd(),Q=rd();
	for(int i=1;i<=N;++i) {
		int x=rd(),y=rd();
		addEdge(x,y,i),addEdge(y,x,i);
	}
	if(N<=1500) {
		do_1();
	} else do_2();
	return 0;
}
int pos1,pos2;

void do_1() {
	dfs_circle(1,0);
	for(int i=1;i<=ccnt;++i) LOG("%d ",cir[i]);LOG("  \t%d\n",0);
	while(Q--) {
		int x=rd(),y=rd();
		memset(vis,0,(sizeof (int))*N);
		pos1=pos2=0;
	}
}
void dfs_circle(int st,int fa) {
	S[++top]=st;
	ins[st]=vis[st]=true;
	for(int i=head[st];i;i=E[i].nxt) {
		int ed=E[i].ed;
		if(ed!=fa) {
			if(!vis[ed]) {
				dfs_circle(ed,st);
			} else if(ins[ed]) {
				do{
					cir[++ccnt]=S[top];
					rid[S[top]]=ccnt;
				}while(S[top--]!=ed);
			}
		}
	}
	ins[st]=false;
	--top;
}
void do_2() {
	
}
