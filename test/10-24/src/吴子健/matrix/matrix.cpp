#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define re register int
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
#define maxx(a,b) ((a)>(b)?(a):(b))
using namespace std;
namespace Fast_IO{
	const int L=1000000;
	char LZH[L];
	char *SSS,*TTT;
	inline char gc(){
		if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
		return *SSS++;
	}
	inline int rd(){
		int x=0;
		char c=gc();
		for (;c<'0'||c>'9';c=gc());
		for (;c>='0'&&c<='9';c=gc()) x=(x<<1)+(x<<3)+c-48;
		return x;
	}
}
using Fast_IO::rd;
//defs========================
ll tot_tri_num,tri_per_pos;
int N;
int a[3000][3000];
bool used[3010][3010];
int l[3000*3000],lcnt;
struct node {
	int ed,nxt;
}E[3000*3000];
int head[3000*3000],Ecnt;
ll cnt[3000*3000];
int lis_todo[3000*3000],liscnt;
void addNode(int val,int ed) {
	E[++Ecnt].ed=ed,E[Ecnt].nxt=head[val];
	head[val]=Ecnt;
	++cnt[val];
	if(cnt[val]==1) lis_todo[++liscnt]=val;//加入新出现的颜色 
}
ll num[3010];
ll ano[3010];
vector<int> illegal;
void get_3();
//main========================
ll get_zhong() {
	ll number=num[1]+ano[1],tim=1;
	for(int i=2;i<=N;++i) {
		if(tim) {
			if(num[i]+ano[i]==number) ++tim;
			else --tim;			
		}
		else number=num[i]+ano[i],tim=1;
	}
	return number;
}
int main() {
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	N=rd();
	tot_tri_num=(ll)N*(N-1)*(N-2)/3/2;
	tri_per_pos=(ll)(N-1)*(N-2)/2;
	for(re i=1;i<=N;++i) for(re j=1;j<=N;++j) a[i][j]=rd(),l[++lcnt]=a[i][j];
	sort(l+1,l+lcnt+1);
	lcnt=unique(l+1,l+lcnt+1)-l-1;
	for(re i=1;i<=N;++i) for(re j=1;j<=N;++j) a[i][j]=lower_bound(l+1,l+lcnt+1,a[i][j])-l-1;
	for(re i=1;i<=N;++i) if(a[i][i]) {
		printf("1\n%d 0 0\n",i);
		return 0;
	}
	for(re i=1;i<=N;++i) for(re j=1;j<=N;++j) if(a[i][j]!=a[j][i]) {
		printf("2\n%d %d 0\n",i,j);
		return 0;		
	} 
	
	if(N<=1500) {
		for(re i=1;i<=N;++i) for(re j=i+1;j<=N;++j) for(re k=1;k<=N;++k) {
			if(a[i][j]>maxx(a[i][k],a[k][j])) {
				printf("3\n%d %d %d\n",i,j,k);
				exit(0);
			}
		}
	} else get_3();
	puts("0");
	return 0;
}

void get_3() {
	for(re i=1;i<=N;++i) {
		Ecnt=0;
	//	LOG("added %d\n",i);
		for(re j=1;j<=N;++j) if(j!=i) {
			addNode(a[i][j],j);
			//used[i][j]=used[j][i]=true;
		}
		//LOG("added %d\n",i);
		for(int j=1;j<=liscnt;++j) {//枚举每一种颜色 

			int st=lis_todo[j];
		//LOG("Meet col=%d num=%d\n",st,cnt[st]);
			num[i]+=(ll)cnt[st]*(cnt[st]-1)/2;//顶点的贡献 
			for(int k=head[st];k;k=E[k].nxt) {//枚举可以作为底点的节点 
				int ed=E[k].ed;
				ano[ed]+=(cnt[st]-1);//底点的贡献 
			}
		}
		for(int j=1;j<=liscnt;++j) head[lis_todo[j]]=cnt[lis_todo[j]]=0;//清空 
		Ecnt=liscnt=0;
	}
	//LOG("%d\n",tri_per_pos);
	ll zhong=get_zhong();
//	cerr<<ano[884]+num[884]<<endl;
	for(int i=1;i<=N;++i) if(abs(ano[i]+num[i]-zhong)>2) /*LOG("tot[%d]=%lld \n",i,ano[i]+num[i]),*/illegal.push_back(i);
	if(illegal.size()) {
		puts("3");
		int m=illegal[0],n=illegal[1],p=illegal[2];
		if(a[m][n]>a[n][p]&&a[m][n]>a[m][p]) {
			printf("%d %d %d\n",m,n,p);
		} else if(a[m][p]>a[m][n]&&a[m][p]>a[n][p]) {
			printf("%d %d %d\n",m,p,n);
		} else printf("%d %d %d\n",n,p,m);
		exit(0);
	}
}

