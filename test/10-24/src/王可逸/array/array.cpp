#include <bits/stdc++.h>
using namespace std;
const int L=1000000;
int n,m,a[20][120000],b[20][120000],one,tim,ans,ztn;
char ch[120000];
int subtask1() {
	int ans=one;
	while(1) {
		bool flag=true;
		for(int i=1;i<=n;i++) {
			int sum=0;
			for(int j=1;j<=m;j++) sum+=(a[i][j]==1?1:0);
			if(sum<(m-sum)) continue;
			else if(sum>(m-sum)) {
				flag=false;
				ans-=(sum-(m-sum));
				for(int j=1;j<=m;j++) a[i][j]^=1;
			} else for(int j=1;j<=m;j++) a[i][j]^=1;	
		}
		for(int j=1;j<=m;j++) {
			int sum=0;
			for(int i=1;i<=n;i++) sum+=(a[i][j]==1?1:0);
			if(sum<(n-sum)) continue;
			else if(sum>(n-sum)) {
				flag=false;
				ans-=(sum-(n-sum));
				for(int i=1;i<=n;i++) a[i][j]^=1;
			} else for(int i=1;i<=n;i++) a[i][j]^=1;
		}
		if(flag) break;
	}
	return ans;
}
void subtask2() {
	for(int i=0;i<(1<<n);i++) {
		if(clock()-tim>1500) {
			cout<<ans;
			exit(0);	
		}
		int now=0;
		for(int k=1;k<=n;k++) for(int j=1;j<=m;j++) a[k][j]=b[k][j];
		for(int j=1;j<n;j++) {
			if((i%(1<<(j+1)))/(1<<(j))==1) {
				for(int k=1;k<=m;k++) a[k][j]^=1;
			}
		}
		for(int j=1;j<=m;j++) {
			int sum=0;
			for(int k=1;k<=n;k++) if(a[i][j]==1) sum++;
			if(sum>(n-sum)) now+=sum-(n-sum);		
		}
		ans=min(ans,now);
	}
}
signed main() {
	srand((int)time(0));
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	int tim=clock();
	scanf("%lld%lld",&n,&m);
	getchar();
	for(int i=1;i<=n;i++) {
		scanf("%s",ch+1);
		for(int j=1;j<=strlen(ch+1);j++) {
			a[i][j]=b[i][j]=ch[j]-'0';
			if(a[i][j]&1) one++;
		}
	}
	ans=one;
	ans=min(ans,subtask1());
//	subtask2();
	cout<<ans;
	return 0;
}
