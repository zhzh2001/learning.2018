#include <bits/stdc++.h>
using namespace std;
const int N=2100;
//#define gc getchar
//inline int read(){
//	register int x=0;
//	register char c=gc();
//	for (;c<'0'||c>'9';c=gc());
//	for (;c>='0'&&c<='9';c=gc())
//		x=(x<<1)+(x<<3)+c-48;
//	return x;
//}
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
void write(int x) {
	if(x>=10) write(x/10);
	char ch=x%10+'0';
	putchar(ch);
}
struct node {int next,to;} sxd[N];
int n,qq,cnt,have[1200][1200],head[N],dis[N],pre[N],x,y,s,t;
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	head[u]=cnt;
}
struct pfy {
	int num,at;
	friend bool operator < (pfy a,pfy b) {if(a.num!=b.num) return a.num>b.num;else return a.at>b.at;}	
};
priority_queue <pfy> q;
bool vis[1200];
inline void dij(int x,int tt) {
	memset(vis,0,sizeof vis);
	memset(dis,0x3f,sizeof dis);
	memset(pre,0,sizeof pre);
	dis[x]=0;
	q.push((pfy) {0,x});
	while(!q.empty()) {
		pfy now=q.top();
		q.pop();
		vis[now.at]=true;
		for(register int j=head[now.at];j;j=sxd[j].next) {
			int i=sxd[j].to;
			if(dis[i]>dis[now.at]+1) {
				dis[i]=dis[now.at]+1;
				pre[i]=now.at;
				if(!vis[i]) q.push((pfy) {dis[i],i});
 			} 
		}	
	}	
	L1:;
}
inline void dfs(int at) {
	vis[at]=true;
	for(register int i=1;i<=n;i++) {
		if(have[at][i] && !vis[i])	dfs(i);
	}
}
inline int calc(int s,int t) {
	dij(s,t);
	register int now=t;
	while(now!=s) {
		have[pre[now]][now]^=1;
		have[now][pre[now]]^=1;
		now=pre[now];
	}
	memset(vis,0,sizeof vis);
	register int lian=0;
	for(register int i=1;i<=n;i++)
		if(!vis[i])	lian++,dfs(i);
	return lian;
}
signed main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);	
	n=read(),qq=read();
	for(register int i=1;i<=n;i++) {
		x=read(),y=read();
		add(x,y);
		add(y,x);
	}
	while(qq--) {
		s=read(),t=read();
		write(calc(s,t));
		puts("");
	}
	return 0;	
}
