#include<iostream>
#include<algorithm>
#include<fstream>
#include<math.h>
#include<vector>
#include<queue>
#include<stdio.h>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(long long x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
struct ppap{
	int x,y,v;
}a[100003];
struct qqaq{
	int x,k;
};
inline bool operator <(qqaq a,qqaq b){
	if(a.x==b.x){
		return a.k<b.k;
	}
	return a.x<b.x;
}
vector<qqaq> e[100003];
qqaq zy[1003][1003];
int n,m;
queue<int> q;
inline void bfs(int x){
	q.push(x);
	zy[x][x].x=-1;
	while(!q.empty()){
		int now=q.front();q.pop();
		for(int i=0;i<e[now].size();i++){
			int to=e[now][i].x;
			if(!zy[x][to].x){
				zy[x][to]=(qqaq){now,e[now][i].k};
				q.push(to);
			}
		}
	}
}
int fa[100003];
inline int find(int x){
	if(fa[x]==x){
		return x;
	}
	return fa[x]=find(fa[x]);
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=n;i++){
		int x=read(),y=read();
		e[x].push_back((qqaq){y,i});
		e[y].push_back((qqaq){x,i});
		a[i].x=x;a[i].y=y;a[i].v=0;		
	}
	for(int i=1;i<=n;i++){
		sort(e[i].begin(),e[i].end());
	}
	for(int i=1;i<=n;i++){
		bfs(i);
	}
	for(int T=1;T<=m;T++){
		int x=read(),y=read();
		while(y!=x){
			qqaq kp=zy[x][y];
			a[kp.k].v^=1;
			y=kp.x;
		}
		for(int i=1;i<=n;i++){
			fa[i]=i;
		}
		for(int i=1;i<=n;i++){
			if(a[i].v){
				int fx=find(a[i].x),fy=find(a[i].y);
				if(fx!=fy){
					fa[fx]=fy;
				}
			}
		}
		int ans=0;
		for(int i=1;i<=n;i++){
			if(fa[i]==i){
				ans++;
			}
		}
		writeln(ans);
	}
	return 0;
}
