#include<iostream>
#include<algorithm>
#include<fstream>
#include<math.h>
#include<stdio.h>
#include<string.h>
using namespace std;
ifstream fin("matrix.in");
ofstream fout("matrix.out");
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(long long x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
inline int max(int x,int y){
	return x>y?x:y;
}
int dis[2503];
int a[2503][2503];
int head[2503],to[5003],nxt[5003],e[5003],from[2503];
int f[2503],t,n;
inline void add(int x,int y,int z){
	nxt[++t]=head[x],head[x]=t,to[t]=y,e[t]=z;
}
void dfs(int k,int fa){
	for(register int i=head[k];i;i=nxt[i]){
		if(to[i]^fa){
			f[to[i]]=max(f[k],e[i]);
			dfs(to[i],k);
		}
	}
}
inline bool get(int x,int y){
	for(register int i=1;i<=n;i++){
		if(a[x][y]>max(a[x][i],a[i][y])){
			fout<<3<<endl<<x<<" "<<y<<" "<<i<<endl;
			return true;
		}
		if(a[i][x]>max(a[x][y],a[y][i])){
			fout<<3<<endl<<x<<" "<<y<<" "<<i<<endl;
			return true;
		}
	}
	return false;
}
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	for(register int i=1;i<=n;++i){
		for(register int j=1;j<=n;++j){
			a[i][j]=read();
		}
	}
	for(register int i=1;i<=n;++i){
		if(a[i][i]){
			fout<<1<<endl<<i<<" 0 0"<<endl;
			return 0;
		}
	}
	for(register int i=1;i<=n;++i){
		for(register int j=i+1;j<=n;++j){
			if(a[i][j]!=a[j][i]){
				fout<<2<<endl<<i<<" "<<j<<" 0"<<endl;
				return 0;
			}
		}
	}
	if(n<=700){
		for(register int i=1;i<=n;++i){
			for(register int j=1;j<=n;++j){
				if(get(i,j))return 0;
			}
		}
		fout<<"0"<<endl;
		return 0;
	}
	for(register int t=2;t<=n;++t){
		int ma=1;
		for(register int i=2;i<=t-1;++i){
			if(a[ma][t]>a[i][t]){
				ma=i;
			}
		}
		add(ma,t,a[ma][t]),add(t,ma,a[t][ma]);
		memset(f,0,sizeof(f));
		dfs(t,t);
		for(register int i=1;i<=t-1;++i){
			if(a[i][t]<f[i]){
				if(get(i,t)){
					return 0;
				}
			}
		}
	}
	fout<<0<<endl;
	return 0;
}
/*

in:
3
0 1 2
1 0 2
2 2 0
out:
0

in:
2
0 1
2 3
out:
1
2 0 0
*/
