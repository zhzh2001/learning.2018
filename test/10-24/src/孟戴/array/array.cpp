#include<iostream>
#include<algorithm>
#include<fstream>
#include<math.h>
#include<stdio.h>
using namespace std;
ifstream fin("array.in");
ofstream fout("array.out");
inline long long read(){
	long long x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(long long x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x);
	puts("");
}
string s[19];
int f[(1<<18)+1],k[(1<<18)+1],x[100005];
int ans,n,m;
int main(){
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=n;i++){
		fin>>s[i];
	}
	for(int i=1;i<=m;i++){
		x[i]=0;
		for(int j=1;j<=n;j++){
			x[i]=x[i]*2+(int(s[j][i-1]-'0'));
		}
	}
	for(int i=(1<<n)-1;i>=0;i--){
		int tmp=0,now=i;
		while(now){
			tmp+=now&1;
			now>>=1;
		}
		k[i]=max(n-tmp,tmp);
	}
	for(int i=(1<<n)-1;i>=0;i--){
		for(int j=1;j<=m;j++){
			f[i]+=k[i^x[j]];
		}
		ans=max(f[i],ans);
	}
	fout<<n*m-ans;
	return 0;
}
/*

in:
3 4
0110
1010
0111

out:
2

*/
